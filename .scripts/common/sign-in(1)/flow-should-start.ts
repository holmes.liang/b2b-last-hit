import { WorkspaceExtensions } from "last-hit-types";

export default async (
  event: WorkspaceExtensions.FlowShouldStartEvent,
  helpers: WorkspaceExtensions.HandlerHelpers,
): Promise<WorkspaceExtensions.PreparedFlow> => {
  const { story, flow } = event;
  const { params } = flow;
  (params || []).forEach(param => {
    console.log(param.name, param.type, param.value, "123");
  });
  (flow as WorkspaceExtensions.PreparedFlow)._ = {
    input: {
      userName: "u0103",
      userPwd: "Im614000"
    },
  };
  return flow;
};
