import { WorkspaceExtensions } from "last-hit-types";

export default async (
  event: WorkspaceExtensions.FlowShouldStartEvent,
  helpers: WorkspaceExtensions.HandlerHelpers,
): Promise<WorkspaceExtensions.PreparedFlow> => {
  const { story, flow } = event;
  const { params } = flow;
  var Mock = require("mockjs");
  var randomData: any = Mock.mock({
    "userName": "u0103",
    "userPwd": "Im614000",
    "vehicleNo": /[S][\S]{0,9}/,
    "chassisNo": /[a-zA-Z0-9]{6,17}/,
    "name": "@name",
    "idNo": "@id",
    "email": "@email",
    "mobile": /1[0-9]{10}/,
    "postalCode": "542211",
    "unitNo": "@string('number', 3)",
  });
  (params || []).forEach(param => {
    console.log(param.name, param.type, param.value);
  });
  (flow as WorkspaceExtensions.PreparedFlow)._ = {
    input: randomData,
  };
  return flow;
};
