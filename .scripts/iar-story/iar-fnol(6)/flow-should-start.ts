import { WorkspaceExtensions } from "last-hit-types";

export default async (
  event: WorkspaceExtensions.FlowShouldStartEvent,
  helpers: WorkspaceExtensions.HandlerHelpers,
): Promise<WorkspaceExtensions.PreparedFlow> => {
  const { story, flow } = event;
  const { params } = flow;
  var Mock = require("mockjs");
  var randomData: any = Mock.mock({
    "userName": "u0103",
    "userPwd": "Im614000",
    "companyInfoName": "@name",
    "idNo": "@id",
    "email": "@email",
    "officeTel": /1[0-9]{10}/,
    "mobile": /1[0-9]{10}/,
    "postalCode": "542211",
    "unitNo": "@string('number', 3)",
    "locationPostalCode": "542211",
    "locationUnitNo": "@string('number', 3)",
    "locationSumInsured": "@integer(10000, 100000)",
    "locationRate": "@integer(0, 100)",
    "locationPremium": "@integer(1000, 10000)",
    "commissionRate": "@integer(0, 100)",
    "incidentDetails": "@sentence",
    "descriptionProperty": "@sentence",
    "purchasePrice": "@integer(1000, 10000)",
    "salvageValue": "@/purchasePrice",
    "marketValue": "@/purchasePrice",
    "estimatedRepairCost": "@/purchasePrice",
    "amountClaimed": "@/purchasePrice",
  });
  (params || []).forEach(param => {
    console.log(param.name, param.type, param.value);
  });
  (flow as WorkspaceExtensions.PreparedFlow)._ = {
    input: randomData,
  };
  return flow;
};
