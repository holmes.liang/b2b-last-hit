"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var last_hit_workspace_extension_1 = require("last-hit-workspace-extension");
var WorkspaceExtensionEntryPoint = /** @class */ (function (_super) {
    __extends(WorkspaceExtensionEntryPoint, _super);
    function WorkspaceExtensionEntryPoint() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WorkspaceExtensionEntryPoint.prototype.getHandlerLocation = function () {
        return __dirname;
    };
    return WorkspaceExtensionEntryPoint;
}(last_hit_workspace_extension_1.AbstractWorkspaceExtensionEntryPoint));
exports.default = new WorkspaceExtensionEntryPoint();
//# sourceMappingURL=index.js.map