__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "encodeContent", function() { return encodeContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "decodeContent", function() { return decodeContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return exportText; });
function encodeContent(text) {
  return text.split('&').join('&amp;').split('<').join('&lt;').split('>').join('&gt;').split('\xA0').join('&nbsp;').split('\n').join('<br />' + '\n');
}
function decodeContent(text) {
  return text.split('<br />' + '\n').join('\n');
}
function exportText(editorState) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
    encode: false
  };
  var content = editorState.getCurrentContent();
  var blockMap = content.getBlockMap();
  var encode = options.encode;
  return blockMap.map(function (block) {
    var resultText = '';
    var lastPosition = 0;
    var text = block.getText();
    block.findEntityRanges(function (character) {
      return !!character.getEntity();
    }, function (start, end) {
      var key = block.getEntityAt(start);
      var entityData = content.getEntity(key).getData();
      resultText += text.slice(lastPosition, start);
      resultText += entityData && entityData['export'] ? entityData['export'](entityData) : text.slice(start, end);
      lastPosition = end;
    });
    resultText += text.slice(lastPosition);
    return encode ? encodeContent(resultText) : resultText;
  }).join(encode ? '<br />\n' : '\n');
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvRWRpdG9yQ29yZS9leHBvcnQvZXhwb3J0VGV4dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1jb3JlL2VzL0VkaXRvckNvcmUvZXhwb3J0L2V4cG9ydFRleHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGVuY29kZUNvbnRlbnQodGV4dCkge1xuICAgIHJldHVybiB0ZXh0LnNwbGl0KCcmJykuam9pbignJmFtcDsnKS5zcGxpdCgnPCcpLmpvaW4oJyZsdDsnKS5zcGxpdCgnPicpLmpvaW4oJyZndDsnKS5zcGxpdCgnXFx4QTAnKS5qb2luKCcmbmJzcDsnKS5zcGxpdCgnXFxuJykuam9pbignPGJyIC8+JyArICdcXG4nKTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBkZWNvZGVDb250ZW50KHRleHQpIHtcbiAgICByZXR1cm4gdGV4dC5zcGxpdCgnPGJyIC8+JyArICdcXG4nKS5qb2luKCdcXG4nKTtcbn1cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGV4cG9ydFRleHQoZWRpdG9yU3RhdGUpIHtcbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogeyBlbmNvZGU6IGZhbHNlIH07XG5cbiAgICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIGJsb2NrTWFwID0gY29udGVudC5nZXRCbG9ja01hcCgpO1xuICAgIHZhciBlbmNvZGUgPSBvcHRpb25zLmVuY29kZTtcblxuICAgIHJldHVybiBibG9ja01hcC5tYXAoZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICAgIHZhciByZXN1bHRUZXh0ID0gJyc7XG4gICAgICAgIHZhciBsYXN0UG9zaXRpb24gPSAwO1xuICAgICAgICB2YXIgdGV4dCA9IGJsb2NrLmdldFRleHQoKTtcbiAgICAgICAgYmxvY2suZmluZEVudGl0eVJhbmdlcyhmdW5jdGlvbiAoY2hhcmFjdGVyKSB7XG4gICAgICAgICAgICByZXR1cm4gISFjaGFyYWN0ZXIuZ2V0RW50aXR5KCk7XG4gICAgICAgIH0sIGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgICAgICAgICB2YXIga2V5ID0gYmxvY2suZ2V0RW50aXR5QXQoc3RhcnQpO1xuICAgICAgICAgICAgdmFyIGVudGl0eURhdGEgPSBjb250ZW50LmdldEVudGl0eShrZXkpLmdldERhdGEoKTtcbiAgICAgICAgICAgIHJlc3VsdFRleHQgKz0gdGV4dC5zbGljZShsYXN0UG9zaXRpb24sIHN0YXJ0KTtcbiAgICAgICAgICAgIHJlc3VsdFRleHQgKz0gZW50aXR5RGF0YSAmJiBlbnRpdHlEYXRhWydleHBvcnQnXSA/IGVudGl0eURhdGFbJ2V4cG9ydCddKGVudGl0eURhdGEpIDogdGV4dC5zbGljZShzdGFydCwgZW5kKTtcbiAgICAgICAgICAgIGxhc3RQb3NpdGlvbiA9IGVuZDtcbiAgICAgICAgfSk7XG4gICAgICAgIHJlc3VsdFRleHQgKz0gdGV4dC5zbGljZShsYXN0UG9zaXRpb24pO1xuICAgICAgICByZXR1cm4gZW5jb2RlID8gZW5jb2RlQ29udGVudChyZXN1bHRUZXh0KSA6IHJlc3VsdFRleHQ7XG4gICAgfSkuam9pbihlbmNvZGUgPyAnPGJyIC8+XFxuJyA6ICdcXG4nKTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/EditorCore/export/exportText.js
