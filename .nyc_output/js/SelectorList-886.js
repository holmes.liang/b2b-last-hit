__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_animate_es_CSSMotionList__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-animate/es/CSSMotionList */ "./node_modules/rc-animate/es/CSSMotionList.js");
/* harmony import */ var _Selection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Selection */ "./node_modules/rc-tree-select/es/Selector/MultipleSelector/Selection.js");
/* harmony import */ var _SearchInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../SearchInput */ "./node_modules/rc-tree-select/es/SearchInput.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}






var NODE_SELECTOR = 'selector';
var NODE_SEARCH = 'search';
var TREE_SELECT_EMPTY_VALUE_KEY = 'RC_TREE_SELECT_EMPTY_VALUE_KEY';

var SelectorList = function SelectorList(props) {
  var selectorValueList = props.selectorValueList,
      choiceTransitionName = props.choiceTransitionName,
      prefixCls = props.prefixCls,
      onChoiceAnimationLeave = props.onChoiceAnimationLeave,
      labelInValue = props.labelInValue,
      maxTagCount = props.maxTagCount,
      maxTagPlaceholder = props.maxTagPlaceholder,
      showSearch = props.showSearch,
      valueEntities = props.valueEntities,
      inputRef = props.inputRef,
      onMultipleSelectorRemove = props.onMultipleSelectorRemove;
  var nodeKeys = []; // Check if `maxTagCount` is set

  var myValueList = selectorValueList;

  if (maxTagCount >= 0) {
    myValueList = selectorValueList.slice(0, maxTagCount);
  } // Basic selectors


  myValueList.forEach(function (_ref) {
    var label = _ref.label,
        value = _ref.value;

    var _ref2 = (valueEntities[value] || {}).node || {},
        _ref2$props = _ref2.props;

    _ref2$props = _ref2$props === void 0 ? {} : _ref2$props;
    var disabled = _ref2$props.disabled;
    nodeKeys.push({
      key: value,
      type: NODE_SELECTOR,
      label: label,
      value: value,
      disabled: disabled
    });
  }); // Rest node count

  if (maxTagCount >= 0 && maxTagCount < selectorValueList.length) {
    var content = "+ ".concat(selectorValueList.length - maxTagCount, " ...");

    if (typeof maxTagPlaceholder === 'string') {
      content = maxTagPlaceholder;
    } else if (typeof maxTagPlaceholder === 'function') {
      var restValueList = selectorValueList.slice(maxTagCount);
      content = maxTagPlaceholder(labelInValue ? restValueList : restValueList.map(function (_ref3) {
        var value = _ref3.value;
        return value;
      }));
    }

    nodeKeys.push({
      key: 'rc-tree-select-internal-max-tag-counter',
      type: NODE_SELECTOR,
      label: content,
      value: null,
      disabled: true
    });
  } // Search node


  if (showSearch !== false) {
    nodeKeys.push({
      key: '__input',
      type: NODE_SEARCH
    });
  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(rc_animate_es_CSSMotionList__WEBPACK_IMPORTED_MODULE_2__["default"], {
    keys: nodeKeys,
    className: "".concat(prefixCls, "-selection__rendered"),
    component: "ul",
    role: "menubar",
    motionName: choiceTransitionName,
    onLeaveEnd: onChoiceAnimationLeave
  }, function (_ref4) {
    var type = _ref4.type,
        label = _ref4.label,
        value = _ref4.value,
        disabled = _ref4.disabled,
        className = _ref4.className,
        style = _ref4.style;

    if (type === NODE_SELECTOR) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Selection__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, props, {
        className: className,
        style: style,
        key: value || TREE_SELECT_EMPTY_VALUE_KEY,
        label: label,
        value: value,
        onRemove: disabled ? null : onMultipleSelectorRemove
      }));
    }

    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
      className: "".concat(prefixCls, "-search ").concat(prefixCls, "-search--inline")
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SearchInput__WEBPACK_IMPORTED_MODULE_4__["default"], _extends({}, props, {
      ref: inputRef,
      needAlign: true
    })));
  });
};

SelectorList.propTypes = {
  selectorValueList: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  choiceTransitionName: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  onChoiceAnimationLeave: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  labelInValue: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  showSearch: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  maxTagCount: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number,
  maxTagPlaceholder: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func]),
  valueEntities: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  inputRef: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onMultipleSelectorRemove: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (SelectorList);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvU2VsZWN0b3IvTXVsdGlwbGVTZWxlY3Rvci9TZWxlY3Rvckxpc3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10cmVlLXNlbGVjdC9lcy9TZWxlY3Rvci9NdWx0aXBsZVNlbGVjdG9yL1NlbGVjdG9yTGlzdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBfZXh0ZW5kcygpIHsgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9OyByZXR1cm4gX2V4dGVuZHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTsgfVxuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBDU1NNb3Rpb25MaXN0IGZyb20gXCJyYy1hbmltYXRlL2VzL0NTU01vdGlvbkxpc3RcIjtcbmltcG9ydCBTZWxlY3Rpb24gZnJvbSAnLi9TZWxlY3Rpb24nO1xuaW1wb3J0IFNlYXJjaElucHV0IGZyb20gJy4uLy4uL1NlYXJjaElucHV0JztcbnZhciBOT0RFX1NFTEVDVE9SID0gJ3NlbGVjdG9yJztcbnZhciBOT0RFX1NFQVJDSCA9ICdzZWFyY2gnO1xudmFyIFRSRUVfU0VMRUNUX0VNUFRZX1ZBTFVFX0tFWSA9ICdSQ19UUkVFX1NFTEVDVF9FTVBUWV9WQUxVRV9LRVknO1xuXG52YXIgU2VsZWN0b3JMaXN0ID0gZnVuY3Rpb24gU2VsZWN0b3JMaXN0KHByb3BzKSB7XG4gIHZhciBzZWxlY3RvclZhbHVlTGlzdCA9IHByb3BzLnNlbGVjdG9yVmFsdWVMaXN0LFxuICAgICAgY2hvaWNlVHJhbnNpdGlvbk5hbWUgPSBwcm9wcy5jaG9pY2VUcmFuc2l0aW9uTmFtZSxcbiAgICAgIHByZWZpeENscyA9IHByb3BzLnByZWZpeENscyxcbiAgICAgIG9uQ2hvaWNlQW5pbWF0aW9uTGVhdmUgPSBwcm9wcy5vbkNob2ljZUFuaW1hdGlvbkxlYXZlLFxuICAgICAgbGFiZWxJblZhbHVlID0gcHJvcHMubGFiZWxJblZhbHVlLFxuICAgICAgbWF4VGFnQ291bnQgPSBwcm9wcy5tYXhUYWdDb3VudCxcbiAgICAgIG1heFRhZ1BsYWNlaG9sZGVyID0gcHJvcHMubWF4VGFnUGxhY2Vob2xkZXIsXG4gICAgICBzaG93U2VhcmNoID0gcHJvcHMuc2hvd1NlYXJjaCxcbiAgICAgIHZhbHVlRW50aXRpZXMgPSBwcm9wcy52YWx1ZUVudGl0aWVzLFxuICAgICAgaW5wdXRSZWYgPSBwcm9wcy5pbnB1dFJlZixcbiAgICAgIG9uTXVsdGlwbGVTZWxlY3RvclJlbW92ZSA9IHByb3BzLm9uTXVsdGlwbGVTZWxlY3RvclJlbW92ZTtcbiAgdmFyIG5vZGVLZXlzID0gW107IC8vIENoZWNrIGlmIGBtYXhUYWdDb3VudGAgaXMgc2V0XG5cbiAgdmFyIG15VmFsdWVMaXN0ID0gc2VsZWN0b3JWYWx1ZUxpc3Q7XG5cbiAgaWYgKG1heFRhZ0NvdW50ID49IDApIHtcbiAgICBteVZhbHVlTGlzdCA9IHNlbGVjdG9yVmFsdWVMaXN0LnNsaWNlKDAsIG1heFRhZ0NvdW50KTtcbiAgfSAvLyBCYXNpYyBzZWxlY3RvcnNcblxuXG4gIG15VmFsdWVMaXN0LmZvckVhY2goZnVuY3Rpb24gKF9yZWYpIHtcbiAgICB2YXIgbGFiZWwgPSBfcmVmLmxhYmVsLFxuICAgICAgICB2YWx1ZSA9IF9yZWYudmFsdWU7XG5cbiAgICB2YXIgX3JlZjIgPSAodmFsdWVFbnRpdGllc1t2YWx1ZV0gfHwge30pLm5vZGUgfHwge30sXG4gICAgICAgIF9yZWYyJHByb3BzID0gX3JlZjIucHJvcHM7XG5cbiAgICBfcmVmMiRwcm9wcyA9IF9yZWYyJHByb3BzID09PSB2b2lkIDAgPyB7fSA6IF9yZWYyJHByb3BzO1xuICAgIHZhciBkaXNhYmxlZCA9IF9yZWYyJHByb3BzLmRpc2FibGVkO1xuICAgIG5vZGVLZXlzLnB1c2goe1xuICAgICAga2V5OiB2YWx1ZSxcbiAgICAgIHR5cGU6IE5PREVfU0VMRUNUT1IsXG4gICAgICBsYWJlbDogbGFiZWwsXG4gICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICBkaXNhYmxlZDogZGlzYWJsZWRcbiAgICB9KTtcbiAgfSk7IC8vIFJlc3Qgbm9kZSBjb3VudFxuXG4gIGlmIChtYXhUYWdDb3VudCA+PSAwICYmIG1heFRhZ0NvdW50IDwgc2VsZWN0b3JWYWx1ZUxpc3QubGVuZ3RoKSB7XG4gICAgdmFyIGNvbnRlbnQgPSBcIisgXCIuY29uY2F0KHNlbGVjdG9yVmFsdWVMaXN0Lmxlbmd0aCAtIG1heFRhZ0NvdW50LCBcIiAuLi5cIik7XG5cbiAgICBpZiAodHlwZW9mIG1heFRhZ1BsYWNlaG9sZGVyID09PSAnc3RyaW5nJykge1xuICAgICAgY29udGVudCA9IG1heFRhZ1BsYWNlaG9sZGVyO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIG1heFRhZ1BsYWNlaG9sZGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICB2YXIgcmVzdFZhbHVlTGlzdCA9IHNlbGVjdG9yVmFsdWVMaXN0LnNsaWNlKG1heFRhZ0NvdW50KTtcbiAgICAgIGNvbnRlbnQgPSBtYXhUYWdQbGFjZWhvbGRlcihsYWJlbEluVmFsdWUgPyByZXN0VmFsdWVMaXN0IDogcmVzdFZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKF9yZWYzKSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IF9yZWYzLnZhbHVlO1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICB9KSk7XG4gICAgfVxuXG4gICAgbm9kZUtleXMucHVzaCh7XG4gICAgICBrZXk6ICdyYy10cmVlLXNlbGVjdC1pbnRlcm5hbC1tYXgtdGFnLWNvdW50ZXInLFxuICAgICAgdHlwZTogTk9ERV9TRUxFQ1RPUixcbiAgICAgIGxhYmVsOiBjb250ZW50LFxuICAgICAgdmFsdWU6IG51bGwsXG4gICAgICBkaXNhYmxlZDogdHJ1ZVxuICAgIH0pO1xuICB9IC8vIFNlYXJjaCBub2RlXG5cblxuICBpZiAoc2hvd1NlYXJjaCAhPT0gZmFsc2UpIHtcbiAgICBub2RlS2V5cy5wdXNoKHtcbiAgICAgIGtleTogJ19faW5wdXQnLFxuICAgICAgdHlwZTogTk9ERV9TRUFSQ0hcbiAgICB9KTtcbiAgfVxuXG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KENTU01vdGlvbkxpc3QsIHtcbiAgICBrZXlzOiBub2RlS2V5cyxcbiAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2VsZWN0aW9uX19yZW5kZXJlZFwiKSxcbiAgICBjb21wb25lbnQ6IFwidWxcIixcbiAgICByb2xlOiBcIm1lbnViYXJcIixcbiAgICBtb3Rpb25OYW1lOiBjaG9pY2VUcmFuc2l0aW9uTmFtZSxcbiAgICBvbkxlYXZlRW5kOiBvbkNob2ljZUFuaW1hdGlvbkxlYXZlXG4gIH0sIGZ1bmN0aW9uIChfcmVmNCkge1xuICAgIHZhciB0eXBlID0gX3JlZjQudHlwZSxcbiAgICAgICAgbGFiZWwgPSBfcmVmNC5sYWJlbCxcbiAgICAgICAgdmFsdWUgPSBfcmVmNC52YWx1ZSxcbiAgICAgICAgZGlzYWJsZWQgPSBfcmVmNC5kaXNhYmxlZCxcbiAgICAgICAgY2xhc3NOYW1lID0gX3JlZjQuY2xhc3NOYW1lLFxuICAgICAgICBzdHlsZSA9IF9yZWY0LnN0eWxlO1xuXG4gICAgaWYgKHR5cGUgPT09IE5PREVfU0VMRUNUT1IpIHtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFNlbGVjdGlvbiwgX2V4dGVuZHMoe30sIHByb3BzLCB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgIGtleTogdmFsdWUgfHwgVFJFRV9TRUxFQ1RfRU1QVFlfVkFMVUVfS0VZLFxuICAgICAgICBsYWJlbDogbGFiZWwsXG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgb25SZW1vdmU6IGRpc2FibGVkID8gbnVsbCA6IG9uTXVsdGlwbGVTZWxlY3RvclJlbW92ZVxuICAgICAgfSkpO1xuICAgIH1cblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwibGlcIiwge1xuICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNlYXJjaCBcIikuY29uY2F0KHByZWZpeENscywgXCItc2VhcmNoLS1pbmxpbmVcIilcbiAgICB9LCBSZWFjdC5jcmVhdGVFbGVtZW50KFNlYXJjaElucHV0LCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHtcbiAgICAgIHJlZjogaW5wdXRSZWYsXG4gICAgICBuZWVkQWxpZ246IHRydWVcbiAgICB9KSkpO1xuICB9KTtcbn07XG5cblNlbGVjdG9yTGlzdC5wcm9wVHlwZXMgPSB7XG4gIHNlbGVjdG9yVmFsdWVMaXN0OiBQcm9wVHlwZXMuYXJyYXksXG4gIGNob2ljZVRyYW5zaXRpb25OYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG9uQ2hvaWNlQW5pbWF0aW9uTGVhdmU6IFByb3BUeXBlcy5mdW5jLFxuICBsYWJlbEluVmFsdWU6IFByb3BUeXBlcy5ib29sLFxuICBzaG93U2VhcmNoOiBQcm9wVHlwZXMuYm9vbCxcbiAgbWF4VGFnQ291bnQ6IFByb3BUeXBlcy5udW1iZXIsXG4gIG1heFRhZ1BsYWNlaG9sZGVyOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubm9kZSwgUHJvcFR5cGVzLmZ1bmNdKSxcbiAgdmFsdWVFbnRpdGllczogUHJvcFR5cGVzLm9iamVjdCxcbiAgaW5wdXRSZWY6IFByb3BUeXBlcy5mdW5jLFxuICBvbk11bHRpcGxlU2VsZWN0b3JSZW1vdmU6IFByb3BUeXBlcy5mdW5jXG59O1xuZXhwb3J0IGRlZmF1bHQgU2VsZWN0b3JMaXN0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQWFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/es/Selector/MultipleSelector/SelectorList.js
