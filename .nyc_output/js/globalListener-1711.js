/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = makeInner();
var each = zrUtil.each;
/**
 * @param {string} key
 * @param {module:echarts/ExtensionAPI} api
 * @param {Function} handler
 *      param: {string} currTrigger
 *      param: {Array.<number>} point
 */

function register(key, api, handler) {
  if (env.node) {
    return;
  }

  var zr = api.getZr();
  inner(zr).records || (inner(zr).records = {});
  initGlobalListeners(zr, api);
  var record = inner(zr).records[key] || (inner(zr).records[key] = {});
  record.handler = handler;
}

function initGlobalListeners(zr, api) {
  if (inner(zr).initialized) {
    return;
  }

  inner(zr).initialized = true;
  useHandler('click', zrUtil.curry(doEnter, 'click'));
  useHandler('mousemove', zrUtil.curry(doEnter, 'mousemove')); // useHandler('mouseout', onLeave);

  useHandler('globalout', onLeave);

  function useHandler(eventType, cb) {
    zr.on(eventType, function (e) {
      var dis = makeDispatchAction(api);
      each(inner(zr).records, function (record) {
        record && cb(record, e, dis.dispatchAction);
      });
      dispatchTooltipFinally(dis.pendings, api);
    });
  }
}

function dispatchTooltipFinally(pendings, api) {
  var showLen = pendings.showTip.length;
  var hideLen = pendings.hideTip.length;
  var actuallyPayload;

  if (showLen) {
    actuallyPayload = pendings.showTip[showLen - 1];
  } else if (hideLen) {
    actuallyPayload = pendings.hideTip[hideLen - 1];
  }

  if (actuallyPayload) {
    actuallyPayload.dispatchAction = null;
    api.dispatchAction(actuallyPayload);
  }
}

function onLeave(record, e, dispatchAction) {
  record.handler('leave', null, dispatchAction);
}

function doEnter(currTrigger, record, e, dispatchAction) {
  record.handler(currTrigger, e, dispatchAction);
}

function makeDispatchAction(api) {
  var pendings = {
    showTip: [],
    hideTip: []
  }; // FIXME
  // better approach?
  // 'showTip' and 'hideTip' can be triggered by axisPointer and tooltip,
  // which may be conflict, (axisPointer call showTip but tooltip call hideTip);
  // So we have to add "final stage" to merge those dispatched actions.

  var dispatchAction = function dispatchAction(payload) {
    var pendingList = pendings[payload.type];

    if (pendingList) {
      pendingList.push(payload);
    } else {
      payload.dispatchAction = dispatchAction;
      api.dispatchAction(payload);
    }
  };

  return {
    dispatchAction: dispatchAction,
    pendings: pendings
  };
}
/**
 * @param {string} key
 * @param {module:echarts/ExtensionAPI} api
 */


function unregister(key, api) {
  if (env.node) {
    return;
  }

  var zr = api.getZr();
  var record = (inner(zr).records || {})[key];

  if (record) {
    inner(zr).records[key] = null;
  }
}

exports.register = register;
exports.unregister = unregister;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL2dsb2JhbExpc3RlbmVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL2dsb2JhbExpc3RlbmVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGVudiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL2VudlwiKTtcblxudmFyIF9tb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL21vZGVsXCIpO1xuXG52YXIgbWFrZUlubmVyID0gX21vZGVsLm1ha2VJbm5lcjtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGlubmVyID0gbWFrZUlubmVyKCk7XG52YXIgZWFjaCA9IHpyVXRpbC5lYWNoO1xuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBoYW5kbGVyXG4gKiAgICAgIHBhcmFtOiB7c3RyaW5nfSBjdXJyVHJpZ2dlclxuICogICAgICBwYXJhbToge0FycmF5LjxudW1iZXI+fSBwb2ludFxuICovXG5cbmZ1bmN0aW9uIHJlZ2lzdGVyKGtleSwgYXBpLCBoYW5kbGVyKSB7XG4gIGlmIChlbnYubm9kZSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciB6ciA9IGFwaS5nZXRacigpO1xuICBpbm5lcih6cikucmVjb3JkcyB8fCAoaW5uZXIoenIpLnJlY29yZHMgPSB7fSk7XG4gIGluaXRHbG9iYWxMaXN0ZW5lcnMoenIsIGFwaSk7XG4gIHZhciByZWNvcmQgPSBpbm5lcih6cikucmVjb3Jkc1trZXldIHx8IChpbm5lcih6cikucmVjb3Jkc1trZXldID0ge30pO1xuICByZWNvcmQuaGFuZGxlciA9IGhhbmRsZXI7XG59XG5cbmZ1bmN0aW9uIGluaXRHbG9iYWxMaXN0ZW5lcnMoenIsIGFwaSkge1xuICBpZiAoaW5uZXIoenIpLmluaXRpYWxpemVkKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaW5uZXIoenIpLmluaXRpYWxpemVkID0gdHJ1ZTtcbiAgdXNlSGFuZGxlcignY2xpY2snLCB6clV0aWwuY3VycnkoZG9FbnRlciwgJ2NsaWNrJykpO1xuICB1c2VIYW5kbGVyKCdtb3VzZW1vdmUnLCB6clV0aWwuY3VycnkoZG9FbnRlciwgJ21vdXNlbW92ZScpKTsgLy8gdXNlSGFuZGxlcignbW91c2VvdXQnLCBvbkxlYXZlKTtcblxuICB1c2VIYW5kbGVyKCdnbG9iYWxvdXQnLCBvbkxlYXZlKTtcblxuICBmdW5jdGlvbiB1c2VIYW5kbGVyKGV2ZW50VHlwZSwgY2IpIHtcbiAgICB6ci5vbihldmVudFR5cGUsIGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgZGlzID0gbWFrZURpc3BhdGNoQWN0aW9uKGFwaSk7XG4gICAgICBlYWNoKGlubmVyKHpyKS5yZWNvcmRzLCBmdW5jdGlvbiAocmVjb3JkKSB7XG4gICAgICAgIHJlY29yZCAmJiBjYihyZWNvcmQsIGUsIGRpcy5kaXNwYXRjaEFjdGlvbik7XG4gICAgICB9KTtcbiAgICAgIGRpc3BhdGNoVG9vbHRpcEZpbmFsbHkoZGlzLnBlbmRpbmdzLCBhcGkpO1xuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIGRpc3BhdGNoVG9vbHRpcEZpbmFsbHkocGVuZGluZ3MsIGFwaSkge1xuICB2YXIgc2hvd0xlbiA9IHBlbmRpbmdzLnNob3dUaXAubGVuZ3RoO1xuICB2YXIgaGlkZUxlbiA9IHBlbmRpbmdzLmhpZGVUaXAubGVuZ3RoO1xuICB2YXIgYWN0dWFsbHlQYXlsb2FkO1xuXG4gIGlmIChzaG93TGVuKSB7XG4gICAgYWN0dWFsbHlQYXlsb2FkID0gcGVuZGluZ3Muc2hvd1RpcFtzaG93TGVuIC0gMV07XG4gIH0gZWxzZSBpZiAoaGlkZUxlbikge1xuICAgIGFjdHVhbGx5UGF5bG9hZCA9IHBlbmRpbmdzLmhpZGVUaXBbaGlkZUxlbiAtIDFdO1xuICB9XG5cbiAgaWYgKGFjdHVhbGx5UGF5bG9hZCkge1xuICAgIGFjdHVhbGx5UGF5bG9hZC5kaXNwYXRjaEFjdGlvbiA9IG51bGw7XG4gICAgYXBpLmRpc3BhdGNoQWN0aW9uKGFjdHVhbGx5UGF5bG9hZCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gb25MZWF2ZShyZWNvcmQsIGUsIGRpc3BhdGNoQWN0aW9uKSB7XG4gIHJlY29yZC5oYW5kbGVyKCdsZWF2ZScsIG51bGwsIGRpc3BhdGNoQWN0aW9uKTtcbn1cblxuZnVuY3Rpb24gZG9FbnRlcihjdXJyVHJpZ2dlciwgcmVjb3JkLCBlLCBkaXNwYXRjaEFjdGlvbikge1xuICByZWNvcmQuaGFuZGxlcihjdXJyVHJpZ2dlciwgZSwgZGlzcGF0Y2hBY3Rpb24pO1xufVxuXG5mdW5jdGlvbiBtYWtlRGlzcGF0Y2hBY3Rpb24oYXBpKSB7XG4gIHZhciBwZW5kaW5ncyA9IHtcbiAgICBzaG93VGlwOiBbXSxcbiAgICBoaWRlVGlwOiBbXVxuICB9OyAvLyBGSVhNRVxuICAvLyBiZXR0ZXIgYXBwcm9hY2g/XG4gIC8vICdzaG93VGlwJyBhbmQgJ2hpZGVUaXAnIGNhbiBiZSB0cmlnZ2VyZWQgYnkgYXhpc1BvaW50ZXIgYW5kIHRvb2x0aXAsXG4gIC8vIHdoaWNoIG1heSBiZSBjb25mbGljdCwgKGF4aXNQb2ludGVyIGNhbGwgc2hvd1RpcCBidXQgdG9vbHRpcCBjYWxsIGhpZGVUaXApO1xuICAvLyBTbyB3ZSBoYXZlIHRvIGFkZCBcImZpbmFsIHN0YWdlXCIgdG8gbWVyZ2UgdGhvc2UgZGlzcGF0Y2hlZCBhY3Rpb25zLlxuXG4gIHZhciBkaXNwYXRjaEFjdGlvbiA9IGZ1bmN0aW9uIChwYXlsb2FkKSB7XG4gICAgdmFyIHBlbmRpbmdMaXN0ID0gcGVuZGluZ3NbcGF5bG9hZC50eXBlXTtcblxuICAgIGlmIChwZW5kaW5nTGlzdCkge1xuICAgICAgcGVuZGluZ0xpc3QucHVzaChwYXlsb2FkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGF5bG9hZC5kaXNwYXRjaEFjdGlvbiA9IGRpc3BhdGNoQWN0aW9uO1xuICAgICAgYXBpLmRpc3BhdGNoQWN0aW9uKHBheWxvYWQpO1xuICAgIH1cbiAgfTtcblxuICByZXR1cm4ge1xuICAgIGRpc3BhdGNoQWN0aW9uOiBkaXNwYXRjaEFjdGlvbixcbiAgICBwZW5kaW5nczogcGVuZGluZ3NcbiAgfTtcbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICovXG5cblxuZnVuY3Rpb24gdW5yZWdpc3RlcihrZXksIGFwaSkge1xuICBpZiAoZW52Lm5vZGUpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgenIgPSBhcGkuZ2V0WnIoKTtcbiAgdmFyIHJlY29yZCA9IChpbm5lcih6cikucmVjb3JkcyB8fCB7fSlba2V5XTtcblxuICBpZiAocmVjb3JkKSB7XG4gICAgaW5uZXIoenIpLnJlY29yZHNba2V5XSA9IG51bGw7XG4gIH1cbn1cblxuZXhwb3J0cy5yZWdpc3RlciA9IHJlZ2lzdGVyO1xuZXhwb3J0cy51bnJlZ2lzdGVyID0gdW5yZWdpc3RlcjsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/globalListener.js
