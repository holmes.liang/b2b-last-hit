__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/field-group.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n                .group {\n                   .ant-row {\n                        .ant-col {\n                            &.ant-form-item-label {\n                                display: none\n                            }\n                        }\n                   }\n                }\n                label {\n                    line-height: 40px;\n                    // padding-right: 10px;\n                    text-transform: uppercase;\n                    ::after {\n                        content: '';\n                        position: relative;\n                        top: -0.5px;\n                        margin: 0 8px 0 2px;\n                    }\n                    i {\n                        display: inline-block;\n                        margin-right: 4px;\n                        color: #f5222d;\n                        font-size: 14px;\n                        font-family: SimSun, sans-serif;\n                        line-height: 1;\n                    }\n                }\n                .ant-form-item-control-wrapper{\n                    width: 100%;\n                    .ant-input {\n                        border-left: 0;\n                        border-top-left-radius: 0;\n                        border-bottom-left-radius: 0;\n                    }\n\n                }\n                .ant-row {\n                  :focus {\n                      outline: transparent !important;\n                  }\n                }\n                .ant-col {\n                  :focus {\n                      outline: transparent !important;\n                  }\n                }\n                .ant-col label {\n                    color: #9e9e9e;\n                }\n                .ant-select-selection-selected-value{\n                    text-overflow: inherit;\n                }\n                .ant-select-selection{\n                    border-top-right-radius: 0;\n                    border-bottom-right-radius: 0;\n                }\n                .ant-select-selection__rendered {\n                    min-width: ", ";\n                }\n                .group {\n                    .ant-form-item:last-child {\n                        width: 100%;\n                    }\n                }\n                .mobile-input-group {\n                  .ant-col {\n                    width: 100%;\n                    &:first-child {\n                      line-height: 20px;\n                    }\n                  }\n                  .ant-select-selection__rendered {\n                    min-width: 100px;\n                  }\n                }\n                ", "\n   \n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}






var isMobile = _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsMobile();

var FieldGroup =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(FieldGroup, _ModelWidget);

  function FieldGroup(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, FieldGroup);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(FieldGroup).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(FieldGroup, [{
    key: "initComponents",
    value: function initComponents() {
      var minWidth = this.props.minWidth;
      return {
        InputGroup: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject(), minWidth || "60px", this.props.firstChildWithPercent ? "\n                     .ant-col.group >div:nth-child(1) {\n                    width: ".concat(this.props.firstChildWithPercent || 40, "% !important;\n                    margin-right: 10px;\n                }\n                ") : "")
      };
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          label = _this$props.label,
          selectXsSm = _this$props.selectXsSm,
          textXsSm = _this$props.textXsSm,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["label", "selectXsSm", "textXsSm"]);

      var children = this.props.children;
      var arrLabel = [];
      var required;
      var count = 0;

      if (Array.isArray(children)) {
        (children || []).map(function (every) {
          if (every && every.props && every.props.label) {
            arrLabel.push(every.props.label);
          }

          if (every && every.props && every.props.required) {
            count++;
          }
        });
        required = count > 0;
      } else {
        required = false;
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.InputGroup, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Row"], {
        className: isMobile ? "mobile-input-group" : "",
        type: "flex",
        justify: "start",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        xs: !selectXsSm ? 8 : selectXsSm.xs,
        sm: !selectXsSm ? 7 : selectXsSm.sm,
        style: {
          textAlign: isMobile ? "inherit" : "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, (this.props.required || required) && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, "*"), label || arrLabel.join(" / "))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        className: "group",
        xs: !textXsSm ? 16 : textXsSm.xs,
        sm: !textXsSm ? 13 : textXsSm.sm,
        style: {
          display: "flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }, children)));
    }
  }]);

  return FieldGroup;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (FieldGroup);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZpZWxkLWdyb3VwLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9maWVsZC1ncm91cC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgU2VsZWN0UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvc2VsZWN0XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmV4cG9ydCB0eXBlIHNlbGVjdFhzU20gPSB7XG4gIHhzOiBudW1iZXIsXG4gIHNtOiBudW1iZXJcbn07XG5leHBvcnQgdHlwZSB0ZXh0WHNTbSA9IHtcbiAgeHM6IG51bWJlcixcbiAgc206IG51bWJlclxufTtcbmV4cG9ydCB0eXBlIG1vZGVsID0ge1xuICBtb2RlbFNlbGVjdD86IGFueSxcbiAgbW9kZWxUZXh0PzogYW55XG59O1xuZXhwb3J0IHR5cGUgcHJvcE5hbWUgPSB7XG4gIHByb3BOYW1lU2VsZWN0OiBzdHJpbmcsXG4gIHByb3BOYW1lVGV4dDogc3RyaW5nLFxufTtcbmV4cG9ydCB0eXBlIE5TZWxlY3RQcm9wcyA9IHtcbiAgbGFiZWw/OiBSZWFjdC5SZWFjdE5vZGUsXG4gIHNlbGVjdFhzU20/OiBzZWxlY3RYc1NtLFxuICB0ZXh0WHNTbT86IHRleHRYc1NtLFxuICBtaW5XaWR0aD86IHN0cmluZztcbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICBmaXJzdENoaWxkV2l0aFBlcmNlbnQ/OiBudW1iZXI7XG59ICYgU2VsZWN0UHJvcHMgJiBNb2RlbFdpZGdldFByb3BzO1xuXG50eXBlIE5TZWxlY3RTdGF0ZSA9IHtcbiAgb3B0aW9uczogYW55W11cbn1cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBOU2VsZWN0VGV4dENvbXBvbmVudHMgPSB7XG4gIElucHV0R3JvdXA6IFN0eWxlZERJVixcbn1cblxuY2xhc3MgRmllbGRHcm91cDxQIGV4dGVuZHMgTlNlbGVjdFByb3BzLCBTIGV4dGVuZHMgTlNlbGVjdFN0YXRlLCBDIGV4dGVuZHMgTlNlbGVjdFRleHRDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IE5TZWxlY3RQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICBjb25zdCB7IG1pbldpZHRoIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiB7XG4gICAgICBJbnB1dEdyb3VwOiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgICAgIC5ncm91cCB7XG4gICAgICAgICAgICAgICAgICAgLmFudC1yb3cge1xuICAgICAgICAgICAgICAgICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICYuYW50LWZvcm0taXRlbS1sYWJlbCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsYWJlbCB7XG4gICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICAgICAgICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgICAgICAgICA6OmFmdGVyIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAtMC41cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDAgOHB4IDAgMnB4O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2Y1MjIyZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBTaW1TdW4sIHNhbnMtc2VyaWY7XG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuYW50LWZvcm0taXRlbS1jb250cm9sLXdyYXBwZXJ7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICAuYW50LWlucHV0IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuYW50LXJvdyB7XG4gICAgICAgICAgICAgICAgICA6Zm9jdXMge1xuICAgICAgICAgICAgICAgICAgICAgIG91dGxpbmU6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5hbnQtY29sIHtcbiAgICAgICAgICAgICAgICAgIDpmb2N1cyB7XG4gICAgICAgICAgICAgICAgICAgICAgb3V0bGluZTogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmFudC1jb2wgbGFiZWwge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzllOWU5ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmFudC1zZWxlY3Qtc2VsZWN0aW9uLXNlbGVjdGVkLXZhbHVle1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBpbmhlcml0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuYW50LXNlbGVjdC1zZWxlY3Rpb257XG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLmFudC1zZWxlY3Qtc2VsZWN0aW9uX19yZW5kZXJlZCB7XG4gICAgICAgICAgICAgICAgICAgIG1pbi13aWR0aDogJHttaW5XaWR0aCB8fCBcIjYwcHhcIn07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5ncm91cCB7XG4gICAgICAgICAgICAgICAgICAgIC5hbnQtZm9ybS1pdGVtOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLm1vYmlsZS1pbnB1dC1ncm91cCB7XG4gICAgICAgICAgICAgICAgICAuYW50LWNvbCB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgLmFudC1zZWxlY3Qtc2VsZWN0aW9uX19yZW5kZXJlZCB7XG4gICAgICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMTAwcHg7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICR7XG4gICAgICAgIHRoaXMucHJvcHMuZmlyc3RDaGlsZFdpdGhQZXJjZW50ID8gYFxuICAgICAgICAgICAgICAgICAgICAgLmFudC1jb2wuZ3JvdXAgPmRpdjpudGgtY2hpbGQoMSkge1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogJHt0aGlzLnByb3BzLmZpcnN0Q2hpbGRXaXRoUGVyY2VudCB8fCA0MH0lICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYCA6IFwiXCJcbiAgICAgIH1cbiAgIFxuICAgICAgICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuXG4gICAgY29uc3QgeyBsYWJlbCwgc2VsZWN0WHNTbSwgdGV4dFhzU20sIC4uLnJlc3QgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGNoaWxkcmVuOiBhbnkgPSB0aGlzLnByb3BzLmNoaWxkcmVuO1xuICAgIGxldCBhcnJMYWJlbDogYW55ID0gW107XG4gICAgbGV0IHJlcXVpcmVkOiBib29sZWFuO1xuICAgIGxldCBjb3VudDogbnVtYmVyID0gMDtcbiAgICBpZiAoQXJyYXkuaXNBcnJheShjaGlsZHJlbikpIHtcbiAgICAgIChjaGlsZHJlbiB8fCBbXSkubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICAgIGlmIChldmVyeSAmJiBldmVyeS5wcm9wcyAmJiBldmVyeS5wcm9wcy5sYWJlbCkge1xuICAgICAgICAgIGFyckxhYmVsLnB1c2goZXZlcnkucHJvcHMubGFiZWwpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChldmVyeSAmJiBldmVyeS5wcm9wcyAmJiBldmVyeS5wcm9wcy5yZXF1aXJlZCkge1xuICAgICAgICAgIGNvdW50Kys7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmVxdWlyZWQgPSBjb3VudCA+IDA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlcXVpcmVkID0gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICA8Qy5JbnB1dEdyb3VwIHsuLi5yZXN0fT5cbiAgICAgICAgPFJvdyBjbGFzc05hbWU9e2lzTW9iaWxlID8gXCJtb2JpbGUtaW5wdXQtZ3JvdXBcIiA6IFwiXCJ9IHR5cGU9XCJmbGV4XCIganVzdGlmeT1cInN0YXJ0XCI+XG4gICAgICAgICAgPENvbCB4cz17IXNlbGVjdFhzU20gPyA4IDogc2VsZWN0WHNTbS54c30gc209eyFzZWxlY3RYc1NtID8gNyA6IHNlbGVjdFhzU20uc219XG4gICAgICAgICAgICAgICBzdHlsZT17eyB0ZXh0QWxpZ246IGlzTW9iaWxlID8gXCJpbmhlcml0XCIgOiBcInJpZ2h0XCIgfX0+XG4gICAgICAgICAgICA8bGFiZWw+eyh0aGlzLnByb3BzLnJlcXVpcmVkIHx8IHJlcXVpcmVkKSAmJiAoPGk+KjwvaT4pfXtsYWJlbCB8fCBhcnJMYWJlbC5qb2luKFwiIC8gXCIpfTwvbGFiZWw+XG4gICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgPENvbCBjbGFzc05hbWU9XCJncm91cFwiIHhzPXshdGV4dFhzU20gPyAxNiA6IHRleHRYc1NtLnhzfSBzbT17IXRleHRYc1NtID8gMTMgOiB0ZXh0WHNTbS5zbX1cbiAgICAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6IFwiZmxleFwiIH19PlxuICAgICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICAgIDwvQ29sPlxuICAgICAgICA8L1Jvdz5cbiAgICAgIDwvQy5JbnB1dEdyb3VwPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRmllbGRHcm91cDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQWtDQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQUE7QUFFQTtBQUNBO0FBREE7QUF5RkE7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7OztBQXJJQTtBQUNBO0FBdUlBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/field-group.tsx
