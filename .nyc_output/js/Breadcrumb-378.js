__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Breadcrumb; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _BreadcrumbItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./BreadcrumbItem */ "./node_modules/antd/es/breadcrumb/BreadcrumbItem.js");
/* harmony import */ var _menu__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../menu */ "./node_modules/antd/es/menu/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};











function getBreadcrumbName(route, params) {
  if (!route.breadcrumbName) {
    return null;
  }

  var paramsKeys = Object.keys(params).join('|');
  var name = route.breadcrumbName.replace(new RegExp(":(".concat(paramsKeys, ")"), 'g'), function (replacement, key) {
    return params[key] || replacement;
  });
  return name;
}

function defaultItemRender(route, params, routes, paths) {
  var isLastItem = routes.indexOf(route) === routes.length - 1;
  var name = getBreadcrumbName(route, params);
  return isLastItem ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, name) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
    href: "#/".concat(paths.join('/'))
  }, name);
}

function filterFragment(children) {
  return Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__["default"])(children).map(function (element) {
    if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](element) && element.type === react__WEBPACK_IMPORTED_MODULE_0__["Fragment"]) {
      var props = element.props;
      return props.children;
    }

    return element;
  });
}

var Breadcrumb =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Breadcrumb, _React$Component);

  function Breadcrumb() {
    var _this;

    _classCallCheck(this, Breadcrumb);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Breadcrumb).apply(this, arguments));

    _this.getPath = function (path, params) {
      path = (path || '').replace(/^\//, '');
      Object.keys(params).forEach(function (key) {
        path = path.replace(":".concat(key), params[key]);
      });
      return path;
    };

    _this.addChildPath = function (paths) {
      var childPath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var params = arguments.length > 2 ? arguments[2] : undefined;

      var originalPaths = _toConsumableArray(paths);

      var path = _this.getPath(childPath, params);

      if (path) {
        originalPaths.push(path);
      }

      return originalPaths;
    };

    _this.genForRoutes = function (_ref) {
      var _ref$routes = _ref.routes,
          routes = _ref$routes === void 0 ? [] : _ref$routes,
          _ref$params = _ref.params,
          params = _ref$params === void 0 ? {} : _ref$params,
          separator = _ref.separator,
          _ref$itemRender = _ref.itemRender,
          itemRender = _ref$itemRender === void 0 ? defaultItemRender : _ref$itemRender;
      var paths = [];
      return routes.map(function (route) {
        var path = _this.getPath(route.path, params);

        if (path) {
          paths.push(path);
        } // generated overlay by route.children


        var overlay = null;

        if (route.children && route.children.length) {
          overlay = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_menu__WEBPACK_IMPORTED_MODULE_6__["default"], null, route.children.map(function (child) {
            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_menu__WEBPACK_IMPORTED_MODULE_6__["default"].Item, {
              key: child.breadcrumbName || child.path
            }, itemRender(child, params, routes, _this.addChildPath(paths, child.path, params)));
          }));
        }

        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_BreadcrumbItem__WEBPACK_IMPORTED_MODULE_5__["default"], {
          overlay: overlay,
          separator: separator,
          key: route.breadcrumbName || path
        }, itemRender(route, params, routes, paths));
      });
    };

    _this.renderBreadcrumb = function (_ref2) {
      var getPrefixCls = _ref2.getPrefixCls;
      var crumbs;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          separator = _a.separator,
          style = _a.style,
          className = _a.className,
          routes = _a.routes,
          children = _a.children,
          restProps = __rest(_a, ["prefixCls", "separator", "style", "className", "routes", "children"]);

      var prefixCls = getPrefixCls('breadcrumb', customizePrefixCls);

      if (routes && routes.length > 0) {
        // generated by route
        crumbs = _this.genForRoutes(_this.props);
      } else if (children) {
        crumbs = react__WEBPACK_IMPORTED_MODULE_0__["Children"].map(filterFragment(children), function (element, index) {
          if (!element) {
            return element;
          }

          Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(element.type && (element.type.__ANT_BREADCRUMB_ITEM === true || element.type.__ANT_BREADCRUMB_SEPARATOR === true), 'Breadcrumb', "Only accepts Breadcrumb.Item and Breadcrumb.Separator as it's children");
          return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](element, {
            separator: separator,
            key: index
          });
        });
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, prefixCls),
        style: style
      }, Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(restProps, ['itemRender', 'params'])), crumbs);
    };

    return _this;
  }

  _createClass(Breadcrumb, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var props = this.props;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!('linkRender' in props || 'nameRender' in props), 'Breadcrumb', '`linkRender` and `nameRender` are removed, please use `itemRender` instead, ' + 'see: https://u.ant.design/item-render.');
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderBreadcrumb);
    }
  }]);

  return Breadcrumb;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Breadcrumb.defaultProps = {
  separator: '/'
};
Breadcrumb.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  separator: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  routes: prop_types__WEBPACK_IMPORTED_MODULE_1__["array"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9icmVhZGNydW1iL0JyZWFkY3J1bWIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2JyZWFkY3J1bWIvQnJlYWRjcnVtYi5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgdG9BcnJheSBmcm9tICdyYy11dGlsL2xpYi9DaGlsZHJlbi90b0FycmF5JztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IEJyZWFkY3J1bWJJdGVtIGZyb20gJy4vQnJlYWRjcnVtYkl0ZW0nO1xuaW1wb3J0IE1lbnUgZnJvbSAnLi4vbWVudSc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbmZ1bmN0aW9uIGdldEJyZWFkY3J1bWJOYW1lKHJvdXRlLCBwYXJhbXMpIHtcbiAgICBpZiAoIXJvdXRlLmJyZWFkY3J1bWJOYW1lKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBjb25zdCBwYXJhbXNLZXlzID0gT2JqZWN0LmtleXMocGFyYW1zKS5qb2luKCd8Jyk7XG4gICAgY29uc3QgbmFtZSA9IHJvdXRlLmJyZWFkY3J1bWJOYW1lLnJlcGxhY2UobmV3IFJlZ0V4cChgOigke3BhcmFtc0tleXN9KWAsICdnJyksIChyZXBsYWNlbWVudCwga2V5KSA9PiBwYXJhbXNba2V5XSB8fCByZXBsYWNlbWVudCk7XG4gICAgcmV0dXJuIG5hbWU7XG59XG5mdW5jdGlvbiBkZWZhdWx0SXRlbVJlbmRlcihyb3V0ZSwgcGFyYW1zLCByb3V0ZXMsIHBhdGhzKSB7XG4gICAgY29uc3QgaXNMYXN0SXRlbSA9IHJvdXRlcy5pbmRleE9mKHJvdXRlKSA9PT0gcm91dGVzLmxlbmd0aCAtIDE7XG4gICAgY29uc3QgbmFtZSA9IGdldEJyZWFkY3J1bWJOYW1lKHJvdXRlLCBwYXJhbXMpO1xuICAgIHJldHVybiBpc0xhc3RJdGVtID8gPHNwYW4+e25hbWV9PC9zcGFuPiA6IDxhIGhyZWY9e2AjLyR7cGF0aHMuam9pbignLycpfWB9PntuYW1lfTwvYT47XG59XG5mdW5jdGlvbiBmaWx0ZXJGcmFnbWVudChjaGlsZHJlbikge1xuICAgIHJldHVybiB0b0FycmF5KGNoaWxkcmVuKS5tYXAoKGVsZW1lbnQpID0+IHtcbiAgICAgICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGVsZW1lbnQpICYmIGVsZW1lbnQudHlwZSA9PT0gUmVhY3QuRnJhZ21lbnQpIHtcbiAgICAgICAgICAgIGNvbnN0IHByb3BzID0gZWxlbWVudC5wcm9wcztcbiAgICAgICAgICAgIHJldHVybiBwcm9wcy5jaGlsZHJlbjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZWxlbWVudDtcbiAgICB9KTtcbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJyZWFkY3J1bWIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLmdldFBhdGggPSAocGF0aCwgcGFyYW1zKSA9PiB7XG4gICAgICAgICAgICBwYXRoID0gKHBhdGggfHwgJycpLnJlcGxhY2UoL15cXC8vLCAnJyk7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhwYXJhbXMpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgICAgICAgICAgICBwYXRoID0gcGF0aC5yZXBsYWNlKGA6JHtrZXl9YCwgcGFyYW1zW2tleV0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gcGF0aDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5hZGRDaGlsZFBhdGggPSAocGF0aHMsIGNoaWxkUGF0aCA9ICcnLCBwYXJhbXMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IG9yaWdpbmFsUGF0aHMgPSBbLi4ucGF0aHNdO1xuICAgICAgICAgICAgY29uc3QgcGF0aCA9IHRoaXMuZ2V0UGF0aChjaGlsZFBhdGgsIHBhcmFtcyk7XG4gICAgICAgICAgICBpZiAocGF0aCkge1xuICAgICAgICAgICAgICAgIG9yaWdpbmFsUGF0aHMucHVzaChwYXRoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBvcmlnaW5hbFBhdGhzO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmdlbkZvclJvdXRlcyA9ICh7IHJvdXRlcyA9IFtdLCBwYXJhbXMgPSB7fSwgc2VwYXJhdG9yLCBpdGVtUmVuZGVyID0gZGVmYXVsdEl0ZW1SZW5kZXIsIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHBhdGhzID0gW107XG4gICAgICAgICAgICByZXR1cm4gcm91dGVzLm1hcChyb3V0ZSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgcGF0aCA9IHRoaXMuZ2V0UGF0aChyb3V0ZS5wYXRoLCBwYXJhbXMpO1xuICAgICAgICAgICAgICAgIGlmIChwYXRoKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhdGhzLnB1c2gocGF0aCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIGdlbmVyYXRlZCBvdmVybGF5IGJ5IHJvdXRlLmNoaWxkcmVuXG4gICAgICAgICAgICAgICAgbGV0IG92ZXJsYXkgPSBudWxsO1xuICAgICAgICAgICAgICAgIGlmIChyb3V0ZS5jaGlsZHJlbiAmJiByb3V0ZS5jaGlsZHJlbi5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmxheSA9ICg8TWVudT5cbiAgICAgICAgICAgIHtyb3V0ZS5jaGlsZHJlbi5tYXAoY2hpbGQgPT4gKDxNZW51Lkl0ZW0ga2V5PXtjaGlsZC5icmVhZGNydW1iTmFtZSB8fCBjaGlsZC5wYXRofT5cbiAgICAgICAgICAgICAgICB7aXRlbVJlbmRlcihjaGlsZCwgcGFyYW1zLCByb3V0ZXMsIHRoaXMuYWRkQ2hpbGRQYXRoKHBhdGhzLCBjaGlsZC5wYXRoLCBwYXJhbXMpKX1cbiAgICAgICAgICAgICAgPC9NZW51Lkl0ZW0+KSl9XG4gICAgICAgICAgPC9NZW51Pik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiAoPEJyZWFkY3J1bWJJdGVtIG92ZXJsYXk9e292ZXJsYXl9IHNlcGFyYXRvcj17c2VwYXJhdG9yfSBrZXk9e3JvdXRlLmJyZWFkY3J1bWJOYW1lIHx8IHBhdGh9PlxuICAgICAgICAgIHtpdGVtUmVuZGVyKHJvdXRlLCBwYXJhbXMsIHJvdXRlcywgcGF0aHMpfVxuICAgICAgICA8L0JyZWFkY3J1bWJJdGVtPik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJCcmVhZGNydW1iID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGxldCBjcnVtYnM7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHNlcGFyYXRvciwgc3R5bGUsIGNsYXNzTmFtZSwgcm91dGVzLCBjaGlsZHJlbiB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwic2VwYXJhdG9yXCIsIFwic3R5bGVcIiwgXCJjbGFzc05hbWVcIiwgXCJyb3V0ZXNcIiwgXCJjaGlsZHJlblwiXSk7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2JyZWFkY3J1bWInLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgaWYgKHJvdXRlcyAmJiByb3V0ZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIC8vIGdlbmVyYXRlZCBieSByb3V0ZVxuICAgICAgICAgICAgICAgIGNydW1icyA9IHRoaXMuZ2VuRm9yUm91dGVzKHRoaXMucHJvcHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICBjcnVtYnMgPSBSZWFjdC5DaGlsZHJlbi5tYXAoZmlsdGVyRnJhZ21lbnQoY2hpbGRyZW4pLCAoZWxlbWVudCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFlbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZWxlbWVudDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB3YXJuaW5nKGVsZW1lbnQudHlwZSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgKGVsZW1lbnQudHlwZS5fX0FOVF9CUkVBRENSVU1CX0lURU0gPT09IHRydWUgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50LnR5cGUuX19BTlRfQlJFQURDUlVNQl9TRVBBUkFUT1IgPT09IHRydWUpLCAnQnJlYWRjcnVtYicsIFwiT25seSBhY2NlcHRzIEJyZWFkY3J1bWIuSXRlbSBhbmQgQnJlYWRjcnVtYi5TZXBhcmF0b3IgYXMgaXQncyBjaGlsZHJlblwiKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChlbGVtZW50LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZXBhcmF0b3IsXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6IGluZGV4LFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoY2xhc3NOYW1lLCBwcmVmaXhDbHMpfSBzdHlsZT17c3R5bGV9IHsuLi5vbWl0KHJlc3RQcm9wcywgWydpdGVtUmVuZGVyJywgJ3BhcmFtcyddKX0+XG4gICAgICAgIHtjcnVtYnN9XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgY29uc3QgeyBwcm9wcyB9ID0gdGhpcztcbiAgICAgICAgd2FybmluZyghKCdsaW5rUmVuZGVyJyBpbiBwcm9wcyB8fCAnbmFtZVJlbmRlcicgaW4gcHJvcHMpLCAnQnJlYWRjcnVtYicsICdgbGlua1JlbmRlcmAgYW5kIGBuYW1lUmVuZGVyYCBhcmUgcmVtb3ZlZCwgcGxlYXNlIHVzZSBgaXRlbVJlbmRlcmAgaW5zdGVhZCwgJyArXG4gICAgICAgICAgICAnc2VlOiBodHRwczovL3UuYW50LmRlc2lnbi9pdGVtLXJlbmRlci4nKTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckJyZWFkY3J1bWJ9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQnJlYWRjcnVtYi5kZWZhdWx0UHJvcHMgPSB7XG4gICAgc2VwYXJhdG9yOiAnLycsXG59O1xuQnJlYWRjcnVtYi5wcm9wVHlwZXMgPSB7XG4gICAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlcGFyYXRvcjogUHJvcFR5cGVzLm5vZGUsXG4gICAgcm91dGVzOiBQcm9wVHlwZXMuYXJyYXksXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBT0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTEE7QUFDQTtBQU1BO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBTkE7QUFDQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFIQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQUZBO0FBQ0E7QUFvQkE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQVBBO0FBWUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBdEJBO0FBQ0E7QUF2Q0E7QUFnRUE7QUFDQTs7O0FBQUE7QUFBQTtBQUVBO0FBRUE7OztBQUNBO0FBQ0E7QUFDQTs7OztBQXpFQTtBQUNBO0FBREE7QUEyRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFIQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/breadcrumb/Breadcrumb.js
