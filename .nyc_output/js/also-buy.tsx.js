__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlsoBuyItem", function() { return AlsoBuyItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlsoBuyItems", function() { return AlsoBuyItems; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_crosss_sell_pa__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk-component/crosss-sell/pa */ "./src/app/desk/component/crosss-sell/pa.tsx");
/* harmony import */ var _desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/query/prod-type */ "./src/app/desk/component/query/prod-type.tsx");



var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/success/components/also-buy.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .buy {\n            .ant-row {\n                .ant-col {\n                    :first-child {\n                        text-align: right;\n                        font-size: 13px;\n                        color: #9e9e9e;\n                        text-transform: uppercase;\n                    }\n                    :last-child {\n                        font-size: 16px;\n                        color: rgba(0, 0, 0, 0.85);\n                    }\n                    .star {\n                        display: inline-block;\n                        margin-right: 4px;\n                        color: #f5222d;\n                        font-size: 14px;\n                        font-family: SimSun, sans-serif;\n                        line-height: 1;\n                        content: '*';\n                    }\n                }\n                &.t-r {\n                    line-height: 42px;\n                    .ant-col {\n                        :first-child {\n                            padding-right: 20px;\n                        }\n                    }\n                }\n            }\n        }\n        .mobile-buy {\n          padding: 20px;\n          .ant-collapse-header {\n           padding: 12px 0;\n          }\n          .page-title--main {\n            padding: 12px 0;\n          }\n        }\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var isMobile = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].getIsMobile();
var AlsoBuyItem = function AlsoBuyItem(item, index, statelist, selectItem, issueItem) {
  var productCate = lodash__WEBPACK_IMPORTED_MODULE_3___default.a.get(item, "productCate");

  var productCode = lodash__WEBPACK_IMPORTED_MODULE_3___default.a.get(item, "productCode");

  var productName = lodash__WEBPACK_IMPORTED_MODULE_3___default.a.get(item, "productName");

  var header = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_10__["ProdTypeImage"], {
    productCate: productCate,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("span", {
    className: "card-title",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    },
    __self: this
  }, productName), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Switch"], {
    disabled: statelist[index].issued,
    style: {
      float: "right"
    },
    checked: statelist[index].selected,
    onChange: function onChange(checked) {
      console.log("Switch change");
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }), !_common__WEBPACK_IMPORTED_MODULE_6__["Utils"].getIsMobile() && _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("span", {
    className: "price-card",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].renderPolicyPremium(item))));
  return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_7__["NPanel"], {
    key: index,
    showArrow: false,
    header: header,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: this
  }, productCode === "CMI" && _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_8__["CrossSellCMI"], {
    policy: item,
    handleIssue: function handleIssue() {
      issueItem(index);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }), productCode === "PA" && _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component_crosss_sell_pa__WEBPACK_IMPORTED_MODULE_9__["default"], {
    policy: item,
    handleIssue: function handleIssue() {
      issueItem(index);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }), productCode === "BOSS" && _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_8__["CrossSellSME"], {
    policy: item,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }), productCode === "GHS" && _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_8__["CrossSellGHS"], {
    policy: item,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }));
};

function useShow(initData) {
  var _React$useState = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].useState(initData.map(function (item) {
    return {
      selected: false,
      issued: false
    };
  })),
      _React$useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_React$useState, 2),
      statelist = _React$useState2[0],
      changeStatelist = _React$useState2[1];

  var selectItem = function selectItem(index) {
    var newStatelist = statelist.map(function (item, itemIndex) {
      if (itemIndex === index) return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, item, {
        selected: true
      });
      return item;
    });
    changeStatelist(newStatelist);
  };

  var issueItem = function issueItem(index) {
    var newStatelist = statelist.map(function (item, itemIndex) {
      if (itemIndex === index) return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, item, {
        issued: true
      });
      return item;
    });
    changeStatelist(newStatelist);
  };

  var clearSelect = function clearSelect() {
    var newStatelist = statelist.map(function (item, itemIndex) {
      return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, item, {
        selected: false
      });
    });
    changeStatelist(newStatelist);
  };

  return [statelist, selectItem, issueItem, clearSelect];
}

var AlsoBuyItems = function AlsoBuyItems(props) {
  var items = lodash__WEBPACK_IMPORTED_MODULE_3___default.a.get(props.model, "crossSellProducts", []) || [];

  var _useShow = useShow(items),
      _useShow2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_2__["default"])(_useShow, 4),
      statelist = _useShow2[0],
      selectItem = _useShow2[1],
      issueItem = _useShow2[2],
      clearSelect = _useShow2[3];

  return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(StyleAlsoBuy, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
    className: "buy ".concat(isMobile ? "mobile-buy" : ""),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Divider"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
    className: "page-title page-title--main",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_6__["Language"].en("You might also want to buy").thai("คุณอาจสนใจซื้อประกันเหล่านี้เพิ่มเติม").my("သင်တို့သည်လည်းဝယ်ချင်ပေလိမ့်မည်").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_7__["NCollapse"], {
    className: "form-collapse",
    onChange: function onChange(keys) {
      clearSelect();
      keys.forEach(function (item) {
        selectItem(parseInt(item));
      });
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: this
  }, items.map(function (item, index) {
    return AlsoBuyItem(item, index, statelist, selectItem, issueItem);
  }))));
};
var StyleAlsoBuy = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["Styled"].div(_templateObject());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9zdWNjZXNzL2NvbXBvbmVudHMvYWxzby1idXkudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9zdWNjZXNzL2NvbXBvbmVudHMvYWxzby1idXkudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IERpdmlkZXIsIFN3aXRjaCB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmltcG9ydCB7IExhbmd1YWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBOQ29sbGFwc2UsIE5QYW5lbCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQ3Jvc3NTZWxsQ01JLCBDcm9zc1NlbGxTTUUsIENyb3NzU2VsbEdIUyB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCBDcm9zc1NlbGxQQSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2Nyb3Nzcy1zZWxsL3BhXCI7XG5pbXBvcnQgeyBQcm9kVHlwZUltYWdlIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9xdWVyeS9wcm9kLXR5cGVcIjtcblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuZXhwb3J0IGNvbnN0IEFsc29CdXlJdGVtID0gKGl0ZW06IGFueSwgaW5kZXg6IG51bWJlciwgc3RhdGVsaXN0OiBhbnksIHNlbGVjdEl0ZW06IGFueSwgaXNzdWVJdGVtOiBhbnkpID0+IHtcbiAgY29uc3QgcHJvZHVjdENhdGUgPSBfLmdldChpdGVtLCBcInByb2R1Y3RDYXRlXCIpO1xuICBjb25zdCBwcm9kdWN0Q29kZSA9IF8uZ2V0KGl0ZW0sIFwicHJvZHVjdENvZGVcIik7XG4gIGNvbnN0IHByb2R1Y3ROYW1lID0gXy5nZXQoaXRlbSwgXCJwcm9kdWN0TmFtZVwiKTtcbiAgY29uc3QgaGVhZGVyID0gPGRpdj5cbiAgICA8UHJvZFR5cGVJbWFnZSBwcm9kdWN0Q2F0ZT17cHJvZHVjdENhdGV9Lz5cbiAgICA8c3BhbiBjbGFzc05hbWU9XCJjYXJkLXRpdGxlXCI+XG4gICAgICAgICAge3Byb2R1Y3ROYW1lfVxuICAgIDwvc3Bhbj5cblxuICAgIDxTd2l0Y2hcbiAgICAgIGRpc2FibGVkPXtzdGF0ZWxpc3RbaW5kZXhdLmlzc3VlZH1cbiAgICAgIHN0eWxlPXt7IGZsb2F0OiBcInJpZ2h0XCIgfX1cbiAgICAgIGNoZWNrZWQ9e3N0YXRlbGlzdFtpbmRleF0uc2VsZWN0ZWR9XG4gICAgICBvbkNoYW5nZT17Y2hlY2tlZCA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiU3dpdGNoIGNoYW5nZVwiKTtcbiAgICAgIH19XG4gICAgLz5cbiAgICB7IVV0aWxzLmdldElzTW9iaWxlKCkgJiYgKFxuICAgICAgPGRpdj5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicHJpY2UtY2FyZFwiPntVdGlscy5yZW5kZXJQb2xpY3lQcmVtaXVtKGl0ZW0pfTwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgICl9XG4gIDwvZGl2PjtcbiAgcmV0dXJuIDxOUGFuZWxcbiAgICBrZXk9e2luZGV4fVxuICAgIHNob3dBcnJvdz17ZmFsc2V9XG4gICAgaGVhZGVyPXtoZWFkZXJ9XG4gID5cbiAgICB7cHJvZHVjdENvZGUgPT09IFwiQ01JXCIgJiYgPENyb3NzU2VsbENNSVxuICAgICAgcG9saWN5PXtpdGVtfVxuICAgICAgaGFuZGxlSXNzdWU9eygpID0+IHtcbiAgICAgICAgaXNzdWVJdGVtKGluZGV4KTtcbiAgICAgIH19XG4gICAgLz59XG4gICAge3Byb2R1Y3RDb2RlID09PSBcIlBBXCIgJiYgPENyb3NzU2VsbFBBXG4gICAgICBwb2xpY3k9e2l0ZW19XG4gICAgICBoYW5kbGVJc3N1ZT17KCkgPT4ge1xuICAgICAgICBpc3N1ZUl0ZW0oaW5kZXgpO1xuICAgICAgfX1cbiAgICAvPn1cbiAgICB7cHJvZHVjdENvZGUgPT09IFwiQk9TU1wiICYmIDxDcm9zc1NlbGxTTUVcbiAgICAgIHBvbGljeT17aXRlbX1cbiAgICAvPn1cbiAgICB7cHJvZHVjdENvZGUgPT09IFwiR0hTXCIgJiYgPENyb3NzU2VsbEdIU1xuICAgICAgcG9saWN5PXtpdGVtfVxuICAgIC8+fVxuICA8L05QYW5lbD47XG59O1xuXG5mdW5jdGlvbiB1c2VTaG93KGluaXREYXRhOiBhbnkpIHtcbiAgY29uc3QgW3N0YXRlbGlzdCwgY2hhbmdlU3RhdGVsaXN0XSA9IFJlYWN0LnVzZVN0YXRlKGluaXREYXRhLm1hcCgoaXRlbTogYW55KSA9PiAoe1xuICAgIHNlbGVjdGVkOiBmYWxzZSxcbiAgICBpc3N1ZWQ6IGZhbHNlLFxuICB9KSkpO1xuICBjb25zdCBzZWxlY3RJdGVtID0gKGluZGV4OiBudW1iZXIpID0+IHtcbiAgICBjb25zdCBuZXdTdGF0ZWxpc3QgPSBzdGF0ZWxpc3QubWFwKChpdGVtOiBhbnksIGl0ZW1JbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBpZiAoaXRlbUluZGV4ID09PSBpbmRleCkgcmV0dXJuICh7IC4uLml0ZW0sIHNlbGVjdGVkOiB0cnVlIH0pO1xuICAgICAgcmV0dXJuIGl0ZW07XG4gICAgfSk7XG4gICAgY2hhbmdlU3RhdGVsaXN0KG5ld1N0YXRlbGlzdCk7XG4gIH07XG4gIGNvbnN0IGlzc3VlSXRlbSA9IChpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgY29uc3QgbmV3U3RhdGVsaXN0ID0gc3RhdGVsaXN0Lm1hcCgoaXRlbTogYW55LCBpdGVtSW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgaWYgKGl0ZW1JbmRleCA9PT0gaW5kZXgpIHJldHVybiAoeyAuLi5pdGVtLCBpc3N1ZWQ6IHRydWUgfSk7XG4gICAgICByZXR1cm4gaXRlbTtcbiAgICB9KTtcbiAgICBjaGFuZ2VTdGF0ZWxpc3QobmV3U3RhdGVsaXN0KTtcbiAgfTtcbiAgY29uc3QgY2xlYXJTZWxlY3QgPSAoKSA9PiB7XG4gICAgY29uc3QgbmV3U3RhdGVsaXN0ID0gc3RhdGVsaXN0Lm1hcCgoaXRlbTogYW55LCBpdGVtSW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgcmV0dXJuICh7IC4uLml0ZW0sIHNlbGVjdGVkOiBmYWxzZSB9KTtcbiAgICB9KTtcbiAgICBjaGFuZ2VTdGF0ZWxpc3QobmV3U3RhdGVsaXN0KTtcbiAgfTtcbiAgcmV0dXJuIFtzdGF0ZWxpc3QsIHNlbGVjdEl0ZW0sIGlzc3VlSXRlbSwgY2xlYXJTZWxlY3RdO1xufVxuXG5cbmV4cG9ydCBjb25zdCBBbHNvQnV5SXRlbXMgPSAocHJvcHM6IHsgbW9kZWw6IGFueSB9KSA9PiB7XG4gIGNvbnN0IGl0ZW1zID0gXy5nZXQocHJvcHMubW9kZWwsIFwiY3Jvc3NTZWxsUHJvZHVjdHNcIiwgW10pIHx8IFtdO1xuICBjb25zdCBbc3RhdGVsaXN0LCBzZWxlY3RJdGVtLCBpc3N1ZUl0ZW0sIGNsZWFyU2VsZWN0XSA9IHVzZVNob3coaXRlbXMpO1xuICByZXR1cm4gPFN0eWxlQWxzb0J1eT5cbiAgICA8ZGl2IGNsYXNzTmFtZT17YGJ1eSAke2lzTW9iaWxlID8gXCJtb2JpbGUtYnV5XCIgOiBcIlwifWB9PlxuICAgICAgPERpdmlkZXIvPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLXRpdGxlIHBhZ2UtdGl0bGUtLW1haW5cIj5cbiAgICAgICAge0xhbmd1YWdlLmVuKFwiWW91IG1pZ2h0IGFsc28gd2FudCB0byBidXlcIilcbiAgICAgICAgICAudGhhaShcIuC4hOC4uOC4k+C4reC4suC4iOC4quC4meC5g+C4iOC4i+C4t+C5ieC4reC4m+C4o+C4sOC4geC4seC4meC5gOC4q+C4peC5iOC4suC4meC4teC5ieC5gOC4nuC4tOC5iOC4oeC5gOC4leC4tOC4oVwiKVxuICAgICAgICAgIC5teShcIuGAnuGAhOGAuuGAkOGAreGAr+GAt+GAnuGAiuGAuuGAnOGAiuGAuuGAuOGAneGAmuGAuuGAgeGAu+GAhOGAuuGAleGAseGAnOGAreGAmeGAuuGAt+GAmeGAiuGAulwiKVxuICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICA8L2Rpdj5cbiAgICAgIDxOQ29sbGFwc2VcbiAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb2xsYXBzZVwiXG4gICAgICAgIG9uQ2hhbmdlPXsoa2V5czogYW55W10gfCBhbnkpID0+IHtcbiAgICAgICAgICBjbGVhclNlbGVjdCgpO1xuICAgICAgICAgIGtleXMuZm9yRWFjaCgoaXRlbTogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICBzZWxlY3RJdGVtKHBhcnNlSW50KGl0ZW0pKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfX1cbiAgICAgID5cbiAgICAgICAge2l0ZW1zLm1hcCgoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiBBbHNvQnV5SXRlbShpdGVtLCBpbmRleCwgc3RhdGVsaXN0LCBzZWxlY3RJdGVtLCBpc3N1ZUl0ZW0pKX1cbiAgICAgIDwvTkNvbGxhcHNlPlxuICAgIDwvZGl2PlxuICA8L1N0eWxlQWxzb0J1eT47XG59O1xuXG5cbmNvbnN0IFN0eWxlQWxzb0J1eSA9IFN0eWxlZC5kaXZgXG4gICAgICAgIC5idXkge1xuICAgICAgICAgICAgLmFudC1yb3cge1xuICAgICAgICAgICAgICAgIC5hbnQtY29sIHtcbiAgICAgICAgICAgICAgICAgICAgOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM5ZTllOWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIDpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC5zdGFyIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNmNTIyMmQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LWZhbWlseTogU2ltU3VuLCBzYW5zLXNlcmlmO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiAnKic7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJi50LXIge1xuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogNDJweDtcbiAgICAgICAgICAgICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgICAgICAgICAgICAgOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5tb2JpbGUtYnV5IHtcbiAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgICAgIC5hbnQtY29sbGFwc2UtaGVhZGVyIHtcbiAgICAgICAgICAgcGFkZGluZzogMTJweCAwO1xuICAgICAgICAgIH1cbiAgICAgICAgICAucGFnZS10aXRsZS0tbWFpbiB7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMnB4IDA7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbmA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBSUE7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/success/components/also-buy.tsx
