/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createListFromArray = __webpack_require__(/*! ./chart/helper/createListFromArray */ "./node_modules/echarts/lib/chart/helper/createListFromArray.js");

var axisHelper = __webpack_require__(/*! ./coord/axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var axisModelCommonMixin = __webpack_require__(/*! ./coord/axisModelCommonMixin */ "./node_modules/echarts/lib/coord/axisModelCommonMixin.js");

var Model = __webpack_require__(/*! ./model/Model */ "./node_modules/echarts/lib/model/Model.js");

var _layout = __webpack_require__(/*! ./util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutRect = _layout.getLayoutRect;
exports.getLayoutRect = _layout.getLayoutRect;

var _dataStackHelper = __webpack_require__(/*! ./data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var enableDataStack = _dataStackHelper.enableDataStack;
var isDimensionStacked = _dataStackHelper.isDimensionStacked;
var getStackedDimension = _dataStackHelper.getStackedDimension;

var _completeDimensions = __webpack_require__(/*! ./data/helper/completeDimensions */ "./node_modules/echarts/lib/data/helper/completeDimensions.js");

exports.completeDimensions = _completeDimensions;

var _createDimensions = __webpack_require__(/*! ./data/helper/createDimensions */ "./node_modules/echarts/lib/data/helper/createDimensions.js");

exports.createDimensions = _createDimensions;

var _symbol = __webpack_require__(/*! ./util/symbol */ "./node_modules/echarts/lib/util/symbol.js");

exports.createSymbol = _symbol.createSymbol;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// import createGraphFromNodeEdge from './chart/helper/createGraphFromNodeEdge';

/**
 * Create a muti dimension List structure from seriesModel.
 * @param  {module:echarts/model/Model} seriesModel
 * @return {module:echarts/data/List} list
 */

function createList(seriesModel) {
  return createListFromArray(seriesModel.getSource(), seriesModel);
} // export function createGraph(seriesModel) {
//     var nodes = seriesModel.get('data');
//     var links = seriesModel.get('links');
//     return createGraphFromNodeEdge(nodes, links, seriesModel);
// }


var dataStack = {
  isDimensionStacked: isDimensionStacked,
  enableDataStack: enableDataStack,
  getStackedDimension: getStackedDimension
};
/**
 * Create a symbol element with given symbol configuration: shape, x, y, width, height, color
 * @see http://echarts.baidu.com/option.html#series-scatter.symbol
 * @param {string} symbolDesc
 * @param {number} x
 * @param {number} y
 * @param {number} w
 * @param {number} h
 * @param {string} color
 */

/**
 * Create scale
 * @param {Array.<number>} dataExtent
 * @param {Object|module:echarts/Model} option
 */

function createScale(dataExtent, option) {
  var axisModel = option;

  if (!Model.isInstance(option)) {
    axisModel = new Model(option);
    zrUtil.mixin(axisModel, axisModelCommonMixin);
  }

  var scale = axisHelper.createScaleByModel(axisModel);
  scale.setExtent(dataExtent[0], dataExtent[1]);
  axisHelper.niceScaleExtent(scale, axisModel);
  return scale;
}
/**
 * Mixin common methods to axis model,
 *
 * Inlcude methods
 * `getFormattedLabels() => Array.<string>`
 * `getCategories() => Array.<string>`
 * `getMin(origin: boolean) => number`
 * `getMax(origin: boolean) => number`
 * `getNeedCrossZero() => boolean`
 * `setRange(start: number, end: number)`
 * `resetRange()`
 */


function mixinAxisModelCommonMethods(Model) {
  zrUtil.mixin(Model, axisModelCommonMixin);
}

exports.createList = createList;
exports.dataStack = dataStack;
exports.createScale = createScale;
exports.mixinAxisModelCommonMethods = mixinAxisModelCommonMethods;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvaGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvaGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGNyZWF0ZUxpc3RGcm9tQXJyYXkgPSByZXF1aXJlKFwiLi9jaGFydC9oZWxwZXIvY3JlYXRlTGlzdEZyb21BcnJheVwiKTtcblxudmFyIGF4aXNIZWxwZXIgPSByZXF1aXJlKFwiLi9jb29yZC9heGlzSGVscGVyXCIpO1xuXG52YXIgYXhpc01vZGVsQ29tbW9uTWl4aW4gPSByZXF1aXJlKFwiLi9jb29yZC9heGlzTW9kZWxDb21tb25NaXhpblwiKTtcblxudmFyIE1vZGVsID0gcmVxdWlyZShcIi4vbW9kZWwvTW9kZWxcIik7XG5cbnZhciBfbGF5b3V0ID0gcmVxdWlyZShcIi4vdXRpbC9sYXlvdXRcIik7XG5cbnZhciBnZXRMYXlvdXRSZWN0ID0gX2xheW91dC5nZXRMYXlvdXRSZWN0O1xuZXhwb3J0cy5nZXRMYXlvdXRSZWN0ID0gX2xheW91dC5nZXRMYXlvdXRSZWN0O1xuXG52YXIgX2RhdGFTdGFja0hlbHBlciA9IHJlcXVpcmUoXCIuL2RhdGEvaGVscGVyL2RhdGFTdGFja0hlbHBlclwiKTtcblxudmFyIGVuYWJsZURhdGFTdGFjayA9IF9kYXRhU3RhY2tIZWxwZXIuZW5hYmxlRGF0YVN0YWNrO1xudmFyIGlzRGltZW5zaW9uU3RhY2tlZCA9IF9kYXRhU3RhY2tIZWxwZXIuaXNEaW1lbnNpb25TdGFja2VkO1xudmFyIGdldFN0YWNrZWREaW1lbnNpb24gPSBfZGF0YVN0YWNrSGVscGVyLmdldFN0YWNrZWREaW1lbnNpb247XG5cbnZhciBfY29tcGxldGVEaW1lbnNpb25zID0gcmVxdWlyZShcIi4vZGF0YS9oZWxwZXIvY29tcGxldGVEaW1lbnNpb25zXCIpO1xuXG5leHBvcnRzLmNvbXBsZXRlRGltZW5zaW9ucyA9IF9jb21wbGV0ZURpbWVuc2lvbnM7XG5cbnZhciBfY3JlYXRlRGltZW5zaW9ucyA9IHJlcXVpcmUoXCIuL2RhdGEvaGVscGVyL2NyZWF0ZURpbWVuc2lvbnNcIik7XG5cbmV4cG9ydHMuY3JlYXRlRGltZW5zaW9ucyA9IF9jcmVhdGVEaW1lbnNpb25zO1xuXG52YXIgX3N5bWJvbCA9IHJlcXVpcmUoXCIuL3V0aWwvc3ltYm9sXCIpO1xuXG5leHBvcnRzLmNyZWF0ZVN5bWJvbCA9IF9zeW1ib2wuY3JlYXRlU3ltYm9sO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG4vLyBpbXBvcnQgY3JlYXRlR3JhcGhGcm9tTm9kZUVkZ2UgZnJvbSAnLi9jaGFydC9oZWxwZXIvY3JlYXRlR3JhcGhGcm9tTm9kZUVkZ2UnO1xuXG4vKipcbiAqIENyZWF0ZSBhIG11dGkgZGltZW5zaW9uIExpc3Qgc3RydWN0dXJlIGZyb20gc2VyaWVzTW9kZWwuXG4gKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gc2VyaWVzTW9kZWxcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gbGlzdFxuICovXG5mdW5jdGlvbiBjcmVhdGVMaXN0KHNlcmllc01vZGVsKSB7XG4gIHJldHVybiBjcmVhdGVMaXN0RnJvbUFycmF5KHNlcmllc01vZGVsLmdldFNvdXJjZSgpLCBzZXJpZXNNb2RlbCk7XG59IC8vIGV4cG9ydCBmdW5jdGlvbiBjcmVhdGVHcmFwaChzZXJpZXNNb2RlbCkge1xuLy8gICAgIHZhciBub2RlcyA9IHNlcmllc01vZGVsLmdldCgnZGF0YScpO1xuLy8gICAgIHZhciBsaW5rcyA9IHNlcmllc01vZGVsLmdldCgnbGlua3MnKTtcbi8vICAgICByZXR1cm4gY3JlYXRlR3JhcGhGcm9tTm9kZUVkZ2Uobm9kZXMsIGxpbmtzLCBzZXJpZXNNb2RlbCk7XG4vLyB9XG5cblxudmFyIGRhdGFTdGFjayA9IHtcbiAgaXNEaW1lbnNpb25TdGFja2VkOiBpc0RpbWVuc2lvblN0YWNrZWQsXG4gIGVuYWJsZURhdGFTdGFjazogZW5hYmxlRGF0YVN0YWNrLFxuICBnZXRTdGFja2VkRGltZW5zaW9uOiBnZXRTdGFja2VkRGltZW5zaW9uXG59O1xuLyoqXG4gKiBDcmVhdGUgYSBzeW1ib2wgZWxlbWVudCB3aXRoIGdpdmVuIHN5bWJvbCBjb25maWd1cmF0aW9uOiBzaGFwZSwgeCwgeSwgd2lkdGgsIGhlaWdodCwgY29sb3JcbiAqIEBzZWUgaHR0cDovL2VjaGFydHMuYmFpZHUuY29tL29wdGlvbi5odG1sI3Nlcmllcy1zY2F0dGVyLnN5bWJvbFxuICogQHBhcmFtIHtzdHJpbmd9IHN5bWJvbERlc2NcbiAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gKiBAcGFyYW0ge251bWJlcn0geVxuICogQHBhcmFtIHtudW1iZXJ9IHdcbiAqIEBwYXJhbSB7bnVtYmVyfSBoXG4gKiBAcGFyYW0ge3N0cmluZ30gY29sb3JcbiAqL1xuXG4vKipcbiAqIENyZWF0ZSBzY2FsZVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gZGF0YUV4dGVudFxuICogQHBhcmFtIHtPYmplY3R8bW9kdWxlOmVjaGFydHMvTW9kZWx9IG9wdGlvblxuICovXG5mdW5jdGlvbiBjcmVhdGVTY2FsZShkYXRhRXh0ZW50LCBvcHRpb24pIHtcbiAgdmFyIGF4aXNNb2RlbCA9IG9wdGlvbjtcblxuICBpZiAoIU1vZGVsLmlzSW5zdGFuY2Uob3B0aW9uKSkge1xuICAgIGF4aXNNb2RlbCA9IG5ldyBNb2RlbChvcHRpb24pO1xuICAgIHpyVXRpbC5taXhpbihheGlzTW9kZWwsIGF4aXNNb2RlbENvbW1vbk1peGluKTtcbiAgfVxuXG4gIHZhciBzY2FsZSA9IGF4aXNIZWxwZXIuY3JlYXRlU2NhbGVCeU1vZGVsKGF4aXNNb2RlbCk7XG4gIHNjYWxlLnNldEV4dGVudChkYXRhRXh0ZW50WzBdLCBkYXRhRXh0ZW50WzFdKTtcbiAgYXhpc0hlbHBlci5uaWNlU2NhbGVFeHRlbnQoc2NhbGUsIGF4aXNNb2RlbCk7XG4gIHJldHVybiBzY2FsZTtcbn1cbi8qKlxuICogTWl4aW4gY29tbW9uIG1ldGhvZHMgdG8gYXhpcyBtb2RlbCxcbiAqXG4gKiBJbmxjdWRlIG1ldGhvZHNcbiAqIGBnZXRGb3JtYXR0ZWRMYWJlbHMoKSA9PiBBcnJheS48c3RyaW5nPmBcbiAqIGBnZXRDYXRlZ29yaWVzKCkgPT4gQXJyYXkuPHN0cmluZz5gXG4gKiBgZ2V0TWluKG9yaWdpbjogYm9vbGVhbikgPT4gbnVtYmVyYFxuICogYGdldE1heChvcmlnaW46IGJvb2xlYW4pID0+IG51bWJlcmBcbiAqIGBnZXROZWVkQ3Jvc3NaZXJvKCkgPT4gYm9vbGVhbmBcbiAqIGBzZXRSYW5nZShzdGFydDogbnVtYmVyLCBlbmQ6IG51bWJlcilgXG4gKiBgcmVzZXRSYW5nZSgpYFxuICovXG5cblxuZnVuY3Rpb24gbWl4aW5BeGlzTW9kZWxDb21tb25NZXRob2RzKE1vZGVsKSB7XG4gIHpyVXRpbC5taXhpbihNb2RlbCwgYXhpc01vZGVsQ29tbW9uTWl4aW4pO1xufVxuXG5leHBvcnRzLmNyZWF0ZUxpc3QgPSBjcmVhdGVMaXN0O1xuZXhwb3J0cy5kYXRhU3RhY2sgPSBkYXRhU3RhY2s7XG5leHBvcnRzLmNyZWF0ZVNjYWxlID0gY3JlYXRlU2NhbGU7XG5leHBvcnRzLm1peGluQXhpc01vZGVsQ29tbW9uTWV0aG9kcyA9IG1peGluQXhpc01vZGVsQ29tbW9uTWV0aG9kczsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBOzs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7Ozs7Ozs7Ozs7O0FBV0E7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/helper.js
