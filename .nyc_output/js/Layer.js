var util = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");

var _config = __webpack_require__(/*! ./config */ "./node_modules/zrender/lib/config.js");

var devicePixelRatio = _config.devicePixelRatio;

var Style = __webpack_require__(/*! ./graphic/Style */ "./node_modules/zrender/lib/graphic/Style.js");

var Pattern = __webpack_require__(/*! ./graphic/Pattern */ "./node_modules/zrender/lib/graphic/Pattern.js");
/**
 * @module zrender/Layer
 * @author pissang(https://www.github.com/pissang)
 */


function returnFalse() {
  return false;
}
/**
 * 创建dom
 *
 * @inner
 * @param {string} id dom id 待用
 * @param {Painter} painter painter instance
 * @param {number} number
 */


function createDom(id, painter, dpr) {
  var newDom = util.createCanvas();
  var width = painter.getWidth();
  var height = painter.getHeight();
  var newDomStyle = newDom.style;

  if (newDomStyle) {
    // In node or some other non-browser environment
    newDomStyle.position = 'absolute';
    newDomStyle.left = 0;
    newDomStyle.top = 0;
    newDomStyle.width = width + 'px';
    newDomStyle.height = height + 'px';
    newDom.setAttribute('data-zr-dom-id', id);
  }

  newDom.width = width * dpr;
  newDom.height = height * dpr;
  return newDom;
}
/**
 * @alias module:zrender/Layer
 * @constructor
 * @extends module:zrender/mixin/Transformable
 * @param {string} id
 * @param {module:zrender/Painter} painter
 * @param {number} [dpr]
 */


var Layer = function Layer(id, painter, dpr) {
  var dom;
  dpr = dpr || devicePixelRatio;

  if (typeof id === 'string') {
    dom = createDom(id, painter, dpr);
  } // Not using isDom because in node it will return false
  else if (util.isObject(id)) {
      dom = id;
      id = dom.id;
    }

  this.id = id;
  this.dom = dom;
  var domStyle = dom.style;

  if (domStyle) {
    // Not in node
    dom.onselectstart = returnFalse; // 避免页面选中的尴尬

    domStyle['-webkit-user-select'] = 'none';
    domStyle['user-select'] = 'none';
    domStyle['-webkit-touch-callout'] = 'none';
    domStyle['-webkit-tap-highlight-color'] = 'rgba(0,0,0,0)';
    domStyle['padding'] = 0;
    domStyle['margin'] = 0;
    domStyle['border-width'] = 0;
  }

  this.domBack = null;
  this.ctxBack = null;
  this.painter = painter;
  this.config = null; // Configs

  /**
   * 每次清空画布的颜色
   * @type {string}
   * @default 0
   */

  this.clearColor = 0;
  /**
   * 是否开启动态模糊
   * @type {boolean}
   * @default false
   */

  this.motionBlur = false;
  /**
   * 在开启动态模糊的时候使用，与上一帧混合的alpha值，值越大尾迹越明显
   * @type {number}
   * @default 0.7
   */

  this.lastFrameAlpha = 0.7;
  /**
   * Layer dpr
   * @type {number}
   */

  this.dpr = dpr;
};

Layer.prototype = {
  constructor: Layer,
  __dirty: true,
  __used: false,
  __drawIndex: 0,
  __startIndex: 0,
  __endIndex: 0,
  incremental: false,
  getElementCount: function getElementCount() {
    return this.__endIndex - this.__startIndex;
  },
  initContext: function initContext() {
    this.ctx = this.dom.getContext('2d');
    this.ctx.dpr = this.dpr;
  },
  createBackBuffer: function createBackBuffer() {
    var dpr = this.dpr;
    this.domBack = createDom('back-' + this.id, this.painter, dpr);
    this.ctxBack = this.domBack.getContext('2d');

    if (dpr !== 1) {
      this.ctxBack.scale(dpr, dpr);
    }
  },

  /**
   * @param  {number} width
   * @param  {number} height
   */
  resize: function resize(width, height) {
    var dpr = this.dpr;
    var dom = this.dom;
    var domStyle = dom.style;
    var domBack = this.domBack;

    if (domStyle) {
      domStyle.width = width + 'px';
      domStyle.height = height + 'px';
    }

    dom.width = width * dpr;
    dom.height = height * dpr;

    if (domBack) {
      domBack.width = width * dpr;
      domBack.height = height * dpr;

      if (dpr !== 1) {
        this.ctxBack.scale(dpr, dpr);
      }
    }
  },

  /**
   * 清空该层画布
   * @param {boolean} [clearAll]=false Clear all with out motion blur
   * @param {Color} [clearColor]
   */
  clear: function clear(clearAll, clearColor) {
    var dom = this.dom;
    var ctx = this.ctx;
    var width = dom.width;
    var height = dom.height;
    var clearColor = clearColor || this.clearColor;
    var haveMotionBLur = this.motionBlur && !clearAll;
    var lastFrameAlpha = this.lastFrameAlpha;
    var dpr = this.dpr;

    if (haveMotionBLur) {
      if (!this.domBack) {
        this.createBackBuffer();
      }

      this.ctxBack.globalCompositeOperation = 'copy';
      this.ctxBack.drawImage(dom, 0, 0, width / dpr, height / dpr);
    }

    ctx.clearRect(0, 0, width, height);

    if (clearColor && clearColor !== 'transparent') {
      var clearColorGradientOrPattern; // Gradient

      if (clearColor.colorStops) {
        // Cache canvas gradient
        clearColorGradientOrPattern = clearColor.__canvasGradient || Style.getGradient(ctx, clearColor, {
          x: 0,
          y: 0,
          width: width,
          height: height
        });
        clearColor.__canvasGradient = clearColorGradientOrPattern;
      } // Pattern
      else if (clearColor.image) {
          clearColorGradientOrPattern = Pattern.prototype.getCanvasPattern.call(clearColor, ctx);
        }

      ctx.save();
      ctx.fillStyle = clearColorGradientOrPattern || clearColor;
      ctx.fillRect(0, 0, width, height);
      ctx.restore();
    }

    if (haveMotionBLur) {
      var domBack = this.domBack;
      ctx.save();
      ctx.globalAlpha = lastFrameAlpha;
      ctx.drawImage(domBack, 0, 0, width, height);
      ctx.restore();
    }
  }
};
var _default = Layer;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvTGF5ZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9MYXllci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgdXRpbCA9IHJlcXVpcmUoXCIuL2NvcmUvdXRpbFwiKTtcblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi9jb25maWdcIik7XG5cbnZhciBkZXZpY2VQaXhlbFJhdGlvID0gX2NvbmZpZy5kZXZpY2VQaXhlbFJhdGlvO1xuXG52YXIgU3R5bGUgPSByZXF1aXJlKFwiLi9ncmFwaGljL1N0eWxlXCIpO1xuXG52YXIgUGF0dGVybiA9IHJlcXVpcmUoXCIuL2dyYXBoaWMvUGF0dGVyblwiKTtcblxuLyoqXG4gKiBAbW9kdWxlIHpyZW5kZXIvTGF5ZXJcbiAqIEBhdXRob3IgcGlzc2FuZyhodHRwczovL3d3dy5naXRodWIuY29tL3Bpc3NhbmcpXG4gKi9cbmZ1bmN0aW9uIHJldHVybkZhbHNlKCkge1xuICByZXR1cm4gZmFsc2U7XG59XG4vKipcbiAqIOWIm+W7umRvbVxuICpcbiAqIEBpbm5lclxuICogQHBhcmFtIHtzdHJpbmd9IGlkIGRvbSBpZCDlvoXnlKhcbiAqIEBwYXJhbSB7UGFpbnRlcn0gcGFpbnRlciBwYWludGVyIGluc3RhbmNlXG4gKiBAcGFyYW0ge251bWJlcn0gbnVtYmVyXG4gKi9cblxuXG5mdW5jdGlvbiBjcmVhdGVEb20oaWQsIHBhaW50ZXIsIGRwcikge1xuICB2YXIgbmV3RG9tID0gdXRpbC5jcmVhdGVDYW52YXMoKTtcbiAgdmFyIHdpZHRoID0gcGFpbnRlci5nZXRXaWR0aCgpO1xuICB2YXIgaGVpZ2h0ID0gcGFpbnRlci5nZXRIZWlnaHQoKTtcbiAgdmFyIG5ld0RvbVN0eWxlID0gbmV3RG9tLnN0eWxlO1xuXG4gIGlmIChuZXdEb21TdHlsZSkge1xuICAgIC8vIEluIG5vZGUgb3Igc29tZSBvdGhlciBub24tYnJvd3NlciBlbnZpcm9ubWVudFxuICAgIG5ld0RvbVN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcbiAgICBuZXdEb21TdHlsZS5sZWZ0ID0gMDtcbiAgICBuZXdEb21TdHlsZS50b3AgPSAwO1xuICAgIG5ld0RvbVN0eWxlLndpZHRoID0gd2lkdGggKyAncHgnO1xuICAgIG5ld0RvbVN0eWxlLmhlaWdodCA9IGhlaWdodCArICdweCc7XG4gICAgbmV3RG9tLnNldEF0dHJpYnV0ZSgnZGF0YS16ci1kb20taWQnLCBpZCk7XG4gIH1cblxuICBuZXdEb20ud2lkdGggPSB3aWR0aCAqIGRwcjtcbiAgbmV3RG9tLmhlaWdodCA9IGhlaWdodCAqIGRwcjtcbiAgcmV0dXJuIG5ld0RvbTtcbn1cbi8qKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL0xheWVyXG4gKiBAY29uc3RydWN0b3JcbiAqIEBleHRlbmRzIG1vZHVsZTp6cmVuZGVyL21peGluL1RyYW5zZm9ybWFibGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9QYWludGVyfSBwYWludGVyXG4gKiBAcGFyYW0ge251bWJlcn0gW2Rwcl1cbiAqL1xuXG5cbnZhciBMYXllciA9IGZ1bmN0aW9uIChpZCwgcGFpbnRlciwgZHByKSB7XG4gIHZhciBkb207XG4gIGRwciA9IGRwciB8fCBkZXZpY2VQaXhlbFJhdGlvO1xuXG4gIGlmICh0eXBlb2YgaWQgPT09ICdzdHJpbmcnKSB7XG4gICAgZG9tID0gY3JlYXRlRG9tKGlkLCBwYWludGVyLCBkcHIpO1xuICB9IC8vIE5vdCB1c2luZyBpc0RvbSBiZWNhdXNlIGluIG5vZGUgaXQgd2lsbCByZXR1cm4gZmFsc2VcbiAgZWxzZSBpZiAodXRpbC5pc09iamVjdChpZCkpIHtcbiAgICAgIGRvbSA9IGlkO1xuICAgICAgaWQgPSBkb20uaWQ7XG4gICAgfVxuXG4gIHRoaXMuaWQgPSBpZDtcbiAgdGhpcy5kb20gPSBkb207XG4gIHZhciBkb21TdHlsZSA9IGRvbS5zdHlsZTtcblxuICBpZiAoZG9tU3R5bGUpIHtcbiAgICAvLyBOb3QgaW4gbm9kZVxuICAgIGRvbS5vbnNlbGVjdHN0YXJ0ID0gcmV0dXJuRmFsc2U7IC8vIOmBv+WFjemhtemdoumAieS4reeahOWwtOWwrFxuXG4gICAgZG9tU3R5bGVbJy13ZWJraXQtdXNlci1zZWxlY3QnXSA9ICdub25lJztcbiAgICBkb21TdHlsZVsndXNlci1zZWxlY3QnXSA9ICdub25lJztcbiAgICBkb21TdHlsZVsnLXdlYmtpdC10b3VjaC1jYWxsb3V0J10gPSAnbm9uZSc7XG4gICAgZG9tU3R5bGVbJy13ZWJraXQtdGFwLWhpZ2hsaWdodC1jb2xvciddID0gJ3JnYmEoMCwwLDAsMCknO1xuICAgIGRvbVN0eWxlWydwYWRkaW5nJ10gPSAwO1xuICAgIGRvbVN0eWxlWydtYXJnaW4nXSA9IDA7XG4gICAgZG9tU3R5bGVbJ2JvcmRlci13aWR0aCddID0gMDtcbiAgfVxuXG4gIHRoaXMuZG9tQmFjayA9IG51bGw7XG4gIHRoaXMuY3R4QmFjayA9IG51bGw7XG4gIHRoaXMucGFpbnRlciA9IHBhaW50ZXI7XG4gIHRoaXMuY29uZmlnID0gbnVsbDsgLy8gQ29uZmlnc1xuXG4gIC8qKlxuICAgKiDmr4/mrKHmuIXnqbrnlLvluIPnmoTpopzoibJcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICogQGRlZmF1bHQgMFxuICAgKi9cblxuICB0aGlzLmNsZWFyQ29sb3IgPSAwO1xuICAvKipcbiAgICog5piv5ZCm5byA5ZCv5Yqo5oCB5qih57OKXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cblxuICB0aGlzLm1vdGlvbkJsdXIgPSBmYWxzZTtcbiAgLyoqXG4gICAqIOWcqOW8gOWQr+WKqOaAgeaooeeziueahOaXtuWAmeS9v+eUqO+8jOS4juS4iuS4gOW4p+a3t+WQiOeahGFscGhh5YC877yM5YC86LaK5aSn5bC+6L+56LaK5piO5pi+XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqIEBkZWZhdWx0IDAuN1xuICAgKi9cblxuICB0aGlzLmxhc3RGcmFtZUFscGhhID0gMC43O1xuICAvKipcbiAgICogTGF5ZXIgZHByXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuXG4gIHRoaXMuZHByID0gZHByO1xufTtcblxuTGF5ZXIucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogTGF5ZXIsXG4gIF9fZGlydHk6IHRydWUsXG4gIF9fdXNlZDogZmFsc2UsXG4gIF9fZHJhd0luZGV4OiAwLFxuICBfX3N0YXJ0SW5kZXg6IDAsXG4gIF9fZW5kSW5kZXg6IDAsXG4gIGluY3JlbWVudGFsOiBmYWxzZSxcbiAgZ2V0RWxlbWVudENvdW50OiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX19lbmRJbmRleCAtIHRoaXMuX19zdGFydEluZGV4O1xuICB9LFxuICBpbml0Q29udGV4dDogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuY3R4ID0gdGhpcy5kb20uZ2V0Q29udGV4dCgnMmQnKTtcbiAgICB0aGlzLmN0eC5kcHIgPSB0aGlzLmRwcjtcbiAgfSxcbiAgY3JlYXRlQmFja0J1ZmZlcjogZnVuY3Rpb24gKCkge1xuICAgIHZhciBkcHIgPSB0aGlzLmRwcjtcbiAgICB0aGlzLmRvbUJhY2sgPSBjcmVhdGVEb20oJ2JhY2stJyArIHRoaXMuaWQsIHRoaXMucGFpbnRlciwgZHByKTtcbiAgICB0aGlzLmN0eEJhY2sgPSB0aGlzLmRvbUJhY2suZ2V0Q29udGV4dCgnMmQnKTtcblxuICAgIGlmIChkcHIgIT09IDEpIHtcbiAgICAgIHRoaXMuY3R4QmFjay5zY2FsZShkcHIsIGRwcik7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHdpZHRoXG4gICAqIEBwYXJhbSAge251bWJlcn0gaGVpZ2h0XG4gICAqL1xuICByZXNpemU6IGZ1bmN0aW9uICh3aWR0aCwgaGVpZ2h0KSB7XG4gICAgdmFyIGRwciA9IHRoaXMuZHByO1xuICAgIHZhciBkb20gPSB0aGlzLmRvbTtcbiAgICB2YXIgZG9tU3R5bGUgPSBkb20uc3R5bGU7XG4gICAgdmFyIGRvbUJhY2sgPSB0aGlzLmRvbUJhY2s7XG5cbiAgICBpZiAoZG9tU3R5bGUpIHtcbiAgICAgIGRvbVN0eWxlLndpZHRoID0gd2lkdGggKyAncHgnO1xuICAgICAgZG9tU3R5bGUuaGVpZ2h0ID0gaGVpZ2h0ICsgJ3B4JztcbiAgICB9XG5cbiAgICBkb20ud2lkdGggPSB3aWR0aCAqIGRwcjtcbiAgICBkb20uaGVpZ2h0ID0gaGVpZ2h0ICogZHByO1xuXG4gICAgaWYgKGRvbUJhY2spIHtcbiAgICAgIGRvbUJhY2sud2lkdGggPSB3aWR0aCAqIGRwcjtcbiAgICAgIGRvbUJhY2suaGVpZ2h0ID0gaGVpZ2h0ICogZHByO1xuXG4gICAgICBpZiAoZHByICE9PSAxKSB7XG4gICAgICAgIHRoaXMuY3R4QmFjay5zY2FsZShkcHIsIGRwcik7XG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiDmuIXnqbror6XlsYLnlLvluINcbiAgICogQHBhcmFtIHtib29sZWFufSBbY2xlYXJBbGxdPWZhbHNlIENsZWFyIGFsbCB3aXRoIG91dCBtb3Rpb24gYmx1clxuICAgKiBAcGFyYW0ge0NvbG9yfSBbY2xlYXJDb2xvcl1cbiAgICovXG4gIGNsZWFyOiBmdW5jdGlvbiAoY2xlYXJBbGwsIGNsZWFyQ29sb3IpIHtcbiAgICB2YXIgZG9tID0gdGhpcy5kb207XG4gICAgdmFyIGN0eCA9IHRoaXMuY3R4O1xuICAgIHZhciB3aWR0aCA9IGRvbS53aWR0aDtcbiAgICB2YXIgaGVpZ2h0ID0gZG9tLmhlaWdodDtcbiAgICB2YXIgY2xlYXJDb2xvciA9IGNsZWFyQ29sb3IgfHwgdGhpcy5jbGVhckNvbG9yO1xuICAgIHZhciBoYXZlTW90aW9uQkx1ciA9IHRoaXMubW90aW9uQmx1ciAmJiAhY2xlYXJBbGw7XG4gICAgdmFyIGxhc3RGcmFtZUFscGhhID0gdGhpcy5sYXN0RnJhbWVBbHBoYTtcbiAgICB2YXIgZHByID0gdGhpcy5kcHI7XG5cbiAgICBpZiAoaGF2ZU1vdGlvbkJMdXIpIHtcbiAgICAgIGlmICghdGhpcy5kb21CYWNrKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlQmFja0J1ZmZlcigpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmN0eEJhY2suZ2xvYmFsQ29tcG9zaXRlT3BlcmF0aW9uID0gJ2NvcHknO1xuICAgICAgdGhpcy5jdHhCYWNrLmRyYXdJbWFnZShkb20sIDAsIDAsIHdpZHRoIC8gZHByLCBoZWlnaHQgLyBkcHIpO1xuICAgIH1cblxuICAgIGN0eC5jbGVhclJlY3QoMCwgMCwgd2lkdGgsIGhlaWdodCk7XG5cbiAgICBpZiAoY2xlYXJDb2xvciAmJiBjbGVhckNvbG9yICE9PSAndHJhbnNwYXJlbnQnKSB7XG4gICAgICB2YXIgY2xlYXJDb2xvckdyYWRpZW50T3JQYXR0ZXJuOyAvLyBHcmFkaWVudFxuXG4gICAgICBpZiAoY2xlYXJDb2xvci5jb2xvclN0b3BzKSB7XG4gICAgICAgIC8vIENhY2hlIGNhbnZhcyBncmFkaWVudFxuICAgICAgICBjbGVhckNvbG9yR3JhZGllbnRPclBhdHRlcm4gPSBjbGVhckNvbG9yLl9fY2FudmFzR3JhZGllbnQgfHwgU3R5bGUuZ2V0R3JhZGllbnQoY3R4LCBjbGVhckNvbG9yLCB7XG4gICAgICAgICAgeDogMCxcbiAgICAgICAgICB5OiAwLFxuICAgICAgICAgIHdpZHRoOiB3aWR0aCxcbiAgICAgICAgICBoZWlnaHQ6IGhlaWdodFxuICAgICAgICB9KTtcbiAgICAgICAgY2xlYXJDb2xvci5fX2NhbnZhc0dyYWRpZW50ID0gY2xlYXJDb2xvckdyYWRpZW50T3JQYXR0ZXJuO1xuICAgICAgfSAvLyBQYXR0ZXJuXG4gICAgICBlbHNlIGlmIChjbGVhckNvbG9yLmltYWdlKSB7XG4gICAgICAgICAgY2xlYXJDb2xvckdyYWRpZW50T3JQYXR0ZXJuID0gUGF0dGVybi5wcm90b3R5cGUuZ2V0Q2FudmFzUGF0dGVybi5jYWxsKGNsZWFyQ29sb3IsIGN0eCk7XG4gICAgICAgIH1cblxuICAgICAgY3R4LnNhdmUoKTtcbiAgICAgIGN0eC5maWxsU3R5bGUgPSBjbGVhckNvbG9yR3JhZGllbnRPclBhdHRlcm4gfHwgY2xlYXJDb2xvcjtcbiAgICAgIGN0eC5maWxsUmVjdCgwLCAwLCB3aWR0aCwgaGVpZ2h0KTtcbiAgICAgIGN0eC5yZXN0b3JlKCk7XG4gICAgfVxuXG4gICAgaWYgKGhhdmVNb3Rpb25CTHVyKSB7XG4gICAgICB2YXIgZG9tQmFjayA9IHRoaXMuZG9tQmFjaztcbiAgICAgIGN0eC5zYXZlKCk7XG4gICAgICBjdHguZ2xvYmFsQWxwaGEgPSBsYXN0RnJhbWVBbHBoYTtcbiAgICAgIGN0eC5kcmF3SW1hZ2UoZG9tQmFjaywgMCwgMCwgd2lkdGgsIGhlaWdodCk7XG4gICAgICBjdHgucmVzdG9yZSgpO1xuICAgIH1cbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IExheWVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBN0dBO0FBK0dBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/Layer.js
