__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonBack", function() { return ButtonBack; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/antd/button-back.tsx";






var ButtonBackReal = function ButtonBackReal(props) {
  var onClick = props.onClick,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(props, ["onClick"]);

  var backText = props.backText ? props.backText : _common__WEBPACK_IMPORTED_MODULE_5__["Language"].en("Back").thai("กลับ").my("ပြန်.").getMessage();
  if (!props.handel && _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsInApp()) return _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  });
  return _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Button"], Object.assign({
    size: "large"
  }, rest, {
    onClick: function onClick() {
      if (!props.handel) {
        if (props.history.length === 1) {
          //TODO 暂时解决window open 的页面back默认跳转到policy query
          props.history.push(_common__WEBPACK_IMPORTED_MODULE_5__["PATH"].POLICIES_QUERY);
        } else {
          props.history.goBack();
        }
      } else if (lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isString(props.handel)) {
        props.history.push(props.handel);
      } else {
        props.handel(props.history);
      }
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }), backText);
}; // 返回必须用这个组件


var ButtonBack = Object(react_router__WEBPACK_IMPORTED_MODULE_4__["withRouter"])(ButtonBackReal);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FudGQvYnV0dG9uLWJhY2sudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FudGQvYnV0dG9uLWJhY2sudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IEJ1dHRvbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgd2l0aFJvdXRlciwgUm91dGVDb21wb25lbnRQcm9wcyB9IGZyb20gXCJyZWFjdC1yb3V0ZXJcIjtcbmltcG9ydCB7IExhbmd1YWdlLCBQQVRILCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5cblxuaW50ZXJmYWNlIElCdXR0b25CYWNrIHtcbiAgaGFuZGVsPzogRnVuY3Rpb24gfCBzdHJpbmc7XG4gIGJhY2tUZXh0Pzogc3RyaW5nO1xuXG4gIFtwcm9wTmFtZTogc3RyaW5nXTogYW55O1xufVxuXG50eXBlIElCdXR0b25CYWNrUmVhbCA9IElCdXR0b25CYWNrICYgUm91dGVDb21wb25lbnRQcm9wc1xuXG5cbmNvbnN0IEJ1dHRvbkJhY2tSZWFsID0gKHByb3BzOiBJQnV0dG9uQmFja1JlYWwpID0+IHtcbiAgY29uc3QgeyBvbkNsaWNrLCAuLi5yZXN0IH0gPSBwcm9wcztcbiAgY29uc3QgYmFja1RleHQgPSBwcm9wcy5iYWNrVGV4dCA/IHByb3BzLmJhY2tUZXh0IDogTGFuZ3VhZ2UuZW4oXCJCYWNrXCIpLnRoYWkoXCLguIHguKXguLHguJpcIikubXkoXCLhgJXhgLzhgJThgLouXCIpLmdldE1lc3NhZ2UoKTtcbiAgaWYgKCFwcm9wcy5oYW5kZWwgJiYgVXRpbHMuZ2V0SXNJbkFwcCgpKSByZXR1cm4gPGRpdi8+O1xuICByZXR1cm4gPEJ1dHRvbiBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICAgICB7Li4ucmVzdH1cbiAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgIGlmICghcHJvcHMuaGFuZGVsKSB7XG4gICAgICAgICAgICAgICAgICAgICBpZiAocHJvcHMuaGlzdG9yeS5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgLy9UT0RPIOaaguaXtuino+WGs3dpbmRvdyBvcGVuIOeahOmhtemdomJhY2vpu5jorqTot7PovazliLBwb2xpY3kgcXVlcnlcbiAgICAgICAgICAgICAgICAgICAgICAgcHJvcHMuaGlzdG9yeS5wdXNoKFBBVEguUE9MSUNJRVNfUVVFUlkpO1xuICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgcHJvcHMuaGlzdG9yeS5nb0JhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKF8uaXNTdHJpbmcocHJvcHMuaGFuZGVsKSkge1xuICAgICAgICAgICAgICAgICAgICAgcHJvcHMuaGlzdG9yeS5wdXNoKHByb3BzLmhhbmRlbCk7XG4gICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgIHByb3BzLmhhbmRlbChwcm9wcy5oaXN0b3J5KTtcbiAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgIH19XG4gID5cbiAgICB7YmFja1RleHR9XG4gIDwvQnV0dG9uPjtcbn07XG5cblxuLy8g6L+U5Zue5b+F6aG755So6L+Z5Liq57uE5Lu2XG5leHBvcnQgY29uc3QgQnV0dG9uQmFjayA9IHdpdGhSb3V0ZXIoQnV0dG9uQmFja1JlYWwpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVlBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFDQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/antd/button-back.tsx
