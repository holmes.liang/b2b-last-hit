__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FormItemMaritalStatus; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/useful-form-item/marital-status.tsx";





var generatePropsName = function generatePropsName(propName, dataId) {
  if (!!dataId) return "".concat(dataId, "-").concat(propName);
  return propName;
};

var FormItemMaritalStatus =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(FormItemMaritalStatus, _ModelWidget);

  function FormItemMaritalStatus() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, FormItemMaritalStatus);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(FormItemMaritalStatus).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(FormItemMaritalStatus, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          dataId = _this$props.dataId,
          form = _this$props.form,
          model = _this$props.model,
          _this$props$required = _this$props.required,
          required = _this$props$required === void 0 ? false : _this$props$required;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NSelect"], {
        form: form,
        model: model,
        propName: generatePropsName("maritalStatus", dataId),
        tableName: "maritalstatus",
        required: required,
        size: "large",
        style: {
          width: "100%"
        },
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Marital Status").thai("สถานภาพการสมรส").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      });
    }
  }]);

  return FormItemMaritalStatus;
}(_component__WEBPACK_IMPORTED_MODULE_6__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vbWFyaXRhbC1zdGF0dXMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vbWFyaXRhbC1zdGF0dXMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTlNlbGVjdCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuXG5cbmNvbnN0IGdlbmVyYXRlUHJvcHNOYW1lID0gKHByb3BOYW1lOiBzdHJpbmcsIGRhdGFJZD86IHN0cmluZyk6IHN0cmluZyA9PiB7XG4gIGlmICghIWRhdGFJZCkgcmV0dXJuIGAke2RhdGFJZH0tJHtwcm9wTmFtZX1gO1xuICByZXR1cm4gcHJvcE5hbWU7XG59O1xuXG50eXBlIElQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgZm9ybTogYW55O1xuICBkYXRhSWQ/OiBzdHJpbmc7XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbn0gJiBNb2RlbFdpZGdldFByb3BzICYgRm9ybUNvbXBvbmVudFByb3BzO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBGb3JtSXRlbU1hcml0YWxTdGF0dXM8UCBleHRlbmRzIElQcm9wcywgUywgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGRhdGFJZCwgZm9ybSwgbW9kZWwsIHJlcXVpcmVkID0gZmFsc2UgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxOU2VsZWN0XG4gICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcHNOYW1lKFwibWFyaXRhbFN0YXR1c1wiLCBkYXRhSWQpfVxuICAgICAgICB0YWJsZU5hbWU9XCJtYXJpdGFsc3RhdHVzXCJcbiAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmVkfVxuICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiB9fVxuICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJNYXJpdGFsIFN0YXR1c1wiKVxuICAgICAgICAgIC50aGFpKFwi4Liq4LiW4Liy4LiZ4Lig4Liy4Lie4LiB4Liy4Lij4Liq4Lih4Lij4LiqXCIpXG4gICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgIC8+XG4gICAgKTtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUUE7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBOzs7O0FBckJBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/useful-form-item/marital-status.tsx
