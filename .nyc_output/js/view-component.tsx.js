__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _component_create_dialog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _paid_until_table__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../../paid-until-table */ "./src/app/desk/quote/paid-until-table.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var _desk_component_view_item_index__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk-component/view-item/index */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_component_NPanelShowOutputs__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk-component/NPanelShowOutputs */ "./src/app/desk/component/NPanelShowOutputs.tsx");
/* harmony import */ var _desk_component_history_timeline__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @desk-component/history-timeline */ "./src/app/desk/component/history-timeline/index.tsx");
/* harmony import */ var _component_location_table_view__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./component/location-table-view */ "./src/app/desk/quote/SAIC/iar/component/location-table-view.tsx");
/* harmony import */ var _desk_component_NPanelShowAttachment__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @desk-component/NPanelShowAttachment */ "./src/app/desk/component/NPanelShowAttachment.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_viewCurrency__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/viewCurrency */ "./src/app/desk/quote/SAIC/iar/component/viewCurrency.tsx");
/* harmony import */ var _desk_component_npanel_show__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @desk-component/npanel-show */ "./src/app/desk/component/npanel-show/index.tsx");
/* harmony import */ var _desk_component_ph_company_info_view__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @desk-component/ph/company-info-view */ "./src/app/desk/component/ph/company-info-view.tsx");
/* harmony import */ var _desk_quote_components_panyment_arramgement_view_payment_plan__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @desk/quote/components/panyment-arramgement/view-payment-plan */ "./src/app/desk/quote/components/panyment-arramgement/view-payment-plan.tsx");











var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/view-component.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




















var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_19__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var PAYMENT_METHOD_RECURRING = _common__WEBPACK_IMPORTED_MODULE_15__["Consts"].PAYMENT_METHOD_RECURRING,
    YES_OR_N0 = _common__WEBPACK_IMPORTED_MODULE_15__["Consts"].YES_OR_N0;

var toDate = function toDate(date) {
  return date.split("T")[0];
};

var defaultLayout = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItem = function ViewItem(props) {
  return Object(_desk_component_view_item_index__WEBPACK_IMPORTED_MODULE_20__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, props, {
    layout: defaultLayout
  }));
};

var ViewCurrencyItemStyle = function ViewCurrencyItemStyle(props) {
  return Object(_desk_quote_SAIC_iar_component_viewCurrency__WEBPACK_IMPORTED_MODULE_25__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, props, {
    layout: defaultLayout
  }));
};

var ViewPaymentPlanStyle = function ViewPaymentPlanStyle(props) {
  return Object(_desk_quote_components_panyment_arramgement_view_payment_plan__WEBPACK_IMPORTED_MODULE_28__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, props, {
    layout: defaultLayout
  }));
};

var ViewComponent =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(ViewComponent, _ModelWidget);

  function ViewComponent(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, ViewComponent);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(ViewComponent).call(this, props, context));
    _this.policy = void 0;
    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee() {
      var codeTables, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              arr = [];
              ["nationcode", "insurancefund", "gstrate", "currency"].forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _this.getHelpers().getAjax().get("/mastertable", {
                    itntCode: _this.getValueFromModel(_this.getProps("itntCode")),
                    productCode: _this.getValueFromModel(_this.getProps("productCode")),
                    productVersion: _this.getValueFromModel(_this.getProps("productVersion")),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = respData.items || [];
                    resolve();
                  }).catch(function (error) {});
                }));
              });
              Promise.all(arr).then(function (values) {
                codeTables["yesOrNo"] = YES_OR_N0;

                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.getProductAndPlan = function () {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 188
        },
        __self: this
      }, "".concat(_this.getValueFromModel(_this.getProps("productName"))).concat(_this.getValueFromModel(_this.getProps("planTypeName")) ? ", " + _this.getValueFromModel(_this.getProps("planTypeName")) : "").concat(_this.getValueFromModel(_this.getProps("planName")) ? ", " + _this.getValueFromModel(_this.getProps("planName")) : ""));
    };

    _this.getMasterTableItemText = function (tableName, itemId) {
      var codeTables = _this.state.codeTables;

      if (itemId) {
        var item = (codeTables[tableName] || []).find(function (option) {
          return itemId.toString() === option.id.toString();
        }) || {};
        return item.text || "";
      } else {
        return "";
      }
    };

    _this.getPeriodOfInsurance = function () {
      var endoFixed = _this.props.endoFixed;

      var effDate = _this.getValueFromModel(_this.getProps(endoFixed ? "".concat(endoFixed, ".effDate") : "effDate"));

      var expDate = _this.getValueFromModel(_this.getProps(endoFixed ? "".concat(endoFixed, ".expDate") : "expDate"));

      if (!effDate) {
        return "";
      }

      if (_this.getValueFromModel(_this.getProps("openEnd"))) {
        expDate = "~ Infinite";
      } else {
        expDate = expDate ? "~ ".concat(toDate(expDate)) : null;
      }

      return [toDate(effDate), expDate].join(" ");
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(ViewComponent, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        ViewComponent: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "getProps",
    value: function getProps(propName) {
      var dataFixed = this.props.dataFixed;

      if (dataFixed) {
        return "".concat(dataFixed, ".").concat(propName);
      } else {
        return propName;
      }
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(ViewComponent.prototype), "initState", this).call(this), {
        codeTables: {},
        coverageDetails: {},
        pdfList: [],
        recordList: [],
        attaches: [],
        currentItem: null,
        lightbox: {
          index: -1,
          visible: false
        },
        crossSellProducts: [],
        checkedPolicies: new Map(),
        issuedPolicies: new Map(),
        activeKey: [],
        defaultActiveKey: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
      });
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee2() {
        var initKey;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                try {
                  this.getAttaches();
                  this.loadCodeTables();
                  initKey = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
                  this.setState({
                    defaultActiveKey: initKey
                  }, function () {});
                } catch (error) {
                  console.error(error.message);
                }

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "getAttaches",
    value: function getAttaches() {
      var _this2 = this;

      this.getHelpers().getAjax().get("/policies/".concat(this.getValueFromModel(this.getProps("policyId")), "/attaches"), {}, {}).then(function (response) {
        var respData = response.body.respData;

        _this2.setState({
          attaches: respData
        });
      }).catch(function (error) {});
    }
  }, {
    key: "getPolicyPremium",
    value: function getPolicyPremium() {
      var paymentSchedule = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var premiumId = this.props.premiumId;
      var policy = {
        policyPremium: this.getValueFromModel(this.getProps(premiumId))
      };
      var lumpsum = _common__WEBPACK_IMPORTED_MODULE_15__["Utils"].getValueFromJSON(policy, "".concat(premiumId, ".lumpsum")) || {};
      var subsequent = _common__WEBPACK_IMPORTED_MODULE_15__["Utils"].getValueFromJSON(policy, "".concat(premiumId, ".subsequent")) || {};

      if (this.getValueFromModel(this.getProps("openEnd")) || this.getValueFromModel(this.getProps("recurringPayment")) || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    }
  }, {
    key: "basicInfoRender",
    value: function basicInfoRender() {
      var _this3 = this;

      var _this$props = this.props,
          endoFixed = _this$props.endoFixed,
          isEndo = _this$props.isEndo;
      var paidUntil = this.getValueFromModel(this.getProps("paidUntil"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: endoFixed ? _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Endorsement No.").thai("Endorsement No.").getMessage() : _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Policy No.").thai("หมายเลขอ้างอิง").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "headerMiddle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }, endoFixed ? this.getValueFromModel(this.getProps("endoNo")) : this.getValueFromModel(this.getProps("policyNo")) || this.getValueFromModel(this.getProps("quoteNo")), !endoFixed && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("span", {
        className: "boardText",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }, this.getValueFromModel("bizType")), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "headerStatus",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }, this.getValueFromModel("statusName"))))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Product").thai("Product").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 258
        },
        __self: this
      }, this.getProductAndPlan()), isEndo && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Endorsement Effective Date").thai("Endorsement Effective Date").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_15__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_15__["DateUtils"].toDate(this.getValueFromModel("endoEffDate")))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Period of Insurance").thai("Period of Insurance").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 274
        },
        __self: this
      }, this.getPeriodOfInsurance(), paidUntil && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("label", {
        style: {
          fontSize: "13px",
          color: "#9e9e9e",
          padding: "0 5px 0 30px",
          lineHeight: "26px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }, "Paid until"), !paidUntil ? "" : _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("span", {
        style: {
          color: COLOR_PRIMARY,
          cursor: "pointer"
        },
        onClick: function onClick() {
          _component_create_dialog__WEBPACK_IMPORTED_MODULE_17__["default"].create({
            Component: function Component(_ref2) {
              var onCancel = _ref2.onCancel,
                  onOk = _ref2.onOk;
              return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_paid_until_table__WEBPACK_IMPORTED_MODULE_18__["default"], {
                onCancel: onCancel,
                policyId: _this3.getValueFromModel(_this3.getProps("policyId")),
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 300
                },
                __self: this
              });
            }
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_15__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_15__["DateUtils"].toDate(paidUntil)))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Insurance Fund").thai("Insurance Fund").my("Insurance Fund").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311
        },
        __self: this
      }, this.getMasterTableItemText("insurancefund", this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".insuranceFund") : "ext.insuranceFund")))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("GST Rate").thai("GST Rate").my("GST Rate").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 319
        },
        __self: this
      }, this.getMasterTableItemText("gstrate", this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".gstRate") : "ext.gstRate")))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewCurrencyItemStyle, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("SI Currency").thai("SI Currency").getMessage(),
        value: this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".siCurrency") : "siCurrencyCode")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 327
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewCurrencyItemStyle, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Premium Currency").thai("Premium Currency").getMessage(),
        value: this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".premiumCurrency") : "premCurrencyCode")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 333
        },
        __self: this
      }));
    }
  }, {
    key: "renderThousandNum",
    value: function renderThousandNum(value) {
      var newValue = (parseFloat(value) || 0).toFixed(2);
      return _common__WEBPACK_IMPORTED_MODULE_15__["Utils"].toThousands(newValue);
    }
  }, {
    key: "renderSummary",
    value: function renderSummary() {
      var _this$props2 = this.props,
          endoFixed = _this$props2.endoFixed,
          isEndo = _this$props2.isEndo;
      var summary = this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".feeSummary") : "ext.feeSummary"));
      var currency = this.getValueFromModel(this.getProps("currencySymbol"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: isEndo ? _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Endorsement Premium").thai("Endorsement Premium").getMessage() : _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("PREMIUM").thai("PREMIUM").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 354
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "premium", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("GST").thai("GST").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 366
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "gst", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: isEndo ? _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Total").thai("Total").getMessage() : _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Total Premium").thai("Total Premium").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 371
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "totalPremium", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("COMMISSION Rate(%)").thai("COMMISSION Rate(%)").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 378
        },
        __self: this
      }, (parseFloat(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "commissionRate", "") || 0) * 100).toFixed(2)), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("COMMISSION Amount").thai("COMMISSION Amount").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 383
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "commissionAmount", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("COMMISSION GST").thai("COMMISSION GST").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 388
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "commissionGst", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Total Commission").thai("Total Commission").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 393
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "totalCommission", ""))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Net Premium").thai("Net Premium").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 398
        },
        __self: this
      }, currency, " ", this.renderThousandNum(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(summary, "netPremium", ""))));
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var C = this.getComponents();
      var _this$props3 = this.props,
          model = _this$props3.model,
          dataFixed = _this$props3.dataFixed,
          form = _this$props3.form,
          endoFixed = _this$props3.endoFixed;
      var defaultActiveKey = this.state.defaultActiveKey;
      var isInstallmentEnabled = lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(this.props.model, this.getProps("isInstallmentEnabled")) || false;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.ViewComponent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 414
        },
        __self: this
      }, this.getMasterTableItemText("designation", dataFixed ? "".concat(dataFixed, "-ext-beneficiary-0-designation") : "ext-beneficiary-0-designation"), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NCollapse"], {
        defaultActiveKey: defaultActiveKey,
        activeKey: defaultActiveKey,
        onChange: function onChange(key) {
          _this4.setState({
            defaultActiveKey: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(key)
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 416
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Basic Information").thai("ข้อมูลพื้นฐาน").my("အခြေခံအချက်အလက်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 423
        },
        __self: this
      }, this.basicInfoRender()), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "2",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Company Information").thai("Company Information").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 432
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_ph_company_info_view__WEBPACK_IMPORTED_MODULE_27__["default"], {
        model: model,
        layoutCol: {
          labelCol: {
            xs: {
              span: 6
            },
            sm: {
              span: 8
            }
          },
          wrapperCol: {
            xs: {
              span: 18
            },
            sm: {
              span: 16
            }
          }
        },
        isMailingAddress: true,
        propNameFixed: endoFixed ? "ext.changes.policyholder" : this.getProps("ext.policyholder"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 438
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "6",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("LOCATIONS").thai("LOCATIONS").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 452
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component_location_table_view__WEBPACK_IMPORTED_MODULE_23__["default"], {
        endoFixed: endoFixed,
        model: dataFixed ? lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(this.props.model, dataFixed, {}) : this.props.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 458
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "3",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("FEES SUMMARY").thai("FEES SUMMARY").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 461
        },
        __self: this
      }, this.renderSummary()), !endoFixed && isInstallmentEnabled && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "10",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("PAYMENT ARRANGEMENT").thai("PAYMENT ARRANGEMENT").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 471
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(ViewPaymentPlanStyle, {
        title: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Payment Plan").thai("Payment Plan").getMessage(),
        model: this.props.model,
        dataFixed: dataFixed,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 475
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "4",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("CLAUSES").thai("CLAUSES").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 484
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        style: {
          border: 0
        },
        className: "ql-container ql-snow",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 495
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "ql-editor",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        dangerouslySetInnerHTML: {
          __html: this.getValueFromModel(this.getProps(endoFixed ? "".concat(endoFixed, ".specialClauses") : "ext.specialClauses"))
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 497
        },
        __self: this
      })))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_NPanelShowOutputs__WEBPACK_IMPORTED_MODULE_21__["default"], {
        isEndo: !!endoFixed,
        dataFixed: dataFixed,
        key: "5",
        that: this,
        isPolicyDoc: true,
        afterUploadPolicyDoc: this.props.afterUploadPolicyDoc,
        model: this.props.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 502
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_16__["NPanel"], {
        key: "9",
        header: _common__WEBPACK_IMPORTED_MODULE_15__["Language"].en("Recent Communication History").thai("ประวัติการสื่อสารล่าสุด").my("RECENT ဆက်သွယ်မှုမှတ်တမ်း").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 511
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_history_timeline__WEBPACK_IMPORTED_MODULE_22__["default"], {
        dataFixed: dataFixed,
        model: this.props.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 518
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_NPanelShowAttachment__WEBPACK_IMPORTED_MODULE_24__["default"], {
        dataFixed: dataFixed,
        key: "7",
        that: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 520
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_npanel_show__WEBPACK_IMPORTED_MODULE_26__["QuickLinks"], {
        model: this.props.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 521
        },
        __self: this
      })));
    }
  }]);

  return ViewComponent;
}(_component__WEBPACK_IMPORTED_MODULE_13__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ViewComponent);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvdmlldy1jb21wb25lbnQudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvdmlldy1jb21wb25lbnQudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBDb25zdHMsIERhdGVVdGlscywgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTlBhbmVsIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5cbmltcG9ydCBNYXNrIGZyb20gXCIuLi8uLi8uLi9jb21wb25lbnQvY3JlYXRlLWRpYWxvZ1wiO1xuaW1wb3J0IFBhaWRVbnRpbFRhYmxlIGZyb20gXCIuLi8uLi9wYWlkLXVudGlsLXRhYmxlXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXMvaW5kZXhcIjtcbmltcG9ydCBWaWV3SXRlbTEgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC92aWV3LWl0ZW0vaW5kZXhcIjtcbmltcG9ydCBOUGFuZWxTaG93T3V0cHV0cyBmcm9tIFwiQGRlc2stY29tcG9uZW50L05QYW5lbFNob3dPdXRwdXRzXCI7XG5pbXBvcnQgSGlzdG9yeVRpbWVsaW5lIGZyb20gXCJAZGVzay1jb21wb25lbnQvaGlzdG9yeS10aW1lbGluZVwiO1xuaW1wb3J0IExvY2F0aW9uVGFibGVWaWV3IGZyb20gXCIuL2NvbXBvbmVudC9sb2NhdGlvbi10YWJsZS12aWV3XCI7XG5pbXBvcnQgTlBhbmVsU2hvd0F0dGFjaG1lbnQgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9OUGFuZWxTaG93QXR0YWNobWVudFwiO1xuaW1wb3J0IFZpZXdDdXJyZW5jeUl0ZW0gZnJvbSBcIkBkZXNrL3F1b3RlL1NBSUMvaWFyL2NvbXBvbmVudC92aWV3Q3VycmVuY3lcIjtcbmltcG9ydCB7IFF1aWNrTGlua3MgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L25wYW5lbC1zaG93XCI7XG5pbXBvcnQgQ29tcGFueUluZm9WaWV3IGZyb20gXCJAZGVzay1jb21wb25lbnQvcGgvY29tcGFueS1pbmZvLXZpZXdcIjtcbmltcG9ydCBWaWV3UGF5bWVudFBsYW4gZnJvbSBcIkBkZXNrL3F1b3RlL2NvbXBvbmVudHMvcGFueW1lbnQtYXJyYW1nZW1lbnQvdmlldy1wYXltZW50LXBsYW5cIjtcblxuY29uc3QgeyBDT0xPUl9QUklNQVJZIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuXG5jb25zdCB7IFBBWU1FTlRfTUVUSE9EX1JFQ1VSUklORywgWUVTX09SX04wIH0gPSBDb25zdHM7XG5jb25zdCB0b0RhdGUgPSAoZGF0ZTogc3RyaW5nKSA9PiBkYXRlLnNwbGl0KFwiVFwiKVswXTtcbmNvbnN0IGRlZmF1bHRMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogNiB9LFxuICAgIHNtOiB7IHNwYW46IDggfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG5jb25zdCBWaWV3SXRlbSA9IChwcm9wczogYW55KSA9PiBWaWV3SXRlbTEoeyAuLi5wcm9wcywgbGF5b3V0OiBkZWZhdWx0TGF5b3V0IH0pO1xuY29uc3QgVmlld0N1cnJlbmN5SXRlbVN0eWxlID0gKHByb3BzOiBhbnkpID0+IFZpZXdDdXJyZW5jeUl0ZW0oeyAuLi5wcm9wcywgbGF5b3V0OiBkZWZhdWx0TGF5b3V0IH0pO1xuY29uc3QgVmlld1BheW1lbnRQbGFuU3R5bGUgPSAocHJvcHM6IGFueSkgPT4gVmlld1BheW1lbnRQbGFuKHsgLi4ucHJvcHMsIGxheW91dDogZGVmYXVsdExheW91dCB9KTtcbmV4cG9ydCB0eXBlIFZpZXdDb21wb25lbnRQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgb3V0cHV0c1Nob3c/OiBib29sZWFuO1xuICBpc0VkaXQ/OiBib29sZWFuO1xuICBpc0NvbmZpcm0/OiBib29sZWFuO1xuICBkYXRhRml4ZWQ/OiBhbnk7XG4gIHByZW1pdW1JZDogYW55O1xuICBlbmRvRml4ZWQ/OiBhbnk7XG4gIGlzRW5kbz86IGJvb2xlYW47XG4gIGFmdGVyVXBsb2FkUG9saWN5RG9jPzogYW55O1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFZpZXdDb21wb25lbnRTdGF0ZSA9IHtcbiAgY29kZVRhYmxlczogYW55O1xuICBjb3ZlcmFnZURldGFpbHM6IGFueTtcbiAgcGRmTGlzdDogYW55W107XG4gIHJlY29yZExpc3Q6IGFueVtdO1xuICBhdHRhY2hlczogYW55W107XG4gIGN1cnJlbnRJdGVtOiBhbnk7XG4gIGxpZ2h0Ym94OiBhbnk7XG4gIGNyb3NzU2VsbFByb2R1Y3RzOiBhbnk7XG4gIGNoZWNrZWRQb2xpY2llczogYW55O1xuICBpc3N1ZWRQb2xpY2llczogYW55O1xuICBhY3RpdmVLZXk6IGFueTtcbiAgZGVmYXVsdEFjdGl2ZUtleTogYW55O1xufTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBWaWV3Q29tcG9uZW50Q29tcG9uZW50cyA9IHtcbiAgVmlld0NvbXBvbmVudDogU3R5bGVkRElWO1xufTtcblxuY2xhc3MgVmlld0NvbXBvbmVudDxQIGV4dGVuZHMgVmlld0NvbXBvbmVudFByb3BzLFxuICBTIGV4dGVuZHMgVmlld0NvbXBvbmVudFN0YXRlLFxuICBDIGV4dGVuZHMgVmlld0NvbXBvbmVudENvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgcG9saWN5OiBhbnk7XG5cblxuICBjb25zdHJ1Y3Rvcihwcm9wczogVmlld0NvbXBvbmVudFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBWaWV3Q29tcG9uZW50OiBTdHlsZWQuZGl2YFxuICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgZ2V0UHJvcHMocHJvcE5hbWU6IGFueSkge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChkYXRhRml4ZWQpIHtcbiAgICAgIHJldHVybiBgJHtkYXRhRml4ZWR9LiR7cHJvcE5hbWV9YDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHByb3BOYW1lO1xuICAgIH1cblxuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgY29kZVRhYmxlczoge30sXG4gICAgICBjb3ZlcmFnZURldGFpbHM6IHt9LFxuICAgICAgcGRmTGlzdDogW10sXG4gICAgICByZWNvcmRMaXN0OiBbXSxcbiAgICAgIGF0dGFjaGVzOiBbXSxcbiAgICAgIGN1cnJlbnRJdGVtOiBudWxsLFxuICAgICAgbGlnaHRib3g6IHsgaW5kZXg6IC0xLCB2aXNpYmxlOiBmYWxzZSB9LFxuICAgICAgY3Jvc3NTZWxsUHJvZHVjdHM6IFtdLFxuICAgICAgY2hlY2tlZFBvbGljaWVzOiBuZXcgTWFwKCksXG4gICAgICBpc3N1ZWRQb2xpY2llczogbmV3IE1hcCgpLFxuICAgICAgYWN0aXZlS2V5OiBbXSxcbiAgICAgIGRlZmF1bHRBY3RpdmVLZXk6IFtcIjFcIiwgXCIyXCIsIFwiM1wiLCBcIjRcIiwgXCI1XCIsIFwiNlwiLCBcIjdcIiwgXCI4XCIsIFwiOVwiLCBcIjEwXCJdLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0cnkge1xuICAgICAgdGhpcy5nZXRBdHRhY2hlcygpO1xuICAgICAgdGhpcy5sb2FkQ29kZVRhYmxlcygpO1xuICAgICAgbGV0IGluaXRLZXkgPSBbXCIxXCIsIFwiMlwiLCBcIjNcIiwgXCI0XCIsIFwiNVwiLCBcIjZcIiwgXCI3XCIsIFwiOFwiLCBcIjlcIiwgXCIxMFwiXTtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBkZWZhdWx0QWN0aXZlS2V5OiBpbml0S2V5LFxuICAgICAgfSwgKCkgPT4ge1xuICAgICAgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IubWVzc2FnZSk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBnZXRBdHRhY2hlcygpIHtcbiAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgLmdldEFqYXgoKVxuICAgICAgLmdldChgL3BvbGljaWVzLyR7dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicG9saWN5SWRcIikpfS9hdHRhY2hlc2AsIHt9LCB7fSlcbiAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGF0dGFjaGVzOiByZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBsb2FkQ29kZVRhYmxlcyA9IGFzeW5jICgpID0+IHtcbiAgICBsZXQgeyBjb2RlVGFibGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGxldCBhcnI6IGFueSA9IFtdO1xuICAgIFtcbiAgICAgIFwibmF0aW9uY29kZVwiLFxuICAgICAgXCJpbnN1cmFuY2VmdW5kXCIsXG4gICAgICBcImdzdHJhdGVcIixcbiAgICAgIFwiY3VycmVuY3lcIixcbiAgICBdLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICBhcnIucHVzaChcbiAgICAgICAgbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgICAuZ2V0KFwiL21hc3RlcnRhYmxlXCIsIHtcbiAgICAgICAgICAgICAgICBpdG50Q29kZTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwiaXRudENvZGVcIikpLFxuICAgICAgICAgICAgICAgIHByb2R1Y3RDb2RlOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJwcm9kdWN0Q29kZVwiKSksXG4gICAgICAgICAgICAgICAgcHJvZHVjdFZlcnNpb246IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhcInByb2R1Y3RWZXJzaW9uXCIpKSxcbiAgICAgICAgICAgICAgICB0YWJsZU5hbWU6IGl0ZW0sXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHt9LFxuICAgICAgICAgICAgKVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgICAgICAgY29kZVRhYmxlc1tpdGVtXSA9IHJlc3BEYXRhLml0ZW1zIHx8IFtdO1xuICAgICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KSxcbiAgICAgICk7XG4gICAgfSk7XG4gICAgUHJvbWlzZS5hbGwoYXJyKS50aGVuKFxuICAgICAgdmFsdWVzID0+IHtcbiAgICAgICAgY29kZVRhYmxlc1tcInllc09yTm9cIl0gPSBZRVNfT1JfTjA7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjb2RlVGFibGVzOiBjb2RlVGFibGVzIH0pO1xuICAgICAgfSxcbiAgICAgIHJlYXNvbiA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKHJlYXNvbik7XG4gICAgICB9LFxuICAgICk7XG4gIH07XG5cbiAgcHJpdmF0ZSBnZXRQcm9kdWN0QW5kUGxhbiA9ICgpID0+IHtcbiAgICByZXR1cm4gKFxuICAgICAgPHNwYW4+XG4gICAgICAgIHtgJHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJwcm9kdWN0TmFtZVwiKSl9JHtcbiAgICAgICAgICB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJwbGFuVHlwZU5hbWVcIikpID8gXCIsIFwiICsgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicGxhblR5cGVOYW1lXCIpKSA6IFwiXCJcbiAgICAgICAgfSR7dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicGxhbk5hbWVcIikpID8gXCIsIFwiICsgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicGxhbk5hbWVcIikpIDogXCJcIn1gfVxuICAgICAgPC9zcGFuPlxuICAgICk7XG4gIH07XG4gIHByaXZhdGUgZ2V0TWFzdGVyVGFibGVJdGVtVGV4dCA9ICh0YWJsZU5hbWU6IHN0cmluZywgaXRlbUlkOiBzdHJpbmcpID0+IHtcbiAgICBjb25zdCB7IGNvZGVUYWJsZXMgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKGl0ZW1JZCkge1xuICAgICAgY29uc3QgaXRlbSA9XG4gICAgICAgIChjb2RlVGFibGVzW3RhYmxlTmFtZV0gfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1JZC50b1N0cmluZygpID09PSBvcHRpb24uaWQudG9TdHJpbmcoKTtcbiAgICAgICAgfSkgfHwge307XG4gICAgICByZXR1cm4gaXRlbS50ZXh0IHx8IFwiXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgfTtcblxuICBwcml2YXRlIGdldFBvbGljeVByZW1pdW0ocGF5bWVudFNjaGVkdWxlID0gbnVsbCkge1xuICAgIGNvbnN0IHsgcHJlbWl1bUlkIH0gPSB0aGlzLnByb3BzO1xuICAgIGxldCBwb2xpY3kgPSB7IHBvbGljeVByZW1pdW06IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhwcmVtaXVtSWQpKSB9O1xuICAgIGxldCBsdW1wc3VtID0gVXRpbHMuZ2V0VmFsdWVGcm9tSlNPTihwb2xpY3ksIGAke3ByZW1pdW1JZH0ubHVtcHN1bWApIHx8IHt9O1xuICAgIGxldCBzdWJzZXF1ZW50ID0gVXRpbHMuZ2V0VmFsdWVGcm9tSlNPTihwb2xpY3ksIGAke3ByZW1pdW1JZH0uc3Vic2VxdWVudGApIHx8IHt9O1xuICAgIGlmIChcbiAgICAgIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhcIm9wZW5FbmRcIikpIHx8XG4gICAgICB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJyZWN1cnJpbmdQYXltZW50XCIpKSB8fFxuICAgICAgcGF5bWVudFNjaGVkdWxlID09PSBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkdcbiAgICApIHtcbiAgICAgIHJldHVybiBzdWJzZXF1ZW50O1xuICAgIH1cbiAgICByZXR1cm4gbHVtcHN1bTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0UGVyaW9kT2ZJbnN1cmFuY2UgPSAoKSA9PiB7XG4gICAgY29uc3QgeyBlbmRvRml4ZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGVmZkRhdGUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoZW5kb0ZpeGVkID8gYCR7ZW5kb0ZpeGVkfS5lZmZEYXRlYCA6IFwiZWZmRGF0ZVwiKSk7XG4gICAgbGV0IGV4cERhdGUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoZW5kb0ZpeGVkID8gYCR7ZW5kb0ZpeGVkfS5leHBEYXRlYCA6IFwiZXhwRGF0ZVwiKSk7XG4gICAgaWYgKCFlZmZEYXRlKSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gICAgaWYgKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhcIm9wZW5FbmRcIikpKSB7XG4gICAgICBleHBEYXRlID0gXCJ+IEluZmluaXRlXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGV4cERhdGUgPSBleHBEYXRlID8gYH4gJHt0b0RhdGUoZXhwRGF0ZSl9YCA6IG51bGw7XG4gICAgfVxuICAgIHJldHVybiBbdG9EYXRlKGVmZkRhdGUpLCBleHBEYXRlXS5qb2luKFwiIFwiKTtcbiAgfTtcblxuICBwcml2YXRlIGJhc2ljSW5mb1JlbmRlcigpIHtcbiAgICBjb25zdCB7IGVuZG9GaXhlZCwgaXNFbmRvIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHBhaWRVbnRpbCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhcInBhaWRVbnRpbFwiKSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgIHRpdGxlPXtlbmRvRml4ZWQgPyBMYW5ndWFnZS5lbihcIkVuZG9yc2VtZW50IE5vLlwiKVxuICAgICAgICAgICAgLnRoYWkoXCJFbmRvcnNlbWVudCBOby5cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCkgOiBMYW5ndWFnZS5lbihcIlBvbGljeSBOby5cIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiC4Lit4LmJ4Liy4LiH4Lit4Li04LiHXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJNaWRkbGVcIj5cbiAgICAgICAgICAgIHtlbmRvRml4ZWQgPyB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJlbmRvTm9cIikpIDogKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhcInBvbGljeU5vXCIpKSB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJxdW90ZU5vXCIpKSl9XG4gICAgICAgICAgICB7IWVuZG9GaXhlZCAmJiA8PlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJib2FyZFRleHRcIj57dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcImJpelR5cGVcIil9PC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlclN0YXR1c1wiPnt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwic3RhdHVzTmFtZVwiKX08L2Rpdj5cbiAgICAgICAgICAgIDwvPn1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiUHJvZHVjdFwiKVxuICAgICAgICAgICAgLnRoYWkoXCJQcm9kdWN0XCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA+XG4gICAgICAgICAge3RoaXMuZ2V0UHJvZHVjdEFuZFBsYW4oKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAge2lzRW5kbyAmJiA8Vmlld0l0ZW1cbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJFbmRvcnNlbWVudCBFZmZlY3RpdmUgRGF0ZVwiKVxuICAgICAgICAgICAgLnRoYWkoXCJFbmRvcnNlbWVudCBFZmZlY3RpdmUgRGF0ZVwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgICAge1xuICAgICAgICAgICAgRGF0ZVV0aWxzLmZvcm1hdERhdGUoRGF0ZVV0aWxzLnRvRGF0ZSh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiZW5kb0VmZkRhdGVcIikpKVxuICAgICAgICAgIH1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgfVxuICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJQZXJpb2Qgb2YgSW5zdXJhbmNlXCIpXG4gICAgICAgICAgICAudGhhaShcIlBlcmlvZCBvZiBJbnN1cmFuY2VcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5nZXRQZXJpb2RPZkluc3VyYW5jZSgpfVxuICAgICAgICAgIHtwYWlkVW50aWwgJiYgKFxuICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTNweFwiLFxuICAgICAgICAgICAgICAgIGNvbG9yOiBcIiM5ZTllOWVcIixcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiBcIjAgNXB4IDAgMzBweFwiLFxuICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IFwiMjZweFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBQYWlkIHVudGlsXG4gICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICl9XG4gICAgICAgICAgeyFwYWlkVW50aWwgPyAoXG4gICAgICAgICAgICBcIlwiXG4gICAgICAgICAgKSA6IChcbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBDT0xPUl9QUklNQVJZLCBjdXJzb3I6IFwicG9pbnRlclwiIH19XG4gICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICA8UGFpZFVudGlsVGFibGUgb25DYW5jZWw9e29uQ2FuY2VsfSBwb2xpY3lJZD17dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKFwicG9saWN5SWRcIikpfS8+XG4gICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgRGF0ZVV0aWxzLmZvcm1hdERhdGUoRGF0ZVV0aWxzLnRvRGF0ZShwYWlkVW50aWwpKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiSW5zdXJhbmNlIEZ1bmRcIilcbiAgICAgICAgICAgIC50aGFpKFwiSW5zdXJhbmNlIEZ1bmRcIilcbiAgICAgICAgICAgIC5teShcIkluc3VyYW5jZSBGdW5kXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA+XG4gICAgICAgICAge3RoaXMuZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImluc3VyYW5jZWZ1bmRcIiwgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKGVuZG9GaXhlZCA/IGAke2VuZG9GaXhlZH0uaW5zdXJhbmNlRnVuZGAgOiBcImV4dC5pbnN1cmFuY2VGdW5kXCIpKSl9XG4gICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkdTVCBSYXRlXCIpXG4gICAgICAgICAgICAudGhhaShcIkdTVCBSYXRlXCIpXG4gICAgICAgICAgICAubXkoXCJHU1QgUmF0ZVwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHt0aGlzLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJnc3RyYXRlXCIsIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhlbmRvRml4ZWQgPyBgJHtlbmRvRml4ZWR9LmdzdFJhdGVgIDogXCJleHQuZ3N0UmF0ZVwiKSkpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICA8Vmlld0N1cnJlbmN5SXRlbVN0eWxlXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiU0kgQ3VycmVuY3lcIilcbiAgICAgICAgICAgIC50aGFpKFwiU0kgQ3VycmVuY3lcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgdmFsdWU9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhlbmRvRml4ZWQgPyBgJHtlbmRvRml4ZWR9LnNpQ3VycmVuY3lgIDogXCJzaUN1cnJlbmN5Q29kZVwiKSl9XG4gICAgICAgIC8+XG4gICAgICAgIDxWaWV3Q3VycmVuY3lJdGVtU3R5bGVcbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJQcmVtaXVtIEN1cnJlbmN5XCIpXG4gICAgICAgICAgICAudGhhaShcIlByZW1pdW0gQ3VycmVuY3lcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgdmFsdWU9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhlbmRvRml4ZWQgPyBgJHtlbmRvRml4ZWR9LnByZW1pdW1DdXJyZW5jeWAgOiBcInByZW1DdXJyZW5jeUNvZGVcIikpfVxuICAgICAgICAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclRob3VzYW5kTnVtKHZhbHVlOiBhbnkpIHtcbiAgICBjb25zdCBuZXdWYWx1ZSA9IChwYXJzZUZsb2F0KHZhbHVlKSB8fCAwKS50b0ZpeGVkKDIpO1xuICAgIHJldHVybiBVdGlscy50b1Rob3VzYW5kcyhuZXdWYWx1ZSk7XG4gIH1cblxuICByZW5kZXJTdW1tYXJ5KCkge1xuICAgIGNvbnN0IHsgZW5kb0ZpeGVkLCBpc0VuZG8gfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3Qgc3VtbWFyeSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZXRQcm9wcyhlbmRvRml4ZWQgPyBgJHtlbmRvRml4ZWR9LmZlZVN1bW1hcnlgIDogXCJleHQuZmVlU3VtbWFyeVwiKSk7XG4gICAgY29uc3QgY3VycmVuY3kgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcHMoXCJjdXJyZW5jeVN5bWJvbFwiKSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDw+XG4gICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgIHRpdGxlPXtpc0VuZG8gPyBMYW5ndWFnZS5lbihcIkVuZG9yc2VtZW50IFByZW1pdW1cIikudGhhaShcIkVuZG9yc2VtZW50IFByZW1pdW1cIikuZ2V0TWVzc2FnZSgpXG4gICAgICAgICAgICA6XG4gICAgICAgICAgICBMYW5ndWFnZS5lbihcIlBSRU1JVU1cIikudGhhaShcIlBSRU1JVU1cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA+XG4gICAgICAgICAge2N1cnJlbmN5fSB7dGhpcy5yZW5kZXJUaG91c2FuZE51bShfLmdldChzdW1tYXJ5LCBcInByZW1pdW1cIiwgXCJcIikpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICB7Lyo8Vmlld0l0ZW0qL31cbiAgICAgICAgey8qICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJBREpVU1RFRCBQUkVNSVVNXCIpLnRoYWkoXCJBREpVU1RFRCBQUkVNSVVNXCIpLmdldE1lc3NhZ2UoKX0qL31cbiAgICAgICAgey8qPiovfVxuICAgICAgICB7LyogIHtjdXJyZW5jeX0ge3RoaXMucmVuZGVyVGhvdXNhbmROdW0oXy5nZXQoc3VtbWFyeSwgXCJhZGp1c3RlZFByZW1pdW1cIiwgXCJcIikpfSovfVxuICAgICAgICB7Lyo8L1ZpZXdJdGVtPiovfVxuICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJHU1RcIikudGhhaShcIkdTVFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7Y3VycmVuY3l9IHt0aGlzLnJlbmRlclRob3VzYW5kTnVtKF8uZ2V0KHN1bW1hcnksIFwiZ3N0XCIsIFwiXCIpKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e2lzRW5kbyA/IExhbmd1YWdlLmVuKFwiVG90YWxcIikudGhhaShcIlRvdGFsXCIpLmdldE1lc3NhZ2UoKVxuICAgICAgICAgICAgOlxuICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJUb3RhbCBQcmVtaXVtXCIpLnRoYWkoXCJUb3RhbCBQcmVtaXVtXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHtjdXJyZW5jeX0ge3RoaXMucmVuZGVyVGhvdXNhbmROdW0oXy5nZXQoc3VtbWFyeSwgXCJ0b3RhbFByZW1pdW1cIiwgXCJcIikpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJDT01NSVNTSU9OIFJhdGUoJSlcIikudGhhaShcIkNPTU1JU1NJT04gUmF0ZSglKVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7KHBhcnNlRmxvYXQoXy5nZXQoc3VtbWFyeSwgXCJjb21taXNzaW9uUmF0ZVwiLCBcIlwiKSB8fCAwKSAqIDEwMCkudG9GaXhlZCgyKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQ09NTUlTU0lPTiBBbW91bnRcIikudGhhaShcIkNPTU1JU1NJT04gQW1vdW50XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHtjdXJyZW5jeX0ge3RoaXMucmVuZGVyVGhvdXNhbmROdW0oXy5nZXQoc3VtbWFyeSwgXCJjb21taXNzaW9uQW1vdW50XCIsIFwiXCIpKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQ09NTUlTU0lPTiBHU1RcIikudGhhaShcIkNPTU1JU1NJT04gR1NUXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHtjdXJyZW5jeX0ge3RoaXMucmVuZGVyVGhvdXNhbmROdW0oXy5nZXQoc3VtbWFyeSwgXCJjb21taXNzaW9uR3N0XCIsIFwiXCIpKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiVG90YWwgQ29tbWlzc2lvblwiKS50aGFpKFwiVG90YWwgQ29tbWlzc2lvblwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7Y3VycmVuY3l9IHt0aGlzLnJlbmRlclRob3VzYW5kTnVtKF8uZ2V0KHN1bW1hcnksIFwidG90YWxDb21taXNzaW9uXCIsIFwiXCIpKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiTmV0IFByZW1pdW1cIikudGhhaShcIk5ldCBQcmVtaXVtXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHtjdXJyZW5jeX0ge3RoaXMucmVuZGVyVGhvdXNhbmROdW0oXy5nZXQoc3VtbWFyeSwgXCJuZXRQcmVtaXVtXCIsIFwiXCIpKX1cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgIDwvPlxuICAgICk7XG4gIH1cblxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZGF0YUZpeGVkLCBmb3JtLCBlbmRvRml4ZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBkZWZhdWx0QWN0aXZlS2V5IH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IGlzSW5zdGFsbG1lbnRFbmFibGVkID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgdGhpcy5nZXRQcm9wcyhcImlzSW5zdGFsbG1lbnRFbmFibGVkXCIpKSB8fCBmYWxzZTtcbiAgICByZXR1cm4gKFxuICAgICAgPEMuVmlld0NvbXBvbmVudD5cbiAgICAgICAge3RoaXMuZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImRlc2lnbmF0aW9uXCIsIGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0tZXh0LWJlbmVmaWNpYXJ5LTAtZGVzaWduYXRpb25gIDogXCJleHQtYmVuZWZpY2lhcnktMC1kZXNpZ25hdGlvblwiKX1cbiAgICAgICAgPE5Db2xsYXBzZVxuICAgICAgICAgIGRlZmF1bHRBY3RpdmVLZXk9e2RlZmF1bHRBY3RpdmVLZXl9XG4gICAgICAgICAgYWN0aXZlS2V5PXtkZWZhdWx0QWN0aXZlS2V5fVxuICAgICAgICAgIG9uQ2hhbmdlPXsoa2V5OiBhbnkpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBkZWZhdWx0QWN0aXZlS2V5OiBbLi4uKGtleSBhcyBhbnlbXSldIH0pO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICBrZXk9XCIxXCJcbiAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJCYXNpYyBJbmZvcm1hdGlvblwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4guC5ieC4reC4oeC4ueC4peC4nuC4t+C5ieC4meC4kOC4suC4mVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgIHhgLzhgLHhgIHhgLbhgKHhgIHhgLvhgIDhgLrhgKHhgJzhgIDhgLpcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7dGhpcy5iYXNpY0luZm9SZW5kZXIoKX1cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICBrZXk9XCIyXCJcbiAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJDb21wYW55IEluZm9ybWF0aW9uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiQ29tcGFueSBJbmZvcm1hdGlvblwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxDb21wYW55SW5mb1ZpZXcgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbENvbDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeHM6IHsgc3BhbjogNiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc206IHsgc3BhbjogOCB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd3JhcHBlckNvbDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeHM6IHsgc3BhbjogMTggfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNtOiB7IHNwYW46IDE2IH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNNYWlsaW5nQWRkcmVzcz17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWVGaXhlZD17ZW5kb0ZpeGVkID8gKFwiZXh0LmNoYW5nZXMucG9saWN5aG9sZGVyXCIpIDogdGhpcy5nZXRQcm9wcyhcImV4dC5wb2xpY3lob2xkZXJcIil9Lz5cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICBrZXk9XCI2XCJcbiAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJMT0NBVElPTlNcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCJMT0NBVElPTlNcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8TG9jYXRpb25UYWJsZVZpZXcgZW5kb0ZpeGVkPXtlbmRvRml4ZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e2RhdGFGaXhlZCA/IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGRhdGFGaXhlZCwge30pIDogdGhpcy5wcm9wcy5tb2RlbH0vPlxuICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgIDxOUGFuZWxcbiAgICAgICAgICAgIGtleT1cIjNcIlxuICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkZFRVMgU1VNTUFSWVwiKVxuICAgICAgICAgICAgICAudGhhaShcIkZFRVMgU1VNTUFSWVwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlclN1bW1hcnkoKX1cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICB7XG4gICAgICAgICAgICAhZW5kb0ZpeGVkICYmIGlzSW5zdGFsbG1lbnRFbmFibGVkICYmXG4gICAgICAgICAgICA8TlBhbmVsIGtleT17XCIxMFwifVxuICAgICAgICAgICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiUEFZTUVOVCBBUlJBTkdFTUVOVFwiKVxuICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwiUEFZTUVOVCBBUlJBTkdFTUVOVFwiKVxuICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9PlxuICAgICAgICAgICAgICA8Vmlld1BheW1lbnRQbGFuU3R5bGVcbiAgICAgICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJQYXltZW50IFBsYW5cIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwiUGF5bWVudCBQbGFuXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9OUGFuZWw+XG4gICAgICAgICAgfVxuICAgICAgICAgIDxOUGFuZWxcbiAgICAgICAgICAgIGtleT1cIjRcIlxuICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkNMQVVTRVNcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCJDTEFVU0VTXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgey8qPFZpZXdJdGVtKi99XG4gICAgICAgICAgICB7LyogIHRpdGxlPXtMYW5ndWFnZS5lbihcIkNMQVVTRSBURVhUU1wiKS50aGFpKFwiQ0xBVVNFIFRFWFRTXCIpLmdldE1lc3NhZ2UoKX0qL31cbiAgICAgICAgICAgIHsvKj4qL31cbiAgICAgICAgICAgIHsvKiAqL31cbiAgICAgICAgICAgIHsvKjwvVmlld0l0ZW0+Ki99XG4gICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGJvcmRlcjogMCB9fSBjbGFzc05hbWU9XCJxbC1jb250YWluZXIgcWwtc25vd1wiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInFsLWVkaXRvclwiPlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3BzKGVuZG9GaXhlZCA/IGAke2VuZG9GaXhlZH0uc3BlY2lhbENsYXVzZXNgIDogXCJleHQuc3BlY2lhbENsYXVzZXNcIikpIH19Lz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICA8TlBhbmVsU2hvd091dHB1dHNcbiAgICAgICAgICAgIGlzRW5kbz17ISFlbmRvRml4ZWR9XG4gICAgICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgICAgIGtleT1cIjVcIlxuICAgICAgICAgICAgdGhhdD17dGhpc31cbiAgICAgICAgICAgIGlzUG9saWN5RG9jPXt0cnVlfVxuICAgICAgICAgICAgYWZ0ZXJVcGxvYWRQb2xpY3lEb2M9e3RoaXMucHJvcHMuYWZ0ZXJVcGxvYWRQb2xpY3lEb2N9XG4gICAgICAgICAgICBtb2RlbD17dGhpcy5wcm9wcy5tb2RlbH0vPlxuXG4gICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAga2V5PVwiOVwiXG4gICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiUmVjZW50IENvbW11bmljYXRpb24gSGlzdG9yeVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC4p+C4seC4leC4tOC4geC4suC4o+C4quC4t+C5iOC4reC4quC4suC4o+C4peC5iOC4suC4quC4uOC4lFwiKVxuICAgICAgICAgICAgICAubXkoXCJSRUNFTlQg4YCG4YCA4YC64YCe4YC94YCa4YC64YCZ4YC+4YCv4YCZ4YC+4YCQ4YC64YCQ4YCZ4YC64YC4XCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPEhpc3RvcnlUaW1lbGluZSBkYXRhRml4ZWQ9e2RhdGFGaXhlZH0gbW9kZWw9e3RoaXMucHJvcHMubW9kZWx9Lz5cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICA8TlBhbmVsU2hvd0F0dGFjaG1lbnQgZGF0YUZpeGVkPXtkYXRhRml4ZWR9IGtleT1cIjdcIiB0aGF0PXt0aGlzfS8+XG4gICAgICAgICAgPFF1aWNrTGlua3MgbW9kZWw9e3RoaXMucHJvcHMubW9kZWx9Lz5cbiAgICAgICAgPC9OQ29sbGFwc2U+XG4gICAgICA8L0MuVmlld0NvbXBvbmVudD5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFZpZXdDb21wb25lbnQ7XG5cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWdDQTs7Ozs7QUFNQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFrRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUF2Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQW5FQTtBQTZHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFySEE7QUFxSEE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaklBO0FBaUpBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBL0pBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFjQTs7Ozs7Ozs7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUdBOzs7QUFrRUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFpQkE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQUtBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUdBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFHQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7OztBQUdBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQVVBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFHQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7OztBQW5jQTtBQUNBO0FBcWNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/view-component.tsx
