__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Attachments", function() { return Attachments; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _basic_attachment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./basic-attachment */ "./src/app/desk/quote/SAIC/iar/basic-attachment.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk/quote/SAIC/all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/attachment/doc-types */ "./src/app/desk/component/attachment/doc-types.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/attachment.tsx";











var Attachments =
/*#__PURE__*/
function (_BasicAttachment) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Attachments, _BasicAttachment);

  function Attachments() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Attachments);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Attachments)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.onUseVoucher = function (campaignCode) {
      _this.setValueToModel(campaignCode, _this.getProp("promo.promoCode"));

      var newCart = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(_this.props.mergePolicyToServiceModel());

      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].CART_MERGE, newCart, {
        loading: true
      }).then(function (response) {
        _this.props.mergePolicyToUIModel((response.body || {}).respData);
      }).catch(function (error) {
        lodash__WEBPACK_IMPORTED_MODULE_9___default.a.set(newCart, _this.getProp("promo.promoCode"), "");

        _this.props.mergePolicyToUIModel(newCart);

        console.error(error);
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Attachments, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "renderAttachment",
    value: function renderAttachment() {
      var _this2 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_component__WEBPACK_IMPORTED_MODULE_15__["Attachment"], {
        model: model,
        form: form,
        dataIdPrefix: "policy",
        docTypes: Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_13__["getDocTypeForIar"])(),
        onChange: function onChange(disableBtn) {
          _this2.setState({
            disabled: !disableBtn
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      });
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_16__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_14__["list"],
        premiumId: "cartPremium.totalPremium",
        model: model,
        onUseVoucher: this.onUseVoucher,
        dataFixed: "policy",
        activeStep: "Attachments",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      });
    }
  }, {
    key: "handleNext",
    value: function handleNext() {
      var _this3 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var newCart = _this3.props.mergePolicyToServiceModel();

        newCart["policy.ext._ui.step"] = _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__["AllSteps"].SUBMIT;
        _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].CART_MERGE, newCart, {
          loading: true
        }).then(function (response) {
          _this3.props.mergePolicyToUIModel(response.body.respData || {});

          if (response.body.respCode !== "0000") return;
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this3, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__["AllSteps"].SUBMIT);
        }).catch(function (error) {
          return console.error(error);
        });
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "policy.ext.".concat(dataIdPrefix, ".").concat(propName);
      return "policy.ext.".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }]);

  return Attachments;
}(_basic_attachment__WEBPACK_IMPORTED_MODULE_8__["BasicAttachment"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYXR0YWNobWVudC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9hdHRhY2htZW50LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBCYXNpY0F0dGFjaG1lbnQgfSBmcm9tIFwiLi9iYXNpYy1hdHRhY2htZW50XCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgQWpheCwgQXBpcyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBwYWdlVG8gfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3dpemFyZFwiO1xuaW1wb3J0IHsgZ2V0RG9jVHlwZUZvcklhciB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYXR0YWNobWVudC9kb2MtdHlwZXNcIjtcbmltcG9ydCBTaWRlSW5mbyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3NpZGUtaW5mb1wiO1xuaW1wb3J0IHsgbGlzdCB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9pbmRleFwiO1xuaW1wb3J0IHsgQXR0YWNobWVudCB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCBTaWRlSW5mb1BocyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm9cIjtcblxuY2xhc3MgQXR0YWNobWVudHMgZXh0ZW5kcyBCYXNpY0F0dGFjaG1lbnQ8YW55LCBhbnksIGFueT4ge1xuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgfVxuXG4gIHJlbmRlckF0dGFjaG1lbnQoKSB7XG4gICAgY29uc3Qge1xuICAgICAgZm9ybSxcbiAgICAgIG1vZGVsLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8QXR0YWNobWVudFxuICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgZm9ybT17Zm9ybX1cbiAgICAgIGRhdGFJZFByZWZpeD17XCJwb2xpY3lcIn1cbiAgICAgIGRvY1R5cGVzPXtnZXREb2NUeXBlRm9ySWFyKCl9XG4gICAgICBvbkNoYW5nZT17KGRpc2FibGVCdG46IGFueSkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBkaXNhYmxlZDogIWRpc2FibGVCdG4sXG4gICAgICAgIH0pO1xuICAgICAgfX1cbiAgICAvPjtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCk6IGFueSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPFNpZGVJbmZvUGhzXG4gICAgICBsaW5lTGlzdD17bGlzdH1cbiAgICAgIHByZW1pdW1JZD17XCJjYXJ0UHJlbWl1bS50b3RhbFByZW1pdW1cIn1cbiAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgIG9uVXNlVm91Y2hlcj17dGhpcy5vblVzZVZvdWNoZXJ9XG4gICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICBhY3RpdmVTdGVwPXtcIkF0dGFjaG1lbnRzXCJ9Lz47XG4gIH1cblxuICBvblVzZVZvdWNoZXIgPSAoY2FtcGFpZ25Db2RlOiBhbnkpID0+IHtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChjYW1wYWlnbkNvZGUsIHRoaXMuZ2V0UHJvcChcInByb21vLnByb21vQ29kZVwiKSk7XG4gICAgY29uc3QgbmV3Q2FydCA9IF8uY2xvbmVEZWVwKHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpKTtcbiAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbCgocmVzcG9uc2UuYm9keSB8fCB7fSkucmVzcERhdGEpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIF8uc2V0KG5ld0NhcnQsIHRoaXMuZ2V0UHJvcChcInByb21vLnByb21vQ29kZVwiKSwgXCJcIik7XG4gICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwobmV3Q2FydCk7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlTmV4dCgpIHtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgICBjb25zdCBuZXdDYXJ0ID0gdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk7XG4gICAgICAgIG5ld0NhcnRbXCJwb2xpY3kuZXh0Ll91aS5zdGVwXCJdID0gQWxsU3RlcHMuU1VCTUlUO1xuICAgICAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgICAgICAudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgICAgICB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHJlc3BvbnNlLmJvZHkucmVzcERhdGEgfHwge30pO1xuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkucmVzcENvZGUgIT09IFwiMDAwMFwiKSByZXR1cm47XG4gICAgICAgICAgICBwYWdlVG8odGhpcywgQWxsU3RlcHMuU1VCTUlUKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG5cbiAgICB9KTtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBpZiAoZGF0YUlkUHJlZml4KSByZXR1cm4gYHBvbGljeS5leHQuJHtkYXRhSWRQcmVmaXh9LiR7cHJvcE5hbWV9YDtcbiAgICByZXR1cm4gYHBvbGljeS5leHQuJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgZ2V0UHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgIGlmIChfLmlzRW1wdHkocHJvcE5hbWUpKSB7XG4gICAgICByZXR1cm4gXCJwb2xpY3lcIjtcbiAgICB9XG4gICAgcmV0dXJuIGBwb2xpY3kuJHtwcm9wTmFtZX1gO1xuICB9XG59XG5cbmV4cG9ydCB7IEF0dGFjaG1lbnRzIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUExQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTs7O0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7OztBQWdCQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7QUE1RUE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/attachment.tsx
