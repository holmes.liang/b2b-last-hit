__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return createMention; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _component_Suggestions_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Suggestions.react */ "./node_modules/rc-editor-mention/es/component/Suggestions.react.js");
/* harmony import */ var _component_SuggestionPortal_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/SuggestionPortal.react */ "./node_modules/rc-editor-mention/es/component/SuggestionPortal.react.js");
/* harmony import */ var _component_MentionContent_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../component/MentionContent.react */ "./node_modules/rc-editor-mention/es/component/MentionContent.react.js");
/* harmony import */ var _model_mentionStore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/mentionStore */ "./node_modules/rc-editor-mention/es/model/mentionStore.js");
/* harmony import */ var _exportContent__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./exportContent */ "./node_modules/rc-editor-mention/es/utils/exportContent.js");
/* harmony import */ var _utils_getRegExp__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utils/getRegExp */ "./node_modules/rc-editor-mention/es/utils/getRegExp.js");









function findWithRegex(regex, contentBlock, callback) {
  // Get the text from the contentBlock
  var text = contentBlock.getText();
  var matchArr = void 0;
  var start = void 0; // eslint-disable-line

  var end = void 0; // Go through all matches in the text and return the indizes to the callback

  while ((matchArr = regex.exec(text)) !== null) {
    // eslint-disable-line
    start = matchArr.index;
    end = start + matchArr[0].length;
    callback(start, end);
    if (start === end) break;
  }
}

function mentionContentStrategy(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(function (character) {
    var entityKey = character.getEntity();
    return entityKey && contentState.getEntity(entityKey).getType() === 'mention';
  }, callback);
}

function noop() {}

var MentionContentComponent = function MentionContentComponent(props) {
  var entityKey = props.entityKey,
      tag = props.tag,
      callbacks = props.callbacks;
  var contentState = callbacks.getEditorState().getCurrentContent();
  var data = contentState.getEntity(entityKey).getData();
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(tag, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, {
    data: data
  }));
};

function createMention() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var callbacks = {
    onChange: noop,
    onUpArrow: noop,
    onDownArrow: noop,
    getEditorState: noop,
    setEditorState: noop,
    handleReturn: noop,
    onBlur: noop
  };
  var componentProps = {
    callbacks: callbacks,
    mentionStore: _model_mentionStore__WEBPACK_IMPORTED_MODULE_5__["default"]
  };
  var suggestionRegex = Object(_utils_getRegExp__WEBPACK_IMPORTED_MODULE_7__["default"])(config.prefix);
  var tag = config.tag || _component_MentionContent_react__WEBPACK_IMPORTED_MODULE_4__["default"];
  var decorators = [{
    strategy: function strategy(contentBlock, callback) {
      findWithRegex(suggestionRegex, contentBlock, callback);
    },
    component: function component(props) {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_component_SuggestionPortal_react__WEBPACK_IMPORTED_MODULE_3__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, componentProps, {
        style: config.mentionStyle,
        suggestionRegex: Object(_utils_getRegExp__WEBPACK_IMPORTED_MODULE_7__["default"])(config.prefix)
      }));
    }
  }];

  if (config.mode === 'immutable') {
    decorators.unshift({
      strategy: mentionContentStrategy,
      component: function component(props) {
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(MentionContentComponent, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          tag: tag
        }, props, {
          callbacks: callbacks
        }));
      }
    });
  }

  return {
    name: 'mention',
    Suggestions: function Suggestions(props) {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_component_Suggestions_react__WEBPACK_IMPORTED_MODULE_2__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, componentProps, {
        store: _model_mentionStore__WEBPACK_IMPORTED_MODULE_5__["default"]
      }));
    },
    decorators: decorators,
    onChange: function onChange(editorState) {
      return callbacks.onChange ? callbacks.onChange(editorState) : editorState;
    },
    callbacks: callbacks,
    'export': _exportContent__WEBPACK_IMPORTED_MODULE_6__["default"]
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvY3JlYXRlTWVudGlvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1tZW50aW9uL2VzL3V0aWxzL2NyZWF0ZU1lbnRpb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgX1N1Z2dlc3Rpb25zIGZyb20gJy4uL2NvbXBvbmVudC9TdWdnZXN0aW9ucy5yZWFjdCc7XG5pbXBvcnQgU3VnZ2VzdGlvblBvcnRhbCBmcm9tICcuLi9jb21wb25lbnQvU3VnZ2VzdGlvblBvcnRhbC5yZWFjdCc7XG5pbXBvcnQgTWVudGlvbkNvbnRlbnQgZnJvbSAnLi4vY29tcG9uZW50L01lbnRpb25Db250ZW50LnJlYWN0JztcbmltcG9ydCBtZW50aW9uU3RvcmUgZnJvbSAnLi4vbW9kZWwvbWVudGlvblN0b3JlJztcbmltcG9ydCBleHBvcnRDb250ZW50IGZyb20gJy4vZXhwb3J0Q29udGVudCc7XG5pbXBvcnQgZ2V0UmVnRXhwIGZyb20gJy4uL3V0aWxzL2dldFJlZ0V4cCc7XG5cbmZ1bmN0aW9uIGZpbmRXaXRoUmVnZXgocmVnZXgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spIHtcbiAgLy8gR2V0IHRoZSB0ZXh0IGZyb20gdGhlIGNvbnRlbnRCbG9ja1xuICB2YXIgdGV4dCA9IGNvbnRlbnRCbG9jay5nZXRUZXh0KCk7XG4gIHZhciBtYXRjaEFyciA9IHZvaWQgMDtcbiAgdmFyIHN0YXJ0ID0gdm9pZCAwOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gIHZhciBlbmQgPSB2b2lkIDA7XG4gIC8vIEdvIHRocm91Z2ggYWxsIG1hdGNoZXMgaW4gdGhlIHRleHQgYW5kIHJldHVybiB0aGUgaW5kaXplcyB0byB0aGUgY2FsbGJhY2tcbiAgd2hpbGUgKChtYXRjaEFyciA9IHJlZ2V4LmV4ZWModGV4dCkpICE9PSBudWxsKSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgIHN0YXJ0ID0gbWF0Y2hBcnIuaW5kZXg7XG4gICAgZW5kID0gc3RhcnQgKyBtYXRjaEFyclswXS5sZW5ndGg7XG4gICAgY2FsbGJhY2soc3RhcnQsIGVuZCk7XG4gICAgaWYgKHN0YXJ0ID09PSBlbmQpIGJyZWFrO1xuICB9XG59XG5cbmZ1bmN0aW9uIG1lbnRpb25Db250ZW50U3RyYXRlZ3koY29udGVudEJsb2NrLCBjYWxsYmFjaywgY29udGVudFN0YXRlKSB7XG4gIGNvbnRlbnRCbG9jay5maW5kRW50aXR5UmFuZ2VzKGZ1bmN0aW9uIChjaGFyYWN0ZXIpIHtcbiAgICB2YXIgZW50aXR5S2V5ID0gY2hhcmFjdGVyLmdldEVudGl0eSgpO1xuICAgIHJldHVybiBlbnRpdHlLZXkgJiYgY29udGVudFN0YXRlLmdldEVudGl0eShlbnRpdHlLZXkpLmdldFR5cGUoKSA9PT0gJ21lbnRpb24nO1xuICB9LCBjYWxsYmFjayk7XG59XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuXG52YXIgTWVudGlvbkNvbnRlbnRDb21wb25lbnQgPSBmdW5jdGlvbiBNZW50aW9uQ29udGVudENvbXBvbmVudChwcm9wcykge1xuICB2YXIgZW50aXR5S2V5ID0gcHJvcHMuZW50aXR5S2V5LFxuICAgICAgdGFnID0gcHJvcHMudGFnLFxuICAgICAgY2FsbGJhY2tzID0gcHJvcHMuY2FsbGJhY2tzO1xuXG4gIHZhciBjb250ZW50U3RhdGUgPSBjYWxsYmFja3MuZ2V0RWRpdG9yU3RhdGUoKS5nZXRDdXJyZW50Q29udGVudCgpO1xuICB2YXIgZGF0YSA9IGNvbnRlbnRTdGF0ZS5nZXRFbnRpdHkoZW50aXR5S2V5KS5nZXREYXRhKCk7XG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KHRhZywgX2V4dGVuZHMoe30sIHByb3BzLCB7IGRhdGE6IGRhdGEgfSkpO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY3JlYXRlTWVudGlvbigpIHtcbiAgdmFyIGNvbmZpZyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cbiAgdmFyIGNhbGxiYWNrcyA9IHtcbiAgICBvbkNoYW5nZTogbm9vcCxcbiAgICBvblVwQXJyb3c6IG5vb3AsXG4gICAgb25Eb3duQXJyb3c6IG5vb3AsXG4gICAgZ2V0RWRpdG9yU3RhdGU6IG5vb3AsXG4gICAgc2V0RWRpdG9yU3RhdGU6IG5vb3AsXG4gICAgaGFuZGxlUmV0dXJuOiBub29wLFxuICAgIG9uQmx1cjogbm9vcFxuICB9O1xuICB2YXIgY29tcG9uZW50UHJvcHMgPSB7XG4gICAgY2FsbGJhY2tzOiBjYWxsYmFja3MsXG4gICAgbWVudGlvblN0b3JlOiBtZW50aW9uU3RvcmVcbiAgfTtcbiAgdmFyIHN1Z2dlc3Rpb25SZWdleCA9IGdldFJlZ0V4cChjb25maWcucHJlZml4KTtcblxuICB2YXIgdGFnID0gY29uZmlnLnRhZyB8fCBNZW50aW9uQ29udGVudDtcbiAgdmFyIGRlY29yYXRvcnMgPSBbe1xuICAgIHN0cmF0ZWd5OiBmdW5jdGlvbiBzdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrKSB7XG4gICAgICBmaW5kV2l0aFJlZ2V4KHN1Z2dlc3Rpb25SZWdleCwgY29udGVudEJsb2NrLCBjYWxsYmFjayk7XG4gICAgfSxcbiAgICBjb21wb25lbnQ6IGZ1bmN0aW9uIGNvbXBvbmVudChwcm9wcykge1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoU3VnZ2VzdGlvblBvcnRhbCwgX2V4dGVuZHMoe30sIHByb3BzLCBjb21wb25lbnRQcm9wcywge1xuICAgICAgICBzdHlsZTogY29uZmlnLm1lbnRpb25TdHlsZSxcbiAgICAgICAgc3VnZ2VzdGlvblJlZ2V4OiBnZXRSZWdFeHAoY29uZmlnLnByZWZpeClcbiAgICAgIH0pKTtcbiAgICB9XG4gIH1dO1xuICBpZiAoY29uZmlnLm1vZGUgPT09ICdpbW11dGFibGUnKSB7XG4gICAgZGVjb3JhdG9ycy51bnNoaWZ0KHtcbiAgICAgIHN0cmF0ZWd5OiBtZW50aW9uQ29udGVudFN0cmF0ZWd5LFxuICAgICAgY29tcG9uZW50OiBmdW5jdGlvbiBjb21wb25lbnQocHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoTWVudGlvbkNvbnRlbnRDb21wb25lbnQsIF9leHRlbmRzKHsgdGFnOiB0YWcgfSwgcHJvcHMsIHsgY2FsbGJhY2tzOiBjYWxsYmFja3MgfSkpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBuYW1lOiAnbWVudGlvbicsXG4gICAgU3VnZ2VzdGlvbnM6IGZ1bmN0aW9uIFN1Z2dlc3Rpb25zKHByb3BzKSB7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChfU3VnZ2VzdGlvbnMsIF9leHRlbmRzKHt9LCBwcm9wcywgY29tcG9uZW50UHJvcHMsIHtcbiAgICAgICAgc3RvcmU6IG1lbnRpb25TdG9yZVxuICAgICAgfSkpO1xuICAgIH0sXG4gICAgZGVjb3JhdG9yczogZGVjb3JhdG9ycyxcbiAgICBvbkNoYW5nZTogZnVuY3Rpb24gb25DaGFuZ2UoZWRpdG9yU3RhdGUpIHtcbiAgICAgIHJldHVybiBjYWxsYmFja3Mub25DaGFuZ2UgPyBjYWxsYmFja3Mub25DaGFuZ2UoZWRpdG9yU3RhdGUpIDogZWRpdG9yU3RhdGU7XG4gICAgfSxcbiAgICBjYWxsYmFja3M6IGNhbGxiYWNrcyxcbiAgICAnZXhwb3J0JzogZXhwb3J0Q29udGVudFxuICB9O1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/createMention.js
