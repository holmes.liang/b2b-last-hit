__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BreadcrumbItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _dropdown_dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../dropdown/dropdown */ "./node_modules/antd/es/dropdown/dropdown.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};








var BreadcrumbItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BreadcrumbItem, _React$Component);

  function BreadcrumbItem() {
    var _this;

    _classCallCheck(this, BreadcrumbItem);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BreadcrumbItem).apply(this, arguments));

    _this.renderBreadcrumbItem = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          separator = _a.separator,
          children = _a.children,
          restProps = __rest(_a, ["prefixCls", "separator", "children"]);

      var prefixCls = getPrefixCls('breadcrumb', customizePrefixCls);
      var link;

      if ('href' in _this.props) {
        link = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", _extends({
          className: "".concat(prefixCls, "-link")
        }, Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(restProps, ['overlay'])), children);
      } else {
        link = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({
          className: "".concat(prefixCls, "-link")
        }, Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(restProps, ['overlay'])), children);
      } // wrap to dropDown


      link = _this.renderBreadcrumbNode(link, prefixCls);

      if (children) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, link, separator && separator !== '' && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: "".concat(prefixCls, "-separator")
        }, separator));
      }

      return null;
    };
    /**
     * if overlay is have
     * Wrap a DropDown
     */


    _this.renderBreadcrumbNode = function (breadcrumbItem, prefixCls) {
      var overlay = _this.props.overlay;

      if (overlay) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_dropdown_dropdown__WEBPACK_IMPORTED_MODULE_3__["default"], {
          overlay: overlay,
          placement: "bottomCenter"
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: "".concat(prefixCls, "-overlay-link")
        }, breadcrumbItem, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "down"
        })));
      }

      return breadcrumbItem;
    };

    return _this;
  }

  _createClass(BreadcrumbItem, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderBreadcrumbItem);
    }
  }]);

  return BreadcrumbItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


BreadcrumbItem.__ANT_BREADCRUMB_ITEM = true;
BreadcrumbItem.defaultProps = {
  separator: '/'
};
BreadcrumbItem.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  separator: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_1__["string"], prop_types__WEBPACK_IMPORTED_MODULE_1__["element"]]),
  href: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9icmVhZGNydW1iL0JyZWFkY3J1bWJJdGVtLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9icmVhZGNydW1iL0JyZWFkY3J1bWJJdGVtLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgKiBhcyBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBEcm9wRG93biBmcm9tICcuLi9kcm9wZG93bi9kcm9wZG93bic7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJyZWFkY3J1bWJJdGVtIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5yZW5kZXJCcmVhZGNydW1iSXRlbSA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHNlcGFyYXRvciwgY2hpbGRyZW4gfSA9IF9hLCByZXN0UHJvcHMgPSBfX3Jlc3QoX2EsIFtcInByZWZpeENsc1wiLCBcInNlcGFyYXRvclwiLCBcImNoaWxkcmVuXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnYnJlYWRjcnVtYicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBsZXQgbGluaztcbiAgICAgICAgICAgIGlmICgnaHJlZicgaW4gdGhpcy5wcm9wcykge1xuICAgICAgICAgICAgICAgIGxpbmsgPSAoPGEgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWxpbmtgfSB7Li4ub21pdChyZXN0UHJvcHMsIFsnb3ZlcmxheSddKX0+XG4gICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICA8L2E+KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGxpbmsgPSAoPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWxpbmtgfSB7Li4ub21pdChyZXN0UHJvcHMsIFsnb3ZlcmxheSddKX0+XG4gICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICA8L3NwYW4+KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIHdyYXAgdG8gZHJvcERvd25cbiAgICAgICAgICAgIGxpbmsgPSB0aGlzLnJlbmRlckJyZWFkY3J1bWJOb2RlKGxpbmssIHByZWZpeENscyk7XG4gICAgICAgICAgICBpZiAoY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKDxzcGFuPlxuICAgICAgICAgIHtsaW5rfVxuICAgICAgICAgIHtzZXBhcmF0b3IgJiYgc2VwYXJhdG9yICE9PSAnJyAmJiAoPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXNlcGFyYXRvcmB9PntzZXBhcmF0b3J9PC9zcGFuPil9XG4gICAgICAgIDwvc3Bhbj4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH07XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBpZiBvdmVybGF5IGlzIGhhdmVcbiAgICAgICAgICogV3JhcCBhIERyb3BEb3duXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLnJlbmRlckJyZWFkY3J1bWJOb2RlID0gKGJyZWFkY3J1bWJJdGVtLCBwcmVmaXhDbHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb3ZlcmxheSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvdmVybGF5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICg8RHJvcERvd24gb3ZlcmxheT17b3ZlcmxheX0gcGxhY2VtZW50PVwiYm90dG9tQ2VudGVyXCI+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LW92ZXJsYXktbGlua2B9PlxuICAgICAgICAgICAge2JyZWFkY3J1bWJJdGVtfVxuICAgICAgICAgICAgPEljb24gdHlwZT1cImRvd25cIi8+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L0Ryb3BEb3duPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gYnJlYWRjcnVtYkl0ZW07XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJCcmVhZGNydW1iSXRlbX08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5CcmVhZGNydW1iSXRlbS5fX0FOVF9CUkVBRENSVU1CX0lURU0gPSB0cnVlO1xuQnJlYWRjcnVtYkl0ZW0uZGVmYXVsdFByb3BzID0ge1xuICAgIHNlcGFyYXRvcjogJy8nLFxufTtcbkJyZWFkY3J1bWJJdGVtLnByb3BUeXBlcyA9IHtcbiAgICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2VwYXJhdG9yOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMuZWxlbWVudF0pLFxuICAgIGhyZWY6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBVkE7QUFDQTtBQUNBO0FBYUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBdEJBO0FBd0JBOzs7Ozs7QUFJQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFWQTtBQUNBO0FBL0JBO0FBMENBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7OztBQTlDQTtBQUNBO0FBREE7QUFnREE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUhBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/breadcrumb/BreadcrumbItem.js
