__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _field_group__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./field-group */ "./src/app/desk/component/field-group.tsx");
/* harmony import */ var _thai_address__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./thai-address */ "./src/app/desk/component/thai-address.tsx");
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _desk_component_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/index */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_ph__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/ph */ "./src/app/desk/component/ph/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/insured.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        .tax-no-group {\n          .group {\n            .ant-form-item {\n              width: 100%;\n              &:last-child {\n                width: auto;\n              }\n            }\n          } \n        }\n        .mobile-industry {\n          width: 100%\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}











var Insured =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Insured, _ModelWidget);

  function Insured(props, context) {
    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Insured);

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Insured).call(this, props, context));
    _this2.dataIdPrefix = _this2.props.dataIdPrefix || "ext.insured";
    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Insured, [{
    key: "initState",
    value: function initState() {
      var orgType = this.getValueFromModel(this.generatePropName("partyType"));
      var prefix = orgType === _common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ORG_TYPES.CORPORATE ? "corporate" : "individual";
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Insured.prototype), "initState", this).call(this), {
        prefix: prefix
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      return "".concat(this.dataIdPrefix, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();

      var _this = this;

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          dataFixed = _this$props.dataFixed,
          type = _this$props.type,
          notInsuredType = _this$props.notInsuredType,
          defaultCountryCode = _this$props.defaultCountryCode,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["model", "form", "dataFixed", "type", "notInsuredType", "defaultCountryCode"]);

      var orgType = this.getValueFromModel(this.generatePropName("partyType"));
      var formProps = {
        model: model,
        form: form
      };
      var moneyType = {
        MMK: "MMR",
        THB: "THA",
        SGD: "SGP"
      };
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }), !notInsuredType && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NRadio"], Object.assign({}, formProps, {
        required: true,
        tableName: "partytype",
        dataFixed: dataFixed,
        onChange: function onChange(value) {
          _this3.props.onChangeType && _this3.props.onChangeType(value);
        },
        propName: this.generatePropName("partyType"),
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Type of Insured").thai("ประเภทของผู้เอาประกันภัย").my("အာမခံထားအမျိုးအစား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      })), orgType === _common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ORG_TYPES.INDIVIDUAL && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_ph__WEBPACK_IMPORTED_MODULE_16__["PolicyHolder"], {
        model: model,
        form: form,
        isPartyType: false,
        partyType: "INDI",
        defaultCountryCode: defaultCountryCode,
        propsNameFixed: "".concat(this.props.policyholderPrefix || "ext.insured", ".individual"),
        dataFixed: dataFixed,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      })), orgType === _common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ORG_TYPES.CORPORATE && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_12__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Corporate Name").thai("นิติบุคคล").my("ကုမ္ပဏီအမည်").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NSelect"], Object.assign({}, formProps, {
        size: "large",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Title").thai("หัวเรื่อง").getMessage(),
        tableName: "orgtitle",
        dataFixed: this.props.dataFixed,
        propName: this.generatePropName("corporate.title"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], Object.assign({}, formProps, {
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Name").thai("ชื่อ").getMessage(),
        propName: this.generatePropName("corporate.name"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], Object.assign({}, formProps, {
        propName: this.generatePropName("corporate.idNo"),
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Corporate ID").thai("รหัส บริษัท").my("ကုမ္ပဏီအိုင်ဒီ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_14__["TextGroupStyle"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 163
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_12__["default"], {
        className: "tax-no-group",
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax No. / Branch").thai("หมายเลขภาษี. / สาขาภาษี").my("အခွန်မ။ / ညှနျ့").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], Object.assign({}, formProps, {
        required: true,
        placeholder: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax No.").thai("หมายเลขภาษี.").getMessage(),
        propName: this.generatePropName("corporate.taxNo"),
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax No.").thai("หมายเลขภาษี.").getMessage(),
        rules: [{
          validator: function validator(rule, value, callback) {
            if (_this.getValueFromModel(_this.generatePropName("partyType")) === _common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ORG_TYPES.CORPORATE && !_common__WEBPACK_IMPORTED_MODULE_11__["Rules"].isValidaTaxNo(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax No. must be a 13 digit number").thai("หมายเลขภาษีต้องเป็นตัวเลข 13 หลัก").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], Object.assign({}, formProps, {
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax Branch").thai("สาขาภาษี").getMessage(),
        placeholder: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax Branch").thai("สาขาภาษี").getMessage(),
        propName: this.generatePropName("corporate.taxBranch"),
        rules: [{
          validator: function validator(rule, value, callback) {
            if (_this.getValueFromModel(_this.generatePropName("partyType")) === _common__WEBPACK_IMPORTED_MODULE_11__["Consts"].ORG_TYPES.CORPORATE && isNaN(value) && value) {
              callback(_common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Tax Branch must be a number").thai("สาขาภาษีต้องเป็นตัวเลข").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      })))), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NSelect"], Object.assign({
        size: "large"
      }, formProps, {
        className: "mobile-industry",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Industry").thai("ประเภทนิติบุคคล").my("စက်မှုလုပ်ငန်း").getMessage(),
        tableName: "industry",
        dataFixed: this.props.dataFixed,
        propName: this.generatePropName("corporate.industry"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      })), type === "SAIC" ? _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_index__WEBPACK_IMPORTED_MODULE_15__["SpgAddress"], Object.assign({}, formProps, {
        dataId: this.generatePropName("corporate.address"),
        countryCode: moneyType[this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".currencyCode") : "currencyCode")],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      })) : _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_thai_address__WEBPACK_IMPORTED_MODULE_13__["default"], Object.assign({}, formProps, {
        notAddressType: true,
        countryCode: moneyType[this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".currencyCode") : "currencyCode")],
        prefix: this.generatePropName("corporate"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }))));
    }
  }]);

  return Insured;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

Insured.defaultProps = {
  notInsuredType: false
};
/* harmony default export */ __webpack_exports__["default"] = (Insured);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2luc3VyZWQudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2luc3VyZWQudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IE5UZXh0LCBOU2VsZWN0LCBOUmFkaW8gfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IENvbnN0cywgTGFuZ3VhZ2UsIFJ1bGVzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCBGaWVsZEdyb3VwIGZyb20gXCIuL2ZpZWxkLWdyb3VwXCI7XG5pbXBvcnQgVGhhaUFkZHJlc3MgZnJvbSBcIi4vdGhhaS1hZGRyZXNzXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgTW9iaWxlIH0gZnJvbSBcIi4vdXNlZnVsLWZvcm0taXRlbVwiO1xuaW1wb3J0IEdyb3VwQ3VzdG9tZXIgZnJvbSBcIi4vZ3JvdXAtY3VzdG9tZXJcIjtcbmltcG9ydCB7IFRleHRHcm91cFN0eWxlIH0gZnJvbSBcIkBkZXNrLXN0eWxlcy9nbG9iYWxcIjtcbmltcG9ydCB7IFNwZ0FkZHJlc3MgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2luZGV4XCI7XG5pbXBvcnQgeyBQb2xpY3lIb2xkZXIgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3BoXCI7XG5cbnR5cGUgSW5zdXJlZFN0YXRlID0ge1xuICBwcmVmaXg6IHN0cmluZztcbn07XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBJbnN1cmVkUGFnZUNvbXBvbmVudHMgPSB7XG4gIEJveENvbnRlbnQ6IFN0eWxlZERJVjtcbn07XG5leHBvcnQgdHlwZSBJbnN1cmVkUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgdHlwZT86IGFueTtcbiAgZGF0YUlkUHJlZml4Pzogc3RyaW5nO1xuICBkYXRhRml4ZWQ/OiBhbnk7XG4gIHBvbGljeWhvbGRlclByZWZpeD86IHN0cmluZztcbiAgb25DaGFuZ2VUeXBlPzogYW55O1xuICBub3RJbnN1cmVkVHlwZTogYm9vbGVhbjtcbiAgZGVmYXVsdENvdW50cnlDb2RlPzogYW55O1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmNsYXNzIEluc3VyZWQ8UCBleHRlbmRzIEluc3VyZWRQcm9wcywgUyBleHRlbmRzIEluc3VyZWRTdGF0ZSwgQyBleHRlbmRzIEluc3VyZWRQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLFxuICBTLFxuICBDPiB7XG4gIHByaXZhdGUgZGF0YUlkUHJlZml4OiBhbnkgPSB0aGlzLnByb3BzLmRhdGFJZFByZWZpeCB8fCBcImV4dC5pbnN1cmVkXCI7XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICBub3RJbnN1cmVkVHlwZTogZmFsc2UsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IEluc3VyZWRQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgY29uc3Qgb3JnVHlwZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGFydHlUeXBlXCIpKTtcbiAgICBjb25zdCBwcmVmaXggPSBvcmdUeXBlID09PSBDb25zdHMuT1JHX1RZUEVTLkNPUlBPUkFURSA/IFwiY29ycG9yYXRlXCIgOiBcImluZGl2aWR1YWxcIjtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgcHJlZml4OiBwcmVmaXgsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94Q29udGVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgLnRheC1uby1ncm91cCB7XG4gICAgICAgICAgLmdyb3VwIHtcbiAgICAgICAgICAgIC5hbnQtZm9ybS1pdGVtIHtcbiAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IFxuICAgICAgICB9XG4gICAgICAgIC5tb2JpbGUtaW5kdXN0cnkge1xuICAgICAgICAgIHdpZHRoOiAxMDAlXG4gICAgICAgIH1cbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiBgJHt0aGlzLmRhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCBfdGhpcyA9IHRoaXM7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgZGF0YUZpeGVkLCB0eXBlLCBub3RJbnN1cmVkVHlwZSwgZGVmYXVsdENvdW50cnlDb2RlLCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IG9yZ1R5cGUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBhcnR5VHlwZVwiKSk7XG4gICAgY29uc3QgZm9ybVByb3BzID0ge1xuICAgICAgbW9kZWwsXG4gICAgICBmb3JtLFxuICAgIH07XG4gICAgbGV0IG1vbmV5VHlwZTogYW55ID0geyBNTUs6IFwiTU1SXCIsIFRIQjogXCJUSEFcIiwgU0dEOiBcIlNHUFwiIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgIHshbm90SW5zdXJlZFR5cGUgJiYgPE5SYWRpb1xuICAgICAgICAgIHsuLi5mb3JtUHJvcHN9XG4gICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgdGFibGVOYW1lPVwicGFydHl0eXBlXCJcbiAgICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBzdHJpbmcpID0+IHtcbiAgICAgICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2VUeXBlICYmIHRoaXMucHJvcHMub25DaGFuZ2VUeXBlKHZhbHVlKTtcbiAgICAgICAgICB9fVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwYXJ0eVR5cGVcIil9XG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVHlwZSBvZiBJbnN1cmVkXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4l+C4guC4reC4h+C4nOC4ueC5ieC5gOC4reC4suC4m+C4o+C4sOC4geC4seC4meC4oOC4seC4olwiKVxuICAgICAgICAgICAgLm15KFwi4YCh4YCs4YCZ4YCB4YC24YCR4YCs4YC44YCh4YCZ4YC74YCt4YCv4YC44YCh4YCF4YCs4YC4XCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAvPn1cblxuICAgICAgICB7b3JnVHlwZSA9PT0gQ29uc3RzLk9SR19UWVBFUy5JTkRJVklEVUFMICYmIChcbiAgICAgICAgICA8PlxuICAgICAgICAgICAgPFBvbGljeUhvbGRlciBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlzUGFydHlUeXBlPXtmYWxzZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgcGFydHlUeXBlPXtcIklORElcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdENvdW50cnlDb2RlPXtkZWZhdWx0Q291bnRyeUNvZGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BzTmFtZUZpeGVkPXsoYCR7dGhpcy5wcm9wcy5wb2xpY3lob2xkZXJQcmVmaXggfHwgXCJleHQuaW5zdXJlZFwifS5pbmRpdmlkdWFsYCl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfS8+XG4gICAgICAgICAgPC8+XG4gICAgICAgICl9XG5cbiAgICAgICAge29yZ1R5cGUgPT09IENvbnN0cy5PUkdfVFlQRVMuQ09SUE9SQVRFICYmIChcbiAgICAgICAgICA8PlxuICAgICAgICAgICAgPEZpZWxkR3JvdXBcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwidXNhZ2UtZ3JvdXBcIlxuICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJDb3Jwb3JhdGUgTmFtZVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwi4LiZ4Li04LiV4Li04Lia4Li44LiE4LiE4LilXCIpXG4gICAgICAgICAgICAgICAgLm15KFwi4YCA4YCv4YCZ4YC54YCV4YCP4YCu4YCh4YCZ4YCK4YC6XCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgc2VsZWN0WHNTbT17eyB4czogOCwgc206IDYgfX1cbiAgICAgICAgICAgICAgdGV4dFhzU209e3sgeHM6IDE2LCBzbTogMTMgfX1cbiAgICAgICAgICAgICAgbWluV2lkdGg9XCIxNDBweFwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxOU2VsZWN0XG4gICAgICAgICAgICAgICAgey4uLmZvcm1Qcm9wc31cbiAgICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVGl0bGVcIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lix4Lin4LmA4Lij4Li34LmI4Lit4LiHXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgIHRhYmxlTmFtZT1cIm9yZ3RpdGxlXCJcbiAgICAgICAgICAgICAgICBkYXRhRml4ZWQ9e3RoaXMucHJvcHMuZGF0YUZpeGVkfVxuICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjb3Jwb3JhdGUudGl0bGVcIil9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxOVGV4dFxuICAgICAgICAgICAgICAgIHsuLi5mb3JtUHJvcHN9XG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiTmFtZVwiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguIrguLfguYjguK1cIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvcnBvcmF0ZS5uYW1lXCIpfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9GaWVsZEdyb3VwPlxuXG4gICAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgICAgey4uLmZvcm1Qcm9wc31cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNvcnBvcmF0ZS5pZE5vXCIpfVxuICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJDb3Jwb3JhdGUgSURcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4qiDguJrguKPguLTguKnguLHguJdcIilcbiAgICAgICAgICAgICAgICAubXkoXCLhgIDhgK/hgJnhgLnhgJXhgI/hgK7hgKHhgK3hgK/hgIThgLrhgJLhgK5cIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxUZXh0R3JvdXBTdHlsZT5cbiAgICAgICAgICAgICAgPEZpZWxkR3JvdXBcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ0YXgtbm8tZ3JvdXBcIlxuICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlRheCBOby4gLyBCcmFuY2hcIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiC4Lig4Liy4Lip4Li1LiAvIOC4quC4suC4guC4suC4oOC4suC4qeC4tVwiKVxuICAgICAgICAgICAgICAgICAgLm15KFwi4YCh4YCB4YC94YCU4YC64YCZ4YGLwqAvIOGAiuGAvuGAlOGAu+GAt1wiKVxuICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fVxuICAgICAgICAgICAgICAgIHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19XG4gICAgICAgICAgICAgICAgbWluV2lkdGg9XCIxNDBweFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgICAgICAgIHsuLi5mb3JtUHJvcHN9XG4gICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtMYW5ndWFnZS5lbihcIlRheCBOby5cIilcbiAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguKDguLLguKnguLUuXCIpXG4gICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29ycG9yYXRlLnRheE5vXCIpfVxuICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVGF4IE5vLlwiKVxuICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4q+C4oeC4suC4ouC5gOC4peC4guC4oOC4suC4qeC4tS5cIilcbiAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IocnVsZTogYW55LCB2YWx1ZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmdldFZhbHVlRnJvbU1vZGVsKF90aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwYXJ0eVR5cGVcIikpID09PSBDb25zdHMuT1JHX1RZUEVTLkNPUlBPUkFURSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAhUnVsZXMuaXNWYWxpZGFUYXhObyh2YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBMYW5ndWFnZS5lbihcIlRheCBOby4gbXVzdCBiZSBhIDEzIGRpZ2l0IG51bWJlclwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguKDguLLguKnguLXguJXguYnguK3guIfguYDguJvguYfguJnguJXguLHguKfguYDguKXguIIgMTMg4Lir4Lil4Lix4LiBXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICAgICAgPE5UZXh0XG4gICAgICAgICAgICAgICAgICB7Li4uZm9ybVByb3BzfVxuICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJUYXggQnJhbmNoXCIpXG4gICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Liq4Liy4LiC4Liy4Lig4Liy4Lip4Li1XCIpXG4gICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17TGFuZ3VhZ2UuZW4oXCJUYXggQnJhbmNoXCIpXG4gICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Liq4Liy4LiC4Liy4Lig4Liy4Lip4Li1XCIpXG4gICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29ycG9yYXRlLnRheEJyYW5jaFwiKX1cbiAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IocnVsZTogYW55LCB2YWx1ZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmdldFZhbHVlRnJvbU1vZGVsKF90aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwYXJ0eVR5cGVcIikpID09PSBDb25zdHMuT1JHX1RZUEVTLkNPUlBPUkFURSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICBpc05hTih2YWx1ZSkgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBMYW5ndWFnZS5lbihcIlRheCBCcmFuY2ggbXVzdCBiZSBhIG51bWJlclwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKrguLLguILguLLguKDguLLguKnguLXguJXguYnguK3guIfguYDguJvguYfguJnguJXguLHguKfguYDguKXguIJcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBdfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvRmllbGRHcm91cD5cbiAgICAgICAgICAgIDwvVGV4dEdyb3VwU3R5bGU+XG4gICAgICAgICAgICA8TlNlbGVjdFxuICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgIHsuLi5mb3JtUHJvcHN9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm1vYmlsZS1pbmR1c3RyeVwiXG4gICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJJbmR1c3RyeVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwi4Lib4Lij4Liw4LmA4Lig4LiX4LiZ4Li04LiV4Li04Lia4Li44LiE4LiE4LilXCIpXG4gICAgICAgICAgICAgICAgLm15KFwi4YCF4YCA4YC64YCZ4YC+4YCv4YCc4YCv4YCV4YC64YCE4YCU4YC64YC4XCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgdGFibGVOYW1lPVwiaW5kdXN0cnlcIlxuICAgICAgICAgICAgICBkYXRhRml4ZWQ9e3RoaXMucHJvcHMuZGF0YUZpeGVkfVxuICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29ycG9yYXRlLmluZHVzdHJ5XCIpfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIHt0eXBlID09PSBcIlNBSUNcIiA/XG4gICAgICAgICAgICAgIDxTcGdBZGRyZXNzIHsuLi5mb3JtUHJvcHN9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFJZD17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29ycG9yYXRlLmFkZHJlc3NcIil9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvdW50cnlDb2RlPXttb25leVR5cGVbdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChkYXRhRml4ZWQgPyBgJHtkYXRhRml4ZWR9LmN1cnJlbmN5Q29kZWAgOiBcImN1cnJlbmN5Q29kZVwiKV19Lz5cbiAgICAgICAgICAgICAgOlxuICAgICAgICAgICAgICA8VGhhaUFkZHJlc3Mgey4uLmZvcm1Qcm9wc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vdEFkZHJlc3NUeXBlPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnRyeUNvZGU9e21vbmV5VHlwZVt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0uY3VycmVuY3lDb2RlYCA6IFwiY3VycmVuY3lDb2RlXCIpXX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZWZpeD17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY29ycG9yYXRlXCIpfS8+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgPC8+XG4gICAgICAgICl9XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEluc3VyZWQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFzQkE7Ozs7O0FBU0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQWlCQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFHQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUNBO0FBR0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUlBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQVhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdDQTtBQUNBO0FBR0E7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUtBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQVhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBOzs7O0FBbk9BO0FBQ0E7QUFEQTtBQU1BO0FBREE7QUFpT0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/insured.tsx
