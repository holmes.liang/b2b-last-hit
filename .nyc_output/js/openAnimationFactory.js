__webpack_require__.r(__webpack_exports__);
/* harmony import */ var css_animation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! css-animation */ "./node_modules/css-animation/es/index.js");


function animate(node, show, transitionName, done) {
  var height = void 0;
  return Object(css_animation__WEBPACK_IMPORTED_MODULE_0__["default"])(node, transitionName, {
    start: function start() {
      if (!show) {
        node.style.height = node.offsetHeight + 'px';
      } else {
        height = node.offsetHeight;
        node.style.height = 0;
      }
    },
    active: function active() {
      node.style.height = (show ? height : 0) + 'px';
    },
    end: function end() {
      node.style.height = '';
      done();
    }
  });
}

function animation(prefixCls) {
  return {
    enter: function enter(node, done) {
      return animate(node, true, prefixCls + '-anim', done);
    },
    leave: function leave(node, done) {
      return animate(node, false, prefixCls + '-anim', done);
    }
  };
}

/* harmony default export */ __webpack_exports__["default"] = (animation);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY29sbGFwc2UvZXMvb3BlbkFuaW1hdGlvbkZhY3RvcnkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1jb2xsYXBzZS9lcy9vcGVuQW5pbWF0aW9uRmFjdG9yeS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY3NzQW5pbWF0aW9uIGZyb20gJ2Nzcy1hbmltYXRpb24nO1xuXG5mdW5jdGlvbiBhbmltYXRlKG5vZGUsIHNob3csIHRyYW5zaXRpb25OYW1lLCBkb25lKSB7XG4gIHZhciBoZWlnaHQgPSB2b2lkIDA7XG4gIHJldHVybiBjc3NBbmltYXRpb24obm9kZSwgdHJhbnNpdGlvbk5hbWUsIHtcbiAgICBzdGFydDogZnVuY3Rpb24gc3RhcnQoKSB7XG4gICAgICBpZiAoIXNob3cpIHtcbiAgICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSBub2RlLm9mZnNldEhlaWdodCArICdweCc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBoZWlnaHQgPSBub2RlLm9mZnNldEhlaWdodDtcbiAgICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSAwO1xuICAgICAgfVxuICAgIH0sXG4gICAgYWN0aXZlOiBmdW5jdGlvbiBhY3RpdmUoKSB7XG4gICAgICBub2RlLnN0eWxlLmhlaWdodCA9IChzaG93ID8gaGVpZ2h0IDogMCkgKyAncHgnO1xuICAgIH0sXG4gICAgZW5kOiBmdW5jdGlvbiBlbmQoKSB7XG4gICAgICBub2RlLnN0eWxlLmhlaWdodCA9ICcnO1xuICAgICAgZG9uZSgpO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGFuaW1hdGlvbihwcmVmaXhDbHMpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnRlcjogZnVuY3Rpb24gZW50ZXIobm9kZSwgZG9uZSkge1xuICAgICAgcmV0dXJuIGFuaW1hdGUobm9kZSwgdHJ1ZSwgcHJlZml4Q2xzICsgJy1hbmltJywgZG9uZSk7XG4gICAgfSxcbiAgICBsZWF2ZTogZnVuY3Rpb24gbGVhdmUobm9kZSwgZG9uZSkge1xuICAgICAgcmV0dXJuIGFuaW1hdGUobm9kZSwgZmFsc2UsIHByZWZpeENscyArICctYW5pbScsIGRvbmUpO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYW5pbWF0aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-collapse/es/openAnimationFactory.js
