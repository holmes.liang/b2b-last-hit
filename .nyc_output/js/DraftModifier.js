/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftModifier
 * @format
 * 
 */


var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var ContentStateInlineStyle = __webpack_require__(/*! ./ContentStateInlineStyle */ "./node_modules/draft-js/lib/ContentStateInlineStyle.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var applyEntityToContentState = __webpack_require__(/*! ./applyEntityToContentState */ "./node_modules/draft-js/lib/applyEntityToContentState.js");

var getCharacterRemovalRange = __webpack_require__(/*! ./getCharacterRemovalRange */ "./node_modules/draft-js/lib/getCharacterRemovalRange.js");

var getContentStateFragment = __webpack_require__(/*! ./getContentStateFragment */ "./node_modules/draft-js/lib/getContentStateFragment.js");

var insertFragmentIntoContentState = __webpack_require__(/*! ./insertFragmentIntoContentState */ "./node_modules/draft-js/lib/insertFragmentIntoContentState.js");

var insertTextIntoContentState = __webpack_require__(/*! ./insertTextIntoContentState */ "./node_modules/draft-js/lib/insertTextIntoContentState.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var modifyBlockForContentState = __webpack_require__(/*! ./modifyBlockForContentState */ "./node_modules/draft-js/lib/modifyBlockForContentState.js");

var removeEntitiesAtEdges = __webpack_require__(/*! ./removeEntitiesAtEdges */ "./node_modules/draft-js/lib/removeEntitiesAtEdges.js");

var removeRangeFromContentState = __webpack_require__(/*! ./removeRangeFromContentState */ "./node_modules/draft-js/lib/removeRangeFromContentState.js");

var splitBlockInContentState = __webpack_require__(/*! ./splitBlockInContentState */ "./node_modules/draft-js/lib/splitBlockInContentState.js");

var OrderedSet = Immutable.OrderedSet;
/**
 * `DraftModifier` provides a set of convenience methods that apply
 * modifications to a `ContentState` object based on a target `SelectionState`.
 *
 * Any change to a `ContentState` should be decomposable into a series of
 * transaction functions that apply the required changes and return output
 * `ContentState` objects.
 *
 * These functions encapsulate some of the most common transaction sequences.
 */

var DraftModifier = {
  replaceText: function replaceText(contentState, rangeToReplace, text, inlineStyle, entityKey) {
    var withoutEntities = removeEntitiesAtEdges(contentState, rangeToReplace);
    var withoutText = removeRangeFromContentState(withoutEntities, rangeToReplace);
    var character = CharacterMetadata.create({
      style: inlineStyle || OrderedSet(),
      entity: entityKey || null
    });
    return insertTextIntoContentState(withoutText, withoutText.getSelectionAfter(), text, character);
  },
  insertText: function insertText(contentState, targetRange, text, inlineStyle, entityKey) {
    !targetRange.isCollapsed() ?  true ? invariant(false, 'Target range must be collapsed for `insertText`.') : undefined : void 0;
    return DraftModifier.replaceText(contentState, targetRange, text, inlineStyle, entityKey);
  },
  moveText: function moveText(contentState, removalRange, targetRange) {
    var movedFragment = getContentStateFragment(contentState, removalRange);
    var afterRemoval = DraftModifier.removeRange(contentState, removalRange, 'backward');
    return DraftModifier.replaceWithFragment(afterRemoval, targetRange, movedFragment);
  },
  replaceWithFragment: function replaceWithFragment(contentState, targetRange, fragment) {
    var withoutEntities = removeEntitiesAtEdges(contentState, targetRange);
    var withoutText = removeRangeFromContentState(withoutEntities, targetRange);
    return insertFragmentIntoContentState(withoutText, withoutText.getSelectionAfter(), fragment);
  },
  removeRange: function removeRange(contentState, rangeToRemove, removalDirection) {
    var startKey = void 0,
        endKey = void 0,
        startBlock = void 0,
        endBlock = void 0;

    if (rangeToRemove.getIsBackward()) {
      rangeToRemove = rangeToRemove.merge({
        anchorKey: rangeToRemove.getFocusKey(),
        anchorOffset: rangeToRemove.getFocusOffset(),
        focusKey: rangeToRemove.getAnchorKey(),
        focusOffset: rangeToRemove.getAnchorOffset(),
        isBackward: false
      });
    }

    startKey = rangeToRemove.getAnchorKey();
    endKey = rangeToRemove.getFocusKey();
    startBlock = contentState.getBlockForKey(startKey);
    endBlock = contentState.getBlockForKey(endKey);
    var startOffset = rangeToRemove.getStartOffset();
    var endOffset = rangeToRemove.getEndOffset();
    var startEntityKey = startBlock.getEntityAt(startOffset);
    var endEntityKey = endBlock.getEntityAt(endOffset - 1); // Check whether the selection state overlaps with a single entity.
    // If so, try to remove the appropriate substring of the entity text.

    if (startKey === endKey) {
      if (startEntityKey && startEntityKey === endEntityKey) {
        var _adjustedRemovalRange = getCharacterRemovalRange(contentState.getEntityMap(), startBlock, endBlock, rangeToRemove, removalDirection);

        return removeRangeFromContentState(contentState, _adjustedRemovalRange);
      }
    }

    var adjustedRemovalRange = rangeToRemove;

    if (DraftFeatureFlags.draft_segmented_entities_behavior) {
      // Adjust the selection to properly delete segemented and immutable
      // entities
      adjustedRemovalRange = getCharacterRemovalRange(contentState.getEntityMap(), startBlock, endBlock, rangeToRemove, removalDirection);
    }

    var withoutEntities = removeEntitiesAtEdges(contentState, adjustedRemovalRange);
    return removeRangeFromContentState(withoutEntities, adjustedRemovalRange);
  },
  splitBlock: function splitBlock(contentState, selectionState) {
    var withoutEntities = removeEntitiesAtEdges(contentState, selectionState);
    var withoutText = removeRangeFromContentState(withoutEntities, selectionState);
    return splitBlockInContentState(withoutText, withoutText.getSelectionAfter());
  },
  applyInlineStyle: function applyInlineStyle(contentState, selectionState, inlineStyle) {
    return ContentStateInlineStyle.add(contentState, selectionState, inlineStyle);
  },
  removeInlineStyle: function removeInlineStyle(contentState, selectionState, inlineStyle) {
    return ContentStateInlineStyle.remove(contentState, selectionState, inlineStyle);
  },
  setBlockType: function setBlockType(contentState, selectionState, blockType) {
    return modifyBlockForContentState(contentState, selectionState, function (block) {
      return block.merge({
        type: blockType,
        depth: 0
      });
    });
  },
  setBlockData: function setBlockData(contentState, selectionState, blockData) {
    return modifyBlockForContentState(contentState, selectionState, function (block) {
      return block.merge({
        data: blockData
      });
    });
  },
  mergeBlockData: function mergeBlockData(contentState, selectionState, blockData) {
    return modifyBlockForContentState(contentState, selectionState, function (block) {
      return block.merge({
        data: block.getData().merge(blockData)
      });
    });
  },
  applyEntity: function applyEntity(contentState, selectionState, entityKey) {
    var withoutEntities = removeEntitiesAtEdges(contentState, selectionState);
    return applyEntityToContentState(withoutEntities, selectionState, entityKey);
  }
};
module.exports = DraftModifier;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0TW9kaWZpZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRNb2RpZmllci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0TW9kaWZpZXJcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xudmFyIENvbnRlbnRTdGF0ZUlubGluZVN0eWxlID0gcmVxdWlyZSgnLi9Db250ZW50U3RhdGVJbmxpbmVTdHlsZScpO1xudmFyIERyYWZ0RmVhdHVyZUZsYWdzID0gcmVxdWlyZSgnLi9EcmFmdEZlYXR1cmVGbGFncycpO1xudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgYXBwbHlFbnRpdHlUb0NvbnRlbnRTdGF0ZSA9IHJlcXVpcmUoJy4vYXBwbHlFbnRpdHlUb0NvbnRlbnRTdGF0ZScpO1xudmFyIGdldENoYXJhY3RlclJlbW92YWxSYW5nZSA9IHJlcXVpcmUoJy4vZ2V0Q2hhcmFjdGVyUmVtb3ZhbFJhbmdlJyk7XG52YXIgZ2V0Q29udGVudFN0YXRlRnJhZ21lbnQgPSByZXF1aXJlKCcuL2dldENvbnRlbnRTdGF0ZUZyYWdtZW50Jyk7XG52YXIgaW5zZXJ0RnJhZ21lbnRJbnRvQ29udGVudFN0YXRlID0gcmVxdWlyZSgnLi9pbnNlcnRGcmFnbWVudEludG9Db250ZW50U3RhdGUnKTtcbnZhciBpbnNlcnRUZXh0SW50b0NvbnRlbnRTdGF0ZSA9IHJlcXVpcmUoJy4vaW5zZXJ0VGV4dEludG9Db250ZW50U3RhdGUnKTtcbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcbnZhciBtb2RpZnlCbG9ja0ZvckNvbnRlbnRTdGF0ZSA9IHJlcXVpcmUoJy4vbW9kaWZ5QmxvY2tGb3JDb250ZW50U3RhdGUnKTtcbnZhciByZW1vdmVFbnRpdGllc0F0RWRnZXMgPSByZXF1aXJlKCcuL3JlbW92ZUVudGl0aWVzQXRFZGdlcycpO1xudmFyIHJlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZSA9IHJlcXVpcmUoJy4vcmVtb3ZlUmFuZ2VGcm9tQ29udGVudFN0YXRlJyk7XG52YXIgc3BsaXRCbG9ja0luQ29udGVudFN0YXRlID0gcmVxdWlyZSgnLi9zcGxpdEJsb2NrSW5Db250ZW50U3RhdGUnKTtcblxudmFyIE9yZGVyZWRTZXQgPSBJbW11dGFibGUuT3JkZXJlZFNldDtcblxuLyoqXG4gKiBgRHJhZnRNb2RpZmllcmAgcHJvdmlkZXMgYSBzZXQgb2YgY29udmVuaWVuY2UgbWV0aG9kcyB0aGF0IGFwcGx5XG4gKiBtb2RpZmljYXRpb25zIHRvIGEgYENvbnRlbnRTdGF0ZWAgb2JqZWN0IGJhc2VkIG9uIGEgdGFyZ2V0IGBTZWxlY3Rpb25TdGF0ZWAuXG4gKlxuICogQW55IGNoYW5nZSB0byBhIGBDb250ZW50U3RhdGVgIHNob3VsZCBiZSBkZWNvbXBvc2FibGUgaW50byBhIHNlcmllcyBvZlxuICogdHJhbnNhY3Rpb24gZnVuY3Rpb25zIHRoYXQgYXBwbHkgdGhlIHJlcXVpcmVkIGNoYW5nZXMgYW5kIHJldHVybiBvdXRwdXRcbiAqIGBDb250ZW50U3RhdGVgIG9iamVjdHMuXG4gKlxuICogVGhlc2UgZnVuY3Rpb25zIGVuY2Fwc3VsYXRlIHNvbWUgb2YgdGhlIG1vc3QgY29tbW9uIHRyYW5zYWN0aW9uIHNlcXVlbmNlcy5cbiAqL1xuXG52YXIgRHJhZnRNb2RpZmllciA9IHtcbiAgcmVwbGFjZVRleHQ6IGZ1bmN0aW9uIHJlcGxhY2VUZXh0KGNvbnRlbnRTdGF0ZSwgcmFuZ2VUb1JlcGxhY2UsIHRleHQsIGlubGluZVN0eWxlLCBlbnRpdHlLZXkpIHtcbiAgICB2YXIgd2l0aG91dEVudGl0aWVzID0gcmVtb3ZlRW50aXRpZXNBdEVkZ2VzKGNvbnRlbnRTdGF0ZSwgcmFuZ2VUb1JlcGxhY2UpO1xuICAgIHZhciB3aXRob3V0VGV4dCA9IHJlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZSh3aXRob3V0RW50aXRpZXMsIHJhbmdlVG9SZXBsYWNlKTtcblxuICAgIHZhciBjaGFyYWN0ZXIgPSBDaGFyYWN0ZXJNZXRhZGF0YS5jcmVhdGUoe1xuICAgICAgc3R5bGU6IGlubGluZVN0eWxlIHx8IE9yZGVyZWRTZXQoKSxcbiAgICAgIGVudGl0eTogZW50aXR5S2V5IHx8IG51bGxcbiAgICB9KTtcblxuICAgIHJldHVybiBpbnNlcnRUZXh0SW50b0NvbnRlbnRTdGF0ZSh3aXRob3V0VGV4dCwgd2l0aG91dFRleHQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSwgdGV4dCwgY2hhcmFjdGVyKTtcbiAgfSxcblxuICBpbnNlcnRUZXh0OiBmdW5jdGlvbiBpbnNlcnRUZXh0KGNvbnRlbnRTdGF0ZSwgdGFyZ2V0UmFuZ2UsIHRleHQsIGlubGluZVN0eWxlLCBlbnRpdHlLZXkpIHtcbiAgICAhdGFyZ2V0UmFuZ2UuaXNDb2xsYXBzZWQoKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdUYXJnZXQgcmFuZ2UgbXVzdCBiZSBjb2xsYXBzZWQgZm9yIGBpbnNlcnRUZXh0YC4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgcmV0dXJuIERyYWZ0TW9kaWZpZXIucmVwbGFjZVRleHQoY29udGVudFN0YXRlLCB0YXJnZXRSYW5nZSwgdGV4dCwgaW5saW5lU3R5bGUsIGVudGl0eUtleSk7XG4gIH0sXG5cbiAgbW92ZVRleHQ6IGZ1bmN0aW9uIG1vdmVUZXh0KGNvbnRlbnRTdGF0ZSwgcmVtb3ZhbFJhbmdlLCB0YXJnZXRSYW5nZSkge1xuICAgIHZhciBtb3ZlZEZyYWdtZW50ID0gZ2V0Q29udGVudFN0YXRlRnJhZ21lbnQoY29udGVudFN0YXRlLCByZW1vdmFsUmFuZ2UpO1xuXG4gICAgdmFyIGFmdGVyUmVtb3ZhbCA9IERyYWZ0TW9kaWZpZXIucmVtb3ZlUmFuZ2UoY29udGVudFN0YXRlLCByZW1vdmFsUmFuZ2UsICdiYWNrd2FyZCcpO1xuXG4gICAgcmV0dXJuIERyYWZ0TW9kaWZpZXIucmVwbGFjZVdpdGhGcmFnbWVudChhZnRlclJlbW92YWwsIHRhcmdldFJhbmdlLCBtb3ZlZEZyYWdtZW50KTtcbiAgfSxcblxuICByZXBsYWNlV2l0aEZyYWdtZW50OiBmdW5jdGlvbiByZXBsYWNlV2l0aEZyYWdtZW50KGNvbnRlbnRTdGF0ZSwgdGFyZ2V0UmFuZ2UsIGZyYWdtZW50KSB7XG4gICAgdmFyIHdpdGhvdXRFbnRpdGllcyA9IHJlbW92ZUVudGl0aWVzQXRFZGdlcyhjb250ZW50U3RhdGUsIHRhcmdldFJhbmdlKTtcbiAgICB2YXIgd2l0aG91dFRleHQgPSByZW1vdmVSYW5nZUZyb21Db250ZW50U3RhdGUod2l0aG91dEVudGl0aWVzLCB0YXJnZXRSYW5nZSk7XG5cbiAgICByZXR1cm4gaW5zZXJ0RnJhZ21lbnRJbnRvQ29udGVudFN0YXRlKHdpdGhvdXRUZXh0LCB3aXRob3V0VGV4dC5nZXRTZWxlY3Rpb25BZnRlcigpLCBmcmFnbWVudCk7XG4gIH0sXG5cbiAgcmVtb3ZlUmFuZ2U6IGZ1bmN0aW9uIHJlbW92ZVJhbmdlKGNvbnRlbnRTdGF0ZSwgcmFuZ2VUb1JlbW92ZSwgcmVtb3ZhbERpcmVjdGlvbikge1xuICAgIHZhciBzdGFydEtleSA9IHZvaWQgMCxcbiAgICAgICAgZW5kS2V5ID0gdm9pZCAwLFxuICAgICAgICBzdGFydEJsb2NrID0gdm9pZCAwLFxuICAgICAgICBlbmRCbG9jayA9IHZvaWQgMDtcbiAgICBpZiAocmFuZ2VUb1JlbW92ZS5nZXRJc0JhY2t3YXJkKCkpIHtcbiAgICAgIHJhbmdlVG9SZW1vdmUgPSByYW5nZVRvUmVtb3ZlLm1lcmdlKHtcbiAgICAgICAgYW5jaG9yS2V5OiByYW5nZVRvUmVtb3ZlLmdldEZvY3VzS2V5KCksXG4gICAgICAgIGFuY2hvck9mZnNldDogcmFuZ2VUb1JlbW92ZS5nZXRGb2N1c09mZnNldCgpLFxuICAgICAgICBmb2N1c0tleTogcmFuZ2VUb1JlbW92ZS5nZXRBbmNob3JLZXkoKSxcbiAgICAgICAgZm9jdXNPZmZzZXQ6IHJhbmdlVG9SZW1vdmUuZ2V0QW5jaG9yT2Zmc2V0KCksXG4gICAgICAgIGlzQmFja3dhcmQ6IGZhbHNlXG4gICAgICB9KTtcbiAgICB9XG4gICAgc3RhcnRLZXkgPSByYW5nZVRvUmVtb3ZlLmdldEFuY2hvcktleSgpO1xuICAgIGVuZEtleSA9IHJhbmdlVG9SZW1vdmUuZ2V0Rm9jdXNLZXkoKTtcbiAgICBzdGFydEJsb2NrID0gY29udGVudFN0YXRlLmdldEJsb2NrRm9yS2V5KHN0YXJ0S2V5KTtcbiAgICBlbmRCbG9jayA9IGNvbnRlbnRTdGF0ZS5nZXRCbG9ja0ZvcktleShlbmRLZXkpO1xuICAgIHZhciBzdGFydE9mZnNldCA9IHJhbmdlVG9SZW1vdmUuZ2V0U3RhcnRPZmZzZXQoKTtcbiAgICB2YXIgZW5kT2Zmc2V0ID0gcmFuZ2VUb1JlbW92ZS5nZXRFbmRPZmZzZXQoKTtcblxuICAgIHZhciBzdGFydEVudGl0eUtleSA9IHN0YXJ0QmxvY2suZ2V0RW50aXR5QXQoc3RhcnRPZmZzZXQpO1xuICAgIHZhciBlbmRFbnRpdHlLZXkgPSBlbmRCbG9jay5nZXRFbnRpdHlBdChlbmRPZmZzZXQgLSAxKTtcblxuICAgIC8vIENoZWNrIHdoZXRoZXIgdGhlIHNlbGVjdGlvbiBzdGF0ZSBvdmVybGFwcyB3aXRoIGEgc2luZ2xlIGVudGl0eS5cbiAgICAvLyBJZiBzbywgdHJ5IHRvIHJlbW92ZSB0aGUgYXBwcm9wcmlhdGUgc3Vic3RyaW5nIG9mIHRoZSBlbnRpdHkgdGV4dC5cbiAgICBpZiAoc3RhcnRLZXkgPT09IGVuZEtleSkge1xuICAgICAgaWYgKHN0YXJ0RW50aXR5S2V5ICYmIHN0YXJ0RW50aXR5S2V5ID09PSBlbmRFbnRpdHlLZXkpIHtcbiAgICAgICAgdmFyIF9hZGp1c3RlZFJlbW92YWxSYW5nZSA9IGdldENoYXJhY3RlclJlbW92YWxSYW5nZShjb250ZW50U3RhdGUuZ2V0RW50aXR5TWFwKCksIHN0YXJ0QmxvY2ssIGVuZEJsb2NrLCByYW5nZVRvUmVtb3ZlLCByZW1vdmFsRGlyZWN0aW9uKTtcbiAgICAgICAgcmV0dXJuIHJlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIF9hZGp1c3RlZFJlbW92YWxSYW5nZSk7XG4gICAgICB9XG4gICAgfVxuICAgIHZhciBhZGp1c3RlZFJlbW92YWxSYW5nZSA9IHJhbmdlVG9SZW1vdmU7XG4gICAgaWYgKERyYWZ0RmVhdHVyZUZsYWdzLmRyYWZ0X3NlZ21lbnRlZF9lbnRpdGllc19iZWhhdmlvcikge1xuICAgICAgLy8gQWRqdXN0IHRoZSBzZWxlY3Rpb24gdG8gcHJvcGVybHkgZGVsZXRlIHNlZ2VtZW50ZWQgYW5kIGltbXV0YWJsZVxuICAgICAgLy8gZW50aXRpZXNcbiAgICAgIGFkanVzdGVkUmVtb3ZhbFJhbmdlID0gZ2V0Q2hhcmFjdGVyUmVtb3ZhbFJhbmdlKGNvbnRlbnRTdGF0ZS5nZXRFbnRpdHlNYXAoKSwgc3RhcnRCbG9jaywgZW5kQmxvY2ssIHJhbmdlVG9SZW1vdmUsIHJlbW92YWxEaXJlY3Rpb24pO1xuICAgIH1cblxuICAgIHZhciB3aXRob3V0RW50aXRpZXMgPSByZW1vdmVFbnRpdGllc0F0RWRnZXMoY29udGVudFN0YXRlLCBhZGp1c3RlZFJlbW92YWxSYW5nZSk7XG4gICAgcmV0dXJuIHJlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZSh3aXRob3V0RW50aXRpZXMsIGFkanVzdGVkUmVtb3ZhbFJhbmdlKTtcbiAgfSxcblxuICBzcGxpdEJsb2NrOiBmdW5jdGlvbiBzcGxpdEJsb2NrKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUpIHtcbiAgICB2YXIgd2l0aG91dEVudGl0aWVzID0gcmVtb3ZlRW50aXRpZXNBdEVkZ2VzKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUpO1xuICAgIHZhciB3aXRob3V0VGV4dCA9IHJlbW92ZVJhbmdlRnJvbUNvbnRlbnRTdGF0ZSh3aXRob3V0RW50aXRpZXMsIHNlbGVjdGlvblN0YXRlKTtcblxuICAgIHJldHVybiBzcGxpdEJsb2NrSW5Db250ZW50U3RhdGUod2l0aG91dFRleHQsIHdpdGhvdXRUZXh0LmdldFNlbGVjdGlvbkFmdGVyKCkpO1xuICB9LFxuXG4gIGFwcGx5SW5saW5lU3R5bGU6IGZ1bmN0aW9uIGFwcGx5SW5saW5lU3R5bGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgaW5saW5lU3R5bGUpIHtcbiAgICByZXR1cm4gQ29udGVudFN0YXRlSW5saW5lU3R5bGUuYWRkKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGlubGluZVN0eWxlKTtcbiAgfSxcblxuICByZW1vdmVJbmxpbmVTdHlsZTogZnVuY3Rpb24gcmVtb3ZlSW5saW5lU3R5bGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgaW5saW5lU3R5bGUpIHtcbiAgICByZXR1cm4gQ29udGVudFN0YXRlSW5saW5lU3R5bGUucmVtb3ZlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGlubGluZVN0eWxlKTtcbiAgfSxcblxuICBzZXRCbG9ja1R5cGU6IGZ1bmN0aW9uIHNldEJsb2NrVHlwZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBibG9ja1R5cGUpIHtcbiAgICByZXR1cm4gbW9kaWZ5QmxvY2tGb3JDb250ZW50U3RhdGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2UoeyB0eXBlOiBibG9ja1R5cGUsIGRlcHRoOiAwIH0pO1xuICAgIH0pO1xuICB9LFxuXG4gIHNldEJsb2NrRGF0YTogZnVuY3Rpb24gc2V0QmxvY2tEYXRhKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGJsb2NrRGF0YSkge1xuICAgIHJldHVybiBtb2RpZnlCbG9ja0ZvckNvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7IGRhdGE6IGJsb2NrRGF0YSB9KTtcbiAgICB9KTtcbiAgfSxcblxuICBtZXJnZUJsb2NrRGF0YTogZnVuY3Rpb24gbWVyZ2VCbG9ja0RhdGEoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgYmxvY2tEYXRhKSB7XG4gICAgcmV0dXJuIG1vZGlmeUJsb2NrRm9yQ29udGVudFN0YXRlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHsgZGF0YTogYmxvY2suZ2V0RGF0YSgpLm1lcmdlKGJsb2NrRGF0YSkgfSk7XG4gICAgfSk7XG4gIH0sXG5cbiAgYXBwbHlFbnRpdHk6IGZ1bmN0aW9uIGFwcGx5RW50aXR5KGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGVudGl0eUtleSkge1xuICAgIHZhciB3aXRob3V0RW50aXRpZXMgPSByZW1vdmVFbnRpdGllc0F0RWRnZXMoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSk7XG4gICAgcmV0dXJuIGFwcGx5RW50aXR5VG9Db250ZW50U3RhdGUod2l0aG91dEVudGl0aWVzLCBzZWxlY3Rpb25TdGF0ZSwgZW50aXR5S2V5KTtcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBEcmFmdE1vZGlmaWVyOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFoSEE7QUFtSEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftModifier.js
