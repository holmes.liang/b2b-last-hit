__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTwoToneColor", function() { return setTwoToneColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTwoToneColor", function() { return getTwoToneColor; });
/* harmony import */ var _ant_design_icons_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ant-design/icons-react */ "./node_modules/@ant-design/icons-react/es/index.js");

function setTwoToneColor(primaryColor) {
  return _ant_design_icons_react__WEBPACK_IMPORTED_MODULE_0__["default"].setTwoToneColors({
    primaryColor: primaryColor
  });
}
function getTwoToneColor() {
  var colors = _ant_design_icons_react__WEBPACK_IMPORTED_MODULE_0__["default"].getTwoToneColors();
  return colors.primaryColor;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pY29uL3R3b1RvbmVQcmltYXJ5Q29sb3IuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2ljb24vdHdvVG9uZVByaW1hcnlDb2xvci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3RJY29uIGZyb20gJ0BhbnQtZGVzaWduL2ljb25zLXJlYWN0JztcbmV4cG9ydCBmdW5jdGlvbiBzZXRUd29Ub25lQ29sb3IocHJpbWFyeUNvbG9yKSB7XG4gICAgcmV0dXJuIFJlYWN0SWNvbi5zZXRUd29Ub25lQ29sb3JzKHtcbiAgICAgICAgcHJpbWFyeUNvbG9yLFxuICAgIH0pO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldFR3b1RvbmVDb2xvcigpIHtcbiAgICBjb25zdCBjb2xvcnMgPSBSZWFjdEljb24uZ2V0VHdvVG9uZUNvbG9ycygpO1xuICAgIHJldHVybiBjb2xvcnMucHJpbWFyeUNvbG9yO1xufVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/icon/twoTonePrimaryColor.js
