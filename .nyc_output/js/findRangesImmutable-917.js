/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule findRangesImmutable
 * @format
 * 
 */

/**
 * Search through an array to find contiguous stretches of elements that
 * match a specified filter function.
 *
 * When ranges are found, execute a specified `found` function to supply
 * the values to the caller.
 */

function findRangesImmutable(haystack, areEqualFn, filterFn, foundFn) {
  if (!haystack.size) {
    return;
  }

  var cursor = 0;
  haystack.reduce(function (value, nextValue, nextIndex) {
    if (!areEqualFn(value, nextValue)) {
      if (filterFn(value)) {
        foundFn(cursor, nextIndex);
      }

      cursor = nextIndex;
    }

    return nextValue;
  });
  filterFn(haystack.last()) && foundFn(cursor, haystack.count());
}

module.exports = findRangesImmutable;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2ZpbmRSYW5nZXNJbW11dGFibGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZmluZFJhbmdlc0ltbXV0YWJsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGZpbmRSYW5nZXNJbW11dGFibGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBTZWFyY2ggdGhyb3VnaCBhbiBhcnJheSB0byBmaW5kIGNvbnRpZ3VvdXMgc3RyZXRjaGVzIG9mIGVsZW1lbnRzIHRoYXRcbiAqIG1hdGNoIGEgc3BlY2lmaWVkIGZpbHRlciBmdW5jdGlvbi5cbiAqXG4gKiBXaGVuIHJhbmdlcyBhcmUgZm91bmQsIGV4ZWN1dGUgYSBzcGVjaWZpZWQgYGZvdW5kYCBmdW5jdGlvbiB0byBzdXBwbHlcbiAqIHRoZSB2YWx1ZXMgdG8gdGhlIGNhbGxlci5cbiAqL1xuZnVuY3Rpb24gZmluZFJhbmdlc0ltbXV0YWJsZShoYXlzdGFjaywgYXJlRXF1YWxGbiwgZmlsdGVyRm4sIGZvdW5kRm4pIHtcbiAgaWYgKCFoYXlzdGFjay5zaXplKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGN1cnNvciA9IDA7XG5cbiAgaGF5c3RhY2sucmVkdWNlKGZ1bmN0aW9uICh2YWx1ZSwgbmV4dFZhbHVlLCBuZXh0SW5kZXgpIHtcbiAgICBpZiAoIWFyZUVxdWFsRm4odmFsdWUsIG5leHRWYWx1ZSkpIHtcbiAgICAgIGlmIChmaWx0ZXJGbih2YWx1ZSkpIHtcbiAgICAgICAgZm91bmRGbihjdXJzb3IsIG5leHRJbmRleCk7XG4gICAgICB9XG4gICAgICBjdXJzb3IgPSBuZXh0SW5kZXg7XG4gICAgfVxuICAgIHJldHVybiBuZXh0VmFsdWU7XG4gIH0pO1xuXG4gIGZpbHRlckZuKGhheXN0YWNrLmxhc3QoKSkgJiYgZm91bmRGbihjdXJzb3IsIGhheXN0YWNrLmNvdW50KCkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZpbmRSYW5nZXNJbW11dGFibGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFFQTs7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/findRangesImmutable.js
