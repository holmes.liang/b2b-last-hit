/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorDragHandler
 * @format
 * 
 */


var DataTransfer = __webpack_require__(/*! fbjs/lib/DataTransfer */ "./node_modules/fbjs/lib/DataTransfer.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var findAncestorOffsetKey = __webpack_require__(/*! ./findAncestorOffsetKey */ "./node_modules/draft-js/lib/findAncestorOffsetKey.js");

var getTextContentFromFiles = __webpack_require__(/*! ./getTextContentFromFiles */ "./node_modules/draft-js/lib/getTextContentFromFiles.js");

var getUpdatedSelectionState = __webpack_require__(/*! ./getUpdatedSelectionState */ "./node_modules/draft-js/lib/getUpdatedSelectionState.js");

var isEventHandled = __webpack_require__(/*! ./isEventHandled */ "./node_modules/draft-js/lib/isEventHandled.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");
/**
 * Get a SelectionState for the supplied mouse event.
 */


function getSelectionForEvent(event, editorState) {
  var node = null;
  var offset = null;

  if (typeof document.caretRangeFromPoint === 'function') {
    var dropRange = document.caretRangeFromPoint(event.x, event.y);
    node = dropRange.startContainer;
    offset = dropRange.startOffset;
  } else if (event.rangeParent) {
    node = event.rangeParent;
    offset = event.rangeOffset;
  } else {
    return null;
  }

  node = nullthrows(node);
  offset = nullthrows(offset);
  var offsetKey = nullthrows(findAncestorOffsetKey(node));
  return getUpdatedSelectionState(editorState, offsetKey, offset, offsetKey, offset);
}

var DraftEditorDragHandler = {
  /**
   * Drag originating from input terminated.
   */
  onDragEnd: function onDragEnd(editor) {
    editor.exitCurrentMode();
  },

  /**
   * Handle data being dropped.
   */
  onDrop: function onDrop(editor, e) {
    var data = new DataTransfer(e.nativeEvent.dataTransfer);
    var editorState = editor._latestEditorState;
    var dropSelection = getSelectionForEvent(e.nativeEvent, editorState);
    e.preventDefault();
    editor.exitCurrentMode();

    if (dropSelection == null) {
      return;
    }

    var files = data.getFiles();

    if (files.length > 0) {
      if (editor.props.handleDroppedFiles && isEventHandled(editor.props.handleDroppedFiles(dropSelection, files))) {
        return;
      }

      getTextContentFromFiles(files, function (fileText) {
        fileText && editor.update(insertTextAtSelection(editorState, dropSelection, fileText));
      });
      return;
    }

    var dragType = editor._internalDrag ? 'internal' : 'external';

    if (editor.props.handleDrop && isEventHandled(editor.props.handleDrop(dropSelection, data, dragType))) {
      return;
    }

    if (editor._internalDrag) {
      editor.update(moveText(editorState, dropSelection));
      return;
    }

    editor.update(insertTextAtSelection(editorState, dropSelection, data.getText()));
  }
};

function moveText(editorState, targetSelection) {
  var newContentState = DraftModifier.moveText(editorState.getCurrentContent(), editorState.getSelection(), targetSelection);
  return EditorState.push(editorState, newContentState, 'insert-fragment');
}
/**
 * Insert text at a specified selection.
 */


function insertTextAtSelection(editorState, selection, text) {
  var newContentState = DraftModifier.insertText(editorState.getCurrentContent(), selection, text, editorState.getCurrentInlineStyle());
  return EditorState.push(editorState, newContentState, 'insert-fragment');
}

module.exports = DraftEditorDragHandler;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yRHJhZ0hhbmRsZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFZGl0b3JEcmFnSGFuZGxlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RWRpdG9yRHJhZ0hhbmRsZXJcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERhdGFUcmFuc2ZlciA9IHJlcXVpcmUoJ2ZianMvbGliL0RhdGFUcmFuc2ZlcicpO1xudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcblxudmFyIGZpbmRBbmNlc3Rvck9mZnNldEtleSA9IHJlcXVpcmUoJy4vZmluZEFuY2VzdG9yT2Zmc2V0S2V5Jyk7XG52YXIgZ2V0VGV4dENvbnRlbnRGcm9tRmlsZXMgPSByZXF1aXJlKCcuL2dldFRleHRDb250ZW50RnJvbUZpbGVzJyk7XG52YXIgZ2V0VXBkYXRlZFNlbGVjdGlvblN0YXRlID0gcmVxdWlyZSgnLi9nZXRVcGRhdGVkU2VsZWN0aW9uU3RhdGUnKTtcbnZhciBpc0V2ZW50SGFuZGxlZCA9IHJlcXVpcmUoJy4vaXNFdmVudEhhbmRsZWQnKTtcbnZhciBudWxsdGhyb3dzID0gcmVxdWlyZSgnZmJqcy9saWIvbnVsbHRocm93cycpO1xuXG4vKipcbiAqIEdldCBhIFNlbGVjdGlvblN0YXRlIGZvciB0aGUgc3VwcGxpZWQgbW91c2UgZXZlbnQuXG4gKi9cbmZ1bmN0aW9uIGdldFNlbGVjdGlvbkZvckV2ZW50KGV2ZW50LCBlZGl0b3JTdGF0ZSkge1xuICB2YXIgbm9kZSA9IG51bGw7XG4gIHZhciBvZmZzZXQgPSBudWxsO1xuXG4gIGlmICh0eXBlb2YgZG9jdW1lbnQuY2FyZXRSYW5nZUZyb21Qb2ludCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHZhciBkcm9wUmFuZ2UgPSBkb2N1bWVudC5jYXJldFJhbmdlRnJvbVBvaW50KGV2ZW50LngsIGV2ZW50LnkpO1xuICAgIG5vZGUgPSBkcm9wUmFuZ2Uuc3RhcnRDb250YWluZXI7XG4gICAgb2Zmc2V0ID0gZHJvcFJhbmdlLnN0YXJ0T2Zmc2V0O1xuICB9IGVsc2UgaWYgKGV2ZW50LnJhbmdlUGFyZW50KSB7XG4gICAgbm9kZSA9IGV2ZW50LnJhbmdlUGFyZW50O1xuICAgIG9mZnNldCA9IGV2ZW50LnJhbmdlT2Zmc2V0O1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgbm9kZSA9IG51bGx0aHJvd3Mobm9kZSk7XG4gIG9mZnNldCA9IG51bGx0aHJvd3Mob2Zmc2V0KTtcbiAgdmFyIG9mZnNldEtleSA9IG51bGx0aHJvd3MoZmluZEFuY2VzdG9yT2Zmc2V0S2V5KG5vZGUpKTtcblxuICByZXR1cm4gZ2V0VXBkYXRlZFNlbGVjdGlvblN0YXRlKGVkaXRvclN0YXRlLCBvZmZzZXRLZXksIG9mZnNldCwgb2Zmc2V0S2V5LCBvZmZzZXQpO1xufVxuXG52YXIgRHJhZnRFZGl0b3JEcmFnSGFuZGxlciA9IHtcbiAgLyoqXG4gICAqIERyYWcgb3JpZ2luYXRpbmcgZnJvbSBpbnB1dCB0ZXJtaW5hdGVkLlxuICAgKi9cbiAgb25EcmFnRW5kOiBmdW5jdGlvbiBvbkRyYWdFbmQoZWRpdG9yKSB7XG4gICAgZWRpdG9yLmV4aXRDdXJyZW50TW9kZSgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBIYW5kbGUgZGF0YSBiZWluZyBkcm9wcGVkLlxuICAgKi9cbiAgb25Ecm9wOiBmdW5jdGlvbiBvbkRyb3AoZWRpdG9yLCBlKSB7XG4gICAgdmFyIGRhdGEgPSBuZXcgRGF0YVRyYW5zZmVyKGUubmF0aXZlRXZlbnQuZGF0YVRyYW5zZmVyKTtcblxuICAgIHZhciBlZGl0b3JTdGF0ZSA9IGVkaXRvci5fbGF0ZXN0RWRpdG9yU3RhdGU7XG4gICAgdmFyIGRyb3BTZWxlY3Rpb24gPSBnZXRTZWxlY3Rpb25Gb3JFdmVudChlLm5hdGl2ZUV2ZW50LCBlZGl0b3JTdGF0ZSk7XG5cbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgZWRpdG9yLmV4aXRDdXJyZW50TW9kZSgpO1xuXG4gICAgaWYgKGRyb3BTZWxlY3Rpb24gPT0gbnVsbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBmaWxlcyA9IGRhdGEuZ2V0RmlsZXMoKTtcbiAgICBpZiAoZmlsZXMubGVuZ3RoID4gMCkge1xuICAgICAgaWYgKGVkaXRvci5wcm9wcy5oYW5kbGVEcm9wcGVkRmlsZXMgJiYgaXNFdmVudEhhbmRsZWQoZWRpdG9yLnByb3BzLmhhbmRsZURyb3BwZWRGaWxlcyhkcm9wU2VsZWN0aW9uLCBmaWxlcykpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgZ2V0VGV4dENvbnRlbnRGcm9tRmlsZXMoZmlsZXMsIGZ1bmN0aW9uIChmaWxlVGV4dCkge1xuICAgICAgICBmaWxlVGV4dCAmJiBlZGl0b3IudXBkYXRlKGluc2VydFRleHRBdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgZHJvcFNlbGVjdGlvbiwgZmlsZVRleHQpKTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBkcmFnVHlwZSA9IGVkaXRvci5faW50ZXJuYWxEcmFnID8gJ2ludGVybmFsJyA6ICdleHRlcm5hbCc7XG4gICAgaWYgKGVkaXRvci5wcm9wcy5oYW5kbGVEcm9wICYmIGlzRXZlbnRIYW5kbGVkKGVkaXRvci5wcm9wcy5oYW5kbGVEcm9wKGRyb3BTZWxlY3Rpb24sIGRhdGEsIGRyYWdUeXBlKSkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoZWRpdG9yLl9pbnRlcm5hbERyYWcpIHtcbiAgICAgIGVkaXRvci51cGRhdGUobW92ZVRleHQoZWRpdG9yU3RhdGUsIGRyb3BTZWxlY3Rpb24pKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBlZGl0b3IudXBkYXRlKGluc2VydFRleHRBdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgZHJvcFNlbGVjdGlvbiwgZGF0YS5nZXRUZXh0KCkpKTtcbiAgfVxufTtcblxuZnVuY3Rpb24gbW92ZVRleHQoZWRpdG9yU3RhdGUsIHRhcmdldFNlbGVjdGlvbikge1xuICB2YXIgbmV3Q29udGVudFN0YXRlID0gRHJhZnRNb2RpZmllci5tb3ZlVGV4dChlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSwgdGFyZ2V0U2VsZWN0aW9uKTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIG5ld0NvbnRlbnRTdGF0ZSwgJ2luc2VydC1mcmFnbWVudCcpO1xufVxuXG4vKipcbiAqIEluc2VydCB0ZXh0IGF0IGEgc3BlY2lmaWVkIHNlbGVjdGlvbi5cbiAqL1xuZnVuY3Rpb24gaW5zZXJ0VGV4dEF0U2VsZWN0aW9uKGVkaXRvclN0YXRlLCBzZWxlY3Rpb24sIHRleHQpIHtcbiAgdmFyIG5ld0NvbnRlbnRTdGF0ZSA9IERyYWZ0TW9kaWZpZXIuaW5zZXJ0VGV4dChlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBzZWxlY3Rpb24sIHRleHQsIGVkaXRvclN0YXRlLmdldEN1cnJlbnRJbmxpbmVTdHlsZSgpKTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIG5ld0NvbnRlbnRTdGF0ZSwgJ2luc2VydC1mcmFnbWVudCcpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RWRpdG9yRHJhZ0hhbmRsZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0NBO0FBQ0E7QUFpREE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorDragHandler.js
