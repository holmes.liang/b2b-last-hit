__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return uid; });
var now = +new Date();
var index = 0;
function uid() {
  return "rc-upload-" + now + "-" + ++index;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXBsb2FkL2VzL3VpZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXVwbG9hZC9lcy91aWQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIG5vdyA9ICtuZXcgRGF0ZSgpO1xudmFyIGluZGV4ID0gMDtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gdWlkKCkge1xuICByZXR1cm4gXCJyYy11cGxvYWQtXCIgKyBub3cgKyBcIi1cIiArICsraW5kZXg7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-upload/es/uid.js
