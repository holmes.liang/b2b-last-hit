

Object.defineProperty(exports, '__esModule', {
  value: true
});
/**
 * @desc 解决浮动运算问题，避免小数点后产生多位数和计算精度损失。
 * 问题示例：2.3 + 2.4 = 4.699999999999999，1.0 - 0.9 = 0.09999999999999998
 */

/**
 * 把错误的数据转正
 * strip(0.09999999999999998)=0.1
 */

function strip(num, precision) {
  if (precision === void 0) {
    precision = 12;
  }

  return +parseFloat(num.toPrecision(precision));
}
/**
 * Return digits length of a number
 * @param {*number} num Input number
 */


function digitLength(num) {
  // Get digit length of e
  var eSplit = num.toString().split(/[eE]/);
  var len = (eSplit[0].split('.')[1] || '').length - +(eSplit[1] || 0);
  return len > 0 ? len : 0;
}
/**
 * 把小数转成整数，支持科学计数法。如果是小数则放大成整数
 * @param {*number} num 输入数
 */


function float2Fixed(num) {
  if (num.toString().indexOf('e') === -1) {
    return Number(num.toString().replace('.', ''));
  }

  var dLen = digitLength(num);
  return dLen > 0 ? strip(num * Math.pow(10, dLen)) : num;
}
/**
 * 检测数字是否越界，如果越界给出提示
 * @param {*number} num 输入数
 */


function checkBoundary(num) {
  if (_boundaryCheckingState) {
    if (num > Number.MAX_SAFE_INTEGER || num < Number.MIN_SAFE_INTEGER) {
      console.warn(num + " is beyond boundary when transfer to integer, the results may not be accurate");
    }
  }
}
/**
 * 精确乘法
 */


function times(num1, num2) {
  var others = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    others[_i - 2] = arguments[_i];
  }

  if (others.length > 0) {
    return times.apply(void 0, [times(num1, num2), others[0]].concat(others.slice(1)));
  }

  var num1Changed = float2Fixed(num1);
  var num2Changed = float2Fixed(num2);
  var baseNum = digitLength(num1) + digitLength(num2);
  var leftValue = num1Changed * num2Changed;
  checkBoundary(leftValue);
  return leftValue / Math.pow(10, baseNum);
}
/**
 * 精确加法
 */


function plus(num1, num2) {
  var others = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    others[_i - 2] = arguments[_i];
  }

  if (others.length > 0) {
    return plus.apply(void 0, [plus(num1, num2), others[0]].concat(others.slice(1)));
  }

  var baseNum = Math.pow(10, Math.max(digitLength(num1), digitLength(num2)));
  return (times(num1, baseNum) + times(num2, baseNum)) / baseNum;
}
/**
 * 精确减法
 */


function minus(num1, num2) {
  var others = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    others[_i - 2] = arguments[_i];
  }

  if (others.length > 0) {
    return minus.apply(void 0, [minus(num1, num2), others[0]].concat(others.slice(1)));
  }

  var baseNum = Math.pow(10, Math.max(digitLength(num1), digitLength(num2)));
  return (times(num1, baseNum) - times(num2, baseNum)) / baseNum;
}
/**
 * 精确除法
 */


function divide(num1, num2) {
  var others = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    others[_i - 2] = arguments[_i];
  }

  if (others.length > 0) {
    return divide.apply(void 0, [divide(num1, num2), others[0]].concat(others.slice(1)));
  }

  var num1Changed = float2Fixed(num1);
  var num2Changed = float2Fixed(num2);
  checkBoundary(num1Changed);
  checkBoundary(num2Changed); // fix: 类似 10 ** -4 为 0.00009999999999999999，strip 修正

  return times(num1Changed / num2Changed, strip(Math.pow(10, digitLength(num2) - digitLength(num1))));
}
/**
 * 四舍五入
 */


function round(num, ratio) {
  var base = Math.pow(10, ratio);
  return divide(Math.round(times(num, base)), base);
}

var _boundaryCheckingState = true;
/**
 * 是否进行边界检查，默认开启
 * @param flag 标记开关，true 为开启，false 为关闭，默认为 true
 */

function enableBoundaryChecking(flag) {
  if (flag === void 0) {
    flag = true;
  }

  _boundaryCheckingState = flag;
}

var index = {
  strip: strip,
  plus: plus,
  minus: minus,
  times: times,
  divide: divide,
  round: round,
  digitLength: digitLength,
  float2Fixed: float2Fixed,
  enableBoundaryChecking: enableBoundaryChecking
};
exports.strip = strip;
exports.plus = plus;
exports.minus = minus;
exports.times = times;
exports.divide = divide;
exports.round = round;
exports.digitLength = digitLength;
exports.float2Fixed = float2Fixed;
exports.enableBoundaryChecking = enableBoundaryChecking;
exports['default'] = index;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbnVtYmVyLXByZWNpc2lvbi9idWlsZC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL251bWJlci1wcmVjaXNpb24vYnVpbGQvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuXG4vKipcclxuICogQGRlc2Mg6Kej5Yaz5rWu5Yqo6L+Q566X6Zeu6aKY77yM6YG/5YWN5bCP5pWw54K55ZCO5Lqn55Sf5aSa5L2N5pWw5ZKM6K6h566X57K+5bqm5o2f5aSx44CCXHJcbiAqIOmXrumimOekuuS+i++8mjIuMyArIDIuNCA9IDQuNjk5OTk5OTk5OTk5OTk577yMMS4wIC0gMC45ID0gMC4wOTk5OTk5OTk5OTk5OTk5OFxyXG4gKi9cclxuLyoqXHJcbiAqIOaKiumUmeivr+eahOaVsOaNrui9rOato1xyXG4gKiBzdHJpcCgwLjA5OTk5OTk5OTk5OTk5OTk4KT0wLjFcclxuICovXHJcbmZ1bmN0aW9uIHN0cmlwKG51bSwgcHJlY2lzaW9uKSB7XHJcbiAgICBpZiAocHJlY2lzaW9uID09PSB2b2lkIDApIHsgcHJlY2lzaW9uID0gMTI7IH1cclxuICAgIHJldHVybiArcGFyc2VGbG9hdChudW0udG9QcmVjaXNpb24ocHJlY2lzaW9uKSk7XHJcbn1cclxuLyoqXHJcbiAqIFJldHVybiBkaWdpdHMgbGVuZ3RoIG9mIGEgbnVtYmVyXHJcbiAqIEBwYXJhbSB7Km51bWJlcn0gbnVtIElucHV0IG51bWJlclxyXG4gKi9cclxuZnVuY3Rpb24gZGlnaXRMZW5ndGgobnVtKSB7XHJcbiAgICAvLyBHZXQgZGlnaXQgbGVuZ3RoIG9mIGVcclxuICAgIHZhciBlU3BsaXQgPSBudW0udG9TdHJpbmcoKS5zcGxpdCgvW2VFXS8pO1xyXG4gICAgdmFyIGxlbiA9IChlU3BsaXRbMF0uc3BsaXQoJy4nKVsxXSB8fCAnJykubGVuZ3RoIC0gKCsoZVNwbGl0WzFdIHx8IDApKTtcclxuICAgIHJldHVybiBsZW4gPiAwID8gbGVuIDogMDtcclxufVxyXG4vKipcclxuICog5oqK5bCP5pWw6L2s5oiQ5pW05pWw77yM5pSv5oyB56eR5a2m6K6h5pWw5rOV44CC5aaC5p6c5piv5bCP5pWw5YiZ5pS+5aSn5oiQ5pW05pWwXHJcbiAqIEBwYXJhbSB7Km51bWJlcn0gbnVtIOi+k+WFpeaVsFxyXG4gKi9cclxuZnVuY3Rpb24gZmxvYXQyRml4ZWQobnVtKSB7XHJcbiAgICBpZiAobnVtLnRvU3RyaW5nKCkuaW5kZXhPZignZScpID09PSAtMSkge1xyXG4gICAgICAgIHJldHVybiBOdW1iZXIobnVtLnRvU3RyaW5nKCkucmVwbGFjZSgnLicsICcnKSk7XHJcbiAgICB9XHJcbiAgICB2YXIgZExlbiA9IGRpZ2l0TGVuZ3RoKG51bSk7XHJcbiAgICByZXR1cm4gZExlbiA+IDAgPyBzdHJpcChudW0gKiBNYXRoLnBvdygxMCwgZExlbikpIDogbnVtO1xyXG59XHJcbi8qKlxyXG4gKiDmo4DmtYvmlbDlrZfmmK/lkKbotornlYzvvIzlpoLmnpzotornlYznu5nlh7rmj5DnpLpcclxuICogQHBhcmFtIHsqbnVtYmVyfSBudW0g6L6T5YWl5pWwXHJcbiAqL1xyXG5mdW5jdGlvbiBjaGVja0JvdW5kYXJ5KG51bSkge1xyXG4gICAgaWYgKF9ib3VuZGFyeUNoZWNraW5nU3RhdGUpIHtcclxuICAgICAgICBpZiAobnVtID4gTnVtYmVyLk1BWF9TQUZFX0lOVEVHRVIgfHwgbnVtIDwgTnVtYmVyLk1JTl9TQUZFX0lOVEVHRVIpIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKG51bSArIFwiIGlzIGJleW9uZCBib3VuZGFyeSB3aGVuIHRyYW5zZmVyIHRvIGludGVnZXIsIHRoZSByZXN1bHRzIG1heSBub3QgYmUgYWNjdXJhdGVcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi8qKlxyXG4gKiDnsr7noa7kuZjms5VcclxuICovXHJcbmZ1bmN0aW9uIHRpbWVzKG51bTEsIG51bTIpIHtcclxuICAgIHZhciBvdGhlcnMgPSBbXTtcclxuICAgIGZvciAodmFyIF9pID0gMjsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XHJcbiAgICAgICAgb3RoZXJzW19pIC0gMl0gPSBhcmd1bWVudHNbX2ldO1xyXG4gICAgfVxyXG4gICAgaWYgKG90aGVycy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgcmV0dXJuIHRpbWVzLmFwcGx5KHZvaWQgMCwgW3RpbWVzKG51bTEsIG51bTIpLCBvdGhlcnNbMF1dLmNvbmNhdChvdGhlcnMuc2xpY2UoMSkpKTtcclxuICAgIH1cclxuICAgIHZhciBudW0xQ2hhbmdlZCA9IGZsb2F0MkZpeGVkKG51bTEpO1xyXG4gICAgdmFyIG51bTJDaGFuZ2VkID0gZmxvYXQyRml4ZWQobnVtMik7XHJcbiAgICB2YXIgYmFzZU51bSA9IGRpZ2l0TGVuZ3RoKG51bTEpICsgZGlnaXRMZW5ndGgobnVtMik7XHJcbiAgICB2YXIgbGVmdFZhbHVlID0gbnVtMUNoYW5nZWQgKiBudW0yQ2hhbmdlZDtcclxuICAgIGNoZWNrQm91bmRhcnkobGVmdFZhbHVlKTtcclxuICAgIHJldHVybiBsZWZ0VmFsdWUgLyBNYXRoLnBvdygxMCwgYmFzZU51bSk7XHJcbn1cclxuLyoqXHJcbiAqIOeyvuehruWKoOazlVxyXG4gKi9cclxuZnVuY3Rpb24gcGx1cyhudW0xLCBudW0yKSB7XHJcbiAgICB2YXIgb3RoZXJzID0gW107XHJcbiAgICBmb3IgKHZhciBfaSA9IDI7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgIG90aGVyc1tfaSAtIDJdID0gYXJndW1lbnRzW19pXTtcclxuICAgIH1cclxuICAgIGlmIChvdGhlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHJldHVybiBwbHVzLmFwcGx5KHZvaWQgMCwgW3BsdXMobnVtMSwgbnVtMiksIG90aGVyc1swXV0uY29uY2F0KG90aGVycy5zbGljZSgxKSkpO1xyXG4gICAgfVxyXG4gICAgdmFyIGJhc2VOdW0gPSBNYXRoLnBvdygxMCwgTWF0aC5tYXgoZGlnaXRMZW5ndGgobnVtMSksIGRpZ2l0TGVuZ3RoKG51bTIpKSk7XHJcbiAgICByZXR1cm4gKHRpbWVzKG51bTEsIGJhc2VOdW0pICsgdGltZXMobnVtMiwgYmFzZU51bSkpIC8gYmFzZU51bTtcclxufVxyXG4vKipcclxuICog57K+56Gu5YeP5rOVXHJcbiAqL1xyXG5mdW5jdGlvbiBtaW51cyhudW0xLCBudW0yKSB7XHJcbiAgICB2YXIgb3RoZXJzID0gW107XHJcbiAgICBmb3IgKHZhciBfaSA9IDI7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgIG90aGVyc1tfaSAtIDJdID0gYXJndW1lbnRzW19pXTtcclxuICAgIH1cclxuICAgIGlmIChvdGhlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHJldHVybiBtaW51cy5hcHBseSh2b2lkIDAsIFttaW51cyhudW0xLCBudW0yKSwgb3RoZXJzWzBdXS5jb25jYXQob3RoZXJzLnNsaWNlKDEpKSk7XHJcbiAgICB9XHJcbiAgICB2YXIgYmFzZU51bSA9IE1hdGgucG93KDEwLCBNYXRoLm1heChkaWdpdExlbmd0aChudW0xKSwgZGlnaXRMZW5ndGgobnVtMikpKTtcclxuICAgIHJldHVybiAodGltZXMobnVtMSwgYmFzZU51bSkgLSB0aW1lcyhudW0yLCBiYXNlTnVtKSkgLyBiYXNlTnVtO1xyXG59XHJcbi8qKlxyXG4gKiDnsr7noa7pmaTms5VcclxuICovXHJcbmZ1bmN0aW9uIGRpdmlkZShudW0xLCBudW0yKSB7XHJcbiAgICB2YXIgb3RoZXJzID0gW107XHJcbiAgICBmb3IgKHZhciBfaSA9IDI7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgIG90aGVyc1tfaSAtIDJdID0gYXJndW1lbnRzW19pXTtcclxuICAgIH1cclxuICAgIGlmIChvdGhlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHJldHVybiBkaXZpZGUuYXBwbHkodm9pZCAwLCBbZGl2aWRlKG51bTEsIG51bTIpLCBvdGhlcnNbMF1dLmNvbmNhdChvdGhlcnMuc2xpY2UoMSkpKTtcclxuICAgIH1cclxuICAgIHZhciBudW0xQ2hhbmdlZCA9IGZsb2F0MkZpeGVkKG51bTEpO1xyXG4gICAgdmFyIG51bTJDaGFuZ2VkID0gZmxvYXQyRml4ZWQobnVtMik7XHJcbiAgICBjaGVja0JvdW5kYXJ5KG51bTFDaGFuZ2VkKTtcclxuICAgIGNoZWNrQm91bmRhcnkobnVtMkNoYW5nZWQpO1xyXG4gICAgLy8gZml4OiDnsbvkvLwgMTAgKiogLTQg5Li6IDAuMDAwMDk5OTk5OTk5OTk5OTk5OTnvvIxzdHJpcCDkv67mraNcclxuICAgIHJldHVybiB0aW1lcygobnVtMUNoYW5nZWQgLyBudW0yQ2hhbmdlZCksIHN0cmlwKE1hdGgucG93KDEwLCBkaWdpdExlbmd0aChudW0yKSAtIGRpZ2l0TGVuZ3RoKG51bTEpKSkpO1xyXG59XHJcbi8qKlxyXG4gKiDlm5voiI3kupTlhaVcclxuICovXHJcbmZ1bmN0aW9uIHJvdW5kKG51bSwgcmF0aW8pIHtcclxuICAgIHZhciBiYXNlID0gTWF0aC5wb3coMTAsIHJhdGlvKTtcclxuICAgIHJldHVybiBkaXZpZGUoTWF0aC5yb3VuZCh0aW1lcyhudW0sIGJhc2UpKSwgYmFzZSk7XHJcbn1cclxudmFyIF9ib3VuZGFyeUNoZWNraW5nU3RhdGUgPSB0cnVlO1xyXG4vKipcclxuICog5piv5ZCm6L+b6KGM6L6555WM5qOA5p+l77yM6buY6K6k5byA5ZCvXHJcbiAqIEBwYXJhbSBmbGFnIOagh+iusOW8gOWFs++8jHRydWUg5Li65byA5ZCv77yMZmFsc2Ug5Li65YWz6Zet77yM6buY6K6k5Li6IHRydWVcclxuICovXHJcbmZ1bmN0aW9uIGVuYWJsZUJvdW5kYXJ5Q2hlY2tpbmcoZmxhZykge1xyXG4gICAgaWYgKGZsYWcgPT09IHZvaWQgMCkgeyBmbGFnID0gdHJ1ZTsgfVxyXG4gICAgX2JvdW5kYXJ5Q2hlY2tpbmdTdGF0ZSA9IGZsYWc7XHJcbn1cclxudmFyIGluZGV4ID0geyBzdHJpcDogc3RyaXAsIHBsdXM6IHBsdXMsIG1pbnVzOiBtaW51cywgdGltZXM6IHRpbWVzLCBkaXZpZGU6IGRpdmlkZSwgcm91bmQ6IHJvdW5kLCBkaWdpdExlbmd0aDogZGlnaXRMZW5ndGgsIGZsb2F0MkZpeGVkOiBmbG9hdDJGaXhlZCwgZW5hYmxlQm91bmRhcnlDaGVja2luZzogZW5hYmxlQm91bmRhcnlDaGVja2luZyB9O1xuXG5leHBvcnRzLnN0cmlwID0gc3RyaXA7XG5leHBvcnRzLnBsdXMgPSBwbHVzO1xuZXhwb3J0cy5taW51cyA9IG1pbnVzO1xuZXhwb3J0cy50aW1lcyA9IHRpbWVzO1xuZXhwb3J0cy5kaXZpZGUgPSBkaXZpZGU7XG5leHBvcnRzLnJvdW5kID0gcm91bmQ7XG5leHBvcnRzLmRpZ2l0TGVuZ3RoID0gZGlnaXRMZW5ndGg7XG5leHBvcnRzLmZsb2F0MkZpeGVkID0gZmxvYXQyRml4ZWQ7XG5leHBvcnRzLmVuYWJsZUJvdW5kYXJ5Q2hlY2tpbmcgPSBlbmFibGVCb3VuZGFyeUNoZWNraW5nO1xuZXhwb3J0c1snZGVmYXVsdCddID0gaW5kZXg7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTs7Ozs7QUFJQTs7Ozs7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7O0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/number-precision/build/index.js
