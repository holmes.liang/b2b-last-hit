var textHelper = __webpack_require__(/*! ../helper/text */ "./node_modules/zrender/lib/graphic/helper/text.js");

var BoundingRect = __webpack_require__(/*! ../../core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var _constant = __webpack_require__(/*! ../constant */ "./node_modules/zrender/lib/graphic/constant.js");

var WILL_BE_RESTORED = _constant.WILL_BE_RESTORED;
/**
 * Mixin for drawing text in a element bounding rect
 * @module zrender/mixin/RectText
 */

var tmpRect = new BoundingRect();

var RectText = function RectText() {};

RectText.prototype = {
  constructor: RectText,

  /**
   * Draw text in a rect with specified position.
   * @param  {CanvasRenderingContext2D} ctx
   * @param  {Object} rect Displayable rect
   */
  drawRectText: function drawRectText(ctx, rect) {
    var style = this.style;
    rect = style.textRect || rect; // Optimize, avoid normalize every time.

    this.__dirty && textHelper.normalizeTextStyle(style, true);
    var text = style.text; // Convert to string

    text != null && (text += '');

    if (!textHelper.needDrawText(text, style)) {
      return;
    } // FIXME
    // Do not provide prevEl to `textHelper.renderText` for ctx prop cache,
    // but use `ctx.save()` and `ctx.restore()`. Because the cache for rect
    // text propably break the cache for its host elements.


    ctx.save(); // Transform rect to view space

    var transform = this.transform;

    if (!style.transformText) {
      if (transform) {
        tmpRect.copy(rect);
        tmpRect.applyTransform(transform);
        rect = tmpRect;
      }
    } else {
      this.setTransform(ctx);
    } // transformText and textRotation can not be used at the same time.


    textHelper.renderText(this, ctx, text, style, rect, WILL_BE_RESTORED);
    ctx.restore();
  }
};
var _default = RectText;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9taXhpbi9SZWN0VGV4dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvbWl4aW4vUmVjdFRleHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHRleHRIZWxwZXIgPSByZXF1aXJlKFwiLi4vaGVscGVyL3RleHRcIik7XG5cbnZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwiLi4vLi4vY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciBfY29uc3RhbnQgPSByZXF1aXJlKFwiLi4vY29uc3RhbnRcIik7XG5cbnZhciBXSUxMX0JFX1JFU1RPUkVEID0gX2NvbnN0YW50LldJTExfQkVfUkVTVE9SRUQ7XG5cbi8qKlxuICogTWl4aW4gZm9yIGRyYXdpbmcgdGV4dCBpbiBhIGVsZW1lbnQgYm91bmRpbmcgcmVjdFxuICogQG1vZHVsZSB6cmVuZGVyL21peGluL1JlY3RUZXh0XG4gKi9cbnZhciB0bXBSZWN0ID0gbmV3IEJvdW5kaW5nUmVjdCgpO1xuXG52YXIgUmVjdFRleHQgPSBmdW5jdGlvbiAoKSB7fTtcblxuUmVjdFRleHQucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogUmVjdFRleHQsXG5cbiAgLyoqXG4gICAqIERyYXcgdGV4dCBpbiBhIHJlY3Qgd2l0aCBzcGVjaWZpZWQgcG9zaXRpb24uXG4gICAqIEBwYXJhbSAge0NhbnZhc1JlbmRlcmluZ0NvbnRleHQyRH0gY3R4XG4gICAqIEBwYXJhbSAge09iamVjdH0gcmVjdCBEaXNwbGF5YWJsZSByZWN0XG4gICAqL1xuICBkcmF3UmVjdFRleHQ6IGZ1bmN0aW9uIChjdHgsIHJlY3QpIHtcbiAgICB2YXIgc3R5bGUgPSB0aGlzLnN0eWxlO1xuICAgIHJlY3QgPSBzdHlsZS50ZXh0UmVjdCB8fCByZWN0OyAvLyBPcHRpbWl6ZSwgYXZvaWQgbm9ybWFsaXplIGV2ZXJ5IHRpbWUuXG5cbiAgICB0aGlzLl9fZGlydHkgJiYgdGV4dEhlbHBlci5ub3JtYWxpemVUZXh0U3R5bGUoc3R5bGUsIHRydWUpO1xuICAgIHZhciB0ZXh0ID0gc3R5bGUudGV4dDsgLy8gQ29udmVydCB0byBzdHJpbmdcblxuICAgIHRleHQgIT0gbnVsbCAmJiAodGV4dCArPSAnJyk7XG5cbiAgICBpZiAoIXRleHRIZWxwZXIubmVlZERyYXdUZXh0KHRleHQsIHN0eWxlKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH0gLy8gRklYTUVcbiAgICAvLyBEbyBub3QgcHJvdmlkZSBwcmV2RWwgdG8gYHRleHRIZWxwZXIucmVuZGVyVGV4dGAgZm9yIGN0eCBwcm9wIGNhY2hlLFxuICAgIC8vIGJ1dCB1c2UgYGN0eC5zYXZlKClgIGFuZCBgY3R4LnJlc3RvcmUoKWAuIEJlY2F1c2UgdGhlIGNhY2hlIGZvciByZWN0XG4gICAgLy8gdGV4dCBwcm9wYWJseSBicmVhayB0aGUgY2FjaGUgZm9yIGl0cyBob3N0IGVsZW1lbnRzLlxuXG5cbiAgICBjdHguc2F2ZSgpOyAvLyBUcmFuc2Zvcm0gcmVjdCB0byB2aWV3IHNwYWNlXG5cbiAgICB2YXIgdHJhbnNmb3JtID0gdGhpcy50cmFuc2Zvcm07XG5cbiAgICBpZiAoIXN0eWxlLnRyYW5zZm9ybVRleHQpIHtcbiAgICAgIGlmICh0cmFuc2Zvcm0pIHtcbiAgICAgICAgdG1wUmVjdC5jb3B5KHJlY3QpO1xuICAgICAgICB0bXBSZWN0LmFwcGx5VHJhbnNmb3JtKHRyYW5zZm9ybSk7XG4gICAgICAgIHJlY3QgPSB0bXBSZWN0O1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFRyYW5zZm9ybShjdHgpO1xuICAgIH0gLy8gdHJhbnNmb3JtVGV4dCBhbmQgdGV4dFJvdGF0aW9uIGNhbiBub3QgYmUgdXNlZCBhdCB0aGUgc2FtZSB0aW1lLlxuXG5cbiAgICB0ZXh0SGVscGVyLnJlbmRlclRleHQodGhpcywgY3R4LCB0ZXh0LCBzdHlsZSwgcmVjdCwgV0lMTF9CRV9SRVNUT1JFRCk7XG4gICAgY3R4LnJlc3RvcmUoKTtcbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IFJlY3RUZXh0O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMUNBO0FBNENBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/mixin/RectText.js
