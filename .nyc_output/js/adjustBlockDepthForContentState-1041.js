/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule adjustBlockDepthForContentState
 * @format
 * 
 */


function adjustBlockDepthForContentState(contentState, selectionState, adjustment, maxDepth) {
  var startKey = selectionState.getStartKey();
  var endKey = selectionState.getEndKey();
  var blockMap = contentState.getBlockMap();
  var blocks = blockMap.toSeq().skipUntil(function (_, k) {
    return k === startKey;
  }).takeUntil(function (_, k) {
    return k === endKey;
  }).concat([[endKey, blockMap.get(endKey)]]).map(function (block) {
    var depth = block.getDepth() + adjustment;
    depth = Math.max(0, Math.min(depth, maxDepth));
    return block.set('depth', depth);
  });
  blockMap = blockMap.merge(blocks);
  return contentState.merge({
    blockMap: blockMap,
    selectionBefore: selectionState,
    selectionAfter: selectionState
  });
}

module.exports = adjustBlockDepthForContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2FkanVzdEJsb2NrRGVwdGhGb3JDb250ZW50U3RhdGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvYWRqdXN0QmxvY2tEZXB0aEZvckNvbnRlbnRTdGF0ZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGFkanVzdEJsb2NrRGVwdGhGb3JDb250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gYWRqdXN0QmxvY2tEZXB0aEZvckNvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBhZGp1c3RtZW50LCBtYXhEZXB0aCkge1xuICB2YXIgc3RhcnRLZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRTdGFydEtleSgpO1xuICB2YXIgZW5kS2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kS2V5KCk7XG4gIHZhciBibG9ja01hcCA9IGNvbnRlbnRTdGF0ZS5nZXRCbG9ja01hcCgpO1xuICB2YXIgYmxvY2tzID0gYmxvY2tNYXAudG9TZXEoKS5za2lwVW50aWwoZnVuY3Rpb24gKF8sIGspIHtcbiAgICByZXR1cm4gayA9PT0gc3RhcnRLZXk7XG4gIH0pLnRha2VVbnRpbChmdW5jdGlvbiAoXywgaykge1xuICAgIHJldHVybiBrID09PSBlbmRLZXk7XG4gIH0pLmNvbmNhdChbW2VuZEtleSwgYmxvY2tNYXAuZ2V0KGVuZEtleSldXSkubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBkZXB0aCA9IGJsb2NrLmdldERlcHRoKCkgKyBhZGp1c3RtZW50O1xuICAgIGRlcHRoID0gTWF0aC5tYXgoMCwgTWF0aC5taW4oZGVwdGgsIG1heERlcHRoKSk7XG4gICAgcmV0dXJuIGJsb2NrLnNldCgnZGVwdGgnLCBkZXB0aCk7XG4gIH0pO1xuXG4gIGJsb2NrTWFwID0gYmxvY2tNYXAubWVyZ2UoYmxvY2tzKTtcblxuICByZXR1cm4gY29udGVudFN0YXRlLm1lcmdlKHtcbiAgICBibG9ja01hcDogYmxvY2tNYXAsXG4gICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGVcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYWRqdXN0QmxvY2tEZXB0aEZvckNvbnRlbnRTdGF0ZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/adjustBlockDepthForContentState.js
