var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var Style = __webpack_require__(/*! ./Style */ "./node_modules/zrender/lib/graphic/Style.js");

var Element = __webpack_require__(/*! ../Element */ "./node_modules/zrender/lib/Element.js");

var RectText = __webpack_require__(/*! ./mixin/RectText */ "./node_modules/zrender/lib/graphic/mixin/RectText.js");
/**
 * 可绘制的图形基类
 * Base class of all displayable graphic objects
 * @module zrender/graphic/Displayable
 */

/**
 * @alias module:zrender/graphic/Displayable
 * @extends module:zrender/Element
 * @extends module:zrender/graphic/mixin/RectText
 */


function Displayable(opts) {
  opts = opts || {};
  Element.call(this, opts); // Extend properties

  for (var name in opts) {
    if (opts.hasOwnProperty(name) && name !== 'style') {
      this[name] = opts[name];
    }
  }
  /**
   * @type {module:zrender/graphic/Style}
   */


  this.style = new Style(opts.style, this);
  this._rect = null; // Shapes for cascade clipping.

  this.__clipPaths = []; // FIXME Stateful must be mixined after style is setted
  // Stateful.call(this, opts);
}

Displayable.prototype = {
  constructor: Displayable,
  type: 'displayable',

  /**
   * Displayable 是否为脏，Painter 中会根据该标记判断是否需要是否需要重新绘制
   * Dirty flag. From which painter will determine if this displayable object needs brush
   * @name module:zrender/graphic/Displayable#__dirty
   * @type {boolean}
   */
  __dirty: true,

  /**
   * 图形是否可见，为true时不绘制图形，但是仍能触发鼠标事件
   * If ignore drawing of the displayable object. Mouse event will still be triggered
   * @name module:/zrender/graphic/Displayable#invisible
   * @type {boolean}
   * @default false
   */
  invisible: false,

  /**
   * @name module:/zrender/graphic/Displayable#z
   * @type {number}
   * @default 0
   */
  z: 0,

  /**
   * @name module:/zrender/graphic/Displayable#z
   * @type {number}
   * @default 0
   */
  z2: 0,

  /**
   * z层level，决定绘画在哪层canvas中
   * @name module:/zrender/graphic/Displayable#zlevel
   * @type {number}
   * @default 0
   */
  zlevel: 0,

  /**
   * 是否可拖拽
   * @name module:/zrender/graphic/Displayable#draggable
   * @type {boolean}
   * @default false
   */
  draggable: false,

  /**
   * 是否正在拖拽
   * @name module:/zrender/graphic/Displayable#draggable
   * @type {boolean}
   * @default false
   */
  dragging: false,

  /**
   * 是否相应鼠标事件
   * @name module:/zrender/graphic/Displayable#silent
   * @type {boolean}
   * @default false
   */
  silent: false,

  /**
   * If enable culling
   * @type {boolean}
   * @default false
   */
  culling: false,

  /**
   * Mouse cursor when hovered
   * @name module:/zrender/graphic/Displayable#cursor
   * @type {string}
   */
  cursor: 'pointer',

  /**
   * If hover area is bounding rect
   * @name module:/zrender/graphic/Displayable#rectHover
   * @type {string}
   */
  rectHover: false,

  /**
   * Render the element progressively when the value >= 0,
   * usefull for large data.
   * @type {boolean}
   */
  progressive: false,

  /**
   * @type {boolean}
   */
  incremental: false,

  /**
   * Scale ratio for global scale.
   * @type {boolean}
   */
  globalScaleRatio: 1,
  beforeBrush: function beforeBrush(ctx) {},
  afterBrush: function afterBrush(ctx) {},

  /**
   * 图形绘制方法
   * @param {CanvasRenderingContext2D} ctx
   */
  // Interface
  brush: function brush(ctx, prevEl) {},

  /**
   * 获取最小包围盒
   * @return {module:zrender/core/BoundingRect}
   */
  // Interface
  getBoundingRect: function getBoundingRect() {},

  /**
   * 判断坐标 x, y 是否在图形上
   * If displayable element contain coord x, y
   * @param  {number} x
   * @param  {number} y
   * @return {boolean}
   */
  contain: function contain(x, y) {
    return this.rectContain(x, y);
  },

  /**
   * @param  {Function} cb
   * @param  {}   context
   */
  traverse: function traverse(cb, context) {
    cb.call(context, this);
  },

  /**
   * 判断坐标 x, y 是否在图形的包围盒上
   * If bounding rect of element contain coord x, y
   * @param  {number} x
   * @param  {number} y
   * @return {boolean}
   */
  rectContain: function rectContain(x, y) {
    var coord = this.transformCoordToLocal(x, y);
    var rect = this.getBoundingRect();
    return rect.contain(coord[0], coord[1]);
  },

  /**
   * 标记图形元素为脏，并且在下一帧重绘
   * Mark displayable element dirty and refresh next frame
   */
  dirty: function dirty() {
    this.__dirty = this.__dirtyText = true;
    this._rect = null;
    this.__zr && this.__zr.refresh();
  },

  /**
   * 图形是否会触发事件
   * If displayable object binded any event
   * @return {boolean}
   */
  // TODO, 通过 bind 绑定的事件
  // isSilent: function () {
  //     return !(
  //         this.hoverable || this.draggable
  //         || this.onmousemove || this.onmouseover || this.onmouseout
  //         || this.onmousedown || this.onmouseup || this.onclick
  //         || this.ondragenter || this.ondragover || this.ondragleave
  //         || this.ondrop
  //     );
  // },

  /**
   * Alias for animate('style')
   * @param {boolean} loop
   */
  animateStyle: function animateStyle(loop) {
    return this.animate('style', loop);
  },
  attrKV: function attrKV(key, value) {
    if (key !== 'style') {
      Element.prototype.attrKV.call(this, key, value);
    } else {
      this.style.set(value);
    }
  },

  /**
   * @param {Object|string} key
   * @param {*} value
   */
  setStyle: function setStyle(key, value) {
    this.style.set(key, value);
    this.dirty(false);
    return this;
  },

  /**
   * Use given style object
   * @param  {Object} obj
   */
  useStyle: function useStyle(obj) {
    this.style = new Style(obj, this);
    this.dirty(false);
    return this;
  }
};
zrUtil.inherits(Displayable, Element);
zrUtil.mixin(Displayable, RectText); // zrUtil.mixin(Displayable, Stateful);

var _default = Displayable;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9EaXNwbGF5YWJsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvRGlzcGxheWFibGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBTdHlsZSA9IHJlcXVpcmUoXCIuL1N0eWxlXCIpO1xuXG52YXIgRWxlbWVudCA9IHJlcXVpcmUoXCIuLi9FbGVtZW50XCIpO1xuXG52YXIgUmVjdFRleHQgPSByZXF1aXJlKFwiLi9taXhpbi9SZWN0VGV4dFwiKTtcblxuLyoqXG4gKiDlj6/nu5jliLbnmoTlm77lvaLln7rnsbtcbiAqIEJhc2UgY2xhc3Mgb2YgYWxsIGRpc3BsYXlhYmxlIGdyYXBoaWMgb2JqZWN0c1xuICogQG1vZHVsZSB6cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGVcbiAqL1xuXG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6enJlbmRlci9ncmFwaGljL0Rpc3BsYXlhYmxlXG4gKiBAZXh0ZW5kcyBtb2R1bGU6enJlbmRlci9FbGVtZW50XG4gKiBAZXh0ZW5kcyBtb2R1bGU6enJlbmRlci9ncmFwaGljL21peGluL1JlY3RUZXh0XG4gKi9cbmZ1bmN0aW9uIERpc3BsYXlhYmxlKG9wdHMpIHtcbiAgb3B0cyA9IG9wdHMgfHwge307XG4gIEVsZW1lbnQuY2FsbCh0aGlzLCBvcHRzKTsgLy8gRXh0ZW5kIHByb3BlcnRpZXNcblxuICBmb3IgKHZhciBuYW1lIGluIG9wdHMpIHtcbiAgICBpZiAob3B0cy5oYXNPd25Qcm9wZXJ0eShuYW1lKSAmJiBuYW1lICE9PSAnc3R5bGUnKSB7XG4gICAgICB0aGlzW25hbWVdID0gb3B0c1tuYW1lXTtcbiAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9ncmFwaGljL1N0eWxlfVxuICAgKi9cblxuXG4gIHRoaXMuc3R5bGUgPSBuZXcgU3R5bGUob3B0cy5zdHlsZSwgdGhpcyk7XG4gIHRoaXMuX3JlY3QgPSBudWxsOyAvLyBTaGFwZXMgZm9yIGNhc2NhZGUgY2xpcHBpbmcuXG5cbiAgdGhpcy5fX2NsaXBQYXRocyA9IFtdOyAvLyBGSVhNRSBTdGF0ZWZ1bCBtdXN0IGJlIG1peGluZWQgYWZ0ZXIgc3R5bGUgaXMgc2V0dGVkXG4gIC8vIFN0YXRlZnVsLmNhbGwodGhpcywgb3B0cyk7XG59XG5cbkRpc3BsYXlhYmxlLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IERpc3BsYXlhYmxlLFxuICB0eXBlOiAnZGlzcGxheWFibGUnLFxuXG4gIC8qKlxuICAgKiBEaXNwbGF5YWJsZSDmmK/lkKbkuLrohI/vvIxQYWludGVyIOS4reS8muagueaNruivpeagh+iusOWIpOaWreaYr+WQpumcgOimgeaYr+WQpumcgOimgemHjeaWsOe7mOWItlxuICAgKiBEaXJ0eSBmbGFnLiBGcm9tIHdoaWNoIHBhaW50ZXIgd2lsbCBkZXRlcm1pbmUgaWYgdGhpcyBkaXNwbGF5YWJsZSBvYmplY3QgbmVlZHMgYnJ1c2hcbiAgICogQG5hbWUgbW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZSNfX2RpcnR5XG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cbiAgX19kaXJ0eTogdHJ1ZSxcblxuICAvKipcbiAgICog5Zu+5b2i5piv5ZCm5Y+v6KeB77yM5Li6dHJ1ZeaXtuS4jee7mOWItuWbvuW9ou+8jOS9huaYr+S7jeiDveinpuWPkem8oOagh+S6i+S7tlxuICAgKiBJZiBpZ25vcmUgZHJhd2luZyBvZiB0aGUgZGlzcGxheWFibGUgb2JqZWN0LiBNb3VzZSBldmVudCB3aWxsIHN0aWxsIGJlIHRyaWdnZXJlZFxuICAgKiBAbmFtZSBtb2R1bGU6L3pyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZSNpbnZpc2libGVcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBpbnZpc2libGU6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiBAbmFtZSBtb2R1bGU6L3pyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZSN6XG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqIEBkZWZhdWx0IDBcbiAgICovXG4gIHo6IDAsXG5cbiAgLyoqXG4gICAqIEBuYW1lIG1vZHVsZTovenJlbmRlci9ncmFwaGljL0Rpc3BsYXlhYmxlI3pcbiAgICogQHR5cGUge251bWJlcn1cbiAgICogQGRlZmF1bHQgMFxuICAgKi9cbiAgejI6IDAsXG5cbiAgLyoqXG4gICAqIHrlsYJsZXZlbO+8jOWGs+Wumue7mOeUu+WcqOWTquWxgmNhbnZhc+S4rVxuICAgKiBAbmFtZSBtb2R1bGU6L3pyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZSN6bGV2ZWxcbiAgICogQHR5cGUge251bWJlcn1cbiAgICogQGRlZmF1bHQgMFxuICAgKi9cbiAgemxldmVsOiAwLFxuXG4gIC8qKlxuICAgKiDmmK/lkKblj6/mi5bmi71cbiAgICogQG5hbWUgbW9kdWxlOi96cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGUjZHJhZ2dhYmxlXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cbiAgZHJhZ2dhYmxlOiBmYWxzZSxcblxuICAvKipcbiAgICog5piv5ZCm5q2j5Zyo5ouW5ou9XG4gICAqIEBuYW1lIG1vZHVsZTovenJlbmRlci9ncmFwaGljL0Rpc3BsYXlhYmxlI2RyYWdnYWJsZVxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGRyYWdnaW5nOiBmYWxzZSxcblxuICAvKipcbiAgICog5piv5ZCm55u45bqU6byg5qCH5LqL5Lu2XG4gICAqIEBuYW1lIG1vZHVsZTovenJlbmRlci9ncmFwaGljL0Rpc3BsYXlhYmxlI3NpbGVudFxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIHNpbGVudDogZmFsc2UsXG5cbiAgLyoqXG4gICAqIElmIGVuYWJsZSBjdWxsaW5nXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cbiAgY3VsbGluZzogZmFsc2UsXG5cbiAgLyoqXG4gICAqIE1vdXNlIGN1cnNvciB3aGVuIGhvdmVyZWRcbiAgICogQG5hbWUgbW9kdWxlOi96cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGUjY3Vyc29yXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBjdXJzb3I6ICdwb2ludGVyJyxcblxuICAvKipcbiAgICogSWYgaG92ZXIgYXJlYSBpcyBib3VuZGluZyByZWN0XG4gICAqIEBuYW1lIG1vZHVsZTovenJlbmRlci9ncmFwaGljL0Rpc3BsYXlhYmxlI3JlY3RIb3ZlclxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgcmVjdEhvdmVyOiBmYWxzZSxcblxuICAvKipcbiAgICogUmVuZGVyIHRoZSBlbGVtZW50IHByb2dyZXNzaXZlbHkgd2hlbiB0aGUgdmFsdWUgPj0gMCxcbiAgICogdXNlZnVsbCBmb3IgbGFyZ2UgZGF0YS5cbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICBwcm9ncmVzc2l2ZTogZmFsc2UsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cbiAgaW5jcmVtZW50YWw6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiBTY2FsZSByYXRpbyBmb3IgZ2xvYmFsIHNjYWxlLlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG4gIGdsb2JhbFNjYWxlUmF0aW86IDEsXG4gIGJlZm9yZUJydXNoOiBmdW5jdGlvbiAoY3R4KSB7fSxcbiAgYWZ0ZXJCcnVzaDogZnVuY3Rpb24gKGN0eCkge30sXG5cbiAgLyoqXG4gICAqIOWbvuW9oue7mOWItuaWueazlVxuICAgKiBAcGFyYW0ge0NhbnZhc1JlbmRlcmluZ0NvbnRleHQyRH0gY3R4XG4gICAqL1xuICAvLyBJbnRlcmZhY2VcbiAgYnJ1c2g6IGZ1bmN0aW9uIChjdHgsIHByZXZFbCkge30sXG5cbiAgLyoqXG4gICAqIOiOt+WPluacgOWwj+WMheWbtOebklxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL0JvdW5kaW5nUmVjdH1cbiAgICovXG4gIC8vIEludGVyZmFjZVxuICBnZXRCb3VuZGluZ1JlY3Q6IGZ1bmN0aW9uICgpIHt9LFxuXG4gIC8qKlxuICAgKiDliKTmlq3lnZDmoIcgeCwgeSDmmK/lkKblnKjlm77lvaLkuIpcbiAgICogSWYgZGlzcGxheWFibGUgZWxlbWVudCBjb250YWluIGNvb3JkIHgsIHlcbiAgICogQHBhcmFtICB7bnVtYmVyfSB4XG4gICAqIEBwYXJhbSAge251bWJlcn0geVxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgY29udGFpbjogZnVuY3Rpb24gKHgsIHkpIHtcbiAgICByZXR1cm4gdGhpcy5yZWN0Q29udGFpbih4LCB5KTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtICB7RnVuY3Rpb259IGNiXG4gICAqIEBwYXJhbSAge30gICBjb250ZXh0XG4gICAqL1xuICB0cmF2ZXJzZTogZnVuY3Rpb24gKGNiLCBjb250ZXh0KSB7XG4gICAgY2IuY2FsbChjb250ZXh0LCB0aGlzKTtcbiAgfSxcblxuICAvKipcbiAgICog5Yik5pat5Z2Q5qCHIHgsIHkg5piv5ZCm5Zyo5Zu+5b2i55qE5YyF5Zu055uS5LiKXG4gICAqIElmIGJvdW5kaW5nIHJlY3Qgb2YgZWxlbWVudCBjb250YWluIGNvb3JkIHgsIHlcbiAgICogQHBhcmFtICB7bnVtYmVyfSB4XG4gICAqIEBwYXJhbSAge251bWJlcn0geVxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgcmVjdENvbnRhaW46IGZ1bmN0aW9uICh4LCB5KSB7XG4gICAgdmFyIGNvb3JkID0gdGhpcy50cmFuc2Zvcm1Db29yZFRvTG9jYWwoeCwgeSk7XG4gICAgdmFyIHJlY3QgPSB0aGlzLmdldEJvdW5kaW5nUmVjdCgpO1xuICAgIHJldHVybiByZWN0LmNvbnRhaW4oY29vcmRbMF0sIGNvb3JkWzFdKTtcbiAgfSxcblxuICAvKipcbiAgICog5qCH6K6w5Zu+5b2i5YWD57Sg5Li66ISP77yM5bm25LiU5Zyo5LiL5LiA5bin6YeN57uYXG4gICAqIE1hcmsgZGlzcGxheWFibGUgZWxlbWVudCBkaXJ0eSBhbmQgcmVmcmVzaCBuZXh0IGZyYW1lXG4gICAqL1xuICBkaXJ0eTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX19kaXJ0eSA9IHRoaXMuX19kaXJ0eVRleHQgPSB0cnVlO1xuICAgIHRoaXMuX3JlY3QgPSBudWxsO1xuICAgIHRoaXMuX196ciAmJiB0aGlzLl9fenIucmVmcmVzaCgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiDlm77lvaLmmK/lkKbkvJrop6blj5Hkuovku7ZcbiAgICogSWYgZGlzcGxheWFibGUgb2JqZWN0IGJpbmRlZCBhbnkgZXZlbnRcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIC8vIFRPRE8sIOmAmui/hyBiaW5kIOe7keWumueahOS6i+S7tlxuICAvLyBpc1NpbGVudDogZnVuY3Rpb24gKCkge1xuICAvLyAgICAgcmV0dXJuICEoXG4gIC8vICAgICAgICAgdGhpcy5ob3ZlcmFibGUgfHwgdGhpcy5kcmFnZ2FibGVcbiAgLy8gICAgICAgICB8fCB0aGlzLm9ubW91c2Vtb3ZlIHx8IHRoaXMub25tb3VzZW92ZXIgfHwgdGhpcy5vbm1vdXNlb3V0XG4gIC8vICAgICAgICAgfHwgdGhpcy5vbm1vdXNlZG93biB8fCB0aGlzLm9ubW91c2V1cCB8fCB0aGlzLm9uY2xpY2tcbiAgLy8gICAgICAgICB8fCB0aGlzLm9uZHJhZ2VudGVyIHx8IHRoaXMub25kcmFnb3ZlciB8fCB0aGlzLm9uZHJhZ2xlYXZlXG4gIC8vICAgICAgICAgfHwgdGhpcy5vbmRyb3BcbiAgLy8gICAgICk7XG4gIC8vIH0sXG5cbiAgLyoqXG4gICAqIEFsaWFzIGZvciBhbmltYXRlKCdzdHlsZScpXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gbG9vcFxuICAgKi9cbiAgYW5pbWF0ZVN0eWxlOiBmdW5jdGlvbiAobG9vcCkge1xuICAgIHJldHVybiB0aGlzLmFuaW1hdGUoJ3N0eWxlJywgbG9vcCk7XG4gIH0sXG4gIGF0dHJLVjogZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgICBpZiAoa2V5ICE9PSAnc3R5bGUnKSB7XG4gICAgICBFbGVtZW50LnByb3RvdHlwZS5hdHRyS1YuY2FsbCh0aGlzLCBrZXksIHZhbHVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zdHlsZS5zZXQodmFsdWUpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtPYmplY3R8c3RyaW5nfSBrZXlcbiAgICogQHBhcmFtIHsqfSB2YWx1ZVxuICAgKi9cbiAgc2V0U3R5bGU6IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gICAgdGhpcy5zdHlsZS5zZXQoa2V5LCB2YWx1ZSk7XG4gICAgdGhpcy5kaXJ0eShmYWxzZSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFVzZSBnaXZlbiBzdHlsZSBvYmplY3RcbiAgICogQHBhcmFtICB7T2JqZWN0fSBvYmpcbiAgICovXG4gIHVzZVN0eWxlOiBmdW5jdGlvbiAob2JqKSB7XG4gICAgdGhpcy5zdHlsZSA9IG5ldyBTdHlsZShvYmosIHRoaXMpO1xuICAgIHRoaXMuZGlydHkoZmFsc2UpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG59O1xuenJVdGlsLmluaGVyaXRzKERpc3BsYXlhYmxlLCBFbGVtZW50KTtcbnpyVXRpbC5taXhpbihEaXNwbGF5YWJsZSwgUmVjdFRleHQpOyAvLyB6clV0aWwubWl4aW4oRGlzcGxheWFibGUsIFN0YXRlZnVsKTtcblxudmFyIF9kZWZhdWx0ID0gRGlzcGxheWFibGU7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBTUE7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFyTkE7QUF1TkE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Displayable.js
