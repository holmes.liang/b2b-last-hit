/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnCut
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var Style = __webpack_require__(/*! fbjs/lib/Style */ "./node_modules/fbjs/lib/Style.js");

var getFragmentFromSelection = __webpack_require__(/*! ./getFragmentFromSelection */ "./node_modules/draft-js/lib/getFragmentFromSelection.js");

var getScrollPosition = __webpack_require__(/*! fbjs/lib/getScrollPosition */ "./node_modules/fbjs/lib/getScrollPosition.js");
/**
 * On `cut` events, native behavior is allowed to occur so that the system
 * clipboard is set properly. This means that we need to take steps to recover
 * the editor DOM state after the `cut` has occurred in order to maintain
 * control of the component.
 *
 * In addition, we can keep a copy of the removed fragment, including all
 * styles and entities, for use as an internal paste.
 */


function editOnCut(editor, e) {
  var editorState = editor._latestEditorState;
  var selection = editorState.getSelection();
  var element = e.target;
  var scrollPosition = void 0; // No selection, so there's nothing to cut.

  if (selection.isCollapsed()) {
    e.preventDefault();
    return;
  } // Track the current scroll position so that it can be forced back in place
  // after the editor regains control of the DOM.


  if (element instanceof Node) {
    scrollPosition = getScrollPosition(Style.getScrollParent(element));
  }

  var fragment = getFragmentFromSelection(editorState);
  editor.setClipboard(fragment); // Set `cut` mode to disable all event handling temporarily.

  editor.setMode('cut'); // Let native `cut` behavior occur, then recover control.

  setTimeout(function () {
    editor.restoreEditorDOM(scrollPosition);
    editor.exitCurrentMode();
    editor.update(removeFragment(editorState));
  }, 0);
}

function removeFragment(editorState) {
  var newContent = DraftModifier.removeRange(editorState.getCurrentContent(), editorState.getSelection(), 'forward');
  return EditorState.push(editorState, newContent, 'remove-range');
}

module.exports = editOnCut;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkN1dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9lZGl0T25DdXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBlZGl0T25DdXRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBTdHlsZSA9IHJlcXVpcmUoJ2ZianMvbGliL1N0eWxlJyk7XG5cbnZhciBnZXRGcmFnbWVudEZyb21TZWxlY3Rpb24gPSByZXF1aXJlKCcuL2dldEZyYWdtZW50RnJvbVNlbGVjdGlvbicpO1xudmFyIGdldFNjcm9sbFBvc2l0aW9uID0gcmVxdWlyZSgnZmJqcy9saWIvZ2V0U2Nyb2xsUG9zaXRpb24nKTtcblxuLyoqXG4gKiBPbiBgY3V0YCBldmVudHMsIG5hdGl2ZSBiZWhhdmlvciBpcyBhbGxvd2VkIHRvIG9jY3VyIHNvIHRoYXQgdGhlIHN5c3RlbVxuICogY2xpcGJvYXJkIGlzIHNldCBwcm9wZXJseS4gVGhpcyBtZWFucyB0aGF0IHdlIG5lZWQgdG8gdGFrZSBzdGVwcyB0byByZWNvdmVyXG4gKiB0aGUgZWRpdG9yIERPTSBzdGF0ZSBhZnRlciB0aGUgYGN1dGAgaGFzIG9jY3VycmVkIGluIG9yZGVyIHRvIG1haW50YWluXG4gKiBjb250cm9sIG9mIHRoZSBjb21wb25lbnQuXG4gKlxuICogSW4gYWRkaXRpb24sIHdlIGNhbiBrZWVwIGEgY29weSBvZiB0aGUgcmVtb3ZlZCBmcmFnbWVudCwgaW5jbHVkaW5nIGFsbFxuICogc3R5bGVzIGFuZCBlbnRpdGllcywgZm9yIHVzZSBhcyBhbiBpbnRlcm5hbCBwYXN0ZS5cbiAqL1xuZnVuY3Rpb24gZWRpdE9uQ3V0KGVkaXRvciwgZSkge1xuICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlO1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBlbGVtZW50ID0gZS50YXJnZXQ7XG4gIHZhciBzY3JvbGxQb3NpdGlvbiA9IHZvaWQgMDtcblxuICAvLyBObyBzZWxlY3Rpb24sIHNvIHRoZXJlJ3Mgbm90aGluZyB0byBjdXQuXG4gIGlmIChzZWxlY3Rpb24uaXNDb2xsYXBzZWQoKSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICByZXR1cm47XG4gIH1cblxuICAvLyBUcmFjayB0aGUgY3VycmVudCBzY3JvbGwgcG9zaXRpb24gc28gdGhhdCBpdCBjYW4gYmUgZm9yY2VkIGJhY2sgaW4gcGxhY2VcbiAgLy8gYWZ0ZXIgdGhlIGVkaXRvciByZWdhaW5zIGNvbnRyb2wgb2YgdGhlIERPTS5cbiAgaWYgKGVsZW1lbnQgaW5zdGFuY2VvZiBOb2RlKSB7XG4gICAgc2Nyb2xsUG9zaXRpb24gPSBnZXRTY3JvbGxQb3NpdGlvbihTdHlsZS5nZXRTY3JvbGxQYXJlbnQoZWxlbWVudCkpO1xuICB9XG5cbiAgdmFyIGZyYWdtZW50ID0gZ2V0RnJhZ21lbnRGcm9tU2VsZWN0aW9uKGVkaXRvclN0YXRlKTtcbiAgZWRpdG9yLnNldENsaXBib2FyZChmcmFnbWVudCk7XG5cbiAgLy8gU2V0IGBjdXRgIG1vZGUgdG8gZGlzYWJsZSBhbGwgZXZlbnQgaGFuZGxpbmcgdGVtcG9yYXJpbHkuXG4gIGVkaXRvci5zZXRNb2RlKCdjdXQnKTtcblxuICAvLyBMZXQgbmF0aXZlIGBjdXRgIGJlaGF2aW9yIG9jY3VyLCB0aGVuIHJlY292ZXIgY29udHJvbC5cbiAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgZWRpdG9yLnJlc3RvcmVFZGl0b3JET00oc2Nyb2xsUG9zaXRpb24pO1xuICAgIGVkaXRvci5leGl0Q3VycmVudE1vZGUoKTtcbiAgICBlZGl0b3IudXBkYXRlKHJlbW92ZUZyYWdtZW50KGVkaXRvclN0YXRlKSk7XG4gIH0sIDApO1xufVxuXG5mdW5jdGlvbiByZW1vdmVGcmFnbWVudChlZGl0b3JTdGF0ZSkge1xuICB2YXIgbmV3Q29udGVudCA9IERyYWZ0TW9kaWZpZXIucmVtb3ZlUmFuZ2UoZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSwgZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCksICdmb3J3YXJkJyk7XG4gIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBuZXdDb250ZW50LCAncmVtb3ZlLXJhbmdlJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZWRpdE9uQ3V0OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTs7Ozs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnCut.js
