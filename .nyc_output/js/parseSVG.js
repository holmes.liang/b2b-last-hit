var Group = __webpack_require__(/*! ../container/Group */ "./node_modules/zrender/lib/container/Group.js");

var ZImage = __webpack_require__(/*! ../graphic/Image */ "./node_modules/zrender/lib/graphic/Image.js");

var Text = __webpack_require__(/*! ../graphic/Text */ "./node_modules/zrender/lib/graphic/Text.js");

var Circle = __webpack_require__(/*! ../graphic/shape/Circle */ "./node_modules/zrender/lib/graphic/shape/Circle.js");

var Rect = __webpack_require__(/*! ../graphic/shape/Rect */ "./node_modules/zrender/lib/graphic/shape/Rect.js");

var Ellipse = __webpack_require__(/*! ../graphic/shape/Ellipse */ "./node_modules/zrender/lib/graphic/shape/Ellipse.js");

var Line = __webpack_require__(/*! ../graphic/shape/Line */ "./node_modules/zrender/lib/graphic/shape/Line.js");

var Path = __webpack_require__(/*! ../graphic/Path */ "./node_modules/zrender/lib/graphic/Path.js");

var Polygon = __webpack_require__(/*! ../graphic/shape/Polygon */ "./node_modules/zrender/lib/graphic/shape/Polygon.js");

var Polyline = __webpack_require__(/*! ../graphic/shape/Polyline */ "./node_modules/zrender/lib/graphic/shape/Polyline.js");

var LinearGradient = __webpack_require__(/*! ../graphic/LinearGradient */ "./node_modules/zrender/lib/graphic/LinearGradient.js");

var Style = __webpack_require__(/*! ../graphic/Style */ "./node_modules/zrender/lib/graphic/Style.js");

var matrix = __webpack_require__(/*! ../core/matrix */ "./node_modules/zrender/lib/core/matrix.js");

var _path = __webpack_require__(/*! ./path */ "./node_modules/zrender/lib/tool/path.js");

var createFromString = _path.createFromString;

var _util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var isString = _util.isString;
var extend = _util.extend;
var defaults = _util.defaults;
var trim = _util.trim;
var each = _util.each; // import RadialGradient from '../graphic/RadialGradient';
// import Pattern from '../graphic/Pattern';
// import * as vector from '../core/vector';
// Most of the values can be separated by comma and/or white space.

var DILIMITER_REG = /[\s,]+/;
/**
 * For big svg string, this method might be time consuming.
 *
 * @param {string} svg xml string
 * @return {Object} xml root.
 */

function parseXML(svg) {
  if (isString(svg)) {
    var parser = new DOMParser();
    svg = parser.parseFromString(svg, 'text/xml');
  } // Document node. If using $.get, doc node may be input.


  if (svg.nodeType === 9) {
    svg = svg.firstChild;
  } // nodeName of <!DOCTYPE svg> is also 'svg'.


  while (svg.nodeName.toLowerCase() !== 'svg' || svg.nodeType !== 1) {
    svg = svg.nextSibling;
  }

  return svg;
}

function SVGParser() {
  this._defs = {};
  this._root = null;
  this._isDefine = false;
  this._isText = false;
}

SVGParser.prototype.parse = function (xml, opt) {
  opt = opt || {};
  var svg = parseXML(xml);

  if (!svg) {
    throw new Error('Illegal svg');
  }

  var root = new Group();
  this._root = root; // parse view port

  var viewBox = svg.getAttribute('viewBox') || ''; // If width/height not specified, means "100%" of `opt.width/height`.
  // TODO: Other percent value not supported yet.

  var width = parseFloat(svg.getAttribute('width') || opt.width);
  var height = parseFloat(svg.getAttribute('height') || opt.height); // If width/height not specified, set as null for output.

  isNaN(width) && (width = null);
  isNaN(height) && (height = null); // Apply inline style on svg element.

  parseAttributes(svg, root, null, true);
  var child = svg.firstChild;

  while (child) {
    this._parseNode(child, root);

    child = child.nextSibling;
  }

  var viewBoxRect;
  var viewBoxTransform;

  if (viewBox) {
    var viewBoxArr = trim(viewBox).split(DILIMITER_REG); // Some invalid case like viewBox: 'none'.

    if (viewBoxArr.length >= 4) {
      viewBoxRect = {
        x: parseFloat(viewBoxArr[0] || 0),
        y: parseFloat(viewBoxArr[1] || 0),
        width: parseFloat(viewBoxArr[2]),
        height: parseFloat(viewBoxArr[3])
      };
    }
  }

  if (viewBoxRect && width != null && height != null) {
    viewBoxTransform = makeViewBoxTransform(viewBoxRect, width, height);

    if (!opt.ignoreViewBox) {
      // If set transform on the output group, it probably bring trouble when
      // some users only intend to show the clipped content inside the viewBox,
      // but not intend to transform the output group. So we keep the output
      // group no transform. If the user intend to use the viewBox as a
      // camera, just set `opt.ignoreViewBox` as `true` and set transfrom
      // manually according to the viewBox info in the output of this method.
      var elRoot = root;
      root = new Group();
      root.add(elRoot);
      elRoot.scale = viewBoxTransform.scale.slice();
      elRoot.position = viewBoxTransform.position.slice();
    }
  } // Some shapes might be overflow the viewport, which should be
  // clipped despite whether the viewBox is used, as the SVG does.


  if (!opt.ignoreRootClip && width != null && height != null) {
    root.setClipPath(new Rect({
      shape: {
        x: 0,
        y: 0,
        width: width,
        height: height
      }
    }));
  } // Set width/height on group just for output the viewport size.


  return {
    root: root,
    width: width,
    height: height,
    viewBoxRect: viewBoxRect,
    viewBoxTransform: viewBoxTransform
  };
};

SVGParser.prototype._parseNode = function (xmlNode, parentGroup) {
  var nodeName = xmlNode.nodeName.toLowerCase(); // TODO
  // support <style>...</style> in svg, where nodeName is 'style',
  // CSS classes is defined globally wherever the style tags are declared.

  if (nodeName === 'defs') {
    // define flag
    this._isDefine = true;
  } else if (nodeName === 'text') {
    this._isText = true;
  }

  var el;

  if (this._isDefine) {
    var parser = defineParsers[nodeName];

    if (parser) {
      var def = parser.call(this, xmlNode);
      var id = xmlNode.getAttribute('id');

      if (id) {
        this._defs[id] = def;
      }
    }
  } else {
    var parser = nodeParsers[nodeName];

    if (parser) {
      el = parser.call(this, xmlNode, parentGroup);
      parentGroup.add(el);
    }
  }

  var child = xmlNode.firstChild;

  while (child) {
    if (child.nodeType === 1) {
      this._parseNode(child, el);
    } // Is text


    if (child.nodeType === 3 && this._isText) {
      this._parseText(child, el);
    }

    child = child.nextSibling;
  } // Quit define


  if (nodeName === 'defs') {
    this._isDefine = false;
  } else if (nodeName === 'text') {
    this._isText = false;
  }
};

SVGParser.prototype._parseText = function (xmlNode, parentGroup) {
  if (xmlNode.nodeType === 1) {
    var dx = xmlNode.getAttribute('dx') || 0;
    var dy = xmlNode.getAttribute('dy') || 0;
    this._textX += parseFloat(dx);
    this._textY += parseFloat(dy);
  }

  var text = new Text({
    style: {
      text: xmlNode.textContent,
      transformText: true
    },
    position: [this._textX || 0, this._textY || 0]
  });
  inheritStyle(parentGroup, text);
  parseAttributes(xmlNode, text, this._defs);
  var fontSize = text.style.fontSize;

  if (fontSize && fontSize < 9) {
    // PENDING
    text.style.fontSize = 9;
    text.scale = text.scale || [1, 1];
    text.scale[0] *= fontSize / 9;
    text.scale[1] *= fontSize / 9;
  }

  var rect = text.getBoundingRect();
  this._textX += rect.width;
  parentGroup.add(text);
  return text;
};

var nodeParsers = {
  'g': function g(xmlNode, parentGroup) {
    var g = new Group();
    inheritStyle(parentGroup, g);
    parseAttributes(xmlNode, g, this._defs);
    return g;
  },
  'rect': function rect(xmlNode, parentGroup) {
    var rect = new Rect();
    inheritStyle(parentGroup, rect);
    parseAttributes(xmlNode, rect, this._defs);
    rect.setShape({
      x: parseFloat(xmlNode.getAttribute('x') || 0),
      y: parseFloat(xmlNode.getAttribute('y') || 0),
      width: parseFloat(xmlNode.getAttribute('width') || 0),
      height: parseFloat(xmlNode.getAttribute('height') || 0)
    }); // console.log(xmlNode.getAttribute('transform'));
    // console.log(rect.transform);

    return rect;
  },
  'circle': function circle(xmlNode, parentGroup) {
    var circle = new Circle();
    inheritStyle(parentGroup, circle);
    parseAttributes(xmlNode, circle, this._defs);
    circle.setShape({
      cx: parseFloat(xmlNode.getAttribute('cx') || 0),
      cy: parseFloat(xmlNode.getAttribute('cy') || 0),
      r: parseFloat(xmlNode.getAttribute('r') || 0)
    });
    return circle;
  },
  'line': function line(xmlNode, parentGroup) {
    var line = new Line();
    inheritStyle(parentGroup, line);
    parseAttributes(xmlNode, line, this._defs);
    line.setShape({
      x1: parseFloat(xmlNode.getAttribute('x1') || 0),
      y1: parseFloat(xmlNode.getAttribute('y1') || 0),
      x2: parseFloat(xmlNode.getAttribute('x2') || 0),
      y2: parseFloat(xmlNode.getAttribute('y2') || 0)
    });
    return line;
  },
  'ellipse': function ellipse(xmlNode, parentGroup) {
    var ellipse = new Ellipse();
    inheritStyle(parentGroup, ellipse);
    parseAttributes(xmlNode, ellipse, this._defs);
    ellipse.setShape({
      cx: parseFloat(xmlNode.getAttribute('cx') || 0),
      cy: parseFloat(xmlNode.getAttribute('cy') || 0),
      rx: parseFloat(xmlNode.getAttribute('rx') || 0),
      ry: parseFloat(xmlNode.getAttribute('ry') || 0)
    });
    return ellipse;
  },
  'polygon': function polygon(xmlNode, parentGroup) {
    var points = xmlNode.getAttribute('points');

    if (points) {
      points = parsePoints(points);
    }

    var polygon = new Polygon({
      shape: {
        points: points || []
      }
    });
    inheritStyle(parentGroup, polygon);
    parseAttributes(xmlNode, polygon, this._defs);
    return polygon;
  },
  'polyline': function polyline(xmlNode, parentGroup) {
    var path = new Path();
    inheritStyle(parentGroup, path);
    parseAttributes(xmlNode, path, this._defs);
    var points = xmlNode.getAttribute('points');

    if (points) {
      points = parsePoints(points);
    }

    var polyline = new Polyline({
      shape: {
        points: points || []
      }
    });
    return polyline;
  },
  'image': function image(xmlNode, parentGroup) {
    var img = new ZImage();
    inheritStyle(parentGroup, img);
    parseAttributes(xmlNode, img, this._defs);
    img.setStyle({
      image: xmlNode.getAttribute('xlink:href'),
      x: xmlNode.getAttribute('x'),
      y: xmlNode.getAttribute('y'),
      width: xmlNode.getAttribute('width'),
      height: xmlNode.getAttribute('height')
    });
    return img;
  },
  'text': function text(xmlNode, parentGroup) {
    var x = xmlNode.getAttribute('x') || 0;
    var y = xmlNode.getAttribute('y') || 0;
    var dx = xmlNode.getAttribute('dx') || 0;
    var dy = xmlNode.getAttribute('dy') || 0;
    this._textX = parseFloat(x) + parseFloat(dx);
    this._textY = parseFloat(y) + parseFloat(dy);
    var g = new Group();
    inheritStyle(parentGroup, g);
    parseAttributes(xmlNode, g, this._defs);
    return g;
  },
  'tspan': function tspan(xmlNode, parentGroup) {
    var x = xmlNode.getAttribute('x');
    var y = xmlNode.getAttribute('y');

    if (x != null) {
      // new offset x
      this._textX = parseFloat(x);
    }

    if (y != null) {
      // new offset y
      this._textY = parseFloat(y);
    }

    var dx = xmlNode.getAttribute('dx') || 0;
    var dy = xmlNode.getAttribute('dy') || 0;
    var g = new Group();
    inheritStyle(parentGroup, g);
    parseAttributes(xmlNode, g, this._defs);
    this._textX += dx;
    this._textY += dy;
    return g;
  },
  'path': function path(xmlNode, parentGroup) {
    // TODO svg fill rule
    // https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/fill-rule
    // path.style.globalCompositeOperation = 'xor';
    var d = xmlNode.getAttribute('d') || ''; // Performance sensitive.

    var path = createFromString(d);
    inheritStyle(parentGroup, path);
    parseAttributes(xmlNode, path, this._defs);
    return path;
  }
};
var defineParsers = {
  'lineargradient': function lineargradient(xmlNode) {
    var x1 = parseInt(xmlNode.getAttribute('x1') || 0, 10);
    var y1 = parseInt(xmlNode.getAttribute('y1') || 0, 10);
    var x2 = parseInt(xmlNode.getAttribute('x2') || 10, 10);
    var y2 = parseInt(xmlNode.getAttribute('y2') || 0, 10);
    var gradient = new LinearGradient(x1, y1, x2, y2);

    _parseGradientColorStops(xmlNode, gradient);

    return gradient;
  },
  'radialgradient': function radialgradient(xmlNode) {}
};

function _parseGradientColorStops(xmlNode, gradient) {
  var stop = xmlNode.firstChild;

  while (stop) {
    if (stop.nodeType === 1) {
      var offset = stop.getAttribute('offset');

      if (offset.indexOf('%') > 0) {
        // percentage
        offset = parseInt(offset, 10) / 100;
      } else if (offset) {
        // number from 0 to 1
        offset = parseFloat(offset);
      } else {
        offset = 0;
      }

      var stopColor = stop.getAttribute('stop-color') || '#000000';
      gradient.addColorStop(offset, stopColor);
    }

    stop = stop.nextSibling;
  }
}

function inheritStyle(parent, child) {
  if (parent && parent.__inheritedStyle) {
    if (!child.__inheritedStyle) {
      child.__inheritedStyle = {};
    }

    defaults(child.__inheritedStyle, parent.__inheritedStyle);
  }
}

function parsePoints(pointsString) {
  var list = trim(pointsString).split(DILIMITER_REG);
  var points = [];

  for (var i = 0; i < list.length; i += 2) {
    var x = parseFloat(list[i]);
    var y = parseFloat(list[i + 1]);
    points.push([x, y]);
  }

  return points;
}

var attributesMap = {
  'fill': 'fill',
  'stroke': 'stroke',
  'stroke-width': 'lineWidth',
  'opacity': 'opacity',
  'fill-opacity': 'fillOpacity',
  'stroke-opacity': 'strokeOpacity',
  'stroke-dasharray': 'lineDash',
  'stroke-dashoffset': 'lineDashOffset',
  'stroke-linecap': 'lineCap',
  'stroke-linejoin': 'lineJoin',
  'stroke-miterlimit': 'miterLimit',
  'font-family': 'fontFamily',
  'font-size': 'fontSize',
  'font-style': 'fontStyle',
  'font-weight': 'fontWeight',
  'text-align': 'textAlign',
  'alignment-baseline': 'textBaseline'
};

function parseAttributes(xmlNode, el, defs, onlyInlineStyle) {
  var zrStyle = el.__inheritedStyle || {};
  var isTextEl = el.type === 'text'; // TODO Shadow

  if (xmlNode.nodeType === 1) {
    parseTransformAttribute(xmlNode, el);
    extend(zrStyle, parseStyleAttribute(xmlNode));

    if (!onlyInlineStyle) {
      for (var svgAttrName in attributesMap) {
        if (attributesMap.hasOwnProperty(svgAttrName)) {
          var attrValue = xmlNode.getAttribute(svgAttrName);

          if (attrValue != null) {
            zrStyle[attributesMap[svgAttrName]] = attrValue;
          }
        }
      }
    }
  }

  var elFillProp = isTextEl ? 'textFill' : 'fill';
  var elStrokeProp = isTextEl ? 'textStroke' : 'stroke';
  el.style = el.style || new Style();
  var elStyle = el.style;
  zrStyle.fill != null && elStyle.set(elFillProp, getPaint(zrStyle.fill, defs));
  zrStyle.stroke != null && elStyle.set(elStrokeProp, getPaint(zrStyle.stroke, defs));
  each(['lineWidth', 'opacity', 'fillOpacity', 'strokeOpacity', 'miterLimit', 'fontSize'], function (propName) {
    var elPropName = propName === 'lineWidth' && isTextEl ? 'textStrokeWidth' : propName;
    zrStyle[propName] != null && elStyle.set(elPropName, parseFloat(zrStyle[propName]));
  });

  if (!zrStyle.textBaseline || zrStyle.textBaseline === 'auto') {
    zrStyle.textBaseline = 'alphabetic';
  }

  if (zrStyle.textBaseline === 'alphabetic') {
    zrStyle.textBaseline = 'bottom';
  }

  if (zrStyle.textAlign === 'start') {
    zrStyle.textAlign = 'left';
  }

  if (zrStyle.textAlign === 'end') {
    zrStyle.textAlign = 'right';
  }

  each(['lineDashOffset', 'lineCap', 'lineJoin', 'fontWeight', 'fontFamily', 'fontStyle', 'textAlign', 'textBaseline'], function (propName) {
    zrStyle[propName] != null && elStyle.set(propName, zrStyle[propName]);
  });

  if (zrStyle.lineDash) {
    el.style.lineDash = trim(zrStyle.lineDash).split(DILIMITER_REG);
  }

  if (elStyle[elStrokeProp] && elStyle[elStrokeProp] !== 'none') {
    // enable stroke
    el[elStrokeProp] = true;
  }

  el.__inheritedStyle = zrStyle;
}

var urlRegex = /url\(\s*#(.*?)\)/;

function getPaint(str, defs) {
  // if (str === 'none') {
  //     return;
  // }
  var urlMatch = defs && str && str.match(urlRegex);

  if (urlMatch) {
    var url = trim(urlMatch[1]);
    var def = defs[url];
    return def;
  }

  return str;
}

var transformRegex = /(translate|scale|rotate|skewX|skewY|matrix)\(([\-\s0-9\.e,]*)\)/g;

function parseTransformAttribute(xmlNode, node) {
  var transform = xmlNode.getAttribute('transform');

  if (transform) {
    transform = transform.replace(/,/g, ' ');
    var m = null;
    var transformOps = [];
    transform.replace(transformRegex, function (str, type, value) {
      transformOps.push(type, value);
    });

    for (var i = transformOps.length - 1; i > 0; i -= 2) {
      var value = transformOps[i];
      var type = transformOps[i - 1];
      m = m || matrix.create();

      switch (type) {
        case 'translate':
          value = trim(value).split(DILIMITER_REG);
          matrix.translate(m, m, [parseFloat(value[0]), parseFloat(value[1] || 0)]);
          break;

        case 'scale':
          value = trim(value).split(DILIMITER_REG);
          matrix.scale(m, m, [parseFloat(value[0]), parseFloat(value[1] || value[0])]);
          break;

        case 'rotate':
          value = trim(value).split(DILIMITER_REG);
          matrix.rotate(m, m, parseFloat(value[0]));
          break;

        case 'skew':
          value = trim(value).split(DILIMITER_REG);
          console.warn('Skew transform is not supported yet');
          break;

        case 'matrix':
          var value = trim(value).split(DILIMITER_REG);
          m[0] = parseFloat(value[0]);
          m[1] = parseFloat(value[1]);
          m[2] = parseFloat(value[2]);
          m[3] = parseFloat(value[3]);
          m[4] = parseFloat(value[4]);
          m[5] = parseFloat(value[5]);
          break;
      }
    }

    node.setLocalTransform(m);
  }
} // Value may contain space.


var styleRegex = /([^\s:;]+)\s*:\s*([^:;]+)/g;

function parseStyleAttribute(xmlNode) {
  var style = xmlNode.getAttribute('style');
  var result = {};

  if (!style) {
    return result;
  }

  var styleList = {};
  styleRegex.lastIndex = 0;
  var styleRegResult;

  while ((styleRegResult = styleRegex.exec(style)) != null) {
    styleList[styleRegResult[1]] = styleRegResult[2];
  }

  for (var svgAttrName in attributesMap) {
    if (attributesMap.hasOwnProperty(svgAttrName) && styleList[svgAttrName] != null) {
      result[attributesMap[svgAttrName]] = styleList[svgAttrName];
    }
  }

  return result;
}
/**
 * @param {Array.<number>} viewBoxRect
 * @param {number} width
 * @param {number} height
 * @return {Object} {scale, position}
 */


function makeViewBoxTransform(viewBoxRect, width, height) {
  var scaleX = width / viewBoxRect.width;
  var scaleY = height / viewBoxRect.height;
  var scale = Math.min(scaleX, scaleY); // preserveAspectRatio 'xMidYMid'

  var viewBoxScale = [scale, scale];
  var viewBoxPosition = [-(viewBoxRect.x + viewBoxRect.width / 2) * scale + width / 2, -(viewBoxRect.y + viewBoxRect.height / 2) * scale + height / 2];
  return {
    scale: viewBoxScale,
    position: viewBoxPosition
  };
}
/**
 * @param {string|XMLElement} xml
 * @param {Object} [opt]
 * @param {number} [opt.width] Default width if svg width not specified or is a percent value.
 * @param {number} [opt.height] Default height if svg height not specified or is a percent value.
 * @param {boolean} [opt.ignoreViewBox]
 * @param {boolean} [opt.ignoreRootClip]
 * @return {Object} result:
 * {
 *     root: Group, The root of the the result tree of zrender shapes,
 *     width: number, the viewport width of the SVG,
 *     height: number, the viewport height of the SVG,
 *     viewBoxRect: {x, y, width, height}, the declared viewBox rect of the SVG, if exists,
 *     viewBoxTransform: the {scale, position} calculated by viewBox and viewport, is exists.
 * }
 */


function parseSVG(xml, opt) {
  var parser = new SVGParser();
  return parser.parse(xml, opt);
}

exports.parseXML = parseXML;
exports.makeViewBoxTransform = makeViewBoxTransform;
exports.parseSVG = parseSVG;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC9wYXJzZVNWRy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL3Rvb2wvcGFyc2VTVkcuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEdyb3VwID0gcmVxdWlyZShcIi4uL2NvbnRhaW5lci9Hcm91cFwiKTtcblxudmFyIFpJbWFnZSA9IHJlcXVpcmUoXCIuLi9ncmFwaGljL0ltYWdlXCIpO1xuXG52YXIgVGV4dCA9IHJlcXVpcmUoXCIuLi9ncmFwaGljL1RleHRcIik7XG5cbnZhciBDaXJjbGUgPSByZXF1aXJlKFwiLi4vZ3JhcGhpYy9zaGFwZS9DaXJjbGVcIik7XG5cbnZhciBSZWN0ID0gcmVxdWlyZShcIi4uL2dyYXBoaWMvc2hhcGUvUmVjdFwiKTtcblxudmFyIEVsbGlwc2UgPSByZXF1aXJlKFwiLi4vZ3JhcGhpYy9zaGFwZS9FbGxpcHNlXCIpO1xuXG52YXIgTGluZSA9IHJlcXVpcmUoXCIuLi9ncmFwaGljL3NoYXBlL0xpbmVcIik7XG5cbnZhciBQYXRoID0gcmVxdWlyZShcIi4uL2dyYXBoaWMvUGF0aFwiKTtcblxudmFyIFBvbHlnb24gPSByZXF1aXJlKFwiLi4vZ3JhcGhpYy9zaGFwZS9Qb2x5Z29uXCIpO1xuXG52YXIgUG9seWxpbmUgPSByZXF1aXJlKFwiLi4vZ3JhcGhpYy9zaGFwZS9Qb2x5bGluZVwiKTtcblxudmFyIExpbmVhckdyYWRpZW50ID0gcmVxdWlyZShcIi4uL2dyYXBoaWMvTGluZWFyR3JhZGllbnRcIik7XG5cbnZhciBTdHlsZSA9IHJlcXVpcmUoXCIuLi9ncmFwaGljL1N0eWxlXCIpO1xuXG52YXIgbWF0cml4ID0gcmVxdWlyZShcIi4uL2NvcmUvbWF0cml4XCIpO1xuXG52YXIgX3BhdGggPSByZXF1aXJlKFwiLi9wYXRoXCIpO1xuXG52YXIgY3JlYXRlRnJvbVN0cmluZyA9IF9wYXRoLmNyZWF0ZUZyb21TdHJpbmc7XG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBpc1N0cmluZyA9IF91dGlsLmlzU3RyaW5nO1xudmFyIGV4dGVuZCA9IF91dGlsLmV4dGVuZDtcbnZhciBkZWZhdWx0cyA9IF91dGlsLmRlZmF1bHRzO1xudmFyIHRyaW0gPSBfdXRpbC50cmltO1xudmFyIGVhY2ggPSBfdXRpbC5lYWNoO1xuLy8gaW1wb3J0IFJhZGlhbEdyYWRpZW50IGZyb20gJy4uL2dyYXBoaWMvUmFkaWFsR3JhZGllbnQnO1xuLy8gaW1wb3J0IFBhdHRlcm4gZnJvbSAnLi4vZ3JhcGhpYy9QYXR0ZXJuJztcbi8vIGltcG9ydCAqIGFzIHZlY3RvciBmcm9tICcuLi9jb3JlL3ZlY3Rvcic7XG4vLyBNb3N0IG9mIHRoZSB2YWx1ZXMgY2FuIGJlIHNlcGFyYXRlZCBieSBjb21tYSBhbmQvb3Igd2hpdGUgc3BhY2UuXG52YXIgRElMSU1JVEVSX1JFRyA9IC9bXFxzLF0rLztcbi8qKlxuICogRm9yIGJpZyBzdmcgc3RyaW5nLCB0aGlzIG1ldGhvZCBtaWdodCBiZSB0aW1lIGNvbnN1bWluZy5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gc3ZnIHhtbCBzdHJpbmdcbiAqIEByZXR1cm4ge09iamVjdH0geG1sIHJvb3QuXG4gKi9cblxuZnVuY3Rpb24gcGFyc2VYTUwoc3ZnKSB7XG4gIGlmIChpc1N0cmluZyhzdmcpKSB7XG4gICAgdmFyIHBhcnNlciA9IG5ldyBET01QYXJzZXIoKTtcbiAgICBzdmcgPSBwYXJzZXIucGFyc2VGcm9tU3RyaW5nKHN2ZywgJ3RleHQveG1sJyk7XG4gIH0gLy8gRG9jdW1lbnQgbm9kZS4gSWYgdXNpbmcgJC5nZXQsIGRvYyBub2RlIG1heSBiZSBpbnB1dC5cblxuXG4gIGlmIChzdmcubm9kZVR5cGUgPT09IDkpIHtcbiAgICBzdmcgPSBzdmcuZmlyc3RDaGlsZDtcbiAgfSAvLyBub2RlTmFtZSBvZiA8IURPQ1RZUEUgc3ZnPiBpcyBhbHNvICdzdmcnLlxuXG5cbiAgd2hpbGUgKHN2Zy5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpICE9PSAnc3ZnJyB8fCBzdmcubm9kZVR5cGUgIT09IDEpIHtcbiAgICBzdmcgPSBzdmcubmV4dFNpYmxpbmc7XG4gIH1cblxuICByZXR1cm4gc3ZnO1xufVxuXG5mdW5jdGlvbiBTVkdQYXJzZXIoKSB7XG4gIHRoaXMuX2RlZnMgPSB7fTtcbiAgdGhpcy5fcm9vdCA9IG51bGw7XG4gIHRoaXMuX2lzRGVmaW5lID0gZmFsc2U7XG4gIHRoaXMuX2lzVGV4dCA9IGZhbHNlO1xufVxuXG5TVkdQYXJzZXIucHJvdG90eXBlLnBhcnNlID0gZnVuY3Rpb24gKHhtbCwgb3B0KSB7XG4gIG9wdCA9IG9wdCB8fCB7fTtcbiAgdmFyIHN2ZyA9IHBhcnNlWE1MKHhtbCk7XG5cbiAgaWYgKCFzdmcpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0lsbGVnYWwgc3ZnJyk7XG4gIH1cblxuICB2YXIgcm9vdCA9IG5ldyBHcm91cCgpO1xuICB0aGlzLl9yb290ID0gcm9vdDsgLy8gcGFyc2UgdmlldyBwb3J0XG5cbiAgdmFyIHZpZXdCb3ggPSBzdmcuZ2V0QXR0cmlidXRlKCd2aWV3Qm94JykgfHwgJyc7IC8vIElmIHdpZHRoL2hlaWdodCBub3Qgc3BlY2lmaWVkLCBtZWFucyBcIjEwMCVcIiBvZiBgb3B0LndpZHRoL2hlaWdodGAuXG4gIC8vIFRPRE86IE90aGVyIHBlcmNlbnQgdmFsdWUgbm90IHN1cHBvcnRlZCB5ZXQuXG5cbiAgdmFyIHdpZHRoID0gcGFyc2VGbG9hdChzdmcuZ2V0QXR0cmlidXRlKCd3aWR0aCcpIHx8IG9wdC53aWR0aCk7XG4gIHZhciBoZWlnaHQgPSBwYXJzZUZsb2F0KHN2Zy5nZXRBdHRyaWJ1dGUoJ2hlaWdodCcpIHx8IG9wdC5oZWlnaHQpOyAvLyBJZiB3aWR0aC9oZWlnaHQgbm90IHNwZWNpZmllZCwgc2V0IGFzIG51bGwgZm9yIG91dHB1dC5cblxuICBpc05hTih3aWR0aCkgJiYgKHdpZHRoID0gbnVsbCk7XG4gIGlzTmFOKGhlaWdodCkgJiYgKGhlaWdodCA9IG51bGwpOyAvLyBBcHBseSBpbmxpbmUgc3R5bGUgb24gc3ZnIGVsZW1lbnQuXG5cbiAgcGFyc2VBdHRyaWJ1dGVzKHN2Zywgcm9vdCwgbnVsbCwgdHJ1ZSk7XG4gIHZhciBjaGlsZCA9IHN2Zy5maXJzdENoaWxkO1xuXG4gIHdoaWxlIChjaGlsZCkge1xuICAgIHRoaXMuX3BhcnNlTm9kZShjaGlsZCwgcm9vdCk7XG5cbiAgICBjaGlsZCA9IGNoaWxkLm5leHRTaWJsaW5nO1xuICB9XG5cbiAgdmFyIHZpZXdCb3hSZWN0O1xuICB2YXIgdmlld0JveFRyYW5zZm9ybTtcblxuICBpZiAodmlld0JveCkge1xuICAgIHZhciB2aWV3Qm94QXJyID0gdHJpbSh2aWV3Qm94KS5zcGxpdChESUxJTUlURVJfUkVHKTsgLy8gU29tZSBpbnZhbGlkIGNhc2UgbGlrZSB2aWV3Qm94OiAnbm9uZScuXG5cbiAgICBpZiAodmlld0JveEFyci5sZW5ndGggPj0gNCkge1xuICAgICAgdmlld0JveFJlY3QgPSB7XG4gICAgICAgIHg6IHBhcnNlRmxvYXQodmlld0JveEFyclswXSB8fCAwKSxcbiAgICAgICAgeTogcGFyc2VGbG9hdCh2aWV3Qm94QXJyWzFdIHx8IDApLFxuICAgICAgICB3aWR0aDogcGFyc2VGbG9hdCh2aWV3Qm94QXJyWzJdKSxcbiAgICAgICAgaGVpZ2h0OiBwYXJzZUZsb2F0KHZpZXdCb3hBcnJbM10pXG4gICAgICB9O1xuICAgIH1cbiAgfVxuXG4gIGlmICh2aWV3Qm94UmVjdCAmJiB3aWR0aCAhPSBudWxsICYmIGhlaWdodCAhPSBudWxsKSB7XG4gICAgdmlld0JveFRyYW5zZm9ybSA9IG1ha2VWaWV3Qm94VHJhbnNmb3JtKHZpZXdCb3hSZWN0LCB3aWR0aCwgaGVpZ2h0KTtcblxuICAgIGlmICghb3B0Lmlnbm9yZVZpZXdCb3gpIHtcbiAgICAgIC8vIElmIHNldCB0cmFuc2Zvcm0gb24gdGhlIG91dHB1dCBncm91cCwgaXQgcHJvYmFibHkgYnJpbmcgdHJvdWJsZSB3aGVuXG4gICAgICAvLyBzb21lIHVzZXJzIG9ubHkgaW50ZW5kIHRvIHNob3cgdGhlIGNsaXBwZWQgY29udGVudCBpbnNpZGUgdGhlIHZpZXdCb3gsXG4gICAgICAvLyBidXQgbm90IGludGVuZCB0byB0cmFuc2Zvcm0gdGhlIG91dHB1dCBncm91cC4gU28gd2Uga2VlcCB0aGUgb3V0cHV0XG4gICAgICAvLyBncm91cCBubyB0cmFuc2Zvcm0uIElmIHRoZSB1c2VyIGludGVuZCB0byB1c2UgdGhlIHZpZXdCb3ggYXMgYVxuICAgICAgLy8gY2FtZXJhLCBqdXN0IHNldCBgb3B0Lmlnbm9yZVZpZXdCb3hgIGFzIGB0cnVlYCBhbmQgc2V0IHRyYW5zZnJvbVxuICAgICAgLy8gbWFudWFsbHkgYWNjb3JkaW5nIHRvIHRoZSB2aWV3Qm94IGluZm8gaW4gdGhlIG91dHB1dCBvZiB0aGlzIG1ldGhvZC5cbiAgICAgIHZhciBlbFJvb3QgPSByb290O1xuICAgICAgcm9vdCA9IG5ldyBHcm91cCgpO1xuICAgICAgcm9vdC5hZGQoZWxSb290KTtcbiAgICAgIGVsUm9vdC5zY2FsZSA9IHZpZXdCb3hUcmFuc2Zvcm0uc2NhbGUuc2xpY2UoKTtcbiAgICAgIGVsUm9vdC5wb3NpdGlvbiA9IHZpZXdCb3hUcmFuc2Zvcm0ucG9zaXRpb24uc2xpY2UoKTtcbiAgICB9XG4gIH0gLy8gU29tZSBzaGFwZXMgbWlnaHQgYmUgb3ZlcmZsb3cgdGhlIHZpZXdwb3J0LCB3aGljaCBzaG91bGQgYmVcbiAgLy8gY2xpcHBlZCBkZXNwaXRlIHdoZXRoZXIgdGhlIHZpZXdCb3ggaXMgdXNlZCwgYXMgdGhlIFNWRyBkb2VzLlxuXG5cbiAgaWYgKCFvcHQuaWdub3JlUm9vdENsaXAgJiYgd2lkdGggIT0gbnVsbCAmJiBoZWlnaHQgIT0gbnVsbCkge1xuICAgIHJvb3Quc2V0Q2xpcFBhdGgobmV3IFJlY3Qoe1xuICAgICAgc2hhcGU6IHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgd2lkdGg6IHdpZHRoLFxuICAgICAgICBoZWlnaHQ6IGhlaWdodFxuICAgICAgfVxuICAgIH0pKTtcbiAgfSAvLyBTZXQgd2lkdGgvaGVpZ2h0IG9uIGdyb3VwIGp1c3QgZm9yIG91dHB1dCB0aGUgdmlld3BvcnQgc2l6ZS5cblxuXG4gIHJldHVybiB7XG4gICAgcm9vdDogcm9vdCxcbiAgICB3aWR0aDogd2lkdGgsXG4gICAgaGVpZ2h0OiBoZWlnaHQsXG4gICAgdmlld0JveFJlY3Q6IHZpZXdCb3hSZWN0LFxuICAgIHZpZXdCb3hUcmFuc2Zvcm06IHZpZXdCb3hUcmFuc2Zvcm1cbiAgfTtcbn07XG5cblNWR1BhcnNlci5wcm90b3R5cGUuX3BhcnNlTm9kZSA9IGZ1bmN0aW9uICh4bWxOb2RlLCBwYXJlbnRHcm91cCkge1xuICB2YXIgbm9kZU5hbWUgPSB4bWxOb2RlLm5vZGVOYW1lLnRvTG93ZXJDYXNlKCk7IC8vIFRPRE9cbiAgLy8gc3VwcG9ydCA8c3R5bGU+Li4uPC9zdHlsZT4gaW4gc3ZnLCB3aGVyZSBub2RlTmFtZSBpcyAnc3R5bGUnLFxuICAvLyBDU1MgY2xhc3NlcyBpcyBkZWZpbmVkIGdsb2JhbGx5IHdoZXJldmVyIHRoZSBzdHlsZSB0YWdzIGFyZSBkZWNsYXJlZC5cblxuICBpZiAobm9kZU5hbWUgPT09ICdkZWZzJykge1xuICAgIC8vIGRlZmluZSBmbGFnXG4gICAgdGhpcy5faXNEZWZpbmUgPSB0cnVlO1xuICB9IGVsc2UgaWYgKG5vZGVOYW1lID09PSAndGV4dCcpIHtcbiAgICB0aGlzLl9pc1RleHQgPSB0cnVlO1xuICB9XG5cbiAgdmFyIGVsO1xuXG4gIGlmICh0aGlzLl9pc0RlZmluZSkge1xuICAgIHZhciBwYXJzZXIgPSBkZWZpbmVQYXJzZXJzW25vZGVOYW1lXTtcblxuICAgIGlmIChwYXJzZXIpIHtcbiAgICAgIHZhciBkZWYgPSBwYXJzZXIuY2FsbCh0aGlzLCB4bWxOb2RlKTtcbiAgICAgIHZhciBpZCA9IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCdpZCcpO1xuXG4gICAgICBpZiAoaWQpIHtcbiAgICAgICAgdGhpcy5fZGVmc1tpZF0gPSBkZWY7XG4gICAgICB9XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHZhciBwYXJzZXIgPSBub2RlUGFyc2Vyc1tub2RlTmFtZV07XG5cbiAgICBpZiAocGFyc2VyKSB7XG4gICAgICBlbCA9IHBhcnNlci5jYWxsKHRoaXMsIHhtbE5vZGUsIHBhcmVudEdyb3VwKTtcbiAgICAgIHBhcmVudEdyb3VwLmFkZChlbCk7XG4gICAgfVxuICB9XG5cbiAgdmFyIGNoaWxkID0geG1sTm9kZS5maXJzdENoaWxkO1xuXG4gIHdoaWxlIChjaGlsZCkge1xuICAgIGlmIChjaGlsZC5ub2RlVHlwZSA9PT0gMSkge1xuICAgICAgdGhpcy5fcGFyc2VOb2RlKGNoaWxkLCBlbCk7XG4gICAgfSAvLyBJcyB0ZXh0XG5cblxuICAgIGlmIChjaGlsZC5ub2RlVHlwZSA9PT0gMyAmJiB0aGlzLl9pc1RleHQpIHtcbiAgICAgIHRoaXMuX3BhcnNlVGV4dChjaGlsZCwgZWwpO1xuICAgIH1cblxuICAgIGNoaWxkID0gY2hpbGQubmV4dFNpYmxpbmc7XG4gIH0gLy8gUXVpdCBkZWZpbmVcblxuXG4gIGlmIChub2RlTmFtZSA9PT0gJ2RlZnMnKSB7XG4gICAgdGhpcy5faXNEZWZpbmUgPSBmYWxzZTtcbiAgfSBlbHNlIGlmIChub2RlTmFtZSA9PT0gJ3RleHQnKSB7XG4gICAgdGhpcy5faXNUZXh0ID0gZmFsc2U7XG4gIH1cbn07XG5cblNWR1BhcnNlci5wcm90b3R5cGUuX3BhcnNlVGV4dCA9IGZ1bmN0aW9uICh4bWxOb2RlLCBwYXJlbnRHcm91cCkge1xuICBpZiAoeG1sTm9kZS5ub2RlVHlwZSA9PT0gMSkge1xuICAgIHZhciBkeCA9IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCdkeCcpIHx8IDA7XG4gICAgdmFyIGR5ID0geG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ2R5JykgfHwgMDtcbiAgICB0aGlzLl90ZXh0WCArPSBwYXJzZUZsb2F0KGR4KTtcbiAgICB0aGlzLl90ZXh0WSArPSBwYXJzZUZsb2F0KGR5KTtcbiAgfVxuXG4gIHZhciB0ZXh0ID0gbmV3IFRleHQoe1xuICAgIHN0eWxlOiB7XG4gICAgICB0ZXh0OiB4bWxOb2RlLnRleHRDb250ZW50LFxuICAgICAgdHJhbnNmb3JtVGV4dDogdHJ1ZVxuICAgIH0sXG4gICAgcG9zaXRpb246IFt0aGlzLl90ZXh0WCB8fCAwLCB0aGlzLl90ZXh0WSB8fCAwXVxuICB9KTtcbiAgaW5oZXJpdFN0eWxlKHBhcmVudEdyb3VwLCB0ZXh0KTtcbiAgcGFyc2VBdHRyaWJ1dGVzKHhtbE5vZGUsIHRleHQsIHRoaXMuX2RlZnMpO1xuICB2YXIgZm9udFNpemUgPSB0ZXh0LnN0eWxlLmZvbnRTaXplO1xuXG4gIGlmIChmb250U2l6ZSAmJiBmb250U2l6ZSA8IDkpIHtcbiAgICAvLyBQRU5ESU5HXG4gICAgdGV4dC5zdHlsZS5mb250U2l6ZSA9IDk7XG4gICAgdGV4dC5zY2FsZSA9IHRleHQuc2NhbGUgfHwgWzEsIDFdO1xuICAgIHRleHQuc2NhbGVbMF0gKj0gZm9udFNpemUgLyA5O1xuICAgIHRleHQuc2NhbGVbMV0gKj0gZm9udFNpemUgLyA5O1xuICB9XG5cbiAgdmFyIHJlY3QgPSB0ZXh0LmdldEJvdW5kaW5nUmVjdCgpO1xuICB0aGlzLl90ZXh0WCArPSByZWN0LndpZHRoO1xuICBwYXJlbnRHcm91cC5hZGQodGV4dCk7XG4gIHJldHVybiB0ZXh0O1xufTtcblxudmFyIG5vZGVQYXJzZXJzID0ge1xuICAnZyc6IGZ1bmN0aW9uICh4bWxOb2RlLCBwYXJlbnRHcm91cCkge1xuICAgIHZhciBnID0gbmV3IEdyb3VwKCk7XG4gICAgaW5oZXJpdFN0eWxlKHBhcmVudEdyb3VwLCBnKTtcbiAgICBwYXJzZUF0dHJpYnV0ZXMoeG1sTm9kZSwgZywgdGhpcy5fZGVmcyk7XG4gICAgcmV0dXJuIGc7XG4gIH0sXG4gICdyZWN0JzogZnVuY3Rpb24gKHhtbE5vZGUsIHBhcmVudEdyb3VwKSB7XG4gICAgdmFyIHJlY3QgPSBuZXcgUmVjdCgpO1xuICAgIGluaGVyaXRTdHlsZShwYXJlbnRHcm91cCwgcmVjdCk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzKHhtbE5vZGUsIHJlY3QsIHRoaXMuX2RlZnMpO1xuICAgIHJlY3Quc2V0U2hhcGUoe1xuICAgICAgeDogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgneCcpIHx8IDApLFxuICAgICAgeTogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgneScpIHx8IDApLFxuICAgICAgd2lkdGg6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3dpZHRoJykgfHwgMCksXG4gICAgICBoZWlnaHQ6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ2hlaWdodCcpIHx8IDApXG4gICAgfSk7IC8vIGNvbnNvbGUubG9nKHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd0cmFuc2Zvcm0nKSk7XG4gICAgLy8gY29uc29sZS5sb2cocmVjdC50cmFuc2Zvcm0pO1xuXG4gICAgcmV0dXJuIHJlY3Q7XG4gIH0sXG4gICdjaXJjbGUnOiBmdW5jdGlvbiAoeG1sTm9kZSwgcGFyZW50R3JvdXApIHtcbiAgICB2YXIgY2lyY2xlID0gbmV3IENpcmNsZSgpO1xuICAgIGluaGVyaXRTdHlsZShwYXJlbnRHcm91cCwgY2lyY2xlKTtcbiAgICBwYXJzZUF0dHJpYnV0ZXMoeG1sTm9kZSwgY2lyY2xlLCB0aGlzLl9kZWZzKTtcbiAgICBjaXJjbGUuc2V0U2hhcGUoe1xuICAgICAgY3g6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ2N4JykgfHwgMCksXG4gICAgICBjeTogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgnY3knKSB8fCAwKSxcbiAgICAgIHI6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3InKSB8fCAwKVxuICAgIH0pO1xuICAgIHJldHVybiBjaXJjbGU7XG4gIH0sXG4gICdsaW5lJzogZnVuY3Rpb24gKHhtbE5vZGUsIHBhcmVudEdyb3VwKSB7XG4gICAgdmFyIGxpbmUgPSBuZXcgTGluZSgpO1xuICAgIGluaGVyaXRTdHlsZShwYXJlbnRHcm91cCwgbGluZSk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzKHhtbE5vZGUsIGxpbmUsIHRoaXMuX2RlZnMpO1xuICAgIGxpbmUuc2V0U2hhcGUoe1xuICAgICAgeDE6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3gxJykgfHwgMCksXG4gICAgICB5MTogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgneTEnKSB8fCAwKSxcbiAgICAgIHgyOiBwYXJzZUZsb2F0KHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd4MicpIHx8IDApLFxuICAgICAgeTI6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3kyJykgfHwgMClcbiAgICB9KTtcbiAgICByZXR1cm4gbGluZTtcbiAgfSxcbiAgJ2VsbGlwc2UnOiBmdW5jdGlvbiAoeG1sTm9kZSwgcGFyZW50R3JvdXApIHtcbiAgICB2YXIgZWxsaXBzZSA9IG5ldyBFbGxpcHNlKCk7XG4gICAgaW5oZXJpdFN0eWxlKHBhcmVudEdyb3VwLCBlbGxpcHNlKTtcbiAgICBwYXJzZUF0dHJpYnV0ZXMoeG1sTm9kZSwgZWxsaXBzZSwgdGhpcy5fZGVmcyk7XG4gICAgZWxsaXBzZS5zZXRTaGFwZSh7XG4gICAgICBjeDogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgnY3gnKSB8fCAwKSxcbiAgICAgIGN5OiBwYXJzZUZsb2F0KHhtbE5vZGUuZ2V0QXR0cmlidXRlKCdjeScpIHx8IDApLFxuICAgICAgcng6IHBhcnNlRmxvYXQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3J4JykgfHwgMCksXG4gICAgICByeTogcGFyc2VGbG9hdCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgncnknKSB8fCAwKVxuICAgIH0pO1xuICAgIHJldHVybiBlbGxpcHNlO1xuICB9LFxuICAncG9seWdvbic6IGZ1bmN0aW9uICh4bWxOb2RlLCBwYXJlbnRHcm91cCkge1xuICAgIHZhciBwb2ludHMgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgncG9pbnRzJyk7XG5cbiAgICBpZiAocG9pbnRzKSB7XG4gICAgICBwb2ludHMgPSBwYXJzZVBvaW50cyhwb2ludHMpO1xuICAgIH1cblxuICAgIHZhciBwb2x5Z29uID0gbmV3IFBvbHlnb24oe1xuICAgICAgc2hhcGU6IHtcbiAgICAgICAgcG9pbnRzOiBwb2ludHMgfHwgW11cbiAgICAgIH1cbiAgICB9KTtcbiAgICBpbmhlcml0U3R5bGUocGFyZW50R3JvdXAsIHBvbHlnb24pO1xuICAgIHBhcnNlQXR0cmlidXRlcyh4bWxOb2RlLCBwb2x5Z29uLCB0aGlzLl9kZWZzKTtcbiAgICByZXR1cm4gcG9seWdvbjtcbiAgfSxcbiAgJ3BvbHlsaW5lJzogZnVuY3Rpb24gKHhtbE5vZGUsIHBhcmVudEdyb3VwKSB7XG4gICAgdmFyIHBhdGggPSBuZXcgUGF0aCgpO1xuICAgIGluaGVyaXRTdHlsZShwYXJlbnRHcm91cCwgcGF0aCk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzKHhtbE5vZGUsIHBhdGgsIHRoaXMuX2RlZnMpO1xuICAgIHZhciBwb2ludHMgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgncG9pbnRzJyk7XG5cbiAgICBpZiAocG9pbnRzKSB7XG4gICAgICBwb2ludHMgPSBwYXJzZVBvaW50cyhwb2ludHMpO1xuICAgIH1cblxuICAgIHZhciBwb2x5bGluZSA9IG5ldyBQb2x5bGluZSh7XG4gICAgICBzaGFwZToge1xuICAgICAgICBwb2ludHM6IHBvaW50cyB8fCBbXVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBwb2x5bGluZTtcbiAgfSxcbiAgJ2ltYWdlJzogZnVuY3Rpb24gKHhtbE5vZGUsIHBhcmVudEdyb3VwKSB7XG4gICAgdmFyIGltZyA9IG5ldyBaSW1hZ2UoKTtcbiAgICBpbmhlcml0U3R5bGUocGFyZW50R3JvdXAsIGltZyk7XG4gICAgcGFyc2VBdHRyaWJ1dGVzKHhtbE5vZGUsIGltZywgdGhpcy5fZGVmcyk7XG4gICAgaW1nLnNldFN0eWxlKHtcbiAgICAgIGltYWdlOiB4bWxOb2RlLmdldEF0dHJpYnV0ZSgneGxpbms6aHJlZicpLFxuICAgICAgeDogeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3gnKSxcbiAgICAgIHk6IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd5JyksXG4gICAgICB3aWR0aDogeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3dpZHRoJyksXG4gICAgICBoZWlnaHQ6IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCdoZWlnaHQnKVxuICAgIH0pO1xuICAgIHJldHVybiBpbWc7XG4gIH0sXG4gICd0ZXh0JzogZnVuY3Rpb24gKHhtbE5vZGUsIHBhcmVudEdyb3VwKSB7XG4gICAgdmFyIHggPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgneCcpIHx8IDA7XG4gICAgdmFyIHkgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgneScpIHx8IDA7XG4gICAgdmFyIGR4ID0geG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ2R4JykgfHwgMDtcbiAgICB2YXIgZHkgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgnZHknKSB8fCAwO1xuICAgIHRoaXMuX3RleHRYID0gcGFyc2VGbG9hdCh4KSArIHBhcnNlRmxvYXQoZHgpO1xuICAgIHRoaXMuX3RleHRZID0gcGFyc2VGbG9hdCh5KSArIHBhcnNlRmxvYXQoZHkpO1xuICAgIHZhciBnID0gbmV3IEdyb3VwKCk7XG4gICAgaW5oZXJpdFN0eWxlKHBhcmVudEdyb3VwLCBnKTtcbiAgICBwYXJzZUF0dHJpYnV0ZXMoeG1sTm9kZSwgZywgdGhpcy5fZGVmcyk7XG4gICAgcmV0dXJuIGc7XG4gIH0sXG4gICd0c3Bhbic6IGZ1bmN0aW9uICh4bWxOb2RlLCBwYXJlbnRHcm91cCkge1xuICAgIHZhciB4ID0geG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3gnKTtcbiAgICB2YXIgeSA9IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd5Jyk7XG5cbiAgICBpZiAoeCAhPSBudWxsKSB7XG4gICAgICAvLyBuZXcgb2Zmc2V0IHhcbiAgICAgIHRoaXMuX3RleHRYID0gcGFyc2VGbG9hdCh4KTtcbiAgICB9XG5cbiAgICBpZiAoeSAhPSBudWxsKSB7XG4gICAgICAvLyBuZXcgb2Zmc2V0IHlcbiAgICAgIHRoaXMuX3RleHRZID0gcGFyc2VGbG9hdCh5KTtcbiAgICB9XG5cbiAgICB2YXIgZHggPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgnZHgnKSB8fCAwO1xuICAgIHZhciBkeSA9IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCdkeScpIHx8IDA7XG4gICAgdmFyIGcgPSBuZXcgR3JvdXAoKTtcbiAgICBpbmhlcml0U3R5bGUocGFyZW50R3JvdXAsIGcpO1xuICAgIHBhcnNlQXR0cmlidXRlcyh4bWxOb2RlLCBnLCB0aGlzLl9kZWZzKTtcbiAgICB0aGlzLl90ZXh0WCArPSBkeDtcbiAgICB0aGlzLl90ZXh0WSArPSBkeTtcbiAgICByZXR1cm4gZztcbiAgfSxcbiAgJ3BhdGgnOiBmdW5jdGlvbiAoeG1sTm9kZSwgcGFyZW50R3JvdXApIHtcbiAgICAvLyBUT0RPIHN2ZyBmaWxsIHJ1bGVcbiAgICAvLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9TVkcvQXR0cmlidXRlL2ZpbGwtcnVsZVxuICAgIC8vIHBhdGguc3R5bGUuZ2xvYmFsQ29tcG9zaXRlT3BlcmF0aW9uID0gJ3hvcic7XG4gICAgdmFyIGQgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZSgnZCcpIHx8ICcnOyAvLyBQZXJmb3JtYW5jZSBzZW5zaXRpdmUuXG5cbiAgICB2YXIgcGF0aCA9IGNyZWF0ZUZyb21TdHJpbmcoZCk7XG4gICAgaW5oZXJpdFN0eWxlKHBhcmVudEdyb3VwLCBwYXRoKTtcbiAgICBwYXJzZUF0dHJpYnV0ZXMoeG1sTm9kZSwgcGF0aCwgdGhpcy5fZGVmcyk7XG4gICAgcmV0dXJuIHBhdGg7XG4gIH1cbn07XG52YXIgZGVmaW5lUGFyc2VycyA9IHtcbiAgJ2xpbmVhcmdyYWRpZW50JzogZnVuY3Rpb24gKHhtbE5vZGUpIHtcbiAgICB2YXIgeDEgPSBwYXJzZUludCh4bWxOb2RlLmdldEF0dHJpYnV0ZSgneDEnKSB8fCAwLCAxMCk7XG4gICAgdmFyIHkxID0gcGFyc2VJbnQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3kxJykgfHwgMCwgMTApO1xuICAgIHZhciB4MiA9IHBhcnNlSW50KHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd4MicpIHx8IDEwLCAxMCk7XG4gICAgdmFyIHkyID0gcGFyc2VJbnQoeG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3kyJykgfHwgMCwgMTApO1xuICAgIHZhciBncmFkaWVudCA9IG5ldyBMaW5lYXJHcmFkaWVudCh4MSwgeTEsIHgyLCB5Mik7XG5cbiAgICBfcGFyc2VHcmFkaWVudENvbG9yU3RvcHMoeG1sTm9kZSwgZ3JhZGllbnQpO1xuXG4gICAgcmV0dXJuIGdyYWRpZW50O1xuICB9LFxuICAncmFkaWFsZ3JhZGllbnQnOiBmdW5jdGlvbiAoeG1sTm9kZSkge31cbn07XG5cbmZ1bmN0aW9uIF9wYXJzZUdyYWRpZW50Q29sb3JTdG9wcyh4bWxOb2RlLCBncmFkaWVudCkge1xuICB2YXIgc3RvcCA9IHhtbE5vZGUuZmlyc3RDaGlsZDtcblxuICB3aGlsZSAoc3RvcCkge1xuICAgIGlmIChzdG9wLm5vZGVUeXBlID09PSAxKSB7XG4gICAgICB2YXIgb2Zmc2V0ID0gc3RvcC5nZXRBdHRyaWJ1dGUoJ29mZnNldCcpO1xuXG4gICAgICBpZiAob2Zmc2V0LmluZGV4T2YoJyUnKSA+IDApIHtcbiAgICAgICAgLy8gcGVyY2VudGFnZVxuICAgICAgICBvZmZzZXQgPSBwYXJzZUludChvZmZzZXQsIDEwKSAvIDEwMDtcbiAgICAgIH0gZWxzZSBpZiAob2Zmc2V0KSB7XG4gICAgICAgIC8vIG51bWJlciBmcm9tIDAgdG8gMVxuICAgICAgICBvZmZzZXQgPSBwYXJzZUZsb2F0KG9mZnNldCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvZmZzZXQgPSAwO1xuICAgICAgfVxuXG4gICAgICB2YXIgc3RvcENvbG9yID0gc3RvcC5nZXRBdHRyaWJ1dGUoJ3N0b3AtY29sb3InKSB8fCAnIzAwMDAwMCc7XG4gICAgICBncmFkaWVudC5hZGRDb2xvclN0b3Aob2Zmc2V0LCBzdG9wQ29sb3IpO1xuICAgIH1cblxuICAgIHN0b3AgPSBzdG9wLm5leHRTaWJsaW5nO1xuICB9XG59XG5cbmZ1bmN0aW9uIGluaGVyaXRTdHlsZShwYXJlbnQsIGNoaWxkKSB7XG4gIGlmIChwYXJlbnQgJiYgcGFyZW50Ll9faW5oZXJpdGVkU3R5bGUpIHtcbiAgICBpZiAoIWNoaWxkLl9faW5oZXJpdGVkU3R5bGUpIHtcbiAgICAgIGNoaWxkLl9faW5oZXJpdGVkU3R5bGUgPSB7fTtcbiAgICB9XG5cbiAgICBkZWZhdWx0cyhjaGlsZC5fX2luaGVyaXRlZFN0eWxlLCBwYXJlbnQuX19pbmhlcml0ZWRTdHlsZSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcGFyc2VQb2ludHMocG9pbnRzU3RyaW5nKSB7XG4gIHZhciBsaXN0ID0gdHJpbShwb2ludHNTdHJpbmcpLnNwbGl0KERJTElNSVRFUl9SRUcpO1xuICB2YXIgcG9pbnRzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSArPSAyKSB7XG4gICAgdmFyIHggPSBwYXJzZUZsb2F0KGxpc3RbaV0pO1xuICAgIHZhciB5ID0gcGFyc2VGbG9hdChsaXN0W2kgKyAxXSk7XG4gICAgcG9pbnRzLnB1c2goW3gsIHldKTtcbiAgfVxuXG4gIHJldHVybiBwb2ludHM7XG59XG5cbnZhciBhdHRyaWJ1dGVzTWFwID0ge1xuICAnZmlsbCc6ICdmaWxsJyxcbiAgJ3N0cm9rZSc6ICdzdHJva2UnLFxuICAnc3Ryb2tlLXdpZHRoJzogJ2xpbmVXaWR0aCcsXG4gICdvcGFjaXR5JzogJ29wYWNpdHknLFxuICAnZmlsbC1vcGFjaXR5JzogJ2ZpbGxPcGFjaXR5JyxcbiAgJ3N0cm9rZS1vcGFjaXR5JzogJ3N0cm9rZU9wYWNpdHknLFxuICAnc3Ryb2tlLWRhc2hhcnJheSc6ICdsaW5lRGFzaCcsXG4gICdzdHJva2UtZGFzaG9mZnNldCc6ICdsaW5lRGFzaE9mZnNldCcsXG4gICdzdHJva2UtbGluZWNhcCc6ICdsaW5lQ2FwJyxcbiAgJ3N0cm9rZS1saW5lam9pbic6ICdsaW5lSm9pbicsXG4gICdzdHJva2UtbWl0ZXJsaW1pdCc6ICdtaXRlckxpbWl0JyxcbiAgJ2ZvbnQtZmFtaWx5JzogJ2ZvbnRGYW1pbHknLFxuICAnZm9udC1zaXplJzogJ2ZvbnRTaXplJyxcbiAgJ2ZvbnQtc3R5bGUnOiAnZm9udFN0eWxlJyxcbiAgJ2ZvbnQtd2VpZ2h0JzogJ2ZvbnRXZWlnaHQnLFxuICAndGV4dC1hbGlnbic6ICd0ZXh0QWxpZ24nLFxuICAnYWxpZ25tZW50LWJhc2VsaW5lJzogJ3RleHRCYXNlbGluZSdcbn07XG5cbmZ1bmN0aW9uIHBhcnNlQXR0cmlidXRlcyh4bWxOb2RlLCBlbCwgZGVmcywgb25seUlubGluZVN0eWxlKSB7XG4gIHZhciB6clN0eWxlID0gZWwuX19pbmhlcml0ZWRTdHlsZSB8fCB7fTtcbiAgdmFyIGlzVGV4dEVsID0gZWwudHlwZSA9PT0gJ3RleHQnOyAvLyBUT0RPIFNoYWRvd1xuXG4gIGlmICh4bWxOb2RlLm5vZGVUeXBlID09PSAxKSB7XG4gICAgcGFyc2VUcmFuc2Zvcm1BdHRyaWJ1dGUoeG1sTm9kZSwgZWwpO1xuICAgIGV4dGVuZCh6clN0eWxlLCBwYXJzZVN0eWxlQXR0cmlidXRlKHhtbE5vZGUpKTtcblxuICAgIGlmICghb25seUlubGluZVN0eWxlKSB7XG4gICAgICBmb3IgKHZhciBzdmdBdHRyTmFtZSBpbiBhdHRyaWJ1dGVzTWFwKSB7XG4gICAgICAgIGlmIChhdHRyaWJ1dGVzTWFwLmhhc093blByb3BlcnR5KHN2Z0F0dHJOYW1lKSkge1xuICAgICAgICAgIHZhciBhdHRyVmFsdWUgPSB4bWxOb2RlLmdldEF0dHJpYnV0ZShzdmdBdHRyTmFtZSk7XG5cbiAgICAgICAgICBpZiAoYXR0clZhbHVlICE9IG51bGwpIHtcbiAgICAgICAgICAgIHpyU3R5bGVbYXR0cmlidXRlc01hcFtzdmdBdHRyTmFtZV1dID0gYXR0clZhbHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHZhciBlbEZpbGxQcm9wID0gaXNUZXh0RWwgPyAndGV4dEZpbGwnIDogJ2ZpbGwnO1xuICB2YXIgZWxTdHJva2VQcm9wID0gaXNUZXh0RWwgPyAndGV4dFN0cm9rZScgOiAnc3Ryb2tlJztcbiAgZWwuc3R5bGUgPSBlbC5zdHlsZSB8fCBuZXcgU3R5bGUoKTtcbiAgdmFyIGVsU3R5bGUgPSBlbC5zdHlsZTtcbiAgenJTdHlsZS5maWxsICE9IG51bGwgJiYgZWxTdHlsZS5zZXQoZWxGaWxsUHJvcCwgZ2V0UGFpbnQoenJTdHlsZS5maWxsLCBkZWZzKSk7XG4gIHpyU3R5bGUuc3Ryb2tlICE9IG51bGwgJiYgZWxTdHlsZS5zZXQoZWxTdHJva2VQcm9wLCBnZXRQYWludCh6clN0eWxlLnN0cm9rZSwgZGVmcykpO1xuICBlYWNoKFsnbGluZVdpZHRoJywgJ29wYWNpdHknLCAnZmlsbE9wYWNpdHknLCAnc3Ryb2tlT3BhY2l0eScsICdtaXRlckxpbWl0JywgJ2ZvbnRTaXplJ10sIGZ1bmN0aW9uIChwcm9wTmFtZSkge1xuICAgIHZhciBlbFByb3BOYW1lID0gcHJvcE5hbWUgPT09ICdsaW5lV2lkdGgnICYmIGlzVGV4dEVsID8gJ3RleHRTdHJva2VXaWR0aCcgOiBwcm9wTmFtZTtcbiAgICB6clN0eWxlW3Byb3BOYW1lXSAhPSBudWxsICYmIGVsU3R5bGUuc2V0KGVsUHJvcE5hbWUsIHBhcnNlRmxvYXQoenJTdHlsZVtwcm9wTmFtZV0pKTtcbiAgfSk7XG5cbiAgaWYgKCF6clN0eWxlLnRleHRCYXNlbGluZSB8fCB6clN0eWxlLnRleHRCYXNlbGluZSA9PT0gJ2F1dG8nKSB7XG4gICAgenJTdHlsZS50ZXh0QmFzZWxpbmUgPSAnYWxwaGFiZXRpYyc7XG4gIH1cblxuICBpZiAoenJTdHlsZS50ZXh0QmFzZWxpbmUgPT09ICdhbHBoYWJldGljJykge1xuICAgIHpyU3R5bGUudGV4dEJhc2VsaW5lID0gJ2JvdHRvbSc7XG4gIH1cblxuICBpZiAoenJTdHlsZS50ZXh0QWxpZ24gPT09ICdzdGFydCcpIHtcbiAgICB6clN0eWxlLnRleHRBbGlnbiA9ICdsZWZ0JztcbiAgfVxuXG4gIGlmICh6clN0eWxlLnRleHRBbGlnbiA9PT0gJ2VuZCcpIHtcbiAgICB6clN0eWxlLnRleHRBbGlnbiA9ICdyaWdodCc7XG4gIH1cblxuICBlYWNoKFsnbGluZURhc2hPZmZzZXQnLCAnbGluZUNhcCcsICdsaW5lSm9pbicsICdmb250V2VpZ2h0JywgJ2ZvbnRGYW1pbHknLCAnZm9udFN0eWxlJywgJ3RleHRBbGlnbicsICd0ZXh0QmFzZWxpbmUnXSwgZnVuY3Rpb24gKHByb3BOYW1lKSB7XG4gICAgenJTdHlsZVtwcm9wTmFtZV0gIT0gbnVsbCAmJiBlbFN0eWxlLnNldChwcm9wTmFtZSwgenJTdHlsZVtwcm9wTmFtZV0pO1xuICB9KTtcblxuICBpZiAoenJTdHlsZS5saW5lRGFzaCkge1xuICAgIGVsLnN0eWxlLmxpbmVEYXNoID0gdHJpbSh6clN0eWxlLmxpbmVEYXNoKS5zcGxpdChESUxJTUlURVJfUkVHKTtcbiAgfVxuXG4gIGlmIChlbFN0eWxlW2VsU3Ryb2tlUHJvcF0gJiYgZWxTdHlsZVtlbFN0cm9rZVByb3BdICE9PSAnbm9uZScpIHtcbiAgICAvLyBlbmFibGUgc3Ryb2tlXG4gICAgZWxbZWxTdHJva2VQcm9wXSA9IHRydWU7XG4gIH1cblxuICBlbC5fX2luaGVyaXRlZFN0eWxlID0genJTdHlsZTtcbn1cblxudmFyIHVybFJlZ2V4ID0gL3VybFxcKFxccyojKC4qPylcXCkvO1xuXG5mdW5jdGlvbiBnZXRQYWludChzdHIsIGRlZnMpIHtcbiAgLy8gaWYgKHN0ciA9PT0gJ25vbmUnKSB7XG4gIC8vICAgICByZXR1cm47XG4gIC8vIH1cbiAgdmFyIHVybE1hdGNoID0gZGVmcyAmJiBzdHIgJiYgc3RyLm1hdGNoKHVybFJlZ2V4KTtcblxuICBpZiAodXJsTWF0Y2gpIHtcbiAgICB2YXIgdXJsID0gdHJpbSh1cmxNYXRjaFsxXSk7XG4gICAgdmFyIGRlZiA9IGRlZnNbdXJsXTtcbiAgICByZXR1cm4gZGVmO1xuICB9XG5cbiAgcmV0dXJuIHN0cjtcbn1cblxudmFyIHRyYW5zZm9ybVJlZ2V4ID0gLyh0cmFuc2xhdGV8c2NhbGV8cm90YXRlfHNrZXdYfHNrZXdZfG1hdHJpeClcXCgoW1xcLVxcczAtOVxcLmUsXSopXFwpL2c7XG5cbmZ1bmN0aW9uIHBhcnNlVHJhbnNmb3JtQXR0cmlidXRlKHhtbE5vZGUsIG5vZGUpIHtcbiAgdmFyIHRyYW5zZm9ybSA9IHhtbE5vZGUuZ2V0QXR0cmlidXRlKCd0cmFuc2Zvcm0nKTtcblxuICBpZiAodHJhbnNmb3JtKSB7XG4gICAgdHJhbnNmb3JtID0gdHJhbnNmb3JtLnJlcGxhY2UoLywvZywgJyAnKTtcbiAgICB2YXIgbSA9IG51bGw7XG4gICAgdmFyIHRyYW5zZm9ybU9wcyA9IFtdO1xuICAgIHRyYW5zZm9ybS5yZXBsYWNlKHRyYW5zZm9ybVJlZ2V4LCBmdW5jdGlvbiAoc3RyLCB0eXBlLCB2YWx1ZSkge1xuICAgICAgdHJhbnNmb3JtT3BzLnB1c2godHlwZSwgdmFsdWUpO1xuICAgIH0pO1xuXG4gICAgZm9yICh2YXIgaSA9IHRyYW5zZm9ybU9wcy5sZW5ndGggLSAxOyBpID4gMDsgaSAtPSAyKSB7XG4gICAgICB2YXIgdmFsdWUgPSB0cmFuc2Zvcm1PcHNbaV07XG4gICAgICB2YXIgdHlwZSA9IHRyYW5zZm9ybU9wc1tpIC0gMV07XG4gICAgICBtID0gbSB8fCBtYXRyaXguY3JlYXRlKCk7XG5cbiAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICBjYXNlICd0cmFuc2xhdGUnOlxuICAgICAgICAgIHZhbHVlID0gdHJpbSh2YWx1ZSkuc3BsaXQoRElMSU1JVEVSX1JFRyk7XG4gICAgICAgICAgbWF0cml4LnRyYW5zbGF0ZShtLCBtLCBbcGFyc2VGbG9hdCh2YWx1ZVswXSksIHBhcnNlRmxvYXQodmFsdWVbMV0gfHwgMCldKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdzY2FsZSc6XG4gICAgICAgICAgdmFsdWUgPSB0cmltKHZhbHVlKS5zcGxpdChESUxJTUlURVJfUkVHKTtcbiAgICAgICAgICBtYXRyaXguc2NhbGUobSwgbSwgW3BhcnNlRmxvYXQodmFsdWVbMF0pLCBwYXJzZUZsb2F0KHZhbHVlWzFdIHx8IHZhbHVlWzBdKV0pO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ3JvdGF0ZSc6XG4gICAgICAgICAgdmFsdWUgPSB0cmltKHZhbHVlKS5zcGxpdChESUxJTUlURVJfUkVHKTtcbiAgICAgICAgICBtYXRyaXgucm90YXRlKG0sIG0sIHBhcnNlRmxvYXQodmFsdWVbMF0pKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdza2V3JzpcbiAgICAgICAgICB2YWx1ZSA9IHRyaW0odmFsdWUpLnNwbGl0KERJTElNSVRFUl9SRUcpO1xuICAgICAgICAgIGNvbnNvbGUud2FybignU2tldyB0cmFuc2Zvcm0gaXMgbm90IHN1cHBvcnRlZCB5ZXQnKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdtYXRyaXgnOlxuICAgICAgICAgIHZhciB2YWx1ZSA9IHRyaW0odmFsdWUpLnNwbGl0KERJTElNSVRFUl9SRUcpO1xuICAgICAgICAgIG1bMF0gPSBwYXJzZUZsb2F0KHZhbHVlWzBdKTtcbiAgICAgICAgICBtWzFdID0gcGFyc2VGbG9hdCh2YWx1ZVsxXSk7XG4gICAgICAgICAgbVsyXSA9IHBhcnNlRmxvYXQodmFsdWVbMl0pO1xuICAgICAgICAgIG1bM10gPSBwYXJzZUZsb2F0KHZhbHVlWzNdKTtcbiAgICAgICAgICBtWzRdID0gcGFyc2VGbG9hdCh2YWx1ZVs0XSk7XG4gICAgICAgICAgbVs1XSA9IHBhcnNlRmxvYXQodmFsdWVbNV0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIG5vZGUuc2V0TG9jYWxUcmFuc2Zvcm0obSk7XG4gIH1cbn0gLy8gVmFsdWUgbWF5IGNvbnRhaW4gc3BhY2UuXG5cblxudmFyIHN0eWxlUmVnZXggPSAvKFteXFxzOjtdKylcXHMqOlxccyooW146O10rKS9nO1xuXG5mdW5jdGlvbiBwYXJzZVN0eWxlQXR0cmlidXRlKHhtbE5vZGUpIHtcbiAgdmFyIHN0eWxlID0geG1sTm9kZS5nZXRBdHRyaWJ1dGUoJ3N0eWxlJyk7XG4gIHZhciByZXN1bHQgPSB7fTtcblxuICBpZiAoIXN0eWxlKSB7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxuXG4gIHZhciBzdHlsZUxpc3QgPSB7fTtcbiAgc3R5bGVSZWdleC5sYXN0SW5kZXggPSAwO1xuICB2YXIgc3R5bGVSZWdSZXN1bHQ7XG5cbiAgd2hpbGUgKChzdHlsZVJlZ1Jlc3VsdCA9IHN0eWxlUmVnZXguZXhlYyhzdHlsZSkpICE9IG51bGwpIHtcbiAgICBzdHlsZUxpc3Rbc3R5bGVSZWdSZXN1bHRbMV1dID0gc3R5bGVSZWdSZXN1bHRbMl07XG4gIH1cblxuICBmb3IgKHZhciBzdmdBdHRyTmFtZSBpbiBhdHRyaWJ1dGVzTWFwKSB7XG4gICAgaWYgKGF0dHJpYnV0ZXNNYXAuaGFzT3duUHJvcGVydHkoc3ZnQXR0ck5hbWUpICYmIHN0eWxlTGlzdFtzdmdBdHRyTmFtZV0gIT0gbnVsbCkge1xuICAgICAgcmVzdWx0W2F0dHJpYnV0ZXNNYXBbc3ZnQXR0ck5hbWVdXSA9IHN0eWxlTGlzdFtzdmdBdHRyTmFtZV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gdmlld0JveFJlY3RcbiAqIEBwYXJhbSB7bnVtYmVyfSB3aWR0aFxuICogQHBhcmFtIHtudW1iZXJ9IGhlaWdodFxuICogQHJldHVybiB7T2JqZWN0fSB7c2NhbGUsIHBvc2l0aW9ufVxuICovXG5cblxuZnVuY3Rpb24gbWFrZVZpZXdCb3hUcmFuc2Zvcm0odmlld0JveFJlY3QsIHdpZHRoLCBoZWlnaHQpIHtcbiAgdmFyIHNjYWxlWCA9IHdpZHRoIC8gdmlld0JveFJlY3Qud2lkdGg7XG4gIHZhciBzY2FsZVkgPSBoZWlnaHQgLyB2aWV3Qm94UmVjdC5oZWlnaHQ7XG4gIHZhciBzY2FsZSA9IE1hdGgubWluKHNjYWxlWCwgc2NhbGVZKTsgLy8gcHJlc2VydmVBc3BlY3RSYXRpbyAneE1pZFlNaWQnXG5cbiAgdmFyIHZpZXdCb3hTY2FsZSA9IFtzY2FsZSwgc2NhbGVdO1xuICB2YXIgdmlld0JveFBvc2l0aW9uID0gWy0odmlld0JveFJlY3QueCArIHZpZXdCb3hSZWN0LndpZHRoIC8gMikgKiBzY2FsZSArIHdpZHRoIC8gMiwgLSh2aWV3Qm94UmVjdC55ICsgdmlld0JveFJlY3QuaGVpZ2h0IC8gMikgKiBzY2FsZSArIGhlaWdodCAvIDJdO1xuICByZXR1cm4ge1xuICAgIHNjYWxlOiB2aWV3Qm94U2NhbGUsXG4gICAgcG9zaXRpb246IHZpZXdCb3hQb3NpdGlvblxuICB9O1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ3xYTUxFbGVtZW50fSB4bWxcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0XVxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQud2lkdGhdIERlZmF1bHQgd2lkdGggaWYgc3ZnIHdpZHRoIG5vdCBzcGVjaWZpZWQgb3IgaXMgYSBwZXJjZW50IHZhbHVlLlxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQuaGVpZ2h0XSBEZWZhdWx0IGhlaWdodCBpZiBzdmcgaGVpZ2h0IG5vdCBzcGVjaWZpZWQgb3IgaXMgYSBwZXJjZW50IHZhbHVlLlxuICogQHBhcmFtIHtib29sZWFufSBbb3B0Lmlnbm9yZVZpZXdCb3hdXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHQuaWdub3JlUm9vdENsaXBdXG4gKiBAcmV0dXJuIHtPYmplY3R9IHJlc3VsdDpcbiAqIHtcbiAqICAgICByb290OiBHcm91cCwgVGhlIHJvb3Qgb2YgdGhlIHRoZSByZXN1bHQgdHJlZSBvZiB6cmVuZGVyIHNoYXBlcyxcbiAqICAgICB3aWR0aDogbnVtYmVyLCB0aGUgdmlld3BvcnQgd2lkdGggb2YgdGhlIFNWRyxcbiAqICAgICBoZWlnaHQ6IG51bWJlciwgdGhlIHZpZXdwb3J0IGhlaWdodCBvZiB0aGUgU1ZHLFxuICogICAgIHZpZXdCb3hSZWN0OiB7eCwgeSwgd2lkdGgsIGhlaWdodH0sIHRoZSBkZWNsYXJlZCB2aWV3Qm94IHJlY3Qgb2YgdGhlIFNWRywgaWYgZXhpc3RzLFxuICogICAgIHZpZXdCb3hUcmFuc2Zvcm06IHRoZSB7c2NhbGUsIHBvc2l0aW9ufSBjYWxjdWxhdGVkIGJ5IHZpZXdCb3ggYW5kIHZpZXdwb3J0LCBpcyBleGlzdHMuXG4gKiB9XG4gKi9cblxuXG5mdW5jdGlvbiBwYXJzZVNWRyh4bWwsIG9wdCkge1xuICB2YXIgcGFyc2VyID0gbmV3IFNWR1BhcnNlcigpO1xuICByZXR1cm4gcGFyc2VyLnBhcnNlKHhtbCwgb3B0KTtcbn1cblxuZXhwb3J0cy5wYXJzZVhNTCA9IHBhcnNlWE1MO1xuZXhwb3J0cy5tYWtlVmlld0JveFRyYW5zZm9ybSA9IG1ha2VWaWV3Qm94VHJhbnNmb3JtO1xuZXhwb3J0cy5wYXJzZVNWRyA9IHBhcnNlU1ZHOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkpBO0FBcUpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFDQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQUNBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTdCQTtBQStCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/tool/parseSVG.js
