__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FormItemEmail; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/useful-form-item/email.tsx";





var generatePropsName = function generatePropsName(propName, dataId) {
  if (!!dataId) return "".concat(dataId, "-").concat(propName);
  return propName;
};

var FormItemEmail =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(FormItemEmail, _ModelWidget);

  function FormItemEmail() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, FormItemEmail);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(FormItemEmail).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(FormItemEmail, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          label = _this$props.label,
          dataId = _this$props.dataId,
          form = _this$props.form,
          model = _this$props.model,
          _this$props$required = _this$props.required,
          required = _this$props$required === void 0 ? false : _this$props$required,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["label", "dataId", "form", "model", "required"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NText"], Object.assign({
        form: form,
        model: model,
        propName: generatePropsName("email", dataId),
        size: "large",
        label: label || _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Email".toUpperCase()).thai("อีเมล์").my("အီးမေးလ်ပို့ရန်").getMessage(),
        rules: [{
          required: required,
          message: _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Email is invalid").thai("Email is invalid").my("အီးမေးလ်ပို့ရန်လိုအပ်ပါသည်").getMessage(),
          type: "email"
        }]
      }, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        },
        __self: this
      }));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }]);

  return FormItemEmail;
}(_component__WEBPACK_IMPORTED_MODULE_7__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vZW1haWwudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vZW1haWwudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcblxuY29uc3QgZ2VuZXJhdGVQcm9wc05hbWUgPSAocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkPzogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgaWYgKCEhZGF0YUlkKSByZXR1cm4gYCR7ZGF0YUlkfS0ke3Byb3BOYW1lfWA7XG4gIHJldHVybiBwcm9wTmFtZTtcbn07XG5cbnR5cGUgSVByb3BzID0ge1xuICBsYWJlbD86IGFueTtcbiAgbW9kZWw6IGFueTtcbiAgZm9ybTogYW55O1xuICBkYXRhSWQ/OiBzdHJpbmc7XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbn0gJiBNb2RlbFdpZGdldFByb3BzICYgRm9ybUNvbXBvbmVudFByb3BzO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBGb3JtSXRlbUVtYWlsPFAgZXh0ZW5kcyBJUHJvcHMsIFMsIEM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBsYWJlbCwgZGF0YUlkLCBmb3JtLCBtb2RlbCwgcmVxdWlyZWQgPSBmYWxzZSwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPE5UZXh0XG4gICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcHNOYW1lKFwiZW1haWxcIiwgZGF0YUlkKX1cbiAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgbGFiZWw9e2xhYmVsIHx8IExhbmd1YWdlLmVuKFwiRW1haWxcIi50b1VwcGVyQ2FzZSgpKVxuICAgICAgICAgIC50aGFpKFwi4Lit4Li14LmA4Lih4Lil4LmMXCIpXG4gICAgICAgICAgLm15KFwi4YCh4YCu4YC44YCZ4YCx4YC44YCc4YC64YCV4YCt4YCv4YC34YCb4YCU4YC6XCIpXG4gICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgcnVsZXM9e1tcbiAgICAgICAgICB7XG4gICAgICAgICAgICByZXF1aXJlZDogcmVxdWlyZWQsXG4gICAgICAgICAgICBtZXNzYWdlOiBMYW5ndWFnZS5lbihcIkVtYWlsIGlzIGludmFsaWRcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCJFbWFpbCBpcyBpbnZhbGlkXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAoeGAruGAuOGAmeGAseGAuOGAnOGAuuGAleGAreGAr+GAt+GAm+GAlOGAuuGAnOGAreGAr+GAoeGAleGAuuGAleGAq+GAnuGAiuGAulwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgdHlwZTogXCJlbWFpbFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgIF19XG4gICAgICAgIHsuLi5yZXN0fVxuICAgICAgLz5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7fSBhcyBDO1xuICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVNBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFJQTtBQU5BO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzQkE7OztBQUVBO0FBQ0E7QUFDQTs7OztBQTlCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/useful-form-item/email.tsx
