__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetContext", function() { return WidgetContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetRouterHelper", function() { return WidgetRouterHelper; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _src_app_desk_component_spin__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../src/app/desk/component/spin */ "./src/app/desk/component/spin.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");










/**
 * widget router helper
 */

var WidgetRouterHelper =
/*#__PURE__*/
function () {
  function WidgetRouterHelper(widget) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, WidgetRouterHelper);

    this.widget = void 0;
    this.widget = widget;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(WidgetRouterHelper, [{
    key: "getWidget",
    value: function getWidget() {
      return this.widget;
    }
  }, {
    key: "forward",
    value: function forward() {
      this.getHistory().goForward();
      return this;
    }
  }, {
    key: "back",
    value: function back() {
      this.getHistory().goBack();
      return this;
    }
  }, {
    key: "go",
    value: function go(steps) {
      this.getHistory().go(steps);
      return this;
    }
  }, {
    key: "getHistory",
    value: function getHistory() {
      return _common__WEBPACK_IMPORTED_MODULE_6__["Envs"].application().getHistory();
    }
  }, {
    key: "redirect",
    value: function redirect(to) {
      var push = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (!_common__WEBPACK_IMPORTED_MODULE_6__["Utils"].getPureMobile() && _common__WEBPACK_IMPORTED_MODULE_6__["Envs"].os.ios && _common__WEBPACK_IMPORTED_MODULE_6__["Envs"].os.wechat) {
        window.location.assign(_common__WEBPACK_IMPORTED_MODULE_6__["Envs"].getContextLocation(to));
      } else {
        var history = this.getHistory();

        if (push) {
          history.push(to);
        } else {
          history.replace(to);
        }
      }
    }
  }, {
    key: "pushRedirect",
    value: function pushRedirect(to) {
      this.redirect(to, true);
    }
  }, {
    key: "replaceRedirect",
    value: function replaceRedirect(to) {
      this.redirect(to, false);
    }
  }]);

  return WidgetRouterHelper;
}();
/**
 * widget ajax helper
 */


var WidgetAjaxHelper =
/*#__PURE__*/
function () {
  function WidgetAjaxHelper(widget) {
    var _this = this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, WidgetAjaxHelper);

    this.widget = void 0;

    this.on4xx = function (error) {
      if (error.status >= 400 && error.status < 500) {
        switch (error.status) {
          case 401:
            _this.on401(error);

            break;

          case 403:
            _this.on403(error);

            break;

          default:
            // do nothing
            break;
        }
      }
    };

    this.on5xx = function (error) {
      if (error.status >= 500) {
        switch (error.status) {
          case 500:
            _this.getWidget().getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_6__["PATH"].ERROR_500);

            break;

          default:
        }
      }
    };

    this.on2xx = function (error) {
      switch (error.status) {
        case 200:
          var _ref = error.body || {},
              respCode = _ref.respCode,
              respMessage = _ref.respMessage; // notification.error({
          //   message: respCode,
          //   description: respMessage
          // });


          break;

        default:
      }
    };

    this.widget = widget;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(WidgetAjaxHelper, [{
    key: "getWidget",
    value: function getWidget() {
      return this.widget;
    }
  }, {
    key: "doAjax",
    value: function doAjax(url, data) {
      var _this2 = this;

      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var action = arguments.length > 3 ? arguments[3] : undefined;
      return new Promise(function (resolve, reject) {
        var isLoading = !options.hasOwnProperty("loading") || options.loading;
        var wrapClassName = (options || {}).wrapClassName || "";
        var opacityNum = (options || {}).opacityNum || 0;

        try {
          if (isLoading) {
            _src_app_desk_component_spin__WEBPACK_IMPORTED_MODULE_8__["default"].spinShow(wrapClassName, opacityNum);
          }

          action.call(_common__WEBPACK_IMPORTED_MODULE_6__["Ajax"], url, data, options).then(function (response) {
            if (isLoading) {
              _src_app_desk_component_spin__WEBPACK_IMPORTED_MODULE_8__["default"].spinClose();
            }

            resolve(response);
          }).catch(function (error) {
            !(options.ignore4xx || options.ignoreFail) && _this2.on4xx(error);
            !(options.ignore5xx || options.ignoreFail) && _this2.on5xx(error);
            !(options.ignore2xx || options.ignoreFail) && _this2.on2xx(error);

            if (isLoading) {
              _src_app_desk_component_spin__WEBPACK_IMPORTED_MODULE_8__["default"].spinClose();
            }

            reject(error);
          });
        } finally {}
      });
    }
  }, {
    key: "put",
    value: function put(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].put);
    }
  }, {
    key: "post",
    value: function post(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].post);
    }
  }, {
    key: "patch",
    value: function patch(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].patch);
    }
  }, {
    key: "get",
    value: function get(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].get);
    }
  }, {
    key: "delete",
    value: function _delete(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].delete);
    }
    /**
     * arguments are instances of FailFastPromise
     */

  }, {
    key: "all",
    value: function all(promises) {
      return _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].all(promises);
    }
  }, {
    key: "ajax",
    value: function ajax(url, data, options) {
      return this.doAjax(url, data, options, _common__WEBPACK_IMPORTED_MODULE_6__["Ajax"].ajax);
    }
  }, {
    key: "on401",
    value: function on401(error) {
      this.getWidget().getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_6__["PATH"].SIGN_IN);
    }
  }, {
    key: "on403",
    value: function on403(error) {
      // deal with 403, no access permitted exception
      this.getWidget().getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_6__["PATH"].ERROR_403);
    }
  }]);

  return WidgetAjaxHelper;
}();
/**
 * helpers
 */


var Helpers =
/*#__PURE__*/
function () {
  function Helpers(widget) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Helpers);

    this.widget = void 0;
    this.router = void 0;
    this.ajax = void 0;
    this.widget = widget;
  }
  /**
   * @return {Widget}
   */


  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Helpers, [{
    key: "getWidget",
    value: function getWidget() {
      return this.widget;
    }
  }, {
    key: "getRouter",
    value: function getRouter() {
      return this.router ? this.router : this.router = new WidgetRouterHelper(this.getWidget());
    }
    /**
     * @return {WidgetAjaxHelper}
     */

  }, {
    key: "getAjax",
    value: function getAjax() {
      return this.ajax ? this.ajax : this.ajax = new WidgetAjaxHelper(this.getWidget());
    }
  }]);

  return Helpers;
}();
/**
 * all component should extends from this.
 * state has been initialized as {}
 */


var Widget =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Widget, _React$Component);

  function Widget(props, context) {
    var _this3;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Widget);

    _this3 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_1__["default"])(Widget).call(this, props, context));
    _this3.C = void 0;
    _this3.helpers = void 0;
    Object(_component__WEBPACK_IMPORTED_MODULE_9__["registerIntoRuntime"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3));
    _this3.state = _this3.initState();
    _this3.helpers = new Helpers(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3));
    _this3.C = _this3.initComponents();
    return _this3;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Widget, [{
    key: "initState",
    value: function initState() {
      return {};
    }
  }, {
    key: "getComponents",
    value: function getComponents() {
      return this.C;
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState, nextContext) {
      return true;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState, prevContext) {}
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      Object(_component__WEBPACK_IMPORTED_MODULE_9__["unregisterFromRuntime"])(this);
    }
    /**
     * @return {Helpers}
     */

  }, {
    key: "getHelpers",
    value: function getHelpers() {
      return this.helpers;
    }
  }, {
    key: "renderCSS",
    value: function renderCSS(theme) {
      if (this.props.css) {
        return this.props.css.call(this, this.props, theme);
      } else {
        return "";
      }
    }
  }]);

  return Widget;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Component);

var WidgetContext = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createContext({});
Widget.contextType = WidgetContext;
/* harmony default export */ __webpack_exports__["default"] = (Widget);
//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L3dpZGdldC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvd2lkZ2V0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBamF4LCBFbnZzLCBQQVRILCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHNwaW4gZnJvbSBcIi4uLy4uL3NyYy9hcHAvZGVzay9jb21wb25lbnQvc3BpblwiO1xuaW1wb3J0IHtcbiAgQWpheFJlc3BvbnNlLFxuICBJV2lkZ2V0QWpheEhlbHBlcixcbiAgSVdpZGdldFJvdXRlckhlbHBlcixcbiAgT1BUSU9OUyxcbiAgU3R5bGVkUHJvcHNUaGVtZSxcbiAgV2lkZ2V0SGVscGVkLFxuICBXaWRnZXRIZWxwZXJzLFxuICBXaWRnZXRQcm9wcyxcbn0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgcmVnaXN0ZXJJbnRvUnVudGltZSwgdW5yZWdpc3RlckZyb21SdW50aW1lIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcblxuLyoqXG4gKiB3aWRnZXQgcm91dGVyIGhlbHBlclxuICovXG5jbGFzcyBXaWRnZXRSb3V0ZXJIZWxwZXIgaW1wbGVtZW50cyBJV2lkZ2V0Um91dGVySGVscGVyIHtcbiAgcHJpdmF0ZSB3aWRnZXQ6IFdpZGdldDxhbnksIGFueSwgYW55PjtcblxuICBjb25zdHJ1Y3Rvcih3aWRnZXQ6IFdpZGdldDxhbnksIGFueSwgYW55Pikge1xuICAgIHRoaXMud2lkZ2V0ID0gd2lkZ2V0O1xuICB9XG5cbiAgZ2V0V2lkZ2V0KCk6IFdpZGdldDxhbnksIGFueSwgYW55PiB7XG4gICAgcmV0dXJuIHRoaXMud2lkZ2V0O1xuICB9XG5cbiAgZm9yd2FyZCgpOiB0aGlzIHtcbiAgICB0aGlzLmdldEhpc3RvcnkoKS5nb0ZvcndhcmQoKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIGJhY2soKTogdGhpcyB7XG4gICAgdGhpcy5nZXRIaXN0b3J5KCkuZ29CYWNrKCk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBnbyhzdGVwczogbnVtYmVyKTogdGhpcyB7XG4gICAgdGhpcy5nZXRIaXN0b3J5KCkuZ28oc3RlcHMpO1xuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgZ2V0SGlzdG9yeSgpOiBhbnkge1xuICAgIHJldHVybiBFbnZzLmFwcGxpY2F0aW9uKCkhLmdldEhpc3RvcnkoKTtcbiAgfVxuXG4gIHJlZGlyZWN0KHRvOiBzdHJpbmcsIHB1c2g6IGJvb2xlYW4gPSBmYWxzZSkge1xuICAgIGlmICghVXRpbHMuZ2V0UHVyZU1vYmlsZSgpICYmIEVudnMub3MuaW9zICYmIEVudnMub3Mud2VjaGF0KSB7XG4gICAgICB3aW5kb3cubG9jYXRpb24uYXNzaWduKEVudnMuZ2V0Q29udGV4dExvY2F0aW9uKHRvKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IGhpc3RvcnkgPSB0aGlzLmdldEhpc3RvcnkoKTtcbiAgICAgIGlmIChwdXNoKSB7XG4gICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBoaXN0b3J5LnJlcGxhY2UodG8pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHB1c2hSZWRpcmVjdCh0bzogc3RyaW5nKTogdm9pZCB7XG4gICAgdGhpcy5yZWRpcmVjdCh0bywgdHJ1ZSk7XG4gIH1cblxuICByZXBsYWNlUmVkaXJlY3QodG86IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMucmVkaXJlY3QodG8sIGZhbHNlKTtcbiAgfVxufVxuXG4vKipcbiAqIHdpZGdldCBhamF4IGhlbHBlclxuICovXG5jbGFzcyBXaWRnZXRBamF4SGVscGVyIGltcGxlbWVudHMgSVdpZGdldEFqYXhIZWxwZXIge1xuICBwcml2YXRlIHdpZGdldDogV2lkZ2V0PGFueSwgYW55LCBhbnk+O1xuXG4gIGNvbnN0cnVjdG9yKHdpZGdldDogV2lkZ2V0PGFueSwgYW55LCBhbnk+KSB7XG4gICAgdGhpcy53aWRnZXQgPSB3aWRnZXQ7XG4gIH1cblxuICBnZXRXaWRnZXQoKTogV2lkZ2V0PGFueSwgYW55LCBhbnk+IHtcbiAgICByZXR1cm4gdGhpcy53aWRnZXQ7XG4gIH1cblxuICBwcml2YXRlIGRvQWpheCh1cmw6IHN0cmluZywgZGF0YTogYW55LCBvcHRpb25zOiBPUFRJT05TID0ge30sIGFjdGlvbjogRnVuY3Rpb24pOiBQcm9taXNlPEFqYXhSZXNwb25zZT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zdCBpc0xvYWRpbmcgPSAhb3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShcImxvYWRpbmdcIikgfHwgb3B0aW9ucy5sb2FkaW5nO1xuICAgICAgY29uc3Qgd3JhcENsYXNzTmFtZSA9ICgob3B0aW9ucyB8fCB7fSkgYXMgYW55KS53cmFwQ2xhc3NOYW1lIHx8IFwiXCI7XG4gICAgICBjb25zdCBvcGFjaXR5TnVtID0gKChvcHRpb25zIHx8IHt9KSBhcyBhbnkpLm9wYWNpdHlOdW0gfHwgMDtcbiAgICAgIHRyeSB7XG4gICAgICAgIGlmIChpc0xvYWRpbmcpIHtcbiAgICAgICAgICBzcGluLnNwaW5TaG93KHdyYXBDbGFzc05hbWUsIG9wYWNpdHlOdW0pO1xuICAgICAgICB9XG4gICAgICAgIGFjdGlvblxuICAgICAgICAgIC5jYWxsKEFqYXgsIHVybCwgZGF0YSwgb3B0aW9ucylcbiAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgaWYgKGlzTG9hZGluZykge1xuICAgICAgICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmVzb2x2ZShyZXNwb25zZSk7XG4gICAgICAgICAgfSkuY2F0Y2goKGVycm9yOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAhKG9wdGlvbnMuaWdub3JlNHh4IHx8IG9wdGlvbnMuaWdub3JlRmFpbCkgJiYgdGhpcy5vbjR4eChlcnJvcik7XG4gICAgICAgICAgIShvcHRpb25zLmlnbm9yZTV4eCB8fCBvcHRpb25zLmlnbm9yZUZhaWwpICYmIHRoaXMub241eHgoZXJyb3IpO1xuICAgICAgICAgICEob3B0aW9ucy5pZ25vcmUyeHggfHwgb3B0aW9ucy5pZ25vcmVGYWlsKSAmJiB0aGlzLm9uMnh4KGVycm9yKTtcbiAgICAgICAgICBpZiAoaXNMb2FkaW5nKSB7XG4gICAgICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZWplY3QoZXJyb3IpO1xuICAgICAgICB9KTtcbiAgICAgIH0gZmluYWxseSB7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwdXQodXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIG9wdGlvbnM/OiBPUFRJT05TKTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICByZXR1cm4gdGhpcy5kb0FqYXgodXJsLCBkYXRhLCBvcHRpb25zLCBBamF4LnB1dCk7XG4gIH1cblxuICBwb3N0KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBvcHRpb25zPzogT1BUSU9OUyk6IFByb21pc2U8QWpheFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIHRoaXMuZG9BamF4KHVybCwgZGF0YSwgb3B0aW9ucywgQWpheC5wb3N0KTtcbiAgfVxuXG4gIHBhdGNoKHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBvcHRpb25zPzogT1BUSU9OUyk6IFByb21pc2U8QWpheFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIHRoaXMuZG9BamF4KHVybCwgZGF0YSwgb3B0aW9ucywgQWpheC5wYXRjaCk7XG4gIH1cblxuICBnZXQodXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIG9wdGlvbnM/OiBPUFRJT05TKTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICByZXR1cm4gdGhpcy5kb0FqYXgodXJsLCBkYXRhLCBvcHRpb25zLCBBamF4LmdldCk7XG4gIH1cblxuICBkZWxldGUodXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIG9wdGlvbnM/OiBPUFRJT05TKTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICByZXR1cm4gdGhpcy5kb0FqYXgodXJsLCBkYXRhLCBvcHRpb25zLCBBamF4LmRlbGV0ZSk7XG4gIH1cblxuICAvKipcbiAgICogYXJndW1lbnRzIGFyZSBpbnN0YW5jZXMgb2YgRmFpbEZhc3RQcm9taXNlXG4gICAqL1xuICBhbGwocHJvbWlzZXM6IFByb21pc2U8YW55PltdKTogUHJvbWlzZTxhbnlbXT4ge1xuICAgIHJldHVybiBBamF4LmFsbChwcm9taXNlcyk7XG4gIH1cblxuICBhamF4KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBvcHRpb25zPzogT1BUSU9OUyk6IFByb21pc2U8QWpheFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIHRoaXMuZG9BamF4KHVybCwgZGF0YSwgb3B0aW9ucywgQWpheC5hamF4KTtcbiAgfVxuXG4gIHByaXZhdGUgb240MDEoZXJyb3I6IEFqYXhSZXNwb25zZSk6IHZvaWQge1xuICAgIHRoaXMuZ2V0V2lkZ2V0KCkuZ2V0SGVscGVycygpLmdldFJvdXRlcigpLnJlcGxhY2VSZWRpcmVjdChQQVRILlNJR05fSU4pO1xuICB9XG5cbiAgcHJpdmF0ZSBvbjQwMyhlcnJvcjogQWpheFJlc3BvbnNlKTogdm9pZCB7XG4gICAgLy8gZGVhbCB3aXRoIDQwMywgbm8gYWNjZXNzIHBlcm1pdHRlZCBleGNlcHRpb25cbiAgICB0aGlzLmdldFdpZGdldCgpLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5yZXBsYWNlUmVkaXJlY3QoUEFUSC5FUlJPUl80MDMpO1xuICB9XG5cbiAgcHJpdmF0ZSBvbjR4eCA9IChlcnJvcjogQWpheFJlc3BvbnNlKTogdm9pZCA9PiB7XG4gICAgaWYgKGVycm9yLnN0YXR1cyA+PSA0MDAgJiYgZXJyb3Iuc3RhdHVzIDwgNTAwKSB7XG4gICAgICBzd2l0Y2ggKGVycm9yLnN0YXR1cykge1xuICAgICAgICBjYXNlIDQwMTpcbiAgICAgICAgICB0aGlzLm9uNDAxKGVycm9yKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSA0MDM6XG4gICAgICAgICAgdGhpcy5vbjQwMyhlcnJvcik7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgLy8gZG8gbm90aGluZ1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICBwcml2YXRlIG9uNXh4ID0gKGVycm9yOiBBamF4UmVzcG9uc2UpOiB2b2lkID0+IHtcbiAgICBpZiAoZXJyb3Iuc3RhdHVzID49IDUwMCkge1xuICAgICAgc3dpdGNoIChlcnJvci5zdGF0dXMpIHtcbiAgICAgICAgY2FzZSA1MDA6XG4gICAgICAgICAgdGhpcy5nZXRXaWRnZXQoKS5nZXRIZWxwZXJzKCkuZ2V0Um91dGVyKCkucmVwbGFjZVJlZGlyZWN0KFBBVEguRVJST1JfNTAwKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgcHJpdmF0ZSBvbjJ4eCA9IChlcnJvcjogQWpheFJlc3BvbnNlKTogdm9pZCA9PiB7XG4gICAgc3dpdGNoIChlcnJvci5zdGF0dXMpIHtcbiAgICAgIGNhc2UgMjAwOlxuICAgICAgICBjb25zdCB7IHJlc3BDb2RlLCByZXNwTWVzc2FnZSB9ID0gZXJyb3IuYm9keSB8fCB7fSBhcyBhbnk7XG4gICAgICAgIC8vIG5vdGlmaWNhdGlvbi5lcnJvcih7XG4gICAgICAgIC8vICAgbWVzc2FnZTogcmVzcENvZGUsXG4gICAgICAgIC8vICAgZGVzY3JpcHRpb246IHJlc3BNZXNzYWdlXG4gICAgICAgIC8vIH0pO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgfVxuICB9O1xufVxuXG4vKipcbiAqIGhlbHBlcnNcbiAqL1xuY2xhc3MgSGVscGVycyBpbXBsZW1lbnRzIFdpZGdldEhlbHBlcnMge1xuICBwcml2YXRlIHdpZGdldDogV2lkZ2V0PGFueSwgYW55LCBhbnk+O1xuICBwcml2YXRlIHJvdXRlcj86IElXaWRnZXRSb3V0ZXJIZWxwZXI7XG4gIHByaXZhdGUgYWpheD86IElXaWRnZXRBamF4SGVscGVyO1xuXG4gIGNvbnN0cnVjdG9yKHdpZGdldDogV2lkZ2V0PGFueSwgYW55LCBhbnk+KSB7XG4gICAgdGhpcy53aWRnZXQgPSB3aWRnZXQ7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybiB7V2lkZ2V0fVxuICAgKi9cbiAgZ2V0V2lkZ2V0KCk6IFdpZGdldDxhbnksIGFueSwgYW55PiB7XG4gICAgcmV0dXJuIHRoaXMud2lkZ2V0O1xuICB9XG5cbiAgZ2V0Um91dGVyKCk6IElXaWRnZXRSb3V0ZXJIZWxwZXIge1xuICAgIHJldHVybiB0aGlzLnJvdXRlciA/IHRoaXMucm91dGVyIDogKHRoaXMucm91dGVyID0gbmV3IFdpZGdldFJvdXRlckhlbHBlcih0aGlzLmdldFdpZGdldCgpKSk7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybiB7V2lkZ2V0QWpheEhlbHBlcn1cbiAgICovXG4gIGdldEFqYXgoKTogSVdpZGdldEFqYXhIZWxwZXIge1xuICAgIHJldHVybiB0aGlzLmFqYXggPyB0aGlzLmFqYXggOiAodGhpcy5hamF4ID0gbmV3IFdpZGdldEFqYXhIZWxwZXIodGhpcy5nZXRXaWRnZXQoKSkpO1xuICB9XG59XG5cbi8qKlxuICogYWxsIGNvbXBvbmVudCBzaG91bGQgZXh0ZW5kcyBmcm9tIHRoaXMuXG4gKiBzdGF0ZSBoYXMgYmVlbiBpbml0aWFsaXplZCBhcyB7fVxuICovXG5hYnN0cmFjdCBjbGFzcyBXaWRnZXQ8UHJvcGVydGllcyBleHRlbmRzIFdpZGdldFByb3BzLCBTdGF0ZSwgSW5uZXJDb21wb25lbnRzPiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxQcm9wZXJ0aWVzLCBTdGF0ZT5cbiAgaW1wbGVtZW50cyBXaWRnZXRIZWxwZWQge1xuICBwcm90ZWN0ZWQgQzogSW5uZXJDb21wb25lbnRzO1xuICBwcml2YXRlIGhlbHBlcnM6IFdpZGdldEhlbHBlcnM7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgICByZWdpc3RlckludG9SdW50aW1lKHRoaXMpO1xuICAgIHRoaXMuc3RhdGUgPSB0aGlzLmluaXRTdGF0ZSgpO1xuICAgIHRoaXMuaGVscGVycyA9IG5ldyBIZWxwZXJzKHRoaXMpO1xuICAgIHRoaXMuQyA9IHRoaXMuaW5pdENvbXBvbmVudHMoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogU3RhdGUge1xuICAgIHJldHVybiB7fSBhcyBTdGF0ZTtcbiAgfVxuXG4gIHByb3RlY3RlZCBhYnN0cmFjdCBpbml0Q29tcG9uZW50cygpOiBJbm5lckNvbXBvbmVudHM7XG5cbiAgcHJvdGVjdGVkIGdldENvbXBvbmVudHMoKTogSW5uZXJDb21wb25lbnRzIHtcbiAgICByZXR1cm4gdGhpcy5DO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gIH1cblxuICBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzOiBhbnksIG5leHRTdGF0ZT86IGFueSwgbmV4dENvbnRleHQ/OiBhbnkpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHM6IGFueSwgcHJldlN0YXRlPzogYW55LCBwcmV2Q29udGV4dD86IGFueSkge1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgdW5yZWdpc3RlckZyb21SdW50aW1lKHRoaXMpO1xuICB9XG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge0hlbHBlcnN9XG4gICAqL1xuICBnZXRIZWxwZXJzKCk6IFdpZGdldEhlbHBlcnMge1xuICAgIHJldHVybiB0aGlzLmhlbHBlcnM7XG4gIH1cblxuICByZW5kZXJDU1ModGhlbWU6IFN0eWxlZFByb3BzVGhlbWUpOiBzdHJpbmcge1xuICAgIGlmICh0aGlzLnByb3BzLmNzcykge1xuICAgICAgcmV0dXJuIHRoaXMucHJvcHMuY3NzLmNhbGwodGhpcywgdGhpcy5wcm9wcywgdGhlbWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGNvbnN0IFdpZGdldENvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0KHt9KTtcbldpZGdldC5jb250ZXh0VHlwZSA9IFdpZGdldENvbnRleHQ7XG5cbmV4cG9ydCBkZWZhdWx0IFdpZGdldDtcbmV4cG9ydCB7IFdpZGdldFJvdXRlckhlbHBlciB9O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBV0E7QUFFQTs7OztBQUdBOzs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7Ozs7O0FBR0E7Ozs7O0FBR0E7OztBQUdBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUErRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUE3RkE7QUErRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBeEdBO0FBMEdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBUkE7QUFVQTtBQUNBO0FBcEhBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUEyQ0E7Ozs7O0FBR0E7OztBQUtBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQUdBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7QUFHQTs7Ozs7O0FBSUE7Ozs7O0FBS0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFNQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUlBO0FBQ0E7QUFDQTs7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTs7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBbERBO0FBQ0E7QUFvREE7QUFDQTtBQUVBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/component/widget.tsx
