__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "svgBaseProps", function() { return svgBaseProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getThemeFromTypeName", function() { return getThemeFromTypeName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeTypeTheme", function() { return removeTypeTheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withThemeSuffix", function() { return withThemeSuffix; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alias", function() { return alias; });
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
 // These props make sure that the SVG behaviours like general text.
// Reference: https://blog.prototypr.io/align-svg-icons-to-text-and-say-goodbye-to-font-icons-d44b3d7b26b4

var svgBaseProps = {
  width: '1em',
  height: '1em',
  fill: 'currentColor',
  'aria-hidden': true,
  focusable: 'false'
};
var fillTester = /-fill$/;
var outlineTester = /-o$/;
var twoToneTester = /-twotone$/;
function getThemeFromTypeName(type) {
  var result = null;

  if (fillTester.test(type)) {
    result = 'filled';
  } else if (outlineTester.test(type)) {
    result = 'outlined';
  } else if (twoToneTester.test(type)) {
    result = 'twoTone';
  }

  return result;
}
function removeTypeTheme(type) {
  return type.replace(fillTester, '').replace(outlineTester, '').replace(twoToneTester, '');
}
function withThemeSuffix(type, theme) {
  var result = type;

  if (theme === 'filled') {
    result += '-fill';
  } else if (theme === 'outlined') {
    result += '-o';
  } else if (theme === 'twoTone') {
    result += '-twotone';
  } else {
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_0__["default"])(false, 'Icon', "This icon '".concat(type, "' has unknown theme '").concat(theme, "'"));
  }

  return result;
} // For alias or compatibility

function alias(type) {
  var newType = type;

  switch (type) {
    case 'cross':
      newType = 'close';
      break;
    // https://github.com/ant-design/ant-design/issues/13007

    case 'interation':
      newType = 'interaction';
      break;
    // https://github.com/ant-design/ant-design/issues/16810

    case 'canlendar':
      newType = 'calendar';
      break;
    // https://github.com/ant-design/ant-design/issues/17448

    case 'colum-height':
      newType = 'column-height';
      break;

    default:
  }

  Object(_util_warning__WEBPACK_IMPORTED_MODULE_0__["default"])(newType === type, 'Icon', "Icon '".concat(type, "' was a typo and is now deprecated, please use '").concat(newType, "' instead."));
  return newType;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pY29uL3V0aWxzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9pY29uL3V0aWxzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuLy8gVGhlc2UgcHJvcHMgbWFrZSBzdXJlIHRoYXQgdGhlIFNWRyBiZWhhdmlvdXJzIGxpa2UgZ2VuZXJhbCB0ZXh0LlxuLy8gUmVmZXJlbmNlOiBodHRwczovL2Jsb2cucHJvdG90eXByLmlvL2FsaWduLXN2Zy1pY29ucy10by10ZXh0LWFuZC1zYXktZ29vZGJ5ZS10by1mb250LWljb25zLWQ0NGIzZDdiMjZiNFxuZXhwb3J0IGNvbnN0IHN2Z0Jhc2VQcm9wcyA9IHtcbiAgICB3aWR0aDogJzFlbScsXG4gICAgaGVpZ2h0OiAnMWVtJyxcbiAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAnYXJpYS1oaWRkZW4nOiB0cnVlLFxuICAgIGZvY3VzYWJsZTogJ2ZhbHNlJyxcbn07XG5jb25zdCBmaWxsVGVzdGVyID0gLy1maWxsJC87XG5jb25zdCBvdXRsaW5lVGVzdGVyID0gLy1vJC87XG5jb25zdCB0d29Ub25lVGVzdGVyID0gLy10d290b25lJC87XG5leHBvcnQgZnVuY3Rpb24gZ2V0VGhlbWVGcm9tVHlwZU5hbWUodHlwZSkge1xuICAgIGxldCByZXN1bHQgPSBudWxsO1xuICAgIGlmIChmaWxsVGVzdGVyLnRlc3QodHlwZSkpIHtcbiAgICAgICAgcmVzdWx0ID0gJ2ZpbGxlZCc7XG4gICAgfVxuICAgIGVsc2UgaWYgKG91dGxpbmVUZXN0ZXIudGVzdCh0eXBlKSkge1xuICAgICAgICByZXN1bHQgPSAnb3V0bGluZWQnO1xuICAgIH1cbiAgICBlbHNlIGlmICh0d29Ub25lVGVzdGVyLnRlc3QodHlwZSkpIHtcbiAgICAgICAgcmVzdWx0ID0gJ3R3b1RvbmUnO1xuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xufVxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZVR5cGVUaGVtZSh0eXBlKSB7XG4gICAgcmV0dXJuIHR5cGVcbiAgICAgICAgLnJlcGxhY2UoZmlsbFRlc3RlciwgJycpXG4gICAgICAgIC5yZXBsYWNlKG91dGxpbmVUZXN0ZXIsICcnKVxuICAgICAgICAucmVwbGFjZSh0d29Ub25lVGVzdGVyLCAnJyk7XG59XG5leHBvcnQgZnVuY3Rpb24gd2l0aFRoZW1lU3VmZml4KHR5cGUsIHRoZW1lKSB7XG4gICAgbGV0IHJlc3VsdCA9IHR5cGU7XG4gICAgaWYgKHRoZW1lID09PSAnZmlsbGVkJykge1xuICAgICAgICByZXN1bHQgKz0gJy1maWxsJztcbiAgICB9XG4gICAgZWxzZSBpZiAodGhlbWUgPT09ICdvdXRsaW5lZCcpIHtcbiAgICAgICAgcmVzdWx0ICs9ICctbyc7XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoZW1lID09PSAndHdvVG9uZScpIHtcbiAgICAgICAgcmVzdWx0ICs9ICctdHdvdG9uZSc7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICB3YXJuaW5nKGZhbHNlLCAnSWNvbicsIGBUaGlzIGljb24gJyR7dHlwZX0nIGhhcyB1bmtub3duIHRoZW1lICcke3RoZW1lfSdgKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cbi8vIEZvciBhbGlhcyBvciBjb21wYXRpYmlsaXR5XG5leHBvcnQgZnVuY3Rpb24gYWxpYXModHlwZSkge1xuICAgIGxldCBuZXdUeXBlID0gdHlwZTtcbiAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgY2FzZSAnY3Jvc3MnOlxuICAgICAgICAgICAgbmV3VHlwZSA9ICdjbG9zZSc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTMwMDdcbiAgICAgICAgY2FzZSAnaW50ZXJhdGlvbic6XG4gICAgICAgICAgICBuZXdUeXBlID0gJ2ludGVyYWN0aW9uJztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNjgxMFxuICAgICAgICBjYXNlICdjYW5sZW5kYXInOlxuICAgICAgICAgICAgbmV3VHlwZSA9ICdjYWxlbmRhcic7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTc0NDhcbiAgICAgICAgY2FzZSAnY29sdW0taGVpZ2h0JzpcbiAgICAgICAgICAgIG5ld1R5cGUgPSAnY29sdW1uLWhlaWdodCc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICB9XG4gICAgd2FybmluZyhuZXdUeXBlID09PSB0eXBlLCAnSWNvbicsIGBJY29uICcke3R5cGV9JyB3YXMgYSB0eXBvIGFuZCBpcyBub3cgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSAnJHtuZXdUeXBlfScgaW5zdGVhZC5gKTtcbiAgICByZXR1cm4gbmV3VHlwZTtcbn1cbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFoQkE7QUFDQTtBQWlCQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/icon/utils.js
