/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var axisPointerModelHelper = __webpack_require__(/*! ../axisPointer/modelHelper */ "./node_modules/echarts/lib/component/axisPointer/modelHelper.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Base class of AxisView.
 */


var AxisView = echarts.extendComponentView({
  type: 'axis',

  /**
   * @private
   */
  _axisPointer: null,

  /**
   * @protected
   * @type {string}
   */
  axisPointerClass: null,

  /**
   * @override
   */
  render: function render(axisModel, ecModel, api, payload) {
    // FIXME
    // This process should proformed after coordinate systems updated
    // (axis scale updated), and should be performed each time update.
    // So put it here temporarily, although it is not appropriate to
    // put a model-writing procedure in `view`.
    this.axisPointerClass && axisPointerModelHelper.fixValue(axisModel);
    AxisView.superApply(this, 'render', arguments);

    _updateAxisPointer(this, axisModel, ecModel, api, payload, true);
  },

  /**
   * Action handler.
   * @public
   * @param {module:echarts/coord/cartesian/AxisModel} axisModel
   * @param {module:echarts/model/Global} ecModel
   * @param {module:echarts/ExtensionAPI} api
   * @param {Object} payload
   */
  updateAxisPointer: function updateAxisPointer(axisModel, ecModel, api, payload, force) {
    _updateAxisPointer(this, axisModel, ecModel, api, payload, false);
  },

  /**
   * @override
   */
  remove: function remove(ecModel, api) {
    var axisPointer = this._axisPointer;
    axisPointer && axisPointer.remove(api);
    AxisView.superApply(this, 'remove', arguments);
  },

  /**
   * @override
   */
  dispose: function dispose(ecModel, api) {
    disposeAxisPointer(this, api);
    AxisView.superApply(this, 'dispose', arguments);
  }
});

function _updateAxisPointer(axisView, axisModel, ecModel, api, payload, forceRender) {
  var Clazz = AxisView.getAxisPointerClass(axisView.axisPointerClass);

  if (!Clazz) {
    return;
  }

  var axisPointerModel = axisPointerModelHelper.getAxisPointerModel(axisModel);
  axisPointerModel ? (axisView._axisPointer || (axisView._axisPointer = new Clazz())).render(axisModel, axisPointerModel, api, forceRender) : disposeAxisPointer(axisView, api);
}

function disposeAxisPointer(axisView, ecModel, api) {
  var axisPointer = axisView._axisPointer;
  axisPointer && axisPointer.dispose(ecModel, api);
  axisView._axisPointer = null;
}

var axisPointerClazz = [];

AxisView.registerAxisPointerClass = function (type, clazz) {
  axisPointerClazz[type] = clazz;
};

AxisView.getAxisPointerClass = function (type) {
  return type && axisPointerClazz[type];
};

var _default = AxisView;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXMvQXhpc1ZpZXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvYXhpcy9BeGlzVmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vLi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxudmFyIGVjaGFydHMgPSByZXF1aXJlKFwiLi4vLi4vZWNoYXJ0c1wiKTtcblxudmFyIGF4aXNQb2ludGVyTW9kZWxIZWxwZXIgPSByZXF1aXJlKFwiLi4vYXhpc1BvaW50ZXIvbW9kZWxIZWxwZXJcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBCYXNlIGNsYXNzIG9mIEF4aXNWaWV3LlxuICovXG52YXIgQXhpc1ZpZXcgPSBlY2hhcnRzLmV4dGVuZENvbXBvbmVudFZpZXcoe1xuICB0eXBlOiAnYXhpcycsXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfYXhpc1BvaW50ZXI6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwcm90ZWN0ZWRcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIGF4aXNQb2ludGVyQ2xhc3M6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgcmVuZGVyOiBmdW5jdGlvbiAoYXhpc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpIHtcbiAgICAvLyBGSVhNRVxuICAgIC8vIFRoaXMgcHJvY2VzcyBzaG91bGQgcHJvZm9ybWVkIGFmdGVyIGNvb3JkaW5hdGUgc3lzdGVtcyB1cGRhdGVkXG4gICAgLy8gKGF4aXMgc2NhbGUgdXBkYXRlZCksIGFuZCBzaG91bGQgYmUgcGVyZm9ybWVkIGVhY2ggdGltZSB1cGRhdGUuXG4gICAgLy8gU28gcHV0IGl0IGhlcmUgdGVtcG9yYXJpbHksIGFsdGhvdWdoIGl0IGlzIG5vdCBhcHByb3ByaWF0ZSB0b1xuICAgIC8vIHB1dCBhIG1vZGVsLXdyaXRpbmcgcHJvY2VkdXJlIGluIGB2aWV3YC5cbiAgICB0aGlzLmF4aXNQb2ludGVyQ2xhc3MgJiYgYXhpc1BvaW50ZXJNb2RlbEhlbHBlci5maXhWYWx1ZShheGlzTW9kZWwpO1xuICAgIEF4aXNWaWV3LnN1cGVyQXBwbHkodGhpcywgJ3JlbmRlcicsIGFyZ3VtZW50cyk7XG4gICAgdXBkYXRlQXhpc1BvaW50ZXIodGhpcywgYXhpc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQsIHRydWUpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBY3Rpb24gaGFuZGxlci5cbiAgICogQHB1YmxpY1xuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9BeGlzTW9kZWx9IGF4aXNNb2RlbFxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gICAqL1xuICB1cGRhdGVBeGlzUG9pbnRlcjogZnVuY3Rpb24gKGF4aXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkLCBmb3JjZSkge1xuICAgIHVwZGF0ZUF4aXNQb2ludGVyKHRoaXMsIGF4aXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkLCBmYWxzZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgcmVtb3ZlOiBmdW5jdGlvbiAoZWNNb2RlbCwgYXBpKSB7XG4gICAgdmFyIGF4aXNQb2ludGVyID0gdGhpcy5fYXhpc1BvaW50ZXI7XG4gICAgYXhpc1BvaW50ZXIgJiYgYXhpc1BvaW50ZXIucmVtb3ZlKGFwaSk7XG4gICAgQXhpc1ZpZXcuc3VwZXJBcHBseSh0aGlzLCAncmVtb3ZlJywgYXJndW1lbnRzKTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICBkaXNwb3NlOiBmdW5jdGlvbiAoZWNNb2RlbCwgYXBpKSB7XG4gICAgZGlzcG9zZUF4aXNQb2ludGVyKHRoaXMsIGFwaSk7XG4gICAgQXhpc1ZpZXcuc3VwZXJBcHBseSh0aGlzLCAnZGlzcG9zZScsIGFyZ3VtZW50cyk7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiB1cGRhdGVBeGlzUG9pbnRlcihheGlzVmlldywgYXhpc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQsIGZvcmNlUmVuZGVyKSB7XG4gIHZhciBDbGF6eiA9IEF4aXNWaWV3LmdldEF4aXNQb2ludGVyQ2xhc3MoYXhpc1ZpZXcuYXhpc1BvaW50ZXJDbGFzcyk7XG5cbiAgaWYgKCFDbGF6eikge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBheGlzUG9pbnRlck1vZGVsID0gYXhpc1BvaW50ZXJNb2RlbEhlbHBlci5nZXRBeGlzUG9pbnRlck1vZGVsKGF4aXNNb2RlbCk7XG4gIGF4aXNQb2ludGVyTW9kZWwgPyAoYXhpc1ZpZXcuX2F4aXNQb2ludGVyIHx8IChheGlzVmlldy5fYXhpc1BvaW50ZXIgPSBuZXcgQ2xhenooKSkpLnJlbmRlcihheGlzTW9kZWwsIGF4aXNQb2ludGVyTW9kZWwsIGFwaSwgZm9yY2VSZW5kZXIpIDogZGlzcG9zZUF4aXNQb2ludGVyKGF4aXNWaWV3LCBhcGkpO1xufVxuXG5mdW5jdGlvbiBkaXNwb3NlQXhpc1BvaW50ZXIoYXhpc1ZpZXcsIGVjTW9kZWwsIGFwaSkge1xuICB2YXIgYXhpc1BvaW50ZXIgPSBheGlzVmlldy5fYXhpc1BvaW50ZXI7XG4gIGF4aXNQb2ludGVyICYmIGF4aXNQb2ludGVyLmRpc3Bvc2UoZWNNb2RlbCwgYXBpKTtcbiAgYXhpc1ZpZXcuX2F4aXNQb2ludGVyID0gbnVsbDtcbn1cblxudmFyIGF4aXNQb2ludGVyQ2xhenogPSBbXTtcblxuQXhpc1ZpZXcucmVnaXN0ZXJBeGlzUG9pbnRlckNsYXNzID0gZnVuY3Rpb24gKHR5cGUsIGNsYXp6KSB7XG4gIGF4aXNQb2ludGVyQ2xhenpbdHlwZV0gPSBjbGF6ejtcbn07XG5cbkF4aXNWaWV3LmdldEF4aXNQb2ludGVyQ2xhc3MgPSBmdW5jdGlvbiAodHlwZSkge1xuICByZXR1cm4gdHlwZSAmJiBheGlzUG9pbnRlckNsYXp6W3R5cGVdO1xufTtcblxudmFyIF9kZWZhdWx0ID0gQXhpc1ZpZXc7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQXZEQTtBQUNBO0FBeURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axis/AxisView.js
