

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InnerSlider = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var _reactDom = _interopRequireDefault(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

var _initialState = _interopRequireDefault(__webpack_require__(/*! ./initial-state */ "./node_modules/react-slick/lib/initial-state.js"));

var _lodash = _interopRequireDefault(__webpack_require__(/*! lodash.debounce */ "./node_modules/lodash.debounce/index.js"));

var _classnames = _interopRequireDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var _innerSliderUtils = __webpack_require__(/*! ./utils/innerSliderUtils */ "./node_modules/react-slick/lib/utils/innerSliderUtils.js");

var _track = __webpack_require__(/*! ./track */ "./node_modules/react-slick/lib/track.js");

var _dots = __webpack_require__(/*! ./dots */ "./node_modules/react-slick/lib/dots.js");

var _arrows = __webpack_require__(/*! ./arrows */ "./node_modules/react-slick/lib/arrows.js");

var _resizeObserverPolyfill = _interopRequireDefault(__webpack_require__(/*! resize-observer-polyfill */ "./node_modules/resize-observer-polyfill/dist/ResizeObserver.es.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var InnerSlider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InnerSlider, _React$Component);

  function InnerSlider(props) {
    var _this;

    _classCallCheck(this, InnerSlider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(InnerSlider).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "listRefHandler", function (ref) {
      return _this.list = ref;
    });

    _defineProperty(_assertThisInitialized(_this), "trackRefHandler", function (ref) {
      return _this.track = ref;
    });

    _defineProperty(_assertThisInitialized(_this), "adaptHeight", function () {
      if (_this.props.adaptiveHeight && _this.list) {
        var elem = _this.list.querySelector("[data-index=\"".concat(_this.state.currentSlide, "\"]"));

        _this.list.style.height = (0, _innerSliderUtils.getHeight)(elem) + "px";
      }
    });

    _defineProperty(_assertThisInitialized(_this), "UNSAFE_componentWillMount", function () {
      _this.ssrInit();

      _this.props.onInit && _this.props.onInit();

      if (_this.props.lazyLoad) {
        var slidesToLoad = (0, _innerSliderUtils.getOnDemandLazySlides)(_objectSpread({}, _this.props, {}, _this.state));

        if (slidesToLoad.length > 0) {
          _this.setState(function (prevState) {
            return {
              lazyLoadedList: prevState.lazyLoadedList.concat(slidesToLoad)
            };
          });

          if (_this.props.onLazyLoad) {
            _this.props.onLazyLoad(slidesToLoad);
          }
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "componentDidMount", function () {
      var spec = _objectSpread({
        listRef: _this.list,
        trackRef: _this.track
      }, _this.props);

      _this.updateState(spec, true, function () {
        _this.adaptHeight();

        _this.props.autoplay && _this.autoPlay("update");
      });

      if (_this.props.lazyLoad === "progressive") {
        _this.lazyLoadTimer = setInterval(_this.progressiveLazyLoad, 1000);
      }

      _this.ro = new _resizeObserverPolyfill["default"](function () {
        if (_this.state.animating) {
          _this.onWindowResized(false); // don't set trackStyle hence don't break animation


          _this.callbackTimers.push(setTimeout(function () {
            return _this.onWindowResized();
          }, _this.props.speed));
        } else {
          _this.onWindowResized();
        }
      });

      _this.ro.observe(_this.list);

      Array.prototype.forEach.call(document.querySelectorAll(".slick-slide"), function (slide) {
        slide.onfocus = _this.props.pauseOnFocus ? _this.onSlideFocus : null;
        slide.onblur = _this.props.pauseOnFocus ? _this.onSlideBlur : null;
      }); // To support server-side rendering

      if (!window) {
        return;
      }

      if (window.addEventListener) {
        window.addEventListener("resize", _this.onWindowResized);
      } else {
        window.attachEvent("onresize", _this.onWindowResized);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "componentWillUnmount", function () {
      if (_this.animationEndCallback) {
        clearTimeout(_this.animationEndCallback);
      }

      if (_this.lazyLoadTimer) {
        clearInterval(_this.lazyLoadTimer);
      }

      if (_this.callbackTimers.length) {
        _this.callbackTimers.forEach(function (timer) {
          return clearTimeout(timer);
        });

        _this.callbackTimers = [];
      }

      if (window.addEventListener) {
        window.removeEventListener("resize", _this.onWindowResized);
      } else {
        window.detachEvent("onresize", _this.onWindowResized);
      }

      if (_this.autoplayTimer) {
        clearInterval(_this.autoplayTimer);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "UNSAFE_componentWillReceiveProps", function (nextProps) {
      var spec = _objectSpread({
        listRef: _this.list,
        trackRef: _this.track
      }, nextProps, {}, _this.state);

      var setTrackStyle = false;

      for (var _i = 0, _Object$keys = Object.keys(_this.props); _i < _Object$keys.length; _i++) {
        var key = _Object$keys[_i];

        if (!nextProps.hasOwnProperty(key)) {
          setTrackStyle = true;
          break;
        }

        if (_typeof(nextProps[key]) === "object" || typeof nextProps[key] === "function") {
          continue;
        }

        if (nextProps[key] !== _this.props[key]) {
          setTrackStyle = true;
          break;
        }
      }

      _this.updateState(spec, setTrackStyle, function () {
        if (_this.state.currentSlide >= _react["default"].Children.count(nextProps.children)) {
          _this.changeSlide({
            message: "index",
            index: _react["default"].Children.count(nextProps.children) - nextProps.slidesToShow,
            currentSlide: _this.state.currentSlide
          });
        }

        if (nextProps.autoplay) {
          _this.autoPlay("update");
        } else {
          _this.pause("paused");
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "componentDidUpdate", function () {
      _this.checkImagesLoad();

      _this.props.onReInit && _this.props.onReInit();

      if (_this.props.lazyLoad) {
        var slidesToLoad = (0, _innerSliderUtils.getOnDemandLazySlides)(_objectSpread({}, _this.props, {}, _this.state));

        if (slidesToLoad.length > 0) {
          _this.setState(function (prevState) {
            return {
              lazyLoadedList: prevState.lazyLoadedList.concat(slidesToLoad)
            };
          });

          if (_this.props.onLazyLoad) {
            _this.props.onLazyLoad(slidesToLoad);
          }
        }
      } // if (this.props.onLazyLoad) {
      //   this.props.onLazyLoad([leftMostSlide])
      // }


      _this.adaptHeight();
    });

    _defineProperty(_assertThisInitialized(_this), "onWindowResized", function (setTrackStyle) {
      if (_this.debouncedResize) _this.debouncedResize.cancel();
      _this.debouncedResize = (0, _lodash["default"])(function () {
        return _this.resizeWindow(setTrackStyle);
      }, 50);

      _this.debouncedResize();
    });

    _defineProperty(_assertThisInitialized(_this), "resizeWindow", function () {
      var setTrackStyle = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      if (!_reactDom["default"].findDOMNode(_this.track)) return;

      var spec = _objectSpread({
        listRef: _this.list,
        trackRef: _this.track
      }, _this.props, {}, _this.state);

      _this.updateState(spec, setTrackStyle, function () {
        if (_this.props.autoplay) _this.autoPlay("update");else _this.pause("paused");
      }); // animating state should be cleared while resizing, otherwise autoplay stops working


      _this.setState({
        animating: false
      });

      clearTimeout(_this.animationEndCallback);
      delete _this.animationEndCallback;
    });

    _defineProperty(_assertThisInitialized(_this), "updateState", function (spec, setTrackStyle, callback) {
      var updatedState = (0, _innerSliderUtils.initializedState)(spec);
      spec = _objectSpread({}, spec, {}, updatedState, {
        slideIndex: updatedState.currentSlide
      });
      var targetLeft = (0, _innerSliderUtils.getTrackLeft)(spec);
      spec = _objectSpread({}, spec, {
        left: targetLeft
      });
      var trackStyle = (0, _innerSliderUtils.getTrackCSS)(spec);

      if (setTrackStyle || _react["default"].Children.count(_this.props.children) !== _react["default"].Children.count(spec.children)) {
        updatedState["trackStyle"] = trackStyle;
      }

      _this.setState(updatedState, callback);
    });

    _defineProperty(_assertThisInitialized(_this), "ssrInit", function () {
      if (_this.props.variableWidth) {
        var _trackWidth = 0,
            _trackLeft = 0;
        var childrenWidths = [];
        var preClones = (0, _innerSliderUtils.getPreClones)(_objectSpread({}, _this.props, {}, _this.state, {
          slideCount: _this.props.children.length
        }));
        var postClones = (0, _innerSliderUtils.getPostClones)(_objectSpread({}, _this.props, {}, _this.state, {
          slideCount: _this.props.children.length
        }));

        _this.props.children.forEach(function (child) {
          childrenWidths.push(child.props.style.width);
          _trackWidth += child.props.style.width;
        });

        for (var i = 0; i < preClones; i++) {
          _trackLeft += childrenWidths[childrenWidths.length - 1 - i];
          _trackWidth += childrenWidths[childrenWidths.length - 1 - i];
        }

        for (var _i2 = 0; _i2 < postClones; _i2++) {
          _trackWidth += childrenWidths[_i2];
        }

        for (var _i3 = 0; _i3 < _this.state.currentSlide; _i3++) {
          _trackLeft += childrenWidths[_i3];
        }

        var _trackStyle = {
          width: _trackWidth + "px",
          left: -_trackLeft + "px"
        };

        if (_this.props.centerMode) {
          var currentWidth = "".concat(childrenWidths[_this.state.currentSlide], "px");
          _trackStyle.left = "calc(".concat(_trackStyle.left, " + (100% - ").concat(currentWidth, ") / 2 ) ");
        }

        _this.setState({
          trackStyle: _trackStyle
        });

        return;
      }

      var childrenCount = _react["default"].Children.count(_this.props.children);

      var spec = _objectSpread({}, _this.props, {}, _this.state, {
        slideCount: childrenCount
      });

      var slideCount = (0, _innerSliderUtils.getPreClones)(spec) + (0, _innerSliderUtils.getPostClones)(spec) + childrenCount;
      var trackWidth = 100 / _this.props.slidesToShow * slideCount;
      var slideWidth = 100 / slideCount;
      var trackLeft = -slideWidth * ((0, _innerSliderUtils.getPreClones)(spec) + _this.state.currentSlide) * trackWidth / 100;

      if (_this.props.centerMode) {
        trackLeft += (100 - slideWidth * trackWidth / 100) / 2;
      }

      var trackStyle = {
        width: trackWidth + "%",
        left: trackLeft + "%"
      };

      _this.setState({
        slideWidth: slideWidth + "%",
        trackStyle: trackStyle
      });
    });

    _defineProperty(_assertThisInitialized(_this), "checkImagesLoad", function () {
      var images = document.querySelectorAll(".slick-slide img");
      var imagesCount = images.length,
          loadedCount = 0;
      Array.prototype.forEach.call(images, function (image) {
        var handler = function handler() {
          return ++loadedCount && loadedCount >= imagesCount && _this.onWindowResized();
        };

        if (!image.onclick) {
          image.onclick = function () {
            return image.parentNode.focus();
          };
        } else {
          var prevClickHandler = image.onclick;

          image.onclick = function () {
            prevClickHandler();
            image.parentNode.focus();
          };
        }

        if (!image.onload) {
          if (_this.props.lazyLoad) {
            image.onload = function () {
              _this.adaptHeight();

              _this.callbackTimers.push(setTimeout(_this.onWindowResized, _this.props.speed));
            };
          } else {
            image.onload = handler;

            image.onerror = function () {
              handler();
              _this.props.onLazyLoadError && _this.props.onLazyLoadError();
            };
          }
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "progressiveLazyLoad", function () {
      var slidesToLoad = [];

      var spec = _objectSpread({}, _this.props, {}, _this.state);

      for (var index = _this.state.currentSlide; index < _this.state.slideCount + (0, _innerSliderUtils.getPostClones)(spec); index++) {
        if (_this.state.lazyLoadedList.indexOf(index) < 0) {
          slidesToLoad.push(index);
          break;
        }
      }

      for (var _index = _this.state.currentSlide - 1; _index >= -(0, _innerSliderUtils.getPreClones)(spec); _index--) {
        if (_this.state.lazyLoadedList.indexOf(_index) < 0) {
          slidesToLoad.push(_index);
          break;
        }
      }

      if (slidesToLoad.length > 0) {
        _this.setState(function (state) {
          return {
            lazyLoadedList: state.lazyLoadedList.concat(slidesToLoad)
          };
        });

        if (_this.props.onLazyLoad) {
          _this.props.onLazyLoad(slidesToLoad);
        }
      } else {
        if (_this.lazyLoadTimer) {
          clearInterval(_this.lazyLoadTimer);
          delete _this.lazyLoadTimer;
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "slideHandler", function (index) {
      var dontAnimate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var _this$props = _this.props,
          asNavFor = _this$props.asNavFor,
          beforeChange = _this$props.beforeChange,
          onLazyLoad = _this$props.onLazyLoad,
          speed = _this$props.speed,
          afterChange = _this$props.afterChange; // capture currentslide before state is updated

      var currentSlide = _this.state.currentSlide;

      var _slideHandler = (0, _innerSliderUtils.slideHandler)(_objectSpread({
        index: index
      }, _this.props, {}, _this.state, {
        trackRef: _this.track,
        useCSS: _this.props.useCSS && !dontAnimate
      })),
          state = _slideHandler.state,
          nextState = _slideHandler.nextState;

      if (!state) return;
      beforeChange && beforeChange(currentSlide, state.currentSlide);
      var slidesToLoad = state.lazyLoadedList.filter(function (value) {
        return _this.state.lazyLoadedList.indexOf(value) < 0;
      });
      onLazyLoad && slidesToLoad.length > 0 && onLazyLoad(slidesToLoad);

      _this.setState(state, function () {
        asNavFor && asNavFor.innerSlider.slideHandler(index);
        if (!nextState) return;
        _this.animationEndCallback = setTimeout(function () {
          var animating = nextState.animating,
              firstBatch = _objectWithoutProperties(nextState, ["animating"]);

          _this.setState(firstBatch, function () {
            _this.callbackTimers.push(setTimeout(function () {
              return _this.setState({
                animating: animating
              });
            }, 10));

            afterChange && afterChange(state.currentSlide);
            delete _this.animationEndCallback;
          });
        }, speed);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "changeSlide", function (options) {
      var dontAnimate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var spec = _objectSpread({}, _this.props, {}, _this.state);

      var targetSlide = (0, _innerSliderUtils.changeSlide)(spec, options);
      if (targetSlide !== 0 && !targetSlide) return;

      if (dontAnimate === true) {
        _this.slideHandler(targetSlide, dontAnimate);
      } else {
        _this.slideHandler(targetSlide);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "clickHandler", function (e) {
      if (_this.clickable === false) {
        e.stopPropagation();
        e.preventDefault();
      }

      _this.clickable = true;
    });

    _defineProperty(_assertThisInitialized(_this), "keyHandler", function (e) {
      var dir = (0, _innerSliderUtils.keyHandler)(e, _this.props.accessibility, _this.props.rtl);
      dir !== "" && _this.changeSlide({
        message: dir
      });
    });

    _defineProperty(_assertThisInitialized(_this), "selectHandler", function (options) {
      _this.changeSlide(options);
    });

    _defineProperty(_assertThisInitialized(_this), "disableBodyScroll", function () {
      var preventDefault = function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault) e.preventDefault();
        e.returnValue = false;
      };

      window.ontouchmove = preventDefault;
    });

    _defineProperty(_assertThisInitialized(_this), "enableBodyScroll", function () {
      window.ontouchmove = null;
    });

    _defineProperty(_assertThisInitialized(_this), "swipeStart", function (e) {
      if (_this.props.verticalSwiping) {
        _this.disableBodyScroll();
      }

      var state = (0, _innerSliderUtils.swipeStart)(e, _this.props.swipe, _this.props.draggable);
      state !== "" && _this.setState(state);
    });

    _defineProperty(_assertThisInitialized(_this), "swipeMove", function (e) {
      var state = (0, _innerSliderUtils.swipeMove)(e, _objectSpread({}, _this.props, {}, _this.state, {
        trackRef: _this.track,
        listRef: _this.list,
        slideIndex: _this.state.currentSlide
      }));
      if (!state) return;

      if (state["swiping"]) {
        _this.clickable = false;
      }

      _this.setState(state);
    });

    _defineProperty(_assertThisInitialized(_this), "swipeEnd", function (e) {
      var state = (0, _innerSliderUtils.swipeEnd)(e, _objectSpread({}, _this.props, {}, _this.state, {
        trackRef: _this.track,
        listRef: _this.list,
        slideIndex: _this.state.currentSlide
      }));
      if (!state) return;
      var triggerSlideHandler = state["triggerSlideHandler"];
      delete state["triggerSlideHandler"];

      _this.setState(state);

      if (triggerSlideHandler === undefined) return;

      _this.slideHandler(triggerSlideHandler);

      if (_this.props.verticalSwiping) {
        _this.enableBodyScroll();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "slickPrev", function () {
      // this and fellow methods are wrapped in setTimeout
      // to make sure initialize setState has happened before
      // any of such methods are called
      _this.callbackTimers.push(setTimeout(function () {
        return _this.changeSlide({
          message: "previous"
        });
      }, 0));
    });

    _defineProperty(_assertThisInitialized(_this), "slickNext", function () {
      _this.callbackTimers.push(setTimeout(function () {
        return _this.changeSlide({
          message: "next"
        });
      }, 0));
    });

    _defineProperty(_assertThisInitialized(_this), "slickGoTo", function (slide) {
      var dontAnimate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      slide = Number(slide);
      if (isNaN(slide)) return "";

      _this.callbackTimers.push(setTimeout(function () {
        return _this.changeSlide({
          message: "index",
          index: slide,
          currentSlide: _this.state.currentSlide
        }, dontAnimate);
      }, 0));
    });

    _defineProperty(_assertThisInitialized(_this), "play", function () {
      var nextIndex;

      if (_this.props.rtl) {
        nextIndex = _this.state.currentSlide - _this.props.slidesToScroll;
      } else {
        if ((0, _innerSliderUtils.canGoNext)(_objectSpread({}, _this.props, {}, _this.state))) {
          nextIndex = _this.state.currentSlide + _this.props.slidesToScroll;
        } else {
          return false;
        }
      }

      _this.slideHandler(nextIndex);
    });

    _defineProperty(_assertThisInitialized(_this), "autoPlay", function (playType) {
      if (_this.autoplayTimer) {
        clearInterval(_this.autoplayTimer);
      }

      var autoplaying = _this.state.autoplaying;

      if (playType === "update") {
        if (autoplaying === "hovered" || autoplaying === "focused" || autoplaying === "paused") {
          return;
        }
      } else if (playType === "leave") {
        if (autoplaying === "paused" || autoplaying === "focused") {
          return;
        }
      } else if (playType === "blur") {
        if (autoplaying === "paused" || autoplaying === "hovered") {
          return;
        }
      }

      _this.autoplayTimer = setInterval(_this.play, _this.props.autoplaySpeed + 50);

      _this.setState({
        autoplaying: "playing"
      });
    });

    _defineProperty(_assertThisInitialized(_this), "pause", function (pauseType) {
      if (_this.autoplayTimer) {
        clearInterval(_this.autoplayTimer);
        _this.autoplayTimer = null;
      }

      var autoplaying = _this.state.autoplaying;

      if (pauseType === "paused") {
        _this.setState({
          autoplaying: "paused"
        });
      } else if (pauseType === "focused") {
        if (autoplaying === "hovered" || autoplaying === "playing") {
          _this.setState({
            autoplaying: "focused"
          });
        }
      } else {
        // pauseType  is 'hovered'
        if (autoplaying === "playing") {
          _this.setState({
            autoplaying: "hovered"
          });
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onDotsOver", function () {
      return _this.props.autoplay && _this.pause("hovered");
    });

    _defineProperty(_assertThisInitialized(_this), "onDotsLeave", function () {
      return _this.props.autoplay && _this.state.autoplaying === "hovered" && _this.autoPlay("leave");
    });

    _defineProperty(_assertThisInitialized(_this), "onTrackOver", function () {
      return _this.props.autoplay && _this.pause("hovered");
    });

    _defineProperty(_assertThisInitialized(_this), "onTrackLeave", function () {
      return _this.props.autoplay && _this.state.autoplaying === "hovered" && _this.autoPlay("leave");
    });

    _defineProperty(_assertThisInitialized(_this), "onSlideFocus", function () {
      return _this.props.autoplay && _this.pause("focused");
    });

    _defineProperty(_assertThisInitialized(_this), "onSlideBlur", function () {
      return _this.props.autoplay && _this.state.autoplaying === "focused" && _this.autoPlay("blur");
    });

    _defineProperty(_assertThisInitialized(_this), "render", function () {
      var className = (0, _classnames["default"])("slick-slider", _this.props.className, {
        "slick-vertical": _this.props.vertical,
        "slick-initialized": true
      });

      var spec = _objectSpread({}, _this.props, {}, _this.state);

      var trackProps = (0, _innerSliderUtils.extractObject)(spec, ["fade", "cssEase", "speed", "infinite", "centerMode", "focusOnSelect", "currentSlide", "lazyLoad", "lazyLoadedList", "rtl", "slideWidth", "slideHeight", "listHeight", "vertical", "slidesToShow", "slidesToScroll", "slideCount", "trackStyle", "variableWidth", "unslick", "centerPadding"]);
      var pauseOnHover = _this.props.pauseOnHover;
      trackProps = _objectSpread({}, trackProps, {
        onMouseEnter: pauseOnHover ? _this.onTrackOver : null,
        onMouseLeave: pauseOnHover ? _this.onTrackLeave : null,
        onMouseOver: pauseOnHover ? _this.onTrackOver : null,
        focusOnSelect: _this.props.focusOnSelect ? _this.selectHandler : null
      });
      var dots;

      if (_this.props.dots === true && _this.state.slideCount >= _this.props.slidesToShow) {
        var dotProps = (0, _innerSliderUtils.extractObject)(spec, ["dotsClass", "slideCount", "slidesToShow", "currentSlide", "slidesToScroll", "clickHandler", "children", "customPaging", "infinite", "appendDots"]);
        var pauseOnDotsHover = _this.props.pauseOnDotsHover;
        dotProps = _objectSpread({}, dotProps, {
          clickHandler: _this.changeSlide,
          onMouseEnter: pauseOnDotsHover ? _this.onDotsLeave : null,
          onMouseOver: pauseOnDotsHover ? _this.onDotsOver : null,
          onMouseLeave: pauseOnDotsHover ? _this.onDotsLeave : null
        });
        dots = _react["default"].createElement(_dots.Dots, dotProps);
      }

      var prevArrow, nextArrow;
      var arrowProps = (0, _innerSliderUtils.extractObject)(spec, ["infinite", "centerMode", "currentSlide", "slideCount", "slidesToShow", "prevArrow", "nextArrow"]);
      arrowProps.clickHandler = _this.changeSlide;

      if (_this.props.arrows) {
        prevArrow = _react["default"].createElement(_arrows.PrevArrow, arrowProps);
        nextArrow = _react["default"].createElement(_arrows.NextArrow, arrowProps);
      }

      var verticalHeightStyle = null;

      if (_this.props.vertical) {
        verticalHeightStyle = {
          height: _this.state.listHeight
        };
      }

      var centerPaddingStyle = null;

      if (_this.props.vertical === false) {
        if (_this.props.centerMode === true) {
          centerPaddingStyle = {
            padding: "0px " + _this.props.centerPadding
          };
        }
      } else {
        if (_this.props.centerMode === true) {
          centerPaddingStyle = {
            padding: _this.props.centerPadding + " 0px"
          };
        }
      }

      var listStyle = _objectSpread({}, verticalHeightStyle, {}, centerPaddingStyle);

      var touchMove = _this.props.touchMove;
      var listProps = {
        className: "slick-list",
        style: listStyle,
        onClick: _this.clickHandler,
        onMouseDown: touchMove ? _this.swipeStart : null,
        onMouseMove: _this.state.dragging && touchMove ? _this.swipeMove : null,
        onMouseUp: touchMove ? _this.swipeEnd : null,
        onMouseLeave: _this.state.dragging && touchMove ? _this.swipeEnd : null,
        onTouchStart: touchMove ? _this.swipeStart : null,
        onTouchMove: _this.state.dragging && touchMove ? _this.swipeMove : null,
        onTouchEnd: touchMove ? _this.swipeEnd : null,
        onTouchCancel: _this.state.dragging && touchMove ? _this.swipeEnd : null,
        onKeyDown: _this.props.accessibility ? _this.keyHandler : null
      };
      var innerSliderProps = {
        className: className,
        dir: "ltr",
        style: _this.props.style
      };

      if (_this.props.unslick) {
        listProps = {
          className: "slick-list"
        };
        innerSliderProps = {
          className: className
        };
      }

      return _react["default"].createElement("div", innerSliderProps, !_this.props.unslick ? prevArrow : "", _react["default"].createElement("div", _extends({
        ref: _this.listRefHandler
      }, listProps), _react["default"].createElement(_track.Track, _extends({
        ref: _this.trackRefHandler
      }, trackProps), _this.props.children)), !_this.props.unslick ? nextArrow : "", !_this.props.unslick ? dots : "");
    });

    _this.list = null;
    _this.track = null;
    _this.state = _objectSpread({}, _initialState["default"], {
      currentSlide: _this.props.initialSlide,
      slideCount: _react["default"].Children.count(_this.props.children)
    });
    _this.callbackTimers = [];
    _this.clickable = true;
    _this.debouncedResize = null;
    return _this;
  }

  return InnerSlider;
}(_react["default"].Component);

exports.InnerSlider = InnerSlider;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2xpY2svbGliL2lubmVyLXNsaWRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LXNsaWNrL2xpYi9pbm5lci1zbGlkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLklubmVyU2xpZGVyID0gdm9pZCAwO1xuXG52YXIgX3JlYWN0ID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgX3JlYWN0RG9tID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChyZXF1aXJlKFwicmVhY3QtZG9tXCIpKTtcblxudmFyIF9pbml0aWFsU3RhdGUgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KHJlcXVpcmUoXCIuL2luaXRpYWwtc3RhdGVcIikpO1xuXG52YXIgX2xvZGFzaCA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQocmVxdWlyZShcImxvZGFzaC5kZWJvdW5jZVwiKSk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQocmVxdWlyZShcImNsYXNzbmFtZXNcIikpO1xuXG52YXIgX2lubmVyU2xpZGVyVXRpbHMgPSByZXF1aXJlKFwiLi91dGlscy9pbm5lclNsaWRlclV0aWxzXCIpO1xuXG52YXIgX3RyYWNrID0gcmVxdWlyZShcIi4vdHJhY2tcIik7XG5cbnZhciBfZG90cyA9IHJlcXVpcmUoXCIuL2RvdHNcIik7XG5cbnZhciBfYXJyb3dzID0gcmVxdWlyZShcIi4vYXJyb3dzXCIpO1xuXG52YXIgX3Jlc2l6ZU9ic2VydmVyUG9seWZpbGwgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KHJlcXVpcmUoXCJyZXNpemUtb2JzZXJ2ZXItcG9seWZpbGxcIikpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBcImRlZmF1bHRcIjogb2JqIH07IH1cblxuZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKTsgdmFyIGtleSwgaTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHNvdXJjZVN5bWJvbEtleXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHNvdXJjZSk7IGZvciAoaSA9IDA7IGkgPCBzb3VyY2VTeW1ib2xLZXlzLmxlbmd0aDsgaSsrKSB7IGtleSA9IHNvdXJjZVN5bWJvbEtleXNbaV07IGlmIChleGNsdWRlZC5pbmRleE9mKGtleSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHNvdXJjZSwga2V5KSkgY29udGludWU7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllc0xvb3NlKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSB7fTsgdmFyIHNvdXJjZUtleXMgPSBPYmplY3Qua2V5cyhzb3VyY2UpOyB2YXIga2V5LCBpOyBmb3IgKGkgPSAwOyBpIDwgc291cmNlS2V5cy5sZW5ndGg7IGkrKykgeyBrZXkgPSBzb3VyY2VLZXlzW2ldOyBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlOyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfTsgfSBlbHNlIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9OyB9IHJldHVybiBfdHlwZW9mKG9iaik7IH1cblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKHNvdXJjZSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKHNvdXJjZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoY2FsbCAmJiAoX3R5cGVvZihjYWxsKSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSkgeyByZXR1cm4gY2FsbDsgfSByZXR1cm4gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKTsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb25cIik7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKTsgfVxuXG5mdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBfc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgby5fX3Byb3RvX18gPSBwOyByZXR1cm4gbzsgfTsgcmV0dXJuIF9zZXRQcm90b3R5cGVPZihvLCBwKTsgfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG52YXIgSW5uZXJTbGlkZXIgPVxuLyojX19QVVJFX18qL1xuZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKElubmVyU2xpZGVyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJbm5lclNsaWRlcihwcm9wcykge1xuICAgIHZhciBfdGhpcztcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBJbm5lclNsaWRlcik7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihJbm5lclNsaWRlcikuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImxpc3RSZWZIYW5kbGVyXCIsIGZ1bmN0aW9uIChyZWYpIHtcbiAgICAgIHJldHVybiBfdGhpcy5saXN0ID0gcmVmO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcInRyYWNrUmVmSGFuZGxlclwiLCBmdW5jdGlvbiAocmVmKSB7XG4gICAgICByZXR1cm4gX3RoaXMudHJhY2sgPSByZWY7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiYWRhcHRIZWlnaHRcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLmFkYXB0aXZlSGVpZ2h0ICYmIF90aGlzLmxpc3QpIHtcbiAgICAgICAgdmFyIGVsZW0gPSBfdGhpcy5saXN0LnF1ZXJ5U2VsZWN0b3IoXCJbZGF0YS1pbmRleD1cXFwiXCIuY29uY2F0KF90aGlzLnN0YXRlLmN1cnJlbnRTbGlkZSwgXCJcXFwiXVwiKSk7XG5cbiAgICAgICAgX3RoaXMubGlzdC5zdHlsZS5oZWlnaHQgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZ2V0SGVpZ2h0KShlbGVtKSArIFwicHhcIjtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJVTlNBRkVfY29tcG9uZW50V2lsbE1vdW50XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLnNzckluaXQoKTtcblxuICAgICAgX3RoaXMucHJvcHMub25Jbml0ICYmIF90aGlzLnByb3BzLm9uSW5pdCgpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMubGF6eUxvYWQpIHtcbiAgICAgICAgdmFyIHNsaWRlc1RvTG9hZCA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5nZXRPbkRlbWFuZExhenlTbGlkZXMpKF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUpKTtcblxuICAgICAgICBpZiAoc2xpZGVzVG9Mb2FkLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZShmdW5jdGlvbiAocHJldlN0YXRlKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBsYXp5TG9hZGVkTGlzdDogcHJldlN0YXRlLmxhenlMb2FkZWRMaXN0LmNvbmNhdChzbGlkZXNUb0xvYWQpXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgaWYgKF90aGlzLnByb3BzLm9uTGF6eUxvYWQpIHtcbiAgICAgICAgICAgIF90aGlzLnByb3BzLm9uTGF6eUxvYWQoc2xpZGVzVG9Mb2FkKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJjb21wb25lbnREaWRNb3VudFwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgc3BlYyA9IF9vYmplY3RTcHJlYWQoe1xuICAgICAgICBsaXN0UmVmOiBfdGhpcy5saXN0LFxuICAgICAgICB0cmFja1JlZjogX3RoaXMudHJhY2tcbiAgICAgIH0sIF90aGlzLnByb3BzKTtcblxuICAgICAgX3RoaXMudXBkYXRlU3RhdGUoc3BlYywgdHJ1ZSwgZnVuY3Rpb24gKCkge1xuICAgICAgICBfdGhpcy5hZGFwdEhlaWdodCgpO1xuXG4gICAgICAgIF90aGlzLnByb3BzLmF1dG9wbGF5ICYmIF90aGlzLmF1dG9QbGF5KFwidXBkYXRlXCIpO1xuICAgICAgfSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5sYXp5TG9hZCA9PT0gXCJwcm9ncmVzc2l2ZVwiKSB7XG4gICAgICAgIF90aGlzLmxhenlMb2FkVGltZXIgPSBzZXRJbnRlcnZhbChfdGhpcy5wcm9ncmVzc2l2ZUxhenlMb2FkLCAxMDAwKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMucm8gPSBuZXcgX3Jlc2l6ZU9ic2VydmVyUG9seWZpbGxbXCJkZWZhdWx0XCJdKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKF90aGlzLnN0YXRlLmFuaW1hdGluZykge1xuICAgICAgICAgIF90aGlzLm9uV2luZG93UmVzaXplZChmYWxzZSk7IC8vIGRvbid0IHNldCB0cmFja1N0eWxlIGhlbmNlIGRvbid0IGJyZWFrIGFuaW1hdGlvblxuXG5cbiAgICAgICAgICBfdGhpcy5jYWxsYmFja1RpbWVycy5wdXNoKHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLm9uV2luZG93UmVzaXplZCgpO1xuICAgICAgICAgIH0sIF90aGlzLnByb3BzLnNwZWVkKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3RoaXMub25XaW5kb3dSZXNpemVkKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICBfdGhpcy5yby5vYnNlcnZlKF90aGlzLmxpc3QpO1xuXG4gICAgICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIuc2xpY2stc2xpZGVcIiksIGZ1bmN0aW9uIChzbGlkZSkge1xuICAgICAgICBzbGlkZS5vbmZvY3VzID0gX3RoaXMucHJvcHMucGF1c2VPbkZvY3VzID8gX3RoaXMub25TbGlkZUZvY3VzIDogbnVsbDtcbiAgICAgICAgc2xpZGUub25ibHVyID0gX3RoaXMucHJvcHMucGF1c2VPbkZvY3VzID8gX3RoaXMub25TbGlkZUJsdXIgOiBudWxsO1xuICAgICAgfSk7IC8vIFRvIHN1cHBvcnQgc2VydmVyLXNpZGUgcmVuZGVyaW5nXG5cbiAgICAgIGlmICghd2luZG93KSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIF90aGlzLm9uV2luZG93UmVzaXplZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB3aW5kb3cuYXR0YWNoRXZlbnQoXCJvbnJlc2l6ZVwiLCBfdGhpcy5vbldpbmRvd1Jlc2l6ZWQpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImNvbXBvbmVudFdpbGxVbm1vdW50XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5hbmltYXRpb25FbmRDYWxsYmFjaykge1xuICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMuYW5pbWF0aW9uRW5kQ2FsbGJhY2spO1xuICAgICAgfVxuXG4gICAgICBpZiAoX3RoaXMubGF6eUxvYWRUaW1lcikge1xuICAgICAgICBjbGVhckludGVydmFsKF90aGlzLmxhenlMb2FkVGltZXIpO1xuICAgICAgfVxuXG4gICAgICBpZiAoX3RoaXMuY2FsbGJhY2tUaW1lcnMubGVuZ3RoKSB7XG4gICAgICAgIF90aGlzLmNhbGxiYWNrVGltZXJzLmZvckVhY2goZnVuY3Rpb24gKHRpbWVyKSB7XG4gICAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIF90aGlzLmNhbGxiYWNrVGltZXJzID0gW107XG4gICAgICB9XG5cbiAgICAgIGlmICh3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBfdGhpcy5vbldpbmRvd1Jlc2l6ZWQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgd2luZG93LmRldGFjaEV2ZW50KFwib25yZXNpemVcIiwgX3RoaXMub25XaW5kb3dSZXNpemVkKTtcbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLmF1dG9wbGF5VGltZXIpIHtcbiAgICAgICAgY2xlYXJJbnRlcnZhbChfdGhpcy5hdXRvcGxheVRpbWVyKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJVTlNBRkVfY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wc1wiLCBmdW5jdGlvbiAobmV4dFByb3BzKSB7XG4gICAgICB2YXIgc3BlYyA9IF9vYmplY3RTcHJlYWQoe1xuICAgICAgICBsaXN0UmVmOiBfdGhpcy5saXN0LFxuICAgICAgICB0cmFja1JlZjogX3RoaXMudHJhY2tcbiAgICAgIH0sIG5leHRQcm9wcywge30sIF90aGlzLnN0YXRlKTtcblxuICAgICAgdmFyIHNldFRyYWNrU3R5bGUgPSBmYWxzZTtcblxuICAgICAgZm9yICh2YXIgX2kgPSAwLCBfT2JqZWN0JGtleXMgPSBPYmplY3Qua2V5cyhfdGhpcy5wcm9wcyk7IF9pIDwgX09iamVjdCRrZXlzLmxlbmd0aDsgX2krKykge1xuICAgICAgICB2YXIga2V5ID0gX09iamVjdCRrZXlzW19pXTtcblxuICAgICAgICBpZiAoIW5leHRQcm9wcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgc2V0VHJhY2tTdHlsZSA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoX3R5cGVvZihuZXh0UHJvcHNba2V5XSkgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIG5leHRQcm9wc1trZXldID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChuZXh0UHJvcHNba2V5XSAhPT0gX3RoaXMucHJvcHNba2V5XSkge1xuICAgICAgICAgIHNldFRyYWNrU3R5bGUgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnVwZGF0ZVN0YXRlKHNwZWMsIHNldFRyYWNrU3R5bGUsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKF90aGlzLnN0YXRlLmN1cnJlbnRTbGlkZSA+PSBfcmVhY3RbXCJkZWZhdWx0XCJdLkNoaWxkcmVuLmNvdW50KG5leHRQcm9wcy5jaGlsZHJlbikpIHtcbiAgICAgICAgICBfdGhpcy5jaGFuZ2VTbGlkZSh7XG4gICAgICAgICAgICBtZXNzYWdlOiBcImluZGV4XCIsXG4gICAgICAgICAgICBpbmRleDogX3JlYWN0W1wiZGVmYXVsdFwiXS5DaGlsZHJlbi5jb3VudChuZXh0UHJvcHMuY2hpbGRyZW4pIC0gbmV4dFByb3BzLnNsaWRlc1RvU2hvdyxcbiAgICAgICAgICAgIGN1cnJlbnRTbGlkZTogX3RoaXMuc3RhdGUuY3VycmVudFNsaWRlXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobmV4dFByb3BzLmF1dG9wbGF5KSB7XG4gICAgICAgICAgX3RoaXMuYXV0b1BsYXkoXCJ1cGRhdGVcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3RoaXMucGF1c2UoXCJwYXVzZWRcIik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImNvbXBvbmVudERpZFVwZGF0ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5jaGVja0ltYWdlc0xvYWQoKTtcblxuICAgICAgX3RoaXMucHJvcHMub25SZUluaXQgJiYgX3RoaXMucHJvcHMub25SZUluaXQoKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLmxhenlMb2FkKSB7XG4gICAgICAgIHZhciBzbGlkZXNUb0xvYWQgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZ2V0T25EZW1hbmRMYXp5U2xpZGVzKShfb2JqZWN0U3ByZWFkKHt9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlKSk7XG5cbiAgICAgICAgaWYgKHNsaWRlc1RvTG9hZC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgX3RoaXMuc2V0U3RhdGUoZnVuY3Rpb24gKHByZXZTdGF0ZSkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgbGF6eUxvYWRlZExpc3Q6IHByZXZTdGF0ZS5sYXp5TG9hZGVkTGlzdC5jb25jYXQoc2xpZGVzVG9Mb2FkKVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkxhenlMb2FkKSB7XG4gICAgICAgICAgICBfdGhpcy5wcm9wcy5vbkxhenlMb2FkKHNsaWRlc1RvTG9hZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9IC8vIGlmICh0aGlzLnByb3BzLm9uTGF6eUxvYWQpIHtcbiAgICAgIC8vICAgdGhpcy5wcm9wcy5vbkxhenlMb2FkKFtsZWZ0TW9zdFNsaWRlXSlcbiAgICAgIC8vIH1cblxuXG4gICAgICBfdGhpcy5hZGFwdEhlaWdodCgpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uV2luZG93UmVzaXplZFwiLCBmdW5jdGlvbiAoc2V0VHJhY2tTdHlsZSkge1xuICAgICAgaWYgKF90aGlzLmRlYm91bmNlZFJlc2l6ZSkgX3RoaXMuZGVib3VuY2VkUmVzaXplLmNhbmNlbCgpO1xuICAgICAgX3RoaXMuZGVib3VuY2VkUmVzaXplID0gKDAsIF9sb2Rhc2hbXCJkZWZhdWx0XCJdKShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBfdGhpcy5yZXNpemVXaW5kb3coc2V0VHJhY2tTdHlsZSk7XG4gICAgICB9LCA1MCk7XG5cbiAgICAgIF90aGlzLmRlYm91bmNlZFJlc2l6ZSgpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcInJlc2l6ZVdpbmRvd1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgc2V0VHJhY2tTdHlsZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogdHJ1ZTtcbiAgICAgIGlmICghX3JlYWN0RG9tW1wiZGVmYXVsdFwiXS5maW5kRE9NTm9kZShfdGhpcy50cmFjaykpIHJldHVybjtcblxuICAgICAgdmFyIHNwZWMgPSBfb2JqZWN0U3ByZWFkKHtcbiAgICAgICAgbGlzdFJlZjogX3RoaXMubGlzdCxcbiAgICAgICAgdHJhY2tSZWY6IF90aGlzLnRyYWNrXG4gICAgICB9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlKTtcblxuICAgICAgX3RoaXMudXBkYXRlU3RhdGUoc3BlYywgc2V0VHJhY2tTdHlsZSwgZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoX3RoaXMucHJvcHMuYXV0b3BsYXkpIF90aGlzLmF1dG9QbGF5KFwidXBkYXRlXCIpO2Vsc2UgX3RoaXMucGF1c2UoXCJwYXVzZWRcIik7XG4gICAgICB9KTsgLy8gYW5pbWF0aW5nIHN0YXRlIHNob3VsZCBiZSBjbGVhcmVkIHdoaWxlIHJlc2l6aW5nLCBvdGhlcndpc2UgYXV0b3BsYXkgc3RvcHMgd29ya2luZ1xuXG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgYW5pbWF0aW5nOiBmYWxzZVxuICAgICAgfSk7XG5cbiAgICAgIGNsZWFyVGltZW91dChfdGhpcy5hbmltYXRpb25FbmRDYWxsYmFjayk7XG4gICAgICBkZWxldGUgX3RoaXMuYW5pbWF0aW9uRW5kQ2FsbGJhY2s7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwidXBkYXRlU3RhdGVcIiwgZnVuY3Rpb24gKHNwZWMsIHNldFRyYWNrU3R5bGUsIGNhbGxiYWNrKSB7XG4gICAgICB2YXIgdXBkYXRlZFN0YXRlID0gKDAsIF9pbm5lclNsaWRlclV0aWxzLmluaXRpYWxpemVkU3RhdGUpKHNwZWMpO1xuICAgICAgc3BlYyA9IF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHt9LCB1cGRhdGVkU3RhdGUsIHtcbiAgICAgICAgc2xpZGVJbmRleDogdXBkYXRlZFN0YXRlLmN1cnJlbnRTbGlkZVxuICAgICAgfSk7XG4gICAgICB2YXIgdGFyZ2V0TGVmdCA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5nZXRUcmFja0xlZnQpKHNwZWMpO1xuICAgICAgc3BlYyA9IF9vYmplY3RTcHJlYWQoe30sIHNwZWMsIHtcbiAgICAgICAgbGVmdDogdGFyZ2V0TGVmdFxuICAgICAgfSk7XG4gICAgICB2YXIgdHJhY2tTdHlsZSA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5nZXRUcmFja0NTUykoc3BlYyk7XG5cbiAgICAgIGlmIChzZXRUcmFja1N0eWxlIHx8IF9yZWFjdFtcImRlZmF1bHRcIl0uQ2hpbGRyZW4uY291bnQoX3RoaXMucHJvcHMuY2hpbGRyZW4pICE9PSBfcmVhY3RbXCJkZWZhdWx0XCJdLkNoaWxkcmVuLmNvdW50KHNwZWMuY2hpbGRyZW4pKSB7XG4gICAgICAgIHVwZGF0ZWRTdGF0ZVtcInRyYWNrU3R5bGVcIl0gPSB0cmFja1N0eWxlO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5zZXRTdGF0ZSh1cGRhdGVkU3RhdGUsIGNhbGxiYWNrKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzc3JJbml0XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy52YXJpYWJsZVdpZHRoKSB7XG4gICAgICAgIHZhciBfdHJhY2tXaWR0aCA9IDAsXG4gICAgICAgICAgICBfdHJhY2tMZWZ0ID0gMDtcbiAgICAgICAgdmFyIGNoaWxkcmVuV2lkdGhzID0gW107XG4gICAgICAgIHZhciBwcmVDbG9uZXMgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZ2V0UHJlQ2xvbmVzKShfb2JqZWN0U3ByZWFkKHt9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlLCB7XG4gICAgICAgICAgc2xpZGVDb3VudDogX3RoaXMucHJvcHMuY2hpbGRyZW4ubGVuZ3RoXG4gICAgICAgIH0pKTtcbiAgICAgICAgdmFyIHBvc3RDbG9uZXMgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZ2V0UG9zdENsb25lcykoX29iamVjdFNwcmVhZCh7fSwgX3RoaXMucHJvcHMsIHt9LCBfdGhpcy5zdGF0ZSwge1xuICAgICAgICAgIHNsaWRlQ291bnQ6IF90aGlzLnByb3BzLmNoaWxkcmVuLmxlbmd0aFxuICAgICAgICB9KSk7XG5cbiAgICAgICAgX3RoaXMucHJvcHMuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICBjaGlsZHJlbldpZHRocy5wdXNoKGNoaWxkLnByb3BzLnN0eWxlLndpZHRoKTtcbiAgICAgICAgICBfdHJhY2tXaWR0aCArPSBjaGlsZC5wcm9wcy5zdHlsZS53aWR0aDtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcmVDbG9uZXM7IGkrKykge1xuICAgICAgICAgIF90cmFja0xlZnQgKz0gY2hpbGRyZW5XaWR0aHNbY2hpbGRyZW5XaWR0aHMubGVuZ3RoIC0gMSAtIGldO1xuICAgICAgICAgIF90cmFja1dpZHRoICs9IGNoaWxkcmVuV2lkdGhzW2NoaWxkcmVuV2lkdGhzLmxlbmd0aCAtIDEgLSBpXTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIF9pMiA9IDA7IF9pMiA8IHBvc3RDbG9uZXM7IF9pMisrKSB7XG4gICAgICAgICAgX3RyYWNrV2lkdGggKz0gY2hpbGRyZW5XaWR0aHNbX2kyXTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIF9pMyA9IDA7IF9pMyA8IF90aGlzLnN0YXRlLmN1cnJlbnRTbGlkZTsgX2kzKyspIHtcbiAgICAgICAgICBfdHJhY2tMZWZ0ICs9IGNoaWxkcmVuV2lkdGhzW19pM107XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgX3RyYWNrU3R5bGUgPSB7XG4gICAgICAgICAgd2lkdGg6IF90cmFja1dpZHRoICsgXCJweFwiLFxuICAgICAgICAgIGxlZnQ6IC1fdHJhY2tMZWZ0ICsgXCJweFwiXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKF90aGlzLnByb3BzLmNlbnRlck1vZGUpIHtcbiAgICAgICAgICB2YXIgY3VycmVudFdpZHRoID0gXCJcIi5jb25jYXQoY2hpbGRyZW5XaWR0aHNbX3RoaXMuc3RhdGUuY3VycmVudFNsaWRlXSwgXCJweFwiKTtcbiAgICAgICAgICBfdHJhY2tTdHlsZS5sZWZ0ID0gXCJjYWxjKFwiLmNvbmNhdChfdHJhY2tTdHlsZS5sZWZ0LCBcIiArICgxMDAlIC0gXCIpLmNvbmNhdChjdXJyZW50V2lkdGgsIFwiKSAvIDIgKSBcIik7XG4gICAgICAgIH1cblxuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgdHJhY2tTdHlsZTogX3RyYWNrU3R5bGVcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgY2hpbGRyZW5Db3VudCA9IF9yZWFjdFtcImRlZmF1bHRcIl0uQ2hpbGRyZW4uY291bnQoX3RoaXMucHJvcHMuY2hpbGRyZW4pO1xuXG4gICAgICB2YXIgc3BlYyA9IF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUsIHtcbiAgICAgICAgc2xpZGVDb3VudDogY2hpbGRyZW5Db3VudFxuICAgICAgfSk7XG5cbiAgICAgIHZhciBzbGlkZUNvdW50ID0gKDAsIF9pbm5lclNsaWRlclV0aWxzLmdldFByZUNsb25lcykoc3BlYykgKyAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZ2V0UG9zdENsb25lcykoc3BlYykgKyBjaGlsZHJlbkNvdW50O1xuICAgICAgdmFyIHRyYWNrV2lkdGggPSAxMDAgLyBfdGhpcy5wcm9wcy5zbGlkZXNUb1Nob3cgKiBzbGlkZUNvdW50O1xuICAgICAgdmFyIHNsaWRlV2lkdGggPSAxMDAgLyBzbGlkZUNvdW50O1xuICAgICAgdmFyIHRyYWNrTGVmdCA9IC1zbGlkZVdpZHRoICogKCgwLCBfaW5uZXJTbGlkZXJVdGlscy5nZXRQcmVDbG9uZXMpKHNwZWMpICsgX3RoaXMuc3RhdGUuY3VycmVudFNsaWRlKSAqIHRyYWNrV2lkdGggLyAxMDA7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5jZW50ZXJNb2RlKSB7XG4gICAgICAgIHRyYWNrTGVmdCArPSAoMTAwIC0gc2xpZGVXaWR0aCAqIHRyYWNrV2lkdGggLyAxMDApIC8gMjtcbiAgICAgIH1cblxuICAgICAgdmFyIHRyYWNrU3R5bGUgPSB7XG4gICAgICAgIHdpZHRoOiB0cmFja1dpZHRoICsgXCIlXCIsXG4gICAgICAgIGxlZnQ6IHRyYWNrTGVmdCArIFwiJVwiXG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNsaWRlV2lkdGg6IHNsaWRlV2lkdGggKyBcIiVcIixcbiAgICAgICAgdHJhY2tTdHlsZTogdHJhY2tTdHlsZVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiY2hlY2tJbWFnZXNMb2FkXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBpbWFnZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLnNsaWNrLXNsaWRlIGltZ1wiKTtcbiAgICAgIHZhciBpbWFnZXNDb3VudCA9IGltYWdlcy5sZW5ndGgsXG4gICAgICAgICAgbG9hZGVkQ291bnQgPSAwO1xuICAgICAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChpbWFnZXMsIGZ1bmN0aW9uIChpbWFnZSkge1xuICAgICAgICB2YXIgaGFuZGxlciA9IGZ1bmN0aW9uIGhhbmRsZXIoKSB7XG4gICAgICAgICAgcmV0dXJuICsrbG9hZGVkQ291bnQgJiYgbG9hZGVkQ291bnQgPj0gaW1hZ2VzQ291bnQgJiYgX3RoaXMub25XaW5kb3dSZXNpemVkKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKCFpbWFnZS5vbmNsaWNrKSB7XG4gICAgICAgICAgaW1hZ2Uub25jbGljayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBpbWFnZS5wYXJlbnROb2RlLmZvY3VzKCk7XG4gICAgICAgICAgfTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YXIgcHJldkNsaWNrSGFuZGxlciA9IGltYWdlLm9uY2xpY2s7XG5cbiAgICAgICAgICBpbWFnZS5vbmNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcHJldkNsaWNrSGFuZGxlcigpO1xuICAgICAgICAgICAgaW1hZ2UucGFyZW50Tm9kZS5mb2N1cygpO1xuICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWltYWdlLm9ubG9hZCkge1xuICAgICAgICAgIGlmIChfdGhpcy5wcm9wcy5sYXp5TG9hZCkge1xuICAgICAgICAgICAgaW1hZ2Uub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBfdGhpcy5hZGFwdEhlaWdodCgpO1xuXG4gICAgICAgICAgICAgIF90aGlzLmNhbGxiYWNrVGltZXJzLnB1c2goc2V0VGltZW91dChfdGhpcy5vbldpbmRvd1Jlc2l6ZWQsIF90aGlzLnByb3BzLnNwZWVkKSk7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpbWFnZS5vbmxvYWQgPSBoYW5kbGVyO1xuXG4gICAgICAgICAgICBpbWFnZS5vbmVycm9yID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBoYW5kbGVyKCk7XG4gICAgICAgICAgICAgIF90aGlzLnByb3BzLm9uTGF6eUxvYWRFcnJvciAmJiBfdGhpcy5wcm9wcy5vbkxhenlMb2FkRXJyb3IoKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJwcm9ncmVzc2l2ZUxhenlMb2FkXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBzbGlkZXNUb0xvYWQgPSBbXTtcblxuICAgICAgdmFyIHNwZWMgPSBfb2JqZWN0U3ByZWFkKHt9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlKTtcblxuICAgICAgZm9yICh2YXIgaW5kZXggPSBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGU7IGluZGV4IDwgX3RoaXMuc3RhdGUuc2xpZGVDb3VudCArICgwLCBfaW5uZXJTbGlkZXJVdGlscy5nZXRQb3N0Q2xvbmVzKShzcGVjKTsgaW5kZXgrKykge1xuICAgICAgICBpZiAoX3RoaXMuc3RhdGUubGF6eUxvYWRlZExpc3QuaW5kZXhPZihpbmRleCkgPCAwKSB7XG4gICAgICAgICAgc2xpZGVzVG9Mb2FkLnB1c2goaW5kZXgpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIF9pbmRleCA9IF90aGlzLnN0YXRlLmN1cnJlbnRTbGlkZSAtIDE7IF9pbmRleCA+PSAtKDAsIF9pbm5lclNsaWRlclV0aWxzLmdldFByZUNsb25lcykoc3BlYyk7IF9pbmRleC0tKSB7XG4gICAgICAgIGlmIChfdGhpcy5zdGF0ZS5sYXp5TG9hZGVkTGlzdC5pbmRleE9mKF9pbmRleCkgPCAwKSB7XG4gICAgICAgICAgc2xpZGVzVG9Mb2FkLnB1c2goX2luZGV4KTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoc2xpZGVzVG9Mb2FkLmxlbmd0aCA+IDApIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoZnVuY3Rpb24gKHN0YXRlKSB7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxhenlMb2FkZWRMaXN0OiBzdGF0ZS5sYXp5TG9hZGVkTGlzdC5jb25jYXQoc2xpZGVzVG9Mb2FkKVxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkxhenlMb2FkKSB7XG4gICAgICAgICAgX3RoaXMucHJvcHMub25MYXp5TG9hZChzbGlkZXNUb0xvYWQpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoX3RoaXMubGF6eUxvYWRUaW1lcikge1xuICAgICAgICAgIGNsZWFySW50ZXJ2YWwoX3RoaXMubGF6eUxvYWRUaW1lcik7XG4gICAgICAgICAgZGVsZXRlIF90aGlzLmxhenlMb2FkVGltZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzbGlkZUhhbmRsZXJcIiwgZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICB2YXIgZG9udEFuaW1hdGUgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGZhbHNlO1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgYXNOYXZGb3IgPSBfdGhpcyRwcm9wcy5hc05hdkZvcixcbiAgICAgICAgICBiZWZvcmVDaGFuZ2UgPSBfdGhpcyRwcm9wcy5iZWZvcmVDaGFuZ2UsXG4gICAgICAgICAgb25MYXp5TG9hZCA9IF90aGlzJHByb3BzLm9uTGF6eUxvYWQsXG4gICAgICAgICAgc3BlZWQgPSBfdGhpcyRwcm9wcy5zcGVlZCxcbiAgICAgICAgICBhZnRlckNoYW5nZSA9IF90aGlzJHByb3BzLmFmdGVyQ2hhbmdlOyAvLyBjYXB0dXJlIGN1cnJlbnRzbGlkZSBiZWZvcmUgc3RhdGUgaXMgdXBkYXRlZFxuXG4gICAgICB2YXIgY3VycmVudFNsaWRlID0gX3RoaXMuc3RhdGUuY3VycmVudFNsaWRlO1xuXG4gICAgICB2YXIgX3NsaWRlSGFuZGxlciA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5zbGlkZUhhbmRsZXIpKF9vYmplY3RTcHJlYWQoe1xuICAgICAgICBpbmRleDogaW5kZXhcbiAgICAgIH0sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUsIHtcbiAgICAgICAgdHJhY2tSZWY6IF90aGlzLnRyYWNrLFxuICAgICAgICB1c2VDU1M6IF90aGlzLnByb3BzLnVzZUNTUyAmJiAhZG9udEFuaW1hdGVcbiAgICAgIH0pKSxcbiAgICAgICAgICBzdGF0ZSA9IF9zbGlkZUhhbmRsZXIuc3RhdGUsXG4gICAgICAgICAgbmV4dFN0YXRlID0gX3NsaWRlSGFuZGxlci5uZXh0U3RhdGU7XG5cbiAgICAgIGlmICghc3RhdGUpIHJldHVybjtcbiAgICAgIGJlZm9yZUNoYW5nZSAmJiBiZWZvcmVDaGFuZ2UoY3VycmVudFNsaWRlLCBzdGF0ZS5jdXJyZW50U2xpZGUpO1xuICAgICAgdmFyIHNsaWRlc1RvTG9hZCA9IHN0YXRlLmxhenlMb2FkZWRMaXN0LmZpbHRlcihmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzLnN0YXRlLmxhenlMb2FkZWRMaXN0LmluZGV4T2YodmFsdWUpIDwgMDtcbiAgICAgIH0pO1xuICAgICAgb25MYXp5TG9hZCAmJiBzbGlkZXNUb0xvYWQubGVuZ3RoID4gMCAmJiBvbkxhenlMb2FkKHNsaWRlc1RvTG9hZCk7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHN0YXRlLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFzTmF2Rm9yICYmIGFzTmF2Rm9yLmlubmVyU2xpZGVyLnNsaWRlSGFuZGxlcihpbmRleCk7XG4gICAgICAgIGlmICghbmV4dFN0YXRlKSByZXR1cm47XG4gICAgICAgIF90aGlzLmFuaW1hdGlvbkVuZENhbGxiYWNrID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIGFuaW1hdGluZyA9IG5leHRTdGF0ZS5hbmltYXRpbmcsXG4gICAgICAgICAgICAgIGZpcnN0QmF0Y2ggPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMobmV4dFN0YXRlLCBbXCJhbmltYXRpbmdcIl0pO1xuXG4gICAgICAgICAgX3RoaXMuc2V0U3RhdGUoZmlyc3RCYXRjaCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgX3RoaXMuY2FsbGJhY2tUaW1lcnMucHVzaChzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBhbmltYXRpbmc6IGFuaW1hdGluZ1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sIDEwKSk7XG5cbiAgICAgICAgICAgIGFmdGVyQ2hhbmdlICYmIGFmdGVyQ2hhbmdlKHN0YXRlLmN1cnJlbnRTbGlkZSk7XG4gICAgICAgICAgICBkZWxldGUgX3RoaXMuYW5pbWF0aW9uRW5kQ2FsbGJhY2s7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0sIHNwZWVkKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImNoYW5nZVNsaWRlXCIsIGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICB2YXIgZG9udEFuaW1hdGUgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGZhbHNlO1xuXG4gICAgICB2YXIgc3BlYyA9IF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUpO1xuXG4gICAgICB2YXIgdGFyZ2V0U2xpZGUgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuY2hhbmdlU2xpZGUpKHNwZWMsIG9wdGlvbnMpO1xuICAgICAgaWYgKHRhcmdldFNsaWRlICE9PSAwICYmICF0YXJnZXRTbGlkZSkgcmV0dXJuO1xuXG4gICAgICBpZiAoZG9udEFuaW1hdGUgPT09IHRydWUpIHtcbiAgICAgICAgX3RoaXMuc2xpZGVIYW5kbGVyKHRhcmdldFNsaWRlLCBkb250QW5pbWF0ZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfdGhpcy5zbGlkZUhhbmRsZXIodGFyZ2V0U2xpZGUpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImNsaWNrSGFuZGxlclwiLCBmdW5jdGlvbiAoZSkge1xuICAgICAgaWYgKF90aGlzLmNsaWNrYWJsZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5jbGlja2FibGUgPSB0cnVlO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImtleUhhbmRsZXJcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBkaXIgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMua2V5SGFuZGxlcikoZSwgX3RoaXMucHJvcHMuYWNjZXNzaWJpbGl0eSwgX3RoaXMucHJvcHMucnRsKTtcbiAgICAgIGRpciAhPT0gXCJcIiAmJiBfdGhpcy5jaGFuZ2VTbGlkZSh7XG4gICAgICAgIG1lc3NhZ2U6IGRpclxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwic2VsZWN0SGFuZGxlclwiLCBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgX3RoaXMuY2hhbmdlU2xpZGUob3B0aW9ucyk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiZGlzYWJsZUJvZHlTY3JvbGxcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHByZXZlbnREZWZhdWx0ID0gZnVuY3Rpb24gcHJldmVudERlZmF1bHQoZSkge1xuICAgICAgICBlID0gZSB8fCB3aW5kb3cuZXZlbnQ7XG4gICAgICAgIGlmIChlLnByZXZlbnREZWZhdWx0KSBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGUucmV0dXJuVmFsdWUgPSBmYWxzZTtcbiAgICAgIH07XG5cbiAgICAgIHdpbmRvdy5vbnRvdWNobW92ZSA9IHByZXZlbnREZWZhdWx0O1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImVuYWJsZUJvZHlTY3JvbGxcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgd2luZG93Lm9udG91Y2htb3ZlID0gbnVsbDtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzd2lwZVN0YXJ0XCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMudmVydGljYWxTd2lwaW5nKSB7XG4gICAgICAgIF90aGlzLmRpc2FibGVCb2R5U2Nyb2xsKCk7XG4gICAgICB9XG5cbiAgICAgIHZhciBzdGF0ZSA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5zd2lwZVN0YXJ0KShlLCBfdGhpcy5wcm9wcy5zd2lwZSwgX3RoaXMucHJvcHMuZHJhZ2dhYmxlKTtcbiAgICAgIHN0YXRlICE9PSBcIlwiICYmIF90aGlzLnNldFN0YXRlKHN0YXRlKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJzd2lwZU1vdmVcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBzdGF0ZSA9ICgwLCBfaW5uZXJTbGlkZXJVdGlscy5zd2lwZU1vdmUpKGUsIF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUsIHtcbiAgICAgICAgdHJhY2tSZWY6IF90aGlzLnRyYWNrLFxuICAgICAgICBsaXN0UmVmOiBfdGhpcy5saXN0LFxuICAgICAgICBzbGlkZUluZGV4OiBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGVcbiAgICAgIH0pKTtcbiAgICAgIGlmICghc3RhdGUpIHJldHVybjtcblxuICAgICAgaWYgKHN0YXRlW1wic3dpcGluZ1wiXSkge1xuICAgICAgICBfdGhpcy5jbGlja2FibGUgPSBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuc2V0U3RhdGUoc3RhdGUpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcInN3aXBlRW5kXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgc3RhdGUgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuc3dpcGVFbmQpKGUsIF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7fSwgX3RoaXMuc3RhdGUsIHtcbiAgICAgICAgdHJhY2tSZWY6IF90aGlzLnRyYWNrLFxuICAgICAgICBsaXN0UmVmOiBfdGhpcy5saXN0LFxuICAgICAgICBzbGlkZUluZGV4OiBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGVcbiAgICAgIH0pKTtcbiAgICAgIGlmICghc3RhdGUpIHJldHVybjtcbiAgICAgIHZhciB0cmlnZ2VyU2xpZGVIYW5kbGVyID0gc3RhdGVbXCJ0cmlnZ2VyU2xpZGVIYW5kbGVyXCJdO1xuICAgICAgZGVsZXRlIHN0YXRlW1widHJpZ2dlclNsaWRlSGFuZGxlclwiXTtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoc3RhdGUpO1xuXG4gICAgICBpZiAodHJpZ2dlclNsaWRlSGFuZGxlciA9PT0gdW5kZWZpbmVkKSByZXR1cm47XG5cbiAgICAgIF90aGlzLnNsaWRlSGFuZGxlcih0cmlnZ2VyU2xpZGVIYW5kbGVyKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLnZlcnRpY2FsU3dpcGluZykge1xuICAgICAgICBfdGhpcy5lbmFibGVCb2R5U2Nyb2xsKCk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwic2xpY2tQcmV2XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIHRoaXMgYW5kIGZlbGxvdyBtZXRob2RzIGFyZSB3cmFwcGVkIGluIHNldFRpbWVvdXRcbiAgICAgIC8vIHRvIG1ha2Ugc3VyZSBpbml0aWFsaXplIHNldFN0YXRlIGhhcyBoYXBwZW5lZCBiZWZvcmVcbiAgICAgIC8vIGFueSBvZiBzdWNoIG1ldGhvZHMgYXJlIGNhbGxlZFxuICAgICAgX3RoaXMuY2FsbGJhY2tUaW1lcnMucHVzaChzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzLmNoYW5nZVNsaWRlKHtcbiAgICAgICAgICBtZXNzYWdlOiBcInByZXZpb3VzXCJcbiAgICAgICAgfSk7XG4gICAgICB9LCAwKSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwic2xpY2tOZXh0XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLmNhbGxiYWNrVGltZXJzLnB1c2goc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBfdGhpcy5jaGFuZ2VTbGlkZSh7XG4gICAgICAgICAgbWVzc2FnZTogXCJuZXh0XCJcbiAgICAgICAgfSk7XG4gICAgICB9LCAwKSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwic2xpY2tHb1RvXCIsIGZ1bmN0aW9uIChzbGlkZSkge1xuICAgICAgdmFyIGRvbnRBbmltYXRlID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBmYWxzZTtcbiAgICAgIHNsaWRlID0gTnVtYmVyKHNsaWRlKTtcbiAgICAgIGlmIChpc05hTihzbGlkZSkpIHJldHVybiBcIlwiO1xuXG4gICAgICBfdGhpcy5jYWxsYmFja1RpbWVycy5wdXNoKHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gX3RoaXMuY2hhbmdlU2xpZGUoe1xuICAgICAgICAgIG1lc3NhZ2U6IFwiaW5kZXhcIixcbiAgICAgICAgICBpbmRleDogc2xpZGUsXG4gICAgICAgICAgY3VycmVudFNsaWRlOiBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGVcbiAgICAgICAgfSwgZG9udEFuaW1hdGUpO1xuICAgICAgfSwgMCkpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcInBsYXlcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIG5leHRJbmRleDtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLnJ0bCkge1xuICAgICAgICBuZXh0SW5kZXggPSBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGUgLSBfdGhpcy5wcm9wcy5zbGlkZXNUb1Njcm9sbDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICgoMCwgX2lubmVyU2xpZGVyVXRpbHMuY2FuR29OZXh0KShfb2JqZWN0U3ByZWFkKHt9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlKSkpIHtcbiAgICAgICAgICBuZXh0SW5kZXggPSBfdGhpcy5zdGF0ZS5jdXJyZW50U2xpZGUgKyBfdGhpcy5wcm9wcy5zbGlkZXNUb1Njcm9sbDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgX3RoaXMuc2xpZGVIYW5kbGVyKG5leHRJbmRleCk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiYXV0b1BsYXlcIiwgZnVuY3Rpb24gKHBsYXlUeXBlKSB7XG4gICAgICBpZiAoX3RoaXMuYXV0b3BsYXlUaW1lcikge1xuICAgICAgICBjbGVhckludGVydmFsKF90aGlzLmF1dG9wbGF5VGltZXIpO1xuICAgICAgfVxuXG4gICAgICB2YXIgYXV0b3BsYXlpbmcgPSBfdGhpcy5zdGF0ZS5hdXRvcGxheWluZztcblxuICAgICAgaWYgKHBsYXlUeXBlID09PSBcInVwZGF0ZVwiKSB7XG4gICAgICAgIGlmIChhdXRvcGxheWluZyA9PT0gXCJob3ZlcmVkXCIgfHwgYXV0b3BsYXlpbmcgPT09IFwiZm9jdXNlZFwiIHx8IGF1dG9wbGF5aW5nID09PSBcInBhdXNlZFwiKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHBsYXlUeXBlID09PSBcImxlYXZlXCIpIHtcbiAgICAgICAgaWYgKGF1dG9wbGF5aW5nID09PSBcInBhdXNlZFwiIHx8IGF1dG9wbGF5aW5nID09PSBcImZvY3VzZWRcIikge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChwbGF5VHlwZSA9PT0gXCJibHVyXCIpIHtcbiAgICAgICAgaWYgKGF1dG9wbGF5aW5nID09PSBcInBhdXNlZFwiIHx8IGF1dG9wbGF5aW5nID09PSBcImhvdmVyZWRcIikge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBfdGhpcy5hdXRvcGxheVRpbWVyID0gc2V0SW50ZXJ2YWwoX3RoaXMucGxheSwgX3RoaXMucHJvcHMuYXV0b3BsYXlTcGVlZCArIDUwKTtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhdXRvcGxheWluZzogXCJwbGF5aW5nXCJcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcInBhdXNlXCIsIGZ1bmN0aW9uIChwYXVzZVR5cGUpIHtcbiAgICAgIGlmIChfdGhpcy5hdXRvcGxheVRpbWVyKSB7XG4gICAgICAgIGNsZWFySW50ZXJ2YWwoX3RoaXMuYXV0b3BsYXlUaW1lcik7XG4gICAgICAgIF90aGlzLmF1dG9wbGF5VGltZXIgPSBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgYXV0b3BsYXlpbmcgPSBfdGhpcy5zdGF0ZS5hdXRvcGxheWluZztcblxuICAgICAgaWYgKHBhdXNlVHlwZSA9PT0gXCJwYXVzZWRcIikge1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgYXV0b3BsYXlpbmc6IFwicGF1c2VkXCJcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKHBhdXNlVHlwZSA9PT0gXCJmb2N1c2VkXCIpIHtcbiAgICAgICAgaWYgKGF1dG9wbGF5aW5nID09PSBcImhvdmVyZWRcIiB8fCBhdXRvcGxheWluZyA9PT0gXCJwbGF5aW5nXCIpIHtcbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBhdXRvcGxheWluZzogXCJmb2N1c2VkXCJcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gcGF1c2VUeXBlICBpcyAnaG92ZXJlZCdcbiAgICAgICAgaWYgKGF1dG9wbGF5aW5nID09PSBcInBsYXlpbmdcIikge1xuICAgICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGF1dG9wbGF5aW5nOiBcImhvdmVyZWRcIlxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25Eb3RzT3ZlclwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMucHJvcHMuYXV0b3BsYXkgJiYgX3RoaXMucGF1c2UoXCJob3ZlcmVkXCIpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uRG90c0xlYXZlXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBfdGhpcy5wcm9wcy5hdXRvcGxheSAmJiBfdGhpcy5zdGF0ZS5hdXRvcGxheWluZyA9PT0gXCJob3ZlcmVkXCIgJiYgX3RoaXMuYXV0b1BsYXkoXCJsZWF2ZVwiKTtcbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJvblRyYWNrT3ZlclwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMucHJvcHMuYXV0b3BsYXkgJiYgX3RoaXMucGF1c2UoXCJob3ZlcmVkXCIpO1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uVHJhY2tMZWF2ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMucHJvcHMuYXV0b3BsYXkgJiYgX3RoaXMuc3RhdGUuYXV0b3BsYXlpbmcgPT09IFwiaG92ZXJlZFwiICYmIF90aGlzLmF1dG9QbGF5KFwibGVhdmVcIik7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25TbGlkZUZvY3VzXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBfdGhpcy5wcm9wcy5hdXRvcGxheSAmJiBfdGhpcy5wYXVzZShcImZvY3VzZWRcIik7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwib25TbGlkZUJsdXJcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIF90aGlzLnByb3BzLmF1dG9wbGF5ICYmIF90aGlzLnN0YXRlLmF1dG9wbGF5aW5nID09PSBcImZvY3VzZWRcIiAmJiBfdGhpcy5hdXRvUGxheShcImJsdXJcIik7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwicmVuZGVyXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXNbXCJkZWZhdWx0XCJdKShcInNsaWNrLXNsaWRlclwiLCBfdGhpcy5wcm9wcy5jbGFzc05hbWUsIHtcbiAgICAgICAgXCJzbGljay12ZXJ0aWNhbFwiOiBfdGhpcy5wcm9wcy52ZXJ0aWNhbCxcbiAgICAgICAgXCJzbGljay1pbml0aWFsaXplZFwiOiB0cnVlXG4gICAgICB9KTtcblxuICAgICAgdmFyIHNwZWMgPSBfb2JqZWN0U3ByZWFkKHt9LCBfdGhpcy5wcm9wcywge30sIF90aGlzLnN0YXRlKTtcblxuICAgICAgdmFyIHRyYWNrUHJvcHMgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZXh0cmFjdE9iamVjdCkoc3BlYywgW1wiZmFkZVwiLCBcImNzc0Vhc2VcIiwgXCJzcGVlZFwiLCBcImluZmluaXRlXCIsIFwiY2VudGVyTW9kZVwiLCBcImZvY3VzT25TZWxlY3RcIiwgXCJjdXJyZW50U2xpZGVcIiwgXCJsYXp5TG9hZFwiLCBcImxhenlMb2FkZWRMaXN0XCIsIFwicnRsXCIsIFwic2xpZGVXaWR0aFwiLCBcInNsaWRlSGVpZ2h0XCIsIFwibGlzdEhlaWdodFwiLCBcInZlcnRpY2FsXCIsIFwic2xpZGVzVG9TaG93XCIsIFwic2xpZGVzVG9TY3JvbGxcIiwgXCJzbGlkZUNvdW50XCIsIFwidHJhY2tTdHlsZVwiLCBcInZhcmlhYmxlV2lkdGhcIiwgXCJ1bnNsaWNrXCIsIFwiY2VudGVyUGFkZGluZ1wiXSk7XG4gICAgICB2YXIgcGF1c2VPbkhvdmVyID0gX3RoaXMucHJvcHMucGF1c2VPbkhvdmVyO1xuICAgICAgdHJhY2tQcm9wcyA9IF9vYmplY3RTcHJlYWQoe30sIHRyYWNrUHJvcHMsIHtcbiAgICAgICAgb25Nb3VzZUVudGVyOiBwYXVzZU9uSG92ZXIgPyBfdGhpcy5vblRyYWNrT3ZlciA6IG51bGwsXG4gICAgICAgIG9uTW91c2VMZWF2ZTogcGF1c2VPbkhvdmVyID8gX3RoaXMub25UcmFja0xlYXZlIDogbnVsbCxcbiAgICAgICAgb25Nb3VzZU92ZXI6IHBhdXNlT25Ib3ZlciA/IF90aGlzLm9uVHJhY2tPdmVyIDogbnVsbCxcbiAgICAgICAgZm9jdXNPblNlbGVjdDogX3RoaXMucHJvcHMuZm9jdXNPblNlbGVjdCA/IF90aGlzLnNlbGVjdEhhbmRsZXIgOiBudWxsXG4gICAgICB9KTtcbiAgICAgIHZhciBkb3RzO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMuZG90cyA9PT0gdHJ1ZSAmJiBfdGhpcy5zdGF0ZS5zbGlkZUNvdW50ID49IF90aGlzLnByb3BzLnNsaWRlc1RvU2hvdykge1xuICAgICAgICB2YXIgZG90UHJvcHMgPSAoMCwgX2lubmVyU2xpZGVyVXRpbHMuZXh0cmFjdE9iamVjdCkoc3BlYywgW1wiZG90c0NsYXNzXCIsIFwic2xpZGVDb3VudFwiLCBcInNsaWRlc1RvU2hvd1wiLCBcImN1cnJlbnRTbGlkZVwiLCBcInNsaWRlc1RvU2Nyb2xsXCIsIFwiY2xpY2tIYW5kbGVyXCIsIFwiY2hpbGRyZW5cIiwgXCJjdXN0b21QYWdpbmdcIiwgXCJpbmZpbml0ZVwiLCBcImFwcGVuZERvdHNcIl0pO1xuICAgICAgICB2YXIgcGF1c2VPbkRvdHNIb3ZlciA9IF90aGlzLnByb3BzLnBhdXNlT25Eb3RzSG92ZXI7XG4gICAgICAgIGRvdFByb3BzID0gX29iamVjdFNwcmVhZCh7fSwgZG90UHJvcHMsIHtcbiAgICAgICAgICBjbGlja0hhbmRsZXI6IF90aGlzLmNoYW5nZVNsaWRlLFxuICAgICAgICAgIG9uTW91c2VFbnRlcjogcGF1c2VPbkRvdHNIb3ZlciA/IF90aGlzLm9uRG90c0xlYXZlIDogbnVsbCxcbiAgICAgICAgICBvbk1vdXNlT3ZlcjogcGF1c2VPbkRvdHNIb3ZlciA/IF90aGlzLm9uRG90c092ZXIgOiBudWxsLFxuICAgICAgICAgIG9uTW91c2VMZWF2ZTogcGF1c2VPbkRvdHNIb3ZlciA/IF90aGlzLm9uRG90c0xlYXZlIDogbnVsbFxuICAgICAgICB9KTtcbiAgICAgICAgZG90cyA9IF9yZWFjdFtcImRlZmF1bHRcIl0uY3JlYXRlRWxlbWVudChfZG90cy5Eb3RzLCBkb3RQcm9wcyk7XG4gICAgICB9XG5cbiAgICAgIHZhciBwcmV2QXJyb3csIG5leHRBcnJvdztcbiAgICAgIHZhciBhcnJvd1Byb3BzID0gKDAsIF9pbm5lclNsaWRlclV0aWxzLmV4dHJhY3RPYmplY3QpKHNwZWMsIFtcImluZmluaXRlXCIsIFwiY2VudGVyTW9kZVwiLCBcImN1cnJlbnRTbGlkZVwiLCBcInNsaWRlQ291bnRcIiwgXCJzbGlkZXNUb1Nob3dcIiwgXCJwcmV2QXJyb3dcIiwgXCJuZXh0QXJyb3dcIl0pO1xuICAgICAgYXJyb3dQcm9wcy5jbGlja0hhbmRsZXIgPSBfdGhpcy5jaGFuZ2VTbGlkZTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLmFycm93cykge1xuICAgICAgICBwcmV2QXJyb3cgPSBfcmVhY3RbXCJkZWZhdWx0XCJdLmNyZWF0ZUVsZW1lbnQoX2Fycm93cy5QcmV2QXJyb3csIGFycm93UHJvcHMpO1xuICAgICAgICBuZXh0QXJyb3cgPSBfcmVhY3RbXCJkZWZhdWx0XCJdLmNyZWF0ZUVsZW1lbnQoX2Fycm93cy5OZXh0QXJyb3csIGFycm93UHJvcHMpO1xuICAgICAgfVxuXG4gICAgICB2YXIgdmVydGljYWxIZWlnaHRTdHlsZSA9IG51bGw7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy52ZXJ0aWNhbCkge1xuICAgICAgICB2ZXJ0aWNhbEhlaWdodFN0eWxlID0ge1xuICAgICAgICAgIGhlaWdodDogX3RoaXMuc3RhdGUubGlzdEhlaWdodFxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICB2YXIgY2VudGVyUGFkZGluZ1N0eWxlID0gbnVsbDtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLnZlcnRpY2FsID09PSBmYWxzZSkge1xuICAgICAgICBpZiAoX3RoaXMucHJvcHMuY2VudGVyTW9kZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGNlbnRlclBhZGRpbmdTdHlsZSA9IHtcbiAgICAgICAgICAgIHBhZGRpbmc6IFwiMHB4IFwiICsgX3RoaXMucHJvcHMuY2VudGVyUGFkZGluZ1xuICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChfdGhpcy5wcm9wcy5jZW50ZXJNb2RlID09PSB0cnVlKSB7XG4gICAgICAgICAgY2VudGVyUGFkZGluZ1N0eWxlID0ge1xuICAgICAgICAgICAgcGFkZGluZzogX3RoaXMucHJvcHMuY2VudGVyUGFkZGluZyArIFwiIDBweFwiXG4gICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgbGlzdFN0eWxlID0gX29iamVjdFNwcmVhZCh7fSwgdmVydGljYWxIZWlnaHRTdHlsZSwge30sIGNlbnRlclBhZGRpbmdTdHlsZSk7XG5cbiAgICAgIHZhciB0b3VjaE1vdmUgPSBfdGhpcy5wcm9wcy50b3VjaE1vdmU7XG4gICAgICB2YXIgbGlzdFByb3BzID0ge1xuICAgICAgICBjbGFzc05hbWU6IFwic2xpY2stbGlzdFwiLFxuICAgICAgICBzdHlsZTogbGlzdFN0eWxlLFxuICAgICAgICBvbkNsaWNrOiBfdGhpcy5jbGlja0hhbmRsZXIsXG4gICAgICAgIG9uTW91c2VEb3duOiB0b3VjaE1vdmUgPyBfdGhpcy5zd2lwZVN0YXJ0IDogbnVsbCxcbiAgICAgICAgb25Nb3VzZU1vdmU6IF90aGlzLnN0YXRlLmRyYWdnaW5nICYmIHRvdWNoTW92ZSA/IF90aGlzLnN3aXBlTW92ZSA6IG51bGwsXG4gICAgICAgIG9uTW91c2VVcDogdG91Y2hNb3ZlID8gX3RoaXMuc3dpcGVFbmQgOiBudWxsLFxuICAgICAgICBvbk1vdXNlTGVhdmU6IF90aGlzLnN0YXRlLmRyYWdnaW5nICYmIHRvdWNoTW92ZSA/IF90aGlzLnN3aXBlRW5kIDogbnVsbCxcbiAgICAgICAgb25Ub3VjaFN0YXJ0OiB0b3VjaE1vdmUgPyBfdGhpcy5zd2lwZVN0YXJ0IDogbnVsbCxcbiAgICAgICAgb25Ub3VjaE1vdmU6IF90aGlzLnN0YXRlLmRyYWdnaW5nICYmIHRvdWNoTW92ZSA/IF90aGlzLnN3aXBlTW92ZSA6IG51bGwsXG4gICAgICAgIG9uVG91Y2hFbmQ6IHRvdWNoTW92ZSA/IF90aGlzLnN3aXBlRW5kIDogbnVsbCxcbiAgICAgICAgb25Ub3VjaENhbmNlbDogX3RoaXMuc3RhdGUuZHJhZ2dpbmcgJiYgdG91Y2hNb3ZlID8gX3RoaXMuc3dpcGVFbmQgOiBudWxsLFxuICAgICAgICBvbktleURvd246IF90aGlzLnByb3BzLmFjY2Vzc2liaWxpdHkgPyBfdGhpcy5rZXlIYW5kbGVyIDogbnVsbFxuICAgICAgfTtcbiAgICAgIHZhciBpbm5lclNsaWRlclByb3BzID0ge1xuICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgZGlyOiBcImx0clwiLFxuICAgICAgICBzdHlsZTogX3RoaXMucHJvcHMuc3R5bGVcbiAgICAgIH07XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy51bnNsaWNrKSB7XG4gICAgICAgIGxpc3RQcm9wcyA9IHtcbiAgICAgICAgICBjbGFzc05hbWU6IFwic2xpY2stbGlzdFwiXG4gICAgICAgIH07XG4gICAgICAgIGlubmVyU2xpZGVyUHJvcHMgPSB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdFtcImRlZmF1bHRcIl0uY3JlYXRlRWxlbWVudChcImRpdlwiLCBpbm5lclNsaWRlclByb3BzLCAhX3RoaXMucHJvcHMudW5zbGljayA/IHByZXZBcnJvdyA6IFwiXCIsIF9yZWFjdFtcImRlZmF1bHRcIl0uY3JlYXRlRWxlbWVudChcImRpdlwiLCBfZXh0ZW5kcyh7XG4gICAgICAgIHJlZjogX3RoaXMubGlzdFJlZkhhbmRsZXJcbiAgICAgIH0sIGxpc3RQcm9wcyksIF9yZWFjdFtcImRlZmF1bHRcIl0uY3JlYXRlRWxlbWVudChfdHJhY2suVHJhY2ssIF9leHRlbmRzKHtcbiAgICAgICAgcmVmOiBfdGhpcy50cmFja1JlZkhhbmRsZXJcbiAgICAgIH0sIHRyYWNrUHJvcHMpLCBfdGhpcy5wcm9wcy5jaGlsZHJlbikpLCAhX3RoaXMucHJvcHMudW5zbGljayA/IG5leHRBcnJvdyA6IFwiXCIsICFfdGhpcy5wcm9wcy51bnNsaWNrID8gZG90cyA6IFwiXCIpO1xuICAgIH0pO1xuXG4gICAgX3RoaXMubGlzdCA9IG51bGw7XG4gICAgX3RoaXMudHJhY2sgPSBudWxsO1xuICAgIF90aGlzLnN0YXRlID0gX29iamVjdFNwcmVhZCh7fSwgX2luaXRpYWxTdGF0ZVtcImRlZmF1bHRcIl0sIHtcbiAgICAgIGN1cnJlbnRTbGlkZTogX3RoaXMucHJvcHMuaW5pdGlhbFNsaWRlLFxuICAgICAgc2xpZGVDb3VudDogX3JlYWN0W1wiZGVmYXVsdFwiXS5DaGlsZHJlbi5jb3VudChfdGhpcy5wcm9wcy5jaGlsZHJlbilcbiAgICB9KTtcbiAgICBfdGhpcy5jYWxsYmFja1RpbWVycyA9IFtdO1xuICAgIF90aGlzLmNsaWNrYWJsZSA9IHRydWU7XG4gICAgX3RoaXMuZGVib3VuY2VkUmVzaXplID0gbnVsbDtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICByZXR1cm4gSW5uZXJTbGlkZXI7XG59KF9yZWFjdFtcImRlZmF1bHRcIl0uQ29tcG9uZW50KTtcblxuZXhwb3J0cy5Jbm5lclNsaWRlciA9IElubmVyU2xpZGVyOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUZBO0FBRkE7QUFBQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-slick/lib/inner-slider.js
