__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var FilterDropdownMenuWrapper = function FilterDropdownMenuWrapper(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: props.className,
    onClick: function onClick(e) {
      return e.stopPropagation();
    }
  }, props.children);
};

/* harmony default export */ __webpack_exports__["default"] = (FilterDropdownMenuWrapper);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90YWJsZS9GaWx0ZXJEcm9wZG93bk1lbnVXcmFwcGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90YWJsZS9GaWx0ZXJEcm9wZG93bk1lbnVXcmFwcGVyLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5jb25zdCBGaWx0ZXJEcm9wZG93bk1lbnVXcmFwcGVyID0gKHByb3BzKSA9PiAoPGRpdiBjbGFzc05hbWU9e3Byb3BzLmNsYXNzTmFtZX0gb25DbGljaz17ZSA9PiBlLnN0b3BQcm9wYWdhdGlvbigpfT5cbiAgICB7cHJvcHMuY2hpbGRyZW59XG4gIDwvZGl2Pik7XG5leHBvcnQgZGVmYXVsdCBGaWx0ZXJEcm9wZG93bk1lbnVXcmFwcGVyO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/table/FilterDropdownMenuWrapper.js
