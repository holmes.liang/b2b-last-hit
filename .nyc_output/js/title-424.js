/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../echarts */ "./node_modules/echarts/lib/echarts.js");

var graphic = __webpack_require__(/*! ../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var _layout = __webpack_require__(/*! ../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutRect = _layout.getLayoutRect;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Model

echarts.extendComponentModel({
  type: 'title',
  layoutMode: {
    type: 'box',
    ignoreSize: true
  },
  defaultOption: {
    // 一级层叠
    zlevel: 0,
    // 二级层叠
    z: 6,
    show: true,
    text: '',
    // 超链接跳转
    // link: null,
    // 仅支持self | blank
    target: 'blank',
    subtext: '',
    // 超链接跳转
    // sublink: null,
    // 仅支持self | blank
    subtarget: 'blank',
    // 'center' ¦ 'left' ¦ 'right'
    // ¦ {number}（x坐标，单位px）
    left: 0,
    // 'top' ¦ 'bottom' ¦ 'center'
    // ¦ {number}（y坐标，单位px）
    top: 0,
    // 水平对齐
    // 'auto' | 'left' | 'right' | 'center'
    // 默认根据 left 的位置判断是左对齐还是右对齐
    // textAlign: null
    //
    // 垂直对齐
    // 'auto' | 'top' | 'bottom' | 'middle'
    // 默认根据 top 位置判断是上对齐还是下对齐
    // textBaseline: null
    backgroundColor: 'rgba(0,0,0,0)',
    // 标题边框颜色
    borderColor: '#ccc',
    // 标题边框线宽，单位px，默认为0（无边框）
    borderWidth: 0,
    // 标题内边距，单位px，默认各方向内边距为5，
    // 接受数组分别设定上右下左边距，同css
    padding: 5,
    // 主副标题纵向间隔，单位px，默认为10，
    itemGap: 10,
    textStyle: {
      fontSize: 18,
      fontWeight: 'bolder',
      color: '#333'
    },
    subtextStyle: {
      color: '#aaa'
    }
  }
}); // View

echarts.extendComponentView({
  type: 'title',
  render: function render(titleModel, ecModel, api) {
    this.group.removeAll();

    if (!titleModel.get('show')) {
      return;
    }

    var group = this.group;
    var textStyleModel = titleModel.getModel('textStyle');
    var subtextStyleModel = titleModel.getModel('subtextStyle');
    var textAlign = titleModel.get('textAlign');
    var textBaseline = titleModel.get('textBaseline');
    var textEl = new graphic.Text({
      style: graphic.setTextStyle({}, textStyleModel, {
        text: titleModel.get('text'),
        textFill: textStyleModel.getTextColor()
      }, {
        disableBox: true
      }),
      z2: 10
    });
    var textRect = textEl.getBoundingRect();
    var subText = titleModel.get('subtext');
    var subTextEl = new graphic.Text({
      style: graphic.setTextStyle({}, subtextStyleModel, {
        text: subText,
        textFill: subtextStyleModel.getTextColor(),
        y: textRect.height + titleModel.get('itemGap'),
        textVerticalAlign: 'top'
      }, {
        disableBox: true
      }),
      z2: 10
    });
    var link = titleModel.get('link');
    var sublink = titleModel.get('sublink');
    var triggerEvent = titleModel.get('triggerEvent', true);
    textEl.silent = !link && !triggerEvent;
    subTextEl.silent = !sublink && !triggerEvent;

    if (link) {
      textEl.on('click', function () {
        window.open(link, '_' + titleModel.get('target'));
      });
    }

    if (sublink) {
      subTextEl.on('click', function () {
        window.open(sublink, '_' + titleModel.get('subtarget'));
      });
    }

    textEl.eventData = subTextEl.eventData = triggerEvent ? {
      componentType: 'title',
      componentIndex: titleModel.componentIndex
    } : null;
    group.add(textEl);
    subText && group.add(subTextEl); // If no subText, but add subTextEl, there will be an empty line.

    var groupRect = group.getBoundingRect();
    var layoutOption = titleModel.getBoxLayoutParams();
    layoutOption.width = groupRect.width;
    layoutOption.height = groupRect.height;
    var layoutRect = getLayoutRect(layoutOption, {
      width: api.getWidth(),
      height: api.getHeight()
    }, titleModel.get('padding')); // Adjust text align based on position

    if (!textAlign) {
      // Align left if title is on the left. center and right is same
      textAlign = titleModel.get('left') || titleModel.get('right');

      if (textAlign === 'middle') {
        textAlign = 'center';
      } // Adjust layout by text align


      if (textAlign === 'right') {
        layoutRect.x += layoutRect.width;
      } else if (textAlign === 'center') {
        layoutRect.x += layoutRect.width / 2;
      }
    }

    if (!textBaseline) {
      textBaseline = titleModel.get('top') || titleModel.get('bottom');

      if (textBaseline === 'center') {
        textBaseline = 'middle';
      }

      if (textBaseline === 'bottom') {
        layoutRect.y += layoutRect.height;
      } else if (textBaseline === 'middle') {
        layoutRect.y += layoutRect.height / 2;
      }

      textBaseline = textBaseline || 'top';
    }

    group.attr('position', [layoutRect.x, layoutRect.y]);
    var alignStyle = {
      textAlign: textAlign,
      textVerticalAlign: textBaseline
    };
    textEl.setStyle(alignStyle);
    subTextEl.setStyle(alignStyle); // Render background
    // Get groupRect again because textAlign has been changed

    groupRect = group.getBoundingRect();
    var padding = layoutRect.margin;
    var style = titleModel.getItemStyle(['color', 'opacity']);
    style.fill = titleModel.get('backgroundColor');
    var rect = new graphic.Rect({
      shape: {
        x: groupRect.x - padding[3],
        y: groupRect.y - padding[0],
        width: groupRect.width + padding[1] + padding[3],
        height: groupRect.height + padding[0] + padding[2],
        r: titleModel.get('borderRadius')
      },
      style: style,
      silent: true
    });
    graphic.subPixelOptimizeRect(rect);
    group.add(rect);
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3RpdGxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3RpdGxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgZWNoYXJ0cyA9IHJlcXVpcmUoXCIuLi9lY2hhcnRzXCIpO1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi91dGlsL2dyYXBoaWNcIik7XG5cbnZhciBfbGF5b3V0ID0gcmVxdWlyZShcIi4uL3V0aWwvbGF5b3V0XCIpO1xuXG52YXIgZ2V0TGF5b3V0UmVjdCA9IF9sYXlvdXQuZ2V0TGF5b3V0UmVjdDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gTW9kZWxcbmVjaGFydHMuZXh0ZW5kQ29tcG9uZW50TW9kZWwoe1xuICB0eXBlOiAndGl0bGUnLFxuICBsYXlvdXRNb2RlOiB7XG4gICAgdHlwZTogJ2JveCcsXG4gICAgaWdub3JlU2l6ZTogdHJ1ZVxuICB9LFxuICBkZWZhdWx0T3B0aW9uOiB7XG4gICAgLy8g5LiA57qn5bGC5Y+gXG4gICAgemxldmVsOiAwLFxuICAgIC8vIOS6jOe6p+WxguWPoFxuICAgIHo6IDYsXG4gICAgc2hvdzogdHJ1ZSxcbiAgICB0ZXh0OiAnJyxcbiAgICAvLyDotoXpk77mjqXot7PovaxcbiAgICAvLyBsaW5rOiBudWxsLFxuICAgIC8vIOS7heaUr+aMgXNlbGYgfCBibGFua1xuICAgIHRhcmdldDogJ2JsYW5rJyxcbiAgICBzdWJ0ZXh0OiAnJyxcbiAgICAvLyDotoXpk77mjqXot7PovaxcbiAgICAvLyBzdWJsaW5rOiBudWxsLFxuICAgIC8vIOS7heaUr+aMgXNlbGYgfCBibGFua1xuICAgIHN1YnRhcmdldDogJ2JsYW5rJyxcbiAgICAvLyAnY2VudGVyJyDCpiAnbGVmdCcgwqYgJ3JpZ2h0J1xuICAgIC8vIMKmIHtudW1iZXJ977yIeOWdkOagh++8jOWNleS9jXB477yJXG4gICAgbGVmdDogMCxcbiAgICAvLyAndG9wJyDCpiAnYm90dG9tJyDCpiAnY2VudGVyJ1xuICAgIC8vIMKmIHtudW1iZXJ977yIeeWdkOagh++8jOWNleS9jXB477yJXG4gICAgdG9wOiAwLFxuICAgIC8vIOawtOW5s+Wvuem9kFxuICAgIC8vICdhdXRvJyB8ICdsZWZ0JyB8ICdyaWdodCcgfCAnY2VudGVyJ1xuICAgIC8vIOm7mOiupOagueaNriBsZWZ0IOeahOS9jee9ruWIpOaWreaYr+W3puWvuem9kOi/mOaYr+WPs+Wvuem9kFxuICAgIC8vIHRleHRBbGlnbjogbnVsbFxuICAgIC8vXG4gICAgLy8g5Z6C55u05a+56b2QXG4gICAgLy8gJ2F1dG8nIHwgJ3RvcCcgfCAnYm90dG9tJyB8ICdtaWRkbGUnXG4gICAgLy8g6buY6K6k5qC55o2uIHRvcCDkvY3nva7liKTmlq3mmK/kuIrlr7npvZDov5jmmK/kuIvlr7npvZBcbiAgICAvLyB0ZXh0QmFzZWxpbmU6IG51bGxcbiAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsMCwwLDApJyxcbiAgICAvLyDmoIfpopjovrnmoYbpopzoibJcbiAgICBib3JkZXJDb2xvcjogJyNjY2MnLFxuICAgIC8vIOagh+mimOi+ueahhue6v+Wuve+8jOWNleS9jXB477yM6buY6K6k5Li6MO+8iOaXoOi+ueahhu+8iVxuICAgIGJvcmRlcldpZHRoOiAwLFxuICAgIC8vIOagh+mimOWGhei+uei3ne+8jOWNleS9jXB477yM6buY6K6k5ZCE5pa55ZCR5YaF6L656Led5Li6Ne+8jFxuICAgIC8vIOaOpeWPl+aVsOe7hOWIhuWIq+iuvuWumuS4iuWPs+S4i+W3pui+uei3ne+8jOWQjGNzc1xuICAgIHBhZGRpbmc6IDUsXG4gICAgLy8g5Li75Ymv5qCH6aKY57q15ZCR6Ze06ZqU77yM5Y2V5L2NcHjvvIzpu5jorqTkuLoxMO+8jFxuICAgIGl0ZW1HYXA6IDEwLFxuICAgIHRleHRTdHlsZToge1xuICAgICAgZm9udFNpemU6IDE4LFxuICAgICAgZm9udFdlaWdodDogJ2JvbGRlcicsXG4gICAgICBjb2xvcjogJyMzMzMnXG4gICAgfSxcbiAgICBzdWJ0ZXh0U3R5bGU6IHtcbiAgICAgIGNvbG9yOiAnI2FhYSdcbiAgICB9XG4gIH1cbn0pOyAvLyBWaWV3XG5cbmVjaGFydHMuZXh0ZW5kQ29tcG9uZW50Vmlldyh7XG4gIHR5cGU6ICd0aXRsZScsXG4gIHJlbmRlcjogZnVuY3Rpb24gKHRpdGxlTW9kZWwsIGVjTW9kZWwsIGFwaSkge1xuICAgIHRoaXMuZ3JvdXAucmVtb3ZlQWxsKCk7XG5cbiAgICBpZiAoIXRpdGxlTW9kZWwuZ2V0KCdzaG93JykpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZ3JvdXAgPSB0aGlzLmdyb3VwO1xuICAgIHZhciB0ZXh0U3R5bGVNb2RlbCA9IHRpdGxlTW9kZWwuZ2V0TW9kZWwoJ3RleHRTdHlsZScpO1xuICAgIHZhciBzdWJ0ZXh0U3R5bGVNb2RlbCA9IHRpdGxlTW9kZWwuZ2V0TW9kZWwoJ3N1YnRleHRTdHlsZScpO1xuICAgIHZhciB0ZXh0QWxpZ24gPSB0aXRsZU1vZGVsLmdldCgndGV4dEFsaWduJyk7XG4gICAgdmFyIHRleHRCYXNlbGluZSA9IHRpdGxlTW9kZWwuZ2V0KCd0ZXh0QmFzZWxpbmUnKTtcbiAgICB2YXIgdGV4dEVsID0gbmV3IGdyYXBoaWMuVGV4dCh7XG4gICAgICBzdHlsZTogZ3JhcGhpYy5zZXRUZXh0U3R5bGUoe30sIHRleHRTdHlsZU1vZGVsLCB7XG4gICAgICAgIHRleHQ6IHRpdGxlTW9kZWwuZ2V0KCd0ZXh0JyksXG4gICAgICAgIHRleHRGaWxsOiB0ZXh0U3R5bGVNb2RlbC5nZXRUZXh0Q29sb3IoKVxuICAgICAgfSwge1xuICAgICAgICBkaXNhYmxlQm94OiB0cnVlXG4gICAgICB9KSxcbiAgICAgIHoyOiAxMFxuICAgIH0pO1xuICAgIHZhciB0ZXh0UmVjdCA9IHRleHRFbC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgc3ViVGV4dCA9IHRpdGxlTW9kZWwuZ2V0KCdzdWJ0ZXh0Jyk7XG4gICAgdmFyIHN1YlRleHRFbCA9IG5ldyBncmFwaGljLlRleHQoe1xuICAgICAgc3R5bGU6IGdyYXBoaWMuc2V0VGV4dFN0eWxlKHt9LCBzdWJ0ZXh0U3R5bGVNb2RlbCwge1xuICAgICAgICB0ZXh0OiBzdWJUZXh0LFxuICAgICAgICB0ZXh0RmlsbDogc3VidGV4dFN0eWxlTW9kZWwuZ2V0VGV4dENvbG9yKCksXG4gICAgICAgIHk6IHRleHRSZWN0LmhlaWdodCArIHRpdGxlTW9kZWwuZ2V0KCdpdGVtR2FwJyksXG4gICAgICAgIHRleHRWZXJ0aWNhbEFsaWduOiAndG9wJ1xuICAgICAgfSwge1xuICAgICAgICBkaXNhYmxlQm94OiB0cnVlXG4gICAgICB9KSxcbiAgICAgIHoyOiAxMFxuICAgIH0pO1xuICAgIHZhciBsaW5rID0gdGl0bGVNb2RlbC5nZXQoJ2xpbmsnKTtcbiAgICB2YXIgc3VibGluayA9IHRpdGxlTW9kZWwuZ2V0KCdzdWJsaW5rJyk7XG4gICAgdmFyIHRyaWdnZXJFdmVudCA9IHRpdGxlTW9kZWwuZ2V0KCd0cmlnZ2VyRXZlbnQnLCB0cnVlKTtcbiAgICB0ZXh0RWwuc2lsZW50ID0gIWxpbmsgJiYgIXRyaWdnZXJFdmVudDtcbiAgICBzdWJUZXh0RWwuc2lsZW50ID0gIXN1YmxpbmsgJiYgIXRyaWdnZXJFdmVudDtcblxuICAgIGlmIChsaW5rKSB7XG4gICAgICB0ZXh0RWwub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICB3aW5kb3cub3BlbihsaW5rLCAnXycgKyB0aXRsZU1vZGVsLmdldCgndGFyZ2V0JykpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHN1YmxpbmspIHtcbiAgICAgIHN1YlRleHRFbC5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHdpbmRvdy5vcGVuKHN1YmxpbmssICdfJyArIHRpdGxlTW9kZWwuZ2V0KCdzdWJ0YXJnZXQnKSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0ZXh0RWwuZXZlbnREYXRhID0gc3ViVGV4dEVsLmV2ZW50RGF0YSA9IHRyaWdnZXJFdmVudCA/IHtcbiAgICAgIGNvbXBvbmVudFR5cGU6ICd0aXRsZScsXG4gICAgICBjb21wb25lbnRJbmRleDogdGl0bGVNb2RlbC5jb21wb25lbnRJbmRleFxuICAgIH0gOiBudWxsO1xuICAgIGdyb3VwLmFkZCh0ZXh0RWwpO1xuICAgIHN1YlRleHQgJiYgZ3JvdXAuYWRkKHN1YlRleHRFbCk7IC8vIElmIG5vIHN1YlRleHQsIGJ1dCBhZGQgc3ViVGV4dEVsLCB0aGVyZSB3aWxsIGJlIGFuIGVtcHR5IGxpbmUuXG5cbiAgICB2YXIgZ3JvdXBSZWN0ID0gZ3JvdXAuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgdmFyIGxheW91dE9wdGlvbiA9IHRpdGxlTW9kZWwuZ2V0Qm94TGF5b3V0UGFyYW1zKCk7XG4gICAgbGF5b3V0T3B0aW9uLndpZHRoID0gZ3JvdXBSZWN0LndpZHRoO1xuICAgIGxheW91dE9wdGlvbi5oZWlnaHQgPSBncm91cFJlY3QuaGVpZ2h0O1xuICAgIHZhciBsYXlvdXRSZWN0ID0gZ2V0TGF5b3V0UmVjdChsYXlvdXRPcHRpb24sIHtcbiAgICAgIHdpZHRoOiBhcGkuZ2V0V2lkdGgoKSxcbiAgICAgIGhlaWdodDogYXBpLmdldEhlaWdodCgpXG4gICAgfSwgdGl0bGVNb2RlbC5nZXQoJ3BhZGRpbmcnKSk7IC8vIEFkanVzdCB0ZXh0IGFsaWduIGJhc2VkIG9uIHBvc2l0aW9uXG5cbiAgICBpZiAoIXRleHRBbGlnbikge1xuICAgICAgLy8gQWxpZ24gbGVmdCBpZiB0aXRsZSBpcyBvbiB0aGUgbGVmdC4gY2VudGVyIGFuZCByaWdodCBpcyBzYW1lXG4gICAgICB0ZXh0QWxpZ24gPSB0aXRsZU1vZGVsLmdldCgnbGVmdCcpIHx8IHRpdGxlTW9kZWwuZ2V0KCdyaWdodCcpO1xuXG4gICAgICBpZiAodGV4dEFsaWduID09PSAnbWlkZGxlJykge1xuICAgICAgICB0ZXh0QWxpZ24gPSAnY2VudGVyJztcbiAgICAgIH0gLy8gQWRqdXN0IGxheW91dCBieSB0ZXh0IGFsaWduXG5cblxuICAgICAgaWYgKHRleHRBbGlnbiA9PT0gJ3JpZ2h0Jykge1xuICAgICAgICBsYXlvdXRSZWN0LnggKz0gbGF5b3V0UmVjdC53aWR0aDtcbiAgICAgIH0gZWxzZSBpZiAodGV4dEFsaWduID09PSAnY2VudGVyJykge1xuICAgICAgICBsYXlvdXRSZWN0LnggKz0gbGF5b3V0UmVjdC53aWR0aCAvIDI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCF0ZXh0QmFzZWxpbmUpIHtcbiAgICAgIHRleHRCYXNlbGluZSA9IHRpdGxlTW9kZWwuZ2V0KCd0b3AnKSB8fCB0aXRsZU1vZGVsLmdldCgnYm90dG9tJyk7XG5cbiAgICAgIGlmICh0ZXh0QmFzZWxpbmUgPT09ICdjZW50ZXInKSB7XG4gICAgICAgIHRleHRCYXNlbGluZSA9ICdtaWRkbGUnO1xuICAgICAgfVxuXG4gICAgICBpZiAodGV4dEJhc2VsaW5lID09PSAnYm90dG9tJykge1xuICAgICAgICBsYXlvdXRSZWN0LnkgKz0gbGF5b3V0UmVjdC5oZWlnaHQ7XG4gICAgICB9IGVsc2UgaWYgKHRleHRCYXNlbGluZSA9PT0gJ21pZGRsZScpIHtcbiAgICAgICAgbGF5b3V0UmVjdC55ICs9IGxheW91dFJlY3QuaGVpZ2h0IC8gMjtcbiAgICAgIH1cblxuICAgICAgdGV4dEJhc2VsaW5lID0gdGV4dEJhc2VsaW5lIHx8ICd0b3AnO1xuICAgIH1cblxuICAgIGdyb3VwLmF0dHIoJ3Bvc2l0aW9uJywgW2xheW91dFJlY3QueCwgbGF5b3V0UmVjdC55XSk7XG4gICAgdmFyIGFsaWduU3R5bGUgPSB7XG4gICAgICB0ZXh0QWxpZ246IHRleHRBbGlnbixcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduOiB0ZXh0QmFzZWxpbmVcbiAgICB9O1xuICAgIHRleHRFbC5zZXRTdHlsZShhbGlnblN0eWxlKTtcbiAgICBzdWJUZXh0RWwuc2V0U3R5bGUoYWxpZ25TdHlsZSk7IC8vIFJlbmRlciBiYWNrZ3JvdW5kXG4gICAgLy8gR2V0IGdyb3VwUmVjdCBhZ2FpbiBiZWNhdXNlIHRleHRBbGlnbiBoYXMgYmVlbiBjaGFuZ2VkXG5cbiAgICBncm91cFJlY3QgPSBncm91cC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgcGFkZGluZyA9IGxheW91dFJlY3QubWFyZ2luO1xuICAgIHZhciBzdHlsZSA9IHRpdGxlTW9kZWwuZ2V0SXRlbVN0eWxlKFsnY29sb3InLCAnb3BhY2l0eSddKTtcbiAgICBzdHlsZS5maWxsID0gdGl0bGVNb2RlbC5nZXQoJ2JhY2tncm91bmRDb2xvcicpO1xuICAgIHZhciByZWN0ID0gbmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgICBzaGFwZToge1xuICAgICAgICB4OiBncm91cFJlY3QueCAtIHBhZGRpbmdbM10sXG4gICAgICAgIHk6IGdyb3VwUmVjdC55IC0gcGFkZGluZ1swXSxcbiAgICAgICAgd2lkdGg6IGdyb3VwUmVjdC53aWR0aCArIHBhZGRpbmdbMV0gKyBwYWRkaW5nWzNdLFxuICAgICAgICBoZWlnaHQ6IGdyb3VwUmVjdC5oZWlnaHQgKyBwYWRkaW5nWzBdICsgcGFkZGluZ1syXSxcbiAgICAgICAgcjogdGl0bGVNb2RlbC5nZXQoJ2JvcmRlclJhZGl1cycpXG4gICAgICB9LFxuICAgICAgc3R5bGU6IHN0eWxlLFxuICAgICAgc2lsZW50OiB0cnVlXG4gICAgfSk7XG4gICAgZ3JhcGhpYy5zdWJQaXhlbE9wdGltaXplUmVjdChyZWN0KTtcbiAgICBncm91cC5hZGQocmVjdCk7XG4gIH1cbn0pOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQURBO0FBOUNBO0FBTkE7QUFDQTtBQXlEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBREE7QUFHQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFEQTtBQUdBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFoSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/title.js
