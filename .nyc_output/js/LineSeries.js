/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var createListFromArray = __webpack_require__(/*! ../helper/createListFromArray */ "./node_modules/echarts/lib/chart/helper/createListFromArray.js");

var SeriesModel = __webpack_require__(/*! ../../model/Series */ "./node_modules/echarts/lib/model/Series.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var _default = SeriesModel.extend({
  type: 'series.line',
  dependencies: ['grid', 'polar'],
  getInitialData: function getInitialData(option, ecModel) {
    return createListFromArray(this.getSource(), this);
  },
  defaultOption: {
    zlevel: 0,
    z: 2,
    coordinateSystem: 'cartesian2d',
    legendHoverLink: true,
    hoverAnimation: true,
    // stack: null
    // xAxisIndex: 0,
    // yAxisIndex: 0,
    // polarIndex: 0,
    // If clip the overflow value
    clipOverflow: true,
    // cursor: null,
    label: {
      position: 'top'
    },
    // itemStyle: {
    // },
    lineStyle: {
      width: 2,
      type: 'solid'
    },
    // areaStyle: {
    // origin of areaStyle. Valid values:
    // `'auto'/null/undefined`: from axisLine to data
    // `'start'`: from min to data
    // `'end'`: from data to max
    // origin: 'auto'
    // },
    // false, 'start', 'end', 'middle'
    step: false,
    // Disabled if step is true
    smooth: false,
    smoothMonotone: null,
    symbol: 'emptyCircle',
    symbolSize: 4,
    symbolRotate: null,
    showSymbol: true,
    // `false`: follow the label interval strategy.
    // `true`: show all symbols.
    // `'auto'`: If possible, show all symbols, otherwise
    //           follow the label interval strategy.
    showAllSymbol: 'auto',
    // Whether to connect break point.
    connectNulls: false,
    // Sampling for large data. Can be: 'average', 'max', 'min', 'sum'.
    sampling: 'none',
    animationEasing: 'linear',
    // Disable progressive
    progressive: 0,
    hoverLayerThreshold: Infinity
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9MaW5lU2VyaWVzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9MaW5lU2VyaWVzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgY3JlYXRlTGlzdEZyb21BcnJheSA9IHJlcXVpcmUoXCIuLi9oZWxwZXIvY3JlYXRlTGlzdEZyb21BcnJheVwiKTtcblxudmFyIFNlcmllc01vZGVsID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL1Nlcmllc1wiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIF9kZWZhdWx0ID0gU2VyaWVzTW9kZWwuZXh0ZW5kKHtcbiAgdHlwZTogJ3Nlcmllcy5saW5lJyxcbiAgZGVwZW5kZW5jaWVzOiBbJ2dyaWQnLCAncG9sYXInXSxcbiAgZ2V0SW5pdGlhbERhdGE6IGZ1bmN0aW9uIChvcHRpb24sIGVjTW9kZWwpIHtcbiAgICByZXR1cm4gY3JlYXRlTGlzdEZyb21BcnJheSh0aGlzLmdldFNvdXJjZSgpLCB0aGlzKTtcbiAgfSxcbiAgZGVmYXVsdE9wdGlvbjoge1xuICAgIHpsZXZlbDogMCxcbiAgICB6OiAyLFxuICAgIGNvb3JkaW5hdGVTeXN0ZW06ICdjYXJ0ZXNpYW4yZCcsXG4gICAgbGVnZW5kSG92ZXJMaW5rOiB0cnVlLFxuICAgIGhvdmVyQW5pbWF0aW9uOiB0cnVlLFxuICAgIC8vIHN0YWNrOiBudWxsXG4gICAgLy8geEF4aXNJbmRleDogMCxcbiAgICAvLyB5QXhpc0luZGV4OiAwLFxuICAgIC8vIHBvbGFySW5kZXg6IDAsXG4gICAgLy8gSWYgY2xpcCB0aGUgb3ZlcmZsb3cgdmFsdWVcbiAgICBjbGlwT3ZlcmZsb3c6IHRydWUsXG4gICAgLy8gY3Vyc29yOiBudWxsLFxuICAgIGxhYmVsOiB7XG4gICAgICBwb3NpdGlvbjogJ3RvcCdcbiAgICB9LFxuICAgIC8vIGl0ZW1TdHlsZToge1xuICAgIC8vIH0sXG4gICAgbGluZVN0eWxlOiB7XG4gICAgICB3aWR0aDogMixcbiAgICAgIHR5cGU6ICdzb2xpZCdcbiAgICB9LFxuICAgIC8vIGFyZWFTdHlsZToge1xuICAgIC8vIG9yaWdpbiBvZiBhcmVhU3R5bGUuIFZhbGlkIHZhbHVlczpcbiAgICAvLyBgJ2F1dG8nL251bGwvdW5kZWZpbmVkYDogZnJvbSBheGlzTGluZSB0byBkYXRhXG4gICAgLy8gYCdzdGFydCdgOiBmcm9tIG1pbiB0byBkYXRhXG4gICAgLy8gYCdlbmQnYDogZnJvbSBkYXRhIHRvIG1heFxuICAgIC8vIG9yaWdpbjogJ2F1dG8nXG4gICAgLy8gfSxcbiAgICAvLyBmYWxzZSwgJ3N0YXJ0JywgJ2VuZCcsICdtaWRkbGUnXG4gICAgc3RlcDogZmFsc2UsXG4gICAgLy8gRGlzYWJsZWQgaWYgc3RlcCBpcyB0cnVlXG4gICAgc21vb3RoOiBmYWxzZSxcbiAgICBzbW9vdGhNb25vdG9uZTogbnVsbCxcbiAgICBzeW1ib2w6ICdlbXB0eUNpcmNsZScsXG4gICAgc3ltYm9sU2l6ZTogNCxcbiAgICBzeW1ib2xSb3RhdGU6IG51bGwsXG4gICAgc2hvd1N5bWJvbDogdHJ1ZSxcbiAgICAvLyBgZmFsc2VgOiBmb2xsb3cgdGhlIGxhYmVsIGludGVydmFsIHN0cmF0ZWd5LlxuICAgIC8vIGB0cnVlYDogc2hvdyBhbGwgc3ltYm9scy5cbiAgICAvLyBgJ2F1dG8nYDogSWYgcG9zc2libGUsIHNob3cgYWxsIHN5bWJvbHMsIG90aGVyd2lzZVxuICAgIC8vICAgICAgICAgICBmb2xsb3cgdGhlIGxhYmVsIGludGVydmFsIHN0cmF0ZWd5LlxuICAgIHNob3dBbGxTeW1ib2w6ICdhdXRvJyxcbiAgICAvLyBXaGV0aGVyIHRvIGNvbm5lY3QgYnJlYWsgcG9pbnQuXG4gICAgY29ubmVjdE51bGxzOiBmYWxzZSxcbiAgICAvLyBTYW1wbGluZyBmb3IgbGFyZ2UgZGF0YS4gQ2FuIGJlOiAnYXZlcmFnZScsICdtYXgnLCAnbWluJywgJ3N1bScuXG4gICAgc2FtcGxpbmc6ICdub25lJyxcbiAgICBhbmltYXRpb25FYXNpbmc6ICdsaW5lYXInLFxuICAgIC8vIERpc2FibGUgcHJvZ3Jlc3NpdmVcbiAgICBwcm9ncmVzc2l2ZTogMCxcbiAgICBob3ZlckxheWVyVGhyZXNob2xkOiBJbmZpbml0eVxuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbERBO0FBTkE7QUFDQTtBQTJEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/line/LineSeries.js
