__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return create; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index */ "./node_modules/antd/es/icon/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};



var customCache = new Set();
function create() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var scriptUrl = options.scriptUrl,
      _options$extraCommonP = options.extraCommonProps,
      extraCommonProps = _options$extraCommonP === void 0 ? {} : _options$extraCommonP;
  /**
   * DOM API required.
   * Make sure in browser environment.
   * The Custom Icon will create a <script/>
   * that loads SVG symbols and insert the SVG Element into the document body.
   */

  if (typeof document !== 'undefined' && typeof window !== 'undefined' && typeof document.createElement === 'function' && typeof scriptUrl === 'string' && scriptUrl.length && !customCache.has(scriptUrl)) {
    var script = document.createElement('script');
    script.setAttribute('src', scriptUrl);
    script.setAttribute('data-namespace', scriptUrl);
    customCache.add(scriptUrl);
    document.body.appendChild(script);
  }

  var Iconfont = function Iconfont(props) {
    var type = props.type,
        children = props.children,
        restProps = __rest(props, ["type", "children"]); // component > children > type


    var content = null;

    if (props.type) {
      content = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("use", {
        xlinkHref: "#".concat(type)
      });
    }

    if (children) {
      content = children;
    }

    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_index__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, extraCommonProps, restProps), content);
  };

  Iconfont.displayName = 'Iconfont';
  return Iconfont;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pY29uL0ljb25Gb250LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9pY29uL0ljb25Gb250LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgSWNvbiBmcm9tICcuL2luZGV4JztcbmNvbnN0IGN1c3RvbUNhY2hlID0gbmV3IFNldCgpO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY3JlYXRlKG9wdGlvbnMgPSB7fSkge1xuICAgIGNvbnN0IHsgc2NyaXB0VXJsLCBleHRyYUNvbW1vblByb3BzID0ge30gfSA9IG9wdGlvbnM7XG4gICAgLyoqXG4gICAgICogRE9NIEFQSSByZXF1aXJlZC5cbiAgICAgKiBNYWtlIHN1cmUgaW4gYnJvd3NlciBlbnZpcm9ubWVudC5cbiAgICAgKiBUaGUgQ3VzdG9tIEljb24gd2lsbCBjcmVhdGUgYSA8c2NyaXB0Lz5cbiAgICAgKiB0aGF0IGxvYWRzIFNWRyBzeW1ib2xzIGFuZCBpbnNlcnQgdGhlIFNWRyBFbGVtZW50IGludG8gdGhlIGRvY3VtZW50IGJvZHkuXG4gICAgICovXG4gICAgaWYgKHR5cGVvZiBkb2N1bWVudCAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICAgICAgdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICAgICAgdHlwZW9mIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQgPT09ICdmdW5jdGlvbicgJiZcbiAgICAgICAgdHlwZW9mIHNjcmlwdFVybCA9PT0gJ3N0cmluZycgJiZcbiAgICAgICAgc2NyaXB0VXJsLmxlbmd0aCAmJlxuICAgICAgICAhY3VzdG9tQ2FjaGUuaGFzKHNjcmlwdFVybCkpIHtcbiAgICAgICAgY29uc3Qgc2NyaXB0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAgIHNjcmlwdC5zZXRBdHRyaWJ1dGUoJ3NyYycsIHNjcmlwdFVybCk7XG4gICAgICAgIHNjcmlwdC5zZXRBdHRyaWJ1dGUoJ2RhdGEtbmFtZXNwYWNlJywgc2NyaXB0VXJsKTtcbiAgICAgICAgY3VzdG9tQ2FjaGUuYWRkKHNjcmlwdFVybCk7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2NyaXB0KTtcbiAgICB9XG4gICAgY29uc3QgSWNvbmZvbnQgPSBwcm9wcyA9PiB7XG4gICAgICAgIGNvbnN0IHsgdHlwZSwgY2hpbGRyZW4gfSA9IHByb3BzLCByZXN0UHJvcHMgPSBfX3Jlc3QocHJvcHMsIFtcInR5cGVcIiwgXCJjaGlsZHJlblwiXSk7XG4gICAgICAgIC8vIGNvbXBvbmVudCA+IGNoaWxkcmVuID4gdHlwZVxuICAgICAgICBsZXQgY29udGVudCA9IG51bGw7XG4gICAgICAgIGlmIChwcm9wcy50eXBlKSB7XG4gICAgICAgICAgICBjb250ZW50ID0gPHVzZSB4bGlua0hyZWY9e2AjJHt0eXBlfWB9Lz47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICAgICAgICBjb250ZW50ID0gY2hpbGRyZW47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICg8SWNvbiB7Li4uZXh0cmFDb21tb25Qcm9wc30gey4uLnJlc3RQcm9wc30+XG4gICAgICAgIHtjb250ZW50fVxuICAgICAgPC9JY29uPik7XG4gICAgfTtcbiAgICBJY29uZm9udC5kaXNwbGF5TmFtZSA9ICdJY29uZm9udCc7XG4gICAgcmV0dXJuIEljb25mb250O1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBOzs7Ozs7O0FBTUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBVkE7QUFDQTtBQWFBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/icon/IconFont.js
