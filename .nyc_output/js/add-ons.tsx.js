__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formDateLayout", function() { return formDateLayout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOns", function() { return AddOns; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component_wizard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/compare/all-steps.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/query/prod-type */ "./src/app/desk/component/query/prod-type.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/compare/vmi/add-ons.tsx";












var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].getIsMobile();

var AddOns =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(AddOns, _QuoteStep);

  function AddOns() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, AddOns);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(AddOns)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.handleNext = function () {
      _this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var cartItems = _this.state.cartItems;

        var newPolicy = _this.props.mergePolicyToServiceModel();

        cartItems.map(function (every, index) {
          newPolicy.items[index].selected = every.selected;
        });
        newPolicy.policy.ext._ui.step = _all_steps__WEBPACK_IMPORTED_MODULE_12__["AllSteps"].DETAILS;

        _this.getHelpers().getAjax().post("/cart/merge", newPolicy).then(function (response) {
          var _ref = response.body || {
            respData: {}
          },
              respData = _ref.respData;

          _this.props.mergePolicyToUIModel(respData);

          Object(_component_wizard__WEBPACK_IMPORTED_MODULE_11__["pageTo"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this), _all_steps__WEBPACK_IMPORTED_MODULE_12__["AllSteps"].DETAILS);
        });
      });
    };

    _this.handleSave = function () {
      var cartItems = _this.state.cartItems;

      var newPolicy = _this.props.mergePolicyToServiceModel();

      cartItems.map(function (every, index) {
        newPolicy.items[index].selected = every.selected;
      });

      _this.getHelpers().getAjax().put("/cart/merge", newPolicy).then(function (response) {
        var _ref2 = response.body || {
          respData: {}
        },
            respData = _ref2.respData;

        _this.props.mergePolicyToUIModel(respData);

        antd__WEBPACK_IMPORTED_MODULE_10__["notification"].success({
          message: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Saved successfully").thai("บันทึกคำพูดสำเร็จแล้ว").getMessage()
        });
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(AddOns, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.getValueFromModel("items.0.ext")) {
        this.getCartNew();
      } else {
        this.initDob();
        this.setState({
          activeKeys: this.setActiveKeys(this.getValueFromModel("items"))
        });
      }
    }
  }, {
    key: "initDob",
    value: function initDob() {
      var ind = -1;
      (this.getValueFromModel("items") || []).map(function (every, index) {
        if (every.productCode === "PA") {
          ind = index;
        }
      });
      var dob = this.getValueFromModel("items.".concat(ind, ".ext.dob"));
      dob = moment__WEBPACK_IMPORTED_MODULE_15___default()(dob, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE).format(_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].DATE_FORMAT.DATE_FORMAT);

      if (dob && dob.split("/").length === 3) {
        this.setState({
          dob: {
            day: dob.split("/")[0],
            month: dob.split("/")[1],
            year: dob.split("/")[2]
          }
        });
      }
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        disabled: false,
        checkedPolicies: new Map(),
        activeKeys: [],
        cartItems: this.getValueFromModel("items") || [],
        loading: false,
        dob: {}
      };
    }
  }, {
    key: "getCartNew",
    value: function getCartNew() {
      var _this2 = this;

      _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].post("/cart/new", lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(this.props.mergePolicyToServiceModel(), "policy"), {
        loading: true
      }).then(function (res) {
        var respData = (res.body || {}).respData || {};

        _this2.setState({
          cartItems: respData.items,
          activeKeys: _this2.setActiveKeys(respData.items),
          loading: false
        }, function () {
          _this2.props.mergePolicyToUIModel(respData);

          _this2.initDob();
        });
      }).catch(function () {});
    }
  }, {
    key: "setActiveKeys",
    value: function setActiveKeys(items) {
      var activeKeys = [];
      (items || []).map(function (every, index) {
        if (every.selected === "Y") {
          activeKeys.push(index);
        }
      });
      return activeKeys;
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(index, propName) {
      if (!propName) return "items.".concat(index, ".ext");
      return "items.".concat(index, ".ext.").concat(propName);
    }
  }, {
    key: "renderActions",
    value: function renderActions() {
      var _this3 = this;

      var disabled = this.state.disabled;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 158
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_18__["ButtonBack"], {
        handel: function handel() {
          Object(_component_wizard__WEBPACK_IMPORTED_MODULE_11__["pageTo"])(_this3, _all_steps__WEBPACK_IMPORTED_MODULE_12__["AllSteps"].PLANS);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Button"], {
        size: "large",
        type: "primary",
        disabled: disabled,
        onClick: this.handleNext,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 163
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Next").thai("ต่อไป").my("နောက်တစ်ခု").getMessage()));
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this4 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      var _this$state = this.state,
          loading = _this$state.loading,
          cartItems = _this$state.cartItems,
          activeKeys = _this$state.activeKeys;
      var openEnd = Boolean(this.getValueFromModel("openEnd"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "quote",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Spin"], {
        size: "large",
        spinning: loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NCollapse"], {
        activeKey: activeKeys,
        onChange: function onChange(keys) {
          var cartItemsCopy = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(cartItems);

          cartItemsCopy.map(function (every, index) {
            if ((keys || []).includes(index.toString())) {
              every.selected = "Y";
            } else {
              every.selected = "N";
            }
          });

          _this4.setState({
            activeKeys: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(keys),
            cartItems: cartItemsCopy
          }, function () {});
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }, cartItems.map(function (crossSellProduct, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
          key: index,
          showArrow: false,
          header: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 203
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_16__["ProdTypeImage"], {
            productCate: crossSellProduct.productCate,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 204
            },
            __self: this
          }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
            className: "card-title",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 205
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en(crossSellProduct.productName).thai("ประกันภัยรถยนต์ภาคบังคับ").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Switch"], {
            checked: crossSellProduct.selected === "Y",
            style: {
              float: "right"
            },
            onChange: function onChange(selected) {
              cartItems[index].selected = selected ? "Y" : "N";

              _this4.setState({
                cartItems: cartItems
              }, function () {
                _this4.setState({
                  activeKeys: _this4.setActiveKeys(_this4.state.cartItems)
                });
              });
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 210
            },
            __self: this
          }), !isMobile && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 225
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
            className: "price-card",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 226
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].renderPolicyPremium(crossSellProduct)))),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 199
          },
          __self: this
        }, crossSellProduct.productCode === "CMI" && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_17__["CrossSellCMI_1"], {
          form: form,
          model: model,
          item: crossSellProduct,
          propsNameFixed: _this4.generatePropName(index),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 233
          },
          __self: this
        })), crossSellProduct.productCode === "PA" && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_17__["CrossSellPA_1"], {
          form: form,
          model: model,
          item: crossSellProduct,
          propsNameFixed: _this4.generatePropName(index),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 239
          },
          __self: this
        })));
      }))));
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 256
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Add-ons").thai("Add-ons").getMessage()));
    }
  }]);

  return AddOns;
}(_quote_step__WEBPACK_IMPORTED_MODULE_9__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcGFyZS92bWkvYWRkLW9ucy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9jb21wYXJlL3ZtaS9hZGQtb25zLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5pbXBvcnQgUXVvdGVTdGVwLCB7IFN0ZXBDb21wb25lbnRzLCBTdGVwUHJvcHMgfSBmcm9tIFwiLi4vLi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IHsgQnV0dG9uLCBub3RpZmljYXRpb24sIFNwaW4sIFN3aXRjaCB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBwYWdlVG8gfSBmcm9tIFwiLi4vLi4vLi4vY29tcG9uZW50L3dpemFyZFwiO1xuaW1wb3J0IHsgQWxsU3RlcHMgfSBmcm9tIFwiLi4vYWxsLXN0ZXBzXCI7XG5pbXBvcnQgeyBOQ29sbGFwc2UsIE5QYW5lbCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzLCBDb25zdHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBQcm9kVHlwZUltYWdlIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9xdWVyeS9wcm9kLXR5cGVcIjtcbmltcG9ydCB7IENyb3NzU2VsbENNSV8xLCBDcm9zc1NlbGxQQV8xIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQnV0dG9uQmFjayB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYW50ZC9idXR0b24tYmFja1wiO1xuXG5leHBvcnQgY29uc3QgZm9ybURhdGVMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxufTtcbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbnR5cGUgRGV0YWlsc1N0YXRlID0ge1xuICBkaXNhYmxlZDogYm9vbGVhbjtcbiAgY2hlY2tlZFBvbGljaWVzOiBhbnk7XG4gIGRvYjogYW55LFxuICBjYXJ0SXRlbXM6IGFueTtcbiAgYWN0aXZlS2V5czogYW55O1xuICBsb2FkaW5nOiBib29sZWFuO1xufTtcblxuXG5jbGFzcyBBZGRPbnM8UCBleHRlbmRzIFN0ZXBQcm9wcywgUyBleHRlbmRzIERldGFpbHNTdGF0ZSwgQyBleHRlbmRzIFN0ZXBDb21wb25lbnRzPiBleHRlbmRzIFF1b3RlU3RlcDxQLCBTLCBDPiB7XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuXG4gICAgaWYgKCF0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiaXRlbXMuMC5leHRcIikpIHtcbiAgICAgIHRoaXMuZ2V0Q2FydE5ldygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmluaXREb2IoKTtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhY3RpdmVLZXlzOiB0aGlzLnNldEFjdGl2ZUtleXModGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcIml0ZW1zXCIpKSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGluaXREb2IoKSB7XG4gICAgbGV0IGluZDogbnVtYmVyID0gLTE7XG4gICAgKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJpdGVtc1wiKSB8fCBbXSkubWFwKChldmVyeTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBpZiAoZXZlcnkucHJvZHVjdENvZGUgPT09IFwiUEFcIikge1xuICAgICAgICBpbmQgPSBpbmRleDtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBsZXQgZG9iID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgaXRlbXMuJHtpbmR9LmV4dC5kb2JgKTtcbiAgICBkb2IgPSBtb21lbnQoZG9iLCBDb25zdHMuREFURV9GT1JNQVQuREFURV9USU1FX1dJVEhfVElNRV9aT05FKS5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUKTtcbiAgICBpZiAoZG9iICYmIGRvYi5zcGxpdChcIi9cIikubGVuZ3RoID09PSAzKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZG9iOiB7XG4gICAgICAgICAgZGF5OiBkb2Iuc3BsaXQoXCIvXCIpWzBdLFxuICAgICAgICAgIG1vbnRoOiBkb2Iuc3BsaXQoXCIvXCIpWzFdLFxuICAgICAgICAgIHllYXI6IGRvYi5zcGxpdChcIi9cIilbMl0sXG4gICAgICAgIH0sXG4gICAgICB9IGFzIGFueSk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4ge1xuICAgICAgZGlzYWJsZWQ6IGZhbHNlLFxuICAgICAgY2hlY2tlZFBvbGljaWVzOiBuZXcgTWFwKCksXG4gICAgICBhY3RpdmVLZXlzOiBbXSxcbiAgICAgIGNhcnRJdGVtczogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcIml0ZW1zXCIpIHx8IFtdLFxuICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICBkb2I6IHt9LFxuICAgIH0gYXMgUztcbiAgfVxuXG4gIGdldENhcnROZXcoKSB7XG4gICAgQWpheC5wb3N0KFwiL2NhcnQvbmV3XCIsIF8uZ2V0KHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpLCBcInBvbGljeVwiKSwgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAudGhlbigocmVzOiBhbnkpID0+IHtcbiAgICAgICAgY29uc3QgcmVzcERhdGEgPSAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhIHx8IHt9O1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBjYXJ0SXRlbXM6IHJlc3BEYXRhLml0ZW1zLFxuICAgICAgICAgIGFjdGl2ZUtleXM6IHRoaXMuc2V0QWN0aXZlS2V5cyhyZXNwRGF0YS5pdGVtcyksXG4gICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHJlc3BEYXRhKTtcbiAgICAgICAgICB0aGlzLmluaXREb2IoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaCgoKSA9PiB7XG4gICAgfSk7XG4gIH1cblxuICBzZXRBY3RpdmVLZXlzKGl0ZW1zOiBhbnkpIHtcbiAgICBsZXQgYWN0aXZlS2V5czogYW55ID0gW107XG4gICAgKGl0ZW1zIHx8IFtdKS5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgIGlmIChldmVyeS5zZWxlY3RlZCA9PT0gXCJZXCIpIHtcbiAgICAgICAgYWN0aXZlS2V5cy5wdXNoKGluZGV4KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gYWN0aXZlS2V5cztcbiAgfVxuXG4gIGhhbmRsZU5leHQgPSAoKTogdm9pZCA9PiB7XG4gICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKChlcnI6IGFueSwgZmllbGRzVmFsdWU6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb25zdCB7IGNhcnRJdGVtcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgIGNvbnN0IG5ld1BvbGljeSA9IHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpO1xuICAgICAgY2FydEl0ZW1zLm1hcCgoZXZlcnk6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICBuZXdQb2xpY3kuaXRlbXNbaW5kZXhdLnNlbGVjdGVkID0gZXZlcnkuc2VsZWN0ZWQ7XG4gICAgICB9KTtcbiAgICAgIG5ld1BvbGljeS5wb2xpY3kuZXh0Ll91aS5zdGVwID0gQWxsU3RlcHMuREVUQUlMUztcbiAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgLnBvc3QoXCIvY2FydC9tZXJnZVwiLCBuZXdQb2xpY3kpXG4gICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keSB8fCB7IHJlc3BEYXRhOiB7fSB9O1xuICAgICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwocmVzcERhdGEpO1xuICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5ERVRBSUxTKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlU2F2ZSA9ICgpOiB2b2lkID0+IHtcbiAgICBjb25zdCB7IGNhcnRJdGVtcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBuZXdQb2xpY3kgPSB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9TZXJ2aWNlTW9kZWwoKTtcbiAgICBjYXJ0SXRlbXMubWFwKChldmVyeTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBuZXdQb2xpY3kuaXRlbXNbaW5kZXhdLnNlbGVjdGVkID0gZXZlcnkuc2VsZWN0ZWQ7XG4gICAgfSk7XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5wdXQoXCIvY2FydC9tZXJnZVwiLCBuZXdQb2xpY3kpXG4gICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IHsgcmVzcERhdGE6IHt9IH07XG4gICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwocmVzcERhdGEpO1xuICAgICAgICBub3RpZmljYXRpb24uc3VjY2Vzcyh7XG4gICAgICAgICAgbWVzc2FnZTogTGFuZ3VhZ2UuZW4oXCJTYXZlZCBzdWNjZXNzZnVsbHlcIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lia4Lix4LiZ4LiX4Li24LiB4LiE4Liz4Lie4Li54LiU4Liq4Liz4LmA4Lij4LmH4LiI4LmB4Lil4LmJ4LinXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9O1xuXG4gIHByaXZhdGUgZ2VuZXJhdGVQcm9wTmFtZShpbmRleDogbnVtYmVyLCBwcm9wTmFtZT86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgaWYgKCFwcm9wTmFtZSkgcmV0dXJuIGBpdGVtcy4ke2luZGV4fS5leHRgO1xuXG4gICAgcmV0dXJuIGBpdGVtcy4ke2luZGV4fS5leHQuJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gICAgY29uc3QgeyBkaXNhYmxlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPEJ1dHRvbkJhY2sgaGFuZGVsPXsoKSA9PiB7XG4gICAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLlBMQU5TKTtcbiAgICAgICAgfX0vPlxuXG4gICAgICAgIDxCdXR0b24gc2l6ZT1cImxhcmdlXCIgdHlwZT1cInByaW1hcnlcIiBkaXNhYmxlZD17ZGlzYWJsZWR9IG9uQ2xpY2s9e3RoaXMuaGFuZGxlTmV4dH0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiTmV4dFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguJXguYjguK3guYTguJtcIikubXkoXCLhgJThgLHhgKzhgIDhgLrhgJDhgIXhgLrhgIHhgK9cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBsb2FkaW5nLCBjYXJ0SXRlbXMsIGFjdGl2ZUtleXMgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3Qgb3BlbkVuZCA9IEJvb2xlYW4odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcIm9wZW5FbmRcIikpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInF1b3RlXCI+XG4gICAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXtsb2FkaW5nfT5cbiAgICAgICAgICA8TkNvbGxhcHNlXG4gICAgICAgICAgICBhY3RpdmVLZXk9e2FjdGl2ZUtleXN9XG4gICAgICAgICAgICBvbkNoYW5nZT17KGtleXM6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBsZXQgY2FydEl0ZW1zQ29weTogYW55W10gPSBfLmNsb25lRGVlcChjYXJ0SXRlbXMpO1xuICAgICAgICAgICAgICBjYXJ0SXRlbXNDb3B5Lm1hcCgoZXZlcnk6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgIGlmICgoa2V5cyB8fCBbXSkuaW5jbHVkZXMoaW5kZXgudG9TdHJpbmcoKSkpIHtcbiAgICAgICAgICAgICAgICAgIGV2ZXJ5LnNlbGVjdGVkID0gXCJZXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGV2ZXJ5LnNlbGVjdGVkID0gXCJOXCI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgYWN0aXZlS2V5czogWy4uLmtleXNdLFxuICAgICAgICAgICAgICAgIGNhcnRJdGVtczogY2FydEl0ZW1zQ29weSxcbiAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAge2NhcnRJdGVtcy5tYXAoKGNyb3NzU2VsbFByb2R1Y3Q6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDxOUGFuZWxcbiAgICAgICAgICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgICAgICAgICBzaG93QXJyb3c9e2ZhbHNlfVxuICAgICAgICAgICAgICAgICAgaGVhZGVyPXtcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8UHJvZFR5cGVJbWFnZSBwcm9kdWN0Q2F0ZT17Y3Jvc3NTZWxsUHJvZHVjdC5wcm9kdWN0Q2F0ZX0vPlxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihjcm9zc1NlbGxQcm9kdWN0LnByb2R1Y3ROYW1lKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC4geC4seC4meC4oOC4seC4ouC4o+C4luC4ouC4meC4leC5jOC4oOC4suC4hOC4muC4seC4h+C4hOC4seC4mlwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICA8U3dpdGNoXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtjcm9zc1NlbGxQcm9kdWN0LnNlbGVjdGVkID09PSBcIllcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZsb2F0OiBcInJpZ2h0XCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtzZWxlY3RlZCA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNhcnRJdGVtc1tpbmRleF0uc2VsZWN0ZWQgPSBzZWxlY3RlZCA/IFwiWVwiIDogXCJOXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhcnRJdGVtcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aXZlS2V5czogdGhpcy5zZXRBY3RpdmVLZXlzKHRoaXMuc3RhdGUuY2FydEl0ZW1zKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgeyFpc01vYmlsZSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwcmljZS1jYXJkXCI+e1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0oY3Jvc3NTZWxsUHJvZHVjdCl9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge2Nyb3NzU2VsbFByb2R1Y3QucHJvZHVjdENvZGUgPT09IFwiQ01JXCIgJiYgPD5cbiAgICAgICAgICAgICAgICAgICAgPENyb3NzU2VsbENNSV8xIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtPXtjcm9zc1NlbGxQcm9kdWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcHNOYW1lRml4ZWQ9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShpbmRleCl9Lz5cbiAgICAgICAgICAgICAgICAgIDwvPn1cbiAgICAgICAgICAgICAgICAgIHtjcm9zc1NlbGxQcm9kdWN0LnByb2R1Y3RDb2RlID09PSBcIlBBXCIgJiYgPD5cbiAgICAgICAgICAgICAgICAgICAgPENyb3NzU2VsbFBBXzEgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtPXtjcm9zc1NlbGxQcm9kdWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wc05hbWVGaXhlZD17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKGluZGV4KX0vPlxuICAgICAgICAgICAgICAgICAgPC8+fVxuICAgICAgICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSl9XG4gICAgICAgICAgPC9OQ29sbGFwc2U+XG4gICAgICAgIDwvU3Bpbj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyVGl0bGUoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJBZGQtb25zXCIpXG4gICAgICAgICAgICAudGhhaShcIkFkZC1vbnNcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBBZGRPbnMgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFVQTtBQUNBO0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBQ0E7Ozs7OztBQTlHQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBT0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQTRDQTtBQUNBO0FBRUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQWhCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEzQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa0NBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUtBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7OztBQWxPQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/compare/vmi/add-ons.tsx
