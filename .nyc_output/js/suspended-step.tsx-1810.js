__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuspendedStep", function() { return SuspendedStep; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./quote-step */ "./src/app/desk/quote/quote-step.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/suspended-step.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        height: 66vh;\n       .congratulation {\n            padding: 44px 0 54px 0;\n            text-align: center;\n            color: #5fbfec;\n            i {\n                font-size: 100px;\n            }\n        }\n        .paragraph {\n            font-size: 16px;\n            margin: 0 0 15px 0;\n            text-align: center;\n            color: rgba(0, 0, 0, 0.85);\n            .policy-no-link {\n                display: inline-block;\n                margin: 0 0 0 3px;\n            }\n        }\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var SuspendedStep =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(SuspendedStep, _QuoteStep);

  function SuspendedStep() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, SuspendedStep);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(SuspendedStep).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(SuspendedStep, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(SuspendedStep.prototype), "initComponents", this).call(this), {
        Quote: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      });
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      return null;
    }
  }, {
    key: "renderActions",
    value: function renderActions() {
      return null;
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var C = this.getComponents();
      var quoteNo = this.getValueFromModel("quoteNo");
      var message = this.getValueFromModel("message");

      var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_10__["default"].getTheme(),
          COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Quote, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "resultPage",
        style: {
          textAlign: "center"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "congratulation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        type: "check-circle",
        style: {
          color: COLOR_PRIMARY
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }, message && message !== "" ? message : _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("Thank you for insuring with us. The case has been sent to insurer for underwriting. Reference Number:").thai("ขอบคุณที่ทำประกันกับเรา กรณีถูกส่งไปยัง บริษัท ประกันสำหรับการจัดจำหน่าย หมายเลขอ้างอิง: ").my("ကြှနျုပျတို့နှငျ့အတူ insuring အတွက်ကျေးဇူးတင်ပါသည်။ အဆိုပါကိစ္စတွင် underwriting များအတွက်အာမခံစေခြင်းငှါစေလွှတ်ခဲ့သည်။ ကိုးကားစရာအရေအတွက်:").getMessage(), !message && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
        className: "policy-no-link",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }, " ", quoteNo))));
    }
  }]);

  return SuspendedStep;
}(_quote_step__WEBPACK_IMPORTED_MODULE_11__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvc3VzcGVuZGVkLXN0ZXAudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvc3VzcGVuZGVkLXN0ZXAudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEljb24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5pbXBvcnQgeyBTdGVwQ29tcG9uZW50cywgU3RlcFByb3BzIH0gZnJvbSBcIi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IFF1b3RlU3RlcCBmcm9tIFwiLi9xdW90ZS1zdGVwXCI7XG5cbmNsYXNzIFN1c3BlbmRlZFN0ZXA8UCBleHRlbmRzIFN0ZXBQcm9wcywgUywgQyBleHRlbmRzIFN0ZXBDb21wb25lbnRzPiBleHRlbmRzIFF1b3RlU3RlcDxQLCBTLCBDPiB7XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCksIHtcbiAgICAgIFF1b3RlOiBTdHlsZWQuZGl2YFxuICAgICAgICBoZWlnaHQ6IDY2dmg7XG4gICAgICAgLmNvbmdyYXR1bGF0aW9uIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDQ0cHggMCA1NHB4IDA7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBjb2xvcjogIzVmYmZlYztcbiAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTAwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLnBhcmFncmFwaCB7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBtYXJnaW46IDAgMCAxNXB4IDA7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg1KTtcbiAgICAgICAgICAgIC5wb2xpY3ktbm8tbGluayB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgICAgICAgIG1hcmdpbjogMCAwIDAgM3B4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgICAgICBgLFxuICAgIH0pIGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyVGl0bGUoKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQWN0aW9ucygpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBsZXQgcXVvdGVObyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJxdW90ZU5vXCIpO1xuICAgIGNvbnN0IG1lc3NhZ2UgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwibWVzc2FnZVwiKTtcbiAgICBjb25zdCB7IENPTE9SX1BSSU1BUlkgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLlF1b3RlPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJlc3VsdFBhZ2VcIiBzdHlsZT17eyB0ZXh0QWxpZ246IFwiY2VudGVyXCIgfX0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb25ncmF0dWxhdGlvblwiPlxuICAgICAgICAgICAgPEljb24gdHlwZT1cImNoZWNrLWNpcmNsZVwiIHN0eWxlPXt7IGNvbG9yOiBDT0xPUl9QUklNQVJZIH19Lz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaCBwYXJhZ3JhcGgtLW1haW5cIj5cbiAgICAgICAgICAgIHttZXNzYWdlICYmIG1lc3NhZ2UgIT09IFwiXCJcbiAgICAgICAgICAgICAgPyBtZXNzYWdlXG4gICAgICAgICAgICAgIDogTGFuZ3VhZ2VcbiAgICAgICAgICAgICAgICAuZW4oYFRoYW5rIHlvdSBmb3IgaW5zdXJpbmcgd2l0aCB1cy4gVGhlIGNhc2UgaGFzIGJlZW4gc2VudCB0byBpbnN1cmVyIGZvciB1bmRlcndyaXRpbmcuIFJlZmVyZW5jZSBOdW1iZXI6YClcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4guC4reC4muC4hOC4uOC4k+C4l+C4teC5iOC4l+C4s+C4m+C4o+C4sOC4geC4seC4meC4geC4seC4muC5gOC4o+C4siDguIHguKPguJPguLXguJbguLnguIHguKrguYjguIfguYTguJvguKLguLHguIcg4Lia4Lij4Li04Lip4Lix4LiXIOC4m+C4o+C4sOC4geC4seC4meC4quC4s+C4q+C4o+C4seC4muC4geC4suC4o+C4iOC4seC4lOC4iOC4s+C4q+C4meC5iOC4suC4oiDguKvguKHguLLguKLguYDguKXguILguK3guYnguLLguIfguK3guLTguIc6IFwiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAgOGAvOGAvuGAlOGAu+GAr+GAleGAu+GAkOGAreGAr+GAt+GAlOGAvuGAhOGAu+GAt+GAoeGAkOGAsCBpbnN1cmluZyDhgKHhgJDhgL3hgIDhgLrhgIDhgLvhgLHhgLjhgIfhgLDhgLjhgJDhgIThgLrhgJXhgKvhgJ7hgIrhgLrhgYsg4YCh4YCG4YCt4YCv4YCV4YCr4YCA4YCt4YCF4YC54YCF4YCQ4YC94YCE4YC6IHVuZGVyd3JpdGluZyDhgJnhgLvhgKzhgLjhgKHhgJDhgL3hgIDhgLrhgKHhgKzhgJnhgIHhgLbhgIXhgLHhgIHhgLzhgIThgLrhgLjhgIThgL7hgKvhgIXhgLHhgJzhgL3hgL7hgJDhgLrhgIHhgLLhgLfhgJ7hgIrhgLrhgYsg4YCA4YCt4YCv4YC44YCA4YCs4YC44YCF4YCb4YCs4YCh4YCb4YCx4YCh4YCQ4YC94YCA4YC6OlwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICB7IW1lc3NhZ2UgJiYgKDxhIGNsYXNzTmFtZT1cInBvbGljeS1uby1saW5rXCI+IHtxdW90ZU5vfTwvYT4pfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvQy5RdW90ZT5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCB7IFN1c3BlbmRlZFN0ZXAgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQXVCQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7OztBQTVEQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/suspended-step.tsx
