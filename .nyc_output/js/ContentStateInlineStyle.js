/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule ContentStateInlineStyle
 * @format
 * 
 */


var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var _require = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js"),
    Map = _require.Map;

var ContentStateInlineStyle = {
  add: function add(contentState, selectionState, inlineStyle) {
    return modifyInlineStyle(contentState, selectionState, inlineStyle, true);
  },
  remove: function remove(contentState, selectionState, inlineStyle) {
    return modifyInlineStyle(contentState, selectionState, inlineStyle, false);
  }
};

function modifyInlineStyle(contentState, selectionState, inlineStyle, addOrRemove) {
  var blockMap = contentState.getBlockMap();
  var startKey = selectionState.getStartKey();
  var startOffset = selectionState.getStartOffset();
  var endKey = selectionState.getEndKey();
  var endOffset = selectionState.getEndOffset();
  var newBlocks = blockMap.skipUntil(function (_, k) {
    return k === startKey;
  }).takeUntil(function (_, k) {
    return k === endKey;
  }).concat(Map([[endKey, blockMap.get(endKey)]])).map(function (block, blockKey) {
    var sliceStart;
    var sliceEnd;

    if (startKey === endKey) {
      sliceStart = startOffset;
      sliceEnd = endOffset;
    } else {
      sliceStart = blockKey === startKey ? startOffset : 0;
      sliceEnd = blockKey === endKey ? endOffset : block.getLength();
    }

    var chars = block.getCharacterList();
    var current;

    while (sliceStart < sliceEnd) {
      current = chars.get(sliceStart);
      chars = chars.set(sliceStart, addOrRemove ? CharacterMetadata.applyStyle(current, inlineStyle) : CharacterMetadata.removeStyle(current, inlineStyle));
      sliceStart++;
    }

    return block.set('characterList', chars);
  });
  return contentState.merge({
    blockMap: blockMap.merge(newBlocks),
    selectionBefore: selectionState,
    selectionAfter: selectionState
  });
}

module.exports = ContentStateInlineStyle;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbnRlbnRTdGF0ZUlubGluZVN0eWxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbnRlbnRTdGF0ZUlubGluZVN0eWxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgQ29udGVudFN0YXRlSW5saW5lU3R5bGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xuXG52YXIgX3JlcXVpcmUgPSByZXF1aXJlKCdpbW11dGFibGUnKSxcbiAgICBNYXAgPSBfcmVxdWlyZS5NYXA7XG5cbnZhciBDb250ZW50U3RhdGVJbmxpbmVTdHlsZSA9IHtcbiAgYWRkOiBmdW5jdGlvbiBhZGQoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgaW5saW5lU3R5bGUpIHtcbiAgICByZXR1cm4gbW9kaWZ5SW5saW5lU3R5bGUoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSwgaW5saW5lU3R5bGUsIHRydWUpO1xuICB9LFxuXG4gIHJlbW92ZTogZnVuY3Rpb24gcmVtb3ZlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGlubGluZVN0eWxlKSB7XG4gICAgcmV0dXJuIG1vZGlmeUlubGluZVN0eWxlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGlubGluZVN0eWxlLCBmYWxzZSk7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIG1vZGlmeUlubGluZVN0eWxlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGlubGluZVN0eWxlLCBhZGRPclJlbW92ZSkge1xuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIHN0YXJ0T2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIGVuZEtleSA9IHNlbGVjdGlvblN0YXRlLmdldEVuZEtleSgpO1xuICB2YXIgZW5kT2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kT2Zmc2V0KCk7XG5cbiAgdmFyIG5ld0Jsb2NrcyA9IGJsb2NrTWFwLnNraXBVbnRpbChmdW5jdGlvbiAoXywgaykge1xuICAgIHJldHVybiBrID09PSBzdGFydEtleTtcbiAgfSkudGFrZVVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IGVuZEtleTtcbiAgfSkuY29uY2F0KE1hcChbW2VuZEtleSwgYmxvY2tNYXAuZ2V0KGVuZEtleSldXSkpLm1hcChmdW5jdGlvbiAoYmxvY2ssIGJsb2NrS2V5KSB7XG4gICAgdmFyIHNsaWNlU3RhcnQ7XG4gICAgdmFyIHNsaWNlRW5kO1xuXG4gICAgaWYgKHN0YXJ0S2V5ID09PSBlbmRLZXkpIHtcbiAgICAgIHNsaWNlU3RhcnQgPSBzdGFydE9mZnNldDtcbiAgICAgIHNsaWNlRW5kID0gZW5kT2Zmc2V0O1xuICAgIH0gZWxzZSB7XG4gICAgICBzbGljZVN0YXJ0ID0gYmxvY2tLZXkgPT09IHN0YXJ0S2V5ID8gc3RhcnRPZmZzZXQgOiAwO1xuICAgICAgc2xpY2VFbmQgPSBibG9ja0tleSA9PT0gZW5kS2V5ID8gZW5kT2Zmc2V0IDogYmxvY2suZ2V0TGVuZ3RoKCk7XG4gICAgfVxuXG4gICAgdmFyIGNoYXJzID0gYmxvY2suZ2V0Q2hhcmFjdGVyTGlzdCgpO1xuICAgIHZhciBjdXJyZW50O1xuICAgIHdoaWxlIChzbGljZVN0YXJ0IDwgc2xpY2VFbmQpIHtcbiAgICAgIGN1cnJlbnQgPSBjaGFycy5nZXQoc2xpY2VTdGFydCk7XG4gICAgICBjaGFycyA9IGNoYXJzLnNldChzbGljZVN0YXJ0LCBhZGRPclJlbW92ZSA/IENoYXJhY3Rlck1ldGFkYXRhLmFwcGx5U3R5bGUoY3VycmVudCwgaW5saW5lU3R5bGUpIDogQ2hhcmFjdGVyTWV0YWRhdGEucmVtb3ZlU3R5bGUoY3VycmVudCwgaW5saW5lU3R5bGUpKTtcbiAgICAgIHNsaWNlU3RhcnQrKztcbiAgICB9XG5cbiAgICByZXR1cm4gYmxvY2suc2V0KCdjaGFyYWN0ZXJMaXN0JywgY2hhcnMpO1xuICB9KTtcblxuICByZXR1cm4gY29udGVudFN0YXRlLm1lcmdlKHtcbiAgICBibG9ja01hcDogYmxvY2tNYXAubWVyZ2UobmV3QmxvY2tzKSxcbiAgICBzZWxlY3Rpb25CZWZvcmU6IHNlbGVjdGlvblN0YXRlLFxuICAgIHNlbGVjdGlvbkFmdGVyOiBzZWxlY3Rpb25TdGF0ZVxuICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBDb250ZW50U3RhdGVJbmxpbmVTdHlsZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/ContentStateInlineStyle.js
