

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _deepMerge = __webpack_require__(/*! ../utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/react-images/lib/components/Icon.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

function Header(_ref, _ref2) {
  var theme = _ref2.theme;

  var customControls = _ref.customControls,
      onClose = _ref.onClose,
      showCloseButton = _ref.showCloseButton,
      closeButtonTitle = _ref.closeButtonTitle,
      props = _objectWithoutProperties(_ref, ['customControls', 'onClose', 'showCloseButton', 'closeButtonTitle']);

  var classes = _noImportant.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, theme));

  return _react2.default.createElement('div', _extends({
    className: (0, _noImportant.css)(classes.header)
  }, props), customControls ? customControls : _react2.default.createElement('span', null), !!showCloseButton && _react2.default.createElement('button', {
    title: closeButtonTitle,
    className: (0, _noImportant.css)(classes.close),
    onClick: onClose
  }, _react2.default.createElement(_Icon2.default, {
    fill: !!theme.close && theme.close.fill || _theme2.default.close.fill,
    type: 'close'
  })));
}

Header.propTypes = {
  customControls: _propTypes2.default.array,
  onClose: _propTypes2.default.func.isRequired,
  showCloseButton: _propTypes2.default.bool
};
Header.contextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    height: _theme2.default.header.height
  },
  close: {
    background: 'none',
    border: 'none',
    cursor: 'pointer',
    outline: 'none',
    position: 'relative',
    top: 0,
    verticalAlign: 'bottom',
    zIndex: 1,
    // increase hit area
    height: 40,
    marginRight: -10,
    padding: 10,
    width: 40
  }
};
exports.default = Header;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0hlYWRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvY29tcG9uZW50cy9IZWFkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX25vSW1wb3J0YW50ID0gcmVxdWlyZSgnYXBocm9kaXRlL25vLWltcG9ydGFudCcpO1xuXG52YXIgX3RoZW1lID0gcmVxdWlyZSgnLi4vdGhlbWUnKTtcblxudmFyIF90aGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aGVtZSk7XG5cbnZhciBfZGVlcE1lcmdlID0gcmVxdWlyZSgnLi4vdXRpbHMvZGVlcE1lcmdlJyk7XG5cbnZhciBfZGVlcE1lcmdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZXBNZXJnZSk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4vSWNvbicpO1xuXG52YXIgX0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gSGVhZGVyKF9yZWYsIF9yZWYyKSB7XG5cdHZhciB0aGVtZSA9IF9yZWYyLnRoZW1lO1xuXG5cdHZhciBjdXN0b21Db250cm9scyA9IF9yZWYuY3VzdG9tQ29udHJvbHMsXG5cdCAgICBvbkNsb3NlID0gX3JlZi5vbkNsb3NlLFxuXHQgICAgc2hvd0Nsb3NlQnV0dG9uID0gX3JlZi5zaG93Q2xvc2VCdXR0b24sXG5cdCAgICBjbG9zZUJ1dHRvblRpdGxlID0gX3JlZi5jbG9zZUJ1dHRvblRpdGxlLFxuXHQgICAgcHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3JlZiwgWydjdXN0b21Db250cm9scycsICdvbkNsb3NlJywgJ3Nob3dDbG9zZUJ1dHRvbicsICdjbG9zZUJ1dHRvblRpdGxlJ10pO1xuXG5cdHZhciBjbGFzc2VzID0gX25vSW1wb3J0YW50LlN0eWxlU2hlZXQuY3JlYXRlKCgwLCBfZGVlcE1lcmdlMi5kZWZhdWx0KShkZWZhdWx0U3R5bGVzLCB0aGVtZSkpO1xuXG5cdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcblx0XHQnZGl2Jyxcblx0XHRfZXh0ZW5kcyh7IGNsYXNzTmFtZTogKDAsIF9ub0ltcG9ydGFudC5jc3MpKGNsYXNzZXMuaGVhZGVyKSB9LCBwcm9wcyksXG5cdFx0Y3VzdG9tQ29udHJvbHMgPyBjdXN0b21Db250cm9scyA6IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdzcGFuJywgbnVsbCksXG5cdFx0ISFzaG93Q2xvc2VCdXR0b24gJiYgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0XHQnYnV0dG9uJyxcblx0XHRcdHtcblx0XHRcdFx0dGl0bGU6IGNsb3NlQnV0dG9uVGl0bGUsXG5cdFx0XHRcdGNsYXNzTmFtZTogKDAsIF9ub0ltcG9ydGFudC5jc3MpKGNsYXNzZXMuY2xvc2UpLFxuXHRcdFx0XHRvbkNsaWNrOiBvbkNsb3NlXG5cdFx0XHR9LFxuXHRcdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0ljb24yLmRlZmF1bHQsIHsgZmlsbDogISF0aGVtZS5jbG9zZSAmJiB0aGVtZS5jbG9zZS5maWxsIHx8IF90aGVtZTIuZGVmYXVsdC5jbG9zZS5maWxsLCB0eXBlOiAnY2xvc2UnIH0pXG5cdFx0KVxuXHQpO1xufVxuXG5IZWFkZXIucHJvcFR5cGVzID0ge1xuXHRjdXN0b21Db250cm9sczogX3Byb3BUeXBlczIuZGVmYXVsdC5hcnJheSxcblx0b25DbG9zZTogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG5cdHNob3dDbG9zZUJ1dHRvbjogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sXG59O1xuSGVhZGVyLmNvbnRleHRUeXBlcyA9IHtcblx0dGhlbWU6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0LmlzUmVxdWlyZWRcbn07XG5cbnZhciBkZWZhdWx0U3R5bGVzID0ge1xuXHRoZWFkZXI6IHtcblx0XHRkaXNwbGF5OiAnZmxleCcsXG5cdFx0anVzdGlmeUNvbnRlbnQ6ICdzcGFjZS1iZXR3ZWVuJyxcblx0XHRoZWlnaHQ6IF90aGVtZTIuZGVmYXVsdC5oZWFkZXIuaGVpZ2h0XG5cdH0sXG5cdGNsb3NlOiB7XG5cdFx0YmFja2dyb3VuZDogJ25vbmUnLFxuXHRcdGJvcmRlcjogJ25vbmUnLFxuXHRcdGN1cnNvcjogJ3BvaW50ZXInLFxuXHRcdG91dGxpbmU6ICdub25lJyxcblx0XHRwb3NpdGlvbjogJ3JlbGF0aXZlJyxcblx0XHR0b3A6IDAsXG5cdFx0dmVydGljYWxBbGlnbjogJ2JvdHRvbScsXG5cdFx0ekluZGV4OiAxLFxuXG5cdFx0Ly8gaW5jcmVhc2UgaGl0IGFyZWFcblx0XHRoZWlnaHQ6IDQwLFxuXHRcdG1hcmdpblJpZ2h0OiAtMTAsXG5cdFx0cGFkZGluZzogMTAsXG5cdFx0d2lkdGg6IDQwXG5cdH1cbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEhlYWRlcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWRBO0FBTkE7QUF3QkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Header.js
