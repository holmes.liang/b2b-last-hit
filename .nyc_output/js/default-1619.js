/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var PI = Math.PI;
/**
 * @param {module:echarts/ExtensionAPI} api
 * @param {Object} [opts]
 * @param {string} [opts.text]
 * @param {string} [opts.color]
 * @param {string} [opts.textColor]
 * @return {module:zrender/Element}
 */

function _default(api, opts) {
  opts = opts || {};
  zrUtil.defaults(opts, {
    text: 'loading',
    color: '#c23531',
    textColor: '#000',
    maskColor: 'rgba(255, 255, 255, 0.8)',
    zlevel: 0
  });
  var mask = new graphic.Rect({
    style: {
      fill: opts.maskColor
    },
    zlevel: opts.zlevel,
    z: 10000
  });
  var arc = new graphic.Arc({
    shape: {
      startAngle: -PI / 2,
      endAngle: -PI / 2 + 0.1,
      r: 10
    },
    style: {
      stroke: opts.color,
      lineCap: 'round',
      lineWidth: 5
    },
    zlevel: opts.zlevel,
    z: 10001
  });
  var labelRect = new graphic.Rect({
    style: {
      fill: 'none',
      text: opts.text,
      textPosition: 'right',
      textDistance: 10,
      textFill: opts.textColor
    },
    zlevel: opts.zlevel,
    z: 10001
  });
  arc.animateShape(true).when(1000, {
    endAngle: PI * 3 / 2
  }).start('circularInOut');
  arc.animateShape(true).when(1000, {
    startAngle: PI * 3 / 2
  }).delay(300).start('circularInOut');
  var group = new graphic.Group();
  group.add(arc);
  group.add(labelRect);
  group.add(mask); // Inject resize

  group.resize = function () {
    var cx = api.getWidth() / 2;
    var cy = api.getHeight() / 2;
    arc.setShape({
      cx: cx,
      cy: cy
    });
    var r = arc.shape.r;
    labelRect.setShape({
      x: cx - r,
      y: cy - r,
      width: r * 2,
      height: r * 2
    });
    mask.setShape({
      x: 0,
      y: 0,
      width: api.getWidth(),
      height: api.getHeight()
    });
  };

  group.resize();
  return group;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbG9hZGluZy9kZWZhdWx0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbG9hZGluZy9kZWZhdWx0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGdyYXBoaWMgPSByZXF1aXJlKFwiLi4vdXRpbC9ncmFwaGljXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgUEkgPSBNYXRoLlBJO1xuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdHNdXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdHMudGV4dF1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5jb2xvcl1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy50ZXh0Q29sb3JdXG4gKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9FbGVtZW50fVxuICovXG5cbmZ1bmN0aW9uIF9kZWZhdWx0KGFwaSwgb3B0cykge1xuICBvcHRzID0gb3B0cyB8fCB7fTtcbiAgenJVdGlsLmRlZmF1bHRzKG9wdHMsIHtcbiAgICB0ZXh0OiAnbG9hZGluZycsXG4gICAgY29sb3I6ICcjYzIzNTMxJyxcbiAgICB0ZXh0Q29sb3I6ICcjMDAwJyxcbiAgICBtYXNrQ29sb3I6ICdyZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOCknLFxuICAgIHpsZXZlbDogMFxuICB9KTtcbiAgdmFyIG1hc2sgPSBuZXcgZ3JhcGhpYy5SZWN0KHtcbiAgICBzdHlsZToge1xuICAgICAgZmlsbDogb3B0cy5tYXNrQ29sb3JcbiAgICB9LFxuICAgIHpsZXZlbDogb3B0cy56bGV2ZWwsXG4gICAgejogMTAwMDBcbiAgfSk7XG4gIHZhciBhcmMgPSBuZXcgZ3JhcGhpYy5BcmMoe1xuICAgIHNoYXBlOiB7XG4gICAgICBzdGFydEFuZ2xlOiAtUEkgLyAyLFxuICAgICAgZW5kQW5nbGU6IC1QSSAvIDIgKyAwLjEsXG4gICAgICByOiAxMFxuICAgIH0sXG4gICAgc3R5bGU6IHtcbiAgICAgIHN0cm9rZTogb3B0cy5jb2xvcixcbiAgICAgIGxpbmVDYXA6ICdyb3VuZCcsXG4gICAgICBsaW5lV2lkdGg6IDVcbiAgICB9LFxuICAgIHpsZXZlbDogb3B0cy56bGV2ZWwsXG4gICAgejogMTAwMDFcbiAgfSk7XG4gIHZhciBsYWJlbFJlY3QgPSBuZXcgZ3JhcGhpYy5SZWN0KHtcbiAgICBzdHlsZToge1xuICAgICAgZmlsbDogJ25vbmUnLFxuICAgICAgdGV4dDogb3B0cy50ZXh0LFxuICAgICAgdGV4dFBvc2l0aW9uOiAncmlnaHQnLFxuICAgICAgdGV4dERpc3RhbmNlOiAxMCxcbiAgICAgIHRleHRGaWxsOiBvcHRzLnRleHRDb2xvclxuICAgIH0sXG4gICAgemxldmVsOiBvcHRzLnpsZXZlbCxcbiAgICB6OiAxMDAwMVxuICB9KTtcbiAgYXJjLmFuaW1hdGVTaGFwZSh0cnVlKS53aGVuKDEwMDAsIHtcbiAgICBlbmRBbmdsZTogUEkgKiAzIC8gMlxuICB9KS5zdGFydCgnY2lyY3VsYXJJbk91dCcpO1xuICBhcmMuYW5pbWF0ZVNoYXBlKHRydWUpLndoZW4oMTAwMCwge1xuICAgIHN0YXJ0QW5nbGU6IFBJICogMyAvIDJcbiAgfSkuZGVsYXkoMzAwKS5zdGFydCgnY2lyY3VsYXJJbk91dCcpO1xuICB2YXIgZ3JvdXAgPSBuZXcgZ3JhcGhpYy5Hcm91cCgpO1xuICBncm91cC5hZGQoYXJjKTtcbiAgZ3JvdXAuYWRkKGxhYmVsUmVjdCk7XG4gIGdyb3VwLmFkZChtYXNrKTsgLy8gSW5qZWN0IHJlc2l6ZVxuXG4gIGdyb3VwLnJlc2l6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgY3ggPSBhcGkuZ2V0V2lkdGgoKSAvIDI7XG4gICAgdmFyIGN5ID0gYXBpLmdldEhlaWdodCgpIC8gMjtcbiAgICBhcmMuc2V0U2hhcGUoe1xuICAgICAgY3g6IGN4LFxuICAgICAgY3k6IGN5XG4gICAgfSk7XG4gICAgdmFyIHIgPSBhcmMuc2hhcGUucjtcbiAgICBsYWJlbFJlY3Quc2V0U2hhcGUoe1xuICAgICAgeDogY3ggLSByLFxuICAgICAgeTogY3kgLSByLFxuICAgICAgd2lkdGg6IHIgKiAyLFxuICAgICAgaGVpZ2h0OiByICogMlxuICAgIH0pO1xuICAgIG1hc2suc2V0U2hhcGUoe1xuICAgICAgeDogMCxcbiAgICAgIHk6IDAsXG4gICAgICB3aWR0aDogYXBpLmdldFdpZHRoKCksXG4gICAgICBoZWlnaHQ6IGFwaS5nZXRIZWlnaHQoKVxuICAgIH0pO1xuICB9O1xuXG4gIGdyb3VwLnJlc2l6ZSgpO1xuICByZXR1cm4gZ3JvdXA7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBWkE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/loading/default.js
