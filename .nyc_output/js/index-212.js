__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Slider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_slider_es_Slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-slider/es/Slider */ "./node_modules/rc-slider/es/Slider.js");
/* harmony import */ var rc_slider_es_Range__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-slider/es/Range */ "./node_modules/rc-slider/es/Range.js");
/* harmony import */ var rc_slider_es_Handle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-slider/es/Handle */ "./node_modules/rc-slider/es/Handle.js");
/* harmony import */ var _tooltip__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tooltip */ "./node_modules/antd/es/tooltip/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};








var Slider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Slider, _React$Component);

  function Slider(props) {
    var _this;

    _classCallCheck(this, Slider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Slider).call(this, props));

    _this.toggleTooltipVisible = function (index, visible) {
      _this.setState(function (_ref) {
        var visibles = _ref.visibles;
        return {
          visibles: _extends(_extends({}, visibles), _defineProperty({}, index, visible))
        };
      });
    };

    _this.handleWithTooltip = function (_a) {
      var tooltipPrefixCls = _a.tooltipPrefixCls,
          prefixCls = _a.prefixCls,
          _b = _a.info,
          value = _b.value,
          dragging = _b.dragging,
          index = _b.index,
          restProps = __rest(_b, ["value", "dragging", "index"]);

      var _this$props = _this.props,
          tipFormatter = _this$props.tipFormatter,
          tooltipVisible = _this$props.tooltipVisible,
          tooltipPlacement = _this$props.tooltipPlacement,
          getTooltipPopupContainer = _this$props.getTooltipPopupContainer;
      var visibles = _this.state.visibles;
      var isTipFormatter = tipFormatter ? visibles[index] || dragging : false;
      var visible = tooltipVisible || tooltipVisible === undefined && isTipFormatter;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_tooltip__WEBPACK_IMPORTED_MODULE_4__["default"], {
        prefixCls: tooltipPrefixCls,
        title: tipFormatter ? tipFormatter(value) : '',
        visible: visible,
        placement: tooltipPlacement || 'top',
        transitionName: "zoom-down",
        key: index,
        overlayClassName: "".concat(prefixCls, "-tooltip"),
        getPopupContainer: getTooltipPopupContainer || function () {
          return document.body;
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_slider_es_Handle__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, restProps, {
        value: value,
        onMouseEnter: function onMouseEnter() {
          return _this.toggleTooltipVisible(index, true);
        },
        onMouseLeave: function onMouseLeave() {
          return _this.toggleTooltipVisible(index, false);
        }
      })));
    };

    _this.saveSlider = function (node) {
      _this.rcSlider = node;
    };

    _this.renderSlider = function (_ref2) {
      var getPrefixCls = _ref2.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          customizeTooltipPrefixCls = _a.tooltipPrefixCls,
          range = _a.range,
          restProps = __rest(_a, ["prefixCls", "tooltipPrefixCls", "range"]);

      var prefixCls = getPrefixCls('slider', customizePrefixCls);
      var tooltipPrefixCls = getPrefixCls('tooltip', customizeTooltipPrefixCls);

      if (range) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_slider_es_Range__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({}, restProps, {
          ref: _this.saveSlider,
          handle: function handle(info) {
            return _this.handleWithTooltip({
              tooltipPrefixCls: tooltipPrefixCls,
              prefixCls: prefixCls,
              info: info
            });
          },
          prefixCls: prefixCls,
          tooltipPrefixCls: tooltipPrefixCls
        }));
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_slider_es_Slider__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, restProps, {
        ref: _this.saveSlider,
        handle: function handle(info) {
          return _this.handleWithTooltip({
            tooltipPrefixCls: tooltipPrefixCls,
            prefixCls: prefixCls,
            info: info
          });
        },
        prefixCls: prefixCls,
        tooltipPrefixCls: tooltipPrefixCls
      }));
    };

    _this.state = {
      visibles: {}
    };
    return _this;
  }

  _createClass(Slider, [{
    key: "focus",
    value: function focus() {
      this.rcSlider.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.rcSlider.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderSlider);
    }
  }]);

  return Slider;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Slider.defaultProps = {
  tipFormatter: function tipFormatter(value) {
    return value.toString();
  }
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zbGlkZXIvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NsaWRlci9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJjU2xpZGVyIGZyb20gJ3JjLXNsaWRlci9saWIvU2xpZGVyJztcbmltcG9ydCBSY1JhbmdlIGZyb20gJ3JjLXNsaWRlci9saWIvUmFuZ2UnO1xuaW1wb3J0IFJjSGFuZGxlIGZyb20gJ3JjLXNsaWRlci9saWIvSGFuZGxlJztcbmltcG9ydCBUb29sdGlwIGZyb20gJy4uL3Rvb2x0aXAnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2xpZGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMudG9nZ2xlVG9vbHRpcFZpc2libGUgPSAoaW5kZXgsIHZpc2libGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoKHsgdmlzaWJsZXMgfSkgPT4gKHtcbiAgICAgICAgICAgICAgICB2aXNpYmxlczogT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCB2aXNpYmxlcyksIHsgW2luZGV4XTogdmlzaWJsZSB9KSxcbiAgICAgICAgICAgIH0pKTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVXaXRoVG9vbHRpcCA9IChfYSkgPT4ge1xuICAgICAgICAgICAgdmFyIHsgdG9vbHRpcFByZWZpeENscywgcHJlZml4Q2xzIH0gPSBfYSwgX2IgPSBfYS5pbmZvLCB7IHZhbHVlLCBkcmFnZ2luZywgaW5kZXggfSA9IF9iLCByZXN0UHJvcHMgPSBfX3Jlc3QoX2IsIFtcInZhbHVlXCIsIFwiZHJhZ2dpbmdcIiwgXCJpbmRleFwiXSk7XG4gICAgICAgICAgICBjb25zdCB7IHRpcEZvcm1hdHRlciwgdG9vbHRpcFZpc2libGUsIHRvb2x0aXBQbGFjZW1lbnQsIGdldFRvb2x0aXBQb3B1cENvbnRhaW5lciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHsgdmlzaWJsZXMgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCBpc1RpcEZvcm1hdHRlciA9IHRpcEZvcm1hdHRlciA/IHZpc2libGVzW2luZGV4XSB8fCBkcmFnZ2luZyA6IGZhbHNlO1xuICAgICAgICAgICAgY29uc3QgdmlzaWJsZSA9IHRvb2x0aXBWaXNpYmxlIHx8ICh0b29sdGlwVmlzaWJsZSA9PT0gdW5kZWZpbmVkICYmIGlzVGlwRm9ybWF0dGVyKTtcbiAgICAgICAgICAgIHJldHVybiAoPFRvb2x0aXAgcHJlZml4Q2xzPXt0b29sdGlwUHJlZml4Q2xzfSB0aXRsZT17dGlwRm9ybWF0dGVyID8gdGlwRm9ybWF0dGVyKHZhbHVlKSA6ICcnfSB2aXNpYmxlPXt2aXNpYmxlfSBwbGFjZW1lbnQ9e3Rvb2x0aXBQbGFjZW1lbnQgfHwgJ3RvcCd9IHRyYW5zaXRpb25OYW1lPVwiem9vbS1kb3duXCIga2V5PXtpbmRleH0gb3ZlcmxheUNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS10b29sdGlwYH0gZ2V0UG9wdXBDb250YWluZXI9e2dldFRvb2x0aXBQb3B1cENvbnRhaW5lciB8fCAoKCkgPT4gZG9jdW1lbnQuYm9keSl9PlxuICAgICAgICA8UmNIYW5kbGUgey4uLnJlc3RQcm9wc30gdmFsdWU9e3ZhbHVlfSBvbk1vdXNlRW50ZXI9eygpID0+IHRoaXMudG9nZ2xlVG9vbHRpcFZpc2libGUoaW5kZXgsIHRydWUpfSBvbk1vdXNlTGVhdmU9eygpID0+IHRoaXMudG9nZ2xlVG9vbHRpcFZpc2libGUoaW5kZXgsIGZhbHNlKX0vPlxuICAgICAgPC9Ub29sdGlwPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2F2ZVNsaWRlciA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnJjU2xpZGVyID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJTbGlkZXIgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCB0b29sdGlwUHJlZml4Q2xzOiBjdXN0b21pemVUb29sdGlwUHJlZml4Q2xzLCByYW5nZSB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwidG9vbHRpcFByZWZpeENsc1wiLCBcInJhbmdlXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnc2xpZGVyJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IHRvb2x0aXBQcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ3Rvb2x0aXAnLCBjdXN0b21pemVUb29sdGlwUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGlmIChyYW5nZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiAoPFJjUmFuZ2Ugey4uLnJlc3RQcm9wc30gcmVmPXt0aGlzLnNhdmVTbGlkZXJ9IGhhbmRsZT17KGluZm8pID0+IHRoaXMuaGFuZGxlV2l0aFRvb2x0aXAoe1xuICAgICAgICAgICAgICAgICAgICB0b29sdGlwUHJlZml4Q2xzLFxuICAgICAgICAgICAgICAgICAgICBwcmVmaXhDbHMsXG4gICAgICAgICAgICAgICAgICAgIGluZm8sXG4gICAgICAgICAgICAgICAgfSl9IHByZWZpeENscz17cHJlZml4Q2xzfSB0b29sdGlwUHJlZml4Q2xzPXt0b29sdGlwUHJlZml4Q2xzfS8+KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPFJjU2xpZGVyIHsuLi5yZXN0UHJvcHN9IHJlZj17dGhpcy5zYXZlU2xpZGVyfSBoYW5kbGU9eyhpbmZvKSA9PiB0aGlzLmhhbmRsZVdpdGhUb29sdGlwKHtcbiAgICAgICAgICAgICAgICB0b29sdGlwUHJlZml4Q2xzLFxuICAgICAgICAgICAgICAgIHByZWZpeENscyxcbiAgICAgICAgICAgICAgICBpbmZvLFxuICAgICAgICAgICAgfSl9IHByZWZpeENscz17cHJlZml4Q2xzfSB0b29sdGlwUHJlZml4Q2xzPXt0b29sdGlwUHJlZml4Q2xzfS8+KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHZpc2libGVzOiB7fSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgZm9jdXMoKSB7XG4gICAgICAgIHRoaXMucmNTbGlkZXIuZm9jdXMoKTtcbiAgICB9XG4gICAgYmx1cigpIHtcbiAgICAgICAgdGhpcy5yY1NsaWRlci5ibHVyKCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJTbGlkZXJ9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuU2xpZGVyLmRlZmF1bHRQcm9wcyA9IHtcbiAgICB0aXBGb3JtYXR0ZXIodmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIHZhbHVlLnRvU3RyaW5nKCk7XG4gICAgfSxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFEQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQUNBO0FBU0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUlBO0FBQUE7QUFKQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFJQTtBQUFBO0FBSkE7QUFYQTtBQUNBO0FBZ0JBO0FBQ0E7QUFEQTtBQXJDQTtBQXdDQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBbERBO0FBQ0E7QUFEQTtBQW9EQTtBQUNBO0FBQ0E7QUFDQTtBQUhBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/slider/index.js
