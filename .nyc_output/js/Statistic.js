__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider_context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider/context */ "./node_modules/antd/es/config-provider/context.js");
/* harmony import */ var _Number__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Number */ "./node_modules/antd/es/statistic/Number.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}






var Statistic = function Statistic(props) {
  var prefixCls = props.prefixCls,
      className = props.className,
      style = props.style,
      valueStyle = props.valueStyle,
      _props$value = props.value,
      value = _props$value === void 0 ? 0 : _props$value,
      title = props.title,
      valueRender = props.valueRender,
      prefix = props.prefix,
      suffix = props.suffix;
  var valueNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Number__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, props, {
    value: value
  }));

  if (valueRender) {
    valueNode = valueRender(valueNode);
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className),
    style: style
  }, title && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-title")
  }, title), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    style: valueStyle,
    className: "".concat(prefixCls, "-content")
  }, prefix && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-content-prefix")
  }, prefix), valueNode, suffix && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-content-suffix")
  }, suffix)));
};

Statistic.defaultProps = {
  decimalSeparator: '.',
  groupSeparator: ','
};
var WrapperStatistic = Object(_config_provider_context__WEBPACK_IMPORTED_MODULE_2__["withConfigConsumer"])({
  prefixCls: 'statistic'
})(Statistic);
/* harmony default export */ __webpack_exports__["default"] = (WrapperStatistic);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zdGF0aXN0aWMvU3RhdGlzdGljLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zdGF0aXN0aWMvU3RhdGlzdGljLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCB7IHdpdGhDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlci9jb250ZXh0JztcbmltcG9ydCBTdGF0aXN0aWNOdW1iZXIgZnJvbSAnLi9OdW1iZXInO1xuY29uc3QgU3RhdGlzdGljID0gcHJvcHMgPT4ge1xuICAgIGNvbnN0IHsgcHJlZml4Q2xzLCBjbGFzc05hbWUsIHN0eWxlLCB2YWx1ZVN0eWxlLCB2YWx1ZSA9IDAsIHRpdGxlLCB2YWx1ZVJlbmRlciwgcHJlZml4LCBzdWZmaXgsIH0gPSBwcm9wcztcbiAgICBsZXQgdmFsdWVOb2RlID0gPFN0YXRpc3RpY051bWJlciB7Li4ucHJvcHN9IHZhbHVlPXt2YWx1ZX0vPjtcbiAgICBpZiAodmFsdWVSZW5kZXIpIHtcbiAgICAgICAgdmFsdWVOb2RlID0gdmFsdWVSZW5kZXIodmFsdWVOb2RlKTtcbiAgICB9XG4gICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSl9IHN0eWxlPXtzdHlsZX0+XG4gICAgICB7dGl0bGUgJiYgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tdGl0bGVgfT57dGl0bGV9PC9kaXY+fVxuICAgICAgPGRpdiBzdHlsZT17dmFsdWVTdHlsZX0gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnRgfT5cbiAgICAgICAge3ByZWZpeCAmJiA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY29udGVudC1wcmVmaXhgfT57cHJlZml4fTwvc3Bhbj59XG4gICAgICAgIHt2YWx1ZU5vZGV9XG4gICAgICAgIHtzdWZmaXggJiYgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtc3VmZml4YH0+e3N1ZmZpeH08L3NwYW4+fVxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+KTtcbn07XG5TdGF0aXN0aWMuZGVmYXVsdFByb3BzID0ge1xuICAgIGRlY2ltYWxTZXBhcmF0b3I6ICcuJyxcbiAgICBncm91cFNlcGFyYXRvcjogJywnLFxufTtcbmNvbnN0IFdyYXBwZXJTdGF0aXN0aWMgPSB3aXRoQ29uZmlnQ29uc3VtZXIoe1xuICAgIHByZWZpeENsczogJ3N0YXRpc3RpYycsXG59KShTdGF0aXN0aWMpO1xuZXhwb3J0IGRlZmF1bHQgV3JhcHBlclN0YXRpc3RpYztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBWEE7QUFDQTtBQWNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/statistic/Statistic.js
