/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorEditHandler
 * @format
 * 
 */


var onBeforeInput = __webpack_require__(/*! ./editOnBeforeInput */ "./node_modules/draft-js/lib/editOnBeforeInput.js");

var onBlur = __webpack_require__(/*! ./editOnBlur */ "./node_modules/draft-js/lib/editOnBlur.js");

var onCompositionStart = __webpack_require__(/*! ./editOnCompositionStart */ "./node_modules/draft-js/lib/editOnCompositionStart.js");

var onCopy = __webpack_require__(/*! ./editOnCopy */ "./node_modules/draft-js/lib/editOnCopy.js");

var onCut = __webpack_require__(/*! ./editOnCut */ "./node_modules/draft-js/lib/editOnCut.js");

var onDragOver = __webpack_require__(/*! ./editOnDragOver */ "./node_modules/draft-js/lib/editOnDragOver.js");

var onDragStart = __webpack_require__(/*! ./editOnDragStart */ "./node_modules/draft-js/lib/editOnDragStart.js");

var onFocus = __webpack_require__(/*! ./editOnFocus */ "./node_modules/draft-js/lib/editOnFocus.js");

var onInput = __webpack_require__(/*! ./editOnInput */ "./node_modules/draft-js/lib/editOnInput.js");

var onKeyDown = __webpack_require__(/*! ./editOnKeyDown */ "./node_modules/draft-js/lib/editOnKeyDown.js");

var onPaste = __webpack_require__(/*! ./editOnPaste */ "./node_modules/draft-js/lib/editOnPaste.js");

var onSelect = __webpack_require__(/*! ./editOnSelect */ "./node_modules/draft-js/lib/editOnSelect.js");

var DraftEditorEditHandler = {
  onBeforeInput: onBeforeInput,
  onBlur: onBlur,
  onCompositionStart: onCompositionStart,
  onCopy: onCopy,
  onCut: onCut,
  onDragOver: onDragOver,
  onDragStart: onDragStart,
  onFocus: onFocus,
  onInput: onInput,
  onKeyDown: onKeyDown,
  onPaste: onPaste,
  onSelect: onSelect
};
module.exports = DraftEditorEditHandler;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yRWRpdEhhbmRsZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFZGl0b3JFZGl0SGFuZGxlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RWRpdG9yRWRpdEhhbmRsZXJcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIG9uQmVmb3JlSW5wdXQgPSByZXF1aXJlKCcuL2VkaXRPbkJlZm9yZUlucHV0Jyk7XG52YXIgb25CbHVyID0gcmVxdWlyZSgnLi9lZGl0T25CbHVyJyk7XG52YXIgb25Db21wb3NpdGlvblN0YXJ0ID0gcmVxdWlyZSgnLi9lZGl0T25Db21wb3NpdGlvblN0YXJ0Jyk7XG52YXIgb25Db3B5ID0gcmVxdWlyZSgnLi9lZGl0T25Db3B5Jyk7XG52YXIgb25DdXQgPSByZXF1aXJlKCcuL2VkaXRPbkN1dCcpO1xudmFyIG9uRHJhZ092ZXIgPSByZXF1aXJlKCcuL2VkaXRPbkRyYWdPdmVyJyk7XG52YXIgb25EcmFnU3RhcnQgPSByZXF1aXJlKCcuL2VkaXRPbkRyYWdTdGFydCcpO1xudmFyIG9uRm9jdXMgPSByZXF1aXJlKCcuL2VkaXRPbkZvY3VzJyk7XG52YXIgb25JbnB1dCA9IHJlcXVpcmUoJy4vZWRpdE9uSW5wdXQnKTtcbnZhciBvbktleURvd24gPSByZXF1aXJlKCcuL2VkaXRPbktleURvd24nKTtcbnZhciBvblBhc3RlID0gcmVxdWlyZSgnLi9lZGl0T25QYXN0ZScpO1xudmFyIG9uU2VsZWN0ID0gcmVxdWlyZSgnLi9lZGl0T25TZWxlY3QnKTtcblxudmFyIERyYWZ0RWRpdG9yRWRpdEhhbmRsZXIgPSB7XG4gIG9uQmVmb3JlSW5wdXQ6IG9uQmVmb3JlSW5wdXQsXG4gIG9uQmx1cjogb25CbHVyLFxuICBvbkNvbXBvc2l0aW9uU3RhcnQ6IG9uQ29tcG9zaXRpb25TdGFydCxcbiAgb25Db3B5OiBvbkNvcHksXG4gIG9uQ3V0OiBvbkN1dCxcbiAgb25EcmFnT3Zlcjogb25EcmFnT3ZlcixcbiAgb25EcmFnU3RhcnQ6IG9uRHJhZ1N0YXJ0LFxuICBvbkZvY3VzOiBvbkZvY3VzLFxuICBvbklucHV0OiBvbklucHV0LFxuICBvbktleURvd246IG9uS2V5RG93bixcbiAgb25QYXN0ZTogb25QYXN0ZSxcbiAgb25TZWxlY3Q6IG9uU2VsZWN0XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RWRpdG9yRWRpdEhhbmRsZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFlQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorEditHandler.js
