/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 * @stub
 * 
 */
 // \u00a1-\u00b1\u00b4-\u00b8\u00ba\u00bb\u00bf
//             is latin supplement punctuation except fractions and superscript
//             numbers
// \u2010-\u2027\u2030-\u205e
//             is punctuation from the general punctuation block:
//             weird quotes, commas, bullets, dashes, etc.
// \u30fb\u3001\u3002\u3008-\u3011\u3014-\u301f
//             is CJK punctuation
// \uff1a-\uff1f\uff01-\uff0f\uff3b-\uff40\uff5b-\uff65
//             is some full-width/half-width punctuation
// \u2E2E\u061f\u066a-\u066c\u061b\u060c\u060d\uFD3e\uFD3F
//             is some Arabic punctuation marks
// \u1801\u0964\u104a\u104b
//             is misc. other language punctuation marks

var PUNCTUATION = '[.,+*?$|#{}()\'\\^\\-\\[\\]\\\\\\/!@%"~=<>_:;' + "\u30FB\u3001\u3002\u3008-\u3011\u3014-\u301F\uFF1A-\uFF1F\uFF01-\uFF0F" + "\uFF3B-\uFF40\uFF5B-\uFF65\u2E2E\u061F\u066A-\u066C\u061B\u060C\u060D" + "\uFD3E\uFD3F\u1801\u0964\u104A\u104B\u2010-\u2027\u2030-\u205E" + '\xA1-\xB1\xB4-\xB8\xBA\xBB\xBF]';
module.exports = {
  getPunctuation: function getPunctuation() {
    return PUNCTUATION;
  }
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVG9rZW5pemVVdGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVG9rZW5pemVVdGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqIEBzdHViXG4gKiBcbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbi8vIFxcdTAwYTEtXFx1MDBiMVxcdTAwYjQtXFx1MDBiOFxcdTAwYmFcXHUwMGJiXFx1MDBiZlxuLy8gICAgICAgICAgICAgaXMgbGF0aW4gc3VwcGxlbWVudCBwdW5jdHVhdGlvbiBleGNlcHQgZnJhY3Rpb25zIGFuZCBzdXBlcnNjcmlwdFxuLy8gICAgICAgICAgICAgbnVtYmVyc1xuLy8gXFx1MjAxMC1cXHUyMDI3XFx1MjAzMC1cXHUyMDVlXG4vLyAgICAgICAgICAgICBpcyBwdW5jdHVhdGlvbiBmcm9tIHRoZSBnZW5lcmFsIHB1bmN0dWF0aW9uIGJsb2NrOlxuLy8gICAgICAgICAgICAgd2VpcmQgcXVvdGVzLCBjb21tYXMsIGJ1bGxldHMsIGRhc2hlcywgZXRjLlxuLy8gXFx1MzBmYlxcdTMwMDFcXHUzMDAyXFx1MzAwOC1cXHUzMDExXFx1MzAxNC1cXHUzMDFmXG4vLyAgICAgICAgICAgICBpcyBDSksgcHVuY3R1YXRpb25cbi8vIFxcdWZmMWEtXFx1ZmYxZlxcdWZmMDEtXFx1ZmYwZlxcdWZmM2ItXFx1ZmY0MFxcdWZmNWItXFx1ZmY2NVxuLy8gICAgICAgICAgICAgaXMgc29tZSBmdWxsLXdpZHRoL2hhbGYtd2lkdGggcHVuY3R1YXRpb25cbi8vIFxcdTJFMkVcXHUwNjFmXFx1MDY2YS1cXHUwNjZjXFx1MDYxYlxcdTA2MGNcXHUwNjBkXFx1RkQzZVxcdUZEM0Zcbi8vICAgICAgICAgICAgIGlzIHNvbWUgQXJhYmljIHB1bmN0dWF0aW9uIG1hcmtzXG4vLyBcXHUxODAxXFx1MDk2NFxcdTEwNGFcXHUxMDRiXG4vLyAgICAgICAgICAgICBpcyBtaXNjLiBvdGhlciBsYW5ndWFnZSBwdW5jdHVhdGlvbiBtYXJrc1xuXG52YXIgUFVOQ1RVQVRJT04gPSAnWy4sKyo/JHwje30oKVxcJ1xcXFxeXFxcXC1cXFxcW1xcXFxdXFxcXFxcXFxcXFxcLyFAJVwifj08Pl86OycgKyAnXFx1MzBGQlxcdTMwMDFcXHUzMDAyXFx1MzAwOC1cXHUzMDExXFx1MzAxNC1cXHUzMDFGXFx1RkYxQS1cXHVGRjFGXFx1RkYwMS1cXHVGRjBGJyArICdcXHVGRjNCLVxcdUZGNDBcXHVGRjVCLVxcdUZGNjVcXHUyRTJFXFx1MDYxRlxcdTA2NkEtXFx1MDY2Q1xcdTA2MUJcXHUwNjBDXFx1MDYwRCcgKyAnXFx1RkQzRVxcdUZEM0ZcXHUxODAxXFx1MDk2NFxcdTEwNEFcXHUxMDRCXFx1MjAxMC1cXHUyMDI3XFx1MjAzMC1cXHUyMDVFJyArICdcXHhBMS1cXHhCMVxceEI0LVxceEI4XFx4QkFcXHhCQlxceEJGXSc7XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBnZXRQdW5jdHVhdGlvbjogZnVuY3Rpb24gZ2V0UHVuY3R1YXRpb24oKSB7XG4gICAgcmV0dXJuIFBVTkNUVUFUSU9OO1xuICB9XG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7QUFXQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/TokenizeUtil.js
