/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftOffsetKey
 * @format
 * 
 */


var KEY_DELIMITER = '-';
var DraftOffsetKey = {
  encode: function encode(blockKey, decoratorKey, leafKey) {
    return blockKey + KEY_DELIMITER + decoratorKey + KEY_DELIMITER + leafKey;
  },
  decode: function decode(offsetKey) {
    var _offsetKey$split = offsetKey.split(KEY_DELIMITER),
        blockKey = _offsetKey$split[0],
        decoratorKey = _offsetKey$split[1],
        leafKey = _offsetKey$split[2];

    return {
      blockKey: blockKey,
      decoratorKey: parseInt(decoratorKey, 10),
      leafKey: parseInt(leafKey, 10)
    };
  }
};
module.exports = DraftOffsetKey;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0T2Zmc2V0S2V5LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0T2Zmc2V0S2V5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRHJhZnRPZmZzZXRLZXlcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEtFWV9ERUxJTUlURVIgPSAnLSc7XG5cbnZhciBEcmFmdE9mZnNldEtleSA9IHtcbiAgZW5jb2RlOiBmdW5jdGlvbiBlbmNvZGUoYmxvY2tLZXksIGRlY29yYXRvcktleSwgbGVhZktleSkge1xuICAgIHJldHVybiBibG9ja0tleSArIEtFWV9ERUxJTUlURVIgKyBkZWNvcmF0b3JLZXkgKyBLRVlfREVMSU1JVEVSICsgbGVhZktleTtcbiAgfSxcblxuICBkZWNvZGU6IGZ1bmN0aW9uIGRlY29kZShvZmZzZXRLZXkpIHtcbiAgICB2YXIgX29mZnNldEtleSRzcGxpdCA9IG9mZnNldEtleS5zcGxpdChLRVlfREVMSU1JVEVSKSxcbiAgICAgICAgYmxvY2tLZXkgPSBfb2Zmc2V0S2V5JHNwbGl0WzBdLFxuICAgICAgICBkZWNvcmF0b3JLZXkgPSBfb2Zmc2V0S2V5JHNwbGl0WzFdLFxuICAgICAgICBsZWFmS2V5ID0gX29mZnNldEtleSRzcGxpdFsyXTtcblxuICAgIHJldHVybiB7XG4gICAgICBibG9ja0tleTogYmxvY2tLZXksXG4gICAgICBkZWNvcmF0b3JLZXk6IHBhcnNlSW50KGRlY29yYXRvcktleSwgMTApLFxuICAgICAgbGVhZktleTogcGFyc2VJbnQobGVhZktleSwgMTApXG4gICAgfTtcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBEcmFmdE9mZnNldEtleTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQWhCQTtBQW1CQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftOffsetKey.js
