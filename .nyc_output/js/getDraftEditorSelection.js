/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getDraftEditorSelection
 * @format
 * 
 */


var getDraftEditorSelectionWithNodes = __webpack_require__(/*! ./getDraftEditorSelectionWithNodes */ "./node_modules/draft-js/lib/getDraftEditorSelectionWithNodes.js");
/**
 * Convert the current selection range to an anchor/focus pair of offset keys
 * and values that can be interpreted by components.
 */


function getDraftEditorSelection(editorState, root) {
  var selection = global.getSelection(); // No active selection.

  if (selection.rangeCount === 0) {
    return {
      selectionState: editorState.getSelection().set('hasFocus', false),
      needsRecovery: false
    };
  }

  return getDraftEditorSelectionWithNodes(editorState, root, selection.anchorNode, selection.anchorOffset, selection.focusNode, selection.focusOffset);
}

module.exports = getDraftEditorSelection;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERyYWZ0RWRpdG9yU2VsZWN0aW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERyYWZ0RWRpdG9yU2VsZWN0aW9uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb25cbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGdldERyYWZ0RWRpdG9yU2VsZWN0aW9uV2l0aE5vZGVzID0gcmVxdWlyZSgnLi9nZXREcmFmdEVkaXRvclNlbGVjdGlvbldpdGhOb2RlcycpO1xuXG4vKipcbiAqIENvbnZlcnQgdGhlIGN1cnJlbnQgc2VsZWN0aW9uIHJhbmdlIHRvIGFuIGFuY2hvci9mb2N1cyBwYWlyIG9mIG9mZnNldCBrZXlzXG4gKiBhbmQgdmFsdWVzIHRoYXQgY2FuIGJlIGludGVycHJldGVkIGJ5IGNvbXBvbmVudHMuXG4gKi9cbmZ1bmN0aW9uIGdldERyYWZ0RWRpdG9yU2VsZWN0aW9uKGVkaXRvclN0YXRlLCByb290KSB7XG4gIHZhciBzZWxlY3Rpb24gPSBnbG9iYWwuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgLy8gTm8gYWN0aXZlIHNlbGVjdGlvbi5cbiAgaWYgKHNlbGVjdGlvbi5yYW5nZUNvdW50ID09PSAwKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHNlbGVjdGlvblN0YXRlOiBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKS5zZXQoJ2hhc0ZvY3VzJywgZmFsc2UpLFxuICAgICAgbmVlZHNSZWNvdmVyeTogZmFsc2VcbiAgICB9O1xuICB9XG5cbiAgcmV0dXJuIGdldERyYWZ0RWRpdG9yU2VsZWN0aW9uV2l0aE5vZGVzKGVkaXRvclN0YXRlLCByb290LCBzZWxlY3Rpb24uYW5jaG9yTm9kZSwgc2VsZWN0aW9uLmFuY2hvck9mZnNldCwgc2VsZWN0aW9uLmZvY3VzTm9kZSwgc2VsZWN0aW9uLmZvY3VzT2Zmc2V0KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXREcmFmdEVkaXRvclNlbGVjdGlvbjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getDraftEditorSelection.js
