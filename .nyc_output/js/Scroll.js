
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/**
 * @param {DOMElement} element
 * @param {DOMDocument} doc
 * @return {boolean}
 */

function _isViewportScrollElement(element, doc) {
  return !!doc && (element === doc.documentElement || element === doc.body);
}
/**
 * Scroll Module. This class contains 4 simple static functions
 * to be used to access Element.scrollTop/scrollLeft properties.
 * To solve the inconsistencies between browsers when either
 * document.body or document.documentElement is supplied,
 * below logic will be used to alleviate the issue:
 *
 * 1. If 'element' is either 'document.body' or 'document.documentElement,
 *    get whichever element's 'scroll{Top,Left}' is larger.
 * 2. If 'element' is either 'document.body' or 'document.documentElement',
 *    set the 'scroll{Top,Left}' on both elements.
 */


var Scroll = {
  /**
   * @param {DOMElement} element
   * @return {number}
   */
  getTop: function getTop(element) {
    var doc = element.ownerDocument;
    return _isViewportScrollElement(element, doc) ? // In practice, they will either both have the same value,
    // or one will be zero and the other will be the scroll position
    // of the viewport. So we can use `X || Y` instead of `Math.max(X, Y)`
    doc.body.scrollTop || doc.documentElement.scrollTop : element.scrollTop;
  },

  /**
   * @param {DOMElement} element
   * @param {number} newTop
   */
  setTop: function setTop(element, newTop) {
    var doc = element.ownerDocument;

    if (_isViewportScrollElement(element, doc)) {
      doc.body.scrollTop = doc.documentElement.scrollTop = newTop;
    } else {
      element.scrollTop = newTop;
    }
  },

  /**
   * @param {DOMElement} element
   * @return {number}
   */
  getLeft: function getLeft(element) {
    var doc = element.ownerDocument;
    return _isViewportScrollElement(element, doc) ? doc.body.scrollLeft || doc.documentElement.scrollLeft : element.scrollLeft;
  },

  /**
   * @param {DOMElement} element
   * @param {number} newLeft
   */
  setLeft: function setLeft(element, newLeft) {
    var doc = element.ownerDocument;

    if (_isViewportScrollElement(element, doc)) {
      doc.body.scrollLeft = doc.documentElement.scrollLeft = newLeft;
    } else {
      element.scrollLeft = newLeft;
    }
  }
};
module.exports = Scroll;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvU2Nyb2xsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvU2Nyb2xsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG4vKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICpcbiAqL1xuXG4vKipcbiAqIEBwYXJhbSB7RE9NRWxlbWVudH0gZWxlbWVudFxuICogQHBhcmFtIHtET01Eb2N1bWVudH0gZG9jXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBfaXNWaWV3cG9ydFNjcm9sbEVsZW1lbnQoZWxlbWVudCwgZG9jKSB7XG4gIHJldHVybiAhIWRvYyAmJiAoZWxlbWVudCA9PT0gZG9jLmRvY3VtZW50RWxlbWVudCB8fCBlbGVtZW50ID09PSBkb2MuYm9keSk7XG59XG5cbi8qKlxuICogU2Nyb2xsIE1vZHVsZS4gVGhpcyBjbGFzcyBjb250YWlucyA0IHNpbXBsZSBzdGF0aWMgZnVuY3Rpb25zXG4gKiB0byBiZSB1c2VkIHRvIGFjY2VzcyBFbGVtZW50LnNjcm9sbFRvcC9zY3JvbGxMZWZ0IHByb3BlcnRpZXMuXG4gKiBUbyBzb2x2ZSB0aGUgaW5jb25zaXN0ZW5jaWVzIGJldHdlZW4gYnJvd3NlcnMgd2hlbiBlaXRoZXJcbiAqIGRvY3VtZW50LmJvZHkgb3IgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50IGlzIHN1cHBsaWVkLFxuICogYmVsb3cgbG9naWMgd2lsbCBiZSB1c2VkIHRvIGFsbGV2aWF0ZSB0aGUgaXNzdWU6XG4gKlxuICogMS4gSWYgJ2VsZW1lbnQnIGlzIGVpdGhlciAnZG9jdW1lbnQuYm9keScgb3IgJ2RvY3VtZW50LmRvY3VtZW50RWxlbWVudCxcbiAqICAgIGdldCB3aGljaGV2ZXIgZWxlbWVudCdzICdzY3JvbGx7VG9wLExlZnR9JyBpcyBsYXJnZXIuXG4gKiAyLiBJZiAnZWxlbWVudCcgaXMgZWl0aGVyICdkb2N1bWVudC5ib2R5JyBvciAnZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50JyxcbiAqICAgIHNldCB0aGUgJ3Njcm9sbHtUb3AsTGVmdH0nIG9uIGJvdGggZWxlbWVudHMuXG4gKi9cblxudmFyIFNjcm9sbCA9IHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7RE9NRWxlbWVudH0gZWxlbWVudFxuICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAqL1xuICBnZXRUb3A6IGZ1bmN0aW9uIGdldFRvcChlbGVtZW50KSB7XG4gICAgdmFyIGRvYyA9IGVsZW1lbnQub3duZXJEb2N1bWVudDtcbiAgICByZXR1cm4gX2lzVmlld3BvcnRTY3JvbGxFbGVtZW50KGVsZW1lbnQsIGRvYykgP1xuICAgIC8vIEluIHByYWN0aWNlLCB0aGV5IHdpbGwgZWl0aGVyIGJvdGggaGF2ZSB0aGUgc2FtZSB2YWx1ZSxcbiAgICAvLyBvciBvbmUgd2lsbCBiZSB6ZXJvIGFuZCB0aGUgb3RoZXIgd2lsbCBiZSB0aGUgc2Nyb2xsIHBvc2l0aW9uXG4gICAgLy8gb2YgdGhlIHZpZXdwb3J0LiBTbyB3ZSBjYW4gdXNlIGBYIHx8IFlgIGluc3RlYWQgb2YgYE1hdGgubWF4KFgsIFkpYFxuICAgIGRvYy5ib2R5LnNjcm9sbFRvcCB8fCBkb2MuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCA6IGVsZW1lbnQuc2Nyb2xsVG9wO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0RPTUVsZW1lbnR9IGVsZW1lbnRcbiAgICogQHBhcmFtIHtudW1iZXJ9IG5ld1RvcFxuICAgKi9cbiAgc2V0VG9wOiBmdW5jdGlvbiBzZXRUb3AoZWxlbWVudCwgbmV3VG9wKSB7XG4gICAgdmFyIGRvYyA9IGVsZW1lbnQub3duZXJEb2N1bWVudDtcbiAgICBpZiAoX2lzVmlld3BvcnRTY3JvbGxFbGVtZW50KGVsZW1lbnQsIGRvYykpIHtcbiAgICAgIGRvYy5ib2R5LnNjcm9sbFRvcCA9IGRvYy5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wID0gbmV3VG9wO1xuICAgIH0gZWxzZSB7XG4gICAgICBlbGVtZW50LnNjcm9sbFRvcCA9IG5ld1RvcDtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7RE9NRWxlbWVudH0gZWxlbWVudFxuICAgKiBAcmV0dXJuIHtudW1iZXJ9XG4gICAqL1xuICBnZXRMZWZ0OiBmdW5jdGlvbiBnZXRMZWZ0KGVsZW1lbnQpIHtcbiAgICB2YXIgZG9jID0gZWxlbWVudC5vd25lckRvY3VtZW50O1xuICAgIHJldHVybiBfaXNWaWV3cG9ydFNjcm9sbEVsZW1lbnQoZWxlbWVudCwgZG9jKSA/IGRvYy5ib2R5LnNjcm9sbExlZnQgfHwgZG9jLmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0IDogZWxlbWVudC5zY3JvbGxMZWZ0O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0RPTUVsZW1lbnR9IGVsZW1lbnRcbiAgICogQHBhcmFtIHtudW1iZXJ9IG5ld0xlZnRcbiAgICovXG4gIHNldExlZnQ6IGZ1bmN0aW9uIHNldExlZnQoZWxlbWVudCwgbmV3TGVmdCkge1xuICAgIHZhciBkb2MgPSBlbGVtZW50Lm93bmVyRG9jdW1lbnQ7XG4gICAgaWYgKF9pc1ZpZXdwb3J0U2Nyb2xsRWxlbWVudChlbGVtZW50LCBkb2MpKSB7XG4gICAgICBkb2MuYm9keS5zY3JvbGxMZWZ0ID0gZG9jLmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0ID0gbmV3TGVmdDtcbiAgICB9IGVsc2Uge1xuICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gbmV3TGVmdDtcbiAgICB9XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gU2Nyb2xsOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7QUFRQTs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvQ0E7QUFrREEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/Scroll.js
