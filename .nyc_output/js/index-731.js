__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Avatar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Avatar */ "./node_modules/antd/es/skeleton/Avatar.js");
/* harmony import */ var _Title__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Title */ "./node_modules/antd/es/skeleton/Title.js");
/* harmony import */ var _Paragraph__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Paragraph */ "./node_modules/antd/es/skeleton/Paragraph.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}








function getComponentProps(prop) {
  if (prop && _typeof(prop) === 'object') {
    return prop;
  }

  return {};
}

function getAvatarBasicProps(hasTitle, hasParagraph) {
  if (hasTitle && !hasParagraph) {
    return {
      shape: 'square'
    };
  }

  return {
    shape: 'circle'
  };
}

function getTitleBasicProps(hasAvatar, hasParagraph) {
  if (!hasAvatar && hasParagraph) {
    return {
      width: '38%'
    };
  }

  if (hasAvatar && hasParagraph) {
    return {
      width: '50%'
    };
  }

  return {};
}

function getParagraphBasicProps(hasAvatar, hasTitle) {
  var basicProps = {}; // Width

  if (!hasAvatar || !hasTitle) {
    basicProps.width = '61%';
  } // Rows


  if (!hasAvatar && hasTitle) {
    basicProps.rows = 3;
  } else {
    basicProps.rows = 2;
  }

  return basicProps;
}

var Skeleton =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Skeleton, _React$Component);

  function Skeleton() {
    var _this;

    _classCallCheck(this, Skeleton);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Skeleton).apply(this, arguments));

    _this.renderSkeleton = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          loading = _this$props.loading,
          className = _this$props.className,
          children = _this$props.children,
          avatar = _this$props.avatar,
          title = _this$props.title,
          paragraph = _this$props.paragraph,
          active = _this$props.active;
      var prefixCls = getPrefixCls('skeleton', customizePrefixCls);

      if (loading || !('loading' in _this.props)) {
        var _classNames;

        var hasAvatar = !!avatar;
        var hasTitle = !!title;
        var hasParagraph = !!paragraph; // Avatar

        var avatarNode;

        if (hasAvatar) {
          var avatarProps = _extends(_extends({
            prefixCls: "".concat(prefixCls, "-avatar")
          }, getAvatarBasicProps(hasTitle, hasParagraph)), getComponentProps(avatar));

          avatarNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
            className: "".concat(prefixCls, "-header")
          }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Avatar__WEBPACK_IMPORTED_MODULE_2__["default"], avatarProps));
        }

        var contentNode;

        if (hasTitle || hasParagraph) {
          // Title
          var $title;

          if (hasTitle) {
            var titleProps = _extends(_extends({
              prefixCls: "".concat(prefixCls, "-title")
            }, getTitleBasicProps(hasAvatar, hasParagraph)), getComponentProps(title));

            $title = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Title__WEBPACK_IMPORTED_MODULE_3__["default"], titleProps);
          } // Paragraph


          var paragraphNode;

          if (hasParagraph) {
            var paragraphProps = _extends(_extends({
              prefixCls: "".concat(prefixCls, "-paragraph")
            }, getParagraphBasicProps(hasAvatar, hasTitle)), getComponentProps(paragraph));

            paragraphNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Paragraph__WEBPACK_IMPORTED_MODULE_4__["default"], paragraphProps);
          }

          contentNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
            className: "".concat(prefixCls, "-content")
          }, $title, paragraphNode);
        }

        var cls = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-with-avatar"), hasAvatar), _defineProperty(_classNames, "".concat(prefixCls, "-active"), active), _classNames));
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: cls
        }, avatarNode, contentNode);
      }

      return children;
    };

    return _this;
  }

  _createClass(Skeleton, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderSkeleton);
    }
  }]);

  return Skeleton;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Skeleton.defaultProps = {
  avatar: false,
  title: true,
  paragraph: true
};
/* harmony default export */ __webpack_exports__["default"] = (Skeleton);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9za2VsZXRvbi9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc2tlbGV0b24vaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IEF2YXRhciBmcm9tICcuL0F2YXRhcic7XG5pbXBvcnQgVGl0bGUgZnJvbSAnLi9UaXRsZSc7XG5pbXBvcnQgUGFyYWdyYXBoIGZyb20gJy4vUGFyYWdyYXBoJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmZ1bmN0aW9uIGdldENvbXBvbmVudFByb3BzKHByb3ApIHtcbiAgICBpZiAocHJvcCAmJiB0eXBlb2YgcHJvcCA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgcmV0dXJuIHByb3A7XG4gICAgfVxuICAgIHJldHVybiB7fTtcbn1cbmZ1bmN0aW9uIGdldEF2YXRhckJhc2ljUHJvcHMoaGFzVGl0bGUsIGhhc1BhcmFncmFwaCkge1xuICAgIGlmIChoYXNUaXRsZSAmJiAhaGFzUGFyYWdyYXBoKSB7XG4gICAgICAgIHJldHVybiB7IHNoYXBlOiAnc3F1YXJlJyB9O1xuICAgIH1cbiAgICByZXR1cm4geyBzaGFwZTogJ2NpcmNsZScgfTtcbn1cbmZ1bmN0aW9uIGdldFRpdGxlQmFzaWNQcm9wcyhoYXNBdmF0YXIsIGhhc1BhcmFncmFwaCkge1xuICAgIGlmICghaGFzQXZhdGFyICYmIGhhc1BhcmFncmFwaCkge1xuICAgICAgICByZXR1cm4geyB3aWR0aDogJzM4JScgfTtcbiAgICB9XG4gICAgaWYgKGhhc0F2YXRhciAmJiBoYXNQYXJhZ3JhcGgpIHtcbiAgICAgICAgcmV0dXJuIHsgd2lkdGg6ICc1MCUnIH07XG4gICAgfVxuICAgIHJldHVybiB7fTtcbn1cbmZ1bmN0aW9uIGdldFBhcmFncmFwaEJhc2ljUHJvcHMoaGFzQXZhdGFyLCBoYXNUaXRsZSkge1xuICAgIGNvbnN0IGJhc2ljUHJvcHMgPSB7fTtcbiAgICAvLyBXaWR0aFxuICAgIGlmICghaGFzQXZhdGFyIHx8ICFoYXNUaXRsZSkge1xuICAgICAgICBiYXNpY1Byb3BzLndpZHRoID0gJzYxJSc7XG4gICAgfVxuICAgIC8vIFJvd3NcbiAgICBpZiAoIWhhc0F2YXRhciAmJiBoYXNUaXRsZSkge1xuICAgICAgICBiYXNpY1Byb3BzLnJvd3MgPSAzO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgYmFzaWNQcm9wcy5yb3dzID0gMjtcbiAgICB9XG4gICAgcmV0dXJuIGJhc2ljUHJvcHM7XG59XG5jbGFzcyBTa2VsZXRvbiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMucmVuZGVyU2tlbGV0b24gPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgbG9hZGluZywgY2xhc3NOYW1lLCBjaGlsZHJlbiwgYXZhdGFyLCB0aXRsZSwgcGFyYWdyYXBoLCBhY3RpdmUsIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdza2VsZXRvbicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBpZiAobG9hZGluZyB8fCAhKCdsb2FkaW5nJyBpbiB0aGlzLnByb3BzKSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGhhc0F2YXRhciA9ICEhYXZhdGFyO1xuICAgICAgICAgICAgICAgIGNvbnN0IGhhc1RpdGxlID0gISF0aXRsZTtcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNQYXJhZ3JhcGggPSAhIXBhcmFncmFwaDtcbiAgICAgICAgICAgICAgICAvLyBBdmF0YXJcbiAgICAgICAgICAgICAgICBsZXQgYXZhdGFyTm9kZTtcbiAgICAgICAgICAgICAgICBpZiAoaGFzQXZhdGFyKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGF2YXRhclByb3BzID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHsgcHJlZml4Q2xzOiBgJHtwcmVmaXhDbHN9LWF2YXRhcmAgfSwgZ2V0QXZhdGFyQmFzaWNQcm9wcyhoYXNUaXRsZSwgaGFzUGFyYWdyYXBoKSksIGdldENvbXBvbmVudFByb3BzKGF2YXRhcikpO1xuICAgICAgICAgICAgICAgICAgICBhdmF0YXJOb2RlID0gKDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWhlYWRlcmB9PlxuICAgICAgICAgICAgPEF2YXRhciB7Li4uYXZhdGFyUHJvcHN9Lz5cbiAgICAgICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsZXQgY29udGVudE5vZGU7XG4gICAgICAgICAgICAgICAgaWYgKGhhc1RpdGxlIHx8IGhhc1BhcmFncmFwaCkge1xuICAgICAgICAgICAgICAgICAgICAvLyBUaXRsZVxuICAgICAgICAgICAgICAgICAgICBsZXQgJHRpdGxlO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaGFzVGl0bGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRpdGxlUHJvcHMgPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oeyBwcmVmaXhDbHM6IGAke3ByZWZpeENsc30tdGl0bGVgIH0sIGdldFRpdGxlQmFzaWNQcm9wcyhoYXNBdmF0YXIsIGhhc1BhcmFncmFwaCkpLCBnZXRDb21wb25lbnRQcm9wcyh0aXRsZSkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHRpdGxlID0gPFRpdGxlIHsuLi50aXRsZVByb3BzfS8+O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIFBhcmFncmFwaFxuICAgICAgICAgICAgICAgICAgICBsZXQgcGFyYWdyYXBoTm9kZTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGhhc1BhcmFncmFwaCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyYWdyYXBoUHJvcHMgPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oeyBwcmVmaXhDbHM6IGAke3ByZWZpeENsc30tcGFyYWdyYXBoYCB9LCBnZXRQYXJhZ3JhcGhCYXNpY1Byb3BzKGhhc0F2YXRhciwgaGFzVGl0bGUpKSwgZ2V0Q29tcG9uZW50UHJvcHMocGFyYWdyYXBoKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhZ3JhcGhOb2RlID0gPFBhcmFncmFwaCB7Li4ucGFyYWdyYXBoUHJvcHN9Lz47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY29udGVudE5vZGUgPSAoPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY29udGVudGB9PlxuICAgICAgICAgICAgeyR0aXRsZX1cbiAgICAgICAgICAgIHtwYXJhZ3JhcGhOb2RlfVxuICAgICAgICAgIDwvZGl2Pik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnN0IGNscyA9IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30td2l0aC1hdmF0YXJgXTogaGFzQXZhdGFyLFxuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1hY3RpdmVgXTogYWN0aXZlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2Nsc30+XG4gICAgICAgICAge2F2YXRhck5vZGV9XG4gICAgICAgICAge2NvbnRlbnROb2RlfVxuICAgICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkcmVuO1xuICAgICAgICB9O1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyU2tlbGV0b259PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuU2tlbGV0b24uZGVmYXVsdFByb3BzID0ge1xuICAgIGF2YXRhcjogZmFsc2UsXG4gICAgdGl0bGU6IHRydWUsXG4gICAgcGFyYWdyYXBoOiB0cnVlLFxufTtcbmV4cG9ydCBkZWZhdWx0IFNrZWxldG9uO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUxBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBM0NBO0FBQ0E7QUFIQTtBQStDQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7Ozs7QUFuREE7QUFDQTtBQW9EQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/skeleton/index.js
