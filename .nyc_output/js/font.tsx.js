__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FontColors", function() { return FontColors; });
// 字体颜色
var FontColors = {
  primary: "regular",
  // 主要
  regular: "regular",
  // 常规
  secondary: "regular",
  // 次要
  placeholder: "placeholder",
  // 占位
  label: "#9e9e9e",
  // label
  valueAfterLabel: "rgba(0, 0, 0, 0.85)",
  // label-value
  black65: "rgba(0, 0, 0, 0.65)" //

};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svc3R5bGVzL2ZvbnQudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svc3R5bGVzL2ZvbnQudHN4Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIOWtl+S9k+minOiJslxuZXhwb3J0IGNvbnN0IEZvbnRDb2xvcnMgPSB7XG4gIHByaW1hcnk6IFwicmVndWxhclwiLCAvLyDkuLvopoFcbiAgcmVndWxhcjogXCJyZWd1bGFyXCIsIC8vIOW4uOinhFxuICBzZWNvbmRhcnk6IFwicmVndWxhclwiLCAvLyDmrKHopoFcbiAgcGxhY2Vob2xkZXI6IFwicGxhY2Vob2xkZXJcIiwgLy8g5Y2g5L2NXG4gIGxhYmVsOiBcIiM5ZTllOWVcIiwgLy8gbGFiZWxcbiAgdmFsdWVBZnRlckxhYmVsOiBcInJnYmEoMCwgMCwgMCwgMC44NSlcIiwgLy8gbGFiZWwtdmFsdWVcbiAgYmxhY2s2NTogXCJyZ2JhKDAsIDAsIDAsIDAuNjUpXCIsIC8vXG59O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/styles/font.tsx
