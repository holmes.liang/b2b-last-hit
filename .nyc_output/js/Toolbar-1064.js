__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! immutable */ "./node_modules/rc-editor-core/node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ToolbarLine__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ToolbarLine */ "./node_modules/rc-editor-core/es/Toolbar/ToolbarLine.js");
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}
/* tslint:disable:interface-name */






function noop() {}

var Toolbar = function (_React$Component) {
  _inherits(Toolbar, _React$Component);

  function Toolbar(props) {
    _classCallCheck(this, Toolbar);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    var map = {};
    props.plugins.forEach(function (plugin) {
      map[plugin.name] = plugin;
    });
    _this.pluginsMap = Object(immutable__WEBPACK_IMPORTED_MODULE_1__["Map"])(map);
    _this.state = {
      editorState: props.editorState,
      toolbars: []
    };
    return _this;
  }

  Toolbar.prototype.renderToolbarItem = function renderToolbarItem(pluginName, idx) {
    var element = this.pluginsMap.get(pluginName);

    if (element && element.component) {
      var component = element.component;
      var props = {
        key: 'toolbar-item-' + idx,
        onClick: component.props ? component.props.onClick : noop
      };

      if (react__WEBPACK_IMPORTED_MODULE_0___default.a.isValidElement(component)) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.cloneElement(component, props);
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(component, props);
    }

    return null;
  };

  Toolbar.prototype.conpomentWillReceiveProps = function conpomentWillReceiveProps(nextProps) {
    this.render();
  };

  Toolbar.prototype.render = function render() {
    var _this2 = this;

    var _props = this.props,
        toolbars = _props.toolbars,
        prefixCls = _props.prefixCls;
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement('div', {
      className: prefixCls + '-toolbar'
    }, toolbars.map(function (toolbar, idx) {
      var children = react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.map(toolbar, _this2.renderToolbarItem.bind(_this2));
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ToolbarLine__WEBPACK_IMPORTED_MODULE_2__["default"], {
        key: 'toolbar-' + idx
      }, children);
    }));
  };

  return Toolbar;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (Toolbar);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvVG9vbGJhci9Ub29sYmFyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvVG9vbGJhci9Ub29sYmFyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxuLyogdHNsaW50OmRpc2FibGU6aW50ZXJmYWNlLW5hbWUgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBNYXAgfSBmcm9tICdpbW11dGFibGUnO1xuaW1wb3J0IFRvb2xiYXJMaW5lIGZyb20gJy4vVG9vbGJhckxpbmUnO1xuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbnZhciBUb29sYmFyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgICBfaW5oZXJpdHMoVG9vbGJhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgICBmdW5jdGlvbiBUb29sYmFyKHByb3BzKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBUb29sYmFyKTtcblxuICAgICAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgICAgICB2YXIgbWFwID0ge307XG4gICAgICAgIHByb3BzLnBsdWdpbnMuZm9yRWFjaChmdW5jdGlvbiAocGx1Z2luKSB7XG4gICAgICAgICAgICBtYXBbcGx1Z2luLm5hbWVdID0gcGx1Z2luO1xuICAgICAgICB9KTtcbiAgICAgICAgX3RoaXMucGx1Z2luc01hcCA9IE1hcChtYXApO1xuICAgICAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGVkaXRvclN0YXRlOiBwcm9wcy5lZGl0b3JTdGF0ZSxcbiAgICAgICAgICAgIHRvb2xiYXJzOiBbXVxuICAgICAgICB9O1xuICAgICAgICByZXR1cm4gX3RoaXM7XG4gICAgfVxuXG4gICAgVG9vbGJhci5wcm90b3R5cGUucmVuZGVyVG9vbGJhckl0ZW0gPSBmdW5jdGlvbiByZW5kZXJUb29sYmFySXRlbShwbHVnaW5OYW1lLCBpZHgpIHtcbiAgICAgICAgdmFyIGVsZW1lbnQgPSB0aGlzLnBsdWdpbnNNYXAuZ2V0KHBsdWdpbk5hbWUpO1xuICAgICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LmNvbXBvbmVudCkge1xuICAgICAgICAgICAgdmFyIGNvbXBvbmVudCA9IGVsZW1lbnQuY29tcG9uZW50O1xuXG4gICAgICAgICAgICB2YXIgcHJvcHMgPSB7XG4gICAgICAgICAgICAgICAga2V5OiAndG9vbGJhci1pdGVtLScgKyBpZHgsXG4gICAgICAgICAgICAgICAgb25DbGljazogY29tcG9uZW50LnByb3BzID8gY29tcG9uZW50LnByb3BzLm9uQ2xpY2sgOiBub29wXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGNvbXBvbmVudCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGNvbXBvbmVudCwgcHJvcHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoY29tcG9uZW50LCBwcm9wcyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIFRvb2xiYXIucHJvdG90eXBlLmNvbnBvbWVudFdpbGxSZWNlaXZlUHJvcHMgPSBmdW5jdGlvbiBjb25wb21lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgICAgICB0aGlzLnJlbmRlcigpO1xuICAgIH07XG5cbiAgICBUb29sYmFyLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgdG9vbGJhcnMgPSBfcHJvcHMudG9vbGJhcnMsXG4gICAgICAgICAgICBwcmVmaXhDbHMgPSBfcHJvcHMucHJlZml4Q2xzO1xuXG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgICB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy10b29sYmFyJyB9LFxuICAgICAgICAgICAgdG9vbGJhcnMubWFwKGZ1bmN0aW9uICh0b29sYmFyLCBpZHgpIHtcbiAgICAgICAgICAgICAgICB2YXIgY2hpbGRyZW4gPSBSZWFjdC5DaGlsZHJlbi5tYXAodG9vbGJhciwgX3RoaXMyLnJlbmRlclRvb2xiYXJJdGVtLmJpbmQoX3RoaXMyKSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICAgICAgIFRvb2xiYXJMaW5lLFxuICAgICAgICAgICAgICAgICAgICB7IGtleTogJ3Rvb2xiYXItJyArIGlkeCB9LFxuICAgICAgICAgICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH07XG5cbiAgICByZXR1cm4gVG9vbGJhcjtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuZXhwb3J0IGRlZmF1bHQgVG9vbGJhcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/Toolbar/Toolbar.js
