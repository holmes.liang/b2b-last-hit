var eventUtil = __webpack_require__(/*! ./event */ "./node_modules/zrender/lib/core/event.js");
/**
 * Only implements needed gestures for mobile.
 */


var GestureMgr = function GestureMgr() {
  /**
   * @private
   * @type {Array.<Object>}
   */
  this._track = [];
};

GestureMgr.prototype = {
  constructor: GestureMgr,
  recognize: function recognize(event, target, root) {
    this._doTrack(event, target, root);

    return this._recognize(event);
  },
  clear: function clear() {
    this._track.length = 0;
    return this;
  },
  _doTrack: function _doTrack(event, target, root) {
    var touches = event.touches;

    if (!touches) {
      return;
    }

    var trackItem = {
      points: [],
      touches: [],
      target: target,
      event: event
    };

    for (var i = 0, len = touches.length; i < len; i++) {
      var touch = touches[i];
      var pos = eventUtil.clientToLocal(root, touch, {});
      trackItem.points.push([pos.zrX, pos.zrY]);
      trackItem.touches.push(touch);
    }

    this._track.push(trackItem);
  },
  _recognize: function _recognize(event) {
    for (var eventName in recognizers) {
      if (recognizers.hasOwnProperty(eventName)) {
        var gestureInfo = recognizers[eventName](this._track, event);

        if (gestureInfo) {
          return gestureInfo;
        }
      }
    }
  }
};

function dist(pointPair) {
  var dx = pointPair[1][0] - pointPair[0][0];
  var dy = pointPair[1][1] - pointPair[0][1];
  return Math.sqrt(dx * dx + dy * dy);
}

function center(pointPair) {
  return [(pointPair[0][0] + pointPair[1][0]) / 2, (pointPair[0][1] + pointPair[1][1]) / 2];
}

var recognizers = {
  pinch: function pinch(track, event) {
    var trackLen = track.length;

    if (!trackLen) {
      return;
    }

    var pinchEnd = (track[trackLen - 1] || {}).points;
    var pinchPre = (track[trackLen - 2] || {}).points || pinchEnd;

    if (pinchPre && pinchPre.length > 1 && pinchEnd && pinchEnd.length > 1) {
      var pinchScale = dist(pinchEnd) / dist(pinchPre);
      !isFinite(pinchScale) && (pinchScale = 1);
      event.pinchScale = pinchScale;
      var pinchCenter = center(pinchEnd);
      event.pinchX = pinchCenter[0];
      event.pinchY = pinchCenter[1];
      return {
        type: 'pinch',
        target: track[0].target,
        event: event
      };
    }
  } // Only pinch currently.

};
var _default = GestureMgr;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9HZXN0dXJlTWdyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9HZXN0dXJlTWdyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBldmVudFV0aWwgPSByZXF1aXJlKFwiLi9ldmVudFwiKTtcblxuLyoqXG4gKiBPbmx5IGltcGxlbWVudHMgbmVlZGVkIGdlc3R1cmVzIGZvciBtb2JpbGUuXG4gKi9cbnZhciBHZXN0dXJlTWdyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge0FycmF5LjxPYmplY3Q+fVxuICAgKi9cbiAgdGhpcy5fdHJhY2sgPSBbXTtcbn07XG5cbkdlc3R1cmVNZ3IucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogR2VzdHVyZU1ncixcbiAgcmVjb2duaXplOiBmdW5jdGlvbiAoZXZlbnQsIHRhcmdldCwgcm9vdCkge1xuICAgIHRoaXMuX2RvVHJhY2soZXZlbnQsIHRhcmdldCwgcm9vdCk7XG5cbiAgICByZXR1cm4gdGhpcy5fcmVjb2duaXplKGV2ZW50KTtcbiAgfSxcbiAgY2xlYXI6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLl90cmFjay5sZW5ndGggPSAwO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuICBfZG9UcmFjazogZnVuY3Rpb24gKGV2ZW50LCB0YXJnZXQsIHJvb3QpIHtcbiAgICB2YXIgdG91Y2hlcyA9IGV2ZW50LnRvdWNoZXM7XG5cbiAgICBpZiAoIXRvdWNoZXMpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgdHJhY2tJdGVtID0ge1xuICAgICAgcG9pbnRzOiBbXSxcbiAgICAgIHRvdWNoZXM6IFtdLFxuICAgICAgdGFyZ2V0OiB0YXJnZXQsXG4gICAgICBldmVudDogZXZlbnRcbiAgICB9O1xuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRvdWNoZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIHZhciB0b3VjaCA9IHRvdWNoZXNbaV07XG4gICAgICB2YXIgcG9zID0gZXZlbnRVdGlsLmNsaWVudFRvTG9jYWwocm9vdCwgdG91Y2gsIHt9KTtcbiAgICAgIHRyYWNrSXRlbS5wb2ludHMucHVzaChbcG9zLnpyWCwgcG9zLnpyWV0pO1xuICAgICAgdHJhY2tJdGVtLnRvdWNoZXMucHVzaCh0b3VjaCk7XG4gICAgfVxuXG4gICAgdGhpcy5fdHJhY2sucHVzaCh0cmFja0l0ZW0pO1xuICB9LFxuICBfcmVjb2duaXplOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBmb3IgKHZhciBldmVudE5hbWUgaW4gcmVjb2duaXplcnMpIHtcbiAgICAgIGlmIChyZWNvZ25pemVycy5oYXNPd25Qcm9wZXJ0eShldmVudE5hbWUpKSB7XG4gICAgICAgIHZhciBnZXN0dXJlSW5mbyA9IHJlY29nbml6ZXJzW2V2ZW50TmFtZV0odGhpcy5fdHJhY2ssIGV2ZW50KTtcblxuICAgICAgICBpZiAoZ2VzdHVyZUluZm8pIHtcbiAgICAgICAgICByZXR1cm4gZ2VzdHVyZUluZm87XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGRpc3QocG9pbnRQYWlyKSB7XG4gIHZhciBkeCA9IHBvaW50UGFpclsxXVswXSAtIHBvaW50UGFpclswXVswXTtcbiAgdmFyIGR5ID0gcG9pbnRQYWlyWzFdWzFdIC0gcG9pbnRQYWlyWzBdWzFdO1xuICByZXR1cm4gTWF0aC5zcXJ0KGR4ICogZHggKyBkeSAqIGR5KTtcbn1cblxuZnVuY3Rpb24gY2VudGVyKHBvaW50UGFpcikge1xuICByZXR1cm4gWyhwb2ludFBhaXJbMF1bMF0gKyBwb2ludFBhaXJbMV1bMF0pIC8gMiwgKHBvaW50UGFpclswXVsxXSArIHBvaW50UGFpclsxXVsxXSkgLyAyXTtcbn1cblxudmFyIHJlY29nbml6ZXJzID0ge1xuICBwaW5jaDogZnVuY3Rpb24gKHRyYWNrLCBldmVudCkge1xuICAgIHZhciB0cmFja0xlbiA9IHRyYWNrLmxlbmd0aDtcblxuICAgIGlmICghdHJhY2tMZW4pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcGluY2hFbmQgPSAodHJhY2tbdHJhY2tMZW4gLSAxXSB8fCB7fSkucG9pbnRzO1xuICAgIHZhciBwaW5jaFByZSA9ICh0cmFja1t0cmFja0xlbiAtIDJdIHx8IHt9KS5wb2ludHMgfHwgcGluY2hFbmQ7XG5cbiAgICBpZiAocGluY2hQcmUgJiYgcGluY2hQcmUubGVuZ3RoID4gMSAmJiBwaW5jaEVuZCAmJiBwaW5jaEVuZC5sZW5ndGggPiAxKSB7XG4gICAgICB2YXIgcGluY2hTY2FsZSA9IGRpc3QocGluY2hFbmQpIC8gZGlzdChwaW5jaFByZSk7XG4gICAgICAhaXNGaW5pdGUocGluY2hTY2FsZSkgJiYgKHBpbmNoU2NhbGUgPSAxKTtcbiAgICAgIGV2ZW50LnBpbmNoU2NhbGUgPSBwaW5jaFNjYWxlO1xuICAgICAgdmFyIHBpbmNoQ2VudGVyID0gY2VudGVyKHBpbmNoRW5kKTtcbiAgICAgIGV2ZW50LnBpbmNoWCA9IHBpbmNoQ2VudGVyWzBdO1xuICAgICAgZXZlbnQucGluY2hZID0gcGluY2hDZW50ZXJbMV07XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0eXBlOiAncGluY2gnLFxuICAgICAgICB0YXJnZXQ6IHRyYWNrWzBdLnRhcmdldCxcbiAgICAgICAgZXZlbnQ6IGV2ZW50XG4gICAgICB9O1xuICAgIH1cbiAgfSAvLyBPbmx5IHBpbmNoIGN1cnJlbnRseS5cblxufTtcbnZhciBfZGVmYXVsdCA9IEdlc3R1cmVNZ3I7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7QUFHQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUNBO0FBQ0E7QUE4Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBekJBO0FBMkJBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/GestureMgr.js
