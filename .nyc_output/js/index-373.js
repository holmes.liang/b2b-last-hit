__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BackTop; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/Dom/addEventListener */ "./node_modules/rc-util/es/Dom/addEventListener.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_getScroll__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/getScroll */ "./node_modules/antd/es/_util/getScroll.js");
/* harmony import */ var _util_scrollTo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/scrollTo */ "./node_modules/antd/es/_util/scrollTo.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}










function getDefaultTarget() {
  return window;
}

var BackTop =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BackTop, _React$Component);

  function BackTop(props) {
    var _this;

    _classCallCheck(this, BackTop);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BackTop).call(this, props));

    _this.scrollToTop = function (e) {
      var _this$props = _this.props,
          _this$props$target = _this$props.target,
          target = _this$props$target === void 0 ? getDefaultTarget : _this$props$target,
          onClick = _this$props.onClick;
      Object(_util_scrollTo__WEBPACK_IMPORTED_MODULE_7__["default"])(0, {
        getContainer: target
      });

      if (typeof onClick === 'function') {
        onClick(e);
      }
    };

    _this.handleScroll = function () {
      var _this$props2 = _this.props,
          visibilityHeight = _this$props2.visibilityHeight,
          _this$props2$target = _this$props2.target,
          target = _this$props2$target === void 0 ? getDefaultTarget : _this$props2$target;
      var scrollTop = Object(_util_getScroll__WEBPACK_IMPORTED_MODULE_6__["default"])(target(), true);

      _this.setState({
        visible: scrollTop > visibilityHeight
      });
    };

    _this.renderBackTop = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$props3 = _this.props,
          customizePrefixCls = _this$props3.prefixCls,
          _this$props3$classNam = _this$props3.className,
          className = _this$props3$classNam === void 0 ? '' : _this$props3$classNam,
          children = _this$props3.children;
      var prefixCls = getPrefixCls('back-top', customizePrefixCls);
      var classString = classnames__WEBPACK_IMPORTED_MODULE_3___default()(prefixCls, className);
      var defaultElement = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-content")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-icon")
      })); // fix https://fb.me/react-unknown-prop

      var divProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(_this.props, ['prefixCls', 'className', 'children', 'visibilityHeight', 'target', 'visible']);
      var visible = 'visible' in _this.props ? _this.props.visible : _this.state.visible;
      var backTopBtn = visible ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, divProps, {
        className: classString,
        onClick: _this.scrollToTop
      }), children || defaultElement) : null;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_1__["default"], {
        component: "",
        transitionName: "fade"
      }, backTopBtn);
    };

    _this.state = {
      visible: false
    };
    return _this;
  }

  _createClass(BackTop, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var getTarget = this.props.target || getDefaultTarget;
      this.scrollEvent = Object(rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_2__["default"])(getTarget(), 'scroll', this.handleScroll);
      this.handleScroll();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.scrollEvent) {
        this.scrollEvent.remove();
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderBackTop);
    }
  }]);

  return BackTop;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


BackTop.defaultProps = {
  visibilityHeight: 400
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9iYWNrLXRvcC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYmFjay10b3AvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBbmltYXRlIGZyb20gJ3JjLWFuaW1hdGUnO1xuaW1wb3J0IGFkZEV2ZW50TGlzdGVuZXIgZnJvbSAncmMtdXRpbC9saWIvRG9tL2FkZEV2ZW50TGlzdGVuZXInO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCBnZXRTY3JvbGwgZnJvbSAnLi4vX3V0aWwvZ2V0U2Nyb2xsJztcbmltcG9ydCBzY3JvbGxUbyBmcm9tICcuLi9fdXRpbC9zY3JvbGxUbyc7XG5mdW5jdGlvbiBnZXREZWZhdWx0VGFyZ2V0KCkge1xuICAgIHJldHVybiB3aW5kb3c7XG59XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCYWNrVG9wIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc2Nyb2xsVG9Ub3AgPSAoZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyB0YXJnZXQgPSBnZXREZWZhdWx0VGFyZ2V0LCBvbkNsaWNrIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgc2Nyb2xsVG8oMCwge1xuICAgICAgICAgICAgICAgIGdldENvbnRhaW5lcjogdGFyZ2V0LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAodHlwZW9mIG9uQ2xpY2sgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICBvbkNsaWNrKGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbCA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgdmlzaWJpbGl0eUhlaWdodCwgdGFyZ2V0ID0gZ2V0RGVmYXVsdFRhcmdldCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbFRvcCA9IGdldFNjcm9sbCh0YXJnZXQoKSwgdHJ1ZSk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICB2aXNpYmxlOiBzY3JvbGxUb3AgPiB2aXNpYmlsaXR5SGVpZ2h0LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQmFja1RvcCA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUgPSAnJywgY2hpbGRyZW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2JhY2stdG9wJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGNsYXNzU3RyaW5nID0gY2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSk7XG4gICAgICAgICAgICBjb25zdCBkZWZhdWx0RWxlbWVudCA9ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50YH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWljb25gfS8+XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgLy8gZml4IGh0dHBzOi8vZmIubWUvcmVhY3QtdW5rbm93bi1wcm9wXG4gICAgICAgICAgICBjb25zdCBkaXZQcm9wcyA9IG9taXQodGhpcy5wcm9wcywgW1xuICAgICAgICAgICAgICAgICdwcmVmaXhDbHMnLFxuICAgICAgICAgICAgICAgICdjbGFzc05hbWUnLFxuICAgICAgICAgICAgICAgICdjaGlsZHJlbicsXG4gICAgICAgICAgICAgICAgJ3Zpc2liaWxpdHlIZWlnaHQnLFxuICAgICAgICAgICAgICAgICd0YXJnZXQnLFxuICAgICAgICAgICAgICAgICd2aXNpYmxlJyxcbiAgICAgICAgICAgIF0pO1xuICAgICAgICAgICAgY29uc3QgdmlzaWJsZSA9ICd2aXNpYmxlJyBpbiB0aGlzLnByb3BzID8gdGhpcy5wcm9wcy52aXNpYmxlIDogdGhpcy5zdGF0ZS52aXNpYmxlO1xuICAgICAgICAgICAgY29uc3QgYmFja1RvcEJ0biA9IHZpc2libGUgPyAoPGRpdiB7Li4uZGl2UHJvcHN9IGNsYXNzTmFtZT17Y2xhc3NTdHJpbmd9IG9uQ2xpY2s9e3RoaXMuc2Nyb2xsVG9Ub3B9PlxuICAgICAgICB7Y2hpbGRyZW4gfHwgZGVmYXVsdEVsZW1lbnR9XG4gICAgICA8L2Rpdj4pIDogbnVsbDtcbiAgICAgICAgICAgIHJldHVybiAoPEFuaW1hdGUgY29tcG9uZW50PVwiXCIgdHJhbnNpdGlvbk5hbWU9XCJmYWRlXCI+XG4gICAgICAgIHtiYWNrVG9wQnRufVxuICAgICAgPC9BbmltYXRlPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICB2aXNpYmxlOiBmYWxzZSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIGNvbnN0IGdldFRhcmdldCA9IHRoaXMucHJvcHMudGFyZ2V0IHx8IGdldERlZmF1bHRUYXJnZXQ7XG4gICAgICAgIHRoaXMuc2Nyb2xsRXZlbnQgPSBhZGRFdmVudExpc3RlbmVyKGdldFRhcmdldCgpLCAnc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbCgpO1xuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgaWYgKHRoaXMuc2Nyb2xsRXZlbnQpIHtcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsRXZlbnQucmVtb3ZlKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckJhY2tUb3B9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQmFja1RvcC5kZWZhdWx0UHJvcHMgPSB7XG4gICAgdmlzaWJpbGl0eUhlaWdodDogNDAwLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSEE7QUFDQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBUUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBcEJBO0FBQ0E7QUF1QkE7QUFDQTtBQURBO0FBMUNBO0FBNkNBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7OztBQTNEQTtBQUNBO0FBREE7QUE2REE7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/back-top/index.js
