/**
 * echarts设备环境识别
 *
 * @desc echarts基于Canvas，纯Javascript图表库，提供直观，生动，可交互，可个性化定制的数据统计图表。
 * @author firede[firede@firede.us]
 * @desc thanks zepto.
 */
var env = {};

if (typeof wx === 'object' && typeof wx.getSystemInfoSync === 'function') {
  // In Weixin Application
  env = {
    browser: {},
    os: {},
    node: false,
    wxa: true,
    // Weixin Application
    canvasSupported: true,
    svgSupported: false,
    touchEventsSupported: true,
    domSupported: false
  };
} else if (typeof document === 'undefined' && typeof self !== 'undefined') {
  // In worker
  env = {
    browser: {},
    os: {},
    node: false,
    worker: true,
    canvasSupported: true,
    domSupported: false
  };
} else if (typeof navigator === 'undefined') {
  // In node
  env = {
    browser: {},
    os: {},
    node: true,
    worker: false,
    // Assume canvas is supported
    canvasSupported: true,
    svgSupported: true,
    domSupported: false
  };
} else {
  env = detect(navigator.userAgent);
}

var _default = env; // Zepto.js
// (c) 2010-2013 Thomas Fuchs
// Zepto.js may be freely distributed under the MIT license.

function detect(ua) {
  var os = {};
  var browser = {}; // var webkit = ua.match(/Web[kK]it[\/]{0,1}([\d.]+)/);
  // var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
  // var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
  // var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
  // var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
  // var webos = ua.match(/(webOS|hpwOS)[\s\/]([\d.]+)/);
  // var touchpad = webos && ua.match(/TouchPad/);
  // var kindle = ua.match(/Kindle\/([\d.]+)/);
  // var silk = ua.match(/Silk\/([\d._]+)/);
  // var blackberry = ua.match(/(BlackBerry).*Version\/([\d.]+)/);
  // var bb10 = ua.match(/(BB10).*Version\/([\d.]+)/);
  // var rimtabletos = ua.match(/(RIM\sTablet\sOS)\s([\d.]+)/);
  // var playbook = ua.match(/PlayBook/);
  // var chrome = ua.match(/Chrome\/([\d.]+)/) || ua.match(/CriOS\/([\d.]+)/);

  var firefox = ua.match(/Firefox\/([\d.]+)/); // var safari = webkit && ua.match(/Mobile\//) && !chrome;
  // var webview = ua.match(/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/) && !chrome;

  var ie = ua.match(/MSIE\s([\d.]+)/) // IE 11 Trident/7.0; rv:11.0
  || ua.match(/Trident\/.+?rv:(([\d.]+))/);
  var edge = ua.match(/Edge\/([\d.]+)/); // IE 12 and 12+

  var weChat = /micromessenger/i.test(ua); // Todo: clean this up with a better OS/browser seperation:
  // - discern (more) between multiple browsers on android
  // - decide if kindle fire in silk mode is android or not
  // - Firefox on Android doesn't specify the Android version
  // - possibly devide in os, device and browser hashes
  // if (browser.webkit = !!webkit) browser.version = webkit[1];
  // if (android) os.android = true, os.version = android[2];
  // if (iphone && !ipod) os.ios = os.iphone = true, os.version = iphone[2].replace(/_/g, '.');
  // if (ipad) os.ios = os.ipad = true, os.version = ipad[2].replace(/_/g, '.');
  // if (ipod) os.ios = os.ipod = true, os.version = ipod[3] ? ipod[3].replace(/_/g, '.') : null;
  // if (webos) os.webos = true, os.version = webos[2];
  // if (touchpad) os.touchpad = true;
  // if (blackberry) os.blackberry = true, os.version = blackberry[2];
  // if (bb10) os.bb10 = true, os.version = bb10[2];
  // if (rimtabletos) os.rimtabletos = true, os.version = rimtabletos[2];
  // if (playbook) browser.playbook = true;
  // if (kindle) os.kindle = true, os.version = kindle[1];
  // if (silk) browser.silk = true, browser.version = silk[1];
  // if (!silk && os.android && ua.match(/Kindle Fire/)) browser.silk = true;
  // if (chrome) browser.chrome = true, browser.version = chrome[1];

  if (firefox) {
    browser.firefox = true;
    browser.version = firefox[1];
  } // if (safari && (ua.match(/Safari/) || !!os.ios)) browser.safari = true;
  // if (webview) browser.webview = true;


  if (ie) {
    browser.ie = true;
    browser.version = ie[1];
  }

  if (edge) {
    browser.edge = true;
    browser.version = edge[1];
  } // It is difficult to detect WeChat in Win Phone precisely, because ua can
  // not be set on win phone. So we do not consider Win Phone.


  if (weChat) {
    browser.weChat = true;
  } // os.tablet = !!(ipad || playbook || (android && !ua.match(/Mobile/)) ||
  //     (firefox && ua.match(/Tablet/)) || (ie && !ua.match(/Phone/) && ua.match(/Touch/)));
  // os.phone  = !!(!os.tablet && !os.ipod && (android || iphone || webos ||
  //     (chrome && ua.match(/Android/)) || (chrome && ua.match(/CriOS\/([\d.]+)/)) ||
  //     (firefox && ua.match(/Mobile/)) || (ie && ua.match(/Touch/))));


  return {
    browser: browser,
    os: os,
    node: false,
    // 原生canvas支持，改极端点了
    // canvasSupported : !(browser.ie && parseFloat(browser.version) < 9)
    canvasSupported: !!document.createElement('canvas').getContext,
    svgSupported: typeof SVGRect !== 'undefined',
    // works on most browsers
    // IE10/11 does not support touch event, and MS Edge supports them but not by
    // default, so we dont check navigator.maxTouchPoints for them here.
    touchEventsSupported: 'ontouchstart' in window && !browser.ie && !browser.edge,
    // <http://caniuse.com/#search=pointer%20event>.
    pointerEventsSupported: 'onpointerdown' in window // Firefox supports pointer but not by default, only MS browsers are reliable on pointer
    // events currently. So we dont use that on other browsers unless tested sufficiently.
    // Although IE 10 supports pointer event, it use old style and is different from the
    // standard. So we exclude that. (IE 10 is hardly used on touch device)
    && (browser.edge || browser.ie && browser.version >= 11),
    // passiveSupported: detectPassiveSupport()
    domSupported: typeof document !== 'undefined'
  };
} // See https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md#feature-detection
// function detectPassiveSupport() {
//     // Test via a getter in the options object to see if the passive property is accessed
//     var supportsPassive = false;
//     try {
//         var opts = Object.defineProperty({}, 'passive', {
//             get: function() {
//                 supportsPassive = true;
//             }
//         });
//         window.addEventListener('testPassive', function() {}, opts);
//     } catch (e) {
//     }
//     return supportsPassive;
// }


module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9lbnYuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL2Vudi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIGVjaGFydHPorr7lpIfnjq/looPor4bliKtcbiAqXG4gKiBAZGVzYyBlY2hhcnRz5Z+65LqOQ2FudmFz77yM57qvSmF2YXNjcmlwdOWbvuihqOW6k++8jOaPkOS+m+ebtOingu+8jOeUn+WKqO+8jOWPr+S6pOS6ku+8jOWPr+S4quaAp+WMluWumuWItueahOaVsOaNrue7n+iuoeWbvuihqOOAglxuICogQGF1dGhvciBmaXJlZGVbZmlyZWRlQGZpcmVkZS51c11cbiAqIEBkZXNjIHRoYW5rcyB6ZXB0by5cbiAqL1xudmFyIGVudiA9IHt9O1xuXG5pZiAodHlwZW9mIHd4ID09PSAnb2JqZWN0JyAmJiB0eXBlb2Ygd3guZ2V0U3lzdGVtSW5mb1N5bmMgPT09ICdmdW5jdGlvbicpIHtcbiAgLy8gSW4gV2VpeGluIEFwcGxpY2F0aW9uXG4gIGVudiA9IHtcbiAgICBicm93c2VyOiB7fSxcbiAgICBvczoge30sXG4gICAgbm9kZTogZmFsc2UsXG4gICAgd3hhOiB0cnVlLFxuICAgIC8vIFdlaXhpbiBBcHBsaWNhdGlvblxuICAgIGNhbnZhc1N1cHBvcnRlZDogdHJ1ZSxcbiAgICBzdmdTdXBwb3J0ZWQ6IGZhbHNlLFxuICAgIHRvdWNoRXZlbnRzU3VwcG9ydGVkOiB0cnVlLFxuICAgIGRvbVN1cHBvcnRlZDogZmFsc2VcbiAgfTtcbn0gZWxzZSBpZiAodHlwZW9mIGRvY3VtZW50ID09PSAndW5kZWZpbmVkJyAmJiB0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgLy8gSW4gd29ya2VyXG4gIGVudiA9IHtcbiAgICBicm93c2VyOiB7fSxcbiAgICBvczoge30sXG4gICAgbm9kZTogZmFsc2UsXG4gICAgd29ya2VyOiB0cnVlLFxuICAgIGNhbnZhc1N1cHBvcnRlZDogdHJ1ZSxcbiAgICBkb21TdXBwb3J0ZWQ6IGZhbHNlXG4gIH07XG59IGVsc2UgaWYgKHR5cGVvZiBuYXZpZ2F0b3IgPT09ICd1bmRlZmluZWQnKSB7XG4gIC8vIEluIG5vZGVcbiAgZW52ID0ge1xuICAgIGJyb3dzZXI6IHt9LFxuICAgIG9zOiB7fSxcbiAgICBub2RlOiB0cnVlLFxuICAgIHdvcmtlcjogZmFsc2UsXG4gICAgLy8gQXNzdW1lIGNhbnZhcyBpcyBzdXBwb3J0ZWRcbiAgICBjYW52YXNTdXBwb3J0ZWQ6IHRydWUsXG4gICAgc3ZnU3VwcG9ydGVkOiB0cnVlLFxuICAgIGRvbVN1cHBvcnRlZDogZmFsc2VcbiAgfTtcbn0gZWxzZSB7XG4gIGVudiA9IGRldGVjdChuYXZpZ2F0b3IudXNlckFnZW50KTtcbn1cblxudmFyIF9kZWZhdWx0ID0gZW52OyAvLyBaZXB0by5qc1xuLy8gKGMpIDIwMTAtMjAxMyBUaG9tYXMgRnVjaHNcbi8vIFplcHRvLmpzIG1heSBiZSBmcmVlbHkgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlLlxuXG5mdW5jdGlvbiBkZXRlY3QodWEpIHtcbiAgdmFyIG9zID0ge307XG4gIHZhciBicm93c2VyID0ge307IC8vIHZhciB3ZWJraXQgPSB1YS5tYXRjaCgvV2ViW2tLXWl0W1xcL117MCwxfShbXFxkLl0rKS8pO1xuICAvLyB2YXIgYW5kcm9pZCA9IHVhLm1hdGNoKC8oQW5kcm9pZCk7P1tcXHNcXC9dKyhbXFxkLl0rKT8vKTtcbiAgLy8gdmFyIGlwYWQgPSB1YS5tYXRjaCgvKGlQYWQpLipPU1xccyhbXFxkX10rKS8pO1xuICAvLyB2YXIgaXBvZCA9IHVhLm1hdGNoKC8oaVBvZCkoLipPU1xccyhbXFxkX10rKSk/Lyk7XG4gIC8vIHZhciBpcGhvbmUgPSAhaXBhZCAmJiB1YS5tYXRjaCgvKGlQaG9uZVxcc09TKVxccyhbXFxkX10rKS8pO1xuICAvLyB2YXIgd2Vib3MgPSB1YS5tYXRjaCgvKHdlYk9TfGhwd09TKVtcXHNcXC9dKFtcXGQuXSspLyk7XG4gIC8vIHZhciB0b3VjaHBhZCA9IHdlYm9zICYmIHVhLm1hdGNoKC9Ub3VjaFBhZC8pO1xuICAvLyB2YXIga2luZGxlID0gdWEubWF0Y2goL0tpbmRsZVxcLyhbXFxkLl0rKS8pO1xuICAvLyB2YXIgc2lsayA9IHVhLm1hdGNoKC9TaWxrXFwvKFtcXGQuX10rKS8pO1xuICAvLyB2YXIgYmxhY2tiZXJyeSA9IHVhLm1hdGNoKC8oQmxhY2tCZXJyeSkuKlZlcnNpb25cXC8oW1xcZC5dKykvKTtcbiAgLy8gdmFyIGJiMTAgPSB1YS5tYXRjaCgvKEJCMTApLipWZXJzaW9uXFwvKFtcXGQuXSspLyk7XG4gIC8vIHZhciByaW10YWJsZXRvcyA9IHVhLm1hdGNoKC8oUklNXFxzVGFibGV0XFxzT1MpXFxzKFtcXGQuXSspLyk7XG4gIC8vIHZhciBwbGF5Ym9vayA9IHVhLm1hdGNoKC9QbGF5Qm9vay8pO1xuICAvLyB2YXIgY2hyb21lID0gdWEubWF0Y2goL0Nocm9tZVxcLyhbXFxkLl0rKS8pIHx8IHVhLm1hdGNoKC9DcmlPU1xcLyhbXFxkLl0rKS8pO1xuXG4gIHZhciBmaXJlZm94ID0gdWEubWF0Y2goL0ZpcmVmb3hcXC8oW1xcZC5dKykvKTsgLy8gdmFyIHNhZmFyaSA9IHdlYmtpdCAmJiB1YS5tYXRjaCgvTW9iaWxlXFwvLykgJiYgIWNocm9tZTtcbiAgLy8gdmFyIHdlYnZpZXcgPSB1YS5tYXRjaCgvKGlQaG9uZXxpUG9kfGlQYWQpLipBcHBsZVdlYktpdCg/IS4qU2FmYXJpKS8pICYmICFjaHJvbWU7XG5cbiAgdmFyIGllID0gdWEubWF0Y2goL01TSUVcXHMoW1xcZC5dKykvKSAvLyBJRSAxMSBUcmlkZW50LzcuMDsgcnY6MTEuMFxuICB8fCB1YS5tYXRjaCgvVHJpZGVudFxcLy4rP3J2OigoW1xcZC5dKykpLyk7XG4gIHZhciBlZGdlID0gdWEubWF0Y2goL0VkZ2VcXC8oW1xcZC5dKykvKTsgLy8gSUUgMTIgYW5kIDEyK1xuXG4gIHZhciB3ZUNoYXQgPSAvbWljcm9tZXNzZW5nZXIvaS50ZXN0KHVhKTsgLy8gVG9kbzogY2xlYW4gdGhpcyB1cCB3aXRoIGEgYmV0dGVyIE9TL2Jyb3dzZXIgc2VwZXJhdGlvbjpcbiAgLy8gLSBkaXNjZXJuIChtb3JlKSBiZXR3ZWVuIG11bHRpcGxlIGJyb3dzZXJzIG9uIGFuZHJvaWRcbiAgLy8gLSBkZWNpZGUgaWYga2luZGxlIGZpcmUgaW4gc2lsayBtb2RlIGlzIGFuZHJvaWQgb3Igbm90XG4gIC8vIC0gRmlyZWZveCBvbiBBbmRyb2lkIGRvZXNuJ3Qgc3BlY2lmeSB0aGUgQW5kcm9pZCB2ZXJzaW9uXG4gIC8vIC0gcG9zc2libHkgZGV2aWRlIGluIG9zLCBkZXZpY2UgYW5kIGJyb3dzZXIgaGFzaGVzXG4gIC8vIGlmIChicm93c2VyLndlYmtpdCA9ICEhd2Via2l0KSBicm93c2VyLnZlcnNpb24gPSB3ZWJraXRbMV07XG4gIC8vIGlmIChhbmRyb2lkKSBvcy5hbmRyb2lkID0gdHJ1ZSwgb3MudmVyc2lvbiA9IGFuZHJvaWRbMl07XG4gIC8vIGlmIChpcGhvbmUgJiYgIWlwb2QpIG9zLmlvcyA9IG9zLmlwaG9uZSA9IHRydWUsIG9zLnZlcnNpb24gPSBpcGhvbmVbMl0ucmVwbGFjZSgvXy9nLCAnLicpO1xuICAvLyBpZiAoaXBhZCkgb3MuaW9zID0gb3MuaXBhZCA9IHRydWUsIG9zLnZlcnNpb24gPSBpcGFkWzJdLnJlcGxhY2UoL18vZywgJy4nKTtcbiAgLy8gaWYgKGlwb2QpIG9zLmlvcyA9IG9zLmlwb2QgPSB0cnVlLCBvcy52ZXJzaW9uID0gaXBvZFszXSA/IGlwb2RbM10ucmVwbGFjZSgvXy9nLCAnLicpIDogbnVsbDtcbiAgLy8gaWYgKHdlYm9zKSBvcy53ZWJvcyA9IHRydWUsIG9zLnZlcnNpb24gPSB3ZWJvc1syXTtcbiAgLy8gaWYgKHRvdWNocGFkKSBvcy50b3VjaHBhZCA9IHRydWU7XG4gIC8vIGlmIChibGFja2JlcnJ5KSBvcy5ibGFja2JlcnJ5ID0gdHJ1ZSwgb3MudmVyc2lvbiA9IGJsYWNrYmVycnlbMl07XG4gIC8vIGlmIChiYjEwKSBvcy5iYjEwID0gdHJ1ZSwgb3MudmVyc2lvbiA9IGJiMTBbMl07XG4gIC8vIGlmIChyaW10YWJsZXRvcykgb3MucmltdGFibGV0b3MgPSB0cnVlLCBvcy52ZXJzaW9uID0gcmltdGFibGV0b3NbMl07XG4gIC8vIGlmIChwbGF5Ym9vaykgYnJvd3Nlci5wbGF5Ym9vayA9IHRydWU7XG4gIC8vIGlmIChraW5kbGUpIG9zLmtpbmRsZSA9IHRydWUsIG9zLnZlcnNpb24gPSBraW5kbGVbMV07XG4gIC8vIGlmIChzaWxrKSBicm93c2VyLnNpbGsgPSB0cnVlLCBicm93c2VyLnZlcnNpb24gPSBzaWxrWzFdO1xuICAvLyBpZiAoIXNpbGsgJiYgb3MuYW5kcm9pZCAmJiB1YS5tYXRjaCgvS2luZGxlIEZpcmUvKSkgYnJvd3Nlci5zaWxrID0gdHJ1ZTtcbiAgLy8gaWYgKGNocm9tZSkgYnJvd3Nlci5jaHJvbWUgPSB0cnVlLCBicm93c2VyLnZlcnNpb24gPSBjaHJvbWVbMV07XG5cbiAgaWYgKGZpcmVmb3gpIHtcbiAgICBicm93c2VyLmZpcmVmb3ggPSB0cnVlO1xuICAgIGJyb3dzZXIudmVyc2lvbiA9IGZpcmVmb3hbMV07XG4gIH0gLy8gaWYgKHNhZmFyaSAmJiAodWEubWF0Y2goL1NhZmFyaS8pIHx8ICEhb3MuaW9zKSkgYnJvd3Nlci5zYWZhcmkgPSB0cnVlO1xuICAvLyBpZiAod2VidmlldykgYnJvd3Nlci53ZWJ2aWV3ID0gdHJ1ZTtcblxuXG4gIGlmIChpZSkge1xuICAgIGJyb3dzZXIuaWUgPSB0cnVlO1xuICAgIGJyb3dzZXIudmVyc2lvbiA9IGllWzFdO1xuICB9XG5cbiAgaWYgKGVkZ2UpIHtcbiAgICBicm93c2VyLmVkZ2UgPSB0cnVlO1xuICAgIGJyb3dzZXIudmVyc2lvbiA9IGVkZ2VbMV07XG4gIH0gLy8gSXQgaXMgZGlmZmljdWx0IHRvIGRldGVjdCBXZUNoYXQgaW4gV2luIFBob25lIHByZWNpc2VseSwgYmVjYXVzZSB1YSBjYW5cbiAgLy8gbm90IGJlIHNldCBvbiB3aW4gcGhvbmUuIFNvIHdlIGRvIG5vdCBjb25zaWRlciBXaW4gUGhvbmUuXG5cblxuICBpZiAod2VDaGF0KSB7XG4gICAgYnJvd3Nlci53ZUNoYXQgPSB0cnVlO1xuICB9IC8vIG9zLnRhYmxldCA9ICEhKGlwYWQgfHwgcGxheWJvb2sgfHwgKGFuZHJvaWQgJiYgIXVhLm1hdGNoKC9Nb2JpbGUvKSkgfHxcbiAgLy8gICAgIChmaXJlZm94ICYmIHVhLm1hdGNoKC9UYWJsZXQvKSkgfHwgKGllICYmICF1YS5tYXRjaCgvUGhvbmUvKSAmJiB1YS5tYXRjaCgvVG91Y2gvKSkpO1xuICAvLyBvcy5waG9uZSAgPSAhISghb3MudGFibGV0ICYmICFvcy5pcG9kICYmIChhbmRyb2lkIHx8IGlwaG9uZSB8fCB3ZWJvcyB8fFxuICAvLyAgICAgKGNocm9tZSAmJiB1YS5tYXRjaCgvQW5kcm9pZC8pKSB8fCAoY2hyb21lICYmIHVhLm1hdGNoKC9DcmlPU1xcLyhbXFxkLl0rKS8pKSB8fFxuICAvLyAgICAgKGZpcmVmb3ggJiYgdWEubWF0Y2goL01vYmlsZS8pKSB8fCAoaWUgJiYgdWEubWF0Y2goL1RvdWNoLykpKSk7XG5cblxuICByZXR1cm4ge1xuICAgIGJyb3dzZXI6IGJyb3dzZXIsXG4gICAgb3M6IG9zLFxuICAgIG5vZGU6IGZhbHNlLFxuICAgIC8vIOWOn+eUn2NhbnZhc+aUr+aMge+8jOaUueaegeerr+eCueS6hlxuICAgIC8vIGNhbnZhc1N1cHBvcnRlZCA6ICEoYnJvd3Nlci5pZSAmJiBwYXJzZUZsb2F0KGJyb3dzZXIudmVyc2lvbikgPCA5KVxuICAgIGNhbnZhc1N1cHBvcnRlZDogISFkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKS5nZXRDb250ZXh0LFxuICAgIHN2Z1N1cHBvcnRlZDogdHlwZW9mIFNWR1JlY3QgIT09ICd1bmRlZmluZWQnLFxuICAgIC8vIHdvcmtzIG9uIG1vc3QgYnJvd3NlcnNcbiAgICAvLyBJRTEwLzExIGRvZXMgbm90IHN1cHBvcnQgdG91Y2ggZXZlbnQsIGFuZCBNUyBFZGdlIHN1cHBvcnRzIHRoZW0gYnV0IG5vdCBieVxuICAgIC8vIGRlZmF1bHQsIHNvIHdlIGRvbnQgY2hlY2sgbmF2aWdhdG9yLm1heFRvdWNoUG9pbnRzIGZvciB0aGVtIGhlcmUuXG4gICAgdG91Y2hFdmVudHNTdXBwb3J0ZWQ6ICdvbnRvdWNoc3RhcnQnIGluIHdpbmRvdyAmJiAhYnJvd3Nlci5pZSAmJiAhYnJvd3Nlci5lZGdlLFxuICAgIC8vIDxodHRwOi8vY2FuaXVzZS5jb20vI3NlYXJjaD1wb2ludGVyJTIwZXZlbnQ+LlxuICAgIHBvaW50ZXJFdmVudHNTdXBwb3J0ZWQ6ICdvbnBvaW50ZXJkb3duJyBpbiB3aW5kb3cgLy8gRmlyZWZveCBzdXBwb3J0cyBwb2ludGVyIGJ1dCBub3QgYnkgZGVmYXVsdCwgb25seSBNUyBicm93c2VycyBhcmUgcmVsaWFibGUgb24gcG9pbnRlclxuICAgIC8vIGV2ZW50cyBjdXJyZW50bHkuIFNvIHdlIGRvbnQgdXNlIHRoYXQgb24gb3RoZXIgYnJvd3NlcnMgdW5sZXNzIHRlc3RlZCBzdWZmaWNpZW50bHkuXG4gICAgLy8gQWx0aG91Z2ggSUUgMTAgc3VwcG9ydHMgcG9pbnRlciBldmVudCwgaXQgdXNlIG9sZCBzdHlsZSBhbmQgaXMgZGlmZmVyZW50IGZyb20gdGhlXG4gICAgLy8gc3RhbmRhcmQuIFNvIHdlIGV4Y2x1ZGUgdGhhdC4gKElFIDEwIGlzIGhhcmRseSB1c2VkIG9uIHRvdWNoIGRldmljZSlcbiAgICAmJiAoYnJvd3Nlci5lZGdlIHx8IGJyb3dzZXIuaWUgJiYgYnJvd3Nlci52ZXJzaW9uID49IDExKSxcbiAgICAvLyBwYXNzaXZlU3VwcG9ydGVkOiBkZXRlY3RQYXNzaXZlU3VwcG9ydCgpXG4gICAgZG9tU3VwcG9ydGVkOiB0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnXG4gIH07XG59IC8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vV0lDRy9FdmVudExpc3RlbmVyT3B0aW9ucy9ibG9iL2doLXBhZ2VzL2V4cGxhaW5lci5tZCNmZWF0dXJlLWRldGVjdGlvblxuLy8gZnVuY3Rpb24gZGV0ZWN0UGFzc2l2ZVN1cHBvcnQoKSB7XG4vLyAgICAgLy8gVGVzdCB2aWEgYSBnZXR0ZXIgaW4gdGhlIG9wdGlvbnMgb2JqZWN0IHRvIHNlZSBpZiB0aGUgcGFzc2l2ZSBwcm9wZXJ0eSBpcyBhY2Nlc3NlZFxuLy8gICAgIHZhciBzdXBwb3J0c1Bhc3NpdmUgPSBmYWxzZTtcbi8vICAgICB0cnkge1xuLy8gICAgICAgICB2YXIgb3B0cyA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh7fSwgJ3Bhc3NpdmUnLCB7XG4vLyAgICAgICAgICAgICBnZXQ6IGZ1bmN0aW9uKCkge1xuLy8gICAgICAgICAgICAgICAgIHN1cHBvcnRzUGFzc2l2ZSA9IHRydWU7XG4vLyAgICAgICAgICAgICB9XG4vLyAgICAgICAgIH0pO1xuLy8gICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndGVzdFBhc3NpdmUnLCBmdW5jdGlvbigpIHt9LCBvcHRzKTtcbi8vICAgICB9IGNhdGNoIChlKSB7XG4vLyAgICAgfVxuLy8gICAgIHJldHVybiBzdXBwb3J0c1Bhc3NpdmU7XG4vLyB9XG5cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBbkJBO0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/env.js
