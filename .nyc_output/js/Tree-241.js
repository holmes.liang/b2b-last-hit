__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _contextTypes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contextTypes */ "./node_modules/rc-tree/es/contextTypes.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree/es/util.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}










var Tree =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tree, _React$Component);

  function Tree() {
    var _this;

    _classCallCheck(this, Tree);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tree).apply(this, arguments));
    /** Internal usage for `rc-tree-select`, we don't promise it will not change. */

    _this.domTreeNodes = {};
    _this.state = {
      keyEntities: {},
      selectedKeys: [],
      checkedKeys: [],
      halfCheckedKeys: [],
      loadedKeys: [],
      loadingKeys: [],
      expandedKeys: [],
      dragNodesKeys: [],
      dragOverNodeKey: null,
      dropPosition: null,
      treeNode: [],
      prevProps: null
    };

    _this.onNodeDragStart = function (event, node) {
      var expandedKeys = _this.state.expandedKeys;
      var onDragStart = _this.props.onDragStart;
      var _node$props = node.props,
          eventKey = _node$props.eventKey,
          children = _node$props.children;
      _this.dragNode = node;

      _this.setState({
        dragNodesKeys: Object(_util__WEBPACK_IMPORTED_MODULE_7__["getDragNodesKeys"])(children, node),
        expandedKeys: Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(expandedKeys, eventKey)
      });

      if (onDragStart) {
        onDragStart({
          event: event,
          node: node
        });
      }
    };
    /**
     * [Legacy] Select handler is less small than node,
     * so that this will trigger when drag enter node or select handler.
     * This is a little tricky if customize css without padding.
     * Better for use mouse move event to refresh drag state.
     * But let's just keep it to avoid event trigger logic change.
     */


    _this.onNodeDragEnter = function (event, node) {
      var expandedKeys = _this.state.expandedKeys;
      var onDragEnter = _this.props.onDragEnter;
      var _node$props2 = node.props,
          pos = _node$props2.pos,
          eventKey = _node$props2.eventKey;
      if (!_this.dragNode) return;
      var dropPosition = Object(_util__WEBPACK_IMPORTED_MODULE_7__["calcDropPosition"])(event, node); // Skip if drag node is self

      if (_this.dragNode.props.eventKey === eventKey && dropPosition === 0) {
        _this.setState({
          dragOverNodeKey: '',
          dropPosition: null
        });

        return;
      } // Ref: https://github.com/react-component/tree/issues/132
      // Add timeout to let onDragLevel fire before onDragEnter,
      // so that we can clean drag props for onDragLeave node.
      // Macro task for this:
      // https://html.spec.whatwg.org/multipage/webappapis.html#clean-up-after-running-script


      setTimeout(function () {
        // Update drag over node
        _this.setState({
          dragOverNodeKey: eventKey,
          dropPosition: dropPosition
        }); // Side effect for delay drag


        if (!_this.delayedDragEnterLogic) {
          _this.delayedDragEnterLogic = {};
        }

        Object.keys(_this.delayedDragEnterLogic).forEach(function (key) {
          clearTimeout(_this.delayedDragEnterLogic[key]);
        });
        _this.delayedDragEnterLogic[pos] = window.setTimeout(function () {
          var newExpandedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(expandedKeys, eventKey);

          if (!('expandedKeys' in _this.props)) {
            _this.setState({
              expandedKeys: newExpandedKeys
            });
          }

          if (onDragEnter) {
            onDragEnter({
              event: event,
              node: node,
              expandedKeys: newExpandedKeys
            });
          }
        }, 400);
      }, 0);
    };

    _this.onNodeDragOver = function (event, node) {
      var onDragOver = _this.props.onDragOver;
      var eventKey = node.props.eventKey; // Update drag position

      if (_this.dragNode && eventKey === _this.state.dragOverNodeKey) {
        var dropPosition = Object(_util__WEBPACK_IMPORTED_MODULE_7__["calcDropPosition"])(event, node);
        if (dropPosition === _this.state.dropPosition) return;

        _this.setState({
          dropPosition: dropPosition
        });
      }

      if (onDragOver) {
        onDragOver({
          event: event,
          node: node
        });
      }
    };

    _this.onNodeDragLeave = function (event, node) {
      var onDragLeave = _this.props.onDragLeave;

      _this.setState({
        dragOverNodeKey: ''
      });

      if (onDragLeave) {
        onDragLeave({
          event: event,
          node: node
        });
      }
    };

    _this.onNodeDragEnd = function (event, node) {
      var onDragEnd = _this.props.onDragEnd;

      _this.setState({
        dragOverNodeKey: ''
      });

      if (onDragEnd) {
        onDragEnd({
          event: event,
          node: node
        });
      }

      _this.dragNode = null;
    };

    _this.onNodeDrop = function (event, node) {
      var _this$state = _this.state,
          _this$state$dragNodes = _this$state.dragNodesKeys,
          dragNodesKeys = _this$state$dragNodes === void 0 ? [] : _this$state$dragNodes,
          dropPosition = _this$state.dropPosition;
      var onDrop = _this.props.onDrop;
      var _node$props3 = node.props,
          eventKey = _node$props3.eventKey,
          pos = _node$props3.pos;

      _this.setState({
        dragOverNodeKey: ''
      });

      if (dragNodesKeys.indexOf(eventKey) !== -1) {
        warning__WEBPACK_IMPORTED_MODULE_3___default()(false, "Can not drop to dragNode(include it's children node)");
        return;
      }

      var posArr = Object(_util__WEBPACK_IMPORTED_MODULE_7__["posToArr"])(pos);
      var dropResult = {
        event: event,
        node: node,
        dragNode: _this.dragNode,
        dragNodesKeys: dragNodesKeys.slice(),
        dropPosition: dropPosition + Number(posArr[posArr.length - 1]),
        dropToGap: false
      };

      if (dropPosition !== 0) {
        dropResult.dropToGap = true;
      }

      if (onDrop) {
        onDrop(dropResult);
      }

      _this.dragNode = null;
    };

    _this.onNodeClick = function (e, treeNode) {
      var onClick = _this.props.onClick;

      if (onClick) {
        onClick(e, treeNode);
      }
    };

    _this.onNodeDoubleClick = function (e, treeNode) {
      var onDoubleClick = _this.props.onDoubleClick;

      if (onDoubleClick) {
        onDoubleClick(e, treeNode);
      }
    };

    _this.onNodeSelect = function (e, treeNode) {
      var selectedKeys = _this.state.selectedKeys;
      var keyEntities = _this.state.keyEntities;
      var _this$props = _this.props,
          onSelect = _this$props.onSelect,
          multiple = _this$props.multiple;
      var _treeNode$props = treeNode.props,
          selected = _treeNode$props.selected,
          eventKey = _treeNode$props.eventKey;
      var targetSelected = !selected; // Update selected keys

      if (!targetSelected) {
        selectedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(selectedKeys, eventKey);
      } else if (!multiple) {
        selectedKeys = [eventKey];
      } else {
        selectedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(selectedKeys, eventKey);
      } // [Legacy] Not found related usage in doc or upper libs


      var selectedNodes = selectedKeys.map(function (key) {
        var entity = keyEntities[key];
        if (!entity) return null;
        return entity.node;
      }).filter(function (node) {
        return node;
      });

      _this.setUncontrolledState({
        selectedKeys: selectedKeys
      });

      if (onSelect) {
        onSelect(selectedKeys, {
          event: 'select',
          selected: targetSelected,
          node: treeNode,
          selectedNodes: selectedNodes,
          nativeEvent: e.nativeEvent
        });
      }
    };

    _this.onNodeCheck = function (e, treeNode, checked) {
      var _this$state2 = _this.state,
          keyEntities = _this$state2.keyEntities,
          oriCheckedKeys = _this$state2.checkedKeys,
          oriHalfCheckedKeys = _this$state2.halfCheckedKeys;
      var _this$props2 = _this.props,
          checkStrictly = _this$props2.checkStrictly,
          onCheck = _this$props2.onCheck;
      var eventKey = treeNode.props.eventKey; // Prepare trigger arguments

      var checkedObj;
      var eventObj = {
        event: 'check',
        node: treeNode,
        checked: checked,
        nativeEvent: e.nativeEvent
      };

      if (checkStrictly) {
        var checkedKeys = checked ? Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(oriCheckedKeys, eventKey) : Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(oriCheckedKeys, eventKey);
        var halfCheckedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(oriHalfCheckedKeys, eventKey);
        checkedObj = {
          checked: checkedKeys,
          halfChecked: halfCheckedKeys
        };
        eventObj.checkedNodes = checkedKeys.map(function (key) {
          return keyEntities[key];
        }).filter(function (entity) {
          return entity;
        }).map(function (entity) {
          return entity.node;
        });

        _this.setUncontrolledState({
          checkedKeys: checkedKeys
        });
      } else {
        var _conductCheck = Object(_util__WEBPACK_IMPORTED_MODULE_7__["conductCheck"])([eventKey], checked, keyEntities, {
          checkedKeys: oriCheckedKeys,
          halfCheckedKeys: oriHalfCheckedKeys
        }),
            _checkedKeys = _conductCheck.checkedKeys,
            _halfCheckedKeys = _conductCheck.halfCheckedKeys;

        checkedObj = _checkedKeys; // [Legacy] This is used for `rc-tree-select`

        eventObj.checkedNodes = [];
        eventObj.checkedNodesPositions = [];
        eventObj.halfCheckedKeys = _halfCheckedKeys;

        _checkedKeys.forEach(function (key) {
          var entity = keyEntities[key];
          if (!entity) return;
          var node = entity.node,
              pos = entity.pos;
          eventObj.checkedNodes.push(node);
          eventObj.checkedNodesPositions.push({
            node: node,
            pos: pos
          });
        });

        _this.setUncontrolledState({
          checkedKeys: _checkedKeys,
          halfCheckedKeys: _halfCheckedKeys
        });
      }

      if (onCheck) {
        onCheck(checkedObj, eventObj);
      }
    };

    _this.onNodeLoad = function (treeNode) {
      return new Promise(function (resolve) {
        // We need to get the latest state of loading/loaded keys
        _this.setState(function (_ref) {
          var _ref$loadedKeys = _ref.loadedKeys,
              loadedKeys = _ref$loadedKeys === void 0 ? [] : _ref$loadedKeys,
              _ref$loadingKeys = _ref.loadingKeys,
              loadingKeys = _ref$loadingKeys === void 0 ? [] : _ref$loadingKeys;
          var _this$props3 = _this.props,
              loadData = _this$props3.loadData,
              onLoad = _this$props3.onLoad;
          var eventKey = treeNode.props.eventKey;

          if (!loadData || loadedKeys.indexOf(eventKey) !== -1 || loadingKeys.indexOf(eventKey) !== -1) {
            // react 15 will warn if return null
            return {};
          } // Process load data


          var promise = loadData(treeNode);
          promise.then(function () {
            var _this$state3 = _this.state,
                currentLoadedKeys = _this$state3.loadedKeys,
                currentLoadingKeys = _this$state3.loadingKeys;
            var newLoadedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(currentLoadedKeys, eventKey);
            var newLoadingKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(currentLoadingKeys, eventKey); // onLoad should trigger before internal setState to avoid `loadData` trigger twice.
            // https://github.com/ant-design/ant-design/issues/12464

            if (onLoad) {
              onLoad(newLoadedKeys, {
                event: 'load',
                node: treeNode
              });
            }

            _this.setUncontrolledState({
              loadedKeys: newLoadedKeys
            });

            _this.setState({
              loadingKeys: newLoadingKeys
            });

            resolve();
          });
          return {
            loadingKeys: Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(loadingKeys, eventKey)
          };
        });
      });
    };

    _this.onNodeExpand = function (e, treeNode) {
      var expandedKeys = _this.state.expandedKeys;
      var _this$props4 = _this.props,
          onExpand = _this$props4.onExpand,
          loadData = _this$props4.loadData;
      var _treeNode$props2 = treeNode.props,
          eventKey = _treeNode$props2.eventKey,
          expanded = _treeNode$props2.expanded; // Update selected keys

      var index = expandedKeys.indexOf(eventKey);
      var targetExpanded = !expanded;
      warning__WEBPACK_IMPORTED_MODULE_3___default()(expanded && index !== -1 || !expanded && index === -1, 'Expand state not sync with index check');

      if (targetExpanded) {
        expandedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrAdd"])(expandedKeys, eventKey);
      } else {
        expandedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["arrDel"])(expandedKeys, eventKey);
      }

      _this.setUncontrolledState({
        expandedKeys: expandedKeys
      });

      if (onExpand) {
        onExpand(expandedKeys, {
          node: treeNode,
          expanded: targetExpanded,
          nativeEvent: e.nativeEvent
        });
      } // Async Load data


      if (targetExpanded && loadData) {
        var loadPromise = _this.onNodeLoad(treeNode);

        return loadPromise ? loadPromise.then(function () {
          // [Legacy] Refresh logic
          _this.setUncontrolledState({
            expandedKeys: expandedKeys
          });
        }) : null;
      }

      return null;
    };

    _this.onNodeMouseEnter = function (event, node) {
      var onMouseEnter = _this.props.onMouseEnter;

      if (onMouseEnter) {
        onMouseEnter({
          event: event,
          node: node
        });
      }
    };

    _this.onNodeMouseLeave = function (event, node) {
      var onMouseLeave = _this.props.onMouseLeave;

      if (onMouseLeave) {
        onMouseLeave({
          event: event,
          node: node
        });
      }
    };

    _this.onNodeContextMenu = function (event, node) {
      var onRightClick = _this.props.onRightClick;

      if (onRightClick) {
        event.preventDefault();
        onRightClick({
          event: event,
          node: node
        });
      }
    };
    /**
     * Only update the value which is not in props
     */


    _this.setUncontrolledState = function (state) {
      var needSync = false;
      var newState = {};
      Object.keys(state).forEach(function (name) {
        if (name in _this.props) return;
        needSync = true;
        newState[name] = state[name];
      });

      if (needSync) {
        _this.setState(newState);
      }
    };

    _this.registerTreeNode = function (key, node) {
      if (node) {
        _this.domTreeNodes[key] = node;
      } else {
        delete _this.domTreeNodes[key];
      }
    };

    _this.isKeyChecked = function (key) {
      var _this$state$checkedKe = _this.state.checkedKeys,
          checkedKeys = _this$state$checkedKe === void 0 ? [] : _this$state$checkedKe;
      return checkedKeys.indexOf(key) !== -1;
    };
    /**
     * [Legacy] Original logic use `key` as tracking clue.
     * We have to use `cloneElement` to pass `key`.
     */


    _this.renderTreeNode = function (child, index) {
      var level = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var _this$state4 = _this.state,
          keyEntities = _this$state4.keyEntities,
          _this$state4$expanded = _this$state4.expandedKeys,
          expandedKeys = _this$state4$expanded === void 0 ? [] : _this$state4$expanded,
          _this$state4$selected = _this$state4.selectedKeys,
          selectedKeys = _this$state4$selected === void 0 ? [] : _this$state4$selected,
          _this$state4$halfChec = _this$state4.halfCheckedKeys,
          halfCheckedKeys = _this$state4$halfChec === void 0 ? [] : _this$state4$halfChec,
          _this$state4$loadedKe = _this$state4.loadedKeys,
          loadedKeys = _this$state4$loadedKe === void 0 ? [] : _this$state4$loadedKe,
          _this$state4$loadingK = _this$state4.loadingKeys,
          loadingKeys = _this$state4$loadingK === void 0 ? [] : _this$state4$loadingK,
          dragOverNodeKey = _this$state4.dragOverNodeKey,
          dropPosition = _this$state4.dropPosition;
      var pos = Object(_util__WEBPACK_IMPORTED_MODULE_7__["getPosition"])(level, index);
      var key = child.key || pos;

      if (!keyEntities[key]) {
        Object(_util__WEBPACK_IMPORTED_MODULE_7__["warnOnlyTreeNode"])();
        return null;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](child, {
        key: key,
        eventKey: key,
        expanded: expandedKeys.indexOf(key) !== -1,
        selected: selectedKeys.indexOf(key) !== -1,
        loaded: loadedKeys.indexOf(key) !== -1,
        loading: loadingKeys.indexOf(key) !== -1,
        checked: _this.isKeyChecked(key),
        halfChecked: halfCheckedKeys.indexOf(key) !== -1,
        pos: pos,
        // [Legacy] Drag props
        dragOver: dragOverNodeKey === key && dropPosition === 0,
        dragOverGapTop: dragOverNodeKey === key && dropPosition === -1,
        dragOverGapBottom: dragOverNodeKey === key && dropPosition === 1
      });
    };

    return _this;
  }

  _createClass(Tree, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var treeNode = this.state.treeNode;
      var _this$props5 = this.props,
          prefixCls = _this$props5.prefixCls,
          className = _this$props5.className,
          focusable = _this$props5.focusable,
          style = _this$props5.style,
          showLine = _this$props5.showLine,
          _this$props5$tabIndex = _this$props5.tabIndex,
          tabIndex = _this$props5$tabIndex === void 0 ? 0 : _this$props5$tabIndex,
          selectable = _this$props5.selectable,
          showIcon = _this$props5.showIcon,
          icon = _this$props5.icon,
          switcherIcon = _this$props5.switcherIcon,
          draggable = _this$props5.draggable,
          checkable = _this$props5.checkable,
          checkStrictly = _this$props5.checkStrictly,
          disabled = _this$props5.disabled,
          motion = _this$props5.motion,
          loadData = _this$props5.loadData,
          filterTreeNode = _this$props5.filterTreeNode;
      var domProps = Object(_util__WEBPACK_IMPORTED_MODULE_7__["getDataAndAria"])(this.props);

      if (focusable) {
        domProps.tabIndex = tabIndex;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_contextTypes__WEBPACK_IMPORTED_MODULE_6__["TreeContext"].Provider, {
        value: {
          prefixCls: prefixCls,
          selectable: selectable,
          showIcon: showIcon,
          icon: icon,
          switcherIcon: switcherIcon,
          draggable: draggable,
          checkable: checkable,
          checkStrictly: checkStrictly,
          disabled: disabled,
          motion: motion,
          loadData: loadData,
          filterTreeNode: filterTreeNode,
          renderTreeNode: this.renderTreeNode,
          isKeyChecked: this.isKeyChecked,
          onNodeClick: this.onNodeClick,
          onNodeDoubleClick: this.onNodeDoubleClick,
          onNodeExpand: this.onNodeExpand,
          onNodeSelect: this.onNodeSelect,
          onNodeCheck: this.onNodeCheck,
          onNodeLoad: this.onNodeLoad,
          onNodeMouseEnter: this.onNodeMouseEnter,
          onNodeMouseLeave: this.onNodeMouseLeave,
          onNodeContextMenu: this.onNodeContextMenu,
          onNodeDragStart: this.onNodeDragStart,
          onNodeDragEnter: this.onNodeDragEnter,
          onNodeDragOver: this.onNodeDragOver,
          onNodeDragLeave: this.onNodeDragLeave,
          onNodeDragEnd: this.onNodeDragEnd,
          onNodeDrop: this.onNodeDrop,
          registerTreeNode: this.registerTreeNode
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", Object.assign({}, domProps, {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, className, _defineProperty({}, "".concat(prefixCls, "-show-line"), showLine)),
        style: style,
        role: "tree",
        unselectable: "on"
      }), Object(_util__WEBPACK_IMPORTED_MODULE_7__["mapChildren"])(treeNode, function (node, index) {
        return _this2.renderTreeNode(node, index);
      })));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, prevState) {
      var prevProps = prevState.prevProps;
      var newState = {
        prevProps: props
      };

      function needSync(name) {
        return !prevProps && name in props || prevProps && prevProps[name] !== props[name];
      } // ================== Tree Node ==================


      var treeNode = null; // Check if `treeData` or `children` changed and save into the state.

      if (needSync('treeData')) {
        treeNode = Object(_util__WEBPACK_IMPORTED_MODULE_7__["convertDataToTree"])(props.treeData);
      } else if (needSync('children')) {
        treeNode = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_4__["default"])(props.children);
      } // Tree support filter function which will break the tree structure in the vdm.
      // We cache the treeNodes in state so that we can return the treeNode in event trigger.


      if (treeNode) {
        newState.treeNode = treeNode; // Calculate the entities data for quick match

        var entitiesMap = Object(_util__WEBPACK_IMPORTED_MODULE_7__["convertTreeToEntities"])(treeNode);
        newState.keyEntities = entitiesMap.keyEntities;
      }

      var keyEntities = newState.keyEntities || prevState.keyEntities; // ================ expandedKeys =================

      if (needSync('expandedKeys') || prevProps && needSync('autoExpandParent')) {
        newState.expandedKeys = props.autoExpandParent || !prevProps && props.defaultExpandParent ? Object(_util__WEBPACK_IMPORTED_MODULE_7__["conductExpandParent"])(props.expandedKeys, keyEntities) : props.expandedKeys;
      } else if (!prevProps && props.defaultExpandAll) {
        newState.expandedKeys = Object.keys(keyEntities);
      } else if (!prevProps && props.defaultExpandedKeys) {
        newState.expandedKeys = props.autoExpandParent || props.defaultExpandParent ? Object(_util__WEBPACK_IMPORTED_MODULE_7__["conductExpandParent"])(props.defaultExpandedKeys, keyEntities) : props.defaultExpandedKeys;
      } // ================ selectedKeys =================


      if (props.selectable) {
        if (needSync('selectedKeys')) {
          newState.selectedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["calcSelectedKeys"])(props.selectedKeys, props);
        } else if (!prevProps && props.defaultSelectedKeys) {
          newState.selectedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["calcSelectedKeys"])(props.defaultSelectedKeys, props);
        }
      } // ================= checkedKeys =================


      if (props.checkable) {
        var checkedKeyEntity;

        if (needSync('checkedKeys')) {
          checkedKeyEntity = Object(_util__WEBPACK_IMPORTED_MODULE_7__["parseCheckedKeys"])(props.checkedKeys) || {};
        } else if (!prevProps && props.defaultCheckedKeys) {
          checkedKeyEntity = Object(_util__WEBPACK_IMPORTED_MODULE_7__["parseCheckedKeys"])(props.defaultCheckedKeys) || {};
        } else if (treeNode) {
          // If treeNode changed, we also need check it
          checkedKeyEntity = Object(_util__WEBPACK_IMPORTED_MODULE_7__["parseCheckedKeys"])(props.checkedKeys) || {
            checkedKeys: prevState.checkedKeys,
            halfCheckedKeys: prevState.halfCheckedKeys
          };
        }

        if (checkedKeyEntity) {
          var _checkedKeyEntity = checkedKeyEntity,
              _checkedKeyEntity$che = _checkedKeyEntity.checkedKeys,
              checkedKeys = _checkedKeyEntity$che === void 0 ? [] : _checkedKeyEntity$che,
              _checkedKeyEntity$hal = _checkedKeyEntity.halfCheckedKeys,
              halfCheckedKeys = _checkedKeyEntity$hal === void 0 ? [] : _checkedKeyEntity$hal;

          if (!props.checkStrictly) {
            var conductKeys = Object(_util__WEBPACK_IMPORTED_MODULE_7__["conductCheck"])(checkedKeys, true, keyEntities);
            checkedKeys = conductKeys.checkedKeys;
            halfCheckedKeys = conductKeys.halfCheckedKeys;
          }

          newState.checkedKeys = checkedKeys;
          newState.halfCheckedKeys = halfCheckedKeys;
        }
      } // ================= loadedKeys ==================


      if (needSync('loadedKeys')) {
        newState.loadedKeys = props.loadedKeys;
      }

      return newState;
    }
  }]);

  return Tree;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Tree.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  style: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  tabIndex: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number]),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.any,
  treeData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  showLine: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  showIcon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  icon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func]),
  focusable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  selectable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  multiple: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  checkable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node]),
  checkStrictly: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  draggable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  defaultExpandParent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  autoExpandParent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  defaultExpandAll: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  defaultExpandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  expandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  defaultCheckedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  checkedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number])), prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object]),
  defaultSelectedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  selectedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDoubleClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onExpand: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onCheck: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onLoad: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  loadData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  loadedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string),
  onMouseEnter: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onMouseLeave: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onRightClick: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDragStart: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDragEnter: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDragOver: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDragLeave: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDragEnd: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onDrop: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  filterTreeNode: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  motion: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  switcherIcon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func])
};
Tree.defaultProps = {
  prefixCls: 'rc-tree',
  showLine: false,
  showIcon: true,
  selectable: true,
  multiple: false,
  checkable: false,
  disabled: false,
  checkStrictly: false,
  draggable: false,
  defaultExpandParent: true,
  autoExpandParent: false,
  defaultExpandAll: false,
  defaultExpandedKeys: [],
  defaultCheckedKeys: [],
  defaultSelectedKeys: []
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__["polyfill"])(Tree);
/* harmony default export */ __webpack_exports__["default"] = (Tree);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS9lcy9UcmVlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdHJlZS9lcy9UcmVlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoY2FsbCAmJiAoX3R5cGVvZihjYWxsKSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSkgeyByZXR1cm4gY2FsbDsgfSByZXR1cm4gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKTsgfVxuXG5mdW5jdGlvbiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpIHsgaWYgKHNlbGYgPT09IHZvaWQgMCkgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIHNlbGY7IH1cblxuZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgX2dldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LmdldFByb3RvdHlwZU9mIDogZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgcmV0dXJuIG8uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihvKTsgfTsgcmV0dXJuIF9nZXRQcm90b3R5cGVPZihvKTsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb25cIik7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKTsgfVxuXG5mdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBfc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgby5fX3Byb3RvX18gPSBwOyByZXR1cm4gbzsgfTsgcmV0dXJuIF9zZXRQcm90b3R5cGVPZihvLCBwKTsgfVxuXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgd2FybmluZyBmcm9tICd3YXJuaW5nJztcbmltcG9ydCB0b0FycmF5IGZyb20gXCJyYy11dGlsL2VzL0NoaWxkcmVuL3RvQXJyYXlcIjtcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IHsgVHJlZUNvbnRleHQgfSBmcm9tICcuL2NvbnRleHRUeXBlcyc7XG5pbXBvcnQgeyBjb252ZXJ0VHJlZVRvRW50aXRpZXMsIGNvbnZlcnREYXRhVG9UcmVlLCBnZXREYXRhQW5kQXJpYSwgZ2V0UG9zaXRpb24sIGdldERyYWdOb2Rlc0tleXMsIHBhcnNlQ2hlY2tlZEtleXMsIGNvbmR1Y3RFeHBhbmRQYXJlbnQsIGNhbGNTZWxlY3RlZEtleXMsIGNhbGNEcm9wUG9zaXRpb24sIGFyckFkZCwgYXJyRGVsLCBwb3NUb0FyciwgbWFwQ2hpbGRyZW4sIGNvbmR1Y3RDaGVjaywgd2Fybk9ubHlUcmVlTm9kZSB9IGZyb20gJy4vdXRpbCc7XG5cbnZhciBUcmVlID1cbi8qI19fUFVSRV9fKi9cbmZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhUcmVlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUcmVlKCkge1xuICAgIHZhciBfdGhpcztcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBUcmVlKTtcblxuICAgIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX2dldFByb3RvdHlwZU9mKFRyZWUpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgIC8qKiBJbnRlcm5hbCB1c2FnZSBmb3IgYHJjLXRyZWUtc2VsZWN0YCwgd2UgZG9uJ3QgcHJvbWlzZSBpdCB3aWxsIG5vdCBjaGFuZ2UuICovXG5cbiAgICBfdGhpcy5kb21UcmVlTm9kZXMgPSB7fTtcbiAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGtleUVudGl0aWVzOiB7fSxcbiAgICAgIHNlbGVjdGVkS2V5czogW10sXG4gICAgICBjaGVja2VkS2V5czogW10sXG4gICAgICBoYWxmQ2hlY2tlZEtleXM6IFtdLFxuICAgICAgbG9hZGVkS2V5czogW10sXG4gICAgICBsb2FkaW5nS2V5czogW10sXG4gICAgICBleHBhbmRlZEtleXM6IFtdLFxuICAgICAgZHJhZ05vZGVzS2V5czogW10sXG4gICAgICBkcmFnT3Zlck5vZGVLZXk6IG51bGwsXG4gICAgICBkcm9wUG9zaXRpb246IG51bGwsXG4gICAgICB0cmVlTm9kZTogW10sXG4gICAgICBwcmV2UHJvcHM6IG51bGxcbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlRHJhZ1N0YXJ0ID0gZnVuY3Rpb24gKGV2ZW50LCBub2RlKSB7XG4gICAgICB2YXIgZXhwYW5kZWRLZXlzID0gX3RoaXMuc3RhdGUuZXhwYW5kZWRLZXlzO1xuICAgICAgdmFyIG9uRHJhZ1N0YXJ0ID0gX3RoaXMucHJvcHMub25EcmFnU3RhcnQ7XG4gICAgICB2YXIgX25vZGUkcHJvcHMgPSBub2RlLnByb3BzLFxuICAgICAgICAgIGV2ZW50S2V5ID0gX25vZGUkcHJvcHMuZXZlbnRLZXksXG4gICAgICAgICAgY2hpbGRyZW4gPSBfbm9kZSRwcm9wcy5jaGlsZHJlbjtcbiAgICAgIF90aGlzLmRyYWdOb2RlID0gbm9kZTtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBkcmFnTm9kZXNLZXlzOiBnZXREcmFnTm9kZXNLZXlzKGNoaWxkcmVuLCBub2RlKSxcbiAgICAgICAgZXhwYW5kZWRLZXlzOiBhcnJEZWwoZXhwYW5kZWRLZXlzLCBldmVudEtleSlcbiAgICAgIH0pO1xuXG4gICAgICBpZiAob25EcmFnU3RhcnQpIHtcbiAgICAgICAgb25EcmFnU3RhcnQoe1xuICAgICAgICAgIGV2ZW50OiBldmVudCxcbiAgICAgICAgICBub2RlOiBub2RlXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogW0xlZ2FjeV0gU2VsZWN0IGhhbmRsZXIgaXMgbGVzcyBzbWFsbCB0aGFuIG5vZGUsXG4gICAgICogc28gdGhhdCB0aGlzIHdpbGwgdHJpZ2dlciB3aGVuIGRyYWcgZW50ZXIgbm9kZSBvciBzZWxlY3QgaGFuZGxlci5cbiAgICAgKiBUaGlzIGlzIGEgbGl0dGxlIHRyaWNreSBpZiBjdXN0b21pemUgY3NzIHdpdGhvdXQgcGFkZGluZy5cbiAgICAgKiBCZXR0ZXIgZm9yIHVzZSBtb3VzZSBtb3ZlIGV2ZW50IHRvIHJlZnJlc2ggZHJhZyBzdGF0ZS5cbiAgICAgKiBCdXQgbGV0J3MganVzdCBrZWVwIGl0IHRvIGF2b2lkIGV2ZW50IHRyaWdnZXIgbG9naWMgY2hhbmdlLlxuICAgICAqL1xuXG5cbiAgICBfdGhpcy5vbk5vZGVEcmFnRW50ZXIgPSBmdW5jdGlvbiAoZXZlbnQsIG5vZGUpIHtcbiAgICAgIHZhciBleHBhbmRlZEtleXMgPSBfdGhpcy5zdGF0ZS5leHBhbmRlZEtleXM7XG4gICAgICB2YXIgb25EcmFnRW50ZXIgPSBfdGhpcy5wcm9wcy5vbkRyYWdFbnRlcjtcbiAgICAgIHZhciBfbm9kZSRwcm9wczIgPSBub2RlLnByb3BzLFxuICAgICAgICAgIHBvcyA9IF9ub2RlJHByb3BzMi5wb3MsXG4gICAgICAgICAgZXZlbnRLZXkgPSBfbm9kZSRwcm9wczIuZXZlbnRLZXk7XG4gICAgICBpZiAoIV90aGlzLmRyYWdOb2RlKSByZXR1cm47XG4gICAgICB2YXIgZHJvcFBvc2l0aW9uID0gY2FsY0Ryb3BQb3NpdGlvbihldmVudCwgbm9kZSk7IC8vIFNraXAgaWYgZHJhZyBub2RlIGlzIHNlbGZcblxuICAgICAgaWYgKF90aGlzLmRyYWdOb2RlLnByb3BzLmV2ZW50S2V5ID09PSBldmVudEtleSAmJiBkcm9wUG9zaXRpb24gPT09IDApIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGRyYWdPdmVyTm9kZUtleTogJycsXG4gICAgICAgICAgZHJvcFBvc2l0aW9uOiBudWxsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH0gLy8gUmVmOiBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtY29tcG9uZW50L3RyZWUvaXNzdWVzLzEzMlxuICAgICAgLy8gQWRkIHRpbWVvdXQgdG8gbGV0IG9uRHJhZ0xldmVsIGZpcmUgYmVmb3JlIG9uRHJhZ0VudGVyLFxuICAgICAgLy8gc28gdGhhdCB3ZSBjYW4gY2xlYW4gZHJhZyBwcm9wcyBmb3Igb25EcmFnTGVhdmUgbm9kZS5cbiAgICAgIC8vIE1hY3JvIHRhc2sgZm9yIHRoaXM6XG4gICAgICAvLyBodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS93ZWJhcHBhcGlzLmh0bWwjY2xlYW4tdXAtYWZ0ZXItcnVubmluZy1zY3JpcHRcblxuXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gVXBkYXRlIGRyYWcgb3ZlciBub2RlXG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBkcmFnT3Zlck5vZGVLZXk6IGV2ZW50S2V5LFxuICAgICAgICAgIGRyb3BQb3NpdGlvbjogZHJvcFBvc2l0aW9uXG4gICAgICAgIH0pOyAvLyBTaWRlIGVmZmVjdCBmb3IgZGVsYXkgZHJhZ1xuXG5cbiAgICAgICAgaWYgKCFfdGhpcy5kZWxheWVkRHJhZ0VudGVyTG9naWMpIHtcbiAgICAgICAgICBfdGhpcy5kZWxheWVkRHJhZ0VudGVyTG9naWMgPSB7fTtcbiAgICAgICAgfVxuXG4gICAgICAgIE9iamVjdC5rZXlzKF90aGlzLmRlbGF5ZWREcmFnRW50ZXJMb2dpYykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgY2xlYXJUaW1lb3V0KF90aGlzLmRlbGF5ZWREcmFnRW50ZXJMb2dpY1trZXldKTtcbiAgICAgICAgfSk7XG4gICAgICAgIF90aGlzLmRlbGF5ZWREcmFnRW50ZXJMb2dpY1twb3NdID0gd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBuZXdFeHBhbmRlZEtleXMgPSBhcnJBZGQoZXhwYW5kZWRLZXlzLCBldmVudEtleSk7XG5cbiAgICAgICAgICBpZiAoISgnZXhwYW5kZWRLZXlzJyBpbiBfdGhpcy5wcm9wcykpIHtcbiAgICAgICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgZXhwYW5kZWRLZXlzOiBuZXdFeHBhbmRlZEtleXNcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChvbkRyYWdFbnRlcikge1xuICAgICAgICAgICAgb25EcmFnRW50ZXIoe1xuICAgICAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgICAgICAgIG5vZGU6IG5vZGUsXG4gICAgICAgICAgICAgIGV4cGFuZGVkS2V5czogbmV3RXhwYW5kZWRLZXlzXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sIDQwMCk7XG4gICAgICB9LCAwKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlRHJhZ092ZXIgPSBmdW5jdGlvbiAoZXZlbnQsIG5vZGUpIHtcbiAgICAgIHZhciBvbkRyYWdPdmVyID0gX3RoaXMucHJvcHMub25EcmFnT3ZlcjtcbiAgICAgIHZhciBldmVudEtleSA9IG5vZGUucHJvcHMuZXZlbnRLZXk7IC8vIFVwZGF0ZSBkcmFnIHBvc2l0aW9uXG5cbiAgICAgIGlmIChfdGhpcy5kcmFnTm9kZSAmJiBldmVudEtleSA9PT0gX3RoaXMuc3RhdGUuZHJhZ092ZXJOb2RlS2V5KSB7XG4gICAgICAgIHZhciBkcm9wUG9zaXRpb24gPSBjYWxjRHJvcFBvc2l0aW9uKGV2ZW50LCBub2RlKTtcbiAgICAgICAgaWYgKGRyb3BQb3NpdGlvbiA9PT0gX3RoaXMuc3RhdGUuZHJvcFBvc2l0aW9uKSByZXR1cm47XG5cbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGRyb3BQb3NpdGlvbjogZHJvcFBvc2l0aW9uXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBpZiAob25EcmFnT3Zlcikge1xuICAgICAgICBvbkRyYWdPdmVyKHtcbiAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgICAgbm9kZTogbm9kZVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlRHJhZ0xlYXZlID0gZnVuY3Rpb24gKGV2ZW50LCBub2RlKSB7XG4gICAgICB2YXIgb25EcmFnTGVhdmUgPSBfdGhpcy5wcm9wcy5vbkRyYWdMZWF2ZTtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBkcmFnT3Zlck5vZGVLZXk6ICcnXG4gICAgICB9KTtcblxuICAgICAgaWYgKG9uRHJhZ0xlYXZlKSB7XG4gICAgICAgIG9uRHJhZ0xlYXZlKHtcbiAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgICAgbm9kZTogbm9kZVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlRHJhZ0VuZCA9IGZ1bmN0aW9uIChldmVudCwgbm9kZSkge1xuICAgICAgdmFyIG9uRHJhZ0VuZCA9IF90aGlzLnByb3BzLm9uRHJhZ0VuZDtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBkcmFnT3Zlck5vZGVLZXk6ICcnXG4gICAgICB9KTtcblxuICAgICAgaWYgKG9uRHJhZ0VuZCkge1xuICAgICAgICBvbkRyYWdFbmQoe1xuICAgICAgICAgIGV2ZW50OiBldmVudCxcbiAgICAgICAgICBub2RlOiBub2RlXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5kcmFnTm9kZSA9IG51bGw7XG4gICAgfTtcblxuICAgIF90aGlzLm9uTm9kZURyb3AgPSBmdW5jdGlvbiAoZXZlbnQsIG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRzdGF0ZSA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIF90aGlzJHN0YXRlJGRyYWdOb2RlcyA9IF90aGlzJHN0YXRlLmRyYWdOb2Rlc0tleXMsXG4gICAgICAgICAgZHJhZ05vZGVzS2V5cyA9IF90aGlzJHN0YXRlJGRyYWdOb2RlcyA9PT0gdm9pZCAwID8gW10gOiBfdGhpcyRzdGF0ZSRkcmFnTm9kZXMsXG4gICAgICAgICAgZHJvcFBvc2l0aW9uID0gX3RoaXMkc3RhdGUuZHJvcFBvc2l0aW9uO1xuICAgICAgdmFyIG9uRHJvcCA9IF90aGlzLnByb3BzLm9uRHJvcDtcbiAgICAgIHZhciBfbm9kZSRwcm9wczMgPSBub2RlLnByb3BzLFxuICAgICAgICAgIGV2ZW50S2V5ID0gX25vZGUkcHJvcHMzLmV2ZW50S2V5LFxuICAgICAgICAgIHBvcyA9IF9ub2RlJHByb3BzMy5wb3M7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZHJhZ092ZXJOb2RlS2V5OiAnJ1xuICAgICAgfSk7XG5cbiAgICAgIGlmIChkcmFnTm9kZXNLZXlzLmluZGV4T2YoZXZlbnRLZXkpICE9PSAtMSkge1xuICAgICAgICB3YXJuaW5nKGZhbHNlLCBcIkNhbiBub3QgZHJvcCB0byBkcmFnTm9kZShpbmNsdWRlIGl0J3MgY2hpbGRyZW4gbm9kZSlcIik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIHBvc0FyciA9IHBvc1RvQXJyKHBvcyk7XG4gICAgICB2YXIgZHJvcFJlc3VsdCA9IHtcbiAgICAgICAgZXZlbnQ6IGV2ZW50LFxuICAgICAgICBub2RlOiBub2RlLFxuICAgICAgICBkcmFnTm9kZTogX3RoaXMuZHJhZ05vZGUsXG4gICAgICAgIGRyYWdOb2Rlc0tleXM6IGRyYWdOb2Rlc0tleXMuc2xpY2UoKSxcbiAgICAgICAgZHJvcFBvc2l0aW9uOiBkcm9wUG9zaXRpb24gKyBOdW1iZXIocG9zQXJyW3Bvc0Fyci5sZW5ndGggLSAxXSksXG4gICAgICAgIGRyb3BUb0dhcDogZmFsc2VcbiAgICAgIH07XG5cbiAgICAgIGlmIChkcm9wUG9zaXRpb24gIT09IDApIHtcbiAgICAgICAgZHJvcFJlc3VsdC5kcm9wVG9HYXAgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAob25Ecm9wKSB7XG4gICAgICAgIG9uRHJvcChkcm9wUmVzdWx0KTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuZHJhZ05vZGUgPSBudWxsO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbk5vZGVDbGljayA9IGZ1bmN0aW9uIChlLCB0cmVlTm9kZSkge1xuICAgICAgdmFyIG9uQ2xpY2sgPSBfdGhpcy5wcm9wcy5vbkNsaWNrO1xuXG4gICAgICBpZiAob25DbGljaykge1xuICAgICAgICBvbkNsaWNrKGUsIHRyZWVOb2RlKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlRG91YmxlQ2xpY2sgPSBmdW5jdGlvbiAoZSwgdHJlZU5vZGUpIHtcbiAgICAgIHZhciBvbkRvdWJsZUNsaWNrID0gX3RoaXMucHJvcHMub25Eb3VibGVDbGljaztcblxuICAgICAgaWYgKG9uRG91YmxlQ2xpY2spIHtcbiAgICAgICAgb25Eb3VibGVDbGljayhlLCB0cmVlTm9kZSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uTm9kZVNlbGVjdCA9IGZ1bmN0aW9uIChlLCB0cmVlTm9kZSkge1xuICAgICAgdmFyIHNlbGVjdGVkS2V5cyA9IF90aGlzLnN0YXRlLnNlbGVjdGVkS2V5cztcbiAgICAgIHZhciBrZXlFbnRpdGllcyA9IF90aGlzLnN0YXRlLmtleUVudGl0aWVzO1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb25TZWxlY3QgPSBfdGhpcyRwcm9wcy5vblNlbGVjdCxcbiAgICAgICAgICBtdWx0aXBsZSA9IF90aGlzJHByb3BzLm11bHRpcGxlO1xuICAgICAgdmFyIF90cmVlTm9kZSRwcm9wcyA9IHRyZWVOb2RlLnByb3BzLFxuICAgICAgICAgIHNlbGVjdGVkID0gX3RyZWVOb2RlJHByb3BzLnNlbGVjdGVkLFxuICAgICAgICAgIGV2ZW50S2V5ID0gX3RyZWVOb2RlJHByb3BzLmV2ZW50S2V5O1xuICAgICAgdmFyIHRhcmdldFNlbGVjdGVkID0gIXNlbGVjdGVkOyAvLyBVcGRhdGUgc2VsZWN0ZWQga2V5c1xuXG4gICAgICBpZiAoIXRhcmdldFNlbGVjdGVkKSB7XG4gICAgICAgIHNlbGVjdGVkS2V5cyA9IGFyckRlbChzZWxlY3RlZEtleXMsIGV2ZW50S2V5KTtcbiAgICAgIH0gZWxzZSBpZiAoIW11bHRpcGxlKSB7XG4gICAgICAgIHNlbGVjdGVkS2V5cyA9IFtldmVudEtleV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZWxlY3RlZEtleXMgPSBhcnJBZGQoc2VsZWN0ZWRLZXlzLCBldmVudEtleSk7XG4gICAgICB9IC8vIFtMZWdhY3ldIE5vdCBmb3VuZCByZWxhdGVkIHVzYWdlIGluIGRvYyBvciB1cHBlciBsaWJzXG5cblxuICAgICAgdmFyIHNlbGVjdGVkTm9kZXMgPSBzZWxlY3RlZEtleXMubWFwKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgdmFyIGVudGl0eSA9IGtleUVudGl0aWVzW2tleV07XG4gICAgICAgIGlmICghZW50aXR5KSByZXR1cm4gbnVsbDtcbiAgICAgICAgcmV0dXJuIGVudGl0eS5ub2RlO1xuICAgICAgfSkuZmlsdGVyKGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgIHJldHVybiBub2RlO1xuICAgICAgfSk7XG5cbiAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgc2VsZWN0ZWRLZXlzOiBzZWxlY3RlZEtleXNcbiAgICAgIH0pO1xuXG4gICAgICBpZiAob25TZWxlY3QpIHtcbiAgICAgICAgb25TZWxlY3Qoc2VsZWN0ZWRLZXlzLCB7XG4gICAgICAgICAgZXZlbnQ6ICdzZWxlY3QnLFxuICAgICAgICAgIHNlbGVjdGVkOiB0YXJnZXRTZWxlY3RlZCxcbiAgICAgICAgICBub2RlOiB0cmVlTm9kZSxcbiAgICAgICAgICBzZWxlY3RlZE5vZGVzOiBzZWxlY3RlZE5vZGVzLFxuICAgICAgICAgIG5hdGl2ZUV2ZW50OiBlLm5hdGl2ZUV2ZW50XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbk5vZGVDaGVjayA9IGZ1bmN0aW9uIChlLCB0cmVlTm9kZSwgY2hlY2tlZCkge1xuICAgICAgdmFyIF90aGlzJHN0YXRlMiA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIGtleUVudGl0aWVzID0gX3RoaXMkc3RhdGUyLmtleUVudGl0aWVzLFxuICAgICAgICAgIG9yaUNoZWNrZWRLZXlzID0gX3RoaXMkc3RhdGUyLmNoZWNrZWRLZXlzLFxuICAgICAgICAgIG9yaUhhbGZDaGVja2VkS2V5cyA9IF90aGlzJHN0YXRlMi5oYWxmQ2hlY2tlZEtleXM7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgY2hlY2tTdHJpY3RseSA9IF90aGlzJHByb3BzMi5jaGVja1N0cmljdGx5LFxuICAgICAgICAgIG9uQ2hlY2sgPSBfdGhpcyRwcm9wczIub25DaGVjaztcbiAgICAgIHZhciBldmVudEtleSA9IHRyZWVOb2RlLnByb3BzLmV2ZW50S2V5OyAvLyBQcmVwYXJlIHRyaWdnZXIgYXJndW1lbnRzXG5cbiAgICAgIHZhciBjaGVja2VkT2JqO1xuICAgICAgdmFyIGV2ZW50T2JqID0ge1xuICAgICAgICBldmVudDogJ2NoZWNrJyxcbiAgICAgICAgbm9kZTogdHJlZU5vZGUsXG4gICAgICAgIGNoZWNrZWQ6IGNoZWNrZWQsXG4gICAgICAgIG5hdGl2ZUV2ZW50OiBlLm5hdGl2ZUV2ZW50XG4gICAgICB9O1xuXG4gICAgICBpZiAoY2hlY2tTdHJpY3RseSkge1xuICAgICAgICB2YXIgY2hlY2tlZEtleXMgPSBjaGVja2VkID8gYXJyQWRkKG9yaUNoZWNrZWRLZXlzLCBldmVudEtleSkgOiBhcnJEZWwob3JpQ2hlY2tlZEtleXMsIGV2ZW50S2V5KTtcbiAgICAgICAgdmFyIGhhbGZDaGVja2VkS2V5cyA9IGFyckRlbChvcmlIYWxmQ2hlY2tlZEtleXMsIGV2ZW50S2V5KTtcbiAgICAgICAgY2hlY2tlZE9iaiA9IHtcbiAgICAgICAgICBjaGVja2VkOiBjaGVja2VkS2V5cyxcbiAgICAgICAgICBoYWxmQ2hlY2tlZDogaGFsZkNoZWNrZWRLZXlzXG4gICAgICAgIH07XG4gICAgICAgIGV2ZW50T2JqLmNoZWNrZWROb2RlcyA9IGNoZWNrZWRLZXlzLm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgcmV0dXJuIGtleUVudGl0aWVzW2tleV07XG4gICAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgICAgcmV0dXJuIGVudGl0eTtcbiAgICAgICAgfSkubWFwKGZ1bmN0aW9uIChlbnRpdHkpIHtcbiAgICAgICAgICByZXR1cm4gZW50aXR5Lm5vZGU7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICBjaGVja2VkS2V5czogY2hlY2tlZEtleXNcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgX2NvbmR1Y3RDaGVjayA9IGNvbmR1Y3RDaGVjayhbZXZlbnRLZXldLCBjaGVja2VkLCBrZXlFbnRpdGllcywge1xuICAgICAgICAgIGNoZWNrZWRLZXlzOiBvcmlDaGVja2VkS2V5cyxcbiAgICAgICAgICBoYWxmQ2hlY2tlZEtleXM6IG9yaUhhbGZDaGVja2VkS2V5c1xuICAgICAgICB9KSxcbiAgICAgICAgICAgIF9jaGVja2VkS2V5cyA9IF9jb25kdWN0Q2hlY2suY2hlY2tlZEtleXMsXG4gICAgICAgICAgICBfaGFsZkNoZWNrZWRLZXlzID0gX2NvbmR1Y3RDaGVjay5oYWxmQ2hlY2tlZEtleXM7XG5cbiAgICAgICAgY2hlY2tlZE9iaiA9IF9jaGVja2VkS2V5czsgLy8gW0xlZ2FjeV0gVGhpcyBpcyB1c2VkIGZvciBgcmMtdHJlZS1zZWxlY3RgXG5cbiAgICAgICAgZXZlbnRPYmouY2hlY2tlZE5vZGVzID0gW107XG4gICAgICAgIGV2ZW50T2JqLmNoZWNrZWROb2Rlc1Bvc2l0aW9ucyA9IFtdO1xuICAgICAgICBldmVudE9iai5oYWxmQ2hlY2tlZEtleXMgPSBfaGFsZkNoZWNrZWRLZXlzO1xuXG4gICAgICAgIF9jaGVja2VkS2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICB2YXIgZW50aXR5ID0ga2V5RW50aXRpZXNba2V5XTtcbiAgICAgICAgICBpZiAoIWVudGl0eSkgcmV0dXJuO1xuICAgICAgICAgIHZhciBub2RlID0gZW50aXR5Lm5vZGUsXG4gICAgICAgICAgICAgIHBvcyA9IGVudGl0eS5wb3M7XG4gICAgICAgICAgZXZlbnRPYmouY2hlY2tlZE5vZGVzLnB1c2gobm9kZSk7XG4gICAgICAgICAgZXZlbnRPYmouY2hlY2tlZE5vZGVzUG9zaXRpb25zLnB1c2goe1xuICAgICAgICAgICAgbm9kZTogbm9kZSxcbiAgICAgICAgICAgIHBvczogcG9zXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICBjaGVja2VkS2V5czogX2NoZWNrZWRLZXlzLFxuICAgICAgICAgIGhhbGZDaGVja2VkS2V5czogX2hhbGZDaGVja2VkS2V5c1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgaWYgKG9uQ2hlY2spIHtcbiAgICAgICAgb25DaGVjayhjaGVja2VkT2JqLCBldmVudE9iaik7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uTm9kZUxvYWQgPSBmdW5jdGlvbiAodHJlZU5vZGUpIHtcbiAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSkge1xuICAgICAgICAvLyBXZSBuZWVkIHRvIGdldCB0aGUgbGF0ZXN0IHN0YXRlIG9mIGxvYWRpbmcvbG9hZGVkIGtleXNcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoZnVuY3Rpb24gKF9yZWYpIHtcbiAgICAgICAgICB2YXIgX3JlZiRsb2FkZWRLZXlzID0gX3JlZi5sb2FkZWRLZXlzLFxuICAgICAgICAgICAgICBsb2FkZWRLZXlzID0gX3JlZiRsb2FkZWRLZXlzID09PSB2b2lkIDAgPyBbXSA6IF9yZWYkbG9hZGVkS2V5cyxcbiAgICAgICAgICAgICAgX3JlZiRsb2FkaW5nS2V5cyA9IF9yZWYubG9hZGluZ0tleXMsXG4gICAgICAgICAgICAgIGxvYWRpbmdLZXlzID0gX3JlZiRsb2FkaW5nS2V5cyA9PT0gdm9pZCAwID8gW10gOiBfcmVmJGxvYWRpbmdLZXlzO1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wczMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgICAgbG9hZERhdGEgPSBfdGhpcyRwcm9wczMubG9hZERhdGEsXG4gICAgICAgICAgICAgIG9uTG9hZCA9IF90aGlzJHByb3BzMy5vbkxvYWQ7XG4gICAgICAgICAgdmFyIGV2ZW50S2V5ID0gdHJlZU5vZGUucHJvcHMuZXZlbnRLZXk7XG5cbiAgICAgICAgICBpZiAoIWxvYWREYXRhIHx8IGxvYWRlZEtleXMuaW5kZXhPZihldmVudEtleSkgIT09IC0xIHx8IGxvYWRpbmdLZXlzLmluZGV4T2YoZXZlbnRLZXkpICE9PSAtMSkge1xuICAgICAgICAgICAgLy8gcmVhY3QgMTUgd2lsbCB3YXJuIGlmIHJldHVybiBudWxsXG4gICAgICAgICAgICByZXR1cm4ge307XG4gICAgICAgICAgfSAvLyBQcm9jZXNzIGxvYWQgZGF0YVxuXG5cbiAgICAgICAgICB2YXIgcHJvbWlzZSA9IGxvYWREYXRhKHRyZWVOb2RlKTtcbiAgICAgICAgICBwcm9taXNlLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIF90aGlzJHN0YXRlMyA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgICAgICAgIGN1cnJlbnRMb2FkZWRLZXlzID0gX3RoaXMkc3RhdGUzLmxvYWRlZEtleXMsXG4gICAgICAgICAgICAgICAgY3VycmVudExvYWRpbmdLZXlzID0gX3RoaXMkc3RhdGUzLmxvYWRpbmdLZXlzO1xuICAgICAgICAgICAgdmFyIG5ld0xvYWRlZEtleXMgPSBhcnJBZGQoY3VycmVudExvYWRlZEtleXMsIGV2ZW50S2V5KTtcbiAgICAgICAgICAgIHZhciBuZXdMb2FkaW5nS2V5cyA9IGFyckRlbChjdXJyZW50TG9hZGluZ0tleXMsIGV2ZW50S2V5KTsgLy8gb25Mb2FkIHNob3VsZCB0cmlnZ2VyIGJlZm9yZSBpbnRlcm5hbCBzZXRTdGF0ZSB0byBhdm9pZCBgbG9hZERhdGFgIHRyaWdnZXIgdHdpY2UuXG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMjQ2NFxuXG4gICAgICAgICAgICBpZiAob25Mb2FkKSB7XG4gICAgICAgICAgICAgIG9uTG9hZChuZXdMb2FkZWRLZXlzLCB7XG4gICAgICAgICAgICAgICAgZXZlbnQ6ICdsb2FkJyxcbiAgICAgICAgICAgICAgICBub2RlOiB0cmVlTm9kZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgX3RoaXMuc2V0VW5jb250cm9sbGVkU3RhdGUoe1xuICAgICAgICAgICAgICBsb2FkZWRLZXlzOiBuZXdMb2FkZWRLZXlzXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBsb2FkaW5nS2V5czogbmV3TG9hZGluZ0tleXNcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxvYWRpbmdLZXlzOiBhcnJBZGQobG9hZGluZ0tleXMsIGV2ZW50S2V5KVxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uTm9kZUV4cGFuZCA9IGZ1bmN0aW9uIChlLCB0cmVlTm9kZSkge1xuICAgICAgdmFyIGV4cGFuZGVkS2V5cyA9IF90aGlzLnN0YXRlLmV4cGFuZGVkS2V5cztcbiAgICAgIHZhciBfdGhpcyRwcm9wczQgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBvbkV4cGFuZCA9IF90aGlzJHByb3BzNC5vbkV4cGFuZCxcbiAgICAgICAgICBsb2FkRGF0YSA9IF90aGlzJHByb3BzNC5sb2FkRGF0YTtcbiAgICAgIHZhciBfdHJlZU5vZGUkcHJvcHMyID0gdHJlZU5vZGUucHJvcHMsXG4gICAgICAgICAgZXZlbnRLZXkgPSBfdHJlZU5vZGUkcHJvcHMyLmV2ZW50S2V5LFxuICAgICAgICAgIGV4cGFuZGVkID0gX3RyZWVOb2RlJHByb3BzMi5leHBhbmRlZDsgLy8gVXBkYXRlIHNlbGVjdGVkIGtleXNcblxuICAgICAgdmFyIGluZGV4ID0gZXhwYW5kZWRLZXlzLmluZGV4T2YoZXZlbnRLZXkpO1xuICAgICAgdmFyIHRhcmdldEV4cGFuZGVkID0gIWV4cGFuZGVkO1xuICAgICAgd2FybmluZyhleHBhbmRlZCAmJiBpbmRleCAhPT0gLTEgfHwgIWV4cGFuZGVkICYmIGluZGV4ID09PSAtMSwgJ0V4cGFuZCBzdGF0ZSBub3Qgc3luYyB3aXRoIGluZGV4IGNoZWNrJyk7XG5cbiAgICAgIGlmICh0YXJnZXRFeHBhbmRlZCkge1xuICAgICAgICBleHBhbmRlZEtleXMgPSBhcnJBZGQoZXhwYW5kZWRLZXlzLCBldmVudEtleSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBleHBhbmRlZEtleXMgPSBhcnJEZWwoZXhwYW5kZWRLZXlzLCBldmVudEtleSk7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgZXhwYW5kZWRLZXlzOiBleHBhbmRlZEtleXNcbiAgICAgIH0pO1xuXG4gICAgICBpZiAob25FeHBhbmQpIHtcbiAgICAgICAgb25FeHBhbmQoZXhwYW5kZWRLZXlzLCB7XG4gICAgICAgICAgbm9kZTogdHJlZU5vZGUsXG4gICAgICAgICAgZXhwYW5kZWQ6IHRhcmdldEV4cGFuZGVkLFxuICAgICAgICAgIG5hdGl2ZUV2ZW50OiBlLm5hdGl2ZUV2ZW50XG4gICAgICAgIH0pO1xuICAgICAgfSAvLyBBc3luYyBMb2FkIGRhdGFcblxuXG4gICAgICBpZiAodGFyZ2V0RXhwYW5kZWQgJiYgbG9hZERhdGEpIHtcbiAgICAgICAgdmFyIGxvYWRQcm9taXNlID0gX3RoaXMub25Ob2RlTG9hZCh0cmVlTm9kZSk7XG5cbiAgICAgICAgcmV0dXJuIGxvYWRQcm9taXNlID8gbG9hZFByb21pc2UudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgLy8gW0xlZ2FjeV0gUmVmcmVzaCBsb2dpY1xuICAgICAgICAgIF90aGlzLnNldFVuY29udHJvbGxlZFN0YXRlKHtcbiAgICAgICAgICAgIGV4cGFuZGVkS2V5czogZXhwYW5kZWRLZXlzXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pIDogbnVsbDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIF90aGlzLm9uTm9kZU1vdXNlRW50ZXIgPSBmdW5jdGlvbiAoZXZlbnQsIG5vZGUpIHtcbiAgICAgIHZhciBvbk1vdXNlRW50ZXIgPSBfdGhpcy5wcm9wcy5vbk1vdXNlRW50ZXI7XG5cbiAgICAgIGlmIChvbk1vdXNlRW50ZXIpIHtcbiAgICAgICAgb25Nb3VzZUVudGVyKHtcbiAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgICAgbm9kZTogbm9kZVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25Ob2RlTW91c2VMZWF2ZSA9IGZ1bmN0aW9uIChldmVudCwgbm9kZSkge1xuICAgICAgdmFyIG9uTW91c2VMZWF2ZSA9IF90aGlzLnByb3BzLm9uTW91c2VMZWF2ZTtcblxuICAgICAgaWYgKG9uTW91c2VMZWF2ZSkge1xuICAgICAgICBvbk1vdXNlTGVhdmUoe1xuICAgICAgICAgIGV2ZW50OiBldmVudCxcbiAgICAgICAgICBub2RlOiBub2RlXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbk5vZGVDb250ZXh0TWVudSA9IGZ1bmN0aW9uIChldmVudCwgbm9kZSkge1xuICAgICAgdmFyIG9uUmlnaHRDbGljayA9IF90aGlzLnByb3BzLm9uUmlnaHRDbGljaztcblxuICAgICAgaWYgKG9uUmlnaHRDbGljaykge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBvblJpZ2h0Q2xpY2soe1xuICAgICAgICAgIGV2ZW50OiBldmVudCxcbiAgICAgICAgICBub2RlOiBub2RlXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogT25seSB1cGRhdGUgdGhlIHZhbHVlIHdoaWNoIGlzIG5vdCBpbiBwcm9wc1xuICAgICAqL1xuXG5cbiAgICBfdGhpcy5zZXRVbmNvbnRyb2xsZWRTdGF0ZSA9IGZ1bmN0aW9uIChzdGF0ZSkge1xuICAgICAgdmFyIG5lZWRTeW5jID0gZmFsc2U7XG4gICAgICB2YXIgbmV3U3RhdGUgPSB7fTtcbiAgICAgIE9iamVjdC5rZXlzKHN0YXRlKS5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIGlmIChuYW1lIGluIF90aGlzLnByb3BzKSByZXR1cm47XG4gICAgICAgIG5lZWRTeW5jID0gdHJ1ZTtcbiAgICAgICAgbmV3U3RhdGVbbmFtZV0gPSBzdGF0ZVtuYW1lXTtcbiAgICAgIH0pO1xuXG4gICAgICBpZiAobmVlZFN5bmMpIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUobmV3U3RhdGUpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5yZWdpc3RlclRyZWVOb2RlID0gZnVuY3Rpb24gKGtleSwgbm9kZSkge1xuICAgICAgaWYgKG5vZGUpIHtcbiAgICAgICAgX3RoaXMuZG9tVHJlZU5vZGVzW2tleV0gPSBub2RlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZGVsZXRlIF90aGlzLmRvbVRyZWVOb2Rlc1trZXldO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5pc0tleUNoZWNrZWQgPSBmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgX3RoaXMkc3RhdGUkY2hlY2tlZEtlID0gX3RoaXMuc3RhdGUuY2hlY2tlZEtleXMsXG4gICAgICAgICAgY2hlY2tlZEtleXMgPSBfdGhpcyRzdGF0ZSRjaGVja2VkS2UgPT09IHZvaWQgMCA/IFtdIDogX3RoaXMkc3RhdGUkY2hlY2tlZEtlO1xuICAgICAgcmV0dXJuIGNoZWNrZWRLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTE7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBbTGVnYWN5XSBPcmlnaW5hbCBsb2dpYyB1c2UgYGtleWAgYXMgdHJhY2tpbmcgY2x1ZS5cbiAgICAgKiBXZSBoYXZlIHRvIHVzZSBgY2xvbmVFbGVtZW50YCB0byBwYXNzIGBrZXlgLlxuICAgICAqL1xuXG5cbiAgICBfdGhpcy5yZW5kZXJUcmVlTm9kZSA9IGZ1bmN0aW9uIChjaGlsZCwgaW5kZXgpIHtcbiAgICAgIHZhciBsZXZlbCA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogMDtcbiAgICAgIHZhciBfdGhpcyRzdGF0ZTQgPSBfdGhpcy5zdGF0ZSxcbiAgICAgICAgICBrZXlFbnRpdGllcyA9IF90aGlzJHN0YXRlNC5rZXlFbnRpdGllcyxcbiAgICAgICAgICBfdGhpcyRzdGF0ZTQkZXhwYW5kZWQgPSBfdGhpcyRzdGF0ZTQuZXhwYW5kZWRLZXlzLFxuICAgICAgICAgIGV4cGFuZGVkS2V5cyA9IF90aGlzJHN0YXRlNCRleHBhbmRlZCA9PT0gdm9pZCAwID8gW10gOiBfdGhpcyRzdGF0ZTQkZXhwYW5kZWQsXG4gICAgICAgICAgX3RoaXMkc3RhdGU0JHNlbGVjdGVkID0gX3RoaXMkc3RhdGU0LnNlbGVjdGVkS2V5cyxcbiAgICAgICAgICBzZWxlY3RlZEtleXMgPSBfdGhpcyRzdGF0ZTQkc2VsZWN0ZWQgPT09IHZvaWQgMCA/IFtdIDogX3RoaXMkc3RhdGU0JHNlbGVjdGVkLFxuICAgICAgICAgIF90aGlzJHN0YXRlNCRoYWxmQ2hlYyA9IF90aGlzJHN0YXRlNC5oYWxmQ2hlY2tlZEtleXMsXG4gICAgICAgICAgaGFsZkNoZWNrZWRLZXlzID0gX3RoaXMkc3RhdGU0JGhhbGZDaGVjID09PSB2b2lkIDAgPyBbXSA6IF90aGlzJHN0YXRlNCRoYWxmQ2hlYyxcbiAgICAgICAgICBfdGhpcyRzdGF0ZTQkbG9hZGVkS2UgPSBfdGhpcyRzdGF0ZTQubG9hZGVkS2V5cyxcbiAgICAgICAgICBsb2FkZWRLZXlzID0gX3RoaXMkc3RhdGU0JGxvYWRlZEtlID09PSB2b2lkIDAgPyBbXSA6IF90aGlzJHN0YXRlNCRsb2FkZWRLZSxcbiAgICAgICAgICBfdGhpcyRzdGF0ZTQkbG9hZGluZ0sgPSBfdGhpcyRzdGF0ZTQubG9hZGluZ0tleXMsXG4gICAgICAgICAgbG9hZGluZ0tleXMgPSBfdGhpcyRzdGF0ZTQkbG9hZGluZ0sgPT09IHZvaWQgMCA/IFtdIDogX3RoaXMkc3RhdGU0JGxvYWRpbmdLLFxuICAgICAgICAgIGRyYWdPdmVyTm9kZUtleSA9IF90aGlzJHN0YXRlNC5kcmFnT3Zlck5vZGVLZXksXG4gICAgICAgICAgZHJvcFBvc2l0aW9uID0gX3RoaXMkc3RhdGU0LmRyb3BQb3NpdGlvbjtcbiAgICAgIHZhciBwb3MgPSBnZXRQb3NpdGlvbihsZXZlbCwgaW5kZXgpO1xuICAgICAgdmFyIGtleSA9IGNoaWxkLmtleSB8fCBwb3M7XG5cbiAgICAgIGlmICgha2V5RW50aXRpZXNba2V5XSkge1xuICAgICAgICB3YXJuT25seVRyZWVOb2RlKCk7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgIGtleToga2V5LFxuICAgICAgICBldmVudEtleToga2V5LFxuICAgICAgICBleHBhbmRlZDogZXhwYW5kZWRLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTEsXG4gICAgICAgIHNlbGVjdGVkOiBzZWxlY3RlZEtleXMuaW5kZXhPZihrZXkpICE9PSAtMSxcbiAgICAgICAgbG9hZGVkOiBsb2FkZWRLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTEsXG4gICAgICAgIGxvYWRpbmc6IGxvYWRpbmdLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTEsXG4gICAgICAgIGNoZWNrZWQ6IF90aGlzLmlzS2V5Q2hlY2tlZChrZXkpLFxuICAgICAgICBoYWxmQ2hlY2tlZDogaGFsZkNoZWNrZWRLZXlzLmluZGV4T2Yoa2V5KSAhPT0gLTEsXG4gICAgICAgIHBvczogcG9zLFxuICAgICAgICAvLyBbTGVnYWN5XSBEcmFnIHByb3BzXG4gICAgICAgIGRyYWdPdmVyOiBkcmFnT3Zlck5vZGVLZXkgPT09IGtleSAmJiBkcm9wUG9zaXRpb24gPT09IDAsXG4gICAgICAgIGRyYWdPdmVyR2FwVG9wOiBkcmFnT3Zlck5vZGVLZXkgPT09IGtleSAmJiBkcm9wUG9zaXRpb24gPT09IC0xLFxuICAgICAgICBkcmFnT3ZlckdhcEJvdHRvbTogZHJhZ092ZXJOb2RlS2V5ID09PSBrZXkgJiYgZHJvcFBvc2l0aW9uID09PSAxXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKFRyZWUsIFt7XG4gICAga2V5OiBcInJlbmRlclwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgdmFyIHRyZWVOb2RlID0gdGhpcy5zdGF0ZS50cmVlTm9kZTtcbiAgICAgIHZhciBfdGhpcyRwcm9wczUgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzNS5wcmVmaXhDbHMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3RoaXMkcHJvcHM1LmNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGUgPSBfdGhpcyRwcm9wczUuZm9jdXNhYmxlLFxuICAgICAgICAgIHN0eWxlID0gX3RoaXMkcHJvcHM1LnN0eWxlLFxuICAgICAgICAgIHNob3dMaW5lID0gX3RoaXMkcHJvcHM1LnNob3dMaW5lLFxuICAgICAgICAgIF90aGlzJHByb3BzNSR0YWJJbmRleCA9IF90aGlzJHByb3BzNS50YWJJbmRleCxcbiAgICAgICAgICB0YWJJbmRleCA9IF90aGlzJHByb3BzNSR0YWJJbmRleCA9PT0gdm9pZCAwID8gMCA6IF90aGlzJHByb3BzNSR0YWJJbmRleCxcbiAgICAgICAgICBzZWxlY3RhYmxlID0gX3RoaXMkcHJvcHM1LnNlbGVjdGFibGUsXG4gICAgICAgICAgc2hvd0ljb24gPSBfdGhpcyRwcm9wczUuc2hvd0ljb24sXG4gICAgICAgICAgaWNvbiA9IF90aGlzJHByb3BzNS5pY29uLFxuICAgICAgICAgIHN3aXRjaGVySWNvbiA9IF90aGlzJHByb3BzNS5zd2l0Y2hlckljb24sXG4gICAgICAgICAgZHJhZ2dhYmxlID0gX3RoaXMkcHJvcHM1LmRyYWdnYWJsZSxcbiAgICAgICAgICBjaGVja2FibGUgPSBfdGhpcyRwcm9wczUuY2hlY2thYmxlLFxuICAgICAgICAgIGNoZWNrU3RyaWN0bHkgPSBfdGhpcyRwcm9wczUuY2hlY2tTdHJpY3RseSxcbiAgICAgICAgICBkaXNhYmxlZCA9IF90aGlzJHByb3BzNS5kaXNhYmxlZCxcbiAgICAgICAgICBtb3Rpb24gPSBfdGhpcyRwcm9wczUubW90aW9uLFxuICAgICAgICAgIGxvYWREYXRhID0gX3RoaXMkcHJvcHM1LmxvYWREYXRhLFxuICAgICAgICAgIGZpbHRlclRyZWVOb2RlID0gX3RoaXMkcHJvcHM1LmZpbHRlclRyZWVOb2RlO1xuICAgICAgdmFyIGRvbVByb3BzID0gZ2V0RGF0YUFuZEFyaWEodGhpcy5wcm9wcyk7XG5cbiAgICAgIGlmIChmb2N1c2FibGUpIHtcbiAgICAgICAgZG9tUHJvcHMudGFiSW5kZXggPSB0YWJJbmRleDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoVHJlZUNvbnRleHQuUHJvdmlkZXIsIHtcbiAgICAgICAgdmFsdWU6IHtcbiAgICAgICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgICAgICBzZWxlY3RhYmxlOiBzZWxlY3RhYmxlLFxuICAgICAgICAgIHNob3dJY29uOiBzaG93SWNvbixcbiAgICAgICAgICBpY29uOiBpY29uLFxuICAgICAgICAgIHN3aXRjaGVySWNvbjogc3dpdGNoZXJJY29uLFxuICAgICAgICAgIGRyYWdnYWJsZTogZHJhZ2dhYmxlLFxuICAgICAgICAgIGNoZWNrYWJsZTogY2hlY2thYmxlLFxuICAgICAgICAgIGNoZWNrU3RyaWN0bHk6IGNoZWNrU3RyaWN0bHksXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkLFxuICAgICAgICAgIG1vdGlvbjogbW90aW9uLFxuICAgICAgICAgIGxvYWREYXRhOiBsb2FkRGF0YSxcbiAgICAgICAgICBmaWx0ZXJUcmVlTm9kZTogZmlsdGVyVHJlZU5vZGUsXG4gICAgICAgICAgcmVuZGVyVHJlZU5vZGU6IHRoaXMucmVuZGVyVHJlZU5vZGUsXG4gICAgICAgICAgaXNLZXlDaGVja2VkOiB0aGlzLmlzS2V5Q2hlY2tlZCxcbiAgICAgICAgICBvbk5vZGVDbGljazogdGhpcy5vbk5vZGVDbGljayxcbiAgICAgICAgICBvbk5vZGVEb3VibGVDbGljazogdGhpcy5vbk5vZGVEb3VibGVDbGljayxcbiAgICAgICAgICBvbk5vZGVFeHBhbmQ6IHRoaXMub25Ob2RlRXhwYW5kLFxuICAgICAgICAgIG9uTm9kZVNlbGVjdDogdGhpcy5vbk5vZGVTZWxlY3QsXG4gICAgICAgICAgb25Ob2RlQ2hlY2s6IHRoaXMub25Ob2RlQ2hlY2ssXG4gICAgICAgICAgb25Ob2RlTG9hZDogdGhpcy5vbk5vZGVMb2FkLFxuICAgICAgICAgIG9uTm9kZU1vdXNlRW50ZXI6IHRoaXMub25Ob2RlTW91c2VFbnRlcixcbiAgICAgICAgICBvbk5vZGVNb3VzZUxlYXZlOiB0aGlzLm9uTm9kZU1vdXNlTGVhdmUsXG4gICAgICAgICAgb25Ob2RlQ29udGV4dE1lbnU6IHRoaXMub25Ob2RlQ29udGV4dE1lbnUsXG4gICAgICAgICAgb25Ob2RlRHJhZ1N0YXJ0OiB0aGlzLm9uTm9kZURyYWdTdGFydCxcbiAgICAgICAgICBvbk5vZGVEcmFnRW50ZXI6IHRoaXMub25Ob2RlRHJhZ0VudGVyLFxuICAgICAgICAgIG9uTm9kZURyYWdPdmVyOiB0aGlzLm9uTm9kZURyYWdPdmVyLFxuICAgICAgICAgIG9uTm9kZURyYWdMZWF2ZTogdGhpcy5vbk5vZGVEcmFnTGVhdmUsXG4gICAgICAgICAgb25Ob2RlRHJhZ0VuZDogdGhpcy5vbk5vZGVEcmFnRW5kLFxuICAgICAgICAgIG9uTm9kZURyb3A6IHRoaXMub25Ob2RlRHJvcCxcbiAgICAgICAgICByZWdpc3RlclRyZWVOb2RlOiB0aGlzLnJlZ2lzdGVyVHJlZU5vZGVcbiAgICAgICAgfVxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcInVsXCIsIE9iamVjdC5hc3NpZ24oe30sIGRvbVByb3BzLCB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSwgX2RlZmluZVByb3BlcnR5KHt9LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNob3ctbGluZVwiKSwgc2hvd0xpbmUpKSxcbiAgICAgICAgc3R5bGU6IHN0eWxlLFxuICAgICAgICByb2xlOiBcInRyZWVcIixcbiAgICAgICAgdW5zZWxlY3RhYmxlOiBcIm9uXCJcbiAgICAgIH0pLCBtYXBDaGlsZHJlbih0cmVlTm9kZSwgZnVuY3Rpb24gKG5vZGUsIGluZGV4KSB7XG4gICAgICAgIHJldHVybiBfdGhpczIucmVuZGVyVHJlZU5vZGUobm9kZSwgaW5kZXgpO1xuICAgICAgfSkpKTtcbiAgICB9XG4gIH1dLCBbe1xuICAgIGtleTogXCJnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHNcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKHByb3BzLCBwcmV2U3RhdGUpIHtcbiAgICAgIHZhciBwcmV2UHJvcHMgPSBwcmV2U3RhdGUucHJldlByb3BzO1xuICAgICAgdmFyIG5ld1N0YXRlID0ge1xuICAgICAgICBwcmV2UHJvcHM6IHByb3BzXG4gICAgICB9O1xuXG4gICAgICBmdW5jdGlvbiBuZWVkU3luYyhuYW1lKSB7XG4gICAgICAgIHJldHVybiAhcHJldlByb3BzICYmIG5hbWUgaW4gcHJvcHMgfHwgcHJldlByb3BzICYmIHByZXZQcm9wc1tuYW1lXSAhPT0gcHJvcHNbbmFtZV07XG4gICAgICB9IC8vID09PT09PT09PT09PT09PT09PSBUcmVlIE5vZGUgPT09PT09PT09PT09PT09PT09XG5cblxuICAgICAgdmFyIHRyZWVOb2RlID0gbnVsbDsgLy8gQ2hlY2sgaWYgYHRyZWVEYXRhYCBvciBgY2hpbGRyZW5gIGNoYW5nZWQgYW5kIHNhdmUgaW50byB0aGUgc3RhdGUuXG5cbiAgICAgIGlmIChuZWVkU3luYygndHJlZURhdGEnKSkge1xuICAgICAgICB0cmVlTm9kZSA9IGNvbnZlcnREYXRhVG9UcmVlKHByb3BzLnRyZWVEYXRhKTtcbiAgICAgIH0gZWxzZSBpZiAobmVlZFN5bmMoJ2NoaWxkcmVuJykpIHtcbiAgICAgICAgdHJlZU5vZGUgPSB0b0FycmF5KHByb3BzLmNoaWxkcmVuKTtcbiAgICAgIH0gLy8gVHJlZSBzdXBwb3J0IGZpbHRlciBmdW5jdGlvbiB3aGljaCB3aWxsIGJyZWFrIHRoZSB0cmVlIHN0cnVjdHVyZSBpbiB0aGUgdmRtLlxuICAgICAgLy8gV2UgY2FjaGUgdGhlIHRyZWVOb2RlcyBpbiBzdGF0ZSBzbyB0aGF0IHdlIGNhbiByZXR1cm4gdGhlIHRyZWVOb2RlIGluIGV2ZW50IHRyaWdnZXIuXG5cblxuICAgICAgaWYgKHRyZWVOb2RlKSB7XG4gICAgICAgIG5ld1N0YXRlLnRyZWVOb2RlID0gdHJlZU5vZGU7IC8vIENhbGN1bGF0ZSB0aGUgZW50aXRpZXMgZGF0YSBmb3IgcXVpY2sgbWF0Y2hcblxuICAgICAgICB2YXIgZW50aXRpZXNNYXAgPSBjb252ZXJ0VHJlZVRvRW50aXRpZXModHJlZU5vZGUpO1xuICAgICAgICBuZXdTdGF0ZS5rZXlFbnRpdGllcyA9IGVudGl0aWVzTWFwLmtleUVudGl0aWVzO1xuICAgICAgfVxuXG4gICAgICB2YXIga2V5RW50aXRpZXMgPSBuZXdTdGF0ZS5rZXlFbnRpdGllcyB8fCBwcmV2U3RhdGUua2V5RW50aXRpZXM7IC8vID09PT09PT09PT09PT09PT0gZXhwYW5kZWRLZXlzID09PT09PT09PT09PT09PT09XG5cbiAgICAgIGlmIChuZWVkU3luYygnZXhwYW5kZWRLZXlzJykgfHwgcHJldlByb3BzICYmIG5lZWRTeW5jKCdhdXRvRXhwYW5kUGFyZW50JykpIHtcbiAgICAgICAgbmV3U3RhdGUuZXhwYW5kZWRLZXlzID0gcHJvcHMuYXV0b0V4cGFuZFBhcmVudCB8fCAhcHJldlByb3BzICYmIHByb3BzLmRlZmF1bHRFeHBhbmRQYXJlbnQgPyBjb25kdWN0RXhwYW5kUGFyZW50KHByb3BzLmV4cGFuZGVkS2V5cywga2V5RW50aXRpZXMpIDogcHJvcHMuZXhwYW5kZWRLZXlzO1xuICAgICAgfSBlbHNlIGlmICghcHJldlByb3BzICYmIHByb3BzLmRlZmF1bHRFeHBhbmRBbGwpIHtcbiAgICAgICAgbmV3U3RhdGUuZXhwYW5kZWRLZXlzID0gT2JqZWN0LmtleXMoa2V5RW50aXRpZXMpO1xuICAgICAgfSBlbHNlIGlmICghcHJldlByb3BzICYmIHByb3BzLmRlZmF1bHRFeHBhbmRlZEtleXMpIHtcbiAgICAgICAgbmV3U3RhdGUuZXhwYW5kZWRLZXlzID0gcHJvcHMuYXV0b0V4cGFuZFBhcmVudCB8fCBwcm9wcy5kZWZhdWx0RXhwYW5kUGFyZW50ID8gY29uZHVjdEV4cGFuZFBhcmVudChwcm9wcy5kZWZhdWx0RXhwYW5kZWRLZXlzLCBrZXlFbnRpdGllcykgOiBwcm9wcy5kZWZhdWx0RXhwYW5kZWRLZXlzO1xuICAgICAgfSAvLyA9PT09PT09PT09PT09PT09IHNlbGVjdGVkS2V5cyA9PT09PT09PT09PT09PT09PVxuXG5cbiAgICAgIGlmIChwcm9wcy5zZWxlY3RhYmxlKSB7XG4gICAgICAgIGlmIChuZWVkU3luYygnc2VsZWN0ZWRLZXlzJykpIHtcbiAgICAgICAgICBuZXdTdGF0ZS5zZWxlY3RlZEtleXMgPSBjYWxjU2VsZWN0ZWRLZXlzKHByb3BzLnNlbGVjdGVkS2V5cywgcHJvcHMpO1xuICAgICAgICB9IGVsc2UgaWYgKCFwcmV2UHJvcHMgJiYgcHJvcHMuZGVmYXVsdFNlbGVjdGVkS2V5cykge1xuICAgICAgICAgIG5ld1N0YXRlLnNlbGVjdGVkS2V5cyA9IGNhbGNTZWxlY3RlZEtleXMocHJvcHMuZGVmYXVsdFNlbGVjdGVkS2V5cywgcHJvcHMpO1xuICAgICAgICB9XG4gICAgICB9IC8vID09PT09PT09PT09PT09PT09IGNoZWNrZWRLZXlzID09PT09PT09PT09PT09PT09XG5cblxuICAgICAgaWYgKHByb3BzLmNoZWNrYWJsZSkge1xuICAgICAgICB2YXIgY2hlY2tlZEtleUVudGl0eTtcblxuICAgICAgICBpZiAobmVlZFN5bmMoJ2NoZWNrZWRLZXlzJykpIHtcbiAgICAgICAgICBjaGVja2VkS2V5RW50aXR5ID0gcGFyc2VDaGVja2VkS2V5cyhwcm9wcy5jaGVja2VkS2V5cykgfHwge307XG4gICAgICAgIH0gZWxzZSBpZiAoIXByZXZQcm9wcyAmJiBwcm9wcy5kZWZhdWx0Q2hlY2tlZEtleXMpIHtcbiAgICAgICAgICBjaGVja2VkS2V5RW50aXR5ID0gcGFyc2VDaGVja2VkS2V5cyhwcm9wcy5kZWZhdWx0Q2hlY2tlZEtleXMpIHx8IHt9O1xuICAgICAgICB9IGVsc2UgaWYgKHRyZWVOb2RlKSB7XG4gICAgICAgICAgLy8gSWYgdHJlZU5vZGUgY2hhbmdlZCwgd2UgYWxzbyBuZWVkIGNoZWNrIGl0XG4gICAgICAgICAgY2hlY2tlZEtleUVudGl0eSA9IHBhcnNlQ2hlY2tlZEtleXMocHJvcHMuY2hlY2tlZEtleXMpIHx8IHtcbiAgICAgICAgICAgIGNoZWNrZWRLZXlzOiBwcmV2U3RhdGUuY2hlY2tlZEtleXMsXG4gICAgICAgICAgICBoYWxmQ2hlY2tlZEtleXM6IHByZXZTdGF0ZS5oYWxmQ2hlY2tlZEtleXNcbiAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNoZWNrZWRLZXlFbnRpdHkpIHtcbiAgICAgICAgICB2YXIgX2NoZWNrZWRLZXlFbnRpdHkgPSBjaGVja2VkS2V5RW50aXR5LFxuICAgICAgICAgICAgICBfY2hlY2tlZEtleUVudGl0eSRjaGUgPSBfY2hlY2tlZEtleUVudGl0eS5jaGVja2VkS2V5cyxcbiAgICAgICAgICAgICAgY2hlY2tlZEtleXMgPSBfY2hlY2tlZEtleUVudGl0eSRjaGUgPT09IHZvaWQgMCA/IFtdIDogX2NoZWNrZWRLZXlFbnRpdHkkY2hlLFxuICAgICAgICAgICAgICBfY2hlY2tlZEtleUVudGl0eSRoYWwgPSBfY2hlY2tlZEtleUVudGl0eS5oYWxmQ2hlY2tlZEtleXMsXG4gICAgICAgICAgICAgIGhhbGZDaGVja2VkS2V5cyA9IF9jaGVja2VkS2V5RW50aXR5JGhhbCA9PT0gdm9pZCAwID8gW10gOiBfY2hlY2tlZEtleUVudGl0eSRoYWw7XG5cbiAgICAgICAgICBpZiAoIXByb3BzLmNoZWNrU3RyaWN0bHkpIHtcbiAgICAgICAgICAgIHZhciBjb25kdWN0S2V5cyA9IGNvbmR1Y3RDaGVjayhjaGVja2VkS2V5cywgdHJ1ZSwga2V5RW50aXRpZXMpO1xuICAgICAgICAgICAgY2hlY2tlZEtleXMgPSBjb25kdWN0S2V5cy5jaGVja2VkS2V5cztcbiAgICAgICAgICAgIGhhbGZDaGVja2VkS2V5cyA9IGNvbmR1Y3RLZXlzLmhhbGZDaGVja2VkS2V5cztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBuZXdTdGF0ZS5jaGVja2VkS2V5cyA9IGNoZWNrZWRLZXlzO1xuICAgICAgICAgIG5ld1N0YXRlLmhhbGZDaGVja2VkS2V5cyA9IGhhbGZDaGVja2VkS2V5cztcbiAgICAgICAgfVxuICAgICAgfSAvLyA9PT09PT09PT09PT09PT09PSBsb2FkZWRLZXlzID09PT09PT09PT09PT09PT09PVxuXG5cbiAgICAgIGlmIChuZWVkU3luYygnbG9hZGVkS2V5cycpKSB7XG4gICAgICAgIG5ld1N0YXRlLmxvYWRlZEtleXMgPSBwcm9wcy5sb2FkZWRLZXlzO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbmV3U3RhdGU7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFRyZWU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cblRyZWUucHJvcFR5cGVzID0ge1xuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gIHRhYkluZGV4OiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSksXG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMuYW55LFxuICB0cmVlRGF0YTogUHJvcFR5cGVzLmFycmF5LFxuICBzaG93TGluZTogUHJvcFR5cGVzLmJvb2wsXG4gIHNob3dJY29uOiBQcm9wVHlwZXMuYm9vbCxcbiAgaWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm5vZGUsIFByb3BUeXBlcy5mdW5jXSksXG4gIGZvY3VzYWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIHNlbGVjdGFibGU6IFByb3BUeXBlcy5ib29sLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gIG11bHRpcGxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgY2hlY2thYmxlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuYm9vbCwgUHJvcFR5cGVzLm5vZGVdKSxcbiAgY2hlY2tTdHJpY3RseTogUHJvcFR5cGVzLmJvb2wsXG4gIGRyYWdnYWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRFeHBhbmRQYXJlbnQ6IFByb3BUeXBlcy5ib29sLFxuICBhdXRvRXhwYW5kUGFyZW50OiBQcm9wVHlwZXMuYm9vbCxcbiAgZGVmYXVsdEV4cGFuZEFsbDogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRFeHBhbmRlZEtleXM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5zdHJpbmcpLFxuICBleHBhbmRlZEtleXM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5zdHJpbmcpLFxuICBkZWZhdWx0Q2hlY2tlZEtleXM6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5zdHJpbmcpLFxuICBjaGVja2VkS2V5czogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pKSwgUHJvcFR5cGVzLm9iamVjdF0pLFxuICBkZWZhdWx0U2VsZWN0ZWRLZXlzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMuc3RyaW5nKSxcbiAgc2VsZWN0ZWRLZXlzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMuc3RyaW5nKSxcbiAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uRG91YmxlQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxuICBvbkV4cGFuZDogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uQ2hlY2s6IFByb3BUeXBlcy5mdW5jLFxuICBvblNlbGVjdDogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uTG9hZDogUHJvcFR5cGVzLmZ1bmMsXG4gIGxvYWREYXRhOiBQcm9wVHlwZXMuZnVuYyxcbiAgbG9hZGVkS2V5czogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyksXG4gIG9uTW91c2VFbnRlcjogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uTW91c2VMZWF2ZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uUmlnaHRDbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uRHJhZ1N0YXJ0OiBQcm9wVHlwZXMuZnVuYyxcbiAgb25EcmFnRW50ZXI6IFByb3BUeXBlcy5mdW5jLFxuICBvbkRyYWdPdmVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25EcmFnTGVhdmU6IFByb3BUeXBlcy5mdW5jLFxuICBvbkRyYWdFbmQ6IFByb3BUeXBlcy5mdW5jLFxuICBvbkRyb3A6IFByb3BUeXBlcy5mdW5jLFxuICBmaWx0ZXJUcmVlTm9kZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG1vdGlvbjogUHJvcFR5cGVzLm9iamVjdCxcbiAgc3dpdGNoZXJJY29uOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubm9kZSwgUHJvcFR5cGVzLmZ1bmNdKVxufTtcblRyZWUuZGVmYXVsdFByb3BzID0ge1xuICBwcmVmaXhDbHM6ICdyYy10cmVlJyxcbiAgc2hvd0xpbmU6IGZhbHNlLFxuICBzaG93SWNvbjogdHJ1ZSxcbiAgc2VsZWN0YWJsZTogdHJ1ZSxcbiAgbXVsdGlwbGU6IGZhbHNlLFxuICBjaGVja2FibGU6IGZhbHNlLFxuICBkaXNhYmxlZDogZmFsc2UsXG4gIGNoZWNrU3RyaWN0bHk6IGZhbHNlLFxuICBkcmFnZ2FibGU6IGZhbHNlLFxuICBkZWZhdWx0RXhwYW5kUGFyZW50OiB0cnVlLFxuICBhdXRvRXhwYW5kUGFyZW50OiBmYWxzZSxcbiAgZGVmYXVsdEV4cGFuZEFsbDogZmFsc2UsXG4gIGRlZmF1bHRFeHBhbmRlZEtleXM6IFtdLFxuICBkZWZhdWx0Q2hlY2tlZEtleXM6IFtdLFxuICBkZWZhdWx0U2VsZWN0ZWRLZXlzOiBbXVxufTtcbnBvbHlmaWxsKFRyZWUpO1xuZXhwb3J0IGRlZmF1bHQgVHJlZTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBQ0E7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTlCQTtBQURBO0FBa0NBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUF4RUE7QUEwRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpGQTtBQUNBO0FBMkZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBN0NBO0FBK0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree/es/Tree.js
