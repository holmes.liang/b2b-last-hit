__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigContext", function() { return ConfigContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigConsumer", function() { return ConfigConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withConfigConsumer", function() { return withConfigConsumer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js");
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _renderEmpty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./renderEmpty */ "./node_modules/antd/es/config-provider/renderEmpty.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}




var ConfigContext = _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_1___default()({
  // We provide a default function for Context without provider
  getPrefixCls: function getPrefixCls(suffixCls, customizePrefixCls) {
    if (customizePrefixCls) return customizePrefixCls;
    return "ant-".concat(suffixCls);
  },
  renderEmpty: _renderEmpty__WEBPACK_IMPORTED_MODULE_2__["default"]
});
var ConfigConsumer = ConfigContext.Consumer;
function withConfigConsumer(config) {
  return function withConfigConsumerFunc(Component) {
    // Wrap with ConfigConsumer. Since we need compatible with react 15, be care when using ref methods
    var SFC = function SFC(props) {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](ConfigConsumer, null, function (configProps) {
        var basicPrefixCls = config.prefixCls;
        var getPrefixCls = configProps.getPrefixCls;
        var customizePrefixCls = props.prefixCls;
        var prefixCls = getPrefixCls(basicPrefixCls, customizePrefixCls);
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Component, _extends({}, configProps, props, {
          prefixCls: prefixCls
        }));
      });
    };

    var cons = Component.constructor;
    var name = cons && cons.displayName || Component.name || 'Component';
    SFC.displayName = "withConfigConsumer(".concat(name, ")");
    return SFC;
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jb25maWctcHJvdmlkZXIvY29udGV4dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvY29uZmlnLXByb3ZpZGVyL2NvbnRleHQuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjcmVhdGVSZWFjdENvbnRleHQgZnJvbSAnQGFudC1kZXNpZ24vY3JlYXRlLXJlYWN0LWNvbnRleHQnO1xuaW1wb3J0IGRlZmF1bHRSZW5kZXJFbXB0eSBmcm9tICcuL3JlbmRlckVtcHR5JztcbmV4cG9ydCBjb25zdCBDb25maWdDb250ZXh0ID0gY3JlYXRlUmVhY3RDb250ZXh0KHtcbiAgICAvLyBXZSBwcm92aWRlIGEgZGVmYXVsdCBmdW5jdGlvbiBmb3IgQ29udGV4dCB3aXRob3V0IHByb3ZpZGVyXG4gICAgZ2V0UHJlZml4Q2xzOiAoc3VmZml4Q2xzLCBjdXN0b21pemVQcmVmaXhDbHMpID0+IHtcbiAgICAgICAgaWYgKGN1c3RvbWl6ZVByZWZpeENscylcbiAgICAgICAgICAgIHJldHVybiBjdXN0b21pemVQcmVmaXhDbHM7XG4gICAgICAgIHJldHVybiBgYW50LSR7c3VmZml4Q2xzfWA7XG4gICAgfSxcbiAgICByZW5kZXJFbXB0eTogZGVmYXVsdFJlbmRlckVtcHR5LFxufSk7XG5leHBvcnQgY29uc3QgQ29uZmlnQ29uc3VtZXIgPSBDb25maWdDb250ZXh0LkNvbnN1bWVyO1xuZXhwb3J0IGZ1bmN0aW9uIHdpdGhDb25maWdDb25zdW1lcihjb25maWcpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gd2l0aENvbmZpZ0NvbnN1bWVyRnVuYyhDb21wb25lbnQpIHtcbiAgICAgICAgLy8gV3JhcCB3aXRoIENvbmZpZ0NvbnN1bWVyLiBTaW5jZSB3ZSBuZWVkIGNvbXBhdGlibGUgd2l0aCByZWFjdCAxNSwgYmUgY2FyZSB3aGVuIHVzaW5nIHJlZiBtZXRob2RzXG4gICAgICAgIGNvbnN0IFNGQyA9ICgocHJvcHMpID0+ICg8Q29uZmlnQ29uc3VtZXI+XG4gICAgICAgIHsoY29uZmlnUHJvcHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBiYXNpY1ByZWZpeENscyB9ID0gY29uZmlnO1xuICAgICAgICAgICAgY29uc3QgeyBnZXRQcmVmaXhDbHMgfSA9IGNvbmZpZ1Byb3BzO1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscyB9ID0gcHJvcHM7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoYmFzaWNQcmVmaXhDbHMsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICByZXR1cm4gPENvbXBvbmVudCB7Li4uY29uZmlnUHJvcHN9IHsuLi5wcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9Lz47XG4gICAgICAgIH19XG4gICAgICA8L0NvbmZpZ0NvbnN1bWVyPikpO1xuICAgICAgICBjb25zdCBjb25zID0gQ29tcG9uZW50LmNvbnN0cnVjdG9yO1xuICAgICAgICBjb25zdCBuYW1lID0gKGNvbnMgJiYgY29ucy5kaXNwbGF5TmFtZSkgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG4gICAgICAgIFNGQy5kaXNwbGF5TmFtZSA9IGB3aXRoQ29uZmlnQ29uc3VtZXIoJHtuYW1lfSlgO1xuICAgICAgICByZXR1cm4gU0ZDO1xuICAgIH07XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFMQTtBQU9BO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQU5BO0FBQUE7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFnQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/config-provider/context.js
