__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyItem", function() { return PolicyItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyItems", function() { return PolicyItems; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @desk-component/query/prod-type */ "./src/app/desk/component/query/prod-type.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/success/components/policy-item.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nmargin-bottom: 20px;\n  \n                 .listContainer {\n                    img {\n                        display: inline-block;\n                        width: 30px;\n                        height: 30px;\n                        margin-right: 1em;\n                        vertical-align: middle;\n                    }\n                    tbody {\n                        border: 1px solid #ebebeb;\n                    }\n                    .itnt-logo-wrap {\n                        display: -ms-flexbox;\n                        display: flex;\n                        -ms-flex-direction: column;\n                        flex-direction: column;\n                        -ms-flex-pack: center;\n                        justify-content: center;\n                        position: relative;\n                        min-height: 3.5em;\n                        padding-left: 65px;\n                    }\n                    td {\n                        padding: 5px 10px;\n                    }\n                   \n                    th {\n                        font-weight: normal;\n                        padding: 8px 15px;\n                        text-align: left;\n                        background: #fcfcfc;\n                        line-height: 2;\n                        border: 1px solid #efefef;\n                        border-width: 1px 0;\n                        color: #878787;\n                        &:first-child {\n                            border-left-width: 1px;\n                        }\n                        .itnt-badge {\n                            font-size: 75%;\n                            padding: 0 3px;\n                            margin: 0 5px;\n                            vertical-align: 1px;\n                            border: 1px solid;\n                            border-radius: 5px;\n                        }\n                    }\n                    td {\n                        a {\n                            color: grey;\n                        }\n                        strong {\n                          color: #878787;\n                        }\n                        img {\n                            position: absolute;\n                            top: 50%;\n                            left: 0;\n                            width: 55px;\n                            -webkit-transform: translateY(-50%);\n                            transform: translateY(-50%);\n                            content: '';\n                            background-repeat: no-repeat;\n                            background-size: contain;\n                            background-position: center;\n                            height: auto;\n                        }\n                        .title {\n                            font-weight: bold;\n                            line-height: 20px;\n                            padding: 5px 0;\n                            color: rgba(0, 0, 0, 0.85);\n                            text-transform: uppercase;\n                            word-break: break-word;\n                            font-size: 14px;\n                            margin-top: 0;\n                            padding: 0 0 0 10px;\n                            text-align: left;\n                            margin-bottom: .5em;\n                            line-height: 28px;\n                        }\n                        .title--sub {\n                            font-size: 13px;\n                            font-weight: normal;\n                            color: rgba(0, 0, 0, 0.45);\n                            margin-bottom: .5em;\n                            line-height: 28px;\n                            padding-left: 10px;\n                        }\n                    }\n                }\n                .mobile-list-container {\n                  padding: 20px;\n                  td {\n                    width: 100%;\n                    float: left;\n                    padding-left: 75px;\n                    margin-left: 1px;\n                    border-right: 1px solid #ebebeb;\n                    &:first-child {\n                    padding-left: 0;\n                    }\n                    .title,.title--sub {\n                      line-height: 28px;\n                      margin-bottom: 0;\n                    }\n                  }\n                  tbody {\n                    tr:nth-child(2) {\n                      padding: 10px;\n                    }\n                  }\n                }\n        \n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}






var isMobile = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsMobile();
var PolicyItem = function PolicyItem(props) {
  var productCate = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "productCate");

  var quoteNo = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "refNo");

  var bizType = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "bizType");

  var itemKey = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "refId");

  var lastUpdatedAt = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "lastUpdatedAt");

  lastUpdatedAt = lastUpdatedAt ? _common__WEBPACK_IMPORTED_MODULE_5__["DateUtils"].toDate(lastUpdatedAt).fromNow() : "";

  var statusName = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "statusName");

  var subStatusName = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "subStatusName");

  var renderPolicyMainInfo = function renderPolicyMainInfo() {
    var itntLogoUrl = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "itntLogoUrl");

    var phStr = [lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "phName", ""), lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "phMobile", "")].filter(function (item) {
      return item;
    }).join(", ");
    return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("div", {
      className: "policyMainInfo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("div", {
      className: "itnt-logo-wrap",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }, !!itntLogoUrl && _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("img", {
      src: itntLogoUrl,
      alt: "",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
      className: "title max-limit--22",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 28
      },
      __self: this
    }, lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "insuredDescription", "")), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
      className: "title--sub",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, [lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "productName"), lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.policy, "planName")].filter(function (v, i, a) {
      return v && a.indexOf(v) === i;
    }).join(", ")), phStr && _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
      className: "title--sub",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, phStr)));
  };

  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(StylePolicyItem, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("div", {
    className: "listContainer ".concat(_common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsMobile() ? "mobile-list-container" : ""),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("table", {
    style: {
      width: "100%"
    },
    className: "listInnerTable table-layout--fixed",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("colgroup", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("col", {
    width: "5",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("col", {
    width: "2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("col", {
    width: "2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("col", {
    width: "3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("col", {
    width: "3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  })), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("tbody", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].Fragment, {
    key: itemKey,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("th", {
    colSpan: 5,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    style: {
      float: "left"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(_desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_4__["ProdTypeImage"], {
    productCate: productCate,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }), quoteNo, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("span", {
    className: "itnt-badge",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }, bizType)), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    style: {
      float: "right"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64
    },
    __self: this
  }, "".concat(props.policy.ctntStructCode || "", " \n                    ").concat(props.policy.ctntStructCode && props.policy.ctntStructName ? "-" : "", " \n                    ").concat(props.policy.ctntStructName || ""))))), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("td", {
    style: {
      width: isMobile ? "100%" : "32%",
      textAlign: "left"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74
    },
    __self: this
  }, renderPolicyMainInfo()), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("td", {
    style: {
      width: isMobile ? "100%" : "20%",
      textAlign: isMobile ? "left" : "right"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].renderPolicyPremium(props.policy)), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("td", {
    style: {
      width: isMobile ? "100%" : "28%",
      textAlign: isMobile ? "left" : "center"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("strong", {
    style: {
      fontWeight: 700
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }, statusName), subStatusName && _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
    style: {
      fontSize: 12,
      color: "#999"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }, subStatusName), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
    className: "title--sub",
    style: {
      paddingLeft: 0
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  }, lastUpdatedAt)), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("td", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("a", {
    className: "button",
    onClick: function onClick() {
      var _props$policy = props.policy,
          productCode = _props$policy.productCode,
          refId = _props$policy.refId,
          bizType = _props$policy.bizType,
          itntCode = _props$policy.itntCode,
          productVersion = _props$policy.productVersion;

      var _url = _common__WEBPACK_IMPORTED_MODULE_5__["PATH"].NORMAL_QUOTE.replace(":itntCode", itntCode).replace(":prdtCode", productCode).replace("/:policyId?", "?clonePolicyId=" + refId);

      props.router.pushRedirect(_url);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_5__["Language"].en("Clone").thai("Clone").getMessage())), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("td", {
    style: {
      width: isMobile ? "100%" : "20%",
      textAlign: isMobile ? "left" : "center"
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103
    },
    __self: this
  }, renderOpers(props.policy, props.isECard, props.policyId))))))));
};

function renderOpers(item, isECard, policyId) {
  var result = [];
  var moreOper = [];

  var outputTypes = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(item, "outputTypes", []);

  var bizType = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(item, "bizType");

  moreOper = outputTypes.map(function (output, index) {
    var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_5__["Ajax"].appendAuthToUrl(bizType === "ENDO" ? "/endo/".concat(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(item, "refId"), "/pdf/").concat(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(output, "name")) : "/policies/".concat(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(item, "refId"), "/pdf/").concat(lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(output, "name")));
    return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "rejectable".concat(index),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 125
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("a", {
      href: pdfUrl,
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 126
      },
      __self: this
    }, lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(output, "displayName")));
  });

  if (isECard) {
    var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_5__["Ajax"].appendAuthToUrl("/policies/".concat(policyId, "/passkit/applewallet"));
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "rejectableECard",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 136
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("a", {
      href: pdfUrl,
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 137
      },
      __self: this
    }, "E-card")));
  }

  var menu = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 144
    },
    __self: this
  }, moreOper);

  if (moreOper.length > 0) {
    result.push(_common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Dropdown"], {
      overlay: menu,
      key: "dropdown",
      trigger: ["hover", "click"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("a", {
      className: "ant-dropdown-link button",
      href: "javascript:;",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 148
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_5__["Language"].en("Outputs").getMessage(), " ", _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
      type: "down",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    }))));
  }

  if (!result.length) {
    result.push("");
  }

  return _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].joinElements(result, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Divider"], {
    type: "vertical",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 161
    },
    __self: this
  }));
}

var PolicyItems = function PolicyItems(props) {
  var items = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(props.model, "items", []) || [];
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].Fragment, null, items.map(function (item, index) {
    return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(PolicyItem, {
      key: index,
      model: props.model,
      policyId: props.policyId,
      router: props.router,
      isECard: props.isECard,
      policy: item,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 168
      },
      __self: this
    });
  }));
};
var StylePolicyItem = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["Styled"].div(_templateObject());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9zdWNjZXNzL2NvbXBvbmVudHMvcG9saWN5LWl0ZW0udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9zdWNjZXNzL2NvbXBvbmVudHMvcG9saWN5LWl0ZW0udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcblxuaW1wb3J0IHsgQ29sLCBEaXZpZGVyLCBEcm9wZG93biwgSWNvbiwgTWVudSwgUm93IH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IFByb2RUeXBlSW1hZ2UgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3F1ZXJ5L3Byb2QtdHlwZVwiO1xuaW1wb3J0IHsgQWpheCwgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgUEFUSCwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5leHBvcnQgY29uc3QgUG9saWN5SXRlbSA9IChwcm9wczogeyBtb2RlbDogYW55LCBwb2xpY3k6IGFueSwgaXNFQ2FyZD86IGFueSwgcG9saWN5SWQ/OiBhbnksIHJvdXRlcjogYW55IH0pID0+IHtcbiAgY29uc3QgcHJvZHVjdENhdGUgPSBfLmdldChwcm9wcy5wb2xpY3ksIFwicHJvZHVjdENhdGVcIik7XG4gIGNvbnN0IHF1b3RlTm8gPSBfLmdldChwcm9wcy5wb2xpY3ksIFwicmVmTm9cIik7XG4gIGNvbnN0IGJpelR5cGUgPSBfLmdldChwcm9wcy5wb2xpY3ksIFwiYml6VHlwZVwiKTtcbiAgY29uc3QgaXRlbUtleSA9IF8uZ2V0KHByb3BzLnBvbGljeSwgXCJyZWZJZFwiKTtcbiAgbGV0IGxhc3RVcGRhdGVkQXQgPSBfLmdldChwcm9wcy5wb2xpY3ksIFwibGFzdFVwZGF0ZWRBdFwiKTtcbiAgbGFzdFVwZGF0ZWRBdCA9IGxhc3RVcGRhdGVkQXQgPyBEYXRlVXRpbHMudG9EYXRlKGxhc3RVcGRhdGVkQXQpLmZyb21Ob3coKSA6IFwiXCI7XG4gIGNvbnN0IHN0YXR1c05hbWUgPSBfLmdldChwcm9wcy5wb2xpY3ksIFwic3RhdHVzTmFtZVwiKTtcbiAgY29uc3Qgc3ViU3RhdHVzTmFtZSA9IF8uZ2V0KHByb3BzLnBvbGljeSwgXCJzdWJTdGF0dXNOYW1lXCIpO1xuXG4gIGNvbnN0IHJlbmRlclBvbGljeU1haW5JbmZvID0gKCkgPT4ge1xuICAgIGNvbnN0IGl0bnRMb2dvVXJsID0gXy5nZXQocHJvcHMucG9saWN5LCBcIml0bnRMb2dvVXJsXCIpO1xuICAgIGNvbnN0IHBoU3RyID0gW18uZ2V0KHByb3BzLnBvbGljeSwgXCJwaE5hbWVcIiwgXCJcIiksIF8uZ2V0KHByb3BzLnBvbGljeSwgXCJwaE1vYmlsZVwiLCBcIlwiKV1cbiAgICAgIC5maWx0ZXIoKGl0ZW06IGFueSkgPT4gaXRlbSlcbiAgICAgIC5qb2luKFwiLCBcIik7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicG9saWN5TWFpbkluZm9cIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpdG50LWxvZ28td3JhcFwiPlxuICAgICAgICAgIHshIWl0bnRMb2dvVXJsICYmIDxpbWcgc3JjPXtpdG50TG9nb1VybH0gYWx0PVwiXCIvPn1cbiAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZSBtYXgtbGltaXQtLTIyXCI+XG4gICAgICAgICAgICB7Xy5nZXQocHJvcHMucG9saWN5LCBcImluc3VyZWREZXNjcmlwdGlvblwiLCBcIlwiKX1cbiAgICAgICAgICA8L3A+XG4gICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGUtLXN1YlwiPlxuICAgICAgICAgICAge1tfLmdldChwcm9wcy5wb2xpY3ksIFwicHJvZHVjdE5hbWVcIiksIF8uZ2V0KHByb3BzLnBvbGljeSwgXCJwbGFuTmFtZVwiKV1cbiAgICAgICAgICAgICAgLmZpbHRlcigodiwgaSwgYSkgPT4gdiAmJiBhLmluZGV4T2YodikgPT09IGkpXG4gICAgICAgICAgICAgIC5qb2luKFwiLCBcIil9XG4gICAgICAgICAgPC9wPlxuICAgICAgICAgIHtwaFN0ciAmJiA8cCBjbGFzc05hbWU9XCJ0aXRsZS0tc3ViXCI+e3BoU3RyfTwvcD59XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICByZXR1cm4gKFxuICAgIDxTdHlsZVBvbGljeUl0ZW0+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YGxpc3RDb250YWluZXIgJHtVdGlscy5nZXRJc01vYmlsZSgpID8gXCJtb2JpbGUtbGlzdC1jb250YWluZXJcIiA6IFwiXCJ9YH0+XG4gICAgICAgIDx0YWJsZSBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX0gY2xhc3NOYW1lPVwibGlzdElubmVyVGFibGUgdGFibGUtbGF5b3V0LS1maXhlZFwiPlxuICAgICAgICAgIDxjb2xncm91cD5cbiAgICAgICAgICAgIDxjb2wgd2lkdGg9XCI1XCIvPlxuICAgICAgICAgICAgPGNvbCB3aWR0aD1cIjJcIi8+XG4gICAgICAgICAgICA8Y29sIHdpZHRoPVwiMlwiLz5cbiAgICAgICAgICAgIDxjb2wgd2lkdGg9XCIzXCIvPlxuICAgICAgICAgICAgPGNvbCB3aWR0aD1cIjNcIi8+XG4gICAgICAgICAgPC9jb2xncm91cD5cblxuICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICA8UmVhY3QuRnJhZ21lbnQga2V5PXtpdGVtS2V5fT5cbiAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgPHRoIGNvbFNwYW49ezV9PlxuICAgICAgICAgICAgICAgIDxSb3c+XG4gICAgICAgICAgICAgICAgICA8Q29sIHN0eWxlPXt7IGZsb2F0OiBcImxlZnRcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgPFByb2RUeXBlSW1hZ2UgcHJvZHVjdENhdGU9e3Byb2R1Y3RDYXRlfS8+XG4gICAgICAgICAgICAgICAgICAgIHtxdW90ZU5vfVxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpdG50LWJhZGdlXCI+e2JpelR5cGV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICA8Q29sIHN0eWxlPXt7IGZsb2F0OiBcInJpZ2h0XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgIHtgJHtwcm9wcy5wb2xpY3kuY3RudFN0cnVjdENvZGUgfHwgXCJcIn0gXG4gICAgICAgICAgICAgICAgICAgICR7cHJvcHMucG9saWN5LmN0bnRTdHJ1Y3RDb2RlICYmIHByb3BzLnBvbGljeS5jdG50U3RydWN0TmFtZSA/IFwiLVwiIDogXCJcIn0gXG4gICAgICAgICAgICAgICAgICAgICR7cHJvcHMucG9saWN5LmN0bnRTdHJ1Y3ROYW1lIHx8IFwiXCJ9YH1cbiAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICA8L3RoPlxuICAgICAgICAgICAgPC90cj5cblxuICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICA8dGQgc3R5bGU9e3sgd2lkdGg6IGlzTW9iaWxlID8gXCIxMDAlXCIgOiBcIjMyJVwiLCB0ZXh0QWxpZ246IFwibGVmdFwiIH19PntyZW5kZXJQb2xpY3lNYWluSW5mbygpfTwvdGQ+XG4gICAgICAgICAgICAgIDx0ZCBzdHlsZT17e1xuICAgICAgICAgICAgICAgIHdpZHRoOiBpc01vYmlsZSA/IFwiMTAwJVwiIDogXCIyMCVcIixcbiAgICAgICAgICAgICAgICB0ZXh0QWxpZ246IGlzTW9iaWxlID8gXCJsZWZ0XCIgOiBcInJpZ2h0XCIsXG4gICAgICAgICAgICAgIH19PntVdGlscy5yZW5kZXJQb2xpY3lQcmVtaXVtKHByb3BzLnBvbGljeSl9PC90ZD5cbiAgICAgICAgICAgICAgPHRkIHN0eWxlPXt7IHdpZHRoOiBpc01vYmlsZSA/IFwiMTAwJVwiIDogXCIyOCVcIiwgdGV4dEFsaWduOiBpc01vYmlsZSA/IFwibGVmdFwiIDogXCJjZW50ZXJcIiB9fT5cbiAgICAgICAgICAgICAgICA8c3Ryb25nIHN0eWxlPXt7IGZvbnRXZWlnaHQ6IDcwMCB9fT57c3RhdHVzTmFtZX08L3N0cm9uZz5cbiAgICAgICAgICAgICAgICB7c3ViU3RhdHVzTmFtZSAmJiA8cCBzdHlsZT17eyBmb250U2l6ZTogMTIsIGNvbG9yOiBcIiM5OTlcIiB9fT57c3ViU3RhdHVzTmFtZX08L3A+fVxuICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRpdGxlLS1zdWJcIiBzdHlsZT17eyBwYWRkaW5nTGVmdDogMCB9fT5cbiAgICAgICAgICAgICAgICAgIHtsYXN0VXBkYXRlZEF0fVxuICAgICAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgICAgICAgPHRkPlxuICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImJ1dHRvblwiIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHtcbiAgICAgICAgICAgICAgICAgICAgcHJvZHVjdENvZGUsXG4gICAgICAgICAgICAgICAgICAgIHJlZklkLFxuICAgICAgICAgICAgICAgICAgICBiaXpUeXBlLFxuICAgICAgICAgICAgICAgICAgICBpdG50Q29kZSxcbiAgICAgICAgICAgICAgICAgICAgcHJvZHVjdFZlcnNpb24sXG4gICAgICAgICAgICAgICAgICB9ID0gcHJvcHMucG9saWN5O1xuICAgICAgICAgICAgICAgICAgY29uc3QgX3VybCA9IFBBVEguTk9STUFMX1FVT1RFLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRudENvZGUpXG4gICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByZHRDb2RlXCIsIHByb2R1Y3RDb2RlKVxuICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcIi86cG9saWN5SWQ/XCIsIFwiP2Nsb25lUG9saWN5SWQ9XCIgKyByZWZJZCk7XG4gICAgICAgICAgICAgICAgICBwcm9wcy5yb3V0ZXIucHVzaFJlZGlyZWN0KF91cmwpO1xuICAgICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiQ2xvbmVcIikudGhhaShcIkNsb25lXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvdGQ+XG4gICAgICAgICAgICAgIDx0ZCBzdHlsZT17eyB3aWR0aDogaXNNb2JpbGUgPyBcIjEwMCVcIiA6IFwiMjAlXCIsIHRleHRBbGlnbjogaXNNb2JpbGUgPyBcImxlZnRcIiA6IFwiY2VudGVyXCIgfX0+XG4gICAgICAgICAgICAgICAge3JlbmRlck9wZXJzKHByb3BzLnBvbGljeSwgcHJvcHMuaXNFQ2FyZCwgcHJvcHMucG9saWN5SWQpfVxuICAgICAgICAgICAgICA8L3RkPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICA8L1JlYWN0LkZyYWdtZW50PlxuICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgIDwvdGFibGU+XG4gICAgICA8L2Rpdj5cbiAgICA8L1N0eWxlUG9saWN5SXRlbT5cbiAgKTtcbn07XG5cbmZ1bmN0aW9uIHJlbmRlck9wZXJzKGl0ZW06IGFueSwgaXNFQ2FyZD86IGFueSwgcG9saWN5SWQ/OiBhbnkpIHtcblxuICBjb25zdCByZXN1bHQgPSBbXTtcbiAgbGV0IG1vcmVPcGVyID0gW107XG4gIGNvbnN0IG91dHB1dFR5cGVzID0gXy5nZXQoaXRlbSwgXCJvdXRwdXRUeXBlc1wiLCBbXSk7XG4gIGNvbnN0IGJpelR5cGUgPSBfLmdldChpdGVtLCBcImJpelR5cGVcIik7XG4gIG1vcmVPcGVyID0gb3V0cHV0VHlwZXMubWFwKChvdXRwdXQ6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgIGNvbnN0IHBkZlVybCA9IEFqYXguYXBwZW5kQXV0aFRvVXJsKGJpelR5cGUgPT09IFwiRU5ET1wiXG4gICAgICA/IGAvZW5kby8ke18uZ2V0KGl0ZW0sIFwicmVmSWRcIil9L3BkZi8ke18uZ2V0KG91dHB1dCwgXCJuYW1lXCIpfWBcbiAgICAgIDogYC9wb2xpY2llcy8ke18uZ2V0KGl0ZW0sIFwicmVmSWRcIil9L3BkZi8ke18uZ2V0KG91dHB1dCwgXCJuYW1lXCIpfWApO1xuICAgIHJldHVybiA8TWVudS5JdGVtIGtleT17YHJlamVjdGFibGUke2luZGV4fWB9PlxuICAgICAgPGEgaHJlZj17cGRmVXJsfSBjbGFzc05hbWU9XCJidXR0b25cIj5cbiAgICAgICAge18uZ2V0KG91dHB1dCwgXCJkaXNwbGF5TmFtZVwiKX1cbiAgICAgIDwvYT5cbiAgICA8L01lbnUuSXRlbT47XG4gIH0pO1xuXG4gIGlmIChpc0VDYXJkKSB7XG4gICAgY29uc3QgcGRmVXJsID0gQWpheC5hcHBlbmRBdXRoVG9VcmwoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9wYXNza2l0L2FwcGxld2FsbGV0YCk7XG5cbiAgICBtb3JlT3Blci5wdXNoKFxuICAgICAgPE1lbnUuSXRlbSBrZXk9XCJyZWplY3RhYmxlRUNhcmRcIj5cbiAgICAgICAgPGEgaHJlZj17cGRmVXJsfSBjbGFzc05hbWU9XCJidXR0b25cIj5cbiAgICAgICAgICBFLWNhcmRcbiAgICAgICAgPC9hPlxuICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICk7XG4gIH1cblxuICBjb25zdCBtZW51ID0gPE1lbnU+e21vcmVPcGVyfTwvTWVudT47XG4gIGlmIChtb3JlT3Blci5sZW5ndGggPiAwKSB7XG4gICAgcmVzdWx0LnB1c2goXG4gICAgICA8RHJvcGRvd24gb3ZlcmxheT17bWVudX0ga2V5PVwiZHJvcGRvd25cIiB0cmlnZ2VyPXtbXCJob3ZlclwiLCBcImNsaWNrXCJdfT5cbiAgICAgICAgPGEgY2xhc3NOYW1lPXtgYW50LWRyb3Bkb3duLWxpbmsgYnV0dG9uYH0gaHJlZj1cImphdmFzY3JpcHQ6O1wiPlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIk91dHB1dHNcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9e1wiIFwifVxuICAgICAgICAgIDxJY29uIHR5cGU9XCJkb3duXCIvPlxuICAgICAgICA8L2E+XG4gICAgICA8L0Ryb3Bkb3duPixcbiAgICApO1xuICB9XG5cbiAgaWYgKCFyZXN1bHQubGVuZ3RoKSB7XG4gICAgcmVzdWx0LnB1c2goXCJcIik7XG4gIH1cblxuICByZXR1cm4gVXRpbHMuam9pbkVsZW1lbnRzKHJlc3VsdCwgPERpdmlkZXIgdHlwZT1cInZlcnRpY2FsXCIvPik7XG59XG5cblxuZXhwb3J0IGNvbnN0IFBvbGljeUl0ZW1zID0gKHByb3BzOiB7IG1vZGVsOiBhbnksIGlzRUNhcmQ/OiBhbnksIHBvbGljeUlkPzogYW55LCByb3V0ZXI6IGFueSB9KSA9PiB7XG4gIGNvbnN0IGl0ZW1zID0gXy5nZXQocHJvcHMubW9kZWwsIFwiaXRlbXNcIiwgW10pIHx8IFtdO1xuICByZXR1cm4gPD5cbiAgICB7aXRlbXMubWFwKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IDxQb2xpY3lJdGVtIGtleT17aW5kZXh9IG1vZGVsPXtwcm9wcy5tb2RlbH0gcG9saWN5SWQ9e3Byb3BzLnBvbGljeUlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm91dGVyPXtwcm9wcy5yb3V0ZXJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0VDYXJkPXtwcm9wcy5pc0VDYXJkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9saWN5PXtpdGVtfS8+KX1cbiAgPC8+O1xufTtcblxuXG5jb25zdCBTdHlsZVBvbGljeUl0ZW0gPSBTdHlsZWQuZGl2YFxubWFyZ2luLWJvdHRvbTogMjBweDtcbiAgXG4gICAgICAgICAgICAgICAgIC5saXN0Q29udGFpbmVyIHtcbiAgICAgICAgICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxZW07XG4gICAgICAgICAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRib2R5IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLml0bnQtbG9nby13cmFwIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAgICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgICAgICAgICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWluLWhlaWdodDogMy41ZW07XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDY1cHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGQge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogNXB4IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgdGgge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDhweCAxNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmY2ZjZmM7XG4gICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmVmZWY7XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItd2lkdGg6IDFweCAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM4Nzg3ODc7XG4gICAgICAgICAgICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItbGVmdC13aWR0aDogMXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLml0bnQtYmFkZ2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogNzUlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAgM3B4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCA1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IDFweDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGQge1xuICAgICAgICAgICAgICAgICAgICAgICAgYSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IGdyZXk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJvbmcge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGltZyB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNTAlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDU1cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAudGl0bGUge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweCAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd29yZC1icmVhazogYnJlYWstd29yZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDAgMCAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogLjVlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjhweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC50aXRsZS0tc3ViIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjQ1KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAuNWVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyOHB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAubW9iaWxlLWxpc3QtY29udGFpbmVyIHtcbiAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgICAgICAgICAgICAgICB0ZCB7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiA3NXB4O1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogMXB4O1xuICAgICAgICAgICAgICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZWJlYmViO1xuICAgICAgICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC50aXRsZSwudGl0bGUtLXN1YiB7XG4gICAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI4cHg7XG4gICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgdGJvZHkge1xuICAgICAgICAgICAgICAgICAgICB0cjpudGgtY2hpbGQoMikge1xuICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgIFxuYDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU9BO0FBQ0E7QUFFQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/success/components/policy-item.tsx
