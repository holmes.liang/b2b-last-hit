/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Model = __webpack_require__(/*! ../../model/Model */ "./node_modules/echarts/lib/model/Model.js");

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var isNameSpecified = _model.isNameSpecified;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var LegendModel = echarts.extendComponentModel({
  type: 'legend.plain',
  dependencies: ['series'],
  layoutMode: {
    type: 'box',
    // legend.width/height are maxWidth/maxHeight actually,
    // whereas realy width/height is calculated by its content.
    // (Setting {left: 10, right: 10} does not make sense).
    // So consider the case:
    // `setOption({legend: {left: 10});`
    // then `setOption({legend: {right: 10});`
    // The previous `left` should be cleared by setting `ignoreSize`.
    ignoreSize: true
  },
  init: function init(option, parentModel, ecModel) {
    this.mergeDefaultAndTheme(option, ecModel);
    option.selected = option.selected || {};
  },
  mergeOption: function mergeOption(option) {
    LegendModel.superCall(this, 'mergeOption', option);
  },
  optionUpdated: function optionUpdated() {
    this._updateData(this.ecModel);

    var legendData = this._data; // If selectedMode is single, try to select one

    if (legendData[0] && this.get('selectedMode') === 'single') {
      var hasSelected = false; // If has any selected in option.selected

      for (var i = 0; i < legendData.length; i++) {
        var name = legendData[i].get('name');

        if (this.isSelected(name)) {
          // Force to unselect others
          this.select(name);
          hasSelected = true;
          break;
        }
      } // Try select the first if selectedMode is single


      !hasSelected && this.select(legendData[0].get('name'));
    }
  },
  _updateData: function _updateData(ecModel) {
    var potentialData = [];
    var availableNames = [];
    ecModel.eachRawSeries(function (seriesModel) {
      var seriesName = seriesModel.name;
      availableNames.push(seriesName);
      var isPotential;

      if (seriesModel.legendDataProvider) {
        var data = seriesModel.legendDataProvider();
        var names = data.mapArray(data.getName);

        if (!ecModel.isSeriesFiltered(seriesModel)) {
          availableNames = availableNames.concat(names);
        }

        if (names.length) {
          potentialData = potentialData.concat(names);
        } else {
          isPotential = true;
        }
      } else {
        isPotential = true;
      }

      if (isPotential && isNameSpecified(seriesModel)) {
        potentialData.push(seriesModel.name);
      }
    });
    /**
     * @type {Array.<string>}
     * @private
     */

    this._availableNames = availableNames; // If legend.data not specified in option, use availableNames as data,
    // which is convinient for user preparing option.

    var rawData = this.get('data') || potentialData;
    var legendData = zrUtil.map(rawData, function (dataItem) {
      // Can be string or number
      if (typeof dataItem === 'string' || typeof dataItem === 'number') {
        dataItem = {
          name: dataItem
        };
      }

      return new Model(dataItem, this, this.ecModel);
    }, this);
    /**
     * @type {Array.<module:echarts/model/Model>}
     * @private
     */

    this._data = legendData;
  },

  /**
   * @return {Array.<module:echarts/model/Model>}
   */
  getData: function getData() {
    return this._data;
  },

  /**
   * @param {string} name
   */
  select: function select(name) {
    var selected = this.option.selected;
    var selectedMode = this.get('selectedMode');

    if (selectedMode === 'single') {
      var data = this._data;
      zrUtil.each(data, function (dataItem) {
        selected[dataItem.get('name')] = false;
      });
    }

    selected[name] = true;
  },

  /**
   * @param {string} name
   */
  unSelect: function unSelect(name) {
    if (this.get('selectedMode') !== 'single') {
      this.option.selected[name] = false;
    }
  },

  /**
   * @param {string} name
   */
  toggleSelected: function toggleSelected(name) {
    var selected = this.option.selected; // Default is true

    if (!selected.hasOwnProperty(name)) {
      selected[name] = true;
    }

    this[selected[name] ? 'unSelect' : 'select'](name);
  },

  /**
   * @param {string} name
   */
  isSelected: function isSelected(name) {
    var selected = this.option.selected;
    return !(selected.hasOwnProperty(name) && !selected[name]) && zrUtil.indexOf(this._availableNames, name) >= 0;
  },
  defaultOption: {
    // 一级层叠
    zlevel: 0,
    // 二级层叠
    z: 4,
    show: true,
    // 布局方式，默认为水平布局，可选为：
    // 'horizontal' | 'vertical'
    orient: 'horizontal',
    left: 'center',
    // right: 'center',
    top: 0,
    // bottom: null,
    // 水平对齐
    // 'auto' | 'left' | 'right'
    // 默认为 'auto', 根据 x 的位置判断是左对齐还是右对齐
    align: 'auto',
    backgroundColor: 'rgba(0,0,0,0)',
    // 图例边框颜色
    borderColor: '#ccc',
    borderRadius: 0,
    // 图例边框线宽，单位px，默认为0（无边框）
    borderWidth: 0,
    // 图例内边距，单位px，默认各方向内边距为5，
    // 接受数组分别设定上右下左边距，同css
    padding: 5,
    // 各个item之间的间隔，单位px，默认为10，
    // 横向布局时为水平间隔，纵向布局时为纵向间隔
    itemGap: 10,
    // 图例图形宽度
    itemWidth: 25,
    // 图例图形高度
    itemHeight: 14,
    // 图例关闭时候的颜色
    inactiveColor: '#ccc',
    textStyle: {
      // 图例文字颜色
      color: '#333'
    },
    // formatter: '',
    // 选择模式，默认开启图例开关
    selectedMode: true,
    // 配置默认选中状态，可配合LEGEND.SELECTED事件做动态数据载入
    // selected: null,
    // 图例内容（详见legend.data，数组中每一项代表一个item
    // data: [],
    // Tooltip 相关配置
    tooltip: {
      show: false
    }
  }
});
var _default = LegendModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZC9MZWdlbmRNb2RlbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmQvTGVnZW5kTW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBlY2hhcnRzID0gcmVxdWlyZShcIi4uLy4uL2VjaGFydHNcIik7XG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgTW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvTW9kZWxcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIGlzTmFtZVNwZWNpZmllZCA9IF9tb2RlbC5pc05hbWVTcGVjaWZpZWQ7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBMZWdlbmRNb2RlbCA9IGVjaGFydHMuZXh0ZW5kQ29tcG9uZW50TW9kZWwoe1xuICB0eXBlOiAnbGVnZW5kLnBsYWluJyxcbiAgZGVwZW5kZW5jaWVzOiBbJ3NlcmllcyddLFxuICBsYXlvdXRNb2RlOiB7XG4gICAgdHlwZTogJ2JveCcsXG4gICAgLy8gbGVnZW5kLndpZHRoL2hlaWdodCBhcmUgbWF4V2lkdGgvbWF4SGVpZ2h0IGFjdHVhbGx5LFxuICAgIC8vIHdoZXJlYXMgcmVhbHkgd2lkdGgvaGVpZ2h0IGlzIGNhbGN1bGF0ZWQgYnkgaXRzIGNvbnRlbnQuXG4gICAgLy8gKFNldHRpbmcge2xlZnQ6IDEwLCByaWdodDogMTB9IGRvZXMgbm90IG1ha2Ugc2Vuc2UpLlxuICAgIC8vIFNvIGNvbnNpZGVyIHRoZSBjYXNlOlxuICAgIC8vIGBzZXRPcHRpb24oe2xlZ2VuZDoge2xlZnQ6IDEwfSk7YFxuICAgIC8vIHRoZW4gYHNldE9wdGlvbih7bGVnZW5kOiB7cmlnaHQ6IDEwfSk7YFxuICAgIC8vIFRoZSBwcmV2aW91cyBgbGVmdGAgc2hvdWxkIGJlIGNsZWFyZWQgYnkgc2V0dGluZyBgaWdub3JlU2l6ZWAuXG4gICAgaWdub3JlU2l6ZTogdHJ1ZVxuICB9LFxuICBpbml0OiBmdW5jdGlvbiAob3B0aW9uLCBwYXJlbnRNb2RlbCwgZWNNb2RlbCkge1xuICAgIHRoaXMubWVyZ2VEZWZhdWx0QW5kVGhlbWUob3B0aW9uLCBlY01vZGVsKTtcbiAgICBvcHRpb24uc2VsZWN0ZWQgPSBvcHRpb24uc2VsZWN0ZWQgfHwge307XG4gIH0sXG4gIG1lcmdlT3B0aW9uOiBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgTGVnZW5kTW9kZWwuc3VwZXJDYWxsKHRoaXMsICdtZXJnZU9wdGlvbicsIG9wdGlvbik7XG4gIH0sXG4gIG9wdGlvblVwZGF0ZWQ6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLl91cGRhdGVEYXRhKHRoaXMuZWNNb2RlbCk7XG5cbiAgICB2YXIgbGVnZW5kRGF0YSA9IHRoaXMuX2RhdGE7IC8vIElmIHNlbGVjdGVkTW9kZSBpcyBzaW5nbGUsIHRyeSB0byBzZWxlY3Qgb25lXG5cbiAgICBpZiAobGVnZW5kRGF0YVswXSAmJiB0aGlzLmdldCgnc2VsZWN0ZWRNb2RlJykgPT09ICdzaW5nbGUnKSB7XG4gICAgICB2YXIgaGFzU2VsZWN0ZWQgPSBmYWxzZTsgLy8gSWYgaGFzIGFueSBzZWxlY3RlZCBpbiBvcHRpb24uc2VsZWN0ZWRcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZWdlbmREYXRhLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBuYW1lID0gbGVnZW5kRGF0YVtpXS5nZXQoJ25hbWUnKTtcblxuICAgICAgICBpZiAodGhpcy5pc1NlbGVjdGVkKG5hbWUpKSB7XG4gICAgICAgICAgLy8gRm9yY2UgdG8gdW5zZWxlY3Qgb3RoZXJzXG4gICAgICAgICAgdGhpcy5zZWxlY3QobmFtZSk7XG4gICAgICAgICAgaGFzU2VsZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9IC8vIFRyeSBzZWxlY3QgdGhlIGZpcnN0IGlmIHNlbGVjdGVkTW9kZSBpcyBzaW5nbGVcblxuXG4gICAgICAhaGFzU2VsZWN0ZWQgJiYgdGhpcy5zZWxlY3QobGVnZW5kRGF0YVswXS5nZXQoJ25hbWUnKSk7XG4gICAgfVxuICB9LFxuICBfdXBkYXRlRGF0YTogZnVuY3Rpb24gKGVjTW9kZWwpIHtcbiAgICB2YXIgcG90ZW50aWFsRGF0YSA9IFtdO1xuICAgIHZhciBhdmFpbGFibGVOYW1lcyA9IFtdO1xuICAgIGVjTW9kZWwuZWFjaFJhd1NlcmllcyhmdW5jdGlvbiAoc2VyaWVzTW9kZWwpIHtcbiAgICAgIHZhciBzZXJpZXNOYW1lID0gc2VyaWVzTW9kZWwubmFtZTtcbiAgICAgIGF2YWlsYWJsZU5hbWVzLnB1c2goc2VyaWVzTmFtZSk7XG4gICAgICB2YXIgaXNQb3RlbnRpYWw7XG5cbiAgICAgIGlmIChzZXJpZXNNb2RlbC5sZWdlbmREYXRhUHJvdmlkZXIpIHtcbiAgICAgICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5sZWdlbmREYXRhUHJvdmlkZXIoKTtcbiAgICAgICAgdmFyIG5hbWVzID0gZGF0YS5tYXBBcnJheShkYXRhLmdldE5hbWUpO1xuXG4gICAgICAgIGlmICghZWNNb2RlbC5pc1Nlcmllc0ZpbHRlcmVkKHNlcmllc01vZGVsKSkge1xuICAgICAgICAgIGF2YWlsYWJsZU5hbWVzID0gYXZhaWxhYmxlTmFtZXMuY29uY2F0KG5hbWVzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChuYW1lcy5sZW5ndGgpIHtcbiAgICAgICAgICBwb3RlbnRpYWxEYXRhID0gcG90ZW50aWFsRGF0YS5jb25jYXQobmFtZXMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlzUG90ZW50aWFsID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaXNQb3RlbnRpYWwgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNQb3RlbnRpYWwgJiYgaXNOYW1lU3BlY2lmaWVkKHNlcmllc01vZGVsKSkge1xuICAgICAgICBwb3RlbnRpYWxEYXRhLnB1c2goc2VyaWVzTW9kZWwubmFtZSk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgLyoqXG4gICAgICogQHR5cGUge0FycmF5LjxzdHJpbmc+fVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG5cbiAgICB0aGlzLl9hdmFpbGFibGVOYW1lcyA9IGF2YWlsYWJsZU5hbWVzOyAvLyBJZiBsZWdlbmQuZGF0YSBub3Qgc3BlY2lmaWVkIGluIG9wdGlvbiwgdXNlIGF2YWlsYWJsZU5hbWVzIGFzIGRhdGEsXG4gICAgLy8gd2hpY2ggaXMgY29udmluaWVudCBmb3IgdXNlciBwcmVwYXJpbmcgb3B0aW9uLlxuXG4gICAgdmFyIHJhd0RhdGEgPSB0aGlzLmdldCgnZGF0YScpIHx8IHBvdGVudGlhbERhdGE7XG4gICAgdmFyIGxlZ2VuZERhdGEgPSB6clV0aWwubWFwKHJhd0RhdGEsIGZ1bmN0aW9uIChkYXRhSXRlbSkge1xuICAgICAgLy8gQ2FuIGJlIHN0cmluZyBvciBudW1iZXJcbiAgICAgIGlmICh0eXBlb2YgZGF0YUl0ZW0gPT09ICdzdHJpbmcnIHx8IHR5cGVvZiBkYXRhSXRlbSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgZGF0YUl0ZW0gPSB7XG4gICAgICAgICAgbmFtZTogZGF0YUl0ZW1cbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ldyBNb2RlbChkYXRhSXRlbSwgdGhpcywgdGhpcy5lY01vZGVsKTtcbiAgICB9LCB0aGlzKTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7QXJyYXkuPG1vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsPn1cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuXG4gICAgdGhpcy5fZGF0YSA9IGxlZ2VuZERhdGE7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbD59XG4gICAqL1xuICBnZXREYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2RhdGE7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqL1xuICBzZWxlY3Q6IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdmFyIHNlbGVjdGVkID0gdGhpcy5vcHRpb24uc2VsZWN0ZWQ7XG4gICAgdmFyIHNlbGVjdGVkTW9kZSA9IHRoaXMuZ2V0KCdzZWxlY3RlZE1vZGUnKTtcblxuICAgIGlmIChzZWxlY3RlZE1vZGUgPT09ICdzaW5nbGUnKSB7XG4gICAgICB2YXIgZGF0YSA9IHRoaXMuX2RhdGE7XG4gICAgICB6clV0aWwuZWFjaChkYXRhLCBmdW5jdGlvbiAoZGF0YUl0ZW0pIHtcbiAgICAgICAgc2VsZWN0ZWRbZGF0YUl0ZW0uZ2V0KCduYW1lJyldID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBzZWxlY3RlZFtuYW1lXSA9IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqL1xuICB1blNlbGVjdDogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICBpZiAodGhpcy5nZXQoJ3NlbGVjdGVkTW9kZScpICE9PSAnc2luZ2xlJykge1xuICAgICAgdGhpcy5vcHRpb24uc2VsZWN0ZWRbbmFtZV0gPSBmYWxzZTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqL1xuICB0b2dnbGVTZWxlY3RlZDogZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB2YXIgc2VsZWN0ZWQgPSB0aGlzLm9wdGlvbi5zZWxlY3RlZDsgLy8gRGVmYXVsdCBpcyB0cnVlXG5cbiAgICBpZiAoIXNlbGVjdGVkLmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICBzZWxlY3RlZFtuYW1lXSA9IHRydWU7XG4gICAgfVxuXG4gICAgdGhpc1tzZWxlY3RlZFtuYW1lXSA/ICd1blNlbGVjdCcgOiAnc2VsZWN0J10obmFtZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqL1xuICBpc1NlbGVjdGVkOiBmdW5jdGlvbiAobmFtZSkge1xuICAgIHZhciBzZWxlY3RlZCA9IHRoaXMub3B0aW9uLnNlbGVjdGVkO1xuICAgIHJldHVybiAhKHNlbGVjdGVkLmhhc093blByb3BlcnR5KG5hbWUpICYmICFzZWxlY3RlZFtuYW1lXSkgJiYgenJVdGlsLmluZGV4T2YodGhpcy5fYXZhaWxhYmxlTmFtZXMsIG5hbWUpID49IDA7XG4gIH0sXG4gIGRlZmF1bHRPcHRpb246IHtcbiAgICAvLyDkuIDnuqflsYLlj6BcbiAgICB6bGV2ZWw6IDAsXG4gICAgLy8g5LqM57qn5bGC5Y+gXG4gICAgejogNCxcbiAgICBzaG93OiB0cnVlLFxuICAgIC8vIOW4g+WxgOaWueW8j++8jOm7mOiupOS4uuawtOW5s+W4g+WxgO+8jOWPr+mAieS4uu+8mlxuICAgIC8vICdob3Jpem9udGFsJyB8ICd2ZXJ0aWNhbCdcbiAgICBvcmllbnQ6ICdob3Jpem9udGFsJyxcbiAgICBsZWZ0OiAnY2VudGVyJyxcbiAgICAvLyByaWdodDogJ2NlbnRlcicsXG4gICAgdG9wOiAwLFxuICAgIC8vIGJvdHRvbTogbnVsbCxcbiAgICAvLyDmsLTlubPlr7npvZBcbiAgICAvLyAnYXV0bycgfCAnbGVmdCcgfCAncmlnaHQnXG4gICAgLy8g6buY6K6k5Li6ICdhdXRvJywg5qC55o2uIHgg55qE5L2N572u5Yik5pat5piv5bem5a+56b2Q6L+Y5piv5Y+z5a+56b2QXG4gICAgYWxpZ246ICdhdXRvJyxcbiAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsMCwwLDApJyxcbiAgICAvLyDlm77kvovovrnmoYbpopzoibJcbiAgICBib3JkZXJDb2xvcjogJyNjY2MnLFxuICAgIGJvcmRlclJhZGl1czogMCxcbiAgICAvLyDlm77kvovovrnmoYbnur/lrr3vvIzljZXkvY1weO+8jOm7mOiupOS4ujDvvIjml6DovrnmoYbvvIlcbiAgICBib3JkZXJXaWR0aDogMCxcbiAgICAvLyDlm77kvovlhoXovrnot53vvIzljZXkvY1weO+8jOm7mOiupOWQhOaWueWQkeWGhei+uei3neS4ujXvvIxcbiAgICAvLyDmjqXlj5fmlbDnu4TliIbliKvorr7lrprkuIrlj7PkuIvlt6bovrnot53vvIzlkIxjc3NcbiAgICBwYWRkaW5nOiA1LFxuICAgIC8vIOWQhOS4qml0ZW3kuYvpl7TnmoTpl7TpmpTvvIzljZXkvY1weO+8jOm7mOiupOS4ujEw77yMXG4gICAgLy8g5qiq5ZCR5biD5bGA5pe25Li65rC05bmz6Ze06ZqU77yM57q15ZCR5biD5bGA5pe25Li657q15ZCR6Ze06ZqUXG4gICAgaXRlbUdhcDogMTAsXG4gICAgLy8g5Zu+5L6L5Zu+5b2i5a695bqmXG4gICAgaXRlbVdpZHRoOiAyNSxcbiAgICAvLyDlm77kvovlm77lvaLpq5jluqZcbiAgICBpdGVtSGVpZ2h0OiAxNCxcbiAgICAvLyDlm77kvovlhbPpl63ml7blgJnnmoTpopzoibJcbiAgICBpbmFjdGl2ZUNvbG9yOiAnI2NjYycsXG4gICAgdGV4dFN0eWxlOiB7XG4gICAgICAvLyDlm77kvovmloflrZfpopzoibJcbiAgICAgIGNvbG9yOiAnIzMzMydcbiAgICB9LFxuICAgIC8vIGZvcm1hdHRlcjogJycsXG4gICAgLy8g6YCJ5oup5qih5byP77yM6buY6K6k5byA5ZCv5Zu+5L6L5byA5YWzXG4gICAgc2VsZWN0ZWRNb2RlOiB0cnVlLFxuICAgIC8vIOmFjee9rum7mOiupOmAieS4reeKtuaAge+8jOWPr+mFjeWQiExFR0VORC5TRUxFQ1RFROS6i+S7tuWBmuWKqOaAgeaVsOaNrui9veWFpVxuICAgIC8vIHNlbGVjdGVkOiBudWxsLFxuICAgIC8vIOWbvuS+i+WGheWuue+8iOivpuingWxlZ2VuZC5kYXRh77yM5pWw57uE5Lit5q+P5LiA6aG55Luj6KGo5LiA5LiqaXRlbVxuICAgIC8vIGRhdGE6IFtdLFxuICAgIC8vIFRvb2x0aXAg55u45YWz6YWN572uXG4gICAgdG9vbHRpcDoge1xuICAgICAgc2hvdzogZmFsc2VcbiAgICB9XG4gIH1cbn0pO1xudmFyIF9kZWZhdWx0ID0gTGVnZW5kTW9kZWw7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBL0NBO0FBekpBO0FBNk1BO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/legend/LegendModel.js
