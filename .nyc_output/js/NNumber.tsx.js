__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NNumber", function() { return NNumber; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _desk_login_sign_in__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../desk/login/sign-in */ "./src/app/desk/login/sign-in.tsx");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NNumber.tsx";

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__["default"])(["\n  .ant-input-number {\n   ", ";\n   ", ";\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__["default"])(["\n      .ant-input-number {\n        width: 100%;\n      }\n      .ant-input-number-handler-wrap {\n        display: ", ";\n      }\n      .ant-row {\n        .ant-col:first-child {\n          height: ", ";\n        }\n      }\n      .ant-form-item-label {\n        text-transform: uppercase;\n      }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_6__["default"])(["\n    input {\n      height: 36px;\n      border: none;\n      background: transparent;\n      color: #989898 !important;\n      -webkit-text-fill-color: white;\n    \n      &:focus {\n        border: none;\n        box-shadow: none;\n      }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}






var LoginInput = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject());

var NNumber =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(NNumber, _ModelWidget);

  function NNumber(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, NNumber);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(NNumber).call(this, props, context));

    _this.renderBox = function (inner) {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(StyleNumberInput, {
        addonBefore: _this.props.addonBefore,
        addonAfter: _this.props.addonAfter,
        className: "ant-input-group-wrapper",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "ant-input-wrapper ant-input-group",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, _this.props.addonBefore && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "ant-input-group-addon",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      }, _this.props.addonBefore), inner, _this.props.addonAfter && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "ant-input-group-addon",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _this.props.addonAfter)));
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(NNumber, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        LoginInput: LoginInput,
        BoxDiv: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject2(), this.props.maxMinStyle ? "block" : "none", this.props.maxMinStyle ? "40px" : "auto")
      };
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          label = _this$props.label,
          propName = _this$props.propName,
          onChange = _this$props.onChange,
          rules = _this$props.rules,
          required = _this$props.required,
          layoutCol = _this$props.layoutCol,
          placeholder = _this$props.placeholder,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["form", "model", "label", "propName", "onChange", "rules", "required", "layoutCol", "placeholder"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.BoxDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], Object.assign({
        form: form,
        model: model,
        label: label,
        propName: propName,
        onChange: onChange,
        rules: rules,
        required: required,
        layoutCol: layoutCol
      }, {
        renderBox: this.renderBox,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["InputNumber"], Object.assign({}, rest, {
        placeholder: placeholder,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }))));
    }
  }]);

  return NNumber;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);


var StyleNumberInput = styled_components__WEBPACK_IMPORTED_MODULE_11__["default"].div(_templateObject3(), function (props) {
  return props.addonBefore ? "border-top-left-radius: 0;border-bottom-left-radius: 0;" : "";
}, function (props) {
  return props.addonAfter ? "border-top-right-radius: 0;border-bottom-right-radius: 0;" : "";
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9OTnVtYmVyLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9jb21wb25lbnQvTk51bWJlci50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgSW5wdXROdW1iZXJQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9pbnB1dC1udW1iZXJcIjtcbmltcG9ydCB7IElucHV0TnVtYmVyIH0gZnJvbSBcImFudGRcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQYWdlQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IE5Gb3JtSXRlbVByb3BzIH0gZnJvbSBcIkBjb21wb25lbnQvTkZvcm1JdGVtXCI7XG5cbmltcG9ydCB7IFN0eWxlZElOUFVUIH0gZnJvbSBcIi4uL2Rlc2svbG9naW4vc2lnbi1pblwiO1xuaW1wb3J0IHN0eWxlZCBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcblxuZXhwb3J0IHR5cGUgTk51bWJlclByb3BzID0ge1xuICBhZGRvbkJlZm9yZT86IHN0cmluZztcbiAgYWRkb25BZnRlcj86IHN0cmluZztcbiAgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG4gIG1heE1pblN0eWxlPzogYm9vbGVhbjtcbn0gJiBORm9ybUl0ZW1Qcm9wcyAmIElucHV0TnVtYmVyUHJvcHM7XG5cbmV4cG9ydCB0eXBlIFN0eWxlZElOUFVUID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImlucHV0XCIsIGFueSwgeyByZWFkT25seT86IGJvb2xlYW4gfSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU3R5bGVkRGl2ID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHsgcmVhZE9ubHk/OiBib29sZWFuIH0sIG5ldmVyPjtcbmV4cG9ydCB0eXBlIE5OdW1iZXJQYWdlQ29tcG9uZW50cyA9IHtcbiAgTG9naW5JbnB1dDogU3R5bGVkSU5QVVQsXG4gIEJveERpdjogU3R5bGVkRGl2XG59ICYgUGFnZUNvbXBvbmVudHM7XG5cbmNvbnN0IExvZ2luSW5wdXQgPSBTdHlsZWQuZGl2YFxuICAgIGlucHV0IHtcbiAgICAgIGhlaWdodDogMzZweDtcbiAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgY29sb3I6ICM5ODk4OTggIWltcG9ydGFudDtcbiAgICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB3aGl0ZTtcbiAgICBcbiAgICAgICY6Zm9jdXMge1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICB9XG4gIH1cbmA7XG5cbmNsYXNzIE5OdW1iZXI8UCBleHRlbmRzIE5OdW1iZXJQcm9wcywgUywgQyBleHRlbmRzIE5OdW1iZXJQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBOTnVtYmVyUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIExvZ2luSW5wdXQsIEJveERpdjogU3R5bGVkLmRpdmBcbiAgICAgIC5hbnQtaW5wdXQtbnVtYmVyIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICB9XG4gICAgICAuYW50LWlucHV0LW51bWJlci1oYW5kbGVyLXdyYXAge1xuICAgICAgICBkaXNwbGF5OiAke3RoaXMucHJvcHMubWF4TWluU3R5bGUgPyBcImJsb2NrXCIgOiBcIm5vbmVcIn07XG4gICAgICB9XG4gICAgICAuYW50LXJvdyB7XG4gICAgICAgIC5hbnQtY29sOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICBoZWlnaHQ6ICR7dGhpcy5wcm9wcy5tYXhNaW5TdHlsZSA/IFwiNDBweFwiIDogXCJhdXRvXCJ9O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICAuYW50LWZvcm0taXRlbS1sYWJlbCB7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICB9XG4gICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICByZW5kZXJCb3ggPSAoaW5uZXI6IGFueSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICA8U3R5bGVOdW1iZXJJbnB1dCBhZGRvbkJlZm9yZT17dGhpcy5wcm9wcy5hZGRvbkJlZm9yZX0gYWRkb25BZnRlcj17dGhpcy5wcm9wcy5hZGRvbkFmdGVyfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYW50LWlucHV0LWdyb3VwLXdyYXBwZXJcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbnQtaW5wdXQtd3JhcHBlciBhbnQtaW5wdXQtZ3JvdXBcIj5cbiAgICAgICAgICB7dGhpcy5wcm9wcy5hZGRvbkJlZm9yZSAmJiA8c3BhbiBjbGFzc05hbWU9XCJhbnQtaW5wdXQtZ3JvdXAtYWRkb25cIj57dGhpcy5wcm9wcy5hZGRvbkJlZm9yZX08L3NwYW4+fVxuICAgICAgICAgIHtpbm5lcn1cbiAgICAgICAgICB7dGhpcy5wcm9wcy5hZGRvbkFmdGVyICYmIDxzcGFuIGNsYXNzTmFtZT1cImFudC1pbnB1dC1ncm91cC1hZGRvblwiPnt0aGlzLnByb3BzLmFkZG9uQWZ0ZXJ9PC9zcGFuPn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L1N0eWxlTnVtYmVySW5wdXQ+XG4gICAgKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuXG4gICAgY29uc3Qge1xuICAgICAgZm9ybSwgbW9kZWwsIGxhYmVsLCBwcm9wTmFtZSwgb25DaGFuZ2UsIHJ1bGVzLCByZXF1aXJlZCwgbGF5b3V0Q29sLCBwbGFjZWhvbGRlciwgLi4ucmVzdFxuICAgIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveERpdj5cbiAgICAgICAgPE5Gb3JtSXRlbSB7Li4ueyBmb3JtLCBtb2RlbCwgbGFiZWwsIHByb3BOYW1lLCBvbkNoYW5nZSwgcnVsZXMsIHJlcXVpcmVkLCBsYXlvdXRDb2wgfX1cbiAgICAgICAgICAgICAgICAgICByZW5kZXJCb3g9e3RoaXMucmVuZGVyQm94fT5cbiAgICAgICAgICA8SW5wdXROdW1iZXIgey4uLnJlc3R9IHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn0vPlxuICAgICAgICA8L05Gb3JtSXRlbT5cbiAgICAgIDwvQy5Cb3hEaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBOTnVtYmVyIH07XG5cbmNvbnN0IFN0eWxlTnVtYmVySW5wdXQ6IGFueSA9IHN0eWxlZC5kaXZgXG4gIC5hbnQtaW5wdXQtbnVtYmVyIHtcbiAgICR7KHByb3BzOiBhbnkpID0+IHByb3BzLmFkZG9uQmVmb3JlID8gXCJib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO2JvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XCIgOiBcIlwifTtcbiAgICR7KHByb3BzOiBhbnkpID0+IHByb3BzLmFkZG9uQWZ0ZXIgPyBcImJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO2JvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1wiIDogXCJcIn07XG4gIH1cbmA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFHQTtBQUVBO0FBSUE7QUFDQTtBQWdCQTtBQUNBO0FBY0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQTBCQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFyQ0E7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQURBO0FBa0JBOzs7QUFlQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7OztBQXREQTtBQUNBO0FBd0RBO0FBRUE7QUFFQTtBQUFBO0FBQ0E7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/component/NNumber.tsx
