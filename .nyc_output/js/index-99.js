__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Badge; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ScrollNumber__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ScrollNumber */ "./node_modules/antd/es/badge/ScrollNumber.js");
/* harmony import */ var _util_colors__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/colors */ "./node_modules/antd/es/_util/colors.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};










function isPresetColor(color) {
  return _util_colors__WEBPACK_IMPORTED_MODULE_6__["PresetColorTypes"].indexOf(color) !== -1;
}

var Badge =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Badge, _React$Component);

  function Badge() {
    var _this;

    _classCallCheck(this, Badge);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Badge).apply(this, arguments));

    _this.renderBadge = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          customizeScrollNumberPrefixCls = _a.scrollNumberPrefixCls,
          children = _a.children,
          status = _a.status,
          text = _a.text,
          color = _a.color,
          restProps = __rest(_a, ["prefixCls", "scrollNumberPrefixCls", "children", "status", "text", "color"]);

      var omitArr = ['count', 'showZero', 'overflowCount', 'className', 'style', 'dot', 'offset', 'title'];
      var prefixCls = getPrefixCls('badge', customizePrefixCls);
      var scrollNumberPrefixCls = getPrefixCls('scroll-number', customizeScrollNumberPrefixCls);

      var scrollNumber = _this.renderBadgeNumber(prefixCls, scrollNumberPrefixCls);

      var statusText = _this.renderStatusText(prefixCls);

      var statusCls = classnames__WEBPACK_IMPORTED_MODULE_4___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-status-dot"), _this.hasStatus()), _defineProperty(_classNames, "".concat(prefixCls, "-status-").concat(status), !!status), _defineProperty(_classNames, "".concat(prefixCls, "-status-").concat(color), isPresetColor(color)), _classNames));
      var statusStyle = {};

      if (color && !isPresetColor(color)) {
        statusStyle.background = color;
      } // <Badge status="success" />


      if (!children && _this.hasStatus()) {
        var styleWithOffset = _this.getStyleWithOffset();

        var statusTextColor = styleWithOffset && styleWithOffset.color;
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(restProps, omitArr), {
          className: _this.getBadgeClassName(prefixCls),
          style: styleWithOffset
        }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: statusCls,
          style: statusStyle
        }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          style: {
            color: statusTextColor
          },
          className: "".concat(prefixCls, "-status-text")
        }, text));
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(restProps, omitArr), {
        className: _this.getBadgeClassName(prefixCls)
      }), children, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_2__["default"], {
        component: "",
        showProp: "data-show",
        transitionName: children ? "".concat(prefixCls, "-zoom") : '',
        transitionAppear: true
      }, scrollNumber), statusText);
    };

    return _this;
  }

  _createClass(Badge, [{
    key: "getNumberedDispayCount",
    value: function getNumberedDispayCount() {
      var _this$props = this.props,
          count = _this$props.count,
          overflowCount = _this$props.overflowCount;
      var displayCount = count > overflowCount ? "".concat(overflowCount, "+") : count;
      return displayCount;
    }
  }, {
    key: "getDispayCount",
    value: function getDispayCount() {
      var isDot = this.isDot(); // dot mode don't need count

      if (isDot) {
        return '';
      }

      return this.getNumberedDispayCount();
    }
  }, {
    key: "getScrollNumberTitle",
    value: function getScrollNumberTitle() {
      var _this$props2 = this.props,
          title = _this$props2.title,
          count = _this$props2.count;

      if (title) {
        return title;
      }

      return typeof count === 'string' || typeof count === 'number' ? count : undefined;
    }
  }, {
    key: "getStyleWithOffset",
    value: function getStyleWithOffset() {
      var _this$props3 = this.props,
          offset = _this$props3.offset,
          style = _this$props3.style;
      return offset ? _extends({
        right: -parseInt(offset[0], 10),
        marginTop: offset[1]
      }, style) : style;
    }
  }, {
    key: "getBadgeClassName",
    value: function getBadgeClassName(prefixCls) {
      var _classNames2;

      var _this$props4 = this.props,
          className = _this$props4.className,
          children = _this$props4.children;
      return classnames__WEBPACK_IMPORTED_MODULE_4___default()(className, prefixCls, (_classNames2 = {}, _defineProperty(_classNames2, "".concat(prefixCls, "-status"), this.hasStatus()), _defineProperty(_classNames2, "".concat(prefixCls, "-not-a-wrapper"), !children), _classNames2));
    }
  }, {
    key: "hasStatus",
    value: function hasStatus() {
      var _this$props5 = this.props,
          status = _this$props5.status,
          color = _this$props5.color;
      return !!status || !!color;
    }
  }, {
    key: "isZero",
    value: function isZero() {
      var numberedDispayCount = this.getNumberedDispayCount();
      return numberedDispayCount === '0' || numberedDispayCount === 0;
    }
  }, {
    key: "isDot",
    value: function isDot() {
      var dot = this.props.dot;
      var isZero = this.isZero();
      return dot && !isZero || this.hasStatus();
    }
  }, {
    key: "isHidden",
    value: function isHidden() {
      var showZero = this.props.showZero;
      var displayCount = this.getDispayCount();
      var isZero = this.isZero();
      var isDot = this.isDot();
      var isEmpty = displayCount === null || displayCount === undefined || displayCount === '';
      return (isEmpty || isZero && !showZero) && !isDot;
    }
  }, {
    key: "renderStatusText",
    value: function renderStatusText(prefixCls) {
      var text = this.props.text;
      var hidden = this.isHidden();
      return hidden || !text ? null : react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-status-text")
      }, text);
    }
  }, {
    key: "renderDispayComponent",
    value: function renderDispayComponent() {
      var count = this.props.count;
      var customNode = count;

      if (!customNode || _typeof(customNode) !== 'object') {
        return undefined;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](customNode, {
        style: _extends(_extends({}, this.getStyleWithOffset()), customNode.props && customNode.props.style)
      });
    }
  }, {
    key: "renderBadgeNumber",
    value: function renderBadgeNumber(prefixCls, scrollNumberPrefixCls) {
      var _classNames3;

      var _this$props6 = this.props,
          status = _this$props6.status,
          count = _this$props6.count;
      var displayCount = this.getDispayCount();
      var isDot = this.isDot();
      var hidden = this.isHidden();
      var scrollNumberCls = classnames__WEBPACK_IMPORTED_MODULE_4___default()((_classNames3 = {}, _defineProperty(_classNames3, "".concat(prefixCls, "-dot"), isDot), _defineProperty(_classNames3, "".concat(prefixCls, "-count"), !isDot), _defineProperty(_classNames3, "".concat(prefixCls, "-multiple-words"), !isDot && count && count.toString && count.toString().length > 1), _defineProperty(_classNames3, "".concat(prefixCls, "-status-").concat(status), this.hasStatus()), _classNames3));
      return hidden ? null : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ScrollNumber__WEBPACK_IMPORTED_MODULE_5__["default"], {
        prefixCls: scrollNumberPrefixCls,
        "data-show": !hidden,
        className: scrollNumberCls,
        count: displayCount,
        displayComponent: this.renderDispayComponent() // <Badge status="success" count={<Icon type="xxx" />}></Badge>
        ,
        title: this.getScrollNumberTitle(),
        style: this.getStyleWithOffset(),
        key: "scrollNumber"
      });
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderBadge);
    }
  }]);

  return Badge;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Badge.defaultProps = {
  count: null,
  showZero: false,
  dot: false,
  overflowCount: 99
};
Badge.propTypes = {
  count: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  showZero: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  dot: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  overflowCount: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9iYWRnZS9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYmFkZ2UvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBBbmltYXRlIGZyb20gJ3JjLWFuaW1hdGUnO1xuaW1wb3J0IG9taXQgZnJvbSAnb21pdC5qcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBTY3JvbGxOdW1iZXIgZnJvbSAnLi9TY3JvbGxOdW1iZXInO1xuaW1wb3J0IHsgUHJlc2V0Q29sb3JUeXBlcyB9IGZyb20gJy4uL191dGlsL2NvbG9ycyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5mdW5jdGlvbiBpc1ByZXNldENvbG9yKGNvbG9yKSB7XG4gICAgcmV0dXJuIFByZXNldENvbG9yVHlwZXMuaW5kZXhPZihjb2xvcikgIT09IC0xO1xufVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFkZ2UgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnJlbmRlckJhZGdlID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc2Nyb2xsTnVtYmVyUHJlZml4Q2xzOiBjdXN0b21pemVTY3JvbGxOdW1iZXJQcmVmaXhDbHMsIGNoaWxkcmVuLCBzdGF0dXMsIHRleHQsIGNvbG9yIH0gPSBfYSwgcmVzdFByb3BzID0gX19yZXN0KF9hLCBbXCJwcmVmaXhDbHNcIiwgXCJzY3JvbGxOdW1iZXJQcmVmaXhDbHNcIiwgXCJjaGlsZHJlblwiLCBcInN0YXR1c1wiLCBcInRleHRcIiwgXCJjb2xvclwiXSk7XG4gICAgICAgICAgICBjb25zdCBvbWl0QXJyID0gW1xuICAgICAgICAgICAgICAgICdjb3VudCcsXG4gICAgICAgICAgICAgICAgJ3Nob3daZXJvJyxcbiAgICAgICAgICAgICAgICAnb3ZlcmZsb3dDb3VudCcsXG4gICAgICAgICAgICAgICAgJ2NsYXNzTmFtZScsXG4gICAgICAgICAgICAgICAgJ3N0eWxlJyxcbiAgICAgICAgICAgICAgICAnZG90JyxcbiAgICAgICAgICAgICAgICAnb2Zmc2V0JyxcbiAgICAgICAgICAgICAgICAndGl0bGUnLFxuICAgICAgICAgICAgXTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnYmFkZ2UnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3Qgc2Nyb2xsTnVtYmVyUHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdzY3JvbGwtbnVtYmVyJywgY3VzdG9taXplU2Nyb2xsTnVtYmVyUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbE51bWJlciA9IHRoaXMucmVuZGVyQmFkZ2VOdW1iZXIocHJlZml4Q2xzLCBzY3JvbGxOdW1iZXJQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3Qgc3RhdHVzVGV4dCA9IHRoaXMucmVuZGVyU3RhdHVzVGV4dChwcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3Qgc3RhdHVzQ2xzID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc3RhdHVzLWRvdGBdOiB0aGlzLmhhc1N0YXR1cygpLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXN0YXR1cy0ke3N0YXR1c31gXTogISFzdGF0dXMsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc3RhdHVzLSR7Y29sb3J9YF06IGlzUHJlc2V0Q29sb3IoY29sb3IpLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBzdGF0dXNTdHlsZSA9IHt9O1xuICAgICAgICAgICAgaWYgKGNvbG9yICYmICFpc1ByZXNldENvbG9yKGNvbG9yKSkge1xuICAgICAgICAgICAgICAgIHN0YXR1c1N0eWxlLmJhY2tncm91bmQgPSBjb2xvcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIDxCYWRnZSBzdGF0dXM9XCJzdWNjZXNzXCIgLz5cbiAgICAgICAgICAgIGlmICghY2hpbGRyZW4gJiYgdGhpcy5oYXNTdGF0dXMoKSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHN0eWxlV2l0aE9mZnNldCA9IHRoaXMuZ2V0U3R5bGVXaXRoT2Zmc2V0KCk7XG4gICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzVGV4dENvbG9yID0gc3R5bGVXaXRoT2Zmc2V0ICYmIHN0eWxlV2l0aE9mZnNldC5jb2xvcjtcbiAgICAgICAgICAgICAgICByZXR1cm4gKDxzcGFuIHsuLi5vbWl0KHJlc3RQcm9wcywgb21pdEFycil9IGNsYXNzTmFtZT17dGhpcy5nZXRCYWRnZUNsYXNzTmFtZShwcmVmaXhDbHMpfSBzdHlsZT17c3R5bGVXaXRoT2Zmc2V0fT5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e3N0YXR1c0Nsc30gc3R5bGU9e3N0YXR1c1N0eWxlfS8+XG4gICAgICAgICAgPHNwYW4gc3R5bGU9e3sgY29sb3I6IHN0YXR1c1RleHRDb2xvciB9fSBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tc3RhdHVzLXRleHRgfT5cbiAgICAgICAgICAgIHt0ZXh0fVxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPC9zcGFuPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gKDxzcGFuIHsuLi5vbWl0KHJlc3RQcm9wcywgb21pdEFycil9IGNsYXNzTmFtZT17dGhpcy5nZXRCYWRnZUNsYXNzTmFtZShwcmVmaXhDbHMpfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgICA8QW5pbWF0ZSBjb21wb25lbnQ9XCJcIiBzaG93UHJvcD1cImRhdGEtc2hvd1wiIHRyYW5zaXRpb25OYW1lPXtjaGlsZHJlbiA/IGAke3ByZWZpeENsc30tem9vbWAgOiAnJ30gdHJhbnNpdGlvbkFwcGVhcj5cbiAgICAgICAgICB7c2Nyb2xsTnVtYmVyfVxuICAgICAgICA8L0FuaW1hdGU+XG4gICAgICAgIHtzdGF0dXNUZXh0fVxuICAgICAgPC9zcGFuPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGdldE51bWJlcmVkRGlzcGF5Q291bnQoKSB7XG4gICAgICAgIGNvbnN0IHsgY291bnQsIG92ZXJmbG93Q291bnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGRpc3BsYXlDb3VudCA9IGNvdW50ID4gb3ZlcmZsb3dDb3VudCA/IGAke292ZXJmbG93Q291bnR9K2AgOiBjb3VudDtcbiAgICAgICAgcmV0dXJuIGRpc3BsYXlDb3VudDtcbiAgICB9XG4gICAgZ2V0RGlzcGF5Q291bnQoKSB7XG4gICAgICAgIGNvbnN0IGlzRG90ID0gdGhpcy5pc0RvdCgpO1xuICAgICAgICAvLyBkb3QgbW9kZSBkb24ndCBuZWVkIGNvdW50XG4gICAgICAgIGlmIChpc0RvdCkge1xuICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmdldE51bWJlcmVkRGlzcGF5Q291bnQoKTtcbiAgICB9XG4gICAgZ2V0U2Nyb2xsTnVtYmVyVGl0bGUoKSB7XG4gICAgICAgIGNvbnN0IHsgdGl0bGUsIGNvdW50IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAodGl0bGUpIHtcbiAgICAgICAgICAgIHJldHVybiB0aXRsZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdHlwZW9mIGNvdW50ID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgY291bnQgPT09ICdudW1iZXInID8gY291bnQgOiB1bmRlZmluZWQ7XG4gICAgfVxuICAgIGdldFN0eWxlV2l0aE9mZnNldCgpIHtcbiAgICAgICAgY29uc3QgeyBvZmZzZXQsIHN0eWxlIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gb2Zmc2V0XG4gICAgICAgICAgICA/IE9iamVjdC5hc3NpZ24oeyByaWdodDogLXBhcnNlSW50KG9mZnNldFswXSwgMTApLCBtYXJnaW5Ub3A6IG9mZnNldFsxXSB9LCBzdHlsZSkgOiBzdHlsZTtcbiAgICB9XG4gICAgZ2V0QmFkZ2VDbGFzc05hbWUocHJlZml4Q2xzKSB7XG4gICAgICAgIGNvbnN0IHsgY2xhc3NOYW1lLCBjaGlsZHJlbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIGNsYXNzTmFtZXMoY2xhc3NOYW1lLCBwcmVmaXhDbHMsIHtcbiAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXN0YXR1c2BdOiB0aGlzLmhhc1N0YXR1cygpLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tbm90LWEtd3JhcHBlcmBdOiAhY2hpbGRyZW4sXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBoYXNTdGF0dXMoKSB7XG4gICAgICAgIGNvbnN0IHsgc3RhdHVzLCBjb2xvciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuICEhc3RhdHVzIHx8ICEhY29sb3I7XG4gICAgfVxuICAgIGlzWmVybygpIHtcbiAgICAgICAgY29uc3QgbnVtYmVyZWREaXNwYXlDb3VudCA9IHRoaXMuZ2V0TnVtYmVyZWREaXNwYXlDb3VudCgpO1xuICAgICAgICByZXR1cm4gbnVtYmVyZWREaXNwYXlDb3VudCA9PT0gJzAnIHx8IG51bWJlcmVkRGlzcGF5Q291bnQgPT09IDA7XG4gICAgfVxuICAgIGlzRG90KCkge1xuICAgICAgICBjb25zdCB7IGRvdCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgaXNaZXJvID0gdGhpcy5pc1plcm8oKTtcbiAgICAgICAgcmV0dXJuIChkb3QgJiYgIWlzWmVybykgfHwgdGhpcy5oYXNTdGF0dXMoKTtcbiAgICB9XG4gICAgaXNIaWRkZW4oKSB7XG4gICAgICAgIGNvbnN0IHsgc2hvd1plcm8gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGRpc3BsYXlDb3VudCA9IHRoaXMuZ2V0RGlzcGF5Q291bnQoKTtcbiAgICAgICAgY29uc3QgaXNaZXJvID0gdGhpcy5pc1plcm8oKTtcbiAgICAgICAgY29uc3QgaXNEb3QgPSB0aGlzLmlzRG90KCk7XG4gICAgICAgIGNvbnN0IGlzRW1wdHkgPSBkaXNwbGF5Q291bnQgPT09IG51bGwgfHwgZGlzcGxheUNvdW50ID09PSB1bmRlZmluZWQgfHwgZGlzcGxheUNvdW50ID09PSAnJztcbiAgICAgICAgcmV0dXJuIChpc0VtcHR5IHx8IChpc1plcm8gJiYgIXNob3daZXJvKSkgJiYgIWlzRG90O1xuICAgIH1cbiAgICByZW5kZXJTdGF0dXNUZXh0KHByZWZpeENscykge1xuICAgICAgICBjb25zdCB7IHRleHQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGhpZGRlbiA9IHRoaXMuaXNIaWRkZW4oKTtcbiAgICAgICAgcmV0dXJuIGhpZGRlbiB8fCAhdGV4dCA/IG51bGwgOiA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tc3RhdHVzLXRleHRgfT57dGV4dH08L3NwYW4+O1xuICAgIH1cbiAgICByZW5kZXJEaXNwYXlDb21wb25lbnQoKSB7XG4gICAgICAgIGNvbnN0IHsgY291bnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGN1c3RvbU5vZGUgPSBjb3VudDtcbiAgICAgICAgaWYgKCFjdXN0b21Ob2RlIHx8IHR5cGVvZiBjdXN0b21Ob2RlICE9PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGN1c3RvbU5vZGUsIHtcbiAgICAgICAgICAgIHN0eWxlOiBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHRoaXMuZ2V0U3R5bGVXaXRoT2Zmc2V0KCkpLCAoY3VzdG9tTm9kZS5wcm9wcyAmJiBjdXN0b21Ob2RlLnByb3BzLnN0eWxlKSksXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICByZW5kZXJCYWRnZU51bWJlcihwcmVmaXhDbHMsIHNjcm9sbE51bWJlclByZWZpeENscykge1xuICAgICAgICBjb25zdCB7IHN0YXR1cywgY291bnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGRpc3BsYXlDb3VudCA9IHRoaXMuZ2V0RGlzcGF5Q291bnQoKTtcbiAgICAgICAgY29uc3QgaXNEb3QgPSB0aGlzLmlzRG90KCk7XG4gICAgICAgIGNvbnN0IGhpZGRlbiA9IHRoaXMuaXNIaWRkZW4oKTtcbiAgICAgICAgY29uc3Qgc2Nyb2xsTnVtYmVyQ2xzID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1kb3RgXTogaXNEb3QsXG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb3VudGBdOiAhaXNEb3QsXG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1tdWx0aXBsZS13b3Jkc2BdOiAhaXNEb3QgJiYgY291bnQgJiYgY291bnQudG9TdHJpbmcgJiYgY291bnQudG9TdHJpbmcoKS5sZW5ndGggPiAxLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc3RhdHVzLSR7c3RhdHVzfWBdOiB0aGlzLmhhc1N0YXR1cygpLFxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGhpZGRlbiA/IG51bGwgOiAoPFNjcm9sbE51bWJlciBwcmVmaXhDbHM9e3Njcm9sbE51bWJlclByZWZpeENsc30gZGF0YS1zaG93PXshaGlkZGVufSBjbGFzc05hbWU9e3Njcm9sbE51bWJlckNsc30gY291bnQ9e2Rpc3BsYXlDb3VudH0gZGlzcGxheUNvbXBvbmVudD17dGhpcy5yZW5kZXJEaXNwYXlDb21wb25lbnQoKX0gLy8gPEJhZGdlIHN0YXR1cz1cInN1Y2Nlc3NcIiBjb3VudD17PEljb24gdHlwZT1cInh4eFwiIC8+fT48L0JhZGdlPlxuICAgICAgICAgdGl0bGU9e3RoaXMuZ2V0U2Nyb2xsTnVtYmVyVGl0bGUoKX0gc3R5bGU9e3RoaXMuZ2V0U3R5bGVXaXRoT2Zmc2V0KCl9IGtleT1cInNjcm9sbE51bWJlclwiLz4pO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQmFkZ2V9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQmFkZ2UuZGVmYXVsdFByb3BzID0ge1xuICAgIGNvdW50OiBudWxsLFxuICAgIHNob3daZXJvOiBmYWxzZSxcbiAgICBkb3Q6IGZhbHNlLFxuICAgIG92ZXJmbG93Q291bnQ6IDk5LFxufTtcbkJhZGdlLnByb3BUeXBlcyA9IHtcbiAgICBjb3VudDogUHJvcFR5cGVzLm5vZGUsXG4gICAgc2hvd1plcm86IFByb3BUeXBlcy5ib29sLFxuICAgIGRvdDogUHJvcFR5cGVzLmJvb2wsXG4gICAgb3ZlcmZsb3dDb3VudDogUHJvcFR5cGVzLm51bWJlcixcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQXZCQTtBQUNBO0FBQ0E7QUF3QkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRDQTtBQUNBO0FBSEE7QUE4Q0E7QUFDQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUlBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFFQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBcElBO0FBQ0E7QUFEQTtBQXNJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/badge/index.js
