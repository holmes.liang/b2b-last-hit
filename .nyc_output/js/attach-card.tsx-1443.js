__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-images */ "./node_modules/react-images/lib/Lightbox.js");
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_images__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @assets/attachment.svg */ "./src/assets/attachment.svg");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_assets_attachment_svg__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @desk-component/icons/pdf.png */ "./src/app/desk/component/icons/pdf.png");
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_18__);









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/attach-card.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            .AttachCardForm {\n                display: flex;\n            }\n            .ant-upload-text {\n              text-transform: uppercase;\n              font-size: 12px;\n              overflow: hidden;\n              text-overflow: ellipsis;\n            }\n            .operate {\n              position: absolute;\n              z-index: 22;\n              width: 100%; \n              height: 23px; \n              bottom: 2px;\n              margin-left: -5px;\n              display: none;\n              .bg {\n                background: #fff;\n                height: 23px; \n                zoom: 1; \n              }\n              .con {\n                position: relative;\n              }\n              a {\n                display: inline-block !important;\n                height: 22px !important;\n                line-height: 22px !important;\n                .anticon {\n                  font-size: 20px;\n                }\n                &.a-pl {\n                  padding-left: 48px;\n                }\n              }\n            }\n            .AttachCard-preview-list {\n                display: -ms-flexbox;\n                display: flex;\n                -ms-flex-wrap: wrap;\n                flex-wrap: wrap;\n            }\n            .AttachCard-preview-item {\n                position: relative;\n                display: -ms-flexbox;\n                display: flex;\n                -ms-flex-direction: column;\n                flex-direction: column;\n                margin: 0 15px 15px 0;\n                padding: 5px;\n                // width: 135px;\n                height: 135px;\n                text-align: center;\n                border: 1px solid #ebebeb;\n                border-radius: 2px;\n                cursor: pointer;\n                img {\n                    flex: 1 1 0%;\n                    max-height: 100px;\n                    \n                }\n                a {\n                    display: block;\n                    height: 23px;\n                    line-height: 23px;\n                    white-space: nowrap;\n                    overflow: hidden;\n                    text-overflow: ellipsis;\n                    color: #878787;\n                    &.img-a {\n                      height: auto;\n                    }\n                }\n              \n                .AttachCard-preview-item__oper {\n                    position: absolute;\n                    top: -9px;\n                    right: -10px;\n                    display: none;\n                    z-index: 22;\n                    img {\n                      width: 23px;\n                    }\n                    i{\n                        position: relative;\n                        color: #5fbfec;\n                        z-index: 25;\n                    }\n                }\n                .file-name {\n                  display: block;\n                }\n                &:hover {\n                    .AttachCard-preview-item__oper, .operate {\n                        display: block;\n                    }\n                    .file-name {\n                      display: none;\n                    }\n                }\n            }\n            .AttachCard-uploader-list {\n                display: -ms-flexbox;\n                display: flex;\n                -ms-flex-wrap: wrap;\n                flex-wrap: wrap;\n                overflow-x: auto;\n                overflow-y: hidden;\n                .AttachCard-uploader-item {\n                    margin-right: 30px;\n                    text-align: center;\n                    a,img {\n                        display: block;\n                        height: 65px;\n                        width: 80px;\n                        margin: 0 auto;\n                        opacity: .6;\n                        background-size: contain;\n                        background-repeat: no-repeat;\n                        background-position: center bottom;\n                    }\n                     p {\n                        font-size: 13px;\n                        margin-top: 5px;\n                        color: #9e9e9e;\n                        text-transform: uppercase;\n                    }\n                }\n            } \n            .ant-upload.ant-upload-drag {\n                width: 135px;\n                height: 135px;\n            }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var Dragger = antd__WEBPACK_IMPORTED_MODULE_13__["Upload"].Dragger;



var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_16__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var UploadFileState = {
  PROGRESS: 0,
  COMPLETED: 1
};
var uploadFileMgr = {
  startUpload: function startUpload(list, docType, callback) {
    var _this = this;

    var result = list.map(function (file) {
      var name = file.name,
          uid = file.uid,
          type = file.type;
      var isImage = type.indexOf("image/") === 0;

      if (isImage) {
        _this.getBase64(file, function (url) {
          return callback(file, url);
        });
      }

      return {
        uid: uid,
        state: UploadFileState.PROGRESS,
        fileName: name,
        type: docType,
        isImage: isImage,
        isPdf: type.includes("pdf")
      };
    });
    return result;
  },
  getBase64: function getBase64(file, callback) {
    var reader = new FileReader();
    reader.addEventListener("load", function () {
      return callback(reader.result);
    });
    reader.readAsDataURL(file);
  },
  getFileRenderUrl: function getFileRenderUrl(item, policyId) {
    var id = item.id,
        url = item.url,
        isImage = item.isImage,
        isPdf = item.isPdf,
        attachId = item.attachId;
    var authKey = _common__WEBPACK_IMPORTED_MODULE_14__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].AUTH_KEY);

    if (isPdf) {
      return _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_18___default.a;
    }

    if (!isImage) {
      return _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_17___default.a;
    }

    if (attachId || id) {
      return _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].getServiceLocation("/storage/".concat(id, "?access_token=").concat(authKey));
    }

    if (url) {
      return url;
    }

    return null;
  },
  getFileDownloadUrl: function getFileDownloadUrl(item, policyId) {
    var id = item.id;
    var authKey = _common__WEBPACK_IMPORTED_MODULE_14__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].AUTH_KEY);

    if (id) {
      return _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].getServiceLocation("/blob/files/".concat(id, "?access_token=").concat(authKey));
    }

    return "";
  },
  completeUpload: function completeUpload(file, fid) {
    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, file, {
      state: UploadFileState.COMPLETED,
      id: fid
    });
  }
};

var AttachCard =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(AttachCard, _ModelWidget);

  function AttachCard(props, context) {
    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, AttachCard);

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(AttachCard).call(this, props, context));

    _this2.handleFileRemove = function (uid, item) {
      var removeImg = _this2.props.removeImg;
      var fileList = _this2.state.fileList;

      var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(fileList); // todo: 优化


      removeImg && removeImg(uid, item);
      var index = nextList.findIndex(function (item) {
        return item.uid === uid;
      });

      if (index === -1) {
        return;
      }

      var file = nextList.splice(index, 1)[0];

      _this2.setState({
        fileList: nextList,
        imageList: _this2.getImgList(nextList)
      }, function () {
        _this2.storeFiles();
      });

      if (file.id) {
        _this2.deletePolicyAttach(file.id, file.type);
      }
    };

    _this2.setLightbox = function (nextLightbox) {
      _this2.setState(function (_ref) {
        var lightbox = _ref.lightbox;
        return {
          lightbox: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, lightbox, {}, nextLightbox)
        };
      });
    };

    _this2.handleFileChange = function (info, docType) {
      var uploadImgSuccess = _this2.props.uploadImgSuccess;
      var fileList = _this2.state.fileList;
      var uploadedFileList = info.fileList;
      uploadedFileList.forEach(function (file) {
        if (file.response) {
          var fid = file.attachId = file.response.respData;
          var uid = file.uid;

          if (fid && fileList.findIndex(function (item) {
            return item.id === fid;
          }) !== -1) {
            return;
          }

          _this2.setState(function (_ref2) {
            var fileList = _ref2.fileList;

            var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(fileList);

            var index = fileList.findIndex(function (item) {
              return item.uid === uid;
            });

            if (index !== -1) {
              nextList[index] = uploadFileMgr.completeUpload(nextList[index], fid);
            }

            return {
              fileList: nextList,
              imageList: _this2.getImgList(nextList)
            };
          }, function () {
            if (_this2.state.fileList.find(function (item) {
              return item.uid == uid;
            })) {
              uploadImgSuccess && uploadImgSuccess(file, docType);
            }

            _this2.storeFiles();
          });
        }
      });
    };

    _this2.handleFileBeforeUpload = function (file, files, docType, type) {
      var maxLen = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(type, "maxLen");

      if (maxLen) {
        var filterLen = _this2.state.fileList.filter(function (item) {
          return item.type === docType;
        }).length;

        if (filterLen >= maxLen) {
          antd__WEBPACK_IMPORTED_MODULE_13__["notification"].error({
            message: "".concat(docType, " more than ").concat(maxLen)
          });
          return false;
        }
      }

      var willUploadedFiles = uploadFileMgr.startUpload(files, docType, function (file, url) {
        var fileList = _this2.state.fileList;
        var index = fileList.findIndex(function (item) {
          return item.uid === file.uid;
        });

        if (index !== -1) {
          var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(fileList);

          nextList[index] = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, nextList[index], {
            url: url
          });

          _this2.setState({
            fileList: nextList
          });
        }
      });

      _this2.setState({
        fileList: [].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_this2.state.fileList), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(willUploadedFiles)),
        imageList: _this2.getImgList([].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_this2.state.fileList), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(willUploadedFiles)))
      });

      return true;
    };

    _this2.gotoPrevLightboxImage = function () {
      var index = _this2.state.lightbox.index;

      if (index === 0) {
        return;
      }

      _this2.setLightbox({
        index: index - 1
      });
    };

    _this2.gotoNextLightboxImage = function () {
      var _this2$state = _this2.state,
          index = _this2$state.lightbox.index,
          fileList = _this2$state.fileList;

      if (index === fileList.length - 1) {
        return;
      }

      _this2.setLightbox({
        index: index + 1
      });
    };

    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(AttachCard, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        AttachCard: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(AttachCard.prototype), "initState", this).call(this), {
        uploadList: [],
        fileList: [],
        lightbox: {
          index: -1,
          visible: false
        },
        imageList: []
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "getImgList",
    value: function getImgList(fileList) {
      var _this3 = this;

      var imgList = [];
      if (!fileList) return [];
      fileList.map(function (every) {
        if (_this3.isPicture(every.fileName) || every.isImage) {
          imgList.push(every);
        }
      });
      return imgList;
    }
  }, {
    key: "deletePolicyAttach",
    value: function deletePolicyAttach(attachId, type) {
      this.getHelpers().getAjax().delete("/blob/files/".concat(attachId), {}, {}).then(function (response) {
        antd__WEBPACK_IMPORTED_MODULE_13__["notification"].success({
          message: "success",
          description: "Your file is deleted successfully"
        });
      }).catch(function (error) {
        antd__WEBPACK_IMPORTED_MODULE_13__["notification"].error({
          message: "error",
          description: error.message
        });
      });
    }
  }, {
    key: "renderPreview",
    value: function renderPreview() {
      var _this4 = this;

      var fileList = this.state.fileList;
      var imageList = [];
      fileList.map(function (item) {
        imageList.push(item);
      });
      var policyId = this.getValueFromModel("policyId");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("ul", {
        style: {
          paddingLeft: 0
        },
        className: "AttachCard-preview-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 350
        },
        __self: this
      }, imageList.map(function (item, index) {
        var type = item.type,
            fileName = item.fileName,
            state = item.state,
            uid = item.uid,
            isImage = item.isImage;
        var url = uploadFileMgr.getFileRenderUrl(item, policyId);
        var downloadUrl = uploadFileMgr.getFileDownloadUrl(item, policyId);
        var findImgIndex;

        _this4.getImgList(imageList).forEach(function (imageItem, imageIndex) {
          if (imageItem.uid === uid) {
            findImgIndex = imageIndex;
          }
        });

        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Spin"], {
          size: "large",
          key: uid,
          spinning: state === UploadFileState.PROGRESS,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 362
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Tooltip"], {
          overlayClassName: "file-name-tip",
          placement: "bottomRight",
          title: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 363
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("li", {
          className: "AttachCard-preview-item",
          "data-type": type,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 364
          },
          __self: this
        }, url && (_this4.isPicture(fileName) || isImage) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("img", {
          alt: uid,
          src: url,
          onClick: function onClick() {
            _this4.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 366
          },
          __self: this
        }), url && !(_this4.isPicture(fileName) || isImage) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          className: "img-a",
          href: downloadUrl + "&&resize=md",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 375
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("img", {
          alt: uid,
          src: url,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 376
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          className: "file-name",
          href: downloadUrl + "&&resize=md",
          title: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 379
          },
          __self: this
        }, fileName), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "AttachCard-preview-item__oper",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 382
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("img", {
          src: __webpack_require__(/*! @assets/close.svg */ "./src/assets/close.svg"),
          onClick: function onClick() {
            return _this4.handleFileRemove(uid, item);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 383
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "operate",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 385
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "bg",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 386
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "con",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 387
          },
          __self: this
        }, (_this4.isPicture(fileName) || isImage) && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          onClick: function onClick() {
            return _this4.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          href: "javascript:void(0)",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 389
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "eye",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 393
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          className: _this4.isPicture(fileName) || isImage ? "a-pl" : "",
          style: {
            color: COLOR_PRIMARY
          },
          href: downloadUrl + "&&resize=orig",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 396
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "download",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 401
          },
          __self: this
        }))))))));
      }));
    }
  }, {
    key: "storeFiles",
    value: function storeFiles() {}
  }, {
    key: "isPicture",
    value: function isPicture(fileName) {
      if (!fileName) return;
      var type = fileName.split(".")[fileName.split(".").length - 1];
      return /gif|jpg|jpeg|png|gif|jpg|png$/.test(type);
    }
  }, {
    key: "renderImageLightbox",
    value: function renderImageLightbox() {
      var _this5 = this;

      var policyId = this.getValueFromModel("policyId");
      var _this$state = this.state,
          fileList = _this$state.fileList,
          _this$state$lightbox = _this$state.lightbox,
          visible = _this$state$lightbox.visible,
          uid = _this$state$lightbox.uid,
          index = _this$state$lightbox.index;
      var imageList = [];
      fileList.map(function (item) {
        var fileName = item.fileName,
            isImage = item.isImage;

        if (_this5.isPicture(fileName) || isImage) {
          imageList.push({
            caption: fileName,
            src: uploadFileMgr.getFileRenderUrl(item, policyId) + "&&resize=md",
            uid: item.uid
          });
        }
      });
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(react_images__WEBPACK_IMPORTED_MODULE_15___default.a, {
        images: imageList,
        currentImage: index,
        backdropClosesModal: true,
        isOpen: visible && imageList.length > 0,
        onClickPrev: this.gotoPrevLightboxImage,
        onClickNext: this.gotoNextLightboxImage,
        onClose: function onClose() {
          return _this5.setLightbox({
            visible: false
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 533
        },
        __self: this
      });
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.AttachCard, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 548
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "clearfix",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 549
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "AttachCardForm",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 550
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 551
        },
        __self: this
      }, this.renderPreview()), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "AttachCard-uploader-wrap",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 552
        },
        __self: this
      }, this.renderUploalders()))), this.renderImageLightbox());
    }
  }, {
    key: "renderUploalders",
    value: function renderUploalders() {
      var _this6 = this;

      var fileList = this.state.fileList;
      var authKey = _common__WEBPACK_IMPORTED_MODULE_14__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].AUTH_KEY);

      var buildProps = function buildProps(docType, type) {
        return {
          action: function action(file) {
            return _common__WEBPACK_IMPORTED_MODULE_14__["Ajax"].getServiceLocation("/blob/files");
          },
          headers: {
            Authorization: authKey
          },
          beforeUpload: function beforeUpload(file, fileList) {
            return _this6.handleFileBeforeUpload(file, fileList, docType, type);
          },
          showUploadList: false,
          onChange: function onChange(info) {
            return _this6.handleFileChange(info, docType);
          },
          multiple: true,
          accept: "image/*"
        };
      };

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        className: "AttachCard-uploader-list",
        style: {
          display: fileList.length < 2 ? "block" : "none"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 580
        },
        __self: this
      }, (this.props.docTypes || this.state.uploadList || []).map(function (type) {
        var docType = type.docType,
            docTypeDesc = type.docTypeDesc,
            _type$uploadProps = type.uploadProps,
            uploadProps = _type$uploadProps === void 0 ? {} : _type$uploadProps;
        var props = buildProps(docType, type);
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(Dragger, Object.assign({}, props, {
          key: docType,
          multiple: true
        }, uploadProps, {
          listType: "picture-card",
          className: "AttachCard-uploader-item-wrap",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 586
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 590
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "plus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 591
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          className: "ant-upload-text",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 592
          },
          __self: this
        }, docTypeDesc)));
      }));
    }
  }]);

  return AttachCard;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (AttachCard);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL2F0dGFjaC1jYXJkLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC9hdHRhY2gtY2FyZC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBJY29uLCBub3RpZmljYXRpb24sIFNwaW4sIFRvb2x0aXAsIFVwbG9hZCB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBBamF4LCBDb25zdHMsIFN0b3JhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgUmNGaWxlIH0gZnJvbSBcImFudGQvbGliL3VwbG9hZFwiO1xuaW1wb3J0IExpZ2h0Ym94IGZyb20gXCJyZWFjdC1pbWFnZXNcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlc1wiO1xuXG5jb25zdCB7IERyYWdnZXIgfSA9IFVwbG9hZDtcbmltcG9ydCBBdHRhY2hDYXJkSW1hZ2VVcmwgZnJvbSBcIkBhc3NldHMvYXR0YWNobWVudC5zdmdcIjtcbmltcG9ydCBBdHRhY2hDYXJkUGRmIGZyb20gXCJAZGVzay1jb21wb25lbnQvaWNvbnMvcGRmLnBuZ1wiO1xuXG5jb25zdCB7IENPTE9SX1BSSU1BUlkgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5cbmNvbnN0IFVwbG9hZEZpbGVTdGF0ZSA9IHtcbiAgICBQUk9HUkVTUzogMCxcbiAgICBDT01QTEVURUQ6IDEsXG59O1xuXG5jb25zdCB1cGxvYWRGaWxlTWdyID0ge1xuICAgIHN0YXJ0VXBsb2FkKGxpc3Q6IGFueSwgZG9jVHlwZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGxpc3QubWFwKChmaWxlOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgbmFtZSwgdWlkLCB0eXBlIH0gPSBmaWxlO1xuICAgICAgICAgICAgY29uc3QgaXNJbWFnZSA9IHR5cGUuaW5kZXhPZihcImltYWdlL1wiKSA9PT0gMDtcblxuICAgICAgICAgICAgaWYgKGlzSW1hZ2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdldEJhc2U2NChmaWxlLCAodXJsOiBzdHJpbmcpID0+IGNhbGxiYWNrKGZpbGUsIHVybCkpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHVpZCxcbiAgICAgICAgICAgICAgICBzdGF0ZTogVXBsb2FkRmlsZVN0YXRlLlBST0dSRVNTLFxuICAgICAgICAgICAgICAgIGZpbGVOYW1lOiBuYW1lLFxuICAgICAgICAgICAgICAgIHR5cGU6IGRvY1R5cGUsXG4gICAgICAgICAgICAgICAgaXNJbWFnZSxcbiAgICAgICAgICAgICAgICBpc1BkZjogdHlwZS5pbmNsdWRlcyhcInBkZlwiKSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSxcblxuICAgIGdldEJhc2U2NChmaWxlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgY29uc3QgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcbiAgICAgICAgcmVhZGVyLmFkZEV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsICgpID0+IGNhbGxiYWNrKHJlYWRlci5yZXN1bHQpKTtcbiAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gICAgfSxcblxuICAgIGdldEZpbGVSZW5kZXJVcmwoaXRlbTogYW55LCBwb2xpY3lJZDogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHsgaWQsIHVybCwgaXNJbWFnZSwgaXNQZGYsIGF0dGFjaElkIH0gPSBpdGVtO1xuICAgICAgICBjb25zdCBhdXRoS2V5ID0gU3RvcmFnZS5BdXRoLnNlc3Npb24oKS5nZXQoQ29uc3RzLkFVVEhfS0VZKTtcbiAgICAgICAgaWYgKGlzUGRmKSB7XG4gICAgICAgICAgICByZXR1cm4gQXR0YWNoQ2FyZFBkZjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIWlzSW1hZ2UpIHtcbiAgICAgICAgICAgIHJldHVybiBBdHRhY2hDYXJkSW1hZ2VVcmw7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGF0dGFjaElkIHx8IGlkKSB7XG4gICAgICAgICAgICByZXR1cm4gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oYC9zdG9yYWdlLyR7aWR9P2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9YCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodXJsKSB7XG4gICAgICAgICAgICByZXR1cm4gdXJsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH0sXG5cbiAgICBnZXRGaWxlRG93bmxvYWRVcmwoaXRlbTogYW55LCBwb2xpY3lJZDogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHsgaWQgfSA9IGl0ZW07XG4gICAgICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgICAgICBpZiAoaWQpIHtcbiAgICAgICAgICAgIHJldHVybiBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihgL2Jsb2IvZmlsZXMvJHtpZH0/YWNjZXNzX3Rva2VuPSR7YXV0aEtleX1gKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBcIlwiO1xuICAgIH0sXG5cbiAgICBjb21wbGV0ZVVwbG9hZChmaWxlOiBhbnksIGZpZDogc3RyaW5nKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAuLi5maWxlLFxuICAgICAgICAgICAgc3RhdGU6IFVwbG9hZEZpbGVTdGF0ZS5DT01QTEVURUQsXG4gICAgICAgICAgICBpZDogZmlkLFxuICAgICAgICB9O1xuICAgIH0sXG59O1xuXG5leHBvcnQgdHlwZSBBdHRhY2hDYXJkUHJvcHMgPSB7XG4gICAgb25DaGFuZ2U/OiAodmFsdWU6IGFueSkgPT4gdm9pZDtcbiAgICB1cGxvYWRJbWdTdWNjZXNzPzogRnVuY3Rpb247XG4gICAgcmVtb3ZlSW1nPzogRnVuY3Rpb247XG4gICAgbW9kZWw6IGFueTtcbiAgICBmb3JtOiBhbnk7XG4gICAgZG9jVHlwZXM/OiBhbnk7XG4gICAgZ2V0VXBsb2FkTGlzdD86IEZ1bmN0aW9uO1xuICAgIHN0b3JlTW9kZWxQYXRoPzogc3RyaW5nO1xuICAgIGRvbm90U3RvcmVNb2RlbD86IGJvb2xlYW47XG4gICAgaGFuZGVsQWZ0ZXJGZXRjaERhdGE/OiBGdW5jdGlvbjtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBBdHRhY2hDYXJkU3RhdGUgPSB7XG4gICAgZmlsZUxpc3Q6IGFueTtcbiAgICB1cGxvYWRMaXN0OiBhbnk7XG4gICAgbGlnaHRib3g6IGFueTtcbiAgICBpbWFnZUxpc3Q6IGFueTtcbn07XG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgQXR0YWNoQ2FyZENvbXBvbmVudHMgPSB7XG4gICAgQXR0YWNoQ2FyZDogU3R5bGVkRElWO1xufTtcblxuY2xhc3MgQXR0YWNoQ2FyZDxQIGV4dGVuZHMgQXR0YWNoQ2FyZFByb3BzLFxuICAgIFMgZXh0ZW5kcyBBdHRhY2hDYXJkU3RhdGUsXG4gICAgQyBleHRlbmRzIEF0dGFjaENhcmRDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogQXR0YWNoQ2FyZFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICBBdHRhY2hDYXJkOiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgLkF0dGFjaENhcmRGb3JtIHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmFudC11cGxvYWQtdGV4dCB7XG4gICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAub3BlcmF0ZSB7XG4gICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgei1pbmRleDogMjI7XG4gICAgICAgICAgICAgIHdpZHRoOiAxMDAlOyBcbiAgICAgICAgICAgICAgaGVpZ2h0OiAyM3B4OyBcbiAgICAgICAgICAgICAgYm90dG9tOiAycHg7XG4gICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICAgICAgICAuYmcge1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyM3B4OyBcbiAgICAgICAgICAgICAgICB6b29tOiAxOyBcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuY29uIHtcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgYSB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyMnB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIycHggIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICAuYW50aWNvbiB7XG4gICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICYuYS1wbCB7XG4gICAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDQ4cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuQXR0YWNoQ2FyZC1wcmV2aWV3LWxpc3Qge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuQXR0YWNoQ2FyZC1wcmV2aWV3LWl0ZW0ge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwIDE1cHggMTVweCAwO1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgICAgICAgICAvLyB3aWR0aDogMTM1cHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMzVweDtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgICAgIGltZyB7XG4gICAgICAgICAgICAgICAgICAgIGZsZXg6IDEgMSAwJTtcbiAgICAgICAgICAgICAgICAgICAgbWF4LWhlaWdodDogMTAwcHg7XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjNweDtcbiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIzcHg7XG4gICAgICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcbiAgICAgICAgICAgICAgICAgICAgJi5pbWctYSB7XG4gICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAuQXR0YWNoQ2FyZC1wcmV2aWV3LWl0ZW1fX29wZXIge1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAgICAgICAgIHRvcDogLTlweDtcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQ6IC0xMHB4O1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiAyMjtcbiAgICAgICAgICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjNweDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpe1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM1ZmJmZWM7XG4gICAgICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiAyNTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAuZmlsZS1uYW1lIHtcbiAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgICAgICAgICAgLkF0dGFjaENhcmQtcHJldmlldy1pdGVtX19vcGVyLCAub3BlcmF0ZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAuZmlsZS1uYW1lIHtcbiAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLkF0dGFjaENhcmQtdXBsb2FkZXItbGlzdCB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICAgICAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICAgICAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgICAgICAgICBvdmVyZmxvdy14OiBhdXRvO1xuICAgICAgICAgICAgICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICAgICAgICAgICAgICAuQXR0YWNoQ2FyZC11cGxvYWRlci1pdGVtIHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgICAgIGEsaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA2NXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDgwcHg7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDAgYXV0bztcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IC42O1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBib3R0b207XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgIHAge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM5ZTllOWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBcbiAgICAgICAgICAgIC5hbnQtdXBsb2FkLmFudC11cGxvYWQtZHJhZyB7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDEzNXB4O1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTM1cHg7XG4gICAgICAgICAgICB9XG4gICAgICBgLFxuICAgICAgICB9IGFzIEM7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgICAgICAgIHVwbG9hZExpc3Q6IFtdLFxuICAgICAgICAgICAgZmlsZUxpc3Q6IFtdLFxuICAgICAgICAgICAgbGlnaHRib3g6IHsgaW5kZXg6IC0xLCB2aXNpYmxlOiBmYWxzZSB9LFxuICAgICAgICAgICAgaW1hZ2VMaXN0OiBbXSxcbiAgICAgICAgfSkgYXMgUztcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEltZ0xpc3QoZmlsZUxpc3Q6IGFueSk6IGFueVtdIHtcbiAgICAgICAgbGV0IGltZ0xpc3Q6IGFueSA9IFtdO1xuICAgICAgICBpZiAoIWZpbGVMaXN0KSByZXR1cm4gW107XG4gICAgICAgIGZpbGVMaXN0Lm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKHRoaXMuaXNQaWN0dXJlKGV2ZXJ5LmZpbGVOYW1lKSB8fCBldmVyeS5pc0ltYWdlKSB7XG4gICAgICAgICAgICAgICAgaW1nTGlzdC5wdXNoKGV2ZXJ5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBpbWdMaXN0O1xuICAgIH1cblxuICAgIHByaXZhdGUgaGFuZGxlRmlsZVJlbW92ZSA9ICh1aWQ6IHN0cmluZywgaXRlbTogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVtb3ZlSW1nIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCB7IGZpbGVMaXN0IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCBuZXh0TGlzdCA9IFsuLi5maWxlTGlzdF07XG4gICAgICAgIC8vIHRvZG86IOS8mOWMllxuICAgICAgICByZW1vdmVJbWcgJiYgcmVtb3ZlSW1nKHVpZCwgaXRlbSk7XG4gICAgICAgIGNvbnN0IGluZGV4ID0gbmV4dExpc3QuZmluZEluZGV4KGl0ZW0gPT4gaXRlbS51aWQgPT09IHVpZCk7XG4gICAgICAgIGlmIChpbmRleCA9PT0gLTEpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGZpbGUgPSBuZXh0TGlzdC5zcGxpY2UoaW5kZXgsIDEpWzBdO1xuICAgICAgICB0aGlzLnNldFN0YXRlKFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGZpbGVMaXN0OiBuZXh0TGlzdCxcbiAgICAgICAgICAgICAgICBpbWFnZUxpc3Q6IHRoaXMuZ2V0SW1nTGlzdChuZXh0TGlzdCksXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc3RvcmVGaWxlcygpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgKTtcblxuICAgICAgICBpZiAoZmlsZS5pZCkge1xuICAgICAgICAgICAgdGhpcy5kZWxldGVQb2xpY3lBdHRhY2goZmlsZS5pZCwgZmlsZS50eXBlKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBwcml2YXRlIGRlbGV0ZVBvbGljeUF0dGFjaChhdHRhY2hJZDogc3RyaW5nLCB0eXBlOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgICAgIC5kZWxldGUoYC9ibG9iL2ZpbGVzLyR7YXR0YWNoSWR9YCwge30sIHt9KVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBub3RpZmljYXRpb24uc3VjY2Vzcyh7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwic3VjY2Vzc1wiLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJZb3VyIGZpbGUgaXMgZGVsZXRlZCBzdWNjZXNzZnVsbHlcIixcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goKGVycm9yOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICBub3RpZmljYXRpb24uZXJyb3Ioe1xuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBcImVycm9yXCIsXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBlcnJvci5tZXNzYWdlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRMaWdodGJveCA9IChuZXh0TGlnaHRib3g6IGFueSkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKCh7IGxpZ2h0Ym94IH0pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgbGlnaHRib3g6IHtcbiAgICAgICAgICAgICAgICAgICAgLi4ubGlnaHRib3gsXG4gICAgICAgICAgICAgICAgICAgIC4uLm5leHRMaWdodGJveCxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuXG4gICAgcHJpdmF0ZSByZW5kZXJQcmV2aWV3KCkge1xuICAgICAgICBjb25zdCB7IGZpbGVMaXN0IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBsZXQgaW1hZ2VMaXN0OiBhbnkgPSBbXTtcbiAgICAgICAgZmlsZUxpc3QubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGltYWdlTGlzdC5wdXNoKGl0ZW0pO1xuICAgICAgICB9KTtcbiAgICAgICAgY29uc3QgcG9saWN5SWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5SWRcIik7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8dWwgc3R5bGU9e3sgcGFkZGluZ0xlZnQ6IDAgfX0gY2xhc3NOYW1lPVwiQXR0YWNoQ2FyZC1wcmV2aWV3LWxpc3RcIj5cbiAgICAgICAgICAgICAgICB7aW1hZ2VMaXN0Lm1hcCgoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHsgdHlwZSwgZmlsZU5hbWUsIHN0YXRlLCB1aWQsIGlzSW1hZ2UgfSA9IGl0ZW07XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHVybCA9IHVwbG9hZEZpbGVNZ3IuZ2V0RmlsZVJlbmRlclVybChpdGVtLCBwb2xpY3lJZCk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRvd25sb2FkVXJsID0gdXBsb2FkRmlsZU1nci5nZXRGaWxlRG93bmxvYWRVcmwoaXRlbSwgcG9saWN5SWQpO1xuICAgICAgICAgICAgICAgICAgICBsZXQgZmluZEltZ0luZGV4OiBudW1iZXI7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0SW1nTGlzdChpbWFnZUxpc3QpLmZvckVhY2goKGltYWdlSXRlbTogYW55LCBpbWFnZUluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpbWFnZUl0ZW0udWlkID09PSB1aWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaW5kSW1nSW5kZXggPSBpbWFnZUluZGV4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIGtleT17dWlkfSBzcGlubmluZz17c3RhdGUgPT09IFVwbG9hZEZpbGVTdGF0ZS5QUk9HUkVTU30+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRvb2x0aXAgb3ZlcmxheUNsYXNzTmFtZT1cImZpbGUtbmFtZS10aXBcIiBwbGFjZW1lbnQ9XCJib3R0b21SaWdodFwiIHRpdGxlPXtmaWxlTmFtZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJBdHRhY2hDYXJkLXByZXZpZXctaXRlbVwiIGRhdGEtdHlwZT17dHlwZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dXJsICYmICh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXt1aWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNyYz17dXJsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldExpZ2h0Ym94KHsgaW5kZXg6IGZpbmRJbWdJbmRleCwgdmlzaWJsZTogdHJ1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt1cmwgJiYgISh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImltZy1hXCIgaHJlZj17ZG93bmxvYWRVcmwgKyBcIiYmcmVzaXplPW1kXCJ9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGFsdD17dWlkfSBzcmM9e3VybH0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJmaWxlLW5hbWVcIiBocmVmPXtkb3dubG9hZFVybCArIFwiJiZyZXNpemU9bWRcIn0gdGl0bGU9e2ZpbGVOYW1lfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZmlsZU5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIkF0dGFjaENhcmQtcHJldmlldy1pdGVtX19vcGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9e3JlcXVpcmUoXCJAYXNzZXRzL2Nsb3NlLnN2Z1wiKX0gb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVGaWxlUmVtb3ZlKHVpZCwgaXRlbSl9Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJvcGVyYXRlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJiZ1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuc2V0TGlnaHRib3goeyBpbmRleDogZmluZEltZ0luZGV4LCB2aXNpYmxlOiB0cnVlIH0pfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJY29uIHR5cGU9XCJleWVcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXsodGhpcy5pc1BpY3R1cmUoZmlsZU5hbWUpIHx8IGlzSW1hZ2UpID8gXCJhLXBsXCIgOiBcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBDT0xPUl9QUklNQVJZIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZj17ZG93bmxvYWRVcmwgKyBcIiYmcmVzaXplPW9yaWdcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwiZG93bmxvYWRcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TcGluPlxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGhhbmRsZUZpbGVDaGFuZ2UgPSAoaW5mbzogYW55LCBkb2NUeXBlOiBzdHJpbmcpID0+IHtcbiAgICAgICAgY29uc3QgeyB1cGxvYWRJbWdTdWNjZXNzIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCB7IGZpbGVMaXN0IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCB7IGZpbGVMaXN0OiB1cGxvYWRlZEZpbGVMaXN0IH0gPSBpbmZvO1xuICAgICAgICB1cGxvYWRlZEZpbGVMaXN0LmZvckVhY2goKGZpbGU6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKGZpbGUucmVzcG9uc2UpIHtcblxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZCA9IChmaWxlLmF0dGFjaElkID0gZmlsZS5yZXNwb25zZS5yZXNwRGF0YSk7XG4gICAgICAgICAgICAgICAgY29uc3QgeyB1aWQgfSA9IGZpbGU7XG4gICAgICAgICAgICAgICAgaWYgKGZpZCAmJiBmaWxlTGlzdC5maW5kSW5kZXgoKGl0ZW06IGFueSkgPT4gaXRlbS5pZCA9PT0gZmlkKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoXG4gICAgICAgICAgICAgICAgICAgICh7IGZpbGVMaXN0IH0pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG5leHRMaXN0ID0gWy4uLmZpbGVMaXN0XTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gZmlsZUxpc3QuZmluZEluZGV4KChpdGVtOiBhbnkpID0+IGl0ZW0udWlkID09PSB1aWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5leHRMaXN0W2luZGV4XSA9IHVwbG9hZEZpbGVNZ3IuY29tcGxldGVVcGxvYWQobmV4dExpc3RbaW5kZXhdLCBmaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVMaXN0OiBuZXh0TGlzdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWFnZUxpc3Q6IHRoaXMuZ2V0SW1nTGlzdChuZXh0TGlzdCksXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5maWxlTGlzdC5maW5kKChpdGVtOiBhbnkpID0+IGl0ZW0udWlkID09IHVpZCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cGxvYWRJbWdTdWNjZXNzICYmIHVwbG9hZEltZ1N1Y2Nlc3MoZmlsZSwgZG9jVHlwZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RvcmVGaWxlcygpO1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBwcml2YXRlIHN0b3JlRmlsZXMoKSB7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBoYW5kbGVGaWxlQmVmb3JlVXBsb2FkID0gKGZpbGU6IFJjRmlsZSwgZmlsZXM6IFJjRmlsZVtdLCBkb2NUeXBlOiBzdHJpbmcsIHR5cGU6IGFueSkgPT4ge1xuICAgICAgICBjb25zdCBtYXhMZW4gPSBfLmdldCh0eXBlLCBcIm1heExlblwiKTtcbiAgICAgICAgaWYgKG1heExlbikge1xuICAgICAgICAgICAgY29uc3QgZmlsdGVyTGVuID0gdGhpcy5zdGF0ZS5maWxlTGlzdC5maWx0ZXIoKGl0ZW06IGFueSkgPT4gaXRlbS50eXBlID09PSBkb2NUeXBlKS5sZW5ndGg7XG4gICAgICAgICAgICBpZiAoZmlsdGVyTGVuID49IG1heExlbikge1xuICAgICAgICAgICAgICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7IG1lc3NhZ2U6IGAke2RvY1R5cGV9IG1vcmUgdGhhbiAke21heExlbn1gIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjb25zdCB3aWxsVXBsb2FkZWRGaWxlcyA9IHVwbG9hZEZpbGVNZ3Iuc3RhcnRVcGxvYWQoZmlsZXMsIGRvY1R5cGUsIChmaWxlOiBhbnksIHVybDogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGZpbGVMaXN0IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgaW5kZXggPSBmaWxlTGlzdC5maW5kSW5kZXgoKGl0ZW06IGFueSkgPT4gaXRlbS51aWQgPT09IGZpbGUudWlkKTtcbiAgICAgICAgICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBuZXh0TGlzdCA9IFsuLi5maWxlTGlzdF07XG4gICAgICAgICAgICAgICAgbmV4dExpc3RbaW5kZXhdID0geyAuLi5uZXh0TGlzdFtpbmRleF0sIHVybCB9O1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBmaWxlTGlzdDogbmV4dExpc3QsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgZmlsZUxpc3Q6IFsuLi50aGlzLnN0YXRlLmZpbGVMaXN0LCAuLi53aWxsVXBsb2FkZWRGaWxlc10sXG4gICAgICAgICAgICBpbWFnZUxpc3Q6IHRoaXMuZ2V0SW1nTGlzdChbLi4udGhpcy5zdGF0ZS5maWxlTGlzdCwgLi4ud2lsbFVwbG9hZGVkRmlsZXNdKSxcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG4gICAgcHJpdmF0ZSBnb3RvUHJldkxpZ2h0Ym94SW1hZ2UgPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IHtcbiAgICAgICAgICAgIGxpZ2h0Ym94OiB7IGluZGV4IH0sXG4gICAgICAgIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBpZiAoaW5kZXggPT09IDApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuc2V0TGlnaHRib3goe1xuICAgICAgICAgICAgaW5kZXg6IGluZGV4IC0gMSxcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBwcml2YXRlIGdvdG9OZXh0TGlnaHRib3hJbWFnZSA9ICgpID0+IHtcbiAgICAgICAgY29uc3Qge1xuICAgICAgICAgICAgbGlnaHRib3g6IHsgaW5kZXggfSxcbiAgICAgICAgICAgIGZpbGVMaXN0LFxuICAgICAgICB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgaWYgKGluZGV4ID09PSBmaWxlTGlzdC5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNldExpZ2h0Ym94KHtcbiAgICAgICAgICAgIGluZGV4OiBpbmRleCArIDEsXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBwcml2YXRlIGlzUGljdHVyZShmaWxlTmFtZTogc3RyaW5nKSB7XG4gICAgICAgIGlmICghZmlsZU5hbWUpIHJldHVybjtcbiAgICAgICAgY29uc3QgdHlwZTogc3RyaW5nID0gZmlsZU5hbWUuc3BsaXQoXCIuXCIpW2ZpbGVOYW1lLnNwbGl0KFwiLlwiKS5sZW5ndGggLSAxXTtcbiAgICAgICAgcmV0dXJuIC9naWZ8anBnfGpwZWd8cG5nfGdpZnxqcGd8cG5nJC8udGVzdCh0eXBlKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlbmRlckltYWdlTGlnaHRib3goKSB7XG4gICAgICAgIGNvbnN0IHBvbGljeUlkID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeUlkXCIpO1xuICAgICAgICBjb25zdCB7XG4gICAgICAgICAgICBmaWxlTGlzdCxcbiAgICAgICAgICAgIGxpZ2h0Ym94OiB7IHZpc2libGUsIHVpZCwgaW5kZXggfSxcbiAgICAgICAgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGxldCBpbWFnZUxpc3Q6IGFueSA9IFtdO1xuICAgICAgICBmaWxlTGlzdC5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBmaWxlTmFtZSwgaXNJbWFnZSB9ID0gaXRlbTtcbiAgICAgICAgICAgIGlmICh0aGlzLmlzUGljdHVyZShmaWxlTmFtZSkgfHwgaXNJbWFnZSkge1xuICAgICAgICAgICAgICAgIGltYWdlTGlzdC5wdXNoKHtcbiAgICAgICAgICAgICAgICAgICAgY2FwdGlvbjogZmlsZU5hbWUsXG4gICAgICAgICAgICAgICAgICAgIHNyYzogdXBsb2FkRmlsZU1nci5nZXRGaWxlUmVuZGVyVXJsKGl0ZW0sIHBvbGljeUlkKSArIFwiJiZyZXNpemU9bWRcIixcbiAgICAgICAgICAgICAgICAgICAgdWlkOiBpdGVtLnVpZCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8TGlnaHRib3hcbiAgICAgICAgICAgICAgICBpbWFnZXM9e2ltYWdlTGlzdH1cbiAgICAgICAgICAgICAgICBjdXJyZW50SW1hZ2U9e2luZGV4fVxuICAgICAgICAgICAgICAgIGJhY2tkcm9wQ2xvc2VzTW9kYWxcbiAgICAgICAgICAgICAgICBpc09wZW49e3Zpc2libGUgJiYgaW1hZ2VMaXN0Lmxlbmd0aCA+IDB9XG4gICAgICAgICAgICAgICAgb25DbGlja1ByZXY9e3RoaXMuZ290b1ByZXZMaWdodGJveEltYWdlfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2tOZXh0PXt0aGlzLmdvdG9OZXh0TGlnaHRib3hJbWFnZX1cbiAgICAgICAgICAgICAgICBvbkNsb3NlPXsoKSA9PiB0aGlzLnNldExpZ2h0Ym94KHsgdmlzaWJsZTogZmFsc2UgfSl9XG4gICAgICAgICAgICAvPlxuICAgICAgICApO1xuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEMuQXR0YWNoQ2FyZD5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNsZWFyZml4XCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiQXR0YWNoQ2FyZEZvcm1cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+e3RoaXMucmVuZGVyUHJldmlldygpfTwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJBdHRhY2hDYXJkLXVwbG9hZGVyLXdyYXBcIj57dGhpcy5yZW5kZXJVcGxvYWxkZXJzKCl9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIHt0aGlzLnJlbmRlckltYWdlTGlnaHRib3goKX1cbiAgICAgICAgICAgIDwvQy5BdHRhY2hDYXJkPlxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByaXZhdGUgcmVuZGVyVXBsb2FsZGVycygpIHtcbiAgICAgICAgY29uc3Qge2ZpbGVMaXN0fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgICAgICBjb25zdCBidWlsZFByb3BzID0gKGRvY1R5cGU6IHN0cmluZywgdHlwZT86IGFueSkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBhY3Rpb246IChmaWxlOiBSY0ZpbGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKGAvYmxvYi9maWxlc2ApO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOiBhdXRoS2V5LFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgYmVmb3JlVXBsb2FkOiAoZmlsZTogUmNGaWxlLCBmaWxlTGlzdDogUmNGaWxlW10pID0+IHRoaXMuaGFuZGxlRmlsZUJlZm9yZVVwbG9hZChmaWxlLCBmaWxlTGlzdCwgZG9jVHlwZSwgdHlwZSksXG4gICAgICAgICAgICAgICAgc2hvd1VwbG9hZExpc3Q6IGZhbHNlLFxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlOiAoaW5mbzogYW55KSA9PiB0aGlzLmhhbmRsZUZpbGVDaGFuZ2UoaW5mbywgZG9jVHlwZSksXG4gICAgICAgICAgICAgICAgbXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICAgICAgYWNjZXB0OiBcImltYWdlLypcIixcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiQXR0YWNoQ2FyZC11cGxvYWRlci1saXN0XCIgc3R5bGU9e3tkaXNwbGF5OiBmaWxlTGlzdC5sZW5ndGggPCAyID8gXCJibG9ja1wiIDogXCJub25lXCJ9fT5cbiAgICAgICAgICAgICAgICB7KHRoaXMucHJvcHMuZG9jVHlwZXMgfHwgdGhpcy5zdGF0ZS51cGxvYWRMaXN0IHx8IFtdKS5tYXAoKHR5cGU6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB7IGRvY1R5cGUsIGRvY1R5cGVEZXNjLCB1cGxvYWRQcm9wcyA9IHt9IH0gPSB0eXBlO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9wcyA9IGJ1aWxkUHJvcHMoZG9jVHlwZSwgdHlwZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxEcmFnZ2VyIHsuLi5wcm9wc30ga2V5PXtkb2NUeXBlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtdWx0aXBsZT17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLnVwbG9hZFByb3BzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaXN0VHlwZT1cInBpY3R1cmUtY2FyZFwiIGNsYXNzTmFtZT1cIkF0dGFjaENhcmQtdXBsb2FkZXItaXRlbS13cmFwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT17XCJwbHVzXCJ9Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhbnQtdXBsb2FkLXRleHRcIj57ZG9jVHlwZURlc2N9PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0RyYWdnZXI+XG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0dGFjaENhcmQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBSEE7QUFLQTtBQWhFQTtBQUNBO0FBMkZBOzs7OztBQUdBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUF1S0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpNQTtBQXFOQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBOU5BO0FBeVNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFFQTtBQUZBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUE5VUE7QUFtVkE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBN1dBO0FBNldBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUF6WEE7QUF5WEE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUF0WUE7QUFFQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQTBJQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7OztBQUdBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBNkJBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7QUFjQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUdBOzs7QUF3Q0E7OztBQXdEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBWEE7QUFjQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQUE7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFHQTs7OztBQWxlQTtBQUNBO0FBb2VBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/attach-card.tsx
