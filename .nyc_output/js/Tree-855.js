__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Tree; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_tree__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-tree */ "./node_modules/rc-tree/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _DirectoryTree__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./DirectoryTree */ "./node_modules/antd/es/tree/DirectoryTree.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_motion__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/motion */ "./node_modules/antd/es/_util/motion.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}









var Tree =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tree, _React$Component);

  function Tree() {
    var _this;

    _classCallCheck(this, Tree);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tree).apply(this, arguments));

    _this.renderSwitcherIcon = function (prefixCls, switcherIcon, _ref) {
      var isLeaf = _ref.isLeaf,
          expanded = _ref.expanded,
          loading = _ref.loading;
      var showLine = _this.props.showLine;

      if (loading) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "loading",
          className: "".concat(prefixCls, "-switcher-loading-icon")
        });
      }

      var switcherCls = "".concat(prefixCls, "-switcher-icon");

      if (switcherIcon) {
        return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](switcherIcon, {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(switcherIcon.props.className || '', switcherCls)
        });
      }

      if (isLeaf) {
        return showLine ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "file",
          className: "".concat(prefixCls, "-switcher-line-icon")
        }) : null;
      }

      return showLine ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: expanded ? 'minus-square' : 'plus-square',
        className: "".concat(prefixCls, "-switcher-line-icon"),
        theme: "outlined"
      }) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "caret-down",
        className: switcherCls,
        theme: "filled"
      });
    };

    _this.setTreeRef = function (node) {
      _this.tree = node;
    };

    _this.renderTree = function (_ref2) {
      var _classNames;

      var getPrefixCls = _ref2.getPrefixCls;

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props;

      var customizePrefixCls = props.prefixCls,
          className = props.className,
          showIcon = props.showIcon,
          _switcherIcon = props.switcherIcon,
          blockNode = props.blockNode,
          children = props.children;
      var checkable = props.checkable;
      var prefixCls = getPrefixCls('tree', customizePrefixCls);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_tree__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
        ref: _this.setTreeRef
      }, props, {
        prefixCls: prefixCls,
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-icon-hide"), !showIcon), _defineProperty(_classNames, "".concat(prefixCls, "-block-node"), blockNode), _classNames)),
        checkable: checkable ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: "".concat(prefixCls, "-checkbox-inner")
        }) : checkable,
        switcherIcon: function switcherIcon(nodeProps) {
          return _this.renderSwitcherIcon(prefixCls, _switcherIcon, nodeProps);
        }
      }), children);
    };

    return _this;
  }

  _createClass(Tree, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderTree);
    }
  }]);

  return Tree;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Tree.TreeNode = rc_tree__WEBPACK_IMPORTED_MODULE_1__["TreeNode"];
Tree.DirectoryTree = _DirectoryTree__WEBPACK_IMPORTED_MODULE_3__["default"];
Tree.defaultProps = {
  checkable: false,
  showIcon: false,
  motion: _extends(_extends({}, _util_motion__WEBPACK_IMPORTED_MODULE_6__["default"]), {
    motionAppear: false
  }),
  blockNode: false
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90cmVlL1RyZWUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3RyZWUvVHJlZS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJjVHJlZSwgeyBUcmVlTm9kZSB9IGZyb20gJ3JjLXRyZWUnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgRGlyZWN0b3J5VHJlZSBmcm9tICcuL0RpcmVjdG9yeVRyZWUnO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgY29sbGFwc2VNb3Rpb24gZnJvbSAnLi4vX3V0aWwvbW90aW9uJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRyZWUgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnJlbmRlclN3aXRjaGVySWNvbiA9IChwcmVmaXhDbHMsIHN3aXRjaGVySWNvbiwgeyBpc0xlYWYsIGV4cGFuZGVkLCBsb2FkaW5nIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgc2hvd0xpbmUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAobG9hZGluZykge1xuICAgICAgICAgICAgICAgIHJldHVybiA8SWNvbiB0eXBlPVwibG9hZGluZ1wiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zd2l0Y2hlci1sb2FkaW5nLWljb25gfS8+O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3Qgc3dpdGNoZXJDbHMgPSBgJHtwcmVmaXhDbHN9LXN3aXRjaGVyLWljb25gO1xuICAgICAgICAgICAgaWYgKHN3aXRjaGVySWNvbikge1xuICAgICAgICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoc3dpdGNoZXJJY29uLCB7XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzd2l0Y2hlckljb24ucHJvcHMuY2xhc3NOYW1lIHx8ICcnLCBzd2l0Y2hlckNscyksXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoaXNMZWFmKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNob3dMaW5lID8gPEljb24gdHlwZT1cImZpbGVcIiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tc3dpdGNoZXItbGluZS1pY29uYH0vPiA6IG51bGw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc2hvd0xpbmUgPyAoPEljb24gdHlwZT17ZXhwYW5kZWQgPyAnbWludXMtc3F1YXJlJyA6ICdwbHVzLXNxdWFyZSd9IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zd2l0Y2hlci1saW5lLWljb25gfSB0aGVtZT1cIm91dGxpbmVkXCIvPikgOiAoPEljb24gdHlwZT1cImNhcmV0LWRvd25cIiBjbGFzc05hbWU9e3N3aXRjaGVyQ2xzfSB0aGVtZT1cImZpbGxlZFwiLz4pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNldFRyZWVSZWYgPSAobm9kZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy50cmVlID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJUcmVlID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcHJvcHMgfSA9IHRoaXM7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUsIHNob3dJY29uLCBzd2l0Y2hlckljb24sIGJsb2NrTm9kZSwgY2hpbGRyZW4sIH0gPSBwcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHsgY2hlY2thYmxlIH0gPSBwcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygndHJlZScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICByZXR1cm4gKDxSY1RyZWUgcmVmPXt0aGlzLnNldFRyZWVSZWZ9IHsuLi5wcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1pY29uLWhpZGVgXTogIXNob3dJY29uLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWJsb2NrLW5vZGVgXTogYmxvY2tOb2RlLFxuICAgICAgICAgICAgfSl9IGNoZWNrYWJsZT17Y2hlY2thYmxlID8gPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNoZWNrYm94LWlubmVyYH0vPiA6IGNoZWNrYWJsZX0gc3dpdGNoZXJJY29uPXsobm9kZVByb3BzKSA9PiB0aGlzLnJlbmRlclN3aXRjaGVySWNvbihwcmVmaXhDbHMsIHN3aXRjaGVySWNvbiwgbm9kZVByb3BzKX0+XG4gICAgICAgIHtjaGlsZHJlbn1cbiAgICAgIDwvUmNUcmVlPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJUcmVlfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cblRyZWUuVHJlZU5vZGUgPSBUcmVlTm9kZTtcblRyZWUuRGlyZWN0b3J5VHJlZSA9IERpcmVjdG9yeVRyZWU7XG5UcmVlLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBjaGVja2FibGU6IGZhbHNlLFxuICAgIHNob3dJY29uOiBmYWxzZSxcbiAgICBtb3Rpb246IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgY29sbGFwc2VNb3Rpb24pLCB7IG1vdGlvbkFwcGVhcjogZmFsc2UgfSksXG4gICAgYmxvY2tOb2RlOiBmYWxzZSxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWRBO0FBQ0E7QUFlQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBTEE7QUFDQTtBQXRCQTtBQWlDQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7Ozs7QUFyQ0E7QUFDQTtBQURBO0FBdUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUpBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/tree/Tree.js
