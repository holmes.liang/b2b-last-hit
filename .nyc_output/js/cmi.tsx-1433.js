__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DATE_FORMAT", function() { return DATE_FORMAT; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _quote_compare_all_steps__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../quote/compare/all-steps */ "./src/app/desk/quote/compare/all-steps.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _desk_styles_common__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk-styles/common */ "./src/app/desk/styles/common.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");










var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/crosss-sell/cmi.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            .congratulation {\n            padding: 44px 0 54px 0;\n            text-align: center;\n            color: ", ";\n            i {\n                font-size: 100px;\n            }\n        }\n        \n         .paragraph {\n            font-size: 16px;\n            margin: 0 0 15px 0;\n            text-align: center;\n            color: rgba(0, 0, 0, 0.85);\n            padding: 0 10px;\n            .policy-no-link {\n                display: inline-block;\n                margin: 0 0 0 3px;\n            }\n            .pdf-link-list {\n                font-size: 14px;\n                margin: 10px 0;\n                .pdf-link-item {\n                    display: block;\n                    margin: 0 15px 10px 0;\n                    line-height: 20px;\n                }\n            }\n        }\n        .page-title--main {\n            font-size: 20px;\n            padding: 14px 24px 24px 24px;\n            text-align: center;\n            text-transform: uppercase;\n            font-weight: bold;\n            color: rgba(0, 0, 0, 0.85);\n            white-space: normal;\n        }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .mobile-sell {\n          .ant-row .ant-col:first-child {\n            text-align: left;\n            width: 100%;\n            padding-bottom: 5px;\n          }\n          .ant-row .ant-col:last-child {\n            text-align: left;\n            width: 100%;\n          }\n          .mobile-view {\n            .ant-row .ant-col:first-child {\n              width: 37.5%;\n              line-height: 40px;\n            }\n            .ant-row .ant-col:last-child { \n              width: auto;\n            }\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}















var isMobile = _common__WEBPACK_IMPORTED_MODULE_16__["Utils"].getIsMobile();
var DATE_FORMAT = "DD/MM/YYYY";
var formItemLayout = {
  labelCol: {
    span: 9
  },
  wrapperCol: {
    span: 12
  }
};

var Cmi =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(Cmi, _ModelWidget);

  function Cmi(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Cmi);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Cmi).call(this, props, context));

    _this.handleIssue =
    /*#__PURE__*/
    function () {
      var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(policy) {
        var callIssue;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                callIssue =
                /*#__PURE__*/
                function () {
                  var _ref2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
                  /*#__PURE__*/
                  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
                    var transId;
                    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _this.setState({
                              loading: true
                            });

                            transId = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(_this.props.match, "params.transId");

                            try {
                              policy.ext._ui = {
                                web: {
                                  step: _quote_compare_all_steps__WEBPACK_IMPORTED_MODULE_19__["AllSteps"].ISSUE
                                }
                              };
                              _common__WEBPACK_IMPORTED_MODULE_16__["Ajax"].post("/cart/".concat(transId, "/crosssell/issue"), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_2__["default"])({}, policy), {
                                loading: true
                              }).then(function (response) {
                                var _ref3 = response.body || {
                                  respData: {}
                                },
                                    respData = _ref3.respData;

                                var success = respData.success,
                                    suspended = respData.suspended,
                                    nextPolicy = respData.nextPolicy,
                                    message = respData.message,
                                    policyId = respData.policyId,
                                    policy = respData.policy;

                                if (!success && !suspended) {
                                  antd__WEBPACK_IMPORTED_MODULE_12__["notification"].error({
                                    message: message
                                  });
                                } else {
                                  if (success) {
                                    _this.setState({
                                      issued: true,
                                      policy: policy
                                    });

                                    _this.loadPdfList(policyId);

                                    _this.props.handleIssue && _this.props.handleIssue();
                                  } else if (suspended) {
                                    _this.setState({
                                      policy: lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(respData, "policy"),
                                      suspended: true
                                    });

                                    _this.props.handleIssue && _this.props.handleIssue();
                                  }
                                }
                              }).catch(function (error) {
                                console.log(error);
                              });
                            } catch (error) {
                              console.error(error);
                            } finally {
                              _this.setState({
                                loading: false
                              });
                            }

                          case 3:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function callIssue() {
                    return _ref2.apply(this, arguments);
                  };
                }();

                _this.props.form.validateFields(function (err, fieldsValue) {
                  if (err) {
                    return;
                  }

                  callIssue();
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Cmi, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        CrossSellCMI: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Cmi.prototype), "initState", this).call(this), {
        issued: false,
        suspended: false,
        policy: this.props.policy,
        loading: false,
        pdfList: []
      });
    }
  }, {
    key: "renderPdfList",
    value: function renderPdfList() {
      var _this2 = this;

      var _this$state = this.state,
          pdfList = _this$state.pdfList,
          policy = _this$state.policy;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        style: {
          display: "inline-flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_16__["Ajax"].getServiceLocation("/policies/".concat(_this2.getValueFromModel("policyId") || policy.policyId, "/pdf/").concat(name, "?access_token=").concat(_common__WEBPACK_IMPORTED_MODULE_16__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_16__["Consts"].AUTH_KEY)));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("a", {
          href: pdfUrl,
          className: "pdf-link-item",
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 98
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 99
          },
          __self: this
        }), "".concat(displayName));
      }));
    }
  }, {
    key: "renderIssued",
    value: function renderIssued() {
      var policy = this.state.policy;
      var policyNo = policy.policyNo;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(StyleIssued, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "congratulation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
        type: "check-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Thank you for insuring with us.").thai("ขอบคุณที่ทำประกันกับเรา.").my("ကြှနျုပျတို့နှငျ့အတူ insuring အတွက်ကျေးဇူးတင်ပါသည်.").getMessage(), " ", _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Your policy has been issued, policy number").thai("กรมธรรม์ของท่านได้รับการอนุมัติเรียบร้อยแล้ว หมายเลขกรมธรรมของท่านคือ ").my("သင့်ရဲ့မူဝါဒ, မူဝါဒသည်အရေအတွက်ကိုထုတ်ပေးလျက်ရှိသည် ").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("a", {
        className: "policy-no-link",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, policyNo)), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "paragraph",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "pdf-link-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, this.renderPdfList())));
    }
  }, {
    key: "renderSuspended",
    value: function renderSuspended() {
      var quoteNo = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(this.state.policy, "quoteNo");

      var policyNo = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(this.state.policy, "policyNo");

      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(StyleIssued, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Thank you for insuring with us. The case has been sent to insurer for underwriting. Reference Number: ".concat(policyNo || quoteNo)).thai("\u0E02\u0E2D\u0E1A\u0E04\u0E38\u0E13\u0E17\u0E35\u0E48\u0E17\u0E33\u0E1B\u0E23\u0E30\u0E01\u0E31\u0E19\u0E01\u0E31\u0E1A\u0E40\u0E23\u0E32 \u0E01\u0E23\u0E13\u0E35\u0E16\u0E39\u0E01\u0E2A\u0E48\u0E07\u0E44\u0E1B\u0E22\u0E31\u0E07 \u0E1A\u0E23\u0E34\u0E29\u0E31\u0E17 \u0E1B\u0E23\u0E30\u0E01\u0E31\u0E19\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E01\u0E32\u0E23\u0E08\u0E31\u0E14\u0E08\u0E33\u0E2B\u0E19\u0E48\u0E32\u0E22 \u0E2B\u0E21\u0E32\u0E22\u0E40\u0E25\u0E02\u0E2D\u0E49\u0E32\u0E07\u0E2D\u0E34\u0E07: ".concat(policyNo || quoteNo)).my("\u1000\u103C\u103E\u1014\u103B\u102F\u1015\u103B\u1010\u102D\u102F\u1037\u1014\u103E\u1004\u103B\u1037\u1021\u1010\u1030 insuring \u1021\u1010\u103D\u1000\u103A\u1000\u103B\u1031\u1038\u1007\u1030\u1038\u1010\u1004\u103A\u1015\u102B\u101E\u100A\u103A\u104B \u1021\u1006\u102D\u102F\u1015\u102B\u1000\u102D\u1005\u1039\u1005\u1010\u103D\u1004\u103A underwriting \u1019\u103B\u102C\u1038\u1021\u1010\u103D\u1000\u103A\u1021\u102C\u1019\u1001\u1036\u1005\u1031\u1001\u103C\u1004\u103A\u1038\u1004\u103E\u102B\u1005\u1031\u101C\u103D\u103E\u1010\u103A\u1001\u1032\u1037\u101E\u100A\u103A\u104B \u1000\u102D\u102F\u1038\u1000\u102C\u1038\u1005\u101B\u102C\u1021\u101B\u1031\u1021\u1010\u103D\u1000\u103A: ".concat(policyNo || quoteNo)).getMessage()));
    }
  }, {
    key: "loadPdfList",
    value: function loadPdfList(policyId) {
      var _this3 = this;

      if (!policyId) {
        return;
      }

      this.getHelpers().getAjax().get("/policies/".concat(policyId, "/outputs"), {}, {}).then(function (response) {
        var respData = response.body.respData;

        _this3.setState({
          pdfList: respData
        });
      }).catch(function (error) {});
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, propsNameFixed) {
      if (!propsNameFixed) return propName;
      return "".concat(propsNameFixed, ".").concat(propName);
    }
  }, {
    key: "renderDateRange",
    value: function renderDateRange() {
      var _this4 = this;

      var policy = this.state.policy;
      var form = this.props.form;
      var openEnd = Boolean(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(_model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy), "openEnd"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_21__["StyleDateRange"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 234
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_22__["FieldGroup"], {
        className: "date-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 235
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NDateFilter"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        size: "large",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Start Date").thai("วันที่เริ่มต้นโดยสมัครใจ").getMessage(),
        propName: this.generatePropName("effDate", "ext"),
        format: _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT,
        onChangeStartDate: function onChangeStartDate(value) {
          if (!openEnd) {
            //TODO this will be refactor
            _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].setValue({
              model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy)
            }, _this4.generatePropName("expDate", "ext"), _common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].toDate(value).add(1, "years").format(_common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT), true);
            _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].setValue({
              model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy)
            }, _this4.generatePropName("effDate", "ext"), _common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].toDate(value)), true);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 236
        },
        __self: this
      }), !openEnd && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
        className: "to-date",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 271
        },
        __self: this
      }, "~"), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NDate"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        size: "large",
        disabled: true,
        format: _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT,
        propName: this.generatePropName("expDate", "ext"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 273
        },
        __self: this
      }))));
    }
  }, {
    key: "renderQuote",
    value: function renderQuote() {
      var _this5 = this;

      var policy = this.state.policy;
      var ext = policy.ext;
      var form = this.props.form;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 294
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: isMobile ? "mobile-sell" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: isMobile ? "mobile-view" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 296
        },
        __self: this
      }, isMobile && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_14__["NFormItem"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Premium").thai("รหัสรถยนต์").getMessage(),
        propName: "ext.cmiCode",
        layoutCol: formItemLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 297
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Utils"].renderPolicyPremium(_model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy)))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_14__["NFormItem"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("CMI Code").thai("รหัสรถยนต์").getMessage(),
        propName: "ext.cmiCode",
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 310
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 319
        },
        __self: this
      }, ext.cmiCode))), this.renderDateRange(), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Form"].Item, Object.assign({
        colon: false
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].FORM_ITEM_LAYOUT, {
        label: " ",
        className: "btn-wrap",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 323
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Button"], {
        type: "primary",
        style: {
          width: isMobile ? "100%" : "auto"
        },
        size: "large",
        onClick: function onClick() {
          return _this5.handleIssue(policy);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 324
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Confirm And Issue").thai("ยืนยันการออกกรมธรรม์").my("အတည်ပြုပြီး Issue").getMessage()))));
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$state2 = this.state,
          suspended = _this$state2.suspended,
          issued = _this$state2.issued;
      if (!this.props.policy) return null;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.CrossSellCMI, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 342
        },
        __self: this
      }, issued ? this.renderIssued() : suspended ? this.renderSuspended() : this.renderQuote());
    }
  }]);

  return Cmi;
}(_component__WEBPACK_IMPORTED_MODULE_14__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_13__["withRouter"])(antd__WEBPACK_IMPORTED_MODULE_12__["Form"].create()(Cmi)));

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_20__["default"].getTheme(),
    BORDER_COLOR = _Theme$getTheme.BORDER_COLOR;

var StyleIssued = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject2(), BORDER_COLOR);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2Nyb3Nzcy1zZWxsL2NtaS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvY3Jvc3NzLXNlbGwvY21pLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBGb3JtLCBCdXR0b24sIFNwaW4sIEljb24sIG5vdGlmaWNhdGlvbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgbW9tZW50LCB7IE1vbWVudCB9IGZyb20gXCJtb21lbnRcIjtcbmltcG9ydCB7IHdpdGhSb3V0ZXIsIFJvdXRlQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwicmVhY3Qtcm91dGVyXCI7XG5cbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgU3R5bGVkRElWIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQWpheCwgQ29uc3RzLCBTdG9yYWdlLCBVdGlscywgTGFuZ3VhZ2UsIERhdGVVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBORm9ybUl0ZW0gfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTkRhdGUsIE5EYXRlRmlsdGVyIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbGxlciB9IGZyb20gXCJAbW9kZWxcIjtcbmltcG9ydCB7IEZvcm1Db21wb25lbnRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9mb3JtXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCIuLi8uLi9xdW90ZS9jb21wYXJlL2FsbC1zdGVwc1wiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5pbXBvcnQgeyBTdHlsZURhdGVSYW5nZSB9IGZyb20gXCJAZGVzay1zdHlsZXMvY29tbW9uXCI7XG5pbXBvcnQgeyBGaWVsZEdyb3VwIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5leHBvcnQgdHlwZSBDcm9zc1NlbGxDTUlQcm9wcyA9IHtcbiAgcG9saWN5OiBhbnk7XG4gIGhhbmRsZUlzc3VlPzogKCkgPT4gdm9pZDtcbn0gJiBGb3JtQ29tcG9uZW50UHJvcHMgJiBSb3V0ZUNvbXBvbmVudFByb3BzO1xuZXhwb3J0IGNvbnN0IERBVEVfRk9STUFUID0gXCJERC9NTS9ZWVlZXCI7XG5leHBvcnQgdHlwZSBDcm9zc1NlbGxDTUlTdGF0ZSA9IHtcbiAgaXNzdWVkOiBib29sZWFuO1xuICBzdXNwZW5kZWQ6IGJvb2xlYW47XG4gIHBvbGljeTogYW55O1xuICBsb2FkaW5nOiBib29sZWFuO1xuICBwZGZMaXN0OiBhbnk7XG59O1xuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmV4cG9ydCB0eXBlIENyb3NzU2VsbENNSUNvbXBvbmVudHMgPSB7XG4gIENyb3NzU2VsbENNSTogU3R5bGVkRElWO1xufTtcbmNvbnN0IGZvcm1JdGVtTGF5b3V0ID0ge1xuICBsYWJlbENvbDogeyBzcGFuOiA5IH0sXG4gIHdyYXBwZXJDb2w6IHsgc3BhbjogMTIgfSxcbn07XG5cbmNsYXNzIENtaTxQIGV4dGVuZHMgQ3Jvc3NTZWxsQ01JUHJvcHMsXG4gIFMgZXh0ZW5kcyBDcm9zc1NlbGxDTUlTdGF0ZSxcbiAgQyBleHRlbmRzIENyb3NzU2VsbENNSUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogQ3Jvc3NTZWxsQ01JUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIENyb3NzU2VsbENNSTogU3R5bGVkLmRpdmBcbiAgICAgICAgLm1vYmlsZS1zZWxsIHtcbiAgICAgICAgICAuYW50LXJvdyAuYW50LWNvbDpmaXJzdC1jaGlsZCB7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICAuYW50LXJvdyAuYW50LWNvbDpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vYmlsZS12aWV3IHtcbiAgICAgICAgICAgIC5hbnQtcm93IC5hbnQtY29sOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgd2lkdGg6IDM3LjUlO1xuICAgICAgICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5hbnQtcm93IC5hbnQtY29sOmxhc3QtY2hpbGQgeyBcbiAgICAgICAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIGlzc3VlZDogZmFsc2UsXG4gICAgICBzdXNwZW5kZWQ6IGZhbHNlLFxuICAgICAgcG9saWN5OiB0aGlzLnByb3BzLnBvbGljeSxcbiAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgcGRmTGlzdDogW10sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUGRmTGlzdCgpIHtcbiAgICBjb25zdCB7IHBkZkxpc3QsIHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBzdHlsZT17eyBkaXNwbGF5OiBcImlubGluZS1mbGV4XCIgfX0+XG4gICAgICAgIHsocGRmTGlzdCB8fCBbXSkubWFwKChwZGY6IGFueSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgZGlzcGxheU5hbWUsIG5hbWUsIGZpbGVOYW1lIH0gPSBwZGY7XG4gICAgICAgICAgY29uc3QgcGRmVXJsID0gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oXG4gICAgICAgICAgICBgL3BvbGljaWVzLyR7dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeUlkXCIpIHx8XG4gICAgICAgICAgICBwb2xpY3kucG9saWN5SWR9L3BkZi8ke25hbWV9P2FjY2Vzc190b2tlbj0ke1N0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSl9YCxcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8YSBocmVmPXtwZGZVcmx9IGNsYXNzTmFtZT1cInBkZi1saW5rLWl0ZW1cIiBrZXk9e2ZpbGVOYW1lfT5cbiAgICAgICAgICAgICAgPEljb24gdHlwZT1cImZpbGUtcGRmXCIvPlxuICAgICAgICAgICAgICB7YCR7ZGlzcGxheU5hbWV9YH1cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlcklzc3VlZCgpIHtcbiAgICBjb25zdCB7IHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IHBvbGljeU5vIH0gPSBwb2xpY3k7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPFN0eWxlSXNzdWVkPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbmdyYXR1bGF0aW9uXCI+XG4gICAgICAgICAgPEljb24gdHlwZT1cImNoZWNrLWNpcmNsZVwiLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFyYWdyYXBoIHBhcmFncmFwaC0tbWFpblwiPlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlRoYW5rIHlvdSBmb3IgaW5zdXJpbmcgd2l0aCB1cy5cIilcbiAgICAgICAgICAgIC50aGFpKFwi4LiC4Lit4Lia4LiE4Li44LiT4LiX4Li14LmI4LiX4Liz4Lib4Lij4Liw4LiB4Lix4LiZ4LiB4Lix4Lia4LmA4Lij4LiyLlwiKVxuICAgICAgICAgICAgLm15KFwi4YCA4YC84YC+4YCU4YC74YCv4YCV4YC74YCQ4YCt4YCv4YC34YCU4YC+4YCE4YC74YC34YCh4YCQ4YCwIGluc3VyaW5nIOGAoeGAkOGAveGAgOGAuuGAgOGAu+GAseGAuOGAh+GAsOGAuOGAkOGAhOGAuuGAleGAq+GAnuGAiuGAui5cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9e1wiIFwifVxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIllvdXIgcG9saWN5IGhhcyBiZWVuIGlzc3VlZCwgcG9saWN5IG51bWJlclwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguIHguKPguKHguJjguKPguKPguKHguYzguILguK3guIfguJfguYjguLLguJnguYTguJTguYnguKPguLHguJrguIHguLLguKPguK3guJnguLjguKHguLHguJXguLTguYDguKPguLXguKLguJrguKPguYnguK3guKLguYHguKXguYnguKcg4Lir4Lih4Liy4Lii4LmA4Lil4LiC4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LiC4Lit4LiH4LiX4LmI4Liy4LiZ4LiE4Li34LitIFwiKVxuICAgICAgICAgICAgLm15KFwi4YCe4YCE4YC64YC34YCb4YCy4YC34YCZ4YCw4YCd4YCr4YCSLCDhgJnhgLDhgJ3hgKvhgJLhgJ7hgIrhgLrhgKHhgJvhgLHhgKHhgJDhgL3hgIDhgLrhgIDhgK3hgK/hgJHhgK/hgJDhgLrhgJXhgLHhgLjhgJzhgLvhgIDhgLrhgJvhgL7hgK3hgJ7hgIrhgLogXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDxhIGNsYXNzTmFtZT1cInBvbGljeS1uby1saW5rXCI+e3BvbGljeU5vfTwvYT5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFyYWdyYXBoXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwZGYtbGluay1saXN0XCI+e3RoaXMucmVuZGVyUGRmTGlzdCgpfTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvU3R5bGVJc3N1ZWQ+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyU3VzcGVuZGVkKCkge1xuICAgIGxldCBxdW90ZU5vID0gXy5nZXQodGhpcy5zdGF0ZS5wb2xpY3ksIFwicXVvdGVOb1wiKTtcbiAgICBsZXQgcG9saWN5Tm8gPSBfLmdldCh0aGlzLnN0YXRlLnBvbGljeSwgXCJwb2xpY3lOb1wiKTtcbiAgICByZXR1cm4gKFxuICAgICAgPFN0eWxlSXNzdWVkPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaCBwYXJhZ3JhcGgtLW1haW5cIj5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXG4gICAgICAgICAgICBgVGhhbmsgeW91IGZvciBpbnN1cmluZyB3aXRoIHVzLiBUaGUgY2FzZSBoYXMgYmVlbiBzZW50IHRvIGluc3VyZXIgZm9yIHVuZGVyd3JpdGluZy4gUmVmZXJlbmNlIE51bWJlcjogJHtwb2xpY3lObyB8fCBxdW90ZU5vfWAsXG4gICAgICAgICAgKVxuICAgICAgICAgICAgLnRoYWkoYOC4guC4reC4muC4hOC4uOC4k+C4l+C4teC5iOC4l+C4s+C4m+C4o+C4sOC4geC4seC4meC4geC4seC4muC5gOC4o+C4siDguIHguKPguJPguLXguJbguLnguIHguKrguYjguIfguYTguJvguKLguLHguIcg4Lia4Lij4Li04Lip4Lix4LiXIOC4m+C4o+C4sOC4geC4seC4meC4quC4s+C4q+C4o+C4seC4muC4geC4suC4o+C4iOC4seC4lOC4iOC4s+C4q+C4meC5iOC4suC4oiDguKvguKHguLLguKLguYDguKXguILguK3guYnguLLguIfguK3guLTguIc6ICR7cG9saWN5Tm8gfHwgcXVvdGVOb31gKVxuICAgICAgICAgICAgLm15KGDhgIDhgLzhgL7hgJThgLvhgK/hgJXhgLvhgJDhgK3hgK/hgLfhgJThgL7hgIThgLvhgLfhgKHhgJDhgLAgaW5zdXJpbmcg4YCh4YCQ4YC94YCA4YC64YCA4YC74YCx4YC44YCH4YCw4YC44YCQ4YCE4YC64YCV4YCr4YCe4YCK4YC64YGLIOGAoeGAhuGAreGAr+GAleGAq+GAgOGAreGAheGAueGAheGAkOGAveGAhOGAuiB1bmRlcndyaXRpbmcg4YCZ4YC74YCs4YC44YCh4YCQ4YC94YCA4YC64YCh4YCs4YCZ4YCB4YC24YCF4YCx4YCB4YC84YCE4YC64YC44YCE4YC+4YCr4YCF4YCx4YCc4YC94YC+4YCQ4YC64YCB4YCy4YC34YCe4YCK4YC64YGLIOGAgOGAreGAr+GAuOGAgOGArOGAuOGAheGAm+GArOGAoeGAm+GAseGAoeGAkOGAveGAgOGAujogJHtwb2xpY3lObyB8fCBxdW90ZU5vfWApXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvU3R5bGVJc3N1ZWQ+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgbG9hZFBkZkxpc3QocG9saWN5SWQ6IGFueSkge1xuICAgIGlmICghcG9saWN5SWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5nZXQoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9vdXRwdXRzYCwge30sIHt9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgcGRmTGlzdDogcmVzcERhdGEsXG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgaGFuZGxlSXNzdWUgPSBhc3luYyAocG9saWN5OiBhbnkpID0+IHtcbiAgICBjb25zdCBjYWxsSXNzdWUgPSBhc3luYyAoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgbG9hZGluZzogdHJ1ZSB9KTtcbiAgICAgIGNvbnN0IHRyYW5zSWQgPSBfLmdldCh0aGlzLnByb3BzLm1hdGNoLCBcInBhcmFtcy50cmFuc0lkXCIpO1xuICAgICAgdHJ5IHtcbiAgICAgICAgcG9saWN5LmV4dC5fdWkgPSB7IHdlYjogeyBzdGVwOiBBbGxTdGVwcy5JU1NVRSB9IH07XG4gICAgICAgIEFqYXgucG9zdChcbiAgICAgICAgICBgL2NhcnQvJHt0cmFuc0lkfS9jcm9zc3NlbGwvaXNzdWVgLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIC4uLnBvbGljeSxcbiAgICAgICAgICB9LCB7IGxvYWRpbmc6IHRydWUgfSxcbiAgICAgICAgKVxuICAgICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IHtcbiAgICAgICAgICAgICAgcmVzcERhdGE6IHt9LFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGNvbnN0IHsgc3VjY2Vzcywgc3VzcGVuZGVkLCBuZXh0UG9saWN5LCBtZXNzYWdlLCBwb2xpY3lJZCwgcG9saWN5IH0gPSByZXNwRGF0YTtcbiAgICAgICAgICAgIGlmICghc3VjY2VzcyAmJiAhc3VzcGVuZGVkKSB7XG4gICAgICAgICAgICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7IG1lc3NhZ2U6IG1lc3NhZ2UgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICBpZiAoc3VjY2Vzcykge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgaXNzdWVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgcG9saWN5OiBwb2xpY3ksXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkUGRmTGlzdChwb2xpY3lJZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5oYW5kbGVJc3N1ZSAmJiB0aGlzLnByb3BzLmhhbmRsZUlzc3VlKCk7XG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoc3VzcGVuZGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBwb2xpY3k6IF8uZ2V0KHJlc3BEYXRhLCBcInBvbGljeVwiKSxcbiAgICAgICAgICAgICAgICAgIHN1c3BlbmRlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmhhbmRsZUlzc3VlICYmIHRoaXMucHJvcHMuaGFuZGxlSXNzdWUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgfSBmaW5hbGx5IHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH07XG4gICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKChlcnI6IGFueSwgZmllbGRzVmFsdWU6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjYWxsSXNzdWUoKTtcbiAgICB9KTtcbiAgfTtcblxuICBwcml2YXRlIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgcHJvcHNOYW1lRml4ZWQ/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmICghcHJvcHNOYW1lRml4ZWQpIHJldHVybiBwcm9wTmFtZTtcbiAgICByZXR1cm4gYCR7cHJvcHNOYW1lRml4ZWR9LiR7cHJvcE5hbWV9YDtcbiAgfVxuXG4gIHJlbmRlckRhdGVSYW5nZSgpIHtcbiAgICBjb25zdCB7IHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGZvcm0gfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3Qgb3BlbkVuZCA9IEJvb2xlYW4oXy5nZXQoTW9kZWxsZXIuYXNQcm94aWVkKHBvbGljeSksIFwib3BlbkVuZFwiKSk7XG5cbiAgICByZXR1cm4gPFN0eWxlRGF0ZVJhbmdlPlxuICAgICAgPEZpZWxkR3JvdXAgY2xhc3NOYW1lPVwiZGF0ZS1ncm91cFwiIHNlbGVjdFhzU209e3sgeHM6IDgsIHNtOiA2IH19IHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19PlxuICAgICAgICA8TkRhdGVGaWx0ZXJcbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiU3RhcnQgRGF0ZVwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguKfguLHguJnguJfguLXguYjguYDguKPguLTguYjguKHguJXguYnguJnguYLguJTguKLguKrguKHguLHguITguKPguYPguIhcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG5cbiAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiZWZmRGF0ZVwiLCBcImV4dFwiKX1cbiAgICAgICAgICBmb3JtYXQ9e0NvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX0ZPUk1BVH1cblxuICAgICAgICAgIG9uQ2hhbmdlU3RhcnREYXRlPXsodmFsdWU6IE1vbWVudCkgPT4ge1xuICAgICAgICAgICAgaWYgKCFvcGVuRW5kKSB7XG4gICAgICAgICAgICAgIC8vVE9ETyB0aGlzIHdpbGwgYmUgcmVmYWN0b3JcbiAgICAgICAgICAgICAgTW9kZWxsZXIuc2V0VmFsdWUoXG4gICAgICAgICAgICAgICAgeyBtb2RlbDogTW9kZWxsZXIuYXNQcm94aWVkKHBvbGljeSkgfSxcbiAgICAgICAgICAgICAgICB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJleHBEYXRlXCIsIFwiZXh0XCIpLFxuICAgICAgICAgICAgICAgIERhdGVVdGlscy50b0RhdGUodmFsdWUpXG4gICAgICAgICAgICAgICAgICAuYWRkKDEsIFwieWVhcnNcIilcbiAgICAgICAgICAgICAgICAgIC5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUKSxcbiAgICAgICAgICAgICAgICB0cnVlLFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICBNb2RlbGxlci5zZXRWYWx1ZShcbiAgICAgICAgICAgICAgICB7IG1vZGVsOiBNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KSB9LFxuICAgICAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImVmZkRhdGVcIiwgXCJleHRcIiksXG4gICAgICAgICAgICAgICAgRGF0ZVV0aWxzLmZvcm1hdERhdGUoRGF0ZVV0aWxzLnRvRGF0ZSh2YWx1ZSkpLFxuICAgICAgICAgICAgICAgIHRydWUsXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfX1cbiAgICAgICAgLz5cblxuICAgICAgICB7IW9wZW5FbmQgJiYgKFxuICAgICAgICAgIDw+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0by1kYXRlXCI+fjwvc3Bhbj5cblxuICAgICAgICAgICAgPE5EYXRlXG4gICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICAgICAgICBkaXNhYmxlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgZm9ybWF0PXtDb25zdHMuREFURV9GT1JNQVQuREFURV9GT1JNQVR9XG4gICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJleHBEYXRlXCIsIFwiZXh0XCIpfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8Lz5cbiAgICAgICAgKX1cbiAgICAgIDwvRmllbGRHcm91cD5cbiAgICA8L1N0eWxlRGF0ZVJhbmdlPjtcbiAgfVxuXG5cbiAgcHJpdmF0ZSByZW5kZXJRdW90ZSgpIHtcbiAgICBjb25zdCB7IHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGV4dCB9ID0gcG9saWN5O1xuICAgIGNvbnN0IHsgZm9ybSB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8U3BpbiBzaXplPVwibGFyZ2VcIiBzcGlubmluZz17dGhpcy5zdGF0ZS5sb2FkaW5nfT5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2lzTW9iaWxlID8gXCJtb2JpbGUtc2VsbFwiIDogXCJcIn0+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2lzTW9iaWxlID8gXCJtb2JpbGUtdmlld1wiIDogXCJcIn0+XG4gICAgICAgICAgICB7aXNNb2JpbGUgJiYgPE5Gb3JtSXRlbVxuICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICBtb2RlbD17TW9kZWxsZXIuYXNQcm94aWVkKHBvbGljeSl9XG4gICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlByZW1pdW1cIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4quC4o+C4luC4ouC4meC4leC5jFwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIHByb3BOYW1lPXtcImV4dC5jbWlDb2RlXCJ9XG4gICAgICAgICAgICAgIGxheW91dENvbD17Zm9ybUl0ZW1MYXlvdXR9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAge1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0oTW9kZWxsZXIuYXNQcm94aWVkKHBvbGljeSkpfVxuICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8L05Gb3JtSXRlbT59XG4gICAgICAgICAgICA8TkZvcm1JdGVtXG4gICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiQ01JIENvZGVcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4quC4o+C4luC4ouC4meC4leC5jFwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIHByb3BOYW1lPXtcImV4dC5jbWlDb2RlXCJ9XG4gICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3Bhbj57ZXh0LmNtaUNvZGV9PC9zcGFuPlxuICAgICAgICAgICAgPC9ORm9ybUl0ZW0+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAge3RoaXMucmVuZGVyRGF0ZVJhbmdlKCl9XG4gICAgICAgICAgPEZvcm0uSXRlbSBjb2xvbj17ZmFsc2V9IHsuLi5Db25zdHMuRk9STV9JVEVNX0xBWU9VVH0gbGFiZWw9XCIgXCIgY2xhc3NOYW1lPVwiYnRuLXdyYXBcIj5cbiAgICAgICAgICAgIDxCdXR0b24gdHlwZT1cInByaW1hcnlcIiBzdHlsZT17eyB3aWR0aDogaXNNb2JpbGUgPyBcIjEwMCVcIiA6IFwiYXV0b1wiIH19IHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlSXNzdWUocG9saWN5KX0+XG4gICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkNvbmZpcm0gQW5kIElzc3VlXCIpXG4gICAgICAgICAgICAgICAgLnRoYWkoXCLguKLguLfguJnguKLguLHguJnguIHguLLguKPguK3guK3guIHguIHguKPguKHguJjguKPguKPguKHguYxcIilcbiAgICAgICAgICAgICAgICAubXkoXCLhgKHhgJDhgIrhgLrhgJXhgLzhgK/hgJXhgLzhgK7hgLggSXNzdWVcIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgPC9Gb3JtLkl0ZW0+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgc3VzcGVuZGVkLCBpc3N1ZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKCF0aGlzLnByb3BzLnBvbGljeSkgcmV0dXJuIG51bGw7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkNyb3NzU2VsbENNST5cbiAgICAgICAge2lzc3VlZCA/IHRoaXMucmVuZGVySXNzdWVkKCkgOiBzdXNwZW5kZWQgPyB0aGlzLnJlbmRlclN1c3BlbmRlZCgpIDogdGhpcy5yZW5kZXJRdW90ZSgpfVxuICAgICAgPC9DLkNyb3NzU2VsbENNST5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhSb3V0ZXIoRm9ybS5jcmVhdGU8Q3Jvc3NTZWxsQ01JUHJvcHM+KCkoQ21pKSk7XG5cbmNvbnN0IHsgQk9SREVSX0NPTE9SIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgU3R5bGVJc3N1ZWQgPSBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgLmNvbmdyYXR1bGF0aW9uIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDQ0cHggMCA1NHB4IDA7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBjb2xvcjogJHtCT1JERVJfQ09MT1J9O1xuICAgICAgICAgICAgaSB7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgIC5wYXJhZ3JhcGgge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIDAgMTVweCAwO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44NSk7XG4gICAgICAgICAgICBwYWRkaW5nOiAwIDEwcHg7XG4gICAgICAgICAgICAucG9saWN5LW5vLWxpbmsge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICBtYXJnaW46IDAgMCAwIDNweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5wZGYtbGluay1saXN0IHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAxMHB4IDA7XG4gICAgICAgICAgICAgICAgLnBkZi1saW5rLWl0ZW0ge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIDE1cHggMTBweCAwO1xuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLnBhZ2UtdGl0bGUtLW1haW4ge1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgcGFkZGluZzogMTRweCAyNHB4IDI0cHggMjRweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICAgICAgfVxuICAgIGA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBS0E7QUFhQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQ0E7QUFJQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTJIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFBQTtBQUVBO0FBRUE7QUFEQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBN0NBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBOENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFyREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQTVIQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQXdCQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7OztBQUVBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFHQTs7O0FBeURBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBR0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBT0E7QUFDQTtBQUFBO0FBS0E7QUFDQTtBQTlCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7OztBQUVBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUE3U0E7QUFDQTtBQStTQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/crosss-sell/cmi.tsx
