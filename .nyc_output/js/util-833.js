__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flatArray", function() { return flatArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "treeMap", function() { return treeMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flatFilter", function() { return flatFilter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizeColumns", function() { return normalizeColumns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generateValueMaps", function() { return generateValueMaps; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}


function flatArray() {
  var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var childrenName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'children';
  var result = [];

  var loop = function loop(array) {
    array.forEach(function (item) {
      if (item[childrenName]) {
        var newItem = _extends({}, item);

        delete newItem[childrenName];
        result.push(newItem);

        if (item[childrenName].length > 0) {
          loop(item[childrenName]);
        }
      } else {
        result.push(item);
      }
    });
  };

  loop(data);
  return result;
}
function treeMap(tree, mapper) {
  var childrenName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'children';
  return tree.map(function (node, index) {
    var extra = {};

    if (node[childrenName]) {
      extra[childrenName] = treeMap(node[childrenName], mapper, childrenName);
    }

    return _extends(_extends({}, mapper(node, index)), extra);
  });
}
function flatFilter(tree, callback) {
  return tree.reduce(function (acc, node) {
    if (callback(node)) {
      acc.push(node);
    }

    if (node.children) {
      var children = flatFilter(node.children, callback);
      acc.push.apply(acc, _toConsumableArray(children));
    }

    return acc;
  }, []);
}
function normalizeColumns(elements) {
  var columns = [];
  react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(elements, function (element) {
    if (!react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](element)) {
      return;
    }

    var column = _extends({}, element.props);

    if (element.key) {
      column.key = element.key;
    }

    if (element.type && element.type.__ANT_TABLE_COLUMN_GROUP) {
      column.children = normalizeColumns(column.children);
    }

    columns.push(column);
  });
  return columns;
}
function generateValueMaps(items) {
  var maps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  (items || []).forEach(function (_ref) {
    var value = _ref.value,
        children = _ref.children;
    maps[value.toString()] = value;
    generateValueMaps(children, maps);
  });
  return maps;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90YWJsZS91dGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90YWJsZS91dGlsLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5leHBvcnQgZnVuY3Rpb24gZmxhdEFycmF5KGRhdGEgPSBbXSwgY2hpbGRyZW5OYW1lID0gJ2NoaWxkcmVuJykge1xuICAgIGNvbnN0IHJlc3VsdCA9IFtdO1xuICAgIGNvbnN0IGxvb3AgPSAoYXJyYXkpID0+IHtcbiAgICAgICAgYXJyYXkuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgICAgICAgIGlmIChpdGVtW2NoaWxkcmVuTmFtZV0pIHtcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdJdGVtID0gT2JqZWN0LmFzc2lnbih7fSwgaXRlbSk7XG4gICAgICAgICAgICAgICAgZGVsZXRlIG5ld0l0ZW1bY2hpbGRyZW5OYW1lXTtcbiAgICAgICAgICAgICAgICByZXN1bHQucHVzaChuZXdJdGVtKTtcbiAgICAgICAgICAgICAgICBpZiAoaXRlbVtjaGlsZHJlbk5hbWVdLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgbG9vcChpdGVtW2NoaWxkcmVuTmFtZV0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGl0ZW0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xuICAgIGxvb3AoZGF0YSk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cbmV4cG9ydCBmdW5jdGlvbiB0cmVlTWFwKHRyZWUsIG1hcHBlciwgY2hpbGRyZW5OYW1lID0gJ2NoaWxkcmVuJykge1xuICAgIHJldHVybiB0cmVlLm1hcCgobm9kZSwgaW5kZXgpID0+IHtcbiAgICAgICAgY29uc3QgZXh0cmEgPSB7fTtcbiAgICAgICAgaWYgKG5vZGVbY2hpbGRyZW5OYW1lXSkge1xuICAgICAgICAgICAgZXh0cmFbY2hpbGRyZW5OYW1lXSA9IHRyZWVNYXAobm9kZVtjaGlsZHJlbk5hbWVdLCBtYXBwZXIsIGNoaWxkcmVuTmFtZSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgbWFwcGVyKG5vZGUsIGluZGV4KSksIGV4dHJhKTtcbiAgICB9KTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBmbGF0RmlsdGVyKHRyZWUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIHRyZWUucmVkdWNlKChhY2MsIG5vZGUpID0+IHtcbiAgICAgICAgaWYgKGNhbGxiYWNrKG5vZGUpKSB7XG4gICAgICAgICAgICBhY2MucHVzaChub2RlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAobm9kZS5jaGlsZHJlbikge1xuICAgICAgICAgICAgY29uc3QgY2hpbGRyZW4gPSBmbGF0RmlsdGVyKG5vZGUuY2hpbGRyZW4sIGNhbGxiYWNrKTtcbiAgICAgICAgICAgIGFjYy5wdXNoKC4uLmNoaWxkcmVuKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYWNjO1xuICAgIH0sIFtdKTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBub3JtYWxpemVDb2x1bW5zKGVsZW1lbnRzKSB7XG4gICAgY29uc3QgY29sdW1ucyA9IFtdO1xuICAgIFJlYWN0LkNoaWxkcmVuLmZvckVhY2goZWxlbWVudHMsIGVsZW1lbnQgPT4ge1xuICAgICAgICBpZiAoIVJlYWN0LmlzVmFsaWRFbGVtZW50KGVsZW1lbnQpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgY29sdW1uID0gT2JqZWN0LmFzc2lnbih7fSwgZWxlbWVudC5wcm9wcyk7XG4gICAgICAgIGlmIChlbGVtZW50LmtleSkge1xuICAgICAgICAgICAgY29sdW1uLmtleSA9IGVsZW1lbnQua2V5O1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbGVtZW50LnR5cGUgJiYgZWxlbWVudC50eXBlLl9fQU5UX1RBQkxFX0NPTFVNTl9HUk9VUCkge1xuICAgICAgICAgICAgY29sdW1uLmNoaWxkcmVuID0gbm9ybWFsaXplQ29sdW1ucyhjb2x1bW4uY2hpbGRyZW4pO1xuICAgICAgICB9XG4gICAgICAgIGNvbHVtbnMucHVzaChjb2x1bW4pO1xuICAgIH0pO1xuICAgIHJldHVybiBjb2x1bW5zO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdlbmVyYXRlVmFsdWVNYXBzKGl0ZW1zLCBtYXBzID0ge30pIHtcbiAgICAoaXRlbXMgfHwgW10pLmZvckVhY2goKHsgdmFsdWUsIGNoaWxkcmVuIH0pID0+IHtcbiAgICAgICAgbWFwc1t2YWx1ZS50b1N0cmluZygpXSA9IHZhbHVlO1xuICAgICAgICBnZW5lcmF0ZVZhbHVlTWFwcyhjaGlsZHJlbiwgbWFwcyk7XG4gICAgfSk7XG4gICAgcmV0dXJuIG1hcHM7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFYQTtBQURBO0FBQ0E7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/table/util.js
