__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputSizes", function() { return InputSizes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fixControlledValue", function() { return fixControlledValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolveOnChange", function() { return resolveOnChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getInputClassName", function() { return getInputClassName; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ClearableLabeledInput */ "./node_modules/antd/es/input/ClearableLabeledInput.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}










var InputSizes = Object(_util_type__WEBPACK_IMPORTED_MODULE_5__["tuple"])('small', 'default', 'large');
function fixControlledValue(value) {
  if (typeof value === 'undefined' || value === null) {
    return '';
  }

  return value;
}
function resolveOnChange(target, e, onChange) {
  if (onChange) {
    var event = e;

    if (e.type === 'click') {
      // click clear icon
      event = Object.create(e);
      event.target = target;
      event.currentTarget = target;
      var originalInputValue = target.value; // change target ref value cause e.target.value should be '' when clear input

      target.value = '';
      onChange(event); // reset target ref value

      target.value = originalInputValue;
      return;
    }

    onChange(event);
  }
}
function getInputClassName(prefixCls, size, disabled) {
  var _classNames;

  return classnames__WEBPACK_IMPORTED_MODULE_3___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-sm"), size === 'small'), _defineProperty(_classNames, "".concat(prefixCls, "-lg"), size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-disabled"), disabled), _classNames));
}

var Input =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Input, _React$Component);

  function Input(props) {
    var _this;

    _classCallCheck(this, Input);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Input).call(this, props));

    _this.saveClearableInput = function (input) {
      _this.clearableInput = input;
    };

    _this.saveInput = function (input) {
      _this.input = input;
    };

    _this.handleReset = function (e) {
      _this.setValue('', function () {
        _this.focus();
      });

      resolveOnChange(_this.input, e, _this.props.onChange);
    };

    _this.renderInput = function (prefixCls) {
      var _this$props = _this.props,
          className = _this$props.className,
          addonBefore = _this$props.addonBefore,
          addonAfter = _this$props.addonAfter,
          size = _this$props.size,
          disabled = _this$props.disabled; // Fix https://fb.me/react-unknown-prop

      var otherProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(_this.props, ['prefixCls', 'onPressEnter', 'addonBefore', 'addonAfter', 'prefix', 'suffix', 'allowClear', // Input elements must be either controlled or uncontrolled,
      // specify either the value prop, or the defaultValue prop, but not both.
      'defaultValue', 'size', 'inputType']);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("input", _extends({}, otherProps, {
        onChange: _this.handleChange,
        onKeyDown: _this.handleKeyDown,
        className: classnames__WEBPACK_IMPORTED_MODULE_3___default()(getInputClassName(prefixCls, size, disabled), _defineProperty({}, className, className && !addonBefore && !addonAfter)),
        ref: _this.saveInput
      }));
    };

    _this.clearPasswordValueAttribute = function () {
      // https://github.com/ant-design/ant-design/issues/20541
      _this.removePasswordTimeout = setTimeout(function () {
        if (_this.input && _this.input.getAttribute('type') === 'password' && _this.input.hasAttribute('value')) {
          _this.input.removeAttribute('value');
        }
      });
    };

    _this.handleChange = function (e) {
      _this.setValue(e.target.value, _this.clearPasswordValueAttribute);

      resolveOnChange(_this.input, e, _this.props.onChange);
    };

    _this.handleKeyDown = function (e) {
      var _this$props2 = _this.props,
          onPressEnter = _this$props2.onPressEnter,
          onKeyDown = _this$props2.onKeyDown;

      if (e.keyCode === 13 && onPressEnter) {
        onPressEnter(e);
      }

      if (onKeyDown) {
        onKeyDown(e);
      }
    };

    _this.renderComponent = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var value = _this.state.value;
      var customizePrefixCls = _this.props.prefixCls;
      var prefixCls = getPrefixCls('input', customizePrefixCls);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_6__["default"], _extends({}, _this.props, {
        prefixCls: prefixCls,
        inputType: "input",
        value: fixControlledValue(value),
        element: _this.renderInput(prefixCls),
        handleReset: _this.handleReset,
        ref: _this.saveClearableInput
      }));
    };

    var value = typeof props.value === 'undefined' ? props.defaultValue : props.value;
    _this.state = {
      value: value
    };
    return _this;
  }

  _createClass(Input, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.clearPasswordValueAttribute();
    } // Since polyfill `getSnapshotBeforeUpdate` need work with `componentDidUpdate`.
    // We keep an empty function here.

  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {}
  }, {
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate(prevProps) {
      if (Object(_ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_6__["hasPrefixSuffix"])(prevProps) !== Object(_ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_6__["hasPrefixSuffix"])(this.props)) {
        Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(this.input !== document.activeElement, 'Input', "When Input is focused, dynamic add or remove prefix / suffix will make it lose focus caused by dom structure change. Read more: https://ant.design/components/input/#FAQ");
      }

      return null;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.removePasswordTimeout) {
        clearTimeout(this.removePasswordTimeout);
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      this.input.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.input.blur();
    }
  }, {
    key: "select",
    value: function select() {
      this.input.select();
    }
  }, {
    key: "setValue",
    value: function setValue(value, callback) {
      if (!('value' in this.props)) {
        this.setState({
          value: value
        }, callback);
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderComponent);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      if ('value' in nextProps) {
        return {
          value: nextProps.value
        };
      }

      return null;
    }
  }]);

  return Input;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Input.defaultProps = {
  type: 'text'
};
Input.propTypes = {
  type: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  id: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  size: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](InputSizes),
  maxLength: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  value: prop_types__WEBPACK_IMPORTED_MODULE_1__["any"],
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_1__["any"],
  className: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  addonBefore: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  addonAfter: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  onPressEnter: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  onKeyUp: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  onFocus: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  onBlur: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  prefix: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  suffix: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  allowClear: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"]
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(Input);
/* harmony default export */ __webpack_exports__["default"] = (Input);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9JbnB1dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvaW5wdXQvSW5wdXQuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCB7IHR1cGxlIH0gZnJvbSAnLi4vX3V0aWwvdHlwZSc7XG5pbXBvcnQgQ2xlYXJhYmxlTGFiZWxlZElucHV0LCB7IGhhc1ByZWZpeFN1ZmZpeCB9IGZyb20gJy4vQ2xlYXJhYmxlTGFiZWxlZElucHV0JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuZXhwb3J0IGNvbnN0IElucHV0U2l6ZXMgPSB0dXBsZSgnc21hbGwnLCAnZGVmYXVsdCcsICdsYXJnZScpO1xuZXhwb3J0IGZ1bmN0aW9uIGZpeENvbnRyb2xsZWRWYWx1ZSh2YWx1ZSkge1xuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICd1bmRlZmluZWQnIHx8IHZhbHVlID09PSBudWxsKSB7XG4gICAgICAgIHJldHVybiAnJztcbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlO1xufVxuZXhwb3J0IGZ1bmN0aW9uIHJlc29sdmVPbkNoYW5nZSh0YXJnZXQsIGUsIG9uQ2hhbmdlKSB7XG4gICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgIGxldCBldmVudCA9IGU7XG4gICAgICAgIGlmIChlLnR5cGUgPT09ICdjbGljaycpIHtcbiAgICAgICAgICAgIC8vIGNsaWNrIGNsZWFyIGljb25cbiAgICAgICAgICAgIGV2ZW50ID0gT2JqZWN0LmNyZWF0ZShlKTtcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldCA9IHRhcmdldDtcbiAgICAgICAgICAgIGV2ZW50LmN1cnJlbnRUYXJnZXQgPSB0YXJnZXQ7XG4gICAgICAgICAgICBjb25zdCBvcmlnaW5hbElucHV0VmFsdWUgPSB0YXJnZXQudmFsdWU7XG4gICAgICAgICAgICAvLyBjaGFuZ2UgdGFyZ2V0IHJlZiB2YWx1ZSBjYXVzZSBlLnRhcmdldC52YWx1ZSBzaG91bGQgYmUgJycgd2hlbiBjbGVhciBpbnB1dFxuICAgICAgICAgICAgdGFyZ2V0LnZhbHVlID0gJyc7XG4gICAgICAgICAgICBvbkNoYW5nZShldmVudCk7XG4gICAgICAgICAgICAvLyByZXNldCB0YXJnZXQgcmVmIHZhbHVlXG4gICAgICAgICAgICB0YXJnZXQudmFsdWUgPSBvcmlnaW5hbElucHV0VmFsdWU7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgb25DaGFuZ2UoZXZlbnQpO1xuICAgIH1cbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRJbnB1dENsYXNzTmFtZShwcmVmaXhDbHMsIHNpemUsIGRpc2FibGVkKSB7XG4gICAgcmV0dXJuIGNsYXNzTmFtZXMocHJlZml4Q2xzLCB7XG4gICAgICAgIFtgJHtwcmVmaXhDbHN9LXNtYF06IHNpemUgPT09ICdzbWFsbCcsXG4gICAgICAgIFtgJHtwcmVmaXhDbHN9LWxnYF06IHNpemUgPT09ICdsYXJnZScsXG4gICAgICAgIFtgJHtwcmVmaXhDbHN9LWRpc2FibGVkYF06IGRpc2FibGVkLFxuICAgIH0pO1xufVxuY2xhc3MgSW5wdXQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgdGhpcy5zYXZlQ2xlYXJhYmxlSW5wdXQgPSAoaW5wdXQpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xlYXJhYmxlSW5wdXQgPSBpbnB1dDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zYXZlSW5wdXQgPSAoaW5wdXQpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5wdXQgPSBpbnB1dDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVSZXNldCA9IChlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFZhbHVlKCcnLCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5mb2N1cygpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXNvbHZlT25DaGFuZ2UodGhpcy5pbnB1dCwgZSwgdGhpcy5wcm9wcy5vbkNoYW5nZSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVySW5wdXQgPSAocHJlZml4Q2xzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGNsYXNzTmFtZSwgYWRkb25CZWZvcmUsIGFkZG9uQWZ0ZXIsIHNpemUsIGRpc2FibGVkIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgLy8gRml4IGh0dHBzOi8vZmIubWUvcmVhY3QtdW5rbm93bi1wcm9wXG4gICAgICAgICAgICBjb25zdCBvdGhlclByb3BzID0gb21pdCh0aGlzLnByb3BzLCBbXG4gICAgICAgICAgICAgICAgJ3ByZWZpeENscycsXG4gICAgICAgICAgICAgICAgJ29uUHJlc3NFbnRlcicsXG4gICAgICAgICAgICAgICAgJ2FkZG9uQmVmb3JlJyxcbiAgICAgICAgICAgICAgICAnYWRkb25BZnRlcicsXG4gICAgICAgICAgICAgICAgJ3ByZWZpeCcsXG4gICAgICAgICAgICAgICAgJ3N1ZmZpeCcsXG4gICAgICAgICAgICAgICAgJ2FsbG93Q2xlYXInLFxuICAgICAgICAgICAgICAgIC8vIElucHV0IGVsZW1lbnRzIG11c3QgYmUgZWl0aGVyIGNvbnRyb2xsZWQgb3IgdW5jb250cm9sbGVkLFxuICAgICAgICAgICAgICAgIC8vIHNwZWNpZnkgZWl0aGVyIHRoZSB2YWx1ZSBwcm9wLCBvciB0aGUgZGVmYXVsdFZhbHVlIHByb3AsIGJ1dCBub3QgYm90aC5cbiAgICAgICAgICAgICAgICAnZGVmYXVsdFZhbHVlJyxcbiAgICAgICAgICAgICAgICAnc2l6ZScsXG4gICAgICAgICAgICAgICAgJ2lucHV0VHlwZScsXG4gICAgICAgICAgICBdKTtcbiAgICAgICAgICAgIHJldHVybiAoPGlucHV0IHsuLi5vdGhlclByb3BzfSBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9IG9uS2V5RG93bj17dGhpcy5oYW5kbGVLZXlEb3dufSBjbGFzc05hbWU9e2NsYXNzTmFtZXMoZ2V0SW5wdXRDbGFzc05hbWUocHJlZml4Q2xzLCBzaXplLCBkaXNhYmxlZCksIHtcbiAgICAgICAgICAgICAgICBbY2xhc3NOYW1lXTogY2xhc3NOYW1lICYmICFhZGRvbkJlZm9yZSAmJiAhYWRkb25BZnRlcixcbiAgICAgICAgICAgIH0pfSByZWY9e3RoaXMuc2F2ZUlucHV0fS8+KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5jbGVhclBhc3N3b3JkVmFsdWVBdHRyaWJ1dGUgPSAoKSA9PiB7XG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8yMDU0MVxuICAgICAgICAgICAgdGhpcy5yZW1vdmVQYXNzd29yZFRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pbnB1dCAmJlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlucHV0LmdldEF0dHJpYnV0ZSgndHlwZScpID09PSAncGFzc3dvcmQnICYmXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5wdXQuaGFzQXR0cmlidXRlKCd2YWx1ZScpKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5wdXQucmVtb3ZlQXR0cmlidXRlKCd2YWx1ZScpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZUNoYW5nZSA9IChlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFZhbHVlKGUudGFyZ2V0LnZhbHVlLCB0aGlzLmNsZWFyUGFzc3dvcmRWYWx1ZUF0dHJpYnV0ZSk7XG4gICAgICAgICAgICByZXNvbHZlT25DaGFuZ2UodGhpcy5pbnB1dCwgZSwgdGhpcy5wcm9wcy5vbkNoYW5nZSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlS2V5RG93biA9IChlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uUHJlc3NFbnRlciwgb25LZXlEb3duIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGUua2V5Q29kZSA9PT0gMTMgJiYgb25QcmVzc0VudGVyKSB7XG4gICAgICAgICAgICAgICAgb25QcmVzc0VudGVyKGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKG9uS2V5RG93bikge1xuICAgICAgICAgICAgICAgIG9uS2V5RG93bihlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJDb21wb25lbnQgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyB2YWx1ZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2lucHV0JywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIHJldHVybiAoPENsZWFyYWJsZUxhYmVsZWRJbnB1dCB7Li4udGhpcy5wcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9IGlucHV0VHlwZT1cImlucHV0XCIgdmFsdWU9e2ZpeENvbnRyb2xsZWRWYWx1ZSh2YWx1ZSl9IGVsZW1lbnQ9e3RoaXMucmVuZGVySW5wdXQocHJlZml4Q2xzKX0gaGFuZGxlUmVzZXQ9e3RoaXMuaGFuZGxlUmVzZXR9IHJlZj17dGhpcy5zYXZlQ2xlYXJhYmxlSW5wdXR9Lz4pO1xuICAgICAgICB9O1xuICAgICAgICBjb25zdCB2YWx1ZSA9IHR5cGVvZiBwcm9wcy52YWx1ZSA9PT0gJ3VuZGVmaW5lZCcgPyBwcm9wcy5kZWZhdWx0VmFsdWUgOiBwcm9wcy52YWx1ZTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICB9O1xuICAgIH1cbiAgICBzdGF0aWMgZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKG5leHRQcm9wcykge1xuICAgICAgICBpZiAoJ3ZhbHVlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdmFsdWU6IG5leHRQcm9wcy52YWx1ZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICB0aGlzLmNsZWFyUGFzc3dvcmRWYWx1ZUF0dHJpYnV0ZSgpO1xuICAgIH1cbiAgICAvLyBTaW5jZSBwb2x5ZmlsbCBgZ2V0U25hcHNob3RCZWZvcmVVcGRhdGVgIG5lZWQgd29yayB3aXRoIGBjb21wb25lbnREaWRVcGRhdGVgLlxuICAgIC8vIFdlIGtlZXAgYW4gZW1wdHkgZnVuY3Rpb24gaGVyZS5cbiAgICBjb21wb25lbnREaWRVcGRhdGUoKSB7IH1cbiAgICBnZXRTbmFwc2hvdEJlZm9yZVVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgICAgaWYgKGhhc1ByZWZpeFN1ZmZpeChwcmV2UHJvcHMpICE9PSBoYXNQcmVmaXhTdWZmaXgodGhpcy5wcm9wcykpIHtcbiAgICAgICAgICAgIHdhcm5pbmcodGhpcy5pbnB1dCAhPT0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudCwgJ0lucHV0JywgYFdoZW4gSW5wdXQgaXMgZm9jdXNlZCwgZHluYW1pYyBhZGQgb3IgcmVtb3ZlIHByZWZpeCAvIHN1ZmZpeCB3aWxsIG1ha2UgaXQgbG9zZSBmb2N1cyBjYXVzZWQgYnkgZG9tIHN0cnVjdHVyZSBjaGFuZ2UuIFJlYWQgbW9yZTogaHR0cHM6Ly9hbnQuZGVzaWduL2NvbXBvbmVudHMvaW5wdXQvI0ZBUWApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgaWYgKHRoaXMucmVtb3ZlUGFzc3dvcmRUaW1lb3V0KSB7XG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5yZW1vdmVQYXNzd29yZFRpbWVvdXQpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGZvY3VzKCkge1xuICAgICAgICB0aGlzLmlucHV0LmZvY3VzKCk7XG4gICAgfVxuICAgIGJsdXIoKSB7XG4gICAgICAgIHRoaXMuaW5wdXQuYmx1cigpO1xuICAgIH1cbiAgICBzZWxlY3QoKSB7XG4gICAgICAgIHRoaXMuaW5wdXQuc2VsZWN0KCk7XG4gICAgfVxuICAgIHNldFZhbHVlKHZhbHVlLCBjYWxsYmFjaykge1xuICAgICAgICBpZiAoISgndmFsdWUnIGluIHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdmFsdWUgfSwgY2FsbGJhY2spO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJDb21wb25lbnR9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuSW5wdXQuZGVmYXVsdFByb3BzID0ge1xuICAgIHR5cGU6ICd0ZXh0Jyxcbn07XG5JbnB1dC5wcm9wVHlwZXMgPSB7XG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzaXplOiBQcm9wVHlwZXMub25lT2YoSW5wdXRTaXplcyksXG4gICAgbWF4TGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLmFueSxcbiAgICBkZWZhdWx0VmFsdWU6IFByb3BUeXBlcy5hbnksXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGFkZG9uQmVmb3JlOiBQcm9wVHlwZXMubm9kZSxcbiAgICBhZGRvbkFmdGVyOiBQcm9wVHlwZXMubm9kZSxcbiAgICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgb25QcmVzc0VudGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbktleURvd246IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uS2V5VXA6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uRm9jdXM6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uQmx1cjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgcHJlZml4OiBQcm9wVHlwZXMubm9kZSxcbiAgICBzdWZmaXg6IFByb3BUeXBlcy5ub2RlLFxuICAgIGFsbG93Q2xlYXI6IFByb3BUeXBlcy5ib29sLFxufTtcbnBvbHlmaWxsKElucHV0KTtcbmV4cG9ydCBkZWZhdWx0IElucHV0O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBS0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUpBO0FBQ0E7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFTQTtBQVRBO0FBY0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUZBO0FBakJBO0FBQ0E7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBTEE7QUFGQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQURBO0FBakVBO0FBb0VBO0FBQ0E7OztBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQUFBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBekNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTs7OztBQTdFQTtBQUNBO0FBZ0hBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUFxQkE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/Input.js
