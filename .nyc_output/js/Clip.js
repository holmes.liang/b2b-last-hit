var easingFuncs = __webpack_require__(/*! ./easing */ "./node_modules/zrender/lib/animation/easing.js");
/**
 * 动画主控制器
 * @config target 动画对象，可以是数组，如果是数组的话会批量分发onframe等事件
 * @config life(1000) 动画时长
 * @config delay(0) 动画延迟时间
 * @config loop(true)
 * @config gap(0) 循环的间隔时间
 * @config onframe
 * @config easing(optional)
 * @config ondestroy(optional)
 * @config onrestart(optional)
 *
 * TODO pause
 */


function Clip(options) {
  this._target = options.target; // 生命周期

  this._life = options.life || 1000; // 延时

  this._delay = options.delay || 0; // 开始时间
  // this._startTime = new Date().getTime() + this._delay;// 单位毫秒

  this._initialized = false; // 是否循环

  this.loop = options.loop == null ? false : options.loop;
  this.gap = options.gap || 0;
  this.easing = options.easing || 'Linear';
  this.onframe = options.onframe;
  this.ondestroy = options.ondestroy;
  this.onrestart = options.onrestart;
  this._pausedTime = 0;
  this._paused = false;
}

Clip.prototype = {
  constructor: Clip,
  step: function step(globalTime, deltaTime) {
    // Set startTime on first step, or _startTime may has milleseconds different between clips
    // PENDING
    if (!this._initialized) {
      this._startTime = globalTime + this._delay;
      this._initialized = true;
    }

    if (this._paused) {
      this._pausedTime += deltaTime;
      return;
    }

    var percent = (globalTime - this._startTime - this._pausedTime) / this._life; // 还没开始

    if (percent < 0) {
      return;
    }

    percent = Math.min(percent, 1);
    var easing = this.easing;
    var easingFunc = typeof easing === 'string' ? easingFuncs[easing] : easing;
    var schedule = typeof easingFunc === 'function' ? easingFunc(percent) : percent;
    this.fire('frame', schedule); // 结束

    if (percent === 1) {
      if (this.loop) {
        this.restart(globalTime); // 重新开始周期
        // 抛出而不是直接调用事件直到 stage.update 后再统一调用这些事件

        return 'restart';
      } // 动画完成将这个控制器标识为待删除
      // 在Animation.update中进行批量删除


      this._needsRemove = true;
      return 'destroy';
    }

    return null;
  },
  restart: function restart(globalTime) {
    var remainder = (globalTime - this._startTime - this._pausedTime) % this._life;
    this._startTime = globalTime - remainder + this.gap;
    this._pausedTime = 0;
    this._needsRemove = false;
  },
  fire: function fire(eventType, arg) {
    eventType = 'on' + eventType;

    if (this[eventType]) {
      this[eventType](this._target, arg);
    }
  },
  pause: function pause() {
    this._paused = true;
  },
  resume: function resume() {
    this._paused = false;
  }
};
var _default = Clip;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvYW5pbWF0aW9uL0NsaXAuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9hbmltYXRpb24vQ2xpcC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgZWFzaW5nRnVuY3MgPSByZXF1aXJlKFwiLi9lYXNpbmdcIik7XG5cbi8qKlxuICog5Yqo55S75Li75o6n5Yi25ZmoXG4gKiBAY29uZmlnIHRhcmdldCDliqjnlLvlr7nosaHvvIzlj6/ku6XmmK/mlbDnu4TvvIzlpoLmnpzmmK/mlbDnu4TnmoTor53kvJrmibnph4/liIblj5FvbmZyYW1l562J5LqL5Lu2XG4gKiBAY29uZmlnIGxpZmUoMTAwMCkg5Yqo55S75pe26ZW/XG4gKiBAY29uZmlnIGRlbGF5KDApIOWKqOeUu+W7tui/n+aXtumXtFxuICogQGNvbmZpZyBsb29wKHRydWUpXG4gKiBAY29uZmlnIGdhcCgwKSDlvqrnjq/nmoTpl7TpmpTml7bpl7RcbiAqIEBjb25maWcgb25mcmFtZVxuICogQGNvbmZpZyBlYXNpbmcob3B0aW9uYWwpXG4gKiBAY29uZmlnIG9uZGVzdHJveShvcHRpb25hbClcbiAqIEBjb25maWcgb25yZXN0YXJ0KG9wdGlvbmFsKVxuICpcbiAqIFRPRE8gcGF1c2VcbiAqL1xuZnVuY3Rpb24gQ2xpcChvcHRpb25zKSB7XG4gIHRoaXMuX3RhcmdldCA9IG9wdGlvbnMudGFyZ2V0OyAvLyDnlJ/lkb3lkajmnJ9cblxuICB0aGlzLl9saWZlID0gb3B0aW9ucy5saWZlIHx8IDEwMDA7IC8vIOW7tuaXtlxuXG4gIHRoaXMuX2RlbGF5ID0gb3B0aW9ucy5kZWxheSB8fCAwOyAvLyDlvIDlp4vml7bpl7RcbiAgLy8gdGhpcy5fc3RhcnRUaW1lID0gbmV3IERhdGUoKS5nZXRUaW1lKCkgKyB0aGlzLl9kZWxheTsvLyDljZXkvY3mr6vnp5JcblxuICB0aGlzLl9pbml0aWFsaXplZCA9IGZhbHNlOyAvLyDmmK/lkKblvqrnjq9cblxuICB0aGlzLmxvb3AgPSBvcHRpb25zLmxvb3AgPT0gbnVsbCA/IGZhbHNlIDogb3B0aW9ucy5sb29wO1xuICB0aGlzLmdhcCA9IG9wdGlvbnMuZ2FwIHx8IDA7XG4gIHRoaXMuZWFzaW5nID0gb3B0aW9ucy5lYXNpbmcgfHwgJ0xpbmVhcic7XG4gIHRoaXMub25mcmFtZSA9IG9wdGlvbnMub25mcmFtZTtcbiAgdGhpcy5vbmRlc3Ryb3kgPSBvcHRpb25zLm9uZGVzdHJveTtcbiAgdGhpcy5vbnJlc3RhcnQgPSBvcHRpb25zLm9ucmVzdGFydDtcbiAgdGhpcy5fcGF1c2VkVGltZSA9IDA7XG4gIHRoaXMuX3BhdXNlZCA9IGZhbHNlO1xufVxuXG5DbGlwLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IENsaXAsXG4gIHN0ZXA6IGZ1bmN0aW9uIChnbG9iYWxUaW1lLCBkZWx0YVRpbWUpIHtcbiAgICAvLyBTZXQgc3RhcnRUaW1lIG9uIGZpcnN0IHN0ZXAsIG9yIF9zdGFydFRpbWUgbWF5IGhhcyBtaWxsZXNlY29uZHMgZGlmZmVyZW50IGJldHdlZW4gY2xpcHNcbiAgICAvLyBQRU5ESU5HXG4gICAgaWYgKCF0aGlzLl9pbml0aWFsaXplZCkge1xuICAgICAgdGhpcy5fc3RhcnRUaW1lID0gZ2xvYmFsVGltZSArIHRoaXMuX2RlbGF5O1xuICAgICAgdGhpcy5faW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9wYXVzZWQpIHtcbiAgICAgIHRoaXMuX3BhdXNlZFRpbWUgKz0gZGVsdGFUaW1lO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBwZXJjZW50ID0gKGdsb2JhbFRpbWUgLSB0aGlzLl9zdGFydFRpbWUgLSB0aGlzLl9wYXVzZWRUaW1lKSAvIHRoaXMuX2xpZmU7IC8vIOi/mOayoeW8gOWni1xuXG4gICAgaWYgKHBlcmNlbnQgPCAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgcGVyY2VudCA9IE1hdGgubWluKHBlcmNlbnQsIDEpO1xuICAgIHZhciBlYXNpbmcgPSB0aGlzLmVhc2luZztcbiAgICB2YXIgZWFzaW5nRnVuYyA9IHR5cGVvZiBlYXNpbmcgPT09ICdzdHJpbmcnID8gZWFzaW5nRnVuY3NbZWFzaW5nXSA6IGVhc2luZztcbiAgICB2YXIgc2NoZWR1bGUgPSB0eXBlb2YgZWFzaW5nRnVuYyA9PT0gJ2Z1bmN0aW9uJyA/IGVhc2luZ0Z1bmMocGVyY2VudCkgOiBwZXJjZW50O1xuICAgIHRoaXMuZmlyZSgnZnJhbWUnLCBzY2hlZHVsZSk7IC8vIOe7k+adn1xuXG4gICAgaWYgKHBlcmNlbnQgPT09IDEpIHtcbiAgICAgIGlmICh0aGlzLmxvb3ApIHtcbiAgICAgICAgdGhpcy5yZXN0YXJ0KGdsb2JhbFRpbWUpOyAvLyDph43mlrDlvIDlp4vlkajmnJ9cbiAgICAgICAgLy8g5oqb5Ye66ICM5LiN5piv55u05o6l6LCD55So5LqL5Lu255u05YiwIHN0YWdlLnVwZGF0ZSDlkI7lho3nu5/kuIDosIPnlKjov5nkupvkuovku7ZcblxuICAgICAgICByZXR1cm4gJ3Jlc3RhcnQnO1xuICAgICAgfSAvLyDliqjnlLvlrozmiJDlsIbov5nkuKrmjqfliLblmajmoIfor4bkuLrlvoXliKDpmaRcbiAgICAgIC8vIOWcqEFuaW1hdGlvbi51cGRhdGXkuK3ov5vooYzmibnph4/liKDpmaRcblxuXG4gICAgICB0aGlzLl9uZWVkc1JlbW92ZSA9IHRydWU7XG4gICAgICByZXR1cm4gJ2Rlc3Ryb3knO1xuICAgIH1cblxuICAgIHJldHVybiBudWxsO1xuICB9LFxuICByZXN0YXJ0OiBmdW5jdGlvbiAoZ2xvYmFsVGltZSkge1xuICAgIHZhciByZW1haW5kZXIgPSAoZ2xvYmFsVGltZSAtIHRoaXMuX3N0YXJ0VGltZSAtIHRoaXMuX3BhdXNlZFRpbWUpICUgdGhpcy5fbGlmZTtcbiAgICB0aGlzLl9zdGFydFRpbWUgPSBnbG9iYWxUaW1lIC0gcmVtYWluZGVyICsgdGhpcy5nYXA7XG4gICAgdGhpcy5fcGF1c2VkVGltZSA9IDA7XG4gICAgdGhpcy5fbmVlZHNSZW1vdmUgPSBmYWxzZTtcbiAgfSxcbiAgZmlyZTogZnVuY3Rpb24gKGV2ZW50VHlwZSwgYXJnKSB7XG4gICAgZXZlbnRUeXBlID0gJ29uJyArIGV2ZW50VHlwZTtcblxuICAgIGlmICh0aGlzW2V2ZW50VHlwZV0pIHtcbiAgICAgIHRoaXNbZXZlbnRUeXBlXSh0aGlzLl90YXJnZXQsIGFyZyk7XG4gICAgfVxuICB9LFxuICBwYXVzZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3BhdXNlZCA9IHRydWU7XG4gIH0sXG4gIHJlc3VtZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3BhdXNlZCA9IGZhbHNlO1xuICB9XG59O1xudmFyIF9kZWZhdWx0ID0gQ2xpcDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE3REE7QUErREE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/animation/Clip.js
