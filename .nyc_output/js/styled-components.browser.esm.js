__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createGlobalStyle", function() { return createGlobalStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "css", function() { return css; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isStyledComponent", function() { return isStyledComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "keyframes", function() { return keyframes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerStyleSheet", function() { return ServerStyleSheet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetConsumer", function() { return StyleSheetConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetContext", function() { return StyleSheetContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StyleSheetManager", function() { return StyleSheetManager; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeConsumer", function() { return ThemeConsumer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeContext", function() { return ThemeContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeProvider", function() { return ThemeProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withTheme", function() { return withTheme; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS", function() { return __DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS; });
/* harmony import */ var stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! stylis/stylis.min */ "./node_modules/stylis/stylis.min.js");
/* harmony import */ var stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stylis-rule-sheet */ "./node_modules/stylis-rule-sheet/index.js");
/* harmony import */ var stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _emotion_unitless__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @emotion/unitless */ "./node_modules/@emotion/unitless/dist/unitless.browser.esm.js");
/* harmony import */ var react_is__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-is */ "./node_modules/react-is/index.js");
/* harmony import */ var react_is__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_is__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var memoize_one__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! memoize-one */ "./node_modules/memoize-one/dist/memoize-one.esm.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _emotion_is_prop_valid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @emotion/is-prop-valid */ "./node_modules/@emotion/is-prop-valid/dist/is-prop-valid.browser.esm.js");
/* harmony import */ var merge_anything__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! merge-anything */ "./node_modules/merge-anything/dist/index.esm.js");








 // 

var interleave = function interleave(strings, interpolations) {
  var result = [strings[0]];

  for (var i = 0, len = interpolations.length; i < len; i += 1) {
    result.push(interpolations[i], strings[i + 1]);
  }

  return result;
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var classCallCheck = function classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var objectWithoutProperties = function objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
};

var possibleConstructorReturn = function possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}; // 


var isPlainObject = function isPlainObject(x) {
  return (typeof x === 'undefined' ? 'undefined' : _typeof(x)) === 'object' && x.constructor === Object;
}; // 


var EMPTY_ARRAY = Object.freeze([]);
var EMPTY_OBJECT = Object.freeze({}); // 

function isFunction(test) {
  return typeof test === 'function';
} // 


function getComponentName(target) {
  return ( true ? typeof target === 'string' && target : undefined) || target.displayName || target.name || 'Component';
} // 


function isStatelessFunction(test) {
  return typeof test === 'function' && !(test.prototype && test.prototype.isReactComponent);
} // 


function isStyledComponent(target) {
  return target && typeof target.styledComponentId === 'string';
} // 


var SC_ATTR = typeof process !== 'undefined' && (Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_SC_ATTR || Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).SC_ATTR) || 'data-styled';
var SC_VERSION_ATTR = 'data-styled-version';
var SC_STREAM_ATTR = 'data-styled-streamed';
var IS_BROWSER = typeof window !== 'undefined' && 'HTMLElement' in window;
var DISABLE_SPEEDY = typeof SC_DISABLE_SPEEDY === 'boolean' && SC_DISABLE_SPEEDY || typeof process !== 'undefined' && (Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_SC_DISABLE_SPEEDY || Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).SC_DISABLE_SPEEDY) || "development" !== 'production'; // Shared empty execution context when generating static styles

var STATIC_EXECUTION_CONTEXT = {}; // 

/**
 * Parse errors.md and turn it into a simple hash of code: message
 */

var ERRORS =  true ? {
  "1": "Cannot create styled-component for component: %s.\n\n",
  "2": "Can't collect styles once you've consumed a `ServerStyleSheet`'s styles! `ServerStyleSheet` is a one off instance for each server-side render cycle.\n\n- Are you trying to reuse it across renders?\n- Are you accidentally calling collectStyles twice?\n\n",
  "3": "Streaming SSR is only supported in a Node.js environment; Please do not try to call this method in the browser.\n\n",
  "4": "The `StyleSheetManager` expects a valid target or sheet prop!\n\n- Does this error occur on the client and is your target falsy?\n- Does this error occur on the server and is the sheet falsy?\n\n",
  "5": "The clone method cannot be used on the client!\n\n- Are you running in a client-like environment on the server?\n- Are you trying to run SSR on the client?\n\n",
  "6": "Trying to insert a new style tag, but the given Node is unmounted!\n\n- Are you using a custom target that isn't mounted?\n- Does your document not have a valid head element?\n- Have you accidentally removed a style tag manually?\n\n",
  "7": "ThemeProvider: Please return an object from your \"theme\" prop function, e.g.\n\n```js\ntheme={() => ({})}\n```\n\n",
  "8": "ThemeProvider: Please make your \"theme\" prop an object.\n\n",
  "9": "Missing document `<head>`\n\n",
  "10": "Cannot find a StyleSheet instance. Usually this happens if there are multiple copies of styled-components loaded at once. Check out this issue for how to troubleshoot and fix the common cases where this situation can happen: https://github.com/styled-components/styled-components/issues/1941#issuecomment-417862021\n\n",
  "11": "_This error was replaced with a dev-time warning, it will be deleted for v4 final._ [createGlobalStyle] received children which will not be rendered. Please use the component without passing children elements.\n\n",
  "12": "It seems you are interpolating a keyframe declaration (%s) into an untagged string. This was supported in styled-components v3, but is not longer supported in v4 as keyframes are now injected on-demand. Please wrap your string in the css\\`\\` helper which ensures the styles are injected correctly. See https://www.styled-components.com/docs/api#css\n\n",
  "13": "%s is not a styled component and cannot be referred to via component selector. See https://www.styled-components.com/docs/advanced#referring-to-other-components for more details.\n"
} : undefined;
/**
 * super basic version of sprintf
 */

function format() {
  var a = arguments.length <= 0 ? undefined : arguments[0];
  var b = [];

  for (var c = 1, len = arguments.length; c < len; c += 1) {
    b.push(arguments.length <= c ? undefined : arguments[c]);
  }

  b.forEach(function (d) {
    a = a.replace(/%[a-z]/, d);
  });
  return a;
}
/**
 * Create an error file out of errors.md for development and a simple web link to the full errors
 * in production mode.
 */


var StyledComponentsError = function (_Error) {
  inherits(StyledComponentsError, _Error);

  function StyledComponentsError(code) {
    classCallCheck(this, StyledComponentsError);

    for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      interpolations[_key - 1] = arguments[_key];
    }

    if (false) { var _this; } else {
      var _this = possibleConstructorReturn(this, _Error.call(this, format.apply(undefined, [ERRORS[code]].concat(interpolations)).trim()));
    }

    return possibleConstructorReturn(_this);
  }

  return StyledComponentsError;
}(Error); // 


var SC_COMPONENT_ID = /^[^\S\n]*?\/\* sc-component-id:\s*(\S+)\s+\*\//gm;

var extractComps = function extractComps(maybeCSS) {
  var css = '' + (maybeCSS || ''); // Definitely a string, and a clone

  var existingComponents = [];
  css.replace(SC_COMPONENT_ID, function (match, componentId, matchIndex) {
    existingComponents.push({
      componentId: componentId,
      matchIndex: matchIndex
    });
    return match;
  });
  return existingComponents.map(function (_ref, i) {
    var componentId = _ref.componentId,
        matchIndex = _ref.matchIndex;
    var nextComp = existingComponents[i + 1];
    var cssFromDOM = nextComp ? css.slice(matchIndex, nextComp.matchIndex) : css.slice(matchIndex);
    return {
      componentId: componentId,
      cssFromDOM: cssFromDOM
    };
  });
}; // 


var COMMENT_REGEX = /^\s*\/\/.*$/gm; // NOTE: This stylis instance is only used to split rules from SSR'd style tags

var stylisSplitter = new stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default.a({
  global: false,
  cascade: true,
  keyframe: false,
  prefix: false,
  compress: false,
  semicolon: true
});
var stylis = new stylis_stylis_min__WEBPACK_IMPORTED_MODULE_0___default.a({
  global: false,
  cascade: true,
  keyframe: false,
  prefix: true,
  compress: false,
  semicolon: false // NOTE: This means "autocomplete missing semicolons"

}); // Wrap `insertRulePlugin to build a list of rules,
// and then make our own plugin to return the rules. This
// makes it easier to hook into the existing SSR architecture

var parsingRules = []; // eslint-disable-next-line consistent-return

var returnRulesPlugin = function returnRulesPlugin(context) {
  if (context === -2) {
    var parsedRules = parsingRules;
    parsingRules = [];
    return parsedRules;
  }
};

var parseRulesPlugin = stylis_rule_sheet__WEBPACK_IMPORTED_MODULE_1___default()(function (rule) {
  parsingRules.push(rule);
});

var _componentId = void 0;

var _selector = void 0;

var _selectorRegexp = void 0;

var selfReferenceReplacer = function selfReferenceReplacer(match, offset, string) {
  if ( // the first self-ref is always untouched
  offset > 0 && // there should be at least two self-refs to do a replacement (.b > .b)
  string.slice(0, offset).indexOf(_selector) !== -1 && // no consecutive self refs (.b.b); that is a precedence boost and treated differently
  string.slice(offset - _selector.length, offset) !== _selector) {
    return '.' + _componentId;
  }

  return match;
};
/**
 * When writing a style like
 *
 * & + & {
 *   color: red;
 * }
 *
 * The second ampersand should be a reference to the static component class. stylis
 * has no knowledge of static class so we have to intelligently replace the base selector.
 */


var selfReferenceReplacementPlugin = function selfReferenceReplacementPlugin(context, _, selectors) {
  if (context === 2 && selectors.length && selectors[0].lastIndexOf(_selector) > 0) {
    // eslint-disable-next-line no-param-reassign
    selectors[0] = selectors[0].replace(_selectorRegexp, selfReferenceReplacer);
  }
};

stylis.use([selfReferenceReplacementPlugin, parseRulesPlugin, returnRulesPlugin]);
stylisSplitter.use([parseRulesPlugin, returnRulesPlugin]);

var splitByRules = function splitByRules(css) {
  return stylisSplitter('', css);
};

function stringifyRules(rules, selector, prefix) {
  var componentId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '&';
  var flatCSS = rules.join('').replace(COMMENT_REGEX, ''); // replace JS comments

  var cssStr = selector && prefix ? prefix + ' ' + selector + ' { ' + flatCSS + ' }' : flatCSS; // stylis has no concept of state to be passed to plugins
  // but since JS is single=threaded, we can rely on that to ensure
  // these properties stay in sync with the current stylis run

  _componentId = componentId;
  _selector = selector;
  _selectorRegexp = new RegExp('\\' + _selector + '\\b', 'g');
  return stylis(prefix || !selector ? '' : selector, cssStr);
} // 

/* eslint-disable camelcase, no-undef */


var getNonce = function getNonce() {
  return  true ? __webpack_require__.nc : undefined;
}; // 

/* These are helpers for the StyleTags to keep track of the injected
 * rule names for each (component) ID that they're keeping track of.
 * They're crucial for detecting whether a name has already been
 * injected.
 * (This excludes rehydrated names) */

/* adds a new ID:name pairing to a names dictionary */


var addNameForId = function addNameForId(names, id, name) {
  if (name) {
    // eslint-disable-next-line no-param-reassign
    var namesForId = names[id] || (names[id] = Object.create(null));
    namesForId[name] = true;
  }
};
/* resets an ID entirely by overwriting it in the dictionary */


var resetIdNames = function resetIdNames(names, id) {
  // eslint-disable-next-line no-param-reassign
  names[id] = Object.create(null);
};
/* factory for a names dictionary checking the existance of an ID:name pairing */


var hasNameForId = function hasNameForId(names) {
  return function (id, name) {
    return names[id] !== undefined && names[id][name];
  };
};
/* stringifies names for the html/element output */


var stringifyNames = function stringifyNames(names) {
  var str = ''; // eslint-disable-next-line guard-for-in

  for (var id in names) {
    str += Object.keys(names[id]).join(' ') + ' ';
  }

  return str.trim();
};
/* clones the nested names dictionary */


var cloneNames = function cloneNames(names) {
  var clone = Object.create(null); // eslint-disable-next-line guard-for-in

  for (var id in names) {
    clone[id] = _extends({}, names[id]);
  }

  return clone;
}; // 

/* These are helpers that deal with the insertRule (aka speedy) API
 * They are used in the StyleTags and specifically the speedy tag
 */

/* retrieve a sheet for a given style tag */


var sheetForTag = function sheetForTag(tag) {
  // $FlowFixMe
  if (tag.sheet) return tag.sheet;
  /* Firefox quirk requires us to step through all stylesheets to find one owned by the given tag */

  var size = tag.ownerDocument.styleSheets.length;

  for (var i = 0; i < size; i += 1) {
    var sheet = tag.ownerDocument.styleSheets[i]; // $FlowFixMe

    if (sheet.ownerNode === tag) return sheet;
  }
  /* we should always be able to find a tag */


  throw new StyledComponentsError(10);
};
/* insert a rule safely and return whether it was actually injected */


var safeInsertRule = function safeInsertRule(sheet, cssRule, index) {
  /* abort early if cssRule string is falsy */
  if (!cssRule) return false;
  var maxIndex = sheet.cssRules.length;

  try {
    /* use insertRule and cap passed index with maxIndex (no of cssRules) */
    sheet.insertRule(cssRule, index <= maxIndex ? index : maxIndex);
  } catch (err) {
    /* any error indicates an invalid rule */
    return false;
  }

  return true;
};
/* deletes `size` rules starting from `removalIndex` */


var deleteRules = function deleteRules(sheet, removalIndex, size) {
  var lowerBound = removalIndex - size;

  for (var i = removalIndex; i > lowerBound; i -= 1) {
    sheet.deleteRule(i);
  }
}; // 

/* this marker separates component styles and is important for rehydration */


var makeTextMarker = function makeTextMarker(id) {
  return '\n/* sc-component-id: ' + id + ' */\n';
};
/* add up all numbers in array up until and including the index */


var addUpUntilIndex = function addUpUntilIndex(sizes, index) {
  var totalUpToIndex = 0;

  for (var i = 0; i <= index; i += 1) {
    totalUpToIndex += sizes[i];
  }

  return totalUpToIndex;
};
/* create a new style tag after lastEl */


var makeStyleTag = function makeStyleTag(target, tagEl, insertBefore) {
  var targetDocument = document;
  if (target) targetDocument = target.ownerDocument;else if (tagEl) targetDocument = tagEl.ownerDocument;
  var el = targetDocument.createElement('style');
  el.setAttribute(SC_ATTR, '');
  el.setAttribute(SC_VERSION_ATTR, "4.4.1");
  var nonce = getNonce();

  if (nonce) {
    el.setAttribute('nonce', nonce);
  }
  /* Work around insertRule quirk in EdgeHTML */


  el.appendChild(targetDocument.createTextNode(''));

  if (target && !tagEl) {
    /* Append to target when no previous element was passed */
    target.appendChild(el);
  } else {
    if (!tagEl || !target || !tagEl.parentNode) {
      throw new StyledComponentsError(6);
    }
    /* Insert new style tag after the previous one */


    tagEl.parentNode.insertBefore(el, insertBefore ? tagEl : tagEl.nextSibling);
  }

  return el;
};
/* takes a css factory function and outputs an html styled tag factory */


var wrapAsHtmlTag = function wrapAsHtmlTag(css, names) {
  return function (additionalAttrs) {
    var nonce = getNonce();
    var attrs = [nonce && 'nonce="' + nonce + '"', SC_ATTR + '="' + stringifyNames(names) + '"', SC_VERSION_ATTR + '="' + "4.4.1" + '"', additionalAttrs];
    var htmlAttr = attrs.filter(Boolean).join(' ');
    return '<style ' + htmlAttr + '>' + css() + '</style>';
  };
};
/* takes a css factory function and outputs an element factory */


var wrapAsElement = function wrapAsElement(css, names) {
  return function () {
    var _props;

    var props = (_props = {}, _props[SC_ATTR] = stringifyNames(names), _props[SC_VERSION_ATTR] = "4.4.1", _props);
    var nonce = getNonce();

    if (nonce) {
      // $FlowFixMe
      props.nonce = nonce;
    } // eslint-disable-next-line react/no-danger


    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('style', _extends({}, props, {
      dangerouslySetInnerHTML: {
        __html: css()
      }
    }));
  };
};

var getIdsFromMarkersFactory = function getIdsFromMarkersFactory(markers) {
  return function () {
    return Object.keys(markers);
  };
};
/* speedy tags utilise insertRule */


var makeSpeedyTag = function makeSpeedyTag(el, getImportRuleTag) {
  var names = Object.create(null);
  var markers = Object.create(null);
  var sizes = [];
  var extractImport = getImportRuleTag !== undefined;
  /* indicates whether getImportRuleTag was called */

  var usedImportRuleTag = false;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];

    if (prev !== undefined) {
      return prev;
    }

    markers[id] = sizes.length;
    sizes.push(0);
    resetIdNames(names, id);
    return markers[id];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    var sheet = sheetForTag(el);
    var insertIndex = addUpUntilIndex(sizes, marker);
    var injectedRules = 0;
    var importRules = [];
    var cssRulesSize = cssRules.length;

    for (var i = 0; i < cssRulesSize; i += 1) {
      var cssRule = cssRules[i];
      var mayHaveImport = extractImport;
      /* @import rules are reordered to appear first */

      if (mayHaveImport && cssRule.indexOf('@import') !== -1) {
        importRules.push(cssRule);
      } else if (safeInsertRule(sheet, cssRule, insertIndex + injectedRules)) {
        mayHaveImport = false;
        injectedRules += 1;
      }
    }

    if (extractImport && importRules.length > 0) {
      usedImportRuleTag = true; // $FlowFixMe

      getImportRuleTag().insertRules(id + '-import', importRules);
    }

    sizes[marker] += injectedRules;
    /* add up no of injected rules */

    addNameForId(names, id, name);
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return; // $FlowFixMe

    if (el.isConnected === false) return;
    var size = sizes[marker];
    var sheet = sheetForTag(el);
    var removalIndex = addUpUntilIndex(sizes, marker) - 1;
    deleteRules(sheet, removalIndex, size);
    sizes[marker] = 0;
    resetIdNames(names, id);

    if (extractImport && usedImportRuleTag) {
      // $FlowFixMe
      getImportRuleTag().removeRules(id + '-import');
    }
  };

  var css = function css() {
    var _sheetForTag = sheetForTag(el),
        cssRules = _sheetForTag.cssRules;

    var str = ''; // eslint-disable-next-line guard-for-in

    for (var id in markers) {
      str += makeTextMarker(id);
      var marker = markers[id];
      var end = addUpUntilIndex(sizes, marker);
      var size = sizes[marker];

      for (var i = end - size; i < end; i += 1) {
        var rule = cssRules[i];

        if (rule !== undefined) {
          str += rule.cssText;
        }
      }
    }

    return str;
  };

  return {
    clone: function clone() {
      throw new StyledComponentsError(5);
    },
    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: el,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };
};

var makeTextNode = function makeTextNode(targetDocument, id) {
  return targetDocument.createTextNode(makeTextMarker(id));
};

var makeBrowserTag = function makeBrowserTag(el, getImportRuleTag) {
  var names = Object.create(null);
  var markers = Object.create(null);
  var extractImport = getImportRuleTag !== undefined;
  /* indicates whether getImportRuleTag was called */

  var usedImportRuleTag = false;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];

    if (prev !== undefined) {
      return prev;
    }

    markers[id] = makeTextNode(el.ownerDocument, id);
    el.appendChild(markers[id]);
    names[id] = Object.create(null);
    return markers[id];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    var importRules = [];
    var cssRulesSize = cssRules.length;

    for (var i = 0; i < cssRulesSize; i += 1) {
      var rule = cssRules[i];
      var mayHaveImport = extractImport;

      if (mayHaveImport && rule.indexOf('@import') !== -1) {
        importRules.push(rule);
      } else {
        mayHaveImport = false;
        var separator = i === cssRulesSize - 1 ? '' : ' ';
        marker.appendData('' + rule + separator);
      }
    }

    addNameForId(names, id, name);

    if (extractImport && importRules.length > 0) {
      usedImportRuleTag = true; // $FlowFixMe

      getImportRuleTag().insertRules(id + '-import', importRules);
    }
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return;
    /* create new empty text node and replace the current one */

    var newMarker = makeTextNode(el.ownerDocument, id);
    el.replaceChild(newMarker, marker);
    markers[id] = newMarker;
    resetIdNames(names, id);

    if (extractImport && usedImportRuleTag) {
      // $FlowFixMe
      getImportRuleTag().removeRules(id + '-import');
    }
  };

  var css = function css() {
    var str = ''; // eslint-disable-next-line guard-for-in

    for (var id in markers) {
      str += markers[id].data;
    }

    return str;
  };

  return {
    clone: function clone() {
      throw new StyledComponentsError(5);
    },
    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: el,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };
};

var makeServerTag = function makeServerTag(namesArg, markersArg) {
  var names = namesArg === undefined ? Object.create(null) : namesArg;
  var markers = markersArg === undefined ? Object.create(null) : markersArg;

  var insertMarker = function insertMarker(id) {
    var prev = markers[id];

    if (prev !== undefined) {
      return prev;
    }

    return markers[id] = [''];
  };

  var insertRules = function insertRules(id, cssRules, name) {
    var marker = insertMarker(id);
    marker[0] += cssRules.join(' ');
    addNameForId(names, id, name);
  };

  var removeRules = function removeRules(id) {
    var marker = markers[id];
    if (marker === undefined) return;
    marker[0] = '';
    resetIdNames(names, id);
  };

  var css = function css() {
    var str = ''; // eslint-disable-next-line guard-for-in

    for (var id in markers) {
      var cssForId = markers[id][0];

      if (cssForId) {
        str += makeTextMarker(id) + cssForId;
      }
    }

    return str;
  };

  var clone = function clone() {
    var namesClone = cloneNames(names);
    var markersClone = Object.create(null); // eslint-disable-next-line guard-for-in

    for (var id in markers) {
      markersClone[id] = [markers[id][0]];
    }

    return makeServerTag(namesClone, markersClone);
  };

  var tag = {
    clone: clone,
    css: css,
    getIds: getIdsFromMarkersFactory(markers),
    hasNameForId: hasNameForId(names),
    insertMarker: insertMarker,
    insertRules: insertRules,
    removeRules: removeRules,
    sealed: false,
    styleTag: null,
    toElement: wrapAsElement(css, names),
    toHTML: wrapAsHtmlTag(css, names)
  };
  return tag;
};

var makeTag = function makeTag(target, tagEl, forceServer, insertBefore, getImportRuleTag) {
  if (IS_BROWSER && !forceServer) {
    var el = makeStyleTag(target, tagEl, insertBefore);

    if (DISABLE_SPEEDY) {
      return makeBrowserTag(el, getImportRuleTag);
    } else {
      return makeSpeedyTag(el, getImportRuleTag);
    }
  }

  return makeServerTag();
};

var rehydrate = function rehydrate(tag, els, extracted) {
  /* add all extracted components to the new tag */
  for (var i = 0, len = extracted.length; i < len; i += 1) {
    var _extracted$i = extracted[i],
        componentId = _extracted$i.componentId,
        cssFromDOM = _extracted$i.cssFromDOM;
    var cssRules = splitByRules(cssFromDOM);
    tag.insertRules(componentId, cssRules);
  }
  /* remove old HTMLStyleElements, since they have been rehydrated */


  for (var _i = 0, _len = els.length; _i < _len; _i += 1) {
    var el = els[_i];

    if (el.parentNode) {
      el.parentNode.removeChild(el);
    }
  }
}; // 


var SPLIT_REGEX = /\s+/;
/* determine the maximum number of components before tags are sharded */

var MAX_SIZE = void 0;

if (IS_BROWSER) {
  /* in speedy mode we can keep a lot more rules in a sheet before a slowdown can be expected */
  MAX_SIZE = DISABLE_SPEEDY ? 40 : 1000;
} else {
  /* for servers we do not need to shard at all */
  MAX_SIZE = -1;
}

var sheetRunningId = 0;
var master = void 0;

var StyleSheet = function () {
  /* a map from ids to tags */

  /* deferred rules for a given id */

  /* this is used for not reinjecting rules via hasNameForId() */

  /* when rules for an id are removed using remove() we have to ignore rehydratedNames for it */

  /* a list of tags belonging to this StyleSheet */

  /* a tag for import rules */

  /* current capacity until a new tag must be created */

  /* children (aka clones) of this StyleSheet inheriting all and future injections */
  function StyleSheet() {
    var _this = this;

    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : IS_BROWSER ? document.head : null;
    var forceServer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    classCallCheck(this, StyleSheet);

    this.getImportRuleTag = function () {
      var importRuleTag = _this.importRuleTag;

      if (importRuleTag !== undefined) {
        return importRuleTag;
      }

      var firstTag = _this.tags[0];
      var insertBefore = true;
      return _this.importRuleTag = makeTag(_this.target, firstTag ? firstTag.styleTag : null, _this.forceServer, insertBefore);
    };

    sheetRunningId += 1;
    this.id = sheetRunningId;
    this.forceServer = forceServer;
    this.target = forceServer ? null : target;
    this.tagMap = {};
    this.deferred = {};
    this.rehydratedNames = {};
    this.ignoreRehydratedNames = {};
    this.tags = [];
    this.capacity = 1;
    this.clones = [];
  }
  /* rehydrate all SSR'd style tags */


  StyleSheet.prototype.rehydrate = function rehydrate$$1() {
    if (!IS_BROWSER || this.forceServer) return this;
    var els = [];
    var extracted = [];
    var isStreamed = false;
    /* retrieve all of our SSR style elements from the DOM */

    var nodes = document.querySelectorAll('style[' + SC_ATTR + '][' + SC_VERSION_ATTR + '="' + "4.4.1" + '"]');
    var nodesSize = nodes.length;
    /* abort rehydration if no previous style tags were found */

    if (!nodesSize) return this;

    for (var i = 0; i < nodesSize; i += 1) {
      var el = nodes[i];
      /* check if style tag is a streamed tag */

      if (!isStreamed) isStreamed = !!el.getAttribute(SC_STREAM_ATTR);
      /* retrieve all component names */

      var elNames = (el.getAttribute(SC_ATTR) || '').trim().split(SPLIT_REGEX);
      var elNamesSize = elNames.length;

      for (var j = 0, name; j < elNamesSize; j += 1) {
        name = elNames[j];
        /* add rehydrated name to sheet to avoid re-adding styles */

        this.rehydratedNames[name] = true;
      }
      /* extract all components and their CSS */


      extracted.push.apply(extracted, extractComps(el.textContent));
      /* store original HTMLStyleElement */

      els.push(el);
    }
    /* abort rehydration if nothing was extracted */


    var extractedSize = extracted.length;
    if (!extractedSize) return this;
    /* create a tag to be used for rehydration */

    var tag = this.makeTag(null);
    rehydrate(tag, els, extracted);
    /* reset capacity and adjust MAX_SIZE by the initial size of the rehydration */

    this.capacity = Math.max(1, MAX_SIZE - extractedSize);
    this.tags.push(tag);
    /* retrieve all component ids */

    for (var _j = 0; _j < extractedSize; _j += 1) {
      this.tagMap[extracted[_j].componentId] = tag;
    }

    return this;
  };
  /* retrieve a "master" instance of StyleSheet which is typically used when no other is available
   * The master StyleSheet is targeted by createGlobalStyle, keyframes, and components outside of any
    * StyleSheetManager's context */

  /* reset the internal "master" instance */


  StyleSheet.reset = function reset() {
    var forceServer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    master = new StyleSheet(undefined, forceServer).rehydrate();
  };
  /* adds "children" to the StyleSheet that inherit all of the parents' rules
   * while their own rules do not affect the parent */


  StyleSheet.prototype.clone = function clone() {
    var sheet = new StyleSheet(this.target, this.forceServer);
    /* add to clone array */

    this.clones.push(sheet);
    /* clone all tags */

    sheet.tags = this.tags.map(function (tag) {
      var ids = tag.getIds();
      var newTag = tag.clone();
      /* reconstruct tagMap */

      for (var i = 0; i < ids.length; i += 1) {
        sheet.tagMap[ids[i]] = newTag;
      }

      return newTag;
    });
    /* clone other maps */

    sheet.rehydratedNames = _extends({}, this.rehydratedNames);
    sheet.deferred = _extends({}, this.deferred);
    return sheet;
  };
  /* force StyleSheet to create a new tag on the next injection */


  StyleSheet.prototype.sealAllTags = function sealAllTags() {
    this.capacity = 1;
    this.tags.forEach(function (tag) {
      // eslint-disable-next-line no-param-reassign
      tag.sealed = true;
    });
  };

  StyleSheet.prototype.makeTag = function makeTag$$1(tag) {
    var lastEl = tag ? tag.styleTag : null;
    var insertBefore = false;
    return makeTag(this.target, lastEl, this.forceServer, insertBefore, this.getImportRuleTag);
  };
  /* get a tag for a given componentId, assign the componentId to one, or shard */


  StyleSheet.prototype.getTagForId = function getTagForId(id) {
    /* simply return a tag, when the componentId was already assigned one */
    var prev = this.tagMap[id];

    if (prev !== undefined && !prev.sealed) {
      return prev;
    }

    var tag = this.tags[this.tags.length - 1];
    /* shard (create a new tag) if the tag is exhausted (See MAX_SIZE) */

    this.capacity -= 1;

    if (this.capacity === 0) {
      this.capacity = MAX_SIZE;
      tag = this.makeTag(tag);
      this.tags.push(tag);
    }

    return this.tagMap[id] = tag;
  };
  /* mainly for createGlobalStyle to check for its id */


  StyleSheet.prototype.hasId = function hasId(id) {
    return this.tagMap[id] !== undefined;
  };
  /* caching layer checking id+name to already have a corresponding tag and injected rules */


  StyleSheet.prototype.hasNameForId = function hasNameForId(id, name) {
    /* exception for rehydrated names which are checked separately */
    if (this.ignoreRehydratedNames[id] === undefined && this.rehydratedNames[name]) {
      return true;
    }

    var tag = this.tagMap[id];
    return tag !== undefined && tag.hasNameForId(id, name);
  };
  /* registers a componentId and registers it on its tag */


  StyleSheet.prototype.deferredInject = function deferredInject(id, cssRules) {
    /* don't inject when the id is already registered */
    if (this.tagMap[id] !== undefined) return;
    var clones = this.clones;

    for (var i = 0; i < clones.length; i += 1) {
      clones[i].deferredInject(id, cssRules);
    }

    this.getTagForId(id).insertMarker(id);
    this.deferred[id] = cssRules;
  };
  /* injects rules for a given id with a name that will need to be cached */


  StyleSheet.prototype.inject = function inject(id, cssRules, name) {
    var clones = this.clones;

    for (var i = 0; i < clones.length; i += 1) {
      clones[i].inject(id, cssRules, name);
    }

    var tag = this.getTagForId(id);
    /* add deferred rules for component */

    if (this.deferred[id] !== undefined) {
      // Combine passed cssRules with previously deferred CSS rules
      // NOTE: We cannot mutate the deferred array itself as all clones
      // do the same (see clones[i].inject)
      var rules = this.deferred[id].concat(cssRules);
      tag.insertRules(id, rules, name);
      this.deferred[id] = undefined;
    } else {
      tag.insertRules(id, cssRules, name);
    }
  };
  /* removes all rules for a given id, which doesn't remove its marker but resets it */


  StyleSheet.prototype.remove = function remove(id) {
    var tag = this.tagMap[id];
    if (tag === undefined) return;
    var clones = this.clones;

    for (var i = 0; i < clones.length; i += 1) {
      clones[i].remove(id);
    }
    /* remove all rules from the tag */


    tag.removeRules(id);
    /* ignore possible rehydrated names */

    this.ignoreRehydratedNames[id] = true;
    /* delete possible deferred rules */

    this.deferred[id] = undefined;
  };

  StyleSheet.prototype.toHTML = function toHTML() {
    return this.tags.map(function (tag) {
      return tag.toHTML();
    }).join('');
  };

  StyleSheet.prototype.toReactElements = function toReactElements() {
    var id = this.id;
    return this.tags.map(function (tag, i) {
      var key = 'sc-' + id + '-' + i;
      return Object(react__WEBPACK_IMPORTED_MODULE_2__["cloneElement"])(tag.toElement(), {
        key: key
      });
    });
  };

  createClass(StyleSheet, null, [{
    key: 'master',
    get: function get$$1() {
      return master || (master = new StyleSheet().rehydrate());
    }
    /* NOTE: This is just for backwards-compatibility with jest-styled-components */

  }, {
    key: 'instance',
    get: function get$$1() {
      return StyleSheet.master;
    }
  }]);
  return StyleSheet;
}(); // 


var Keyframes = function () {
  function Keyframes(name, rules) {
    var _this = this;

    classCallCheck(this, Keyframes);

    this.inject = function (styleSheet) {
      if (!styleSheet.hasNameForId(_this.id, _this.name)) {
        styleSheet.inject(_this.id, _this.rules, _this.name);
      }
    };

    this.toString = function () {
      throw new StyledComponentsError(12, String(_this.name));
    };

    this.name = name;
    this.rules = rules;
    this.id = 'sc-keyframes-' + name;
  }

  Keyframes.prototype.getName = function getName() {
    return this.name;
  };

  return Keyframes;
}(); // 

/**
 * inlined version of
 * https://github.com/facebook/fbjs/blob/master/packages/fbjs/src/core/hyphenateStyleName.js
 */


var uppercasePattern = /([A-Z])/g;
var msPattern = /^ms-/;
/**
 * Hyphenates a camelcased CSS property name, for example:
 *
 *   > hyphenateStyleName('backgroundColor')
 *   < "background-color"
 *   > hyphenateStyleName('MozTransition')
 *   < "-moz-transition"
 *   > hyphenateStyleName('msTransition')
 *   < "-ms-transition"
 *
 * As Modernizr suggests (http://modernizr.com/docs/#prefixed), an `ms` prefix
 * is converted to `-ms-`.
 *
 * @param {string} string
 * @return {string}
 */

function hyphenateStyleName(string) {
  return string.replace(uppercasePattern, '-$1').toLowerCase().replace(msPattern, '-ms-');
} // 
// Taken from https://github.com/facebook/react/blob/b87aabdfe1b7461e7331abb3601d9e6bb27544bc/packages/react-dom/src/shared/dangerousStyleValue.js


function addUnitIfNeeded(name, value) {
  // https://github.com/amilajack/eslint-plugin-flowtype-errors/issues/133
  // $FlowFixMe
  if (value == null || typeof value === 'boolean' || value === '') {
    return '';
  }

  if (typeof value === 'number' && value !== 0 && !(name in _emotion_unitless__WEBPACK_IMPORTED_MODULE_3__["default"])) {
    return value + 'px'; // Presumes implicit 'px' suffix for unitless numbers
  }

  return String(value).trim();
} // 

/**
 * It's falsish not falsy because 0 is allowed.
 */


var isFalsish = function isFalsish(chunk) {
  return chunk === undefined || chunk === null || chunk === false || chunk === '';
};

var objToCssArray = function objToCssArray(obj, prevKey) {
  var rules = [];
  var keys = Object.keys(obj);
  keys.forEach(function (key) {
    if (!isFalsish(obj[key])) {
      if (isPlainObject(obj[key])) {
        rules.push.apply(rules, objToCssArray(obj[key], key));
        return rules;
      } else if (isFunction(obj[key])) {
        rules.push(hyphenateStyleName(key) + ':', obj[key], ';');
        return rules;
      }

      rules.push(hyphenateStyleName(key) + ': ' + addUnitIfNeeded(key, obj[key]) + ';');
    }

    return rules;
  });
  return prevKey ? [prevKey + ' {'].concat(rules, ['}']) : rules;
};

function flatten(chunk, executionContext, styleSheet) {
  if (Array.isArray(chunk)) {
    var ruleSet = [];

    for (var i = 0, len = chunk.length, result; i < len; i += 1) {
      result = flatten(chunk[i], executionContext, styleSheet);
      if (result === null) continue;else if (Array.isArray(result)) ruleSet.push.apply(ruleSet, result);else ruleSet.push(result);
    }

    return ruleSet;
  }

  if (isFalsish(chunk)) {
    return null;
  }
  /* Handle other components */


  if (isStyledComponent(chunk)) {
    return '.' + chunk.styledComponentId;
  }
  /* Either execute or defer the function */


  if (isFunction(chunk)) {
    if (isStatelessFunction(chunk) && executionContext) {
      var _result = chunk(executionContext);

      if ( true && Object(react_is__WEBPACK_IMPORTED_MODULE_4__["isElement"])(_result)) {
        // eslint-disable-next-line no-console
        console.warn(getComponentName(chunk) + ' is not a styled component and cannot be referred to via component selector. See https://www.styled-components.com/docs/advanced#referring-to-other-components for more details.');
      }

      return flatten(_result, executionContext, styleSheet);
    } else return chunk;
  }

  if (chunk instanceof Keyframes) {
    if (styleSheet) {
      chunk.inject(styleSheet);
      return chunk.getName();
    } else return chunk;
  }
  /* Handle objects */


  return isPlainObject(chunk) ? objToCssArray(chunk) : chunk.toString();
} // 


function css(styles) {
  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  if (isFunction(styles) || isPlainObject(styles)) {
    // $FlowFixMe
    return flatten(interleave(EMPTY_ARRAY, [styles].concat(interpolations)));
  } // $FlowFixMe


  return flatten(interleave(styles, interpolations));
} // 


function constructWithOptions(componentConstructor, tag) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : EMPTY_OBJECT;

  if (!Object(react_is__WEBPACK_IMPORTED_MODULE_4__["isValidElementType"])(tag)) {
    throw new StyledComponentsError(1, String(tag));
  }
  /* This is callable directly as a template function */
  // $FlowFixMe: Not typed to avoid destructuring arguments


  var templateFunction = function templateFunction() {
    return componentConstructor(tag, options, css.apply(undefined, arguments));
  };
  /* If config methods are called, wrap up a new template function and merge options */


  templateFunction.withConfig = function (config) {
    return constructWithOptions(componentConstructor, tag, _extends({}, options, config));
  };
  /* Modify/inject new props at runtime */


  templateFunction.attrs = function (attrs) {
    return constructWithOptions(componentConstructor, tag, _extends({}, options, {
      attrs: Array.prototype.concat(options.attrs, attrs).filter(Boolean)
    }));
  };

  return templateFunction;
} // 
// Source: https://github.com/garycourt/murmurhash-js/blob/master/murmurhash2_gc.js


function murmurhash(c) {
  for (var e = c.length | 0, a = e | 0, d = 0, b; e >= 4;) {
    b = c.charCodeAt(d) & 255 | (c.charCodeAt(++d) & 255) << 8 | (c.charCodeAt(++d) & 255) << 16 | (c.charCodeAt(++d) & 255) << 24, b = 1540483477 * (b & 65535) + ((1540483477 * (b >>> 16) & 65535) << 16), b ^= b >>> 24, b = 1540483477 * (b & 65535) + ((1540483477 * (b >>> 16) & 65535) << 16), a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16) ^ b, e -= 4, ++d;
  }

  switch (e) {
    case 3:
      a ^= (c.charCodeAt(d + 2) & 255) << 16;

    case 2:
      a ^= (c.charCodeAt(d + 1) & 255) << 8;

    case 1:
      a ^= c.charCodeAt(d) & 255, a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16);
  }

  a ^= a >>> 13;
  a = 1540483477 * (a & 65535) + ((1540483477 * (a >>> 16) & 65535) << 16);
  return (a ^ a >>> 15) >>> 0;
} // 

/* eslint-disable no-bitwise */

/* This is the "capacity" of our alphabet i.e. 2x26 for all letters plus their capitalised
 * counterparts */


var charsLength = 52;
/* start at 75 for 'a' until 'z' (25) and then start at 65 for capitalised letters */

var getAlphabeticChar = function getAlphabeticChar(code) {
  return String.fromCharCode(code + (code > 25 ? 39 : 97));
};
/* input a number, usually a hash and convert it to base-52 */


function generateAlphabeticName(code) {
  var name = '';
  var x = void 0;
  /* get a char and divide by alphabet-length */

  for (x = code; x > charsLength; x = Math.floor(x / charsLength)) {
    name = getAlphabeticChar(x % charsLength) + name;
  }

  return getAlphabeticChar(x % charsLength) + name;
} // 


function hasFunctionObjectKey(obj) {
  // eslint-disable-next-line guard-for-in, no-restricted-syntax
  for (var key in obj) {
    if (isFunction(obj[key])) {
      return true;
    }
  }

  return false;
}

function isStaticRules(rules, attrs) {
  for (var i = 0; i < rules.length; i += 1) {
    var rule = rules[i]; // recursive case

    if (Array.isArray(rule) && !isStaticRules(rule, attrs)) {
      return false;
    } else if (isFunction(rule) && !isStyledComponent(rule)) {
      // functions are allowed to be static if they're just being
      // used to get the classname of a nested styled component
      return false;
    }
  }

  if (attrs.some(function (x) {
    return isFunction(x) || hasFunctionObjectKey(x);
  })) return false;
  return true;
} // 

/* combines hashStr (murmurhash) and nameGenerator for convenience */


var hasher = function hasher(str) {
  return generateAlphabeticName(murmurhash(str));
};
/*
 ComponentStyle is all the CSS-specific stuff, not
 the React-specific stuff.
 */


var ComponentStyle = function () {
  function ComponentStyle(rules, attrs, componentId) {
    classCallCheck(this, ComponentStyle);
    this.rules = rules;
    this.isStatic =  false && false;
    this.componentId = componentId;

    if (!StyleSheet.master.hasId(componentId)) {
      StyleSheet.master.deferredInject(componentId, []);
    }
  }
  /*
   * Flattens a rule set into valid CSS
   * Hashes it, wraps the whole chunk in a .hash1234 {}
   * Returns the hash to be injected on render()
   * */


  ComponentStyle.prototype.generateAndInjectStyles = function generateAndInjectStyles(executionContext, styleSheet) {
    var isStatic = this.isStatic,
        componentId = this.componentId,
        lastClassName = this.lastClassName;

    if (IS_BROWSER && isStatic && typeof lastClassName === 'string' && styleSheet.hasNameForId(componentId, lastClassName)) {
      return lastClassName;
    }

    var flatCSS = flatten(this.rules, executionContext, styleSheet);
    var name = hasher(this.componentId + flatCSS.join(''));

    if (!styleSheet.hasNameForId(componentId, name)) {
      styleSheet.inject(this.componentId, stringifyRules(flatCSS, '.' + name, undefined, componentId), name);
    }

    this.lastClassName = name;
    return name;
  };

  ComponentStyle.generateName = function generateName(str) {
    return hasher(str);
  };

  return ComponentStyle;
}(); // 


var LIMIT = 200;

var createWarnTooManyClasses = function createWarnTooManyClasses(displayName) {
  var generatedClasses = {};
  var warningSeen = false;
  return function (className) {
    if (!warningSeen) {
      generatedClasses[className] = true;

      if (Object.keys(generatedClasses).length >= LIMIT) {
        // Unable to find latestRule in test environment.

        /* eslint-disable no-console, prefer-template */
        console.warn('Over ' + LIMIT + ' classes were generated for component ' + displayName + '. \n' + 'Consider using the attrs method, together with a style object for frequently changed styles.\n' + 'Example:\n' + '  const Component = styled.div.attrs(props => ({\n' + '    style: {\n' + '      background: props.background,\n' + '    },\n' + '  }))`width: 100%;`\n\n' + '  <Component />');
        warningSeen = true;
        generatedClasses = {};
      }
    }
  };
}; // 


var determineTheme = function determineTheme(props, fallbackTheme) {
  var defaultProps = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : EMPTY_OBJECT; // Props should take precedence over ThemeProvider, which should take precedence over
  // defaultProps, but React automatically puts defaultProps on props.

  /* eslint-disable react/prop-types, flowtype-errors/show-errors */

  var isDefaultTheme = defaultProps ? props.theme === defaultProps.theme : false;
  var theme = props.theme && !isDefaultTheme ? props.theme : fallbackTheme || defaultProps.theme;
  /* eslint-enable */

  return theme;
}; // 


var escapeRegex = /[[\].#*$><+~=|^:(),"'`-]+/g;
var dashesAtEnds = /(^-|-$)/g;
/**
 * TODO: Explore using CSS.escape when it becomes more available
 * in evergreen browsers.
 */

function escape(str) {
  return str // Replace all possible CSS selectors
  .replace(escapeRegex, '-') // Remove extraneous hyphens at the start and end
  .replace(dashesAtEnds, '');
} // 


function isTag(target) {
  return typeof target === 'string' && ( true ? target.charAt(0) === target.charAt(0).toLowerCase() : undefined);
} // 


function generateDisplayName(target) {
  // $FlowFixMe
  return isTag(target) ? 'styled.' + target : 'Styled(' + getComponentName(target) + ')';
}

var _TYPE_STATICS;

var REACT_STATICS = {
  childContextTypes: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDerivedStateFromProps: true,
  propTypes: true,
  type: true
};
var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true
};
var TYPE_STATICS = (_TYPE_STATICS = {}, _TYPE_STATICS[react_is__WEBPACK_IMPORTED_MODULE_4__["ForwardRef"]] = {
  $$typeof: true,
  render: true
}, _TYPE_STATICS);
var defineProperty$1 = Object.defineProperty,
    getOwnPropertyNames = Object.getOwnPropertyNames,
    _Object$getOwnPropert = Object.getOwnPropertySymbols,
    getOwnPropertySymbols = _Object$getOwnPropert === undefined ? function () {
  return [];
} : _Object$getOwnPropert,
    getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor,
    getPrototypeOf = Object.getPrototypeOf,
    objectPrototype = Object.prototype;
var arrayPrototype = Array.prototype;

function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components
    var inheritedComponent = getPrototypeOf(sourceComponent);

    if (inheritedComponent && inheritedComponent !== objectPrototype) {
      hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
    }

    var keys = arrayPrototype.concat(getOwnPropertyNames(sourceComponent), // $FlowFixMe
    getOwnPropertySymbols(sourceComponent));
    var targetStatics = TYPE_STATICS[targetComponent.$$typeof] || REACT_STATICS;
    var sourceStatics = TYPE_STATICS[sourceComponent.$$typeof] || REACT_STATICS;
    var i = keys.length;
    var descriptor = void 0;
    var key = void 0; // eslint-disable-next-line no-plusplus

    while (i--) {
      key = keys[i];

      if ( // $FlowFixMe
      !KNOWN_STATICS[key] && !(blacklist && blacklist[key]) && !(sourceStatics && sourceStatics[key]) && // $FlowFixMe
      !(targetStatics && targetStatics[key])) {
        descriptor = getOwnPropertyDescriptor(sourceComponent, key);

        if (descriptor) {
          try {
            // Avoid failures from read-only properties
            defineProperty$1(targetComponent, key, descriptor);
          } catch (e) {
            /* fail silently */
          }
        }
      }
    }

    return targetComponent;
  }

  return targetComponent;
} // 


function isDerivedReactComponent(fn) {
  return !!(fn && fn.prototype && fn.prototype.isReactComponent);
} // 
// Helper to call a given function, only once


var once = function once(cb) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      cb.apply(undefined, arguments);
    }
  };
}; // 


var ThemeContext = Object(react__WEBPACK_IMPORTED_MODULE_2__["createContext"])();
var ThemeConsumer = ThemeContext.Consumer;
/**
 * Provide a theme to an entire react component tree via context
 */

var ThemeProvider = function (_Component) {
  inherits(ThemeProvider, _Component);

  function ThemeProvider(props) {
    classCallCheck(this, ThemeProvider);

    var _this = possibleConstructorReturn(this, _Component.call(this, props));

    _this.getContext = Object(memoize_one__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.getContext.bind(_this));
    _this.renderInner = _this.renderInner.bind(_this);
    return _this;
  }

  ThemeProvider.prototype.render = function render() {
    if (!this.props.children) return null;
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ThemeContext.Consumer, null, this.renderInner);
  };

  ThemeProvider.prototype.renderInner = function renderInner(outerTheme) {
    var context = this.getContext(this.props.theme, outerTheme);
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ThemeContext.Provider, {
      value: context
    }, this.props.children);
  };
  /**
   * Get the theme from the props, supporting both (outerTheme) => {}
   * as well as object notation
   */


  ThemeProvider.prototype.getTheme = function getTheme(theme, outerTheme) {
    if (isFunction(theme)) {
      var mergedTheme = theme(outerTheme);

      if ( true && (mergedTheme === null || Array.isArray(mergedTheme) || (typeof mergedTheme === 'undefined' ? 'undefined' : _typeof(mergedTheme)) !== 'object')) {
        throw new StyledComponentsError(7);
      }

      return mergedTheme;
    }

    if (theme === null || Array.isArray(theme) || (typeof theme === 'undefined' ? 'undefined' : _typeof(theme)) !== 'object') {
      throw new StyledComponentsError(8);
    }

    return _extends({}, outerTheme, theme);
  };

  ThemeProvider.prototype.getContext = function getContext(theme, outerTheme) {
    return this.getTheme(theme, outerTheme);
  };

  return ThemeProvider;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]); // 


var CLOSING_TAG_R = /^\s*<\/[a-z]/i;

var ServerStyleSheet = function () {
  function ServerStyleSheet() {
    classCallCheck(this, ServerStyleSheet);
    /* The master sheet might be reset, so keep a reference here */

    this.masterSheet = StyleSheet.master;
    this.instance = this.masterSheet.clone();
    this.sealed = false;
  }
  /**
   * Mark the ServerStyleSheet as being fully emitted and manually GC it from the
   * StyleSheet singleton.
   */


  ServerStyleSheet.prototype.seal = function seal() {
    if (!this.sealed) {
      /* Remove sealed StyleSheets from the master sheet */
      var index = this.masterSheet.clones.indexOf(this.instance);
      this.masterSheet.clones.splice(index, 1);
      this.sealed = true;
    }
  };

  ServerStyleSheet.prototype.collectStyles = function collectStyles(children) {
    if (this.sealed) {
      throw new StyledComponentsError(2);
    }

    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(StyleSheetManager, {
      sheet: this.instance
    }, children);
  };

  ServerStyleSheet.prototype.getStyleTags = function getStyleTags() {
    this.seal();
    return this.instance.toHTML();
  };

  ServerStyleSheet.prototype.getStyleElement = function getStyleElement() {
    this.seal();
    return this.instance.toReactElements();
  };

  ServerStyleSheet.prototype.interleaveWithNodeStream = function interleaveWithNodeStream(readableStream) {
    var _this = this;

    {
      throw new StyledComponentsError(3);
    }
    /* the tag index keeps track of which tags have already been emitted */

    var instance = this.instance;
    var instanceTagIndex = 0;
    var streamAttr = SC_STREAM_ATTR + '="true"';
    var transformer = new stream.Transform({
      transform: function appendStyleChunks(chunk,
      /* encoding */
      _, callback) {
        var tags = instance.tags;
        var html = '';
        /* retrieve html for each new style tag */

        for (; instanceTagIndex < tags.length; instanceTagIndex += 1) {
          var tag = tags[instanceTagIndex];
          html += tag.toHTML(streamAttr);
        }
        /* force our StyleSheets to emit entirely new tags */


        instance.sealAllTags();
        var renderedHtml = chunk.toString();
        /* prepend style html to chunk, unless the start of the chunk is a closing tag in which case append right after that */

        if (CLOSING_TAG_R.test(renderedHtml)) {
          var endOfClosingTag = renderedHtml.indexOf('>');
          this.push(renderedHtml.slice(0, endOfClosingTag + 1) + html + renderedHtml.slice(endOfClosingTag + 1));
        } else this.push(html + renderedHtml);

        callback();
      }
    });
    readableStream.on('end', function () {
      return _this.seal();
    });
    readableStream.on('error', function (err) {
      _this.seal(); // forward the error to the transform stream


      transformer.emit('error', err);
    });
    return readableStream.pipe(transformer);
  };

  return ServerStyleSheet;
}(); // 


var StyleSheetContext = Object(react__WEBPACK_IMPORTED_MODULE_2__["createContext"])();
var StyleSheetConsumer = StyleSheetContext.Consumer;

var StyleSheetManager = function (_Component) {
  inherits(StyleSheetManager, _Component);

  function StyleSheetManager(props) {
    classCallCheck(this, StyleSheetManager);

    var _this = possibleConstructorReturn(this, _Component.call(this, props));

    _this.getContext = Object(memoize_one__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.getContext);
    return _this;
  }

  StyleSheetManager.prototype.getContext = function getContext(sheet, target) {
    if (sheet) {
      return sheet;
    } else if (target) {
      return new StyleSheet(target);
    } else {
      throw new StyledComponentsError(4);
    }
  };

  StyleSheetManager.prototype.render = function render() {
    var _props = this.props,
        children = _props.children,
        sheet = _props.sheet,
        target = _props.target;
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(StyleSheetContext.Provider, {
      value: this.getContext(sheet, target)
    },  true ? react__WEBPACK_IMPORTED_MODULE_2___default.a.Children.only(children) : undefined);
  };

  return StyleSheetManager;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

 true ? StyleSheetManager.propTypes = {
  sheet: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.instanceOf(StyleSheet), prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.instanceOf(ServerStyleSheet)]),
  target: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.shape({
    appendChild: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func.isRequired
  })
} : undefined; // 

var identifiers = {};
/* We depend on components having unique IDs */

function generateId(_ComponentStyle, _displayName, parentComponentId) {
  var displayName = typeof _displayName !== 'string' ? 'sc' : escape(_displayName);
  /**
   * This ensures uniqueness if two components happen to share
   * the same displayName.
   */

  var nr = (identifiers[displayName] || 0) + 1;
  identifiers[displayName] = nr;

  var componentId = displayName + '-' + _ComponentStyle.generateName(displayName + nr);

  return parentComponentId ? parentComponentId + '-' + componentId : componentId;
} // $FlowFixMe


var StyledComponent = function (_Component) {
  inherits(StyledComponent, _Component);

  function StyledComponent() {
    classCallCheck(this, StyledComponent);

    var _this = possibleConstructorReturn(this, _Component.call(this));

    _this.attrs = {};
    _this.renderOuter = _this.renderOuter.bind(_this);
    _this.renderInner = _this.renderInner.bind(_this);

    if (true) {
      _this.warnInnerRef = once(function (displayName) {
        return (// eslint-disable-next-line no-console
          console.warn('The "innerRef" API has been removed in styled-components v4 in favor of React 16 ref forwarding, use "ref" instead like a typical component. "innerRef" was detected on component "' + displayName + '".')
        );
      });
      _this.warnAttrsFnObjectKeyDeprecated = once(function (key, displayName) {
        return (// eslint-disable-next-line no-console
          console.warn('Functions as object-form attrs({}) keys are now deprecated and will be removed in a future version of styled-components. Switch to the new attrs(props => ({})) syntax instead for easier and more powerful composition. The attrs key in question is "' + key + '" on component "' + displayName + '".', '\n ' + new Error().stack)
        );
      });
      _this.warnNonStyledComponentAttrsObjectKey = once(function (key, displayName) {
        return (// eslint-disable-next-line no-console
          console.warn('It looks like you\'ve used a non styled-component as the value for the "' + key + '" prop in an object-form attrs constructor of "' + displayName + '".\n' + 'You should use the new function-form attrs constructor which avoids this issue: attrs(props => ({ yourStuff }))\n' + "To continue using the deprecated object syntax, you'll need to wrap your component prop in a function to make it available inside the styled component (you'll still get the deprecation warning though.)\n" + ('For example, { ' + key + ': () => InnerComponent } instead of { ' + key + ': InnerComponent }'))
        );
      });
    }

    return _this;
  }

  StyledComponent.prototype.render = function render() {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(StyleSheetConsumer, null, this.renderOuter);
  };

  StyledComponent.prototype.renderOuter = function renderOuter() {
    var styleSheet = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : StyleSheet.master;
    this.styleSheet = styleSheet; // No need to subscribe a static component to theme changes, it won't change anything

    if (this.props.forwardedComponent.componentStyle.isStatic) return this.renderInner();
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ThemeConsumer, null, this.renderInner);
  };

  StyledComponent.prototype.renderInner = function renderInner(theme) {
    var _props$forwardedCompo = this.props.forwardedComponent,
        componentStyle = _props$forwardedCompo.componentStyle,
        defaultProps = _props$forwardedCompo.defaultProps,
        displayName = _props$forwardedCompo.displayName,
        foldedComponentIds = _props$forwardedCompo.foldedComponentIds,
        styledComponentId = _props$forwardedCompo.styledComponentId,
        target = _props$forwardedCompo.target;
    var generatedClassName = void 0;

    if (componentStyle.isStatic) {
      generatedClassName = this.generateAndInjectStyles(EMPTY_OBJECT, this.props);
    } else {
      generatedClassName = this.generateAndInjectStyles(determineTheme(this.props, theme, defaultProps) || EMPTY_OBJECT, this.props);
    }

    var elementToBeCreated = this.props.as || this.attrs.as || target;
    var isTargetTag = isTag(elementToBeCreated);
    var propsForElement = {};

    var computedProps = _extends({}, this.props, this.attrs);

    var key = void 0; // eslint-disable-next-line guard-for-in

    for (key in computedProps) {
      if ( true && key === 'innerRef' && isTargetTag) {
        this.warnInnerRef(displayName);
      }

      if (key === 'forwardedComponent' || key === 'as') {
        continue;
      } else if (key === 'forwardedRef') propsForElement.ref = computedProps[key];else if (key === 'forwardedAs') propsForElement.as = computedProps[key];else if (!isTargetTag || Object(_emotion_is_prop_valid__WEBPACK_IMPORTED_MODULE_7__["default"])(key)) {
        // Don't pass through non HTML tags through to HTML elements
        propsForElement[key] = computedProps[key];
      }
    }

    if (this.props.style && this.attrs.style) {
      propsForElement.style = _extends({}, this.attrs.style, this.props.style);
    }

    propsForElement.className = Array.prototype.concat(foldedComponentIds, styledComponentId, generatedClassName !== styledComponentId ? generatedClassName : null, this.props.className, this.attrs.className).filter(Boolean).join(' ');
    return Object(react__WEBPACK_IMPORTED_MODULE_2__["createElement"])(elementToBeCreated, propsForElement);
  };

  StyledComponent.prototype.buildExecutionContext = function buildExecutionContext(theme, props, attrs) {
    var _this2 = this;

    var context = _extends({}, props, {
      theme: theme
    });

    if (!attrs.length) return context;
    this.attrs = {};
    attrs.forEach(function (attrDef) {
      var resolvedAttrDef = attrDef;
      var attrDefWasFn = false;
      var attr = void 0;
      var key = void 0;

      if (isFunction(resolvedAttrDef)) {
        // $FlowFixMe
        resolvedAttrDef = resolvedAttrDef(context);
        attrDefWasFn = true;
      }
      /* eslint-disable guard-for-in */
      // $FlowFixMe


      for (key in resolvedAttrDef) {
        attr = resolvedAttrDef[key];

        if (!attrDefWasFn) {
          if (isFunction(attr) && !isDerivedReactComponent(attr) && !isStyledComponent(attr)) {
            if (true) {
              _this2.warnAttrsFnObjectKeyDeprecated(key, props.forwardedComponent.displayName);
            }

            attr = attr(context);

            if ( true && react__WEBPACK_IMPORTED_MODULE_2___default.a.isValidElement(attr)) {
              _this2.warnNonStyledComponentAttrsObjectKey(key, props.forwardedComponent.displayName);
            }
          }
        }

        _this2.attrs[key] = attr;
        context[key] = attr;
      }
      /* eslint-enable */

    });
    return context;
  };

  StyledComponent.prototype.generateAndInjectStyles = function generateAndInjectStyles(theme, props) {
    var _props$forwardedCompo2 = props.forwardedComponent,
        attrs = _props$forwardedCompo2.attrs,
        componentStyle = _props$forwardedCompo2.componentStyle,
        warnTooManyClasses = _props$forwardedCompo2.warnTooManyClasses; // statically styled-components don't need to build an execution context object,
    // and shouldn't be increasing the number of class names

    if (componentStyle.isStatic && !attrs.length) {
      return componentStyle.generateAndInjectStyles(EMPTY_OBJECT, this.styleSheet);
    }

    var className = componentStyle.generateAndInjectStyles(this.buildExecutionContext(theme, props, attrs), this.styleSheet);
    if ( true && warnTooManyClasses) warnTooManyClasses(className);
    return className;
  };

  return StyledComponent;
}(react__WEBPACK_IMPORTED_MODULE_2__["Component"]);

function createStyledComponent(target, options, rules) {
  var isTargetStyledComp = isStyledComponent(target);
  var isClass = !isTag(target);
  var _options$displayName = options.displayName,
      displayName = _options$displayName === undefined ? generateDisplayName(target) : _options$displayName,
      _options$componentId = options.componentId,
      componentId = _options$componentId === undefined ? generateId(ComponentStyle, options.displayName, options.parentComponentId) : _options$componentId,
      _options$ParentCompon = options.ParentComponent,
      ParentComponent = _options$ParentCompon === undefined ? StyledComponent : _options$ParentCompon,
      _options$attrs = options.attrs,
      attrs = _options$attrs === undefined ? EMPTY_ARRAY : _options$attrs;
  var styledComponentId = options.displayName && options.componentId ? escape(options.displayName) + '-' + options.componentId : options.componentId || componentId; // fold the underlying StyledComponent attrs up (implicit extend)

  var finalAttrs = // $FlowFixMe
  isTargetStyledComp && target.attrs ? Array.prototype.concat(target.attrs, attrs).filter(Boolean) : attrs;
  var componentStyle = new ComponentStyle(isTargetStyledComp ? // fold the underlying StyledComponent rules up (implicit extend)
  // $FlowFixMe
  target.componentStyle.rules.concat(rules) : rules, finalAttrs, styledComponentId);
  /**
   * forwardRef creates a new interim component, which we'll take advantage of
   * instead of extending ParentComponent to create _another_ interim class
   */

  var WrappedStyledComponent = void 0;

  var forwardRef = function forwardRef(props, ref) {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ParentComponent, _extends({}, props, {
      forwardedComponent: WrappedStyledComponent,
      forwardedRef: ref
    }));
  };

  forwardRef.displayName = displayName;
  WrappedStyledComponent = react__WEBPACK_IMPORTED_MODULE_2___default.a.forwardRef(forwardRef);
  WrappedStyledComponent.displayName = displayName; // $FlowFixMe

  WrappedStyledComponent.attrs = finalAttrs; // $FlowFixMe

  WrappedStyledComponent.componentStyle = componentStyle; // $FlowFixMe

  WrappedStyledComponent.foldedComponentIds = isTargetStyledComp ? // $FlowFixMe
  Array.prototype.concat(target.foldedComponentIds, target.styledComponentId) : EMPTY_ARRAY; // $FlowFixMe

  WrappedStyledComponent.styledComponentId = styledComponentId; // fold the underlying StyledComponent target up since we folded the styles
  // $FlowFixMe

  WrappedStyledComponent.target = isTargetStyledComp ? target.target : target; // $FlowFixMe

  WrappedStyledComponent.withComponent = function withComponent(tag) {
    var previousComponentId = options.componentId,
        optionsToCopy = objectWithoutProperties(options, ['componentId']);
    var newComponentId = previousComponentId && previousComponentId + '-' + (isTag(tag) ? tag : escape(getComponentName(tag)));

    var newOptions = _extends({}, optionsToCopy, {
      attrs: finalAttrs,
      componentId: newComponentId,
      ParentComponent: ParentComponent
    });

    return createStyledComponent(tag, newOptions, rules);
  }; // $FlowFixMe


  Object.defineProperty(WrappedStyledComponent, 'defaultProps', {
    get: function get$$1() {
      return this._foldedDefaultProps;
    },
    set: function set$$1(obj) {
      // $FlowFixMe
      this._foldedDefaultProps = isTargetStyledComp ? Object(merge_anything__WEBPACK_IMPORTED_MODULE_8__["default"])(target.defaultProps, obj) : obj;
    }
  });

  if (true) {
    // $FlowFixMe
    WrappedStyledComponent.warnTooManyClasses = createWarnTooManyClasses(displayName);
  } // $FlowFixMe


  WrappedStyledComponent.toString = function () {
    return '.' + WrappedStyledComponent.styledComponentId;
  };

  if (isClass) {
    hoistNonReactStatics(WrappedStyledComponent, target, {
      // all SC-specific things should not be hoisted
      attrs: true,
      componentStyle: true,
      displayName: true,
      foldedComponentIds: true,
      styledComponentId: true,
      target: true,
      withComponent: true
    });
  }

  return WrappedStyledComponent;
} // 
// Thanks to ReactDOMFactories for this handy list!


var domElements = ['a', 'abbr', 'address', 'area', 'article', 'aside', 'audio', 'b', 'base', 'bdi', 'bdo', 'big', 'blockquote', 'body', 'br', 'button', 'canvas', 'caption', 'cite', 'code', 'col', 'colgroup', 'data', 'datalist', 'dd', 'del', 'details', 'dfn', 'dialog', 'div', 'dl', 'dt', 'em', 'embed', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hgroup', 'hr', 'html', 'i', 'iframe', 'img', 'input', 'ins', 'kbd', 'keygen', 'label', 'legend', 'li', 'link', 'main', 'map', 'mark', 'marquee', 'menu', 'menuitem', 'meta', 'meter', 'nav', 'noscript', 'object', 'ol', 'optgroup', 'option', 'output', 'p', 'param', 'picture', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script', 'section', 'select', 'small', 'source', 'span', 'strong', 'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'track', 'u', 'ul', 'var', 'video', 'wbr', // SVG
'circle', 'clipPath', 'defs', 'ellipse', 'foreignObject', 'g', 'image', 'line', 'linearGradient', 'marker', 'mask', 'path', 'pattern', 'polygon', 'polyline', 'radialGradient', 'rect', 'stop', 'svg', 'text', 'tspan']; // 

var styled = function styled(tag) {
  return constructWithOptions(createStyledComponent, tag);
}; // Shorthands for all valid HTML Elements


domElements.forEach(function (domElement) {
  styled[domElement] = styled(domElement);
}); // 

var GlobalStyle = function () {
  function GlobalStyle(rules, componentId) {
    classCallCheck(this, GlobalStyle);
    this.rules = rules;
    this.componentId = componentId;
    this.isStatic = isStaticRules(rules, EMPTY_ARRAY);

    if (!StyleSheet.master.hasId(componentId)) {
      StyleSheet.master.deferredInject(componentId, []);
    }
  }

  GlobalStyle.prototype.createStyles = function createStyles(executionContext, styleSheet) {
    var flatCSS = flatten(this.rules, executionContext, styleSheet);
    var css = stringifyRules(flatCSS, '');
    styleSheet.inject(this.componentId, css);
  };

  GlobalStyle.prototype.removeStyles = function removeStyles(styleSheet) {
    var componentId = this.componentId;

    if (styleSheet.hasId(componentId)) {
      styleSheet.remove(componentId);
    }
  }; // TODO: overwrite in-place instead of remove+create?


  GlobalStyle.prototype.renderStyles = function renderStyles(executionContext, styleSheet) {
    this.removeStyles(styleSheet);
    this.createStyles(executionContext, styleSheet);
  };

  return GlobalStyle;
}(); // 
// place our cache into shared context so it'll persist between HMRs


if (IS_BROWSER) {
  window.scCGSHMRCache = {};
}

function createGlobalStyle(strings) {
  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  var rules = css.apply(undefined, [strings].concat(interpolations));
  var id = 'sc-global-' + murmurhash(JSON.stringify(rules));
  var style = new GlobalStyle(rules, id);

  var GlobalStyleComponent = function (_React$Component) {
    inherits(GlobalStyleComponent, _React$Component);

    function GlobalStyleComponent(props) {
      classCallCheck(this, GlobalStyleComponent);

      var _this = possibleConstructorReturn(this, _React$Component.call(this, props));

      var _this$constructor = _this.constructor,
          globalStyle = _this$constructor.globalStyle,
          styledComponentId = _this$constructor.styledComponentId;

      if (IS_BROWSER) {
        window.scCGSHMRCache[styledComponentId] = (window.scCGSHMRCache[styledComponentId] || 0) + 1;
      }
      /**
       * This fixes HMR compatibility. Don't ask me why, but this combination of
       * caching the closure variables via statics and then persisting the statics in
       * state works across HMR where no other combination did. ¯\_(ツ)_/¯
       */


      _this.state = {
        globalStyle: globalStyle,
        styledComponentId: styledComponentId
      };
      return _this;
    }

    GlobalStyleComponent.prototype.componentWillUnmount = function componentWillUnmount() {
      if (window.scCGSHMRCache[this.state.styledComponentId]) {
        window.scCGSHMRCache[this.state.styledComponentId] -= 1;
      }
      /**
       * Depending on the order "render" is called this can cause the styles to be lost
       * until the next render pass of the remaining instance, which may
       * not be immediate.
       */


      if (window.scCGSHMRCache[this.state.styledComponentId] === 0) {
        this.state.globalStyle.removeStyles(this.styleSheet);
      }
    };

    GlobalStyleComponent.prototype.render = function render() {
      var _this2 = this;

      if ( true && react__WEBPACK_IMPORTED_MODULE_2___default.a.Children.count(this.props.children)) {
        // eslint-disable-next-line no-console
        console.warn('The global style component ' + this.state.styledComponentId + ' was given child JSX. createGlobalStyle does not render children.');
      }

      return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(StyleSheetConsumer, null, function (styleSheet) {
        _this2.styleSheet = styleSheet || StyleSheet.master;
        var globalStyle = _this2.state.globalStyle;

        if (globalStyle.isStatic) {
          globalStyle.renderStyles(STATIC_EXECUTION_CONTEXT, _this2.styleSheet);
          return null;
        } else {
          return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ThemeConsumer, null, function (theme) {
            // $FlowFixMe
            var defaultProps = _this2.constructor.defaultProps;

            var context = _extends({}, _this2.props);

            if (typeof theme !== 'undefined') {
              context.theme = determineTheme(_this2.props, theme, defaultProps);
            }

            globalStyle.renderStyles(context, _this2.styleSheet);
            return null;
          });
        }
      });
    };

    return GlobalStyleComponent;
  }(react__WEBPACK_IMPORTED_MODULE_2___default.a.Component);

  GlobalStyleComponent.globalStyle = style;
  GlobalStyleComponent.styledComponentId = id;
  return GlobalStyleComponent;
} // 


var replaceWhitespace = function replaceWhitespace(str) {
  return str.replace(/\s|\\n/g, '');
};

function keyframes(strings) {
  /* Warning if you've used keyframes on React Native */
  if ( true && typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    // eslint-disable-next-line no-console
    console.warn('`keyframes` cannot be used on ReactNative, only on the web. To do animation in ReactNative please use Animated.');
  }

  for (var _len = arguments.length, interpolations = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    interpolations[_key - 1] = arguments[_key];
  }

  var rules = css.apply(undefined, [strings].concat(interpolations));
  var name = generateAlphabeticName(murmurhash(replaceWhitespace(JSON.stringify(rules))));
  return new Keyframes(name, stringifyRules(rules, name, '@keyframes'));
} // 


var withTheme = function withTheme(Component$$1) {
  var WithTheme = react__WEBPACK_IMPORTED_MODULE_2___default.a.forwardRef(function (props, ref) {
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(ThemeConsumer, null, function (theme) {
      // $FlowFixMe
      var defaultProps = Component$$1.defaultProps;
      var themeProp = determineTheme(props, theme, defaultProps);

      if ( true && themeProp === undefined) {
        // eslint-disable-next-line no-console
        console.warn('[withTheme] You are not using a ThemeProvider nor passing a theme prop or a theme in defaultProps in component class "' + getComponentName(Component$$1) + '"');
      }

      return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(Component$$1, _extends({}, props, {
        theme: themeProp,
        ref: ref
      }));
    });
  });
  hoistNonReactStatics(WithTheme, Component$$1);
  WithTheme.displayName = 'WithTheme(' + getComponentName(Component$$1) + ')';
  return WithTheme;
}; // 

/* eslint-disable */


var __DO_NOT_USE_OR_YOU_WILL_BE_HAUNTED_BY_SPOOKY_GHOSTS = {
  StyleSheet: StyleSheet
}; // 

/* Warning if you've imported this file on React Native */

if ( true && typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
  // eslint-disable-next-line no-console
  console.warn("It looks like you've imported 'styled-components' on React Native.\n" + "Perhaps you're looking to import 'styled-components/native'?\n" + 'Read more about this at https://www.styled-components.com/docs/basics#react-native');
}
/* Warning if there are several instances of styled-components */


if ( true && typeof window !== 'undefined' && typeof navigator !== 'undefined' && typeof navigator.userAgent === 'string' && navigator.userAgent.indexOf('Node.js') === -1 && navigator.userAgent.indexOf('jsdom') === -1) {
  window['__styled-components-init__'] = window['__styled-components-init__'] || 0;

  if (window['__styled-components-init__'] === 1) {
    // eslint-disable-next-line no-console
    console.warn("It looks like there are several instances of 'styled-components' initialized in this application. " + 'This may cause dynamic styles not rendering properly, errors happening during rehydration process ' + 'and makes your application bigger without a good reason.\n\n' + 'See https://s-c.sh/2BAXzed for more info.');
  }

  window['__styled-components-init__'] += 1;
} //


/* harmony default export */ __webpack_exports__["default"] = (styled);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../process/browser.js */ "./node_modules/process/browser.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvc3R5bGVkLWNvbXBvbmVudHMvZGlzdC9zdHlsZWQtY29tcG9uZW50cy5icm93c2VyLmVzbS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9zcmMvbW9kZWxzL1N0eWxlVGFncy5qcyIsIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3Avc3JjL21vZGVscy9UaGVtZVByb3ZpZGVyLmpzIiwiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9zcmMvbW9kZWxzL1N0eWxlU2hlZXRNYW5hZ2VyLmpzIiwiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9zcmMvbW9kZWxzL1N0eWxlZENvbXBvbmVudC5qcyIsIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3Avc3JjL21vZGVscy9HbG9iYWxTdHlsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBAZmxvd1xuLyogZXNsaW50LWRpc2FibGUgZmxvd3R5cGUvb2JqZWN0LXR5cGUtZGVsaW1pdGVyICovXG4vKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9wcm9wLXR5cGVzICovXG5cbmltcG9ydCBSZWFjdCwgeyB0eXBlIEVsZW1lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBJU19CUk9XU0VSLCBESVNBQkxFX1NQRUVEWSwgU0NfQVRUUiwgU0NfVkVSU0lPTl9BVFRSIH0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCBTdHlsZWRFcnJvciBmcm9tICcuLi91dGlscy9lcnJvcic7XG5pbXBvcnQgeyB0eXBlIEV4dHJhY3RlZENvbXAgfSBmcm9tICcuLi91dGlscy9leHRyYWN0Q29tcHNGcm9tQ1NTJztcbmltcG9ydCB7IHNwbGl0QnlSdWxlcyB9IGZyb20gJy4uL3V0aWxzL3N0cmluZ2lmeVJ1bGVzJztcbmltcG9ydCBnZXROb25jZSBmcm9tICcuLi91dGlscy9ub25jZSc7XG5cbmltcG9ydCB7XG4gIHR5cGUgTmFtZXMsXG4gIGFkZE5hbWVGb3JJZCxcbiAgcmVzZXRJZE5hbWVzLFxuICBoYXNOYW1lRm9ySWQsXG4gIHN0cmluZ2lmeU5hbWVzLFxuICBjbG9uZU5hbWVzLFxufSBmcm9tICcuLi91dGlscy9zdHlsZU5hbWVzJztcblxuaW1wb3J0IHsgc2hlZXRGb3JUYWcsIHNhZmVJbnNlcnRSdWxlLCBkZWxldGVSdWxlcyB9IGZyb20gJy4uL3V0aWxzL2luc2VydFJ1bGVIZWxwZXJzJztcblxuZGVjbGFyZSB2YXIgX19WRVJTSU9OX186IHN0cmluZztcblxuZXhwb3J0IGludGVyZmFjZSBUYWc8VD4ge1xuICAvLyAkRmxvd0ZpeE1lOiBEb2Vzbid0IHNlZW0gdG8gYWNjZXB0IGFueSBjb21iaW5hdGlvbiB3LyBIVE1MU3R5bGVFbGVtZW50IGZvciBzb21lIHJlYXNvblxuICBzdHlsZVRhZzogSFRNTFN0eWxlRWxlbWVudCB8IG51bGw7XG4gIC8qIGxpc3RzIGFsbCBpZHMgb2YgdGhlIHRhZyAqL1xuICBnZXRJZHMoKTogc3RyaW5nW107XG4gIC8qIGNoZWNrcyB3aGV0aGVyIGBuYW1lYCBpcyBhbHJlYWR5IGluamVjdGVkIGZvciBgaWRgICovXG4gIGhhc05hbWVGb3JJZChpZDogc3RyaW5nLCBuYW1lOiBzdHJpbmcpOiBib29sZWFuO1xuICAvKiBpbnNlcnRzIGEgbWFya2VyIHRvIGVuc3VyZSB0aGUgaWQncyBjb3JyZWN0IHBvc2l0aW9uIGluIHRoZSBzaGVldCAqL1xuICBpbnNlcnRNYXJrZXIoaWQ6IHN0cmluZyk6IFQ7XG4gIC8qIGluc2VydHMgcnVsZXMgYWNjb3JkaW5nIHRvIHRoZSBpZHMgbWFya2VycyAqL1xuICBpbnNlcnRSdWxlcyhpZDogc3RyaW5nLCBjc3NSdWxlczogc3RyaW5nW10sIG5hbWU6ID9zdHJpbmcpOiB2b2lkO1xuICAvKiByZW1vdmVzIGFsbCBydWxlcyBiZWxvbmdpbmcgdG8gdGhlIGlkLCBrZWVwaW5nIHRoZSBtYXJrZXIgYXJvdW5kICovXG4gIHJlbW92ZVJ1bGVzKGlkOiBzdHJpbmcpOiB2b2lkO1xuICBjc3MoKTogc3RyaW5nO1xuICB0b0hUTUwoYWRkaXRpb25hbEF0dHJzOiA/c3RyaW5nKTogc3RyaW5nO1xuICB0b0VsZW1lbnQoKTogRWxlbWVudDwqPjtcbiAgY2xvbmUoKTogVGFnPFQ+O1xuICAvKiB1c2VkIGluIHNlcnZlciBzaWRlIHJlbmRlcmluZyB0byBpbmRpY2F0ZSB0aGF0IHRoZSBydWxlcyBpbiB0aGUgdGFnIGhhdmUgYmVlbiBmbHVzaGVkIHRvIEhUTUwgKi9cbiAgc2VhbGVkOiBib29sZWFuO1xufVxuXG4vKiB0aGlzIG1hcmtlciBzZXBhcmF0ZXMgY29tcG9uZW50IHN0eWxlcyBhbmQgaXMgaW1wb3J0YW50IGZvciByZWh5ZHJhdGlvbiAqL1xuY29uc3QgbWFrZVRleHRNYXJrZXIgPSBpZCA9PiBgXFxuLyogc2MtY29tcG9uZW50LWlkOiAke2lkfSAqL1xcbmA7XG5cbi8qIGFkZCB1cCBhbGwgbnVtYmVycyBpbiBhcnJheSB1cCB1bnRpbCBhbmQgaW5jbHVkaW5nIHRoZSBpbmRleCAqL1xuY29uc3QgYWRkVXBVbnRpbEluZGV4ID0gKHNpemVzOiBudW1iZXJbXSwgaW5kZXg6IG51bWJlcik6IG51bWJlciA9PiB7XG4gIGxldCB0b3RhbFVwVG9JbmRleCA9IDA7XG4gIGZvciAobGV0IGkgPSAwOyBpIDw9IGluZGV4OyBpICs9IDEpIHtcbiAgICB0b3RhbFVwVG9JbmRleCArPSBzaXplc1tpXTtcbiAgfVxuXG4gIHJldHVybiB0b3RhbFVwVG9JbmRleDtcbn07XG5cbi8qIGNyZWF0ZSBhIG5ldyBzdHlsZSB0YWcgYWZ0ZXIgbGFzdEVsICovXG5jb25zdCBtYWtlU3R5bGVUYWcgPSAodGFyZ2V0OiA/SFRNTEVsZW1lbnQsIHRhZ0VsOiA/Tm9kZSwgaW5zZXJ0QmVmb3JlOiA/Ym9vbGVhbikgPT4ge1xuICBsZXQgdGFyZ2V0RG9jdW1lbnQgPSBkb2N1bWVudDtcbiAgaWYodGFyZ2V0KSB0YXJnZXREb2N1bWVudCA9IHRhcmdldC5vd25lckRvY3VtZW50O1xuICBlbHNlIGlmKHRhZ0VsKSB0YXJnZXREb2N1bWVudCA9IHRhZ0VsLm93bmVyRG9jdW1lbnQ7XG5cbiAgY29uc3QgZWwgPSB0YXJnZXREb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzdHlsZScpO1xuICBlbC5zZXRBdHRyaWJ1dGUoU0NfQVRUUiwgJycpO1xuICBlbC5zZXRBdHRyaWJ1dGUoU0NfVkVSU0lPTl9BVFRSLCBfX1ZFUlNJT05fXyk7XG5cbiAgY29uc3Qgbm9uY2UgPSBnZXROb25jZSgpO1xuICBpZiAobm9uY2UpIHtcbiAgICBlbC5zZXRBdHRyaWJ1dGUoJ25vbmNlJywgbm9uY2UpO1xuICB9XG5cbiAgLyogV29yayBhcm91bmQgaW5zZXJ0UnVsZSBxdWlyayBpbiBFZGdlSFRNTCAqL1xuICBlbC5hcHBlbmRDaGlsZCh0YXJnZXREb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSgnJykpO1xuXG4gIGlmICh0YXJnZXQgJiYgIXRhZ0VsKSB7XG4gICAgLyogQXBwZW5kIHRvIHRhcmdldCB3aGVuIG5vIHByZXZpb3VzIGVsZW1lbnQgd2FzIHBhc3NlZCAqL1xuICAgIHRhcmdldC5hcHBlbmRDaGlsZChlbCk7XG4gIH0gZWxzZSB7XG4gICAgaWYgKCF0YWdFbCB8fCAhdGFyZ2V0IHx8ICF0YWdFbC5wYXJlbnROb2RlKSB7XG4gICAgICB0aHJvdyBuZXcgU3R5bGVkRXJyb3IoNik7XG4gICAgfVxuXG4gICAgLyogSW5zZXJ0IG5ldyBzdHlsZSB0YWcgYWZ0ZXIgdGhlIHByZXZpb3VzIG9uZSAqL1xuICAgIHRhZ0VsLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGVsLCBpbnNlcnRCZWZvcmUgPyB0YWdFbCA6IHRhZ0VsLm5leHRTaWJsaW5nKTtcbiAgfVxuXG4gIHJldHVybiBlbDtcbn07XG5cbi8qIHRha2VzIGEgY3NzIGZhY3RvcnkgZnVuY3Rpb24gYW5kIG91dHB1dHMgYW4gaHRtbCBzdHlsZWQgdGFnIGZhY3RvcnkgKi9cbmNvbnN0IHdyYXBBc0h0bWxUYWcgPSAoY3NzOiAoKSA9PiBzdHJpbmcsIG5hbWVzOiBOYW1lcykgPT4gKGFkZGl0aW9uYWxBdHRyczogP3N0cmluZyk6IHN0cmluZyA9PiB7XG4gIGNvbnN0IG5vbmNlID0gZ2V0Tm9uY2UoKTtcbiAgY29uc3QgYXR0cnMgPSBbXG4gICAgbm9uY2UgJiYgYG5vbmNlPVwiJHtub25jZX1cImAsXG4gICAgYCR7U0NfQVRUUn09XCIke3N0cmluZ2lmeU5hbWVzKG5hbWVzKX1cImAsXG4gICAgYCR7U0NfVkVSU0lPTl9BVFRSfT1cIiR7X19WRVJTSU9OX199XCJgLFxuICAgIGFkZGl0aW9uYWxBdHRycyxcbiAgXTtcblxuICBjb25zdCBodG1sQXR0ciA9IGF0dHJzLmZpbHRlcihCb29sZWFuKS5qb2luKCcgJyk7XG4gIHJldHVybiBgPHN0eWxlICR7aHRtbEF0dHJ9PiR7Y3NzKCl9PC9zdHlsZT5gO1xufTtcblxuLyogdGFrZXMgYSBjc3MgZmFjdG9yeSBmdW5jdGlvbiBhbmQgb3V0cHV0cyBhbiBlbGVtZW50IGZhY3RvcnkgKi9cbmNvbnN0IHdyYXBBc0VsZW1lbnQgPSAoY3NzOiAoKSA9PiBzdHJpbmcsIG5hbWVzOiBOYW1lcykgPT4gKCkgPT4ge1xuICBjb25zdCBwcm9wcyA9IHtcbiAgICBbU0NfQVRUUl06IHN0cmluZ2lmeU5hbWVzKG5hbWVzKSxcbiAgICBbU0NfVkVSU0lPTl9BVFRSXTogX19WRVJTSU9OX18sXG4gIH07XG5cbiAgY29uc3Qgbm9uY2UgPSBnZXROb25jZSgpO1xuICBpZiAobm9uY2UpIHtcbiAgICAvLyAkRmxvd0ZpeE1lXG4gICAgcHJvcHMubm9uY2UgPSBub25jZTtcbiAgfVxuXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC9uby1kYW5nZXJcbiAgcmV0dXJuIDxzdHlsZSB7Li4ucHJvcHN9IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogY3NzKCkgfX0gLz47XG59O1xuXG5jb25zdCBnZXRJZHNGcm9tTWFya2Vyc0ZhY3RvcnkgPSAobWFya2VyczogT2JqZWN0KSA9PiAoKTogc3RyaW5nW10gPT4gT2JqZWN0LmtleXMobWFya2Vycyk7XG5cbi8qIHNwZWVkeSB0YWdzIHV0aWxpc2UgaW5zZXJ0UnVsZSAqL1xuY29uc3QgbWFrZVNwZWVkeVRhZyA9IChlbDogSFRNTFN0eWxlRWxlbWVudCwgZ2V0SW1wb3J0UnVsZVRhZzogPygpID0+IFRhZzxhbnk+KTogVGFnPG51bWJlcj4gPT4ge1xuICBjb25zdCBuYW1lczogTmFtZXMgPSAoT2JqZWN0LmNyZWF0ZShudWxsKTogT2JqZWN0KTtcbiAgY29uc3QgbWFya2VycyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gIGNvbnN0IHNpemVzOiBudW1iZXJbXSA9IFtdO1xuXG4gIGNvbnN0IGV4dHJhY3RJbXBvcnQgPSBnZXRJbXBvcnRSdWxlVGFnICE9PSB1bmRlZmluZWQ7XG4gIC8qIGluZGljYXRlcyB3aGV0aGVyIGdldEltcG9ydFJ1bGVUYWcgd2FzIGNhbGxlZCAqL1xuICBsZXQgdXNlZEltcG9ydFJ1bGVUYWcgPSBmYWxzZTtcblxuICBjb25zdCBpbnNlcnRNYXJrZXIgPSBpZCA9PiB7XG4gICAgY29uc3QgcHJldiA9IG1hcmtlcnNbaWRdO1xuICAgIGlmIChwcmV2ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiBwcmV2O1xuICAgIH1cblxuICAgIG1hcmtlcnNbaWRdID0gc2l6ZXMubGVuZ3RoO1xuICAgIHNpemVzLnB1c2goMCk7XG4gICAgcmVzZXRJZE5hbWVzKG5hbWVzLCBpZCk7XG5cbiAgICByZXR1cm4gbWFya2Vyc1tpZF07XG4gIH07XG5cbiAgY29uc3QgaW5zZXJ0UnVsZXMgPSAoaWQsIGNzc1J1bGVzLCBuYW1lKSA9PiB7XG4gICAgY29uc3QgbWFya2VyID0gaW5zZXJ0TWFya2VyKGlkKTtcbiAgICBjb25zdCBzaGVldCA9IHNoZWV0Rm9yVGFnKGVsKTtcbiAgICBjb25zdCBpbnNlcnRJbmRleCA9IGFkZFVwVW50aWxJbmRleChzaXplcywgbWFya2VyKTtcblxuICAgIGxldCBpbmplY3RlZFJ1bGVzID0gMDtcbiAgICBjb25zdCBpbXBvcnRSdWxlcyA9IFtdO1xuICAgIGNvbnN0IGNzc1J1bGVzU2l6ZSA9IGNzc1J1bGVzLmxlbmd0aDtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY3NzUnVsZXNTaXplOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IGNzc1J1bGUgPSBjc3NSdWxlc1tpXTtcbiAgICAgIGxldCBtYXlIYXZlSW1wb3J0ID0gZXh0cmFjdEltcG9ydDsgLyogQGltcG9ydCBydWxlcyBhcmUgcmVvcmRlcmVkIHRvIGFwcGVhciBmaXJzdCAqL1xuICAgICAgaWYgKG1heUhhdmVJbXBvcnQgJiYgY3NzUnVsZS5pbmRleE9mKCdAaW1wb3J0JykgIT09IC0xKSB7XG4gICAgICAgIGltcG9ydFJ1bGVzLnB1c2goY3NzUnVsZSk7XG4gICAgICB9IGVsc2UgaWYgKHNhZmVJbnNlcnRSdWxlKHNoZWV0LCBjc3NSdWxlLCBpbnNlcnRJbmRleCArIGluamVjdGVkUnVsZXMpKSB7XG4gICAgICAgIG1heUhhdmVJbXBvcnQgPSBmYWxzZTtcbiAgICAgICAgaW5qZWN0ZWRSdWxlcyArPSAxO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChleHRyYWN0SW1wb3J0ICYmIGltcG9ydFJ1bGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIHVzZWRJbXBvcnRSdWxlVGFnID0gdHJ1ZTtcbiAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgIGdldEltcG9ydFJ1bGVUYWcoKS5pbnNlcnRSdWxlcyhgJHtpZH0taW1wb3J0YCwgaW1wb3J0UnVsZXMpO1xuICAgIH1cblxuICAgIHNpemVzW21hcmtlcl0gKz0gaW5qZWN0ZWRSdWxlczsgLyogYWRkIHVwIG5vIG9mIGluamVjdGVkIHJ1bGVzICovXG4gICAgYWRkTmFtZUZvcklkKG5hbWVzLCBpZCwgbmFtZSk7XG4gIH07XG5cbiAgY29uc3QgcmVtb3ZlUnVsZXMgPSBpZCA9PiB7XG4gICAgY29uc3QgbWFya2VyID0gbWFya2Vyc1tpZF07XG4gICAgaWYgKG1hcmtlciA9PT0gdW5kZWZpbmVkKSByZXR1cm47XG4gICAgLy8gJEZsb3dGaXhNZVxuICAgIGlmIChlbC5pc0Nvbm5lY3RlZCA9PT0gZmFsc2UpIHJldHVybjtcblxuICAgIGNvbnN0IHNpemUgPSBzaXplc1ttYXJrZXJdO1xuICAgIGNvbnN0IHNoZWV0ID0gc2hlZXRGb3JUYWcoZWwpO1xuICAgIGNvbnN0IHJlbW92YWxJbmRleCA9IGFkZFVwVW50aWxJbmRleChzaXplcywgbWFya2VyKSAtIDE7XG4gICAgZGVsZXRlUnVsZXMoc2hlZXQsIHJlbW92YWxJbmRleCwgc2l6ZSk7XG4gICAgc2l6ZXNbbWFya2VyXSA9IDA7XG4gICAgcmVzZXRJZE5hbWVzKG5hbWVzLCBpZCk7XG5cbiAgICBpZiAoZXh0cmFjdEltcG9ydCAmJiB1c2VkSW1wb3J0UnVsZVRhZykge1xuICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgZ2V0SW1wb3J0UnVsZVRhZygpLnJlbW92ZVJ1bGVzKGAke2lkfS1pbXBvcnRgKTtcbiAgICB9XG4gIH07XG5cbiAgY29uc3QgY3NzID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgY3NzUnVsZXMgfSA9IHNoZWV0Rm9yVGFnKGVsKTtcbiAgICBsZXQgc3RyID0gJyc7XG5cbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZ3VhcmQtZm9yLWluXG4gICAgZm9yIChjb25zdCBpZCBpbiBtYXJrZXJzKSB7XG4gICAgICBzdHIgKz0gbWFrZVRleHRNYXJrZXIoaWQpO1xuICAgICAgY29uc3QgbWFya2VyID0gbWFya2Vyc1tpZF07XG4gICAgICBjb25zdCBlbmQgPSBhZGRVcFVudGlsSW5kZXgoc2l6ZXMsIG1hcmtlcik7XG4gICAgICBjb25zdCBzaXplID0gc2l6ZXNbbWFya2VyXTtcbiAgICAgIGZvciAobGV0IGkgPSBlbmQgLSBzaXplOyBpIDwgZW5kOyBpICs9IDEpIHtcbiAgICAgICAgY29uc3QgcnVsZSA9IGNzc1J1bGVzW2ldO1xuICAgICAgICBpZiAocnVsZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgc3RyICs9IHJ1bGUuY3NzVGV4dDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBzdHI7XG4gIH07XG5cbiAgcmV0dXJuIHtcbiAgICBjbG9uZSgpIHtcbiAgICAgIHRocm93IG5ldyBTdHlsZWRFcnJvcig1KTtcbiAgICB9LFxuICAgIGNzcyxcbiAgICBnZXRJZHM6IGdldElkc0Zyb21NYXJrZXJzRmFjdG9yeShtYXJrZXJzKSxcbiAgICBoYXNOYW1lRm9ySWQ6IGhhc05hbWVGb3JJZChuYW1lcyksXG4gICAgaW5zZXJ0TWFya2VyLFxuICAgIGluc2VydFJ1bGVzLFxuICAgIHJlbW92ZVJ1bGVzLFxuICAgIHNlYWxlZDogZmFsc2UsXG4gICAgc3R5bGVUYWc6IGVsLFxuICAgIHRvRWxlbWVudDogd3JhcEFzRWxlbWVudChjc3MsIG5hbWVzKSxcbiAgICB0b0hUTUw6IHdyYXBBc0h0bWxUYWcoY3NzLCBuYW1lcyksXG4gIH07XG59O1xuXG5jb25zdCBtYWtlVGV4dE5vZGUgPSAodGFyZ2V0RG9jdW1lbnQsIGlkKSA9PiB0YXJnZXREb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShtYWtlVGV4dE1hcmtlcihpZCkpO1xuXG5jb25zdCBtYWtlQnJvd3NlclRhZyA9IChlbDogSFRNTFN0eWxlRWxlbWVudCwgZ2V0SW1wb3J0UnVsZVRhZzogPygpID0+IFRhZzxhbnk+KTogVGFnPFRleHQ+ID0+IHtcbiAgY29uc3QgbmFtZXMgPSAoT2JqZWN0LmNyZWF0ZShudWxsKTogT2JqZWN0KTtcbiAgY29uc3QgbWFya2VycyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG5cbiAgY29uc3QgZXh0cmFjdEltcG9ydCA9IGdldEltcG9ydFJ1bGVUYWcgIT09IHVuZGVmaW5lZDtcblxuICAvKiBpbmRpY2F0ZXMgd2hldGhlciBnZXRJbXBvcnRSdWxlVGFnIHdhcyBjYWxsZWQgKi9cbiAgbGV0IHVzZWRJbXBvcnRSdWxlVGFnID0gZmFsc2U7XG5cbiAgY29uc3QgaW5zZXJ0TWFya2VyID0gaWQgPT4ge1xuICAgIGNvbnN0IHByZXYgPSBtYXJrZXJzW2lkXTtcbiAgICBpZiAocHJldiAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gcHJldjtcbiAgICB9XG5cbiAgICBtYXJrZXJzW2lkXSA9IG1ha2VUZXh0Tm9kZShlbC5vd25lckRvY3VtZW50LCBpZCk7XG4gICAgZWwuYXBwZW5kQ2hpbGQobWFya2Vyc1tpZF0pO1xuICAgIG5hbWVzW2lkXSA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG5cbiAgICByZXR1cm4gbWFya2Vyc1tpZF07XG4gIH07XG5cbiAgY29uc3QgaW5zZXJ0UnVsZXMgPSAoaWQsIGNzc1J1bGVzLCBuYW1lKSA9PiB7XG4gICAgY29uc3QgbWFya2VyID0gaW5zZXJ0TWFya2VyKGlkKTtcbiAgICBjb25zdCBpbXBvcnRSdWxlcyA9IFtdO1xuICAgIGNvbnN0IGNzc1J1bGVzU2l6ZSA9IGNzc1J1bGVzLmxlbmd0aDtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY3NzUnVsZXNTaXplOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IHJ1bGUgPSBjc3NSdWxlc1tpXTtcbiAgICAgIGxldCBtYXlIYXZlSW1wb3J0ID0gZXh0cmFjdEltcG9ydDtcbiAgICAgIGlmIChtYXlIYXZlSW1wb3J0ICYmIHJ1bGUuaW5kZXhPZignQGltcG9ydCcpICE9PSAtMSkge1xuICAgICAgICBpbXBvcnRSdWxlcy5wdXNoKHJ1bGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbWF5SGF2ZUltcG9ydCA9IGZhbHNlO1xuICAgICAgICBjb25zdCBzZXBhcmF0b3IgPSBpID09PSBjc3NSdWxlc1NpemUgLSAxID8gJycgOiAnICc7XG4gICAgICAgIG1hcmtlci5hcHBlbmREYXRhKGAke3J1bGV9JHtzZXBhcmF0b3J9YCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgYWRkTmFtZUZvcklkKG5hbWVzLCBpZCwgbmFtZSk7XG5cbiAgICBpZiAoZXh0cmFjdEltcG9ydCAmJiBpbXBvcnRSdWxlcy5sZW5ndGggPiAwKSB7XG4gICAgICB1c2VkSW1wb3J0UnVsZVRhZyA9IHRydWU7XG4gICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICBnZXRJbXBvcnRSdWxlVGFnKCkuaW5zZXJ0UnVsZXMoYCR7aWR9LWltcG9ydGAsIGltcG9ydFJ1bGVzKTtcbiAgICB9XG4gIH07XG5cbiAgY29uc3QgcmVtb3ZlUnVsZXMgPSBpZCA9PiB7XG4gICAgY29uc3QgbWFya2VyID0gbWFya2Vyc1tpZF07XG4gICAgaWYgKG1hcmtlciA9PT0gdW5kZWZpbmVkKSByZXR1cm47XG5cbiAgICAvKiBjcmVhdGUgbmV3IGVtcHR5IHRleHQgbm9kZSBhbmQgcmVwbGFjZSB0aGUgY3VycmVudCBvbmUgKi9cbiAgICBjb25zdCBuZXdNYXJrZXIgPSBtYWtlVGV4dE5vZGUoZWwub3duZXJEb2N1bWVudCwgaWQpO1xuICAgIGVsLnJlcGxhY2VDaGlsZChuZXdNYXJrZXIsIG1hcmtlcik7XG4gICAgbWFya2Vyc1tpZF0gPSBuZXdNYXJrZXI7XG4gICAgcmVzZXRJZE5hbWVzKG5hbWVzLCBpZCk7XG5cbiAgICBpZiAoZXh0cmFjdEltcG9ydCAmJiB1c2VkSW1wb3J0UnVsZVRhZykge1xuICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgZ2V0SW1wb3J0UnVsZVRhZygpLnJlbW92ZVJ1bGVzKGAke2lkfS1pbXBvcnRgKTtcbiAgICB9XG4gIH07XG5cbiAgY29uc3QgY3NzID0gKCkgPT4ge1xuICAgIGxldCBzdHIgPSAnJztcblxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBndWFyZC1mb3ItaW5cbiAgICBmb3IgKGNvbnN0IGlkIGluIG1hcmtlcnMpIHtcbiAgICAgIHN0ciArPSBtYXJrZXJzW2lkXS5kYXRhO1xuICAgIH1cblxuICAgIHJldHVybiBzdHI7XG4gIH07XG5cbiAgcmV0dXJuIHtcbiAgICBjbG9uZSgpIHtcbiAgICAgIHRocm93IG5ldyBTdHlsZWRFcnJvcig1KTtcbiAgICB9LFxuICAgIGNzcyxcbiAgICBnZXRJZHM6IGdldElkc0Zyb21NYXJrZXJzRmFjdG9yeShtYXJrZXJzKSxcbiAgICBoYXNOYW1lRm9ySWQ6IGhhc05hbWVGb3JJZChuYW1lcyksXG4gICAgaW5zZXJ0TWFya2VyLFxuICAgIGluc2VydFJ1bGVzLFxuICAgIHJlbW92ZVJ1bGVzLFxuICAgIHNlYWxlZDogZmFsc2UsXG4gICAgc3R5bGVUYWc6IGVsLFxuICAgIHRvRWxlbWVudDogd3JhcEFzRWxlbWVudChjc3MsIG5hbWVzKSxcbiAgICB0b0hUTUw6IHdyYXBBc0h0bWxUYWcoY3NzLCBuYW1lcyksXG4gIH07XG59O1xuXG5jb25zdCBtYWtlU2VydmVyVGFnID0gKG5hbWVzQXJnLCBtYXJrZXJzQXJnKTogVGFnPFtzdHJpbmddPiA9PiB7XG4gIGNvbnN0IG5hbWVzID0gbmFtZXNBcmcgPT09IHVuZGVmaW5lZCA/IChPYmplY3QuY3JlYXRlKG51bGwpOiBPYmplY3QpIDogbmFtZXNBcmc7XG4gIGNvbnN0IG1hcmtlcnMgPSBtYXJrZXJzQXJnID09PSB1bmRlZmluZWQgPyBPYmplY3QuY3JlYXRlKG51bGwpIDogbWFya2Vyc0FyZztcblxuICBjb25zdCBpbnNlcnRNYXJrZXIgPSBpZCA9PiB7XG4gICAgY29uc3QgcHJldiA9IG1hcmtlcnNbaWRdO1xuICAgIGlmIChwcmV2ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiBwcmV2O1xuICAgIH1cblxuICAgIHJldHVybiAobWFya2Vyc1tpZF0gPSBbJyddKTtcbiAgfTtcblxuICBjb25zdCBpbnNlcnRSdWxlcyA9IChpZCwgY3NzUnVsZXMsIG5hbWUpID0+IHtcbiAgICBjb25zdCBtYXJrZXIgPSBpbnNlcnRNYXJrZXIoaWQpO1xuICAgIG1hcmtlclswXSArPSBjc3NSdWxlcy5qb2luKCcgJyk7XG4gICAgYWRkTmFtZUZvcklkKG5hbWVzLCBpZCwgbmFtZSk7XG4gIH07XG5cbiAgY29uc3QgcmVtb3ZlUnVsZXMgPSBpZCA9PiB7XG4gICAgY29uc3QgbWFya2VyID0gbWFya2Vyc1tpZF07XG4gICAgaWYgKG1hcmtlciA9PT0gdW5kZWZpbmVkKSByZXR1cm47XG4gICAgbWFya2VyWzBdID0gJyc7XG4gICAgcmVzZXRJZE5hbWVzKG5hbWVzLCBpZCk7XG4gIH07XG5cbiAgY29uc3QgY3NzID0gKCkgPT4ge1xuICAgIGxldCBzdHIgPSAnJztcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZ3VhcmQtZm9yLWluXG4gICAgZm9yIChjb25zdCBpZCBpbiBtYXJrZXJzKSB7XG4gICAgICBjb25zdCBjc3NGb3JJZCA9IG1hcmtlcnNbaWRdWzBdO1xuICAgICAgaWYgKGNzc0ZvcklkKSB7XG4gICAgICAgIHN0ciArPSBtYWtlVGV4dE1hcmtlcihpZCkgKyBjc3NGb3JJZDtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHN0cjtcbiAgfTtcblxuICBjb25zdCBjbG9uZSA9ICgpID0+IHtcbiAgICBjb25zdCBuYW1lc0Nsb25lID0gY2xvbmVOYW1lcyhuYW1lcyk7XG4gICAgY29uc3QgbWFya2Vyc0Nsb25lID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcblxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBndWFyZC1mb3ItaW5cbiAgICBmb3IgKGNvbnN0IGlkIGluIG1hcmtlcnMpIHtcbiAgICAgIG1hcmtlcnNDbG9uZVtpZF0gPSBbbWFya2Vyc1tpZF1bMF1dO1xuICAgIH1cblxuICAgIHJldHVybiBtYWtlU2VydmVyVGFnKG5hbWVzQ2xvbmUsIG1hcmtlcnNDbG9uZSk7XG4gIH07XG5cbiAgY29uc3QgdGFnID0ge1xuICAgIGNsb25lLFxuICAgIGNzcyxcbiAgICBnZXRJZHM6IGdldElkc0Zyb21NYXJrZXJzRmFjdG9yeShtYXJrZXJzKSxcbiAgICBoYXNOYW1lRm9ySWQ6IGhhc05hbWVGb3JJZChuYW1lcyksXG4gICAgaW5zZXJ0TWFya2VyLFxuICAgIGluc2VydFJ1bGVzLFxuICAgIHJlbW92ZVJ1bGVzLFxuICAgIHNlYWxlZDogZmFsc2UsXG4gICAgc3R5bGVUYWc6IG51bGwsXG4gICAgdG9FbGVtZW50OiB3cmFwQXNFbGVtZW50KGNzcywgbmFtZXMpLFxuICAgIHRvSFRNTDogd3JhcEFzSHRtbFRhZyhjc3MsIG5hbWVzKSxcbiAgfTtcblxuICByZXR1cm4gdGFnO1xufTtcblxuZXhwb3J0IGNvbnN0IG1ha2VUYWcgPSAoXG4gIHRhcmdldDogP0hUTUxFbGVtZW50LFxuICB0YWdFbDogP0hUTUxTdHlsZUVsZW1lbnQsXG4gIGZvcmNlU2VydmVyPzogYm9vbGVhbixcbiAgaW5zZXJ0QmVmb3JlPzogYm9vbGVhbixcbiAgZ2V0SW1wb3J0UnVsZVRhZz86ICgpID0+IFRhZzxhbnk+XG4pOiBUYWc8YW55PiA9PiB7XG4gIGlmIChJU19CUk9XU0VSICYmICFmb3JjZVNlcnZlcikge1xuICAgIGNvbnN0IGVsID0gbWFrZVN0eWxlVGFnKHRhcmdldCwgdGFnRWwsIGluc2VydEJlZm9yZSk7XG5cbiAgICBpZiAoRElTQUJMRV9TUEVFRFkpIHtcbiAgICAgIHJldHVybiBtYWtlQnJvd3NlclRhZyhlbCwgZ2V0SW1wb3J0UnVsZVRhZyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBtYWtlU3BlZWR5VGFnKGVsLCBnZXRJbXBvcnRSdWxlVGFnKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWFrZVNlcnZlclRhZygpO1xufTtcblxuZXhwb3J0IGNvbnN0IHJlaHlkcmF0ZSA9IChcbiAgdGFnOiBUYWc8YW55PixcbiAgZWxzOiBIVE1MU3R5bGVFbGVtZW50W10sXG4gIGV4dHJhY3RlZDogRXh0cmFjdGVkQ29tcFtdXG4pOiB2b2lkID0+IHtcbiAgLyogYWRkIGFsbCBleHRyYWN0ZWQgY29tcG9uZW50cyB0byB0aGUgbmV3IHRhZyAqL1xuICBmb3IgKGxldCBpID0gMCwgbGVuID0gZXh0cmFjdGVkLmxlbmd0aDsgaSA8IGxlbjsgaSArPSAxKSB7XG4gICAgY29uc3QgeyBjb21wb25lbnRJZCwgY3NzRnJvbURPTSB9ID0gZXh0cmFjdGVkW2ldO1xuICAgIGNvbnN0IGNzc1J1bGVzID0gc3BsaXRCeVJ1bGVzKGNzc0Zyb21ET00pO1xuICAgIHRhZy5pbnNlcnRSdWxlcyhjb21wb25lbnRJZCwgY3NzUnVsZXMpO1xuICB9XG5cbiAgLyogcmVtb3ZlIG9sZCBIVE1MU3R5bGVFbGVtZW50cywgc2luY2UgdGhleSBoYXZlIGJlZW4gcmVoeWRyYXRlZCAqL1xuICBmb3IgKGxldCBpID0gMCwgbGVuID0gZWxzLmxlbmd0aDsgaSA8IGxlbjsgaSArPSAxKSB7XG4gICAgY29uc3QgZWwgPSBlbHNbaV07XG4gICAgaWYgKGVsLnBhcmVudE5vZGUpIHtcbiAgICAgIGVsLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZWwpO1xuICAgIH1cbiAgfVxufTtcbiIsIi8vIEBmbG93XG5pbXBvcnQgUmVhY3QsIHsgY3JlYXRlQ29udGV4dCwgQ29tcG9uZW50LCB0eXBlIEVsZW1lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgbWVtb2l6ZSBmcm9tICdtZW1vaXplLW9uZSc7XG5pbXBvcnQgU3R5bGVkRXJyb3IgZnJvbSAnLi4vdXRpbHMvZXJyb3InO1xuaW1wb3J0IGlzRnVuY3Rpb24gZnJvbSAnLi4vdXRpbHMvaXNGdW5jdGlvbic7XG5cbmV4cG9ydCB0eXBlIFRoZW1lID0geyBba2V5OiBzdHJpbmddOiBtaXhlZCB9O1xuXG50eXBlIFByb3BzID0ge1xuICBjaGlsZHJlbj86IEVsZW1lbnQ8YW55PixcbiAgdGhlbWU6IFRoZW1lIHwgKChvdXRlclRoZW1lOiBUaGVtZSkgPT4gdm9pZCksXG59O1xuXG5leHBvcnQgY29uc3QgVGhlbWVDb250ZXh0ID0gY3JlYXRlQ29udGV4dCgpO1xuXG5leHBvcnQgY29uc3QgVGhlbWVDb25zdW1lciA9IFRoZW1lQ29udGV4dC5Db25zdW1lcjtcblxuLyoqXG4gKiBQcm92aWRlIGEgdGhlbWUgdG8gYW4gZW50aXJlIHJlYWN0IGNvbXBvbmVudCB0cmVlIHZpYSBjb250ZXh0XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRoZW1lUHJvdmlkZXIgZXh0ZW5kcyBDb21wb25lbnQ8UHJvcHM+IHtcbiAgZ2V0Q29udGV4dDogKHRoZW1lOiBUaGVtZSB8ICgob3V0ZXJUaGVtZTogVGhlbWUpID0+IHZvaWQpLCBvdXRlclRoZW1lPzogVGhlbWUpID0+IFRoZW1lO1xuXG4gIHJlbmRlcklubmVyOiBGdW5jdGlvbjtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogUHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5nZXRDb250ZXh0ID0gbWVtb2l6ZSh0aGlzLmdldENvbnRleHQuYmluZCh0aGlzKSk7XG4gICAgdGhpcy5yZW5kZXJJbm5lciA9IHRoaXMucmVuZGVySW5uZXIuYmluZCh0aGlzKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBpZiAoIXRoaXMucHJvcHMuY2hpbGRyZW4pIHJldHVybiBudWxsO1xuXG4gICAgcmV0dXJuIDxUaGVtZUNvbnRleHQuQ29uc3VtZXI+e3RoaXMucmVuZGVySW5uZXJ9PC9UaGVtZUNvbnRleHQuQ29uc3VtZXI+O1xuICB9XG5cbiAgcmVuZGVySW5uZXIob3V0ZXJUaGVtZT86IFRoZW1lKSB7XG4gICAgY29uc3QgY29udGV4dCA9IHRoaXMuZ2V0Q29udGV4dCh0aGlzLnByb3BzLnRoZW1lLCBvdXRlclRoZW1lKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8VGhlbWVDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXtjb250ZXh0fT5cbiAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICA8L1RoZW1lQ29udGV4dC5Qcm92aWRlcj5cbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgdGhlbWUgZnJvbSB0aGUgcHJvcHMsIHN1cHBvcnRpbmcgYm90aCAob3V0ZXJUaGVtZSkgPT4ge31cbiAgICogYXMgd2VsbCBhcyBvYmplY3Qgbm90YXRpb25cbiAgICovXG4gIGdldFRoZW1lKHRoZW1lOiAob3V0ZXJUaGVtZTogP1RoZW1lKSA9PiB2b2lkLCBvdXRlclRoZW1lOiA/VGhlbWUpIHtcbiAgICBpZiAoaXNGdW5jdGlvbih0aGVtZSkpIHtcbiAgICAgIGNvbnN0IG1lcmdlZFRoZW1lID0gdGhlbWUob3V0ZXJUaGVtZSk7XG5cbiAgICAgIGlmIChcbiAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJlxuICAgICAgICAobWVyZ2VkVGhlbWUgPT09IG51bGwgfHwgQXJyYXkuaXNBcnJheShtZXJnZWRUaGVtZSkgfHwgdHlwZW9mIG1lcmdlZFRoZW1lICE9PSAnb2JqZWN0JylcbiAgICAgICkge1xuICAgICAgICB0aHJvdyBuZXcgU3R5bGVkRXJyb3IoNyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBtZXJnZWRUaGVtZTtcbiAgICB9XG5cbiAgICBpZiAodGhlbWUgPT09IG51bGwgfHwgQXJyYXkuaXNBcnJheSh0aGVtZSkgfHwgdHlwZW9mIHRoZW1lICE9PSAnb2JqZWN0Jykge1xuICAgICAgdGhyb3cgbmV3IFN0eWxlZEVycm9yKDgpO1xuICAgIH1cblxuICAgIHJldHVybiB7IC4uLm91dGVyVGhlbWUsIC4uLnRoZW1lIH07XG4gIH1cblxuICBnZXRDb250ZXh0KHRoZW1lOiAob3V0ZXJUaGVtZTogP1RoZW1lKSA9PiB2b2lkLCBvdXRlclRoZW1lPzogVGhlbWUpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRUaGVtZSh0aGVtZSwgb3V0ZXJUaGVtZSk7XG4gIH1cbn1cbiIsIi8vIEBmbG93XG5pbXBvcnQgUmVhY3QsIHsgY3JlYXRlQ29udGV4dCwgQ29tcG9uZW50LCB0eXBlIEVsZW1lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IG1lbW9pemUgZnJvbSAnbWVtb2l6ZS1vbmUnO1xuaW1wb3J0IFN0eWxlU2hlZXQgZnJvbSAnLi9TdHlsZVNoZWV0JztcbmltcG9ydCBTZXJ2ZXJTdHlsZVNoZWV0IGZyb20gJy4vU2VydmVyU3R5bGVTaGVldCc7XG5pbXBvcnQgU3R5bGVkRXJyb3IgZnJvbSAnLi4vdXRpbHMvZXJyb3InO1xuXG50eXBlIFByb3BzID0ge1xuICBjaGlsZHJlbj86IEVsZW1lbnQ8YW55PixcbiAgc2hlZXQ/OiBTdHlsZVNoZWV0LFxuICB0YXJnZXQ/OiBIVE1MRWxlbWVudCxcbn07XG5cbmV4cG9ydCBjb25zdCBTdHlsZVNoZWV0Q29udGV4dCA9IGNyZWF0ZUNvbnRleHQoKTtcbmV4cG9ydCBjb25zdCBTdHlsZVNoZWV0Q29uc3VtZXIgPSBTdHlsZVNoZWV0Q29udGV4dC5Db25zdW1lcjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3R5bGVTaGVldE1hbmFnZXIgZXh0ZW5kcyBDb21wb25lbnQ8UHJvcHM+IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBzaGVldDogUHJvcFR5cGVzLm9uZU9mVHlwZShbXG4gICAgICBQcm9wVHlwZXMuaW5zdGFuY2VPZihTdHlsZVNoZWV0KSxcbiAgICAgIFByb3BUeXBlcy5pbnN0YW5jZU9mKFNlcnZlclN0eWxlU2hlZXQpLFxuICAgIF0pLFxuXG4gICAgdGFyZ2V0OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgYXBwZW5kQ2hpbGQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgfSksXG4gIH07XG5cbiAgZ2V0Q29udGV4dDogKHNoZWV0OiA/U3R5bGVTaGVldCwgdGFyZ2V0OiA/SFRNTEVsZW1lbnQpID0+IFN0eWxlU2hlZXQ7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IFByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuZ2V0Q29udGV4dCA9IG1lbW9pemUodGhpcy5nZXRDb250ZXh0KTtcbiAgfVxuXG4gIGdldENvbnRleHQoc2hlZXQ6ID9TdHlsZVNoZWV0LCB0YXJnZXQ6ID9IVE1MRWxlbWVudCkge1xuICAgIGlmIChzaGVldCkge1xuICAgICAgcmV0dXJuIHNoZWV0O1xuICAgIH0gZWxzZSBpZiAodGFyZ2V0KSB7XG4gICAgICByZXR1cm4gbmV3IFN0eWxlU2hlZXQodGFyZ2V0KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IFN0eWxlZEVycm9yKDQpO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNoaWxkcmVuLCBzaGVldCwgdGFyZ2V0IH0gPSB0aGlzLnByb3BzO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxTdHlsZVNoZWV0Q29udGV4dC5Qcm92aWRlciB2YWx1ZT17dGhpcy5nZXRDb250ZXh0KHNoZWV0LCB0YXJnZXQpfT5cbiAgICAgICAge3Byb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBSZWFjdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKSA6IGNoaWxkcmVufVxuICAgICAgPC9TdHlsZVNoZWV0Q29udGV4dC5Qcm92aWRlcj5cbiAgICApO1xuICB9XG59XG4iLCIvLyBAZmxvd1xuaW1wb3J0IHZhbGlkQXR0ciBmcm9tICdAZW1vdGlvbi9pcy1wcm9wLXZhbGlkJztcbmltcG9ydCBtZXJnZSBmcm9tICdtZXJnZS1hbnl0aGluZyc7XG5pbXBvcnQgUmVhY3QsIHsgY3JlYXRlRWxlbWVudCwgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IENvbXBvbmVudFN0eWxlIGZyb20gJy4vQ29tcG9uZW50U3R5bGUnO1xuaW1wb3J0IGNyZWF0ZVdhcm5Ub29NYW55Q2xhc3NlcyBmcm9tICcuLi91dGlscy9jcmVhdGVXYXJuVG9vTWFueUNsYXNzZXMnO1xuaW1wb3J0IGRldGVybWluZVRoZW1lIGZyb20gJy4uL3V0aWxzL2RldGVybWluZVRoZW1lJztcbmltcG9ydCBlc2NhcGUgZnJvbSAnLi4vdXRpbHMvZXNjYXBlJztcbmltcG9ydCBnZW5lcmF0ZURpc3BsYXlOYW1lIGZyb20gJy4uL3V0aWxzL2dlbmVyYXRlRGlzcGxheU5hbWUnO1xuaW1wb3J0IGdldENvbXBvbmVudE5hbWUgZnJvbSAnLi4vdXRpbHMvZ2V0Q29tcG9uZW50TmFtZSc7XG5pbXBvcnQgaG9pc3QgZnJvbSAnLi4vdXRpbHMvaG9pc3QnO1xuaW1wb3J0IGlzRnVuY3Rpb24gZnJvbSAnLi4vdXRpbHMvaXNGdW5jdGlvbic7XG5pbXBvcnQgaXNUYWcgZnJvbSAnLi4vdXRpbHMvaXNUYWcnO1xuaW1wb3J0IGlzRGVyaXZlZFJlYWN0Q29tcG9uZW50IGZyb20gJy4uL3V0aWxzL2lzRGVyaXZlZFJlYWN0Q29tcG9uZW50JztcbmltcG9ydCBpc1N0eWxlZENvbXBvbmVudCBmcm9tICcuLi91dGlscy9pc1N0eWxlZENvbXBvbmVudCc7XG5pbXBvcnQgb25jZSBmcm9tICcuLi91dGlscy9vbmNlJztcbmltcG9ydCBTdHlsZVNoZWV0IGZyb20gJy4vU3R5bGVTaGVldCc7XG5pbXBvcnQgeyBUaGVtZUNvbnN1bWVyLCB0eXBlIFRoZW1lIH0gZnJvbSAnLi9UaGVtZVByb3ZpZGVyJztcbmltcG9ydCB7IFN0eWxlU2hlZXRDb25zdW1lciB9IGZyb20gJy4vU3R5bGVTaGVldE1hbmFnZXInO1xuaW1wb3J0IHsgRU1QVFlfQVJSQVksIEVNUFRZX09CSkVDVCB9IGZyb20gJy4uL3V0aWxzL2VtcHRpZXMnO1xuXG5pbXBvcnQgdHlwZSB7IEF0dHJzLCBSdWxlU2V0LCBUYXJnZXQgfSBmcm9tICcuLi90eXBlcyc7XG5cbmNvbnN0IGlkZW50aWZpZXJzID0ge307XG5cbi8qIFdlIGRlcGVuZCBvbiBjb21wb25lbnRzIGhhdmluZyB1bmlxdWUgSURzICovXG5mdW5jdGlvbiBnZW5lcmF0ZUlkKF9Db21wb25lbnRTdHlsZTogRnVuY3Rpb24sIF9kaXNwbGF5TmFtZTogc3RyaW5nLCBwYXJlbnRDb21wb25lbnRJZDogc3RyaW5nKSB7XG4gIGNvbnN0IGRpc3BsYXlOYW1lID0gdHlwZW9mIF9kaXNwbGF5TmFtZSAhPT0gJ3N0cmluZycgPyAnc2MnIDogZXNjYXBlKF9kaXNwbGF5TmFtZSk7XG5cbiAgLyoqXG4gICAqIFRoaXMgZW5zdXJlcyB1bmlxdWVuZXNzIGlmIHR3byBjb21wb25lbnRzIGhhcHBlbiB0byBzaGFyZVxuICAgKiB0aGUgc2FtZSBkaXNwbGF5TmFtZS5cbiAgICovXG4gIGNvbnN0IG5yID0gKGlkZW50aWZpZXJzW2Rpc3BsYXlOYW1lXSB8fCAwKSArIDE7XG4gIGlkZW50aWZpZXJzW2Rpc3BsYXlOYW1lXSA9IG5yO1xuXG4gIGNvbnN0IGNvbXBvbmVudElkID0gYCR7ZGlzcGxheU5hbWV9LSR7X0NvbXBvbmVudFN0eWxlLmdlbmVyYXRlTmFtZShkaXNwbGF5TmFtZSArIG5yKX1gO1xuXG4gIHJldHVybiBwYXJlbnRDb21wb25lbnRJZCA/IGAke3BhcmVudENvbXBvbmVudElkfS0ke2NvbXBvbmVudElkfWAgOiBjb21wb25lbnRJZDtcbn1cblxuLy8gJEZsb3dGaXhNZVxuY2xhc3MgU3R5bGVkQ29tcG9uZW50IGV4dGVuZHMgQ29tcG9uZW50PCo+IHtcbiAgcmVuZGVyT3V0ZXI6IEZ1bmN0aW9uO1xuXG4gIHJlbmRlcklubmVyOiBGdW5jdGlvbjtcblxuICBzdHlsZVNoZWV0OiA/U3R5bGVTaGVldDtcblxuICB3YXJuSW5uZXJSZWY6IEZ1bmN0aW9uO1xuXG4gIHdhcm5BdHRyc0ZuT2JqZWN0S2V5RGVwcmVjYXRlZDogRnVuY3Rpb247XG5cbiAgd2Fybk5vblN0eWxlZENvbXBvbmVudEF0dHJzT2JqZWN0S2V5OiBGdW5jdGlvbjtcblxuICBhdHRycyA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgdGhpcy5yZW5kZXJPdXRlciA9IHRoaXMucmVuZGVyT3V0ZXIuYmluZCh0aGlzKTtcbiAgICB0aGlzLnJlbmRlcklubmVyID0gdGhpcy5yZW5kZXJJbm5lci5iaW5kKHRoaXMpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHRoaXMud2FybklubmVyUmVmID0gb25jZShkaXNwbGF5TmFtZSA9PlxuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tY29uc29sZVxuICAgICAgICBjb25zb2xlLndhcm4oXG4gICAgICAgICAgYFRoZSBcImlubmVyUmVmXCIgQVBJIGhhcyBiZWVuIHJlbW92ZWQgaW4gc3R5bGVkLWNvbXBvbmVudHMgdjQgaW4gZmF2b3Igb2YgUmVhY3QgMTYgcmVmIGZvcndhcmRpbmcsIHVzZSBcInJlZlwiIGluc3RlYWQgbGlrZSBhIHR5cGljYWwgY29tcG9uZW50LiBcImlubmVyUmVmXCIgd2FzIGRldGVjdGVkIG9uIGNvbXBvbmVudCBcIiR7ZGlzcGxheU5hbWV9XCIuYFxuICAgICAgICApXG4gICAgICApO1xuXG4gICAgICB0aGlzLndhcm5BdHRyc0ZuT2JqZWN0S2V5RGVwcmVjYXRlZCA9IG9uY2UoXG4gICAgICAgIChrZXksIGRpc3BsYXlOYW1lKTogdm9pZCA9PlxuICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1jb25zb2xlXG4gICAgICAgICAgY29uc29sZS53YXJuKFxuICAgICAgICAgICAgYEZ1bmN0aW9ucyBhcyBvYmplY3QtZm9ybSBhdHRycyh7fSkga2V5cyBhcmUgbm93IGRlcHJlY2F0ZWQgYW5kIHdpbGwgYmUgcmVtb3ZlZCBpbiBhIGZ1dHVyZSB2ZXJzaW9uIG9mIHN0eWxlZC1jb21wb25lbnRzLiBTd2l0Y2ggdG8gdGhlIG5ldyBhdHRycyhwcm9wcyA9PiAoe30pKSBzeW50YXggaW5zdGVhZCBmb3IgZWFzaWVyIGFuZCBtb3JlIHBvd2VyZnVsIGNvbXBvc2l0aW9uLiBUaGUgYXR0cnMga2V5IGluIHF1ZXN0aW9uIGlzIFwiJHtrZXl9XCIgb24gY29tcG9uZW50IFwiJHtkaXNwbGF5TmFtZX1cIi5gLFxuICAgICAgICAgICAgYFxcbiAke25ldyBFcnJvcigpLnN0YWNrfWBcbiAgICAgICAgICApXG4gICAgICApO1xuXG4gICAgICB0aGlzLndhcm5Ob25TdHlsZWRDb21wb25lbnRBdHRyc09iamVjdEtleSA9IG9uY2UoXG4gICAgICAgIChrZXksIGRpc3BsYXlOYW1lKTogdm9pZCA9PlxuICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1jb25zb2xlXG4gICAgICAgICAgY29uc29sZS53YXJuKFxuICAgICAgICAgICAgYEl0IGxvb2tzIGxpa2UgeW91J3ZlIHVzZWQgYSBub24gc3R5bGVkLWNvbXBvbmVudCBhcyB0aGUgdmFsdWUgZm9yIHRoZSBcIiR7a2V5fVwiIHByb3AgaW4gYW4gb2JqZWN0LWZvcm0gYXR0cnMgY29uc3RydWN0b3Igb2YgXCIke2Rpc3BsYXlOYW1lfVwiLlxcbmAgK1xuICAgICAgICAgICAgICAnWW91IHNob3VsZCB1c2UgdGhlIG5ldyBmdW5jdGlvbi1mb3JtIGF0dHJzIGNvbnN0cnVjdG9yIHdoaWNoIGF2b2lkcyB0aGlzIGlzc3VlOiBhdHRycyhwcm9wcyA9PiAoeyB5b3VyU3R1ZmYgfSkpXFxuJyArXG4gICAgICAgICAgICAgIFwiVG8gY29udGludWUgdXNpbmcgdGhlIGRlcHJlY2F0ZWQgb2JqZWN0IHN5bnRheCwgeW91J2xsIG5lZWQgdG8gd3JhcCB5b3VyIGNvbXBvbmVudCBwcm9wIGluIGEgZnVuY3Rpb24gdG8gbWFrZSBpdCBhdmFpbGFibGUgaW5zaWRlIHRoZSBzdHlsZWQgY29tcG9uZW50ICh5b3UnbGwgc3RpbGwgZ2V0IHRoZSBkZXByZWNhdGlvbiB3YXJuaW5nIHRob3VnaC4pXFxuXCIgK1xuICAgICAgICAgICAgICBgRm9yIGV4YW1wbGUsIHsgJHtrZXl9OiAoKSA9PiBJbm5lckNvbXBvbmVudCB9IGluc3RlYWQgb2YgeyAke2tleX06IElubmVyQ29tcG9uZW50IH1gXG4gICAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIDxTdHlsZVNoZWV0Q29uc3VtZXI+e3RoaXMucmVuZGVyT3V0ZXJ9PC9TdHlsZVNoZWV0Q29uc3VtZXI+O1xuICB9XG5cbiAgcmVuZGVyT3V0ZXIoc3R5bGVTaGVldD86IFN0eWxlU2hlZXQgPSBTdHlsZVNoZWV0Lm1hc3Rlcikge1xuICAgIHRoaXMuc3R5bGVTaGVldCA9IHN0eWxlU2hlZXQ7XG5cbiAgICAvLyBObyBuZWVkIHRvIHN1YnNjcmliZSBhIHN0YXRpYyBjb21wb25lbnQgdG8gdGhlbWUgY2hhbmdlcywgaXQgd29uJ3QgY2hhbmdlIGFueXRoaW5nXG4gICAgaWYgKHRoaXMucHJvcHMuZm9yd2FyZGVkQ29tcG9uZW50LmNvbXBvbmVudFN0eWxlLmlzU3RhdGljKSByZXR1cm4gdGhpcy5yZW5kZXJJbm5lcigpO1xuXG4gICAgcmV0dXJuIDxUaGVtZUNvbnN1bWVyPnt0aGlzLnJlbmRlcklubmVyfTwvVGhlbWVDb25zdW1lcj47XG4gIH1cblxuICByZW5kZXJJbm5lcih0aGVtZT86IFRoZW1lKSB7XG4gICAgY29uc3Qge1xuICAgICAgY29tcG9uZW50U3R5bGUsXG4gICAgICBkZWZhdWx0UHJvcHMsXG4gICAgICBkaXNwbGF5TmFtZSxcbiAgICAgIGZvbGRlZENvbXBvbmVudElkcyxcbiAgICAgIHN0eWxlZENvbXBvbmVudElkLFxuICAgICAgdGFyZ2V0LFxuICAgIH0gPSB0aGlzLnByb3BzLmZvcndhcmRlZENvbXBvbmVudDtcblxuICAgIGxldCBnZW5lcmF0ZWRDbGFzc05hbWU7XG4gICAgaWYgKGNvbXBvbmVudFN0eWxlLmlzU3RhdGljKSB7XG4gICAgICBnZW5lcmF0ZWRDbGFzc05hbWUgPSB0aGlzLmdlbmVyYXRlQW5kSW5qZWN0U3R5bGVzKEVNUFRZX09CSkVDVCwgdGhpcy5wcm9wcyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGdlbmVyYXRlZENsYXNzTmFtZSA9IHRoaXMuZ2VuZXJhdGVBbmRJbmplY3RTdHlsZXMoXG4gICAgICAgIGRldGVybWluZVRoZW1lKHRoaXMucHJvcHMsIHRoZW1lLCBkZWZhdWx0UHJvcHMpIHx8IEVNUFRZX09CSkVDVCxcbiAgICAgICAgdGhpcy5wcm9wc1xuICAgICAgKTtcbiAgICB9XG5cbiAgICBjb25zdCBlbGVtZW50VG9CZUNyZWF0ZWQgPSB0aGlzLnByb3BzLmFzIHx8IHRoaXMuYXR0cnMuYXMgfHwgdGFyZ2V0O1xuICAgIGNvbnN0IGlzVGFyZ2V0VGFnID0gaXNUYWcoZWxlbWVudFRvQmVDcmVhdGVkKTtcblxuICAgIGNvbnN0IHByb3BzRm9yRWxlbWVudCA9IHt9O1xuICAgIGNvbnN0IGNvbXB1dGVkUHJvcHMgPSB7IC4uLnRoaXMucHJvcHMsIC4uLnRoaXMuYXR0cnMgfTtcblxuICAgIGxldCBrZXk7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGd1YXJkLWZvci1pblxuICAgIGZvciAoa2V5IGluIGNvbXB1dGVkUHJvcHMpIHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmIGtleSA9PT0gJ2lubmVyUmVmJyAmJiBpc1RhcmdldFRhZykge1xuICAgICAgICB0aGlzLndhcm5Jbm5lclJlZihkaXNwbGF5TmFtZSk7XG4gICAgICB9XG5cbiAgICAgIGlmIChrZXkgPT09ICdmb3J3YXJkZWRDb21wb25lbnQnIHx8IGtleSA9PT0gJ2FzJykge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSAnZm9yd2FyZGVkUmVmJykgcHJvcHNGb3JFbGVtZW50LnJlZiA9IGNvbXB1dGVkUHJvcHNba2V5XTtcbiAgICAgIGVsc2UgaWYgKGtleSA9PT0gJ2ZvcndhcmRlZEFzJykgcHJvcHNGb3JFbGVtZW50LmFzID0gY29tcHV0ZWRQcm9wc1trZXldO1xuICAgICAgZWxzZSBpZiAoIWlzVGFyZ2V0VGFnIHx8IHZhbGlkQXR0cihrZXkpKSB7XG4gICAgICAgIC8vIERvbid0IHBhc3MgdGhyb3VnaCBub24gSFRNTCB0YWdzIHRocm91Z2ggdG8gSFRNTCBlbGVtZW50c1xuICAgICAgICBwcm9wc0ZvckVsZW1lbnRba2V5XSA9IGNvbXB1dGVkUHJvcHNba2V5XTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodGhpcy5wcm9wcy5zdHlsZSAmJiB0aGlzLmF0dHJzLnN0eWxlKSB7XG4gICAgICBwcm9wc0ZvckVsZW1lbnQuc3R5bGUgPSB7IC4uLnRoaXMuYXR0cnMuc3R5bGUsIC4uLnRoaXMucHJvcHMuc3R5bGUgfTtcbiAgICB9XG5cbiAgICBwcm9wc0ZvckVsZW1lbnQuY2xhc3NOYW1lID0gQXJyYXkucHJvdG90eXBlXG4gICAgICAuY29uY2F0KFxuICAgICAgICBmb2xkZWRDb21wb25lbnRJZHMsXG4gICAgICAgIHN0eWxlZENvbXBvbmVudElkLFxuICAgICAgICBnZW5lcmF0ZWRDbGFzc05hbWUgIT09IHN0eWxlZENvbXBvbmVudElkID8gZ2VuZXJhdGVkQ2xhc3NOYW1lIDogbnVsbCxcbiAgICAgICAgdGhpcy5wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgIHRoaXMuYXR0cnMuY2xhc3NOYW1lXG4gICAgICApXG4gICAgICAuZmlsdGVyKEJvb2xlYW4pXG4gICAgICAuam9pbignICcpO1xuXG4gICAgcmV0dXJuIGNyZWF0ZUVsZW1lbnQoZWxlbWVudFRvQmVDcmVhdGVkLCBwcm9wc0ZvckVsZW1lbnQpO1xuICB9XG5cbiAgYnVpbGRFeGVjdXRpb25Db250ZXh0KHRoZW1lOiA/T2JqZWN0LCBwcm9wczogT2JqZWN0LCBhdHRyczogQXR0cnMpIHtcbiAgICBjb25zdCBjb250ZXh0ID0geyAuLi5wcm9wcywgdGhlbWUgfTtcblxuICAgIGlmICghYXR0cnMubGVuZ3RoKSByZXR1cm4gY29udGV4dDtcblxuICAgIHRoaXMuYXR0cnMgPSB7fTtcblxuICAgIGF0dHJzLmZvckVhY2goYXR0ckRlZiA9PiB7XG4gICAgICBsZXQgcmVzb2x2ZWRBdHRyRGVmID0gYXR0ckRlZjtcbiAgICAgIGxldCBhdHRyRGVmV2FzRm4gPSBmYWxzZTtcbiAgICAgIGxldCBhdHRyO1xuICAgICAgbGV0IGtleTtcblxuICAgICAgaWYgKGlzRnVuY3Rpb24ocmVzb2x2ZWRBdHRyRGVmKSkge1xuICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgIHJlc29sdmVkQXR0ckRlZiA9IHJlc29sdmVkQXR0ckRlZihjb250ZXh0KTtcbiAgICAgICAgYXR0ckRlZldhc0ZuID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgLyogZXNsaW50LWRpc2FibGUgZ3VhcmQtZm9yLWluICovXG4gICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICBmb3IgKGtleSBpbiByZXNvbHZlZEF0dHJEZWYpIHtcbiAgICAgICAgYXR0ciA9IHJlc29sdmVkQXR0ckRlZltrZXldO1xuXG4gICAgICAgIGlmICghYXR0ckRlZldhc0ZuKSB7XG4gICAgICAgICAgaWYgKGlzRnVuY3Rpb24oYXR0cikgJiYgIWlzRGVyaXZlZFJlYWN0Q29tcG9uZW50KGF0dHIpICYmICFpc1N0eWxlZENvbXBvbmVudChhdHRyKSkge1xuICAgICAgICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgICAgICAgdGhpcy53YXJuQXR0cnNGbk9iamVjdEtleURlcHJlY2F0ZWQoa2V5LCBwcm9wcy5mb3J3YXJkZWRDb21wb25lbnQuZGlzcGxheU5hbWUpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBhdHRyID0gYXR0cihjb250ZXh0KTtcblxuICAgICAgICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgUmVhY3QuaXNWYWxpZEVsZW1lbnQoYXR0cikpIHtcbiAgICAgICAgICAgICAgdGhpcy53YXJuTm9uU3R5bGVkQ29tcG9uZW50QXR0cnNPYmplY3RLZXkoa2V5LCBwcm9wcy5mb3J3YXJkZWRDb21wb25lbnQuZGlzcGxheU5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYXR0cnNba2V5XSA9IGF0dHI7XG4gICAgICAgIGNvbnRleHRba2V5XSA9IGF0dHI7XG4gICAgICB9XG4gICAgICAvKiBlc2xpbnQtZW5hYmxlICovXG4gICAgfSk7XG5cbiAgICByZXR1cm4gY29udGV4dDtcbiAgfVxuXG4gIGdlbmVyYXRlQW5kSW5qZWN0U3R5bGVzKHRoZW1lOiBhbnksIHByb3BzOiBhbnkpIHtcbiAgICBjb25zdCB7IGF0dHJzLCBjb21wb25lbnRTdHlsZSwgd2FyblRvb01hbnlDbGFzc2VzIH0gPSBwcm9wcy5mb3J3YXJkZWRDb21wb25lbnQ7XG5cbiAgICAvLyBzdGF0aWNhbGx5IHN0eWxlZC1jb21wb25lbnRzIGRvbid0IG5lZWQgdG8gYnVpbGQgYW4gZXhlY3V0aW9uIGNvbnRleHQgb2JqZWN0LFxuICAgIC8vIGFuZCBzaG91bGRuJ3QgYmUgaW5jcmVhc2luZyB0aGUgbnVtYmVyIG9mIGNsYXNzIG5hbWVzXG4gICAgaWYgKGNvbXBvbmVudFN0eWxlLmlzU3RhdGljICYmICFhdHRycy5sZW5ndGgpIHtcbiAgICAgIHJldHVybiBjb21wb25lbnRTdHlsZS5nZW5lcmF0ZUFuZEluamVjdFN0eWxlcyhFTVBUWV9PQkpFQ1QsIHRoaXMuc3R5bGVTaGVldCk7XG4gICAgfVxuXG4gICAgY29uc3QgY2xhc3NOYW1lID0gY29tcG9uZW50U3R5bGUuZ2VuZXJhdGVBbmRJbmplY3RTdHlsZXMoXG4gICAgICB0aGlzLmJ1aWxkRXhlY3V0aW9uQ29udGV4dCh0aGVtZSwgcHJvcHMsIGF0dHJzKSxcbiAgICAgIHRoaXMuc3R5bGVTaGVldFxuICAgICk7XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiB3YXJuVG9vTWFueUNsYXNzZXMpIHdhcm5Ub29NYW55Q2xhc3NlcyhjbGFzc05hbWUpO1xuXG4gICAgcmV0dXJuIGNsYXNzTmFtZTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBjcmVhdGVTdHlsZWRDb21wb25lbnQodGFyZ2V0OiBUYXJnZXQsIG9wdGlvbnM6IE9iamVjdCwgcnVsZXM6IFJ1bGVTZXQpIHtcbiAgY29uc3QgaXNUYXJnZXRTdHlsZWRDb21wID0gaXNTdHlsZWRDb21wb25lbnQodGFyZ2V0KTtcbiAgY29uc3QgaXNDbGFzcyA9ICFpc1RhZyh0YXJnZXQpO1xuXG4gIGNvbnN0IHtcbiAgICBkaXNwbGF5TmFtZSA9IGdlbmVyYXRlRGlzcGxheU5hbWUodGFyZ2V0KSxcbiAgICBjb21wb25lbnRJZCA9IGdlbmVyYXRlSWQoQ29tcG9uZW50U3R5bGUsIG9wdGlvbnMuZGlzcGxheU5hbWUsIG9wdGlvbnMucGFyZW50Q29tcG9uZW50SWQpLFxuICAgIFBhcmVudENvbXBvbmVudCA9IFN0eWxlZENvbXBvbmVudCxcbiAgICBhdHRycyA9IEVNUFRZX0FSUkFZLFxuICB9ID0gb3B0aW9ucztcblxuICBjb25zdCBzdHlsZWRDb21wb25lbnRJZCA9XG4gICAgb3B0aW9ucy5kaXNwbGF5TmFtZSAmJiBvcHRpb25zLmNvbXBvbmVudElkXG4gICAgICA/IGAke2VzY2FwZShvcHRpb25zLmRpc3BsYXlOYW1lKX0tJHtvcHRpb25zLmNvbXBvbmVudElkfWBcbiAgICAgIDogb3B0aW9ucy5jb21wb25lbnRJZCB8fCBjb21wb25lbnRJZDtcblxuICAvLyBmb2xkIHRoZSB1bmRlcmx5aW5nIFN0eWxlZENvbXBvbmVudCBhdHRycyB1cCAoaW1wbGljaXQgZXh0ZW5kKVxuICBjb25zdCBmaW5hbEF0dHJzID1cbiAgICAvLyAkRmxvd0ZpeE1lXG4gICAgaXNUYXJnZXRTdHlsZWRDb21wICYmIHRhcmdldC5hdHRyc1xuICAgICAgPyBBcnJheS5wcm90b3R5cGUuY29uY2F0KHRhcmdldC5hdHRycywgYXR0cnMpLmZpbHRlcihCb29sZWFuKVxuICAgICAgOiBhdHRycztcblxuICBjb25zdCBjb21wb25lbnRTdHlsZSA9IG5ldyBDb21wb25lbnRTdHlsZShcbiAgICBpc1RhcmdldFN0eWxlZENvbXBcbiAgICAgID8gLy8gZm9sZCB0aGUgdW5kZXJseWluZyBTdHlsZWRDb21wb25lbnQgcnVsZXMgdXAgKGltcGxpY2l0IGV4dGVuZClcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICB0YXJnZXQuY29tcG9uZW50U3R5bGUucnVsZXMuY29uY2F0KHJ1bGVzKVxuICAgICAgOiBydWxlcyxcbiAgICBmaW5hbEF0dHJzLFxuICAgIHN0eWxlZENvbXBvbmVudElkXG4gICk7XG5cbiAgLyoqXG4gICAqIGZvcndhcmRSZWYgY3JlYXRlcyBhIG5ldyBpbnRlcmltIGNvbXBvbmVudCwgd2hpY2ggd2UnbGwgdGFrZSBhZHZhbnRhZ2Ugb2ZcbiAgICogaW5zdGVhZCBvZiBleHRlbmRpbmcgUGFyZW50Q29tcG9uZW50IHRvIGNyZWF0ZSBfYW5vdGhlcl8gaW50ZXJpbSBjbGFzc1xuICAgKi9cbiAgbGV0IFdyYXBwZWRTdHlsZWRDb21wb25lbnQ7XG4gIGNvbnN0IGZvcndhcmRSZWYgPSAocHJvcHMsIHJlZikgPT4gKFxuICAgIDxQYXJlbnRDb21wb25lbnQgey4uLnByb3BzfSBmb3J3YXJkZWRDb21wb25lbnQ9e1dyYXBwZWRTdHlsZWRDb21wb25lbnR9IGZvcndhcmRlZFJlZj17cmVmfSAvPlxuICApO1xuICBmb3J3YXJkUmVmLmRpc3BsYXlOYW1lID0gZGlzcGxheU5hbWU7XG4gIFdyYXBwZWRTdHlsZWRDb21wb25lbnQgPSBSZWFjdC5mb3J3YXJkUmVmKGZvcndhcmRSZWYpO1xuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LmRpc3BsYXlOYW1lID0gZGlzcGxheU5hbWU7XG5cbiAgLy8gJEZsb3dGaXhNZVxuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LmF0dHJzID0gZmluYWxBdHRycztcbiAgLy8gJEZsb3dGaXhNZVxuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LmNvbXBvbmVudFN0eWxlID0gY29tcG9uZW50U3R5bGU7XG5cbiAgLy8gJEZsb3dGaXhNZVxuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LmZvbGRlZENvbXBvbmVudElkcyA9IGlzVGFyZ2V0U3R5bGVkQ29tcFxuICAgID8gLy8gJEZsb3dGaXhNZVxuICAgICAgQXJyYXkucHJvdG90eXBlLmNvbmNhdCh0YXJnZXQuZm9sZGVkQ29tcG9uZW50SWRzLCB0YXJnZXQuc3R5bGVkQ29tcG9uZW50SWQpXG4gICAgOiBFTVBUWV9BUlJBWTtcblxuICAvLyAkRmxvd0ZpeE1lXG4gIFdyYXBwZWRTdHlsZWRDb21wb25lbnQuc3R5bGVkQ29tcG9uZW50SWQgPSBzdHlsZWRDb21wb25lbnRJZDtcblxuICAvLyBmb2xkIHRoZSB1bmRlcmx5aW5nIFN0eWxlZENvbXBvbmVudCB0YXJnZXQgdXAgc2luY2Ugd2UgZm9sZGVkIHRoZSBzdHlsZXNcbiAgLy8gJEZsb3dGaXhNZVxuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LnRhcmdldCA9IGlzVGFyZ2V0U3R5bGVkQ29tcCA/IHRhcmdldC50YXJnZXQgOiB0YXJnZXQ7XG5cbiAgLy8gJEZsb3dGaXhNZVxuICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50LndpdGhDb21wb25lbnQgPSBmdW5jdGlvbiB3aXRoQ29tcG9uZW50KHRhZzogVGFyZ2V0KSB7XG4gICAgY29uc3QgeyBjb21wb25lbnRJZDogcHJldmlvdXNDb21wb25lbnRJZCwgLi4ub3B0aW9uc1RvQ29weSB9ID0gb3B0aW9ucztcblxuICAgIGNvbnN0IG5ld0NvbXBvbmVudElkID1cbiAgICAgIHByZXZpb3VzQ29tcG9uZW50SWQgJiZcbiAgICAgIGAke3ByZXZpb3VzQ29tcG9uZW50SWR9LSR7aXNUYWcodGFnKSA/IHRhZyA6IGVzY2FwZShnZXRDb21wb25lbnROYW1lKHRhZykpfWA7XG5cbiAgICBjb25zdCBuZXdPcHRpb25zID0ge1xuICAgICAgLi4ub3B0aW9uc1RvQ29weSxcbiAgICAgIGF0dHJzOiBmaW5hbEF0dHJzLFxuICAgICAgY29tcG9uZW50SWQ6IG5ld0NvbXBvbmVudElkLFxuICAgICAgUGFyZW50Q29tcG9uZW50LFxuICAgIH07XG5cbiAgICByZXR1cm4gY3JlYXRlU3R5bGVkQ29tcG9uZW50KHRhZywgbmV3T3B0aW9ucywgcnVsZXMpO1xuICB9O1xuXG4gIC8vICRGbG93Rml4TWVcbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KFdyYXBwZWRTdHlsZWRDb21wb25lbnQsICdkZWZhdWx0UHJvcHMnLCB7XG4gICAgZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2ZvbGRlZERlZmF1bHRQcm9wcztcbiAgICB9LFxuXG4gICAgc2V0KG9iaikge1xuICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgdGhpcy5fZm9sZGVkRGVmYXVsdFByb3BzID0gaXNUYXJnZXRTdHlsZWRDb21wID8gbWVyZ2UodGFyZ2V0LmRlZmF1bHRQcm9wcywgb2JqKSA6IG9iajtcbiAgICB9LFxuICB9KTtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIC8vICRGbG93Rml4TWVcbiAgICBXcmFwcGVkU3R5bGVkQ29tcG9uZW50Lndhcm5Ub29NYW55Q2xhc3NlcyA9IGNyZWF0ZVdhcm5Ub29NYW55Q2xhc3NlcyhkaXNwbGF5TmFtZSk7XG4gIH1cblxuICAvLyAkRmxvd0ZpeE1lXG4gIFdyYXBwZWRTdHlsZWRDb21wb25lbnQudG9TdHJpbmcgPSAoKSA9PiBgLiR7V3JhcHBlZFN0eWxlZENvbXBvbmVudC5zdHlsZWRDb21wb25lbnRJZH1gO1xuXG4gIGlmIChpc0NsYXNzKSB7XG4gICAgaG9pc3QoV3JhcHBlZFN0eWxlZENvbXBvbmVudCwgdGFyZ2V0LCB7XG4gICAgICAvLyBhbGwgU0Mtc3BlY2lmaWMgdGhpbmdzIHNob3VsZCBub3QgYmUgaG9pc3RlZFxuICAgICAgYXR0cnM6IHRydWUsXG4gICAgICBjb21wb25lbnRTdHlsZTogdHJ1ZSxcbiAgICAgIGRpc3BsYXlOYW1lOiB0cnVlLFxuICAgICAgZm9sZGVkQ29tcG9uZW50SWRzOiB0cnVlLFxuICAgICAgc3R5bGVkQ29tcG9uZW50SWQ6IHRydWUsXG4gICAgICB0YXJnZXQ6IHRydWUsXG4gICAgICB3aXRoQ29tcG9uZW50OiB0cnVlLFxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIFdyYXBwZWRTdHlsZWRDb21wb25lbnQ7XG59XG4iLCIvLyBAZmxvd1xuaW1wb3J0IHsgRU1QVFlfQVJSQVkgfSBmcm9tICcuLi91dGlscy9lbXB0aWVzJztcbmltcG9ydCBmbGF0dGVuIGZyb20gJy4uL3V0aWxzL2ZsYXR0ZW4nO1xuaW1wb3J0IGlzU3RhdGljUnVsZXMgZnJvbSAnLi4vdXRpbHMvaXNTdGF0aWNSdWxlcyc7XG5pbXBvcnQgc3RyaW5naWZ5UnVsZXMgZnJvbSAnLi4vdXRpbHMvc3RyaW5naWZ5UnVsZXMnO1xuaW1wb3J0IFN0eWxlU2hlZXQgZnJvbSAnLi9TdHlsZVNoZWV0JztcblxuaW1wb3J0IHR5cGUgeyBSdWxlU2V0IH0gZnJvbSAnLi4vdHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHbG9iYWxTdHlsZSB7XG4gIGNvbXBvbmVudElkOiBzdHJpbmc7XG5cbiAgaXNTdGF0aWM6IGJvb2xlYW47XG5cbiAgcnVsZXM6IFJ1bGVTZXQ7XG5cbiAgY29uc3RydWN0b3IocnVsZXM6IFJ1bGVTZXQsIGNvbXBvbmVudElkOiBzdHJpbmcpIHtcbiAgICB0aGlzLnJ1bGVzID0gcnVsZXM7XG4gICAgdGhpcy5jb21wb25lbnRJZCA9IGNvbXBvbmVudElkO1xuICAgIHRoaXMuaXNTdGF0aWMgPSBpc1N0YXRpY1J1bGVzKHJ1bGVzLCBFTVBUWV9BUlJBWSk7XG5cbiAgICBpZiAoIVN0eWxlU2hlZXQubWFzdGVyLmhhc0lkKGNvbXBvbmVudElkKSkge1xuICAgICAgU3R5bGVTaGVldC5tYXN0ZXIuZGVmZXJyZWRJbmplY3QoY29tcG9uZW50SWQsIFtdKTtcbiAgICB9XG4gIH1cblxuICBjcmVhdGVTdHlsZXMoZXhlY3V0aW9uQ29udGV4dDogT2JqZWN0LCBzdHlsZVNoZWV0OiBTdHlsZVNoZWV0KSB7XG4gICAgY29uc3QgZmxhdENTUyA9IGZsYXR0ZW4odGhpcy5ydWxlcywgZXhlY3V0aW9uQ29udGV4dCwgc3R5bGVTaGVldCk7XG4gICAgY29uc3QgY3NzID0gc3RyaW5naWZ5UnVsZXMoZmxhdENTUywgJycpO1xuXG4gICAgc3R5bGVTaGVldC5pbmplY3QodGhpcy5jb21wb25lbnRJZCwgY3NzKTtcbiAgfVxuXG4gIHJlbW92ZVN0eWxlcyhzdHlsZVNoZWV0OiBTdHlsZVNoZWV0KSB7XG4gICAgY29uc3QgeyBjb21wb25lbnRJZCB9ID0gdGhpcztcbiAgICBpZiAoc3R5bGVTaGVldC5oYXNJZChjb21wb25lbnRJZCkpIHtcbiAgICAgIHN0eWxlU2hlZXQucmVtb3ZlKGNvbXBvbmVudElkKTtcbiAgICB9XG4gIH1cblxuICAvLyBUT0RPOiBvdmVyd3JpdGUgaW4tcGxhY2UgaW5zdGVhZCBvZiByZW1vdmUrY3JlYXRlP1xuICByZW5kZXJTdHlsZXMoZXhlY3V0aW9uQ29udGV4dDogT2JqZWN0LCBzdHlsZVNoZWV0OiBTdHlsZVNoZWV0KSB7XG4gICAgdGhpcy5yZW1vdmVTdHlsZXMoc3R5bGVTaGVldCk7XG4gICAgdGhpcy5jcmVhdGVTdHlsZXMoZXhlY3V0aW9uQ29udGV4dCwgc3R5bGVTaGVldCk7XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMkNBOztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNTQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZaQTs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1FBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeUlBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxSkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/styled-components/dist/styled-components.browser.esm.js
