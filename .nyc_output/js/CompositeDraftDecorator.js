/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule CompositeDraftDecorator
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var List = Immutable.List;
var DELIMITER = '.';
/**
 * A CompositeDraftDecorator traverses through a list of DraftDecorator
 * instances to identify sections of a ContentBlock that should be rendered
 * in a "decorated" manner. For example, hashtags, mentions, and links may
 * be intended to stand out visually, be rendered as anchors, etc.
 *
 * The list of decorators supplied to the constructor will be used in the
 * order they are provided. This allows the caller to specify a priority for
 * string matching, in case of match collisions among decorators.
 *
 * For instance, I may have a link with a `#` in its text. Though this section
 * of text may match our hashtag decorator, it should not be treated as a
 * hashtag. I should therefore list my link DraftDecorator
 * before my hashtag DraftDecorator when constructing this composite
 * decorator instance.
 *
 * Thus, when a collision like this is encountered, the earlier match is
 * preserved and the new match is discarded.
 */

var CompositeDraftDecorator = function () {
  function CompositeDraftDecorator(decorators) {
    _classCallCheck(this, CompositeDraftDecorator); // Copy the decorator array, since we use this array order to determine
    // precedence of decoration matching. If the array is mutated externally,
    // we don't want to be affected here.


    this._decorators = decorators.slice();
  }

  CompositeDraftDecorator.prototype.getDecorations = function getDecorations(block, contentState) {
    var decorations = Array(block.getText().length).fill(null);

    this._decorators.forEach(function (
    /*object*/
    decorator,
    /*number*/
    ii) {
      var counter = 0;
      var strategy = decorator.strategy;

      var callback = function callback(
      /*number*/
      start,
      /*number*/
      end) {
        // Find out if any of our matching range is already occupied
        // by another decorator. If so, discard the match. Otherwise, store
        // the component key for rendering.
        if (canOccupySlice(decorations, start, end)) {
          occupySlice(decorations, start, end, ii + DELIMITER + counter);
          counter++;
        }
      };

      strategy(block, callback, contentState);
    });

    return List(decorations);
  };

  CompositeDraftDecorator.prototype.getComponentForKey = function getComponentForKey(key) {
    var componentKey = parseInt(key.split(DELIMITER)[0], 10);
    return this._decorators[componentKey].component;
  };

  CompositeDraftDecorator.prototype.getPropsForKey = function getPropsForKey(key) {
    var componentKey = parseInt(key.split(DELIMITER)[0], 10);
    return this._decorators[componentKey].props;
  };

  return CompositeDraftDecorator;
}();
/**
 * Determine whether we can occupy the specified slice of the decorations
 * array.
 */


function canOccupySlice(decorations, start, end) {
  for (var ii = start; ii < end; ii++) {
    if (decorations[ii] != null) {
      return false;
    }
  }

  return true;
}
/**
 * Splice the specified component into our decoration array at the desired
 * range.
 */


function occupySlice(targetArr, start, end, componentKey) {
  for (var ii = start; ii < end; ii++) {
    targetArr[ii] = componentKey;
  }
}

module.exports = CompositeDraftDecorator;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbXBvc2l0ZURyYWZ0RGVjb3JhdG9yLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbXBvc2l0ZURyYWZ0RGVjb3JhdG9yLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgQ29tcG9zaXRlRHJhZnREZWNvcmF0b3JcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0O1xuXG5cbnZhciBERUxJTUlURVIgPSAnLic7XG5cbi8qKlxuICogQSBDb21wb3NpdGVEcmFmdERlY29yYXRvciB0cmF2ZXJzZXMgdGhyb3VnaCBhIGxpc3Qgb2YgRHJhZnREZWNvcmF0b3JcbiAqIGluc3RhbmNlcyB0byBpZGVudGlmeSBzZWN0aW9ucyBvZiBhIENvbnRlbnRCbG9jayB0aGF0IHNob3VsZCBiZSByZW5kZXJlZFxuICogaW4gYSBcImRlY29yYXRlZFwiIG1hbm5lci4gRm9yIGV4YW1wbGUsIGhhc2h0YWdzLCBtZW50aW9ucywgYW5kIGxpbmtzIG1heVxuICogYmUgaW50ZW5kZWQgdG8gc3RhbmQgb3V0IHZpc3VhbGx5LCBiZSByZW5kZXJlZCBhcyBhbmNob3JzLCBldGMuXG4gKlxuICogVGhlIGxpc3Qgb2YgZGVjb3JhdG9ycyBzdXBwbGllZCB0byB0aGUgY29uc3RydWN0b3Igd2lsbCBiZSB1c2VkIGluIHRoZVxuICogb3JkZXIgdGhleSBhcmUgcHJvdmlkZWQuIFRoaXMgYWxsb3dzIHRoZSBjYWxsZXIgdG8gc3BlY2lmeSBhIHByaW9yaXR5IGZvclxuICogc3RyaW5nIG1hdGNoaW5nLCBpbiBjYXNlIG9mIG1hdGNoIGNvbGxpc2lvbnMgYW1vbmcgZGVjb3JhdG9ycy5cbiAqXG4gKiBGb3IgaW5zdGFuY2UsIEkgbWF5IGhhdmUgYSBsaW5rIHdpdGggYSBgI2AgaW4gaXRzIHRleHQuIFRob3VnaCB0aGlzIHNlY3Rpb25cbiAqIG9mIHRleHQgbWF5IG1hdGNoIG91ciBoYXNodGFnIGRlY29yYXRvciwgaXQgc2hvdWxkIG5vdCBiZSB0cmVhdGVkIGFzIGFcbiAqIGhhc2h0YWcuIEkgc2hvdWxkIHRoZXJlZm9yZSBsaXN0IG15IGxpbmsgRHJhZnREZWNvcmF0b3JcbiAqIGJlZm9yZSBteSBoYXNodGFnIERyYWZ0RGVjb3JhdG9yIHdoZW4gY29uc3RydWN0aW5nIHRoaXMgY29tcG9zaXRlXG4gKiBkZWNvcmF0b3IgaW5zdGFuY2UuXG4gKlxuICogVGh1cywgd2hlbiBhIGNvbGxpc2lvbiBsaWtlIHRoaXMgaXMgZW5jb3VudGVyZWQsIHRoZSBlYXJsaWVyIG1hdGNoIGlzXG4gKiBwcmVzZXJ2ZWQgYW5kIHRoZSBuZXcgbWF0Y2ggaXMgZGlzY2FyZGVkLlxuICovXG5cbnZhciBDb21wb3NpdGVEcmFmdERlY29yYXRvciA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gQ29tcG9zaXRlRHJhZnREZWNvcmF0b3IoZGVjb3JhdG9ycykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDb21wb3NpdGVEcmFmdERlY29yYXRvcik7XG5cbiAgICAvLyBDb3B5IHRoZSBkZWNvcmF0b3IgYXJyYXksIHNpbmNlIHdlIHVzZSB0aGlzIGFycmF5IG9yZGVyIHRvIGRldGVybWluZVxuICAgIC8vIHByZWNlZGVuY2Ugb2YgZGVjb3JhdGlvbiBtYXRjaGluZy4gSWYgdGhlIGFycmF5IGlzIG11dGF0ZWQgZXh0ZXJuYWxseSxcbiAgICAvLyB3ZSBkb24ndCB3YW50IHRvIGJlIGFmZmVjdGVkIGhlcmUuXG4gICAgdGhpcy5fZGVjb3JhdG9ycyA9IGRlY29yYXRvcnMuc2xpY2UoKTtcbiAgfVxuXG4gIENvbXBvc2l0ZURyYWZ0RGVjb3JhdG9yLnByb3RvdHlwZS5nZXREZWNvcmF0aW9ucyA9IGZ1bmN0aW9uIGdldERlY29yYXRpb25zKGJsb2NrLCBjb250ZW50U3RhdGUpIHtcbiAgICB2YXIgZGVjb3JhdGlvbnMgPSBBcnJheShibG9jay5nZXRUZXh0KCkubGVuZ3RoKS5maWxsKG51bGwpO1xuXG4gICAgdGhpcy5fZGVjb3JhdG9ycy5mb3JFYWNoKGZ1bmN0aW9uICggLypvYmplY3QqL2RlY29yYXRvciwgLypudW1iZXIqL2lpKSB7XG4gICAgICB2YXIgY291bnRlciA9IDA7XG4gICAgICB2YXIgc3RyYXRlZ3kgPSBkZWNvcmF0b3Iuc3RyYXRlZ3k7XG4gICAgICB2YXIgY2FsbGJhY2sgPSBmdW5jdGlvbiBjYWxsYmFjayggLypudW1iZXIqL3N0YXJ0LCAvKm51bWJlciovZW5kKSB7XG4gICAgICAgIC8vIEZpbmQgb3V0IGlmIGFueSBvZiBvdXIgbWF0Y2hpbmcgcmFuZ2UgaXMgYWxyZWFkeSBvY2N1cGllZFxuICAgICAgICAvLyBieSBhbm90aGVyIGRlY29yYXRvci4gSWYgc28sIGRpc2NhcmQgdGhlIG1hdGNoLiBPdGhlcndpc2UsIHN0b3JlXG4gICAgICAgIC8vIHRoZSBjb21wb25lbnQga2V5IGZvciByZW5kZXJpbmcuXG4gICAgICAgIGlmIChjYW5PY2N1cHlTbGljZShkZWNvcmF0aW9ucywgc3RhcnQsIGVuZCkpIHtcbiAgICAgICAgICBvY2N1cHlTbGljZShkZWNvcmF0aW9ucywgc3RhcnQsIGVuZCwgaWkgKyBERUxJTUlURVIgKyBjb3VudGVyKTtcbiAgICAgICAgICBjb3VudGVyKys7XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICBzdHJhdGVneShibG9jaywgY2FsbGJhY2ssIGNvbnRlbnRTdGF0ZSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gTGlzdChkZWNvcmF0aW9ucyk7XG4gIH07XG5cbiAgQ29tcG9zaXRlRHJhZnREZWNvcmF0b3IucHJvdG90eXBlLmdldENvbXBvbmVudEZvcktleSA9IGZ1bmN0aW9uIGdldENvbXBvbmVudEZvcktleShrZXkpIHtcbiAgICB2YXIgY29tcG9uZW50S2V5ID0gcGFyc2VJbnQoa2V5LnNwbGl0KERFTElNSVRFUilbMF0sIDEwKTtcbiAgICByZXR1cm4gdGhpcy5fZGVjb3JhdG9yc1tjb21wb25lbnRLZXldLmNvbXBvbmVudDtcbiAgfTtcblxuICBDb21wb3NpdGVEcmFmdERlY29yYXRvci5wcm90b3R5cGUuZ2V0UHJvcHNGb3JLZXkgPSBmdW5jdGlvbiBnZXRQcm9wc0ZvcktleShrZXkpIHtcbiAgICB2YXIgY29tcG9uZW50S2V5ID0gcGFyc2VJbnQoa2V5LnNwbGl0KERFTElNSVRFUilbMF0sIDEwKTtcbiAgICByZXR1cm4gdGhpcy5fZGVjb3JhdG9yc1tjb21wb25lbnRLZXldLnByb3BzO1xuICB9O1xuXG4gIHJldHVybiBDb21wb3NpdGVEcmFmdERlY29yYXRvcjtcbn0oKTtcblxuLyoqXG4gKiBEZXRlcm1pbmUgd2hldGhlciB3ZSBjYW4gb2NjdXB5IHRoZSBzcGVjaWZpZWQgc2xpY2Ugb2YgdGhlIGRlY29yYXRpb25zXG4gKiBhcnJheS5cbiAqL1xuXG5cbmZ1bmN0aW9uIGNhbk9jY3VweVNsaWNlKGRlY29yYXRpb25zLCBzdGFydCwgZW5kKSB7XG4gIGZvciAodmFyIGlpID0gc3RhcnQ7IGlpIDwgZW5kOyBpaSsrKSB7XG4gICAgaWYgKGRlY29yYXRpb25zW2lpXSAhPSBudWxsKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG4gIHJldHVybiB0cnVlO1xufVxuXG4vKipcbiAqIFNwbGljZSB0aGUgc3BlY2lmaWVkIGNvbXBvbmVudCBpbnRvIG91ciBkZWNvcmF0aW9uIGFycmF5IGF0IHRoZSBkZXNpcmVkXG4gKiByYW5nZS5cbiAqL1xuZnVuY3Rpb24gb2NjdXB5U2xpY2UodGFyZ2V0QXJyLCBzdGFydCwgZW5kLCBjb21wb25lbnRLZXkpIHtcbiAgZm9yICh2YXIgaWkgPSBzdGFydDsgaWkgPCBlbmQ7IGlpKyspIHtcbiAgICB0YXJnZXRBcnJbaWldID0gY29tcG9uZW50S2V5O1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQ29tcG9zaXRlRHJhZnREZWNvcmF0b3I7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/CompositeDraftDecorator.js
