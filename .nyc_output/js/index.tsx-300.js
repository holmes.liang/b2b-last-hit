__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page */ "./src/component/page/page.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Page", function() { return _page__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _page_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./page-header */ "./src/component/page/page-header.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageHeader", function() { return _page_header__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _page_body__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./page-body */ "./src/component/page/page-body.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageBody", function() { return _page_body__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _page_footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page-footer */ "./src/component/page/page-footer.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PageFooter", function() { return _page_footer__WEBPACK_IMPORTED_MODULE_3__["default"]; });








//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L3BhZ2UvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tcG9uZW50L3BhZ2UvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQYWdlIGZyb20gJy4vcGFnZSc7XG5pbXBvcnQgUGFnZUhlYWRlciBmcm9tICcuL3BhZ2UtaGVhZGVyJztcbmltcG9ydCBQYWdlQm9keSBmcm9tICcuL3BhZ2UtYm9keSc7XG5pbXBvcnQgUGFnZUZvb3RlciBmcm9tICcuL3BhZ2UtZm9vdGVyJztcblxuZXhwb3J0IHsgUGFnZSB9O1xuZXhwb3J0IHsgUGFnZUhlYWRlciB9O1xuZXhwb3J0IHsgUGFnZUJvZHkgfTtcbmV4cG9ydCB7IFBhZ2VGb290ZXIgfTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/component/page/index.tsx
