/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandBackspaceToStartOfLine
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var expandRangeToStartOfLine = __webpack_require__(/*! ./expandRangeToStartOfLine */ "./node_modules/draft-js/lib/expandRangeToStartOfLine.js");

var getDraftEditorSelectionWithNodes = __webpack_require__(/*! ./getDraftEditorSelectionWithNodes */ "./node_modules/draft-js/lib/getDraftEditorSelectionWithNodes.js");

var moveSelectionBackward = __webpack_require__(/*! ./moveSelectionBackward */ "./node_modules/draft-js/lib/moveSelectionBackward.js");

var removeTextWithStrategy = __webpack_require__(/*! ./removeTextWithStrategy */ "./node_modules/draft-js/lib/removeTextWithStrategy.js");

function keyCommandBackspaceToStartOfLine(editorState) {
  var afterRemoval = removeTextWithStrategy(editorState, function (strategyState) {
    var selection = strategyState.getSelection();

    if (selection.isCollapsed() && selection.getAnchorOffset() === 0) {
      return moveSelectionBackward(strategyState, 1);
    }

    var domSelection = global.getSelection();
    var range = domSelection.getRangeAt(0);
    range = expandRangeToStartOfLine(range);
    return getDraftEditorSelectionWithNodes(strategyState, null, range.endContainer, range.endOffset, range.startContainer, range.startOffset).selectionState;
  }, 'backward');

  if (afterRemoval === editorState.getCurrentContent()) {
    return editorState;
  }

  return EditorState.push(editorState, afterRemoval, 'remove-range');
}

module.exports = keyCommandBackspaceToStartOfLine;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRCYWNrc3BhY2VUb1N0YXJ0T2ZMaW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRCYWNrc3BhY2VUb1N0YXJ0T2ZMaW5lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZEJhY2tzcGFjZVRvU3RhcnRPZkxpbmVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG52YXIgZXhwYW5kUmFuZ2VUb1N0YXJ0T2ZMaW5lID0gcmVxdWlyZSgnLi9leHBhbmRSYW5nZVRvU3RhcnRPZkxpbmUnKTtcbnZhciBnZXREcmFmdEVkaXRvclNlbGVjdGlvbldpdGhOb2RlcyA9IHJlcXVpcmUoJy4vZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb25XaXRoTm9kZXMnKTtcbnZhciBtb3ZlU2VsZWN0aW9uQmFja3dhcmQgPSByZXF1aXJlKCcuL21vdmVTZWxlY3Rpb25CYWNrd2FyZCcpO1xudmFyIHJlbW92ZVRleHRXaXRoU3RyYXRlZ3kgPSByZXF1aXJlKCcuL3JlbW92ZVRleHRXaXRoU3RyYXRlZ3knKTtcblxuZnVuY3Rpb24ga2V5Q29tbWFuZEJhY2tzcGFjZVRvU3RhcnRPZkxpbmUoZWRpdG9yU3RhdGUpIHtcbiAgdmFyIGFmdGVyUmVtb3ZhbCA9IHJlbW92ZVRleHRXaXRoU3RyYXRlZ3koZWRpdG9yU3RhdGUsIGZ1bmN0aW9uIChzdHJhdGVneVN0YXRlKSB7XG4gICAgdmFyIHNlbGVjdGlvbiA9IHN0cmF0ZWd5U3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gICAgaWYgKHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpICYmIHNlbGVjdGlvbi5nZXRBbmNob3JPZmZzZXQoKSA9PT0gMCkge1xuICAgICAgcmV0dXJuIG1vdmVTZWxlY3Rpb25CYWNrd2FyZChzdHJhdGVneVN0YXRlLCAxKTtcbiAgICB9XG5cbiAgICB2YXIgZG9tU2VsZWN0aW9uID0gZ2xvYmFsLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciByYW5nZSA9IGRvbVNlbGVjdGlvbi5nZXRSYW5nZUF0KDApO1xuICAgIHJhbmdlID0gZXhwYW5kUmFuZ2VUb1N0YXJ0T2ZMaW5lKHJhbmdlKTtcblxuICAgIHJldHVybiBnZXREcmFmdEVkaXRvclNlbGVjdGlvbldpdGhOb2RlcyhzdHJhdGVneVN0YXRlLCBudWxsLCByYW5nZS5lbmRDb250YWluZXIsIHJhbmdlLmVuZE9mZnNldCwgcmFuZ2Uuc3RhcnRDb250YWluZXIsIHJhbmdlLnN0YXJ0T2Zmc2V0KS5zZWxlY3Rpb25TdGF0ZTtcbiAgfSwgJ2JhY2t3YXJkJyk7XG5cbiAgaWYgKGFmdGVyUmVtb3ZhbCA9PT0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSkge1xuICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgfVxuXG4gIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBhZnRlclJlbW92YWwsICdyZW1vdmUtcmFuZ2UnKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBrZXlDb21tYW5kQmFja3NwYWNlVG9TdGFydE9mTGluZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandBackspaceToStartOfLine.js
