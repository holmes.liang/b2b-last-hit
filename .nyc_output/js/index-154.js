__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _util_responsiveObserve__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_util/responsiveObserve */ "./node_modules/antd/es/_util/responsiveObserve.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _Col__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Col */ "./node_modules/antd/es/descriptions/Col.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







 // https://github.com/smooth-code/react-flatten-children/

function flattenChildren(children) {
  if (!children) {
    return [];
  }

  return Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_2__["default"])(children).reduce(function (flatChildren, child) {
    if (child && child.type === react__WEBPACK_IMPORTED_MODULE_0__["Fragment"]) {
      return flatChildren.concat(flattenChildren(child.props.children));
    }

    flatChildren.push(child);
    return flatChildren;
  }, []);
}

var DescriptionsItem = function DescriptionsItem(_ref) {
  var children = _ref.children;
  return children;
};
/**
 * Convert children into `column` groups.
 * @param children: DescriptionsItem
 * @param column: number
 */


var generateChildrenRows = function generateChildrenRows(children, column) {
  var rows = [];
  var columns = null;
  var leftSpans;
  var itemNodes = flattenChildren(children);
  itemNodes.forEach(function (node, index) {
    var itemNode = node;

    if (!columns) {
      leftSpans = column;
      columns = [];
      rows.push(columns);
    } // Always set last span to align the end of Descriptions


    var lastItem = index === itemNodes.length - 1;
    var lastSpanSame = true;

    if (lastItem) {
      lastSpanSame = !itemNode.props.span || itemNode.props.span === leftSpans;
      itemNode = react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](itemNode, {
        span: leftSpans
      });
    } // Calculate left fill span


    var _itemNode$props$span = itemNode.props.span,
        span = _itemNode$props$span === void 0 ? 1 : _itemNode$props$span;
    columns.push(itemNode);
    leftSpans -= span;

    if (leftSpans <= 0) {
      columns = null;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_3__["default"])(leftSpans === 0 && lastSpanSame, 'Descriptions', 'Sum of column `span` in a line exceeds `column` of Descriptions.');
    }
  });
  return rows;
};

var renderRow = function renderRow(children, index, _ref2, bordered, layout, colon) {
  var prefixCls = _ref2.prefixCls;

  var renderCol = function renderCol(colItem, type, idx) {
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Col__WEBPACK_IMPORTED_MODULE_6__["default"], {
      child: colItem,
      bordered: bordered,
      colon: colon,
      type: type,
      key: "".concat(type, "-").concat(colItem.key || idx),
      layout: layout
    });
  };

  var cloneChildren = [];
  var cloneContentChildren = [];
  flattenChildren(children).forEach(function (childrenItem, idx) {
    cloneChildren.push(renderCol(childrenItem, 'label', idx));

    if (layout === 'vertical') {
      cloneContentChildren.push(renderCol(childrenItem, 'content', idx));
    } else if (bordered) {
      cloneChildren.push(renderCol(childrenItem, 'content', idx));
    }
  });

  if (layout === 'vertical') {
    return [react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tr", {
      className: "".concat(prefixCls, "-row"),
      key: "label-".concat(index)
    }, cloneChildren), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tr", {
      className: "".concat(prefixCls, "-row"),
      key: "content-".concat(index)
    }, cloneContentChildren)];
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tr", {
    className: "".concat(prefixCls, "-row"),
    key: index
  }, cloneChildren);
};

var defaultColumnMap = {
  xxl: 3,
  xl: 3,
  lg: 3,
  md: 3,
  sm: 2,
  xs: 1
};

var Descriptions =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Descriptions, _React$Component);

  function Descriptions() {
    var _this;

    _classCallCheck(this, Descriptions);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Descriptions).apply(this, arguments));
    _this.state = {
      screens: {}
    };
    return _this;
  }

  _createClass(Descriptions, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var column = this.props.column;
      this.token = _util_responsiveObserve__WEBPACK_IMPORTED_MODULE_4__["default"].subscribe(function (screens) {
        if (_typeof(column) !== 'object') {
          return;
        }

        _this2.setState({
          screens: screens
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _util_responsiveObserve__WEBPACK_IMPORTED_MODULE_4__["default"].unsubscribe(this.token);
    }
  }, {
    key: "getColumn",
    value: function getColumn() {
      var column = this.props.column;

      if (_typeof(column) === 'object') {
        for (var i = 0; i < _util_responsiveObserve__WEBPACK_IMPORTED_MODULE_4__["responsiveArray"].length; i++) {
          var breakpoint = _util_responsiveObserve__WEBPACK_IMPORTED_MODULE_4__["responsiveArray"][i];

          if (this.state.screens[breakpoint] && column[breakpoint] !== undefined) {
            return column[breakpoint] || defaultColumnMap[breakpoint];
          }
        }
      } // If the configuration is not an object, it is a number, return number


      if (typeof column === 'number') {
        return column;
      } // If it is an object, but no response is found, this happens only in the test.
      // Maybe there are some strange environments


      return 3;
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, function (_ref3) {
        var _classNames;

        var getPrefixCls = _ref3.getPrefixCls;
        var _this3$props = _this3.props,
            className = _this3$props.className,
            customizePrefixCls = _this3$props.prefixCls,
            title = _this3$props.title,
            size = _this3$props.size,
            children = _this3$props.children,
            _this3$props$bordered = _this3$props.bordered,
            bordered = _this3$props$bordered === void 0 ? false : _this3$props$bordered,
            _this3$props$layout = _this3$props.layout,
            layout = _this3$props$layout === void 0 ? 'horizontal' : _this3$props$layout,
            _this3$props$colon = _this3$props.colon,
            colon = _this3$props$colon === void 0 ? true : _this3$props$colon,
            style = _this3$props.style;
        var prefixCls = getPrefixCls('descriptions', customizePrefixCls);

        var column = _this3.getColumn();

        var cloneChildren = flattenChildren(children).map(function (child) {
          if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](child)) {
            return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](child, {
              prefixCls: prefixCls
            });
          }

          return null;
        }).filter(function (node) {
          return node;
        });
        var childrenArray = generateChildrenRows(cloneChildren, column);
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(size), size !== 'default'), _defineProperty(_classNames, "".concat(prefixCls, "-bordered"), !!bordered), _classNames)),
          style: style
        }, title && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: "".concat(prefixCls, "-title")
        }, title), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: "".concat(prefixCls, "-view")
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("table", null, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("tbody", null, childrenArray.map(function (child, index) {
          return renderRow(child, index, {
            prefixCls: prefixCls
          }, bordered, layout, colon);
        })))));
      });
    }
  }]);

  return Descriptions;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Descriptions.defaultProps = {
  size: 'default',
  column: defaultColumnMap
};
Descriptions.Item = DescriptionsItem;
/* harmony default export */ __webpack_exports__["default"] = (Descriptions);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9kZXNjcmlwdGlvbnMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2Rlc2NyaXB0aW9ucy9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgdG9BcnJheSBmcm9tICdyYy11dGlsL2xpYi9DaGlsZHJlbi90b0FycmF5JztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuaW1wb3J0IFJlc3BvbnNpdmVPYnNlcnZlLCB7IHJlc3BvbnNpdmVBcnJheSwgfSBmcm9tICcuLi9fdXRpbC9yZXNwb25zaXZlT2JzZXJ2ZSc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgQ29sIGZyb20gJy4vQ29sJztcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9zbW9vdGgtY29kZS9yZWFjdC1mbGF0dGVuLWNoaWxkcmVuL1xuZnVuY3Rpb24gZmxhdHRlbkNoaWxkcmVuKGNoaWxkcmVuKSB7XG4gICAgaWYgKCFjaGlsZHJlbikge1xuICAgICAgICByZXR1cm4gW107XG4gICAgfVxuICAgIHJldHVybiB0b0FycmF5KGNoaWxkcmVuKS5yZWR1Y2UoKGZsYXRDaGlsZHJlbiwgY2hpbGQpID0+IHtcbiAgICAgICAgaWYgKGNoaWxkICYmIGNoaWxkLnR5cGUgPT09IFJlYWN0LkZyYWdtZW50KSB7XG4gICAgICAgICAgICByZXR1cm4gZmxhdENoaWxkcmVuLmNvbmNhdChmbGF0dGVuQ2hpbGRyZW4oY2hpbGQucHJvcHMuY2hpbGRyZW4pKTtcbiAgICAgICAgfVxuICAgICAgICBmbGF0Q2hpbGRyZW4ucHVzaChjaGlsZCk7XG4gICAgICAgIHJldHVybiBmbGF0Q2hpbGRyZW47XG4gICAgfSwgW10pO1xufVxuY29uc3QgRGVzY3JpcHRpb25zSXRlbSA9ICh7IGNoaWxkcmVuIH0pID0+IGNoaWxkcmVuO1xuLyoqXG4gKiBDb252ZXJ0IGNoaWxkcmVuIGludG8gYGNvbHVtbmAgZ3JvdXBzLlxuICogQHBhcmFtIGNoaWxkcmVuOiBEZXNjcmlwdGlvbnNJdGVtXG4gKiBAcGFyYW0gY29sdW1uOiBudW1iZXJcbiAqL1xuY29uc3QgZ2VuZXJhdGVDaGlsZHJlblJvd3MgPSAoY2hpbGRyZW4sIGNvbHVtbikgPT4ge1xuICAgIGNvbnN0IHJvd3MgPSBbXTtcbiAgICBsZXQgY29sdW1ucyA9IG51bGw7XG4gICAgbGV0IGxlZnRTcGFucztcbiAgICBjb25zdCBpdGVtTm9kZXMgPSBmbGF0dGVuQ2hpbGRyZW4oY2hpbGRyZW4pO1xuICAgIGl0ZW1Ob2Rlcy5mb3JFYWNoKChub2RlLCBpbmRleCkgPT4ge1xuICAgICAgICBsZXQgaXRlbU5vZGUgPSBub2RlO1xuICAgICAgICBpZiAoIWNvbHVtbnMpIHtcbiAgICAgICAgICAgIGxlZnRTcGFucyA9IGNvbHVtbjtcbiAgICAgICAgICAgIGNvbHVtbnMgPSBbXTtcbiAgICAgICAgICAgIHJvd3MucHVzaChjb2x1bW5zKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBBbHdheXMgc2V0IGxhc3Qgc3BhbiB0byBhbGlnbiB0aGUgZW5kIG9mIERlc2NyaXB0aW9uc1xuICAgICAgICBjb25zdCBsYXN0SXRlbSA9IGluZGV4ID09PSBpdGVtTm9kZXMubGVuZ3RoIC0gMTtcbiAgICAgICAgbGV0IGxhc3RTcGFuU2FtZSA9IHRydWU7XG4gICAgICAgIGlmIChsYXN0SXRlbSkge1xuICAgICAgICAgICAgbGFzdFNwYW5TYW1lID0gIWl0ZW1Ob2RlLnByb3BzLnNwYW4gfHwgaXRlbU5vZGUucHJvcHMuc3BhbiA9PT0gbGVmdFNwYW5zO1xuICAgICAgICAgICAgaXRlbU5vZGUgPSBSZWFjdC5jbG9uZUVsZW1lbnQoaXRlbU5vZGUsIHtcbiAgICAgICAgICAgICAgICBzcGFuOiBsZWZ0U3BhbnMsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICAvLyBDYWxjdWxhdGUgbGVmdCBmaWxsIHNwYW5cbiAgICAgICAgY29uc3QgeyBzcGFuID0gMSB9ID0gaXRlbU5vZGUucHJvcHM7XG4gICAgICAgIGNvbHVtbnMucHVzaChpdGVtTm9kZSk7XG4gICAgICAgIGxlZnRTcGFucyAtPSBzcGFuO1xuICAgICAgICBpZiAobGVmdFNwYW5zIDw9IDApIHtcbiAgICAgICAgICAgIGNvbHVtbnMgPSBudWxsO1xuICAgICAgICAgICAgd2FybmluZyhsZWZ0U3BhbnMgPT09IDAgJiYgbGFzdFNwYW5TYW1lLCAnRGVzY3JpcHRpb25zJywgJ1N1bSBvZiBjb2x1bW4gYHNwYW5gIGluIGEgbGluZSBleGNlZWRzIGBjb2x1bW5gIG9mIERlc2NyaXB0aW9ucy4nKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiByb3dzO1xufTtcbmNvbnN0IHJlbmRlclJvdyA9IChjaGlsZHJlbiwgaW5kZXgsIHsgcHJlZml4Q2xzIH0sIGJvcmRlcmVkLCBsYXlvdXQsIGNvbG9uKSA9PiB7XG4gICAgY29uc3QgcmVuZGVyQ29sID0gKGNvbEl0ZW0sIHR5cGUsIGlkeCkgPT4ge1xuICAgICAgICByZXR1cm4gKDxDb2wgY2hpbGQ9e2NvbEl0ZW19IGJvcmRlcmVkPXtib3JkZXJlZH0gY29sb249e2NvbG9ufSB0eXBlPXt0eXBlfSBrZXk9e2Ake3R5cGV9LSR7Y29sSXRlbS5rZXkgfHwgaWR4fWB9IGxheW91dD17bGF5b3V0fS8+KTtcbiAgICB9O1xuICAgIGNvbnN0IGNsb25lQ2hpbGRyZW4gPSBbXTtcbiAgICBjb25zdCBjbG9uZUNvbnRlbnRDaGlsZHJlbiA9IFtdO1xuICAgIGZsYXR0ZW5DaGlsZHJlbihjaGlsZHJlbikuZm9yRWFjaCgoY2hpbGRyZW5JdGVtLCBpZHgpID0+IHtcbiAgICAgICAgY2xvbmVDaGlsZHJlbi5wdXNoKHJlbmRlckNvbChjaGlsZHJlbkl0ZW0sICdsYWJlbCcsIGlkeCkpO1xuICAgICAgICBpZiAobGF5b3V0ID09PSAndmVydGljYWwnKSB7XG4gICAgICAgICAgICBjbG9uZUNvbnRlbnRDaGlsZHJlbi5wdXNoKHJlbmRlckNvbChjaGlsZHJlbkl0ZW0sICdjb250ZW50JywgaWR4KSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoYm9yZGVyZWQpIHtcbiAgICAgICAgICAgIGNsb25lQ2hpbGRyZW4ucHVzaChyZW5kZXJDb2woY2hpbGRyZW5JdGVtLCAnY29udGVudCcsIGlkeCkpO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgaWYgKGxheW91dCA9PT0gJ3ZlcnRpY2FsJykge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgPHRyIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1yb3dgfSBrZXk9e2BsYWJlbC0ke2luZGV4fWB9PlxuICAgICAgICB7Y2xvbmVDaGlsZHJlbn1cbiAgICAgIDwvdHI+LFxuICAgICAgICAgICAgPHRyIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1yb3dgfSBrZXk9e2Bjb250ZW50LSR7aW5kZXh9YH0+XG4gICAgICAgIHtjbG9uZUNvbnRlbnRDaGlsZHJlbn1cbiAgICAgIDwvdHI+LFxuICAgICAgICBdO1xuICAgIH1cbiAgICByZXR1cm4gKDx0ciBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tcm93YH0ga2V5PXtpbmRleH0+XG4gICAgICB7Y2xvbmVDaGlsZHJlbn1cbiAgICA8L3RyPik7XG59O1xuY29uc3QgZGVmYXVsdENvbHVtbk1hcCA9IHtcbiAgICB4eGw6IDMsXG4gICAgeGw6IDMsXG4gICAgbGc6IDMsXG4gICAgbWQ6IDMsXG4gICAgc206IDIsXG4gICAgeHM6IDEsXG59O1xuY2xhc3MgRGVzY3JpcHRpb25zIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHNjcmVlbnM6IHt9LFxuICAgICAgICB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgY29uc3QgeyBjb2x1bW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHRoaXMudG9rZW4gPSBSZXNwb25zaXZlT2JzZXJ2ZS5zdWJzY3JpYmUoc2NyZWVucyA9PiB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbHVtbiAhPT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBzY3JlZW5zLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgUmVzcG9uc2l2ZU9ic2VydmUudW5zdWJzY3JpYmUodGhpcy50b2tlbik7XG4gICAgfVxuICAgIGdldENvbHVtbigpIHtcbiAgICAgICAgY29uc3QgeyBjb2x1bW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmICh0eXBlb2YgY29sdW1uID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByZXNwb25zaXZlQXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBjb25zdCBicmVha3BvaW50ID0gcmVzcG9uc2l2ZUFycmF5W2ldO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnNjcmVlbnNbYnJlYWtwb2ludF0gJiYgY29sdW1uW2JyZWFrcG9pbnRdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbHVtblticmVha3BvaW50XSB8fCBkZWZhdWx0Q29sdW1uTWFwW2JyZWFrcG9pbnRdO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyBJZiB0aGUgY29uZmlndXJhdGlvbiBpcyBub3QgYW4gb2JqZWN0LCBpdCBpcyBhIG51bWJlciwgcmV0dXJuIG51bWJlclxuICAgICAgICBpZiAodHlwZW9mIGNvbHVtbiA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIHJldHVybiBjb2x1bW47XG4gICAgICAgIH1cbiAgICAgICAgLy8gSWYgaXQgaXMgYW4gb2JqZWN0LCBidXQgbm8gcmVzcG9uc2UgaXMgZm91bmQsIHRoaXMgaGFwcGVucyBvbmx5IGluIHRoZSB0ZXN0LlxuICAgICAgICAvLyBNYXliZSB0aGVyZSBhcmUgc29tZSBzdHJhbmdlIGVudmlyb25tZW50c1xuICAgICAgICByZXR1cm4gMztcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gKDxDb25maWdDb25zdW1lcj5cbiAgICAgICAgeyh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGNsYXNzTmFtZSwgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHRpdGxlLCBzaXplLCBjaGlsZHJlbiwgYm9yZGVyZWQgPSBmYWxzZSwgbGF5b3V0ID0gJ2hvcml6b250YWwnLCBjb2xvbiA9IHRydWUsIHN0eWxlLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnZGVzY3JpcHRpb25zJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGNvbHVtbiA9IHRoaXMuZ2V0Q29sdW1uKCk7XG4gICAgICAgICAgICBjb25zdCBjbG9uZUNoaWxkcmVuID0gZmxhdHRlbkNoaWxkcmVuKGNoaWxkcmVuKVxuICAgICAgICAgICAgICAgIC5tYXAoKGNoaWxkKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGNoaWxkKSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcmVmaXhDbHMsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLmZpbHRlcigobm9kZSkgPT4gbm9kZSk7XG4gICAgICAgICAgICBjb25zdCBjaGlsZHJlbkFycmF5ID0gZ2VuZXJhdGVDaGlsZHJlblJvd3MoY2xvbmVDaGlsZHJlbiwgY29sdW1uKTtcbiAgICAgICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke3NpemV9YF06IHNpemUgIT09ICdkZWZhdWx0JyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1ib3JkZXJlZGBdOiAhIWJvcmRlcmVkLFxuICAgICAgICAgICAgfSl9IHN0eWxlPXtzdHlsZX0+XG4gICAgICAgICAgICAgIHt0aXRsZSAmJiA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS10aXRsZWB9Pnt0aXRsZX08L2Rpdj59XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXZpZXdgfT5cbiAgICAgICAgICAgICAgICA8dGFibGU+XG4gICAgICAgICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICAgICAgICAgIHtjaGlsZHJlbkFycmF5Lm1hcCgoY2hpbGQsIGluZGV4KSA9PiByZW5kZXJSb3coY2hpbGQsIGluZGV4LCB7XG4gICAgICAgICAgICAgICAgcHJlZml4Q2xzLFxuICAgICAgICAgICAgfSwgYm9yZGVyZWQsIGxheW91dCwgY29sb24pKX1cbiAgICAgICAgICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgICAgICAgICAgPC90YWJsZT5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj4pO1xuICAgICAgICB9fVxuICAgICAgPC9Db25maWdDb25zdW1lcj4pO1xuICAgIH1cbn1cbkRlc2NyaXB0aW9ucy5kZWZhdWx0UHJvcHMgPSB7XG4gICAgc2l6ZTogJ2RlZmF1bHQnLFxuICAgIGNvbHVtbjogZGVmYXVsdENvbHVtbk1hcCxcbn07XG5EZXNjcmlwdGlvbnMuSXRlbSA9IERlc2NyaXB0aW9uc0l0ZW07XG5leHBvcnQgZGVmYXVsdCBEZXNjcmlwdGlvbnM7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQVpBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFrQkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF2QkE7QUF5QkE7QUE5QkE7QUFDQTtBQStCQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFQQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXpCQTtBQUNBO0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQU9BOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBS0E7QUFDQTs7O0FBQUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSkE7QUFRQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQUNBO0FBU0E7QUFDQTtBQVpBO0FBZUE7QUFDQTtBQUNBO0FBREE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQVBBO0FBU0E7QUFUQTtBQVVBO0FBQ0E7QUFBQTtBQUdBO0FBSEE7QUFJQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFEQTtBQVJBO0FBaEJBO0FBaUNBOzs7O0FBekVBO0FBQ0E7QUEwRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/descriptions/index.js
