__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _Checkbox__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Checkbox */ "./node_modules/antd/es/checkbox/Checkbox.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};










var CheckboxGroup =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CheckboxGroup, _React$Component);

  function CheckboxGroup(props) {
    var _this;

    _classCallCheck(this, CheckboxGroup);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CheckboxGroup).call(this, props));

    _this.cancelValue = function (value) {
      _this.setState(function (_ref) {
        var registeredValues = _ref.registeredValues;
        return {
          registeredValues: registeredValues.filter(function (val) {
            return val !== value;
          })
        };
      });
    };

    _this.registerValue = function (value) {
      _this.setState(function (_ref2) {
        var registeredValues = _ref2.registeredValues;
        return {
          registeredValues: [].concat(_toConsumableArray(registeredValues), [value])
        };
      });
    };

    _this.toggleOption = function (option) {
      var registeredValues = _this.state.registeredValues;

      var optionIndex = _this.state.value.indexOf(option.value);

      var value = _toConsumableArray(_this.state.value);

      if (optionIndex === -1) {
        value.push(option.value);
      } else {
        value.splice(optionIndex, 1);
      }

      if (!('value' in _this.props)) {
        _this.setState({
          value: value
        });
      }

      var onChange = _this.props.onChange;

      if (onChange) {
        var options = _this.getOptions();

        onChange(value.filter(function (val) {
          return registeredValues.indexOf(val) !== -1;
        }).sort(function (a, b) {
          var indexA = options.findIndex(function (opt) {
            return opt.value === a;
          });
          var indexB = options.findIndex(function (opt) {
            return opt.value === b;
          });
          return indexA - indexB;
        }));
      }
    };

    _this.renderGroup = function (_ref3) {
      var getPrefixCls = _ref3.getPrefixCls;

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props,
          state = _assertThisInitialize.state;

      var customizePrefixCls = props.prefixCls,
          className = props.className,
          style = props.style,
          options = props.options,
          restProps = __rest(props, ["prefixCls", "className", "style", "options"]);

      var prefixCls = getPrefixCls('checkbox', customizePrefixCls);
      var groupPrefixCls = "".concat(prefixCls, "-group");
      var domProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_5__["default"])(restProps, ['children', 'defaultValue', 'value', 'onChange', 'disabled']);
      var children = props.children;

      if (options && options.length > 0) {
        children = _this.getOptions().map(function (option) {
          return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Checkbox__WEBPACK_IMPORTED_MODULE_6__["default"], {
            prefixCls: prefixCls,
            key: option.value.toString(),
            disabled: 'disabled' in option ? option.disabled : props.disabled,
            value: option.value,
            checked: state.value.indexOf(option.value) !== -1,
            onChange: option.onChange,
            className: "".concat(groupPrefixCls, "-item")
          }, option.label);
        });
      }

      var classString = classnames__WEBPACK_IMPORTED_MODULE_3___default()(groupPrefixCls, className);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({
        className: classString,
        style: style
      }, domProps), children);
    };

    _this.state = {
      value: props.value || props.defaultValue || [],
      registeredValues: []
    };
    return _this;
  }

  _createClass(CheckboxGroup, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        checkboxGroup: {
          toggleOption: this.toggleOption,
          value: this.state.value,
          disabled: this.props.disabled,
          name: this.props.name,
          // https://github.com/ant-design/ant-design/issues/16376
          registerValue: this.registerValue,
          cancelValue: this.cancelValue
        }
      };
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState) {
      return !shallowequal__WEBPACK_IMPORTED_MODULE_4___default()(this.props, nextProps) || !shallowequal__WEBPACK_IMPORTED_MODULE_4___default()(this.state, nextState);
    }
  }, {
    key: "getOptions",
    value: function getOptions() {
      var options = this.props.options; // https://github.com/Microsoft/TypeScript/issues/7960

      return options.map(function (option) {
        if (typeof option === 'string') {
          return {
            label: option,
            value: option
          };
        }

        return option;
      });
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderGroup);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      if ('value' in nextProps) {
        return {
          value: nextProps.value || []
        };
      }

      return null;
    }
  }]);

  return CheckboxGroup;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

CheckboxGroup.defaultProps = {
  options: []
};
CheckboxGroup.propTypes = {
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_1__["array"],
  value: prop_types__WEBPACK_IMPORTED_MODULE_1__["array"],
  options: prop_types__WEBPACK_IMPORTED_MODULE_1__["array"].isRequired,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"]
};
CheckboxGroup.childContextTypes = {
  checkboxGroup: prop_types__WEBPACK_IMPORTED_MODULE_1__["any"]
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(CheckboxGroup);
/* harmony default export */ __webpack_exports__["default"] = (CheckboxGroup);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jaGVja2JveC9Hcm91cC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvY2hlY2tib3gvR3JvdXAuanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc2hhbGxvd0VxdWFsIGZyb20gJ3NoYWxsb3dlcXVhbCc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBDaGVja2JveCBmcm9tICcuL0NoZWNrYm94JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmNsYXNzIENoZWNrYm94R3JvdXAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgdGhpcy5jYW5jZWxWYWx1ZSA9ICh2YWx1ZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSgoeyByZWdpc3RlcmVkVmFsdWVzIH0pID0+ICh7XG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJlZFZhbHVlczogcmVnaXN0ZXJlZFZhbHVlcy5maWx0ZXIodmFsID0+IHZhbCAhPT0gdmFsdWUpLFxuICAgICAgICAgICAgfSkpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlZ2lzdGVyVmFsdWUgPSAodmFsdWUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoKHsgcmVnaXN0ZXJlZFZhbHVlcyB9KSA9PiAoe1xuICAgICAgICAgICAgICAgIHJlZ2lzdGVyZWRWYWx1ZXM6IFsuLi5yZWdpc3RlcmVkVmFsdWVzLCB2YWx1ZV0sXG4gICAgICAgICAgICB9KSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMudG9nZ2xlT3B0aW9uID0gKG9wdGlvbikgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyByZWdpc3RlcmVkVmFsdWVzIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3Qgb3B0aW9uSW5kZXggPSB0aGlzLnN0YXRlLnZhbHVlLmluZGV4T2Yob3B0aW9uLnZhbHVlKTtcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gWy4uLnRoaXMuc3RhdGUudmFsdWVdO1xuICAgICAgICAgICAgaWYgKG9wdGlvbkluZGV4ID09PSAtMSkge1xuICAgICAgICAgICAgICAgIHZhbHVlLnB1c2gob3B0aW9uLnZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhbHVlLnNwbGljZShvcHRpb25JbmRleCwgMSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoISgndmFsdWUnIGluIHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHZhbHVlIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgeyBvbkNoYW5nZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkNoYW5nZSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSB0aGlzLmdldE9wdGlvbnMoKTtcbiAgICAgICAgICAgICAgICBvbkNoYW5nZSh2YWx1ZVxuICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKHZhbCA9PiByZWdpc3RlcmVkVmFsdWVzLmluZGV4T2YodmFsKSAhPT0gLTEpXG4gICAgICAgICAgICAgICAgICAgIC5zb3J0KChhLCBiKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGluZGV4QSA9IG9wdGlvbnMuZmluZEluZGV4KG9wdCA9PiBvcHQudmFsdWUgPT09IGEpO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBpbmRleEIgPSBvcHRpb25zLmZpbmRJbmRleChvcHQgPT4gb3B0LnZhbHVlID09PSBiKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGluZGV4QSAtIGluZGV4QjtcbiAgICAgICAgICAgICAgICB9KSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyR3JvdXAgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcm9wcywgc3RhdGUgfSA9IHRoaXM7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUsIHN0eWxlLCBvcHRpb25zIH0gPSBwcm9wcywgcmVzdFByb3BzID0gX19yZXN0KHByb3BzLCBbXCJwcmVmaXhDbHNcIiwgXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiLCBcIm9wdGlvbnNcIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdjaGVja2JveCcsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBncm91cFByZWZpeENscyA9IGAke3ByZWZpeENsc30tZ3JvdXBgO1xuICAgICAgICAgICAgY29uc3QgZG9tUHJvcHMgPSBvbWl0KHJlc3RQcm9wcywgWydjaGlsZHJlbicsICdkZWZhdWx0VmFsdWUnLCAndmFsdWUnLCAnb25DaGFuZ2UnLCAnZGlzYWJsZWQnXSk7XG4gICAgICAgICAgICBsZXQgeyBjaGlsZHJlbiB9ID0gcHJvcHM7XG4gICAgICAgICAgICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBjaGlsZHJlbiA9IHRoaXMuZ2V0T3B0aW9ucygpLm1hcChvcHRpb24gPT4gKDxDaGVja2JveCBwcmVmaXhDbHM9e3ByZWZpeENsc30ga2V5PXtvcHRpb24udmFsdWUudG9TdHJpbmcoKX0gZGlzYWJsZWQ9eydkaXNhYmxlZCcgaW4gb3B0aW9uID8gb3B0aW9uLmRpc2FibGVkIDogcHJvcHMuZGlzYWJsZWR9IHZhbHVlPXtvcHRpb24udmFsdWV9IGNoZWNrZWQ9e3N0YXRlLnZhbHVlLmluZGV4T2Yob3B0aW9uLnZhbHVlKSAhPT0gLTF9IG9uQ2hhbmdlPXtvcHRpb24ub25DaGFuZ2V9IGNsYXNzTmFtZT17YCR7Z3JvdXBQcmVmaXhDbHN9LWl0ZW1gfT5cbiAgICAgICAgICB7b3B0aW9uLmxhYmVsfVxuICAgICAgICA8L0NoZWNrYm94PikpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgY2xhc3NTdHJpbmcgPSBjbGFzc05hbWVzKGdyb3VwUHJlZml4Q2xzLCBjbGFzc05hbWUpO1xuICAgICAgICAgICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17Y2xhc3NTdHJpbmd9IHN0eWxlPXtzdHlsZX0gey4uLmRvbVByb3BzfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9kaXY+KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHZhbHVlOiBwcm9wcy52YWx1ZSB8fCBwcm9wcy5kZWZhdWx0VmFsdWUgfHwgW10sXG4gICAgICAgICAgICByZWdpc3RlcmVkVmFsdWVzOiBbXSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMpIHtcbiAgICAgICAgaWYgKCd2YWx1ZScgaW4gbmV4dFByb3BzKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHZhbHVlOiBuZXh0UHJvcHMudmFsdWUgfHwgW10sXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBjaGVja2JveEdyb3VwOiB7XG4gICAgICAgICAgICAgICAgdG9nZ2xlT3B0aW9uOiB0aGlzLnRvZ2dsZU9wdGlvbixcbiAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5zdGF0ZS52YWx1ZSxcbiAgICAgICAgICAgICAgICBkaXNhYmxlZDogdGhpcy5wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLnByb3BzLm5hbWUsXG4gICAgICAgICAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTYzNzZcbiAgICAgICAgICAgICAgICByZWdpc3RlclZhbHVlOiB0aGlzLnJlZ2lzdGVyVmFsdWUsXG4gICAgICAgICAgICAgICAgY2FuY2VsVmFsdWU6IHRoaXMuY2FuY2VsVmFsdWUsXG4gICAgICAgICAgICB9LFxuICAgICAgICB9O1xuICAgIH1cbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcbiAgICAgICAgcmV0dXJuICFzaGFsbG93RXF1YWwodGhpcy5wcm9wcywgbmV4dFByb3BzKSB8fCAhc2hhbGxvd0VxdWFsKHRoaXMuc3RhdGUsIG5leHRTdGF0ZSk7XG4gICAgfVxuICAgIGdldE9wdGlvbnMoKSB7XG4gICAgICAgIGNvbnN0IHsgb3B0aW9ucyB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL01pY3Jvc29mdC9UeXBlU2NyaXB0L2lzc3Vlcy83OTYwXG4gICAgICAgIHJldHVybiBvcHRpb25zLm1hcChvcHRpb24gPT4ge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvcHRpb24gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw6IG9wdGlvbixcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG9wdGlvbixcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG9wdGlvbjtcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJHcm91cH08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5DaGVja2JveEdyb3VwLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBvcHRpb25zOiBbXSxcbn07XG5DaGVja2JveEdyb3VwLnByb3BUeXBlcyA9IHtcbiAgICBkZWZhdWx0VmFsdWU6IFByb3BUeXBlcy5hcnJheSxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLmFycmF5LFxuICAgIG9wdGlvbnM6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5DaGVja2JveEdyb3VwLmNoaWxkQ29udGV4dFR5cGVzID0ge1xuICAgIGNoZWNrYm94R3JvdXA6IFByb3BUeXBlcy5hbnksXG59O1xucG9seWZpbGwoQ2hlY2tib3hHcm91cCk7XG5leHBvcnQgZGVmYXVsdCBDaGVja2JveEdyb3VwO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFBQTtBQURBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQURBO0FBQ0E7QUFJQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQWJBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUxBO0FBT0E7QUF2QkE7QUFDQTtBQXdCQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQWJBO0FBQ0E7QUFnQkE7QUFDQTtBQUNBO0FBRkE7QUF0REE7QUEwREE7QUFDQTs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQURBO0FBV0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBUEE7QUFTQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUF2Q0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBbkVBO0FBQ0E7QUFvR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQURBO0FBR0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/checkbox/Group.js
