__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/address/show-address.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n              \n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var ShowAddress =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(ShowAddress, _ModelWidget);

  function ShowAddress(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ShowAddress);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ShowAddress).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ShowAddress, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getAddressContent();
    }
  }, {
    key: "getAddressContent",
    value: function getAddressContent() {
      var _this = this;

      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post("/addresses/full/1_line", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, this.props.addressFixed)).then(function (res) {
        var respData = (res.body || {}).respData || "";

        _this.setState({
          addressInfoContent: respData === "null" ? "" : respData
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var addressInfoContent = this.state.addressInfoContent;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.ShowAddress, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, addressInfoContent);
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        ShowAddress: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ShowAddress.prototype), "initState", this).call(this), {
        addressInfoContent: ""
      });
    }
  }]);

  return ShowAddress;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ShowAddress);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3Mvc2hvdy1hZGRyZXNzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9hZGRyZXNzL3Nob3ctYWRkcmVzcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcywgU3R5bGVkRElWIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQWpheCB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmV4cG9ydCB0eXBlIFNob3dBZGRyZXNzUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGFkZHJlc3NGaXhlZDogc3RyaW5nO1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFNob3dBZGRyZXNzU3RhdGUgPSB7XG4gIGFkZHJlc3NJbmZvQ29udGVudDogc3RyaW5nO1xufTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBTaG93QWRkcmVzc0NvbXBvbmVudHMgPSB7XG4gIFNob3dBZGRyZXNzOiBTdHlsZWRESVY7XG59O1xuXG5jbGFzcyBTaG93QWRkcmVzczxQIGV4dGVuZHMgU2hvd0FkZHJlc3NQcm9wcyxcbiAgUyBleHRlbmRzIFNob3dBZGRyZXNzU3RhdGUsXG4gIEMgZXh0ZW5kcyBTaG93QWRkcmVzc0NvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogU2hvd0FkZHJlc3NQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuZ2V0QWRkcmVzc0NvbnRlbnQoKTtcbiAgfVxuXG4gIGdldEFkZHJlc3NDb250ZW50KCkge1xuICAgIEFqYXgucG9zdChgL2FkZHJlc3Nlcy9mdWxsLzFfbGluZWAsIF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIHRoaXMucHJvcHMuYWRkcmVzc0ZpeGVkKSkudGhlbigocmVzOiBhbnkpID0+IHtcbiAgICAgIGNvbnN0IHJlc3BEYXRhID0gKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCBcIlwiO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFkZHJlc3NJbmZvQ29udGVudDogcmVzcERhdGEgPT09IFwibnVsbFwiID8gXCJcIiA6IHJlc3BEYXRhLFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgYWRkcmVzc0luZm9Db250ZW50IH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5TaG93QWRkcmVzcz5cbiAgICAgICAge2FkZHJlc3NJbmZvQ29udGVudH1cbiAgICAgIDwvQy5TaG93QWRkcmVzcz5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBTaG93QWRkcmVzczogU3R5bGVkLmRpdmBcbiAgICAgICAgICAgICAgXG4gICAgICAgICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIGFkZHJlc3NJbmZvQ29udGVudDogXCJcIixcbiAgICB9KSBhcyBTO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNob3dBZGRyZXNzO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFlQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7OztBQXhDQTtBQUNBO0FBMENBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/address/show-address.tsx
