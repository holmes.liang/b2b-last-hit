__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEventFromHandle", function() { return isEventFromHandle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isValueOutOfRange", function() { return isValueOutOfRange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNotTouchEvent", function() { return isNotTouchEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getClosestPoint", function() { return getClosestPoint; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPrecision", function() { return getPrecision; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMousePosition", function() { return getMousePosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTouchPosition", function() { return getTouchPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHandleCenterPosition", function() { return getHandleCenterPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ensureValueInRange", function() { return ensureValueInRange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ensureValuePrecision", function() { return ensureValuePrecision; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pauseEvent", function() { return pauseEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateNextValue", function() { return calculateNextValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getKeyboardValueMutator", function() { return getKeyboardValueMutator; });
/* harmony import */ var babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");
/* harmony import */ var babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");



function isEventFromHandle(e, handles) {
  try {
    return Object.keys(handles).some(function (key) {
      return e.target === Object(react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"])(handles[key]);
    });
  } catch (error) {
    return false;
  }
}
function isValueOutOfRange(value, _ref) {
  var min = _ref.min,
      max = _ref.max;
  return value < min || value > max;
}
function isNotTouchEvent(e) {
  return e.touches.length > 1 || e.type.toLowerCase() === 'touchend' && e.touches.length > 0;
}
function getClosestPoint(val, _ref2) {
  var marks = _ref2.marks,
      step = _ref2.step,
      min = _ref2.min,
      max = _ref2.max;
  var points = Object.keys(marks).map(parseFloat);

  if (step !== null) {
    var maxSteps = Math.floor((max - min) / step);
    var steps = Math.min((val - min) / step, maxSteps);
    var closestStep = Math.round(steps) * step + min;
    points.push(closestStep);
  }

  var diffs = points.map(function (point) {
    return Math.abs(val - point);
  });
  return points[diffs.indexOf(Math.min.apply(Math, babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(diffs)))];
}
function getPrecision(step) {
  var stepString = step.toString();
  var precision = 0;

  if (stepString.indexOf('.') >= 0) {
    precision = stepString.length - stepString.indexOf('.') - 1;
  }

  return precision;
}
function getMousePosition(vertical, e) {
  return vertical ? e.clientY : e.pageX;
}
function getTouchPosition(vertical, e) {
  return vertical ? e.touches[0].clientY : e.touches[0].pageX;
}
function getHandleCenterPosition(vertical, handle) {
  var coords = handle.getBoundingClientRect();
  return vertical ? coords.top + coords.height * 0.5 : window.pageXOffset + coords.left + coords.width * 0.5;
}
function ensureValueInRange(val, _ref3) {
  var max = _ref3.max,
      min = _ref3.min;

  if (val <= min) {
    return min;
  }

  if (val >= max) {
    return max;
  }

  return val;
}
function ensureValuePrecision(val, props) {
  var step = props.step;
  var closestPoint = isFinite(getClosestPoint(val, props)) ? getClosestPoint(val, props) : 0; // eslint-disable-line

  return step === null ? closestPoint : parseFloat(closestPoint.toFixed(getPrecision(step)));
}
function pauseEvent(e) {
  e.stopPropagation();
  e.preventDefault();
}
function calculateNextValue(func, value, props) {
  var operations = {
    increase: function increase(a, b) {
      return a + b;
    },
    decrease: function decrease(a, b) {
      return a - b;
    }
  };
  var indexToGet = operations[func](Object.keys(props.marks).indexOf(JSON.stringify(value)), 1);
  var keyToGet = Object.keys(props.marks)[indexToGet];

  if (props.step) {
    return operations[func](value, props.step);
  } else if (!!Object.keys(props.marks).length && !!props.marks[keyToGet]) {
    return props.marks[keyToGet];
  }

  return value;
}
function getKeyboardValueMutator(e, vertical, reverse) {
  var increase = 'increase';
  var decrease = 'decrease';
  var method = increase;

  switch (e.keyCode) {
    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP:
      method = vertical && reverse ? decrease : increase;
      break;

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].RIGHT:
      method = !vertical && reverse ? decrease : increase;
      break;

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].DOWN:
      method = vertical && reverse ? increase : decrease;
      break;

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].LEFT:
      method = !vertical && reverse ? increase : decrease;
      break;

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].END:
      return function (value, props) {
        return props.max;
      };

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].HOME:
      return function (value, props) {
        return props.min;
      };

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].PAGE_UP:
      return function (value, props) {
        return value + props.step * 2;
      };

    case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].PAGE_DOWN:
      return function (value, props) {
        return value - props.step * 2;
      };

    default:
      return undefined;
  }

  return function (value, props) {
    return calculateNextValue(method, value, props);
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL3V0aWxzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL3V0aWxzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfdG9Db25zdW1hYmxlQXJyYXkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3RvQ29uc3VtYWJsZUFycmF5JztcbmltcG9ydCB7IGZpbmRET01Ob2RlIH0gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCBrZXlDb2RlIGZyb20gJ3JjLXV0aWwvZXMvS2V5Q29kZSc7XG5cbmV4cG9ydCBmdW5jdGlvbiBpc0V2ZW50RnJvbUhhbmRsZShlLCBoYW5kbGVzKSB7XG4gIHRyeSB7XG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKGhhbmRsZXMpLnNvbWUoZnVuY3Rpb24gKGtleSkge1xuICAgICAgcmV0dXJuIGUudGFyZ2V0ID09PSBmaW5kRE9NTm9kZShoYW5kbGVzW2tleV0pO1xuICAgIH0pO1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNWYWx1ZU91dE9mUmFuZ2UodmFsdWUsIF9yZWYpIHtcbiAgdmFyIG1pbiA9IF9yZWYubWluLFxuICAgICAgbWF4ID0gX3JlZi5tYXg7XG5cbiAgcmV0dXJuIHZhbHVlIDwgbWluIHx8IHZhbHVlID4gbWF4O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNOb3RUb3VjaEV2ZW50KGUpIHtcbiAgcmV0dXJuIGUudG91Y2hlcy5sZW5ndGggPiAxIHx8IGUudHlwZS50b0xvd2VyQ2FzZSgpID09PSAndG91Y2hlbmQnICYmIGUudG91Y2hlcy5sZW5ndGggPiAwO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q2xvc2VzdFBvaW50KHZhbCwgX3JlZjIpIHtcbiAgdmFyIG1hcmtzID0gX3JlZjIubWFya3MsXG4gICAgICBzdGVwID0gX3JlZjIuc3RlcCxcbiAgICAgIG1pbiA9IF9yZWYyLm1pbixcbiAgICAgIG1heCA9IF9yZWYyLm1heDtcblxuICB2YXIgcG9pbnRzID0gT2JqZWN0LmtleXMobWFya3MpLm1hcChwYXJzZUZsb2F0KTtcbiAgaWYgKHN0ZXAgIT09IG51bGwpIHtcbiAgICB2YXIgbWF4U3RlcHMgPSBNYXRoLmZsb29yKChtYXggLSBtaW4pIC8gc3RlcCk7XG4gICAgdmFyIHN0ZXBzID0gTWF0aC5taW4oKHZhbCAtIG1pbikgLyBzdGVwLCBtYXhTdGVwcyk7XG4gICAgdmFyIGNsb3Nlc3RTdGVwID0gTWF0aC5yb3VuZChzdGVwcykgKiBzdGVwICsgbWluO1xuICAgIHBvaW50cy5wdXNoKGNsb3Nlc3RTdGVwKTtcbiAgfVxuICB2YXIgZGlmZnMgPSBwb2ludHMubWFwKGZ1bmN0aW9uIChwb2ludCkge1xuICAgIHJldHVybiBNYXRoLmFicyh2YWwgLSBwb2ludCk7XG4gIH0pO1xuICByZXR1cm4gcG9pbnRzW2RpZmZzLmluZGV4T2YoTWF0aC5taW4uYXBwbHkoTWF0aCwgX3RvQ29uc3VtYWJsZUFycmF5KGRpZmZzKSkpXTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFByZWNpc2lvbihzdGVwKSB7XG4gIHZhciBzdGVwU3RyaW5nID0gc3RlcC50b1N0cmluZygpO1xuICB2YXIgcHJlY2lzaW9uID0gMDtcbiAgaWYgKHN0ZXBTdHJpbmcuaW5kZXhPZignLicpID49IDApIHtcbiAgICBwcmVjaXNpb24gPSBzdGVwU3RyaW5nLmxlbmd0aCAtIHN0ZXBTdHJpbmcuaW5kZXhPZignLicpIC0gMTtcbiAgfVxuICByZXR1cm4gcHJlY2lzaW9uO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TW91c2VQb3NpdGlvbih2ZXJ0aWNhbCwgZSkge1xuICByZXR1cm4gdmVydGljYWwgPyBlLmNsaWVudFkgOiBlLnBhZ2VYO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VG91Y2hQb3NpdGlvbih2ZXJ0aWNhbCwgZSkge1xuICByZXR1cm4gdmVydGljYWwgPyBlLnRvdWNoZXNbMF0uY2xpZW50WSA6IGUudG91Y2hlc1swXS5wYWdlWDtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhhbmRsZUNlbnRlclBvc2l0aW9uKHZlcnRpY2FsLCBoYW5kbGUpIHtcbiAgdmFyIGNvb3JkcyA9IGhhbmRsZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgcmV0dXJuIHZlcnRpY2FsID8gY29vcmRzLnRvcCArIGNvb3Jkcy5oZWlnaHQgKiAwLjUgOiB3aW5kb3cucGFnZVhPZmZzZXQgKyBjb29yZHMubGVmdCArIGNvb3Jkcy53aWR0aCAqIDAuNTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGVuc3VyZVZhbHVlSW5SYW5nZSh2YWwsIF9yZWYzKSB7XG4gIHZhciBtYXggPSBfcmVmMy5tYXgsXG4gICAgICBtaW4gPSBfcmVmMy5taW47XG5cbiAgaWYgKHZhbCA8PSBtaW4pIHtcbiAgICByZXR1cm4gbWluO1xuICB9XG4gIGlmICh2YWwgPj0gbWF4KSB7XG4gICAgcmV0dXJuIG1heDtcbiAgfVxuICByZXR1cm4gdmFsO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZW5zdXJlVmFsdWVQcmVjaXNpb24odmFsLCBwcm9wcykge1xuICB2YXIgc3RlcCA9IHByb3BzLnN0ZXA7XG5cbiAgdmFyIGNsb3Nlc3RQb2ludCA9IGlzRmluaXRlKGdldENsb3Nlc3RQb2ludCh2YWwsIHByb3BzKSkgPyBnZXRDbG9zZXN0UG9pbnQodmFsLCBwcm9wcykgOiAwOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gIHJldHVybiBzdGVwID09PSBudWxsID8gY2xvc2VzdFBvaW50IDogcGFyc2VGbG9hdChjbG9zZXN0UG9pbnQudG9GaXhlZChnZXRQcmVjaXNpb24oc3RlcCkpKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBhdXNlRXZlbnQoZSkge1xuICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBjYWxjdWxhdGVOZXh0VmFsdWUoZnVuYywgdmFsdWUsIHByb3BzKSB7XG4gIHZhciBvcGVyYXRpb25zID0ge1xuICAgIGluY3JlYXNlOiBmdW5jdGlvbiBpbmNyZWFzZShhLCBiKSB7XG4gICAgICByZXR1cm4gYSArIGI7XG4gICAgfSxcbiAgICBkZWNyZWFzZTogZnVuY3Rpb24gZGVjcmVhc2UoYSwgYikge1xuICAgICAgcmV0dXJuIGEgLSBiO1xuICAgIH1cbiAgfTtcblxuICB2YXIgaW5kZXhUb0dldCA9IG9wZXJhdGlvbnNbZnVuY10oT2JqZWN0LmtleXMocHJvcHMubWFya3MpLmluZGV4T2YoSlNPTi5zdHJpbmdpZnkodmFsdWUpKSwgMSk7XG4gIHZhciBrZXlUb0dldCA9IE9iamVjdC5rZXlzKHByb3BzLm1hcmtzKVtpbmRleFRvR2V0XTtcblxuICBpZiAocHJvcHMuc3RlcCkge1xuICAgIHJldHVybiBvcGVyYXRpb25zW2Z1bmNdKHZhbHVlLCBwcm9wcy5zdGVwKTtcbiAgfSBlbHNlIGlmICghIU9iamVjdC5rZXlzKHByb3BzLm1hcmtzKS5sZW5ndGggJiYgISFwcm9wcy5tYXJrc1trZXlUb0dldF0pIHtcbiAgICByZXR1cm4gcHJvcHMubWFya3Nba2V5VG9HZXRdO1xuICB9XG4gIHJldHVybiB2YWx1ZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEtleWJvYXJkVmFsdWVNdXRhdG9yKGUsIHZlcnRpY2FsLCByZXZlcnNlKSB7XG4gIHZhciBpbmNyZWFzZSA9ICdpbmNyZWFzZSc7XG4gIHZhciBkZWNyZWFzZSA9ICdkZWNyZWFzZSc7XG4gIHZhciBtZXRob2QgPSBpbmNyZWFzZTtcbiAgc3dpdGNoIChlLmtleUNvZGUpIHtcbiAgICBjYXNlIGtleUNvZGUuVVA6XG4gICAgICBtZXRob2QgPSB2ZXJ0aWNhbCAmJiByZXZlcnNlID8gZGVjcmVhc2UgOiBpbmNyZWFzZTticmVhaztcbiAgICBjYXNlIGtleUNvZGUuUklHSFQ6XG4gICAgICBtZXRob2QgPSAhdmVydGljYWwgJiYgcmV2ZXJzZSA/IGRlY3JlYXNlIDogaW5jcmVhc2U7YnJlYWs7XG4gICAgY2FzZSBrZXlDb2RlLkRPV046XG4gICAgICBtZXRob2QgPSB2ZXJ0aWNhbCAmJiByZXZlcnNlID8gaW5jcmVhc2UgOiBkZWNyZWFzZTticmVhaztcbiAgICBjYXNlIGtleUNvZGUuTEVGVDpcbiAgICAgIG1ldGhvZCA9ICF2ZXJ0aWNhbCAmJiByZXZlcnNlID8gaW5jcmVhc2UgOiBkZWNyZWFzZTticmVhaztcblxuICAgIGNhc2Uga2V5Q29kZS5FTkQ6XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKHZhbHVlLCBwcm9wcykge1xuICAgICAgICByZXR1cm4gcHJvcHMubWF4O1xuICAgICAgfTtcbiAgICBjYXNlIGtleUNvZGUuSE9NRTpcbiAgICAgIHJldHVybiBmdW5jdGlvbiAodmFsdWUsIHByb3BzKSB7XG4gICAgICAgIHJldHVybiBwcm9wcy5taW47XG4gICAgICB9O1xuICAgIGNhc2Uga2V5Q29kZS5QQUdFX1VQOlxuICAgICAgcmV0dXJuIGZ1bmN0aW9uICh2YWx1ZSwgcHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHZhbHVlICsgcHJvcHMuc3RlcCAqIDI7XG4gICAgICB9O1xuICAgIGNhc2Uga2V5Q29kZS5QQUdFX0RPV046XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKHZhbHVlLCBwcm9wcykge1xuICAgICAgICByZXR1cm4gdmFsdWUgLSBwcm9wcy5zdGVwICogMjtcbiAgICAgIH07XG5cbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24gKHZhbHVlLCBwcm9wcykge1xuICAgIHJldHVybiBjYWxjdWxhdGVOZXh0VmFsdWUobWV0aG9kLCB2YWx1ZSwgcHJvcHMpO1xuICB9O1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1QkE7QUFDQTtBQTZCQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/utils.js
