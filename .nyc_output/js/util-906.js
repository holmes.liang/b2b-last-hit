__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");


 // We only handle element & text node.

var ELEMENT_NODE = 1;
var TEXT_NODE = 3;
var COMMENT_NODE = 8;
var ellipsisContainer;
var wrapperStyle = {
  padding: 0,
  margin: 0,
  display: 'inline',
  lineHeight: 'inherit'
};

function pxToNumber(value) {
  if (!value) return 0;
  var match = value.match(/^\d*(\.\d*)?/);
  return match ? Number(match[0]) : 0;
}

function styleToString(style) {
  // There are some different behavior between Firefox & Chrome.
  // We have to handle this ourself.
  var styleNames = Array.prototype.slice.apply(style);
  return styleNames.map(function (name) {
    return "".concat(name, ": ").concat(style.getPropertyValue(name), ";");
  }).join('');
}

function mergeChildren(children) {
  var childList = [];
  children.forEach(function (child) {
    var prevChild = childList[childList.length - 1];

    if (typeof child === 'string' && typeof prevChild === 'string') {
      childList[childList.length - 1] += child;
    } else {
      childList.push(child);
    }
  });
  return childList;
}

/* harmony default export */ __webpack_exports__["default"] = (function (originEle, rows, content, fixedContent, ellipsisStr) {
  if (!ellipsisContainer) {
    ellipsisContainer = document.createElement('div');
    ellipsisContainer.setAttribute('aria-hidden', 'true');
    document.body.appendChild(ellipsisContainer);
  } // Get origin style


  var originStyle = window.getComputedStyle(originEle);
  var originCSS = styleToString(originStyle);
  var lineHeight = pxToNumber(originStyle.lineHeight);
  var maxHeight = lineHeight * (rows + 1) + pxToNumber(originStyle.paddingTop) + pxToNumber(originStyle.paddingBottom); // Set shadow

  ellipsisContainer.setAttribute('style', originCSS);
  ellipsisContainer.style.position = 'fixed';
  ellipsisContainer.style.left = '0';
  ellipsisContainer.style.height = 'auto';
  ellipsisContainer.style.minHeight = 'auto';
  ellipsisContainer.style.maxHeight = 'auto';
  ellipsisContainer.style.top = '-999999px';
  ellipsisContainer.style.zIndex = '-1000'; // clean up css overflow

  ellipsisContainer.style.textOverflow = 'clip';
  ellipsisContainer.style.whiteSpace = 'normal';
  ellipsisContainer.style.webkitLineClamp = 'none'; // Render in the fake container

  var contentList = mergeChildren(Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_2__["default"])(content));
  Object(react_dom__WEBPACK_IMPORTED_MODULE_0__["render"])(react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("div", {
    style: wrapperStyle
  }, react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("span", {
    style: wrapperStyle
  }, contentList), react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("span", {
    style: wrapperStyle
  }, fixedContent)), ellipsisContainer); // wrap in an div for old version react
  // Check if ellipsis in measure div is height enough for content

  function inRange() {
    return ellipsisContainer.offsetHeight < maxHeight;
  } // Skip ellipsis if already match


  if (inRange()) {
    Object(react_dom__WEBPACK_IMPORTED_MODULE_0__["unmountComponentAtNode"])(ellipsisContainer);
    return {
      content: content,
      text: ellipsisContainer.innerHTML,
      ellipsis: false
    };
  } // We should clone the childNode since they're controlled by React and we can't reuse it without warning


  var childNodes = Array.prototype.slice.apply(ellipsisContainer.childNodes[0].childNodes[0].cloneNode(true).childNodes).filter(function (_ref) {
    var nodeType = _ref.nodeType;
    return nodeType !== COMMENT_NODE;
  });
  var fixedNodes = Array.prototype.slice.apply(ellipsisContainer.childNodes[0].childNodes[1].cloneNode(true).childNodes);
  Object(react_dom__WEBPACK_IMPORTED_MODULE_0__["unmountComponentAtNode"])(ellipsisContainer); // ========================= Find match ellipsis content =========================

  var ellipsisChildren = [];
  ellipsisContainer.innerHTML = ''; // Create origin content holder

  var ellipsisContentHolder = document.createElement('span');
  ellipsisContainer.appendChild(ellipsisContentHolder);
  var ellipsisTextNode = document.createTextNode(ellipsisStr);
  ellipsisContentHolder.appendChild(ellipsisTextNode);
  fixedNodes.forEach(function (childNode) {
    ellipsisContainer.appendChild(childNode);
  }); // Append before fixed nodes

  function appendChildNode(node) {
    ellipsisContentHolder.insertBefore(node, ellipsisTextNode);
  } // Get maximum text


  function measureText(textNode, fullText) {
    var startLoc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
    var endLoc = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : fullText.length;
    var lastSuccessLoc = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 0;
    var midLoc = Math.floor((startLoc + endLoc) / 2);
    var currentText = fullText.slice(0, midLoc);
    textNode.textContent = currentText;

    if (startLoc >= endLoc - 1) {
      // Loop when step is small
      for (var step = endLoc; step >= startLoc; step -= 1) {
        var currentStepText = fullText.slice(0, step);
        textNode.textContent = currentStepText;

        if (inRange()) {
          return step === fullText.length ? {
            finished: false,
            reactNode: fullText
          } : {
            finished: true,
            reactNode: currentStepText
          };
        }
      }
    }

    if (inRange()) {
      return measureText(textNode, fullText, midLoc, endLoc, midLoc);
    }

    return measureText(textNode, fullText, startLoc, midLoc, lastSuccessLoc);
  }

  function measureNode(childNode, index) {
    var type = childNode.nodeType;

    if (type === ELEMENT_NODE) {
      // We don't split element, it will keep if whole element can be displayed.
      appendChildNode(childNode);

      if (inRange()) {
        return {
          finished: false,
          reactNode: contentList[index]
        };
      } // Clean up if can not pull in


      ellipsisContentHolder.removeChild(childNode);
      return {
        finished: true,
        reactNode: null
      };
    }

    if (type === TEXT_NODE) {
      var fullText = childNode.textContent || '';
      var textNode = document.createTextNode(fullText);
      appendChildNode(textNode);
      return measureText(textNode, fullText);
    } // Not handle other type of content
    // PS: This code should not be attached after react 16


    return {
      finished: false,
      reactNode: null
    };
  }

  childNodes.some(function (childNode, index) {
    var _measureNode = measureNode(childNode, index),
        finished = _measureNode.finished,
        reactNode = _measureNode.reactNode;

    if (reactNode) {
      ellipsisChildren.push(reactNode);
    }

    return finished;
  });
  return {
    content: ellipsisChildren,
    text: ellipsisContainer.innerHTML,
    ellipsis: true
  };
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L3V0aWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3R5cG9ncmFwaHkvdXRpbC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgcmVuZGVyLCB1bm1vdW50Q29tcG9uZW50QXROb2RlIH0gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB0b0FycmF5IGZyb20gJ3JjLXV0aWwvbGliL0NoaWxkcmVuL3RvQXJyYXknO1xuLy8gV2Ugb25seSBoYW5kbGUgZWxlbWVudCAmIHRleHQgbm9kZS5cbmNvbnN0IEVMRU1FTlRfTk9ERSA9IDE7XG5jb25zdCBURVhUX05PREUgPSAzO1xuY29uc3QgQ09NTUVOVF9OT0RFID0gODtcbmxldCBlbGxpcHNpc0NvbnRhaW5lcjtcbmNvbnN0IHdyYXBwZXJTdHlsZSA9IHtcbiAgICBwYWRkaW5nOiAwLFxuICAgIG1hcmdpbjogMCxcbiAgICBkaXNwbGF5OiAnaW5saW5lJyxcbiAgICBsaW5lSGVpZ2h0OiAnaW5oZXJpdCcsXG59O1xuZnVuY3Rpb24gcHhUb051bWJlcih2YWx1ZSkge1xuICAgIGlmICghdmFsdWUpXG4gICAgICAgIHJldHVybiAwO1xuICAgIGNvbnN0IG1hdGNoID0gdmFsdWUubWF0Y2goL15cXGQqKFxcLlxcZCopPy8pO1xuICAgIHJldHVybiBtYXRjaCA/IE51bWJlcihtYXRjaFswXSkgOiAwO1xufVxuZnVuY3Rpb24gc3R5bGVUb1N0cmluZyhzdHlsZSkge1xuICAgIC8vIFRoZXJlIGFyZSBzb21lIGRpZmZlcmVudCBiZWhhdmlvciBiZXR3ZWVuIEZpcmVmb3ggJiBDaHJvbWUuXG4gICAgLy8gV2UgaGF2ZSB0byBoYW5kbGUgdGhpcyBvdXJzZWxmLlxuICAgIGNvbnN0IHN0eWxlTmFtZXMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuYXBwbHkoc3R5bGUpO1xuICAgIHJldHVybiBzdHlsZU5hbWVzLm1hcChuYW1lID0+IGAke25hbWV9OiAke3N0eWxlLmdldFByb3BlcnR5VmFsdWUobmFtZSl9O2ApLmpvaW4oJycpO1xufVxuZnVuY3Rpb24gbWVyZ2VDaGlsZHJlbihjaGlsZHJlbikge1xuICAgIGNvbnN0IGNoaWxkTGlzdCA9IFtdO1xuICAgIGNoaWxkcmVuLmZvckVhY2goKGNoaWxkKSA9PiB7XG4gICAgICAgIGNvbnN0IHByZXZDaGlsZCA9IGNoaWxkTGlzdFtjaGlsZExpc3QubGVuZ3RoIC0gMV07XG4gICAgICAgIGlmICh0eXBlb2YgY2hpbGQgPT09ICdzdHJpbmcnICYmIHR5cGVvZiBwcmV2Q2hpbGQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICBjaGlsZExpc3RbY2hpbGRMaXN0Lmxlbmd0aCAtIDFdICs9IGNoaWxkO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgY2hpbGRMaXN0LnB1c2goY2hpbGQpO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGNoaWxkTGlzdDtcbn1cbmV4cG9ydCBkZWZhdWx0IChvcmlnaW5FbGUsIHJvd3MsIGNvbnRlbnQsIGZpeGVkQ29udGVudCwgZWxsaXBzaXNTdHIpID0+IHtcbiAgICBpZiAoIWVsbGlwc2lzQ29udGFpbmVyKSB7XG4gICAgICAgIGVsbGlwc2lzQ29udGFpbmVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIGVsbGlwc2lzQ29udGFpbmVyLnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGVsbGlwc2lzQ29udGFpbmVyKTtcbiAgICB9XG4gICAgLy8gR2V0IG9yaWdpbiBzdHlsZVxuICAgIGNvbnN0IG9yaWdpblN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUob3JpZ2luRWxlKTtcbiAgICBjb25zdCBvcmlnaW5DU1MgPSBzdHlsZVRvU3RyaW5nKG9yaWdpblN0eWxlKTtcbiAgICBjb25zdCBsaW5lSGVpZ2h0ID0gcHhUb051bWJlcihvcmlnaW5TdHlsZS5saW5lSGVpZ2h0KTtcbiAgICBjb25zdCBtYXhIZWlnaHQgPSBsaW5lSGVpZ2h0ICogKHJvd3MgKyAxKSArXG4gICAgICAgIHB4VG9OdW1iZXIob3JpZ2luU3R5bGUucGFkZGluZ1RvcCkgK1xuICAgICAgICBweFRvTnVtYmVyKG9yaWdpblN0eWxlLnBhZGRpbmdCb3R0b20pO1xuICAgIC8vIFNldCBzaGFkb3dcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgb3JpZ2luQ1NTKTtcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zdHlsZS5wb3NpdGlvbiA9ICdmaXhlZCc7XG4gICAgZWxsaXBzaXNDb250YWluZXIuc3R5bGUubGVmdCA9ICcwJztcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zdHlsZS5oZWlnaHQgPSAnYXV0byc7XG4gICAgZWxsaXBzaXNDb250YWluZXIuc3R5bGUubWluSGVpZ2h0ID0gJ2F1dG8nO1xuICAgIGVsbGlwc2lzQ29udGFpbmVyLnN0eWxlLm1heEhlaWdodCA9ICdhdXRvJztcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zdHlsZS50b3AgPSAnLTk5OTk5OXB4JztcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zdHlsZS56SW5kZXggPSAnLTEwMDAnO1xuICAgIC8vIGNsZWFuIHVwIGNzcyBvdmVyZmxvd1xuICAgIGVsbGlwc2lzQ29udGFpbmVyLnN0eWxlLnRleHRPdmVyZmxvdyA9ICdjbGlwJztcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5zdHlsZS53aGl0ZVNwYWNlID0gJ25vcm1hbCc7XG4gICAgZWxsaXBzaXNDb250YWluZXIuc3R5bGUud2Via2l0TGluZUNsYW1wID0gJ25vbmUnO1xuICAgIC8vIFJlbmRlciBpbiB0aGUgZmFrZSBjb250YWluZXJcbiAgICBjb25zdCBjb250ZW50TGlzdCA9IG1lcmdlQ2hpbGRyZW4odG9BcnJheShjb250ZW50KSk7XG4gICAgcmVuZGVyKDxkaXYgc3R5bGU9e3dyYXBwZXJTdHlsZX0+XG4gICAgICA8c3BhbiBzdHlsZT17d3JhcHBlclN0eWxlfT57Y29udGVudExpc3R9PC9zcGFuPlxuICAgICAgPHNwYW4gc3R5bGU9e3dyYXBwZXJTdHlsZX0+e2ZpeGVkQ29udGVudH08L3NwYW4+XG4gICAgPC9kaXY+LCBlbGxpcHNpc0NvbnRhaW5lcik7IC8vIHdyYXAgaW4gYW4gZGl2IGZvciBvbGQgdmVyc2lvbiByZWFjdFxuICAgIC8vIENoZWNrIGlmIGVsbGlwc2lzIGluIG1lYXN1cmUgZGl2IGlzIGhlaWdodCBlbm91Z2ggZm9yIGNvbnRlbnRcbiAgICBmdW5jdGlvbiBpblJhbmdlKCkge1xuICAgICAgICByZXR1cm4gZWxsaXBzaXNDb250YWluZXIub2Zmc2V0SGVpZ2h0IDwgbWF4SGVpZ2h0O1xuICAgIH1cbiAgICAvLyBTa2lwIGVsbGlwc2lzIGlmIGFscmVhZHkgbWF0Y2hcbiAgICBpZiAoaW5SYW5nZSgpKSB7XG4gICAgICAgIHVubW91bnRDb21wb25lbnRBdE5vZGUoZWxsaXBzaXNDb250YWluZXIpO1xuICAgICAgICByZXR1cm4geyBjb250ZW50LCB0ZXh0OiBlbGxpcHNpc0NvbnRhaW5lci5pbm5lckhUTUwsIGVsbGlwc2lzOiBmYWxzZSB9O1xuICAgIH1cbiAgICAvLyBXZSBzaG91bGQgY2xvbmUgdGhlIGNoaWxkTm9kZSBzaW5jZSB0aGV5J3JlIGNvbnRyb2xsZWQgYnkgUmVhY3QgYW5kIHdlIGNhbid0IHJldXNlIGl0IHdpdGhvdXQgd2FybmluZ1xuICAgIGNvbnN0IGNoaWxkTm9kZXMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2VcbiAgICAgICAgLmFwcGx5KGVsbGlwc2lzQ29udGFpbmVyLmNoaWxkTm9kZXNbMF0uY2hpbGROb2Rlc1swXS5jbG9uZU5vZGUodHJ1ZSkuY2hpbGROb2RlcylcbiAgICAgICAgLmZpbHRlcigoeyBub2RlVHlwZSB9KSA9PiBub2RlVHlwZSAhPT0gQ09NTUVOVF9OT0RFKTtcbiAgICBjb25zdCBmaXhlZE5vZGVzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmFwcGx5KGVsbGlwc2lzQ29udGFpbmVyLmNoaWxkTm9kZXNbMF0uY2hpbGROb2Rlc1sxXS5jbG9uZU5vZGUodHJ1ZSkuY2hpbGROb2Rlcyk7XG4gICAgdW5tb3VudENvbXBvbmVudEF0Tm9kZShlbGxpcHNpc0NvbnRhaW5lcik7XG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PSBGaW5kIG1hdGNoIGVsbGlwc2lzIGNvbnRlbnQgPT09PT09PT09PT09PT09PT09PT09PT09PVxuICAgIGNvbnN0IGVsbGlwc2lzQ2hpbGRyZW4gPSBbXTtcbiAgICBlbGxpcHNpc0NvbnRhaW5lci5pbm5lckhUTUwgPSAnJztcbiAgICAvLyBDcmVhdGUgb3JpZ2luIGNvbnRlbnQgaG9sZGVyXG4gICAgY29uc3QgZWxsaXBzaXNDb250ZW50SG9sZGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpO1xuICAgIGVsbGlwc2lzQ29udGFpbmVyLmFwcGVuZENoaWxkKGVsbGlwc2lzQ29udGVudEhvbGRlcik7XG4gICAgY29uc3QgZWxsaXBzaXNUZXh0Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGVsbGlwc2lzU3RyKTtcbiAgICBlbGxpcHNpc0NvbnRlbnRIb2xkZXIuYXBwZW5kQ2hpbGQoZWxsaXBzaXNUZXh0Tm9kZSk7XG4gICAgZml4ZWROb2Rlcy5mb3JFYWNoKGNoaWxkTm9kZSA9PiB7XG4gICAgICAgIGVsbGlwc2lzQ29udGFpbmVyLmFwcGVuZENoaWxkKGNoaWxkTm9kZSk7XG4gICAgfSk7XG4gICAgLy8gQXBwZW5kIGJlZm9yZSBmaXhlZCBub2Rlc1xuICAgIGZ1bmN0aW9uIGFwcGVuZENoaWxkTm9kZShub2RlKSB7XG4gICAgICAgIGVsbGlwc2lzQ29udGVudEhvbGRlci5pbnNlcnRCZWZvcmUobm9kZSwgZWxsaXBzaXNUZXh0Tm9kZSk7XG4gICAgfVxuICAgIC8vIEdldCBtYXhpbXVtIHRleHRcbiAgICBmdW5jdGlvbiBtZWFzdXJlVGV4dCh0ZXh0Tm9kZSwgZnVsbFRleHQsIHN0YXJ0TG9jID0gMCwgZW5kTG9jID0gZnVsbFRleHQubGVuZ3RoLCBsYXN0U3VjY2Vzc0xvYyA9IDApIHtcbiAgICAgICAgY29uc3QgbWlkTG9jID0gTWF0aC5mbG9vcigoc3RhcnRMb2MgKyBlbmRMb2MpIC8gMik7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRUZXh0ID0gZnVsbFRleHQuc2xpY2UoMCwgbWlkTG9jKTtcbiAgICAgICAgdGV4dE5vZGUudGV4dENvbnRlbnQgPSBjdXJyZW50VGV4dDtcbiAgICAgICAgaWYgKHN0YXJ0TG9jID49IGVuZExvYyAtIDEpIHtcbiAgICAgICAgICAgIC8vIExvb3Agd2hlbiBzdGVwIGlzIHNtYWxsXG4gICAgICAgICAgICBmb3IgKGxldCBzdGVwID0gZW5kTG9jOyBzdGVwID49IHN0YXJ0TG9jOyBzdGVwIC09IDEpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50U3RlcFRleHQgPSBmdWxsVGV4dC5zbGljZSgwLCBzdGVwKTtcbiAgICAgICAgICAgICAgICB0ZXh0Tm9kZS50ZXh0Q29udGVudCA9IGN1cnJlbnRTdGVwVGV4dDtcbiAgICAgICAgICAgICAgICBpZiAoaW5SYW5nZSgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzdGVwID09PSBmdWxsVGV4dC5sZW5ndGhcbiAgICAgICAgICAgICAgICAgICAgICAgID8ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbmlzaGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFjdE5vZGU6IGZ1bGxUZXh0LFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmluaXNoZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVhY3ROb2RlOiBjdXJyZW50U3RlcFRleHQsXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAoaW5SYW5nZSgpKSB7XG4gICAgICAgICAgICByZXR1cm4gbWVhc3VyZVRleHQodGV4dE5vZGUsIGZ1bGxUZXh0LCBtaWRMb2MsIGVuZExvYywgbWlkTG9jKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbWVhc3VyZVRleHQodGV4dE5vZGUsIGZ1bGxUZXh0LCBzdGFydExvYywgbWlkTG9jLCBsYXN0U3VjY2Vzc0xvYyk7XG4gICAgfVxuICAgIGZ1bmN0aW9uIG1lYXN1cmVOb2RlKGNoaWxkTm9kZSwgaW5kZXgpIHtcbiAgICAgICAgY29uc3QgdHlwZSA9IGNoaWxkTm9kZS5ub2RlVHlwZTtcbiAgICAgICAgaWYgKHR5cGUgPT09IEVMRU1FTlRfTk9ERSkge1xuICAgICAgICAgICAgLy8gV2UgZG9uJ3Qgc3BsaXQgZWxlbWVudCwgaXQgd2lsbCBrZWVwIGlmIHdob2xlIGVsZW1lbnQgY2FuIGJlIGRpc3BsYXllZC5cbiAgICAgICAgICAgIGFwcGVuZENoaWxkTm9kZShjaGlsZE5vZGUpO1xuICAgICAgICAgICAgaWYgKGluUmFuZ2UoKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIGZpbmlzaGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgcmVhY3ROb2RlOiBjb250ZW50TGlzdFtpbmRleF0sXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIENsZWFuIHVwIGlmIGNhbiBub3QgcHVsbCBpblxuICAgICAgICAgICAgZWxsaXBzaXNDb250ZW50SG9sZGVyLnJlbW92ZUNoaWxkKGNoaWxkTm9kZSk7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIGZpbmlzaGVkOiB0cnVlLFxuICAgICAgICAgICAgICAgIHJlYWN0Tm9kZTogbnVsbCxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGUgPT09IFRFWFRfTk9ERSkge1xuICAgICAgICAgICAgY29uc3QgZnVsbFRleHQgPSBjaGlsZE5vZGUudGV4dENvbnRlbnQgfHwgJyc7XG4gICAgICAgICAgICBjb25zdCB0ZXh0Tm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGZ1bGxUZXh0KTtcbiAgICAgICAgICAgIGFwcGVuZENoaWxkTm9kZSh0ZXh0Tm9kZSk7XG4gICAgICAgICAgICByZXR1cm4gbWVhc3VyZVRleHQodGV4dE5vZGUsIGZ1bGxUZXh0KTtcbiAgICAgICAgfVxuICAgICAgICAvLyBOb3QgaGFuZGxlIG90aGVyIHR5cGUgb2YgY29udGVudFxuICAgICAgICAvLyBQUzogVGhpcyBjb2RlIHNob3VsZCBub3QgYmUgYXR0YWNoZWQgYWZ0ZXIgcmVhY3QgMTZcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGZpbmlzaGVkOiBmYWxzZSxcbiAgICAgICAgICAgIHJlYWN0Tm9kZTogbnVsbCxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgY2hpbGROb2Rlcy5zb21lKChjaGlsZE5vZGUsIGluZGV4KSA9PiB7XG4gICAgICAgIGNvbnN0IHsgZmluaXNoZWQsIHJlYWN0Tm9kZSB9ID0gbWVhc3VyZU5vZGUoY2hpbGROb2RlLCBpbmRleCk7XG4gICAgICAgIGlmIChyZWFjdE5vZGUpIHtcbiAgICAgICAgICAgIGVsbGlwc2lzQ2hpbGRyZW4ucHVzaChyZWFjdE5vZGUpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmaW5pc2hlZDtcbiAgICB9KTtcbiAgICByZXR1cm4ge1xuICAgICAgICBjb250ZW50OiBlbGxpcHNpc0NoaWxkcmVuLFxuICAgICAgICB0ZXh0OiBlbGxpcHNpc0NvbnRhaW5lci5pbm5lckhUTUwsXG4gICAgICAgIGVsbGlwc2lzOiB0cnVlLFxuICAgIH07XG59O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFLQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQWxDQTtBQUNBO0FBQ0E7QUFtQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF2Q0E7QUFDQTtBQUNBO0FBd0NBO0FBRUE7QUFBQTtBQUZBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeERBO0FBQ0E7QUEwREE7QUFDQTtBQTVEQTtBQUNBO0FBQ0E7QUE2REE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSkE7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdEJBO0FBeUJBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBaElBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/util.js
