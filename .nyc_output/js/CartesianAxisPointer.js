/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var BaseAxisPointer = __webpack_require__(/*! ./BaseAxisPointer */ "./node_modules/echarts/lib/component/axisPointer/BaseAxisPointer.js");

var viewHelper = __webpack_require__(/*! ./viewHelper */ "./node_modules/echarts/lib/component/axisPointer/viewHelper.js");

var cartesianAxisHelper = __webpack_require__(/*! ../../coord/cartesian/cartesianAxisHelper */ "./node_modules/echarts/lib/coord/cartesian/cartesianAxisHelper.js");

var AxisView = __webpack_require__(/*! ../axis/AxisView */ "./node_modules/echarts/lib/component/axis/AxisView.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var CartesianAxisPointer = BaseAxisPointer.extend({
  /**
   * @override
   */
  makeElOption: function makeElOption(elOption, value, axisModel, axisPointerModel, api) {
    var axis = axisModel.axis;
    var grid = axis.grid;
    var axisPointerType = axisPointerModel.get('type');
    var otherExtent = getCartesian(grid, axis).getOtherAxis(axis).getGlobalExtent();
    var pixelValue = axis.toGlobalCoord(axis.dataToCoord(value, true));

    if (axisPointerType && axisPointerType !== 'none') {
      var elStyle = viewHelper.buildElStyle(axisPointerModel);
      var pointerOption = pointerShapeBuilder[axisPointerType](axis, pixelValue, otherExtent, elStyle);
      pointerOption.style = elStyle;
      elOption.graphicKey = pointerOption.type;
      elOption.pointer = pointerOption;
    }

    var layoutInfo = cartesianAxisHelper.layout(grid.model, axisModel);
    viewHelper.buildCartesianSingleLabelElOption(value, elOption, layoutInfo, axisModel, axisPointerModel, api);
  },

  /**
   * @override
   */
  getHandleTransform: function getHandleTransform(value, axisModel, axisPointerModel) {
    var layoutInfo = cartesianAxisHelper.layout(axisModel.axis.grid.model, axisModel, {
      labelInside: false
    });
    layoutInfo.labelMargin = axisPointerModel.get('handle.margin');
    return {
      position: viewHelper.getTransformedPosition(axisModel.axis, value, layoutInfo),
      rotation: layoutInfo.rotation + (layoutInfo.labelDirection < 0 ? Math.PI : 0)
    };
  },

  /**
   * @override
   */
  updateHandleTransform: function updateHandleTransform(transform, delta, axisModel, axisPointerModel) {
    var axis = axisModel.axis;
    var grid = axis.grid;
    var axisExtent = axis.getGlobalExtent(true);
    var otherExtent = getCartesian(grid, axis).getOtherAxis(axis).getGlobalExtent();
    var dimIndex = axis.dim === 'x' ? 0 : 1;
    var currPosition = transform.position;
    currPosition[dimIndex] += delta[dimIndex];
    currPosition[dimIndex] = Math.min(axisExtent[1], currPosition[dimIndex]);
    currPosition[dimIndex] = Math.max(axisExtent[0], currPosition[dimIndex]);
    var cursorOtherValue = (otherExtent[1] + otherExtent[0]) / 2;
    var cursorPoint = [cursorOtherValue, cursorOtherValue];
    cursorPoint[dimIndex] = currPosition[dimIndex]; // Make tooltip do not overlap axisPointer and in the middle of the grid.

    var tooltipOptions = [{
      verticalAlign: 'middle'
    }, {
      align: 'center'
    }];
    return {
      position: currPosition,
      rotation: transform.rotation,
      cursorPoint: cursorPoint,
      tooltipOption: tooltipOptions[dimIndex]
    };
  }
});

function getCartesian(grid, axis) {
  var opt = {};
  opt[axis.dim + 'AxisIndex'] = axis.index;
  return grid.getCartesian(opt);
}

var pointerShapeBuilder = {
  line: function line(axis, pixelValue, otherExtent, elStyle) {
    var targetShape = viewHelper.makeLineShape([pixelValue, otherExtent[0]], [pixelValue, otherExtent[1]], getAxisDimIndex(axis));
    graphic.subPixelOptimizeLine({
      shape: targetShape,
      style: elStyle
    });
    return {
      type: 'Line',
      shape: targetShape
    };
  },
  shadow: function shadow(axis, pixelValue, otherExtent, elStyle) {
    var bandWidth = Math.max(1, axis.getBandWidth());
    var span = otherExtent[1] - otherExtent[0];
    return {
      type: 'Rect',
      shape: viewHelper.makeRectShape([pixelValue - bandWidth / 2, otherExtent[0]], [bandWidth, span], getAxisDimIndex(axis))
    };
  }
};

function getAxisDimIndex(axis) {
  return axis.dim === 'x' ? 0 : 1;
}

AxisView.registerAxisPointerClass('CartesianAxisPointer', CartesianAxisPointer);
var _default = CartesianAxisPointer;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL0NhcnRlc2lhbkF4aXNQb2ludGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL0NhcnRlc2lhbkF4aXNQb2ludGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2dyYXBoaWNcIik7XG5cbnZhciBCYXNlQXhpc1BvaW50ZXIgPSByZXF1aXJlKFwiLi9CYXNlQXhpc1BvaW50ZXJcIik7XG5cbnZhciB2aWV3SGVscGVyID0gcmVxdWlyZShcIi4vdmlld0hlbHBlclwiKTtcblxudmFyIGNhcnRlc2lhbkF4aXNIZWxwZXIgPSByZXF1aXJlKFwiLi4vLi4vY29vcmQvY2FydGVzaWFuL2NhcnRlc2lhbkF4aXNIZWxwZXJcIik7XG5cbnZhciBBeGlzVmlldyA9IHJlcXVpcmUoXCIuLi9heGlzL0F4aXNWaWV3XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgQ2FydGVzaWFuQXhpc1BvaW50ZXIgPSBCYXNlQXhpc1BvaW50ZXIuZXh0ZW5kKHtcbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgbWFrZUVsT3B0aW9uOiBmdW5jdGlvbiAoZWxPcHRpb24sIHZhbHVlLCBheGlzTW9kZWwsIGF4aXNQb2ludGVyTW9kZWwsIGFwaSkge1xuICAgIHZhciBheGlzID0gYXhpc01vZGVsLmF4aXM7XG4gICAgdmFyIGdyaWQgPSBheGlzLmdyaWQ7XG4gICAgdmFyIGF4aXNQb2ludGVyVHlwZSA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCd0eXBlJyk7XG4gICAgdmFyIG90aGVyRXh0ZW50ID0gZ2V0Q2FydGVzaWFuKGdyaWQsIGF4aXMpLmdldE90aGVyQXhpcyhheGlzKS5nZXRHbG9iYWxFeHRlbnQoKTtcbiAgICB2YXIgcGl4ZWxWYWx1ZSA9IGF4aXMudG9HbG9iYWxDb29yZChheGlzLmRhdGFUb0Nvb3JkKHZhbHVlLCB0cnVlKSk7XG5cbiAgICBpZiAoYXhpc1BvaW50ZXJUeXBlICYmIGF4aXNQb2ludGVyVHlwZSAhPT0gJ25vbmUnKSB7XG4gICAgICB2YXIgZWxTdHlsZSA9IHZpZXdIZWxwZXIuYnVpbGRFbFN0eWxlKGF4aXNQb2ludGVyTW9kZWwpO1xuICAgICAgdmFyIHBvaW50ZXJPcHRpb24gPSBwb2ludGVyU2hhcGVCdWlsZGVyW2F4aXNQb2ludGVyVHlwZV0oYXhpcywgcGl4ZWxWYWx1ZSwgb3RoZXJFeHRlbnQsIGVsU3R5bGUpO1xuICAgICAgcG9pbnRlck9wdGlvbi5zdHlsZSA9IGVsU3R5bGU7XG4gICAgICBlbE9wdGlvbi5ncmFwaGljS2V5ID0gcG9pbnRlck9wdGlvbi50eXBlO1xuICAgICAgZWxPcHRpb24ucG9pbnRlciA9IHBvaW50ZXJPcHRpb247XG4gICAgfVxuXG4gICAgdmFyIGxheW91dEluZm8gPSBjYXJ0ZXNpYW5BeGlzSGVscGVyLmxheW91dChncmlkLm1vZGVsLCBheGlzTW9kZWwpO1xuICAgIHZpZXdIZWxwZXIuYnVpbGRDYXJ0ZXNpYW5TaW5nbGVMYWJlbEVsT3B0aW9uKHZhbHVlLCBlbE9wdGlvbiwgbGF5b3V0SW5mbywgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsLCBhcGkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIGdldEhhbmRsZVRyYW5zZm9ybTogZnVuY3Rpb24gKHZhbHVlLCBheGlzTW9kZWwsIGF4aXNQb2ludGVyTW9kZWwpIHtcbiAgICB2YXIgbGF5b3V0SW5mbyA9IGNhcnRlc2lhbkF4aXNIZWxwZXIubGF5b3V0KGF4aXNNb2RlbC5heGlzLmdyaWQubW9kZWwsIGF4aXNNb2RlbCwge1xuICAgICAgbGFiZWxJbnNpZGU6IGZhbHNlXG4gICAgfSk7XG4gICAgbGF5b3V0SW5mby5sYWJlbE1hcmdpbiA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdoYW5kbGUubWFyZ2luJyk7XG4gICAgcmV0dXJuIHtcbiAgICAgIHBvc2l0aW9uOiB2aWV3SGVscGVyLmdldFRyYW5zZm9ybWVkUG9zaXRpb24oYXhpc01vZGVsLmF4aXMsIHZhbHVlLCBsYXlvdXRJbmZvKSxcbiAgICAgIHJvdGF0aW9uOiBsYXlvdXRJbmZvLnJvdGF0aW9uICsgKGxheW91dEluZm8ubGFiZWxEaXJlY3Rpb24gPCAwID8gTWF0aC5QSSA6IDApXG4gICAgfTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICB1cGRhdGVIYW5kbGVUcmFuc2Zvcm06IGZ1bmN0aW9uICh0cmFuc2Zvcm0sIGRlbHRhLCBheGlzTW9kZWwsIGF4aXNQb2ludGVyTW9kZWwpIHtcbiAgICB2YXIgYXhpcyA9IGF4aXNNb2RlbC5heGlzO1xuICAgIHZhciBncmlkID0gYXhpcy5ncmlkO1xuICAgIHZhciBheGlzRXh0ZW50ID0gYXhpcy5nZXRHbG9iYWxFeHRlbnQodHJ1ZSk7XG4gICAgdmFyIG90aGVyRXh0ZW50ID0gZ2V0Q2FydGVzaWFuKGdyaWQsIGF4aXMpLmdldE90aGVyQXhpcyhheGlzKS5nZXRHbG9iYWxFeHRlbnQoKTtcbiAgICB2YXIgZGltSW5kZXggPSBheGlzLmRpbSA9PT0gJ3gnID8gMCA6IDE7XG4gICAgdmFyIGN1cnJQb3NpdGlvbiA9IHRyYW5zZm9ybS5wb3NpdGlvbjtcbiAgICBjdXJyUG9zaXRpb25bZGltSW5kZXhdICs9IGRlbHRhW2RpbUluZGV4XTtcbiAgICBjdXJyUG9zaXRpb25bZGltSW5kZXhdID0gTWF0aC5taW4oYXhpc0V4dGVudFsxXSwgY3VyclBvc2l0aW9uW2RpbUluZGV4XSk7XG4gICAgY3VyclBvc2l0aW9uW2RpbUluZGV4XSA9IE1hdGgubWF4KGF4aXNFeHRlbnRbMF0sIGN1cnJQb3NpdGlvbltkaW1JbmRleF0pO1xuICAgIHZhciBjdXJzb3JPdGhlclZhbHVlID0gKG90aGVyRXh0ZW50WzFdICsgb3RoZXJFeHRlbnRbMF0pIC8gMjtcbiAgICB2YXIgY3Vyc29yUG9pbnQgPSBbY3Vyc29yT3RoZXJWYWx1ZSwgY3Vyc29yT3RoZXJWYWx1ZV07XG4gICAgY3Vyc29yUG9pbnRbZGltSW5kZXhdID0gY3VyclBvc2l0aW9uW2RpbUluZGV4XTsgLy8gTWFrZSB0b29sdGlwIGRvIG5vdCBvdmVybGFwIGF4aXNQb2ludGVyIGFuZCBpbiB0aGUgbWlkZGxlIG9mIHRoZSBncmlkLlxuXG4gICAgdmFyIHRvb2x0aXBPcHRpb25zID0gW3tcbiAgICAgIHZlcnRpY2FsQWxpZ246ICdtaWRkbGUnXG4gICAgfSwge1xuICAgICAgYWxpZ246ICdjZW50ZXInXG4gICAgfV07XG4gICAgcmV0dXJuIHtcbiAgICAgIHBvc2l0aW9uOiBjdXJyUG9zaXRpb24sXG4gICAgICByb3RhdGlvbjogdHJhbnNmb3JtLnJvdGF0aW9uLFxuICAgICAgY3Vyc29yUG9pbnQ6IGN1cnNvclBvaW50LFxuICAgICAgdG9vbHRpcE9wdGlvbjogdG9vbHRpcE9wdGlvbnNbZGltSW5kZXhdXG4gICAgfTtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIGdldENhcnRlc2lhbihncmlkLCBheGlzKSB7XG4gIHZhciBvcHQgPSB7fTtcbiAgb3B0W2F4aXMuZGltICsgJ0F4aXNJbmRleCddID0gYXhpcy5pbmRleDtcbiAgcmV0dXJuIGdyaWQuZ2V0Q2FydGVzaWFuKG9wdCk7XG59XG5cbnZhciBwb2ludGVyU2hhcGVCdWlsZGVyID0ge1xuICBsaW5lOiBmdW5jdGlvbiAoYXhpcywgcGl4ZWxWYWx1ZSwgb3RoZXJFeHRlbnQsIGVsU3R5bGUpIHtcbiAgICB2YXIgdGFyZ2V0U2hhcGUgPSB2aWV3SGVscGVyLm1ha2VMaW5lU2hhcGUoW3BpeGVsVmFsdWUsIG90aGVyRXh0ZW50WzBdXSwgW3BpeGVsVmFsdWUsIG90aGVyRXh0ZW50WzFdXSwgZ2V0QXhpc0RpbUluZGV4KGF4aXMpKTtcbiAgICBncmFwaGljLnN1YlBpeGVsT3B0aW1pemVMaW5lKHtcbiAgICAgIHNoYXBlOiB0YXJnZXRTaGFwZSxcbiAgICAgIHN0eWxlOiBlbFN0eWxlXG4gICAgfSk7XG4gICAgcmV0dXJuIHtcbiAgICAgIHR5cGU6ICdMaW5lJyxcbiAgICAgIHNoYXBlOiB0YXJnZXRTaGFwZVxuICAgIH07XG4gIH0sXG4gIHNoYWRvdzogZnVuY3Rpb24gKGF4aXMsIHBpeGVsVmFsdWUsIG90aGVyRXh0ZW50LCBlbFN0eWxlKSB7XG4gICAgdmFyIGJhbmRXaWR0aCA9IE1hdGgubWF4KDEsIGF4aXMuZ2V0QmFuZFdpZHRoKCkpO1xuICAgIHZhciBzcGFuID0gb3RoZXJFeHRlbnRbMV0gLSBvdGhlckV4dGVudFswXTtcbiAgICByZXR1cm4ge1xuICAgICAgdHlwZTogJ1JlY3QnLFxuICAgICAgc2hhcGU6IHZpZXdIZWxwZXIubWFrZVJlY3RTaGFwZShbcGl4ZWxWYWx1ZSAtIGJhbmRXaWR0aCAvIDIsIG90aGVyRXh0ZW50WzBdXSwgW2JhbmRXaWR0aCwgc3Bhbl0sIGdldEF4aXNEaW1JbmRleChheGlzKSlcbiAgICB9O1xuICB9XG59O1xuXG5mdW5jdGlvbiBnZXRBeGlzRGltSW5kZXgoYXhpcykge1xuICByZXR1cm4gYXhpcy5kaW0gPT09ICd4JyA/IDAgOiAxO1xufVxuXG5BeGlzVmlldy5yZWdpc3RlckF4aXNQb2ludGVyQ2xhc3MoJ0NhcnRlc2lhbkF4aXNQb2ludGVyJywgQ2FydGVzaWFuQXhpc1BvaW50ZXIpO1xudmFyIF9kZWZhdWx0ID0gQ2FydGVzaWFuQXhpc1BvaW50ZXI7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBakVBO0FBQ0E7QUFtRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQW5CQTtBQUNBO0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/CartesianAxisPointer.js
