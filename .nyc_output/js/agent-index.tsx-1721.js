__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! echarts/lib/chart/bar */ "./node_modules/echarts/lib/chart/bar.js");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! echarts/lib/chart/line */ "./node_modules/echarts/lib/chart/line.js");
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! echarts/lib/chart/pie */ "./node_modules/echarts/lib/chart/pie.js");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! echarts/lib/component/legend */ "./node_modules/echarts/lib/component/legend.js");
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! echarts/lib/component/legendScroll */ "./node_modules/echarts/lib/component/legendScroll.js");
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! echarts/lib/component/tooltip */ "./node_modules/echarts/lib/component/tooltip.js");
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! echarts/lib/echarts */ "./node_modules/echarts/lib/echarts.js");
/* harmony import */ var echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _agent_index_style__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./agent-index-style */ "./src/app/desk/home/agent-index-style.tsx");
/* harmony import */ var _desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../desk/component/query/prod-type */ "./src/app/desk/component/query/prod-type.tsx");
/* harmony import */ var _index_common_style__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./index-common-style */ "./src/app/desk/home/index-common-style.tsx");
/* harmony import */ var _desk_styles_common__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @desk-styles/common */ "./src/app/desk/styles/common.tsx");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _common_route_common_route__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @common/route/common-route */ "./src/common/route/common-route.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/home/agent-index.tsx";



















var gridTop = {
  left: "1px",
  bottom: "2px",
  top: "10px",
  right: "1px",
  containLabel: false
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var AgentIndex =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(AgentIndex, _ModelWidget);

  function AgentIndex(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, AgentIndex);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(AgentIndex).call(this, props, context));

    _this.onRejectPolicy = function (policy) {
      if (policy.bizType === "COMP") {
        _this.getHelpers().getAjax().patch(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].COMP_QUOTE_REJECT.replace(":compQuoteId", policy.refId), {}).then(function (response) {
          _this.getSalesHistory("9610");
        });
      } else {
        _this.getHelpers().getAjax().post(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].POLICY_REJECT.replace(":policyId", policy.refId), {}).then(function (response) {
          _this.getSalesHistory("9610");
        });
      }
    };

    _this.handleRejectPolicy = function (policy) {
      antd__WEBPACK_IMPORTED_MODULE_9__["Modal"].confirm({
        content: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Are you sure reject now ?").thai("คุณแน่ใจหรือว่าปฏิเสธในตอนนี้?").my("သင်ကငြင်းဆန်သေချာလား?").getMessage(),
        onOk: function onOk() {
          _this.onRejectPolicy(policy);
        },
        okText: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Yes").thai("ใช่").my("ဟုတ်ကဲ့").getMessage(),
        cancelText: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("No").thai("ไม่").my("မဟုတ်").getMessage()
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(AgentIndex, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var isSales = _common__WEBPACK_IMPORTED_MODULE_10__["Authority"].isAgent() || _common__WEBPACK_IMPORTED_MODULE_10__["Authority"].isBroker(); // if (isSales) {

      ["9601", "9602", "9603", "9604", "9605", "9606", "9607", "9608", "9609", "9610", "9611", "9612"].forEach(function (every) {
        _this2.getSalesHistory(every);
      }); // }
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return _agent_index_style__WEBPACK_IMPORTED_MODULE_20__["default"];
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(AgentIndex.prototype), "initState", this).call(this), {
        annualisedReportData: false,
        annualisedPremium: {},
        rankOption: {},
        policies: {},
        policiesReportData: false,
        newBusinessConversion: {},
        renewalRatio: {},
        balanceOption: {},
        kpiOption: {},
        recentTransactions: {},
        newCustomers: {},
        comingBirthdays: {}
      });
    }
  }, {
    key: "getSalesHistory",
    value: function getSalesHistory(reportId, isLoading) {
      var _this3 = this;

      this.getHelpers().getAjax().get(encodeURI("".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].REPORT.replace(":reportId", reportId), "?reportParams={}")), {}, {
        loading: !isLoading
      }).then(function (response) {
        var _ref = response.body || {
          respData: {}
        },
            respData = _ref.respData;

        var _ref2 = respData || {
          option: {}
        },
            option = _ref2.option;

        switch (reportId) {
          case "9601":
            _this3.setState({
              annualisedPremium: option
            });

            break;

          case "9602":
            _this3.getAnnualisedReportData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, {
              grid: gridTop
            }));

            break;

          case "9603":
            _this3.setState({
              rankOption: option
            });

            break;

          case "9604":
            _this3.setState({
              policies: option
            });

            break;

          case "9605":
            _this3.getPoliciesReportData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, {
              grid: gridTop
            }));

            break;

          case "9606":
            _this3.setState({
              newBusinessConversion: option
            });

            break;

          case "9607":
            _this3.setState({
              renewalRatio: option
            });

            break;

          case "9608":
            _this3.setState({
              balanceOption: option
            });

            break;

          case "9609":
            _this3.setState({
              kpiOption: option
            });

            break;

          case "9610":
            _this3.setState({
              recentTransactions: option
            });

            break;

          case "9611":
            _this3.setState({
              newCustomers: option
            }, function () {});

            break;

          case "9612":
            _this3.setState({
              comingBirthdays: option
            });

            break;
        }
      }).catch(function () {
        _this3.getCatch(reportId);
      });
    } //9602

  }, {
    key: "getAnnualisedReportData",
    value: function getAnnualisedReportData(option) {
      this.setState({
        annualisedReportData: true
      });
      var el = document.getElementById("annualisedReportData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        ((option || {}).tooltip || {}).confine = true;
        myChart.setOption(option);
      }
    } //9605

  }, {
    key: "getPoliciesReportData",
    value: function getPoliciesReportData(option) {
      this.setState({
        policiesReportData: true
      });
      var el = document.getElementById("policiesReportData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        ((option || {}).tooltip || {}).confine = true;
        myChart.setOption(option);
      }
    }
  }, {
    key: "getCatch",
    value: function getCatch(reportId) {
      if (reportId === "9602") {
        this.setState({
          annualisedReportData: false
        });
      }

      if (reportId === "9605") {
        this.setState({
          policiesReportData: false
        });
      }
    }
  }, {
    key: "annualisedPremiumRenderTitle",
    value: function annualisedPremiumRenderTitle() {
      var _this$state = this.state,
          annualisedReportData = _this$state.annualisedReportData,
          annualisedPremium = _this$state.annualisedPremium;

      var premium = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "yoy", 0);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("PREMIUM").thai("จำนวนเบี้ยทั้งหมด").my("အပိုဆောင်းကုန်ကျစရိတ်").getMessage(), " ", new Date().getFullYear()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: "Total premium since 1st of Jan this year",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 208
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "info-circle-o",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          fontSize: "16px",
          paddingRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "currencySymbol")), _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "annualizedPremium", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 220
        },
        __self: this
      }, premium >= 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-up",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 221
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-down",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 221
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "percent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 222
        },
        __self: this
      }, Math.abs(parseInt(premium * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 223
        },
        __self: this
      }, "YOY"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          height: 68,
          marginTop: "20px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 226
        },
        __self: this
      }, annualisedReportData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "annualisedReportData",
        style: {
          width: "100%",
          height: 68
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 228
        },
        __self: this
      })));
    }
  }, {
    key: "policiesRenderTitle",
    value: function policiesRenderTitle() {
      var _this$state2 = this.state,
          policies = _this$state2.policies,
          policiesReportData = _this$state2.policiesReportData;

      var premium = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(policies, "yoy", 0);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 238
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("POLICIES").thai("จำนวนกรมธรรม์ทั้งหมด").my("မူဝါဒအ").getMessage(), " ", new Date().getFullYear()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: "Total No. of policies since 1st of Jan this year",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 241
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "info-circle-o",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 244
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 247
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 248
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(policies, "policies", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 249
        },
        __self: this
      }, premium >= 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-up",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-down",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "percent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 251
        },
        __self: this
      }, Math.abs(parseInt(premium * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252
        },
        __self: this
      }, "YOY"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          height: 81,
          marginTop: "7px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255
        },
        __self: this
      }, policiesReportData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "policiesReportData",
        style: {
          width: "100%",
          height: 81
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      })));
    }
  }, {
    key: "balanceRenderTitle",
    value: function balanceRenderTitle() {
      var balanceOption = this.state.balanceOption;

      var status = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(balanceOption, "lastPayment.status");

      var index = ["PAID", "FAILED", "PENDING"].indexOf(status);
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("O/S BALANCE").thai("ยอดหักบัญชีตัวแทนอัตโนมัติ").my("အို / S ကိုချိန်ခွင်လျှာ").getMessage())), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 272
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 273
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          fontSize: "16px",
          paddingRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 274
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(balanceOption, "currencySymbol")), _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(balanceOption, "currBal", 0)))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "payment",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 279
        },
        __self: this
      }, status && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("img", {
        src: __webpack_require__("./src/assets/reports sync recursive ^\\.\\/payment\\-.*\\.svg$")("./payment-".concat(status.toLowerCase(), ".svg")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 280
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          fontSize: "12px",
          paddingRight: "5px",
          color: ["#a6db97", "#df6b53", "#6b6868"][index]
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(balanceOption, "lastPayment.description")))));
    }
  }, {
    key: "renderOpers",
    value: function renderOpers(item) {
      var _this4 = this;

      var _item$opers = item.opers,
          editable = _item$opers.editable,
          rejectable = _item$opers.rejectable,
          viewable = _item$opers.viewable,
          allowToCancelFromInception = _item$opers.allowToCancelFromInception,
          allowToNFnEndo = _item$opers.allowToNFnEndo,
          productCode = item.productCode,
          productCate = item.productCate,
          refId = item.refId,
          itntCode = item.itntCode,
          productVersion = item.productVersion,
          bizType = item.bizType;
      var result = [];
      var moreOper = [];

      if (viewable) {
        result.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          key: "viewable",
          className: "button",
          onClick: function onClick() {
            _this4.getHelpers().getRouter().pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_25__["policyPath"])({
              type: "view",
              productCode: productCode,
              policyId: refId,
              bizType: bizType,
              productVersion: productVersion,
              itntCode: itntCode
            }));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 307
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-view",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 323
          },
          __self: this
        })));
      }

      if (editable) {
        result.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          key: "editable",
          className: "button",
          onClick: function onClick() {
            //TODO need compatible multi insurer tenant
            _this4.getHelpers().getRouter().pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_25__["policyPath"])({
              productCode: productCode,
              policyId: refId,
              bizType: bizType,
              productVersion: productVersion,
              itntCode: itntCode,
              type: "work"
            }));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 330
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-work-on",
          style: {
            fontSize: "16px"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 345
          },
          __self: this
        })));
      }

      if (allowToCancelFromInception) {
        moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: "cancellation",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 353
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          className: "button",
          onClick: function onClick() {
            _this4.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].ENDORSEMENT_ENTRY.replace(":itntCode", item.itntCode).replace(":policyId", item.refId).replace(":endoType", _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].ENDO_TYPES.INCEPTION_CANCELLATION.toLowerCase()));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 354
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Cancel").thai("Cancel").my("Cancel").getMessage())));
      }

      if (allowToNFnEndo) {
        moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: "non-financial",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 371
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          className: "button",
          onClick: function onClick() {
            _this4.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].ENDORSEMENT_ENTRY.replace(":itntCode", item.itntCode).replace(":policyId", item.refId).replace(":endoType", _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].ENDO_TYPES.NFN.toLowerCase()));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 372
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Non-financial Endorsement").thai("การรับรองที่ไม่ใช่ทางการเงิน").my("non-ဘဏ္ဍာရေးလက်မှတ်").getMessage())));
      }

      if (rejectable) {
        moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"].Item, {
          key: "rejectable",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 388
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          className: "button",
          onClick: function onClick() {
            return _this4.handleRejectPolicy(item);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 389
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Reject").thai("ยกเลิกกรมธรรม์").my("ဖျက်သိမ်းခြင်းပေါ်လစီ").getMessage())));
      }

      var menu = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Menu"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 398
        },
        __self: this
      }, moreOper);

      if (moreOper.length > 0) {
        result.push(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Dropdown"], {
          trigger: ["hover", "click"],
          overlay: menu,
          key: "dropdown",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 401
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
          className: "ant-dropdown-link button",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 402
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-more",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 405
          },
          __self: this
        }))));
      }

      if (!result.length) {
        result.push("-");
      }

      return _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].joinElements(result, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Divider"], {
        type: "vertical",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 415
        },
        __self: this
      }));
    }
  }, {
    key: "filterProgress",
    value: function filterProgress(realValue, filterValue) {
      if (realValue > 100) {
        return realValue + "%";
      } else {
        return filterValue + "%";
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var C = this.getComponents();
      var _this$state3 = this.state,
          rankOption = _this$state3.rankOption,
          newBusinessConversion = _this$state3.newBusinessConversion,
          renewalRatio = _this$state3.renewalRatio;
      var _this$state4 = this.state,
          balanceOption = _this$state4.balanceOption,
          kpiOption = _this$state4.kpiOption,
          recentTransactions = _this$state4.recentTransactions,
          newCustomers = _this$state4.newCustomers,
          comingBirthdays = _this$state4.comingBirthdays;

      var conversionRatio = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(newBusinessConversion, "conversionRatio", 0);

      var renewal = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(renewalRatio, "renewalRatio", 0);

      var nextBillingDate = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(balanceOption, "nextBillingDate");

      var customerGrowth = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "customerGrowth", 0) * 100));
      var sales = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "sales", 0) * 100));
      var lossRatio = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "lossRatio", 0) * 100));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.ManagerIndex, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 469
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: isMobile ? "mobile-agent-index" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 470
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        className: "card-100",
        gutter: 16,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 471
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 472
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.annualisedPremiumRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 473
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 476
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 477
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Ranking").thai("การจัดอันดับ").my("ဦးဆောင်သူ").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 478
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(rankOption, "ranking", 0), " / ", lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(rankOption, "totalSales", 0)), " ")))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 482
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.policiesRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 483
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 486
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 487
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Conversion").thai("การแปลง").my("ပြောင်းလဲမှု").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 488
        },
        __self: this
      }, "NB", _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 490
        },
        __self: this
      }, Math.abs(parseInt(conversionRatio * 100)), "%")), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        style: {
          marginLeft: "10px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 492
        },
        __self: this
      }, "RN", _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 494
        },
        __self: this
      }, Math.abs(parseInt(renewal * 100)), "%"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "remark",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 497
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 500
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 501
          },
          __self: this
        }, "New Biz Conversion ", Math.abs(parseInt(conversionRatio * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 502
          },
          __self: this
        }, "Renewal Ratio ", Math.abs(parseInt(renewal * 100)), "%")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 498
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "question-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 506
        },
        __self: this
      })))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 512
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.balanceRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 513
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 516
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 517
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Next billing date: ").thai("วันที่เรียกเก็บเงินถัดไป").my("လာမယ့်ငွေတောင်းခံသည့်ရက်စွဲ").getMessage(), " ", nextBillingDate && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 518
        },
        __self: this
      }, nextBillingDate), " ")))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        className: "card-last",
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 522
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 523
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 524
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 525
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 526
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("KPI ACHIEVEMENTS").thai("อัตราการเติบโตตัวแทนและลูกค้า").my("KPI အောင်မြင်မှုများ").getMessage(), " ", new Date().getFullYear())), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_index_common_style__WEBPACK_IMPORTED_MODULE_22__["default"].KPI, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 528
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "kpi",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 529
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("ul", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 530
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 531
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 532
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Sales").thai("ขาย").my("အရောင်း").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 533
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        format: function format(pp) {
          return _this5.filterProgress(sales, pp);
        },
        strokeColor: "#65a172",
        status: "active",
        percent: sales,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 534
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 539
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 540
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Customer Growth").thai("การเติบโตของลูกค้า").my("ဖောက်သည်တိုးတက်မှုနှုန်း").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 541
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        strokeColor: "#65a172",
        format: function format(pp) {
          return _this5.filterProgress(customerGrowth, pp);
        },
        status: "active",
        percent: customerGrowth,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 542
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 547
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 548
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Loss Ratio").thai("อัตราการสูญเสีย").my("အရှုံးအချိုး").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 549
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        strokeColor: "#ca7a68",
        status: "active",
        format: function format(pp) {
          return _this5.filterProgress(lossRatio, pp);
        },
        percent: lossRatio,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 550
        },
        __self: this
      })))))))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "recent-customers",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 562
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        className: "card-101",
        gutter: 16,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 563
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 12,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 564
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        style: {
          height: isMobile ? "auto" : "100%"
        },
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 565
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 566
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 567
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("RECENT TRANSACTIONS").thai("สถานะกรมธรรม์").my("မကြာသေးမီငွေကြေးလွှဲပြောင်းမှုမှာ").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
        className: "iconfont icon-addnote",
        onClick: function onClick() {
          _this5.props.history.push("".concat(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].NEW_QUOTE_LIST)); // Mask.create({
          //   Component: ({ onCancel, onOk }: any) => <NewQuote onCancel={onCancel}/>,
          // });

        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 568
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "pecent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 575
        },
        __self: this
      }, (lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(recentTransactions, "recentTransactions") || []).slice(0, 10).map(function (every, index) {
        return !isMobile ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
          gutter: 16,
          key: index,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 578
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 3,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 579
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_21__["ProdTypeImage"], {
          productCate: every.productCate,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 580
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          style: {
            color: "rgba(0,0,0,.95)"
          },
          span: 6,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 582
          },
          __self: this
        }, every.insuredDescription && every.insuredDescription.length > 10 && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Popover"], {
          content: every.insuredDescription,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 584
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_23__["StyleMaxLine"], {
          maxLine: "2",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 586
          },
          __self: this
        }, every.insuredDescription)), every.insuredDescription && every.insuredDescription.length <= 10 && every.insuredDescription), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 6,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 593
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].renderPolicyPremium(every)), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 4,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 594
          },
          __self: this
        }, every.statusName), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 6,
          className: "opers",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 595
          },
          __self: this
        }, _this5.renderOpers(every))) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "mobile-percent",
          key: index,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 596
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          className: "info-title",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 597
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_query_prod_type__WEBPACK_IMPORTED_MODULE_21__["ProdTypeImage"], {
          productCate: every.productCate,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 598
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          className: "opers",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 599
          },
          __self: this
        }, _this5.renderOpers(every))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
          gutter: 16,
          key: index,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 601
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          style: {
            color: "rgba(0,0,0,.95)"
          },
          span: 12,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 602
          },
          __self: this
        }, every.insuredDescription), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 7,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 603
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].renderPolicyPremium(every)), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
          span: 5,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 604
          },
          __self: this
        }, every.statusName)));
      })))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 612
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        style: {
          height: isMobile ? "auto" : "100%"
        },
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 613
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 614
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 615
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("NEW CUSTOMERS").thai("รายละเอียดลูกค้า").my("အသစ်ကဖောက်သည်များ").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
        className: "iconfont icon-add-person",
        onClick: function onClick() {
          _this5.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].CUSTOMERS_ENTRY.replace(":customerId", ""));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 616
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "customers-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 620
        },
        __self: this
      }, (lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(newCustomers, "newCustomers") || []).slice(0, 5).map(function (every, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "info",
          key: index,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 623
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "info-title",
          style: {
            display: "flex"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 624
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 625
          },
          __self: this
        }, every.name), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "operate-i",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 626
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-view",
          onClick: function onClick() {
            _common__WEBPACK_IMPORTED_MODULE_10__["Storage"].currentRouters.session().set(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].CUSTOMER_ROUTER, "agent");

            _this5.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].CUSTOMERS_VIEW.replace(":customerId", every.custId));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 627
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Divider"], {
          type: "vertical",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 631
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-work-on",
          style: {
            fontSize: "16px"
          },
          onClick: function onClick() {
            _common__WEBPACK_IMPORTED_MODULE_10__["Storage"].currentRouters.session().set(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].CUSTOMER_ROUTER, "agent");

            _this5.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].CUSTOMERS_ENTRY.replace(":customerId", every.custId));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 632
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Divider"], {
          type: "vertical",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 636
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-addnote",
          style: {
            fontSize: "16px"
          },
          onClick: function onClick() {
            _this5.props.history.push("".concat(_common__WEBPACK_IMPORTED_MODULE_10__["PATH"].NEW_QUOTE_LIST, "?custId=").concat(every.custId)); // Mask.create({
            //   Component: ({ onCancel, onOk }: any) => <NewQuote onCancel={onCancel}
            //                                                     customer={every}/>,
            // });

          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 637
          },
          __self: this
        }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "iphone-email",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 646
          },
          __self: this
        }, every.mobileNo.split(" ")[1] !== "null" && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 649
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-iphone",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 650
          },
          __self: this
        }), every.mobileNo.split(" ")[0] === "null" ? every.mobileNo.split(" ")[1] : every.mobileNo), every.email !== null && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 656
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-email",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 657
          },
          __self: this
        }), every.email)));
      })))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 669
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        style: {
          height: isMobile ? "auto" : "100%"
        },
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 670
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 671
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 672
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("COMING BIRTHDAYS").thai("วันเกิด").my("လာမည့်မွေးနေ့").getMessage())), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "customers-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 674
        },
        __self: this
      }, (lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(comingBirthdays, "comingBirthdays") || []).slice(0, 5).map(function (every, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "info",
          key: index,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 677
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          className: "info-title",
          style: {
            display: "flex"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 678
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 679
          },
          __self: this
        }, every.name), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 680
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          style: {
            paddingRight: "5px"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 681
          },
          __self: this
        }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(every, "age", "")), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-birthday",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 682
          },
          __self: this
        }), every.dob)), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "iphone-email",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 686
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 687
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-iphone",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 688
          },
          __self: this
        }), every.mobileNo), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 691
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("i", {
          className: "iconfont icon-email",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 692
          },
          __self: this
        }), every.email)));
      }))))))));
    }
  }]);

  return AgentIndex;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_24__["withRouter"])(AgentIndex));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svaG9tZS9hZ2VudC1pbmRleC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9ob21lL2FnZW50LWluZGV4LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQ2FyZCwgQ29sLCBEaXZpZGVyLCBEcm9wZG93biwgSWNvbiwgTWVudSwgTW9kYWwsIFBvcG92ZXIsIFByb2dyZXNzLCBSb3csIFRvb2x0aXAgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgQXBpcywgQXV0aG9yaXR5LCBDb25zdHMsIExhbmd1YWdlLCBQQVRILCBTdG9yYWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jaGFydC9iYXJcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NoYXJ0L2xpbmVcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NoYXJ0L3BpZVwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZFwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZFNjcm9sbFwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY29tcG9uZW50L3RpdGxlXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jb21wb25lbnQvdG9vbHRpcFwiO1xuaW1wb3J0IEVDaGFydHMgZnJvbSBcImVjaGFydHMvbGliL2VjaGFydHNcIjtcbmltcG9ydCBBZ2VudEluZGV4U3R5bGUgZnJvbSBcIi4vYWdlbnQtaW5kZXgtc3R5bGVcIjtcbmltcG9ydCB7IFByb2RUeXBlSW1hZ2UgfSBmcm9tIFwiLi4vLi4vZGVzay9jb21wb25lbnQvcXVlcnkvcHJvZC10eXBlXCI7XG5pbXBvcnQgSW5kZXhDb21tb25TdHlsZSBmcm9tIFwiLi9pbmRleC1jb21tb24tc3R5bGVcIjtcbmltcG9ydCB7IFN0eWxlTWF4TGluZSB9IGZyb20gXCJAZGVzay1zdHlsZXMvY29tbW9uXCI7XG5pbXBvcnQgeyBSb3V0ZUNvbXBvbmVudFByb3BzLCB3aXRoUm91dGVyIH0gZnJvbSBcInJlYWN0LXJvdXRlclwiO1xuaW1wb3J0IHsgcG9saWN5UGF0aCB9IGZyb20gXCJAY29tbW9uL3JvdXRlL2NvbW1vbi1yb3V0ZVwiO1xuXG5jb25zdCBncmlkVG9wID0ge1xuICBsZWZ0OiBcIjFweFwiLFxuICBib3R0b206IFwiMnB4XCIsXG4gIHRvcDogXCIxMHB4XCIsXG4gIHJpZ2h0OiBcIjFweFwiLFxuICBjb250YWluTGFiZWw6IGZhbHNlLFxufTtcbnR5cGUgQWdlbnRJbmRleFByb3BzID0ge30gJiBNb2RlbFdpZGdldFByb3BzICYgUm91dGVDb21wb25lbnRQcm9wcztcblxudHlwZSBBZ2VudEluZGV4U3RhdGUgPSB7XG4gIGFubnVhbGlzZWRSZXBvcnREYXRhOiBib29sZWFuO1xuICBhbm51YWxpc2VkUHJlbWl1bTogYW55O1xuICByYW5rT3B0aW9uOiBhbnk7XG4gIHBvbGljaWVzOiBhbnk7XG4gIHBvbGljaWVzUmVwb3J0RGF0YTogYm9vbGVhbjtcbiAgbmV3QnVzaW5lc3NDb252ZXJzaW9uOiBhbnk7XG4gIHJlbmV3YWxSYXRpbzogYW55O1xuICBiYWxhbmNlT3B0aW9uOiBhbnk7XG4gIGtwaU9wdGlvbjogYW55O1xuICByZWNlbnRUcmFuc2FjdGlvbnM6IGFueTtcbiAgbmV3Q3VzdG9tZXJzOiBhbnk7XG4gIGNvbWluZ0JpcnRoZGF5czogYW55O1xufVxudHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxudHlwZSBNYW5hZ2VySW5kZXhDb21wb25lbnRzID0ge1xuICBNYW5hZ2VySW5kZXg6IFN0eWxlZERJVixcbn1cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcblxuY2xhc3MgQWdlbnRJbmRleDxQIGV4dGVuZHMgQWdlbnRJbmRleFByb3BzLCBTIGV4dGVuZHMgQWdlbnRJbmRleFN0YXRlLCBDIGV4dGVuZHMgTWFuYWdlckluZGV4Q29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IEFnZW50SW5kZXhQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IGlzU2FsZXMgPSBBdXRob3JpdHkuaXNBZ2VudCgpIHx8IEF1dGhvcml0eS5pc0Jyb2tlcigpO1xuICAgIC8vIGlmIChpc1NhbGVzKSB7XG4gICAgW1wiOTYwMVwiLCBcIjk2MDJcIiwgXCI5NjAzXCIsIFwiOTYwNFwiLCBcIjk2MDVcIiwgXCI5NjA2XCIsIFwiOTYwN1wiLCBcIjk2MDhcIiwgXCI5NjA5XCIsIFwiOTYxMFwiLCBcIjk2MTFcIiwgXCI5NjEyXCJdLmZvckVhY2goKGV2ZXJ5OiBhbnkpID0+IHtcbiAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KGV2ZXJ5KTtcbiAgICB9KTtcbiAgICAvLyB9XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIEFnZW50SW5kZXhTdHlsZSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgYW5udWFsaXNlZFJlcG9ydERhdGE6IGZhbHNlLFxuICAgICAgYW5udWFsaXNlZFByZW1pdW06IHt9LFxuICAgICAgcmFua09wdGlvbjoge30sXG4gICAgICBwb2xpY2llczoge30sXG4gICAgICBwb2xpY2llc1JlcG9ydERhdGE6IGZhbHNlLFxuICAgICAgbmV3QnVzaW5lc3NDb252ZXJzaW9uOiB7fSxcbiAgICAgIHJlbmV3YWxSYXRpbzoge30sXG4gICAgICBiYWxhbmNlT3B0aW9uOiB7fSxcbiAgICAgIGtwaU9wdGlvbjoge30sXG4gICAgICByZWNlbnRUcmFuc2FjdGlvbnM6IHt9LFxuICAgICAgbmV3Q3VzdG9tZXJzOiB7fSxcbiAgICAgIGNvbWluZ0JpcnRoZGF5czoge30sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByaXZhdGUgZ2V0U2FsZXNIaXN0b3J5KHJlcG9ydElkOiBzdHJpbmcsIGlzTG9hZGluZz86IGJvb2xlYW4pIHtcbiAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgLmdldEFqYXgoKS5nZXQoZW5jb2RlVVJJKGAke0FwaXMuUkVQT1JULnJlcGxhY2UoXCI6cmVwb3J0SWRcIiwgcmVwb3J0SWQpfT9yZXBvcnRQYXJhbXM9e31gKSwge30sIHtcbiAgICAgIGxvYWRpbmc6ICFpc0xvYWRpbmcsXG4gICAgfSlcbiAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHkgfHwgeyByZXNwRGF0YToge30gfTtcbiAgICAgICAgY29uc3QgeyBvcHRpb24gfSA9IHJlc3BEYXRhIHx8IHsgb3B0aW9uOiB7fSB9O1xuICAgICAgICBzd2l0Y2ggKHJlcG9ydElkKSB7XG4gICAgICAgICAgY2FzZSBcIjk2MDFcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBhbm51YWxpc2VkUHJlbWl1bTogb3B0aW9uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTYwMlwiOlxuICAgICAgICAgICAgdGhpcy5nZXRBbm51YWxpc2VkUmVwb3J0RGF0YSh7IC4uLm9wdGlvbiwgLi4ueyBncmlkOiBncmlkVG9wIH0gfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTYwM1wiOlxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgIHJhbmtPcHRpb246IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MDRcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBwb2xpY2llczogb3B0aW9uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTYwNVwiOlxuICAgICAgICAgICAgdGhpcy5nZXRQb2xpY2llc1JlcG9ydERhdGEoeyAuLi5vcHRpb24sIC4uLnsgZ3JpZDogZ3JpZFRvcCB9IH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MDZcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBuZXdCdXNpbmVzc0NvbnZlcnNpb246IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MDdcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICByZW5ld2FsUmF0aW86IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MDhcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBiYWxhbmNlT3B0aW9uOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NjA5XCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAga3BpT3B0aW9uOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NjEwXCI6XG5cbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICByZWNlbnRUcmFuc2FjdGlvbnM6IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MTFcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBuZXdDdXN0b21lcnM6IG9wdGlvbixcbiAgICAgICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk2MTJcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBjb21pbmdCaXJ0aGRheXM6IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH0pLmNhdGNoKCgpID0+IHtcbiAgICAgIHRoaXMuZ2V0Q2F0Y2gocmVwb3J0SWQpO1xuICAgIH0pO1xuICB9XG5cbiAgLy85NjAyXG4gIHByaXZhdGUgZ2V0QW5udWFsaXNlZFJlcG9ydERhdGEob3B0aW9uOiBhbnkpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGFubnVhbGlzZWRSZXBvcnREYXRhOiB0cnVlLFxuICAgIH0pO1xuICAgIGxldCBlbDogYW55ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhbm51YWxpc2VkUmVwb3J0RGF0YVwiKTtcbiAgICBpZiAoISFlbCkge1xuICAgICAgY29uc3QgbXlDaGFydCA9IEVDaGFydHMuaW5pdChlbCk7XG4gICAgICAoKG9wdGlvbiB8fCB7fSkudG9vbHRpcCB8fCB7fSkuY29uZmluZSA9IHRydWU7XG4gICAgICBteUNoYXJ0LnNldE9wdGlvbihvcHRpb24pO1xuICAgIH1cbiAgfVxuXG4gIC8vOTYwNVxuICBnZXRQb2xpY2llc1JlcG9ydERhdGEob3B0aW9uOiBhbnkpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHBvbGljaWVzUmVwb3J0RGF0YTogdHJ1ZSxcbiAgICB9KTtcbiAgICBsZXQgZWw6IGFueSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9saWNpZXNSZXBvcnREYXRhXCIpO1xuICAgIGlmICghIWVsKSB7XG4gICAgICBjb25zdCBteUNoYXJ0ID0gRUNoYXJ0cy5pbml0KGVsKTtcbiAgICAgICgob3B0aW9uIHx8IHt9KS50b29sdGlwIHx8IHt9KS5jb25maW5lID0gdHJ1ZTtcbiAgICAgIG15Q2hhcnQuc2V0T3B0aW9uKG9wdGlvbik7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBnZXRDYXRjaChyZXBvcnRJZDogc3RyaW5nKSB7XG4gICAgaWYgKHJlcG9ydElkID09PSBcIjk2MDJcIikge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFubnVhbGlzZWRSZXBvcnREYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAocmVwb3J0SWQgPT09IFwiOTYwNVwiKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgcG9saWNpZXNSZXBvcnREYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYW5udWFsaXNlZFByZW1pdW1SZW5kZXJUaXRsZSgpIHtcbiAgICBjb25zdCB7IGFubnVhbGlzZWRSZXBvcnREYXRhLCBhbm51YWxpc2VkUHJlbWl1bSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBwcmVtaXVtID0gXy5nZXQoYW5udWFsaXNlZFByZW1pdW0sIFwieW95XCIsIDApO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJQUkVNSVVNXCIpLnRoYWkoXCLguIjguLPguJnguKfguJnguYDguJrguLXguYnguKLguJfguLHguYnguIfguKvguKHguJRcIikubXkoXCLhgKHhgJXhgK3hgK/hgIbhgLHhgKzhgIThgLrhgLjhgIDhgK/hgJThgLrhgIDhgLvhgIXhgJvhgK3hgJDhgLpcIikuZ2V0TWVzc2FnZSgpfSB7bmV3IERhdGUoKS5nZXRGdWxsWWVhcigpfTwvc3Bhbj5cbiAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgdGl0bGU9XCJUb3RhbCBwcmVtaXVtIHNpbmNlIDFzdCBvZiBKYW4gdGhpcyB5ZWFyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8SWNvbiB0eXBlPVwiaW5mby1jaXJjbGUtb1wiLz5cbiAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb25leVwiPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm1vbmV5LWxlZnRcIj5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxNnB4XCIsIHBhZGRpbmdSaWdodDogXCI1cHhcIiB9fT57Xy5nZXQoYW5udWFsaXNlZFByZW1pdW0sIFwiY3VycmVuY3lTeW1ib2xcIil9PC9zcGFuPlxuICAgICAgICAgICAge1V0aWxzLnRvVGhvdXNhbmRzKF8uZ2V0KGFubnVhbGlzZWRQcmVtaXVtLCBcImFubnVhbGl6ZWRQcmVtaXVtXCIsIDApKX1cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibW9uZXktcmlnaHRcIj5cbiAgICAgICAgICAgIHtwcmVtaXVtID49IDAgPyA8SWNvbiB0eXBlPVwiY2FyZXQtdXBcIi8+IDogPEljb24gdHlwZT1cImNhcmV0LWRvd25cIi8+fVxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicGVyY2VudFwiPntNYXRoLmFicyhwYXJzZUludCgocHJlbWl1bSAqIDEwMCkgYXMgYW55KSl9JTwvc3Bhbj5cbiAgICAgICAgICAgIDxsYWJlbD5ZT1k8L2xhYmVsPlxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3sgaGVpZ2h0OiA2OCwgbWFyZ2luVG9wOiBcIjIwcHhcIiB9fT5cbiAgICAgICAgICB7YW5udWFsaXNlZFJlcG9ydERhdGEgJiZcbiAgICAgICAgICA8ZGl2IGlkPVwiYW5udWFsaXNlZFJlcG9ydERhdGFcIiBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIsIGhlaWdodDogNjggfX0vPn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSBwb2xpY2llc1JlbmRlclRpdGxlKCkge1xuICAgIGNvbnN0IHsgcG9saWNpZXMsIHBvbGljaWVzUmVwb3J0RGF0YSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBwcmVtaXVtID0gXy5nZXQocG9saWNpZXMsIFwieW95XCIsIDApO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJQT0xJQ0lFU1wiKS50aGFpKFwi4LiI4Liz4LiZ4Lin4LiZ4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmM4LiX4Lix4LmJ4LiH4Lir4Lih4LiUXCIpLm15KFwi4YCZ4YCw4YCd4YCr4YCS4YChXCIpLmdldE1lc3NhZ2UoKX0ge25ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKX08L3NwYW4+XG4gICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgIHRpdGxlPVwiVG90YWwgTm8uIG9mIHBvbGljaWVzIHNpbmNlIDFzdCBvZiBKYW4gdGhpcyB5ZWFyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8SWNvbiB0eXBlPVwiaW5mby1jaXJjbGUtb1wiLz5cbiAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb25leVwiPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm1vbmV5LWxlZnRcIj57VXRpbHMudG9UaG91c2FuZHMoXy5nZXQocG9saWNpZXMsIFwicG9saWNpZXNcIiwgMCkpfTwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJtb25leS1yaWdodFwiPlxuICAgICAgICAgICAge3ByZW1pdW0gPj0gMCA/IDxJY29uIHR5cGU9XCJjYXJldC11cFwiLz4gOiA8SWNvbiB0eXBlPVwiY2FyZXQtZG93blwiLz59XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwZXJjZW50XCI+e01hdGguYWJzKHBhcnNlSW50KChwcmVtaXVtICogMTAwKSBhcyBhbnkpKX0lPC9zcGFuPlxuICAgICAgICAgICAgPGxhYmVsPllPWTwvbGFiZWw+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDgxLCBtYXJnaW5Ub3A6IFwiN3B4XCIgfX0+XG4gICAgICAgICAge3BvbGljaWVzUmVwb3J0RGF0YSAmJlxuICAgICAgICAgIDxkaXYgaWQ9XCJwb2xpY2llc1JlcG9ydERhdGFcIiBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIsIGhlaWdodDogODEgfX0vPn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSBiYWxhbmNlUmVuZGVyVGl0bGUoKSB7XG4gICAgY29uc3QgeyBiYWxhbmNlT3B0aW9uIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHN0YXR1cyA9IF8uZ2V0KGJhbGFuY2VPcHRpb24sIFwibGFzdFBheW1lbnQuc3RhdHVzXCIpO1xuICAgIGNvbnN0IGluZGV4ID0gW1wiUEFJRFwiLCBcIkZBSUxFRFwiLCBcIlBFTkRJTkdcIl0uaW5kZXhPZihzdGF0dXMpO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJPL1MgQkFMQU5DRVwiKS50aGFpKFwi4Lii4Lit4LiU4Lir4Lix4LiB4Lia4Lix4LiN4LiK4Li14LiV4Lix4Lin4LmB4LiX4LiZ4Lit4Lix4LiV4LmC4LiZ4Lih4Lix4LiV4Li0XCIpLm15KFwi4YCh4YCt4YCvIC8gUyDhgIDhgK3hgK/hgIHhgLvhgK3hgJThgLrhgIHhgL3hgIThgLrhgJzhgLvhgL7hgKxcIikuZ2V0TWVzc2FnZSgpfTwvc3Bhbj5cbiAgICAgICAgPC9wPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1vbmV5XCI+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibW9uZXktbGVmdFwiPlxuICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgZm9udFNpemU6IFwiMTZweFwiLCBwYWRkaW5nUmlnaHQ6IFwiNXB4XCIgfX0+e18uZ2V0KGJhbGFuY2VPcHRpb24sIFwiY3VycmVuY3lTeW1ib2xcIil9PC9zcGFuPlxuICAgICAgICAgICAge1V0aWxzLnRvVGhvdXNhbmRzKF8uZ2V0KGJhbGFuY2VPcHRpb24sIFwiY3VyckJhbFwiLCAwKSl9XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYXltZW50XCI+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICB7c3RhdHVzICYmIDxpbWcgc3JjPXtyZXF1aXJlKGAuLi8uLi8uLi9hc3NldHMvcmVwb3J0cy9wYXltZW50LSR7c3RhdHVzLnRvTG93ZXJDYXNlKCl9LnN2Z2ApfS8+fVxuICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgZm9udFNpemU6IFwiMTJweFwiLCBwYWRkaW5nUmlnaHQ6IFwiNXB4XCIsIGNvbG9yOiBbXCIjYTZkYjk3XCIsIFwiI2RmNmI1M1wiLCBcIiM2YjY4NjhcIl1baW5kZXhdIH19PlxuICAgICAgICAgICAgICB7Xy5nZXQoYmFsYW5jZU9wdGlvbiwgXCJsYXN0UGF5bWVudC5kZXNjcmlwdGlvblwiKX1cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyT3BlcnMoaXRlbTogYW55KSB7XG4gICAgY29uc3Qge1xuICAgICAgb3BlcnM6IHtcbiAgICAgICAgZWRpdGFibGUsXG4gICAgICAgIHJlamVjdGFibGUsXG4gICAgICAgIHZpZXdhYmxlLFxuICAgICAgICBhbGxvd1RvQ2FuY2VsRnJvbUluY2VwdGlvbixcbiAgICAgICAgYWxsb3dUb05GbkVuZG8sXG4gICAgICB9LCBwcm9kdWN0Q29kZSwgcHJvZHVjdENhdGUsIHJlZklkLFxuICAgICAgaXRudENvZGUsXG4gICAgICBwcm9kdWN0VmVyc2lvbixcbiAgICAgIGJpelR5cGUsXG4gICAgfSA9IGl0ZW07XG4gICAgY29uc3QgcmVzdWx0ID0gW107XG4gICAgY29uc3QgbW9yZU9wZXIgPSBbXTtcbiAgICBpZiAodmlld2FibGUpIHtcbiAgICAgIHJlc3VsdC5wdXNoKFxuICAgICAgICA8YVxuICAgICAgICAgIGtleT1cInZpZXdhYmxlXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCJidXR0b25cIlxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgIC5nZXRSb3V0ZXIoKVxuICAgICAgICAgICAgICAucHVzaFJlZGlyZWN0KHBvbGljeVBhdGgoe1xuICAgICAgICAgICAgICAgIHR5cGU6IFwidmlld1wiLFxuICAgICAgICAgICAgICAgIHByb2R1Y3RDb2RlLFxuICAgICAgICAgICAgICAgIHBvbGljeUlkOiByZWZJZCxcbiAgICAgICAgICAgICAgICBiaXpUeXBlLFxuICAgICAgICAgICAgICAgIHByb2R1Y3RWZXJzaW9uLFxuICAgICAgICAgICAgICAgIGl0bnRDb2RlLFxuICAgICAgICAgICAgICB9KSk7XG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tdmlld1wiLz5cbiAgICAgICAgPC9hPixcbiAgICAgICk7XG4gICAgfVxuXG4gICAgaWYgKGVkaXRhYmxlKSB7XG4gICAgICByZXN1bHQucHVzaChcbiAgICAgICAgPGFcbiAgICAgICAgICBrZXk9XCJlZGl0YWJsZVwiXG4gICAgICAgICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAvL1RPRE8gbmVlZCBjb21wYXRpYmxlIG11bHRpIGluc3VyZXIgdGVuYW50XG4gICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5wdXNoUmVkaXJlY3QoXG4gICAgICAgICAgICAgIHBvbGljeVBhdGgoe1xuICAgICAgICAgICAgICAgIHByb2R1Y3RDb2RlLFxuICAgICAgICAgICAgICAgIHBvbGljeUlkOiByZWZJZCxcbiAgICAgICAgICAgICAgICBiaXpUeXBlLFxuICAgICAgICAgICAgICAgIHByb2R1Y3RWZXJzaW9uLFxuICAgICAgICAgICAgICAgIGl0bnRDb2RlLFxuICAgICAgICAgICAgICAgIHR5cGU6IFwid29ya1wiLFxuICAgICAgICAgICAgICB9KSk7XG4gICAgICAgICAgfX0+XG4gICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbmZvbnQgaWNvbi13b3JrLW9uXCIgc3R5bGU9e3sgZm9udFNpemU6IFwiMTZweFwiIH19Lz5cbiAgICAgICAgICB7Lyp7TGFuZ3VhZ2UuZW4oXCJXb3JrIE9uXCIpLnRoYWkoXCLguYHguIHguYnguYTguIJcIikuZ2V0TWVzc2FnZSgpfSovfVxuICAgICAgICA8L2E+LFxuICAgICAgKTtcbiAgICB9XG5cbiAgICBpZiAoYWxsb3dUb0NhbmNlbEZyb21JbmNlcHRpb24pIHtcbiAgICAgIG1vcmVPcGVyLnB1c2goXG4gICAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiY2FuY2VsbGF0aW9uXCI+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpLmdldFJvdXRlcigpLnB1c2hSZWRpcmVjdChQQVRILkVORE9SU0VNRU5UX0VOVFJZXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRlbS5pdG50Q29kZSlcbiAgICAgICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBpdGVtLnJlZklkKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9UeXBlXCIsIENvbnN0cy5FTkRPX1RZUEVTLklOQ0VQVElPTl9DQU5DRUxMQVRJT04udG9Mb3dlckNhc2UoKSkpO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDYW5jZWxcIikudGhhaShcIkNhbmNlbFwiKS5teShcIkNhbmNlbFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPC9hPlxuICAgICAgICA8L01lbnUuSXRlbT4sXG4gICAgICApO1xuICAgIH1cblxuICAgIGlmIChhbGxvd1RvTkZuRW5kbykge1xuICAgICAgbW9yZU9wZXIucHVzaChcbiAgICAgICAgPE1lbnUuSXRlbSBrZXk9XCJub24tZmluYW5jaWFsXCI+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpLmdldFJvdXRlcigpLnB1c2hSZWRpcmVjdChQQVRILkVORE9SU0VNRU5UX0VOVFJZXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRlbS5pdG50Q29kZSlcbiAgICAgICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBpdGVtLnJlZklkKVxuICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9UeXBlXCIsIENvbnN0cy5FTkRPX1RZUEVTLk5GTi50b0xvd2VyQ2FzZSgpKSk7XG4gICAgICAgICAgICB9fT5cbiAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIk5vbi1maW5hbmNpYWwgRW5kb3JzZW1lbnRcIikudGhhaShcIuC4geC4suC4o+C4o+C4seC4muC4o+C4reC4h+C4l+C4teC5iOC5hOC4oeC5iOC5g+C4iuC5iOC4l+C4suC4h+C4geC4suC4o+C5gOC4h+C4tOC4mVwiKS5teShcIm5vbi3hgJjhgI/hgLnhgI3hgKzhgJvhgLHhgLjhgJzhgIDhgLrhgJnhgL7hgJDhgLpcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDwvYT5cbiAgICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICAgKTtcbiAgICB9XG5cbiAgICBpZiAocmVqZWN0YWJsZSkge1xuICAgICAgbW9yZU9wZXIucHVzaChcbiAgICAgICAgPE1lbnUuSXRlbSBrZXk9XCJyZWplY3RhYmxlXCI+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZVJlamVjdFBvbGljeShpdGVtKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJSZWplY3RcIikudGhhaShcIuC4ouC4geC5gOC4peC4tOC4geC4geC4o+C4oeC4mOC4o+C4o+C4oeC5jFwiKS5teShcIuGAluGAu+GAgOGAuuGAnuGAreGAmeGAuuGAuOGAgeGAvOGAhOGAuuGAuOGAleGAseGAq+GAuuGAnOGAheGArlwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPC9hPlxuICAgICAgICA8L01lbnUuSXRlbT4sXG4gICAgICApO1xuICAgIH1cbiAgICBjb25zdCBtZW51ID0gPE1lbnU+e21vcmVPcGVyfTwvTWVudT47XG4gICAgaWYgKG1vcmVPcGVyLmxlbmd0aCA+IDApIHtcbiAgICAgIHJlc3VsdC5wdXNoKFxuICAgICAgICA8RHJvcGRvd24gdHJpZ2dlcj17W1wiaG92ZXJcIiwgXCJjbGlja1wiXX0gb3ZlcmxheT17bWVudX0ga2V5PVwiZHJvcGRvd25cIj5cbiAgICAgICAgICA8YVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtgYW50LWRyb3Bkb3duLWxpbmsgYnV0dG9uYH1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLW1vcmVcIi8+XG4gICAgICAgICAgPC9hPlxuICAgICAgICA8L0Ryb3Bkb3duPixcbiAgICAgICk7XG4gICAgfVxuXG4gICAgaWYgKCFyZXN1bHQubGVuZ3RoKSB7XG4gICAgICByZXN1bHQucHVzaChcIi1cIik7XG4gICAgfVxuXG4gICAgcmV0dXJuIFV0aWxzLmpvaW5FbGVtZW50cyhyZXN1bHQsIDxEaXZpZGVyIHR5cGU9XCJ2ZXJ0aWNhbFwiLz4pO1xuICB9XG5cbiAgcHJpdmF0ZSBvblJlamVjdFBvbGljeSA9IChwb2xpY3k6IGFueSkgPT4ge1xuICAgIGlmIChwb2xpY3kuYml6VHlwZSA9PT0gXCJDT01QXCIpIHtcbiAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgLnBhdGNoKEFwaXMuQ09NUF9RVU9URV9SRUpFQ1QucmVwbGFjZShcIjpjb21wUXVvdGVJZFwiLCBwb2xpY3kucmVmSWQpLCB7fSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICB0aGlzLmdldFNhbGVzSGlzdG9yeShcIjk2MTBcIik7XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgIC5wb3N0KEFwaXMuUE9MSUNZX1JFSkVDVC5yZXBsYWNlKFwiOnBvbGljeUlkXCIsIHBvbGljeS5yZWZJZCksIHt9KVxuICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KFwiOTYxMFwiKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG5cbiAgfTtcblxuICBwcml2YXRlIGhhbmRsZVJlamVjdFBvbGljeSA9IChwb2xpY3k6IGFueSkgPT4ge1xuICAgIE1vZGFsLmNvbmZpcm0oe1xuICAgICAgY29udGVudDogTGFuZ3VhZ2UuZW4oXCJBcmUgeW91IHN1cmUgcmVqZWN0IG5vdyA/XCIpLnRoYWkoXCLguITguLjguJPguYHguJnguYjguYPguIjguKvguKPguLfguK3guKfguYjguLLguJvguI/guLTguYDguKrguJjguYPguJnguJXguK3guJnguJnguLXguYk/XCIpLm15KFwi4YCe4YCE4YC64YCA4YCE4YC84YCE4YC64YC44YCG4YCU4YC64YCe4YCx4YCB4YC74YCs4YCc4YCs4YC4P1wiKS5nZXRNZXNzYWdlKCksXG4gICAgICBvbk9rOiAoKSA9PiB7XG4gICAgICAgIHRoaXMub25SZWplY3RQb2xpY3kocG9saWN5KTtcbiAgICAgIH0sXG4gICAgICBva1RleHQ6IExhbmd1YWdlLmVuKFwiWWVzXCIpLnRoYWkoXCLguYPguIrguYhcIikubXkoXCLhgJ/hgK/hgJDhgLrhgIDhgLLhgLdcIikuZ2V0TWVzc2FnZSgpLFxuICAgICAgY2FuY2VsVGV4dDogTGFuZ3VhZ2UuZW4oXCJOb1wiKS50aGFpKFwi4LmE4Lih4LmIXCIpLm15KFwi4YCZ4YCf4YCv4YCQ4YC6XCIpLmdldE1lc3NhZ2UoKSxcbiAgICB9KTtcbiAgfTtcblxuICBwcm90ZWN0ZWQgZmlsdGVyUHJvZ3Jlc3MocmVhbFZhbHVlOiBudW1iZXIsIGZpbHRlclZhbHVlOiBudW1iZXIpIHtcbiAgICBpZiAocmVhbFZhbHVlID4gMTAwKSB7XG4gICAgICByZXR1cm4gcmVhbFZhbHVlICsgXCIlXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBmaWx0ZXJWYWx1ZSArIFwiJVwiO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyByYW5rT3B0aW9uLCBuZXdCdXNpbmVzc0NvbnZlcnNpb24sIHJlbmV3YWxSYXRpbyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGJhbGFuY2VPcHRpb24sIGtwaU9wdGlvbiwgcmVjZW50VHJhbnNhY3Rpb25zLCBuZXdDdXN0b21lcnMsIGNvbWluZ0JpcnRoZGF5cyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBsZXQgY29udmVyc2lvblJhdGlvOiBhbnkgPSBfLmdldChuZXdCdXNpbmVzc0NvbnZlcnNpb24sIFwiY29udmVyc2lvblJhdGlvXCIsIDApO1xuICAgIGxldCByZW5ld2FsOiBhbnkgPSBfLmdldChyZW5ld2FsUmF0aW8sIFwicmVuZXdhbFJhdGlvXCIsIDApO1xuICAgIGxldCBuZXh0QmlsbGluZ0RhdGU6IGFueSA9IF8uZ2V0KGJhbGFuY2VPcHRpb24sIFwibmV4dEJpbGxpbmdEYXRlXCIpO1xuICAgIGxldCBjdXN0b21lckdyb3d0aCA9IE1hdGguYWJzKHBhcnNlSW50KChfLmdldChrcGlPcHRpb24sIFwiY3VzdG9tZXJHcm93dGhcIiwgMCkgKiAxMDApIGFzIGFueSkpO1xuICAgIGxldCBzYWxlcyA9IE1hdGguYWJzKHBhcnNlSW50KChfLmdldChrcGlPcHRpb24sIFwic2FsZXNcIiwgMCkgKiAxMDApIGFzIGFueSkpO1xuICAgIGxldCBsb3NzUmF0aW8gPSBNYXRoLmFicyhwYXJzZUludCgoXy5nZXQoa3BpT3B0aW9uLCBcImxvc3NSYXRpb1wiLCAwKSAqIDEwMCkgYXMgYW55KSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEMuTWFuYWdlckluZGV4PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17aXNNb2JpbGUgPyBcIm1vYmlsZS1hZ2VudC1pbmRleFwiIDogXCJcIn0+XG4gICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJjYXJkLTEwMFwiIGd1dHRlcj17MTZ9PlxuICAgICAgICAgICAgPENvbCBzcGFuPXs2fT5cbiAgICAgICAgICAgICAgPENhcmQgdGl0bGU9e1xuICAgICAgICAgICAgICAgIHRoaXMuYW5udWFsaXNlZFByZW1pdW1SZW5kZXJUaXRsZSgpXG4gICAgICAgICAgICAgIH0gYm9yZGVyZWQ9e2ZhbHNlfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtYm9keVwiPlxuICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiUmFua2luZ1wiKS50aGFpKFwi4LiB4Liy4Lij4LiI4Lix4LiU4Lit4Lix4LiZ4LiU4Lix4LiaXCIpLm15KFwi4YCl4YCu4YC44YCG4YCx4YCs4YCE4YC64YCe4YCwXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsPntfLmdldChyYW5rT3B0aW9uLCBcInJhbmtpbmdcIiwgMCl9IC8ge18uZ2V0KHJhbmtPcHRpb24sIFwidG90YWxTYWxlc1wiLCAwKX08L2xhYmVsPiA8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgPENvbCBzcGFuPXs2fT5cbiAgICAgICAgICAgICAgPENhcmQgdGl0bGU9e1xuICAgICAgICAgICAgICAgIHRoaXMucG9saWNpZXNSZW5kZXJUaXRsZSgpXG4gICAgICAgICAgICAgIH0gYm9yZGVyZWQ9e2ZhbHNlfT5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcmQtYm9keVwiPlxuICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiQ29udmVyc2lvblwiKS50aGFpKFwi4LiB4Liy4Lij4LmB4Lib4Lil4LiHXCIpLm15KFwi4YCV4YC84YCx4YCs4YCE4YC64YC44YCc4YCy4YCZ4YC+4YCvXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgIE5CXG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e01hdGguYWJzKHBhcnNlSW50KChjb252ZXJzaW9uUmF0aW8gKiAxMDApIGFzIGFueSkpfSU8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBzdHlsZT17eyBtYXJnaW5MZWZ0OiBcIjEwcHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICBSTlxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntNYXRoLmFicyhwYXJzZUludCgocmVuZXdhbCAqIDEwMCkgYXMgYW55KSl9JTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInJlbWFya1wiPlxuICAgICAgICAgICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxwPk5ldyBCaXogQ29udmVyc2lvbiB7TWF0aC5hYnMocGFyc2VJbnQoKGNvbnZlcnNpb25SYXRpbyAqIDEwMCkgYXMgYW55KSl9JTwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+UmVuZXdhbCBSYXRpbyB7TWF0aC5hYnMocGFyc2VJbnQoKHJlbmV3YWwgKiAxMDApIGFzIGFueSkpfSU8L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwicXVlc3Rpb24tY2lyY2xlXCIvPlxuICAgICAgICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgPENvbCBzcGFuPXs2fT5cbiAgICAgICAgICAgICAgPENhcmQgdGl0bGU9e1xuICAgICAgICAgICAgICAgIHRoaXMuYmFsYW5jZVJlbmRlclRpdGxlKClcbiAgICAgICAgICAgICAgfSBib3JkZXJlZD17ZmFsc2V9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJOZXh0IGJpbGxpbmcgZGF0ZTogXCIpLnRoYWkoXCLguKfguLHguJnguJfguLXguYjguYDguKPguLXguKLguIHguYDguIHguYfguJrguYDguIfguLTguJnguJbguLHguJTguYTguJtcIikubXkoXCLhgJzhgKzhgJnhgJrhgLrhgLfhgIThgL3hgLHhgJDhgLHhgKzhgIThgLrhgLjhgIHhgLbhgJ7hgIrhgLrhgLfhgJvhgIDhgLrhgIXhgL3hgLJcIikuZ2V0TWVzc2FnZSgpfSB7bmV4dEJpbGxpbmdEYXRlICYmXG4gICAgICAgICAgICAgICAgICA8bGFiZWw+e25leHRCaWxsaW5nRGF0ZX08L2xhYmVsPn0gPC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwiY2FyZC1sYXN0XCIgc3Bhbj17Nn0+XG4gICAgICAgICAgICAgIDxDYXJkIGJvcmRlcmVkPXtmYWxzZX0+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLXRpdGxlXCI+XG4gICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJLUEkgQUNISUVWRU1FTlRTXCIpLnRoYWkoXCLguK3guLHguJXguKPguLLguIHguLLguKPguYDguJXguLTguJrguYLguJXguJXguLHguKfguYHguJfguJnguYHguKXguLDguKXguLnguIHguITguYnguLJcIikubXkoXCJLUEkg4YCh4YCx4YCs4YCE4YC64YCZ4YC84YCE4YC64YCZ4YC+4YCv4YCZ4YC74YCs4YC4XCIpLmdldE1lc3NhZ2UoKX0ge25ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICA8SW5kZXhDb21tb25TdHlsZS5LUEk+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwia3BpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJTYWxlc1wiKS50aGFpKFwi4LiC4Liy4LiiXCIpLm15KFwi4YCh4YCb4YCx4YCs4YCE4YC64YC4XCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2dyZXNzIGZvcm1hdD17KHBwOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbHRlclByb2dyZXNzKHNhbGVzLCBwcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gc3Ryb2tlQ29sb3I9JyM2NWExNzInIHN0YXR1cz1cImFjdGl2ZVwiIHBlcmNlbnQ9e3NhbGVzfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiQ3VzdG9tZXIgR3Jvd3RoXCIpLnRoYWkoXCLguIHguLLguKPguYDguJXguLTguJrguYLguJXguILguK3guIfguKXguLnguIHguITguYnguLJcIikubXkoXCLhgJbhgLHhgKzhgIDhgLrhgJ7hgIrhgLrhgJDhgK3hgK/hgLjhgJDhgIDhgLrhgJnhgL7hgK/hgJThgL7hgK/hgJThgLrhgLhcIikuZ2V0TWVzc2FnZSgpfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UHJvZ3Jlc3Mgc3Ryb2tlQ29sb3I9JyM2NWExNzInIGZvcm1hdD17KHBwOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbHRlclByb2dyZXNzKGN1c3RvbWVyR3Jvd3RoLCBwcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gc3RhdHVzPVwiYWN0aXZlXCIgcGVyY2VudD17Y3VzdG9tZXJHcm93dGh9Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJMb3NzIFJhdGlvXCIpLnRoYWkoXCLguK3guLHguJXguKPguLLguIHguLLguKPguKrguLnguI3guYDguKrguLXguKJcIikubXkoXCLhgKHhgJvhgL7hgK/hgLbhgLjhgKHhgIHhgLvhgK3hgK/hgLhcIikuZ2V0TWVzc2FnZSgpfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UHJvZ3Jlc3Mgc3Ryb2tlQ29sb3I9JyNjYTdhNjgnIHN0YXR1cz1cImFjdGl2ZVwiIGZvcm1hdD17KHBwOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbHRlclByb2dyZXNzKGxvc3NSYXRpbywgcHApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19IHBlcmNlbnQ9e2xvc3NSYXRpb30vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L0luZGV4Q29tbW9uU3R5bGUuS1BJPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInJlY2VudC1jdXN0b21lcnNcIj5cbiAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwiY2FyZC0xMDFcIiBndXR0ZXI9ezE2fT5cbiAgICAgICAgICAgICAgPENvbCBzcGFuPXsxMn0+XG4gICAgICAgICAgICAgICAgPENhcmQgc3R5bGU9e3sgaGVpZ2h0OiBpc01vYmlsZSA/IFwiYXV0b1wiIDogXCIxMDAlXCIgfX0gYm9yZGVyZWQ9e2ZhbHNlfT5cbiAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIlJFQ0VOVCBUUkFOU0FDVElPTlNcIikudGhhaShcIuC4quC4luC4suC4meC4sOC4geC4o+C4oeC4mOC4o+C4o+C4oeC5jFwiKS5teShcIuGAmeGAgOGAvOGArOGAnuGAseGAuOGAmeGAruGAhOGAveGAseGAgOGAvOGAseGAuOGAnOGAveGAvuGAsuGAleGAvOGAseGArOGAhOGAuuGAuOGAmeGAvuGAr+GAmeGAvuGArFwiKS5nZXRNZXNzYWdlKCl9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLWFkZG5vdGVcIiBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goYCR7UEFUSC5ORVdfUVVPVEVfTElTVH1gKTtcbiAgICAgICAgICAgICAgICAgICAgICAvLyBNYXNrLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPE5ld1F1b3RlIG9uQ2FuY2VsPXtvbkNhbmNlbH0vPixcbiAgICAgICAgICAgICAgICAgICAgICAvLyB9KTtcbiAgICAgICAgICAgICAgICAgICAgfX0vPlxuICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwZWNlbnRcIj5cbiAgICAgICAgICAgICAgICAgICAgeyhfLmdldChyZWNlbnRUcmFuc2FjdGlvbnMsIFwicmVjZW50VHJhbnNhY3Rpb25zXCIpIHx8IFtdKS5zbGljZSgwLCAxMCkubWFwKChldmVyeTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICFpc01vYmlsZSA/IDxSb3cgZ3V0dGVyPXsxNn0ga2V5PXtpbmRleH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgc3Bhbj17M30+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2RUeXBlSW1hZ2UgcHJvZHVjdENhdGU9e2V2ZXJ5LnByb2R1Y3RDYXRlfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHN0eWxlPXt7IGNvbG9yOiBcInJnYmEoMCwwLDAsLjk1KVwiIH19IHNwYW49ezZ9PlxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2V2ZXJ5Lmluc3VyZWREZXNjcmlwdGlvbiAmJiBldmVyeS5pbnN1cmVkRGVzY3JpcHRpb24ubGVuZ3RoID4gMTAgJiYgPFBvcG92ZXJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ9e2V2ZXJ5Lmluc3VyZWREZXNjcmlwdGlvbn0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVNYXhMaW5lIG1heExpbmU9XCIyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtldmVyeS5pbnN1cmVkRGVzY3JpcHRpb259XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0eWxlTWF4TGluZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1BvcG92ZXI+fVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2V2ZXJ5Lmluc3VyZWREZXNjcmlwdGlvbiAmJiBldmVyeS5pbnN1cmVkRGVzY3JpcHRpb24ubGVuZ3RoIDw9IDEwICYmIGV2ZXJ5Lmluc3VyZWREZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0+e1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0oZXZlcnkpfTwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHNwYW49ezR9PntldmVyeS5zdGF0dXNOYW1lfTwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHNwYW49ezZ9IGNsYXNzTmFtZT1cIm9wZXJzXCI+e3RoaXMucmVuZGVyT3BlcnMoZXZlcnkpfTwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Sb3c+IDogPGRpdiBjbGFzc05hbWU9XCJtb2JpbGUtcGVyY2VudFwiIGtleT17aW5kZXh9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJpbmZvLXRpdGxlXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2RUeXBlSW1hZ2UgcHJvZHVjdENhdGU9e2V2ZXJ5LnByb2R1Y3RDYXRlfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwib3BlcnNcIj57dGhpcy5yZW5kZXJPcGVycyhldmVyeSl9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxSb3cgZ3V0dGVyPXsxNn0ga2V5PXtpbmRleH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENvbCBzdHlsZT17eyBjb2xvcjogXCJyZ2JhKDAsMCwwLC45NSlcIiB9fSBzcGFuPXsxMn0+e2V2ZXJ5Lmluc3VyZWREZXNjcmlwdGlvbn08L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHNwYW49ezd9PntVdGlscy5yZW5kZXJQb2xpY3lQcmVtaXVtKGV2ZXJ5KX08L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHNwYW49ezV9PntldmVyeS5zdGF0dXNOYW1lfTwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgPENvbCBzcGFuPXs2fT5cbiAgICAgICAgICAgICAgICA8Q2FyZCBzdHlsZT17eyBoZWlnaHQ6IGlzTW9iaWxlID8gXCJhdXRvXCIgOiBcIjEwMCVcIiB9fSBib3JkZXJlZD17ZmFsc2V9PlxuICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiTkVXIENVU1RPTUVSU1wiKS50aGFpKFwi4Lij4Liy4Lii4Lil4Liw4LmA4Lit4Li14Lii4LiU4Lil4Li54LiB4LiE4LmJ4LiyXCIpLm15KFwi4YCh4YCe4YCF4YC64YCA4YCW4YCx4YCs4YCA4YC64YCe4YCK4YC64YCZ4YC74YCs4YC4XCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tYWRkLXBlcnNvblwiIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5wdXNoUmVkaXJlY3QoUEFUSC5DVVNUT01FUlNfRU5UUlkucmVwbGFjZShcIjpjdXN0b21lcklkXCIsIFwiXCIpKTtcbiAgICAgICAgICAgICAgICAgICAgfX0vPlxuICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjdXN0b21lcnMtY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICB7KF8uZ2V0KG5ld0N1c3RvbWVycywgXCJuZXdDdXN0b21lcnNcIikgfHwgW10pLnNsaWNlKDAsIDUpLm1hcCgoZXZlcnk6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9cIiBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbmZvLXRpdGxlXCIgc3R5bGU9e3sgZGlzcGxheTogXCJmbGV4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2V2ZXJ5Lm5hbWV9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib3BlcmF0ZS1pXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLXZpZXdcIiBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFN0b3JhZ2UuY3VycmVudFJvdXRlcnMuc2Vzc2lvbigpLnNldChDb25zdHMuQ1VTVE9NRVJfUk9VVEVSLCBcImFnZW50XCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKS5wdXNoUmVkaXJlY3QoUEFUSC5DVVNUT01FUlNfVklFVy5yZXBsYWNlKFwiOmN1c3RvbWVySWRcIiwgZXZlcnkuY3VzdElkKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RGl2aWRlciB0eXBlPVwidmVydGljYWxcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLXdvcmstb25cIiBzdHlsZT17eyBmb250U2l6ZTogXCIxNnB4XCIgfX0gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBTdG9yYWdlLmN1cnJlbnRSb3V0ZXJzLnNlc3Npb24oKS5zZXQoQ29uc3RzLkNVU1RPTUVSX1JPVVRFUiwgXCJhZ2VudFwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRIZWxwZXJzKCkuZ2V0Um91dGVyKCkucHVzaFJlZGlyZWN0KFBBVEguQ1VTVE9NRVJTX0VOVFJZLnJlcGxhY2UoXCI6Y3VzdG9tZXJJZFwiLCBldmVyeS5jdXN0SWQpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEaXZpZGVyIHR5cGU9XCJ2ZXJ0aWNhbFwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tYWRkbm90ZVwiIHN0eWxlPXt7IGZvbnRTaXplOiBcIjE2cHhcIiB9fSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKGAke1BBVEguTkVXX1FVT1RFX0xJU1R9P2N1c3RJZD0ke2V2ZXJ5LmN1c3RJZH1gKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gTWFzay5jcmVhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIENvbXBvbmVudDogKHsgb25DYW5jZWwsIG9uT2sgfTogYW55KSA9PiA8TmV3UXVvdGUgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VzdG9tZXI9e2V2ZXJ5fS8+LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaXBob25lLWVtYWlsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlcnkubW9iaWxlTm8uc3BsaXQoXCIgXCIpWzFdICE9PSBcIm51bGxcIiAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24taXBob25lXCIvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXZlcnkubW9iaWxlTm8uc3BsaXQoXCIgXCIpWzBdID09PSBcIm51bGxcIiA/IGV2ZXJ5Lm1vYmlsZU5vLnNwbGl0KFwiIFwiKVsxXSA6IGV2ZXJ5Lm1vYmlsZU5vfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVyeS5lbWFpbCAhPT0gbnVsbCAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbmZvbnQgaWNvbi1lbWFpbFwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2V2ZXJ5LmVtYWlsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0+XG4gICAgICAgICAgICAgICAgPENhcmQgc3R5bGU9e3sgaGVpZ2h0OiBpc01vYmlsZSA/IFwiYXV0b1wiIDogXCIxMDAlXCIgfX0gYm9yZGVyZWQ9e2ZhbHNlfT5cbiAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIkNPTUlORyBCSVJUSERBWVNcIikudGhhaShcIuC4p+C4seC4meC5gOC4geC4tOC4lFwiKS5teShcIuGAnOGArOGAmeGAiuGAuuGAt+GAmeGAveGAseGAuOGAlOGAseGAt1wiKS5nZXRNZXNzYWdlKCl9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjdXN0b21lcnMtY29udGVudFwiPlxuICAgICAgICAgICAgICAgICAgICB7KF8uZ2V0KGNvbWluZ0JpcnRoZGF5cywgXCJjb21pbmdCaXJ0aGRheXNcIikgfHwgW10pLnNsaWNlKDAsIDUpLm1hcCgoZXZlcnk6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9cIiBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiaW5mby10aXRsZVwiIHN0eWxlPXt7IGRpc3BsYXk6IFwiZmxleFwiIH19PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntldmVyeS5uYW1lfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogXCI1cHhcIiB9fT57Xy5nZXQoZXZlcnksIFwiYWdlXCIsIFwiXCIpfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tYmlydGhkYXlcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXZlcnkuZG9ifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImlwaG9uZS1lbWFpbFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiaWNvbmZvbnQgaWNvbi1pcGhvbmVcIi8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXZlcnkubW9iaWxlTm99XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLWVtYWlsXCIvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2V2ZXJ5LmVtYWlsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L0MuTWFuYWdlckluZGV4PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFJvdXRlcihBZ2VudEluZGV4KTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBNEJBO0FBQ0E7QUFDQTs7Ozs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBNFdBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUE5WEE7QUFnWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUF6WUE7QUFFQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQURBO0FBQ0E7QUFEQTtBQUVBO0FBQUE7QUFGQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBMURBO0FBNERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQWRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFJQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFJQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQWlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBVUE7Ozs7QUEvb0JBO0FBQ0E7QUFpcEJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/home/agent-index.tsx
