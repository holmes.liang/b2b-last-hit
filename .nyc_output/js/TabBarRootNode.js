__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-tabs/es/utils.js");












var TabBarRootNode = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default()(TabBarRootNode, _React$Component);

  function TabBarRootNode() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, TabBarRootNode);

    return babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default()(this, (TabBarRootNode.__proto__ || Object.getPrototypeOf(TabBarRootNode)).apply(this, arguments));
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(TabBarRootNode, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          prefixCls = _props.prefixCls,
          onKeyDown = _props.onKeyDown,
          className = _props.className,
          extraContent = _props.extraContent,
          style = _props.style,
          tabBarPosition = _props.tabBarPosition,
          children = _props.children,
          restProps = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default()(_props, ['prefixCls', 'onKeyDown', 'className', 'extraContent', 'style', 'tabBarPosition', 'children']);

      var cls = classnames__WEBPACK_IMPORTED_MODULE_9___default()(prefixCls + '-bar', babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()({}, className, !!className));
      var topOrBottom = tabBarPosition === 'top' || tabBarPosition === 'bottom';
      var tabBarExtraContentStyle = topOrBottom ? {
        float: 'right'
      } : {};
      var extraContentStyle = extraContent && extraContent.props ? extraContent.props.style : {};
      var newChildren = children;

      if (extraContent) {
        newChildren = [Object(react__WEBPACK_IMPORTED_MODULE_7__["cloneElement"])(extraContent, {
          key: 'extra',
          style: babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, tabBarExtraContentStyle, extraContentStyle)
        }), Object(react__WEBPACK_IMPORTED_MODULE_7__["cloneElement"])(children, {
          key: 'content'
        })];
        newChildren = topOrBottom ? newChildren : newChildren.reverse();
      }

      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement('div', babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
        role: 'tablist',
        className: cls,
        tabIndex: '0',
        ref: this.props.saveRef('root'),
        onKeyDown: onKeyDown,
        style: style
      }, Object(_utils__WEBPACK_IMPORTED_MODULE_10__["getDataAttr"])(restProps)), newChildren);
    }
  }]);

  return TabBarRootNode;
}(react__WEBPACK_IMPORTED_MODULE_7___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (TabBarRootNode);
TabBarRootNode.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.string,
  style: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.object,
  tabBarPosition: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.oneOf(['left', 'right', 'top', 'bottom']),
  children: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.node,
  extraContent: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.node,
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.func,
  saveRef: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.func
};
TabBarRootNode.defaultProps = {
  prefixCls: '',
  className: '',
  style: {},
  tabBarPosition: 'top',
  extraContent: null,
  children: null,
  onKeyDown: function onKeyDown() {},
  saveRef: function saveRef() {}
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFicy9lcy9UYWJCYXJSb290Tm9kZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRhYnMvZXMvVGFiQmFyUm9vdE5vZGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5JztcbmltcG9ydCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfY3JlYXRlQ2xhc3MgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgUmVhY3QsIHsgY2xvbmVFbGVtZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgZ2V0RGF0YUF0dHIgfSBmcm9tICcuL3V0aWxzJztcblxudmFyIFRhYkJhclJvb3ROb2RlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFRhYkJhclJvb3ROb2RlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUYWJCYXJSb290Tm9kZSgpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgVGFiQmFyUm9vdE5vZGUpO1xuXG4gICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChUYWJCYXJSb290Tm9kZS5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFRhYkJhclJvb3ROb2RlKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoVGFiQmFyUm9vdE5vZGUsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzLnByZWZpeENscyxcbiAgICAgICAgICBvbktleURvd24gPSBfcHJvcHMub25LZXlEb3duLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZXh0cmFDb250ZW50ID0gX3Byb3BzLmV4dHJhQ29udGVudCxcbiAgICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0YWJCYXJQb3NpdGlvbiA9IF9wcm9wcy50YWJCYXJQb3NpdGlvbixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICByZXN0UHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3ByZWZpeENscycsICdvbktleURvd24nLCAnY2xhc3NOYW1lJywgJ2V4dHJhQ29udGVudCcsICdzdHlsZScsICd0YWJCYXJQb3NpdGlvbicsICdjaGlsZHJlbiddKTtcblxuICAgICAgdmFyIGNscyA9IGNsYXNzbmFtZXMocHJlZml4Q2xzICsgJy1iYXInLCBfZGVmaW5lUHJvcGVydHkoe30sIGNsYXNzTmFtZSwgISFjbGFzc05hbWUpKTtcbiAgICAgIHZhciB0b3BPckJvdHRvbSA9IHRhYkJhclBvc2l0aW9uID09PSAndG9wJyB8fCB0YWJCYXJQb3NpdGlvbiA9PT0gJ2JvdHRvbSc7XG4gICAgICB2YXIgdGFiQmFyRXh0cmFDb250ZW50U3R5bGUgPSB0b3BPckJvdHRvbSA/IHsgZmxvYXQ6ICdyaWdodCcgfSA6IHt9O1xuICAgICAgdmFyIGV4dHJhQ29udGVudFN0eWxlID0gZXh0cmFDb250ZW50ICYmIGV4dHJhQ29udGVudC5wcm9wcyA/IGV4dHJhQ29udGVudC5wcm9wcy5zdHlsZSA6IHt9O1xuICAgICAgdmFyIG5ld0NoaWxkcmVuID0gY2hpbGRyZW47XG4gICAgICBpZiAoZXh0cmFDb250ZW50KSB7XG4gICAgICAgIG5ld0NoaWxkcmVuID0gW2Nsb25lRWxlbWVudChleHRyYUNvbnRlbnQsIHtcbiAgICAgICAgICBrZXk6ICdleHRyYScsXG4gICAgICAgICAgc3R5bGU6IF9leHRlbmRzKHt9LCB0YWJCYXJFeHRyYUNvbnRlbnRTdHlsZSwgZXh0cmFDb250ZW50U3R5bGUpXG4gICAgICAgIH0pLCBjbG9uZUVsZW1lbnQoY2hpbGRyZW4sIHsga2V5OiAnY29udGVudCcgfSldO1xuICAgICAgICBuZXdDaGlsZHJlbiA9IHRvcE9yQm90dG9tID8gbmV3Q2hpbGRyZW4gOiBuZXdDaGlsZHJlbi5yZXZlcnNlKCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICByb2xlOiAndGFibGlzdCcsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbHMsXG4gICAgICAgICAgdGFiSW5kZXg6ICcwJyxcbiAgICAgICAgICByZWY6IHRoaXMucHJvcHMuc2F2ZVJlZigncm9vdCcpLFxuICAgICAgICAgIG9uS2V5RG93bjogb25LZXlEb3duLFxuICAgICAgICAgIHN0eWxlOiBzdHlsZVxuICAgICAgICB9LCBnZXREYXRhQXR0cihyZXN0UHJvcHMpKSxcbiAgICAgICAgbmV3Q2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFRhYkJhclJvb3ROb2RlO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5leHBvcnQgZGVmYXVsdCBUYWJCYXJSb290Tm9kZTtcblxuXG5UYWJCYXJSb290Tm9kZS5wcm9wVHlwZXMgPSB7XG4gIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgdGFiQmFyUG9zaXRpb246IFByb3BUeXBlcy5vbmVPZihbJ2xlZnQnLCAncmlnaHQnLCAndG9wJywgJ2JvdHRvbSddKSxcbiAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICBleHRyYUNvbnRlbnQ6IFByb3BUeXBlcy5ub2RlLFxuICBvbktleURvd246IFByb3BUeXBlcy5mdW5jLFxuICBzYXZlUmVmOiBQcm9wVHlwZXMuZnVuY1xufTtcblxuVGFiQmFyUm9vdE5vZGUuZGVmYXVsdFByb3BzID0ge1xuICBwcmVmaXhDbHM6ICcnLFxuICBjbGFzc05hbWU6ICcnLFxuICBzdHlsZToge30sXG4gIHRhYkJhclBvc2l0aW9uOiAndG9wJyxcbiAgZXh0cmFDb250ZW50OiBudWxsLFxuICBjaGlsZHJlbjogbnVsbCxcbiAgb25LZXlEb3duOiBmdW5jdGlvbiBvbktleURvd24oKSB7fSxcbiAgc2F2ZVJlZjogZnVuY3Rpb24gc2F2ZVJlZigpIHt9XG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFVQTtBQXJDQTtBQUNBO0FBdUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tabs/es/TabBarRootNode.js
