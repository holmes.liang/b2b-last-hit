__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var rc_align__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rc-align */ "./node_modules/rc-align/es/index.js");
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _PopupInner__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./PopupInner */ "./node_modules/rc-tree-select/node_modules/rc-trigger/es/PopupInner.js");
/* harmony import */ var _LazyRenderBox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./LazyRenderBox */ "./node_modules/rc-tree-select/node_modules/rc-trigger/es/LazyRenderBox.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-tree-select/node_modules/rc-trigger/es/utils.js");














var Popup = function (_Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Popup, _Component);

  function Popup(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Popup);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _Component.call(this, props));

    _initialiseProps.call(_this);

    _this.state = {
      // Used for stretch
      stretchChecked: false,
      targetWidth: undefined,
      targetHeight: undefined
    };
    _this.savePopupRef = _utils__WEBPACK_IMPORTED_MODULE_12__["saveRef"].bind(_this, 'popupInstance');
    _this.saveAlignRef = _utils__WEBPACK_IMPORTED_MODULE_12__["saveRef"].bind(_this, 'alignInstance');
    return _this;
  }

  Popup.prototype.componentDidMount = function componentDidMount() {
    this.rootNode = this.getPopupDomNode();
    this.setStretchSize();
  };

  Popup.prototype.componentDidUpdate = function componentDidUpdate() {
    this.setStretchSize();
  }; // Record size if stretch needed


  Popup.prototype.getPopupDomNode = function getPopupDomNode() {
    return react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(this.popupInstance);
  }; // `target` on `rc-align` can accept as a function to get the bind element or a point.
  // ref: https://www.npmjs.com/package/rc-align


  Popup.prototype.getMaskTransitionName = function getMaskTransitionName() {
    var props = this.props;
    var transitionName = props.maskTransitionName;
    var animation = props.maskAnimation;

    if (!transitionName && animation) {
      transitionName = props.prefixCls + '-' + animation;
    }

    return transitionName;
  };

  Popup.prototype.getTransitionName = function getTransitionName() {
    var props = this.props;
    var transitionName = props.transitionName;

    if (!transitionName && props.animation) {
      transitionName = props.prefixCls + '-' + props.animation;
    }

    return transitionName;
  };

  Popup.prototype.getClassName = function getClassName(currentAlignClassName) {
    return this.props.prefixCls + ' ' + this.props.className + ' ' + currentAlignClassName;
  };

  Popup.prototype.getPopupElement = function getPopupElement() {
    var _this2 = this;

    var savePopupRef = this.savePopupRef;
    var _state = this.state,
        stretchChecked = _state.stretchChecked,
        targetHeight = _state.targetHeight,
        targetWidth = _state.targetWidth;
    var _props = this.props,
        align = _props.align,
        visible = _props.visible,
        prefixCls = _props.prefixCls,
        style = _props.style,
        getClassNameFromAlign = _props.getClassNameFromAlign,
        destroyPopupOnHide = _props.destroyPopupOnHide,
        stretch = _props.stretch,
        children = _props.children,
        onMouseEnter = _props.onMouseEnter,
        onMouseLeave = _props.onMouseLeave;
    var className = this.getClassName(this.currentAlignClassName || getClassNameFromAlign(align));
    var hiddenClassName = prefixCls + '-hidden';

    if (!visible) {
      this.currentAlignClassName = null;
    }

    var sizeStyle = {};

    if (stretch) {
      // Stretch with target
      if (stretch.indexOf('height') !== -1) {
        sizeStyle.height = targetHeight;
      } else if (stretch.indexOf('minHeight') !== -1) {
        sizeStyle.minHeight = targetHeight;
      }

      if (stretch.indexOf('width') !== -1) {
        sizeStyle.width = targetWidth;
      } else if (stretch.indexOf('minWidth') !== -1) {
        sizeStyle.minWidth = targetWidth;
      } // Delay force align to makes ui smooth


      if (!stretchChecked) {
        sizeStyle.visibility = 'hidden';
        raf__WEBPACK_IMPORTED_MODULE_9___default()(function () {
          if (_this2.alignInstance) {
            _this2.alignInstance.forceAlign();
          }
        });
      }
    }

    var newStyle = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, sizeStyle, style, this.getZIndexStyle());

    var popupInnerProps = {
      className: className,
      prefixCls: prefixCls,
      ref: savePopupRef,
      onMouseEnter: onMouseEnter,
      onMouseLeave: onMouseLeave,
      style: newStyle
    };

    if (destroyPopupOnHide) {
      return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
        component: '',
        exclusive: true,
        transitionAppear: true,
        transitionName: this.getTransitionName(),
        onLeave: this.onAnimateLeaved
      }, visible ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_align__WEBPACK_IMPORTED_MODULE_7__["default"], {
        target: this.getAlignTarget(),
        key: 'popup',
        ref: this.saveAlignRef,
        monitorWindowResize: true,
        align: align,
        onAlign: this.onAlign
      }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_PopupInner__WEBPACK_IMPORTED_MODULE_10__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
        visible: true
      }, popupInnerProps), children)) : null);
    }

    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
      component: '',
      exclusive: true,
      transitionAppear: true,
      transitionName: this.getTransitionName(),
      showProp: 'xVisible',
      onLeave: this.onAnimateLeaved
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_align__WEBPACK_IMPORTED_MODULE_7__["default"], {
      target: this.getAlignTarget(),
      key: 'popup',
      ref: this.saveAlignRef,
      monitorWindowResize: true,
      xVisible: visible,
      childrenProps: {
        visible: 'xVisible'
      },
      disabled: !visible,
      align: align,
      onAlign: this.onAlign
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_PopupInner__WEBPACK_IMPORTED_MODULE_10__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      hiddenClassName: hiddenClassName
    }, popupInnerProps), children)));
  };

  Popup.prototype.getZIndexStyle = function getZIndexStyle() {
    var style = {};
    var props = this.props;

    if (props.zIndex !== undefined) {
      style.zIndex = props.zIndex;
    }

    return style;
  };

  Popup.prototype.getMaskElement = function getMaskElement() {
    var props = this.props;
    var maskElement = void 0;

    if (props.mask) {
      var maskTransition = this.getMaskTransitionName();
      maskElement = react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_LazyRenderBox__WEBPACK_IMPORTED_MODULE_11__["default"], {
        style: this.getZIndexStyle(),
        key: 'mask',
        className: props.prefixCls + '-mask',
        hiddenClassName: props.prefixCls + '-mask-hidden',
        visible: props.visible
      });

      if (maskTransition) {
        maskElement = react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
          key: 'mask',
          showProp: 'visible',
          transitionAppear: true,
          component: '',
          transitionName: maskTransition
        }, maskElement);
      }
    }

    return maskElement;
  };

  Popup.prototype.render = function render() {
    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', null, this.getMaskElement(), this.getPopupElement());
  };

  return Popup;
}(react__WEBPACK_IMPORTED_MODULE_4__["Component"]);

Popup.propTypes = {
  visible: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  style: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  getClassNameFromAlign: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onAlign: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  getRootDomNode: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onMouseEnter: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  align: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  destroyPopupOnHide: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  className: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  onMouseLeave: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  stretch: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  children: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.node,
  point: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.shape({
    pageX: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
    pageY: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number
  })
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.onAlign = function (popupDomNode, align) {
    var props = _this3.props;
    var currentAlignClassName = props.getClassNameFromAlign(align); // FIX: https://github.com/react-component/trigger/issues/56
    // FIX: https://github.com/react-component/tooltip/issues/79

    if (_this3.currentAlignClassName !== currentAlignClassName) {
      _this3.currentAlignClassName = currentAlignClassName;
      popupDomNode.className = _this3.getClassName(currentAlignClassName);
    }

    props.onAlign(popupDomNode, align);
  };

  this.onAnimateLeaved = function () {
    var stretch = _this3.props.stretch;
    var stretchChecked = _this3.state.stretchChecked;

    if (stretch && stretchChecked) {
      _this3.setState({
        stretchChecked: false
      });
    }
  };

  this.setStretchSize = function () {
    var getRootDomNode = _this3.props.getRootDomNode;
    var _state2 = _this3.state,
        stretchChecked = _state2.stretchChecked,
        targetHeight = _state2.targetHeight,
        targetWidth = _state2.targetWidth;
    var $ele = getRootDomNode();
    if (!$ele) return;
    var height = $ele.offsetHeight;
    var width = $ele.offsetWidth;

    if (targetHeight !== height || targetWidth !== width || !stretchChecked) {
      _this3.setState({
        stretchChecked: true,
        targetHeight: height,
        targetWidth: width
      });
    }
  };

  this.getTargetElement = function () {
    return _this3.props.getRootDomNode();
  };

  this.getAlignTarget = function () {
    var point = _this3.props.point;

    if (point) {
      return point;
    }

    return _this3.getTargetElement;
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Popup);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvZXMvUG9wdXAuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10cmVlLXNlbGVjdC9ub2RlX21vZHVsZXMvcmMtdHJpZ2dlci9lcy9Qb3B1cC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgQWxpZ24gZnJvbSAncmMtYWxpZ24nO1xuaW1wb3J0IEFuaW1hdGUgZnJvbSAncmMtYW5pbWF0ZSc7XG5pbXBvcnQgcmFmIGZyb20gJ3JhZic7XG5pbXBvcnQgUG9wdXBJbm5lciBmcm9tICcuL1BvcHVwSW5uZXInO1xuaW1wb3J0IExhenlSZW5kZXJCb3ggZnJvbSAnLi9MYXp5UmVuZGVyQm94JztcbmltcG9ydCB7IHNhdmVSZWYgfSBmcm9tICcuL3V0aWxzJztcblxudmFyIFBvcHVwID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFBvcHVwLCBfQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBQb3B1cChwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBQb3B1cCk7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgIF9pbml0aWFsaXNlUHJvcHMuY2FsbChfdGhpcyk7XG5cbiAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIC8vIFVzZWQgZm9yIHN0cmV0Y2hcbiAgICAgIHN0cmV0Y2hDaGVja2VkOiBmYWxzZSxcbiAgICAgIHRhcmdldFdpZHRoOiB1bmRlZmluZWQsXG4gICAgICB0YXJnZXRIZWlnaHQ6IHVuZGVmaW5lZFxuICAgIH07XG5cbiAgICBfdGhpcy5zYXZlUG9wdXBSZWYgPSBzYXZlUmVmLmJpbmQoX3RoaXMsICdwb3B1cEluc3RhbmNlJyk7XG4gICAgX3RoaXMuc2F2ZUFsaWduUmVmID0gc2F2ZVJlZi5iaW5kKF90aGlzLCAnYWxpZ25JbnN0YW5jZScpO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIFBvcHVwLnByb3RvdHlwZS5jb21wb25lbnREaWRNb3VudCA9IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMucm9vdE5vZGUgPSB0aGlzLmdldFBvcHVwRG9tTm9kZSgpO1xuICAgIHRoaXMuc2V0U3RyZXRjaFNpemUoKTtcbiAgfTtcblxuICBQb3B1cC5wcm90b3R5cGUuY29tcG9uZW50RGlkVXBkYXRlID0gZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgIHRoaXMuc2V0U3RyZXRjaFNpemUoKTtcbiAgfTtcblxuICAvLyBSZWNvcmQgc2l6ZSBpZiBzdHJldGNoIG5lZWRlZFxuXG5cbiAgUG9wdXAucHJvdG90eXBlLmdldFBvcHVwRG9tTm9kZSA9IGZ1bmN0aW9uIGdldFBvcHVwRG9tTm9kZSgpIHtcbiAgICByZXR1cm4gUmVhY3RET00uZmluZERPTU5vZGUodGhpcy5wb3B1cEluc3RhbmNlKTtcbiAgfTtcblxuICAvLyBgdGFyZ2V0YCBvbiBgcmMtYWxpZ25gIGNhbiBhY2NlcHQgYXMgYSBmdW5jdGlvbiB0byBnZXQgdGhlIGJpbmQgZWxlbWVudCBvciBhIHBvaW50LlxuICAvLyByZWY6IGh0dHBzOi8vd3d3Lm5wbWpzLmNvbS9wYWNrYWdlL3JjLWFsaWduXG5cblxuICBQb3B1cC5wcm90b3R5cGUuZ2V0TWFza1RyYW5zaXRpb25OYW1lID0gZnVuY3Rpb24gZ2V0TWFza1RyYW5zaXRpb25OYW1lKCkge1xuICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgdmFyIHRyYW5zaXRpb25OYW1lID0gcHJvcHMubWFza1RyYW5zaXRpb25OYW1lO1xuICAgIHZhciBhbmltYXRpb24gPSBwcm9wcy5tYXNrQW5pbWF0aW9uO1xuICAgIGlmICghdHJhbnNpdGlvbk5hbWUgJiYgYW5pbWF0aW9uKSB7XG4gICAgICB0cmFuc2l0aW9uTmFtZSA9IHByb3BzLnByZWZpeENscyArICctJyArIGFuaW1hdGlvbjtcbiAgICB9XG4gICAgcmV0dXJuIHRyYW5zaXRpb25OYW1lO1xuICB9O1xuXG4gIFBvcHVwLnByb3RvdHlwZS5nZXRUcmFuc2l0aW9uTmFtZSA9IGZ1bmN0aW9uIGdldFRyYW5zaXRpb25OYW1lKCkge1xuICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgdmFyIHRyYW5zaXRpb25OYW1lID0gcHJvcHMudHJhbnNpdGlvbk5hbWU7XG4gICAgaWYgKCF0cmFuc2l0aW9uTmFtZSAmJiBwcm9wcy5hbmltYXRpb24pIHtcbiAgICAgIHRyYW5zaXRpb25OYW1lID0gcHJvcHMucHJlZml4Q2xzICsgJy0nICsgcHJvcHMuYW5pbWF0aW9uO1xuICAgIH1cbiAgICByZXR1cm4gdHJhbnNpdGlvbk5hbWU7XG4gIH07XG5cbiAgUG9wdXAucHJvdG90eXBlLmdldENsYXNzTmFtZSA9IGZ1bmN0aW9uIGdldENsYXNzTmFtZShjdXJyZW50QWxpZ25DbGFzc05hbWUpIHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy5wcmVmaXhDbHMgKyAnICcgKyB0aGlzLnByb3BzLmNsYXNzTmFtZSArICcgJyArIGN1cnJlbnRBbGlnbkNsYXNzTmFtZTtcbiAgfTtcblxuICBQb3B1cC5wcm90b3R5cGUuZ2V0UG9wdXBFbGVtZW50ID0gZnVuY3Rpb24gZ2V0UG9wdXBFbGVtZW50KCkge1xuICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgdmFyIHNhdmVQb3B1cFJlZiA9IHRoaXMuc2F2ZVBvcHVwUmVmO1xuICAgIHZhciBfc3RhdGUgPSB0aGlzLnN0YXRlLFxuICAgICAgICBzdHJldGNoQ2hlY2tlZCA9IF9zdGF0ZS5zdHJldGNoQ2hlY2tlZCxcbiAgICAgICAgdGFyZ2V0SGVpZ2h0ID0gX3N0YXRlLnRhcmdldEhlaWdodCxcbiAgICAgICAgdGFyZ2V0V2lkdGggPSBfc3RhdGUudGFyZ2V0V2lkdGg7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIGFsaWduID0gX3Byb3BzLmFsaWduLFxuICAgICAgICB2aXNpYmxlID0gX3Byb3BzLnZpc2libGUsXG4gICAgICAgIHByZWZpeENscyA9IF9wcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgIHN0eWxlID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICBnZXRDbGFzc05hbWVGcm9tQWxpZ24gPSBfcHJvcHMuZ2V0Q2xhc3NOYW1lRnJvbUFsaWduLFxuICAgICAgICBkZXN0cm95UG9wdXBPbkhpZGUgPSBfcHJvcHMuZGVzdHJveVBvcHVwT25IaWRlLFxuICAgICAgICBzdHJldGNoID0gX3Byb3BzLnN0cmV0Y2gsXG4gICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICBvbk1vdXNlRW50ZXIgPSBfcHJvcHMub25Nb3VzZUVudGVyLFxuICAgICAgICBvbk1vdXNlTGVhdmUgPSBfcHJvcHMub25Nb3VzZUxlYXZlO1xuXG4gICAgdmFyIGNsYXNzTmFtZSA9IHRoaXMuZ2V0Q2xhc3NOYW1lKHRoaXMuY3VycmVudEFsaWduQ2xhc3NOYW1lIHx8IGdldENsYXNzTmFtZUZyb21BbGlnbihhbGlnbikpO1xuICAgIHZhciBoaWRkZW5DbGFzc05hbWUgPSBwcmVmaXhDbHMgKyAnLWhpZGRlbic7XG5cbiAgICBpZiAoIXZpc2libGUpIHtcbiAgICAgIHRoaXMuY3VycmVudEFsaWduQ2xhc3NOYW1lID0gbnVsbDtcbiAgICB9XG5cbiAgICB2YXIgc2l6ZVN0eWxlID0ge307XG4gICAgaWYgKHN0cmV0Y2gpIHtcbiAgICAgIC8vIFN0cmV0Y2ggd2l0aCB0YXJnZXRcbiAgICAgIGlmIChzdHJldGNoLmluZGV4T2YoJ2hlaWdodCcpICE9PSAtMSkge1xuICAgICAgICBzaXplU3R5bGUuaGVpZ2h0ID0gdGFyZ2V0SGVpZ2h0O1xuICAgICAgfSBlbHNlIGlmIChzdHJldGNoLmluZGV4T2YoJ21pbkhlaWdodCcpICE9PSAtMSkge1xuICAgICAgICBzaXplU3R5bGUubWluSGVpZ2h0ID0gdGFyZ2V0SGVpZ2h0O1xuICAgICAgfVxuICAgICAgaWYgKHN0cmV0Y2guaW5kZXhPZignd2lkdGgnKSAhPT0gLTEpIHtcbiAgICAgICAgc2l6ZVN0eWxlLndpZHRoID0gdGFyZ2V0V2lkdGg7XG4gICAgICB9IGVsc2UgaWYgKHN0cmV0Y2guaW5kZXhPZignbWluV2lkdGgnKSAhPT0gLTEpIHtcbiAgICAgICAgc2l6ZVN0eWxlLm1pbldpZHRoID0gdGFyZ2V0V2lkdGg7XG4gICAgICB9XG5cbiAgICAgIC8vIERlbGF5IGZvcmNlIGFsaWduIHRvIG1ha2VzIHVpIHNtb290aFxuICAgICAgaWYgKCFzdHJldGNoQ2hlY2tlZCkge1xuICAgICAgICBzaXplU3R5bGUudmlzaWJpbGl0eSA9ICdoaWRkZW4nO1xuICAgICAgICByYWYoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmIChfdGhpczIuYWxpZ25JbnN0YW5jZSkge1xuICAgICAgICAgICAgX3RoaXMyLmFsaWduSW5zdGFuY2UuZm9yY2VBbGlnbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIG5ld1N0eWxlID0gX2V4dGVuZHMoe30sIHNpemVTdHlsZSwgc3R5bGUsIHRoaXMuZ2V0WkluZGV4U3R5bGUoKSk7XG5cbiAgICB2YXIgcG9wdXBJbm5lclByb3BzID0ge1xuICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgIHJlZjogc2F2ZVBvcHVwUmVmLFxuICAgICAgb25Nb3VzZUVudGVyOiBvbk1vdXNlRW50ZXIsXG4gICAgICBvbk1vdXNlTGVhdmU6IG9uTW91c2VMZWF2ZSxcbiAgICAgIHN0eWxlOiBuZXdTdHlsZVxuICAgIH07XG5cbiAgICBpZiAoZGVzdHJveVBvcHVwT25IaWRlKSB7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQW5pbWF0ZSxcbiAgICAgICAge1xuICAgICAgICAgIGNvbXBvbmVudDogJycsXG4gICAgICAgICAgZXhjbHVzaXZlOiB0cnVlLFxuICAgICAgICAgIHRyYW5zaXRpb25BcHBlYXI6IHRydWUsXG4gICAgICAgICAgdHJhbnNpdGlvbk5hbWU6IHRoaXMuZ2V0VHJhbnNpdGlvbk5hbWUoKSxcbiAgICAgICAgICBvbkxlYXZlOiB0aGlzLm9uQW5pbWF0ZUxlYXZlZFxuICAgICAgICB9LFxuICAgICAgICB2aXNpYmxlID8gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBBbGlnbixcbiAgICAgICAgICB7XG4gICAgICAgICAgICB0YXJnZXQ6IHRoaXMuZ2V0QWxpZ25UYXJnZXQoKSxcbiAgICAgICAgICAgIGtleTogJ3BvcHVwJyxcbiAgICAgICAgICAgIHJlZjogdGhpcy5zYXZlQWxpZ25SZWYsXG4gICAgICAgICAgICBtb25pdG9yV2luZG93UmVzaXplOiB0cnVlLFxuICAgICAgICAgICAgYWxpZ246IGFsaWduLFxuICAgICAgICAgICAgb25BbGlnbjogdGhpcy5vbkFsaWduXG4gICAgICAgICAgfSxcbiAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgUG9wdXBJbm5lcixcbiAgICAgICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZVxuICAgICAgICAgICAgfSwgcG9wdXBJbm5lclByb3BzKSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKVxuICAgICAgICApIDogbnVsbFxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgIEFuaW1hdGUsXG4gICAgICB7XG4gICAgICAgIGNvbXBvbmVudDogJycsXG4gICAgICAgIGV4Y2x1c2l2ZTogdHJ1ZSxcbiAgICAgICAgdHJhbnNpdGlvbkFwcGVhcjogdHJ1ZSxcbiAgICAgICAgdHJhbnNpdGlvbk5hbWU6IHRoaXMuZ2V0VHJhbnNpdGlvbk5hbWUoKSxcbiAgICAgICAgc2hvd1Byb3A6ICd4VmlzaWJsZScsXG4gICAgICAgIG9uTGVhdmU6IHRoaXMub25BbmltYXRlTGVhdmVkXG4gICAgICB9LFxuICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQWxpZ24sXG4gICAgICAgIHtcbiAgICAgICAgICB0YXJnZXQ6IHRoaXMuZ2V0QWxpZ25UYXJnZXQoKSxcbiAgICAgICAgICBrZXk6ICdwb3B1cCcsXG4gICAgICAgICAgcmVmOiB0aGlzLnNhdmVBbGlnblJlZixcbiAgICAgICAgICBtb25pdG9yV2luZG93UmVzaXplOiB0cnVlLFxuICAgICAgICAgIHhWaXNpYmxlOiB2aXNpYmxlLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcHM6IHsgdmlzaWJsZTogJ3hWaXNpYmxlJyB9LFxuICAgICAgICAgIGRpc2FibGVkOiAhdmlzaWJsZSxcbiAgICAgICAgICBhbGlnbjogYWxpZ24sXG4gICAgICAgICAgb25BbGlnbjogdGhpcy5vbkFsaWduXG4gICAgICAgIH0sXG4gICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgUG9wdXBJbm5lcixcbiAgICAgICAgICBfZXh0ZW5kcyh7XG4gICAgICAgICAgICBoaWRkZW5DbGFzc05hbWU6IGhpZGRlbkNsYXNzTmFtZVxuICAgICAgICAgIH0sIHBvcHVwSW5uZXJQcm9wcyksXG4gICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgKVxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgUG9wdXAucHJvdG90eXBlLmdldFpJbmRleFN0eWxlID0gZnVuY3Rpb24gZ2V0WkluZGV4U3R5bGUoKSB7XG4gICAgdmFyIHN0eWxlID0ge307XG4gICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICBpZiAocHJvcHMuekluZGV4ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHN0eWxlLnpJbmRleCA9IHByb3BzLnpJbmRleDtcbiAgICB9XG4gICAgcmV0dXJuIHN0eWxlO1xuICB9O1xuXG4gIFBvcHVwLnByb3RvdHlwZS5nZXRNYXNrRWxlbWVudCA9IGZ1bmN0aW9uIGdldE1hc2tFbGVtZW50KCkge1xuICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgdmFyIG1hc2tFbGVtZW50ID0gdm9pZCAwO1xuICAgIGlmIChwcm9wcy5tYXNrKSB7XG4gICAgICB2YXIgbWFza1RyYW5zaXRpb24gPSB0aGlzLmdldE1hc2tUcmFuc2l0aW9uTmFtZSgpO1xuICAgICAgbWFza0VsZW1lbnQgPSBSZWFjdC5jcmVhdGVFbGVtZW50KExhenlSZW5kZXJCb3gsIHtcbiAgICAgICAgc3R5bGU6IHRoaXMuZ2V0WkluZGV4U3R5bGUoKSxcbiAgICAgICAga2V5OiAnbWFzaycsXG4gICAgICAgIGNsYXNzTmFtZTogcHJvcHMucHJlZml4Q2xzICsgJy1tYXNrJyxcbiAgICAgICAgaGlkZGVuQ2xhc3NOYW1lOiBwcm9wcy5wcmVmaXhDbHMgKyAnLW1hc2staGlkZGVuJyxcbiAgICAgICAgdmlzaWJsZTogcHJvcHMudmlzaWJsZVxuICAgICAgfSk7XG4gICAgICBpZiAobWFza1RyYW5zaXRpb24pIHtcbiAgICAgICAgbWFza0VsZW1lbnQgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIEFuaW1hdGUsXG4gICAgICAgICAge1xuICAgICAgICAgICAga2V5OiAnbWFzaycsXG4gICAgICAgICAgICBzaG93UHJvcDogJ3Zpc2libGUnLFxuICAgICAgICAgICAgdHJhbnNpdGlvbkFwcGVhcjogdHJ1ZSxcbiAgICAgICAgICAgIGNvbXBvbmVudDogJycsXG4gICAgICAgICAgICB0cmFuc2l0aW9uTmFtZTogbWFza1RyYW5zaXRpb25cbiAgICAgICAgICB9LFxuICAgICAgICAgIG1hc2tFbGVtZW50XG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBtYXNrRWxlbWVudDtcbiAgfTtcblxuICBQb3B1cC5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ2RpdicsXG4gICAgICBudWxsLFxuICAgICAgdGhpcy5nZXRNYXNrRWxlbWVudCgpLFxuICAgICAgdGhpcy5nZXRQb3B1cEVsZW1lbnQoKVxuICAgICk7XG4gIH07XG5cbiAgcmV0dXJuIFBvcHVwO1xufShDb21wb25lbnQpO1xuXG5Qb3B1cC5wcm9wVHlwZXMgPSB7XG4gIHZpc2libGU6IFByb3BUeXBlcy5ib29sLFxuICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgZ2V0Q2xhc3NOYW1lRnJvbUFsaWduOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25BbGlnbjogUHJvcFR5cGVzLmZ1bmMsXG4gIGdldFJvb3REb21Ob2RlOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Nb3VzZUVudGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgYWxpZ246IFByb3BUeXBlcy5hbnksXG4gIGRlc3Ryb3lQb3B1cE9uSGlkZTogUHJvcFR5cGVzLmJvb2wsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBvbk1vdXNlTGVhdmU6IFByb3BUeXBlcy5mdW5jLFxuICBzdHJldGNoOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBjaGlsZHJlbjogUHJvcFR5cGVzLm5vZGUsXG4gIHBvaW50OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIHBhZ2VYOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHBhZ2VZOiBQcm9wVHlwZXMubnVtYmVyXG4gIH0pXG59O1xuXG52YXIgX2luaXRpYWxpc2VQcm9wcyA9IGZ1bmN0aW9uIF9pbml0aWFsaXNlUHJvcHMoKSB7XG4gIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gIHRoaXMub25BbGlnbiA9IGZ1bmN0aW9uIChwb3B1cERvbU5vZGUsIGFsaWduKSB7XG4gICAgdmFyIHByb3BzID0gX3RoaXMzLnByb3BzO1xuICAgIHZhciBjdXJyZW50QWxpZ25DbGFzc05hbWUgPSBwcm9wcy5nZXRDbGFzc05hbWVGcm9tQWxpZ24oYWxpZ24pO1xuICAgIC8vIEZJWDogaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWNvbXBvbmVudC90cmlnZ2VyL2lzc3Vlcy81NlxuICAgIC8vIEZJWDogaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWNvbXBvbmVudC90b29sdGlwL2lzc3Vlcy83OVxuICAgIGlmIChfdGhpczMuY3VycmVudEFsaWduQ2xhc3NOYW1lICE9PSBjdXJyZW50QWxpZ25DbGFzc05hbWUpIHtcbiAgICAgIF90aGlzMy5jdXJyZW50QWxpZ25DbGFzc05hbWUgPSBjdXJyZW50QWxpZ25DbGFzc05hbWU7XG4gICAgICBwb3B1cERvbU5vZGUuY2xhc3NOYW1lID0gX3RoaXMzLmdldENsYXNzTmFtZShjdXJyZW50QWxpZ25DbGFzc05hbWUpO1xuICAgIH1cbiAgICBwcm9wcy5vbkFsaWduKHBvcHVwRG9tTm9kZSwgYWxpZ24pO1xuICB9O1xuXG4gIHRoaXMub25BbmltYXRlTGVhdmVkID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBzdHJldGNoID0gX3RoaXMzLnByb3BzLnN0cmV0Y2g7XG4gICAgdmFyIHN0cmV0Y2hDaGVja2VkID0gX3RoaXMzLnN0YXRlLnN0cmV0Y2hDaGVja2VkO1xuXG4gICAgaWYgKHN0cmV0Y2ggJiYgc3RyZXRjaENoZWNrZWQpIHtcbiAgICAgIF90aGlzMy5zZXRTdGF0ZSh7IHN0cmV0Y2hDaGVja2VkOiBmYWxzZSB9KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5zZXRTdHJldGNoU2l6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZ2V0Um9vdERvbU5vZGUgPSBfdGhpczMucHJvcHMuZ2V0Um9vdERvbU5vZGU7XG4gICAgdmFyIF9zdGF0ZTIgPSBfdGhpczMuc3RhdGUsXG4gICAgICAgIHN0cmV0Y2hDaGVja2VkID0gX3N0YXRlMi5zdHJldGNoQ2hlY2tlZCxcbiAgICAgICAgdGFyZ2V0SGVpZ2h0ID0gX3N0YXRlMi50YXJnZXRIZWlnaHQsXG4gICAgICAgIHRhcmdldFdpZHRoID0gX3N0YXRlMi50YXJnZXRXaWR0aDtcblxuXG4gICAgdmFyICRlbGUgPSBnZXRSb290RG9tTm9kZSgpO1xuICAgIGlmICghJGVsZSkgcmV0dXJuO1xuXG4gICAgdmFyIGhlaWdodCA9ICRlbGUub2Zmc2V0SGVpZ2h0O1xuICAgIHZhciB3aWR0aCA9ICRlbGUub2Zmc2V0V2lkdGg7XG5cbiAgICBpZiAodGFyZ2V0SGVpZ2h0ICE9PSBoZWlnaHQgfHwgdGFyZ2V0V2lkdGggIT09IHdpZHRoIHx8ICFzdHJldGNoQ2hlY2tlZCkge1xuICAgICAgX3RoaXMzLnNldFN0YXRlKHtcbiAgICAgICAgc3RyZXRjaENoZWNrZWQ6IHRydWUsXG4gICAgICAgIHRhcmdldEhlaWdodDogaGVpZ2h0LFxuICAgICAgICB0YXJnZXRXaWR0aDogd2lkdGhcbiAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmdldFRhcmdldEVsZW1lbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIF90aGlzMy5wcm9wcy5nZXRSb290RG9tTm9kZSgpO1xuICB9O1xuXG4gIHRoaXMuZ2V0QWxpZ25UYXJnZXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHBvaW50ID0gX3RoaXMzLnByb3BzLnBvaW50O1xuXG4gICAgaWYgKHBvaW50KSB7XG4gICAgICByZXR1cm4gcG9pbnQ7XG4gICAgfVxuICAgIHJldHVybiBfdGhpczMuZ2V0VGFyZ2V0RWxlbWVudDtcbiAgfTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFBvcHVwOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQVFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVdBO0FBREE7QUFPQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQWNBO0FBREE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFkQTtBQUNBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/node_modules/rc-trigger/es/Popup.js
