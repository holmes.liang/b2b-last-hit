/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Model = __webpack_require__(/*! ../model/Model */ "./node_modules/echarts/lib/model/Model.js");

var DataDiffer = __webpack_require__(/*! ./DataDiffer */ "./node_modules/echarts/lib/data/DataDiffer.js");

var Source = __webpack_require__(/*! ./Source */ "./node_modules/echarts/lib/data/Source.js");

var _dataProvider = __webpack_require__(/*! ./helper/dataProvider */ "./node_modules/echarts/lib/data/helper/dataProvider.js");

var defaultDimValueGetters = _dataProvider.defaultDimValueGetters;
var DefaultDataProvider = _dataProvider.DefaultDataProvider;

var _dimensionHelper = __webpack_require__(/*! ./helper/dimensionHelper */ "./node_modules/echarts/lib/data/helper/dimensionHelper.js");

var summarizeDimensions = _dimensionHelper.summarizeDimensions;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/* global Float64Array, Int32Array, Uint32Array, Uint16Array */

/**
 * List for data storage
 * @module echarts/data/List
 */

var isObject = zrUtil.isObject;
var UNDEFINED = 'undefined';
var INDEX_NOT_FOUND = -1; // Use prefix to avoid index to be the same as otherIdList[idx],
// which will cause weird udpate animation.

var ID_PREFIX = 'e\0\0';
var dataCtors = {
  'float': typeof Float64Array === UNDEFINED ? Array : Float64Array,
  'int': typeof Int32Array === UNDEFINED ? Array : Int32Array,
  // Ordinal data type can be string or int
  'ordinal': Array,
  'number': Array,
  'time': Array
}; // Caution: MUST not use `new CtorUint32Array(arr, 0, len)`, because the Ctor of array is
// different from the Ctor of typed array.

var CtorUint32Array = typeof Uint32Array === UNDEFINED ? Array : Uint32Array;
var CtorInt32Array = typeof Int32Array === UNDEFINED ? Array : Int32Array;
var CtorUint16Array = typeof Uint16Array === UNDEFINED ? Array : Uint16Array;

function getIndicesCtor(list) {
  // The possible max value in this._indicies is always this._rawCount despite of filtering.
  return list._rawCount > 65535 ? CtorUint32Array : CtorUint16Array;
}

function cloneChunk(originalChunk) {
  var Ctor = originalChunk.constructor; // Only shallow clone is enough when Array.

  return Ctor === Array ? originalChunk.slice() : new Ctor(originalChunk);
}

var TRANSFERABLE_PROPERTIES = ['hasItemOption', '_nameList', '_idList', '_invertedIndicesMap', '_rawData', '_chunkSize', '_chunkCount', '_dimValueGetter', '_count', '_rawCount', '_nameDimIdx', '_idDimIdx'];
var CLONE_PROPERTIES = ['_extent', '_approximateExtent', '_rawExtent'];

function transferProperties(target, source) {
  zrUtil.each(TRANSFERABLE_PROPERTIES.concat(source.__wrappedMethods || []), function (propName) {
    if (source.hasOwnProperty(propName)) {
      target[propName] = source[propName];
    }
  });
  target.__wrappedMethods = source.__wrappedMethods;
  zrUtil.each(CLONE_PROPERTIES, function (propName) {
    target[propName] = zrUtil.clone(source[propName]);
  });
  target._calculationInfo = zrUtil.extend(source._calculationInfo);
}
/**
 * @constructor
 * @alias module:echarts/data/List
 *
 * @param {Array.<string|Object>} dimensions
 *      For example, ['someDimName', {name: 'someDimName', type: 'someDimType'}, ...].
 *      Dimensions should be concrete names like x, y, z, lng, lat, angle, radius
 *      Spetial fields: {
 *          ordinalMeta: <module:echarts/data/OrdinalMeta>
 *          createInvertedIndices: <boolean>
 *      }
 * @param {module:echarts/model/Model} hostModel
 */


var List = function List(dimensions, hostModel) {
  dimensions = dimensions || ['x', 'y'];
  var dimensionInfos = {};
  var dimensionNames = [];
  var invertedIndicesMap = {};

  for (var i = 0; i < dimensions.length; i++) {
    // Use the original dimensions[i], where other flag props may exists.
    var dimensionInfo = dimensions[i];

    if (zrUtil.isString(dimensionInfo)) {
      dimensionInfo = {
        name: dimensionInfo
      };
    }

    var dimensionName = dimensionInfo.name;
    dimensionInfo.type = dimensionInfo.type || 'float';

    if (!dimensionInfo.coordDim) {
      dimensionInfo.coordDim = dimensionName;
      dimensionInfo.coordDimIndex = 0;
    }

    dimensionInfo.otherDims = dimensionInfo.otherDims || {};
    dimensionNames.push(dimensionName);
    dimensionInfos[dimensionName] = dimensionInfo;
    dimensionInfo.index = i;

    if (dimensionInfo.createInvertedIndices) {
      invertedIndicesMap[dimensionName] = [];
    }
  }
  /**
   * @readOnly
   * @type {Array.<string>}
   */


  this.dimensions = dimensionNames;
  /**
   * Infomation of each data dimension, like data type.
   * @type {Object}
   */

  this._dimensionInfos = dimensionInfos;
  /**
   * @type {module:echarts/model/Model}
   */

  this.hostModel = hostModel;
  /**
   * @type {module:echarts/model/Model}
   */

  this.dataType;
  /**
   * Indices stores the indices of data subset after filtered.
   * This data subset will be used in chart.
   * @type {Array.<number>}
   * @readOnly
   */

  this._indices = null;
  this._count = 0;
  this._rawCount = 0;
  /**
   * Data storage
   * @type {Object.<key, Array.<TypedArray|Array>>}
   * @private
   */

  this._storage = {};
  /**
   * @type {Array.<string>}
   */

  this._nameList = [];
  /**
   * @type {Array.<string>}
   */

  this._idList = [];
  /**
   * Models of data option is stored sparse for optimizing memory cost
   * @type {Array.<module:echarts/model/Model>}
   * @private
   */

  this._optionModels = [];
  /**
   * Global visual properties after visual coding
   * @type {Object}
   * @private
   */

  this._visual = {};
  /**
   * Globel layout properties.
   * @type {Object}
   * @private
   */

  this._layout = {};
  /**
   * Item visual properties after visual coding
   * @type {Array.<Object>}
   * @private
   */

  this._itemVisuals = [];
  /**
   * Key: visual type, Value: boolean
   * @type {Object}
   * @readOnly
   */

  this.hasItemVisual = {};
  /**
   * Item layout properties after layout
   * @type {Array.<Object>}
   * @private
   */

  this._itemLayouts = [];
  /**
   * Graphic elemnents
   * @type {Array.<module:zrender/Element>}
   * @private
   */

  this._graphicEls = [];
  /**
   * Max size of each chunk.
   * @type {number}
   * @private
   */

  this._chunkSize = 1e5;
  /**
   * @type {number}
   * @private
   */

  this._chunkCount = 0;
  /**
   * @type {Array.<Array|Object>}
   * @private
   */

  this._rawData;
  /**
   * Raw extent will not be cloned, but only transfered.
   * It will not be calculated util needed.
   * key: dim,
   * value: {end: number, extent: Array.<number>}
   * @type {Object}
   * @private
   */

  this._rawExtent = {};
  /**
   * @type {Object}
   * @private
   */

  this._extent = {};
  /**
   * key: dim
   * value: extent
   * @type {Object}
   * @private
   */

  this._approximateExtent = {};
  /**
   * Cache summary info for fast visit. See "dimensionHelper".
   * @type {Object}
   * @private
   */

  this._dimensionsSummary = summarizeDimensions(this);
  /**
   * @type {Object.<Array|TypedArray>}
   * @private
   */

  this._invertedIndicesMap = invertedIndicesMap;
  /**
   * @type {Object}
   * @private
   */

  this._calculationInfo = {};
};

var listProto = List.prototype;
listProto.type = 'list';
/**
 * If each data item has it's own option
 * @type {boolean}
 */

listProto.hasItemOption = true;
/**
 * Get dimension name
 * @param {string|number} dim
 *        Dimension can be concrete names like x, y, z, lng, lat, angle, radius
 *        Or a ordinal number. For example getDimensionInfo(0) will return 'x' or 'lng' or 'radius'
 * @return {string} Concrete dim name.
 */

listProto.getDimension = function (dim) {
  if (!isNaN(dim)) {
    dim = this.dimensions[dim] || dim;
  }

  return dim;
};
/**
 * Get type and calculation info of particular dimension
 * @param {string|number} dim
 *        Dimension can be concrete names like x, y, z, lng, lat, angle, radius
 *        Or a ordinal number. For example getDimensionInfo(0) will return 'x' or 'lng' or 'radius'
 */


listProto.getDimensionInfo = function (dim) {
  // Do not clone, because there may be categories in dimInfo.
  return this._dimensionInfos[this.getDimension(dim)];
};
/**
 * @return {Array.<string>} concrete dimension name list on coord.
 */


listProto.getDimensionsOnCoord = function () {
  return this._dimensionsSummary.dataDimsOnCoord.slice();
};
/**
 * @param {string} coordDim
 * @param {number} [idx] A coordDim may map to more than one data dim.
 *        If idx is `true`, return a array of all mapped dims.
 *        If idx is not specified, return the first dim not extra.
 * @return {string|Array.<string>} concrete data dim.
 *        If idx is number, and not found, return null/undefined.
 *        If idx is `true`, and not found, return empty array (always return array).
 */


listProto.mapDimension = function (coordDim, idx) {
  var dimensionsSummary = this._dimensionsSummary;

  if (idx == null) {
    return dimensionsSummary.encodeFirstDimNotExtra[coordDim];
  }

  var dims = dimensionsSummary.encode[coordDim];
  return idx === true // always return array if idx is `true`
  ? (dims || []).slice() : dims && dims[idx];
};
/**
 * Initialize from data
 * @param {Array.<Object|number|Array>} data source or data or data provider.
 * @param {Array.<string>} [nameLIst] The name of a datum is used on data diff and
 *        defualt label/tooltip.
 *        A name can be specified in encode.itemName,
 *        or dataItem.name (only for series option data),
 *        or provided in nameList from outside.
 * @param {Function} [dimValueGetter] (dataItem, dimName, dataIndex, dimIndex) => number
 */


listProto.initData = function (data, nameList, dimValueGetter) {
  var notProvider = Source.isInstance(data) || zrUtil.isArrayLike(data);

  if (notProvider) {
    data = new DefaultDataProvider(data, this.dimensions.length);
  }

  this._rawData = data; // Clear

  this._storage = {};
  this._indices = null;
  this._nameList = nameList || [];
  this._idList = [];
  this._nameRepeatCount = {};

  if (!dimValueGetter) {
    this.hasItemOption = false;
  }
  /**
   * @readOnly
   */


  this.defaultDimValueGetter = defaultDimValueGetters[this._rawData.getSource().sourceFormat]; // Default dim value getter

  this._dimValueGetter = dimValueGetter = dimValueGetter || this.defaultDimValueGetter;
  this._dimValueGetterArrayRows = defaultDimValueGetters.arrayRows; // Reset raw extent.

  this._rawExtent = {};

  this._initDataFromProvider(0, data.count()); // If data has no item option.


  if (data.pure) {
    this.hasItemOption = false;
  }
};

listProto.getProvider = function () {
  return this._rawData;
};
/**
 * Caution: Can be only called on raw data (before `this._indices` created).
 */


listProto.appendData = function (data) {
  var rawData = this._rawData;
  var start = this.count();
  rawData.appendData(data);
  var end = rawData.count();

  if (!rawData.persistent) {
    end += start;
  }

  this._initDataFromProvider(start, end);
};
/**
 * Caution: Can be only called on raw data (before `this._indices` created).
 * This method does not modify `rawData` (`dataProvider`), but only
 * add values to storage.
 *
 * The final count will be increased by `Math.max(values.length, names.length)`.
 *
 * @param {Array.<Array.<*>>} values That is the SourceType: 'arrayRows', like
 *        [
 *            [12, 33, 44],
 *            [NaN, 43, 1],
 *            ['-', 'asdf', 0]
 *        ]
 *        Each item is exaclty cooresponding to a dimension.
 * @param {Array.<string>} [names]
 */


listProto.appendValues = function (values, names) {
  var chunkSize = this._chunkSize;
  var storage = this._storage;
  var dimensions = this.dimensions;
  var dimLen = dimensions.length;
  var rawExtent = this._rawExtent;
  var start = this.count();
  var end = start + Math.max(values.length, names ? names.length : 0);
  var originalChunkCount = this._chunkCount;

  for (var i = 0; i < dimLen; i++) {
    var dim = dimensions[i];

    if (!rawExtent[dim]) {
      rawExtent[dim] = getInitialExtent();
    }

    if (!storage[dim]) {
      storage[dim] = [];
    }

    prepareChunks(storage, this._dimensionInfos[dim], chunkSize, originalChunkCount, end);
    this._chunkCount = storage[dim].length;
  }

  var emptyDataItem = new Array(dimLen);

  for (var idx = start; idx < end; idx++) {
    var sourceIdx = idx - start;
    var chunkIndex = Math.floor(idx / chunkSize);
    var chunkOffset = idx % chunkSize; // Store the data by dimensions

    for (var k = 0; k < dimLen; k++) {
      var dim = dimensions[k];

      var val = this._dimValueGetterArrayRows(values[sourceIdx] || emptyDataItem, dim, sourceIdx, k);

      storage[dim][chunkIndex][chunkOffset] = val;
      var dimRawExtent = rawExtent[dim];
      val < dimRawExtent[0] && (dimRawExtent[0] = val);
      val > dimRawExtent[1] && (dimRawExtent[1] = val);
    }

    if (names) {
      this._nameList[idx] = names[sourceIdx];
    }
  }

  this._rawCount = this._count = end; // Reset data extent

  this._extent = {};
  prepareInvertedIndex(this);
};

listProto._initDataFromProvider = function (start, end) {
  // Optimize.
  if (start >= end) {
    return;
  }

  var chunkSize = this._chunkSize;
  var rawData = this._rawData;
  var storage = this._storage;
  var dimensions = this.dimensions;
  var dimLen = dimensions.length;
  var dimensionInfoMap = this._dimensionInfos;
  var nameList = this._nameList;
  var idList = this._idList;
  var rawExtent = this._rawExtent;
  var nameRepeatCount = this._nameRepeatCount = {};
  var nameDimIdx;
  var originalChunkCount = this._chunkCount;

  for (var i = 0; i < dimLen; i++) {
    var dim = dimensions[i];

    if (!rawExtent[dim]) {
      rawExtent[dim] = getInitialExtent();
    }

    var dimInfo = dimensionInfoMap[dim];

    if (dimInfo.otherDims.itemName === 0) {
      nameDimIdx = this._nameDimIdx = i;
    }

    if (dimInfo.otherDims.itemId === 0) {
      this._idDimIdx = i;
    }

    if (!storage[dim]) {
      storage[dim] = [];
    }

    prepareChunks(storage, dimInfo, chunkSize, originalChunkCount, end);
    this._chunkCount = storage[dim].length;
  }

  var dataItem = new Array(dimLen);

  for (var idx = start; idx < end; idx++) {
    // NOTICE: Try not to write things into dataItem
    dataItem = rawData.getItem(idx, dataItem); // Each data item is value
    // [1, 2]
    // 2
    // Bar chart, line chart which uses category axis
    // only gives the 'y' value. 'x' value is the indices of category
    // Use a tempValue to normalize the value to be a (x, y) value

    var chunkIndex = Math.floor(idx / chunkSize);
    var chunkOffset = idx % chunkSize; // Store the data by dimensions

    for (var k = 0; k < dimLen; k++) {
      var dim = dimensions[k];
      var dimStorage = storage[dim][chunkIndex]; // PENDING NULL is empty or zero

      var val = this._dimValueGetter(dataItem, dim, idx, k);

      dimStorage[chunkOffset] = val;
      var dimRawExtent = rawExtent[dim];
      val < dimRawExtent[0] && (dimRawExtent[0] = val);
      val > dimRawExtent[1] && (dimRawExtent[1] = val);
    } // ??? FIXME not check by pure but sourceFormat?
    // TODO refactor these logic.


    if (!rawData.pure) {
      var name = nameList[idx];

      if (dataItem && name == null) {
        // If dataItem is {name: ...}, it has highest priority.
        // That is appropriate for many common cases.
        if (dataItem.name != null) {
          // There is no other place to persistent dataItem.name,
          // so save it to nameList.
          nameList[idx] = name = dataItem.name;
        } else if (nameDimIdx != null) {
          var nameDim = dimensions[nameDimIdx];
          var nameDimChunk = storage[nameDim][chunkIndex];

          if (nameDimChunk) {
            name = nameDimChunk[chunkOffset];
            var ordinalMeta = dimensionInfoMap[nameDim].ordinalMeta;

            if (ordinalMeta && ordinalMeta.categories.length) {
              name = ordinalMeta.categories[name];
            }
          }
        }
      } // Try using the id in option
      // id or name is used on dynamical data, mapping old and new items.


      var id = dataItem == null ? null : dataItem.id;

      if (id == null && name != null) {
        // Use name as id and add counter to avoid same name
        nameRepeatCount[name] = nameRepeatCount[name] || 0;
        id = name;

        if (nameRepeatCount[name] > 0) {
          id += '__ec__' + nameRepeatCount[name];
        }

        nameRepeatCount[name]++;
      }

      id != null && (idList[idx] = id);
    }
  }

  if (!rawData.persistent && rawData.clean) {
    // Clean unused data if data source is typed array.
    rawData.clean();
  }

  this._rawCount = this._count = end; // Reset data extent

  this._extent = {};
  prepareInvertedIndex(this);
};

function prepareChunks(storage, dimInfo, chunkSize, chunkCount, end) {
  var DataCtor = dataCtors[dimInfo.type];
  var lastChunkIndex = chunkCount - 1;
  var dim = dimInfo.name;
  var resizeChunkArray = storage[dim][lastChunkIndex];

  if (resizeChunkArray && resizeChunkArray.length < chunkSize) {
    var newStore = new DataCtor(Math.min(end - lastChunkIndex * chunkSize, chunkSize)); // The cost of the copy is probably inconsiderable
    // within the initial chunkSize.

    for (var j = 0; j < resizeChunkArray.length; j++) {
      newStore[j] = resizeChunkArray[j];
    }

    storage[dim][lastChunkIndex] = newStore;
  } // Create new chunks.


  for (var k = chunkCount * chunkSize; k < end; k += chunkSize) {
    storage[dim].push(new DataCtor(Math.min(end - k, chunkSize)));
  }
}

function prepareInvertedIndex(list) {
  var invertedIndicesMap = list._invertedIndicesMap;
  zrUtil.each(invertedIndicesMap, function (invertedIndices, dim) {
    var dimInfo = list._dimensionInfos[dim]; // Currently, only dimensions that has ordinalMeta can create inverted indices.

    var ordinalMeta = dimInfo.ordinalMeta;

    if (ordinalMeta) {
      invertedIndices = invertedIndicesMap[dim] = new CtorInt32Array(ordinalMeta.categories.length); // The default value of TypedArray is 0. To avoid miss
      // mapping to 0, we should set it as INDEX_NOT_FOUND.

      for (var i = 0; i < invertedIndices.length; i++) {
        invertedIndices[i] = INDEX_NOT_FOUND;
      }

      for (var i = 0; i < list._count; i++) {
        // Only support the case that all values are distinct.
        invertedIndices[list.get(dim, i)] = i;
      }
    }
  });
}

function getRawValueFromStore(list, dimIndex, rawIndex) {
  var val;

  if (dimIndex != null) {
    var chunkSize = list._chunkSize;
    var chunkIndex = Math.floor(rawIndex / chunkSize);
    var chunkOffset = rawIndex % chunkSize;
    var dim = list.dimensions[dimIndex];
    var chunk = list._storage[dim][chunkIndex];

    if (chunk) {
      val = chunk[chunkOffset];
      var ordinalMeta = list._dimensionInfos[dim].ordinalMeta;

      if (ordinalMeta && ordinalMeta.categories.length) {
        val = ordinalMeta.categories[val];
      }
    }
  }

  return val;
}
/**
 * @return {number}
 */


listProto.count = function () {
  return this._count;
};

listProto.getIndices = function () {
  var newIndices;
  var indices = this._indices;

  if (indices) {
    var Ctor = indices.constructor;
    var thisCount = this._count; // `new Array(a, b, c)` is different from `new Uint32Array(a, b, c)`.

    if (Ctor === Array) {
      newIndices = new Ctor(thisCount);

      for (var i = 0; i < thisCount; i++) {
        newIndices[i] = indices[i];
      }
    } else {
      newIndices = new Ctor(indices.buffer, 0, thisCount);
    }
  } else {
    var Ctor = getIndicesCtor(this);
    var newIndices = new Ctor(this.count());

    for (var i = 0; i < newIndices.length; i++) {
      newIndices[i] = i;
    }
  }

  return newIndices;
};
/**
 * Get value. Return NaN if idx is out of range.
 * @param {string} dim Dim must be concrete name.
 * @param {number} idx
 * @param {boolean} stack
 * @return {number}
 */


listProto.get = function (dim, idx
/*, stack */
) {
  if (!(idx >= 0 && idx < this._count)) {
    return NaN;
  }

  var storage = this._storage;

  if (!storage[dim]) {
    // TODO Warn ?
    return NaN;
  }

  idx = this.getRawIndex(idx);
  var chunkIndex = Math.floor(idx / this._chunkSize);
  var chunkOffset = idx % this._chunkSize;
  var chunkStore = storage[dim][chunkIndex];
  var value = chunkStore[chunkOffset]; // FIXME ordinal data type is not stackable
  // if (stack) {
  //     var dimensionInfo = this._dimensionInfos[dim];
  //     if (dimensionInfo && dimensionInfo.stackable) {
  //         var stackedOn = this.stackedOn;
  //         while (stackedOn) {
  //             // Get no stacked data of stacked on
  //             var stackedValue = stackedOn.get(dim, idx);
  //             // Considering positive stack, negative stack and empty data
  //             if ((value >= 0 && stackedValue > 0)  // Positive stack
  //                 || (value <= 0 && stackedValue < 0) // Negative stack
  //             ) {
  //                 value += stackedValue;
  //             }
  //             stackedOn = stackedOn.stackedOn;
  //         }
  //     }
  // }

  return value;
};
/**
 * @param {string} dim concrete dim
 * @param {number} rawIndex
 * @return {number|string}
 */


listProto.getByRawIndex = function (dim, rawIdx) {
  if (!(rawIdx >= 0 && rawIdx < this._rawCount)) {
    return NaN;
  }

  var dimStore = this._storage[dim];

  if (!dimStore) {
    // TODO Warn ?
    return NaN;
  }

  var chunkIndex = Math.floor(rawIdx / this._chunkSize);
  var chunkOffset = rawIdx % this._chunkSize;
  var chunkStore = dimStore[chunkIndex];
  return chunkStore[chunkOffset];
};
/**
 * FIXME Use `get` on chrome maybe slow(in filterSelf and selectRange).
 * Hack a much simpler _getFast
 * @private
 */


listProto._getFast = function (dim, rawIdx) {
  var chunkIndex = Math.floor(rawIdx / this._chunkSize);
  var chunkOffset = rawIdx % this._chunkSize;
  var chunkStore = this._storage[dim][chunkIndex];
  return chunkStore[chunkOffset];
};
/**
 * Get value for multi dimensions.
 * @param {Array.<string>} [dimensions] If ignored, using all dimensions.
 * @param {number} idx
 * @return {number}
 */


listProto.getValues = function (dimensions, idx
/*, stack */
) {
  var values = [];

  if (!zrUtil.isArray(dimensions)) {
    // stack = idx;
    idx = dimensions;
    dimensions = this.dimensions;
  }

  for (var i = 0, len = dimensions.length; i < len; i++) {
    values.push(this.get(dimensions[i], idx
    /*, stack */
    ));
  }

  return values;
};
/**
 * If value is NaN. Inlcuding '-'
 * Only check the coord dimensions.
 * @param {string} dim
 * @param {number} idx
 * @return {number}
 */


listProto.hasValue = function (idx) {
  var dataDimsOnCoord = this._dimensionsSummary.dataDimsOnCoord;
  var dimensionInfos = this._dimensionInfos;

  for (var i = 0, len = dataDimsOnCoord.length; i < len; i++) {
    if ( // Ordinal type can be string or number
    dimensionInfos[dataDimsOnCoord[i]].type !== 'ordinal' // FIXME check ordinal when using index?
    && isNaN(this.get(dataDimsOnCoord[i], idx))) {
      return false;
    }
  }

  return true;
};
/**
 * Get extent of data in one dimension
 * @param {string} dim
 * @param {boolean} stack
 */


listProto.getDataExtent = function (dim
/*, stack */
) {
  // Make sure use concrete dim as cache name.
  dim = this.getDimension(dim);
  var dimData = this._storage[dim];
  var initialExtent = getInitialExtent(); // stack = !!((stack || false) && this.getCalculationInfo(dim));

  if (!dimData) {
    return initialExtent;
  } // Make more strict checkings to ensure hitting cache.


  var currEnd = this.count(); // var cacheName = [dim, !!stack].join('_');
  // var cacheName = dim;
  // Consider the most cases when using data zoom, `getDataExtent`
  // happened before filtering. We cache raw extent, which is not
  // necessary to be cleared and recalculated when restore data.

  var useRaw = !this._indices; // && !stack;

  var dimExtent;

  if (useRaw) {
    return this._rawExtent[dim].slice();
  }

  dimExtent = this._extent[dim];

  if (dimExtent) {
    return dimExtent.slice();
  }

  dimExtent = initialExtent;
  var min = dimExtent[0];
  var max = dimExtent[1];

  for (var i = 0; i < currEnd; i++) {
    // var value = stack ? this.get(dim, i, true) : this._getFast(dim, this.getRawIndex(i));
    var value = this._getFast(dim, this.getRawIndex(i));

    value < min && (min = value);
    value > max && (max = value);
  }

  dimExtent = [min, max];
  this._extent[dim] = dimExtent;
  return dimExtent;
};
/**
 * Optimize for the scenario that data is filtered by a given extent.
 * Consider that if data amount is more than hundreds of thousand,
 * extent calculation will cost more than 10ms and the cache will
 * be erased because of the filtering.
 */


listProto.getApproximateExtent = function (dim
/*, stack */
) {
  dim = this.getDimension(dim);
  return this._approximateExtent[dim] || this.getDataExtent(dim
  /*, stack */
  );
};

listProto.setApproximateExtent = function (extent, dim
/*, stack */
) {
  dim = this.getDimension(dim);
  this._approximateExtent[dim] = extent.slice();
};
/**
 * @param {string} key
 * @return {*}
 */


listProto.getCalculationInfo = function (key) {
  return this._calculationInfo[key];
};
/**
 * @param {string|Object} key or k-v object
 * @param {*} [value]
 */


listProto.setCalculationInfo = function (key, value) {
  isObject(key) ? zrUtil.extend(this._calculationInfo, key) : this._calculationInfo[key] = value;
};
/**
 * Get sum of data in one dimension
 * @param {string} dim
 */


listProto.getSum = function (dim
/*, stack */
) {
  var dimData = this._storage[dim];
  var sum = 0;

  if (dimData) {
    for (var i = 0, len = this.count(); i < len; i++) {
      var value = this.get(dim, i
      /*, stack */
      );

      if (!isNaN(value)) {
        sum += value;
      }
    }
  }

  return sum;
};
/**
 * Get median of data in one dimension
 * @param {string} dim
 */


listProto.getMedian = function (dim
/*, stack */
) {
  var dimDataArray = []; // map all data of one dimension

  this.each(dim, function (val, idx) {
    if (!isNaN(val)) {
      dimDataArray.push(val);
    }
  }); // TODO
  // Use quick select?
  // immutability & sort

  var sortedDimDataArray = [].concat(dimDataArray).sort(function (a, b) {
    return a - b;
  });
  var len = this.count(); // calculate median

  return len === 0 ? 0 : len % 2 === 1 ? sortedDimDataArray[(len - 1) / 2] : (sortedDimDataArray[len / 2] + sortedDimDataArray[len / 2 - 1]) / 2;
}; // /**
//  * Retreive the index with given value
//  * @param {string} dim Concrete dimension.
//  * @param {number} value
//  * @return {number}
//  */
// Currently incorrect: should return dataIndex but not rawIndex.
// Do not fix it until this method is to be used somewhere.
// FIXME Precision of float value
// listProto.indexOf = function (dim, value) {
//     var storage = this._storage;
//     var dimData = storage[dim];
//     var chunkSize = this._chunkSize;
//     if (dimData) {
//         for (var i = 0, len = this.count(); i < len; i++) {
//             var chunkIndex = Math.floor(i / chunkSize);
//             var chunkOffset = i % chunkSize;
//             if (dimData[chunkIndex][chunkOffset] === value) {
//                 return i;
//             }
//         }
//     }
//     return -1;
// };

/**
 * Only support the dimension which inverted index created.
 * Do not support other cases until required.
 * @param {string} concrete dim
 * @param {number|string} value
 * @return {number} rawIndex
 */


listProto.rawIndexOf = function (dim, value) {
  var invertedIndices = dim && this._invertedIndicesMap[dim];
  var rawIndex = invertedIndices[value];

  if (rawIndex == null || isNaN(rawIndex)) {
    return INDEX_NOT_FOUND;
  }

  return rawIndex;
};
/**
 * Retreive the index with given name
 * @param {number} idx
 * @param {number} name
 * @return {number}
 */


listProto.indexOfName = function (name) {
  for (var i = 0, len = this.count(); i < len; i++) {
    if (this.getName(i) === name) {
      return i;
    }
  }

  return -1;
};
/**
 * Retreive the index with given raw data index
 * @param {number} idx
 * @param {number} name
 * @return {number}
 */


listProto.indexOfRawIndex = function (rawIndex) {
  if (!this._indices) {
    return rawIndex;
  }

  if (rawIndex >= this._rawCount || rawIndex < 0) {
    return -1;
  } // Indices are ascending


  var indices = this._indices; // If rawIndex === dataIndex

  var rawDataIndex = indices[rawIndex];

  if (rawDataIndex != null && rawDataIndex < this._count && rawDataIndex === rawIndex) {
    return rawIndex;
  }

  var left = 0;
  var right = this._count - 1;

  while (left <= right) {
    var mid = (left + right) / 2 | 0;

    if (indices[mid] < rawIndex) {
      left = mid + 1;
    } else if (indices[mid] > rawIndex) {
      right = mid - 1;
    } else {
      return mid;
    }
  }

  return -1;
};
/**
 * Retreive the index of nearest value
 * @param {string} dim
 * @param {number} value
 * @param {number} [maxDistance=Infinity]
 * @return {Array.<number>} Considere multiple points has the same value.
 */


listProto.indicesOfNearest = function (dim, value, maxDistance) {
  var storage = this._storage;
  var dimData = storage[dim];
  var nearestIndices = [];

  if (!dimData) {
    return nearestIndices;
  }

  if (maxDistance == null) {
    maxDistance = Infinity;
  }

  var minDist = Number.MAX_VALUE;
  var minDiff = -1;

  for (var i = 0, len = this.count(); i < len; i++) {
    var diff = value - this.get(dim, i
    /*, stack */
    );
    var dist = Math.abs(diff);

    if (diff <= maxDistance && dist <= minDist) {
      // For the case of two data are same on xAxis, which has sequence data.
      // Show the nearest index
      // https://github.com/ecomfe/echarts/issues/2869
      if (dist < minDist || diff >= 0 && minDiff < 0) {
        minDist = dist;
        minDiff = diff;
        nearestIndices.length = 0;
      }

      nearestIndices.push(i);
    }
  }

  return nearestIndices;
};
/**
 * Get raw data index
 * @param {number} idx
 * @return {number}
 */


listProto.getRawIndex = getRawIndexWithoutIndices;

function getRawIndexWithoutIndices(idx) {
  return idx;
}

function getRawIndexWithIndices(idx) {
  if (idx < this._count && idx >= 0) {
    return this._indices[idx];
  }

  return -1;
}
/**
 * Get raw data item
 * @param {number} idx
 * @return {number}
 */


listProto.getRawDataItem = function (idx) {
  if (!this._rawData.persistent) {
    var val = [];

    for (var i = 0; i < this.dimensions.length; i++) {
      var dim = this.dimensions[i];
      val.push(this.get(dim, idx));
    }

    return val;
  } else {
    return this._rawData.getItem(this.getRawIndex(idx));
  }
};
/**
 * @param {number} idx
 * @param {boolean} [notDefaultIdx=false]
 * @return {string}
 */


listProto.getName = function (idx) {
  var rawIndex = this.getRawIndex(idx);
  return this._nameList[rawIndex] || getRawValueFromStore(this, this._nameDimIdx, rawIndex) || '';
};
/**
 * @param {number} idx
 * @param {boolean} [notDefaultIdx=false]
 * @return {string}
 */


listProto.getId = function (idx) {
  return getId(this, this.getRawIndex(idx));
};

function getId(list, rawIndex) {
  var id = list._idList[rawIndex];

  if (id == null) {
    id = getRawValueFromStore(list, list._idDimIdx, rawIndex);
  }

  if (id == null) {
    // FIXME Check the usage in graph, should not use prefix.
    id = ID_PREFIX + rawIndex;
  }

  return id;
}

function normalizeDimensions(dimensions) {
  if (!zrUtil.isArray(dimensions)) {
    dimensions = [dimensions];
  }

  return dimensions;
}

function validateDimensions(list, dims) {
  for (var i = 0; i < dims.length; i++) {
    // stroage may be empty when no data, so use
    // dimensionInfos to check.
    if (!list._dimensionInfos[dims[i]]) {
      console.error('Unkown dimension ' + dims[i]);
    }
  }
}
/**
 * Data iteration
 * @param {string|Array.<string>}
 * @param {Function} cb
 * @param {*} [context=this]
 *
 * @example
 *  list.each('x', function (x, idx) {});
 *  list.each(['x', 'y'], function (x, y, idx) {});
 *  list.each(function (idx) {})
 */


listProto.each = function (dims, cb, context, contextCompat) {
  'use strict';

  if (!this._count) {
    return;
  }

  if (typeof dims === 'function') {
    contextCompat = context;
    context = cb;
    cb = dims;
    dims = [];
  } // contextCompat just for compat echarts3


  context = context || contextCompat || this;
  dims = zrUtil.map(normalizeDimensions(dims), this.getDimension, this);
  var dimSize = dims.length;

  for (var i = 0; i < this.count(); i++) {
    // Simple optimization
    switch (dimSize) {
      case 0:
        cb.call(context, i);
        break;

      case 1:
        cb.call(context, this.get(dims[0], i), i);
        break;

      case 2:
        cb.call(context, this.get(dims[0], i), this.get(dims[1], i), i);
        break;

      default:
        var k = 0;
        var value = [];

        for (; k < dimSize; k++) {
          value[k] = this.get(dims[k], i);
        } // Index


        value[k] = i;
        cb.apply(context, value);
    }
  }
};
/**
 * Data filter
 * @param {string|Array.<string>}
 * @param {Function} cb
 * @param {*} [context=this]
 */


listProto.filterSelf = function (dimensions, cb, context, contextCompat) {
  'use strict';

  if (!this._count) {
    return;
  }

  if (typeof dimensions === 'function') {
    contextCompat = context;
    context = cb;
    cb = dimensions;
    dimensions = [];
  } // contextCompat just for compat echarts3


  context = context || contextCompat || this;
  dimensions = zrUtil.map(normalizeDimensions(dimensions), this.getDimension, this);
  var count = this.count();
  var Ctor = getIndicesCtor(this);
  var newIndices = new Ctor(count);
  var value = [];
  var dimSize = dimensions.length;
  var offset = 0;
  var dim0 = dimensions[0];

  for (var i = 0; i < count; i++) {
    var keep;
    var rawIdx = this.getRawIndex(i); // Simple optimization

    if (dimSize === 0) {
      keep = cb.call(context, i);
    } else if (dimSize === 1) {
      var val = this._getFast(dim0, rawIdx);

      keep = cb.call(context, val, i);
    } else {
      for (var k = 0; k < dimSize; k++) {
        value[k] = this._getFast(dim0, rawIdx);
      }

      value[k] = i;
      keep = cb.apply(context, value);
    }

    if (keep) {
      newIndices[offset++] = rawIdx;
    }
  } // Set indices after filtered.


  if (offset < count) {
    this._indices = newIndices;
  }

  this._count = offset; // Reset data extent

  this._extent = {};
  this.getRawIndex = this._indices ? getRawIndexWithIndices : getRawIndexWithoutIndices;
  return this;
};
/**
 * Select data in range. (For optimization of filter)
 * (Manually inline code, support 5 million data filtering in data zoom.)
 */


listProto.selectRange = function (range) {
  'use strict';

  if (!this._count) {
    return;
  }

  var dimensions = [];

  for (var dim in range) {
    if (range.hasOwnProperty(dim)) {
      dimensions.push(dim);
    }
  }

  var dimSize = dimensions.length;

  if (!dimSize) {
    return;
  }

  var originalCount = this.count();
  var Ctor = getIndicesCtor(this);
  var newIndices = new Ctor(originalCount);
  var offset = 0;
  var dim0 = dimensions[0];
  var min = range[dim0][0];
  var max = range[dim0][1];
  var quickFinished = false;

  if (!this._indices) {
    // Extreme optimization for common case. About 2x faster in chrome.
    var idx = 0;

    if (dimSize === 1) {
      var dimStorage = this._storage[dimensions[0]];

      for (var k = 0; k < this._chunkCount; k++) {
        var chunkStorage = dimStorage[k];
        var len = Math.min(this._count - k * this._chunkSize, this._chunkSize);

        for (var i = 0; i < len; i++) {
          var val = chunkStorage[i]; // NaN will not be filtered. Consider the case, in line chart, empty
          // value indicates the line should be broken. But for the case like
          // scatter plot, a data item with empty value will not be rendered,
          // but the axis extent may be effected if some other dim of the data
          // item has value. Fortunately it is not a significant negative effect.

          if (val >= min && val <= max || isNaN(val)) {
            newIndices[offset++] = idx;
          }

          idx++;
        }
      }

      quickFinished = true;
    } else if (dimSize === 2) {
      var dimStorage = this._storage[dim0];
      var dimStorage2 = this._storage[dimensions[1]];
      var min2 = range[dimensions[1]][0];
      var max2 = range[dimensions[1]][1];

      for (var k = 0; k < this._chunkCount; k++) {
        var chunkStorage = dimStorage[k];
        var chunkStorage2 = dimStorage2[k];
        var len = Math.min(this._count - k * this._chunkSize, this._chunkSize);

        for (var i = 0; i < len; i++) {
          var val = chunkStorage[i];
          var val2 = chunkStorage2[i]; // Do not filter NaN, see comment above.

          if ((val >= min && val <= max || isNaN(val)) && (val2 >= min2 && val2 <= max2 || isNaN(val2))) {
            newIndices[offset++] = idx;
          }

          idx++;
        }
      }

      quickFinished = true;
    }
  }

  if (!quickFinished) {
    if (dimSize === 1) {
      for (var i = 0; i < originalCount; i++) {
        var rawIndex = this.getRawIndex(i);

        var val = this._getFast(dim0, rawIndex); // Do not filter NaN, see comment above.


        if (val >= min && val <= max || isNaN(val)) {
          newIndices[offset++] = rawIndex;
        }
      }
    } else {
      for (var i = 0; i < originalCount; i++) {
        var keep = true;
        var rawIndex = this.getRawIndex(i);

        for (var k = 0; k < dimSize; k++) {
          var dimk = dimensions[k];

          var val = this._getFast(dim, rawIndex); // Do not filter NaN, see comment above.


          if (val < range[dimk][0] || val > range[dimk][1]) {
            keep = false;
          }
        }

        if (keep) {
          newIndices[offset++] = this.getRawIndex(i);
        }
      }
    }
  } // Set indices after filtered.


  if (offset < originalCount) {
    this._indices = newIndices;
  }

  this._count = offset; // Reset data extent

  this._extent = {};
  this.getRawIndex = this._indices ? getRawIndexWithIndices : getRawIndexWithoutIndices;
  return this;
};
/**
 * Data mapping to a plain array
 * @param {string|Array.<string>} [dimensions]
 * @param {Function} cb
 * @param {*} [context=this]
 * @return {Array}
 */


listProto.mapArray = function (dimensions, cb, context, contextCompat) {
  'use strict';

  if (typeof dimensions === 'function') {
    contextCompat = context;
    context = cb;
    cb = dimensions;
    dimensions = [];
  } // contextCompat just for compat echarts3


  context = context || contextCompat || this;
  var result = [];
  this.each(dimensions, function () {
    result.push(cb && cb.apply(this, arguments));
  }, context);
  return result;
}; // Data in excludeDimensions is copied, otherwise transfered.


function cloneListForMapAndSample(original, excludeDimensions) {
  var allDimensions = original.dimensions;
  var list = new List(zrUtil.map(allDimensions, original.getDimensionInfo, original), original.hostModel); // FIXME If needs stackedOn, value may already been stacked

  transferProperties(list, original);
  var storage = list._storage = {};
  var originalStorage = original._storage; // Init storage

  for (var i = 0; i < allDimensions.length; i++) {
    var dim = allDimensions[i];

    if (originalStorage[dim]) {
      // Notice that we do not reset invertedIndicesMap here, becuase
      // there is no scenario of mapping or sampling ordinal dimension.
      if (zrUtil.indexOf(excludeDimensions, dim) >= 0) {
        storage[dim] = cloneDimStore(originalStorage[dim]);
        list._rawExtent[dim] = getInitialExtent();
        list._extent[dim] = null;
      } else {
        // Direct reference for other dimensions
        storage[dim] = originalStorage[dim];
      }
    }
  }

  return list;
}

function cloneDimStore(originalDimStore) {
  var newDimStore = new Array(originalDimStore.length);

  for (var j = 0; j < originalDimStore.length; j++) {
    newDimStore[j] = cloneChunk(originalDimStore[j]);
  }

  return newDimStore;
}

function getInitialExtent() {
  return [Infinity, -Infinity];
}
/**
 * Data mapping to a new List with given dimensions
 * @param {string|Array.<string>} dimensions
 * @param {Function} cb
 * @param {*} [context=this]
 * @return {Array}
 */


listProto.map = function (dimensions, cb, context, contextCompat) {
  'use strict'; // contextCompat just for compat echarts3

  context = context || contextCompat || this;
  dimensions = zrUtil.map(normalizeDimensions(dimensions), this.getDimension, this);
  var list = cloneListForMapAndSample(this, dimensions); // Following properties are all immutable.
  // So we can reference to the same value

  list._indices = this._indices;
  list.getRawIndex = list._indices ? getRawIndexWithIndices : getRawIndexWithoutIndices;
  var storage = list._storage;
  var tmpRetValue = [];
  var chunkSize = this._chunkSize;
  var dimSize = dimensions.length;
  var dataCount = this.count();
  var values = [];
  var rawExtent = list._rawExtent;

  for (var dataIndex = 0; dataIndex < dataCount; dataIndex++) {
    for (var dimIndex = 0; dimIndex < dimSize; dimIndex++) {
      values[dimIndex] = this.get(dimensions[dimIndex], dataIndex
      /*, stack */
      );
    }

    values[dimSize] = dataIndex;
    var retValue = cb && cb.apply(context, values);

    if (retValue != null) {
      // a number or string (in oridinal dimension)?
      if (typeof retValue !== 'object') {
        tmpRetValue[0] = retValue;
        retValue = tmpRetValue;
      }

      var rawIndex = this.getRawIndex(dataIndex);
      var chunkIndex = Math.floor(rawIndex / chunkSize);
      var chunkOffset = rawIndex % chunkSize;

      for (var i = 0; i < retValue.length; i++) {
        var dim = dimensions[i];
        var val = retValue[i];
        var rawExtentOnDim = rawExtent[dim];
        var dimStore = storage[dim];

        if (dimStore) {
          dimStore[chunkIndex][chunkOffset] = val;
        }

        if (val < rawExtentOnDim[0]) {
          rawExtentOnDim[0] = val;
        }

        if (val > rawExtentOnDim[1]) {
          rawExtentOnDim[1] = val;
        }
      }
    }
  }

  return list;
};
/**
 * Large data down sampling on given dimension
 * @param {string} dimension
 * @param {number} rate
 * @param {Function} sampleValue
 * @param {Function} sampleIndex Sample index for name and id
 */


listProto.downSample = function (dimension, rate, sampleValue, sampleIndex) {
  var list = cloneListForMapAndSample(this, [dimension]);
  var targetStorage = list._storage;
  var frameValues = [];
  var frameSize = Math.floor(1 / rate);
  var dimStore = targetStorage[dimension];
  var len = this.count();
  var chunkSize = this._chunkSize;
  var rawExtentOnDim = list._rawExtent[dimension];
  var newIndices = new (getIndicesCtor(this))(len);
  var offset = 0;

  for (var i = 0; i < len; i += frameSize) {
    // Last frame
    if (frameSize > len - i) {
      frameSize = len - i;
      frameValues.length = frameSize;
    }

    for (var k = 0; k < frameSize; k++) {
      var dataIdx = this.getRawIndex(i + k);
      var originalChunkIndex = Math.floor(dataIdx / chunkSize);
      var originalChunkOffset = dataIdx % chunkSize;
      frameValues[k] = dimStore[originalChunkIndex][originalChunkOffset];
    }

    var value = sampleValue(frameValues);
    var sampleFrameIdx = this.getRawIndex(Math.min(i + sampleIndex(frameValues, value) || 0, len - 1));
    var sampleChunkIndex = Math.floor(sampleFrameIdx / chunkSize);
    var sampleChunkOffset = sampleFrameIdx % chunkSize; // Only write value on the filtered data

    dimStore[sampleChunkIndex][sampleChunkOffset] = value;

    if (value < rawExtentOnDim[0]) {
      rawExtentOnDim[0] = value;
    }

    if (value > rawExtentOnDim[1]) {
      rawExtentOnDim[1] = value;
    }

    newIndices[offset++] = sampleFrameIdx;
  }

  list._count = offset;
  list._indices = newIndices;
  list.getRawIndex = getRawIndexWithIndices;
  return list;
};
/**
 * Get model of one data item.
 *
 * @param {number} idx
 */
// FIXME Model proxy ?


listProto.getItemModel = function (idx) {
  var hostModel = this.hostModel;
  return new Model(this.getRawDataItem(idx), hostModel, hostModel && hostModel.ecModel);
};
/**
 * Create a data differ
 * @param {module:echarts/data/List} otherList
 * @return {module:echarts/data/DataDiffer}
 */


listProto.diff = function (otherList) {
  var thisList = this;
  return new DataDiffer(otherList ? otherList.getIndices() : [], this.getIndices(), function (idx) {
    return getId(otherList, idx);
  }, function (idx) {
    return getId(thisList, idx);
  });
};
/**
 * Get visual property.
 * @param {string} key
 */


listProto.getVisual = function (key) {
  var visual = this._visual;
  return visual && visual[key];
};
/**
 * Set visual property
 * @param {string|Object} key
 * @param {*} [value]
 *
 * @example
 *  setVisual('color', color);
 *  setVisual({
 *      'color': color
 *  });
 */


listProto.setVisual = function (key, val) {
  if (isObject(key)) {
    for (var name in key) {
      if (key.hasOwnProperty(name)) {
        this.setVisual(name, key[name]);
      }
    }

    return;
  }

  this._visual = this._visual || {};
  this._visual[key] = val;
};
/**
 * Set layout property.
 * @param {string|Object} key
 * @param {*} [val]
 */


listProto.setLayout = function (key, val) {
  if (isObject(key)) {
    for (var name in key) {
      if (key.hasOwnProperty(name)) {
        this.setLayout(name, key[name]);
      }
    }

    return;
  }

  this._layout[key] = val;
};
/**
 * Get layout property.
 * @param  {string} key.
 * @return {*}
 */


listProto.getLayout = function (key) {
  return this._layout[key];
};
/**
 * Get layout of single data item
 * @param {number} idx
 */


listProto.getItemLayout = function (idx) {
  return this._itemLayouts[idx];
};
/**
 * Set layout of single data item
 * @param {number} idx
 * @param {Object} layout
 * @param {boolean=} [merge=false]
 */


listProto.setItemLayout = function (idx, layout, merge) {
  this._itemLayouts[idx] = merge ? zrUtil.extend(this._itemLayouts[idx] || {}, layout) : layout;
};
/**
 * Clear all layout of single data item
 */


listProto.clearItemLayouts = function () {
  this._itemLayouts.length = 0;
};
/**
 * Get visual property of single data item
 * @param {number} idx
 * @param {string} key
 * @param {boolean} [ignoreParent=false]
 */


listProto.getItemVisual = function (idx, key, ignoreParent) {
  var itemVisual = this._itemVisuals[idx];
  var val = itemVisual && itemVisual[key];

  if (val == null && !ignoreParent) {
    // Use global visual property
    return this.getVisual(key);
  }

  return val;
};
/**
 * Set visual property of single data item
 *
 * @param {number} idx
 * @param {string|Object} key
 * @param {*} [value]
 *
 * @example
 *  setItemVisual(0, 'color', color);
 *  setItemVisual(0, {
 *      'color': color
 *  });
 */


listProto.setItemVisual = function (idx, key, value) {
  var itemVisual = this._itemVisuals[idx] || {};
  var hasItemVisual = this.hasItemVisual;
  this._itemVisuals[idx] = itemVisual;

  if (isObject(key)) {
    for (var name in key) {
      if (key.hasOwnProperty(name)) {
        itemVisual[name] = key[name];
        hasItemVisual[name] = true;
      }
    }

    return;
  }

  itemVisual[key] = value;
  hasItemVisual[key] = true;
};
/**
 * Clear itemVisuals and list visual.
 */


listProto.clearAllVisual = function () {
  this._visual = {};
  this._itemVisuals = [];
  this.hasItemVisual = {};
};

var setItemDataAndSeriesIndex = function setItemDataAndSeriesIndex(child) {
  child.seriesIndex = this.seriesIndex;
  child.dataIndex = this.dataIndex;
  child.dataType = this.dataType;
};
/**
 * Set graphic element relative to data. It can be set as null
 * @param {number} idx
 * @param {module:zrender/Element} [el]
 */


listProto.setItemGraphicEl = function (idx, el) {
  var hostModel = this.hostModel;

  if (el) {
    // Add data index and series index for indexing the data by element
    // Useful in tooltip
    el.dataIndex = idx;
    el.dataType = this.dataType;
    el.seriesIndex = hostModel && hostModel.seriesIndex;

    if (el.type === 'group') {
      el.traverse(setItemDataAndSeriesIndex, el);
    }
  }

  this._graphicEls[idx] = el;
};
/**
 * @param {number} idx
 * @return {module:zrender/Element}
 */


listProto.getItemGraphicEl = function (idx) {
  return this._graphicEls[idx];
};
/**
 * @param {Function} cb
 * @param {*} context
 */


listProto.eachItemGraphicEl = function (cb, context) {
  zrUtil.each(this._graphicEls, function (el, idx) {
    if (el) {
      cb && cb.call(context, el, idx);
    }
  });
};
/**
 * Shallow clone a new list except visual and layout properties, and graph elements.
 * New list only change the indices.
 */


listProto.cloneShallow = function (list) {
  if (!list) {
    var dimensionInfoList = zrUtil.map(this.dimensions, this.getDimensionInfo, this);
    list = new List(dimensionInfoList, this.hostModel);
  } // FIXME


  list._storage = this._storage;
  transferProperties(list, this); // Clone will not change the data extent and indices

  if (this._indices) {
    var Ctor = this._indices.constructor;
    list._indices = new Ctor(this._indices);
  } else {
    list._indices = null;
  }

  list.getRawIndex = list._indices ? getRawIndexWithIndices : getRawIndexWithoutIndices;
  return list;
};
/**
 * Wrap some method to add more feature
 * @param {string} methodName
 * @param {Function} injectFunction
 */


listProto.wrapMethod = function (methodName, injectFunction) {
  var originalMethod = this[methodName];

  if (typeof originalMethod !== 'function') {
    return;
  }

  this.__wrappedMethods = this.__wrappedMethods || [];

  this.__wrappedMethods.push(methodName);

  this[methodName] = function () {
    var res = originalMethod.apply(this, arguments);
    return injectFunction.apply(this, [res].concat(zrUtil.slice(arguments)));
  };
}; // Methods that create a new list based on this list should be listed here.
// Notice that those method should `RETURN` the new list.


listProto.TRANSFERABLE_METHODS = ['cloneShallow', 'downSample', 'map']; // Methods that change indices of this list should be listed here.

listProto.CHANGABLE_METHODS = ['filterSelf', 'selectRange'];
var _default = List;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9MaXN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9MaXN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIE1vZGVsID0gcmVxdWlyZShcIi4uL21vZGVsL01vZGVsXCIpO1xuXG52YXIgRGF0YURpZmZlciA9IHJlcXVpcmUoXCIuL0RhdGFEaWZmZXJcIik7XG5cbnZhciBTb3VyY2UgPSByZXF1aXJlKFwiLi9Tb3VyY2VcIik7XG5cbnZhciBfZGF0YVByb3ZpZGVyID0gcmVxdWlyZShcIi4vaGVscGVyL2RhdGFQcm92aWRlclwiKTtcblxudmFyIGRlZmF1bHREaW1WYWx1ZUdldHRlcnMgPSBfZGF0YVByb3ZpZGVyLmRlZmF1bHREaW1WYWx1ZUdldHRlcnM7XG52YXIgRGVmYXVsdERhdGFQcm92aWRlciA9IF9kYXRhUHJvdmlkZXIuRGVmYXVsdERhdGFQcm92aWRlcjtcblxudmFyIF9kaW1lbnNpb25IZWxwZXIgPSByZXF1aXJlKFwiLi9oZWxwZXIvZGltZW5zaW9uSGVscGVyXCIpO1xuXG52YXIgc3VtbWFyaXplRGltZW5zaW9ucyA9IF9kaW1lbnNpb25IZWxwZXIuc3VtbWFyaXplRGltZW5zaW9ucztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKiBnbG9iYWwgRmxvYXQ2NEFycmF5LCBJbnQzMkFycmF5LCBVaW50MzJBcnJheSwgVWludDE2QXJyYXkgKi9cblxuLyoqXG4gKiBMaXN0IGZvciBkYXRhIHN0b3JhZ2VcbiAqIEBtb2R1bGUgZWNoYXJ0cy9kYXRhL0xpc3RcbiAqL1xudmFyIGlzT2JqZWN0ID0genJVdGlsLmlzT2JqZWN0O1xudmFyIFVOREVGSU5FRCA9ICd1bmRlZmluZWQnO1xudmFyIElOREVYX05PVF9GT1VORCA9IC0xOyAvLyBVc2UgcHJlZml4IHRvIGF2b2lkIGluZGV4IHRvIGJlIHRoZSBzYW1lIGFzIG90aGVySWRMaXN0W2lkeF0sXG4vLyB3aGljaCB3aWxsIGNhdXNlIHdlaXJkIHVkcGF0ZSBhbmltYXRpb24uXG5cbnZhciBJRF9QUkVGSVggPSAnZVxcMFxcMCc7XG52YXIgZGF0YUN0b3JzID0ge1xuICAnZmxvYXQnOiB0eXBlb2YgRmxvYXQ2NEFycmF5ID09PSBVTkRFRklORUQgPyBBcnJheSA6IEZsb2F0NjRBcnJheSxcbiAgJ2ludCc6IHR5cGVvZiBJbnQzMkFycmF5ID09PSBVTkRFRklORUQgPyBBcnJheSA6IEludDMyQXJyYXksXG4gIC8vIE9yZGluYWwgZGF0YSB0eXBlIGNhbiBiZSBzdHJpbmcgb3IgaW50XG4gICdvcmRpbmFsJzogQXJyYXksXG4gICdudW1iZXInOiBBcnJheSxcbiAgJ3RpbWUnOiBBcnJheVxufTsgLy8gQ2F1dGlvbjogTVVTVCBub3QgdXNlIGBuZXcgQ3RvclVpbnQzMkFycmF5KGFyciwgMCwgbGVuKWAsIGJlY2F1c2UgdGhlIEN0b3Igb2YgYXJyYXkgaXNcbi8vIGRpZmZlcmVudCBmcm9tIHRoZSBDdG9yIG9mIHR5cGVkIGFycmF5LlxuXG52YXIgQ3RvclVpbnQzMkFycmF5ID0gdHlwZW9mIFVpbnQzMkFycmF5ID09PSBVTkRFRklORUQgPyBBcnJheSA6IFVpbnQzMkFycmF5O1xudmFyIEN0b3JJbnQzMkFycmF5ID0gdHlwZW9mIEludDMyQXJyYXkgPT09IFVOREVGSU5FRCA/IEFycmF5IDogSW50MzJBcnJheTtcbnZhciBDdG9yVWludDE2QXJyYXkgPSB0eXBlb2YgVWludDE2QXJyYXkgPT09IFVOREVGSU5FRCA/IEFycmF5IDogVWludDE2QXJyYXk7XG5cbmZ1bmN0aW9uIGdldEluZGljZXNDdG9yKGxpc3QpIHtcbiAgLy8gVGhlIHBvc3NpYmxlIG1heCB2YWx1ZSBpbiB0aGlzLl9pbmRpY2llcyBpcyBhbHdheXMgdGhpcy5fcmF3Q291bnQgZGVzcGl0ZSBvZiBmaWx0ZXJpbmcuXG4gIHJldHVybiBsaXN0Ll9yYXdDb3VudCA+IDY1NTM1ID8gQ3RvclVpbnQzMkFycmF5IDogQ3RvclVpbnQxNkFycmF5O1xufVxuXG5mdW5jdGlvbiBjbG9uZUNodW5rKG9yaWdpbmFsQ2h1bmspIHtcbiAgdmFyIEN0b3IgPSBvcmlnaW5hbENodW5rLmNvbnN0cnVjdG9yOyAvLyBPbmx5IHNoYWxsb3cgY2xvbmUgaXMgZW5vdWdoIHdoZW4gQXJyYXkuXG5cbiAgcmV0dXJuIEN0b3IgPT09IEFycmF5ID8gb3JpZ2luYWxDaHVuay5zbGljZSgpIDogbmV3IEN0b3Iob3JpZ2luYWxDaHVuayk7XG59XG5cbnZhciBUUkFOU0ZFUkFCTEVfUFJPUEVSVElFUyA9IFsnaGFzSXRlbU9wdGlvbicsICdfbmFtZUxpc3QnLCAnX2lkTGlzdCcsICdfaW52ZXJ0ZWRJbmRpY2VzTWFwJywgJ19yYXdEYXRhJywgJ19jaHVua1NpemUnLCAnX2NodW5rQ291bnQnLCAnX2RpbVZhbHVlR2V0dGVyJywgJ19jb3VudCcsICdfcmF3Q291bnQnLCAnX25hbWVEaW1JZHgnLCAnX2lkRGltSWR4J107XG52YXIgQ0xPTkVfUFJPUEVSVElFUyA9IFsnX2V4dGVudCcsICdfYXBwcm94aW1hdGVFeHRlbnQnLCAnX3Jhd0V4dGVudCddO1xuXG5mdW5jdGlvbiB0cmFuc2ZlclByb3BlcnRpZXModGFyZ2V0LCBzb3VyY2UpIHtcbiAgenJVdGlsLmVhY2goVFJBTlNGRVJBQkxFX1BST1BFUlRJRVMuY29uY2F0KHNvdXJjZS5fX3dyYXBwZWRNZXRob2RzIHx8IFtdKSwgZnVuY3Rpb24gKHByb3BOYW1lKSB7XG4gICAgaWYgKHNvdXJjZS5oYXNPd25Qcm9wZXJ0eShwcm9wTmFtZSkpIHtcbiAgICAgIHRhcmdldFtwcm9wTmFtZV0gPSBzb3VyY2VbcHJvcE5hbWVdO1xuICAgIH1cbiAgfSk7XG4gIHRhcmdldC5fX3dyYXBwZWRNZXRob2RzID0gc291cmNlLl9fd3JhcHBlZE1ldGhvZHM7XG4gIHpyVXRpbC5lYWNoKENMT05FX1BST1BFUlRJRVMsIGZ1bmN0aW9uIChwcm9wTmFtZSkge1xuICAgIHRhcmdldFtwcm9wTmFtZV0gPSB6clV0aWwuY2xvbmUoc291cmNlW3Byb3BOYW1lXSk7XG4gIH0pO1xuICB0YXJnZXQuX2NhbGN1bGF0aW9uSW5mbyA9IHpyVXRpbC5leHRlbmQoc291cmNlLl9jYWxjdWxhdGlvbkluZm8pO1xufVxuLyoqXG4gKiBAY29uc3RydWN0b3JcbiAqIEBhbGlhcyBtb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3RcbiAqXG4gKiBAcGFyYW0ge0FycmF5LjxzdHJpbmd8T2JqZWN0Pn0gZGltZW5zaW9uc1xuICogICAgICBGb3IgZXhhbXBsZSwgWydzb21lRGltTmFtZScsIHtuYW1lOiAnc29tZURpbU5hbWUnLCB0eXBlOiAnc29tZURpbVR5cGUnfSwgLi4uXS5cbiAqICAgICAgRGltZW5zaW9ucyBzaG91bGQgYmUgY29uY3JldGUgbmFtZXMgbGlrZSB4LCB5LCB6LCBsbmcsIGxhdCwgYW5nbGUsIHJhZGl1c1xuICogICAgICBTcGV0aWFsIGZpZWxkczoge1xuICogICAgICAgICAgb3JkaW5hbE1ldGE6IDxtb2R1bGU6ZWNoYXJ0cy9kYXRhL09yZGluYWxNZXRhPlxuICogICAgICAgICAgY3JlYXRlSW52ZXJ0ZWRJbmRpY2VzOiA8Ym9vbGVhbj5cbiAqICAgICAgfVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gaG9zdE1vZGVsXG4gKi9cblxuXG52YXIgTGlzdCA9IGZ1bmN0aW9uIChkaW1lbnNpb25zLCBob3N0TW9kZWwpIHtcbiAgZGltZW5zaW9ucyA9IGRpbWVuc2lvbnMgfHwgWyd4JywgJ3knXTtcbiAgdmFyIGRpbWVuc2lvbkluZm9zID0ge307XG4gIHZhciBkaW1lbnNpb25OYW1lcyA9IFtdO1xuICB2YXIgaW52ZXJ0ZWRJbmRpY2VzTWFwID0ge307XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaW1lbnNpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gVXNlIHRoZSBvcmlnaW5hbCBkaW1lbnNpb25zW2ldLCB3aGVyZSBvdGhlciBmbGFnIHByb3BzIG1heSBleGlzdHMuXG4gICAgdmFyIGRpbWVuc2lvbkluZm8gPSBkaW1lbnNpb25zW2ldO1xuXG4gICAgaWYgKHpyVXRpbC5pc1N0cmluZyhkaW1lbnNpb25JbmZvKSkge1xuICAgICAgZGltZW5zaW9uSW5mbyA9IHtcbiAgICAgICAgbmFtZTogZGltZW5zaW9uSW5mb1xuICAgICAgfTtcbiAgICB9XG5cbiAgICB2YXIgZGltZW5zaW9uTmFtZSA9IGRpbWVuc2lvbkluZm8ubmFtZTtcbiAgICBkaW1lbnNpb25JbmZvLnR5cGUgPSBkaW1lbnNpb25JbmZvLnR5cGUgfHwgJ2Zsb2F0JztcblxuICAgIGlmICghZGltZW5zaW9uSW5mby5jb29yZERpbSkge1xuICAgICAgZGltZW5zaW9uSW5mby5jb29yZERpbSA9IGRpbWVuc2lvbk5hbWU7XG4gICAgICBkaW1lbnNpb25JbmZvLmNvb3JkRGltSW5kZXggPSAwO1xuICAgIH1cblxuICAgIGRpbWVuc2lvbkluZm8ub3RoZXJEaW1zID0gZGltZW5zaW9uSW5mby5vdGhlckRpbXMgfHwge307XG4gICAgZGltZW5zaW9uTmFtZXMucHVzaChkaW1lbnNpb25OYW1lKTtcbiAgICBkaW1lbnNpb25JbmZvc1tkaW1lbnNpb25OYW1lXSA9IGRpbWVuc2lvbkluZm87XG4gICAgZGltZW5zaW9uSW5mby5pbmRleCA9IGk7XG5cbiAgICBpZiAoZGltZW5zaW9uSW5mby5jcmVhdGVJbnZlcnRlZEluZGljZXMpIHtcbiAgICAgIGludmVydGVkSW5kaWNlc01hcFtkaW1lbnNpb25OYW1lXSA9IFtdO1xuICAgIH1cbiAgfVxuICAvKipcbiAgICogQHJlYWRPbmx5XG4gICAqIEB0eXBlIHtBcnJheS48c3RyaW5nPn1cbiAgICovXG5cblxuICB0aGlzLmRpbWVuc2lvbnMgPSBkaW1lbnNpb25OYW1lcztcbiAgLyoqXG4gICAqIEluZm9tYXRpb24gb2YgZWFjaCBkYXRhIGRpbWVuc2lvbiwgbGlrZSBkYXRhIHR5cGUuXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuXG4gIHRoaXMuX2RpbWVuc2lvbkluZm9zID0gZGltZW5zaW9uSW5mb3M7XG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9XG4gICAqL1xuXG4gIHRoaXMuaG9zdE1vZGVsID0gaG9zdE1vZGVsO1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfVxuICAgKi9cblxuICB0aGlzLmRhdGFUeXBlO1xuICAvKipcbiAgICogSW5kaWNlcyBzdG9yZXMgdGhlIGluZGljZXMgb2YgZGF0YSBzdWJzZXQgYWZ0ZXIgZmlsdGVyZWQuXG4gICAqIFRoaXMgZGF0YSBzdWJzZXQgd2lsbCBiZSB1c2VkIGluIGNoYXJ0LlxuICAgKiBAdHlwZSB7QXJyYXkuPG51bWJlcj59XG4gICAqIEByZWFkT25seVxuICAgKi9cblxuICB0aGlzLl9pbmRpY2VzID0gbnVsbDtcbiAgdGhpcy5fY291bnQgPSAwO1xuICB0aGlzLl9yYXdDb3VudCA9IDA7XG4gIC8qKlxuICAgKiBEYXRhIHN0b3JhZ2VcbiAgICogQHR5cGUge09iamVjdC48a2V5LCBBcnJheS48VHlwZWRBcnJheXxBcnJheT4+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9zdG9yYWdlID0ge307XG4gIC8qKlxuICAgKiBAdHlwZSB7QXJyYXkuPHN0cmluZz59XG4gICAqL1xuXG4gIHRoaXMuX25hbWVMaXN0ID0gW107XG4gIC8qKlxuICAgKiBAdHlwZSB7QXJyYXkuPHN0cmluZz59XG4gICAqL1xuXG4gIHRoaXMuX2lkTGlzdCA9IFtdO1xuICAvKipcbiAgICogTW9kZWxzIG9mIGRhdGEgb3B0aW9uIGlzIHN0b3JlZCBzcGFyc2UgZm9yIG9wdGltaXppbmcgbWVtb3J5IGNvc3RcbiAgICogQHR5cGUge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX29wdGlvbk1vZGVscyA9IFtdO1xuICAvKipcbiAgICogR2xvYmFsIHZpc3VhbCBwcm9wZXJ0aWVzIGFmdGVyIHZpc3VhbCBjb2RpbmdcbiAgICogQHR5cGUge09iamVjdH1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fdmlzdWFsID0ge307XG4gIC8qKlxuICAgKiBHbG9iZWwgbGF5b3V0IHByb3BlcnRpZXMuXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2xheW91dCA9IHt9O1xuICAvKipcbiAgICogSXRlbSB2aXN1YWwgcHJvcGVydGllcyBhZnRlciB2aXN1YWwgY29kaW5nXG4gICAqIEB0eXBlIHtBcnJheS48T2JqZWN0Pn1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5faXRlbVZpc3VhbHMgPSBbXTtcbiAgLyoqXG4gICAqIEtleTogdmlzdWFsIHR5cGUsIFZhbHVlOiBib29sZWFuXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqIEByZWFkT25seVxuICAgKi9cblxuICB0aGlzLmhhc0l0ZW1WaXN1YWwgPSB7fTtcbiAgLyoqXG4gICAqIEl0ZW0gbGF5b3V0IHByb3BlcnRpZXMgYWZ0ZXIgbGF5b3V0XG4gICAqIEB0eXBlIHtBcnJheS48T2JqZWN0Pn1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5faXRlbUxheW91dHMgPSBbXTtcbiAgLyoqXG4gICAqIEdyYXBoaWMgZWxlbW5lbnRzXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOnpyZW5kZXIvRWxlbWVudD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2dyYXBoaWNFbHMgPSBbXTtcbiAgLyoqXG4gICAqIE1heCBzaXplIG9mIGVhY2ggY2h1bmsuXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2NodW5rU2l6ZSA9IDFlNTtcbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2NodW5rQ291bnQgPSAwO1xuICAvKipcbiAgICogQHR5cGUge0FycmF5LjxBcnJheXxPYmplY3Q+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9yYXdEYXRhO1xuICAvKipcbiAgICogUmF3IGV4dGVudCB3aWxsIG5vdCBiZSBjbG9uZWQsIGJ1dCBvbmx5IHRyYW5zZmVyZWQuXG4gICAqIEl0IHdpbGwgbm90IGJlIGNhbGN1bGF0ZWQgdXRpbCBuZWVkZWQuXG4gICAqIGtleTogZGltLFxuICAgKiB2YWx1ZToge2VuZDogbnVtYmVyLCBleHRlbnQ6IEFycmF5LjxudW1iZXI+fVxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9yYXdFeHRlbnQgPSB7fTtcbiAgLyoqXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2V4dGVudCA9IHt9O1xuICAvKipcbiAgICoga2V5OiBkaW1cbiAgICogdmFsdWU6IGV4dGVudFxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9hcHByb3hpbWF0ZUV4dGVudCA9IHt9O1xuICAvKipcbiAgICogQ2FjaGUgc3VtbWFyeSBpbmZvIGZvciBmYXN0IHZpc2l0LiBTZWUgXCJkaW1lbnNpb25IZWxwZXJcIi5cbiAgICogQHR5cGUge09iamVjdH1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fZGltZW5zaW9uc1N1bW1hcnkgPSBzdW1tYXJpemVEaW1lbnNpb25zKHRoaXMpO1xuICAvKipcbiAgICogQHR5cGUge09iamVjdC48QXJyYXl8VHlwZWRBcnJheT59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2ludmVydGVkSW5kaWNlc01hcCA9IGludmVydGVkSW5kaWNlc01hcDtcbiAgLyoqXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2NhbGN1bGF0aW9uSW5mbyA9IHt9O1xufTtcblxudmFyIGxpc3RQcm90byA9IExpc3QucHJvdG90eXBlO1xubGlzdFByb3RvLnR5cGUgPSAnbGlzdCc7XG4vKipcbiAqIElmIGVhY2ggZGF0YSBpdGVtIGhhcyBpdCdzIG93biBvcHRpb25cbiAqIEB0eXBlIHtib29sZWFufVxuICovXG5cbmxpc3RQcm90by5oYXNJdGVtT3B0aW9uID0gdHJ1ZTtcbi8qKlxuICogR2V0IGRpbWVuc2lvbiBuYW1lXG4gKiBAcGFyYW0ge3N0cmluZ3xudW1iZXJ9IGRpbVxuICogICAgICAgIERpbWVuc2lvbiBjYW4gYmUgY29uY3JldGUgbmFtZXMgbGlrZSB4LCB5LCB6LCBsbmcsIGxhdCwgYW5nbGUsIHJhZGl1c1xuICogICAgICAgIE9yIGEgb3JkaW5hbCBudW1iZXIuIEZvciBleGFtcGxlIGdldERpbWVuc2lvbkluZm8oMCkgd2lsbCByZXR1cm4gJ3gnIG9yICdsbmcnIG9yICdyYWRpdXMnXG4gKiBAcmV0dXJuIHtzdHJpbmd9IENvbmNyZXRlIGRpbSBuYW1lLlxuICovXG5cbmxpc3RQcm90by5nZXREaW1lbnNpb24gPSBmdW5jdGlvbiAoZGltKSB7XG4gIGlmICghaXNOYU4oZGltKSkge1xuICAgIGRpbSA9IHRoaXMuZGltZW5zaW9uc1tkaW1dIHx8IGRpbTtcbiAgfVxuXG4gIHJldHVybiBkaW07XG59O1xuLyoqXG4gKiBHZXQgdHlwZSBhbmQgY2FsY3VsYXRpb24gaW5mbyBvZiBwYXJ0aWN1bGFyIGRpbWVuc2lvblxuICogQHBhcmFtIHtzdHJpbmd8bnVtYmVyfSBkaW1cbiAqICAgICAgICBEaW1lbnNpb24gY2FuIGJlIGNvbmNyZXRlIG5hbWVzIGxpa2UgeCwgeSwgeiwgbG5nLCBsYXQsIGFuZ2xlLCByYWRpdXNcbiAqICAgICAgICBPciBhIG9yZGluYWwgbnVtYmVyLiBGb3IgZXhhbXBsZSBnZXREaW1lbnNpb25JbmZvKDApIHdpbGwgcmV0dXJuICd4JyBvciAnbG5nJyBvciAncmFkaXVzJ1xuICovXG5cblxubGlzdFByb3RvLmdldERpbWVuc2lvbkluZm8gPSBmdW5jdGlvbiAoZGltKSB7XG4gIC8vIERvIG5vdCBjbG9uZSwgYmVjYXVzZSB0aGVyZSBtYXkgYmUgY2F0ZWdvcmllcyBpbiBkaW1JbmZvLlxuICByZXR1cm4gdGhpcy5fZGltZW5zaW9uSW5mb3NbdGhpcy5nZXREaW1lbnNpb24oZGltKV07XG59O1xuLyoqXG4gKiBAcmV0dXJuIHtBcnJheS48c3RyaW5nPn0gY29uY3JldGUgZGltZW5zaW9uIG5hbWUgbGlzdCBvbiBjb29yZC5cbiAqL1xuXG5cbmxpc3RQcm90by5nZXREaW1lbnNpb25zT25Db29yZCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX2RpbWVuc2lvbnNTdW1tYXJ5LmRhdGFEaW1zT25Db29yZC5zbGljZSgpO1xufTtcbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IGNvb3JkRGltXG4gKiBAcGFyYW0ge251bWJlcn0gW2lkeF0gQSBjb29yZERpbSBtYXkgbWFwIHRvIG1vcmUgdGhhbiBvbmUgZGF0YSBkaW0uXG4gKiAgICAgICAgSWYgaWR4IGlzIGB0cnVlYCwgcmV0dXJuIGEgYXJyYXkgb2YgYWxsIG1hcHBlZCBkaW1zLlxuICogICAgICAgIElmIGlkeCBpcyBub3Qgc3BlY2lmaWVkLCByZXR1cm4gdGhlIGZpcnN0IGRpbSBub3QgZXh0cmEuXG4gKiBAcmV0dXJuIHtzdHJpbmd8QXJyYXkuPHN0cmluZz59IGNvbmNyZXRlIGRhdGEgZGltLlxuICogICAgICAgIElmIGlkeCBpcyBudW1iZXIsIGFuZCBub3QgZm91bmQsIHJldHVybiBudWxsL3VuZGVmaW5lZC5cbiAqICAgICAgICBJZiBpZHggaXMgYHRydWVgLCBhbmQgbm90IGZvdW5kLCByZXR1cm4gZW1wdHkgYXJyYXkgKGFsd2F5cyByZXR1cm4gYXJyYXkpLlxuICovXG5cblxubGlzdFByb3RvLm1hcERpbWVuc2lvbiA9IGZ1bmN0aW9uIChjb29yZERpbSwgaWR4KSB7XG4gIHZhciBkaW1lbnNpb25zU3VtbWFyeSA9IHRoaXMuX2RpbWVuc2lvbnNTdW1tYXJ5O1xuXG4gIGlmIChpZHggPT0gbnVsbCkge1xuICAgIHJldHVybiBkaW1lbnNpb25zU3VtbWFyeS5lbmNvZGVGaXJzdERpbU5vdEV4dHJhW2Nvb3JkRGltXTtcbiAgfVxuXG4gIHZhciBkaW1zID0gZGltZW5zaW9uc1N1bW1hcnkuZW5jb2RlW2Nvb3JkRGltXTtcbiAgcmV0dXJuIGlkeCA9PT0gdHJ1ZSAvLyBhbHdheXMgcmV0dXJuIGFycmF5IGlmIGlkeCBpcyBgdHJ1ZWBcbiAgPyAoZGltcyB8fCBbXSkuc2xpY2UoKSA6IGRpbXMgJiYgZGltc1tpZHhdO1xufTtcbi8qKlxuICogSW5pdGlhbGl6ZSBmcm9tIGRhdGFcbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdHxudW1iZXJ8QXJyYXk+fSBkYXRhIHNvdXJjZSBvciBkYXRhIG9yIGRhdGEgcHJvdmlkZXIuXG4gKiBAcGFyYW0ge0FycmF5LjxzdHJpbmc+fSBbbmFtZUxJc3RdIFRoZSBuYW1lIG9mIGEgZGF0dW0gaXMgdXNlZCBvbiBkYXRhIGRpZmYgYW5kXG4gKiAgICAgICAgZGVmdWFsdCBsYWJlbC90b29sdGlwLlxuICogICAgICAgIEEgbmFtZSBjYW4gYmUgc3BlY2lmaWVkIGluIGVuY29kZS5pdGVtTmFtZSxcbiAqICAgICAgICBvciBkYXRhSXRlbS5uYW1lIChvbmx5IGZvciBzZXJpZXMgb3B0aW9uIGRhdGEpLFxuICogICAgICAgIG9yIHByb3ZpZGVkIGluIG5hbWVMaXN0IGZyb20gb3V0c2lkZS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtkaW1WYWx1ZUdldHRlcl0gKGRhdGFJdGVtLCBkaW1OYW1lLCBkYXRhSW5kZXgsIGRpbUluZGV4KSA9PiBudW1iZXJcbiAqL1xuXG5cbmxpc3RQcm90by5pbml0RGF0YSA9IGZ1bmN0aW9uIChkYXRhLCBuYW1lTGlzdCwgZGltVmFsdWVHZXR0ZXIpIHtcbiAgdmFyIG5vdFByb3ZpZGVyID0gU291cmNlLmlzSW5zdGFuY2UoZGF0YSkgfHwgenJVdGlsLmlzQXJyYXlMaWtlKGRhdGEpO1xuXG4gIGlmIChub3RQcm92aWRlcikge1xuICAgIGRhdGEgPSBuZXcgRGVmYXVsdERhdGFQcm92aWRlcihkYXRhLCB0aGlzLmRpbWVuc2lvbnMubGVuZ3RoKTtcbiAgfVxuXG4gIHRoaXMuX3Jhd0RhdGEgPSBkYXRhOyAvLyBDbGVhclxuXG4gIHRoaXMuX3N0b3JhZ2UgPSB7fTtcbiAgdGhpcy5faW5kaWNlcyA9IG51bGw7XG4gIHRoaXMuX25hbWVMaXN0ID0gbmFtZUxpc3QgfHwgW107XG4gIHRoaXMuX2lkTGlzdCA9IFtdO1xuICB0aGlzLl9uYW1lUmVwZWF0Q291bnQgPSB7fTtcblxuICBpZiAoIWRpbVZhbHVlR2V0dGVyKSB7XG4gICAgdGhpcy5oYXNJdGVtT3B0aW9uID0gZmFsc2U7XG4gIH1cbiAgLyoqXG4gICAqIEByZWFkT25seVxuICAgKi9cblxuXG4gIHRoaXMuZGVmYXVsdERpbVZhbHVlR2V0dGVyID0gZGVmYXVsdERpbVZhbHVlR2V0dGVyc1t0aGlzLl9yYXdEYXRhLmdldFNvdXJjZSgpLnNvdXJjZUZvcm1hdF07IC8vIERlZmF1bHQgZGltIHZhbHVlIGdldHRlclxuXG4gIHRoaXMuX2RpbVZhbHVlR2V0dGVyID0gZGltVmFsdWVHZXR0ZXIgPSBkaW1WYWx1ZUdldHRlciB8fCB0aGlzLmRlZmF1bHREaW1WYWx1ZUdldHRlcjtcbiAgdGhpcy5fZGltVmFsdWVHZXR0ZXJBcnJheVJvd3MgPSBkZWZhdWx0RGltVmFsdWVHZXR0ZXJzLmFycmF5Um93czsgLy8gUmVzZXQgcmF3IGV4dGVudC5cblxuICB0aGlzLl9yYXdFeHRlbnQgPSB7fTtcblxuICB0aGlzLl9pbml0RGF0YUZyb21Qcm92aWRlcigwLCBkYXRhLmNvdW50KCkpOyAvLyBJZiBkYXRhIGhhcyBubyBpdGVtIG9wdGlvbi5cblxuXG4gIGlmIChkYXRhLnB1cmUpIHtcbiAgICB0aGlzLmhhc0l0ZW1PcHRpb24gPSBmYWxzZTtcbiAgfVxufTtcblxubGlzdFByb3RvLmdldFByb3ZpZGVyID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fcmF3RGF0YTtcbn07XG4vKipcbiAqIENhdXRpb246IENhbiBiZSBvbmx5IGNhbGxlZCBvbiByYXcgZGF0YSAoYmVmb3JlIGB0aGlzLl9pbmRpY2VzYCBjcmVhdGVkKS5cbiAqL1xuXG5cbmxpc3RQcm90by5hcHBlbmREYXRhID0gZnVuY3Rpb24gKGRhdGEpIHtcbiAgdmFyIHJhd0RhdGEgPSB0aGlzLl9yYXdEYXRhO1xuICB2YXIgc3RhcnQgPSB0aGlzLmNvdW50KCk7XG4gIHJhd0RhdGEuYXBwZW5kRGF0YShkYXRhKTtcbiAgdmFyIGVuZCA9IHJhd0RhdGEuY291bnQoKTtcblxuICBpZiAoIXJhd0RhdGEucGVyc2lzdGVudCkge1xuICAgIGVuZCArPSBzdGFydDtcbiAgfVxuXG4gIHRoaXMuX2luaXREYXRhRnJvbVByb3ZpZGVyKHN0YXJ0LCBlbmQpO1xufTtcbi8qKlxuICogQ2F1dGlvbjogQ2FuIGJlIG9ubHkgY2FsbGVkIG9uIHJhdyBkYXRhIChiZWZvcmUgYHRoaXMuX2luZGljZXNgIGNyZWF0ZWQpLlxuICogVGhpcyBtZXRob2QgZG9lcyBub3QgbW9kaWZ5IGByYXdEYXRhYCAoYGRhdGFQcm92aWRlcmApLCBidXQgb25seVxuICogYWRkIHZhbHVlcyB0byBzdG9yYWdlLlxuICpcbiAqIFRoZSBmaW5hbCBjb3VudCB3aWxsIGJlIGluY3JlYXNlZCBieSBgTWF0aC5tYXgodmFsdWVzLmxlbmd0aCwgbmFtZXMubGVuZ3RoKWAuXG4gKlxuICogQHBhcmFtIHtBcnJheS48QXJyYXkuPCo+Pn0gdmFsdWVzIFRoYXQgaXMgdGhlIFNvdXJjZVR5cGU6ICdhcnJheVJvd3MnLCBsaWtlXG4gKiAgICAgICAgW1xuICogICAgICAgICAgICBbMTIsIDMzLCA0NF0sXG4gKiAgICAgICAgICAgIFtOYU4sIDQzLCAxXSxcbiAqICAgICAgICAgICAgWyctJywgJ2FzZGYnLCAwXVxuICogICAgICAgIF1cbiAqICAgICAgICBFYWNoIGl0ZW0gaXMgZXhhY2x0eSBjb29yZXNwb25kaW5nIHRvIGEgZGltZW5zaW9uLlxuICogQHBhcmFtIHtBcnJheS48c3RyaW5nPn0gW25hbWVzXVxuICovXG5cblxubGlzdFByb3RvLmFwcGVuZFZhbHVlcyA9IGZ1bmN0aW9uICh2YWx1ZXMsIG5hbWVzKSB7XG4gIHZhciBjaHVua1NpemUgPSB0aGlzLl9jaHVua1NpemU7XG4gIHZhciBzdG9yYWdlID0gdGhpcy5fc3RvcmFnZTtcbiAgdmFyIGRpbWVuc2lvbnMgPSB0aGlzLmRpbWVuc2lvbnM7XG4gIHZhciBkaW1MZW4gPSBkaW1lbnNpb25zLmxlbmd0aDtcbiAgdmFyIHJhd0V4dGVudCA9IHRoaXMuX3Jhd0V4dGVudDtcbiAgdmFyIHN0YXJ0ID0gdGhpcy5jb3VudCgpO1xuICB2YXIgZW5kID0gc3RhcnQgKyBNYXRoLm1heCh2YWx1ZXMubGVuZ3RoLCBuYW1lcyA/IG5hbWVzLmxlbmd0aCA6IDApO1xuICB2YXIgb3JpZ2luYWxDaHVua0NvdW50ID0gdGhpcy5fY2h1bmtDb3VudDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGRpbUxlbjsgaSsrKSB7XG4gICAgdmFyIGRpbSA9IGRpbWVuc2lvbnNbaV07XG5cbiAgICBpZiAoIXJhd0V4dGVudFtkaW1dKSB7XG4gICAgICByYXdFeHRlbnRbZGltXSA9IGdldEluaXRpYWxFeHRlbnQoKTtcbiAgICB9XG5cbiAgICBpZiAoIXN0b3JhZ2VbZGltXSkge1xuICAgICAgc3RvcmFnZVtkaW1dID0gW107XG4gICAgfVxuXG4gICAgcHJlcGFyZUNodW5rcyhzdG9yYWdlLCB0aGlzLl9kaW1lbnNpb25JbmZvc1tkaW1dLCBjaHVua1NpemUsIG9yaWdpbmFsQ2h1bmtDb3VudCwgZW5kKTtcbiAgICB0aGlzLl9jaHVua0NvdW50ID0gc3RvcmFnZVtkaW1dLmxlbmd0aDtcbiAgfVxuXG4gIHZhciBlbXB0eURhdGFJdGVtID0gbmV3IEFycmF5KGRpbUxlbik7XG5cbiAgZm9yICh2YXIgaWR4ID0gc3RhcnQ7IGlkeCA8IGVuZDsgaWR4KyspIHtcbiAgICB2YXIgc291cmNlSWR4ID0gaWR4IC0gc3RhcnQ7XG4gICAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKGlkeCAvIGNodW5rU2l6ZSk7XG4gICAgdmFyIGNodW5rT2Zmc2V0ID0gaWR4ICUgY2h1bmtTaXplOyAvLyBTdG9yZSB0aGUgZGF0YSBieSBkaW1lbnNpb25zXG5cbiAgICBmb3IgKHZhciBrID0gMDsgayA8IGRpbUxlbjsgaysrKSB7XG4gICAgICB2YXIgZGltID0gZGltZW5zaW9uc1trXTtcblxuICAgICAgdmFyIHZhbCA9IHRoaXMuX2RpbVZhbHVlR2V0dGVyQXJyYXlSb3dzKHZhbHVlc1tzb3VyY2VJZHhdIHx8IGVtcHR5RGF0YUl0ZW0sIGRpbSwgc291cmNlSWR4LCBrKTtcblxuICAgICAgc3RvcmFnZVtkaW1dW2NodW5rSW5kZXhdW2NodW5rT2Zmc2V0XSA9IHZhbDtcbiAgICAgIHZhciBkaW1SYXdFeHRlbnQgPSByYXdFeHRlbnRbZGltXTtcbiAgICAgIHZhbCA8IGRpbVJhd0V4dGVudFswXSAmJiAoZGltUmF3RXh0ZW50WzBdID0gdmFsKTtcbiAgICAgIHZhbCA+IGRpbVJhd0V4dGVudFsxXSAmJiAoZGltUmF3RXh0ZW50WzFdID0gdmFsKTtcbiAgICB9XG5cbiAgICBpZiAobmFtZXMpIHtcbiAgICAgIHRoaXMuX25hbWVMaXN0W2lkeF0gPSBuYW1lc1tzb3VyY2VJZHhdO1xuICAgIH1cbiAgfVxuXG4gIHRoaXMuX3Jhd0NvdW50ID0gdGhpcy5fY291bnQgPSBlbmQ7IC8vIFJlc2V0IGRhdGEgZXh0ZW50XG5cbiAgdGhpcy5fZXh0ZW50ID0ge307XG4gIHByZXBhcmVJbnZlcnRlZEluZGV4KHRoaXMpO1xufTtcblxubGlzdFByb3RvLl9pbml0RGF0YUZyb21Qcm92aWRlciA9IGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gIC8vIE9wdGltaXplLlxuICBpZiAoc3RhcnQgPj0gZW5kKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGNodW5rU2l6ZSA9IHRoaXMuX2NodW5rU2l6ZTtcbiAgdmFyIHJhd0RhdGEgPSB0aGlzLl9yYXdEYXRhO1xuICB2YXIgc3RvcmFnZSA9IHRoaXMuX3N0b3JhZ2U7XG4gIHZhciBkaW1lbnNpb25zID0gdGhpcy5kaW1lbnNpb25zO1xuICB2YXIgZGltTGVuID0gZGltZW5zaW9ucy5sZW5ndGg7XG4gIHZhciBkaW1lbnNpb25JbmZvTWFwID0gdGhpcy5fZGltZW5zaW9uSW5mb3M7XG4gIHZhciBuYW1lTGlzdCA9IHRoaXMuX25hbWVMaXN0O1xuICB2YXIgaWRMaXN0ID0gdGhpcy5faWRMaXN0O1xuICB2YXIgcmF3RXh0ZW50ID0gdGhpcy5fcmF3RXh0ZW50O1xuICB2YXIgbmFtZVJlcGVhdENvdW50ID0gdGhpcy5fbmFtZVJlcGVhdENvdW50ID0ge307XG4gIHZhciBuYW1lRGltSWR4O1xuICB2YXIgb3JpZ2luYWxDaHVua0NvdW50ID0gdGhpcy5fY2h1bmtDb3VudDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGRpbUxlbjsgaSsrKSB7XG4gICAgdmFyIGRpbSA9IGRpbWVuc2lvbnNbaV07XG5cbiAgICBpZiAoIXJhd0V4dGVudFtkaW1dKSB7XG4gICAgICByYXdFeHRlbnRbZGltXSA9IGdldEluaXRpYWxFeHRlbnQoKTtcbiAgICB9XG5cbiAgICB2YXIgZGltSW5mbyA9IGRpbWVuc2lvbkluZm9NYXBbZGltXTtcblxuICAgIGlmIChkaW1JbmZvLm90aGVyRGltcy5pdGVtTmFtZSA9PT0gMCkge1xuICAgICAgbmFtZURpbUlkeCA9IHRoaXMuX25hbWVEaW1JZHggPSBpO1xuICAgIH1cblxuICAgIGlmIChkaW1JbmZvLm90aGVyRGltcy5pdGVtSWQgPT09IDApIHtcbiAgICAgIHRoaXMuX2lkRGltSWR4ID0gaTtcbiAgICB9XG5cbiAgICBpZiAoIXN0b3JhZ2VbZGltXSkge1xuICAgICAgc3RvcmFnZVtkaW1dID0gW107XG4gICAgfVxuXG4gICAgcHJlcGFyZUNodW5rcyhzdG9yYWdlLCBkaW1JbmZvLCBjaHVua1NpemUsIG9yaWdpbmFsQ2h1bmtDb3VudCwgZW5kKTtcbiAgICB0aGlzLl9jaHVua0NvdW50ID0gc3RvcmFnZVtkaW1dLmxlbmd0aDtcbiAgfVxuXG4gIHZhciBkYXRhSXRlbSA9IG5ldyBBcnJheShkaW1MZW4pO1xuXG4gIGZvciAodmFyIGlkeCA9IHN0YXJ0OyBpZHggPCBlbmQ7IGlkeCsrKSB7XG4gICAgLy8gTk9USUNFOiBUcnkgbm90IHRvIHdyaXRlIHRoaW5ncyBpbnRvIGRhdGFJdGVtXG4gICAgZGF0YUl0ZW0gPSByYXdEYXRhLmdldEl0ZW0oaWR4LCBkYXRhSXRlbSk7IC8vIEVhY2ggZGF0YSBpdGVtIGlzIHZhbHVlXG4gICAgLy8gWzEsIDJdXG4gICAgLy8gMlxuICAgIC8vIEJhciBjaGFydCwgbGluZSBjaGFydCB3aGljaCB1c2VzIGNhdGVnb3J5IGF4aXNcbiAgICAvLyBvbmx5IGdpdmVzIHRoZSAneScgdmFsdWUuICd4JyB2YWx1ZSBpcyB0aGUgaW5kaWNlcyBvZiBjYXRlZ29yeVxuICAgIC8vIFVzZSBhIHRlbXBWYWx1ZSB0byBub3JtYWxpemUgdGhlIHZhbHVlIHRvIGJlIGEgKHgsIHkpIHZhbHVlXG5cbiAgICB2YXIgY2h1bmtJbmRleCA9IE1hdGguZmxvb3IoaWR4IC8gY2h1bmtTaXplKTtcbiAgICB2YXIgY2h1bmtPZmZzZXQgPSBpZHggJSBjaHVua1NpemU7IC8vIFN0b3JlIHRoZSBkYXRhIGJ5IGRpbWVuc2lvbnNcblxuICAgIGZvciAodmFyIGsgPSAwOyBrIDwgZGltTGVuOyBrKyspIHtcbiAgICAgIHZhciBkaW0gPSBkaW1lbnNpb25zW2tdO1xuICAgICAgdmFyIGRpbVN0b3JhZ2UgPSBzdG9yYWdlW2RpbV1bY2h1bmtJbmRleF07IC8vIFBFTkRJTkcgTlVMTCBpcyBlbXB0eSBvciB6ZXJvXG5cbiAgICAgIHZhciB2YWwgPSB0aGlzLl9kaW1WYWx1ZUdldHRlcihkYXRhSXRlbSwgZGltLCBpZHgsIGspO1xuXG4gICAgICBkaW1TdG9yYWdlW2NodW5rT2Zmc2V0XSA9IHZhbDtcbiAgICAgIHZhciBkaW1SYXdFeHRlbnQgPSByYXdFeHRlbnRbZGltXTtcbiAgICAgIHZhbCA8IGRpbVJhd0V4dGVudFswXSAmJiAoZGltUmF3RXh0ZW50WzBdID0gdmFsKTtcbiAgICAgIHZhbCA+IGRpbVJhd0V4dGVudFsxXSAmJiAoZGltUmF3RXh0ZW50WzFdID0gdmFsKTtcbiAgICB9IC8vID8/PyBGSVhNRSBub3QgY2hlY2sgYnkgcHVyZSBidXQgc291cmNlRm9ybWF0P1xuICAgIC8vIFRPRE8gcmVmYWN0b3IgdGhlc2UgbG9naWMuXG5cblxuICAgIGlmICghcmF3RGF0YS5wdXJlKSB7XG4gICAgICB2YXIgbmFtZSA9IG5hbWVMaXN0W2lkeF07XG5cbiAgICAgIGlmIChkYXRhSXRlbSAmJiBuYW1lID09IG51bGwpIHtcbiAgICAgICAgLy8gSWYgZGF0YUl0ZW0gaXMge25hbWU6IC4uLn0sIGl0IGhhcyBoaWdoZXN0IHByaW9yaXR5LlxuICAgICAgICAvLyBUaGF0IGlzIGFwcHJvcHJpYXRlIGZvciBtYW55IGNvbW1vbiBjYXNlcy5cbiAgICAgICAgaWYgKGRhdGFJdGVtLm5hbWUgIT0gbnVsbCkge1xuICAgICAgICAgIC8vIFRoZXJlIGlzIG5vIG90aGVyIHBsYWNlIHRvIHBlcnNpc3RlbnQgZGF0YUl0ZW0ubmFtZSxcbiAgICAgICAgICAvLyBzbyBzYXZlIGl0IHRvIG5hbWVMaXN0LlxuICAgICAgICAgIG5hbWVMaXN0W2lkeF0gPSBuYW1lID0gZGF0YUl0ZW0ubmFtZTtcbiAgICAgICAgfSBlbHNlIGlmIChuYW1lRGltSWR4ICE9IG51bGwpIHtcbiAgICAgICAgICB2YXIgbmFtZURpbSA9IGRpbWVuc2lvbnNbbmFtZURpbUlkeF07XG4gICAgICAgICAgdmFyIG5hbWVEaW1DaHVuayA9IHN0b3JhZ2VbbmFtZURpbV1bY2h1bmtJbmRleF07XG5cbiAgICAgICAgICBpZiAobmFtZURpbUNodW5rKSB7XG4gICAgICAgICAgICBuYW1lID0gbmFtZURpbUNodW5rW2NodW5rT2Zmc2V0XTtcbiAgICAgICAgICAgIHZhciBvcmRpbmFsTWV0YSA9IGRpbWVuc2lvbkluZm9NYXBbbmFtZURpbV0ub3JkaW5hbE1ldGE7XG5cbiAgICAgICAgICAgIGlmIChvcmRpbmFsTWV0YSAmJiBvcmRpbmFsTWV0YS5jYXRlZ29yaWVzLmxlbmd0aCkge1xuICAgICAgICAgICAgICBuYW1lID0gb3JkaW5hbE1ldGEuY2F0ZWdvcmllc1tuYW1lXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gLy8gVHJ5IHVzaW5nIHRoZSBpZCBpbiBvcHRpb25cbiAgICAgIC8vIGlkIG9yIG5hbWUgaXMgdXNlZCBvbiBkeW5hbWljYWwgZGF0YSwgbWFwcGluZyBvbGQgYW5kIG5ldyBpdGVtcy5cblxuXG4gICAgICB2YXIgaWQgPSBkYXRhSXRlbSA9PSBudWxsID8gbnVsbCA6IGRhdGFJdGVtLmlkO1xuXG4gICAgICBpZiAoaWQgPT0gbnVsbCAmJiBuYW1lICE9IG51bGwpIHtcbiAgICAgICAgLy8gVXNlIG5hbWUgYXMgaWQgYW5kIGFkZCBjb3VudGVyIHRvIGF2b2lkIHNhbWUgbmFtZVxuICAgICAgICBuYW1lUmVwZWF0Q291bnRbbmFtZV0gPSBuYW1lUmVwZWF0Q291bnRbbmFtZV0gfHwgMDtcbiAgICAgICAgaWQgPSBuYW1lO1xuXG4gICAgICAgIGlmIChuYW1lUmVwZWF0Q291bnRbbmFtZV0gPiAwKSB7XG4gICAgICAgICAgaWQgKz0gJ19fZWNfXycgKyBuYW1lUmVwZWF0Q291bnRbbmFtZV07XG4gICAgICAgIH1cblxuICAgICAgICBuYW1lUmVwZWF0Q291bnRbbmFtZV0rKztcbiAgICAgIH1cblxuICAgICAgaWQgIT0gbnVsbCAmJiAoaWRMaXN0W2lkeF0gPSBpZCk7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFyYXdEYXRhLnBlcnNpc3RlbnQgJiYgcmF3RGF0YS5jbGVhbikge1xuICAgIC8vIENsZWFuIHVudXNlZCBkYXRhIGlmIGRhdGEgc291cmNlIGlzIHR5cGVkIGFycmF5LlxuICAgIHJhd0RhdGEuY2xlYW4oKTtcbiAgfVxuXG4gIHRoaXMuX3Jhd0NvdW50ID0gdGhpcy5fY291bnQgPSBlbmQ7IC8vIFJlc2V0IGRhdGEgZXh0ZW50XG5cbiAgdGhpcy5fZXh0ZW50ID0ge307XG4gIHByZXBhcmVJbnZlcnRlZEluZGV4KHRoaXMpO1xufTtcblxuZnVuY3Rpb24gcHJlcGFyZUNodW5rcyhzdG9yYWdlLCBkaW1JbmZvLCBjaHVua1NpemUsIGNodW5rQ291bnQsIGVuZCkge1xuICB2YXIgRGF0YUN0b3IgPSBkYXRhQ3RvcnNbZGltSW5mby50eXBlXTtcbiAgdmFyIGxhc3RDaHVua0luZGV4ID0gY2h1bmtDb3VudCAtIDE7XG4gIHZhciBkaW0gPSBkaW1JbmZvLm5hbWU7XG4gIHZhciByZXNpemVDaHVua0FycmF5ID0gc3RvcmFnZVtkaW1dW2xhc3RDaHVua0luZGV4XTtcblxuICBpZiAocmVzaXplQ2h1bmtBcnJheSAmJiByZXNpemVDaHVua0FycmF5Lmxlbmd0aCA8IGNodW5rU2l6ZSkge1xuICAgIHZhciBuZXdTdG9yZSA9IG5ldyBEYXRhQ3RvcihNYXRoLm1pbihlbmQgLSBsYXN0Q2h1bmtJbmRleCAqIGNodW5rU2l6ZSwgY2h1bmtTaXplKSk7IC8vIFRoZSBjb3N0IG9mIHRoZSBjb3B5IGlzIHByb2JhYmx5IGluY29uc2lkZXJhYmxlXG4gICAgLy8gd2l0aGluIHRoZSBpbml0aWFsIGNodW5rU2l6ZS5cblxuICAgIGZvciAodmFyIGogPSAwOyBqIDwgcmVzaXplQ2h1bmtBcnJheS5sZW5ndGg7IGorKykge1xuICAgICAgbmV3U3RvcmVbal0gPSByZXNpemVDaHVua0FycmF5W2pdO1xuICAgIH1cblxuICAgIHN0b3JhZ2VbZGltXVtsYXN0Q2h1bmtJbmRleF0gPSBuZXdTdG9yZTtcbiAgfSAvLyBDcmVhdGUgbmV3IGNodW5rcy5cblxuXG4gIGZvciAodmFyIGsgPSBjaHVua0NvdW50ICogY2h1bmtTaXplOyBrIDwgZW5kOyBrICs9IGNodW5rU2l6ZSkge1xuICAgIHN0b3JhZ2VbZGltXS5wdXNoKG5ldyBEYXRhQ3RvcihNYXRoLm1pbihlbmQgLSBrLCBjaHVua1NpemUpKSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcHJlcGFyZUludmVydGVkSW5kZXgobGlzdCkge1xuICB2YXIgaW52ZXJ0ZWRJbmRpY2VzTWFwID0gbGlzdC5faW52ZXJ0ZWRJbmRpY2VzTWFwO1xuICB6clV0aWwuZWFjaChpbnZlcnRlZEluZGljZXNNYXAsIGZ1bmN0aW9uIChpbnZlcnRlZEluZGljZXMsIGRpbSkge1xuICAgIHZhciBkaW1JbmZvID0gbGlzdC5fZGltZW5zaW9uSW5mb3NbZGltXTsgLy8gQ3VycmVudGx5LCBvbmx5IGRpbWVuc2lvbnMgdGhhdCBoYXMgb3JkaW5hbE1ldGEgY2FuIGNyZWF0ZSBpbnZlcnRlZCBpbmRpY2VzLlxuXG4gICAgdmFyIG9yZGluYWxNZXRhID0gZGltSW5mby5vcmRpbmFsTWV0YTtcblxuICAgIGlmIChvcmRpbmFsTWV0YSkge1xuICAgICAgaW52ZXJ0ZWRJbmRpY2VzID0gaW52ZXJ0ZWRJbmRpY2VzTWFwW2RpbV0gPSBuZXcgQ3RvckludDMyQXJyYXkob3JkaW5hbE1ldGEuY2F0ZWdvcmllcy5sZW5ndGgpOyAvLyBUaGUgZGVmYXVsdCB2YWx1ZSBvZiBUeXBlZEFycmF5IGlzIDAuIFRvIGF2b2lkIG1pc3NcbiAgICAgIC8vIG1hcHBpbmcgdG8gMCwgd2Ugc2hvdWxkIHNldCBpdCBhcyBJTkRFWF9OT1RfRk9VTkQuXG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaW52ZXJ0ZWRJbmRpY2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGludmVydGVkSW5kaWNlc1tpXSA9IElOREVYX05PVF9GT1VORDtcbiAgICAgIH1cblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Ll9jb3VudDsgaSsrKSB7XG4gICAgICAgIC8vIE9ubHkgc3VwcG9ydCB0aGUgY2FzZSB0aGF0IGFsbCB2YWx1ZXMgYXJlIGRpc3RpbmN0LlxuICAgICAgICBpbnZlcnRlZEluZGljZXNbbGlzdC5nZXQoZGltLCBpKV0gPSBpO1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGdldFJhd1ZhbHVlRnJvbVN0b3JlKGxpc3QsIGRpbUluZGV4LCByYXdJbmRleCkge1xuICB2YXIgdmFsO1xuXG4gIGlmIChkaW1JbmRleCAhPSBudWxsKSB7XG4gICAgdmFyIGNodW5rU2l6ZSA9IGxpc3QuX2NodW5rU2l6ZTtcbiAgICB2YXIgY2h1bmtJbmRleCA9IE1hdGguZmxvb3IocmF3SW5kZXggLyBjaHVua1NpemUpO1xuICAgIHZhciBjaHVua09mZnNldCA9IHJhd0luZGV4ICUgY2h1bmtTaXplO1xuICAgIHZhciBkaW0gPSBsaXN0LmRpbWVuc2lvbnNbZGltSW5kZXhdO1xuICAgIHZhciBjaHVuayA9IGxpc3QuX3N0b3JhZ2VbZGltXVtjaHVua0luZGV4XTtcblxuICAgIGlmIChjaHVuaykge1xuICAgICAgdmFsID0gY2h1bmtbY2h1bmtPZmZzZXRdO1xuICAgICAgdmFyIG9yZGluYWxNZXRhID0gbGlzdC5fZGltZW5zaW9uSW5mb3NbZGltXS5vcmRpbmFsTWV0YTtcblxuICAgICAgaWYgKG9yZGluYWxNZXRhICYmIG9yZGluYWxNZXRhLmNhdGVnb3JpZXMubGVuZ3RoKSB7XG4gICAgICAgIHZhbCA9IG9yZGluYWxNZXRhLmNhdGVnb3JpZXNbdmFsXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gdmFsO1xufVxuLyoqXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5saXN0UHJvdG8uY291bnQgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl9jb3VudDtcbn07XG5cbmxpc3RQcm90by5nZXRJbmRpY2VzID0gZnVuY3Rpb24gKCkge1xuICB2YXIgbmV3SW5kaWNlcztcbiAgdmFyIGluZGljZXMgPSB0aGlzLl9pbmRpY2VzO1xuXG4gIGlmIChpbmRpY2VzKSB7XG4gICAgdmFyIEN0b3IgPSBpbmRpY2VzLmNvbnN0cnVjdG9yO1xuICAgIHZhciB0aGlzQ291bnQgPSB0aGlzLl9jb3VudDsgLy8gYG5ldyBBcnJheShhLCBiLCBjKWAgaXMgZGlmZmVyZW50IGZyb20gYG5ldyBVaW50MzJBcnJheShhLCBiLCBjKWAuXG5cbiAgICBpZiAoQ3RvciA9PT0gQXJyYXkpIHtcbiAgICAgIG5ld0luZGljZXMgPSBuZXcgQ3Rvcih0aGlzQ291bnQpO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXNDb3VudDsgaSsrKSB7XG4gICAgICAgIG5ld0luZGljZXNbaV0gPSBpbmRpY2VzW2ldO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBuZXdJbmRpY2VzID0gbmV3IEN0b3IoaW5kaWNlcy5idWZmZXIsIDAsIHRoaXNDb3VudCk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHZhciBDdG9yID0gZ2V0SW5kaWNlc0N0b3IodGhpcyk7XG4gICAgdmFyIG5ld0luZGljZXMgPSBuZXcgQ3Rvcih0aGlzLmNvdW50KCkpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBuZXdJbmRpY2VzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBuZXdJbmRpY2VzW2ldID0gaTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbmV3SW5kaWNlcztcbn07XG4vKipcbiAqIEdldCB2YWx1ZS4gUmV0dXJuIE5hTiBpZiBpZHggaXMgb3V0IG9mIHJhbmdlLlxuICogQHBhcmFtIHtzdHJpbmd9IGRpbSBEaW0gbXVzdCBiZSBjb25jcmV0ZSBuYW1lLlxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHBhcmFtIHtib29sZWFufSBzdGFja1xuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxubGlzdFByb3RvLmdldCA9IGZ1bmN0aW9uIChkaW0sIGlkeFxuLyosIHN0YWNrICovXG4pIHtcbiAgaWYgKCEoaWR4ID49IDAgJiYgaWR4IDwgdGhpcy5fY291bnQpKSB7XG4gICAgcmV0dXJuIE5hTjtcbiAgfVxuXG4gIHZhciBzdG9yYWdlID0gdGhpcy5fc3RvcmFnZTtcblxuICBpZiAoIXN0b3JhZ2VbZGltXSkge1xuICAgIC8vIFRPRE8gV2FybiA/XG4gICAgcmV0dXJuIE5hTjtcbiAgfVxuXG4gIGlkeCA9IHRoaXMuZ2V0UmF3SW5kZXgoaWR4KTtcbiAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKGlkeCAvIHRoaXMuX2NodW5rU2l6ZSk7XG4gIHZhciBjaHVua09mZnNldCA9IGlkeCAlIHRoaXMuX2NodW5rU2l6ZTtcbiAgdmFyIGNodW5rU3RvcmUgPSBzdG9yYWdlW2RpbV1bY2h1bmtJbmRleF07XG4gIHZhciB2YWx1ZSA9IGNodW5rU3RvcmVbY2h1bmtPZmZzZXRdOyAvLyBGSVhNRSBvcmRpbmFsIGRhdGEgdHlwZSBpcyBub3Qgc3RhY2thYmxlXG4gIC8vIGlmIChzdGFjaykge1xuICAvLyAgICAgdmFyIGRpbWVuc2lvbkluZm8gPSB0aGlzLl9kaW1lbnNpb25JbmZvc1tkaW1dO1xuICAvLyAgICAgaWYgKGRpbWVuc2lvbkluZm8gJiYgZGltZW5zaW9uSW5mby5zdGFja2FibGUpIHtcbiAgLy8gICAgICAgICB2YXIgc3RhY2tlZE9uID0gdGhpcy5zdGFja2VkT247XG4gIC8vICAgICAgICAgd2hpbGUgKHN0YWNrZWRPbikge1xuICAvLyAgICAgICAgICAgICAvLyBHZXQgbm8gc3RhY2tlZCBkYXRhIG9mIHN0YWNrZWQgb25cbiAgLy8gICAgICAgICAgICAgdmFyIHN0YWNrZWRWYWx1ZSA9IHN0YWNrZWRPbi5nZXQoZGltLCBpZHgpO1xuICAvLyAgICAgICAgICAgICAvLyBDb25zaWRlcmluZyBwb3NpdGl2ZSBzdGFjaywgbmVnYXRpdmUgc3RhY2sgYW5kIGVtcHR5IGRhdGFcbiAgLy8gICAgICAgICAgICAgaWYgKCh2YWx1ZSA+PSAwICYmIHN0YWNrZWRWYWx1ZSA+IDApICAvLyBQb3NpdGl2ZSBzdGFja1xuICAvLyAgICAgICAgICAgICAgICAgfHwgKHZhbHVlIDw9IDAgJiYgc3RhY2tlZFZhbHVlIDwgMCkgLy8gTmVnYXRpdmUgc3RhY2tcbiAgLy8gICAgICAgICAgICAgKSB7XG4gIC8vICAgICAgICAgICAgICAgICB2YWx1ZSArPSBzdGFja2VkVmFsdWU7XG4gIC8vICAgICAgICAgICAgIH1cbiAgLy8gICAgICAgICAgICAgc3RhY2tlZE9uID0gc3RhY2tlZE9uLnN0YWNrZWRPbjtcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICB9XG4gIC8vIH1cblxuICByZXR1cm4gdmFsdWU7XG59O1xuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gZGltIGNvbmNyZXRlIGRpbVxuICogQHBhcmFtIHtudW1iZXJ9IHJhd0luZGV4XG4gKiBAcmV0dXJuIHtudW1iZXJ8c3RyaW5nfVxuICovXG5cblxubGlzdFByb3RvLmdldEJ5UmF3SW5kZXggPSBmdW5jdGlvbiAoZGltLCByYXdJZHgpIHtcbiAgaWYgKCEocmF3SWR4ID49IDAgJiYgcmF3SWR4IDwgdGhpcy5fcmF3Q291bnQpKSB7XG4gICAgcmV0dXJuIE5hTjtcbiAgfVxuXG4gIHZhciBkaW1TdG9yZSA9IHRoaXMuX3N0b3JhZ2VbZGltXTtcblxuICBpZiAoIWRpbVN0b3JlKSB7XG4gICAgLy8gVE9ETyBXYXJuID9cbiAgICByZXR1cm4gTmFOO1xuICB9XG5cbiAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKHJhd0lkeCAvIHRoaXMuX2NodW5rU2l6ZSk7XG4gIHZhciBjaHVua09mZnNldCA9IHJhd0lkeCAlIHRoaXMuX2NodW5rU2l6ZTtcbiAgdmFyIGNodW5rU3RvcmUgPSBkaW1TdG9yZVtjaHVua0luZGV4XTtcbiAgcmV0dXJuIGNodW5rU3RvcmVbY2h1bmtPZmZzZXRdO1xufTtcbi8qKlxuICogRklYTUUgVXNlIGBnZXRgIG9uIGNocm9tZSBtYXliZSBzbG93KGluIGZpbHRlclNlbGYgYW5kIHNlbGVjdFJhbmdlKS5cbiAqIEhhY2sgYSBtdWNoIHNpbXBsZXIgX2dldEZhc3RcbiAqIEBwcml2YXRlXG4gKi9cblxuXG5saXN0UHJvdG8uX2dldEZhc3QgPSBmdW5jdGlvbiAoZGltLCByYXdJZHgpIHtcbiAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKHJhd0lkeCAvIHRoaXMuX2NodW5rU2l6ZSk7XG4gIHZhciBjaHVua09mZnNldCA9IHJhd0lkeCAlIHRoaXMuX2NodW5rU2l6ZTtcbiAgdmFyIGNodW5rU3RvcmUgPSB0aGlzLl9zdG9yYWdlW2RpbV1bY2h1bmtJbmRleF07XG4gIHJldHVybiBjaHVua1N0b3JlW2NodW5rT2Zmc2V0XTtcbn07XG4vKipcbiAqIEdldCB2YWx1ZSBmb3IgbXVsdGkgZGltZW5zaW9ucy5cbiAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IFtkaW1lbnNpb25zXSBJZiBpZ25vcmVkLCB1c2luZyBhbGwgZGltZW5zaW9ucy5cbiAqIEBwYXJhbSB7bnVtYmVyfSBpZHhcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmxpc3RQcm90by5nZXRWYWx1ZXMgPSBmdW5jdGlvbiAoZGltZW5zaW9ucywgaWR4XG4vKiwgc3RhY2sgKi9cbikge1xuICB2YXIgdmFsdWVzID0gW107XG5cbiAgaWYgKCF6clV0aWwuaXNBcnJheShkaW1lbnNpb25zKSkge1xuICAgIC8vIHN0YWNrID0gaWR4O1xuICAgIGlkeCA9IGRpbWVuc2lvbnM7XG4gICAgZGltZW5zaW9ucyA9IHRoaXMuZGltZW5zaW9ucztcbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwLCBsZW4gPSBkaW1lbnNpb25zLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgdmFsdWVzLnB1c2godGhpcy5nZXQoZGltZW5zaW9uc1tpXSwgaWR4XG4gICAgLyosIHN0YWNrICovXG4gICAgKSk7XG4gIH1cblxuICByZXR1cm4gdmFsdWVzO1xufTtcbi8qKlxuICogSWYgdmFsdWUgaXMgTmFOLiBJbmxjdWRpbmcgJy0nXG4gKiBPbmx5IGNoZWNrIHRoZSBjb29yZCBkaW1lbnNpb25zLlxuICogQHBhcmFtIHtzdHJpbmd9IGRpbVxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxubGlzdFByb3RvLmhhc1ZhbHVlID0gZnVuY3Rpb24gKGlkeCkge1xuICB2YXIgZGF0YURpbXNPbkNvb3JkID0gdGhpcy5fZGltZW5zaW9uc1N1bW1hcnkuZGF0YURpbXNPbkNvb3JkO1xuICB2YXIgZGltZW5zaW9uSW5mb3MgPSB0aGlzLl9kaW1lbnNpb25JbmZvcztcblxuICBmb3IgKHZhciBpID0gMCwgbGVuID0gZGF0YURpbXNPbkNvb3JkLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgaWYgKCAvLyBPcmRpbmFsIHR5cGUgY2FuIGJlIHN0cmluZyBvciBudW1iZXJcbiAgICBkaW1lbnNpb25JbmZvc1tkYXRhRGltc09uQ29vcmRbaV1dLnR5cGUgIT09ICdvcmRpbmFsJyAvLyBGSVhNRSBjaGVjayBvcmRpbmFsIHdoZW4gdXNpbmcgaW5kZXg/XG4gICAgJiYgaXNOYU4odGhpcy5nZXQoZGF0YURpbXNPbkNvb3JkW2ldLCBpZHgpKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufTtcbi8qKlxuICogR2V0IGV4dGVudCBvZiBkYXRhIGluIG9uZSBkaW1lbnNpb25cbiAqIEBwYXJhbSB7c3RyaW5nfSBkaW1cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gc3RhY2tcbiAqL1xuXG5cbmxpc3RQcm90by5nZXREYXRhRXh0ZW50ID0gZnVuY3Rpb24gKGRpbVxuLyosIHN0YWNrICovXG4pIHtcbiAgLy8gTWFrZSBzdXJlIHVzZSBjb25jcmV0ZSBkaW0gYXMgY2FjaGUgbmFtZS5cbiAgZGltID0gdGhpcy5nZXREaW1lbnNpb24oZGltKTtcbiAgdmFyIGRpbURhdGEgPSB0aGlzLl9zdG9yYWdlW2RpbV07XG4gIHZhciBpbml0aWFsRXh0ZW50ID0gZ2V0SW5pdGlhbEV4dGVudCgpOyAvLyBzdGFjayA9ICEhKChzdGFjayB8fCBmYWxzZSkgJiYgdGhpcy5nZXRDYWxjdWxhdGlvbkluZm8oZGltKSk7XG5cbiAgaWYgKCFkaW1EYXRhKSB7XG4gICAgcmV0dXJuIGluaXRpYWxFeHRlbnQ7XG4gIH0gLy8gTWFrZSBtb3JlIHN0cmljdCBjaGVja2luZ3MgdG8gZW5zdXJlIGhpdHRpbmcgY2FjaGUuXG5cblxuICB2YXIgY3VyckVuZCA9IHRoaXMuY291bnQoKTsgLy8gdmFyIGNhY2hlTmFtZSA9IFtkaW0sICEhc3RhY2tdLmpvaW4oJ18nKTtcbiAgLy8gdmFyIGNhY2hlTmFtZSA9IGRpbTtcbiAgLy8gQ29uc2lkZXIgdGhlIG1vc3QgY2FzZXMgd2hlbiB1c2luZyBkYXRhIHpvb20sIGBnZXREYXRhRXh0ZW50YFxuICAvLyBoYXBwZW5lZCBiZWZvcmUgZmlsdGVyaW5nLiBXZSBjYWNoZSByYXcgZXh0ZW50LCB3aGljaCBpcyBub3RcbiAgLy8gbmVjZXNzYXJ5IHRvIGJlIGNsZWFyZWQgYW5kIHJlY2FsY3VsYXRlZCB3aGVuIHJlc3RvcmUgZGF0YS5cblxuICB2YXIgdXNlUmF3ID0gIXRoaXMuX2luZGljZXM7IC8vICYmICFzdGFjaztcblxuICB2YXIgZGltRXh0ZW50O1xuXG4gIGlmICh1c2VSYXcpIHtcbiAgICByZXR1cm4gdGhpcy5fcmF3RXh0ZW50W2RpbV0uc2xpY2UoKTtcbiAgfVxuXG4gIGRpbUV4dGVudCA9IHRoaXMuX2V4dGVudFtkaW1dO1xuXG4gIGlmIChkaW1FeHRlbnQpIHtcbiAgICByZXR1cm4gZGltRXh0ZW50LnNsaWNlKCk7XG4gIH1cblxuICBkaW1FeHRlbnQgPSBpbml0aWFsRXh0ZW50O1xuICB2YXIgbWluID0gZGltRXh0ZW50WzBdO1xuICB2YXIgbWF4ID0gZGltRXh0ZW50WzFdO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY3VyckVuZDsgaSsrKSB7XG4gICAgLy8gdmFyIHZhbHVlID0gc3RhY2sgPyB0aGlzLmdldChkaW0sIGksIHRydWUpIDogdGhpcy5fZ2V0RmFzdChkaW0sIHRoaXMuZ2V0UmF3SW5kZXgoaSkpO1xuICAgIHZhciB2YWx1ZSA9IHRoaXMuX2dldEZhc3QoZGltLCB0aGlzLmdldFJhd0luZGV4KGkpKTtcblxuICAgIHZhbHVlIDwgbWluICYmIChtaW4gPSB2YWx1ZSk7XG4gICAgdmFsdWUgPiBtYXggJiYgKG1heCA9IHZhbHVlKTtcbiAgfVxuXG4gIGRpbUV4dGVudCA9IFttaW4sIG1heF07XG4gIHRoaXMuX2V4dGVudFtkaW1dID0gZGltRXh0ZW50O1xuICByZXR1cm4gZGltRXh0ZW50O1xufTtcbi8qKlxuICogT3B0aW1pemUgZm9yIHRoZSBzY2VuYXJpbyB0aGF0IGRhdGEgaXMgZmlsdGVyZWQgYnkgYSBnaXZlbiBleHRlbnQuXG4gKiBDb25zaWRlciB0aGF0IGlmIGRhdGEgYW1vdW50IGlzIG1vcmUgdGhhbiBodW5kcmVkcyBvZiB0aG91c2FuZCxcbiAqIGV4dGVudCBjYWxjdWxhdGlvbiB3aWxsIGNvc3QgbW9yZSB0aGFuIDEwbXMgYW5kIHRoZSBjYWNoZSB3aWxsXG4gKiBiZSBlcmFzZWQgYmVjYXVzZSBvZiB0aGUgZmlsdGVyaW5nLlxuICovXG5cblxubGlzdFByb3RvLmdldEFwcHJveGltYXRlRXh0ZW50ID0gZnVuY3Rpb24gKGRpbVxuLyosIHN0YWNrICovXG4pIHtcbiAgZGltID0gdGhpcy5nZXREaW1lbnNpb24oZGltKTtcbiAgcmV0dXJuIHRoaXMuX2FwcHJveGltYXRlRXh0ZW50W2RpbV0gfHwgdGhpcy5nZXREYXRhRXh0ZW50KGRpbVxuICAvKiwgc3RhY2sgKi9cbiAgKTtcbn07XG5cbmxpc3RQcm90by5zZXRBcHByb3hpbWF0ZUV4dGVudCA9IGZ1bmN0aW9uIChleHRlbnQsIGRpbVxuLyosIHN0YWNrICovXG4pIHtcbiAgZGltID0gdGhpcy5nZXREaW1lbnNpb24oZGltKTtcbiAgdGhpcy5fYXBwcm94aW1hdGVFeHRlbnRbZGltXSA9IGV4dGVudC5zbGljZSgpO1xufTtcbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICogQHJldHVybiB7Kn1cbiAqL1xuXG5cbmxpc3RQcm90by5nZXRDYWxjdWxhdGlvbkluZm8gPSBmdW5jdGlvbiAoa2V5KSB7XG4gIHJldHVybiB0aGlzLl9jYWxjdWxhdGlvbkluZm9ba2V5XTtcbn07XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfE9iamVjdH0ga2V5IG9yIGstdiBvYmplY3RcbiAqIEBwYXJhbSB7Kn0gW3ZhbHVlXVxuICovXG5cblxubGlzdFByb3RvLnNldENhbGN1bGF0aW9uSW5mbyA9IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gIGlzT2JqZWN0KGtleSkgPyB6clV0aWwuZXh0ZW5kKHRoaXMuX2NhbGN1bGF0aW9uSW5mbywga2V5KSA6IHRoaXMuX2NhbGN1bGF0aW9uSW5mb1trZXldID0gdmFsdWU7XG59O1xuLyoqXG4gKiBHZXQgc3VtIG9mIGRhdGEgaW4gb25lIGRpbWVuc2lvblxuICogQHBhcmFtIHtzdHJpbmd9IGRpbVxuICovXG5cblxubGlzdFByb3RvLmdldFN1bSA9IGZ1bmN0aW9uIChkaW1cbi8qLCBzdGFjayAqL1xuKSB7XG4gIHZhciBkaW1EYXRhID0gdGhpcy5fc3RvcmFnZVtkaW1dO1xuICB2YXIgc3VtID0gMDtcblxuICBpZiAoZGltRGF0YSkge1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSB0aGlzLmNvdW50KCk7IGkgPCBsZW47IGkrKykge1xuICAgICAgdmFyIHZhbHVlID0gdGhpcy5nZXQoZGltLCBpXG4gICAgICAvKiwgc3RhY2sgKi9cbiAgICAgICk7XG5cbiAgICAgIGlmICghaXNOYU4odmFsdWUpKSB7XG4gICAgICAgIHN1bSArPSB2YWx1ZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gc3VtO1xufTtcbi8qKlxuICogR2V0IG1lZGlhbiBvZiBkYXRhIGluIG9uZSBkaW1lbnNpb25cbiAqIEBwYXJhbSB7c3RyaW5nfSBkaW1cbiAqL1xuXG5cbmxpc3RQcm90by5nZXRNZWRpYW4gPSBmdW5jdGlvbiAoZGltXG4vKiwgc3RhY2sgKi9cbikge1xuICB2YXIgZGltRGF0YUFycmF5ID0gW107IC8vIG1hcCBhbGwgZGF0YSBvZiBvbmUgZGltZW5zaW9uXG5cbiAgdGhpcy5lYWNoKGRpbSwgZnVuY3Rpb24gKHZhbCwgaWR4KSB7XG4gICAgaWYgKCFpc05hTih2YWwpKSB7XG4gICAgICBkaW1EYXRhQXJyYXkucHVzaCh2YWwpO1xuICAgIH1cbiAgfSk7IC8vIFRPRE9cbiAgLy8gVXNlIHF1aWNrIHNlbGVjdD9cbiAgLy8gaW1tdXRhYmlsaXR5ICYgc29ydFxuXG4gIHZhciBzb3J0ZWREaW1EYXRhQXJyYXkgPSBbXS5jb25jYXQoZGltRGF0YUFycmF5KS5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgcmV0dXJuIGEgLSBiO1xuICB9KTtcbiAgdmFyIGxlbiA9IHRoaXMuY291bnQoKTsgLy8gY2FsY3VsYXRlIG1lZGlhblxuXG4gIHJldHVybiBsZW4gPT09IDAgPyAwIDogbGVuICUgMiA9PT0gMSA/IHNvcnRlZERpbURhdGFBcnJheVsobGVuIC0gMSkgLyAyXSA6IChzb3J0ZWREaW1EYXRhQXJyYXlbbGVuIC8gMl0gKyBzb3J0ZWREaW1EYXRhQXJyYXlbbGVuIC8gMiAtIDFdKSAvIDI7XG59OyAvLyAvKipcbi8vICAqIFJldHJlaXZlIHRoZSBpbmRleCB3aXRoIGdpdmVuIHZhbHVlXG4vLyAgKiBAcGFyYW0ge3N0cmluZ30gZGltIENvbmNyZXRlIGRpbWVuc2lvbi5cbi8vICAqIEBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxuLy8gICogQHJldHVybiB7bnVtYmVyfVxuLy8gICovXG4vLyBDdXJyZW50bHkgaW5jb3JyZWN0OiBzaG91bGQgcmV0dXJuIGRhdGFJbmRleCBidXQgbm90IHJhd0luZGV4LlxuLy8gRG8gbm90IGZpeCBpdCB1bnRpbCB0aGlzIG1ldGhvZCBpcyB0byBiZSB1c2VkIHNvbWV3aGVyZS5cbi8vIEZJWE1FIFByZWNpc2lvbiBvZiBmbG9hdCB2YWx1ZVxuLy8gbGlzdFByb3RvLmluZGV4T2YgPSBmdW5jdGlvbiAoZGltLCB2YWx1ZSkge1xuLy8gICAgIHZhciBzdG9yYWdlID0gdGhpcy5fc3RvcmFnZTtcbi8vICAgICB2YXIgZGltRGF0YSA9IHN0b3JhZ2VbZGltXTtcbi8vICAgICB2YXIgY2h1bmtTaXplID0gdGhpcy5fY2h1bmtTaXplO1xuLy8gICAgIGlmIChkaW1EYXRhKSB7XG4vLyAgICAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSB0aGlzLmNvdW50KCk7IGkgPCBsZW47IGkrKykge1xuLy8gICAgICAgICAgICAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKGkgLyBjaHVua1NpemUpO1xuLy8gICAgICAgICAgICAgdmFyIGNodW5rT2Zmc2V0ID0gaSAlIGNodW5rU2l6ZTtcbi8vICAgICAgICAgICAgIGlmIChkaW1EYXRhW2NodW5rSW5kZXhdW2NodW5rT2Zmc2V0XSA9PT0gdmFsdWUpIHtcbi8vICAgICAgICAgICAgICAgICByZXR1cm4gaTtcbi8vICAgICAgICAgICAgIH1cbi8vICAgICAgICAgfVxuLy8gICAgIH1cbi8vICAgICByZXR1cm4gLTE7XG4vLyB9O1xuXG4vKipcbiAqIE9ubHkgc3VwcG9ydCB0aGUgZGltZW5zaW9uIHdoaWNoIGludmVydGVkIGluZGV4IGNyZWF0ZWQuXG4gKiBEbyBub3Qgc3VwcG9ydCBvdGhlciBjYXNlcyB1bnRpbCByZXF1aXJlZC5cbiAqIEBwYXJhbSB7c3RyaW5nfSBjb25jcmV0ZSBkaW1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gdmFsdWVcbiAqIEByZXR1cm4ge251bWJlcn0gcmF3SW5kZXhcbiAqL1xuXG5cbmxpc3RQcm90by5yYXdJbmRleE9mID0gZnVuY3Rpb24gKGRpbSwgdmFsdWUpIHtcbiAgdmFyIGludmVydGVkSW5kaWNlcyA9IGRpbSAmJiB0aGlzLl9pbnZlcnRlZEluZGljZXNNYXBbZGltXTtcbiAgdmFyIHJhd0luZGV4ID0gaW52ZXJ0ZWRJbmRpY2VzW3ZhbHVlXTtcblxuICBpZiAocmF3SW5kZXggPT0gbnVsbCB8fCBpc05hTihyYXdJbmRleCkpIHtcbiAgICByZXR1cm4gSU5ERVhfTk9UX0ZPVU5EO1xuICB9XG5cbiAgcmV0dXJuIHJhd0luZGV4O1xufTtcbi8qKlxuICogUmV0cmVpdmUgdGhlIGluZGV4IHdpdGggZ2l2ZW4gbmFtZVxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHBhcmFtIHtudW1iZXJ9IG5hbWVcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmxpc3RQcm90by5pbmRleE9mTmFtZSA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gIGZvciAodmFyIGkgPSAwLCBsZW4gPSB0aGlzLmNvdW50KCk7IGkgPCBsZW47IGkrKykge1xuICAgIGlmICh0aGlzLmdldE5hbWUoaSkgPT09IG5hbWUpIHtcbiAgICAgIHJldHVybiBpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiAtMTtcbn07XG4vKipcbiAqIFJldHJlaXZlIHRoZSBpbmRleCB3aXRoIGdpdmVuIHJhdyBkYXRhIGluZGV4XG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKiBAcGFyYW0ge251bWJlcn0gbmFtZVxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxubGlzdFByb3RvLmluZGV4T2ZSYXdJbmRleCA9IGZ1bmN0aW9uIChyYXdJbmRleCkge1xuICBpZiAoIXRoaXMuX2luZGljZXMpIHtcbiAgICByZXR1cm4gcmF3SW5kZXg7XG4gIH1cblxuICBpZiAocmF3SW5kZXggPj0gdGhpcy5fcmF3Q291bnQgfHwgcmF3SW5kZXggPCAwKSB7XG4gICAgcmV0dXJuIC0xO1xuICB9IC8vIEluZGljZXMgYXJlIGFzY2VuZGluZ1xuXG5cbiAgdmFyIGluZGljZXMgPSB0aGlzLl9pbmRpY2VzOyAvLyBJZiByYXdJbmRleCA9PT0gZGF0YUluZGV4XG5cbiAgdmFyIHJhd0RhdGFJbmRleCA9IGluZGljZXNbcmF3SW5kZXhdO1xuXG4gIGlmIChyYXdEYXRhSW5kZXggIT0gbnVsbCAmJiByYXdEYXRhSW5kZXggPCB0aGlzLl9jb3VudCAmJiByYXdEYXRhSW5kZXggPT09IHJhd0luZGV4KSB7XG4gICAgcmV0dXJuIHJhd0luZGV4O1xuICB9XG5cbiAgdmFyIGxlZnQgPSAwO1xuICB2YXIgcmlnaHQgPSB0aGlzLl9jb3VudCAtIDE7XG5cbiAgd2hpbGUgKGxlZnQgPD0gcmlnaHQpIHtcbiAgICB2YXIgbWlkID0gKGxlZnQgKyByaWdodCkgLyAyIHwgMDtcblxuICAgIGlmIChpbmRpY2VzW21pZF0gPCByYXdJbmRleCkge1xuICAgICAgbGVmdCA9IG1pZCArIDE7XG4gICAgfSBlbHNlIGlmIChpbmRpY2VzW21pZF0gPiByYXdJbmRleCkge1xuICAgICAgcmlnaHQgPSBtaWQgLSAxO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gbWlkO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiAtMTtcbn07XG4vKipcbiAqIFJldHJlaXZlIHRoZSBpbmRleCBvZiBuZWFyZXN0IHZhbHVlXG4gKiBAcGFyYW0ge3N0cmluZ30gZGltXG4gKiBAcGFyYW0ge251bWJlcn0gdmFsdWVcbiAqIEBwYXJhbSB7bnVtYmVyfSBbbWF4RGlzdGFuY2U9SW5maW5pdHldXG4gKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn0gQ29uc2lkZXJlIG11bHRpcGxlIHBvaW50cyBoYXMgdGhlIHNhbWUgdmFsdWUuXG4gKi9cblxuXG5saXN0UHJvdG8uaW5kaWNlc09mTmVhcmVzdCA9IGZ1bmN0aW9uIChkaW0sIHZhbHVlLCBtYXhEaXN0YW5jZSkge1xuICB2YXIgc3RvcmFnZSA9IHRoaXMuX3N0b3JhZ2U7XG4gIHZhciBkaW1EYXRhID0gc3RvcmFnZVtkaW1dO1xuICB2YXIgbmVhcmVzdEluZGljZXMgPSBbXTtcblxuICBpZiAoIWRpbURhdGEpIHtcbiAgICByZXR1cm4gbmVhcmVzdEluZGljZXM7XG4gIH1cblxuICBpZiAobWF4RGlzdGFuY2UgPT0gbnVsbCkge1xuICAgIG1heERpc3RhbmNlID0gSW5maW5pdHk7XG4gIH1cblxuICB2YXIgbWluRGlzdCA9IE51bWJlci5NQVhfVkFMVUU7XG4gIHZhciBtaW5EaWZmID0gLTE7XG5cbiAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHRoaXMuY291bnQoKTsgaSA8IGxlbjsgaSsrKSB7XG4gICAgdmFyIGRpZmYgPSB2YWx1ZSAtIHRoaXMuZ2V0KGRpbSwgaVxuICAgIC8qLCBzdGFjayAqL1xuICAgICk7XG4gICAgdmFyIGRpc3QgPSBNYXRoLmFicyhkaWZmKTtcblxuICAgIGlmIChkaWZmIDw9IG1heERpc3RhbmNlICYmIGRpc3QgPD0gbWluRGlzdCkge1xuICAgICAgLy8gRm9yIHRoZSBjYXNlIG9mIHR3byBkYXRhIGFyZSBzYW1lIG9uIHhBeGlzLCB3aGljaCBoYXMgc2VxdWVuY2UgZGF0YS5cbiAgICAgIC8vIFNob3cgdGhlIG5lYXJlc3QgaW5kZXhcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9lY29tZmUvZWNoYXJ0cy9pc3N1ZXMvMjg2OVxuICAgICAgaWYgKGRpc3QgPCBtaW5EaXN0IHx8IGRpZmYgPj0gMCAmJiBtaW5EaWZmIDwgMCkge1xuICAgICAgICBtaW5EaXN0ID0gZGlzdDtcbiAgICAgICAgbWluRGlmZiA9IGRpZmY7XG4gICAgICAgIG5lYXJlc3RJbmRpY2VzLmxlbmd0aCA9IDA7XG4gICAgICB9XG5cbiAgICAgIG5lYXJlc3RJbmRpY2VzLnB1c2goaSk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIG5lYXJlc3RJbmRpY2VzO1xufTtcbi8qKlxuICogR2V0IHJhdyBkYXRhIGluZGV4XG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5saXN0UHJvdG8uZ2V0UmF3SW5kZXggPSBnZXRSYXdJbmRleFdpdGhvdXRJbmRpY2VzO1xuXG5mdW5jdGlvbiBnZXRSYXdJbmRleFdpdGhvdXRJbmRpY2VzKGlkeCkge1xuICByZXR1cm4gaWR4O1xufVxuXG5mdW5jdGlvbiBnZXRSYXdJbmRleFdpdGhJbmRpY2VzKGlkeCkge1xuICBpZiAoaWR4IDwgdGhpcy5fY291bnQgJiYgaWR4ID49IDApIHtcbiAgICByZXR1cm4gdGhpcy5faW5kaWNlc1tpZHhdO1xuICB9XG5cbiAgcmV0dXJuIC0xO1xufVxuLyoqXG4gKiBHZXQgcmF3IGRhdGEgaXRlbVxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxubGlzdFByb3RvLmdldFJhd0RhdGFJdGVtID0gZnVuY3Rpb24gKGlkeCkge1xuICBpZiAoIXRoaXMuX3Jhd0RhdGEucGVyc2lzdGVudCkge1xuICAgIHZhciB2YWwgPSBbXTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5kaW1lbnNpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGltID0gdGhpcy5kaW1lbnNpb25zW2ldO1xuICAgICAgdmFsLnB1c2godGhpcy5nZXQoZGltLCBpZHgpKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdmFsO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiB0aGlzLl9yYXdEYXRhLmdldEl0ZW0odGhpcy5nZXRSYXdJbmRleChpZHgpKTtcbiAgfVxufTtcbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHBhcmFtIHtib29sZWFufSBbbm90RGVmYXVsdElkeD1mYWxzZV1cbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuXG5cbmxpc3RQcm90by5nZXROYW1lID0gZnVuY3Rpb24gKGlkeCkge1xuICB2YXIgcmF3SW5kZXggPSB0aGlzLmdldFJhd0luZGV4KGlkeCk7XG4gIHJldHVybiB0aGlzLl9uYW1lTGlzdFtyYXdJbmRleF0gfHwgZ2V0UmF3VmFsdWVGcm9tU3RvcmUodGhpcywgdGhpcy5fbmFtZURpbUlkeCwgcmF3SW5kZXgpIHx8ICcnO1xufTtcbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHBhcmFtIHtib29sZWFufSBbbm90RGVmYXVsdElkeD1mYWxzZV1cbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuXG5cbmxpc3RQcm90by5nZXRJZCA9IGZ1bmN0aW9uIChpZHgpIHtcbiAgcmV0dXJuIGdldElkKHRoaXMsIHRoaXMuZ2V0UmF3SW5kZXgoaWR4KSk7XG59O1xuXG5mdW5jdGlvbiBnZXRJZChsaXN0LCByYXdJbmRleCkge1xuICB2YXIgaWQgPSBsaXN0Ll9pZExpc3RbcmF3SW5kZXhdO1xuXG4gIGlmIChpZCA9PSBudWxsKSB7XG4gICAgaWQgPSBnZXRSYXdWYWx1ZUZyb21TdG9yZShsaXN0LCBsaXN0Ll9pZERpbUlkeCwgcmF3SW5kZXgpO1xuICB9XG5cbiAgaWYgKGlkID09IG51bGwpIHtcbiAgICAvLyBGSVhNRSBDaGVjayB0aGUgdXNhZ2UgaW4gZ3JhcGgsIHNob3VsZCBub3QgdXNlIHByZWZpeC5cbiAgICBpZCA9IElEX1BSRUZJWCArIHJhd0luZGV4O1xuICB9XG5cbiAgcmV0dXJuIGlkO1xufVxuXG5mdW5jdGlvbiBub3JtYWxpemVEaW1lbnNpb25zKGRpbWVuc2lvbnMpIHtcbiAgaWYgKCF6clV0aWwuaXNBcnJheShkaW1lbnNpb25zKSkge1xuICAgIGRpbWVuc2lvbnMgPSBbZGltZW5zaW9uc107XG4gIH1cblxuICByZXR1cm4gZGltZW5zaW9ucztcbn1cblxuZnVuY3Rpb24gdmFsaWRhdGVEaW1lbnNpb25zKGxpc3QsIGRpbXMpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgLy8gc3Ryb2FnZSBtYXkgYmUgZW1wdHkgd2hlbiBubyBkYXRhLCBzbyB1c2VcbiAgICAvLyBkaW1lbnNpb25JbmZvcyB0byBjaGVjay5cbiAgICBpZiAoIWxpc3QuX2RpbWVuc2lvbkluZm9zW2RpbXNbaV1dKSB7XG4gICAgICBjb25zb2xlLmVycm9yKCdVbmtvd24gZGltZW5zaW9uICcgKyBkaW1zW2ldKTtcbiAgICB9XG4gIH1cbn1cbi8qKlxuICogRGF0YSBpdGVyYXRpb25cbiAqIEBwYXJhbSB7c3RyaW5nfEFycmF5LjxzdHJpbmc+fVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAqIEBwYXJhbSB7Kn0gW2NvbnRleHQ9dGhpc11cbiAqXG4gKiBAZXhhbXBsZVxuICogIGxpc3QuZWFjaCgneCcsIGZ1bmN0aW9uICh4LCBpZHgpIHt9KTtcbiAqICBsaXN0LmVhY2goWyd4JywgJ3knXSwgZnVuY3Rpb24gKHgsIHksIGlkeCkge30pO1xuICogIGxpc3QuZWFjaChmdW5jdGlvbiAoaWR4KSB7fSlcbiAqL1xuXG5cbmxpc3RQcm90by5lYWNoID0gZnVuY3Rpb24gKGRpbXMsIGNiLCBjb250ZXh0LCBjb250ZXh0Q29tcGF0KSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBpZiAoIXRoaXMuX2NvdW50KSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBkaW1zID09PSAnZnVuY3Rpb24nKSB7XG4gICAgY29udGV4dENvbXBhdCA9IGNvbnRleHQ7XG4gICAgY29udGV4dCA9IGNiO1xuICAgIGNiID0gZGltcztcbiAgICBkaW1zID0gW107XG4gIH0gLy8gY29udGV4dENvbXBhdCBqdXN0IGZvciBjb21wYXQgZWNoYXJ0czNcblxuXG4gIGNvbnRleHQgPSBjb250ZXh0IHx8IGNvbnRleHRDb21wYXQgfHwgdGhpcztcbiAgZGltcyA9IHpyVXRpbC5tYXAobm9ybWFsaXplRGltZW5zaW9ucyhkaW1zKSwgdGhpcy5nZXREaW1lbnNpb24sIHRoaXMpO1xuICB2YXIgZGltU2l6ZSA9IGRpbXMubGVuZ3RoO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5jb3VudCgpOyBpKyspIHtcbiAgICAvLyBTaW1wbGUgb3B0aW1pemF0aW9uXG4gICAgc3dpdGNoIChkaW1TaXplKSB7XG4gICAgICBjYXNlIDA6XG4gICAgICAgIGNiLmNhbGwoY29udGV4dCwgaSk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIDE6XG4gICAgICAgIGNiLmNhbGwoY29udGV4dCwgdGhpcy5nZXQoZGltc1swXSwgaSksIGkpO1xuICAgICAgICBicmVhaztcblxuICAgICAgY2FzZSAyOlxuICAgICAgICBjYi5jYWxsKGNvbnRleHQsIHRoaXMuZ2V0KGRpbXNbMF0sIGkpLCB0aGlzLmdldChkaW1zWzFdLCBpKSwgaSk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICB2YXIgayA9IDA7XG4gICAgICAgIHZhciB2YWx1ZSA9IFtdO1xuXG4gICAgICAgIGZvciAoOyBrIDwgZGltU2l6ZTsgaysrKSB7XG4gICAgICAgICAgdmFsdWVba10gPSB0aGlzLmdldChkaW1zW2tdLCBpKTtcbiAgICAgICAgfSAvLyBJbmRleFxuXG5cbiAgICAgICAgdmFsdWVba10gPSBpO1xuICAgICAgICBjYi5hcHBseShjb250ZXh0LCB2YWx1ZSk7XG4gICAgfVxuICB9XG59O1xuLyoqXG4gKiBEYXRhIGZpbHRlclxuICogQHBhcmFtIHtzdHJpbmd8QXJyYXkuPHN0cmluZz59XG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBbY29udGV4dD10aGlzXVxuICovXG5cblxubGlzdFByb3RvLmZpbHRlclNlbGYgPSBmdW5jdGlvbiAoZGltZW5zaW9ucywgY2IsIGNvbnRleHQsIGNvbnRleHRDb21wYXQpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGlmICghdGhpcy5fY291bnQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAodHlwZW9mIGRpbWVuc2lvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb250ZXh0Q29tcGF0ID0gY29udGV4dDtcbiAgICBjb250ZXh0ID0gY2I7XG4gICAgY2IgPSBkaW1lbnNpb25zO1xuICAgIGRpbWVuc2lvbnMgPSBbXTtcbiAgfSAvLyBjb250ZXh0Q29tcGF0IGp1c3QgZm9yIGNvbXBhdCBlY2hhcnRzM1xuXG5cbiAgY29udGV4dCA9IGNvbnRleHQgfHwgY29udGV4dENvbXBhdCB8fCB0aGlzO1xuICBkaW1lbnNpb25zID0genJVdGlsLm1hcChub3JtYWxpemVEaW1lbnNpb25zKGRpbWVuc2lvbnMpLCB0aGlzLmdldERpbWVuc2lvbiwgdGhpcyk7XG4gIHZhciBjb3VudCA9IHRoaXMuY291bnQoKTtcbiAgdmFyIEN0b3IgPSBnZXRJbmRpY2VzQ3Rvcih0aGlzKTtcbiAgdmFyIG5ld0luZGljZXMgPSBuZXcgQ3Rvcihjb3VudCk7XG4gIHZhciB2YWx1ZSA9IFtdO1xuICB2YXIgZGltU2l6ZSA9IGRpbWVuc2lvbnMubGVuZ3RoO1xuICB2YXIgb2Zmc2V0ID0gMDtcbiAgdmFyIGRpbTAgPSBkaW1lbnNpb25zWzBdO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY291bnQ7IGkrKykge1xuICAgIHZhciBrZWVwO1xuICAgIHZhciByYXdJZHggPSB0aGlzLmdldFJhd0luZGV4KGkpOyAvLyBTaW1wbGUgb3B0aW1pemF0aW9uXG5cbiAgICBpZiAoZGltU2l6ZSA9PT0gMCkge1xuICAgICAga2VlcCA9IGNiLmNhbGwoY29udGV4dCwgaSk7XG4gICAgfSBlbHNlIGlmIChkaW1TaXplID09PSAxKSB7XG4gICAgICB2YXIgdmFsID0gdGhpcy5fZ2V0RmFzdChkaW0wLCByYXdJZHgpO1xuXG4gICAgICBrZWVwID0gY2IuY2FsbChjb250ZXh0LCB2YWwsIGkpO1xuICAgIH0gZWxzZSB7XG4gICAgICBmb3IgKHZhciBrID0gMDsgayA8IGRpbVNpemU7IGsrKykge1xuICAgICAgICB2YWx1ZVtrXSA9IHRoaXMuX2dldEZhc3QoZGltMCwgcmF3SWR4KTtcbiAgICAgIH1cblxuICAgICAgdmFsdWVba10gPSBpO1xuICAgICAga2VlcCA9IGNiLmFwcGx5KGNvbnRleHQsIHZhbHVlKTtcbiAgICB9XG5cbiAgICBpZiAoa2VlcCkge1xuICAgICAgbmV3SW5kaWNlc1tvZmZzZXQrK10gPSByYXdJZHg7XG4gICAgfVxuICB9IC8vIFNldCBpbmRpY2VzIGFmdGVyIGZpbHRlcmVkLlxuXG5cbiAgaWYgKG9mZnNldCA8IGNvdW50KSB7XG4gICAgdGhpcy5faW5kaWNlcyA9IG5ld0luZGljZXM7XG4gIH1cblxuICB0aGlzLl9jb3VudCA9IG9mZnNldDsgLy8gUmVzZXQgZGF0YSBleHRlbnRcblxuICB0aGlzLl9leHRlbnQgPSB7fTtcbiAgdGhpcy5nZXRSYXdJbmRleCA9IHRoaXMuX2luZGljZXMgPyBnZXRSYXdJbmRleFdpdGhJbmRpY2VzIDogZ2V0UmF3SW5kZXhXaXRob3V0SW5kaWNlcztcbiAgcmV0dXJuIHRoaXM7XG59O1xuLyoqXG4gKiBTZWxlY3QgZGF0YSBpbiByYW5nZS4gKEZvciBvcHRpbWl6YXRpb24gb2YgZmlsdGVyKVxuICogKE1hbnVhbGx5IGlubGluZSBjb2RlLCBzdXBwb3J0IDUgbWlsbGlvbiBkYXRhIGZpbHRlcmluZyBpbiBkYXRhIHpvb20uKVxuICovXG5cblxubGlzdFByb3RvLnNlbGVjdFJhbmdlID0gZnVuY3Rpb24gKHJhbmdlKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICBpZiAoIXRoaXMuX2NvdW50KSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGRpbWVuc2lvbnMgPSBbXTtcblxuICBmb3IgKHZhciBkaW0gaW4gcmFuZ2UpIHtcbiAgICBpZiAocmFuZ2UuaGFzT3duUHJvcGVydHkoZGltKSkge1xuICAgICAgZGltZW5zaW9ucy5wdXNoKGRpbSk7XG4gICAgfVxuICB9XG5cbiAgdmFyIGRpbVNpemUgPSBkaW1lbnNpb25zLmxlbmd0aDtcblxuICBpZiAoIWRpbVNpemUpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgb3JpZ2luYWxDb3VudCA9IHRoaXMuY291bnQoKTtcbiAgdmFyIEN0b3IgPSBnZXRJbmRpY2VzQ3Rvcih0aGlzKTtcbiAgdmFyIG5ld0luZGljZXMgPSBuZXcgQ3RvcihvcmlnaW5hbENvdW50KTtcbiAgdmFyIG9mZnNldCA9IDA7XG4gIHZhciBkaW0wID0gZGltZW5zaW9uc1swXTtcbiAgdmFyIG1pbiA9IHJhbmdlW2RpbTBdWzBdO1xuICB2YXIgbWF4ID0gcmFuZ2VbZGltMF1bMV07XG4gIHZhciBxdWlja0ZpbmlzaGVkID0gZmFsc2U7XG5cbiAgaWYgKCF0aGlzLl9pbmRpY2VzKSB7XG4gICAgLy8gRXh0cmVtZSBvcHRpbWl6YXRpb24gZm9yIGNvbW1vbiBjYXNlLiBBYm91dCAyeCBmYXN0ZXIgaW4gY2hyb21lLlxuICAgIHZhciBpZHggPSAwO1xuXG4gICAgaWYgKGRpbVNpemUgPT09IDEpIHtcbiAgICAgIHZhciBkaW1TdG9yYWdlID0gdGhpcy5fc3RvcmFnZVtkaW1lbnNpb25zWzBdXTtcblxuICAgICAgZm9yICh2YXIgayA9IDA7IGsgPCB0aGlzLl9jaHVua0NvdW50OyBrKyspIHtcbiAgICAgICAgdmFyIGNodW5rU3RvcmFnZSA9IGRpbVN0b3JhZ2Vba107XG4gICAgICAgIHZhciBsZW4gPSBNYXRoLm1pbih0aGlzLl9jb3VudCAtIGsgKiB0aGlzLl9jaHVua1NpemUsIHRoaXMuX2NodW5rU2l6ZSk7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgIHZhciB2YWwgPSBjaHVua1N0b3JhZ2VbaV07IC8vIE5hTiB3aWxsIG5vdCBiZSBmaWx0ZXJlZC4gQ29uc2lkZXIgdGhlIGNhc2UsIGluIGxpbmUgY2hhcnQsIGVtcHR5XG4gICAgICAgICAgLy8gdmFsdWUgaW5kaWNhdGVzIHRoZSBsaW5lIHNob3VsZCBiZSBicm9rZW4uIEJ1dCBmb3IgdGhlIGNhc2UgbGlrZVxuICAgICAgICAgIC8vIHNjYXR0ZXIgcGxvdCwgYSBkYXRhIGl0ZW0gd2l0aCBlbXB0eSB2YWx1ZSB3aWxsIG5vdCBiZSByZW5kZXJlZCxcbiAgICAgICAgICAvLyBidXQgdGhlIGF4aXMgZXh0ZW50IG1heSBiZSBlZmZlY3RlZCBpZiBzb21lIG90aGVyIGRpbSBvZiB0aGUgZGF0YVxuICAgICAgICAgIC8vIGl0ZW0gaGFzIHZhbHVlLiBGb3J0dW5hdGVseSBpdCBpcyBub3QgYSBzaWduaWZpY2FudCBuZWdhdGl2ZSBlZmZlY3QuXG5cbiAgICAgICAgICBpZiAodmFsID49IG1pbiAmJiB2YWwgPD0gbWF4IHx8IGlzTmFOKHZhbCkpIHtcbiAgICAgICAgICAgIG5ld0luZGljZXNbb2Zmc2V0KytdID0gaWR4O1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlkeCsrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHF1aWNrRmluaXNoZWQgPSB0cnVlO1xuICAgIH0gZWxzZSBpZiAoZGltU2l6ZSA9PT0gMikge1xuICAgICAgdmFyIGRpbVN0b3JhZ2UgPSB0aGlzLl9zdG9yYWdlW2RpbTBdO1xuICAgICAgdmFyIGRpbVN0b3JhZ2UyID0gdGhpcy5fc3RvcmFnZVtkaW1lbnNpb25zWzFdXTtcbiAgICAgIHZhciBtaW4yID0gcmFuZ2VbZGltZW5zaW9uc1sxXV1bMF07XG4gICAgICB2YXIgbWF4MiA9IHJhbmdlW2RpbWVuc2lvbnNbMV1dWzFdO1xuXG4gICAgICBmb3IgKHZhciBrID0gMDsgayA8IHRoaXMuX2NodW5rQ291bnQ7IGsrKykge1xuICAgICAgICB2YXIgY2h1bmtTdG9yYWdlID0gZGltU3RvcmFnZVtrXTtcbiAgICAgICAgdmFyIGNodW5rU3RvcmFnZTIgPSBkaW1TdG9yYWdlMltrXTtcbiAgICAgICAgdmFyIGxlbiA9IE1hdGgubWluKHRoaXMuX2NvdW50IC0gayAqIHRoaXMuX2NodW5rU2l6ZSwgdGhpcy5fY2h1bmtTaXplKTtcblxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IGNodW5rU3RvcmFnZVtpXTtcbiAgICAgICAgICB2YXIgdmFsMiA9IGNodW5rU3RvcmFnZTJbaV07IC8vIERvIG5vdCBmaWx0ZXIgTmFOLCBzZWUgY29tbWVudCBhYm92ZS5cblxuICAgICAgICAgIGlmICgodmFsID49IG1pbiAmJiB2YWwgPD0gbWF4IHx8IGlzTmFOKHZhbCkpICYmICh2YWwyID49IG1pbjIgJiYgdmFsMiA8PSBtYXgyIHx8IGlzTmFOKHZhbDIpKSkge1xuICAgICAgICAgICAgbmV3SW5kaWNlc1tvZmZzZXQrK10gPSBpZHg7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWR4Kys7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcXVpY2tGaW5pc2hlZCA9IHRydWU7XG4gICAgfVxuICB9XG5cbiAgaWYgKCFxdWlja0ZpbmlzaGVkKSB7XG4gICAgaWYgKGRpbVNpemUgPT09IDEpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgb3JpZ2luYWxDb3VudDsgaSsrKSB7XG4gICAgICAgIHZhciByYXdJbmRleCA9IHRoaXMuZ2V0UmF3SW5kZXgoaSk7XG5cbiAgICAgICAgdmFyIHZhbCA9IHRoaXMuX2dldEZhc3QoZGltMCwgcmF3SW5kZXgpOyAvLyBEbyBub3QgZmlsdGVyIE5hTiwgc2VlIGNvbW1lbnQgYWJvdmUuXG5cblxuICAgICAgICBpZiAodmFsID49IG1pbiAmJiB2YWwgPD0gbWF4IHx8IGlzTmFOKHZhbCkpIHtcbiAgICAgICAgICBuZXdJbmRpY2VzW29mZnNldCsrXSA9IHJhd0luZGV4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgb3JpZ2luYWxDb3VudDsgaSsrKSB7XG4gICAgICAgIHZhciBrZWVwID0gdHJ1ZTtcbiAgICAgICAgdmFyIHJhd0luZGV4ID0gdGhpcy5nZXRSYXdJbmRleChpKTtcblxuICAgICAgICBmb3IgKHZhciBrID0gMDsgayA8IGRpbVNpemU7IGsrKykge1xuICAgICAgICAgIHZhciBkaW1rID0gZGltZW5zaW9uc1trXTtcblxuICAgICAgICAgIHZhciB2YWwgPSB0aGlzLl9nZXRGYXN0KGRpbSwgcmF3SW5kZXgpOyAvLyBEbyBub3QgZmlsdGVyIE5hTiwgc2VlIGNvbW1lbnQgYWJvdmUuXG5cblxuICAgICAgICAgIGlmICh2YWwgPCByYW5nZVtkaW1rXVswXSB8fCB2YWwgPiByYW5nZVtkaW1rXVsxXSkge1xuICAgICAgICAgICAga2VlcCA9IGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChrZWVwKSB7XG4gICAgICAgICAgbmV3SW5kaWNlc1tvZmZzZXQrK10gPSB0aGlzLmdldFJhd0luZGV4KGkpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9IC8vIFNldCBpbmRpY2VzIGFmdGVyIGZpbHRlcmVkLlxuXG5cbiAgaWYgKG9mZnNldCA8IG9yaWdpbmFsQ291bnQpIHtcbiAgICB0aGlzLl9pbmRpY2VzID0gbmV3SW5kaWNlcztcbiAgfVxuXG4gIHRoaXMuX2NvdW50ID0gb2Zmc2V0OyAvLyBSZXNldCBkYXRhIGV4dGVudFxuXG4gIHRoaXMuX2V4dGVudCA9IHt9O1xuICB0aGlzLmdldFJhd0luZGV4ID0gdGhpcy5faW5kaWNlcyA/IGdldFJhd0luZGV4V2l0aEluZGljZXMgOiBnZXRSYXdJbmRleFdpdGhvdXRJbmRpY2VzO1xuICByZXR1cm4gdGhpcztcbn07XG4vKipcbiAqIERhdGEgbWFwcGluZyB0byBhIHBsYWluIGFycmF5XG4gKiBAcGFyYW0ge3N0cmluZ3xBcnJheS48c3RyaW5nPn0gW2RpbWVuc2lvbnNdXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBbY29udGV4dD10aGlzXVxuICogQHJldHVybiB7QXJyYXl9XG4gKi9cblxuXG5saXN0UHJvdG8ubWFwQXJyYXkgPSBmdW5jdGlvbiAoZGltZW5zaW9ucywgY2IsIGNvbnRleHQsIGNvbnRleHRDb21wYXQpIHtcbiAgJ3VzZSBzdHJpY3QnO1xuXG4gIGlmICh0eXBlb2YgZGltZW5zaW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGNvbnRleHRDb21wYXQgPSBjb250ZXh0O1xuICAgIGNvbnRleHQgPSBjYjtcbiAgICBjYiA9IGRpbWVuc2lvbnM7XG4gICAgZGltZW5zaW9ucyA9IFtdO1xuICB9IC8vIGNvbnRleHRDb21wYXQganVzdCBmb3IgY29tcGF0IGVjaGFydHMzXG5cblxuICBjb250ZXh0ID0gY29udGV4dCB8fCBjb250ZXh0Q29tcGF0IHx8IHRoaXM7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgdGhpcy5lYWNoKGRpbWVuc2lvbnMsIGZ1bmN0aW9uICgpIHtcbiAgICByZXN1bHQucHVzaChjYiAmJiBjYi5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfSwgY29udGV4dCk7XG4gIHJldHVybiByZXN1bHQ7XG59OyAvLyBEYXRhIGluIGV4Y2x1ZGVEaW1lbnNpb25zIGlzIGNvcGllZCwgb3RoZXJ3aXNlIHRyYW5zZmVyZWQuXG5cblxuZnVuY3Rpb24gY2xvbmVMaXN0Rm9yTWFwQW5kU2FtcGxlKG9yaWdpbmFsLCBleGNsdWRlRGltZW5zaW9ucykge1xuICB2YXIgYWxsRGltZW5zaW9ucyA9IG9yaWdpbmFsLmRpbWVuc2lvbnM7XG4gIHZhciBsaXN0ID0gbmV3IExpc3QoenJVdGlsLm1hcChhbGxEaW1lbnNpb25zLCBvcmlnaW5hbC5nZXREaW1lbnNpb25JbmZvLCBvcmlnaW5hbCksIG9yaWdpbmFsLmhvc3RNb2RlbCk7IC8vIEZJWE1FIElmIG5lZWRzIHN0YWNrZWRPbiwgdmFsdWUgbWF5IGFscmVhZHkgYmVlbiBzdGFja2VkXG5cbiAgdHJhbnNmZXJQcm9wZXJ0aWVzKGxpc3QsIG9yaWdpbmFsKTtcbiAgdmFyIHN0b3JhZ2UgPSBsaXN0Ll9zdG9yYWdlID0ge307XG4gIHZhciBvcmlnaW5hbFN0b3JhZ2UgPSBvcmlnaW5hbC5fc3RvcmFnZTsgLy8gSW5pdCBzdG9yYWdlXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBhbGxEaW1lbnNpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGRpbSA9IGFsbERpbWVuc2lvbnNbaV07XG5cbiAgICBpZiAob3JpZ2luYWxTdG9yYWdlW2RpbV0pIHtcbiAgICAgIC8vIE5vdGljZSB0aGF0IHdlIGRvIG5vdCByZXNldCBpbnZlcnRlZEluZGljZXNNYXAgaGVyZSwgYmVjdWFzZVxuICAgICAgLy8gdGhlcmUgaXMgbm8gc2NlbmFyaW8gb2YgbWFwcGluZyBvciBzYW1wbGluZyBvcmRpbmFsIGRpbWVuc2lvbi5cbiAgICAgIGlmICh6clV0aWwuaW5kZXhPZihleGNsdWRlRGltZW5zaW9ucywgZGltKSA+PSAwKSB7XG4gICAgICAgIHN0b3JhZ2VbZGltXSA9IGNsb25lRGltU3RvcmUob3JpZ2luYWxTdG9yYWdlW2RpbV0pO1xuICAgICAgICBsaXN0Ll9yYXdFeHRlbnRbZGltXSA9IGdldEluaXRpYWxFeHRlbnQoKTtcbiAgICAgICAgbGlzdC5fZXh0ZW50W2RpbV0gPSBudWxsO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gRGlyZWN0IHJlZmVyZW5jZSBmb3Igb3RoZXIgZGltZW5zaW9uc1xuICAgICAgICBzdG9yYWdlW2RpbV0gPSBvcmlnaW5hbFN0b3JhZ2VbZGltXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gbGlzdDtcbn1cblxuZnVuY3Rpb24gY2xvbmVEaW1TdG9yZShvcmlnaW5hbERpbVN0b3JlKSB7XG4gIHZhciBuZXdEaW1TdG9yZSA9IG5ldyBBcnJheShvcmlnaW5hbERpbVN0b3JlLmxlbmd0aCk7XG5cbiAgZm9yICh2YXIgaiA9IDA7IGogPCBvcmlnaW5hbERpbVN0b3JlLmxlbmd0aDsgaisrKSB7XG4gICAgbmV3RGltU3RvcmVbal0gPSBjbG9uZUNodW5rKG9yaWdpbmFsRGltU3RvcmVbal0pO1xuICB9XG5cbiAgcmV0dXJuIG5ld0RpbVN0b3JlO1xufVxuXG5mdW5jdGlvbiBnZXRJbml0aWFsRXh0ZW50KCkge1xuICByZXR1cm4gW0luZmluaXR5LCAtSW5maW5pdHldO1xufVxuLyoqXG4gKiBEYXRhIG1hcHBpbmcgdG8gYSBuZXcgTGlzdCB3aXRoIGdpdmVuIGRpbWVuc2lvbnNcbiAqIEBwYXJhbSB7c3RyaW5nfEFycmF5LjxzdHJpbmc+fSBkaW1lbnNpb25zXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBbY29udGV4dD10aGlzXVxuICogQHJldHVybiB7QXJyYXl9XG4gKi9cblxuXG5saXN0UHJvdG8ubWFwID0gZnVuY3Rpb24gKGRpbWVuc2lvbnMsIGNiLCBjb250ZXh0LCBjb250ZXh0Q29tcGF0KSB7XG4gICd1c2Ugc3RyaWN0JzsgLy8gY29udGV4dENvbXBhdCBqdXN0IGZvciBjb21wYXQgZWNoYXJ0czNcblxuICBjb250ZXh0ID0gY29udGV4dCB8fCBjb250ZXh0Q29tcGF0IHx8IHRoaXM7XG4gIGRpbWVuc2lvbnMgPSB6clV0aWwubWFwKG5vcm1hbGl6ZURpbWVuc2lvbnMoZGltZW5zaW9ucyksIHRoaXMuZ2V0RGltZW5zaW9uLCB0aGlzKTtcbiAgdmFyIGxpc3QgPSBjbG9uZUxpc3RGb3JNYXBBbmRTYW1wbGUodGhpcywgZGltZW5zaW9ucyk7IC8vIEZvbGxvd2luZyBwcm9wZXJ0aWVzIGFyZSBhbGwgaW1tdXRhYmxlLlxuICAvLyBTbyB3ZSBjYW4gcmVmZXJlbmNlIHRvIHRoZSBzYW1lIHZhbHVlXG5cbiAgbGlzdC5faW5kaWNlcyA9IHRoaXMuX2luZGljZXM7XG4gIGxpc3QuZ2V0UmF3SW5kZXggPSBsaXN0Ll9pbmRpY2VzID8gZ2V0UmF3SW5kZXhXaXRoSW5kaWNlcyA6IGdldFJhd0luZGV4V2l0aG91dEluZGljZXM7XG4gIHZhciBzdG9yYWdlID0gbGlzdC5fc3RvcmFnZTtcbiAgdmFyIHRtcFJldFZhbHVlID0gW107XG4gIHZhciBjaHVua1NpemUgPSB0aGlzLl9jaHVua1NpemU7XG4gIHZhciBkaW1TaXplID0gZGltZW5zaW9ucy5sZW5ndGg7XG4gIHZhciBkYXRhQ291bnQgPSB0aGlzLmNvdW50KCk7XG4gIHZhciB2YWx1ZXMgPSBbXTtcbiAgdmFyIHJhd0V4dGVudCA9IGxpc3QuX3Jhd0V4dGVudDtcblxuICBmb3IgKHZhciBkYXRhSW5kZXggPSAwOyBkYXRhSW5kZXggPCBkYXRhQ291bnQ7IGRhdGFJbmRleCsrKSB7XG4gICAgZm9yICh2YXIgZGltSW5kZXggPSAwOyBkaW1JbmRleCA8IGRpbVNpemU7IGRpbUluZGV4KyspIHtcbiAgICAgIHZhbHVlc1tkaW1JbmRleF0gPSB0aGlzLmdldChkaW1lbnNpb25zW2RpbUluZGV4XSwgZGF0YUluZGV4XG4gICAgICAvKiwgc3RhY2sgKi9cbiAgICAgICk7XG4gICAgfVxuXG4gICAgdmFsdWVzW2RpbVNpemVdID0gZGF0YUluZGV4O1xuICAgIHZhciByZXRWYWx1ZSA9IGNiICYmIGNiLmFwcGx5KGNvbnRleHQsIHZhbHVlcyk7XG5cbiAgICBpZiAocmV0VmFsdWUgIT0gbnVsbCkge1xuICAgICAgLy8gYSBudW1iZXIgb3Igc3RyaW5nIChpbiBvcmlkaW5hbCBkaW1lbnNpb24pP1xuICAgICAgaWYgKHR5cGVvZiByZXRWYWx1ZSAhPT0gJ29iamVjdCcpIHtcbiAgICAgICAgdG1wUmV0VmFsdWVbMF0gPSByZXRWYWx1ZTtcbiAgICAgICAgcmV0VmFsdWUgPSB0bXBSZXRWYWx1ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHJhd0luZGV4ID0gdGhpcy5nZXRSYXdJbmRleChkYXRhSW5kZXgpO1xuICAgICAgdmFyIGNodW5rSW5kZXggPSBNYXRoLmZsb29yKHJhd0luZGV4IC8gY2h1bmtTaXplKTtcbiAgICAgIHZhciBjaHVua09mZnNldCA9IHJhd0luZGV4ICUgY2h1bmtTaXplO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHJldFZhbHVlLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBkaW0gPSBkaW1lbnNpb25zW2ldO1xuICAgICAgICB2YXIgdmFsID0gcmV0VmFsdWVbaV07XG4gICAgICAgIHZhciByYXdFeHRlbnRPbkRpbSA9IHJhd0V4dGVudFtkaW1dO1xuICAgICAgICB2YXIgZGltU3RvcmUgPSBzdG9yYWdlW2RpbV07XG5cbiAgICAgICAgaWYgKGRpbVN0b3JlKSB7XG4gICAgICAgICAgZGltU3RvcmVbY2h1bmtJbmRleF1bY2h1bmtPZmZzZXRdID0gdmFsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHZhbCA8IHJhd0V4dGVudE9uRGltWzBdKSB7XG4gICAgICAgICAgcmF3RXh0ZW50T25EaW1bMF0gPSB2YWw7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodmFsID4gcmF3RXh0ZW50T25EaW1bMV0pIHtcbiAgICAgICAgICByYXdFeHRlbnRPbkRpbVsxXSA9IHZhbDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBsaXN0O1xufTtcbi8qKlxuICogTGFyZ2UgZGF0YSBkb3duIHNhbXBsaW5nIG9uIGdpdmVuIGRpbWVuc2lvblxuICogQHBhcmFtIHtzdHJpbmd9IGRpbWVuc2lvblxuICogQHBhcmFtIHtudW1iZXJ9IHJhdGVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHNhbXBsZVZhbHVlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBzYW1wbGVJbmRleCBTYW1wbGUgaW5kZXggZm9yIG5hbWUgYW5kIGlkXG4gKi9cblxuXG5saXN0UHJvdG8uZG93blNhbXBsZSA9IGZ1bmN0aW9uIChkaW1lbnNpb24sIHJhdGUsIHNhbXBsZVZhbHVlLCBzYW1wbGVJbmRleCkge1xuICB2YXIgbGlzdCA9IGNsb25lTGlzdEZvck1hcEFuZFNhbXBsZSh0aGlzLCBbZGltZW5zaW9uXSk7XG4gIHZhciB0YXJnZXRTdG9yYWdlID0gbGlzdC5fc3RvcmFnZTtcbiAgdmFyIGZyYW1lVmFsdWVzID0gW107XG4gIHZhciBmcmFtZVNpemUgPSBNYXRoLmZsb29yKDEgLyByYXRlKTtcbiAgdmFyIGRpbVN0b3JlID0gdGFyZ2V0U3RvcmFnZVtkaW1lbnNpb25dO1xuICB2YXIgbGVuID0gdGhpcy5jb3VudCgpO1xuICB2YXIgY2h1bmtTaXplID0gdGhpcy5fY2h1bmtTaXplO1xuICB2YXIgcmF3RXh0ZW50T25EaW0gPSBsaXN0Ll9yYXdFeHRlbnRbZGltZW5zaW9uXTtcbiAgdmFyIG5ld0luZGljZXMgPSBuZXcgKGdldEluZGljZXNDdG9yKHRoaXMpKShsZW4pO1xuICB2YXIgb2Zmc2V0ID0gMDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSArPSBmcmFtZVNpemUpIHtcbiAgICAvLyBMYXN0IGZyYW1lXG4gICAgaWYgKGZyYW1lU2l6ZSA+IGxlbiAtIGkpIHtcbiAgICAgIGZyYW1lU2l6ZSA9IGxlbiAtIGk7XG4gICAgICBmcmFtZVZhbHVlcy5sZW5ndGggPSBmcmFtZVNpemU7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgayA9IDA7IGsgPCBmcmFtZVNpemU7IGsrKykge1xuICAgICAgdmFyIGRhdGFJZHggPSB0aGlzLmdldFJhd0luZGV4KGkgKyBrKTtcbiAgICAgIHZhciBvcmlnaW5hbENodW5rSW5kZXggPSBNYXRoLmZsb29yKGRhdGFJZHggLyBjaHVua1NpemUpO1xuICAgICAgdmFyIG9yaWdpbmFsQ2h1bmtPZmZzZXQgPSBkYXRhSWR4ICUgY2h1bmtTaXplO1xuICAgICAgZnJhbWVWYWx1ZXNba10gPSBkaW1TdG9yZVtvcmlnaW5hbENodW5rSW5kZXhdW29yaWdpbmFsQ2h1bmtPZmZzZXRdO1xuICAgIH1cblxuICAgIHZhciB2YWx1ZSA9IHNhbXBsZVZhbHVlKGZyYW1lVmFsdWVzKTtcbiAgICB2YXIgc2FtcGxlRnJhbWVJZHggPSB0aGlzLmdldFJhd0luZGV4KE1hdGgubWluKGkgKyBzYW1wbGVJbmRleChmcmFtZVZhbHVlcywgdmFsdWUpIHx8IDAsIGxlbiAtIDEpKTtcbiAgICB2YXIgc2FtcGxlQ2h1bmtJbmRleCA9IE1hdGguZmxvb3Ioc2FtcGxlRnJhbWVJZHggLyBjaHVua1NpemUpO1xuICAgIHZhciBzYW1wbGVDaHVua09mZnNldCA9IHNhbXBsZUZyYW1lSWR4ICUgY2h1bmtTaXplOyAvLyBPbmx5IHdyaXRlIHZhbHVlIG9uIHRoZSBmaWx0ZXJlZCBkYXRhXG5cbiAgICBkaW1TdG9yZVtzYW1wbGVDaHVua0luZGV4XVtzYW1wbGVDaHVua09mZnNldF0gPSB2YWx1ZTtcblxuICAgIGlmICh2YWx1ZSA8IHJhd0V4dGVudE9uRGltWzBdKSB7XG4gICAgICByYXdFeHRlbnRPbkRpbVswXSA9IHZhbHVlO1xuICAgIH1cblxuICAgIGlmICh2YWx1ZSA+IHJhd0V4dGVudE9uRGltWzFdKSB7XG4gICAgICByYXdFeHRlbnRPbkRpbVsxXSA9IHZhbHVlO1xuICAgIH1cblxuICAgIG5ld0luZGljZXNbb2Zmc2V0KytdID0gc2FtcGxlRnJhbWVJZHg7XG4gIH1cblxuICBsaXN0Ll9jb3VudCA9IG9mZnNldDtcbiAgbGlzdC5faW5kaWNlcyA9IG5ld0luZGljZXM7XG4gIGxpc3QuZ2V0UmF3SW5kZXggPSBnZXRSYXdJbmRleFdpdGhJbmRpY2VzO1xuICByZXR1cm4gbGlzdDtcbn07XG4vKipcbiAqIEdldCBtb2RlbCBvZiBvbmUgZGF0YSBpdGVtLlxuICpcbiAqIEBwYXJhbSB7bnVtYmVyfSBpZHhcbiAqL1xuLy8gRklYTUUgTW9kZWwgcHJveHkgP1xuXG5cbmxpc3RQcm90by5nZXRJdGVtTW9kZWwgPSBmdW5jdGlvbiAoaWR4KSB7XG4gIHZhciBob3N0TW9kZWwgPSB0aGlzLmhvc3RNb2RlbDtcbiAgcmV0dXJuIG5ldyBNb2RlbCh0aGlzLmdldFJhd0RhdGFJdGVtKGlkeCksIGhvc3RNb2RlbCwgaG9zdE1vZGVsICYmIGhvc3RNb2RlbC5lY01vZGVsKTtcbn07XG4vKipcbiAqIENyZWF0ZSBhIGRhdGEgZGlmZmVyXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gb3RoZXJMaXN0XG4gKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0RhdGFEaWZmZXJ9XG4gKi9cblxuXG5saXN0UHJvdG8uZGlmZiA9IGZ1bmN0aW9uIChvdGhlckxpc3QpIHtcbiAgdmFyIHRoaXNMaXN0ID0gdGhpcztcbiAgcmV0dXJuIG5ldyBEYXRhRGlmZmVyKG90aGVyTGlzdCA/IG90aGVyTGlzdC5nZXRJbmRpY2VzKCkgOiBbXSwgdGhpcy5nZXRJbmRpY2VzKCksIGZ1bmN0aW9uIChpZHgpIHtcbiAgICByZXR1cm4gZ2V0SWQob3RoZXJMaXN0LCBpZHgpO1xuICB9LCBmdW5jdGlvbiAoaWR4KSB7XG4gICAgcmV0dXJuIGdldElkKHRoaXNMaXN0LCBpZHgpO1xuICB9KTtcbn07XG4vKipcbiAqIEdldCB2aXN1YWwgcHJvcGVydHkuXG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gKi9cblxuXG5saXN0UHJvdG8uZ2V0VmlzdWFsID0gZnVuY3Rpb24gKGtleSkge1xuICB2YXIgdmlzdWFsID0gdGhpcy5fdmlzdWFsO1xuICByZXR1cm4gdmlzdWFsICYmIHZpc3VhbFtrZXldO1xufTtcbi8qKlxuICogU2V0IHZpc3VhbCBwcm9wZXJ0eVxuICogQHBhcmFtIHtzdHJpbmd8T2JqZWN0fSBrZXlcbiAqIEBwYXJhbSB7Kn0gW3ZhbHVlXVxuICpcbiAqIEBleGFtcGxlXG4gKiAgc2V0VmlzdWFsKCdjb2xvcicsIGNvbG9yKTtcbiAqICBzZXRWaXN1YWwoe1xuICogICAgICAnY29sb3InOiBjb2xvclxuICogIH0pO1xuICovXG5cblxubGlzdFByb3RvLnNldFZpc3VhbCA9IGZ1bmN0aW9uIChrZXksIHZhbCkge1xuICBpZiAoaXNPYmplY3Qoa2V5KSkge1xuICAgIGZvciAodmFyIG5hbWUgaW4ga2V5KSB7XG4gICAgICBpZiAoa2V5Lmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgIHRoaXMuc2V0VmlzdWFsKG5hbWUsIGtleVtuYW1lXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdGhpcy5fdmlzdWFsID0gdGhpcy5fdmlzdWFsIHx8IHt9O1xuICB0aGlzLl92aXN1YWxba2V5XSA9IHZhbDtcbn07XG4vKipcbiAqIFNldCBsYXlvdXQgcHJvcGVydHkuXG4gKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IGtleVxuICogQHBhcmFtIHsqfSBbdmFsXVxuICovXG5cblxubGlzdFByb3RvLnNldExheW91dCA9IGZ1bmN0aW9uIChrZXksIHZhbCkge1xuICBpZiAoaXNPYmplY3Qoa2V5KSkge1xuICAgIGZvciAodmFyIG5hbWUgaW4ga2V5KSB7XG4gICAgICBpZiAoa2V5Lmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgIHRoaXMuc2V0TGF5b3V0KG5hbWUsIGtleVtuYW1lXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdGhpcy5fbGF5b3V0W2tleV0gPSB2YWw7XG59O1xuLyoqXG4gKiBHZXQgbGF5b3V0IHByb3BlcnR5LlxuICogQHBhcmFtICB7c3RyaW5nfSBrZXkuXG4gKiBAcmV0dXJuIHsqfVxuICovXG5cblxubGlzdFByb3RvLmdldExheW91dCA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgcmV0dXJuIHRoaXMuX2xheW91dFtrZXldO1xufTtcbi8qKlxuICogR2V0IGxheW91dCBvZiBzaW5nbGUgZGF0YSBpdGVtXG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKi9cblxuXG5saXN0UHJvdG8uZ2V0SXRlbUxheW91dCA9IGZ1bmN0aW9uIChpZHgpIHtcbiAgcmV0dXJuIHRoaXMuX2l0ZW1MYXlvdXRzW2lkeF07XG59O1xuLyoqXG4gKiBTZXQgbGF5b3V0IG9mIHNpbmdsZSBkYXRhIGl0ZW1cbiAqIEBwYXJhbSB7bnVtYmVyfSBpZHhcbiAqIEBwYXJhbSB7T2JqZWN0fSBsYXlvdXRcbiAqIEBwYXJhbSB7Ym9vbGVhbj19IFttZXJnZT1mYWxzZV1cbiAqL1xuXG5cbmxpc3RQcm90by5zZXRJdGVtTGF5b3V0ID0gZnVuY3Rpb24gKGlkeCwgbGF5b3V0LCBtZXJnZSkge1xuICB0aGlzLl9pdGVtTGF5b3V0c1tpZHhdID0gbWVyZ2UgPyB6clV0aWwuZXh0ZW5kKHRoaXMuX2l0ZW1MYXlvdXRzW2lkeF0gfHwge30sIGxheW91dCkgOiBsYXlvdXQ7XG59O1xuLyoqXG4gKiBDbGVhciBhbGwgbGF5b3V0IG9mIHNpbmdsZSBkYXRhIGl0ZW1cbiAqL1xuXG5cbmxpc3RQcm90by5jbGVhckl0ZW1MYXlvdXRzID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLl9pdGVtTGF5b3V0cy5sZW5ndGggPSAwO1xufTtcbi8qKlxuICogR2V0IHZpc3VhbCBwcm9wZXJ0eSBvZiBzaW5nbGUgZGF0YSBpdGVtXG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtpZ25vcmVQYXJlbnQ9ZmFsc2VdXG4gKi9cblxuXG5saXN0UHJvdG8uZ2V0SXRlbVZpc3VhbCA9IGZ1bmN0aW9uIChpZHgsIGtleSwgaWdub3JlUGFyZW50KSB7XG4gIHZhciBpdGVtVmlzdWFsID0gdGhpcy5faXRlbVZpc3VhbHNbaWR4XTtcbiAgdmFyIHZhbCA9IGl0ZW1WaXN1YWwgJiYgaXRlbVZpc3VhbFtrZXldO1xuXG4gIGlmICh2YWwgPT0gbnVsbCAmJiAhaWdub3JlUGFyZW50KSB7XG4gICAgLy8gVXNlIGdsb2JhbCB2aXN1YWwgcHJvcGVydHlcbiAgICByZXR1cm4gdGhpcy5nZXRWaXN1YWwoa2V5KTtcbiAgfVxuXG4gIHJldHVybiB2YWw7XG59O1xuLyoqXG4gKiBTZXQgdmlzdWFsIHByb3BlcnR5IG9mIHNpbmdsZSBkYXRhIGl0ZW1cbiAqXG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IGtleVxuICogQHBhcmFtIHsqfSBbdmFsdWVdXG4gKlxuICogQGV4YW1wbGVcbiAqICBzZXRJdGVtVmlzdWFsKDAsICdjb2xvcicsIGNvbG9yKTtcbiAqICBzZXRJdGVtVmlzdWFsKDAsIHtcbiAqICAgICAgJ2NvbG9yJzogY29sb3JcbiAqICB9KTtcbiAqL1xuXG5cbmxpc3RQcm90by5zZXRJdGVtVmlzdWFsID0gZnVuY3Rpb24gKGlkeCwga2V5LCB2YWx1ZSkge1xuICB2YXIgaXRlbVZpc3VhbCA9IHRoaXMuX2l0ZW1WaXN1YWxzW2lkeF0gfHwge307XG4gIHZhciBoYXNJdGVtVmlzdWFsID0gdGhpcy5oYXNJdGVtVmlzdWFsO1xuICB0aGlzLl9pdGVtVmlzdWFsc1tpZHhdID0gaXRlbVZpc3VhbDtcblxuICBpZiAoaXNPYmplY3Qoa2V5KSkge1xuICAgIGZvciAodmFyIG5hbWUgaW4ga2V5KSB7XG4gICAgICBpZiAoa2V5Lmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgIGl0ZW1WaXN1YWxbbmFtZV0gPSBrZXlbbmFtZV07XG4gICAgICAgIGhhc0l0ZW1WaXN1YWxbbmFtZV0gPSB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybjtcbiAgfVxuXG4gIGl0ZW1WaXN1YWxba2V5XSA9IHZhbHVlO1xuICBoYXNJdGVtVmlzdWFsW2tleV0gPSB0cnVlO1xufTtcbi8qKlxuICogQ2xlYXIgaXRlbVZpc3VhbHMgYW5kIGxpc3QgdmlzdWFsLlxuICovXG5cblxubGlzdFByb3RvLmNsZWFyQWxsVmlzdWFsID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLl92aXN1YWwgPSB7fTtcbiAgdGhpcy5faXRlbVZpc3VhbHMgPSBbXTtcbiAgdGhpcy5oYXNJdGVtVmlzdWFsID0ge307XG59O1xuXG52YXIgc2V0SXRlbURhdGFBbmRTZXJpZXNJbmRleCA9IGZ1bmN0aW9uIChjaGlsZCkge1xuICBjaGlsZC5zZXJpZXNJbmRleCA9IHRoaXMuc2VyaWVzSW5kZXg7XG4gIGNoaWxkLmRhdGFJbmRleCA9IHRoaXMuZGF0YUluZGV4O1xuICBjaGlsZC5kYXRhVHlwZSA9IHRoaXMuZGF0YVR5cGU7XG59O1xuLyoqXG4gKiBTZXQgZ3JhcGhpYyBlbGVtZW50IHJlbGF0aXZlIHRvIGRhdGEuIEl0IGNhbiBiZSBzZXQgYXMgbnVsbFxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBbZWxdXG4gKi9cblxuXG5saXN0UHJvdG8uc2V0SXRlbUdyYXBoaWNFbCA9IGZ1bmN0aW9uIChpZHgsIGVsKSB7XG4gIHZhciBob3N0TW9kZWwgPSB0aGlzLmhvc3RNb2RlbDtcblxuICBpZiAoZWwpIHtcbiAgICAvLyBBZGQgZGF0YSBpbmRleCBhbmQgc2VyaWVzIGluZGV4IGZvciBpbmRleGluZyB0aGUgZGF0YSBieSBlbGVtZW50XG4gICAgLy8gVXNlZnVsIGluIHRvb2x0aXBcbiAgICBlbC5kYXRhSW5kZXggPSBpZHg7XG4gICAgZWwuZGF0YVR5cGUgPSB0aGlzLmRhdGFUeXBlO1xuICAgIGVsLnNlcmllc0luZGV4ID0gaG9zdE1vZGVsICYmIGhvc3RNb2RlbC5zZXJpZXNJbmRleDtcblxuICAgIGlmIChlbC50eXBlID09PSAnZ3JvdXAnKSB7XG4gICAgICBlbC50cmF2ZXJzZShzZXRJdGVtRGF0YUFuZFNlcmllc0luZGV4LCBlbCk7XG4gICAgfVxuICB9XG5cbiAgdGhpcy5fZ3JhcGhpY0Vsc1tpZHhdID0gZWw7XG59O1xuLyoqXG4gKiBAcGFyYW0ge251bWJlcn0gaWR4XG4gKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9FbGVtZW50fVxuICovXG5cblxubGlzdFByb3RvLmdldEl0ZW1HcmFwaGljRWwgPSBmdW5jdGlvbiAoaWR4KSB7XG4gIHJldHVybiB0aGlzLl9ncmFwaGljRWxzW2lkeF07XG59O1xuLyoqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBjb250ZXh0XG4gKi9cblxuXG5saXN0UHJvdG8uZWFjaEl0ZW1HcmFwaGljRWwgPSBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHtcbiAgenJVdGlsLmVhY2godGhpcy5fZ3JhcGhpY0VscywgZnVuY3Rpb24gKGVsLCBpZHgpIHtcbiAgICBpZiAoZWwpIHtcbiAgICAgIGNiICYmIGNiLmNhbGwoY29udGV4dCwgZWwsIGlkeCk7XG4gICAgfVxuICB9KTtcbn07XG4vKipcbiAqIFNoYWxsb3cgY2xvbmUgYSBuZXcgbGlzdCBleGNlcHQgdmlzdWFsIGFuZCBsYXlvdXQgcHJvcGVydGllcywgYW5kIGdyYXBoIGVsZW1lbnRzLlxuICogTmV3IGxpc3Qgb25seSBjaGFuZ2UgdGhlIGluZGljZXMuXG4gKi9cblxuXG5saXN0UHJvdG8uY2xvbmVTaGFsbG93ID0gZnVuY3Rpb24gKGxpc3QpIHtcbiAgaWYgKCFsaXN0KSB7XG4gICAgdmFyIGRpbWVuc2lvbkluZm9MaXN0ID0genJVdGlsLm1hcCh0aGlzLmRpbWVuc2lvbnMsIHRoaXMuZ2V0RGltZW5zaW9uSW5mbywgdGhpcyk7XG4gICAgbGlzdCA9IG5ldyBMaXN0KGRpbWVuc2lvbkluZm9MaXN0LCB0aGlzLmhvc3RNb2RlbCk7XG4gIH0gLy8gRklYTUVcblxuXG4gIGxpc3QuX3N0b3JhZ2UgPSB0aGlzLl9zdG9yYWdlO1xuICB0cmFuc2ZlclByb3BlcnRpZXMobGlzdCwgdGhpcyk7IC8vIENsb25lIHdpbGwgbm90IGNoYW5nZSB0aGUgZGF0YSBleHRlbnQgYW5kIGluZGljZXNcblxuICBpZiAodGhpcy5faW5kaWNlcykge1xuICAgIHZhciBDdG9yID0gdGhpcy5faW5kaWNlcy5jb25zdHJ1Y3RvcjtcbiAgICBsaXN0Ll9pbmRpY2VzID0gbmV3IEN0b3IodGhpcy5faW5kaWNlcyk7XG4gIH0gZWxzZSB7XG4gICAgbGlzdC5faW5kaWNlcyA9IG51bGw7XG4gIH1cblxuICBsaXN0LmdldFJhd0luZGV4ID0gbGlzdC5faW5kaWNlcyA/IGdldFJhd0luZGV4V2l0aEluZGljZXMgOiBnZXRSYXdJbmRleFdpdGhvdXRJbmRpY2VzO1xuICByZXR1cm4gbGlzdDtcbn07XG4vKipcbiAqIFdyYXAgc29tZSBtZXRob2QgdG8gYWRkIG1vcmUgZmVhdHVyZVxuICogQHBhcmFtIHtzdHJpbmd9IG1ldGhvZE5hbWVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGluamVjdEZ1bmN0aW9uXG4gKi9cblxuXG5saXN0UHJvdG8ud3JhcE1ldGhvZCA9IGZ1bmN0aW9uIChtZXRob2ROYW1lLCBpbmplY3RGdW5jdGlvbikge1xuICB2YXIgb3JpZ2luYWxNZXRob2QgPSB0aGlzW21ldGhvZE5hbWVdO1xuXG4gIGlmICh0eXBlb2Ygb3JpZ2luYWxNZXRob2QgIT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB0aGlzLl9fd3JhcHBlZE1ldGhvZHMgPSB0aGlzLl9fd3JhcHBlZE1ldGhvZHMgfHwgW107XG5cbiAgdGhpcy5fX3dyYXBwZWRNZXRob2RzLnB1c2gobWV0aG9kTmFtZSk7XG5cbiAgdGhpc1ttZXRob2ROYW1lXSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcmVzID0gb3JpZ2luYWxNZXRob2QuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICByZXR1cm4gaW5qZWN0RnVuY3Rpb24uYXBwbHkodGhpcywgW3Jlc10uY29uY2F0KHpyVXRpbC5zbGljZShhcmd1bWVudHMpKSk7XG4gIH07XG59OyAvLyBNZXRob2RzIHRoYXQgY3JlYXRlIGEgbmV3IGxpc3QgYmFzZWQgb24gdGhpcyBsaXN0IHNob3VsZCBiZSBsaXN0ZWQgaGVyZS5cbi8vIE5vdGljZSB0aGF0IHRob3NlIG1ldGhvZCBzaG91bGQgYFJFVFVSTmAgdGhlIG5ldyBsaXN0LlxuXG5cbmxpc3RQcm90by5UUkFOU0ZFUkFCTEVfTUVUSE9EUyA9IFsnY2xvbmVTaGFsbG93JywgJ2Rvd25TYW1wbGUnLCAnbWFwJ107IC8vIE1ldGhvZHMgdGhhdCBjaGFuZ2UgaW5kaWNlcyBvZiB0aGlzIGxpc3Qgc2hvdWxkIGJlIGxpc3RlZCBoZXJlLlxuXG5saXN0UHJvdG8uQ0hBTkdBQkxFX01FVEhPRFMgPSBbJ2ZpbHRlclNlbGYnLCAnc2VsZWN0UmFuZ2UnXTtcbnZhciBfZGVmYXVsdCA9IExpc3Q7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBOzs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7O0FBSUE7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBOzs7O0FBSUE7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBOzs7Ozs7QUFNQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZCQTtBQXlCQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/List.js
