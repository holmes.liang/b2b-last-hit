/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var SymbolClz = __webpack_require__(/*! ./Symbol */ "./node_modules/echarts/lib/chart/helper/Symbol.js");

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var isObject = _util.isObject;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @module echarts/chart/helper/SymbolDraw
 */

/**
 * @constructor
 * @alias module:echarts/chart/helper/SymbolDraw
 * @param {module:zrender/graphic/Group} [symbolCtor]
 */

function SymbolDraw(symbolCtor) {
  this.group = new graphic.Group();
  this._symbolCtor = symbolCtor || SymbolClz;
}

var symbolDrawProto = SymbolDraw.prototype;

function symbolNeedsDraw(data, point, idx, opt) {
  return point && !isNaN(point[0]) && !isNaN(point[1]) && !(opt.isIgnore && opt.isIgnore(idx)) // We do not set clipShape on group, because it will cut part of
  // the symbol element shape. We use the same clip shape here as
  // the line clip.
  && !(opt.clipShape && !opt.clipShape.contain(point[0], point[1])) && data.getItemVisual(idx, 'symbol') !== 'none';
}
/**
 * Update symbols draw by new data
 * @param {module:echarts/data/List} data
 * @param {Object} [opt] Or isIgnore
 * @param {Function} [opt.isIgnore]
 * @param {Object} [opt.clipShape]
 */


symbolDrawProto.updateData = function (data, opt) {
  opt = normalizeUpdateOpt(opt);
  var group = this.group;
  var seriesModel = data.hostModel;
  var oldData = this._data;
  var SymbolCtor = this._symbolCtor;
  var seriesScope = makeSeriesScope(data); // There is no oldLineData only when first rendering or switching from
  // stream mode to normal mode, where previous elements should be removed.

  if (!oldData) {
    group.removeAll();
  }

  data.diff(oldData).add(function (newIdx) {
    var point = data.getItemLayout(newIdx);

    if (symbolNeedsDraw(data, point, newIdx, opt)) {
      var symbolEl = new SymbolCtor(data, newIdx, seriesScope);
      symbolEl.attr('position', point);
      data.setItemGraphicEl(newIdx, symbolEl);
      group.add(symbolEl);
    }
  }).update(function (newIdx, oldIdx) {
    var symbolEl = oldData.getItemGraphicEl(oldIdx);
    var point = data.getItemLayout(newIdx);

    if (!symbolNeedsDraw(data, point, newIdx, opt)) {
      group.remove(symbolEl);
      return;
    }

    if (!symbolEl) {
      symbolEl = new SymbolCtor(data, newIdx);
      symbolEl.attr('position', point);
    } else {
      symbolEl.updateData(data, newIdx, seriesScope);
      graphic.updateProps(symbolEl, {
        position: point
      }, seriesModel);
    } // Add back


    group.add(symbolEl);
    data.setItemGraphicEl(newIdx, symbolEl);
  }).remove(function (oldIdx) {
    var el = oldData.getItemGraphicEl(oldIdx);
    el && el.fadeOut(function () {
      group.remove(el);
    });
  }).execute();
  this._data = data;
};

symbolDrawProto.isPersistent = function () {
  return true;
};

symbolDrawProto.updateLayout = function () {
  var data = this._data;

  if (data) {
    // Not use animation
    data.eachItemGraphicEl(function (el, idx) {
      var point = data.getItemLayout(idx);
      el.attr('position', point);
    });
  }
};

symbolDrawProto.incrementalPrepareUpdate = function (data) {
  this._seriesScope = makeSeriesScope(data);
  this._data = null;
  this.group.removeAll();
};
/**
 * Update symbols draw by new data
 * @param {module:echarts/data/List} data
 * @param {Object} [opt] Or isIgnore
 * @param {Function} [opt.isIgnore]
 * @param {Object} [opt.clipShape]
 */


symbolDrawProto.incrementalUpdate = function (taskParams, data, opt) {
  opt = normalizeUpdateOpt(opt);

  function updateIncrementalAndHover(el) {
    if (!el.isGroup) {
      el.incremental = el.useHoverLayer = true;
    }
  }

  for (var idx = taskParams.start; idx < taskParams.end; idx++) {
    var point = data.getItemLayout(idx);

    if (symbolNeedsDraw(data, point, idx, opt)) {
      var el = new this._symbolCtor(data, idx, this._seriesScope);
      el.traverse(updateIncrementalAndHover);
      el.attr('position', point);
      this.group.add(el);
      data.setItemGraphicEl(idx, el);
    }
  }
};

function normalizeUpdateOpt(opt) {
  if (opt != null && !isObject(opt)) {
    opt = {
      isIgnore: opt
    };
  }

  return opt || {};
}

symbolDrawProto.remove = function (enableAnimation) {
  var group = this.group;
  var data = this._data; // Incremental model do not have this._data.

  if (data && enableAnimation) {
    data.eachItemGraphicEl(function (el) {
      el.fadeOut(function () {
        group.remove(el);
      });
    });
  } else {
    group.removeAll();
  }
};

function makeSeriesScope(data) {
  var seriesModel = data.hostModel;
  return {
    itemStyle: seriesModel.getModel('itemStyle').getItemStyle(['color']),
    hoverItemStyle: seriesModel.getModel('emphasis.itemStyle').getItemStyle(),
    symbolRotate: seriesModel.get('symbolRotate'),
    symbolOffset: seriesModel.get('symbolOffset'),
    hoverAnimation: seriesModel.get('hoverAnimation'),
    labelModel: seriesModel.getModel('label'),
    hoverLabelModel: seriesModel.getModel('emphasis.label'),
    cursorStyle: seriesModel.get('cursor')
  };
}

var _default = SymbolDraw;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvaGVscGVyL1N5bWJvbERyYXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9oZWxwZXIvU3ltYm9sRHJhdy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIGdyYXBoaWMgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9ncmFwaGljXCIpO1xuXG52YXIgU3ltYm9sQ2x6ID0gcmVxdWlyZShcIi4vU3ltYm9sXCIpO1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgaXNPYmplY3QgPSBfdXRpbC5pc09iamVjdDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBtb2R1bGUgZWNoYXJ0cy9jaGFydC9oZWxwZXIvU3ltYm9sRHJhd1xuICovXG5cbi8qKlxuICogQGNvbnN0cnVjdG9yXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvY2hhcnQvaGVscGVyL1N5bWJvbERyYXdcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9Hcm91cH0gW3N5bWJvbEN0b3JdXG4gKi9cbmZ1bmN0aW9uIFN5bWJvbERyYXcoc3ltYm9sQ3Rvcikge1xuICB0aGlzLmdyb3VwID0gbmV3IGdyYXBoaWMuR3JvdXAoKTtcbiAgdGhpcy5fc3ltYm9sQ3RvciA9IHN5bWJvbEN0b3IgfHwgU3ltYm9sQ2x6O1xufVxuXG52YXIgc3ltYm9sRHJhd1Byb3RvID0gU3ltYm9sRHJhdy5wcm90b3R5cGU7XG5cbmZ1bmN0aW9uIHN5bWJvbE5lZWRzRHJhdyhkYXRhLCBwb2ludCwgaWR4LCBvcHQpIHtcbiAgcmV0dXJuIHBvaW50ICYmICFpc05hTihwb2ludFswXSkgJiYgIWlzTmFOKHBvaW50WzFdKSAmJiAhKG9wdC5pc0lnbm9yZSAmJiBvcHQuaXNJZ25vcmUoaWR4KSkgLy8gV2UgZG8gbm90IHNldCBjbGlwU2hhcGUgb24gZ3JvdXAsIGJlY2F1c2UgaXQgd2lsbCBjdXQgcGFydCBvZlxuICAvLyB0aGUgc3ltYm9sIGVsZW1lbnQgc2hhcGUuIFdlIHVzZSB0aGUgc2FtZSBjbGlwIHNoYXBlIGhlcmUgYXNcbiAgLy8gdGhlIGxpbmUgY2xpcC5cbiAgJiYgIShvcHQuY2xpcFNoYXBlICYmICFvcHQuY2xpcFNoYXBlLmNvbnRhaW4ocG9pbnRbMF0sIHBvaW50WzFdKSkgJiYgZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ3N5bWJvbCcpICE9PSAnbm9uZSc7XG59XG4vKipcbiAqIFVwZGF0ZSBzeW1ib2xzIGRyYXcgYnkgbmV3IGRhdGFcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fSBkYXRhXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdF0gT3IgaXNJZ25vcmVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtvcHQuaXNJZ25vcmVdXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdC5jbGlwU2hhcGVdXG4gKi9cblxuXG5zeW1ib2xEcmF3UHJvdG8udXBkYXRlRGF0YSA9IGZ1bmN0aW9uIChkYXRhLCBvcHQpIHtcbiAgb3B0ID0gbm9ybWFsaXplVXBkYXRlT3B0KG9wdCk7XG4gIHZhciBncm91cCA9IHRoaXMuZ3JvdXA7XG4gIHZhciBzZXJpZXNNb2RlbCA9IGRhdGEuaG9zdE1vZGVsO1xuICB2YXIgb2xkRGF0YSA9IHRoaXMuX2RhdGE7XG4gIHZhciBTeW1ib2xDdG9yID0gdGhpcy5fc3ltYm9sQ3RvcjtcbiAgdmFyIHNlcmllc1Njb3BlID0gbWFrZVNlcmllc1Njb3BlKGRhdGEpOyAvLyBUaGVyZSBpcyBubyBvbGRMaW5lRGF0YSBvbmx5IHdoZW4gZmlyc3QgcmVuZGVyaW5nIG9yIHN3aXRjaGluZyBmcm9tXG4gIC8vIHN0cmVhbSBtb2RlIHRvIG5vcm1hbCBtb2RlLCB3aGVyZSBwcmV2aW91cyBlbGVtZW50cyBzaG91bGQgYmUgcmVtb3ZlZC5cblxuICBpZiAoIW9sZERhdGEpIHtcbiAgICBncm91cC5yZW1vdmVBbGwoKTtcbiAgfVxuXG4gIGRhdGEuZGlmZihvbGREYXRhKS5hZGQoZnVuY3Rpb24gKG5ld0lkeCkge1xuICAgIHZhciBwb2ludCA9IGRhdGEuZ2V0SXRlbUxheW91dChuZXdJZHgpO1xuXG4gICAgaWYgKHN5bWJvbE5lZWRzRHJhdyhkYXRhLCBwb2ludCwgbmV3SWR4LCBvcHQpKSB7XG4gICAgICB2YXIgc3ltYm9sRWwgPSBuZXcgU3ltYm9sQ3RvcihkYXRhLCBuZXdJZHgsIHNlcmllc1Njb3BlKTtcbiAgICAgIHN5bWJvbEVsLmF0dHIoJ3Bvc2l0aW9uJywgcG9pbnQpO1xuICAgICAgZGF0YS5zZXRJdGVtR3JhcGhpY0VsKG5ld0lkeCwgc3ltYm9sRWwpO1xuICAgICAgZ3JvdXAuYWRkKHN5bWJvbEVsKTtcbiAgICB9XG4gIH0pLnVwZGF0ZShmdW5jdGlvbiAobmV3SWR4LCBvbGRJZHgpIHtcbiAgICB2YXIgc3ltYm9sRWwgPSBvbGREYXRhLmdldEl0ZW1HcmFwaGljRWwob2xkSWR4KTtcbiAgICB2YXIgcG9pbnQgPSBkYXRhLmdldEl0ZW1MYXlvdXQobmV3SWR4KTtcblxuICAgIGlmICghc3ltYm9sTmVlZHNEcmF3KGRhdGEsIHBvaW50LCBuZXdJZHgsIG9wdCkpIHtcbiAgICAgIGdyb3VwLnJlbW92ZShzeW1ib2xFbCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCFzeW1ib2xFbCkge1xuICAgICAgc3ltYm9sRWwgPSBuZXcgU3ltYm9sQ3RvcihkYXRhLCBuZXdJZHgpO1xuICAgICAgc3ltYm9sRWwuYXR0cigncG9zaXRpb24nLCBwb2ludCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN5bWJvbEVsLnVwZGF0ZURhdGEoZGF0YSwgbmV3SWR4LCBzZXJpZXNTY29wZSk7XG4gICAgICBncmFwaGljLnVwZGF0ZVByb3BzKHN5bWJvbEVsLCB7XG4gICAgICAgIHBvc2l0aW9uOiBwb2ludFxuICAgICAgfSwgc2VyaWVzTW9kZWwpO1xuICAgIH0gLy8gQWRkIGJhY2tcblxuXG4gICAgZ3JvdXAuYWRkKHN5bWJvbEVsKTtcbiAgICBkYXRhLnNldEl0ZW1HcmFwaGljRWwobmV3SWR4LCBzeW1ib2xFbCk7XG4gIH0pLnJlbW92ZShmdW5jdGlvbiAob2xkSWR4KSB7XG4gICAgdmFyIGVsID0gb2xkRGF0YS5nZXRJdGVtR3JhcGhpY0VsKG9sZElkeCk7XG4gICAgZWwgJiYgZWwuZmFkZU91dChmdW5jdGlvbiAoKSB7XG4gICAgICBncm91cC5yZW1vdmUoZWwpO1xuICAgIH0pO1xuICB9KS5leGVjdXRlKCk7XG4gIHRoaXMuX2RhdGEgPSBkYXRhO1xufTtcblxuc3ltYm9sRHJhd1Byb3RvLmlzUGVyc2lzdGVudCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRydWU7XG59O1xuXG5zeW1ib2xEcmF3UHJvdG8udXBkYXRlTGF5b3V0ID0gZnVuY3Rpb24gKCkge1xuICB2YXIgZGF0YSA9IHRoaXMuX2RhdGE7XG5cbiAgaWYgKGRhdGEpIHtcbiAgICAvLyBOb3QgdXNlIGFuaW1hdGlvblxuICAgIGRhdGEuZWFjaEl0ZW1HcmFwaGljRWwoZnVuY3Rpb24gKGVsLCBpZHgpIHtcbiAgICAgIHZhciBwb2ludCA9IGRhdGEuZ2V0SXRlbUxheW91dChpZHgpO1xuICAgICAgZWwuYXR0cigncG9zaXRpb24nLCBwb2ludCk7XG4gICAgfSk7XG4gIH1cbn07XG5cbnN5bWJvbERyYXdQcm90by5pbmNyZW1lbnRhbFByZXBhcmVVcGRhdGUgPSBmdW5jdGlvbiAoZGF0YSkge1xuICB0aGlzLl9zZXJpZXNTY29wZSA9IG1ha2VTZXJpZXNTY29wZShkYXRhKTtcbiAgdGhpcy5fZGF0YSA9IG51bGw7XG4gIHRoaXMuZ3JvdXAucmVtb3ZlQWxsKCk7XG59O1xuLyoqXG4gKiBVcGRhdGUgc3ltYm9scyBkcmF3IGJ5IG5ldyBkYXRhXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdIE9yIGlzSWdub3JlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbb3B0LmlzSWdub3JlXVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHQuY2xpcFNoYXBlXVxuICovXG5cblxuc3ltYm9sRHJhd1Byb3RvLmluY3JlbWVudGFsVXBkYXRlID0gZnVuY3Rpb24gKHRhc2tQYXJhbXMsIGRhdGEsIG9wdCkge1xuICBvcHQgPSBub3JtYWxpemVVcGRhdGVPcHQob3B0KTtcblxuICBmdW5jdGlvbiB1cGRhdGVJbmNyZW1lbnRhbEFuZEhvdmVyKGVsKSB7XG4gICAgaWYgKCFlbC5pc0dyb3VwKSB7XG4gICAgICBlbC5pbmNyZW1lbnRhbCA9IGVsLnVzZUhvdmVyTGF5ZXIgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGZvciAodmFyIGlkeCA9IHRhc2tQYXJhbXMuc3RhcnQ7IGlkeCA8IHRhc2tQYXJhbXMuZW5kOyBpZHgrKykge1xuICAgIHZhciBwb2ludCA9IGRhdGEuZ2V0SXRlbUxheW91dChpZHgpO1xuXG4gICAgaWYgKHN5bWJvbE5lZWRzRHJhdyhkYXRhLCBwb2ludCwgaWR4LCBvcHQpKSB7XG4gICAgICB2YXIgZWwgPSBuZXcgdGhpcy5fc3ltYm9sQ3RvcihkYXRhLCBpZHgsIHRoaXMuX3Nlcmllc1Njb3BlKTtcbiAgICAgIGVsLnRyYXZlcnNlKHVwZGF0ZUluY3JlbWVudGFsQW5kSG92ZXIpO1xuICAgICAgZWwuYXR0cigncG9zaXRpb24nLCBwb2ludCk7XG4gICAgICB0aGlzLmdyb3VwLmFkZChlbCk7XG4gICAgICBkYXRhLnNldEl0ZW1HcmFwaGljRWwoaWR4LCBlbCk7XG4gICAgfVxuICB9XG59O1xuXG5mdW5jdGlvbiBub3JtYWxpemVVcGRhdGVPcHQob3B0KSB7XG4gIGlmIChvcHQgIT0gbnVsbCAmJiAhaXNPYmplY3Qob3B0KSkge1xuICAgIG9wdCA9IHtcbiAgICAgIGlzSWdub3JlOiBvcHRcbiAgICB9O1xuICB9XG5cbiAgcmV0dXJuIG9wdCB8fCB7fTtcbn1cblxuc3ltYm9sRHJhd1Byb3RvLnJlbW92ZSA9IGZ1bmN0aW9uIChlbmFibGVBbmltYXRpb24pIHtcbiAgdmFyIGdyb3VwID0gdGhpcy5ncm91cDtcbiAgdmFyIGRhdGEgPSB0aGlzLl9kYXRhOyAvLyBJbmNyZW1lbnRhbCBtb2RlbCBkbyBub3QgaGF2ZSB0aGlzLl9kYXRhLlxuXG4gIGlmIChkYXRhICYmIGVuYWJsZUFuaW1hdGlvbikge1xuICAgIGRhdGEuZWFjaEl0ZW1HcmFwaGljRWwoZnVuY3Rpb24gKGVsKSB7XG4gICAgICBlbC5mYWRlT3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZ3JvdXAucmVtb3ZlKGVsKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9IGVsc2Uge1xuICAgIGdyb3VwLnJlbW92ZUFsbCgpO1xuICB9XG59O1xuXG5mdW5jdGlvbiBtYWtlU2VyaWVzU2NvcGUoZGF0YSkge1xuICB2YXIgc2VyaWVzTW9kZWwgPSBkYXRhLmhvc3RNb2RlbDtcbiAgcmV0dXJuIHtcbiAgICBpdGVtU3R5bGU6IHNlcmllc01vZGVsLmdldE1vZGVsKCdpdGVtU3R5bGUnKS5nZXRJdGVtU3R5bGUoWydjb2xvciddKSxcbiAgICBob3Zlckl0ZW1TdHlsZTogc2VyaWVzTW9kZWwuZ2V0TW9kZWwoJ2VtcGhhc2lzLml0ZW1TdHlsZScpLmdldEl0ZW1TdHlsZSgpLFxuICAgIHN5bWJvbFJvdGF0ZTogc2VyaWVzTW9kZWwuZ2V0KCdzeW1ib2xSb3RhdGUnKSxcbiAgICBzeW1ib2xPZmZzZXQ6IHNlcmllc01vZGVsLmdldCgnc3ltYm9sT2Zmc2V0JyksXG4gICAgaG92ZXJBbmltYXRpb246IHNlcmllc01vZGVsLmdldCgnaG92ZXJBbmltYXRpb24nKSxcbiAgICBsYWJlbE1vZGVsOiBzZXJpZXNNb2RlbC5nZXRNb2RlbCgnbGFiZWwnKSxcbiAgICBob3ZlckxhYmVsTW9kZWw6IHNlcmllc01vZGVsLmdldE1vZGVsKCdlbXBoYXNpcy5sYWJlbCcpLFxuICAgIGN1cnNvclN0eWxlOiBzZXJpZXNNb2RlbC5nZXQoJ2N1cnNvcicpXG4gIH07XG59XG5cbnZhciBfZGVmYXVsdCA9IFN5bWJvbERyYXc7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7O0FBSUE7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/helper/SymbolDraw.js
