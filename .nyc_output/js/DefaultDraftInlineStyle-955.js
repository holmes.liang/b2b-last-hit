/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DefaultDraftInlineStyle
 * @format
 * 
 */


module.exports = {
  BOLD: {
    fontWeight: 'bold'
  },
  CODE: {
    fontFamily: 'monospace',
    wordWrap: 'break-word'
  },
  ITALIC: {
    fontStyle: 'italic'
  },
  STRIKETHROUGH: {
    textDecoration: 'line-through'
  },
  UNDERLINE: {
    textDecoration: 'underline'
  }
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RlZmF1bHREcmFmdElubGluZVN0eWxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RlZmF1bHREcmFmdElubGluZVN0eWxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRGVmYXVsdERyYWZ0SW5saW5lU3R5bGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIEJPTEQ6IHtcbiAgICBmb250V2VpZ2h0OiAnYm9sZCdcbiAgfSxcblxuICBDT0RFOiB7XG4gICAgZm9udEZhbWlseTogJ21vbm9zcGFjZScsXG4gICAgd29yZFdyYXA6ICdicmVhay13b3JkJ1xuICB9LFxuXG4gIElUQUxJQzoge1xuICAgIGZvbnRTdHlsZTogJ2l0YWxpYydcbiAgfSxcblxuICBTVFJJS0VUSFJPVUdIOiB7XG4gICAgdGV4dERlY29yYXRpb246ICdsaW5lLXRocm91Z2gnXG4gIH0sXG5cbiAgVU5ERVJMSU5FOiB7XG4gICAgdGV4dERlY29yYXRpb246ICd1bmRlcmxpbmUnXG4gIH1cbn07Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBREE7QUFsQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DefaultDraftInlineStyle.js
