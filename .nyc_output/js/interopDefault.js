__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return interopDefault; });
// https://github.com/moment/moment/issues/3650
// since we are using ts 3.5.1, it should be safe to remove.
function interopDefault(m) {
  return m["default"] || m;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9pbnRlcm9wRGVmYXVsdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvX3V0aWwvaW50ZXJvcERlZmF1bHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gaHR0cHM6Ly9naXRodWIuY29tL21vbWVudC9tb21lbnQvaXNzdWVzLzM2NTBcbi8vIHNpbmNlIHdlIGFyZSB1c2luZyB0cyAzLjUuMSwgaXQgc2hvdWxkIGJlIHNhZmUgdG8gcmVtb3ZlLlxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW50ZXJvcERlZmF1bHQobSkge1xuICAgIHJldHVybiBtLmRlZmF1bHQgfHwgbTtcbn1cbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/interopDefault.js
