// https://github.com/mziccard/node-timsort
var DEFAULT_MIN_MERGE = 32;
var DEFAULT_MIN_GALLOPING = 7;
var DEFAULT_TMP_STORAGE_LENGTH = 256;

function minRunLength(n) {
  var r = 0;

  while (n >= DEFAULT_MIN_MERGE) {
    r |= n & 1;
    n >>= 1;
  }

  return n + r;
}

function makeAscendingRun(array, lo, hi, compare) {
  var runHi = lo + 1;

  if (runHi === hi) {
    return 1;
  }

  if (compare(array[runHi++], array[lo]) < 0) {
    while (runHi < hi && compare(array[runHi], array[runHi - 1]) < 0) {
      runHi++;
    }

    reverseRun(array, lo, runHi);
  } else {
    while (runHi < hi && compare(array[runHi], array[runHi - 1]) >= 0) {
      runHi++;
    }
  }

  return runHi - lo;
}

function reverseRun(array, lo, hi) {
  hi--;

  while (lo < hi) {
    var t = array[lo];
    array[lo++] = array[hi];
    array[hi--] = t;
  }
}

function binaryInsertionSort(array, lo, hi, start, compare) {
  if (start === lo) {
    start++;
  }

  for (; start < hi; start++) {
    var pivot = array[start];
    var left = lo;
    var right = start;
    var mid;

    while (left < right) {
      mid = left + right >>> 1;

      if (compare(pivot, array[mid]) < 0) {
        right = mid;
      } else {
        left = mid + 1;
      }
    }

    var n = start - left;

    switch (n) {
      case 3:
        array[left + 3] = array[left + 2];

      case 2:
        array[left + 2] = array[left + 1];

      case 1:
        array[left + 1] = array[left];
        break;

      default:
        while (n > 0) {
          array[left + n] = array[left + n - 1];
          n--;
        }

    }

    array[left] = pivot;
  }
}

function gallopLeft(value, array, start, length, hint, compare) {
  var lastOffset = 0;
  var maxOffset = 0;
  var offset = 1;

  if (compare(value, array[start + hint]) > 0) {
    maxOffset = length - hint;

    while (offset < maxOffset && compare(value, array[start + hint + offset]) > 0) {
      lastOffset = offset;
      offset = (offset << 1) + 1;

      if (offset <= 0) {
        offset = maxOffset;
      }
    }

    if (offset > maxOffset) {
      offset = maxOffset;
    }

    lastOffset += hint;
    offset += hint;
  } else {
    maxOffset = hint + 1;

    while (offset < maxOffset && compare(value, array[start + hint - offset]) <= 0) {
      lastOffset = offset;
      offset = (offset << 1) + 1;

      if (offset <= 0) {
        offset = maxOffset;
      }
    }

    if (offset > maxOffset) {
      offset = maxOffset;
    }

    var tmp = lastOffset;
    lastOffset = hint - offset;
    offset = hint - tmp;
  }

  lastOffset++;

  while (lastOffset < offset) {
    var m = lastOffset + (offset - lastOffset >>> 1);

    if (compare(value, array[start + m]) > 0) {
      lastOffset = m + 1;
    } else {
      offset = m;
    }
  }

  return offset;
}

function gallopRight(value, array, start, length, hint, compare) {
  var lastOffset = 0;
  var maxOffset = 0;
  var offset = 1;

  if (compare(value, array[start + hint]) < 0) {
    maxOffset = hint + 1;

    while (offset < maxOffset && compare(value, array[start + hint - offset]) < 0) {
      lastOffset = offset;
      offset = (offset << 1) + 1;

      if (offset <= 0) {
        offset = maxOffset;
      }
    }

    if (offset > maxOffset) {
      offset = maxOffset;
    }

    var tmp = lastOffset;
    lastOffset = hint - offset;
    offset = hint - tmp;
  } else {
    maxOffset = length - hint;

    while (offset < maxOffset && compare(value, array[start + hint + offset]) >= 0) {
      lastOffset = offset;
      offset = (offset << 1) + 1;

      if (offset <= 0) {
        offset = maxOffset;
      }
    }

    if (offset > maxOffset) {
      offset = maxOffset;
    }

    lastOffset += hint;
    offset += hint;
  }

  lastOffset++;

  while (lastOffset < offset) {
    var m = lastOffset + (offset - lastOffset >>> 1);

    if (compare(value, array[start + m]) < 0) {
      offset = m;
    } else {
      lastOffset = m + 1;
    }
  }

  return offset;
}

function TimSort(array, compare) {
  var minGallop = DEFAULT_MIN_GALLOPING;
  var length = 0;
  var tmpStorageLength = DEFAULT_TMP_STORAGE_LENGTH;
  var stackLength = 0;
  var runStart;
  var runLength;
  var stackSize = 0;
  length = array.length;

  if (length < 2 * DEFAULT_TMP_STORAGE_LENGTH) {
    tmpStorageLength = length >>> 1;
  }

  var tmp = [];
  stackLength = length < 120 ? 5 : length < 1542 ? 10 : length < 119151 ? 19 : 40;
  runStart = [];
  runLength = [];

  function pushRun(_runStart, _runLength) {
    runStart[stackSize] = _runStart;
    runLength[stackSize] = _runLength;
    stackSize += 1;
  }

  function mergeRuns() {
    while (stackSize > 1) {
      var n = stackSize - 2;

      if (n >= 1 && runLength[n - 1] <= runLength[n] + runLength[n + 1] || n >= 2 && runLength[n - 2] <= runLength[n] + runLength[n - 1]) {
        if (runLength[n - 1] < runLength[n + 1]) {
          n--;
        }
      } else if (runLength[n] > runLength[n + 1]) {
        break;
      }

      mergeAt(n);
    }
  }

  function forceMergeRuns() {
    while (stackSize > 1) {
      var n = stackSize - 2;

      if (n > 0 && runLength[n - 1] < runLength[n + 1]) {
        n--;
      }

      mergeAt(n);
    }
  }

  function mergeAt(i) {
    var start1 = runStart[i];
    var length1 = runLength[i];
    var start2 = runStart[i + 1];
    var length2 = runLength[i + 1];
    runLength[i] = length1 + length2;

    if (i === stackSize - 3) {
      runStart[i + 1] = runStart[i + 2];
      runLength[i + 1] = runLength[i + 2];
    }

    stackSize--;
    var k = gallopRight(array[start2], array, start1, length1, 0, compare);
    start1 += k;
    length1 -= k;

    if (length1 === 0) {
      return;
    }

    length2 = gallopLeft(array[start1 + length1 - 1], array, start2, length2, length2 - 1, compare);

    if (length2 === 0) {
      return;
    }

    if (length1 <= length2) {
      mergeLow(start1, length1, start2, length2);
    } else {
      mergeHigh(start1, length1, start2, length2);
    }
  }

  function mergeLow(start1, length1, start2, length2) {
    var i = 0;

    for (i = 0; i < length1; i++) {
      tmp[i] = array[start1 + i];
    }

    var cursor1 = 0;
    var cursor2 = start2;
    var dest = start1;
    array[dest++] = array[cursor2++];

    if (--length2 === 0) {
      for (i = 0; i < length1; i++) {
        array[dest + i] = tmp[cursor1 + i];
      }

      return;
    }

    if (length1 === 1) {
      for (i = 0; i < length2; i++) {
        array[dest + i] = array[cursor2 + i];
      }

      array[dest + length2] = tmp[cursor1];
      return;
    }

    var _minGallop = minGallop;
    var count1, count2, exit;

    while (1) {
      count1 = 0;
      count2 = 0;
      exit = false;

      do {
        if (compare(array[cursor2], tmp[cursor1]) < 0) {
          array[dest++] = array[cursor2++];
          count2++;
          count1 = 0;

          if (--length2 === 0) {
            exit = true;
            break;
          }
        } else {
          array[dest++] = tmp[cursor1++];
          count1++;
          count2 = 0;

          if (--length1 === 1) {
            exit = true;
            break;
          }
        }
      } while ((count1 | count2) < _minGallop);

      if (exit) {
        break;
      }

      do {
        count1 = gallopRight(array[cursor2], tmp, cursor1, length1, 0, compare);

        if (count1 !== 0) {
          for (i = 0; i < count1; i++) {
            array[dest + i] = tmp[cursor1 + i];
          }

          dest += count1;
          cursor1 += count1;
          length1 -= count1;

          if (length1 <= 1) {
            exit = true;
            break;
          }
        }

        array[dest++] = array[cursor2++];

        if (--length2 === 0) {
          exit = true;
          break;
        }

        count2 = gallopLeft(tmp[cursor1], array, cursor2, length2, 0, compare);

        if (count2 !== 0) {
          for (i = 0; i < count2; i++) {
            array[dest + i] = array[cursor2 + i];
          }

          dest += count2;
          cursor2 += count2;
          length2 -= count2;

          if (length2 === 0) {
            exit = true;
            break;
          }
        }

        array[dest++] = tmp[cursor1++];

        if (--length1 === 1) {
          exit = true;
          break;
        }

        _minGallop--;
      } while (count1 >= DEFAULT_MIN_GALLOPING || count2 >= DEFAULT_MIN_GALLOPING);

      if (exit) {
        break;
      }

      if (_minGallop < 0) {
        _minGallop = 0;
      }

      _minGallop += 2;
    }

    minGallop = _minGallop;
    minGallop < 1 && (minGallop = 1);

    if (length1 === 1) {
      for (i = 0; i < length2; i++) {
        array[dest + i] = array[cursor2 + i];
      }

      array[dest + length2] = tmp[cursor1];
    } else if (length1 === 0) {
      throw new Error(); // throw new Error('mergeLow preconditions were not respected');
    } else {
      for (i = 0; i < length1; i++) {
        array[dest + i] = tmp[cursor1 + i];
      }
    }
  }

  function mergeHigh(start1, length1, start2, length2) {
    var i = 0;

    for (i = 0; i < length2; i++) {
      tmp[i] = array[start2 + i];
    }

    var cursor1 = start1 + length1 - 1;
    var cursor2 = length2 - 1;
    var dest = start2 + length2 - 1;
    var customCursor = 0;
    var customDest = 0;
    array[dest--] = array[cursor1--];

    if (--length1 === 0) {
      customCursor = dest - (length2 - 1);

      for (i = 0; i < length2; i++) {
        array[customCursor + i] = tmp[i];
      }

      return;
    }

    if (length2 === 1) {
      dest -= length1;
      cursor1 -= length1;
      customDest = dest + 1;
      customCursor = cursor1 + 1;

      for (i = length1 - 1; i >= 0; i--) {
        array[customDest + i] = array[customCursor + i];
      }

      array[dest] = tmp[cursor2];
      return;
    }

    var _minGallop = minGallop;

    while (true) {
      var count1 = 0;
      var count2 = 0;
      var exit = false;

      do {
        if (compare(tmp[cursor2], array[cursor1]) < 0) {
          array[dest--] = array[cursor1--];
          count1++;
          count2 = 0;

          if (--length1 === 0) {
            exit = true;
            break;
          }
        } else {
          array[dest--] = tmp[cursor2--];
          count2++;
          count1 = 0;

          if (--length2 === 1) {
            exit = true;
            break;
          }
        }
      } while ((count1 | count2) < _minGallop);

      if (exit) {
        break;
      }

      do {
        count1 = length1 - gallopRight(tmp[cursor2], array, start1, length1, length1 - 1, compare);

        if (count1 !== 0) {
          dest -= count1;
          cursor1 -= count1;
          length1 -= count1;
          customDest = dest + 1;
          customCursor = cursor1 + 1;

          for (i = count1 - 1; i >= 0; i--) {
            array[customDest + i] = array[customCursor + i];
          }

          if (length1 === 0) {
            exit = true;
            break;
          }
        }

        array[dest--] = tmp[cursor2--];

        if (--length2 === 1) {
          exit = true;
          break;
        }

        count2 = length2 - gallopLeft(array[cursor1], tmp, 0, length2, length2 - 1, compare);

        if (count2 !== 0) {
          dest -= count2;
          cursor2 -= count2;
          length2 -= count2;
          customDest = dest + 1;
          customCursor = cursor2 + 1;

          for (i = 0; i < count2; i++) {
            array[customDest + i] = tmp[customCursor + i];
          }

          if (length2 <= 1) {
            exit = true;
            break;
          }
        }

        array[dest--] = array[cursor1--];

        if (--length1 === 0) {
          exit = true;
          break;
        }

        _minGallop--;
      } while (count1 >= DEFAULT_MIN_GALLOPING || count2 >= DEFAULT_MIN_GALLOPING);

      if (exit) {
        break;
      }

      if (_minGallop < 0) {
        _minGallop = 0;
      }

      _minGallop += 2;
    }

    minGallop = _minGallop;

    if (minGallop < 1) {
      minGallop = 1;
    }

    if (length2 === 1) {
      dest -= length1;
      cursor1 -= length1;
      customDest = dest + 1;
      customCursor = cursor1 + 1;

      for (i = length1 - 1; i >= 0; i--) {
        array[customDest + i] = array[customCursor + i];
      }

      array[dest] = tmp[cursor2];
    } else if (length2 === 0) {
      throw new Error(); // throw new Error('mergeHigh preconditions were not respected');
    } else {
      customCursor = dest - (length2 - 1);

      for (i = 0; i < length2; i++) {
        array[customCursor + i] = tmp[i];
      }
    }
  }

  this.mergeRuns = mergeRuns;
  this.forceMergeRuns = forceMergeRuns;
  this.pushRun = pushRun;
}

function sort(array, compare, lo, hi) {
  if (!lo) {
    lo = 0;
  }

  if (!hi) {
    hi = array.length;
  }

  var remaining = hi - lo;

  if (remaining < 2) {
    return;
  }

  var runLength = 0;

  if (remaining < DEFAULT_MIN_MERGE) {
    runLength = makeAscendingRun(array, lo, hi, compare);
    binaryInsertionSort(array, lo, hi, lo + runLength, compare);
    return;
  }

  var ts = new TimSort(array, compare);
  var minRun = minRunLength(remaining);

  do {
    runLength = makeAscendingRun(array, lo, hi, compare);

    if (runLength < minRun) {
      var force = remaining;

      if (force > minRun) {
        force = minRun;
      }

      binaryInsertionSort(array, lo, lo + force, lo + runLength, compare);
      runLength = force;
    }

    ts.pushRun(lo, runLength);
    ts.mergeRuns();
    remaining -= runLength;
    lo += runLength;
  } while (remaining !== 0);

  ts.forceMergeRuns();
}

module.exports = sort;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS90aW1zb3J0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS90aW1zb3J0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9temljY2FyZC9ub2RlLXRpbXNvcnRcbnZhciBERUZBVUxUX01JTl9NRVJHRSA9IDMyO1xudmFyIERFRkFVTFRfTUlOX0dBTExPUElORyA9IDc7XG52YXIgREVGQVVMVF9UTVBfU1RPUkFHRV9MRU5HVEggPSAyNTY7XG5cbmZ1bmN0aW9uIG1pblJ1bkxlbmd0aChuKSB7XG4gIHZhciByID0gMDtcblxuICB3aGlsZSAobiA+PSBERUZBVUxUX01JTl9NRVJHRSkge1xuICAgIHIgfD0gbiAmIDE7XG4gICAgbiA+Pj0gMTtcbiAgfVxuXG4gIHJldHVybiBuICsgcjtcbn1cblxuZnVuY3Rpb24gbWFrZUFzY2VuZGluZ1J1bihhcnJheSwgbG8sIGhpLCBjb21wYXJlKSB7XG4gIHZhciBydW5IaSA9IGxvICsgMTtcblxuICBpZiAocnVuSGkgPT09IGhpKSB7XG4gICAgcmV0dXJuIDE7XG4gIH1cblxuICBpZiAoY29tcGFyZShhcnJheVtydW5IaSsrXSwgYXJyYXlbbG9dKSA8IDApIHtcbiAgICB3aGlsZSAocnVuSGkgPCBoaSAmJiBjb21wYXJlKGFycmF5W3J1bkhpXSwgYXJyYXlbcnVuSGkgLSAxXSkgPCAwKSB7XG4gICAgICBydW5IaSsrO1xuICAgIH1cblxuICAgIHJldmVyc2VSdW4oYXJyYXksIGxvLCBydW5IaSk7XG4gIH0gZWxzZSB7XG4gICAgd2hpbGUgKHJ1bkhpIDwgaGkgJiYgY29tcGFyZShhcnJheVtydW5IaV0sIGFycmF5W3J1bkhpIC0gMV0pID49IDApIHtcbiAgICAgIHJ1bkhpKys7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJ1bkhpIC0gbG87XG59XG5cbmZ1bmN0aW9uIHJldmVyc2VSdW4oYXJyYXksIGxvLCBoaSkge1xuICBoaS0tO1xuXG4gIHdoaWxlIChsbyA8IGhpKSB7XG4gICAgdmFyIHQgPSBhcnJheVtsb107XG4gICAgYXJyYXlbbG8rK10gPSBhcnJheVtoaV07XG4gICAgYXJyYXlbaGktLV0gPSB0O1xuICB9XG59XG5cbmZ1bmN0aW9uIGJpbmFyeUluc2VydGlvblNvcnQoYXJyYXksIGxvLCBoaSwgc3RhcnQsIGNvbXBhcmUpIHtcbiAgaWYgKHN0YXJ0ID09PSBsbykge1xuICAgIHN0YXJ0Kys7XG4gIH1cblxuICBmb3IgKDsgc3RhcnQgPCBoaTsgc3RhcnQrKykge1xuICAgIHZhciBwaXZvdCA9IGFycmF5W3N0YXJ0XTtcbiAgICB2YXIgbGVmdCA9IGxvO1xuICAgIHZhciByaWdodCA9IHN0YXJ0O1xuICAgIHZhciBtaWQ7XG5cbiAgICB3aGlsZSAobGVmdCA8IHJpZ2h0KSB7XG4gICAgICBtaWQgPSBsZWZ0ICsgcmlnaHQgPj4+IDE7XG5cbiAgICAgIGlmIChjb21wYXJlKHBpdm90LCBhcnJheVttaWRdKSA8IDApIHtcbiAgICAgICAgcmlnaHQgPSBtaWQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsZWZ0ID0gbWlkICsgMTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgbiA9IHN0YXJ0IC0gbGVmdDtcblxuICAgIHN3aXRjaCAobikge1xuICAgICAgY2FzZSAzOlxuICAgICAgICBhcnJheVtsZWZ0ICsgM10gPSBhcnJheVtsZWZ0ICsgMl07XG5cbiAgICAgIGNhc2UgMjpcbiAgICAgICAgYXJyYXlbbGVmdCArIDJdID0gYXJyYXlbbGVmdCArIDFdO1xuXG4gICAgICBjYXNlIDE6XG4gICAgICAgIGFycmF5W2xlZnQgKyAxXSA9IGFycmF5W2xlZnRdO1xuICAgICAgICBicmVhaztcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgd2hpbGUgKG4gPiAwKSB7XG4gICAgICAgICAgYXJyYXlbbGVmdCArIG5dID0gYXJyYXlbbGVmdCArIG4gLSAxXTtcbiAgICAgICAgICBuLS07XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIGFycmF5W2xlZnRdID0gcGl2b3Q7XG4gIH1cbn1cblxuZnVuY3Rpb24gZ2FsbG9wTGVmdCh2YWx1ZSwgYXJyYXksIHN0YXJ0LCBsZW5ndGgsIGhpbnQsIGNvbXBhcmUpIHtcbiAgdmFyIGxhc3RPZmZzZXQgPSAwO1xuICB2YXIgbWF4T2Zmc2V0ID0gMDtcbiAgdmFyIG9mZnNldCA9IDE7XG5cbiAgaWYgKGNvbXBhcmUodmFsdWUsIGFycmF5W3N0YXJ0ICsgaGludF0pID4gMCkge1xuICAgIG1heE9mZnNldCA9IGxlbmd0aCAtIGhpbnQ7XG5cbiAgICB3aGlsZSAob2Zmc2V0IDwgbWF4T2Zmc2V0ICYmIGNvbXBhcmUodmFsdWUsIGFycmF5W3N0YXJ0ICsgaGludCArIG9mZnNldF0pID4gMCkge1xuICAgICAgbGFzdE9mZnNldCA9IG9mZnNldDtcbiAgICAgIG9mZnNldCA9IChvZmZzZXQgPDwgMSkgKyAxO1xuXG4gICAgICBpZiAob2Zmc2V0IDw9IDApIHtcbiAgICAgICAgb2Zmc2V0ID0gbWF4T2Zmc2V0O1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChvZmZzZXQgPiBtYXhPZmZzZXQpIHtcbiAgICAgIG9mZnNldCA9IG1heE9mZnNldDtcbiAgICB9XG5cbiAgICBsYXN0T2Zmc2V0ICs9IGhpbnQ7XG4gICAgb2Zmc2V0ICs9IGhpbnQ7XG4gIH0gZWxzZSB7XG4gICAgbWF4T2Zmc2V0ID0gaGludCArIDE7XG5cbiAgICB3aGlsZSAob2Zmc2V0IDwgbWF4T2Zmc2V0ICYmIGNvbXBhcmUodmFsdWUsIGFycmF5W3N0YXJ0ICsgaGludCAtIG9mZnNldF0pIDw9IDApIHtcbiAgICAgIGxhc3RPZmZzZXQgPSBvZmZzZXQ7XG4gICAgICBvZmZzZXQgPSAob2Zmc2V0IDw8IDEpICsgMTtcblxuICAgICAgaWYgKG9mZnNldCA8PSAwKSB7XG4gICAgICAgIG9mZnNldCA9IG1heE9mZnNldDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAob2Zmc2V0ID4gbWF4T2Zmc2V0KSB7XG4gICAgICBvZmZzZXQgPSBtYXhPZmZzZXQ7XG4gICAgfVxuXG4gICAgdmFyIHRtcCA9IGxhc3RPZmZzZXQ7XG4gICAgbGFzdE9mZnNldCA9IGhpbnQgLSBvZmZzZXQ7XG4gICAgb2Zmc2V0ID0gaGludCAtIHRtcDtcbiAgfVxuXG4gIGxhc3RPZmZzZXQrKztcblxuICB3aGlsZSAobGFzdE9mZnNldCA8IG9mZnNldCkge1xuICAgIHZhciBtID0gbGFzdE9mZnNldCArIChvZmZzZXQgLSBsYXN0T2Zmc2V0ID4+PiAxKTtcblxuICAgIGlmIChjb21wYXJlKHZhbHVlLCBhcnJheVtzdGFydCArIG1dKSA+IDApIHtcbiAgICAgIGxhc3RPZmZzZXQgPSBtICsgMTtcbiAgICB9IGVsc2Uge1xuICAgICAgb2Zmc2V0ID0gbTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gb2Zmc2V0O1xufVxuXG5mdW5jdGlvbiBnYWxsb3BSaWdodCh2YWx1ZSwgYXJyYXksIHN0YXJ0LCBsZW5ndGgsIGhpbnQsIGNvbXBhcmUpIHtcbiAgdmFyIGxhc3RPZmZzZXQgPSAwO1xuICB2YXIgbWF4T2Zmc2V0ID0gMDtcbiAgdmFyIG9mZnNldCA9IDE7XG5cbiAgaWYgKGNvbXBhcmUodmFsdWUsIGFycmF5W3N0YXJ0ICsgaGludF0pIDwgMCkge1xuICAgIG1heE9mZnNldCA9IGhpbnQgKyAxO1xuXG4gICAgd2hpbGUgKG9mZnNldCA8IG1heE9mZnNldCAmJiBjb21wYXJlKHZhbHVlLCBhcnJheVtzdGFydCArIGhpbnQgLSBvZmZzZXRdKSA8IDApIHtcbiAgICAgIGxhc3RPZmZzZXQgPSBvZmZzZXQ7XG4gICAgICBvZmZzZXQgPSAob2Zmc2V0IDw8IDEpICsgMTtcblxuICAgICAgaWYgKG9mZnNldCA8PSAwKSB7XG4gICAgICAgIG9mZnNldCA9IG1heE9mZnNldDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAob2Zmc2V0ID4gbWF4T2Zmc2V0KSB7XG4gICAgICBvZmZzZXQgPSBtYXhPZmZzZXQ7XG4gICAgfVxuXG4gICAgdmFyIHRtcCA9IGxhc3RPZmZzZXQ7XG4gICAgbGFzdE9mZnNldCA9IGhpbnQgLSBvZmZzZXQ7XG4gICAgb2Zmc2V0ID0gaGludCAtIHRtcDtcbiAgfSBlbHNlIHtcbiAgICBtYXhPZmZzZXQgPSBsZW5ndGggLSBoaW50O1xuXG4gICAgd2hpbGUgKG9mZnNldCA8IG1heE9mZnNldCAmJiBjb21wYXJlKHZhbHVlLCBhcnJheVtzdGFydCArIGhpbnQgKyBvZmZzZXRdKSA+PSAwKSB7XG4gICAgICBsYXN0T2Zmc2V0ID0gb2Zmc2V0O1xuICAgICAgb2Zmc2V0ID0gKG9mZnNldCA8PCAxKSArIDE7XG5cbiAgICAgIGlmIChvZmZzZXQgPD0gMCkge1xuICAgICAgICBvZmZzZXQgPSBtYXhPZmZzZXQ7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKG9mZnNldCA+IG1heE9mZnNldCkge1xuICAgICAgb2Zmc2V0ID0gbWF4T2Zmc2V0O1xuICAgIH1cblxuICAgIGxhc3RPZmZzZXQgKz0gaGludDtcbiAgICBvZmZzZXQgKz0gaGludDtcbiAgfVxuXG4gIGxhc3RPZmZzZXQrKztcblxuICB3aGlsZSAobGFzdE9mZnNldCA8IG9mZnNldCkge1xuICAgIHZhciBtID0gbGFzdE9mZnNldCArIChvZmZzZXQgLSBsYXN0T2Zmc2V0ID4+PiAxKTtcblxuICAgIGlmIChjb21wYXJlKHZhbHVlLCBhcnJheVtzdGFydCArIG1dKSA8IDApIHtcbiAgICAgIG9mZnNldCA9IG07XG4gICAgfSBlbHNlIHtcbiAgICAgIGxhc3RPZmZzZXQgPSBtICsgMTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gb2Zmc2V0O1xufVxuXG5mdW5jdGlvbiBUaW1Tb3J0KGFycmF5LCBjb21wYXJlKSB7XG4gIHZhciBtaW5HYWxsb3AgPSBERUZBVUxUX01JTl9HQUxMT1BJTkc7XG4gIHZhciBsZW5ndGggPSAwO1xuICB2YXIgdG1wU3RvcmFnZUxlbmd0aCA9IERFRkFVTFRfVE1QX1NUT1JBR0VfTEVOR1RIO1xuICB2YXIgc3RhY2tMZW5ndGggPSAwO1xuICB2YXIgcnVuU3RhcnQ7XG4gIHZhciBydW5MZW5ndGg7XG4gIHZhciBzdGFja1NpemUgPSAwO1xuICBsZW5ndGggPSBhcnJheS5sZW5ndGg7XG5cbiAgaWYgKGxlbmd0aCA8IDIgKiBERUZBVUxUX1RNUF9TVE9SQUdFX0xFTkdUSCkge1xuICAgIHRtcFN0b3JhZ2VMZW5ndGggPSBsZW5ndGggPj4+IDE7XG4gIH1cblxuICB2YXIgdG1wID0gW107XG4gIHN0YWNrTGVuZ3RoID0gbGVuZ3RoIDwgMTIwID8gNSA6IGxlbmd0aCA8IDE1NDIgPyAxMCA6IGxlbmd0aCA8IDExOTE1MSA/IDE5IDogNDA7XG4gIHJ1blN0YXJ0ID0gW107XG4gIHJ1bkxlbmd0aCA9IFtdO1xuXG4gIGZ1bmN0aW9uIHB1c2hSdW4oX3J1blN0YXJ0LCBfcnVuTGVuZ3RoKSB7XG4gICAgcnVuU3RhcnRbc3RhY2tTaXplXSA9IF9ydW5TdGFydDtcbiAgICBydW5MZW5ndGhbc3RhY2tTaXplXSA9IF9ydW5MZW5ndGg7XG4gICAgc3RhY2tTaXplICs9IDE7XG4gIH1cblxuICBmdW5jdGlvbiBtZXJnZVJ1bnMoKSB7XG4gICAgd2hpbGUgKHN0YWNrU2l6ZSA+IDEpIHtcbiAgICAgIHZhciBuID0gc3RhY2tTaXplIC0gMjtcblxuICAgICAgaWYgKG4gPj0gMSAmJiBydW5MZW5ndGhbbiAtIDFdIDw9IHJ1bkxlbmd0aFtuXSArIHJ1bkxlbmd0aFtuICsgMV0gfHwgbiA+PSAyICYmIHJ1bkxlbmd0aFtuIC0gMl0gPD0gcnVuTGVuZ3RoW25dICsgcnVuTGVuZ3RoW24gLSAxXSkge1xuICAgICAgICBpZiAocnVuTGVuZ3RoW24gLSAxXSA8IHJ1bkxlbmd0aFtuICsgMV0pIHtcbiAgICAgICAgICBuLS07XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAocnVuTGVuZ3RoW25dID4gcnVuTGVuZ3RoW24gKyAxXSkge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgbWVyZ2VBdChuKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBmb3JjZU1lcmdlUnVucygpIHtcbiAgICB3aGlsZSAoc3RhY2tTaXplID4gMSkge1xuICAgICAgdmFyIG4gPSBzdGFja1NpemUgLSAyO1xuXG4gICAgICBpZiAobiA+IDAgJiYgcnVuTGVuZ3RoW24gLSAxXSA8IHJ1bkxlbmd0aFtuICsgMV0pIHtcbiAgICAgICAgbi0tO1xuICAgICAgfVxuXG4gICAgICBtZXJnZUF0KG4pO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIG1lcmdlQXQoaSkge1xuICAgIHZhciBzdGFydDEgPSBydW5TdGFydFtpXTtcbiAgICB2YXIgbGVuZ3RoMSA9IHJ1bkxlbmd0aFtpXTtcbiAgICB2YXIgc3RhcnQyID0gcnVuU3RhcnRbaSArIDFdO1xuICAgIHZhciBsZW5ndGgyID0gcnVuTGVuZ3RoW2kgKyAxXTtcbiAgICBydW5MZW5ndGhbaV0gPSBsZW5ndGgxICsgbGVuZ3RoMjtcblxuICAgIGlmIChpID09PSBzdGFja1NpemUgLSAzKSB7XG4gICAgICBydW5TdGFydFtpICsgMV0gPSBydW5TdGFydFtpICsgMl07XG4gICAgICBydW5MZW5ndGhbaSArIDFdID0gcnVuTGVuZ3RoW2kgKyAyXTtcbiAgICB9XG5cbiAgICBzdGFja1NpemUtLTtcbiAgICB2YXIgayA9IGdhbGxvcFJpZ2h0KGFycmF5W3N0YXJ0Ml0sIGFycmF5LCBzdGFydDEsIGxlbmd0aDEsIDAsIGNvbXBhcmUpO1xuICAgIHN0YXJ0MSArPSBrO1xuICAgIGxlbmd0aDEgLT0gaztcblxuICAgIGlmIChsZW5ndGgxID09PSAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgbGVuZ3RoMiA9IGdhbGxvcExlZnQoYXJyYXlbc3RhcnQxICsgbGVuZ3RoMSAtIDFdLCBhcnJheSwgc3RhcnQyLCBsZW5ndGgyLCBsZW5ndGgyIC0gMSwgY29tcGFyZSk7XG5cbiAgICBpZiAobGVuZ3RoMiA9PT0gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChsZW5ndGgxIDw9IGxlbmd0aDIpIHtcbiAgICAgIG1lcmdlTG93KHN0YXJ0MSwgbGVuZ3RoMSwgc3RhcnQyLCBsZW5ndGgyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbWVyZ2VIaWdoKHN0YXJ0MSwgbGVuZ3RoMSwgc3RhcnQyLCBsZW5ndGgyKTtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBtZXJnZUxvdyhzdGFydDEsIGxlbmd0aDEsIHN0YXJ0MiwgbGVuZ3RoMikge1xuICAgIHZhciBpID0gMDtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGgxOyBpKyspIHtcbiAgICAgIHRtcFtpXSA9IGFycmF5W3N0YXJ0MSArIGldO1xuICAgIH1cblxuICAgIHZhciBjdXJzb3IxID0gMDtcbiAgICB2YXIgY3Vyc29yMiA9IHN0YXJ0MjtcbiAgICB2YXIgZGVzdCA9IHN0YXJ0MTtcbiAgICBhcnJheVtkZXN0KytdID0gYXJyYXlbY3Vyc29yMisrXTtcblxuICAgIGlmICgtLWxlbmd0aDIgPT09IDApIHtcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGgxOyBpKyspIHtcbiAgICAgICAgYXJyYXlbZGVzdCArIGldID0gdG1wW2N1cnNvcjEgKyBpXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChsZW5ndGgxID09PSAxKSB7XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoMjsgaSsrKSB7XG4gICAgICAgIGFycmF5W2Rlc3QgKyBpXSA9IGFycmF5W2N1cnNvcjIgKyBpXTtcbiAgICAgIH1cblxuICAgICAgYXJyYXlbZGVzdCArIGxlbmd0aDJdID0gdG1wW2N1cnNvcjFdO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBfbWluR2FsbG9wID0gbWluR2FsbG9wO1xuICAgIHZhciBjb3VudDEsIGNvdW50MiwgZXhpdDtcblxuICAgIHdoaWxlICgxKSB7XG4gICAgICBjb3VudDEgPSAwO1xuICAgICAgY291bnQyID0gMDtcbiAgICAgIGV4aXQgPSBmYWxzZTtcblxuICAgICAgZG8ge1xuICAgICAgICBpZiAoY29tcGFyZShhcnJheVtjdXJzb3IyXSwgdG1wW2N1cnNvcjFdKSA8IDApIHtcbiAgICAgICAgICBhcnJheVtkZXN0KytdID0gYXJyYXlbY3Vyc29yMisrXTtcbiAgICAgICAgICBjb3VudDIrKztcbiAgICAgICAgICBjb3VudDEgPSAwO1xuXG4gICAgICAgICAgaWYgKC0tbGVuZ3RoMiA9PT0gMCkge1xuICAgICAgICAgICAgZXhpdCA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYXJyYXlbZGVzdCsrXSA9IHRtcFtjdXJzb3IxKytdO1xuICAgICAgICAgIGNvdW50MSsrO1xuICAgICAgICAgIGNvdW50MiA9IDA7XG5cbiAgICAgICAgICBpZiAoLS1sZW5ndGgxID09PSAxKSB7XG4gICAgICAgICAgICBleGl0ID0gdHJ1ZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSB3aGlsZSAoKGNvdW50MSB8IGNvdW50MikgPCBfbWluR2FsbG9wKTtcblxuICAgICAgaWYgKGV4aXQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGRvIHtcbiAgICAgICAgY291bnQxID0gZ2FsbG9wUmlnaHQoYXJyYXlbY3Vyc29yMl0sIHRtcCwgY3Vyc29yMSwgbGVuZ3RoMSwgMCwgY29tcGFyZSk7XG5cbiAgICAgICAgaWYgKGNvdW50MSAhPT0gMCkge1xuICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBjb3VudDE7IGkrKykge1xuICAgICAgICAgICAgYXJyYXlbZGVzdCArIGldID0gdG1wW2N1cnNvcjEgKyBpXTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBkZXN0ICs9IGNvdW50MTtcbiAgICAgICAgICBjdXJzb3IxICs9IGNvdW50MTtcbiAgICAgICAgICBsZW5ndGgxIC09IGNvdW50MTtcblxuICAgICAgICAgIGlmIChsZW5ndGgxIDw9IDEpIHtcbiAgICAgICAgICAgIGV4aXQgPSB0cnVlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgYXJyYXlbZGVzdCsrXSA9IGFycmF5W2N1cnNvcjIrK107XG5cbiAgICAgICAgaWYgKC0tbGVuZ3RoMiA9PT0gMCkge1xuICAgICAgICAgIGV4aXQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgY291bnQyID0gZ2FsbG9wTGVmdCh0bXBbY3Vyc29yMV0sIGFycmF5LCBjdXJzb3IyLCBsZW5ndGgyLCAwLCBjb21wYXJlKTtcblxuICAgICAgICBpZiAoY291bnQyICE9PSAwKSB7XG4gICAgICAgICAgZm9yIChpID0gMDsgaSA8IGNvdW50MjsgaSsrKSB7XG4gICAgICAgICAgICBhcnJheVtkZXN0ICsgaV0gPSBhcnJheVtjdXJzb3IyICsgaV07XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgZGVzdCArPSBjb3VudDI7XG4gICAgICAgICAgY3Vyc29yMiArPSBjb3VudDI7XG4gICAgICAgICAgbGVuZ3RoMiAtPSBjb3VudDI7XG5cbiAgICAgICAgICBpZiAobGVuZ3RoMiA9PT0gMCkge1xuICAgICAgICAgICAgZXhpdCA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBhcnJheVtkZXN0KytdID0gdG1wW2N1cnNvcjErK107XG5cbiAgICAgICAgaWYgKC0tbGVuZ3RoMSA9PT0gMSkge1xuICAgICAgICAgIGV4aXQgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgX21pbkdhbGxvcC0tO1xuICAgICAgfSB3aGlsZSAoY291bnQxID49IERFRkFVTFRfTUlOX0dBTExPUElORyB8fCBjb3VudDIgPj0gREVGQVVMVF9NSU5fR0FMTE9QSU5HKTtcblxuICAgICAgaWYgKGV4aXQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGlmIChfbWluR2FsbG9wIDwgMCkge1xuICAgICAgICBfbWluR2FsbG9wID0gMDtcbiAgICAgIH1cblxuICAgICAgX21pbkdhbGxvcCArPSAyO1xuICAgIH1cblxuICAgIG1pbkdhbGxvcCA9IF9taW5HYWxsb3A7XG4gICAgbWluR2FsbG9wIDwgMSAmJiAobWluR2FsbG9wID0gMSk7XG5cbiAgICBpZiAobGVuZ3RoMSA9PT0gMSkge1xuICAgICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDI7IGkrKykge1xuICAgICAgICBhcnJheVtkZXN0ICsgaV0gPSBhcnJheVtjdXJzb3IyICsgaV07XG4gICAgICB9XG5cbiAgICAgIGFycmF5W2Rlc3QgKyBsZW5ndGgyXSA9IHRtcFtjdXJzb3IxXTtcbiAgICB9IGVsc2UgaWYgKGxlbmd0aDEgPT09IDApIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcigpOyAvLyB0aHJvdyBuZXcgRXJyb3IoJ21lcmdlTG93IHByZWNvbmRpdGlvbnMgd2VyZSBub3QgcmVzcGVjdGVkJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW5ndGgxOyBpKyspIHtcbiAgICAgICAgYXJyYXlbZGVzdCArIGldID0gdG1wW2N1cnNvcjEgKyBpXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBtZXJnZUhpZ2goc3RhcnQxLCBsZW5ndGgxLCBzdGFydDIsIGxlbmd0aDIpIHtcbiAgICB2YXIgaSA9IDA7XG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoMjsgaSsrKSB7XG4gICAgICB0bXBbaV0gPSBhcnJheVtzdGFydDIgKyBpXTtcbiAgICB9XG5cbiAgICB2YXIgY3Vyc29yMSA9IHN0YXJ0MSArIGxlbmd0aDEgLSAxO1xuICAgIHZhciBjdXJzb3IyID0gbGVuZ3RoMiAtIDE7XG4gICAgdmFyIGRlc3QgPSBzdGFydDIgKyBsZW5ndGgyIC0gMTtcbiAgICB2YXIgY3VzdG9tQ3Vyc29yID0gMDtcbiAgICB2YXIgY3VzdG9tRGVzdCA9IDA7XG4gICAgYXJyYXlbZGVzdC0tXSA9IGFycmF5W2N1cnNvcjEtLV07XG5cbiAgICBpZiAoLS1sZW5ndGgxID09PSAwKSB7XG4gICAgICBjdXN0b21DdXJzb3IgPSBkZXN0IC0gKGxlbmd0aDIgLSAxKTtcblxuICAgICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDI7IGkrKykge1xuICAgICAgICBhcnJheVtjdXN0b21DdXJzb3IgKyBpXSA9IHRtcFtpXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChsZW5ndGgyID09PSAxKSB7XG4gICAgICBkZXN0IC09IGxlbmd0aDE7XG4gICAgICBjdXJzb3IxIC09IGxlbmd0aDE7XG4gICAgICBjdXN0b21EZXN0ID0gZGVzdCArIDE7XG4gICAgICBjdXN0b21DdXJzb3IgPSBjdXJzb3IxICsgMTtcblxuICAgICAgZm9yIChpID0gbGVuZ3RoMSAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgIGFycmF5W2N1c3RvbURlc3QgKyBpXSA9IGFycmF5W2N1c3RvbUN1cnNvciArIGldO1xuICAgICAgfVxuXG4gICAgICBhcnJheVtkZXN0XSA9IHRtcFtjdXJzb3IyXTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX21pbkdhbGxvcCA9IG1pbkdhbGxvcDtcblxuICAgIHdoaWxlICh0cnVlKSB7XG4gICAgICB2YXIgY291bnQxID0gMDtcbiAgICAgIHZhciBjb3VudDIgPSAwO1xuICAgICAgdmFyIGV4aXQgPSBmYWxzZTtcblxuICAgICAgZG8ge1xuICAgICAgICBpZiAoY29tcGFyZSh0bXBbY3Vyc29yMl0sIGFycmF5W2N1cnNvcjFdKSA8IDApIHtcbiAgICAgICAgICBhcnJheVtkZXN0LS1dID0gYXJyYXlbY3Vyc29yMS0tXTtcbiAgICAgICAgICBjb3VudDErKztcbiAgICAgICAgICBjb3VudDIgPSAwO1xuXG4gICAgICAgICAgaWYgKC0tbGVuZ3RoMSA9PT0gMCkge1xuICAgICAgICAgICAgZXhpdCA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYXJyYXlbZGVzdC0tXSA9IHRtcFtjdXJzb3IyLS1dO1xuICAgICAgICAgIGNvdW50MisrO1xuICAgICAgICAgIGNvdW50MSA9IDA7XG5cbiAgICAgICAgICBpZiAoLS1sZW5ndGgyID09PSAxKSB7XG4gICAgICAgICAgICBleGl0ID0gdHJ1ZTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSB3aGlsZSAoKGNvdW50MSB8IGNvdW50MikgPCBfbWluR2FsbG9wKTtcblxuICAgICAgaWYgKGV4aXQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGRvIHtcbiAgICAgICAgY291bnQxID0gbGVuZ3RoMSAtIGdhbGxvcFJpZ2h0KHRtcFtjdXJzb3IyXSwgYXJyYXksIHN0YXJ0MSwgbGVuZ3RoMSwgbGVuZ3RoMSAtIDEsIGNvbXBhcmUpO1xuXG4gICAgICAgIGlmIChjb3VudDEgIT09IDApIHtcbiAgICAgICAgICBkZXN0IC09IGNvdW50MTtcbiAgICAgICAgICBjdXJzb3IxIC09IGNvdW50MTtcbiAgICAgICAgICBsZW5ndGgxIC09IGNvdW50MTtcbiAgICAgICAgICBjdXN0b21EZXN0ID0gZGVzdCArIDE7XG4gICAgICAgICAgY3VzdG9tQ3Vyc29yID0gY3Vyc29yMSArIDE7XG5cbiAgICAgICAgICBmb3IgKGkgPSBjb3VudDEgLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgICAgYXJyYXlbY3VzdG9tRGVzdCArIGldID0gYXJyYXlbY3VzdG9tQ3Vyc29yICsgaV07XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGxlbmd0aDEgPT09IDApIHtcbiAgICAgICAgICAgIGV4aXQgPSB0cnVlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgYXJyYXlbZGVzdC0tXSA9IHRtcFtjdXJzb3IyLS1dO1xuXG4gICAgICAgIGlmICgtLWxlbmd0aDIgPT09IDEpIHtcbiAgICAgICAgICBleGl0ID0gdHJ1ZTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIGNvdW50MiA9IGxlbmd0aDIgLSBnYWxsb3BMZWZ0KGFycmF5W2N1cnNvcjFdLCB0bXAsIDAsIGxlbmd0aDIsIGxlbmd0aDIgLSAxLCBjb21wYXJlKTtcblxuICAgICAgICBpZiAoY291bnQyICE9PSAwKSB7XG4gICAgICAgICAgZGVzdCAtPSBjb3VudDI7XG4gICAgICAgICAgY3Vyc29yMiAtPSBjb3VudDI7XG4gICAgICAgICAgbGVuZ3RoMiAtPSBjb3VudDI7XG4gICAgICAgICAgY3VzdG9tRGVzdCA9IGRlc3QgKyAxO1xuICAgICAgICAgIGN1c3RvbUN1cnNvciA9IGN1cnNvcjIgKyAxO1xuXG4gICAgICAgICAgZm9yIChpID0gMDsgaSA8IGNvdW50MjsgaSsrKSB7XG4gICAgICAgICAgICBhcnJheVtjdXN0b21EZXN0ICsgaV0gPSB0bXBbY3VzdG9tQ3Vyc29yICsgaV07XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGxlbmd0aDIgPD0gMSkge1xuICAgICAgICAgICAgZXhpdCA9IHRydWU7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBhcnJheVtkZXN0LS1dID0gYXJyYXlbY3Vyc29yMS0tXTtcblxuICAgICAgICBpZiAoLS1sZW5ndGgxID09PSAwKSB7XG4gICAgICAgICAgZXhpdCA9IHRydWU7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICBfbWluR2FsbG9wLS07XG4gICAgICB9IHdoaWxlIChjb3VudDEgPj0gREVGQVVMVF9NSU5fR0FMTE9QSU5HIHx8IGNvdW50MiA+PSBERUZBVUxUX01JTl9HQUxMT1BJTkcpO1xuXG4gICAgICBpZiAoZXhpdCkge1xuICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgaWYgKF9taW5HYWxsb3AgPCAwKSB7XG4gICAgICAgIF9taW5HYWxsb3AgPSAwO1xuICAgICAgfVxuXG4gICAgICBfbWluR2FsbG9wICs9IDI7XG4gICAgfVxuXG4gICAgbWluR2FsbG9wID0gX21pbkdhbGxvcDtcblxuICAgIGlmIChtaW5HYWxsb3AgPCAxKSB7XG4gICAgICBtaW5HYWxsb3AgPSAxO1xuICAgIH1cblxuICAgIGlmIChsZW5ndGgyID09PSAxKSB7XG4gICAgICBkZXN0IC09IGxlbmd0aDE7XG4gICAgICBjdXJzb3IxIC09IGxlbmd0aDE7XG4gICAgICBjdXN0b21EZXN0ID0gZGVzdCArIDE7XG4gICAgICBjdXN0b21DdXJzb3IgPSBjdXJzb3IxICsgMTtcblxuICAgICAgZm9yIChpID0gbGVuZ3RoMSAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgIGFycmF5W2N1c3RvbURlc3QgKyBpXSA9IGFycmF5W2N1c3RvbUN1cnNvciArIGldO1xuICAgICAgfVxuXG4gICAgICBhcnJheVtkZXN0XSA9IHRtcFtjdXJzb3IyXTtcbiAgICB9IGVsc2UgaWYgKGxlbmd0aDIgPT09IDApIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcigpOyAvLyB0aHJvdyBuZXcgRXJyb3IoJ21lcmdlSGlnaCBwcmVjb25kaXRpb25zIHdlcmUgbm90IHJlc3BlY3RlZCcpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjdXN0b21DdXJzb3IgPSBkZXN0IC0gKGxlbmd0aDIgLSAxKTtcblxuICAgICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDI7IGkrKykge1xuICAgICAgICBhcnJheVtjdXN0b21DdXJzb3IgKyBpXSA9IHRtcFtpXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICB0aGlzLm1lcmdlUnVucyA9IG1lcmdlUnVucztcbiAgdGhpcy5mb3JjZU1lcmdlUnVucyA9IGZvcmNlTWVyZ2VSdW5zO1xuICB0aGlzLnB1c2hSdW4gPSBwdXNoUnVuO1xufVxuXG5mdW5jdGlvbiBzb3J0KGFycmF5LCBjb21wYXJlLCBsbywgaGkpIHtcbiAgaWYgKCFsbykge1xuICAgIGxvID0gMDtcbiAgfVxuXG4gIGlmICghaGkpIHtcbiAgICBoaSA9IGFycmF5Lmxlbmd0aDtcbiAgfVxuXG4gIHZhciByZW1haW5pbmcgPSBoaSAtIGxvO1xuXG4gIGlmIChyZW1haW5pbmcgPCAyKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHJ1bkxlbmd0aCA9IDA7XG5cbiAgaWYgKHJlbWFpbmluZyA8IERFRkFVTFRfTUlOX01FUkdFKSB7XG4gICAgcnVuTGVuZ3RoID0gbWFrZUFzY2VuZGluZ1J1bihhcnJheSwgbG8sIGhpLCBjb21wYXJlKTtcbiAgICBiaW5hcnlJbnNlcnRpb25Tb3J0KGFycmF5LCBsbywgaGksIGxvICsgcnVuTGVuZ3RoLCBjb21wYXJlKTtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgdHMgPSBuZXcgVGltU29ydChhcnJheSwgY29tcGFyZSk7XG4gIHZhciBtaW5SdW4gPSBtaW5SdW5MZW5ndGgocmVtYWluaW5nKTtcblxuICBkbyB7XG4gICAgcnVuTGVuZ3RoID0gbWFrZUFzY2VuZGluZ1J1bihhcnJheSwgbG8sIGhpLCBjb21wYXJlKTtcblxuICAgIGlmIChydW5MZW5ndGggPCBtaW5SdW4pIHtcbiAgICAgIHZhciBmb3JjZSA9IHJlbWFpbmluZztcblxuICAgICAgaWYgKGZvcmNlID4gbWluUnVuKSB7XG4gICAgICAgIGZvcmNlID0gbWluUnVuO1xuICAgICAgfVxuXG4gICAgICBiaW5hcnlJbnNlcnRpb25Tb3J0KGFycmF5LCBsbywgbG8gKyBmb3JjZSwgbG8gKyBydW5MZW5ndGgsIGNvbXBhcmUpO1xuICAgICAgcnVuTGVuZ3RoID0gZm9yY2U7XG4gICAgfVxuXG4gICAgdHMucHVzaFJ1bihsbywgcnVuTGVuZ3RoKTtcbiAgICB0cy5tZXJnZVJ1bnMoKTtcbiAgICByZW1haW5pbmcgLT0gcnVuTGVuZ3RoO1xuICAgIGxvICs9IHJ1bkxlbmd0aDtcbiAgfSB3aGlsZSAocmVtYWluaW5nICE9PSAwKTtcblxuICB0cy5mb3JjZU1lcmdlUnVucygpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHNvcnQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQUNBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/timsort.js
