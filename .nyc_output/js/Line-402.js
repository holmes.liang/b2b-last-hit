var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var _subPixelOptimize = __webpack_require__(/*! ../helper/subPixelOptimize */ "./node_modules/zrender/lib/graphic/helper/subPixelOptimize.js");

var subPixelOptimizeLine = _subPixelOptimize.subPixelOptimizeLine;
/**
 * 直线
 * @module zrender/graphic/shape/Line
 */
// Avoid create repeatly.

var subPixelOptimizeOutputShape = {};

var _default = Path.extend({
  type: 'line',
  shape: {
    // Start point
    x1: 0,
    y1: 0,
    // End point
    x2: 0,
    y2: 0,
    percent: 1
  },
  style: {
    stroke: '#000',
    fill: null
  },
  buildPath: function buildPath(ctx, shape) {
    var x1;
    var y1;
    var x2;
    var y2;

    if (this.subPixelOptimize) {
      subPixelOptimizeLine(subPixelOptimizeOutputShape, shape, this.style);
      x1 = subPixelOptimizeOutputShape.x1;
      y1 = subPixelOptimizeOutputShape.y1;
      x2 = subPixelOptimizeOutputShape.x2;
      y2 = subPixelOptimizeOutputShape.y2;
    } else {
      x1 = shape.x1;
      y1 = shape.y1;
      x2 = shape.x2;
      y2 = shape.y2;
    }

    var percent = shape.percent;

    if (percent === 0) {
      return;
    }

    ctx.moveTo(x1, y1);

    if (percent < 1) {
      x2 = x1 * (1 - percent) + x2 * percent;
      y2 = y1 * (1 - percent) + y2 * percent;
    }

    ctx.lineTo(x2, y2);
  },

  /**
   * Get point at percent
   * @param  {number} percent
   * @return {Array.<number>}
   */
  pointAt: function pointAt(p) {
    var shape = this.shape;
    return [shape.x1 * (1 - p) + shape.x2 * p, shape.y1 * (1 - p) + shape.y2 * p];
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9MaW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9MaW5lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL1BhdGhcIik7XG5cbnZhciBfc3ViUGl4ZWxPcHRpbWl6ZSA9IHJlcXVpcmUoXCIuLi9oZWxwZXIvc3ViUGl4ZWxPcHRpbWl6ZVwiKTtcblxudmFyIHN1YlBpeGVsT3B0aW1pemVMaW5lID0gX3N1YlBpeGVsT3B0aW1pemUuc3ViUGl4ZWxPcHRpbWl6ZUxpbmU7XG5cbi8qKlxuICog55u057q/XG4gKiBAbW9kdWxlIHpyZW5kZXIvZ3JhcGhpYy9zaGFwZS9MaW5lXG4gKi9cbi8vIEF2b2lkIGNyZWF0ZSByZXBlYXRseS5cbnZhciBzdWJQaXhlbE9wdGltaXplT3V0cHV0U2hhcGUgPSB7fTtcblxudmFyIF9kZWZhdWx0ID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAnbGluZScsXG4gIHNoYXBlOiB7XG4gICAgLy8gU3RhcnQgcG9pbnRcbiAgICB4MTogMCxcbiAgICB5MTogMCxcbiAgICAvLyBFbmQgcG9pbnRcbiAgICB4MjogMCxcbiAgICB5MjogMCxcbiAgICBwZXJjZW50OiAxXG4gIH0sXG4gIHN0eWxlOiB7XG4gICAgc3Ryb2tlOiAnIzAwMCcsXG4gICAgZmlsbDogbnVsbFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChjdHgsIHNoYXBlKSB7XG4gICAgdmFyIHgxO1xuICAgIHZhciB5MTtcbiAgICB2YXIgeDI7XG4gICAgdmFyIHkyO1xuXG4gICAgaWYgKHRoaXMuc3ViUGl4ZWxPcHRpbWl6ZSkge1xuICAgICAgc3ViUGl4ZWxPcHRpbWl6ZUxpbmUoc3ViUGl4ZWxPcHRpbWl6ZU91dHB1dFNoYXBlLCBzaGFwZSwgdGhpcy5zdHlsZSk7XG4gICAgICB4MSA9IHN1YlBpeGVsT3B0aW1pemVPdXRwdXRTaGFwZS54MTtcbiAgICAgIHkxID0gc3ViUGl4ZWxPcHRpbWl6ZU91dHB1dFNoYXBlLnkxO1xuICAgICAgeDIgPSBzdWJQaXhlbE9wdGltaXplT3V0cHV0U2hhcGUueDI7XG4gICAgICB5MiA9IHN1YlBpeGVsT3B0aW1pemVPdXRwdXRTaGFwZS55MjtcbiAgICB9IGVsc2Uge1xuICAgICAgeDEgPSBzaGFwZS54MTtcbiAgICAgIHkxID0gc2hhcGUueTE7XG4gICAgICB4MiA9IHNoYXBlLngyO1xuICAgICAgeTIgPSBzaGFwZS55MjtcbiAgICB9XG5cbiAgICB2YXIgcGVyY2VudCA9IHNoYXBlLnBlcmNlbnQ7XG5cbiAgICBpZiAocGVyY2VudCA9PT0gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGN0eC5tb3ZlVG8oeDEsIHkxKTtcblxuICAgIGlmIChwZXJjZW50IDwgMSkge1xuICAgICAgeDIgPSB4MSAqICgxIC0gcGVyY2VudCkgKyB4MiAqIHBlcmNlbnQ7XG4gICAgICB5MiA9IHkxICogKDEgLSBwZXJjZW50KSArIHkyICogcGVyY2VudDtcbiAgICB9XG5cbiAgICBjdHgubGluZVRvKHgyLCB5Mik7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBwb2ludCBhdCBwZXJjZW50XG4gICAqIEBwYXJhbSAge251bWJlcn0gcGVyY2VudFxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIHBvaW50QXQ6IGZ1bmN0aW9uIChwKSB7XG4gICAgdmFyIHNoYXBlID0gdGhpcy5zaGFwZTtcbiAgICByZXR1cm4gW3NoYXBlLngxICogKDEgLSBwKSArIHNoYXBlLngyICogcCwgc2hhcGUueTEgKiAoMSAtIHApICsgc2hhcGUueTIgKiBwXTtcbiAgfVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQTFEQTtBQUNBO0FBNERBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Line.js
