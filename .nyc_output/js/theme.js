

Object.defineProperty(exports, "__esModule", {
  value: true
}); // ==============================
// THEME
// ==============================

var theme = {}; // container

theme.container = {
  background: 'rgba(0, 0, 0, 0.8)',
  gutter: {
    horizontal: 10,
    vertical: 10
  },
  zIndex: 2001
}; // header

theme.header = {
  height: 40
};
theme.close = {
  fill: 'white'
}; // footer

theme.footer = {
  color: 'white',
  count: {
    color: 'rgba(255, 255, 255, 0.75)',
    fontSize: '0.85em'
  },
  height: 40,
  gutter: {
    horizontal: 0,
    vertical: 5
  }
}; // thumbnails

theme.thumbnail = {
  activeBorderColor: 'white',
  size: 50,
  gutter: 2
}; // arrow

theme.arrow = {
  background: 'none',
  fill: 'white',
  height: 120
};
exports.default = theme;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi90aGVtZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvdGhlbWUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4vLyBUSEVNRVxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cbnZhciB0aGVtZSA9IHt9O1xuXG4vLyBjb250YWluZXJcbnRoZW1lLmNvbnRhaW5lciA9IHtcblx0YmFja2dyb3VuZDogJ3JnYmEoMCwgMCwgMCwgMC44KScsXG5cdGd1dHRlcjoge1xuXHRcdGhvcml6b250YWw6IDEwLFxuXHRcdHZlcnRpY2FsOiAxMFxuXHR9LFxuXHR6SW5kZXg6IDIwMDFcbn07XG5cbi8vIGhlYWRlclxudGhlbWUuaGVhZGVyID0ge1xuXHRoZWlnaHQ6IDQwXG59O1xudGhlbWUuY2xvc2UgPSB7XG5cdGZpbGw6ICd3aGl0ZSdcbn07XG5cbi8vIGZvb3RlclxudGhlbWUuZm9vdGVyID0ge1xuXHRjb2xvcjogJ3doaXRlJyxcblx0Y291bnQ6IHtcblx0XHRjb2xvcjogJ3JnYmEoMjU1LCAyNTUsIDI1NSwgMC43NSknLFxuXHRcdGZvbnRTaXplOiAnMC44NWVtJ1xuXHR9LFxuXHRoZWlnaHQ6IDQwLFxuXHRndXR0ZXI6IHtcblx0XHRob3Jpem9udGFsOiAwLFxuXHRcdHZlcnRpY2FsOiA1XG5cdH1cbn07XG5cbi8vIHRodW1ibmFpbHNcbnRoZW1lLnRodW1ibmFpbCA9IHtcblx0YWN0aXZlQm9yZGVyQ29sb3I6ICd3aGl0ZScsXG5cdHNpemU6IDUwLFxuXHRndXR0ZXI6IDJcbn07XG5cbi8vIGFycm93XG50aGVtZS5hcnJvdyA9IHtcblx0YmFja2dyb3VuZDogJ25vbmUnLFxuXHRmaWxsOiAnd2hpdGUnLFxuXHRoZWlnaHQ6IDEyMFxufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gdGhlbWU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBTkE7QUFDQTtBQVNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQVBBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/theme.js
