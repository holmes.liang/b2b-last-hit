/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var textContain = __webpack_require__(/*! zrender/lib/contain/text */ "./node_modules/zrender/lib/contain/text.js");

var formatUtil = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");

var matrix = __webpack_require__(/*! zrender/lib/core/matrix */ "./node_modules/zrender/lib/core/matrix.js");

var axisHelper = __webpack_require__(/*! ../../coord/axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var AxisBuilder = __webpack_require__(/*! ../axis/AxisBuilder */ "./node_modules/echarts/lib/component/axis/AxisBuilder.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @param {module:echarts/model/Model} axisPointerModel
 */


function buildElStyle(axisPointerModel) {
  var axisPointerType = axisPointerModel.get('type');
  var styleModel = axisPointerModel.getModel(axisPointerType + 'Style');
  var style;

  if (axisPointerType === 'line') {
    style = styleModel.getLineStyle();
    style.fill = null;
  } else if (axisPointerType === 'shadow') {
    style = styleModel.getAreaStyle();
    style.stroke = null;
  }

  return style;
}
/**
 * @param {Function} labelPos {align, verticalAlign, position}
 */


function buildLabelElOption(elOption, axisModel, axisPointerModel, api, labelPos) {
  var value = axisPointerModel.get('value');
  var text = getValueLabel(value, axisModel.axis, axisModel.ecModel, axisPointerModel.get('seriesDataIndices'), {
    precision: axisPointerModel.get('label.precision'),
    formatter: axisPointerModel.get('label.formatter')
  });
  var labelModel = axisPointerModel.getModel('label');
  var paddings = formatUtil.normalizeCssArray(labelModel.get('padding') || 0);
  var font = labelModel.getFont();
  var textRect = textContain.getBoundingRect(text, font);
  var position = labelPos.position;
  var width = textRect.width + paddings[1] + paddings[3];
  var height = textRect.height + paddings[0] + paddings[2]; // Adjust by align.

  var align = labelPos.align;
  align === 'right' && (position[0] -= width);
  align === 'center' && (position[0] -= width / 2);
  var verticalAlign = labelPos.verticalAlign;
  verticalAlign === 'bottom' && (position[1] -= height);
  verticalAlign === 'middle' && (position[1] -= height / 2); // Not overflow ec container

  confineInContainer(position, width, height, api);
  var bgColor = labelModel.get('backgroundColor');

  if (!bgColor || bgColor === 'auto') {
    bgColor = axisModel.get('axisLine.lineStyle.color');
  }

  elOption.label = {
    shape: {
      x: 0,
      y: 0,
      width: width,
      height: height,
      r: labelModel.get('borderRadius')
    },
    position: position.slice(),
    // TODO: rich
    style: {
      text: text,
      textFont: font,
      textFill: labelModel.getTextColor(),
      textPosition: 'inside',
      fill: bgColor,
      stroke: labelModel.get('borderColor') || 'transparent',
      lineWidth: labelModel.get('borderWidth') || 0,
      shadowBlur: labelModel.get('shadowBlur'),
      shadowColor: labelModel.get('shadowColor'),
      shadowOffsetX: labelModel.get('shadowOffsetX'),
      shadowOffsetY: labelModel.get('shadowOffsetY')
    },
    // Lable should be over axisPointer.
    z2: 10
  };
} // Do not overflow ec container


function confineInContainer(position, width, height, api) {
  var viewWidth = api.getWidth();
  var viewHeight = api.getHeight();
  position[0] = Math.min(position[0] + width, viewWidth) - width;
  position[1] = Math.min(position[1] + height, viewHeight) - height;
  position[0] = Math.max(position[0], 0);
  position[1] = Math.max(position[1], 0);
}
/**
 * @param {number} value
 * @param {module:echarts/coord/Axis} axis
 * @param {module:echarts/model/Global} ecModel
 * @param {Object} opt
 * @param {Array.<Object>} seriesDataIndices
 * @param {number|string} opt.precision 'auto' or a number
 * @param {string|Function} opt.formatter label formatter
 */


function getValueLabel(value, axis, ecModel, seriesDataIndices, opt) {
  value = axis.scale.parse(value);
  var text = axis.scale.getLabel( // If `precision` is set, width can be fixed (like '12.00500'), which
  // helps to debounce when when moving label.
  value, {
    precision: opt.precision
  });
  var formatter = opt.formatter;

  if (formatter) {
    var params = {
      value: axisHelper.getAxisRawValue(axis, value),
      seriesData: []
    };
    zrUtil.each(seriesDataIndices, function (idxItem) {
      var series = ecModel.getSeriesByIndex(idxItem.seriesIndex);
      var dataIndex = idxItem.dataIndexInside;
      var dataParams = series && series.getDataParams(dataIndex);
      dataParams && params.seriesData.push(dataParams);
    });

    if (zrUtil.isString(formatter)) {
      text = formatter.replace('{value}', text);
    } else if (zrUtil.isFunction(formatter)) {
      text = formatter(params);
    }
  }

  return text;
}
/**
 * @param {module:echarts/coord/Axis} axis
 * @param {number} value
 * @param {Object} layoutInfo {
 *  rotation, position, labelOffset, labelDirection, labelMargin
 * }
 */


function getTransformedPosition(axis, value, layoutInfo) {
  var transform = matrix.create();
  matrix.rotate(transform, transform, layoutInfo.rotation);
  matrix.translate(transform, transform, layoutInfo.position);
  return graphic.applyTransform([axis.dataToCoord(value), (layoutInfo.labelOffset || 0) + (layoutInfo.labelDirection || 1) * (layoutInfo.labelMargin || 0)], transform);
}

function buildCartesianSingleLabelElOption(value, elOption, layoutInfo, axisModel, axisPointerModel, api) {
  var textLayout = AxisBuilder.innerTextLayout(layoutInfo.rotation, 0, layoutInfo.labelDirection);
  layoutInfo.labelMargin = axisPointerModel.get('label.margin');
  buildLabelElOption(elOption, axisModel, axisPointerModel, api, {
    position: getTransformedPosition(axisModel.axis, value, layoutInfo),
    align: textLayout.textAlign,
    verticalAlign: textLayout.textVerticalAlign
  });
}
/**
 * @param {Array.<number>} p1
 * @param {Array.<number>} p2
 * @param {number} [xDimIndex=0] or 1
 */


function makeLineShape(p1, p2, xDimIndex) {
  xDimIndex = xDimIndex || 0;
  return {
    x1: p1[xDimIndex],
    y1: p1[1 - xDimIndex],
    x2: p2[xDimIndex],
    y2: p2[1 - xDimIndex]
  };
}
/**
 * @param {Array.<number>} xy
 * @param {Array.<number>} wh
 * @param {number} [xDimIndex=0] or 1
 */


function makeRectShape(xy, wh, xDimIndex) {
  xDimIndex = xDimIndex || 0;
  return {
    x: xy[xDimIndex],
    y: xy[1 - xDimIndex],
    width: wh[xDimIndex],
    height: wh[1 - xDimIndex]
  };
}

function makeSectorShape(cx, cy, r0, r, startAngle, endAngle) {
  return {
    cx: cx,
    cy: cy,
    r0: r0,
    r: r,
    startAngle: startAngle,
    endAngle: endAngle,
    clockwise: true
  };
}

exports.buildElStyle = buildElStyle;
exports.buildLabelElOption = buildLabelElOption;
exports.getValueLabel = getValueLabel;
exports.getTransformedPosition = getTransformedPosition;
exports.buildCartesianSingleLabelElOption = buildCartesianSingleLabelElOption;
exports.makeLineShape = makeLineShape;
exports.makeRectShape = makeRectShape;
exports.makeSectorShape = makeSectorShape;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL3ZpZXdIZWxwZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvYXhpc1BvaW50ZXIvdmlld0hlbHBlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIHRleHRDb250YWluID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvbnRhaW4vdGV4dFwiKTtcblxudmFyIGZvcm1hdFV0aWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9mb3JtYXRcIik7XG5cbnZhciBtYXRyaXggPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9tYXRyaXhcIik7XG5cbnZhciBheGlzSGVscGVyID0gcmVxdWlyZShcIi4uLy4uL2Nvb3JkL2F4aXNIZWxwZXJcIik7XG5cbnZhciBBeGlzQnVpbGRlciA9IHJlcXVpcmUoXCIuLi9heGlzL0F4aXNCdWlsZGVyXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gYXhpc1BvaW50ZXJNb2RlbFxuICovXG5mdW5jdGlvbiBidWlsZEVsU3R5bGUoYXhpc1BvaW50ZXJNb2RlbCkge1xuICB2YXIgYXhpc1BvaW50ZXJUeXBlID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3R5cGUnKTtcbiAgdmFyIHN0eWxlTW9kZWwgPSBheGlzUG9pbnRlck1vZGVsLmdldE1vZGVsKGF4aXNQb2ludGVyVHlwZSArICdTdHlsZScpO1xuICB2YXIgc3R5bGU7XG5cbiAgaWYgKGF4aXNQb2ludGVyVHlwZSA9PT0gJ2xpbmUnKSB7XG4gICAgc3R5bGUgPSBzdHlsZU1vZGVsLmdldExpbmVTdHlsZSgpO1xuICAgIHN0eWxlLmZpbGwgPSBudWxsO1xuICB9IGVsc2UgaWYgKGF4aXNQb2ludGVyVHlwZSA9PT0gJ3NoYWRvdycpIHtcbiAgICBzdHlsZSA9IHN0eWxlTW9kZWwuZ2V0QXJlYVN0eWxlKCk7XG4gICAgc3R5bGUuc3Ryb2tlID0gbnVsbDtcbiAgfVxuXG4gIHJldHVybiBzdHlsZTtcbn1cbi8qKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gbGFiZWxQb3Mge2FsaWduLCB2ZXJ0aWNhbEFsaWduLCBwb3NpdGlvbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGJ1aWxkTGFiZWxFbE9wdGlvbihlbE9wdGlvbiwgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsLCBhcGksIGxhYmVsUG9zKSB7XG4gIHZhciB2YWx1ZSA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCd2YWx1ZScpO1xuICB2YXIgdGV4dCA9IGdldFZhbHVlTGFiZWwodmFsdWUsIGF4aXNNb2RlbC5heGlzLCBheGlzTW9kZWwuZWNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3Nlcmllc0RhdGFJbmRpY2VzJyksIHtcbiAgICBwcmVjaXNpb246IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdsYWJlbC5wcmVjaXNpb24nKSxcbiAgICBmb3JtYXR0ZXI6IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdsYWJlbC5mb3JtYXR0ZXInKVxuICB9KTtcbiAgdmFyIGxhYmVsTW9kZWwgPSBheGlzUG9pbnRlck1vZGVsLmdldE1vZGVsKCdsYWJlbCcpO1xuICB2YXIgcGFkZGluZ3MgPSBmb3JtYXRVdGlsLm5vcm1hbGl6ZUNzc0FycmF5KGxhYmVsTW9kZWwuZ2V0KCdwYWRkaW5nJykgfHwgMCk7XG4gIHZhciBmb250ID0gbGFiZWxNb2RlbC5nZXRGb250KCk7XG4gIHZhciB0ZXh0UmVjdCA9IHRleHRDb250YWluLmdldEJvdW5kaW5nUmVjdCh0ZXh0LCBmb250KTtcbiAgdmFyIHBvc2l0aW9uID0gbGFiZWxQb3MucG9zaXRpb247XG4gIHZhciB3aWR0aCA9IHRleHRSZWN0LndpZHRoICsgcGFkZGluZ3NbMV0gKyBwYWRkaW5nc1szXTtcbiAgdmFyIGhlaWdodCA9IHRleHRSZWN0LmhlaWdodCArIHBhZGRpbmdzWzBdICsgcGFkZGluZ3NbMl07IC8vIEFkanVzdCBieSBhbGlnbi5cblxuICB2YXIgYWxpZ24gPSBsYWJlbFBvcy5hbGlnbjtcbiAgYWxpZ24gPT09ICdyaWdodCcgJiYgKHBvc2l0aW9uWzBdIC09IHdpZHRoKTtcbiAgYWxpZ24gPT09ICdjZW50ZXInICYmIChwb3NpdGlvblswXSAtPSB3aWR0aCAvIDIpO1xuICB2YXIgdmVydGljYWxBbGlnbiA9IGxhYmVsUG9zLnZlcnRpY2FsQWxpZ247XG4gIHZlcnRpY2FsQWxpZ24gPT09ICdib3R0b20nICYmIChwb3NpdGlvblsxXSAtPSBoZWlnaHQpO1xuICB2ZXJ0aWNhbEFsaWduID09PSAnbWlkZGxlJyAmJiAocG9zaXRpb25bMV0gLT0gaGVpZ2h0IC8gMik7IC8vIE5vdCBvdmVyZmxvdyBlYyBjb250YWluZXJcblxuICBjb25maW5lSW5Db250YWluZXIocG9zaXRpb24sIHdpZHRoLCBoZWlnaHQsIGFwaSk7XG4gIHZhciBiZ0NvbG9yID0gbGFiZWxNb2RlbC5nZXQoJ2JhY2tncm91bmRDb2xvcicpO1xuXG4gIGlmICghYmdDb2xvciB8fCBiZ0NvbG9yID09PSAnYXV0bycpIHtcbiAgICBiZ0NvbG9yID0gYXhpc01vZGVsLmdldCgnYXhpc0xpbmUubGluZVN0eWxlLmNvbG9yJyk7XG4gIH1cblxuICBlbE9wdGlvbi5sYWJlbCA9IHtcbiAgICBzaGFwZToge1xuICAgICAgeDogMCxcbiAgICAgIHk6IDAsXG4gICAgICB3aWR0aDogd2lkdGgsXG4gICAgICBoZWlnaHQ6IGhlaWdodCxcbiAgICAgIHI6IGxhYmVsTW9kZWwuZ2V0KCdib3JkZXJSYWRpdXMnKVxuICAgIH0sXG4gICAgcG9zaXRpb246IHBvc2l0aW9uLnNsaWNlKCksXG4gICAgLy8gVE9ETzogcmljaFxuICAgIHN0eWxlOiB7XG4gICAgICB0ZXh0OiB0ZXh0LFxuICAgICAgdGV4dEZvbnQ6IGZvbnQsXG4gICAgICB0ZXh0RmlsbDogbGFiZWxNb2RlbC5nZXRUZXh0Q29sb3IoKSxcbiAgICAgIHRleHRQb3NpdGlvbjogJ2luc2lkZScsXG4gICAgICBmaWxsOiBiZ0NvbG9yLFxuICAgICAgc3Ryb2tlOiBsYWJlbE1vZGVsLmdldCgnYm9yZGVyQ29sb3InKSB8fCAndHJhbnNwYXJlbnQnLFxuICAgICAgbGluZVdpZHRoOiBsYWJlbE1vZGVsLmdldCgnYm9yZGVyV2lkdGgnKSB8fCAwLFxuICAgICAgc2hhZG93Qmx1cjogbGFiZWxNb2RlbC5nZXQoJ3NoYWRvd0JsdXInKSxcbiAgICAgIHNoYWRvd0NvbG9yOiBsYWJlbE1vZGVsLmdldCgnc2hhZG93Q29sb3InKSxcbiAgICAgIHNoYWRvd09mZnNldFg6IGxhYmVsTW9kZWwuZ2V0KCdzaGFkb3dPZmZzZXRYJyksXG4gICAgICBzaGFkb3dPZmZzZXRZOiBsYWJlbE1vZGVsLmdldCgnc2hhZG93T2Zmc2V0WScpXG4gICAgfSxcbiAgICAvLyBMYWJsZSBzaG91bGQgYmUgb3ZlciBheGlzUG9pbnRlci5cbiAgICB6MjogMTBcbiAgfTtcbn0gLy8gRG8gbm90IG92ZXJmbG93IGVjIGNvbnRhaW5lclxuXG5cbmZ1bmN0aW9uIGNvbmZpbmVJbkNvbnRhaW5lcihwb3NpdGlvbiwgd2lkdGgsIGhlaWdodCwgYXBpKSB7XG4gIHZhciB2aWV3V2lkdGggPSBhcGkuZ2V0V2lkdGgoKTtcbiAgdmFyIHZpZXdIZWlnaHQgPSBhcGkuZ2V0SGVpZ2h0KCk7XG4gIHBvc2l0aW9uWzBdID0gTWF0aC5taW4ocG9zaXRpb25bMF0gKyB3aWR0aCwgdmlld1dpZHRoKSAtIHdpZHRoO1xuICBwb3NpdGlvblsxXSA9IE1hdGgubWluKHBvc2l0aW9uWzFdICsgaGVpZ2h0LCB2aWV3SGVpZ2h0KSAtIGhlaWdodDtcbiAgcG9zaXRpb25bMF0gPSBNYXRoLm1heChwb3NpdGlvblswXSwgMCk7XG4gIHBvc2l0aW9uWzFdID0gTWF0aC5tYXgocG9zaXRpb25bMV0sIDApO1xufVxuLyoqXG4gKiBAcGFyYW0ge251bWJlcn0gdmFsdWVcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvY29vcmQvQXhpc30gYXhpc1xuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRcbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdD59IHNlcmllc0RhdGFJbmRpY2VzXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IG9wdC5wcmVjaXNpb24gJ2F1dG8nIG9yIGEgbnVtYmVyXG4gKiBAcGFyYW0ge3N0cmluZ3xGdW5jdGlvbn0gb3B0LmZvcm1hdHRlciBsYWJlbCBmb3JtYXR0ZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFZhbHVlTGFiZWwodmFsdWUsIGF4aXMsIGVjTW9kZWwsIHNlcmllc0RhdGFJbmRpY2VzLCBvcHQpIHtcbiAgdmFsdWUgPSBheGlzLnNjYWxlLnBhcnNlKHZhbHVlKTtcbiAgdmFyIHRleHQgPSBheGlzLnNjYWxlLmdldExhYmVsKCAvLyBJZiBgcHJlY2lzaW9uYCBpcyBzZXQsIHdpZHRoIGNhbiBiZSBmaXhlZCAobGlrZSAnMTIuMDA1MDAnKSwgd2hpY2hcbiAgLy8gaGVscHMgdG8gZGVib3VuY2Ugd2hlbiB3aGVuIG1vdmluZyBsYWJlbC5cbiAgdmFsdWUsIHtcbiAgICBwcmVjaXNpb246IG9wdC5wcmVjaXNpb25cbiAgfSk7XG4gIHZhciBmb3JtYXR0ZXIgPSBvcHQuZm9ybWF0dGVyO1xuXG4gIGlmIChmb3JtYXR0ZXIpIHtcbiAgICB2YXIgcGFyYW1zID0ge1xuICAgICAgdmFsdWU6IGF4aXNIZWxwZXIuZ2V0QXhpc1Jhd1ZhbHVlKGF4aXMsIHZhbHVlKSxcbiAgICAgIHNlcmllc0RhdGE6IFtdXG4gICAgfTtcbiAgICB6clV0aWwuZWFjaChzZXJpZXNEYXRhSW5kaWNlcywgZnVuY3Rpb24gKGlkeEl0ZW0pIHtcbiAgICAgIHZhciBzZXJpZXMgPSBlY01vZGVsLmdldFNlcmllc0J5SW5kZXgoaWR4SXRlbS5zZXJpZXNJbmRleCk7XG4gICAgICB2YXIgZGF0YUluZGV4ID0gaWR4SXRlbS5kYXRhSW5kZXhJbnNpZGU7XG4gICAgICB2YXIgZGF0YVBhcmFtcyA9IHNlcmllcyAmJiBzZXJpZXMuZ2V0RGF0YVBhcmFtcyhkYXRhSW5kZXgpO1xuICAgICAgZGF0YVBhcmFtcyAmJiBwYXJhbXMuc2VyaWVzRGF0YS5wdXNoKGRhdGFQYXJhbXMpO1xuICAgIH0pO1xuXG4gICAgaWYgKHpyVXRpbC5pc1N0cmluZyhmb3JtYXR0ZXIpKSB7XG4gICAgICB0ZXh0ID0gZm9ybWF0dGVyLnJlcGxhY2UoJ3t2YWx1ZX0nLCB0ZXh0KTtcbiAgICB9IGVsc2UgaWYgKHpyVXRpbC5pc0Z1bmN0aW9uKGZvcm1hdHRlcikpIHtcbiAgICAgIHRleHQgPSBmb3JtYXR0ZXIocGFyYW1zKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGV4dDtcbn1cbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9BeGlzfSBheGlzXG4gKiBAcGFyYW0ge251bWJlcn0gdmFsdWVcbiAqIEBwYXJhbSB7T2JqZWN0fSBsYXlvdXRJbmZvIHtcbiAqICByb3RhdGlvbiwgcG9zaXRpb24sIGxhYmVsT2Zmc2V0LCBsYWJlbERpcmVjdGlvbiwgbGFiZWxNYXJnaW5cbiAqIH1cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFRyYW5zZm9ybWVkUG9zaXRpb24oYXhpcywgdmFsdWUsIGxheW91dEluZm8pIHtcbiAgdmFyIHRyYW5zZm9ybSA9IG1hdHJpeC5jcmVhdGUoKTtcbiAgbWF0cml4LnJvdGF0ZSh0cmFuc2Zvcm0sIHRyYW5zZm9ybSwgbGF5b3V0SW5mby5yb3RhdGlvbik7XG4gIG1hdHJpeC50cmFuc2xhdGUodHJhbnNmb3JtLCB0cmFuc2Zvcm0sIGxheW91dEluZm8ucG9zaXRpb24pO1xuICByZXR1cm4gZ3JhcGhpYy5hcHBseVRyYW5zZm9ybShbYXhpcy5kYXRhVG9Db29yZCh2YWx1ZSksIChsYXlvdXRJbmZvLmxhYmVsT2Zmc2V0IHx8IDApICsgKGxheW91dEluZm8ubGFiZWxEaXJlY3Rpb24gfHwgMSkgKiAobGF5b3V0SW5mby5sYWJlbE1hcmdpbiB8fCAwKV0sIHRyYW5zZm9ybSk7XG59XG5cbmZ1bmN0aW9uIGJ1aWxkQ2FydGVzaWFuU2luZ2xlTGFiZWxFbE9wdGlvbih2YWx1ZSwgZWxPcHRpb24sIGxheW91dEluZm8sIGF4aXNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbCwgYXBpKSB7XG4gIHZhciB0ZXh0TGF5b3V0ID0gQXhpc0J1aWxkZXIuaW5uZXJUZXh0TGF5b3V0KGxheW91dEluZm8ucm90YXRpb24sIDAsIGxheW91dEluZm8ubGFiZWxEaXJlY3Rpb24pO1xuICBsYXlvdXRJbmZvLmxhYmVsTWFyZ2luID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ2xhYmVsLm1hcmdpbicpO1xuICBidWlsZExhYmVsRWxPcHRpb24oZWxPcHRpb24sIGF4aXNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbCwgYXBpLCB7XG4gICAgcG9zaXRpb246IGdldFRyYW5zZm9ybWVkUG9zaXRpb24oYXhpc01vZGVsLmF4aXMsIHZhbHVlLCBsYXlvdXRJbmZvKSxcbiAgICBhbGlnbjogdGV4dExheW91dC50ZXh0QWxpZ24sXG4gICAgdmVydGljYWxBbGlnbjogdGV4dExheW91dC50ZXh0VmVydGljYWxBbGlnblxuICB9KTtcbn1cbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gcDFcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHAyXG4gKiBAcGFyYW0ge251bWJlcn0gW3hEaW1JbmRleD0wXSBvciAxXG4gKi9cblxuXG5mdW5jdGlvbiBtYWtlTGluZVNoYXBlKHAxLCBwMiwgeERpbUluZGV4KSB7XG4gIHhEaW1JbmRleCA9IHhEaW1JbmRleCB8fCAwO1xuICByZXR1cm4ge1xuICAgIHgxOiBwMVt4RGltSW5kZXhdLFxuICAgIHkxOiBwMVsxIC0geERpbUluZGV4XSxcbiAgICB4MjogcDJbeERpbUluZGV4XSxcbiAgICB5MjogcDJbMSAtIHhEaW1JbmRleF1cbiAgfTtcbn1cbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0geHlcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHdoXG4gKiBAcGFyYW0ge251bWJlcn0gW3hEaW1JbmRleD0wXSBvciAxXG4gKi9cblxuXG5mdW5jdGlvbiBtYWtlUmVjdFNoYXBlKHh5LCB3aCwgeERpbUluZGV4KSB7XG4gIHhEaW1JbmRleCA9IHhEaW1JbmRleCB8fCAwO1xuICByZXR1cm4ge1xuICAgIHg6IHh5W3hEaW1JbmRleF0sXG4gICAgeTogeHlbMSAtIHhEaW1JbmRleF0sXG4gICAgd2lkdGg6IHdoW3hEaW1JbmRleF0sXG4gICAgaGVpZ2h0OiB3aFsxIC0geERpbUluZGV4XVxuICB9O1xufVxuXG5mdW5jdGlvbiBtYWtlU2VjdG9yU2hhcGUoY3gsIGN5LCByMCwgciwgc3RhcnRBbmdsZSwgZW5kQW5nbGUpIHtcbiAgcmV0dXJuIHtcbiAgICBjeDogY3gsXG4gICAgY3k6IGN5LFxuICAgIHIwOiByMCxcbiAgICByOiByLFxuICAgIHN0YXJ0QW5nbGU6IHN0YXJ0QW5nbGUsXG4gICAgZW5kQW5nbGU6IGVuZEFuZ2xlLFxuICAgIGNsb2Nrd2lzZTogdHJ1ZVxuICB9O1xufVxuXG5leHBvcnRzLmJ1aWxkRWxTdHlsZSA9IGJ1aWxkRWxTdHlsZTtcbmV4cG9ydHMuYnVpbGRMYWJlbEVsT3B0aW9uID0gYnVpbGRMYWJlbEVsT3B0aW9uO1xuZXhwb3J0cy5nZXRWYWx1ZUxhYmVsID0gZ2V0VmFsdWVMYWJlbDtcbmV4cG9ydHMuZ2V0VHJhbnNmb3JtZWRQb3NpdGlvbiA9IGdldFRyYW5zZm9ybWVkUG9zaXRpb247XG5leHBvcnRzLmJ1aWxkQ2FydGVzaWFuU2luZ2xlTGFiZWxFbE9wdGlvbiA9IGJ1aWxkQ2FydGVzaWFuU2luZ2xlTGFiZWxFbE9wdGlvbjtcbmV4cG9ydHMubWFrZUxpbmVTaGFwZSA9IG1ha2VMaW5lU2hhcGU7XG5leHBvcnRzLm1ha2VSZWN0U2hhcGUgPSBtYWtlUmVjdFNoYXBlO1xuZXhwb3J0cy5tYWtlU2VjdG9yU2hhcGUgPSBtYWtlU2VjdG9yU2hhcGU7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBYUE7QUFDQTtBQXhCQTtBQTBCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/viewHelper.js
