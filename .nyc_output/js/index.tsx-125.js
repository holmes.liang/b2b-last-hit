__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var react_quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-quill/dist/quill.snow.css */ "./node_modules/react-quill/dist/quill.snow.css");
/* harmony import */ var react_quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _desk_component_session_login__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/session-login */ "./src/app/desk/component/session-login.tsx");
/* harmony import */ var _common_sys_log__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/sys-log */ "./src/common/sys-log/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/index.tsx";






 // desk page

var Desk = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].lazy(function () {
  return Promise.all(/*! import() | desk */[__webpack_require__.e("vendors~desk~._node_modules_@"), __webpack_require__.e("vendors~desk~._node_modules_ba"), __webpack_require__.e("vendors~desk~._node_modules_e"), __webpack_require__.e("vendors~desk~._node_modules_echarts_lib_C"), __webpack_require__.e("vendors~desk~._node_modules_echarts_lib_con"), __webpack_require__.e("vendors~desk~._node_modules_echarts_lib_data_D"), __webpack_require__.e("vendors~desk~._node_modules_echarts_lib_e"), __webpack_require__.e("vendors~desk~._node_modules_echarts_lib_l"), __webpack_require__.e("vendors~desk~._node_modules_i"), __webpack_require__.e("vendors~desk~._node_modules_quill_dist_quill.js~635496f7"), __webpack_require__.e("vendors~desk~._node_modules_r"), __webpack_require__.e("vendors~desk~._node_modules_zrender_lib_E"), __webpack_require__.e("vendors~desk~._node_modules_zrender_lib_P"), __webpack_require__.e("desk~._src_a"), __webpack_require__.e("desk~._src_app_component_N"), __webpack_require__.e("desk~._src_app_desk_b"), __webpack_require__.e("desk~._src_app_desk_component_c"), __webpack_require__.e("desk~._src_app_desk_component_p"), __webpack_require__.e("desk~._src_app_desk_e"), __webpack_require__.e("desk~._src_app_desk_ekyc_i"), __webpack_require__.e("desk~._src_app_desk_q"), __webpack_require__.e("desk~._src_data-model_f"), __webpack_require__.e("desk~._src_s")]).then(__webpack_require__.bind(null, /*! ./desk */ "./src/app/desk/index.tsx"));
});

var App =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(App, _React$Component);

  function App(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, App);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(App).call(this, props, context));
    _this.history = void 0;
    _this.styleRef = void 0;
    _this.state = void 0;
    _this.history = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["History"].createBrowserHistory({
      basename: Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_AJAX_CLIENT_CONTEXT || ""
    });
    _this.styleRef = _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createRef();
    var themeName = _common__WEBPACK_IMPORTED_MODULE_5__["Envs"].getCurrentTheme();
    _this.state = {
      themeName: themeName,
      globalStyles: _styles__WEBPACK_IMPORTED_MODULE_8__["default"].createGlobalStyles(themeName),
      theme: _styles__WEBPACK_IMPORTED_MODULE_8__["default"].getTheme(themeName)
    };

    _this.history.listen(function () {
      window.scrollTo(0, 0);
    });

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(App, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _common__WEBPACK_IMPORTED_MODULE_5__["Envs"].application(this);
      var timer; // 获取site key

      var envUrl = "http://localhost";

      if (true) {
        envUrl = "".concat(envUrl, ":").concat("4000");
      }

      _common__WEBPACK_IMPORTED_MODULE_5__["Ajax"].get("".concat(envUrl, "/recaptchakey")).then(function (res) {
        var siteKey = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.get(res, "body.respData", "");

        if (siteKey) {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.async = true;
          script.src = "https://www.google.com/recaptcha/api.js?render=".concat(siteKey);
          document.head.appendChild(script);
        }
      });
      _common__WEBPACK_IMPORTED_MODULE_5__["Ajax"].get("/timezone").then(function (res) {
        _common__WEBPACK_IMPORTED_MODULE_5__["Storage"].GlobalParams.session().set("timeZone", res.body.respData);
      });

      function startTimer() {
        clearTimeout(timer);
        timer = setTimeout(function () {
          _desk_component_session_login__WEBPACK_IMPORTED_MODULE_10__["default"].routerSign();
        }, 1800 * 1000);
      }

      document.onmousemove = document.onmousedown = startTimer;

      window.onerror = function (message, source, lineno, colno, error) {
        Object(_common_sys_log__WEBPACK_IMPORTED_MODULE_11__["writeSyslog"])({
          logType: "Page Log",
          logLevel: "ERROR",
          message: "\n          message\uFF1A ".concat(message, ";\n          error\uFF1A ").concat(error, ";\n          lineno\uFF1A ").concat(lineno, "; colno\uFF1A ").concat(colno, ";\n          source\uFF1A ").concat(source, ";\n        ")
        });
      };
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _common__WEBPACK_IMPORTED_MODULE_5__["Envs"].application(null);
    }
  }, {
    key: "renderGlobalStyle",
    value: function renderGlobalStyle() {
      var GloablStyles = this.state.globalStyles;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(GloablStyles, {
        ref: this.styleRef,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      });
    }
  }, {
    key: "render",
    value: function render() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, this.renderGlobalStyle(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["StyledF"].ThemeProvider, {
        theme: this.getTheme(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Suspense, {
        fallback: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 99
          },
          __self: this
        }),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["Router"], {
        history: this.history,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["Switch"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["Route"], {
        path: "/*",
        render: function render() {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(Desk, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 102
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }))))));
    }
  }, {
    key: "getHistory",
    value: function getHistory() {
      return this.history;
    }
  }, {
    key: "getTheme",
    value: function getTheme() {
      return this.state.theme;
    }
  }, {
    key: "getThemeName",
    value: function getThemeName() {
      return this.state.themeName;
    }
  }, {
    key: "changeTheme",
    value: function changeTheme(name) {
      if (!name || name === this.state.themeName) {
        return;
      }

      if (name.startsWith("style-")) {
        name = name.substring(6);
      }

      if (name === this.state.themeName) {
        return;
      }

      _common__WEBPACK_IMPORTED_MODULE_5__["Storage"].Theme.set(_common__WEBPACK_IMPORTED_MODULE_5__["Consts"].THEME_KEY, name);
      this.setState({
        gloablStyle: _styles__WEBPACK_IMPORTED_MODULE_8__["default"].createGlobalStyles(name),
        theme: _styles__WEBPACK_IMPORTED_MODULE_8__["default"].getTheme(name),
        themeName: name
      });
    }
  }]);

  return App;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Component);

/* harmony default export */ __webpack_exports__["default"] = (App);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2luZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBamF4LCBDb25zdHMsIEVudnMsIFN0b3JhZ2V9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQge0hpc3RvcnksIFJlYWN0LCBSb3V0ZSwgUm91dGVyLCBTdHlsZWRGLCBTd2l0Y2h9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHtBcHBsaWNhdGlvbiwgU3R5bGVkUHJvcHNUaGVtZX0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IFN0eWxlcyBmcm9tIFwiQHN0eWxlc1wiO1xuaW1wb3J0IFwicmVhY3QtcXVpbGwvZGlzdC9xdWlsbC5zbm93LmNzc1wiO1xuaW1wb3J0IFNlc3Npb25Mb2dpbiBmcm9tIFwiQGRlc2stY29tcG9uZW50L3Nlc3Npb24tbG9naW5cIjtcbmltcG9ydCB7d3JpdGVTeXNsb2d9IGZyb20gXCJAY29tbW9uL3N5cy1sb2dcIjtcblxuLy8gZGVzayBwYWdlXG5jb25zdCBEZXNrID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJkZXNrXCIgKi8gXCIuL2Rlc2tcIikpO1xuXG5jbGFzcyBBcHAgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQgaW1wbGVtZW50cyBBcHBsaWNhdGlvbiB7XG4gIGhpc3Rvcnk6IEhpc3RvcnkuSGlzdG9yeTtcbiAgc3R5bGVSZWY6IFJlYWN0LlJlZk9iamVjdDxhbnk+O1xuICBzdGF0ZToge1xuICAgIHRoZW1lTmFtZTogc3RyaW5nO1xuICAgIGdsb2JhbFN0eWxlczogUmVhY3QuQ29tcG9uZW50Q2xhc3M8YW55PjtcbiAgICB0aGVtZTogU3R5bGVkUHJvcHNUaGVtZTtcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICAgIHRoaXMuaGlzdG9yeSA9IEhpc3RvcnkuY3JlYXRlQnJvd3Nlckhpc3Rvcnkoe1xuICAgICAgYmFzZW5hbWU6IHByb2Nlc3MuZW52LlJFQUNUX0FQUF9BSkFYX0NMSUVOVF9DT05URVhUIHx8IFwiXCIsXG4gICAgfSk7XG4gICAgdGhpcy5zdHlsZVJlZiA9IFJlYWN0LmNyZWF0ZVJlZigpO1xuICAgIGNvbnN0IHRoZW1lTmFtZSA9IEVudnMuZ2V0Q3VycmVudFRoZW1lKCk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHRoZW1lTmFtZTogdGhlbWVOYW1lLFxuICAgICAgZ2xvYmFsU3R5bGVzOiBTdHlsZXMuY3JlYXRlR2xvYmFsU3R5bGVzKHRoZW1lTmFtZSksXG4gICAgICB0aGVtZTogU3R5bGVzLmdldFRoZW1lKHRoZW1lTmFtZSksXG4gICAgfTtcblxuICAgIHRoaXMuaGlzdG9yeS5saXN0ZW4oKCkgPT4ge1xuICAgICAgd2luZG93LnNjcm9sbFRvKDAsIDApO1xuICAgIH0pO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgRW52cy5hcHBsaWNhdGlvbih0aGlzKTtcbiAgICBsZXQgdGltZXI6IGFueTtcbiAgICAvLyDojrflj5ZzaXRlIGtleVxuICAgIGxldCBlbnZVcmwgPSBwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfSE9TVDtcbiAgICBpZiAocHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0VOVl9OQU1FID09PSBcIkxvY2FsXCIpIHtcbiAgICAgIGVudlVybCA9IGAke2VudlVybH06JHtwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfUE9SVH1gO1xuICAgIH1cbiAgICBBamF4LmdldChgJHtlbnZVcmx9L3JlY2FwdGNoYWtleWApLnRoZW4oKHJlcykgPT4ge1xuICAgICAgY29uc3Qgc2l0ZUtleSA9IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhXCIsIFwiXCIpO1xuICAgICAgaWYgKHNpdGVLZXkpIHtcbiAgICAgICAgdmFyIHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzY3JpcHRcIik7XG4gICAgICAgIHNjcmlwdC50eXBlID0gXCJ0ZXh0L2phdmFzY3JpcHRcIjtcbiAgICAgICAgc2NyaXB0LmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgc2NyaXB0LnNyYyA9IGBodHRwczovL3d3dy5nb29nbGUuY29tL3JlY2FwdGNoYS9hcGkuanM/cmVuZGVyPSR7c2l0ZUtleX1gO1xuICAgICAgICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKHNjcmlwdCk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgQWpheC5nZXQoXCIvdGltZXpvbmVcIikudGhlbigocmVzKSA9PiB7XG4gICAgICBTdG9yYWdlLkdsb2JhbFBhcmFtcy5zZXNzaW9uKCkuc2V0KFwidGltZVpvbmVcIiwgcmVzLmJvZHkucmVzcERhdGEpO1xuICAgIH0pO1xuXG4gICAgZnVuY3Rpb24gc3RhcnRUaW1lcigpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICB0aW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgIFNlc3Npb25Mb2dpbi5yb3V0ZXJTaWduKCk7XG4gICAgICB9LCAxODAwICogMTAwMCk7XG4gICAgfVxuXG4gICAgZG9jdW1lbnQub25tb3VzZW1vdmUgPSBkb2N1bWVudC5vbm1vdXNlZG93biA9IHN0YXJ0VGltZXI7XG4gICAgd2luZG93Lm9uZXJyb3IgPSBmdW5jdGlvbihtZXNzYWdlLCBzb3VyY2UsIGxpbmVubywgY29sbm8sIGVycm9yKSB7XG4gICAgICB3cml0ZVN5c2xvZyh7XG4gICAgICAgIGxvZ1R5cGU6IFwiUGFnZSBMb2dcIixcbiAgICAgICAgbG9nTGV2ZWw6IFwiRVJST1JcIixcbiAgICAgICAgbWVzc2FnZTogYFxuICAgICAgICAgIG1lc3NhZ2XvvJogJHttZXNzYWdlfTtcbiAgICAgICAgICBlcnJvcu+8miAke2Vycm9yfTtcbiAgICAgICAgICBsaW5lbm/vvJogJHtsaW5lbm99OyBjb2xub++8miAke2NvbG5vfTtcbiAgICAgICAgICBzb3VyY2XvvJogJHtzb3VyY2V9O1xuICAgICAgICBgLFxuICAgICAgfSk7XG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIEVudnMuYXBwbGljYXRpb24obnVsbCk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlckdsb2JhbFN0eWxlKCk6IEpTWC5FbGVtZW50IHtcbiAgICBjb25zdCBHbG9hYmxTdHlsZXMgPSB0aGlzLnN0YXRlLmdsb2JhbFN0eWxlcztcbiAgICByZXR1cm4gPEdsb2FibFN0eWxlcyByZWY9e3RoaXMuc3R5bGVSZWYgYXMgYW55fS8+O1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG5cbiAgICAgIDxSZWFjdC5GcmFnbWVudD5cbiAgICAgICAge3RoaXMucmVuZGVyR2xvYmFsU3R5bGUoKX1cbiAgICAgICAgPFN0eWxlZEYuVGhlbWVQcm92aWRlciB0aGVtZT17dGhpcy5nZXRUaGVtZSgpfT5cbiAgICAgICAgICA8UmVhY3QuU3VzcGVuc2UgZmFsbGJhY2s9ezxkaXYvPn0+XG4gICAgICAgICAgICA8Um91dGVyIGhpc3Rvcnk9e3RoaXMuaGlzdG9yeX0+XG4gICAgICAgICAgICAgIDxTd2l0Y2g+XG4gICAgICAgICAgICAgICAgPFJvdXRlIHBhdGg9XCIvKlwiIHJlbmRlcj17KCkgPT4gPERlc2svPn0vPlxuICAgICAgICAgICAgICA8L1N3aXRjaD5cbiAgICAgICAgICAgIDwvUm91dGVyPlxuICAgICAgICAgIDwvUmVhY3QuU3VzcGVuc2U+XG4gICAgICAgIDwvU3R5bGVkRi5UaGVtZVByb3ZpZGVyPlxuICAgICAgPC9SZWFjdC5GcmFnbWVudD5cblxuICAgICk7XG4gIH1cblxuICBnZXRIaXN0b3J5KCk6IEhpc3RvcnkuSGlzdG9yeSB7XG4gICAgcmV0dXJuIHRoaXMuaGlzdG9yeTtcbiAgfVxuXG4gIGdldFRoZW1lKCk6IFN0eWxlZFByb3BzVGhlbWUge1xuICAgIHJldHVybiB0aGlzLnN0YXRlLnRoZW1lO1xuICB9XG5cbiAgZ2V0VGhlbWVOYW1lKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuc3RhdGUudGhlbWVOYW1lO1xuICB9XG5cbiAgY2hhbmdlVGhlbWUobmFtZTogc3RyaW5nKTogdm9pZCB7XG4gICAgaWYgKCFuYW1lIHx8IG5hbWUgPT09IHRoaXMuc3RhdGUudGhlbWVOYW1lKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmIChuYW1lLnN0YXJ0c1dpdGgoXCJzdHlsZS1cIikpIHtcbiAgICAgIG5hbWUgPSBuYW1lLnN1YnN0cmluZyg2KTtcbiAgICB9XG4gICAgaWYgKG5hbWUgPT09IHRoaXMuc3RhdGUudGhlbWVOYW1lKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgU3RvcmFnZS5UaGVtZS5zZXQoQ29uc3RzLlRIRU1FX0tFWSwgbmFtZSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBnbG9hYmxTdHlsZTogU3R5bGVzLmNyZWF0ZUdsb2JhbFN0eWxlcyhuYW1lKSxcbiAgICAgIHRoZW1lOiBTdHlsZXMuZ2V0VGhlbWUobmFtZSksXG4gICAgICB0aGVtZU5hbWU6IG5hbWUsXG4gICAgfSk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQXBwO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7Ozs7QUFTQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFnQkE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFVQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBOzs7O0FBaElBO0FBQ0E7QUFrSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/index.tsx
