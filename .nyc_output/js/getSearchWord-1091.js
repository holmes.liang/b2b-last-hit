__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getSearchWord; });
function getWord(text, position) {
  var str = String(text);
  /* eslint no-bitwise:0 */

  var pos = Number(position) >>> 0; // Search for the word's beginning and end.

  var left = str.slice(0, pos + 1).search(/\S+$/);
  var right = str.slice(pos).search(/\s/);

  if (right < 0) {
    return {
      word: str.slice(left),
      begin: left,
      end: str.length
    };
  } // Return the word, using the located bounds to extract it from the string.


  return {
    word: str.slice(left, right + pos),
    begin: left,
    end: right + pos
  };
}

function getSearchWord(editorState, selection) {
  var anchorKey = selection.getAnchorKey();
  var anchorOffset = selection.getAnchorOffset() - 1;
  var currentContent = editorState.getCurrentContent();
  var currentBlock = currentContent.getBlockForKey(anchorKey);

  if (currentBlock) {
    var blockText = currentBlock.getText();
    return getWord(blockText, anchorOffset);
  }

  return '';
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0U2VhcmNoV29yZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1tZW50aW9uL2VzL3V0aWxzL2dldFNlYXJjaFdvcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gZ2V0V29yZCh0ZXh0LCBwb3NpdGlvbikge1xuICB2YXIgc3RyID0gU3RyaW5nKHRleHQpO1xuICAvKiBlc2xpbnQgbm8tYml0d2lzZTowICovXG4gIHZhciBwb3MgPSBOdW1iZXIocG9zaXRpb24pID4+PiAwO1xuXG4gIC8vIFNlYXJjaCBmb3IgdGhlIHdvcmQncyBiZWdpbm5pbmcgYW5kIGVuZC5cbiAgdmFyIGxlZnQgPSBzdHIuc2xpY2UoMCwgcG9zICsgMSkuc2VhcmNoKC9cXFMrJC8pO1xuICB2YXIgcmlnaHQgPSBzdHIuc2xpY2UocG9zKS5zZWFyY2goL1xccy8pO1xuXG4gIGlmIChyaWdodCA8IDApIHtcbiAgICByZXR1cm4ge1xuICAgICAgd29yZDogc3RyLnNsaWNlKGxlZnQpLFxuICAgICAgYmVnaW46IGxlZnQsXG4gICAgICBlbmQ6IHN0ci5sZW5ndGhcbiAgICB9O1xuICB9XG5cbiAgLy8gUmV0dXJuIHRoZSB3b3JkLCB1c2luZyB0aGUgbG9jYXRlZCBib3VuZHMgdG8gZXh0cmFjdCBpdCBmcm9tIHRoZSBzdHJpbmcuXG4gIHJldHVybiB7XG4gICAgd29yZDogc3RyLnNsaWNlKGxlZnQsIHJpZ2h0ICsgcG9zKSxcbiAgICBiZWdpbjogbGVmdCxcbiAgICBlbmQ6IHJpZ2h0ICsgcG9zXG4gIH07XG59XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGdldFNlYXJjaFdvcmQoZWRpdG9yU3RhdGUsIHNlbGVjdGlvbikge1xuICB2YXIgYW5jaG9yS2V5ID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICB2YXIgYW5jaG9yT2Zmc2V0ID0gc2VsZWN0aW9uLmdldEFuY2hvck9mZnNldCgpIC0gMTtcbiAgdmFyIGN1cnJlbnRDb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgdmFyIGN1cnJlbnRCbG9jayA9IGN1cnJlbnRDb250ZW50LmdldEJsb2NrRm9yS2V5KGFuY2hvcktleSk7XG4gIGlmIChjdXJyZW50QmxvY2spIHtcbiAgICB2YXIgYmxvY2tUZXh0ID0gY3VycmVudEJsb2NrLmdldFRleHQoKTtcbiAgICByZXR1cm4gZ2V0V29yZChibG9ja1RleHQsIGFuY2hvck9mZnNldCk7XG4gIH1cbiAgcmV0dXJuICcnO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/getSearchWord.js
