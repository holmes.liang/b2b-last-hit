__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");
/* harmony import */ var babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _common_Track__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./common/Track */ "./node_modules/rc-slider/es/common/Track.js");
/* harmony import */ var _common_createSlider__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./common/createSlider */ "./node_modules/rc-slider/es/common/createSlider.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-slider/es/utils.js");







/* eslint-disable react/prop-types */










var _trimAlignValue = function _trimAlignValue(_ref) {
  var value = _ref.value,
      handle = _ref.handle,
      bounds = _ref.bounds,
      props = _ref.props;
  var allowCross = props.allowCross,
      pushable = props.pushable;
  var thershold = Number(pushable);
  var valInRange = _utils__WEBPACK_IMPORTED_MODULE_14__["ensureValueInRange"](value, props);
  var valNotConflict = valInRange;

  if (!allowCross && handle != null && bounds !== undefined) {
    if (handle > 0 && valInRange <= bounds[handle - 1] + thershold) {
      valNotConflict = bounds[handle - 1] + thershold;
    }

    if (handle < bounds.length - 1 && valInRange >= bounds[handle + 1] - thershold) {
      valNotConflict = bounds[handle + 1] - thershold;
    }
  }

  return _utils__WEBPACK_IMPORTED_MODULE_14__["ensureValuePrecision"](valNotConflict, props);
};

var Range = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default()(Range, _React$Component);

  function Range(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Range);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default()(this, (Range.__proto__ || Object.getPrototypeOf(Range)).call(this, props));

    _this.onEnd = function (force) {
      var handle = _this.state.handle;

      _this.removeDocumentEvents();

      if (handle !== null || force) {
        _this.props.onAfterChange(_this.getValue());
      }

      _this.setState({
        handle: null
      });
    };

    var count = props.count,
        min = props.min,
        max = props.max;
    var initialValue = Array.apply(undefined, babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(Array(count + 1))).map(function () {
      return min;
    });
    var defaultValue = 'defaultValue' in props ? props.defaultValue : initialValue;
    var value = props.value !== undefined ? props.value : defaultValue;
    var bounds = value.map(function (v, i) {
      return _trimAlignValue({
        value: v,
        handle: i,
        props: props
      });
    });
    var recent = bounds[0] === max ? 0 : bounds.length - 1;
    _this.state = {
      handle: null,
      recent: recent,
      bounds: bounds
    };
    return _this;
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Range, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      var _this2 = this;

      if (!('value' in this.props || 'min' in this.props || 'max' in this.props)) {
        return;
      }

      if (this.props.min === prevProps.min && this.props.max === prevProps.max && shallowequal__WEBPACK_IMPORTED_MODULE_11___default()(this.props.value, prevProps.value)) {
        return;
      }

      var _props = this.props,
          onChange = _props.onChange,
          value = _props.value;
      var currentValue = value || prevState.bounds;

      if (currentValue.some(function (v) {
        return _utils__WEBPACK_IMPORTED_MODULE_14__["isValueOutOfRange"](v, _this2.props);
      })) {
        var newValues = currentValue.map(function (v) {
          return _utils__WEBPACK_IMPORTED_MODULE_14__["ensureValueInRange"](v, _this2.props);
        });
        onChange(newValues);
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(state) {
      var props = this.props;
      var isNotControlled = !('value' in props);

      if (isNotControlled) {
        this.setState(state);
      } else {
        var controlledState = {};
        ['handle', 'recent'].forEach(function (item) {
          if (state[item] !== undefined) {
            controlledState[item] = state[item];
          }
        });

        if (Object.keys(controlledState).length) {
          this.setState(controlledState);
        }
      }

      var data = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, this.state, state);

      var changedValue = data.bounds;
      props.onChange(changedValue);
    }
  }, {
    key: 'onStart',
    value: function onStart(position) {
      var props = this.props;
      var state = this.state;
      var bounds = this.getValue();
      props.onBeforeChange(bounds);
      var value = this.calcValueByPos(position);
      this.startValue = value;
      this.startPosition = position;
      var closestBound = this.getClosestBound(value);
      this.prevMovedHandleIndex = this.getBoundNeedMoving(value, closestBound);
      this.setState({
        handle: this.prevMovedHandleIndex,
        recent: this.prevMovedHandleIndex
      });
      var prevValue = bounds[this.prevMovedHandleIndex];
      if (value === prevValue) return;
      var nextBounds = [].concat(babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(state.bounds));
      nextBounds[this.prevMovedHandleIndex] = value;
      this.onChange({
        bounds: nextBounds
      });
    }
  }, {
    key: 'onMove',
    value: function onMove(e, position) {
      _utils__WEBPACK_IMPORTED_MODULE_14__["pauseEvent"](e);
      var state = this.state;
      var value = this.calcValueByPos(position);
      var oldValue = state.bounds[state.handle];
      if (value === oldValue) return;
      this.moveTo(value);
    }
  }, {
    key: 'onKeyboard',
    value: function onKeyboard(e) {
      var _props2 = this.props,
          reverse = _props2.reverse,
          vertical = _props2.vertical;
      var valueMutator = _utils__WEBPACK_IMPORTED_MODULE_14__["getKeyboardValueMutator"](e, vertical, reverse);

      if (valueMutator) {
        _utils__WEBPACK_IMPORTED_MODULE_14__["pauseEvent"](e);
        var state = this.state,
            props = this.props;
        var bounds = state.bounds,
            handle = state.handle;
        var oldValue = bounds[handle === null ? state.recent : handle];
        var mutatedValue = valueMutator(oldValue, props);

        var value = _trimAlignValue({
          value: mutatedValue,
          handle: handle,
          bounds: state.bounds,
          props: props
        });

        if (value === oldValue) return;
        var isFromKeyboardEvent = true;
        this.moveTo(value, isFromKeyboardEvent);
      }
    }
  }, {
    key: 'getValue',
    value: function getValue() {
      return this.state.bounds;
    }
  }, {
    key: 'getClosestBound',
    value: function getClosestBound(value) {
      var bounds = this.state.bounds;
      var closestBound = 0;

      for (var i = 1; i < bounds.length - 1; ++i) {
        if (value >= bounds[i]) {
          closestBound = i;
        }
      }

      if (Math.abs(bounds[closestBound + 1] - value) < Math.abs(bounds[closestBound] - value)) {
        closestBound += 1;
      }

      return closestBound;
    }
  }, {
    key: 'getBoundNeedMoving',
    value: function getBoundNeedMoving(value, closestBound) {
      var _state = this.state,
          bounds = _state.bounds,
          recent = _state.recent;
      var boundNeedMoving = closestBound;
      var isAtTheSamePoint = bounds[closestBound + 1] === bounds[closestBound];

      if (isAtTheSamePoint && bounds[recent] === bounds[closestBound]) {
        boundNeedMoving = recent;
      }

      if (isAtTheSamePoint && value !== bounds[closestBound + 1]) {
        boundNeedMoving = value < bounds[closestBound + 1] ? closestBound : closestBound + 1;
      }

      return boundNeedMoving;
    }
  }, {
    key: 'getLowerBound',
    value: function getLowerBound() {
      return this.state.bounds[0];
    }
  }, {
    key: 'getUpperBound',
    value: function getUpperBound() {
      var bounds = this.state.bounds;
      return bounds[bounds.length - 1];
    }
    /**
     * Returns an array of possible slider points, taking into account both
     * `marks` and `step`. The result is cached.
     */

  }, {
    key: 'getPoints',
    value: function getPoints() {
      var _props3 = this.props,
          marks = _props3.marks,
          step = _props3.step,
          min = _props3.min,
          max = _props3.max;
      var cache = this._getPointsCache;

      if (!cache || cache.marks !== marks || cache.step !== step) {
        var pointsObject = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, marks);

        if (step !== null) {
          for (var point = min; point <= max; point += step) {
            pointsObject[point] = point;
          }
        }

        var points = Object.keys(pointsObject).map(parseFloat);
        points.sort(function (a, b) {
          return a - b;
        });
        this._getPointsCache = {
          marks: marks,
          step: step,
          points: points
        };
      }

      return this._getPointsCache.points;
    }
  }, {
    key: 'moveTo',
    value: function moveTo(value, isFromKeyboardEvent) {
      var _this3 = this;

      var state = this.state,
          props = this.props;
      var nextBounds = [].concat(babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(state.bounds));
      var handle = state.handle === null ? state.recent : state.handle;
      nextBounds[handle] = value;
      var nextHandle = handle;

      if (props.pushable !== false) {
        this.pushSurroundingHandles(nextBounds, nextHandle);
      } else if (props.allowCross) {
        nextBounds.sort(function (a, b) {
          return a - b;
        });
        nextHandle = nextBounds.indexOf(value);
      }

      this.onChange({
        recent: nextHandle,
        handle: nextHandle,
        bounds: nextBounds
      });

      if (isFromKeyboardEvent) {
        // known problem: because setState is async,
        // so trigger focus will invoke handler's onEnd and another handler's onStart too early,
        // cause onBeforeChange and onAfterChange receive wrong value.
        // here use setState callback to hack，but not elegant
        this.props.onAfterChange(nextBounds);
        this.setState({}, function () {
          _this3.handlesRefs[nextHandle].focus();
        });
        this.onEnd();
      }
    }
  }, {
    key: 'pushSurroundingHandles',
    value: function pushSurroundingHandles(bounds, handle) {
      var value = bounds[handle];
      var threshold = this.props.pushable;
      threshold = Number(threshold);
      var direction = 0;

      if (bounds[handle + 1] - value < threshold) {
        direction = +1; // push to right
      }

      if (value - bounds[handle - 1] < threshold) {
        direction = -1; // push to left
      }

      if (direction === 0) {
        return;
      }

      var nextHandle = handle + direction;
      var diffToNext = direction * (bounds[nextHandle] - value);

      if (!this.pushHandle(bounds, nextHandle, direction, threshold - diffToNext)) {
        // revert to original value if pushing is impossible
        bounds[handle] = bounds[nextHandle] - direction * threshold;
      }
    }
  }, {
    key: 'pushHandle',
    value: function pushHandle(bounds, handle, direction, amount) {
      var originalValue = bounds[handle];
      var currentValue = bounds[handle];

      while (direction * (currentValue - originalValue) < amount) {
        if (!this.pushHandleOnePoint(bounds, handle, direction)) {
          // can't push handle enough to create the needed `amount` gap, so we
          // revert its position to the original value
          bounds[handle] = originalValue;
          return false;
        }

        currentValue = bounds[handle];
      } // the handle was pushed enough to create the needed `amount` gap


      return true;
    }
  }, {
    key: 'pushHandleOnePoint',
    value: function pushHandleOnePoint(bounds, handle, direction) {
      var points = this.getPoints();
      var pointIndex = points.indexOf(bounds[handle]);
      var nextPointIndex = pointIndex + direction;

      if (nextPointIndex >= points.length || nextPointIndex < 0) {
        // reached the minimum or maximum available point, can't push anymore
        return false;
      }

      var nextHandle = handle + direction;
      var nextValue = points[nextPointIndex];
      var threshold = this.props.pushable;
      var diffToNext = direction * (bounds[nextHandle] - nextValue);

      if (!this.pushHandle(bounds, nextHandle, direction, threshold - diffToNext)) {
        // couldn't push next handle, so we won't push this one either
        return false;
      } // push the handle


      bounds[handle] = nextValue;
      return true;
    }
  }, {
    key: 'trimAlignValue',
    value: function trimAlignValue(value) {
      var _state2 = this.state,
          handle = _state2.handle,
          bounds = _state2.bounds;
      return _trimAlignValue({
        value: value,
        handle: handle,
        bounds: bounds,
        props: this.props
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var _state3 = this.state,
          handle = _state3.handle,
          bounds = _state3.bounds;
      var _props4 = this.props,
          prefixCls = _props4.prefixCls,
          vertical = _props4.vertical,
          included = _props4.included,
          disabled = _props4.disabled,
          min = _props4.min,
          max = _props4.max,
          reverse = _props4.reverse,
          handleGenerator = _props4.handle,
          trackStyle = _props4.trackStyle,
          handleStyle = _props4.handleStyle,
          tabIndex = _props4.tabIndex;
      var offsets = bounds.map(function (v) {
        return _this4.calcOffset(v);
      });
      var handleClassName = prefixCls + '-handle';
      var handles = bounds.map(function (v, i) {
        var _classNames;

        var _tabIndex = tabIndex[i] || 0;

        if (disabled || tabIndex[i] === null) {
          _tabIndex = null;
        }

        return handleGenerator({
          className: classnames__WEBPACK_IMPORTED_MODULE_9___default()((_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, handleClassName, true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, handleClassName + '-' + (i + 1), true), _classNames)),
          prefixCls: prefixCls,
          vertical: vertical,
          offset: offsets[i],
          value: v,
          dragging: handle === i,
          index: i,
          tabIndex: _tabIndex,
          min: min,
          max: max,
          reverse: reverse,
          disabled: disabled,
          style: handleStyle[i],
          ref: function ref(h) {
            return _this4.saveHandle(i, h);
          }
        });
      });
      var tracks = bounds.slice(0, -1).map(function (_, index) {
        var _classNames2;

        var i = index + 1;
        var trackClassName = classnames__WEBPACK_IMPORTED_MODULE_9___default()((_classNames2 = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames2, prefixCls + '-track', true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames2, prefixCls + '-track-' + i, true), _classNames2));
        return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_common_Track__WEBPACK_IMPORTED_MODULE_12__["default"], {
          className: trackClassName,
          vertical: vertical,
          reverse: reverse,
          included: included,
          offset: offsets[i - 1],
          length: offsets[i] - offsets[i - 1],
          style: trackStyle[index],
          key: i
        });
      });
      return {
        tracks: tracks,
        handles: handles
      };
    }
  }], [{
    key: 'getDerivedStateFromProps',
    value: function getDerivedStateFromProps(props, state) {
      if ('value' in props || 'min' in props || 'max' in props) {
        var value = props.value || state.bounds;
        var nextBounds = value.map(function (v, i) {
          return _trimAlignValue({
            value: v,
            handle: i,
            bounds: state.bounds,
            props: props
          });
        });

        if (nextBounds.length === state.bounds.length && nextBounds.every(function (v, i) {
          return v === state.bounds[i];
        })) {
          return null;
        }

        return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, state, {
          bounds: nextBounds
        });
      }

      return null;
    }
  }]);

  return Range;
}(react__WEBPACK_IMPORTED_MODULE_7___default.a.Component);

Range.displayName = 'Range';
Range.propTypes = {
  autoFocus: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool,
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number),
  value: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number),
  count: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number,
  pushable: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number]),
  allowCross: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool,
  reverse: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.bool,
  tabIndex: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number),
  min: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number,
  max: prop_types__WEBPACK_IMPORTED_MODULE_8___default.a.number
};
Range.defaultProps = {
  count: 1,
  allowCross: true,
  pushable: false,
  tabIndex: []
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_10__["polyfill"])(Range);
/* harmony default export */ __webpack_exports__["default"] = (Object(_common_createSlider__WEBPACK_IMPORTED_MODULE_13__["default"])(Range));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL1JhbmdlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL1JhbmdlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5JztcbmltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX3RvQ29uc3VtYWJsZUFycmF5IGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy90b0NvbnN1bWFibGVBcnJheSc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuLyogZXNsaW50LWRpc2FibGUgcmVhY3QvcHJvcC10eXBlcyAqL1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IHNoYWxsb3dFcXVhbCBmcm9tICdzaGFsbG93ZXF1YWwnO1xuaW1wb3J0IFRyYWNrIGZyb20gJy4vY29tbW9uL1RyYWNrJztcbmltcG9ydCBjcmVhdGVTbGlkZXIgZnJvbSAnLi9jb21tb24vY3JlYXRlU2xpZGVyJztcbmltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4vdXRpbHMnO1xuXG52YXIgX3RyaW1BbGlnblZhbHVlID0gZnVuY3Rpb24gX3RyaW1BbGlnblZhbHVlKF9yZWYpIHtcbiAgdmFyIHZhbHVlID0gX3JlZi52YWx1ZSxcbiAgICAgIGhhbmRsZSA9IF9yZWYuaGFuZGxlLFxuICAgICAgYm91bmRzID0gX3JlZi5ib3VuZHMsXG4gICAgICBwcm9wcyA9IF9yZWYucHJvcHM7XG4gIHZhciBhbGxvd0Nyb3NzID0gcHJvcHMuYWxsb3dDcm9zcyxcbiAgICAgIHB1c2hhYmxlID0gcHJvcHMucHVzaGFibGU7XG5cbiAgdmFyIHRoZXJzaG9sZCA9IE51bWJlcihwdXNoYWJsZSk7XG4gIHZhciB2YWxJblJhbmdlID0gdXRpbHMuZW5zdXJlVmFsdWVJblJhbmdlKHZhbHVlLCBwcm9wcyk7XG4gIHZhciB2YWxOb3RDb25mbGljdCA9IHZhbEluUmFuZ2U7XG4gIGlmICghYWxsb3dDcm9zcyAmJiBoYW5kbGUgIT0gbnVsbCAmJiBib3VuZHMgIT09IHVuZGVmaW5lZCkge1xuICAgIGlmIChoYW5kbGUgPiAwICYmIHZhbEluUmFuZ2UgPD0gYm91bmRzW2hhbmRsZSAtIDFdICsgdGhlcnNob2xkKSB7XG4gICAgICB2YWxOb3RDb25mbGljdCA9IGJvdW5kc1toYW5kbGUgLSAxXSArIHRoZXJzaG9sZDtcbiAgICB9XG4gICAgaWYgKGhhbmRsZSA8IGJvdW5kcy5sZW5ndGggLSAxICYmIHZhbEluUmFuZ2UgPj0gYm91bmRzW2hhbmRsZSArIDFdIC0gdGhlcnNob2xkKSB7XG4gICAgICB2YWxOb3RDb25mbGljdCA9IGJvdW5kc1toYW5kbGUgKyAxXSAtIHRoZXJzaG9sZDtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHV0aWxzLmVuc3VyZVZhbHVlUHJlY2lzaW9uKHZhbE5vdENvbmZsaWN0LCBwcm9wcyk7XG59O1xuXG52YXIgUmFuZ2UgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoUmFuZ2UsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFJhbmdlKHByb3BzKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFJhbmdlKTtcblxuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChSYW5nZS5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFJhbmdlKSkuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX3RoaXMub25FbmQgPSBmdW5jdGlvbiAoZm9yY2UpIHtcbiAgICAgIHZhciBoYW5kbGUgPSBfdGhpcy5zdGF0ZS5oYW5kbGU7XG5cbiAgICAgIF90aGlzLnJlbW92ZURvY3VtZW50RXZlbnRzKCk7XG5cbiAgICAgIGlmIChoYW5kbGUgIT09IG51bGwgfHwgZm9yY2UpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25BZnRlckNoYW5nZShfdGhpcy5nZXRWYWx1ZSgpKTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBoYW5kbGU6IG51bGxcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICB2YXIgY291bnQgPSBwcm9wcy5jb3VudCxcbiAgICAgICAgbWluID0gcHJvcHMubWluLFxuICAgICAgICBtYXggPSBwcm9wcy5tYXg7XG5cbiAgICB2YXIgaW5pdGlhbFZhbHVlID0gQXJyYXkuYXBwbHkodW5kZWZpbmVkLCBfdG9Db25zdW1hYmxlQXJyYXkoQXJyYXkoY291bnQgKyAxKSkpLm1hcChmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gbWluO1xuICAgIH0pO1xuICAgIHZhciBkZWZhdWx0VmFsdWUgPSAnZGVmYXVsdFZhbHVlJyBpbiBwcm9wcyA/IHByb3BzLmRlZmF1bHRWYWx1ZSA6IGluaXRpYWxWYWx1ZTtcbiAgICB2YXIgdmFsdWUgPSBwcm9wcy52YWx1ZSAhPT0gdW5kZWZpbmVkID8gcHJvcHMudmFsdWUgOiBkZWZhdWx0VmFsdWU7XG4gICAgdmFyIGJvdW5kcyA9IHZhbHVlLm1hcChmdW5jdGlvbiAodiwgaSkge1xuICAgICAgcmV0dXJuIF90cmltQWxpZ25WYWx1ZSh7XG4gICAgICAgIHZhbHVlOiB2LFxuICAgICAgICBoYW5kbGU6IGksXG4gICAgICAgIHByb3BzOiBwcm9wc1xuICAgICAgfSk7XG4gICAgfSk7XG4gICAgdmFyIHJlY2VudCA9IGJvdW5kc1swXSA9PT0gbWF4ID8gMCA6IGJvdW5kcy5sZW5ndGggLSAxO1xuXG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBoYW5kbGU6IG51bGwsXG4gICAgICByZWNlbnQ6IHJlY2VudCxcbiAgICAgIGJvdW5kczogYm91bmRzXG4gICAgfTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoUmFuZ2UsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcywgcHJldlN0YXRlKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgaWYgKCEoJ3ZhbHVlJyBpbiB0aGlzLnByb3BzIHx8ICdtaW4nIGluIHRoaXMucHJvcHMgfHwgJ21heCcgaW4gdGhpcy5wcm9wcykpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMucHJvcHMubWluID09PSBwcmV2UHJvcHMubWluICYmIHRoaXMucHJvcHMubWF4ID09PSBwcmV2UHJvcHMubWF4ICYmIHNoYWxsb3dFcXVhbCh0aGlzLnByb3BzLnZhbHVlLCBwcmV2UHJvcHMudmFsdWUpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIG9uQ2hhbmdlID0gX3Byb3BzLm9uQ2hhbmdlLFxuICAgICAgICAgIHZhbHVlID0gX3Byb3BzLnZhbHVlO1xuXG4gICAgICB2YXIgY3VycmVudFZhbHVlID0gdmFsdWUgfHwgcHJldlN0YXRlLmJvdW5kcztcbiAgICAgIGlmIChjdXJyZW50VmFsdWUuc29tZShmdW5jdGlvbiAodikge1xuICAgICAgICByZXR1cm4gdXRpbHMuaXNWYWx1ZU91dE9mUmFuZ2UodiwgX3RoaXMyLnByb3BzKTtcbiAgICAgIH0pKSB7XG4gICAgICAgIHZhciBuZXdWYWx1ZXMgPSBjdXJyZW50VmFsdWUubWFwKGZ1bmN0aW9uICh2KSB7XG4gICAgICAgICAgcmV0dXJuIHV0aWxzLmVuc3VyZVZhbHVlSW5SYW5nZSh2LCBfdGhpczIucHJvcHMpO1xuICAgICAgICB9KTtcbiAgICAgICAgb25DaGFuZ2UobmV3VmFsdWVzKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkNoYW5nZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2hhbmdlKHN0YXRlKSB7XG4gICAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgICAgdmFyIGlzTm90Q29udHJvbGxlZCA9ICEoJ3ZhbHVlJyBpbiBwcm9wcyk7XG4gICAgICBpZiAoaXNOb3RDb250cm9sbGVkKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoc3RhdGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGNvbnRyb2xsZWRTdGF0ZSA9IHt9O1xuXG4gICAgICAgIFsnaGFuZGxlJywgJ3JlY2VudCddLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICBpZiAoc3RhdGVbaXRlbV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgY29udHJvbGxlZFN0YXRlW2l0ZW1dID0gc3RhdGVbaXRlbV07XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoT2JqZWN0LmtleXMoY29udHJvbGxlZFN0YXRlKS5sZW5ndGgpIHtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKGNvbnRyb2xsZWRTdGF0ZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdmFyIGRhdGEgPSBfZXh0ZW5kcyh7fSwgdGhpcy5zdGF0ZSwgc3RhdGUpO1xuICAgICAgdmFyIGNoYW5nZWRWYWx1ZSA9IGRhdGEuYm91bmRzO1xuICAgICAgcHJvcHMub25DaGFuZ2UoY2hhbmdlZFZhbHVlKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvblN0YXJ0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25TdGFydChwb3NpdGlvbikge1xuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgIHZhciBzdGF0ZSA9IHRoaXMuc3RhdGU7XG4gICAgICB2YXIgYm91bmRzID0gdGhpcy5nZXRWYWx1ZSgpO1xuICAgICAgcHJvcHMub25CZWZvcmVDaGFuZ2UoYm91bmRzKTtcblxuICAgICAgdmFyIHZhbHVlID0gdGhpcy5jYWxjVmFsdWVCeVBvcyhwb3NpdGlvbik7XG4gICAgICB0aGlzLnN0YXJ0VmFsdWUgPSB2YWx1ZTtcbiAgICAgIHRoaXMuc3RhcnRQb3NpdGlvbiA9IHBvc2l0aW9uO1xuXG4gICAgICB2YXIgY2xvc2VzdEJvdW5kID0gdGhpcy5nZXRDbG9zZXN0Qm91bmQodmFsdWUpO1xuICAgICAgdGhpcy5wcmV2TW92ZWRIYW5kbGVJbmRleCA9IHRoaXMuZ2V0Qm91bmROZWVkTW92aW5nKHZhbHVlLCBjbG9zZXN0Qm91bmQpO1xuXG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaGFuZGxlOiB0aGlzLnByZXZNb3ZlZEhhbmRsZUluZGV4LFxuICAgICAgICByZWNlbnQ6IHRoaXMucHJldk1vdmVkSGFuZGxlSW5kZXhcbiAgICAgIH0pO1xuXG4gICAgICB2YXIgcHJldlZhbHVlID0gYm91bmRzW3RoaXMucHJldk1vdmVkSGFuZGxlSW5kZXhdO1xuICAgICAgaWYgKHZhbHVlID09PSBwcmV2VmFsdWUpIHJldHVybjtcblxuICAgICAgdmFyIG5leHRCb3VuZHMgPSBbXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KHN0YXRlLmJvdW5kcykpO1xuICAgICAgbmV4dEJvdW5kc1t0aGlzLnByZXZNb3ZlZEhhbmRsZUluZGV4XSA9IHZhbHVlO1xuICAgICAgdGhpcy5vbkNoYW5nZSh7IGJvdW5kczogbmV4dEJvdW5kcyB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbk1vdmUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbk1vdmUoZSwgcG9zaXRpb24pIHtcbiAgICAgIHV0aWxzLnBhdXNlRXZlbnQoZSk7XG4gICAgICB2YXIgc3RhdGUgPSB0aGlzLnN0YXRlO1xuXG4gICAgICB2YXIgdmFsdWUgPSB0aGlzLmNhbGNWYWx1ZUJ5UG9zKHBvc2l0aW9uKTtcbiAgICAgIHZhciBvbGRWYWx1ZSA9IHN0YXRlLmJvdW5kc1tzdGF0ZS5oYW5kbGVdO1xuICAgICAgaWYgKHZhbHVlID09PSBvbGRWYWx1ZSkgcmV0dXJuO1xuXG4gICAgICB0aGlzLm1vdmVUbyh2YWx1ZSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25LZXlib2FyZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uS2V5Ym9hcmQoZSkge1xuICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIHJldmVyc2UgPSBfcHJvcHMyLnJldmVyc2UsXG4gICAgICAgICAgdmVydGljYWwgPSBfcHJvcHMyLnZlcnRpY2FsO1xuXG4gICAgICB2YXIgdmFsdWVNdXRhdG9yID0gdXRpbHMuZ2V0S2V5Ym9hcmRWYWx1ZU11dGF0b3IoZSwgdmVydGljYWwsIHJldmVyc2UpO1xuXG4gICAgICBpZiAodmFsdWVNdXRhdG9yKSB7XG4gICAgICAgIHV0aWxzLnBhdXNlRXZlbnQoZSk7XG4gICAgICAgIHZhciBzdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgICBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgICAgIHZhciBib3VuZHMgPSBzdGF0ZS5ib3VuZHMsXG4gICAgICAgICAgICBoYW5kbGUgPSBzdGF0ZS5oYW5kbGU7XG5cbiAgICAgICAgdmFyIG9sZFZhbHVlID0gYm91bmRzW2hhbmRsZSA9PT0gbnVsbCA/IHN0YXRlLnJlY2VudCA6IGhhbmRsZV07XG4gICAgICAgIHZhciBtdXRhdGVkVmFsdWUgPSB2YWx1ZU11dGF0b3Iob2xkVmFsdWUsIHByb3BzKTtcbiAgICAgICAgdmFyIHZhbHVlID0gX3RyaW1BbGlnblZhbHVlKHtcbiAgICAgICAgICB2YWx1ZTogbXV0YXRlZFZhbHVlLFxuICAgICAgICAgIGhhbmRsZTogaGFuZGxlLFxuICAgICAgICAgIGJvdW5kczogc3RhdGUuYm91bmRzLFxuICAgICAgICAgIHByb3BzOiBwcm9wc1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKHZhbHVlID09PSBvbGRWYWx1ZSkgcmV0dXJuO1xuICAgICAgICB2YXIgaXNGcm9tS2V5Ym9hcmRFdmVudCA9IHRydWU7XG4gICAgICAgIHRoaXMubW92ZVRvKHZhbHVlLCBpc0Zyb21LZXlib2FyZEV2ZW50KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFZhbHVlKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RhdGUuYm91bmRzO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldENsb3Nlc3RCb3VuZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENsb3Nlc3RCb3VuZCh2YWx1ZSkge1xuICAgICAgdmFyIGJvdW5kcyA9IHRoaXMuc3RhdGUuYm91bmRzO1xuXG4gICAgICB2YXIgY2xvc2VzdEJvdW5kID0gMDtcbiAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgYm91bmRzLmxlbmd0aCAtIDE7ICsraSkge1xuICAgICAgICBpZiAodmFsdWUgPj0gYm91bmRzW2ldKSB7XG4gICAgICAgICAgY2xvc2VzdEJvdW5kID0gaTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKE1hdGguYWJzKGJvdW5kc1tjbG9zZXN0Qm91bmQgKyAxXSAtIHZhbHVlKSA8IE1hdGguYWJzKGJvdW5kc1tjbG9zZXN0Qm91bmRdIC0gdmFsdWUpKSB7XG4gICAgICAgIGNsb3Nlc3RCb3VuZCArPSAxO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNsb3Nlc3RCb3VuZDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRCb3VuZE5lZWRNb3ZpbmcnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRCb3VuZE5lZWRNb3ZpbmcodmFsdWUsIGNsb3Nlc3RCb3VuZCkge1xuICAgICAgdmFyIF9zdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgYm91bmRzID0gX3N0YXRlLmJvdW5kcyxcbiAgICAgICAgICByZWNlbnQgPSBfc3RhdGUucmVjZW50O1xuXG4gICAgICB2YXIgYm91bmROZWVkTW92aW5nID0gY2xvc2VzdEJvdW5kO1xuICAgICAgdmFyIGlzQXRUaGVTYW1lUG9pbnQgPSBib3VuZHNbY2xvc2VzdEJvdW5kICsgMV0gPT09IGJvdW5kc1tjbG9zZXN0Qm91bmRdO1xuXG4gICAgICBpZiAoaXNBdFRoZVNhbWVQb2ludCAmJiBib3VuZHNbcmVjZW50XSA9PT0gYm91bmRzW2Nsb3Nlc3RCb3VuZF0pIHtcbiAgICAgICAgYm91bmROZWVkTW92aW5nID0gcmVjZW50O1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNBdFRoZVNhbWVQb2ludCAmJiB2YWx1ZSAhPT0gYm91bmRzW2Nsb3Nlc3RCb3VuZCArIDFdKSB7XG4gICAgICAgIGJvdW5kTmVlZE1vdmluZyA9IHZhbHVlIDwgYm91bmRzW2Nsb3Nlc3RCb3VuZCArIDFdID8gY2xvc2VzdEJvdW5kIDogY2xvc2VzdEJvdW5kICsgMTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBib3VuZE5lZWRNb3Zpbmc7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0TG93ZXJCb3VuZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldExvd2VyQm91bmQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdGF0ZS5ib3VuZHNbMF07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0VXBwZXJCb3VuZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFVwcGVyQm91bmQoKSB7XG4gICAgICB2YXIgYm91bmRzID0gdGhpcy5zdGF0ZS5ib3VuZHM7XG5cbiAgICAgIHJldHVybiBib3VuZHNbYm91bmRzLmxlbmd0aCAtIDFdO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgYW4gYXJyYXkgb2YgcG9zc2libGUgc2xpZGVyIHBvaW50cywgdGFraW5nIGludG8gYWNjb3VudCBib3RoXG4gICAgICogYG1hcmtzYCBhbmQgYHN0ZXBgLiBUaGUgcmVzdWx0IGlzIGNhY2hlZC5cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0UG9pbnRzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0UG9pbnRzKCkge1xuICAgICAgdmFyIF9wcm9wczMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIG1hcmtzID0gX3Byb3BzMy5tYXJrcyxcbiAgICAgICAgICBzdGVwID0gX3Byb3BzMy5zdGVwLFxuICAgICAgICAgIG1pbiA9IF9wcm9wczMubWluLFxuICAgICAgICAgIG1heCA9IF9wcm9wczMubWF4O1xuXG4gICAgICB2YXIgY2FjaGUgPSB0aGlzLl9nZXRQb2ludHNDYWNoZTtcbiAgICAgIGlmICghY2FjaGUgfHwgY2FjaGUubWFya3MgIT09IG1hcmtzIHx8IGNhY2hlLnN0ZXAgIT09IHN0ZXApIHtcbiAgICAgICAgdmFyIHBvaW50c09iamVjdCA9IF9leHRlbmRzKHt9LCBtYXJrcyk7XG4gICAgICAgIGlmIChzdGVwICE9PSBudWxsKSB7XG4gICAgICAgICAgZm9yICh2YXIgcG9pbnQgPSBtaW47IHBvaW50IDw9IG1heDsgcG9pbnQgKz0gc3RlcCkge1xuICAgICAgICAgICAgcG9pbnRzT2JqZWN0W3BvaW50XSA9IHBvaW50O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB2YXIgcG9pbnRzID0gT2JqZWN0LmtleXMocG9pbnRzT2JqZWN0KS5tYXAocGFyc2VGbG9hdCk7XG4gICAgICAgIHBvaW50cy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgICAgcmV0dXJuIGEgLSBiO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5fZ2V0UG9pbnRzQ2FjaGUgPSB7IG1hcmtzOiBtYXJrcywgc3RlcDogc3RlcCwgcG9pbnRzOiBwb2ludHMgfTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB0aGlzLl9nZXRQb2ludHNDYWNoZS5wb2ludHM7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnbW92ZVRvJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gbW92ZVRvKHZhbHVlLCBpc0Zyb21LZXlib2FyZEV2ZW50KSB7XG4gICAgICB2YXIgX3RoaXMzID0gdGhpcztcblxuICAgICAgdmFyIHN0YXRlID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBwcm9wcyA9IHRoaXMucHJvcHM7XG5cbiAgICAgIHZhciBuZXh0Qm91bmRzID0gW10uY29uY2F0KF90b0NvbnN1bWFibGVBcnJheShzdGF0ZS5ib3VuZHMpKTtcbiAgICAgIHZhciBoYW5kbGUgPSBzdGF0ZS5oYW5kbGUgPT09IG51bGwgPyBzdGF0ZS5yZWNlbnQgOiBzdGF0ZS5oYW5kbGU7XG4gICAgICBuZXh0Qm91bmRzW2hhbmRsZV0gPSB2YWx1ZTtcbiAgICAgIHZhciBuZXh0SGFuZGxlID0gaGFuZGxlO1xuICAgICAgaWYgKHByb3BzLnB1c2hhYmxlICE9PSBmYWxzZSkge1xuICAgICAgICB0aGlzLnB1c2hTdXJyb3VuZGluZ0hhbmRsZXMobmV4dEJvdW5kcywgbmV4dEhhbmRsZSk7XG4gICAgICB9IGVsc2UgaWYgKHByb3BzLmFsbG93Q3Jvc3MpIHtcbiAgICAgICAgbmV4dEJvdW5kcy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICAgICAgcmV0dXJuIGEgLSBiO1xuICAgICAgICB9KTtcbiAgICAgICAgbmV4dEhhbmRsZSA9IG5leHRCb3VuZHMuaW5kZXhPZih2YWx1ZSk7XG4gICAgICB9XG4gICAgICB0aGlzLm9uQ2hhbmdlKHtcbiAgICAgICAgcmVjZW50OiBuZXh0SGFuZGxlLFxuICAgICAgICBoYW5kbGU6IG5leHRIYW5kbGUsXG4gICAgICAgIGJvdW5kczogbmV4dEJvdW5kc1xuICAgICAgfSk7XG4gICAgICBpZiAoaXNGcm9tS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICAvLyBrbm93biBwcm9ibGVtOiBiZWNhdXNlIHNldFN0YXRlIGlzIGFzeW5jLFxuICAgICAgICAvLyBzbyB0cmlnZ2VyIGZvY3VzIHdpbGwgaW52b2tlIGhhbmRsZXIncyBvbkVuZCBhbmQgYW5vdGhlciBoYW5kbGVyJ3Mgb25TdGFydCB0b28gZWFybHksXG4gICAgICAgIC8vIGNhdXNlIG9uQmVmb3JlQ2hhbmdlIGFuZCBvbkFmdGVyQ2hhbmdlIHJlY2VpdmUgd3JvbmcgdmFsdWUuXG4gICAgICAgIC8vIGhlcmUgdXNlIHNldFN0YXRlIGNhbGxiYWNrIHRvIGhhY2vvvIxidXQgbm90IGVsZWdhbnRcbiAgICAgICAgdGhpcy5wcm9wcy5vbkFmdGVyQ2hhbmdlKG5leHRCb3VuZHMpO1xuICAgICAgICB0aGlzLnNldFN0YXRlKHt9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgX3RoaXMzLmhhbmRsZXNSZWZzW25leHRIYW5kbGVdLmZvY3VzKCk7XG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLm9uRW5kKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncHVzaFN1cnJvdW5kaW5nSGFuZGxlcycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHB1c2hTdXJyb3VuZGluZ0hhbmRsZXMoYm91bmRzLCBoYW5kbGUpIHtcbiAgICAgIHZhciB2YWx1ZSA9IGJvdW5kc1toYW5kbGVdO1xuICAgICAgdmFyIHRocmVzaG9sZCA9IHRoaXMucHJvcHMucHVzaGFibGU7XG5cbiAgICAgIHRocmVzaG9sZCA9IE51bWJlcih0aHJlc2hvbGQpO1xuXG4gICAgICB2YXIgZGlyZWN0aW9uID0gMDtcbiAgICAgIGlmIChib3VuZHNbaGFuZGxlICsgMV0gLSB2YWx1ZSA8IHRocmVzaG9sZCkge1xuICAgICAgICBkaXJlY3Rpb24gPSArMTsgLy8gcHVzaCB0byByaWdodFxuICAgICAgfVxuICAgICAgaWYgKHZhbHVlIC0gYm91bmRzW2hhbmRsZSAtIDFdIDwgdGhyZXNob2xkKSB7XG4gICAgICAgIGRpcmVjdGlvbiA9IC0xOyAvLyBwdXNoIHRvIGxlZnRcbiAgICAgIH1cblxuICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBuZXh0SGFuZGxlID0gaGFuZGxlICsgZGlyZWN0aW9uO1xuICAgICAgdmFyIGRpZmZUb05leHQgPSBkaXJlY3Rpb24gKiAoYm91bmRzW25leHRIYW5kbGVdIC0gdmFsdWUpO1xuICAgICAgaWYgKCF0aGlzLnB1c2hIYW5kbGUoYm91bmRzLCBuZXh0SGFuZGxlLCBkaXJlY3Rpb24sIHRocmVzaG9sZCAtIGRpZmZUb05leHQpKSB7XG4gICAgICAgIC8vIHJldmVydCB0byBvcmlnaW5hbCB2YWx1ZSBpZiBwdXNoaW5nIGlzIGltcG9zc2libGVcbiAgICAgICAgYm91bmRzW2hhbmRsZV0gPSBib3VuZHNbbmV4dEhhbmRsZV0gLSBkaXJlY3Rpb24gKiB0aHJlc2hvbGQ7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncHVzaEhhbmRsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHB1c2hIYW5kbGUoYm91bmRzLCBoYW5kbGUsIGRpcmVjdGlvbiwgYW1vdW50KSB7XG4gICAgICB2YXIgb3JpZ2luYWxWYWx1ZSA9IGJvdW5kc1toYW5kbGVdO1xuICAgICAgdmFyIGN1cnJlbnRWYWx1ZSA9IGJvdW5kc1toYW5kbGVdO1xuICAgICAgd2hpbGUgKGRpcmVjdGlvbiAqIChjdXJyZW50VmFsdWUgLSBvcmlnaW5hbFZhbHVlKSA8IGFtb3VudCkge1xuICAgICAgICBpZiAoIXRoaXMucHVzaEhhbmRsZU9uZVBvaW50KGJvdW5kcywgaGFuZGxlLCBkaXJlY3Rpb24pKSB7XG4gICAgICAgICAgLy8gY2FuJ3QgcHVzaCBoYW5kbGUgZW5vdWdoIHRvIGNyZWF0ZSB0aGUgbmVlZGVkIGBhbW91bnRgIGdhcCwgc28gd2VcbiAgICAgICAgICAvLyByZXZlcnQgaXRzIHBvc2l0aW9uIHRvIHRoZSBvcmlnaW5hbCB2YWx1ZVxuICAgICAgICAgIGJvdW5kc1toYW5kbGVdID0gb3JpZ2luYWxWYWx1ZTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgY3VycmVudFZhbHVlID0gYm91bmRzW2hhbmRsZV07XG4gICAgICB9XG4gICAgICAvLyB0aGUgaGFuZGxlIHdhcyBwdXNoZWQgZW5vdWdoIHRvIGNyZWF0ZSB0aGUgbmVlZGVkIGBhbW91bnRgIGdhcFxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncHVzaEhhbmRsZU9uZVBvaW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcHVzaEhhbmRsZU9uZVBvaW50KGJvdW5kcywgaGFuZGxlLCBkaXJlY3Rpb24pIHtcbiAgICAgIHZhciBwb2ludHMgPSB0aGlzLmdldFBvaW50cygpO1xuICAgICAgdmFyIHBvaW50SW5kZXggPSBwb2ludHMuaW5kZXhPZihib3VuZHNbaGFuZGxlXSk7XG4gICAgICB2YXIgbmV4dFBvaW50SW5kZXggPSBwb2ludEluZGV4ICsgZGlyZWN0aW9uO1xuICAgICAgaWYgKG5leHRQb2ludEluZGV4ID49IHBvaW50cy5sZW5ndGggfHwgbmV4dFBvaW50SW5kZXggPCAwKSB7XG4gICAgICAgIC8vIHJlYWNoZWQgdGhlIG1pbmltdW0gb3IgbWF4aW11bSBhdmFpbGFibGUgcG9pbnQsIGNhbid0IHB1c2ggYW55bW9yZVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgICB2YXIgbmV4dEhhbmRsZSA9IGhhbmRsZSArIGRpcmVjdGlvbjtcbiAgICAgIHZhciBuZXh0VmFsdWUgPSBwb2ludHNbbmV4dFBvaW50SW5kZXhdO1xuICAgICAgdmFyIHRocmVzaG9sZCA9IHRoaXMucHJvcHMucHVzaGFibGU7XG5cbiAgICAgIHZhciBkaWZmVG9OZXh0ID0gZGlyZWN0aW9uICogKGJvdW5kc1tuZXh0SGFuZGxlXSAtIG5leHRWYWx1ZSk7XG4gICAgICBpZiAoIXRoaXMucHVzaEhhbmRsZShib3VuZHMsIG5leHRIYW5kbGUsIGRpcmVjdGlvbiwgdGhyZXNob2xkIC0gZGlmZlRvTmV4dCkpIHtcbiAgICAgICAgLy8gY291bGRuJ3QgcHVzaCBuZXh0IGhhbmRsZSwgc28gd2Ugd29uJ3QgcHVzaCB0aGlzIG9uZSBlaXRoZXJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgLy8gcHVzaCB0aGUgaGFuZGxlXG4gICAgICBib3VuZHNbaGFuZGxlXSA9IG5leHRWYWx1ZTtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3RyaW1BbGlnblZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdHJpbUFsaWduVmFsdWUodmFsdWUpIHtcbiAgICAgIHZhciBfc3RhdGUyID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBoYW5kbGUgPSBfc3RhdGUyLmhhbmRsZSxcbiAgICAgICAgICBib3VuZHMgPSBfc3RhdGUyLmJvdW5kcztcblxuICAgICAgcmV0dXJuIF90cmltQWxpZ25WYWx1ZSh7XG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgaGFuZGxlOiBoYW5kbGUsXG4gICAgICAgIGJvdW5kczogYm91bmRzLFxuICAgICAgICBwcm9wczogdGhpcy5wcm9wc1xuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XG5cbiAgICAgIHZhciBfc3RhdGUzID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBoYW5kbGUgPSBfc3RhdGUzLmhhbmRsZSxcbiAgICAgICAgICBib3VuZHMgPSBfc3RhdGUzLmJvdW5kcztcbiAgICAgIHZhciBfcHJvcHM0ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfcHJvcHM0LnByZWZpeENscyxcbiAgICAgICAgICB2ZXJ0aWNhbCA9IF9wcm9wczQudmVydGljYWwsXG4gICAgICAgICAgaW5jbHVkZWQgPSBfcHJvcHM0LmluY2x1ZGVkLFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzNC5kaXNhYmxlZCxcbiAgICAgICAgICBtaW4gPSBfcHJvcHM0Lm1pbixcbiAgICAgICAgICBtYXggPSBfcHJvcHM0Lm1heCxcbiAgICAgICAgICByZXZlcnNlID0gX3Byb3BzNC5yZXZlcnNlLFxuICAgICAgICAgIGhhbmRsZUdlbmVyYXRvciA9IF9wcm9wczQuaGFuZGxlLFxuICAgICAgICAgIHRyYWNrU3R5bGUgPSBfcHJvcHM0LnRyYWNrU3R5bGUsXG4gICAgICAgICAgaGFuZGxlU3R5bGUgPSBfcHJvcHM0LmhhbmRsZVN0eWxlLFxuICAgICAgICAgIHRhYkluZGV4ID0gX3Byb3BzNC50YWJJbmRleDtcblxuXG4gICAgICB2YXIgb2Zmc2V0cyA9IGJvdW5kcy5tYXAoZnVuY3Rpb24gKHYpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzNC5jYWxjT2Zmc2V0KHYpO1xuICAgICAgfSk7XG5cbiAgICAgIHZhciBoYW5kbGVDbGFzc05hbWUgPSBwcmVmaXhDbHMgKyAnLWhhbmRsZSc7XG4gICAgICB2YXIgaGFuZGxlcyA9IGJvdW5kcy5tYXAoZnVuY3Rpb24gKHYsIGkpIHtcbiAgICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICAgIHZhciBfdGFiSW5kZXggPSB0YWJJbmRleFtpXSB8fCAwO1xuICAgICAgICBpZiAoZGlzYWJsZWQgfHwgdGFiSW5kZXhbaV0gPT09IG51bGwpIHtcbiAgICAgICAgICBfdGFiSW5kZXggPSBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBoYW5kbGVHZW5lcmF0b3Ioe1xuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcygoX2NsYXNzTmFtZXMgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBoYW5kbGVDbGFzc05hbWUsIHRydWUpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIGhhbmRsZUNsYXNzTmFtZSArICctJyArIChpICsgMSksIHRydWUpLCBfY2xhc3NOYW1lcykpLFxuICAgICAgICAgIHByZWZpeENsczogcHJlZml4Q2xzLFxuICAgICAgICAgIHZlcnRpY2FsOiB2ZXJ0aWNhbCxcbiAgICAgICAgICBvZmZzZXQ6IG9mZnNldHNbaV0sXG4gICAgICAgICAgdmFsdWU6IHYsXG4gICAgICAgICAgZHJhZ2dpbmc6IGhhbmRsZSA9PT0gaSxcbiAgICAgICAgICBpbmRleDogaSxcbiAgICAgICAgICB0YWJJbmRleDogX3RhYkluZGV4LFxuICAgICAgICAgIG1pbjogbWluLFxuICAgICAgICAgIG1heDogbWF4LFxuICAgICAgICAgIHJldmVyc2U6IHJldmVyc2UsXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkLFxuICAgICAgICAgIHN0eWxlOiBoYW5kbGVTdHlsZVtpXSxcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihoKSB7XG4gICAgICAgICAgICByZXR1cm4gX3RoaXM0LnNhdmVIYW5kbGUoaSwgaCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuXG4gICAgICB2YXIgdHJhY2tzID0gYm91bmRzLnNsaWNlKDAsIC0xKS5tYXAoZnVuY3Rpb24gKF8sIGluZGV4KSB7XG4gICAgICAgIHZhciBfY2xhc3NOYW1lczI7XG5cbiAgICAgICAgdmFyIGkgPSBpbmRleCArIDE7XG4gICAgICAgIHZhciB0cmFja0NsYXNzTmFtZSA9IGNsYXNzTmFtZXMoKF9jbGFzc05hbWVzMiA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMyLCBwcmVmaXhDbHMgKyAnLXRyYWNrJywgdHJ1ZSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lczIsIHByZWZpeENscyArICctdHJhY2stJyArIGksIHRydWUpLCBfY2xhc3NOYW1lczIpKTtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoVHJhY2ssIHtcbiAgICAgICAgICBjbGFzc05hbWU6IHRyYWNrQ2xhc3NOYW1lLFxuICAgICAgICAgIHZlcnRpY2FsOiB2ZXJ0aWNhbCxcbiAgICAgICAgICByZXZlcnNlOiByZXZlcnNlLFxuICAgICAgICAgIGluY2x1ZGVkOiBpbmNsdWRlZCxcbiAgICAgICAgICBvZmZzZXQ6IG9mZnNldHNbaSAtIDFdLFxuICAgICAgICAgIGxlbmd0aDogb2Zmc2V0c1tpXSAtIG9mZnNldHNbaSAtIDFdLFxuICAgICAgICAgIHN0eWxlOiB0cmFja1N0eWxlW2luZGV4XSxcbiAgICAgICAgICBrZXk6IGlcbiAgICAgICAgfSk7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIHsgdHJhY2tzOiB0cmFja3MsIGhhbmRsZXM6IGhhbmRsZXMgfTtcbiAgICB9XG4gIH1dLCBbe1xuICAgIGtleTogJ2dldERlcml2ZWRTdGF0ZUZyb21Qcm9wcycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhwcm9wcywgc3RhdGUpIHtcbiAgICAgIGlmICgndmFsdWUnIGluIHByb3BzIHx8ICdtaW4nIGluIHByb3BzIHx8ICdtYXgnIGluIHByb3BzKSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IHByb3BzLnZhbHVlIHx8IHN0YXRlLmJvdW5kcztcbiAgICAgICAgdmFyIG5leHRCb3VuZHMgPSB2YWx1ZS5tYXAoZnVuY3Rpb24gKHYsIGkpIHtcbiAgICAgICAgICByZXR1cm4gX3RyaW1BbGlnblZhbHVlKHtcbiAgICAgICAgICAgIHZhbHVlOiB2LFxuICAgICAgICAgICAgaGFuZGxlOiBpLFxuICAgICAgICAgICAgYm91bmRzOiBzdGF0ZS5ib3VuZHMsXG4gICAgICAgICAgICBwcm9wczogcHJvcHNcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChuZXh0Qm91bmRzLmxlbmd0aCA9PT0gc3RhdGUuYm91bmRzLmxlbmd0aCAmJiBuZXh0Qm91bmRzLmV2ZXJ5KGZ1bmN0aW9uICh2LCBpKSB7XG4gICAgICAgICAgcmV0dXJuIHYgPT09IHN0YXRlLmJvdW5kc1tpXTtcbiAgICAgICAgfSkpIHtcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gX2V4dGVuZHMoe30sIHN0YXRlLCB7XG4gICAgICAgICAgYm91bmRzOiBuZXh0Qm91bmRzXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFJhbmdlO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5SYW5nZS5kaXNwbGF5TmFtZSA9ICdSYW5nZSc7XG5SYW5nZS5wcm9wVHlwZXMgPSB7XG4gIGF1dG9Gb2N1czogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRWYWx1ZTogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm51bWJlciksXG4gIHZhbHVlOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMubnVtYmVyKSxcbiAgY291bnQ6IFByb3BUeXBlcy5udW1iZXIsXG4gIHB1c2hhYmxlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuYm9vbCwgUHJvcFR5cGVzLm51bWJlcl0pLFxuICBhbGxvd0Nyb3NzOiBQcm9wVHlwZXMuYm9vbCxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICByZXZlcnNlOiBQcm9wVHlwZXMuYm9vbCxcbiAgdGFiSW5kZXg6IFByb3BUeXBlcy5hcnJheU9mKFByb3BUeXBlcy5udW1iZXIpLFxuICBtaW46IFByb3BUeXBlcy5udW1iZXIsXG4gIG1heDogUHJvcFR5cGVzLm51bWJlclxufTtcblJhbmdlLmRlZmF1bHRQcm9wcyA9IHtcbiAgY291bnQ6IDEsXG4gIGFsbG93Q3Jvc3M6IHRydWUsXG4gIHB1c2hhYmxlOiBmYWxzZSxcbiAgdGFiSW5kZXg6IFtdXG59O1xuXG5cbnBvbHlmaWxsKFJhbmdlKTtcblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlU2xpZGVyKFJhbmdlKTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeEJBO0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBeEJBO0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUExQkE7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWxCQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOzs7OztBQVJBO0FBY0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUF4QkE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBDQTtBQXNDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExQkE7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFoQkE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUF0QkE7QUF3QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBYkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQWtCQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXhFQTtBQTBFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBdkJBO0FBQ0E7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/Range.js
