__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "warnOnlyTreeNode", function() { return warnOnlyTreeNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrDel", function() { return arrDel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "arrAdd", function() { return arrAdd; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "posToArr", function() { return posToArr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPosition", function() { return getPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTreeNode", function() { return isTreeNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getNodeChildren", function() { return getNodeChildren; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isCheckDisabled", function() { return isCheckDisabled; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "traverseTreeNodes", function() { return traverseTreeNodes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapChildren", function() { return mapChildren; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDragNodesKeys", function() { return getDragNodesKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calcDropPosition", function() { return calcDropPosition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calcSelectedKeys", function() { return calcSelectedKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertDataToTree", function() { return convertDataToTree; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertTreeToEntities", function() { return convertTreeToEntities; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseCheckedKeys", function() { return parseCheckedKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conductCheck", function() { return conductCheck; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conductExpandParent", function() { return conductExpandParent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDataAndAria", function() { return getDataAndAria; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _TreeNode__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TreeNode */ "./node_modules/rc-tree/es/TreeNode.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}





var DRAG_SIDE_RANGE = 0.25;
var DRAG_MIN_GAP = 2;
var onlyTreeNodeWarned = false;
function warnOnlyTreeNode() {
  if (onlyTreeNodeWarned) return;
  onlyTreeNodeWarned = true;
  warning__WEBPACK_IMPORTED_MODULE_2___default()(false, 'Tree only accept TreeNode as children.');
}
function arrDel(list, value) {
  var clone = list.slice();
  var index = clone.indexOf(value);

  if (index >= 0) {
    clone.splice(index, 1);
  }

  return clone;
}
function arrAdd(list, value) {
  var clone = list.slice();

  if (clone.indexOf(value) === -1) {
    clone.push(value);
  }

  return clone;
}
function posToArr(pos) {
  return pos.split('-');
}
function getPosition(level, index) {
  return "".concat(level, "-").concat(index);
}
function isTreeNode(node) {
  return node && node.type && node.type.isTreeNode;
}
function getNodeChildren(children) {
  return Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_1__["default"])(children).filter(isTreeNode);
}
function isCheckDisabled(node) {
  var _ref = node.props || {},
      disabled = _ref.disabled,
      disableCheckbox = _ref.disableCheckbox,
      checkable = _ref.checkable;

  return !!(disabled || disableCheckbox) || checkable === false;
}
function traverseTreeNodes(treeNodes, callback) {
  function processNode(node, index, parent) {
    var children = node ? node.props.children : treeNodes;
    var pos = node ? getPosition(parent.pos, index) : 0; // Filter children

    var childList = getNodeChildren(children); // Process node if is not root

    if (node) {
      var data = {
        node: node,
        index: index,
        pos: pos,
        key: node.key || pos,
        parentPos: parent.node ? parent.pos : null
      };
      callback(data);
    } // Process children node


    react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(childList, function (subNode, subIndex) {
      processNode(subNode, subIndex, {
        node: node,
        pos: pos
      });
    });
  }

  processNode(null);
}
/**
 * Use `rc-util` `toArray` to get the children list which keeps the key.
 * And return single node if children is only one(This can avoid `key` missing check).
 */

function mapChildren(children, func) {
  var list = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_1__["default"])(children).map(func);

  if (list.length === 1) {
    return list[0];
  }

  return list;
}
function getDragNodesKeys(treeNodes, node) {
  var _node$props = node.props,
      eventKey = _node$props.eventKey,
      pos = _node$props.pos;
  var dragNodesKeys = [];
  traverseTreeNodes(treeNodes, function (_ref2) {
    var key = _ref2.key;
    dragNodesKeys.push(key);
  });
  dragNodesKeys.push(eventKey || pos);
  return dragNodesKeys;
} // Only used when drag, not affect SSR.

function calcDropPosition(event, treeNode) {
  var clientY = event.clientY;

  var _treeNode$selectHandl = treeNode.selectHandle.getBoundingClientRect(),
      top = _treeNode$selectHandl.top,
      bottom = _treeNode$selectHandl.bottom,
      height = _treeNode$selectHandl.height;

  var des = Math.max(height * DRAG_SIDE_RANGE, DRAG_MIN_GAP);

  if (clientY <= top + des) {
    return -1;
  }

  if (clientY >= bottom - des) {
    return 1;
  }

  return 0;
}
/**
 * Return selectedKeys according with multiple prop
 * @param selectedKeys
 * @param props
 * @returns [string]
 */

function calcSelectedKeys(selectedKeys, props) {
  if (!selectedKeys) return undefined;
  var multiple = props.multiple;

  if (multiple) {
    return selectedKeys.slice();
  }

  if (selectedKeys.length) {
    return [selectedKeys[0]];
  }

  return selectedKeys;
}
/**
 * Since React internal will convert key to string,
 * we need do this to avoid `checkStrictly` use number match
 */

function keyListToString(keyList) {
  if (!keyList) return keyList;
  return keyList.map(function (key) {
    return String(key);
  });
}

var internalProcessProps = function internalProcessProps(props) {
  return props;
};

function convertDataToTree(treeData, processor) {
  if (!treeData) return [];

  var _ref3 = processor || {},
      _ref3$processProps = _ref3.processProps,
      processProps = _ref3$processProps === void 0 ? internalProcessProps : _ref3$processProps;

  var list = Array.isArray(treeData) ? treeData : [treeData];
  return list.map(function (_ref4) {
    var children = _ref4.children,
        props = _objectWithoutProperties(_ref4, ["children"]);

    var childrenNodes = convertDataToTree(children, processor);
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_TreeNode__WEBPACK_IMPORTED_MODULE_3__["default"], Object.assign({}, processProps(props)), childrenNodes);
  });
}
/**
 * Calculate treeNodes entities. `processTreeEntity` is used for `rc-tree-select`
 * @param treeNodes
 * @param processTreeEntity  User can customize the entity
 */

function convertTreeToEntities(treeNodes) {
  var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      initWrapper = _ref5.initWrapper,
      processEntity = _ref5.processEntity,
      onProcessFinished = _ref5.onProcessFinished;

  var posEntities = {};
  var keyEntities = {};
  var wrapper = {
    posEntities: posEntities,
    keyEntities: keyEntities
  };

  if (initWrapper) {
    wrapper = initWrapper(wrapper) || wrapper;
  }

  traverseTreeNodes(treeNodes, function (item) {
    var node = item.node,
        index = item.index,
        pos = item.pos,
        key = item.key,
        parentPos = item.parentPos;
    var entity = {
      node: node,
      index: index,
      key: key,
      pos: pos
    };
    posEntities[pos] = entity;
    keyEntities[key] = entity; // Fill children

    entity.parent = posEntities[parentPos];

    if (entity.parent) {
      entity.parent.children = entity.parent.children || [];
      entity.parent.children.push(entity);
    }

    if (processEntity) {
      processEntity(entity, wrapper);
    }
  });

  if (onProcessFinished) {
    onProcessFinished(wrapper);
  }

  return wrapper;
}
/**
 * Parse `checkedKeys` to { checkedKeys, halfCheckedKeys } style
 */

function parseCheckedKeys(keys) {
  if (!keys) {
    return null;
  } // Convert keys to object format


  var keyProps;

  if (Array.isArray(keys)) {
    // [Legacy] Follow the api doc
    keyProps = {
      checkedKeys: keys,
      halfCheckedKeys: undefined
    };
  } else if (_typeof(keys) === 'object') {
    keyProps = {
      checkedKeys: keys.checked || undefined,
      halfCheckedKeys: keys.halfChecked || undefined
    };
  } else {
    warning__WEBPACK_IMPORTED_MODULE_2___default()(false, '`checkedKeys` is not an array or an object');
    return null;
  }

  keyProps.checkedKeys = keyListToString(keyProps.checkedKeys);
  keyProps.halfCheckedKeys = keyListToString(keyProps.halfCheckedKeys);
  return keyProps;
}
/**
 * Conduct check state by the keyList. It will conduct up & from the provided key.
 * If the conduct path reach the disabled or already checked / unchecked node will stop conduct.
 */

function conductCheck(
/** list of keys */
keyList,
/** is check the node or not */
isCheck,
/** parsed by `convertTreeToEntities` function in Tree */
keyEntities) {
  var checkStatus = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  var checkedKeys = {};
  var halfCheckedKeys = {}; // Record the key has some child checked (include child half checked)

  (checkStatus.checkedKeys || []).forEach(function (key) {
    checkedKeys[key] = true;
  });
  (checkStatus.halfCheckedKeys || []).forEach(function (key) {
    halfCheckedKeys[key] = true;
  }); // Conduct up

  function conductUp(key) {
    if (checkedKeys[key] === isCheck) return;
    var entity = keyEntities[key];
    if (!entity) return;
    var children = entity.children,
        parent = entity.parent,
        node = entity.node;
    if (isCheckDisabled(node)) return; // Check child node checked status

    var everyChildChecked = true;
    var someChildChecked = false; // Child checked or half checked

    (children || []).filter(function (child) {
      return !isCheckDisabled(child.node);
    }).forEach(function (_ref6) {
      var childKey = _ref6.key;
      var childChecked = checkedKeys[childKey];
      var childHalfChecked = halfCheckedKeys[childKey];
      if (childChecked || childHalfChecked) someChildChecked = true;
      if (!childChecked) everyChildChecked = false;
    }); // Update checked status

    if (isCheck) {
      checkedKeys[key] = everyChildChecked;
    } else {
      checkedKeys[key] = false;
    }

    halfCheckedKeys[key] = someChildChecked;

    if (parent) {
      conductUp(parent.key);
    }
  } // Conduct down


  function conductDown(key) {
    if (checkedKeys[key] === isCheck) return;
    var entity = keyEntities[key];
    if (!entity) return;
    var children = entity.children,
        node = entity.node;
    if (isCheckDisabled(node)) return;
    checkedKeys[key] = isCheck;
    (children || []).forEach(function (child) {
      conductDown(child.key);
    });
  }

  function conduct(key) {
    var entity = keyEntities[key];

    if (!entity) {
      warning__WEBPACK_IMPORTED_MODULE_2___default()(false, "'".concat(key, "' does not exist in the tree."));
      return;
    }

    var children = entity.children,
        parent = entity.parent,
        node = entity.node;
    checkedKeys[key] = isCheck;
    if (isCheckDisabled(node)) return; // Conduct down

    (children || []).filter(function (child) {
      return !isCheckDisabled(child.node);
    }).forEach(function (child) {
      conductDown(child.key);
    }); // Conduct up

    if (parent) {
      conductUp(parent.key);
    }
  }

  (keyList || []).forEach(function (key) {
    conduct(key);
  });
  var checkedKeyList = [];
  var halfCheckedKeyList = []; // Fill checked list

  Object.keys(checkedKeys).forEach(function (key) {
    if (checkedKeys[key]) {
      checkedKeyList.push(key);
    }
  }); // Fill half checked list

  Object.keys(halfCheckedKeys).forEach(function (key) {
    if (!checkedKeys[key] && halfCheckedKeys[key]) {
      halfCheckedKeyList.push(key);
    }
  });
  return {
    checkedKeys: checkedKeyList,
    halfCheckedKeys: halfCheckedKeyList
  };
}
/**
 * If user use `autoExpandParent` we should get the list of parent node
 * @param keyList
 * @param keyEntities
 */

function conductExpandParent(keyList, keyEntities) {
  var expandedKeys = {};

  function conductUp(key) {
    if (expandedKeys[key]) return;
    var entity = keyEntities[key];
    if (!entity) return;
    expandedKeys[key] = true;
    var parent = entity.parent,
        node = entity.node;
    if (node.props && node.props.disabled) return;

    if (parent) {
      conductUp(parent.key);
    }
  }

  (keyList || []).forEach(function (key) {
    conductUp(key);
  });
  return Object.keys(expandedKeys);
}
/**
 * Returns only the data- and aria- key/value pairs
 */

function getDataAndAria(props) {
  return Object.keys(props).reduce(function (prev, key) {
    if (key.substr(0, 5) === 'data-' || key.substr(0, 5) === 'aria-') {
      prev[key] = props[key];
    }

    return prev;
  }, {});
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS9lcy91dGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdHJlZS9lcy91dGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhzb3VyY2UsIGV4Y2x1ZGVkKSB7IGlmIChzb3VyY2UgPT0gbnVsbCkgcmV0dXJuIHt9OyB2YXIgdGFyZ2V0ID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2Uoc291cmNlLCBleGNsdWRlZCk7IHZhciBrZXksIGk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzb3VyY2VTeW1ib2xLZXlzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzb3VyY2UpOyBmb3IgKGkgPSAwOyBpIDwgc291cmNlU3ltYm9sS2V5cy5sZW5ndGg7IGkrKykgeyBrZXkgPSBzb3VyY2VTeW1ib2xLZXlzW2ldOyBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzb3VyY2UsIGtleSkpIGNvbnRpbnVlOyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKSB7IGlmIChzb3VyY2UgPT0gbnVsbCkgcmV0dXJuIHt9OyB2YXIgdGFyZ2V0ID0ge307IHZhciBzb3VyY2VLZXlzID0gT2JqZWN0LmtleXMoc291cmNlKTsgdmFyIGtleSwgaTsgZm9yIChpID0gMDsgaSA8IHNvdXJjZUtleXMubGVuZ3RoOyBpKyspIHsga2V5ID0gc291cmNlS2V5c1tpXTsgaWYgKGV4Y2x1ZGVkLmluZGV4T2Yoa2V5KSA+PSAwKSBjb250aW51ZTsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmltcG9ydCBSZWFjdCwgeyBDaGlsZHJlbiB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB0b0FycmF5IGZyb20gXCJyYy11dGlsL2VzL0NoaWxkcmVuL3RvQXJyYXlcIjtcbmltcG9ydCB3YXJuaW5nIGZyb20gJ3dhcm5pbmcnO1xuaW1wb3J0IFRyZWVOb2RlIGZyb20gJy4vVHJlZU5vZGUnO1xudmFyIERSQUdfU0lERV9SQU5HRSA9IDAuMjU7XG52YXIgRFJBR19NSU5fR0FQID0gMjtcbnZhciBvbmx5VHJlZU5vZGVXYXJuZWQgPSBmYWxzZTtcbmV4cG9ydCBmdW5jdGlvbiB3YXJuT25seVRyZWVOb2RlKCkge1xuICBpZiAob25seVRyZWVOb2RlV2FybmVkKSByZXR1cm47XG4gIG9ubHlUcmVlTm9kZVdhcm5lZCA9IHRydWU7XG4gIHdhcm5pbmcoZmFsc2UsICdUcmVlIG9ubHkgYWNjZXB0IFRyZWVOb2RlIGFzIGNoaWxkcmVuLicpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGFyckRlbChsaXN0LCB2YWx1ZSkge1xuICB2YXIgY2xvbmUgPSBsaXN0LnNsaWNlKCk7XG4gIHZhciBpbmRleCA9IGNsb25lLmluZGV4T2YodmFsdWUpO1xuXG4gIGlmIChpbmRleCA+PSAwKSB7XG4gICAgY2xvbmUuc3BsaWNlKGluZGV4LCAxKTtcbiAgfVxuXG4gIHJldHVybiBjbG9uZTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBhcnJBZGQobGlzdCwgdmFsdWUpIHtcbiAgdmFyIGNsb25lID0gbGlzdC5zbGljZSgpO1xuXG4gIGlmIChjbG9uZS5pbmRleE9mKHZhbHVlKSA9PT0gLTEpIHtcbiAgICBjbG9uZS5wdXNoKHZhbHVlKTtcbiAgfVxuXG4gIHJldHVybiBjbG9uZTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBwb3NUb0Fycihwb3MpIHtcbiAgcmV0dXJuIHBvcy5zcGxpdCgnLScpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldFBvc2l0aW9uKGxldmVsLCBpbmRleCkge1xuICByZXR1cm4gXCJcIi5jb25jYXQobGV2ZWwsIFwiLVwiKS5jb25jYXQoaW5kZXgpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGlzVHJlZU5vZGUobm9kZSkge1xuICByZXR1cm4gbm9kZSAmJiBub2RlLnR5cGUgJiYgbm9kZS50eXBlLmlzVHJlZU5vZGU7XG59XG5leHBvcnQgZnVuY3Rpb24gZ2V0Tm9kZUNoaWxkcmVuKGNoaWxkcmVuKSB7XG4gIHJldHVybiB0b0FycmF5KGNoaWxkcmVuKS5maWx0ZXIoaXNUcmVlTm9kZSk7XG59XG5leHBvcnQgZnVuY3Rpb24gaXNDaGVja0Rpc2FibGVkKG5vZGUpIHtcbiAgdmFyIF9yZWYgPSBub2RlLnByb3BzIHx8IHt9LFxuICAgICAgZGlzYWJsZWQgPSBfcmVmLmRpc2FibGVkLFxuICAgICAgZGlzYWJsZUNoZWNrYm94ID0gX3JlZi5kaXNhYmxlQ2hlY2tib3gsXG4gICAgICBjaGVja2FibGUgPSBfcmVmLmNoZWNrYWJsZTtcblxuICByZXR1cm4gISEoZGlzYWJsZWQgfHwgZGlzYWJsZUNoZWNrYm94KSB8fCBjaGVja2FibGUgPT09IGZhbHNlO1xufVxuZXhwb3J0IGZ1bmN0aW9uIHRyYXZlcnNlVHJlZU5vZGVzKHRyZWVOb2RlcywgY2FsbGJhY2spIHtcbiAgZnVuY3Rpb24gcHJvY2Vzc05vZGUobm9kZSwgaW5kZXgsIHBhcmVudCkge1xuICAgIHZhciBjaGlsZHJlbiA9IG5vZGUgPyBub2RlLnByb3BzLmNoaWxkcmVuIDogdHJlZU5vZGVzO1xuICAgIHZhciBwb3MgPSBub2RlID8gZ2V0UG9zaXRpb24ocGFyZW50LnBvcywgaW5kZXgpIDogMDsgLy8gRmlsdGVyIGNoaWxkcmVuXG5cbiAgICB2YXIgY2hpbGRMaXN0ID0gZ2V0Tm9kZUNoaWxkcmVuKGNoaWxkcmVuKTsgLy8gUHJvY2VzcyBub2RlIGlmIGlzIG5vdCByb290XG5cbiAgICBpZiAobm9kZSkge1xuICAgICAgdmFyIGRhdGEgPSB7XG4gICAgICAgIG5vZGU6IG5vZGUsXG4gICAgICAgIGluZGV4OiBpbmRleCxcbiAgICAgICAgcG9zOiBwb3MsXG4gICAgICAgIGtleTogbm9kZS5rZXkgfHwgcG9zLFxuICAgICAgICBwYXJlbnRQb3M6IHBhcmVudC5ub2RlID8gcGFyZW50LnBvcyA6IG51bGxcbiAgICAgIH07XG4gICAgICBjYWxsYmFjayhkYXRhKTtcbiAgICB9IC8vIFByb2Nlc3MgY2hpbGRyZW4gbm9kZVxuXG5cbiAgICBDaGlsZHJlbi5mb3JFYWNoKGNoaWxkTGlzdCwgZnVuY3Rpb24gKHN1Yk5vZGUsIHN1YkluZGV4KSB7XG4gICAgICBwcm9jZXNzTm9kZShzdWJOb2RlLCBzdWJJbmRleCwge1xuICAgICAgICBub2RlOiBub2RlLFxuICAgICAgICBwb3M6IHBvc1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcm9jZXNzTm9kZShudWxsKTtcbn1cbi8qKlxuICogVXNlIGByYy11dGlsYCBgdG9BcnJheWAgdG8gZ2V0IHRoZSBjaGlsZHJlbiBsaXN0IHdoaWNoIGtlZXBzIHRoZSBrZXkuXG4gKiBBbmQgcmV0dXJuIHNpbmdsZSBub2RlIGlmIGNoaWxkcmVuIGlzIG9ubHkgb25lKFRoaXMgY2FuIGF2b2lkIGBrZXlgIG1pc3NpbmcgY2hlY2spLlxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBtYXBDaGlsZHJlbihjaGlsZHJlbiwgZnVuYykge1xuICB2YXIgbGlzdCA9IHRvQXJyYXkoY2hpbGRyZW4pLm1hcChmdW5jKTtcblxuICBpZiAobGlzdC5sZW5ndGggPT09IDEpIHtcbiAgICByZXR1cm4gbGlzdFswXTtcbiAgfVxuXG4gIHJldHVybiBsaXN0O1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldERyYWdOb2Rlc0tleXModHJlZU5vZGVzLCBub2RlKSB7XG4gIHZhciBfbm9kZSRwcm9wcyA9IG5vZGUucHJvcHMsXG4gICAgICBldmVudEtleSA9IF9ub2RlJHByb3BzLmV2ZW50S2V5LFxuICAgICAgcG9zID0gX25vZGUkcHJvcHMucG9zO1xuICB2YXIgZHJhZ05vZGVzS2V5cyA9IFtdO1xuICB0cmF2ZXJzZVRyZWVOb2Rlcyh0cmVlTm9kZXMsIGZ1bmN0aW9uIChfcmVmMikge1xuICAgIHZhciBrZXkgPSBfcmVmMi5rZXk7XG4gICAgZHJhZ05vZGVzS2V5cy5wdXNoKGtleSk7XG4gIH0pO1xuICBkcmFnTm9kZXNLZXlzLnB1c2goZXZlbnRLZXkgfHwgcG9zKTtcbiAgcmV0dXJuIGRyYWdOb2Rlc0tleXM7XG59IC8vIE9ubHkgdXNlZCB3aGVuIGRyYWcsIG5vdCBhZmZlY3QgU1NSLlxuXG5leHBvcnQgZnVuY3Rpb24gY2FsY0Ryb3BQb3NpdGlvbihldmVudCwgdHJlZU5vZGUpIHtcbiAgdmFyIGNsaWVudFkgPSBldmVudC5jbGllbnRZO1xuXG4gIHZhciBfdHJlZU5vZGUkc2VsZWN0SGFuZGwgPSB0cmVlTm9kZS5zZWxlY3RIYW5kbGUuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICB0b3AgPSBfdHJlZU5vZGUkc2VsZWN0SGFuZGwudG9wLFxuICAgICAgYm90dG9tID0gX3RyZWVOb2RlJHNlbGVjdEhhbmRsLmJvdHRvbSxcbiAgICAgIGhlaWdodCA9IF90cmVlTm9kZSRzZWxlY3RIYW5kbC5oZWlnaHQ7XG5cbiAgdmFyIGRlcyA9IE1hdGgubWF4KGhlaWdodCAqIERSQUdfU0lERV9SQU5HRSwgRFJBR19NSU5fR0FQKTtcblxuICBpZiAoY2xpZW50WSA8PSB0b3AgKyBkZXMpIHtcbiAgICByZXR1cm4gLTE7XG4gIH1cblxuICBpZiAoY2xpZW50WSA+PSBib3R0b20gLSBkZXMpIHtcbiAgICByZXR1cm4gMTtcbiAgfVxuXG4gIHJldHVybiAwO1xufVxuLyoqXG4gKiBSZXR1cm4gc2VsZWN0ZWRLZXlzIGFjY29yZGluZyB3aXRoIG11bHRpcGxlIHByb3BcbiAqIEBwYXJhbSBzZWxlY3RlZEtleXNcbiAqIEBwYXJhbSBwcm9wc1xuICogQHJldHVybnMgW3N0cmluZ11cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gY2FsY1NlbGVjdGVkS2V5cyhzZWxlY3RlZEtleXMsIHByb3BzKSB7XG4gIGlmICghc2VsZWN0ZWRLZXlzKSByZXR1cm4gdW5kZWZpbmVkO1xuICB2YXIgbXVsdGlwbGUgPSBwcm9wcy5tdWx0aXBsZTtcblxuICBpZiAobXVsdGlwbGUpIHtcbiAgICByZXR1cm4gc2VsZWN0ZWRLZXlzLnNsaWNlKCk7XG4gIH1cblxuICBpZiAoc2VsZWN0ZWRLZXlzLmxlbmd0aCkge1xuICAgIHJldHVybiBbc2VsZWN0ZWRLZXlzWzBdXTtcbiAgfVxuXG4gIHJldHVybiBzZWxlY3RlZEtleXM7XG59XG4vKipcbiAqIFNpbmNlIFJlYWN0IGludGVybmFsIHdpbGwgY29udmVydCBrZXkgdG8gc3RyaW5nLFxuICogd2UgbmVlZCBkbyB0aGlzIHRvIGF2b2lkIGBjaGVja1N0cmljdGx5YCB1c2UgbnVtYmVyIG1hdGNoXG4gKi9cblxuZnVuY3Rpb24ga2V5TGlzdFRvU3RyaW5nKGtleUxpc3QpIHtcbiAgaWYgKCFrZXlMaXN0KSByZXR1cm4ga2V5TGlzdDtcbiAgcmV0dXJuIGtleUxpc3QubWFwKGZ1bmN0aW9uIChrZXkpIHtcbiAgICByZXR1cm4gU3RyaW5nKGtleSk7XG4gIH0pO1xufVxuXG52YXIgaW50ZXJuYWxQcm9jZXNzUHJvcHMgPSBmdW5jdGlvbiBpbnRlcm5hbFByb2Nlc3NQcm9wcyhwcm9wcykge1xuICByZXR1cm4gcHJvcHM7XG59O1xuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydERhdGFUb1RyZWUodHJlZURhdGEsIHByb2Nlc3Nvcikge1xuICBpZiAoIXRyZWVEYXRhKSByZXR1cm4gW107XG5cbiAgdmFyIF9yZWYzID0gcHJvY2Vzc29yIHx8IHt9LFxuICAgICAgX3JlZjMkcHJvY2Vzc1Byb3BzID0gX3JlZjMucHJvY2Vzc1Byb3BzLFxuICAgICAgcHJvY2Vzc1Byb3BzID0gX3JlZjMkcHJvY2Vzc1Byb3BzID09PSB2b2lkIDAgPyBpbnRlcm5hbFByb2Nlc3NQcm9wcyA6IF9yZWYzJHByb2Nlc3NQcm9wcztcblxuICB2YXIgbGlzdCA9IEFycmF5LmlzQXJyYXkodHJlZURhdGEpID8gdHJlZURhdGEgOiBbdHJlZURhdGFdO1xuICByZXR1cm4gbGlzdC5tYXAoZnVuY3Rpb24gKF9yZWY0KSB7XG4gICAgdmFyIGNoaWxkcmVuID0gX3JlZjQuY2hpbGRyZW4sXG4gICAgICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9yZWY0LCBbXCJjaGlsZHJlblwiXSk7XG5cbiAgICB2YXIgY2hpbGRyZW5Ob2RlcyA9IGNvbnZlcnREYXRhVG9UcmVlKGNoaWxkcmVuLCBwcm9jZXNzb3IpO1xuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFRyZWVOb2RlLCBPYmplY3QuYXNzaWduKHt9LCBwcm9jZXNzUHJvcHMocHJvcHMpKSwgY2hpbGRyZW5Ob2Rlcyk7XG4gIH0pO1xufVxuLyoqXG4gKiBDYWxjdWxhdGUgdHJlZU5vZGVzIGVudGl0aWVzLiBgcHJvY2Vzc1RyZWVFbnRpdHlgIGlzIHVzZWQgZm9yIGByYy10cmVlLXNlbGVjdGBcbiAqIEBwYXJhbSB0cmVlTm9kZXNcbiAqIEBwYXJhbSBwcm9jZXNzVHJlZUVudGl0eSAgVXNlciBjYW4gY3VzdG9taXplIHRoZSBlbnRpdHlcbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydFRyZWVUb0VudGl0aWVzKHRyZWVOb2Rlcykge1xuICB2YXIgX3JlZjUgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9LFxuICAgICAgaW5pdFdyYXBwZXIgPSBfcmVmNS5pbml0V3JhcHBlcixcbiAgICAgIHByb2Nlc3NFbnRpdHkgPSBfcmVmNS5wcm9jZXNzRW50aXR5LFxuICAgICAgb25Qcm9jZXNzRmluaXNoZWQgPSBfcmVmNS5vblByb2Nlc3NGaW5pc2hlZDtcblxuICB2YXIgcG9zRW50aXRpZXMgPSB7fTtcbiAgdmFyIGtleUVudGl0aWVzID0ge307XG4gIHZhciB3cmFwcGVyID0ge1xuICAgIHBvc0VudGl0aWVzOiBwb3NFbnRpdGllcyxcbiAgICBrZXlFbnRpdGllczoga2V5RW50aXRpZXNcbiAgfTtcblxuICBpZiAoaW5pdFdyYXBwZXIpIHtcbiAgICB3cmFwcGVyID0gaW5pdFdyYXBwZXIod3JhcHBlcikgfHwgd3JhcHBlcjtcbiAgfVxuXG4gIHRyYXZlcnNlVHJlZU5vZGVzKHRyZWVOb2RlcywgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICB2YXIgbm9kZSA9IGl0ZW0ubm9kZSxcbiAgICAgICAgaW5kZXggPSBpdGVtLmluZGV4LFxuICAgICAgICBwb3MgPSBpdGVtLnBvcyxcbiAgICAgICAga2V5ID0gaXRlbS5rZXksXG4gICAgICAgIHBhcmVudFBvcyA9IGl0ZW0ucGFyZW50UG9zO1xuICAgIHZhciBlbnRpdHkgPSB7XG4gICAgICBub2RlOiBub2RlLFxuICAgICAgaW5kZXg6IGluZGV4LFxuICAgICAga2V5OiBrZXksXG4gICAgICBwb3M6IHBvc1xuICAgIH07XG4gICAgcG9zRW50aXRpZXNbcG9zXSA9IGVudGl0eTtcbiAgICBrZXlFbnRpdGllc1trZXldID0gZW50aXR5OyAvLyBGaWxsIGNoaWxkcmVuXG5cbiAgICBlbnRpdHkucGFyZW50ID0gcG9zRW50aXRpZXNbcGFyZW50UG9zXTtcblxuICAgIGlmIChlbnRpdHkucGFyZW50KSB7XG4gICAgICBlbnRpdHkucGFyZW50LmNoaWxkcmVuID0gZW50aXR5LnBhcmVudC5jaGlsZHJlbiB8fCBbXTtcbiAgICAgIGVudGl0eS5wYXJlbnQuY2hpbGRyZW4ucHVzaChlbnRpdHkpO1xuICAgIH1cblxuICAgIGlmIChwcm9jZXNzRW50aXR5KSB7XG4gICAgICBwcm9jZXNzRW50aXR5KGVudGl0eSwgd3JhcHBlcik7XG4gICAgfVxuICB9KTtcblxuICBpZiAob25Qcm9jZXNzRmluaXNoZWQpIHtcbiAgICBvblByb2Nlc3NGaW5pc2hlZCh3cmFwcGVyKTtcbiAgfVxuXG4gIHJldHVybiB3cmFwcGVyO1xufVxuLyoqXG4gKiBQYXJzZSBgY2hlY2tlZEtleXNgIHRvIHsgY2hlY2tlZEtleXMsIGhhbGZDaGVja2VkS2V5cyB9IHN0eWxlXG4gKi9cblxuZXhwb3J0IGZ1bmN0aW9uIHBhcnNlQ2hlY2tlZEtleXMoa2V5cykge1xuICBpZiAoIWtleXMpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfSAvLyBDb252ZXJ0IGtleXMgdG8gb2JqZWN0IGZvcm1hdFxuXG5cbiAgdmFyIGtleVByb3BzO1xuXG4gIGlmIChBcnJheS5pc0FycmF5KGtleXMpKSB7XG4gICAgLy8gW0xlZ2FjeV0gRm9sbG93IHRoZSBhcGkgZG9jXG4gICAga2V5UHJvcHMgPSB7XG4gICAgICBjaGVja2VkS2V5czoga2V5cyxcbiAgICAgIGhhbGZDaGVja2VkS2V5czogdW5kZWZpbmVkXG4gICAgfTtcbiAgfSBlbHNlIGlmIChfdHlwZW9mKGtleXMpID09PSAnb2JqZWN0Jykge1xuICAgIGtleVByb3BzID0ge1xuICAgICAgY2hlY2tlZEtleXM6IGtleXMuY2hlY2tlZCB8fCB1bmRlZmluZWQsXG4gICAgICBoYWxmQ2hlY2tlZEtleXM6IGtleXMuaGFsZkNoZWNrZWQgfHwgdW5kZWZpbmVkXG4gICAgfTtcbiAgfSBlbHNlIHtcbiAgICB3YXJuaW5nKGZhbHNlLCAnYGNoZWNrZWRLZXlzYCBpcyBub3QgYW4gYXJyYXkgb3IgYW4gb2JqZWN0Jyk7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBrZXlQcm9wcy5jaGVja2VkS2V5cyA9IGtleUxpc3RUb1N0cmluZyhrZXlQcm9wcy5jaGVja2VkS2V5cyk7XG4gIGtleVByb3BzLmhhbGZDaGVja2VkS2V5cyA9IGtleUxpc3RUb1N0cmluZyhrZXlQcm9wcy5oYWxmQ2hlY2tlZEtleXMpO1xuICByZXR1cm4ga2V5UHJvcHM7XG59XG4vKipcbiAqIENvbmR1Y3QgY2hlY2sgc3RhdGUgYnkgdGhlIGtleUxpc3QuIEl0IHdpbGwgY29uZHVjdCB1cCAmIGZyb20gdGhlIHByb3ZpZGVkIGtleS5cbiAqIElmIHRoZSBjb25kdWN0IHBhdGggcmVhY2ggdGhlIGRpc2FibGVkIG9yIGFscmVhZHkgY2hlY2tlZCAvIHVuY2hlY2tlZCBub2RlIHdpbGwgc3RvcCBjb25kdWN0LlxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBjb25kdWN0Q2hlY2soXG4vKiogbGlzdCBvZiBrZXlzICovXG5rZXlMaXN0LFxuLyoqIGlzIGNoZWNrIHRoZSBub2RlIG9yIG5vdCAqL1xuaXNDaGVjayxcbi8qKiBwYXJzZWQgYnkgYGNvbnZlcnRUcmVlVG9FbnRpdGllc2AgZnVuY3Rpb24gaW4gVHJlZSAqL1xua2V5RW50aXRpZXMpIHtcbiAgdmFyIGNoZWNrU3RhdHVzID0gYXJndW1lbnRzLmxlbmd0aCA+IDMgJiYgYXJndW1lbnRzWzNdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbM10gOiB7fTtcbiAgdmFyIGNoZWNrZWRLZXlzID0ge307XG4gIHZhciBoYWxmQ2hlY2tlZEtleXMgPSB7fTsgLy8gUmVjb3JkIHRoZSBrZXkgaGFzIHNvbWUgY2hpbGQgY2hlY2tlZCAoaW5jbHVkZSBjaGlsZCBoYWxmIGNoZWNrZWQpXG5cbiAgKGNoZWNrU3RhdHVzLmNoZWNrZWRLZXlzIHx8IFtdKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICBjaGVja2VkS2V5c1trZXldID0gdHJ1ZTtcbiAgfSk7XG4gIChjaGVja1N0YXR1cy5oYWxmQ2hlY2tlZEtleXMgfHwgW10pLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIGhhbGZDaGVja2VkS2V5c1trZXldID0gdHJ1ZTtcbiAgfSk7IC8vIENvbmR1Y3QgdXBcblxuICBmdW5jdGlvbiBjb25kdWN0VXAoa2V5KSB7XG4gICAgaWYgKGNoZWNrZWRLZXlzW2tleV0gPT09IGlzQ2hlY2spIHJldHVybjtcbiAgICB2YXIgZW50aXR5ID0ga2V5RW50aXRpZXNba2V5XTtcbiAgICBpZiAoIWVudGl0eSkgcmV0dXJuO1xuICAgIHZhciBjaGlsZHJlbiA9IGVudGl0eS5jaGlsZHJlbixcbiAgICAgICAgcGFyZW50ID0gZW50aXR5LnBhcmVudCxcbiAgICAgICAgbm9kZSA9IGVudGl0eS5ub2RlO1xuICAgIGlmIChpc0NoZWNrRGlzYWJsZWQobm9kZSkpIHJldHVybjsgLy8gQ2hlY2sgY2hpbGQgbm9kZSBjaGVja2VkIHN0YXR1c1xuXG4gICAgdmFyIGV2ZXJ5Q2hpbGRDaGVja2VkID0gdHJ1ZTtcbiAgICB2YXIgc29tZUNoaWxkQ2hlY2tlZCA9IGZhbHNlOyAvLyBDaGlsZCBjaGVja2VkIG9yIGhhbGYgY2hlY2tlZFxuXG4gICAgKGNoaWxkcmVuIHx8IFtdKS5maWx0ZXIoZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICByZXR1cm4gIWlzQ2hlY2tEaXNhYmxlZChjaGlsZC5ub2RlKTtcbiAgICB9KS5mb3JFYWNoKGZ1bmN0aW9uIChfcmVmNikge1xuICAgICAgdmFyIGNoaWxkS2V5ID0gX3JlZjYua2V5O1xuICAgICAgdmFyIGNoaWxkQ2hlY2tlZCA9IGNoZWNrZWRLZXlzW2NoaWxkS2V5XTtcbiAgICAgIHZhciBjaGlsZEhhbGZDaGVja2VkID0gaGFsZkNoZWNrZWRLZXlzW2NoaWxkS2V5XTtcbiAgICAgIGlmIChjaGlsZENoZWNrZWQgfHwgY2hpbGRIYWxmQ2hlY2tlZCkgc29tZUNoaWxkQ2hlY2tlZCA9IHRydWU7XG4gICAgICBpZiAoIWNoaWxkQ2hlY2tlZCkgZXZlcnlDaGlsZENoZWNrZWQgPSBmYWxzZTtcbiAgICB9KTsgLy8gVXBkYXRlIGNoZWNrZWQgc3RhdHVzXG5cbiAgICBpZiAoaXNDaGVjaykge1xuICAgICAgY2hlY2tlZEtleXNba2V5XSA9IGV2ZXJ5Q2hpbGRDaGVja2VkO1xuICAgIH0gZWxzZSB7XG4gICAgICBjaGVja2VkS2V5c1trZXldID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaGFsZkNoZWNrZWRLZXlzW2tleV0gPSBzb21lQ2hpbGRDaGVja2VkO1xuXG4gICAgaWYgKHBhcmVudCkge1xuICAgICAgY29uZHVjdFVwKHBhcmVudC5rZXkpO1xuICAgIH1cbiAgfSAvLyBDb25kdWN0IGRvd25cblxuXG4gIGZ1bmN0aW9uIGNvbmR1Y3REb3duKGtleSkge1xuICAgIGlmIChjaGVja2VkS2V5c1trZXldID09PSBpc0NoZWNrKSByZXR1cm47XG4gICAgdmFyIGVudGl0eSA9IGtleUVudGl0aWVzW2tleV07XG4gICAgaWYgKCFlbnRpdHkpIHJldHVybjtcbiAgICB2YXIgY2hpbGRyZW4gPSBlbnRpdHkuY2hpbGRyZW4sXG4gICAgICAgIG5vZGUgPSBlbnRpdHkubm9kZTtcbiAgICBpZiAoaXNDaGVja0Rpc2FibGVkKG5vZGUpKSByZXR1cm47XG4gICAgY2hlY2tlZEtleXNba2V5XSA9IGlzQ2hlY2s7XG4gICAgKGNoaWxkcmVuIHx8IFtdKS5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgY29uZHVjdERvd24oY2hpbGQua2V5KTtcbiAgICB9KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNvbmR1Y3Qoa2V5KSB7XG4gICAgdmFyIGVudGl0eSA9IGtleUVudGl0aWVzW2tleV07XG5cbiAgICBpZiAoIWVudGl0eSkge1xuICAgICAgd2FybmluZyhmYWxzZSwgXCInXCIuY29uY2F0KGtleSwgXCInIGRvZXMgbm90IGV4aXN0IGluIHRoZSB0cmVlLlwiKSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGNoaWxkcmVuID0gZW50aXR5LmNoaWxkcmVuLFxuICAgICAgICBwYXJlbnQgPSBlbnRpdHkucGFyZW50LFxuICAgICAgICBub2RlID0gZW50aXR5Lm5vZGU7XG4gICAgY2hlY2tlZEtleXNba2V5XSA9IGlzQ2hlY2s7XG4gICAgaWYgKGlzQ2hlY2tEaXNhYmxlZChub2RlKSkgcmV0dXJuOyAvLyBDb25kdWN0IGRvd25cblxuICAgIChjaGlsZHJlbiB8fCBbXSkuZmlsdGVyKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgcmV0dXJuICFpc0NoZWNrRGlzYWJsZWQoY2hpbGQubm9kZSk7XG4gICAgfSkuZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgIGNvbmR1Y3REb3duKGNoaWxkLmtleSk7XG4gICAgfSk7IC8vIENvbmR1Y3QgdXBcblxuICAgIGlmIChwYXJlbnQpIHtcbiAgICAgIGNvbmR1Y3RVcChwYXJlbnQua2V5KTtcbiAgICB9XG4gIH1cblxuICAoa2V5TGlzdCB8fCBbXSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgY29uZHVjdChrZXkpO1xuICB9KTtcbiAgdmFyIGNoZWNrZWRLZXlMaXN0ID0gW107XG4gIHZhciBoYWxmQ2hlY2tlZEtleUxpc3QgPSBbXTsgLy8gRmlsbCBjaGVja2VkIGxpc3RcblxuICBPYmplY3Qua2V5cyhjaGVja2VkS2V5cykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgaWYgKGNoZWNrZWRLZXlzW2tleV0pIHtcbiAgICAgIGNoZWNrZWRLZXlMaXN0LnB1c2goa2V5KTtcbiAgICB9XG4gIH0pOyAvLyBGaWxsIGhhbGYgY2hlY2tlZCBsaXN0XG5cbiAgT2JqZWN0LmtleXMoaGFsZkNoZWNrZWRLZXlzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICBpZiAoIWNoZWNrZWRLZXlzW2tleV0gJiYgaGFsZkNoZWNrZWRLZXlzW2tleV0pIHtcbiAgICAgIGhhbGZDaGVja2VkS2V5TGlzdC5wdXNoKGtleSk7XG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIHtcbiAgICBjaGVja2VkS2V5czogY2hlY2tlZEtleUxpc3QsXG4gICAgaGFsZkNoZWNrZWRLZXlzOiBoYWxmQ2hlY2tlZEtleUxpc3RcbiAgfTtcbn1cbi8qKlxuICogSWYgdXNlciB1c2UgYGF1dG9FeHBhbmRQYXJlbnRgIHdlIHNob3VsZCBnZXQgdGhlIGxpc3Qgb2YgcGFyZW50IG5vZGVcbiAqIEBwYXJhbSBrZXlMaXN0XG4gKiBAcGFyYW0ga2V5RW50aXRpZXNcbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gY29uZHVjdEV4cGFuZFBhcmVudChrZXlMaXN0LCBrZXlFbnRpdGllcykge1xuICB2YXIgZXhwYW5kZWRLZXlzID0ge307XG5cbiAgZnVuY3Rpb24gY29uZHVjdFVwKGtleSkge1xuICAgIGlmIChleHBhbmRlZEtleXNba2V5XSkgcmV0dXJuO1xuICAgIHZhciBlbnRpdHkgPSBrZXlFbnRpdGllc1trZXldO1xuICAgIGlmICghZW50aXR5KSByZXR1cm47XG4gICAgZXhwYW5kZWRLZXlzW2tleV0gPSB0cnVlO1xuICAgIHZhciBwYXJlbnQgPSBlbnRpdHkucGFyZW50LFxuICAgICAgICBub2RlID0gZW50aXR5Lm5vZGU7XG4gICAgaWYgKG5vZGUucHJvcHMgJiYgbm9kZS5wcm9wcy5kaXNhYmxlZCkgcmV0dXJuO1xuXG4gICAgaWYgKHBhcmVudCkge1xuICAgICAgY29uZHVjdFVwKHBhcmVudC5rZXkpO1xuICAgIH1cbiAgfVxuXG4gIChrZXlMaXN0IHx8IFtdKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICBjb25kdWN0VXAoa2V5KTtcbiAgfSk7XG4gIHJldHVybiBPYmplY3Qua2V5cyhleHBhbmRlZEtleXMpO1xufVxuLyoqXG4gKiBSZXR1cm5zIG9ubHkgdGhlIGRhdGEtIGFuZCBhcmlhLSBrZXkvdmFsdWUgcGFpcnNcbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RGF0YUFuZEFyaWEocHJvcHMpIHtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKHByb3BzKS5yZWR1Y2UoZnVuY3Rpb24gKHByZXYsIGtleSkge1xuICAgIGlmIChrZXkuc3Vic3RyKDAsIDUpID09PSAnZGF0YS0nIHx8IGtleS5zdWJzdHIoMCwgNSkgPT09ICdhcmlhLScpIHtcbiAgICAgIHByZXZba2V5XSA9IHByb3BzW2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIHByZXY7XG4gIH0sIHt9KTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-tree/es/util.js
