
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

var isTextNode = __webpack_require__(/*! ./isTextNode */ "./node_modules/fbjs/lib/isTextNode.js");
/*eslint-disable no-bitwise */

/**
 * Checks if a given DOM node contains or is another DOM node.
 */


function containsNode(outerNode, innerNode) {
  if (!outerNode || !innerNode) {
    return false;
  } else if (outerNode === innerNode) {
    return true;
  } else if (isTextNode(outerNode)) {
    return false;
  } else if (isTextNode(innerNode)) {
    return containsNode(outerNode, innerNode.parentNode);
  } else if ('contains' in outerNode) {
    return outerNode.contains(innerNode);
  } else if (outerNode.compareDocumentPosition) {
    return !!(outerNode.compareDocumentPosition(innerNode) & 16);
  } else {
    return false;
  }
}

module.exports = containsNode;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvY29udGFpbnNOb2RlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvY29udGFpbnNOb2RlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKiBcbiAqL1xuXG52YXIgaXNUZXh0Tm9kZSA9IHJlcXVpcmUoJy4vaXNUZXh0Tm9kZScpO1xuXG4vKmVzbGludC1kaXNhYmxlIG5vLWJpdHdpc2UgKi9cblxuLyoqXG4gKiBDaGVja3MgaWYgYSBnaXZlbiBET00gbm9kZSBjb250YWlucyBvciBpcyBhbm90aGVyIERPTSBub2RlLlxuICovXG5mdW5jdGlvbiBjb250YWluc05vZGUob3V0ZXJOb2RlLCBpbm5lck5vZGUpIHtcbiAgaWYgKCFvdXRlck5vZGUgfHwgIWlubmVyTm9kZSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfSBlbHNlIGlmIChvdXRlck5vZGUgPT09IGlubmVyTm9kZSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9IGVsc2UgaWYgKGlzVGV4dE5vZGUob3V0ZXJOb2RlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfSBlbHNlIGlmIChpc1RleHROb2RlKGlubmVyTm9kZSkpIHtcbiAgICByZXR1cm4gY29udGFpbnNOb2RlKG91dGVyTm9kZSwgaW5uZXJOb2RlLnBhcmVudE5vZGUpO1xuICB9IGVsc2UgaWYgKCdjb250YWlucycgaW4gb3V0ZXJOb2RlKSB7XG4gICAgcmV0dXJuIG91dGVyTm9kZS5jb250YWlucyhpbm5lck5vZGUpO1xuICB9IGVsc2UgaWYgKG91dGVyTm9kZS5jb21wYXJlRG9jdW1lbnRQb3NpdGlvbikge1xuICAgIHJldHVybiAhIShvdXRlck5vZGUuY29tcGFyZURvY3VtZW50UG9zaXRpb24oaW5uZXJOb2RlKSAmIDE2KTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBjb250YWluc05vZGU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7Ozs7QUFTQTtBQUVBO0FBQ0E7QUFDQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/containsNode.js
