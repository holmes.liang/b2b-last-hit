__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Typography */ "./node_modules/antd/es/typography/Typography.js");
/* harmony import */ var _Text__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Text */ "./node_modules/antd/es/typography/Text.js");
/* harmony import */ var _Title__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Title */ "./node_modules/antd/es/typography/Title.js");
/* harmony import */ var _Paragraph__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Paragraph */ "./node_modules/antd/es/typography/Paragraph.js");




var Typography = _Typography__WEBPACK_IMPORTED_MODULE_0__["default"];
Typography.Text = _Text__WEBPACK_IMPORTED_MODULE_1__["default"];
Typography.Title = _Title__WEBPACK_IMPORTED_MODULE_2__["default"];
Typography.Paragraph = _Paragraph__WEBPACK_IMPORTED_MODULE_3__["default"];
/* harmony default export */ __webpack_exports__["default"] = (Typography);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90eXBvZ3JhcGh5L2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgT3JpZ2luVHlwb2dyYXBoeSBmcm9tICcuL1R5cG9ncmFwaHknO1xuaW1wb3J0IFRleHQgZnJvbSAnLi9UZXh0JztcbmltcG9ydCBUaXRsZSBmcm9tICcuL1RpdGxlJztcbmltcG9ydCBQYXJhZ3JhcGggZnJvbSAnLi9QYXJhZ3JhcGgnO1xuY29uc3QgVHlwb2dyYXBoeSA9IE9yaWdpblR5cG9ncmFwaHk7XG5UeXBvZ3JhcGh5LlRleHQgPSBUZXh0O1xuVHlwb2dyYXBoeS5UaXRsZSA9IFRpdGxlO1xuVHlwb2dyYXBoeS5QYXJhZ3JhcGggPSBQYXJhZ3JhcGg7XG5leHBvcnQgZGVmYXVsdCBUeXBvZ3JhcGh5O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/index.js
