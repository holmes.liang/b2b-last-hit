

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

var Spinner = function Spinner(props) {
  var classes = _noImportant.StyleSheet.create(styles(props));

  return _react2.default.createElement('div', {
    className: (0, _noImportant.css)(classes.spinner)
  }, _react2.default.createElement('div', {
    className: (0, _noImportant.css)(classes.ripple)
  }));
};

Spinner.propTypes = {
  color: _propTypes2.default.string,
  size: _propTypes2.default.number
};
var rippleKeyframes = {
  '0%': {
    top: '50%',
    left: '50%',
    width: 0,
    height: 0,
    opacity: 1
  },
  '100%': {
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    opacity: 0
  }
};

var styles = function styles(_ref) {
  var color = _ref.color,
      size = _ref.size;
  return {
    spinner: {
      display: 'inline-block',
      position: 'relative',
      width: size,
      height: size
    },
    ripple: {
      position: 'absolute',
      border: '4px solid ' + color,
      opacity: 1,
      borderRadius: '50%',
      animationName: rippleKeyframes,
      animationDuration: '1s',
      animationTimingFunction: 'cubic-bezier(0, 0.2, 0.8, 1)',
      animationIterationCount: 'infinite'
    }
  };
};

exports.default = Spinner;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL1NwaW5uZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1pbWFnZXMvbGliL2NvbXBvbmVudHMvU3Bpbm5lci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfbm9JbXBvcnRhbnQgPSByZXF1aXJlKCdhcGhyb2RpdGUvbm8taW1wb3J0YW50Jyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBTcGlubmVyID0gZnVuY3Rpb24gU3Bpbm5lcihwcm9wcykge1xuXHR2YXIgY2xhc3NlcyA9IF9ub0ltcG9ydGFudC5TdHlsZVNoZWV0LmNyZWF0ZShzdHlsZXMocHJvcHMpKTtcblxuXHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0J2RpdicsXG5cdFx0eyBjbGFzc05hbWU6ICgwLCBfbm9JbXBvcnRhbnQuY3NzKShjbGFzc2VzLnNwaW5uZXIpIH0sXG5cdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHsgY2xhc3NOYW1lOiAoMCwgX25vSW1wb3J0YW50LmNzcykoY2xhc3Nlcy5yaXBwbGUpIH0pXG5cdCk7XG59O1xuXG5TcGlubmVyLnByb3BUeXBlcyA9IHtcblx0Y29sb3I6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuXHRzaXplOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlclxufTtcblxudmFyIHJpcHBsZUtleWZyYW1lcyA9IHtcblx0JzAlJzoge1xuXHRcdHRvcDogJzUwJScsXG5cdFx0bGVmdDogJzUwJScsXG5cdFx0d2lkdGg6IDAsXG5cdFx0aGVpZ2h0OiAwLFxuXHRcdG9wYWNpdHk6IDFcblx0fSxcblx0JzEwMCUnOiB7XG5cdFx0dG9wOiAwLFxuXHRcdGxlZnQ6IDAsXG5cdFx0d2lkdGg6ICcxMDAlJyxcblx0XHRoZWlnaHQ6ICcxMDAlJyxcblx0XHRvcGFjaXR5OiAwXG5cdH1cbn07XG5cbnZhciBzdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXMoX3JlZikge1xuXHR2YXIgY29sb3IgPSBfcmVmLmNvbG9yLFxuXHQgICAgc2l6ZSA9IF9yZWYuc2l6ZTtcblx0cmV0dXJuIHtcblx0XHRzcGlubmVyOiB7XG5cdFx0XHRkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcblx0XHRcdHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuXHRcdFx0d2lkdGg6IHNpemUsXG5cdFx0XHRoZWlnaHQ6IHNpemVcblx0XHR9LFxuXHRcdHJpcHBsZToge1xuXHRcdFx0cG9zaXRpb246ICdhYnNvbHV0ZScsXG5cdFx0XHRib3JkZXI6ICc0cHggc29saWQgJyArIGNvbG9yLFxuXHRcdFx0b3BhY2l0eTogMSxcblx0XHRcdGJvcmRlclJhZGl1czogJzUwJScsXG5cdFx0XHRhbmltYXRpb25OYW1lOiByaXBwbGVLZXlmcmFtZXMsXG5cdFx0XHRhbmltYXRpb25EdXJhdGlvbjogJzFzJyxcblx0XHRcdGFuaW1hdGlvblRpbWluZ0Z1bmN0aW9uOiAnY3ViaWMtYmV6aWVyKDAsIDAuMiwgMC44LCAxKScsXG5cdFx0XHRhbmltYXRpb25JdGVyYXRpb25Db3VudDogJ2luZmluaXRlJ1xuXHRcdH1cblx0fTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNwaW5uZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFSQTtBQUNBO0FBZ0JBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFQQTtBQWtCQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Spinner.js
