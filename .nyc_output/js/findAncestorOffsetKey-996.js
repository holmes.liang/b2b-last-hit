/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule findAncestorOffsetKey
 * @format
 * 
 */


var getSelectionOffsetKeyForNode = __webpack_require__(/*! ./getSelectionOffsetKeyForNode */ "./node_modules/draft-js/lib/getSelectionOffsetKeyForNode.js");
/**
 * Get the key from the node's nearest offset-aware ancestor.
 */


function findAncestorOffsetKey(node) {
  var searchNode = node;

  while (searchNode && searchNode !== document.documentElement) {
    var key = getSelectionOffsetKeyForNode(searchNode);

    if (key != null) {
      return key;
    }

    searchNode = searchNode.parentNode;
  }

  return null;
}

module.exports = findAncestorOffsetKey;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2ZpbmRBbmNlc3Rvck9mZnNldEtleS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9maW5kQW5jZXN0b3JPZmZzZXRLZXkuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBmaW5kQW5jZXN0b3JPZmZzZXRLZXlcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGdldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUgPSByZXF1aXJlKCcuL2dldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUnKTtcblxuLyoqXG4gKiBHZXQgdGhlIGtleSBmcm9tIHRoZSBub2RlJ3MgbmVhcmVzdCBvZmZzZXQtYXdhcmUgYW5jZXN0b3IuXG4gKi9cbmZ1bmN0aW9uIGZpbmRBbmNlc3Rvck9mZnNldEtleShub2RlKSB7XG4gIHZhciBzZWFyY2hOb2RlID0gbm9kZTtcbiAgd2hpbGUgKHNlYXJjaE5vZGUgJiYgc2VhcmNoTm9kZSAhPT0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgdmFyIGtleSA9IGdldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUoc2VhcmNoTm9kZSk7XG4gICAgaWYgKGtleSAhPSBudWxsKSB7XG4gICAgICByZXR1cm4ga2V5O1xuICAgIH1cbiAgICBzZWFyY2hOb2RlID0gc2VhcmNoTm9kZS5wYXJlbnROb2RlO1xuICB9XG4gIHJldHVybiBudWxsO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZpbmRBbmNlc3Rvck9mZnNldEtleTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/findAncestorOffsetKey.js
