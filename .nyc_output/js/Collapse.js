__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Collapse; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_collapse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-collapse */ "./node_modules/rc-collapse/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _CollapsePanel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CollapsePanel */ "./node_modules/antd/es/collapse/CollapsePanel.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_openAnimation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/openAnimation */ "./node_modules/antd/es/_util/openAnimation.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}









var Collapse =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Collapse, _React$Component);

  function Collapse() {
    var _this;

    _classCallCheck(this, Collapse);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Collapse).apply(this, arguments));

    _this.renderExpandIcon = function () {
      var panelProps = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var prefixCls = arguments.length > 1 ? arguments[1] : undefined;
      var expandIcon = _this.props.expandIcon;
      var icon = expandIcon ? expandIcon(panelProps) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "right",
        rotate: panelProps.isActive ? 90 : undefined
      });
      return react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](icon) ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](icon, {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(icon.props.className, "".concat(prefixCls, "-arrow"))
      }) : icon;
    };

    _this.renderCollapse = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          bordered = _this$props.bordered,
          expandIconPosition = _this$props.expandIconPosition;
      var prefixCls = getPrefixCls('collapse', customizePrefixCls);
      var collapseClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-borderless"), !bordered), _defineProperty(_classNames, "".concat(prefixCls, "-icon-position-").concat(expandIconPosition), true), _classNames), className);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_collapse__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, _this.props, {
        expandIcon: function expandIcon(panelProps) {
          return _this.renderExpandIcon(panelProps, prefixCls);
        },
        prefixCls: prefixCls,
        className: collapseClassName
      }));
    };

    return _this;
  }

  _createClass(Collapse, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderCollapse);
    }
  }]);

  return Collapse;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Collapse.Panel = _CollapsePanel__WEBPACK_IMPORTED_MODULE_3__["default"];
Collapse.defaultProps = {
  bordered: true,
  openAnimation: _extends(_extends({}, _util_openAnimation__WEBPACK_IMPORTED_MODULE_6__["default"]), {
    appear: function appear() {}
  }),
  expandIconPosition: 'left'
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jb2xsYXBzZS9Db2xsYXBzZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvY29sbGFwc2UvQ29sbGFwc2UuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSY0NvbGxhcHNlIGZyb20gJ3JjLWNvbGxhcHNlJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IENvbGxhcHNlUGFuZWwgZnJvbSAnLi9Db2xsYXBzZVBhbmVsJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IGFuaW1hdGlvbiBmcm9tICcuLi9fdXRpbC9vcGVuQW5pbWF0aW9uJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbGxhcHNlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5yZW5kZXJFeHBhbmRJY29uID0gKHBhbmVsUHJvcHMgPSB7fSwgcHJlZml4Q2xzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGV4cGFuZEljb24gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBpY29uID0gKGV4cGFuZEljb24gPyAoZXhwYW5kSWNvbihwYW5lbFByb3BzKSkgOiAoPEljb24gdHlwZT1cInJpZ2h0XCIgcm90YXRlPXtwYW5lbFByb3BzLmlzQWN0aXZlID8gOTAgOiB1bmRlZmluZWR9Lz4pKTtcbiAgICAgICAgICAgIHJldHVybiBSZWFjdC5pc1ZhbGlkRWxlbWVudChpY29uKVxuICAgICAgICAgICAgICAgID8gUmVhY3QuY2xvbmVFbGVtZW50KGljb24sIHtcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKGljb24ucHJvcHMuY2xhc3NOYW1lLCBgJHtwcmVmaXhDbHN9LWFycm93YCksXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICA6IGljb247XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQ29sbGFwc2UgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lID0gJycsIGJvcmRlcmVkLCBleHBhbmRJY29uUG9zaXRpb24sIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdjb2xsYXBzZScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBjb2xsYXBzZUNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoe1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWJvcmRlcmxlc3NgXTogIWJvcmRlcmVkLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWljb24tcG9zaXRpb24tJHtleHBhbmRJY29uUG9zaXRpb259YF06IHRydWUsXG4gICAgICAgICAgICB9LCBjbGFzc05hbWUpO1xuICAgICAgICAgICAgcmV0dXJuICg8UmNDb2xsYXBzZSB7Li4udGhpcy5wcm9wc30gZXhwYW5kSWNvbj17KHBhbmVsUHJvcHMpID0+IHRoaXMucmVuZGVyRXhwYW5kSWNvbihwYW5lbFByb3BzLCBwcmVmaXhDbHMpfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gY2xhc3NOYW1lPXtjb2xsYXBzZUNsYXNzTmFtZX0vPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJDb2xsYXBzZX08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5Db2xsYXBzZS5QYW5lbCA9IENvbGxhcHNlUGFuZWw7XG5Db2xsYXBzZS5kZWZhdWx0UHJvcHMgPSB7XG4gICAgYm9yZGVyZWQ6IHRydWUsXG4gICAgb3BlbkFuaW1hdGlvbjogT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBhbmltYXRpb24pLCB7IGFwcGVhcigpIHsgfSB9KSxcbiAgICBleHBhbmRJY29uUG9zaXRpb246ICdsZWZ0Jyxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQURBO0FBSkE7QUFDQTtBQVFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUEE7QUFDQTtBQVpBO0FBb0JBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7OztBQXhCQTtBQUNBO0FBREE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFIQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/collapse/Collapse.js
