/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var filter = _util.filter;
var map = _util.map;
var isArray = _util.isArray;
var indexOf = _util.indexOf;
var isObject = _util.isObject;
var isString = _util.isString;
var createHashMap = _util.createHashMap;
var assert = _util.assert;
var clone = _util.clone;
var merge = _util.merge;
var extend = _util.extend;
var mixin = _util.mixin;

var modelUtil = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var Model = __webpack_require__(/*! ./Model */ "./node_modules/echarts/lib/model/Model.js");

var ComponentModel = __webpack_require__(/*! ./Component */ "./node_modules/echarts/lib/model/Component.js");

var globalDefault = __webpack_require__(/*! ./globalDefault */ "./node_modules/echarts/lib/model/globalDefault.js");

var colorPaletteMixin = __webpack_require__(/*! ./mixin/colorPalette */ "./node_modules/echarts/lib/model/mixin/colorPalette.js");

var _sourceHelper = __webpack_require__(/*! ../data/helper/sourceHelper */ "./node_modules/echarts/lib/data/helper/sourceHelper.js");

var resetSourceDefaulter = _sourceHelper.resetSourceDefaulter;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * ECharts global model
 *
 * @module {echarts/model/Global}
 */

/**
 * Caution: If the mechanism should be changed some day, these cases
 * should be considered:
 *
 * (1) In `merge option` mode, if using the same option to call `setOption`
 * many times, the result should be the same (try our best to ensure that).
 * (2) In `merge option` mode, if a component has no id/name specified, it
 * will be merged by index, and the result sequence of the components is
 * consistent to the original sequence.
 * (3) `reset` feature (in toolbox). Find detailed info in comments about
 * `mergeOption` in module:echarts/model/OptionManager.
 */

var OPTION_INNER_KEY = '\0_ec_inner';
/**
 * @alias module:echarts/model/Global
 *
 * @param {Object} option
 * @param {module:echarts/model/Model} parentModel
 * @param {Object} theme
 */

var GlobalModel = Model.extend({
  init: function init(option, parentModel, theme, optionManager) {
    theme = theme || {};
    this.option = null; // Mark as not initialized.

    /**
     * @type {module:echarts/model/Model}
     * @private
     */

    this._theme = new Model(theme);
    /**
     * @type {module:echarts/model/OptionManager}
     */

    this._optionManager = optionManager;
  },
  setOption: function setOption(option, optionPreprocessorFuncs) {
    assert(!(OPTION_INNER_KEY in option), 'please use chart.getOption()');

    this._optionManager.setOption(option, optionPreprocessorFuncs);

    this.resetOption(null);
  },

  /**
   * @param {string} type null/undefined: reset all.
   *                      'recreate': force recreate all.
   *                      'timeline': only reset timeline option
   *                      'media': only reset media query option
   * @return {boolean} Whether option changed.
   */
  resetOption: function resetOption(type) {
    var optionChanged = false;
    var optionManager = this._optionManager;

    if (!type || type === 'recreate') {
      var baseOption = optionManager.mountOption(type === 'recreate');

      if (!this.option || type === 'recreate') {
        initBase.call(this, baseOption);
      } else {
        this.restoreData();
        this.mergeOption(baseOption);
      }

      optionChanged = true;
    }

    if (type === 'timeline' || type === 'media') {
      this.restoreData();
    }

    if (!type || type === 'recreate' || type === 'timeline') {
      var timelineOption = optionManager.getTimelineOption(this);
      timelineOption && (this.mergeOption(timelineOption), optionChanged = true);
    }

    if (!type || type === 'recreate' || type === 'media') {
      var mediaOptions = optionManager.getMediaOption(this, this._api);

      if (mediaOptions.length) {
        each(mediaOptions, function (mediaOption) {
          this.mergeOption(mediaOption, optionChanged = true);
        }, this);
      }
    }

    return optionChanged;
  },

  /**
   * @protected
   */
  mergeOption: function mergeOption(newOption) {
    var option = this.option;
    var componentsMap = this._componentsMap;
    var newCptTypes = [];
    resetSourceDefaulter(this); // If no component class, merge directly.
    // For example: color, animaiton options, etc.

    each(newOption, function (componentOption, mainType) {
      if (componentOption == null) {
        return;
      }

      if (!ComponentModel.hasClass(mainType)) {
        // globalSettingTask.dirty();
        option[mainType] = option[mainType] == null ? clone(componentOption) : merge(option[mainType], componentOption, true);
      } else if (mainType) {
        newCptTypes.push(mainType);
      }
    });
    ComponentModel.topologicalTravel(newCptTypes, ComponentModel.getAllClassMainTypes(), visitComponent, this);

    function visitComponent(mainType, dependencies) {
      var newCptOptionList = modelUtil.normalizeToArray(newOption[mainType]);
      var mapResult = modelUtil.mappingToExists(componentsMap.get(mainType), newCptOptionList);
      modelUtil.makeIdAndName(mapResult); // Set mainType and complete subType.

      each(mapResult, function (item, index) {
        var opt = item.option;

        if (isObject(opt)) {
          item.keyInfo.mainType = mainType;
          item.keyInfo.subType = determineSubType(mainType, opt, item.exist);
        }
      });
      var dependentModels = getComponentsByTypes(componentsMap, dependencies);
      option[mainType] = [];
      componentsMap.set(mainType, []);
      each(mapResult, function (resultItem, index) {
        var componentModel = resultItem.exist;
        var newCptOption = resultItem.option;
        assert(isObject(newCptOption) || componentModel, 'Empty component definition'); // Consider where is no new option and should be merged using {},
        // see removeEdgeAndAdd in topologicalTravel and
        // ComponentModel.getAllClassMainTypes.

        if (!newCptOption) {
          componentModel.mergeOption({}, this);
          componentModel.optionUpdated({}, false);
        } else {
          var ComponentModelClass = ComponentModel.getClass(mainType, resultItem.keyInfo.subType, true);

          if (componentModel && componentModel instanceof ComponentModelClass) {
            componentModel.name = resultItem.keyInfo.name; // componentModel.settingTask && componentModel.settingTask.dirty();

            componentModel.mergeOption(newCptOption, this);
            componentModel.optionUpdated(newCptOption, false);
          } else {
            // PENDING Global as parent ?
            var extraOpt = extend({
              dependentModels: dependentModels,
              componentIndex: index
            }, resultItem.keyInfo);
            componentModel = new ComponentModelClass(newCptOption, this, this, extraOpt);
            extend(componentModel, extraOpt);
            componentModel.init(newCptOption, this, this, extraOpt); // Call optionUpdated after init.
            // newCptOption has been used as componentModel.option
            // and may be merged with theme and default, so pass null
            // to avoid confusion.

            componentModel.optionUpdated(null, true);
          }
        }

        componentsMap.get(mainType)[index] = componentModel;
        option[mainType][index] = componentModel.option;
      }, this); // Backup series for filtering.

      if (mainType === 'series') {
        createSeriesIndices(this, componentsMap.get('series'));
      }
    }

    this._seriesIndicesMap = createHashMap(this._seriesIndices = this._seriesIndices || []);
  },

  /**
   * Get option for output (cloned option and inner info removed)
   * @public
   * @return {Object}
   */
  getOption: function getOption() {
    var option = clone(this.option);
    each(option, function (opts, mainType) {
      if (ComponentModel.hasClass(mainType)) {
        var opts = modelUtil.normalizeToArray(opts);

        for (var i = opts.length - 1; i >= 0; i--) {
          // Remove options with inner id.
          if (modelUtil.isIdInner(opts[i])) {
            opts.splice(i, 1);
          }
        }

        option[mainType] = opts;
      }
    });
    delete option[OPTION_INNER_KEY];
    return option;
  },

  /**
   * @return {module:echarts/model/Model}
   */
  getTheme: function getTheme() {
    return this._theme;
  },

  /**
   * @param {string} mainType
   * @param {number} [idx=0]
   * @return {module:echarts/model/Component}
   */
  getComponent: function getComponent(mainType, idx) {
    var list = this._componentsMap.get(mainType);

    if (list) {
      return list[idx || 0];
    }
  },

  /**
   * If none of index and id and name used, return all components with mainType.
   * @param {Object} condition
   * @param {string} condition.mainType
   * @param {string} [condition.subType] If ignore, only query by mainType
   * @param {number|Array.<number>} [condition.index] Either input index or id or name.
   * @param {string|Array.<string>} [condition.id] Either input index or id or name.
   * @param {string|Array.<string>} [condition.name] Either input index or id or name.
   * @return {Array.<module:echarts/model/Component>}
   */
  queryComponents: function queryComponents(condition) {
    var mainType = condition.mainType;

    if (!mainType) {
      return [];
    }

    var index = condition.index;
    var id = condition.id;
    var name = condition.name;

    var cpts = this._componentsMap.get(mainType);

    if (!cpts || !cpts.length) {
      return [];
    }

    var result;

    if (index != null) {
      if (!isArray(index)) {
        index = [index];
      }

      result = filter(map(index, function (idx) {
        return cpts[idx];
      }), function (val) {
        return !!val;
      });
    } else if (id != null) {
      var isIdArray = isArray(id);
      result = filter(cpts, function (cpt) {
        return isIdArray && indexOf(id, cpt.id) >= 0 || !isIdArray && cpt.id === id;
      });
    } else if (name != null) {
      var isNameArray = isArray(name);
      result = filter(cpts, function (cpt) {
        return isNameArray && indexOf(name, cpt.name) >= 0 || !isNameArray && cpt.name === name;
      });
    } else {
      // Return all components with mainType
      result = cpts.slice();
    }

    return filterBySubType(result, condition);
  },

  /**
   * The interface is different from queryComponents,
   * which is convenient for inner usage.
   *
   * @usage
   * var result = findComponents(
   *     {mainType: 'dataZoom', query: {dataZoomId: 'abc'}}
   * );
   * var result = findComponents(
   *     {mainType: 'series', subType: 'pie', query: {seriesName: 'uio'}}
   * );
   * var result = findComponents(
   *     {mainType: 'series'},
   *     function (model, index) {...}
   * );
   * // result like [component0, componnet1, ...]
   *
   * @param {Object} condition
   * @param {string} condition.mainType Mandatory.
   * @param {string} [condition.subType] Optional.
   * @param {Object} [condition.query] like {xxxIndex, xxxId, xxxName},
   *        where xxx is mainType.
   *        If query attribute is null/undefined or has no index/id/name,
   *        do not filtering by query conditions, which is convenient for
   *        no-payload situations or when target of action is global.
   * @param {Function} [condition.filter] parameter: component, return boolean.
   * @return {Array.<module:echarts/model/Component>}
   */
  findComponents: function findComponents(condition) {
    var query = condition.query;
    var mainType = condition.mainType;
    var queryCond = getQueryCond(query);
    var result = queryCond ? this.queryComponents(queryCond) : this._componentsMap.get(mainType);
    return doFilter(filterBySubType(result, condition));

    function getQueryCond(q) {
      var indexAttr = mainType + 'Index';
      var idAttr = mainType + 'Id';
      var nameAttr = mainType + 'Name';
      return q && (q[indexAttr] != null || q[idAttr] != null || q[nameAttr] != null) ? {
        mainType: mainType,
        // subType will be filtered finally.
        index: q[indexAttr],
        id: q[idAttr],
        name: q[nameAttr]
      } : null;
    }

    function doFilter(res) {
      return condition.filter ? filter(res, condition.filter) : res;
    }
  },

  /**
   * @usage
   * eachComponent('legend', function (legendModel, index) {
   *     ...
   * });
   * eachComponent(function (componentType, model, index) {
   *     // componentType does not include subType
   *     // (componentType is 'xxx' but not 'xxx.aa')
   * });
   * eachComponent(
   *     {mainType: 'dataZoom', query: {dataZoomId: 'abc'}},
   *     function (model, index) {...}
   * );
   * eachComponent(
   *     {mainType: 'series', subType: 'pie', query: {seriesName: 'uio'}},
   *     function (model, index) {...}
   * );
   *
   * @param {string|Object=} mainType When mainType is object, the definition
   *                                  is the same as the method 'findComponents'.
   * @param {Function} cb
   * @param {*} context
   */
  eachComponent: function eachComponent(mainType, cb, context) {
    var componentsMap = this._componentsMap;

    if (typeof mainType === 'function') {
      context = cb;
      cb = mainType;
      componentsMap.each(function (components, componentType) {
        each(components, function (component, index) {
          cb.call(context, componentType, component, index);
        });
      });
    } else if (isString(mainType)) {
      each(componentsMap.get(mainType), cb, context);
    } else if (isObject(mainType)) {
      var queryResult = this.findComponents(mainType);
      each(queryResult, cb, context);
    }
  },

  /**
   * @param {string} name
   * @return {Array.<module:echarts/model/Series>}
   */
  getSeriesByName: function getSeriesByName(name) {
    var series = this._componentsMap.get('series');

    return filter(series, function (oneSeries) {
      return oneSeries.name === name;
    });
  },

  /**
   * @param {number} seriesIndex
   * @return {module:echarts/model/Series}
   */
  getSeriesByIndex: function getSeriesByIndex(seriesIndex) {
    return this._componentsMap.get('series')[seriesIndex];
  },

  /**
   * Get series list before filtered by type.
   * FIXME: rename to getRawSeriesByType?
   *
   * @param {string} subType
   * @return {Array.<module:echarts/model/Series>}
   */
  getSeriesByType: function getSeriesByType(subType) {
    var series = this._componentsMap.get('series');

    return filter(series, function (oneSeries) {
      return oneSeries.subType === subType;
    });
  },

  /**
   * @return {Array.<module:echarts/model/Series>}
   */
  getSeries: function getSeries() {
    return this._componentsMap.get('series').slice();
  },

  /**
   * @return {number}
   */
  getSeriesCount: function getSeriesCount() {
    return this._componentsMap.get('series').length;
  },

  /**
   * After filtering, series may be different
   * frome raw series.
   *
   * @param {Function} cb
   * @param {*} context
   */
  eachSeries: function eachSeries(cb, context) {
    assertSeriesInitialized(this);
    each(this._seriesIndices, function (rawSeriesIndex) {
      var series = this._componentsMap.get('series')[rawSeriesIndex];

      cb.call(context, series, rawSeriesIndex);
    }, this);
  },

  /**
   * Iterate raw series before filtered.
   *
   * @param {Function} cb
   * @param {*} context
   */
  eachRawSeries: function eachRawSeries(cb, context) {
    each(this._componentsMap.get('series'), cb, context);
  },

  /**
   * After filtering, series may be different.
   * frome raw series.
   *
   * @parma {string} subType
   * @param {Function} cb
   * @param {*} context
   */
  eachSeriesByType: function eachSeriesByType(subType, cb, context) {
    assertSeriesInitialized(this);
    each(this._seriesIndices, function (rawSeriesIndex) {
      var series = this._componentsMap.get('series')[rawSeriesIndex];

      if (series.subType === subType) {
        cb.call(context, series, rawSeriesIndex);
      }
    }, this);
  },

  /**
   * Iterate raw series before filtered of given type.
   *
   * @parma {string} subType
   * @param {Function} cb
   * @param {*} context
   */
  eachRawSeriesByType: function eachRawSeriesByType(subType, cb, context) {
    return each(this.getSeriesByType(subType), cb, context);
  },

  /**
   * @param {module:echarts/model/Series} seriesModel
   */
  isSeriesFiltered: function isSeriesFiltered(seriesModel) {
    assertSeriesInitialized(this);
    return this._seriesIndicesMap.get(seriesModel.componentIndex) == null;
  },

  /**
   * @return {Array.<number>}
   */
  getCurrentSeriesIndices: function getCurrentSeriesIndices() {
    return (this._seriesIndices || []).slice();
  },

  /**
   * @param {Function} cb
   * @param {*} context
   */
  filterSeries: function filterSeries(cb, context) {
    assertSeriesInitialized(this);
    var filteredSeries = filter(this._componentsMap.get('series'), cb, context);
    createSeriesIndices(this, filteredSeries);
  },
  restoreData: function restoreData(payload) {
    var componentsMap = this._componentsMap;
    createSeriesIndices(this, componentsMap.get('series'));
    var componentTypes = [];
    componentsMap.each(function (components, componentType) {
      componentTypes.push(componentType);
    });
    ComponentModel.topologicalTravel(componentTypes, ComponentModel.getAllClassMainTypes(), function (componentType, dependencies) {
      each(componentsMap.get(componentType), function (component) {
        (componentType !== 'series' || !isNotTargetSeries(component, payload)) && component.restoreData();
      });
    });
  }
});

function isNotTargetSeries(seriesModel, payload) {
  if (payload) {
    var index = payload.seiresIndex;
    var id = payload.seriesId;
    var name = payload.seriesName;
    return index != null && seriesModel.componentIndex !== index || id != null && seriesModel.id !== id || name != null && seriesModel.name !== name;
  }
}
/**
 * @inner
 */


function mergeTheme(option, theme) {
  // PENDING
  // NOT use `colorLayer` in theme if option has `color`
  var notMergeColorLayer = option.color && !option.colorLayer;
  each(theme, function (themeItem, name) {
    if (name === 'colorLayer' && notMergeColorLayer) {
      return;
    } // 如果有 component model 则把具体的 merge 逻辑交给该 model 处理


    if (!ComponentModel.hasClass(name)) {
      if (typeof themeItem === 'object') {
        option[name] = !option[name] ? clone(themeItem) : merge(option[name], themeItem, false);
      } else {
        if (option[name] == null) {
          option[name] = themeItem;
        }
      }
    }
  });
}

function initBase(baseOption) {
  baseOption = baseOption; // Using OPTION_INNER_KEY to mark that this option can not be used outside,
  // i.e. `chart.setOption(chart.getModel().option);` is forbiden.

  this.option = {};
  this.option[OPTION_INNER_KEY] = 1;
  /**
   * Init with series: [], in case of calling findSeries method
   * before series initialized.
   * @type {Object.<string, Array.<module:echarts/model/Model>>}
   * @private
   */

  this._componentsMap = createHashMap({
    series: []
  });
  /**
   * Mapping between filtered series list and raw series list.
   * key: filtered series indices, value: raw series indices.
   * @type {Array.<nubmer>}
   * @private
   */

  this._seriesIndices;
  this._seriesIndicesMap;
  mergeTheme(baseOption, this._theme.option); // TODO Needs clone when merging to the unexisted property

  merge(baseOption, globalDefault, false);
  this.mergeOption(baseOption);
}
/**
 * @inner
 * @param {Array.<string>|string} types model types
 * @return {Object} key: {string} type, value: {Array.<Object>} models
 */


function getComponentsByTypes(componentsMap, types) {
  if (!isArray(types)) {
    types = types ? [types] : [];
  }

  var ret = {};
  each(types, function (type) {
    ret[type] = (componentsMap.get(type) || []).slice();
  });
  return ret;
}
/**
 * @inner
 */


function determineSubType(mainType, newCptOption, existComponent) {
  var subType = newCptOption.type ? newCptOption.type : existComponent ? existComponent.subType // Use determineSubType only when there is no existComponent.
  : ComponentModel.determineSubType(mainType, newCptOption); // tooltip, markline, markpoint may always has no subType

  return subType;
}
/**
 * @inner
 */


function createSeriesIndices(ecModel, seriesModels) {
  ecModel._seriesIndicesMap = createHashMap(ecModel._seriesIndices = map(seriesModels, function (series) {
    return series.componentIndex;
  }) || []);
}
/**
 * @inner
 */


function filterBySubType(components, condition) {
  // Using hasOwnProperty for restrict. Consider
  // subType is undefined in user payload.
  return condition.hasOwnProperty('subType') ? filter(components, function (cpt) {
    return cpt.subType === condition.subType;
  }) : components;
}
/**
 * @inner
 */


function assertSeriesInitialized(ecModel) {}

mixin(GlobalModel, colorPaletteMixin);
var _default = GlobalModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvR2xvYmFsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvR2xvYmFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgZmlsdGVyID0gX3V0aWwuZmlsdGVyO1xudmFyIG1hcCA9IF91dGlsLm1hcDtcbnZhciBpc0FycmF5ID0gX3V0aWwuaXNBcnJheTtcbnZhciBpbmRleE9mID0gX3V0aWwuaW5kZXhPZjtcbnZhciBpc09iamVjdCA9IF91dGlsLmlzT2JqZWN0O1xudmFyIGlzU3RyaW5nID0gX3V0aWwuaXNTdHJpbmc7XG52YXIgY3JlYXRlSGFzaE1hcCA9IF91dGlsLmNyZWF0ZUhhc2hNYXA7XG52YXIgYXNzZXJ0ID0gX3V0aWwuYXNzZXJ0O1xudmFyIGNsb25lID0gX3V0aWwuY2xvbmU7XG52YXIgbWVyZ2UgPSBfdXRpbC5tZXJnZTtcbnZhciBleHRlbmQgPSBfdXRpbC5leHRlbmQ7XG52YXIgbWl4aW4gPSBfdXRpbC5taXhpbjtcblxudmFyIG1vZGVsVXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL21vZGVsXCIpO1xuXG52YXIgTW9kZWwgPSByZXF1aXJlKFwiLi9Nb2RlbFwiKTtcblxudmFyIENvbXBvbmVudE1vZGVsID0gcmVxdWlyZShcIi4vQ29tcG9uZW50XCIpO1xuXG52YXIgZ2xvYmFsRGVmYXVsdCA9IHJlcXVpcmUoXCIuL2dsb2JhbERlZmF1bHRcIik7XG5cbnZhciBjb2xvclBhbGV0dGVNaXhpbiA9IHJlcXVpcmUoXCIuL21peGluL2NvbG9yUGFsZXR0ZVwiKTtcblxudmFyIF9zb3VyY2VIZWxwZXIgPSByZXF1aXJlKFwiLi4vZGF0YS9oZWxwZXIvc291cmNlSGVscGVyXCIpO1xuXG52YXIgcmVzZXRTb3VyY2VEZWZhdWx0ZXIgPSBfc291cmNlSGVscGVyLnJlc2V0U291cmNlRGVmYXVsdGVyO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogRUNoYXJ0cyBnbG9iYWwgbW9kZWxcbiAqXG4gKiBAbW9kdWxlIHtlY2hhcnRzL21vZGVsL0dsb2JhbH1cbiAqL1xuXG4vKipcbiAqIENhdXRpb246IElmIHRoZSBtZWNoYW5pc20gc2hvdWxkIGJlIGNoYW5nZWQgc29tZSBkYXksIHRoZXNlIGNhc2VzXG4gKiBzaG91bGQgYmUgY29uc2lkZXJlZDpcbiAqXG4gKiAoMSkgSW4gYG1lcmdlIG9wdGlvbmAgbW9kZSwgaWYgdXNpbmcgdGhlIHNhbWUgb3B0aW9uIHRvIGNhbGwgYHNldE9wdGlvbmBcbiAqIG1hbnkgdGltZXMsIHRoZSByZXN1bHQgc2hvdWxkIGJlIHRoZSBzYW1lICh0cnkgb3VyIGJlc3QgdG8gZW5zdXJlIHRoYXQpLlxuICogKDIpIEluIGBtZXJnZSBvcHRpb25gIG1vZGUsIGlmIGEgY29tcG9uZW50IGhhcyBubyBpZC9uYW1lIHNwZWNpZmllZCwgaXRcbiAqIHdpbGwgYmUgbWVyZ2VkIGJ5IGluZGV4LCBhbmQgdGhlIHJlc3VsdCBzZXF1ZW5jZSBvZiB0aGUgY29tcG9uZW50cyBpc1xuICogY29uc2lzdGVudCB0byB0aGUgb3JpZ2luYWwgc2VxdWVuY2UuXG4gKiAoMykgYHJlc2V0YCBmZWF0dXJlIChpbiB0b29sYm94KS4gRmluZCBkZXRhaWxlZCBpbmZvIGluIGNvbW1lbnRzIGFib3V0XG4gKiBgbWVyZ2VPcHRpb25gIGluIG1vZHVsZTplY2hhcnRzL21vZGVsL09wdGlvbk1hbmFnZXIuXG4gKi9cbnZhciBPUFRJT05fSU5ORVJfS0VZID0gJ1xcMF9lY19pbm5lcic7XG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWxcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBwYXJlbnRNb2RlbFxuICogQHBhcmFtIHtPYmplY3R9IHRoZW1lXG4gKi9cblxudmFyIEdsb2JhbE1vZGVsID0gTW9kZWwuZXh0ZW5kKHtcbiAgaW5pdDogZnVuY3Rpb24gKG9wdGlvbiwgcGFyZW50TW9kZWwsIHRoZW1lLCBvcHRpb25NYW5hZ2VyKSB7XG4gICAgdGhlbWUgPSB0aGVtZSB8fCB7fTtcbiAgICB0aGlzLm9wdGlvbiA9IG51bGw7IC8vIE1hcmsgYXMgbm90IGluaXRpYWxpemVkLlxuXG4gICAgLyoqXG4gICAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG5cbiAgICB0aGlzLl90aGVtZSA9IG5ldyBNb2RlbCh0aGVtZSk7XG4gICAgLyoqXG4gICAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL21vZGVsL09wdGlvbk1hbmFnZXJ9XG4gICAgICovXG5cbiAgICB0aGlzLl9vcHRpb25NYW5hZ2VyID0gb3B0aW9uTWFuYWdlcjtcbiAgfSxcbiAgc2V0T3B0aW9uOiBmdW5jdGlvbiAob3B0aW9uLCBvcHRpb25QcmVwcm9jZXNzb3JGdW5jcykge1xuICAgIGFzc2VydCghKE9QVElPTl9JTk5FUl9LRVkgaW4gb3B0aW9uKSwgJ3BsZWFzZSB1c2UgY2hhcnQuZ2V0T3B0aW9uKCknKTtcblxuICAgIHRoaXMuX29wdGlvbk1hbmFnZXIuc2V0T3B0aW9uKG9wdGlvbiwgb3B0aW9uUHJlcHJvY2Vzc29yRnVuY3MpO1xuXG4gICAgdGhpcy5yZXNldE9wdGlvbihudWxsKTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgbnVsbC91bmRlZmluZWQ6IHJlc2V0IGFsbC5cbiAgICogICAgICAgICAgICAgICAgICAgICAgJ3JlY3JlYXRlJzogZm9yY2UgcmVjcmVhdGUgYWxsLlxuICAgKiAgICAgICAgICAgICAgICAgICAgICAndGltZWxpbmUnOiBvbmx5IHJlc2V0IHRpbWVsaW5lIG9wdGlvblxuICAgKiAgICAgICAgICAgICAgICAgICAgICAnbWVkaWEnOiBvbmx5IHJlc2V0IG1lZGlhIHF1ZXJ5IG9wdGlvblxuICAgKiBAcmV0dXJuIHtib29sZWFufSBXaGV0aGVyIG9wdGlvbiBjaGFuZ2VkLlxuICAgKi9cbiAgcmVzZXRPcHRpb246IGZ1bmN0aW9uICh0eXBlKSB7XG4gICAgdmFyIG9wdGlvbkNoYW5nZWQgPSBmYWxzZTtcbiAgICB2YXIgb3B0aW9uTWFuYWdlciA9IHRoaXMuX29wdGlvbk1hbmFnZXI7XG5cbiAgICBpZiAoIXR5cGUgfHwgdHlwZSA9PT0gJ3JlY3JlYXRlJykge1xuICAgICAgdmFyIGJhc2VPcHRpb24gPSBvcHRpb25NYW5hZ2VyLm1vdW50T3B0aW9uKHR5cGUgPT09ICdyZWNyZWF0ZScpO1xuXG4gICAgICBpZiAoIXRoaXMub3B0aW9uIHx8IHR5cGUgPT09ICdyZWNyZWF0ZScpIHtcbiAgICAgICAgaW5pdEJhc2UuY2FsbCh0aGlzLCBiYXNlT3B0aW9uKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMucmVzdG9yZURhdGEoKTtcbiAgICAgICAgdGhpcy5tZXJnZU9wdGlvbihiYXNlT3B0aW9uKTtcbiAgICAgIH1cblxuICAgICAgb3B0aW9uQ2hhbmdlZCA9IHRydWU7XG4gICAgfVxuXG4gICAgaWYgKHR5cGUgPT09ICd0aW1lbGluZScgfHwgdHlwZSA9PT0gJ21lZGlhJykge1xuICAgICAgdGhpcy5yZXN0b3JlRGF0YSgpO1xuICAgIH1cblxuICAgIGlmICghdHlwZSB8fCB0eXBlID09PSAncmVjcmVhdGUnIHx8IHR5cGUgPT09ICd0aW1lbGluZScpIHtcbiAgICAgIHZhciB0aW1lbGluZU9wdGlvbiA9IG9wdGlvbk1hbmFnZXIuZ2V0VGltZWxpbmVPcHRpb24odGhpcyk7XG4gICAgICB0aW1lbGluZU9wdGlvbiAmJiAodGhpcy5tZXJnZU9wdGlvbih0aW1lbGluZU9wdGlvbiksIG9wdGlvbkNoYW5nZWQgPSB0cnVlKTtcbiAgICB9XG5cbiAgICBpZiAoIXR5cGUgfHwgdHlwZSA9PT0gJ3JlY3JlYXRlJyB8fCB0eXBlID09PSAnbWVkaWEnKSB7XG4gICAgICB2YXIgbWVkaWFPcHRpb25zID0gb3B0aW9uTWFuYWdlci5nZXRNZWRpYU9wdGlvbih0aGlzLCB0aGlzLl9hcGkpO1xuXG4gICAgICBpZiAobWVkaWFPcHRpb25zLmxlbmd0aCkge1xuICAgICAgICBlYWNoKG1lZGlhT3B0aW9ucywgZnVuY3Rpb24gKG1lZGlhT3B0aW9uKSB7XG4gICAgICAgICAgdGhpcy5tZXJnZU9wdGlvbihtZWRpYU9wdGlvbiwgb3B0aW9uQ2hhbmdlZCA9IHRydWUpO1xuICAgICAgICB9LCB0aGlzKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gb3B0aW9uQ2hhbmdlZDtcbiAgfSxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgbWVyZ2VPcHRpb246IGZ1bmN0aW9uIChuZXdPcHRpb24pIHtcbiAgICB2YXIgb3B0aW9uID0gdGhpcy5vcHRpb247XG4gICAgdmFyIGNvbXBvbmVudHNNYXAgPSB0aGlzLl9jb21wb25lbnRzTWFwO1xuICAgIHZhciBuZXdDcHRUeXBlcyA9IFtdO1xuICAgIHJlc2V0U291cmNlRGVmYXVsdGVyKHRoaXMpOyAvLyBJZiBubyBjb21wb25lbnQgY2xhc3MsIG1lcmdlIGRpcmVjdGx5LlxuICAgIC8vIEZvciBleGFtcGxlOiBjb2xvciwgYW5pbWFpdG9uIG9wdGlvbnMsIGV0Yy5cblxuICAgIGVhY2gobmV3T3B0aW9uLCBmdW5jdGlvbiAoY29tcG9uZW50T3B0aW9uLCBtYWluVHlwZSkge1xuICAgICAgaWYgKGNvbXBvbmVudE9wdGlvbiA9PSBudWxsKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKCFDb21wb25lbnRNb2RlbC5oYXNDbGFzcyhtYWluVHlwZSkpIHtcbiAgICAgICAgLy8gZ2xvYmFsU2V0dGluZ1Rhc2suZGlydHkoKTtcbiAgICAgICAgb3B0aW9uW21haW5UeXBlXSA9IG9wdGlvblttYWluVHlwZV0gPT0gbnVsbCA/IGNsb25lKGNvbXBvbmVudE9wdGlvbikgOiBtZXJnZShvcHRpb25bbWFpblR5cGVdLCBjb21wb25lbnRPcHRpb24sIHRydWUpO1xuICAgICAgfSBlbHNlIGlmIChtYWluVHlwZSkge1xuICAgICAgICBuZXdDcHRUeXBlcy5wdXNoKG1haW5UeXBlKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBDb21wb25lbnRNb2RlbC50b3BvbG9naWNhbFRyYXZlbChuZXdDcHRUeXBlcywgQ29tcG9uZW50TW9kZWwuZ2V0QWxsQ2xhc3NNYWluVHlwZXMoKSwgdmlzaXRDb21wb25lbnQsIHRoaXMpO1xuXG4gICAgZnVuY3Rpb24gdmlzaXRDb21wb25lbnQobWFpblR5cGUsIGRlcGVuZGVuY2llcykge1xuICAgICAgdmFyIG5ld0NwdE9wdGlvbkxpc3QgPSBtb2RlbFV0aWwubm9ybWFsaXplVG9BcnJheShuZXdPcHRpb25bbWFpblR5cGVdKTtcbiAgICAgIHZhciBtYXBSZXN1bHQgPSBtb2RlbFV0aWwubWFwcGluZ1RvRXhpc3RzKGNvbXBvbmVudHNNYXAuZ2V0KG1haW5UeXBlKSwgbmV3Q3B0T3B0aW9uTGlzdCk7XG4gICAgICBtb2RlbFV0aWwubWFrZUlkQW5kTmFtZShtYXBSZXN1bHQpOyAvLyBTZXQgbWFpblR5cGUgYW5kIGNvbXBsZXRlIHN1YlR5cGUuXG5cbiAgICAgIGVhY2gobWFwUmVzdWx0LCBmdW5jdGlvbiAoaXRlbSwgaW5kZXgpIHtcbiAgICAgICAgdmFyIG9wdCA9IGl0ZW0ub3B0aW9uO1xuXG4gICAgICAgIGlmIChpc09iamVjdChvcHQpKSB7XG4gICAgICAgICAgaXRlbS5rZXlJbmZvLm1haW5UeXBlID0gbWFpblR5cGU7XG4gICAgICAgICAgaXRlbS5rZXlJbmZvLnN1YlR5cGUgPSBkZXRlcm1pbmVTdWJUeXBlKG1haW5UeXBlLCBvcHQsIGl0ZW0uZXhpc3QpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHZhciBkZXBlbmRlbnRNb2RlbHMgPSBnZXRDb21wb25lbnRzQnlUeXBlcyhjb21wb25lbnRzTWFwLCBkZXBlbmRlbmNpZXMpO1xuICAgICAgb3B0aW9uW21haW5UeXBlXSA9IFtdO1xuICAgICAgY29tcG9uZW50c01hcC5zZXQobWFpblR5cGUsIFtdKTtcbiAgICAgIGVhY2gobWFwUmVzdWx0LCBmdW5jdGlvbiAocmVzdWx0SXRlbSwgaW5kZXgpIHtcbiAgICAgICAgdmFyIGNvbXBvbmVudE1vZGVsID0gcmVzdWx0SXRlbS5leGlzdDtcbiAgICAgICAgdmFyIG5ld0NwdE9wdGlvbiA9IHJlc3VsdEl0ZW0ub3B0aW9uO1xuICAgICAgICBhc3NlcnQoaXNPYmplY3QobmV3Q3B0T3B0aW9uKSB8fCBjb21wb25lbnRNb2RlbCwgJ0VtcHR5IGNvbXBvbmVudCBkZWZpbml0aW9uJyk7IC8vIENvbnNpZGVyIHdoZXJlIGlzIG5vIG5ldyBvcHRpb24gYW5kIHNob3VsZCBiZSBtZXJnZWQgdXNpbmcge30sXG4gICAgICAgIC8vIHNlZSByZW1vdmVFZGdlQW5kQWRkIGluIHRvcG9sb2dpY2FsVHJhdmVsIGFuZFxuICAgICAgICAvLyBDb21wb25lbnRNb2RlbC5nZXRBbGxDbGFzc01haW5UeXBlcy5cblxuICAgICAgICBpZiAoIW5ld0NwdE9wdGlvbikge1xuICAgICAgICAgIGNvbXBvbmVudE1vZGVsLm1lcmdlT3B0aW9uKHt9LCB0aGlzKTtcbiAgICAgICAgICBjb21wb25lbnRNb2RlbC5vcHRpb25VcGRhdGVkKHt9LCBmYWxzZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFyIENvbXBvbmVudE1vZGVsQ2xhc3MgPSBDb21wb25lbnRNb2RlbC5nZXRDbGFzcyhtYWluVHlwZSwgcmVzdWx0SXRlbS5rZXlJbmZvLnN1YlR5cGUsIHRydWUpO1xuXG4gICAgICAgICAgaWYgKGNvbXBvbmVudE1vZGVsICYmIGNvbXBvbmVudE1vZGVsIGluc3RhbmNlb2YgQ29tcG9uZW50TW9kZWxDbGFzcykge1xuICAgICAgICAgICAgY29tcG9uZW50TW9kZWwubmFtZSA9IHJlc3VsdEl0ZW0ua2V5SW5mby5uYW1lOyAvLyBjb21wb25lbnRNb2RlbC5zZXR0aW5nVGFzayAmJiBjb21wb25lbnRNb2RlbC5zZXR0aW5nVGFzay5kaXJ0eSgpO1xuXG4gICAgICAgICAgICBjb21wb25lbnRNb2RlbC5tZXJnZU9wdGlvbihuZXdDcHRPcHRpb24sIHRoaXMpO1xuICAgICAgICAgICAgY29tcG9uZW50TW9kZWwub3B0aW9uVXBkYXRlZChuZXdDcHRPcHRpb24sIGZhbHNlKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gUEVORElORyBHbG9iYWwgYXMgcGFyZW50ID9cbiAgICAgICAgICAgIHZhciBleHRyYU9wdCA9IGV4dGVuZCh7XG4gICAgICAgICAgICAgIGRlcGVuZGVudE1vZGVsczogZGVwZW5kZW50TW9kZWxzLFxuICAgICAgICAgICAgICBjb21wb25lbnRJbmRleDogaW5kZXhcbiAgICAgICAgICAgIH0sIHJlc3VsdEl0ZW0ua2V5SW5mbyk7XG4gICAgICAgICAgICBjb21wb25lbnRNb2RlbCA9IG5ldyBDb21wb25lbnRNb2RlbENsYXNzKG5ld0NwdE9wdGlvbiwgdGhpcywgdGhpcywgZXh0cmFPcHQpO1xuICAgICAgICAgICAgZXh0ZW5kKGNvbXBvbmVudE1vZGVsLCBleHRyYU9wdCk7XG4gICAgICAgICAgICBjb21wb25lbnRNb2RlbC5pbml0KG5ld0NwdE9wdGlvbiwgdGhpcywgdGhpcywgZXh0cmFPcHQpOyAvLyBDYWxsIG9wdGlvblVwZGF0ZWQgYWZ0ZXIgaW5pdC5cbiAgICAgICAgICAgIC8vIG5ld0NwdE9wdGlvbiBoYXMgYmVlbiB1c2VkIGFzIGNvbXBvbmVudE1vZGVsLm9wdGlvblxuICAgICAgICAgICAgLy8gYW5kIG1heSBiZSBtZXJnZWQgd2l0aCB0aGVtZSBhbmQgZGVmYXVsdCwgc28gcGFzcyBudWxsXG4gICAgICAgICAgICAvLyB0byBhdm9pZCBjb25mdXNpb24uXG5cbiAgICAgICAgICAgIGNvbXBvbmVudE1vZGVsLm9wdGlvblVwZGF0ZWQobnVsbCwgdHJ1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29tcG9uZW50c01hcC5nZXQobWFpblR5cGUpW2luZGV4XSA9IGNvbXBvbmVudE1vZGVsO1xuICAgICAgICBvcHRpb25bbWFpblR5cGVdW2luZGV4XSA9IGNvbXBvbmVudE1vZGVsLm9wdGlvbjtcbiAgICAgIH0sIHRoaXMpOyAvLyBCYWNrdXAgc2VyaWVzIGZvciBmaWx0ZXJpbmcuXG5cbiAgICAgIGlmIChtYWluVHlwZSA9PT0gJ3NlcmllcycpIHtcbiAgICAgICAgY3JlYXRlU2VyaWVzSW5kaWNlcyh0aGlzLCBjb21wb25lbnRzTWFwLmdldCgnc2VyaWVzJykpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX3Nlcmllc0luZGljZXNNYXAgPSBjcmVhdGVIYXNoTWFwKHRoaXMuX3Nlcmllc0luZGljZXMgPSB0aGlzLl9zZXJpZXNJbmRpY2VzIHx8IFtdKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IG9wdGlvbiBmb3Igb3V0cHV0IChjbG9uZWQgb3B0aW9uIGFuZCBpbm5lciBpbmZvIHJlbW92ZWQpXG4gICAqIEBwdWJsaWNcbiAgICogQHJldHVybiB7T2JqZWN0fVxuICAgKi9cbiAgZ2V0T3B0aW9uOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIG9wdGlvbiA9IGNsb25lKHRoaXMub3B0aW9uKTtcbiAgICBlYWNoKG9wdGlvbiwgZnVuY3Rpb24gKG9wdHMsIG1haW5UeXBlKSB7XG4gICAgICBpZiAoQ29tcG9uZW50TW9kZWwuaGFzQ2xhc3MobWFpblR5cGUpKSB7XG4gICAgICAgIHZhciBvcHRzID0gbW9kZWxVdGlsLm5vcm1hbGl6ZVRvQXJyYXkob3B0cyk7XG5cbiAgICAgICAgZm9yICh2YXIgaSA9IG9wdHMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICAvLyBSZW1vdmUgb3B0aW9ucyB3aXRoIGlubmVyIGlkLlxuICAgICAgICAgIGlmIChtb2RlbFV0aWwuaXNJZElubmVyKG9wdHNbaV0pKSB7XG4gICAgICAgICAgICBvcHRzLnNwbGljZShpLCAxKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBvcHRpb25bbWFpblR5cGVdID0gb3B0cztcbiAgICAgIH1cbiAgICB9KTtcbiAgICBkZWxldGUgb3B0aW9uW09QVElPTl9JTk5FUl9LRVldO1xuICAgIHJldHVybiBvcHRpb247XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfVxuICAgKi9cbiAgZ2V0VGhlbWU6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fdGhlbWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBtYWluVHlwZVxuICAgKiBAcGFyYW0ge251bWJlcn0gW2lkeD0wXVxuICAgKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Db21wb25lbnR9XG4gICAqL1xuICBnZXRDb21wb25lbnQ6IGZ1bmN0aW9uIChtYWluVHlwZSwgaWR4KSB7XG4gICAgdmFyIGxpc3QgPSB0aGlzLl9jb21wb25lbnRzTWFwLmdldChtYWluVHlwZSk7XG5cbiAgICBpZiAobGlzdCkge1xuICAgICAgcmV0dXJuIGxpc3RbaWR4IHx8IDBdO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogSWYgbm9uZSBvZiBpbmRleCBhbmQgaWQgYW5kIG5hbWUgdXNlZCwgcmV0dXJuIGFsbCBjb21wb25lbnRzIHdpdGggbWFpblR5cGUuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBjb25kaXRpb25cbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbmRpdGlvbi5tYWluVHlwZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2NvbmRpdGlvbi5zdWJUeXBlXSBJZiBpZ25vcmUsIG9ubHkgcXVlcnkgYnkgbWFpblR5cGVcbiAgICogQHBhcmFtIHtudW1iZXJ8QXJyYXkuPG51bWJlcj59IFtjb25kaXRpb24uaW5kZXhdIEVpdGhlciBpbnB1dCBpbmRleCBvciBpZCBvciBuYW1lLlxuICAgKiBAcGFyYW0ge3N0cmluZ3xBcnJheS48c3RyaW5nPn0gW2NvbmRpdGlvbi5pZF0gRWl0aGVyIGlucHV0IGluZGV4IG9yIGlkIG9yIG5hbWUuXG4gICAqIEBwYXJhbSB7c3RyaW5nfEFycmF5LjxzdHJpbmc+fSBbY29uZGl0aW9uLm5hbWVdIEVpdGhlciBpbnB1dCBpbmRleCBvciBpZCBvciBuYW1lLlxuICAgKiBAcmV0dXJuIHtBcnJheS48bW9kdWxlOmVjaGFydHMvbW9kZWwvQ29tcG9uZW50Pn1cbiAgICovXG4gIHF1ZXJ5Q29tcG9uZW50czogZnVuY3Rpb24gKGNvbmRpdGlvbikge1xuICAgIHZhciBtYWluVHlwZSA9IGNvbmRpdGlvbi5tYWluVHlwZTtcblxuICAgIGlmICghbWFpblR5cGUpIHtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG5cbiAgICB2YXIgaW5kZXggPSBjb25kaXRpb24uaW5kZXg7XG4gICAgdmFyIGlkID0gY29uZGl0aW9uLmlkO1xuICAgIHZhciBuYW1lID0gY29uZGl0aW9uLm5hbWU7XG5cbiAgICB2YXIgY3B0cyA9IHRoaXMuX2NvbXBvbmVudHNNYXAuZ2V0KG1haW5UeXBlKTtcblxuICAgIGlmICghY3B0cyB8fCAhY3B0cy5sZW5ndGgpIHtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG5cbiAgICB2YXIgcmVzdWx0O1xuXG4gICAgaWYgKGluZGV4ICE9IG51bGwpIHtcbiAgICAgIGlmICghaXNBcnJheShpbmRleCkpIHtcbiAgICAgICAgaW5kZXggPSBbaW5kZXhdO1xuICAgICAgfVxuXG4gICAgICByZXN1bHQgPSBmaWx0ZXIobWFwKGluZGV4LCBmdW5jdGlvbiAoaWR4KSB7XG4gICAgICAgIHJldHVybiBjcHRzW2lkeF07XG4gICAgICB9KSwgZnVuY3Rpb24gKHZhbCkge1xuICAgICAgICByZXR1cm4gISF2YWw7XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKGlkICE9IG51bGwpIHtcbiAgICAgIHZhciBpc0lkQXJyYXkgPSBpc0FycmF5KGlkKTtcbiAgICAgIHJlc3VsdCA9IGZpbHRlcihjcHRzLCBmdW5jdGlvbiAoY3B0KSB7XG4gICAgICAgIHJldHVybiBpc0lkQXJyYXkgJiYgaW5kZXhPZihpZCwgY3B0LmlkKSA+PSAwIHx8ICFpc0lkQXJyYXkgJiYgY3B0LmlkID09PSBpZDtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAobmFtZSAhPSBudWxsKSB7XG4gICAgICB2YXIgaXNOYW1lQXJyYXkgPSBpc0FycmF5KG5hbWUpO1xuICAgICAgcmVzdWx0ID0gZmlsdGVyKGNwdHMsIGZ1bmN0aW9uIChjcHQpIHtcbiAgICAgICAgcmV0dXJuIGlzTmFtZUFycmF5ICYmIGluZGV4T2YobmFtZSwgY3B0Lm5hbWUpID49IDAgfHwgIWlzTmFtZUFycmF5ICYmIGNwdC5uYW1lID09PSBuYW1lO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIFJldHVybiBhbGwgY29tcG9uZW50cyB3aXRoIG1haW5UeXBlXG4gICAgICByZXN1bHQgPSBjcHRzLnNsaWNlKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZpbHRlckJ5U3ViVHlwZShyZXN1bHQsIGNvbmRpdGlvbik7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFRoZSBpbnRlcmZhY2UgaXMgZGlmZmVyZW50IGZyb20gcXVlcnlDb21wb25lbnRzLFxuICAgKiB3aGljaCBpcyBjb252ZW5pZW50IGZvciBpbm5lciB1c2FnZS5cbiAgICpcbiAgICogQHVzYWdlXG4gICAqIHZhciByZXN1bHQgPSBmaW5kQ29tcG9uZW50cyhcbiAgICogICAgIHttYWluVHlwZTogJ2RhdGFab29tJywgcXVlcnk6IHtkYXRhWm9vbUlkOiAnYWJjJ319XG4gICAqICk7XG4gICAqIHZhciByZXN1bHQgPSBmaW5kQ29tcG9uZW50cyhcbiAgICogICAgIHttYWluVHlwZTogJ3NlcmllcycsIHN1YlR5cGU6ICdwaWUnLCBxdWVyeToge3Nlcmllc05hbWU6ICd1aW8nfX1cbiAgICogKTtcbiAgICogdmFyIHJlc3VsdCA9IGZpbmRDb21wb25lbnRzKFxuICAgKiAgICAge21haW5UeXBlOiAnc2VyaWVzJ30sXG4gICAqICAgICBmdW5jdGlvbiAobW9kZWwsIGluZGV4KSB7Li4ufVxuICAgKiApO1xuICAgKiAvLyByZXN1bHQgbGlrZSBbY29tcG9uZW50MCwgY29tcG9ubmV0MSwgLi4uXVxuICAgKlxuICAgKiBAcGFyYW0ge09iamVjdH0gY29uZGl0aW9uXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBjb25kaXRpb24ubWFpblR5cGUgTWFuZGF0b3J5LlxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2NvbmRpdGlvbi5zdWJUeXBlXSBPcHRpb25hbC5cbiAgICogQHBhcmFtIHtPYmplY3R9IFtjb25kaXRpb24ucXVlcnldIGxpa2Uge3h4eEluZGV4LCB4eHhJZCwgeHh4TmFtZX0sXG4gICAqICAgICAgICB3aGVyZSB4eHggaXMgbWFpblR5cGUuXG4gICAqICAgICAgICBJZiBxdWVyeSBhdHRyaWJ1dGUgaXMgbnVsbC91bmRlZmluZWQgb3IgaGFzIG5vIGluZGV4L2lkL25hbWUsXG4gICAqICAgICAgICBkbyBub3QgZmlsdGVyaW5nIGJ5IHF1ZXJ5IGNvbmRpdGlvbnMsIHdoaWNoIGlzIGNvbnZlbmllbnQgZm9yXG4gICAqICAgICAgICBuby1wYXlsb2FkIHNpdHVhdGlvbnMgb3Igd2hlbiB0YXJnZXQgb2YgYWN0aW9uIGlzIGdsb2JhbC5cbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gW2NvbmRpdGlvbi5maWx0ZXJdIHBhcmFtZXRlcjogY29tcG9uZW50LCByZXR1cm4gYm9vbGVhbi5cbiAgICogQHJldHVybiB7QXJyYXkuPG1vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudD59XG4gICAqL1xuICBmaW5kQ29tcG9uZW50czogZnVuY3Rpb24gKGNvbmRpdGlvbikge1xuICAgIHZhciBxdWVyeSA9IGNvbmRpdGlvbi5xdWVyeTtcbiAgICB2YXIgbWFpblR5cGUgPSBjb25kaXRpb24ubWFpblR5cGU7XG4gICAgdmFyIHF1ZXJ5Q29uZCA9IGdldFF1ZXJ5Q29uZChxdWVyeSk7XG4gICAgdmFyIHJlc3VsdCA9IHF1ZXJ5Q29uZCA/IHRoaXMucXVlcnlDb21wb25lbnRzKHF1ZXJ5Q29uZCkgOiB0aGlzLl9jb21wb25lbnRzTWFwLmdldChtYWluVHlwZSk7XG4gICAgcmV0dXJuIGRvRmlsdGVyKGZpbHRlckJ5U3ViVHlwZShyZXN1bHQsIGNvbmRpdGlvbikpO1xuXG4gICAgZnVuY3Rpb24gZ2V0UXVlcnlDb25kKHEpIHtcbiAgICAgIHZhciBpbmRleEF0dHIgPSBtYWluVHlwZSArICdJbmRleCc7XG4gICAgICB2YXIgaWRBdHRyID0gbWFpblR5cGUgKyAnSWQnO1xuICAgICAgdmFyIG5hbWVBdHRyID0gbWFpblR5cGUgKyAnTmFtZSc7XG4gICAgICByZXR1cm4gcSAmJiAocVtpbmRleEF0dHJdICE9IG51bGwgfHwgcVtpZEF0dHJdICE9IG51bGwgfHwgcVtuYW1lQXR0cl0gIT0gbnVsbCkgPyB7XG4gICAgICAgIG1haW5UeXBlOiBtYWluVHlwZSxcbiAgICAgICAgLy8gc3ViVHlwZSB3aWxsIGJlIGZpbHRlcmVkIGZpbmFsbHkuXG4gICAgICAgIGluZGV4OiBxW2luZGV4QXR0cl0sXG4gICAgICAgIGlkOiBxW2lkQXR0cl0sXG4gICAgICAgIG5hbWU6IHFbbmFtZUF0dHJdXG4gICAgICB9IDogbnVsbDtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBkb0ZpbHRlcihyZXMpIHtcbiAgICAgIHJldHVybiBjb25kaXRpb24uZmlsdGVyID8gZmlsdGVyKHJlcywgY29uZGl0aW9uLmZpbHRlcikgOiByZXM7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAdXNhZ2VcbiAgICogZWFjaENvbXBvbmVudCgnbGVnZW5kJywgZnVuY3Rpb24gKGxlZ2VuZE1vZGVsLCBpbmRleCkge1xuICAgKiAgICAgLi4uXG4gICAqIH0pO1xuICAgKiBlYWNoQ29tcG9uZW50KGZ1bmN0aW9uIChjb21wb25lbnRUeXBlLCBtb2RlbCwgaW5kZXgpIHtcbiAgICogICAgIC8vIGNvbXBvbmVudFR5cGUgZG9lcyBub3QgaW5jbHVkZSBzdWJUeXBlXG4gICAqICAgICAvLyAoY29tcG9uZW50VHlwZSBpcyAneHh4JyBidXQgbm90ICd4eHguYWEnKVxuICAgKiB9KTtcbiAgICogZWFjaENvbXBvbmVudChcbiAgICogICAgIHttYWluVHlwZTogJ2RhdGFab29tJywgcXVlcnk6IHtkYXRhWm9vbUlkOiAnYWJjJ319LFxuICAgKiAgICAgZnVuY3Rpb24gKG1vZGVsLCBpbmRleCkgey4uLn1cbiAgICogKTtcbiAgICogZWFjaENvbXBvbmVudChcbiAgICogICAgIHttYWluVHlwZTogJ3NlcmllcycsIHN1YlR5cGU6ICdwaWUnLCBxdWVyeToge3Nlcmllc05hbWU6ICd1aW8nfX0sXG4gICAqICAgICBmdW5jdGlvbiAobW9kZWwsIGluZGV4KSB7Li4ufVxuICAgKiApO1xuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ3xPYmplY3Q9fSBtYWluVHlwZSBXaGVuIG1haW5UeXBlIGlzIG9iamVjdCwgdGhlIGRlZmluaXRpb25cbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXMgdGhlIHNhbWUgYXMgdGhlIG1ldGhvZCAnZmluZENvbXBvbmVudHMnLlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICAgKiBAcGFyYW0geyp9IGNvbnRleHRcbiAgICovXG4gIGVhY2hDb21wb25lbnQ6IGZ1bmN0aW9uIChtYWluVHlwZSwgY2IsIGNvbnRleHQpIHtcbiAgICB2YXIgY29tcG9uZW50c01hcCA9IHRoaXMuX2NvbXBvbmVudHNNYXA7XG5cbiAgICBpZiAodHlwZW9mIG1haW5UeXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb250ZXh0ID0gY2I7XG4gICAgICBjYiA9IG1haW5UeXBlO1xuICAgICAgY29tcG9uZW50c01hcC5lYWNoKGZ1bmN0aW9uIChjb21wb25lbnRzLCBjb21wb25lbnRUeXBlKSB7XG4gICAgICAgIGVhY2goY29tcG9uZW50cywgZnVuY3Rpb24gKGNvbXBvbmVudCwgaW5kZXgpIHtcbiAgICAgICAgICBjYi5jYWxsKGNvbnRleHQsIGNvbXBvbmVudFR5cGUsIGNvbXBvbmVudCwgaW5kZXgpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAoaXNTdHJpbmcobWFpblR5cGUpKSB7XG4gICAgICBlYWNoKGNvbXBvbmVudHNNYXAuZ2V0KG1haW5UeXBlKSwgY2IsIGNvbnRleHQpO1xuICAgIH0gZWxzZSBpZiAoaXNPYmplY3QobWFpblR5cGUpKSB7XG4gICAgICB2YXIgcXVlcnlSZXN1bHQgPSB0aGlzLmZpbmRDb21wb25lbnRzKG1haW5UeXBlKTtcbiAgICAgIGVhY2gocXVlcnlSZXN1bHQsIGNiLCBjb250ZXh0KTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gICAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXM+fVxuICAgKi9cbiAgZ2V0U2VyaWVzQnlOYW1lOiBmdW5jdGlvbiAobmFtZSkge1xuICAgIHZhciBzZXJpZXMgPSB0aGlzLl9jb21wb25lbnRzTWFwLmdldCgnc2VyaWVzJyk7XG5cbiAgICByZXR1cm4gZmlsdGVyKHNlcmllcywgZnVuY3Rpb24gKG9uZVNlcmllcykge1xuICAgICAgcmV0dXJuIG9uZVNlcmllcy5uYW1lID09PSBuYW1lO1xuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcn0gc2VyaWVzSW5kZXhcbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfVxuICAgKi9cbiAgZ2V0U2VyaWVzQnlJbmRleDogZnVuY3Rpb24gKHNlcmllc0luZGV4KSB7XG4gICAgcmV0dXJuIHRoaXMuX2NvbXBvbmVudHNNYXAuZ2V0KCdzZXJpZXMnKVtzZXJpZXNJbmRleF07XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBzZXJpZXMgbGlzdCBiZWZvcmUgZmlsdGVyZWQgYnkgdHlwZS5cbiAgICogRklYTUU6IHJlbmFtZSB0byBnZXRSYXdTZXJpZXNCeVR5cGU/XG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBzdWJUeXBlXG4gICAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXM+fVxuICAgKi9cbiAgZ2V0U2VyaWVzQnlUeXBlOiBmdW5jdGlvbiAoc3ViVHlwZSkge1xuICAgIHZhciBzZXJpZXMgPSB0aGlzLl9jb21wb25lbnRzTWFwLmdldCgnc2VyaWVzJyk7XG5cbiAgICByZXR1cm4gZmlsdGVyKHNlcmllcywgZnVuY3Rpb24gKG9uZVNlcmllcykge1xuICAgICAgcmV0dXJuIG9uZVNlcmllcy5zdWJUeXBlID09PSBzdWJUeXBlO1xuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtBcnJheS48bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzPn1cbiAgICovXG4gIGdldFNlcmllczogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9jb21wb25lbnRzTWFwLmdldCgnc2VyaWVzJykuc2xpY2UoKTtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7bnVtYmVyfVxuICAgKi9cbiAgZ2V0U2VyaWVzQ291bnQ6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fY29tcG9uZW50c01hcC5nZXQoJ3NlcmllcycpLmxlbmd0aDtcbiAgfSxcblxuICAvKipcbiAgICogQWZ0ZXIgZmlsdGVyaW5nLCBzZXJpZXMgbWF5IGJlIGRpZmZlcmVudFxuICAgKiBmcm9tZSByYXcgc2VyaWVzLlxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICAgKiBAcGFyYW0geyp9IGNvbnRleHRcbiAgICovXG4gIGVhY2hTZXJpZXM6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIGFzc2VydFNlcmllc0luaXRpYWxpemVkKHRoaXMpO1xuICAgIGVhY2godGhpcy5fc2VyaWVzSW5kaWNlcywgZnVuY3Rpb24gKHJhd1Nlcmllc0luZGV4KSB7XG4gICAgICB2YXIgc2VyaWVzID0gdGhpcy5fY29tcG9uZW50c01hcC5nZXQoJ3NlcmllcycpW3Jhd1Nlcmllc0luZGV4XTtcblxuICAgICAgY2IuY2FsbChjb250ZXh0LCBzZXJpZXMsIHJhd1Nlcmllc0luZGV4KTtcbiAgICB9LCB0aGlzKTtcbiAgfSxcblxuICAvKipcbiAgICogSXRlcmF0ZSByYXcgc2VyaWVzIGJlZm9yZSBmaWx0ZXJlZC5cbiAgICpcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAgICogQHBhcmFtIHsqfSBjb250ZXh0XG4gICAqL1xuICBlYWNoUmF3U2VyaWVzOiBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHtcbiAgICBlYWNoKHRoaXMuX2NvbXBvbmVudHNNYXAuZ2V0KCdzZXJpZXMnKSwgY2IsIGNvbnRleHQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBBZnRlciBmaWx0ZXJpbmcsIHNlcmllcyBtYXkgYmUgZGlmZmVyZW50LlxuICAgKiBmcm9tZSByYXcgc2VyaWVzLlxuICAgKlxuICAgKiBAcGFybWEge3N0cmluZ30gc3ViVHlwZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICAgKiBAcGFyYW0geyp9IGNvbnRleHRcbiAgICovXG4gIGVhY2hTZXJpZXNCeVR5cGU6IGZ1bmN0aW9uIChzdWJUeXBlLCBjYiwgY29udGV4dCkge1xuICAgIGFzc2VydFNlcmllc0luaXRpYWxpemVkKHRoaXMpO1xuICAgIGVhY2godGhpcy5fc2VyaWVzSW5kaWNlcywgZnVuY3Rpb24gKHJhd1Nlcmllc0luZGV4KSB7XG4gICAgICB2YXIgc2VyaWVzID0gdGhpcy5fY29tcG9uZW50c01hcC5nZXQoJ3NlcmllcycpW3Jhd1Nlcmllc0luZGV4XTtcblxuICAgICAgaWYgKHNlcmllcy5zdWJUeXBlID09PSBzdWJUeXBlKSB7XG4gICAgICAgIGNiLmNhbGwoY29udGV4dCwgc2VyaWVzLCByYXdTZXJpZXNJbmRleCk7XG4gICAgICB9XG4gICAgfSwgdGhpcyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEl0ZXJhdGUgcmF3IHNlcmllcyBiZWZvcmUgZmlsdGVyZWQgb2YgZ2l2ZW4gdHlwZS5cbiAgICpcbiAgICogQHBhcm1hIHtzdHJpbmd9IHN1YlR5cGVcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAgICogQHBhcmFtIHsqfSBjb250ZXh0XG4gICAqL1xuICBlYWNoUmF3U2VyaWVzQnlUeXBlOiBmdW5jdGlvbiAoc3ViVHlwZSwgY2IsIGNvbnRleHQpIHtcbiAgICByZXR1cm4gZWFjaCh0aGlzLmdldFNlcmllc0J5VHlwZShzdWJUeXBlKSwgY2IsIGNvbnRleHQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL1Nlcmllc30gc2VyaWVzTW9kZWxcbiAgICovXG4gIGlzU2VyaWVzRmlsdGVyZWQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIGFzc2VydFNlcmllc0luaXRpYWxpemVkKHRoaXMpO1xuICAgIHJldHVybiB0aGlzLl9zZXJpZXNJbmRpY2VzTWFwLmdldChzZXJpZXNNb2RlbC5jb21wb25lbnRJbmRleCkgPT0gbnVsbDtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICBnZXRDdXJyZW50U2VyaWVzSW5kaWNlczogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiAodGhpcy5fc2VyaWVzSW5kaWNlcyB8fCBbXSkuc2xpY2UoKTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAgICogQHBhcmFtIHsqfSBjb250ZXh0XG4gICAqL1xuICBmaWx0ZXJTZXJpZXM6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIGFzc2VydFNlcmllc0luaXRpYWxpemVkKHRoaXMpO1xuICAgIHZhciBmaWx0ZXJlZFNlcmllcyA9IGZpbHRlcih0aGlzLl9jb21wb25lbnRzTWFwLmdldCgnc2VyaWVzJyksIGNiLCBjb250ZXh0KTtcbiAgICBjcmVhdGVTZXJpZXNJbmRpY2VzKHRoaXMsIGZpbHRlcmVkU2VyaWVzKTtcbiAgfSxcbiAgcmVzdG9yZURhdGE6IGZ1bmN0aW9uIChwYXlsb2FkKSB7XG4gICAgdmFyIGNvbXBvbmVudHNNYXAgPSB0aGlzLl9jb21wb25lbnRzTWFwO1xuICAgIGNyZWF0ZVNlcmllc0luZGljZXModGhpcywgY29tcG9uZW50c01hcC5nZXQoJ3NlcmllcycpKTtcbiAgICB2YXIgY29tcG9uZW50VHlwZXMgPSBbXTtcbiAgICBjb21wb25lbnRzTWFwLmVhY2goZnVuY3Rpb24gKGNvbXBvbmVudHMsIGNvbXBvbmVudFR5cGUpIHtcbiAgICAgIGNvbXBvbmVudFR5cGVzLnB1c2goY29tcG9uZW50VHlwZSk7XG4gICAgfSk7XG4gICAgQ29tcG9uZW50TW9kZWwudG9wb2xvZ2ljYWxUcmF2ZWwoY29tcG9uZW50VHlwZXMsIENvbXBvbmVudE1vZGVsLmdldEFsbENsYXNzTWFpblR5cGVzKCksIGZ1bmN0aW9uIChjb21wb25lbnRUeXBlLCBkZXBlbmRlbmNpZXMpIHtcbiAgICAgIGVhY2goY29tcG9uZW50c01hcC5nZXQoY29tcG9uZW50VHlwZSksIGZ1bmN0aW9uIChjb21wb25lbnQpIHtcbiAgICAgICAgKGNvbXBvbmVudFR5cGUgIT09ICdzZXJpZXMnIHx8ICFpc05vdFRhcmdldFNlcmllcyhjb21wb25lbnQsIHBheWxvYWQpKSAmJiBjb21wb25lbnQucmVzdG9yZURhdGEoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59KTtcblxuZnVuY3Rpb24gaXNOb3RUYXJnZXRTZXJpZXMoc2VyaWVzTW9kZWwsIHBheWxvYWQpIHtcbiAgaWYgKHBheWxvYWQpIHtcbiAgICB2YXIgaW5kZXggPSBwYXlsb2FkLnNlaXJlc0luZGV4O1xuICAgIHZhciBpZCA9IHBheWxvYWQuc2VyaWVzSWQ7XG4gICAgdmFyIG5hbWUgPSBwYXlsb2FkLnNlcmllc05hbWU7XG4gICAgcmV0dXJuIGluZGV4ICE9IG51bGwgJiYgc2VyaWVzTW9kZWwuY29tcG9uZW50SW5kZXggIT09IGluZGV4IHx8IGlkICE9IG51bGwgJiYgc2VyaWVzTW9kZWwuaWQgIT09IGlkIHx8IG5hbWUgIT0gbnVsbCAmJiBzZXJpZXNNb2RlbC5uYW1lICE9PSBuYW1lO1xuICB9XG59XG4vKipcbiAqIEBpbm5lclxuICovXG5cblxuZnVuY3Rpb24gbWVyZ2VUaGVtZShvcHRpb24sIHRoZW1lKSB7XG4gIC8vIFBFTkRJTkdcbiAgLy8gTk9UIHVzZSBgY29sb3JMYXllcmAgaW4gdGhlbWUgaWYgb3B0aW9uIGhhcyBgY29sb3JgXG4gIHZhciBub3RNZXJnZUNvbG9yTGF5ZXIgPSBvcHRpb24uY29sb3IgJiYgIW9wdGlvbi5jb2xvckxheWVyO1xuICBlYWNoKHRoZW1lLCBmdW5jdGlvbiAodGhlbWVJdGVtLCBuYW1lKSB7XG4gICAgaWYgKG5hbWUgPT09ICdjb2xvckxheWVyJyAmJiBub3RNZXJnZUNvbG9yTGF5ZXIpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IC8vIOWmguaenOaciSBjb21wb25lbnQgbW9kZWwg5YiZ5oqK5YW35L2T55qEIG1lcmdlIOmAu+i+keS6pOe7meivpSBtb2RlbCDlpITnkIZcblxuXG4gICAgaWYgKCFDb21wb25lbnRNb2RlbC5oYXNDbGFzcyhuYW1lKSkge1xuICAgICAgaWYgKHR5cGVvZiB0aGVtZUl0ZW0gPT09ICdvYmplY3QnKSB7XG4gICAgICAgIG9wdGlvbltuYW1lXSA9ICFvcHRpb25bbmFtZV0gPyBjbG9uZSh0aGVtZUl0ZW0pIDogbWVyZ2Uob3B0aW9uW25hbWVdLCB0aGVtZUl0ZW0sIGZhbHNlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmIChvcHRpb25bbmFtZV0gPT0gbnVsbCkge1xuICAgICAgICAgIG9wdGlvbltuYW1lXSA9IHRoZW1lSXRlbTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGluaXRCYXNlKGJhc2VPcHRpb24pIHtcbiAgYmFzZU9wdGlvbiA9IGJhc2VPcHRpb247IC8vIFVzaW5nIE9QVElPTl9JTk5FUl9LRVkgdG8gbWFyayB0aGF0IHRoaXMgb3B0aW9uIGNhbiBub3QgYmUgdXNlZCBvdXRzaWRlLFxuICAvLyBpLmUuIGBjaGFydC5zZXRPcHRpb24oY2hhcnQuZ2V0TW9kZWwoKS5vcHRpb24pO2AgaXMgZm9yYmlkZW4uXG5cbiAgdGhpcy5vcHRpb24gPSB7fTtcbiAgdGhpcy5vcHRpb25bT1BUSU9OX0lOTkVSX0tFWV0gPSAxO1xuICAvKipcbiAgICogSW5pdCB3aXRoIHNlcmllczogW10sIGluIGNhc2Ugb2YgY2FsbGluZyBmaW5kU2VyaWVzIG1ldGhvZFxuICAgKiBiZWZvcmUgc2VyaWVzIGluaXRpYWxpemVkLlxuICAgKiBAdHlwZSB7T2JqZWN0LjxzdHJpbmcsIEFycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbD4+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9jb21wb25lbnRzTWFwID0gY3JlYXRlSGFzaE1hcCh7XG4gICAgc2VyaWVzOiBbXVxuICB9KTtcbiAgLyoqXG4gICAqIE1hcHBpbmcgYmV0d2VlbiBmaWx0ZXJlZCBzZXJpZXMgbGlzdCBhbmQgcmF3IHNlcmllcyBsaXN0LlxuICAgKiBrZXk6IGZpbHRlcmVkIHNlcmllcyBpbmRpY2VzLCB2YWx1ZTogcmF3IHNlcmllcyBpbmRpY2VzLlxuICAgKiBAdHlwZSB7QXJyYXkuPG51Ym1lcj59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX3Nlcmllc0luZGljZXM7XG4gIHRoaXMuX3Nlcmllc0luZGljZXNNYXA7XG4gIG1lcmdlVGhlbWUoYmFzZU9wdGlvbiwgdGhpcy5fdGhlbWUub3B0aW9uKTsgLy8gVE9ETyBOZWVkcyBjbG9uZSB3aGVuIG1lcmdpbmcgdG8gdGhlIHVuZXhpc3RlZCBwcm9wZXJ0eVxuXG4gIG1lcmdlKGJhc2VPcHRpb24sIGdsb2JhbERlZmF1bHQsIGZhbHNlKTtcbiAgdGhpcy5tZXJnZU9wdGlvbihiYXNlT3B0aW9uKTtcbn1cbi8qKlxuICogQGlubmVyXG4gKiBAcGFyYW0ge0FycmF5LjxzdHJpbmc+fHN0cmluZ30gdHlwZXMgbW9kZWwgdHlwZXNcbiAqIEByZXR1cm4ge09iamVjdH0ga2V5OiB7c3RyaW5nfSB0eXBlLCB2YWx1ZToge0FycmF5LjxPYmplY3Q+fSBtb2RlbHNcbiAqL1xuXG5cbmZ1bmN0aW9uIGdldENvbXBvbmVudHNCeVR5cGVzKGNvbXBvbmVudHNNYXAsIHR5cGVzKSB7XG4gIGlmICghaXNBcnJheSh0eXBlcykpIHtcbiAgICB0eXBlcyA9IHR5cGVzID8gW3R5cGVzXSA6IFtdO1xuICB9XG5cbiAgdmFyIHJldCA9IHt9O1xuICBlYWNoKHR5cGVzLCBmdW5jdGlvbiAodHlwZSkge1xuICAgIHJldFt0eXBlXSA9IChjb21wb25lbnRzTWFwLmdldCh0eXBlKSB8fCBbXSkuc2xpY2UoKTtcbiAgfSk7XG4gIHJldHVybiByZXQ7XG59XG4vKipcbiAqIEBpbm5lclxuICovXG5cblxuZnVuY3Rpb24gZGV0ZXJtaW5lU3ViVHlwZShtYWluVHlwZSwgbmV3Q3B0T3B0aW9uLCBleGlzdENvbXBvbmVudCkge1xuICB2YXIgc3ViVHlwZSA9IG5ld0NwdE9wdGlvbi50eXBlID8gbmV3Q3B0T3B0aW9uLnR5cGUgOiBleGlzdENvbXBvbmVudCA/IGV4aXN0Q29tcG9uZW50LnN1YlR5cGUgLy8gVXNlIGRldGVybWluZVN1YlR5cGUgb25seSB3aGVuIHRoZXJlIGlzIG5vIGV4aXN0Q29tcG9uZW50LlxuICA6IENvbXBvbmVudE1vZGVsLmRldGVybWluZVN1YlR5cGUobWFpblR5cGUsIG5ld0NwdE9wdGlvbik7IC8vIHRvb2x0aXAsIG1hcmtsaW5lLCBtYXJrcG9pbnQgbWF5IGFsd2F5cyBoYXMgbm8gc3ViVHlwZVxuXG4gIHJldHVybiBzdWJUeXBlO1xufVxuLyoqXG4gKiBAaW5uZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIGNyZWF0ZVNlcmllc0luZGljZXMoZWNNb2RlbCwgc2VyaWVzTW9kZWxzKSB7XG4gIGVjTW9kZWwuX3Nlcmllc0luZGljZXNNYXAgPSBjcmVhdGVIYXNoTWFwKGVjTW9kZWwuX3Nlcmllc0luZGljZXMgPSBtYXAoc2VyaWVzTW9kZWxzLCBmdW5jdGlvbiAoc2VyaWVzKSB7XG4gICAgcmV0dXJuIHNlcmllcy5jb21wb25lbnRJbmRleDtcbiAgfSkgfHwgW10pO1xufVxuLyoqXG4gKiBAaW5uZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIGZpbHRlckJ5U3ViVHlwZShjb21wb25lbnRzLCBjb25kaXRpb24pIHtcbiAgLy8gVXNpbmcgaGFzT3duUHJvcGVydHkgZm9yIHJlc3RyaWN0LiBDb25zaWRlclxuICAvLyBzdWJUeXBlIGlzIHVuZGVmaW5lZCBpbiB1c2VyIHBheWxvYWQuXG4gIHJldHVybiBjb25kaXRpb24uaGFzT3duUHJvcGVydHkoJ3N1YlR5cGUnKSA/IGZpbHRlcihjb21wb25lbnRzLCBmdW5jdGlvbiAoY3B0KSB7XG4gICAgcmV0dXJuIGNwdC5zdWJUeXBlID09PSBjb25kaXRpb24uc3ViVHlwZTtcbiAgfSkgOiBjb21wb25lbnRzO1xufVxuLyoqXG4gKiBAaW5uZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIGFzc2VydFNlcmllc0luaXRpYWxpemVkKGVjTW9kZWwpIHt9XG5cbm1peGluKEdsb2JhbE1vZGVsLCBjb2xvclBhbGV0dGVNaXhpbik7XG52YXIgX2RlZmF1bHQgPSBHbG9iYWxNb2RlbDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7QUFNQTs7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoZkE7QUFDQTtBQWtmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFEQTtBQUdBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/Global.js
