__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genArrProps", function() { return genArrProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "valueProp", function() { return valueProp; });
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree-select/es/util.js");


var internalValProp = prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.number]);
function genArrProps(propType) {
  return prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.oneOfType([propType, prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.arrayOf(propType)]);
}
/**
 * Origin code check `multiple` is true when `treeCheckStrictly` & `labelInValue`.
 * But in process logic is already cover to array.
 * Check array is not necessary. Let's simplify this check logic.
 */

function valueProp() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var props = args[0],
      propName = args[1],
      Component = args[2];

  if (Object(_util__WEBPACK_IMPORTED_MODULE_1__["isLabelInValue"])(props)) {
    var _err = genArrProps(prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.shape({
      label: prop_types__WEBPACK_IMPORTED_MODULE_0___default.a.node,
      value: internalValProp
    })).apply(void 0, args);

    if (_err) {
      return new Error("Invalid prop `".concat(propName, "` supplied to `").concat(Component, "`. ") + "You should use { label: string, value: string | number } or [{ label: string, value: string | number }] instead.");
    }

    return null;
  }

  var err = genArrProps(internalValProp).apply(void 0, args);

  if (err) {
    return new Error("Invalid prop `".concat(propName, "` supplied to `").concat(Component, "`. ") + "You should use string or [string] instead.");
  }

  return null;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvcHJvcFR5cGVzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvcHJvcFR5cGVzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBpc0xhYmVsSW5WYWx1ZSB9IGZyb20gJy4vdXRpbCc7XG52YXIgaW50ZXJuYWxWYWxQcm9wID0gUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pO1xuZXhwb3J0IGZ1bmN0aW9uIGdlbkFyclByb3BzKHByb3BUeXBlKSB7XG4gIHJldHVybiBQcm9wVHlwZXMub25lT2ZUeXBlKFtwcm9wVHlwZSwgUHJvcFR5cGVzLmFycmF5T2YocHJvcFR5cGUpXSk7XG59XG4vKipcbiAqIE9yaWdpbiBjb2RlIGNoZWNrIGBtdWx0aXBsZWAgaXMgdHJ1ZSB3aGVuIGB0cmVlQ2hlY2tTdHJpY3RseWAgJiBgbGFiZWxJblZhbHVlYC5cbiAqIEJ1dCBpbiBwcm9jZXNzIGxvZ2ljIGlzIGFscmVhZHkgY292ZXIgdG8gYXJyYXkuXG4gKiBDaGVjayBhcnJheSBpcyBub3QgbmVjZXNzYXJ5LiBMZXQncyBzaW1wbGlmeSB0aGlzIGNoZWNrIGxvZ2ljLlxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiB2YWx1ZVByb3AoKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gIH1cblxuICB2YXIgcHJvcHMgPSBhcmdzWzBdLFxuICAgICAgcHJvcE5hbWUgPSBhcmdzWzFdLFxuICAgICAgQ29tcG9uZW50ID0gYXJnc1syXTtcblxuICBpZiAoaXNMYWJlbEluVmFsdWUocHJvcHMpKSB7XG4gICAgdmFyIF9lcnIgPSBnZW5BcnJQcm9wcyhQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgbGFiZWw6IFByb3BUeXBlcy5ub2RlLFxuICAgICAgdmFsdWU6IGludGVybmFsVmFsUHJvcFxuICAgIH0pKS5hcHBseSh2b2lkIDAsIGFyZ3MpO1xuXG4gICAgaWYgKF9lcnIpIHtcbiAgICAgIHJldHVybiBuZXcgRXJyb3IoXCJJbnZhbGlkIHByb3AgYFwiLmNvbmNhdChwcm9wTmFtZSwgXCJgIHN1cHBsaWVkIHRvIGBcIikuY29uY2F0KENvbXBvbmVudCwgXCJgLiBcIikgKyBcIllvdSBzaG91bGQgdXNlIHsgbGFiZWw6IHN0cmluZywgdmFsdWU6IHN0cmluZyB8IG51bWJlciB9IG9yIFt7IGxhYmVsOiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcgfCBudW1iZXIgfV0gaW5zdGVhZC5cIik7XG4gICAgfVxuXG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB2YXIgZXJyID0gZ2VuQXJyUHJvcHMoaW50ZXJuYWxWYWxQcm9wKS5hcHBseSh2b2lkIDAsIGFyZ3MpO1xuXG4gIGlmIChlcnIpIHtcbiAgICByZXR1cm4gbmV3IEVycm9yKFwiSW52YWxpZCBwcm9wIGBcIi5jb25jYXQocHJvcE5hbWUsIFwiYCBzdXBwbGllZCB0byBgXCIpLmNvbmNhdChDb21wb25lbnQsIFwiYC4gXCIpICsgXCJZb3Ugc2hvdWxkIHVzZSBzdHJpbmcgb3IgW3N0cmluZ10gaW5zdGVhZC5cIik7XG4gIH1cblxuICByZXR1cm4gbnVsbDtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/es/propTypes.js
