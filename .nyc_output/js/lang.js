/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _default = {
  toolbox: {
    brush: {
      title: {
        rect: '矩形选择',
        polygon: '圈选',
        lineX: '横向选择',
        lineY: '纵向选择',
        keep: '保持选择',
        clear: '清除选择'
      }
    },
    dataView: {
      title: '数据视图',
      lang: ['数据视图', '关闭', '刷新']
    },
    dataZoom: {
      title: {
        zoom: '区域缩放',
        back: '区域缩放还原'
      }
    },
    magicType: {
      title: {
        line: '切换为折线图',
        bar: '切换为柱状图',
        stack: '切换为堆叠',
        tiled: '切换为平铺'
      }
    },
    restore: {
      title: '还原'
    },
    saveAsImage: {
      title: '保存为图片',
      lang: ['右键另存为图片']
    }
  },
  series: {
    typeNames: {
      pie: '饼图',
      bar: '柱状图',
      line: '折线图',
      scatter: '散点图',
      effectScatter: '涟漪散点图',
      radar: '雷达图',
      tree: '树图',
      treemap: '矩形树图',
      boxplot: '箱型图',
      candlestick: 'K线图',
      k: 'K线图',
      heatmap: '热力图',
      map: '地图',
      parallel: '平行坐标图',
      lines: '线图',
      graph: '关系图',
      sankey: '桑基图',
      funnel: '漏斗图',
      gauge: '仪表盘图',
      pictorialBar: '象形柱图',
      themeRiver: '主题河流图',
      sunburst: '旭日图'
    }
  },
  aria: {
    general: {
      withTitle: '这是一个关于“{title}”的图表。',
      withoutTitle: '这是一个图表，'
    },
    series: {
      single: {
        prefix: '',
        withName: '图表类型是{seriesType}，表示{seriesName}。',
        withoutName: '图表类型是{seriesType}。'
      },
      multiple: {
        prefix: '它由{seriesCount}个图表系列组成。',
        withName: '第{seriesId}个系列是一个表示{seriesName}的{seriesType}，',
        withoutName: '第{seriesId}个系列是一个{seriesType}，',
        separator: {
          middle: '；',
          end: '。'
        }
      }
    },
    data: {
      allData: '其数据是——',
      partialData: '其中，前{displayCnt}项是——',
      withName: '{name}的数据是{value}',
      withoutName: '{value}',
      separator: {
        middle: '，',
        end: ''
      }
    }
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbGFuZy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2xhbmcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBfZGVmYXVsdCA9IHtcbiAgdG9vbGJveDoge1xuICAgIGJydXNoOiB7XG4gICAgICB0aXRsZToge1xuICAgICAgICByZWN0OiAn55+p5b2i6YCJ5oupJyxcbiAgICAgICAgcG9seWdvbjogJ+WciOmAiScsXG4gICAgICAgIGxpbmVYOiAn5qiq5ZCR6YCJ5oupJyxcbiAgICAgICAgbGluZVk6ICfnurXlkJHpgInmi6knLFxuICAgICAgICBrZWVwOiAn5L+d5oyB6YCJ5oupJyxcbiAgICAgICAgY2xlYXI6ICfmuIXpmaTpgInmi6knXG4gICAgICB9XG4gICAgfSxcbiAgICBkYXRhVmlldzoge1xuICAgICAgdGl0bGU6ICfmlbDmja7op4blm74nLFxuICAgICAgbGFuZzogWyfmlbDmja7op4blm74nLCAn5YWz6ZetJywgJ+WIt+aWsCddXG4gICAgfSxcbiAgICBkYXRhWm9vbToge1xuICAgICAgdGl0bGU6IHtcbiAgICAgICAgem9vbTogJ+WMuuWfn+e8qeaUvicsXG4gICAgICAgIGJhY2s6ICfljLrln5/nvKnmlL7ov5jljp8nXG4gICAgICB9XG4gICAgfSxcbiAgICBtYWdpY1R5cGU6IHtcbiAgICAgIHRpdGxlOiB7XG4gICAgICAgIGxpbmU6ICfliIfmjaLkuLrmipjnur/lm74nLFxuICAgICAgICBiYXI6ICfliIfmjaLkuLrmn7Hnirblm74nLFxuICAgICAgICBzdGFjazogJ+WIh+aNouS4uuWghuWPoCcsXG4gICAgICAgIHRpbGVkOiAn5YiH5o2i5Li65bmz6ZO6J1xuICAgICAgfVxuICAgIH0sXG4gICAgcmVzdG9yZToge1xuICAgICAgdGl0bGU6ICfov5jljp8nXG4gICAgfSxcbiAgICBzYXZlQXNJbWFnZToge1xuICAgICAgdGl0bGU6ICfkv53lrZjkuLrlm77niYcnLFxuICAgICAgbGFuZzogWyflj7PplK7lj6blrZjkuLrlm77niYcnXVxuICAgIH1cbiAgfSxcbiAgc2VyaWVzOiB7XG4gICAgdHlwZU5hbWVzOiB7XG4gICAgICBwaWU6ICfppbzlm74nLFxuICAgICAgYmFyOiAn5p+x54q25Zu+JyxcbiAgICAgIGxpbmU6ICfmipjnur/lm74nLFxuICAgICAgc2NhdHRlcjogJ+aVo+eCueWbvicsXG4gICAgICBlZmZlY3RTY2F0dGVyOiAn5raf5ryq5pWj54K55Zu+JyxcbiAgICAgIHJhZGFyOiAn6Zu36L6+5Zu+JyxcbiAgICAgIHRyZWU6ICfmoJHlm74nLFxuICAgICAgdHJlZW1hcDogJ+efqeW9ouagkeWbvicsXG4gICAgICBib3hwbG90OiAn566x5Z6L5Zu+JyxcbiAgICAgIGNhbmRsZXN0aWNrOiAnS+e6v+WbvicsXG4gICAgICBrOiAnS+e6v+WbvicsXG4gICAgICBoZWF0bWFwOiAn54Ot5Yqb5Zu+JyxcbiAgICAgIG1hcDogJ+WcsOWbvicsXG4gICAgICBwYXJhbGxlbDogJ+W5s+ihjOWdkOagh+WbvicsXG4gICAgICBsaW5lczogJ+e6v+WbvicsXG4gICAgICBncmFwaDogJ+WFs+ezu+WbvicsXG4gICAgICBzYW5rZXk6ICfmoZHln7rlm74nLFxuICAgICAgZnVubmVsOiAn5ryP5paX5Zu+JyxcbiAgICAgIGdhdWdlOiAn5Luq6KGo55uY5Zu+JyxcbiAgICAgIHBpY3RvcmlhbEJhcjogJ+ixoeW9ouafseWbvicsXG4gICAgICB0aGVtZVJpdmVyOiAn5Li76aKY5rKz5rWB5Zu+JyxcbiAgICAgIHN1bmJ1cnN0OiAn5pet5pel5Zu+J1xuICAgIH1cbiAgfSxcbiAgYXJpYToge1xuICAgIGdlbmVyYWw6IHtcbiAgICAgIHdpdGhUaXRsZTogJ+i/meaYr+S4gOS4quWFs+S6juKAnHt0aXRsZX3igJ3nmoTlm77ooajjgIInLFxuICAgICAgd2l0aG91dFRpdGxlOiAn6L+Z5piv5LiA5Liq5Zu+6KGo77yMJ1xuICAgIH0sXG4gICAgc2VyaWVzOiB7XG4gICAgICBzaW5nbGU6IHtcbiAgICAgICAgcHJlZml4OiAnJyxcbiAgICAgICAgd2l0aE5hbWU6ICflm77ooajnsbvlnovmmK97c2VyaWVzVHlwZX3vvIzooajnpLp7c2VyaWVzTmFtZX3jgIInLFxuICAgICAgICB3aXRob3V0TmFtZTogJ+WbvuihqOexu+Wei+aYr3tzZXJpZXNUeXBlfeOAgidcbiAgICAgIH0sXG4gICAgICBtdWx0aXBsZToge1xuICAgICAgICBwcmVmaXg6ICflroPnlLF7c2VyaWVzQ291bnR95Liq5Zu+6KGo57O75YiX57uE5oiQ44CCJyxcbiAgICAgICAgd2l0aE5hbWU6ICfnrKx7c2VyaWVzSWR95Liq57O75YiX5piv5LiA5Liq6KGo56S6e3Nlcmllc05hbWV955qEe3Nlcmllc1R5cGV977yMJyxcbiAgICAgICAgd2l0aG91dE5hbWU6ICfnrKx7c2VyaWVzSWR95Liq57O75YiX5piv5LiA5Liqe3Nlcmllc1R5cGV977yMJyxcbiAgICAgICAgc2VwYXJhdG9yOiB7XG4gICAgICAgICAgbWlkZGxlOiAn77ybJyxcbiAgICAgICAgICBlbmQ6ICfjgIInXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuICAgIGRhdGE6IHtcbiAgICAgIGFsbERhdGE6ICflhbbmlbDmja7mmK/igJTigJQnLFxuICAgICAgcGFydGlhbERhdGE6ICflhbbkuK3vvIzliY17ZGlzcGxheUNudH3pobnmmK/igJTigJQnLFxuICAgICAgd2l0aE5hbWU6ICd7bmFtZX3nmoTmlbDmja7mmK97dmFsdWV9JyxcbiAgICAgIHdpdGhvdXROYW1lOiAne3ZhbHVlfScsXG4gICAgICBzZXBhcmF0b3I6IHtcbiAgICAgICAgbWlkZGxlOiAn77yMJyxcbiAgICAgICAgZW5kOiAnJ1xuICAgICAgfVxuICAgIH1cbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFEQTtBQVVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQVFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBaENBO0FBcUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRCQTtBQURBO0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSkE7QUFOQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFMQTtBQXJCQTtBQWhFQTtBQWlHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/lang.js
