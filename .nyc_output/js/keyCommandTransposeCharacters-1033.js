/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandTransposeCharacters
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var getContentStateFragment = __webpack_require__(/*! ./getContentStateFragment */ "./node_modules/draft-js/lib/getContentStateFragment.js");
/**
 * Transpose the characters on either side of a collapsed cursor, or
 * if the cursor is at the end of the block, transpose the last two
 * characters.
 */


function keyCommandTransposeCharacters(editorState) {
  var selection = editorState.getSelection();

  if (!selection.isCollapsed()) {
    return editorState;
  }

  var offset = selection.getAnchorOffset();

  if (offset === 0) {
    return editorState;
  }

  var blockKey = selection.getAnchorKey();
  var content = editorState.getCurrentContent();
  var block = content.getBlockForKey(blockKey);
  var length = block.getLength(); // Nothing to transpose if there aren't two characters.

  if (length <= 1) {
    return editorState;
  }

  var removalRange;
  var finalSelection;

  if (offset === length) {
    // The cursor is at the end of the block. Swap the last two characters.
    removalRange = selection.set('anchorOffset', offset - 1);
    finalSelection = selection;
  } else {
    removalRange = selection.set('focusOffset', offset + 1);
    finalSelection = removalRange.set('anchorOffset', offset + 1);
  } // Extract the character to move as a fragment. This preserves its
  // styling and entity, if any.


  var movedFragment = getContentStateFragment(content, removalRange);
  var afterRemoval = DraftModifier.removeRange(content, removalRange, 'backward'); // After the removal, the insertion target is one character back.

  var selectionAfter = afterRemoval.getSelectionAfter();
  var targetOffset = selectionAfter.getAnchorOffset() - 1;
  var targetRange = selectionAfter.merge({
    anchorOffset: targetOffset,
    focusOffset: targetOffset
  });
  var afterInsert = DraftModifier.replaceWithFragment(afterRemoval, targetRange, movedFragment);
  var newEditorState = EditorState.push(editorState, afterInsert, 'insert-fragment');
  return EditorState.acceptSelection(newEditorState, finalSelection);
}

module.exports = keyCommandTransposeCharacters;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRUcmFuc3Bvc2VDaGFyYWN0ZXJzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRUcmFuc3Bvc2VDaGFyYWN0ZXJzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZFRyYW5zcG9zZUNoYXJhY3RlcnNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcblxudmFyIGdldENvbnRlbnRTdGF0ZUZyYWdtZW50ID0gcmVxdWlyZSgnLi9nZXRDb250ZW50U3RhdGVGcmFnbWVudCcpO1xuXG4vKipcbiAqIFRyYW5zcG9zZSB0aGUgY2hhcmFjdGVycyBvbiBlaXRoZXIgc2lkZSBvZiBhIGNvbGxhcHNlZCBjdXJzb3IsIG9yXG4gKiBpZiB0aGUgY3Vyc29yIGlzIGF0IHRoZSBlbmQgb2YgdGhlIGJsb2NrLCB0cmFuc3Bvc2UgdGhlIGxhc3QgdHdvXG4gKiBjaGFyYWN0ZXJzLlxuICovXG5mdW5jdGlvbiBrZXlDb21tYW5kVHJhbnNwb3NlQ2hhcmFjdGVycyhlZGl0b3JTdGF0ZSkge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIGlmICghc2VsZWN0aW9uLmlzQ29sbGFwc2VkKCkpIHtcbiAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gIH1cblxuICB2YXIgb2Zmc2V0ID0gc2VsZWN0aW9uLmdldEFuY2hvck9mZnNldCgpO1xuICBpZiAob2Zmc2V0ID09PSAwKSB7XG4gICAgcmV0dXJuIGVkaXRvclN0YXRlO1xuICB9XG5cbiAgdmFyIGJsb2NrS2V5ID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gIHZhciBibG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tGb3JLZXkoYmxvY2tLZXkpO1xuICB2YXIgbGVuZ3RoID0gYmxvY2suZ2V0TGVuZ3RoKCk7XG5cbiAgLy8gTm90aGluZyB0byB0cmFuc3Bvc2UgaWYgdGhlcmUgYXJlbid0IHR3byBjaGFyYWN0ZXJzLlxuICBpZiAobGVuZ3RoIDw9IDEpIHtcbiAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gIH1cblxuICB2YXIgcmVtb3ZhbFJhbmdlO1xuICB2YXIgZmluYWxTZWxlY3Rpb247XG5cbiAgaWYgKG9mZnNldCA9PT0gbGVuZ3RoKSB7XG4gICAgLy8gVGhlIGN1cnNvciBpcyBhdCB0aGUgZW5kIG9mIHRoZSBibG9jay4gU3dhcCB0aGUgbGFzdCB0d28gY2hhcmFjdGVycy5cbiAgICByZW1vdmFsUmFuZ2UgPSBzZWxlY3Rpb24uc2V0KCdhbmNob3JPZmZzZXQnLCBvZmZzZXQgLSAxKTtcbiAgICBmaW5hbFNlbGVjdGlvbiA9IHNlbGVjdGlvbjtcbiAgfSBlbHNlIHtcbiAgICByZW1vdmFsUmFuZ2UgPSBzZWxlY3Rpb24uc2V0KCdmb2N1c09mZnNldCcsIG9mZnNldCArIDEpO1xuICAgIGZpbmFsU2VsZWN0aW9uID0gcmVtb3ZhbFJhbmdlLnNldCgnYW5jaG9yT2Zmc2V0Jywgb2Zmc2V0ICsgMSk7XG4gIH1cblxuICAvLyBFeHRyYWN0IHRoZSBjaGFyYWN0ZXIgdG8gbW92ZSBhcyBhIGZyYWdtZW50LiBUaGlzIHByZXNlcnZlcyBpdHNcbiAgLy8gc3R5bGluZyBhbmQgZW50aXR5LCBpZiBhbnkuXG4gIHZhciBtb3ZlZEZyYWdtZW50ID0gZ2V0Q29udGVudFN0YXRlRnJhZ21lbnQoY29udGVudCwgcmVtb3ZhbFJhbmdlKTtcbiAgdmFyIGFmdGVyUmVtb3ZhbCA9IERyYWZ0TW9kaWZpZXIucmVtb3ZlUmFuZ2UoY29udGVudCwgcmVtb3ZhbFJhbmdlLCAnYmFja3dhcmQnKTtcblxuICAvLyBBZnRlciB0aGUgcmVtb3ZhbCwgdGhlIGluc2VydGlvbiB0YXJnZXQgaXMgb25lIGNoYXJhY3RlciBiYWNrLlxuICB2YXIgc2VsZWN0aW9uQWZ0ZXIgPSBhZnRlclJlbW92YWwuZ2V0U2VsZWN0aW9uQWZ0ZXIoKTtcbiAgdmFyIHRhcmdldE9mZnNldCA9IHNlbGVjdGlvbkFmdGVyLmdldEFuY2hvck9mZnNldCgpIC0gMTtcbiAgdmFyIHRhcmdldFJhbmdlID0gc2VsZWN0aW9uQWZ0ZXIubWVyZ2Uoe1xuICAgIGFuY2hvck9mZnNldDogdGFyZ2V0T2Zmc2V0LFxuICAgIGZvY3VzT2Zmc2V0OiB0YXJnZXRPZmZzZXRcbiAgfSk7XG5cbiAgdmFyIGFmdGVySW5zZXJ0ID0gRHJhZnRNb2RpZmllci5yZXBsYWNlV2l0aEZyYWdtZW50KGFmdGVyUmVtb3ZhbCwgdGFyZ2V0UmFuZ2UsIG1vdmVkRnJhZ21lbnQpO1xuXG4gIHZhciBuZXdFZGl0b3JTdGF0ZSA9IEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGFmdGVySW5zZXJ0LCAnaW5zZXJ0LWZyYWdtZW50Jyk7XG5cbiAgcmV0dXJuIEVkaXRvclN0YXRlLmFjY2VwdFNlbGVjdGlvbihuZXdFZGl0b3JTdGF0ZSwgZmluYWxTZWxlY3Rpb24pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGtleUNvbW1hbmRUcmFuc3Bvc2VDaGFyYWN0ZXJzOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandTransposeCharacters.js
