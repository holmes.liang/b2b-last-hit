__webpack_require__.r(__webpack_exports__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _message_count_state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./message-count-state */ "./src/app/desk/component/page/message-count-state.tsx");
var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/page/badge-bell.tsx";




var Bell = function Bell(props) {
  var countChange = _message_count_state__WEBPACK_IMPORTED_MODULE_2__["default"].useContainer();
  _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].useEffect(function () {
    countChange.getCount();
    var timer = setInterval(function () {
      countChange.getCount();
    }, 30000);
    return function cleanUp() {
      clearInterval(timer);
    };
  }, []);
  return _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_0__["Badge"], {
    count: countChange.count,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_0__["Icon"], {
    type: "bell",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Bell);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvYmFkZ2UtYmVsbC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9iYWRnZS1iZWxsLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCYWRnZSwgSWNvbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IENvdW50Q29udGFpbmVyIGZyb20gXCIuL21lc3NhZ2UtY291bnQtc3RhdGVcIjtcblxuY29uc3QgQmVsbCA9IChwcm9wczogYW55KSA9PiB7XG4gIGxldCBjb3VudENoYW5nZSA9IENvdW50Q29udGFpbmVyLnVzZUNvbnRhaW5lcigpO1xuXG4gIFJlYWN0LnVzZUVmZmVjdCgoKSA9PiB7XG4gICAgY291bnRDaGFuZ2UuZ2V0Q291bnQoKTtcbiAgICBjb25zdCB0aW1lciA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgIGNvdW50Q2hhbmdlLmdldENvdW50KCk7XG4gICAgfSwgMzAwMDApO1xuICAgIHJldHVybiBmdW5jdGlvbiBjbGVhblVwKCkge1xuICAgICAgY2xlYXJJbnRlcnZhbCh0aW1lcik7XG4gICAgfTtcbiAgfSwgW10pO1xuXG4gIHJldHVybiAoXG4gICAgPEJhZGdlIGNvdW50PXtjb3VudENoYW5nZS5jb3VudH0+XG4gICAgICA8SWNvbiB0eXBlPVwiYmVsbFwiLz5cbiAgICA8L0JhZGdlPlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQmVsbDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/page/badge-bell.tsx
