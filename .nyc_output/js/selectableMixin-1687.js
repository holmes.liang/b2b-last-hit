/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Data selectable mixin for chart series.
 * To eanble data select, option of series must have `selectedMode`.
 * And each data item will use `selected` to toggle itself selected status
 */


var _default = {
  /**
   * @param {Array.<Object>} targetList [{name, value, selected}, ...]
   *        If targetList is an array, it should like [{name: ..., value: ...}, ...].
   *        If targetList is a "List", it must have coordDim: 'value' dimension and name.
   */
  updateSelectedMap: function updateSelectedMap(targetList) {
    this._targetList = zrUtil.isArray(targetList) ? targetList.slice() : [];
    this._selectTargetMap = zrUtil.reduce(targetList || [], function (targetMap, target) {
      targetMap.set(target.name, target);
      return targetMap;
    }, zrUtil.createHashMap());
  },

  /**
   * Either name or id should be passed as input here.
   * If both of them are defined, id is used.
   *
   * @param {string|undefined} name name of data
   * @param {number|undefined} id dataIndex of data
   */
  // PENGING If selectedMode is null ?
  select: function select(name, id) {
    var target = id != null ? this._targetList[id] : this._selectTargetMap.get(name);
    var selectedMode = this.get('selectedMode');

    if (selectedMode === 'single') {
      this._selectTargetMap.each(function (target) {
        target.selected = false;
      });
    }

    target && (target.selected = true);
  },

  /**
   * Either name or id should be passed as input here.
   * If both of them are defined, id is used.
   *
   * @param {string|undefined} name name of data
   * @param {number|undefined} id dataIndex of data
   */
  unSelect: function unSelect(name, id) {
    var target = id != null ? this._targetList[id] : this._selectTargetMap.get(name); // var selectedMode = this.get('selectedMode');
    // selectedMode !== 'single' && target && (target.selected = false);

    target && (target.selected = false);
  },

  /**
   * Either name or id should be passed as input here.
   * If both of them are defined, id is used.
   *
   * @param {string|undefined} name name of data
   * @param {number|undefined} id dataIndex of data
   */
  toggleSelected: function toggleSelected(name, id) {
    var target = id != null ? this._targetList[id] : this._selectTargetMap.get(name);

    if (target != null) {
      this[target.selected ? 'unSelect' : 'select'](name, id);
      return target.selected;
    }
  },

  /**
   * Either name or id should be passed as input here.
   * If both of them are defined, id is used.
   *
   * @param {string|undefined} name name of data
   * @param {number|undefined} id dataIndex of data
   */
  isSelected: function isSelected(name, id) {
    var target = id != null ? this._targetList[id] : this._selectTargetMap.get(name);
    return target && target.selected;
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2hlbHBlci9zZWxlY3RhYmxlTWl4aW4uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvaGVscGVyL3NlbGVjdGFibGVNaXhpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBEYXRhIHNlbGVjdGFibGUgbWl4aW4gZm9yIGNoYXJ0IHNlcmllcy5cbiAqIFRvIGVhbmJsZSBkYXRhIHNlbGVjdCwgb3B0aW9uIG9mIHNlcmllcyBtdXN0IGhhdmUgYHNlbGVjdGVkTW9kZWAuXG4gKiBBbmQgZWFjaCBkYXRhIGl0ZW0gd2lsbCB1c2UgYHNlbGVjdGVkYCB0byB0b2dnbGUgaXRzZWxmIHNlbGVjdGVkIHN0YXR1c1xuICovXG52YXIgX2RlZmF1bHQgPSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge0FycmF5LjxPYmplY3Q+fSB0YXJnZXRMaXN0IFt7bmFtZSwgdmFsdWUsIHNlbGVjdGVkfSwgLi4uXVxuICAgKiAgICAgICAgSWYgdGFyZ2V0TGlzdCBpcyBhbiBhcnJheSwgaXQgc2hvdWxkIGxpa2UgW3tuYW1lOiAuLi4sIHZhbHVlOiAuLi59LCAuLi5dLlxuICAgKiAgICAgICAgSWYgdGFyZ2V0TGlzdCBpcyBhIFwiTGlzdFwiLCBpdCBtdXN0IGhhdmUgY29vcmREaW06ICd2YWx1ZScgZGltZW5zaW9uIGFuZCBuYW1lLlxuICAgKi9cbiAgdXBkYXRlU2VsZWN0ZWRNYXA6IGZ1bmN0aW9uICh0YXJnZXRMaXN0KSB7XG4gICAgdGhpcy5fdGFyZ2V0TGlzdCA9IHpyVXRpbC5pc0FycmF5KHRhcmdldExpc3QpID8gdGFyZ2V0TGlzdC5zbGljZSgpIDogW107XG4gICAgdGhpcy5fc2VsZWN0VGFyZ2V0TWFwID0genJVdGlsLnJlZHVjZSh0YXJnZXRMaXN0IHx8IFtdLCBmdW5jdGlvbiAodGFyZ2V0TWFwLCB0YXJnZXQpIHtcbiAgICAgIHRhcmdldE1hcC5zZXQodGFyZ2V0Lm5hbWUsIHRhcmdldCk7XG4gICAgICByZXR1cm4gdGFyZ2V0TWFwO1xuICAgIH0sIHpyVXRpbC5jcmVhdGVIYXNoTWFwKCkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBFaXRoZXIgbmFtZSBvciBpZCBzaG91bGQgYmUgcGFzc2VkIGFzIGlucHV0IGhlcmUuXG4gICAqIElmIGJvdGggb2YgdGhlbSBhcmUgZGVmaW5lZCwgaWQgaXMgdXNlZC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd8dW5kZWZpbmVkfSBuYW1lIG5hbWUgb2YgZGF0YVxuICAgKiBAcGFyYW0ge251bWJlcnx1bmRlZmluZWR9IGlkIGRhdGFJbmRleCBvZiBkYXRhXG4gICAqL1xuICAvLyBQRU5HSU5HIElmIHNlbGVjdGVkTW9kZSBpcyBudWxsID9cbiAgc2VsZWN0OiBmdW5jdGlvbiAobmFtZSwgaWQpIHtcbiAgICB2YXIgdGFyZ2V0ID0gaWQgIT0gbnVsbCA/IHRoaXMuX3RhcmdldExpc3RbaWRdIDogdGhpcy5fc2VsZWN0VGFyZ2V0TWFwLmdldChuYW1lKTtcbiAgICB2YXIgc2VsZWN0ZWRNb2RlID0gdGhpcy5nZXQoJ3NlbGVjdGVkTW9kZScpO1xuXG4gICAgaWYgKHNlbGVjdGVkTW9kZSA9PT0gJ3NpbmdsZScpIHtcbiAgICAgIHRoaXMuX3NlbGVjdFRhcmdldE1hcC5lYWNoKGZ1bmN0aW9uICh0YXJnZXQpIHtcbiAgICAgICAgdGFyZ2V0LnNlbGVjdGVkID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0YXJnZXQgJiYgKHRhcmdldC5zZWxlY3RlZCA9IHRydWUpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBFaXRoZXIgbmFtZSBvciBpZCBzaG91bGQgYmUgcGFzc2VkIGFzIGlucHV0IGhlcmUuXG4gICAqIElmIGJvdGggb2YgdGhlbSBhcmUgZGVmaW5lZCwgaWQgaXMgdXNlZC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd8dW5kZWZpbmVkfSBuYW1lIG5hbWUgb2YgZGF0YVxuICAgKiBAcGFyYW0ge251bWJlcnx1bmRlZmluZWR9IGlkIGRhdGFJbmRleCBvZiBkYXRhXG4gICAqL1xuICB1blNlbGVjdDogZnVuY3Rpb24gKG5hbWUsIGlkKSB7XG4gICAgdmFyIHRhcmdldCA9IGlkICE9IG51bGwgPyB0aGlzLl90YXJnZXRMaXN0W2lkXSA6IHRoaXMuX3NlbGVjdFRhcmdldE1hcC5nZXQobmFtZSk7IC8vIHZhciBzZWxlY3RlZE1vZGUgPSB0aGlzLmdldCgnc2VsZWN0ZWRNb2RlJyk7XG4gICAgLy8gc2VsZWN0ZWRNb2RlICE9PSAnc2luZ2xlJyAmJiB0YXJnZXQgJiYgKHRhcmdldC5zZWxlY3RlZCA9IGZhbHNlKTtcblxuICAgIHRhcmdldCAmJiAodGFyZ2V0LnNlbGVjdGVkID0gZmFsc2UpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBFaXRoZXIgbmFtZSBvciBpZCBzaG91bGQgYmUgcGFzc2VkIGFzIGlucHV0IGhlcmUuXG4gICAqIElmIGJvdGggb2YgdGhlbSBhcmUgZGVmaW5lZCwgaWQgaXMgdXNlZC5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd8dW5kZWZpbmVkfSBuYW1lIG5hbWUgb2YgZGF0YVxuICAgKiBAcGFyYW0ge251bWJlcnx1bmRlZmluZWR9IGlkIGRhdGFJbmRleCBvZiBkYXRhXG4gICAqL1xuICB0b2dnbGVTZWxlY3RlZDogZnVuY3Rpb24gKG5hbWUsIGlkKSB7XG4gICAgdmFyIHRhcmdldCA9IGlkICE9IG51bGwgPyB0aGlzLl90YXJnZXRMaXN0W2lkXSA6IHRoaXMuX3NlbGVjdFRhcmdldE1hcC5nZXQobmFtZSk7XG5cbiAgICBpZiAodGFyZ2V0ICE9IG51bGwpIHtcbiAgICAgIHRoaXNbdGFyZ2V0LnNlbGVjdGVkID8gJ3VuU2VsZWN0JyA6ICdzZWxlY3QnXShuYW1lLCBpZCk7XG4gICAgICByZXR1cm4gdGFyZ2V0LnNlbGVjdGVkO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogRWl0aGVyIG5hbWUgb3IgaWQgc2hvdWxkIGJlIHBhc3NlZCBhcyBpbnB1dCBoZXJlLlxuICAgKiBJZiBib3RoIG9mIHRoZW0gYXJlIGRlZmluZWQsIGlkIGlzIHVzZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfHVuZGVmaW5lZH0gbmFtZSBuYW1lIG9mIGRhdGFcbiAgICogQHBhcmFtIHtudW1iZXJ8dW5kZWZpbmVkfSBpZCBkYXRhSW5kZXggb2YgZGF0YVxuICAgKi9cbiAgaXNTZWxlY3RlZDogZnVuY3Rpb24gKG5hbWUsIGlkKSB7XG4gICAgdmFyIHRhcmdldCA9IGlkICE9IG51bGwgPyB0aGlzLl90YXJnZXRMaXN0W2lkXSA6IHRoaXMuX3NlbGVjdFRhcmdldE1hcC5nZXQobmFtZSk7XG4gICAgcmV0dXJuIHRhcmdldCAmJiB0YXJnZXQuc2VsZWN0ZWQ7XG4gIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQTNFQTtBQTZFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/helper/selectableMixin.js
