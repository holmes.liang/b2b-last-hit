/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var isString = _util.isString;
var isArray = _util.isArray;
var each = _util.each;
var assert = _util.assert;

var _parseSVG = __webpack_require__(/*! zrender/lib/tool/parseSVG */ "./node_modules/zrender/lib/tool/parseSVG.js");

var parseXML = _parseSVG.parseXML;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var storage = createHashMap(); // For minimize the code size of common echarts package,
// do not put too much logic in this module.

var _default = {
  // The format of record: see `echarts.registerMap`.
  // Compatible with previous `echarts.registerMap`.
  registerMap: function registerMap(mapName, rawGeoJson, rawSpecialAreas) {
    var records;

    if (isArray(rawGeoJson)) {
      records = rawGeoJson;
    } else if (rawGeoJson.svg) {
      records = [{
        type: 'svg',
        source: rawGeoJson.svg,
        specialAreas: rawGeoJson.specialAreas
      }];
    } else {
      // Backward compatibility.
      if (rawGeoJson.geoJson && !rawGeoJson.features) {
        rawSpecialAreas = rawGeoJson.specialAreas;
        rawGeoJson = rawGeoJson.geoJson;
      }

      records = [{
        type: 'geoJSON',
        source: rawGeoJson,
        specialAreas: rawSpecialAreas
      }];
    }

    each(records, function (record) {
      var type = record.type;
      type === 'geoJson' && (type = record.type = 'geoJSON');
      var parse = parsers[type];
      parse(record);
    });
    return storage.set(mapName, records);
  },
  retrieveMap: function retrieveMap(mapName) {
    return storage.get(mapName);
  }
};
var parsers = {
  geoJSON: function geoJSON(record) {
    var source = record.source;
    record.geoJSON = !isString(source) ? source : typeof JSON !== 'undefined' && JSON.parse ? JSON.parse(source) : new Function('return (' + source + ');')();
  },
  // Only perform parse to XML object here, which might be time
  // consiming for large SVG.
  // Although convert XML to zrender element is also time consiming,
  // if we do it here, the clone of zrender elements has to be
  // required. So we do it once for each geo instance, util real
  // performance issues call for optimizing it.
  svg: function svg(record) {
    record.svgXML = parseXML(record.source);
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvZ2VvL21hcERhdGFTdG9yYWdlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvZ2VvL21hcERhdGFTdG9yYWdlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgY3JlYXRlSGFzaE1hcCA9IF91dGlsLmNyZWF0ZUhhc2hNYXA7XG52YXIgaXNTdHJpbmcgPSBfdXRpbC5pc1N0cmluZztcbnZhciBpc0FycmF5ID0gX3V0aWwuaXNBcnJheTtcbnZhciBlYWNoID0gX3V0aWwuZWFjaDtcbnZhciBhc3NlcnQgPSBfdXRpbC5hc3NlcnQ7XG5cbnZhciBfcGFyc2VTVkcgPSByZXF1aXJlKFwienJlbmRlci9saWIvdG9vbC9wYXJzZVNWR1wiKTtcblxudmFyIHBhcnNlWE1MID0gX3BhcnNlU1ZHLnBhcnNlWE1MO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgc3RvcmFnZSA9IGNyZWF0ZUhhc2hNYXAoKTsgLy8gRm9yIG1pbmltaXplIHRoZSBjb2RlIHNpemUgb2YgY29tbW9uIGVjaGFydHMgcGFja2FnZSxcbi8vIGRvIG5vdCBwdXQgdG9vIG11Y2ggbG9naWMgaW4gdGhpcyBtb2R1bGUuXG5cbnZhciBfZGVmYXVsdCA9IHtcbiAgLy8gVGhlIGZvcm1hdCBvZiByZWNvcmQ6IHNlZSBgZWNoYXJ0cy5yZWdpc3Rlck1hcGAuXG4gIC8vIENvbXBhdGlibGUgd2l0aCBwcmV2aW91cyBgZWNoYXJ0cy5yZWdpc3Rlck1hcGAuXG4gIHJlZ2lzdGVyTWFwOiBmdW5jdGlvbiAobWFwTmFtZSwgcmF3R2VvSnNvbiwgcmF3U3BlY2lhbEFyZWFzKSB7XG4gICAgdmFyIHJlY29yZHM7XG5cbiAgICBpZiAoaXNBcnJheShyYXdHZW9Kc29uKSkge1xuICAgICAgcmVjb3JkcyA9IHJhd0dlb0pzb247XG4gICAgfSBlbHNlIGlmIChyYXdHZW9Kc29uLnN2Zykge1xuICAgICAgcmVjb3JkcyA9IFt7XG4gICAgICAgIHR5cGU6ICdzdmcnLFxuICAgICAgICBzb3VyY2U6IHJhd0dlb0pzb24uc3ZnLFxuICAgICAgICBzcGVjaWFsQXJlYXM6IHJhd0dlb0pzb24uc3BlY2lhbEFyZWFzXG4gICAgICB9XTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gQmFja3dhcmQgY29tcGF0aWJpbGl0eS5cbiAgICAgIGlmIChyYXdHZW9Kc29uLmdlb0pzb24gJiYgIXJhd0dlb0pzb24uZmVhdHVyZXMpIHtcbiAgICAgICAgcmF3U3BlY2lhbEFyZWFzID0gcmF3R2VvSnNvbi5zcGVjaWFsQXJlYXM7XG4gICAgICAgIHJhd0dlb0pzb24gPSByYXdHZW9Kc29uLmdlb0pzb247XG4gICAgICB9XG5cbiAgICAgIHJlY29yZHMgPSBbe1xuICAgICAgICB0eXBlOiAnZ2VvSlNPTicsXG4gICAgICAgIHNvdXJjZTogcmF3R2VvSnNvbixcbiAgICAgICAgc3BlY2lhbEFyZWFzOiByYXdTcGVjaWFsQXJlYXNcbiAgICAgIH1dO1xuICAgIH1cblxuICAgIGVhY2gocmVjb3JkcywgZnVuY3Rpb24gKHJlY29yZCkge1xuICAgICAgdmFyIHR5cGUgPSByZWNvcmQudHlwZTtcbiAgICAgIHR5cGUgPT09ICdnZW9Kc29uJyAmJiAodHlwZSA9IHJlY29yZC50eXBlID0gJ2dlb0pTT04nKTtcbiAgICAgIHZhciBwYXJzZSA9IHBhcnNlcnNbdHlwZV07XG4gICAgICBwYXJzZShyZWNvcmQpO1xuICAgIH0pO1xuICAgIHJldHVybiBzdG9yYWdlLnNldChtYXBOYW1lLCByZWNvcmRzKTtcbiAgfSxcbiAgcmV0cmlldmVNYXA6IGZ1bmN0aW9uIChtYXBOYW1lKSB7XG4gICAgcmV0dXJuIHN0b3JhZ2UuZ2V0KG1hcE5hbWUpO1xuICB9XG59O1xudmFyIHBhcnNlcnMgPSB7XG4gIGdlb0pTT046IGZ1bmN0aW9uIChyZWNvcmQpIHtcbiAgICB2YXIgc291cmNlID0gcmVjb3JkLnNvdXJjZTtcbiAgICByZWNvcmQuZ2VvSlNPTiA9ICFpc1N0cmluZyhzb3VyY2UpID8gc291cmNlIDogdHlwZW9mIEpTT04gIT09ICd1bmRlZmluZWQnICYmIEpTT04ucGFyc2UgPyBKU09OLnBhcnNlKHNvdXJjZSkgOiBuZXcgRnVuY3Rpb24oJ3JldHVybiAoJyArIHNvdXJjZSArICcpOycpKCk7XG4gIH0sXG4gIC8vIE9ubHkgcGVyZm9ybSBwYXJzZSB0byBYTUwgb2JqZWN0IGhlcmUsIHdoaWNoIG1pZ2h0IGJlIHRpbWVcbiAgLy8gY29uc2ltaW5nIGZvciBsYXJnZSBTVkcuXG4gIC8vIEFsdGhvdWdoIGNvbnZlcnQgWE1MIHRvIHpyZW5kZXIgZWxlbWVudCBpcyBhbHNvIHRpbWUgY29uc2ltaW5nLFxuICAvLyBpZiB3ZSBkbyBpdCBoZXJlLCB0aGUgY2xvbmUgb2YgenJlbmRlciBlbGVtZW50cyBoYXMgdG8gYmVcbiAgLy8gcmVxdWlyZWQuIFNvIHdlIGRvIGl0IG9uY2UgZm9yIGVhY2ggZ2VvIGluc3RhbmNlLCB1dGlsIHJlYWxcbiAgLy8gcGVyZm9ybWFuY2UgaXNzdWVzIGNhbGwgZm9yIG9wdGltaXppbmcgaXQuXG4gIHN2ZzogZnVuY3Rpb24gKHJlY29yZCkge1xuICAgIHJlY29yZC5zdmdYTUwgPSBwYXJzZVhNTChyZWNvcmQuc291cmNlKTtcbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRDQTtBQXdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFlQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/geo/mapDataStorage.js
