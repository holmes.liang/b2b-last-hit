/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */


var hasOwnProperty = Object.prototype.hasOwnProperty;
/**
 * Executes the provided `callback` once for each enumerable own property in the
 * object and constructs a new object from the results. The `callback` is
 * invoked with three arguments:
 *
 *  - the property value
 *  - the property name
 *  - the object being traversed
 *
 * Properties that are added after the call to `mapObject` will not be visited
 * by `callback`. If the values of existing properties are changed, the value
 * passed to `callback` will be the value at the time `mapObject` visits them.
 * Properties that are deleted before being visited are not visited.
 *
 * @grep function objectMap()
 * @grep function objMap()
 *
 * @param {?object} object
 * @param {function} callback
 * @param {*} context
 * @return {?object}
 */

function mapObject(object, callback, context) {
  if (!object) {
    return null;
  }

  var result = {};

  for (var name in object) {
    if (hasOwnProperty.call(object, name)) {
      result[name] = callback.call(context, object[name], name, object);
    }
  }

  return result;
}

module.exports = mapObject;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvbWFwT2JqZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvbWFwT2JqZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGhhc093blByb3BlcnR5ID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTtcblxuLyoqXG4gKiBFeGVjdXRlcyB0aGUgcHJvdmlkZWQgYGNhbGxiYWNrYCBvbmNlIGZvciBlYWNoIGVudW1lcmFibGUgb3duIHByb3BlcnR5IGluIHRoZVxuICogb2JqZWN0IGFuZCBjb25zdHJ1Y3RzIGEgbmV3IG9iamVjdCBmcm9tIHRoZSByZXN1bHRzLiBUaGUgYGNhbGxiYWNrYCBpc1xuICogaW52b2tlZCB3aXRoIHRocmVlIGFyZ3VtZW50czpcbiAqXG4gKiAgLSB0aGUgcHJvcGVydHkgdmFsdWVcbiAqICAtIHRoZSBwcm9wZXJ0eSBuYW1lXG4gKiAgLSB0aGUgb2JqZWN0IGJlaW5nIHRyYXZlcnNlZFxuICpcbiAqIFByb3BlcnRpZXMgdGhhdCBhcmUgYWRkZWQgYWZ0ZXIgdGhlIGNhbGwgdG8gYG1hcE9iamVjdGAgd2lsbCBub3QgYmUgdmlzaXRlZFxuICogYnkgYGNhbGxiYWNrYC4gSWYgdGhlIHZhbHVlcyBvZiBleGlzdGluZyBwcm9wZXJ0aWVzIGFyZSBjaGFuZ2VkLCB0aGUgdmFsdWVcbiAqIHBhc3NlZCB0byBgY2FsbGJhY2tgIHdpbGwgYmUgdGhlIHZhbHVlIGF0IHRoZSB0aW1lIGBtYXBPYmplY3RgIHZpc2l0cyB0aGVtLlxuICogUHJvcGVydGllcyB0aGF0IGFyZSBkZWxldGVkIGJlZm9yZSBiZWluZyB2aXNpdGVkIGFyZSBub3QgdmlzaXRlZC5cbiAqXG4gKiBAZ3JlcCBmdW5jdGlvbiBvYmplY3RNYXAoKVxuICogQGdyZXAgZnVuY3Rpb24gb2JqTWFwKClcbiAqXG4gKiBAcGFyYW0gez9vYmplY3R9IG9iamVjdFxuICogQHBhcmFtIHtmdW5jdGlvbn0gY2FsbGJhY2tcbiAqIEBwYXJhbSB7Kn0gY29udGV4dFxuICogQHJldHVybiB7P29iamVjdH1cbiAqL1xuZnVuY3Rpb24gbWFwT2JqZWN0KG9iamVjdCwgY2FsbGJhY2ssIGNvbnRleHQpIHtcbiAgaWYgKCFvYmplY3QpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICB2YXIgcmVzdWx0ID0ge307XG4gIGZvciAodmFyIG5hbWUgaW4gb2JqZWN0KSB7XG4gICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBuYW1lKSkge1xuICAgICAgcmVzdWx0W25hbWVdID0gY2FsbGJhY2suY2FsbChjb250ZXh0LCBvYmplY3RbbmFtZV0sIG5hbWUsIG9iamVjdCk7XG4gICAgfVxuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbWFwT2JqZWN0OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/mapObject.js
