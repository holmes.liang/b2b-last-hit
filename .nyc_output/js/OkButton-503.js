__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return OkButton; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

function OkButton(_ref) {
  var prefixCls = _ref.prefixCls,
      locale = _ref.locale,
      okDisabled = _ref.okDisabled,
      onOk = _ref.onOk;
  var className = prefixCls + "-ok-btn";

  if (okDisabled) {
    className += " " + prefixCls + "-ok-btn-disabled";
  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    className: className,
    role: "button",
    onClick: okDisabled ? null : onOk
  }, locale.ok);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvY2FsZW5kYXIvT2tCdXR0b24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1jYWxlbmRhci9lcy9jYWxlbmRhci9Pa0J1dHRvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBPa0J1dHRvbihfcmVmKSB7XG4gIHZhciBwcmVmaXhDbHMgPSBfcmVmLnByZWZpeENscyxcbiAgICAgIGxvY2FsZSA9IF9yZWYubG9jYWxlLFxuICAgICAgb2tEaXNhYmxlZCA9IF9yZWYub2tEaXNhYmxlZCxcbiAgICAgIG9uT2sgPSBfcmVmLm9uT2s7XG5cbiAgdmFyIGNsYXNzTmFtZSA9IHByZWZpeENscyArIFwiLW9rLWJ0blwiO1xuICBpZiAob2tEaXNhYmxlZCkge1xuICAgIGNsYXNzTmFtZSArPSBcIiBcIiArIHByZWZpeENscyArIFwiLW9rLWJ0bi1kaXNhYmxlZFwiO1xuICB9XG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgIFwiYVwiLFxuICAgIHtcbiAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgcm9sZTogXCJidXR0b25cIixcbiAgICAgIG9uQ2xpY2s6IG9rRGlzYWJsZWQgPyBudWxsIDogb25Pa1xuICAgIH0sXG4gICAgbG9jYWxlLm9rXG4gICk7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFPQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/calendar/OkButton.js
