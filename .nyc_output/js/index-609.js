

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

var _util = __webpack_require__(/*! ./util */ "./node_modules/async-validator/es/util.js");

var _validator = __webpack_require__(/*! ./validator/ */ "./node_modules/async-validator/es/validator/index.js");

var _validator2 = _interopRequireDefault(_validator);

var _messages2 = __webpack_require__(/*! ./messages */ "./node_modules/async-validator/es/messages.js");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}
/**
 *  Encapsulates a validation schema.
 *
 *  @param descriptor An object declaring validation rules
 *  for this schema.
 */


function Schema(descriptor) {
  this.rules = null;
  this._messages = _messages2.messages;
  this.define(descriptor);
}

Schema.prototype = {
  messages: function messages(_messages) {
    if (_messages) {
      this._messages = (0, _util.deepMerge)((0, _messages2.newMessages)(), _messages);
    }

    return this._messages;
  },
  define: function define(rules) {
    if (!rules) {
      throw new Error('Cannot configure a schema with no rules');
    }

    if ((typeof rules === 'undefined' ? 'undefined' : _typeof(rules)) !== 'object' || Array.isArray(rules)) {
      throw new Error('Rules must be an object');
    }

    this.rules = {};
    var z = void 0;
    var item = void 0;

    for (z in rules) {
      if (rules.hasOwnProperty(z)) {
        item = rules[z];
        this.rules[z] = Array.isArray(item) ? item : [item];
      }
    }
  },
  validate: function validate(source_) {
    var _this = this;

    var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var oc = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
    var source = source_;
    var options = o;
    var callback = oc;

    if (typeof options === 'function') {
      callback = options;
      options = {};
    }

    if (!this.rules || Object.keys(this.rules).length === 0) {
      if (callback) {
        callback();
      }

      return Promise.resolve();
    }

    function complete(results) {
      var i = void 0;
      var errors = [];
      var fields = {};

      function add(e) {
        if (Array.isArray(e)) {
          var _errors;

          errors = (_errors = errors).concat.apply(_errors, e);
        } else {
          errors.push(e);
        }
      }

      for (i = 0; i < results.length; i++) {
        add(results[i]);
      }

      if (!errors.length) {
        errors = null;
        fields = null;
      } else {
        fields = (0, _util.convertFieldsError)(errors);
      }

      callback(errors, fields);
    }

    if (options.messages) {
      var messages = this.messages();

      if (messages === _messages2.messages) {
        messages = (0, _messages2.newMessages)();
      }

      (0, _util.deepMerge)(messages, options.messages);
      options.messages = messages;
    } else {
      options.messages = this.messages();
    }

    var arr = void 0;
    var value = void 0;
    var series = {};
    var keys = options.keys || Object.keys(this.rules);
    keys.forEach(function (z) {
      arr = _this.rules[z];
      value = source[z];
      arr.forEach(function (r) {
        var rule = r;

        if (typeof rule.transform === 'function') {
          if (source === source_) {
            source = _extends({}, source);
          }

          value = source[z] = rule.transform(value);
        }

        if (typeof rule === 'function') {
          rule = {
            validator: rule
          };
        } else {
          rule = _extends({}, rule);
        }

        rule.validator = _this.getValidationMethod(rule);
        rule.field = z;
        rule.fullField = rule.fullField || z;
        rule.type = _this.getType(rule);

        if (!rule.validator) {
          return;
        }

        series[z] = series[z] || [];
        series[z].push({
          rule: rule,
          value: value,
          source: source,
          field: z
        });
      });
    });
    var errorFields = {};
    return (0, _util.asyncMap)(series, options, function (data, doIt) {
      var rule = data.rule;
      var deep = (rule.type === 'object' || rule.type === 'array') && (_typeof(rule.fields) === 'object' || _typeof(rule.defaultField) === 'object');
      deep = deep && (rule.required || !rule.required && data.value);
      rule.field = data.field;

      function addFullfield(key, schema) {
        return _extends({}, schema, {
          fullField: rule.fullField + '.' + key
        });
      }

      function cb() {
        var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
        var errors = e;

        if (!Array.isArray(errors)) {
          errors = [errors];
        }

        if (!options.suppressWarning && errors.length) {
          Schema.warning('async-validator:', errors);
        }

        if (errors.length && rule.message) {
          errors = [].concat(rule.message);
        }

        errors = errors.map((0, _util.complementError)(rule));

        if (options.first && errors.length) {
          errorFields[rule.field] = 1;
          return doIt(errors);
        }

        if (!deep) {
          doIt(errors);
        } else {
          // if rule is required but the target object
          // does not exist fail at the rule level and don't
          // go deeper
          if (rule.required && !data.value) {
            if (rule.message) {
              errors = [].concat(rule.message).map((0, _util.complementError)(rule));
            } else if (options.error) {
              errors = [options.error(rule, (0, _util.format)(options.messages.required, rule.field))];
            } else {
              errors = [];
            }

            return doIt(errors);
          }

          var fieldsSchema = {};

          if (rule.defaultField) {
            for (var k in data.value) {
              if (data.value.hasOwnProperty(k)) {
                fieldsSchema[k] = rule.defaultField;
              }
            }
          }

          fieldsSchema = _extends({}, fieldsSchema, data.rule.fields);

          for (var f in fieldsSchema) {
            if (fieldsSchema.hasOwnProperty(f)) {
              var fieldSchema = Array.isArray(fieldsSchema[f]) ? fieldsSchema[f] : [fieldsSchema[f]];
              fieldsSchema[f] = fieldSchema.map(addFullfield.bind(null, f));
            }
          }

          var schema = new Schema(fieldsSchema);
          schema.messages(options.messages);

          if (data.rule.options) {
            data.rule.options.messages = options.messages;
            data.rule.options.error = options.error;
          }

          schema.validate(data.value, data.rule.options || options, function (errs) {
            var finalErrors = [];

            if (errors && errors.length) {
              finalErrors.push.apply(finalErrors, errors);
            }

            if (errs && errs.length) {
              finalErrors.push.apply(finalErrors, errs);
            }

            doIt(finalErrors.length ? finalErrors : null);
          });
        }
      }

      var res = void 0;

      if (rule.asyncValidator) {
        res = rule.asyncValidator(rule, data.value, cb, data.source, options);
      } else if (rule.validator) {
        res = rule.validator(rule, data.value, cb, data.source, options);

        if (res === true) {
          cb();
        } else if (res === false) {
          cb(rule.message || rule.field + ' fails');
        } else if (res instanceof Array) {
          cb(res);
        } else if (res instanceof Error) {
          cb(res.message);
        }
      }

      if (res && res.then) {
        res.then(function () {
          return cb();
        }, function (e) {
          return cb(e);
        });
      }
    }, function (results) {
      complete(results);
    });
  },
  getType: function getType(rule) {
    if (rule.type === undefined && rule.pattern instanceof RegExp) {
      rule.type = 'pattern';
    }

    if (typeof rule.validator !== 'function' && rule.type && !_validator2['default'].hasOwnProperty(rule.type)) {
      throw new Error((0, _util.format)('Unknown rule type %s', rule.type));
    }

    return rule.type || 'string';
  },
  getValidationMethod: function getValidationMethod(rule) {
    if (typeof rule.validator === 'function') {
      return rule.validator;
    }

    var keys = Object.keys(rule);
    var messageIndex = keys.indexOf('message');

    if (messageIndex !== -1) {
      keys.splice(messageIndex, 1);
    }

    if (keys.length === 1 && keys[0] === 'required') {
      return _validator2['default'].required;
    }

    return _validator2['default'][this.getType(rule)] || false;
  }
};

Schema.register = function register(type, validator) {
  if (typeof validator !== 'function') {
    throw new Error('Cannot register a validator by type, validator is not a function');
  }

  _validator2['default'][type] = validator;
};

Schema.warning = _util.warning;
Schema.messages = _messages2.messages;
exports['default'] = Schema;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYXN5bmMtdmFsaWRhdG9yL2VzL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvYXN5bmMtdmFsaWRhdG9yL2VzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKCcuL3V0aWwnKTtcblxudmFyIF92YWxpZGF0b3IgPSByZXF1aXJlKCcuL3ZhbGlkYXRvci8nKTtcblxudmFyIF92YWxpZGF0b3IyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdmFsaWRhdG9yKTtcblxudmFyIF9tZXNzYWdlczIgPSByZXF1aXJlKCcuL21lc3NhZ2VzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7ICdkZWZhdWx0Jzogb2JqIH07IH1cblxuLyoqXG4gKiAgRW5jYXBzdWxhdGVzIGEgdmFsaWRhdGlvbiBzY2hlbWEuXG4gKlxuICogIEBwYXJhbSBkZXNjcmlwdG9yIEFuIG9iamVjdCBkZWNsYXJpbmcgdmFsaWRhdGlvbiBydWxlc1xuICogIGZvciB0aGlzIHNjaGVtYS5cbiAqL1xuZnVuY3Rpb24gU2NoZW1hKGRlc2NyaXB0b3IpIHtcbiAgdGhpcy5ydWxlcyA9IG51bGw7XG4gIHRoaXMuX21lc3NhZ2VzID0gX21lc3NhZ2VzMi5tZXNzYWdlcztcbiAgdGhpcy5kZWZpbmUoZGVzY3JpcHRvcik7XG59XG5cblNjaGVtYS5wcm90b3R5cGUgPSB7XG4gIG1lc3NhZ2VzOiBmdW5jdGlvbiBtZXNzYWdlcyhfbWVzc2FnZXMpIHtcbiAgICBpZiAoX21lc3NhZ2VzKSB7XG4gICAgICB0aGlzLl9tZXNzYWdlcyA9ICgwLCBfdXRpbC5kZWVwTWVyZ2UpKCgwLCBfbWVzc2FnZXMyLm5ld01lc3NhZ2VzKSgpLCBfbWVzc2FnZXMpO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5fbWVzc2FnZXM7XG4gIH0sXG4gIGRlZmluZTogZnVuY3Rpb24gZGVmaW5lKHJ1bGVzKSB7XG4gICAgaWYgKCFydWxlcykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdDYW5ub3QgY29uZmlndXJlIGEgc2NoZW1hIHdpdGggbm8gcnVsZXMnKTtcbiAgICB9XG4gICAgaWYgKCh0eXBlb2YgcnVsZXMgPT09ICd1bmRlZmluZWQnID8gJ3VuZGVmaW5lZCcgOiBfdHlwZW9mKHJ1bGVzKSkgIT09ICdvYmplY3QnIHx8IEFycmF5LmlzQXJyYXkocnVsZXMpKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1J1bGVzIG11c3QgYmUgYW4gb2JqZWN0Jyk7XG4gICAgfVxuICAgIHRoaXMucnVsZXMgPSB7fTtcbiAgICB2YXIgeiA9IHZvaWQgMDtcbiAgICB2YXIgaXRlbSA9IHZvaWQgMDtcbiAgICBmb3IgKHogaW4gcnVsZXMpIHtcbiAgICAgIGlmIChydWxlcy5oYXNPd25Qcm9wZXJ0eSh6KSkge1xuICAgICAgICBpdGVtID0gcnVsZXNbel07XG4gICAgICAgIHRoaXMucnVsZXNbel0gPSBBcnJheS5pc0FycmF5KGl0ZW0pID8gaXRlbSA6IFtpdGVtXTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIHZhbGlkYXRlOiBmdW5jdGlvbiB2YWxpZGF0ZShzb3VyY2VfKSB7XG4gICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgIHZhciBvID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcbiAgICB2YXIgb2MgPSBhcmd1bWVudHMubGVuZ3RoID4gMiAmJiBhcmd1bWVudHNbMl0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1syXSA6IGZ1bmN0aW9uICgpIHt9O1xuXG4gICAgdmFyIHNvdXJjZSA9IHNvdXJjZV87XG4gICAgdmFyIG9wdGlvbnMgPSBvO1xuICAgIHZhciBjYWxsYmFjayA9IG9jO1xuICAgIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgY2FsbGJhY2sgPSBvcHRpb25zO1xuICAgICAgb3B0aW9ucyA9IHt9O1xuICAgIH1cbiAgICBpZiAoIXRoaXMucnVsZXMgfHwgT2JqZWN0LmtleXModGhpcy5ydWxlcykubGVuZ3RoID09PSAwKSB7XG4gICAgICBpZiAoY2FsbGJhY2spIHtcbiAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBjb21wbGV0ZShyZXN1bHRzKSB7XG4gICAgICB2YXIgaSA9IHZvaWQgMDtcbiAgICAgIHZhciBlcnJvcnMgPSBbXTtcbiAgICAgIHZhciBmaWVsZHMgPSB7fTtcblxuICAgICAgZnVuY3Rpb24gYWRkKGUpIHtcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZSkpIHtcbiAgICAgICAgICB2YXIgX2Vycm9ycztcblxuICAgICAgICAgIGVycm9ycyA9IChfZXJyb3JzID0gZXJyb3JzKS5jb25jYXQuYXBwbHkoX2Vycm9ycywgZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgZXJyb3JzLnB1c2goZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZm9yIChpID0gMDsgaSA8IHJlc3VsdHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgYWRkKHJlc3VsdHNbaV0pO1xuICAgICAgfVxuICAgICAgaWYgKCFlcnJvcnMubGVuZ3RoKSB7XG4gICAgICAgIGVycm9ycyA9IG51bGw7XG4gICAgICAgIGZpZWxkcyA9IG51bGw7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmaWVsZHMgPSAoMCwgX3V0aWwuY29udmVydEZpZWxkc0Vycm9yKShlcnJvcnMpO1xuICAgICAgfVxuICAgICAgY2FsbGJhY2soZXJyb3JzLCBmaWVsZHMpO1xuICAgIH1cblxuICAgIGlmIChvcHRpb25zLm1lc3NhZ2VzKSB7XG4gICAgICB2YXIgbWVzc2FnZXMgPSB0aGlzLm1lc3NhZ2VzKCk7XG4gICAgICBpZiAobWVzc2FnZXMgPT09IF9tZXNzYWdlczIubWVzc2FnZXMpIHtcbiAgICAgICAgbWVzc2FnZXMgPSAoMCwgX21lc3NhZ2VzMi5uZXdNZXNzYWdlcykoKTtcbiAgICAgIH1cbiAgICAgICgwLCBfdXRpbC5kZWVwTWVyZ2UpKG1lc3NhZ2VzLCBvcHRpb25zLm1lc3NhZ2VzKTtcbiAgICAgIG9wdGlvbnMubWVzc2FnZXMgPSBtZXNzYWdlcztcbiAgICB9IGVsc2Uge1xuICAgICAgb3B0aW9ucy5tZXNzYWdlcyA9IHRoaXMubWVzc2FnZXMoKTtcbiAgICB9XG4gICAgdmFyIGFyciA9IHZvaWQgMDtcbiAgICB2YXIgdmFsdWUgPSB2b2lkIDA7XG4gICAgdmFyIHNlcmllcyA9IHt9O1xuICAgIHZhciBrZXlzID0gb3B0aW9ucy5rZXlzIHx8IE9iamVjdC5rZXlzKHRoaXMucnVsZXMpO1xuICAgIGtleXMuZm9yRWFjaChmdW5jdGlvbiAoeikge1xuICAgICAgYXJyID0gX3RoaXMucnVsZXNbel07XG4gICAgICB2YWx1ZSA9IHNvdXJjZVt6XTtcbiAgICAgIGFyci5mb3JFYWNoKGZ1bmN0aW9uIChyKSB7XG4gICAgICAgIHZhciBydWxlID0gcjtcbiAgICAgICAgaWYgKHR5cGVvZiBydWxlLnRyYW5zZm9ybSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIGlmIChzb3VyY2UgPT09IHNvdXJjZV8pIHtcbiAgICAgICAgICAgIHNvdXJjZSA9IF9leHRlbmRzKHt9LCBzb3VyY2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YWx1ZSA9IHNvdXJjZVt6XSA9IHJ1bGUudHJhbnNmb3JtKHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodHlwZW9mIHJ1bGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICBydWxlID0ge1xuICAgICAgICAgICAgdmFsaWRhdG9yOiBydWxlXG4gICAgICAgICAgfTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBydWxlID0gX2V4dGVuZHMoe30sIHJ1bGUpO1xuICAgICAgICB9XG4gICAgICAgIHJ1bGUudmFsaWRhdG9yID0gX3RoaXMuZ2V0VmFsaWRhdGlvbk1ldGhvZChydWxlKTtcbiAgICAgICAgcnVsZS5maWVsZCA9IHo7XG4gICAgICAgIHJ1bGUuZnVsbEZpZWxkID0gcnVsZS5mdWxsRmllbGQgfHwgejtcbiAgICAgICAgcnVsZS50eXBlID0gX3RoaXMuZ2V0VHlwZShydWxlKTtcbiAgICAgICAgaWYgKCFydWxlLnZhbGlkYXRvcikge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBzZXJpZXNbel0gPSBzZXJpZXNbel0gfHwgW107XG4gICAgICAgIHNlcmllc1t6XS5wdXNoKHtcbiAgICAgICAgICBydWxlOiBydWxlLFxuICAgICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgICBzb3VyY2U6IHNvdXJjZSxcbiAgICAgICAgICBmaWVsZDogelxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHZhciBlcnJvckZpZWxkcyA9IHt9O1xuICAgIHJldHVybiAoMCwgX3V0aWwuYXN5bmNNYXApKHNlcmllcywgb3B0aW9ucywgZnVuY3Rpb24gKGRhdGEsIGRvSXQpIHtcbiAgICAgIHZhciBydWxlID0gZGF0YS5ydWxlO1xuICAgICAgdmFyIGRlZXAgPSAocnVsZS50eXBlID09PSAnb2JqZWN0JyB8fCBydWxlLnR5cGUgPT09ICdhcnJheScpICYmIChfdHlwZW9mKHJ1bGUuZmllbGRzKSA9PT0gJ29iamVjdCcgfHwgX3R5cGVvZihydWxlLmRlZmF1bHRGaWVsZCkgPT09ICdvYmplY3QnKTtcbiAgICAgIGRlZXAgPSBkZWVwICYmIChydWxlLnJlcXVpcmVkIHx8ICFydWxlLnJlcXVpcmVkICYmIGRhdGEudmFsdWUpO1xuICAgICAgcnVsZS5maWVsZCA9IGRhdGEuZmllbGQ7XG5cbiAgICAgIGZ1bmN0aW9uIGFkZEZ1bGxmaWVsZChrZXksIHNjaGVtYSkge1xuICAgICAgICByZXR1cm4gX2V4dGVuZHMoe30sIHNjaGVtYSwge1xuICAgICAgICAgIGZ1bGxGaWVsZDogcnVsZS5mdWxsRmllbGQgKyAnLicgKyBrZXlcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIGNiKCkge1xuICAgICAgICB2YXIgZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogW107XG5cbiAgICAgICAgdmFyIGVycm9ycyA9IGU7XG4gICAgICAgIGlmICghQXJyYXkuaXNBcnJheShlcnJvcnMpKSB7XG4gICAgICAgICAgZXJyb3JzID0gW2Vycm9yc107XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCFvcHRpb25zLnN1cHByZXNzV2FybmluZyAmJiBlcnJvcnMubGVuZ3RoKSB7XG4gICAgICAgICAgU2NoZW1hLndhcm5pbmcoJ2FzeW5jLXZhbGlkYXRvcjonLCBlcnJvcnMpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlcnJvcnMubGVuZ3RoICYmIHJ1bGUubWVzc2FnZSkge1xuICAgICAgICAgIGVycm9ycyA9IFtdLmNvbmNhdChydWxlLm1lc3NhZ2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgZXJyb3JzID0gZXJyb3JzLm1hcCgoMCwgX3V0aWwuY29tcGxlbWVudEVycm9yKShydWxlKSk7XG5cbiAgICAgICAgaWYgKG9wdGlvbnMuZmlyc3QgJiYgZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICAgIGVycm9yRmllbGRzW3J1bGUuZmllbGRdID0gMTtcbiAgICAgICAgICByZXR1cm4gZG9JdChlcnJvcnMpO1xuICAgICAgICB9XG4gICAgICAgIGlmICghZGVlcCkge1xuICAgICAgICAgIGRvSXQoZXJyb3JzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBpZiBydWxlIGlzIHJlcXVpcmVkIGJ1dCB0aGUgdGFyZ2V0IG9iamVjdFxuICAgICAgICAgIC8vIGRvZXMgbm90IGV4aXN0IGZhaWwgYXQgdGhlIHJ1bGUgbGV2ZWwgYW5kIGRvbid0XG4gICAgICAgICAgLy8gZ28gZGVlcGVyXG4gICAgICAgICAgaWYgKHJ1bGUucmVxdWlyZWQgJiYgIWRhdGEudmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChydWxlLm1lc3NhZ2UpIHtcbiAgICAgICAgICAgICAgZXJyb3JzID0gW10uY29uY2F0KHJ1bGUubWVzc2FnZSkubWFwKCgwLCBfdXRpbC5jb21wbGVtZW50RXJyb3IpKHJ1bGUpKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAob3B0aW9ucy5lcnJvcikge1xuICAgICAgICAgICAgICBlcnJvcnMgPSBbb3B0aW9ucy5lcnJvcihydWxlLCAoMCwgX3V0aWwuZm9ybWF0KShvcHRpb25zLm1lc3NhZ2VzLnJlcXVpcmVkLCBydWxlLmZpZWxkKSldO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgZXJyb3JzID0gW107XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZG9JdChlcnJvcnMpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHZhciBmaWVsZHNTY2hlbWEgPSB7fTtcbiAgICAgICAgICBpZiAocnVsZS5kZWZhdWx0RmllbGQpIHtcbiAgICAgICAgICAgIGZvciAodmFyIGsgaW4gZGF0YS52YWx1ZSkge1xuICAgICAgICAgICAgICBpZiAoZGF0YS52YWx1ZS5oYXNPd25Qcm9wZXJ0eShrKSkge1xuICAgICAgICAgICAgICAgIGZpZWxkc1NjaGVtYVtrXSA9IHJ1bGUuZGVmYXVsdEZpZWxkO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGZpZWxkc1NjaGVtYSA9IF9leHRlbmRzKHt9LCBmaWVsZHNTY2hlbWEsIGRhdGEucnVsZS5maWVsZHMpO1xuICAgICAgICAgIGZvciAodmFyIGYgaW4gZmllbGRzU2NoZW1hKSB7XG4gICAgICAgICAgICBpZiAoZmllbGRzU2NoZW1hLmhhc093blByb3BlcnR5KGYpKSB7XG4gICAgICAgICAgICAgIHZhciBmaWVsZFNjaGVtYSA9IEFycmF5LmlzQXJyYXkoZmllbGRzU2NoZW1hW2ZdKSA/IGZpZWxkc1NjaGVtYVtmXSA6IFtmaWVsZHNTY2hlbWFbZl1dO1xuICAgICAgICAgICAgICBmaWVsZHNTY2hlbWFbZl0gPSBmaWVsZFNjaGVtYS5tYXAoYWRkRnVsbGZpZWxkLmJpbmQobnVsbCwgZikpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgc2NoZW1hID0gbmV3IFNjaGVtYShmaWVsZHNTY2hlbWEpO1xuICAgICAgICAgIHNjaGVtYS5tZXNzYWdlcyhvcHRpb25zLm1lc3NhZ2VzKTtcbiAgICAgICAgICBpZiAoZGF0YS5ydWxlLm9wdGlvbnMpIHtcbiAgICAgICAgICAgIGRhdGEucnVsZS5vcHRpb25zLm1lc3NhZ2VzID0gb3B0aW9ucy5tZXNzYWdlcztcbiAgICAgICAgICAgIGRhdGEucnVsZS5vcHRpb25zLmVycm9yID0gb3B0aW9ucy5lcnJvcjtcbiAgICAgICAgICB9XG4gICAgICAgICAgc2NoZW1hLnZhbGlkYXRlKGRhdGEudmFsdWUsIGRhdGEucnVsZS5vcHRpb25zIHx8IG9wdGlvbnMsIGZ1bmN0aW9uIChlcnJzKSB7XG4gICAgICAgICAgICB2YXIgZmluYWxFcnJvcnMgPSBbXTtcbiAgICAgICAgICAgIGlmIChlcnJvcnMgJiYgZXJyb3JzLmxlbmd0aCkge1xuICAgICAgICAgICAgICBmaW5hbEVycm9ycy5wdXNoLmFwcGx5KGZpbmFsRXJyb3JzLCBlcnJvcnMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGVycnMgJiYgZXJycy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgZmluYWxFcnJvcnMucHVzaC5hcHBseShmaW5hbEVycm9ycywgZXJycyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkb0l0KGZpbmFsRXJyb3JzLmxlbmd0aCA/IGZpbmFsRXJyb3JzIDogbnVsbCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdmFyIHJlcyA9IHZvaWQgMDtcbiAgICAgIGlmIChydWxlLmFzeW5jVmFsaWRhdG9yKSB7XG4gICAgICAgIHJlcyA9IHJ1bGUuYXN5bmNWYWxpZGF0b3IocnVsZSwgZGF0YS52YWx1ZSwgY2IsIGRhdGEuc291cmNlLCBvcHRpb25zKTtcbiAgICAgIH0gZWxzZSBpZiAocnVsZS52YWxpZGF0b3IpIHtcbiAgICAgICAgcmVzID0gcnVsZS52YWxpZGF0b3IocnVsZSwgZGF0YS52YWx1ZSwgY2IsIGRhdGEuc291cmNlLCBvcHRpb25zKTtcbiAgICAgICAgaWYgKHJlcyA9PT0gdHJ1ZSkge1xuICAgICAgICAgIGNiKCk7XG4gICAgICAgIH0gZWxzZSBpZiAocmVzID09PSBmYWxzZSkge1xuICAgICAgICAgIGNiKHJ1bGUubWVzc2FnZSB8fCBydWxlLmZpZWxkICsgJyBmYWlscycpO1xuICAgICAgICB9IGVsc2UgaWYgKHJlcyBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAgICAgY2IocmVzKTtcbiAgICAgICAgfSBlbHNlIGlmIChyZXMgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICAgIGNiKHJlcy5tZXNzYWdlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKHJlcyAmJiByZXMudGhlbikge1xuICAgICAgICByZXMudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIGNiKCk7XG4gICAgICAgIH0sIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgcmV0dXJuIGNiKGUpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9LCBmdW5jdGlvbiAocmVzdWx0cykge1xuICAgICAgY29tcGxldGUocmVzdWx0cyk7XG4gICAgfSk7XG4gIH0sXG4gIGdldFR5cGU6IGZ1bmN0aW9uIGdldFR5cGUocnVsZSkge1xuICAgIGlmIChydWxlLnR5cGUgPT09IHVuZGVmaW5lZCAmJiBydWxlLnBhdHRlcm4gaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICAgIHJ1bGUudHlwZSA9ICdwYXR0ZXJuJztcbiAgICB9XG4gICAgaWYgKHR5cGVvZiBydWxlLnZhbGlkYXRvciAhPT0gJ2Z1bmN0aW9uJyAmJiBydWxlLnR5cGUgJiYgIV92YWxpZGF0b3IyWydkZWZhdWx0J10uaGFzT3duUHJvcGVydHkocnVsZS50eXBlKSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCgwLCBfdXRpbC5mb3JtYXQpKCdVbmtub3duIHJ1bGUgdHlwZSAlcycsIHJ1bGUudHlwZSkpO1xuICAgIH1cbiAgICByZXR1cm4gcnVsZS50eXBlIHx8ICdzdHJpbmcnO1xuICB9LFxuICBnZXRWYWxpZGF0aW9uTWV0aG9kOiBmdW5jdGlvbiBnZXRWYWxpZGF0aW9uTWV0aG9kKHJ1bGUpIHtcbiAgICBpZiAodHlwZW9mIHJ1bGUudmFsaWRhdG9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXR1cm4gcnVsZS52YWxpZGF0b3I7XG4gICAgfVxuICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMocnVsZSk7XG4gICAgdmFyIG1lc3NhZ2VJbmRleCA9IGtleXMuaW5kZXhPZignbWVzc2FnZScpO1xuICAgIGlmIChtZXNzYWdlSW5kZXggIT09IC0xKSB7XG4gICAgICBrZXlzLnNwbGljZShtZXNzYWdlSW5kZXgsIDEpO1xuICAgIH1cbiAgICBpZiAoa2V5cy5sZW5ndGggPT09IDEgJiYga2V5c1swXSA9PT0gJ3JlcXVpcmVkJykge1xuICAgICAgcmV0dXJuIF92YWxpZGF0b3IyWydkZWZhdWx0J10ucmVxdWlyZWQ7XG4gICAgfVxuICAgIHJldHVybiBfdmFsaWRhdG9yMlsnZGVmYXVsdCddW3RoaXMuZ2V0VHlwZShydWxlKV0gfHwgZmFsc2U7XG4gIH1cbn07XG5cblNjaGVtYS5yZWdpc3RlciA9IGZ1bmN0aW9uIHJlZ2lzdGVyKHR5cGUsIHZhbGlkYXRvcikge1xuICBpZiAodHlwZW9mIHZhbGlkYXRvciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHRocm93IG5ldyBFcnJvcignQ2Fubm90IHJlZ2lzdGVyIGEgdmFsaWRhdG9yIGJ5IHR5cGUsIHZhbGlkYXRvciBpcyBub3QgYSBmdW5jdGlvbicpO1xuICB9XG4gIF92YWxpZGF0b3IyWydkZWZhdWx0J11bdHlwZV0gPSB2YWxpZGF0b3I7XG59O1xuXG5TY2hlbWEud2FybmluZyA9IF91dGlsLndhcm5pbmc7XG5cblNjaGVtYS5tZXNzYWdlcyA9IF9tZXNzYWdlczIubWVzc2FnZXM7XG5cbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IFNjaGVtYTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBOzs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQTNQQTtBQUNBO0FBNlBBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/async-validator/es/index.js
