/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getContentStateFragment
 * @format
 * 
 */


var randomizeBlockMapKeys = __webpack_require__(/*! ./randomizeBlockMapKeys */ "./node_modules/draft-js/lib/randomizeBlockMapKeys.js");

var removeEntitiesAtEdges = __webpack_require__(/*! ./removeEntitiesAtEdges */ "./node_modules/draft-js/lib/removeEntitiesAtEdges.js");

var getContentStateFragment = function getContentStateFragment(contentState, selectionState) {
  var startKey = selectionState.getStartKey();
  var startOffset = selectionState.getStartOffset();
  var endKey = selectionState.getEndKey();
  var endOffset = selectionState.getEndOffset(); // Edge entities should be stripped to ensure that we don't preserve
  // invalid partial entities when the fragment is reused. We do, however,
  // preserve entities that are entirely within the selection range.

  var contentWithoutEdgeEntities = removeEntitiesAtEdges(contentState, selectionState);
  var blockMap = contentWithoutEdgeEntities.getBlockMap();
  var blockKeys = blockMap.keySeq();
  var startIndex = blockKeys.indexOf(startKey);
  var endIndex = blockKeys.indexOf(endKey) + 1;
  return randomizeBlockMapKeys(blockMap.slice(startIndex, endIndex).map(function (block, blockKey) {
    var text = block.getText();
    var chars = block.getCharacterList();

    if (startKey === endKey) {
      return block.merge({
        text: text.slice(startOffset, endOffset),
        characterList: chars.slice(startOffset, endOffset)
      });
    }

    if (blockKey === startKey) {
      return block.merge({
        text: text.slice(startOffset),
        characterList: chars.slice(startOffset)
      });
    }

    if (blockKey === endKey) {
      return block.merge({
        text: text.slice(0, endOffset),
        characterList: chars.slice(0, endOffset)
      });
    }

    return block;
  }));
};

module.exports = getContentStateFragment;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldENvbnRlbnRTdGF0ZUZyYWdtZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldENvbnRlbnRTdGF0ZUZyYWdtZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0Q29udGVudFN0YXRlRnJhZ21lbnRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIHJhbmRvbWl6ZUJsb2NrTWFwS2V5cyA9IHJlcXVpcmUoJy4vcmFuZG9taXplQmxvY2tNYXBLZXlzJyk7XG52YXIgcmVtb3ZlRW50aXRpZXNBdEVkZ2VzID0gcmVxdWlyZSgnLi9yZW1vdmVFbnRpdGllc0F0RWRnZXMnKTtcblxudmFyIGdldENvbnRlbnRTdGF0ZUZyYWdtZW50ID0gZnVuY3Rpb24gZ2V0Q29udGVudFN0YXRlRnJhZ21lbnQoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSkge1xuICB2YXIgc3RhcnRLZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRTdGFydEtleSgpO1xuICB2YXIgc3RhcnRPZmZzZXQgPSBzZWxlY3Rpb25TdGF0ZS5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgZW5kS2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kS2V5KCk7XG4gIHZhciBlbmRPZmZzZXQgPSBzZWxlY3Rpb25TdGF0ZS5nZXRFbmRPZmZzZXQoKTtcblxuICAvLyBFZGdlIGVudGl0aWVzIHNob3VsZCBiZSBzdHJpcHBlZCB0byBlbnN1cmUgdGhhdCB3ZSBkb24ndCBwcmVzZXJ2ZVxuICAvLyBpbnZhbGlkIHBhcnRpYWwgZW50aXRpZXMgd2hlbiB0aGUgZnJhZ21lbnQgaXMgcmV1c2VkLiBXZSBkbywgaG93ZXZlcixcbiAgLy8gcHJlc2VydmUgZW50aXRpZXMgdGhhdCBhcmUgZW50aXJlbHkgd2l0aGluIHRoZSBzZWxlY3Rpb24gcmFuZ2UuXG4gIHZhciBjb250ZW50V2l0aG91dEVkZ2VFbnRpdGllcyA9IHJlbW92ZUVudGl0aWVzQXRFZGdlcyhjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlKTtcblxuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50V2l0aG91dEVkZ2VFbnRpdGllcy5nZXRCbG9ja01hcCgpO1xuICB2YXIgYmxvY2tLZXlzID0gYmxvY2tNYXAua2V5U2VxKCk7XG4gIHZhciBzdGFydEluZGV4ID0gYmxvY2tLZXlzLmluZGV4T2Yoc3RhcnRLZXkpO1xuICB2YXIgZW5kSW5kZXggPSBibG9ja0tleXMuaW5kZXhPZihlbmRLZXkpICsgMTtcblxuICByZXR1cm4gcmFuZG9taXplQmxvY2tNYXBLZXlzKGJsb2NrTWFwLnNsaWNlKHN0YXJ0SW5kZXgsIGVuZEluZGV4KS5tYXAoZnVuY3Rpb24gKGJsb2NrLCBibG9ja0tleSkge1xuICAgIHZhciB0ZXh0ID0gYmxvY2suZ2V0VGV4dCgpO1xuICAgIHZhciBjaGFycyA9IGJsb2NrLmdldENoYXJhY3Rlckxpc3QoKTtcblxuICAgIGlmIChzdGFydEtleSA9PT0gZW5kS2V5KSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICB0ZXh0OiB0ZXh0LnNsaWNlKHN0YXJ0T2Zmc2V0LCBlbmRPZmZzZXQpLFxuICAgICAgICBjaGFyYWN0ZXJMaXN0OiBjaGFycy5zbGljZShzdGFydE9mZnNldCwgZW5kT2Zmc2V0KVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKGJsb2NrS2V5ID09PSBzdGFydEtleSkge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgdGV4dDogdGV4dC5zbGljZShzdGFydE9mZnNldCksXG4gICAgICAgIGNoYXJhY3Rlckxpc3Q6IGNoYXJzLnNsaWNlKHN0YXJ0T2Zmc2V0KVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKGJsb2NrS2V5ID09PSBlbmRLZXkpIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIHRleHQ6IHRleHQuc2xpY2UoMCwgZW5kT2Zmc2V0KSxcbiAgICAgICAgY2hhcmFjdGVyTGlzdDogY2hhcnMuc2xpY2UoMCwgZW5kT2Zmc2V0KVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGJsb2NrO1xuICB9KSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGdldENvbnRlbnRTdGF0ZUZyYWdtZW50OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getContentStateFragment.js
