/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var map = _util.map;

var createRenderPlanner = __webpack_require__(/*! ../chart/helper/createRenderPlanner */ "./node_modules/echarts/lib/chart/helper/createRenderPlanner.js");

var _dataStackHelper = __webpack_require__(/*! ../data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var isDimensionStacked = _dataStackHelper.isDimensionStacked;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/* global Float32Array */

function _default(seriesType) {
  return {
    seriesType: seriesType,
    plan: createRenderPlanner(),
    reset: function reset(seriesModel) {
      var data = seriesModel.getData();
      var coordSys = seriesModel.coordinateSystem;
      var pipelineContext = seriesModel.pipelineContext;
      var isLargeRender = pipelineContext.large;

      if (!coordSys) {
        return;
      }

      var dims = map(coordSys.dimensions, function (dim) {
        return data.mapDimension(dim);
      }).slice(0, 2);
      var dimLen = dims.length;
      var stackResultDim = data.getCalculationInfo('stackResultDimension');

      if (isDimensionStacked(data, dims[0]
      /*, dims[1]*/
      )) {
        dims[0] = stackResultDim;
      }

      if (isDimensionStacked(data, dims[1]
      /*, dims[0]*/
      )) {
        dims[1] = stackResultDim;
      }

      function progress(params, data) {
        var segCount = params.end - params.start;
        var points = isLargeRender && new Float32Array(segCount * dimLen);

        for (var i = params.start, offset = 0, tmpIn = [], tmpOut = []; i < params.end; i++) {
          var point;

          if (dimLen === 1) {
            var x = data.get(dims[0], i);
            point = !isNaN(x) && coordSys.dataToPoint(x, null, tmpOut);
          } else {
            var x = tmpIn[0] = data.get(dims[0], i);
            var y = tmpIn[1] = data.get(dims[1], i); // Also {Array.<number>}, not undefined to avoid if...else... statement

            point = !isNaN(x) && !isNaN(y) && coordSys.dataToPoint(tmpIn, null, tmpOut);
          }

          if (isLargeRender) {
            points[offset++] = point ? point[0] : NaN;
            points[offset++] = point ? point[1] : NaN;
          } else {
            data.setItemLayout(i, point && point.slice() || [NaN, NaN]);
          }
        }

        isLargeRender && data.setLayout('symbolPoints', points);
      }

      return dimLen && {
        progress: progress
      };
    }
  };
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbGF5b3V0L3BvaW50cy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2xheW91dC9wb2ludHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBtYXAgPSBfdXRpbC5tYXA7XG5cbnZhciBjcmVhdGVSZW5kZXJQbGFubmVyID0gcmVxdWlyZShcIi4uL2NoYXJ0L2hlbHBlci9jcmVhdGVSZW5kZXJQbGFubmVyXCIpO1xuXG52YXIgX2RhdGFTdGFja0hlbHBlciA9IHJlcXVpcmUoXCIuLi9kYXRhL2hlbHBlci9kYXRhU3RhY2tIZWxwZXJcIik7XG5cbnZhciBpc0RpbWVuc2lvblN0YWNrZWQgPSBfZGF0YVN0YWNrSGVscGVyLmlzRGltZW5zaW9uU3RhY2tlZDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKiBnbG9iYWwgRmxvYXQzMkFycmF5ICovXG5mdW5jdGlvbiBfZGVmYXVsdChzZXJpZXNUeXBlKSB7XG4gIHJldHVybiB7XG4gICAgc2VyaWVzVHlwZTogc2VyaWVzVHlwZSxcbiAgICBwbGFuOiBjcmVhdGVSZW5kZXJQbGFubmVyKCksXG4gICAgcmVzZXQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gICAgICB2YXIgY29vcmRTeXMgPSBzZXJpZXNNb2RlbC5jb29yZGluYXRlU3lzdGVtO1xuICAgICAgdmFyIHBpcGVsaW5lQ29udGV4dCA9IHNlcmllc01vZGVsLnBpcGVsaW5lQ29udGV4dDtcbiAgICAgIHZhciBpc0xhcmdlUmVuZGVyID0gcGlwZWxpbmVDb250ZXh0LmxhcmdlO1xuXG4gICAgICBpZiAoIWNvb3JkU3lzKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGRpbXMgPSBtYXAoY29vcmRTeXMuZGltZW5zaW9ucywgZnVuY3Rpb24gKGRpbSkge1xuICAgICAgICByZXR1cm4gZGF0YS5tYXBEaW1lbnNpb24oZGltKTtcbiAgICAgIH0pLnNsaWNlKDAsIDIpO1xuICAgICAgdmFyIGRpbUxlbiA9IGRpbXMubGVuZ3RoO1xuICAgICAgdmFyIHN0YWNrUmVzdWx0RGltID0gZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ3N0YWNrUmVzdWx0RGltZW5zaW9uJyk7XG5cbiAgICAgIGlmIChpc0RpbWVuc2lvblN0YWNrZWQoZGF0YSwgZGltc1swXVxuICAgICAgLyosIGRpbXNbMV0qL1xuICAgICAgKSkge1xuICAgICAgICBkaW1zWzBdID0gc3RhY2tSZXN1bHREaW07XG4gICAgICB9XG5cbiAgICAgIGlmIChpc0RpbWVuc2lvblN0YWNrZWQoZGF0YSwgZGltc1sxXVxuICAgICAgLyosIGRpbXNbMF0qL1xuICAgICAgKSkge1xuICAgICAgICBkaW1zWzFdID0gc3RhY2tSZXN1bHREaW07XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIHByb2dyZXNzKHBhcmFtcywgZGF0YSkge1xuICAgICAgICB2YXIgc2VnQ291bnQgPSBwYXJhbXMuZW5kIC0gcGFyYW1zLnN0YXJ0O1xuICAgICAgICB2YXIgcG9pbnRzID0gaXNMYXJnZVJlbmRlciAmJiBuZXcgRmxvYXQzMkFycmF5KHNlZ0NvdW50ICogZGltTGVuKTtcblxuICAgICAgICBmb3IgKHZhciBpID0gcGFyYW1zLnN0YXJ0LCBvZmZzZXQgPSAwLCB0bXBJbiA9IFtdLCB0bXBPdXQgPSBbXTsgaSA8IHBhcmFtcy5lbmQ7IGkrKykge1xuICAgICAgICAgIHZhciBwb2ludDtcblxuICAgICAgICAgIGlmIChkaW1MZW4gPT09IDEpIHtcbiAgICAgICAgICAgIHZhciB4ID0gZGF0YS5nZXQoZGltc1swXSwgaSk7XG4gICAgICAgICAgICBwb2ludCA9ICFpc05hTih4KSAmJiBjb29yZFN5cy5kYXRhVG9Qb2ludCh4LCBudWxsLCB0bXBPdXQpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YXIgeCA9IHRtcEluWzBdID0gZGF0YS5nZXQoZGltc1swXSwgaSk7XG4gICAgICAgICAgICB2YXIgeSA9IHRtcEluWzFdID0gZGF0YS5nZXQoZGltc1sxXSwgaSk7IC8vIEFsc28ge0FycmF5LjxudW1iZXI+fSwgbm90IHVuZGVmaW5lZCB0byBhdm9pZCBpZi4uLmVsc2UuLi4gc3RhdGVtZW50XG5cbiAgICAgICAgICAgIHBvaW50ID0gIWlzTmFOKHgpICYmICFpc05hTih5KSAmJiBjb29yZFN5cy5kYXRhVG9Qb2ludCh0bXBJbiwgbnVsbCwgdG1wT3V0KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoaXNMYXJnZVJlbmRlcikge1xuICAgICAgICAgICAgcG9pbnRzW29mZnNldCsrXSA9IHBvaW50ID8gcG9pbnRbMF0gOiBOYU47XG4gICAgICAgICAgICBwb2ludHNbb2Zmc2V0KytdID0gcG9pbnQgPyBwb2ludFsxXSA6IE5hTjtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZGF0YS5zZXRJdGVtTGF5b3V0KGksIHBvaW50ICYmIHBvaW50LnNsaWNlKCkgfHwgW05hTiwgTmFOXSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaXNMYXJnZVJlbmRlciAmJiBkYXRhLnNldExheW91dCgnc3ltYm9sUG9pbnRzJywgcG9pbnRzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGRpbUxlbiAmJiB7XG4gICAgICAgIHByb2dyZXNzOiBwcm9ncmVzc1xuICAgICAgfTtcbiAgICB9XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQTlEQTtBQWdFQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/layout/points.js
