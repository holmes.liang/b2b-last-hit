/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var _default = echarts.extendComponentModel({
  type: 'tooltip',
  dependencies: ['axisPointer'],
  defaultOption: {
    zlevel: 0,
    z: 60,
    show: true,
    // tooltip主体内容
    showContent: true,
    // 'trigger' only works on coordinate system.
    // 'item' | 'axis' | 'none'
    trigger: 'item',
    // 'click' | 'mousemove' | 'none'
    triggerOn: 'mousemove|click',
    alwaysShowContent: false,
    displayMode: 'single',
    // 'single' | 'multipleByCoordSys'
    renderMode: 'auto',
    // 'auto' | 'html' | 'richText'
    // 'auto': use html by default, and use non-html if `document` is not defined
    // 'html': use html for tooltip
    // 'richText': use canvas, svg, and etc. for tooltip
    // 位置 {Array} | {Function}
    // position: null
    // Consider triggered from axisPointer handle, verticalAlign should be 'middle'
    // align: null,
    // verticalAlign: null,
    // 是否约束 content 在 viewRect 中。默认 false 是为了兼容以前版本。
    confine: false,
    // 内容格式器：{string}（Template） ¦ {Function}
    // formatter: null
    showDelay: 0,
    // 隐藏延迟，单位ms
    hideDelay: 100,
    // 动画变换时间，单位s
    transitionDuration: 0.4,
    enterable: false,
    // 提示背景颜色，默认为透明度为0.7的黑色
    backgroundColor: 'rgba(50,50,50,0.7)',
    // 提示边框颜色
    borderColor: '#333',
    // 提示边框圆角，单位px，默认为4
    borderRadius: 4,
    // 提示边框线宽，单位px，默认为0（无边框）
    borderWidth: 0,
    // 提示内边距，单位px，默认各方向内边距为5，
    // 接受数组分别设定上右下左边距，同css
    padding: 5,
    // Extra css text
    extraCssText: '',
    // 坐标轴指示器，坐标轴触发有效
    axisPointer: {
      // 默认为直线
      // 可选为：'line' | 'shadow' | 'cross'
      type: 'line',
      // type 为 line 的时候有效，指定 tooltip line 所在的轴，可选
      // 可选 'x' | 'y' | 'angle' | 'radius' | 'auto'
      // 默认 'auto'，会选择类型为 category 的轴，对于双数值轴，笛卡尔坐标系会默认选择 x 轴
      // 极坐标系会默认选择 angle 轴
      axis: 'auto',
      animation: 'auto',
      animationDurationUpdate: 200,
      animationEasingUpdate: 'exponentialOut',
      crossStyle: {
        color: '#999',
        width: 1,
        type: 'dashed',
        // TODO formatter
        textStyle: {} // lineStyle and shadowStyle should not be specified here,
        // otherwise it will always override those styles on option.axisPointer.

      }
    },
    textStyle: {
      color: '#fff',
      fontSize: 14
    }
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcE1vZGVsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcE1vZGVsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgZWNoYXJ0cyA9IHJlcXVpcmUoXCIuLi8uLi9lY2hhcnRzXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgX2RlZmF1bHQgPSBlY2hhcnRzLmV4dGVuZENvbXBvbmVudE1vZGVsKHtcbiAgdHlwZTogJ3Rvb2x0aXAnLFxuICBkZXBlbmRlbmNpZXM6IFsnYXhpc1BvaW50ZXInXSxcbiAgZGVmYXVsdE9wdGlvbjoge1xuICAgIHpsZXZlbDogMCxcbiAgICB6OiA2MCxcbiAgICBzaG93OiB0cnVlLFxuICAgIC8vIHRvb2x0aXDkuLvkvZPlhoXlrrlcbiAgICBzaG93Q29udGVudDogdHJ1ZSxcbiAgICAvLyAndHJpZ2dlcicgb25seSB3b3JrcyBvbiBjb29yZGluYXRlIHN5c3RlbS5cbiAgICAvLyAnaXRlbScgfCAnYXhpcycgfCAnbm9uZSdcbiAgICB0cmlnZ2VyOiAnaXRlbScsXG4gICAgLy8gJ2NsaWNrJyB8ICdtb3VzZW1vdmUnIHwgJ25vbmUnXG4gICAgdHJpZ2dlck9uOiAnbW91c2Vtb3ZlfGNsaWNrJyxcbiAgICBhbHdheXNTaG93Q29udGVudDogZmFsc2UsXG4gICAgZGlzcGxheU1vZGU6ICdzaW5nbGUnLFxuICAgIC8vICdzaW5nbGUnIHwgJ211bHRpcGxlQnlDb29yZFN5cydcbiAgICByZW5kZXJNb2RlOiAnYXV0bycsXG4gICAgLy8gJ2F1dG8nIHwgJ2h0bWwnIHwgJ3JpY2hUZXh0J1xuICAgIC8vICdhdXRvJzogdXNlIGh0bWwgYnkgZGVmYXVsdCwgYW5kIHVzZSBub24taHRtbCBpZiBgZG9jdW1lbnRgIGlzIG5vdCBkZWZpbmVkXG4gICAgLy8gJ2h0bWwnOiB1c2UgaHRtbCBmb3IgdG9vbHRpcFxuICAgIC8vICdyaWNoVGV4dCc6IHVzZSBjYW52YXMsIHN2ZywgYW5kIGV0Yy4gZm9yIHRvb2x0aXBcbiAgICAvLyDkvY3nva4ge0FycmF5fSB8IHtGdW5jdGlvbn1cbiAgICAvLyBwb3NpdGlvbjogbnVsbFxuICAgIC8vIENvbnNpZGVyIHRyaWdnZXJlZCBmcm9tIGF4aXNQb2ludGVyIGhhbmRsZSwgdmVydGljYWxBbGlnbiBzaG91bGQgYmUgJ21pZGRsZSdcbiAgICAvLyBhbGlnbjogbnVsbCxcbiAgICAvLyB2ZXJ0aWNhbEFsaWduOiBudWxsLFxuICAgIC8vIOaYr+WQpue6puadnyBjb250ZW50IOWcqCB2aWV3UmVjdCDkuK3jgILpu5jorqQgZmFsc2Ug5piv5Li65LqG5YW85a655Lul5YmN54mI5pys44CCXG4gICAgY29uZmluZTogZmFsc2UsXG4gICAgLy8g5YaF5a655qC85byP5Zmo77yae3N0cmluZ33vvIhUZW1wbGF0Ze+8iSDCpiB7RnVuY3Rpb259XG4gICAgLy8gZm9ybWF0dGVyOiBudWxsXG4gICAgc2hvd0RlbGF5OiAwLFxuICAgIC8vIOmakOiXj+W7tui/n++8jOWNleS9jW1zXG4gICAgaGlkZURlbGF5OiAxMDAsXG4gICAgLy8g5Yqo55S75Y+Y5o2i5pe26Ze077yM5Y2V5L2Nc1xuICAgIHRyYW5zaXRpb25EdXJhdGlvbjogMC40LFxuICAgIGVudGVyYWJsZTogZmFsc2UsXG4gICAgLy8g5o+Q56S66IOM5pmv6aKc6Imy77yM6buY6K6k5Li66YCP5piO5bqm5Li6MC4355qE6buR6ImyXG4gICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSg1MCw1MCw1MCwwLjcpJyxcbiAgICAvLyDmj5DnpLrovrnmoYbpopzoibJcbiAgICBib3JkZXJDb2xvcjogJyMzMzMnLFxuICAgIC8vIOaPkOekuui+ueahhuWchuinku+8jOWNleS9jXB477yM6buY6K6k5Li6NFxuICAgIGJvcmRlclJhZGl1czogNCxcbiAgICAvLyDmj5DnpLrovrnmoYbnur/lrr3vvIzljZXkvY1weO+8jOm7mOiupOS4ujDvvIjml6DovrnmoYbvvIlcbiAgICBib3JkZXJXaWR0aDogMCxcbiAgICAvLyDmj5DnpLrlhoXovrnot53vvIzljZXkvY1weO+8jOm7mOiupOWQhOaWueWQkeWGhei+uei3neS4ujXvvIxcbiAgICAvLyDmjqXlj5fmlbDnu4TliIbliKvorr7lrprkuIrlj7PkuIvlt6bovrnot53vvIzlkIxjc3NcbiAgICBwYWRkaW5nOiA1LFxuICAgIC8vIEV4dHJhIGNzcyB0ZXh0XG4gICAgZXh0cmFDc3NUZXh0OiAnJyxcbiAgICAvLyDlnZDmoIfovbTmjIfnpLrlmajvvIzlnZDmoIfovbTop6blj5HmnInmlYhcbiAgICBheGlzUG9pbnRlcjoge1xuICAgICAgLy8g6buY6K6k5Li655u057q/XG4gICAgICAvLyDlj6/pgInkuLrvvJonbGluZScgfCAnc2hhZG93JyB8ICdjcm9zcydcbiAgICAgIHR5cGU6ICdsaW5lJyxcbiAgICAgIC8vIHR5cGUg5Li6IGxpbmUg55qE5pe25YCZ5pyJ5pWI77yM5oyH5a6aIHRvb2x0aXAgbGluZSDmiYDlnKjnmoTovbTvvIzlj6/pgIlcbiAgICAgIC8vIOWPr+mAiSAneCcgfCAneScgfCAnYW5nbGUnIHwgJ3JhZGl1cycgfCAnYXV0bydcbiAgICAgIC8vIOm7mOiupCAnYXV0byfvvIzkvJrpgInmi6nnsbvlnovkuLogY2F0ZWdvcnkg55qE6L2077yM5a+55LqO5Y+M5pWw5YC86L2077yM56yb5Y2h5bCU5Z2Q5qCH57O75Lya6buY6K6k6YCJ5oupIHgg6L20XG4gICAgICAvLyDmnoHlnZDmoIfns7vkvJrpu5jorqTpgInmi6kgYW5nbGUg6L20XG4gICAgICBheGlzOiAnYXV0bycsXG4gICAgICBhbmltYXRpb246ICdhdXRvJyxcbiAgICAgIGFuaW1hdGlvbkR1cmF0aW9uVXBkYXRlOiAyMDAsXG4gICAgICBhbmltYXRpb25FYXNpbmdVcGRhdGU6ICdleHBvbmVudGlhbE91dCcsXG4gICAgICBjcm9zc1N0eWxlOiB7XG4gICAgICAgIGNvbG9yOiAnIzk5OScsXG4gICAgICAgIHdpZHRoOiAxLFxuICAgICAgICB0eXBlOiAnZGFzaGVkJyxcbiAgICAgICAgLy8gVE9ETyBmb3JtYXR0ZXJcbiAgICAgICAgdGV4dFN0eWxlOiB7fSAvLyBsaW5lU3R5bGUgYW5kIHNoYWRvd1N0eWxlIHNob3VsZCBub3QgYmUgc3BlY2lmaWVkIGhlcmUsXG4gICAgICAgIC8vIG90aGVyd2lzZSBpdCB3aWxsIGFsd2F5cyBvdmVycmlkZSB0aG9zZSBzdHlsZXMgb24gb3B0aW9uLmF4aXNQb2ludGVyLlxuXG4gICAgICB9XG4gICAgfSxcbiAgICB0ZXh0U3R5bGU6IHtcbiAgICAgIGNvbG9yOiAnI2ZmZicsXG4gICAgICBmb250U2l6ZTogMTRcbiAgICB9XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFaQTtBQXNCQTtBQUNBO0FBQ0E7QUFGQTtBQXRFQTtBQUhBO0FBQ0E7QUErRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/tooltip/TooltipModel.js
