/**
 * Copyright 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule ReactComponentWithPureRenderMixin
 */
var shallowEqual = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");

function shallowCompare(instance, nextProps, nextState) {
  return !shallowEqual(instance.props, nextProps) || !shallowEqual(instance.state, nextState);
}
/**
 * If your React component's render function is "pure", e.g. it will render the
 * same result given the same props and state, provide this mixin for a
 * considerable performance boost.
 *
 * Most React components have pure render functions.
 *
 * Example:
 *
 *   var ReactComponentWithPureRenderMixin =
 *     require('ReactComponentWithPureRenderMixin');
 *   React.createClass({
 *     mixins: [ReactComponentWithPureRenderMixin],
 *
 *     render: function() {
 *       return <div className={this.props.className}>foo</div>;
 *     }
 *   });
 *
 * Note: This only checks shallow equality for props and state. If these contain
 * complex data structures this mixin may have false-negatives for deeper
 * differences. Only mixin to components which have simple props and state, or
 * use `forceUpdate()` when you know deep data structures have changed.
 *
 * See https://facebook.github.io/react/docs/pure-render-mixin.html
 */


var ReactComponentWithPureRenderMixin = {
  shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }
};
module.exports = ReactComponentWithPureRenderMixin;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9lcy9QdXJlUmVuZGVyTWl4aW4uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11dGlsL2VzL1B1cmVSZW5kZXJNaXhpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgUmVhY3RDb21wb25lbnRXaXRoUHVyZVJlbmRlck1peGluXG4gKi9cbnZhciBzaGFsbG93RXF1YWwgPSByZXF1aXJlKCdzaGFsbG93ZXF1YWwnKTtcblxuZnVuY3Rpb24gc2hhbGxvd0NvbXBhcmUoaW5zdGFuY2UsIG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gIHJldHVybiAhc2hhbGxvd0VxdWFsKGluc3RhbmNlLnByb3BzLCBuZXh0UHJvcHMpIHx8ICFzaGFsbG93RXF1YWwoaW5zdGFuY2Uuc3RhdGUsIG5leHRTdGF0ZSk7XG59XG4vKipcbiAqIElmIHlvdXIgUmVhY3QgY29tcG9uZW50J3MgcmVuZGVyIGZ1bmN0aW9uIGlzIFwicHVyZVwiLCBlLmcuIGl0IHdpbGwgcmVuZGVyIHRoZVxuICogc2FtZSByZXN1bHQgZ2l2ZW4gdGhlIHNhbWUgcHJvcHMgYW5kIHN0YXRlLCBwcm92aWRlIHRoaXMgbWl4aW4gZm9yIGFcbiAqIGNvbnNpZGVyYWJsZSBwZXJmb3JtYW5jZSBib29zdC5cbiAqXG4gKiBNb3N0IFJlYWN0IGNvbXBvbmVudHMgaGF2ZSBwdXJlIHJlbmRlciBmdW5jdGlvbnMuXG4gKlxuICogRXhhbXBsZTpcbiAqXG4gKiAgIHZhciBSZWFjdENvbXBvbmVudFdpdGhQdXJlUmVuZGVyTWl4aW4gPVxuICogICAgIHJlcXVpcmUoJ1JlYWN0Q29tcG9uZW50V2l0aFB1cmVSZW5kZXJNaXhpbicpO1xuICogICBSZWFjdC5jcmVhdGVDbGFzcyh7XG4gKiAgICAgbWl4aW5zOiBbUmVhY3RDb21wb25lbnRXaXRoUHVyZVJlbmRlck1peGluXSxcbiAqXG4gKiAgICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcbiAqICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc05hbWV9PmZvbzwvZGl2PjtcbiAqICAgICB9XG4gKiAgIH0pO1xuICpcbiAqIE5vdGU6IFRoaXMgb25seSBjaGVja3Mgc2hhbGxvdyBlcXVhbGl0eSBmb3IgcHJvcHMgYW5kIHN0YXRlLiBJZiB0aGVzZSBjb250YWluXG4gKiBjb21wbGV4IGRhdGEgc3RydWN0dXJlcyB0aGlzIG1peGluIG1heSBoYXZlIGZhbHNlLW5lZ2F0aXZlcyBmb3IgZGVlcGVyXG4gKiBkaWZmZXJlbmNlcy4gT25seSBtaXhpbiB0byBjb21wb25lbnRzIHdoaWNoIGhhdmUgc2ltcGxlIHByb3BzIGFuZCBzdGF0ZSwgb3JcbiAqIHVzZSBgZm9yY2VVcGRhdGUoKWAgd2hlbiB5b3Uga25vdyBkZWVwIGRhdGEgc3RydWN0dXJlcyBoYXZlIGNoYW5nZWQuXG4gKlxuICogU2VlIGh0dHBzOi8vZmFjZWJvb2suZ2l0aHViLmlvL3JlYWN0L2RvY3MvcHVyZS1yZW5kZXItbWl4aW4uaHRtbFxuICovXG5cblxudmFyIFJlYWN0Q29tcG9uZW50V2l0aFB1cmVSZW5kZXJNaXhpbiA9IHtcbiAgc2hvdWxkQ29tcG9uZW50VXBkYXRlOiBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcbiAgICByZXR1cm4gc2hhbGxvd0NvbXBhcmUodGhpcywgbmV4dFByb3BzLCBuZXh0U3RhdGUpO1xuICB9XG59O1xubW9kdWxlLmV4cG9ydHMgPSBSZWFjdENvbXBvbmVudFdpdGhQdXJlUmVuZGVyTWl4aW47Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-util/es/PureRenderMixin.js
