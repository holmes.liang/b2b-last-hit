

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var mini_store_1 = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");

var react_lifecycles_compat_1 = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");

var shallowequal_1 = __importDefault(__webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js"));

var TableRow_1 = __importDefault(__webpack_require__(/*! ./TableRow */ "./node_modules/rc-table/es/TableRow.js"));

var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/rc-table/es/utils.js");

var ExpandableTable =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ExpandableTable, _React$Component);

  function ExpandableTable(props) {
    var _this;

    _classCallCheck(this, ExpandableTable);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ExpandableTable).call(this, props));

    _this.handleExpandChange = function (expanded, record, event, rowKey) {
      var destroy = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

      if (event) {
        event.stopPropagation();
      }

      var _this$props = _this.props,
          onExpandedRowsChange = _this$props.onExpandedRowsChange,
          onExpand = _this$props.onExpand;

      var _this$store$getState = _this.store.getState(),
          expandedRowKeys = _this$store$getState.expandedRowKeys;

      if (expanded) {
        // row was expanded
        expandedRowKeys = [].concat(_toConsumableArray(expandedRowKeys), [rowKey]);
      } else {
        // row was collapse
        var expandedRowIndex = expandedRowKeys.indexOf(rowKey);

        if (expandedRowIndex !== -1) {
          expandedRowKeys = utils_1.remove(expandedRowKeys, rowKey);
        }
      }

      if (!_this.props.expandedRowKeys) {
        _this.store.setState({
          expandedRowKeys: expandedRowKeys
        });
      } // De-dup of repeat call


      if (!_this.latestExpandedRows || !shallowequal_1.default(_this.latestExpandedRows, expandedRowKeys)) {
        _this.latestExpandedRows = expandedRowKeys;
        onExpandedRowsChange(expandedRowKeys);
      }

      if (!destroy) {
        onExpand(expanded, record);
      }
    };

    _this.renderExpandIndentCell = function (rows, fixed) {
      var _this$props2 = _this.props,
          prefixCls = _this$props2.prefixCls,
          expandIconAsCell = _this$props2.expandIconAsCell;

      if (!expandIconAsCell || fixed === 'right' || !rows.length) {
        return;
      }

      var iconColumn = {
        key: 'rc-table-expand-icon-cell',
        className: "".concat(prefixCls, "-expand-icon-th"),
        title: '',
        rowSpan: rows.length
      };
      rows[0].unshift(_objectSpread({}, iconColumn, {
        column: iconColumn
      }));
    };

    _this.renderRows = function (renderRows, rows, record, index, indent, fixed, parentKey, ancestorKeys) {
      var _this$props3 = _this.props,
          expandedRowClassName = _this$props3.expandedRowClassName,
          expandedRowRender = _this$props3.expandedRowRender,
          childrenColumnName = _this$props3.childrenColumnName;
      var childrenData = record[childrenColumnName];
      var nextAncestorKeys = [].concat(_toConsumableArray(ancestorKeys), [parentKey]);
      var nextIndent = indent + 1;

      if (expandedRowRender) {
        rows.push(_this.renderExpandedRow(record, index, expandedRowRender, expandedRowClassName(record, index, indent), nextAncestorKeys, nextIndent, fixed));
      }

      if (childrenData) {
        rows.push.apply(rows, _toConsumableArray(renderRows(childrenData, nextIndent, nextAncestorKeys)));
      }
    };

    var data = props.data,
        childrenColumnName = props.childrenColumnName,
        defaultExpandAllRows = props.defaultExpandAllRows,
        expandedRowKeys = props.expandedRowKeys,
        defaultExpandedRowKeys = props.defaultExpandedRowKeys,
        getRowKey = props.getRowKey;
    var finalExpandedRowKeys = [];

    var rows = _toConsumableArray(data);

    if (defaultExpandAllRows) {
      for (var i = 0; i < rows.length; i += 1) {
        var row = rows[i];
        finalExpandedRowKeys.push(getRowKey(row, i));
        rows = rows.concat(row[childrenColumnName] || []);
      }
    } else {
      finalExpandedRowKeys = expandedRowKeys || defaultExpandedRowKeys;
    }

    _this.columnManager = props.columnManager;
    _this.store = props.store;

    _this.store.setState({
      expandedRowsHeight: {},
      expandedRowKeys: finalExpandedRowKeys
    });

    return _this;
  }

  _createClass(ExpandableTable, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.handleUpdated();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if ('expandedRowKeys' in this.props) {
        this.store.setState({
          expandedRowKeys: this.props.expandedRowKeys
        });
      }

      this.handleUpdated();
    }
  }, {
    key: "handleUpdated",
    value: function handleUpdated() {
      /**
       * We should record latest expanded rows to avoid
       * multiple rows remove cause `onExpandedRowsChange` trigger many times
       */
      this.latestExpandedRows = null;
    }
  }, {
    key: "renderExpandedRow",
    value: function renderExpandedRow(record, index, _render, className, ancestorKeys, indent, fixed) {
      var _this2 = this;

      var _this$props4 = this.props,
          prefixCls = _this$props4.prefixCls,
          expandIconAsCell = _this$props4.expandIconAsCell,
          indentSize = _this$props4.indentSize;
      var parentKey = ancestorKeys[ancestorKeys.length - 1];
      var rowKey = "".concat(parentKey, "-extra-row");
      var components = {
        body: {
          row: 'tr',
          cell: 'td'
        }
      };
      var colCount;

      if (fixed === 'left') {
        colCount = this.columnManager.leftLeafColumns().length;
      } else if (fixed === 'right') {
        colCount = this.columnManager.rightLeafColumns().length;
      } else {
        colCount = this.columnManager.leafColumns().length;
      }

      var columns = [{
        key: 'extra-row',
        render: function render() {
          var _this2$store$getState = _this2.store.getState(),
              expandedRowKeys = _this2$store$getState.expandedRowKeys;

          var expanded = expandedRowKeys.includes(parentKey);
          return {
            props: {
              colSpan: colCount
            },
            children: fixed !== 'right' ? _render(record, index, indent, expanded) : '&nbsp;'
          };
        }
      }];

      if (expandIconAsCell && fixed !== 'right') {
        columns.unshift({
          key: 'expand-icon-placeholder',
          render: function render() {
            return null;
          }
        });
      }

      return React.createElement(TableRow_1.default, {
        key: rowKey,
        columns: columns,
        className: className,
        rowKey: rowKey,
        ancestorKeys: ancestorKeys,
        prefixCls: "".concat(prefixCls, "-expanded-row"),
        indentSize: indentSize,
        indent: indent,
        fixed: fixed,
        components: components,
        expandedRow: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props5 = this.props,
          data = _this$props5.data,
          childrenColumnName = _this$props5.childrenColumnName,
          children = _this$props5.children;
      var needIndentSpaced = data.some(function (record) {
        return record[childrenColumnName];
      });
      return children({
        props: this.props,
        needIndentSpaced: needIndentSpaced,
        renderRows: this.renderRows,
        handleExpandChange: this.handleExpandChange,
        renderExpandIndentCell: this.renderExpandIndentCell
      });
    }
  }]);

  return ExpandableTable;
}(React.Component);

ExpandableTable.defaultProps = {
  expandIconAsCell: false,
  expandedRowClassName: function expandedRowClassName() {
    return '';
  },
  expandIconColumnIndex: 0,
  defaultExpandAllRows: false,
  defaultExpandedRowKeys: [],
  childrenColumnName: 'children',
  indentSize: 15,
  onExpand: function onExpand() {},
  onExpandedRowsChange: function onExpandedRowsChange() {}
};
react_lifecycles_compat_1.polyfill(ExpandableTable);
exports.default = mini_store_1.connect()(ExpandableTable);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvRXhwYW5kYWJsZVRhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvRXhwYW5kYWJsZVRhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBfdHlwZW9mKG9iaikgeyBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9OyB9IGVsc2UgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07IH0gcmV0dXJuIF90eXBlb2Yob2JqKTsgfVxuXG5mdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSksIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX3RvQ29uc3VtYWJsZUFycmF5KGFycikgeyByZXR1cm4gX2FycmF5V2l0aG91dEhvbGVzKGFycikgfHwgX2l0ZXJhYmxlVG9BcnJheShhcnIpIHx8IF9ub25JdGVyYWJsZVNwcmVhZCgpOyB9XG5cbmZ1bmN0aW9uIF9ub25JdGVyYWJsZVNwcmVhZCgpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBzcHJlYWQgbm9uLWl0ZXJhYmxlIGluc3RhbmNlXCIpOyB9XG5cbmZ1bmN0aW9uIF9pdGVyYWJsZVRvQXJyYXkoaXRlcikgeyBpZiAoU3ltYm9sLml0ZXJhdG9yIGluIE9iamVjdChpdGVyKSB8fCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoaXRlcikgPT09IFwiW29iamVjdCBBcmd1bWVudHNdXCIpIHJldHVybiBBcnJheS5mcm9tKGl0ZXIpOyB9XG5cbmZ1bmN0aW9uIF9hcnJheVdpdGhvdXRIb2xlcyhhcnIpIHsgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkgeyBmb3IgKHZhciBpID0gMCwgYXJyMiA9IG5ldyBBcnJheShhcnIubGVuZ3RoKTsgaSA8IGFyci5sZW5ndGg7IGkrKykgeyBhcnIyW2ldID0gYXJyW2ldOyB9IHJldHVybiBhcnIyOyB9IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgbWluaV9zdG9yZV8xID0gcmVxdWlyZShcIm1pbmktc3RvcmVcIik7XG5cbnZhciByZWFjdF9saWZlY3ljbGVzX2NvbXBhdF8xID0gcmVxdWlyZShcInJlYWN0LWxpZmVjeWNsZXMtY29tcGF0XCIpO1xuXG52YXIgc2hhbGxvd2VxdWFsXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcInNoYWxsb3dlcXVhbFwiKSk7XG5cbnZhciBUYWJsZVJvd18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL1RhYmxlUm93XCIpKTtcblxudmFyIHV0aWxzXzEgPSByZXF1aXJlKFwiLi91dGlsc1wiKTtcblxudmFyIEV4cGFuZGFibGVUYWJsZSA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoRXhwYW5kYWJsZVRhYmxlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBFeHBhbmRhYmxlVGFibGUocHJvcHMpIHtcbiAgICB2YXIgX3RoaXM7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRXhwYW5kYWJsZVRhYmxlKTtcblxuICAgIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX2dldFByb3RvdHlwZU9mKEV4cGFuZGFibGVUYWJsZSkuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX3RoaXMuaGFuZGxlRXhwYW5kQ2hhbmdlID0gZnVuY3Rpb24gKGV4cGFuZGVkLCByZWNvcmQsIGV2ZW50LCByb3dLZXkpIHtcbiAgICAgIHZhciBkZXN0cm95ID0gYXJndW1lbnRzLmxlbmd0aCA+IDQgJiYgYXJndW1lbnRzWzRdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbNF0gOiBmYWxzZTtcblxuICAgICAgaWYgKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgfVxuXG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBvbkV4cGFuZGVkUm93c0NoYW5nZSA9IF90aGlzJHByb3BzLm9uRXhwYW5kZWRSb3dzQ2hhbmdlLFxuICAgICAgICAgIG9uRXhwYW5kID0gX3RoaXMkcHJvcHMub25FeHBhbmQ7XG5cbiAgICAgIHZhciBfdGhpcyRzdG9yZSRnZXRTdGF0ZSA9IF90aGlzLnN0b3JlLmdldFN0YXRlKCksXG4gICAgICAgICAgZXhwYW5kZWRSb3dLZXlzID0gX3RoaXMkc3RvcmUkZ2V0U3RhdGUuZXhwYW5kZWRSb3dLZXlzO1xuXG4gICAgICBpZiAoZXhwYW5kZWQpIHtcbiAgICAgICAgLy8gcm93IHdhcyBleHBhbmRlZFxuICAgICAgICBleHBhbmRlZFJvd0tleXMgPSBbXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KGV4cGFuZGVkUm93S2V5cyksIFtyb3dLZXldKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIHJvdyB3YXMgY29sbGFwc2VcbiAgICAgICAgdmFyIGV4cGFuZGVkUm93SW5kZXggPSBleHBhbmRlZFJvd0tleXMuaW5kZXhPZihyb3dLZXkpO1xuXG4gICAgICAgIGlmIChleHBhbmRlZFJvd0luZGV4ICE9PSAtMSkge1xuICAgICAgICAgIGV4cGFuZGVkUm93S2V5cyA9IHV0aWxzXzEucmVtb3ZlKGV4cGFuZGVkUm93S2V5cywgcm93S2V5KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoIV90aGlzLnByb3BzLmV4cGFuZGVkUm93S2V5cykge1xuICAgICAgICBfdGhpcy5zdG9yZS5zZXRTdGF0ZSh7XG4gICAgICAgICAgZXhwYW5kZWRSb3dLZXlzOiBleHBhbmRlZFJvd0tleXNcbiAgICAgICAgfSk7XG4gICAgICB9IC8vIERlLWR1cCBvZiByZXBlYXQgY2FsbFxuXG5cbiAgICAgIGlmICghX3RoaXMubGF0ZXN0RXhwYW5kZWRSb3dzIHx8ICFzaGFsbG93ZXF1YWxfMS5kZWZhdWx0KF90aGlzLmxhdGVzdEV4cGFuZGVkUm93cywgZXhwYW5kZWRSb3dLZXlzKSkge1xuICAgICAgICBfdGhpcy5sYXRlc3RFeHBhbmRlZFJvd3MgPSBleHBhbmRlZFJvd0tleXM7XG4gICAgICAgIG9uRXhwYW5kZWRSb3dzQ2hhbmdlKGV4cGFuZGVkUm93S2V5cyk7XG4gICAgICB9XG5cbiAgICAgIGlmICghZGVzdHJveSkge1xuICAgICAgICBvbkV4cGFuZChleHBhbmRlZCwgcmVjb3JkKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMucmVuZGVyRXhwYW5kSW5kZW50Q2VsbCA9IGZ1bmN0aW9uIChyb3dzLCBmaXhlZCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzMi5wcmVmaXhDbHMsXG4gICAgICAgICAgZXhwYW5kSWNvbkFzQ2VsbCA9IF90aGlzJHByb3BzMi5leHBhbmRJY29uQXNDZWxsO1xuXG4gICAgICBpZiAoIWV4cGFuZEljb25Bc0NlbGwgfHwgZml4ZWQgPT09ICdyaWdodCcgfHwgIXJvd3MubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGljb25Db2x1bW4gPSB7XG4gICAgICAgIGtleTogJ3JjLXRhYmxlLWV4cGFuZC1pY29uLWNlbGwnLFxuICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItZXhwYW5kLWljb24tdGhcIiksXG4gICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgcm93U3Bhbjogcm93cy5sZW5ndGhcbiAgICAgIH07XG4gICAgICByb3dzWzBdLnVuc2hpZnQoX29iamVjdFNwcmVhZCh7fSwgaWNvbkNvbHVtbiwge1xuICAgICAgICBjb2x1bW46IGljb25Db2x1bW5cbiAgICAgIH0pKTtcbiAgICB9O1xuXG4gICAgX3RoaXMucmVuZGVyUm93cyA9IGZ1bmN0aW9uIChyZW5kZXJSb3dzLCByb3dzLCByZWNvcmQsIGluZGV4LCBpbmRlbnQsIGZpeGVkLCBwYXJlbnRLZXksIGFuY2VzdG9yS2V5cykge1xuICAgICAgdmFyIF90aGlzJHByb3BzMyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGV4cGFuZGVkUm93Q2xhc3NOYW1lID0gX3RoaXMkcHJvcHMzLmV4cGFuZGVkUm93Q2xhc3NOYW1lLFxuICAgICAgICAgIGV4cGFuZGVkUm93UmVuZGVyID0gX3RoaXMkcHJvcHMzLmV4cGFuZGVkUm93UmVuZGVyLFxuICAgICAgICAgIGNoaWxkcmVuQ29sdW1uTmFtZSA9IF90aGlzJHByb3BzMy5jaGlsZHJlbkNvbHVtbk5hbWU7XG4gICAgICB2YXIgY2hpbGRyZW5EYXRhID0gcmVjb3JkW2NoaWxkcmVuQ29sdW1uTmFtZV07XG4gICAgICB2YXIgbmV4dEFuY2VzdG9yS2V5cyA9IFtdLmNvbmNhdChfdG9Db25zdW1hYmxlQXJyYXkoYW5jZXN0b3JLZXlzKSwgW3BhcmVudEtleV0pO1xuICAgICAgdmFyIG5leHRJbmRlbnQgPSBpbmRlbnQgKyAxO1xuXG4gICAgICBpZiAoZXhwYW5kZWRSb3dSZW5kZXIpIHtcbiAgICAgICAgcm93cy5wdXNoKF90aGlzLnJlbmRlckV4cGFuZGVkUm93KHJlY29yZCwgaW5kZXgsIGV4cGFuZGVkUm93UmVuZGVyLCBleHBhbmRlZFJvd0NsYXNzTmFtZShyZWNvcmQsIGluZGV4LCBpbmRlbnQpLCBuZXh0QW5jZXN0b3JLZXlzLCBuZXh0SW5kZW50LCBmaXhlZCkpO1xuICAgICAgfVxuXG4gICAgICBpZiAoY2hpbGRyZW5EYXRhKSB7XG4gICAgICAgIHJvd3MucHVzaC5hcHBseShyb3dzLCBfdG9Db25zdW1hYmxlQXJyYXkocmVuZGVyUm93cyhjaGlsZHJlbkRhdGEsIG5leHRJbmRlbnQsIG5leHRBbmNlc3RvcktleXMpKSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIHZhciBkYXRhID0gcHJvcHMuZGF0YSxcbiAgICAgICAgY2hpbGRyZW5Db2x1bW5OYW1lID0gcHJvcHMuY2hpbGRyZW5Db2x1bW5OYW1lLFxuICAgICAgICBkZWZhdWx0RXhwYW5kQWxsUm93cyA9IHByb3BzLmRlZmF1bHRFeHBhbmRBbGxSb3dzLFxuICAgICAgICBleHBhbmRlZFJvd0tleXMgPSBwcm9wcy5leHBhbmRlZFJvd0tleXMsXG4gICAgICAgIGRlZmF1bHRFeHBhbmRlZFJvd0tleXMgPSBwcm9wcy5kZWZhdWx0RXhwYW5kZWRSb3dLZXlzLFxuICAgICAgICBnZXRSb3dLZXkgPSBwcm9wcy5nZXRSb3dLZXk7XG4gICAgdmFyIGZpbmFsRXhwYW5kZWRSb3dLZXlzID0gW107XG5cbiAgICB2YXIgcm93cyA9IF90b0NvbnN1bWFibGVBcnJheShkYXRhKTtcblxuICAgIGlmIChkZWZhdWx0RXhwYW5kQWxsUm93cykge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCByb3dzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgIHZhciByb3cgPSByb3dzW2ldO1xuICAgICAgICBmaW5hbEV4cGFuZGVkUm93S2V5cy5wdXNoKGdldFJvd0tleShyb3csIGkpKTtcbiAgICAgICAgcm93cyA9IHJvd3MuY29uY2F0KHJvd1tjaGlsZHJlbkNvbHVtbk5hbWVdIHx8IFtdKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgZmluYWxFeHBhbmRlZFJvd0tleXMgPSBleHBhbmRlZFJvd0tleXMgfHwgZGVmYXVsdEV4cGFuZGVkUm93S2V5cztcbiAgICB9XG5cbiAgICBfdGhpcy5jb2x1bW5NYW5hZ2VyID0gcHJvcHMuY29sdW1uTWFuYWdlcjtcbiAgICBfdGhpcy5zdG9yZSA9IHByb3BzLnN0b3JlO1xuXG4gICAgX3RoaXMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgZXhwYW5kZWRSb3dzSGVpZ2h0OiB7fSxcbiAgICAgIGV4cGFuZGVkUm93S2V5czogZmluYWxFeHBhbmRlZFJvd0tleXNcbiAgICB9KTtcblxuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhFeHBhbmRhYmxlVGFibGUsIFt7XG4gICAga2V5OiBcImNvbXBvbmVudERpZE1vdW50XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5oYW5kbGVVcGRhdGVkKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImNvbXBvbmVudERpZFVwZGF0ZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICBpZiAoJ2V4cGFuZGVkUm93S2V5cycgaW4gdGhpcy5wcm9wcykge1xuICAgICAgICB0aGlzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICBleHBhbmRlZFJvd0tleXM6IHRoaXMucHJvcHMuZXhwYW5kZWRSb3dLZXlzXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmhhbmRsZVVwZGF0ZWQoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiaGFuZGxlVXBkYXRlZFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYW5kbGVVcGRhdGVkKCkge1xuICAgICAgLyoqXG4gICAgICAgKiBXZSBzaG91bGQgcmVjb3JkIGxhdGVzdCBleHBhbmRlZCByb3dzIHRvIGF2b2lkXG4gICAgICAgKiBtdWx0aXBsZSByb3dzIHJlbW92ZSBjYXVzZSBgb25FeHBhbmRlZFJvd3NDaGFuZ2VgIHRyaWdnZXIgbWFueSB0aW1lc1xuICAgICAgICovXG4gICAgICB0aGlzLmxhdGVzdEV4cGFuZGVkUm93cyA9IG51bGw7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlckV4cGFuZGVkUm93XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckV4cGFuZGVkUm93KHJlY29yZCwgaW5kZXgsIF9yZW5kZXIsIGNsYXNzTmFtZSwgYW5jZXN0b3JLZXlzLCBpbmRlbnQsIGZpeGVkKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgdmFyIF90aGlzJHByb3BzNCA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHM0LnByZWZpeENscyxcbiAgICAgICAgICBleHBhbmRJY29uQXNDZWxsID0gX3RoaXMkcHJvcHM0LmV4cGFuZEljb25Bc0NlbGwsXG4gICAgICAgICAgaW5kZW50U2l6ZSA9IF90aGlzJHByb3BzNC5pbmRlbnRTaXplO1xuICAgICAgdmFyIHBhcmVudEtleSA9IGFuY2VzdG9yS2V5c1thbmNlc3RvcktleXMubGVuZ3RoIC0gMV07XG4gICAgICB2YXIgcm93S2V5ID0gXCJcIi5jb25jYXQocGFyZW50S2V5LCBcIi1leHRyYS1yb3dcIik7XG4gICAgICB2YXIgY29tcG9uZW50cyA9IHtcbiAgICAgICAgYm9keToge1xuICAgICAgICAgIHJvdzogJ3RyJyxcbiAgICAgICAgICBjZWxsOiAndGQnXG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgICB2YXIgY29sQ291bnQ7XG5cbiAgICAgIGlmIChmaXhlZCA9PT0gJ2xlZnQnKSB7XG4gICAgICAgIGNvbENvdW50ID0gdGhpcy5jb2x1bW5NYW5hZ2VyLmxlZnRMZWFmQ29sdW1ucygpLmxlbmd0aDtcbiAgICAgIH0gZWxzZSBpZiAoZml4ZWQgPT09ICdyaWdodCcpIHtcbiAgICAgICAgY29sQ291bnQgPSB0aGlzLmNvbHVtbk1hbmFnZXIucmlnaHRMZWFmQ29sdW1ucygpLmxlbmd0aDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbENvdW50ID0gdGhpcy5jb2x1bW5NYW5hZ2VyLmxlYWZDb2x1bW5zKCkubGVuZ3RoO1xuICAgICAgfVxuXG4gICAgICB2YXIgY29sdW1ucyA9IFt7XG4gICAgICAgIGtleTogJ2V4dHJhLXJvdycsXG4gICAgICAgIHJlbmRlcjogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICAgIHZhciBfdGhpczIkc3RvcmUkZ2V0U3RhdGUgPSBfdGhpczIuc3RvcmUuZ2V0U3RhdGUoKSxcbiAgICAgICAgICAgICAgZXhwYW5kZWRSb3dLZXlzID0gX3RoaXMyJHN0b3JlJGdldFN0YXRlLmV4cGFuZGVkUm93S2V5cztcblxuICAgICAgICAgIHZhciBleHBhbmRlZCA9IGV4cGFuZGVkUm93S2V5cy5pbmNsdWRlcyhwYXJlbnRLZXkpO1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBwcm9wczoge1xuICAgICAgICAgICAgICBjb2xTcGFuOiBjb2xDb3VudFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNoaWxkcmVuOiBmaXhlZCAhPT0gJ3JpZ2h0JyA/IF9yZW5kZXIocmVjb3JkLCBpbmRleCwgaW5kZW50LCBleHBhbmRlZCkgOiAnJm5ic3A7J1xuICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgIH1dO1xuXG4gICAgICBpZiAoZXhwYW5kSWNvbkFzQ2VsbCAmJiBmaXhlZCAhPT0gJ3JpZ2h0Jykge1xuICAgICAgICBjb2x1bW5zLnVuc2hpZnQoe1xuICAgICAgICAgIGtleTogJ2V4cGFuZC1pY29uLXBsYWNlaG9sZGVyJyxcbiAgICAgICAgICByZW5kZXI6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFRhYmxlUm93XzEuZGVmYXVsdCwge1xuICAgICAgICBrZXk6IHJvd0tleSxcbiAgICAgICAgY29sdW1uczogY29sdW1ucyxcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgIHJvd0tleTogcm93S2V5LFxuICAgICAgICBhbmNlc3RvcktleXM6IGFuY2VzdG9yS2V5cyxcbiAgICAgICAgcHJlZml4Q2xzOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWV4cGFuZGVkLXJvd1wiKSxcbiAgICAgICAgaW5kZW50U2l6ZTogaW5kZW50U2l6ZSxcbiAgICAgICAgaW5kZW50OiBpbmRlbnQsXG4gICAgICAgIGZpeGVkOiBmaXhlZCxcbiAgICAgICAgY29tcG9uZW50czogY29tcG9uZW50cyxcbiAgICAgICAgZXhwYW5kZWRSb3c6IHRydWVcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJyZW5kZXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNSA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgZGF0YSA9IF90aGlzJHByb3BzNS5kYXRhLFxuICAgICAgICAgIGNoaWxkcmVuQ29sdW1uTmFtZSA9IF90aGlzJHByb3BzNS5jaGlsZHJlbkNvbHVtbk5hbWUsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfdGhpcyRwcm9wczUuY2hpbGRyZW47XG4gICAgICB2YXIgbmVlZEluZGVudFNwYWNlZCA9IGRhdGEuc29tZShmdW5jdGlvbiAocmVjb3JkKSB7XG4gICAgICAgIHJldHVybiByZWNvcmRbY2hpbGRyZW5Db2x1bW5OYW1lXTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIGNoaWxkcmVuKHtcbiAgICAgICAgcHJvcHM6IHRoaXMucHJvcHMsXG4gICAgICAgIG5lZWRJbmRlbnRTcGFjZWQ6IG5lZWRJbmRlbnRTcGFjZWQsXG4gICAgICAgIHJlbmRlclJvd3M6IHRoaXMucmVuZGVyUm93cyxcbiAgICAgICAgaGFuZGxlRXhwYW5kQ2hhbmdlOiB0aGlzLmhhbmRsZUV4cGFuZENoYW5nZSxcbiAgICAgICAgcmVuZGVyRXhwYW5kSW5kZW50Q2VsbDogdGhpcy5yZW5kZXJFeHBhbmRJbmRlbnRDZWxsXG4gICAgICB9KTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gRXhwYW5kYWJsZVRhYmxlO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5FeHBhbmRhYmxlVGFibGUuZGVmYXVsdFByb3BzID0ge1xuICBleHBhbmRJY29uQXNDZWxsOiBmYWxzZSxcbiAgZXhwYW5kZWRSb3dDbGFzc05hbWU6IGZ1bmN0aW9uIGV4cGFuZGVkUm93Q2xhc3NOYW1lKCkge1xuICAgIHJldHVybiAnJztcbiAgfSxcbiAgZXhwYW5kSWNvbkNvbHVtbkluZGV4OiAwLFxuICBkZWZhdWx0RXhwYW5kQWxsUm93czogZmFsc2UsXG4gIGRlZmF1bHRFeHBhbmRlZFJvd0tleXM6IFtdLFxuICBjaGlsZHJlbkNvbHVtbk5hbWU6ICdjaGlsZHJlbicsXG4gIGluZGVudFNpemU6IDE1LFxuICBvbkV4cGFuZDogZnVuY3Rpb24gb25FeHBhbmQoKSB7fSxcbiAgb25FeHBhbmRlZFJvd3NDaGFuZ2U6IGZ1bmN0aW9uIG9uRXhwYW5kZWRSb3dzQ2hhbmdlKCkge31cbn07XG5yZWFjdF9saWZlY3ljbGVzX2NvbXBhdF8xLnBvbHlmaWxsKEV4cGFuZGFibGVUYWJsZSk7XG5leHBvcnRzLmRlZmF1bHQgPSBtaW5pX3N0b3JlXzEuY29ubmVjdCgpKEV4cGFuZGFibGVUYWJsZSk7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBTUE7QUFiQTtBQUNBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQWpFQTtBQW1FQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBakJBO0FBQ0E7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/ExpandableTable.js
