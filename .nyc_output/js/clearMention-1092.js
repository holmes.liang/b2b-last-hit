__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return clearMention; });
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _getSearchWord__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./getSearchWord */ "./node_modules/rc-editor-mention/es/utils/getSearchWord.js");


function clearMention(editorState) {
  var selection = editorState.getSelection();
  var searchWord = Object(_getSearchWord__WEBPACK_IMPORTED_MODULE_1__["default"])(editorState, selection);
  var begin = searchWord.begin,
      end = searchWord.end;
  var replacedContent = draft_js__WEBPACK_IMPORTED_MODULE_0__["Modifier"].replaceText(editorState.getCurrentContent(), selection.merge({
    anchorOffset: begin,
    focusOffset: end
  }), '', null);
  var InsertSpaceContent = draft_js__WEBPACK_IMPORTED_MODULE_0__["Modifier"].insertText(replacedContent, replacedContent.getSelectionAfter(), ' ');
  var newEditorState = draft_js__WEBPACK_IMPORTED_MODULE_0__["EditorState"].push(editorState, InsertSpaceContent, 'insert-mention');
  return draft_js__WEBPACK_IMPORTED_MODULE_0__["EditorState"].forceSelection(newEditorState, InsertSpaceContent.getSelectionAfter());
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvY2xlYXJNZW50aW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvY2xlYXJNZW50aW9uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZGlmaWVyLCBFZGl0b3JTdGF0ZSB9IGZyb20gJ2RyYWZ0LWpzJztcbmltcG9ydCBnZXRTZWFyY2hXb3JkIGZyb20gJy4vZ2V0U2VhcmNoV29yZCc7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGNsZWFyTWVudGlvbihlZGl0b3JTdGF0ZSkge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBzZWFyY2hXb3JkID0gZ2V0U2VhcmNoV29yZChlZGl0b3JTdGF0ZSwgc2VsZWN0aW9uKTtcbiAgdmFyIGJlZ2luID0gc2VhcmNoV29yZC5iZWdpbixcbiAgICAgIGVuZCA9IHNlYXJjaFdvcmQuZW5kO1xuXG4gIHZhciByZXBsYWNlZENvbnRlbnQgPSBNb2RpZmllci5yZXBsYWNlVGV4dChlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBzZWxlY3Rpb24ubWVyZ2Uoe1xuICAgIGFuY2hvck9mZnNldDogYmVnaW4sXG4gICAgZm9jdXNPZmZzZXQ6IGVuZFxuICB9KSwgJycsIG51bGwpO1xuXG4gIHZhciBJbnNlcnRTcGFjZUNvbnRlbnQgPSBNb2RpZmllci5pbnNlcnRUZXh0KHJlcGxhY2VkQ29udGVudCwgcmVwbGFjZWRDb250ZW50LmdldFNlbGVjdGlvbkFmdGVyKCksICcgJyk7XG5cbiAgdmFyIG5ld0VkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgSW5zZXJ0U3BhY2VDb250ZW50LCAnaW5zZXJ0LW1lbnRpb24nKTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKG5ld0VkaXRvclN0YXRlLCBJbnNlcnRTcGFjZUNvbnRlbnQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSk7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBRUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/clearMention.js
