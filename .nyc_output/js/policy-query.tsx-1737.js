__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _common_apis__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common/apis */ "./src/common/apis.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @common/utils */ "./src/common/utils.tsx");
/* harmony import */ var _common_authority__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @common/authority */ "./src/common/authority.tsx");
/* harmony import */ var _component_filter__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../component/filter */ "./src/app/desk/component/filter/index.tsx");
/* harmony import */ var _policy_query_style__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./policy-query-style */ "./src/app/desk/quote/query/policy-query-style.tsx");
/* harmony import */ var _admin_common_QueryPage__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../admin/common/QueryPage */ "./src/app/desk/admin/common/QueryPage.tsx");
/* harmony import */ var _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../component/filter/date-range-util */ "./src/app/desk/component/filter/date-range-util.tsx");
/* harmony import */ var _component_query_prod_type__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../component/query/prod-type */ "./src/app/desk/component/query/prod-type.tsx");
/* harmony import */ var _admin_common_filters__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../../admin/common/filters */ "./src/app/desk/admin/common/filters.tsx");
/* harmony import */ var _components_query_opers__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/query-opers */ "./src/app/desk/quote/query/components/query-opers.tsx");
/* harmony import */ var _desk_component_hoc_query_refresh__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @desk-component/hoc/query-refresh */ "./src/app/desk/component/hoc/query-refresh.tsx");









var _defaultSearchCriteri,
    _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/query/policy-query.tsx";


















var isMobile = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].getIsMobile();
var pendingTab = "pending",
    issuedTab = "issued",
    cancelledTab = "cancelled",
    allTab = "all";
var defaultSearchCriteria = (_defaultSearchCriteri = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(_defaultSearchCriteri, pendingTab, {
  quoteStatuses: [_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.BOUND, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.IN_PGRES]
}), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(_defaultSearchCriteri, issuedTab, {
  quoteStatuses: [_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.ISSUED, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.CLOSED]
}), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(_defaultSearchCriteri, cancelledTab, {
  quoteStatuses: [_common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.EXP, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.CANCELLED, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.LAPSED, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.UW_DECLINE, _common__WEBPACK_IMPORTED_MODULE_14__["Consts"].QUOTE_STATUS.REJECTED]
}), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_7__["default"])(_defaultSearchCriteri, allTab, {
  quoteStatuses: []
}), _defaultSearchCriteri);

var PolicyQuery =
/*#__PURE__*/
function (_DeskPage) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(PolicyQuery, _DeskPage);

  function PolicyQuery(props, context) {
    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, PolicyQuery);

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(PolicyQuery).call(this, props, context));
    _this2.queryPage = {};
    _this2.queryTabs = [{
      key: pendingTab,
      name: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Pending").thai("รอดำเนินการ").getMessage()
    }, {
      key: issuedTab,
      name: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Issued").thai("ออกกรมธรรม์").getMessage()
    }, {
      key: cancelledTab,
      name: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Cancelled").thai("ยกเลิกกรมธรรม์").getMessage()
    }, {
      key: allTab,
      name: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("All").thai("ทั้งหมด").getMessage()
    }];

    _this2.initQueryPageRef = function (ref) {
      _this2.queryPage = ref;
    };

    _this2.initQueryParams = function () {
      var _ref = _common_utils__WEBPACK_IMPORTED_MODULE_15__["default"].getParamsFromUrl() || {},
          _ref$structId = _ref.structId,
          structId = _ref$structId === void 0 ? "" : _ref$structId,
          _ref$tenantCode = _ref.tenantCode,
          tenantCode = _ref$tenantCode === void 0 ? "" : _ref$tenantCode;

      var isPlatform = _common_authority__WEBPACK_IMPORTED_MODULE_16__["default"].isPlatform();
      var tenantCodeN = _common_authority__WEBPACK_IMPORTED_MODULE_16__["default"].getTenantCode();
      var initTenantCode = tenantCode !== "" ? tenantCode : isPlatform ? "" : tenantCodeN;
      var initParams = {
        structId: structId,
        tenantCode: initTenantCode
      };
      return initParams;
    };

    _this2.onRejectPolicy = function (policy) {
      if (policy.bizType === "COMP") {
        _this2.getHelpers().getAjax().patch(_common__WEBPACK_IMPORTED_MODULE_14__["Apis"].COMP_QUOTE_REJECT.replace(":compQuoteId", policy.refId), {}).then(function (response) {
          _this2.queryPage.refreshCardList(defaultSearchCriteria[_this2.state.currentTab], {
            pageIndex: 1,
            pageSize: 5
          });
        });
      } else {
        _this2.getHelpers().getAjax().post(_common__WEBPACK_IMPORTED_MODULE_14__["Apis"].POLICY_REJECT.replace(":policyId", policy.refId), {}).then(function (response) {
          _this2.queryPage.refreshCardList(defaultSearchCriteria[_this2.state.currentTab], {
            pageIndex: 1,
            pageSize: 5
          });
        });
      }
    };

    _this2.handleRejectPolicy = function (policy) {
      antd__WEBPACK_IMPORTED_MODULE_10__["Modal"].confirm({
        content: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Customer payment might be in progress. Are you sure to reject the quotation?").thai("Customer payment might be in progress. Are you sure to reject the quotation?").getMessage(),
        onOk: function onOk() {
          _this2.onRejectPolicy(policy);
        },
        okText: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Yes").thai("ใช่").getMessage(),
        cancelText: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("No").thai("ไม่").getMessage()
      });
    };

    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(PolicyQuery, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(PolicyQuery.prototype), "initComponents", this).call(this), {});
    }
  }, {
    key: "initState",
    value: function initState() {
      var _ref2 = this.props.history || {
        action: "",
        location: {}
      },
          action = _ref2.action,
          location = _ref2.location;

      var currentTab;
      var params = new URLSearchParams(window.location.search);
      var paramCurrentTab = params.get("currentTab");

      if (action === "PUSH" && lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(location, "state.currentTab")) {
        currentTab = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(location, "state.currentTab");
      } else if (paramCurrentTab) {
        currentTab = paramCurrentTab;
      } else {
        currentTab = pendingTab;
      }

      return {
        currentTab: currentTab
      };
    } // renders

  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      var _this3 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_policy_query_style__WEBPACK_IMPORTED_MODULE_18__["default"].Container, {
        "data-widget": "page-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Tabs"], {
        activeKey: this.state.currentTab,
        onChange: function onChange(tab) {
          _this3.setState({
            currentTab: tab
          }); //after change tab, need clear selected search criteria


          _this3.queryPage.resetFilterValues();

          setTimeout(function () {
            _this3.queryPage.fetchData(_common_apis__WEBPACK_IMPORTED_MODULE_13__["default"].POLICIES_QUERY, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, defaultSearchCriteria[tab], {}, {
              pageIndex: 1,
              pageSize: 5
            }));
          }, 20);
        },
        className: "ant-tabs--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, this.queryTabs.map(function (item) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Tabs"].TabPane, {
          tab: item.name,
          key: item.key,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 140
          },
          __self: this
        });
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_admin_common_QueryPage__WEBPACK_IMPORTED_MODULE_19__["default"], {
        ref: this.initQueryPageRef,
        itemKey: "refId",
        filter: this.getFilter(),
        columns: isMobile ? this.getColumnsMobile() : this.getColumns(),
        apiUrl: _common_apis__WEBPACK_IMPORTED_MODULE_13__["default"].POLICIES_QUERY,
        onBeforeRefresh: function onBeforeRefresh(searchCriteria) {
          if (!!searchCriteria.ctntStructIds) {
            searchCriteria.ctntStructIds = [searchCriteria.ctntStructIds];
          }

          return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, defaultSearchCriteria[_this3.state.currentTab], {}, searchCriteria);
        },
        renderCardHead: function renderCardHead(item) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Row"], {
            key: item.refId,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 157
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Col"], {
            className: "col-title",
            style: {
              float: "left"
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 158
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_component_query_prod_type__WEBPACK_IMPORTED_MODULE_21__["ProdTypeImage"], {
            productCate: item.productCate,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 159
            },
            __self: this
          }), item.refNo, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
            className: "itnt-badge",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 161
            },
            __self: this
          }, "".concat(item.bizType))), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_10__["Col"], {
            className: "text--align-right",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 163
            },
            __self: this
          }, "".concat(item.ctntStructCode || "", " \n                    ").concat(item.ctntStructCode && item.ctntStructName ? "-" : "", " \n                    ").concat(item.ctntStructName || "")));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      })));
    }
  }, {
    key: "renderItntLogo",
    value: function renderItntLogo(item) {
      var itntLogoUrl = item.itntLogoUrl;

      if (!itntLogoUrl) {
        return null;
      }

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isBroker() || _common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isPlatform() || _common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isD2C()) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("img", {
          src: itntLogoUrl,
          alt: "",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 183
          },
          __self: this
        });
      }

      return null;
    }
  }, {
    key: "renderPolicyMainInfo",
    value: function renderPolicyMainInfo(row) {
      var phStr = (row.phName ? "".concat(row.phName, ", ") : "") + (row.phMobile ? "".concat(row.phMobile, ", ") : "");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "policy-main-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "itnt-logo-wrap",
        style: this.renderItntLogo(row) ? {} : {
          paddingLeft: 10
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 193
        },
        __self: this
      }, this.renderItntLogo(row), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        className: "title max-limit--22",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 199
        },
        __self: this
      }, !row.insuredDescription || row.insuredDescription === "null" ? "" : row.insuredDescription), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        className: "title--sub",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      }, [row.productName, row.planName].filter(function (v, i, a) {
        return v && a.indexOf(v) === i;
      }).join(", ")), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        className: "title--sub",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, phStr ? phStr.substring(0, phStr.length - 2) : "-")));
    }
  }, {
    key: "getColumns",
    value: function getColumns() {
      var _this = this;

      return [{
        style: {
          width: "32%"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 216
            },
            __self: this
          }, _this.renderPolicyMainInfo(row));
        }
      }, {
        style: {
          width: "20%",
          textAlign: "right"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 222
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].renderPolicyPremium(row));
        }
      }, {
        style: {
          width: "28%",
          textAlign: "center"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 229
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("strong", {
            style: {
              fontWeight: 700
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 230
            },
            __self: this
          }, row.statusName), row.subStatusName && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
            style: {
              fontSize: 12,
              color: "#999"
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 231
            },
            __self: this
          }, row.subStatusName), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
            className: "title--sub",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 232
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].dateTimeFromNow(row.lastUpdatedAt)));
        }
      }, {
        style: {
          width: "20%",
          textAlign: "center"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 240
            },
            __self: this
          }, Object(_components_query_opers__WEBPACK_IMPORTED_MODULE_23__["renderOpers"])(row, _this.getHelpers().getRouter(), _this.handleRejectPolicy, _this));
        }
      }];
    }
  }, {
    key: "getColumnsMobile",
    value: function getColumnsMobile() {
      var _this = this;

      var isLeft = _common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isBroker() || _common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isPlatform() || _common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isD2C();
      return [{
        style: {
          width: "100%"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 253
            },
            __self: this
          }, _this.renderPolicyMainInfo(row));
        }
      }, {
        style: {
          width: "100%",
          textAlign: "left",
          marginLeft: isLeft ? "65px" : "10px"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 259
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].renderPolicyPremium(row));
        }
      }, {
        style: {
          width: "100%",
          textAlign: "left",
          marginLeft: isLeft ? "65px" : "10px"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 267
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("strong", {
            style: {
              fontWeight: 700
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 269
            },
            __self: this
          }, row.statusName), row.subStatusName && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
            style: {
              fontSize: 12,
              color: "#999"
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 270
            },
            __self: this
          }, row.subStatusName)), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
            className: "title--sub",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 273
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].dateTimeFromNow(row.lastUpdatedAt)));
        }
      }, {
        style: {
          width: "100%",
          textAlign: "left",
          marginLeft: isLeft ? "65px" : "10px"
        },
        render: function render(value, row) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 281
            },
            __self: this
          }, Object(_components_query_opers__WEBPACK_IMPORTED_MODULE_23__["renderOpers"])(row, _this.getHelpers().getRouter(), _this.handleRejectPolicy, _this));
        }
      }];
    }
  }, {
    key: "getFilter",
    value: function getFilter() {
      var _this$initQueryParams = this.initQueryParams(),
          tenantCode = _this$initQueryParams.tenantCode;

      var tags = {
        type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].CHECKBOX,
        props: {
          label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("TAGS").thai("ขั้นตอน").getMessage(),
          field: "tags",
          multiple: true,
          options: [{
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Inception Cancel").thai("ยกเลิกก่อนความคุ้มครอง").getMessage(),
            value: "INCEPTION_CANCEL"
          }, {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Midway Cancel").thai("ยกเลิกจากการขาดชำระ").getMessage(),
            value: "MIDWAY_CANCEL"
          }]
        }
      };
      return {
        items: [_admin_common_filters__WEBPACK_IMPORTED_MODULE_22__["default"].getSelectTenantQueryEntityItem(tenantCode), !_common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isAgent() && !_common__WEBPACK_IMPORTED_MODULE_14__["Authority"].isInsurer() && {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].CHECKBOX,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Insurer").thai("บริษัทผู้รับประกัน").my("အာမခံ").getMessage(),
            field: "itntCodes",
            multiple: true,
            get: true,
            url: _common_apis__WEBPACK_IMPORTED_MODULE_13__["default"].TENANT_INSURERS,
            callback: function callback(respData) {
              return (respData || []).map(function (item) {
                return {
                  label: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("img", {
                    alt: item.code,
                    src: item.logoUrl,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 332
                    },
                    __self: this
                  }),
                  value: item.code
                };
              });
            }
          }
        }, {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].CHECKBOX,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Type of Business").thai("รอดำเนินการ").getMessage(),
            field: "bizTypes",
            multiple: true,
            options: _common__WEBPACK_IMPORTED_MODULE_14__["Codes"].BIZ_TYPE()
          }
        }, {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].CHECKBOX,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Product Line").thai("ผลิตภัณฑ์").getMessage(),
            field: "productCates",
            multiple: true,
            get: true,
            url: _common_apis__WEBPACK_IMPORTED_MODULE_13__["default"].PRDT_CATEGORIES,
            callback: function callback(respData) {
              return (respData || []).map(function (item) {
                return {
                  label: item.productCateName,
                  value: item.productCate
                };
              });
            }
          }
        }, [pendingTab, cancelledTab, allTab].includes(this.state.currentTab) && {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].DATERANGE,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Created On").thai("สร้างกรมธรรม์เมื่อ").getMessage(),
            field: "transCreationDateRange",
            options: [_component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].TODAY, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_TWO_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_MONTH],
            diffMonths: 1
          }
        }, [allTab].includes(this.state.currentTab) && {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].DATERANGE,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Policy Effect On").thai("วันเริ่มคุ้มครอง").getMessage(),
            field: "effDateRange",
            options: [_component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].TODAY, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_TWO_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_MONTH],
            diffMonths: 1
          }
        }, [issuedTab, allTab].includes(this.state.currentTab) && {
          type: _component_filter__WEBPACK_IMPORTED_MODULE_17__["FilterItemTypes"].DATERANGE,
          props: {
            label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Policy Issued On").thai("วันที่ออกกรมธรรม์").getMessage(),
            field: "issueDateRange",
            options: [_component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].TODAY, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_TWO_WEEK, _component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_20__["FilterDateRanges"].LAST_ONE_MONTH],
            diffMonths: 1
          }
        }, [cancelledTab].includes(this.state.currentTab) && tags]
      };
    } // methods

  }]);

  return PolicyQuery;
}(_desk_component__WEBPACK_IMPORTED_MODULE_12__["DeskPage"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_11__["withRouter"])(Object(_desk_component_hoc_query_refresh__WEBPACK_IMPORTED_MODULE_24__["withRefreshQueryHoc"])(PolicyQuery)));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVlcnkvcG9saWN5LXF1ZXJ5LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL3F1ZXJ5L3BvbGljeS1xdWVyeS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IENvbCwgTW9kYWwsIFJvdywgVGFicyB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSb3V0ZUNvbXBvbmVudFByb3BzLCB3aXRoUm91dGVyIH0gZnJvbSBcInJlYWN0LXJvdXRlclwiO1xuXG5pbXBvcnQgeyBEZXNrUGFnZSB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgUGFnZUNvbXBvbmVudHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgQVBJIGZyb20gXCJAY29tbW9uL2FwaXNcIjtcbmltcG9ydCB7IEFwaXMsIEF1dGhvcml0eSwgQ29kZXMsIENvbnN0cywgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB1dGlscyBmcm9tIFwiQGNvbW1vbi91dGlsc1wiO1xuaW1wb3J0IGF1dGhvcml0eSBmcm9tIFwiQGNvbW1vbi9hdXRob3JpdHlcIjtcblxuaW1wb3J0IHsgRmlsdGVySXRlbVR5cGVzIH0gZnJvbSBcIi4uLy4uL2NvbXBvbmVudC9maWx0ZXJcIjtcbmltcG9ydCBQb2xpY3lRdWVyeVN0eWxlIGZyb20gXCIuL3BvbGljeS1xdWVyeS1zdHlsZVwiO1xuaW1wb3J0IFF1ZXJ5UGFnZSBmcm9tIFwiLi4vLi4vYWRtaW4vY29tbW9uL1F1ZXJ5UGFnZVwiO1xuaW1wb3J0IHsgRmlsdGVyRGF0ZVJhbmdlcyB9IGZyb20gXCIuLi8uLi9jb21wb25lbnQvZmlsdGVyL2RhdGUtcmFuZ2UtdXRpbFwiO1xuaW1wb3J0IHsgUHJvZFR5cGVJbWFnZSB9IGZyb20gXCIuLi8uLi9jb21wb25lbnQvcXVlcnkvcHJvZC10eXBlXCI7XG5pbXBvcnQgRmlsdGVycyBmcm9tIFwiLi4vLi4vYWRtaW4vY29tbW9uL2ZpbHRlcnNcIjtcbmltcG9ydCB7IHJlbmRlck9wZXJzIH0gZnJvbSBcIi4vY29tcG9uZW50cy9xdWVyeS1vcGVyc1wiO1xuaW1wb3J0IHsgd2l0aFJlZnJlc2hRdWVyeUhvYyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvaG9jL3F1ZXJ5LXJlZnJlc2hcIjtcblxuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5cblxuY29uc3QgcGVuZGluZ1RhYiA9IFwicGVuZGluZ1wiLFxuICBpc3N1ZWRUYWIgPSBcImlzc3VlZFwiLFxuICBjYW5jZWxsZWRUYWIgPSBcImNhbmNlbGxlZFwiLFxuICBhbGxUYWIgPSBcImFsbFwiO1xuY29uc3QgZGVmYXVsdFNlYXJjaENyaXRlcmlhOiBhbnkgPSB7XG4gIFtwZW5kaW5nVGFiXToge1xuICAgIHF1b3RlU3RhdHVzZXM6IFtDb25zdHMuUVVPVEVfU1RBVFVTLkJPVU5ELCBDb25zdHMuUVVPVEVfU1RBVFVTLklOX1BHUkVTXSxcbiAgfSxcbiAgW2lzc3VlZFRhYl06IHtcbiAgICBxdW90ZVN0YXR1c2VzOiBbQ29uc3RzLlFVT1RFX1NUQVRVUy5JU1NVRUQsIENvbnN0cy5RVU9URV9TVEFUVVMuQ0xPU0VEXSxcbiAgfSxcbiAgW2NhbmNlbGxlZFRhYl06IHtcbiAgICBxdW90ZVN0YXR1c2VzOiBbXG4gICAgICBDb25zdHMuUVVPVEVfU1RBVFVTLkVYUCxcbiAgICAgIENvbnN0cy5RVU9URV9TVEFUVVMuQ0FOQ0VMTEVELFxuICAgICAgQ29uc3RzLlFVT1RFX1NUQVRVUy5MQVBTRUQsXG4gICAgICBDb25zdHMuUVVPVEVfU1RBVFVTLlVXX0RFQ0xJTkUsXG4gICAgICBDb25zdHMuUVVPVEVfU1RBVFVTLlJFSkVDVEVELFxuICAgIF0sXG4gIH0sXG4gIFthbGxUYWJdOiB7XG4gICAgcXVvdGVTdGF0dXNlczogW10sXG4gIH0sXG59O1xuXG50eXBlIFF1ZXJ5U2F0ZSA9IHtcbiAgY3VycmVudFRhYjogc3RyaW5nO1xufTtcbnR5cGUgUXVlcnlQcm9wcyA9IHt9ICYgUm91dGVDb21wb25lbnRQcm9wcztcblxuY2xhc3MgUG9saWN5UXVlcnk8UCBleHRlbmRzIFF1ZXJ5UHJvcHMsIFMgZXh0ZW5kcyBRdWVyeVNhdGUsIEMgZXh0ZW5kcyBQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBEZXNrUGFnZTxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBxdWVyeVBhZ2U6IGFueSA9IHt9O1xuICBwcm90ZWN0ZWQgcXVlcnlUYWJzOiBhbnkgPSBbXG4gICAge1xuICAgICAga2V5OiBwZW5kaW5nVGFiLFxuICAgICAgbmFtZTogTGFuZ3VhZ2UuZW4oXCJQZW5kaW5nXCIpXG4gICAgICAgIC50aGFpKFwi4Lij4Lit4LiU4Liz4LmA4LiZ4Li04LiZ4LiB4Liy4LijXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgICB7XG4gICAgICBrZXk6IGlzc3VlZFRhYixcbiAgICAgIG5hbWU6IExhbmd1YWdlLmVuKFwiSXNzdWVkXCIpXG4gICAgICAgIC50aGFpKFwi4Lit4Lit4LiB4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmMXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgICB7XG4gICAgICBrZXk6IGNhbmNlbGxlZFRhYixcbiAgICAgIG5hbWU6IExhbmd1YWdlLmVuKFwiQ2FuY2VsbGVkXCIpXG4gICAgICAgIC50aGFpKFwi4Lii4LiB4LmA4Lil4Li04LiB4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmMXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgICB7XG4gICAgICBrZXk6IGFsbFRhYixcbiAgICAgIG5hbWU6IExhbmd1YWdlLmVuKFwiQWxsXCIpXG4gICAgICAgIC50aGFpKFwi4LiX4Lix4LmJ4LiH4Lir4Lih4LiUXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgXTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCksIHt9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgY29uc3QgeyBhY3Rpb24sIGxvY2F0aW9uIH0gPSB0aGlzLnByb3BzLmhpc3RvcnkgfHwge1xuICAgICAgYWN0aW9uOiBcIlwiLFxuICAgICAgbG9jYXRpb246IHt9LFxuICAgIH07XG4gICAgbGV0IGN1cnJlbnRUYWI6IGFueTtcbiAgICBsZXQgcGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcyh3aW5kb3cubG9jYXRpb24uc2VhcmNoKTtcbiAgICBsZXQgcGFyYW1DdXJyZW50VGFiID0gcGFyYW1zLmdldChcImN1cnJlbnRUYWJcIik7XG5cbiAgICBpZiAoYWN0aW9uID09PSBcIlBVU0hcIiAmJiBfLmdldChsb2NhdGlvbiwgXCJzdGF0ZS5jdXJyZW50VGFiXCIpKSB7XG4gICAgICBjdXJyZW50VGFiID0gXy5nZXQobG9jYXRpb24sIFwic3RhdGUuY3VycmVudFRhYlwiKTtcbiAgICB9IGVsc2UgaWYgKHBhcmFtQ3VycmVudFRhYikge1xuICAgICAgY3VycmVudFRhYiA9IHBhcmFtQ3VycmVudFRhYjtcbiAgICB9IGVsc2Uge1xuICAgICAgY3VycmVudFRhYiA9IHBlbmRpbmdUYWI7XG4gICAgfVxuICAgIHJldHVybiB7XG4gICAgICBjdXJyZW50VGFiLFxuICAgIH0gYXMgUztcbiAgfVxuXG5cbiAgLy8gcmVuZGVyc1xuICBwcm90ZWN0ZWQgcmVuZGVyUGFnZUJvZHkoKSB7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPFBvbGljeVF1ZXJ5U3R5bGUuQ29udGFpbmVyIGRhdGEtd2lkZ2V0PVwicGFnZS1ib2R5XCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgPFRhYnNcbiAgICAgICAgICAgIGFjdGl2ZUtleT17dGhpcy5zdGF0ZS5jdXJyZW50VGFifVxuICAgICAgICAgICAgb25DaGFuZ2U9e3RhYiA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjdXJyZW50VGFiOiB0YWIgfSk7XG4gICAgICAgICAgICAgIC8vYWZ0ZXIgY2hhbmdlIHRhYiwgbmVlZCBjbGVhciBzZWxlY3RlZCBzZWFyY2ggY3JpdGVyaWFcbiAgICAgICAgICAgICAgdGhpcy5xdWVyeVBhZ2UucmVzZXRGaWx0ZXJWYWx1ZXMoKTtcbiAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5xdWVyeVBhZ2UuZmV0Y2hEYXRhKEFQSS5QT0xJQ0lFU19RVUVSWSwge1xuICAgICAgICAgICAgICAgICAgLi4uZGVmYXVsdFNlYXJjaENyaXRlcmlhW3RhYl0sXG4gICAgICAgICAgICAgICAgICAuLi57XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VJbmRleDogMSxcbiAgICAgICAgICAgICAgICAgICAgcGFnZVNpemU6IDUsXG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9LCAyMCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiYW50LXRhYnMtLW1haW5cIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIHt0aGlzLnF1ZXJ5VGFicy5tYXAoKGl0ZW06IGFueSk6IGFueSA9PiB7XG4gICAgICAgICAgICAgIHJldHVybiA8VGFicy5UYWJQYW5lIHRhYj17aXRlbS5uYW1lfSBrZXk9e2l0ZW0ua2V5fS8+O1xuICAgICAgICAgICAgfSl9XG4gICAgICAgICAgPC9UYWJzPlxuICAgICAgICAgIDxRdWVyeVBhZ2VcbiAgICAgICAgICAgIHJlZj17dGhpcy5pbml0UXVlcnlQYWdlUmVmfVxuICAgICAgICAgICAgaXRlbUtleT1cInJlZklkXCJcbiAgICAgICAgICAgIGZpbHRlcj17dGhpcy5nZXRGaWx0ZXIoKX1cbiAgICAgICAgICAgIGNvbHVtbnM9e2lzTW9iaWxlID8gdGhpcy5nZXRDb2x1bW5zTW9iaWxlKCkgOiB0aGlzLmdldENvbHVtbnMoKX1cbiAgICAgICAgICAgIGFwaVVybD17QVBJLlBPTElDSUVTX1FVRVJZfVxuICAgICAgICAgICAgb25CZWZvcmVSZWZyZXNoPXsoc2VhcmNoQ3JpdGVyaWE6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBpZiAoISFzZWFyY2hDcml0ZXJpYS5jdG50U3RydWN0SWRzKSB7XG4gICAgICAgICAgICAgICAgc2VhcmNoQ3JpdGVyaWEuY3RudFN0cnVjdElkcyA9IFtzZWFyY2hDcml0ZXJpYS5jdG50U3RydWN0SWRzXTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4geyAuLi5kZWZhdWx0U2VhcmNoQ3JpdGVyaWFbdGhpcy5zdGF0ZS5jdXJyZW50VGFiXSwgLi4uc2VhcmNoQ3JpdGVyaWEgfTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICByZW5kZXJDYXJkSGVhZD17KGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDxSb3cga2V5PXtpdGVtLnJlZklkfT5cbiAgICAgICAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwiY29sLXRpdGxlXCIgc3R5bGU9e3sgZmxvYXQ6IFwibGVmdFwiIH19PlxuICAgICAgICAgICAgICAgICAgICA8UHJvZFR5cGVJbWFnZSBwcm9kdWN0Q2F0ZT17aXRlbS5wcm9kdWN0Q2F0ZX0vPlxuICAgICAgICAgICAgICAgICAgICB7aXRlbS5yZWZOb31cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiaXRudC1iYWRnZVwiPntgJHtpdGVtLmJpelR5cGV9YH08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwidGV4dC0tYWxpZ24tcmlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAge2Ake2l0ZW0uY3RudFN0cnVjdENvZGUgfHwgXCJcIn0gXG4gICAgICAgICAgICAgICAgICAgICR7aXRlbS5jdG50U3RydWN0Q29kZSAmJiBpdGVtLmN0bnRTdHJ1Y3ROYW1lID8gXCItXCIgOiBcIlwifSBcbiAgICAgICAgICAgICAgICAgICAgJHtpdGVtLmN0bnRTdHJ1Y3ROYW1lIHx8IFwiXCJ9YH1cbiAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvUG9saWN5UXVlcnlTdHlsZS5Db250YWluZXI+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVySXRudExvZ28oaXRlbTogYW55KTogYW55IHtcbiAgICBjb25zdCB7IGl0bnRMb2dvVXJsIH0gPSBpdGVtO1xuICAgIGlmICghaXRudExvZ29VcmwpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBpZiAoQXV0aG9yaXR5LmlzQnJva2VyKCkgfHwgQXV0aG9yaXR5LmlzUGxhdGZvcm0oKSB8fCBBdXRob3JpdHkuaXNEMkMoKSkge1xuICAgICAgcmV0dXJuIDxpbWcgc3JjPXtpdG50TG9nb1VybH0gYWx0PVwiXCIvPjtcbiAgICB9XG5cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUG9saWN5TWFpbkluZm8ocm93OiBhbnkpIHtcbiAgICBjb25zdCBwaFN0ciA9IChyb3cucGhOYW1lID8gYCR7cm93LnBoTmFtZX0sIGAgOiBcIlwiKSArIChyb3cucGhNb2JpbGUgPyBgJHtyb3cucGhNb2JpbGV9LCBgIDogXCJcIik7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicG9saWN5LW1haW4taW5mb1wiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIml0bnQtbG9nby13cmFwXCIgc3R5bGU9e1xuICAgICAgICAgIHRoaXMucmVuZGVySXRudExvZ28ocm93KSA/IHt9IDoge1xuICAgICAgICAgICAgcGFkZGluZ0xlZnQ6IDEwLFxuICAgICAgICAgIH1cbiAgICAgICAgfT5cbiAgICAgICAgICB7dGhpcy5yZW5kZXJJdG50TG9nbyhyb3cpfVxuICAgICAgICAgIDxwXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJ0aXRsZSBtYXgtbGltaXQtLTIyXCI+eyFyb3cuaW5zdXJlZERlc2NyaXB0aW9uIHx8IHJvdy5pbnN1cmVkRGVzY3JpcHRpb24gPT09IFwibnVsbFwiID8gXCJcIiA6IHJvdy5pbnN1cmVkRGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRpdGxlLS1zdWJcIj5cbiAgICAgICAgICAgIHtbcm93LnByb2R1Y3ROYW1lLCByb3cucGxhbk5hbWVdLmZpbHRlcigodiwgaSwgYSkgPT4gdiAmJiBhLmluZGV4T2YodikgPT09IGkpLmpvaW4oXCIsIFwiKX1cbiAgICAgICAgICA8L3A+XG4gICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGUtLXN1YlwiPntwaFN0ciA/IHBoU3RyLnN1YnN0cmluZygwLCBwaFN0ci5sZW5ndGggLSAyKSA6IFwiLVwifTwvcD5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRDb2x1bW5zKCk6IGFueSB7XG4gICAgbGV0IF90aGlzID0gdGhpcztcbiAgICByZXR1cm4gW1xuICAgICAge1xuICAgICAgICBzdHlsZTogeyB3aWR0aDogXCIzMiVcIiB9LFxuICAgICAgICByZW5kZXIodmFsdWU6IGFueSwgcm93OiBhbnkpIHtcbiAgICAgICAgICByZXR1cm4gPGRpdj57X3RoaXMucmVuZGVyUG9saWN5TWFpbkluZm8ocm93KX08L2Rpdj47XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBzdHlsZTogeyB3aWR0aDogXCIyMCVcIiwgdGV4dEFsaWduOiBcInJpZ2h0XCIgfSxcbiAgICAgICAgcmVuZGVyKHZhbHVlOiBhbnksIHJvdzogYW55KSB7XG4gICAgICAgICAgcmV0dXJuIDxkaXY+e1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0ocm93KX08L2Rpdj47XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBzdHlsZTogeyB3aWR0aDogXCIyOCVcIiwgdGV4dEFsaWduOiBcImNlbnRlclwiIH0sXG4gICAgICAgIHJlbmRlcih2YWx1ZTogYW55LCByb3c6IGFueSkge1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8c3Ryb25nIHN0eWxlPXt7IGZvbnRXZWlnaHQ6IDcwMCB9fT57cm93LnN0YXR1c05hbWV9PC9zdHJvbmc+XG4gICAgICAgICAgICAgIHtyb3cuc3ViU3RhdHVzTmFtZSAmJiA8cCBzdHlsZT17eyBmb250U2l6ZTogMTIsIGNvbG9yOiBcIiM5OTlcIiB9fT57cm93LnN1YlN0YXR1c05hbWV9PC9wPn1cbiAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidGl0bGUtLXN1YlwiPntVdGlscy5kYXRlVGltZUZyb21Ob3cocm93Lmxhc3RVcGRhdGVkQXQpfTwvcD5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICk7XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBzdHlsZTogeyB3aWR0aDogXCIyMCVcIiwgdGV4dEFsaWduOiBcImNlbnRlclwiIH0sXG4gICAgICAgIHJlbmRlcih2YWx1ZTogYW55LCByb3c6IGFueSkge1xuICAgICAgICAgIHJldHVybiA8ZGl2PntyZW5kZXJPcGVycyhyb3csIF90aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKSwgX3RoaXMuaGFuZGxlUmVqZWN0UG9saWN5LCBfdGhpcyl9PC9kaXY+O1xuICAgICAgICB9LFxuICAgICAgfSxcbiAgICBdO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRDb2x1bW5zTW9iaWxlKCk6IGFueSB7XG4gICAgbGV0IF90aGlzID0gdGhpcztcbiAgICBjb25zdCBpc0xlZnQgPSBBdXRob3JpdHkuaXNCcm9rZXIoKSB8fCBBdXRob3JpdHkuaXNQbGF0Zm9ybSgpIHx8IEF1dGhvcml0eS5pc0QyQygpO1xuICAgIHJldHVybiBbXG4gICAgICB7XG4gICAgICAgIHN0eWxlOiB7IHdpZHRoOiBcIjEwMCVcIiB9LFxuICAgICAgICByZW5kZXIodmFsdWU6IGFueSwgcm93OiBhbnkpIHtcbiAgICAgICAgICByZXR1cm4gPGRpdj57X3RoaXMucmVuZGVyUG9saWN5TWFpbkluZm8ocm93KX08L2Rpdj47XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBzdHlsZTogeyB3aWR0aDogXCIxMDAlXCIsIHRleHRBbGlnbjogXCJsZWZ0XCIsIG1hcmdpbkxlZnQ6IGlzTGVmdCA/IFwiNjVweFwiIDogXCIxMHB4XCIgfSxcbiAgICAgICAgcmVuZGVyKHZhbHVlOiBhbnksIHJvdzogYW55KSB7XG4gICAgICAgICAgcmV0dXJuIDxkaXY+e1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0ocm93KX08L2Rpdj47XG4gICAgICAgIH0sXG4gICAgICB9LFxuXG4gICAgICB7XG4gICAgICAgIHN0eWxlOiB7IHdpZHRoOiBcIjEwMCVcIiwgdGV4dEFsaWduOiBcImxlZnRcIiwgbWFyZ2luTGVmdDogaXNMZWZ0ID8gXCI2NXB4XCIgOiBcIjEwcHhcIiB9LFxuICAgICAgICByZW5kZXIodmFsdWU6IGFueSwgcm93OiBhbnkpIHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICA8c3Ryb25nIHN0eWxlPXt7IGZvbnRXZWlnaHQ6IDcwMCB9fT57cm93LnN0YXR1c05hbWV9PC9zdHJvbmc+XG4gICAgICAgICAgICAgICAge3Jvdy5zdWJTdGF0dXNOYW1lICYmIDxwIHN0eWxlPXt7IGZvbnRTaXplOiAxMiwgY29sb3I6IFwiIzk5OVwiIH19Pntyb3cuc3ViU3RhdHVzTmFtZX08L3A+fVxuICAgICAgICAgICAgICA8Lz5cblxuICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZS0tc3ViXCI+e1V0aWxzLmRhdGVUaW1lRnJvbU5vdyhyb3cubGFzdFVwZGF0ZWRBdCl9PC9wPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHN0eWxlOiB7IHdpZHRoOiBcIjEwMCVcIiwgdGV4dEFsaWduOiBcImxlZnRcIiwgbWFyZ2luTGVmdDogaXNMZWZ0ID8gXCI2NXB4XCIgOiBcIjEwcHhcIiB9LFxuICAgICAgICByZW5kZXIodmFsdWU6IGFueSwgcm93OiBhbnkpIHtcbiAgICAgICAgICByZXR1cm4gPGRpdj57cmVuZGVyT3BlcnMocm93LCBfdGhpcy5nZXRIZWxwZXJzKCkuZ2V0Um91dGVyKCksIF90aGlzLmhhbmRsZVJlamVjdFBvbGljeSwgX3RoaXMpfTwvZGl2PjtcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgXTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0RmlsdGVyKCk6IGFueSB7XG4gICAgY29uc3QgeyB0ZW5hbnRDb2RlIH0gPSB0aGlzLmluaXRRdWVyeVBhcmFtcygpO1xuICAgIGNvbnN0IHRhZ3MgPSB7XG4gICAgICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuQ0hFQ0tCT1gsXG4gICAgICBwcm9wczoge1xuICAgICAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJUQUdTXCIpXG4gICAgICAgICAgLnRoYWkoXCLguILguLHguYnguJnguJXguK3guJlcIilcbiAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICBmaWVsZDogXCJ0YWdzXCIsXG4gICAgICAgIG11bHRpcGxlOiB0cnVlLFxuICAgICAgICBvcHRpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiSW5jZXB0aW9uIENhbmNlbFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4ouC4geC5gOC4peC4tOC4geC4geC5iOC4reC4meC4hOC4p+C4suC4oeC4hOC4uOC5ieC4oeC4hOC4o+C4reC4h1wiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgdmFsdWU6IFwiSU5DRVBUSU9OX0NBTkNFTFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiTWlkd2F5IENhbmNlbFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4ouC4geC5gOC4peC4tOC4geC4iOC4suC4geC4geC4suC4o+C4guC4suC4lOC4iuC4s+C4o+C4sFwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgdmFsdWU6IFwiTUlEV0FZX0NBTkNFTFwiLFxuICAgICAgICAgIH0sXG4gICAgICAgIF0sXG4gICAgICB9LFxuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgaXRlbXM6IFtcbiAgICAgICAgRmlsdGVycy5nZXRTZWxlY3RUZW5hbnRRdWVyeUVudGl0eUl0ZW0odGVuYW50Q29kZSksXG4gICAgICAgICFBdXRob3JpdHkuaXNBZ2VudCgpICYmXG4gICAgICAgICFBdXRob3JpdHkuaXNJbnN1cmVyKCkgJiYge1xuICAgICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5DSEVDS0JPWCxcbiAgICAgICAgICBwcm9wczoge1xuICAgICAgICAgICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiSW5zdXJlclwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4muC4o+C4tOC4qeC4seC4l+C4nOC4ueC5ieC4o+C4seC4muC4m+C4o+C4sOC4geC4seC4mVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgKzhgJnhgIHhgLZcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIGZpZWxkOiBcIml0bnRDb2Rlc1wiLFxuICAgICAgICAgICAgbXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICBnZXQ6IHRydWUsXG4gICAgICAgICAgICB1cmw6IEFQSS5URU5BTlRfSU5TVVJFUlMsXG4gICAgICAgICAgICBjYWxsYmFjazogKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIChyZXNwRGF0YSB8fCBbXSkubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgbGFiZWw6IDxpbWcgYWx0PXtpdGVtLmNvZGV9IHNyYz17aXRlbS5sb2dvVXJsfS8+LFxuICAgICAgICAgICAgICAgICAgdmFsdWU6IGl0ZW0uY29kZSxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5DSEVDS0JPWCxcbiAgICAgICAgICBwcm9wczoge1xuICAgICAgICAgICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiVHlwZSBvZiBCdXNpbmVzc1wiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4o+C4reC4lOC4s+C5gOC4meC4tOC4meC4geC4suC4o1wiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgZmllbGQ6IFwiYml6VHlwZXNcIixcbiAgICAgICAgICAgIG11bHRpcGxlOiB0cnVlLFxuICAgICAgICAgICAgb3B0aW9uczogQ29kZXMuQklaX1RZUEUoKSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkNIRUNLQk9YLFxuICAgICAgICAgIHByb3BzOiB7XG4gICAgICAgICAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJQcm9kdWN0IExpbmVcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguJzguKXguLTguJXguKDguLHguJPguJHguYxcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIGZpZWxkOiBcInByb2R1Y3RDYXRlc1wiLFxuICAgICAgICAgICAgbXVsdGlwbGU6IHRydWUsXG4gICAgICAgICAgICBnZXQ6IHRydWUsXG4gICAgICAgICAgICB1cmw6IEFQSS5QUkRUX0NBVEVHT1JJRVMsXG4gICAgICAgICAgICBjYWxsYmFjazogKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIChyZXNwRGF0YSB8fCBbXSkubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyBsYWJlbDogaXRlbS5wcm9kdWN0Q2F0ZU5hbWUsIHZhbHVlOiBpdGVtLnByb2R1Y3RDYXRlIH07XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgICBbcGVuZGluZ1RhYiwgY2FuY2VsbGVkVGFiLCBhbGxUYWJdLmluY2x1ZGVzKHRoaXMuc3RhdGUuY3VycmVudFRhYikgJiYge1xuICAgICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5EQVRFUkFOR0UsXG4gICAgICAgICAgcHJvcHM6IHtcbiAgICAgICAgICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIkNyZWF0ZWQgT25cIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguKrguKPguYnguLLguIfguIHguKPguKHguJjguKPguKPguKHguYzguYDguKHguLfguYjguK1cIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIGZpZWxkOiBcInRyYW5zQ3JlYXRpb25EYXRlUmFuZ2VcIixcbiAgICAgICAgICAgIG9wdGlvbnM6IFtcbiAgICAgICAgICAgICAgRmlsdGVyRGF0ZVJhbmdlcy5UT0RBWSxcbiAgICAgICAgICAgICAgRmlsdGVyRGF0ZVJhbmdlcy5MQVNUX09ORV9XRUVLLFxuICAgICAgICAgICAgICBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfVFdPX1dFRUssXG4gICAgICAgICAgICAgIEZpbHRlckRhdGVSYW5nZXMuTEFTVF9PTkVfTU9OVEgsXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGlmZk1vbnRoczogMSxcbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuXG4gICAgICAgIFthbGxUYWJdLmluY2x1ZGVzKHRoaXMuc3RhdGUuY3VycmVudFRhYikgJiYge1xuICAgICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5EQVRFUkFOR0UsXG4gICAgICAgICAgcHJvcHM6IHtcbiAgICAgICAgICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIlBvbGljeSBFZmZlY3QgT25cIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguKfguLHguJnguYDguKPguLTguYjguKHguITguLjguYnguKHguITguKPguK3guIdcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgIGZpZWxkOiBcImVmZkRhdGVSYW5nZVwiLFxuICAgICAgICAgICAgb3B0aW9uczogW1xuICAgICAgICAgICAgICBGaWx0ZXJEYXRlUmFuZ2VzLlRPREFZLFxuICAgICAgICAgICAgICBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX1dFRUssXG4gICAgICAgICAgICAgIEZpbHRlckRhdGVSYW5nZXMuTEFTVF9UV09fV0VFSyxcbiAgICAgICAgICAgICAgRmlsdGVyRGF0ZVJhbmdlcy5MQVNUX09ORV9NT05USCxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkaWZmTW9udGhzOiAxLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG5cbiAgICAgICAgW2lzc3VlZFRhYiwgYWxsVGFiXS5pbmNsdWRlcyh0aGlzLnN0YXRlLmN1cnJlbnRUYWIpICYmIHtcbiAgICAgICAgICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuREFURVJBTkdFLFxuICAgICAgICAgIHByb3BzOiB7XG4gICAgICAgICAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJQb2xpY3kgSXNzdWVkIE9uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4Lin4Lix4LiZ4LiX4Li14LmI4Lit4Lit4LiB4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmMXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICBmaWVsZDogXCJpc3N1ZURhdGVSYW5nZVwiLFxuICAgICAgICAgICAgb3B0aW9uczogW1xuICAgICAgICAgICAgICBGaWx0ZXJEYXRlUmFuZ2VzLlRPREFZLFxuICAgICAgICAgICAgICBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX1dFRUssXG4gICAgICAgICAgICAgIEZpbHRlckRhdGVSYW5nZXMuTEFTVF9UV09fV0VFSyxcbiAgICAgICAgICAgICAgRmlsdGVyRGF0ZVJhbmdlcy5MQVNUX09ORV9NT05USCxcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkaWZmTW9udGhzOiAxLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0sXG4gICAgICAgIFtjYW5jZWxsZWRUYWJdLmluY2x1ZGVzKHRoaXMuc3RhdGUuY3VycmVudFRhYikgJiYgdGFncyxcbiAgICAgIF0sXG4gICAgfTtcbiAgfVxuXG4gIC8vIG1ldGhvZHNcbiAgaW5pdFF1ZXJ5UGFnZVJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgIHRoaXMucXVlcnlQYWdlID0gcmVmO1xuICB9O1xuXG4gIGluaXRRdWVyeVBhcmFtcyA9ICgpID0+IHtcbiAgICBjb25zdCB7IHN0cnVjdElkID0gXCJcIiwgdGVuYW50Q29kZSA9IFwiXCIgfSA9IHV0aWxzLmdldFBhcmFtc0Zyb21VcmwoKSB8fCB7fTtcbiAgICBjb25zdCBpc1BsYXRmb3JtID0gYXV0aG9yaXR5LmlzUGxhdGZvcm0oKTtcbiAgICBjb25zdCB0ZW5hbnRDb2RlTiA9IGF1dGhvcml0eS5nZXRUZW5hbnRDb2RlKCk7XG4gICAgY29uc3QgaW5pdFRlbmFudENvZGUgPSB0ZW5hbnRDb2RlICE9PSBcIlwiID8gdGVuYW50Q29kZSA6IChpc1BsYXRmb3JtID8gXCJcIiA6IHRlbmFudENvZGVOKTtcbiAgICBjb25zdCBpbml0UGFyYW1zID0geyBzdHJ1Y3RJZCwgdGVuYW50Q29kZTogaW5pdFRlbmFudENvZGUgfTtcblxuICAgIHJldHVybiBpbml0UGFyYW1zO1xuICB9O1xuXG4gIC8vIGFjdGlvbnNcbiAgcHJpdmF0ZSBvblJlamVjdFBvbGljeSA9IChwb2xpY3k6IGFueSkgPT4ge1xuICAgIGlmIChwb2xpY3kuYml6VHlwZSA9PT0gXCJDT01QXCIpIHtcbiAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgLnBhdGNoKEFwaXMuQ09NUF9RVU9URV9SRUpFQ1QucmVwbGFjZShcIjpjb21wUXVvdGVJZFwiLCBwb2xpY3kucmVmSWQpLCB7fSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICB0aGlzLnF1ZXJ5UGFnZS5yZWZyZXNoQ2FyZExpc3QoZGVmYXVsdFNlYXJjaENyaXRlcmlhW3RoaXMuc3RhdGUuY3VycmVudFRhYl0sIHsgcGFnZUluZGV4OiAxLCBwYWdlU2l6ZTogNSB9KTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgIC5nZXRBamF4KClcbiAgICAgICAgLnBvc3QoQXBpcy5QT0xJQ1lfUkVKRUNULnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgcG9saWN5LnJlZklkKSwge30pXG4gICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgdGhpcy5xdWVyeVBhZ2UucmVmcmVzaENhcmRMaXN0KGRlZmF1bHRTZWFyY2hDcml0ZXJpYVt0aGlzLnN0YXRlLmN1cnJlbnRUYWJdLCB7IHBhZ2VJbmRleDogMSwgcGFnZVNpemU6IDUgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgfTtcblxuXG4gIHByaXZhdGUgaGFuZGxlUmVqZWN0UG9saWN5ID0gKHBvbGljeTogYW55KSA9PiB7XG4gICAgTW9kYWwuY29uZmlybSh7XG4gICAgICBjb250ZW50OiBMYW5ndWFnZS5lbihcIkN1c3RvbWVyIHBheW1lbnQgbWlnaHQgYmUgaW4gcHJvZ3Jlc3MuIEFyZSB5b3Ugc3VyZSB0byByZWplY3QgdGhlIHF1b3RhdGlvbj9cIilcbiAgICAgICAgLnRoYWkoXCJDdXN0b21lciBwYXltZW50IG1pZ2h0IGJlIGluIHByb2dyZXNzLiBBcmUgeW91IHN1cmUgdG8gcmVqZWN0IHRoZSBxdW90YXRpb24/XCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICBvbk9rOiAoKSA9PiB7XG4gICAgICAgIHRoaXMub25SZWplY3RQb2xpY3kocG9saWN5KTtcbiAgICAgIH0sXG4gICAgICBva1RleHQ6IExhbmd1YWdlLmVuKFwiWWVzXCIpXG4gICAgICAgIC50aGFpKFwi4LmD4LiK4LmIXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICBjYW5jZWxUZXh0OiBMYW5ndWFnZS5lbihcIk5vXCIpXG4gICAgICAgIC50aGFpKFwi4LmE4Lih4LmIXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSk7XG4gIH07XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFJvdXRlcih3aXRoUmVmcmVzaFF1ZXJ5SG9jKFBvbGljeVF1ZXJ5KSk7XG5cblxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBRUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQURBO0FBVUE7QUFEQTtBQUNBO0FBU0E7Ozs7O0FBNkJBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUF6QkE7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBQ0E7QUFPQTtBQW1WQTtBQUNBO0FBQ0E7QUFyVkE7QUFzVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUEvVkE7QUFrV0E7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxYQTtBQXFYQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQVZBO0FBY0E7QUFDQTtBQXBZQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQURBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQTNCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFEQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFKQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFWQTtBQWFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFPQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUpBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUpBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFiQTtBQWdCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFPQTs7O0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBR0E7QUFKQTtBQU9BO0FBR0E7QUFKQTtBQWJBO0FBRkE7QUF5QkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFoQkE7QUFGQTtBQXNCQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFOQTtBQUZBO0FBWUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBWkE7QUFGQTtBQWtCQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBTUE7QUFYQTtBQUZBO0FBa0JBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFNQTtBQVhBO0FBRkE7QUFrQkE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQU1BO0FBWEE7QUFGQTtBQXZGQTtBQTBHQTtBQUNBOzs7O0FBN1dBO0FBQ0E7QUFtYUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/query/policy-query.tsx
