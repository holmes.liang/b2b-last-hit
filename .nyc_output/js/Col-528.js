__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}




var Col = function Col(props) {
  var _classNames;

  var child = props.child,
      bordered = props.bordered,
      colon = props.colon,
      type = props.type,
      layout = props.layout;
  var _child$props = child.props,
      prefixCls = _child$props.prefixCls,
      label = _child$props.label,
      className = _child$props.className,
      children = _child$props.children,
      _child$props$span = _child$props.span,
      span = _child$props$span === void 0 ? 1 : _child$props$span;
  var labelProps = {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item-label"), (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-item-colon"), colon), _defineProperty(_classNames, "".concat(prefixCls, "-item-no-label"), !label), _classNames)),
    key: 'label'
  };

  if (layout === 'vertical') {
    labelProps.colSpan = span * 2 - 1;
  }

  if (bordered) {
    if (type === 'label') {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("th", labelProps, label);
    }

    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item-content"), className),
      key: "content",
      colSpan: span * 2 - 1
    }, children);
  }

  if (layout === 'vertical') {
    if (type === 'content') {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
        colSpan: span,
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item"), className)
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-item-content"),
        key: "content"
      }, children));
    }

    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
      colSpan: span,
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item"), className)
    }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item-label"), _defineProperty({}, "".concat(prefixCls, "-item-colon"), colon)),
      key: "label"
    }, label));
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("td", {
    colSpan: span,
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-item"), className)
  }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", labelProps, label), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-item-content"),
    key: "content"
  }, children));
};

/* harmony default export */ __webpack_exports__["default"] = (Col);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9kZXNjcmlwdGlvbnMvQ29sLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9kZXNjcmlwdGlvbnMvQ29sLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmNvbnN0IENvbCA9IHByb3BzID0+IHtcbiAgICBjb25zdCB7IGNoaWxkLCBib3JkZXJlZCwgY29sb24sIHR5cGUsIGxheW91dCB9ID0gcHJvcHM7XG4gICAgY29uc3QgeyBwcmVmaXhDbHMsIGxhYmVsLCBjbGFzc05hbWUsIGNoaWxkcmVuLCBzcGFuID0gMSB9ID0gY2hpbGQucHJvcHM7XG4gICAgY29uc3QgbGFiZWxQcm9wcyA9IHtcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKGAke3ByZWZpeENsc30taXRlbS1sYWJlbGAsIHtcbiAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWl0ZW0tY29sb25gXTogY29sb24sXG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtLW5vLWxhYmVsYF06ICFsYWJlbCxcbiAgICAgICAgfSksXG4gICAgICAgIGtleTogJ2xhYmVsJyxcbiAgICB9O1xuICAgIGlmIChsYXlvdXQgPT09ICd2ZXJ0aWNhbCcpIHtcbiAgICAgICAgbGFiZWxQcm9wcy5jb2xTcGFuID0gc3BhbiAqIDIgLSAxO1xuICAgIH1cbiAgICBpZiAoYm9yZGVyZWQpIHtcbiAgICAgICAgaWYgKHR5cGUgPT09ICdsYWJlbCcpIHtcbiAgICAgICAgICAgIHJldHVybiA8dGggey4uLmxhYmVsUHJvcHN9PntsYWJlbH08L3RoPjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gKDx0ZCBjbGFzc05hbWU9e2NsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1pdGVtLWNvbnRlbnRgLCBjbGFzc05hbWUpfSBrZXk9XCJjb250ZW50XCIgY29sU3Bhbj17c3BhbiAqIDIgLSAxfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC90ZD4pO1xuICAgIH1cbiAgICBpZiAobGF5b3V0ID09PSAndmVydGljYWwnKSB7XG4gICAgICAgIGlmICh0eXBlID09PSAnY29udGVudCcpIHtcbiAgICAgICAgICAgIHJldHVybiAoPHRkIGNvbFNwYW49e3NwYW59IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWl0ZW1gLCBjbGFzc05hbWUpfT5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1jb250ZW50YH0ga2V5PVwiY29udGVudFwiPlxuICAgICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPC90ZD4pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoPHRkIGNvbFNwYW49e3NwYW59IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWl0ZW1gLCBjbGFzc05hbWUpfT5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtjbGFzc05hbWVzKGAke3ByZWZpeENsc30taXRlbS1sYWJlbGAsIHsgW2Ake3ByZWZpeENsc30taXRlbS1jb2xvbmBdOiBjb2xvbiB9KX0ga2V5PVwibGFiZWxcIj5cbiAgICAgICAgICB7bGFiZWx9XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgIDwvdGQ+KTtcbiAgICB9XG4gICAgcmV0dXJuICg8dGQgY29sU3Bhbj17c3Bhbn0gY2xhc3NOYW1lPXtjbGFzc05hbWVzKGAke3ByZWZpeENsc30taXRlbWAsIGNsYXNzTmFtZSl9PlxuICAgICAgPHNwYW4gey4uLmxhYmVsUHJvcHN9PntsYWJlbH08L3NwYW4+XG4gICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1jb250ZW50YH0ga2V5PVwiY29udGVudFwiPlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L3NwYW4+XG4gICAgPC90ZD4pO1xufTtcbmV4cG9ydCBkZWZhdWx0IENvbDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBSUE7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFyQ0E7QUFDQTtBQXlDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/descriptions/Col.js
