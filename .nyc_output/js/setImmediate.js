/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */
 // setimmediate adds setImmediate to the global. We want to make sure we export
// the actual function.

__webpack_require__(/*! setimmediate */ "./node_modules/setimmediate/setImmediate.js");

module.exports = global.setImmediate;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvc2V0SW1tZWRpYXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvc2V0SW1tZWRpYXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLy8gc2V0aW1tZWRpYXRlIGFkZHMgc2V0SW1tZWRpYXRlIHRvIHRoZSBnbG9iYWwuIFdlIHdhbnQgdG8gbWFrZSBzdXJlIHdlIGV4cG9ydFxuLy8gdGhlIGFjdHVhbCBmdW5jdGlvbi5cblxucmVxdWlyZSgnc2V0aW1tZWRpYXRlJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGdsb2JhbC5zZXRJbW1lZGlhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQVFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/setImmediate.js
