__webpack_require__.r(__webpack_exports__);
// ================== Collapse Motion ==================
var getCollapsedHeight = function getCollapsedHeight() {
  return {
    height: 0,
    opacity: 0
  };
};

var getRealHeight = function getRealHeight(node) {
  return {
    height: node.scrollHeight,
    opacity: 1
  };
};

var getCurrentHeight = function getCurrentHeight(node) {
  return {
    height: node.offsetHeight
  };
};

var collapseMotion = {
  motionName: 'ant-motion-collapse',
  onAppearStart: getCollapsedHeight,
  onEnterStart: getCollapsedHeight,
  onAppearActive: getRealHeight,
  onEnterActive: getRealHeight,
  onLeaveStart: getCurrentHeight,
  onLeaveActive: getCollapsedHeight
};
/* harmony default export */ __webpack_exports__["default"] = (collapseMotion);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9tb3Rpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL191dGlsL21vdGlvbi5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiLy8gPT09PT09PT09PT09PT09PT09IENvbGxhcHNlIE1vdGlvbiA9PT09PT09PT09PT09PT09PT1cbmNvbnN0IGdldENvbGxhcHNlZEhlaWdodCA9ICgpID0+ICh7IGhlaWdodDogMCwgb3BhY2l0eTogMCB9KTtcbmNvbnN0IGdldFJlYWxIZWlnaHQgPSBub2RlID0+ICh7IGhlaWdodDogbm9kZS5zY3JvbGxIZWlnaHQsIG9wYWNpdHk6IDEgfSk7XG5jb25zdCBnZXRDdXJyZW50SGVpZ2h0ID0gbm9kZSA9PiAoeyBoZWlnaHQ6IG5vZGUub2Zmc2V0SGVpZ2h0IH0pO1xuY29uc3QgY29sbGFwc2VNb3Rpb24gPSB7XG4gICAgbW90aW9uTmFtZTogJ2FudC1tb3Rpb24tY29sbGFwc2UnLFxuICAgIG9uQXBwZWFyU3RhcnQ6IGdldENvbGxhcHNlZEhlaWdodCxcbiAgICBvbkVudGVyU3RhcnQ6IGdldENvbGxhcHNlZEhlaWdodCxcbiAgICBvbkFwcGVhckFjdGl2ZTogZ2V0UmVhbEhlaWdodCxcbiAgICBvbkVudGVyQWN0aXZlOiBnZXRSZWFsSGVpZ2h0LFxuICAgIG9uTGVhdmVTdGFydDogZ2V0Q3VycmVudEhlaWdodCxcbiAgICBvbkxlYXZlQWN0aXZlOiBnZXRDb2xsYXBzZWRIZWlnaHQsXG59O1xuZXhwb3J0IGRlZmF1bHQgY29sbGFwc2VNb3Rpb247XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/motion.js
