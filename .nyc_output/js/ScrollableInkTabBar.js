__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _InkTabBarNode__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./InkTabBarNode */ "./node_modules/rc-tabs/es/InkTabBarNode.js");
/* harmony import */ var _TabBarTabsNode__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./TabBarTabsNode */ "./node_modules/rc-tabs/es/TabBarTabsNode.js");
/* harmony import */ var _TabBarRootNode__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./TabBarRootNode */ "./node_modules/rc-tabs/es/TabBarRootNode.js");
/* harmony import */ var _ScrollableTabBarNode__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ScrollableTabBarNode */ "./node_modules/rc-tabs/es/ScrollableTabBarNode.js");
/* harmony import */ var _SaveRef__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./SaveRef */ "./node_modules/rc-tabs/es/SaveRef.js");






/* eslint-disable react/prefer-stateless-function */









var ScrollableInkTabBar = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(ScrollableInkTabBar, _React$Component);

  function ScrollableInkTabBar() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, ScrollableInkTabBar);

    return babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, (ScrollableInkTabBar.__proto__ || Object.getPrototypeOf(ScrollableInkTabBar)).apply(this, arguments));
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(ScrollableInkTabBar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          renderTabBarNode = _props.children,
          restProps = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default()(_props, ['children']);

      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_SaveRef__WEBPACK_IMPORTED_MODULE_12__["default"], null, function (saveRef, getRef) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_TabBarRootNode__WEBPACK_IMPORTED_MODULE_10__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          saveRef: saveRef
        }, restProps), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_ScrollableTabBarNode__WEBPACK_IMPORTED_MODULE_11__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          saveRef: saveRef,
          getRef: getRef
        }, restProps), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_TabBarTabsNode__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          saveRef: saveRef,
          renderTabBarNode: renderTabBarNode
        }, restProps)), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_InkTabBarNode__WEBPACK_IMPORTED_MODULE_8__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
          saveRef: saveRef,
          getRef: getRef
        }, restProps))));
      });
    }
  }]);

  return ScrollableInkTabBar;
}(react__WEBPACK_IMPORTED_MODULE_6___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (ScrollableInkTabBar);
ScrollableInkTabBar.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFicy9lcy9TY3JvbGxhYmxlSW5rVGFiQmFyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFicy9lcy9TY3JvbGxhYmxlSW5rVGFiQmFyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuLyogZXNsaW50LWRpc2FibGUgcmVhY3QvcHJlZmVyLXN0YXRlbGVzcy1mdW5jdGlvbiAqL1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgSW5rVGFiQmFyTm9kZSBmcm9tICcuL0lua1RhYkJhck5vZGUnO1xuaW1wb3J0IFRhYkJhclRhYnNOb2RlIGZyb20gJy4vVGFiQmFyVGFic05vZGUnO1xuaW1wb3J0IFRhYkJhclJvb3ROb2RlIGZyb20gJy4vVGFiQmFyUm9vdE5vZGUnO1xuaW1wb3J0IFNjcm9sbGFibGVUYWJCYXJOb2RlIGZyb20gJy4vU2Nyb2xsYWJsZVRhYkJhck5vZGUnO1xuaW1wb3J0IFNhdmVSZWYgZnJvbSAnLi9TYXZlUmVmJztcblxudmFyIFNjcm9sbGFibGVJbmtUYWJCYXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoU2Nyb2xsYWJsZUlua1RhYkJhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU2Nyb2xsYWJsZUlua1RhYkJhcigpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2Nyb2xsYWJsZUlua1RhYkJhcik7XG5cbiAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKFNjcm9sbGFibGVJbmtUYWJCYXIuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihTY3JvbGxhYmxlSW5rVGFiQmFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoU2Nyb2xsYWJsZUlua1RhYkJhciwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICByZW5kZXJUYWJCYXJOb2RlID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIHJlc3RQcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsnY2hpbGRyZW4nXSk7XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBTYXZlUmVmLFxuICAgICAgICBudWxsLFxuICAgICAgICBmdW5jdGlvbiAoc2F2ZVJlZiwgZ2V0UmVmKSB7XG4gICAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBUYWJCYXJSb290Tm9kZSxcbiAgICAgICAgICAgIF9leHRlbmRzKHsgc2F2ZVJlZjogc2F2ZVJlZiB9LCByZXN0UHJvcHMpLFxuICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgU2Nyb2xsYWJsZVRhYkJhck5vZGUsXG4gICAgICAgICAgICAgIF9leHRlbmRzKHsgc2F2ZVJlZjogc2F2ZVJlZiwgZ2V0UmVmOiBnZXRSZWYgfSwgcmVzdFByb3BzKSxcbiAgICAgICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChUYWJCYXJUYWJzTm9kZSwgX2V4dGVuZHMoeyBzYXZlUmVmOiBzYXZlUmVmLCByZW5kZXJUYWJCYXJOb2RlOiByZW5kZXJUYWJCYXJOb2RlIH0sIHJlc3RQcm9wcykpLFxuICAgICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KElua1RhYkJhck5vZGUsIF9leHRlbmRzKHsgc2F2ZVJlZjogc2F2ZVJlZiwgZ2V0UmVmOiBnZXRSZWYgfSwgcmVzdFByb3BzKSlcbiAgICAgICAgICAgIClcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBTY3JvbGxhYmxlSW5rVGFiQmFyO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5leHBvcnQgZGVmYXVsdCBTY3JvbGxhYmxlSW5rVGFiQmFyO1xuXG5cblNjcm9sbGFibGVJbmtUYWJCYXIucHJvcFR5cGVzID0ge1xuICBjaGlsZHJlbjogUHJvcFR5cGVzLmZ1bmNcbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUlBO0FBRUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUF2QkE7QUFDQTtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tabs/es/ScrollableInkTabBar.js
