__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasClass", function() { return hasClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addClass", function() { return addClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeClass", function() { return removeClass; });
function hasClass(node, className) {
  if (node.classList) {
    return node.classList.contains(className);
  }

  var originClass = node.className;
  return " ".concat(originClass, " ").indexOf(" ".concat(className, " ")) > -1;
}
function addClass(node, className) {
  if (node.classList) {
    node.classList.add(className);
  } else {
    if (!hasClass(node, className)) {
      node.className = "".concat(node.className, " ").concat(className);
    }
  }
}
function removeClass(node, className) {
  if (node.classList) {
    node.classList.remove(className);
  } else {
    if (hasClass(node, className)) {
      var originClass = node.className;
      node.className = " ".concat(originClass, " ").replace(" ".concat(className, " "), ' ');
    }
  }
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9lcy9Eb20vY2xhc3MuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11dGlsL2VzL0RvbS9jbGFzcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gaGFzQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKSB7XG4gIGlmIChub2RlLmNsYXNzTGlzdCkge1xuICAgIHJldHVybiBub2RlLmNsYXNzTGlzdC5jb250YWlucyhjbGFzc05hbWUpO1xuICB9XG5cbiAgdmFyIG9yaWdpbkNsYXNzID0gbm9kZS5jbGFzc05hbWU7XG4gIHJldHVybiBcIiBcIi5jb25jYXQob3JpZ2luQ2xhc3MsIFwiIFwiKS5pbmRleE9mKFwiIFwiLmNvbmNhdChjbGFzc05hbWUsIFwiIFwiKSkgPiAtMTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBhZGRDbGFzcyhub2RlLCBjbGFzc05hbWUpIHtcbiAgaWYgKG5vZGUuY2xhc3NMaXN0KSB7XG4gICAgbm9kZS5jbGFzc0xpc3QuYWRkKGNsYXNzTmFtZSk7XG4gIH0gZWxzZSB7XG4gICAgaWYgKCFoYXNDbGFzcyhub2RlLCBjbGFzc05hbWUpKSB7XG4gICAgICBub2RlLmNsYXNzTmFtZSA9IFwiXCIuY29uY2F0KG5vZGUuY2xhc3NOYW1lLCBcIiBcIikuY29uY2F0KGNsYXNzTmFtZSk7XG4gICAgfVxuICB9XG59XG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKSB7XG4gIGlmIChub2RlLmNsYXNzTGlzdCkge1xuICAgIG5vZGUuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO1xuICB9IGVsc2Uge1xuICAgIGlmIChoYXNDbGFzcyhub2RlLCBjbGFzc05hbWUpKSB7XG4gICAgICB2YXIgb3JpZ2luQ2xhc3MgPSBub2RlLmNsYXNzTmFtZTtcbiAgICAgIG5vZGUuY2xhc3NOYW1lID0gXCIgXCIuY29uY2F0KG9yaWdpbkNsYXNzLCBcIiBcIikucmVwbGFjZShcIiBcIi5jb25jYXQoY2xhc3NOYW1lLCBcIiBcIiksICcgJyk7XG4gICAgfVxuICB9XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-util/es/Dom/class.js
