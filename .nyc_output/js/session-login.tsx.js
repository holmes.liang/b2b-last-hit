__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_language__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/language */ "./src/common/language.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");


var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/session-login.tsx";




var success = antd__WEBPACK_IMPORTED_MODULE_3__["Modal"].success;

var SessionLogin =
/*#__PURE__*/
function () {
  function SessionLogin() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, SessionLogin);

    this.lock = false;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(SessionLogin, [{
    key: "showConfirm",
    value: function showConfirm() {
      if (this.lock) return;
      this.lock = true;

      var _this = this;

      success({
        title: _common_language__WEBPACK_IMPORTED_MODULE_4__["default"].en("Session has expired").thai("เข้าสู่ระบบหมดอายุ").getMessage(),
        content: _common_language__WEBPACK_IMPORTED_MODULE_4__["default"].en("We will take you to login page to sign in again.").thai("เราจะพาคุณไปที่หน้าเข้าสู่ระบบเพื่อลงชื่อเข้าใช้อีกครั้ง").getMessage(),
        icon: react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
          type: "question-circle",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 21
          },
          __self: this
        }),
        onOk: function onOk() {
          _this.routerSign();
        }
      });
    }
  }, {
    key: "routerSign",
    value: function routerSign() {
      _common__WEBPACK_IMPORTED_MODULE_5__["Storage"].clear();
      location.replace("".concat(Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_AJAX_CLIENT_CONTEXT).concat(_common__WEBPACK_IMPORTED_MODULE_5__["PATH"].SIGN_IN));
    }
  }]);

  return SessionLogin;
}();

/* harmony default export */ __webpack_exports__["default"] = (new SessionLogin());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3Nlc3Npb24tbG9naW4udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3Nlc3Npb24tbG9naW4udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IEljb24sIE1vZGFsIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCBMYW5ndWFnZSBmcm9tIFwiQGNvbW1vbi9sYW5ndWFnZVwiO1xuaW1wb3J0IHsgUEFUSCwgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmNvbnN0IHsgc3VjY2VzcyB9ID0gTW9kYWw7XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5jbGFzcyBTZXNzaW9uTG9naW4ge1xuICBsb2NrOiBib29sZWFuID0gZmFsc2U7XG5cbiAgc2hvd0NvbmZpcm0oKSB7XG4gICAgaWYgKHRoaXMubG9jaykgcmV0dXJuO1xuICAgIHRoaXMubG9jayA9IHRydWU7XG4gICAgY29uc3QgX3RoaXMgPSB0aGlzO1xuICAgIHN1Y2Nlc3Moe1xuICAgICAgdGl0bGU6IExhbmd1YWdlLmVuKFwiU2Vzc2lvbiBoYXMgZXhwaXJlZFwiKS50aGFpKFwi4LmA4LiC4LmJ4Liy4Liq4Li54LmI4Lij4Liw4Lia4Lia4Lir4Lih4LiU4Lit4Liy4Lii4Li4XCIpLmdldE1lc3NhZ2UoKSxcbiAgICAgIGNvbnRlbnQ6IExhbmd1YWdlLmVuKFwiV2Ugd2lsbCB0YWtlIHlvdSB0byBsb2dpbiBwYWdlIHRvIHNpZ24gaW4gYWdhaW4uXCIpLnRoYWkoXCLguYDguKPguLLguIjguLDguJ7guLLguITguLjguJPguYTguJvguJfguLXguYjguKvguJnguYnguLLguYDguILguYnguLLguKrguLnguYjguKPguLDguJrguJrguYDguJ7guLfguYjguK3guKXguIfguIrguLfguYjguK3guYDguILguYnguLLguYPguIrguYnguK3guLXguIHguITguKPguLHguYnguIdcIikuZ2V0TWVzc2FnZSgpLFxuICAgICAgaWNvbjogPEljb24gdHlwZT1cInF1ZXN0aW9uLWNpcmNsZVwiLz4sXG4gICAgICBvbk9rKCkge1xuICAgICAgICBfdGhpcy5yb3V0ZXJTaWduKCk7XG4gICAgICB9LFxuICAgIH0pO1xuICB9O1xuXG4gIHJvdXRlclNpZ24oKSB7XG4gICAgU3RvcmFnZS5jbGVhcigpO1xuICAgIGxvY2F0aW9uLnJlcGxhY2UoYCR7cHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0FKQVhfQ0xJRU5UX0NPTlRFWFR9JHtQQVRILlNJR05fSU59YCk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgbmV3IFNlc3Npb25Mb2dpbigpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUdBOzs7Ozs7QUFDQTs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/session-login.tsx
