__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mini_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");
/* harmony import */ var mini_store__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(mini_store__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SubPopupMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SubPopupMenu */ "./node_modules/rc-menu/es/SubPopupMenu.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./util */ "./node_modules/rc-menu/es/util.js");
/* harmony import */ var _utils_legacyUtil__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils/legacyUtil */ "./node_modules/rc-menu/es/utils/legacyUtil.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var Menu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Menu, _React$Component);

  function Menu(props) {
    var _this;

    _classCallCheck(this, Menu);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Menu).call(this, props));

    _this.onSelect = function (selectInfo) {
      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props;

      if (props.selectable) {
        // root menu
        var _this$store$getState = _this.store.getState(),
            _selectedKeys = _this$store$getState.selectedKeys;

        var selectedKey = selectInfo.key;

        if (props.multiple) {
          _selectedKeys = _selectedKeys.concat([selectedKey]);
        } else {
          _selectedKeys = [selectedKey];
        }

        if (!('selectedKeys' in props)) {
          _this.store.setState({
            selectedKeys: _selectedKeys
          });
        }

        props.onSelect(_objectSpread({}, selectInfo, {
          selectedKeys: _selectedKeys
        }));
      }
    };

    _this.onClick = function (e) {
      _this.props.onClick(e);
    }; // onKeyDown needs to be exposed as a instance method
    // e.g., in rc-select, we need to navigate menu item while
    // current active item is rc-select input box rather than the menu itself


    _this.onKeyDown = function (e, callback) {
      _this.innerMenu.getWrappedInstance().onKeyDown(e, callback);
    };

    _this.onOpenChange = function (event) {
      var _assertThisInitialize2 = _assertThisInitialized(_this),
          props = _assertThisInitialize2.props;

      var openKeys = _this.store.getState().openKeys.concat();

      var changed = false;

      var processSingle = function processSingle(e) {
        var oneChanged = false;

        if (e.open) {
          oneChanged = openKeys.indexOf(e.key) === -1;

          if (oneChanged) {
            openKeys.push(e.key);
          }
        } else {
          var index = openKeys.indexOf(e.key);
          oneChanged = index !== -1;

          if (oneChanged) {
            openKeys.splice(index, 1);
          }
        }

        changed = changed || oneChanged;
      };

      if (Array.isArray(event)) {
        // batch change call
        event.forEach(processSingle);
      } else {
        processSingle(event);
      }

      if (changed) {
        if (!('openKeys' in _this.props)) {
          _this.store.setState({
            openKeys: openKeys
          });
        }

        props.onOpenChange(openKeys);
      }
    };

    _this.onDeselect = function (selectInfo) {
      var _assertThisInitialize3 = _assertThisInitialized(_this),
          props = _assertThisInitialize3.props;

      if (props.selectable) {
        var _selectedKeys2 = _this.store.getState().selectedKeys.concat();

        var selectedKey = selectInfo.key;

        var index = _selectedKeys2.indexOf(selectedKey);

        if (index !== -1) {
          _selectedKeys2.splice(index, 1);
        }

        if (!('selectedKeys' in props)) {
          _this.store.setState({
            selectedKeys: _selectedKeys2
          });
        }

        props.onDeselect(_objectSpread({}, selectInfo, {
          selectedKeys: _selectedKeys2
        }));
      }
    };

    _this.getOpenTransitionName = function () {
      var _assertThisInitialize4 = _assertThisInitialized(_this),
          props = _assertThisInitialize4.props;

      var transitionName = props.openTransitionName;
      var animationName = props.openAnimation;

      if (!transitionName && typeof animationName === 'string') {
        transitionName = "".concat(props.prefixCls, "-open-").concat(animationName);
      }

      return transitionName;
    };

    _this.setInnerMenu = function (node) {
      _this.innerMenu = node;
    };

    _this.isRootMenu = true;
    var selectedKeys = props.defaultSelectedKeys;
    var openKeys = props.defaultOpenKeys;

    if ('selectedKeys' in props) {
      selectedKeys = props.selectedKeys || [];
    }

    if ('openKeys' in props) {
      openKeys = props.openKeys || [];
    }

    _this.store = Object(mini_store__WEBPACK_IMPORTED_MODULE_1__["create"])({
      selectedKeys: selectedKeys,
      openKeys: openKeys,
      activeKey: {
        '0-menu-': Object(_SubPopupMenu__WEBPACK_IMPORTED_MODULE_2__["getActiveKey"])(props, props.activeKey)
      }
    });
    return _this;
  }

  _createClass(Menu, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.updateMiniStore();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.updateMiniStore();
    }
  }, {
    key: "updateMiniStore",
    value: function updateMiniStore() {
      if ('selectedKeys' in this.props) {
        this.store.setState({
          selectedKeys: this.props.selectedKeys || []
        });
      }

      if ('openKeys' in this.props) {
        this.store.setState({
          openKeys: this.props.openKeys || []
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var props = _objectSpread({}, this.props);

      props.className += " ".concat(props.prefixCls, "-root");
      props = _objectSpread({}, props, {
        onClick: this.onClick,
        onOpenChange: this.onOpenChange,
        onDeselect: this.onDeselect,
        onSelect: this.onSelect,
        parentMenu: this,
        motion: Object(_utils_legacyUtil__WEBPACK_IMPORTED_MODULE_4__["getMotion"])(this.props)
      });
      delete props.openAnimation;
      delete props.openTransitionName;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](mini_store__WEBPACK_IMPORTED_MODULE_1__["Provider"], {
        store: this.store
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_SubPopupMenu__WEBPACK_IMPORTED_MODULE_2__["default"], Object.assign({}, props, {
        ref: this.setInnerMenu
      }), this.props.children));
    }
  }]);

  return Menu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Menu.defaultProps = {
  selectable: true,
  onClick: _util__WEBPACK_IMPORTED_MODULE_3__["noop"],
  onSelect: _util__WEBPACK_IMPORTED_MODULE_3__["noop"],
  onOpenChange: _util__WEBPACK_IMPORTED_MODULE_3__["noop"],
  onDeselect: _util__WEBPACK_IMPORTED_MODULE_3__["noop"],
  defaultSelectedKeys: [],
  defaultOpenKeys: [],
  subMenuOpenDelay: 0.1,
  subMenuCloseDelay: 0.1,
  triggerSubMenuAction: 'hover',
  prefixCls: 'rc-menu',
  className: '',
  mode: 'vertical',
  style: {},
  builtinPlacements: {},
  overflowedIndicator: react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, "\xB7\xB7\xB7")
};
/* harmony default export */ __webpack_exports__["default"] = (Menu);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy9NZW51LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy9NZW51LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmZ1bmN0aW9uIG93bktleXMob2JqZWN0LCBlbnVtZXJhYmxlT25seSkgeyB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iamVjdCk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzeW1ib2xzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhvYmplY3QpOyBpZiAoZW51bWVyYWJsZU9ubHkpIHN5bWJvbHMgPSBzeW1ib2xzLmZpbHRlcihmdW5jdGlvbiAoc3ltKSB7IHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgc3ltKS5lbnVtZXJhYmxlOyB9KTsga2V5cy5wdXNoLmFwcGx5KGtleXMsIHN5bWJvbHMpOyB9IHJldHVybiBrZXlzOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RTcHJlYWQodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV0gIT0gbnVsbCA/IGFyZ3VtZW50c1tpXSA6IHt9OyBpZiAoaSAlIDIpIHsgb3duS2V5cyhzb3VyY2UsIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhzb3VyY2UpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBrZXkpKTsgfSk7IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoY2FsbCAmJiAoX3R5cGVvZihjYWxsKSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSkgeyByZXR1cm4gY2FsbDsgfSByZXR1cm4gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKTsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb25cIik7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKTsgfVxuXG5mdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBfc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgby5fX3Byb3RvX18gPSBwOyByZXR1cm4gbzsgfTsgcmV0dXJuIF9zZXRQcm90b3R5cGVPZihvLCBwKTsgfVxuXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBQcm92aWRlciwgY3JlYXRlIH0gZnJvbSAnbWluaS1zdG9yZSc7XG5pbXBvcnQgU3ViUG9wdXBNZW51LCB7IGdldEFjdGl2ZUtleSB9IGZyb20gJy4vU3ViUG9wdXBNZW51JztcbmltcG9ydCB7IG5vb3AgfSBmcm9tICcuL3V0aWwnO1xuaW1wb3J0IHsgZ2V0TW90aW9uIH0gZnJvbSAnLi91dGlscy9sZWdhY3lVdGlsJztcblxudmFyIE1lbnUgPVxuLyojX19QVVJFX18qL1xuZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKE1lbnUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1lbnUocHJvcHMpIHtcbiAgICB2YXIgX3RoaXM7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTWVudSk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihNZW51KS5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICBfdGhpcy5vblNlbGVjdCA9IGZ1bmN0aW9uIChzZWxlY3RJbmZvKSB7XG4gICAgICB2YXIgX2Fzc2VydFRoaXNJbml0aWFsaXplID0gX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksXG4gICAgICAgICAgcHJvcHMgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUucHJvcHM7XG5cbiAgICAgIGlmIChwcm9wcy5zZWxlY3RhYmxlKSB7XG4gICAgICAgIC8vIHJvb3QgbWVudVxuICAgICAgICB2YXIgX3RoaXMkc3RvcmUkZ2V0U3RhdGUgPSBfdGhpcy5zdG9yZS5nZXRTdGF0ZSgpLFxuICAgICAgICAgICAgX3NlbGVjdGVkS2V5cyA9IF90aGlzJHN0b3JlJGdldFN0YXRlLnNlbGVjdGVkS2V5cztcblxuICAgICAgICB2YXIgc2VsZWN0ZWRLZXkgPSBzZWxlY3RJbmZvLmtleTtcblxuICAgICAgICBpZiAocHJvcHMubXVsdGlwbGUpIHtcbiAgICAgICAgICBfc2VsZWN0ZWRLZXlzID0gX3NlbGVjdGVkS2V5cy5jb25jYXQoW3NlbGVjdGVkS2V5XSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgX3NlbGVjdGVkS2V5cyA9IFtzZWxlY3RlZEtleV07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoISgnc2VsZWN0ZWRLZXlzJyBpbiBwcm9wcykpIHtcbiAgICAgICAgICBfdGhpcy5zdG9yZS5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBzZWxlY3RlZEtleXM6IF9zZWxlY3RlZEtleXNcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb3BzLm9uU2VsZWN0KF9vYmplY3RTcHJlYWQoe30sIHNlbGVjdEluZm8sIHtcbiAgICAgICAgICBzZWxlY3RlZEtleXM6IF9zZWxlY3RlZEtleXNcbiAgICAgICAgfSkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbkNsaWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIF90aGlzLnByb3BzLm9uQ2xpY2soZSk7XG4gICAgfTsgLy8gb25LZXlEb3duIG5lZWRzIHRvIGJlIGV4cG9zZWQgYXMgYSBpbnN0YW5jZSBtZXRob2RcbiAgICAvLyBlLmcuLCBpbiByYy1zZWxlY3QsIHdlIG5lZWQgdG8gbmF2aWdhdGUgbWVudSBpdGVtIHdoaWxlXG4gICAgLy8gY3VycmVudCBhY3RpdmUgaXRlbSBpcyByYy1zZWxlY3QgaW5wdXQgYm94IHJhdGhlciB0aGFuIHRoZSBtZW51IGl0c2VsZlxuXG5cbiAgICBfdGhpcy5vbktleURvd24gPSBmdW5jdGlvbiAoZSwgY2FsbGJhY2spIHtcbiAgICAgIF90aGlzLmlubmVyTWVudS5nZXRXcmFwcGVkSW5zdGFuY2UoKS5vbktleURvd24oZSwgY2FsbGJhY2spO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbk9wZW5DaGFuZ2UgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBfYXNzZXJ0VGhpc0luaXRpYWxpemUyID0gX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksXG4gICAgICAgICAgcHJvcHMgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUyLnByb3BzO1xuXG4gICAgICB2YXIgb3BlbktleXMgPSBfdGhpcy5zdG9yZS5nZXRTdGF0ZSgpLm9wZW5LZXlzLmNvbmNhdCgpO1xuXG4gICAgICB2YXIgY2hhbmdlZCA9IGZhbHNlO1xuXG4gICAgICB2YXIgcHJvY2Vzc1NpbmdsZSA9IGZ1bmN0aW9uIHByb2Nlc3NTaW5nbGUoZSkge1xuICAgICAgICB2YXIgb25lQ2hhbmdlZCA9IGZhbHNlO1xuXG4gICAgICAgIGlmIChlLm9wZW4pIHtcbiAgICAgICAgICBvbmVDaGFuZ2VkID0gb3BlbktleXMuaW5kZXhPZihlLmtleSkgPT09IC0xO1xuXG4gICAgICAgICAgaWYgKG9uZUNoYW5nZWQpIHtcbiAgICAgICAgICAgIG9wZW5LZXlzLnB1c2goZS5rZXkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YXIgaW5kZXggPSBvcGVuS2V5cy5pbmRleE9mKGUua2V5KTtcbiAgICAgICAgICBvbmVDaGFuZ2VkID0gaW5kZXggIT09IC0xO1xuXG4gICAgICAgICAgaWYgKG9uZUNoYW5nZWQpIHtcbiAgICAgICAgICAgIG9wZW5LZXlzLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY2hhbmdlZCA9IGNoYW5nZWQgfHwgb25lQ2hhbmdlZDtcbiAgICAgIH07XG5cbiAgICAgIGlmIChBcnJheS5pc0FycmF5KGV2ZW50KSkge1xuICAgICAgICAvLyBiYXRjaCBjaGFuZ2UgY2FsbFxuICAgICAgICBldmVudC5mb3JFYWNoKHByb2Nlc3NTaW5nbGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcHJvY2Vzc1NpbmdsZShldmVudCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChjaGFuZ2VkKSB7XG4gICAgICAgIGlmICghKCdvcGVuS2V5cycgaW4gX3RoaXMucHJvcHMpKSB7XG4gICAgICAgICAgX3RoaXMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgICAgICAgb3BlbktleXM6IG9wZW5LZXlzXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBwcm9wcy5vbk9wZW5DaGFuZ2Uob3BlbktleXMpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbkRlc2VsZWN0ID0gZnVuY3Rpb24gKHNlbGVjdEluZm8pIHtcbiAgICAgIHZhciBfYXNzZXJ0VGhpc0luaXRpYWxpemUzID0gX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksXG4gICAgICAgICAgcHJvcHMgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUzLnByb3BzO1xuXG4gICAgICBpZiAocHJvcHMuc2VsZWN0YWJsZSkge1xuICAgICAgICB2YXIgX3NlbGVjdGVkS2V5czIgPSBfdGhpcy5zdG9yZS5nZXRTdGF0ZSgpLnNlbGVjdGVkS2V5cy5jb25jYXQoKTtcblxuICAgICAgICB2YXIgc2VsZWN0ZWRLZXkgPSBzZWxlY3RJbmZvLmtleTtcblxuICAgICAgICB2YXIgaW5kZXggPSBfc2VsZWN0ZWRLZXlzMi5pbmRleE9mKHNlbGVjdGVkS2V5KTtcblxuICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgX3NlbGVjdGVkS2V5czIuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICghKCdzZWxlY3RlZEtleXMnIGluIHByb3BzKSkge1xuICAgICAgICAgIF90aGlzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICAgIHNlbGVjdGVkS2V5czogX3NlbGVjdGVkS2V5czJcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHByb3BzLm9uRGVzZWxlY3QoX29iamVjdFNwcmVhZCh7fSwgc2VsZWN0SW5mbywge1xuICAgICAgICAgIHNlbGVjdGVkS2V5czogX3NlbGVjdGVkS2V5czJcbiAgICAgICAgfSkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5nZXRPcGVuVHJhbnNpdGlvbk5hbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX2Fzc2VydFRoaXNJbml0aWFsaXplNCA9IF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLFxuICAgICAgICAgIHByb3BzID0gX2Fzc2VydFRoaXNJbml0aWFsaXplNC5wcm9wcztcblxuICAgICAgdmFyIHRyYW5zaXRpb25OYW1lID0gcHJvcHMub3BlblRyYW5zaXRpb25OYW1lO1xuICAgICAgdmFyIGFuaW1hdGlvbk5hbWUgPSBwcm9wcy5vcGVuQW5pbWF0aW9uO1xuXG4gICAgICBpZiAoIXRyYW5zaXRpb25OYW1lICYmIHR5cGVvZiBhbmltYXRpb25OYW1lID09PSAnc3RyaW5nJykge1xuICAgICAgICB0cmFuc2l0aW9uTmFtZSA9IFwiXCIuY29uY2F0KHByb3BzLnByZWZpeENscywgXCItb3Blbi1cIikuY29uY2F0KGFuaW1hdGlvbk5hbWUpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJhbnNpdGlvbk5hbWU7XG4gICAgfTtcblxuICAgIF90aGlzLnNldElubmVyTWVudSA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICBfdGhpcy5pbm5lck1lbnUgPSBub2RlO1xuICAgIH07XG5cbiAgICBfdGhpcy5pc1Jvb3RNZW51ID0gdHJ1ZTtcbiAgICB2YXIgc2VsZWN0ZWRLZXlzID0gcHJvcHMuZGVmYXVsdFNlbGVjdGVkS2V5cztcbiAgICB2YXIgb3BlbktleXMgPSBwcm9wcy5kZWZhdWx0T3BlbktleXM7XG5cbiAgICBpZiAoJ3NlbGVjdGVkS2V5cycgaW4gcHJvcHMpIHtcbiAgICAgIHNlbGVjdGVkS2V5cyA9IHByb3BzLnNlbGVjdGVkS2V5cyB8fCBbXTtcbiAgICB9XG5cbiAgICBpZiAoJ29wZW5LZXlzJyBpbiBwcm9wcykge1xuICAgICAgb3BlbktleXMgPSBwcm9wcy5vcGVuS2V5cyB8fCBbXTtcbiAgICB9XG5cbiAgICBfdGhpcy5zdG9yZSA9IGNyZWF0ZSh7XG4gICAgICBzZWxlY3RlZEtleXM6IHNlbGVjdGVkS2V5cyxcbiAgICAgIG9wZW5LZXlzOiBvcGVuS2V5cyxcbiAgICAgIGFjdGl2ZUtleToge1xuICAgICAgICAnMC1tZW51LSc6IGdldEFjdGl2ZUtleShwcm9wcywgcHJvcHMuYWN0aXZlS2V5KVxuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhNZW51LCBbe1xuICAgIGtleTogXCJjb21wb25lbnREaWRNb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHRoaXMudXBkYXRlTWluaVN0b3JlKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImNvbXBvbmVudERpZFVwZGF0ZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICB0aGlzLnVwZGF0ZU1pbmlTdG9yZSgpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJ1cGRhdGVNaW5pU3RvcmVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gdXBkYXRlTWluaVN0b3JlKCkge1xuICAgICAgaWYgKCdzZWxlY3RlZEtleXMnIGluIHRoaXMucHJvcHMpIHtcbiAgICAgICAgdGhpcy5zdG9yZS5zZXRTdGF0ZSh7XG4gICAgICAgICAgc2VsZWN0ZWRLZXlzOiB0aGlzLnByb3BzLnNlbGVjdGVkS2V5cyB8fCBbXVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgaWYgKCdvcGVuS2V5cycgaW4gdGhpcy5wcm9wcykge1xuICAgICAgICB0aGlzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICBvcGVuS2V5czogdGhpcy5wcm9wcy5vcGVuS2V5cyB8fCBbXVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBwcm9wcyA9IF9vYmplY3RTcHJlYWQoe30sIHRoaXMucHJvcHMpO1xuXG4gICAgICBwcm9wcy5jbGFzc05hbWUgKz0gXCIgXCIuY29uY2F0KHByb3BzLnByZWZpeENscywgXCItcm9vdFwiKTtcbiAgICAgIHByb3BzID0gX29iamVjdFNwcmVhZCh7fSwgcHJvcHMsIHtcbiAgICAgICAgb25DbGljazogdGhpcy5vbkNsaWNrLFxuICAgICAgICBvbk9wZW5DaGFuZ2U6IHRoaXMub25PcGVuQ2hhbmdlLFxuICAgICAgICBvbkRlc2VsZWN0OiB0aGlzLm9uRGVzZWxlY3QsXG4gICAgICAgIG9uU2VsZWN0OiB0aGlzLm9uU2VsZWN0LFxuICAgICAgICBwYXJlbnRNZW51OiB0aGlzLFxuICAgICAgICBtb3Rpb246IGdldE1vdGlvbih0aGlzLnByb3BzKVxuICAgICAgfSk7XG4gICAgICBkZWxldGUgcHJvcHMub3BlbkFuaW1hdGlvbjtcbiAgICAgIGRlbGV0ZSBwcm9wcy5vcGVuVHJhbnNpdGlvbk5hbWU7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChQcm92aWRlciwge1xuICAgICAgICBzdG9yZTogdGhpcy5zdG9yZVxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChTdWJQb3B1cE1lbnUsIE9iamVjdC5hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgIHJlZjogdGhpcy5zZXRJbm5lck1lbnVcbiAgICAgIH0pLCB0aGlzLnByb3BzLmNoaWxkcmVuKSk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIE1lbnU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbk1lbnUuZGVmYXVsdFByb3BzID0ge1xuICBzZWxlY3RhYmxlOiB0cnVlLFxuICBvbkNsaWNrOiBub29wLFxuICBvblNlbGVjdDogbm9vcCxcbiAgb25PcGVuQ2hhbmdlOiBub29wLFxuICBvbkRlc2VsZWN0OiBub29wLFxuICBkZWZhdWx0U2VsZWN0ZWRLZXlzOiBbXSxcbiAgZGVmYXVsdE9wZW5LZXlzOiBbXSxcbiAgc3ViTWVudU9wZW5EZWxheTogMC4xLFxuICBzdWJNZW51Q2xvc2VEZWxheTogMC4xLFxuICB0cmlnZ2VyU3ViTWVudUFjdGlvbjogJ2hvdmVyJyxcbiAgcHJlZml4Q2xzOiAncmMtbWVudScsXG4gIGNsYXNzTmFtZTogJycsXG4gIG1vZGU6ICd2ZXJ0aWNhbCcsXG4gIHN0eWxlOiB7fSxcbiAgYnVpbHRpblBsYWNlbWVudHM6IHt9LFxuICBvdmVyZmxvd2VkSW5kaWNhdG9yOiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCBudWxsLCBcIlxceEI3XFx4QjdcXHhCN1wiKVxufTtcbmV4cG9ydCBkZWZhdWx0IE1lbnU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQWRBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBREE7QUFHQTtBQXJCQTtBQUNBO0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFrQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-menu/es/Menu.js
