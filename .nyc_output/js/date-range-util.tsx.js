__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterDateRanges", function() { return FilterDateRanges; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilterOptions", function() { return getFilterOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDateRangeByType", function() { return getDateRangeByType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appendDefaultDataRange", function() { return appendDefaultDataRange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appendDefaultParmas", function() { return appendDefaultParmas; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/utils */ "./src/common/utils.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");



var FilterDateRanges;

(function (FilterDateRanges) {
  FilterDateRanges["OVER_DUE"] = "OVER_DUE";
  FilterDateRanges["PAST_1_HOUR"] = "PAST_1_HOUR";
  FilterDateRanges["PAST_12_HOUR"] = "PAST_12_HOUR";
  FilterDateRanges["THIS_WEEK"] = "THIS_WEEK";
  FilterDateRanges["PAST_10_DAYS"] = "PAST_10_DAYS";
  FilterDateRanges["TODAY"] = "TODAY";
  FilterDateRanges["LAST_10_DAYS"] = "LAST_10_DAYS";
  FilterDateRanges["LAST_ONE_WEEK"] = "LAST_ONE_WEEK";
  FilterDateRanges["LAST_TWO_WEEK"] = "LAST_TWO_WEEK";
  FilterDateRanges["LAST_ONE_MONTH"] = "LAST_ONE_MONTH";
  FilterDateRanges["THIS_YEAR"] = "THIS_YEAR";
  FilterDateRanges["CUSTOM_DATE"] = "CUSTOM_DATE";
  FilterDateRanges["OVER_DUE_5_DAYS"] = "OVER_DUE_5_DAYS";
  FilterDateRanges["THIS_MONTH"] = "THIS_MONTH";
  FilterDateRanges["LAST_3_MONTHS"] = "LAST_3_MONTHS";
  FilterDateRanges["LAST_6_MONTHS"] = "LAST_6_MONTHS";
  FilterDateRanges["LAST_12_MONTHS"] = "LAST_12_MONTHS";
  FilterDateRanges["PAST_3_DAYS"] = "PAST_3_DAYS";
  FilterDateRanges["IN_1_WEEK"] = "IN_1_WEEK";
  FilterDateRanges["IN_1_MONTH"] = "IN_1_MONTH";
})(FilterDateRanges || (FilterDateRanges = {}));

var allRangeOptions = [{
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Past 1 Hour").thai("Past 1 Hour").getMessage(),
  value: FilterDateRanges.PAST_1_HOUR
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Past 12 Hour").thai("Past 12 Hour").getMessage(),
  value: FilterDateRanges.PAST_12_HOUR
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("This Week").thai("This Week").getMessage(),
  value: FilterDateRanges.THIS_WEEK
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Past 10 days").thai("Past 3 days").getMessage(),
  value: FilterDateRanges.PAST_10_DAYS
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Today").thai("วันนี้").getMessage(),
  value: FilterDateRanges.TODAY
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Last 1 week").thai("สัปดาห์ที่แล้ว").getMessage(),
  value: FilterDateRanges.LAST_ONE_WEEK
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Last 2 weeks").thai("สองสัปดาห์ที่แล้ว").getMessage(),
  value: FilterDateRanges.LAST_TWO_WEEK
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Last 1 month").thai("เดือนที่แล้ว").getMessage(),
  value: FilterDateRanges.LAST_ONE_MONTH
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("This Month").thai("This month").getMessage(),
  value: FilterDateRanges.THIS_MONTH
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Last 3 Months").thai("Last 3 Months").getMessage(),
  value: FilterDateRanges.LAST_3_MONTHS
}, {
  label: "This Year",
  value: FilterDateRanges.THIS_YEAR
}, {
  label: "5 Days",
  value: FilterDateRanges.OVER_DUE_5_DAYS
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Overdue").thai("เกินกำหนด").getMessage(),
  value: FilterDateRanges.OVER_DUE
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Past 3 days").thai("Past 3 days").getMessage(),
  value: FilterDateRanges.PAST_3_DAYS
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Past 3 days").thai("Past 3 days").getMessage(),
  value: FilterDateRanges.PAST_3_DAYS
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("In 1 week").thai("In 1 week").getMessage(),
  value: FilterDateRanges.IN_1_WEEK
}, {
  label: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("In 1 month").thai("In 1 month").getMessage(),
  value: FilterDateRanges.IN_1_MONTH
}];
var calcRules = new Map();
calcRules.set(FilterDateRanges.PAST_1_HOUR, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXHoursFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].now()
  };
});
calcRules.set(FilterDateRanges.PAST_12_HOUR, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXHoursFromNow(12),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].now()
  };
});
calcRules.set(FilterDateRanges.THIS_WEEK, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(7),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.PAST_10_DAYS, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(10),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.OVER_DUE, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.LAST_ONE_WEEK, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXWeeksFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.LAST_TWO_WEEK, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXWeeksFromNow(2),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.LAST_ONE_MONTH, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXMonthsFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.THIS_MONTH, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXMonthsFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.LAST_3_MONTHS, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXMonthsFromNow(3),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.PAST_3_DAYS, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(2),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
calcRules.set(FilterDateRanges.IN_1_WEEK, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].plusXDaysFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].plusXWeeksFromNow(1)
  };
});
calcRules.set(FilterDateRanges.IN_1_MONTH, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].plusXDaysFromNow(1),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].plusXMonthsFromNow(1)
  };
});
calcRules.set(FilterDateRanges.THIS_YEAR, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].nowWithTimeAtStartOfYear(),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].nowWithTimeAtEndOfYear()
  };
});
calcRules.set(FilterDateRanges.OVER_DUE_5_DAYS, function () {
  return {
    from: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].nowWithTimeAtStartOfDay(),
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].plusXDaysFromNow(5)
  };
});
calcRules.set(FilterDateRanges.OVER_DUE, function () {
  return {
    from: "",
    to: _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].minusXDaysFromNow(1)
  };
});
function getFilterOptions(rangeTypes) {
  var ret = [];
  rangeTypes.forEach(function (item) {
    var option = allRangeOptions.find(function (range) {
      return range.value === item;
    });

    if (option) {
      ret.push(option);
    }
  });
  return ret;
}
function getDateRangeByType(rangeType) {
  if (!rangeType) {
    return {};
  }

  var isHour = [FilterDateRanges.PAST_1_HOUR, FilterDateRanges.PAST_12_HOUR].includes(rangeType);
  var calcRule = calcRules.get(rangeType);
  var from = _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].now();
  var to = _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].now();

  var ret = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({
    from: from,
    to: to
  }, calcRule && calcRule());

  if (isHour) {
    return {
      from: ret.from ? _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].formatDateTimeWithTimeZone(ret.from) : null,
      to: ret.to ? _common__WEBPACK_IMPORTED_MODULE_2__["DateUtils"].formatDateTimeWithTimeZone(ret.to) : null
    };
  }

  return {
    from: _common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].stringifyDate(ret.from.startOf && ret.from.startOf("day")),
    to: _common_utils__WEBPACK_IMPORTED_MODULE_1__["default"].stringifyDate(ret.to.endOf("day"))
  };
}
function appendDefaultDataRange(params, fields) {
  if (!fields.length) {
    return params;
  }

  var ret = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, params);

  return fields.reduce(function (acc, cur) {
    var dateRange = acc[cur];

    if (!dateRange || !dateRange.from) {
      acc[cur] = getDateRangeByType(FilterDateRanges.OVER_DUE);
    }

    return acc;
  }, ret);
}
function appendDefaultParmas(params, defaultParams) {
  var fields = Object.keys(defaultParams);

  if (!fields.length) {
    return params;
  }

  var ret = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, params);

  return fields.reduce(function (acc, cur) {
    if (acc[cur] === undefined) {
      var value = defaultParams[cur];
      var isDateRange = FilterDateRanges[value];
      acc[cur] = isDateRange ? getDateRangeByType(value) : value;
    }

    if (acc[cur] && acc[cur].type && FilterDateRanges[acc[cur].type]) {
      acc[cur] = getDateRangeByType(acc[cur].type);
    }

    return acc;
  }, ret);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZpbHRlci9kYXRlLXJhbmdlLXV0aWwudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZpbHRlci9kYXRlLXJhbmdlLXV0aWwudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB1dGlscyBmcm9tIFwiQGNvbW1vbi91dGlsc1wiO1xuaW1wb3J0IHsgRGF0ZVV0aWxzLCBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5cbnR5cGUgRmlsdGVyRGF0ZVJhbmdlT3B0aW9uID0ge1xuICBsYWJlbDogc3RyaW5nO1xuICB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcztcbn1cblxuZXhwb3J0IGVudW0gRmlsdGVyRGF0ZVJhbmdlcyB7XG4gIE9WRVJfRFVFID0gXCJPVkVSX0RVRVwiLFxuICBQQVNUXzFfSE9VUiA9IFwiUEFTVF8xX0hPVVJcIixcbiAgUEFTVF8xMl9IT1VSID0gXCJQQVNUXzEyX0hPVVJcIixcbiAgVEhJU19XRUVLID0gXCJUSElTX1dFRUtcIixcbiAgUEFTVF8xMF9EQVlTID0gXCJQQVNUXzEwX0RBWVNcIixcbiAgVE9EQVkgPSBcIlRPREFZXCIsXG4gIExBU1RfMTBfREFZUyA9IFwiTEFTVF8xMF9EQVlTXCIsXG4gIExBU1RfT05FX1dFRUsgPSBcIkxBU1RfT05FX1dFRUtcIixcbiAgTEFTVF9UV09fV0VFSyA9IFwiTEFTVF9UV09fV0VFS1wiLFxuICBMQVNUX09ORV9NT05USCA9IFwiTEFTVF9PTkVfTU9OVEhcIixcbiAgVEhJU19ZRUFSID0gXCJUSElTX1lFQVJcIixcbiAgQ1VTVE9NX0RBVEUgPSBcIkNVU1RPTV9EQVRFXCIsXG4gIE9WRVJfRFVFXzVfREFZUyA9IFwiT1ZFUl9EVUVfNV9EQVlTXCIsXG5cbiAgVEhJU19NT05USCA9IFwiVEhJU19NT05USFwiLFxuICBMQVNUXzNfTU9OVEhTID0gXCJMQVNUXzNfTU9OVEhTXCIsXG4gIExBU1RfNl9NT05USFMgPSBcIkxBU1RfNl9NT05USFNcIixcbiAgTEFTVF8xMl9NT05USFMgPSBcIkxBU1RfMTJfTU9OVEhTXCIsXG4gIFBBU1RfM19EQVlTID0gXCJQQVNUXzNfREFZU1wiLFxuXG4gIElOXzFfV0VFSyA9IFwiSU5fMV9XRUVLXCIsXG4gIElOXzFfTU9OVEggPSBcIklOXzFfTU9OVEhcIixcbn1cblxuY29uc3QgYWxsUmFuZ2VPcHRpb25zOiBGaWx0ZXJEYXRlUmFuZ2VPcHRpb25bXSA9IFtcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJQYXN0IDEgSG91clwiKS50aGFpKFwiUGFzdCAxIEhvdXJcIikuZ2V0TWVzc2FnZSgpLCB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcy5QQVNUXzFfSE9VUiB9LFxuICB7IGxhYmVsOiBMYW5ndWFnZS5lbihcIlBhc3QgMTIgSG91clwiKS50aGFpKFwiUGFzdCAxMiBIb3VyXCIpLmdldE1lc3NhZ2UoKSwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuUEFTVF8xMl9IT1VSIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiVGhpcyBXZWVrXCIpLnRoYWkoXCJUaGlzIFdlZWtcIikuZ2V0TWVzc2FnZSgpLCB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcy5USElTX1dFRUsgfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJQYXN0IDEwIGRheXNcIikudGhhaShcIlBhc3QgMyBkYXlzXCIpLmdldE1lc3NhZ2UoKSwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuUEFTVF8xMF9EQVlTIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiVG9kYXlcIikudGhhaShcIuC4p+C4seC4meC4meC4teC5iVwiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLlRPREFZIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiTGFzdCAxIHdlZWtcIikudGhhaShcIuC4quC4seC4m+C4lOC4suC4q+C5jOC4l+C4teC5iOC5geC4peC5ieC4p1wiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX1dFRUsgfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJMYXN0IDIgd2Vla3NcIikudGhhaShcIuC4quC4reC4h+C4quC4seC4m+C4lOC4suC4q+C5jOC4l+C4teC5iOC5geC4peC5ieC4p1wiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfVFdPX1dFRUsgfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJMYXN0IDEgbW9udGhcIikudGhhaShcIuC5gOC4lOC4t+C4reC4meC4l+C4teC5iOC5geC4peC5ieC4p1wiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX01PTlRIIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiVGhpcyBNb250aFwiKS50aGFpKFwiVGhpcyBtb250aFwiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLlRISVNfTU9OVEggfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJMYXN0IDMgTW9udGhzXCIpLnRoYWkoXCJMYXN0IDMgTW9udGhzXCIpLmdldE1lc3NhZ2UoKSwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuTEFTVF8zX01PTlRIUyB9LFxuICB7IGxhYmVsOiBcIlRoaXMgWWVhclwiLCB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcy5USElTX1lFQVIgfSxcbiAgeyBsYWJlbDogXCI1IERheXNcIiwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuT1ZFUl9EVUVfNV9EQVlTIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiT3ZlcmR1ZVwiKS50aGFpKFwi4LmA4LiB4Li04LiZ4LiB4Liz4Lir4LiZ4LiUXCIpLmdldE1lc3NhZ2UoKSwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuT1ZFUl9EVUUgfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJQYXN0IDMgZGF5c1wiKS50aGFpKFwiUGFzdCAzIGRheXNcIikuZ2V0TWVzc2FnZSgpLCB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcy5QQVNUXzNfREFZUyB9LFxuICB7IGxhYmVsOiBMYW5ndWFnZS5lbihcIlBhc3QgMyBkYXlzXCIpLnRoYWkoXCJQYXN0IDMgZGF5c1wiKS5nZXRNZXNzYWdlKCksIHZhbHVlOiBGaWx0ZXJEYXRlUmFuZ2VzLlBBU1RfM19EQVlTIH0sXG4gIHsgbGFiZWw6IExhbmd1YWdlLmVuKFwiSW4gMSB3ZWVrXCIpLnRoYWkoXCJJbiAxIHdlZWtcIikuZ2V0TWVzc2FnZSgpLCB2YWx1ZTogRmlsdGVyRGF0ZVJhbmdlcy5JTl8xX1dFRUsgfSxcbiAgeyBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJJbiAxIG1vbnRoXCIpLnRoYWkoXCJJbiAxIG1vbnRoXCIpLmdldE1lc3NhZ2UoKSwgdmFsdWU6IEZpbHRlckRhdGVSYW5nZXMuSU5fMV9NT05USCB9LFxuXTtcblxuY29uc3QgY2FsY1J1bGVzID0gbmV3IE1hcDxGaWx0ZXJEYXRlUmFuZ2VzLCBGdW5jdGlvbj4oKTtcbmNhbGNSdWxlcy5zZXQoRmlsdGVyRGF0ZVJhbmdlcy5QQVNUXzFfSE9VUiwgKCkgPT4ge1xuICByZXR1cm4ge1xuICAgIGZyb206IERhdGVVdGlscy5taW51c1hIb3Vyc0Zyb21Ob3coMSksXG4gICAgdG86IERhdGVVdGlscy5ub3coKSxcbiAgfTtcbn0pO1xuXG5jYWxjUnVsZXMuc2V0KEZpbHRlckRhdGVSYW5nZXMuUEFTVF8xMl9IT1VSLCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogRGF0ZVV0aWxzLm1pbnVzWEhvdXJzRnJvbU5vdygxMiksXG4gICAgdG86IERhdGVVdGlscy5ub3coKSxcbiAgfTtcbn0pO1xuXG5jYWxjUnVsZXMuc2V0KEZpbHRlckRhdGVSYW5nZXMuVEhJU19XRUVLLCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogRGF0ZVV0aWxzLm1pbnVzWERheXNGcm9tTm93KDcpLFxuICAgIHRvOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLlBBU1RfMTBfREFZUywgKCkgPT4ge1xuICByZXR1cm4ge1xuICAgIGZyb206IERhdGVVdGlscy5taW51c1hEYXlzRnJvbU5vdygxMCksXG4gICAgdG86IERhdGVVdGlscy5taW51c1hEYXlzRnJvbU5vdygxKSxcbiAgfTtcbn0pO1xuXG5jYWxjUnVsZXMuc2V0KEZpbHRlckRhdGVSYW5nZXMuT1ZFUl9EVUUsICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX1dFRUssICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMubWludXNYV2Vla3NGcm9tTm93KDEpLFxuICAgIHRvOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfVFdPX1dFRUssICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMubWludXNYV2Vla3NGcm9tTm93KDIpLFxuICAgIHRvOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLkxBU1RfT05FX01PTlRILCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogRGF0ZVV0aWxzLm1pbnVzWE1vbnRoc0Zyb21Ob3coMSksXG4gICAgdG86IERhdGVVdGlscy5taW51c1hEYXlzRnJvbU5vdygxKSxcbiAgfTtcbn0pO1xuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLlRISVNfTU9OVEgsICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMubWludXNYTW9udGhzRnJvbU5vdygxKSxcbiAgICB0bzogRGF0ZVV0aWxzLm1pbnVzWERheXNGcm9tTm93KDEpLFxuICB9O1xufSk7XG5jYWxjUnVsZXMuc2V0KEZpbHRlckRhdGVSYW5nZXMuTEFTVF8zX01PTlRIUywgKCkgPT4ge1xuICByZXR1cm4ge1xuICAgIGZyb206IERhdGVVdGlscy5taW51c1hNb250aHNGcm9tTm93KDMpLFxuICAgIHRvOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLlBBU1RfM19EQVlTLCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogRGF0ZVV0aWxzLm1pbnVzWERheXNGcm9tTm93KDIpLFxuICAgIHRvOiBEYXRlVXRpbHMubWludXNYRGF5c0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLklOXzFfV0VFSywgKCkgPT4ge1xuICByZXR1cm4ge1xuICAgIGZyb206IERhdGVVdGlscy5wbHVzWERheXNGcm9tTm93KDEpLFxuICAgIHRvOiBEYXRlVXRpbHMucGx1c1hXZWVrc0Zyb21Ob3coMSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLklOXzFfTU9OVEgsICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMucGx1c1hEYXlzRnJvbU5vdygxKSxcbiAgICB0bzogRGF0ZVV0aWxzLnBsdXNYTW9udGhzRnJvbU5vdygxKSxcbiAgfTtcbn0pO1xuXG5jYWxjUnVsZXMuc2V0KEZpbHRlckRhdGVSYW5nZXMuVEhJU19ZRUFSLCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogRGF0ZVV0aWxzLm5vd1dpdGhUaW1lQXRTdGFydE9mWWVhcigpLFxuICAgIHRvOiBEYXRlVXRpbHMubm93V2l0aFRpbWVBdEVuZE9mWWVhcigpLFxuICB9O1xufSk7XG5cbmNhbGNSdWxlcy5zZXQoRmlsdGVyRGF0ZVJhbmdlcy5PVkVSX0RVRV81X0RBWVMsICgpID0+IHtcbiAgcmV0dXJuIHtcbiAgICBmcm9tOiBEYXRlVXRpbHMubm93V2l0aFRpbWVBdFN0YXJ0T2ZEYXkoKSxcbiAgICB0bzogRGF0ZVV0aWxzLnBsdXNYRGF5c0Zyb21Ob3coNSksXG4gIH07XG59KTtcblxuY2FsY1J1bGVzLnNldChGaWx0ZXJEYXRlUmFuZ2VzLk9WRVJfRFVFLCAoKSA9PiB7XG4gIHJldHVybiB7XG4gICAgZnJvbTogXCJcIixcbiAgICB0bzogRGF0ZVV0aWxzLm1pbnVzWERheXNGcm9tTm93KDEpLFxuICB9O1xufSk7XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRGaWx0ZXJPcHRpb25zKHJhbmdlVHlwZXM6IEZpbHRlckRhdGVSYW5nZXNbXSk6IEZpbHRlckRhdGVSYW5nZU9wdGlvbltdIHtcbiAgY29uc3QgcmV0OiBGaWx0ZXJEYXRlUmFuZ2VPcHRpb25bXSA9IFtdO1xuXG4gIHJhbmdlVHlwZXMuZm9yRWFjaChpdGVtID0+IHtcbiAgICBjb25zdCBvcHRpb24gPSBhbGxSYW5nZU9wdGlvbnMuZmluZChyYW5nZSA9PiByYW5nZS52YWx1ZSA9PT0gaXRlbSk7XG4gICAgaWYgKG9wdGlvbikge1xuICAgICAgcmV0LnB1c2gob3B0aW9uKTtcbiAgICB9XG4gIH0pO1xuXG4gIHJldHVybiByZXQ7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREYXRlUmFuZ2VCeVR5cGUocmFuZ2VUeXBlOiBGaWx0ZXJEYXRlUmFuZ2VzKSB7XG4gIGlmICghcmFuZ2VUeXBlKSB7XG4gICAgcmV0dXJuIHt9O1xuICB9XG4gIGNvbnN0IGlzSG91ciA9IFtGaWx0ZXJEYXRlUmFuZ2VzLlBBU1RfMV9IT1VSLCBGaWx0ZXJEYXRlUmFuZ2VzLlBBU1RfMTJfSE9VUl0uaW5jbHVkZXMocmFuZ2VUeXBlKTtcblxuICBjb25zdCBjYWxjUnVsZSA9IGNhbGNSdWxlcy5nZXQocmFuZ2VUeXBlKTtcbiAgY29uc3QgZnJvbSA9IERhdGVVdGlscy5ub3coKTtcbiAgY29uc3QgdG8gPSBEYXRlVXRpbHMubm93KCk7XG5cbiAgY29uc3QgcmV0ID0ge1xuICAgIGZyb20sXG4gICAgdG8sXG4gICAgLi4uKGNhbGNSdWxlICYmIGNhbGNSdWxlKCkpLFxuICB9O1xuICBpZiAoaXNIb3VyKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGZyb206IHJldC5mcm9tID8gRGF0ZVV0aWxzLmZvcm1hdERhdGVUaW1lV2l0aFRpbWVab25lKHJldC5mcm9tKSA6IG51bGwsXG4gICAgICB0bzogcmV0LnRvID8gRGF0ZVV0aWxzLmZvcm1hdERhdGVUaW1lV2l0aFRpbWVab25lKHJldC50bykgOiBudWxsLFxuICAgIH07XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGZyb206IHV0aWxzLnN0cmluZ2lmeURhdGUocmV0LmZyb20uc3RhcnRPZiAmJiByZXQuZnJvbS5zdGFydE9mKFwiZGF5XCIpKSxcbiAgICB0bzogdXRpbHMuc3RyaW5naWZ5RGF0ZShyZXQudG8uZW5kT2YoXCJkYXlcIikpLFxuICB9O1xuXG5cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGFwcGVuZERlZmF1bHREYXRhUmFuZ2UocGFyYW1zOiBhbnksIGZpZWxkczogc3RyaW5nW10pIHtcbiAgaWYgKCFmaWVsZHMubGVuZ3RoKSB7XG4gICAgcmV0dXJuIHBhcmFtcztcbiAgfVxuXG4gIGNvbnN0IHJldCA9IHsgLi4ucGFyYW1zIH07XG5cbiAgcmV0dXJuIGZpZWxkcy5yZWR1Y2UoKGFjYywgY3VyKSA9PiB7XG4gICAgY29uc3QgZGF0ZVJhbmdlID0gYWNjW2N1cl07XG5cbiAgICBpZiAoIWRhdGVSYW5nZSB8fCAhZGF0ZVJhbmdlLmZyb20pIHtcbiAgICAgIGFjY1tjdXJdID0gZ2V0RGF0ZVJhbmdlQnlUeXBlKEZpbHRlckRhdGVSYW5nZXMuT1ZFUl9EVUUpO1xuICAgIH1cblxuICAgIHJldHVybiBhY2M7XG4gIH0sIHJldCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBhcHBlbmREZWZhdWx0UGFybWFzKHBhcmFtczogYW55LCBkZWZhdWx0UGFyYW1zOiBhbnkpIHtcbiAgY29uc3QgZmllbGRzID0gT2JqZWN0LmtleXMoZGVmYXVsdFBhcmFtcyk7XG4gIGlmICghZmllbGRzLmxlbmd0aCkge1xuICAgIHJldHVybiBwYXJhbXM7XG4gIH1cblxuICBjb25zdCByZXQgPSB7IC4uLnBhcmFtcyB9O1xuXG4gIHJldHVybiBmaWVsZHMucmVkdWNlKChhY2MsIGN1cikgPT4ge1xuICAgIGlmIChhY2NbY3VyXSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICBjb25zdCB2YWx1ZSA9IGRlZmF1bHRQYXJhbXNbY3VyXTtcbiAgICAgIGNvbnN0IGlzRGF0ZVJhbmdlID0gRmlsdGVyRGF0ZVJhbmdlc1t2YWx1ZV07XG4gICAgICBhY2NbY3VyXSA9IGlzRGF0ZVJhbmdlID8gZ2V0RGF0ZVJhbmdlQnlUeXBlKHZhbHVlKSA6IHZhbHVlO1xuICAgIH1cblxuICAgIGlmIChhY2NbY3VyXSAmJiBhY2NbY3VyXS50eXBlICYmIEZpbHRlckRhdGVSYW5nZXNbYWNjW2N1cl0udHlwZV0pIHtcbiAgICAgIGFjY1tjdXJdID0gZ2V0RGF0ZVJhbmdlQnlUeXBlKGFjY1tjdXJdLnR5cGUpO1xuICAgIH1cblxuICAgIHJldHVybiBhY2M7XG4gIH0sIHJldCk7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBT0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUF3QkE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/filter/date-range-util.tsx
