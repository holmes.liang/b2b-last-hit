var SHADOW_PROPS = {
  'shadowBlur': 1,
  'shadowOffsetX': 1,
  'shadowOffsetY': 1,
  'textShadowBlur': 1,
  'textShadowOffsetX': 1,
  'textShadowOffsetY': 1,
  'textBoxShadowBlur': 1,
  'textBoxShadowOffsetX': 1,
  'textBoxShadowOffsetY': 1
};

function _default(ctx, propName, value) {
  if (SHADOW_PROPS.hasOwnProperty(propName)) {
    return value *= ctx.dpr;
  }

  return value;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvZml4U2hhZG93LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvZml4U2hhZG93LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBTSEFET1dfUFJPUFMgPSB7XG4gICdzaGFkb3dCbHVyJzogMSxcbiAgJ3NoYWRvd09mZnNldFgnOiAxLFxuICAnc2hhZG93T2Zmc2V0WSc6IDEsXG4gICd0ZXh0U2hhZG93Qmx1cic6IDEsXG4gICd0ZXh0U2hhZG93T2Zmc2V0WCc6IDEsXG4gICd0ZXh0U2hhZG93T2Zmc2V0WSc6IDEsXG4gICd0ZXh0Qm94U2hhZG93Qmx1cic6IDEsXG4gICd0ZXh0Qm94U2hhZG93T2Zmc2V0WCc6IDEsXG4gICd0ZXh0Qm94U2hhZG93T2Zmc2V0WSc6IDFcbn07XG5cbmZ1bmN0aW9uIF9kZWZhdWx0KGN0eCwgcHJvcE5hbWUsIHZhbHVlKSB7XG4gIGlmIChTSEFET1dfUFJPUFMuaGFzT3duUHJvcGVydHkocHJvcE5hbWUpKSB7XG4gICAgcmV0dXJuIHZhbHVlICo9IGN0eC5kcHI7XG4gIH1cblxuICByZXR1cm4gdmFsdWU7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/fixShadow.js
