__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./consts */ "./src/common/consts.tsx");
/* harmony import */ var _storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./storage */ "./src/common/storage.tsx");





var Language =
/*#__PURE__*/
function () {
  function Language() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Language);

    this.message = void 0;
    this._language = _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN;
    this._profile = void 0;
    this.useEn = void 0;
    this.message = "";
    this.useEn = false;
    this.profile = (_storage__WEBPACK_IMPORTED_MODULE_3__["default"].Account.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].ACCOUNT_KEY) || {}).profile;
    this.language = !this.profile ? _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN : this.profile.preferredLang || _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Language, [{
    key: "refreshLanguage",
    value: function refreshLanguage() {
      this.profile = (_storage__WEBPACK_IMPORTED_MODULE_3__["default"].Account.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].ACCOUNT_KEY) || {}).profile;
      this.language = !this.profile ? _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN : this.profile.preferredLang || _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN;
    }
  }, {
    key: "en",
    value: function en(message) {
      this.useEn = true;
      this.message = message;
      return this;
    }
  }, {
    key: "thai",
    value: function thai(message) {
      if (this.language === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_THAI) {
        this.message = message;
      }

      return this;
    }
  }, {
    key: "my",
    value: function my(message) {
      if (this.language === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_MY) {
        this.message = message;
      }

      return this;
    }
  }, {
    key: "getMessage",
    value: function getMessage() {
      if (!this.useEn) {
        console.error("You must use Language.en first.");
        if (this.language === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].LANGUAGE_EN) return "";
      }

      var message = this.message;
      this.useEn = false;
      return message;
    }
  }, {
    key: "profile",
    get: function get() {
      return (_storage__WEBPACK_IMPORTED_MODULE_3__["default"].Account.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].ACCOUNT_KEY) || {}).profile;
    },
    set: function set(v) {
      this._profile = v;
    }
  }, {
    key: "language",
    set: function set(language) {
      this._language = language;
    },
    get: function get() {
      return this._language;
    }
  }]);

  return Language;
}();

var HungrySingleton = function () {
  var _instance = new Language();

  return function () {
    return _instance;
  };
}();

var LanguageInstance = new HungrySingleton();
/* harmony default export */ __webpack_exports__["default"] = (LanguageInstance);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2xhbmd1YWdlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2NvbW1vbi9sYW5ndWFnZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBDb25zdHMgZnJvbSBcIi4vY29uc3RzXCI7XG5pbXBvcnQgU3RvcmFnZSBmcm9tIFwiLi9zdG9yYWdlXCI7XG5cbmNsYXNzIExhbmd1YWdlIHtcbiAgbWVzc2FnZTogc3RyaW5nO1xuICBwcm90ZWN0ZWQgX2xhbmd1YWdlOiBzdHJpbmcgPSBDb25zdHMuTEFOR1VBR0VfRU47XG4gIHByb3RlY3RlZCBfcHJvZmlsZTogYW55O1xuICBwcm90ZWN0ZWQgdXNlRW46IGJvb2xlYW47XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5tZXNzYWdlID0gXCJcIjtcbiAgICB0aGlzLnVzZUVuID0gZmFsc2U7XG4gICAgdGhpcy5wcm9maWxlID0gKFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BQ0NPVU5UX0tFWSkgfHwge30pLnByb2ZpbGU7XG4gICAgdGhpcy5sYW5ndWFnZSA9ICF0aGlzLnByb2ZpbGUgPyBDb25zdHMuTEFOR1VBR0VfRU4gOiB0aGlzLnByb2ZpbGUucHJlZmVycmVkTGFuZyB8fCBDb25zdHMuTEFOR1VBR0VfRU47XG4gIH1cblxuICByZWZyZXNoTGFuZ3VhZ2UoKSB7XG4gICAgdGhpcy5wcm9maWxlID0gKFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BQ0NPVU5UX0tFWSkgfHwge30pLnByb2ZpbGU7XG4gICAgdGhpcy5sYW5ndWFnZSA9ICF0aGlzLnByb2ZpbGUgPyBDb25zdHMuTEFOR1VBR0VfRU4gOiB0aGlzLnByb2ZpbGUucHJlZmVycmVkTGFuZyB8fCBDb25zdHMuTEFOR1VBR0VfRU47XG4gIH1cblxuICBnZXQgcHJvZmlsZSgpIHtcbiAgICByZXR1cm4gKFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BQ0NPVU5UX0tFWSkgfHwge30pLnByb2ZpbGU7XG4gIH1cblxuICBzZXQgcHJvZmlsZSh2OiBhbnkpIHtcbiAgICB0aGlzLl9wcm9maWxlID0gdjtcbiAgfVxuXG4gIHNldCBsYW5ndWFnZShsYW5ndWFnZTogc3RyaW5nKSB7XG4gICAgdGhpcy5fbGFuZ3VhZ2UgPSBsYW5ndWFnZTtcbiAgfVxuXG4gIGdldCBsYW5ndWFnZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl9sYW5ndWFnZTtcbiAgfVxuXG4gIGVuKG1lc3NhZ2U6IHN0cmluZykge1xuICAgIHRoaXMudXNlRW4gPSB0cnVlO1xuICAgIHRoaXMubWVzc2FnZSA9IG1lc3NhZ2U7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICB0aGFpKG1lc3NhZ2U6IHN0cmluZykge1xuICAgIGlmICh0aGlzLmxhbmd1YWdlID09PSBDb25zdHMuTEFOR1VBR0VfVEhBSSkge1xuICAgICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBteShtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICBpZiAodGhpcy5sYW5ndWFnZSA9PT0gQ29uc3RzLkxBTkdVQUdFX01ZKSB7XG4gICAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuICBnZXRNZXNzYWdlKCkge1xuICAgIGlmICghdGhpcy51c2VFbikge1xuICAgICAgY29uc29sZS5lcnJvcihcIllvdSBtdXN0IHVzZSBMYW5ndWFnZS5lbiBmaXJzdC5cIik7XG4gICAgICBpZiAodGhpcy5sYW5ndWFnZSA9PT0gQ29uc3RzLkxBTkdVQUdFX0VOKSByZXR1cm4gXCJcIjtcbiAgICB9XG4gICAgY29uc3QgbWVzc2FnZSA9IHRoaXMubWVzc2FnZTtcbiAgICB0aGlzLnVzZUVuID0gZmFsc2U7XG4gICAgcmV0dXJuIG1lc3NhZ2U7XG4gIH1cbn1cblxuY29uc3QgSHVuZ3J5U2luZ2xldG9uOiBhbnkgPSAoZnVuY3Rpb24oKSB7XG4gIGNvbnN0IF9pbnN0YW5jZSA9IG5ldyBMYW5ndWFnZSgpO1xuXG4gIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gX2luc3RhbmNlO1xuICB9O1xufSkoKTtcblxuY29uc3QgTGFuZ3VhZ2VJbnN0YW5jZSA9IG5ldyBIdW5ncnlTaW5nbGV0b24oKTtcblxuZXhwb3J0IGRlZmF1bHQgTGFuZ3VhZ2VJbnN0YW5jZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFNQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUEzQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7Ozs7OztBQWdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/common/language.tsx
