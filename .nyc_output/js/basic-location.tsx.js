__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RightDiv", function() { return RightDiv; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicLocation", function() { return BasicLocation; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _desk_component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @desk-component/filter/date-range-util */ "./src/app/desk/component/filter/date-range-util.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _component_location_view__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./component/location-view */ "./src/app/desk/quote/SAIC/iar/component/location-view.tsx");
/* harmony import */ var _desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @desk-component/table/action-bars */ "./src/app/desk/component/table/action-bars.tsx");










var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-location.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_9__["default"])(["\n  .ant-input-number-input {\n    text-align: right\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}














var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 10
    }
  }
};
var RightDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject());
var isMobile = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile();

var BasicLocation =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(BasicLocation, _QuoteStep);

  function BasicLocation() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicLocation);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(BasicLocation)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.queryPage = {};
    _this.columns = [];

    _this.handleMemberView = function (record) {
      _this.setValueToModel(record, "member.ext");

      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_20__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component_location_view__WEBPACK_IMPORTED_MODULE_21__["LocationView"], {
            onCancel: onCancel,
            model: _this.props.model,
            getMasterTableItemText: _this.getMasterTableItemText.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this)),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 213
            },
            __self: this
          });
        }
      });
    };

    _this.handleListRefresh = function (query) {
      var defaultParams = _this.initQueryParams();

      return Object(_desk_component_filter_date_range_util__WEBPACK_IMPORTED_MODULE_19__["appendDefaultParmas"])(query, defaultParams);
    };

    _this.handleMember = function (record, index) {
      if (!record) {
        _this.setValueToModel({}, "member");

        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].get("/projects/bf/products/iar/benefits", {}, {
          loading: true
        }).then(function (res) {
          var data = lodash__WEBPACK_IMPORTED_MODULE_18___default.a.get(res, "body.respData", []);

          _this.setValueToModel(data, "member.ext.coverages");

          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_14__["pageTo"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this), _all_steps__WEBPACK_IMPORTED_MODULE_16__["AllSteps"].ADDLOCATION);
        });
      } else {
        var newRecord = lodash__WEBPACK_IMPORTED_MODULE_18___default.a.cloneDeep(record);

        _this.setValueToModel(newRecord, "member.ext");

        _this.setValueToModel("edit", "member.action");

        _this.setValueToModel(index, "member.index");

        Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_14__["pageTo"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this), _all_steps__WEBPACK_IMPORTED_MODULE_16__["AllSteps"].ADDLOCATION);
      }
    };

    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.getMasterTableItemText = function (tableName, itemId) {
      var codeTables = _this.state.codeTables;

      if (itemId) {
        var item = (codeTables[tableName] || []).find(function (option) {
          return itemId.toString() === option.id.toString();
        }) || {};
        return item.text || "";
      } else {
        return "";
      }
    };

    _this.ajaxDelete = function (index) {};

    _this.initQueryParams = function () {
      var initParams = {};
      return initParams;
    };

    _this.calcGst = function () {
      return 0;
    };

    _this.calcAmount = function () {
      return 0;
    };

    _this.initQueryPageRef = function (ref) {
      _this.queryPage = ref;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicLocation, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(BasicLocation.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        codeTables: {},
        gst: "",
        amount: ""
      };
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }() // renders

  }, {
    key: "getIsCoinsuranceEnabled",
    value: function getIsCoinsuranceEnabled() {
      return true;
    }
  }, {
    key: "getIsArrangementEnabled",
    value: function getIsArrangementEnabled() {
      return false;
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("INSURED LOCATIONS").thai("INSURED LOCATIONS").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NCollapse"], {
        defaultActiveKey: ["1", "2", "3", "4", "5"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("LOCATIONS").thai("LOCATIONS").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, this.renderLocationTable()), this.getIsCoinsuranceEnabled() && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "3",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("CO-INSURANCE").thai("CO-INSURANCE").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }, this.renderCoInsurance()), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "2",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("FEES SUMMARY").thai("FEES SUMMARY").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, this.renderFeeSummary()), this.getIsArrangementEnabled() && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "4",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Payment Arrangement").thai("Payment Arrangement").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, this.renderPaymentArrangement()), this.renderOutput()), this.renderRightActions())));
    }
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this2 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_17__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_14__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_16__["AllSteps"].BUSINESS);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Button"], {
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this2.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Save and Continue").thai("Save and Continue").my("Save and Continue").getMessage()));
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderLocationTable",
    value: function renderLocationTable() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderFeeSummary",
    value: function renderFeeSummary() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderCoInsurance",
    value: function renderCoInsurance() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderPaymentArrangement",
    value: function renderPaymentArrangement() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderOutput",
    value: function renderOutput() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null);
    }
  }, {
    key: "renderOperColumn",
    value: function renderOperColumn(record, index) {
      var _this3 = this;

      var _record$opers = record.opers,
          opers = _record$opers === void 0 ? {} : _record$opers;
      var actions = [{
        actionName: "View",
        handel: function handel() {
          // todo 验证
          _this3.handleMemberView(record);
        }
      }, {
        actionName: "Edit",
        handel: function handel() {
          // todo 验证
          _this3.handleMember(record, index);
        }
      }, {
        actionName: "Delete",
        handel: function handel() {
          // todo 验证
          _this3.handleMemberDelete(record);
        }
      }];
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_table_action_bars__WEBPACK_IMPORTED_MODULE_22__["TableActionBars"], {
        actions: actions,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      });
    }
  }, {
    key: "renderThousandNum",
    value: function renderThousandNum(value) {
      var newValue = (parseFloat(value) || 0).toFixed(2);
      return _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].toThousands(newValue);
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }, {
    key: "renderActions",
    value: function renderActions() {} // actions

  }, {
    key: "handleMemberDelete",
    value: function handleMemberDelete(index) {
      var _this4 = this;

      antd__WEBPACK_IMPORTED_MODULE_11__["Modal"].confirm({
        centered: true,
        content: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 225
          },
          __self: this
        }, "Are you sure to delete this location?"),
        okText: "Yes",
        cancelText: "No",
        onOk: function onOk() {
          _this4.ajaxDelete(index);
        },
        onCancel: function onCancel() {}
      });
    }
  }, {
    key: "handleNext",
    value: function handleNext() {} // methods

  }, {
    key: "getProps",
    value: function getProps(propName, dataFixed) {
      return "";
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }]);

  return BasicLocation;
}(_quote_step__WEBPACK_IMPORTED_MODULE_15__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtbG9jYXRpb24udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtbG9jYXRpb24udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEJ1dHRvbiwgQ29sLCBNb2RhbCwgUm93LCBTcGluIH0gZnJvbSBcImFudGRcIjtcblxuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTlBhbmVsIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBwYWdlVG8gfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3dpemFyZFwiO1xuXG5pbXBvcnQgUXVvdGVTdGVwLCB7IFN0ZXBDb21wb25lbnRzLCBTdGVwUHJvcHMgfSBmcm9tIFwiLi4vLi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IHsgQWxsU3RlcHMgfSBmcm9tIFwiLi4vYWxsLXN0ZXBzXCI7XG5pbXBvcnQgeyBCdXR0b25CYWNrIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9hbnRkL2J1dHRvbi1iYWNrXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBhcHBlbmREZWZhdWx0UGFybWFzIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9maWx0ZXIvZGF0ZS1yYW5nZS11dGlsXCI7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCB7IExvY2F0aW9uVmlldyB9IGZyb20gXCIuL2NvbXBvbmVudC9sb2NhdGlvbi12aWV3XCI7XG5pbXBvcnQgeyBJQWN0aW9uLCBUYWJsZUFjdGlvbkJhcnMgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3RhYmxlL2FjdGlvbi1iYXJzXCI7XG5pbXBvcnQgTlBhbmVsU2hvd091dHB1dHMgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9OUGFuZWxTaG93T3V0cHV0c1wiO1xuXG5jb25zdCBmb3JtRGF0ZUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG59O1xuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDEwIH0sXG4gIH0sXG59O1xuXG5cbmV4cG9ydCBjb25zdCBSaWdodERpdiA9IFN0eWxlZC5kaXZgXG4gIC5hbnQtaW5wdXQtbnVtYmVyLWlucHV0IHtcbiAgICB0ZXh0LWFsaWduOiByaWdodFxuICB9XG5gO1xuXG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIGNvZGVUYWJsZXM6IGFueTtcbiAgZ3N0OiBhbnk7XG4gIGFtb3VudDogYW55XG59XG5cbmNsYXNzIEJhc2ljTG9jYXRpb248UCBleHRlbmRzIFN0ZXBQcm9wcywgUyBleHRlbmRzIElTdGF0ZSwgQyBleHRlbmRzIFN0ZXBDb21wb25lbnRzPiBleHRlbmRzIFF1b3RlU3RlcDxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0Q29tcG9uZW50cygpKSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4ge1xuICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICBjb2RlVGFibGVzOiB7fSxcbiAgICAgIGdzdDogXCJcIixcbiAgICAgIGFtb3VudDogXCJcIixcbiAgICB9IGFzIFM7XG4gIH1cblxuICBxdWVyeVBhZ2UgPSB7fSBhcyBhbnk7XG5cbiAgY29sdW1uczogYW55ID0gW107XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gIH1cblxuICAvLyByZW5kZXJzXG4gIGdldElzQ29pbnN1cmFuY2VFbmFibGVkKCkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgZ2V0SXNBcnJhbmdlbWVudEVuYWJsZWQoKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckNvbnRlbnQoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXt0aGlzLnN0YXRlLmxvYWRpbmd9PlxuICAgICAgICA8Um93IHR5cGU9XCJmbGV4XCIganVzdGlmeT17XCJzcGFjZS1iZXR3ZWVuXCJ9PlxuICAgICAgICAgIHt0aGlzLnJlbmRlclNpZGVJbmZvKCl9XG4gICAgICAgICAgPENvbCBzbT17MTh9IHhzPXsyM30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIklOU1VSRUQgTE9DQVRJT05TXCIpLnRoYWkoXCJJTlNVUkVEIExPQ0FUSU9OU1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxOQ29sbGFwc2UgZGVmYXVsdEFjdGl2ZUtleT17W1wiMVwiLCBcIjJcIiwgXCIzXCIsIFwiNFwiLCBcIjVcIl19PlxuICAgICAgICAgICAgICA8TlBhbmVsXG4gICAgICAgICAgICAgICAga2V5PVwiMVwiXG4gICAgICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkxPQ0FUSU9OU1wiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCJMT0NBVElPTlNcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJMb2NhdGlvblRhYmxlKCl9XG4gICAgICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgICAgICB7dGhpcy5nZXRJc0NvaW5zdXJhbmNlRW5hYmxlZCgpICYmIDxOUGFuZWwga2V5PVwiM1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiQ08tSU5TVVJBTkNFXCIpLnRoYWkoXCJDTy1JTlNVUkFOQ0VcIikuZ2V0TWVzc2FnZSgpfT5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJDb0luc3VyYW5jZSgpfVxuICAgICAgICAgICAgICA8L05QYW5lbD59XG4gICAgICAgICAgICAgIDxOUGFuZWwga2V5PVwiMlwiXG4gICAgICAgICAgICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkZFRVMgU1VNTUFSWVwiKS50aGFpKFwiRkVFUyBTVU1NQVJZXCIpLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVyRmVlU3VtbWFyeSgpfVxuICAgICAgICAgICAgICA8L05QYW5lbD5cblxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRJc0FycmFuZ2VtZW50RW5hYmxlZCgpICYmIDxOUGFuZWwga2V5PXtcIjRcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiUGF5bWVudCBBcnJhbmdlbWVudFwiKS50aGFpKFwiUGF5bWVudCBBcnJhbmdlbWVudFwiKS5nZXRNZXNzYWdlKCl9PlxuICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlbmRlclBheW1lbnRBcnJhbmdlbWVudCgpXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgPC9OUGFuZWw+XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJPdXRwdXQoKVxuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIDwvTkNvbGxhcHNlPlxuICAgICAgICAgICAge3RoaXMucmVuZGVyUmlnaHRBY3Rpb25zKCl9XG4gICAgICAgICAgPC9Db2w+XG4gICAgICAgIDwvUm93PlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJSaWdodEFjdGlvbnMoKSB7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPEJ1dHRvbkJhY2sgaGFuZGVsPXsoKSA9PiB7XG4gICAgICAgICAgcGFnZVRvKHRoaXMsIEFsbFN0ZXBzLkJVU0lORVNTKTtcbiAgICAgICAgfX0vPlxuICAgICAgICA8QnV0dG9uIHNpemU9XCJsYXJnZVwiIHR5cGU9XCJwcmltYXJ5XCIgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVOZXh0KCl9PlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlNhdmUgYW5kIENvbnRpbnVlXCIpLnRoYWkoXCJTYXZlIGFuZCBDb250aW51ZVwiKS5teShcIlNhdmUgYW5kIENvbnRpbnVlXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyU2lkZUluZm8oKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyTG9jYXRpb25UYWJsZSgpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICByZW5kZXJGZWVTdW1tYXJ5KCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlckNvSW5zdXJhbmNlKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlclBheW1lbnRBcnJhbmdlbWVudCgpIHtcbiAgICByZXR1cm4gPD48Lz47XG4gIH1cblxuICByZW5kZXJPdXRwdXQoKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyT3BlckNvbHVtbihyZWNvcmQ6IGFueSwgaW5kZXg6IGFueSkge1xuICAgIGNvbnN0IHsgb3BlcnMgPSB7fSB9ID0gcmVjb3JkO1xuXG4gICAgY29uc3QgYWN0aW9uczogSUFjdGlvbltdID0gW1xuICAgICAge1xuICAgICAgICBhY3Rpb25OYW1lOiBcIlZpZXdcIixcbiAgICAgICAgaGFuZGVsOiAoKSA9PiB7IC8vIHRvZG8g6aqM6K+BXG4gICAgICAgICAgdGhpcy5oYW5kbGVNZW1iZXJWaWV3KHJlY29yZCk7XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBhY3Rpb25OYW1lOiBcIkVkaXRcIixcbiAgICAgICAgaGFuZGVsOiAoKSA9PiB7IC8vIHRvZG8g6aqM6K+BXG4gICAgICAgICAgdGhpcy5oYW5kbGVNZW1iZXIocmVjb3JkLCBpbmRleCk7XG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBhY3Rpb25OYW1lOiBcIkRlbGV0ZVwiLFxuICAgICAgICBoYW5kZWw6ICgpID0+IHsgLy8gdG9kbyDpqozor4FcbiAgICAgICAgICB0aGlzLmhhbmRsZU1lbWJlckRlbGV0ZShyZWNvcmQpO1xuICAgICAgICB9LFxuICAgICAgfSxcbiAgICBdO1xuICAgIHJldHVybiA8VGFibGVBY3Rpb25CYXJzIGFjdGlvbnM9e2FjdGlvbnN9Lz47XG4gIH1cblxuICByZW5kZXJUaG91c2FuZE51bSh2YWx1ZTogYW55KSB7XG4gICAgY29uc3QgbmV3VmFsdWUgPSAocGFyc2VGbG9hdCh2YWx1ZSkgfHwgMCkudG9GaXhlZCgyKTtcbiAgICByZXR1cm4gVXRpbHMudG9UaG91c2FuZHMobmV3VmFsdWUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclRpdGxlKCkge1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gIH1cblxuICAvLyBhY3Rpb25zXG4gIGhhbmRsZU1lbWJlclZpZXcgPSAocmVjb3JkOiBhbnkpID0+IHtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChyZWNvcmQsIFwibWVtYmVyLmV4dFwiKTtcbiAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPExvY2F0aW9uVmlld1xuICAgICAgICBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0PXt0aGlzLmdldE1hc3RlclRhYmxlSXRlbVRleHQuYmluZCh0aGlzKX1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuXG4gIHByaXZhdGUgaGFuZGxlTWVtYmVyRGVsZXRlKGluZGV4OiBhbnkpIHtcbiAgICBNb2RhbC5jb25maXJtKHtcbiAgICAgIGNlbnRlcmVkOiB0cnVlLFxuICAgICAgY29udGVudDogKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIEFyZSB5b3Ugc3VyZSB0byBkZWxldGUgdGhpcyBsb2NhdGlvbj9cbiAgICAgICAgPC9kaXY+XG4gICAgICApLFxuICAgICAgb2tUZXh0OiBcIlllc1wiLFxuICAgICAgY2FuY2VsVGV4dDogXCJOb1wiLFxuICAgICAgb25PazogKCkgPT4ge1xuICAgICAgICB0aGlzLmFqYXhEZWxldGUoaW5kZXgpO1xuICAgICAgfSxcbiAgICAgIG9uQ2FuY2VsOiAoKSA9PiB7XG5cbiAgICAgIH0sXG4gICAgfSk7XG4gIH1cblxuICBoYW5kbGVMaXN0UmVmcmVzaCA9IChxdWVyeTogYW55KSA9PiB7XG4gICAgY29uc3QgZGVmYXVsdFBhcmFtcyA9IHRoaXMuaW5pdFF1ZXJ5UGFyYW1zKCk7XG4gICAgcmV0dXJuIGFwcGVuZERlZmF1bHRQYXJtYXMocXVlcnksIGRlZmF1bHRQYXJhbXMpO1xuICB9O1xuXG4gIGhhbmRsZU1lbWJlciA9IChyZWNvcmQ/OiBhbnksIGluZGV4PzogbnVtYmVyKSA9PiB7XG4gICAgaWYgKCFyZWNvcmQpIHtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHt9LCBcIm1lbWJlclwiKTtcbiAgICAgIEFqYXguZ2V0KFwiL3Byb2plY3RzL2JmL3Byb2R1Y3RzL2lhci9iZW5lZml0c1wiLCB7fSwgeyBsb2FkaW5nOiB0cnVlIH0pLnRoZW4oKHJlczogYW55KSA9PiB7XG4gICAgICAgIGNvbnN0IGRhdGEgPSBfLmdldChyZXMsIFwiYm9keS5yZXNwRGF0YVwiLCBbXSk7XG4gICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGRhdGEsIFwibWVtYmVyLmV4dC5jb3ZlcmFnZXNcIik7XG4gICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5BRERMT0NBVElPTik7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgbmV3UmVjb3JkID0gXy5jbG9uZURlZXAocmVjb3JkKTtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKG5ld1JlY29yZCwgXCJtZW1iZXIuZXh0XCIpO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJlZGl0XCIsIFwibWVtYmVyLmFjdGlvblwiKTtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKGluZGV4LCBcIm1lbWJlci5pbmRleFwiKTtcbiAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5BRERMT0NBVElPTik7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZU5leHQoKSB7XG4gIH1cblxuICAvLyBtZXRob2RzXG5cbiAgZ2V0UHJvcHMocHJvcE5hbWU6IGFueSwgZGF0YUZpeGVkPzogYW55KSB7XG4gICAgcmV0dXJuIFwiXCI7XG4gIH1cblxuICBsb2FkQ29kZVRhYmxlcyA9IGFzeW5jICgpID0+IHtcblxuICB9O1xuXG4gIGdldE1hc3RlclRhYmxlSXRlbVRleHQgPSAodGFibGVOYW1lOiBzdHJpbmcsIGl0ZW1JZDogc3RyaW5nKSA9PiB7XG4gICAgY29uc3QgeyBjb2RlVGFibGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmIChpdGVtSWQpIHtcbiAgICAgIGNvbnN0IGl0ZW0gPVxuICAgICAgICAoY29kZVRhYmxlc1t0YWJsZU5hbWVdIHx8IFtdKS5maW5kKChvcHRpb246IGFueSkgPT4ge1xuICAgICAgICAgIHJldHVybiBpdGVtSWQudG9TdHJpbmcoKSA9PT0gb3B0aW9uLmlkLnRvU3RyaW5nKCk7XG4gICAgICAgIH0pIHx8IHt9O1xuICAgICAgcmV0dXJuIGl0ZW0udGV4dCB8fCBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gIH07XG5cblxuICBhamF4RGVsZXRlID0gKGluZGV4OiBhbnkpID0+IHtcblxuICB9O1xuXG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG5cbiAgZ2V0UHJvcChwcm9wTmFtZT86IHN0cmluZykge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG5cbiAgaW5pdFF1ZXJ5UGFyYW1zID0gKCkgPT4ge1xuICAgIGNvbnN0IGluaXRQYXJhbXMgPSB7fTtcbiAgICByZXR1cm4gaW5pdFBhcmFtcztcbiAgfTtcblxuXG4gIGNhbGNHc3QgPSAoKSA9PiB7XG4gICAgcmV0dXJuIDA7XG4gIH07XG5cbiAgY2FsY0Ftb3VudCA9ICgpID0+IHtcbiAgICByZXR1cm4gMDtcbiAgfTtcblxuICBpbml0UXVlcnlQYWdlUmVmID0gKHJlZjogYW55KSA9PiB7XG4gICAgdGhpcy5xdWVyeVBhZ2UgPSByZWY7XG4gIH07XG5cblxufVxuXG5leHBvcnQgeyBCYXNpY0xvY2F0aW9uIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFVQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFZQTtBQU9BO0FBQ0E7QUFRQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUVBO0FBQ0E7QUF5SUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFPQTtBQUNBO0FBb0JBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFVQTs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQXRRQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBU0E7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWtCQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7OztBQUdBO0FBQ0E7OztBQWNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBZ0JBOzs7QUF3QkE7QUFDQTs7O0FBSUE7QUFDQTtBQUNBOzs7QUF5QkE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7Ozs7QUFwUEE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-location.tsx
