/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandInsertNewline
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

function keyCommandInsertNewline(editorState) {
  var contentState = DraftModifier.splitBlock(editorState.getCurrentContent(), editorState.getSelection());
  return EditorState.push(editorState, contentState, 'split-block');
}

module.exports = keyCommandInsertNewline;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRJbnNlcnROZXdsaW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRJbnNlcnROZXdsaW5lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZEluc2VydE5ld2xpbmVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcblxuZnVuY3Rpb24ga2V5Q29tbWFuZEluc2VydE5ld2xpbmUoZWRpdG9yU3RhdGUpIHtcbiAgdmFyIGNvbnRlbnRTdGF0ZSA9IERyYWZ0TW9kaWZpZXIuc3BsaXRCbG9jayhlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSk7XG4gIHJldHVybiBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBjb250ZW50U3RhdGUsICdzcGxpdC1ibG9jaycpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGtleUNvbW1hbmRJbnNlcnROZXdsaW5lOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandInsertNewline.js
