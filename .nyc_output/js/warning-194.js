

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.warning = warning;
exports.note = note;
exports.resetWarned = resetWarned;
exports.call = call;
exports.warningOnce = warningOnce;
exports.noteOnce = noteOnce;
exports.default = void 0;
/* eslint-disable no-console */

var warned = {};

function warning(valid, message) {
  // Support uglify
  if ( true && !valid && console !== undefined) {
    console.error("Warning: ".concat(message));
  }
}

function note(valid, message) {
  // Support uglify
  if ( true && !valid && console !== undefined) {
    console.warn("Note: ".concat(message));
  }
}

function resetWarned() {
  warned = {};
}

function call(method, valid, message) {
  if (!valid && !warned[message]) {
    method(false, message);
    warned[message] = true;
  }
}

function warningOnce(valid, message) {
  call(warning, valid, message);
}

function noteOnce(valid, message) {
  call(note, valid, message);
}

var _default = warningOnce;
/* eslint-enable */

exports.default = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9saWIvd2FybmluZy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXV0aWwvbGliL3dhcm5pbmcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLndhcm5pbmcgPSB3YXJuaW5nO1xuZXhwb3J0cy5ub3RlID0gbm90ZTtcbmV4cG9ydHMucmVzZXRXYXJuZWQgPSByZXNldFdhcm5lZDtcbmV4cG9ydHMuY2FsbCA9IGNhbGw7XG5leHBvcnRzLndhcm5pbmdPbmNlID0gd2FybmluZ09uY2U7XG5leHBvcnRzLm5vdGVPbmNlID0gbm90ZU9uY2U7XG5leHBvcnRzLmRlZmF1bHQgPSB2b2lkIDA7XG5cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnNvbGUgKi9cbnZhciB3YXJuZWQgPSB7fTtcblxuZnVuY3Rpb24gd2FybmluZyh2YWxpZCwgbWVzc2FnZSkge1xuICAvLyBTdXBwb3J0IHVnbGlmeVxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiAhdmFsaWQgJiYgY29uc29sZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgY29uc29sZS5lcnJvcihcIldhcm5pbmc6IFwiLmNvbmNhdChtZXNzYWdlKSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gbm90ZSh2YWxpZCwgbWVzc2FnZSkge1xuICAvLyBTdXBwb3J0IHVnbGlmeVxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiAhdmFsaWQgJiYgY29uc29sZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgY29uc29sZS53YXJuKFwiTm90ZTogXCIuY29uY2F0KG1lc3NhZ2UpKTtcbiAgfVxufVxuXG5mdW5jdGlvbiByZXNldFdhcm5lZCgpIHtcbiAgd2FybmVkID0ge307XG59XG5cbmZ1bmN0aW9uIGNhbGwobWV0aG9kLCB2YWxpZCwgbWVzc2FnZSkge1xuICBpZiAoIXZhbGlkICYmICF3YXJuZWRbbWVzc2FnZV0pIHtcbiAgICBtZXRob2QoZmFsc2UsIG1lc3NhZ2UpO1xuICAgIHdhcm5lZFttZXNzYWdlXSA9IHRydWU7XG4gIH1cbn1cblxuZnVuY3Rpb24gd2FybmluZ09uY2UodmFsaWQsIG1lc3NhZ2UpIHtcbiAgY2FsbCh3YXJuaW5nLCB2YWxpZCwgbWVzc2FnZSk7XG59XG5cbmZ1bmN0aW9uIG5vdGVPbmNlKHZhbGlkLCBtZXNzYWdlKSB7XG4gIGNhbGwobm90ZSwgdmFsaWQsIG1lc3NhZ2UpO1xufVxuXG52YXIgX2RlZmF1bHQgPSB3YXJuaW5nT25jZTtcbi8qIGVzbGludC1lbmFibGUgKi9cblxuZXhwb3J0cy5kZWZhdWx0ID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-util/lib/warning.js
