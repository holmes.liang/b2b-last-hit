var _util = __webpack_require__(/*! ./util */ "./node_modules/zrender/lib/contain/util.js");

var normalizeRadian = _util.normalizeRadian;
var PI2 = Math.PI * 2;
/**
 * 圆弧描边包含判断
 * @param  {number}  cx
 * @param  {number}  cy
 * @param  {number}  r
 * @param  {number}  startAngle
 * @param  {number}  endAngle
 * @param  {boolean}  anticlockwise
 * @param  {number} lineWidth
 * @param  {number}  x
 * @param  {number}  y
 * @return {Boolean}
 */

function containStroke(cx, cy, r, startAngle, endAngle, anticlockwise, lineWidth, x, y) {
  if (lineWidth === 0) {
    return false;
  }

  var _l = lineWidth;
  x -= cx;
  y -= cy;
  var d = Math.sqrt(x * x + y * y);

  if (d - _l > r || d + _l < r) {
    return false;
  }

  if (Math.abs(startAngle - endAngle) % PI2 < 1e-4) {
    // Is a circle
    return true;
  }

  if (anticlockwise) {
    var tmp = startAngle;
    startAngle = normalizeRadian(endAngle);
    endAngle = normalizeRadian(tmp);
  } else {
    startAngle = normalizeRadian(startAngle);
    endAngle = normalizeRadian(endAngle);
  }

  if (startAngle > endAngle) {
    endAngle += PI2;
  }

  var angle = Math.atan2(y, x);

  if (angle < 0) {
    angle += PI2;
  }

  return angle >= startAngle && angle <= endAngle || angle + PI2 >= startAngle && angle + PI2 <= endAngle;
}

exports.containStroke = containStroke;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9hcmMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb250YWluL2FyYy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX3V0aWwgPSByZXF1aXJlKFwiLi91dGlsXCIpO1xuXG52YXIgbm9ybWFsaXplUmFkaWFuID0gX3V0aWwubm9ybWFsaXplUmFkaWFuO1xudmFyIFBJMiA9IE1hdGguUEkgKiAyO1xuLyoqXG4gKiDlnIblvKfmj4/ovrnljIXlkKvliKTmlq1cbiAqIEBwYXJhbSAge251bWJlcn0gIGN4XG4gKiBAcGFyYW0gIHtudW1iZXJ9ICBjeVxuICogQHBhcmFtICB7bnVtYmVyfSAgclxuICogQHBhcmFtICB7bnVtYmVyfSAgc3RhcnRBbmdsZVxuICogQHBhcmFtICB7bnVtYmVyfSAgZW5kQW5nbGVcbiAqIEBwYXJhbSAge2Jvb2xlYW59ICBhbnRpY2xvY2t3aXNlXG4gKiBAcGFyYW0gIHtudW1iZXJ9IGxpbmVXaWR0aFxuICogQHBhcmFtICB7bnVtYmVyfSAgeFxuICogQHBhcmFtICB7bnVtYmVyfSAgeVxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqL1xuXG5mdW5jdGlvbiBjb250YWluU3Ryb2tlKGN4LCBjeSwgciwgc3RhcnRBbmdsZSwgZW5kQW5nbGUsIGFudGljbG9ja3dpc2UsIGxpbmVXaWR0aCwgeCwgeSkge1xuICBpZiAobGluZVdpZHRoID09PSAwKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgdmFyIF9sID0gbGluZVdpZHRoO1xuICB4IC09IGN4O1xuICB5IC09IGN5O1xuICB2YXIgZCA9IE1hdGguc3FydCh4ICogeCArIHkgKiB5KTtcblxuICBpZiAoZCAtIF9sID4gciB8fCBkICsgX2wgPCByKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgaWYgKE1hdGguYWJzKHN0YXJ0QW5nbGUgLSBlbmRBbmdsZSkgJSBQSTIgPCAxZS00KSB7XG4gICAgLy8gSXMgYSBjaXJjbGVcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGlmIChhbnRpY2xvY2t3aXNlKSB7XG4gICAgdmFyIHRtcCA9IHN0YXJ0QW5nbGU7XG4gICAgc3RhcnRBbmdsZSA9IG5vcm1hbGl6ZVJhZGlhbihlbmRBbmdsZSk7XG4gICAgZW5kQW5nbGUgPSBub3JtYWxpemVSYWRpYW4odG1wKTtcbiAgfSBlbHNlIHtcbiAgICBzdGFydEFuZ2xlID0gbm9ybWFsaXplUmFkaWFuKHN0YXJ0QW5nbGUpO1xuICAgIGVuZEFuZ2xlID0gbm9ybWFsaXplUmFkaWFuKGVuZEFuZ2xlKTtcbiAgfVxuXG4gIGlmIChzdGFydEFuZ2xlID4gZW5kQW5nbGUpIHtcbiAgICBlbmRBbmdsZSArPSBQSTI7XG4gIH1cblxuICB2YXIgYW5nbGUgPSBNYXRoLmF0YW4yKHksIHgpO1xuXG4gIGlmIChhbmdsZSA8IDApIHtcbiAgICBhbmdsZSArPSBQSTI7XG4gIH1cblxuICByZXR1cm4gYW5nbGUgPj0gc3RhcnRBbmdsZSAmJiBhbmdsZSA8PSBlbmRBbmdsZSB8fCBhbmdsZSArIFBJMiA+PSBzdGFydEFuZ2xlICYmIGFuZ2xlICsgUEkyIDw9IGVuZEFuZ2xlO1xufVxuXG5leHBvcnRzLmNvbnRhaW5TdHJva2UgPSBjb250YWluU3Ryb2tlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/arc.js
