__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var _desk_component_field_group__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/field-group */ "./src/app/desk/component/field-group.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/address/spgAddress.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        .usage-group{\n          .ant-col {\n            >label {\n              text-transform: uppercase\n            }\n          }\n        }\n        .group .ant-row {\n          &:first-child {\n            width: 100%;\n            .ant-col {\n              .ant-input {\n                border-left: 1px solid #ebebeb;\n                border-top-left-radius: 4px;\n                border-bottom-left-radius: 4px;\n                border-top-right-radius: 0;\n                border-bottom-right-radius: 0;\n                &:focus, &:hover {\n                  border-color: ", ";\n                }\n              }\n              .has-error .ant-input {\n                border-color: #f5222d;\n              }\n            }\n          }\n        }\n        .address-view-row{\n          margin-top: -10px;\n          margin-bottom: 20px;\n          .mobile-with {\n            width: 100%;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_12__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();
var defaultLayout = {
  selectXsSm: {
    xs: 8,
    sm: 6
  },
  textXsSm: {
    xs: 16,
    sm: 13
  }
};

var SpgAddress =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(SpgAddress, _ModelWidget);

  function SpgAddress(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, SpgAddress);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(SpgAddress).call(this, props, state));

    _this.generatePropsName = function (propName, dataId) {
      var isNotAddressPresix = _this.props.isNotAddressPresix;
      var addressFix = _this.props.addressFix;

      if (addressFix) {
        if (!!dataId) return isNotAddressPresix ? "".concat(dataId, ".").concat(propName) : "".concat(dataId, ".").concat(addressFix, ".").concat(propName);else return isNotAddressPresix ? propName : "".concat(addressFix, ".").concat(propName);
      }

      if (!!dataId) return isNotAddressPresix ? "".concat(dataId, ".").concat(propName) : "".concat(dataId, ".address.").concat(propName);
      return isNotAddressPresix ? propName : "address.".concat(propName);
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(SpgAddress, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getAddress = lodash__WEBPACK_IMPORTED_MODULE_15___default.a.debounce(this.getAddress, 300);
      this.getAddressContent = lodash__WEBPACK_IMPORTED_MODULE_15___default.a.debounce(this.getAddressContent, 300);

      if (this.getAddressInfoData() && Object.values(this.getAddressInfoData()).length > 0) {
        this.getAddressContent();
      }
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject(), COLOR_A)
      };
    }
  }, {
    key: "getAddressInfoData",
    value: function getAddressInfoData() {
      var _this$props = this.props,
          dataId = _this$props.dataId,
          isNotAddressPresix = _this$props.isNotAddressPresix,
          addressFix = _this$props.addressFix;
      var addressInfo = this.getValueFromModel(dataId ? isNotAddressPresix ? dataId : addressFix ? "".concat(dataId, ".").concat(addressFix) : "".concat(dataId, ".address") : "address");
      return addressInfo;
    }
  }, {
    key: "getAddressContent",
    value: function getAddressContent() {
      var _this2 = this;

      var dataId = this.props.dataId;
      if (lodash__WEBPACK_IMPORTED_MODULE_15___default.a.isEmpty(this.getValueFromModel(this.generatePropsName("postalCode", dataId)))) return;
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post("/addresses/full/1_line", this.getAddressInfoData()).then(function (res) {
        var respData = (res.body || {}).respData || "";

        _this2.setState({
          addressInfoContent: respData === "null" ? "" : respData
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();
      var _this$props2 = this.props,
          dataId = _this$props2.dataId,
          form = _this$props2.form,
          model = _this$props2.model,
          _this$props2$required = _this$props2.required,
          required = _this$props2$required === void 0 ? true : _this$props2$required,
          selectXsSm = _this$props2.selectXsSm,
          textXsSm = _this$props2.textXsSm;
      var addressInfoContent = this.state.addressInfoContent;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_field_group__WEBPACK_IMPORTED_MODULE_13__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en(this.props.propLabel || "Address").thai("Address").getMessage(),
        required: required,
        selectXsSm: this.props.selectXsSm || defaultLayout.selectXsSm,
        textXsSm: this.props.textXsSm || defaultLayout.textXsSm,
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NText"], {
        required: required,
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Postal Code").thai("Postal Code").getMessage(),
        placeholder: "Postal Code",
        propName: this.generatePropsName("postalCode", dataId),
        onChange: function onChange(value) {
          if (/^([1-9][0-9]{5})$/.test(value)) {
            _this3.getAddress(value);
          }
        },
        rules: [{
          validator: function validator(rule, value, callback) {
            if (value && !required && !/^([1-9][0-9]{5})$/.test(value) || required && !/^([1-9][0-9]{5})$/.test(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Postal Code is valid").thai("Postal Code is invalid").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NText"], {
        required: false,
        form: form,
        model: model,
        placeholder: "Unit No.",
        onChange: function onChange() {
          _this3.getAddressContent();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Unit No.").thai("Unit No.").getMessage(),
        propName: this.generatePropsName("unitNo", dataId),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      })), addressInfoContent && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        className: "address-view-row",
        gutter: 16,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: selectXsSm && lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(selectXsSm, "sm.span", 6) || 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: textXsSm && lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(textXsSm, "sm.span", 13) || 13,
        className: isMobile ? "mobile-with" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, addressInfoContent))));
    }
  }, {
    key: "getAddress",
    value: function getAddress(value) {
      var _this4 = this;

      var _this$props3 = this.props,
          dataId = _this$props3.dataId,
          isNotAddressPresix = _this$props3.isNotAddressPresix,
          addressFix = _this$props3.addressFix;
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/addresses/postalcode", {
        countryCode: "SGP",
        postalCode: value
      }).then(function (res) {
        _this4.setState({
          addressInfo: (res.body || {}).respData || {}
        }, function () {
          var addressInfo = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this4.state.addressInfo, {}, {
            unitNo: lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(_this4.props.model, _this4.generatePropsName("unitNo", dataId))
          });

          _this4.setValueToModel(addressInfo, dataId ? isNotAddressPresix ? dataId : addressFix ? "".concat(dataId, ".").concat(addressFix) : "".concat(dataId, ".address") : "address");

          _this4.getAddressContent();
        });
      });
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(SpgAddress.prototype), "initState", this).call(this), {
        addressInfo: {},
        addressInfoContent: ""
      });
    }
  }]);

  return SpgAddress;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (SpgAddress);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3Mvc3BnQWRkcmVzcy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvYWRkcmVzcy9zcGdBZGRyZXNzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTdHlsZWRESVYsIFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IENvbCwgUm93IH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuaW1wb3J0IEZpZWxkR3JvdXAgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9maWVsZC1ncm91cFwiO1xuaW1wb3J0IHsgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuY29uc3QgeyBDT0xPUl9BIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgc2VsZWN0WHNTbToge1xuICAgIHhzOiA4LFxuICAgIHNtOiA2LFxuICB9LFxuICB0ZXh0WHNTbToge1xuICAgIHhzOiAxNixcbiAgICBzbTogMTMsXG4gIH0sXG59O1xuZXhwb3J0IHR5cGUgU3BnQWRkcmVzc1Byb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBmb3JtOiBhbnk7XG4gIGRhdGFJZD86IHN0cmluZztcbiAgY291bnRyeUNvZGU/OiBzdHJpbmc7XG4gIHNlbGVjdFhzU20/OiBhbnk7XG4gIHRleHRYc1NtPzogYW55O1xuICByZXF1aXJlZD86IGJvb2xlYW47XG4gIGFkZHJlc3NGaXg/OiBhbnk7XG4gIHByb3BMYWJlbD86IGFueTtcbiAgaXNOb3RBZGRyZXNzUHJlc2l4PzogYm9vbGVhbjtcbn0gJiBXaWRnZXRQcm9wcztcblxuZXhwb3J0IHR5cGUgU3BnQWRkcmVzc1N0YXRlID0ge1xuICBhZGRyZXNzSW5mbzogYW55O1xuICBhZGRyZXNzSW5mb0NvbnRlbnQ6IHN0cmluZztcbn07XG5cbmV4cG9ydCB0eXBlIFNwZ0FkZHJlc3NDb21wb25lbnRzID0ge1xuICBCb3g6IFN0eWxlZERJVjtcbn07XG5cblxuY2xhc3MgU3BnQWRkcmVzczxQIGV4dGVuZHMgU3BnQWRkcmVzc1Byb3BzLCBTIGV4dGVuZHMgU3BnQWRkcmVzc1N0YXRlLCBDIGV4dGVuZHMgU3BnQWRkcmVzc0NvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBzdGF0ZT86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBzdGF0ZSk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmdldEFkZHJlc3MgPSBfLmRlYm91bmNlKHRoaXMuZ2V0QWRkcmVzcywgMzAwKTtcbiAgICB0aGlzLmdldEFkZHJlc3NDb250ZW50ID0gXy5kZWJvdW5jZSh0aGlzLmdldEFkZHJlc3NDb250ZW50LCAzMDApO1xuICAgIGlmICh0aGlzLmdldEFkZHJlc3NJbmZvRGF0YSgpICYmIE9iamVjdC52YWx1ZXModGhpcy5nZXRBZGRyZXNzSW5mb0RhdGEoKSkubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5nZXRBZGRyZXNzQ29udGVudCgpO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YFxuICAgICAgICAudXNhZ2UtZ3JvdXB7XG4gICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgPmxhYmVsIHtcbiAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZ3JvdXAgLmFudC1yb3cge1xuICAgICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAuYW50LWNvbCB7XG4gICAgICAgICAgICAgIC5hbnQtaW5wdXQge1xuICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1xuICAgICAgICAgICAgICAgICY6Zm9jdXMsICY6aG92ZXIge1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAke0NPTE9SX0F9O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuaGFzLWVycm9yIC5hbnQtaW5wdXQge1xuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2Y1MjIyZDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuYWRkcmVzcy12aWV3LXJvd3tcbiAgICAgICAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICAgIC5tb2JpbGUtd2l0aCB7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgZ2V0QWRkcmVzc0luZm9EYXRhKCkge1xuICAgIGNvbnN0IHsgZGF0YUlkLCBpc05vdEFkZHJlc3NQcmVzaXgsIGFkZHJlc3NGaXggfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgYWRkcmVzc0luZm8gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGRhdGFJZCA/IChpc05vdEFkZHJlc3NQcmVzaXggPyBkYXRhSWQgOiAoYWRkcmVzc0ZpeCA/IGAke2RhdGFJZH0uJHthZGRyZXNzRml4fWAgOiBgJHtkYXRhSWR9LmFkZHJlc3NgKSkgOiBcImFkZHJlc3NcIik7XG4gICAgcmV0dXJuIGFkZHJlc3NJbmZvO1xuICB9XG5cbiAgZ2V0QWRkcmVzc0NvbnRlbnQoKSB7XG4gICAgY29uc3QgeyBkYXRhSWQgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBpZiAoXy5pc0VtcHR5KHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BzTmFtZShcInBvc3RhbENvZGVcIiwgZGF0YUlkKSkpKSByZXR1cm47XG4gICAgQWpheC5wb3N0KGAvYWRkcmVzc2VzL2Z1bGwvMV9saW5lYCwgdGhpcy5nZXRBZGRyZXNzSW5mb0RhdGEoKSkudGhlbigocmVzOiBhbnkpID0+IHtcbiAgICAgIGNvbnN0IHJlc3BEYXRhID0gKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCBcIlwiO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFkZHJlc3NJbmZvQ29udGVudDogcmVzcERhdGEgPT09IFwibnVsbFwiID8gXCJcIiA6IHJlc3BEYXRhLFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBnZW5lcmF0ZVByb3BzTmFtZSA9IChwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWQ/OiBzdHJpbmcpOiBzdHJpbmcgPT4ge1xuICAgIGNvbnN0IHsgaXNOb3RBZGRyZXNzUHJlc2l4IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgYWRkcmVzc0ZpeCB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoYWRkcmVzc0ZpeCkge1xuICAgICAgaWYgKCEhZGF0YUlkKSByZXR1cm4gaXNOb3RBZGRyZXNzUHJlc2l4ID8gYCR7ZGF0YUlkfS4ke3Byb3BOYW1lfWAgOiBgJHtkYXRhSWR9LiR7YWRkcmVzc0ZpeH0uJHtwcm9wTmFtZX1gO1xuICAgICAgZWxzZSByZXR1cm4gaXNOb3RBZGRyZXNzUHJlc2l4ID8gcHJvcE5hbWUgOiBgJHthZGRyZXNzRml4fS4ke3Byb3BOYW1lfWA7XG4gICAgfVxuICAgIGlmICghIWRhdGFJZCkgcmV0dXJuIGlzTm90QWRkcmVzc1ByZXNpeCA/IGAke2RhdGFJZH0uJHtwcm9wTmFtZX1gIDogYCR7ZGF0YUlkfS5hZGRyZXNzLiR7cHJvcE5hbWV9YDtcbiAgICByZXR1cm4gaXNOb3RBZGRyZXNzUHJlc2l4ID8gcHJvcE5hbWUgOiBgYWRkcmVzcy4ke3Byb3BOYW1lfWA7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IGRhdGFJZCwgZm9ybSwgbW9kZWwsIHJlcXVpcmVkID0gdHJ1ZSwgc2VsZWN0WHNTbSwgdGV4dFhzU20gfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBhZGRyZXNzSW5mb0NvbnRlbnQgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveD5cbiAgICAgICAgPEZpZWxkR3JvdXBcbiAgICAgICAgICBjbGFzc05hbWU9XCJ1c2FnZS1ncm91cFwiXG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKHRoaXMucHJvcHMucHJvcExhYmVsIHx8IFwiQWRkcmVzc1wiKVxuICAgICAgICAgICAgLnRoYWkoXCJBZGRyZXNzXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICBzZWxlY3RYc1NtPXt0aGlzLnByb3BzLnNlbGVjdFhzU20gfHwgZGVmYXVsdExheW91dC5zZWxlY3RYc1NtfVxuICAgICAgICAgIHRleHRYc1NtPXt0aGlzLnByb3BzLnRleHRYc1NtIHx8IGRlZmF1bHRMYXlvdXQudGV4dFhzU219XG4gICAgICAgICAgbWluV2lkdGg9XCIxNDBweFwiXG4gICAgICAgID5cbiAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQb3N0YWwgQ29kZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIlBvc3RhbCBDb2RlXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBvc3RhbCBDb2RlXCJcbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcHNOYW1lKFwicG9zdGFsQ29kZVwiLCBkYXRhSWQpfVxuICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgIGlmICgvXihbMS05XVswLTldezV9KSQvLnRlc3QodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRBZGRyZXNzKHZhbHVlIGFzIGFueSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH19XG5cbiAgICAgICAgICAgIHJ1bGVzPXtbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB2YWxpZGF0b3IocnVsZTogYW55LCB2YWx1ZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgICAgICAgICAgICBpZiAoKHZhbHVlICYmICFyZXF1aXJlZCAmJiAhL14oWzEtOV1bMC05XXs1fSkkLy50ZXN0KHZhbHVlKSlcbiAgICAgICAgICAgICAgICAgICAgfHwgKHJlcXVpcmVkICYmICEvXihbMS05XVswLTldezV9KSQvLnRlc3QodmFsdWUpKSkge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhMYW5ndWFnZS5lbihcIlBvc3RhbCBDb2RlIGlzIHZhbGlkXCIpLnRoYWkoXCJQb3N0YWwgQ29kZSBpcyBpbnZhbGlkXCIpLmdldE1lc3NhZ2UoKSk7XG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPE5UZXh0XG4gICAgICAgICAgICByZXF1aXJlZD17ZmFsc2V9XG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJVbml0IE5vLlwiXG4gICAgICAgICAgICBvbkNoYW5nZT17KCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmdldEFkZHJlc3NDb250ZW50KCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVW5pdCBOby5cIilcbiAgICAgICAgICAgICAgLnRoYWkoXCJVbml0IE5vLlwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wc05hbWUoXCJ1bml0Tm9cIiwgZGF0YUlkKX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICAgIHthZGRyZXNzSW5mb0NvbnRlbnQgJiZcbiAgICAgICAgPD5cbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImFkZHJlc3Mtdmlldy1yb3dcIiBndXR0ZXI9ezE2fT5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17KHNlbGVjdFhzU20gJiYgXy5nZXQoc2VsZWN0WHNTbSwgXCJzbS5zcGFuXCIsIDYpKSB8fCA2fS8+XG4gICAgICAgICAgICA8Q29sIHNwYW49eyh0ZXh0WHNTbSAmJiBfLmdldCh0ZXh0WHNTbSwgXCJzbS5zcGFuXCIsIDEzKSkgfHwgMTN9XG4gICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17aXNNb2JpbGUgPyBcIm1vYmlsZS13aXRoXCIgOiBcIlwifT5cbiAgICAgICAgICAgICAge2FkZHJlc3NJbmZvQ29udGVudH1cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgIDwvUm93PlxuICAgICAgICA8Lz5cbiAgICAgICAgfVxuICAgICAgPC9DLkJveD5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEFkZHJlc3ModmFsdWU6IGFueSkge1xuICAgIGNvbnN0IHsgZGF0YUlkLCBpc05vdEFkZHJlc3NQcmVzaXgsIGFkZHJlc3NGaXggfSA9IHRoaXMucHJvcHM7XG4gICAgQWpheC5nZXQoXCIvYWRkcmVzc2VzL3Bvc3RhbGNvZGVcIiwge1xuICAgICAgY291bnRyeUNvZGU6IFwiU0dQXCIsXG4gICAgICBwb3N0YWxDb2RlOiB2YWx1ZSxcbiAgICB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhZGRyZXNzSW5mbzogKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCB7fSxcbiAgICAgIH0sICgpID0+IHtcbiAgICAgICAgY29uc3QgYWRkcmVzc0luZm8gPSB7XG4gICAgICAgICAgLi4udGhpcy5zdGF0ZS5hZGRyZXNzSW5mbyxcbiAgICAgICAgICAuLi57IHVuaXRObzogXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgdGhpcy5nZW5lcmF0ZVByb3BzTmFtZShcInVuaXROb1wiLCBkYXRhSWQpKSB9LFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChhZGRyZXNzSW5mbywgZGF0YUlkID8gKGlzTm90QWRkcmVzc1ByZXNpeCA/IGRhdGFJZCA6IChhZGRyZXNzRml4ID8gYCR7ZGF0YUlkfS4ke2FkZHJlc3NGaXh9YCA6IGAke2RhdGFJZH0uYWRkcmVzc2ApKSA6IFwiYWRkcmVzc1wiKTtcbiAgICAgICAgdGhpcy5nZXRBZGRyZXNzQ29udGVudCgpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBhZGRyZXNzSW5mbzoge30sXG4gICAgICBhZGRyZXNzSW5mb0NvbnRlbnQ6IFwiXCIsXG4gICAgfSkgYXMgUztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTcGdBZGRyZXNzO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBTEE7QUFDQTtBQWdDQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBdUVBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFqRkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQXNDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQWFBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7OztBQXBMQTtBQUNBO0FBc0xBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/address/spgAddress.tsx
