

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/rc-table/es/utils.js");

var BaseTable_1 = __importDefault(__webpack_require__(/*! ./BaseTable */ "./node_modules/rc-table/es/BaseTable.js"));

function BodyTable(props, _ref) {
  var table = _ref.table;
  var _table$props = table.props,
      prefixCls = _table$props.prefixCls,
      scroll = _table$props.scroll;
  var columns = props.columns,
      fixed = props.fixed,
      tableClassName = props.tableClassName,
      getRowKey = props.getRowKey,
      handleBodyScroll = props.handleBodyScroll,
      handleWheel = props.handleWheel,
      expander = props.expander,
      isAnyColumnsFixed = props.isAnyColumnsFixed;
  var saveRef = table.saveRef;
  var useFixedHeader = table.props.useFixedHeader;

  var bodyStyle = _objectSpread({}, table.props.bodyStyle);

  var innerBodyStyle = {};

  if (scroll.x || fixed) {
    bodyStyle.overflowX = bodyStyle.overflowX || 'scroll'; // Fix weird webkit render bug
    // https://github.com/ant-design/ant-design/issues/7783

    bodyStyle.WebkitTransform = 'translate3d (0, 0, 0)';
  }

  if (scroll.y) {
    // maxHeight will make fixed-Table scrolling not working
    // so we only set maxHeight to body-Table here
    if (fixed) {
      innerBodyStyle.maxHeight = bodyStyle.maxHeight || scroll.y;
      innerBodyStyle.overflowY = bodyStyle.overflowY || 'scroll';
    } else {
      bodyStyle.maxHeight = bodyStyle.maxHeight || scroll.y;
    }

    bodyStyle.overflowY = bodyStyle.overflowY || 'scroll';
    useFixedHeader = true; // Add negative margin bottom for scroll bar overflow bug

    var scrollbarWidth = utils_1.measureScrollbar({
      direction: 'vertical'
    });

    if (scrollbarWidth > 0 && fixed) {
      bodyStyle.marginBottom = "-".concat(scrollbarWidth, "px");
      bodyStyle.paddingBottom = '0px';
    }
  }

  var baseTable = React.createElement(BaseTable_1.default, {
    tableClassName: tableClassName,
    hasHead: !useFixedHeader,
    hasBody: true,
    fixed: fixed,
    columns: columns,
    expander: expander,
    getRowKey: getRowKey,
    isAnyColumnsFixed: isAnyColumnsFixed
  });

  if (fixed && columns.length) {
    var refName;

    if (columns[0].fixed === 'left' || columns[0].fixed === true) {
      refName = 'fixedColumnsBodyLeft';
    } else if (columns[0].fixed === 'right') {
      refName = 'fixedColumnsBodyRight';
    }

    delete bodyStyle.overflowX;
    delete bodyStyle.overflowY;
    return React.createElement("div", {
      key: "bodyTable",
      className: "".concat(prefixCls, "-body-outer"),
      style: _objectSpread({}, bodyStyle)
    }, React.createElement("div", {
      className: "".concat(prefixCls, "-body-inner"),
      style: innerBodyStyle,
      ref: saveRef(refName),
      onWheel: handleWheel,
      onScroll: handleBodyScroll
    }, baseTable));
  } // Should provides `tabIndex` if use scroll to enable keyboard scroll


  var useTabIndex = scroll && (scroll.x || scroll.y);
  return React.createElement("div", {
    tabIndex: useTabIndex ? -1 : undefined,
    key: "bodyTable",
    className: "".concat(prefixCls, "-body"),
    style: bodyStyle,
    ref: saveRef('bodyTable'),
    onWheel: handleWheel,
    onScroll: handleBodyScroll
  }, baseTable);
}

exports.default = BodyTable;
BodyTable.contextTypes = {
  table: PropTypes.any
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQm9keVRhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQm9keVRhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSksIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxudmFyIF9faW1wb3J0U3RhciA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydFN0YXIgfHwgZnVuY3Rpb24gKG1vZCkge1xuICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIHtcbiAgICBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xuICB9XG4gIHJlc3VsdFtcImRlZmF1bHRcIl0gPSBtb2Q7XG4gIHJldHVybiByZXN1bHQ7XG59O1xuXG52YXIgX19pbXBvcnREZWZhdWx0ID0gdGhpcyAmJiB0aGlzLl9faW1wb3J0RGVmYXVsdCB8fCBmdW5jdGlvbiAobW9kKSB7XG4gIHJldHVybiBtb2QgJiYgbW9kLl9fZXNNb2R1bGUgPyBtb2QgOiB7XG4gICAgXCJkZWZhdWx0XCI6IG1vZFxuICB9O1xufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIFJlYWN0ID0gX19pbXBvcnRTdGFyKHJlcXVpcmUoXCJyZWFjdFwiKSk7XG5cbnZhciBQcm9wVHlwZXMgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInByb3AtdHlwZXNcIikpO1xuXG52YXIgdXRpbHNfMSA9IHJlcXVpcmUoXCIuL3V0aWxzXCIpO1xuXG52YXIgQmFzZVRhYmxlXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vQmFzZVRhYmxlXCIpKTtcblxuZnVuY3Rpb24gQm9keVRhYmxlKHByb3BzLCBfcmVmKSB7XG4gIHZhciB0YWJsZSA9IF9yZWYudGFibGU7XG4gIHZhciBfdGFibGUkcHJvcHMgPSB0YWJsZS5wcm9wcyxcbiAgICAgIHByZWZpeENscyA9IF90YWJsZSRwcm9wcy5wcmVmaXhDbHMsXG4gICAgICBzY3JvbGwgPSBfdGFibGUkcHJvcHMuc2Nyb2xsO1xuICB2YXIgY29sdW1ucyA9IHByb3BzLmNvbHVtbnMsXG4gICAgICBmaXhlZCA9IHByb3BzLmZpeGVkLFxuICAgICAgdGFibGVDbGFzc05hbWUgPSBwcm9wcy50YWJsZUNsYXNzTmFtZSxcbiAgICAgIGdldFJvd0tleSA9IHByb3BzLmdldFJvd0tleSxcbiAgICAgIGhhbmRsZUJvZHlTY3JvbGwgPSBwcm9wcy5oYW5kbGVCb2R5U2Nyb2xsLFxuICAgICAgaGFuZGxlV2hlZWwgPSBwcm9wcy5oYW5kbGVXaGVlbCxcbiAgICAgIGV4cGFuZGVyID0gcHJvcHMuZXhwYW5kZXIsXG4gICAgICBpc0FueUNvbHVtbnNGaXhlZCA9IHByb3BzLmlzQW55Q29sdW1uc0ZpeGVkO1xuICB2YXIgc2F2ZVJlZiA9IHRhYmxlLnNhdmVSZWY7XG4gIHZhciB1c2VGaXhlZEhlYWRlciA9IHRhYmxlLnByb3BzLnVzZUZpeGVkSGVhZGVyO1xuXG4gIHZhciBib2R5U3R5bGUgPSBfb2JqZWN0U3ByZWFkKHt9LCB0YWJsZS5wcm9wcy5ib2R5U3R5bGUpO1xuXG4gIHZhciBpbm5lckJvZHlTdHlsZSA9IHt9O1xuXG4gIGlmIChzY3JvbGwueCB8fCBmaXhlZCkge1xuICAgIGJvZHlTdHlsZS5vdmVyZmxvd1ggPSBib2R5U3R5bGUub3ZlcmZsb3dYIHx8ICdzY3JvbGwnOyAvLyBGaXggd2VpcmQgd2Via2l0IHJlbmRlciBidWdcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy83NzgzXG5cbiAgICBib2R5U3R5bGUuV2Via2l0VHJhbnNmb3JtID0gJ3RyYW5zbGF0ZTNkICgwLCAwLCAwKSc7XG4gIH1cblxuICBpZiAoc2Nyb2xsLnkpIHtcbiAgICAvLyBtYXhIZWlnaHQgd2lsbCBtYWtlIGZpeGVkLVRhYmxlIHNjcm9sbGluZyBub3Qgd29ya2luZ1xuICAgIC8vIHNvIHdlIG9ubHkgc2V0IG1heEhlaWdodCB0byBib2R5LVRhYmxlIGhlcmVcbiAgICBpZiAoZml4ZWQpIHtcbiAgICAgIGlubmVyQm9keVN0eWxlLm1heEhlaWdodCA9IGJvZHlTdHlsZS5tYXhIZWlnaHQgfHwgc2Nyb2xsLnk7XG4gICAgICBpbm5lckJvZHlTdHlsZS5vdmVyZmxvd1kgPSBib2R5U3R5bGUub3ZlcmZsb3dZIHx8ICdzY3JvbGwnO1xuICAgIH0gZWxzZSB7XG4gICAgICBib2R5U3R5bGUubWF4SGVpZ2h0ID0gYm9keVN0eWxlLm1heEhlaWdodCB8fCBzY3JvbGwueTtcbiAgICB9XG5cbiAgICBib2R5U3R5bGUub3ZlcmZsb3dZID0gYm9keVN0eWxlLm92ZXJmbG93WSB8fCAnc2Nyb2xsJztcbiAgICB1c2VGaXhlZEhlYWRlciA9IHRydWU7IC8vIEFkZCBuZWdhdGl2ZSBtYXJnaW4gYm90dG9tIGZvciBzY3JvbGwgYmFyIG92ZXJmbG93IGJ1Z1xuXG4gICAgdmFyIHNjcm9sbGJhcldpZHRoID0gdXRpbHNfMS5tZWFzdXJlU2Nyb2xsYmFyKHtcbiAgICAgIGRpcmVjdGlvbjogJ3ZlcnRpY2FsJ1xuICAgIH0pO1xuXG4gICAgaWYgKHNjcm9sbGJhcldpZHRoID4gMCAmJiBmaXhlZCkge1xuICAgICAgYm9keVN0eWxlLm1hcmdpbkJvdHRvbSA9IFwiLVwiLmNvbmNhdChzY3JvbGxiYXJXaWR0aCwgXCJweFwiKTtcbiAgICAgIGJvZHlTdHlsZS5wYWRkaW5nQm90dG9tID0gJzBweCc7XG4gICAgfVxuICB9XG5cbiAgdmFyIGJhc2VUYWJsZSA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoQmFzZVRhYmxlXzEuZGVmYXVsdCwge1xuICAgIHRhYmxlQ2xhc3NOYW1lOiB0YWJsZUNsYXNzTmFtZSxcbiAgICBoYXNIZWFkOiAhdXNlRml4ZWRIZWFkZXIsXG4gICAgaGFzQm9keTogdHJ1ZSxcbiAgICBmaXhlZDogZml4ZWQsXG4gICAgY29sdW1uczogY29sdW1ucyxcbiAgICBleHBhbmRlcjogZXhwYW5kZXIsXG4gICAgZ2V0Um93S2V5OiBnZXRSb3dLZXksXG4gICAgaXNBbnlDb2x1bW5zRml4ZWQ6IGlzQW55Q29sdW1uc0ZpeGVkXG4gIH0pO1xuXG4gIGlmIChmaXhlZCAmJiBjb2x1bW5zLmxlbmd0aCkge1xuICAgIHZhciByZWZOYW1lO1xuXG4gICAgaWYgKGNvbHVtbnNbMF0uZml4ZWQgPT09ICdsZWZ0JyB8fCBjb2x1bW5zWzBdLmZpeGVkID09PSB0cnVlKSB7XG4gICAgICByZWZOYW1lID0gJ2ZpeGVkQ29sdW1uc0JvZHlMZWZ0JztcbiAgICB9IGVsc2UgaWYgKGNvbHVtbnNbMF0uZml4ZWQgPT09ICdyaWdodCcpIHtcbiAgICAgIHJlZk5hbWUgPSAnZml4ZWRDb2x1bW5zQm9keVJpZ2h0JztcbiAgICB9XG5cbiAgICBkZWxldGUgYm9keVN0eWxlLm92ZXJmbG93WDtcbiAgICBkZWxldGUgYm9keVN0eWxlLm92ZXJmbG93WTtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICBrZXk6IFwiYm9keVRhYmxlXCIsXG4gICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItYm9keS1vdXRlclwiKSxcbiAgICAgIHN0eWxlOiBfb2JqZWN0U3ByZWFkKHt9LCBib2R5U3R5bGUpXG4gICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItYm9keS1pbm5lclwiKSxcbiAgICAgIHN0eWxlOiBpbm5lckJvZHlTdHlsZSxcbiAgICAgIHJlZjogc2F2ZVJlZihyZWZOYW1lKSxcbiAgICAgIG9uV2hlZWw6IGhhbmRsZVdoZWVsLFxuICAgICAgb25TY3JvbGw6IGhhbmRsZUJvZHlTY3JvbGxcbiAgICB9LCBiYXNlVGFibGUpKTtcbiAgfSAvLyBTaG91bGQgcHJvdmlkZXMgYHRhYkluZGV4YCBpZiB1c2Ugc2Nyb2xsIHRvIGVuYWJsZSBrZXlib2FyZCBzY3JvbGxcblxuXG4gIHZhciB1c2VUYWJJbmRleCA9IHNjcm9sbCAmJiAoc2Nyb2xsLnggfHwgc2Nyb2xsLnkpO1xuICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgdGFiSW5kZXg6IHVzZVRhYkluZGV4ID8gLTEgOiB1bmRlZmluZWQsXG4gICAga2V5OiBcImJvZHlUYWJsZVwiLFxuICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1ib2R5XCIpLFxuICAgIHN0eWxlOiBib2R5U3R5bGUsXG4gICAgcmVmOiBzYXZlUmVmKCdib2R5VGFibGUnKSxcbiAgICBvbldoZWVsOiBoYW5kbGVXaGVlbCxcbiAgICBvblNjcm9sbDogaGFuZGxlQm9keVNjcm9sbFxuICB9LCBiYXNlVGFibGUpO1xufVxuXG5leHBvcnRzLmRlZmF1bHQgPSBCb2R5VGFibGU7XG5Cb2R5VGFibGUuY29udGV4dFR5cGVzID0ge1xuICB0YWJsZTogUHJvcFR5cGVzLmFueVxufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/BodyTable.js
