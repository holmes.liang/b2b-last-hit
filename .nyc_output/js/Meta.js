__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};





var Meta = function Meta(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref) {
    var getPrefixCls = _ref.getPrefixCls;

    var customizePrefixCls = props.prefixCls,
        className = props.className,
        avatar = props.avatar,
        title = props.title,
        description = props.description,
        others = __rest(props, ["prefixCls", "className", "avatar", "title", "description"]);

    var prefixCls = getPrefixCls('card', customizePrefixCls);
    var classString = classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-meta"), className);
    var avatarDom = avatar ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-meta-avatar")
    }, avatar) : null;
    var titleDom = title ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-meta-title")
    }, title) : null;
    var descriptionDom = description ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-meta-description")
    }, description) : null;
    var MetaDetail = titleDom || descriptionDom ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-meta-detail")
    }, titleDom, descriptionDom) : null;
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, others, {
      className: classString
    }), avatarDom, MetaDetail);
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Meta);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jYXJkL01ldGEuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2NhcmQvTWV0YS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5jb25zdCBNZXRhID0gcHJvcHMgPT4gKDxDb25maWdDb25zdW1lcj5cbiAgICB7KHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUsIGF2YXRhciwgdGl0bGUsIGRlc2NyaXB0aW9uIH0gPSBwcm9wcywgb3RoZXJzID0gX19yZXN0KHByb3BzLCBbXCJwcmVmaXhDbHNcIiwgXCJjbGFzc05hbWVcIiwgXCJhdmF0YXJcIiwgXCJ0aXRsZVwiLCBcImRlc2NyaXB0aW9uXCJdKTtcbiAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2NhcmQnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgIGNvbnN0IGNsYXNzU3RyaW5nID0gY2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LW1ldGFgLCBjbGFzc05hbWUpO1xuICAgIGNvbnN0IGF2YXRhckRvbSA9IGF2YXRhciA/IDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LW1ldGEtYXZhdGFyYH0+e2F2YXRhcn08L2Rpdj4gOiBudWxsO1xuICAgIGNvbnN0IHRpdGxlRG9tID0gdGl0bGUgPyA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1tZXRhLXRpdGxlYH0+e3RpdGxlfTwvZGl2PiA6IG51bGw7XG4gICAgY29uc3QgZGVzY3JpcHRpb25Eb20gPSBkZXNjcmlwdGlvbiA/ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1tZXRhLWRlc2NyaXB0aW9uYH0+e2Rlc2NyaXB0aW9ufTwvZGl2PikgOiBudWxsO1xuICAgIGNvbnN0IE1ldGFEZXRhaWwgPSB0aXRsZURvbSB8fCBkZXNjcmlwdGlvbkRvbSA/ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1tZXRhLWRldGFpbGB9PlxuICAgICAgICAgICAge3RpdGxlRG9tfVxuICAgICAgICAgICAge2Rlc2NyaXB0aW9uRG9tfVxuICAgICAgICAgIDwvZGl2PikgOiBudWxsO1xuICAgIHJldHVybiAoPGRpdiB7Li4ub3RoZXJzfSBjbGFzc05hbWU9e2NsYXNzU3RyaW5nfT5cbiAgICAgICAgICB7YXZhdGFyRG9tfVxuICAgICAgICAgIHtNZXRhRGV0YWlsfVxuICAgICAgICA8L2Rpdj4pO1xufX1cbiAgPC9Db25maWdDb25zdW1lcj4pO1xuZXhwb3J0IGRlZmF1bHQgTWV0YTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBWkE7QUFBQTtBQUNBO0FBaUJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/card/Meta.js
