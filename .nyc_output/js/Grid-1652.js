/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var isObject = _util.isObject;
var each = _util.each;
var map = _util.map;
var indexOf = _util.indexOf;
var retrieve = _util.retrieve;

var _layout = __webpack_require__(/*! ../../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutRect = _layout.getLayoutRect;

var _axisHelper = __webpack_require__(/*! ../../coord/axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var createScaleByModel = _axisHelper.createScaleByModel;
var ifAxisCrossZero = _axisHelper.ifAxisCrossZero;
var niceScaleExtent = _axisHelper.niceScaleExtent;
var estimateLabelUnionRect = _axisHelper.estimateLabelUnionRect;

var Cartesian2D = __webpack_require__(/*! ./Cartesian2D */ "./node_modules/echarts/lib/coord/cartesian/Cartesian2D.js");

var Axis2D = __webpack_require__(/*! ./Axis2D */ "./node_modules/echarts/lib/coord/cartesian/Axis2D.js");

var CoordinateSystem = __webpack_require__(/*! ../../CoordinateSystem */ "./node_modules/echarts/lib/CoordinateSystem.js");

var _dataStackHelper = __webpack_require__(/*! ../../data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var getStackedDimension = _dataStackHelper.getStackedDimension;

__webpack_require__(/*! ./GridModel */ "./node_modules/echarts/lib/coord/cartesian/GridModel.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Grid is a region which contains at most 4 cartesian systems
 *
 * TODO Default cartesian
 */
// Depends on GridModel, AxisModel, which performs preprocess.

/**
 * Check if the axis is used in the specified grid
 * @inner
 */


function isAxisUsedInTheGrid(axisModel, gridModel, ecModel) {
  return axisModel.getCoordSysModel() === gridModel;
}

function Grid(gridModel, ecModel, api) {
  /**
   * @type {Object.<string, module:echarts/coord/cartesian/Cartesian2D>}
   * @private
   */
  this._coordsMap = {};
  /**
   * @type {Array.<module:echarts/coord/cartesian/Cartesian>}
   * @private
   */

  this._coordsList = [];
  /**
   * @type {Object.<string, module:echarts/coord/cartesian/Axis2D>}
   * @private
   */

  this._axesMap = {};
  /**
   * @type {Array.<module:echarts/coord/cartesian/Axis2D>}
   * @private
   */

  this._axesList = [];

  this._initCartesian(gridModel, ecModel, api);

  this.model = gridModel;
}

var gridProto = Grid.prototype;
gridProto.type = 'grid';
gridProto.axisPointerEnabled = true;

gridProto.getRect = function () {
  return this._rect;
};

gridProto.update = function (ecModel, api) {
  var axesMap = this._axesMap;

  this._updateScale(ecModel, this.model);

  each(axesMap.x, function (xAxis) {
    niceScaleExtent(xAxis.scale, xAxis.model);
  });
  each(axesMap.y, function (yAxis) {
    niceScaleExtent(yAxis.scale, yAxis.model);
  }); // Key: axisDim_axisIndex, value: boolean, whether onZero target.

  var onZeroRecords = {};
  each(axesMap.x, function (xAxis) {
    fixAxisOnZero(axesMap, 'y', xAxis, onZeroRecords);
  });
  each(axesMap.y, function (yAxis) {
    fixAxisOnZero(axesMap, 'x', yAxis, onZeroRecords);
  }); // Resize again if containLabel is enabled
  // FIXME It may cause getting wrong grid size in data processing stage

  this.resize(this.model, api);
};

function fixAxisOnZero(axesMap, otherAxisDim, axis, onZeroRecords) {
  axis.getAxesOnZeroOf = function () {
    // TODO: onZero of multiple axes.
    return otherAxisOnZeroOf ? [otherAxisOnZeroOf] : [];
  }; // onZero can not be enabled in these two situations:
  // 1. When any other axis is a category axis.
  // 2. When no axis is cross 0 point.


  var otherAxes = axesMap[otherAxisDim];
  var otherAxisOnZeroOf;
  var axisModel = axis.model;
  var onZero = axisModel.get('axisLine.onZero');
  var onZeroAxisIndex = axisModel.get('axisLine.onZeroAxisIndex');

  if (!onZero) {
    return;
  } // If target axis is specified.


  if (onZeroAxisIndex != null) {
    if (canOnZeroToAxis(otherAxes[onZeroAxisIndex])) {
      otherAxisOnZeroOf = otherAxes[onZeroAxisIndex];
    }
  } else {
    // Find the first available other axis.
    for (var idx in otherAxes) {
      if (otherAxes.hasOwnProperty(idx) && canOnZeroToAxis(otherAxes[idx]) // Consider that two Y axes on one value axis,
      // if both onZero, the two Y axes overlap.
      && !onZeroRecords[getOnZeroRecordKey(otherAxes[idx])]) {
        otherAxisOnZeroOf = otherAxes[idx];
        break;
      }
    }
  }

  if (otherAxisOnZeroOf) {
    onZeroRecords[getOnZeroRecordKey(otherAxisOnZeroOf)] = true;
  }

  function getOnZeroRecordKey(axis) {
    return axis.dim + '_' + axis.index;
  }
}

function canOnZeroToAxis(axis) {
  return axis && axis.type !== 'category' && axis.type !== 'time' && ifAxisCrossZero(axis);
}
/**
 * Resize the grid
 * @param {module:echarts/coord/cartesian/GridModel} gridModel
 * @param {module:echarts/ExtensionAPI} api
 */


gridProto.resize = function (gridModel, api, ignoreContainLabel) {
  var gridRect = getLayoutRect(gridModel.getBoxLayoutParams(), {
    width: api.getWidth(),
    height: api.getHeight()
  });
  this._rect = gridRect;
  var axesList = this._axesList;
  adjustAxes(); // Minus label size

  if (!ignoreContainLabel && gridModel.get('containLabel')) {
    each(axesList, function (axis) {
      if (!axis.model.get('axisLabel.inside')) {
        var labelUnionRect = estimateLabelUnionRect(axis);

        if (labelUnionRect) {
          var dim = axis.isHorizontal() ? 'height' : 'width';
          var margin = axis.model.get('axisLabel.margin');
          gridRect[dim] -= labelUnionRect[dim] + margin;

          if (axis.position === 'top') {
            gridRect.y += labelUnionRect.height + margin;
          } else if (axis.position === 'left') {
            gridRect.x += labelUnionRect.width + margin;
          }
        }
      }
    });
    adjustAxes();
  }

  function adjustAxes() {
    each(axesList, function (axis) {
      var isHorizontal = axis.isHorizontal();
      var extent = isHorizontal ? [0, gridRect.width] : [0, gridRect.height];
      var idx = axis.inverse ? 1 : 0;
      axis.setExtent(extent[idx], extent[1 - idx]);
      updateAxisTransform(axis, isHorizontal ? gridRect.x : gridRect.y);
    });
  }
};
/**
 * @param {string} axisType
 * @param {number} [axisIndex]
 */


gridProto.getAxis = function (axisType, axisIndex) {
  var axesMapOnDim = this._axesMap[axisType];

  if (axesMapOnDim != null) {
    if (axisIndex == null) {
      // Find first axis
      for (var name in axesMapOnDim) {
        if (axesMapOnDim.hasOwnProperty(name)) {
          return axesMapOnDim[name];
        }
      }
    }

    return axesMapOnDim[axisIndex];
  }
};
/**
 * @return {Array.<module:echarts/coord/Axis>}
 */


gridProto.getAxes = function () {
  return this._axesList.slice();
};
/**
 * Usage:
 *      grid.getCartesian(xAxisIndex, yAxisIndex);
 *      grid.getCartesian(xAxisIndex);
 *      grid.getCartesian(null, yAxisIndex);
 *      grid.getCartesian({xAxisIndex: ..., yAxisIndex: ...});
 *
 * @param {number|Object} [xAxisIndex]
 * @param {number} [yAxisIndex]
 */


gridProto.getCartesian = function (xAxisIndex, yAxisIndex) {
  if (xAxisIndex != null && yAxisIndex != null) {
    var key = 'x' + xAxisIndex + 'y' + yAxisIndex;
    return this._coordsMap[key];
  }

  if (isObject(xAxisIndex)) {
    yAxisIndex = xAxisIndex.yAxisIndex;
    xAxisIndex = xAxisIndex.xAxisIndex;
  } // When only xAxisIndex or yAxisIndex given, find its first cartesian.


  for (var i = 0, coordList = this._coordsList; i < coordList.length; i++) {
    if (coordList[i].getAxis('x').index === xAxisIndex || coordList[i].getAxis('y').index === yAxisIndex) {
      return coordList[i];
    }
  }
};

gridProto.getCartesians = function () {
  return this._coordsList.slice();
};
/**
 * @implements
 * see {module:echarts/CoodinateSystem}
 */


gridProto.convertToPixel = function (ecModel, finder, value) {
  var target = this._findConvertTarget(ecModel, finder);

  return target.cartesian ? target.cartesian.dataToPoint(value) : target.axis ? target.axis.toGlobalCoord(target.axis.dataToCoord(value)) : null;
};
/**
 * @implements
 * see {module:echarts/CoodinateSystem}
 */


gridProto.convertFromPixel = function (ecModel, finder, value) {
  var target = this._findConvertTarget(ecModel, finder);

  return target.cartesian ? target.cartesian.pointToData(value) : target.axis ? target.axis.coordToData(target.axis.toLocalCoord(value)) : null;
};
/**
 * @inner
 */


gridProto._findConvertTarget = function (ecModel, finder) {
  var seriesModel = finder.seriesModel;
  var xAxisModel = finder.xAxisModel || seriesModel && seriesModel.getReferringComponents('xAxis')[0];
  var yAxisModel = finder.yAxisModel || seriesModel && seriesModel.getReferringComponents('yAxis')[0];
  var gridModel = finder.gridModel;
  var coordsList = this._coordsList;
  var cartesian;
  var axis;

  if (seriesModel) {
    cartesian = seriesModel.coordinateSystem;
    indexOf(coordsList, cartesian) < 0 && (cartesian = null);
  } else if (xAxisModel && yAxisModel) {
    cartesian = this.getCartesian(xAxisModel.componentIndex, yAxisModel.componentIndex);
  } else if (xAxisModel) {
    axis = this.getAxis('x', xAxisModel.componentIndex);
  } else if (yAxisModel) {
    axis = this.getAxis('y', yAxisModel.componentIndex);
  } // Lowest priority.
  else if (gridModel) {
      var grid = gridModel.coordinateSystem;

      if (grid === this) {
        cartesian = this._coordsList[0];
      }
    }

  return {
    cartesian: cartesian,
    axis: axis
  };
};
/**
 * @implements
 * see {module:echarts/CoodinateSystem}
 */


gridProto.containPoint = function (point) {
  var coord = this._coordsList[0];

  if (coord) {
    return coord.containPoint(point);
  }
};
/**
 * Initialize cartesian coordinate systems
 * @private
 */


gridProto._initCartesian = function (gridModel, ecModel, api) {
  var axisPositionUsed = {
    left: false,
    right: false,
    top: false,
    bottom: false
  };
  var axesMap = {
    x: {},
    y: {}
  };
  var axesCount = {
    x: 0,
    y: 0
  }; /// Create axis

  ecModel.eachComponent('xAxis', createAxisCreator('x'), this);
  ecModel.eachComponent('yAxis', createAxisCreator('y'), this);

  if (!axesCount.x || !axesCount.y) {
    // Roll back when there no either x or y axis
    this._axesMap = {};
    this._axesList = [];
    return;
  }

  this._axesMap = axesMap; /// Create cartesian2d

  each(axesMap.x, function (xAxis, xAxisIndex) {
    each(axesMap.y, function (yAxis, yAxisIndex) {
      var key = 'x' + xAxisIndex + 'y' + yAxisIndex;
      var cartesian = new Cartesian2D(key);
      cartesian.grid = this;
      cartesian.model = gridModel;
      this._coordsMap[key] = cartesian;

      this._coordsList.push(cartesian);

      cartesian.addAxis(xAxis);
      cartesian.addAxis(yAxis);
    }, this);
  }, this);

  function createAxisCreator(axisType) {
    return function (axisModel, idx) {
      if (!isAxisUsedInTheGrid(axisModel, gridModel, ecModel)) {
        return;
      }

      var axisPosition = axisModel.get('position');

      if (axisType === 'x') {
        // Fix position
        if (axisPosition !== 'top' && axisPosition !== 'bottom') {
          // Default bottom of X
          axisPosition = 'bottom';

          if (axisPositionUsed[axisPosition]) {
            axisPosition = axisPosition === 'top' ? 'bottom' : 'top';
          }
        }
      } else {
        // Fix position
        if (axisPosition !== 'left' && axisPosition !== 'right') {
          // Default left of Y
          axisPosition = 'left';

          if (axisPositionUsed[axisPosition]) {
            axisPosition = axisPosition === 'left' ? 'right' : 'left';
          }
        }
      }

      axisPositionUsed[axisPosition] = true;
      var axis = new Axis2D(axisType, createScaleByModel(axisModel), [0, 0], axisModel.get('type'), axisPosition);
      var isCategory = axis.type === 'category';
      axis.onBand = isCategory && axisModel.get('boundaryGap');
      axis.inverse = axisModel.get('inverse'); // Inject axis into axisModel

      axisModel.axis = axis; // Inject axisModel into axis

      axis.model = axisModel; // Inject grid info axis

      axis.grid = this; // Index of axis, can be used as key

      axis.index = idx;

      this._axesList.push(axis);

      axesMap[axisType][idx] = axis;
      axesCount[axisType]++;
    };
  }
};
/**
 * Update cartesian properties from series
 * @param  {module:echarts/model/Option} option
 * @private
 */


gridProto._updateScale = function (ecModel, gridModel) {
  // Reset scale
  each(this._axesList, function (axis) {
    axis.scale.setExtent(Infinity, -Infinity);
  });
  ecModel.eachSeries(function (seriesModel) {
    if (isCartesian2D(seriesModel)) {
      var axesModels = findAxesModels(seriesModel, ecModel);
      var xAxisModel = axesModels[0];
      var yAxisModel = axesModels[1];

      if (!isAxisUsedInTheGrid(xAxisModel, gridModel, ecModel) || !isAxisUsedInTheGrid(yAxisModel, gridModel, ecModel)) {
        return;
      }

      var cartesian = this.getCartesian(xAxisModel.componentIndex, yAxisModel.componentIndex);
      var data = seriesModel.getData();
      var xAxis = cartesian.getAxis('x');
      var yAxis = cartesian.getAxis('y');

      if (data.type === 'list') {
        unionExtent(data, xAxis, seriesModel);
        unionExtent(data, yAxis, seriesModel);
      }
    }
  }, this);

  function unionExtent(data, axis, seriesModel) {
    each(data.mapDimension(axis.dim, true), function (dim) {
      axis.scale.unionExtentFromData( // For example, the extent of the orginal dimension
      // is [0.1, 0.5], the extent of the `stackResultDimension`
      // is [7, 9], the final extent should not include [0.1, 0.5].
      data, getStackedDimension(data, dim));
    });
  }
};
/**
 * @param {string} [dim] 'x' or 'y' or 'auto' or null/undefined
 * @return {Object} {baseAxes: [], otherAxes: []}
 */


gridProto.getTooltipAxes = function (dim) {
  var baseAxes = [];
  var otherAxes = [];
  each(this.getCartesians(), function (cartesian) {
    var baseAxis = dim != null && dim !== 'auto' ? cartesian.getAxis(dim) : cartesian.getBaseAxis();
    var otherAxis = cartesian.getOtherAxis(baseAxis);
    indexOf(baseAxes, baseAxis) < 0 && baseAxes.push(baseAxis);
    indexOf(otherAxes, otherAxis) < 0 && otherAxes.push(otherAxis);
  });
  return {
    baseAxes: baseAxes,
    otherAxes: otherAxes
  };
};
/**
 * @inner
 */


function updateAxisTransform(axis, coordBase) {
  var axisExtent = axis.getExtent();
  var axisExtentSum = axisExtent[0] + axisExtent[1]; // Fast transform

  axis.toGlobalCoord = axis.dim === 'x' ? function (coord) {
    return coord + coordBase;
  } : function (coord) {
    return axisExtentSum - coord + coordBase;
  };
  axis.toLocalCoord = axis.dim === 'x' ? function (coord) {
    return coord - coordBase;
  } : function (coord) {
    return axisExtentSum - coord + coordBase;
  };
}

var axesTypes = ['xAxis', 'yAxis'];
/**
 * @inner
 */

function findAxesModels(seriesModel, ecModel) {
  return map(axesTypes, function (axisType) {
    var axisModel = seriesModel.getReferringComponents(axisType)[0];
    return axisModel;
  });
}
/**
 * @inner
 */


function isCartesian2D(seriesModel) {
  return seriesModel.get('coordinateSystem') === 'cartesian2d';
}

Grid.create = function (ecModel, api) {
  var grids = [];
  ecModel.eachComponent('grid', function (gridModel, idx) {
    var grid = new Grid(gridModel, ecModel, api);
    grid.name = 'grid_' + idx; // dataSampling requires axis extent, so resize
    // should be performed in create stage.

    grid.resize(gridModel, api, true);
    gridModel.coordinateSystem = grid;
    grids.push(grid);
  }); // Inject the coordinateSystems into seriesModel

  ecModel.eachSeries(function (seriesModel) {
    if (!isCartesian2D(seriesModel)) {
      return;
    }

    var axesModels = findAxesModels(seriesModel, ecModel);
    var xAxisModel = axesModels[0];
    var yAxisModel = axesModels[1];
    var gridModel = xAxisModel.getCoordSysModel();
    var grid = gridModel.coordinateSystem;
    seriesModel.coordinateSystem = grid.getCartesian(xAxisModel.componentIndex, yAxisModel.componentIndex);
  });
  return grids;
}; // For deciding which dimensions to use when creating list data


Grid.dimensions = Grid.prototype.dimensions = Cartesian2D.prototype.dimensions;
CoordinateSystem.register('cartesian2d', Grid);
var _default = Grid;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL0dyaWQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb29yZC9jYXJ0ZXNpYW4vR3JpZC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vLi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGlzT2JqZWN0ID0gX3V0aWwuaXNPYmplY3Q7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgbWFwID0gX3V0aWwubWFwO1xudmFyIGluZGV4T2YgPSBfdXRpbC5pbmRleE9mO1xudmFyIHJldHJpZXZlID0gX3V0aWwucmV0cmlldmU7XG5cbnZhciBfbGF5b3V0ID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbGF5b3V0XCIpO1xuXG52YXIgZ2V0TGF5b3V0UmVjdCA9IF9sYXlvdXQuZ2V0TGF5b3V0UmVjdDtcblxudmFyIF9heGlzSGVscGVyID0gcmVxdWlyZShcIi4uLy4uL2Nvb3JkL2F4aXNIZWxwZXJcIik7XG5cbnZhciBjcmVhdGVTY2FsZUJ5TW9kZWwgPSBfYXhpc0hlbHBlci5jcmVhdGVTY2FsZUJ5TW9kZWw7XG52YXIgaWZBeGlzQ3Jvc3NaZXJvID0gX2F4aXNIZWxwZXIuaWZBeGlzQ3Jvc3NaZXJvO1xudmFyIG5pY2VTY2FsZUV4dGVudCA9IF9heGlzSGVscGVyLm5pY2VTY2FsZUV4dGVudDtcbnZhciBlc3RpbWF0ZUxhYmVsVW5pb25SZWN0ID0gX2F4aXNIZWxwZXIuZXN0aW1hdGVMYWJlbFVuaW9uUmVjdDtcblxudmFyIENhcnRlc2lhbjJEID0gcmVxdWlyZShcIi4vQ2FydGVzaWFuMkRcIik7XG5cbnZhciBBeGlzMkQgPSByZXF1aXJlKFwiLi9BeGlzMkRcIik7XG5cbnZhciBDb29yZGluYXRlU3lzdGVtID0gcmVxdWlyZShcIi4uLy4uL0Nvb3JkaW5hdGVTeXN0ZW1cIik7XG5cbnZhciBfZGF0YVN0YWNrSGVscGVyID0gcmVxdWlyZShcIi4uLy4uL2RhdGEvaGVscGVyL2RhdGFTdGFja0hlbHBlclwiKTtcblxudmFyIGdldFN0YWNrZWREaW1lbnNpb24gPSBfZGF0YVN0YWNrSGVscGVyLmdldFN0YWNrZWREaW1lbnNpb247XG5cbnJlcXVpcmUoXCIuL0dyaWRNb2RlbFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEdyaWQgaXMgYSByZWdpb24gd2hpY2ggY29udGFpbnMgYXQgbW9zdCA0IGNhcnRlc2lhbiBzeXN0ZW1zXG4gKlxuICogVE9ETyBEZWZhdWx0IGNhcnRlc2lhblxuICovXG4vLyBEZXBlbmRzIG9uIEdyaWRNb2RlbCwgQXhpc01vZGVsLCB3aGljaCBwZXJmb3JtcyBwcmVwcm9jZXNzLlxuXG4vKipcbiAqIENoZWNrIGlmIHRoZSBheGlzIGlzIHVzZWQgaW4gdGhlIHNwZWNpZmllZCBncmlkXG4gKiBAaW5uZXJcbiAqL1xuZnVuY3Rpb24gaXNBeGlzVXNlZEluVGhlR3JpZChheGlzTW9kZWwsIGdyaWRNb2RlbCwgZWNNb2RlbCkge1xuICByZXR1cm4gYXhpc01vZGVsLmdldENvb3JkU3lzTW9kZWwoKSA9PT0gZ3JpZE1vZGVsO1xufVxuXG5mdW5jdGlvbiBHcmlkKGdyaWRNb2RlbCwgZWNNb2RlbCwgYXBpKSB7XG4gIC8qKlxuICAgKiBAdHlwZSB7T2JqZWN0LjxzdHJpbmcsIG1vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9DYXJ0ZXNpYW4yRD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuICB0aGlzLl9jb29yZHNNYXAgPSB7fTtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0NhcnRlc2lhbj59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2Nvb3Jkc0xpc3QgPSBbXTtcbiAgLyoqXG4gICAqIEB0eXBlIHtPYmplY3QuPHN0cmluZywgbW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0F4aXMyRD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2F4ZXNNYXAgPSB7fTtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOmVjaGFydHMvY29vcmQvY2FydGVzaWFuL0F4aXMyRD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2F4ZXNMaXN0ID0gW107XG5cbiAgdGhpcy5faW5pdENhcnRlc2lhbihncmlkTW9kZWwsIGVjTW9kZWwsIGFwaSk7XG5cbiAgdGhpcy5tb2RlbCA9IGdyaWRNb2RlbDtcbn1cblxudmFyIGdyaWRQcm90byA9IEdyaWQucHJvdG90eXBlO1xuZ3JpZFByb3RvLnR5cGUgPSAnZ3JpZCc7XG5ncmlkUHJvdG8uYXhpc1BvaW50ZXJFbmFibGVkID0gdHJ1ZTtcblxuZ3JpZFByb3RvLmdldFJlY3QgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl9yZWN0O1xufTtcblxuZ3JpZFByb3RvLnVwZGF0ZSA9IGZ1bmN0aW9uIChlY01vZGVsLCBhcGkpIHtcbiAgdmFyIGF4ZXNNYXAgPSB0aGlzLl9heGVzTWFwO1xuXG4gIHRoaXMuX3VwZGF0ZVNjYWxlKGVjTW9kZWwsIHRoaXMubW9kZWwpO1xuXG4gIGVhY2goYXhlc01hcC54LCBmdW5jdGlvbiAoeEF4aXMpIHtcbiAgICBuaWNlU2NhbGVFeHRlbnQoeEF4aXMuc2NhbGUsIHhBeGlzLm1vZGVsKTtcbiAgfSk7XG4gIGVhY2goYXhlc01hcC55LCBmdW5jdGlvbiAoeUF4aXMpIHtcbiAgICBuaWNlU2NhbGVFeHRlbnQoeUF4aXMuc2NhbGUsIHlBeGlzLm1vZGVsKTtcbiAgfSk7IC8vIEtleTogYXhpc0RpbV9heGlzSW5kZXgsIHZhbHVlOiBib29sZWFuLCB3aGV0aGVyIG9uWmVybyB0YXJnZXQuXG5cbiAgdmFyIG9uWmVyb1JlY29yZHMgPSB7fTtcbiAgZWFjaChheGVzTWFwLngsIGZ1bmN0aW9uICh4QXhpcykge1xuICAgIGZpeEF4aXNPblplcm8oYXhlc01hcCwgJ3knLCB4QXhpcywgb25aZXJvUmVjb3Jkcyk7XG4gIH0pO1xuICBlYWNoKGF4ZXNNYXAueSwgZnVuY3Rpb24gKHlBeGlzKSB7XG4gICAgZml4QXhpc09uWmVybyhheGVzTWFwLCAneCcsIHlBeGlzLCBvblplcm9SZWNvcmRzKTtcbiAgfSk7IC8vIFJlc2l6ZSBhZ2FpbiBpZiBjb250YWluTGFiZWwgaXMgZW5hYmxlZFxuICAvLyBGSVhNRSBJdCBtYXkgY2F1c2UgZ2V0dGluZyB3cm9uZyBncmlkIHNpemUgaW4gZGF0YSBwcm9jZXNzaW5nIHN0YWdlXG5cbiAgdGhpcy5yZXNpemUodGhpcy5tb2RlbCwgYXBpKTtcbn07XG5cbmZ1bmN0aW9uIGZpeEF4aXNPblplcm8oYXhlc01hcCwgb3RoZXJBeGlzRGltLCBheGlzLCBvblplcm9SZWNvcmRzKSB7XG4gIGF4aXMuZ2V0QXhlc09uWmVyb09mID0gZnVuY3Rpb24gKCkge1xuICAgIC8vIFRPRE86IG9uWmVybyBvZiBtdWx0aXBsZSBheGVzLlxuICAgIHJldHVybiBvdGhlckF4aXNPblplcm9PZiA/IFtvdGhlckF4aXNPblplcm9PZl0gOiBbXTtcbiAgfTsgLy8gb25aZXJvIGNhbiBub3QgYmUgZW5hYmxlZCBpbiB0aGVzZSB0d28gc2l0dWF0aW9uczpcbiAgLy8gMS4gV2hlbiBhbnkgb3RoZXIgYXhpcyBpcyBhIGNhdGVnb3J5IGF4aXMuXG4gIC8vIDIuIFdoZW4gbm8gYXhpcyBpcyBjcm9zcyAwIHBvaW50LlxuXG5cbiAgdmFyIG90aGVyQXhlcyA9IGF4ZXNNYXBbb3RoZXJBeGlzRGltXTtcbiAgdmFyIG90aGVyQXhpc09uWmVyb09mO1xuICB2YXIgYXhpc01vZGVsID0gYXhpcy5tb2RlbDtcbiAgdmFyIG9uWmVybyA9IGF4aXNNb2RlbC5nZXQoJ2F4aXNMaW5lLm9uWmVybycpO1xuICB2YXIgb25aZXJvQXhpc0luZGV4ID0gYXhpc01vZGVsLmdldCgnYXhpc0xpbmUub25aZXJvQXhpc0luZGV4Jyk7XG5cbiAgaWYgKCFvblplcm8pIHtcbiAgICByZXR1cm47XG4gIH0gLy8gSWYgdGFyZ2V0IGF4aXMgaXMgc3BlY2lmaWVkLlxuXG5cbiAgaWYgKG9uWmVyb0F4aXNJbmRleCAhPSBudWxsKSB7XG4gICAgaWYgKGNhbk9uWmVyb1RvQXhpcyhvdGhlckF4ZXNbb25aZXJvQXhpc0luZGV4XSkpIHtcbiAgICAgIG90aGVyQXhpc09uWmVyb09mID0gb3RoZXJBeGVzW29uWmVyb0F4aXNJbmRleF07XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIC8vIEZpbmQgdGhlIGZpcnN0IGF2YWlsYWJsZSBvdGhlciBheGlzLlxuICAgIGZvciAodmFyIGlkeCBpbiBvdGhlckF4ZXMpIHtcbiAgICAgIGlmIChvdGhlckF4ZXMuaGFzT3duUHJvcGVydHkoaWR4KSAmJiBjYW5Pblplcm9Ub0F4aXMob3RoZXJBeGVzW2lkeF0pIC8vIENvbnNpZGVyIHRoYXQgdHdvIFkgYXhlcyBvbiBvbmUgdmFsdWUgYXhpcyxcbiAgICAgIC8vIGlmIGJvdGggb25aZXJvLCB0aGUgdHdvIFkgYXhlcyBvdmVybGFwLlxuICAgICAgJiYgIW9uWmVyb1JlY29yZHNbZ2V0T25aZXJvUmVjb3JkS2V5KG90aGVyQXhlc1tpZHhdKV0pIHtcbiAgICAgICAgb3RoZXJBeGlzT25aZXJvT2YgPSBvdGhlckF4ZXNbaWR4XTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKG90aGVyQXhpc09uWmVyb09mKSB7XG4gICAgb25aZXJvUmVjb3Jkc1tnZXRPblplcm9SZWNvcmRLZXkob3RoZXJBeGlzT25aZXJvT2YpXSA9IHRydWU7XG4gIH1cblxuICBmdW5jdGlvbiBnZXRPblplcm9SZWNvcmRLZXkoYXhpcykge1xuICAgIHJldHVybiBheGlzLmRpbSArICdfJyArIGF4aXMuaW5kZXg7XG4gIH1cbn1cblxuZnVuY3Rpb24gY2FuT25aZXJvVG9BeGlzKGF4aXMpIHtcbiAgcmV0dXJuIGF4aXMgJiYgYXhpcy50eXBlICE9PSAnY2F0ZWdvcnknICYmIGF4aXMudHlwZSAhPT0gJ3RpbWUnICYmIGlmQXhpc0Nyb3NzWmVybyhheGlzKTtcbn1cbi8qKlxuICogUmVzaXplIHRoZSBncmlkXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9HcmlkTW9kZWx9IGdyaWRNb2RlbFxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICovXG5cblxuZ3JpZFByb3RvLnJlc2l6ZSA9IGZ1bmN0aW9uIChncmlkTW9kZWwsIGFwaSwgaWdub3JlQ29udGFpbkxhYmVsKSB7XG4gIHZhciBncmlkUmVjdCA9IGdldExheW91dFJlY3QoZ3JpZE1vZGVsLmdldEJveExheW91dFBhcmFtcygpLCB7XG4gICAgd2lkdGg6IGFwaS5nZXRXaWR0aCgpLFxuICAgIGhlaWdodDogYXBpLmdldEhlaWdodCgpXG4gIH0pO1xuICB0aGlzLl9yZWN0ID0gZ3JpZFJlY3Q7XG4gIHZhciBheGVzTGlzdCA9IHRoaXMuX2F4ZXNMaXN0O1xuICBhZGp1c3RBeGVzKCk7IC8vIE1pbnVzIGxhYmVsIHNpemVcblxuICBpZiAoIWlnbm9yZUNvbnRhaW5MYWJlbCAmJiBncmlkTW9kZWwuZ2V0KCdjb250YWluTGFiZWwnKSkge1xuICAgIGVhY2goYXhlc0xpc3QsIGZ1bmN0aW9uIChheGlzKSB7XG4gICAgICBpZiAoIWF4aXMubW9kZWwuZ2V0KCdheGlzTGFiZWwuaW5zaWRlJykpIHtcbiAgICAgICAgdmFyIGxhYmVsVW5pb25SZWN0ID0gZXN0aW1hdGVMYWJlbFVuaW9uUmVjdChheGlzKTtcblxuICAgICAgICBpZiAobGFiZWxVbmlvblJlY3QpIHtcbiAgICAgICAgICB2YXIgZGltID0gYXhpcy5pc0hvcml6b250YWwoKSA/ICdoZWlnaHQnIDogJ3dpZHRoJztcbiAgICAgICAgICB2YXIgbWFyZ2luID0gYXhpcy5tb2RlbC5nZXQoJ2F4aXNMYWJlbC5tYXJnaW4nKTtcbiAgICAgICAgICBncmlkUmVjdFtkaW1dIC09IGxhYmVsVW5pb25SZWN0W2RpbV0gKyBtYXJnaW47XG5cbiAgICAgICAgICBpZiAoYXhpcy5wb3NpdGlvbiA9PT0gJ3RvcCcpIHtcbiAgICAgICAgICAgIGdyaWRSZWN0LnkgKz0gbGFiZWxVbmlvblJlY3QuaGVpZ2h0ICsgbWFyZ2luO1xuICAgICAgICAgIH0gZWxzZSBpZiAoYXhpcy5wb3NpdGlvbiA9PT0gJ2xlZnQnKSB7XG4gICAgICAgICAgICBncmlkUmVjdC54ICs9IGxhYmVsVW5pb25SZWN0LndpZHRoICsgbWFyZ2luO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIGFkanVzdEF4ZXMoKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGFkanVzdEF4ZXMoKSB7XG4gICAgZWFjaChheGVzTGlzdCwgZnVuY3Rpb24gKGF4aXMpIHtcbiAgICAgIHZhciBpc0hvcml6b250YWwgPSBheGlzLmlzSG9yaXpvbnRhbCgpO1xuICAgICAgdmFyIGV4dGVudCA9IGlzSG9yaXpvbnRhbCA/IFswLCBncmlkUmVjdC53aWR0aF0gOiBbMCwgZ3JpZFJlY3QuaGVpZ2h0XTtcbiAgICAgIHZhciBpZHggPSBheGlzLmludmVyc2UgPyAxIDogMDtcbiAgICAgIGF4aXMuc2V0RXh0ZW50KGV4dGVudFtpZHhdLCBleHRlbnRbMSAtIGlkeF0pO1xuICAgICAgdXBkYXRlQXhpc1RyYW5zZm9ybShheGlzLCBpc0hvcml6b250YWwgPyBncmlkUmVjdC54IDogZ3JpZFJlY3QueSk7XG4gICAgfSk7XG4gIH1cbn07XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSBheGlzVHlwZVxuICogQHBhcmFtIHtudW1iZXJ9IFtheGlzSW5kZXhdXG4gKi9cblxuXG5ncmlkUHJvdG8uZ2V0QXhpcyA9IGZ1bmN0aW9uIChheGlzVHlwZSwgYXhpc0luZGV4KSB7XG4gIHZhciBheGVzTWFwT25EaW0gPSB0aGlzLl9heGVzTWFwW2F4aXNUeXBlXTtcblxuICBpZiAoYXhlc01hcE9uRGltICE9IG51bGwpIHtcbiAgICBpZiAoYXhpc0luZGV4ID09IG51bGwpIHtcbiAgICAgIC8vIEZpbmQgZmlyc3QgYXhpc1xuICAgICAgZm9yICh2YXIgbmFtZSBpbiBheGVzTWFwT25EaW0pIHtcbiAgICAgICAgaWYgKGF4ZXNNYXBPbkRpbS5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgIHJldHVybiBheGVzTWFwT25EaW1bbmFtZV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYXhlc01hcE9uRGltW2F4aXNJbmRleF07XG4gIH1cbn07XG4vKipcbiAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6ZWNoYXJ0cy9jb29yZC9BeGlzPn1cbiAqL1xuXG5cbmdyaWRQcm90by5nZXRBeGVzID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fYXhlc0xpc3Quc2xpY2UoKTtcbn07XG4vKipcbiAqIFVzYWdlOlxuICogICAgICBncmlkLmdldENhcnRlc2lhbih4QXhpc0luZGV4LCB5QXhpc0luZGV4KTtcbiAqICAgICAgZ3JpZC5nZXRDYXJ0ZXNpYW4oeEF4aXNJbmRleCk7XG4gKiAgICAgIGdyaWQuZ2V0Q2FydGVzaWFuKG51bGwsIHlBeGlzSW5kZXgpO1xuICogICAgICBncmlkLmdldENhcnRlc2lhbih7eEF4aXNJbmRleDogLi4uLCB5QXhpc0luZGV4OiAuLi59KTtcbiAqXG4gKiBAcGFyYW0ge251bWJlcnxPYmplY3R9IFt4QXhpc0luZGV4XVxuICogQHBhcmFtIHtudW1iZXJ9IFt5QXhpc0luZGV4XVxuICovXG5cblxuZ3JpZFByb3RvLmdldENhcnRlc2lhbiA9IGZ1bmN0aW9uICh4QXhpc0luZGV4LCB5QXhpc0luZGV4KSB7XG4gIGlmICh4QXhpc0luZGV4ICE9IG51bGwgJiYgeUF4aXNJbmRleCAhPSBudWxsKSB7XG4gICAgdmFyIGtleSA9ICd4JyArIHhBeGlzSW5kZXggKyAneScgKyB5QXhpc0luZGV4O1xuICAgIHJldHVybiB0aGlzLl9jb29yZHNNYXBba2V5XTtcbiAgfVxuXG4gIGlmIChpc09iamVjdCh4QXhpc0luZGV4KSkge1xuICAgIHlBeGlzSW5kZXggPSB4QXhpc0luZGV4LnlBeGlzSW5kZXg7XG4gICAgeEF4aXNJbmRleCA9IHhBeGlzSW5kZXgueEF4aXNJbmRleDtcbiAgfSAvLyBXaGVuIG9ubHkgeEF4aXNJbmRleCBvciB5QXhpc0luZGV4IGdpdmVuLCBmaW5kIGl0cyBmaXJzdCBjYXJ0ZXNpYW4uXG5cblxuICBmb3IgKHZhciBpID0gMCwgY29vcmRMaXN0ID0gdGhpcy5fY29vcmRzTGlzdDsgaSA8IGNvb3JkTGlzdC5sZW5ndGg7IGkrKykge1xuICAgIGlmIChjb29yZExpc3RbaV0uZ2V0QXhpcygneCcpLmluZGV4ID09PSB4QXhpc0luZGV4IHx8IGNvb3JkTGlzdFtpXS5nZXRBeGlzKCd5JykuaW5kZXggPT09IHlBeGlzSW5kZXgpIHtcbiAgICAgIHJldHVybiBjb29yZExpc3RbaV07XG4gICAgfVxuICB9XG59O1xuXG5ncmlkUHJvdG8uZ2V0Q2FydGVzaWFucyA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX2Nvb3Jkc0xpc3Quc2xpY2UoKTtcbn07XG4vKipcbiAqIEBpbXBsZW1lbnRzXG4gKiBzZWUge21vZHVsZTplY2hhcnRzL0Nvb2RpbmF0ZVN5c3RlbX1cbiAqL1xuXG5cbmdyaWRQcm90by5jb252ZXJ0VG9QaXhlbCA9IGZ1bmN0aW9uIChlY01vZGVsLCBmaW5kZXIsIHZhbHVlKSB7XG4gIHZhciB0YXJnZXQgPSB0aGlzLl9maW5kQ29udmVydFRhcmdldChlY01vZGVsLCBmaW5kZXIpO1xuXG4gIHJldHVybiB0YXJnZXQuY2FydGVzaWFuID8gdGFyZ2V0LmNhcnRlc2lhbi5kYXRhVG9Qb2ludCh2YWx1ZSkgOiB0YXJnZXQuYXhpcyA/IHRhcmdldC5heGlzLnRvR2xvYmFsQ29vcmQodGFyZ2V0LmF4aXMuZGF0YVRvQ29vcmQodmFsdWUpKSA6IG51bGw7XG59O1xuLyoqXG4gKiBAaW1wbGVtZW50c1xuICogc2VlIHttb2R1bGU6ZWNoYXJ0cy9Db29kaW5hdGVTeXN0ZW19XG4gKi9cblxuXG5ncmlkUHJvdG8uY29udmVydEZyb21QaXhlbCA9IGZ1bmN0aW9uIChlY01vZGVsLCBmaW5kZXIsIHZhbHVlKSB7XG4gIHZhciB0YXJnZXQgPSB0aGlzLl9maW5kQ29udmVydFRhcmdldChlY01vZGVsLCBmaW5kZXIpO1xuXG4gIHJldHVybiB0YXJnZXQuY2FydGVzaWFuID8gdGFyZ2V0LmNhcnRlc2lhbi5wb2ludFRvRGF0YSh2YWx1ZSkgOiB0YXJnZXQuYXhpcyA/IHRhcmdldC5heGlzLmNvb3JkVG9EYXRhKHRhcmdldC5heGlzLnRvTG9jYWxDb29yZCh2YWx1ZSkpIDogbnVsbDtcbn07XG4vKipcbiAqIEBpbm5lclxuICovXG5cblxuZ3JpZFByb3RvLl9maW5kQ29udmVydFRhcmdldCA9IGZ1bmN0aW9uIChlY01vZGVsLCBmaW5kZXIpIHtcbiAgdmFyIHNlcmllc01vZGVsID0gZmluZGVyLnNlcmllc01vZGVsO1xuICB2YXIgeEF4aXNNb2RlbCA9IGZpbmRlci54QXhpc01vZGVsIHx8IHNlcmllc01vZGVsICYmIHNlcmllc01vZGVsLmdldFJlZmVycmluZ0NvbXBvbmVudHMoJ3hBeGlzJylbMF07XG4gIHZhciB5QXhpc01vZGVsID0gZmluZGVyLnlBeGlzTW9kZWwgfHwgc2VyaWVzTW9kZWwgJiYgc2VyaWVzTW9kZWwuZ2V0UmVmZXJyaW5nQ29tcG9uZW50cygneUF4aXMnKVswXTtcbiAgdmFyIGdyaWRNb2RlbCA9IGZpbmRlci5ncmlkTW9kZWw7XG4gIHZhciBjb29yZHNMaXN0ID0gdGhpcy5fY29vcmRzTGlzdDtcbiAgdmFyIGNhcnRlc2lhbjtcbiAgdmFyIGF4aXM7XG5cbiAgaWYgKHNlcmllc01vZGVsKSB7XG4gICAgY2FydGVzaWFuID0gc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbTtcbiAgICBpbmRleE9mKGNvb3Jkc0xpc3QsIGNhcnRlc2lhbikgPCAwICYmIChjYXJ0ZXNpYW4gPSBudWxsKTtcbiAgfSBlbHNlIGlmICh4QXhpc01vZGVsICYmIHlBeGlzTW9kZWwpIHtcbiAgICBjYXJ0ZXNpYW4gPSB0aGlzLmdldENhcnRlc2lhbih4QXhpc01vZGVsLmNvbXBvbmVudEluZGV4LCB5QXhpc01vZGVsLmNvbXBvbmVudEluZGV4KTtcbiAgfSBlbHNlIGlmICh4QXhpc01vZGVsKSB7XG4gICAgYXhpcyA9IHRoaXMuZ2V0QXhpcygneCcsIHhBeGlzTW9kZWwuY29tcG9uZW50SW5kZXgpO1xuICB9IGVsc2UgaWYgKHlBeGlzTW9kZWwpIHtcbiAgICBheGlzID0gdGhpcy5nZXRBeGlzKCd5JywgeUF4aXNNb2RlbC5jb21wb25lbnRJbmRleCk7XG4gIH0gLy8gTG93ZXN0IHByaW9yaXR5LlxuICBlbHNlIGlmIChncmlkTW9kZWwpIHtcbiAgICAgIHZhciBncmlkID0gZ3JpZE1vZGVsLmNvb3JkaW5hdGVTeXN0ZW07XG5cbiAgICAgIGlmIChncmlkID09PSB0aGlzKSB7XG4gICAgICAgIGNhcnRlc2lhbiA9IHRoaXMuX2Nvb3Jkc0xpc3RbMF07XG4gICAgICB9XG4gICAgfVxuXG4gIHJldHVybiB7XG4gICAgY2FydGVzaWFuOiBjYXJ0ZXNpYW4sXG4gICAgYXhpczogYXhpc1xuICB9O1xufTtcbi8qKlxuICogQGltcGxlbWVudHNcbiAqIHNlZSB7bW9kdWxlOmVjaGFydHMvQ29vZGluYXRlU3lzdGVtfVxuICovXG5cblxuZ3JpZFByb3RvLmNvbnRhaW5Qb2ludCA9IGZ1bmN0aW9uIChwb2ludCkge1xuICB2YXIgY29vcmQgPSB0aGlzLl9jb29yZHNMaXN0WzBdO1xuXG4gIGlmIChjb29yZCkge1xuICAgIHJldHVybiBjb29yZC5jb250YWluUG9pbnQocG9pbnQpO1xuICB9XG59O1xuLyoqXG4gKiBJbml0aWFsaXplIGNhcnRlc2lhbiBjb29yZGluYXRlIHN5c3RlbXNcbiAqIEBwcml2YXRlXG4gKi9cblxuXG5ncmlkUHJvdG8uX2luaXRDYXJ0ZXNpYW4gPSBmdW5jdGlvbiAoZ3JpZE1vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgdmFyIGF4aXNQb3NpdGlvblVzZWQgPSB7XG4gICAgbGVmdDogZmFsc2UsXG4gICAgcmlnaHQ6IGZhbHNlLFxuICAgIHRvcDogZmFsc2UsXG4gICAgYm90dG9tOiBmYWxzZVxuICB9O1xuICB2YXIgYXhlc01hcCA9IHtcbiAgICB4OiB7fSxcbiAgICB5OiB7fVxuICB9O1xuICB2YXIgYXhlc0NvdW50ID0ge1xuICAgIHg6IDAsXG4gICAgeTogMFxuICB9OyAvLy8gQ3JlYXRlIGF4aXNcblxuICBlY01vZGVsLmVhY2hDb21wb25lbnQoJ3hBeGlzJywgY3JlYXRlQXhpc0NyZWF0b3IoJ3gnKSwgdGhpcyk7XG4gIGVjTW9kZWwuZWFjaENvbXBvbmVudCgneUF4aXMnLCBjcmVhdGVBeGlzQ3JlYXRvcigneScpLCB0aGlzKTtcblxuICBpZiAoIWF4ZXNDb3VudC54IHx8ICFheGVzQ291bnQueSkge1xuICAgIC8vIFJvbGwgYmFjayB3aGVuIHRoZXJlIG5vIGVpdGhlciB4IG9yIHkgYXhpc1xuICAgIHRoaXMuX2F4ZXNNYXAgPSB7fTtcbiAgICB0aGlzLl9heGVzTGlzdCA9IFtdO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHRoaXMuX2F4ZXNNYXAgPSBheGVzTWFwOyAvLy8gQ3JlYXRlIGNhcnRlc2lhbjJkXG5cbiAgZWFjaChheGVzTWFwLngsIGZ1bmN0aW9uICh4QXhpcywgeEF4aXNJbmRleCkge1xuICAgIGVhY2goYXhlc01hcC55LCBmdW5jdGlvbiAoeUF4aXMsIHlBeGlzSW5kZXgpIHtcbiAgICAgIHZhciBrZXkgPSAneCcgKyB4QXhpc0luZGV4ICsgJ3knICsgeUF4aXNJbmRleDtcbiAgICAgIHZhciBjYXJ0ZXNpYW4gPSBuZXcgQ2FydGVzaWFuMkQoa2V5KTtcbiAgICAgIGNhcnRlc2lhbi5ncmlkID0gdGhpcztcbiAgICAgIGNhcnRlc2lhbi5tb2RlbCA9IGdyaWRNb2RlbDtcbiAgICAgIHRoaXMuX2Nvb3Jkc01hcFtrZXldID0gY2FydGVzaWFuO1xuXG4gICAgICB0aGlzLl9jb29yZHNMaXN0LnB1c2goY2FydGVzaWFuKTtcblxuICAgICAgY2FydGVzaWFuLmFkZEF4aXMoeEF4aXMpO1xuICAgICAgY2FydGVzaWFuLmFkZEF4aXMoeUF4aXMpO1xuICAgIH0sIHRoaXMpO1xuICB9LCB0aGlzKTtcblxuICBmdW5jdGlvbiBjcmVhdGVBeGlzQ3JlYXRvcihheGlzVHlwZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoYXhpc01vZGVsLCBpZHgpIHtcbiAgICAgIGlmICghaXNBeGlzVXNlZEluVGhlR3JpZChheGlzTW9kZWwsIGdyaWRNb2RlbCwgZWNNb2RlbCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgYXhpc1Bvc2l0aW9uID0gYXhpc01vZGVsLmdldCgncG9zaXRpb24nKTtcblxuICAgICAgaWYgKGF4aXNUeXBlID09PSAneCcpIHtcbiAgICAgICAgLy8gRml4IHBvc2l0aW9uXG4gICAgICAgIGlmIChheGlzUG9zaXRpb24gIT09ICd0b3AnICYmIGF4aXNQb3NpdGlvbiAhPT0gJ2JvdHRvbScpIHtcbiAgICAgICAgICAvLyBEZWZhdWx0IGJvdHRvbSBvZiBYXG4gICAgICAgICAgYXhpc1Bvc2l0aW9uID0gJ2JvdHRvbSc7XG5cbiAgICAgICAgICBpZiAoYXhpc1Bvc2l0aW9uVXNlZFtheGlzUG9zaXRpb25dKSB7XG4gICAgICAgICAgICBheGlzUG9zaXRpb24gPSBheGlzUG9zaXRpb24gPT09ICd0b3AnID8gJ2JvdHRvbScgOiAndG9wJztcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEZpeCBwb3NpdGlvblxuICAgICAgICBpZiAoYXhpc1Bvc2l0aW9uICE9PSAnbGVmdCcgJiYgYXhpc1Bvc2l0aW9uICE9PSAncmlnaHQnKSB7XG4gICAgICAgICAgLy8gRGVmYXVsdCBsZWZ0IG9mIFlcbiAgICAgICAgICBheGlzUG9zaXRpb24gPSAnbGVmdCc7XG5cbiAgICAgICAgICBpZiAoYXhpc1Bvc2l0aW9uVXNlZFtheGlzUG9zaXRpb25dKSB7XG4gICAgICAgICAgICBheGlzUG9zaXRpb24gPSBheGlzUG9zaXRpb24gPT09ICdsZWZ0JyA/ICdyaWdodCcgOiAnbGVmdCc7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGF4aXNQb3NpdGlvblVzZWRbYXhpc1Bvc2l0aW9uXSA9IHRydWU7XG4gICAgICB2YXIgYXhpcyA9IG5ldyBBeGlzMkQoYXhpc1R5cGUsIGNyZWF0ZVNjYWxlQnlNb2RlbChheGlzTW9kZWwpLCBbMCwgMF0sIGF4aXNNb2RlbC5nZXQoJ3R5cGUnKSwgYXhpc1Bvc2l0aW9uKTtcbiAgICAgIHZhciBpc0NhdGVnb3J5ID0gYXhpcy50eXBlID09PSAnY2F0ZWdvcnknO1xuICAgICAgYXhpcy5vbkJhbmQgPSBpc0NhdGVnb3J5ICYmIGF4aXNNb2RlbC5nZXQoJ2JvdW5kYXJ5R2FwJyk7XG4gICAgICBheGlzLmludmVyc2UgPSBheGlzTW9kZWwuZ2V0KCdpbnZlcnNlJyk7IC8vIEluamVjdCBheGlzIGludG8gYXhpc01vZGVsXG5cbiAgICAgIGF4aXNNb2RlbC5heGlzID0gYXhpczsgLy8gSW5qZWN0IGF4aXNNb2RlbCBpbnRvIGF4aXNcblxuICAgICAgYXhpcy5tb2RlbCA9IGF4aXNNb2RlbDsgLy8gSW5qZWN0IGdyaWQgaW5mbyBheGlzXG5cbiAgICAgIGF4aXMuZ3JpZCA9IHRoaXM7IC8vIEluZGV4IG9mIGF4aXMsIGNhbiBiZSB1c2VkIGFzIGtleVxuXG4gICAgICBheGlzLmluZGV4ID0gaWR4O1xuXG4gICAgICB0aGlzLl9heGVzTGlzdC5wdXNoKGF4aXMpO1xuXG4gICAgICBheGVzTWFwW2F4aXNUeXBlXVtpZHhdID0gYXhpcztcbiAgICAgIGF4ZXNDb3VudFtheGlzVHlwZV0rKztcbiAgICB9O1xuICB9XG59O1xuLyoqXG4gKiBVcGRhdGUgY2FydGVzaWFuIHByb3BlcnRpZXMgZnJvbSBzZXJpZXNcbiAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL09wdGlvbn0gb3B0aW9uXG4gKiBAcHJpdmF0ZVxuICovXG5cblxuZ3JpZFByb3RvLl91cGRhdGVTY2FsZSA9IGZ1bmN0aW9uIChlY01vZGVsLCBncmlkTW9kZWwpIHtcbiAgLy8gUmVzZXQgc2NhbGVcbiAgZWFjaCh0aGlzLl9heGVzTGlzdCwgZnVuY3Rpb24gKGF4aXMpIHtcbiAgICBheGlzLnNjYWxlLnNldEV4dGVudChJbmZpbml0eSwgLUluZmluaXR5KTtcbiAgfSk7XG4gIGVjTW9kZWwuZWFjaFNlcmllcyhmdW5jdGlvbiAoc2VyaWVzTW9kZWwpIHtcbiAgICBpZiAoaXNDYXJ0ZXNpYW4yRChzZXJpZXNNb2RlbCkpIHtcbiAgICAgIHZhciBheGVzTW9kZWxzID0gZmluZEF4ZXNNb2RlbHMoc2VyaWVzTW9kZWwsIGVjTW9kZWwpO1xuICAgICAgdmFyIHhBeGlzTW9kZWwgPSBheGVzTW9kZWxzWzBdO1xuICAgICAgdmFyIHlBeGlzTW9kZWwgPSBheGVzTW9kZWxzWzFdO1xuXG4gICAgICBpZiAoIWlzQXhpc1VzZWRJblRoZUdyaWQoeEF4aXNNb2RlbCwgZ3JpZE1vZGVsLCBlY01vZGVsKSB8fCAhaXNBeGlzVXNlZEluVGhlR3JpZCh5QXhpc01vZGVsLCBncmlkTW9kZWwsIGVjTW9kZWwpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGNhcnRlc2lhbiA9IHRoaXMuZ2V0Q2FydGVzaWFuKHhBeGlzTW9kZWwuY29tcG9uZW50SW5kZXgsIHlBeGlzTW9kZWwuY29tcG9uZW50SW5kZXgpO1xuICAgICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gICAgICB2YXIgeEF4aXMgPSBjYXJ0ZXNpYW4uZ2V0QXhpcygneCcpO1xuICAgICAgdmFyIHlBeGlzID0gY2FydGVzaWFuLmdldEF4aXMoJ3knKTtcblxuICAgICAgaWYgKGRhdGEudHlwZSA9PT0gJ2xpc3QnKSB7XG4gICAgICAgIHVuaW9uRXh0ZW50KGRhdGEsIHhBeGlzLCBzZXJpZXNNb2RlbCk7XG4gICAgICAgIHVuaW9uRXh0ZW50KGRhdGEsIHlBeGlzLCBzZXJpZXNNb2RlbCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB0aGlzKTtcblxuICBmdW5jdGlvbiB1bmlvbkV4dGVudChkYXRhLCBheGlzLCBzZXJpZXNNb2RlbCkge1xuICAgIGVhY2goZGF0YS5tYXBEaW1lbnNpb24oYXhpcy5kaW0sIHRydWUpLCBmdW5jdGlvbiAoZGltKSB7XG4gICAgICBheGlzLnNjYWxlLnVuaW9uRXh0ZW50RnJvbURhdGEoIC8vIEZvciBleGFtcGxlLCB0aGUgZXh0ZW50IG9mIHRoZSBvcmdpbmFsIGRpbWVuc2lvblxuICAgICAgLy8gaXMgWzAuMSwgMC41XSwgdGhlIGV4dGVudCBvZiB0aGUgYHN0YWNrUmVzdWx0RGltZW5zaW9uYFxuICAgICAgLy8gaXMgWzcsIDldLCB0aGUgZmluYWwgZXh0ZW50IHNob3VsZCBub3QgaW5jbHVkZSBbMC4xLCAwLjVdLlxuICAgICAgZGF0YSwgZ2V0U3RhY2tlZERpbWVuc2lvbihkYXRhLCBkaW0pKTtcbiAgICB9KTtcbiAgfVxufTtcbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IFtkaW1dICd4JyBvciAneScgb3IgJ2F1dG8nIG9yIG51bGwvdW5kZWZpbmVkXG4gKiBAcmV0dXJuIHtPYmplY3R9IHtiYXNlQXhlczogW10sIG90aGVyQXhlczogW119XG4gKi9cblxuXG5ncmlkUHJvdG8uZ2V0VG9vbHRpcEF4ZXMgPSBmdW5jdGlvbiAoZGltKSB7XG4gIHZhciBiYXNlQXhlcyA9IFtdO1xuICB2YXIgb3RoZXJBeGVzID0gW107XG4gIGVhY2godGhpcy5nZXRDYXJ0ZXNpYW5zKCksIGZ1bmN0aW9uIChjYXJ0ZXNpYW4pIHtcbiAgICB2YXIgYmFzZUF4aXMgPSBkaW0gIT0gbnVsbCAmJiBkaW0gIT09ICdhdXRvJyA/IGNhcnRlc2lhbi5nZXRBeGlzKGRpbSkgOiBjYXJ0ZXNpYW4uZ2V0QmFzZUF4aXMoKTtcbiAgICB2YXIgb3RoZXJBeGlzID0gY2FydGVzaWFuLmdldE90aGVyQXhpcyhiYXNlQXhpcyk7XG4gICAgaW5kZXhPZihiYXNlQXhlcywgYmFzZUF4aXMpIDwgMCAmJiBiYXNlQXhlcy5wdXNoKGJhc2VBeGlzKTtcbiAgICBpbmRleE9mKG90aGVyQXhlcywgb3RoZXJBeGlzKSA8IDAgJiYgb3RoZXJBeGVzLnB1c2gob3RoZXJBeGlzKTtcbiAgfSk7XG4gIHJldHVybiB7XG4gICAgYmFzZUF4ZXM6IGJhc2VBeGVzLFxuICAgIG90aGVyQXhlczogb3RoZXJBeGVzXG4gIH07XG59O1xuLyoqXG4gKiBAaW5uZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIHVwZGF0ZUF4aXNUcmFuc2Zvcm0oYXhpcywgY29vcmRCYXNlKSB7XG4gIHZhciBheGlzRXh0ZW50ID0gYXhpcy5nZXRFeHRlbnQoKTtcbiAgdmFyIGF4aXNFeHRlbnRTdW0gPSBheGlzRXh0ZW50WzBdICsgYXhpc0V4dGVudFsxXTsgLy8gRmFzdCB0cmFuc2Zvcm1cblxuICBheGlzLnRvR2xvYmFsQ29vcmQgPSBheGlzLmRpbSA9PT0gJ3gnID8gZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgcmV0dXJuIGNvb3JkICsgY29vcmRCYXNlO1xuICB9IDogZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgcmV0dXJuIGF4aXNFeHRlbnRTdW0gLSBjb29yZCArIGNvb3JkQmFzZTtcbiAgfTtcbiAgYXhpcy50b0xvY2FsQ29vcmQgPSBheGlzLmRpbSA9PT0gJ3gnID8gZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgcmV0dXJuIGNvb3JkIC0gY29vcmRCYXNlO1xuICB9IDogZnVuY3Rpb24gKGNvb3JkKSB7XG4gICAgcmV0dXJuIGF4aXNFeHRlbnRTdW0gLSBjb29yZCArIGNvb3JkQmFzZTtcbiAgfTtcbn1cblxudmFyIGF4ZXNUeXBlcyA9IFsneEF4aXMnLCAneUF4aXMnXTtcbi8qKlxuICogQGlubmVyXG4gKi9cblxuZnVuY3Rpb24gZmluZEF4ZXNNb2RlbHMoc2VyaWVzTW9kZWwsIGVjTW9kZWwpIHtcbiAgcmV0dXJuIG1hcChheGVzVHlwZXMsIGZ1bmN0aW9uIChheGlzVHlwZSkge1xuICAgIHZhciBheGlzTW9kZWwgPSBzZXJpZXNNb2RlbC5nZXRSZWZlcnJpbmdDb21wb25lbnRzKGF4aXNUeXBlKVswXTtcbiAgICByZXR1cm4gYXhpc01vZGVsO1xuICB9KTtcbn1cbi8qKlxuICogQGlubmVyXG4gKi9cblxuXG5mdW5jdGlvbiBpc0NhcnRlc2lhbjJEKHNlcmllc01vZGVsKSB7XG4gIHJldHVybiBzZXJpZXNNb2RlbC5nZXQoJ2Nvb3JkaW5hdGVTeXN0ZW0nKSA9PT0gJ2NhcnRlc2lhbjJkJztcbn1cblxuR3JpZC5jcmVhdGUgPSBmdW5jdGlvbiAoZWNNb2RlbCwgYXBpKSB7XG4gIHZhciBncmlkcyA9IFtdO1xuICBlY01vZGVsLmVhY2hDb21wb25lbnQoJ2dyaWQnLCBmdW5jdGlvbiAoZ3JpZE1vZGVsLCBpZHgpIHtcbiAgICB2YXIgZ3JpZCA9IG5ldyBHcmlkKGdyaWRNb2RlbCwgZWNNb2RlbCwgYXBpKTtcbiAgICBncmlkLm5hbWUgPSAnZ3JpZF8nICsgaWR4OyAvLyBkYXRhU2FtcGxpbmcgcmVxdWlyZXMgYXhpcyBleHRlbnQsIHNvIHJlc2l6ZVxuICAgIC8vIHNob3VsZCBiZSBwZXJmb3JtZWQgaW4gY3JlYXRlIHN0YWdlLlxuXG4gICAgZ3JpZC5yZXNpemUoZ3JpZE1vZGVsLCBhcGksIHRydWUpO1xuICAgIGdyaWRNb2RlbC5jb29yZGluYXRlU3lzdGVtID0gZ3JpZDtcbiAgICBncmlkcy5wdXNoKGdyaWQpO1xuICB9KTsgLy8gSW5qZWN0IHRoZSBjb29yZGluYXRlU3lzdGVtcyBpbnRvIHNlcmllc01vZGVsXG5cbiAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIGlmICghaXNDYXJ0ZXNpYW4yRChzZXJpZXNNb2RlbCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgYXhlc01vZGVscyA9IGZpbmRBeGVzTW9kZWxzKHNlcmllc01vZGVsLCBlY01vZGVsKTtcbiAgICB2YXIgeEF4aXNNb2RlbCA9IGF4ZXNNb2RlbHNbMF07XG4gICAgdmFyIHlBeGlzTW9kZWwgPSBheGVzTW9kZWxzWzFdO1xuICAgIHZhciBncmlkTW9kZWwgPSB4QXhpc01vZGVsLmdldENvb3JkU3lzTW9kZWwoKTtcbiAgICB2YXIgZ3JpZCA9IGdyaWRNb2RlbC5jb29yZGluYXRlU3lzdGVtO1xuICAgIHNlcmllc01vZGVsLmNvb3JkaW5hdGVTeXN0ZW0gPSBncmlkLmdldENhcnRlc2lhbih4QXhpc01vZGVsLmNvbXBvbmVudEluZGV4LCB5QXhpc01vZGVsLmNvbXBvbmVudEluZGV4KTtcbiAgfSk7XG4gIHJldHVybiBncmlkcztcbn07IC8vIEZvciBkZWNpZGluZyB3aGljaCBkaW1lbnNpb25zIHRvIHVzZSB3aGVuIGNyZWF0aW5nIGxpc3QgZGF0YVxuXG5cbkdyaWQuZGltZW5zaW9ucyA9IEdyaWQucHJvdG90eXBlLmRpbWVuc2lvbnMgPSBDYXJ0ZXNpYW4yRC5wcm90b3R5cGUuZGltZW5zaW9ucztcbkNvb3JkaW5hdGVTeXN0ZW0ucmVnaXN0ZXIoJ2NhcnRlc2lhbjJkJywgR3JpZCk7XG52YXIgX2RlZmF1bHQgPSBHcmlkO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/Grid.js
