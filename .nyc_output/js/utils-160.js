__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dataToArray", function() { return dataToArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transitionStr", function() { return transitionStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transitionEnd", function() { return transitionEnd; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addEventListener", function() { return addEventListener; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeEventListener", function() { return removeEventListener; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transformArguments", function() { return transformArguments; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumeric", function() { return isNumeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "windowIsUndefined", function() { return windowIsUndefined; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTouchParentScroll", function() { return getTouchParentScroll; });
function dataToArray(vars) {
  if (Array.isArray(vars)) {
    return vars;
  }

  return [vars];
}
var transitionEndObject = {
  transition: 'transitionend',
  WebkitTransition: 'webkitTransitionEnd',
  MozTransition: 'transitionend',
  OTransition: 'oTransitionEnd otransitionend'
};
var transitionStr = Object.keys(transitionEndObject).filter(function (key) {
  if (typeof document === 'undefined') {
    return false;
  }

  var html = document.getElementsByTagName('html')[0];
  return key in (html ? html.style : {});
})[0];
var transitionEnd = transitionEndObject[transitionStr];
function addEventListener(target, eventType, callback, options) {
  if (target.addEventListener) {
    target.addEventListener(eventType, callback, options);
  } else if (target.attachEvent) {
    // tslint:disable-line
    target.attachEvent("on".concat(eventType), callback); // tslint:disable-line
  }
}
function removeEventListener(target, eventType, callback, options) {
  if (target.removeEventListener) {
    target.removeEventListener(eventType, callback, options);
  } else if (target.attachEvent) {
    // tslint:disable-line
    target.detachEvent("on".concat(eventType), callback); // tslint:disable-line
  }
}
function transformArguments(arg, cb) {
  var result = typeof arg === 'function' ? arg(cb) : arg;

  if (Array.isArray(result)) {
    if (result.length === 2) {
      return result;
    }

    return [result[0], result[1]];
  }

  return [result];
}
var isNumeric = function isNumeric(value) {
  return !isNaN(parseFloat(value)) && isFinite(value);
};
var windowIsUndefined = !(typeof window !== 'undefined' && window.document && window.document.createElement);
var getTouchParentScroll = function getTouchParentScroll(root, currentTarget, differX, differY) {
  if (!currentTarget || currentTarget === document || currentTarget instanceof Document) {
    return false;
  } // root 为 drawer-content 设定了 overflow, 判断为 root 的 parent 时结束滚动；


  if (currentTarget === root.parentNode) {
    return true;
  }

  var isY = Math.max(Math.abs(differX), Math.abs(differY)) === Math.abs(differY);
  var isX = Math.max(Math.abs(differX), Math.abs(differY)) === Math.abs(differX);
  var scrollY = currentTarget.scrollHeight - currentTarget.clientHeight;
  var scrollX = currentTarget.scrollWidth - currentTarget.clientWidth;
  var style = document.defaultView.getComputedStyle(currentTarget);
  var overflowY = style.overflowY === 'auto' || style.overflowY === 'scroll';
  var overflowX = style.overflowX === 'auto' || style.overflowX === 'scroll';
  var y = scrollY && overflowY;
  var x = scrollX && overflowX;

  if (isY && (!y || y && (currentTarget.scrollTop >= scrollY && differY < 0 || currentTarget.scrollTop <= 0 && differY > 0)) || isX && (!x || x && (currentTarget.scrollLeft >= scrollX && scrollX < 0 || currentTarget.scrollLeft <= 0 && scrollX > 0))) {
    return getTouchParentScroll(root, currentTarget.parentNode, differX, differY);
  }

  return false;
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZHJhd2VyL2VzL3V0aWxzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZHJhd2VyL2VzL3V0aWxzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBkYXRhVG9BcnJheSh2YXJzKSB7XG4gIGlmIChBcnJheS5pc0FycmF5KHZhcnMpKSB7XG4gICAgcmV0dXJuIHZhcnM7XG4gIH1cblxuICByZXR1cm4gW3ZhcnNdO1xufVxudmFyIHRyYW5zaXRpb25FbmRPYmplY3QgPSB7XG4gIHRyYW5zaXRpb246ICd0cmFuc2l0aW9uZW5kJyxcbiAgV2Via2l0VHJhbnNpdGlvbjogJ3dlYmtpdFRyYW5zaXRpb25FbmQnLFxuICBNb3pUcmFuc2l0aW9uOiAndHJhbnNpdGlvbmVuZCcsXG4gIE9UcmFuc2l0aW9uOiAnb1RyYW5zaXRpb25FbmQgb3RyYW5zaXRpb25lbmQnXG59O1xuZXhwb3J0IHZhciB0cmFuc2l0aW9uU3RyID0gT2JqZWN0LmtleXModHJhbnNpdGlvbkVuZE9iamVjdCkuZmlsdGVyKGZ1bmN0aW9uIChrZXkpIHtcbiAgaWYgKHR5cGVvZiBkb2N1bWVudCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB2YXIgaHRtbCA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdodG1sJylbMF07XG4gIHJldHVybiBrZXkgaW4gKGh0bWwgPyBodG1sLnN0eWxlIDoge30pO1xufSlbMF07XG5leHBvcnQgdmFyIHRyYW5zaXRpb25FbmQgPSB0cmFuc2l0aW9uRW5kT2JqZWN0W3RyYW5zaXRpb25TdHJdO1xuZXhwb3J0IGZ1bmN0aW9uIGFkZEV2ZW50TGlzdGVuZXIodGFyZ2V0LCBldmVudFR5cGUsIGNhbGxiYWNrLCBvcHRpb25zKSB7XG4gIGlmICh0YXJnZXQuYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgIHRhcmdldC5hZGRFdmVudExpc3RlbmVyKGV2ZW50VHlwZSwgY2FsbGJhY2ssIG9wdGlvbnMpO1xuICB9IGVsc2UgaWYgKHRhcmdldC5hdHRhY2hFdmVudCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLWxpbmVcbiAgICB0YXJnZXQuYXR0YWNoRXZlbnQoXCJvblwiLmNvbmNhdChldmVudFR5cGUpLCBjYWxsYmFjayk7IC8vIHRzbGludDpkaXNhYmxlLWxpbmVcbiAgfVxufVxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZUV2ZW50TGlzdGVuZXIodGFyZ2V0LCBldmVudFR5cGUsIGNhbGxiYWNrLCBvcHRpb25zKSB7XG4gIGlmICh0YXJnZXQucmVtb3ZlRXZlbnRMaXN0ZW5lcikge1xuICAgIHRhcmdldC5yZW1vdmVFdmVudExpc3RlbmVyKGV2ZW50VHlwZSwgY2FsbGJhY2ssIG9wdGlvbnMpO1xuICB9IGVsc2UgaWYgKHRhcmdldC5hdHRhY2hFdmVudCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLWxpbmVcbiAgICB0YXJnZXQuZGV0YWNoRXZlbnQoXCJvblwiLmNvbmNhdChldmVudFR5cGUpLCBjYWxsYmFjayk7IC8vIHRzbGludDpkaXNhYmxlLWxpbmVcbiAgfVxufVxuZXhwb3J0IGZ1bmN0aW9uIHRyYW5zZm9ybUFyZ3VtZW50cyhhcmcsIGNiKSB7XG4gIHZhciByZXN1bHQgPSB0eXBlb2YgYXJnID09PSAnZnVuY3Rpb24nID8gYXJnKGNiKSA6IGFyZztcblxuICBpZiAoQXJyYXkuaXNBcnJheShyZXN1bHQpKSB7XG4gICAgaWYgKHJlc3VsdC5sZW5ndGggPT09IDIpIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIFtyZXN1bHRbMF0sIHJlc3VsdFsxXV07XG4gIH1cblxuICByZXR1cm4gW3Jlc3VsdF07XG59XG5leHBvcnQgdmFyIGlzTnVtZXJpYyA9IGZ1bmN0aW9uIGlzTnVtZXJpYyh2YWx1ZSkge1xuICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQodmFsdWUpKSAmJiBpc0Zpbml0ZSh2YWx1ZSk7XG59O1xuZXhwb3J0IHZhciB3aW5kb3dJc1VuZGVmaW5lZCA9ICEodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50ICYmIHdpbmRvdy5kb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcbmV4cG9ydCB2YXIgZ2V0VG91Y2hQYXJlbnRTY3JvbGwgPSBmdW5jdGlvbiBnZXRUb3VjaFBhcmVudFNjcm9sbChyb290LCBjdXJyZW50VGFyZ2V0LCBkaWZmZXJYLCBkaWZmZXJZKSB7XG4gIGlmICghY3VycmVudFRhcmdldCB8fCBjdXJyZW50VGFyZ2V0ID09PSBkb2N1bWVudCB8fCBjdXJyZW50VGFyZ2V0IGluc3RhbmNlb2YgRG9jdW1lbnQpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH0gLy8gcm9vdCDkuLogZHJhd2VyLWNvbnRlbnQg6K6+5a6a5LqGIG92ZXJmbG93LCDliKTmlq3kuLogcm9vdCDnmoQgcGFyZW50IOaXtue7k+adn+a7muWKqO+8m1xuXG5cbiAgaWYgKGN1cnJlbnRUYXJnZXQgPT09IHJvb3QucGFyZW50Tm9kZSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgdmFyIGlzWSA9IE1hdGgubWF4KE1hdGguYWJzKGRpZmZlclgpLCBNYXRoLmFicyhkaWZmZXJZKSkgPT09IE1hdGguYWJzKGRpZmZlclkpO1xuICB2YXIgaXNYID0gTWF0aC5tYXgoTWF0aC5hYnMoZGlmZmVyWCksIE1hdGguYWJzKGRpZmZlclkpKSA9PT0gTWF0aC5hYnMoZGlmZmVyWCk7XG4gIHZhciBzY3JvbGxZID0gY3VycmVudFRhcmdldC5zY3JvbGxIZWlnaHQgLSBjdXJyZW50VGFyZ2V0LmNsaWVudEhlaWdodDtcbiAgdmFyIHNjcm9sbFggPSBjdXJyZW50VGFyZ2V0LnNjcm9sbFdpZHRoIC0gY3VycmVudFRhcmdldC5jbGllbnRXaWR0aDtcbiAgdmFyIHN0eWxlID0gZG9jdW1lbnQuZGVmYXVsdFZpZXcuZ2V0Q29tcHV0ZWRTdHlsZShjdXJyZW50VGFyZ2V0KTtcbiAgdmFyIG92ZXJmbG93WSA9IHN0eWxlLm92ZXJmbG93WSA9PT0gJ2F1dG8nIHx8IHN0eWxlLm92ZXJmbG93WSA9PT0gJ3Njcm9sbCc7XG4gIHZhciBvdmVyZmxvd1ggPSBzdHlsZS5vdmVyZmxvd1ggPT09ICdhdXRvJyB8fCBzdHlsZS5vdmVyZmxvd1ggPT09ICdzY3JvbGwnO1xuICB2YXIgeSA9IHNjcm9sbFkgJiYgb3ZlcmZsb3dZO1xuICB2YXIgeCA9IHNjcm9sbFggJiYgb3ZlcmZsb3dYO1xuXG4gIGlmIChpc1kgJiYgKCF5IHx8IHkgJiYgKGN1cnJlbnRUYXJnZXQuc2Nyb2xsVG9wID49IHNjcm9sbFkgJiYgZGlmZmVyWSA8IDAgfHwgY3VycmVudFRhcmdldC5zY3JvbGxUb3AgPD0gMCAmJiBkaWZmZXJZID4gMCkpIHx8IGlzWCAmJiAoIXggfHwgeCAmJiAoY3VycmVudFRhcmdldC5zY3JvbGxMZWZ0ID49IHNjcm9sbFggJiYgc2Nyb2xsWCA8IDAgfHwgY3VycmVudFRhcmdldC5zY3JvbGxMZWZ0IDw9IDAgJiYgc2Nyb2xsWCA+IDApKSkge1xuICAgIHJldHVybiBnZXRUb3VjaFBhcmVudFNjcm9sbChyb290LCBjdXJyZW50VGFyZ2V0LnBhcmVudE5vZGUsIGRpZmZlclgsIGRpZmZlclkpO1xuICB9XG5cbiAgcmV0dXJuIGZhbHNlO1xufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-drawer/es/utils.js
