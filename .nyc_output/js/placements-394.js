__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOverflowOptions", function() { return getOverflowOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getPlacements; });
/* harmony import */ var rc_tooltip_es_placements__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-tooltip/es/placements */ "./node_modules/rc-tooltip/es/placements.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}


var autoAdjustOverflowEnabled = {
  adjustX: 1,
  adjustY: 1
};
var autoAdjustOverflowDisabled = {
  adjustX: 0,
  adjustY: 0
};
var targetOffset = [0, 0];
function getOverflowOptions(autoAdjustOverflow) {
  if (typeof autoAdjustOverflow === 'boolean') {
    return autoAdjustOverflow ? autoAdjustOverflowEnabled : autoAdjustOverflowDisabled;
  }

  return _extends(_extends({}, autoAdjustOverflowDisabled), autoAdjustOverflow);
}
function getPlacements() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _config$arrowWidth = config.arrowWidth,
      arrowWidth = _config$arrowWidth === void 0 ? 5 : _config$arrowWidth,
      _config$horizontalArr = config.horizontalArrowShift,
      horizontalArrowShift = _config$horizontalArr === void 0 ? 16 : _config$horizontalArr,
      _config$verticalArrow = config.verticalArrowShift,
      verticalArrowShift = _config$verticalArrow === void 0 ? 12 : _config$verticalArrow,
      _config$autoAdjustOve = config.autoAdjustOverflow,
      autoAdjustOverflow = _config$autoAdjustOve === void 0 ? true : _config$autoAdjustOve;
  var placementMap = {
    left: {
      points: ['cr', 'cl'],
      offset: [-4, 0]
    },
    right: {
      points: ['cl', 'cr'],
      offset: [4, 0]
    },
    top: {
      points: ['bc', 'tc'],
      offset: [0, -4]
    },
    bottom: {
      points: ['tc', 'bc'],
      offset: [0, 4]
    },
    topLeft: {
      points: ['bl', 'tc'],
      offset: [-(horizontalArrowShift + arrowWidth), -4]
    },
    leftTop: {
      points: ['tr', 'cl'],
      offset: [-4, -(verticalArrowShift + arrowWidth)]
    },
    topRight: {
      points: ['br', 'tc'],
      offset: [horizontalArrowShift + arrowWidth, -4]
    },
    rightTop: {
      points: ['tl', 'cr'],
      offset: [4, -(verticalArrowShift + arrowWidth)]
    },
    bottomRight: {
      points: ['tr', 'bc'],
      offset: [horizontalArrowShift + arrowWidth, 4]
    },
    rightBottom: {
      points: ['bl', 'cr'],
      offset: [4, verticalArrowShift + arrowWidth]
    },
    bottomLeft: {
      points: ['tl', 'bc'],
      offset: [-(horizontalArrowShift + arrowWidth), 4]
    },
    leftBottom: {
      points: ['br', 'cl'],
      offset: [-4, verticalArrowShift + arrowWidth]
    }
  };
  Object.keys(placementMap).forEach(function (key) {
    placementMap[key] = config.arrowPointAtCenter ? _extends(_extends({}, placementMap[key]), {
      overflow: getOverflowOptions(autoAdjustOverflow),
      targetOffset: targetOffset
    }) : _extends(_extends({}, rc_tooltip_es_placements__WEBPACK_IMPORTED_MODULE_0__["placements"][key]), {
      overflow: getOverflowOptions(autoAdjustOverflow)
    });
    placementMap[key].ignoreShake = true;
  });
  return placementMap;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90b29sdGlwL3BsYWNlbWVudHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3Rvb2x0aXAvcGxhY2VtZW50cy5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgcGxhY2VtZW50cyBhcyByY1BsYWNlbWVudHMgfSBmcm9tICdyYy10b29sdGlwL2xpYi9wbGFjZW1lbnRzJztcbmNvbnN0IGF1dG9BZGp1c3RPdmVyZmxvd0VuYWJsZWQgPSB7XG4gICAgYWRqdXN0WDogMSxcbiAgICBhZGp1c3RZOiAxLFxufTtcbmNvbnN0IGF1dG9BZGp1c3RPdmVyZmxvd0Rpc2FibGVkID0ge1xuICAgIGFkanVzdFg6IDAsXG4gICAgYWRqdXN0WTogMCxcbn07XG5jb25zdCB0YXJnZXRPZmZzZXQgPSBbMCwgMF07XG5leHBvcnQgZnVuY3Rpb24gZ2V0T3ZlcmZsb3dPcHRpb25zKGF1dG9BZGp1c3RPdmVyZmxvdykge1xuICAgIGlmICh0eXBlb2YgYXV0b0FkanVzdE92ZXJmbG93ID09PSAnYm9vbGVhbicpIHtcbiAgICAgICAgcmV0dXJuIGF1dG9BZGp1c3RPdmVyZmxvdyA/IGF1dG9BZGp1c3RPdmVyZmxvd0VuYWJsZWQgOiBhdXRvQWRqdXN0T3ZlcmZsb3dEaXNhYmxlZDtcbiAgICB9XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgYXV0b0FkanVzdE92ZXJmbG93RGlzYWJsZWQpLCBhdXRvQWRqdXN0T3ZlcmZsb3cpO1xufVxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gZ2V0UGxhY2VtZW50cyhjb25maWcgPSB7fSkge1xuICAgIGNvbnN0IHsgYXJyb3dXaWR0aCA9IDUsIGhvcml6b250YWxBcnJvd1NoaWZ0ID0gMTYsIHZlcnRpY2FsQXJyb3dTaGlmdCA9IDEyLCBhdXRvQWRqdXN0T3ZlcmZsb3cgPSB0cnVlLCB9ID0gY29uZmlnO1xuICAgIGNvbnN0IHBsYWNlbWVudE1hcCA9IHtcbiAgICAgICAgbGVmdDoge1xuICAgICAgICAgICAgcG9pbnRzOiBbJ2NyJywgJ2NsJ10sXG4gICAgICAgICAgICBvZmZzZXQ6IFstNCwgMF0sXG4gICAgICAgIH0sXG4gICAgICAgIHJpZ2h0OiB7XG4gICAgICAgICAgICBwb2ludHM6IFsnY2wnLCAnY3InXSxcbiAgICAgICAgICAgIG9mZnNldDogWzQsIDBdLFxuICAgICAgICB9LFxuICAgICAgICB0b3A6IHtcbiAgICAgICAgICAgIHBvaW50czogWydiYycsICd0YyddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbMCwgLTRdLFxuICAgICAgICB9LFxuICAgICAgICBib3R0b206IHtcbiAgICAgICAgICAgIHBvaW50czogWyd0YycsICdiYyddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbMCwgNF0sXG4gICAgICAgIH0sXG4gICAgICAgIHRvcExlZnQ6IHtcbiAgICAgICAgICAgIHBvaW50czogWydibCcsICd0YyddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbLShob3Jpem9udGFsQXJyb3dTaGlmdCArIGFycm93V2lkdGgpLCAtNF0sXG4gICAgICAgIH0sXG4gICAgICAgIGxlZnRUb3A6IHtcbiAgICAgICAgICAgIHBvaW50czogWyd0cicsICdjbCddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbLTQsIC0odmVydGljYWxBcnJvd1NoaWZ0ICsgYXJyb3dXaWR0aCldLFxuICAgICAgICB9LFxuICAgICAgICB0b3BSaWdodDoge1xuICAgICAgICAgICAgcG9pbnRzOiBbJ2JyJywgJ3RjJ10sXG4gICAgICAgICAgICBvZmZzZXQ6IFtob3Jpem9udGFsQXJyb3dTaGlmdCArIGFycm93V2lkdGgsIC00XSxcbiAgICAgICAgfSxcbiAgICAgICAgcmlnaHRUb3A6IHtcbiAgICAgICAgICAgIHBvaW50czogWyd0bCcsICdjciddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbNCwgLSh2ZXJ0aWNhbEFycm93U2hpZnQgKyBhcnJvd1dpZHRoKV0sXG4gICAgICAgIH0sXG4gICAgICAgIGJvdHRvbVJpZ2h0OiB7XG4gICAgICAgICAgICBwb2ludHM6IFsndHInLCAnYmMnXSxcbiAgICAgICAgICAgIG9mZnNldDogW2hvcml6b250YWxBcnJvd1NoaWZ0ICsgYXJyb3dXaWR0aCwgNF0sXG4gICAgICAgIH0sXG4gICAgICAgIHJpZ2h0Qm90dG9tOiB7XG4gICAgICAgICAgICBwb2ludHM6IFsnYmwnLCAnY3InXSxcbiAgICAgICAgICAgIG9mZnNldDogWzQsIHZlcnRpY2FsQXJyb3dTaGlmdCArIGFycm93V2lkdGhdLFxuICAgICAgICB9LFxuICAgICAgICBib3R0b21MZWZ0OiB7XG4gICAgICAgICAgICBwb2ludHM6IFsndGwnLCAnYmMnXSxcbiAgICAgICAgICAgIG9mZnNldDogWy0oaG9yaXpvbnRhbEFycm93U2hpZnQgKyBhcnJvd1dpZHRoKSwgNF0sXG4gICAgICAgIH0sXG4gICAgICAgIGxlZnRCb3R0b206IHtcbiAgICAgICAgICAgIHBvaW50czogWydicicsICdjbCddLFxuICAgICAgICAgICAgb2Zmc2V0OiBbLTQsIHZlcnRpY2FsQXJyb3dTaGlmdCArIGFycm93V2lkdGhdLFxuICAgICAgICB9LFxuICAgIH07XG4gICAgT2JqZWN0LmtleXMocGxhY2VtZW50TWFwKS5mb3JFYWNoKGtleSA9PiB7XG4gICAgICAgIHBsYWNlbWVudE1hcFtrZXldID0gY29uZmlnLmFycm93UG9pbnRBdENlbnRlclxuICAgICAgICAgICAgPyBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHBsYWNlbWVudE1hcFtrZXldKSwgeyBvdmVyZmxvdzogZ2V0T3ZlcmZsb3dPcHRpb25zKGF1dG9BZGp1c3RPdmVyZmxvdyksIHRhcmdldE9mZnNldCB9KSA6IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgcmNQbGFjZW1lbnRzW2tleV0pLCB7IG92ZXJmbG93OiBnZXRPdmVyZmxvd09wdGlvbnMoYXV0b0FkanVzdE92ZXJmbG93KSB9KTtcbiAgICAgICAgcGxhY2VtZW50TWFwW2tleV0uaWdub3JlU2hha2UgPSB0cnVlO1xuICAgIH0pO1xuICAgIHJldHVybiBwbGFjZW1lbnRNYXA7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQTdDQTtBQWtEQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSEE7QUFLQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/tooltip/placements.js
