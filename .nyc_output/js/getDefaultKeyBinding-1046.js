/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getDefaultKeyBinding
 * @format
 * 
 */


var KeyBindingUtil = __webpack_require__(/*! ./KeyBindingUtil */ "./node_modules/draft-js/lib/KeyBindingUtil.js");

var Keys = __webpack_require__(/*! fbjs/lib/Keys */ "./node_modules/fbjs/lib/Keys.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var isOSX = UserAgent.isPlatform('Mac OS X');
var isWindows = UserAgent.isPlatform('Windows'); // Firefox on OSX had a bug resulting in navigation instead of cursor movement.
// This bug was fixed in Firefox 29. Feature detection is virtually impossible
// so we just check the version number. See #342765.

var shouldFixFirefoxMovement = isOSX && UserAgent.isBrowser('Firefox < 29');
var hasCommandModifier = KeyBindingUtil.hasCommandModifier,
    isCtrlKeyCommand = KeyBindingUtil.isCtrlKeyCommand;

function shouldRemoveWord(e) {
  return isOSX && e.altKey || isCtrlKeyCommand(e);
}
/**
 * Get the appropriate undo/redo command for a Z key command.
 */


function getZCommand(e) {
  if (!hasCommandModifier(e)) {
    return null;
  }

  return e.shiftKey ? 'redo' : 'undo';
}

function getDeleteCommand(e) {
  // Allow default "cut" behavior for Windows on Shift + Delete.
  if (isWindows && e.shiftKey) {
    return null;
  }

  return shouldRemoveWord(e) ? 'delete-word' : 'delete';
}

function getBackspaceCommand(e) {
  if (hasCommandModifier(e) && isOSX) {
    return 'backspace-to-start-of-line';
  }

  return shouldRemoveWord(e) ? 'backspace-word' : 'backspace';
}
/**
 * Retrieve a bound key command for the given event.
 */


function getDefaultKeyBinding(e) {
  switch (e.keyCode) {
    case 66:
      // B
      return hasCommandModifier(e) ? 'bold' : null;

    case 68:
      // D
      return isCtrlKeyCommand(e) ? 'delete' : null;

    case 72:
      // H
      return isCtrlKeyCommand(e) ? 'backspace' : null;

    case 73:
      // I
      return hasCommandModifier(e) ? 'italic' : null;

    case 74:
      // J
      return hasCommandModifier(e) ? 'code' : null;

    case 75:
      // K
      return !isWindows && isCtrlKeyCommand(e) ? 'secondary-cut' : null;

    case 77:
      // M
      return isCtrlKeyCommand(e) ? 'split-block' : null;

    case 79:
      // O
      return isCtrlKeyCommand(e) ? 'split-block' : null;

    case 84:
      // T
      return isOSX && isCtrlKeyCommand(e) ? 'transpose-characters' : null;

    case 85:
      // U
      return hasCommandModifier(e) ? 'underline' : null;

    case 87:
      // W
      return isOSX && isCtrlKeyCommand(e) ? 'backspace-word' : null;

    case 89:
      // Y
      if (isCtrlKeyCommand(e)) {
        return isWindows ? 'redo' : 'secondary-paste';
      }

      return null;

    case 90:
      // Z
      return getZCommand(e) || null;

    case Keys.RETURN:
      return 'split-block';

    case Keys.DELETE:
      return getDeleteCommand(e);

    case Keys.BACKSPACE:
      return getBackspaceCommand(e);
    // LEFT/RIGHT handlers serve as a workaround for a Firefox bug.

    case Keys.LEFT:
      return shouldFixFirefoxMovement && hasCommandModifier(e) ? 'move-selection-to-start-of-block' : null;

    case Keys.RIGHT:
      return shouldFixFirefoxMovement && hasCommandModifier(e) ? 'move-selection-to-end-of-block' : null;

    default:
      return null;
  }
}

module.exports = getDefaultKeyBinding;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERlZmF1bHRLZXlCaW5kaW5nLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERlZmF1bHRLZXlCaW5kaW5nLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0RGVmYXVsdEtleUJpbmRpbmdcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEtleUJpbmRpbmdVdGlsID0gcmVxdWlyZSgnLi9LZXlCaW5kaW5nVXRpbCcpO1xudmFyIEtleXMgPSByZXF1aXJlKCdmYmpzL2xpYi9LZXlzJyk7XG52YXIgVXNlckFnZW50ID0gcmVxdWlyZSgnZmJqcy9saWIvVXNlckFnZW50Jyk7XG5cbnZhciBpc09TWCA9IFVzZXJBZ2VudC5pc1BsYXRmb3JtKCdNYWMgT1MgWCcpO1xudmFyIGlzV2luZG93cyA9IFVzZXJBZ2VudC5pc1BsYXRmb3JtKCdXaW5kb3dzJyk7XG5cbi8vIEZpcmVmb3ggb24gT1NYIGhhZCBhIGJ1ZyByZXN1bHRpbmcgaW4gbmF2aWdhdGlvbiBpbnN0ZWFkIG9mIGN1cnNvciBtb3ZlbWVudC5cbi8vIFRoaXMgYnVnIHdhcyBmaXhlZCBpbiBGaXJlZm94IDI5LiBGZWF0dXJlIGRldGVjdGlvbiBpcyB2aXJ0dWFsbHkgaW1wb3NzaWJsZVxuLy8gc28gd2UganVzdCBjaGVjayB0aGUgdmVyc2lvbiBudW1iZXIuIFNlZSAjMzQyNzY1LlxudmFyIHNob3VsZEZpeEZpcmVmb3hNb3ZlbWVudCA9IGlzT1NYICYmIFVzZXJBZ2VudC5pc0Jyb3dzZXIoJ0ZpcmVmb3ggPCAyOScpO1xuXG52YXIgaGFzQ29tbWFuZE1vZGlmaWVyID0gS2V5QmluZGluZ1V0aWwuaGFzQ29tbWFuZE1vZGlmaWVyLFxuICAgIGlzQ3RybEtleUNvbW1hbmQgPSBLZXlCaW5kaW5nVXRpbC5pc0N0cmxLZXlDb21tYW5kO1xuXG5cbmZ1bmN0aW9uIHNob3VsZFJlbW92ZVdvcmQoZSkge1xuICByZXR1cm4gaXNPU1ggJiYgZS5hbHRLZXkgfHwgaXNDdHJsS2V5Q29tbWFuZChlKTtcbn1cblxuLyoqXG4gKiBHZXQgdGhlIGFwcHJvcHJpYXRlIHVuZG8vcmVkbyBjb21tYW5kIGZvciBhIFoga2V5IGNvbW1hbmQuXG4gKi9cbmZ1bmN0aW9uIGdldFpDb21tYW5kKGUpIHtcbiAgaWYgKCFoYXNDb21tYW5kTW9kaWZpZXIoZSkpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICByZXR1cm4gZS5zaGlmdEtleSA/ICdyZWRvJyA6ICd1bmRvJztcbn1cblxuZnVuY3Rpb24gZ2V0RGVsZXRlQ29tbWFuZChlKSB7XG4gIC8vIEFsbG93IGRlZmF1bHQgXCJjdXRcIiBiZWhhdmlvciBmb3IgV2luZG93cyBvbiBTaGlmdCArIERlbGV0ZS5cbiAgaWYgKGlzV2luZG93cyAmJiBlLnNoaWZ0S2V5KSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cbiAgcmV0dXJuIHNob3VsZFJlbW92ZVdvcmQoZSkgPyAnZGVsZXRlLXdvcmQnIDogJ2RlbGV0ZSc7XG59XG5cbmZ1bmN0aW9uIGdldEJhY2tzcGFjZUNvbW1hbmQoZSkge1xuICBpZiAoaGFzQ29tbWFuZE1vZGlmaWVyKGUpICYmIGlzT1NYKSB7XG4gICAgcmV0dXJuICdiYWNrc3BhY2UtdG8tc3RhcnQtb2YtbGluZSc7XG4gIH1cbiAgcmV0dXJuIHNob3VsZFJlbW92ZVdvcmQoZSkgPyAnYmFja3NwYWNlLXdvcmQnIDogJ2JhY2tzcGFjZSc7XG59XG5cbi8qKlxuICogUmV0cmlldmUgYSBib3VuZCBrZXkgY29tbWFuZCBmb3IgdGhlIGdpdmVuIGV2ZW50LlxuICovXG5mdW5jdGlvbiBnZXREZWZhdWx0S2V5QmluZGluZyhlKSB7XG4gIHN3aXRjaCAoZS5rZXlDb2RlKSB7XG4gICAgY2FzZSA2NjpcbiAgICAgIC8vIEJcbiAgICAgIHJldHVybiBoYXNDb21tYW5kTW9kaWZpZXIoZSkgPyAnYm9sZCcgOiBudWxsO1xuICAgIGNhc2UgNjg6XG4gICAgICAvLyBEXG4gICAgICByZXR1cm4gaXNDdHJsS2V5Q29tbWFuZChlKSA/ICdkZWxldGUnIDogbnVsbDtcbiAgICBjYXNlIDcyOlxuICAgICAgLy8gSFxuICAgICAgcmV0dXJuIGlzQ3RybEtleUNvbW1hbmQoZSkgPyAnYmFja3NwYWNlJyA6IG51bGw7XG4gICAgY2FzZSA3MzpcbiAgICAgIC8vIElcbiAgICAgIHJldHVybiBoYXNDb21tYW5kTW9kaWZpZXIoZSkgPyAnaXRhbGljJyA6IG51bGw7XG4gICAgY2FzZSA3NDpcbiAgICAgIC8vIEpcbiAgICAgIHJldHVybiBoYXNDb21tYW5kTW9kaWZpZXIoZSkgPyAnY29kZScgOiBudWxsO1xuICAgIGNhc2UgNzU6XG4gICAgICAvLyBLXG4gICAgICByZXR1cm4gIWlzV2luZG93cyAmJiBpc0N0cmxLZXlDb21tYW5kKGUpID8gJ3NlY29uZGFyeS1jdXQnIDogbnVsbDtcbiAgICBjYXNlIDc3OlxuICAgICAgLy8gTVxuICAgICAgcmV0dXJuIGlzQ3RybEtleUNvbW1hbmQoZSkgPyAnc3BsaXQtYmxvY2snIDogbnVsbDtcbiAgICBjYXNlIDc5OlxuICAgICAgLy8gT1xuICAgICAgcmV0dXJuIGlzQ3RybEtleUNvbW1hbmQoZSkgPyAnc3BsaXQtYmxvY2snIDogbnVsbDtcbiAgICBjYXNlIDg0OlxuICAgICAgLy8gVFxuICAgICAgcmV0dXJuIGlzT1NYICYmIGlzQ3RybEtleUNvbW1hbmQoZSkgPyAndHJhbnNwb3NlLWNoYXJhY3RlcnMnIDogbnVsbDtcbiAgICBjYXNlIDg1OlxuICAgICAgLy8gVVxuICAgICAgcmV0dXJuIGhhc0NvbW1hbmRNb2RpZmllcihlKSA/ICd1bmRlcmxpbmUnIDogbnVsbDtcbiAgICBjYXNlIDg3OlxuICAgICAgLy8gV1xuICAgICAgcmV0dXJuIGlzT1NYICYmIGlzQ3RybEtleUNvbW1hbmQoZSkgPyAnYmFja3NwYWNlLXdvcmQnIDogbnVsbDtcbiAgICBjYXNlIDg5OlxuICAgICAgLy8gWVxuICAgICAgaWYgKGlzQ3RybEtleUNvbW1hbmQoZSkpIHtcbiAgICAgICAgcmV0dXJuIGlzV2luZG93cyA/ICdyZWRvJyA6ICdzZWNvbmRhcnktcGFzdGUnO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgY2FzZSA5MDpcbiAgICAgIC8vIFpcbiAgICAgIHJldHVybiBnZXRaQ29tbWFuZChlKSB8fCBudWxsO1xuICAgIGNhc2UgS2V5cy5SRVRVUk46XG4gICAgICByZXR1cm4gJ3NwbGl0LWJsb2NrJztcbiAgICBjYXNlIEtleXMuREVMRVRFOlxuICAgICAgcmV0dXJuIGdldERlbGV0ZUNvbW1hbmQoZSk7XG4gICAgY2FzZSBLZXlzLkJBQ0tTUEFDRTpcbiAgICAgIHJldHVybiBnZXRCYWNrc3BhY2VDb21tYW5kKGUpO1xuICAgIC8vIExFRlQvUklHSFQgaGFuZGxlcnMgc2VydmUgYXMgYSB3b3JrYXJvdW5kIGZvciBhIEZpcmVmb3ggYnVnLlxuICAgIGNhc2UgS2V5cy5MRUZUOlxuICAgICAgcmV0dXJuIHNob3VsZEZpeEZpcmVmb3hNb3ZlbWVudCAmJiBoYXNDb21tYW5kTW9kaWZpZXIoZSkgPyAnbW92ZS1zZWxlY3Rpb24tdG8tc3RhcnQtb2YtYmxvY2snIDogbnVsbDtcbiAgICBjYXNlIEtleXMuUklHSFQ6XG4gICAgICByZXR1cm4gc2hvdWxkRml4RmlyZWZveE1vdmVtZW50ICYmIGhhc0NvbW1hbmRNb2RpZmllcihlKSA/ICdtb3ZlLXNlbGVjdGlvbi10by1lbmQtb2YtYmxvY2snIDogbnVsbDtcbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIG51bGw7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXREZWZhdWx0S2V5QmluZGluZzsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUF2REE7QUF5REE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getDefaultKeyBinding.js
