

function getViewportWidth() {
  var width = void 0;

  if (document.documentElement) {
    width = document.documentElement.clientWidth;
  }

  if (!width && document.body) {
    width = document.body.clientWidth;
  }

  return width || 0;
}
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 * @typechecks
 */


function getViewportHeight() {
  var height = void 0;

  if (document.documentElement) {
    height = document.documentElement.clientHeight;
  }

  if (!height && document.body) {
    height = document.body.clientHeight;
  }

  return height || 0;
}
/**
 * Gets the viewport dimensions including any scrollbars.
 */


function getViewportDimensions() {
  return {
    width: window.innerWidth || getViewportWidth(),
    height: window.innerHeight || getViewportHeight()
  };
}
/**
 * Gets the viewport dimensions excluding any scrollbars.
 */


getViewportDimensions.withoutScrollbars = function () {
  return {
    width: getViewportWidth(),
    height: getViewportHeight()
  };
};

module.exports = getViewportDimensions;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0Vmlld3BvcnREaW1lbnNpb25zLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0Vmlld3BvcnREaW1lbnNpb25zLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBnZXRWaWV3cG9ydFdpZHRoKCkge1xuICB2YXIgd2lkdGggPSB2b2lkIDA7XG4gIGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcbiAgICB3aWR0aCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aDtcbiAgfVxuXG4gIGlmICghd2lkdGggJiYgZG9jdW1lbnQuYm9keSkge1xuICAgIHdpZHRoID0gZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aDtcbiAgfVxuXG4gIHJldHVybiB3aWR0aCB8fCAwO1xufSAvKipcbiAgICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gICAqXG4gICAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICAgKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gICAqXG4gICAqIFxuICAgKiBAdHlwZWNoZWNrc1xuICAgKi9cblxuZnVuY3Rpb24gZ2V0Vmlld3BvcnRIZWlnaHQoKSB7XG4gIHZhciBoZWlnaHQgPSB2b2lkIDA7XG4gIGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpIHtcbiAgICBoZWlnaHQgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0O1xuICB9XG5cbiAgaWYgKCFoZWlnaHQgJiYgZG9jdW1lbnQuYm9keSkge1xuICAgIGhlaWdodCA9IGRvY3VtZW50LmJvZHkuY2xpZW50SGVpZ2h0O1xuICB9XG5cbiAgcmV0dXJuIGhlaWdodCB8fCAwO1xufVxuXG4vKipcbiAqIEdldHMgdGhlIHZpZXdwb3J0IGRpbWVuc2lvbnMgaW5jbHVkaW5nIGFueSBzY3JvbGxiYXJzLlxuICovXG5mdW5jdGlvbiBnZXRWaWV3cG9ydERpbWVuc2lvbnMoKSB7XG4gIHJldHVybiB7XG4gICAgd2lkdGg6IHdpbmRvdy5pbm5lcldpZHRoIHx8IGdldFZpZXdwb3J0V2lkdGgoKSxcbiAgICBoZWlnaHQ6IHdpbmRvdy5pbm5lckhlaWdodCB8fCBnZXRWaWV3cG9ydEhlaWdodCgpXG4gIH07XG59XG5cbi8qKlxuICogR2V0cyB0aGUgdmlld3BvcnQgZGltZW5zaW9ucyBleGNsdWRpbmcgYW55IHNjcm9sbGJhcnMuXG4gKi9cbmdldFZpZXdwb3J0RGltZW5zaW9ucy53aXRob3V0U2Nyb2xsYmFycyA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHtcbiAgICB3aWR0aDogZ2V0Vmlld3BvcnRXaWR0aCgpLFxuICAgIGhlaWdodDogZ2V0Vmlld3BvcnRIZWlnaHQoKVxuICB9O1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBnZXRWaWV3cG9ydERpbWVuc2lvbnM7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getViewportDimensions.js
