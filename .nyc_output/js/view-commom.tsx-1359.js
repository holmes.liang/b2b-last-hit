__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common */ "./src/app/desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _desk_component_ph_componnets_extra_info_view__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/ph/componnets/extra-info-view */ "./src/app/desk/component/ph/componnets/extra-info-view.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/view-commom.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItem = function ViewItem(props) {
  return Object(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_13__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, props, {
    layout: props.layoutCol || defaultLayout
  }));
};

var PolicyholderViewCommon =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(PolicyholderViewCommon, _ModelWidget);

  function PolicyholderViewCommon(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, PolicyholderViewCommon);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PolicyholderViewCommon).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(PolicyholderViewCommon, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.props.isAddressType) {
        _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadCodeTables(this, ["id_types"], true);
      }

      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadCodeTables(this, ["tel_nation_codes"], true);
      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadCodeTables(this, ["idtype", "title", "addresstype", "orgtitle"]);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "genePropName",
    value: function genePropName(propName) {
      var propNameFixed = this.props.propNameFixed;
      var propsName = propNameFixed || "ext.policyholder";
      if (!propName) return propsName;
      return "".concat(propsName, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          type = _this$props.type,
          propNameFixed = _this$props.propNameFixed,
          layoutCol = _this$props.layoutCol,
          isAddressType = _this$props.isAddressType,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props, ["model", "type", "propNameFixed", "layoutCol", "isAddressType"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Full Name").thai("ชื่อ-สกุล").my("နာမည်အပြည့်အစုံ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("title", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("title")), this) || _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("orgtitle", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("title")), this), " ", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("name"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("ID Type/No.").thai("ประเภท ID / หมายเลข").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("id_types", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("idType")), this) || _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("idtype", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("idType")), this), " ", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("idNo"))), type === "EXTRA-INFO" && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component_ph_componnets_extra_info_view__WEBPACK_IMPORTED_MODULE_16__["default"], {
        model: model,
        propNameFixed: propNameFixed,
        layoutCol: layoutCol,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }), this.props.childDom || "", _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Mobile No.").thai("โทรศัพท์มือถือ").my("လက်ကိုင်ဖုန်းနာပတ်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("tel_nation_codes", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("mobileNationCode")), this), " ", " ", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("mobile"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Email").thai("อีเมล์").my("အီးမေးလ်က").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("email"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address Type").thai("สถานที่ทำงาน (กรณีไม่มี ให้เลือกที่อยู่ปัจจุบัน)").my("လိပ်စာအမျိုးအစား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("addresstype", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("addressType")), this)), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address").thai("ที่อยู่").my("လိပ်စာ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["ShowAddress"], {
        model: model,
        addressFixed: this.genePropName("address"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PolicyholderViewCommon.prototype), "initState", this).call(this), {
        codeTables: [],
        isAddressType: false
      });
    }
  }]);

  return PolicyholderViewCommon;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (PolicyholderViewCommon);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL3ZpZXctY29tbW9tLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC92aWV3LWNvbW1vbS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IFNob3dBZGRyZXNzIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuaW1wb3J0IFZpZXdJdGVtMSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3ZpZXctaXRlbVwiO1xuaW1wb3J0IENvbW1vbkdwY0NsYWltIGZyb20gXCJAZGVzay9jbGFpbXMvaGFuZGluZy9kZXRhaWxzL1NBSUMvZ3BjL3ZpZXcvc2VjdGlvbnMvY29uc2VxdWVuY2UvY29tcG9uZW50cy9jb21tb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCBFeHRyYUluZm9WaWV3IGZyb20gXCJAZGVzay1jb21wb25lbnQvcGgvY29tcG9ubmV0cy9leHRyYS1pbmZvLXZpZXdcIjtcblxuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTYgfSxcbiAgICBzbTogeyBzcGFuOiAxMyB9LFxuICB9LFxufTtcblxuY29uc3QgVmlld0l0ZW0gPSAocHJvcHM6IGFueSkgPT4gVmlld0l0ZW0xKHsgLi4ucHJvcHMsIGxheW91dDogcHJvcHMubGF5b3V0Q29sIHx8IGRlZmF1bHRMYXlvdXQgfSk7XG50eXBlIFBheWVyU3RhdGUgPSB7XG4gIGNvZGVUYWJsZXM6IGFueTtcbn07XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBQYXllclBhZ2VDb21wb25lbnRzID0ge1xuICBCb3hDb250ZW50OiBTdHlsZWRESVY7XG59O1xuZXhwb3J0IHR5cGUgUGF5ZXJQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgcHJvcE5hbWVGaXhlZD86IGFueTtcbiAgbGF5b3V0Q29sPzogYW55O1xuICBjaGlsZERvbT86IGFueTtcbiAgdHlwZT86IFwiRVhUUkEtSU5GT1wiIHwgdW5kZWZpbmVkO1xuICBpc0FkZHJlc3NUeXBlPzogYm9vbGVhbjtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5jbGFzcyBQb2xpY3lob2xkZXJWaWV3Q29tbW9uPFAgZXh0ZW5kcyBQYXllclByb3BzLCBTIGV4dGVuZHMgUGF5ZXJTdGF0ZSwgQyBleHRlbmRzIFBheWVyUGFnZUNvbXBvbmVudHM+XG4gIGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBQYXllclByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgaWYgKCF0aGlzLnByb3BzLmlzQWRkcmVzc1R5cGUpIHtcbiAgICAgIENvbW1vbkdwY0NsYWltLmxvYWRDb2RlVGFibGVzKHRoaXMsIFtcImlkX3R5cGVzXCJdLCB0cnVlKTtcbiAgICB9XG4gICAgQ29tbW9uR3BjQ2xhaW0ubG9hZENvZGVUYWJsZXModGhpcywgW1widGVsX25hdGlvbl9jb2Rlc1wiXSwgdHJ1ZSk7XG4gICAgQ29tbW9uR3BjQ2xhaW0ubG9hZENvZGVUYWJsZXModGhpcywgW1wiaWR0eXBlXCIsIFwidGl0bGVcIiwgXCJhZGRyZXNzdHlwZVwiLCBcIm9yZ3RpdGxlXCJdKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICB9XG5cbiAgZ2VuZVByb3BOYW1lKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgY29uc3QgeyBwcm9wTmFtZUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHByb3BzTmFtZSA9IHByb3BOYW1lRml4ZWQgfHwgXCJleHQucG9saWN5aG9sZGVyXCI7XG4gICAgaWYgKCFwcm9wTmFtZSkgcmV0dXJuIHByb3BzTmFtZTtcbiAgICByZXR1cm4gYCR7cHJvcHNOYW1lfS4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgbW9kZWwsIHR5cGUsIHByb3BOYW1lRml4ZWQsIGxheW91dENvbCwgaXNBZGRyZXNzVHlwZSwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgIDw+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkZ1bGwgTmFtZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rS3guKrguIHguLjguKVcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCU4YCs4YCZ4YCK4YC64YCh4YCV4YC84YCK4YC64YC34YCh4YCF4YCv4YC2XCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBDb21tb25HcGNDbGFpbS5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwidGl0bGVcIixcbiAgICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJ0aXRsZVwiKSksIHRoaXMpXG4gICAgICAgICAgICAgIHx8IENvbW1vbkdwY0NsYWltLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJvcmd0aXRsZVwiLFxuICAgICAgICAgICAgICAgIF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcInRpdGxlXCIpKSwgdGhpcylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHtcIiBcIn1cbiAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJuYW1lXCIpKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgbGF5b3V0Q29sPXtsYXlvdXRDb2x9XG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJJRCBUeXBlL05vLlwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRCAvIOC4q+C4oeC4suC4ouC5gOC4peC4glwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgK3hgK/hgIThgLrhgJLhgK7hgKHhgJnhgLvhgK3hgK/hgLjhgKHhgIXhgKzhgLggLyBOTyDhgYtcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIENvbW1vbkdwY0NsYWltLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJpZF90eXBlc1wiLFxuICAgICAgICAgICAgICAgIF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcImlkVHlwZVwiKSksIHRoaXMpIHx8XG4gICAgICAgICAgICAgIENvbW1vbkdwY0NsYWltLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJpZHR5cGVcIixcbiAgICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJpZFR5cGVcIikpLCB0aGlzKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAge1wiIFwifVxuICAgICAgICAgICAge18uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcImlkTm9cIikpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAge3R5cGUgPT09IFwiRVhUUkEtSU5GT1wiICYmIDxFeHRyYUluZm9WaWV3IG1vZGVsPXttb2RlbH0gcHJvcE5hbWVGaXhlZD17cHJvcE5hbWVGaXhlZH0gbGF5b3V0Q29sPXtsYXlvdXRDb2x9Lz59XG4gICAgICAgICAge3RoaXMucHJvcHMuY2hpbGREb20gfHwgXCJcIn1cbiAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiTW9iaWxlIE5vLlwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC5guC4l+C4o+C4qOC4seC4nuC4l+C5jOC4oeC4t+C4reC4luC4t+C4rVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgJzhgIDhgLrhgIDhgK3hgK/hgIThgLrhgJbhgK/hgJThgLrhgLjhgJThgKzhgJXhgJDhgLpcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7Q29tbW9uR3BjQ2xhaW0uZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcInRlbF9uYXRpb25fY29kZXNcIixcbiAgICAgICAgICAgICAgXy5nZXQobW9kZWwsIHRoaXMuZ2VuZVByb3BOYW1lKFwibW9iaWxlTmF0aW9uQ29kZVwiKSksIHRoaXMpfSB7XCIgXCJ9XG4gICAgICAgICAgICB7Xy5nZXQobW9kZWwsIHRoaXMuZ2VuZVByb3BOYW1lKFwibW9iaWxlXCIpKX1cbiAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgbGF5b3V0Q29sPXtsYXlvdXRDb2x9XG4gICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJFbWFpbFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4reC4teC5gOC4oeC4peC5jFwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgK7hgLjhgJnhgLHhgLjhgJzhgLrhgIBcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7Xy5nZXQobW9kZWwsIHRoaXMuZ2VuZVByb3BOYW1lKFwiZW1haWxcIikpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkFkZHJlc3MgVHlwZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4quC4luC4suC4meC4l+C4teC5iOC4l+C4s+C4h+C4suC4mSAo4LiB4Lij4LiT4Li14LmE4Lih4LmI4Lih4Li1IOC5g+C4q+C5ieC5gOC4peC4t+C4reC4geC4l+C4teC5iOC4reC4ouC4ueC5iOC4m+C4seC4iOC4iOC4uOC4muC4seC4mSlcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCc4YCt4YCV4YC64YCF4YCs4YCh4YCZ4YC74YCt4YCv4YC44YCh4YCF4YCs4YC4XCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge0NvbW1vbkdwY0NsYWltLmdldE1hc3RlclRhYmxlSXRlbVRleHQoXCJhZGRyZXNzdHlwZVwiLFxuICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJhZGRyZXNzVHlwZVwiKSksIHRoaXMpfVxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkFkZHJlc3NcIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguJfguLXguYjguK3guKLguLnguYhcIilcbiAgICAgICAgICAgICAgLm15KFwi4YCc4YCt4YCV4YC64YCF4YCsXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPFNob3dBZGRyZXNzIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBhZGRyZXNzRml4ZWQ9e3RoaXMuZ2VuZVByb3BOYW1lKFwiYWRkcmVzc1wiKX0vPlxuICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgIDwvPlxuICAgICAgPC9DLkJveENvbnRlbnQ+XG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94Q29udGVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgY29kZVRhYmxlczogW10sXG4gICAgICBpc0FkZHJlc3NUeXBlOiBmYWxzZSxcbiAgICB9KSBhcyBTO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBvbGljeWhvbGRlclZpZXdDb21tb247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBQ0E7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFrQkE7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTs7O0FBR0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFJQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7O0FBekhBO0FBQ0E7QUEySEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/view-commom.tsx
