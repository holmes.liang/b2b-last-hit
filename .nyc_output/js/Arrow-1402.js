

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _deepMerge = __webpack_require__(/*! ../utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/react-images/lib/components/Icon.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

function Arrow(_ref, _ref2) {
  var theme = _ref2.theme;

  var direction = _ref.direction,
      icon = _ref.icon,
      onClick = _ref.onClick,
      size = _ref.size,
      props = _objectWithoutProperties(_ref, ['direction', 'icon', 'onClick', 'size']);

  var classes = _noImportant.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, theme));

  return _react2.default.createElement('button', _extends({
    type: 'button',
    className: (0, _noImportant.css)(classes.arrow, classes['arrow__direction__' + direction], size && classes['arrow__size__' + size]),
    onClick: onClick,
    onTouchEnd: onClick
  }, props), _react2.default.createElement(_Icon2.default, {
    fill: !!theme.arrow && theme.arrow.fill || _theme2.default.arrow.fill,
    type: icon
  }));
}

Arrow.propTypes = {
  direction: _propTypes2.default.oneOf(['left', 'right']),
  icon: _propTypes2.default.string,
  onClick: _propTypes2.default.func.isRequired,
  size: _propTypes2.default.oneOf(['medium', 'small']).isRequired
};
Arrow.defaultProps = {
  size: 'medium'
};
Arrow.contextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  arrow: {
    background: 'none',
    border: 'none',
    borderRadius: 4,
    cursor: 'pointer',
    outline: 'none',
    padding: 10,
    // increase hit area
    position: 'absolute',
    top: '50%',
    // disable user select
    WebkitTouchCallout: 'none',
    userSelect: 'none'
  },
  // sizes
  arrow__size__medium: {
    height: _theme2.default.arrow.height,
    marginTop: _theme2.default.arrow.height / -2,
    width: 40,
    '@media (min-width: 768px)': {
      width: 70
    }
  },
  arrow__size__small: {
    height: _theme2.default.thumbnail.size,
    marginTop: _theme2.default.thumbnail.size / -2,
    width: 30,
    '@media (min-width: 500px)': {
      width: 40
    }
  },
  // direction
  arrow__direction__right: {
    right: _theme2.default.container.gutter.horizontal
  },
  arrow__direction__left: {
    left: _theme2.default.container.gutter.horizontal
  }
};
exports.default = Arrow;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0Fycm93LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0Fycm93LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9ub0ltcG9ydGFudCA9IHJlcXVpcmUoJ2FwaHJvZGl0ZS9uby1pbXBvcnRhbnQnKTtcblxudmFyIF90aGVtZSA9IHJlcXVpcmUoJy4uL3RoZW1lJyk7XG5cbnZhciBfdGhlbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdGhlbWUpO1xuXG52YXIgX2RlZXBNZXJnZSA9IHJlcXVpcmUoJy4uL3V0aWxzL2RlZXBNZXJnZScpO1xuXG52YXIgX2RlZXBNZXJnZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWVwTWVyZ2UpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIEFycm93KF9yZWYsIF9yZWYyKSB7XG5cdHZhciB0aGVtZSA9IF9yZWYyLnRoZW1lO1xuXG5cdHZhciBkaXJlY3Rpb24gPSBfcmVmLmRpcmVjdGlvbixcblx0ICAgIGljb24gPSBfcmVmLmljb24sXG5cdCAgICBvbkNsaWNrID0gX3JlZi5vbkNsaWNrLFxuXHQgICAgc2l6ZSA9IF9yZWYuc2l6ZSxcblx0ICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9yZWYsIFsnZGlyZWN0aW9uJywgJ2ljb24nLCAnb25DbGljaycsICdzaXplJ10pO1xuXG5cdHZhciBjbGFzc2VzID0gX25vSW1wb3J0YW50LlN0eWxlU2hlZXQuY3JlYXRlKCgwLCBfZGVlcE1lcmdlMi5kZWZhdWx0KShkZWZhdWx0U3R5bGVzLCB0aGVtZSkpO1xuXG5cdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcblx0XHQnYnV0dG9uJyxcblx0XHRfZXh0ZW5kcyh7XG5cdFx0XHR0eXBlOiAnYnV0dG9uJyxcblx0XHRcdGNsYXNzTmFtZTogKDAsIF9ub0ltcG9ydGFudC5jc3MpKGNsYXNzZXMuYXJyb3csIGNsYXNzZXNbJ2Fycm93X19kaXJlY3Rpb25fXycgKyBkaXJlY3Rpb25dLCBzaXplICYmIGNsYXNzZXNbJ2Fycm93X19zaXplX18nICsgc2l6ZV0pLFxuXHRcdFx0b25DbGljazogb25DbGljayxcblx0XHRcdG9uVG91Y2hFbmQ6IG9uQ2xpY2tcblx0XHR9LCBwcm9wcyksXG5cdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0ljb24yLmRlZmF1bHQsIHsgZmlsbDogISF0aGVtZS5hcnJvdyAmJiB0aGVtZS5hcnJvdy5maWxsIHx8IF90aGVtZTIuZGVmYXVsdC5hcnJvdy5maWxsLCB0eXBlOiBpY29uIH0pXG5cdCk7XG59XG5cbkFycm93LnByb3BUeXBlcyA9IHtcblx0ZGlyZWN0aW9uOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mKFsnbGVmdCcsICdyaWdodCddKSxcblx0aWNvbjogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG5cdG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuXHRzaXplOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mKFsnbWVkaXVtJywgJ3NtYWxsJ10pLmlzUmVxdWlyZWRcbn07XG5BcnJvdy5kZWZhdWx0UHJvcHMgPSB7XG5cdHNpemU6ICdtZWRpdW0nXG59O1xuQXJyb3cuY29udGV4dFR5cGVzID0ge1xuXHR0aGVtZTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZFxufTtcblxudmFyIGRlZmF1bHRTdHlsZXMgPSB7XG5cdGFycm93OiB7XG5cdFx0YmFja2dyb3VuZDogJ25vbmUnLFxuXHRcdGJvcmRlcjogJ25vbmUnLFxuXHRcdGJvcmRlclJhZGl1czogNCxcblx0XHRjdXJzb3I6ICdwb2ludGVyJyxcblx0XHRvdXRsaW5lOiAnbm9uZScsXG5cdFx0cGFkZGluZzogMTAsIC8vIGluY3JlYXNlIGhpdCBhcmVhXG5cdFx0cG9zaXRpb246ICdhYnNvbHV0ZScsXG5cdFx0dG9wOiAnNTAlJyxcblxuXHRcdC8vIGRpc2FibGUgdXNlciBzZWxlY3Rcblx0XHRXZWJraXRUb3VjaENhbGxvdXQ6ICdub25lJyxcblx0XHR1c2VyU2VsZWN0OiAnbm9uZSdcblx0fSxcblxuXHQvLyBzaXplc1xuXHRhcnJvd19fc2l6ZV9fbWVkaXVtOiB7XG5cdFx0aGVpZ2h0OiBfdGhlbWUyLmRlZmF1bHQuYXJyb3cuaGVpZ2h0LFxuXHRcdG1hcmdpblRvcDogX3RoZW1lMi5kZWZhdWx0LmFycm93LmhlaWdodCAvIC0yLFxuXHRcdHdpZHRoOiA0MCxcblxuXHRcdCdAbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpJzoge1xuXHRcdFx0d2lkdGg6IDcwXG5cdFx0fVxuXHR9LFxuXHRhcnJvd19fc2l6ZV9fc21hbGw6IHtcblx0XHRoZWlnaHQ6IF90aGVtZTIuZGVmYXVsdC50aHVtYm5haWwuc2l6ZSxcblx0XHRtYXJnaW5Ub3A6IF90aGVtZTIuZGVmYXVsdC50aHVtYm5haWwuc2l6ZSAvIC0yLFxuXHRcdHdpZHRoOiAzMCxcblxuXHRcdCdAbWVkaWEgKG1pbi13aWR0aDogNTAwcHgpJzoge1xuXHRcdFx0d2lkdGg6IDQwXG5cdFx0fVxuXHR9LFxuXG5cdC8vIGRpcmVjdGlvblxuXHRhcnJvd19fZGlyZWN0aW9uX19yaWdodDoge1xuXHRcdHJpZ2h0OiBfdGhlbWUyLmRlZmF1bHQuY29udGFpbmVyLmd1dHRlci5ob3Jpem9udGFsXG5cdH0sXG5cdGFycm93X19kaXJlY3Rpb25fX2xlZnQ6IHtcblx0XHRsZWZ0OiBfdGhlbWUyLmRlZmF1bHQuY29udGFpbmVyLmd1dHRlci5ob3Jpem9udGFsXG5cdH1cbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEFycm93OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBWkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBTEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUxBO0FBVUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUF4Q0E7QUE2Q0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Arrow.js
