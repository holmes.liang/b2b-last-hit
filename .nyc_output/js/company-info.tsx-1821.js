__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_useful_form_item__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/useful-form-item */ "./src/app/desk/component/useful-form-item/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_group_customer_company_group_company_info__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component/group-customer-company/group-company-info */ "./src/app/desk/component/group-customer-company/group-company-info.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/company-info.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var formViewItemLayout = {
  selectXsSm: {
    xs: 8,
    sm: 6
  },
  textXsSm: {
    xs: 16,
    sm: 13
  }
};

var CompanyInfo =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(CompanyInfo, _ModelWidget);

  function CompanyInfo(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, CompanyInfo);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(CompanyInfo).call(this, props, context));
    _this.idNoPage = void 0;

    _this.initIdNoRef = function (ref) {
      _this.idNoPage = ref;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(CompanyInfo, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          dataFixed = _this$props.dataFixed,
          notDob = _this$props.notDob,
          propsNameFixed = _this$props.propsNameFixed,
          extraArrObj = _this$props.extraArrObj,
          isMailingAddress = _this$props.isMailingAddress,
          isCedant = _this$props.isCedant,
          countryCode = _this$props.countryCode,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$props, ["model", "form", "dataFixed", "notDob", "propsNameFixed", "extraArrObj", "isMailingAddress", "isCedant", "countryCode"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_group_customer_company_group_company_info__WEBPACK_IMPORTED_MODULE_17__["default"], {
        form: form,
        model: model,
        countryCode: countryCode || "SGP",
        isCedant: isCedant,
        onSelectChange: function onSelectChange(item) {
          _this2.setValuesToModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this2.generatePropName("address"), item.address));

          _this2.idNoPage && _this2.idNoPage.changeCountry(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.countryCode", ""));

          if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item.address, "countryCode") === "SGP") {
            _this2.idNoPage && _this2.idNoPage.idNoPage && _this2.idNoPage.idNoPage.getAddress(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.postalCode"));
          }
        },
        dataId: this.generatePropName(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        form: form,
        model: model,
        propName: this.generatePropName("email"),
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Email").thai("อีเมล์").my("အီးမေးလ်က").getMessage(),
        rules: [{
          required: true,
          message: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Email is invalid").thai("Email is invalid").getMessage(),
          type: "email"
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }), this.props.childDom || "", _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_16__["FieldGroup"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Office No.").thai("Office No.").getMessage(),
        minWidth: "140px",
        selectXsSm: formViewItemLayout.selectXsSm,
        textXsSm: formViewItemLayout.textXsSm,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Nation Code").thai("รหัสประเทศ").my("တိုင်းပြည်ကုဒ်").getMessage(),
        form: form,
        model: model,
        tableName: "nationcode",
        propName: this.generatePropName("officeTelNationCode"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Office").thai("Office").getMessage(),
        propName: this.generatePropName("officeTel"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component_useful_form_item__WEBPACK_IMPORTED_MODULE_15__["Mobile"], {
        form: form,
        model: model,
        required: false,
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Mobile No.").thai("Mobile No.").getMessage(),
        dataId: this.generatePropName(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }), isMailingAddress && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_12__["NFormItem"], {
        form: form,
        model: model,
        propName: this.generatePropName("mailingAddress"),
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("mailing Address").thai("mailing Address").getMessage(),
        checked: true,
        onChange: function onChange(value) {
          _this2.setState({
            isMailingAddress: value === "Y"
          });

          _this2.setValueToModel(value, _this2.generatePropName("mailingAddress"));

          if (value === "N") {
            _this2.setValueToModel({}, _this2.generatePropName("address"));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Checkbox"], {
        style: {
          textTransform: "inherit"
        },
        checked: this.state.isMailingAddress,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("Same as location").thai("Same as location").getMessage())), !this.state.isMailingAddress && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_16__["CommonAddress"], {
        model: model,
        form: form,
        ref: this.initIdNoRef,
        defaultCountryCode: isCedant ? "" : "SGP",
        dataIdOrPrefix: this.generatePropName(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 158
        },
        __self: this
      }));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(CompanyInfo.prototype), "initState", this).call(this), {
        isMailingAddress: this.getValueFromModel(this.generatePropName("mailingAddress")) === "Y"
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      var _this$props2 = this.props,
          dataFixed = _this$props2.dataFixed,
          propsNameFixed = _this$props2.propsNameFixed;
      var extraPropsName = propsNameFixed ? "".concat(propsNameFixed) : "ext.policyholder";
      var propsName = dataFixed ? "".concat(dataFixed, ".").concat(extraPropsName) : extraPropsName;
      if (propName) return "".concat(propsName, ".").concat(propName);
      return "".concat(propsName);
    }
  }]);

  return CompanyInfo;
}(_component__WEBPACK_IMPORTED_MODULE_12__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (CompanyInfo);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL2NvbXBhbnktaW5mby50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGgvY29tcGFueS1pbmZvLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBDaGVja2JveCB9IGZyb20gXCJhbnRkXCI7XG5cbmltcG9ydCB7IE1vZGVsV2lkZ2V0LCBORm9ybUl0ZW0gfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTlNlbGVjdCwgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBNb2JpbGUgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW1cIjtcbmltcG9ydCB7IENvbW1vbkFkZHJlc3MsIEZpZWxkR3JvdXAgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50XCI7XG5pbXBvcnQgR3JvdXBDb21wYW55SW5mbyBmcm9tIFwiQGRlc2stY29tcG9uZW50L2dyb3VwLWN1c3RvbWVyLWNvbXBhbnkvZ3JvdXAtY29tcGFueS1pbmZvXCI7XG5cblxuY29uc3QgZm9ybVZpZXdJdGVtTGF5b3V0ID0ge1xuICBzZWxlY3RYc1NtOiB7XG4gICAgeHM6IDgsXG4gICAgc206IDYsXG4gIH0sXG4gIHRleHRYc1NtOiB7XG4gICAgeHM6IDE2LFxuICAgIHNtOiAxMyxcbiAgfSxcbn07XG50eXBlIFBheWVyU3RhdGUgPSB7XG4gIGlzTWFpbGluZ0FkZHJlc3M6IGJvb2xlYW47XG59O1xuXG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgUGF5ZXJQYWdlQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcbmV4cG9ydCB0eXBlIFBheWVyUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgZGF0YUZpeGVkPzogYW55O1xuICBwcm9wc05hbWVGaXhlZD86IGFueTtcbiAgZXh0cmFBcnJPYmo/OiBhbnlbXTtcbiAgY2hpbGREb20/OiBhbnk7XG4gIG5vdERvYj86IGJvb2xlYW47XG4gIGNvdW50cnlDb2RlPzogYW55O1xuICBpc0NlZGFudD86IGJvb2xlYW47XG4gIGlzTWFpbGluZ0FkZHJlc3M/OiBib29sZWFuO1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmNsYXNzIENvbXBhbnlJbmZvPFAgZXh0ZW5kcyBQYXllclByb3BzLCBTIGV4dGVuZHMgUGF5ZXJTdGF0ZSwgQyBleHRlbmRzIFBheWVyUGFnZUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwdWJsaWMgaWROb1BhZ2U6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogUGF5ZXJQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICB9XG5cbiAgaW5pdElkTm9SZWYgPSAocmVmOiBhbnkpID0+IHtcbiAgICB0aGlzLmlkTm9QYWdlID0gcmVmO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgZGF0YUZpeGVkLCBub3REb2IsIHByb3BzTmFtZUZpeGVkLCBleHRyYUFyck9iaiwgaXNNYWlsaW5nQWRkcmVzcywgaXNDZWRhbnQsIGNvdW50cnlDb2RlLCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hDb250ZW50IHsuLi5yZXN0fT5cbiAgICAgICAgPEdyb3VwQ29tcGFueUluZm8gZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudHJ5Q29kZT17Y291bnRyeUNvZGUgfHwgXCJTR1BcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgaXNDZWRhbnQ9e2lzQ2VkYW50fVxuICAgICAgICAgICAgICAgICAgICAgICAgICBvblNlbGVjdENoYW5nZT17KGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1wiKV06IGl0ZW0uYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlkTm9QYWdlICYmIHRoaXMuaWROb1BhZ2UuY2hhbmdlQ291bnRyeShfLmdldChpdGVtLCBcImFkZHJlc3MuY291bnRyeUNvZGVcIiwgXCJcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChfLmdldChpdGVtLmFkZHJlc3MsIFwiY291bnRyeUNvZGVcIikgPT09IFwiU0dQXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaWROb1BhZ2UgJiYgdGhpcy5pZE5vUGFnZS5pZE5vUGFnZSAmJiB0aGlzLmlkTm9QYWdlLmlkTm9QYWdlLmdldEFkZHJlc3MoXy5nZXQoaXRlbSwgXCJhZGRyZXNzLnBvc3RhbENvZGVcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YUlkPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoKX0vPlxuICAgICAgICA8TlRleHRcbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiZW1haWxcIil9XG4gICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJFbWFpbFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguK3guLXguYDguKHguKXguYxcIilcbiAgICAgICAgICAgIC5teShcIuGAoeGAruGAuOGAmeGAseGAuOGAnOGAuuGAgFwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgbWVzc2FnZTogTGFuZ3VhZ2UuZW4oXCJFbWFpbCBpcyBpbnZhbGlkXCIpXG4gICAgICAgICAgICAgICAgLnRoYWkoXCJFbWFpbCBpcyBpbnZhbGlkXCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgICAgdHlwZTogXCJlbWFpbFwiLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdfVxuICAgICAgICAvPlxuICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZERvbSB8fCBcIlwifVxuICAgICAgICA8RmllbGRHcm91cFxuICAgICAgICAgIGNsYXNzTmFtZT1cInVzYWdlLWdyb3VwXCJcbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2VcbiAgICAgICAgICAgIC5lbihcIk9mZmljZSBOby5cIilcbiAgICAgICAgICAgIC50aGFpKFwiT2ZmaWNlIE5vLlwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBtaW5XaWR0aD1cIjE0MHB4XCJcbiAgICAgICAgICBzZWxlY3RYc1NtPXtmb3JtVmlld0l0ZW1MYXlvdXQuc2VsZWN0WHNTbX1cbiAgICAgICAgICB0ZXh0WHNTbT17Zm9ybVZpZXdJdGVtTGF5b3V0LnRleHRYc1NtfVxuICAgICAgICA+XG4gICAgICAgICAgPE5TZWxlY3RcbiAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk5hdGlvbiBDb2RlXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4Lij4Lir4Lix4Liq4Lib4Lij4Liw4LmA4LiX4LioXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAkOGAreGAr+GAhOGAuuGAuOGAleGAvOGAiuGAuuGAgOGAr+GAkuGAulwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIHRhYmxlTmFtZT1cIm5hdGlvbmNvZGVcIlxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm9mZmljZVRlbE5hdGlvbkNvZGVcIil9XG4gICAgICAgICAgLz5cblxuICAgICAgICAgIDxOVGV4dFxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk9mZmljZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIk9mZmljZVwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm9mZmljZVRlbFwiKX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICAgIDxNb2JpbGUgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ9e2ZhbHNlfVxuICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk1vYmlsZSBOby5cIikudGhhaShcIk1vYmlsZSBOby5cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgIGRhdGFJZD17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKCl9Lz5cbiAgICAgICAge2lzTWFpbGluZ0FkZHJlc3MgJiYgPE5Gb3JtSXRlbSBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibWFpbGluZ0FkZHJlc3NcIil9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwibWFpbGluZyBBZGRyZXNzXCIpLnRoYWkoXCJtYWlsaW5nIEFkZHJlc3NcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNNYWlsaW5nQWRkcmVzczogdmFsdWUgPT09IFwiWVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbCh2YWx1ZSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibWFpbGluZ0FkZHJlc3NcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlID09PSBcIk5cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbCh7fSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1wiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxDaGVja2JveFxuICAgICAgICAgICAgc3R5bGU9e3sgdGV4dFRyYW5zZm9ybTogXCJpbmhlcml0XCIgfX1cbiAgICAgICAgICAgIGNoZWNrZWQ9e3RoaXMuc3RhdGUuaXNNYWlsaW5nQWRkcmVzc30+XG4gICAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTYW1lIGFzIGxvY2F0aW9uXCIpLnRoYWkoXCJTYW1lIGFzIGxvY2F0aW9uXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA8L0NoZWNrYm94PlxuICAgICAgICA8L05Gb3JtSXRlbT59XG5cbiAgICAgICAgeyghdGhpcy5zdGF0ZS5pc01haWxpbmdBZGRyZXNzKSAmJiA8Q29tbW9uQWRkcmVzcyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWY9e3RoaXMuaW5pdElkTm9SZWZ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdENvdW50cnlDb2RlPXtpc0NlZGFudCA/IFwiXCIgOiBcIlNHUFwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFJZE9yUHJlZml4PXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoKX1cbiAgICAgICAgLz59XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBpc01haWxpbmdBZGRyZXNzOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm1haWxpbmdBZGRyZXNzXCIpKSA9PT0gXCJZXCIsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByaXZhdGUgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZT86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgY29uc3QgeyBkYXRhRml4ZWQsIHByb3BzTmFtZUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGV4dHJhUHJvcHNOYW1lID0gcHJvcHNOYW1lRml4ZWQgPyBgJHtwcm9wc05hbWVGaXhlZH1gIDogYGV4dC5wb2xpY3lob2xkZXJgO1xuICAgIGNvbnN0IHByb3BzTmFtZSA9IGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0uJHtleHRyYVByb3BzTmFtZX1gIDogZXh0cmFQcm9wc05hbWU7XG4gICAgaWYgKHByb3BOYW1lKSByZXR1cm4gYCR7cHJvcHNOYW1lfS4ke3Byb3BOYW1lfWA7XG4gICAgcmV0dXJuIGAke3Byb3BzTmFtZX1gO1xuICB9XG59XG5cblxuZXhwb3J0IGRlZmF1bHQgQ29tcGFueUluZm87XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBTEE7QUFDQTtBQStCQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFRQTtBQUNBO0FBQ0E7QUFWQTtBQUVBO0FBQ0E7OztBQUNBOzs7QUFPQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBRUE7QUFDQTtBQUdBO0FBTEE7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFDQTtBQUdBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQUE7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUE1SUE7QUFDQTtBQStJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/company-info.tsx
