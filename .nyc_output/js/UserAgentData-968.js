/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

/**
 * Usage note:
 * This module makes a best effort to export the same data we would internally.
 * At Facebook we use a server-generated module that does the parsing and
 * exports the data for the client to use. We can't rely on a server-side
 * implementation in open source so instead we make use of an open source
 * library to do the heavy lifting and then make some adjustments as necessary.
 * It's likely there will be some differences. Some we can smooth over.
 * Others are going to be harder.
 */


var UAParser = __webpack_require__(/*! ua-parser-js */ "./node_modules/ua-parser-js/src/ua-parser.js");

var UNKNOWN = 'Unknown';
var PLATFORM_MAP = {
  'Mac OS': 'Mac OS X'
};
/**
 * Convert from UAParser platform name to what we expect.
 */

function convertPlatformName(name) {
  return PLATFORM_MAP[name] || name;
}
/**
 * Get the version number in parts. This is very naive. We actually get major
 * version as a part of UAParser already, which is generally good enough, but
 * let's get the minor just in case.
 */


function getBrowserVersion(version) {
  if (!version) {
    return {
      major: '',
      minor: ''
    };
  }

  var parts = version.split('.');
  return {
    major: parts[0],
    minor: parts[1]
  };
}
/**
 * Get the UA data fom UAParser and then convert it to the format we're
 * expecting for our APIS.
 */


var parser = new UAParser();
var results = parser.getResult(); // Do some conversion first.

var browserVersionData = getBrowserVersion(results.browser.version);
var uaData = {
  browserArchitecture: results.cpu.architecture || UNKNOWN,
  browserFullVersion: results.browser.version || UNKNOWN,
  browserMinorVersion: browserVersionData.minor || UNKNOWN,
  browserName: results.browser.name || UNKNOWN,
  browserVersion: results.browser.major || UNKNOWN,
  deviceName: results.device.model || UNKNOWN,
  engineName: results.engine.name || UNKNOWN,
  engineVersion: results.engine.version || UNKNOWN,
  platformArchitecture: results.cpu.architecture || UNKNOWN,
  platformName: convertPlatformName(results.os.name) || UNKNOWN,
  platformVersion: results.os.version || UNKNOWN,
  platformFullVersion: results.os.version || UNKNOWN
};
module.exports = uaData;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVXNlckFnZW50RGF0YS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2ZianMvbGliL1VzZXJBZ2VudERhdGEuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKi9cblxuLyoqXG4gKiBVc2FnZSBub3RlOlxuICogVGhpcyBtb2R1bGUgbWFrZXMgYSBiZXN0IGVmZm9ydCB0byBleHBvcnQgdGhlIHNhbWUgZGF0YSB3ZSB3b3VsZCBpbnRlcm5hbGx5LlxuICogQXQgRmFjZWJvb2sgd2UgdXNlIGEgc2VydmVyLWdlbmVyYXRlZCBtb2R1bGUgdGhhdCBkb2VzIHRoZSBwYXJzaW5nIGFuZFxuICogZXhwb3J0cyB0aGUgZGF0YSBmb3IgdGhlIGNsaWVudCB0byB1c2UuIFdlIGNhbid0IHJlbHkgb24gYSBzZXJ2ZXItc2lkZVxuICogaW1wbGVtZW50YXRpb24gaW4gb3BlbiBzb3VyY2Ugc28gaW5zdGVhZCB3ZSBtYWtlIHVzZSBvZiBhbiBvcGVuIHNvdXJjZVxuICogbGlicmFyeSB0byBkbyB0aGUgaGVhdnkgbGlmdGluZyBhbmQgdGhlbiBtYWtlIHNvbWUgYWRqdXN0bWVudHMgYXMgbmVjZXNzYXJ5LlxuICogSXQncyBsaWtlbHkgdGhlcmUgd2lsbCBiZSBzb21lIGRpZmZlcmVuY2VzLiBTb21lIHdlIGNhbiBzbW9vdGggb3Zlci5cbiAqIE90aGVycyBhcmUgZ29pbmcgdG8gYmUgaGFyZGVyLlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVBUGFyc2VyID0gcmVxdWlyZSgndWEtcGFyc2VyLWpzJyk7XG5cbnZhciBVTktOT1dOID0gJ1Vua25vd24nO1xuXG52YXIgUExBVEZPUk1fTUFQID0ge1xuICAnTWFjIE9TJzogJ01hYyBPUyBYJ1xufTtcblxuLyoqXG4gKiBDb252ZXJ0IGZyb20gVUFQYXJzZXIgcGxhdGZvcm0gbmFtZSB0byB3aGF0IHdlIGV4cGVjdC5cbiAqL1xuZnVuY3Rpb24gY29udmVydFBsYXRmb3JtTmFtZShuYW1lKSB7XG4gIHJldHVybiBQTEFURk9STV9NQVBbbmFtZV0gfHwgbmFtZTtcbn1cblxuLyoqXG4gKiBHZXQgdGhlIHZlcnNpb24gbnVtYmVyIGluIHBhcnRzLiBUaGlzIGlzIHZlcnkgbmFpdmUuIFdlIGFjdHVhbGx5IGdldCBtYWpvclxuICogdmVyc2lvbiBhcyBhIHBhcnQgb2YgVUFQYXJzZXIgYWxyZWFkeSwgd2hpY2ggaXMgZ2VuZXJhbGx5IGdvb2QgZW5vdWdoLCBidXRcbiAqIGxldCdzIGdldCB0aGUgbWlub3IganVzdCBpbiBjYXNlLlxuICovXG5mdW5jdGlvbiBnZXRCcm93c2VyVmVyc2lvbih2ZXJzaW9uKSB7XG4gIGlmICghdmVyc2lvbikge1xuICAgIHJldHVybiB7XG4gICAgICBtYWpvcjogJycsXG4gICAgICBtaW5vcjogJydcbiAgICB9O1xuICB9XG4gIHZhciBwYXJ0cyA9IHZlcnNpb24uc3BsaXQoJy4nKTtcbiAgcmV0dXJuIHtcbiAgICBtYWpvcjogcGFydHNbMF0sXG4gICAgbWlub3I6IHBhcnRzWzFdXG4gIH07XG59XG5cbi8qKlxuICogR2V0IHRoZSBVQSBkYXRhIGZvbSBVQVBhcnNlciBhbmQgdGhlbiBjb252ZXJ0IGl0IHRvIHRoZSBmb3JtYXQgd2UncmVcbiAqIGV4cGVjdGluZyBmb3Igb3VyIEFQSVMuXG4gKi9cbnZhciBwYXJzZXIgPSBuZXcgVUFQYXJzZXIoKTtcbnZhciByZXN1bHRzID0gcGFyc2VyLmdldFJlc3VsdCgpO1xuXG4vLyBEbyBzb21lIGNvbnZlcnNpb24gZmlyc3QuXG52YXIgYnJvd3NlclZlcnNpb25EYXRhID0gZ2V0QnJvd3NlclZlcnNpb24ocmVzdWx0cy5icm93c2VyLnZlcnNpb24pO1xudmFyIHVhRGF0YSA9IHtcbiAgYnJvd3NlckFyY2hpdGVjdHVyZTogcmVzdWx0cy5jcHUuYXJjaGl0ZWN0dXJlIHx8IFVOS05PV04sXG4gIGJyb3dzZXJGdWxsVmVyc2lvbjogcmVzdWx0cy5icm93c2VyLnZlcnNpb24gfHwgVU5LTk9XTixcbiAgYnJvd3Nlck1pbm9yVmVyc2lvbjogYnJvd3NlclZlcnNpb25EYXRhLm1pbm9yIHx8IFVOS05PV04sXG4gIGJyb3dzZXJOYW1lOiByZXN1bHRzLmJyb3dzZXIubmFtZSB8fCBVTktOT1dOLFxuICBicm93c2VyVmVyc2lvbjogcmVzdWx0cy5icm93c2VyLm1ham9yIHx8IFVOS05PV04sXG4gIGRldmljZU5hbWU6IHJlc3VsdHMuZGV2aWNlLm1vZGVsIHx8IFVOS05PV04sXG4gIGVuZ2luZU5hbWU6IHJlc3VsdHMuZW5naW5lLm5hbWUgfHwgVU5LTk9XTixcbiAgZW5naW5lVmVyc2lvbjogcmVzdWx0cy5lbmdpbmUudmVyc2lvbiB8fCBVTktOT1dOLFxuICBwbGF0Zm9ybUFyY2hpdGVjdHVyZTogcmVzdWx0cy5jcHUuYXJjaGl0ZWN0dXJlIHx8IFVOS05PV04sXG4gIHBsYXRmb3JtTmFtZTogY29udmVydFBsYXRmb3JtTmFtZShyZXN1bHRzLm9zLm5hbWUpIHx8IFVOS05PV04sXG4gIHBsYXRmb3JtVmVyc2lvbjogcmVzdWx0cy5vcy52ZXJzaW9uIHx8IFVOS05PV04sXG4gIHBsYXRmb3JtRnVsbFZlcnNpb246IHJlc3VsdHMub3MudmVyc2lvbiB8fCBVTktOT1dOXG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IHVhRGF0YTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7OztBQVFBOzs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUlBOzs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/UserAgentData.js
