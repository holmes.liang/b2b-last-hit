/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getRangesForDraftEntity
 * @format
 * 
 */


var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");
/**
 * Obtain the start and end positions of the range that has the
 * specified entity applied to it.
 *
 * Entity keys are applied only to contiguous stretches of text, so this
 * method searches for the first instance of the entity key and returns
 * the subsequent range.
 */


function getRangesForDraftEntity(block, key) {
  var ranges = [];
  block.findEntityRanges(function (c) {
    return c.getEntity() === key;
  }, function (start, end) {
    ranges.push({
      start: start,
      end: end
    });
  });
  !!!ranges.length ?  true ? invariant(false, 'Entity key not found in this range.') : undefined : void 0;
  return ranges;
}

module.exports = getRangesForDraftEntity;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFJhbmdlc0ZvckRyYWZ0RW50aXR5LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFJhbmdlc0ZvckRyYWZ0RW50aXR5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0UmFuZ2VzRm9yRHJhZnRFbnRpdHlcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG4vKipcbiAqIE9idGFpbiB0aGUgc3RhcnQgYW5kIGVuZCBwb3NpdGlvbnMgb2YgdGhlIHJhbmdlIHRoYXQgaGFzIHRoZVxuICogc3BlY2lmaWVkIGVudGl0eSBhcHBsaWVkIHRvIGl0LlxuICpcbiAqIEVudGl0eSBrZXlzIGFyZSBhcHBsaWVkIG9ubHkgdG8gY29udGlndW91cyBzdHJldGNoZXMgb2YgdGV4dCwgc28gdGhpc1xuICogbWV0aG9kIHNlYXJjaGVzIGZvciB0aGUgZmlyc3QgaW5zdGFuY2Ugb2YgdGhlIGVudGl0eSBrZXkgYW5kIHJldHVybnNcbiAqIHRoZSBzdWJzZXF1ZW50IHJhbmdlLlxuICovXG5mdW5jdGlvbiBnZXRSYW5nZXNGb3JEcmFmdEVudGl0eShibG9jaywga2V5KSB7XG4gIHZhciByYW5nZXMgPSBbXTtcbiAgYmxvY2suZmluZEVudGl0eVJhbmdlcyhmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiBjLmdldEVudGl0eSgpID09PSBrZXk7XG4gIH0sIGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgcmFuZ2VzLnB1c2goeyBzdGFydDogc3RhcnQsIGVuZDogZW5kIH0pO1xuICB9KTtcblxuICAhISFyYW5nZXMubGVuZ3RoID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0VudGl0eSBrZXkgbm90IGZvdW5kIGluIHRoaXMgcmFuZ2UuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIHJldHVybiByYW5nZXM7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0UmFuZ2VzRm9yRHJhZnRFbnRpdHk7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getRangesForDraftEntity.js
