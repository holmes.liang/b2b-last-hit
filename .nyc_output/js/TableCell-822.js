

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var get_1 = __importDefault(__webpack_require__(/*! lodash/get */ "./node_modules/lodash/get.js"));

function isInvalidRenderCellText(text) {
  return text && !React.isValidElement(text) && Object.prototype.toString.call(text) === '[object Object]';
}

var TableCell =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TableCell, _React$Component);

  function TableCell() {
    var _this;

    _classCallCheck(this, TableCell);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TableCell).apply(this, arguments));

    _this.handleClick = function (e) {
      var _this$props = _this.props,
          record = _this$props.record,
          onCellClick = _this$props.column.onCellClick;

      if (onCellClick) {
        onCellClick(record, e);
      }
    };

    return _this;
  }

  _createClass(TableCell, [{
    key: "render",
    value: function render() {
      var _classnames_1$default;

      var _this$props2 = this.props,
          record = _this$props2.record,
          indentSize = _this$props2.indentSize,
          prefixCls = _this$props2.prefixCls,
          indent = _this$props2.indent,
          index = _this$props2.index,
          expandIcon = _this$props2.expandIcon,
          column = _this$props2.column,
          BodyCell = _this$props2.component;
      var dataIndex = column.dataIndex,
          render = column.render,
          _column$className = column.className,
          className = _column$className === void 0 ? '' : _column$className; // We should return undefined if no dataIndex is specified, but in order to
      // be compatible with object-path's behavior, we return the record object instead.

      var text;

      if (typeof dataIndex === 'number') {
        text = get_1.default(record, dataIndex);
      } else if (!dataIndex || dataIndex.length === 0) {
        text = record;
      } else {
        text = get_1.default(record, dataIndex);
      }

      var tdProps = {};
      var colSpan;
      var rowSpan;

      if (render) {
        text = render(text, record, index); // `render` support cell with additional config like `props`

        if (isInvalidRenderCellText(text)) {
          tdProps = text.props || tdProps;
          var _tdProps = tdProps;
          colSpan = _tdProps.colSpan;
          rowSpan = _tdProps.rowSpan;
          text = text.children;
        }
      }

      if (column.onCell) {
        tdProps = _objectSpread({}, tdProps, {}, column.onCell(record, index));
      } // Fix https://github.com/ant-design/ant-design/issues/1202


      if (isInvalidRenderCellText(text)) {
        text = null;
      }

      var indentText = expandIcon ? React.createElement("span", {
        style: {
          paddingLeft: "".concat(indentSize * indent, "px")
        },
        className: "".concat(prefixCls, "-indent indent-level-").concat(indent)
      }) : null;

      if (rowSpan === 0 || colSpan === 0) {
        return null;
      }

      if (column.align) {
        tdProps.style = _objectSpread({
          textAlign: column.align
        }, tdProps.style);
      }

      var cellClassName = classnames_1.default(className, (_classnames_1$default = {}, _defineProperty(_classnames_1$default, "".concat(prefixCls, "-cell-ellipsis"), !!column.ellipsis), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-cell-break-word"), !!column.width), _classnames_1$default));

      if (column.ellipsis) {
        if (typeof text === 'string') {
          tdProps.title = text;
        } else if (text) {
          var _text = text,
              textProps = _text.props;

          if (textProps && textProps.children && typeof textProps.children === 'string') {
            tdProps.title = textProps.children;
          }
        }
      }

      return React.createElement(BodyCell, Object.assign({
        className: cellClassName,
        onClick: this.handleClick
      }, tdProps), indentText, expandIcon, text);
    }
  }]);

  return TableCell;
}(React.Component);

exports.default = TableCell;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvVGFibGVDZWxsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvVGFibGVDZWxsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBfdHlwZW9mKG9iaikgeyBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9OyB9IGVsc2UgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07IH0gcmV0dXJuIF90eXBlb2Yob2JqKTsgfVxuXG5mdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSksIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgY2xhc3NuYW1lc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJjbGFzc25hbWVzXCIpKTtcblxudmFyIGdldF8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJsb2Rhc2gvZ2V0XCIpKTtcblxuZnVuY3Rpb24gaXNJbnZhbGlkUmVuZGVyQ2VsbFRleHQodGV4dCkge1xuICByZXR1cm4gdGV4dCAmJiAhUmVhY3QuaXNWYWxpZEVsZW1lbnQodGV4dCkgJiYgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHRleHQpID09PSAnW29iamVjdCBPYmplY3RdJztcbn1cblxudmFyIFRhYmxlQ2VsbCA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoVGFibGVDZWxsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUYWJsZUNlbGwoKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFRhYmxlQ2VsbCk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihUYWJsZUNlbGwpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuXG4gICAgX3RoaXMuaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgcmVjb3JkID0gX3RoaXMkcHJvcHMucmVjb3JkLFxuICAgICAgICAgIG9uQ2VsbENsaWNrID0gX3RoaXMkcHJvcHMuY29sdW1uLm9uQ2VsbENsaWNrO1xuXG4gICAgICBpZiAob25DZWxsQ2xpY2spIHtcbiAgICAgICAgb25DZWxsQ2xpY2socmVjb3JkLCBlKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKFRhYmxlQ2VsbCwgW3tcbiAgICBrZXk6IFwicmVuZGVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NuYW1lc18xJGRlZmF1bHQ7XG5cbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIHJlY29yZCA9IF90aGlzJHByb3BzMi5yZWNvcmQsXG4gICAgICAgICAgaW5kZW50U2l6ZSA9IF90aGlzJHByb3BzMi5pbmRlbnRTaXplLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzMi5wcmVmaXhDbHMsXG4gICAgICAgICAgaW5kZW50ID0gX3RoaXMkcHJvcHMyLmluZGVudCxcbiAgICAgICAgICBpbmRleCA9IF90aGlzJHByb3BzMi5pbmRleCxcbiAgICAgICAgICBleHBhbmRJY29uID0gX3RoaXMkcHJvcHMyLmV4cGFuZEljb24sXG4gICAgICAgICAgY29sdW1uID0gX3RoaXMkcHJvcHMyLmNvbHVtbixcbiAgICAgICAgICBCb2R5Q2VsbCA9IF90aGlzJHByb3BzMi5jb21wb25lbnQ7XG4gICAgICB2YXIgZGF0YUluZGV4ID0gY29sdW1uLmRhdGFJbmRleCxcbiAgICAgICAgICByZW5kZXIgPSBjb2x1bW4ucmVuZGVyLFxuICAgICAgICAgIF9jb2x1bW4kY2xhc3NOYW1lID0gY29sdW1uLmNsYXNzTmFtZSxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfY29sdW1uJGNsYXNzTmFtZSA9PT0gdm9pZCAwID8gJycgOiBfY29sdW1uJGNsYXNzTmFtZTsgLy8gV2Ugc2hvdWxkIHJldHVybiB1bmRlZmluZWQgaWYgbm8gZGF0YUluZGV4IGlzIHNwZWNpZmllZCwgYnV0IGluIG9yZGVyIHRvXG4gICAgICAvLyBiZSBjb21wYXRpYmxlIHdpdGggb2JqZWN0LXBhdGgncyBiZWhhdmlvciwgd2UgcmV0dXJuIHRoZSByZWNvcmQgb2JqZWN0IGluc3RlYWQuXG5cbiAgICAgIHZhciB0ZXh0O1xuXG4gICAgICBpZiAodHlwZW9mIGRhdGFJbmRleCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgdGV4dCA9IGdldF8xLmRlZmF1bHQocmVjb3JkLCBkYXRhSW5kZXgpO1xuICAgICAgfSBlbHNlIGlmICghZGF0YUluZGV4IHx8IGRhdGFJbmRleC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgdGV4dCA9IHJlY29yZDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRleHQgPSBnZXRfMS5kZWZhdWx0KHJlY29yZCwgZGF0YUluZGV4KTtcbiAgICAgIH1cblxuICAgICAgdmFyIHRkUHJvcHMgPSB7fTtcbiAgICAgIHZhciBjb2xTcGFuO1xuICAgICAgdmFyIHJvd1NwYW47XG5cbiAgICAgIGlmIChyZW5kZXIpIHtcbiAgICAgICAgdGV4dCA9IHJlbmRlcih0ZXh0LCByZWNvcmQsIGluZGV4KTsgLy8gYHJlbmRlcmAgc3VwcG9ydCBjZWxsIHdpdGggYWRkaXRpb25hbCBjb25maWcgbGlrZSBgcHJvcHNgXG5cbiAgICAgICAgaWYgKGlzSW52YWxpZFJlbmRlckNlbGxUZXh0KHRleHQpKSB7XG4gICAgICAgICAgdGRQcm9wcyA9IHRleHQucHJvcHMgfHwgdGRQcm9wcztcbiAgICAgICAgICB2YXIgX3RkUHJvcHMgPSB0ZFByb3BzO1xuICAgICAgICAgIGNvbFNwYW4gPSBfdGRQcm9wcy5jb2xTcGFuO1xuICAgICAgICAgIHJvd1NwYW4gPSBfdGRQcm9wcy5yb3dTcGFuO1xuICAgICAgICAgIHRleHQgPSB0ZXh0LmNoaWxkcmVuO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChjb2x1bW4ub25DZWxsKSB7XG4gICAgICAgIHRkUHJvcHMgPSBfb2JqZWN0U3ByZWFkKHt9LCB0ZFByb3BzLCB7fSwgY29sdW1uLm9uQ2VsbChyZWNvcmQsIGluZGV4KSk7XG4gICAgICB9IC8vIEZpeCBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMjAyXG5cblxuICAgICAgaWYgKGlzSW52YWxpZFJlbmRlckNlbGxUZXh0KHRleHQpKSB7XG4gICAgICAgIHRleHQgPSBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW5kZW50VGV4dCA9IGV4cGFuZEljb24gPyBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7XG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgcGFkZGluZ0xlZnQ6IFwiXCIuY29uY2F0KGluZGVudFNpemUgKiBpbmRlbnQsIFwicHhcIilcbiAgICAgICAgfSxcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWluZGVudCBpbmRlbnQtbGV2ZWwtXCIpLmNvbmNhdChpbmRlbnQpXG4gICAgICB9KSA6IG51bGw7XG5cbiAgICAgIGlmIChyb3dTcGFuID09PSAwIHx8IGNvbFNwYW4gPT09IDApIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIGlmIChjb2x1bW4uYWxpZ24pIHtcbiAgICAgICAgdGRQcm9wcy5zdHlsZSA9IF9vYmplY3RTcHJlYWQoe1xuICAgICAgICAgIHRleHRBbGlnbjogY29sdW1uLmFsaWduXG4gICAgICAgIH0sIHRkUHJvcHMuc3R5bGUpO1xuICAgICAgfVxuXG4gICAgICB2YXIgY2VsbENsYXNzTmFtZSA9IGNsYXNzbmFtZXNfMS5kZWZhdWx0KGNsYXNzTmFtZSwgKF9jbGFzc25hbWVzXzEkZGVmYXVsdCA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXNfMSRkZWZhdWx0LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWNlbGwtZWxsaXBzaXNcIiksICEhY29sdW1uLmVsbGlwc2lzKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzXzEkZGVmYXVsdCwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jZWxsLWJyZWFrLXdvcmRcIiksICEhY29sdW1uLndpZHRoKSwgX2NsYXNzbmFtZXNfMSRkZWZhdWx0KSk7XG5cbiAgICAgIGlmIChjb2x1bW4uZWxsaXBzaXMpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB0ZXh0ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIHRkUHJvcHMudGl0bGUgPSB0ZXh0O1xuICAgICAgICB9IGVsc2UgaWYgKHRleHQpIHtcbiAgICAgICAgICB2YXIgX3RleHQgPSB0ZXh0LFxuICAgICAgICAgICAgICB0ZXh0UHJvcHMgPSBfdGV4dC5wcm9wcztcblxuICAgICAgICAgIGlmICh0ZXh0UHJvcHMgJiYgdGV4dFByb3BzLmNoaWxkcmVuICYmIHR5cGVvZiB0ZXh0UHJvcHMuY2hpbGRyZW4gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICB0ZFByb3BzLnRpdGxlID0gdGV4dFByb3BzLmNoaWxkcmVuO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChCb2R5Q2VsbCwgT2JqZWN0LmFzc2lnbih7XG4gICAgICAgIGNsYXNzTmFtZTogY2VsbENsYXNzTmFtZSxcbiAgICAgICAgb25DbGljazogdGhpcy5oYW5kbGVDbGlja1xuICAgICAgfSwgdGRQcm9wcyksIGluZGVudFRleHQsIGV4cGFuZEljb24sIHRleHQpO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBUYWJsZUNlbGw7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFRhYmxlQ2VsbDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUEzRkE7QUFDQTtBQTZGQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/TableCell.js
