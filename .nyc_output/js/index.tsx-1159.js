__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUrlInfo", function() { return getUrlInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "writeSyslog", function() { return writeSyslog; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_ajax_before__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/ajax-before */ "./src/common/ajax-before.ts");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");





function getBrowserInfo() {
  var clientInfo = Object(_common_ajax_before__WEBPACK_IMPORTED_MODULE_2__["getClientInfo"])();
  return {
    clientOs: clientInfo["x-insmate-client-os"],
    browser: clientInfo["x-insmate-client-browser"],
    browserVersion: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(navigator, "appVersion") //(KHTML, like Gecko) Version/12.1.2

  };
}

function getUrlInfo() {
  var windowProps = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
  return {
    domainName: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(windowProps, "location.host"),
    pageUrl: lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(windowProps, "location.pathname")
  };
}
function writeSyslog() {
  var syslogArgument = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var syslog = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, getBrowserInfo(), {}, getUrlInfo(), {}, syslogArgument);

  console.log("ISyslog: ", syslog);
  _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].post("/syslogs/write", syslog);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3N5cy1sb2cvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL3N5cy1sb2cvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IGdldENsaWVudEluZm8gfSBmcm9tIFwiQGNvbW1vbi9hamF4LWJlZm9yZVwiO1xuaW1wb3J0IHsgSVN5c2xvZyB9IGZyb20gXCJAY29tbW9uL3N5cy1sb2cvdHlwZXNcIjtcbmltcG9ydCB7IEFqYXggfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5cbmZ1bmN0aW9uIGdldEJyb3dzZXJJbmZvKCkge1xuICBjb25zdCBjbGllbnRJbmZvID0gZ2V0Q2xpZW50SW5mbygpO1xuICByZXR1cm4ge1xuICAgIGNsaWVudE9zOiBjbGllbnRJbmZvW1wieC1pbnNtYXRlLWNsaWVudC1vc1wiXSxcbiAgICBicm93c2VyOiBjbGllbnRJbmZvW1wieC1pbnNtYXRlLWNsaWVudC1icm93c2VyXCJdLFxuICAgIGJyb3dzZXJWZXJzaW9uOiBfLmdldChuYXZpZ2F0b3IsIFwiYXBwVmVyc2lvblwiKSwgLy8oS0hUTUwsIGxpa2UgR2Vja28pIFZlcnNpb24vMTIuMS4yXG4gIH07XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFVybEluZm8od2luZG93UHJvcHM6IGFueSA9IHdpbmRvdykge1xuICByZXR1cm4ge1xuICAgIGRvbWFpbk5hbWU6IF8uZ2V0KHdpbmRvd1Byb3BzLCBcImxvY2F0aW9uLmhvc3RcIiksXG4gICAgcGFnZVVybDogXy5nZXQod2luZG93UHJvcHMsIFwibG9jYXRpb24ucGF0aG5hbWVcIiksXG4gIH07XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIHdyaXRlU3lzbG9nKHN5c2xvZ0FyZ3VtZW50OiBJU3lzbG9nID0ge30pIHtcbiAgY29uc3Qgc3lzbG9nOiBJU3lzbG9nID0ge1xuICAgIC4uLmdldEJyb3dzZXJJbmZvKCksXG4gICAgLi4uZ2V0VXJsSW5mbygpLFxuICAgIC4uLnN5c2xvZ0FyZ3VtZW50LFxuICB9O1xuICBjb25zb2xlLmxvZyhcIklTeXNsb2c6IFwiLCBzeXNsb2cpO1xuICBBamF4LnBvc3QoXCIvc3lzbG9ncy93cml0ZVwiLCBzeXNsb2cpO1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/common/sys-log/index.tsx
