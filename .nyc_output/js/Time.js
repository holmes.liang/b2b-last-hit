/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var numberUtil = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var formatUtil = __webpack_require__(/*! ../util/format */ "./node_modules/echarts/lib/util/format.js");

var scaleHelper = __webpack_require__(/*! ./helper */ "./node_modules/echarts/lib/scale/helper.js");

var IntervalScale = __webpack_require__(/*! ./Interval */ "./node_modules/echarts/lib/scale/Interval.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* A third-party license is embeded for some of the code in this file:
* The "scaleLevels" was originally copied from "d3.js" with some
* modifications made for this project.
* (See more details in the comment on the definition of "scaleLevels" below.)
* The use of the source code of this file is also subject to the terms
* and consitions of the license of "d3.js" (BSD-3Clause, see
* </licenses/LICENSE-d3>).
*/
// [About UTC and local time zone]:
// In most cases, `number.parseDate` will treat input data string as local time
// (except time zone is specified in time string). And `format.formateTime` returns
// local time by default. option.useUTC is false by default. This design have
// concidered these common case:
// (1) Time that is persistent in server is in UTC, but it is needed to be diplayed
// in local time by default.
// (2) By default, the input data string (e.g., '2011-01-02') should be displayed
// as its original time, without any time difference.


var intervalScaleProto = IntervalScale.prototype;
var mathCeil = Math.ceil;
var mathFloor = Math.floor;
var ONE_SECOND = 1000;
var ONE_MINUTE = ONE_SECOND * 60;
var ONE_HOUR = ONE_MINUTE * 60;
var ONE_DAY = ONE_HOUR * 24; // FIXME 公用？

var bisect = function bisect(a, x, lo, hi) {
  while (lo < hi) {
    var mid = lo + hi >>> 1;

    if (a[mid][1] < x) {
      lo = mid + 1;
    } else {
      hi = mid;
    }
  }

  return lo;
};
/**
 * @alias module:echarts/coord/scale/Time
 * @constructor
 */


var TimeScale = IntervalScale.extend({
  type: 'time',

  /**
   * @override
   */
  getLabel: function getLabel(val) {
    var stepLvl = this._stepLvl;
    var date = new Date(val);
    return formatUtil.formatTime(stepLvl[0], date, this.getSetting('useUTC'));
  },

  /**
   * @override
   */
  niceExtent: function niceExtent(opt) {
    var extent = this._extent; // If extent start and end are same, expand them

    if (extent[0] === extent[1]) {
      // Expand extent
      extent[0] -= ONE_DAY;
      extent[1] += ONE_DAY;
    } // If there are no data and extent are [Infinity, -Infinity]


    if (extent[1] === -Infinity && extent[0] === Infinity) {
      var d = new Date();
      extent[1] = +new Date(d.getFullYear(), d.getMonth(), d.getDate());
      extent[0] = extent[1] - ONE_DAY;
    }

    this.niceTicks(opt.splitNumber, opt.minInterval, opt.maxInterval); // var extent = this._extent;

    var interval = this._interval;

    if (!opt.fixMin) {
      extent[0] = numberUtil.round(mathFloor(extent[0] / interval) * interval);
    }

    if (!opt.fixMax) {
      extent[1] = numberUtil.round(mathCeil(extent[1] / interval) * interval);
    }
  },

  /**
   * @override
   */
  niceTicks: function niceTicks(approxTickNum, minInterval, maxInterval) {
    approxTickNum = approxTickNum || 10;
    var extent = this._extent;
    var span = extent[1] - extent[0];
    var approxInterval = span / approxTickNum;

    if (minInterval != null && approxInterval < minInterval) {
      approxInterval = minInterval;
    }

    if (maxInterval != null && approxInterval > maxInterval) {
      approxInterval = maxInterval;
    }

    var scaleLevelsLen = scaleLevels.length;
    var idx = bisect(scaleLevels, approxInterval, 0, scaleLevelsLen);
    var level = scaleLevels[Math.min(idx, scaleLevelsLen - 1)];
    var interval = level[1]; // Same with interval scale if span is much larger than 1 year

    if (level[0] === 'year') {
      var yearSpan = span / interval; // From "Nice Numbers for Graph Labels" of Graphic Gems
      // var niceYearSpan = numberUtil.nice(yearSpan, false);

      var yearStep = numberUtil.nice(yearSpan / approxTickNum, true);
      interval *= yearStep;
    }

    var timezoneOffset = this.getSetting('useUTC') ? 0 : new Date(+extent[0] || +extent[1]).getTimezoneOffset() * 60 * 1000;
    var niceExtent = [Math.round(mathCeil((extent[0] - timezoneOffset) / interval) * interval + timezoneOffset), Math.round(mathFloor((extent[1] - timezoneOffset) / interval) * interval + timezoneOffset)];
    scaleHelper.fixExtent(niceExtent, extent);
    this._stepLvl = level; // Interval will be used in getTicks

    this._interval = interval;
    this._niceExtent = niceExtent;
  },
  parse: function parse(val) {
    // val might be float.
    return +numberUtil.parseDate(val);
  }
});
zrUtil.each(['contain', 'normalize'], function (methodName) {
  TimeScale.prototype[methodName] = function (val) {
    return intervalScaleProto[methodName].call(this, this.parse(val));
  };
});
/**
 * This implementation was originally copied from "d3.js"
 * <https://github.com/d3/d3/blob/b516d77fb8566b576088e73410437494717ada26/src/time/scale.js>
 * with some modifications made for this program.
 * See the license statement at the head of this file.
 */

var scaleLevels = [// Format              interval
['hh:mm:ss', ONE_SECOND], // 1s
['hh:mm:ss', ONE_SECOND * 5], // 5s
['hh:mm:ss', ONE_SECOND * 10], // 10s
['hh:mm:ss', ONE_SECOND * 15], // 15s
['hh:mm:ss', ONE_SECOND * 30], // 30s
['hh:mm\nMM-dd', ONE_MINUTE], // 1m
['hh:mm\nMM-dd', ONE_MINUTE * 5], // 5m
['hh:mm\nMM-dd', ONE_MINUTE * 10], // 10m
['hh:mm\nMM-dd', ONE_MINUTE * 15], // 15m
['hh:mm\nMM-dd', ONE_MINUTE * 30], // 30m
['hh:mm\nMM-dd', ONE_HOUR], // 1h
['hh:mm\nMM-dd', ONE_HOUR * 2], // 2h
['hh:mm\nMM-dd', ONE_HOUR * 6], // 6h
['hh:mm\nMM-dd', ONE_HOUR * 12], // 12h
['MM-dd\nyyyy', ONE_DAY], // 1d
['MM-dd\nyyyy', ONE_DAY * 2], // 2d
['MM-dd\nyyyy', ONE_DAY * 3], // 3d
['MM-dd\nyyyy', ONE_DAY * 4], // 4d
['MM-dd\nyyyy', ONE_DAY * 5], // 5d
['MM-dd\nyyyy', ONE_DAY * 6], // 6d
['week', ONE_DAY * 7], // 7d
['MM-dd\nyyyy', ONE_DAY * 10], // 10d
['week', ONE_DAY * 14], // 2w
['week', ONE_DAY * 21], // 3w
['month', ONE_DAY * 31], // 1M
['week', ONE_DAY * 42], // 6w
['month', ONE_DAY * 62], // 2M
['week', ONE_DAY * 70], // 10w
['quarter', ONE_DAY * 95], // 3M
['month', ONE_DAY * 31 * 4], // 4M
['month', ONE_DAY * 31 * 5], // 5M
['half-year', ONE_DAY * 380 / 2], // 6M
['month', ONE_DAY * 31 * 8], // 8M
['month', ONE_DAY * 31 * 10], // 10M
['year', ONE_DAY * 380] // 1Y
];
/**
 * @param {module:echarts/model/Model}
 * @return {module:echarts/scale/Time}
 */

TimeScale.create = function (model) {
  return new TimeScale({
    useUTC: model.ecModel.get('useUTC')
  });
};

var _default = TimeScale;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvVGltZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3NjYWxlL1RpbWUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgbnVtYmVyVXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL251bWJlclwiKTtcblxudmFyIGZvcm1hdFV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9mb3JtYXRcIik7XG5cbnZhciBzY2FsZUhlbHBlciA9IHJlcXVpcmUoXCIuL2hlbHBlclwiKTtcblxudmFyIEludGVydmFsU2NhbGUgPSByZXF1aXJlKFwiLi9JbnRlcnZhbFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKlxuKiBBIHRoaXJkLXBhcnR5IGxpY2Vuc2UgaXMgZW1iZWRlZCBmb3Igc29tZSBvZiB0aGUgY29kZSBpbiB0aGlzIGZpbGU6XG4qIFRoZSBcInNjYWxlTGV2ZWxzXCIgd2FzIG9yaWdpbmFsbHkgY29waWVkIGZyb20gXCJkMy5qc1wiIHdpdGggc29tZVxuKiBtb2RpZmljYXRpb25zIG1hZGUgZm9yIHRoaXMgcHJvamVjdC5cbiogKFNlZSBtb3JlIGRldGFpbHMgaW4gdGhlIGNvbW1lbnQgb24gdGhlIGRlZmluaXRpb24gb2YgXCJzY2FsZUxldmVsc1wiIGJlbG93LilcbiogVGhlIHVzZSBvZiB0aGUgc291cmNlIGNvZGUgb2YgdGhpcyBmaWxlIGlzIGFsc28gc3ViamVjdCB0byB0aGUgdGVybXNcbiogYW5kIGNvbnNpdGlvbnMgb2YgdGhlIGxpY2Vuc2Ugb2YgXCJkMy5qc1wiIChCU0QtM0NsYXVzZSwgc2VlXG4qIDwvbGljZW5zZXMvTElDRU5TRS1kMz4pLlxuKi9cbi8vIFtBYm91dCBVVEMgYW5kIGxvY2FsIHRpbWUgem9uZV06XG4vLyBJbiBtb3N0IGNhc2VzLCBgbnVtYmVyLnBhcnNlRGF0ZWAgd2lsbCB0cmVhdCBpbnB1dCBkYXRhIHN0cmluZyBhcyBsb2NhbCB0aW1lXG4vLyAoZXhjZXB0IHRpbWUgem9uZSBpcyBzcGVjaWZpZWQgaW4gdGltZSBzdHJpbmcpLiBBbmQgYGZvcm1hdC5mb3JtYXRlVGltZWAgcmV0dXJuc1xuLy8gbG9jYWwgdGltZSBieSBkZWZhdWx0LiBvcHRpb24udXNlVVRDIGlzIGZhbHNlIGJ5IGRlZmF1bHQuIFRoaXMgZGVzaWduIGhhdmVcbi8vIGNvbmNpZGVyZWQgdGhlc2UgY29tbW9uIGNhc2U6XG4vLyAoMSkgVGltZSB0aGF0IGlzIHBlcnNpc3RlbnQgaW4gc2VydmVyIGlzIGluIFVUQywgYnV0IGl0IGlzIG5lZWRlZCB0byBiZSBkaXBsYXllZFxuLy8gaW4gbG9jYWwgdGltZSBieSBkZWZhdWx0LlxuLy8gKDIpIEJ5IGRlZmF1bHQsIHRoZSBpbnB1dCBkYXRhIHN0cmluZyAoZS5nLiwgJzIwMTEtMDEtMDInKSBzaG91bGQgYmUgZGlzcGxheWVkXG4vLyBhcyBpdHMgb3JpZ2luYWwgdGltZSwgd2l0aG91dCBhbnkgdGltZSBkaWZmZXJlbmNlLlxudmFyIGludGVydmFsU2NhbGVQcm90byA9IEludGVydmFsU2NhbGUucHJvdG90eXBlO1xudmFyIG1hdGhDZWlsID0gTWF0aC5jZWlsO1xudmFyIG1hdGhGbG9vciA9IE1hdGguZmxvb3I7XG52YXIgT05FX1NFQ09ORCA9IDEwMDA7XG52YXIgT05FX01JTlVURSA9IE9ORV9TRUNPTkQgKiA2MDtcbnZhciBPTkVfSE9VUiA9IE9ORV9NSU5VVEUgKiA2MDtcbnZhciBPTkVfREFZID0gT05FX0hPVVIgKiAyNDsgLy8gRklYTUUg5YWs55So77yfXG5cbnZhciBiaXNlY3QgPSBmdW5jdGlvbiAoYSwgeCwgbG8sIGhpKSB7XG4gIHdoaWxlIChsbyA8IGhpKSB7XG4gICAgdmFyIG1pZCA9IGxvICsgaGkgPj4+IDE7XG5cbiAgICBpZiAoYVttaWRdWzFdIDwgeCkge1xuICAgICAgbG8gPSBtaWQgKyAxO1xuICAgIH0gZWxzZSB7XG4gICAgICBoaSA9IG1pZDtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbG87XG59O1xuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvY29vcmQvc2NhbGUvVGltZVxuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuXG52YXIgVGltZVNjYWxlID0gSW50ZXJ2YWxTY2FsZS5leHRlbmQoe1xuICB0eXBlOiAndGltZScsXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgZ2V0TGFiZWw6IGZ1bmN0aW9uICh2YWwpIHtcbiAgICB2YXIgc3RlcEx2bCA9IHRoaXMuX3N0ZXBMdmw7XG4gICAgdmFyIGRhdGUgPSBuZXcgRGF0ZSh2YWwpO1xuICAgIHJldHVybiBmb3JtYXRVdGlsLmZvcm1hdFRpbWUoc3RlcEx2bFswXSwgZGF0ZSwgdGhpcy5nZXRTZXR0aW5nKCd1c2VVVEMnKSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgbmljZUV4dGVudDogZnVuY3Rpb24gKG9wdCkge1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7IC8vIElmIGV4dGVudCBzdGFydCBhbmQgZW5kIGFyZSBzYW1lLCBleHBhbmQgdGhlbVxuXG4gICAgaWYgKGV4dGVudFswXSA9PT0gZXh0ZW50WzFdKSB7XG4gICAgICAvLyBFeHBhbmQgZXh0ZW50XG4gICAgICBleHRlbnRbMF0gLT0gT05FX0RBWTtcbiAgICAgIGV4dGVudFsxXSArPSBPTkVfREFZO1xuICAgIH0gLy8gSWYgdGhlcmUgYXJlIG5vIGRhdGEgYW5kIGV4dGVudCBhcmUgW0luZmluaXR5LCAtSW5maW5pdHldXG5cblxuICAgIGlmIChleHRlbnRbMV0gPT09IC1JbmZpbml0eSAmJiBleHRlbnRbMF0gPT09IEluZmluaXR5KSB7XG4gICAgICB2YXIgZCA9IG5ldyBEYXRlKCk7XG4gICAgICBleHRlbnRbMV0gPSArbmV3IERhdGUoZC5nZXRGdWxsWWVhcigpLCBkLmdldE1vbnRoKCksIGQuZ2V0RGF0ZSgpKTtcbiAgICAgIGV4dGVudFswXSA9IGV4dGVudFsxXSAtIE9ORV9EQVk7XG4gICAgfVxuXG4gICAgdGhpcy5uaWNlVGlja3Mob3B0LnNwbGl0TnVtYmVyLCBvcHQubWluSW50ZXJ2YWwsIG9wdC5tYXhJbnRlcnZhbCk7IC8vIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG5cbiAgICB2YXIgaW50ZXJ2YWwgPSB0aGlzLl9pbnRlcnZhbDtcblxuICAgIGlmICghb3B0LmZpeE1pbikge1xuICAgICAgZXh0ZW50WzBdID0gbnVtYmVyVXRpbC5yb3VuZChtYXRoRmxvb3IoZXh0ZW50WzBdIC8gaW50ZXJ2YWwpICogaW50ZXJ2YWwpO1xuICAgIH1cblxuICAgIGlmICghb3B0LmZpeE1heCkge1xuICAgICAgZXh0ZW50WzFdID0gbnVtYmVyVXRpbC5yb3VuZChtYXRoQ2VpbChleHRlbnRbMV0gLyBpbnRlcnZhbCkgKiBpbnRlcnZhbCk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIG5pY2VUaWNrczogZnVuY3Rpb24gKGFwcHJveFRpY2tOdW0sIG1pbkludGVydmFsLCBtYXhJbnRlcnZhbCkge1xuICAgIGFwcHJveFRpY2tOdW0gPSBhcHByb3hUaWNrTnVtIHx8IDEwO1xuICAgIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gICAgdmFyIHNwYW4gPSBleHRlbnRbMV0gLSBleHRlbnRbMF07XG4gICAgdmFyIGFwcHJveEludGVydmFsID0gc3BhbiAvIGFwcHJveFRpY2tOdW07XG5cbiAgICBpZiAobWluSW50ZXJ2YWwgIT0gbnVsbCAmJiBhcHByb3hJbnRlcnZhbCA8IG1pbkludGVydmFsKSB7XG4gICAgICBhcHByb3hJbnRlcnZhbCA9IG1pbkludGVydmFsO1xuICAgIH1cblxuICAgIGlmIChtYXhJbnRlcnZhbCAhPSBudWxsICYmIGFwcHJveEludGVydmFsID4gbWF4SW50ZXJ2YWwpIHtcbiAgICAgIGFwcHJveEludGVydmFsID0gbWF4SW50ZXJ2YWw7XG4gICAgfVxuXG4gICAgdmFyIHNjYWxlTGV2ZWxzTGVuID0gc2NhbGVMZXZlbHMubGVuZ3RoO1xuICAgIHZhciBpZHggPSBiaXNlY3Qoc2NhbGVMZXZlbHMsIGFwcHJveEludGVydmFsLCAwLCBzY2FsZUxldmVsc0xlbik7XG4gICAgdmFyIGxldmVsID0gc2NhbGVMZXZlbHNbTWF0aC5taW4oaWR4LCBzY2FsZUxldmVsc0xlbiAtIDEpXTtcbiAgICB2YXIgaW50ZXJ2YWwgPSBsZXZlbFsxXTsgLy8gU2FtZSB3aXRoIGludGVydmFsIHNjYWxlIGlmIHNwYW4gaXMgbXVjaCBsYXJnZXIgdGhhbiAxIHllYXJcblxuICAgIGlmIChsZXZlbFswXSA9PT0gJ3llYXInKSB7XG4gICAgICB2YXIgeWVhclNwYW4gPSBzcGFuIC8gaW50ZXJ2YWw7IC8vIEZyb20gXCJOaWNlIE51bWJlcnMgZm9yIEdyYXBoIExhYmVsc1wiIG9mIEdyYXBoaWMgR2Vtc1xuICAgICAgLy8gdmFyIG5pY2VZZWFyU3BhbiA9IG51bWJlclV0aWwubmljZSh5ZWFyU3BhbiwgZmFsc2UpO1xuXG4gICAgICB2YXIgeWVhclN0ZXAgPSBudW1iZXJVdGlsLm5pY2UoeWVhclNwYW4gLyBhcHByb3hUaWNrTnVtLCB0cnVlKTtcbiAgICAgIGludGVydmFsICo9IHllYXJTdGVwO1xuICAgIH1cblxuICAgIHZhciB0aW1lem9uZU9mZnNldCA9IHRoaXMuZ2V0U2V0dGluZygndXNlVVRDJykgPyAwIDogbmV3IERhdGUoK2V4dGVudFswXSB8fCArZXh0ZW50WzFdKS5nZXRUaW1lem9uZU9mZnNldCgpICogNjAgKiAxMDAwO1xuICAgIHZhciBuaWNlRXh0ZW50ID0gW01hdGgucm91bmQobWF0aENlaWwoKGV4dGVudFswXSAtIHRpbWV6b25lT2Zmc2V0KSAvIGludGVydmFsKSAqIGludGVydmFsICsgdGltZXpvbmVPZmZzZXQpLCBNYXRoLnJvdW5kKG1hdGhGbG9vcigoZXh0ZW50WzFdIC0gdGltZXpvbmVPZmZzZXQpIC8gaW50ZXJ2YWwpICogaW50ZXJ2YWwgKyB0aW1lem9uZU9mZnNldCldO1xuICAgIHNjYWxlSGVscGVyLmZpeEV4dGVudChuaWNlRXh0ZW50LCBleHRlbnQpO1xuICAgIHRoaXMuX3N0ZXBMdmwgPSBsZXZlbDsgLy8gSW50ZXJ2YWwgd2lsbCBiZSB1c2VkIGluIGdldFRpY2tzXG5cbiAgICB0aGlzLl9pbnRlcnZhbCA9IGludGVydmFsO1xuICAgIHRoaXMuX25pY2VFeHRlbnQgPSBuaWNlRXh0ZW50O1xuICB9LFxuICBwYXJzZTogZnVuY3Rpb24gKHZhbCkge1xuICAgIC8vIHZhbCBtaWdodCBiZSBmbG9hdC5cbiAgICByZXR1cm4gK251bWJlclV0aWwucGFyc2VEYXRlKHZhbCk7XG4gIH1cbn0pO1xuenJVdGlsLmVhY2goWydjb250YWluJywgJ25vcm1hbGl6ZSddLCBmdW5jdGlvbiAobWV0aG9kTmFtZSkge1xuICBUaW1lU2NhbGUucHJvdG90eXBlW21ldGhvZE5hbWVdID0gZnVuY3Rpb24gKHZhbCkge1xuICAgIHJldHVybiBpbnRlcnZhbFNjYWxlUHJvdG9bbWV0aG9kTmFtZV0uY2FsbCh0aGlzLCB0aGlzLnBhcnNlKHZhbCkpO1xuICB9O1xufSk7XG4vKipcbiAqIFRoaXMgaW1wbGVtZW50YXRpb24gd2FzIG9yaWdpbmFsbHkgY29waWVkIGZyb20gXCJkMy5qc1wiXG4gKiA8aHR0cHM6Ly9naXRodWIuY29tL2QzL2QzL2Jsb2IvYjUxNmQ3N2ZiODU2NmI1NzYwODhlNzM0MTA0Mzc0OTQ3MTdhZGEyNi9zcmMvdGltZS9zY2FsZS5qcz5cbiAqIHdpdGggc29tZSBtb2RpZmljYXRpb25zIG1hZGUgZm9yIHRoaXMgcHJvZ3JhbS5cbiAqIFNlZSB0aGUgbGljZW5zZSBzdGF0ZW1lbnQgYXQgdGhlIGhlYWQgb2YgdGhpcyBmaWxlLlxuICovXG5cbnZhciBzY2FsZUxldmVscyA9IFsvLyBGb3JtYXQgICAgICAgICAgICAgIGludGVydmFsXG5bJ2hoOm1tOnNzJywgT05FX1NFQ09ORF0sIC8vIDFzXG5bJ2hoOm1tOnNzJywgT05FX1NFQ09ORCAqIDVdLCAvLyA1c1xuWydoaDptbTpzcycsIE9ORV9TRUNPTkQgKiAxMF0sIC8vIDEwc1xuWydoaDptbTpzcycsIE9ORV9TRUNPTkQgKiAxNV0sIC8vIDE1c1xuWydoaDptbTpzcycsIE9ORV9TRUNPTkQgKiAzMF0sIC8vIDMwc1xuWydoaDptbVxcbk1NLWRkJywgT05FX01JTlVURV0sIC8vIDFtXG5bJ2hoOm1tXFxuTU0tZGQnLCBPTkVfTUlOVVRFICogNV0sIC8vIDVtXG5bJ2hoOm1tXFxuTU0tZGQnLCBPTkVfTUlOVVRFICogMTBdLCAvLyAxMG1cblsnaGg6bW1cXG5NTS1kZCcsIE9ORV9NSU5VVEUgKiAxNV0sIC8vIDE1bVxuWydoaDptbVxcbk1NLWRkJywgT05FX01JTlVURSAqIDMwXSwgLy8gMzBtXG5bJ2hoOm1tXFxuTU0tZGQnLCBPTkVfSE9VUl0sIC8vIDFoXG5bJ2hoOm1tXFxuTU0tZGQnLCBPTkVfSE9VUiAqIDJdLCAvLyAyaFxuWydoaDptbVxcbk1NLWRkJywgT05FX0hPVVIgKiA2XSwgLy8gNmhcblsnaGg6bW1cXG5NTS1kZCcsIE9ORV9IT1VSICogMTJdLCAvLyAxMmhcblsnTU0tZGRcXG55eXl5JywgT05FX0RBWV0sIC8vIDFkXG5bJ01NLWRkXFxueXl5eScsIE9ORV9EQVkgKiAyXSwgLy8gMmRcblsnTU0tZGRcXG55eXl5JywgT05FX0RBWSAqIDNdLCAvLyAzZFxuWydNTS1kZFxcbnl5eXknLCBPTkVfREFZICogNF0sIC8vIDRkXG5bJ01NLWRkXFxueXl5eScsIE9ORV9EQVkgKiA1XSwgLy8gNWRcblsnTU0tZGRcXG55eXl5JywgT05FX0RBWSAqIDZdLCAvLyA2ZFxuWyd3ZWVrJywgT05FX0RBWSAqIDddLCAvLyA3ZFxuWydNTS1kZFxcbnl5eXknLCBPTkVfREFZICogMTBdLCAvLyAxMGRcblsnd2VlaycsIE9ORV9EQVkgKiAxNF0sIC8vIDJ3XG5bJ3dlZWsnLCBPTkVfREFZICogMjFdLCAvLyAzd1xuWydtb250aCcsIE9ORV9EQVkgKiAzMV0sIC8vIDFNXG5bJ3dlZWsnLCBPTkVfREFZICogNDJdLCAvLyA2d1xuWydtb250aCcsIE9ORV9EQVkgKiA2Ml0sIC8vIDJNXG5bJ3dlZWsnLCBPTkVfREFZICogNzBdLCAvLyAxMHdcblsncXVhcnRlcicsIE9ORV9EQVkgKiA5NV0sIC8vIDNNXG5bJ21vbnRoJywgT05FX0RBWSAqIDMxICogNF0sIC8vIDRNXG5bJ21vbnRoJywgT05FX0RBWSAqIDMxICogNV0sIC8vIDVNXG5bJ2hhbGYteWVhcicsIE9ORV9EQVkgKiAzODAgLyAyXSwgLy8gNk1cblsnbW9udGgnLCBPTkVfREFZICogMzEgKiA4XSwgLy8gOE1cblsnbW9udGgnLCBPTkVfREFZICogMzEgKiAxMF0sIC8vIDEwTVxuWyd5ZWFyJywgT05FX0RBWSAqIDM4MF0gLy8gMVlcbl07XG4vKipcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9XG4gKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9zY2FsZS9UaW1lfVxuICovXG5cblRpbWVTY2FsZS5jcmVhdGUgPSBmdW5jdGlvbiAobW9kZWwpIHtcbiAgcmV0dXJuIG5ldyBUaW1lU2NhbGUoe1xuICAgIHVzZVVUQzogbW9kZWwuZWNNb2RlbC5nZXQoJ3VzZVVUQycpXG4gIH0pO1xufTtcblxudmFyIF9kZWZhdWx0ID0gVGltZVNjYWxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJGQTtBQXVGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQ0E7QUFxQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/Time.js
