
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

var invariant = __webpack_require__(/*! ./invariant */ "./node_modules/fbjs/lib/invariant.js");
/**
 * Convert array-like objects to arrays.
 *
 * This API assumes the caller knows the contents of the data type. For less
 * well defined inputs use createArrayFromMixed.
 *
 * @param {object|function|filelist} obj
 * @return {array}
 */


function toArray(obj) {
  var length = obj.length; // Some browsers builtin objects can report typeof 'function' (e.g. NodeList
  // in old versions of Safari).

  !(!Array.isArray(obj) && (typeof obj === 'object' || typeof obj === 'function')) ?  true ? invariant(false, 'toArray: Array-like object expected') : undefined : void 0;
  !(typeof length === 'number') ?  true ? invariant(false, 'toArray: Object needs a length property') : undefined : void 0;
  !(length === 0 || length - 1 in obj) ?  true ? invariant(false, 'toArray: Object should have keys for indices') : undefined : void 0;
  !(typeof obj.callee !== 'function') ?  true ? invariant(false, 'toArray: Object can\'t be `arguments`. Use rest params ' + '(function(...args) {}) or Array.from() instead.') : undefined : void 0; // Old IE doesn't give collections access to hasOwnProperty. Assume inputs
  // without method will throw during the slice call and skip straight to the
  // fallback.

  if (obj.hasOwnProperty) {
    try {
      return Array.prototype.slice.call(obj);
    } catch (e) {// IE < 9 does not support Array#slice on collections objects
    }
  } // Fall back to copying key by key. This assumes all keys have a value,
  // so will not preserve sparsely populated inputs.


  var ret = Array(length);

  for (var ii = 0; ii < length; ii++) {
    ret[ii] = obj[ii];
  }

  return ret;
}
/**
 * Perform a heuristic test to determine if an object is "array-like".
 *
 *   A monk asked Joshu, a Zen master, "Has a dog Buddha nature?"
 *   Joshu replied: "Mu."
 *
 * This function determines if its argument has "array nature": it returns
 * true if the argument is an actual array, an `arguments' object, or an
 * HTMLCollection (e.g. node.childNodes or node.getElementsByTagName()).
 *
 * It will return false for other array-like objects like Filelist.
 *
 * @param {*} obj
 * @return {boolean}
 */


function hasArrayNature(obj) {
  return (// not null/false
    !!obj && ( // arrays are objects, NodeLists are functions in Safari
    typeof obj == 'object' || typeof obj == 'function') && // quacks like an array
    'length' in obj && // not window
    !('setInterval' in obj) && // no DOM node should be considered an array-like
    // a 'select' element has 'length' and 'item' properties on IE8
    typeof obj.nodeType != 'number' && ( // a real array
    Array.isArray(obj) || // arguments
    'callee' in obj || // HTMLCollection/NodeList
    'item' in obj)
  );
}
/**
 * Ensure that the argument is an array by wrapping it in an array if it is not.
 * Creates a copy of the argument if it is already an array.
 *
 * This is mostly useful idiomatically:
 *
 *   var createArrayFromMixed = require('createArrayFromMixed');
 *
 *   function takesOneOrMoreThings(things) {
 *     things = createArrayFromMixed(things);
 *     ...
 *   }
 *
 * This allows you to treat `things' as an array, but accept scalars in the API.
 *
 * If you need to convert an array-like object, like `arguments`, into an array
 * use toArray instead.
 *
 * @param {*} obj
 * @return {array}
 */


function createArrayFromMixed(obj) {
  if (!hasArrayNature(obj)) {
    return [obj];
  } else if (Array.isArray(obj)) {
    return obj.slice();
  } else {
    return toArray(obj);
  }
}

module.exports = createArrayFromMixed;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvY3JlYXRlQXJyYXlGcm9tTWl4ZWQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9jcmVhdGVBcnJheUZyb21NaXhlZC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqL1xuXG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnLi9pbnZhcmlhbnQnKTtcblxuLyoqXG4gKiBDb252ZXJ0IGFycmF5LWxpa2Ugb2JqZWN0cyB0byBhcnJheXMuXG4gKlxuICogVGhpcyBBUEkgYXNzdW1lcyB0aGUgY2FsbGVyIGtub3dzIHRoZSBjb250ZW50cyBvZiB0aGUgZGF0YSB0eXBlLiBGb3IgbGVzc1xuICogd2VsbCBkZWZpbmVkIGlucHV0cyB1c2UgY3JlYXRlQXJyYXlGcm9tTWl4ZWQuXG4gKlxuICogQHBhcmFtIHtvYmplY3R8ZnVuY3Rpb258ZmlsZWxpc3R9IG9ialxuICogQHJldHVybiB7YXJyYXl9XG4gKi9cbmZ1bmN0aW9uIHRvQXJyYXkob2JqKSB7XG4gIHZhciBsZW5ndGggPSBvYmoubGVuZ3RoO1xuXG4gIC8vIFNvbWUgYnJvd3NlcnMgYnVpbHRpbiBvYmplY3RzIGNhbiByZXBvcnQgdHlwZW9mICdmdW5jdGlvbicgKGUuZy4gTm9kZUxpc3RcbiAgLy8gaW4gb2xkIHZlcnNpb25zIG9mIFNhZmFyaSkuXG4gICEoIUFycmF5LmlzQXJyYXkob2JqKSAmJiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcgfHwgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ3RvQXJyYXk6IEFycmF5LWxpa2Ugb2JqZWN0IGV4cGVjdGVkJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gICEodHlwZW9mIGxlbmd0aCA9PT0gJ251bWJlcicpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ3RvQXJyYXk6IE9iamVjdCBuZWVkcyBhIGxlbmd0aCBwcm9wZXJ0eScpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICAhKGxlbmd0aCA9PT0gMCB8fCBsZW5ndGggLSAxIGluIG9iaikgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAndG9BcnJheTogT2JqZWN0IHNob3VsZCBoYXZlIGtleXMgZm9yIGluZGljZXMnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgISh0eXBlb2Ygb2JqLmNhbGxlZSAhPT0gJ2Z1bmN0aW9uJykgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAndG9BcnJheTogT2JqZWN0IGNhblxcJ3QgYmUgYGFyZ3VtZW50c2AuIFVzZSByZXN0IHBhcmFtcyAnICsgJyhmdW5jdGlvbiguLi5hcmdzKSB7fSkgb3IgQXJyYXkuZnJvbSgpIGluc3RlYWQuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIC8vIE9sZCBJRSBkb2Vzbid0IGdpdmUgY29sbGVjdGlvbnMgYWNjZXNzIHRvIGhhc093blByb3BlcnR5LiBBc3N1bWUgaW5wdXRzXG4gIC8vIHdpdGhvdXQgbWV0aG9kIHdpbGwgdGhyb3cgZHVyaW5nIHRoZSBzbGljZSBjYWxsIGFuZCBza2lwIHN0cmFpZ2h0IHRvIHRoZVxuICAvLyBmYWxsYmFjay5cbiAgaWYgKG9iai5oYXNPd25Qcm9wZXJ0eSkge1xuICAgIHRyeSB7XG4gICAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwob2JqKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAvLyBJRSA8IDkgZG9lcyBub3Qgc3VwcG9ydCBBcnJheSNzbGljZSBvbiBjb2xsZWN0aW9ucyBvYmplY3RzXG4gICAgfVxuICB9XG5cbiAgLy8gRmFsbCBiYWNrIHRvIGNvcHlpbmcga2V5IGJ5IGtleS4gVGhpcyBhc3N1bWVzIGFsbCBrZXlzIGhhdmUgYSB2YWx1ZSxcbiAgLy8gc28gd2lsbCBub3QgcHJlc2VydmUgc3BhcnNlbHkgcG9wdWxhdGVkIGlucHV0cy5cbiAgdmFyIHJldCA9IEFycmF5KGxlbmd0aCk7XG4gIGZvciAodmFyIGlpID0gMDsgaWkgPCBsZW5ndGg7IGlpKyspIHtcbiAgICByZXRbaWldID0gb2JqW2lpXTtcbiAgfVxuICByZXR1cm4gcmV0O1xufVxuXG4vKipcbiAqIFBlcmZvcm0gYSBoZXVyaXN0aWMgdGVzdCB0byBkZXRlcm1pbmUgaWYgYW4gb2JqZWN0IGlzIFwiYXJyYXktbGlrZVwiLlxuICpcbiAqICAgQSBtb25rIGFza2VkIEpvc2h1LCBhIFplbiBtYXN0ZXIsIFwiSGFzIGEgZG9nIEJ1ZGRoYSBuYXR1cmU/XCJcbiAqICAgSm9zaHUgcmVwbGllZDogXCJNdS5cIlxuICpcbiAqIFRoaXMgZnVuY3Rpb24gZGV0ZXJtaW5lcyBpZiBpdHMgYXJndW1lbnQgaGFzIFwiYXJyYXkgbmF0dXJlXCI6IGl0IHJldHVybnNcbiAqIHRydWUgaWYgdGhlIGFyZ3VtZW50IGlzIGFuIGFjdHVhbCBhcnJheSwgYW4gYGFyZ3VtZW50cycgb2JqZWN0LCBvciBhblxuICogSFRNTENvbGxlY3Rpb24gKGUuZy4gbm9kZS5jaGlsZE5vZGVzIG9yIG5vZGUuZ2V0RWxlbWVudHNCeVRhZ05hbWUoKSkuXG4gKlxuICogSXQgd2lsbCByZXR1cm4gZmFsc2UgZm9yIG90aGVyIGFycmF5LWxpa2Ugb2JqZWN0cyBsaWtlIEZpbGVsaXN0LlxuICpcbiAqIEBwYXJhbSB7Kn0gb2JqXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBoYXNBcnJheU5hdHVyZShvYmopIHtcbiAgcmV0dXJuIChcbiAgICAvLyBub3QgbnVsbC9mYWxzZVxuICAgICEhb2JqICYmIChcbiAgICAvLyBhcnJheXMgYXJlIG9iamVjdHMsIE5vZGVMaXN0cyBhcmUgZnVuY3Rpb25zIGluIFNhZmFyaVxuICAgIHR5cGVvZiBvYmogPT0gJ29iamVjdCcgfHwgdHlwZW9mIG9iaiA9PSAnZnVuY3Rpb24nKSAmJlxuICAgIC8vIHF1YWNrcyBsaWtlIGFuIGFycmF5XG4gICAgJ2xlbmd0aCcgaW4gb2JqICYmXG4gICAgLy8gbm90IHdpbmRvd1xuICAgICEoJ3NldEludGVydmFsJyBpbiBvYmopICYmXG4gICAgLy8gbm8gRE9NIG5vZGUgc2hvdWxkIGJlIGNvbnNpZGVyZWQgYW4gYXJyYXktbGlrZVxuICAgIC8vIGEgJ3NlbGVjdCcgZWxlbWVudCBoYXMgJ2xlbmd0aCcgYW5kICdpdGVtJyBwcm9wZXJ0aWVzIG9uIElFOFxuICAgIHR5cGVvZiBvYmoubm9kZVR5cGUgIT0gJ251bWJlcicgJiYgKFxuICAgIC8vIGEgcmVhbCBhcnJheVxuICAgIEFycmF5LmlzQXJyYXkob2JqKSB8fFxuICAgIC8vIGFyZ3VtZW50c1xuICAgICdjYWxsZWUnIGluIG9iaiB8fFxuICAgIC8vIEhUTUxDb2xsZWN0aW9uL05vZGVMaXN0XG4gICAgJ2l0ZW0nIGluIG9iailcbiAgKTtcbn1cblxuLyoqXG4gKiBFbnN1cmUgdGhhdCB0aGUgYXJndW1lbnQgaXMgYW4gYXJyYXkgYnkgd3JhcHBpbmcgaXQgaW4gYW4gYXJyYXkgaWYgaXQgaXMgbm90LlxuICogQ3JlYXRlcyBhIGNvcHkgb2YgdGhlIGFyZ3VtZW50IGlmIGl0IGlzIGFscmVhZHkgYW4gYXJyYXkuXG4gKlxuICogVGhpcyBpcyBtb3N0bHkgdXNlZnVsIGlkaW9tYXRpY2FsbHk6XG4gKlxuICogICB2YXIgY3JlYXRlQXJyYXlGcm9tTWl4ZWQgPSByZXF1aXJlKCdjcmVhdGVBcnJheUZyb21NaXhlZCcpO1xuICpcbiAqICAgZnVuY3Rpb24gdGFrZXNPbmVPck1vcmVUaGluZ3ModGhpbmdzKSB7XG4gKiAgICAgdGhpbmdzID0gY3JlYXRlQXJyYXlGcm9tTWl4ZWQodGhpbmdzKTtcbiAqICAgICAuLi5cbiAqICAgfVxuICpcbiAqIFRoaXMgYWxsb3dzIHlvdSB0byB0cmVhdCBgdGhpbmdzJyBhcyBhbiBhcnJheSwgYnV0IGFjY2VwdCBzY2FsYXJzIGluIHRoZSBBUEkuXG4gKlxuICogSWYgeW91IG5lZWQgdG8gY29udmVydCBhbiBhcnJheS1saWtlIG9iamVjdCwgbGlrZSBgYXJndW1lbnRzYCwgaW50byBhbiBhcnJheVxuICogdXNlIHRvQXJyYXkgaW5zdGVhZC5cbiAqXG4gKiBAcGFyYW0geyp9IG9ialxuICogQHJldHVybiB7YXJyYXl9XG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZUFycmF5RnJvbU1peGVkKG9iaikge1xuICBpZiAoIWhhc0FycmF5TmF0dXJlKG9iaikpIHtcbiAgICByZXR1cm4gW29ial07XG4gIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheShvYmopKSB7XG4gICAgcmV0dXJuIG9iai5zbGljZSgpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiB0b0FycmF5KG9iaik7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBjcmVhdGVBcnJheUZyb21NaXhlZDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7Ozs7OztBQVNBO0FBRUE7Ozs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFlQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBakJBO0FBbUJBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/createArrayFromMixed.js
