/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var assert = _util.assert;
var isArray = _util.isArray;

var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @param {Object} define
 * @return See the return of `createTask`.
 */

function createTask(define) {
  return new Task(define);
}
/**
 * @constructor
 * @param {Object} define
 * @param {Function} define.reset Custom reset
 * @param {Function} [define.plan] Returns 'reset' indicate reset immediately.
 * @param {Function} [define.count] count is used to determin data task.
 * @param {Function} [define.onDirty] count is used to determin data task.
 */


function Task(define) {
  define = define || {};
  this._reset = define.reset;
  this._plan = define.plan;
  this._count = define.count;
  this._onDirty = define.onDirty;
  this._dirty = true; // Context must be specified implicitly, to
  // avoid miss update context when model changed.

  this.context;
}

var taskProto = Task.prototype;
/**
 * @param {Object} performArgs
 * @param {number} [performArgs.step] Specified step.
 * @param {number} [performArgs.skip] Skip customer perform call.
 * @param {number} [performArgs.modBy] Sampling window size.
 * @param {number} [performArgs.modDataCount] Sampling count.
 */

taskProto.perform = function (performArgs) {
  var upTask = this._upstream;
  var skip = performArgs && performArgs.skip; // TODO some refactor.
  // Pull data. Must pull data each time, because context.data
  // may be updated by Series.setData.

  if (this._dirty && upTask) {
    var context = this.context;
    context.data = context.outputData = upTask.context.outputData;
  }

  if (this.__pipeline) {
    this.__pipeline.currentTask = this;
  }

  var planResult;

  if (this._plan && !skip) {
    planResult = this._plan(this.context);
  } // Support sharding by mod, which changes the render sequence and makes the rendered graphic
  // elements uniformed distributed when progress, especially when moving or zooming.


  var lastModBy = normalizeModBy(this._modBy);
  var lastModDataCount = this._modDataCount || 0;
  var modBy = normalizeModBy(performArgs && performArgs.modBy);
  var modDataCount = performArgs && performArgs.modDataCount || 0;

  if (lastModBy !== modBy || lastModDataCount !== modDataCount) {
    planResult = 'reset';
  }

  function normalizeModBy(val) {
    !(val >= 1) && (val = 1); // jshint ignore:line

    return val;
  }

  var forceFirstProgress;

  if (this._dirty || planResult === 'reset') {
    this._dirty = false;
    forceFirstProgress = reset(this, skip);
  }

  this._modBy = modBy;
  this._modDataCount = modDataCount;
  var step = performArgs && performArgs.step;

  if (upTask) {
    this._dueEnd = upTask._outputDueEnd;
  } // DataTask or overallTask
  else {
      this._dueEnd = this._count ? this._count(this.context) : Infinity;
    } // Note: Stubs, that its host overall task let it has progress, has progress.
  // If no progress, pass index from upstream to downstream each time plan called.


  if (this._progress) {
    var start = this._dueIndex;
    var end = Math.min(step != null ? this._dueIndex + step : Infinity, this._dueEnd);

    if (!skip && (forceFirstProgress || start < end)) {
      var progress = this._progress;

      if (isArray(progress)) {
        for (var i = 0; i < progress.length; i++) {
          doProgress(this, progress[i], start, end, modBy, modDataCount);
        }
      } else {
        doProgress(this, progress, start, end, modBy, modDataCount);
      }
    }

    this._dueIndex = end; // If no `outputDueEnd`, assume that output data and
    // input data is the same, so use `dueIndex` as `outputDueEnd`.

    var outputDueEnd = this._settedOutputEnd != null ? this._settedOutputEnd : end;
    this._outputDueEnd = outputDueEnd;
  } else {
    // (1) Some overall task has no progress.
    // (2) Stubs, that its host overall task do not let it has progress, has no progress.
    // This should always be performed so it can be passed to downstream.
    this._dueIndex = this._outputDueEnd = this._settedOutputEnd != null ? this._settedOutputEnd : this._dueEnd;
  }

  return this.unfinished();
};

var iterator = function () {
  var end;
  var current;
  var modBy;
  var modDataCount;
  var winCount;
  var it = {
    reset: function reset(s, e, sStep, sCount) {
      current = s;
      end = e;
      modBy = sStep;
      modDataCount = sCount;
      winCount = Math.ceil(modDataCount / modBy);
      it.next = modBy > 1 && modDataCount > 0 ? modNext : sequentialNext;
    }
  };
  return it;

  function sequentialNext() {
    return current < end ? current++ : null;
  }

  function modNext() {
    var dataIndex = current % winCount * modBy + Math.ceil(current / winCount);
    var result = current >= end ? null : dataIndex < modDataCount ? dataIndex // If modDataCount is smaller than data.count() (consider `appendData` case),
    // Use normal linear rendering mode.
    : current;
    current++;
    return result;
  }
}();

taskProto.dirty = function () {
  this._dirty = true;
  this._onDirty && this._onDirty(this.context);
};

function doProgress(taskIns, progress, start, end, modBy, modDataCount) {
  iterator.reset(start, end, modBy, modDataCount);
  taskIns._callingProgress = progress;

  taskIns._callingProgress({
    start: start,
    end: end,
    count: end - start,
    next: iterator.next
  }, taskIns.context);
}

function reset(taskIns, skip) {
  taskIns._dueIndex = taskIns._outputDueEnd = taskIns._dueEnd = 0;
  taskIns._settedOutputEnd = null;
  var progress;
  var forceFirstProgress;

  if (!skip && taskIns._reset) {
    progress = taskIns._reset(taskIns.context);

    if (progress && progress.progress) {
      forceFirstProgress = progress.forceFirstProgress;
      progress = progress.progress;
    } // To simplify no progress checking, array must has item.


    if (isArray(progress) && !progress.length) {
      progress = null;
    }
  }

  taskIns._progress = progress;
  taskIns._modBy = taskIns._modDataCount = null;
  var downstream = taskIns._downstream;
  downstream && downstream.dirty();
  return forceFirstProgress;
}
/**
 * @return {boolean}
 */


taskProto.unfinished = function () {
  return this._progress && this._dueIndex < this._dueEnd;
};
/**
 * @param {Object} downTask The downstream task.
 * @return {Object} The downstream task.
 */


taskProto.pipe = function (downTask) {
  // If already downstream, do not dirty downTask.
  if (this._downstream !== downTask || this._dirty) {
    this._downstream = downTask;
    downTask._upstream = this;
    downTask.dirty();
  }
};

taskProto.dispose = function () {
  if (this._disposed) {
    return;
  }

  this._upstream && (this._upstream._downstream = null);
  this._downstream && (this._downstream._upstream = null);
  this._dirty = false;
  this._disposed = true;
};

taskProto.getUpstream = function () {
  return this._upstream;
};

taskProto.getDownstream = function () {
  return this._downstream;
};

taskProto.setOutputEnd = function (end) {
  // This only happend in dataTask, dataZoom, map, currently.
  // where dataZoom do not set end each time, but only set
  // when reset. So we should record the setted end, in case
  // that the stub of dataZoom perform again and earse the
  // setted end by upstream.
  this._outputDueEnd = this._settedOutputEnd = end;
}; ///////////////////////////////////////////////////////////
// For stream debug (Should be commented out after used!)
// Usage: printTask(this, 'begin');
// Usage: printTask(this, null, {someExtraProp});
// function printTask(task, prefix, extra) {
//     window.ecTaskUID == null && (window.ecTaskUID = 0);
//     task.uidDebug == null && (task.uidDebug = `task_${window.ecTaskUID++}`);
//     task.agent && task.agent.uidDebug == null && (task.agent.uidDebug = `task_${window.ecTaskUID++}`);
//     var props = [];
//     if (task.__pipeline) {
//         var val = `${task.__idxInPipeline}/${task.__pipeline.tail.__idxInPipeline} ${task.agent ? '(stub)' : ''}`;
//         props.push({text: 'idx', value: val});
//     } else {
//         var stubCount = 0;
//         task.agentStubMap.each(() => stubCount++);
//         props.push({text: 'idx', value: `overall (stubs: ${stubCount})`});
//     }
//     props.push({text: 'uid', value: task.uidDebug});
//     if (task.__pipeline) {
//         props.push({text: 'pid', value: task.__pipeline.id});
//         task.agent && props.push(
//             {text: 'stubFor', value: task.agent.uidDebug}
//         );
//     }
//     props.push(
//         {text: 'dirty', value: task._dirty},
//         {text: 'dueIndex', value: task._dueIndex},
//         {text: 'dueEnd', value: task._dueEnd},
//         {text: 'outputDueEnd', value: task._outputDueEnd}
//     );
//     if (extra) {
//         Object.keys(extra).forEach(key => {
//             props.push({text: key, value: extra[key]});
//         });
//     }
//     var args = ['color: blue'];
//     var msg = `%c[${prefix || 'T'}] %c` + props.map(item => (
//         args.push('color: black', 'color: red'),
//         `${item.text}: %c${item.value}`
//     )).join('%c, ');
//     console.log.apply(console, [msg].concat(args));
//     // console.log(this);
// }


exports.createTask = createTask;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc3RyZWFtL3Rhc2suanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9zdHJlYW0vdGFzay5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGFzc2VydCA9IF91dGlsLmFzc2VydDtcbnZhciBpc0FycmF5ID0gX3V0aWwuaXNBcnJheTtcblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fSBkZWZpbmVcbiAqIEByZXR1cm4gU2VlIHRoZSByZXR1cm4gb2YgYGNyZWF0ZVRhc2tgLlxuICovXG5mdW5jdGlvbiBjcmVhdGVUYXNrKGRlZmluZSkge1xuICByZXR1cm4gbmV3IFRhc2soZGVmaW5lKTtcbn1cbi8qKlxuICogQGNvbnN0cnVjdG9yXG4gKiBAcGFyYW0ge09iamVjdH0gZGVmaW5lXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBkZWZpbmUucmVzZXQgQ3VzdG9tIHJlc2V0XG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbZGVmaW5lLnBsYW5dIFJldHVybnMgJ3Jlc2V0JyBpbmRpY2F0ZSByZXNldCBpbW1lZGlhdGVseS5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtkZWZpbmUuY291bnRdIGNvdW50IGlzIHVzZWQgdG8gZGV0ZXJtaW4gZGF0YSB0YXNrLlxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2RlZmluZS5vbkRpcnR5XSBjb3VudCBpcyB1c2VkIHRvIGRldGVybWluIGRhdGEgdGFzay5cbiAqL1xuXG5cbmZ1bmN0aW9uIFRhc2soZGVmaW5lKSB7XG4gIGRlZmluZSA9IGRlZmluZSB8fCB7fTtcbiAgdGhpcy5fcmVzZXQgPSBkZWZpbmUucmVzZXQ7XG4gIHRoaXMuX3BsYW4gPSBkZWZpbmUucGxhbjtcbiAgdGhpcy5fY291bnQgPSBkZWZpbmUuY291bnQ7XG4gIHRoaXMuX29uRGlydHkgPSBkZWZpbmUub25EaXJ0eTtcbiAgdGhpcy5fZGlydHkgPSB0cnVlOyAvLyBDb250ZXh0IG11c3QgYmUgc3BlY2lmaWVkIGltcGxpY2l0bHksIHRvXG4gIC8vIGF2b2lkIG1pc3MgdXBkYXRlIGNvbnRleHQgd2hlbiBtb2RlbCBjaGFuZ2VkLlxuXG4gIHRoaXMuY29udGV4dDtcbn1cblxudmFyIHRhc2tQcm90byA9IFRhc2sucHJvdG90eXBlO1xuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gcGVyZm9ybUFyZ3NcbiAqIEBwYXJhbSB7bnVtYmVyfSBbcGVyZm9ybUFyZ3Muc3RlcF0gU3BlY2lmaWVkIHN0ZXAuXG4gKiBAcGFyYW0ge251bWJlcn0gW3BlcmZvcm1BcmdzLnNraXBdIFNraXAgY3VzdG9tZXIgcGVyZm9ybSBjYWxsLlxuICogQHBhcmFtIHtudW1iZXJ9IFtwZXJmb3JtQXJncy5tb2RCeV0gU2FtcGxpbmcgd2luZG93IHNpemUuXG4gKiBAcGFyYW0ge251bWJlcn0gW3BlcmZvcm1BcmdzLm1vZERhdGFDb3VudF0gU2FtcGxpbmcgY291bnQuXG4gKi9cblxudGFza1Byb3RvLnBlcmZvcm0gPSBmdW5jdGlvbiAocGVyZm9ybUFyZ3MpIHtcbiAgdmFyIHVwVGFzayA9IHRoaXMuX3Vwc3RyZWFtO1xuICB2YXIgc2tpcCA9IHBlcmZvcm1BcmdzICYmIHBlcmZvcm1BcmdzLnNraXA7IC8vIFRPRE8gc29tZSByZWZhY3Rvci5cbiAgLy8gUHVsbCBkYXRhLiBNdXN0IHB1bGwgZGF0YSBlYWNoIHRpbWUsIGJlY2F1c2UgY29udGV4dC5kYXRhXG4gIC8vIG1heSBiZSB1cGRhdGVkIGJ5IFNlcmllcy5zZXREYXRhLlxuXG4gIGlmICh0aGlzLl9kaXJ0eSAmJiB1cFRhc2spIHtcbiAgICB2YXIgY29udGV4dCA9IHRoaXMuY29udGV4dDtcbiAgICBjb250ZXh0LmRhdGEgPSBjb250ZXh0Lm91dHB1dERhdGEgPSB1cFRhc2suY29udGV4dC5vdXRwdXREYXRhO1xuICB9XG5cbiAgaWYgKHRoaXMuX19waXBlbGluZSkge1xuICAgIHRoaXMuX19waXBlbGluZS5jdXJyZW50VGFzayA9IHRoaXM7XG4gIH1cblxuICB2YXIgcGxhblJlc3VsdDtcblxuICBpZiAodGhpcy5fcGxhbiAmJiAhc2tpcCkge1xuICAgIHBsYW5SZXN1bHQgPSB0aGlzLl9wbGFuKHRoaXMuY29udGV4dCk7XG4gIH0gLy8gU3VwcG9ydCBzaGFyZGluZyBieSBtb2QsIHdoaWNoIGNoYW5nZXMgdGhlIHJlbmRlciBzZXF1ZW5jZSBhbmQgbWFrZXMgdGhlIHJlbmRlcmVkIGdyYXBoaWNcbiAgLy8gZWxlbWVudHMgdW5pZm9ybWVkIGRpc3RyaWJ1dGVkIHdoZW4gcHJvZ3Jlc3MsIGVzcGVjaWFsbHkgd2hlbiBtb3Zpbmcgb3Igem9vbWluZy5cblxuXG4gIHZhciBsYXN0TW9kQnkgPSBub3JtYWxpemVNb2RCeSh0aGlzLl9tb2RCeSk7XG4gIHZhciBsYXN0TW9kRGF0YUNvdW50ID0gdGhpcy5fbW9kRGF0YUNvdW50IHx8IDA7XG4gIHZhciBtb2RCeSA9IG5vcm1hbGl6ZU1vZEJ5KHBlcmZvcm1BcmdzICYmIHBlcmZvcm1BcmdzLm1vZEJ5KTtcbiAgdmFyIG1vZERhdGFDb3VudCA9IHBlcmZvcm1BcmdzICYmIHBlcmZvcm1BcmdzLm1vZERhdGFDb3VudCB8fCAwO1xuXG4gIGlmIChsYXN0TW9kQnkgIT09IG1vZEJ5IHx8IGxhc3RNb2REYXRhQ291bnQgIT09IG1vZERhdGFDb3VudCkge1xuICAgIHBsYW5SZXN1bHQgPSAncmVzZXQnO1xuICB9XG5cbiAgZnVuY3Rpb24gbm9ybWFsaXplTW9kQnkodmFsKSB7XG4gICAgISh2YWwgPj0gMSkgJiYgKHZhbCA9IDEpOyAvLyBqc2hpbnQgaWdub3JlOmxpbmVcblxuICAgIHJldHVybiB2YWw7XG4gIH1cblxuICB2YXIgZm9yY2VGaXJzdFByb2dyZXNzO1xuXG4gIGlmICh0aGlzLl9kaXJ0eSB8fCBwbGFuUmVzdWx0ID09PSAncmVzZXQnKSB7XG4gICAgdGhpcy5fZGlydHkgPSBmYWxzZTtcbiAgICBmb3JjZUZpcnN0UHJvZ3Jlc3MgPSByZXNldCh0aGlzLCBza2lwKTtcbiAgfVxuXG4gIHRoaXMuX21vZEJ5ID0gbW9kQnk7XG4gIHRoaXMuX21vZERhdGFDb3VudCA9IG1vZERhdGFDb3VudDtcbiAgdmFyIHN0ZXAgPSBwZXJmb3JtQXJncyAmJiBwZXJmb3JtQXJncy5zdGVwO1xuXG4gIGlmICh1cFRhc2spIHtcbiAgICB0aGlzLl9kdWVFbmQgPSB1cFRhc2suX291dHB1dER1ZUVuZDtcbiAgfSAvLyBEYXRhVGFzayBvciBvdmVyYWxsVGFza1xuICBlbHNlIHtcbiAgICAgIHRoaXMuX2R1ZUVuZCA9IHRoaXMuX2NvdW50ID8gdGhpcy5fY291bnQodGhpcy5jb250ZXh0KSA6IEluZmluaXR5O1xuICAgIH0gLy8gTm90ZTogU3R1YnMsIHRoYXQgaXRzIGhvc3Qgb3ZlcmFsbCB0YXNrIGxldCBpdCBoYXMgcHJvZ3Jlc3MsIGhhcyBwcm9ncmVzcy5cbiAgLy8gSWYgbm8gcHJvZ3Jlc3MsIHBhc3MgaW5kZXggZnJvbSB1cHN0cmVhbSB0byBkb3duc3RyZWFtIGVhY2ggdGltZSBwbGFuIGNhbGxlZC5cblxuXG4gIGlmICh0aGlzLl9wcm9ncmVzcykge1xuICAgIHZhciBzdGFydCA9IHRoaXMuX2R1ZUluZGV4O1xuICAgIHZhciBlbmQgPSBNYXRoLm1pbihzdGVwICE9IG51bGwgPyB0aGlzLl9kdWVJbmRleCArIHN0ZXAgOiBJbmZpbml0eSwgdGhpcy5fZHVlRW5kKTtcblxuICAgIGlmICghc2tpcCAmJiAoZm9yY2VGaXJzdFByb2dyZXNzIHx8IHN0YXJ0IDwgZW5kKSkge1xuICAgICAgdmFyIHByb2dyZXNzID0gdGhpcy5fcHJvZ3Jlc3M7XG5cbiAgICAgIGlmIChpc0FycmF5KHByb2dyZXNzKSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb2dyZXNzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgZG9Qcm9ncmVzcyh0aGlzLCBwcm9ncmVzc1tpXSwgc3RhcnQsIGVuZCwgbW9kQnksIG1vZERhdGFDb3VudCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRvUHJvZ3Jlc3ModGhpcywgcHJvZ3Jlc3MsIHN0YXJ0LCBlbmQsIG1vZEJ5LCBtb2REYXRhQ291bnQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX2R1ZUluZGV4ID0gZW5kOyAvLyBJZiBubyBgb3V0cHV0RHVlRW5kYCwgYXNzdW1lIHRoYXQgb3V0cHV0IGRhdGEgYW5kXG4gICAgLy8gaW5wdXQgZGF0YSBpcyB0aGUgc2FtZSwgc28gdXNlIGBkdWVJbmRleGAgYXMgYG91dHB1dER1ZUVuZGAuXG5cbiAgICB2YXIgb3V0cHV0RHVlRW5kID0gdGhpcy5fc2V0dGVkT3V0cHV0RW5kICE9IG51bGwgPyB0aGlzLl9zZXR0ZWRPdXRwdXRFbmQgOiBlbmQ7XG4gICAgdGhpcy5fb3V0cHV0RHVlRW5kID0gb3V0cHV0RHVlRW5kO1xuICB9IGVsc2Uge1xuICAgIC8vICgxKSBTb21lIG92ZXJhbGwgdGFzayBoYXMgbm8gcHJvZ3Jlc3MuXG4gICAgLy8gKDIpIFN0dWJzLCB0aGF0IGl0cyBob3N0IG92ZXJhbGwgdGFzayBkbyBub3QgbGV0IGl0IGhhcyBwcm9ncmVzcywgaGFzIG5vIHByb2dyZXNzLlxuICAgIC8vIFRoaXMgc2hvdWxkIGFsd2F5cyBiZSBwZXJmb3JtZWQgc28gaXQgY2FuIGJlIHBhc3NlZCB0byBkb3duc3RyZWFtLlxuICAgIHRoaXMuX2R1ZUluZGV4ID0gdGhpcy5fb3V0cHV0RHVlRW5kID0gdGhpcy5fc2V0dGVkT3V0cHV0RW5kICE9IG51bGwgPyB0aGlzLl9zZXR0ZWRPdXRwdXRFbmQgOiB0aGlzLl9kdWVFbmQ7XG4gIH1cblxuICByZXR1cm4gdGhpcy51bmZpbmlzaGVkKCk7XG59O1xuXG52YXIgaXRlcmF0b3IgPSBmdW5jdGlvbiAoKSB7XG4gIHZhciBlbmQ7XG4gIHZhciBjdXJyZW50O1xuICB2YXIgbW9kQnk7XG4gIHZhciBtb2REYXRhQ291bnQ7XG4gIHZhciB3aW5Db3VudDtcbiAgdmFyIGl0ID0ge1xuICAgIHJlc2V0OiBmdW5jdGlvbiAocywgZSwgc1N0ZXAsIHNDb3VudCkge1xuICAgICAgY3VycmVudCA9IHM7XG4gICAgICBlbmQgPSBlO1xuICAgICAgbW9kQnkgPSBzU3RlcDtcbiAgICAgIG1vZERhdGFDb3VudCA9IHNDb3VudDtcbiAgICAgIHdpbkNvdW50ID0gTWF0aC5jZWlsKG1vZERhdGFDb3VudCAvIG1vZEJ5KTtcbiAgICAgIGl0Lm5leHQgPSBtb2RCeSA+IDEgJiYgbW9kRGF0YUNvdW50ID4gMCA/IG1vZE5leHQgOiBzZXF1ZW50aWFsTmV4dDtcbiAgICB9XG4gIH07XG4gIHJldHVybiBpdDtcblxuICBmdW5jdGlvbiBzZXF1ZW50aWFsTmV4dCgpIHtcbiAgICByZXR1cm4gY3VycmVudCA8IGVuZCA/IGN1cnJlbnQrKyA6IG51bGw7XG4gIH1cblxuICBmdW5jdGlvbiBtb2ROZXh0KCkge1xuICAgIHZhciBkYXRhSW5kZXggPSBjdXJyZW50ICUgd2luQ291bnQgKiBtb2RCeSArIE1hdGguY2VpbChjdXJyZW50IC8gd2luQ291bnQpO1xuICAgIHZhciByZXN1bHQgPSBjdXJyZW50ID49IGVuZCA/IG51bGwgOiBkYXRhSW5kZXggPCBtb2REYXRhQ291bnQgPyBkYXRhSW5kZXggLy8gSWYgbW9kRGF0YUNvdW50IGlzIHNtYWxsZXIgdGhhbiBkYXRhLmNvdW50KCkgKGNvbnNpZGVyIGBhcHBlbmREYXRhYCBjYXNlKSxcbiAgICAvLyBVc2Ugbm9ybWFsIGxpbmVhciByZW5kZXJpbmcgbW9kZS5cbiAgICA6IGN1cnJlbnQ7XG4gICAgY3VycmVudCsrO1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbn0oKTtcblxudGFza1Byb3RvLmRpcnR5ID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLl9kaXJ0eSA9IHRydWU7XG4gIHRoaXMuX29uRGlydHkgJiYgdGhpcy5fb25EaXJ0eSh0aGlzLmNvbnRleHQpO1xufTtcblxuZnVuY3Rpb24gZG9Qcm9ncmVzcyh0YXNrSW5zLCBwcm9ncmVzcywgc3RhcnQsIGVuZCwgbW9kQnksIG1vZERhdGFDb3VudCkge1xuICBpdGVyYXRvci5yZXNldChzdGFydCwgZW5kLCBtb2RCeSwgbW9kRGF0YUNvdW50KTtcbiAgdGFza0lucy5fY2FsbGluZ1Byb2dyZXNzID0gcHJvZ3Jlc3M7XG5cbiAgdGFza0lucy5fY2FsbGluZ1Byb2dyZXNzKHtcbiAgICBzdGFydDogc3RhcnQsXG4gICAgZW5kOiBlbmQsXG4gICAgY291bnQ6IGVuZCAtIHN0YXJ0LFxuICAgIG5leHQ6IGl0ZXJhdG9yLm5leHRcbiAgfSwgdGFza0lucy5jb250ZXh0KTtcbn1cblxuZnVuY3Rpb24gcmVzZXQodGFza0lucywgc2tpcCkge1xuICB0YXNrSW5zLl9kdWVJbmRleCA9IHRhc2tJbnMuX291dHB1dER1ZUVuZCA9IHRhc2tJbnMuX2R1ZUVuZCA9IDA7XG4gIHRhc2tJbnMuX3NldHRlZE91dHB1dEVuZCA9IG51bGw7XG4gIHZhciBwcm9ncmVzcztcbiAgdmFyIGZvcmNlRmlyc3RQcm9ncmVzcztcblxuICBpZiAoIXNraXAgJiYgdGFza0lucy5fcmVzZXQpIHtcbiAgICBwcm9ncmVzcyA9IHRhc2tJbnMuX3Jlc2V0KHRhc2tJbnMuY29udGV4dCk7XG5cbiAgICBpZiAocHJvZ3Jlc3MgJiYgcHJvZ3Jlc3MucHJvZ3Jlc3MpIHtcbiAgICAgIGZvcmNlRmlyc3RQcm9ncmVzcyA9IHByb2dyZXNzLmZvcmNlRmlyc3RQcm9ncmVzcztcbiAgICAgIHByb2dyZXNzID0gcHJvZ3Jlc3MucHJvZ3Jlc3M7XG4gICAgfSAvLyBUbyBzaW1wbGlmeSBubyBwcm9ncmVzcyBjaGVja2luZywgYXJyYXkgbXVzdCBoYXMgaXRlbS5cblxuXG4gICAgaWYgKGlzQXJyYXkocHJvZ3Jlc3MpICYmICFwcm9ncmVzcy5sZW5ndGgpIHtcbiAgICAgIHByb2dyZXNzID0gbnVsbDtcbiAgICB9XG4gIH1cblxuICB0YXNrSW5zLl9wcm9ncmVzcyA9IHByb2dyZXNzO1xuICB0YXNrSW5zLl9tb2RCeSA9IHRhc2tJbnMuX21vZERhdGFDb3VudCA9IG51bGw7XG4gIHZhciBkb3duc3RyZWFtID0gdGFza0lucy5fZG93bnN0cmVhbTtcbiAgZG93bnN0cmVhbSAmJiBkb3duc3RyZWFtLmRpcnR5KCk7XG4gIHJldHVybiBmb3JjZUZpcnN0UHJvZ3Jlc3M7XG59XG4vKipcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG50YXNrUHJvdG8udW5maW5pc2hlZCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX3Byb2dyZXNzICYmIHRoaXMuX2R1ZUluZGV4IDwgdGhpcy5fZHVlRW5kO1xufTtcbi8qKlxuICogQHBhcmFtIHtPYmplY3R9IGRvd25UYXNrIFRoZSBkb3duc3RyZWFtIHRhc2suXG4gKiBAcmV0dXJuIHtPYmplY3R9IFRoZSBkb3duc3RyZWFtIHRhc2suXG4gKi9cblxuXG50YXNrUHJvdG8ucGlwZSA9IGZ1bmN0aW9uIChkb3duVGFzaykge1xuICAvLyBJZiBhbHJlYWR5IGRvd25zdHJlYW0sIGRvIG5vdCBkaXJ0eSBkb3duVGFzay5cbiAgaWYgKHRoaXMuX2Rvd25zdHJlYW0gIT09IGRvd25UYXNrIHx8IHRoaXMuX2RpcnR5KSB7XG4gICAgdGhpcy5fZG93bnN0cmVhbSA9IGRvd25UYXNrO1xuICAgIGRvd25UYXNrLl91cHN0cmVhbSA9IHRoaXM7XG4gICAgZG93blRhc2suZGlydHkoKTtcbiAgfVxufTtcblxudGFza1Byb3RvLmRpc3Bvc2UgPSBmdW5jdGlvbiAoKSB7XG4gIGlmICh0aGlzLl9kaXNwb3NlZCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHRoaXMuX3Vwc3RyZWFtICYmICh0aGlzLl91cHN0cmVhbS5fZG93bnN0cmVhbSA9IG51bGwpO1xuICB0aGlzLl9kb3duc3RyZWFtICYmICh0aGlzLl9kb3duc3RyZWFtLl91cHN0cmVhbSA9IG51bGwpO1xuICB0aGlzLl9kaXJ0eSA9IGZhbHNlO1xuICB0aGlzLl9kaXNwb3NlZCA9IHRydWU7XG59O1xuXG50YXNrUHJvdG8uZ2V0VXBzdHJlYW0gPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl91cHN0cmVhbTtcbn07XG5cbnRhc2tQcm90by5nZXREb3duc3RyZWFtID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fZG93bnN0cmVhbTtcbn07XG5cbnRhc2tQcm90by5zZXRPdXRwdXRFbmQgPSBmdW5jdGlvbiAoZW5kKSB7XG4gIC8vIFRoaXMgb25seSBoYXBwZW5kIGluIGRhdGFUYXNrLCBkYXRhWm9vbSwgbWFwLCBjdXJyZW50bHkuXG4gIC8vIHdoZXJlIGRhdGFab29tIGRvIG5vdCBzZXQgZW5kIGVhY2ggdGltZSwgYnV0IG9ubHkgc2V0XG4gIC8vIHdoZW4gcmVzZXQuIFNvIHdlIHNob3VsZCByZWNvcmQgdGhlIHNldHRlZCBlbmQsIGluIGNhc2VcbiAgLy8gdGhhdCB0aGUgc3R1YiBvZiBkYXRhWm9vbSBwZXJmb3JtIGFnYWluIGFuZCBlYXJzZSB0aGVcbiAgLy8gc2V0dGVkIGVuZCBieSB1cHN0cmVhbS5cbiAgdGhpcy5fb3V0cHV0RHVlRW5kID0gdGhpcy5fc2V0dGVkT3V0cHV0RW5kID0gZW5kO1xufTsgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIEZvciBzdHJlYW0gZGVidWcgKFNob3VsZCBiZSBjb21tZW50ZWQgb3V0IGFmdGVyIHVzZWQhKVxuLy8gVXNhZ2U6IHByaW50VGFzayh0aGlzLCAnYmVnaW4nKTtcbi8vIFVzYWdlOiBwcmludFRhc2sodGhpcywgbnVsbCwge3NvbWVFeHRyYVByb3B9KTtcbi8vIGZ1bmN0aW9uIHByaW50VGFzayh0YXNrLCBwcmVmaXgsIGV4dHJhKSB7XG4vLyAgICAgd2luZG93LmVjVGFza1VJRCA9PSBudWxsICYmICh3aW5kb3cuZWNUYXNrVUlEID0gMCk7XG4vLyAgICAgdGFzay51aWREZWJ1ZyA9PSBudWxsICYmICh0YXNrLnVpZERlYnVnID0gYHRhc2tfJHt3aW5kb3cuZWNUYXNrVUlEKyt9YCk7XG4vLyAgICAgdGFzay5hZ2VudCAmJiB0YXNrLmFnZW50LnVpZERlYnVnID09IG51bGwgJiYgKHRhc2suYWdlbnQudWlkRGVidWcgPSBgdGFza18ke3dpbmRvdy5lY1Rhc2tVSUQrK31gKTtcbi8vICAgICB2YXIgcHJvcHMgPSBbXTtcbi8vICAgICBpZiAodGFzay5fX3BpcGVsaW5lKSB7XG4vLyAgICAgICAgIHZhciB2YWwgPSBgJHt0YXNrLl9faWR4SW5QaXBlbGluZX0vJHt0YXNrLl9fcGlwZWxpbmUudGFpbC5fX2lkeEluUGlwZWxpbmV9ICR7dGFzay5hZ2VudCA/ICcoc3R1YiknIDogJyd9YDtcbi8vICAgICAgICAgcHJvcHMucHVzaCh7dGV4dDogJ2lkeCcsIHZhbHVlOiB2YWx9KTtcbi8vICAgICB9IGVsc2Uge1xuLy8gICAgICAgICB2YXIgc3R1YkNvdW50ID0gMDtcbi8vICAgICAgICAgdGFzay5hZ2VudFN0dWJNYXAuZWFjaCgoKSA9PiBzdHViQ291bnQrKyk7XG4vLyAgICAgICAgIHByb3BzLnB1c2goe3RleHQ6ICdpZHgnLCB2YWx1ZTogYG92ZXJhbGwgKHN0dWJzOiAke3N0dWJDb3VudH0pYH0pO1xuLy8gICAgIH1cbi8vICAgICBwcm9wcy5wdXNoKHt0ZXh0OiAndWlkJywgdmFsdWU6IHRhc2sudWlkRGVidWd9KTtcbi8vICAgICBpZiAodGFzay5fX3BpcGVsaW5lKSB7XG4vLyAgICAgICAgIHByb3BzLnB1c2goe3RleHQ6ICdwaWQnLCB2YWx1ZTogdGFzay5fX3BpcGVsaW5lLmlkfSk7XG4vLyAgICAgICAgIHRhc2suYWdlbnQgJiYgcHJvcHMucHVzaChcbi8vICAgICAgICAgICAgIHt0ZXh0OiAnc3R1YkZvcicsIHZhbHVlOiB0YXNrLmFnZW50LnVpZERlYnVnfVxuLy8gICAgICAgICApO1xuLy8gICAgIH1cbi8vICAgICBwcm9wcy5wdXNoKFxuLy8gICAgICAgICB7dGV4dDogJ2RpcnR5JywgdmFsdWU6IHRhc2suX2RpcnR5fSxcbi8vICAgICAgICAge3RleHQ6ICdkdWVJbmRleCcsIHZhbHVlOiB0YXNrLl9kdWVJbmRleH0sXG4vLyAgICAgICAgIHt0ZXh0OiAnZHVlRW5kJywgdmFsdWU6IHRhc2suX2R1ZUVuZH0sXG4vLyAgICAgICAgIHt0ZXh0OiAnb3V0cHV0RHVlRW5kJywgdmFsdWU6IHRhc2suX291dHB1dER1ZUVuZH1cbi8vICAgICApO1xuLy8gICAgIGlmIChleHRyYSkge1xuLy8gICAgICAgICBPYmplY3Qua2V5cyhleHRyYSkuZm9yRWFjaChrZXkgPT4ge1xuLy8gICAgICAgICAgICAgcHJvcHMucHVzaCh7dGV4dDoga2V5LCB2YWx1ZTogZXh0cmFba2V5XX0pO1xuLy8gICAgICAgICB9KTtcbi8vICAgICB9XG4vLyAgICAgdmFyIGFyZ3MgPSBbJ2NvbG9yOiBibHVlJ107XG4vLyAgICAgdmFyIG1zZyA9IGAlY1ske3ByZWZpeCB8fCAnVCd9XSAlY2AgKyBwcm9wcy5tYXAoaXRlbSA9PiAoXG4vLyAgICAgICAgIGFyZ3MucHVzaCgnY29sb3I6IGJsYWNrJywgJ2NvbG9yOiByZWQnKSxcbi8vICAgICAgICAgYCR7aXRlbS50ZXh0fTogJWMke2l0ZW0udmFsdWV9YFxuLy8gICAgICkpLmpvaW4oJyVjLCAnKTtcbi8vICAgICBjb25zb2xlLmxvZy5hcHBseShjb25zb2xlLCBbbXNnXS5jb25jYXQoYXJncykpO1xuLy8gICAgIC8vIGNvbnNvbGUubG9nKHRoaXMpO1xuLy8gfVxuXG5cbmV4cG9ydHMuY3JlYXRlVGFzayA9IGNyZWF0ZVRhc2s7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/stream/task.js
