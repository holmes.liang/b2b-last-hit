/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnCompositionStart
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");
/**
 * The user has begun using an IME input system. Switching to `composite` mode
 * allows handling composition input and disables other edit behavior.
 */


function editOnCompositionStart(editor, e) {
  editor.setMode('composite');
  editor.update(EditorState.set(editor._latestEditorState, {
    inCompositionMode: true
  })); // Allow composition handler to interpret the compositionstart event

  editor._onCompositionStart(e);
}

module.exports = editOnCompositionStart;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkNvbXBvc2l0aW9uU3RhcnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZWRpdE9uQ29tcG9zaXRpb25TdGFydC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGVkaXRPbkNvbXBvc2l0aW9uU3RhcnRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG4vKipcbiAqIFRoZSB1c2VyIGhhcyBiZWd1biB1c2luZyBhbiBJTUUgaW5wdXQgc3lzdGVtLiBTd2l0Y2hpbmcgdG8gYGNvbXBvc2l0ZWAgbW9kZVxuICogYWxsb3dzIGhhbmRsaW5nIGNvbXBvc2l0aW9uIGlucHV0IGFuZCBkaXNhYmxlcyBvdGhlciBlZGl0IGJlaGF2aW9yLlxuICovXG5mdW5jdGlvbiBlZGl0T25Db21wb3NpdGlvblN0YXJ0KGVkaXRvciwgZSkge1xuICBlZGl0b3Iuc2V0TW9kZSgnY29tcG9zaXRlJyk7XG4gIGVkaXRvci51cGRhdGUoRWRpdG9yU3RhdGUuc2V0KGVkaXRvci5fbGF0ZXN0RWRpdG9yU3RhdGUsIHsgaW5Db21wb3NpdGlvbk1vZGU6IHRydWUgfSkpO1xuICAvLyBBbGxvdyBjb21wb3NpdGlvbiBoYW5kbGVyIHRvIGludGVycHJldCB0aGUgY29tcG9zaXRpb25zdGFydCBldmVudFxuICBlZGl0b3IuX29uQ29tcG9zaXRpb25TdGFydChlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlZGl0T25Db21wb3NpdGlvblN0YXJ0OyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnCompositionStart.js
