__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllSteps", function() { return AllSteps; });
var AllSteps;

(function (AllSteps) {
  AllSteps["QUOTE"] = "quote";
  AllSteps["PLANS"] = "plans";
  AllSteps["CUSTOMIZATION"] = "customization";
  AllSteps["DECLARATION"] = "Declaration";
  AllSteps["COMPARE"] = "compare";
  AllSteps["DETAILS"] = "details";
  AllSteps["RENEWAL_EDIT"] = "renewal-edit";
  AllSteps["CONFIRM"] = "confirm";
  AllSteps["CONFIRM_RN"] = "confirm-rn";
  AllSteps["PAYMENT"] = "payment";
  AllSteps["ISSUE"] = "issue";
  AllSteps["PAYMENT_SHARE"] = "payment-share";
  AllSteps["PAYMENT_SHARE_ENDO"] = "payment-share_endo";
  AllSteps["COMPARE_SHARE"] = "compare-share";
  AllSteps["SUSPENDED"] = "suspended";
  AllSteps["BP_SLIP"] = "bp-slip";
  AllSteps["PAYMENT_ENDO"] = "payment-endo";
  AllSteps["MOVEMENT_ENDO"] = "movement-endo";
  AllSteps["ISSUE_ENDO"] = "issue-endo";
  AllSteps["OPTIONAL_COVERS"] = "optional";
  AllSteps["ADD_ONS"] = "add-ons";
  AllSteps["CUSTOMISE"] = "customise";
  AllSteps["EMPLOYEE_INFO"] = "employee";
  AllSteps["QUESTION"] = "questionnaie";
  AllSteps["BUSINESS"] = "business";
  AllSteps["LOCATION"] = "location";
  AllSteps["ADDLOCATION"] = "add-location";
  AllSteps["CLAUSE"] = "clause";
  AllSteps["ATTACHMENTS"] = "attachments";
  AllSteps["SUBMIT"] = "submit";
  AllSteps["RIDERS"] = "riders";
  AllSteps["CERTIFICATE_ENTRY"] = "certificate-entry";
  AllSteps["MASTER_POLICY"] = "master-policy";
})(AllSteps || (AllSteps = {}));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9hbGwtc3RlcHMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9hbGwtc3RlcHMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIEFsbFN0ZXBzIHtcbiAgUVVPVEUgPSBcInF1b3RlXCIsXG4gIFBMQU5TID0gXCJwbGFuc1wiLFxuICBDVVNUT01JWkFUSU9OID0gXCJjdXN0b21pemF0aW9uXCIsXG4gIERFQ0xBUkFUSU9OID0gXCJEZWNsYXJhdGlvblwiLFxuICBDT01QQVJFID0gXCJjb21wYXJlXCIsXG4gIERFVEFJTFMgPSBcImRldGFpbHNcIixcbiAgUkVORVdBTF9FRElUID0gXCJyZW5ld2FsLWVkaXRcIixcbiAgQ09ORklSTSA9IFwiY29uZmlybVwiLFxuICBDT05GSVJNX1JOID0gXCJjb25maXJtLXJuXCIsXG4gIFBBWU1FTlQgPSBcInBheW1lbnRcIixcbiAgSVNTVUUgPSBcImlzc3VlXCIsXG4gIFBBWU1FTlRfU0hBUkUgPSBcInBheW1lbnQtc2hhcmVcIixcbiAgUEFZTUVOVF9TSEFSRV9FTkRPID0gXCJwYXltZW50LXNoYXJlX2VuZG9cIixcbiAgQ09NUEFSRV9TSEFSRSA9IFwiY29tcGFyZS1zaGFyZVwiLFxuICBTVVNQRU5ERUQgPSBcInN1c3BlbmRlZFwiLFxuICBCUF9TTElQID0gXCJicC1zbGlwXCIsXG4gIFBBWU1FTlRfRU5ETyA9IFwicGF5bWVudC1lbmRvXCIsXG4gIE1PVkVNRU5UX0VORE8gPSBcIm1vdmVtZW50LWVuZG9cIixcbiAgSVNTVUVfRU5ETyA9IFwiaXNzdWUtZW5kb1wiLFxuICBPUFRJT05BTF9DT1ZFUlMgPSBcIm9wdGlvbmFsXCIsXG4gIEFERF9PTlMgPSBcImFkZC1vbnNcIixcbiAgQ1VTVE9NSVNFID0gXCJjdXN0b21pc2VcIixcbiAgRU1QTE9ZRUVfSU5GTyA9IFwiZW1wbG95ZWVcIixcbiAgUVVFU1RJT04gPSBcInF1ZXN0aW9ubmFpZVwiLFxuICBCVVNJTkVTUyA9IFwiYnVzaW5lc3NcIixcbiAgTE9DQVRJT04gPSBcImxvY2F0aW9uXCIsXG4gIEFERExPQ0FUSU9OID0gXCJhZGQtbG9jYXRpb25cIixcbiAgQ0xBVVNFID0gXCJjbGF1c2VcIixcbiAgQVRUQUNITUVOVFMgPSBcImF0dGFjaG1lbnRzXCIsXG4gIFNVQk1JVCA9IFwic3VibWl0XCIsXG4gIFJJREVSUyA9IFwicmlkZXJzXCIsXG4gIENFUlRJRklDQVRFX0VOVFJZID0gXCJjZXJ0aWZpY2F0ZS1lbnRyeVwiLFxuXG4gIE1BU1RFUl9QT0xJQ1kgPSBcIm1hc3Rlci1wb2xpY3lcIixcbn1cbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/all-steps.tsx
