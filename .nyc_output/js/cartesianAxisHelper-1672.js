/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Can only be called after coordinate system creation stage.
 * (Can be called before coordinate system update stage).
 *
 * @param {Object} opt {labelInside}
 * @return {Object} {
 *  position, rotation, labelDirection, labelOffset,
 *  tickDirection, labelRotate, z2
 * }
 */


function layout(gridModel, axisModel, opt) {
  opt = opt || {};
  var grid = gridModel.coordinateSystem;
  var axis = axisModel.axis;
  var layout = {};
  var otherAxisOnZeroOf = axis.getAxesOnZeroOf()[0];
  var rawAxisPosition = axis.position;
  var axisPosition = otherAxisOnZeroOf ? 'onZero' : rawAxisPosition;
  var axisDim = axis.dim;
  var rect = grid.getRect();
  var rectBound = [rect.x, rect.x + rect.width, rect.y, rect.y + rect.height];
  var idx = {
    left: 0,
    right: 1,
    top: 0,
    bottom: 1,
    onZero: 2
  };
  var axisOffset = axisModel.get('offset') || 0;
  var posBound = axisDim === 'x' ? [rectBound[2] - axisOffset, rectBound[3] + axisOffset] : [rectBound[0] - axisOffset, rectBound[1] + axisOffset];

  if (otherAxisOnZeroOf) {
    var onZeroCoord = otherAxisOnZeroOf.toGlobalCoord(otherAxisOnZeroOf.dataToCoord(0));
    posBound[idx.onZero] = Math.max(Math.min(onZeroCoord, posBound[1]), posBound[0]);
  } // Axis position


  layout.position = [axisDim === 'y' ? posBound[idx[axisPosition]] : rectBound[0], axisDim === 'x' ? posBound[idx[axisPosition]] : rectBound[3]]; // Axis rotation

  layout.rotation = Math.PI / 2 * (axisDim === 'x' ? 0 : 1); // Tick and label direction, x y is axisDim

  var dirMap = {
    top: -1,
    bottom: 1,
    left: -1,
    right: 1
  };
  layout.labelDirection = layout.tickDirection = layout.nameDirection = dirMap[rawAxisPosition];
  layout.labelOffset = otherAxisOnZeroOf ? posBound[idx[rawAxisPosition]] - posBound[idx.onZero] : 0;

  if (axisModel.get('axisTick.inside')) {
    layout.tickDirection = -layout.tickDirection;
  }

  if (zrUtil.retrieve(opt.labelInside, axisModel.get('axisLabel.inside'))) {
    layout.labelDirection = -layout.labelDirection;
  } // Special label rotation


  var labelRotate = axisModel.get('axisLabel.rotate');
  layout.labelRotate = axisPosition === 'top' ? -labelRotate : labelRotate; // Over splitLine and splitArea

  layout.z2 = 1;
  return layout;
}

exports.layout = layout;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvY2FydGVzaWFuL2NhcnRlc2lhbkF4aXNIZWxwZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb29yZC9jYXJ0ZXNpYW4vY2FydGVzaWFuQXhpc0hlbHBlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBDYW4gb25seSBiZSBjYWxsZWQgYWZ0ZXIgY29vcmRpbmF0ZSBzeXN0ZW0gY3JlYXRpb24gc3RhZ2UuXG4gKiAoQ2FuIGJlIGNhbGxlZCBiZWZvcmUgY29vcmRpbmF0ZSBzeXN0ZW0gdXBkYXRlIHN0YWdlKS5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0IHtsYWJlbEluc2lkZX1cbiAqIEByZXR1cm4ge09iamVjdH0ge1xuICogIHBvc2l0aW9uLCByb3RhdGlvbiwgbGFiZWxEaXJlY3Rpb24sIGxhYmVsT2Zmc2V0LFxuICogIHRpY2tEaXJlY3Rpb24sIGxhYmVsUm90YXRlLCB6MlxuICogfVxuICovXG5mdW5jdGlvbiBsYXlvdXQoZ3JpZE1vZGVsLCBheGlzTW9kZWwsIG9wdCkge1xuICBvcHQgPSBvcHQgfHwge307XG4gIHZhciBncmlkID0gZ3JpZE1vZGVsLmNvb3JkaW5hdGVTeXN0ZW07XG4gIHZhciBheGlzID0gYXhpc01vZGVsLmF4aXM7XG4gIHZhciBsYXlvdXQgPSB7fTtcbiAgdmFyIG90aGVyQXhpc09uWmVyb09mID0gYXhpcy5nZXRBeGVzT25aZXJvT2YoKVswXTtcbiAgdmFyIHJhd0F4aXNQb3NpdGlvbiA9IGF4aXMucG9zaXRpb247XG4gIHZhciBheGlzUG9zaXRpb24gPSBvdGhlckF4aXNPblplcm9PZiA/ICdvblplcm8nIDogcmF3QXhpc1Bvc2l0aW9uO1xuICB2YXIgYXhpc0RpbSA9IGF4aXMuZGltO1xuICB2YXIgcmVjdCA9IGdyaWQuZ2V0UmVjdCgpO1xuICB2YXIgcmVjdEJvdW5kID0gW3JlY3QueCwgcmVjdC54ICsgcmVjdC53aWR0aCwgcmVjdC55LCByZWN0LnkgKyByZWN0LmhlaWdodF07XG4gIHZhciBpZHggPSB7XG4gICAgbGVmdDogMCxcbiAgICByaWdodDogMSxcbiAgICB0b3A6IDAsXG4gICAgYm90dG9tOiAxLFxuICAgIG9uWmVybzogMlxuICB9O1xuICB2YXIgYXhpc09mZnNldCA9IGF4aXNNb2RlbC5nZXQoJ29mZnNldCcpIHx8IDA7XG4gIHZhciBwb3NCb3VuZCA9IGF4aXNEaW0gPT09ICd4JyA/IFtyZWN0Qm91bmRbMl0gLSBheGlzT2Zmc2V0LCByZWN0Qm91bmRbM10gKyBheGlzT2Zmc2V0XSA6IFtyZWN0Qm91bmRbMF0gLSBheGlzT2Zmc2V0LCByZWN0Qm91bmRbMV0gKyBheGlzT2Zmc2V0XTtcblxuICBpZiAob3RoZXJBeGlzT25aZXJvT2YpIHtcbiAgICB2YXIgb25aZXJvQ29vcmQgPSBvdGhlckF4aXNPblplcm9PZi50b0dsb2JhbENvb3JkKG90aGVyQXhpc09uWmVyb09mLmRhdGFUb0Nvb3JkKDApKTtcbiAgICBwb3NCb3VuZFtpZHgub25aZXJvXSA9IE1hdGgubWF4KE1hdGgubWluKG9uWmVyb0Nvb3JkLCBwb3NCb3VuZFsxXSksIHBvc0JvdW5kWzBdKTtcbiAgfSAvLyBBeGlzIHBvc2l0aW9uXG5cblxuICBsYXlvdXQucG9zaXRpb24gPSBbYXhpc0RpbSA9PT0gJ3knID8gcG9zQm91bmRbaWR4W2F4aXNQb3NpdGlvbl1dIDogcmVjdEJvdW5kWzBdLCBheGlzRGltID09PSAneCcgPyBwb3NCb3VuZFtpZHhbYXhpc1Bvc2l0aW9uXV0gOiByZWN0Qm91bmRbM11dOyAvLyBBeGlzIHJvdGF0aW9uXG5cbiAgbGF5b3V0LnJvdGF0aW9uID0gTWF0aC5QSSAvIDIgKiAoYXhpc0RpbSA9PT0gJ3gnID8gMCA6IDEpOyAvLyBUaWNrIGFuZCBsYWJlbCBkaXJlY3Rpb24sIHggeSBpcyBheGlzRGltXG5cbiAgdmFyIGRpck1hcCA9IHtcbiAgICB0b3A6IC0xLFxuICAgIGJvdHRvbTogMSxcbiAgICBsZWZ0OiAtMSxcbiAgICByaWdodDogMVxuICB9O1xuICBsYXlvdXQubGFiZWxEaXJlY3Rpb24gPSBsYXlvdXQudGlja0RpcmVjdGlvbiA9IGxheW91dC5uYW1lRGlyZWN0aW9uID0gZGlyTWFwW3Jhd0F4aXNQb3NpdGlvbl07XG4gIGxheW91dC5sYWJlbE9mZnNldCA9IG90aGVyQXhpc09uWmVyb09mID8gcG9zQm91bmRbaWR4W3Jhd0F4aXNQb3NpdGlvbl1dIC0gcG9zQm91bmRbaWR4Lm9uWmVyb10gOiAwO1xuXG4gIGlmIChheGlzTW9kZWwuZ2V0KCdheGlzVGljay5pbnNpZGUnKSkge1xuICAgIGxheW91dC50aWNrRGlyZWN0aW9uID0gLWxheW91dC50aWNrRGlyZWN0aW9uO1xuICB9XG5cbiAgaWYgKHpyVXRpbC5yZXRyaWV2ZShvcHQubGFiZWxJbnNpZGUsIGF4aXNNb2RlbC5nZXQoJ2F4aXNMYWJlbC5pbnNpZGUnKSkpIHtcbiAgICBsYXlvdXQubGFiZWxEaXJlY3Rpb24gPSAtbGF5b3V0LmxhYmVsRGlyZWN0aW9uO1xuICB9IC8vIFNwZWNpYWwgbGFiZWwgcm90YXRpb25cblxuXG4gIHZhciBsYWJlbFJvdGF0ZSA9IGF4aXNNb2RlbC5nZXQoJ2F4aXNMYWJlbC5yb3RhdGUnKTtcbiAgbGF5b3V0LmxhYmVsUm90YXRlID0gYXhpc1Bvc2l0aW9uID09PSAndG9wJyA/IC1sYWJlbFJvdGF0ZSA6IGxhYmVsUm90YXRlOyAvLyBPdmVyIHNwbGl0TGluZSBhbmQgc3BsaXRBcmVhXG5cbiAgbGF5b3V0LnoyID0gMTtcbiAgcmV0dXJuIGxheW91dDtcbn1cblxuZXhwb3J0cy5sYXlvdXQgPSBsYXlvdXQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/cartesian/cartesianAxisHelper.js
