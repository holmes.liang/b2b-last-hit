__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    display: flex;\n    align-items: center;\n\n    .ant-calendar-picker {\n      input {\n        height: 32px;\n        max-width: 10em;\n      }\n    }\n\n    .ant-btn {\n      margin-left: .5em;\n    }\n\n    .filter-date-range {\n      &__splitor {\n        margin: .2em;\n      }\n    }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/* harmony default export */ __webpack_exports__["default"] = ({
  Scope: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZpbHRlci9maWx0ZXItZGF0ZXJhbmdlLWNoZWNrYm94LXN0eWxlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9maWx0ZXIvZmlsdGVyLWRhdGVyYW5nZS1jaGVja2JveC1zdHlsZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgU2NvcGU6IFN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgLmFudC1jYWxlbmRhci1waWNrZXIge1xuICAgICAgaW5wdXQge1xuICAgICAgICBoZWlnaHQ6IDMycHg7XG4gICAgICAgIG1heC13aWR0aDogMTBlbTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuYW50LWJ0biB7XG4gICAgICBtYXJnaW4tbGVmdDogLjVlbTtcbiAgICB9XG5cbiAgICAuZmlsdGVyLWRhdGUtcmFuZ2Uge1xuICAgICAgJl9fc3BsaXRvciB7XG4gICAgICAgIG1hcmdpbjogLjJlbTtcbiAgICAgIH1cbiAgICB9XG4gIGAsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/filter/filter-daterange-checkbox-style.tsx
