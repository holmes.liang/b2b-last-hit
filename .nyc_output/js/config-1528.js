var dpr = 1; // If in browser environment

if (typeof window !== 'undefined') {
  dpr = Math.max(window.devicePixelRatio || 1, 1);
}
/**
 * config默认配置项
 * @exports zrender/config
 * @author Kener (@Kener-林峰, kener.linfeng@gmail.com)
 */

/**
 * debug日志选项：catchBrushException为true下有效
 * 0 : 不生成debug数据，发布用
 * 1 : 异常抛出，调试用
 * 2 : 控制台输出，调试用
 */


var debugMode = 0; // retina 屏幕优化

var devicePixelRatio = dpr;
exports.debugMode = debugMode;
exports.devicePixelRatio = devicePixelRatio;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29uZmlnLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29uZmlnLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBkcHIgPSAxOyAvLyBJZiBpbiBicm93c2VyIGVudmlyb25tZW50XG5cbmlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJykge1xuICBkcHIgPSBNYXRoLm1heCh3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyB8fCAxLCAxKTtcbn1cbi8qKlxuICogY29uZmln6buY6K6k6YWN572u6aG5XG4gKiBAZXhwb3J0cyB6cmVuZGVyL2NvbmZpZ1xuICogQGF1dGhvciBLZW5lciAoQEtlbmVyLeael+WzsCwga2VuZXIubGluZmVuZ0BnbWFpbC5jb20pXG4gKi9cblxuLyoqXG4gKiBkZWJ1Z+aXpeW/l+mAiemhue+8mmNhdGNoQnJ1c2hFeGNlcHRpb27kuLp0cnVl5LiL5pyJ5pWIXG4gKiAwIDog5LiN55Sf5oiQZGVidWfmlbDmja7vvIzlj5HluIPnlKhcbiAqIDEgOiDlvILluLjmipvlh7rvvIzosIPor5XnlKhcbiAqIDIgOiDmjqfliLblj7DovpPlh7rvvIzosIPor5XnlKhcbiAqL1xuXG5cbnZhciBkZWJ1Z01vZGUgPSAwOyAvLyByZXRpbmEg5bGP5bmV5LyY5YyWXG5cbnZhciBkZXZpY2VQaXhlbFJhdGlvID0gZHByO1xuZXhwb3J0cy5kZWJ1Z01vZGUgPSBkZWJ1Z01vZGU7XG5leHBvcnRzLmRldmljZVBpeGVsUmF0aW8gPSBkZXZpY2VQaXhlbFJhdGlvOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/config.js
