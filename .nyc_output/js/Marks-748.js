__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);






var Marks = function Marks(_ref) {
  var className = _ref.className,
      vertical = _ref.vertical,
      reverse = _ref.reverse,
      marks = _ref.marks,
      included = _ref.included,
      upperBound = _ref.upperBound,
      lowerBound = _ref.lowerBound,
      max = _ref.max,
      min = _ref.min,
      onClickLabel = _ref.onClickLabel;
  var marksKeys = Object.keys(marks);
  var range = max - min;
  var elements = marksKeys.map(parseFloat).sort(function (a, b) {
    return a - b;
  }).map(function (point) {
    var _classNames;

    var markPoint = marks[point];
    var markPointIsObject = typeof markPoint === 'object' && !react__WEBPACK_IMPORTED_MODULE_2___default.a.isValidElement(markPoint);
    var markLabel = markPointIsObject ? markPoint.label : markPoint;

    if (!markLabel && markLabel !== 0) {
      return null;
    }

    var isActive = !included && point === upperBound || included && point <= upperBound && point >= lowerBound;
    var markClassName = classnames__WEBPACK_IMPORTED_MODULE_4___default()((_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_classNames, className + '-text', true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_classNames, className + '-text-active', isActive), _classNames));

    var bottomStyle = babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()({
      marginBottom: '-50%'
    }, reverse ? 'top' : 'bottom', (point - min) / range * 100 + '%');

    var leftStyle = babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()({
      transform: 'translateX(-50%)',
      msTransform: 'translateX(-50%)'
    }, reverse ? 'right' : 'left', reverse ? (point - min / 4) / range * 100 + '%' : (point - min) / range * 100 + '%');

    var style = vertical ? bottomStyle : leftStyle;
    var markStyle = markPointIsObject ? babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, style, markPoint.style) : style;
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('span', {
      className: markClassName,
      style: markStyle,
      key: point,
      onMouseDown: function onMouseDown(e) {
        return onClickLabel(e, point);
      },
      onTouchStart: function onTouchStart(e) {
        return onClickLabel(e, point);
      }
    }, markLabel);
  });
  return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('div', {
    className: className
  }, elements);
};

Marks.propTypes = {
  className: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string,
  vertical: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  reverse: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  marks: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  included: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  upperBound: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  lowerBound: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  max: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  min: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  onClickLabel: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (Marks);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL2NvbW1vbi9NYXJrcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNsaWRlci9lcy9jb21tb24vTWFya3MuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5JztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5cbnZhciBNYXJrcyA9IGZ1bmN0aW9uIE1hcmtzKF9yZWYpIHtcbiAgdmFyIGNsYXNzTmFtZSA9IF9yZWYuY2xhc3NOYW1lLFxuICAgICAgdmVydGljYWwgPSBfcmVmLnZlcnRpY2FsLFxuICAgICAgcmV2ZXJzZSA9IF9yZWYucmV2ZXJzZSxcbiAgICAgIG1hcmtzID0gX3JlZi5tYXJrcyxcbiAgICAgIGluY2x1ZGVkID0gX3JlZi5pbmNsdWRlZCxcbiAgICAgIHVwcGVyQm91bmQgPSBfcmVmLnVwcGVyQm91bmQsXG4gICAgICBsb3dlckJvdW5kID0gX3JlZi5sb3dlckJvdW5kLFxuICAgICAgbWF4ID0gX3JlZi5tYXgsXG4gICAgICBtaW4gPSBfcmVmLm1pbixcbiAgICAgIG9uQ2xpY2tMYWJlbCA9IF9yZWYub25DbGlja0xhYmVsO1xuXG4gIHZhciBtYXJrc0tleXMgPSBPYmplY3Qua2V5cyhtYXJrcyk7XG5cbiAgdmFyIHJhbmdlID0gbWF4IC0gbWluO1xuICB2YXIgZWxlbWVudHMgPSBtYXJrc0tleXMubWFwKHBhcnNlRmxvYXQpLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gYSAtIGI7XG4gIH0pLm1hcChmdW5jdGlvbiAocG9pbnQpIHtcbiAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICB2YXIgbWFya1BvaW50ID0gbWFya3NbcG9pbnRdO1xuICAgIHZhciBtYXJrUG9pbnRJc09iamVjdCA9IHR5cGVvZiBtYXJrUG9pbnQgPT09ICdvYmplY3QnICYmICFSZWFjdC5pc1ZhbGlkRWxlbWVudChtYXJrUG9pbnQpO1xuICAgIHZhciBtYXJrTGFiZWwgPSBtYXJrUG9pbnRJc09iamVjdCA/IG1hcmtQb2ludC5sYWJlbCA6IG1hcmtQb2ludDtcbiAgICBpZiAoIW1hcmtMYWJlbCAmJiBtYXJrTGFiZWwgIT09IDApIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIHZhciBpc0FjdGl2ZSA9ICFpbmNsdWRlZCAmJiBwb2ludCA9PT0gdXBwZXJCb3VuZCB8fCBpbmNsdWRlZCAmJiBwb2ludCA8PSB1cHBlckJvdW5kICYmIHBvaW50ID49IGxvd2VyQm91bmQ7XG4gICAgdmFyIG1hcmtDbGFzc05hbWUgPSBjbGFzc05hbWVzKChfY2xhc3NOYW1lcyA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIGNsYXNzTmFtZSArICctdGV4dCcsIHRydWUpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIGNsYXNzTmFtZSArICctdGV4dC1hY3RpdmUnLCBpc0FjdGl2ZSksIF9jbGFzc05hbWVzKSk7XG5cbiAgICB2YXIgYm90dG9tU3R5bGUgPSBfZGVmaW5lUHJvcGVydHkoe1xuICAgICAgbWFyZ2luQm90dG9tOiAnLTUwJSdcbiAgICB9LCByZXZlcnNlID8gJ3RvcCcgOiAnYm90dG9tJywgKHBvaW50IC0gbWluKSAvIHJhbmdlICogMTAwICsgJyUnKTtcblxuICAgIHZhciBsZWZ0U3R5bGUgPSBfZGVmaW5lUHJvcGVydHkoe1xuICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtNTAlKScsXG4gICAgICBtc1RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTUwJSknXG4gICAgfSwgcmV2ZXJzZSA/ICdyaWdodCcgOiAnbGVmdCcsIHJldmVyc2UgPyAocG9pbnQgLSBtaW4gLyA0KSAvIHJhbmdlICogMTAwICsgJyUnIDogKHBvaW50IC0gbWluKSAvIHJhbmdlICogMTAwICsgJyUnKTtcblxuICAgIHZhciBzdHlsZSA9IHZlcnRpY2FsID8gYm90dG9tU3R5bGUgOiBsZWZ0U3R5bGU7XG4gICAgdmFyIG1hcmtTdHlsZSA9IG1hcmtQb2ludElzT2JqZWN0ID8gX2V4dGVuZHMoe30sIHN0eWxlLCBtYXJrUG9pbnQuc3R5bGUpIDogc3R5bGU7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAnc3BhbicsXG4gICAgICB7XG4gICAgICAgIGNsYXNzTmFtZTogbWFya0NsYXNzTmFtZSxcbiAgICAgICAgc3R5bGU6IG1hcmtTdHlsZSxcbiAgICAgICAga2V5OiBwb2ludCxcbiAgICAgICAgb25Nb3VzZURvd246IGZ1bmN0aW9uIG9uTW91c2VEb3duKGUpIHtcbiAgICAgICAgICByZXR1cm4gb25DbGlja0xhYmVsKGUsIHBvaW50KTtcbiAgICAgICAgfSxcbiAgICAgICAgb25Ub3VjaFN0YXJ0OiBmdW5jdGlvbiBvblRvdWNoU3RhcnQoZSkge1xuICAgICAgICAgIHJldHVybiBvbkNsaWNrTGFiZWwoZSwgcG9pbnQpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgbWFya0xhYmVsXG4gICAgKTtcbiAgfSk7XG5cbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ2RpdicsXG4gICAgeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LFxuICAgIGVsZW1lbnRzXG4gICk7XG59O1xuXG5NYXJrcy5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgdmVydGljYWw6IFByb3BUeXBlcy5ib29sLFxuICByZXZlcnNlOiBQcm9wVHlwZXMuYm9vbCxcbiAgbWFya3M6IFByb3BUeXBlcy5vYmplY3QsXG4gIGluY2x1ZGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgdXBwZXJCb3VuZDogUHJvcFR5cGVzLm51bWJlcixcbiAgbG93ZXJCb3VuZDogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4OiBQcm9wVHlwZXMubnVtYmVyLFxuICBtaW46IFByb3BUeXBlcy5udW1iZXIsXG4gIG9uQ2xpY2tMYWJlbDogUHJvcFR5cGVzLmZ1bmNcbn07XG5cbmV4cG9ydCBkZWZhdWx0IE1hcmtzOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQWFBO0FBRUE7QUFFQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQWFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/common/Marks.js
