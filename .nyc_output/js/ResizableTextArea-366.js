__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-resize-observer */ "./node_modules/rc-resize-observer/es/index.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rc_resize_observer__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _calculateNodeHeight__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./calculateNodeHeight */ "./node_modules/antd/es/input/calculateNodeHeight.js");
/* harmony import */ var _util_raf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/raf */ "./node_modules/antd/es/_util/raf.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}










var ResizableTextArea =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ResizableTextArea, _React$Component);

  function ResizableTextArea(props) {
    var _this;

    _classCallCheck(this, ResizableTextArea);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ResizableTextArea).call(this, props));

    _this.saveTextArea = function (textArea) {
      _this.textArea = textArea;
    };

    _this.resizeOnNextFrame = function () {
      _util_raf__WEBPACK_IMPORTED_MODULE_6__["default"].cancel(_this.nextFrameActionId);
      _this.nextFrameActionId = Object(_util_raf__WEBPACK_IMPORTED_MODULE_6__["default"])(_this.resizeTextarea);
    };

    _this.resizeTextarea = function () {
      var autoSize = _this.props.autoSize || _this.props.autosize;

      if (!autoSize || !_this.textArea) {
        return;
      }

      var minRows = autoSize.minRows,
          maxRows = autoSize.maxRows;
      var textareaStyles = Object(_calculateNodeHeight__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.textArea, false, minRows, maxRows);

      _this.setState({
        textareaStyles: textareaStyles,
        resizing: true
      }, function () {
        _util_raf__WEBPACK_IMPORTED_MODULE_6__["default"].cancel(_this.resizeFrameId);
        _this.resizeFrameId = Object(_util_raf__WEBPACK_IMPORTED_MODULE_6__["default"])(function () {
          _this.setState({
            resizing: false
          });
        });
      });
    };

    _this.renderTextArea = function () {
      var _this$props = _this.props,
          prefixCls = _this$props.prefixCls,
          autoSize = _this$props.autoSize,
          autosize = _this$props.autosize,
          className = _this$props.className,
          disabled = _this$props.disabled;
      var _this$state = _this.state,
          textareaStyles = _this$state.textareaStyles,
          resizing = _this$state.resizing;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_7__["default"])(autosize === undefined, 'Input.TextArea', 'autosize is deprecated, please use autoSize instead.');
      var otherProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_this.props, ['prefixCls', 'onPressEnter', 'autoSize', 'autosize', 'defaultValue', 'allowClear']);
      var cls = classnames__WEBPACK_IMPORTED_MODULE_4___default()(prefixCls, className, _defineProperty({}, "".concat(prefixCls, "-disabled"), disabled)); // Fix https://github.com/ant-design/ant-design/issues/6776
      // Make sure it could be reset when using form.getFieldDecorator

      if ('value' in otherProps) {
        otherProps.value = otherProps.value || '';
      }

      var style = _extends(_extends(_extends({}, _this.props.style), textareaStyles), resizing ? {
        overflow: 'hidden'
      } : null);

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_resize_observer__WEBPACK_IMPORTED_MODULE_2___default.a, {
        onResize: _this.resizeOnNextFrame,
        disabled: !(autoSize || autosize)
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("textarea", _extends({}, otherProps, {
        className: cls,
        style: style,
        ref: _this.saveTextArea
      })));
    };

    _this.state = {
      textareaStyles: {},
      resizing: false
    };
    return _this;
  }

  _createClass(ResizableTextArea, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.resizeTextarea();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      // Re-render with the new content then recalculate the height as required.
      if (prevProps.value !== this.props.value) {
        this.resizeTextarea();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _util_raf__WEBPACK_IMPORTED_MODULE_6__["default"].cancel(this.nextFrameActionId);
      _util_raf__WEBPACK_IMPORTED_MODULE_6__["default"].cancel(this.resizeFrameId);
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderTextArea();
    }
  }]);

  return ResizableTextArea;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__["polyfill"])(ResizableTextArea);
/* harmony default export */ __webpack_exports__["default"] = (ResizableTextArea);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9SZXNpemFibGVUZXh0QXJlYS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvaW5wdXQvUmVzaXphYmxlVGV4dEFyZWEuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IFJlc2l6ZU9ic2VydmVyIGZyb20gJ3JjLXJlc2l6ZS1vYnNlcnZlcic7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IGNhbGN1bGF0ZU5vZGVIZWlnaHQgZnJvbSAnLi9jYWxjdWxhdGVOb2RlSGVpZ2h0JztcbmltcG9ydCByYWYgZnJvbSAnLi4vX3V0aWwvcmFmJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuY2xhc3MgUmVzaXphYmxlVGV4dEFyZWEgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgdGhpcy5zYXZlVGV4dEFyZWEgPSAodGV4dEFyZWEpID0+IHtcbiAgICAgICAgICAgIHRoaXMudGV4dEFyZWEgPSB0ZXh0QXJlYTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZXNpemVPbk5leHRGcmFtZSA9ICgpID0+IHtcbiAgICAgICAgICAgIHJhZi5jYW5jZWwodGhpcy5uZXh0RnJhbWVBY3Rpb25JZCk7XG4gICAgICAgICAgICB0aGlzLm5leHRGcmFtZUFjdGlvbklkID0gcmFmKHRoaXMucmVzaXplVGV4dGFyZWEpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlc2l6ZVRleHRhcmVhID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgYXV0b1NpemUgPSB0aGlzLnByb3BzLmF1dG9TaXplIHx8IHRoaXMucHJvcHMuYXV0b3NpemU7XG4gICAgICAgICAgICBpZiAoIWF1dG9TaXplIHx8ICF0aGlzLnRleHRBcmVhKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgeyBtaW5Sb3dzLCBtYXhSb3dzIH0gPSBhdXRvU2l6ZTtcbiAgICAgICAgICAgIGNvbnN0IHRleHRhcmVhU3R5bGVzID0gY2FsY3VsYXRlTm9kZUhlaWdodCh0aGlzLnRleHRBcmVhLCBmYWxzZSwgbWluUm93cywgbWF4Um93cyk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdGV4dGFyZWFTdHlsZXMsIHJlc2l6aW5nOiB0cnVlIH0sICgpID0+IHtcbiAgICAgICAgICAgICAgICByYWYuY2FuY2VsKHRoaXMucmVzaXplRnJhbWVJZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNpemVGcmFtZUlkID0gcmFmKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc2l6aW5nOiBmYWxzZSB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlclRleHRBcmVhID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHMsIGF1dG9TaXplLCBhdXRvc2l6ZSwgY2xhc3NOYW1lLCBkaXNhYmxlZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHsgdGV4dGFyZWFTdHlsZXMsIHJlc2l6aW5nIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgd2FybmluZyhhdXRvc2l6ZSA9PT0gdW5kZWZpbmVkLCAnSW5wdXQuVGV4dEFyZWEnLCAnYXV0b3NpemUgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBhdXRvU2l6ZSBpbnN0ZWFkLicpO1xuICAgICAgICAgICAgY29uc3Qgb3RoZXJQcm9wcyA9IG9taXQodGhpcy5wcm9wcywgW1xuICAgICAgICAgICAgICAgICdwcmVmaXhDbHMnLFxuICAgICAgICAgICAgICAgICdvblByZXNzRW50ZXInLFxuICAgICAgICAgICAgICAgICdhdXRvU2l6ZScsXG4gICAgICAgICAgICAgICAgJ2F1dG9zaXplJyxcbiAgICAgICAgICAgICAgICAnZGVmYXVsdFZhbHVlJyxcbiAgICAgICAgICAgICAgICAnYWxsb3dDbGVhcicsXG4gICAgICAgICAgICBdKTtcbiAgICAgICAgICAgIGNvbnN0IGNscyA9IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1kaXNhYmxlZGBdOiBkaXNhYmxlZCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gRml4IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzY3NzZcbiAgICAgICAgICAgIC8vIE1ha2Ugc3VyZSBpdCBjb3VsZCBiZSByZXNldCB3aGVuIHVzaW5nIGZvcm0uZ2V0RmllbGREZWNvcmF0b3JcbiAgICAgICAgICAgIGlmICgndmFsdWUnIGluIG90aGVyUHJvcHMpIHtcbiAgICAgICAgICAgICAgICBvdGhlclByb3BzLnZhbHVlID0gb3RoZXJQcm9wcy52YWx1ZSB8fCAnJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHN0eWxlID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHRoaXMucHJvcHMuc3R5bGUpLCB0ZXh0YXJlYVN0eWxlcyksIChyZXNpemluZyA/IHsgb3ZlcmZsb3c6ICdoaWRkZW4nIH0gOiBudWxsKSk7XG4gICAgICAgICAgICByZXR1cm4gKDxSZXNpemVPYnNlcnZlciBvblJlc2l6ZT17dGhpcy5yZXNpemVPbk5leHRGcmFtZX0gZGlzYWJsZWQ9eyEoYXV0b1NpemUgfHwgYXV0b3NpemUpfT5cbiAgICAgICAgPHRleHRhcmVhIHsuLi5vdGhlclByb3BzfSBjbGFzc05hbWU9e2Nsc30gc3R5bGU9e3N0eWxlfSByZWY9e3RoaXMuc2F2ZVRleHRBcmVhfS8+XG4gICAgICA8L1Jlc2l6ZU9ic2VydmVyPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICB0ZXh0YXJlYVN0eWxlczoge30sXG4gICAgICAgICAgICByZXNpemluZzogZmFsc2UsXG4gICAgICAgIH07XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICB0aGlzLnJlc2l6ZVRleHRhcmVhKCk7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgICAgLy8gUmUtcmVuZGVyIHdpdGggdGhlIG5ldyBjb250ZW50IHRoZW4gcmVjYWxjdWxhdGUgdGhlIGhlaWdodCBhcyByZXF1aXJlZC5cbiAgICAgICAgaWYgKHByZXZQcm9wcy52YWx1ZSAhPT0gdGhpcy5wcm9wcy52YWx1ZSkge1xuICAgICAgICAgICAgdGhpcy5yZXNpemVUZXh0YXJlYSgpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICByYWYuY2FuY2VsKHRoaXMubmV4dEZyYW1lQWN0aW9uSWQpO1xuICAgICAgICByYWYuY2FuY2VsKHRoaXMucmVzaXplRnJhbWVJZCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVuZGVyVGV4dEFyZWEoKTtcbiAgICB9XG59XG5wb2x5ZmlsbChSZXNpemFibGVUZXh0QXJlYSk7XG5leHBvcnQgZGVmYXVsdCBSZXNpemFibGVUZXh0QXJlYTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUZBO0FBUEE7QUFDQTtBQWFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQVFBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBdEJBO0FBQ0E7QUF3QkE7QUFDQTtBQUNBO0FBRkE7QUFoREE7QUFvREE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFyRUE7QUFDQTtBQXNFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/ResizableTextArea.js
