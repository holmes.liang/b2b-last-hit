

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var dom_scroll_into_view_1 = __importDefault(__webpack_require__(/*! dom-scroll-into-view */ "./node_modules/dom-scroll-into-view/lib/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var raf_1 = __importDefault(__webpack_require__(/*! raf */ "./node_modules/raf/index.js"));

var rc_menu_1 = __importDefault(__webpack_require__(/*! rc-menu */ "./node_modules/rc-menu/es/index.js"));

var toArray_1 = __importDefault(__webpack_require__(/*! rc-util/lib/Children/toArray */ "./node_modules/rc-util/lib/Children/toArray.js"));

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_dom_1 = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var util_1 = __webpack_require__(/*! ./util */ "./node_modules/rc-select/es/util.js");

var DropdownMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DropdownMenu, _React$Component);

  function DropdownMenu(props) {
    var _this;

    _classCallCheck(this, DropdownMenu);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DropdownMenu).call(this, props));
    _this.rafInstance = null;
    _this.lastVisible = false;

    _this.scrollActiveItemToView = function () {
      // scroll into view
      var itemComponent = react_dom_1.findDOMNode(_this.firstActiveItem);
      var _this$props = _this.props,
          visible = _this$props.visible,
          firstActiveValue = _this$props.firstActiveValue;
      var value = _this.props.value;

      if (!itemComponent || !visible) {
        return;
      }

      var scrollIntoViewOpts = {
        onlyScrollIfNeeded: true
      };

      if ((!value || value.length === 0) && firstActiveValue) {
        scrollIntoViewOpts.alignWithTop = true;
      } // Delay to scroll since current frame item position is not ready when pre view is by filter
      // https://github.com/ant-design/ant-design/issues/11268#issuecomment-406634462


      _this.rafInstance = raf_1["default"](function () {
        dom_scroll_into_view_1["default"](itemComponent, react_dom_1.findDOMNode(_this.menuRef), scrollIntoViewOpts);
      });
    };

    _this.renderMenu = function () {
      var _this$props2 = _this.props,
          menuItems = _this$props2.menuItems,
          menuItemSelectedIcon = _this$props2.menuItemSelectedIcon,
          defaultActiveFirstOption = _this$props2.defaultActiveFirstOption,
          prefixCls = _this$props2.prefixCls,
          multiple = _this$props2.multiple,
          onMenuSelect = _this$props2.onMenuSelect,
          inputValue = _this$props2.inputValue,
          backfillValue = _this$props2.backfillValue,
          onMenuDeselect = _this$props2.onMenuDeselect,
          visible = _this$props2.visible;
      var firstActiveValue = _this.props.firstActiveValue;

      if (menuItems && menuItems.length) {
        var menuProps = {};

        if (multiple) {
          menuProps.onDeselect = onMenuDeselect;
          menuProps.onSelect = onMenuSelect;
        } else {
          menuProps.onClick = onMenuSelect;
        }

        var value = _this.props.value;
        var selectedKeys = util_1.getSelectKeys(menuItems, value);
        var activeKeyProps = {};
        var defaultActiveFirst = defaultActiveFirstOption;
        var clonedMenuItems = menuItems;

        if (selectedKeys.length || firstActiveValue) {
          if (visible && !_this.lastVisible) {
            activeKeyProps.activeKey = selectedKeys[0] || firstActiveValue;
          } else if (!visible) {
            // Do not trigger auto active since we already have selectedKeys
            if (selectedKeys[0]) {
              defaultActiveFirst = false;
            }

            activeKeyProps.activeKey = undefined;
          }

          var foundFirst = false; // set firstActiveItem via cloning menus
          // for scroll into view

          var clone = function clone(item) {
            var key = item.key;

            if (!foundFirst && selectedKeys.indexOf(key) !== -1 || !foundFirst && !selectedKeys.length && firstActiveValue.indexOf(item.key) !== -1) {
              foundFirst = true;
              return React.cloneElement(item, {
                ref: function ref(_ref) {
                  _this.firstActiveItem = _ref;
                }
              });
            }

            return item;
          };

          clonedMenuItems = menuItems.map(function (item) {
            if (item.type.isMenuItemGroup) {
              var children = toArray_1["default"](item.props.children).map(clone);
              return React.cloneElement(item, {}, children);
            }

            return clone(item);
          });
        } else {
          // Clear firstActiveItem when dropdown menu items was empty
          // Avoid `Unable to find node on an unmounted component`
          // https://github.com/ant-design/ant-design/issues/10774
          _this.firstActiveItem = null;
        } // clear activeKey when inputValue change


        var lastValue = value && value[value.length - 1];

        if (inputValue !== _this.lastInputValue && (!lastValue || lastValue !== backfillValue)) {
          activeKeyProps.activeKey = '';
        }

        return React.createElement(rc_menu_1["default"], _extends({
          ref: _this.saveMenuRef,
          style: _this.props.dropdownMenuStyle,
          defaultActiveFirst: defaultActiveFirst,
          role: "listbox",
          itemIcon: multiple ? menuItemSelectedIcon : null
        }, activeKeyProps, {
          multiple: multiple
        }, menuProps, {
          selectedKeys: selectedKeys,
          prefixCls: "".concat(prefixCls, "-menu")
        }), clonedMenuItems);
      }

      return null;
    };

    _this.lastInputValue = props.inputValue;
    _this.saveMenuRef = util_1.saveRef(_assertThisInitialized(_this), 'menuRef');
    return _this;
  }

  _createClass(DropdownMenu, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.scrollActiveItemToView();
      this.lastVisible = this.props.visible;
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      if (!nextProps.visible) {
        this.lastVisible = false;
      } // freeze when hide


      return this.props.visible && !nextProps.visible || nextProps.visible || nextProps.inputValue !== this.props.inputValue;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var props = this.props;

      if (!prevProps.visible && props.visible) {
        this.scrollActiveItemToView();
      }

      this.lastVisible = props.visible;
      this.lastInputValue = props.inputValue;
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.rafInstance) {
        raf_1["default"].cancel(this.rafInstance);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var renderMenu = this.renderMenu();
      return renderMenu ? React.createElement("div", {
        style: {
          overflow: 'auto',
          transform: 'translateZ(0)'
        },
        id: this.props.ariaId,
        onFocus: this.props.onPopupFocus,
        onMouseDown: util_1.preventDefaultEvent,
        onScroll: this.props.onPopupScroll
      }, renderMenu) : null;
    }
  }]);

  return DropdownMenu;
}(React.Component);

exports["default"] = DropdownMenu;
DropdownMenu.displayName = 'DropdownMenu';
DropdownMenu.propTypes = {
  ariaId: PropTypes.string,
  defaultActiveFirstOption: PropTypes.bool,
  value: PropTypes.any,
  dropdownMenuStyle: PropTypes.object,
  multiple: PropTypes.bool,
  onPopupFocus: PropTypes.func,
  onPopupScroll: PropTypes.func,
  onMenuDeSelect: PropTypes.func,
  onMenuSelect: PropTypes.func,
  prefixCls: PropTypes.string,
  menuItems: PropTypes.any,
  inputValue: PropTypes.string,
  visible: PropTypes.bool,
  firstActiveValue: PropTypes.string,
  menuItemSelectedIcon: PropTypes.oneOfType([PropTypes.func, PropTypes.node])
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2VsZWN0L2VzL0Ryb3Bkb3duTWVudS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNlbGVjdC9lcy9Ecm9wZG93bk1lbnUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbmZ1bmN0aW9uIF9leHRlbmRzKCkgeyBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07IHJldHVybiBfZXh0ZW5kcy5hcHBseSh0aGlzLCBhcmd1bWVudHMpOyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikpIHsgcmV0dXJuIGNhbGw7IH0gcmV0dXJuIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZik7IH1cblxuZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgX2dldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LmdldFByb3RvdHlwZU9mIDogZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgcmV0dXJuIG8uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihvKTsgfTsgcmV0dXJuIF9nZXRQcm90b3R5cGVPZihvKTsgfVxuXG5mdW5jdGlvbiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpIHsgaWYgKHNlbGYgPT09IHZvaWQgMCkgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uXCIpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIF9zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcyk7IH1cblxuZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgX3NldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8IGZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IG8uX19wcm90b19fID0gcDsgcmV0dXJuIG87IH07IHJldHVybiBfc2V0UHJvdG90eXBlT2YobywgcCk7IH1cblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIGRvbV9zY3JvbGxfaW50b192aWV3XzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcImRvbS1zY3JvbGwtaW50by12aWV3XCIpKTtcblxudmFyIFByb3BUeXBlcyA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicHJvcC10eXBlc1wiKSk7XG5cbnZhciByYWZfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwicmFmXCIpKTtcblxudmFyIHJjX21lbnVfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwicmMtbWVudVwiKSk7XG5cbnZhciB0b0FycmF5XzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcInJjLXV0aWwvbGliL0NoaWxkcmVuL3RvQXJyYXlcIikpO1xuXG52YXIgUmVhY3QgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInJlYWN0XCIpKTtcblxudmFyIHJlYWN0X2RvbV8xID0gcmVxdWlyZShcInJlYWN0LWRvbVwiKTtcblxudmFyIHV0aWxfMSA9IHJlcXVpcmUoXCIuL3V0aWxcIik7XG5cbnZhciBEcm9wZG93bk1lbnUgPVxuLyojX19QVVJFX18qL1xuZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKERyb3Bkb3duTWVudSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gRHJvcGRvd25NZW51KHByb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIERyb3Bkb3duTWVudSk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihEcm9wZG93bk1lbnUpLmNhbGwodGhpcywgcHJvcHMpKTtcbiAgICBfdGhpcy5yYWZJbnN0YW5jZSA9IG51bGw7XG4gICAgX3RoaXMubGFzdFZpc2libGUgPSBmYWxzZTtcblxuICAgIF90aGlzLnNjcm9sbEFjdGl2ZUl0ZW1Ub1ZpZXcgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBzY3JvbGwgaW50byB2aWV3XG4gICAgICB2YXIgaXRlbUNvbXBvbmVudCA9IHJlYWN0X2RvbV8xLmZpbmRET01Ob2RlKF90aGlzLmZpcnN0QWN0aXZlSXRlbSk7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB2aXNpYmxlID0gX3RoaXMkcHJvcHMudmlzaWJsZSxcbiAgICAgICAgICBmaXJzdEFjdGl2ZVZhbHVlID0gX3RoaXMkcHJvcHMuZmlyc3RBY3RpdmVWYWx1ZTtcbiAgICAgIHZhciB2YWx1ZSA9IF90aGlzLnByb3BzLnZhbHVlO1xuXG4gICAgICBpZiAoIWl0ZW1Db21wb25lbnQgfHwgIXZpc2libGUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgc2Nyb2xsSW50b1ZpZXdPcHRzID0ge1xuICAgICAgICBvbmx5U2Nyb2xsSWZOZWVkZWQ6IHRydWVcbiAgICAgIH07XG5cbiAgICAgIGlmICgoIXZhbHVlIHx8IHZhbHVlLmxlbmd0aCA9PT0gMCkgJiYgZmlyc3RBY3RpdmVWYWx1ZSkge1xuICAgICAgICBzY3JvbGxJbnRvVmlld09wdHMuYWxpZ25XaXRoVG9wID0gdHJ1ZTtcbiAgICAgIH0gLy8gRGVsYXkgdG8gc2Nyb2xsIHNpbmNlIGN1cnJlbnQgZnJhbWUgaXRlbSBwb3NpdGlvbiBpcyBub3QgcmVhZHkgd2hlbiBwcmUgdmlldyBpcyBieSBmaWx0ZXJcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzExMjY4I2lzc3VlY29tbWVudC00MDY2MzQ0NjJcblxuXG4gICAgICBfdGhpcy5yYWZJbnN0YW5jZSA9IHJhZl8xW1wiZGVmYXVsdFwiXShmdW5jdGlvbiAoKSB7XG4gICAgICAgIGRvbV9zY3JvbGxfaW50b192aWV3XzFbXCJkZWZhdWx0XCJdKGl0ZW1Db21wb25lbnQsIHJlYWN0X2RvbV8xLmZpbmRET01Ob2RlKF90aGlzLm1lbnVSZWYpLCBzY3JvbGxJbnRvVmlld09wdHMpO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLnJlbmRlck1lbnUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgbWVudUl0ZW1zID0gX3RoaXMkcHJvcHMyLm1lbnVJdGVtcyxcbiAgICAgICAgICBtZW51SXRlbVNlbGVjdGVkSWNvbiA9IF90aGlzJHByb3BzMi5tZW51SXRlbVNlbGVjdGVkSWNvbixcbiAgICAgICAgICBkZWZhdWx0QWN0aXZlRmlyc3RPcHRpb24gPSBfdGhpcyRwcm9wczIuZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzMi5wcmVmaXhDbHMsXG4gICAgICAgICAgbXVsdGlwbGUgPSBfdGhpcyRwcm9wczIubXVsdGlwbGUsXG4gICAgICAgICAgb25NZW51U2VsZWN0ID0gX3RoaXMkcHJvcHMyLm9uTWVudVNlbGVjdCxcbiAgICAgICAgICBpbnB1dFZhbHVlID0gX3RoaXMkcHJvcHMyLmlucHV0VmFsdWUsXG4gICAgICAgICAgYmFja2ZpbGxWYWx1ZSA9IF90aGlzJHByb3BzMi5iYWNrZmlsbFZhbHVlLFxuICAgICAgICAgIG9uTWVudURlc2VsZWN0ID0gX3RoaXMkcHJvcHMyLm9uTWVudURlc2VsZWN0LFxuICAgICAgICAgIHZpc2libGUgPSBfdGhpcyRwcm9wczIudmlzaWJsZTtcbiAgICAgIHZhciBmaXJzdEFjdGl2ZVZhbHVlID0gX3RoaXMucHJvcHMuZmlyc3RBY3RpdmVWYWx1ZTtcblxuICAgICAgaWYgKG1lbnVJdGVtcyAmJiBtZW51SXRlbXMubGVuZ3RoKSB7XG4gICAgICAgIHZhciBtZW51UHJvcHMgPSB7fTtcblxuICAgICAgICBpZiAobXVsdGlwbGUpIHtcbiAgICAgICAgICBtZW51UHJvcHMub25EZXNlbGVjdCA9IG9uTWVudURlc2VsZWN0O1xuICAgICAgICAgIG1lbnVQcm9wcy5vblNlbGVjdCA9IG9uTWVudVNlbGVjdDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBtZW51UHJvcHMub25DbGljayA9IG9uTWVudVNlbGVjdDtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB2YWx1ZSA9IF90aGlzLnByb3BzLnZhbHVlO1xuICAgICAgICB2YXIgc2VsZWN0ZWRLZXlzID0gdXRpbF8xLmdldFNlbGVjdEtleXMobWVudUl0ZW1zLCB2YWx1ZSk7XG4gICAgICAgIHZhciBhY3RpdmVLZXlQcm9wcyA9IHt9O1xuICAgICAgICB2YXIgZGVmYXVsdEFjdGl2ZUZpcnN0ID0gZGVmYXVsdEFjdGl2ZUZpcnN0T3B0aW9uO1xuICAgICAgICB2YXIgY2xvbmVkTWVudUl0ZW1zID0gbWVudUl0ZW1zO1xuXG4gICAgICAgIGlmIChzZWxlY3RlZEtleXMubGVuZ3RoIHx8IGZpcnN0QWN0aXZlVmFsdWUpIHtcbiAgICAgICAgICBpZiAodmlzaWJsZSAmJiAhX3RoaXMubGFzdFZpc2libGUpIHtcbiAgICAgICAgICAgIGFjdGl2ZUtleVByb3BzLmFjdGl2ZUtleSA9IHNlbGVjdGVkS2V5c1swXSB8fCBmaXJzdEFjdGl2ZVZhbHVlO1xuICAgICAgICAgIH0gZWxzZSBpZiAoIXZpc2libGUpIHtcbiAgICAgICAgICAgIC8vIERvIG5vdCB0cmlnZ2VyIGF1dG8gYWN0aXZlIHNpbmNlIHdlIGFscmVhZHkgaGF2ZSBzZWxlY3RlZEtleXNcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEtleXNbMF0pIHtcbiAgICAgICAgICAgICAgZGVmYXVsdEFjdGl2ZUZpcnN0ID0gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGFjdGl2ZUtleVByb3BzLmFjdGl2ZUtleSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgZm91bmRGaXJzdCA9IGZhbHNlOyAvLyBzZXQgZmlyc3RBY3RpdmVJdGVtIHZpYSBjbG9uaW5nIG1lbnVzXG4gICAgICAgICAgLy8gZm9yIHNjcm9sbCBpbnRvIHZpZXdcblxuICAgICAgICAgIHZhciBjbG9uZSA9IGZ1bmN0aW9uIGNsb25lKGl0ZW0pIHtcbiAgICAgICAgICAgIHZhciBrZXkgPSBpdGVtLmtleTtcblxuICAgICAgICAgICAgaWYgKCFmb3VuZEZpcnN0ICYmIHNlbGVjdGVkS2V5cy5pbmRleE9mKGtleSkgIT09IC0xIHx8ICFmb3VuZEZpcnN0ICYmICFzZWxlY3RlZEtleXMubGVuZ3RoICYmIGZpcnN0QWN0aXZlVmFsdWUuaW5kZXhPZihpdGVtLmtleSkgIT09IC0xKSB7XG4gICAgICAgICAgICAgIGZvdW5kRmlyc3QgPSB0cnVlO1xuICAgICAgICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGl0ZW0sIHtcbiAgICAgICAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihfcmVmKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5maXJzdEFjdGl2ZUl0ZW0gPSBfcmVmO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBpdGVtO1xuICAgICAgICAgIH07XG5cbiAgICAgICAgICBjbG9uZWRNZW51SXRlbXMgPSBtZW51SXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICBpZiAoaXRlbS50eXBlLmlzTWVudUl0ZW1Hcm91cCkge1xuICAgICAgICAgICAgICB2YXIgY2hpbGRyZW4gPSB0b0FycmF5XzFbXCJkZWZhdWx0XCJdKGl0ZW0ucHJvcHMuY2hpbGRyZW4pLm1hcChjbG9uZSk7XG4gICAgICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoaXRlbSwge30sIGNoaWxkcmVuKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNsb25lKGl0ZW0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIENsZWFyIGZpcnN0QWN0aXZlSXRlbSB3aGVuIGRyb3Bkb3duIG1lbnUgaXRlbXMgd2FzIGVtcHR5XG4gICAgICAgICAgLy8gQXZvaWQgYFVuYWJsZSB0byBmaW5kIG5vZGUgb24gYW4gdW5tb3VudGVkIGNvbXBvbmVudGBcbiAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMDc3NFxuICAgICAgICAgIF90aGlzLmZpcnN0QWN0aXZlSXRlbSA9IG51bGw7XG4gICAgICAgIH0gLy8gY2xlYXIgYWN0aXZlS2V5IHdoZW4gaW5wdXRWYWx1ZSBjaGFuZ2VcblxuXG4gICAgICAgIHZhciBsYXN0VmFsdWUgPSB2YWx1ZSAmJiB2YWx1ZVt2YWx1ZS5sZW5ndGggLSAxXTtcblxuICAgICAgICBpZiAoaW5wdXRWYWx1ZSAhPT0gX3RoaXMubGFzdElucHV0VmFsdWUgJiYgKCFsYXN0VmFsdWUgfHwgbGFzdFZhbHVlICE9PSBiYWNrZmlsbFZhbHVlKSkge1xuICAgICAgICAgIGFjdGl2ZUtleVByb3BzLmFjdGl2ZUtleSA9ICcnO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQocmNfbWVudV8xW1wiZGVmYXVsdFwiXSwgX2V4dGVuZHMoe1xuICAgICAgICAgIHJlZjogX3RoaXMuc2F2ZU1lbnVSZWYsXG4gICAgICAgICAgc3R5bGU6IF90aGlzLnByb3BzLmRyb3Bkb3duTWVudVN0eWxlLFxuICAgICAgICAgIGRlZmF1bHRBY3RpdmVGaXJzdDogZGVmYXVsdEFjdGl2ZUZpcnN0LFxuICAgICAgICAgIHJvbGU6IFwibGlzdGJveFwiLFxuICAgICAgICAgIGl0ZW1JY29uOiBtdWx0aXBsZSA/IG1lbnVJdGVtU2VsZWN0ZWRJY29uIDogbnVsbFxuICAgICAgICB9LCBhY3RpdmVLZXlQcm9wcywge1xuICAgICAgICAgIG11bHRpcGxlOiBtdWx0aXBsZVxuICAgICAgICB9LCBtZW51UHJvcHMsIHtcbiAgICAgICAgICBzZWxlY3RlZEtleXM6IHNlbGVjdGVkS2V5cyxcbiAgICAgICAgICBwcmVmaXhDbHM6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItbWVudVwiKVxuICAgICAgICB9KSwgY2xvbmVkTWVudUl0ZW1zKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIF90aGlzLmxhc3RJbnB1dFZhbHVlID0gcHJvcHMuaW5wdXRWYWx1ZTtcbiAgICBfdGhpcy5zYXZlTWVudVJlZiA9IHV0aWxfMS5zYXZlUmVmKF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCAnbWVudVJlZicpO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhEcm9wZG93bk1lbnUsIFt7XG4gICAga2V5OiBcImNvbXBvbmVudERpZE1vdW50XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5zY3JvbGxBY3RpdmVJdGVtVG9WaWV3KCk7XG4gICAgICB0aGlzLmxhc3RWaXNpYmxlID0gdGhpcy5wcm9wcy52aXNpYmxlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJzaG91bGRDb21wb25lbnRVcGRhdGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgaWYgKCFuZXh0UHJvcHMudmlzaWJsZSkge1xuICAgICAgICB0aGlzLmxhc3RWaXNpYmxlID0gZmFsc2U7XG4gICAgICB9IC8vIGZyZWV6ZSB3aGVuIGhpZGVcblxuXG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy52aXNpYmxlICYmICFuZXh0UHJvcHMudmlzaWJsZSB8fCBuZXh0UHJvcHMudmlzaWJsZSB8fCBuZXh0UHJvcHMuaW5wdXRWYWx1ZSAhPT0gdGhpcy5wcm9wcy5pbnB1dFZhbHVlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnREaWRVcGRhdGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcblxuICAgICAgaWYgKCFwcmV2UHJvcHMudmlzaWJsZSAmJiBwcm9wcy52aXNpYmxlKSB7XG4gICAgICAgIHRoaXMuc2Nyb2xsQWN0aXZlSXRlbVRvVmlldygpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmxhc3RWaXNpYmxlID0gcHJvcHMudmlzaWJsZTtcbiAgICAgIHRoaXMubGFzdElucHV0VmFsdWUgPSBwcm9wcy5pbnB1dFZhbHVlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnRXaWxsVW5tb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLnJhZkluc3RhbmNlKSB7XG4gICAgICAgIHJhZl8xW1wiZGVmYXVsdFwiXS5jYW5jZWwodGhpcy5yYWZJbnN0YW5jZSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlclwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgcmVuZGVyTWVudSA9IHRoaXMucmVuZGVyTWVudSgpO1xuICAgICAgcmV0dXJuIHJlbmRlck1lbnUgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICBvdmVyZmxvdzogJ2F1dG8nLFxuICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVooMCknXG4gICAgICAgIH0sXG4gICAgICAgIGlkOiB0aGlzLnByb3BzLmFyaWFJZCxcbiAgICAgICAgb25Gb2N1czogdGhpcy5wcm9wcy5vblBvcHVwRm9jdXMsXG4gICAgICAgIG9uTW91c2VEb3duOiB1dGlsXzEucHJldmVudERlZmF1bHRFdmVudCxcbiAgICAgICAgb25TY3JvbGw6IHRoaXMucHJvcHMub25Qb3B1cFNjcm9sbFxuICAgICAgfSwgcmVuZGVyTWVudSkgOiBudWxsO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBEcm9wZG93bk1lbnU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gRHJvcGRvd25NZW51O1xuRHJvcGRvd25NZW51LmRpc3BsYXlOYW1lID0gJ0Ryb3Bkb3duTWVudSc7XG5Ecm9wZG93bk1lbnUucHJvcFR5cGVzID0ge1xuICBhcmlhSWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRlZmF1bHRBY3RpdmVGaXJzdE9wdGlvbjogUHJvcFR5cGVzLmJvb2wsXG4gIHZhbHVlOiBQcm9wVHlwZXMuYW55LFxuICBkcm9wZG93bk1lbnVTdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgbXVsdGlwbGU6IFByb3BUeXBlcy5ib29sLFxuICBvblBvcHVwRm9jdXM6IFByb3BUeXBlcy5mdW5jLFxuICBvblBvcHVwU2Nyb2xsOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25NZW51RGVTZWxlY3Q6IFByb3BUeXBlcy5mdW5jLFxuICBvbk1lbnVTZWxlY3Q6IFByb3BUeXBlcy5mdW5jLFxuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG1lbnVJdGVtczogUHJvcFR5cGVzLmFueSxcbiAgaW5wdXRWYWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIGZpcnN0QWN0aXZlVmFsdWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG1lbnVJdGVtU2VsZWN0ZWRJY29uOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuZnVuYywgUHJvcFR5cGVzLm5vZGVdKVxufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBREE7QUFHQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFkQTtBQUNBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-select/es/DropdownMenu.js
