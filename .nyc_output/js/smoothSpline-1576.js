var _vector = __webpack_require__(/*! ../../core/vector */ "./node_modules/zrender/lib/core/vector.js");

var v2Distance = _vector.distance;
/**
 * Catmull-Rom spline 插值折线
 * @module zrender/shape/util/smoothSpline
 * @author pissang (https://www.github.com/pissang)
 *         Kener (@Kener-林峰, kener.linfeng@gmail.com)
 *         errorrik (errorrik@gmail.com)
 */

/**
 * @inner
 */

function interpolate(p0, p1, p2, p3, t, t2, t3) {
  var v0 = (p2 - p0) * 0.5;
  var v1 = (p3 - p1) * 0.5;
  return (2 * (p1 - p2) + v0 + v1) * t3 + (-3 * (p1 - p2) - 2 * v0 - v1) * t2 + v0 * t + p1;
}
/**
 * @alias module:zrender/shape/util/smoothSpline
 * @param {Array} points 线段顶点数组
 * @param {boolean} isLoop
 * @return {Array}
 */


function _default(points, isLoop) {
  var len = points.length;
  var ret = [];
  var distance = 0;

  for (var i = 1; i < len; i++) {
    distance += v2Distance(points[i - 1], points[i]);
  }

  var segs = distance / 2;
  segs = segs < len ? len : segs;

  for (var i = 0; i < segs; i++) {
    var pos = i / (segs - 1) * (isLoop ? len : len - 1);
    var idx = Math.floor(pos);
    var w = pos - idx;
    var p0;
    var p1 = points[idx % len];
    var p2;
    var p3;

    if (!isLoop) {
      p0 = points[idx === 0 ? idx : idx - 1];
      p2 = points[idx > len - 2 ? len - 1 : idx + 1];
      p3 = points[idx > len - 3 ? len - 1 : idx + 2];
    } else {
      p0 = points[(idx - 1 + len) % len];
      p2 = points[(idx + 1) % len];
      p3 = points[(idx + 2) % len];
    }

    var w2 = w * w;
    var w3 = w * w2;
    ret.push([interpolate(p0[0], p1[0], p2[0], p3[0], w, w2, w3), interpolate(p0[1], p1[1], p2[1], p3[1], w, w2, w3)]);
  }

  return ret;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvc21vb3RoU3BsaW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvc21vb3RoU3BsaW5lLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBfdmVjdG9yID0gcmVxdWlyZShcIi4uLy4uL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgdjJEaXN0YW5jZSA9IF92ZWN0b3IuZGlzdGFuY2U7XG5cbi8qKlxuICogQ2F0bXVsbC1Sb20gc3BsaW5lIOaPkuWAvOaKmOe6v1xuICogQG1vZHVsZSB6cmVuZGVyL3NoYXBlL3V0aWwvc21vb3RoU3BsaW5lXG4gKiBAYXV0aG9yIHBpc3NhbmcgKGh0dHBzOi8vd3d3LmdpdGh1Yi5jb20vcGlzc2FuZylcbiAqICAgICAgICAgS2VuZXIgKEBLZW5lci3mnpfls7AsIGtlbmVyLmxpbmZlbmdAZ21haWwuY29tKVxuICogICAgICAgICBlcnJvcnJpayAoZXJyb3JyaWtAZ21haWwuY29tKVxuICovXG5cbi8qKlxuICogQGlubmVyXG4gKi9cbmZ1bmN0aW9uIGludGVycG9sYXRlKHAwLCBwMSwgcDIsIHAzLCB0LCB0MiwgdDMpIHtcbiAgdmFyIHYwID0gKHAyIC0gcDApICogMC41O1xuICB2YXIgdjEgPSAocDMgLSBwMSkgKiAwLjU7XG4gIHJldHVybiAoMiAqIChwMSAtIHAyKSArIHYwICsgdjEpICogdDMgKyAoLTMgKiAocDEgLSBwMikgLSAyICogdjAgLSB2MSkgKiB0MiArIHYwICogdCArIHAxO1xufVxuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOnpyZW5kZXIvc2hhcGUvdXRpbC9zbW9vdGhTcGxpbmVcbiAqIEBwYXJhbSB7QXJyYXl9IHBvaW50cyDnur/mrrXpobbngrnmlbDnu4RcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gaXNMb29wXG4gKiBAcmV0dXJuIHtBcnJheX1cbiAqL1xuXG5cbmZ1bmN0aW9uIF9kZWZhdWx0KHBvaW50cywgaXNMb29wKSB7XG4gIHZhciBsZW4gPSBwb2ludHMubGVuZ3RoO1xuICB2YXIgcmV0ID0gW107XG4gIHZhciBkaXN0YW5jZSA9IDA7XG5cbiAgZm9yICh2YXIgaSA9IDE7IGkgPCBsZW47IGkrKykge1xuICAgIGRpc3RhbmNlICs9IHYyRGlzdGFuY2UocG9pbnRzW2kgLSAxXSwgcG9pbnRzW2ldKTtcbiAgfVxuXG4gIHZhciBzZWdzID0gZGlzdGFuY2UgLyAyO1xuICBzZWdzID0gc2VncyA8IGxlbiA/IGxlbiA6IHNlZ3M7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzZWdzOyBpKyspIHtcbiAgICB2YXIgcG9zID0gaSAvIChzZWdzIC0gMSkgKiAoaXNMb29wID8gbGVuIDogbGVuIC0gMSk7XG4gICAgdmFyIGlkeCA9IE1hdGguZmxvb3IocG9zKTtcbiAgICB2YXIgdyA9IHBvcyAtIGlkeDtcbiAgICB2YXIgcDA7XG4gICAgdmFyIHAxID0gcG9pbnRzW2lkeCAlIGxlbl07XG4gICAgdmFyIHAyO1xuICAgIHZhciBwMztcblxuICAgIGlmICghaXNMb29wKSB7XG4gICAgICBwMCA9IHBvaW50c1tpZHggPT09IDAgPyBpZHggOiBpZHggLSAxXTtcbiAgICAgIHAyID0gcG9pbnRzW2lkeCA+IGxlbiAtIDIgPyBsZW4gLSAxIDogaWR4ICsgMV07XG4gICAgICBwMyA9IHBvaW50c1tpZHggPiBsZW4gLSAzID8gbGVuIC0gMSA6IGlkeCArIDJdO1xuICAgIH0gZWxzZSB7XG4gICAgICBwMCA9IHBvaW50c1soaWR4IC0gMSArIGxlbikgJSBsZW5dO1xuICAgICAgcDIgPSBwb2ludHNbKGlkeCArIDEpICUgbGVuXTtcbiAgICAgIHAzID0gcG9pbnRzWyhpZHggKyAyKSAlIGxlbl07XG4gICAgfVxuXG4gICAgdmFyIHcyID0gdyAqIHc7XG4gICAgdmFyIHczID0gdyAqIHcyO1xuICAgIHJldC5wdXNoKFtpbnRlcnBvbGF0ZShwMFswXSwgcDFbMF0sIHAyWzBdLCBwM1swXSwgdywgdzIsIHczKSwgaW50ZXJwb2xhdGUocDBbMV0sIHAxWzFdLCBwMlsxXSwgcDNbMV0sIHcsIHcyLCB3MyldKTtcbiAgfVxuXG4gIHJldHVybiByZXQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFRQTs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/smoothSpline.js
