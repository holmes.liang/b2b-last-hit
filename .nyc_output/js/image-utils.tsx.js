__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var image_conversion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! image-conversion */ "./node_modules/image-conversion/index.js");
/* harmony import */ var image_conversion__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(image_conversion__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var exif_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! exif-js */ "./node_modules/exif-js/exif.js");
/* harmony import */ var exif_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(exif_js__WEBPACK_IMPORTED_MODULE_5__);







var ImageUtils =
/*#__PURE__*/
function () {
  function ImageUtils() {
    var _this = this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, ImageUtils);

    this._exifOrientationPromise = function (file, resolve, reject) {
      exif_js__WEBPACK_IMPORTED_MODULE_5___default.a.getData(file, function () {
        // @ts-ignore
        var orientation = exif_js__WEBPACK_IMPORTED_MODULE_5___default.a.getTag(this, "Orientation"); //获取图像的某个数据

        resolve(orientation);
      });
    };

    this.exifOrientationPromise = function (file) {
      return new Promise(function (resolve, reject) {
        _this._exifOrientationPromise(file, resolve, reject);
      });
    };

    this._getImagePromise = function (file, resolve, reject) {
      var reader = new FileReader();

      reader.onload = function (e) {
        var data = e.target.result; //加载图片获取图片真实宽度和高度

        var image = new Image();

        image.onload = function () {
          resolve(image);
        };

        image.src = data;
      };

      reader.readAsDataURL(file);
    };

    this.getImagePromise = function (file) {
      return new Promise(function (resolve, reject) {
        _this._getImagePromise(file, resolve, reject);
      });
    };
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(ImageUtils, [{
    key: "compress",
    value: function () {
      var _compress = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(file) {
        var compressToWidth,
            image,
            width,
            height,
            newWidth,
            newHeight,
            isMinWidth,
            newCanvas,
            newFile,
            _args = arguments;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                compressToWidth = _args.length > 1 && _args[1] !== undefined ? _args[1] : 200;
                _context.next = 3;
                return this.getImagePromise(file);

              case 3:
                image = _context.sent;
                width = image.width;
                height = image.height;
                isMinWidth = Math.min(width, height) === width;

                if (isMinWidth && width > compressToWidth) {
                  newWidth = compressToWidth;
                  newHeight = height * (compressToWidth / width);
                } else if (!isMinWidth && height > compressToWidth) {
                  newHeight = compressToWidth;
                  newWidth = width * (compressToWidth / height);
                } else {
                  newWidth = width;
                  newHeight = height;
                }

                _context.prev = 8;
                _context.next = 11;
                return image_conversion__WEBPACK_IMPORTED_MODULE_4___default.a.imagetoCanvas(image, {
                  width: newWidth,
                  height: newHeight,
                  orientation: 1
                });

              case 11:
                newCanvas = _context.sent;
                _context.next = 14;
                return image_conversion__WEBPACK_IMPORTED_MODULE_4___default.a.canvastoFile(newCanvas);

              case 14:
                newFile = _context.sent;
                return _context.abrupt("return", newFile);

              case 18:
                _context.prev = 18;
                _context.t0 = _context["catch"](8);
                return _context.abrupt("return", file);

              case 21:
                return _context.abrupt("return", file);

              case 22:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[8, 18]]);
      }));

      function compress(_x) {
        return _compress.apply(this, arguments);
      }

      return compress;
    }()
  }]);

  return ImageUtils;
}();

/* harmony default export */ __webpack_exports__["default"] = (new ImageUtils());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3V0aWxzL2ltYWdlLXV0aWxzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2NvbW1vbi91dGlscy9pbWFnZS11dGlscy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGltYWdlQ29udmVyc2lvbiBmcm9tIFwiaW1hZ2UtY29udmVyc2lvblwiO1xuaW1wb3J0IEVYSUYgZnJvbSBcImV4aWYtanNcIjtcblxuY2xhc3MgSW1hZ2VVdGlscyB7XG4gIGFzeW5jIGNvbXByZXNzKGZpbGU6IGFueSwgY29tcHJlc3NUb1dpZHRoID0gMjAwKSB7XG4gICAgY29uc3QgaW1hZ2U6IGFueSA9IGF3YWl0IHRoaXMuZ2V0SW1hZ2VQcm9taXNlKGZpbGUpO1xuICAgIGNvbnN0IHdpZHRoID0gaW1hZ2Uud2lkdGg7XG4gICAgY29uc3QgaGVpZ2h0ID0gaW1hZ2UuaGVpZ2h0O1xuICAgIGxldCBuZXdXaWR0aCwgbmV3SGVpZ2h0O1xuICAgIGNvbnN0IGlzTWluV2lkdGggPSBNYXRoLm1pbih3aWR0aCwgaGVpZ2h0KSA9PT0gd2lkdGg7XG4gICAgaWYgKGlzTWluV2lkdGggJiYgd2lkdGggPiBjb21wcmVzc1RvV2lkdGgpIHtcbiAgICAgIG5ld1dpZHRoID0gY29tcHJlc3NUb1dpZHRoO1xuICAgICAgbmV3SGVpZ2h0ID0gaGVpZ2h0ICogKGNvbXByZXNzVG9XaWR0aCAvIHdpZHRoKTtcbiAgICB9IGVsc2UgaWYgKCFpc01pbldpZHRoICYmIGhlaWdodCA+IGNvbXByZXNzVG9XaWR0aCkge1xuICAgICAgbmV3SGVpZ2h0ID0gY29tcHJlc3NUb1dpZHRoO1xuICAgICAgbmV3V2lkdGggPSB3aWR0aCAqIChjb21wcmVzc1RvV2lkdGggLyBoZWlnaHQpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXdXaWR0aCA9IHdpZHRoO1xuICAgICAgbmV3SGVpZ2h0ID0gaGVpZ2h0O1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgY29uc3QgbmV3Q2FudmFzID0gYXdhaXQgaW1hZ2VDb252ZXJzaW9uLmltYWdldG9DYW52YXMoaW1hZ2UsIHtcbiAgICAgICAgd2lkdGg6IG5ld1dpZHRoLFxuICAgICAgICBoZWlnaHQ6IG5ld0hlaWdodCxcbiAgICAgICAgb3JpZW50YXRpb246IDEsXG4gICAgICB9KTtcbiAgICAgIGNvbnN0IG5ld0ZpbGUgPSBhd2FpdCBpbWFnZUNvbnZlcnNpb24uY2FudmFzdG9GaWxlKG5ld0NhbnZhcyk7XG4gICAgICByZXR1cm4gbmV3RmlsZTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICByZXR1cm4gZmlsZTtcbiAgICB9XG4gICAgcmV0dXJuIGZpbGU7XG4gIH1cblxuICBfZXhpZk9yaWVudGF0aW9uUHJvbWlzZSA9IChmaWxlOiBhbnksIHJlc29sdmU6IGFueSwgcmVqZWN0OiBhbnkpID0+IHtcbiAgICBFWElGLmdldERhdGEoZmlsZSxcbiAgICAgIGZ1bmN0aW9uKCkge1xuICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgIGNvbnN0IG9yaWVudGF0aW9uID0gRVhJRi5nZXRUYWcodGhpcywgXCJPcmllbnRhdGlvblwiKTsgLy/ojrflj5blm77lg4/nmoTmn5DkuKrmlbDmja5cbiAgICAgICAgcmVzb2x2ZShvcmllbnRhdGlvbik7XG4gICAgICB9LFxuICAgICk7XG4gIH07XG5cblxuICBleGlmT3JpZW50YXRpb25Qcm9taXNlID0gKGZpbGU6IGFueSkgPT4ge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICB0aGlzLl9leGlmT3JpZW50YXRpb25Qcm9taXNlKGZpbGUsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgfSk7XG4gIH07XG5cbiAgX2dldEltYWdlUHJvbWlzZSA9IChmaWxlOiBhbnksIHJlc29sdmU6IGFueSwgcmVqZWN0OiBhbnkpID0+IHtcbiAgICBsZXQgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcbiAgICByZWFkZXIub25sb2FkID0gZnVuY3Rpb24oZTogYW55KSB7XG4gICAgICBsZXQgZGF0YSA9IGUudGFyZ2V0LnJlc3VsdDtcbiAgICAgIC8v5Yqg6L295Zu+54mH6I635Y+W5Zu+54mH55yf5a6e5a695bqm5ZKM6auY5bqmXG4gICAgICBsZXQgaW1hZ2UgPSBuZXcgSW1hZ2UoKTtcbiAgICAgIGltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXNvbHZlKGltYWdlKTtcbiAgICAgIH07XG4gICAgICBpbWFnZS5zcmMgPSBkYXRhO1xuICAgIH07XG4gICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XG4gIH07XG5cblxuICBnZXRJbWFnZVByb21pc2UgPSAoZmlsZTogYW55KSA9PiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHRoaXMuX2dldEltYWdlUHJvbWlzZShmaWxlLCByZXNvbHZlLCByZWplY3QpO1xuICAgIH0pO1xuICB9O1xufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgSW1hZ2VVdGlscygpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQStCQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQWxFQTs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7O0FBQ0E7QUFDQTs7QUFEQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBOztBQURBOztBQUtBO0FBQ0E7O0FBREE7QUFDQTtBQUNBOzs7O0FBQ0E7QUFDQTs7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/common/utils/image-utils.tsx
