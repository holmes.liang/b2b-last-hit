__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NPrice", function() { return NPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatterPrice", function() { return formatterPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parserPrice", function() { return parserPrice; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component_index__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app-component/index */ "./src/app/component/index.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NPrice.tsx";






var NPrice =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(NPrice, _ModelWidget);

  function NPrice(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, NPrice);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(NPrice).call(this, props, context));

    _this.formatterPrice = function (value) {
      if (_this.props.formatter) {
        return _this.props.formatter(value);
      }

      if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isFloatNumber(value)) {
        return _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].toThousands(value);
      }

      return value;
    };

    _this.parserPrice = function (value) {
      if (_this.props.parser) {
        return _this.props.parser(value);
      }

      return value.replace(/\S\$\s?|(,*)/g, "");
    };

    _this.getAddonBefore = function () {
      if (_this.props.addonBefore) {
        return _this.props.addonBefore;
      }

      return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.props.model, "currencySymbol", "S$");
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(NPrice, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "render",
    value: function render() {
      var that = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          _this$props$allowZero = _this$props.allowZero,
          allowZero = _this$props$allowZero === void 0 ? true : _this$props$allowZero,
          label = _this$props.label,
          propName = _this$props.propName,
          _this$props$isAddonBe = _this$props.isAddonBefore,
          isAddonBefore = _this$props$isAddonBe === void 0 ? true : _this$props$isAddonBe,
          onChange = _this$props.onChange,
          rules = _this$props.rules,
          initRules = _this$props.initRules,
          required = _this$props.required,
          layoutCol = _this$props.layoutCol,
          placeholder = _this$props.placeholder,
          transform = _this$props.transform,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["form", "model", "allowZero", "label", "propName", "isAddonBefore", "onChange", "rules", "initRules", "required", "layoutCol", "placeholder", "transform"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component_index__WEBPACK_IMPORTED_MODULE_10__["NNumber"], Object.assign({
        form: form,
        model: model,
        label: label,
        propName: propName,
        onChange: onChange,
        rules: rules,
        required: required,
        layoutCol: layoutCol,
        transform: transform
      }, rest, {
        addonBefore: !isAddonBefore ? "" : this.getAddonBefore(),
        formatter: this.formatterPrice,
        parser: this.parserPrice,
        rules: initRules || [{
          validator: function validator(rule, value, callback) {
            if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isNumFromZero(value)) {
              if (that.props.precision === 0 && !_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isNumberFromZero(value)) {
                callback("".concat(that.props.label, " must be positive integer"));
              } else {
                callback();
              }
            } else if (!required && !value && value !== 0) {
              callback();
            } else if (allowZero) {
              if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isNumFromZeroAndAllowZero(value)) {
                callback();
              } else {
                callback("".concat(that.props.label, " must a number greater than or equal to 0"));
              }
            } else {
              callback("".concat(that.props.label, " must be positive number"));
            }
          }
        }].concat(this.props.rules || []),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }));
    }
  }]);

  return NPrice;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

NPrice.defaultProps = {
  precision: 2
};

var formatterPrice = function formatterPrice(value) {
  if (_common__WEBPACK_IMPORTED_MODULE_9__["Rules"].isFloatNumber(value)) {
    return _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].toThousands(value);
  }

  return value;
};
var parserPrice = function parserPrice(value) {
  return value.replace(/\S\$\s?|(,*)/g, "");
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9OUHJpY2UudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2NvbXBvbmVudC9OUHJpY2UudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSdWxlcywgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTk51bWJlciB9IGZyb20gXCJAYXBwLWNvbXBvbmVudC9pbmRleFwiO1xuaW1wb3J0IHsgTk51bWJlclByb3BzIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50L05OdW1iZXJcIjtcblxuXG5leHBvcnQgdHlwZSBOUHJpY2VQcm9wcyA9IHtcbiAgaW5pdFJ1bGVzPzogYW55O1xuICBhbGxvd1plcm8/OiBib29sZWFuO1xuICBpc0FkZG9uQmVmb3JlPzogYm9vbGVhbjtcbn0gJiBOTnVtYmVyUHJvcHNcblxuY2xhc3MgTlByaWNlPFAgZXh0ZW5kcyBOUHJpY2VQcm9wcywgUywgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgcHJlY2lzaW9uOiAyLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBOUHJpY2VQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIGZvcm1hdHRlclByaWNlOiBhbnkgPSAodmFsdWU6IGFueSkgPT4ge1xuICAgIGlmICh0aGlzLnByb3BzLmZvcm1hdHRlcikge1xuICAgICAgcmV0dXJuIHRoaXMucHJvcHMuZm9ybWF0dGVyKHZhbHVlKTtcbiAgICB9XG4gICAgaWYgKFJ1bGVzLmlzRmxvYXROdW1iZXIodmFsdWUpKSB7XG4gICAgICByZXR1cm4gVXRpbHMudG9UaG91c2FuZHModmFsdWUpO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG4gIH07XG5cbiAgcGFyc2VyUHJpY2U6IGFueSA9ICh2YWx1ZTogYW55KSA9PiB7XG4gICAgaWYgKHRoaXMucHJvcHMucGFyc2VyKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy5wYXJzZXIodmFsdWUpO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWUucmVwbGFjZSgvXFxTXFwkXFxzP3woLCopL2csIFwiXCIpO1xuICB9O1xuXG4gIGdldEFkZG9uQmVmb3JlID0gKCkgPT4ge1xuICAgIGlmICh0aGlzLnByb3BzLmFkZG9uQmVmb3JlKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy5hZGRvbkJlZm9yZTtcbiAgICB9XG4gICAgcmV0dXJuIF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwiY3VycmVuY3lTeW1ib2xcIiwgXCJTJFwiKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgIGNvbnN0IHtcbiAgICAgIGZvcm0sIG1vZGVsLCBhbGxvd1plcm8gPSB0cnVlLCBsYWJlbCwgcHJvcE5hbWUsIGlzQWRkb25CZWZvcmUgPSB0cnVlLCBvbkNoYW5nZSwgcnVsZXMsIGluaXRSdWxlcywgcmVxdWlyZWQsIGxheW91dENvbCxcbiAgICAgIHBsYWNlaG9sZGVyLCB0cmFuc2Zvcm0sIC4uLnJlc3RcbiAgICB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gPE5OdW1iZXJcbiAgICAgIHsuLi57IGZvcm0sIG1vZGVsLCBsYWJlbCwgcHJvcE5hbWUsIG9uQ2hhbmdlLCBydWxlcywgcmVxdWlyZWQsIGxheW91dENvbCwgdHJhbnNmb3JtIH19XG4gICAgICB7Li4ucmVzdH1cbiAgICAgIGFkZG9uQmVmb3JlPXshaXNBZGRvbkJlZm9yZSA/IFwiXCIgOiB0aGlzLmdldEFkZG9uQmVmb3JlKCl9XG4gICAgICBmb3JtYXR0ZXI9e3RoaXMuZm9ybWF0dGVyUHJpY2V9XG4gICAgICBwYXJzZXI9e3RoaXMucGFyc2VyUHJpY2V9XG4gICAgICBydWxlcz17aW5pdFJ1bGVzIHx8IFtcbiAgICAgICAge1xuICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgIGlmIChSdWxlcy5pc051bUZyb21aZXJvKHZhbHVlKSkge1xuICAgICAgICAgICAgICBpZiAodGhhdC5wcm9wcy5wcmVjaXNpb24gPT09IDAgJiYgIVJ1bGVzLmlzTnVtYmVyRnJvbVplcm8odmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2soYCR7dGhhdC5wcm9wcy5sYWJlbH0gbXVzdCBiZSBwb3NpdGl2ZSBpbnRlZ2VyYCk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmICghcmVxdWlyZWQgJiYgIXZhbHVlICYmIHZhbHVlICE9PSAwKSB7XG4gICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGFsbG93WmVybykge1xuICAgICAgICAgICAgICBpZiAoUnVsZXMuaXNOdW1Gcm9tWmVyb0FuZEFsbG93WmVybyh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKGAke3RoYXQucHJvcHMubGFiZWx9IG11c3QgYSBudW1iZXIgZ3JlYXRlciB0aGFuIG9yIGVxdWFsIHRvIDBgKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgY2FsbGJhY2soYCR7dGhhdC5wcm9wcy5sYWJlbH0gbXVzdCBiZSBwb3NpdGl2ZSBudW1iZXJgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgXS5jb25jYXQodGhpcy5wcm9wcy5ydWxlcyB8fCBbXSl9XG4gICAgLz47XG4gIH1cbn1cblxuZXhwb3J0IHsgTlByaWNlIH07XG5cblxuZXhwb3J0IGNvbnN0IGZvcm1hdHRlclByaWNlOiBhbnkgPSAodmFsdWU6IGFueSkgPT4ge1xuICBpZiAoUnVsZXMuaXNGbG9hdE51bWJlcih2YWx1ZSkpIHtcbiAgICByZXR1cm4gVXRpbHMudG9UaG91c2FuZHModmFsdWUpO1xuICB9XG4gIHJldHVybiB2YWx1ZTtcbn07XG5cbmV4cG9ydCBjb25zdCBwYXJzZXJQcmljZTogYW55ID0gKHZhbHVlOiBhbnkpID0+IHtcbiAgcmV0dXJuIHZhbHVlLnJlcGxhY2UoL1xcU1xcJFxccz98KCwqKS9nLCBcIlwiKTtcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBU0E7Ozs7O0FBS0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFqQkE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUF4QkE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUEvQkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQTBCQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5CQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQThCQTs7OztBQXpFQTtBQUNBO0FBREE7QUFFQTtBQURBO0FBMkVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/component/NPrice.tsx
