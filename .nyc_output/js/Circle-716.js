__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_progress__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-progress */ "./node_modules/rc-progress/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils */ "./node_modules/antd/es/progress/utils.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}





var statusColorMap = {
  normal: '#108ee9',
  exception: '#ff5500',
  success: '#87d068'
};

function getPercentage(_ref) {
  var percent = _ref.percent,
      successPercent = _ref.successPercent;
  var ptg = Object(_utils__WEBPACK_IMPORTED_MODULE_3__["validProgress"])(percent);

  if (!successPercent) {
    return ptg;
  }

  var successPtg = Object(_utils__WEBPACK_IMPORTED_MODULE_3__["validProgress"])(successPercent);
  return [successPercent, Object(_utils__WEBPACK_IMPORTED_MODULE_3__["validProgress"])(ptg - successPtg)];
}

function getStrokeColor(_ref2) {
  var progressStatus = _ref2.progressStatus,
      successPercent = _ref2.successPercent,
      strokeColor = _ref2.strokeColor;
  var color = strokeColor || statusColorMap[progressStatus];

  if (!successPercent) {
    return color;
  }

  return [statusColorMap.success, color];
}

var Circle = function Circle(props) {
  var prefixCls = props.prefixCls,
      width = props.width,
      strokeWidth = props.strokeWidth,
      trailColor = props.trailColor,
      strokeLinecap = props.strokeLinecap,
      gapPosition = props.gapPosition,
      gapDegree = props.gapDegree,
      type = props.type,
      children = props.children;
  var circleSize = width || 120;
  var circleStyle = {
    width: circleSize,
    height: circleSize,
    fontSize: circleSize * 0.15 + 6
  };
  var circleWidth = strokeWidth || 6;
  var gapPos = gapPosition || type === 'dashboard' && 'bottom' || 'top';
  var gapDeg = gapDegree || (type === 'dashboard' ? 75 : undefined);
  var strokeColor = getStrokeColor(props);
  var isGradient = Object.prototype.toString.call(strokeColor) === '[object Object]';
  var wrapperClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-inner"), _defineProperty({}, "".concat(prefixCls, "-circle-gradient"), isGradient));
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: wrapperClassName,
    style: circleStyle
  }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_progress__WEBPACK_IMPORTED_MODULE_1__["Circle"], {
    percent: getPercentage(props),
    strokeWidth: circleWidth,
    trailWidth: circleWidth,
    strokeColor: strokeColor,
    strokeLinecap: strokeLinecap,
    trailColor: trailColor,
    prefixCls: prefixCls,
    gapDegree: gapDeg,
    gapPosition: gapPos
  }), children);
};

/* harmony default export */ __webpack_exports__["default"] = (Circle);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9wcm9ncmVzcy9DaXJjbGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3Byb2dyZXNzL0NpcmNsZS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgQ2lyY2xlIGFzIFJDQ2lyY2xlIH0gZnJvbSAncmMtcHJvZ3Jlc3MnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyB2YWxpZFByb2dyZXNzIH0gZnJvbSAnLi91dGlscyc7XG5jb25zdCBzdGF0dXNDb2xvck1hcCA9IHtcbiAgICBub3JtYWw6ICcjMTA4ZWU5JyxcbiAgICBleGNlcHRpb246ICcjZmY1NTAwJyxcbiAgICBzdWNjZXNzOiAnIzg3ZDA2OCcsXG59O1xuZnVuY3Rpb24gZ2V0UGVyY2VudGFnZSh7IHBlcmNlbnQsIHN1Y2Nlc3NQZXJjZW50IH0pIHtcbiAgICBjb25zdCBwdGcgPSB2YWxpZFByb2dyZXNzKHBlcmNlbnQpO1xuICAgIGlmICghc3VjY2Vzc1BlcmNlbnQpIHtcbiAgICAgICAgcmV0dXJuIHB0ZztcbiAgICB9XG4gICAgY29uc3Qgc3VjY2Vzc1B0ZyA9IHZhbGlkUHJvZ3Jlc3Moc3VjY2Vzc1BlcmNlbnQpO1xuICAgIHJldHVybiBbc3VjY2Vzc1BlcmNlbnQsIHZhbGlkUHJvZ3Jlc3MocHRnIC0gc3VjY2Vzc1B0ZyldO1xufVxuZnVuY3Rpb24gZ2V0U3Ryb2tlQ29sb3IoeyBwcm9ncmVzc1N0YXR1cywgc3VjY2Vzc1BlcmNlbnQsIHN0cm9rZUNvbG9yIH0pIHtcbiAgICBjb25zdCBjb2xvciA9IHN0cm9rZUNvbG9yIHx8IHN0YXR1c0NvbG9yTWFwW3Byb2dyZXNzU3RhdHVzXTtcbiAgICBpZiAoIXN1Y2Nlc3NQZXJjZW50KSB7XG4gICAgICAgIHJldHVybiBjb2xvcjtcbiAgICB9XG4gICAgcmV0dXJuIFtzdGF0dXNDb2xvck1hcC5zdWNjZXNzLCBjb2xvcl07XG59XG5jb25zdCBDaXJjbGUgPSBwcm9wcyA9PiB7XG4gICAgY29uc3QgeyBwcmVmaXhDbHMsIHdpZHRoLCBzdHJva2VXaWR0aCwgdHJhaWxDb2xvciwgc3Ryb2tlTGluZWNhcCwgZ2FwUG9zaXRpb24sIGdhcERlZ3JlZSwgdHlwZSwgY2hpbGRyZW4sIH0gPSBwcm9wcztcbiAgICBjb25zdCBjaXJjbGVTaXplID0gd2lkdGggfHwgMTIwO1xuICAgIGNvbnN0IGNpcmNsZVN0eWxlID0ge1xuICAgICAgICB3aWR0aDogY2lyY2xlU2l6ZSxcbiAgICAgICAgaGVpZ2h0OiBjaXJjbGVTaXplLFxuICAgICAgICBmb250U2l6ZTogY2lyY2xlU2l6ZSAqIDAuMTUgKyA2LFxuICAgIH07XG4gICAgY29uc3QgY2lyY2xlV2lkdGggPSBzdHJva2VXaWR0aCB8fCA2O1xuICAgIGNvbnN0IGdhcFBvcyA9IGdhcFBvc2l0aW9uIHx8ICh0eXBlID09PSAnZGFzaGJvYXJkJyAmJiAnYm90dG9tJykgfHwgJ3RvcCc7XG4gICAgY29uc3QgZ2FwRGVnID0gZ2FwRGVncmVlIHx8ICh0eXBlID09PSAnZGFzaGJvYXJkJyA/IDc1IDogdW5kZWZpbmVkKTtcbiAgICBjb25zdCBzdHJva2VDb2xvciA9IGdldFN0cm9rZUNvbG9yKHByb3BzKTtcbiAgICBjb25zdCBpc0dyYWRpZW50ID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHN0cm9rZUNvbG9yKSA9PT0gJ1tvYmplY3QgT2JqZWN0XSc7XG4gICAgY29uc3Qgd3JhcHBlckNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1pbm5lcmAsIHtcbiAgICAgICAgW2Ake3ByZWZpeENsc30tY2lyY2xlLWdyYWRpZW50YF06IGlzR3JhZGllbnQsXG4gICAgfSk7XG4gICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17d3JhcHBlckNsYXNzTmFtZX0gc3R5bGU9e2NpcmNsZVN0eWxlfT5cbiAgICAgIDxSQ0NpcmNsZSBwZXJjZW50PXtnZXRQZXJjZW50YWdlKHByb3BzKX0gc3Ryb2tlV2lkdGg9e2NpcmNsZVdpZHRofSB0cmFpbFdpZHRoPXtjaXJjbGVXaWR0aH0gc3Ryb2tlQ29sb3I9e3N0cm9rZUNvbG9yfSBzdHJva2VMaW5lY2FwPXtzdHJva2VMaW5lY2FwfSB0cmFpbENvbG9yPXt0cmFpbENvbG9yfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gZ2FwRGVncmVlPXtnYXBEZWd9IGdhcFBvc2l0aW9uPXtnYXBQb3N9Lz5cbiAgICAgIHtjaGlsZHJlbn1cbiAgICA8L2Rpdj4pO1xufTtcbmV4cG9ydCBkZWZhdWx0IENpcmNsZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWpCQTtBQUNBO0FBb0JBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/progress/Circle.js
