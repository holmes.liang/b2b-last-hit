__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withRefreshQueryHoc", function() { return withRefreshQueryHoc; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/hoc/query-refresh.tsx";

function withRefreshQueryHoc(WrappedComponent) {
  var QueryHoc =
  /*#__PURE__*/
  function (_React$Component) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(QueryHoc, _React$Component);

    function QueryHoc(props, context) {
      var _this;

      Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, QueryHoc);

      _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(QueryHoc).call(this, props, context));
      _this.queryComponent = {};

      _this.componentDidRecover = function () {
        if (!_this.queryComponent) return;
        if (!_this.queryComponent.queryPage) return;
        if (!_this.queryComponent.queryPage.refreshCardList) return;

        _this.queryComponent.queryPage.refreshCardList();
      };

      _this.initQueryComponentRef = function (ref) {
        _this.queryComponent = ref;
      };

      props.cacheLifecycles && props.cacheLifecycles.didRecover(_this.componentDidRecover);
      return _this;
    }

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(QueryHoc, [{
      key: "render",
      value: function render() {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(WrappedComponent, Object.assign({
          ref: this.initQueryComponentRef
        }, this.props, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 20
          },
          __self: this
        }));
      } // methods

    }]);

    return QueryHoc;
  }(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].Component);

  ;
  return QueryHoc;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2hvYy9xdWVyeS1yZWZyZXNoLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9ob2MvcXVlcnktcmVmcmVzaC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcblxuZXhwb3J0IGZ1bmN0aW9uIHdpdGhSZWZyZXNoUXVlcnlIb2MoV3JhcHBlZENvbXBvbmVudDogYW55KTogYW55IHtcbiAgY2xhc3MgUXVlcnlIb2MgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIHByb3RlY3RlZCBxdWVyeUNvbXBvbmVudDogYW55ID0ge307XG5cbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBjb250ZXh0PzogYW55KSB7XG4gICAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gICAgICBwcm9wcy5jYWNoZUxpZmVjeWNsZXMgJiYgcHJvcHMuY2FjaGVMaWZlY3ljbGVzLmRpZFJlY292ZXIodGhpcy5jb21wb25lbnREaWRSZWNvdmVyKTtcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRSZWNvdmVyID0gKCkgPT4ge1xuICAgICAgaWYgKCF0aGlzLnF1ZXJ5Q29tcG9uZW50KSByZXR1cm47XG4gICAgICBpZiAoIXRoaXMucXVlcnlDb21wb25lbnQucXVlcnlQYWdlKSByZXR1cm47XG4gICAgICBpZiAoIXRoaXMucXVlcnlDb21wb25lbnQucXVlcnlQYWdlLnJlZnJlc2hDYXJkTGlzdCkgcmV0dXJuO1xuICAgICAgdGhpcy5xdWVyeUNvbXBvbmVudC5xdWVyeVBhZ2UucmVmcmVzaENhcmRMaXN0KCk7XG4gICAgfTtcblxuICAgIHJlbmRlcigpIHtcbiAgICAgIHJldHVybiA8V3JhcHBlZENvbXBvbmVudCByZWY9e3RoaXMuaW5pdFF1ZXJ5Q29tcG9uZW50UmVmfSB7Li4uKHRoaXMucHJvcHMgYXMgYW55KX0gLz47XG4gICAgfVxuXG4gICAgLy8gbWV0aG9kc1xuICAgIGluaXRRdWVyeUNvbXBvbmVudFJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgICAgdGhpcy5xdWVyeUNvbXBvbmVudCA9IHJlZjtcbiAgICB9O1xuICB9O1xuICByZXR1cm4gUXVlcnlIb2M7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVhBO0FBa0JBO0FBQ0E7QUFDQTtBQWxCQTtBQUZBO0FBR0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQWlCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBbkJBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUF1QkE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/hoc/query-refresh.tsx
