
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

var getStyleProperty = __webpack_require__(/*! ./getStyleProperty */ "./node_modules/fbjs/lib/getStyleProperty.js");
/**
 * @param {DOMNode} element [description]
 * @param {string} name Overflow style property name.
 * @return {boolean} True if the supplied ndoe is scrollable.
 */


function _isNodeScrollable(element, name) {
  var overflow = Style.get(element, name);
  return overflow === 'auto' || overflow === 'scroll';
}
/**
 * Utilities for querying and mutating style properties.
 */


var Style = {
  /**
   * Gets the style property for the supplied node. This will return either the
   * computed style, if available, or the declared style.
   *
   * @param {DOMNode} node
   * @param {string} name Style property name.
   * @return {?string} Style property value.
   */
  get: getStyleProperty,

  /**
   * Determines the nearest ancestor of a node that is scrollable.
   *
   * NOTE: This can be expensive if used repeatedly or on a node nested deeply.
   *
   * @param {?DOMNode} node Node from which to start searching.
   * @return {?DOMWindow|DOMElement} Scroll parent of the supplied node.
   */
  getScrollParent: function getScrollParent(node) {
    if (!node) {
      return null;
    }

    var ownerDocument = node.ownerDocument;

    while (node && node !== ownerDocument.body) {
      if (_isNodeScrollable(node, 'overflow') || _isNodeScrollable(node, 'overflowY') || _isNodeScrollable(node, 'overflowX')) {
        return node;
      }

      node = node.parentNode;
    }

    return ownerDocument.defaultView || ownerDocument.parentWindow;
  }
};
module.exports = Style;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvU3R5bGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9TdHlsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqL1xuXG52YXIgZ2V0U3R5bGVQcm9wZXJ0eSA9IHJlcXVpcmUoJy4vZ2V0U3R5bGVQcm9wZXJ0eScpO1xuXG4vKipcbiAqIEBwYXJhbSB7RE9NTm9kZX0gZWxlbWVudCBbZGVzY3JpcHRpb25dXG4gKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBPdmVyZmxvdyBzdHlsZSBwcm9wZXJ0eSBuYW1lLlxuICogQHJldHVybiB7Ym9vbGVhbn0gVHJ1ZSBpZiB0aGUgc3VwcGxpZWQgbmRvZSBpcyBzY3JvbGxhYmxlLlxuICovXG5mdW5jdGlvbiBfaXNOb2RlU2Nyb2xsYWJsZShlbGVtZW50LCBuYW1lKSB7XG4gIHZhciBvdmVyZmxvdyA9IFN0eWxlLmdldChlbGVtZW50LCBuYW1lKTtcbiAgcmV0dXJuIG92ZXJmbG93ID09PSAnYXV0bycgfHwgb3ZlcmZsb3cgPT09ICdzY3JvbGwnO1xufVxuXG4vKipcbiAqIFV0aWxpdGllcyBmb3IgcXVlcnlpbmcgYW5kIG11dGF0aW5nIHN0eWxlIHByb3BlcnRpZXMuXG4gKi9cbnZhciBTdHlsZSA9IHtcbiAgLyoqXG4gICAqIEdldHMgdGhlIHN0eWxlIHByb3BlcnR5IGZvciB0aGUgc3VwcGxpZWQgbm9kZS4gVGhpcyB3aWxsIHJldHVybiBlaXRoZXIgdGhlXG4gICAqIGNvbXB1dGVkIHN0eWxlLCBpZiBhdmFpbGFibGUsIG9yIHRoZSBkZWNsYXJlZCBzdHlsZS5cbiAgICpcbiAgICogQHBhcmFtIHtET01Ob2RlfSBub2RlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIFN0eWxlIHByb3BlcnR5IG5hbWUuXG4gICAqIEByZXR1cm4gez9zdHJpbmd9IFN0eWxlIHByb3BlcnR5IHZhbHVlLlxuICAgKi9cbiAgZ2V0OiBnZXRTdHlsZVByb3BlcnR5LFxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHRoZSBuZWFyZXN0IGFuY2VzdG9yIG9mIGEgbm9kZSB0aGF0IGlzIHNjcm9sbGFibGUuXG4gICAqXG4gICAqIE5PVEU6IFRoaXMgY2FuIGJlIGV4cGVuc2l2ZSBpZiB1c2VkIHJlcGVhdGVkbHkgb3Igb24gYSBub2RlIG5lc3RlZCBkZWVwbHkuXG4gICAqXG4gICAqIEBwYXJhbSB7P0RPTU5vZGV9IG5vZGUgTm9kZSBmcm9tIHdoaWNoIHRvIHN0YXJ0IHNlYXJjaGluZy5cbiAgICogQHJldHVybiB7P0RPTVdpbmRvd3xET01FbGVtZW50fSBTY3JvbGwgcGFyZW50IG9mIHRoZSBzdXBwbGllZCBub2RlLlxuICAgKi9cbiAgZ2V0U2Nyb2xsUGFyZW50OiBmdW5jdGlvbiBnZXRTY3JvbGxQYXJlbnQobm9kZSkge1xuICAgIGlmICghbm9kZSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIHZhciBvd25lckRvY3VtZW50ID0gbm9kZS5vd25lckRvY3VtZW50O1xuICAgIHdoaWxlIChub2RlICYmIG5vZGUgIT09IG93bmVyRG9jdW1lbnQuYm9keSkge1xuICAgICAgaWYgKF9pc05vZGVTY3JvbGxhYmxlKG5vZGUsICdvdmVyZmxvdycpIHx8IF9pc05vZGVTY3JvbGxhYmxlKG5vZGUsICdvdmVyZmxvd1knKSB8fCBfaXNOb2RlU2Nyb2xsYWJsZShub2RlLCAnb3ZlcmZsb3dYJykpIHtcbiAgICAgICAgcmV0dXJuIG5vZGU7XG4gICAgICB9XG4gICAgICBub2RlID0gbm9kZS5wYXJlbnROb2RlO1xuICAgIH1cbiAgICByZXR1cm4gb3duZXJEb2N1bWVudC5kZWZhdWx0VmlldyB8fCBvd25lckRvY3VtZW50LnBhcmVudFdpbmRvdztcbiAgfVxuXG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFN0eWxlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7O0FBU0E7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQS9CQTtBQW1DQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/Style.js
