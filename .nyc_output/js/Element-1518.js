var guid = __webpack_require__(/*! ./core/guid */ "./node_modules/zrender/lib/core/guid.js");

var Eventful = __webpack_require__(/*! ./mixin/Eventful */ "./node_modules/zrender/lib/mixin/Eventful.js");

var Transformable = __webpack_require__(/*! ./mixin/Transformable */ "./node_modules/zrender/lib/mixin/Transformable.js");

var Animatable = __webpack_require__(/*! ./mixin/Animatable */ "./node_modules/zrender/lib/mixin/Animatable.js");

var zrUtil = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");
/**
 * @alias module:zrender/Element
 * @constructor
 * @extends {module:zrender/mixin/Animatable}
 * @extends {module:zrender/mixin/Transformable}
 * @extends {module:zrender/mixin/Eventful}
 */


var Element = function Element(opts) {
  // jshint ignore:line
  Transformable.call(this, opts);
  Eventful.call(this, opts);
  Animatable.call(this, opts);
  /**
   * 画布元素ID
   * @type {string}
   */

  this.id = opts.id || guid();
};

Element.prototype = {
  /**
   * 元素类型
   * Element type
   * @type {string}
   */
  type: 'element',

  /**
   * 元素名字
   * Element name
   * @type {string}
   */
  name: '',

  /**
   * ZRender 实例对象，会在 element 添加到 zrender 实例中后自动赋值
   * ZRender instance will be assigned when element is associated with zrender
   * @name module:/zrender/Element#__zr
   * @type {module:zrender/ZRender}
   */
  __zr: null,

  /**
   * 图形是否忽略，为true时忽略图形的绘制以及事件触发
   * If ignore drawing and events of the element object
   * @name module:/zrender/Element#ignore
   * @type {boolean}
   * @default false
   */
  ignore: false,

  /**
   * 用于裁剪的路径(shape)，所有 Group 内的路径在绘制时都会被这个路径裁剪
   * 该路径会继承被裁减对象的变换
   * @type {module:zrender/graphic/Path}
   * @see http://www.w3.org/TR/2dcontext/#clipping-region
   * @readOnly
   */
  clipPath: null,

  /**
   * 是否是 Group
   * @type {boolean}
   */
  isGroup: false,

  /**
   * Drift element
   * @param  {number} dx dx on the global space
   * @param  {number} dy dy on the global space
   */
  drift: function drift(dx, dy) {
    switch (this.draggable) {
      case 'horizontal':
        dy = 0;
        break;

      case 'vertical':
        dx = 0;
        break;
    }

    var m = this.transform;

    if (!m) {
      m = this.transform = [1, 0, 0, 1, 0, 0];
    }

    m[4] += dx;
    m[5] += dy;
    this.decomposeTransform();
    this.dirty(false);
  },

  /**
   * Hook before update
   */
  beforeUpdate: function beforeUpdate() {},

  /**
   * Hook after update
   */
  afterUpdate: function afterUpdate() {},

  /**
   * Update each frame
   */
  update: function update() {
    this.updateTransform();
  },

  /**
   * @param  {Function} cb
   * @param  {}   context
   */
  traverse: function traverse(cb, context) {},

  /**
   * @protected
   */
  attrKV: function attrKV(key, value) {
    if (key === 'position' || key === 'scale' || key === 'origin') {
      // Copy the array
      if (value) {
        var target = this[key];

        if (!target) {
          target = this[key] = [];
        }

        target[0] = value[0];
        target[1] = value[1];
      }
    } else {
      this[key] = value;
    }
  },

  /**
   * Hide the element
   */
  hide: function hide() {
    this.ignore = true;
    this.__zr && this.__zr.refresh();
  },

  /**
   * Show the element
   */
  show: function show() {
    this.ignore = false;
    this.__zr && this.__zr.refresh();
  },

  /**
   * @param {string|Object} key
   * @param {*} value
   */
  attr: function attr(key, value) {
    if (typeof key === 'string') {
      this.attrKV(key, value);
    } else if (zrUtil.isObject(key)) {
      for (var name in key) {
        if (key.hasOwnProperty(name)) {
          this.attrKV(name, key[name]);
        }
      }
    }

    this.dirty(false);
    return this;
  },

  /**
   * @param {module:zrender/graphic/Path} clipPath
   */
  setClipPath: function setClipPath(clipPath) {
    var zr = this.__zr;

    if (zr) {
      clipPath.addSelfToZr(zr);
    } // Remove previous clip path


    if (this.clipPath && this.clipPath !== clipPath) {
      this.removeClipPath();
    }

    this.clipPath = clipPath;
    clipPath.__zr = zr;
    clipPath.__clipTarget = this;
    this.dirty(false);
  },

  /**
   */
  removeClipPath: function removeClipPath() {
    var clipPath = this.clipPath;

    if (clipPath) {
      if (clipPath.__zr) {
        clipPath.removeSelfFromZr(clipPath.__zr);
      }

      clipPath.__zr = null;
      clipPath.__clipTarget = null;
      this.clipPath = null;
      this.dirty(false);
    }
  },

  /**
   * Add self from zrender instance.
   * Not recursively because it will be invoked when element added to storage.
   * @param {module:zrender/ZRender} zr
   */
  addSelfToZr: function addSelfToZr(zr) {
    this.__zr = zr; // 添加动画

    var animators = this.animators;

    if (animators) {
      for (var i = 0; i < animators.length; i++) {
        zr.animation.addAnimator(animators[i]);
      }
    }

    if (this.clipPath) {
      this.clipPath.addSelfToZr(zr);
    }
  },

  /**
   * Remove self from zrender instance.
   * Not recursively because it will be invoked when element added to storage.
   * @param {module:zrender/ZRender} zr
   */
  removeSelfFromZr: function removeSelfFromZr(zr) {
    this.__zr = null; // 移除动画

    var animators = this.animators;

    if (animators) {
      for (var i = 0; i < animators.length; i++) {
        zr.animation.removeAnimator(animators[i]);
      }
    }

    if (this.clipPath) {
      this.clipPath.removeSelfFromZr(zr);
    }
  }
};
zrUtil.mixin(Element, Animatable);
zrUtil.mixin(Element, Transformable);
zrUtil.mixin(Element, Eventful);
var _default = Element;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvRWxlbWVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL0VsZW1lbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGd1aWQgPSByZXF1aXJlKFwiLi9jb3JlL2d1aWRcIik7XG5cbnZhciBFdmVudGZ1bCA9IHJlcXVpcmUoXCIuL21peGluL0V2ZW50ZnVsXCIpO1xuXG52YXIgVHJhbnNmb3JtYWJsZSA9IHJlcXVpcmUoXCIuL21peGluL1RyYW5zZm9ybWFibGVcIik7XG5cbnZhciBBbmltYXRhYmxlID0gcmVxdWlyZShcIi4vbWl4aW4vQW5pbWF0YWJsZVwiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuL2NvcmUvdXRpbFwiKTtcblxuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOnpyZW5kZXIvRWxlbWVudFxuICogQGNvbnN0cnVjdG9yXG4gKiBAZXh0ZW5kcyB7bW9kdWxlOnpyZW5kZXIvbWl4aW4vQW5pbWF0YWJsZX1cbiAqIEBleHRlbmRzIHttb2R1bGU6enJlbmRlci9taXhpbi9UcmFuc2Zvcm1hYmxlfVxuICogQGV4dGVuZHMge21vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsfVxuICovXG52YXIgRWxlbWVudCA9IGZ1bmN0aW9uIChvcHRzKSB7XG4gIC8vIGpzaGludCBpZ25vcmU6bGluZVxuICBUcmFuc2Zvcm1hYmxlLmNhbGwodGhpcywgb3B0cyk7XG4gIEV2ZW50ZnVsLmNhbGwodGhpcywgb3B0cyk7XG4gIEFuaW1hdGFibGUuY2FsbCh0aGlzLCBvcHRzKTtcbiAgLyoqXG4gICAqIOeUu+W4g+WFg+e0oElEXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuXG4gIHRoaXMuaWQgPSBvcHRzLmlkIHx8IGd1aWQoKTtcbn07XG5cbkVsZW1lbnQucHJvdG90eXBlID0ge1xuICAvKipcbiAgICog5YWD57Sg57G75Z6LXG4gICAqIEVsZW1lbnQgdHlwZVxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgdHlwZTogJ2VsZW1lbnQnLFxuXG4gIC8qKlxuICAgKiDlhYPntKDlkI3lrZdcbiAgICogRWxlbWVudCBuYW1lXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBuYW1lOiAnJyxcblxuICAvKipcbiAgICogWlJlbmRlciDlrp7kvovlr7nosaHvvIzkvJrlnKggZWxlbWVudCDmt7vliqDliLAgenJlbmRlciDlrp7kvovkuK3lkI7oh6rliqjotYvlgLxcbiAgICogWlJlbmRlciBpbnN0YW5jZSB3aWxsIGJlIGFzc2lnbmVkIHdoZW4gZWxlbWVudCBpcyBhc3NvY2lhdGVkIHdpdGggenJlbmRlclxuICAgKiBAbmFtZSBtb2R1bGU6L3pyZW5kZXIvRWxlbWVudCNfX3pyXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9aUmVuZGVyfVxuICAgKi9cbiAgX196cjogbnVsbCxcblxuICAvKipcbiAgICog5Zu+5b2i5piv5ZCm5b+955Wl77yM5Li6dHJ1ZeaXtuW/veeVpeWbvuW9oueahOe7mOWItuS7peWPiuS6i+S7tuinpuWPkVxuICAgKiBJZiBpZ25vcmUgZHJhd2luZyBhbmQgZXZlbnRzIG9mIHRoZSBlbGVtZW50IG9iamVjdFxuICAgKiBAbmFtZSBtb2R1bGU6L3pyZW5kZXIvRWxlbWVudCNpZ25vcmVcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBpZ25vcmU6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiDnlKjkuo7oo4HliarnmoTot6/lvoQoc2hhcGUp77yM5omA5pyJIEdyb3VwIOWGheeahOi3r+W+hOWcqOe7mOWItuaXtumDveS8muiiq+i/meS4qui3r+W+hOijgeWJqlxuICAgKiDor6Xot6/lvoTkvJrnu6fmib/ooqvoo4Hlh4/lr7nosaHnmoTlj5jmjaJcbiAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL2dyYXBoaWMvUGF0aH1cbiAgICogQHNlZSBodHRwOi8vd3d3LnczLm9yZy9UUi8yZGNvbnRleHQvI2NsaXBwaW5nLXJlZ2lvblxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIGNsaXBQYXRoOiBudWxsLFxuXG4gIC8qKlxuICAgKiDmmK/lkKbmmK8gR3JvdXBcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICBpc0dyb3VwOiBmYWxzZSxcblxuICAvKipcbiAgICogRHJpZnQgZWxlbWVudFxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IGR4IGR4IG9uIHRoZSBnbG9iYWwgc3BhY2VcbiAgICogQHBhcmFtICB7bnVtYmVyfSBkeSBkeSBvbiB0aGUgZ2xvYmFsIHNwYWNlXG4gICAqL1xuICBkcmlmdDogZnVuY3Rpb24gKGR4LCBkeSkge1xuICAgIHN3aXRjaCAodGhpcy5kcmFnZ2FibGUpIHtcbiAgICAgIGNhc2UgJ2hvcml6b250YWwnOlxuICAgICAgICBkeSA9IDA7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlICd2ZXJ0aWNhbCc6XG4gICAgICAgIGR4ID0gMDtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgdmFyIG0gPSB0aGlzLnRyYW5zZm9ybTtcblxuICAgIGlmICghbSkge1xuICAgICAgbSA9IHRoaXMudHJhbnNmb3JtID0gWzEsIDAsIDAsIDEsIDAsIDBdO1xuICAgIH1cblxuICAgIG1bNF0gKz0gZHg7XG4gICAgbVs1XSArPSBkeTtcbiAgICB0aGlzLmRlY29tcG9zZVRyYW5zZm9ybSgpO1xuICAgIHRoaXMuZGlydHkoZmFsc2UpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBIb29rIGJlZm9yZSB1cGRhdGVcbiAgICovXG4gIGJlZm9yZVVwZGF0ZTogZnVuY3Rpb24gKCkge30sXG5cbiAgLyoqXG4gICAqIEhvb2sgYWZ0ZXIgdXBkYXRlXG4gICAqL1xuICBhZnRlclVwZGF0ZTogZnVuY3Rpb24gKCkge30sXG5cbiAgLyoqXG4gICAqIFVwZGF0ZSBlYWNoIGZyYW1lXG4gICAqL1xuICB1cGRhdGU6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLnVwZGF0ZVRyYW5zZm9ybSgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0gIHtGdW5jdGlvbn0gY2JcbiAgICogQHBhcmFtICB7fSAgIGNvbnRleHRcbiAgICovXG4gIHRyYXZlcnNlOiBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHt9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBhdHRyS1Y6IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gICAgaWYgKGtleSA9PT0gJ3Bvc2l0aW9uJyB8fCBrZXkgPT09ICdzY2FsZScgfHwga2V5ID09PSAnb3JpZ2luJykge1xuICAgICAgLy8gQ29weSB0aGUgYXJyYXlcbiAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICB2YXIgdGFyZ2V0ID0gdGhpc1trZXldO1xuXG4gICAgICAgIGlmICghdGFyZ2V0KSB7XG4gICAgICAgICAgdGFyZ2V0ID0gdGhpc1trZXldID0gW107XG4gICAgICAgIH1cblxuICAgICAgICB0YXJnZXRbMF0gPSB2YWx1ZVswXTtcbiAgICAgICAgdGFyZ2V0WzFdID0gdmFsdWVbMV07XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXNba2V5XSA9IHZhbHVlO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogSGlkZSB0aGUgZWxlbWVudFxuICAgKi9cbiAgaGlkZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuaWdub3JlID0gdHJ1ZTtcbiAgICB0aGlzLl9fenIgJiYgdGhpcy5fX3pyLnJlZnJlc2goKTtcbiAgfSxcblxuICAvKipcbiAgICogU2hvdyB0aGUgZWxlbWVudFxuICAgKi9cbiAgc2hvdzogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuaWdub3JlID0gZmFsc2U7XG4gICAgdGhpcy5fX3pyICYmIHRoaXMuX196ci5yZWZyZXNoKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfE9iamVjdH0ga2V5XG4gICAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAgICovXG4gIGF0dHI6IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gICAgaWYgKHR5cGVvZiBrZXkgPT09ICdzdHJpbmcnKSB7XG4gICAgICB0aGlzLmF0dHJLVihrZXksIHZhbHVlKTtcbiAgICB9IGVsc2UgaWYgKHpyVXRpbC5pc09iamVjdChrZXkpKSB7XG4gICAgICBmb3IgKHZhciBuYW1lIGluIGtleSkge1xuICAgICAgICBpZiAoa2V5Lmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgICAgdGhpcy5hdHRyS1YobmFtZSwga2V5W25hbWVdKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuZGlydHkoZmFsc2UpO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2dyYXBoaWMvUGF0aH0gY2xpcFBhdGhcbiAgICovXG4gIHNldENsaXBQYXRoOiBmdW5jdGlvbiAoY2xpcFBhdGgpIHtcbiAgICB2YXIgenIgPSB0aGlzLl9fenI7XG5cbiAgICBpZiAoenIpIHtcbiAgICAgIGNsaXBQYXRoLmFkZFNlbGZUb1pyKHpyKTtcbiAgICB9IC8vIFJlbW92ZSBwcmV2aW91cyBjbGlwIHBhdGhcblxuXG4gICAgaWYgKHRoaXMuY2xpcFBhdGggJiYgdGhpcy5jbGlwUGF0aCAhPT0gY2xpcFBhdGgpIHtcbiAgICAgIHRoaXMucmVtb3ZlQ2xpcFBhdGgoKTtcbiAgICB9XG5cbiAgICB0aGlzLmNsaXBQYXRoID0gY2xpcFBhdGg7XG4gICAgY2xpcFBhdGguX196ciA9IHpyO1xuICAgIGNsaXBQYXRoLl9fY2xpcFRhcmdldCA9IHRoaXM7XG4gICAgdGhpcy5kaXJ0eShmYWxzZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqL1xuICByZW1vdmVDbGlwUGF0aDogZnVuY3Rpb24gKCkge1xuICAgIHZhciBjbGlwUGF0aCA9IHRoaXMuY2xpcFBhdGg7XG5cbiAgICBpZiAoY2xpcFBhdGgpIHtcbiAgICAgIGlmIChjbGlwUGF0aC5fX3pyKSB7XG4gICAgICAgIGNsaXBQYXRoLnJlbW92ZVNlbGZGcm9tWnIoY2xpcFBhdGguX196cik7XG4gICAgICB9XG5cbiAgICAgIGNsaXBQYXRoLl9fenIgPSBudWxsO1xuICAgICAgY2xpcFBhdGguX19jbGlwVGFyZ2V0ID0gbnVsbDtcbiAgICAgIHRoaXMuY2xpcFBhdGggPSBudWxsO1xuICAgICAgdGhpcy5kaXJ0eShmYWxzZSk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBBZGQgc2VsZiBmcm9tIHpyZW5kZXIgaW5zdGFuY2UuXG4gICAqIE5vdCByZWN1cnNpdmVseSBiZWNhdXNlIGl0IHdpbGwgYmUgaW52b2tlZCB3aGVuIGVsZW1lbnQgYWRkZWQgdG8gc3RvcmFnZS5cbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9aUmVuZGVyfSB6clxuICAgKi9cbiAgYWRkU2VsZlRvWnI6IGZ1bmN0aW9uICh6cikge1xuICAgIHRoaXMuX196ciA9IHpyOyAvLyDmt7vliqDliqjnlLtcblxuICAgIHZhciBhbmltYXRvcnMgPSB0aGlzLmFuaW1hdG9ycztcblxuICAgIGlmIChhbmltYXRvcnMpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYW5pbWF0b3JzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHpyLmFuaW1hdGlvbi5hZGRBbmltYXRvcihhbmltYXRvcnNbaV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLmNsaXBQYXRoKSB7XG4gICAgICB0aGlzLmNsaXBQYXRoLmFkZFNlbGZUb1pyKHpyKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlbW92ZSBzZWxmIGZyb20genJlbmRlciBpbnN0YW5jZS5cbiAgICogTm90IHJlY3Vyc2l2ZWx5IGJlY2F1c2UgaXQgd2lsbCBiZSBpbnZva2VkIHdoZW4gZWxlbWVudCBhZGRlZCB0byBzdG9yYWdlLlxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL1pSZW5kZXJ9IHpyXG4gICAqL1xuICByZW1vdmVTZWxmRnJvbVpyOiBmdW5jdGlvbiAoenIpIHtcbiAgICB0aGlzLl9fenIgPSBudWxsOyAvLyDnp7vpmaTliqjnlLtcblxuICAgIHZhciBhbmltYXRvcnMgPSB0aGlzLmFuaW1hdG9ycztcblxuICAgIGlmIChhbmltYXRvcnMpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYW5pbWF0b3JzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHpyLmFuaW1hdGlvbi5yZW1vdmVBbmltYXRvcihhbmltYXRvcnNbaV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLmNsaXBQYXRoKSB7XG4gICAgICB0aGlzLmNsaXBQYXRoLnJlbW92ZVNlbGZGcm9tWnIoenIpO1xuICAgIH1cbiAgfVxufTtcbnpyVXRpbC5taXhpbihFbGVtZW50LCBBbmltYXRhYmxlKTtcbnpyVXRpbC5taXhpbihFbGVtZW50LCBUcmFuc2Zvcm1hYmxlKTtcbnpyVXRpbC5taXhpbihFbGVtZW50LCBFdmVudGZ1bCk7XG52YXIgX2RlZmF1bHQgPSBFbGVtZW50O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeE9BO0FBME9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/Element.js
