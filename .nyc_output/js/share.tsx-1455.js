__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/share.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        .share-Popover {\n          .ant-popover-inner-content {\n            padding: 0;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var Share =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Share, _ModelWidget);

  function Share(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Share);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Share).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Share, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        Share: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Share.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var C = this.getComponents();
      var content = this.props.content || _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("ul", {
        className: "share-btn",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("li", {
        onClick: function onClick() {
          _this.props.handleShareToLine && _this.props.handleShareToLine();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
        className: "iconfont icon-line",
        style: {
          fontSize: "15px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54
        },
        __self: this
      }), _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Send via Line").thai("ส่งผ่าน Line").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("li", {
        onClick: function onClick() {
          _this.props.handleCopyShareLink && _this.props.handleCopyShareLink();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Icon"], {
        type: "copy",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }), _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Copy Link").thai("คัดลอกลิงค์").getMessage()));
      var style = isMobile ? Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, this.props.style, {}, {
        display: "block",
        textAlign: "center",
        marginBottom: "20px"
      }) : this.props.style;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Share, {
        style: style,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Popover"], {
        overlayClassName: "share-Popover",
        placement: "right",
        title: "",
        content: content,
        trigger: "click",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("img", {
        style: {
          width: "45px",
          cursor: "pointer"
        },
        src: __webpack_require__(/*! ../../../assets/share.png */ "./src/assets/share.png"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      })));
    }
  }]);

  return Share;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Share);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3NoYXJlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9zaGFyZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgUmVhY3QsXG4gIFN0eWxlZCxcbn0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24sIFBvcG92ZXIgfSBmcm9tIFwiYW50ZFwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5leHBvcnQgdHlwZSBTaGFyZVByb3BzID0ge1xuICBoYW5kbGVTaGFyZVRvTGluZT86ICgpID0+IHZvaWQ7XG4gIGhhbmRsZUNvcHlTaGFyZUxpbms/OiAoKSA9PiB2b2lkO1xuICBzdHlsZT86IGFueTtcbiAgY29udGVudD86IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBTaGFyZVN0YXRlID0ge31cbnR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbnR5cGUgU2hhcmVDb21wb25lbnRzID0ge1xuICBTaGFyZTogU3R5bGVkRElWLFxufVxuXG5jbGFzcyBTaGFyZTxQIGV4dGVuZHMgU2hhcmVQcm9wcywgUyBleHRlbmRzIFNoYXJlU3RhdGUsIEMgZXh0ZW5kcyBTaGFyZUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogU2hhcmVQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgU2hhcmU6IFN0eWxlZC5kaXZgXG4gICAgICAgIC5zaGFyZS1Qb3BvdmVyIHtcbiAgICAgICAgICAuYW50LXBvcG92ZXItaW5uZXItY29udGVudCB7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7fSkgYXMgUztcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgY29udGVudCA9IHRoaXMucHJvcHMuY29udGVudCB8fCAoXG4gICAgICA8dWwgY2xhc3NOYW1lPVwic2hhcmUtYnRuXCI+XG4gICAgICAgIDxsaSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgdGhpcy5wcm9wcy5oYW5kbGVTaGFyZVRvTGluZSAmJiB0aGlzLnByb3BzLmhhbmRsZVNoYXJlVG9MaW5lKCk7XG4gICAgICAgIH19PlxuICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tbGluZVwiIHN0eWxlPXt7IGZvbnRTaXplOiBcIjE1cHhcIiB9fS8+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2VuZCB2aWEgTGluZVwiKS50aGFpKFwi4Liq4LmI4LiH4Lic4LmI4Liy4LiZIExpbmVcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2xpPlxuICAgICAgICA8bGkgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIHRoaXMucHJvcHMuaGFuZGxlQ29weVNoYXJlTGluayAmJiB0aGlzLnByb3BzLmhhbmRsZUNvcHlTaGFyZUxpbmsoKTtcbiAgICAgICAgfX0+XG4gICAgICAgICAgPEljb24gdHlwZT1cImNvcHlcIi8+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiQ29weSBMaW5rXCIpLnRoYWkoXCLguITguLHguJTguKXguK3guIHguKXguLTguIfguITguYxcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2xpPlxuICAgICAgPC91bD5cbiAgICApO1xuICAgIGNvbnN0IHN0eWxlID0gaXNNb2JpbGUgPyB7XG4gICAgICAuLi50aGlzLnByb3BzLnN0eWxlLFxuICAgICAgLi4ue1xuICAgICAgICBkaXNwbGF5OiBcImJsb2NrXCIsXG4gICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcbiAgICAgICAgbWFyZ2luQm90dG9tOiBcIjIwcHhcIixcbiAgICAgIH0sXG4gICAgfSA6IHRoaXMucHJvcHMuc3R5bGU7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLlNoYXJlIHN0eWxlPXtzdHlsZX0+XG4gICAgICAgIDxQb3BvdmVyIG92ZXJsYXlDbGFzc05hbWU9XCJzaGFyZS1Qb3BvdmVyXCIgcGxhY2VtZW50PVwicmlnaHRcIiB0aXRsZT17XCJcIn0gY29udGVudD17Y29udGVudH0gdHJpZ2dlcj1cImNsaWNrXCI+XG4gICAgICAgICAgPGltZyBzdHlsZT17eyB3aWR0aDogXCI0NXB4XCIsIGN1cnNvcjogXCJwb2ludGVyXCIgfX0gc3JjPXtyZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL3NoYXJlLnBuZ1wiKX0vPlxuICAgICAgICA8L1BvcG92ZXI+XG4gICAgICA8L0MuU2hhcmU+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTaGFyZTsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBSUE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQWNBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFTQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFHQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBdERBO0FBQ0E7QUF3REEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/share.tsx
