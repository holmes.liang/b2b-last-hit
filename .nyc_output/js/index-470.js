__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Carousel; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}




 // matchMedia polyfill for
// https://github.com/WickyNilliams/enquire.js/issues/82
// TODO: Will be removed in antd 4.0 because we will no longer support ie9

if (typeof window !== 'undefined') {
  var matchMediaPolyfill = function matchMediaPolyfill(mediaQuery) {
    return {
      media: mediaQuery,
      matches: false,
      addListener: function addListener() {},
      removeListener: function removeListener() {}
    };
  }; // ref: https://github.com/ant-design/ant-design/issues/18774


  if (!window.matchMedia) window.matchMedia = matchMediaPolyfill;
} // Use require over import (will be lifted up)
// make sure matchMedia polyfill run before require('react-slick')
// Fix https://github.com/ant-design/ant-design/issues/6560
// Fix https://github.com/ant-design/ant-design/issues/3308


var SlickCarousel = __webpack_require__(/*! react-slick */ "./node_modules/react-slick/lib/index.js")["default"];

var Carousel =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Carousel, _React$Component);

  function Carousel(props) {
    var _this;

    _classCallCheck(this, Carousel);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Carousel).call(this, props));

    _this.saveSlick = function (node) {
      _this.slick = node;
    };

    _this.onWindowResized = function () {
      // Fix https://github.com/ant-design/ant-design/issues/2550
      var autoplay = _this.props.autoplay;

      if (autoplay && _this.slick && _this.slick.innerSlider && _this.slick.innerSlider.autoPlay) {
        _this.slick.innerSlider.autoPlay();
      }
    };

    _this.renderCarousel = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;

      var props = _extends({}, _this.props);

      if (props.effect === 'fade') {
        props.fade = true;
      }

      var className = getPrefixCls('carousel', props.prefixCls);
      var dotsClass = 'slick-dots';

      var dotPosition = _this.getDotPosition();

      props.vertical = dotPosition === 'left' || dotPosition === 'right';
      props.dotsClass = "".concat(dotsClass, " ").concat(dotsClass, "-").concat(dotPosition || 'bottom');

      if (props.vertical) {
        className = "".concat(className, " ").concat(className, "-vertical");
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: className
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](SlickCarousel, _extends({
        ref: _this.saveSlick
      }, props)));
    };

    _this.onWindowResized = lodash_debounce__WEBPACK_IMPORTED_MODULE_1___default()(_this.onWindowResized, 500, {
      leading: false
    });

    if ('vertical' in _this.props) {
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_3__["default"])(!_this.props.vertical, 'Carousel', '`vertical` is deprecated, please use `dotPosition` instead.');
    }

    return _this;
  }

  _createClass(Carousel, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var autoplay = this.props.autoplay;

      if (autoplay) {
        window.addEventListener('resize', this.onWindowResized);
      } // https://github.com/ant-design/ant-design/issues/7191


      this.innerSlider = this.slick && this.slick.innerSlider;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (react__WEBPACK_IMPORTED_MODULE_0__["Children"].count(this.props.children) !== react__WEBPACK_IMPORTED_MODULE_0__["Children"].count(prevProps.children)) {
        this.goTo(this.props.initialSlide || 0, false);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var autoplay = this.props.autoplay;

      if (autoplay) {
        window.removeEventListener('resize', this.onWindowResized);
        this.onWindowResized.cancel();
      }
    }
  }, {
    key: "getDotPosition",
    value: function getDotPosition() {
      if (this.props.dotPosition) {
        return this.props.dotPosition;
      }

      if ('vertical' in this.props) {
        return this.props.vertical ? 'right' : 'bottom';
      }

      return 'bottom';
    }
  }, {
    key: "next",
    value: function next() {
      this.slick.slickNext();
    }
  }, {
    key: "prev",
    value: function prev() {
      this.slick.slickPrev();
    }
  }, {
    key: "goTo",
    value: function goTo(slide) {
      var dontAnimate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      this.slick.slickGoTo(slide, dontAnimate);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, this.renderCarousel);
    }
  }]);

  return Carousel;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Carousel.defaultProps = {
  dots: true,
  arrows: false,
  draggable: false
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jYXJvdXNlbC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvY2Fyb3VzZWwvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBkZWJvdW5jZSBmcm9tICdsb2Rhc2gvZGVib3VuY2UnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG4vLyBtYXRjaE1lZGlhIHBvbHlmaWxsIGZvclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL1dpY2t5TmlsbGlhbXMvZW5xdWlyZS5qcy9pc3N1ZXMvODJcbi8vIFRPRE86IFdpbGwgYmUgcmVtb3ZlZCBpbiBhbnRkIDQuMCBiZWNhdXNlIHdlIHdpbGwgbm8gbG9uZ2VyIHN1cHBvcnQgaWU5XG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBjb25zdCBtYXRjaE1lZGlhUG9seWZpbGwgPSAobWVkaWFRdWVyeSkgPT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbWVkaWE6IG1lZGlhUXVlcnksXG4gICAgICAgICAgICBtYXRjaGVzOiBmYWxzZSxcbiAgICAgICAgICAgIGFkZExpc3RlbmVyKCkgeyB9LFxuICAgICAgICAgICAgcmVtb3ZlTGlzdGVuZXIoKSB7IH0sXG4gICAgICAgIH07XG4gICAgfTtcbiAgICAvLyByZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE4Nzc0XG4gICAgaWYgKCF3aW5kb3cubWF0Y2hNZWRpYSlcbiAgICAgICAgd2luZG93Lm1hdGNoTWVkaWEgPSBtYXRjaE1lZGlhUG9seWZpbGw7XG59XG4vLyBVc2UgcmVxdWlyZSBvdmVyIGltcG9ydCAod2lsbCBiZSBsaWZ0ZWQgdXApXG4vLyBtYWtlIHN1cmUgbWF0Y2hNZWRpYSBwb2x5ZmlsbCBydW4gYmVmb3JlIHJlcXVpcmUoJ3JlYWN0LXNsaWNrJylcbi8vIEZpeCBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy82NTYwXG4vLyBGaXggaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMzMwOFxuY29uc3QgU2xpY2tDYXJvdXNlbCA9IHJlcXVpcmUoJ3JlYWN0LXNsaWNrJykuZGVmYXVsdDtcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhcm91c2VsIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc2F2ZVNsaWNrID0gKG5vZGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2xpY2sgPSBub2RlO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uV2luZG93UmVzaXplZCA9ICgpID0+IHtcbiAgICAgICAgICAgIC8vIEZpeCBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8yNTUwXG4gICAgICAgICAgICBjb25zdCB7IGF1dG9wbGF5IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGF1dG9wbGF5ICYmIHRoaXMuc2xpY2sgJiYgdGhpcy5zbGljay5pbm5lclNsaWRlciAmJiB0aGlzLnNsaWNrLmlubmVyU2xpZGVyLmF1dG9QbGF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zbGljay5pbm5lclNsaWRlci5hdXRvUGxheSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckNhcm91c2VsID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHByb3BzID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5wcm9wcyk7XG4gICAgICAgICAgICBpZiAocHJvcHMuZWZmZWN0ID09PSAnZmFkZScpIHtcbiAgICAgICAgICAgICAgICBwcm9wcy5mYWRlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBjbGFzc05hbWUgPSBnZXRQcmVmaXhDbHMoJ2Nhcm91c2VsJywgcHJvcHMucHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGRvdHNDbGFzcyA9ICdzbGljay1kb3RzJztcbiAgICAgICAgICAgIGNvbnN0IGRvdFBvc2l0aW9uID0gdGhpcy5nZXREb3RQb3NpdGlvbigpO1xuICAgICAgICAgICAgcHJvcHMudmVydGljYWwgPSBkb3RQb3NpdGlvbiA9PT0gJ2xlZnQnIHx8IGRvdFBvc2l0aW9uID09PSAncmlnaHQnO1xuICAgICAgICAgICAgcHJvcHMuZG90c0NsYXNzID0gYCR7ZG90c0NsYXNzfSAke2RvdHNDbGFzc30tJHtkb3RQb3NpdGlvbiB8fCAnYm90dG9tJ31gO1xuICAgICAgICAgICAgaWYgKHByb3BzLnZlcnRpY2FsKSB7XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lID0gYCR7Y2xhc3NOYW1lfSAke2NsYXNzTmFtZX0tdmVydGljYWxgO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICg8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lfT5cbiAgICAgICAgPFNsaWNrQ2Fyb3VzZWwgcmVmPXt0aGlzLnNhdmVTbGlja30gey4uLnByb3BzfS8+XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uV2luZG93UmVzaXplZCA9IGRlYm91bmNlKHRoaXMub25XaW5kb3dSZXNpemVkLCA1MDAsIHtcbiAgICAgICAgICAgIGxlYWRpbmc6IGZhbHNlLFxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCd2ZXJ0aWNhbCcgaW4gdGhpcy5wcm9wcykge1xuICAgICAgICAgICAgd2FybmluZyghdGhpcy5wcm9wcy52ZXJ0aWNhbCwgJ0Nhcm91c2VsJywgJ2B2ZXJ0aWNhbGAgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBgZG90UG9zaXRpb25gIGluc3RlYWQuJyk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIGNvbnN0IHsgYXV0b3BsYXkgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChhdXRvcGxheSkge1xuICAgICAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIHRoaXMub25XaW5kb3dSZXNpemVkKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy83MTkxXG4gICAgICAgIHRoaXMuaW5uZXJTbGlkZXIgPSB0aGlzLnNsaWNrICYmIHRoaXMuc2xpY2suaW5uZXJTbGlkZXI7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgICAgaWYgKFJlYWN0LkNoaWxkcmVuLmNvdW50KHRoaXMucHJvcHMuY2hpbGRyZW4pICE9PSBSZWFjdC5DaGlsZHJlbi5jb3VudChwcmV2UHJvcHMuY2hpbGRyZW4pKSB7XG4gICAgICAgICAgICB0aGlzLmdvVG8odGhpcy5wcm9wcy5pbml0aWFsU2xpZGUgfHwgMCwgZmFsc2UpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICBjb25zdCB7IGF1dG9wbGF5IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAoYXV0b3BsYXkpIHtcbiAgICAgICAgICAgIHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aGlzLm9uV2luZG93UmVzaXplZCk7XG4gICAgICAgICAgICB0aGlzLm9uV2luZG93UmVzaXplZC5jYW5jZWwoKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBnZXREb3RQb3NpdGlvbigpIHtcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuZG90UG9zaXRpb24pIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BzLmRvdFBvc2l0aW9uO1xuICAgICAgICB9XG4gICAgICAgIGlmICgndmVydGljYWwnIGluIHRoaXMucHJvcHMpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BzLnZlcnRpY2FsID8gJ3JpZ2h0JyA6ICdib3R0b20nO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnYm90dG9tJztcbiAgICB9XG4gICAgbmV4dCgpIHtcbiAgICAgICAgdGhpcy5zbGljay5zbGlja05leHQoKTtcbiAgICB9XG4gICAgcHJldigpIHtcbiAgICAgICAgdGhpcy5zbGljay5zbGlja1ByZXYoKTtcbiAgICB9XG4gICAgZ29UbyhzbGlkZSwgZG9udEFuaW1hdGUgPSBmYWxzZSkge1xuICAgICAgICB0aGlzLnNsaWNrLnNsaWNrR29UbyhzbGlkZSwgZG9udEFuaW1hdGUpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQ2Fyb3VzZWx9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQ2Fyb3VzZWwuZGVmYXVsdFByb3BzID0ge1xuICAgIGRvdHM6IHRydWUsXG4gICAgYXJyb3dzOiBmYWxzZSxcbiAgICBkcmFnZ2FibGU6IGZhbHNlLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUZBO0FBQ0E7QUFDQTtBQVFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBZEE7QUFDQTtBQWdCQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBbkNBO0FBbUNBO0FBQ0E7OztBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBQ0E7QUFJQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7OztBQTdFQTtBQUNBO0FBREE7QUErRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/carousel/index.js
