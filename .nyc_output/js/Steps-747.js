__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_5__);







var calcPoints = function calcPoints(vertical, marks, dots, step, min, max) {
  warning__WEBPACK_IMPORTED_MODULE_5___default()(dots ? step > 0 : true, '`Slider[step]` should be a positive number in order to make Slider[dots] work.');
  var points = Object.keys(marks).map(parseFloat).sort(function (a, b) {
    return a - b;
  });

  if (dots && step) {
    for (var i = min; i <= max; i += step) {
      if (points.indexOf(i) === -1) {
        points.push(i);
      }
    }
  }

  return points;
};

var Steps = function Steps(_ref) {
  var prefixCls = _ref.prefixCls,
      vertical = _ref.vertical,
      reverse = _ref.reverse,
      marks = _ref.marks,
      dots = _ref.dots,
      step = _ref.step,
      included = _ref.included,
      lowerBound = _ref.lowerBound,
      upperBound = _ref.upperBound,
      max = _ref.max,
      min = _ref.min,
      dotStyle = _ref.dotStyle,
      activeDotStyle = _ref.activeDotStyle;
  var range = max - min;
  var elements = calcPoints(vertical, marks, dots, step, min, max).map(function (point) {
    var _classNames;

    var offset = Math.abs(point - min) / range * 100 + '%';
    var isActived = !included && point === upperBound || included && point <= upperBound && point >= lowerBound;
    var style = vertical ? babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, dotStyle, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, reverse ? 'top' : 'bottom', offset)) : babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, dotStyle, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, reverse ? 'right' : 'left', offset));

    if (isActived) {
      style = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, style, activeDotStyle);
    }

    var pointClassName = classnames__WEBPACK_IMPORTED_MODULE_4___default()((_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-dot', true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-dot-active', isActived), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-dot-reverse', reverse), _classNames));
    return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('span', {
      className: pointClassName,
      style: style,
      key: point
    });
  });
  return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('div', {
    className: prefixCls + '-step'
  }, elements);
};

Steps.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string,
  activeDotStyle: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  dotStyle: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  min: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  max: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  upperBound: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  lowerBound: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  included: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  dots: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  step: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.number,
  marks: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  vertical: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  reverse: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool
};
/* harmony default export */ __webpack_exports__["default"] = (Steps);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL2NvbW1vbi9TdGVwcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNsaWRlci9lcy9jb21tb24vU3RlcHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9kZWZpbmVQcm9wZXJ0eSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknO1xuaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgd2FybmluZyBmcm9tICd3YXJuaW5nJztcblxudmFyIGNhbGNQb2ludHMgPSBmdW5jdGlvbiBjYWxjUG9pbnRzKHZlcnRpY2FsLCBtYXJrcywgZG90cywgc3RlcCwgbWluLCBtYXgpIHtcbiAgd2FybmluZyhkb3RzID8gc3RlcCA+IDAgOiB0cnVlLCAnYFNsaWRlcltzdGVwXWAgc2hvdWxkIGJlIGEgcG9zaXRpdmUgbnVtYmVyIGluIG9yZGVyIHRvIG1ha2UgU2xpZGVyW2RvdHNdIHdvcmsuJyk7XG4gIHZhciBwb2ludHMgPSBPYmplY3Qua2V5cyhtYXJrcykubWFwKHBhcnNlRmxvYXQpLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gYSAtIGI7XG4gIH0pO1xuICBpZiAoZG90cyAmJiBzdGVwKSB7XG4gICAgZm9yICh2YXIgaSA9IG1pbjsgaSA8PSBtYXg7IGkgKz0gc3RlcCkge1xuICAgICAgaWYgKHBvaW50cy5pbmRleE9mKGkpID09PSAtMSkge1xuICAgICAgICBwb2ludHMucHVzaChpKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIHBvaW50cztcbn07XG5cbnZhciBTdGVwcyA9IGZ1bmN0aW9uIFN0ZXBzKF9yZWYpIHtcbiAgdmFyIHByZWZpeENscyA9IF9yZWYucHJlZml4Q2xzLFxuICAgICAgdmVydGljYWwgPSBfcmVmLnZlcnRpY2FsLFxuICAgICAgcmV2ZXJzZSA9IF9yZWYucmV2ZXJzZSxcbiAgICAgIG1hcmtzID0gX3JlZi5tYXJrcyxcbiAgICAgIGRvdHMgPSBfcmVmLmRvdHMsXG4gICAgICBzdGVwID0gX3JlZi5zdGVwLFxuICAgICAgaW5jbHVkZWQgPSBfcmVmLmluY2x1ZGVkLFxuICAgICAgbG93ZXJCb3VuZCA9IF9yZWYubG93ZXJCb3VuZCxcbiAgICAgIHVwcGVyQm91bmQgPSBfcmVmLnVwcGVyQm91bmQsXG4gICAgICBtYXggPSBfcmVmLm1heCxcbiAgICAgIG1pbiA9IF9yZWYubWluLFxuICAgICAgZG90U3R5bGUgPSBfcmVmLmRvdFN0eWxlLFxuICAgICAgYWN0aXZlRG90U3R5bGUgPSBfcmVmLmFjdGl2ZURvdFN0eWxlO1xuXG4gIHZhciByYW5nZSA9IG1heCAtIG1pbjtcbiAgdmFyIGVsZW1lbnRzID0gY2FsY1BvaW50cyh2ZXJ0aWNhbCwgbWFya3MsIGRvdHMsIHN0ZXAsIG1pbiwgbWF4KS5tYXAoZnVuY3Rpb24gKHBvaW50KSB7XG4gICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgdmFyIG9mZnNldCA9IE1hdGguYWJzKHBvaW50IC0gbWluKSAvIHJhbmdlICogMTAwICsgJyUnO1xuXG4gICAgdmFyIGlzQWN0aXZlZCA9ICFpbmNsdWRlZCAmJiBwb2ludCA9PT0gdXBwZXJCb3VuZCB8fCBpbmNsdWRlZCAmJiBwb2ludCA8PSB1cHBlckJvdW5kICYmIHBvaW50ID49IGxvd2VyQm91bmQ7XG4gICAgdmFyIHN0eWxlID0gdmVydGljYWwgPyBfZXh0ZW5kcyh7fSwgZG90U3R5bGUsIF9kZWZpbmVQcm9wZXJ0eSh7fSwgcmV2ZXJzZSA/ICd0b3AnIDogJ2JvdHRvbScsIG9mZnNldCkpIDogX2V4dGVuZHMoe30sIGRvdFN0eWxlLCBfZGVmaW5lUHJvcGVydHkoe30sIHJldmVyc2UgPyAncmlnaHQnIDogJ2xlZnQnLCBvZmZzZXQpKTtcbiAgICBpZiAoaXNBY3RpdmVkKSB7XG4gICAgICBzdHlsZSA9IF9leHRlbmRzKHt9LCBzdHlsZSwgYWN0aXZlRG90U3R5bGUpO1xuICAgIH1cblxuICAgIHZhciBwb2ludENsYXNzTmFtZSA9IGNsYXNzTmFtZXMoKF9jbGFzc05hbWVzID0ge30sIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgcHJlZml4Q2xzICsgJy1kb3QnLCB0cnVlKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBwcmVmaXhDbHMgKyAnLWRvdC1hY3RpdmUnLCBpc0FjdGl2ZWQpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIHByZWZpeENscyArICctZG90LXJldmVyc2UnLCByZXZlcnNlKSwgX2NsYXNzTmFtZXMpKTtcblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KCdzcGFuJywgeyBjbGFzc05hbWU6IHBvaW50Q2xhc3NOYW1lLCBzdHlsZTogc3R5bGUsIGtleTogcG9pbnQgfSk7XG4gIH0pO1xuXG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICdkaXYnLFxuICAgIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXN0ZXAnIH0sXG4gICAgZWxlbWVudHNcbiAgKTtcbn07XG5cblN0ZXBzLnByb3BUeXBlcyA9IHtcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBhY3RpdmVEb3RTdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgZG90U3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gIG1pbjogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4OiBQcm9wVHlwZXMubnVtYmVyLFxuICB1cHBlckJvdW5kOiBQcm9wVHlwZXMubnVtYmVyLFxuICBsb3dlckJvdW5kOiBQcm9wVHlwZXMubnVtYmVyLFxuICBpbmNsdWRlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGRvdHM6IFByb3BUeXBlcy5ib29sLFxuICBzdGVwOiBQcm9wVHlwZXMubnVtYmVyLFxuICBtYXJrczogUHJvcFR5cGVzLm9iamVjdCxcbiAgdmVydGljYWw6IFByb3BUeXBlcy5ib29sLFxuICByZXZlcnNlOiBQcm9wVHlwZXMuYm9vbFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU3RlcHM7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFnQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/common/Steps.js
