

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _arrowLeft = __webpack_require__(/*! ../icons/arrowLeft */ "./node_modules/react-images/lib/icons/arrowLeft.js");

var _arrowLeft2 = _interopRequireDefault(_arrowLeft);

var _arrowRight = __webpack_require__(/*! ../icons/arrowRight */ "./node_modules/react-images/lib/icons/arrowRight.js");

var _arrowRight2 = _interopRequireDefault(_arrowRight);

var _close = __webpack_require__(/*! ../icons/close */ "./node_modules/react-images/lib/icons/close.js");

var _close2 = _interopRequireDefault(_close);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

var icons = {
  arrowLeft: _arrowLeft2.default,
  arrowRight: _arrowRight2.default,
  close: _close2.default
};

var Icon = function Icon(_ref) {
  var fill = _ref.fill,
      type = _ref.type,
      props = _objectWithoutProperties(_ref, ['fill', 'type']);

  var icon = icons[type];
  return _react2.default.createElement('span', _extends({
    dangerouslySetInnerHTML: {
      __html: icon(fill)
    }
  }, props));
};

Icon.propTypes = {
  fill: _propTypes2.default.string,
  type: _propTypes2.default.oneOf(Object.keys(icons))
};
Icon.defaultProps = {
  fill: 'white'
};
exports.default = Icon;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0ljb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1pbWFnZXMvbGliL2NvbXBvbmVudHMvSWNvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfYXJyb3dMZWZ0ID0gcmVxdWlyZSgnLi4vaWNvbnMvYXJyb3dMZWZ0Jyk7XG5cbnZhciBfYXJyb3dMZWZ0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Fycm93TGVmdCk7XG5cbnZhciBfYXJyb3dSaWdodCA9IHJlcXVpcmUoJy4uL2ljb25zL2Fycm93UmlnaHQnKTtcblxudmFyIF9hcnJvd1JpZ2h0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Fycm93UmlnaHQpO1xuXG52YXIgX2Nsb3NlID0gcmVxdWlyZSgnLi4vaWNvbnMvY2xvc2UnKTtcblxudmFyIF9jbG9zZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbG9zZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxudmFyIGljb25zID0geyBhcnJvd0xlZnQ6IF9hcnJvd0xlZnQyLmRlZmF1bHQsIGFycm93UmlnaHQ6IF9hcnJvd1JpZ2h0Mi5kZWZhdWx0LCBjbG9zZTogX2Nsb3NlMi5kZWZhdWx0IH07XG5cbnZhciBJY29uID0gZnVuY3Rpb24gSWNvbihfcmVmKSB7XG5cdHZhciBmaWxsID0gX3JlZi5maWxsLFxuXHQgICAgdHlwZSA9IF9yZWYudHlwZSxcblx0ICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9yZWYsIFsnZmlsbCcsICd0eXBlJ10pO1xuXG5cdHZhciBpY29uID0gaWNvbnNbdHlwZV07XG5cblx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdzcGFuJywgX2V4dGVuZHMoe1xuXHRcdGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MOiB7IF9faHRtbDogaWNvbihmaWxsKSB9XG5cdH0sIHByb3BzKSk7XG59O1xuXG5JY29uLnByb3BUeXBlcyA9IHtcblx0ZmlsbDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG5cdHR5cGU6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2YoT2JqZWN0LmtleXMoaWNvbnMpKVxufTtcbkljb24uZGVmYXVsdFByb3BzID0ge1xuXHRmaWxsOiAnd2hpdGUnXG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJY29uOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Icon.js
