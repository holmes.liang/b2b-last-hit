__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_language__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @common/language */ "./src/common/language.tsx");
/* harmony import */ var _common_consts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/consts */ "./src/common/consts.tsx");



var UW_LEVELS = function UW_LEVELS() {
  return [{
    value: "1",
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Level 1").thai("Level 1").getMessage()
  }, {
    value: "2",
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Level 2").thai("Level 2").getMessage()
  }, {
    value: "3",
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Level 3").thai("Level 3").getMessage()
  }, {
    value: "4",
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Level 4").thai("Level 4").getMessage()
  }, {
    value: "5",
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Level 5").thai("Level 5").getMessage()
  }];
};

var uwTags = function uwTags() {
  return [{
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Premium Checking").thai("ตรวจสอบราคา").getMessage(),
    value: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].UW_PRICE_CHECKING
  }, {
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Manual Issue").thai("ตีพิมพ์ด้วยมือ").getMessage(),
    value: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].MANUAL_ISSUING
  }, {
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Non-financial Endorsement").thai("การเปลี่ยนแปลงนโยบายที่ไม่ใช่ทางการเงิน").getMessage(),
    value: "NFN_ENDO"
  }];
};

var uwTypes = function uwTypes() {
  return [{
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Premium Checking").thai("ตรวจสอบราคา").getMessage(),
    value: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].UW_TYPES.UW_TYPE_PREMIUM_ADJUSTABLE
  }, {
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Manual Issue").thai("ตีพิมพ์ด้วยมือ").getMessage(),
    value: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].UW_TYPES.UW_TYPE_PREMIUM_UNADJUSTABLE
  }, {
    label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Non-financial Endorsement").thai("การเปลี่ยนแปลงนโยบายที่ไม่ใช่ทางการเงิน").getMessage(),
    value: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].UW_TYPES.UW_TYPE_NFN_ENDO
  }];
};

var REASON_FOR_MANUAL;

(function (REASON_FOR_MANUAL) {
  REASON_FOR_MANUAL["EKYC_FAILED"] = "EKYC_FAILED";
  REASON_FOR_MANUAL["EKYC_SKIPPED"] = "EKYC_SKIPPED";
  REASON_FOR_MANUAL["EKYC_IN_PROGRESS"] = "EKYC_IN_PROGRESS";
})(REASON_FOR_MANUAL || (REASON_FOR_MANUAL = {}));

var reasonForManual = [{
  value: REASON_FOR_MANUAL.EKYC_FAILED,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("eKYC Failed").thai("eKYC Failed").getMessage()
}, {
  value: REASON_FOR_MANUAL.EKYC_SKIPPED,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("eKYC Skipped").thai("eKYC Skipped").getMessage()
}, {
  value: REASON_FOR_MANUAL.EKYC_IN_PROGRESS,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("eKYC In Progress").thai("eKYC In Progress").getMessage()
}];
var PROCESSED_BY;

(function (PROCESSED_BY) {
  PROCESSED_BY["BIOMETRICS"] = "BIOMETRICS";
  PROCESSED_BY["MANUALLY"] = "MANUALLY";
})(PROCESSED_BY || (PROCESSED_BY = {}));

var processedByArr = [{
  value: PROCESSED_BY.BIOMETRICS,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Biometrics").thai("Biometrics").getMessage()
}, {
  value: PROCESSED_BY.MANUALLY,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("BManually").thai("Manually").getMessage()
}];
var FINAL_RESULT;

(function (FINAL_RESULT) {
  FINAL_RESULT["PASS"] = "PASS";
  FINAL_RESULT["FAIL"] = "FAIL";
})(FINAL_RESULT || (FINAL_RESULT = {}));

var finalResultArr = [{
  value: FINAL_RESULT.PASS,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Pass").thai("Pass").getMessage()
}, {
  value: FINAL_RESULT.FAIL,
  label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Fail").thai("Fail").getMessage()
}];
/* harmony default export */ __webpack_exports__["default"] = ({
  BOOLEAN_YN: [{
    id: "Y",
    text: "Yes"
  }, {
    id: "N",
    text: "No"
  }],
  BIZ_TYPE: function BIZ_TYPE() {
    return [{
      value: ["NB", "COMP"],
      label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("New Business").thai("งานใหม่").getMessage()
    }, {
      value: ["RN"],
      label: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Renewal").thai("งานต่ออายุ").getMessage()
    }];
  },
  CODE_YN: function CODE_YN() {
    return [{
      value: "Y",
      label: "Yes"
    }, {
      value: "N",
      label: "No"
    }];
  },
  REASON_FOR_MANUAL: function REASON_FOR_MANUAL() {
    return reasonForManual;
  },
  PROCESSED_BY: function PROCESSED_BY() {
    return processedByArr;
  },
  FINAL_RESULT: function FINAL_RESULT() {
    return finalResultArr;
  },
  UNDERWRITING_TAGS: uwTags,
  UW_TYPES: uwTypes,
  UW_LEVELS: UW_LEVELS,
  UW_DECISIONS: function UW_DECISIONS() {
    return [{
      id: "APPROVED",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Approve").thai("อนุมัติ").getMessage()
    }, {
      id: "DECLINED",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Decline").thai("ปฏิเสธ").getMessage()
    }, {
      id: "SENT_BACK",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Send Back").thai("ส่งกลับ").getMessage()
    }];
  },
  ADJUSTMENT: function ADJUSTMENT() {
    return [{
      id: "NO_ADJ",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("No Adjustment").thai("No Adjustment").getMessage()
    }, {
      id: "ADJ_DISCOUNT",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Apply Discounts").thai("Apply Discounts").getMessage()
    }, {
      id: "ADJ_LOADING",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Apply Loadings").thai("Apply Loadings").getMessage()
    }, {
      id: "ADJ_GWP",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Overwrite GWP").thai("Overwrite GWP").getMessage()
    }];
  },
  GROUP_ENTRY_METHOD_OPTIONS: [{
    id: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].ENTRY_METHOD.ON_SCREEN,
    text: "On Screen"
  }, {
    id: _common_consts__WEBPACK_IMPORTED_MODULE_1__["default"].ENTRY_METHOD.UPLOAD,
    text: "Upload"
  }],
  GENDER_OPTIONS: function GENDER_OPTIONS() {
    return [{
      id: "M",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Male").thai("ชาย").getMessage()
    }, {
      id: "F",
      text: _common_language__WEBPACK_IMPORTED_MODULE_0__["default"].en("Female").thai("หญิง").getMessage()
    }];
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2NvZGVzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2NvbW1vbi9jb2Rlcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29kZUl0ZW0gfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgTGFuZ3VhZ2UgZnJvbSBcIkBjb21tb24vbGFuZ3VhZ2VcIjtcbmltcG9ydCBDb25zdHMgZnJvbSBcIkBjb21tb24vY29uc3RzXCI7XG5cbnR5cGUgQ09ERVMgPSB7XG4gIEJPT0xFQU5fWU46IENvZGVJdGVtW107XG4gIEJJWl9UWVBFOiAoKSA9PiBhbnlbXTtcbiAgVU5ERVJXUklUSU5HX1RBR1M6ICgpID0+IGFueVtdO1xuICBVV19ERUNJU0lPTlM6ICgpID0+IGFueVtdO1xuICBBREpVU1RNRU5UOiAoKSA9PiBhbnlbXTtcbiAgR1JPVVBfRU5UUllfTUVUSE9EX09QVElPTlM6IGFueVtdO1xuICBHRU5ERVJfT1BUSU9OUzogKCkgPT4gYW55W107XG4gIFVXX1RZUEVTOiAoKSA9PiBhbnlbXTtcbiAgVVdfTEVWRUxTOiAoKSA9PiBhbnlbXTtcbiAgUkVBU09OX0ZPUl9NQU5VQUw6ICgpID0+IGFueVtdO1xuICBQUk9DRVNTRURfQlk6ICgpID0+IGFueVtdO1xuICBGSU5BTF9SRVNVTFQ6ICgpID0+IGFueVtdO1xuICBDT0RFX1lOOiAoKSA9PiBhbnlbXTtcbn07XG5cbmNvbnN0IFVXX0xFVkVMUyA9ICgpID0+IFtcbiAge1xuICAgIHZhbHVlOiBcIjFcIixcbiAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJMZXZlbCAxXCIpXG4gICAgICAudGhhaShcIkxldmVsIDFcIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gIH0sXG4gIHtcbiAgICB2YWx1ZTogXCIyXCIsXG4gICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiTGV2ZWwgMlwiKVxuICAgICAgLnRoYWkoXCJMZXZlbCAyXCIpXG4gICAgICAuZ2V0TWVzc2FnZSgpLFxuICB9LFxuICB7XG4gICAgdmFsdWU6IFwiM1wiLFxuICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIkxldmVsIDNcIilcbiAgICAgIC50aGFpKFwiTGV2ZWwgM1wiKVxuICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgfSxcbiAge1xuICAgIHZhbHVlOiBcIjRcIixcbiAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJMZXZlbCA0XCIpXG4gICAgICAudGhhaShcIkxldmVsIDRcIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gIH0sXG4gIHtcbiAgICB2YWx1ZTogXCI1XCIsXG4gICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiTGV2ZWwgNVwiKVxuICAgICAgLnRoYWkoXCJMZXZlbCA1XCIpXG4gICAgICAuZ2V0TWVzc2FnZSgpLFxuICB9LFxuXTtcblxuY29uc3QgdXdUYWdzID0gKCkgPT4gW1xuICB7XG4gICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiUHJlbWl1bSBDaGVja2luZ1wiKVxuICAgICAgLnRoYWkoXCLguJXguKPguKfguIjguKrguK3guJrguKPguLLguITguLJcIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgdmFsdWU6IENvbnN0cy5VV19QUklDRV9DSEVDS0lORyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIk1hbnVhbCBJc3N1ZVwiKVxuICAgICAgLnRoYWkoXCLguJXguLXguJ7guLTguKHguJ7guYzguJTguYnguKfguKLguKHguLfguK1cIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgdmFsdWU6IENvbnN0cy5NQU5VQUxfSVNTVUlORyxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIk5vbi1maW5hbmNpYWwgRW5kb3JzZW1lbnRcIilcbiAgICAgIC50aGFpKFwi4LiB4Liy4Lij4LmA4Lib4Lil4Li14LmI4Lii4LiZ4LmB4Lib4Lil4LiH4LiZ4LmC4Lii4Lia4Liy4Lii4LiX4Li14LmI4LmE4Lih4LmI4LmD4LiK4LmI4LiX4Liy4LiH4LiB4Liy4Lij4LmA4LiH4Li04LiZXCIpXG4gICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgIHZhbHVlOiBcIk5GTl9FTkRPXCIsXG4gIH0sXG5dO1xuXG5jb25zdCB1d1R5cGVzID0gKCkgPT4gW1xuICB7XG4gICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiUHJlbWl1bSBDaGVja2luZ1wiKVxuICAgICAgLnRoYWkoXCLguJXguKPguKfguIjguKrguK3guJrguKPguLLguITguLJcIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgdmFsdWU6IENvbnN0cy5VV19UWVBFUy5VV19UWVBFX1BSRU1JVU1fQURKVVNUQUJMRSxcbiAgfSxcbiAge1xuICAgIGxhYmVsOiBMYW5ndWFnZS5lbihcIk1hbnVhbCBJc3N1ZVwiKVxuICAgICAgLnRoYWkoXCLguJXguLXguJ7guLTguKHguJ7guYzguJTguYnguKfguKLguKHguLfguK1cIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgdmFsdWU6IENvbnN0cy5VV19UWVBFUy5VV19UWVBFX1BSRU1JVU1fVU5BREpVU1RBQkxFLFxuICB9LFxuICB7XG4gICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiTm9uLWZpbmFuY2lhbCBFbmRvcnNlbWVudFwiKVxuICAgICAgLnRoYWkoXCLguIHguLLguKPguYDguJvguKXguLXguYjguKLguJnguYHguJvguKXguIfguJnguYLguKLguJrguLLguKLguJfguLXguYjguYTguKHguYjguYPguIrguYjguJfguLLguIfguIHguLLguKPguYDguIfguLTguJlcIilcbiAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgdmFsdWU6IENvbnN0cy5VV19UWVBFUy5VV19UWVBFX05GTl9FTkRPLFxuICB9LFxuXTtcblxuZW51bSBSRUFTT05fRk9SX01BTlVBTCB7XG4gIEVLWUNfRkFJTEVEID0gXCJFS1lDX0ZBSUxFRFwiLFxuICBFS1lDX1NLSVBQRUQgPSBcIkVLWUNfU0tJUFBFRFwiLFxuICBFS1lDX0lOX1BST0dSRVNTID0gXCJFS1lDX0lOX1BST0dSRVNTXCIsXG59XG5cbmNvbnN0IHJlYXNvbkZvck1hbnVhbDogYW55W10gPSBbe1xuICB2YWx1ZTogUkVBU09OX0ZPUl9NQU5VQUwuRUtZQ19GQUlMRUQsXG4gIGxhYmVsOiBMYW5ndWFnZS5lbihcImVLWUMgRmFpbGVkXCIpXG4gICAgLnRoYWkoXCJlS1lDIEZhaWxlZFwiKVxuICAgIC5nZXRNZXNzYWdlKCksXG59LCB7XG4gIHZhbHVlOiBSRUFTT05fRk9SX01BTlVBTC5FS1lDX1NLSVBQRUQsXG4gIGxhYmVsOiBMYW5ndWFnZS5lbihcImVLWUMgU2tpcHBlZFwiKVxuICAgIC50aGFpKFwiZUtZQyBTa2lwcGVkXCIpXG4gICAgLmdldE1lc3NhZ2UoKSxcbn0sIHtcbiAgdmFsdWU6IFJFQVNPTl9GT1JfTUFOVUFMLkVLWUNfSU5fUFJPR1JFU1MsXG4gIGxhYmVsOiBMYW5ndWFnZS5lbihcImVLWUMgSW4gUHJvZ3Jlc3NcIilcbiAgICAudGhhaShcImVLWUMgSW4gUHJvZ3Jlc3NcIilcbiAgICAuZ2V0TWVzc2FnZSgpLFxufV07XG5cbmVudW0gUFJPQ0VTU0VEX0JZIHtcbiAgQklPTUVUUklDUyA9IFwiQklPTUVUUklDU1wiLFxuICBNQU5VQUxMWSA9IFwiTUFOVUFMTFlcIixcbn1cblxuY29uc3QgcHJvY2Vzc2VkQnlBcnIgPSBbe1xuICB2YWx1ZTogUFJPQ0VTU0VEX0JZLkJJT01FVFJJQ1MsXG4gIGxhYmVsOiBMYW5ndWFnZS5lbihcIkJpb21ldHJpY3NcIilcbiAgICAudGhhaShcIkJpb21ldHJpY3NcIilcbiAgICAuZ2V0TWVzc2FnZSgpLFxufSwge1xuICB2YWx1ZTogUFJPQ0VTU0VEX0JZLk1BTlVBTExZLFxuICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJCTWFudWFsbHlcIilcbiAgICAudGhhaShcIk1hbnVhbGx5XCIpXG4gICAgLmdldE1lc3NhZ2UoKSxcbn1dO1xuXG5lbnVtIEZJTkFMX1JFU1VMVCB7XG4gIFBBU1MgPSBcIlBBU1NcIixcbiAgRkFJTCA9IFwiRkFJTFwiLFxufVxuXG5jb25zdCBmaW5hbFJlc3VsdEFyciA9IFt7XG4gIHZhbHVlOiBGSU5BTF9SRVNVTFQuUEFTUyxcbiAgbGFiZWw6IExhbmd1YWdlLmVuKFwiUGFzc1wiKVxuICAgIC50aGFpKFwiUGFzc1wiKVxuICAgIC5nZXRNZXNzYWdlKCksXG59LCB7XG4gIHZhbHVlOiBGSU5BTF9SRVNVTFQuRkFJTCxcbiAgbGFiZWw6IExhbmd1YWdlLmVuKFwiRmFpbFwiKVxuICAgIC50aGFpKFwiRmFpbFwiKVxuICAgIC5nZXRNZXNzYWdlKCksXG59XTtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBCT09MRUFOX1lOOiBbeyBpZDogXCJZXCIsIHRleHQ6IFwiWWVzXCIgfSwgeyBpZDogXCJOXCIsIHRleHQ6IFwiTm9cIiB9XSxcbiAgQklaX1RZUEU6ICgpID0+IFtcbiAgICB7XG4gICAgICB2YWx1ZTogW1wiTkJcIiwgXCJDT01QXCJdLFxuICAgICAgbGFiZWw6IExhbmd1YWdlLmVuKFwiTmV3IEJ1c2luZXNzXCIpXG4gICAgICAgIC50aGFpKFwi4LiH4Liy4LiZ4LmD4Lir4Lih4LmIXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgICB7XG4gICAgICB2YWx1ZTogW1wiUk5cIl0sXG4gICAgICBsYWJlbDogTGFuZ3VhZ2UuZW4oXCJSZW5ld2FsXCIpXG4gICAgICAgIC50aGFpKFwi4LiH4Liy4LiZ4LiV4LmI4Lit4Lit4Liy4Lii4Li4XCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgXSxcbiAgQ09ERV9ZTjogKCkgPT4gW3tcbiAgICB2YWx1ZTogXCJZXCIsIGxhYmVsOiBcIlllc1wiLFxuICB9LCB7XG4gICAgdmFsdWU6IFwiTlwiLCBsYWJlbDogXCJOb1wiLFxuICB9XSxcbiAgUkVBU09OX0ZPUl9NQU5VQUw6ICgpID0+IHJlYXNvbkZvck1hbnVhbCxcbiAgUFJPQ0VTU0VEX0JZOiAoKSA9PiBwcm9jZXNzZWRCeUFycixcbiAgRklOQUxfUkVTVUxUOiAoKSA9PiBmaW5hbFJlc3VsdEFycixcbiAgVU5ERVJXUklUSU5HX1RBR1M6IHV3VGFncyxcbiAgVVdfVFlQRVM6IHV3VHlwZXMsXG4gIFVXX0xFVkVMUyxcbiAgVVdfREVDSVNJT05TOiAoKSA9PiBbXG4gICAge1xuICAgICAgaWQ6IFwiQVBQUk9WRURcIixcbiAgICAgIHRleHQ6IExhbmd1YWdlLmVuKFwiQXBwcm92ZVwiKVxuICAgICAgICAudGhhaShcIuC4reC4meC4uOC4oeC4seC4leC4tFwiKVxuICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgIH0sXG4gICAge1xuICAgICAgaWQ6IFwiREVDTElORURcIixcbiAgICAgIHRleHQ6IExhbmd1YWdlLmVuKFwiRGVjbGluZVwiKVxuICAgICAgICAudGhhaShcIuC4m+C4j+C4tOC5gOC4quC4mFwiKVxuICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgIH0sXG4gICAge1xuICAgICAgaWQ6IFwiU0VOVF9CQUNLXCIsXG4gICAgICB0ZXh0OiBMYW5ndWFnZS5lbihcIlNlbmQgQmFja1wiKVxuICAgICAgICAudGhhaShcIuC4quC5iOC4h+C4geC4peC4seC4mlwiKVxuICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgIH0sXG4gIF0sXG4gIEFESlVTVE1FTlQ6ICgpID0+IFtcbiAgICB7XG4gICAgICBpZDogXCJOT19BREpcIixcbiAgICAgIHRleHQ6IExhbmd1YWdlLmVuKFwiTm8gQWRqdXN0bWVudFwiKVxuICAgICAgICAudGhhaShcIk5vIEFkanVzdG1lbnRcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIGlkOiBcIkFESl9ESVNDT1VOVFwiLFxuICAgICAgdGV4dDogTGFuZ3VhZ2UuZW4oXCJBcHBseSBEaXNjb3VudHNcIilcbiAgICAgICAgLnRoYWkoXCJBcHBseSBEaXNjb3VudHNcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIGlkOiBcIkFESl9MT0FESU5HXCIsXG4gICAgICB0ZXh0OiBMYW5ndWFnZS5lbihcIkFwcGx5IExvYWRpbmdzXCIpXG4gICAgICAgIC50aGFpKFwiQXBwbHkgTG9hZGluZ3NcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIGlkOiBcIkFESl9HV1BcIixcbiAgICAgIHRleHQ6IExhbmd1YWdlLmVuKFwiT3ZlcndyaXRlIEdXUFwiKVxuICAgICAgICAudGhhaShcIk92ZXJ3cml0ZSBHV1BcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICBdLFxuICBHUk9VUF9FTlRSWV9NRVRIT0RfT1BUSU9OUzogW1xuICAgIHsgaWQ6IENvbnN0cy5FTlRSWV9NRVRIT0QuT05fU0NSRUVOLCB0ZXh0OiBcIk9uIFNjcmVlblwiIH0sXG4gICAgeyBpZDogQ29uc3RzLkVOVFJZX01FVEhPRC5VUExPQUQsIHRleHQ6IFwiVXBsb2FkXCIgfSxcbiAgXSxcbiAgR0VOREVSX09QVElPTlM6ICgpID0+IFtcbiAgICB7XG4gICAgICBpZDogXCJNXCIsXG4gICAgICB0ZXh0OiBMYW5ndWFnZS5lbihcIk1hbGVcIilcbiAgICAgICAgLnRoYWkoXCLguIrguLLguKJcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIGlkOiBcIkZcIixcbiAgICAgIHRleHQ6IExhbmd1YWdlLmVuKFwiRmVtYWxlXCIpXG4gICAgICAgIC50aGFpKFwi4Lir4LiN4Li04LiHXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgfSxcbiAgXSxcbn0gYXMgQ09ERVM7XG4iXSwibWFwcGluZ3MiOiJBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQWlCQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBekJBO0FBQ0E7QUFnQ0E7QUFBQTtBQUVBO0FBR0E7QUFKQTtBQU9BO0FBR0E7QUFKQTtBQU9BO0FBR0E7QUFKQTtBQWJBO0FBQ0E7QUFvQkE7QUFBQTtBQUVBO0FBR0E7QUFKQTtBQU9BO0FBR0E7QUFKQTtBQU9BO0FBR0E7QUFKQTtBQWJBO0FBQ0E7QUFvQkE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBTUE7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFGQTtBQU9BO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBRkE7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQU9BO0FBQ0E7QUFGQTtBQVBBO0FBY0E7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUdBO0FBQUE7QUFEQTtBQUZBO0FBS0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQU9BO0FBQ0E7QUFGQTtBQU9BO0FBQ0E7QUFGQTtBQWJBO0FBb0JBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFPQTtBQUNBO0FBRkE7QUFPQTtBQUNBO0FBRkE7QUFPQTtBQUNBO0FBRkE7QUFuQkE7QUEwQkE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBT0E7QUFDQTtBQUZBO0FBUEE7QUE3RUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/codes.tsx
