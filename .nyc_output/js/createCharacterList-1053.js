/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule createCharacterList
 * @format
 * 
 */


var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var List = Immutable.List;

function createCharacterList(inlineStyles, entities) {
  var characterArray = inlineStyles.map(function (style, ii) {
    var entity = entities[ii];
    return CharacterMetadata.create({
      style: style,
      entity: entity
    });
  });
  return List(characterArray);
}

module.exports = createCharacterList;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NyZWF0ZUNoYXJhY3Rlckxpc3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvY3JlYXRlQ2hhcmFjdGVyTGlzdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGNyZWF0ZUNoYXJhY3Rlckxpc3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0O1xuXG5cbmZ1bmN0aW9uIGNyZWF0ZUNoYXJhY3Rlckxpc3QoaW5saW5lU3R5bGVzLCBlbnRpdGllcykge1xuICB2YXIgY2hhcmFjdGVyQXJyYXkgPSBpbmxpbmVTdHlsZXMubWFwKGZ1bmN0aW9uIChzdHlsZSwgaWkpIHtcbiAgICB2YXIgZW50aXR5ID0gZW50aXRpZXNbaWldO1xuICAgIHJldHVybiBDaGFyYWN0ZXJNZXRhZGF0YS5jcmVhdGUoeyBzdHlsZTogc3R5bGUsIGVudGl0eTogZW50aXR5IH0pO1xuICB9KTtcbiAgcmV0dXJuIExpc3QoY2hhcmFjdGVyQXJyYXkpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGNyZWF0ZUNoYXJhY3Rlckxpc3Q7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/createCharacterList.js
