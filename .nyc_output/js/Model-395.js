/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var _model = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;

var _clazz = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");

var enableClassExtend = _clazz.enableClassExtend;
var enableClassCheck = _clazz.enableClassCheck;

var lineStyleMixin = __webpack_require__(/*! ./mixin/lineStyle */ "./node_modules/echarts/lib/model/mixin/lineStyle.js");

var areaStyleMixin = __webpack_require__(/*! ./mixin/areaStyle */ "./node_modules/echarts/lib/model/mixin/areaStyle.js");

var textStyleMixin = __webpack_require__(/*! ./mixin/textStyle */ "./node_modules/echarts/lib/model/mixin/textStyle.js");

var itemStyleMixin = __webpack_require__(/*! ./mixin/itemStyle */ "./node_modules/echarts/lib/model/mixin/itemStyle.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @module echarts/model/Model
 */


var mixin = zrUtil.mixin;
var inner = makeInner();
/**
 * @alias module:echarts/model/Model
 * @constructor
 * @param {Object} [option]
 * @param {module:echarts/model/Model} [parentModel]
 * @param {module:echarts/model/Global} [ecModel]
 */

function Model(option, parentModel, ecModel) {
  /**
   * @type {module:echarts/model/Model}
   * @readOnly
   */
  this.parentModel = parentModel;
  /**
   * @type {module:echarts/model/Global}
   * @readOnly
   */

  this.ecModel = ecModel;
  /**
   * @type {Object}
   * @protected
   */

  this.option = option; // Simple optimization
  // if (this.init) {
  //     if (arguments.length <= 4) {
  //         this.init(option, parentModel, ecModel, extraOpt);
  //     }
  //     else {
  //         this.init.apply(this, arguments);
  //     }
  // }
}

Model.prototype = {
  constructor: Model,

  /**
   * Model 的初始化函数
   * @param {Object} option
   */
  init: null,

  /**
   * 从新的 Option merge
   */
  mergeOption: function mergeOption(option) {
    zrUtil.merge(this.option, option, true);
  },

  /**
   * @param {string|Array.<string>} path
   * @param {boolean} [ignoreParent=false]
   * @return {*}
   */
  get: function get(path, ignoreParent) {
    if (path == null) {
      return this.option;
    }

    return doGet(this.option, this.parsePath(path), !ignoreParent && getParent(this, path));
  },

  /**
   * @param {string} key
   * @param {boolean} [ignoreParent=false]
   * @return {*}
   */
  getShallow: function getShallow(key, ignoreParent) {
    var option = this.option;
    var val = option == null ? option : option[key];
    var parentModel = !ignoreParent && getParent(this, key);

    if (val == null && parentModel) {
      val = parentModel.getShallow(key);
    }

    return val;
  },

  /**
   * @param {string|Array.<string>} [path]
   * @param {module:echarts/model/Model} [parentModel]
   * @return {module:echarts/model/Model}
   */
  getModel: function getModel(path, parentModel) {
    var obj = path == null ? this.option : doGet(this.option, path = this.parsePath(path));
    var thisParentModel;
    parentModel = parentModel || (thisParentModel = getParent(this, path)) && thisParentModel.getModel(path);
    return new Model(obj, parentModel, this.ecModel);
  },

  /**
   * If model has option
   */
  isEmpty: function isEmpty() {
    return this.option == null;
  },
  restoreData: function restoreData() {},
  // Pending
  clone: function clone() {
    var Ctor = this.constructor;
    return new Ctor(zrUtil.clone(this.option));
  },
  setReadOnly: function setReadOnly(properties) {// clazzUtil.setReadOnly(this, properties);
  },
  // If path is null/undefined, return null/undefined.
  parsePath: function parsePath(path) {
    if (typeof path === 'string') {
      path = path.split('.');
    }

    return path;
  },

  /**
   * @param {Function} getParentMethod
   *        param {Array.<string>|string} path
   *        return {module:echarts/model/Model}
   */
  customizeGetParent: function customizeGetParent(getParentMethod) {
    inner(this).getParent = getParentMethod;
  },
  isAnimationEnabled: function isAnimationEnabled() {
    if (!env.node) {
      if (this.option.animation != null) {
        return !!this.option.animation;
      } else if (this.parentModel) {
        return this.parentModel.isAnimationEnabled();
      }
    }
  }
};

function doGet(obj, pathArr, parentModel) {
  for (var i = 0; i < pathArr.length; i++) {
    // Ignore empty
    if (!pathArr[i]) {
      continue;
    } // obj could be number/string/... (like 0)


    obj = obj && typeof obj === 'object' ? obj[pathArr[i]] : null;

    if (obj == null) {
      break;
    }
  }

  if (obj == null && parentModel) {
    obj = parentModel.get(pathArr);
  }

  return obj;
} // `path` can be null/undefined


function getParent(model, path) {
  var getParentMethod = inner(model).getParent;
  return getParentMethod ? getParentMethod.call(model, path) : model.parentModel;
} // Enable Model.extend.


enableClassExtend(Model);
enableClassCheck(Model);
mixin(Model, lineStyleMixin);
mixin(Model, areaStyleMixin);
mixin(Model, textStyleMixin);
mixin(Model, itemStyleMixin);
var _default = Model;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvTW9kZWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9tb2RlbC9Nb2RlbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBlbnYgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9lbnZcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIG1ha2VJbm5lciA9IF9tb2RlbC5tYWtlSW5uZXI7XG5cbnZhciBfY2xhenogPSByZXF1aXJlKFwiLi4vdXRpbC9jbGF6elwiKTtcblxudmFyIGVuYWJsZUNsYXNzRXh0ZW5kID0gX2NsYXp6LmVuYWJsZUNsYXNzRXh0ZW5kO1xudmFyIGVuYWJsZUNsYXNzQ2hlY2sgPSBfY2xhenouZW5hYmxlQ2xhc3NDaGVjaztcblxudmFyIGxpbmVTdHlsZU1peGluID0gcmVxdWlyZShcIi4vbWl4aW4vbGluZVN0eWxlXCIpO1xuXG52YXIgYXJlYVN0eWxlTWl4aW4gPSByZXF1aXJlKFwiLi9taXhpbi9hcmVhU3R5bGVcIik7XG5cbnZhciB0ZXh0U3R5bGVNaXhpbiA9IHJlcXVpcmUoXCIuL21peGluL3RleHRTdHlsZVwiKTtcblxudmFyIGl0ZW1TdHlsZU1peGluID0gcmVxdWlyZShcIi4vbWl4aW4vaXRlbVN0eWxlXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogQG1vZHVsZSBlY2hhcnRzL21vZGVsL01vZGVsXG4gKi9cbnZhciBtaXhpbiA9IHpyVXRpbC5taXhpbjtcbnZhciBpbm5lciA9IG1ha2VJbm5lcigpO1xuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWxcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25dXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBbcGFyZW50TW9kZWxdXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gW2VjTW9kZWxdXG4gKi9cblxuZnVuY3Rpb24gTW9kZWwob3B0aW9uLCBwYXJlbnRNb2RlbCwgZWNNb2RlbCkge1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfVxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIHRoaXMucGFyZW50TW9kZWwgPSBwYXJlbnRNb2RlbDtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9XG4gICAqIEByZWFkT25seVxuICAgKi9cblxuICB0aGlzLmVjTW9kZWwgPSBlY01vZGVsO1xuICAvKipcbiAgICogQHR5cGUge09iamVjdH1cbiAgICogQHByb3RlY3RlZFxuICAgKi9cblxuICB0aGlzLm9wdGlvbiA9IG9wdGlvbjsgLy8gU2ltcGxlIG9wdGltaXphdGlvblxuICAvLyBpZiAodGhpcy5pbml0KSB7XG4gIC8vICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA8PSA0KSB7XG4gIC8vICAgICAgICAgdGhpcy5pbml0KG9wdGlvbiwgcGFyZW50TW9kZWwsIGVjTW9kZWwsIGV4dHJhT3B0KTtcbiAgLy8gICAgIH1cbiAgLy8gICAgIGVsc2Uge1xuICAvLyAgICAgICAgIHRoaXMuaW5pdC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAvLyAgICAgfVxuICAvLyB9XG59XG5cbk1vZGVsLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IE1vZGVsLFxuXG4gIC8qKlxuICAgKiBNb2RlbCDnmoTliJ3lp4vljJblh73mlbBcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvblxuICAgKi9cbiAgaW5pdDogbnVsbCxcblxuICAvKipcbiAgICog5LuO5paw55qEIE9wdGlvbiBtZXJnZVxuICAgKi9cbiAgbWVyZ2VPcHRpb246IGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgICB6clV0aWwubWVyZ2UodGhpcy5vcHRpb24sIG9wdGlvbiwgdHJ1ZSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfEFycmF5LjxzdHJpbmc+fSBwYXRoXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW2lnbm9yZVBhcmVudD1mYWxzZV1cbiAgICogQHJldHVybiB7Kn1cbiAgICovXG4gIGdldDogZnVuY3Rpb24gKHBhdGgsIGlnbm9yZVBhcmVudCkge1xuICAgIGlmIChwYXRoID09IG51bGwpIHtcbiAgICAgIHJldHVybiB0aGlzLm9wdGlvbjtcbiAgICB9XG5cbiAgICByZXR1cm4gZG9HZXQodGhpcy5vcHRpb24sIHRoaXMucGFyc2VQYXRoKHBhdGgpLCAhaWdub3JlUGFyZW50ICYmIGdldFBhcmVudCh0aGlzLCBwYXRoKSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBrZXlcbiAgICogQHBhcmFtIHtib29sZWFufSBbaWdub3JlUGFyZW50PWZhbHNlXVxuICAgKiBAcmV0dXJuIHsqfVxuICAgKi9cbiAgZ2V0U2hhbGxvdzogZnVuY3Rpb24gKGtleSwgaWdub3JlUGFyZW50KSB7XG4gICAgdmFyIG9wdGlvbiA9IHRoaXMub3B0aW9uO1xuICAgIHZhciB2YWwgPSBvcHRpb24gPT0gbnVsbCA/IG9wdGlvbiA6IG9wdGlvbltrZXldO1xuICAgIHZhciBwYXJlbnRNb2RlbCA9ICFpZ25vcmVQYXJlbnQgJiYgZ2V0UGFyZW50KHRoaXMsIGtleSk7XG5cbiAgICBpZiAodmFsID09IG51bGwgJiYgcGFyZW50TW9kZWwpIHtcbiAgICAgIHZhbCA9IHBhcmVudE1vZGVsLmdldFNoYWxsb3coa2V5KTtcbiAgICB9XG5cbiAgICByZXR1cm4gdmFsO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ3xBcnJheS48c3RyaW5nPn0gW3BhdGhdXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IFtwYXJlbnRNb2RlbF1cbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9XG4gICAqL1xuICBnZXRNb2RlbDogZnVuY3Rpb24gKHBhdGgsIHBhcmVudE1vZGVsKSB7XG4gICAgdmFyIG9iaiA9IHBhdGggPT0gbnVsbCA/IHRoaXMub3B0aW9uIDogZG9HZXQodGhpcy5vcHRpb24sIHBhdGggPSB0aGlzLnBhcnNlUGF0aChwYXRoKSk7XG4gICAgdmFyIHRoaXNQYXJlbnRNb2RlbDtcbiAgICBwYXJlbnRNb2RlbCA9IHBhcmVudE1vZGVsIHx8ICh0aGlzUGFyZW50TW9kZWwgPSBnZXRQYXJlbnQodGhpcywgcGF0aCkpICYmIHRoaXNQYXJlbnRNb2RlbC5nZXRNb2RlbChwYXRoKTtcbiAgICByZXR1cm4gbmV3IE1vZGVsKG9iaiwgcGFyZW50TW9kZWwsIHRoaXMuZWNNb2RlbCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIElmIG1vZGVsIGhhcyBvcHRpb25cbiAgICovXG4gIGlzRW1wdHk6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5vcHRpb24gPT0gbnVsbDtcbiAgfSxcbiAgcmVzdG9yZURhdGE6IGZ1bmN0aW9uICgpIHt9LFxuICAvLyBQZW5kaW5nXG4gIGNsb25lOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIEN0b3IgPSB0aGlzLmNvbnN0cnVjdG9yO1xuICAgIHJldHVybiBuZXcgQ3Rvcih6clV0aWwuY2xvbmUodGhpcy5vcHRpb24pKTtcbiAgfSxcbiAgc2V0UmVhZE9ubHk6IGZ1bmN0aW9uIChwcm9wZXJ0aWVzKSB7Ly8gY2xhenpVdGlsLnNldFJlYWRPbmx5KHRoaXMsIHByb3BlcnRpZXMpO1xuICB9LFxuICAvLyBJZiBwYXRoIGlzIG51bGwvdW5kZWZpbmVkLCByZXR1cm4gbnVsbC91bmRlZmluZWQuXG4gIHBhcnNlUGF0aDogZnVuY3Rpb24gKHBhdGgpIHtcbiAgICBpZiAodHlwZW9mIHBhdGggPT09ICdzdHJpbmcnKSB7XG4gICAgICBwYXRoID0gcGF0aC5zcGxpdCgnLicpO1xuICAgIH1cblxuICAgIHJldHVybiBwYXRoO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBnZXRQYXJlbnRNZXRob2RcbiAgICogICAgICAgIHBhcmFtIHtBcnJheS48c3RyaW5nPnxzdHJpbmd9IHBhdGhcbiAgICogICAgICAgIHJldHVybiB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9XG4gICAqL1xuICBjdXN0b21pemVHZXRQYXJlbnQ6IGZ1bmN0aW9uIChnZXRQYXJlbnRNZXRob2QpIHtcbiAgICBpbm5lcih0aGlzKS5nZXRQYXJlbnQgPSBnZXRQYXJlbnRNZXRob2Q7XG4gIH0sXG4gIGlzQW5pbWF0aW9uRW5hYmxlZDogZnVuY3Rpb24gKCkge1xuICAgIGlmICghZW52Lm5vZGUpIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbi5hbmltYXRpb24gIT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gISF0aGlzLm9wdGlvbi5hbmltYXRpb247XG4gICAgICB9IGVsc2UgaWYgKHRoaXMucGFyZW50TW9kZWwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGFyZW50TW9kZWwuaXNBbmltYXRpb25FbmFibGVkKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG59O1xuXG5mdW5jdGlvbiBkb0dldChvYmosIHBhdGhBcnIsIHBhcmVudE1vZGVsKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aEFyci5sZW5ndGg7IGkrKykge1xuICAgIC8vIElnbm9yZSBlbXB0eVxuICAgIGlmICghcGF0aEFycltpXSkge1xuICAgICAgY29udGludWU7XG4gICAgfSAvLyBvYmogY291bGQgYmUgbnVtYmVyL3N0cmluZy8uLi4gKGxpa2UgMClcblxuXG4gICAgb2JqID0gb2JqICYmIHR5cGVvZiBvYmogPT09ICdvYmplY3QnID8gb2JqW3BhdGhBcnJbaV1dIDogbnVsbDtcblxuICAgIGlmIChvYmogPT0gbnVsbCkge1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgaWYgKG9iaiA9PSBudWxsICYmIHBhcmVudE1vZGVsKSB7XG4gICAgb2JqID0gcGFyZW50TW9kZWwuZ2V0KHBhdGhBcnIpO1xuICB9XG5cbiAgcmV0dXJuIG9iajtcbn0gLy8gYHBhdGhgIGNhbiBiZSBudWxsL3VuZGVmaW5lZFxuXG5cbmZ1bmN0aW9uIGdldFBhcmVudChtb2RlbCwgcGF0aCkge1xuICB2YXIgZ2V0UGFyZW50TWV0aG9kID0gaW5uZXIobW9kZWwpLmdldFBhcmVudDtcbiAgcmV0dXJuIGdldFBhcmVudE1ldGhvZCA/IGdldFBhcmVudE1ldGhvZC5jYWxsKG1vZGVsLCBwYXRoKSA6IG1vZGVsLnBhcmVudE1vZGVsO1xufSAvLyBFbmFibGUgTW9kZWwuZXh0ZW5kLlxuXG5cbmVuYWJsZUNsYXNzRXh0ZW5kKE1vZGVsKTtcbmVuYWJsZUNsYXNzQ2hlY2soTW9kZWwpO1xubWl4aW4oTW9kZWwsIGxpbmVTdHlsZU1peGluKTtcbm1peGluKE1vZGVsLCBhcmVhU3R5bGVNaXhpbik7XG5taXhpbihNb2RlbCwgdGV4dFN0eWxlTWl4aW4pO1xubWl4aW4oTW9kZWwsIGl0ZW1TdHlsZU1peGluKTtcbnZhciBfZGVmYXVsdCA9IE1vZGVsO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBOzs7O0FBSUE7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpHQTtBQUNBO0FBbUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/Model.js
