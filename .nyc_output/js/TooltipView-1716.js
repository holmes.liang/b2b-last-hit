/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var TooltipContent = __webpack_require__(/*! ./TooltipContent */ "./node_modules/echarts/lib/component/tooltip/TooltipContent.js");

var TooltipRichContent = __webpack_require__(/*! ./TooltipRichContent */ "./node_modules/echarts/lib/component/tooltip/TooltipRichContent.js");

var formatUtil = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");

var numberUtil = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var findPointFromSeries = __webpack_require__(/*! ../axisPointer/findPointFromSeries */ "./node_modules/echarts/lib/component/axisPointer/findPointFromSeries.js");

var layoutUtil = __webpack_require__(/*! ../../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var Model = __webpack_require__(/*! ../../model/Model */ "./node_modules/echarts/lib/model/Model.js");

var globalListener = __webpack_require__(/*! ../axisPointer/globalListener */ "./node_modules/echarts/lib/component/axisPointer/globalListener.js");

var axisHelper = __webpack_require__(/*! ../../coord/axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var axisPointerViewHelper = __webpack_require__(/*! ../axisPointer/viewHelper */ "./node_modules/echarts/lib/component/axisPointer/viewHelper.js");

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var getTooltipRenderMode = _model.getTooltipRenderMode;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var bind = zrUtil.bind;
var each = zrUtil.each;
var parsePercent = numberUtil.parsePercent;
var proxyRect = new graphic.Rect({
  shape: {
    x: -1,
    y: -1,
    width: 2,
    height: 2
  }
});

var _default = echarts.extendComponentView({
  type: 'tooltip',
  init: function init(ecModel, api) {
    if (env.node) {
      return;
    }

    var tooltipModel = ecModel.getComponent('tooltip');
    var renderMode = tooltipModel.get('renderMode');
    this._renderMode = getTooltipRenderMode(renderMode);
    var tooltipContent;

    if (this._renderMode === 'html') {
      tooltipContent = new TooltipContent(api.getDom(), api);
      this._newLine = '<br/>';
    } else {
      tooltipContent = new TooltipRichContent(api);
      this._newLine = '\n';
    }

    this._tooltipContent = tooltipContent;
  },
  render: function render(tooltipModel, ecModel, api) {
    if (env.node) {
      return;
    } // Reset


    this.group.removeAll();
    /**
     * @private
     * @type {module:echarts/component/tooltip/TooltipModel}
     */

    this._tooltipModel = tooltipModel;
    /**
     * @private
     * @type {module:echarts/model/Global}
     */

    this._ecModel = ecModel;
    /**
     * @private
     * @type {module:echarts/ExtensionAPI}
     */

    this._api = api;
    /**
     * Should be cleaned when render.
     * @private
     * @type {Array.<Array.<Object>>}
     */

    this._lastDataByCoordSys = null;
    /**
     * @private
     * @type {boolean}
     */

    this._alwaysShowContent = tooltipModel.get('alwaysShowContent');
    var tooltipContent = this._tooltipContent;
    tooltipContent.update();
    tooltipContent.setEnterable(tooltipModel.get('enterable'));

    this._initGlobalListener();

    this._keepShow();
  },
  _initGlobalListener: function _initGlobalListener() {
    var tooltipModel = this._tooltipModel;
    var triggerOn = tooltipModel.get('triggerOn');
    globalListener.register('itemTooltip', this._api, bind(function (currTrigger, e, dispatchAction) {
      // If 'none', it is not controlled by mouse totally.
      if (triggerOn !== 'none') {
        if (triggerOn.indexOf(currTrigger) >= 0) {
          this._tryShow(e, dispatchAction);
        } else if (currTrigger === 'leave') {
          this._hide(dispatchAction);
        }
      }
    }, this));
  },
  _keepShow: function _keepShow() {
    var tooltipModel = this._tooltipModel;
    var ecModel = this._ecModel;
    var api = this._api; // Try to keep the tooltip show when refreshing

    if (this._lastX != null && this._lastY != null // When user is willing to control tooltip totally using API,
    // self.manuallyShowTip({x, y}) might cause tooltip hide,
    // which is not expected.
    && tooltipModel.get('triggerOn') !== 'none') {
      var self = this;
      clearTimeout(this._refreshUpdateTimeout);
      this._refreshUpdateTimeout = setTimeout(function () {
        // Show tip next tick after other charts are rendered
        // In case highlight action has wrong result
        // FIXME
        self.manuallyShowTip(tooltipModel, ecModel, api, {
          x: self._lastX,
          y: self._lastY
        });
      });
    }
  },

  /**
   * Show tip manually by
   * dispatchAction({
   *     type: 'showTip',
   *     x: 10,
   *     y: 10
   * });
   * Or
   * dispatchAction({
   *      type: 'showTip',
   *      seriesIndex: 0,
   *      dataIndex or dataIndexInside or name
   * });
   *
   *  TODO Batch
   */
  manuallyShowTip: function manuallyShowTip(tooltipModel, ecModel, api, payload) {
    if (payload.from === this.uid || env.node) {
      return;
    }

    var dispatchAction = makeDispatchAction(payload, api); // Reset ticket

    this._ticket = ''; // When triggered from axisPointer.

    var dataByCoordSys = payload.dataByCoordSys;

    if (payload.tooltip && payload.x != null && payload.y != null) {
      var el = proxyRect;
      el.position = [payload.x, payload.y];
      el.update();
      el.tooltip = payload.tooltip; // Manually show tooltip while view is not using zrender elements.

      this._tryShow({
        offsetX: payload.x,
        offsetY: payload.y,
        target: el
      }, dispatchAction);
    } else if (dataByCoordSys) {
      this._tryShow({
        offsetX: payload.x,
        offsetY: payload.y,
        position: payload.position,
        event: {},
        dataByCoordSys: payload.dataByCoordSys,
        tooltipOption: payload.tooltipOption
      }, dispatchAction);
    } else if (payload.seriesIndex != null) {
      if (this._manuallyAxisShowTip(tooltipModel, ecModel, api, payload)) {
        return;
      }

      var pointInfo = findPointFromSeries(payload, ecModel);
      var cx = pointInfo.point[0];
      var cy = pointInfo.point[1];

      if (cx != null && cy != null) {
        this._tryShow({
          offsetX: cx,
          offsetY: cy,
          position: payload.position,
          target: pointInfo.el,
          event: {}
        }, dispatchAction);
      }
    } else if (payload.x != null && payload.y != null) {
      // FIXME
      // should wrap dispatchAction like `axisPointer/globalListener` ?
      api.dispatchAction({
        type: 'updateAxisPointer',
        x: payload.x,
        y: payload.y
      });

      this._tryShow({
        offsetX: payload.x,
        offsetY: payload.y,
        position: payload.position,
        target: api.getZr().findHover(payload.x, payload.y).target,
        event: {}
      }, dispatchAction);
    }
  },
  manuallyHideTip: function manuallyHideTip(tooltipModel, ecModel, api, payload) {
    var tooltipContent = this._tooltipContent;

    if (!this._alwaysShowContent && this._tooltipModel) {
      tooltipContent.hideLater(this._tooltipModel.get('hideDelay'));
    }

    this._lastX = this._lastY = null;

    if (payload.from !== this.uid) {
      this._hide(makeDispatchAction(payload, api));
    }
  },
  // Be compatible with previous design, that is, when tooltip.type is 'axis' and
  // dispatchAction 'showTip' with seriesIndex and dataIndex will trigger axis pointer
  // and tooltip.
  _manuallyAxisShowTip: function _manuallyAxisShowTip(tooltipModel, ecModel, api, payload) {
    var seriesIndex = payload.seriesIndex;
    var dataIndex = payload.dataIndex;
    var coordSysAxesInfo = ecModel.getComponent('axisPointer').coordSysAxesInfo;

    if (seriesIndex == null || dataIndex == null || coordSysAxesInfo == null) {
      return;
    }

    var seriesModel = ecModel.getSeriesByIndex(seriesIndex);

    if (!seriesModel) {
      return;
    }

    var data = seriesModel.getData();
    var tooltipModel = buildTooltipModel([data.getItemModel(dataIndex), seriesModel, (seriesModel.coordinateSystem || {}).model, tooltipModel]);

    if (tooltipModel.get('trigger') !== 'axis') {
      return;
    }

    api.dispatchAction({
      type: 'updateAxisPointer',
      seriesIndex: seriesIndex,
      dataIndex: dataIndex,
      position: payload.position
    });
    return true;
  },
  _tryShow: function _tryShow(e, dispatchAction) {
    var el = e.target;
    var tooltipModel = this._tooltipModel;

    if (!tooltipModel) {
      return;
    } // Save mouse x, mouse y. So we can try to keep showing the tip if chart is refreshed


    this._lastX = e.offsetX;
    this._lastY = e.offsetY;
    var dataByCoordSys = e.dataByCoordSys;

    if (dataByCoordSys && dataByCoordSys.length) {
      this._showAxisTooltip(dataByCoordSys, e);
    } // Always show item tooltip if mouse is on the element with dataIndex
    else if (el && el.dataIndex != null) {
        this._lastDataByCoordSys = null;

        this._showSeriesItemTooltip(e, el, dispatchAction);
      } // Tooltip provided directly. Like legend.
      else if (el && el.tooltip) {
          this._lastDataByCoordSys = null;

          this._showComponentItemTooltip(e, el, dispatchAction);
        } else {
          this._lastDataByCoordSys = null;

          this._hide(dispatchAction);
        }
  },
  _showOrMove: function _showOrMove(tooltipModel, cb) {
    // showDelay is used in this case: tooltip.enterable is set
    // as true. User intent to move mouse into tooltip and click
    // something. `showDelay` makes it easyer to enter the content
    // but tooltip do not move immediately.
    var delay = tooltipModel.get('showDelay');
    cb = zrUtil.bind(cb, this);
    clearTimeout(this._showTimout);
    delay > 0 ? this._showTimout = setTimeout(cb, delay) : cb();
  },
  _showAxisTooltip: function _showAxisTooltip(dataByCoordSys, e) {
    var ecModel = this._ecModel;
    var globalTooltipModel = this._tooltipModel;
    var point = [e.offsetX, e.offsetY];
    var singleDefaultHTML = [];
    var singleParamsList = [];
    var singleTooltipModel = buildTooltipModel([e.tooltipOption, globalTooltipModel]);
    var renderMode = this._renderMode;
    var newLine = this._newLine;
    var markers = {};
    each(dataByCoordSys, function (itemCoordSys) {
      // var coordParamList = [];
      // var coordDefaultHTML = [];
      // var coordTooltipModel = buildTooltipModel([
      //     e.tooltipOption,
      //     itemCoordSys.tooltipOption,
      //     ecModel.getComponent(itemCoordSys.coordSysMainType, itemCoordSys.coordSysIndex),
      //     globalTooltipModel
      // ]);
      // var displayMode = coordTooltipModel.get('displayMode');
      // var paramsList = displayMode === 'single' ? singleParamsList : [];
      each(itemCoordSys.dataByAxis, function (item) {
        var axisModel = ecModel.getComponent(item.axisDim + 'Axis', item.axisIndex);
        var axisValue = item.value;
        var seriesDefaultHTML = [];

        if (!axisModel || axisValue == null) {
          return;
        }

        var valueLabel = axisPointerViewHelper.getValueLabel(axisValue, axisModel.axis, ecModel, item.seriesDataIndices, item.valueLabelOpt);
        zrUtil.each(item.seriesDataIndices, function (idxItem) {
          var series = ecModel.getSeriesByIndex(idxItem.seriesIndex);
          var dataIndex = idxItem.dataIndexInside;
          var dataParams = series && series.getDataParams(dataIndex);
          dataParams.axisDim = item.axisDim;
          dataParams.axisIndex = item.axisIndex;
          dataParams.axisType = item.axisType;
          dataParams.axisId = item.axisId;
          dataParams.axisValue = axisHelper.getAxisRawValue(axisModel.axis, axisValue);
          dataParams.axisValueLabel = valueLabel;

          if (dataParams) {
            singleParamsList.push(dataParams);
            var seriesTooltip = series.formatTooltip(dataIndex, true, null, renderMode);
            var html;

            if (zrUtil.isObject(seriesTooltip)) {
              html = seriesTooltip.html;
              var newMarkers = seriesTooltip.markers;
              zrUtil.merge(markers, newMarkers);
            } else {
              html = seriesTooltip;
            }

            seriesDefaultHTML.push(html);
          }
        }); // Default tooltip content
        // FIXME
        // (1) shold be the first data which has name?
        // (2) themeRiver, firstDataIndex is array, and first line is unnecessary.

        var firstLine = valueLabel;

        if (renderMode !== 'html') {
          singleDefaultHTML.push(seriesDefaultHTML.join(newLine));
        } else {
          singleDefaultHTML.push((firstLine ? formatUtil.encodeHTML(firstLine) + newLine : '') + seriesDefaultHTML.join(newLine));
        }
      });
    }, this); // In most case, the second axis is shown upper than the first one.

    singleDefaultHTML.reverse();
    singleDefaultHTML = singleDefaultHTML.join(this._newLine + this._newLine);
    var positionExpr = e.position;

    this._showOrMove(singleTooltipModel, function () {
      if (this._updateContentNotChangedOnAxis(dataByCoordSys)) {
        this._updatePosition(singleTooltipModel, positionExpr, point[0], point[1], this._tooltipContent, singleParamsList);
      } else {
        this._showTooltipContent(singleTooltipModel, singleDefaultHTML, singleParamsList, Math.random(), point[0], point[1], positionExpr, undefined, markers);
      }
    }); // Do not trigger events here, because this branch only be entered
    // from dispatchAction.

  },
  _showSeriesItemTooltip: function _showSeriesItemTooltip(e, el, dispatchAction) {
    var ecModel = this._ecModel; // Use dataModel in element if possible
    // Used when mouseover on a element like markPoint or edge
    // In which case, the data is not main data in series.

    var seriesIndex = el.seriesIndex;
    var seriesModel = ecModel.getSeriesByIndex(seriesIndex); // For example, graph link.

    var dataModel = el.dataModel || seriesModel;
    var dataIndex = el.dataIndex;
    var dataType = el.dataType;
    var data = dataModel.getData();
    var tooltipModel = buildTooltipModel([data.getItemModel(dataIndex), dataModel, seriesModel && (seriesModel.coordinateSystem || {}).model, this._tooltipModel]);
    var tooltipTrigger = tooltipModel.get('trigger');

    if (tooltipTrigger != null && tooltipTrigger !== 'item') {
      return;
    }

    var params = dataModel.getDataParams(dataIndex, dataType);
    var seriesTooltip = dataModel.formatTooltip(dataIndex, false, dataType, this._renderMode);
    var defaultHtml;
    var markers;

    if (zrUtil.isObject(seriesTooltip)) {
      defaultHtml = seriesTooltip.html;
      markers = seriesTooltip.markers;
    } else {
      defaultHtml = seriesTooltip;
      markers = null;
    }

    var asyncTicket = 'item_' + dataModel.name + '_' + dataIndex;

    this._showOrMove(tooltipModel, function () {
      this._showTooltipContent(tooltipModel, defaultHtml, params, asyncTicket, e.offsetX, e.offsetY, e.position, e.target, markers);
    }); // FIXME
    // duplicated showtip if manuallyShowTip is called from dispatchAction.


    dispatchAction({
      type: 'showTip',
      dataIndexInside: dataIndex,
      dataIndex: data.getRawIndex(dataIndex),
      seriesIndex: seriesIndex,
      from: this.uid
    });
  },
  _showComponentItemTooltip: function _showComponentItemTooltip(e, el, dispatchAction) {
    var tooltipOpt = el.tooltip;

    if (typeof tooltipOpt === 'string') {
      var content = tooltipOpt;
      tooltipOpt = {
        content: content,
        // Fixed formatter
        formatter: content
      };
    }

    var subTooltipModel = new Model(tooltipOpt, this._tooltipModel, this._ecModel);
    var defaultHtml = subTooltipModel.get('content');
    var asyncTicket = Math.random(); // Do not check whether `trigger` is 'none' here, because `trigger`
    // only works on cooridinate system. In fact, we have not found case
    // that requires setting `trigger` nothing on component yet.

    this._showOrMove(subTooltipModel, function () {
      this._showTooltipContent(subTooltipModel, defaultHtml, subTooltipModel.get('formatterParams') || {}, asyncTicket, e.offsetX, e.offsetY, e.position, el);
    }); // If not dispatch showTip, tip may be hide triggered by axis.


    dispatchAction({
      type: 'showTip',
      from: this.uid
    });
  },
  _showTooltipContent: function _showTooltipContent(tooltipModel, defaultHtml, params, asyncTicket, x, y, positionExpr, el, markers) {
    // Reset ticket
    this._ticket = '';

    if (!tooltipModel.get('showContent') || !tooltipModel.get('show')) {
      return;
    }

    var tooltipContent = this._tooltipContent;
    var formatter = tooltipModel.get('formatter');
    positionExpr = positionExpr || tooltipModel.get('position');
    var html = defaultHtml;

    if (formatter && typeof formatter === 'string') {
      html = formatUtil.formatTpl(formatter, params, true);
    } else if (typeof formatter === 'function') {
      var callback = bind(function (cbTicket, html) {
        if (cbTicket === this._ticket) {
          tooltipContent.setContent(html, markers, tooltipModel);

          this._updatePosition(tooltipModel, positionExpr, x, y, tooltipContent, params, el);
        }
      }, this);
      this._ticket = asyncTicket;
      html = formatter(params, asyncTicket, callback);
    }

    tooltipContent.setContent(html, markers, tooltipModel);
    tooltipContent.show(tooltipModel);

    this._updatePosition(tooltipModel, positionExpr, x, y, tooltipContent, params, el);
  },

  /**
   * @param  {string|Function|Array.<number>|Object} positionExpr
   * @param  {number} x Mouse x
   * @param  {number} y Mouse y
   * @param  {boolean} confine Whether confine tooltip content in view rect.
   * @param  {Object|<Array.<Object>} params
   * @param  {module:zrender/Element} el target element
   * @param  {module:echarts/ExtensionAPI} api
   * @return {Array.<number>}
   */
  _updatePosition: function _updatePosition(tooltipModel, positionExpr, x, y, content, params, el) {
    var viewWidth = this._api.getWidth();

    var viewHeight = this._api.getHeight();

    positionExpr = positionExpr || tooltipModel.get('position');
    var contentSize = content.getSize();
    var align = tooltipModel.get('align');
    var vAlign = tooltipModel.get('verticalAlign');
    var rect = el && el.getBoundingRect().clone();
    el && rect.applyTransform(el.transform);

    if (typeof positionExpr === 'function') {
      // Callback of position can be an array or a string specify the position
      positionExpr = positionExpr([x, y], params, content.el, rect, {
        viewSize: [viewWidth, viewHeight],
        contentSize: contentSize.slice()
      });
    }

    if (zrUtil.isArray(positionExpr)) {
      x = parsePercent(positionExpr[0], viewWidth);
      y = parsePercent(positionExpr[1], viewHeight);
    } else if (zrUtil.isObject(positionExpr)) {
      positionExpr.width = contentSize[0];
      positionExpr.height = contentSize[1];
      var layoutRect = layoutUtil.getLayoutRect(positionExpr, {
        width: viewWidth,
        height: viewHeight
      });
      x = layoutRect.x;
      y = layoutRect.y;
      align = null; // When positionExpr is left/top/right/bottom,
      // align and verticalAlign will not work.

      vAlign = null;
    } // Specify tooltip position by string 'top' 'bottom' 'left' 'right' around graphic element
    else if (typeof positionExpr === 'string' && el) {
        var pos = calcTooltipPosition(positionExpr, rect, contentSize);
        x = pos[0];
        y = pos[1];
      } else {
        var pos = refixTooltipPosition(x, y, content, viewWidth, viewHeight, align ? null : 20, vAlign ? null : 20);
        x = pos[0];
        y = pos[1];
      }

    align && (x -= isCenterAlign(align) ? contentSize[0] / 2 : align === 'right' ? contentSize[0] : 0);
    vAlign && (y -= isCenterAlign(vAlign) ? contentSize[1] / 2 : vAlign === 'bottom' ? contentSize[1] : 0);

    if (tooltipModel.get('confine')) {
      var pos = confineTooltipPosition(x, y, content, viewWidth, viewHeight);
      x = pos[0];
      y = pos[1];
    }

    content.moveTo(x, y);
  },
  // FIXME
  // Should we remove this but leave this to user?
  _updateContentNotChangedOnAxis: function _updateContentNotChangedOnAxis(dataByCoordSys) {
    var lastCoordSys = this._lastDataByCoordSys;
    var contentNotChanged = !!lastCoordSys && lastCoordSys.length === dataByCoordSys.length;
    contentNotChanged && each(lastCoordSys, function (lastItemCoordSys, indexCoordSys) {
      var lastDataByAxis = lastItemCoordSys.dataByAxis || {};
      var thisItemCoordSys = dataByCoordSys[indexCoordSys] || {};
      var thisDataByAxis = thisItemCoordSys.dataByAxis || [];
      contentNotChanged &= lastDataByAxis.length === thisDataByAxis.length;
      contentNotChanged && each(lastDataByAxis, function (lastItem, indexAxis) {
        var thisItem = thisDataByAxis[indexAxis] || {};
        var lastIndices = lastItem.seriesDataIndices || [];
        var newIndices = thisItem.seriesDataIndices || [];
        contentNotChanged &= lastItem.value === thisItem.value && lastItem.axisType === thisItem.axisType && lastItem.axisId === thisItem.axisId && lastIndices.length === newIndices.length;
        contentNotChanged && each(lastIndices, function (lastIdxItem, j) {
          var newIdxItem = newIndices[j];
          contentNotChanged &= lastIdxItem.seriesIndex === newIdxItem.seriesIndex && lastIdxItem.dataIndex === newIdxItem.dataIndex;
        });
      });
    });
    this._lastDataByCoordSys = dataByCoordSys;
    return !!contentNotChanged;
  },
  _hide: function _hide(dispatchAction) {
    // Do not directly hideLater here, because this behavior may be prevented
    // in dispatchAction when showTip is dispatched.
    // FIXME
    // duplicated hideTip if manuallyHideTip is called from dispatchAction.
    this._lastDataByCoordSys = null;
    dispatchAction({
      type: 'hideTip',
      from: this.uid
    });
  },
  dispose: function dispose(ecModel, api) {
    if (env.node) {
      return;
    }

    this._tooltipContent.hide();

    globalListener.unregister('itemTooltip', api);
  }
});
/**
 * @param {Array.<Object|module:echarts/model/Model>} modelCascade
 * From top to bottom. (the last one should be globalTooltipModel);
 */


function buildTooltipModel(modelCascade) {
  var resultModel = modelCascade.pop();

  while (modelCascade.length) {
    var tooltipOpt = modelCascade.pop();

    if (tooltipOpt) {
      if (Model.isInstance(tooltipOpt)) {
        tooltipOpt = tooltipOpt.get('tooltip', true);
      } // In each data item tooltip can be simply write:
      // {
      //  value: 10,
      //  tooltip: 'Something you need to know'
      // }


      if (typeof tooltipOpt === 'string') {
        tooltipOpt = {
          formatter: tooltipOpt
        };
      }

      resultModel = new Model(tooltipOpt, resultModel, resultModel.ecModel);
    }
  }

  return resultModel;
}

function makeDispatchAction(payload, api) {
  return payload.dispatchAction || zrUtil.bind(api.dispatchAction, api);
}

function refixTooltipPosition(x, y, content, viewWidth, viewHeight, gapH, gapV) {
  var size = content.getOuterSize();
  var width = size.width;
  var height = size.height;

  if (gapH != null) {
    if (x + width + gapH > viewWidth) {
      x -= width + gapH;
    } else {
      x += gapH;
    }
  }

  if (gapV != null) {
    if (y + height + gapV > viewHeight) {
      y -= height + gapV;
    } else {
      y += gapV;
    }
  }

  return [x, y];
}

function confineTooltipPosition(x, y, content, viewWidth, viewHeight) {
  var size = content.getOuterSize();
  var width = size.width;
  var height = size.height;
  x = Math.min(x + width, viewWidth) - width;
  y = Math.min(y + height, viewHeight) - height;
  x = Math.max(x, 0);
  y = Math.max(y, 0);
  return [x, y];
}

function calcTooltipPosition(position, rect, contentSize) {
  var domWidth = contentSize[0];
  var domHeight = contentSize[1];
  var gap = 5;
  var x = 0;
  var y = 0;
  var rectWidth = rect.width;
  var rectHeight = rect.height;

  switch (position) {
    case 'inside':
      x = rect.x + rectWidth / 2 - domWidth / 2;
      y = rect.y + rectHeight / 2 - domHeight / 2;
      break;

    case 'top':
      x = rect.x + rectWidth / 2 - domWidth / 2;
      y = rect.y - domHeight - gap;
      break;

    case 'bottom':
      x = rect.x + rectWidth / 2 - domWidth / 2;
      y = rect.y + rectHeight + gap;
      break;

    case 'left':
      x = rect.x - domWidth - gap;
      y = rect.y + rectHeight / 2 - domHeight / 2;
      break;

    case 'right':
      x = rect.x + rectWidth + gap;
      y = rect.y + rectHeight / 2 - domHeight / 2;
  }

  return [x, y];
}

function isCenterAlign(align) {
  return align === 'center' || align === 'middle';
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcFZpZXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvdG9vbHRpcC9Ub29sdGlwVmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIGVjaGFydHMgPSByZXF1aXJlKFwiLi4vLi4vZWNoYXJ0c1wiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBlbnYgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9lbnZcIik7XG5cbnZhciBUb29sdGlwQ29udGVudCA9IHJlcXVpcmUoXCIuL1Rvb2x0aXBDb250ZW50XCIpO1xuXG52YXIgVG9vbHRpcFJpY2hDb250ZW50ID0gcmVxdWlyZShcIi4vVG9vbHRpcFJpY2hDb250ZW50XCIpO1xuXG52YXIgZm9ybWF0VXRpbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2Zvcm1hdFwiKTtcblxudmFyIG51bWJlclV0aWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9udW1iZXJcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIGZpbmRQb2ludEZyb21TZXJpZXMgPSByZXF1aXJlKFwiLi4vYXhpc1BvaW50ZXIvZmluZFBvaW50RnJvbVNlcmllc1wiKTtcblxudmFyIGxheW91dFV0aWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9sYXlvdXRcIik7XG5cbnZhciBNb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9Nb2RlbFwiKTtcblxudmFyIGdsb2JhbExpc3RlbmVyID0gcmVxdWlyZShcIi4uL2F4aXNQb2ludGVyL2dsb2JhbExpc3RlbmVyXCIpO1xuXG52YXIgYXhpc0hlbHBlciA9IHJlcXVpcmUoXCIuLi8uLi9jb29yZC9heGlzSGVscGVyXCIpO1xuXG52YXIgYXhpc1BvaW50ZXJWaWV3SGVscGVyID0gcmVxdWlyZShcIi4uL2F4aXNQb2ludGVyL3ZpZXdIZWxwZXJcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIGdldFRvb2x0aXBSZW5kZXJNb2RlID0gX21vZGVsLmdldFRvb2x0aXBSZW5kZXJNb2RlO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgYmluZCA9IHpyVXRpbC5iaW5kO1xudmFyIGVhY2ggPSB6clV0aWwuZWFjaDtcbnZhciBwYXJzZVBlcmNlbnQgPSBudW1iZXJVdGlsLnBhcnNlUGVyY2VudDtcbnZhciBwcm94eVJlY3QgPSBuZXcgZ3JhcGhpYy5SZWN0KHtcbiAgc2hhcGU6IHtcbiAgICB4OiAtMSxcbiAgICB5OiAtMSxcbiAgICB3aWR0aDogMixcbiAgICBoZWlnaHQ6IDJcbiAgfVxufSk7XG5cbnZhciBfZGVmYXVsdCA9IGVjaGFydHMuZXh0ZW5kQ29tcG9uZW50Vmlldyh7XG4gIHR5cGU6ICd0b29sdGlwJyxcbiAgaW5pdDogZnVuY3Rpb24gKGVjTW9kZWwsIGFwaSkge1xuICAgIGlmIChlbnYubm9kZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciB0b29sdGlwTW9kZWwgPSBlY01vZGVsLmdldENvbXBvbmVudCgndG9vbHRpcCcpO1xuICAgIHZhciByZW5kZXJNb2RlID0gdG9vbHRpcE1vZGVsLmdldCgncmVuZGVyTW9kZScpO1xuICAgIHRoaXMuX3JlbmRlck1vZGUgPSBnZXRUb29sdGlwUmVuZGVyTW9kZShyZW5kZXJNb2RlKTtcbiAgICB2YXIgdG9vbHRpcENvbnRlbnQ7XG5cbiAgICBpZiAodGhpcy5fcmVuZGVyTW9kZSA9PT0gJ2h0bWwnKSB7XG4gICAgICB0b29sdGlwQ29udGVudCA9IG5ldyBUb29sdGlwQ29udGVudChhcGkuZ2V0RG9tKCksIGFwaSk7XG4gICAgICB0aGlzLl9uZXdMaW5lID0gJzxici8+JztcbiAgICB9IGVsc2Uge1xuICAgICAgdG9vbHRpcENvbnRlbnQgPSBuZXcgVG9vbHRpcFJpY2hDb250ZW50KGFwaSk7XG4gICAgICB0aGlzLl9uZXdMaW5lID0gJ1xcbic7XG4gICAgfVxuXG4gICAgdGhpcy5fdG9vbHRpcENvbnRlbnQgPSB0b29sdGlwQ29udGVudDtcbiAgfSxcbiAgcmVuZGVyOiBmdW5jdGlvbiAodG9vbHRpcE1vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICBpZiAoZW52Lm5vZGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IC8vIFJlc2V0XG5cblxuICAgIHRoaXMuZ3JvdXAucmVtb3ZlQWxsKCk7XG4gICAgLyoqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcE1vZGVsfVxuICAgICAqL1xuXG4gICAgdGhpcy5fdG9vbHRpcE1vZGVsID0gdG9vbHRpcE1vZGVsO1xuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH1cbiAgICAgKi9cblxuICAgIHRoaXMuX2VjTW9kZWwgPSBlY01vZGVsO1xuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX1cbiAgICAgKi9cblxuICAgIHRoaXMuX2FwaSA9IGFwaTtcbiAgICAvKipcbiAgICAgKiBTaG91bGQgYmUgY2xlYW5lZCB3aGVuIHJlbmRlci5cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEB0eXBlIHtBcnJheS48QXJyYXkuPE9iamVjdD4+fVxuICAgICAqL1xuXG4gICAgdGhpcy5fbGFzdERhdGFCeUNvb3JkU3lzID0gbnVsbDtcbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAqL1xuXG4gICAgdGhpcy5fYWx3YXlzU2hvd0NvbnRlbnQgPSB0b29sdGlwTW9kZWwuZ2V0KCdhbHdheXNTaG93Q29udGVudCcpO1xuICAgIHZhciB0b29sdGlwQ29udGVudCA9IHRoaXMuX3Rvb2x0aXBDb250ZW50O1xuICAgIHRvb2x0aXBDb250ZW50LnVwZGF0ZSgpO1xuICAgIHRvb2x0aXBDb250ZW50LnNldEVudGVyYWJsZSh0b29sdGlwTW9kZWwuZ2V0KCdlbnRlcmFibGUnKSk7XG5cbiAgICB0aGlzLl9pbml0R2xvYmFsTGlzdGVuZXIoKTtcblxuICAgIHRoaXMuX2tlZXBTaG93KCk7XG4gIH0sXG4gIF9pbml0R2xvYmFsTGlzdGVuZXI6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdG9vbHRpcE1vZGVsID0gdGhpcy5fdG9vbHRpcE1vZGVsO1xuICAgIHZhciB0cmlnZ2VyT24gPSB0b29sdGlwTW9kZWwuZ2V0KCd0cmlnZ2VyT24nKTtcbiAgICBnbG9iYWxMaXN0ZW5lci5yZWdpc3RlcignaXRlbVRvb2x0aXAnLCB0aGlzLl9hcGksIGJpbmQoZnVuY3Rpb24gKGN1cnJUcmlnZ2VyLCBlLCBkaXNwYXRjaEFjdGlvbikge1xuICAgICAgLy8gSWYgJ25vbmUnLCBpdCBpcyBub3QgY29udHJvbGxlZCBieSBtb3VzZSB0b3RhbGx5LlxuICAgICAgaWYgKHRyaWdnZXJPbiAhPT0gJ25vbmUnKSB7XG4gICAgICAgIGlmICh0cmlnZ2VyT24uaW5kZXhPZihjdXJyVHJpZ2dlcikgPj0gMCkge1xuICAgICAgICAgIHRoaXMuX3RyeVNob3coZSwgZGlzcGF0Y2hBY3Rpb24pO1xuICAgICAgICB9IGVsc2UgaWYgKGN1cnJUcmlnZ2VyID09PSAnbGVhdmUnKSB7XG4gICAgICAgICAgdGhpcy5faGlkZShkaXNwYXRjaEFjdGlvbik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCB0aGlzKSk7XG4gIH0sXG4gIF9rZWVwU2hvdzogZnVuY3Rpb24gKCkge1xuICAgIHZhciB0b29sdGlwTW9kZWwgPSB0aGlzLl90b29sdGlwTW9kZWw7XG4gICAgdmFyIGVjTW9kZWwgPSB0aGlzLl9lY01vZGVsO1xuICAgIHZhciBhcGkgPSB0aGlzLl9hcGk7IC8vIFRyeSB0byBrZWVwIHRoZSB0b29sdGlwIHNob3cgd2hlbiByZWZyZXNoaW5nXG5cbiAgICBpZiAodGhpcy5fbGFzdFggIT0gbnVsbCAmJiB0aGlzLl9sYXN0WSAhPSBudWxsIC8vIFdoZW4gdXNlciBpcyB3aWxsaW5nIHRvIGNvbnRyb2wgdG9vbHRpcCB0b3RhbGx5IHVzaW5nIEFQSSxcbiAgICAvLyBzZWxmLm1hbnVhbGx5U2hvd1RpcCh7eCwgeX0pIG1pZ2h0IGNhdXNlIHRvb2x0aXAgaGlkZSxcbiAgICAvLyB3aGljaCBpcyBub3QgZXhwZWN0ZWQuXG4gICAgJiYgdG9vbHRpcE1vZGVsLmdldCgndHJpZ2dlck9uJykgIT09ICdub25lJykge1xuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuX3JlZnJlc2hVcGRhdGVUaW1lb3V0KTtcbiAgICAgIHRoaXMuX3JlZnJlc2hVcGRhdGVUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vIFNob3cgdGlwIG5leHQgdGljayBhZnRlciBvdGhlciBjaGFydHMgYXJlIHJlbmRlcmVkXG4gICAgICAgIC8vIEluIGNhc2UgaGlnaGxpZ2h0IGFjdGlvbiBoYXMgd3JvbmcgcmVzdWx0XG4gICAgICAgIC8vIEZJWE1FXG4gICAgICAgIHNlbGYubWFudWFsbHlTaG93VGlwKHRvb2x0aXBNb2RlbCwgZWNNb2RlbCwgYXBpLCB7XG4gICAgICAgICAgeDogc2VsZi5fbGFzdFgsXG4gICAgICAgICAgeTogc2VsZi5fbGFzdFlcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIFNob3cgdGlwIG1hbnVhbGx5IGJ5XG4gICAqIGRpc3BhdGNoQWN0aW9uKHtcbiAgICogICAgIHR5cGU6ICdzaG93VGlwJyxcbiAgICogICAgIHg6IDEwLFxuICAgKiAgICAgeTogMTBcbiAgICogfSk7XG4gICAqIE9yXG4gICAqIGRpc3BhdGNoQWN0aW9uKHtcbiAgICogICAgICB0eXBlOiAnc2hvd1RpcCcsXG4gICAqICAgICAgc2VyaWVzSW5kZXg6IDAsXG4gICAqICAgICAgZGF0YUluZGV4IG9yIGRhdGFJbmRleEluc2lkZSBvciBuYW1lXG4gICAqIH0pO1xuICAgKlxuICAgKiAgVE9ETyBCYXRjaFxuICAgKi9cbiAgbWFudWFsbHlTaG93VGlwOiBmdW5jdGlvbiAodG9vbHRpcE1vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpIHtcbiAgICBpZiAocGF5bG9hZC5mcm9tID09PSB0aGlzLnVpZCB8fCBlbnYubm9kZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBkaXNwYXRjaEFjdGlvbiA9IG1ha2VEaXNwYXRjaEFjdGlvbihwYXlsb2FkLCBhcGkpOyAvLyBSZXNldCB0aWNrZXRcblxuICAgIHRoaXMuX3RpY2tldCA9ICcnOyAvLyBXaGVuIHRyaWdnZXJlZCBmcm9tIGF4aXNQb2ludGVyLlxuXG4gICAgdmFyIGRhdGFCeUNvb3JkU3lzID0gcGF5bG9hZC5kYXRhQnlDb29yZFN5cztcblxuICAgIGlmIChwYXlsb2FkLnRvb2x0aXAgJiYgcGF5bG9hZC54ICE9IG51bGwgJiYgcGF5bG9hZC55ICE9IG51bGwpIHtcbiAgICAgIHZhciBlbCA9IHByb3h5UmVjdDtcbiAgICAgIGVsLnBvc2l0aW9uID0gW3BheWxvYWQueCwgcGF5bG9hZC55XTtcbiAgICAgIGVsLnVwZGF0ZSgpO1xuICAgICAgZWwudG9vbHRpcCA9IHBheWxvYWQudG9vbHRpcDsgLy8gTWFudWFsbHkgc2hvdyB0b29sdGlwIHdoaWxlIHZpZXcgaXMgbm90IHVzaW5nIHpyZW5kZXIgZWxlbWVudHMuXG5cbiAgICAgIHRoaXMuX3RyeVNob3coe1xuICAgICAgICBvZmZzZXRYOiBwYXlsb2FkLngsXG4gICAgICAgIG9mZnNldFk6IHBheWxvYWQueSxcbiAgICAgICAgdGFyZ2V0OiBlbFxuICAgICAgfSwgZGlzcGF0Y2hBY3Rpb24pO1xuICAgIH0gZWxzZSBpZiAoZGF0YUJ5Q29vcmRTeXMpIHtcbiAgICAgIHRoaXMuX3RyeVNob3coe1xuICAgICAgICBvZmZzZXRYOiBwYXlsb2FkLngsXG4gICAgICAgIG9mZnNldFk6IHBheWxvYWQueSxcbiAgICAgICAgcG9zaXRpb246IHBheWxvYWQucG9zaXRpb24sXG4gICAgICAgIGV2ZW50OiB7fSxcbiAgICAgICAgZGF0YUJ5Q29vcmRTeXM6IHBheWxvYWQuZGF0YUJ5Q29vcmRTeXMsXG4gICAgICAgIHRvb2x0aXBPcHRpb246IHBheWxvYWQudG9vbHRpcE9wdGlvblxuICAgICAgfSwgZGlzcGF0Y2hBY3Rpb24pO1xuICAgIH0gZWxzZSBpZiAocGF5bG9hZC5zZXJpZXNJbmRleCAhPSBudWxsKSB7XG4gICAgICBpZiAodGhpcy5fbWFudWFsbHlBeGlzU2hvd1RpcCh0b29sdGlwTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgcG9pbnRJbmZvID0gZmluZFBvaW50RnJvbVNlcmllcyhwYXlsb2FkLCBlY01vZGVsKTtcbiAgICAgIHZhciBjeCA9IHBvaW50SW5mby5wb2ludFswXTtcbiAgICAgIHZhciBjeSA9IHBvaW50SW5mby5wb2ludFsxXTtcblxuICAgICAgaWYgKGN4ICE9IG51bGwgJiYgY3kgIT0gbnVsbCkge1xuICAgICAgICB0aGlzLl90cnlTaG93KHtcbiAgICAgICAgICBvZmZzZXRYOiBjeCxcbiAgICAgICAgICBvZmZzZXRZOiBjeSxcbiAgICAgICAgICBwb3NpdGlvbjogcGF5bG9hZC5wb3NpdGlvbixcbiAgICAgICAgICB0YXJnZXQ6IHBvaW50SW5mby5lbCxcbiAgICAgICAgICBldmVudDoge31cbiAgICAgICAgfSwgZGlzcGF0Y2hBY3Rpb24pO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAocGF5bG9hZC54ICE9IG51bGwgJiYgcGF5bG9hZC55ICE9IG51bGwpIHtcbiAgICAgIC8vIEZJWE1FXG4gICAgICAvLyBzaG91bGQgd3JhcCBkaXNwYXRjaEFjdGlvbiBsaWtlIGBheGlzUG9pbnRlci9nbG9iYWxMaXN0ZW5lcmAgP1xuICAgICAgYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICAgICAgdHlwZTogJ3VwZGF0ZUF4aXNQb2ludGVyJyxcbiAgICAgICAgeDogcGF5bG9hZC54LFxuICAgICAgICB5OiBwYXlsb2FkLnlcbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLl90cnlTaG93KHtcbiAgICAgICAgb2Zmc2V0WDogcGF5bG9hZC54LFxuICAgICAgICBvZmZzZXRZOiBwYXlsb2FkLnksXG4gICAgICAgIHBvc2l0aW9uOiBwYXlsb2FkLnBvc2l0aW9uLFxuICAgICAgICB0YXJnZXQ6IGFwaS5nZXRacigpLmZpbmRIb3ZlcihwYXlsb2FkLngsIHBheWxvYWQueSkudGFyZ2V0LFxuICAgICAgICBldmVudDoge31cbiAgICAgIH0sIGRpc3BhdGNoQWN0aW9uKTtcbiAgICB9XG4gIH0sXG4gIG1hbnVhbGx5SGlkZVRpcDogZnVuY3Rpb24gKHRvb2x0aXBNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7XG4gICAgdmFyIHRvb2x0aXBDb250ZW50ID0gdGhpcy5fdG9vbHRpcENvbnRlbnQ7XG5cbiAgICBpZiAoIXRoaXMuX2Fsd2F5c1Nob3dDb250ZW50ICYmIHRoaXMuX3Rvb2x0aXBNb2RlbCkge1xuICAgICAgdG9vbHRpcENvbnRlbnQuaGlkZUxhdGVyKHRoaXMuX3Rvb2x0aXBNb2RlbC5nZXQoJ2hpZGVEZWxheScpKTtcbiAgICB9XG5cbiAgICB0aGlzLl9sYXN0WCA9IHRoaXMuX2xhc3RZID0gbnVsbDtcblxuICAgIGlmIChwYXlsb2FkLmZyb20gIT09IHRoaXMudWlkKSB7XG4gICAgICB0aGlzLl9oaWRlKG1ha2VEaXNwYXRjaEFjdGlvbihwYXlsb2FkLCBhcGkpKTtcbiAgICB9XG4gIH0sXG4gIC8vIEJlIGNvbXBhdGlibGUgd2l0aCBwcmV2aW91cyBkZXNpZ24sIHRoYXQgaXMsIHdoZW4gdG9vbHRpcC50eXBlIGlzICdheGlzJyBhbmRcbiAgLy8gZGlzcGF0Y2hBY3Rpb24gJ3Nob3dUaXAnIHdpdGggc2VyaWVzSW5kZXggYW5kIGRhdGFJbmRleCB3aWxsIHRyaWdnZXIgYXhpcyBwb2ludGVyXG4gIC8vIGFuZCB0b29sdGlwLlxuICBfbWFudWFsbHlBeGlzU2hvd1RpcDogZnVuY3Rpb24gKHRvb2x0aXBNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7XG4gICAgdmFyIHNlcmllc0luZGV4ID0gcGF5bG9hZC5zZXJpZXNJbmRleDtcbiAgICB2YXIgZGF0YUluZGV4ID0gcGF5bG9hZC5kYXRhSW5kZXg7XG4gICAgdmFyIGNvb3JkU3lzQXhlc0luZm8gPSBlY01vZGVsLmdldENvbXBvbmVudCgnYXhpc1BvaW50ZXInKS5jb29yZFN5c0F4ZXNJbmZvO1xuXG4gICAgaWYgKHNlcmllc0luZGV4ID09IG51bGwgfHwgZGF0YUluZGV4ID09IG51bGwgfHwgY29vcmRTeXNBeGVzSW5mbyA9PSBudWxsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHNlcmllc01vZGVsID0gZWNNb2RlbC5nZXRTZXJpZXNCeUluZGV4KHNlcmllc0luZGV4KTtcblxuICAgIGlmICghc2VyaWVzTW9kZWwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgdG9vbHRpcE1vZGVsID0gYnVpbGRUb29sdGlwTW9kZWwoW2RhdGEuZ2V0SXRlbU1vZGVsKGRhdGFJbmRleCksIHNlcmllc01vZGVsLCAoc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbSB8fCB7fSkubW9kZWwsIHRvb2x0aXBNb2RlbF0pO1xuXG4gICAgaWYgKHRvb2x0aXBNb2RlbC5nZXQoJ3RyaWdnZXInKSAhPT0gJ2F4aXMnKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICAgIHR5cGU6ICd1cGRhdGVBeGlzUG9pbnRlcicsXG4gICAgICBzZXJpZXNJbmRleDogc2VyaWVzSW5kZXgsXG4gICAgICBkYXRhSW5kZXg6IGRhdGFJbmRleCxcbiAgICAgIHBvc2l0aW9uOiBwYXlsb2FkLnBvc2l0aW9uXG4gICAgfSk7XG4gICAgcmV0dXJuIHRydWU7XG4gIH0sXG4gIF90cnlTaG93OiBmdW5jdGlvbiAoZSwgZGlzcGF0Y2hBY3Rpb24pIHtcbiAgICB2YXIgZWwgPSBlLnRhcmdldDtcbiAgICB2YXIgdG9vbHRpcE1vZGVsID0gdGhpcy5fdG9vbHRpcE1vZGVsO1xuXG4gICAgaWYgKCF0b29sdGlwTW9kZWwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9IC8vIFNhdmUgbW91c2UgeCwgbW91c2UgeS4gU28gd2UgY2FuIHRyeSB0byBrZWVwIHNob3dpbmcgdGhlIHRpcCBpZiBjaGFydCBpcyByZWZyZXNoZWRcblxuXG4gICAgdGhpcy5fbGFzdFggPSBlLm9mZnNldFg7XG4gICAgdGhpcy5fbGFzdFkgPSBlLm9mZnNldFk7XG4gICAgdmFyIGRhdGFCeUNvb3JkU3lzID0gZS5kYXRhQnlDb29yZFN5cztcblxuICAgIGlmIChkYXRhQnlDb29yZFN5cyAmJiBkYXRhQnlDb29yZFN5cy5sZW5ndGgpIHtcbiAgICAgIHRoaXMuX3Nob3dBeGlzVG9vbHRpcChkYXRhQnlDb29yZFN5cywgZSk7XG4gICAgfSAvLyBBbHdheXMgc2hvdyBpdGVtIHRvb2x0aXAgaWYgbW91c2UgaXMgb24gdGhlIGVsZW1lbnQgd2l0aCBkYXRhSW5kZXhcbiAgICBlbHNlIGlmIChlbCAmJiBlbC5kYXRhSW5kZXggIT0gbnVsbCkge1xuICAgICAgICB0aGlzLl9sYXN0RGF0YUJ5Q29vcmRTeXMgPSBudWxsO1xuXG4gICAgICAgIHRoaXMuX3Nob3dTZXJpZXNJdGVtVG9vbHRpcChlLCBlbCwgZGlzcGF0Y2hBY3Rpb24pO1xuICAgICAgfSAvLyBUb29sdGlwIHByb3ZpZGVkIGRpcmVjdGx5LiBMaWtlIGxlZ2VuZC5cbiAgICAgIGVsc2UgaWYgKGVsICYmIGVsLnRvb2x0aXApIHtcbiAgICAgICAgICB0aGlzLl9sYXN0RGF0YUJ5Q29vcmRTeXMgPSBudWxsO1xuXG4gICAgICAgICAgdGhpcy5fc2hvd0NvbXBvbmVudEl0ZW1Ub29sdGlwKGUsIGVsLCBkaXNwYXRjaEFjdGlvbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5fbGFzdERhdGFCeUNvb3JkU3lzID0gbnVsbDtcblxuICAgICAgICAgIHRoaXMuX2hpZGUoZGlzcGF0Y2hBY3Rpb24pO1xuICAgICAgICB9XG4gIH0sXG4gIF9zaG93T3JNb3ZlOiBmdW5jdGlvbiAodG9vbHRpcE1vZGVsLCBjYikge1xuICAgIC8vIHNob3dEZWxheSBpcyB1c2VkIGluIHRoaXMgY2FzZTogdG9vbHRpcC5lbnRlcmFibGUgaXMgc2V0XG4gICAgLy8gYXMgdHJ1ZS4gVXNlciBpbnRlbnQgdG8gbW92ZSBtb3VzZSBpbnRvIHRvb2x0aXAgYW5kIGNsaWNrXG4gICAgLy8gc29tZXRoaW5nLiBgc2hvd0RlbGF5YCBtYWtlcyBpdCBlYXN5ZXIgdG8gZW50ZXIgdGhlIGNvbnRlbnRcbiAgICAvLyBidXQgdG9vbHRpcCBkbyBub3QgbW92ZSBpbW1lZGlhdGVseS5cbiAgICB2YXIgZGVsYXkgPSB0b29sdGlwTW9kZWwuZ2V0KCdzaG93RGVsYXknKTtcbiAgICBjYiA9IHpyVXRpbC5iaW5kKGNiLCB0aGlzKTtcbiAgICBjbGVhclRpbWVvdXQodGhpcy5fc2hvd1RpbW91dCk7XG4gICAgZGVsYXkgPiAwID8gdGhpcy5fc2hvd1RpbW91dCA9IHNldFRpbWVvdXQoY2IsIGRlbGF5KSA6IGNiKCk7XG4gIH0sXG4gIF9zaG93QXhpc1Rvb2x0aXA6IGZ1bmN0aW9uIChkYXRhQnlDb29yZFN5cywgZSkge1xuICAgIHZhciBlY01vZGVsID0gdGhpcy5fZWNNb2RlbDtcbiAgICB2YXIgZ2xvYmFsVG9vbHRpcE1vZGVsID0gdGhpcy5fdG9vbHRpcE1vZGVsO1xuICAgIHZhciBwb2ludCA9IFtlLm9mZnNldFgsIGUub2Zmc2V0WV07XG4gICAgdmFyIHNpbmdsZURlZmF1bHRIVE1MID0gW107XG4gICAgdmFyIHNpbmdsZVBhcmFtc0xpc3QgPSBbXTtcbiAgICB2YXIgc2luZ2xlVG9vbHRpcE1vZGVsID0gYnVpbGRUb29sdGlwTW9kZWwoW2UudG9vbHRpcE9wdGlvbiwgZ2xvYmFsVG9vbHRpcE1vZGVsXSk7XG4gICAgdmFyIHJlbmRlck1vZGUgPSB0aGlzLl9yZW5kZXJNb2RlO1xuICAgIHZhciBuZXdMaW5lID0gdGhpcy5fbmV3TGluZTtcbiAgICB2YXIgbWFya2VycyA9IHt9O1xuICAgIGVhY2goZGF0YUJ5Q29vcmRTeXMsIGZ1bmN0aW9uIChpdGVtQ29vcmRTeXMpIHtcbiAgICAgIC8vIHZhciBjb29yZFBhcmFtTGlzdCA9IFtdO1xuICAgICAgLy8gdmFyIGNvb3JkRGVmYXVsdEhUTUwgPSBbXTtcbiAgICAgIC8vIHZhciBjb29yZFRvb2x0aXBNb2RlbCA9IGJ1aWxkVG9vbHRpcE1vZGVsKFtcbiAgICAgIC8vICAgICBlLnRvb2x0aXBPcHRpb24sXG4gICAgICAvLyAgICAgaXRlbUNvb3JkU3lzLnRvb2x0aXBPcHRpb24sXG4gICAgICAvLyAgICAgZWNNb2RlbC5nZXRDb21wb25lbnQoaXRlbUNvb3JkU3lzLmNvb3JkU3lzTWFpblR5cGUsIGl0ZW1Db29yZFN5cy5jb29yZFN5c0luZGV4KSxcbiAgICAgIC8vICAgICBnbG9iYWxUb29sdGlwTW9kZWxcbiAgICAgIC8vIF0pO1xuICAgICAgLy8gdmFyIGRpc3BsYXlNb2RlID0gY29vcmRUb29sdGlwTW9kZWwuZ2V0KCdkaXNwbGF5TW9kZScpO1xuICAgICAgLy8gdmFyIHBhcmFtc0xpc3QgPSBkaXNwbGF5TW9kZSA9PT0gJ3NpbmdsZScgPyBzaW5nbGVQYXJhbXNMaXN0IDogW107XG4gICAgICBlYWNoKGl0ZW1Db29yZFN5cy5kYXRhQnlBeGlzLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICB2YXIgYXhpc01vZGVsID0gZWNNb2RlbC5nZXRDb21wb25lbnQoaXRlbS5heGlzRGltICsgJ0F4aXMnLCBpdGVtLmF4aXNJbmRleCk7XG4gICAgICAgIHZhciBheGlzVmFsdWUgPSBpdGVtLnZhbHVlO1xuICAgICAgICB2YXIgc2VyaWVzRGVmYXVsdEhUTUwgPSBbXTtcblxuICAgICAgICBpZiAoIWF4aXNNb2RlbCB8fCBheGlzVmFsdWUgPT0gbnVsbCkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciB2YWx1ZUxhYmVsID0gYXhpc1BvaW50ZXJWaWV3SGVscGVyLmdldFZhbHVlTGFiZWwoYXhpc1ZhbHVlLCBheGlzTW9kZWwuYXhpcywgZWNNb2RlbCwgaXRlbS5zZXJpZXNEYXRhSW5kaWNlcywgaXRlbS52YWx1ZUxhYmVsT3B0KTtcbiAgICAgICAgenJVdGlsLmVhY2goaXRlbS5zZXJpZXNEYXRhSW5kaWNlcywgZnVuY3Rpb24gKGlkeEl0ZW0pIHtcbiAgICAgICAgICB2YXIgc2VyaWVzID0gZWNNb2RlbC5nZXRTZXJpZXNCeUluZGV4KGlkeEl0ZW0uc2VyaWVzSW5kZXgpO1xuICAgICAgICAgIHZhciBkYXRhSW5kZXggPSBpZHhJdGVtLmRhdGFJbmRleEluc2lkZTtcbiAgICAgICAgICB2YXIgZGF0YVBhcmFtcyA9IHNlcmllcyAmJiBzZXJpZXMuZ2V0RGF0YVBhcmFtcyhkYXRhSW5kZXgpO1xuICAgICAgICAgIGRhdGFQYXJhbXMuYXhpc0RpbSA9IGl0ZW0uYXhpc0RpbTtcbiAgICAgICAgICBkYXRhUGFyYW1zLmF4aXNJbmRleCA9IGl0ZW0uYXhpc0luZGV4O1xuICAgICAgICAgIGRhdGFQYXJhbXMuYXhpc1R5cGUgPSBpdGVtLmF4aXNUeXBlO1xuICAgICAgICAgIGRhdGFQYXJhbXMuYXhpc0lkID0gaXRlbS5heGlzSWQ7XG4gICAgICAgICAgZGF0YVBhcmFtcy5heGlzVmFsdWUgPSBheGlzSGVscGVyLmdldEF4aXNSYXdWYWx1ZShheGlzTW9kZWwuYXhpcywgYXhpc1ZhbHVlKTtcbiAgICAgICAgICBkYXRhUGFyYW1zLmF4aXNWYWx1ZUxhYmVsID0gdmFsdWVMYWJlbDtcblxuICAgICAgICAgIGlmIChkYXRhUGFyYW1zKSB7XG4gICAgICAgICAgICBzaW5nbGVQYXJhbXNMaXN0LnB1c2goZGF0YVBhcmFtcyk7XG4gICAgICAgICAgICB2YXIgc2VyaWVzVG9vbHRpcCA9IHNlcmllcy5mb3JtYXRUb29sdGlwKGRhdGFJbmRleCwgdHJ1ZSwgbnVsbCwgcmVuZGVyTW9kZSk7XG4gICAgICAgICAgICB2YXIgaHRtbDtcblxuICAgICAgICAgICAgaWYgKHpyVXRpbC5pc09iamVjdChzZXJpZXNUb29sdGlwKSkge1xuICAgICAgICAgICAgICBodG1sID0gc2VyaWVzVG9vbHRpcC5odG1sO1xuICAgICAgICAgICAgICB2YXIgbmV3TWFya2VycyA9IHNlcmllc1Rvb2x0aXAubWFya2VycztcbiAgICAgICAgICAgICAgenJVdGlsLm1lcmdlKG1hcmtlcnMsIG5ld01hcmtlcnMpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgaHRtbCA9IHNlcmllc1Rvb2x0aXA7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHNlcmllc0RlZmF1bHRIVE1MLnB1c2goaHRtbCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTsgLy8gRGVmYXVsdCB0b29sdGlwIGNvbnRlbnRcbiAgICAgICAgLy8gRklYTUVcbiAgICAgICAgLy8gKDEpIHNob2xkIGJlIHRoZSBmaXJzdCBkYXRhIHdoaWNoIGhhcyBuYW1lP1xuICAgICAgICAvLyAoMikgdGhlbWVSaXZlciwgZmlyc3REYXRhSW5kZXggaXMgYXJyYXksIGFuZCBmaXJzdCBsaW5lIGlzIHVubmVjZXNzYXJ5LlxuXG4gICAgICAgIHZhciBmaXJzdExpbmUgPSB2YWx1ZUxhYmVsO1xuXG4gICAgICAgIGlmIChyZW5kZXJNb2RlICE9PSAnaHRtbCcpIHtcbiAgICAgICAgICBzaW5nbGVEZWZhdWx0SFRNTC5wdXNoKHNlcmllc0RlZmF1bHRIVE1MLmpvaW4obmV3TGluZSkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHNpbmdsZURlZmF1bHRIVE1MLnB1c2goKGZpcnN0TGluZSA/IGZvcm1hdFV0aWwuZW5jb2RlSFRNTChmaXJzdExpbmUpICsgbmV3TGluZSA6ICcnKSArIHNlcmllc0RlZmF1bHRIVE1MLmpvaW4obmV3TGluZSkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9LCB0aGlzKTsgLy8gSW4gbW9zdCBjYXNlLCB0aGUgc2Vjb25kIGF4aXMgaXMgc2hvd24gdXBwZXIgdGhhbiB0aGUgZmlyc3Qgb25lLlxuXG4gICAgc2luZ2xlRGVmYXVsdEhUTUwucmV2ZXJzZSgpO1xuICAgIHNpbmdsZURlZmF1bHRIVE1MID0gc2luZ2xlRGVmYXVsdEhUTUwuam9pbih0aGlzLl9uZXdMaW5lICsgdGhpcy5fbmV3TGluZSk7XG4gICAgdmFyIHBvc2l0aW9uRXhwciA9IGUucG9zaXRpb247XG5cbiAgICB0aGlzLl9zaG93T3JNb3ZlKHNpbmdsZVRvb2x0aXBNb2RlbCwgZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRoaXMuX3VwZGF0ZUNvbnRlbnROb3RDaGFuZ2VkT25BeGlzKGRhdGFCeUNvb3JkU3lzKSkge1xuICAgICAgICB0aGlzLl91cGRhdGVQb3NpdGlvbihzaW5nbGVUb29sdGlwTW9kZWwsIHBvc2l0aW9uRXhwciwgcG9pbnRbMF0sIHBvaW50WzFdLCB0aGlzLl90b29sdGlwQ29udGVudCwgc2luZ2xlUGFyYW1zTGlzdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLl9zaG93VG9vbHRpcENvbnRlbnQoc2luZ2xlVG9vbHRpcE1vZGVsLCBzaW5nbGVEZWZhdWx0SFRNTCwgc2luZ2xlUGFyYW1zTGlzdCwgTWF0aC5yYW5kb20oKSwgcG9pbnRbMF0sIHBvaW50WzFdLCBwb3NpdGlvbkV4cHIsIHVuZGVmaW5lZCwgbWFya2Vycyk7XG4gICAgICB9XG4gICAgfSk7IC8vIERvIG5vdCB0cmlnZ2VyIGV2ZW50cyBoZXJlLCBiZWNhdXNlIHRoaXMgYnJhbmNoIG9ubHkgYmUgZW50ZXJlZFxuICAgIC8vIGZyb20gZGlzcGF0Y2hBY3Rpb24uXG5cbiAgfSxcbiAgX3Nob3dTZXJpZXNJdGVtVG9vbHRpcDogZnVuY3Rpb24gKGUsIGVsLCBkaXNwYXRjaEFjdGlvbikge1xuICAgIHZhciBlY01vZGVsID0gdGhpcy5fZWNNb2RlbDsgLy8gVXNlIGRhdGFNb2RlbCBpbiBlbGVtZW50IGlmIHBvc3NpYmxlXG4gICAgLy8gVXNlZCB3aGVuIG1vdXNlb3ZlciBvbiBhIGVsZW1lbnQgbGlrZSBtYXJrUG9pbnQgb3IgZWRnZVxuICAgIC8vIEluIHdoaWNoIGNhc2UsIHRoZSBkYXRhIGlzIG5vdCBtYWluIGRhdGEgaW4gc2VyaWVzLlxuXG4gICAgdmFyIHNlcmllc0luZGV4ID0gZWwuc2VyaWVzSW5kZXg7XG4gICAgdmFyIHNlcmllc01vZGVsID0gZWNNb2RlbC5nZXRTZXJpZXNCeUluZGV4KHNlcmllc0luZGV4KTsgLy8gRm9yIGV4YW1wbGUsIGdyYXBoIGxpbmsuXG5cbiAgICB2YXIgZGF0YU1vZGVsID0gZWwuZGF0YU1vZGVsIHx8IHNlcmllc01vZGVsO1xuICAgIHZhciBkYXRhSW5kZXggPSBlbC5kYXRhSW5kZXg7XG4gICAgdmFyIGRhdGFUeXBlID0gZWwuZGF0YVR5cGU7XG4gICAgdmFyIGRhdGEgPSBkYXRhTW9kZWwuZ2V0RGF0YSgpO1xuICAgIHZhciB0b29sdGlwTW9kZWwgPSBidWlsZFRvb2x0aXBNb2RlbChbZGF0YS5nZXRJdGVtTW9kZWwoZGF0YUluZGV4KSwgZGF0YU1vZGVsLCBzZXJpZXNNb2RlbCAmJiAoc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbSB8fCB7fSkubW9kZWwsIHRoaXMuX3Rvb2x0aXBNb2RlbF0pO1xuICAgIHZhciB0b29sdGlwVHJpZ2dlciA9IHRvb2x0aXBNb2RlbC5nZXQoJ3RyaWdnZXInKTtcblxuICAgIGlmICh0b29sdGlwVHJpZ2dlciAhPSBudWxsICYmIHRvb2x0aXBUcmlnZ2VyICE9PSAnaXRlbScpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcGFyYW1zID0gZGF0YU1vZGVsLmdldERhdGFQYXJhbXMoZGF0YUluZGV4LCBkYXRhVHlwZSk7XG4gICAgdmFyIHNlcmllc1Rvb2x0aXAgPSBkYXRhTW9kZWwuZm9ybWF0VG9vbHRpcChkYXRhSW5kZXgsIGZhbHNlLCBkYXRhVHlwZSwgdGhpcy5fcmVuZGVyTW9kZSk7XG4gICAgdmFyIGRlZmF1bHRIdG1sO1xuICAgIHZhciBtYXJrZXJzO1xuXG4gICAgaWYgKHpyVXRpbC5pc09iamVjdChzZXJpZXNUb29sdGlwKSkge1xuICAgICAgZGVmYXVsdEh0bWwgPSBzZXJpZXNUb29sdGlwLmh0bWw7XG4gICAgICBtYXJrZXJzID0gc2VyaWVzVG9vbHRpcC5tYXJrZXJzO1xuICAgIH0gZWxzZSB7XG4gICAgICBkZWZhdWx0SHRtbCA9IHNlcmllc1Rvb2x0aXA7XG4gICAgICBtYXJrZXJzID0gbnVsbDtcbiAgICB9XG5cbiAgICB2YXIgYXN5bmNUaWNrZXQgPSAnaXRlbV8nICsgZGF0YU1vZGVsLm5hbWUgKyAnXycgKyBkYXRhSW5kZXg7XG5cbiAgICB0aGlzLl9zaG93T3JNb3ZlKHRvb2x0aXBNb2RlbCwgZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5fc2hvd1Rvb2x0aXBDb250ZW50KHRvb2x0aXBNb2RlbCwgZGVmYXVsdEh0bWwsIHBhcmFtcywgYXN5bmNUaWNrZXQsIGUub2Zmc2V0WCwgZS5vZmZzZXRZLCBlLnBvc2l0aW9uLCBlLnRhcmdldCwgbWFya2Vycyk7XG4gICAgfSk7IC8vIEZJWE1FXG4gICAgLy8gZHVwbGljYXRlZCBzaG93dGlwIGlmIG1hbnVhbGx5U2hvd1RpcCBpcyBjYWxsZWQgZnJvbSBkaXNwYXRjaEFjdGlvbi5cblxuXG4gICAgZGlzcGF0Y2hBY3Rpb24oe1xuICAgICAgdHlwZTogJ3Nob3dUaXAnLFxuICAgICAgZGF0YUluZGV4SW5zaWRlOiBkYXRhSW5kZXgsXG4gICAgICBkYXRhSW5kZXg6IGRhdGEuZ2V0UmF3SW5kZXgoZGF0YUluZGV4KSxcbiAgICAgIHNlcmllc0luZGV4OiBzZXJpZXNJbmRleCxcbiAgICAgIGZyb206IHRoaXMudWlkXG4gICAgfSk7XG4gIH0sXG4gIF9zaG93Q29tcG9uZW50SXRlbVRvb2x0aXA6IGZ1bmN0aW9uIChlLCBlbCwgZGlzcGF0Y2hBY3Rpb24pIHtcbiAgICB2YXIgdG9vbHRpcE9wdCA9IGVsLnRvb2x0aXA7XG5cbiAgICBpZiAodHlwZW9mIHRvb2x0aXBPcHQgPT09ICdzdHJpbmcnKSB7XG4gICAgICB2YXIgY29udGVudCA9IHRvb2x0aXBPcHQ7XG4gICAgICB0b29sdGlwT3B0ID0ge1xuICAgICAgICBjb250ZW50OiBjb250ZW50LFxuICAgICAgICAvLyBGaXhlZCBmb3JtYXR0ZXJcbiAgICAgICAgZm9ybWF0dGVyOiBjb250ZW50XG4gICAgICB9O1xuICAgIH1cblxuICAgIHZhciBzdWJUb29sdGlwTW9kZWwgPSBuZXcgTW9kZWwodG9vbHRpcE9wdCwgdGhpcy5fdG9vbHRpcE1vZGVsLCB0aGlzLl9lY01vZGVsKTtcbiAgICB2YXIgZGVmYXVsdEh0bWwgPSBzdWJUb29sdGlwTW9kZWwuZ2V0KCdjb250ZW50Jyk7XG4gICAgdmFyIGFzeW5jVGlja2V0ID0gTWF0aC5yYW5kb20oKTsgLy8gRG8gbm90IGNoZWNrIHdoZXRoZXIgYHRyaWdnZXJgIGlzICdub25lJyBoZXJlLCBiZWNhdXNlIGB0cmlnZ2VyYFxuICAgIC8vIG9ubHkgd29ya3Mgb24gY29vcmlkaW5hdGUgc3lzdGVtLiBJbiBmYWN0LCB3ZSBoYXZlIG5vdCBmb3VuZCBjYXNlXG4gICAgLy8gdGhhdCByZXF1aXJlcyBzZXR0aW5nIGB0cmlnZ2VyYCBub3RoaW5nIG9uIGNvbXBvbmVudCB5ZXQuXG5cbiAgICB0aGlzLl9zaG93T3JNb3ZlKHN1YlRvb2x0aXBNb2RlbCwgZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5fc2hvd1Rvb2x0aXBDb250ZW50KHN1YlRvb2x0aXBNb2RlbCwgZGVmYXVsdEh0bWwsIHN1YlRvb2x0aXBNb2RlbC5nZXQoJ2Zvcm1hdHRlclBhcmFtcycpIHx8IHt9LCBhc3luY1RpY2tldCwgZS5vZmZzZXRYLCBlLm9mZnNldFksIGUucG9zaXRpb24sIGVsKTtcbiAgICB9KTsgLy8gSWYgbm90IGRpc3BhdGNoIHNob3dUaXAsIHRpcCBtYXkgYmUgaGlkZSB0cmlnZ2VyZWQgYnkgYXhpcy5cblxuXG4gICAgZGlzcGF0Y2hBY3Rpb24oe1xuICAgICAgdHlwZTogJ3Nob3dUaXAnLFxuICAgICAgZnJvbTogdGhpcy51aWRcbiAgICB9KTtcbiAgfSxcbiAgX3Nob3dUb29sdGlwQ29udGVudDogZnVuY3Rpb24gKHRvb2x0aXBNb2RlbCwgZGVmYXVsdEh0bWwsIHBhcmFtcywgYXN5bmNUaWNrZXQsIHgsIHksIHBvc2l0aW9uRXhwciwgZWwsIG1hcmtlcnMpIHtcbiAgICAvLyBSZXNldCB0aWNrZXRcbiAgICB0aGlzLl90aWNrZXQgPSAnJztcblxuICAgIGlmICghdG9vbHRpcE1vZGVsLmdldCgnc2hvd0NvbnRlbnQnKSB8fCAhdG9vbHRpcE1vZGVsLmdldCgnc2hvdycpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHRvb2x0aXBDb250ZW50ID0gdGhpcy5fdG9vbHRpcENvbnRlbnQ7XG4gICAgdmFyIGZvcm1hdHRlciA9IHRvb2x0aXBNb2RlbC5nZXQoJ2Zvcm1hdHRlcicpO1xuICAgIHBvc2l0aW9uRXhwciA9IHBvc2l0aW9uRXhwciB8fCB0b29sdGlwTW9kZWwuZ2V0KCdwb3NpdGlvbicpO1xuICAgIHZhciBodG1sID0gZGVmYXVsdEh0bWw7XG5cbiAgICBpZiAoZm9ybWF0dGVyICYmIHR5cGVvZiBmb3JtYXR0ZXIgPT09ICdzdHJpbmcnKSB7XG4gICAgICBodG1sID0gZm9ybWF0VXRpbC5mb3JtYXRUcGwoZm9ybWF0dGVyLCBwYXJhbXMsIHRydWUpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGZvcm1hdHRlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdmFyIGNhbGxiYWNrID0gYmluZChmdW5jdGlvbiAoY2JUaWNrZXQsIGh0bWwpIHtcbiAgICAgICAgaWYgKGNiVGlja2V0ID09PSB0aGlzLl90aWNrZXQpIHtcbiAgICAgICAgICB0b29sdGlwQ29udGVudC5zZXRDb250ZW50KGh0bWwsIG1hcmtlcnMsIHRvb2x0aXBNb2RlbCk7XG5cbiAgICAgICAgICB0aGlzLl91cGRhdGVQb3NpdGlvbih0b29sdGlwTW9kZWwsIHBvc2l0aW9uRXhwciwgeCwgeSwgdG9vbHRpcENvbnRlbnQsIHBhcmFtcywgZWwpO1xuICAgICAgICB9XG4gICAgICB9LCB0aGlzKTtcbiAgICAgIHRoaXMuX3RpY2tldCA9IGFzeW5jVGlja2V0O1xuICAgICAgaHRtbCA9IGZvcm1hdHRlcihwYXJhbXMsIGFzeW5jVGlja2V0LCBjYWxsYmFjayk7XG4gICAgfVxuXG4gICAgdG9vbHRpcENvbnRlbnQuc2V0Q29udGVudChodG1sLCBtYXJrZXJzLCB0b29sdGlwTW9kZWwpO1xuICAgIHRvb2x0aXBDb250ZW50LnNob3codG9vbHRpcE1vZGVsKTtcblxuICAgIHRoaXMuX3VwZGF0ZVBvc2l0aW9uKHRvb2x0aXBNb2RlbCwgcG9zaXRpb25FeHByLCB4LCB5LCB0b29sdGlwQ29udGVudCwgcGFyYW1zLCBlbCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSAge3N0cmluZ3xGdW5jdGlvbnxBcnJheS48bnVtYmVyPnxPYmplY3R9IHBvc2l0aW9uRXhwclxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHggTW91c2UgeFxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHkgTW91c2UgeVxuICAgKiBAcGFyYW0gIHtib29sZWFufSBjb25maW5lIFdoZXRoZXIgY29uZmluZSB0b29sdGlwIGNvbnRlbnQgaW4gdmlldyByZWN0LlxuICAgKiBAcGFyYW0gIHtPYmplY3R8PEFycmF5LjxPYmplY3Q+fSBwYXJhbXNcbiAgICogQHBhcmFtICB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH0gZWwgdGFyZ2V0IGVsZW1lbnRcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvRXh0ZW5zaW9uQVBJfSBhcGlcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICBfdXBkYXRlUG9zaXRpb246IGZ1bmN0aW9uICh0b29sdGlwTW9kZWwsIHBvc2l0aW9uRXhwciwgeCwgeSwgY29udGVudCwgcGFyYW1zLCBlbCkge1xuICAgIHZhciB2aWV3V2lkdGggPSB0aGlzLl9hcGkuZ2V0V2lkdGgoKTtcblxuICAgIHZhciB2aWV3SGVpZ2h0ID0gdGhpcy5fYXBpLmdldEhlaWdodCgpO1xuXG4gICAgcG9zaXRpb25FeHByID0gcG9zaXRpb25FeHByIHx8IHRvb2x0aXBNb2RlbC5nZXQoJ3Bvc2l0aW9uJyk7XG4gICAgdmFyIGNvbnRlbnRTaXplID0gY29udGVudC5nZXRTaXplKCk7XG4gICAgdmFyIGFsaWduID0gdG9vbHRpcE1vZGVsLmdldCgnYWxpZ24nKTtcbiAgICB2YXIgdkFsaWduID0gdG9vbHRpcE1vZGVsLmdldCgndmVydGljYWxBbGlnbicpO1xuICAgIHZhciByZWN0ID0gZWwgJiYgZWwuZ2V0Qm91bmRpbmdSZWN0KCkuY2xvbmUoKTtcbiAgICBlbCAmJiByZWN0LmFwcGx5VHJhbnNmb3JtKGVsLnRyYW5zZm9ybSk7XG5cbiAgICBpZiAodHlwZW9mIHBvc2l0aW9uRXhwciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgLy8gQ2FsbGJhY2sgb2YgcG9zaXRpb24gY2FuIGJlIGFuIGFycmF5IG9yIGEgc3RyaW5nIHNwZWNpZnkgdGhlIHBvc2l0aW9uXG4gICAgICBwb3NpdGlvbkV4cHIgPSBwb3NpdGlvbkV4cHIoW3gsIHldLCBwYXJhbXMsIGNvbnRlbnQuZWwsIHJlY3QsIHtcbiAgICAgICAgdmlld1NpemU6IFt2aWV3V2lkdGgsIHZpZXdIZWlnaHRdLFxuICAgICAgICBjb250ZW50U2l6ZTogY29udGVudFNpemUuc2xpY2UoKVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHpyVXRpbC5pc0FycmF5KHBvc2l0aW9uRXhwcikpIHtcbiAgICAgIHggPSBwYXJzZVBlcmNlbnQocG9zaXRpb25FeHByWzBdLCB2aWV3V2lkdGgpO1xuICAgICAgeSA9IHBhcnNlUGVyY2VudChwb3NpdGlvbkV4cHJbMV0sIHZpZXdIZWlnaHQpO1xuICAgIH0gZWxzZSBpZiAoenJVdGlsLmlzT2JqZWN0KHBvc2l0aW9uRXhwcikpIHtcbiAgICAgIHBvc2l0aW9uRXhwci53aWR0aCA9IGNvbnRlbnRTaXplWzBdO1xuICAgICAgcG9zaXRpb25FeHByLmhlaWdodCA9IGNvbnRlbnRTaXplWzFdO1xuICAgICAgdmFyIGxheW91dFJlY3QgPSBsYXlvdXRVdGlsLmdldExheW91dFJlY3QocG9zaXRpb25FeHByLCB7XG4gICAgICAgIHdpZHRoOiB2aWV3V2lkdGgsXG4gICAgICAgIGhlaWdodDogdmlld0hlaWdodFxuICAgICAgfSk7XG4gICAgICB4ID0gbGF5b3V0UmVjdC54O1xuICAgICAgeSA9IGxheW91dFJlY3QueTtcbiAgICAgIGFsaWduID0gbnVsbDsgLy8gV2hlbiBwb3NpdGlvbkV4cHIgaXMgbGVmdC90b3AvcmlnaHQvYm90dG9tLFxuICAgICAgLy8gYWxpZ24gYW5kIHZlcnRpY2FsQWxpZ24gd2lsbCBub3Qgd29yay5cblxuICAgICAgdkFsaWduID0gbnVsbDtcbiAgICB9IC8vIFNwZWNpZnkgdG9vbHRpcCBwb3NpdGlvbiBieSBzdHJpbmcgJ3RvcCcgJ2JvdHRvbScgJ2xlZnQnICdyaWdodCcgYXJvdW5kIGdyYXBoaWMgZWxlbWVudFxuICAgIGVsc2UgaWYgKHR5cGVvZiBwb3NpdGlvbkV4cHIgPT09ICdzdHJpbmcnICYmIGVsKSB7XG4gICAgICAgIHZhciBwb3MgPSBjYWxjVG9vbHRpcFBvc2l0aW9uKHBvc2l0aW9uRXhwciwgcmVjdCwgY29udGVudFNpemUpO1xuICAgICAgICB4ID0gcG9zWzBdO1xuICAgICAgICB5ID0gcG9zWzFdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIHBvcyA9IHJlZml4VG9vbHRpcFBvc2l0aW9uKHgsIHksIGNvbnRlbnQsIHZpZXdXaWR0aCwgdmlld0hlaWdodCwgYWxpZ24gPyBudWxsIDogMjAsIHZBbGlnbiA/IG51bGwgOiAyMCk7XG4gICAgICAgIHggPSBwb3NbMF07XG4gICAgICAgIHkgPSBwb3NbMV07XG4gICAgICB9XG5cbiAgICBhbGlnbiAmJiAoeCAtPSBpc0NlbnRlckFsaWduKGFsaWduKSA/IGNvbnRlbnRTaXplWzBdIC8gMiA6IGFsaWduID09PSAncmlnaHQnID8gY29udGVudFNpemVbMF0gOiAwKTtcbiAgICB2QWxpZ24gJiYgKHkgLT0gaXNDZW50ZXJBbGlnbih2QWxpZ24pID8gY29udGVudFNpemVbMV0gLyAyIDogdkFsaWduID09PSAnYm90dG9tJyA/IGNvbnRlbnRTaXplWzFdIDogMCk7XG5cbiAgICBpZiAodG9vbHRpcE1vZGVsLmdldCgnY29uZmluZScpKSB7XG4gICAgICB2YXIgcG9zID0gY29uZmluZVRvb2x0aXBQb3NpdGlvbih4LCB5LCBjb250ZW50LCB2aWV3V2lkdGgsIHZpZXdIZWlnaHQpO1xuICAgICAgeCA9IHBvc1swXTtcbiAgICAgIHkgPSBwb3NbMV07XG4gICAgfVxuXG4gICAgY29udGVudC5tb3ZlVG8oeCwgeSk7XG4gIH0sXG4gIC8vIEZJWE1FXG4gIC8vIFNob3VsZCB3ZSByZW1vdmUgdGhpcyBidXQgbGVhdmUgdGhpcyB0byB1c2VyP1xuICBfdXBkYXRlQ29udGVudE5vdENoYW5nZWRPbkF4aXM6IGZ1bmN0aW9uIChkYXRhQnlDb29yZFN5cykge1xuICAgIHZhciBsYXN0Q29vcmRTeXMgPSB0aGlzLl9sYXN0RGF0YUJ5Q29vcmRTeXM7XG4gICAgdmFyIGNvbnRlbnROb3RDaGFuZ2VkID0gISFsYXN0Q29vcmRTeXMgJiYgbGFzdENvb3JkU3lzLmxlbmd0aCA9PT0gZGF0YUJ5Q29vcmRTeXMubGVuZ3RoO1xuICAgIGNvbnRlbnROb3RDaGFuZ2VkICYmIGVhY2gobGFzdENvb3JkU3lzLCBmdW5jdGlvbiAobGFzdEl0ZW1Db29yZFN5cywgaW5kZXhDb29yZFN5cykge1xuICAgICAgdmFyIGxhc3REYXRhQnlBeGlzID0gbGFzdEl0ZW1Db29yZFN5cy5kYXRhQnlBeGlzIHx8IHt9O1xuICAgICAgdmFyIHRoaXNJdGVtQ29vcmRTeXMgPSBkYXRhQnlDb29yZFN5c1tpbmRleENvb3JkU3lzXSB8fCB7fTtcbiAgICAgIHZhciB0aGlzRGF0YUJ5QXhpcyA9IHRoaXNJdGVtQ29vcmRTeXMuZGF0YUJ5QXhpcyB8fCBbXTtcbiAgICAgIGNvbnRlbnROb3RDaGFuZ2VkICY9IGxhc3REYXRhQnlBeGlzLmxlbmd0aCA9PT0gdGhpc0RhdGFCeUF4aXMubGVuZ3RoO1xuICAgICAgY29udGVudE5vdENoYW5nZWQgJiYgZWFjaChsYXN0RGF0YUJ5QXhpcywgZnVuY3Rpb24gKGxhc3RJdGVtLCBpbmRleEF4aXMpIHtcbiAgICAgICAgdmFyIHRoaXNJdGVtID0gdGhpc0RhdGFCeUF4aXNbaW5kZXhBeGlzXSB8fCB7fTtcbiAgICAgICAgdmFyIGxhc3RJbmRpY2VzID0gbGFzdEl0ZW0uc2VyaWVzRGF0YUluZGljZXMgfHwgW107XG4gICAgICAgIHZhciBuZXdJbmRpY2VzID0gdGhpc0l0ZW0uc2VyaWVzRGF0YUluZGljZXMgfHwgW107XG4gICAgICAgIGNvbnRlbnROb3RDaGFuZ2VkICY9IGxhc3RJdGVtLnZhbHVlID09PSB0aGlzSXRlbS52YWx1ZSAmJiBsYXN0SXRlbS5heGlzVHlwZSA9PT0gdGhpc0l0ZW0uYXhpc1R5cGUgJiYgbGFzdEl0ZW0uYXhpc0lkID09PSB0aGlzSXRlbS5heGlzSWQgJiYgbGFzdEluZGljZXMubGVuZ3RoID09PSBuZXdJbmRpY2VzLmxlbmd0aDtcbiAgICAgICAgY29udGVudE5vdENoYW5nZWQgJiYgZWFjaChsYXN0SW5kaWNlcywgZnVuY3Rpb24gKGxhc3RJZHhJdGVtLCBqKSB7XG4gICAgICAgICAgdmFyIG5ld0lkeEl0ZW0gPSBuZXdJbmRpY2VzW2pdO1xuICAgICAgICAgIGNvbnRlbnROb3RDaGFuZ2VkICY9IGxhc3RJZHhJdGVtLnNlcmllc0luZGV4ID09PSBuZXdJZHhJdGVtLnNlcmllc0luZGV4ICYmIGxhc3RJZHhJdGVtLmRhdGFJbmRleCA9PT0gbmV3SWR4SXRlbS5kYXRhSW5kZXg7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfSk7XG4gICAgdGhpcy5fbGFzdERhdGFCeUNvb3JkU3lzID0gZGF0YUJ5Q29vcmRTeXM7XG4gICAgcmV0dXJuICEhY29udGVudE5vdENoYW5nZWQ7XG4gIH0sXG4gIF9oaWRlOiBmdW5jdGlvbiAoZGlzcGF0Y2hBY3Rpb24pIHtcbiAgICAvLyBEbyBub3QgZGlyZWN0bHkgaGlkZUxhdGVyIGhlcmUsIGJlY2F1c2UgdGhpcyBiZWhhdmlvciBtYXkgYmUgcHJldmVudGVkXG4gICAgLy8gaW4gZGlzcGF0Y2hBY3Rpb24gd2hlbiBzaG93VGlwIGlzIGRpc3BhdGNoZWQuXG4gICAgLy8gRklYTUVcbiAgICAvLyBkdXBsaWNhdGVkIGhpZGVUaXAgaWYgbWFudWFsbHlIaWRlVGlwIGlzIGNhbGxlZCBmcm9tIGRpc3BhdGNoQWN0aW9uLlxuICAgIHRoaXMuX2xhc3REYXRhQnlDb29yZFN5cyA9IG51bGw7XG4gICAgZGlzcGF0Y2hBY3Rpb24oe1xuICAgICAgdHlwZTogJ2hpZGVUaXAnLFxuICAgICAgZnJvbTogdGhpcy51aWRcbiAgICB9KTtcbiAgfSxcbiAgZGlzcG9zZTogZnVuY3Rpb24gKGVjTW9kZWwsIGFwaSkge1xuICAgIGlmIChlbnYubm9kZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuX3Rvb2x0aXBDb250ZW50LmhpZGUoKTtcblxuICAgIGdsb2JhbExpc3RlbmVyLnVucmVnaXN0ZXIoJ2l0ZW1Ub29sdGlwJywgYXBpKTtcbiAgfVxufSk7XG4vKipcbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdHxtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbD59IG1vZGVsQ2FzY2FkZVxuICogRnJvbSB0b3AgdG8gYm90dG9tLiAodGhlIGxhc3Qgb25lIHNob3VsZCBiZSBnbG9iYWxUb29sdGlwTW9kZWwpO1xuICovXG5cblxuZnVuY3Rpb24gYnVpbGRUb29sdGlwTW9kZWwobW9kZWxDYXNjYWRlKSB7XG4gIHZhciByZXN1bHRNb2RlbCA9IG1vZGVsQ2FzY2FkZS5wb3AoKTtcblxuICB3aGlsZSAobW9kZWxDYXNjYWRlLmxlbmd0aCkge1xuICAgIHZhciB0b29sdGlwT3B0ID0gbW9kZWxDYXNjYWRlLnBvcCgpO1xuXG4gICAgaWYgKHRvb2x0aXBPcHQpIHtcbiAgICAgIGlmIChNb2RlbC5pc0luc3RhbmNlKHRvb2x0aXBPcHQpKSB7XG4gICAgICAgIHRvb2x0aXBPcHQgPSB0b29sdGlwT3B0LmdldCgndG9vbHRpcCcsIHRydWUpO1xuICAgICAgfSAvLyBJbiBlYWNoIGRhdGEgaXRlbSB0b29sdGlwIGNhbiBiZSBzaW1wbHkgd3JpdGU6XG4gICAgICAvLyB7XG4gICAgICAvLyAgdmFsdWU6IDEwLFxuICAgICAgLy8gIHRvb2x0aXA6ICdTb21ldGhpbmcgeW91IG5lZWQgdG8ga25vdydcbiAgICAgIC8vIH1cblxuXG4gICAgICBpZiAodHlwZW9mIHRvb2x0aXBPcHQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHRvb2x0aXBPcHQgPSB7XG4gICAgICAgICAgZm9ybWF0dGVyOiB0b29sdGlwT3B0XG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIHJlc3VsdE1vZGVsID0gbmV3IE1vZGVsKHRvb2x0aXBPcHQsIHJlc3VsdE1vZGVsLCByZXN1bHRNb2RlbC5lY01vZGVsKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmVzdWx0TW9kZWw7XG59XG5cbmZ1bmN0aW9uIG1ha2VEaXNwYXRjaEFjdGlvbihwYXlsb2FkLCBhcGkpIHtcbiAgcmV0dXJuIHBheWxvYWQuZGlzcGF0Y2hBY3Rpb24gfHwgenJVdGlsLmJpbmQoYXBpLmRpc3BhdGNoQWN0aW9uLCBhcGkpO1xufVxuXG5mdW5jdGlvbiByZWZpeFRvb2x0aXBQb3NpdGlvbih4LCB5LCBjb250ZW50LCB2aWV3V2lkdGgsIHZpZXdIZWlnaHQsIGdhcEgsIGdhcFYpIHtcbiAgdmFyIHNpemUgPSBjb250ZW50LmdldE91dGVyU2l6ZSgpO1xuICB2YXIgd2lkdGggPSBzaXplLndpZHRoO1xuICB2YXIgaGVpZ2h0ID0gc2l6ZS5oZWlnaHQ7XG5cbiAgaWYgKGdhcEggIT0gbnVsbCkge1xuICAgIGlmICh4ICsgd2lkdGggKyBnYXBIID4gdmlld1dpZHRoKSB7XG4gICAgICB4IC09IHdpZHRoICsgZ2FwSDtcbiAgICB9IGVsc2Uge1xuICAgICAgeCArPSBnYXBIO1xuICAgIH1cbiAgfVxuXG4gIGlmIChnYXBWICE9IG51bGwpIHtcbiAgICBpZiAoeSArIGhlaWdodCArIGdhcFYgPiB2aWV3SGVpZ2h0KSB7XG4gICAgICB5IC09IGhlaWdodCArIGdhcFY7XG4gICAgfSBlbHNlIHtcbiAgICAgIHkgKz0gZ2FwVjtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gW3gsIHldO1xufVxuXG5mdW5jdGlvbiBjb25maW5lVG9vbHRpcFBvc2l0aW9uKHgsIHksIGNvbnRlbnQsIHZpZXdXaWR0aCwgdmlld0hlaWdodCkge1xuICB2YXIgc2l6ZSA9IGNvbnRlbnQuZ2V0T3V0ZXJTaXplKCk7XG4gIHZhciB3aWR0aCA9IHNpemUud2lkdGg7XG4gIHZhciBoZWlnaHQgPSBzaXplLmhlaWdodDtcbiAgeCA9IE1hdGgubWluKHggKyB3aWR0aCwgdmlld1dpZHRoKSAtIHdpZHRoO1xuICB5ID0gTWF0aC5taW4oeSArIGhlaWdodCwgdmlld0hlaWdodCkgLSBoZWlnaHQ7XG4gIHggPSBNYXRoLm1heCh4LCAwKTtcbiAgeSA9IE1hdGgubWF4KHksIDApO1xuICByZXR1cm4gW3gsIHldO1xufVxuXG5mdW5jdGlvbiBjYWxjVG9vbHRpcFBvc2l0aW9uKHBvc2l0aW9uLCByZWN0LCBjb250ZW50U2l6ZSkge1xuICB2YXIgZG9tV2lkdGggPSBjb250ZW50U2l6ZVswXTtcbiAgdmFyIGRvbUhlaWdodCA9IGNvbnRlbnRTaXplWzFdO1xuICB2YXIgZ2FwID0gNTtcbiAgdmFyIHggPSAwO1xuICB2YXIgeSA9IDA7XG4gIHZhciByZWN0V2lkdGggPSByZWN0LndpZHRoO1xuICB2YXIgcmVjdEhlaWdodCA9IHJlY3QuaGVpZ2h0O1xuXG4gIHN3aXRjaCAocG9zaXRpb24pIHtcbiAgICBjYXNlICdpbnNpZGUnOlxuICAgICAgeCA9IHJlY3QueCArIHJlY3RXaWR0aCAvIDIgLSBkb21XaWR0aCAvIDI7XG4gICAgICB5ID0gcmVjdC55ICsgcmVjdEhlaWdodCAvIDIgLSBkb21IZWlnaHQgLyAyO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICd0b3AnOlxuICAgICAgeCA9IHJlY3QueCArIHJlY3RXaWR0aCAvIDIgLSBkb21XaWR0aCAvIDI7XG4gICAgICB5ID0gcmVjdC55IC0gZG9tSGVpZ2h0IC0gZ2FwO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdib3R0b20nOlxuICAgICAgeCA9IHJlY3QueCArIHJlY3RXaWR0aCAvIDIgLSBkb21XaWR0aCAvIDI7XG4gICAgICB5ID0gcmVjdC55ICsgcmVjdEhlaWdodCArIGdhcDtcbiAgICAgIGJyZWFrO1xuXG4gICAgY2FzZSAnbGVmdCc6XG4gICAgICB4ID0gcmVjdC54IC0gZG9tV2lkdGggLSBnYXA7XG4gICAgICB5ID0gcmVjdC55ICsgcmVjdEhlaWdodCAvIDIgLSBkb21IZWlnaHQgLyAyO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdyaWdodCc6XG4gICAgICB4ID0gcmVjdC54ICsgcmVjdFdpZHRoICsgZ2FwO1xuICAgICAgeSA9IHJlY3QueSArIHJlY3RIZWlnaHQgLyAyIC0gZG9tSGVpZ2h0IC8gMjtcbiAgfVxuXG4gIHJldHVybiBbeCwgeV07XG59XG5cbmZ1bmN0aW9uIGlzQ2VudGVyQWxpZ24oYWxpZ24pIHtcbiAgcmV0dXJuIGFsaWduID09PSAnY2VudGVyJyB8fCBhbGlnbiA9PT0gJ21pZGRsZSc7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7Ozs7QUFNQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJrQkE7QUF1a0JBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdkJBO0FBQ0E7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/tooltip/TooltipView.js
