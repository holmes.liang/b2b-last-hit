/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule randomizeBlockMapKeys
 * @format
 * 
 */


var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var OrderedMap = Immutable.OrderedMap;

var randomizeContentBlockNodeKeys = function randomizeContentBlockNodeKeys(blockMap) {
  var newKeysRef = {}; // we keep track of root blocks in order to update subsequent sibling links

  var lastRootBlock = void 0;
  return OrderedMap(blockMap.withMutations(function (blockMapState) {
    blockMapState.forEach(function (block, index) {
      var oldKey = block.getKey();
      var nextKey = block.getNextSiblingKey();
      var prevKey = block.getPrevSiblingKey();
      var childrenKeys = block.getChildKeys();
      var parentKey = block.getParentKey(); // new key that we will use to build linking

      var key = generateRandomKey(); // we will add it here to re-use it later

      newKeysRef[oldKey] = key;

      if (nextKey) {
        var nextBlock = blockMapState.get(nextKey);

        if (nextBlock) {
          blockMapState.setIn([nextKey, 'prevSibling'], key);
        } else {
          // this can happen when generating random keys for fragments
          blockMapState.setIn([oldKey, 'nextSibling'], null);
        }
      }

      if (prevKey) {
        var prevBlock = blockMapState.get(prevKey);

        if (prevBlock) {
          blockMapState.setIn([prevKey, 'nextSibling'], key);
        } else {
          // this can happen when generating random keys for fragments
          blockMapState.setIn([oldKey, 'prevSibling'], null);
        }
      }

      if (parentKey && blockMapState.get(parentKey)) {
        var parentBlock = blockMapState.get(parentKey);
        var parentChildrenList = parentBlock.getChildKeys();
        blockMapState.setIn([parentKey, 'children'], parentChildrenList.set(parentChildrenList.indexOf(block.getKey()), key));
      } else {
        // blocks will then be treated as root block nodes
        blockMapState.setIn([oldKey, 'parent'], null);

        if (lastRootBlock) {
          blockMapState.setIn([lastRootBlock.getKey(), 'nextSibling'], key);
          blockMapState.setIn([oldKey, 'prevSibling'], newKeysRef[lastRootBlock.getKey()]);
        }

        lastRootBlock = blockMapState.get(oldKey);
      }

      childrenKeys.forEach(function (childKey) {
        var childBlock = blockMapState.get(childKey);

        if (childBlock) {
          blockMapState.setIn([childKey, 'parent'], key);
        } else {
          blockMapState.setIn([oldKey, 'children'], block.getChildKeys().filter(function (child) {
            return child !== childKey;
          }));
        }
      });
    });
  }).toArray().map(function (block) {
    return [newKeysRef[block.getKey()], block.set('key', newKeysRef[block.getKey()])];
  }));
};

var randomizeContentBlockKeys = function randomizeContentBlockKeys(blockMap) {
  return OrderedMap(blockMap.toArray().map(function (block) {
    var key = generateRandomKey();
    return [key, block.set('key', key)];
  }));
};

var randomizeBlockMapKeys = function randomizeBlockMapKeys(blockMap) {
  var isTreeBasedBlockMap = blockMap.first() instanceof ContentBlockNode;

  if (!isTreeBasedBlockMap) {
    return randomizeContentBlockKeys(blockMap);
  }

  return randomizeContentBlockNodeKeys(blockMap);
};

module.exports = randomizeBlockMapKeys;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3JhbmRvbWl6ZUJsb2NrTWFwS2V5cy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9yYW5kb21pemVCbG9ja01hcEtleXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSByYW5kb21pemVCbG9ja01hcEtleXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcblxudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xuXG52YXIgT3JkZXJlZE1hcCA9IEltbXV0YWJsZS5PcmRlcmVkTWFwO1xuXG5cbnZhciByYW5kb21pemVDb250ZW50QmxvY2tOb2RlS2V5cyA9IGZ1bmN0aW9uIHJhbmRvbWl6ZUNvbnRlbnRCbG9ja05vZGVLZXlzKGJsb2NrTWFwKSB7XG4gIHZhciBuZXdLZXlzUmVmID0ge307XG5cbiAgLy8gd2Uga2VlcCB0cmFjayBvZiByb290IGJsb2NrcyBpbiBvcmRlciB0byB1cGRhdGUgc3Vic2VxdWVudCBzaWJsaW5nIGxpbmtzXG4gIHZhciBsYXN0Um9vdEJsb2NrID0gdm9pZCAwO1xuXG4gIHJldHVybiBPcmRlcmVkTWFwKGJsb2NrTWFwLndpdGhNdXRhdGlvbnMoZnVuY3Rpb24gKGJsb2NrTWFwU3RhdGUpIHtcbiAgICBibG9ja01hcFN0YXRlLmZvckVhY2goZnVuY3Rpb24gKGJsb2NrLCBpbmRleCkge1xuICAgICAgdmFyIG9sZEtleSA9IGJsb2NrLmdldEtleSgpO1xuICAgICAgdmFyIG5leHRLZXkgPSBibG9jay5nZXROZXh0U2libGluZ0tleSgpO1xuICAgICAgdmFyIHByZXZLZXkgPSBibG9jay5nZXRQcmV2U2libGluZ0tleSgpO1xuICAgICAgdmFyIGNoaWxkcmVuS2V5cyA9IGJsb2NrLmdldENoaWxkS2V5cygpO1xuICAgICAgdmFyIHBhcmVudEtleSA9IGJsb2NrLmdldFBhcmVudEtleSgpO1xuXG4gICAgICAvLyBuZXcga2V5IHRoYXQgd2Ugd2lsbCB1c2UgdG8gYnVpbGQgbGlua2luZ1xuICAgICAgdmFyIGtleSA9IGdlbmVyYXRlUmFuZG9tS2V5KCk7XG5cbiAgICAgIC8vIHdlIHdpbGwgYWRkIGl0IGhlcmUgdG8gcmUtdXNlIGl0IGxhdGVyXG4gICAgICBuZXdLZXlzUmVmW29sZEtleV0gPSBrZXk7XG5cbiAgICAgIGlmIChuZXh0S2V5KSB7XG4gICAgICAgIHZhciBuZXh0QmxvY2sgPSBibG9ja01hcFN0YXRlLmdldChuZXh0S2V5KTtcbiAgICAgICAgaWYgKG5leHRCbG9jaykge1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW25leHRLZXksICdwcmV2U2libGluZyddLCBrZXkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIHRoaXMgY2FuIGhhcHBlbiB3aGVuIGdlbmVyYXRpbmcgcmFuZG9tIGtleXMgZm9yIGZyYWdtZW50c1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW29sZEtleSwgJ25leHRTaWJsaW5nJ10sIG51bGwpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChwcmV2S2V5KSB7XG4gICAgICAgIHZhciBwcmV2QmxvY2sgPSBibG9ja01hcFN0YXRlLmdldChwcmV2S2V5KTtcbiAgICAgICAgaWYgKHByZXZCbG9jaykge1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW3ByZXZLZXksICduZXh0U2libGluZyddLCBrZXkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIHRoaXMgY2FuIGhhcHBlbiB3aGVuIGdlbmVyYXRpbmcgcmFuZG9tIGtleXMgZm9yIGZyYWdtZW50c1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW29sZEtleSwgJ3ByZXZTaWJsaW5nJ10sIG51bGwpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChwYXJlbnRLZXkgJiYgYmxvY2tNYXBTdGF0ZS5nZXQocGFyZW50S2V5KSkge1xuICAgICAgICB2YXIgcGFyZW50QmxvY2sgPSBibG9ja01hcFN0YXRlLmdldChwYXJlbnRLZXkpO1xuICAgICAgICB2YXIgcGFyZW50Q2hpbGRyZW5MaXN0ID0gcGFyZW50QmxvY2suZ2V0Q2hpbGRLZXlzKCk7XG4gICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW3BhcmVudEtleSwgJ2NoaWxkcmVuJ10sIHBhcmVudENoaWxkcmVuTGlzdC5zZXQocGFyZW50Q2hpbGRyZW5MaXN0LmluZGV4T2YoYmxvY2suZ2V0S2V5KCkpLCBrZXkpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIGJsb2NrcyB3aWxsIHRoZW4gYmUgdHJlYXRlZCBhcyByb290IGJsb2NrIG5vZGVzXG4gICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW29sZEtleSwgJ3BhcmVudCddLCBudWxsKTtcblxuICAgICAgICBpZiAobGFzdFJvb3RCbG9jaykge1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW2xhc3RSb290QmxvY2suZ2V0S2V5KCksICduZXh0U2libGluZyddLCBrZXkpO1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW29sZEtleSwgJ3ByZXZTaWJsaW5nJ10sIG5ld0tleXNSZWZbbGFzdFJvb3RCbG9jay5nZXRLZXkoKV0pO1xuICAgICAgICB9XG5cbiAgICAgICAgbGFzdFJvb3RCbG9jayA9IGJsb2NrTWFwU3RhdGUuZ2V0KG9sZEtleSk7XG4gICAgICB9XG5cbiAgICAgIGNoaWxkcmVuS2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZEtleSkge1xuICAgICAgICB2YXIgY2hpbGRCbG9jayA9IGJsb2NrTWFwU3RhdGUuZ2V0KGNoaWxkS2V5KTtcbiAgICAgICAgaWYgKGNoaWxkQmxvY2spIHtcbiAgICAgICAgICBibG9ja01hcFN0YXRlLnNldEluKFtjaGlsZEtleSwgJ3BhcmVudCddLCBrZXkpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGJsb2NrTWFwU3RhdGUuc2V0SW4oW29sZEtleSwgJ2NoaWxkcmVuJ10sIGJsb2NrLmdldENoaWxkS2V5cygpLmZpbHRlcihmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICAgIHJldHVybiBjaGlsZCAhPT0gY2hpbGRLZXk7XG4gICAgICAgICAgfSkpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfSkudG9BcnJheSgpLm1hcChmdW5jdGlvbiAoYmxvY2spIHtcbiAgICByZXR1cm4gW25ld0tleXNSZWZbYmxvY2suZ2V0S2V5KCldLCBibG9jay5zZXQoJ2tleScsIG5ld0tleXNSZWZbYmxvY2suZ2V0S2V5KCldKV07XG4gIH0pKTtcbn07XG5cbnZhciByYW5kb21pemVDb250ZW50QmxvY2tLZXlzID0gZnVuY3Rpb24gcmFuZG9taXplQ29udGVudEJsb2NrS2V5cyhibG9ja01hcCkge1xuICByZXR1cm4gT3JkZXJlZE1hcChibG9ja01hcC50b0FycmF5KCkubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBrZXkgPSBnZW5lcmF0ZVJhbmRvbUtleSgpO1xuICAgIHJldHVybiBba2V5LCBibG9jay5zZXQoJ2tleScsIGtleSldO1xuICB9KSk7XG59O1xuXG52YXIgcmFuZG9taXplQmxvY2tNYXBLZXlzID0gZnVuY3Rpb24gcmFuZG9taXplQmxvY2tNYXBLZXlzKGJsb2NrTWFwKSB7XG4gIHZhciBpc1RyZWVCYXNlZEJsb2NrTWFwID0gYmxvY2tNYXAuZmlyc3QoKSBpbnN0YW5jZW9mIENvbnRlbnRCbG9ja05vZGU7XG5cbiAgaWYgKCFpc1RyZWVCYXNlZEJsb2NrTWFwKSB7XG4gICAgcmV0dXJuIHJhbmRvbWl6ZUNvbnRlbnRCbG9ja0tleXMoYmxvY2tNYXApO1xuICB9XG5cbiAgcmV0dXJuIHJhbmRvbWl6ZUNvbnRlbnRCbG9ja05vZGVLZXlzKGJsb2NrTWFwKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gcmFuZG9taXplQmxvY2tNYXBLZXlzOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/randomizeBlockMapKeys.js
