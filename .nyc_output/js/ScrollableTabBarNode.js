__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var resize_observer_polyfill__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! resize-observer-polyfill */ "./node_modules/resize-observer-polyfill/dist/ResizeObserver.es.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-tabs/es/utils.js");












var ScrollableTabBarNode = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(ScrollableTabBarNode, _React$Component);

  function ScrollableTabBarNode(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, ScrollableTabBarNode);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (ScrollableTabBarNode.__proto__ || Object.getPrototypeOf(ScrollableTabBarNode)).call(this, props));

    _this.prevTransitionEnd = function (e) {
      if (e.propertyName !== 'opacity') {
        return;
      }

      var container = _this.props.getRef('container');

      _this.scrollToActiveTab({
        target: container,
        currentTarget: container
      });
    };

    _this.scrollToActiveTab = function (e) {
      var activeTab = _this.props.getRef('activeTab');

      var navWrap = _this.props.getRef('navWrap');

      if (e && e.target !== e.currentTarget || !activeTab) {
        return;
      } // when not scrollable or enter scrollable first time, don't emit scrolling


      var needToSroll = _this.isNextPrevShown() && _this.lastNextPrevShown;

      _this.lastNextPrevShown = _this.isNextPrevShown();

      if (!needToSroll) {
        return;
      }

      var activeTabWH = _this.getScrollWH(activeTab);

      var navWrapNodeWH = _this.getOffsetWH(navWrap);

      var offset = _this.offset;

      var wrapOffset = _this.getOffsetLT(navWrap);

      var activeTabOffset = _this.getOffsetLT(activeTab);

      if (wrapOffset > activeTabOffset) {
        offset += wrapOffset - activeTabOffset;

        _this.setOffset(offset);
      } else if (wrapOffset + navWrapNodeWH < activeTabOffset + activeTabWH) {
        offset -= activeTabOffset + activeTabWH - (wrapOffset + navWrapNodeWH);

        _this.setOffset(offset);
      }
    };

    _this.prev = function (e) {
      _this.props.onPrevClick(e);

      var navWrapNode = _this.props.getRef('navWrap');

      var navWrapNodeWH = _this.getOffsetWH(navWrapNode);

      var offset = _this.offset;

      _this.setOffset(offset + navWrapNodeWH);
    };

    _this.next = function (e) {
      _this.props.onNextClick(e);

      var navWrapNode = _this.props.getRef('navWrap');

      var navWrapNodeWH = _this.getOffsetWH(navWrapNode);

      var offset = _this.offset;

      _this.setOffset(offset - navWrapNodeWH);
    };

    _this.offset = 0;
    _this.state = {
      next: false,
      prev: false
    };
    return _this;
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(ScrollableTabBarNode, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      this.componentDidUpdate();
      this.debouncedResize = lodash_debounce__WEBPACK_IMPORTED_MODULE_8___default()(function () {
        _this2.setNextPrev();

        _this2.scrollToActiveTab();
      }, 200);
      this.resizeObserver = new resize_observer_polyfill__WEBPACK_IMPORTED_MODULE_9__["default"](this.debouncedResize);
      this.resizeObserver.observe(this.props.getRef('container'));
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      var props = this.props;

      if (prevProps && prevProps.tabBarPosition !== props.tabBarPosition) {
        this.setOffset(0);
        return;
      }

      var nextPrev = this.setNextPrev(); // wait next, prev show hide

      /* eslint react/no-did-update-set-state:0 */

      if (this.isNextPrevShown(this.state) !== this.isNextPrevShown(nextPrev)) {
        this.setState({}, this.scrollToActiveTab);
      } else if (!prevProps || props.activeKey !== prevProps.activeKey) {
        // can not use props.activeKey
        this.scrollToActiveTab();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.resizeObserver) {
        this.resizeObserver.disconnect();
      }

      if (this.debouncedResize && this.debouncedResize.cancel) {
        this.debouncedResize.cancel();
      }
    }
  }, {
    key: 'setNextPrev',
    value: function setNextPrev() {
      var navNode = this.props.getRef('nav');
      var navTabsContainer = this.props.getRef('navTabsContainer');
      var navNodeWH = this.getScrollWH(navTabsContainer || navNode); // Add 1px to fix `offsetWidth` with decimal in Chrome not correct handle
      // https://github.com/ant-design/ant-design/issues/13423

      var containerWH = this.getOffsetWH(this.props.getRef('container')) + 1;
      var navWrapNodeWH = this.getOffsetWH(this.props.getRef('navWrap'));
      var offset = this.offset;
      var minOffset = containerWH - navNodeWH;
      var _state = this.state,
          next = _state.next,
          prev = _state.prev;

      if (minOffset >= 0) {
        next = false;
        this.setOffset(0, false);
        offset = 0;
      } else if (minOffset < offset) {
        next = true;
      } else {
        next = false; // Fix https://github.com/ant-design/ant-design/issues/8861
        // Test with container offset which is stable
        // and set the offset of the nav wrap node

        var realOffset = navWrapNodeWH - navNodeWH;
        this.setOffset(realOffset, false);
        offset = realOffset;
      }

      if (offset < 0) {
        prev = true;
      } else {
        prev = false;
      }

      this.setNext(next);
      this.setPrev(prev);
      return {
        next: next,
        prev: prev
      };
    }
  }, {
    key: 'getOffsetWH',
    value: function getOffsetWH(node) {
      var tabBarPosition = this.props.tabBarPosition;
      var prop = 'offsetWidth';

      if (tabBarPosition === 'left' || tabBarPosition === 'right') {
        prop = 'offsetHeight';
      }

      return node[prop];
    }
  }, {
    key: 'getScrollWH',
    value: function getScrollWH(node) {
      var tabBarPosition = this.props.tabBarPosition;
      var prop = 'scrollWidth';

      if (tabBarPosition === 'left' || tabBarPosition === 'right') {
        prop = 'scrollHeight';
      }

      return node[prop];
    }
  }, {
    key: 'getOffsetLT',
    value: function getOffsetLT(node) {
      var tabBarPosition = this.props.tabBarPosition;
      var prop = 'left';

      if (tabBarPosition === 'left' || tabBarPosition === 'right') {
        prop = 'top';
      }

      return node.getBoundingClientRect()[prop];
    }
  }, {
    key: 'setOffset',
    value: function setOffset(offset) {
      var checkNextPrev = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      var target = Math.min(0, offset);

      if (this.offset !== target) {
        this.offset = target;
        var navOffset = {};
        var tabBarPosition = this.props.tabBarPosition;
        var navStyle = this.props.getRef('nav').style;
        var transformSupported = Object(_utils__WEBPACK_IMPORTED_MODULE_10__["isTransform3dSupported"])(navStyle);

        if (tabBarPosition === 'left' || tabBarPosition === 'right') {
          if (transformSupported) {
            navOffset = {
              value: 'translate3d(0,' + target + 'px,0)'
            };
          } else {
            navOffset = {
              name: 'top',
              value: target + 'px'
            };
          }
        } else if (transformSupported) {
          if (this.props.direction === 'rtl') {
            target = -target;
          }

          navOffset = {
            value: 'translate3d(' + target + 'px,0,0)'
          };
        } else {
          navOffset = {
            name: 'left',
            value: target + 'px'
          };
        }

        if (transformSupported) {
          Object(_utils__WEBPACK_IMPORTED_MODULE_10__["setTransform"])(navStyle, navOffset.value);
        } else {
          navStyle[navOffset.name] = navOffset.value;
        }

        if (checkNextPrev) {
          this.setNextPrev();
        }
      }
    }
  }, {
    key: 'setPrev',
    value: function setPrev(v) {
      if (this.state.prev !== v) {
        this.setState({
          prev: v
        });
      }
    }
  }, {
    key: 'setNext',
    value: function setNext(v) {
      if (this.state.next !== v) {
        this.setState({
          next: v
        });
      }
    }
  }, {
    key: 'isNextPrevShown',
    value: function isNextPrevShown(state) {
      if (state) {
        return state.next || state.prev;
      }

      return this.state.next || this.state.prev;
    }
  }, {
    key: 'render',
    value: function render() {
      var _classnames, _classnames2, _classnames3, _classnames4;

      var _state2 = this.state,
          next = _state2.next,
          prev = _state2.prev;
      var _props = this.props,
          prefixCls = _props.prefixCls,
          scrollAnimated = _props.scrollAnimated,
          navWrapper = _props.navWrapper,
          prevIcon = _props.prevIcon,
          nextIcon = _props.nextIcon;
      var showNextPrev = prev || next;
      var prevButton = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
        onClick: prev ? this.prev : null,
        unselectable: 'unselectable',
        className: classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classnames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames, prefixCls + '-tab-prev', 1), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames, prefixCls + '-tab-btn-disabled', !prev), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames, prefixCls + '-tab-arrow-show', showNextPrev), _classnames)),
        onTransitionEnd: this.prevTransitionEnd
      }, prevIcon || react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
        className: prefixCls + '-tab-prev-icon'
      }));
      var nextButton = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
        onClick: next ? this.next : null,
        unselectable: 'unselectable',
        className: classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classnames2 = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames2, prefixCls + '-tab-next', 1), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames2, prefixCls + '-tab-btn-disabled', !next), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames2, prefixCls + '-tab-arrow-show', showNextPrev), _classnames2))
      }, nextIcon || react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
        className: prefixCls + '-tab-next-icon'
      }));
      var navClassName = prefixCls + '-nav';
      var navClasses = classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classnames3 = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames3, navClassName, true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames3, scrollAnimated ? navClassName + '-animated' : navClassName + '-no-animated', true), _classnames3));
      return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
        className: classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classnames4 = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames4, prefixCls + '-nav-container', 1), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classnames4, prefixCls + '-nav-container-scrolling', showNextPrev), _classnames4)),
        key: 'container',
        ref: this.props.saveRef('container')
      }, prevButton, nextButton, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
        className: prefixCls + '-nav-wrap',
        ref: this.props.saveRef('navWrap')
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
        className: prefixCls + '-nav-scroll'
      }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
        className: navClasses,
        ref: this.props.saveRef('nav')
      }, navWrapper(this.props.children)))));
    }
  }]);

  return ScrollableTabBarNode;
}(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (ScrollableTabBarNode);
ScrollableTabBarNode.propTypes = {
  activeKey: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  getRef: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func.isRequired,
  saveRef: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func.isRequired,
  tabBarPosition: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOf(['left', 'right', 'top', 'bottom']),
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  scrollAnimated: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  onPrevClick: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onNextClick: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  navWrapper: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  children: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
  prevIcon: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
  nextIcon: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
  direction: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node
};
ScrollableTabBarNode.defaultProps = {
  tabBarPosition: 'left',
  prefixCls: '',
  scrollAnimated: true,
  onPrevClick: function onPrevClick() {},
  onNextClick: function onNextClick() {},
  navWrapper: function navWrapper(ele) {
    return ele;
  }
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFicy9lcy9TY3JvbGxhYmxlVGFiQmFyTm9kZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRhYnMvZXMvU2Nyb2xsYWJsZVRhYkJhck5vZGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9kZWZpbmVQcm9wZXJ0eSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9jcmVhdGVDbGFzcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgZGVib3VuY2UgZnJvbSAnbG9kYXNoL2RlYm91bmNlJztcbmltcG9ydCBSZXNpemVPYnNlcnZlciBmcm9tICdyZXNpemUtb2JzZXJ2ZXItcG9seWZpbGwnO1xuaW1wb3J0IHsgc2V0VHJhbnNmb3JtLCBpc1RyYW5zZm9ybTNkU3VwcG9ydGVkIH0gZnJvbSAnLi91dGlscyc7XG5cbnZhciBTY3JvbGxhYmxlVGFiQmFyTm9kZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhTY3JvbGxhYmxlVGFiQmFyTm9kZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU2Nyb2xsYWJsZVRhYkJhck5vZGUocHJvcHMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2Nyb2xsYWJsZVRhYkJhck5vZGUpO1xuXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKFNjcm9sbGFibGVUYWJCYXJOb2RlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoU2Nyb2xsYWJsZVRhYkJhck5vZGUpKS5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICBfdGhpcy5wcmV2VHJhbnNpdGlvbkVuZCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoZS5wcm9wZXJ0eU5hbWUgIT09ICdvcGFjaXR5Jykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB2YXIgY29udGFpbmVyID0gX3RoaXMucHJvcHMuZ2V0UmVmKCdjb250YWluZXInKTtcbiAgICAgIF90aGlzLnNjcm9sbFRvQWN0aXZlVGFiKHtcbiAgICAgICAgdGFyZ2V0OiBjb250YWluZXIsXG4gICAgICAgIGN1cnJlbnRUYXJnZXQ6IGNvbnRhaW5lclxuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLnNjcm9sbFRvQWN0aXZlVGFiID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBhY3RpdmVUYWIgPSBfdGhpcy5wcm9wcy5nZXRSZWYoJ2FjdGl2ZVRhYicpO1xuICAgICAgdmFyIG5hdldyYXAgPSBfdGhpcy5wcm9wcy5nZXRSZWYoJ25hdldyYXAnKTtcbiAgICAgIGlmIChlICYmIGUudGFyZ2V0ICE9PSBlLmN1cnJlbnRUYXJnZXQgfHwgIWFjdGl2ZVRhYikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIHdoZW4gbm90IHNjcm9sbGFibGUgb3IgZW50ZXIgc2Nyb2xsYWJsZSBmaXJzdCB0aW1lLCBkb24ndCBlbWl0IHNjcm9sbGluZ1xuICAgICAgdmFyIG5lZWRUb1Nyb2xsID0gX3RoaXMuaXNOZXh0UHJldlNob3duKCkgJiYgX3RoaXMubGFzdE5leHRQcmV2U2hvd247XG4gICAgICBfdGhpcy5sYXN0TmV4dFByZXZTaG93biA9IF90aGlzLmlzTmV4dFByZXZTaG93bigpO1xuICAgICAgaWYgKCFuZWVkVG9Tcm9sbCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBhY3RpdmVUYWJXSCA9IF90aGlzLmdldFNjcm9sbFdIKGFjdGl2ZVRhYik7XG4gICAgICB2YXIgbmF2V3JhcE5vZGVXSCA9IF90aGlzLmdldE9mZnNldFdIKG5hdldyYXApO1xuICAgICAgdmFyIG9mZnNldCA9IF90aGlzLm9mZnNldDtcblxuICAgICAgdmFyIHdyYXBPZmZzZXQgPSBfdGhpcy5nZXRPZmZzZXRMVChuYXZXcmFwKTtcbiAgICAgIHZhciBhY3RpdmVUYWJPZmZzZXQgPSBfdGhpcy5nZXRPZmZzZXRMVChhY3RpdmVUYWIpO1xuICAgICAgaWYgKHdyYXBPZmZzZXQgPiBhY3RpdmVUYWJPZmZzZXQpIHtcbiAgICAgICAgb2Zmc2V0ICs9IHdyYXBPZmZzZXQgLSBhY3RpdmVUYWJPZmZzZXQ7XG4gICAgICAgIF90aGlzLnNldE9mZnNldChvZmZzZXQpO1xuICAgICAgfSBlbHNlIGlmICh3cmFwT2Zmc2V0ICsgbmF2V3JhcE5vZGVXSCA8IGFjdGl2ZVRhYk9mZnNldCArIGFjdGl2ZVRhYldIKSB7XG4gICAgICAgIG9mZnNldCAtPSBhY3RpdmVUYWJPZmZzZXQgKyBhY3RpdmVUYWJXSCAtICh3cmFwT2Zmc2V0ICsgbmF2V3JhcE5vZGVXSCk7XG4gICAgICAgIF90aGlzLnNldE9mZnNldChvZmZzZXQpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5wcmV2ID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIF90aGlzLnByb3BzLm9uUHJldkNsaWNrKGUpO1xuICAgICAgdmFyIG5hdldyYXBOb2RlID0gX3RoaXMucHJvcHMuZ2V0UmVmKCduYXZXcmFwJyk7XG4gICAgICB2YXIgbmF2V3JhcE5vZGVXSCA9IF90aGlzLmdldE9mZnNldFdIKG5hdldyYXBOb2RlKTtcbiAgICAgIHZhciBvZmZzZXQgPSBfdGhpcy5vZmZzZXQ7XG5cbiAgICAgIF90aGlzLnNldE9mZnNldChvZmZzZXQgKyBuYXZXcmFwTm9kZVdIKTtcbiAgICB9O1xuXG4gICAgX3RoaXMubmV4dCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBfdGhpcy5wcm9wcy5vbk5leHRDbGljayhlKTtcbiAgICAgIHZhciBuYXZXcmFwTm9kZSA9IF90aGlzLnByb3BzLmdldFJlZignbmF2V3JhcCcpO1xuICAgICAgdmFyIG5hdldyYXBOb2RlV0ggPSBfdGhpcy5nZXRPZmZzZXRXSChuYXZXcmFwTm9kZSk7XG4gICAgICB2YXIgb2Zmc2V0ID0gX3RoaXMub2Zmc2V0O1xuXG4gICAgICBfdGhpcy5zZXRPZmZzZXQob2Zmc2V0IC0gbmF2V3JhcE5vZGVXSCk7XG4gICAgfTtcblxuICAgIF90aGlzLm9mZnNldCA9IDA7XG5cbiAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG5leHQ6IGZhbHNlLFxuICAgICAgcHJldjogZmFsc2VcbiAgICB9O1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhTY3JvbGxhYmxlVGFiQmFyTm9kZSwgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHRoaXMuY29tcG9uZW50RGlkVXBkYXRlKCk7XG4gICAgICB0aGlzLmRlYm91bmNlZFJlc2l6ZSA9IGRlYm91bmNlKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgX3RoaXMyLnNldE5leHRQcmV2KCk7XG4gICAgICAgIF90aGlzMi5zY3JvbGxUb0FjdGl2ZVRhYigpO1xuICAgICAgfSwgMjAwKTtcbiAgICAgIHRoaXMucmVzaXplT2JzZXJ2ZXIgPSBuZXcgUmVzaXplT2JzZXJ2ZXIodGhpcy5kZWJvdW5jZWRSZXNpemUpO1xuICAgICAgdGhpcy5yZXNpemVPYnNlcnZlci5vYnNlcnZlKHRoaXMucHJvcHMuZ2V0UmVmKCdjb250YWluZXInKSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgIGlmIChwcmV2UHJvcHMgJiYgcHJldlByb3BzLnRhYkJhclBvc2l0aW9uICE9PSBwcm9wcy50YWJCYXJQb3NpdGlvbikge1xuICAgICAgICB0aGlzLnNldE9mZnNldCgwKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdmFyIG5leHRQcmV2ID0gdGhpcy5zZXROZXh0UHJldigpO1xuICAgICAgLy8gd2FpdCBuZXh0LCBwcmV2IHNob3cgaGlkZVxuICAgICAgLyogZXNsaW50IHJlYWN0L25vLWRpZC11cGRhdGUtc2V0LXN0YXRlOjAgKi9cbiAgICAgIGlmICh0aGlzLmlzTmV4dFByZXZTaG93bih0aGlzLnN0YXRlKSAhPT0gdGhpcy5pc05leHRQcmV2U2hvd24obmV4dFByZXYpKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe30sIHRoaXMuc2Nyb2xsVG9BY3RpdmVUYWIpO1xuICAgICAgfSBlbHNlIGlmICghcHJldlByb3BzIHx8IHByb3BzLmFjdGl2ZUtleSAhPT0gcHJldlByb3BzLmFjdGl2ZUtleSkge1xuICAgICAgICAvLyBjYW4gbm90IHVzZSBwcm9wcy5hY3RpdmVLZXlcbiAgICAgICAgdGhpcy5zY3JvbGxUb0FjdGl2ZVRhYigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICBpZiAodGhpcy5yZXNpemVPYnNlcnZlcikge1xuICAgICAgICB0aGlzLnJlc2l6ZU9ic2VydmVyLmRpc2Nvbm5lY3QoKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmRlYm91bmNlZFJlc2l6ZSAmJiB0aGlzLmRlYm91bmNlZFJlc2l6ZS5jYW5jZWwpIHtcbiAgICAgICAgdGhpcy5kZWJvdW5jZWRSZXNpemUuY2FuY2VsKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2V0TmV4dFByZXYnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXROZXh0UHJldigpIHtcbiAgICAgIHZhciBuYXZOb2RlID0gdGhpcy5wcm9wcy5nZXRSZWYoJ25hdicpO1xuICAgICAgdmFyIG5hdlRhYnNDb250YWluZXIgPSB0aGlzLnByb3BzLmdldFJlZignbmF2VGFic0NvbnRhaW5lcicpO1xuICAgICAgdmFyIG5hdk5vZGVXSCA9IHRoaXMuZ2V0U2Nyb2xsV0gobmF2VGFic0NvbnRhaW5lciB8fCBuYXZOb2RlKTtcbiAgICAgIC8vIEFkZCAxcHggdG8gZml4IGBvZmZzZXRXaWR0aGAgd2l0aCBkZWNpbWFsIGluIENocm9tZSBub3QgY29ycmVjdCBoYW5kbGVcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzEzNDIzXG4gICAgICB2YXIgY29udGFpbmVyV0ggPSB0aGlzLmdldE9mZnNldFdIKHRoaXMucHJvcHMuZ2V0UmVmKCdjb250YWluZXInKSkgKyAxO1xuICAgICAgdmFyIG5hdldyYXBOb2RlV0ggPSB0aGlzLmdldE9mZnNldFdIKHRoaXMucHJvcHMuZ2V0UmVmKCduYXZXcmFwJykpO1xuICAgICAgdmFyIG9mZnNldCA9IHRoaXMub2Zmc2V0O1xuXG4gICAgICB2YXIgbWluT2Zmc2V0ID0gY29udGFpbmVyV0ggLSBuYXZOb2RlV0g7XG4gICAgICB2YXIgX3N0YXRlID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBuZXh0ID0gX3N0YXRlLm5leHQsXG4gICAgICAgICAgcHJldiA9IF9zdGF0ZS5wcmV2O1xuXG4gICAgICBpZiAobWluT2Zmc2V0ID49IDApIHtcbiAgICAgICAgbmV4dCA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNldE9mZnNldCgwLCBmYWxzZSk7XG4gICAgICAgIG9mZnNldCA9IDA7XG4gICAgICB9IGVsc2UgaWYgKG1pbk9mZnNldCA8IG9mZnNldCkge1xuICAgICAgICBuZXh0ID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG5leHQgPSBmYWxzZTtcbiAgICAgICAgLy8gRml4IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzg4NjFcbiAgICAgICAgLy8gVGVzdCB3aXRoIGNvbnRhaW5lciBvZmZzZXQgd2hpY2ggaXMgc3RhYmxlXG4gICAgICAgIC8vIGFuZCBzZXQgdGhlIG9mZnNldCBvZiB0aGUgbmF2IHdyYXAgbm9kZVxuICAgICAgICB2YXIgcmVhbE9mZnNldCA9IG5hdldyYXBOb2RlV0ggLSBuYXZOb2RlV0g7XG4gICAgICAgIHRoaXMuc2V0T2Zmc2V0KHJlYWxPZmZzZXQsIGZhbHNlKTtcbiAgICAgICAgb2Zmc2V0ID0gcmVhbE9mZnNldDtcbiAgICAgIH1cblxuICAgICAgaWYgKG9mZnNldCA8IDApIHtcbiAgICAgICAgcHJldiA9IHRydWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBwcmV2ID0gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0TmV4dChuZXh0KTtcbiAgICAgIHRoaXMuc2V0UHJldihwcmV2KTtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5leHQ6IG5leHQsXG4gICAgICAgIHByZXY6IHByZXZcbiAgICAgIH07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0T2Zmc2V0V0gnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRPZmZzZXRXSChub2RlKSB7XG4gICAgICB2YXIgdGFiQmFyUG9zaXRpb24gPSB0aGlzLnByb3BzLnRhYkJhclBvc2l0aW9uO1xuICAgICAgdmFyIHByb3AgPSAnb2Zmc2V0V2lkdGgnO1xuICAgICAgaWYgKHRhYkJhclBvc2l0aW9uID09PSAnbGVmdCcgfHwgdGFiQmFyUG9zaXRpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgcHJvcCA9ICdvZmZzZXRIZWlnaHQnO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG5vZGVbcHJvcF07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0U2Nyb2xsV0gnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRTY3JvbGxXSChub2RlKSB7XG4gICAgICB2YXIgdGFiQmFyUG9zaXRpb24gPSB0aGlzLnByb3BzLnRhYkJhclBvc2l0aW9uO1xuICAgICAgdmFyIHByb3AgPSAnc2Nyb2xsV2lkdGgnO1xuICAgICAgaWYgKHRhYkJhclBvc2l0aW9uID09PSAnbGVmdCcgfHwgdGFiQmFyUG9zaXRpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgcHJvcCA9ICdzY3JvbGxIZWlnaHQnO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG5vZGVbcHJvcF07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0T2Zmc2V0TFQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRPZmZzZXRMVChub2RlKSB7XG4gICAgICB2YXIgdGFiQmFyUG9zaXRpb24gPSB0aGlzLnByb3BzLnRhYkJhclBvc2l0aW9uO1xuICAgICAgdmFyIHByb3AgPSAnbGVmdCc7XG4gICAgICBpZiAodGFiQmFyUG9zaXRpb24gPT09ICdsZWZ0JyB8fCB0YWJCYXJQb3NpdGlvbiA9PT0gJ3JpZ2h0Jykge1xuICAgICAgICBwcm9wID0gJ3RvcCc7XG4gICAgICB9XG4gICAgICByZXR1cm4gbm9kZS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKVtwcm9wXTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzZXRPZmZzZXQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRPZmZzZXQob2Zmc2V0KSB7XG4gICAgICB2YXIgY2hlY2tOZXh0UHJldiA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcblxuICAgICAgdmFyIHRhcmdldCA9IE1hdGgubWluKDAsIG9mZnNldCk7XG4gICAgICBpZiAodGhpcy5vZmZzZXQgIT09IHRhcmdldCkge1xuICAgICAgICB0aGlzLm9mZnNldCA9IHRhcmdldDtcbiAgICAgICAgdmFyIG5hdk9mZnNldCA9IHt9O1xuICAgICAgICB2YXIgdGFiQmFyUG9zaXRpb24gPSB0aGlzLnByb3BzLnRhYkJhclBvc2l0aW9uO1xuICAgICAgICB2YXIgbmF2U3R5bGUgPSB0aGlzLnByb3BzLmdldFJlZignbmF2Jykuc3R5bGU7XG4gICAgICAgIHZhciB0cmFuc2Zvcm1TdXBwb3J0ZWQgPSBpc1RyYW5zZm9ybTNkU3VwcG9ydGVkKG5hdlN0eWxlKTtcbiAgICAgICAgaWYgKHRhYkJhclBvc2l0aW9uID09PSAnbGVmdCcgfHwgdGFiQmFyUG9zaXRpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgICBpZiAodHJhbnNmb3JtU3VwcG9ydGVkKSB7XG4gICAgICAgICAgICBuYXZPZmZzZXQgPSB7XG4gICAgICAgICAgICAgIHZhbHVlOiAndHJhbnNsYXRlM2QoMCwnICsgdGFyZ2V0ICsgJ3B4LDApJ1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbmF2T2Zmc2V0ID0ge1xuICAgICAgICAgICAgICBuYW1lOiAndG9wJyxcbiAgICAgICAgICAgICAgdmFsdWU6IHRhcmdldCArICdweCdcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKHRyYW5zZm9ybVN1cHBvcnRlZCkge1xuICAgICAgICAgIGlmICh0aGlzLnByb3BzLmRpcmVjdGlvbiA9PT0gJ3J0bCcpIHtcbiAgICAgICAgICAgIHRhcmdldCA9IC10YXJnZXQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5hdk9mZnNldCA9IHtcbiAgICAgICAgICAgIHZhbHVlOiAndHJhbnNsYXRlM2QoJyArIHRhcmdldCArICdweCwwLDApJ1xuICAgICAgICAgIH07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbmF2T2Zmc2V0ID0ge1xuICAgICAgICAgICAgbmFtZTogJ2xlZnQnLFxuICAgICAgICAgICAgdmFsdWU6IHRhcmdldCArICdweCdcbiAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGlmICh0cmFuc2Zvcm1TdXBwb3J0ZWQpIHtcbiAgICAgICAgICBzZXRUcmFuc2Zvcm0obmF2U3R5bGUsIG5hdk9mZnNldC52YWx1ZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbmF2U3R5bGVbbmF2T2Zmc2V0Lm5hbWVdID0gbmF2T2Zmc2V0LnZhbHVlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChjaGVja05leHRQcmV2KSB7XG4gICAgICAgICAgdGhpcy5zZXROZXh0UHJldigpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2V0UHJldicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFByZXYodikge1xuICAgICAgaWYgKHRoaXMuc3RhdGUucHJldiAhPT0gdikge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBwcmV2OiB2XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3NldE5leHQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXROZXh0KHYpIHtcbiAgICAgIGlmICh0aGlzLnN0YXRlLm5leHQgIT09IHYpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgbmV4dDogdlxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdpc05leHRQcmV2U2hvd24nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc05leHRQcmV2U2hvd24oc3RhdGUpIHtcbiAgICAgIGlmIChzdGF0ZSkge1xuICAgICAgICByZXR1cm4gc3RhdGUubmV4dCB8fCBzdGF0ZS5wcmV2O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMuc3RhdGUubmV4dCB8fCB0aGlzLnN0YXRlLnByZXY7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc25hbWVzLCBfY2xhc3NuYW1lczIsIF9jbGFzc25hbWVzMywgX2NsYXNzbmFtZXM0O1xuXG4gICAgICB2YXIgX3N0YXRlMiA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgbmV4dCA9IF9zdGF0ZTIubmV4dCxcbiAgICAgICAgICBwcmV2ID0gX3N0YXRlMi5wcmV2O1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzLnByZWZpeENscyxcbiAgICAgICAgICBzY3JvbGxBbmltYXRlZCA9IF9wcm9wcy5zY3JvbGxBbmltYXRlZCxcbiAgICAgICAgICBuYXZXcmFwcGVyID0gX3Byb3BzLm5hdldyYXBwZXIsXG4gICAgICAgICAgcHJldkljb24gPSBfcHJvcHMucHJldkljb24sXG4gICAgICAgICAgbmV4dEljb24gPSBfcHJvcHMubmV4dEljb247XG5cbiAgICAgIHZhciBzaG93TmV4dFByZXYgPSBwcmV2IHx8IG5leHQ7XG5cbiAgICAgIHZhciBwcmV2QnV0dG9uID0gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ3NwYW4nLFxuICAgICAgICB7XG4gICAgICAgICAgb25DbGljazogcHJldiA/IHRoaXMucHJldiA6IG51bGwsXG4gICAgICAgICAgdW5zZWxlY3RhYmxlOiAndW5zZWxlY3RhYmxlJyxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzbmFtZXMoKF9jbGFzc25hbWVzID0ge30sIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NuYW1lcywgcHJlZml4Q2xzICsgJy10YWItcHJldicsIDEpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXMsIHByZWZpeENscyArICctdGFiLWJ0bi1kaXNhYmxlZCcsICFwcmV2KSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzLCBwcmVmaXhDbHMgKyAnLXRhYi1hcnJvdy1zaG93Jywgc2hvd05leHRQcmV2KSwgX2NsYXNzbmFtZXMpKSxcbiAgICAgICAgICBvblRyYW5zaXRpb25FbmQ6IHRoaXMucHJldlRyYW5zaXRpb25FbmRcbiAgICAgICAgfSxcbiAgICAgICAgcHJldkljb24gfHwgUmVhY3QuY3JlYXRlRWxlbWVudCgnc3BhbicsIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXRhYi1wcmV2LWljb24nIH0pXG4gICAgICApO1xuXG4gICAgICB2YXIgbmV4dEJ1dHRvbiA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzcGFuJyxcbiAgICAgICAge1xuICAgICAgICAgIG9uQ2xpY2s6IG5leHQgPyB0aGlzLm5leHQgOiBudWxsLFxuICAgICAgICAgIHVuc2VsZWN0YWJsZTogJ3Vuc2VsZWN0YWJsZScsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc25hbWVzKChfY2xhc3NuYW1lczIgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzMiwgcHJlZml4Q2xzICsgJy10YWItbmV4dCcsIDEpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXMyLCBwcmVmaXhDbHMgKyAnLXRhYi1idG4tZGlzYWJsZWQnLCAhbmV4dCksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NuYW1lczIsIHByZWZpeENscyArICctdGFiLWFycm93LXNob3cnLCBzaG93TmV4dFByZXYpLCBfY2xhc3NuYW1lczIpKVxuICAgICAgICB9LFxuICAgICAgICBuZXh0SWNvbiB8fCBSZWFjdC5jcmVhdGVFbGVtZW50KCdzcGFuJywgeyBjbGFzc05hbWU6IHByZWZpeENscyArICctdGFiLW5leHQtaWNvbicgfSlcbiAgICAgICk7XG5cbiAgICAgIHZhciBuYXZDbGFzc05hbWUgPSBwcmVmaXhDbHMgKyAnLW5hdic7XG4gICAgICB2YXIgbmF2Q2xhc3NlcyA9IGNsYXNzbmFtZXMoKF9jbGFzc25hbWVzMyA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXMzLCBuYXZDbGFzc05hbWUsIHRydWUpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXMzLCBzY3JvbGxBbmltYXRlZCA/IG5hdkNsYXNzTmFtZSArICctYW5pbWF0ZWQnIDogbmF2Q2xhc3NOYW1lICsgJy1uby1hbmltYXRlZCcsIHRydWUpLCBfY2xhc3NuYW1lczMpKTtcblxuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc25hbWVzKChfY2xhc3NuYW1lczQgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzNCwgcHJlZml4Q2xzICsgJy1uYXYtY29udGFpbmVyJywgMSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NuYW1lczQsIHByZWZpeENscyArICctbmF2LWNvbnRhaW5lci1zY3JvbGxpbmcnLCBzaG93TmV4dFByZXYpLCBfY2xhc3NuYW1lczQpKSxcbiAgICAgICAgICBrZXk6ICdjb250YWluZXInLFxuICAgICAgICAgIHJlZjogdGhpcy5wcm9wcy5zYXZlUmVmKCdjb250YWluZXInKVxuICAgICAgICB9LFxuICAgICAgICBwcmV2QnV0dG9uLFxuICAgICAgICBuZXh0QnV0dG9uLFxuICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLW5hdi13cmFwJywgcmVmOiB0aGlzLnByb3BzLnNhdmVSZWYoJ25hdldyYXAnKSB9LFxuICAgICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLW5hdi1zY3JvbGwnIH0sXG4gICAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgICAgeyBjbGFzc05hbWU6IG5hdkNsYXNzZXMsIHJlZjogdGhpcy5wcm9wcy5zYXZlUmVmKCduYXYnKSB9LFxuICAgICAgICAgICAgICBuYXZXcmFwcGVyKHRoaXMucHJvcHMuY2hpbGRyZW4pXG4gICAgICAgICAgICApXG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBTY3JvbGxhYmxlVGFiQmFyTm9kZTtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuZXhwb3J0IGRlZmF1bHQgU2Nyb2xsYWJsZVRhYkJhck5vZGU7XG5cblxuU2Nyb2xsYWJsZVRhYkJhck5vZGUucHJvcFR5cGVzID0ge1xuICBhY3RpdmVLZXk6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGdldFJlZjogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgc2F2ZVJlZjogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdGFiQmFyUG9zaXRpb246IFByb3BUeXBlcy5vbmVPZihbJ2xlZnQnLCAncmlnaHQnLCAndG9wJywgJ2JvdHRvbSddKSxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBzY3JvbGxBbmltYXRlZDogUHJvcFR5cGVzLmJvb2wsXG4gIG9uUHJldkNsaWNrOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25OZXh0Q2xpY2s6IFByb3BUeXBlcy5mdW5jLFxuICBuYXZXcmFwcGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICBwcmV2SWNvbjogUHJvcFR5cGVzLm5vZGUsXG4gIG5leHRJY29uOiBQcm9wVHlwZXMubm9kZSxcbiAgZGlyZWN0aW9uOiBQcm9wVHlwZXMubm9kZVxufTtcblxuU2Nyb2xsYWJsZVRhYkJhck5vZGUuZGVmYXVsdFByb3BzID0ge1xuICB0YWJCYXJQb3NpdGlvbjogJ2xlZnQnLFxuICBwcmVmaXhDbHM6ICcnLFxuICBzY3JvbGxBbmltYXRlZDogdHJ1ZSxcbiAgb25QcmV2Q2xpY2s6IGZ1bmN0aW9uIG9uUHJldkNsaWNrKCkge30sXG4gIG9uTmV4dENsaWNrOiBmdW5jdGlvbiBvbk5leHRDbGljaygpIHt9LFxuICBuYXZXcmFwcGVyOiBmdW5jdGlvbiBuYXZXcmFwcGVyKGVsZSkge1xuICAgIHJldHVybiBlbGU7XG4gIH1cbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQTdDQTtBQStDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTdDQTtBQStDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFBQTtBQUdBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUFBO0FBR0E7QUFDQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFTQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBTUE7QUFoRUE7QUFDQTtBQWtFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tabs/es/ScrollableTabBarNode.js
