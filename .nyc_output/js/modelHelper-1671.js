/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Model = __webpack_require__(/*! ../../model/Model */ "./node_modules/echarts/lib/model/Model.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var each = zrUtil.each;
var curry = zrUtil.curry; // Build axisPointerModel, mergin tooltip.axisPointer model for each axis.
// allAxesInfo should be updated when setOption performed.

function collect(ecModel, api) {
  var result = {
    /**
     * key: makeKey(axis.model)
     * value: {
     *      axis,
     *      coordSys,
     *      axisPointerModel,
     *      triggerTooltip,
     *      involveSeries,
     *      snap,
     *      seriesModels,
     *      seriesDataCount
     * }
     */
    axesInfo: {},
    seriesInvolved: false,

    /**
     * key: makeKey(coordSys.model)
     * value: Object: key makeKey(axis.model), value: axisInfo
     */
    coordSysAxesInfo: {},
    coordSysMap: {}
  };
  collectAxesInfo(result, ecModel, api); // Check seriesInvolved for performance, in case too many series in some chart.

  result.seriesInvolved && collectSeriesInfo(result, ecModel);
  return result;
}

function collectAxesInfo(result, ecModel, api) {
  var globalTooltipModel = ecModel.getComponent('tooltip');
  var globalAxisPointerModel = ecModel.getComponent('axisPointer'); // links can only be set on global.

  var linksOption = globalAxisPointerModel.get('link', true) || [];
  var linkGroups = []; // Collect axes info.

  each(api.getCoordinateSystems(), function (coordSys) {
    // Some coordinate system do not support axes, like geo.
    if (!coordSys.axisPointerEnabled) {
      return;
    }

    var coordSysKey = makeKey(coordSys.model);
    var axesInfoInCoordSys = result.coordSysAxesInfo[coordSysKey] = {};
    result.coordSysMap[coordSysKey] = coordSys; // Set tooltip (like 'cross') is a convienent way to show axisPointer
    // for user. So we enable seting tooltip on coordSys model.

    var coordSysModel = coordSys.model;
    var baseTooltipModel = coordSysModel.getModel('tooltip', globalTooltipModel);
    each(coordSys.getAxes(), curry(saveTooltipAxisInfo, false, null)); // If axis tooltip used, choose tooltip axis for each coordSys.
    // Notice this case: coordSys is `grid` but not `cartesian2D` here.

    if (coordSys.getTooltipAxes && globalTooltipModel // If tooltip.showContent is set as false, tooltip will not
    // show but axisPointer will show as normal.
    && baseTooltipModel.get('show')) {
      // Compatible with previous logic. But series.tooltip.trigger: 'axis'
      // or series.data[n].tooltip.trigger: 'axis' are not support any more.
      var triggerAxis = baseTooltipModel.get('trigger') === 'axis';
      var cross = baseTooltipModel.get('axisPointer.type') === 'cross';
      var tooltipAxes = coordSys.getTooltipAxes(baseTooltipModel.get('axisPointer.axis'));

      if (triggerAxis || cross) {
        each(tooltipAxes.baseAxes, curry(saveTooltipAxisInfo, cross ? 'cross' : true, triggerAxis));
      }

      if (cross) {
        each(tooltipAxes.otherAxes, curry(saveTooltipAxisInfo, 'cross', false));
      }
    } // fromTooltip: true | false | 'cross'
    // triggerTooltip: true | false | null


    function saveTooltipAxisInfo(fromTooltip, triggerTooltip, axis) {
      var axisPointerModel = axis.model.getModel('axisPointer', globalAxisPointerModel);
      var axisPointerShow = axisPointerModel.get('show');

      if (!axisPointerShow || axisPointerShow === 'auto' && !fromTooltip && !isHandleTrigger(axisPointerModel)) {
        return;
      }

      if (triggerTooltip == null) {
        triggerTooltip = axisPointerModel.get('triggerTooltip');
      }

      axisPointerModel = fromTooltip ? makeAxisPointerModel(axis, baseTooltipModel, globalAxisPointerModel, ecModel, fromTooltip, triggerTooltip) : axisPointerModel;
      var snap = axisPointerModel.get('snap');
      var key = makeKey(axis.model);
      var involveSeries = triggerTooltip || snap || axis.type === 'category'; // If result.axesInfo[key] exist, override it (tooltip has higher priority).

      var axisInfo = result.axesInfo[key] = {
        key: key,
        axis: axis,
        coordSys: coordSys,
        axisPointerModel: axisPointerModel,
        triggerTooltip: triggerTooltip,
        involveSeries: involveSeries,
        snap: snap,
        useHandle: isHandleTrigger(axisPointerModel),
        seriesModels: []
      };
      axesInfoInCoordSys[key] = axisInfo;
      result.seriesInvolved |= involveSeries;
      var groupIndex = getLinkGroupIndex(linksOption, axis);

      if (groupIndex != null) {
        var linkGroup = linkGroups[groupIndex] || (linkGroups[groupIndex] = {
          axesInfo: {}
        });
        linkGroup.axesInfo[key] = axisInfo;
        linkGroup.mapper = linksOption[groupIndex].mapper;
        axisInfo.linkGroup = linkGroup;
      }
    }
  });
}

function makeAxisPointerModel(axis, baseTooltipModel, globalAxisPointerModel, ecModel, fromTooltip, triggerTooltip) {
  var tooltipAxisPointerModel = baseTooltipModel.getModel('axisPointer');
  var volatileOption = {};
  each(['type', 'snap', 'lineStyle', 'shadowStyle', 'label', 'animation', 'animationDurationUpdate', 'animationEasingUpdate', 'z'], function (field) {
    volatileOption[field] = zrUtil.clone(tooltipAxisPointerModel.get(field));
  }); // category axis do not auto snap, otherwise some tick that do not
  // has value can not be hovered. value/time/log axis default snap if
  // triggered from tooltip and trigger tooltip.

  volatileOption.snap = axis.type !== 'category' && !!triggerTooltip; // Compatibel with previous behavior, tooltip axis do not show label by default.
  // Only these properties can be overrided from tooltip to axisPointer.

  if (tooltipAxisPointerModel.get('type') === 'cross') {
    volatileOption.type = 'line';
  }

  var labelOption = volatileOption.label || (volatileOption.label = {}); // Follow the convention, do not show label when triggered by tooltip by default.

  labelOption.show == null && (labelOption.show = false);

  if (fromTooltip === 'cross') {
    // When 'cross', both axes show labels.
    var tooltipAxisPointerLabelShow = tooltipAxisPointerModel.get('label.show');
    labelOption.show = tooltipAxisPointerLabelShow != null ? tooltipAxisPointerLabelShow : true; // If triggerTooltip, this is a base axis, which should better not use cross style
    // (cross style is dashed by default)

    if (!triggerTooltip) {
      var crossStyle = volatileOption.lineStyle = tooltipAxisPointerModel.get('crossStyle');
      crossStyle && zrUtil.defaults(labelOption, crossStyle.textStyle);
    }
  }

  return axis.model.getModel('axisPointer', new Model(volatileOption, globalAxisPointerModel, ecModel));
}

function collectSeriesInfo(result, ecModel) {
  // Prepare data for axis trigger
  ecModel.eachSeries(function (seriesModel) {
    // Notice this case: this coordSys is `cartesian2D` but not `grid`.
    var coordSys = seriesModel.coordinateSystem;
    var seriesTooltipTrigger = seriesModel.get('tooltip.trigger', true);
    var seriesTooltipShow = seriesModel.get('tooltip.show', true);

    if (!coordSys || seriesTooltipTrigger === 'none' || seriesTooltipTrigger === false || seriesTooltipTrigger === 'item' || seriesTooltipShow === false || seriesModel.get('axisPointer.show', true) === false) {
      return;
    }

    each(result.coordSysAxesInfo[makeKey(coordSys.model)], function (axisInfo) {
      var axis = axisInfo.axis;

      if (coordSys.getAxis(axis.dim) === axis) {
        axisInfo.seriesModels.push(seriesModel);
        axisInfo.seriesDataCount == null && (axisInfo.seriesDataCount = 0);
        axisInfo.seriesDataCount += seriesModel.getData().count();
      }
    });
  }, this);
}
/**
 * For example:
 * {
 *     axisPointer: {
 *         links: [{
 *             xAxisIndex: [2, 4],
 *             yAxisIndex: 'all'
 *         }, {
 *             xAxisId: ['a5', 'a7'],
 *             xAxisName: 'xxx'
 *         }]
 *     }
 * }
 */


function getLinkGroupIndex(linksOption, axis) {
  var axisModel = axis.model;
  var dim = axis.dim;

  for (var i = 0; i < linksOption.length; i++) {
    var linkOption = linksOption[i] || {};

    if (checkPropInLink(linkOption[dim + 'AxisId'], axisModel.id) || checkPropInLink(linkOption[dim + 'AxisIndex'], axisModel.componentIndex) || checkPropInLink(linkOption[dim + 'AxisName'], axisModel.name)) {
      return i;
    }
  }
}

function checkPropInLink(linkPropValue, axisPropValue) {
  return linkPropValue === 'all' || zrUtil.isArray(linkPropValue) && zrUtil.indexOf(linkPropValue, axisPropValue) >= 0 || linkPropValue === axisPropValue;
}

function fixValue(axisModel) {
  var axisInfo = getAxisInfo(axisModel);

  if (!axisInfo) {
    return;
  }

  var axisPointerModel = axisInfo.axisPointerModel;
  var scale = axisInfo.axis.scale;
  var option = axisPointerModel.option;
  var status = axisPointerModel.get('status');
  var value = axisPointerModel.get('value'); // Parse init value for category and time axis.

  if (value != null) {
    value = scale.parse(value);
  }

  var useHandle = isHandleTrigger(axisPointerModel); // If `handle` used, `axisPointer` will always be displayed, so value
  // and status should be initialized.

  if (status == null) {
    option.status = useHandle ? 'show' : 'hide';
  }

  var extent = scale.getExtent().slice();
  extent[0] > extent[1] && extent.reverse();

  if ( // Pick a value on axis when initializing.
  value == null // If both `handle` and `dataZoom` are used, value may be out of axis extent,
  // where we should re-pick a value to keep `handle` displaying normally.
  || value > extent[1]) {
    // Make handle displayed on the end of the axis when init, which looks better.
    value = extent[1];
  }

  if (value < extent[0]) {
    value = extent[0];
  }

  option.value = value;

  if (useHandle) {
    option.status = axisInfo.axis.scale.isBlank() ? 'hide' : 'show';
  }
}

function getAxisInfo(axisModel) {
  var coordSysAxesInfo = (axisModel.ecModel.getComponent('axisPointer') || {}).coordSysAxesInfo;
  return coordSysAxesInfo && coordSysAxesInfo.axesInfo[makeKey(axisModel)];
}

function getAxisPointerModel(axisModel) {
  var axisInfo = getAxisInfo(axisModel);
  return axisInfo && axisInfo.axisPointerModel;
}

function isHandleTrigger(axisPointerModel) {
  return !!axisPointerModel.get('handle.show');
}
/**
 * @param {module:echarts/model/Model} model
 * @return {string} unique key
 */


function makeKey(model) {
  return model.type + '||' + model.id;
}

exports.collect = collect;
exports.fixValue = fixValue;
exports.getAxisInfo = getAxisInfo;
exports.getAxisPointerModel = getAxisPointerModel;
exports.makeKey = makeKey;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL21vZGVsSGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL21vZGVsSGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIE1vZGVsID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL01vZGVsXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgZWFjaCA9IHpyVXRpbC5lYWNoO1xudmFyIGN1cnJ5ID0genJVdGlsLmN1cnJ5OyAvLyBCdWlsZCBheGlzUG9pbnRlck1vZGVsLCBtZXJnaW4gdG9vbHRpcC5heGlzUG9pbnRlciBtb2RlbCBmb3IgZWFjaCBheGlzLlxuLy8gYWxsQXhlc0luZm8gc2hvdWxkIGJlIHVwZGF0ZWQgd2hlbiBzZXRPcHRpb24gcGVyZm9ybWVkLlxuXG5mdW5jdGlvbiBjb2xsZWN0KGVjTW9kZWwsIGFwaSkge1xuICB2YXIgcmVzdWx0ID0ge1xuICAgIC8qKlxuICAgICAqIGtleTogbWFrZUtleShheGlzLm1vZGVsKVxuICAgICAqIHZhbHVlOiB7XG4gICAgICogICAgICBheGlzLFxuICAgICAqICAgICAgY29vcmRTeXMsXG4gICAgICogICAgICBheGlzUG9pbnRlck1vZGVsLFxuICAgICAqICAgICAgdHJpZ2dlclRvb2x0aXAsXG4gICAgICogICAgICBpbnZvbHZlU2VyaWVzLFxuICAgICAqICAgICAgc25hcCxcbiAgICAgKiAgICAgIHNlcmllc01vZGVscyxcbiAgICAgKiAgICAgIHNlcmllc0RhdGFDb3VudFxuICAgICAqIH1cbiAgICAgKi9cbiAgICBheGVzSW5mbzoge30sXG4gICAgc2VyaWVzSW52b2x2ZWQ6IGZhbHNlLFxuXG4gICAgLyoqXG4gICAgICoga2V5OiBtYWtlS2V5KGNvb3JkU3lzLm1vZGVsKVxuICAgICAqIHZhbHVlOiBPYmplY3Q6IGtleSBtYWtlS2V5KGF4aXMubW9kZWwpLCB2YWx1ZTogYXhpc0luZm9cbiAgICAgKi9cbiAgICBjb29yZFN5c0F4ZXNJbmZvOiB7fSxcbiAgICBjb29yZFN5c01hcDoge31cbiAgfTtcbiAgY29sbGVjdEF4ZXNJbmZvKHJlc3VsdCwgZWNNb2RlbCwgYXBpKTsgLy8gQ2hlY2sgc2VyaWVzSW52b2x2ZWQgZm9yIHBlcmZvcm1hbmNlLCBpbiBjYXNlIHRvbyBtYW55IHNlcmllcyBpbiBzb21lIGNoYXJ0LlxuXG4gIHJlc3VsdC5zZXJpZXNJbnZvbHZlZCAmJiBjb2xsZWN0U2VyaWVzSW5mbyhyZXN1bHQsIGVjTW9kZWwpO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBjb2xsZWN0QXhlc0luZm8ocmVzdWx0LCBlY01vZGVsLCBhcGkpIHtcbiAgdmFyIGdsb2JhbFRvb2x0aXBNb2RlbCA9IGVjTW9kZWwuZ2V0Q29tcG9uZW50KCd0b29sdGlwJyk7XG4gIHZhciBnbG9iYWxBeGlzUG9pbnRlck1vZGVsID0gZWNNb2RlbC5nZXRDb21wb25lbnQoJ2F4aXNQb2ludGVyJyk7IC8vIGxpbmtzIGNhbiBvbmx5IGJlIHNldCBvbiBnbG9iYWwuXG5cbiAgdmFyIGxpbmtzT3B0aW9uID0gZ2xvYmFsQXhpc1BvaW50ZXJNb2RlbC5nZXQoJ2xpbmsnLCB0cnVlKSB8fCBbXTtcbiAgdmFyIGxpbmtHcm91cHMgPSBbXTsgLy8gQ29sbGVjdCBheGVzIGluZm8uXG5cbiAgZWFjaChhcGkuZ2V0Q29vcmRpbmF0ZVN5c3RlbXMoKSwgZnVuY3Rpb24gKGNvb3JkU3lzKSB7XG4gICAgLy8gU29tZSBjb29yZGluYXRlIHN5c3RlbSBkbyBub3Qgc3VwcG9ydCBheGVzLCBsaWtlIGdlby5cbiAgICBpZiAoIWNvb3JkU3lzLmF4aXNQb2ludGVyRW5hYmxlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBjb29yZFN5c0tleSA9IG1ha2VLZXkoY29vcmRTeXMubW9kZWwpO1xuICAgIHZhciBheGVzSW5mb0luQ29vcmRTeXMgPSByZXN1bHQuY29vcmRTeXNBeGVzSW5mb1tjb29yZFN5c0tleV0gPSB7fTtcbiAgICByZXN1bHQuY29vcmRTeXNNYXBbY29vcmRTeXNLZXldID0gY29vcmRTeXM7IC8vIFNldCB0b29sdGlwIChsaWtlICdjcm9zcycpIGlzIGEgY29udmllbmVudCB3YXkgdG8gc2hvdyBheGlzUG9pbnRlclxuICAgIC8vIGZvciB1c2VyLiBTbyB3ZSBlbmFibGUgc2V0aW5nIHRvb2x0aXAgb24gY29vcmRTeXMgbW9kZWwuXG5cbiAgICB2YXIgY29vcmRTeXNNb2RlbCA9IGNvb3JkU3lzLm1vZGVsO1xuICAgIHZhciBiYXNlVG9vbHRpcE1vZGVsID0gY29vcmRTeXNNb2RlbC5nZXRNb2RlbCgndG9vbHRpcCcsIGdsb2JhbFRvb2x0aXBNb2RlbCk7XG4gICAgZWFjaChjb29yZFN5cy5nZXRBeGVzKCksIGN1cnJ5KHNhdmVUb29sdGlwQXhpc0luZm8sIGZhbHNlLCBudWxsKSk7IC8vIElmIGF4aXMgdG9vbHRpcCB1c2VkLCBjaG9vc2UgdG9vbHRpcCBheGlzIGZvciBlYWNoIGNvb3JkU3lzLlxuICAgIC8vIE5vdGljZSB0aGlzIGNhc2U6IGNvb3JkU3lzIGlzIGBncmlkYCBidXQgbm90IGBjYXJ0ZXNpYW4yRGAgaGVyZS5cblxuICAgIGlmIChjb29yZFN5cy5nZXRUb29sdGlwQXhlcyAmJiBnbG9iYWxUb29sdGlwTW9kZWwgLy8gSWYgdG9vbHRpcC5zaG93Q29udGVudCBpcyBzZXQgYXMgZmFsc2UsIHRvb2x0aXAgd2lsbCBub3RcbiAgICAvLyBzaG93IGJ1dCBheGlzUG9pbnRlciB3aWxsIHNob3cgYXMgbm9ybWFsLlxuICAgICYmIGJhc2VUb29sdGlwTW9kZWwuZ2V0KCdzaG93JykpIHtcbiAgICAgIC8vIENvbXBhdGlibGUgd2l0aCBwcmV2aW91cyBsb2dpYy4gQnV0IHNlcmllcy50b29sdGlwLnRyaWdnZXI6ICdheGlzJ1xuICAgICAgLy8gb3Igc2VyaWVzLmRhdGFbbl0udG9vbHRpcC50cmlnZ2VyOiAnYXhpcycgYXJlIG5vdCBzdXBwb3J0IGFueSBtb3JlLlxuICAgICAgdmFyIHRyaWdnZXJBeGlzID0gYmFzZVRvb2x0aXBNb2RlbC5nZXQoJ3RyaWdnZXInKSA9PT0gJ2F4aXMnO1xuICAgICAgdmFyIGNyb3NzID0gYmFzZVRvb2x0aXBNb2RlbC5nZXQoJ2F4aXNQb2ludGVyLnR5cGUnKSA9PT0gJ2Nyb3NzJztcbiAgICAgIHZhciB0b29sdGlwQXhlcyA9IGNvb3JkU3lzLmdldFRvb2x0aXBBeGVzKGJhc2VUb29sdGlwTW9kZWwuZ2V0KCdheGlzUG9pbnRlci5heGlzJykpO1xuXG4gICAgICBpZiAodHJpZ2dlckF4aXMgfHwgY3Jvc3MpIHtcbiAgICAgICAgZWFjaCh0b29sdGlwQXhlcy5iYXNlQXhlcywgY3Vycnkoc2F2ZVRvb2x0aXBBeGlzSW5mbywgY3Jvc3MgPyAnY3Jvc3MnIDogdHJ1ZSwgdHJpZ2dlckF4aXMpKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGNyb3NzKSB7XG4gICAgICAgIGVhY2godG9vbHRpcEF4ZXMub3RoZXJBeGVzLCBjdXJyeShzYXZlVG9vbHRpcEF4aXNJbmZvLCAnY3Jvc3MnLCBmYWxzZSkpO1xuICAgICAgfVxuICAgIH0gLy8gZnJvbVRvb2x0aXA6IHRydWUgfCBmYWxzZSB8ICdjcm9zcydcbiAgICAvLyB0cmlnZ2VyVG9vbHRpcDogdHJ1ZSB8IGZhbHNlIHwgbnVsbFxuXG5cbiAgICBmdW5jdGlvbiBzYXZlVG9vbHRpcEF4aXNJbmZvKGZyb21Ub29sdGlwLCB0cmlnZ2VyVG9vbHRpcCwgYXhpcykge1xuICAgICAgdmFyIGF4aXNQb2ludGVyTW9kZWwgPSBheGlzLm1vZGVsLmdldE1vZGVsKCdheGlzUG9pbnRlcicsIGdsb2JhbEF4aXNQb2ludGVyTW9kZWwpO1xuICAgICAgdmFyIGF4aXNQb2ludGVyU2hvdyA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdzaG93Jyk7XG5cbiAgICAgIGlmICghYXhpc1BvaW50ZXJTaG93IHx8IGF4aXNQb2ludGVyU2hvdyA9PT0gJ2F1dG8nICYmICFmcm9tVG9vbHRpcCAmJiAhaXNIYW5kbGVUcmlnZ2VyKGF4aXNQb2ludGVyTW9kZWwpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHRyaWdnZXJUb29sdGlwID09IG51bGwpIHtcbiAgICAgICAgdHJpZ2dlclRvb2x0aXAgPSBheGlzUG9pbnRlck1vZGVsLmdldCgndHJpZ2dlclRvb2x0aXAnKTtcbiAgICAgIH1cblxuICAgICAgYXhpc1BvaW50ZXJNb2RlbCA9IGZyb21Ub29sdGlwID8gbWFrZUF4aXNQb2ludGVyTW9kZWwoYXhpcywgYmFzZVRvb2x0aXBNb2RlbCwgZ2xvYmFsQXhpc1BvaW50ZXJNb2RlbCwgZWNNb2RlbCwgZnJvbVRvb2x0aXAsIHRyaWdnZXJUb29sdGlwKSA6IGF4aXNQb2ludGVyTW9kZWw7XG4gICAgICB2YXIgc25hcCA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdzbmFwJyk7XG4gICAgICB2YXIga2V5ID0gbWFrZUtleShheGlzLm1vZGVsKTtcbiAgICAgIHZhciBpbnZvbHZlU2VyaWVzID0gdHJpZ2dlclRvb2x0aXAgfHwgc25hcCB8fCBheGlzLnR5cGUgPT09ICdjYXRlZ29yeSc7IC8vIElmIHJlc3VsdC5heGVzSW5mb1trZXldIGV4aXN0LCBvdmVycmlkZSBpdCAodG9vbHRpcCBoYXMgaGlnaGVyIHByaW9yaXR5KS5cblxuICAgICAgdmFyIGF4aXNJbmZvID0gcmVzdWx0LmF4ZXNJbmZvW2tleV0gPSB7XG4gICAgICAgIGtleToga2V5LFxuICAgICAgICBheGlzOiBheGlzLFxuICAgICAgICBjb29yZFN5czogY29vcmRTeXMsXG4gICAgICAgIGF4aXNQb2ludGVyTW9kZWw6IGF4aXNQb2ludGVyTW9kZWwsXG4gICAgICAgIHRyaWdnZXJUb29sdGlwOiB0cmlnZ2VyVG9vbHRpcCxcbiAgICAgICAgaW52b2x2ZVNlcmllczogaW52b2x2ZVNlcmllcyxcbiAgICAgICAgc25hcDogc25hcCxcbiAgICAgICAgdXNlSGFuZGxlOiBpc0hhbmRsZVRyaWdnZXIoYXhpc1BvaW50ZXJNb2RlbCksXG4gICAgICAgIHNlcmllc01vZGVsczogW11cbiAgICAgIH07XG4gICAgICBheGVzSW5mb0luQ29vcmRTeXNba2V5XSA9IGF4aXNJbmZvO1xuICAgICAgcmVzdWx0LnNlcmllc0ludm9sdmVkIHw9IGludm9sdmVTZXJpZXM7XG4gICAgICB2YXIgZ3JvdXBJbmRleCA9IGdldExpbmtHcm91cEluZGV4KGxpbmtzT3B0aW9uLCBheGlzKTtcblxuICAgICAgaWYgKGdyb3VwSW5kZXggIT0gbnVsbCkge1xuICAgICAgICB2YXIgbGlua0dyb3VwID0gbGlua0dyb3Vwc1tncm91cEluZGV4XSB8fCAobGlua0dyb3Vwc1tncm91cEluZGV4XSA9IHtcbiAgICAgICAgICBheGVzSW5mbzoge31cbiAgICAgICAgfSk7XG4gICAgICAgIGxpbmtHcm91cC5heGVzSW5mb1trZXldID0gYXhpc0luZm87XG4gICAgICAgIGxpbmtHcm91cC5tYXBwZXIgPSBsaW5rc09wdGlvbltncm91cEluZGV4XS5tYXBwZXI7XG4gICAgICAgIGF4aXNJbmZvLmxpbmtHcm91cCA9IGxpbmtHcm91cDtcbiAgICAgIH1cbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBtYWtlQXhpc1BvaW50ZXJNb2RlbChheGlzLCBiYXNlVG9vbHRpcE1vZGVsLCBnbG9iYWxBeGlzUG9pbnRlck1vZGVsLCBlY01vZGVsLCBmcm9tVG9vbHRpcCwgdHJpZ2dlclRvb2x0aXApIHtcbiAgdmFyIHRvb2x0aXBBeGlzUG9pbnRlck1vZGVsID0gYmFzZVRvb2x0aXBNb2RlbC5nZXRNb2RlbCgnYXhpc1BvaW50ZXInKTtcbiAgdmFyIHZvbGF0aWxlT3B0aW9uID0ge307XG4gIGVhY2goWyd0eXBlJywgJ3NuYXAnLCAnbGluZVN0eWxlJywgJ3NoYWRvd1N0eWxlJywgJ2xhYmVsJywgJ2FuaW1hdGlvbicsICdhbmltYXRpb25EdXJhdGlvblVwZGF0ZScsICdhbmltYXRpb25FYXNpbmdVcGRhdGUnLCAneiddLCBmdW5jdGlvbiAoZmllbGQpIHtcbiAgICB2b2xhdGlsZU9wdGlvbltmaWVsZF0gPSB6clV0aWwuY2xvbmUodG9vbHRpcEF4aXNQb2ludGVyTW9kZWwuZ2V0KGZpZWxkKSk7XG4gIH0pOyAvLyBjYXRlZ29yeSBheGlzIGRvIG5vdCBhdXRvIHNuYXAsIG90aGVyd2lzZSBzb21lIHRpY2sgdGhhdCBkbyBub3RcbiAgLy8gaGFzIHZhbHVlIGNhbiBub3QgYmUgaG92ZXJlZC4gdmFsdWUvdGltZS9sb2cgYXhpcyBkZWZhdWx0IHNuYXAgaWZcbiAgLy8gdHJpZ2dlcmVkIGZyb20gdG9vbHRpcCBhbmQgdHJpZ2dlciB0b29sdGlwLlxuXG4gIHZvbGF0aWxlT3B0aW9uLnNuYXAgPSBheGlzLnR5cGUgIT09ICdjYXRlZ29yeScgJiYgISF0cmlnZ2VyVG9vbHRpcDsgLy8gQ29tcGF0aWJlbCB3aXRoIHByZXZpb3VzIGJlaGF2aW9yLCB0b29sdGlwIGF4aXMgZG8gbm90IHNob3cgbGFiZWwgYnkgZGVmYXVsdC5cbiAgLy8gT25seSB0aGVzZSBwcm9wZXJ0aWVzIGNhbiBiZSBvdmVycmlkZWQgZnJvbSB0b29sdGlwIHRvIGF4aXNQb2ludGVyLlxuXG4gIGlmICh0b29sdGlwQXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3R5cGUnKSA9PT0gJ2Nyb3NzJykge1xuICAgIHZvbGF0aWxlT3B0aW9uLnR5cGUgPSAnbGluZSc7XG4gIH1cblxuICB2YXIgbGFiZWxPcHRpb24gPSB2b2xhdGlsZU9wdGlvbi5sYWJlbCB8fCAodm9sYXRpbGVPcHRpb24ubGFiZWwgPSB7fSk7IC8vIEZvbGxvdyB0aGUgY29udmVudGlvbiwgZG8gbm90IHNob3cgbGFiZWwgd2hlbiB0cmlnZ2VyZWQgYnkgdG9vbHRpcCBieSBkZWZhdWx0LlxuXG4gIGxhYmVsT3B0aW9uLnNob3cgPT0gbnVsbCAmJiAobGFiZWxPcHRpb24uc2hvdyA9IGZhbHNlKTtcblxuICBpZiAoZnJvbVRvb2x0aXAgPT09ICdjcm9zcycpIHtcbiAgICAvLyBXaGVuICdjcm9zcycsIGJvdGggYXhlcyBzaG93IGxhYmVscy5cbiAgICB2YXIgdG9vbHRpcEF4aXNQb2ludGVyTGFiZWxTaG93ID0gdG9vbHRpcEF4aXNQb2ludGVyTW9kZWwuZ2V0KCdsYWJlbC5zaG93Jyk7XG4gICAgbGFiZWxPcHRpb24uc2hvdyA9IHRvb2x0aXBBeGlzUG9pbnRlckxhYmVsU2hvdyAhPSBudWxsID8gdG9vbHRpcEF4aXNQb2ludGVyTGFiZWxTaG93IDogdHJ1ZTsgLy8gSWYgdHJpZ2dlclRvb2x0aXAsIHRoaXMgaXMgYSBiYXNlIGF4aXMsIHdoaWNoIHNob3VsZCBiZXR0ZXIgbm90IHVzZSBjcm9zcyBzdHlsZVxuICAgIC8vIChjcm9zcyBzdHlsZSBpcyBkYXNoZWQgYnkgZGVmYXVsdClcblxuICAgIGlmICghdHJpZ2dlclRvb2x0aXApIHtcbiAgICAgIHZhciBjcm9zc1N0eWxlID0gdm9sYXRpbGVPcHRpb24ubGluZVN0eWxlID0gdG9vbHRpcEF4aXNQb2ludGVyTW9kZWwuZ2V0KCdjcm9zc1N0eWxlJyk7XG4gICAgICBjcm9zc1N0eWxlICYmIHpyVXRpbC5kZWZhdWx0cyhsYWJlbE9wdGlvbiwgY3Jvc3NTdHlsZS50ZXh0U3R5bGUpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBheGlzLm1vZGVsLmdldE1vZGVsKCdheGlzUG9pbnRlcicsIG5ldyBNb2RlbCh2b2xhdGlsZU9wdGlvbiwgZ2xvYmFsQXhpc1BvaW50ZXJNb2RlbCwgZWNNb2RlbCkpO1xufVxuXG5mdW5jdGlvbiBjb2xsZWN0U2VyaWVzSW5mbyhyZXN1bHQsIGVjTW9kZWwpIHtcbiAgLy8gUHJlcGFyZSBkYXRhIGZvciBheGlzIHRyaWdnZXJcbiAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIC8vIE5vdGljZSB0aGlzIGNhc2U6IHRoaXMgY29vcmRTeXMgaXMgYGNhcnRlc2lhbjJEYCBidXQgbm90IGBncmlkYC5cbiAgICB2YXIgY29vcmRTeXMgPSBzZXJpZXNNb2RlbC5jb29yZGluYXRlU3lzdGVtO1xuICAgIHZhciBzZXJpZXNUb29sdGlwVHJpZ2dlciA9IHNlcmllc01vZGVsLmdldCgndG9vbHRpcC50cmlnZ2VyJywgdHJ1ZSk7XG4gICAgdmFyIHNlcmllc1Rvb2x0aXBTaG93ID0gc2VyaWVzTW9kZWwuZ2V0KCd0b29sdGlwLnNob3cnLCB0cnVlKTtcblxuICAgIGlmICghY29vcmRTeXMgfHwgc2VyaWVzVG9vbHRpcFRyaWdnZXIgPT09ICdub25lJyB8fCBzZXJpZXNUb29sdGlwVHJpZ2dlciA9PT0gZmFsc2UgfHwgc2VyaWVzVG9vbHRpcFRyaWdnZXIgPT09ICdpdGVtJyB8fCBzZXJpZXNUb29sdGlwU2hvdyA9PT0gZmFsc2UgfHwgc2VyaWVzTW9kZWwuZ2V0KCdheGlzUG9pbnRlci5zaG93JywgdHJ1ZSkgPT09IGZhbHNlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgZWFjaChyZXN1bHQuY29vcmRTeXNBeGVzSW5mb1ttYWtlS2V5KGNvb3JkU3lzLm1vZGVsKV0sIGZ1bmN0aW9uIChheGlzSW5mbykge1xuICAgICAgdmFyIGF4aXMgPSBheGlzSW5mby5heGlzO1xuXG4gICAgICBpZiAoY29vcmRTeXMuZ2V0QXhpcyhheGlzLmRpbSkgPT09IGF4aXMpIHtcbiAgICAgICAgYXhpc0luZm8uc2VyaWVzTW9kZWxzLnB1c2goc2VyaWVzTW9kZWwpO1xuICAgICAgICBheGlzSW5mby5zZXJpZXNEYXRhQ291bnQgPT0gbnVsbCAmJiAoYXhpc0luZm8uc2VyaWVzRGF0YUNvdW50ID0gMCk7XG4gICAgICAgIGF4aXNJbmZvLnNlcmllc0RhdGFDb3VudCArPSBzZXJpZXNNb2RlbC5nZXREYXRhKCkuY291bnQoKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSwgdGhpcyk7XG59XG4vKipcbiAqIEZvciBleGFtcGxlOlxuICoge1xuICogICAgIGF4aXNQb2ludGVyOiB7XG4gKiAgICAgICAgIGxpbmtzOiBbe1xuICogICAgICAgICAgICAgeEF4aXNJbmRleDogWzIsIDRdLFxuICogICAgICAgICAgICAgeUF4aXNJbmRleDogJ2FsbCdcbiAqICAgICAgICAgfSwge1xuICogICAgICAgICAgICAgeEF4aXNJZDogWydhNScsICdhNyddLFxuICogICAgICAgICAgICAgeEF4aXNOYW1lOiAneHh4J1xuICogICAgICAgICB9XVxuICogICAgIH1cbiAqIH1cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldExpbmtHcm91cEluZGV4KGxpbmtzT3B0aW9uLCBheGlzKSB7XG4gIHZhciBheGlzTW9kZWwgPSBheGlzLm1vZGVsO1xuICB2YXIgZGltID0gYXhpcy5kaW07XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaW5rc09wdGlvbi5sZW5ndGg7IGkrKykge1xuICAgIHZhciBsaW5rT3B0aW9uID0gbGlua3NPcHRpb25baV0gfHwge307XG5cbiAgICBpZiAoY2hlY2tQcm9wSW5MaW5rKGxpbmtPcHRpb25bZGltICsgJ0F4aXNJZCddLCBheGlzTW9kZWwuaWQpIHx8IGNoZWNrUHJvcEluTGluayhsaW5rT3B0aW9uW2RpbSArICdBeGlzSW5kZXgnXSwgYXhpc01vZGVsLmNvbXBvbmVudEluZGV4KSB8fCBjaGVja1Byb3BJbkxpbmsobGlua09wdGlvbltkaW0gKyAnQXhpc05hbWUnXSwgYXhpc01vZGVsLm5hbWUpKSB7XG4gICAgICByZXR1cm4gaTtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gY2hlY2tQcm9wSW5MaW5rKGxpbmtQcm9wVmFsdWUsIGF4aXNQcm9wVmFsdWUpIHtcbiAgcmV0dXJuIGxpbmtQcm9wVmFsdWUgPT09ICdhbGwnIHx8IHpyVXRpbC5pc0FycmF5KGxpbmtQcm9wVmFsdWUpICYmIHpyVXRpbC5pbmRleE9mKGxpbmtQcm9wVmFsdWUsIGF4aXNQcm9wVmFsdWUpID49IDAgfHwgbGlua1Byb3BWYWx1ZSA9PT0gYXhpc1Byb3BWYWx1ZTtcbn1cblxuZnVuY3Rpb24gZml4VmFsdWUoYXhpc01vZGVsKSB7XG4gIHZhciBheGlzSW5mbyA9IGdldEF4aXNJbmZvKGF4aXNNb2RlbCk7XG5cbiAgaWYgKCFheGlzSW5mbykge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBheGlzUG9pbnRlck1vZGVsID0gYXhpc0luZm8uYXhpc1BvaW50ZXJNb2RlbDtcbiAgdmFyIHNjYWxlID0gYXhpc0luZm8uYXhpcy5zY2FsZTtcbiAgdmFyIG9wdGlvbiA9IGF4aXNQb2ludGVyTW9kZWwub3B0aW9uO1xuICB2YXIgc3RhdHVzID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3N0YXR1cycpO1xuICB2YXIgdmFsdWUgPSBheGlzUG9pbnRlck1vZGVsLmdldCgndmFsdWUnKTsgLy8gUGFyc2UgaW5pdCB2YWx1ZSBmb3IgY2F0ZWdvcnkgYW5kIHRpbWUgYXhpcy5cblxuICBpZiAodmFsdWUgIT0gbnVsbCkge1xuICAgIHZhbHVlID0gc2NhbGUucGFyc2UodmFsdWUpO1xuICB9XG5cbiAgdmFyIHVzZUhhbmRsZSA9IGlzSGFuZGxlVHJpZ2dlcihheGlzUG9pbnRlck1vZGVsKTsgLy8gSWYgYGhhbmRsZWAgdXNlZCwgYGF4aXNQb2ludGVyYCB3aWxsIGFsd2F5cyBiZSBkaXNwbGF5ZWQsIHNvIHZhbHVlXG4gIC8vIGFuZCBzdGF0dXMgc2hvdWxkIGJlIGluaXRpYWxpemVkLlxuXG4gIGlmIChzdGF0dXMgPT0gbnVsbCkge1xuICAgIG9wdGlvbi5zdGF0dXMgPSB1c2VIYW5kbGUgPyAnc2hvdycgOiAnaGlkZSc7XG4gIH1cblxuICB2YXIgZXh0ZW50ID0gc2NhbGUuZ2V0RXh0ZW50KCkuc2xpY2UoKTtcbiAgZXh0ZW50WzBdID4gZXh0ZW50WzFdICYmIGV4dGVudC5yZXZlcnNlKCk7XG5cbiAgaWYgKCAvLyBQaWNrIGEgdmFsdWUgb24gYXhpcyB3aGVuIGluaXRpYWxpemluZy5cbiAgdmFsdWUgPT0gbnVsbCAvLyBJZiBib3RoIGBoYW5kbGVgIGFuZCBgZGF0YVpvb21gIGFyZSB1c2VkLCB2YWx1ZSBtYXkgYmUgb3V0IG9mIGF4aXMgZXh0ZW50LFxuICAvLyB3aGVyZSB3ZSBzaG91bGQgcmUtcGljayBhIHZhbHVlIHRvIGtlZXAgYGhhbmRsZWAgZGlzcGxheWluZyBub3JtYWxseS5cbiAgfHwgdmFsdWUgPiBleHRlbnRbMV0pIHtcbiAgICAvLyBNYWtlIGhhbmRsZSBkaXNwbGF5ZWQgb24gdGhlIGVuZCBvZiB0aGUgYXhpcyB3aGVuIGluaXQsIHdoaWNoIGxvb2tzIGJldHRlci5cbiAgICB2YWx1ZSA9IGV4dGVudFsxXTtcbiAgfVxuXG4gIGlmICh2YWx1ZSA8IGV4dGVudFswXSkge1xuICAgIHZhbHVlID0gZXh0ZW50WzBdO1xuICB9XG5cbiAgb3B0aW9uLnZhbHVlID0gdmFsdWU7XG5cbiAgaWYgKHVzZUhhbmRsZSkge1xuICAgIG9wdGlvbi5zdGF0dXMgPSBheGlzSW5mby5heGlzLnNjYWxlLmlzQmxhbmsoKSA/ICdoaWRlJyA6ICdzaG93JztcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRBeGlzSW5mbyhheGlzTW9kZWwpIHtcbiAgdmFyIGNvb3JkU3lzQXhlc0luZm8gPSAoYXhpc01vZGVsLmVjTW9kZWwuZ2V0Q29tcG9uZW50KCdheGlzUG9pbnRlcicpIHx8IHt9KS5jb29yZFN5c0F4ZXNJbmZvO1xuICByZXR1cm4gY29vcmRTeXNBeGVzSW5mbyAmJiBjb29yZFN5c0F4ZXNJbmZvLmF4ZXNJbmZvW21ha2VLZXkoYXhpc01vZGVsKV07XG59XG5cbmZ1bmN0aW9uIGdldEF4aXNQb2ludGVyTW9kZWwoYXhpc01vZGVsKSB7XG4gIHZhciBheGlzSW5mbyA9IGdldEF4aXNJbmZvKGF4aXNNb2RlbCk7XG4gIHJldHVybiBheGlzSW5mbyAmJiBheGlzSW5mby5heGlzUG9pbnRlck1vZGVsO1xufVxuXG5mdW5jdGlvbiBpc0hhbmRsZVRyaWdnZXIoYXhpc1BvaW50ZXJNb2RlbCkge1xuICByZXR1cm4gISFheGlzUG9pbnRlck1vZGVsLmdldCgnaGFuZGxlLnNob3cnKTtcbn1cbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gbW9kZWxcbiAqIEByZXR1cm4ge3N0cmluZ30gdW5pcXVlIGtleVxuICovXG5cblxuZnVuY3Rpb24gbWFrZUtleShtb2RlbCkge1xuICByZXR1cm4gbW9kZWwudHlwZSArICd8fCcgKyBtb2RlbC5pZDtcbn1cblxuZXhwb3J0cy5jb2xsZWN0ID0gY29sbGVjdDtcbmV4cG9ydHMuZml4VmFsdWUgPSBmaXhWYWx1ZTtcbmV4cG9ydHMuZ2V0QXhpc0luZm8gPSBnZXRBeGlzSW5mbztcbmV4cG9ydHMuZ2V0QXhpc1BvaW50ZXJNb2RlbCA9IGdldEF4aXNQb2ludGVyTW9kZWw7XG5leHBvcnRzLm1ha2VLZXkgPSBtYWtlS2V5OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUF0QkE7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/modelHelper.js
