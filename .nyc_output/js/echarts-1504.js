/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ./config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrender = __webpack_require__(/*! zrender/lib/zrender */ "./node_modules/zrender/lib/zrender.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var colorTool = __webpack_require__(/*! zrender/lib/tool/color */ "./node_modules/zrender/lib/tool/color.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var timsort = __webpack_require__(/*! zrender/lib/core/timsort */ "./node_modules/zrender/lib/core/timsort.js");

var Eventful = __webpack_require__(/*! zrender/lib/mixin/Eventful */ "./node_modules/zrender/lib/mixin/Eventful.js");

var GlobalModel = __webpack_require__(/*! ./model/Global */ "./node_modules/echarts/lib/model/Global.js");

var ExtensionAPI = __webpack_require__(/*! ./ExtensionAPI */ "./node_modules/echarts/lib/ExtensionAPI.js");

var CoordinateSystemManager = __webpack_require__(/*! ./CoordinateSystem */ "./node_modules/echarts/lib/CoordinateSystem.js");

var OptionManager = __webpack_require__(/*! ./model/OptionManager */ "./node_modules/echarts/lib/model/OptionManager.js");

var backwardCompat = __webpack_require__(/*! ./preprocessor/backwardCompat */ "./node_modules/echarts/lib/preprocessor/backwardCompat.js");

var dataStack = __webpack_require__(/*! ./processor/dataStack */ "./node_modules/echarts/lib/processor/dataStack.js");

var ComponentModel = __webpack_require__(/*! ./model/Component */ "./node_modules/echarts/lib/model/Component.js");

var SeriesModel = __webpack_require__(/*! ./model/Series */ "./node_modules/echarts/lib/model/Series.js");

var ComponentView = __webpack_require__(/*! ./view/Component */ "./node_modules/echarts/lib/view/Component.js");

var ChartView = __webpack_require__(/*! ./view/Chart */ "./node_modules/echarts/lib/view/Chart.js");

var graphic = __webpack_require__(/*! ./util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var modelUtil = __webpack_require__(/*! ./util/model */ "./node_modules/echarts/lib/util/model.js");

var _throttle = __webpack_require__(/*! ./util/throttle */ "./node_modules/echarts/lib/util/throttle.js");

var throttle = _throttle.throttle;

var seriesColor = __webpack_require__(/*! ./visual/seriesColor */ "./node_modules/echarts/lib/visual/seriesColor.js");

var aria = __webpack_require__(/*! ./visual/aria */ "./node_modules/echarts/lib/visual/aria.js");

var loadingDefault = __webpack_require__(/*! ./loading/default */ "./node_modules/echarts/lib/loading/default.js");

var Scheduler = __webpack_require__(/*! ./stream/Scheduler */ "./node_modules/echarts/lib/stream/Scheduler.js");

var lightTheme = __webpack_require__(/*! ./theme/light */ "./node_modules/echarts/lib/theme/light.js");

var darkTheme = __webpack_require__(/*! ./theme/dark */ "./node_modules/echarts/lib/theme/dark.js");

__webpack_require__(/*! ./component/dataset */ "./node_modules/echarts/lib/component/dataset.js");

var mapDataStorage = __webpack_require__(/*! ./coord/geo/mapDataStorage */ "./node_modules/echarts/lib/coord/geo/mapDataStorage.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var assert = zrUtil.assert;
var each = zrUtil.each;
var isFunction = zrUtil.isFunction;
var isObject = zrUtil.isObject;
var parseClassType = ComponentModel.parseClassType;
var version = '4.2.1';
var dependencies = {
  zrender: '4.0.6'
};
var TEST_FRAME_REMAIN_TIME = 1;
var PRIORITY_PROCESSOR_FILTER = 1000;
var PRIORITY_PROCESSOR_STATISTIC = 5000;
var PRIORITY_VISUAL_LAYOUT = 1000;
var PRIORITY_VISUAL_GLOBAL = 2000;
var PRIORITY_VISUAL_CHART = 3000;
var PRIORITY_VISUAL_COMPONENT = 4000; // FIXME
// necessary?

var PRIORITY_VISUAL_BRUSH = 5000;
var PRIORITY = {
  PROCESSOR: {
    FILTER: PRIORITY_PROCESSOR_FILTER,
    STATISTIC: PRIORITY_PROCESSOR_STATISTIC
  },
  VISUAL: {
    LAYOUT: PRIORITY_VISUAL_LAYOUT,
    GLOBAL: PRIORITY_VISUAL_GLOBAL,
    CHART: PRIORITY_VISUAL_CHART,
    COMPONENT: PRIORITY_VISUAL_COMPONENT,
    BRUSH: PRIORITY_VISUAL_BRUSH
  }
}; // Main process have three entries: `setOption`, `dispatchAction` and `resize`,
// where they must not be invoked nestedly, except the only case: invoke
// dispatchAction with updateMethod "none" in main process.
// This flag is used to carry out this rule.
// All events will be triggered out side main process (i.e. when !this[IN_MAIN_PROCESS]).

var IN_MAIN_PROCESS = '__flagInMainProcess';
var OPTION_UPDATED = '__optionUpdated';
var ACTION_REG = /^[a-zA-Z0-9_]+$/;

function createRegisterEventWithLowercaseName(method) {
  return function (eventName, handler, context) {
    // Event name is all lowercase
    eventName = eventName && eventName.toLowerCase();
    Eventful.prototype[method].call(this, eventName, handler, context);
  };
}
/**
 * @module echarts~MessageCenter
 */


function MessageCenter() {
  Eventful.call(this);
}

MessageCenter.prototype.on = createRegisterEventWithLowercaseName('on');
MessageCenter.prototype.off = createRegisterEventWithLowercaseName('off');
MessageCenter.prototype.one = createRegisterEventWithLowercaseName('one');
zrUtil.mixin(MessageCenter, Eventful);
/**
 * @module echarts~ECharts
 */

function ECharts(dom, theme, opts) {
  opts = opts || {}; // Get theme by name

  if (typeof theme === 'string') {
    theme = themeStorage[theme];
  }
  /**
   * @type {string}
   */


  this.id;
  /**
   * Group id
   * @type {string}
   */

  this.group;
  /**
   * @type {HTMLElement}
   * @private
   */

  this._dom = dom;
  var defaultRenderer = 'canvas';
  /**
   * @type {module:zrender/ZRender}
   * @private
   */

  var zr = this._zr = zrender.init(dom, {
    renderer: opts.renderer || defaultRenderer,
    devicePixelRatio: opts.devicePixelRatio,
    width: opts.width,
    height: opts.height
  });
  /**
   * Expect 60 pfs.
   * @type {Function}
   * @private
   */

  this._throttledZrFlush = throttle(zrUtil.bind(zr.flush, zr), 17);
  var theme = zrUtil.clone(theme);
  theme && backwardCompat(theme, true);
  /**
   * @type {Object}
   * @private
   */

  this._theme = theme;
  /**
   * @type {Array.<module:echarts/view/Chart>}
   * @private
   */

  this._chartsViews = [];
  /**
   * @type {Object.<string, module:echarts/view/Chart>}
   * @private
   */

  this._chartsMap = {};
  /**
   * @type {Array.<module:echarts/view/Component>}
   * @private
   */

  this._componentsViews = [];
  /**
   * @type {Object.<string, module:echarts/view/Component>}
   * @private
   */

  this._componentsMap = {};
  /**
   * @type {module:echarts/CoordinateSystem}
   * @private
   */

  this._coordSysMgr = new CoordinateSystemManager();
  /**
   * @type {module:echarts/ExtensionAPI}
   * @private
   */

  var api = this._api = createExtensionAPI(this); // Sort on demand

  function prioritySortFunc(a, b) {
    return a.__prio - b.__prio;
  }

  timsort(visualFuncs, prioritySortFunc);
  timsort(dataProcessorFuncs, prioritySortFunc);
  /**
   * @type {module:echarts/stream/Scheduler}
   */

  this._scheduler = new Scheduler(this, api, dataProcessorFuncs, visualFuncs);
  Eventful.call(this, this._ecEventProcessor = new EventProcessor());
  /**
   * @type {module:echarts~MessageCenter}
   * @private
   */

  this._messageCenter = new MessageCenter(); // Init mouse events

  this._initEvents(); // In case some people write `window.onresize = chart.resize`


  this.resize = zrUtil.bind(this.resize, this); // Can't dispatch action during rendering procedure

  this._pendingActions = [];
  zr.animation.on('frame', this._onframe, this);
  bindRenderedEvent(zr, this); // ECharts instance can be used as value.

  zrUtil.setAsPrimitive(this);
}

var echartsProto = ECharts.prototype;

echartsProto._onframe = function () {
  if (this._disposed) {
    return;
  }

  var scheduler = this._scheduler; // Lazy update

  if (this[OPTION_UPDATED]) {
    var silent = this[OPTION_UPDATED].silent;
    this[IN_MAIN_PROCESS] = true;
    prepare(this);
    updateMethods.update.call(this);
    this[IN_MAIN_PROCESS] = false;
    this[OPTION_UPDATED] = false;
    flushPendingActions.call(this, silent);
    triggerUpdatedEvent.call(this, silent);
  } // Avoid do both lazy update and progress in one frame.
  else if (scheduler.unfinished) {
      // Stream progress.
      var remainTime = TEST_FRAME_REMAIN_TIME;
      var ecModel = this._model;
      var api = this._api;
      scheduler.unfinished = false;

      do {
        var startTime = +new Date();
        scheduler.performSeriesTasks(ecModel); // Currently dataProcessorFuncs do not check threshold.

        scheduler.performDataProcessorTasks(ecModel);
        updateStreamModes(this, ecModel); // Do not update coordinate system here. Because that coord system update in
        // each frame is not a good user experience. So we follow the rule that
        // the extent of the coordinate system is determin in the first frame (the
        // frame is executed immedietely after task reset.
        // this._coordSysMgr.update(ecModel, api);
        // console.log('--- ec frame visual ---', remainTime);

        scheduler.performVisualTasks(ecModel);
        renderSeries(this, this._model, api, 'remain');
        remainTime -= +new Date() - startTime;
      } while (remainTime > 0 && scheduler.unfinished); // Call flush explicitly for trigger finished event.


      if (!scheduler.unfinished) {
        this._zr.flush();
      } // Else, zr flushing be ensue within the same frame,
      // because zr flushing is after onframe event.

    }
};
/**
 * @return {HTMLElement}
 */


echartsProto.getDom = function () {
  return this._dom;
};
/**
 * @return {module:zrender~ZRender}
 */


echartsProto.getZr = function () {
  return this._zr;
};
/**
 * Usage:
 * chart.setOption(option, notMerge, lazyUpdate);
 * chart.setOption(option, {
 *     notMerge: ...,
 *     lazyUpdate: ...,
 *     silent: ...
 * });
 *
 * @param {Object} option
 * @param {Object|boolean} [opts] opts or notMerge.
 * @param {boolean} [opts.notMerge=false]
 * @param {boolean} [opts.lazyUpdate=false] Useful when setOption frequently.
 */


echartsProto.setOption = function (option, notMerge, lazyUpdate) {
  var silent;

  if (isObject(notMerge)) {
    lazyUpdate = notMerge.lazyUpdate;
    silent = notMerge.silent;
    notMerge = notMerge.notMerge;
  }

  this[IN_MAIN_PROCESS] = true;

  if (!this._model || notMerge) {
    var optionManager = new OptionManager(this._api);
    var theme = this._theme;
    var ecModel = this._model = new GlobalModel(null, null, theme, optionManager);
    ecModel.scheduler = this._scheduler;
    ecModel.init(null, null, theme, optionManager);
  }

  this._model.setOption(option, optionPreprocessorFuncs);

  if (lazyUpdate) {
    this[OPTION_UPDATED] = {
      silent: silent
    };
    this[IN_MAIN_PROCESS] = false;
  } else {
    prepare(this);
    updateMethods.update.call(this); // Ensure zr refresh sychronously, and then pixel in canvas can be
    // fetched after `setOption`.

    this._zr.flush();

    this[OPTION_UPDATED] = false;
    this[IN_MAIN_PROCESS] = false;
    flushPendingActions.call(this, silent);
    triggerUpdatedEvent.call(this, silent);
  }
};
/**
 * @DEPRECATED
 */


echartsProto.setTheme = function () {
  console.error('ECharts#setTheme() is DEPRECATED in ECharts 3.0');
};
/**
 * @return {module:echarts/model/Global}
 */


echartsProto.getModel = function () {
  return this._model;
};
/**
 * @return {Object}
 */


echartsProto.getOption = function () {
  return this._model && this._model.getOption();
};
/**
 * @return {number}
 */


echartsProto.getWidth = function () {
  return this._zr.getWidth();
};
/**
 * @return {number}
 */


echartsProto.getHeight = function () {
  return this._zr.getHeight();
};
/**
 * @return {number}
 */


echartsProto.getDevicePixelRatio = function () {
  return this._zr.painter.dpr || window.devicePixelRatio || 1;
};
/**
 * Get canvas which has all thing rendered
 * @param {Object} opts
 * @param {string} [opts.backgroundColor]
 * @return {string}
 */


echartsProto.getRenderedCanvas = function (opts) {
  if (!env.canvasSupported) {
    return;
  }

  opts = opts || {};
  opts.pixelRatio = opts.pixelRatio || 1;
  opts.backgroundColor = opts.backgroundColor || this._model.get('backgroundColor');
  var zr = this._zr; // var list = zr.storage.getDisplayList();
  // Stop animations
  // Never works before in init animation, so remove it.
  // zrUtil.each(list, function (el) {
  //     el.stopAnimation(true);
  // });

  return zr.painter.getRenderedCanvas(opts);
};
/**
 * Get svg data url
 * @return {string}
 */


echartsProto.getSvgDataUrl = function () {
  if (!env.svgSupported) {
    return;
  }

  var zr = this._zr;
  var list = zr.storage.getDisplayList(); // Stop animations

  zrUtil.each(list, function (el) {
    el.stopAnimation(true);
  });
  return zr.painter.pathToDataUrl();
};
/**
 * @return {string}
 * @param {Object} opts
 * @param {string} [opts.type='png']
 * @param {string} [opts.pixelRatio=1]
 * @param {string} [opts.backgroundColor]
 * @param {string} [opts.excludeComponents]
 */


echartsProto.getDataURL = function (opts) {
  opts = opts || {};
  var excludeComponents = opts.excludeComponents;
  var ecModel = this._model;
  var excludesComponentViews = [];
  var self = this;
  each(excludeComponents, function (componentType) {
    ecModel.eachComponent({
      mainType: componentType
    }, function (component) {
      var view = self._componentsMap[component.__viewId];

      if (!view.group.ignore) {
        excludesComponentViews.push(view);
        view.group.ignore = true;
      }
    });
  });
  var url = this._zr.painter.getType() === 'svg' ? this.getSvgDataUrl() : this.getRenderedCanvas(opts).toDataURL('image/' + (opts && opts.type || 'png'));
  each(excludesComponentViews, function (view) {
    view.group.ignore = false;
  });
  return url;
};
/**
 * @return {string}
 * @param {Object} opts
 * @param {string} [opts.type='png']
 * @param {string} [opts.pixelRatio=1]
 * @param {string} [opts.backgroundColor]
 */


echartsProto.getConnectedDataURL = function (opts) {
  if (!env.canvasSupported) {
    return;
  }

  var groupId = this.group;
  var mathMin = Math.min;
  var mathMax = Math.max;
  var MAX_NUMBER = Infinity;

  if (connectedGroups[groupId]) {
    var left = MAX_NUMBER;
    var top = MAX_NUMBER;
    var right = -MAX_NUMBER;
    var bottom = -MAX_NUMBER;
    var canvasList = [];
    var dpr = opts && opts.pixelRatio || 1;
    zrUtil.each(instances, function (chart, id) {
      if (chart.group === groupId) {
        var canvas = chart.getRenderedCanvas(zrUtil.clone(opts));
        var boundingRect = chart.getDom().getBoundingClientRect();
        left = mathMin(boundingRect.left, left);
        top = mathMin(boundingRect.top, top);
        right = mathMax(boundingRect.right, right);
        bottom = mathMax(boundingRect.bottom, bottom);
        canvasList.push({
          dom: canvas,
          left: boundingRect.left,
          top: boundingRect.top
        });
      }
    });
    left *= dpr;
    top *= dpr;
    right *= dpr;
    bottom *= dpr;
    var width = right - left;
    var height = bottom - top;
    var targetCanvas = zrUtil.createCanvas();
    targetCanvas.width = width;
    targetCanvas.height = height;
    var zr = zrender.init(targetCanvas);
    each(canvasList, function (item) {
      var img = new graphic.Image({
        style: {
          x: item.left * dpr - left,
          y: item.top * dpr - top,
          image: item.dom
        }
      });
      zr.add(img);
    });
    zr.refreshImmediately();
    return targetCanvas.toDataURL('image/' + (opts && opts.type || 'png'));
  } else {
    return this.getDataURL(opts);
  }
};
/**
 * Convert from logical coordinate system to pixel coordinate system.
 * See CoordinateSystem#convertToPixel.
 * @param {string|Object} finder
 *        If string, e.g., 'geo', means {geoIndex: 0}.
 *        If Object, could contain some of these properties below:
 *        {
 *            seriesIndex / seriesId / seriesName,
 *            geoIndex / geoId, geoName,
 *            bmapIndex / bmapId / bmapName,
 *            xAxisIndex / xAxisId / xAxisName,
 *            yAxisIndex / yAxisId / yAxisName,
 *            gridIndex / gridId / gridName,
 *            ... (can be extended)
 *        }
 * @param {Array|number} value
 * @return {Array|number} result
 */


echartsProto.convertToPixel = zrUtil.curry(doConvertPixel, 'convertToPixel');
/**
 * Convert from pixel coordinate system to logical coordinate system.
 * See CoordinateSystem#convertFromPixel.
 * @param {string|Object} finder
 *        If string, e.g., 'geo', means {geoIndex: 0}.
 *        If Object, could contain some of these properties below:
 *        {
 *            seriesIndex / seriesId / seriesName,
 *            geoIndex / geoId / geoName,
 *            bmapIndex / bmapId / bmapName,
 *            xAxisIndex / xAxisId / xAxisName,
 *            yAxisIndex / yAxisId / yAxisName
 *            gridIndex / gridId / gridName,
 *            ... (can be extended)
 *        }
 * @param {Array|number} value
 * @return {Array|number} result
 */

echartsProto.convertFromPixel = zrUtil.curry(doConvertPixel, 'convertFromPixel');

function doConvertPixel(methodName, finder, value) {
  var ecModel = this._model;

  var coordSysList = this._coordSysMgr.getCoordinateSystems();

  var result;
  finder = modelUtil.parseFinder(ecModel, finder);

  for (var i = 0; i < coordSysList.length; i++) {
    var coordSys = coordSysList[i];

    if (coordSys[methodName] && (result = coordSys[methodName](ecModel, finder, value)) != null) {
      return result;
    }
  }
}
/**
 * Is the specified coordinate systems or components contain the given pixel point.
 * @param {string|Object} finder
 *        If string, e.g., 'geo', means {geoIndex: 0}.
 *        If Object, could contain some of these properties below:
 *        {
 *            seriesIndex / seriesId / seriesName,
 *            geoIndex / geoId / geoName,
 *            bmapIndex / bmapId / bmapName,
 *            xAxisIndex / xAxisId / xAxisName,
 *            yAxisIndex / yAxisId / yAxisName,
 *            gridIndex / gridId / gridName,
 *            ... (can be extended)
 *        }
 * @param {Array|number} value
 * @return {boolean} result
 */


echartsProto.containPixel = function (finder, value) {
  var ecModel = this._model;
  var result;
  finder = modelUtil.parseFinder(ecModel, finder);
  zrUtil.each(finder, function (models, key) {
    key.indexOf('Models') >= 0 && zrUtil.each(models, function (model) {
      var coordSys = model.coordinateSystem;

      if (coordSys && coordSys.containPoint) {
        result |= !!coordSys.containPoint(value);
      } else if (key === 'seriesModels') {
        var view = this._chartsMap[model.__viewId];

        if (view && view.containPoint) {
          result |= view.containPoint(value, model);
        } else {}
      } else {}
    }, this);
  }, this);
  return !!result;
};
/**
 * Get visual from series or data.
 * @param {string|Object} finder
 *        If string, e.g., 'series', means {seriesIndex: 0}.
 *        If Object, could contain some of these properties below:
 *        {
 *            seriesIndex / seriesId / seriesName,
 *            dataIndex / dataIndexInside
 *        }
 *        If dataIndex is not specified, series visual will be fetched,
 *        but not data item visual.
 *        If all of seriesIndex, seriesId, seriesName are not specified,
 *        visual will be fetched from first series.
 * @param {string} visualType 'color', 'symbol', 'symbolSize'
 */


echartsProto.getVisual = function (finder, visualType) {
  var ecModel = this._model;
  finder = modelUtil.parseFinder(ecModel, finder, {
    defaultMainType: 'series'
  });
  var seriesModel = finder.seriesModel;
  var data = seriesModel.getData();
  var dataIndexInside = finder.hasOwnProperty('dataIndexInside') ? finder.dataIndexInside : finder.hasOwnProperty('dataIndex') ? data.indexOfRawIndex(finder.dataIndex) : null;
  return dataIndexInside != null ? data.getItemVisual(dataIndexInside, visualType) : data.getVisual(visualType);
};
/**
 * Get view of corresponding component model
 * @param  {module:echarts/model/Component} componentModel
 * @return {module:echarts/view/Component}
 */


echartsProto.getViewOfComponentModel = function (componentModel) {
  return this._componentsMap[componentModel.__viewId];
};
/**
 * Get view of corresponding series model
 * @param  {module:echarts/model/Series} seriesModel
 * @return {module:echarts/view/Chart}
 */


echartsProto.getViewOfSeriesModel = function (seriesModel) {
  return this._chartsMap[seriesModel.__viewId];
};

var updateMethods = {
  prepareAndUpdate: function prepareAndUpdate(payload) {
    prepare(this);
    updateMethods.update.call(this, payload);
  },

  /**
   * @param {Object} payload
   * @private
   */
  update: function update(payload) {
    // console.profile && console.profile('update');
    var ecModel = this._model;
    var api = this._api;
    var zr = this._zr;
    var coordSysMgr = this._coordSysMgr;
    var scheduler = this._scheduler; // update before setOption

    if (!ecModel) {
      return;
    }

    scheduler.restoreData(ecModel, payload);
    scheduler.performSeriesTasks(ecModel); // TODO
    // Save total ecModel here for undo/redo (after restoring data and before processing data).
    // Undo (restoration of total ecModel) can be carried out in 'action' or outside API call.
    // Create new coordinate system each update
    // In LineView may save the old coordinate system and use it to get the orignal point

    coordSysMgr.create(ecModel, api);
    scheduler.performDataProcessorTasks(ecModel, payload); // Current stream render is not supported in data process. So we can update
    // stream modes after data processing, where the filtered data is used to
    // deteming whether use progressive rendering.

    updateStreamModes(this, ecModel); // We update stream modes before coordinate system updated, then the modes info
    // can be fetched when coord sys updating (consider the barGrid extent fix). But
    // the drawback is the full coord info can not be fetched. Fortunately this full
    // coord is not requied in stream mode updater currently.

    coordSysMgr.update(ecModel, api);
    clearColorPalette(ecModel);
    scheduler.performVisualTasks(ecModel, payload);
    render(this, ecModel, api, payload); // Set background

    var backgroundColor = ecModel.get('backgroundColor') || 'transparent'; // In IE8

    if (!env.canvasSupported) {
      var colorArr = colorTool.parse(backgroundColor);
      backgroundColor = colorTool.stringify(colorArr, 'rgb');

      if (colorArr[3] === 0) {
        backgroundColor = 'transparent';
      }
    } else {
      zr.setBackgroundColor(backgroundColor);
    }

    performPostUpdateFuncs(ecModel, api); // console.profile && console.profileEnd('update');
  },

  /**
   * @param {Object} payload
   * @private
   */
  updateTransform: function updateTransform(payload) {
    var ecModel = this._model;
    var ecIns = this;
    var api = this._api; // update before setOption

    if (!ecModel) {
      return;
    } // ChartView.markUpdateMethod(payload, 'updateTransform');


    var componentDirtyList = [];
    ecModel.eachComponent(function (componentType, componentModel) {
      var componentView = ecIns.getViewOfComponentModel(componentModel);

      if (componentView && componentView.__alive) {
        if (componentView.updateTransform) {
          var result = componentView.updateTransform(componentModel, ecModel, api, payload);
          result && result.update && componentDirtyList.push(componentView);
        } else {
          componentDirtyList.push(componentView);
        }
      }
    });
    var seriesDirtyMap = zrUtil.createHashMap();
    ecModel.eachSeries(function (seriesModel) {
      var chartView = ecIns._chartsMap[seriesModel.__viewId];

      if (chartView.updateTransform) {
        var result = chartView.updateTransform(seriesModel, ecModel, api, payload);
        result && result.update && seriesDirtyMap.set(seriesModel.uid, 1);
      } else {
        seriesDirtyMap.set(seriesModel.uid, 1);
      }
    });
    clearColorPalette(ecModel); // Keep pipe to the exist pipeline because it depends on the render task of the full pipeline.
    // this._scheduler.performVisualTasks(ecModel, payload, 'layout', true);

    this._scheduler.performVisualTasks(ecModel, payload, {
      setDirty: true,
      dirtyMap: seriesDirtyMap
    }); // Currently, not call render of components. Geo render cost a lot.
    // renderComponents(ecIns, ecModel, api, payload, componentDirtyList);


    renderSeries(ecIns, ecModel, api, payload, seriesDirtyMap);
    performPostUpdateFuncs(ecModel, this._api);
  },

  /**
   * @param {Object} payload
   * @private
   */
  updateView: function updateView(payload) {
    var ecModel = this._model; // update before setOption

    if (!ecModel) {
      return;
    }

    ChartView.markUpdateMethod(payload, 'updateView');
    clearColorPalette(ecModel); // Keep pipe to the exist pipeline because it depends on the render task of the full pipeline.

    this._scheduler.performVisualTasks(ecModel, payload, {
      setDirty: true
    });

    render(this, this._model, this._api, payload);
    performPostUpdateFuncs(ecModel, this._api);
  },

  /**
   * @param {Object} payload
   * @private
   */
  updateVisual: function updateVisual(payload) {
    updateMethods.update.call(this, payload); // var ecModel = this._model;
    // // update before setOption
    // if (!ecModel) {
    //     return;
    // }
    // ChartView.markUpdateMethod(payload, 'updateVisual');
    // clearColorPalette(ecModel);
    // // Keep pipe to the exist pipeline because it depends on the render task of the full pipeline.
    // this._scheduler.performVisualTasks(ecModel, payload, {visualType: 'visual', setDirty: true});
    // render(this, this._model, this._api, payload);
    // performPostUpdateFuncs(ecModel, this._api);
  },

  /**
   * @param {Object} payload
   * @private
   */
  updateLayout: function updateLayout(payload) {
    updateMethods.update.call(this, payload); // var ecModel = this._model;
    // // update before setOption
    // if (!ecModel) {
    //     return;
    // }
    // ChartView.markUpdateMethod(payload, 'updateLayout');
    // // Keep pipe to the exist pipeline because it depends on the render task of the full pipeline.
    // // this._scheduler.performVisualTasks(ecModel, payload, 'layout', true);
    // this._scheduler.performVisualTasks(ecModel, payload, {setDirty: true});
    // render(this, this._model, this._api, payload);
    // performPostUpdateFuncs(ecModel, this._api);
  }
};

function prepare(ecIns) {
  var ecModel = ecIns._model;
  var scheduler = ecIns._scheduler;
  scheduler.restorePipelines(ecModel);
  scheduler.prepareStageTasks();
  prepareView(ecIns, 'component', ecModel, scheduler);
  prepareView(ecIns, 'chart', ecModel, scheduler);
  scheduler.plan();
}
/**
 * @private
 */


function updateDirectly(ecIns, method, payload, mainType, subType) {
  var ecModel = ecIns._model; // broadcast

  if (!mainType) {
    // FIXME
    // Chart will not be update directly here, except set dirty.
    // But there is no such scenario now.
    each(ecIns._componentsViews.concat(ecIns._chartsViews), callView);
    return;
  }

  var query = {};
  query[mainType + 'Id'] = payload[mainType + 'Id'];
  query[mainType + 'Index'] = payload[mainType + 'Index'];
  query[mainType + 'Name'] = payload[mainType + 'Name'];
  var condition = {
    mainType: mainType,
    query: query
  };
  subType && (condition.subType = subType); // subType may be '' by parseClassType;

  var excludeSeriesId = payload.excludeSeriesId;

  if (excludeSeriesId != null) {
    excludeSeriesId = zrUtil.createHashMap(modelUtil.normalizeToArray(excludeSeriesId));
  } // If dispatchAction before setOption, do nothing.


  ecModel && ecModel.eachComponent(condition, function (model) {
    if (!excludeSeriesId || excludeSeriesId.get(model.id) == null) {
      callView(ecIns[mainType === 'series' ? '_chartsMap' : '_componentsMap'][model.__viewId]);
    }
  }, ecIns);

  function callView(view) {
    view && view.__alive && view[method] && view[method](view.__model, ecModel, ecIns._api, payload);
  }
}
/**
 * Resize the chart
 * @param {Object} opts
 * @param {number} [opts.width] Can be 'auto' (the same as null/undefined)
 * @param {number} [opts.height] Can be 'auto' (the same as null/undefined)
 * @param {boolean} [opts.silent=false]
 */


echartsProto.resize = function (opts) {
  this._zr.resize(opts);

  var ecModel = this._model; // Resize loading effect

  this._loadingFX && this._loadingFX.resize();

  if (!ecModel) {
    return;
  }

  var optionChanged = ecModel.resetOption('media');
  var silent = opts && opts.silent;
  this[IN_MAIN_PROCESS] = true;
  optionChanged && prepare(this);
  updateMethods.update.call(this);
  this[IN_MAIN_PROCESS] = false;
  flushPendingActions.call(this, silent);
  triggerUpdatedEvent.call(this, silent);
};

function updateStreamModes(ecIns, ecModel) {
  var chartsMap = ecIns._chartsMap;
  var scheduler = ecIns._scheduler;
  ecModel.eachSeries(function (seriesModel) {
    scheduler.updateStreamModes(seriesModel, chartsMap[seriesModel.__viewId]);
  });
}
/**
 * Show loading effect
 * @param  {string} [name='default']
 * @param  {Object} [cfg]
 */


echartsProto.showLoading = function (name, cfg) {
  if (isObject(name)) {
    cfg = name;
    name = '';
  }

  name = name || 'default';
  this.hideLoading();

  if (!loadingEffects[name]) {
    return;
  }

  var el = loadingEffects[name](this._api, cfg);
  var zr = this._zr;
  this._loadingFX = el;
  zr.add(el);
};
/**
 * Hide loading effect
 */


echartsProto.hideLoading = function () {
  this._loadingFX && this._zr.remove(this._loadingFX);
  this._loadingFX = null;
};
/**
 * @param {Object} eventObj
 * @return {Object}
 */


echartsProto.makeActionFromEvent = function (eventObj) {
  var payload = zrUtil.extend({}, eventObj);
  payload.type = eventActionMap[eventObj.type];
  return payload;
};
/**
 * @pubilc
 * @param {Object} payload
 * @param {string} [payload.type] Action type
 * @param {Object|boolean} [opt] If pass boolean, means opt.silent
 * @param {boolean} [opt.silent=false] Whether trigger events.
 * @param {boolean} [opt.flush=undefined]
 *                  true: Flush immediately, and then pixel in canvas can be fetched
 *                      immediately. Caution: it might affect performance.
 *                  false: Not not flush.
 *                  undefined: Auto decide whether perform flush.
 */


echartsProto.dispatchAction = function (payload, opt) {
  if (!isObject(opt)) {
    opt = {
      silent: !!opt
    };
  }

  if (!actions[payload.type]) {
    return;
  } // Avoid dispatch action before setOption. Especially in `connect`.


  if (!this._model) {
    return;
  } // May dispatchAction in rendering procedure


  if (this[IN_MAIN_PROCESS]) {
    this._pendingActions.push(payload);

    return;
  }

  doDispatchAction.call(this, payload, opt.silent);

  if (opt.flush) {
    this._zr.flush(true);
  } else if (opt.flush !== false && env.browser.weChat) {
    // In WeChat embeded browser, `requestAnimationFrame` and `setInterval`
    // hang when sliding page (on touch event), which cause that zr does not
    // refresh util user interaction finished, which is not expected.
    // But `dispatchAction` may be called too frequently when pan on touch
    // screen, which impacts performance if do not throttle them.
    this._throttledZrFlush();
  }

  flushPendingActions.call(this, opt.silent);
  triggerUpdatedEvent.call(this, opt.silent);
};

function doDispatchAction(payload, silent) {
  var payloadType = payload.type;
  var escapeConnect = payload.escapeConnect;
  var actionWrap = actions[payloadType];
  var actionInfo = actionWrap.actionInfo;
  var cptType = (actionInfo.update || 'update').split(':');
  var updateMethod = cptType.pop();
  cptType = cptType[0] != null && parseClassType(cptType[0]);
  this[IN_MAIN_PROCESS] = true;
  var payloads = [payload];
  var batched = false; // Batch action

  if (payload.batch) {
    batched = true;
    payloads = zrUtil.map(payload.batch, function (item) {
      item = zrUtil.defaults(zrUtil.extend({}, item), payload);
      item.batch = null;
      return item;
    });
  }

  var eventObjBatch = [];
  var eventObj;
  var isHighDown = payloadType === 'highlight' || payloadType === 'downplay';
  each(payloads, function (batchItem) {
    // Action can specify the event by return it.
    eventObj = actionWrap.action(batchItem, this._model, this._api); // Emit event outside

    eventObj = eventObj || zrUtil.extend({}, batchItem); // Convert type to eventType

    eventObj.type = actionInfo.event || eventObj.type;
    eventObjBatch.push(eventObj); // light update does not perform data process, layout and visual.

    if (isHighDown) {
      // method, payload, mainType, subType
      updateDirectly(this, updateMethod, batchItem, 'series');
    } else if (cptType) {
      updateDirectly(this, updateMethod, batchItem, cptType.main, cptType.sub);
    }
  }, this);

  if (updateMethod !== 'none' && !isHighDown && !cptType) {
    // Still dirty
    if (this[OPTION_UPDATED]) {
      // FIXME Pass payload ?
      prepare(this);
      updateMethods.update.call(this, payload);
      this[OPTION_UPDATED] = false;
    } else {
      updateMethods[updateMethod].call(this, payload);
    }
  } // Follow the rule of action batch


  if (batched) {
    eventObj = {
      type: actionInfo.event || payloadType,
      escapeConnect: escapeConnect,
      batch: eventObjBatch
    };
  } else {
    eventObj = eventObjBatch[0];
  }

  this[IN_MAIN_PROCESS] = false;
  !silent && this._messageCenter.trigger(eventObj.type, eventObj);
}

function flushPendingActions(silent) {
  var pendingActions = this._pendingActions;

  while (pendingActions.length) {
    var payload = pendingActions.shift();
    doDispatchAction.call(this, payload, silent);
  }
}

function triggerUpdatedEvent(silent) {
  !silent && this.trigger('updated');
}
/**
 * Event `rendered` is triggered when zr
 * rendered. It is useful for realtime
 * snapshot (reflect animation).
 *
 * Event `finished` is triggered when:
 * (1) zrender rendering finished.
 * (2) initial animation finished.
 * (3) progressive rendering finished.
 * (4) no pending action.
 * (5) no delayed setOption needs to be processed.
 */


function bindRenderedEvent(zr, ecIns) {
  zr.on('rendered', function () {
    ecIns.trigger('rendered'); // The `finished` event should not be triggered repeatly,
    // so it should only be triggered when rendering indeed happend
    // in zrender. (Consider the case that dipatchAction is keep
    // triggering when mouse move).

    if ( // Although zr is dirty if initial animation is not finished
    // and this checking is called on frame, we also check
    // animation finished for robustness.
    zr.animation.isFinished() && !ecIns[OPTION_UPDATED] && !ecIns._scheduler.unfinished && !ecIns._pendingActions.length) {
      ecIns.trigger('finished');
    }
  });
}
/**
 * @param {Object} params
 * @param {number} params.seriesIndex
 * @param {Array|TypedArray} params.data
 */


echartsProto.appendData = function (params) {
  var seriesIndex = params.seriesIndex;
  var ecModel = this.getModel();
  var seriesModel = ecModel.getSeriesByIndex(seriesIndex);
  seriesModel.appendData(params); // Note: `appendData` does not support that update extent of coordinate
  // system, util some scenario require that. In the expected usage of
  // `appendData`, the initial extent of coordinate system should better
  // be fixed by axis `min`/`max` setting or initial data, otherwise if
  // the extent changed while `appendData`, the location of the painted
  // graphic elements have to be changed, which make the usage of
  // `appendData` meaningless.

  this._scheduler.unfinished = true;
};
/**
 * Register event
 * @method
 */


echartsProto.on = createRegisterEventWithLowercaseName('on');
echartsProto.off = createRegisterEventWithLowercaseName('off');
echartsProto.one = createRegisterEventWithLowercaseName('one');
/**
 * Prepare view instances of charts and components
 * @param  {module:echarts/model/Global} ecModel
 * @private
 */

function prepareView(ecIns, type, ecModel, scheduler) {
  var isComponent = type === 'component';
  var viewList = isComponent ? ecIns._componentsViews : ecIns._chartsViews;
  var viewMap = isComponent ? ecIns._componentsMap : ecIns._chartsMap;
  var zr = ecIns._zr;
  var api = ecIns._api;

  for (var i = 0; i < viewList.length; i++) {
    viewList[i].__alive = false;
  }

  isComponent ? ecModel.eachComponent(function (componentType, model) {
    componentType !== 'series' && doPrepare(model);
  }) : ecModel.eachSeries(doPrepare);

  function doPrepare(model) {
    // Consider: id same and type changed.
    var viewId = '_ec_' + model.id + '_' + model.type;
    var view = viewMap[viewId];

    if (!view) {
      var classType = parseClassType(model.type);
      var Clazz = isComponent ? ComponentView.getClass(classType.main, classType.sub) : ChartView.getClass(classType.sub);
      view = new Clazz();
      view.init(ecModel, api);
      viewMap[viewId] = view;
      viewList.push(view);
      zr.add(view.group);
    }

    model.__viewId = view.__id = viewId;
    view.__alive = true;
    view.__model = model;
    view.group.__ecComponentInfo = {
      mainType: model.mainType,
      index: model.componentIndex
    };
    !isComponent && scheduler.prepareView(view, model, ecModel, api);
  }

  for (var i = 0; i < viewList.length;) {
    var view = viewList[i];

    if (!view.__alive) {
      !isComponent && view.renderTask.dispose();
      zr.remove(view.group);
      view.dispose(ecModel, api);
      viewList.splice(i, 1);
      delete viewMap[view.__id];
      view.__id = view.group.__ecComponentInfo = null;
    } else {
      i++;
    }
  }
} // /**
//  * Encode visual infomation from data after data processing
//  *
//  * @param {module:echarts/model/Global} ecModel
//  * @param {object} layout
//  * @param {boolean} [layoutFilter] `true`: only layout,
//  *                                 `false`: only not layout,
//  *                                 `null`/`undefined`: all.
//  * @param {string} taskBaseTag
//  * @private
//  */
// function startVisualEncoding(ecIns, ecModel, api, payload, layoutFilter) {
//     each(visualFuncs, function (visual, index) {
//         var isLayout = visual.isLayout;
//         if (layoutFilter == null
//             || (layoutFilter === false && !isLayout)
//             || (layoutFilter === true && isLayout)
//         ) {
//             visual.func(ecModel, api, payload);
//         }
//     });
// }


function clearColorPalette(ecModel) {
  ecModel.clearColorPalette();
  ecModel.eachSeries(function (seriesModel) {
    seriesModel.clearColorPalette();
  });
}

function render(ecIns, ecModel, api, payload) {
  renderComponents(ecIns, ecModel, api, payload);
  each(ecIns._chartsViews, function (chart) {
    chart.__alive = false;
  });
  renderSeries(ecIns, ecModel, api, payload); // Remove groups of unrendered charts

  each(ecIns._chartsViews, function (chart) {
    if (!chart.__alive) {
      chart.remove(ecModel, api);
    }
  });
}

function renderComponents(ecIns, ecModel, api, payload, dirtyList) {
  each(dirtyList || ecIns._componentsViews, function (componentView) {
    var componentModel = componentView.__model;
    componentView.render(componentModel, ecModel, api, payload);
    updateZ(componentModel, componentView);
  });
}
/**
 * Render each chart and component
 * @private
 */


function renderSeries(ecIns, ecModel, api, payload, dirtyMap) {
  // Render all charts
  var scheduler = ecIns._scheduler;
  var unfinished;
  ecModel.eachSeries(function (seriesModel) {
    var chartView = ecIns._chartsMap[seriesModel.__viewId];
    chartView.__alive = true;
    var renderTask = chartView.renderTask;
    scheduler.updatePayload(renderTask, payload);

    if (dirtyMap && dirtyMap.get(seriesModel.uid)) {
      renderTask.dirty();
    }

    unfinished |= renderTask.perform(scheduler.getPerformArgs(renderTask));
    chartView.group.silent = !!seriesModel.get('silent');
    updateZ(seriesModel, chartView);
    updateBlend(seriesModel, chartView);
  });
  scheduler.unfinished |= unfinished; // If use hover layer

  updateHoverLayerStatus(ecIns._zr, ecModel); // Add aria

  aria(ecIns._zr.dom, ecModel);
}

function performPostUpdateFuncs(ecModel, api) {
  each(postUpdateFuncs, function (func) {
    func(ecModel, api);
  });
}

var MOUSE_EVENT_NAMES = ['click', 'dblclick', 'mouseover', 'mouseout', 'mousemove', 'mousedown', 'mouseup', 'globalout', 'contextmenu'];
/**
 * @private
 */

echartsProto._initEvents = function () {
  each(MOUSE_EVENT_NAMES, function (eveName) {
    var handler = function handler(e) {
      var ecModel = this.getModel();
      var el = e.target;
      var params;
      var isGlobalOut = eveName === 'globalout'; // no e.target when 'globalout'.

      if (isGlobalOut) {
        params = {};
      } else if (el && el.dataIndex != null) {
        var dataModel = el.dataModel || ecModel.getSeriesByIndex(el.seriesIndex);
        params = dataModel && dataModel.getDataParams(el.dataIndex, el.dataType, el) || {};
      } // If element has custom eventData of components
      else if (el && el.eventData) {
          params = zrUtil.extend({}, el.eventData);
        } // Contract: if params prepared in mouse event,
      // these properties must be specified:
      // {
      //    componentType: string (component main type)
      //    componentIndex: number
      // }
      // Otherwise event query can not work.


      if (params) {
        var componentType = params.componentType;
        var componentIndex = params.componentIndex; // Special handling for historic reason: when trigger by
        // markLine/markPoint/markArea, the componentType is
        // 'markLine'/'markPoint'/'markArea', but we should better
        // enable them to be queried by seriesIndex, since their
        // option is set in each series.

        if (componentType === 'markLine' || componentType === 'markPoint' || componentType === 'markArea') {
          componentType = 'series';
          componentIndex = params.seriesIndex;
        }

        var model = componentType && componentIndex != null && ecModel.getComponent(componentType, componentIndex);
        var view = model && this[model.mainType === 'series' ? '_chartsMap' : '_componentsMap'][model.__viewId];
        params.event = e;
        params.type = eveName;
        this._ecEventProcessor.eventInfo = {
          targetEl: el,
          packedEvent: params,
          model: model,
          view: view
        };
        this.trigger(eveName, params);
      }
    }; // Consider that some component (like tooltip, brush, ...)
    // register zr event handler, but user event handler might
    // do anything, such as call `setOption` or `dispatchAction`,
    // which probably update any of the content and probably
    // cause problem if it is called previous other inner handlers.


    handler.zrEventfulCallAtLast = true;

    this._zr.on(eveName, handler, this);
  }, this);
  each(eventActionMap, function (actionType, eventType) {
    this._messageCenter.on(eventType, function (event) {
      this.trigger(eventType, event);
    }, this);
  }, this);
};
/**
 * @return {boolean}
 */


echartsProto.isDisposed = function () {
  return this._disposed;
};
/**
 * Clear
 */


echartsProto.clear = function () {
  this.setOption({
    series: []
  }, true);
};
/**
 * Dispose instance
 */


echartsProto.dispose = function () {
  if (this._disposed) {
    return;
  }

  this._disposed = true;
  modelUtil.setAttribute(this.getDom(), DOM_ATTRIBUTE_KEY, '');
  var api = this._api;
  var ecModel = this._model;
  each(this._componentsViews, function (component) {
    component.dispose(ecModel, api);
  });
  each(this._chartsViews, function (chart) {
    chart.dispose(ecModel, api);
  }); // Dispose after all views disposed

  this._zr.dispose();

  delete instances[this.id];
};

zrUtil.mixin(ECharts, Eventful);

function updateHoverLayerStatus(zr, ecModel) {
  var storage = zr.storage;
  var elCount = 0;
  storage.traverse(function (el) {
    if (!el.isGroup) {
      elCount++;
    }
  });

  if (elCount > ecModel.get('hoverLayerThreshold') && !env.node) {
    storage.traverse(function (el) {
      if (!el.isGroup) {
        // Don't switch back.
        el.useHoverLayer = true;
      }
    });
  }
}
/**
 * Update chart progressive and blend.
 * @param {module:echarts/model/Series|module:echarts/model/Component} model
 * @param {module:echarts/view/Component|module:echarts/view/Chart} view
 */


function updateBlend(seriesModel, chartView) {
  var blendMode = seriesModel.get('blendMode') || null;
  chartView.group.traverse(function (el) {
    // FIXME marker and other components
    if (!el.isGroup) {
      // Only set if blendMode is changed. In case element is incremental and don't wan't to rerender.
      if (el.style.blend !== blendMode) {
        el.setStyle('blend', blendMode);
      }
    }

    if (el.eachPendingDisplayable) {
      el.eachPendingDisplayable(function (displayable) {
        displayable.setStyle('blend', blendMode);
      });
    }
  });
}
/**
 * @param {module:echarts/model/Series|module:echarts/model/Component} model
 * @param {module:echarts/view/Component|module:echarts/view/Chart} view
 */


function updateZ(model, view) {
  var z = model.get('z');
  var zlevel = model.get('zlevel'); // Set z and zlevel

  view.group.traverse(function (el) {
    if (el.type !== 'group') {
      z != null && (el.z = z);
      zlevel != null && (el.zlevel = zlevel);
    }
  });
}

function createExtensionAPI(ecInstance) {
  var coordSysMgr = ecInstance._coordSysMgr;
  return zrUtil.extend(new ExtensionAPI(ecInstance), {
    // Inject methods
    getCoordinateSystems: zrUtil.bind(coordSysMgr.getCoordinateSystems, coordSysMgr),
    getComponentByElement: function getComponentByElement(el) {
      while (el) {
        var modelInfo = el.__ecComponentInfo;

        if (modelInfo != null) {
          return ecInstance._model.getComponent(modelInfo.mainType, modelInfo.index);
        }

        el = el.parent;
      }
    }
  });
}
/**
 * @class
 * Usage of query:
 * `chart.on('click', query, handler);`
 * The `query` can be:
 * + The component type query string, only `mainType` or `mainType.subType`,
 *   like: 'xAxis', 'series', 'xAxis.category' or 'series.line'.
 * + The component query object, like:
 *   `{seriesIndex: 2}`, `{seriesName: 'xx'}`, `{seriesId: 'some'}`,
 *   `{xAxisIndex: 2}`, `{xAxisName: 'xx'}`, `{xAxisId: 'some'}`.
 * + The data query object, like:
 *   `{dataIndex: 123}`, `{dataType: 'link'}`, `{name: 'some'}`.
 * + The other query object (cmponent customized query), like:
 *   `{element: 'some'}` (only available in custom series).
 *
 * Caveat: If a prop in the `query` object is `null/undefined`, it is the
 * same as there is no such prop in the `query` object.
 */


function EventProcessor() {
  // These info required: targetEl, packedEvent, model, view
  this.eventInfo;
}

EventProcessor.prototype = {
  constructor: EventProcessor,
  normalizeQuery: function normalizeQuery(query) {
    var cptQuery = {};
    var dataQuery = {};
    var otherQuery = {}; // `query` is `mainType` or `mainType.subType` of component.

    if (zrUtil.isString(query)) {
      var condCptType = parseClassType(query); // `.main` and `.sub` may be ''.

      cptQuery.mainType = condCptType.main || null;
      cptQuery.subType = condCptType.sub || null;
    } // `query` is an object, convert to {mainType, index, name, id}.
    else {
        // `xxxIndex`, `xxxName`, `xxxId`, `name`, `dataIndex`, `dataType` is reserved,
        // can not be used in `compomentModel.filterForExposedEvent`.
        var suffixes = ['Index', 'Name', 'Id'];
        var dataKeys = {
          name: 1,
          dataIndex: 1,
          dataType: 1
        };
        zrUtil.each(query, function (val, key) {
          var reserved = false;

          for (var i = 0; i < suffixes.length; i++) {
            var propSuffix = suffixes[i];
            var suffixPos = key.lastIndexOf(propSuffix);

            if (suffixPos > 0 && suffixPos === key.length - propSuffix.length) {
              var mainType = key.slice(0, suffixPos); // Consider `dataIndex`.

              if (mainType !== 'data') {
                cptQuery.mainType = mainType;
                cptQuery[propSuffix.toLowerCase()] = val;
                reserved = true;
              }
            }
          }

          if (dataKeys.hasOwnProperty(key)) {
            dataQuery[key] = val;
            reserved = true;
          }

          if (!reserved) {
            otherQuery[key] = val;
          }
        });
      }

    return {
      cptQuery: cptQuery,
      dataQuery: dataQuery,
      otherQuery: otherQuery
    };
  },
  filter: function filter(eventType, query, args) {
    // They should be assigned before each trigger call.
    var eventInfo = this.eventInfo;

    if (!eventInfo) {
      return true;
    }

    var targetEl = eventInfo.targetEl;
    var packedEvent = eventInfo.packedEvent;
    var model = eventInfo.model;
    var view = eventInfo.view; // For event like 'globalout'.

    if (!model || !view) {
      return true;
    }

    var cptQuery = query.cptQuery;
    var dataQuery = query.dataQuery;
    return check(cptQuery, model, 'mainType') && check(cptQuery, model, 'subType') && check(cptQuery, model, 'index', 'componentIndex') && check(cptQuery, model, 'name') && check(cptQuery, model, 'id') && check(dataQuery, packedEvent, 'name') && check(dataQuery, packedEvent, 'dataIndex') && check(dataQuery, packedEvent, 'dataType') && (!view.filterForExposedEvent || view.filterForExposedEvent(eventType, query.otherQuery, targetEl, packedEvent));

    function check(query, host, prop, propOnHost) {
      return query[prop] == null || host[propOnHost || prop] === query[prop];
    }
  },
  afterTrigger: function afterTrigger() {
    // Make sure the eventInfo wont be used in next trigger.
    this.eventInfo = null;
  }
};
/**
 * @type {Object} key: actionType.
 * @inner
 */

var actions = {};
/**
 * Map eventType to actionType
 * @type {Object}
 */

var eventActionMap = {};
/**
 * Data processor functions of each stage
 * @type {Array.<Object.<string, Function>>}
 * @inner
 */

var dataProcessorFuncs = [];
/**
 * @type {Array.<Function>}
 * @inner
 */

var optionPreprocessorFuncs = [];
/**
 * @type {Array.<Function>}
 * @inner
 */

var postUpdateFuncs = [];
/**
 * Visual encoding functions of each stage
 * @type {Array.<Object.<string, Function>>}
 */

var visualFuncs = [];
/**
 * Theme storage
 * @type {Object.<key, Object>}
 */

var themeStorage = {};
/**
 * Loading effects
 */

var loadingEffects = {};
var instances = {};
var connectedGroups = {};
var idBase = new Date() - 0;
var groupIdBase = new Date() - 0;
var DOM_ATTRIBUTE_KEY = '_echarts_instance_';

function enableConnect(chart) {
  var STATUS_PENDING = 0;
  var STATUS_UPDATING = 1;
  var STATUS_UPDATED = 2;
  var STATUS_KEY = '__connectUpdateStatus';

  function updateConnectedChartsStatus(charts, status) {
    for (var i = 0; i < charts.length; i++) {
      var otherChart = charts[i];
      otherChart[STATUS_KEY] = status;
    }
  }

  each(eventActionMap, function (actionType, eventType) {
    chart._messageCenter.on(eventType, function (event) {
      if (connectedGroups[chart.group] && chart[STATUS_KEY] !== STATUS_PENDING) {
        if (event && event.escapeConnect) {
          return;
        }

        var action = chart.makeActionFromEvent(event);
        var otherCharts = [];
        each(instances, function (otherChart) {
          if (otherChart !== chart && otherChart.group === chart.group) {
            otherCharts.push(otherChart);
          }
        });
        updateConnectedChartsStatus(otherCharts, STATUS_PENDING);
        each(otherCharts, function (otherChart) {
          if (otherChart[STATUS_KEY] !== STATUS_UPDATING) {
            otherChart.dispatchAction(action);
          }
        });
        updateConnectedChartsStatus(otherCharts, STATUS_UPDATED);
      }
    });
  });
}
/**
 * @param {HTMLElement} dom
 * @param {Object} [theme]
 * @param {Object} opts
 * @param {number} [opts.devicePixelRatio] Use window.devicePixelRatio by default
 * @param {string} [opts.renderer] Currently only 'canvas' is supported.
 * @param {number} [opts.width] Use clientWidth of the input `dom` by default.
 *                              Can be 'auto' (the same as null/undefined)
 * @param {number} [opts.height] Use clientHeight of the input `dom` by default.
 *                               Can be 'auto' (the same as null/undefined)
 */


function init(dom, theme, opts) {
  var existInstance = getInstanceByDom(dom);

  if (existInstance) {
    return existInstance;
  }

  var chart = new ECharts(dom, theme, opts);
  chart.id = 'ec_' + idBase++;
  instances[chart.id] = chart;
  modelUtil.setAttribute(dom, DOM_ATTRIBUTE_KEY, chart.id);
  enableConnect(chart);
  return chart;
}
/**
 * @return {string|Array.<module:echarts~ECharts>} groupId
 */


function connect(groupId) {
  // Is array of charts
  if (zrUtil.isArray(groupId)) {
    var charts = groupId;
    groupId = null; // If any chart has group

    each(charts, function (chart) {
      if (chart.group != null) {
        groupId = chart.group;
      }
    });
    groupId = groupId || 'g_' + groupIdBase++;
    each(charts, function (chart) {
      chart.group = groupId;
    });
  }

  connectedGroups[groupId] = true;
  return groupId;
}
/**
 * @DEPRECATED
 * @return {string} groupId
 */


function disConnect(groupId) {
  connectedGroups[groupId] = false;
}
/**
 * @return {string} groupId
 */


var disconnect = disConnect;
/**
 * Dispose a chart instance
 * @param  {module:echarts~ECharts|HTMLDomElement|string} chart
 */

function dispose(chart) {
  if (typeof chart === 'string') {
    chart = instances[chart];
  } else if (!(chart instanceof ECharts)) {
    // Try to treat as dom
    chart = getInstanceByDom(chart);
  }

  if (chart instanceof ECharts && !chart.isDisposed()) {
    chart.dispose();
  }
}
/**
 * @param  {HTMLElement} dom
 * @return {echarts~ECharts}
 */


function getInstanceByDom(dom) {
  return instances[modelUtil.getAttribute(dom, DOM_ATTRIBUTE_KEY)];
}
/**
 * @param {string} key
 * @return {echarts~ECharts}
 */


function getInstanceById(key) {
  return instances[key];
}
/**
 * Register theme
 */


function registerTheme(name, theme) {
  themeStorage[name] = theme;
}
/**
 * Register option preprocessor
 * @param {Function} preprocessorFunc
 */


function registerPreprocessor(preprocessorFunc) {
  optionPreprocessorFuncs.push(preprocessorFunc);
}
/**
 * @param {number} [priority=1000]
 * @param {Object|Function} processor
 */


function registerProcessor(priority, processor) {
  normalizeRegister(dataProcessorFuncs, priority, processor, PRIORITY_PROCESSOR_FILTER);
}
/**
 * Register postUpdater
 * @param {Function} postUpdateFunc
 */


function registerPostUpdate(postUpdateFunc) {
  postUpdateFuncs.push(postUpdateFunc);
}
/**
 * Usage:
 * registerAction('someAction', 'someEvent', function () { ... });
 * registerAction('someAction', function () { ... });
 * registerAction(
 *     {type: 'someAction', event: 'someEvent', update: 'updateView'},
 *     function () { ... }
 * );
 *
 * @param {(string|Object)} actionInfo
 * @param {string} actionInfo.type
 * @param {string} [actionInfo.event]
 * @param {string} [actionInfo.update]
 * @param {string} [eventName]
 * @param {Function} action
 */


function registerAction(actionInfo, eventName, action) {
  if (typeof eventName === 'function') {
    action = eventName;
    eventName = '';
  }

  var actionType = isObject(actionInfo) ? actionInfo.type : [actionInfo, actionInfo = {
    event: eventName
  }][0]; // Event name is all lowercase

  actionInfo.event = (actionInfo.event || actionType).toLowerCase();
  eventName = actionInfo.event; // Validate action type and event name.

  assert(ACTION_REG.test(actionType) && ACTION_REG.test(eventName));

  if (!actions[actionType]) {
    actions[actionType] = {
      action: action,
      actionInfo: actionInfo
    };
  }

  eventActionMap[eventName] = actionType;
}
/**
 * @param {string} type
 * @param {*} CoordinateSystem
 */


function registerCoordinateSystem(type, CoordinateSystem) {
  CoordinateSystemManager.register(type, CoordinateSystem);
}
/**
 * Get dimensions of specified coordinate system.
 * @param {string} type
 * @return {Array.<string|Object>}
 */


function getCoordinateSystemDimensions(type) {
  var coordSysCreator = CoordinateSystemManager.get(type);

  if (coordSysCreator) {
    return coordSysCreator.getDimensionsInfo ? coordSysCreator.getDimensionsInfo() : coordSysCreator.dimensions.slice();
  }
}
/**
 * Layout is a special stage of visual encoding
 * Most visual encoding like color are common for different chart
 * But each chart has it's own layout algorithm
 *
 * @param {number} [priority=1000]
 * @param {Function} layoutTask
 */


function registerLayout(priority, layoutTask) {
  normalizeRegister(visualFuncs, priority, layoutTask, PRIORITY_VISUAL_LAYOUT, 'layout');
}
/**
 * @param {number} [priority=3000]
 * @param {module:echarts/stream/Task} visualTask
 */


function registerVisual(priority, visualTask) {
  normalizeRegister(visualFuncs, priority, visualTask, PRIORITY_VISUAL_CHART, 'visual');
}
/**
 * @param {Object|Function} fn: {seriesType, createOnAllSeries, performRawSeries, reset}
 */


function normalizeRegister(targetList, priority, fn, defaultPriority, visualType) {
  if (isFunction(priority) || isObject(priority)) {
    fn = priority;
    priority = defaultPriority;
  }

  var stageHandler = Scheduler.wrapStageHandler(fn, visualType);
  stageHandler.__prio = priority;
  stageHandler.__raw = fn;
  targetList.push(stageHandler);
  return stageHandler;
}
/**
 * @param {string} name
 */


function registerLoading(name, loadingFx) {
  loadingEffects[name] = loadingFx;
}
/**
 * @param {Object} opts
 * @param {string} [superClass]
 */


function extendComponentModel(opts
/*, superClass*/
) {
  // var Clazz = ComponentModel;
  // if (superClass) {
  //     var classType = parseClassType(superClass);
  //     Clazz = ComponentModel.getClass(classType.main, classType.sub, true);
  // }
  return ComponentModel.extend(opts);
}
/**
 * @param {Object} opts
 * @param {string} [superClass]
 */


function extendComponentView(opts
/*, superClass*/
) {
  // var Clazz = ComponentView;
  // if (superClass) {
  //     var classType = parseClassType(superClass);
  //     Clazz = ComponentView.getClass(classType.main, classType.sub, true);
  // }
  return ComponentView.extend(opts);
}
/**
 * @param {Object} opts
 * @param {string} [superClass]
 */


function extendSeriesModel(opts
/*, superClass*/
) {
  // var Clazz = SeriesModel;
  // if (superClass) {
  //     superClass = 'series.' + superClass.replace('series.', '');
  //     var classType = parseClassType(superClass);
  //     Clazz = ComponentModel.getClass(classType.main, classType.sub, true);
  // }
  return SeriesModel.extend(opts);
}
/**
 * @param {Object} opts
 * @param {string} [superClass]
 */


function extendChartView(opts
/*, superClass*/
) {
  // var Clazz = ChartView;
  // if (superClass) {
  //     superClass = superClass.replace('series.', '');
  //     var classType = parseClassType(superClass);
  //     Clazz = ChartView.getClass(classType.main, true);
  // }
  return ChartView.extend(opts);
}
/**
 * ZRender need a canvas context to do measureText.
 * But in node environment canvas may be created by node-canvas.
 * So we need to specify how to create a canvas instead of using document.createElement('canvas')
 *
 * Be careful of using it in the browser.
 *
 * @param {Function} creator
 * @example
 *     var Canvas = require('canvas');
 *     var echarts = require('echarts');
 *     echarts.setCanvasCreator(function () {
 *         // Small size is enough.
 *         return new Canvas(32, 32);
 *     });
 */


function setCanvasCreator(creator) {
  zrUtil.$override('createCanvas', creator);
}
/**
 * @param {string} mapName
 * @param {Array.<Object>|Object|string} geoJson
 * @param {Object} [specialAreas]
 *
 * @example GeoJSON
 *     $.get('USA.json', function (geoJson) {
 *         echarts.registerMap('USA', geoJson);
 *         // Or
 *         echarts.registerMap('USA', {
 *             geoJson: geoJson,
 *             specialAreas: {}
 *         })
 *     });
 *
 *     $.get('airport.svg', function (svg) {
 *         echarts.registerMap('airport', {
 *             svg: svg
 *         }
 *     });
 *
 *     echarts.registerMap('eu', [
 *         {svg: eu-topographic.svg},
 *         {geoJSON: eu.json}
 *     ])
 */


function registerMap(mapName, geoJson, specialAreas) {
  mapDataStorage.registerMap(mapName, geoJson, specialAreas);
}
/**
 * @param {string} mapName
 * @return {Object}
 */


function getMap(mapName) {
  // For backward compatibility, only return the first one.
  var records = mapDataStorage.retrieveMap(mapName);
  return records && records[0] && {
    geoJson: records[0].geoJSON,
    specialAreas: records[0].specialAreas
  };
}

registerVisual(PRIORITY_VISUAL_GLOBAL, seriesColor);
registerPreprocessor(backwardCompat);
registerProcessor(PRIORITY_PROCESSOR_STATISTIC, dataStack);
registerLoading('default', loadingDefault); // Default actions

registerAction({
  type: 'highlight',
  event: 'highlight',
  update: 'highlight'
}, zrUtil.noop);
registerAction({
  type: 'downplay',
  event: 'downplay',
  update: 'downplay'
}, zrUtil.noop); // Default theme

registerTheme('light', lightTheme);
registerTheme('dark', darkTheme); // For backward compatibility, where the namespace `dataTool` will
// be mounted on `echarts` is the extension `dataTool` is imported.

var dataTool = {};
exports.version = version;
exports.dependencies = dependencies;
exports.PRIORITY = PRIORITY;
exports.init = init;
exports.connect = connect;
exports.disConnect = disConnect;
exports.disconnect = disconnect;
exports.dispose = dispose;
exports.getInstanceByDom = getInstanceByDom;
exports.getInstanceById = getInstanceById;
exports.registerTheme = registerTheme;
exports.registerPreprocessor = registerPreprocessor;
exports.registerProcessor = registerProcessor;
exports.registerPostUpdate = registerPostUpdate;
exports.registerAction = registerAction;
exports.registerCoordinateSystem = registerCoordinateSystem;
exports.getCoordinateSystemDimensions = getCoordinateSystemDimensions;
exports.registerLayout = registerLayout;
exports.registerVisual = registerVisual;
exports.registerLoading = registerLoading;
exports.extendComponentModel = extendComponentModel;
exports.extendComponentView = extendComponentView;
exports.extendSeriesModel = extendSeriesModel;
exports.extendChartView = extendChartView;
exports.setCanvasCreator = setCanvasCreator;
exports.registerMap = registerMap;
exports.getMap = getMap;
exports.dataTool = dataTool;

var ___ec_export = __webpack_require__(/*! ./export */ "./node_modules/echarts/lib/export.js");

(function () {
  for (var key in ___ec_export) {
    if (___ec_export.hasOwnProperty(key)) {
      exports[key] = ___ec_export[key];
    }
  }
})();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZWNoYXJ0cy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2VjaGFydHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfY29uZmlnID0gcmVxdWlyZShcIi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxudmFyIHpyZW5kZXIgPSByZXF1aXJlKFwienJlbmRlci9saWIvenJlbmRlclwiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBjb2xvclRvb2wgPSByZXF1aXJlKFwienJlbmRlci9saWIvdG9vbC9jb2xvclwiKTtcblxudmFyIGVudiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL2VudlwiKTtcblxudmFyIHRpbXNvcnQgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS90aW1zb3J0XCIpO1xuXG52YXIgRXZlbnRmdWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvbWl4aW4vRXZlbnRmdWxcIik7XG5cbnZhciBHbG9iYWxNb2RlbCA9IHJlcXVpcmUoXCIuL21vZGVsL0dsb2JhbFwiKTtcblxudmFyIEV4dGVuc2lvbkFQSSA9IHJlcXVpcmUoXCIuL0V4dGVuc2lvbkFQSVwiKTtcblxudmFyIENvb3JkaW5hdGVTeXN0ZW1NYW5hZ2VyID0gcmVxdWlyZShcIi4vQ29vcmRpbmF0ZVN5c3RlbVwiKTtcblxudmFyIE9wdGlvbk1hbmFnZXIgPSByZXF1aXJlKFwiLi9tb2RlbC9PcHRpb25NYW5hZ2VyXCIpO1xuXG52YXIgYmFja3dhcmRDb21wYXQgPSByZXF1aXJlKFwiLi9wcmVwcm9jZXNzb3IvYmFja3dhcmRDb21wYXRcIik7XG5cbnZhciBkYXRhU3RhY2sgPSByZXF1aXJlKFwiLi9wcm9jZXNzb3IvZGF0YVN0YWNrXCIpO1xuXG52YXIgQ29tcG9uZW50TW9kZWwgPSByZXF1aXJlKFwiLi9tb2RlbC9Db21wb25lbnRcIik7XG5cbnZhciBTZXJpZXNNb2RlbCA9IHJlcXVpcmUoXCIuL21vZGVsL1Nlcmllc1wiKTtcblxudmFyIENvbXBvbmVudFZpZXcgPSByZXF1aXJlKFwiLi92aWV3L0NvbXBvbmVudFwiKTtcblxudmFyIENoYXJ0VmlldyA9IHJlcXVpcmUoXCIuL3ZpZXcvQ2hhcnRcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4vdXRpbC9ncmFwaGljXCIpO1xuXG52YXIgbW9kZWxVdGlsID0gcmVxdWlyZShcIi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIF90aHJvdHRsZSA9IHJlcXVpcmUoXCIuL3V0aWwvdGhyb3R0bGVcIik7XG5cbnZhciB0aHJvdHRsZSA9IF90aHJvdHRsZS50aHJvdHRsZTtcblxudmFyIHNlcmllc0NvbG9yID0gcmVxdWlyZShcIi4vdmlzdWFsL3Nlcmllc0NvbG9yXCIpO1xuXG52YXIgYXJpYSA9IHJlcXVpcmUoXCIuL3Zpc3VhbC9hcmlhXCIpO1xuXG52YXIgbG9hZGluZ0RlZmF1bHQgPSByZXF1aXJlKFwiLi9sb2FkaW5nL2RlZmF1bHRcIik7XG5cbnZhciBTY2hlZHVsZXIgPSByZXF1aXJlKFwiLi9zdHJlYW0vU2NoZWR1bGVyXCIpO1xuXG52YXIgbGlnaHRUaGVtZSA9IHJlcXVpcmUoXCIuL3RoZW1lL2xpZ2h0XCIpO1xuXG52YXIgZGFya1RoZW1lID0gcmVxdWlyZShcIi4vdGhlbWUvZGFya1wiKTtcblxucmVxdWlyZShcIi4vY29tcG9uZW50L2RhdGFzZXRcIik7XG5cbnZhciBtYXBEYXRhU3RvcmFnZSA9IHJlcXVpcmUoXCIuL2Nvb3JkL2dlby9tYXBEYXRhU3RvcmFnZVwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGFzc2VydCA9IHpyVXRpbC5hc3NlcnQ7XG52YXIgZWFjaCA9IHpyVXRpbC5lYWNoO1xudmFyIGlzRnVuY3Rpb24gPSB6clV0aWwuaXNGdW5jdGlvbjtcbnZhciBpc09iamVjdCA9IHpyVXRpbC5pc09iamVjdDtcbnZhciBwYXJzZUNsYXNzVHlwZSA9IENvbXBvbmVudE1vZGVsLnBhcnNlQ2xhc3NUeXBlO1xudmFyIHZlcnNpb24gPSAnNC4yLjEnO1xudmFyIGRlcGVuZGVuY2llcyA9IHtcbiAgenJlbmRlcjogJzQuMC42J1xufTtcbnZhciBURVNUX0ZSQU1FX1JFTUFJTl9USU1FID0gMTtcbnZhciBQUklPUklUWV9QUk9DRVNTT1JfRklMVEVSID0gMTAwMDtcbnZhciBQUklPUklUWV9QUk9DRVNTT1JfU1RBVElTVElDID0gNTAwMDtcbnZhciBQUklPUklUWV9WSVNVQUxfTEFZT1VUID0gMTAwMDtcbnZhciBQUklPUklUWV9WSVNVQUxfR0xPQkFMID0gMjAwMDtcbnZhciBQUklPUklUWV9WSVNVQUxfQ0hBUlQgPSAzMDAwO1xudmFyIFBSSU9SSVRZX1ZJU1VBTF9DT01QT05FTlQgPSA0MDAwOyAvLyBGSVhNRVxuLy8gbmVjZXNzYXJ5P1xuXG52YXIgUFJJT1JJVFlfVklTVUFMX0JSVVNIID0gNTAwMDtcbnZhciBQUklPUklUWSA9IHtcbiAgUFJPQ0VTU09SOiB7XG4gICAgRklMVEVSOiBQUklPUklUWV9QUk9DRVNTT1JfRklMVEVSLFxuICAgIFNUQVRJU1RJQzogUFJJT1JJVFlfUFJPQ0VTU09SX1NUQVRJU1RJQ1xuICB9LFxuICBWSVNVQUw6IHtcbiAgICBMQVlPVVQ6IFBSSU9SSVRZX1ZJU1VBTF9MQVlPVVQsXG4gICAgR0xPQkFMOiBQUklPUklUWV9WSVNVQUxfR0xPQkFMLFxuICAgIENIQVJUOiBQUklPUklUWV9WSVNVQUxfQ0hBUlQsXG4gICAgQ09NUE9ORU5UOiBQUklPUklUWV9WSVNVQUxfQ09NUE9ORU5ULFxuICAgIEJSVVNIOiBQUklPUklUWV9WSVNVQUxfQlJVU0hcbiAgfVxufTsgLy8gTWFpbiBwcm9jZXNzIGhhdmUgdGhyZWUgZW50cmllczogYHNldE9wdGlvbmAsIGBkaXNwYXRjaEFjdGlvbmAgYW5kIGByZXNpemVgLFxuLy8gd2hlcmUgdGhleSBtdXN0IG5vdCBiZSBpbnZva2VkIG5lc3RlZGx5LCBleGNlcHQgdGhlIG9ubHkgY2FzZTogaW52b2tlXG4vLyBkaXNwYXRjaEFjdGlvbiB3aXRoIHVwZGF0ZU1ldGhvZCBcIm5vbmVcIiBpbiBtYWluIHByb2Nlc3MuXG4vLyBUaGlzIGZsYWcgaXMgdXNlZCB0byBjYXJyeSBvdXQgdGhpcyBydWxlLlxuLy8gQWxsIGV2ZW50cyB3aWxsIGJlIHRyaWdnZXJlZCBvdXQgc2lkZSBtYWluIHByb2Nlc3MgKGkuZS4gd2hlbiAhdGhpc1tJTl9NQUlOX1BST0NFU1NdKS5cblxudmFyIElOX01BSU5fUFJPQ0VTUyA9ICdfX2ZsYWdJbk1haW5Qcm9jZXNzJztcbnZhciBPUFRJT05fVVBEQVRFRCA9ICdfX29wdGlvblVwZGF0ZWQnO1xudmFyIEFDVElPTl9SRUcgPSAvXlthLXpBLVowLTlfXSskLztcblxuZnVuY3Rpb24gY3JlYXRlUmVnaXN0ZXJFdmVudFdpdGhMb3dlcmNhc2VOYW1lKG1ldGhvZCkge1xuICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50TmFtZSwgaGFuZGxlciwgY29udGV4dCkge1xuICAgIC8vIEV2ZW50IG5hbWUgaXMgYWxsIGxvd2VyY2FzZVxuICAgIGV2ZW50TmFtZSA9IGV2ZW50TmFtZSAmJiBldmVudE5hbWUudG9Mb3dlckNhc2UoKTtcbiAgICBFdmVudGZ1bC5wcm90b3R5cGVbbWV0aG9kXS5jYWxsKHRoaXMsIGV2ZW50TmFtZSwgaGFuZGxlciwgY29udGV4dCk7XG4gIH07XG59XG4vKipcbiAqIEBtb2R1bGUgZWNoYXJ0c35NZXNzYWdlQ2VudGVyXG4gKi9cblxuXG5mdW5jdGlvbiBNZXNzYWdlQ2VudGVyKCkge1xuICBFdmVudGZ1bC5jYWxsKHRoaXMpO1xufVxuXG5NZXNzYWdlQ2VudGVyLnByb3RvdHlwZS5vbiA9IGNyZWF0ZVJlZ2lzdGVyRXZlbnRXaXRoTG93ZXJjYXNlTmFtZSgnb24nKTtcbk1lc3NhZ2VDZW50ZXIucHJvdG90eXBlLm9mZiA9IGNyZWF0ZVJlZ2lzdGVyRXZlbnRXaXRoTG93ZXJjYXNlTmFtZSgnb2ZmJyk7XG5NZXNzYWdlQ2VudGVyLnByb3RvdHlwZS5vbmUgPSBjcmVhdGVSZWdpc3RlckV2ZW50V2l0aExvd2VyY2FzZU5hbWUoJ29uZScpO1xuenJVdGlsLm1peGluKE1lc3NhZ2VDZW50ZXIsIEV2ZW50ZnVsKTtcbi8qKlxuICogQG1vZHVsZSBlY2hhcnRzfkVDaGFydHNcbiAqL1xuXG5mdW5jdGlvbiBFQ2hhcnRzKGRvbSwgdGhlbWUsIG9wdHMpIHtcbiAgb3B0cyA9IG9wdHMgfHwge307IC8vIEdldCB0aGVtZSBieSBuYW1lXG5cbiAgaWYgKHR5cGVvZiB0aGVtZSA9PT0gJ3N0cmluZycpIHtcbiAgICB0aGVtZSA9IHRoZW1lU3RvcmFnZVt0aGVtZV07XG4gIH1cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuXG5cbiAgdGhpcy5pZDtcbiAgLyoqXG4gICAqIEdyb3VwIGlkXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuXG4gIHRoaXMuZ3JvdXA7XG4gIC8qKlxuICAgKiBAdHlwZSB7SFRNTEVsZW1lbnR9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2RvbSA9IGRvbTtcbiAgdmFyIGRlZmF1bHRSZW5kZXJlciA9ICdjYW52YXMnO1xuXG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvWlJlbmRlcn1cbiAgICogQHByaXZhdGVcbiAgICovXG4gIHZhciB6ciA9IHRoaXMuX3pyID0genJlbmRlci5pbml0KGRvbSwge1xuICAgIHJlbmRlcmVyOiBvcHRzLnJlbmRlcmVyIHx8IGRlZmF1bHRSZW5kZXJlcixcbiAgICBkZXZpY2VQaXhlbFJhdGlvOiBvcHRzLmRldmljZVBpeGVsUmF0aW8sXG4gICAgd2lkdGg6IG9wdHMud2lkdGgsXG4gICAgaGVpZ2h0OiBvcHRzLmhlaWdodFxuICB9KTtcbiAgLyoqXG4gICAqIEV4cGVjdCA2MCBwZnMuXG4gICAqIEB0eXBlIHtGdW5jdGlvbn1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fdGhyb3R0bGVkWnJGbHVzaCA9IHRocm90dGxlKHpyVXRpbC5iaW5kKHpyLmZsdXNoLCB6ciksIDE3KTtcbiAgdmFyIHRoZW1lID0genJVdGlsLmNsb25lKHRoZW1lKTtcbiAgdGhlbWUgJiYgYmFja3dhcmRDb21wYXQodGhlbWUsIHRydWUpO1xuICAvKipcbiAgICogQHR5cGUge09iamVjdH1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fdGhlbWUgPSB0aGVtZTtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOmVjaGFydHMvdmlldy9DaGFydD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2NoYXJ0c1ZpZXdzID0gW107XG4gIC8qKlxuICAgKiBAdHlwZSB7T2JqZWN0LjxzdHJpbmcsIG1vZHVsZTplY2hhcnRzL3ZpZXcvQ2hhcnQ+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9jaGFydHNNYXAgPSB7fTtcbiAgLyoqXG4gICAqIEB0eXBlIHtBcnJheS48bW9kdWxlOmVjaGFydHMvdmlldy9Db21wb25lbnQ+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9jb21wb25lbnRzVmlld3MgPSBbXTtcbiAgLyoqXG4gICAqIEB0eXBlIHtPYmplY3QuPHN0cmluZywgbW9kdWxlOmVjaGFydHMvdmlldy9Db21wb25lbnQ+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9jb21wb25lbnRzTWFwID0ge307XG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvQ29vcmRpbmF0ZVN5c3RlbX1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fY29vcmRTeXNNZ3IgPSBuZXcgQ29vcmRpbmF0ZVN5c3RlbU1hbmFnZXIoKTtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHZhciBhcGkgPSB0aGlzLl9hcGkgPSBjcmVhdGVFeHRlbnNpb25BUEkodGhpcyk7IC8vIFNvcnQgb24gZGVtYW5kXG5cbiAgZnVuY3Rpb24gcHJpb3JpdHlTb3J0RnVuYyhhLCBiKSB7XG4gICAgcmV0dXJuIGEuX19wcmlvIC0gYi5fX3ByaW87XG4gIH1cblxuICB0aW1zb3J0KHZpc3VhbEZ1bmNzLCBwcmlvcml0eVNvcnRGdW5jKTtcbiAgdGltc29ydChkYXRhUHJvY2Vzc29yRnVuY3MsIHByaW9yaXR5U29ydEZ1bmMpO1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL3N0cmVhbS9TY2hlZHVsZXJ9XG4gICAqL1xuXG4gIHRoaXMuX3NjaGVkdWxlciA9IG5ldyBTY2hlZHVsZXIodGhpcywgYXBpLCBkYXRhUHJvY2Vzc29yRnVuY3MsIHZpc3VhbEZ1bmNzKTtcbiAgRXZlbnRmdWwuY2FsbCh0aGlzLCB0aGlzLl9lY0V2ZW50UHJvY2Vzc29yID0gbmV3IEV2ZW50UHJvY2Vzc29yKCkpO1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzfk1lc3NhZ2VDZW50ZXJ9XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX21lc3NhZ2VDZW50ZXIgPSBuZXcgTWVzc2FnZUNlbnRlcigpOyAvLyBJbml0IG1vdXNlIGV2ZW50c1xuXG4gIHRoaXMuX2luaXRFdmVudHMoKTsgLy8gSW4gY2FzZSBzb21lIHBlb3BsZSB3cml0ZSBgd2luZG93Lm9ucmVzaXplID0gY2hhcnQucmVzaXplYFxuXG5cbiAgdGhpcy5yZXNpemUgPSB6clV0aWwuYmluZCh0aGlzLnJlc2l6ZSwgdGhpcyk7IC8vIENhbid0IGRpc3BhdGNoIGFjdGlvbiBkdXJpbmcgcmVuZGVyaW5nIHByb2NlZHVyZVxuXG4gIHRoaXMuX3BlbmRpbmdBY3Rpb25zID0gW107XG4gIHpyLmFuaW1hdGlvbi5vbignZnJhbWUnLCB0aGlzLl9vbmZyYW1lLCB0aGlzKTtcbiAgYmluZFJlbmRlcmVkRXZlbnQoenIsIHRoaXMpOyAvLyBFQ2hhcnRzIGluc3RhbmNlIGNhbiBiZSB1c2VkIGFzIHZhbHVlLlxuXG4gIHpyVXRpbC5zZXRBc1ByaW1pdGl2ZSh0aGlzKTtcbn1cblxudmFyIGVjaGFydHNQcm90byA9IEVDaGFydHMucHJvdG90eXBlO1xuXG5lY2hhcnRzUHJvdG8uX29uZnJhbWUgPSBmdW5jdGlvbiAoKSB7XG4gIGlmICh0aGlzLl9kaXNwb3NlZCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzY2hlZHVsZXIgPSB0aGlzLl9zY2hlZHVsZXI7IC8vIExhenkgdXBkYXRlXG5cbiAgaWYgKHRoaXNbT1BUSU9OX1VQREFURURdKSB7XG4gICAgdmFyIHNpbGVudCA9IHRoaXNbT1BUSU9OX1VQREFURURdLnNpbGVudDtcbiAgICB0aGlzW0lOX01BSU5fUFJPQ0VTU10gPSB0cnVlO1xuICAgIHByZXBhcmUodGhpcyk7XG4gICAgdXBkYXRlTWV0aG9kcy51cGRhdGUuY2FsbCh0aGlzKTtcbiAgICB0aGlzW0lOX01BSU5fUFJPQ0VTU10gPSBmYWxzZTtcbiAgICB0aGlzW09QVElPTl9VUERBVEVEXSA9IGZhbHNlO1xuICAgIGZsdXNoUGVuZGluZ0FjdGlvbnMuY2FsbCh0aGlzLCBzaWxlbnQpO1xuICAgIHRyaWdnZXJVcGRhdGVkRXZlbnQuY2FsbCh0aGlzLCBzaWxlbnQpO1xuICB9IC8vIEF2b2lkIGRvIGJvdGggbGF6eSB1cGRhdGUgYW5kIHByb2dyZXNzIGluIG9uZSBmcmFtZS5cbiAgZWxzZSBpZiAoc2NoZWR1bGVyLnVuZmluaXNoZWQpIHtcbiAgICAgIC8vIFN0cmVhbSBwcm9ncmVzcy5cbiAgICAgIHZhciByZW1haW5UaW1lID0gVEVTVF9GUkFNRV9SRU1BSU5fVElNRTtcbiAgICAgIHZhciBlY01vZGVsID0gdGhpcy5fbW9kZWw7XG4gICAgICB2YXIgYXBpID0gdGhpcy5fYXBpO1xuICAgICAgc2NoZWR1bGVyLnVuZmluaXNoZWQgPSBmYWxzZTtcblxuICAgICAgZG8ge1xuICAgICAgICB2YXIgc3RhcnRUaW1lID0gK25ldyBEYXRlKCk7XG4gICAgICAgIHNjaGVkdWxlci5wZXJmb3JtU2VyaWVzVGFza3MoZWNNb2RlbCk7IC8vIEN1cnJlbnRseSBkYXRhUHJvY2Vzc29yRnVuY3MgZG8gbm90IGNoZWNrIHRocmVzaG9sZC5cblxuICAgICAgICBzY2hlZHVsZXIucGVyZm9ybURhdGFQcm9jZXNzb3JUYXNrcyhlY01vZGVsKTtcbiAgICAgICAgdXBkYXRlU3RyZWFtTW9kZXModGhpcywgZWNNb2RlbCk7IC8vIERvIG5vdCB1cGRhdGUgY29vcmRpbmF0ZSBzeXN0ZW0gaGVyZS4gQmVjYXVzZSB0aGF0IGNvb3JkIHN5c3RlbSB1cGRhdGUgaW5cbiAgICAgICAgLy8gZWFjaCBmcmFtZSBpcyBub3QgYSBnb29kIHVzZXIgZXhwZXJpZW5jZS4gU28gd2UgZm9sbG93IHRoZSBydWxlIHRoYXRcbiAgICAgICAgLy8gdGhlIGV4dGVudCBvZiB0aGUgY29vcmRpbmF0ZSBzeXN0ZW0gaXMgZGV0ZXJtaW4gaW4gdGhlIGZpcnN0IGZyYW1lICh0aGVcbiAgICAgICAgLy8gZnJhbWUgaXMgZXhlY3V0ZWQgaW1tZWRpZXRlbHkgYWZ0ZXIgdGFzayByZXNldC5cbiAgICAgICAgLy8gdGhpcy5fY29vcmRTeXNNZ3IudXBkYXRlKGVjTW9kZWwsIGFwaSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCctLS0gZWMgZnJhbWUgdmlzdWFsIC0tLScsIHJlbWFpblRpbWUpO1xuXG4gICAgICAgIHNjaGVkdWxlci5wZXJmb3JtVmlzdWFsVGFza3MoZWNNb2RlbCk7XG4gICAgICAgIHJlbmRlclNlcmllcyh0aGlzLCB0aGlzLl9tb2RlbCwgYXBpLCAncmVtYWluJyk7XG4gICAgICAgIHJlbWFpblRpbWUgLT0gK25ldyBEYXRlKCkgLSBzdGFydFRpbWU7XG4gICAgICB9IHdoaWxlIChyZW1haW5UaW1lID4gMCAmJiBzY2hlZHVsZXIudW5maW5pc2hlZCk7IC8vIENhbGwgZmx1c2ggZXhwbGljaXRseSBmb3IgdHJpZ2dlciBmaW5pc2hlZCBldmVudC5cblxuXG4gICAgICBpZiAoIXNjaGVkdWxlci51bmZpbmlzaGVkKSB7XG4gICAgICAgIHRoaXMuX3pyLmZsdXNoKCk7XG4gICAgICB9IC8vIEVsc2UsIHpyIGZsdXNoaW5nIGJlIGVuc3VlIHdpdGhpbiB0aGUgc2FtZSBmcmFtZSxcbiAgICAgIC8vIGJlY2F1c2UgenIgZmx1c2hpbmcgaXMgYWZ0ZXIgb25mcmFtZSBldmVudC5cblxuICAgIH1cbn07XG4vKipcbiAqIEByZXR1cm4ge0hUTUxFbGVtZW50fVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmdldERvbSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX2RvbTtcbn07XG4vKipcbiAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyflpSZW5kZXJ9XG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uZ2V0WnIgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl96cjtcbn07XG4vKipcbiAqIFVzYWdlOlxuICogY2hhcnQuc2V0T3B0aW9uKG9wdGlvbiwgbm90TWVyZ2UsIGxhenlVcGRhdGUpO1xuICogY2hhcnQuc2V0T3B0aW9uKG9wdGlvbiwge1xuICogICAgIG5vdE1lcmdlOiAuLi4sXG4gKiAgICAgbGF6eVVwZGF0ZTogLi4uLFxuICogICAgIHNpbGVudDogLi4uXG4gKiB9KTtcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uXG4gKiBAcGFyYW0ge09iamVjdHxib29sZWFufSBbb3B0c10gb3B0cyBvciBub3RNZXJnZS5cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMubm90TWVyZ2U9ZmFsc2VdXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvcHRzLmxhenlVcGRhdGU9ZmFsc2VdIFVzZWZ1bCB3aGVuIHNldE9wdGlvbiBmcmVxdWVudGx5LlxuICovXG5cblxuZWNoYXJ0c1Byb3RvLnNldE9wdGlvbiA9IGZ1bmN0aW9uIChvcHRpb24sIG5vdE1lcmdlLCBsYXp5VXBkYXRlKSB7XG4gIHZhciBzaWxlbnQ7XG5cbiAgaWYgKGlzT2JqZWN0KG5vdE1lcmdlKSkge1xuICAgIGxhenlVcGRhdGUgPSBub3RNZXJnZS5sYXp5VXBkYXRlO1xuICAgIHNpbGVudCA9IG5vdE1lcmdlLnNpbGVudDtcbiAgICBub3RNZXJnZSA9IG5vdE1lcmdlLm5vdE1lcmdlO1xuICB9XG5cbiAgdGhpc1tJTl9NQUlOX1BST0NFU1NdID0gdHJ1ZTtcblxuICBpZiAoIXRoaXMuX21vZGVsIHx8IG5vdE1lcmdlKSB7XG4gICAgdmFyIG9wdGlvbk1hbmFnZXIgPSBuZXcgT3B0aW9uTWFuYWdlcih0aGlzLl9hcGkpO1xuICAgIHZhciB0aGVtZSA9IHRoaXMuX3RoZW1lO1xuICAgIHZhciBlY01vZGVsID0gdGhpcy5fbW9kZWwgPSBuZXcgR2xvYmFsTW9kZWwobnVsbCwgbnVsbCwgdGhlbWUsIG9wdGlvbk1hbmFnZXIpO1xuICAgIGVjTW9kZWwuc2NoZWR1bGVyID0gdGhpcy5fc2NoZWR1bGVyO1xuICAgIGVjTW9kZWwuaW5pdChudWxsLCBudWxsLCB0aGVtZSwgb3B0aW9uTWFuYWdlcik7XG4gIH1cblxuICB0aGlzLl9tb2RlbC5zZXRPcHRpb24ob3B0aW9uLCBvcHRpb25QcmVwcm9jZXNzb3JGdW5jcyk7XG5cbiAgaWYgKGxhenlVcGRhdGUpIHtcbiAgICB0aGlzW09QVElPTl9VUERBVEVEXSA9IHtcbiAgICAgIHNpbGVudDogc2lsZW50XG4gICAgfTtcbiAgICB0aGlzW0lOX01BSU5fUFJPQ0VTU10gPSBmYWxzZTtcbiAgfSBlbHNlIHtcbiAgICBwcmVwYXJlKHRoaXMpO1xuICAgIHVwZGF0ZU1ldGhvZHMudXBkYXRlLmNhbGwodGhpcyk7IC8vIEVuc3VyZSB6ciByZWZyZXNoIHN5Y2hyb25vdXNseSwgYW5kIHRoZW4gcGl4ZWwgaW4gY2FudmFzIGNhbiBiZVxuICAgIC8vIGZldGNoZWQgYWZ0ZXIgYHNldE9wdGlvbmAuXG5cbiAgICB0aGlzLl96ci5mbHVzaCgpO1xuXG4gICAgdGhpc1tPUFRJT05fVVBEQVRFRF0gPSBmYWxzZTtcbiAgICB0aGlzW0lOX01BSU5fUFJPQ0VTU10gPSBmYWxzZTtcbiAgICBmbHVzaFBlbmRpbmdBY3Rpb25zLmNhbGwodGhpcywgc2lsZW50KTtcbiAgICB0cmlnZ2VyVXBkYXRlZEV2ZW50LmNhbGwodGhpcywgc2lsZW50KTtcbiAgfVxufTtcbi8qKlxuICogQERFUFJFQ0FURURcbiAqL1xuXG5cbmVjaGFydHNQcm90by5zZXRUaGVtZSA9IGZ1bmN0aW9uICgpIHtcbiAgY29uc29sZS5lcnJvcignRUNoYXJ0cyNzZXRUaGVtZSgpIGlzIERFUFJFQ0FURUQgaW4gRUNoYXJ0cyAzLjAnKTtcbn07XG4vKipcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH1cbiAqL1xuXG5cbmVjaGFydHNQcm90by5nZXRNb2RlbCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX21vZGVsO1xufTtcbi8qKlxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmdldE9wdGlvbiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX21vZGVsICYmIHRoaXMuX21vZGVsLmdldE9wdGlvbigpO1xufTtcbi8qKlxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmdldFdpZHRoID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fenIuZ2V0V2lkdGgoKTtcbn07XG4vKipcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmVjaGFydHNQcm90by5nZXRIZWlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl96ci5nZXRIZWlnaHQoKTtcbn07XG4vKipcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmVjaGFydHNQcm90by5nZXREZXZpY2VQaXhlbFJhdGlvID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fenIucGFpbnRlci5kcHIgfHwgd2luZG93LmRldmljZVBpeGVsUmF0aW8gfHwgMTtcbn07XG4vKipcbiAqIEdldCBjYW52YXMgd2hpY2ggaGFzIGFsbCB0aGluZyByZW5kZXJlZFxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5iYWNrZ3JvdW5kQ29sb3JdXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uZ2V0UmVuZGVyZWRDYW52YXMgPSBmdW5jdGlvbiAob3B0cykge1xuICBpZiAoIWVudi5jYW52YXNTdXBwb3J0ZWQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBvcHRzID0gb3B0cyB8fCB7fTtcbiAgb3B0cy5waXhlbFJhdGlvID0gb3B0cy5waXhlbFJhdGlvIHx8IDE7XG4gIG9wdHMuYmFja2dyb3VuZENvbG9yID0gb3B0cy5iYWNrZ3JvdW5kQ29sb3IgfHwgdGhpcy5fbW9kZWwuZ2V0KCdiYWNrZ3JvdW5kQ29sb3InKTtcbiAgdmFyIHpyID0gdGhpcy5fenI7IC8vIHZhciBsaXN0ID0genIuc3RvcmFnZS5nZXREaXNwbGF5TGlzdCgpO1xuICAvLyBTdG9wIGFuaW1hdGlvbnNcbiAgLy8gTmV2ZXIgd29ya3MgYmVmb3JlIGluIGluaXQgYW5pbWF0aW9uLCBzbyByZW1vdmUgaXQuXG4gIC8vIHpyVXRpbC5lYWNoKGxpc3QsIGZ1bmN0aW9uIChlbCkge1xuICAvLyAgICAgZWwuc3RvcEFuaW1hdGlvbih0cnVlKTtcbiAgLy8gfSk7XG5cbiAgcmV0dXJuIHpyLnBhaW50ZXIuZ2V0UmVuZGVyZWRDYW52YXMob3B0cyk7XG59O1xuLyoqXG4gKiBHZXQgc3ZnIGRhdGEgdXJsXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uZ2V0U3ZnRGF0YVVybCA9IGZ1bmN0aW9uICgpIHtcbiAgaWYgKCFlbnYuc3ZnU3VwcG9ydGVkKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHpyID0gdGhpcy5fenI7XG4gIHZhciBsaXN0ID0genIuc3RvcmFnZS5nZXREaXNwbGF5TGlzdCgpOyAvLyBTdG9wIGFuaW1hdGlvbnNcblxuICB6clV0aWwuZWFjaChsaXN0LCBmdW5jdGlvbiAoZWwpIHtcbiAgICBlbC5zdG9wQW5pbWF0aW9uKHRydWUpO1xuICB9KTtcbiAgcmV0dXJuIHpyLnBhaW50ZXIucGF0aFRvRGF0YVVybCgpO1xufTtcbi8qKlxuICogQHJldHVybiB7c3RyaW5nfVxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy50eXBlPSdwbmcnXVxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLnBpeGVsUmF0aW89MV1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5iYWNrZ3JvdW5kQ29sb3JdXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdHMuZXhjbHVkZUNvbXBvbmVudHNdXG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uZ2V0RGF0YVVSTCA9IGZ1bmN0aW9uIChvcHRzKSB7XG4gIG9wdHMgPSBvcHRzIHx8IHt9O1xuICB2YXIgZXhjbHVkZUNvbXBvbmVudHMgPSBvcHRzLmV4Y2x1ZGVDb21wb25lbnRzO1xuICB2YXIgZWNNb2RlbCA9IHRoaXMuX21vZGVsO1xuICB2YXIgZXhjbHVkZXNDb21wb25lbnRWaWV3cyA9IFtdO1xuICB2YXIgc2VsZiA9IHRoaXM7XG4gIGVhY2goZXhjbHVkZUNvbXBvbmVudHMsIGZ1bmN0aW9uIChjb21wb25lbnRUeXBlKSB7XG4gICAgZWNNb2RlbC5lYWNoQ29tcG9uZW50KHtcbiAgICAgIG1haW5UeXBlOiBjb21wb25lbnRUeXBlXG4gICAgfSwgZnVuY3Rpb24gKGNvbXBvbmVudCkge1xuICAgICAgdmFyIHZpZXcgPSBzZWxmLl9jb21wb25lbnRzTWFwW2NvbXBvbmVudC5fX3ZpZXdJZF07XG5cbiAgICAgIGlmICghdmlldy5ncm91cC5pZ25vcmUpIHtcbiAgICAgICAgZXhjbHVkZXNDb21wb25lbnRWaWV3cy5wdXNoKHZpZXcpO1xuICAgICAgICB2aWV3Lmdyb3VwLmlnbm9yZSA9IHRydWU7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuICB2YXIgdXJsID0gdGhpcy5fenIucGFpbnRlci5nZXRUeXBlKCkgPT09ICdzdmcnID8gdGhpcy5nZXRTdmdEYXRhVXJsKCkgOiB0aGlzLmdldFJlbmRlcmVkQ2FudmFzKG9wdHMpLnRvRGF0YVVSTCgnaW1hZ2UvJyArIChvcHRzICYmIG9wdHMudHlwZSB8fCAncG5nJykpO1xuICBlYWNoKGV4Y2x1ZGVzQ29tcG9uZW50Vmlld3MsIGZ1bmN0aW9uICh2aWV3KSB7XG4gICAgdmlldy5ncm91cC5pZ25vcmUgPSBmYWxzZTtcbiAgfSk7XG4gIHJldHVybiB1cmw7XG59O1xuLyoqXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLnR5cGU9J3BuZyddXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdHMucGl4ZWxSYXRpbz0xXVxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHRzLmJhY2tncm91bmRDb2xvcl1cbiAqL1xuXG5cbmVjaGFydHNQcm90by5nZXRDb25uZWN0ZWREYXRhVVJMID0gZnVuY3Rpb24gKG9wdHMpIHtcbiAgaWYgKCFlbnYuY2FudmFzU3VwcG9ydGVkKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGdyb3VwSWQgPSB0aGlzLmdyb3VwO1xuICB2YXIgbWF0aE1pbiA9IE1hdGgubWluO1xuICB2YXIgbWF0aE1heCA9IE1hdGgubWF4O1xuICB2YXIgTUFYX05VTUJFUiA9IEluZmluaXR5O1xuXG4gIGlmIChjb25uZWN0ZWRHcm91cHNbZ3JvdXBJZF0pIHtcbiAgICB2YXIgbGVmdCA9IE1BWF9OVU1CRVI7XG4gICAgdmFyIHRvcCA9IE1BWF9OVU1CRVI7XG4gICAgdmFyIHJpZ2h0ID0gLU1BWF9OVU1CRVI7XG4gICAgdmFyIGJvdHRvbSA9IC1NQVhfTlVNQkVSO1xuICAgIHZhciBjYW52YXNMaXN0ID0gW107XG4gICAgdmFyIGRwciA9IG9wdHMgJiYgb3B0cy5waXhlbFJhdGlvIHx8IDE7XG4gICAgenJVdGlsLmVhY2goaW5zdGFuY2VzLCBmdW5jdGlvbiAoY2hhcnQsIGlkKSB7XG4gICAgICBpZiAoY2hhcnQuZ3JvdXAgPT09IGdyb3VwSWQpIHtcbiAgICAgICAgdmFyIGNhbnZhcyA9IGNoYXJ0LmdldFJlbmRlcmVkQ2FudmFzKHpyVXRpbC5jbG9uZShvcHRzKSk7XG4gICAgICAgIHZhciBib3VuZGluZ1JlY3QgPSBjaGFydC5nZXREb20oKS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgbGVmdCA9IG1hdGhNaW4oYm91bmRpbmdSZWN0LmxlZnQsIGxlZnQpO1xuICAgICAgICB0b3AgPSBtYXRoTWluKGJvdW5kaW5nUmVjdC50b3AsIHRvcCk7XG4gICAgICAgIHJpZ2h0ID0gbWF0aE1heChib3VuZGluZ1JlY3QucmlnaHQsIHJpZ2h0KTtcbiAgICAgICAgYm90dG9tID0gbWF0aE1heChib3VuZGluZ1JlY3QuYm90dG9tLCBib3R0b20pO1xuICAgICAgICBjYW52YXNMaXN0LnB1c2goe1xuICAgICAgICAgIGRvbTogY2FudmFzLFxuICAgICAgICAgIGxlZnQ6IGJvdW5kaW5nUmVjdC5sZWZ0LFxuICAgICAgICAgIHRvcDogYm91bmRpbmdSZWN0LnRvcFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBsZWZ0ICo9IGRwcjtcbiAgICB0b3AgKj0gZHByO1xuICAgIHJpZ2h0ICo9IGRwcjtcbiAgICBib3R0b20gKj0gZHByO1xuICAgIHZhciB3aWR0aCA9IHJpZ2h0IC0gbGVmdDtcbiAgICB2YXIgaGVpZ2h0ID0gYm90dG9tIC0gdG9wO1xuICAgIHZhciB0YXJnZXRDYW52YXMgPSB6clV0aWwuY3JlYXRlQ2FudmFzKCk7XG4gICAgdGFyZ2V0Q2FudmFzLndpZHRoID0gd2lkdGg7XG4gICAgdGFyZ2V0Q2FudmFzLmhlaWdodCA9IGhlaWdodDtcbiAgICB2YXIgenIgPSB6cmVuZGVyLmluaXQodGFyZ2V0Q2FudmFzKTtcbiAgICBlYWNoKGNhbnZhc0xpc3QsIGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICB2YXIgaW1nID0gbmV3IGdyYXBoaWMuSW1hZ2Uoe1xuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIHg6IGl0ZW0ubGVmdCAqIGRwciAtIGxlZnQsXG4gICAgICAgICAgeTogaXRlbS50b3AgKiBkcHIgLSB0b3AsXG4gICAgICAgICAgaW1hZ2U6IGl0ZW0uZG9tXG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgenIuYWRkKGltZyk7XG4gICAgfSk7XG4gICAgenIucmVmcmVzaEltbWVkaWF0ZWx5KCk7XG4gICAgcmV0dXJuIHRhcmdldENhbnZhcy50b0RhdGFVUkwoJ2ltYWdlLycgKyAob3B0cyAmJiBvcHRzLnR5cGUgfHwgJ3BuZycpKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gdGhpcy5nZXREYXRhVVJMKG9wdHMpO1xuICB9XG59O1xuLyoqXG4gKiBDb252ZXJ0IGZyb20gbG9naWNhbCBjb29yZGluYXRlIHN5c3RlbSB0byBwaXhlbCBjb29yZGluYXRlIHN5c3RlbS5cbiAqIFNlZSBDb29yZGluYXRlU3lzdGVtI2NvbnZlcnRUb1BpeGVsLlxuICogQHBhcmFtIHtzdHJpbmd8T2JqZWN0fSBmaW5kZXJcbiAqICAgICAgICBJZiBzdHJpbmcsIGUuZy4sICdnZW8nLCBtZWFucyB7Z2VvSW5kZXg6IDB9LlxuICogICAgICAgIElmIE9iamVjdCwgY291bGQgY29udGFpbiBzb21lIG9mIHRoZXNlIHByb3BlcnRpZXMgYmVsb3c6XG4gKiAgICAgICAge1xuICogICAgICAgICAgICBzZXJpZXNJbmRleCAvIHNlcmllc0lkIC8gc2VyaWVzTmFtZSxcbiAqICAgICAgICAgICAgZ2VvSW5kZXggLyBnZW9JZCwgZ2VvTmFtZSxcbiAqICAgICAgICAgICAgYm1hcEluZGV4IC8gYm1hcElkIC8gYm1hcE5hbWUsXG4gKiAgICAgICAgICAgIHhBeGlzSW5kZXggLyB4QXhpc0lkIC8geEF4aXNOYW1lLFxuICogICAgICAgICAgICB5QXhpc0luZGV4IC8geUF4aXNJZCAvIHlBeGlzTmFtZSxcbiAqICAgICAgICAgICAgZ3JpZEluZGV4IC8gZ3JpZElkIC8gZ3JpZE5hbWUsXG4gKiAgICAgICAgICAgIC4uLiAoY2FuIGJlIGV4dGVuZGVkKVxuICogICAgICAgIH1cbiAqIEBwYXJhbSB7QXJyYXl8bnVtYmVyfSB2YWx1ZVxuICogQHJldHVybiB7QXJyYXl8bnVtYmVyfSByZXN1bHRcbiAqL1xuXG5cbmVjaGFydHNQcm90by5jb252ZXJ0VG9QaXhlbCA9IHpyVXRpbC5jdXJyeShkb0NvbnZlcnRQaXhlbCwgJ2NvbnZlcnRUb1BpeGVsJyk7XG4vKipcbiAqIENvbnZlcnQgZnJvbSBwaXhlbCBjb29yZGluYXRlIHN5c3RlbSB0byBsb2dpY2FsIGNvb3JkaW5hdGUgc3lzdGVtLlxuICogU2VlIENvb3JkaW5hdGVTeXN0ZW0jY29udmVydEZyb21QaXhlbC5cbiAqIEBwYXJhbSB7c3RyaW5nfE9iamVjdH0gZmluZGVyXG4gKiAgICAgICAgSWYgc3RyaW5nLCBlLmcuLCAnZ2VvJywgbWVhbnMge2dlb0luZGV4OiAwfS5cbiAqICAgICAgICBJZiBPYmplY3QsIGNvdWxkIGNvbnRhaW4gc29tZSBvZiB0aGVzZSBwcm9wZXJ0aWVzIGJlbG93OlxuICogICAgICAgIHtcbiAqICAgICAgICAgICAgc2VyaWVzSW5kZXggLyBzZXJpZXNJZCAvIHNlcmllc05hbWUsXG4gKiAgICAgICAgICAgIGdlb0luZGV4IC8gZ2VvSWQgLyBnZW9OYW1lLFxuICogICAgICAgICAgICBibWFwSW5kZXggLyBibWFwSWQgLyBibWFwTmFtZSxcbiAqICAgICAgICAgICAgeEF4aXNJbmRleCAvIHhBeGlzSWQgLyB4QXhpc05hbWUsXG4gKiAgICAgICAgICAgIHlBeGlzSW5kZXggLyB5QXhpc0lkIC8geUF4aXNOYW1lXG4gKiAgICAgICAgICAgIGdyaWRJbmRleCAvIGdyaWRJZCAvIGdyaWROYW1lLFxuICogICAgICAgICAgICAuLi4gKGNhbiBiZSBleHRlbmRlZClcbiAqICAgICAgICB9XG4gKiBAcGFyYW0ge0FycmF5fG51bWJlcn0gdmFsdWVcbiAqIEByZXR1cm4ge0FycmF5fG51bWJlcn0gcmVzdWx0XG4gKi9cblxuZWNoYXJ0c1Byb3RvLmNvbnZlcnRGcm9tUGl4ZWwgPSB6clV0aWwuY3VycnkoZG9Db252ZXJ0UGl4ZWwsICdjb252ZXJ0RnJvbVBpeGVsJyk7XG5cbmZ1bmN0aW9uIGRvQ29udmVydFBpeGVsKG1ldGhvZE5hbWUsIGZpbmRlciwgdmFsdWUpIHtcbiAgdmFyIGVjTW9kZWwgPSB0aGlzLl9tb2RlbDtcblxuICB2YXIgY29vcmRTeXNMaXN0ID0gdGhpcy5fY29vcmRTeXNNZ3IuZ2V0Q29vcmRpbmF0ZVN5c3RlbXMoKTtcblxuICB2YXIgcmVzdWx0O1xuICBmaW5kZXIgPSBtb2RlbFV0aWwucGFyc2VGaW5kZXIoZWNNb2RlbCwgZmluZGVyKTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGNvb3JkU3lzTGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZhciBjb29yZFN5cyA9IGNvb3JkU3lzTGlzdFtpXTtcblxuICAgIGlmIChjb29yZFN5c1ttZXRob2ROYW1lXSAmJiAocmVzdWx0ID0gY29vcmRTeXNbbWV0aG9kTmFtZV0oZWNNb2RlbCwgZmluZGVyLCB2YWx1ZSkpICE9IG51bGwpIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuICB9XG59XG4vKipcbiAqIElzIHRoZSBzcGVjaWZpZWQgY29vcmRpbmF0ZSBzeXN0ZW1zIG9yIGNvbXBvbmVudHMgY29udGFpbiB0aGUgZ2l2ZW4gcGl4ZWwgcG9pbnQuXG4gKiBAcGFyYW0ge3N0cmluZ3xPYmplY3R9IGZpbmRlclxuICogICAgICAgIElmIHN0cmluZywgZS5nLiwgJ2dlbycsIG1lYW5zIHtnZW9JbmRleDogMH0uXG4gKiAgICAgICAgSWYgT2JqZWN0LCBjb3VsZCBjb250YWluIHNvbWUgb2YgdGhlc2UgcHJvcGVydGllcyBiZWxvdzpcbiAqICAgICAgICB7XG4gKiAgICAgICAgICAgIHNlcmllc0luZGV4IC8gc2VyaWVzSWQgLyBzZXJpZXNOYW1lLFxuICogICAgICAgICAgICBnZW9JbmRleCAvIGdlb0lkIC8gZ2VvTmFtZSxcbiAqICAgICAgICAgICAgYm1hcEluZGV4IC8gYm1hcElkIC8gYm1hcE5hbWUsXG4gKiAgICAgICAgICAgIHhBeGlzSW5kZXggLyB4QXhpc0lkIC8geEF4aXNOYW1lLFxuICogICAgICAgICAgICB5QXhpc0luZGV4IC8geUF4aXNJZCAvIHlBeGlzTmFtZSxcbiAqICAgICAgICAgICAgZ3JpZEluZGV4IC8gZ3JpZElkIC8gZ3JpZE5hbWUsXG4gKiAgICAgICAgICAgIC4uLiAoY2FuIGJlIGV4dGVuZGVkKVxuICogICAgICAgIH1cbiAqIEBwYXJhbSB7QXJyYXl8bnVtYmVyfSB2YWx1ZVxuICogQHJldHVybiB7Ym9vbGVhbn0gcmVzdWx0XG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uY29udGFpblBpeGVsID0gZnVuY3Rpb24gKGZpbmRlciwgdmFsdWUpIHtcbiAgdmFyIGVjTW9kZWwgPSB0aGlzLl9tb2RlbDtcbiAgdmFyIHJlc3VsdDtcbiAgZmluZGVyID0gbW9kZWxVdGlsLnBhcnNlRmluZGVyKGVjTW9kZWwsIGZpbmRlcik7XG4gIHpyVXRpbC5lYWNoKGZpbmRlciwgZnVuY3Rpb24gKG1vZGVscywga2V5KSB7XG4gICAga2V5LmluZGV4T2YoJ01vZGVscycpID49IDAgJiYgenJVdGlsLmVhY2gobW9kZWxzLCBmdW5jdGlvbiAobW9kZWwpIHtcbiAgICAgIHZhciBjb29yZFN5cyA9IG1vZGVsLmNvb3JkaW5hdGVTeXN0ZW07XG5cbiAgICAgIGlmIChjb29yZFN5cyAmJiBjb29yZFN5cy5jb250YWluUG9pbnQpIHtcbiAgICAgICAgcmVzdWx0IHw9ICEhY29vcmRTeXMuY29udGFpblBvaW50KHZhbHVlKTtcbiAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSAnc2VyaWVzTW9kZWxzJykge1xuICAgICAgICB2YXIgdmlldyA9IHRoaXMuX2NoYXJ0c01hcFttb2RlbC5fX3ZpZXdJZF07XG5cbiAgICAgICAgaWYgKHZpZXcgJiYgdmlldy5jb250YWluUG9pbnQpIHtcbiAgICAgICAgICByZXN1bHQgfD0gdmlldy5jb250YWluUG9pbnQodmFsdWUsIG1vZGVsKTtcbiAgICAgICAgfSBlbHNlIHt9XG4gICAgICB9IGVsc2Uge31cbiAgICB9LCB0aGlzKTtcbiAgfSwgdGhpcyk7XG4gIHJldHVybiAhIXJlc3VsdDtcbn07XG4vKipcbiAqIEdldCB2aXN1YWwgZnJvbSBzZXJpZXMgb3IgZGF0YS5cbiAqIEBwYXJhbSB7c3RyaW5nfE9iamVjdH0gZmluZGVyXG4gKiAgICAgICAgSWYgc3RyaW5nLCBlLmcuLCAnc2VyaWVzJywgbWVhbnMge3Nlcmllc0luZGV4OiAwfS5cbiAqICAgICAgICBJZiBPYmplY3QsIGNvdWxkIGNvbnRhaW4gc29tZSBvZiB0aGVzZSBwcm9wZXJ0aWVzIGJlbG93OlxuICogICAgICAgIHtcbiAqICAgICAgICAgICAgc2VyaWVzSW5kZXggLyBzZXJpZXNJZCAvIHNlcmllc05hbWUsXG4gKiAgICAgICAgICAgIGRhdGFJbmRleCAvIGRhdGFJbmRleEluc2lkZVxuICogICAgICAgIH1cbiAqICAgICAgICBJZiBkYXRhSW5kZXggaXMgbm90IHNwZWNpZmllZCwgc2VyaWVzIHZpc3VhbCB3aWxsIGJlIGZldGNoZWQsXG4gKiAgICAgICAgYnV0IG5vdCBkYXRhIGl0ZW0gdmlzdWFsLlxuICogICAgICAgIElmIGFsbCBvZiBzZXJpZXNJbmRleCwgc2VyaWVzSWQsIHNlcmllc05hbWUgYXJlIG5vdCBzcGVjaWZpZWQsXG4gKiAgICAgICAgdmlzdWFsIHdpbGwgYmUgZmV0Y2hlZCBmcm9tIGZpcnN0IHNlcmllcy5cbiAqIEBwYXJhbSB7c3RyaW5nfSB2aXN1YWxUeXBlICdjb2xvcicsICdzeW1ib2wnLCAnc3ltYm9sU2l6ZSdcbiAqL1xuXG5cbmVjaGFydHNQcm90by5nZXRWaXN1YWwgPSBmdW5jdGlvbiAoZmluZGVyLCB2aXN1YWxUeXBlKSB7XG4gIHZhciBlY01vZGVsID0gdGhpcy5fbW9kZWw7XG4gIGZpbmRlciA9IG1vZGVsVXRpbC5wYXJzZUZpbmRlcihlY01vZGVsLCBmaW5kZXIsIHtcbiAgICBkZWZhdWx0TWFpblR5cGU6ICdzZXJpZXMnXG4gIH0pO1xuICB2YXIgc2VyaWVzTW9kZWwgPSBmaW5kZXIuc2VyaWVzTW9kZWw7XG4gIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICB2YXIgZGF0YUluZGV4SW5zaWRlID0gZmluZGVyLmhhc093blByb3BlcnR5KCdkYXRhSW5kZXhJbnNpZGUnKSA/IGZpbmRlci5kYXRhSW5kZXhJbnNpZGUgOiBmaW5kZXIuaGFzT3duUHJvcGVydHkoJ2RhdGFJbmRleCcpID8gZGF0YS5pbmRleE9mUmF3SW5kZXgoZmluZGVyLmRhdGFJbmRleCkgOiBudWxsO1xuICByZXR1cm4gZGF0YUluZGV4SW5zaWRlICE9IG51bGwgPyBkYXRhLmdldEl0ZW1WaXN1YWwoZGF0YUluZGV4SW5zaWRlLCB2aXN1YWxUeXBlKSA6IGRhdGEuZ2V0VmlzdWFsKHZpc3VhbFR5cGUpO1xufTtcbi8qKlxuICogR2V0IHZpZXcgb2YgY29ycmVzcG9uZGluZyBjb21wb25lbnQgbW9kZWxcbiAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudH0gY29tcG9uZW50TW9kZWxcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL3ZpZXcvQ29tcG9uZW50fVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmdldFZpZXdPZkNvbXBvbmVudE1vZGVsID0gZnVuY3Rpb24gKGNvbXBvbmVudE1vZGVsKSB7XG4gIHJldHVybiB0aGlzLl9jb21wb25lbnRzTWFwW2NvbXBvbmVudE1vZGVsLl9fdmlld0lkXTtcbn07XG4vKipcbiAqIEdldCB2aWV3IG9mIGNvcnJlc3BvbmRpbmcgc2VyaWVzIG1vZGVsXG4gKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXN9IHNlcmllc01vZGVsXG4gKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy92aWV3L0NoYXJ0fVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmdldFZpZXdPZlNlcmllc01vZGVsID0gZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gIHJldHVybiB0aGlzLl9jaGFydHNNYXBbc2VyaWVzTW9kZWwuX192aWV3SWRdO1xufTtcblxudmFyIHVwZGF0ZU1ldGhvZHMgPSB7XG4gIHByZXBhcmVBbmRVcGRhdGU6IGZ1bmN0aW9uIChwYXlsb2FkKSB7XG4gICAgcHJlcGFyZSh0aGlzKTtcbiAgICB1cGRhdGVNZXRob2RzLnVwZGF0ZS5jYWxsKHRoaXMsIHBheWxvYWQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdXBkYXRlOiBmdW5jdGlvbiAocGF5bG9hZCkge1xuICAgIC8vIGNvbnNvbGUucHJvZmlsZSAmJiBjb25zb2xlLnByb2ZpbGUoJ3VwZGF0ZScpO1xuICAgIHZhciBlY01vZGVsID0gdGhpcy5fbW9kZWw7XG4gICAgdmFyIGFwaSA9IHRoaXMuX2FwaTtcbiAgICB2YXIgenIgPSB0aGlzLl96cjtcbiAgICB2YXIgY29vcmRTeXNNZ3IgPSB0aGlzLl9jb29yZFN5c01ncjtcbiAgICB2YXIgc2NoZWR1bGVyID0gdGhpcy5fc2NoZWR1bGVyOyAvLyB1cGRhdGUgYmVmb3JlIHNldE9wdGlvblxuXG4gICAgaWYgKCFlY01vZGVsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgc2NoZWR1bGVyLnJlc3RvcmVEYXRhKGVjTW9kZWwsIHBheWxvYWQpO1xuICAgIHNjaGVkdWxlci5wZXJmb3JtU2VyaWVzVGFza3MoZWNNb2RlbCk7IC8vIFRPRE9cbiAgICAvLyBTYXZlIHRvdGFsIGVjTW9kZWwgaGVyZSBmb3IgdW5kby9yZWRvIChhZnRlciByZXN0b3JpbmcgZGF0YSBhbmQgYmVmb3JlIHByb2Nlc3NpbmcgZGF0YSkuXG4gICAgLy8gVW5kbyAocmVzdG9yYXRpb24gb2YgdG90YWwgZWNNb2RlbCkgY2FuIGJlIGNhcnJpZWQgb3V0IGluICdhY3Rpb24nIG9yIG91dHNpZGUgQVBJIGNhbGwuXG4gICAgLy8gQ3JlYXRlIG5ldyBjb29yZGluYXRlIHN5c3RlbSBlYWNoIHVwZGF0ZVxuICAgIC8vIEluIExpbmVWaWV3IG1heSBzYXZlIHRoZSBvbGQgY29vcmRpbmF0ZSBzeXN0ZW0gYW5kIHVzZSBpdCB0byBnZXQgdGhlIG9yaWduYWwgcG9pbnRcblxuICAgIGNvb3JkU3lzTWdyLmNyZWF0ZShlY01vZGVsLCBhcGkpO1xuICAgIHNjaGVkdWxlci5wZXJmb3JtRGF0YVByb2Nlc3NvclRhc2tzKGVjTW9kZWwsIHBheWxvYWQpOyAvLyBDdXJyZW50IHN0cmVhbSByZW5kZXIgaXMgbm90IHN1cHBvcnRlZCBpbiBkYXRhIHByb2Nlc3MuIFNvIHdlIGNhbiB1cGRhdGVcbiAgICAvLyBzdHJlYW0gbW9kZXMgYWZ0ZXIgZGF0YSBwcm9jZXNzaW5nLCB3aGVyZSB0aGUgZmlsdGVyZWQgZGF0YSBpcyB1c2VkIHRvXG4gICAgLy8gZGV0ZW1pbmcgd2hldGhlciB1c2UgcHJvZ3Jlc3NpdmUgcmVuZGVyaW5nLlxuXG4gICAgdXBkYXRlU3RyZWFtTW9kZXModGhpcywgZWNNb2RlbCk7IC8vIFdlIHVwZGF0ZSBzdHJlYW0gbW9kZXMgYmVmb3JlIGNvb3JkaW5hdGUgc3lzdGVtIHVwZGF0ZWQsIHRoZW4gdGhlIG1vZGVzIGluZm9cbiAgICAvLyBjYW4gYmUgZmV0Y2hlZCB3aGVuIGNvb3JkIHN5cyB1cGRhdGluZyAoY29uc2lkZXIgdGhlIGJhckdyaWQgZXh0ZW50IGZpeCkuIEJ1dFxuICAgIC8vIHRoZSBkcmF3YmFjayBpcyB0aGUgZnVsbCBjb29yZCBpbmZvIGNhbiBub3QgYmUgZmV0Y2hlZC4gRm9ydHVuYXRlbHkgdGhpcyBmdWxsXG4gICAgLy8gY29vcmQgaXMgbm90IHJlcXVpZWQgaW4gc3RyZWFtIG1vZGUgdXBkYXRlciBjdXJyZW50bHkuXG5cbiAgICBjb29yZFN5c01nci51cGRhdGUoZWNNb2RlbCwgYXBpKTtcbiAgICBjbGVhckNvbG9yUGFsZXR0ZShlY01vZGVsKTtcbiAgICBzY2hlZHVsZXIucGVyZm9ybVZpc3VhbFRhc2tzKGVjTW9kZWwsIHBheWxvYWQpO1xuICAgIHJlbmRlcih0aGlzLCBlY01vZGVsLCBhcGksIHBheWxvYWQpOyAvLyBTZXQgYmFja2dyb3VuZFxuXG4gICAgdmFyIGJhY2tncm91bmRDb2xvciA9IGVjTW9kZWwuZ2V0KCdiYWNrZ3JvdW5kQ29sb3InKSB8fCAndHJhbnNwYXJlbnQnOyAvLyBJbiBJRThcblxuICAgIGlmICghZW52LmNhbnZhc1N1cHBvcnRlZCkge1xuICAgICAgdmFyIGNvbG9yQXJyID0gY29sb3JUb29sLnBhcnNlKGJhY2tncm91bmRDb2xvcik7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3IgPSBjb2xvclRvb2wuc3RyaW5naWZ5KGNvbG9yQXJyLCAncmdiJyk7XG5cbiAgICAgIGlmIChjb2xvckFyclszXSA9PT0gMCkge1xuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IgPSAndHJhbnNwYXJlbnQnO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB6ci5zZXRCYWNrZ3JvdW5kQ29sb3IoYmFja2dyb3VuZENvbG9yKTtcbiAgICB9XG5cbiAgICBwZXJmb3JtUG9zdFVwZGF0ZUZ1bmNzKGVjTW9kZWwsIGFwaSk7IC8vIGNvbnNvbGUucHJvZmlsZSAmJiBjb25zb2xlLnByb2ZpbGVFbmQoJ3VwZGF0ZScpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdXBkYXRlVHJhbnNmb3JtOiBmdW5jdGlvbiAocGF5bG9hZCkge1xuICAgIHZhciBlY01vZGVsID0gdGhpcy5fbW9kZWw7XG4gICAgdmFyIGVjSW5zID0gdGhpcztcbiAgICB2YXIgYXBpID0gdGhpcy5fYXBpOyAvLyB1cGRhdGUgYmVmb3JlIHNldE9wdGlvblxuXG4gICAgaWYgKCFlY01vZGVsKSB7XG4gICAgICByZXR1cm47XG4gICAgfSAvLyBDaGFydFZpZXcubWFya1VwZGF0ZU1ldGhvZChwYXlsb2FkLCAndXBkYXRlVHJhbnNmb3JtJyk7XG5cblxuICAgIHZhciBjb21wb25lbnREaXJ0eUxpc3QgPSBbXTtcbiAgICBlY01vZGVsLmVhY2hDb21wb25lbnQoZnVuY3Rpb24gKGNvbXBvbmVudFR5cGUsIGNvbXBvbmVudE1vZGVsKSB7XG4gICAgICB2YXIgY29tcG9uZW50VmlldyA9IGVjSW5zLmdldFZpZXdPZkNvbXBvbmVudE1vZGVsKGNvbXBvbmVudE1vZGVsKTtcblxuICAgICAgaWYgKGNvbXBvbmVudFZpZXcgJiYgY29tcG9uZW50Vmlldy5fX2FsaXZlKSB7XG4gICAgICAgIGlmIChjb21wb25lbnRWaWV3LnVwZGF0ZVRyYW5zZm9ybSkge1xuICAgICAgICAgIHZhciByZXN1bHQgPSBjb21wb25lbnRWaWV3LnVwZGF0ZVRyYW5zZm9ybShjb21wb25lbnRNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKTtcbiAgICAgICAgICByZXN1bHQgJiYgcmVzdWx0LnVwZGF0ZSAmJiBjb21wb25lbnREaXJ0eUxpc3QucHVzaChjb21wb25lbnRWaWV3KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb21wb25lbnREaXJ0eUxpc3QucHVzaChjb21wb25lbnRWaWV3KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICAgIHZhciBzZXJpZXNEaXJ0eU1hcCA9IHpyVXRpbC5jcmVhdGVIYXNoTWFwKCk7XG4gICAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgICAgdmFyIGNoYXJ0VmlldyA9IGVjSW5zLl9jaGFydHNNYXBbc2VyaWVzTW9kZWwuX192aWV3SWRdO1xuXG4gICAgICBpZiAoY2hhcnRWaWV3LnVwZGF0ZVRyYW5zZm9ybSkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gY2hhcnRWaWV3LnVwZGF0ZVRyYW5zZm9ybShzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKTtcbiAgICAgICAgcmVzdWx0ICYmIHJlc3VsdC51cGRhdGUgJiYgc2VyaWVzRGlydHlNYXAuc2V0KHNlcmllc01vZGVsLnVpZCwgMSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZXJpZXNEaXJ0eU1hcC5zZXQoc2VyaWVzTW9kZWwudWlkLCAxKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjbGVhckNvbG9yUGFsZXR0ZShlY01vZGVsKTsgLy8gS2VlcCBwaXBlIHRvIHRoZSBleGlzdCBwaXBlbGluZSBiZWNhdXNlIGl0IGRlcGVuZHMgb24gdGhlIHJlbmRlciB0YXNrIG9mIHRoZSBmdWxsIHBpcGVsaW5lLlxuICAgIC8vIHRoaXMuX3NjaGVkdWxlci5wZXJmb3JtVmlzdWFsVGFza3MoZWNNb2RlbCwgcGF5bG9hZCwgJ2xheW91dCcsIHRydWUpO1xuXG4gICAgdGhpcy5fc2NoZWR1bGVyLnBlcmZvcm1WaXN1YWxUYXNrcyhlY01vZGVsLCBwYXlsb2FkLCB7XG4gICAgICBzZXREaXJ0eTogdHJ1ZSxcbiAgICAgIGRpcnR5TWFwOiBzZXJpZXNEaXJ0eU1hcFxuICAgIH0pOyAvLyBDdXJyZW50bHksIG5vdCBjYWxsIHJlbmRlciBvZiBjb21wb25lbnRzLiBHZW8gcmVuZGVyIGNvc3QgYSBsb3QuXG4gICAgLy8gcmVuZGVyQ29tcG9uZW50cyhlY0lucywgZWNNb2RlbCwgYXBpLCBwYXlsb2FkLCBjb21wb25lbnREaXJ0eUxpc3QpO1xuXG5cbiAgICByZW5kZXJTZXJpZXMoZWNJbnMsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCwgc2VyaWVzRGlydHlNYXApO1xuICAgIHBlcmZvcm1Qb3N0VXBkYXRlRnVuY3MoZWNNb2RlbCwgdGhpcy5fYXBpKTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHVwZGF0ZVZpZXc6IGZ1bmN0aW9uIChwYXlsb2FkKSB7XG4gICAgdmFyIGVjTW9kZWwgPSB0aGlzLl9tb2RlbDsgLy8gdXBkYXRlIGJlZm9yZSBzZXRPcHRpb25cblxuICAgIGlmICghZWNNb2RlbCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIENoYXJ0Vmlldy5tYXJrVXBkYXRlTWV0aG9kKHBheWxvYWQsICd1cGRhdGVWaWV3Jyk7XG4gICAgY2xlYXJDb2xvclBhbGV0dGUoZWNNb2RlbCk7IC8vIEtlZXAgcGlwZSB0byB0aGUgZXhpc3QgcGlwZWxpbmUgYmVjYXVzZSBpdCBkZXBlbmRzIG9uIHRoZSByZW5kZXIgdGFzayBvZiB0aGUgZnVsbCBwaXBlbGluZS5cblxuICAgIHRoaXMuX3NjaGVkdWxlci5wZXJmb3JtVmlzdWFsVGFza3MoZWNNb2RlbCwgcGF5bG9hZCwge1xuICAgICAgc2V0RGlydHk6IHRydWVcbiAgICB9KTtcblxuICAgIHJlbmRlcih0aGlzLCB0aGlzLl9tb2RlbCwgdGhpcy5fYXBpLCBwYXlsb2FkKTtcbiAgICBwZXJmb3JtUG9zdFVwZGF0ZUZ1bmNzKGVjTW9kZWwsIHRoaXMuX2FwaSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICB1cGRhdGVWaXN1YWw6IGZ1bmN0aW9uIChwYXlsb2FkKSB7XG4gICAgdXBkYXRlTWV0aG9kcy51cGRhdGUuY2FsbCh0aGlzLCBwYXlsb2FkKTsgLy8gdmFyIGVjTW9kZWwgPSB0aGlzLl9tb2RlbDtcbiAgICAvLyAvLyB1cGRhdGUgYmVmb3JlIHNldE9wdGlvblxuICAgIC8vIGlmICghZWNNb2RlbCkge1xuICAgIC8vICAgICByZXR1cm47XG4gICAgLy8gfVxuICAgIC8vIENoYXJ0Vmlldy5tYXJrVXBkYXRlTWV0aG9kKHBheWxvYWQsICd1cGRhdGVWaXN1YWwnKTtcbiAgICAvLyBjbGVhckNvbG9yUGFsZXR0ZShlY01vZGVsKTtcbiAgICAvLyAvLyBLZWVwIHBpcGUgdG8gdGhlIGV4aXN0IHBpcGVsaW5lIGJlY2F1c2UgaXQgZGVwZW5kcyBvbiB0aGUgcmVuZGVyIHRhc2sgb2YgdGhlIGZ1bGwgcGlwZWxpbmUuXG4gICAgLy8gdGhpcy5fc2NoZWR1bGVyLnBlcmZvcm1WaXN1YWxUYXNrcyhlY01vZGVsLCBwYXlsb2FkLCB7dmlzdWFsVHlwZTogJ3Zpc3VhbCcsIHNldERpcnR5OiB0cnVlfSk7XG4gICAgLy8gcmVuZGVyKHRoaXMsIHRoaXMuX21vZGVsLCB0aGlzLl9hcGksIHBheWxvYWQpO1xuICAgIC8vIHBlcmZvcm1Qb3N0VXBkYXRlRnVuY3MoZWNNb2RlbCwgdGhpcy5fYXBpKTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHVwZGF0ZUxheW91dDogZnVuY3Rpb24gKHBheWxvYWQpIHtcbiAgICB1cGRhdGVNZXRob2RzLnVwZGF0ZS5jYWxsKHRoaXMsIHBheWxvYWQpOyAvLyB2YXIgZWNNb2RlbCA9IHRoaXMuX21vZGVsO1xuICAgIC8vIC8vIHVwZGF0ZSBiZWZvcmUgc2V0T3B0aW9uXG4gICAgLy8gaWYgKCFlY01vZGVsKSB7XG4gICAgLy8gICAgIHJldHVybjtcbiAgICAvLyB9XG4gICAgLy8gQ2hhcnRWaWV3Lm1hcmtVcGRhdGVNZXRob2QocGF5bG9hZCwgJ3VwZGF0ZUxheW91dCcpO1xuICAgIC8vIC8vIEtlZXAgcGlwZSB0byB0aGUgZXhpc3QgcGlwZWxpbmUgYmVjYXVzZSBpdCBkZXBlbmRzIG9uIHRoZSByZW5kZXIgdGFzayBvZiB0aGUgZnVsbCBwaXBlbGluZS5cbiAgICAvLyAvLyB0aGlzLl9zY2hlZHVsZXIucGVyZm9ybVZpc3VhbFRhc2tzKGVjTW9kZWwsIHBheWxvYWQsICdsYXlvdXQnLCB0cnVlKTtcbiAgICAvLyB0aGlzLl9zY2hlZHVsZXIucGVyZm9ybVZpc3VhbFRhc2tzKGVjTW9kZWwsIHBheWxvYWQsIHtzZXREaXJ0eTogdHJ1ZX0pO1xuICAgIC8vIHJlbmRlcih0aGlzLCB0aGlzLl9tb2RlbCwgdGhpcy5fYXBpLCBwYXlsb2FkKTtcbiAgICAvLyBwZXJmb3JtUG9zdFVwZGF0ZUZ1bmNzKGVjTW9kZWwsIHRoaXMuX2FwaSk7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIHByZXBhcmUoZWNJbnMpIHtcbiAgdmFyIGVjTW9kZWwgPSBlY0lucy5fbW9kZWw7XG4gIHZhciBzY2hlZHVsZXIgPSBlY0lucy5fc2NoZWR1bGVyO1xuICBzY2hlZHVsZXIucmVzdG9yZVBpcGVsaW5lcyhlY01vZGVsKTtcbiAgc2NoZWR1bGVyLnByZXBhcmVTdGFnZVRhc2tzKCk7XG4gIHByZXBhcmVWaWV3KGVjSW5zLCAnY29tcG9uZW50JywgZWNNb2RlbCwgc2NoZWR1bGVyKTtcbiAgcHJlcGFyZVZpZXcoZWNJbnMsICdjaGFydCcsIGVjTW9kZWwsIHNjaGVkdWxlcik7XG4gIHNjaGVkdWxlci5wbGFuKCk7XG59XG4vKipcbiAqIEBwcml2YXRlXG4gKi9cblxuXG5mdW5jdGlvbiB1cGRhdGVEaXJlY3RseShlY0lucywgbWV0aG9kLCBwYXlsb2FkLCBtYWluVHlwZSwgc3ViVHlwZSkge1xuICB2YXIgZWNNb2RlbCA9IGVjSW5zLl9tb2RlbDsgLy8gYnJvYWRjYXN0XG5cbiAgaWYgKCFtYWluVHlwZSkge1xuICAgIC8vIEZJWE1FXG4gICAgLy8gQ2hhcnQgd2lsbCBub3QgYmUgdXBkYXRlIGRpcmVjdGx5IGhlcmUsIGV4Y2VwdCBzZXQgZGlydHkuXG4gICAgLy8gQnV0IHRoZXJlIGlzIG5vIHN1Y2ggc2NlbmFyaW8gbm93LlxuICAgIGVhY2goZWNJbnMuX2NvbXBvbmVudHNWaWV3cy5jb25jYXQoZWNJbnMuX2NoYXJ0c1ZpZXdzKSwgY2FsbFZpZXcpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBxdWVyeSA9IHt9O1xuICBxdWVyeVttYWluVHlwZSArICdJZCddID0gcGF5bG9hZFttYWluVHlwZSArICdJZCddO1xuICBxdWVyeVttYWluVHlwZSArICdJbmRleCddID0gcGF5bG9hZFttYWluVHlwZSArICdJbmRleCddO1xuICBxdWVyeVttYWluVHlwZSArICdOYW1lJ10gPSBwYXlsb2FkW21haW5UeXBlICsgJ05hbWUnXTtcbiAgdmFyIGNvbmRpdGlvbiA9IHtcbiAgICBtYWluVHlwZTogbWFpblR5cGUsXG4gICAgcXVlcnk6IHF1ZXJ5XG4gIH07XG4gIHN1YlR5cGUgJiYgKGNvbmRpdGlvbi5zdWJUeXBlID0gc3ViVHlwZSk7IC8vIHN1YlR5cGUgbWF5IGJlICcnIGJ5IHBhcnNlQ2xhc3NUeXBlO1xuXG4gIHZhciBleGNsdWRlU2VyaWVzSWQgPSBwYXlsb2FkLmV4Y2x1ZGVTZXJpZXNJZDtcblxuICBpZiAoZXhjbHVkZVNlcmllc0lkICE9IG51bGwpIHtcbiAgICBleGNsdWRlU2VyaWVzSWQgPSB6clV0aWwuY3JlYXRlSGFzaE1hcChtb2RlbFV0aWwubm9ybWFsaXplVG9BcnJheShleGNsdWRlU2VyaWVzSWQpKTtcbiAgfSAvLyBJZiBkaXNwYXRjaEFjdGlvbiBiZWZvcmUgc2V0T3B0aW9uLCBkbyBub3RoaW5nLlxuXG5cbiAgZWNNb2RlbCAmJiBlY01vZGVsLmVhY2hDb21wb25lbnQoY29uZGl0aW9uLCBmdW5jdGlvbiAobW9kZWwpIHtcbiAgICBpZiAoIWV4Y2x1ZGVTZXJpZXNJZCB8fCBleGNsdWRlU2VyaWVzSWQuZ2V0KG1vZGVsLmlkKSA9PSBudWxsKSB7XG4gICAgICBjYWxsVmlldyhlY0luc1ttYWluVHlwZSA9PT0gJ3NlcmllcycgPyAnX2NoYXJ0c01hcCcgOiAnX2NvbXBvbmVudHNNYXAnXVttb2RlbC5fX3ZpZXdJZF0pO1xuICAgIH1cbiAgfSwgZWNJbnMpO1xuXG4gIGZ1bmN0aW9uIGNhbGxWaWV3KHZpZXcpIHtcbiAgICB2aWV3ICYmIHZpZXcuX19hbGl2ZSAmJiB2aWV3W21ldGhvZF0gJiYgdmlld1ttZXRob2RdKHZpZXcuX19tb2RlbCwgZWNNb2RlbCwgZWNJbnMuX2FwaSwgcGF5bG9hZCk7XG4gIH1cbn1cbi8qKlxuICogUmVzaXplIHRoZSBjaGFydFxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0cy53aWR0aF0gQ2FuIGJlICdhdXRvJyAodGhlIHNhbWUgYXMgbnVsbC91bmRlZmluZWQpXG4gKiBAcGFyYW0ge251bWJlcn0gW29wdHMuaGVpZ2h0XSBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdHMuc2lsZW50PWZhbHNlXVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLnJlc2l6ZSA9IGZ1bmN0aW9uIChvcHRzKSB7XG4gIHRoaXMuX3pyLnJlc2l6ZShvcHRzKTtcblxuICB2YXIgZWNNb2RlbCA9IHRoaXMuX21vZGVsOyAvLyBSZXNpemUgbG9hZGluZyBlZmZlY3RcblxuICB0aGlzLl9sb2FkaW5nRlggJiYgdGhpcy5fbG9hZGluZ0ZYLnJlc2l6ZSgpO1xuXG4gIGlmICghZWNNb2RlbCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBvcHRpb25DaGFuZ2VkID0gZWNNb2RlbC5yZXNldE9wdGlvbignbWVkaWEnKTtcbiAgdmFyIHNpbGVudCA9IG9wdHMgJiYgb3B0cy5zaWxlbnQ7XG4gIHRoaXNbSU5fTUFJTl9QUk9DRVNTXSA9IHRydWU7XG4gIG9wdGlvbkNoYW5nZWQgJiYgcHJlcGFyZSh0aGlzKTtcbiAgdXBkYXRlTWV0aG9kcy51cGRhdGUuY2FsbCh0aGlzKTtcbiAgdGhpc1tJTl9NQUlOX1BST0NFU1NdID0gZmFsc2U7XG4gIGZsdXNoUGVuZGluZ0FjdGlvbnMuY2FsbCh0aGlzLCBzaWxlbnQpO1xuICB0cmlnZ2VyVXBkYXRlZEV2ZW50LmNhbGwodGhpcywgc2lsZW50KTtcbn07XG5cbmZ1bmN0aW9uIHVwZGF0ZVN0cmVhbU1vZGVzKGVjSW5zLCBlY01vZGVsKSB7XG4gIHZhciBjaGFydHNNYXAgPSBlY0lucy5fY2hhcnRzTWFwO1xuICB2YXIgc2NoZWR1bGVyID0gZWNJbnMuX3NjaGVkdWxlcjtcbiAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIHNjaGVkdWxlci51cGRhdGVTdHJlYW1Nb2RlcyhzZXJpZXNNb2RlbCwgY2hhcnRzTWFwW3Nlcmllc01vZGVsLl9fdmlld0lkXSk7XG4gIH0pO1xufVxuLyoqXG4gKiBTaG93IGxvYWRpbmcgZWZmZWN0XG4gKiBAcGFyYW0gIHtzdHJpbmd9IFtuYW1lPSdkZWZhdWx0J11cbiAqIEBwYXJhbSAge09iamVjdH0gW2NmZ11cbiAqL1xuXG5cbmVjaGFydHNQcm90by5zaG93TG9hZGluZyA9IGZ1bmN0aW9uIChuYW1lLCBjZmcpIHtcbiAgaWYgKGlzT2JqZWN0KG5hbWUpKSB7XG4gICAgY2ZnID0gbmFtZTtcbiAgICBuYW1lID0gJyc7XG4gIH1cblxuICBuYW1lID0gbmFtZSB8fCAnZGVmYXVsdCc7XG4gIHRoaXMuaGlkZUxvYWRpbmcoKTtcblxuICBpZiAoIWxvYWRpbmdFZmZlY3RzW25hbWVdKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGVsID0gbG9hZGluZ0VmZmVjdHNbbmFtZV0odGhpcy5fYXBpLCBjZmcpO1xuICB2YXIgenIgPSB0aGlzLl96cjtcbiAgdGhpcy5fbG9hZGluZ0ZYID0gZWw7XG4gIHpyLmFkZChlbCk7XG59O1xuLyoqXG4gKiBIaWRlIGxvYWRpbmcgZWZmZWN0XG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uaGlkZUxvYWRpbmcgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMuX2xvYWRpbmdGWCAmJiB0aGlzLl96ci5yZW1vdmUodGhpcy5fbG9hZGluZ0ZYKTtcbiAgdGhpcy5fbG9hZGluZ0ZYID0gbnVsbDtcbn07XG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fSBldmVudE9ialxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5cblxuZWNoYXJ0c1Byb3RvLm1ha2VBY3Rpb25Gcm9tRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnRPYmopIHtcbiAgdmFyIHBheWxvYWQgPSB6clV0aWwuZXh0ZW5kKHt9LCBldmVudE9iaik7XG4gIHBheWxvYWQudHlwZSA9IGV2ZW50QWN0aW9uTWFwW2V2ZW50T2JqLnR5cGVdO1xuICByZXR1cm4gcGF5bG9hZDtcbn07XG4vKipcbiAqIEBwdWJpbGNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gW3BheWxvYWQudHlwZV0gQWN0aW9uIHR5cGVcbiAqIEBwYXJhbSB7T2JqZWN0fGJvb2xlYW59IFtvcHRdIElmIHBhc3MgYm9vbGVhbiwgbWVhbnMgb3B0LnNpbGVudFxuICogQHBhcmFtIHtib29sZWFufSBbb3B0LnNpbGVudD1mYWxzZV0gV2hldGhlciB0cmlnZ2VyIGV2ZW50cy5cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW29wdC5mbHVzaD11bmRlZmluZWRdXG4gKiAgICAgICAgICAgICAgICAgIHRydWU6IEZsdXNoIGltbWVkaWF0ZWx5LCBhbmQgdGhlbiBwaXhlbCBpbiBjYW52YXMgY2FuIGJlIGZldGNoZWRcbiAqICAgICAgICAgICAgICAgICAgICAgIGltbWVkaWF0ZWx5LiBDYXV0aW9uOiBpdCBtaWdodCBhZmZlY3QgcGVyZm9ybWFuY2UuXG4gKiAgICAgICAgICAgICAgICAgIGZhbHNlOiBOb3Qgbm90IGZsdXNoLlxuICogICAgICAgICAgICAgICAgICB1bmRlZmluZWQ6IEF1dG8gZGVjaWRlIHdoZXRoZXIgcGVyZm9ybSBmbHVzaC5cbiAqL1xuXG5cbmVjaGFydHNQcm90by5kaXNwYXRjaEFjdGlvbiA9IGZ1bmN0aW9uIChwYXlsb2FkLCBvcHQpIHtcbiAgaWYgKCFpc09iamVjdChvcHQpKSB7XG4gICAgb3B0ID0ge1xuICAgICAgc2lsZW50OiAhIW9wdFxuICAgIH07XG4gIH1cblxuICBpZiAoIWFjdGlvbnNbcGF5bG9hZC50eXBlXSkge1xuICAgIHJldHVybjtcbiAgfSAvLyBBdm9pZCBkaXNwYXRjaCBhY3Rpb24gYmVmb3JlIHNldE9wdGlvbi4gRXNwZWNpYWxseSBpbiBgY29ubmVjdGAuXG5cblxuICBpZiAoIXRoaXMuX21vZGVsKSB7XG4gICAgcmV0dXJuO1xuICB9IC8vIE1heSBkaXNwYXRjaEFjdGlvbiBpbiByZW5kZXJpbmcgcHJvY2VkdXJlXG5cblxuICBpZiAodGhpc1tJTl9NQUlOX1BST0NFU1NdKSB7XG4gICAgdGhpcy5fcGVuZGluZ0FjdGlvbnMucHVzaChwYXlsb2FkKTtcblxuICAgIHJldHVybjtcbiAgfVxuXG4gIGRvRGlzcGF0Y2hBY3Rpb24uY2FsbCh0aGlzLCBwYXlsb2FkLCBvcHQuc2lsZW50KTtcblxuICBpZiAob3B0LmZsdXNoKSB7XG4gICAgdGhpcy5fenIuZmx1c2godHJ1ZSk7XG4gIH0gZWxzZSBpZiAob3B0LmZsdXNoICE9PSBmYWxzZSAmJiBlbnYuYnJvd3Nlci53ZUNoYXQpIHtcbiAgICAvLyBJbiBXZUNoYXQgZW1iZWRlZCBicm93c2VyLCBgcmVxdWVzdEFuaW1hdGlvbkZyYW1lYCBhbmQgYHNldEludGVydmFsYFxuICAgIC8vIGhhbmcgd2hlbiBzbGlkaW5nIHBhZ2UgKG9uIHRvdWNoIGV2ZW50KSwgd2hpY2ggY2F1c2UgdGhhdCB6ciBkb2VzIG5vdFxuICAgIC8vIHJlZnJlc2ggdXRpbCB1c2VyIGludGVyYWN0aW9uIGZpbmlzaGVkLCB3aGljaCBpcyBub3QgZXhwZWN0ZWQuXG4gICAgLy8gQnV0IGBkaXNwYXRjaEFjdGlvbmAgbWF5IGJlIGNhbGxlZCB0b28gZnJlcXVlbnRseSB3aGVuIHBhbiBvbiB0b3VjaFxuICAgIC8vIHNjcmVlbiwgd2hpY2ggaW1wYWN0cyBwZXJmb3JtYW5jZSBpZiBkbyBub3QgdGhyb3R0bGUgdGhlbS5cbiAgICB0aGlzLl90aHJvdHRsZWRackZsdXNoKCk7XG4gIH1cblxuICBmbHVzaFBlbmRpbmdBY3Rpb25zLmNhbGwodGhpcywgb3B0LnNpbGVudCk7XG4gIHRyaWdnZXJVcGRhdGVkRXZlbnQuY2FsbCh0aGlzLCBvcHQuc2lsZW50KTtcbn07XG5cbmZ1bmN0aW9uIGRvRGlzcGF0Y2hBY3Rpb24ocGF5bG9hZCwgc2lsZW50KSB7XG4gIHZhciBwYXlsb2FkVHlwZSA9IHBheWxvYWQudHlwZTtcbiAgdmFyIGVzY2FwZUNvbm5lY3QgPSBwYXlsb2FkLmVzY2FwZUNvbm5lY3Q7XG4gIHZhciBhY3Rpb25XcmFwID0gYWN0aW9uc1twYXlsb2FkVHlwZV07XG4gIHZhciBhY3Rpb25JbmZvID0gYWN0aW9uV3JhcC5hY3Rpb25JbmZvO1xuICB2YXIgY3B0VHlwZSA9IChhY3Rpb25JbmZvLnVwZGF0ZSB8fCAndXBkYXRlJykuc3BsaXQoJzonKTtcbiAgdmFyIHVwZGF0ZU1ldGhvZCA9IGNwdFR5cGUucG9wKCk7XG4gIGNwdFR5cGUgPSBjcHRUeXBlWzBdICE9IG51bGwgJiYgcGFyc2VDbGFzc1R5cGUoY3B0VHlwZVswXSk7XG4gIHRoaXNbSU5fTUFJTl9QUk9DRVNTXSA9IHRydWU7XG4gIHZhciBwYXlsb2FkcyA9IFtwYXlsb2FkXTtcbiAgdmFyIGJhdGNoZWQgPSBmYWxzZTsgLy8gQmF0Y2ggYWN0aW9uXG5cbiAgaWYgKHBheWxvYWQuYmF0Y2gpIHtcbiAgICBiYXRjaGVkID0gdHJ1ZTtcbiAgICBwYXlsb2FkcyA9IHpyVXRpbC5tYXAocGF5bG9hZC5iYXRjaCwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgIGl0ZW0gPSB6clV0aWwuZGVmYXVsdHMoenJVdGlsLmV4dGVuZCh7fSwgaXRlbSksIHBheWxvYWQpO1xuICAgICAgaXRlbS5iYXRjaCA9IG51bGw7XG4gICAgICByZXR1cm4gaXRlbTtcbiAgICB9KTtcbiAgfVxuXG4gIHZhciBldmVudE9iakJhdGNoID0gW107XG4gIHZhciBldmVudE9iajtcbiAgdmFyIGlzSGlnaERvd24gPSBwYXlsb2FkVHlwZSA9PT0gJ2hpZ2hsaWdodCcgfHwgcGF5bG9hZFR5cGUgPT09ICdkb3ducGxheSc7XG4gIGVhY2gocGF5bG9hZHMsIGZ1bmN0aW9uIChiYXRjaEl0ZW0pIHtcbiAgICAvLyBBY3Rpb24gY2FuIHNwZWNpZnkgdGhlIGV2ZW50IGJ5IHJldHVybiBpdC5cbiAgICBldmVudE9iaiA9IGFjdGlvbldyYXAuYWN0aW9uKGJhdGNoSXRlbSwgdGhpcy5fbW9kZWwsIHRoaXMuX2FwaSk7IC8vIEVtaXQgZXZlbnQgb3V0c2lkZVxuXG4gICAgZXZlbnRPYmogPSBldmVudE9iaiB8fCB6clV0aWwuZXh0ZW5kKHt9LCBiYXRjaEl0ZW0pOyAvLyBDb252ZXJ0IHR5cGUgdG8gZXZlbnRUeXBlXG5cbiAgICBldmVudE9iai50eXBlID0gYWN0aW9uSW5mby5ldmVudCB8fCBldmVudE9iai50eXBlO1xuICAgIGV2ZW50T2JqQmF0Y2gucHVzaChldmVudE9iaik7IC8vIGxpZ2h0IHVwZGF0ZSBkb2VzIG5vdCBwZXJmb3JtIGRhdGEgcHJvY2VzcywgbGF5b3V0IGFuZCB2aXN1YWwuXG5cbiAgICBpZiAoaXNIaWdoRG93bikge1xuICAgICAgLy8gbWV0aG9kLCBwYXlsb2FkLCBtYWluVHlwZSwgc3ViVHlwZVxuICAgICAgdXBkYXRlRGlyZWN0bHkodGhpcywgdXBkYXRlTWV0aG9kLCBiYXRjaEl0ZW0sICdzZXJpZXMnKTtcbiAgICB9IGVsc2UgaWYgKGNwdFR5cGUpIHtcbiAgICAgIHVwZGF0ZURpcmVjdGx5KHRoaXMsIHVwZGF0ZU1ldGhvZCwgYmF0Y2hJdGVtLCBjcHRUeXBlLm1haW4sIGNwdFR5cGUuc3ViKTtcbiAgICB9XG4gIH0sIHRoaXMpO1xuXG4gIGlmICh1cGRhdGVNZXRob2QgIT09ICdub25lJyAmJiAhaXNIaWdoRG93biAmJiAhY3B0VHlwZSkge1xuICAgIC8vIFN0aWxsIGRpcnR5XG4gICAgaWYgKHRoaXNbT1BUSU9OX1VQREFURURdKSB7XG4gICAgICAvLyBGSVhNRSBQYXNzIHBheWxvYWQgP1xuICAgICAgcHJlcGFyZSh0aGlzKTtcbiAgICAgIHVwZGF0ZU1ldGhvZHMudXBkYXRlLmNhbGwodGhpcywgcGF5bG9hZCk7XG4gICAgICB0aGlzW09QVElPTl9VUERBVEVEXSA9IGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICB1cGRhdGVNZXRob2RzW3VwZGF0ZU1ldGhvZF0uY2FsbCh0aGlzLCBwYXlsb2FkKTtcbiAgICB9XG4gIH0gLy8gRm9sbG93IHRoZSBydWxlIG9mIGFjdGlvbiBiYXRjaFxuXG5cbiAgaWYgKGJhdGNoZWQpIHtcbiAgICBldmVudE9iaiA9IHtcbiAgICAgIHR5cGU6IGFjdGlvbkluZm8uZXZlbnQgfHwgcGF5bG9hZFR5cGUsXG4gICAgICBlc2NhcGVDb25uZWN0OiBlc2NhcGVDb25uZWN0LFxuICAgICAgYmF0Y2g6IGV2ZW50T2JqQmF0Y2hcbiAgICB9O1xuICB9IGVsc2Uge1xuICAgIGV2ZW50T2JqID0gZXZlbnRPYmpCYXRjaFswXTtcbiAgfVxuXG4gIHRoaXNbSU5fTUFJTl9QUk9DRVNTXSA9IGZhbHNlO1xuICAhc2lsZW50ICYmIHRoaXMuX21lc3NhZ2VDZW50ZXIudHJpZ2dlcihldmVudE9iai50eXBlLCBldmVudE9iaik7XG59XG5cbmZ1bmN0aW9uIGZsdXNoUGVuZGluZ0FjdGlvbnMoc2lsZW50KSB7XG4gIHZhciBwZW5kaW5nQWN0aW9ucyA9IHRoaXMuX3BlbmRpbmdBY3Rpb25zO1xuXG4gIHdoaWxlIChwZW5kaW5nQWN0aW9ucy5sZW5ndGgpIHtcbiAgICB2YXIgcGF5bG9hZCA9IHBlbmRpbmdBY3Rpb25zLnNoaWZ0KCk7XG4gICAgZG9EaXNwYXRjaEFjdGlvbi5jYWxsKHRoaXMsIHBheWxvYWQsIHNpbGVudCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gdHJpZ2dlclVwZGF0ZWRFdmVudChzaWxlbnQpIHtcbiAgIXNpbGVudCAmJiB0aGlzLnRyaWdnZXIoJ3VwZGF0ZWQnKTtcbn1cbi8qKlxuICogRXZlbnQgYHJlbmRlcmVkYCBpcyB0cmlnZ2VyZWQgd2hlbiB6clxuICogcmVuZGVyZWQuIEl0IGlzIHVzZWZ1bCBmb3IgcmVhbHRpbWVcbiAqIHNuYXBzaG90IChyZWZsZWN0IGFuaW1hdGlvbikuXG4gKlxuICogRXZlbnQgYGZpbmlzaGVkYCBpcyB0cmlnZ2VyZWQgd2hlbjpcbiAqICgxKSB6cmVuZGVyIHJlbmRlcmluZyBmaW5pc2hlZC5cbiAqICgyKSBpbml0aWFsIGFuaW1hdGlvbiBmaW5pc2hlZC5cbiAqICgzKSBwcm9ncmVzc2l2ZSByZW5kZXJpbmcgZmluaXNoZWQuXG4gKiAoNCkgbm8gcGVuZGluZyBhY3Rpb24uXG4gKiAoNSkgbm8gZGVsYXllZCBzZXRPcHRpb24gbmVlZHMgdG8gYmUgcHJvY2Vzc2VkLlxuICovXG5cblxuZnVuY3Rpb24gYmluZFJlbmRlcmVkRXZlbnQoenIsIGVjSW5zKSB7XG4gIHpyLm9uKCdyZW5kZXJlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICBlY0lucy50cmlnZ2VyKCdyZW5kZXJlZCcpOyAvLyBUaGUgYGZpbmlzaGVkYCBldmVudCBzaG91bGQgbm90IGJlIHRyaWdnZXJlZCByZXBlYXRseSxcbiAgICAvLyBzbyBpdCBzaG91bGQgb25seSBiZSB0cmlnZ2VyZWQgd2hlbiByZW5kZXJpbmcgaW5kZWVkIGhhcHBlbmRcbiAgICAvLyBpbiB6cmVuZGVyLiAoQ29uc2lkZXIgdGhlIGNhc2UgdGhhdCBkaXBhdGNoQWN0aW9uIGlzIGtlZXBcbiAgICAvLyB0cmlnZ2VyaW5nIHdoZW4gbW91c2UgbW92ZSkuXG5cbiAgICBpZiAoIC8vIEFsdGhvdWdoIHpyIGlzIGRpcnR5IGlmIGluaXRpYWwgYW5pbWF0aW9uIGlzIG5vdCBmaW5pc2hlZFxuICAgIC8vIGFuZCB0aGlzIGNoZWNraW5nIGlzIGNhbGxlZCBvbiBmcmFtZSwgd2UgYWxzbyBjaGVja1xuICAgIC8vIGFuaW1hdGlvbiBmaW5pc2hlZCBmb3Igcm9idXN0bmVzcy5cbiAgICB6ci5hbmltYXRpb24uaXNGaW5pc2hlZCgpICYmICFlY0luc1tPUFRJT05fVVBEQVRFRF0gJiYgIWVjSW5zLl9zY2hlZHVsZXIudW5maW5pc2hlZCAmJiAhZWNJbnMuX3BlbmRpbmdBY3Rpb25zLmxlbmd0aCkge1xuICAgICAgZWNJbnMudHJpZ2dlcignZmluaXNoZWQnKTtcbiAgICB9XG4gIH0pO1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gcGFyYW1zXG4gKiBAcGFyYW0ge251bWJlcn0gcGFyYW1zLnNlcmllc0luZGV4XG4gKiBAcGFyYW0ge0FycmF5fFR5cGVkQXJyYXl9IHBhcmFtcy5kYXRhXG4gKi9cblxuXG5lY2hhcnRzUHJvdG8uYXBwZW5kRGF0YSA9IGZ1bmN0aW9uIChwYXJhbXMpIHtcbiAgdmFyIHNlcmllc0luZGV4ID0gcGFyYW1zLnNlcmllc0luZGV4O1xuICB2YXIgZWNNb2RlbCA9IHRoaXMuZ2V0TW9kZWwoKTtcbiAgdmFyIHNlcmllc01vZGVsID0gZWNNb2RlbC5nZXRTZXJpZXNCeUluZGV4KHNlcmllc0luZGV4KTtcbiAgc2VyaWVzTW9kZWwuYXBwZW5kRGF0YShwYXJhbXMpOyAvLyBOb3RlOiBgYXBwZW5kRGF0YWAgZG9lcyBub3Qgc3VwcG9ydCB0aGF0IHVwZGF0ZSBleHRlbnQgb2YgY29vcmRpbmF0ZVxuICAvLyBzeXN0ZW0sIHV0aWwgc29tZSBzY2VuYXJpbyByZXF1aXJlIHRoYXQuIEluIHRoZSBleHBlY3RlZCB1c2FnZSBvZlxuICAvLyBgYXBwZW5kRGF0YWAsIHRoZSBpbml0aWFsIGV4dGVudCBvZiBjb29yZGluYXRlIHN5c3RlbSBzaG91bGQgYmV0dGVyXG4gIC8vIGJlIGZpeGVkIGJ5IGF4aXMgYG1pbmAvYG1heGAgc2V0dGluZyBvciBpbml0aWFsIGRhdGEsIG90aGVyd2lzZSBpZlxuICAvLyB0aGUgZXh0ZW50IGNoYW5nZWQgd2hpbGUgYGFwcGVuZERhdGFgLCB0aGUgbG9jYXRpb24gb2YgdGhlIHBhaW50ZWRcbiAgLy8gZ3JhcGhpYyBlbGVtZW50cyBoYXZlIHRvIGJlIGNoYW5nZWQsIHdoaWNoIG1ha2UgdGhlIHVzYWdlIG9mXG4gIC8vIGBhcHBlbmREYXRhYCBtZWFuaW5nbGVzcy5cblxuICB0aGlzLl9zY2hlZHVsZXIudW5maW5pc2hlZCA9IHRydWU7XG59O1xuLyoqXG4gKiBSZWdpc3RlciBldmVudFxuICogQG1ldGhvZFxuICovXG5cblxuZWNoYXJ0c1Byb3RvLm9uID0gY3JlYXRlUmVnaXN0ZXJFdmVudFdpdGhMb3dlcmNhc2VOYW1lKCdvbicpO1xuZWNoYXJ0c1Byb3RvLm9mZiA9IGNyZWF0ZVJlZ2lzdGVyRXZlbnRXaXRoTG93ZXJjYXNlTmFtZSgnb2ZmJyk7XG5lY2hhcnRzUHJvdG8ub25lID0gY3JlYXRlUmVnaXN0ZXJFdmVudFdpdGhMb3dlcmNhc2VOYW1lKCdvbmUnKTtcbi8qKlxuICogUHJlcGFyZSB2aWV3IGluc3RhbmNlcyBvZiBjaGFydHMgYW5kIGNvbXBvbmVudHNcbiAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICogQHByaXZhdGVcbiAqL1xuXG5mdW5jdGlvbiBwcmVwYXJlVmlldyhlY0lucywgdHlwZSwgZWNNb2RlbCwgc2NoZWR1bGVyKSB7XG4gIHZhciBpc0NvbXBvbmVudCA9IHR5cGUgPT09ICdjb21wb25lbnQnO1xuICB2YXIgdmlld0xpc3QgPSBpc0NvbXBvbmVudCA/IGVjSW5zLl9jb21wb25lbnRzVmlld3MgOiBlY0lucy5fY2hhcnRzVmlld3M7XG4gIHZhciB2aWV3TWFwID0gaXNDb21wb25lbnQgPyBlY0lucy5fY29tcG9uZW50c01hcCA6IGVjSW5zLl9jaGFydHNNYXA7XG4gIHZhciB6ciA9IGVjSW5zLl96cjtcbiAgdmFyIGFwaSA9IGVjSW5zLl9hcGk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB2aWV3TGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZpZXdMaXN0W2ldLl9fYWxpdmUgPSBmYWxzZTtcbiAgfVxuXG4gIGlzQ29tcG9uZW50ID8gZWNNb2RlbC5lYWNoQ29tcG9uZW50KGZ1bmN0aW9uIChjb21wb25lbnRUeXBlLCBtb2RlbCkge1xuICAgIGNvbXBvbmVudFR5cGUgIT09ICdzZXJpZXMnICYmIGRvUHJlcGFyZShtb2RlbCk7XG4gIH0pIDogZWNNb2RlbC5lYWNoU2VyaWVzKGRvUHJlcGFyZSk7XG5cbiAgZnVuY3Rpb24gZG9QcmVwYXJlKG1vZGVsKSB7XG4gICAgLy8gQ29uc2lkZXI6IGlkIHNhbWUgYW5kIHR5cGUgY2hhbmdlZC5cbiAgICB2YXIgdmlld0lkID0gJ19lY18nICsgbW9kZWwuaWQgKyAnXycgKyBtb2RlbC50eXBlO1xuICAgIHZhciB2aWV3ID0gdmlld01hcFt2aWV3SWRdO1xuXG4gICAgaWYgKCF2aWV3KSB7XG4gICAgICB2YXIgY2xhc3NUeXBlID0gcGFyc2VDbGFzc1R5cGUobW9kZWwudHlwZSk7XG4gICAgICB2YXIgQ2xhenogPSBpc0NvbXBvbmVudCA/IENvbXBvbmVudFZpZXcuZ2V0Q2xhc3MoY2xhc3NUeXBlLm1haW4sIGNsYXNzVHlwZS5zdWIpIDogQ2hhcnRWaWV3LmdldENsYXNzKGNsYXNzVHlwZS5zdWIpO1xuICAgICAgdmlldyA9IG5ldyBDbGF6eigpO1xuICAgICAgdmlldy5pbml0KGVjTW9kZWwsIGFwaSk7XG4gICAgICB2aWV3TWFwW3ZpZXdJZF0gPSB2aWV3O1xuICAgICAgdmlld0xpc3QucHVzaCh2aWV3KTtcbiAgICAgIHpyLmFkZCh2aWV3Lmdyb3VwKTtcbiAgICB9XG5cbiAgICBtb2RlbC5fX3ZpZXdJZCA9IHZpZXcuX19pZCA9IHZpZXdJZDtcbiAgICB2aWV3Ll9fYWxpdmUgPSB0cnVlO1xuICAgIHZpZXcuX19tb2RlbCA9IG1vZGVsO1xuICAgIHZpZXcuZ3JvdXAuX19lY0NvbXBvbmVudEluZm8gPSB7XG4gICAgICBtYWluVHlwZTogbW9kZWwubWFpblR5cGUsXG4gICAgICBpbmRleDogbW9kZWwuY29tcG9uZW50SW5kZXhcbiAgICB9O1xuICAgICFpc0NvbXBvbmVudCAmJiBzY2hlZHVsZXIucHJlcGFyZVZpZXcodmlldywgbW9kZWwsIGVjTW9kZWwsIGFwaSk7XG4gIH1cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHZpZXdMaXN0Lmxlbmd0aDspIHtcbiAgICB2YXIgdmlldyA9IHZpZXdMaXN0W2ldO1xuXG4gICAgaWYgKCF2aWV3Ll9fYWxpdmUpIHtcbiAgICAgICFpc0NvbXBvbmVudCAmJiB2aWV3LnJlbmRlclRhc2suZGlzcG9zZSgpO1xuICAgICAgenIucmVtb3ZlKHZpZXcuZ3JvdXApO1xuICAgICAgdmlldy5kaXNwb3NlKGVjTW9kZWwsIGFwaSk7XG4gICAgICB2aWV3TGlzdC5zcGxpY2UoaSwgMSk7XG4gICAgICBkZWxldGUgdmlld01hcFt2aWV3Ll9faWRdO1xuICAgICAgdmlldy5fX2lkID0gdmlldy5ncm91cC5fX2VjQ29tcG9uZW50SW5mbyA9IG51bGw7XG4gICAgfSBlbHNlIHtcbiAgICAgIGkrKztcbiAgICB9XG4gIH1cbn0gLy8gLyoqXG4vLyAgKiBFbmNvZGUgdmlzdWFsIGluZm9tYXRpb24gZnJvbSBkYXRhIGFmdGVyIGRhdGEgcHJvY2Vzc2luZ1xuLy8gICpcbi8vICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfSBlY01vZGVsXG4vLyAgKiBAcGFyYW0ge29iamVjdH0gbGF5b3V0XG4vLyAgKiBAcGFyYW0ge2Jvb2xlYW59IFtsYXlvdXRGaWx0ZXJdIGB0cnVlYDogb25seSBsYXlvdXQsXG4vLyAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBmYWxzZWA6IG9ubHkgbm90IGxheW91dCxcbi8vICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYG51bGxgL2B1bmRlZmluZWRgOiBhbGwuXG4vLyAgKiBAcGFyYW0ge3N0cmluZ30gdGFza0Jhc2VUYWdcbi8vICAqIEBwcml2YXRlXG4vLyAgKi9cbi8vIGZ1bmN0aW9uIHN0YXJ0VmlzdWFsRW5jb2RpbmcoZWNJbnMsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCwgbGF5b3V0RmlsdGVyKSB7XG4vLyAgICAgZWFjaCh2aXN1YWxGdW5jcywgZnVuY3Rpb24gKHZpc3VhbCwgaW5kZXgpIHtcbi8vICAgICAgICAgdmFyIGlzTGF5b3V0ID0gdmlzdWFsLmlzTGF5b3V0O1xuLy8gICAgICAgICBpZiAobGF5b3V0RmlsdGVyID09IG51bGxcbi8vICAgICAgICAgICAgIHx8IChsYXlvdXRGaWx0ZXIgPT09IGZhbHNlICYmICFpc0xheW91dClcbi8vICAgICAgICAgICAgIHx8IChsYXlvdXRGaWx0ZXIgPT09IHRydWUgJiYgaXNMYXlvdXQpXG4vLyAgICAgICAgICkge1xuLy8gICAgICAgICAgICAgdmlzdWFsLmZ1bmMoZWNNb2RlbCwgYXBpLCBwYXlsb2FkKTtcbi8vICAgICAgICAgfVxuLy8gICAgIH0pO1xuLy8gfVxuXG5cbmZ1bmN0aW9uIGNsZWFyQ29sb3JQYWxldHRlKGVjTW9kZWwpIHtcbiAgZWNNb2RlbC5jbGVhckNvbG9yUGFsZXR0ZSgpO1xuICBlY01vZGVsLmVhY2hTZXJpZXMoZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgc2VyaWVzTW9kZWwuY2xlYXJDb2xvclBhbGV0dGUoKTtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHJlbmRlcihlY0lucywgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7XG4gIHJlbmRlckNvbXBvbmVudHMoZWNJbnMsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCk7XG4gIGVhY2goZWNJbnMuX2NoYXJ0c1ZpZXdzLCBmdW5jdGlvbiAoY2hhcnQpIHtcbiAgICBjaGFydC5fX2FsaXZlID0gZmFsc2U7XG4gIH0pO1xuICByZW5kZXJTZXJpZXMoZWNJbnMsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCk7IC8vIFJlbW92ZSBncm91cHMgb2YgdW5yZW5kZXJlZCBjaGFydHNcblxuICBlYWNoKGVjSW5zLl9jaGFydHNWaWV3cywgZnVuY3Rpb24gKGNoYXJ0KSB7XG4gICAgaWYgKCFjaGFydC5fX2FsaXZlKSB7XG4gICAgICBjaGFydC5yZW1vdmUoZWNNb2RlbCwgYXBpKTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiByZW5kZXJDb21wb25lbnRzKGVjSW5zLCBlY01vZGVsLCBhcGksIHBheWxvYWQsIGRpcnR5TGlzdCkge1xuICBlYWNoKGRpcnR5TGlzdCB8fCBlY0lucy5fY29tcG9uZW50c1ZpZXdzLCBmdW5jdGlvbiAoY29tcG9uZW50Vmlldykge1xuICAgIHZhciBjb21wb25lbnRNb2RlbCA9IGNvbXBvbmVudFZpZXcuX19tb2RlbDtcbiAgICBjb21wb25lbnRWaWV3LnJlbmRlcihjb21wb25lbnRNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKTtcbiAgICB1cGRhdGVaKGNvbXBvbmVudE1vZGVsLCBjb21wb25lbnRWaWV3KTtcbiAgfSk7XG59XG4vKipcbiAqIFJlbmRlciBlYWNoIGNoYXJ0IGFuZCBjb21wb25lbnRcbiAqIEBwcml2YXRlXG4gKi9cblxuXG5mdW5jdGlvbiByZW5kZXJTZXJpZXMoZWNJbnMsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCwgZGlydHlNYXApIHtcbiAgLy8gUmVuZGVyIGFsbCBjaGFydHNcbiAgdmFyIHNjaGVkdWxlciA9IGVjSW5zLl9zY2hlZHVsZXI7XG4gIHZhciB1bmZpbmlzaGVkO1xuICBlY01vZGVsLmVhY2hTZXJpZXMoZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgdmFyIGNoYXJ0VmlldyA9IGVjSW5zLl9jaGFydHNNYXBbc2VyaWVzTW9kZWwuX192aWV3SWRdO1xuICAgIGNoYXJ0Vmlldy5fX2FsaXZlID0gdHJ1ZTtcbiAgICB2YXIgcmVuZGVyVGFzayA9IGNoYXJ0Vmlldy5yZW5kZXJUYXNrO1xuICAgIHNjaGVkdWxlci51cGRhdGVQYXlsb2FkKHJlbmRlclRhc2ssIHBheWxvYWQpO1xuXG4gICAgaWYgKGRpcnR5TWFwICYmIGRpcnR5TWFwLmdldChzZXJpZXNNb2RlbC51aWQpKSB7XG4gICAgICByZW5kZXJUYXNrLmRpcnR5KCk7XG4gICAgfVxuXG4gICAgdW5maW5pc2hlZCB8PSByZW5kZXJUYXNrLnBlcmZvcm0oc2NoZWR1bGVyLmdldFBlcmZvcm1BcmdzKHJlbmRlclRhc2spKTtcbiAgICBjaGFydFZpZXcuZ3JvdXAuc2lsZW50ID0gISFzZXJpZXNNb2RlbC5nZXQoJ3NpbGVudCcpO1xuICAgIHVwZGF0ZVooc2VyaWVzTW9kZWwsIGNoYXJ0Vmlldyk7XG4gICAgdXBkYXRlQmxlbmQoc2VyaWVzTW9kZWwsIGNoYXJ0Vmlldyk7XG4gIH0pO1xuICBzY2hlZHVsZXIudW5maW5pc2hlZCB8PSB1bmZpbmlzaGVkOyAvLyBJZiB1c2UgaG92ZXIgbGF5ZXJcblxuICB1cGRhdGVIb3ZlckxheWVyU3RhdHVzKGVjSW5zLl96ciwgZWNNb2RlbCk7IC8vIEFkZCBhcmlhXG5cbiAgYXJpYShlY0lucy5fenIuZG9tLCBlY01vZGVsKTtcbn1cblxuZnVuY3Rpb24gcGVyZm9ybVBvc3RVcGRhdGVGdW5jcyhlY01vZGVsLCBhcGkpIHtcbiAgZWFjaChwb3N0VXBkYXRlRnVuY3MsIGZ1bmN0aW9uIChmdW5jKSB7XG4gICAgZnVuYyhlY01vZGVsLCBhcGkpO1xuICB9KTtcbn1cblxudmFyIE1PVVNFX0VWRU5UX05BTUVTID0gWydjbGljaycsICdkYmxjbGljaycsICdtb3VzZW92ZXInLCAnbW91c2VvdXQnLCAnbW91c2Vtb3ZlJywgJ21vdXNlZG93bicsICdtb3VzZXVwJywgJ2dsb2JhbG91dCcsICdjb250ZXh0bWVudSddO1xuLyoqXG4gKiBAcHJpdmF0ZVxuICovXG5cbmVjaGFydHNQcm90by5faW5pdEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcbiAgZWFjaChNT1VTRV9FVkVOVF9OQU1FUywgZnVuY3Rpb24gKGV2ZU5hbWUpIHtcbiAgICB2YXIgaGFuZGxlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgZWNNb2RlbCA9IHRoaXMuZ2V0TW9kZWwoKTtcbiAgICAgIHZhciBlbCA9IGUudGFyZ2V0O1xuICAgICAgdmFyIHBhcmFtcztcbiAgICAgIHZhciBpc0dsb2JhbE91dCA9IGV2ZU5hbWUgPT09ICdnbG9iYWxvdXQnOyAvLyBubyBlLnRhcmdldCB3aGVuICdnbG9iYWxvdXQnLlxuXG4gICAgICBpZiAoaXNHbG9iYWxPdXQpIHtcbiAgICAgICAgcGFyYW1zID0ge307XG4gICAgICB9IGVsc2UgaWYgKGVsICYmIGVsLmRhdGFJbmRleCAhPSBudWxsKSB7XG4gICAgICAgIHZhciBkYXRhTW9kZWwgPSBlbC5kYXRhTW9kZWwgfHwgZWNNb2RlbC5nZXRTZXJpZXNCeUluZGV4KGVsLnNlcmllc0luZGV4KTtcbiAgICAgICAgcGFyYW1zID0gZGF0YU1vZGVsICYmIGRhdGFNb2RlbC5nZXREYXRhUGFyYW1zKGVsLmRhdGFJbmRleCwgZWwuZGF0YVR5cGUsIGVsKSB8fCB7fTtcbiAgICAgIH0gLy8gSWYgZWxlbWVudCBoYXMgY3VzdG9tIGV2ZW50RGF0YSBvZiBjb21wb25lbnRzXG4gICAgICBlbHNlIGlmIChlbCAmJiBlbC5ldmVudERhdGEpIHtcbiAgICAgICAgICBwYXJhbXMgPSB6clV0aWwuZXh0ZW5kKHt9LCBlbC5ldmVudERhdGEpO1xuICAgICAgICB9IC8vIENvbnRyYWN0OiBpZiBwYXJhbXMgcHJlcGFyZWQgaW4gbW91c2UgZXZlbnQsXG4gICAgICAvLyB0aGVzZSBwcm9wZXJ0aWVzIG11c3QgYmUgc3BlY2lmaWVkOlxuICAgICAgLy8ge1xuICAgICAgLy8gICAgY29tcG9uZW50VHlwZTogc3RyaW5nIChjb21wb25lbnQgbWFpbiB0eXBlKVxuICAgICAgLy8gICAgY29tcG9uZW50SW5kZXg6IG51bWJlclxuICAgICAgLy8gfVxuICAgICAgLy8gT3RoZXJ3aXNlIGV2ZW50IHF1ZXJ5IGNhbiBub3Qgd29yay5cblxuXG4gICAgICBpZiAocGFyYW1zKSB7XG4gICAgICAgIHZhciBjb21wb25lbnRUeXBlID0gcGFyYW1zLmNvbXBvbmVudFR5cGU7XG4gICAgICAgIHZhciBjb21wb25lbnRJbmRleCA9IHBhcmFtcy5jb21wb25lbnRJbmRleDsgLy8gU3BlY2lhbCBoYW5kbGluZyBmb3IgaGlzdG9yaWMgcmVhc29uOiB3aGVuIHRyaWdnZXIgYnlcbiAgICAgICAgLy8gbWFya0xpbmUvbWFya1BvaW50L21hcmtBcmVhLCB0aGUgY29tcG9uZW50VHlwZSBpc1xuICAgICAgICAvLyAnbWFya0xpbmUnLydtYXJrUG9pbnQnLydtYXJrQXJlYScsIGJ1dCB3ZSBzaG91bGQgYmV0dGVyXG4gICAgICAgIC8vIGVuYWJsZSB0aGVtIHRvIGJlIHF1ZXJpZWQgYnkgc2VyaWVzSW5kZXgsIHNpbmNlIHRoZWlyXG4gICAgICAgIC8vIG9wdGlvbiBpcyBzZXQgaW4gZWFjaCBzZXJpZXMuXG5cbiAgICAgICAgaWYgKGNvbXBvbmVudFR5cGUgPT09ICdtYXJrTGluZScgfHwgY29tcG9uZW50VHlwZSA9PT0gJ21hcmtQb2ludCcgfHwgY29tcG9uZW50VHlwZSA9PT0gJ21hcmtBcmVhJykge1xuICAgICAgICAgIGNvbXBvbmVudFR5cGUgPSAnc2VyaWVzJztcbiAgICAgICAgICBjb21wb25lbnRJbmRleCA9IHBhcmFtcy5zZXJpZXNJbmRleDtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBtb2RlbCA9IGNvbXBvbmVudFR5cGUgJiYgY29tcG9uZW50SW5kZXggIT0gbnVsbCAmJiBlY01vZGVsLmdldENvbXBvbmVudChjb21wb25lbnRUeXBlLCBjb21wb25lbnRJbmRleCk7XG4gICAgICAgIHZhciB2aWV3ID0gbW9kZWwgJiYgdGhpc1ttb2RlbC5tYWluVHlwZSA9PT0gJ3NlcmllcycgPyAnX2NoYXJ0c01hcCcgOiAnX2NvbXBvbmVudHNNYXAnXVttb2RlbC5fX3ZpZXdJZF07XG4gICAgICAgIHBhcmFtcy5ldmVudCA9IGU7XG4gICAgICAgIHBhcmFtcy50eXBlID0gZXZlTmFtZTtcbiAgICAgICAgdGhpcy5fZWNFdmVudFByb2Nlc3Nvci5ldmVudEluZm8gPSB7XG4gICAgICAgICAgdGFyZ2V0RWw6IGVsLFxuICAgICAgICAgIHBhY2tlZEV2ZW50OiBwYXJhbXMsXG4gICAgICAgICAgbW9kZWw6IG1vZGVsLFxuICAgICAgICAgIHZpZXc6IHZpZXdcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy50cmlnZ2VyKGV2ZU5hbWUsIHBhcmFtcyk7XG4gICAgICB9XG4gICAgfTsgLy8gQ29uc2lkZXIgdGhhdCBzb21lIGNvbXBvbmVudCAobGlrZSB0b29sdGlwLCBicnVzaCwgLi4uKVxuICAgIC8vIHJlZ2lzdGVyIHpyIGV2ZW50IGhhbmRsZXIsIGJ1dCB1c2VyIGV2ZW50IGhhbmRsZXIgbWlnaHRcbiAgICAvLyBkbyBhbnl0aGluZywgc3VjaCBhcyBjYWxsIGBzZXRPcHRpb25gIG9yIGBkaXNwYXRjaEFjdGlvbmAsXG4gICAgLy8gd2hpY2ggcHJvYmFibHkgdXBkYXRlIGFueSBvZiB0aGUgY29udGVudCBhbmQgcHJvYmFibHlcbiAgICAvLyBjYXVzZSBwcm9ibGVtIGlmIGl0IGlzIGNhbGxlZCBwcmV2aW91cyBvdGhlciBpbm5lciBoYW5kbGVycy5cblxuXG4gICAgaGFuZGxlci56ckV2ZW50ZnVsQ2FsbEF0TGFzdCA9IHRydWU7XG5cbiAgICB0aGlzLl96ci5vbihldmVOYW1lLCBoYW5kbGVyLCB0aGlzKTtcbiAgfSwgdGhpcyk7XG4gIGVhY2goZXZlbnRBY3Rpb25NYXAsIGZ1bmN0aW9uIChhY3Rpb25UeXBlLCBldmVudFR5cGUpIHtcbiAgICB0aGlzLl9tZXNzYWdlQ2VudGVyLm9uKGV2ZW50VHlwZSwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB0aGlzLnRyaWdnZXIoZXZlbnRUeXBlLCBldmVudCk7XG4gICAgfSwgdGhpcyk7XG4gIH0sIHRoaXMpO1xufTtcbi8qKlxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuXG5cbmVjaGFydHNQcm90by5pc0Rpc3Bvc2VkID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5fZGlzcG9zZWQ7XG59O1xuLyoqXG4gKiBDbGVhclxuICovXG5cblxuZWNoYXJ0c1Byb3RvLmNsZWFyID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLnNldE9wdGlvbih7XG4gICAgc2VyaWVzOiBbXVxuICB9LCB0cnVlKTtcbn07XG4vKipcbiAqIERpc3Bvc2UgaW5zdGFuY2VcbiAqL1xuXG5cbmVjaGFydHNQcm90by5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICBpZiAodGhpcy5fZGlzcG9zZWQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB0aGlzLl9kaXNwb3NlZCA9IHRydWU7XG4gIG1vZGVsVXRpbC5zZXRBdHRyaWJ1dGUodGhpcy5nZXREb20oKSwgRE9NX0FUVFJJQlVURV9LRVksICcnKTtcbiAgdmFyIGFwaSA9IHRoaXMuX2FwaTtcbiAgdmFyIGVjTW9kZWwgPSB0aGlzLl9tb2RlbDtcbiAgZWFjaCh0aGlzLl9jb21wb25lbnRzVmlld3MsIGZ1bmN0aW9uIChjb21wb25lbnQpIHtcbiAgICBjb21wb25lbnQuZGlzcG9zZShlY01vZGVsLCBhcGkpO1xuICB9KTtcbiAgZWFjaCh0aGlzLl9jaGFydHNWaWV3cywgZnVuY3Rpb24gKGNoYXJ0KSB7XG4gICAgY2hhcnQuZGlzcG9zZShlY01vZGVsLCBhcGkpO1xuICB9KTsgLy8gRGlzcG9zZSBhZnRlciBhbGwgdmlld3MgZGlzcG9zZWRcblxuICB0aGlzLl96ci5kaXNwb3NlKCk7XG5cbiAgZGVsZXRlIGluc3RhbmNlc1t0aGlzLmlkXTtcbn07XG5cbnpyVXRpbC5taXhpbihFQ2hhcnRzLCBFdmVudGZ1bCk7XG5cbmZ1bmN0aW9uIHVwZGF0ZUhvdmVyTGF5ZXJTdGF0dXMoenIsIGVjTW9kZWwpIHtcbiAgdmFyIHN0b3JhZ2UgPSB6ci5zdG9yYWdlO1xuICB2YXIgZWxDb3VudCA9IDA7XG4gIHN0b3JhZ2UudHJhdmVyc2UoZnVuY3Rpb24gKGVsKSB7XG4gICAgaWYgKCFlbC5pc0dyb3VwKSB7XG4gICAgICBlbENvdW50Kys7XG4gICAgfVxuICB9KTtcblxuICBpZiAoZWxDb3VudCA+IGVjTW9kZWwuZ2V0KCdob3ZlckxheWVyVGhyZXNob2xkJykgJiYgIWVudi5ub2RlKSB7XG4gICAgc3RvcmFnZS50cmF2ZXJzZShmdW5jdGlvbiAoZWwpIHtcbiAgICAgIGlmICghZWwuaXNHcm91cCkge1xuICAgICAgICAvLyBEb24ndCBzd2l0Y2ggYmFjay5cbiAgICAgICAgZWwudXNlSG92ZXJMYXllciA9IHRydWU7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbn1cbi8qKlxuICogVXBkYXRlIGNoYXJ0IHByb2dyZXNzaXZlIGFuZCBibGVuZC5cbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfG1vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudH0gbW9kZWxcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvdmlldy9Db21wb25lbnR8bW9kdWxlOmVjaGFydHMvdmlldy9DaGFydH0gdmlld1xuICovXG5cblxuZnVuY3Rpb24gdXBkYXRlQmxlbmQoc2VyaWVzTW9kZWwsIGNoYXJ0Vmlldykge1xuICB2YXIgYmxlbmRNb2RlID0gc2VyaWVzTW9kZWwuZ2V0KCdibGVuZE1vZGUnKSB8fCBudWxsO1xuICBjaGFydFZpZXcuZ3JvdXAudHJhdmVyc2UoZnVuY3Rpb24gKGVsKSB7XG4gICAgLy8gRklYTUUgbWFya2VyIGFuZCBvdGhlciBjb21wb25lbnRzXG4gICAgaWYgKCFlbC5pc0dyb3VwKSB7XG4gICAgICAvLyBPbmx5IHNldCBpZiBibGVuZE1vZGUgaXMgY2hhbmdlZC4gSW4gY2FzZSBlbGVtZW50IGlzIGluY3JlbWVudGFsIGFuZCBkb24ndCB3YW4ndCB0byByZXJlbmRlci5cbiAgICAgIGlmIChlbC5zdHlsZS5ibGVuZCAhPT0gYmxlbmRNb2RlKSB7XG4gICAgICAgIGVsLnNldFN0eWxlKCdibGVuZCcsIGJsZW5kTW9kZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGVsLmVhY2hQZW5kaW5nRGlzcGxheWFibGUpIHtcbiAgICAgIGVsLmVhY2hQZW5kaW5nRGlzcGxheWFibGUoZnVuY3Rpb24gKGRpc3BsYXlhYmxlKSB7XG4gICAgICAgIGRpc3BsYXlhYmxlLnNldFN0eWxlKCdibGVuZCcsIGJsZW5kTW9kZSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL1Nlcmllc3xtb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Db21wb25lbnR9IG1vZGVsXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL3ZpZXcvQ29tcG9uZW50fG1vZHVsZTplY2hhcnRzL3ZpZXcvQ2hhcnR9IHZpZXdcbiAqL1xuXG5cbmZ1bmN0aW9uIHVwZGF0ZVoobW9kZWwsIHZpZXcpIHtcbiAgdmFyIHogPSBtb2RlbC5nZXQoJ3onKTtcbiAgdmFyIHpsZXZlbCA9IG1vZGVsLmdldCgnemxldmVsJyk7IC8vIFNldCB6IGFuZCB6bGV2ZWxcblxuICB2aWV3Lmdyb3VwLnRyYXZlcnNlKGZ1bmN0aW9uIChlbCkge1xuICAgIGlmIChlbC50eXBlICE9PSAnZ3JvdXAnKSB7XG4gICAgICB6ICE9IG51bGwgJiYgKGVsLnogPSB6KTtcbiAgICAgIHpsZXZlbCAhPSBudWxsICYmIChlbC56bGV2ZWwgPSB6bGV2ZWwpO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUV4dGVuc2lvbkFQSShlY0luc3RhbmNlKSB7XG4gIHZhciBjb29yZFN5c01nciA9IGVjSW5zdGFuY2UuX2Nvb3JkU3lzTWdyO1xuICByZXR1cm4genJVdGlsLmV4dGVuZChuZXcgRXh0ZW5zaW9uQVBJKGVjSW5zdGFuY2UpLCB7XG4gICAgLy8gSW5qZWN0IG1ldGhvZHNcbiAgICBnZXRDb29yZGluYXRlU3lzdGVtczogenJVdGlsLmJpbmQoY29vcmRTeXNNZ3IuZ2V0Q29vcmRpbmF0ZVN5c3RlbXMsIGNvb3JkU3lzTWdyKSxcbiAgICBnZXRDb21wb25lbnRCeUVsZW1lbnQ6IGZ1bmN0aW9uIChlbCkge1xuICAgICAgd2hpbGUgKGVsKSB7XG4gICAgICAgIHZhciBtb2RlbEluZm8gPSBlbC5fX2VjQ29tcG9uZW50SW5mbztcblxuICAgICAgICBpZiAobW9kZWxJbmZvICE9IG51bGwpIHtcbiAgICAgICAgICByZXR1cm4gZWNJbnN0YW5jZS5fbW9kZWwuZ2V0Q29tcG9uZW50KG1vZGVsSW5mby5tYWluVHlwZSwgbW9kZWxJbmZvLmluZGV4KTtcbiAgICAgICAgfVxuXG4gICAgICAgIGVsID0gZWwucGFyZW50O1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG59XG4vKipcbiAqIEBjbGFzc1xuICogVXNhZ2Ugb2YgcXVlcnk6XG4gKiBgY2hhcnQub24oJ2NsaWNrJywgcXVlcnksIGhhbmRsZXIpO2BcbiAqIFRoZSBgcXVlcnlgIGNhbiBiZTpcbiAqICsgVGhlIGNvbXBvbmVudCB0eXBlIHF1ZXJ5IHN0cmluZywgb25seSBgbWFpblR5cGVgIG9yIGBtYWluVHlwZS5zdWJUeXBlYCxcbiAqICAgbGlrZTogJ3hBeGlzJywgJ3NlcmllcycsICd4QXhpcy5jYXRlZ29yeScgb3IgJ3Nlcmllcy5saW5lJy5cbiAqICsgVGhlIGNvbXBvbmVudCBxdWVyeSBvYmplY3QsIGxpa2U6XG4gKiAgIGB7c2VyaWVzSW5kZXg6IDJ9YCwgYHtzZXJpZXNOYW1lOiAneHgnfWAsIGB7c2VyaWVzSWQ6ICdzb21lJ31gLFxuICogICBge3hBeGlzSW5kZXg6IDJ9YCwgYHt4QXhpc05hbWU6ICd4eCd9YCwgYHt4QXhpc0lkOiAnc29tZSd9YC5cbiAqICsgVGhlIGRhdGEgcXVlcnkgb2JqZWN0LCBsaWtlOlxuICogICBge2RhdGFJbmRleDogMTIzfWAsIGB7ZGF0YVR5cGU6ICdsaW5rJ31gLCBge25hbWU6ICdzb21lJ31gLlxuICogKyBUaGUgb3RoZXIgcXVlcnkgb2JqZWN0IChjbXBvbmVudCBjdXN0b21pemVkIHF1ZXJ5KSwgbGlrZTpcbiAqICAgYHtlbGVtZW50OiAnc29tZSd9YCAob25seSBhdmFpbGFibGUgaW4gY3VzdG9tIHNlcmllcykuXG4gKlxuICogQ2F2ZWF0OiBJZiBhIHByb3AgaW4gdGhlIGBxdWVyeWAgb2JqZWN0IGlzIGBudWxsL3VuZGVmaW5lZGAsIGl0IGlzIHRoZVxuICogc2FtZSBhcyB0aGVyZSBpcyBubyBzdWNoIHByb3AgaW4gdGhlIGBxdWVyeWAgb2JqZWN0LlxuICovXG5cblxuZnVuY3Rpb24gRXZlbnRQcm9jZXNzb3IoKSB7XG4gIC8vIFRoZXNlIGluZm8gcmVxdWlyZWQ6IHRhcmdldEVsLCBwYWNrZWRFdmVudCwgbW9kZWwsIHZpZXdcbiAgdGhpcy5ldmVudEluZm87XG59XG5cbkV2ZW50UHJvY2Vzc29yLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IEV2ZW50UHJvY2Vzc29yLFxuICBub3JtYWxpemVRdWVyeTogZnVuY3Rpb24gKHF1ZXJ5KSB7XG4gICAgdmFyIGNwdFF1ZXJ5ID0ge307XG4gICAgdmFyIGRhdGFRdWVyeSA9IHt9O1xuICAgIHZhciBvdGhlclF1ZXJ5ID0ge307IC8vIGBxdWVyeWAgaXMgYG1haW5UeXBlYCBvciBgbWFpblR5cGUuc3ViVHlwZWAgb2YgY29tcG9uZW50LlxuXG4gICAgaWYgKHpyVXRpbC5pc1N0cmluZyhxdWVyeSkpIHtcbiAgICAgIHZhciBjb25kQ3B0VHlwZSA9IHBhcnNlQ2xhc3NUeXBlKHF1ZXJ5KTsgLy8gYC5tYWluYCBhbmQgYC5zdWJgIG1heSBiZSAnJy5cblxuICAgICAgY3B0UXVlcnkubWFpblR5cGUgPSBjb25kQ3B0VHlwZS5tYWluIHx8IG51bGw7XG4gICAgICBjcHRRdWVyeS5zdWJUeXBlID0gY29uZENwdFR5cGUuc3ViIHx8IG51bGw7XG4gICAgfSAvLyBgcXVlcnlgIGlzIGFuIG9iamVjdCwgY29udmVydCB0byB7bWFpblR5cGUsIGluZGV4LCBuYW1lLCBpZH0uXG4gICAgZWxzZSB7XG4gICAgICAgIC8vIGB4eHhJbmRleGAsIGB4eHhOYW1lYCwgYHh4eElkYCwgYG5hbWVgLCBgZGF0YUluZGV4YCwgYGRhdGFUeXBlYCBpcyByZXNlcnZlZCxcbiAgICAgICAgLy8gY2FuIG5vdCBiZSB1c2VkIGluIGBjb21wb21lbnRNb2RlbC5maWx0ZXJGb3JFeHBvc2VkRXZlbnRgLlxuICAgICAgICB2YXIgc3VmZml4ZXMgPSBbJ0luZGV4JywgJ05hbWUnLCAnSWQnXTtcbiAgICAgICAgdmFyIGRhdGFLZXlzID0ge1xuICAgICAgICAgIG5hbWU6IDEsXG4gICAgICAgICAgZGF0YUluZGV4OiAxLFxuICAgICAgICAgIGRhdGFUeXBlOiAxXG4gICAgICAgIH07XG4gICAgICAgIHpyVXRpbC5lYWNoKHF1ZXJ5LCBmdW5jdGlvbiAodmFsLCBrZXkpIHtcbiAgICAgICAgICB2YXIgcmVzZXJ2ZWQgPSBmYWxzZTtcblxuICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc3VmZml4ZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHZhciBwcm9wU3VmZml4ID0gc3VmZml4ZXNbaV07XG4gICAgICAgICAgICB2YXIgc3VmZml4UG9zID0ga2V5Lmxhc3RJbmRleE9mKHByb3BTdWZmaXgpO1xuXG4gICAgICAgICAgICBpZiAoc3VmZml4UG9zID4gMCAmJiBzdWZmaXhQb3MgPT09IGtleS5sZW5ndGggLSBwcm9wU3VmZml4Lmxlbmd0aCkge1xuICAgICAgICAgICAgICB2YXIgbWFpblR5cGUgPSBrZXkuc2xpY2UoMCwgc3VmZml4UG9zKTsgLy8gQ29uc2lkZXIgYGRhdGFJbmRleGAuXG5cbiAgICAgICAgICAgICAgaWYgKG1haW5UeXBlICE9PSAnZGF0YScpIHtcbiAgICAgICAgICAgICAgICBjcHRRdWVyeS5tYWluVHlwZSA9IG1haW5UeXBlO1xuICAgICAgICAgICAgICAgIGNwdFF1ZXJ5W3Byb3BTdWZmaXgudG9Mb3dlckNhc2UoKV0gPSB2YWw7XG4gICAgICAgICAgICAgICAgcmVzZXJ2ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKGRhdGFLZXlzLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgIGRhdGFRdWVyeVtrZXldID0gdmFsO1xuICAgICAgICAgICAgcmVzZXJ2ZWQgPSB0cnVlO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmICghcmVzZXJ2ZWQpIHtcbiAgICAgICAgICAgIG90aGVyUXVlcnlba2V5XSA9IHZhbDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgIGNwdFF1ZXJ5OiBjcHRRdWVyeSxcbiAgICAgIGRhdGFRdWVyeTogZGF0YVF1ZXJ5LFxuICAgICAgb3RoZXJRdWVyeTogb3RoZXJRdWVyeVxuICAgIH07XG4gIH0sXG4gIGZpbHRlcjogZnVuY3Rpb24gKGV2ZW50VHlwZSwgcXVlcnksIGFyZ3MpIHtcbiAgICAvLyBUaGV5IHNob3VsZCBiZSBhc3NpZ25lZCBiZWZvcmUgZWFjaCB0cmlnZ2VyIGNhbGwuXG4gICAgdmFyIGV2ZW50SW5mbyA9IHRoaXMuZXZlbnRJbmZvO1xuXG4gICAgaWYgKCFldmVudEluZm8pIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHZhciB0YXJnZXRFbCA9IGV2ZW50SW5mby50YXJnZXRFbDtcbiAgICB2YXIgcGFja2VkRXZlbnQgPSBldmVudEluZm8ucGFja2VkRXZlbnQ7XG4gICAgdmFyIG1vZGVsID0gZXZlbnRJbmZvLm1vZGVsO1xuICAgIHZhciB2aWV3ID0gZXZlbnRJbmZvLnZpZXc7IC8vIEZvciBldmVudCBsaWtlICdnbG9iYWxvdXQnLlxuXG4gICAgaWYgKCFtb2RlbCB8fCAhdmlldykge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgdmFyIGNwdFF1ZXJ5ID0gcXVlcnkuY3B0UXVlcnk7XG4gICAgdmFyIGRhdGFRdWVyeSA9IHF1ZXJ5LmRhdGFRdWVyeTtcbiAgICByZXR1cm4gY2hlY2soY3B0UXVlcnksIG1vZGVsLCAnbWFpblR5cGUnKSAmJiBjaGVjayhjcHRRdWVyeSwgbW9kZWwsICdzdWJUeXBlJykgJiYgY2hlY2soY3B0UXVlcnksIG1vZGVsLCAnaW5kZXgnLCAnY29tcG9uZW50SW5kZXgnKSAmJiBjaGVjayhjcHRRdWVyeSwgbW9kZWwsICduYW1lJykgJiYgY2hlY2soY3B0UXVlcnksIG1vZGVsLCAnaWQnKSAmJiBjaGVjayhkYXRhUXVlcnksIHBhY2tlZEV2ZW50LCAnbmFtZScpICYmIGNoZWNrKGRhdGFRdWVyeSwgcGFja2VkRXZlbnQsICdkYXRhSW5kZXgnKSAmJiBjaGVjayhkYXRhUXVlcnksIHBhY2tlZEV2ZW50LCAnZGF0YVR5cGUnKSAmJiAoIXZpZXcuZmlsdGVyRm9yRXhwb3NlZEV2ZW50IHx8IHZpZXcuZmlsdGVyRm9yRXhwb3NlZEV2ZW50KGV2ZW50VHlwZSwgcXVlcnkub3RoZXJRdWVyeSwgdGFyZ2V0RWwsIHBhY2tlZEV2ZW50KSk7XG5cbiAgICBmdW5jdGlvbiBjaGVjayhxdWVyeSwgaG9zdCwgcHJvcCwgcHJvcE9uSG9zdCkge1xuICAgICAgcmV0dXJuIHF1ZXJ5W3Byb3BdID09IG51bGwgfHwgaG9zdFtwcm9wT25Ib3N0IHx8IHByb3BdID09PSBxdWVyeVtwcm9wXTtcbiAgICB9XG4gIH0sXG4gIGFmdGVyVHJpZ2dlcjogZnVuY3Rpb24gKCkge1xuICAgIC8vIE1ha2Ugc3VyZSB0aGUgZXZlbnRJbmZvIHdvbnQgYmUgdXNlZCBpbiBuZXh0IHRyaWdnZXIuXG4gICAgdGhpcy5ldmVudEluZm8gPSBudWxsO1xuICB9XG59O1xuLyoqXG4gKiBAdHlwZSB7T2JqZWN0fSBrZXk6IGFjdGlvblR5cGUuXG4gKiBAaW5uZXJcbiAqL1xuXG52YXIgYWN0aW9ucyA9IHt9O1xuLyoqXG4gKiBNYXAgZXZlbnRUeXBlIHRvIGFjdGlvblR5cGVcbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cblxudmFyIGV2ZW50QWN0aW9uTWFwID0ge307XG4vKipcbiAqIERhdGEgcHJvY2Vzc29yIGZ1bmN0aW9ucyBvZiBlYWNoIHN0YWdlXG4gKiBAdHlwZSB7QXJyYXkuPE9iamVjdC48c3RyaW5nLCBGdW5jdGlvbj4+fVxuICogQGlubmVyXG4gKi9cblxudmFyIGRhdGFQcm9jZXNzb3JGdW5jcyA9IFtdO1xuLyoqXG4gKiBAdHlwZSB7QXJyYXkuPEZ1bmN0aW9uPn1cbiAqIEBpbm5lclxuICovXG5cbnZhciBvcHRpb25QcmVwcm9jZXNzb3JGdW5jcyA9IFtdO1xuLyoqXG4gKiBAdHlwZSB7QXJyYXkuPEZ1bmN0aW9uPn1cbiAqIEBpbm5lclxuICovXG5cbnZhciBwb3N0VXBkYXRlRnVuY3MgPSBbXTtcbi8qKlxuICogVmlzdWFsIGVuY29kaW5nIGZ1bmN0aW9ucyBvZiBlYWNoIHN0YWdlXG4gKiBAdHlwZSB7QXJyYXkuPE9iamVjdC48c3RyaW5nLCBGdW5jdGlvbj4+fVxuICovXG5cbnZhciB2aXN1YWxGdW5jcyA9IFtdO1xuLyoqXG4gKiBUaGVtZSBzdG9yYWdlXG4gKiBAdHlwZSB7T2JqZWN0LjxrZXksIE9iamVjdD59XG4gKi9cblxudmFyIHRoZW1lU3RvcmFnZSA9IHt9O1xuLyoqXG4gKiBMb2FkaW5nIGVmZmVjdHNcbiAqL1xuXG52YXIgbG9hZGluZ0VmZmVjdHMgPSB7fTtcbnZhciBpbnN0YW5jZXMgPSB7fTtcbnZhciBjb25uZWN0ZWRHcm91cHMgPSB7fTtcbnZhciBpZEJhc2UgPSBuZXcgRGF0ZSgpIC0gMDtcbnZhciBncm91cElkQmFzZSA9IG5ldyBEYXRlKCkgLSAwO1xudmFyIERPTV9BVFRSSUJVVEVfS0VZID0gJ19lY2hhcnRzX2luc3RhbmNlXyc7XG5cbmZ1bmN0aW9uIGVuYWJsZUNvbm5lY3QoY2hhcnQpIHtcbiAgdmFyIFNUQVRVU19QRU5ESU5HID0gMDtcbiAgdmFyIFNUQVRVU19VUERBVElORyA9IDE7XG4gIHZhciBTVEFUVVNfVVBEQVRFRCA9IDI7XG4gIHZhciBTVEFUVVNfS0VZID0gJ19fY29ubmVjdFVwZGF0ZVN0YXR1cyc7XG5cbiAgZnVuY3Rpb24gdXBkYXRlQ29ubmVjdGVkQ2hhcnRzU3RhdHVzKGNoYXJ0cywgc3RhdHVzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGFydHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBvdGhlckNoYXJ0ID0gY2hhcnRzW2ldO1xuICAgICAgb3RoZXJDaGFydFtTVEFUVVNfS0VZXSA9IHN0YXR1cztcbiAgICB9XG4gIH1cblxuICBlYWNoKGV2ZW50QWN0aW9uTWFwLCBmdW5jdGlvbiAoYWN0aW9uVHlwZSwgZXZlbnRUeXBlKSB7XG4gICAgY2hhcnQuX21lc3NhZ2VDZW50ZXIub24oZXZlbnRUeXBlLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmIChjb25uZWN0ZWRHcm91cHNbY2hhcnQuZ3JvdXBdICYmIGNoYXJ0W1NUQVRVU19LRVldICE9PSBTVEFUVVNfUEVORElORykge1xuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuZXNjYXBlQ29ubmVjdCkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBhY3Rpb24gPSBjaGFydC5tYWtlQWN0aW9uRnJvbUV2ZW50KGV2ZW50KTtcbiAgICAgICAgdmFyIG90aGVyQ2hhcnRzID0gW107XG4gICAgICAgIGVhY2goaW5zdGFuY2VzLCBmdW5jdGlvbiAob3RoZXJDaGFydCkge1xuICAgICAgICAgIGlmIChvdGhlckNoYXJ0ICE9PSBjaGFydCAmJiBvdGhlckNoYXJ0Lmdyb3VwID09PSBjaGFydC5ncm91cCkge1xuICAgICAgICAgICAgb3RoZXJDaGFydHMucHVzaChvdGhlckNoYXJ0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICB1cGRhdGVDb25uZWN0ZWRDaGFydHNTdGF0dXMob3RoZXJDaGFydHMsIFNUQVRVU19QRU5ESU5HKTtcbiAgICAgICAgZWFjaChvdGhlckNoYXJ0cywgZnVuY3Rpb24gKG90aGVyQ2hhcnQpIHtcbiAgICAgICAgICBpZiAob3RoZXJDaGFydFtTVEFUVVNfS0VZXSAhPT0gU1RBVFVTX1VQREFUSU5HKSB7XG4gICAgICAgICAgICBvdGhlckNoYXJ0LmRpc3BhdGNoQWN0aW9uKGFjdGlvbik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgdXBkYXRlQ29ubmVjdGVkQ2hhcnRzU3RhdHVzKG90aGVyQ2hhcnRzLCBTVEFUVVNfVVBEQVRFRCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufVxuLyoqXG4gKiBAcGFyYW0ge0hUTUxFbGVtZW50fSBkb21cbiAqIEBwYXJhbSB7T2JqZWN0fSBbdGhlbWVdXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICogQHBhcmFtIHtudW1iZXJ9IFtvcHRzLmRldmljZVBpeGVsUmF0aW9dIFVzZSB3aW5kb3cuZGV2aWNlUGl4ZWxSYXRpbyBieSBkZWZhdWx0XG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdHMucmVuZGVyZXJdIEN1cnJlbnRseSBvbmx5ICdjYW52YXMnIGlzIHN1cHBvcnRlZC5cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0cy53aWR0aF0gVXNlIGNsaWVudFdpZHRoIG9mIHRoZSBpbnB1dCBgZG9tYCBieSBkZWZhdWx0LlxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0cy5oZWlnaHRdIFVzZSBjbGllbnRIZWlnaHQgb2YgdGhlIGlucHV0IGBkb21gIGJ5IGRlZmF1bHQuXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDYW4gYmUgJ2F1dG8nICh0aGUgc2FtZSBhcyBudWxsL3VuZGVmaW5lZClcbiAqL1xuXG5cbmZ1bmN0aW9uIGluaXQoZG9tLCB0aGVtZSwgb3B0cykge1xuICB2YXIgZXhpc3RJbnN0YW5jZSA9IGdldEluc3RhbmNlQnlEb20oZG9tKTtcblxuICBpZiAoZXhpc3RJbnN0YW5jZSkge1xuICAgIHJldHVybiBleGlzdEluc3RhbmNlO1xuICB9XG5cbiAgdmFyIGNoYXJ0ID0gbmV3IEVDaGFydHMoZG9tLCB0aGVtZSwgb3B0cyk7XG4gIGNoYXJ0LmlkID0gJ2VjXycgKyBpZEJhc2UrKztcbiAgaW5zdGFuY2VzW2NoYXJ0LmlkXSA9IGNoYXJ0O1xuICBtb2RlbFV0aWwuc2V0QXR0cmlidXRlKGRvbSwgRE9NX0FUVFJJQlVURV9LRVksIGNoYXJ0LmlkKTtcbiAgZW5hYmxlQ29ubmVjdChjaGFydCk7XG4gIHJldHVybiBjaGFydDtcbn1cbi8qKlxuICogQHJldHVybiB7c3RyaW5nfEFycmF5Ljxtb2R1bGU6ZWNoYXJ0c35FQ2hhcnRzPn0gZ3JvdXBJZFxuICovXG5cblxuZnVuY3Rpb24gY29ubmVjdChncm91cElkKSB7XG4gIC8vIElzIGFycmF5IG9mIGNoYXJ0c1xuICBpZiAoenJVdGlsLmlzQXJyYXkoZ3JvdXBJZCkpIHtcbiAgICB2YXIgY2hhcnRzID0gZ3JvdXBJZDtcbiAgICBncm91cElkID0gbnVsbDsgLy8gSWYgYW55IGNoYXJ0IGhhcyBncm91cFxuXG4gICAgZWFjaChjaGFydHMsIGZ1bmN0aW9uIChjaGFydCkge1xuICAgICAgaWYgKGNoYXJ0Lmdyb3VwICE9IG51bGwpIHtcbiAgICAgICAgZ3JvdXBJZCA9IGNoYXJ0Lmdyb3VwO1xuICAgICAgfVxuICAgIH0pO1xuICAgIGdyb3VwSWQgPSBncm91cElkIHx8ICdnXycgKyBncm91cElkQmFzZSsrO1xuICAgIGVhY2goY2hhcnRzLCBmdW5jdGlvbiAoY2hhcnQpIHtcbiAgICAgIGNoYXJ0Lmdyb3VwID0gZ3JvdXBJZDtcbiAgICB9KTtcbiAgfVxuXG4gIGNvbm5lY3RlZEdyb3Vwc1tncm91cElkXSA9IHRydWU7XG4gIHJldHVybiBncm91cElkO1xufVxuLyoqXG4gKiBAREVQUkVDQVRFRFxuICogQHJldHVybiB7c3RyaW5nfSBncm91cElkXG4gKi9cblxuXG5mdW5jdGlvbiBkaXNDb25uZWN0KGdyb3VwSWQpIHtcbiAgY29ubmVjdGVkR3JvdXBzW2dyb3VwSWRdID0gZmFsc2U7XG59XG4vKipcbiAqIEByZXR1cm4ge3N0cmluZ30gZ3JvdXBJZFxuICovXG5cblxudmFyIGRpc2Nvbm5lY3QgPSBkaXNDb25uZWN0O1xuLyoqXG4gKiBEaXNwb3NlIGEgY2hhcnQgaW5zdGFuY2VcbiAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzfkVDaGFydHN8SFRNTERvbUVsZW1lbnR8c3RyaW5nfSBjaGFydFxuICovXG5cbmZ1bmN0aW9uIGRpc3Bvc2UoY2hhcnQpIHtcbiAgaWYgKHR5cGVvZiBjaGFydCA9PT0gJ3N0cmluZycpIHtcbiAgICBjaGFydCA9IGluc3RhbmNlc1tjaGFydF07XG4gIH0gZWxzZSBpZiAoIShjaGFydCBpbnN0YW5jZW9mIEVDaGFydHMpKSB7XG4gICAgLy8gVHJ5IHRvIHRyZWF0IGFzIGRvbVxuICAgIGNoYXJ0ID0gZ2V0SW5zdGFuY2VCeURvbShjaGFydCk7XG4gIH1cblxuICBpZiAoY2hhcnQgaW5zdGFuY2VvZiBFQ2hhcnRzICYmICFjaGFydC5pc0Rpc3Bvc2VkKCkpIHtcbiAgICBjaGFydC5kaXNwb3NlKCk7XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtICB7SFRNTEVsZW1lbnR9IGRvbVxuICogQHJldHVybiB7ZWNoYXJ0c35FQ2hhcnRzfVxuICovXG5cblxuZnVuY3Rpb24gZ2V0SW5zdGFuY2VCeURvbShkb20pIHtcbiAgcmV0dXJuIGluc3RhbmNlc1ttb2RlbFV0aWwuZ2V0QXR0cmlidXRlKGRvbSwgRE9NX0FUVFJJQlVURV9LRVkpXTtcbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICogQHJldHVybiB7ZWNoYXJ0c35FQ2hhcnRzfVxuICovXG5cblxuZnVuY3Rpb24gZ2V0SW5zdGFuY2VCeUlkKGtleSkge1xuICByZXR1cm4gaW5zdGFuY2VzW2tleV07XG59XG4vKipcbiAqIFJlZ2lzdGVyIHRoZW1lXG4gKi9cblxuXG5mdW5jdGlvbiByZWdpc3RlclRoZW1lKG5hbWUsIHRoZW1lKSB7XG4gIHRoZW1lU3RvcmFnZVtuYW1lXSA9IHRoZW1lO1xufVxuLyoqXG4gKiBSZWdpc3RlciBvcHRpb24gcHJlcHJvY2Vzc29yXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcmVwcm9jZXNzb3JGdW5jXG4gKi9cblxuXG5mdW5jdGlvbiByZWdpc3RlclByZXByb2Nlc3NvcihwcmVwcm9jZXNzb3JGdW5jKSB7XG4gIG9wdGlvblByZXByb2Nlc3NvckZ1bmNzLnB1c2gocHJlcHJvY2Vzc29yRnVuYyk7XG59XG4vKipcbiAqIEBwYXJhbSB7bnVtYmVyfSBbcHJpb3JpdHk9MTAwMF1cbiAqIEBwYXJhbSB7T2JqZWN0fEZ1bmN0aW9ufSBwcm9jZXNzb3JcbiAqL1xuXG5cbmZ1bmN0aW9uIHJlZ2lzdGVyUHJvY2Vzc29yKHByaW9yaXR5LCBwcm9jZXNzb3IpIHtcbiAgbm9ybWFsaXplUmVnaXN0ZXIoZGF0YVByb2Nlc3NvckZ1bmNzLCBwcmlvcml0eSwgcHJvY2Vzc29yLCBQUklPUklUWV9QUk9DRVNTT1JfRklMVEVSKTtcbn1cbi8qKlxuICogUmVnaXN0ZXIgcG9zdFVwZGF0ZXJcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHBvc3RVcGRhdGVGdW5jXG4gKi9cblxuXG5mdW5jdGlvbiByZWdpc3RlclBvc3RVcGRhdGUocG9zdFVwZGF0ZUZ1bmMpIHtcbiAgcG9zdFVwZGF0ZUZ1bmNzLnB1c2gocG9zdFVwZGF0ZUZ1bmMpO1xufVxuLyoqXG4gKiBVc2FnZTpcbiAqIHJlZ2lzdGVyQWN0aW9uKCdzb21lQWN0aW9uJywgJ3NvbWVFdmVudCcsIGZ1bmN0aW9uICgpIHsgLi4uIH0pO1xuICogcmVnaXN0ZXJBY3Rpb24oJ3NvbWVBY3Rpb24nLCBmdW5jdGlvbiAoKSB7IC4uLiB9KTtcbiAqIHJlZ2lzdGVyQWN0aW9uKFxuICogICAgIHt0eXBlOiAnc29tZUFjdGlvbicsIGV2ZW50OiAnc29tZUV2ZW50JywgdXBkYXRlOiAndXBkYXRlVmlldyd9LFxuICogICAgIGZ1bmN0aW9uICgpIHsgLi4uIH1cbiAqICk7XG4gKlxuICogQHBhcmFtIHsoc3RyaW5nfE9iamVjdCl9IGFjdGlvbkluZm9cbiAqIEBwYXJhbSB7c3RyaW5nfSBhY3Rpb25JbmZvLnR5cGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBbYWN0aW9uSW5mby5ldmVudF1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbYWN0aW9uSW5mby51cGRhdGVdXG4gKiBAcGFyYW0ge3N0cmluZ30gW2V2ZW50TmFtZV1cbiAqIEBwYXJhbSB7RnVuY3Rpb259IGFjdGlvblxuICovXG5cblxuZnVuY3Rpb24gcmVnaXN0ZXJBY3Rpb24oYWN0aW9uSW5mbywgZXZlbnROYW1lLCBhY3Rpb24pIHtcbiAgaWYgKHR5cGVvZiBldmVudE5hbWUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBhY3Rpb24gPSBldmVudE5hbWU7XG4gICAgZXZlbnROYW1lID0gJyc7XG4gIH1cblxuICB2YXIgYWN0aW9uVHlwZSA9IGlzT2JqZWN0KGFjdGlvbkluZm8pID8gYWN0aW9uSW5mby50eXBlIDogW2FjdGlvbkluZm8sIGFjdGlvbkluZm8gPSB7XG4gICAgZXZlbnQ6IGV2ZW50TmFtZVxuICB9XVswXTsgLy8gRXZlbnQgbmFtZSBpcyBhbGwgbG93ZXJjYXNlXG5cbiAgYWN0aW9uSW5mby5ldmVudCA9IChhY3Rpb25JbmZvLmV2ZW50IHx8IGFjdGlvblR5cGUpLnRvTG93ZXJDYXNlKCk7XG4gIGV2ZW50TmFtZSA9IGFjdGlvbkluZm8uZXZlbnQ7IC8vIFZhbGlkYXRlIGFjdGlvbiB0eXBlIGFuZCBldmVudCBuYW1lLlxuXG4gIGFzc2VydChBQ1RJT05fUkVHLnRlc3QoYWN0aW9uVHlwZSkgJiYgQUNUSU9OX1JFRy50ZXN0KGV2ZW50TmFtZSkpO1xuXG4gIGlmICghYWN0aW9uc1thY3Rpb25UeXBlXSkge1xuICAgIGFjdGlvbnNbYWN0aW9uVHlwZV0gPSB7XG4gICAgICBhY3Rpb246IGFjdGlvbixcbiAgICAgIGFjdGlvbkluZm86IGFjdGlvbkluZm9cbiAgICB9O1xuICB9XG5cbiAgZXZlbnRBY3Rpb25NYXBbZXZlbnROYW1lXSA9IGFjdGlvblR5cGU7XG59XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlXG4gKiBAcGFyYW0geyp9IENvb3JkaW5hdGVTeXN0ZW1cbiAqL1xuXG5cbmZ1bmN0aW9uIHJlZ2lzdGVyQ29vcmRpbmF0ZVN5c3RlbSh0eXBlLCBDb29yZGluYXRlU3lzdGVtKSB7XG4gIENvb3JkaW5hdGVTeXN0ZW1NYW5hZ2VyLnJlZ2lzdGVyKHR5cGUsIENvb3JkaW5hdGVTeXN0ZW0pO1xufVxuLyoqXG4gKiBHZXQgZGltZW5zaW9ucyBvZiBzcGVjaWZpZWQgY29vcmRpbmF0ZSBzeXN0ZW0uXG4gKiBAcGFyYW0ge3N0cmluZ30gdHlwZVxuICogQHJldHVybiB7QXJyYXkuPHN0cmluZ3xPYmplY3Q+fVxuICovXG5cblxuZnVuY3Rpb24gZ2V0Q29vcmRpbmF0ZVN5c3RlbURpbWVuc2lvbnModHlwZSkge1xuICB2YXIgY29vcmRTeXNDcmVhdG9yID0gQ29vcmRpbmF0ZVN5c3RlbU1hbmFnZXIuZ2V0KHR5cGUpO1xuXG4gIGlmIChjb29yZFN5c0NyZWF0b3IpIHtcbiAgICByZXR1cm4gY29vcmRTeXNDcmVhdG9yLmdldERpbWVuc2lvbnNJbmZvID8gY29vcmRTeXNDcmVhdG9yLmdldERpbWVuc2lvbnNJbmZvKCkgOiBjb29yZFN5c0NyZWF0b3IuZGltZW5zaW9ucy5zbGljZSgpO1xuICB9XG59XG4vKipcbiAqIExheW91dCBpcyBhIHNwZWNpYWwgc3RhZ2Ugb2YgdmlzdWFsIGVuY29kaW5nXG4gKiBNb3N0IHZpc3VhbCBlbmNvZGluZyBsaWtlIGNvbG9yIGFyZSBjb21tb24gZm9yIGRpZmZlcmVudCBjaGFydFxuICogQnV0IGVhY2ggY2hhcnQgaGFzIGl0J3Mgb3duIGxheW91dCBhbGdvcml0aG1cbiAqXG4gKiBAcGFyYW0ge251bWJlcn0gW3ByaW9yaXR5PTEwMDBdXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBsYXlvdXRUYXNrXG4gKi9cblxuXG5mdW5jdGlvbiByZWdpc3RlckxheW91dChwcmlvcml0eSwgbGF5b3V0VGFzaykge1xuICBub3JtYWxpemVSZWdpc3Rlcih2aXN1YWxGdW5jcywgcHJpb3JpdHksIGxheW91dFRhc2ssIFBSSU9SSVRZX1ZJU1VBTF9MQVlPVVQsICdsYXlvdXQnKTtcbn1cbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IFtwcmlvcml0eT0zMDAwXVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9zdHJlYW0vVGFza30gdmlzdWFsVGFza1xuICovXG5cblxuZnVuY3Rpb24gcmVnaXN0ZXJWaXN1YWwocHJpb3JpdHksIHZpc3VhbFRhc2spIHtcbiAgbm9ybWFsaXplUmVnaXN0ZXIodmlzdWFsRnVuY3MsIHByaW9yaXR5LCB2aXN1YWxUYXNrLCBQUklPUklUWV9WSVNVQUxfQ0hBUlQsICd2aXN1YWwnKTtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R8RnVuY3Rpb259IGZuOiB7c2VyaWVzVHlwZSwgY3JlYXRlT25BbGxTZXJpZXMsIHBlcmZvcm1SYXdTZXJpZXMsIHJlc2V0fVxuICovXG5cblxuZnVuY3Rpb24gbm9ybWFsaXplUmVnaXN0ZXIodGFyZ2V0TGlzdCwgcHJpb3JpdHksIGZuLCBkZWZhdWx0UHJpb3JpdHksIHZpc3VhbFR5cGUpIHtcbiAgaWYgKGlzRnVuY3Rpb24ocHJpb3JpdHkpIHx8IGlzT2JqZWN0KHByaW9yaXR5KSkge1xuICAgIGZuID0gcHJpb3JpdHk7XG4gICAgcHJpb3JpdHkgPSBkZWZhdWx0UHJpb3JpdHk7XG4gIH1cblxuICB2YXIgc3RhZ2VIYW5kbGVyID0gU2NoZWR1bGVyLndyYXBTdGFnZUhhbmRsZXIoZm4sIHZpc3VhbFR5cGUpO1xuICBzdGFnZUhhbmRsZXIuX19wcmlvID0gcHJpb3JpdHk7XG4gIHN0YWdlSGFuZGxlci5fX3JhdyA9IGZuO1xuICB0YXJnZXRMaXN0LnB1c2goc3RhZ2VIYW5kbGVyKTtcbiAgcmV0dXJuIHN0YWdlSGFuZGxlcjtcbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IG5hbWVcbiAqL1xuXG5cbmZ1bmN0aW9uIHJlZ2lzdGVyTG9hZGluZyhuYW1lLCBsb2FkaW5nRngpIHtcbiAgbG9hZGluZ0VmZmVjdHNbbmFtZV0gPSBsb2FkaW5nRng7XG59XG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXG4gKiBAcGFyYW0ge3N0cmluZ30gW3N1cGVyQ2xhc3NdXG4gKi9cblxuXG5mdW5jdGlvbiBleHRlbmRDb21wb25lbnRNb2RlbChvcHRzXG4vKiwgc3VwZXJDbGFzcyovXG4pIHtcbiAgLy8gdmFyIENsYXp6ID0gQ29tcG9uZW50TW9kZWw7XG4gIC8vIGlmIChzdXBlckNsYXNzKSB7XG4gIC8vICAgICB2YXIgY2xhc3NUeXBlID0gcGFyc2VDbGFzc1R5cGUoc3VwZXJDbGFzcyk7XG4gIC8vICAgICBDbGF6eiA9IENvbXBvbmVudE1vZGVsLmdldENsYXNzKGNsYXNzVHlwZS5tYWluLCBjbGFzc1R5cGUuc3ViLCB0cnVlKTtcbiAgLy8gfVxuICByZXR1cm4gQ29tcG9uZW50TW9kZWwuZXh0ZW5kKG9wdHMpO1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICogQHBhcmFtIHtzdHJpbmd9IFtzdXBlckNsYXNzXVxuICovXG5cblxuZnVuY3Rpb24gZXh0ZW5kQ29tcG9uZW50VmlldyhvcHRzXG4vKiwgc3VwZXJDbGFzcyovXG4pIHtcbiAgLy8gdmFyIENsYXp6ID0gQ29tcG9uZW50VmlldztcbiAgLy8gaWYgKHN1cGVyQ2xhc3MpIHtcbiAgLy8gICAgIHZhciBjbGFzc1R5cGUgPSBwYXJzZUNsYXNzVHlwZShzdXBlckNsYXNzKTtcbiAgLy8gICAgIENsYXp6ID0gQ29tcG9uZW50Vmlldy5nZXRDbGFzcyhjbGFzc1R5cGUubWFpbiwgY2xhc3NUeXBlLnN1YiwgdHJ1ZSk7XG4gIC8vIH1cbiAgcmV0dXJuIENvbXBvbmVudFZpZXcuZXh0ZW5kKG9wdHMpO1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICogQHBhcmFtIHtzdHJpbmd9IFtzdXBlckNsYXNzXVxuICovXG5cblxuZnVuY3Rpb24gZXh0ZW5kU2VyaWVzTW9kZWwob3B0c1xuLyosIHN1cGVyQ2xhc3MqL1xuKSB7XG4gIC8vIHZhciBDbGF6eiA9IFNlcmllc01vZGVsO1xuICAvLyBpZiAoc3VwZXJDbGFzcykge1xuICAvLyAgICAgc3VwZXJDbGFzcyA9ICdzZXJpZXMuJyArIHN1cGVyQ2xhc3MucmVwbGFjZSgnc2VyaWVzLicsICcnKTtcbiAgLy8gICAgIHZhciBjbGFzc1R5cGUgPSBwYXJzZUNsYXNzVHlwZShzdXBlckNsYXNzKTtcbiAgLy8gICAgIENsYXp6ID0gQ29tcG9uZW50TW9kZWwuZ2V0Q2xhc3MoY2xhc3NUeXBlLm1haW4sIGNsYXNzVHlwZS5zdWIsIHRydWUpO1xuICAvLyB9XG4gIHJldHVybiBTZXJpZXNNb2RlbC5leHRlbmQob3B0cyk7XG59XG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXG4gKiBAcGFyYW0ge3N0cmluZ30gW3N1cGVyQ2xhc3NdXG4gKi9cblxuXG5mdW5jdGlvbiBleHRlbmRDaGFydFZpZXcob3B0c1xuLyosIHN1cGVyQ2xhc3MqL1xuKSB7XG4gIC8vIHZhciBDbGF6eiA9IENoYXJ0VmlldztcbiAgLy8gaWYgKHN1cGVyQ2xhc3MpIHtcbiAgLy8gICAgIHN1cGVyQ2xhc3MgPSBzdXBlckNsYXNzLnJlcGxhY2UoJ3Nlcmllcy4nLCAnJyk7XG4gIC8vICAgICB2YXIgY2xhc3NUeXBlID0gcGFyc2VDbGFzc1R5cGUoc3VwZXJDbGFzcyk7XG4gIC8vICAgICBDbGF6eiA9IENoYXJ0Vmlldy5nZXRDbGFzcyhjbGFzc1R5cGUubWFpbiwgdHJ1ZSk7XG4gIC8vIH1cbiAgcmV0dXJuIENoYXJ0Vmlldy5leHRlbmQob3B0cyk7XG59XG4vKipcbiAqIFpSZW5kZXIgbmVlZCBhIGNhbnZhcyBjb250ZXh0IHRvIGRvIG1lYXN1cmVUZXh0LlxuICogQnV0IGluIG5vZGUgZW52aXJvbm1lbnQgY2FudmFzIG1heSBiZSBjcmVhdGVkIGJ5IG5vZGUtY2FudmFzLlxuICogU28gd2UgbmVlZCB0byBzcGVjaWZ5IGhvdyB0byBjcmVhdGUgYSBjYW52YXMgaW5zdGVhZCBvZiB1c2luZyBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKVxuICpcbiAqIEJlIGNhcmVmdWwgb2YgdXNpbmcgaXQgaW4gdGhlIGJyb3dzZXIuXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gY3JlYXRvclxuICogQGV4YW1wbGVcbiAqICAgICB2YXIgQ2FudmFzID0gcmVxdWlyZSgnY2FudmFzJyk7XG4gKiAgICAgdmFyIGVjaGFydHMgPSByZXF1aXJlKCdlY2hhcnRzJyk7XG4gKiAgICAgZWNoYXJ0cy5zZXRDYW52YXNDcmVhdG9yKGZ1bmN0aW9uICgpIHtcbiAqICAgICAgICAgLy8gU21hbGwgc2l6ZSBpcyBlbm91Z2guXG4gKiAgICAgICAgIHJldHVybiBuZXcgQ2FudmFzKDMyLCAzMik7XG4gKiAgICAgfSk7XG4gKi9cblxuXG5mdW5jdGlvbiBzZXRDYW52YXNDcmVhdG9yKGNyZWF0b3IpIHtcbiAgenJVdGlsLiRvdmVycmlkZSgnY3JlYXRlQ2FudmFzJywgY3JlYXRvcik7XG59XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSBtYXBOYW1lXG4gKiBAcGFyYW0ge0FycmF5LjxPYmplY3Q+fE9iamVjdHxzdHJpbmd9IGdlb0pzb25cbiAqIEBwYXJhbSB7T2JqZWN0fSBbc3BlY2lhbEFyZWFzXVxuICpcbiAqIEBleGFtcGxlIEdlb0pTT05cbiAqICAgICAkLmdldCgnVVNBLmpzb24nLCBmdW5jdGlvbiAoZ2VvSnNvbikge1xuICogICAgICAgICBlY2hhcnRzLnJlZ2lzdGVyTWFwKCdVU0EnLCBnZW9Kc29uKTtcbiAqICAgICAgICAgLy8gT3JcbiAqICAgICAgICAgZWNoYXJ0cy5yZWdpc3Rlck1hcCgnVVNBJywge1xuICogICAgICAgICAgICAgZ2VvSnNvbjogZ2VvSnNvbixcbiAqICAgICAgICAgICAgIHNwZWNpYWxBcmVhczoge31cbiAqICAgICAgICAgfSlcbiAqICAgICB9KTtcbiAqXG4gKiAgICAgJC5nZXQoJ2FpcnBvcnQuc3ZnJywgZnVuY3Rpb24gKHN2Zykge1xuICogICAgICAgICBlY2hhcnRzLnJlZ2lzdGVyTWFwKCdhaXJwb3J0Jywge1xuICogICAgICAgICAgICAgc3ZnOiBzdmdcbiAqICAgICAgICAgfVxuICogICAgIH0pO1xuICpcbiAqICAgICBlY2hhcnRzLnJlZ2lzdGVyTWFwKCdldScsIFtcbiAqICAgICAgICAge3N2ZzogZXUtdG9wb2dyYXBoaWMuc3ZnfSxcbiAqICAgICAgICAge2dlb0pTT046IGV1Lmpzb259XG4gKiAgICAgXSlcbiAqL1xuXG5cbmZ1bmN0aW9uIHJlZ2lzdGVyTWFwKG1hcE5hbWUsIGdlb0pzb24sIHNwZWNpYWxBcmVhcykge1xuICBtYXBEYXRhU3RvcmFnZS5yZWdpc3Rlck1hcChtYXBOYW1lLCBnZW9Kc29uLCBzcGVjaWFsQXJlYXMpO1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gbWFwTmFtZVxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5cblxuZnVuY3Rpb24gZ2V0TWFwKG1hcE5hbWUpIHtcbiAgLy8gRm9yIGJhY2t3YXJkIGNvbXBhdGliaWxpdHksIG9ubHkgcmV0dXJuIHRoZSBmaXJzdCBvbmUuXG4gIHZhciByZWNvcmRzID0gbWFwRGF0YVN0b3JhZ2UucmV0cmlldmVNYXAobWFwTmFtZSk7XG4gIHJldHVybiByZWNvcmRzICYmIHJlY29yZHNbMF0gJiYge1xuICAgIGdlb0pzb246IHJlY29yZHNbMF0uZ2VvSlNPTixcbiAgICBzcGVjaWFsQXJlYXM6IHJlY29yZHNbMF0uc3BlY2lhbEFyZWFzXG4gIH07XG59XG5cbnJlZ2lzdGVyVmlzdWFsKFBSSU9SSVRZX1ZJU1VBTF9HTE9CQUwsIHNlcmllc0NvbG9yKTtcbnJlZ2lzdGVyUHJlcHJvY2Vzc29yKGJhY2t3YXJkQ29tcGF0KTtcbnJlZ2lzdGVyUHJvY2Vzc29yKFBSSU9SSVRZX1BST0NFU1NPUl9TVEFUSVNUSUMsIGRhdGFTdGFjayk7XG5yZWdpc3RlckxvYWRpbmcoJ2RlZmF1bHQnLCBsb2FkaW5nRGVmYXVsdCk7IC8vIERlZmF1bHQgYWN0aW9uc1xuXG5yZWdpc3RlckFjdGlvbih7XG4gIHR5cGU6ICdoaWdobGlnaHQnLFxuICBldmVudDogJ2hpZ2hsaWdodCcsXG4gIHVwZGF0ZTogJ2hpZ2hsaWdodCdcbn0sIHpyVXRpbC5ub29wKTtcbnJlZ2lzdGVyQWN0aW9uKHtcbiAgdHlwZTogJ2Rvd25wbGF5JyxcbiAgZXZlbnQ6ICdkb3ducGxheScsXG4gIHVwZGF0ZTogJ2Rvd25wbGF5J1xufSwgenJVdGlsLm5vb3ApOyAvLyBEZWZhdWx0IHRoZW1lXG5cbnJlZ2lzdGVyVGhlbWUoJ2xpZ2h0JywgbGlnaHRUaGVtZSk7XG5yZWdpc3RlclRoZW1lKCdkYXJrJywgZGFya1RoZW1lKTsgLy8gRm9yIGJhY2t3YXJkIGNvbXBhdGliaWxpdHksIHdoZXJlIHRoZSBuYW1lc3BhY2UgYGRhdGFUb29sYCB3aWxsXG4vLyBiZSBtb3VudGVkIG9uIGBlY2hhcnRzYCBpcyB0aGUgZXh0ZW5zaW9uIGBkYXRhVG9vbGAgaXMgaW1wb3J0ZWQuXG5cbnZhciBkYXRhVG9vbCA9IHt9O1xuZXhwb3J0cy52ZXJzaW9uID0gdmVyc2lvbjtcbmV4cG9ydHMuZGVwZW5kZW5jaWVzID0gZGVwZW5kZW5jaWVzO1xuZXhwb3J0cy5QUklPUklUWSA9IFBSSU9SSVRZO1xuZXhwb3J0cy5pbml0ID0gaW5pdDtcbmV4cG9ydHMuY29ubmVjdCA9IGNvbm5lY3Q7XG5leHBvcnRzLmRpc0Nvbm5lY3QgPSBkaXNDb25uZWN0O1xuZXhwb3J0cy5kaXNjb25uZWN0ID0gZGlzY29ubmVjdDtcbmV4cG9ydHMuZGlzcG9zZSA9IGRpc3Bvc2U7XG5leHBvcnRzLmdldEluc3RhbmNlQnlEb20gPSBnZXRJbnN0YW5jZUJ5RG9tO1xuZXhwb3J0cy5nZXRJbnN0YW5jZUJ5SWQgPSBnZXRJbnN0YW5jZUJ5SWQ7XG5leHBvcnRzLnJlZ2lzdGVyVGhlbWUgPSByZWdpc3RlclRoZW1lO1xuZXhwb3J0cy5yZWdpc3RlclByZXByb2Nlc3NvciA9IHJlZ2lzdGVyUHJlcHJvY2Vzc29yO1xuZXhwb3J0cy5yZWdpc3RlclByb2Nlc3NvciA9IHJlZ2lzdGVyUHJvY2Vzc29yO1xuZXhwb3J0cy5yZWdpc3RlclBvc3RVcGRhdGUgPSByZWdpc3RlclBvc3RVcGRhdGU7XG5leHBvcnRzLnJlZ2lzdGVyQWN0aW9uID0gcmVnaXN0ZXJBY3Rpb247XG5leHBvcnRzLnJlZ2lzdGVyQ29vcmRpbmF0ZVN5c3RlbSA9IHJlZ2lzdGVyQ29vcmRpbmF0ZVN5c3RlbTtcbmV4cG9ydHMuZ2V0Q29vcmRpbmF0ZVN5c3RlbURpbWVuc2lvbnMgPSBnZXRDb29yZGluYXRlU3lzdGVtRGltZW5zaW9ucztcbmV4cG9ydHMucmVnaXN0ZXJMYXlvdXQgPSByZWdpc3RlckxheW91dDtcbmV4cG9ydHMucmVnaXN0ZXJWaXN1YWwgPSByZWdpc3RlclZpc3VhbDtcbmV4cG9ydHMucmVnaXN0ZXJMb2FkaW5nID0gcmVnaXN0ZXJMb2FkaW5nO1xuZXhwb3J0cy5leHRlbmRDb21wb25lbnRNb2RlbCA9IGV4dGVuZENvbXBvbmVudE1vZGVsO1xuZXhwb3J0cy5leHRlbmRDb21wb25lbnRWaWV3ID0gZXh0ZW5kQ29tcG9uZW50VmlldztcbmV4cG9ydHMuZXh0ZW5kU2VyaWVzTW9kZWwgPSBleHRlbmRTZXJpZXNNb2RlbDtcbmV4cG9ydHMuZXh0ZW5kQ2hhcnRWaWV3ID0gZXh0ZW5kQ2hhcnRWaWV3O1xuZXhwb3J0cy5zZXRDYW52YXNDcmVhdG9yID0gc2V0Q2FudmFzQ3JlYXRvcjtcbmV4cG9ydHMucmVnaXN0ZXJNYXAgPSByZWdpc3Rlck1hcDtcbmV4cG9ydHMuZ2V0TWFwID0gZ2V0TWFwO1xuZXhwb3J0cy5kYXRhVG9vbCA9IGRhdGFUb29sO1xudmFyIF9fX2VjX2V4cG9ydCA9IHJlcXVpcmUoXCIuL2V4cG9ydFwiKTtcbihmdW5jdGlvbiAoKSB7XG4gICAgZm9yICh2YXIga2V5IGluIF9fX2VjX2V4cG9ydCkge1xuICAgICAgICBpZiAoX19fZWNfZXhwb3J0Lmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgIGV4cG9ydHNba2V5XSA9IF9fX2VjX2V4cG9ydFtrZXldO1xuICAgICAgICB9XG4gICAgfVxufSkoKTsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUxBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUVBOzs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeEtBO0FBQ0E7QUEwS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckZBO0FBdUZBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/echarts.js
