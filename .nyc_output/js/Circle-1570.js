var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");
/**
 * 圆形
 * @module zrender/shape/Circle
 */


var _default = Path.extend({
  type: 'circle',
  shape: {
    cx: 0,
    cy: 0,
    r: 0
  },
  buildPath: function buildPath(ctx, shape, inBundle) {
    // Better stroking in ShapeBundle
    // Always do it may have performence issue ( fill may be 2x more cost)
    if (inBundle) {
      ctx.moveTo(shape.cx + shape.r, shape.cy);
    } // else {
    //     if (ctx.allocate && !ctx.data.length) {
    //         ctx.allocate(ctx.CMD_MEM_SIZE.A);
    //     }
    // }
    // Better stroking in ShapeBundle
    // ctx.moveTo(shape.cx + shape.r, shape.cy);


    ctx.arc(shape.cx, shape.cy, shape.r, 0, Math.PI * 2, true);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9DaXJjbGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL3NoYXBlL0NpcmNsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgUGF0aCA9IHJlcXVpcmUoXCIuLi9QYXRoXCIpO1xuXG4vKipcbiAqIOWchuW9olxuICogQG1vZHVsZSB6cmVuZGVyL3NoYXBlL0NpcmNsZVxuICovXG52YXIgX2RlZmF1bHQgPSBQYXRoLmV4dGVuZCh7XG4gIHR5cGU6ICdjaXJjbGUnLFxuICBzaGFwZToge1xuICAgIGN4OiAwLFxuICAgIGN5OiAwLFxuICAgIHI6IDBcbiAgfSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSwgaW5CdW5kbGUpIHtcbiAgICAvLyBCZXR0ZXIgc3Ryb2tpbmcgaW4gU2hhcGVCdW5kbGVcbiAgICAvLyBBbHdheXMgZG8gaXQgbWF5IGhhdmUgcGVyZm9ybWVuY2UgaXNzdWUgKCBmaWxsIG1heSBiZSAyeCBtb3JlIGNvc3QpXG4gICAgaWYgKGluQnVuZGxlKSB7XG4gICAgICBjdHgubW92ZVRvKHNoYXBlLmN4ICsgc2hhcGUuciwgc2hhcGUuY3kpO1xuICAgIH0gLy8gZWxzZSB7XG4gICAgLy8gICAgIGlmIChjdHguYWxsb2NhdGUgJiYgIWN0eC5kYXRhLmxlbmd0aCkge1xuICAgIC8vICAgICAgICAgY3R4LmFsbG9jYXRlKGN0eC5DTURfTUVNX1NJWkUuQSk7XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG4gICAgLy8gQmV0dGVyIHN0cm9raW5nIGluIFNoYXBlQnVuZGxlXG4gICAgLy8gY3R4Lm1vdmVUbyhzaGFwZS5jeCArIHNoYXBlLnIsIHNoYXBlLmN5KTtcblxuXG4gICAgY3R4LmFyYyhzaGFwZS5jeCwgc2hhcGUuY3ksIHNoYXBlLnIsIDAsIE1hdGguUEkgKiAyLCB0cnVlKTtcbiAgfVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdEJBO0FBQ0E7QUF3QkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Circle.js
