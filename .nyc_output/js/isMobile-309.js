__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

function _iterableToArrayLimit(arr, i) {
  if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) {
    return;
  }

  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
} // MIT License from https://github.com/kaimallea/isMobile


var applePhone = /iPhone/i;
var appleIpod = /iPod/i;
var appleTablet = /iPad/i;
var androidPhone = /\bAndroid(?:.+)Mobile\b/i; // Match 'Android' AND 'Mobile'

var androidTablet = /Android/i;
var amazonPhone = /\bAndroid(?:.+)SD4930UR\b/i;
var amazonTablet = /\bAndroid(?:.+)(?:KF[A-Z]{2,4})\b/i;
var windowsPhone = /Windows Phone/i;
var windowsTablet = /\bWindows(?:.+)ARM\b/i; // Match 'Windows' AND 'ARM'

var otherBlackberry = /BlackBerry/i;
var otherBlackberry10 = /BB10/i;
var otherOpera = /Opera Mini/i;
var otherChrome = /\b(CriOS|Chrome)(?:.+)Mobile/i;
var otherFirefox = /Mobile(?:.+)Firefox\b/i; // Match 'Mobile' AND 'Firefox'

function match(regex, userAgent) {
  return regex.test(userAgent);
}

function isMobile(userAgent) {
  var ua = userAgent || (typeof navigator !== 'undefined' ? navigator.userAgent : ''); // Facebook mobile app's integrated browser adds a bunch of strings that
  // match everything. Strip it out if it exists.

  var tmp = ua.split('[FBAN');

  if (typeof tmp[1] !== 'undefined') {
    var _tmp = tmp;

    var _tmp2 = _slicedToArray(_tmp, 1);

    ua = _tmp2[0];
  } // Twitter mobile app's integrated browser on iPad adds a "Twitter for
  // iPhone" string. Same probably happens on other tablet platforms.
  // This will confuse detection so strip it out if it exists.


  tmp = ua.split('Twitter');

  if (typeof tmp[1] !== 'undefined') {
    var _tmp3 = tmp;

    var _tmp4 = _slicedToArray(_tmp3, 1);

    ua = _tmp4[0];
  }

  var result = {
    apple: {
      phone: match(applePhone, ua) && !match(windowsPhone, ua),
      ipod: match(appleIpod, ua),
      tablet: !match(applePhone, ua) && match(appleTablet, ua) && !match(windowsPhone, ua),
      device: (match(applePhone, ua) || match(appleIpod, ua) || match(appleTablet, ua)) && !match(windowsPhone, ua)
    },
    amazon: {
      phone: match(amazonPhone, ua),
      tablet: !match(amazonPhone, ua) && match(amazonTablet, ua),
      device: match(amazonPhone, ua) || match(amazonTablet, ua)
    },
    android: {
      phone: !match(windowsPhone, ua) && match(amazonPhone, ua) || !match(windowsPhone, ua) && match(androidPhone, ua),
      tablet: !match(windowsPhone, ua) && !match(amazonPhone, ua) && !match(androidPhone, ua) && (match(amazonTablet, ua) || match(androidTablet, ua)),
      device: !match(windowsPhone, ua) && (match(amazonPhone, ua) || match(amazonTablet, ua) || match(androidPhone, ua) || match(androidTablet, ua)) || match(/\bokhttp\b/i, ua)
    },
    windows: {
      phone: match(windowsPhone, ua),
      tablet: match(windowsTablet, ua),
      device: match(windowsPhone, ua) || match(windowsTablet, ua)
    },
    other: {
      blackberry: match(otherBlackberry, ua),
      blackberry10: match(otherBlackberry10, ua),
      opera: match(otherOpera, ua),
      firefox: match(otherFirefox, ua),
      chrome: match(otherChrome, ua),
      device: match(otherBlackberry, ua) || match(otherBlackberry10, ua) || match(otherOpera, ua) || match(otherFirefox, ua) || match(otherChrome, ua)
    },
    // Additional
    any: null,
    phone: null,
    tablet: null
  };
  result.any = result.apple.device || result.android.device || result.windows.device || result.other.device; // excludes 'other' devices and ipods, targeting touchscreen phones

  result.phone = result.apple.phone || result.android.phone || result.windows.phone;
  result.tablet = result.apple.tablet || result.android.tablet || result.windows.tablet;
  return result;
}

var defaultResult = _objectSpread({}, isMobile(), {
  isMobile: isMobile
});

/* harmony default export */ __webpack_exports__["default"] = (defaultResult);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy91dGlscy9pc01vYmlsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLW1lbnUvZXMvdXRpbHMvaXNNb2JpbGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKHNvdXJjZSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKHNvdXJjZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX3NsaWNlZFRvQXJyYXkoYXJyLCBpKSB7IHJldHVybiBfYXJyYXlXaXRoSG9sZXMoYXJyKSB8fCBfaXRlcmFibGVUb0FycmF5TGltaXQoYXJyLCBpKSB8fCBfbm9uSXRlcmFibGVSZXN0KCk7IH1cblxuZnVuY3Rpb24gX25vbkl0ZXJhYmxlUmVzdCgpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2VcIik7IH1cblxuZnVuY3Rpb24gX2l0ZXJhYmxlVG9BcnJheUxpbWl0KGFyciwgaSkgeyBpZiAoIShTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGFycikgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGFycikgPT09IFwiW29iamVjdCBBcmd1bWVudHNdXCIpKSB7IHJldHVybjsgfSB2YXIgX2FyciA9IFtdOyB2YXIgX24gPSB0cnVlOyB2YXIgX2QgPSBmYWxzZTsgdmFyIF9lID0gdW5kZWZpbmVkOyB0cnkgeyBmb3IgKHZhciBfaSA9IGFycltTeW1ib2wuaXRlcmF0b3JdKCksIF9zOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7IF9hcnIucHVzaChfcy52YWx1ZSk7IGlmIChpICYmIF9hcnIubGVuZ3RoID09PSBpKSBicmVhazsgfSB9IGNhdGNoIChlcnIpIHsgX2QgPSB0cnVlOyBfZSA9IGVycjsgfSBmaW5hbGx5IHsgdHJ5IHsgaWYgKCFfbiAmJiBfaVtcInJldHVyblwiXSAhPSBudWxsKSBfaVtcInJldHVyblwiXSgpOyB9IGZpbmFsbHkgeyBpZiAoX2QpIHRocm93IF9lOyB9IH0gcmV0dXJuIF9hcnI7IH1cblxuZnVuY3Rpb24gX2FycmF5V2l0aEhvbGVzKGFycikgeyBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSByZXR1cm4gYXJyOyB9XG5cbi8vIE1JVCBMaWNlbnNlIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2thaW1hbGxlYS9pc01vYmlsZVxudmFyIGFwcGxlUGhvbmUgPSAvaVBob25lL2k7XG52YXIgYXBwbGVJcG9kID0gL2lQb2QvaTtcbnZhciBhcHBsZVRhYmxldCA9IC9pUGFkL2k7XG52YXIgYW5kcm9pZFBob25lID0gL1xcYkFuZHJvaWQoPzouKylNb2JpbGVcXGIvaTsgLy8gTWF0Y2ggJ0FuZHJvaWQnIEFORCAnTW9iaWxlJ1xuXG52YXIgYW5kcm9pZFRhYmxldCA9IC9BbmRyb2lkL2k7XG52YXIgYW1hem9uUGhvbmUgPSAvXFxiQW5kcm9pZCg/Oi4rKVNENDkzMFVSXFxiL2k7XG52YXIgYW1hem9uVGFibGV0ID0gL1xcYkFuZHJvaWQoPzouKykoPzpLRltBLVpdezIsNH0pXFxiL2k7XG52YXIgd2luZG93c1Bob25lID0gL1dpbmRvd3MgUGhvbmUvaTtcbnZhciB3aW5kb3dzVGFibGV0ID0gL1xcYldpbmRvd3MoPzouKylBUk1cXGIvaTsgLy8gTWF0Y2ggJ1dpbmRvd3MnIEFORCAnQVJNJ1xuXG52YXIgb3RoZXJCbGFja2JlcnJ5ID0gL0JsYWNrQmVycnkvaTtcbnZhciBvdGhlckJsYWNrYmVycnkxMCA9IC9CQjEwL2k7XG52YXIgb3RoZXJPcGVyYSA9IC9PcGVyYSBNaW5pL2k7XG52YXIgb3RoZXJDaHJvbWUgPSAvXFxiKENyaU9TfENocm9tZSkoPzouKylNb2JpbGUvaTtcbnZhciBvdGhlckZpcmVmb3ggPSAvTW9iaWxlKD86LispRmlyZWZveFxcYi9pOyAvLyBNYXRjaCAnTW9iaWxlJyBBTkQgJ0ZpcmVmb3gnXG5cbmZ1bmN0aW9uIG1hdGNoKHJlZ2V4LCB1c2VyQWdlbnQpIHtcbiAgcmV0dXJuIHJlZ2V4LnRlc3QodXNlckFnZW50KTtcbn1cblxuZnVuY3Rpb24gaXNNb2JpbGUodXNlckFnZW50KSB7XG4gIHZhciB1YSA9IHVzZXJBZ2VudCB8fCAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgPyBuYXZpZ2F0b3IudXNlckFnZW50IDogJycpOyAvLyBGYWNlYm9vayBtb2JpbGUgYXBwJ3MgaW50ZWdyYXRlZCBicm93c2VyIGFkZHMgYSBidW5jaCBvZiBzdHJpbmdzIHRoYXRcbiAgLy8gbWF0Y2ggZXZlcnl0aGluZy4gU3RyaXAgaXQgb3V0IGlmIGl0IGV4aXN0cy5cblxuICB2YXIgdG1wID0gdWEuc3BsaXQoJ1tGQkFOJyk7XG5cbiAgaWYgKHR5cGVvZiB0bXBbMV0gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgdmFyIF90bXAgPSB0bXA7XG5cbiAgICB2YXIgX3RtcDIgPSBfc2xpY2VkVG9BcnJheShfdG1wLCAxKTtcblxuICAgIHVhID0gX3RtcDJbMF07XG4gIH0gLy8gVHdpdHRlciBtb2JpbGUgYXBwJ3MgaW50ZWdyYXRlZCBicm93c2VyIG9uIGlQYWQgYWRkcyBhIFwiVHdpdHRlciBmb3JcbiAgLy8gaVBob25lXCIgc3RyaW5nLiBTYW1lIHByb2JhYmx5IGhhcHBlbnMgb24gb3RoZXIgdGFibGV0IHBsYXRmb3Jtcy5cbiAgLy8gVGhpcyB3aWxsIGNvbmZ1c2UgZGV0ZWN0aW9uIHNvIHN0cmlwIGl0IG91dCBpZiBpdCBleGlzdHMuXG5cblxuICB0bXAgPSB1YS5zcGxpdCgnVHdpdHRlcicpO1xuXG4gIGlmICh0eXBlb2YgdG1wWzFdICE9PSAndW5kZWZpbmVkJykge1xuICAgIHZhciBfdG1wMyA9IHRtcDtcblxuICAgIHZhciBfdG1wNCA9IF9zbGljZWRUb0FycmF5KF90bXAzLCAxKTtcblxuICAgIHVhID0gX3RtcDRbMF07XG4gIH1cblxuICB2YXIgcmVzdWx0ID0ge1xuICAgIGFwcGxlOiB7XG4gICAgICBwaG9uZTogbWF0Y2goYXBwbGVQaG9uZSwgdWEpICYmICFtYXRjaCh3aW5kb3dzUGhvbmUsIHVhKSxcbiAgICAgIGlwb2Q6IG1hdGNoKGFwcGxlSXBvZCwgdWEpLFxuICAgICAgdGFibGV0OiAhbWF0Y2goYXBwbGVQaG9uZSwgdWEpICYmIG1hdGNoKGFwcGxlVGFibGV0LCB1YSkgJiYgIW1hdGNoKHdpbmRvd3NQaG9uZSwgdWEpLFxuICAgICAgZGV2aWNlOiAobWF0Y2goYXBwbGVQaG9uZSwgdWEpIHx8IG1hdGNoKGFwcGxlSXBvZCwgdWEpIHx8IG1hdGNoKGFwcGxlVGFibGV0LCB1YSkpICYmICFtYXRjaCh3aW5kb3dzUGhvbmUsIHVhKVxuICAgIH0sXG4gICAgYW1hem9uOiB7XG4gICAgICBwaG9uZTogbWF0Y2goYW1hem9uUGhvbmUsIHVhKSxcbiAgICAgIHRhYmxldDogIW1hdGNoKGFtYXpvblBob25lLCB1YSkgJiYgbWF0Y2goYW1hem9uVGFibGV0LCB1YSksXG4gICAgICBkZXZpY2U6IG1hdGNoKGFtYXpvblBob25lLCB1YSkgfHwgbWF0Y2goYW1hem9uVGFibGV0LCB1YSlcbiAgICB9LFxuICAgIGFuZHJvaWQ6IHtcbiAgICAgIHBob25lOiAhbWF0Y2god2luZG93c1Bob25lLCB1YSkgJiYgbWF0Y2goYW1hem9uUGhvbmUsIHVhKSB8fCAhbWF0Y2god2luZG93c1Bob25lLCB1YSkgJiYgbWF0Y2goYW5kcm9pZFBob25lLCB1YSksXG4gICAgICB0YWJsZXQ6ICFtYXRjaCh3aW5kb3dzUGhvbmUsIHVhKSAmJiAhbWF0Y2goYW1hem9uUGhvbmUsIHVhKSAmJiAhbWF0Y2goYW5kcm9pZFBob25lLCB1YSkgJiYgKG1hdGNoKGFtYXpvblRhYmxldCwgdWEpIHx8IG1hdGNoKGFuZHJvaWRUYWJsZXQsIHVhKSksXG4gICAgICBkZXZpY2U6ICFtYXRjaCh3aW5kb3dzUGhvbmUsIHVhKSAmJiAobWF0Y2goYW1hem9uUGhvbmUsIHVhKSB8fCBtYXRjaChhbWF6b25UYWJsZXQsIHVhKSB8fCBtYXRjaChhbmRyb2lkUGhvbmUsIHVhKSB8fCBtYXRjaChhbmRyb2lkVGFibGV0LCB1YSkpIHx8IG1hdGNoKC9cXGJva2h0dHBcXGIvaSwgdWEpXG4gICAgfSxcbiAgICB3aW5kb3dzOiB7XG4gICAgICBwaG9uZTogbWF0Y2god2luZG93c1Bob25lLCB1YSksXG4gICAgICB0YWJsZXQ6IG1hdGNoKHdpbmRvd3NUYWJsZXQsIHVhKSxcbiAgICAgIGRldmljZTogbWF0Y2god2luZG93c1Bob25lLCB1YSkgfHwgbWF0Y2god2luZG93c1RhYmxldCwgdWEpXG4gICAgfSxcbiAgICBvdGhlcjoge1xuICAgICAgYmxhY2tiZXJyeTogbWF0Y2gob3RoZXJCbGFja2JlcnJ5LCB1YSksXG4gICAgICBibGFja2JlcnJ5MTA6IG1hdGNoKG90aGVyQmxhY2tiZXJyeTEwLCB1YSksXG4gICAgICBvcGVyYTogbWF0Y2gob3RoZXJPcGVyYSwgdWEpLFxuICAgICAgZmlyZWZveDogbWF0Y2gob3RoZXJGaXJlZm94LCB1YSksXG4gICAgICBjaHJvbWU6IG1hdGNoKG90aGVyQ2hyb21lLCB1YSksXG4gICAgICBkZXZpY2U6IG1hdGNoKG90aGVyQmxhY2tiZXJyeSwgdWEpIHx8IG1hdGNoKG90aGVyQmxhY2tiZXJyeTEwLCB1YSkgfHwgbWF0Y2gob3RoZXJPcGVyYSwgdWEpIHx8IG1hdGNoKG90aGVyRmlyZWZveCwgdWEpIHx8IG1hdGNoKG90aGVyQ2hyb21lLCB1YSlcbiAgICB9LFxuICAgIC8vIEFkZGl0aW9uYWxcbiAgICBhbnk6IG51bGwsXG4gICAgcGhvbmU6IG51bGwsXG4gICAgdGFibGV0OiBudWxsXG4gIH07XG4gIHJlc3VsdC5hbnkgPSByZXN1bHQuYXBwbGUuZGV2aWNlIHx8IHJlc3VsdC5hbmRyb2lkLmRldmljZSB8fCByZXN1bHQud2luZG93cy5kZXZpY2UgfHwgcmVzdWx0Lm90aGVyLmRldmljZTsgLy8gZXhjbHVkZXMgJ290aGVyJyBkZXZpY2VzIGFuZCBpcG9kcywgdGFyZ2V0aW5nIHRvdWNoc2NyZWVuIHBob25lc1xuXG4gIHJlc3VsdC5waG9uZSA9IHJlc3VsdC5hcHBsZS5waG9uZSB8fCByZXN1bHQuYW5kcm9pZC5waG9uZSB8fCByZXN1bHQud2luZG93cy5waG9uZTtcbiAgcmVzdWx0LnRhYmxldCA9IHJlc3VsdC5hcHBsZS50YWJsZXQgfHwgcmVzdWx0LmFuZHJvaWQudGFibGV0IHx8IHJlc3VsdC53aW5kb3dzLnRhYmxldDtcbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxudmFyIGRlZmF1bHRSZXN1bHQgPSBfb2JqZWN0U3ByZWFkKHt9LCBpc01vYmlsZSgpLCB7XG4gIGlzTW9iaWxlOiBpc01vYmlsZVxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGRlZmF1bHRSZXN1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQWpDQTtBQW1DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-menu/es/utils/isMobile.js
