__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TypeModal; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/mobile-modal/popover-mobile.tsx";





var isMobile = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsMobile();

var TypeModal =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(TypeModal, _React$Component);

  function TypeModal() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, TypeModal);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(TypeModal)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      typeData: null,
      showMsg: "",
      isLoading: false
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(TypeModal, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          labelName = _this$props.labelName,
          childHtml = _this$props.childHtml;
      return isMobile ? _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      }, labelName, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Icon"], {
        onClick: function onClick() {
          return _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_8__["default"].create({
            Component: function Component(_ref) {
              var onCancel = _ref.onCancel,
                  onOk = _ref.onOk;
              return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_9__["MobileProver"], {
                onCancel: onCancel,
                childHtml: childHtml,
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 35
                },
                __self: this
              });
            }
          });
        },
        style: {
          color: "black",
          padding: "5px"
        },
        type: "question-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        },
        __self: this
      })) : _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Popover"], {
        placement: "bottom",
        overlayClassName: "occuClassPopover",
        content: childHtml,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, labelName, "\xA0", _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Icon"], {
        style: {
          color: "black"
        },
        type: "question-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }));
    }
  }]);

  return TypeModal;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Component);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L21vYmlsZS1tb2RhbC9wb3BvdmVyLW1vYmlsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvbW9iaWxlLW1vZGFsL3BvcG92ZXItbW9iaWxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgSWNvbiwgUG9wb3ZlciB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCB7IE1vYmlsZVByb3ZlciB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIHR5cGVEYXRhOiBhbnksXG4gIHNob3dNc2c6IGFueSxcbiAgaXNMb2FkaW5nOiBib29sZWFuLFxufVxuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgY2hpbGRIdG1sOiBhbnk7XG4gIGxhYmVsTmFtZT86IHN0cmluZztcbn1cblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUeXBlTW9kYWwgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQ8SVByb3BzLCBJU3RhdGU+IHtcbiAgc3RhdGU6IElTdGF0ZSA9IHtcbiAgICB0eXBlRGF0YTogbnVsbCxcbiAgICBzaG93TXNnOiBcIlwiLFxuICAgIGlzTG9hZGluZzogZmFsc2UsXG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgbGFiZWxOYW1lLCBjaGlsZEh0bWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIGlzTW9iaWxlID8gPHNwYW4+XG4gICAgICAgIHtsYWJlbE5hbWV9XG4gICAgICAgIDxJY29uIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgTWFzay5jcmVhdGUoe1xuICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPE1vYmlsZVByb3ZlciBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkSHRtbD17Y2hpbGRIdG1sfS8+LFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICApO1xuICAgICAgICB9fSBzdHlsZT17eyBjb2xvcjogXCJibGFja1wiLCBwYWRkaW5nOiBcIjVweFwiIH19IHR5cGU9XCJxdWVzdGlvbi1jaXJjbGVcIi8+XG4gICAgICA8L3NwYW4+IDogPFBvcG92ZXIgcGxhY2VtZW50PVwiYm90dG9tXCIgb3ZlcmxheUNsYXNzTmFtZT1cIm9jY3VDbGFzc1BvcG92ZXJcIiBjb250ZW50PXtjaGlsZEh0bWx9PlxuICAgICAgICB7bGFiZWxOYW1lfVxuICAgICAgICAmbmJzcDtcbiAgICAgICAgPEljb24gc3R5bGU9e3sgY29sb3I6IFwiYmxhY2tcIiB9fSB0eXBlPVwicXVlc3Rpb24tY2lyY2xlXCIvPlxuICAgICAgPC9Qb3BvdmVyPlxuICAgICk7XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWFBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBOzs7Ozs7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7O0FBMUJBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/mobile-modal/popover-mobile.tsx
