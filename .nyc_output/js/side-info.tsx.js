__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SideInfo; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_master_line_list__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk/master/line-list */ "./src/app/desk/master/line-list.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/phs/components/side-info.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__["default"])(["\n  padding-right: 20px;\n  ", "\n  .summary-btn {\n    font-size: 16px;\n  }\n  .info-title {\n    font-size: 14px;\n    color: gray;\n    text-transform: uppercase;\n   }\n   .info-content {\n    font-size: 16px;\n    color: rgb(0, 0, .85);\n   }\n   .info-bottom {\n    .info-icon { \n      border: 1px solid;\n      border-radius: 50%;\n      padding: 3px;\n      cursor: pointer;\n      &:first-child {\n        margin-right:10px;\n      }\n      &:hover {\n        color: ", ";\n      }\n    }\n   }\n  .side-info-header {\n     padding: 20px 0;\n     text-align: center;\n     border-bottom: 1px dashed #e8e8e8;\n     &.mobile-side {\n      &:first-child {\n        padding: 0 0 20px 0;\n      }\n     }\n  }\n  .side-info-time {\n    padding: 30px 0 0 40px;\n    ", "\n  }\n  .ant-timeline-item-content {\n    font-size: 18px;\n    color: rgb(0, 0, .85);\n\n  }\n  .break-down {\n    p {\n      font-size: 12px;\n      line-height: 20px;\n      display: flex;\n      justify-content: space-between;\n      align-items: center;\n      label {\n        line-height: 15px;\n        display: inline-block;\n        word-break: keep-all;\n        text-align: left;\n        padding: 3px 0;\n      }\n      span {\n        display: inline-block;\n        white-space: nowrap;\n      }\n    }\n  }\n  .details {\n    margin-top: 0;\n    &.calc {\n      text-align: left;\n      .ant-collapse .ant-collapse-header {\n        margin-top: -15px;\n      }\n    }\n    .ant-collapse-content{\n      ", "\n    }\n    .ant-collapse {\n      margin-top: -10px;\n    }\n    .ant-collapse .ant-collapse-header {\n      justify-content: center;\n      position: absolute;\n      line-heigh: 34px;\n      margin-top: -19px;\n      right: 15px;\n      color: rgba(0,0,0,0.85);\n      font-size: 14px;\n      .ant-collapse-arrow {\n        left: 74px !important;\n        ", "\n      }\n    }\n    .ant-collapse-content > .ant-collapse-content-box {\n      padding: 16px 0 0 0;\n    }\n  }\n  .show-premium {\n    text-align: left;\n    &.mobile-premium {\n      padding-left: 20px;\n    }\n  }\n  .break-downs {\n    .p-title {\n      padding-bottom: 10px;\n      padding-top: 10px;\n    }\n    &:first-child {\n      .p-title {\n         padding-top: 0;\n      }\n    }\n    &.mobile-downs {\n      padding-left: 20px;\n    }\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_5__["default"])(["\n  width: 300px;\n  max-height: 500px;\n  font-size: 12px;\n  padding-right: 8px;\n  overflow: auto;\n  .row-voucher-item {\n     padding-bottom:5px;\n     border-bottom: 1px solid #ebebeb;\n    &:not(:first-child){\n      margin-top:10px;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}











var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_11__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile();
var VoucherInfoDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject());
var SideInfoStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject2(), isMobile && "padding-right: 0;text-align:center;", COLOR_A, isMobile && "text-align: start", isMobile && "border-top: 0;", isMobile && "right: -14px;left: auto !important");

var SideInfo =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(SideInfo, _Component);

  function SideInfo() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, SideInfo);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(SideInfo)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      visible: false,
      vouchers: []
    };

    _this.getPremium = function () {
      var _this$props = _this.props,
          premiumId = _this$props.premiumId,
          model = _this$props.model;

      var currencyCode = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, _this.getProp("premCurrencyCode"));

      var policy = {
        openEnd: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, _this.getProp("openEnd")),
        recurringPayment: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, _this.getProp("recurringPayment")),
        policyPremium: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, premiumId || "cartPremium.totalPremium"),
        currencyCode: currencyCode,
        premiumDesc: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, _this.getProp("premiumDesc"))
      };
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          marginRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].renderPolicyPremium(policy)));
    };

    _this.showSummary = function () {
      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_14__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Modal"], {
            width: "100%",
            maskClosable: false,
            visible: true,
            centered: true,
            title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Summary").thai("Summary").getMessage(),
            onCancel: onCancel,
            onOk: onOk,
            footer: null,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 219
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Row"], {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 231
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
            sm: 5,
            xs: 24,
            style: {
              borderRight: "1px solid #e8e8e8"
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 232
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(SideInfoStyle, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 233
            },
            __self: this
          }, _this.renderSideInfo()))));
        },
        onCancel: function onCancel() {}
      });
    };

    _this.handleVisibleChange = function (visible) {
      if (visible) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].CAMPAIGN_QUERY, {
          pageSize: 100,
          pageIndex: 1,
          statuses: ["ISSUED"],
          campaignType: _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].CAMPAIGN_TYPE.VOUCHER
        }, {
          loading: true
        }).then(function (res) {
          var data = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "body.respData.items") || [];

          _this.setState({
            visible: visible,
            vouchers: data
          });
        });
      } else {
        _this.setState({
          visible: visible
        });
      }
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(SideInfo, [{
    key: "getPolicyholderProp",
    value: function getPolicyholderProp(propName) {
      var _this$props2 = this.props,
          endoFixed = _this$props2.endoFixed,
          dataFixed = _this$props2.dataFixed;

      if (endoFixed) {
        return "".concat(endoFixed, ".policyholder.").concat(propName);
      } else if (dataFixed) {
        return "".concat(dataFixed, ".ext.policyholder.").concat(propName);
      }

      return "ext.policyholder.".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      var dataFixed = this.props.dataFixed;

      if (dataFixed) {
        return "".concat(dataFixed, ".").concat(propName);
      }

      return propName;
    }
  }, {
    key: "handleVoucher",
    value: function handleVoucher(campaignCode) {
      this.setState({
        visible: false
      });
      this.props.onUseVoucher(campaignCode);
    }
  }, {
    key: "showBreakdown",
    value: function showBreakdown() {
      var _this$props3 = this.props,
          model = _this$props3.model,
          premiumId = _this$props3.premiumId,
          type = _this$props3.type;
      var currencySymbol = model.currencySymbol;
      var breakdowns = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "".concat(premiumId || "cartPremium", ".breakdowns"), []) || [];

      if (type === "policy-view") {
        return this.showBreakdownPolicyView();
      }

      console.log(breakdowns, "breakdowns");
      return breakdowns.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
          className: "break-downs ".concat(isMobile ? "mobile-downs" : ""),
          key: item.productCode,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 279
          },
          __self: this
        }, breakdowns.length > 1 && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
          className: "p-title",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 281
          },
          __self: this
        }, item.productName, item.planName ? " - " : "", " ", item.planName), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
          className: "break-down",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 282
          },
          __self: this
        }, (item.breakdowns || []).map(function (every, index) {
          return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
            key: index,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 283
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("label", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 284
            },
            __self: this
          }, every.name), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 285
            },
            __self: this
          }, _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].showAmount(every.value, currencySymbol)));
        })));
      });
    }
  }, {
    key: "showBreakdownPolicyView",
    value: function showBreakdownPolicyView() {
      var _this$props4 = this.props,
          model = _this$props4.model,
          premiumId = _this$props4.premiumId;
      var currencySymbol = model.currencySymbol;
      var breakdowns = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "".concat(premiumId || "cartPremium", ".breakdowns"), []) || [];
      return breakdowns.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
          className: "break-downs ".concat(isMobile ? "mobile-downs" : ""),
          key: item.type,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 297
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
          className: "break-down",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 298
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 299
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("label", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 300
          },
          __self: this
        }, item.name), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 301
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].showAmount(item.value, currencySymbol)))));
      });
    }
  }, {
    key: "renderBreakdown",
    value: function renderBreakdown() {
      var _this$props5 = this.props,
          model = _this$props5.model,
          isShowPremium = _this$props5.isShowPremium,
          premiumId = _this$props5.premiumId;
      var breakdowns = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "".concat(premiumId || "cartPremium", ".breakdowns"), []) || [];
      return breakdowns.length > 0 ? react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "details ".concat(isShowPremium ? "" : "calc"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "show-premium ".concat(isMobile ? "mobile-premium" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 312
        },
        __self: this
      }, this.getPremium(), this.props.renderBtn || ""), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NCollapse"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 316
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("breakdown").thai("breakdown").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 317
        },
        __self: this
      }, this.showBreakdown()))) : react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, this.getPremium(), this.props.renderBtn || "");
    }
  }, {
    key: "renderWarningMessage",
    value: function renderWarningMessage() {
      var locked = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "policy.promo.campaignLockResult.success");

      var messages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "policy.promo.campaignLockResult.message") || "";
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, !locked && !!messages && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        style: {
          textAlign: "left",
          fontSize: 12,
          marginTop: 10,
          lineHeight: "20px",
          color: "red",
          overflowWrap: "break-word"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 333
        },
        __self: this
      }, messages));
    }
  }, {
    key: "renderPromo",
    value: function renderPromo() {
      var _this2 = this;

      var vouchers = this.state.vouchers;

      var locked = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "policy.promo.campaignLockResult.success");

      var messages = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, "policy.promo.campaignLockResult.message");

      var promoCode = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.model, this.getProp("promo.promoCode"), "") || "";
      var voucherContent = vouchers.map(function (item, i) {
        var expDate = item.expDate;
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Row"], {
          key: i,
          className: "row-voucher-item",
          type: "flex",
          justify: "space-between",
          align: "middle",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 351
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
          span: 10,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 352
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
          style: {
            overflowWrap: "break-word"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 353
          },
          __self: this
        }, item.campaignCode, " - ", item.campaignName), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
          style: {
            overflowWrap: "break-word"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 356
          },
          __self: this
        }, expDate ? _common__WEBPACK_IMPORTED_MODULE_12__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_12__["DateUtils"].toDate(expDate)) : "")), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
          span: 7,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 360
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
          style: {
            overflowWrap: "break-word",
            textAlign: "end"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 361
          },
          __self: this
        }, promoCode !== item.campaignCode && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
          onClick: function onClick() {
            return _this2.handleVoucher(item.campaignCode);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 367
          },
          __self: this
        }, "Use"), promoCode === item.campaignCode && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
          style: {
            color: "rgba(0, 0, 0, 0.25)",
            cursor: "not-allowed",
            pointerEvents: "none",
            marginRight: 5
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 372
          },
          __self: this
        }, "Used"), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
          onClick: function onClick() {
            return _this2.handleVoucher("");
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 378
          },
          __self: this
        }, "Cancel")))));
      });
      var content = react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(VoucherInfoDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 385
        },
        __self: this
      }, !lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(voucherContent) ? voucherContent : react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        style: {
          textAlign: "center"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 386
        },
        __self: this
      }, "No Vouchers"));
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(promoCode) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Popover"], {
        visible: this.state.visible,
        onVisibleChange: this.handleVisibleChange,
        placement: "right",
        title: "Vouchers Available".toUpperCase(),
        content: content,
        trigger: "click",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 394
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
        className: "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 401
        },
        __self: this
      }, "apply promo")), !lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(promoCode) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, !locked && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        style: {
          textAlign: "left",
          fontSize: 12,
          marginTop: 10,
          lineHeight: "20px",
          color: "red",
          overflowWrap: "break-word"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 407
        },
        __self: this
      }, messages), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
        className: "button",
        onClick: function onClick() {
          return _this2.handleVoucher("");
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 415
        },
        __self: this
      }, "remove")));
    }
  }, {
    key: "renderPlan",
    value: function renderPlan() {
      var model = this.props.model;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 425
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("planName")));
    }
  }, {
    key: "renderMaNo",
    value: function renderMaNo() {
      var model = this.props.model;

      var maId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("maId"));

      if (maId && lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("maNo")) && lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("maName"))) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
          className: "info-content",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 438
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
          onClick: function onClick() {
            window.open("/master/view/".concat(maId));
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 439
          },
          __self: this
        }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("maNo")), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("maName"))));
      }

      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 446
        },
        __self: this
      });
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var list = this.props.lineList || _desk_master_line_list__WEBPACK_IMPORTED_MODULE_10__["LineList"];
      var _this$props6 = this.props,
          activeStep = _this$props6.activeStep,
          model = _this$props6.model,
          showList = _this$props6.showList,
          onUseVoucher = _this$props6.onUseVoucher;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_6___default.a.Fragment, null, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("quoteNo")) ? react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header ".concat(isMobile ? "mobile-side" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 454
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 455
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("quoteNo"))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 456
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("productCode")), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("productName"))), this.renderPlan(), this.renderBreakdown(), this.renderMaNo(), this.renderWarningMessage()) : lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("endoNo")) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header ".concat(isMobile ? "mobile-side" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 465
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 466
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("endoNo"))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 467
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("productCode")), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("productName"))), this.renderPlan(), this.getPremium()), lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("name")) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header ".concat(isMobile ? "mobile-side" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 475
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 476
        },
        __self: this
      }, "Customer"), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 477
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("name"))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-bottom",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 478
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: "".concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("mobileNationCode"), "") || "", " ").concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("mobile"), "") || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 479
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "phone",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 482
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("email"), "") || "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 484
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "mail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 485
        },
        __self: this
      })))), lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("scName")) && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-header ".concat(isMobile ? "mobile-side" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 491
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 492
        },
        __self: this
      }, "CHANNEL"), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-content",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 493
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("scCode")), " - ", lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getProp("scName"))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("p", {
        className: "info-bottom",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 495
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: "".concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("mobileNationCode"), "") || "", " ").concat(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("mobile"), "") || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "phone",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 499
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Tooltip"], {
        placement: "top",
        title: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, this.getPolicyholderProp("email"), "") || "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 501
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
        className: "info-icon",
        type: "mail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 502
        },
        __self: this
      })))), showList && react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        className: "side-info-time",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 507
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Timeline"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 508
        },
        __self: this
      }, list.map(function (item, i) {
        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Timeline"].Item, {
          dot: react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Icon"], {
            type: "minus-circle",
            style: {
              fontSize: 16
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 513
            },
            __self: this
          }),
          key: i,
          color: activeStep === item ? "green" : "gray",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 512
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
          style: {
            color: activeStep === item ? "green" : "",
            borderBottom: activeStep === item ? "1px solid green" : "",
            fontWeight: activeStep === item ? 600 : 300
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 515
          },
          __self: this
        }, item));
      }))));
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      if (_common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsInApp()) return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 530
        },
        __self: this
      });
      var list = this.props.lineList || _desk_master_line_list__WEBPACK_IMPORTED_MODULE_10__["LineList"];
      var _this$props7 = this.props,
          activeStep = _this$props7.activeStep,
          model = _this$props7.model,
          showList = _this$props7.showList,
          onUseVoucher = _this$props7.onUseVoucher;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Col"], {
        sm: 5,
        xs: 24,
        style: {
          borderRight: "1px solid #e8e8e8"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 534
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(SideInfoStyle, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 535
        },
        __self: this
      }, isMobile ? react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("a", {
        onClick: function onClick() {
          _this3.showSummary();
        },
        className: "summary-btn",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 537
        },
        __self: this
      }, "Summary") : this.renderSideInfo()));
    }
  }]);

  return SideInfo;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

SideInfo.defaultProps = {
  showList: true,
  isShowPremium: true
};

;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm8udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm8udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IENvbCwgSWNvbiwgTW9kYWwsIFBvcG92ZXIsIFJvdywgVGltZWxpbmUsIFRvb2x0aXAgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBMaW5lTGlzdCB9IGZyb20gXCJAZGVzay9tYXN0ZXIvbGluZS1saXN0XCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIENvbnN0cywgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTkNvbGxhcHNlLCBOUGFuZWwgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCBNYXNrIGZyb20gXCJAZGVzay1jb21wb25lbnQvY3JlYXRlLWRpYWxvZ1wiO1xuXG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5cbmNvbnN0IFZvdWNoZXJJbmZvRGl2ID0gU3R5bGVkLmRpdmBcbiAgd2lkdGg6IDMwMHB4O1xuICBtYXgtaGVpZ2h0OiA1MDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBwYWRkaW5nLXJpZ2h0OiA4cHg7XG4gIG92ZXJmbG93OiBhdXRvO1xuICAucm93LXZvdWNoZXItaXRlbSB7XG4gICAgIHBhZGRpbmctYm90dG9tOjVweDtcbiAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgJjpub3QoOmZpcnN0LWNoaWxkKXtcbiAgICAgIG1hcmdpbi10b3A6MTBweDtcbiAgICB9XG4gIH1cbmA7XG5cbmludGVyZmFjZSBJUHJvcHMge1xuICBsaW5lTGlzdD86IGFueSxcbiAgbW9kZWw6IGFueSxcbiAgYWN0aXZlU3RlcDogYW55LFxuICBzaG93TGlzdD86IGJvb2xlYW4sXG4gIGVuZG9GaXhlZD86IGFueSxcbiAgcHJlbWl1bUlkPzogYW55LFxuICBkYXRhRml4ZWQ/OiBhbnksXG4gIG9uVXNlVm91Y2hlcj86IGFueSxcbiAgcmVuZGVyQnRuPzogYW55LFxuICBpc1Nob3dQcmVtaXVtPzogYm9vbGVhbixcbiAgdHlwZT86IGFueTtcbn1cblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIHZpc2libGU6IGJvb2xlYW4sXG4gIHZvdWNoZXJzOiBhbnlcbn1cblxuY29uc3QgU2lkZUluZm9TdHlsZSA9IFN0eWxlZC5kaXZgXG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICR7aXNNb2JpbGUgJiYgXCJwYWRkaW5nLXJpZ2h0OiAwO3RleHQtYWxpZ246Y2VudGVyO1wifVxuICAuc3VtbWFyeS1idG4ge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuICAuaW5mby10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiBncmF5O1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICB9XG4gICAuaW5mby1jb250ZW50IHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgY29sb3I6IHJnYigwLCAwLCAuODUpO1xuICAgfVxuICAgLmluZm8tYm90dG9tIHtcbiAgICAuaW5mby1pY29uIHsgXG4gICAgICBib3JkZXI6IDFweCBzb2xpZDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6MTBweDtcbiAgICAgIH1cbiAgICAgICY6aG92ZXIge1xuICAgICAgICBjb2xvcjogJHtDT0xPUl9BfTtcbiAgICAgIH1cbiAgICB9XG4gICB9XG4gIC5zaWRlLWluZm8taGVhZGVyIHtcbiAgICAgcGFkZGluZzogMjBweCAwO1xuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgI2U4ZThlODtcbiAgICAgJi5tb2JpbGUtc2lkZSB7XG4gICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgcGFkZGluZzogMCAwIDIwcHggMDtcbiAgICAgIH1cbiAgICAgfVxuICB9XG4gIC5zaWRlLWluZm8tdGltZSB7XG4gICAgcGFkZGluZzogMzBweCAwIDAgNDBweDtcbiAgICAke2lzTW9iaWxlICYmIFwidGV4dC1hbGlnbjogc3RhcnRcIn1cbiAgfVxuICAuYW50LXRpbWVsaW5lLWl0ZW0tY29udGVudCB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGNvbG9yOiByZ2IoMCwgMCwgLjg1KTtcblxuICB9XG4gIC5icmVhay1kb3duIHtcbiAgICBwIHtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBsYWJlbCB7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHdvcmQtYnJlYWs6IGtlZXAtYWxsO1xuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICBwYWRkaW5nOiAzcHggMDtcbiAgICAgIH1cbiAgICAgIHNwYW4ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC5kZXRhaWxzIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgICYuY2FsYyB7XG4gICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgLmFudC1jb2xsYXBzZSAuYW50LWNvbGxhcHNlLWhlYWRlciB7XG4gICAgICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgICAgfVxuICAgIH1cbiAgICAuYW50LWNvbGxhcHNlLWNvbnRlbnR7XG4gICAgICAke2lzTW9iaWxlICYmIFwiYm9yZGVyLXRvcDogMDtcIn1cbiAgICB9XG4gICAgLmFudC1jb2xsYXBzZSB7XG4gICAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICB9XG4gICAgLmFudC1jb2xsYXBzZSAuYW50LWNvbGxhcHNlLWhlYWRlciB7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxpbmUtaGVpZ2g6IDM0cHg7XG4gICAgICBtYXJnaW4tdG9wOiAtMTlweDtcbiAgICAgIHJpZ2h0OiAxNXB4O1xuICAgICAgY29sb3I6IHJnYmEoMCwwLDAsMC44NSk7XG4gICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAuYW50LWNvbGxhcHNlLWFycm93IHtcbiAgICAgICAgbGVmdDogNzRweCAhaW1wb3J0YW50O1xuICAgICAgICAke2lzTW9iaWxlICYmIFwicmlnaHQ6IC0xNHB4O2xlZnQ6IGF1dG8gIWltcG9ydGFudFwifVxuICAgICAgfVxuICAgIH1cbiAgICAuYW50LWNvbGxhcHNlLWNvbnRlbnQgPiAuYW50LWNvbGxhcHNlLWNvbnRlbnQtYm94IHtcbiAgICAgIHBhZGRpbmc6IDE2cHggMCAwIDA7XG4gICAgfVxuICB9XG4gIC5zaG93LXByZW1pdW0ge1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgJi5tb2JpbGUtcHJlbWl1bSB7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgfVxuICB9XG4gIC5icmVhay1kb3ducyB7XG4gICAgLnAtdGl0bGUge1xuICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICB9XG4gICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAucC10aXRsZSB7XG4gICAgICAgICBwYWRkaW5nLXRvcDogMDtcbiAgICAgIH1cbiAgICB9XG4gICAgJi5tb2JpbGUtZG93bnMge1xuICAgICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIH1cbiAgfVxuYDtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2lkZUluZm8gZXh0ZW5kcyBDb21wb25lbnQ8SVByb3BzLCBJU3RhdGU+IHtcbiAgc3RhdGU6IElTdGF0ZSA9IHtcbiAgICB2aXNpYmxlOiBmYWxzZSxcbiAgICB2b3VjaGVyczogW10sXG4gIH07XG5cbiAgZ2V0UG9saWN5aG9sZGVyUHJvcChwcm9wTmFtZTogYW55KSB7XG4gICAgY29uc3QgeyBlbmRvRml4ZWQsIGRhdGFGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoZW5kb0ZpeGVkKSB7XG4gICAgICByZXR1cm4gYCR7ZW5kb0ZpeGVkfS5wb2xpY3lob2xkZXIuJHtwcm9wTmFtZX1gO1xuICAgIH0gZWxzZSBpZiAoZGF0YUZpeGVkKSB7XG4gICAgICByZXR1cm4gYCR7ZGF0YUZpeGVkfS5leHQucG9saWN5aG9sZGVyLiR7cHJvcE5hbWV9YDtcbiAgICB9XG4gICAgcmV0dXJuIGBleHQucG9saWN5aG9sZGVyLiR7cHJvcE5hbWV9YDtcbiAgfVxuXG4gIGdldFByb3AocHJvcE5hbWU6IGFueSkge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChkYXRhRml4ZWQpIHtcbiAgICAgIHJldHVybiBgJHtkYXRhRml4ZWR9LiR7cHJvcE5hbWV9YDtcbiAgICB9XG4gICAgcmV0dXJuIHByb3BOYW1lO1xuICB9XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICBzaG93TGlzdDogdHJ1ZSxcbiAgICBpc1Nob3dQcmVtaXVtOiB0cnVlLFxuICB9O1xuXG4gIGdldFByZW1pdW0gPSAoKSA9PiB7XG4gICAgY29uc3QgeyBwcmVtaXVtSWQsIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGN1cnJlbmN5Q29kZSA9IF8uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJwcmVtQ3VycmVuY3lDb2RlXCIpKTtcbiAgICBsZXQgcG9saWN5ID0ge1xuICAgICAgb3BlbkVuZDogXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcIm9wZW5FbmRcIikpLFxuICAgICAgcmVjdXJyaW5nUGF5bWVudDogXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInJlY3VycmluZ1BheW1lbnRcIikpLFxuICAgICAgcG9saWN5UHJlbWl1bTogXy5nZXQobW9kZWwsIChwcmVtaXVtSWQgfHwgXCJjYXJ0UHJlbWl1bS50b3RhbFByZW1pdW1cIikpLFxuICAgICAgY3VycmVuY3lDb2RlLFxuICAgICAgcHJlbWl1bURlc2M6IF8uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJwcmVtaXVtRGVzY1wiKSksXG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICA8c3Bhbj5cbiAgICAgICAgPHNwYW4gc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IFwiNXB4XCIgfX0+e1V0aWxzLnJlbmRlclBvbGljeVByZW1pdW0ocG9saWN5KX08L3NwYW4+XG4gICAgICA8L3NwYW4+XG4gICAgKTtcbiAgfTtcblxuICBzaG93U3VtbWFyeSA9ICgpID0+IHtcbiAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gKFxuICAgICAgICA8TW9kYWxcbiAgICAgICAgICB3aWR0aD17XCIxMDAlXCJ9XG4gICAgICAgICAgbWFza0Nsb3NhYmxlPXtmYWxzZX1cbiAgICAgICAgICB2aXNpYmxlPXt0cnVlfVxuICAgICAgICAgIGNlbnRlcmVkPXt0cnVlfVxuICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIlN1bW1hcnlcIilcbiAgICAgICAgICAgIC50aGFpKFwiU3VtbWFyeVwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgICAgb25Paz17b25Pa31cbiAgICAgICAgICBmb290ZXI9e251bGx9XG4gICAgICAgID5cbiAgICAgICAgICA8Um93PlxuICAgICAgICAgICAgPENvbCBzbT17NX0geHM9ezI0fSBzdHlsZT17eyBib3JkZXJSaWdodDogXCIxcHggc29saWQgI2U4ZThlOFwiIH19PlxuICAgICAgICAgICAgICA8U2lkZUluZm9TdHlsZT5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJTaWRlSW5mbygpfVxuICAgICAgICAgICAgICA8L1NpZGVJbmZvU3R5bGU+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgPC9Nb2RhbD5cbiAgICAgICksXG4gICAgICBvbkNhbmNlbDogKCkgPT4ge1xuICAgICAgfSxcbiAgICB9KTtcbiAgfTtcblxuXG4gIGhhbmRsZVZvdWNoZXIoY2FtcGFpZ25Db2RlOiBhbnkpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHZpc2libGU6IGZhbHNlLFxuICAgIH0pO1xuICAgIHRoaXMucHJvcHMub25Vc2VWb3VjaGVyKGNhbXBhaWduQ29kZSk7XG4gIH1cblxuICBoYW5kbGVWaXNpYmxlQ2hhbmdlID0gKHZpc2libGU6IGJvb2xlYW4pID0+IHtcbiAgICBpZiAodmlzaWJsZSkge1xuICAgICAgQWpheC5wb3N0KEFwaXMuQ0FNUEFJR05fUVVFUlksXG4gICAgICAgIHsgcGFnZVNpemU6IDEwMCwgcGFnZUluZGV4OiAxLCBzdGF0dXNlczogW1wiSVNTVUVEXCJdLCBjYW1wYWlnblR5cGU6IENvbnN0cy5DQU1QQUlHTl9UWVBFLlZPVUNIRVIgfSxcbiAgICAgICAgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAgIC50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgICAgIGNvbnN0IGRhdGEgPSBfLmdldChyZXMsIFwiYm9keS5yZXNwRGF0YS5pdGVtc1wiKSB8fCBbXTtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIHZpc2libGUsXG4gICAgICAgICAgICB2b3VjaGVyczogZGF0YSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyB2aXNpYmxlIH0pO1xuICAgIH1cbiAgfTtcblxuICBzaG93QnJlYWtkb3duKCkge1xuICAgIGNvbnN0IHsgbW9kZWwsIHByZW1pdW1JZCwgdHlwZSB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGN1cnJlbmN5U3ltYm9sIH0gPSBtb2RlbDtcbiAgICBjb25zdCBicmVha2Rvd25zID0gXy5nZXQobW9kZWwsIGAke3ByZW1pdW1JZCB8fCBcImNhcnRQcmVtaXVtXCJ9LmJyZWFrZG93bnNgLCBbXSkgfHwgW107XG4gICAgaWYgKHR5cGUgPT09IFwicG9saWN5LXZpZXdcIikge1xuICAgICAgcmV0dXJuIHRoaXMuc2hvd0JyZWFrZG93blBvbGljeVZpZXcoKTtcbiAgICB9XG4gICAgY29uc29sZS5sb2coYnJlYWtkb3ducywgXCJicmVha2Rvd25zXCIpO1xuICAgIHJldHVybiBicmVha2Rvd25zLm1hcCgoaXRlbTogYW55KSA9PiB7XG4gICAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9e2BicmVhay1kb3ducyAke2lzTW9iaWxlID8gXCJtb2JpbGUtZG93bnNcIiA6IFwiXCJ9YH0ga2V5PXtpdGVtLnByb2R1Y3RDb2RlfT5cbiAgICAgICAge2JyZWFrZG93bnMubGVuZ3RoID4gMSAmJlxuICAgICAgICA8cCBjbGFzc05hbWU9e1wicC10aXRsZVwifT57aXRlbS5wcm9kdWN0TmFtZX17aXRlbS5wbGFuTmFtZSA/IFwiIC0gXCIgOiBcIlwifSB7aXRlbS5wbGFuTmFtZX08L3A+fVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJicmVhay1kb3duXCJ9PnsoaXRlbS5icmVha2Rvd25zIHx8IFtdKS5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICByZXR1cm4gPHAga2V5PXtpbmRleH0+XG4gICAgICAgICAgICA8bGFiZWw+e2V2ZXJ5Lm5hbWV9PC9sYWJlbD5cbiAgICAgICAgICAgIDxzcGFuPntVdGlscy5zaG93QW1vdW50KGV2ZXJ5LnZhbHVlLCBjdXJyZW5jeVN5bWJvbCl9PC9zcGFuPlxuICAgICAgICAgIDwvcD47XG4gICAgICAgIH0pfTwvZGl2PlxuICAgICAgPC9kaXY+O1xuICAgIH0pO1xuICB9O1xuXG4gIHNob3dCcmVha2Rvd25Qb2xpY3lWaWV3KCkge1xuICAgIGNvbnN0IHsgbW9kZWwsIHByZW1pdW1JZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGN1cnJlbmN5U3ltYm9sIH0gPSBtb2RlbDtcbiAgICBjb25zdCBicmVha2Rvd25zID0gXy5nZXQobW9kZWwsIGAke3ByZW1pdW1JZCB8fCBcImNhcnRQcmVtaXVtXCJ9LmJyZWFrZG93bnNgLCBbXSkgfHwgW107XG4gICAgcmV0dXJuIGJyZWFrZG93bnMubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17YGJyZWFrLWRvd25zICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1kb3duc1wiIDogXCJcIn1gfSBrZXk9e2l0ZW0udHlwZX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcImJyZWFrLWRvd25cIn0+XG4gICAgICAgICAgPHA+XG4gICAgICAgICAgICA8bGFiZWw+e2l0ZW0ubmFtZX08L2xhYmVsPlxuICAgICAgICAgICAgPHNwYW4+e1V0aWxzLnNob3dBbW91bnQoaXRlbS52YWx1ZSwgY3VycmVuY3lTeW1ib2wpfTwvc3Bhbj5cbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+O1xuICAgIH0pO1xuICB9O1xuXG4gIHJlbmRlckJyZWFrZG93bigpIHtcbiAgICBjb25zdCB7IG1vZGVsLCBpc1Nob3dQcmVtaXVtLCBwcmVtaXVtSWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgYnJlYWtkb3ducyA9IF8uZ2V0KG1vZGVsLCBgJHtwcmVtaXVtSWQgfHwgXCJjYXJ0UHJlbWl1bVwifS5icmVha2Rvd25zYCwgW10pIHx8IFtdO1xuICAgIHJldHVybiBicmVha2Rvd25zLmxlbmd0aCA+IDAgPyA8ZGl2IGNsYXNzTmFtZT17YGRldGFpbHMgJHtpc1Nob3dQcmVtaXVtID8gXCJcIiA6IFwiY2FsY1wifWB9PlxuICAgICAgPHAgY2xhc3NOYW1lPXtgc2hvdy1wcmVtaXVtICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1wcmVtaXVtXCIgOiBcIlwifWB9PlxuICAgICAgICB7dGhpcy5nZXRQcmVtaXVtKCl9XG4gICAgICAgIHsodGhpcy5wcm9wcy5yZW5kZXJCdG4gfHwgXCJcIil9XG4gICAgICA8L3A+XG4gICAgICA8TkNvbGxhcHNlPlxuICAgICAgICA8TlBhbmVsXG4gICAgICAgICAga2V5PVwiMVwiXG4gICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcImJyZWFrZG93blwiKVxuICAgICAgICAgICAgLnRoYWkoXCJicmVha2Rvd25cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5zaG93QnJlYWtkb3duKCl9XG4gICAgICAgIDwvTlBhbmVsPlxuICAgICAgPC9OQ29sbGFwc2U+XG4gICAgPC9kaXY+IDogPD57dGhpcy5nZXRQcmVtaXVtKCl9eyh0aGlzLnByb3BzLnJlbmRlckJ0biB8fCBcIlwiKX08Lz47XG4gIH1cblxuICByZW5kZXJXYXJuaW5nTWVzc2FnZSgpIHtcbiAgICBjb25zdCBsb2NrZWQgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBcInBvbGljeS5wcm9tby5jYW1wYWlnbkxvY2tSZXN1bHQuc3VjY2Vzc1wiKTtcbiAgICBjb25zdCBtZXNzYWdlcyA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwicG9saWN5LnByb21vLmNhbXBhaWduTG9ja1Jlc3VsdC5tZXNzYWdlXCIpIHx8IFwiXCI7XG4gICAgcmV0dXJuIDw+XG4gICAgICB7IWxvY2tlZCAmJiAhIW1lc3NhZ2VzICYmIDxwIHN0eWxlPXt7XG4gICAgICAgIHRleHRBbGlnbjogXCJsZWZ0XCIsXG4gICAgICAgIGZvbnRTaXplOiAxMixcbiAgICAgICAgbWFyZ2luVG9wOiAxMCxcbiAgICAgICAgbGluZUhlaWdodDogXCIyMHB4XCIsXG4gICAgICAgIGNvbG9yOiBcInJlZFwiLFxuICAgICAgICBvdmVyZmxvd1dyYXA6IFwiYnJlYWstd29yZFwiLFxuICAgICAgfX0+e21lc3NhZ2VzfTwvcD59XG4gICAgPC8+O1xuICB9XG5cbiAgcmVuZGVyUHJvbW8oKSB7XG4gICAgY29uc3QgeyB2b3VjaGVycyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBsb2NrZWQgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBcInBvbGljeS5wcm9tby5jYW1wYWlnbkxvY2tSZXN1bHQuc3VjY2Vzc1wiKTtcbiAgICBjb25zdCBtZXNzYWdlcyA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwicG9saWN5LnByb21vLmNhbXBhaWduTG9ja1Jlc3VsdC5tZXNzYWdlXCIpO1xuICAgIGNvbnN0IHByb21vQ29kZSA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIHRoaXMuZ2V0UHJvcChcInByb21vLnByb21vQ29kZVwiKSwgXCJcIikgfHwgXCJcIjtcbiAgICBjb25zdCB2b3VjaGVyQ29udGVudCA9IHZvdWNoZXJzLm1hcCgoaXRlbTogYW55LCBpOiBudW1iZXIpID0+IHtcbiAgICAgIGNvbnN0IGV4cERhdGUgPSBpdGVtLmV4cERhdGU7XG4gICAgICByZXR1cm4gPFJvdyBrZXk9e2l9IGNsYXNzTmFtZT17XCJyb3ctdm91Y2hlci1pdGVtXCJ9IHR5cGU9XCJmbGV4XCIganVzdGlmeT1cInNwYWNlLWJldHdlZW5cIiBhbGlnbj17XCJtaWRkbGVcIn0+XG4gICAgICAgIDxDb2wgc3Bhbj17MTB9PlxuICAgICAgICAgIDxwIHN0eWxlPXt7XG4gICAgICAgICAgICBvdmVyZmxvd1dyYXA6IFwiYnJlYWstd29yZFwiLFxuICAgICAgICAgIH19PntpdGVtLmNhbXBhaWduQ29kZX0gLSB7aXRlbS5jYW1wYWlnbk5hbWV9PC9wPlxuICAgICAgICAgIDxwIHN0eWxlPXt7XG4gICAgICAgICAgICBvdmVyZmxvd1dyYXA6IFwiYnJlYWstd29yZFwiLFxuICAgICAgICAgIH19PntleHBEYXRlID8gRGF0ZVV0aWxzLmZvcm1hdERhdGUoRGF0ZVV0aWxzLnRvRGF0ZShleHBEYXRlKSkgOiBcIlwifTwvcD5cbiAgICAgICAgPC9Db2w+XG4gICAgICAgIDxDb2wgc3Bhbj17N30+XG4gICAgICAgICAgPGRpdiBzdHlsZT17e1xuICAgICAgICAgICAgb3ZlcmZsb3dXcmFwOiBcImJyZWFrLXdvcmRcIixcbiAgICAgICAgICAgIHRleHRBbGlnbjogXCJlbmRcIixcbiAgICAgICAgICB9fT5cbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgcHJvbW9Db2RlICE9PSBpdGVtLmNhbXBhaWduQ29kZSAmJlxuICAgICAgICAgICAgICA8YSBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZVZvdWNoZXIoaXRlbS5jYW1wYWlnbkNvZGUpfT5Vc2U8L2E+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHByb21vQ29kZSA9PT0gaXRlbS5jYW1wYWlnbkNvZGUgJiZcbiAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICA8YSBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgY29sb3I6IFwicmdiYSgwLCAwLCAwLCAwLjI1KVwiLFxuICAgICAgICAgICAgICAgICAgY3Vyc29yOiBcIm5vdC1hbGxvd2VkXCIsXG4gICAgICAgICAgICAgICAgICBwb2ludGVyRXZlbnRzOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICAgIG1hcmdpblJpZ2h0OiA1LFxuICAgICAgICAgICAgICAgIH19PlVzZWQ8L2E+XG4gICAgICAgICAgICAgICAgPGEgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVWb3VjaGVyKFwiXCIpfT5DYW5jZWw8L2E+XG4gICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0NvbD5cbiAgICAgIDwvUm93PjtcbiAgICB9KTtcbiAgICBjb25zdCBjb250ZW50ID0gPFZvdWNoZXJJbmZvRGl2PlxuICAgICAgeyFfLmlzRW1wdHkodm91Y2hlckNvbnRlbnQpID8gdm91Y2hlckNvbnRlbnQgOiA8cCBzdHlsZT17e1xuICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXG4gICAgICB9fT5ObyBWb3VjaGVyczwvcD59XG4gICAgPC9Wb3VjaGVySW5mb0Rpdj47XG4gICAgcmV0dXJuIChcbiAgICAgIDw+XG4gICAgICAgIHtcbiAgICAgICAgICBfLmlzRW1wdHkocHJvbW9Db2RlKSAmJlxuICAgICAgICAgIDxQb3BvdmVyXG4gICAgICAgICAgICB2aXNpYmxlPXt0aGlzLnN0YXRlLnZpc2libGV9XG4gICAgICAgICAgICBvblZpc2libGVDaGFuZ2U9e3RoaXMuaGFuZGxlVmlzaWJsZUNoYW5nZX1cbiAgICAgICAgICAgIHBsYWNlbWVudD1cInJpZ2h0XCJcbiAgICAgICAgICAgIHRpdGxlPXtcIlZvdWNoZXJzIEF2YWlsYWJsZVwiLnRvVXBwZXJDYXNlKCl9XG4gICAgICAgICAgICBjb250ZW50PXtjb250ZW50fVxuICAgICAgICAgICAgdHJpZ2dlcj1cImNsaWNrXCI+XG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9e1wiYnV0dG9uXCJ9PmFwcGx5IHByb21vPC9hPlxuICAgICAgICAgIDwvUG9wb3Zlcj5cbiAgICAgICAgfVxuICAgICAgICB7XG4gICAgICAgICAgIV8uaXNFbXB0eShwcm9tb0NvZGUpICYmXG4gICAgICAgICAgPD5cbiAgICAgICAgICAgIHshbG9ja2VkICYmIDxwIHN0eWxlPXt7XG4gICAgICAgICAgICAgIHRleHRBbGlnbjogXCJsZWZ0XCIsXG4gICAgICAgICAgICAgIGZvbnRTaXplOiAxMixcbiAgICAgICAgICAgICAgbWFyZ2luVG9wOiAxMCxcbiAgICAgICAgICAgICAgbGluZUhlaWdodDogXCIyMHB4XCIsXG4gICAgICAgICAgICAgIGNvbG9yOiBcInJlZFwiLFxuICAgICAgICAgICAgICBvdmVyZmxvd1dyYXA6IFwiYnJlYWstd29yZFwiLFxuICAgICAgICAgICAgfX0+e21lc3NhZ2VzfTwvcD59XG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9e1wiYnV0dG9uXCJ9IG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlVm91Y2hlcihcIlwiKX0+cmVtb3ZlPC9hPlxuICAgICAgICAgIDwvPlxuICAgICAgICB9XG4gICAgICA8Lz5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyUGxhbigpIHtcbiAgICBjb25zdCB7IG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8cCBjbGFzc05hbWU9e1wiaW5mby1jb250ZW50XCJ9PlxuICAgICAgICB7XG4gICAgICAgICAgXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInBsYW5OYW1lXCIpKVxuICAgICAgICB9XG4gICAgICA8L3A+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlck1hTm8oKSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBtYUlkID0gXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcIm1hSWRcIikpO1xuXG4gICAgaWYgKG1hSWQgJiYgXy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcIm1hTm9cIikpICYmIF8uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJtYU5hbWVcIikpKSB7XG4gICAgICByZXR1cm4gPHAgY2xhc3NOYW1lPXtcImluZm8tY29udGVudFwifT5cbiAgICAgICAgPGEgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIHdpbmRvdy5vcGVuKFxuICAgICAgICAgICAgYC9tYXN0ZXIvdmlldy8ke21hSWR9YCxcbiAgICAgICAgICApO1xuICAgICAgICB9fT57Xy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcIm1hTm9cIikpfSAtIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwibWFOYW1lXCIpKX08L2E+XG4gICAgICA8L3A+O1xuICAgIH1cbiAgICByZXR1cm4gPHNwYW4vPjtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCkge1xuICAgIGNvbnN0IGxpc3QgPSB0aGlzLnByb3BzLmxpbmVMaXN0IHx8IExpbmVMaXN0O1xuICAgIGNvbnN0IHsgYWN0aXZlU3RlcCwgbW9kZWwsIHNob3dMaXN0LCBvblVzZVZvdWNoZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDw+XG4gICAgICB7Xy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInF1b3RlTm9cIikpID9cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2BzaWRlLWluZm8taGVhZGVyICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1zaWRlXCIgOiBcIlwifWB9PlxuICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+e18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJxdW90ZU5vXCIpKX08L3A+XG4gICAgICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tY29udGVudFwifT5cbiAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicHJvZHVjdENvZGVcIikpfSAtIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicHJvZHVjdE5hbWVcIikpfVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgICB7dGhpcy5yZW5kZXJQbGFuKCl9XG4gICAgICAgICAge3RoaXMucmVuZGVyQnJlYWtkb3duKCl9XG4gICAgICAgICAge3RoaXMucmVuZGVyTWFObygpfVxuICAgICAgICAgIHt0aGlzLnJlbmRlcldhcm5pbmdNZXNzYWdlKCl9XG4gICAgICAgIDwvZGl2PiA6XG4gICAgICAgIChfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwiZW5kb05vXCIpKSAmJlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgc2lkZS1pbmZvLWhlYWRlciAke2lzTW9iaWxlID8gXCJtb2JpbGUtc2lkZVwiIDogXCJcIn1gfT5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+e18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJlbmRvTm9cIikpfTwvcD5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+XG4gICAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicHJvZHVjdENvZGVcIikpfSAtIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQcm9wKFwicHJvZHVjdE5hbWVcIikpfVxuICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAge3RoaXMucmVuZGVyUGxhbigpfVxuICAgICAgICAgICAge3RoaXMuZ2V0UHJlbWl1bSgpfVxuICAgICAgICAgIDwvZGl2PilcbiAgICAgIH1cbiAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZXRQb2xpY3lob2xkZXJQcm9wKFwibmFtZVwiKSkgJiZcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgc2lkZS1pbmZvLWhlYWRlciAke2lzTW9iaWxlID8gXCJtb2JpbGUtc2lkZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tdGl0bGVcIn0+Q3VzdG9tZXI8L3A+XG4gICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWNvbnRlbnRcIn0+e18uZ2V0KG1vZGVsLCB0aGlzLmdldFBvbGljeWhvbGRlclByb3AoXCJuYW1lXCIpKX08L3A+XG4gICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLWJvdHRvbVwifT5cbiAgICAgICAgICA8VG9vbHRpcCBwbGFjZW1lbnQ9XCJ0b3BcIiB0aXRsZT17XG4gICAgICAgICAgICBgJHtfLmdldChtb2RlbCwgdGhpcy5nZXRQb2xpY3lob2xkZXJQcm9wKFwibW9iaWxlTmF0aW9uQ29kZVwiKSwgXCJcIikgfHwgXCJcIn0gJHtfLmdldChtb2RlbCwgdGhpcy5nZXRQb2xpY3lob2xkZXJQcm9wKFwibW9iaWxlXCIpLCBcIlwiKSB8fCBcIlwifWBcbiAgICAgICAgICB9PlxuICAgICAgICAgICAgPEljb24gY2xhc3NOYW1lPXtcImluZm8taWNvblwifSB0eXBlPVwicGhvbmVcIi8+XG4gICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgIDxUb29sdGlwIHBsYWNlbWVudD1cInRvcFwiIHRpdGxlPXtfLmdldChtb2RlbCwgdGhpcy5nZXRQb2xpY3lob2xkZXJQcm9wKFwiZW1haWxcIiksIFwiXCIpIHx8IFwiXCJ9PlxuICAgICAgICAgICAgPEljb24gY2xhc3NOYW1lPXtcImluZm8taWNvblwifSB0eXBlPVwibWFpbFwiLz5cbiAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgIDwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgfVxuICAgICAge18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJzY05hbWVcIikpICYmXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YHNpZGUtaW5mby1oZWFkZXIgJHtpc01vYmlsZSA/IFwibW9iaWxlLXNpZGVcIiA6IFwiXCJ9YH0+XG4gICAgICAgIDxwIGNsYXNzTmFtZT17XCJpbmZvLXRpdGxlXCJ9PkNIQU5ORUw8L3A+XG4gICAgICAgIDxwXG4gICAgICAgICAgY2xhc3NOYW1lPXtcImluZm8tY29udGVudFwifT57Xy5nZXQobW9kZWwsIHRoaXMuZ2V0UHJvcChcInNjQ29kZVwiKSl9IC0ge18uZ2V0KG1vZGVsLCB0aGlzLmdldFByb3AoXCJzY05hbWVcIikpfTwvcD5cbiAgICAgICAgPHAgY2xhc3NOYW1lPXtcImluZm8tYm90dG9tXCJ9PlxuICAgICAgICAgIDxUb29sdGlwIHBsYWNlbWVudD1cInRvcFwiIHRpdGxlPXtcbiAgICAgICAgICAgIGAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFBvbGljeWhvbGRlclByb3AoXCJtb2JpbGVOYXRpb25Db2RlXCIpLCBcIlwiKSB8fCBcIlwifSAke18uZ2V0KG1vZGVsLCB0aGlzLmdldFBvbGljeWhvbGRlclByb3AoXCJtb2JpbGVcIiksIFwiXCIpIHx8IFwiXCJ9YFxuICAgICAgICAgIH0+XG4gICAgICAgICAgICA8SWNvbiBjbGFzc05hbWU9e1wiaW5mby1pY29uXCJ9IHR5cGU9XCJwaG9uZVwiLz5cbiAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgPFRvb2x0aXAgcGxhY2VtZW50PVwidG9wXCIgdGl0bGU9e18uZ2V0KG1vZGVsLCB0aGlzLmdldFBvbGljeWhvbGRlclByb3AoXCJlbWFpbFwiKSwgXCJcIikgfHwgXCJcIn0+XG4gICAgICAgICAgICA8SWNvbiBjbGFzc05hbWU9e1wiaW5mby1pY29uXCJ9IHR5cGU9XCJtYWlsXCIvPlxuICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgPC9wPlxuICAgICAgPC9kaXY+XG4gICAgICB9XG4gICAgICB7c2hvd0xpc3QgJiYgPGRpdiBjbGFzc05hbWU9e1wic2lkZS1pbmZvLXRpbWVcIn0+XG4gICAgICAgIDxUaW1lbGluZT5cbiAgICAgICAgICB7XG4gICAgICAgICAgICBsaXN0Lm1hcCgoaXRlbTogYW55LCBpOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICA8VGltZWxpbmUuSXRlbVxuICAgICAgICAgICAgICAgICAgZG90PXs8SWNvbiB0eXBlPVwibWludXMtY2lyY2xlXCIgc3R5bGU9e3sgZm9udFNpemU6IDE2IH19Lz59XG4gICAgICAgICAgICAgICAgICBrZXk9e2l9IGNvbG9yPXthY3RpdmVTdGVwID09PSBpdGVtID8gXCJncmVlblwiIDogXCJncmF5XCJ9PlxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogYWN0aXZlU3RlcCA9PT0gaXRlbSA/IFwiZ3JlZW5cIiA6IFwiXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXJCb3R0b206IGFjdGl2ZVN0ZXAgPT09IGl0ZW0gPyBcIjFweCBzb2xpZCBncmVlblwiIDogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnRXZWlnaHQ6IGFjdGl2ZVN0ZXAgPT09IGl0ZW0gPyA2MDAgOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgICAgfX0+e2l0ZW19PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvVGltZWxpbmUuSXRlbT5cbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgfVxuICAgICAgICA8L1RpbWVsaW5lPlxuICAgICAgPC9kaXY+fVxuICAgIDwvPjtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBpZiAoVXRpbHMuZ2V0SXNJbkFwcCgpKSByZXR1cm4gPGRpdi8+O1xuICAgIGNvbnN0IGxpc3QgPSB0aGlzLnByb3BzLmxpbmVMaXN0IHx8IExpbmVMaXN0O1xuICAgIGNvbnN0IHsgYWN0aXZlU3RlcCwgbW9kZWwsIHNob3dMaXN0LCBvblVzZVZvdWNoZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDb2wgc209ezV9IHhzPXsyNH0gc3R5bGU9e3sgYm9yZGVyUmlnaHQ6IFwiMXB4IHNvbGlkICNlOGU4ZThcIiB9fT5cbiAgICAgICAgPFNpZGVJbmZvU3R5bGU+XG4gICAgICAgICAge1xuICAgICAgICAgICAgaXNNb2JpbGUgPyA8YSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zaG93U3VtbWFyeSgpO1xuICAgICAgICAgICAgICB9fSBjbGFzc05hbWU9e1wic3VtbWFyeS1idG5cIn0+U3VtbWFyeTwvYT4gOlxuICAgICAgICAgICAgICB0aGlzLnJlbmRlclNpZGVJbmZvKClcbiAgICAgICAgICB9XG4gICAgICAgIDwvU2lkZUluZm9TdHlsZT5cbiAgICAgIDwvQ29sPlxuICAgICk7XG4gIH07XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQWtDQTtBQUNBO0FBd0hBOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUEyQkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZkE7QUFzQkE7QUF2QkE7QUEwQkE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBOUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQXVEQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQW1CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUtBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7Ozs7QUF4WEE7QUFDQTtBQURBO0FBeUJBO0FBQ0E7QUFGQTtBQXhCQTtBQXlYQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/phs/components/side-info.tsx
