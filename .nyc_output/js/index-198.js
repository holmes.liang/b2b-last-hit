__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "configConsumerProps", function() { return configConsumerProps; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _locale_provider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../locale-provider */ "./node_modules/antd/es/locale-provider/index.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./context */ "./node_modules/antd/es/config-provider/context.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ConfigConsumer", function() { return _context__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"]; });

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
} // TODO: remove this lint
// SFC has specified a displayName, but not worked.

/* eslint-disable react/display-name */







var configConsumerProps = ['getPopupContainer', 'rootPrefixCls', 'getPrefixCls', 'renderEmpty', 'csp', 'autoInsertSpaceInButton', 'locale', 'pageHeader'];

var ConfigProvider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ConfigProvider, _React$Component);

  function ConfigProvider() {
    var _this;

    _classCallCheck(this, ConfigProvider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ConfigProvider).apply(this, arguments));

    _this.getPrefixCls = function (suffixCls, customizePrefixCls) {
      var _this$props$prefixCls = _this.props.prefixCls,
          prefixCls = _this$props$prefixCls === void 0 ? 'ant' : _this$props$prefixCls;
      if (customizePrefixCls) return customizePrefixCls;
      return suffixCls ? "".concat(prefixCls, "-").concat(suffixCls) : prefixCls;
    };

    _this.renderProvider = function (context, legacyLocale) {
      var _this$props = _this.props,
          children = _this$props.children,
          getPopupContainer = _this$props.getPopupContainer,
          renderEmpty = _this$props.renderEmpty,
          csp = _this$props.csp,
          autoInsertSpaceInButton = _this$props.autoInsertSpaceInButton,
          locale = _this$props.locale,
          pageHeader = _this$props.pageHeader;

      var config = _extends(_extends({}, context), {
        getPrefixCls: _this.getPrefixCls,
        csp: csp,
        autoInsertSpaceInButton: autoInsertSpaceInButton
      });

      if (getPopupContainer) {
        config.getPopupContainer = getPopupContainer;
      }

      if (renderEmpty) {
        config.renderEmpty = renderEmpty;
      }

      if (pageHeader) {
        config.pageHeader = pageHeader;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_3__["ConfigContext"].Provider, {
        value: config
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider__WEBPACK_IMPORTED_MODULE_1__["default"], {
        locale: locale || legacyLocale,
        _ANT_MARK__: _locale_provider__WEBPACK_IMPORTED_MODULE_1__["ANT_MARK"]
      }, children));
    };

    return _this;
  }

  _createClass(ConfigProvider, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_2__["default"], null, function (_, __, legacyLocale) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"], null, function (context) {
          return _this2.renderProvider(context, legacyLocale);
        });
      });
    }
  }]);

  return ConfigProvider;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (ConfigProvider);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jb25maWctcHJvdmlkZXIvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2NvbmZpZy1wcm92aWRlci9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiLy8gVE9ETzogcmVtb3ZlIHRoaXMgbGludFxuLy8gU0ZDIGhhcyBzcGVjaWZpZWQgYSBkaXNwbGF5TmFtZSwgYnV0IG5vdCB3b3JrZWQuXG4vKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9kaXNwbGF5LW5hbWUgKi9cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMb2NhbGVQcm92aWRlciwgeyBBTlRfTUFSSyB9IGZyb20gJy4uL2xvY2FsZS1wcm92aWRlcic7XG5pbXBvcnQgTG9jYWxlUmVjZWl2ZXIgZnJvbSAnLi4vbG9jYWxlLXByb3ZpZGVyL0xvY2FsZVJlY2VpdmVyJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyLCBDb25maWdDb250ZXh0IH0gZnJvbSAnLi9jb250ZXh0JztcbmV4cG9ydCB7IENvbmZpZ0NvbnN1bWVyIH07XG5leHBvcnQgY29uc3QgY29uZmlnQ29uc3VtZXJQcm9wcyA9IFtcbiAgICAnZ2V0UG9wdXBDb250YWluZXInLFxuICAgICdyb290UHJlZml4Q2xzJyxcbiAgICAnZ2V0UHJlZml4Q2xzJyxcbiAgICAncmVuZGVyRW1wdHknLFxuICAgICdjc3AnLFxuICAgICdhdXRvSW5zZXJ0U3BhY2VJbkJ1dHRvbicsXG4gICAgJ2xvY2FsZScsXG4gICAgJ3BhZ2VIZWFkZXInLFxuXTtcbmNsYXNzIENvbmZpZ1Byb3ZpZGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5nZXRQcmVmaXhDbHMgPSAoc3VmZml4Q2xzLCBjdXN0b21pemVQcmVmaXhDbHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzID0gJ2FudCcgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAoY3VzdG9taXplUHJlZml4Q2xzKVxuICAgICAgICAgICAgICAgIHJldHVybiBjdXN0b21pemVQcmVmaXhDbHM7XG4gICAgICAgICAgICByZXR1cm4gc3VmZml4Q2xzID8gYCR7cHJlZml4Q2xzfS0ke3N1ZmZpeENsc31gIDogcHJlZml4Q2xzO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlclByb3ZpZGVyID0gKGNvbnRleHQsIGxlZ2FjeUxvY2FsZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBjaGlsZHJlbiwgZ2V0UG9wdXBDb250YWluZXIsIHJlbmRlckVtcHR5LCBjc3AsIGF1dG9JbnNlcnRTcGFjZUluQnV0dG9uLCBsb2NhbGUsIHBhZ2VIZWFkZXIsIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgY29uZmlnID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBjb250ZXh0KSwgeyBnZXRQcmVmaXhDbHM6IHRoaXMuZ2V0UHJlZml4Q2xzLCBjc3AsXG4gICAgICAgICAgICAgICAgYXV0b0luc2VydFNwYWNlSW5CdXR0b24gfSk7XG4gICAgICAgICAgICBpZiAoZ2V0UG9wdXBDb250YWluZXIpIHtcbiAgICAgICAgICAgICAgICBjb25maWcuZ2V0UG9wdXBDb250YWluZXIgPSBnZXRQb3B1cENvbnRhaW5lcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChyZW5kZXJFbXB0eSkge1xuICAgICAgICAgICAgICAgIGNvbmZpZy5yZW5kZXJFbXB0eSA9IHJlbmRlckVtcHR5O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHBhZ2VIZWFkZXIpIHtcbiAgICAgICAgICAgICAgICBjb25maWcucGFnZUhlYWRlciA9IHBhZ2VIZWFkZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gKDxDb25maWdDb250ZXh0LlByb3ZpZGVyIHZhbHVlPXtjb25maWd9PlxuICAgICAgICA8TG9jYWxlUHJvdmlkZXIgbG9jYWxlPXtsb2NhbGUgfHwgbGVnYWN5TG9jYWxlfSBfQU5UX01BUktfXz17QU5UX01BUkt9PlxuICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgPC9Mb2NhbGVQcm92aWRlcj5cbiAgICAgIDwvQ29uZmlnQ29udGV4dC5Qcm92aWRlcj4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiAoPExvY2FsZVJlY2VpdmVyPlxuICAgICAgICB7KF8sIF9fLCBsZWdhY3lMb2NhbGUpID0+ICg8Q29uZmlnQ29uc3VtZXI+XG4gICAgICAgICAgICB7Y29udGV4dCA9PiB0aGlzLnJlbmRlclByb3ZpZGVyKGNvbnRleHQsIGxlZ2FjeUxvY2FsZSl9XG4gICAgICAgICAgPC9Db25maWdDb25zdW1lcj4pfVxuICAgICAgPC9Mb2NhbGVSZWNlaXZlcj4pO1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IENvbmZpZ1Byb3ZpZGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVNBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUpBO0FBQ0E7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQWRBO0FBQ0E7QUFUQTtBQTJCQTtBQUNBOzs7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7Ozs7QUFuQ0E7QUFDQTtBQW9DQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/config-provider/index.js
