/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var isTypedArray = _util.isTypedArray;

var _clazz = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");

var enableClassCheck = _clazz.enableClassCheck;

var _sourceType = __webpack_require__(/*! ./helper/sourceType */ "./node_modules/echarts/lib/data/helper/sourceType.js");

var SOURCE_FORMAT_ORIGINAL = _sourceType.SOURCE_FORMAT_ORIGINAL;
var SERIES_LAYOUT_BY_COLUMN = _sourceType.SERIES_LAYOUT_BY_COLUMN;
var SOURCE_FORMAT_UNKNOWN = _sourceType.SOURCE_FORMAT_UNKNOWN;
var SOURCE_FORMAT_TYPED_ARRAY = _sourceType.SOURCE_FORMAT_TYPED_ARRAY;
var SOURCE_FORMAT_KEYED_COLUMNS = _sourceType.SOURCE_FORMAT_KEYED_COLUMNS;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * [sourceFormat]
 *
 * + "original":
 * This format is only used in series.data, where
 * itemStyle can be specified in data item.
 *
 * + "arrayRows":
 * [
 *     ['product', 'score', 'amount'],
 *     ['Matcha Latte', 89.3, 95.8],
 *     ['Milk Tea', 92.1, 89.4],
 *     ['Cheese Cocoa', 94.4, 91.2],
 *     ['Walnut Brownie', 85.4, 76.9]
 * ]
 *
 * + "objectRows":
 * [
 *     {product: 'Matcha Latte', score: 89.3, amount: 95.8},
 *     {product: 'Milk Tea', score: 92.1, amount: 89.4},
 *     {product: 'Cheese Cocoa', score: 94.4, amount: 91.2},
 *     {product: 'Walnut Brownie', score: 85.4, amount: 76.9}
 * ]
 *
 * + "keyedColumns":
 * {
 *     'product': ['Matcha Latte', 'Milk Tea', 'Cheese Cocoa', 'Walnut Brownie'],
 *     'count': [823, 235, 1042, 988],
 *     'score': [95.8, 81.4, 91.2, 76.9]
 * }
 *
 * + "typedArray"
 *
 * + "unknown"
 */

/**
 * @constructor
 * @param {Object} fields
 * @param {string} fields.sourceFormat
 * @param {Array|Object} fields.fromDataset
 * @param {Array|Object} [fields.data]
 * @param {string} [seriesLayoutBy='column']
 * @param {Array.<Object|string>} [dimensionsDefine]
 * @param {Objet|HashMap} [encodeDefine]
 * @param {number} [startIndex=0]
 * @param {number} [dimensionsDetectCount]
 */

function Source(fields) {
  /**
   * @type {boolean}
   */
  this.fromDataset = fields.fromDataset;
  /**
   * Not null/undefined.
   * @type {Array|Object}
   */

  this.data = fields.data || (fields.sourceFormat === SOURCE_FORMAT_KEYED_COLUMNS ? {} : []);
  /**
   * See also "detectSourceFormat".
   * Not null/undefined.
   * @type {string}
   */

  this.sourceFormat = fields.sourceFormat || SOURCE_FORMAT_UNKNOWN;
  /**
   * 'row' or 'column'
   * Not null/undefined.
   * @type {string} seriesLayoutBy
   */

  this.seriesLayoutBy = fields.seriesLayoutBy || SERIES_LAYOUT_BY_COLUMN;
  /**
   * dimensions definition in option.
   * can be null/undefined.
   * @type {Array.<Object|string>}
   */

  this.dimensionsDefine = fields.dimensionsDefine;
  /**
   * encode definition in option.
   * can be null/undefined.
   * @type {Objet|HashMap}
   */

  this.encodeDefine = fields.encodeDefine && createHashMap(fields.encodeDefine);
  /**
   * Not null/undefined, uint.
   * @type {number}
   */

  this.startIndex = fields.startIndex || 0;
  /**
   * Can be null/undefined (when unknown), uint.
   * @type {number}
   */

  this.dimensionsDetectCount = fields.dimensionsDetectCount;
}
/**
 * Wrap original series data for some compatibility cases.
 */


Source.seriesDataToSource = function (data) {
  return new Source({
    data: data,
    sourceFormat: isTypedArray(data) ? SOURCE_FORMAT_TYPED_ARRAY : SOURCE_FORMAT_ORIGINAL,
    fromDataset: false
  });
};

enableClassCheck(Source);
var _default = Source;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9Tb3VyY2UuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9kYXRhL1NvdXJjZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGNyZWF0ZUhhc2hNYXAgPSBfdXRpbC5jcmVhdGVIYXNoTWFwO1xudmFyIGlzVHlwZWRBcnJheSA9IF91dGlsLmlzVHlwZWRBcnJheTtcblxudmFyIF9jbGF6eiA9IHJlcXVpcmUoXCIuLi91dGlsL2NsYXp6XCIpO1xuXG52YXIgZW5hYmxlQ2xhc3NDaGVjayA9IF9jbGF6ei5lbmFibGVDbGFzc0NoZWNrO1xuXG52YXIgX3NvdXJjZVR5cGUgPSByZXF1aXJlKFwiLi9oZWxwZXIvc291cmNlVHlwZVwiKTtcblxudmFyIFNPVVJDRV9GT1JNQVRfT1JJR0lOQUwgPSBfc291cmNlVHlwZS5TT1VSQ0VfRk9STUFUX09SSUdJTkFMO1xudmFyIFNFUklFU19MQVlPVVRfQllfQ09MVU1OID0gX3NvdXJjZVR5cGUuU0VSSUVTX0xBWU9VVF9CWV9DT0xVTU47XG52YXIgU09VUkNFX0ZPUk1BVF9VTktOT1dOID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9VTktOT1dOO1xudmFyIFNPVVJDRV9GT1JNQVRfVFlQRURfQVJSQVkgPSBfc291cmNlVHlwZS5TT1VSQ0VfRk9STUFUX1RZUEVEX0FSUkFZO1xudmFyIFNPVVJDRV9GT1JNQVRfS0VZRURfQ09MVU1OUyA9IF9zb3VyY2VUeXBlLlNPVVJDRV9GT1JNQVRfS0VZRURfQ09MVU1OUztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIFtzb3VyY2VGb3JtYXRdXG4gKlxuICogKyBcIm9yaWdpbmFsXCI6XG4gKiBUaGlzIGZvcm1hdCBpcyBvbmx5IHVzZWQgaW4gc2VyaWVzLmRhdGEsIHdoZXJlXG4gKiBpdGVtU3R5bGUgY2FuIGJlIHNwZWNpZmllZCBpbiBkYXRhIGl0ZW0uXG4gKlxuICogKyBcImFycmF5Um93c1wiOlxuICogW1xuICogICAgIFsncHJvZHVjdCcsICdzY29yZScsICdhbW91bnQnXSxcbiAqICAgICBbJ01hdGNoYSBMYXR0ZScsIDg5LjMsIDk1LjhdLFxuICogICAgIFsnTWlsayBUZWEnLCA5Mi4xLCA4OS40XSxcbiAqICAgICBbJ0NoZWVzZSBDb2NvYScsIDk0LjQsIDkxLjJdLFxuICogICAgIFsnV2FsbnV0IEJyb3duaWUnLCA4NS40LCA3Ni45XVxuICogXVxuICpcbiAqICsgXCJvYmplY3RSb3dzXCI6XG4gKiBbXG4gKiAgICAge3Byb2R1Y3Q6ICdNYXRjaGEgTGF0dGUnLCBzY29yZTogODkuMywgYW1vdW50OiA5NS44fSxcbiAqICAgICB7cHJvZHVjdDogJ01pbGsgVGVhJywgc2NvcmU6IDkyLjEsIGFtb3VudDogODkuNH0sXG4gKiAgICAge3Byb2R1Y3Q6ICdDaGVlc2UgQ29jb2EnLCBzY29yZTogOTQuNCwgYW1vdW50OiA5MS4yfSxcbiAqICAgICB7cHJvZHVjdDogJ1dhbG51dCBCcm93bmllJywgc2NvcmU6IDg1LjQsIGFtb3VudDogNzYuOX1cbiAqIF1cbiAqXG4gKiArIFwia2V5ZWRDb2x1bW5zXCI6XG4gKiB7XG4gKiAgICAgJ3Byb2R1Y3QnOiBbJ01hdGNoYSBMYXR0ZScsICdNaWxrIFRlYScsICdDaGVlc2UgQ29jb2EnLCAnV2FsbnV0IEJyb3duaWUnXSxcbiAqICAgICAnY291bnQnOiBbODIzLCAyMzUsIDEwNDIsIDk4OF0sXG4gKiAgICAgJ3Njb3JlJzogWzk1LjgsIDgxLjQsIDkxLjIsIDc2LjldXG4gKiB9XG4gKlxuICogKyBcInR5cGVkQXJyYXlcIlxuICpcbiAqICsgXCJ1bmtub3duXCJcbiAqL1xuXG4vKipcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IGZpZWxkc1xuICogQHBhcmFtIHtzdHJpbmd9IGZpZWxkcy5zb3VyY2VGb3JtYXRcbiAqIEBwYXJhbSB7QXJyYXl8T2JqZWN0fSBmaWVsZHMuZnJvbURhdGFzZXRcbiAqIEBwYXJhbSB7QXJyYXl8T2JqZWN0fSBbZmllbGRzLmRhdGFdXG4gKiBAcGFyYW0ge3N0cmluZ30gW3Nlcmllc0xheW91dEJ5PSdjb2x1bW4nXVxuICogQHBhcmFtIHtBcnJheS48T2JqZWN0fHN0cmluZz59IFtkaW1lbnNpb25zRGVmaW5lXVxuICogQHBhcmFtIHtPYmpldHxIYXNoTWFwfSBbZW5jb2RlRGVmaW5lXVxuICogQHBhcmFtIHtudW1iZXJ9IFtzdGFydEluZGV4PTBdXG4gKiBAcGFyYW0ge251bWJlcn0gW2RpbWVuc2lvbnNEZXRlY3RDb3VudF1cbiAqL1xuZnVuY3Rpb24gU291cmNlKGZpZWxkcykge1xuICAvKipcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICB0aGlzLmZyb21EYXRhc2V0ID0gZmllbGRzLmZyb21EYXRhc2V0O1xuICAvKipcbiAgICogTm90IG51bGwvdW5kZWZpbmVkLlxuICAgKiBAdHlwZSB7QXJyYXl8T2JqZWN0fVxuICAgKi9cblxuICB0aGlzLmRhdGEgPSBmaWVsZHMuZGF0YSB8fCAoZmllbGRzLnNvdXJjZUZvcm1hdCA9PT0gU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TID8ge30gOiBbXSk7XG4gIC8qKlxuICAgKiBTZWUgYWxzbyBcImRldGVjdFNvdXJjZUZvcm1hdFwiLlxuICAgKiBOb3QgbnVsbC91bmRlZmluZWQuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuXG4gIHRoaXMuc291cmNlRm9ybWF0ID0gZmllbGRzLnNvdXJjZUZvcm1hdCB8fCBTT1VSQ0VfRk9STUFUX1VOS05PV047XG4gIC8qKlxuICAgKiAncm93JyBvciAnY29sdW1uJ1xuICAgKiBOb3QgbnVsbC91bmRlZmluZWQuXG4gICAqIEB0eXBlIHtzdHJpbmd9IHNlcmllc0xheW91dEJ5XG4gICAqL1xuXG4gIHRoaXMuc2VyaWVzTGF5b3V0QnkgPSBmaWVsZHMuc2VyaWVzTGF5b3V0QnkgfHwgU0VSSUVTX0xBWU9VVF9CWV9DT0xVTU47XG4gIC8qKlxuICAgKiBkaW1lbnNpb25zIGRlZmluaXRpb24gaW4gb3B0aW9uLlxuICAgKiBjYW4gYmUgbnVsbC91bmRlZmluZWQuXG4gICAqIEB0eXBlIHtBcnJheS48T2JqZWN0fHN0cmluZz59XG4gICAqL1xuXG4gIHRoaXMuZGltZW5zaW9uc0RlZmluZSA9IGZpZWxkcy5kaW1lbnNpb25zRGVmaW5lO1xuICAvKipcbiAgICogZW5jb2RlIGRlZmluaXRpb24gaW4gb3B0aW9uLlxuICAgKiBjYW4gYmUgbnVsbC91bmRlZmluZWQuXG4gICAqIEB0eXBlIHtPYmpldHxIYXNoTWFwfVxuICAgKi9cblxuICB0aGlzLmVuY29kZURlZmluZSA9IGZpZWxkcy5lbmNvZGVEZWZpbmUgJiYgY3JlYXRlSGFzaE1hcChmaWVsZHMuZW5jb2RlRGVmaW5lKTtcbiAgLyoqXG4gICAqIE5vdCBudWxsL3VuZGVmaW5lZCwgdWludC5cbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG5cbiAgdGhpcy5zdGFydEluZGV4ID0gZmllbGRzLnN0YXJ0SW5kZXggfHwgMDtcbiAgLyoqXG4gICAqIENhbiBiZSBudWxsL3VuZGVmaW5lZCAod2hlbiB1bmtub3duKSwgdWludC5cbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG5cbiAgdGhpcy5kaW1lbnNpb25zRGV0ZWN0Q291bnQgPSBmaWVsZHMuZGltZW5zaW9uc0RldGVjdENvdW50O1xufVxuLyoqXG4gKiBXcmFwIG9yaWdpbmFsIHNlcmllcyBkYXRhIGZvciBzb21lIGNvbXBhdGliaWxpdHkgY2FzZXMuXG4gKi9cblxuXG5Tb3VyY2Uuc2VyaWVzRGF0YVRvU291cmNlID0gZnVuY3Rpb24gKGRhdGEpIHtcbiAgcmV0dXJuIG5ldyBTb3VyY2Uoe1xuICAgIGRhdGE6IGRhdGEsXG4gICAgc291cmNlRm9ybWF0OiBpc1R5cGVkQXJyYXkoZGF0YSkgPyBTT1VSQ0VfRk9STUFUX1RZUEVEX0FSUkFZIDogU09VUkNFX0ZPUk1BVF9PUklHSU5BTCxcbiAgICBmcm9tRGF0YXNldDogZmFsc2VcbiAgfSk7XG59O1xuXG5lbmFibGVDbGFzc0NoZWNrKFNvdXJjZSk7XG52YXIgX2RlZmF1bHQgPSBTb3VyY2U7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQ0E7Ozs7Ozs7Ozs7Ozs7QUFZQTtBQUNBOzs7QUFHQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/Source.js
