__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");


var processor = null;

var QRCodeGenerator =
/*#__PURE__*/
function () {
  var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
  /*#__PURE__*/
  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(processor != null)) {
              _context.next = 2;
              break;
            }

            return _context.abrupt("return", processor);

          case 2:
            _context.next = 4;
            return __webpack_require__.e(/*! import() | qrcode-generator */ "vendors~qrcode-generator~._node_modules_qrcode.es_build_qrcode.umd.js~f815fafd").then(__webpack_require__.t.bind(null, /*! qrcode.es */ "./node_modules/qrcode.es/build/qrcode.umd.js", 7));

          case 4:
            processor = _context.sent;
            return _context.abrupt("return", processor);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function QRCodeGenerator() {
    return _ref.apply(this, arguments);
  };
}();

/* harmony default export */ __webpack_exports__["default"] = (QRCodeGenerator);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3FyY29kZS1nZW5lcmF0b3IudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL3FyY29kZS1nZW5lcmF0b3IudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFFSQ29kZUdlbmVyYXRvckxpYiB9IGZyb20gJ0BteS10eXBlcyc7XG5cbmxldCBwcm9jZXNzb3I6IFFSQ29kZUdlbmVyYXRvckxpYiB8IG51bGwgPSBudWxsO1xuXG5jb25zdCBRUkNvZGVHZW5lcmF0b3IgPSBhc3luYyAoKTogUHJvbWlzZTxRUkNvZGVHZW5lcmF0b3JMaWI+ID0+IHtcblx0aWYgKHByb2Nlc3NvciAhPSBudWxsKSB7XG5cdFx0cmV0dXJuIHByb2Nlc3Nvcjtcblx0fVxuXHRwcm9jZXNzb3IgPSAoYXdhaXQgaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6IFwicXJjb2RlLWdlbmVyYXRvclwiICovICdxcmNvZGUuZXMnKSkgYXMgYW55O1xuXHRyZXR1cm4gcHJvY2Vzc29yIGFzIFFSQ29kZUdlbmVyYXRvckxpYjtcbn07XG5leHBvcnQgZGVmYXVsdCBRUkNvZGVHZW5lcmF0b3I7XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBLHdRQUlBO0FBQ0E7QUFMQTtBQUlBO0FBSkE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/qrcode-generator.tsx
