/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var globalListener = __webpack_require__(/*! ./globalListener */ "./node_modules/echarts/lib/component/axisPointer/globalListener.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var AxisPointerView = echarts.extendComponentView({
  type: 'axisPointer',
  render: function render(globalAxisPointerModel, ecModel, api) {
    var globalTooltipModel = ecModel.getComponent('tooltip');
    var triggerOn = globalAxisPointerModel.get('triggerOn') || globalTooltipModel && globalTooltipModel.get('triggerOn') || 'mousemove|click'; // Register global listener in AxisPointerView to enable
    // AxisPointerView to be independent to Tooltip.

    globalListener.register('axisPointer', api, function (currTrigger, e, dispatchAction) {
      // If 'none', it is not controlled by mouse totally.
      if (triggerOn !== 'none' && (currTrigger === 'leave' || triggerOn.indexOf(currTrigger) >= 0)) {
        dispatchAction({
          type: 'updateAxisPointer',
          currTrigger: currTrigger,
          x: e && e.offsetX,
          y: e && e.offsetY
        });
      }
    });
  },

  /**
   * @override
   */
  remove: function remove(ecModel, api) {
    globalListener.unregister(api.getZr(), 'axisPointer');
    AxisPointerView.superApply(this._model, 'remove', arguments);
  },

  /**
   * @override
   */
  dispose: function dispose(ecModel, api) {
    globalListener.unregister('axisPointer', api);
    AxisPointerView.superApply(this._model, 'dispose', arguments);
  }
});
var _default = AxisPointerView;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL0F4aXNQb2ludGVyVmlldy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NvbXBvbmVudC9heGlzUG9pbnRlci9BeGlzUG9pbnRlclZpZXcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBlY2hhcnRzID0gcmVxdWlyZShcIi4uLy4uL2VjaGFydHNcIik7XG5cbnZhciBnbG9iYWxMaXN0ZW5lciA9IHJlcXVpcmUoXCIuL2dsb2JhbExpc3RlbmVyXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgQXhpc1BvaW50ZXJWaWV3ID0gZWNoYXJ0cy5leHRlbmRDb21wb25lbnRWaWV3KHtcbiAgdHlwZTogJ2F4aXNQb2ludGVyJyxcbiAgcmVuZGVyOiBmdW5jdGlvbiAoZ2xvYmFsQXhpc1BvaW50ZXJNb2RlbCwgZWNNb2RlbCwgYXBpKSB7XG4gICAgdmFyIGdsb2JhbFRvb2x0aXBNb2RlbCA9IGVjTW9kZWwuZ2V0Q29tcG9uZW50KCd0b29sdGlwJyk7XG4gICAgdmFyIHRyaWdnZXJPbiA9IGdsb2JhbEF4aXNQb2ludGVyTW9kZWwuZ2V0KCd0cmlnZ2VyT24nKSB8fCBnbG9iYWxUb29sdGlwTW9kZWwgJiYgZ2xvYmFsVG9vbHRpcE1vZGVsLmdldCgndHJpZ2dlck9uJykgfHwgJ21vdXNlbW92ZXxjbGljayc7IC8vIFJlZ2lzdGVyIGdsb2JhbCBsaXN0ZW5lciBpbiBBeGlzUG9pbnRlclZpZXcgdG8gZW5hYmxlXG4gICAgLy8gQXhpc1BvaW50ZXJWaWV3IHRvIGJlIGluZGVwZW5kZW50IHRvIFRvb2x0aXAuXG5cbiAgICBnbG9iYWxMaXN0ZW5lci5yZWdpc3RlcignYXhpc1BvaW50ZXInLCBhcGksIGZ1bmN0aW9uIChjdXJyVHJpZ2dlciwgZSwgZGlzcGF0Y2hBY3Rpb24pIHtcbiAgICAgIC8vIElmICdub25lJywgaXQgaXMgbm90IGNvbnRyb2xsZWQgYnkgbW91c2UgdG90YWxseS5cbiAgICAgIGlmICh0cmlnZ2VyT24gIT09ICdub25lJyAmJiAoY3VyclRyaWdnZXIgPT09ICdsZWF2ZScgfHwgdHJpZ2dlck9uLmluZGV4T2YoY3VyclRyaWdnZXIpID49IDApKSB7XG4gICAgICAgIGRpc3BhdGNoQWN0aW9uKHtcbiAgICAgICAgICB0eXBlOiAndXBkYXRlQXhpc1BvaW50ZXInLFxuICAgICAgICAgIGN1cnJUcmlnZ2VyOiBjdXJyVHJpZ2dlcixcbiAgICAgICAgICB4OiBlICYmIGUub2Zmc2V0WCxcbiAgICAgICAgICB5OiBlICYmIGUub2Zmc2V0WVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICByZW1vdmU6IGZ1bmN0aW9uIChlY01vZGVsLCBhcGkpIHtcbiAgICBnbG9iYWxMaXN0ZW5lci51bnJlZ2lzdGVyKGFwaS5nZXRacigpLCAnYXhpc1BvaW50ZXInKTtcbiAgICBBeGlzUG9pbnRlclZpZXcuc3VwZXJBcHBseSh0aGlzLl9tb2RlbCwgJ3JlbW92ZScsIGFyZ3VtZW50cyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBvdmVycmlkZVxuICAgKi9cbiAgZGlzcG9zZTogZnVuY3Rpb24gKGVjTW9kZWwsIGFwaSkge1xuICAgIGdsb2JhbExpc3RlbmVyLnVucmVnaXN0ZXIoJ2F4aXNQb2ludGVyJywgYXBpKTtcbiAgICBBeGlzUG9pbnRlclZpZXcuc3VwZXJBcHBseSh0aGlzLl9tb2RlbCwgJ2Rpc3Bvc2UnLCBhcmd1bWVudHMpO1xuICB9XG59KTtcbnZhciBfZGVmYXVsdCA9IEF4aXNQb2ludGVyVmlldztcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFsQ0E7QUFvQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/AxisPointerView.js
