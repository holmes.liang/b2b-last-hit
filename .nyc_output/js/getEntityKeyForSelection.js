/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getEntityKeyForSelection
 * @format
 * 
 */

/**
 * Return the entity key that should be used when inserting text for the
 * specified target selection, only if the entity is `MUTABLE`. `IMMUTABLE`
 * and `SEGMENTED` entities should not be used for insertion behavior.
 */

function getEntityKeyForSelection(contentState, targetSelection) {
  var entityKey;

  if (targetSelection.isCollapsed()) {
    var key = targetSelection.getAnchorKey();
    var offset = targetSelection.getAnchorOffset();

    if (offset > 0) {
      entityKey = contentState.getBlockForKey(key).getEntityAt(offset - 1);

      if (entityKey !== contentState.getBlockForKey(key).getEntityAt(offset)) {
        return null;
      }

      return filterKey(contentState.getEntityMap(), entityKey);
    }

    return null;
  }

  var startKey = targetSelection.getStartKey();
  var startOffset = targetSelection.getStartOffset();
  var startBlock = contentState.getBlockForKey(startKey);
  entityKey = startOffset === startBlock.getLength() ? null : startBlock.getEntityAt(startOffset);
  return filterKey(contentState.getEntityMap(), entityKey);
}
/**
 * Determine whether an entity key corresponds to a `MUTABLE` entity. If so,
 * return it. If not, return null.
 */


function filterKey(entityMap, entityKey) {
  if (entityKey) {
    var entity = entityMap.__get(entityKey);

    return entity.getMutability() === 'MUTABLE' ? entityKey : null;
  }

  return null;
}

module.exports = getEntityKeyForSelection;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldEVudGl0eUtleUZvclNlbGVjdGlvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9nZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBnZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb25cbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBSZXR1cm4gdGhlIGVudGl0eSBrZXkgdGhhdCBzaG91bGQgYmUgdXNlZCB3aGVuIGluc2VydGluZyB0ZXh0IGZvciB0aGVcbiAqIHNwZWNpZmllZCB0YXJnZXQgc2VsZWN0aW9uLCBvbmx5IGlmIHRoZSBlbnRpdHkgaXMgYE1VVEFCTEVgLiBgSU1NVVRBQkxFYFxuICogYW5kIGBTRUdNRU5URURgIGVudGl0aWVzIHNob3VsZCBub3QgYmUgdXNlZCBmb3IgaW5zZXJ0aW9uIGJlaGF2aW9yLlxuICovXG5mdW5jdGlvbiBnZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24oY29udGVudFN0YXRlLCB0YXJnZXRTZWxlY3Rpb24pIHtcbiAgdmFyIGVudGl0eUtleTtcblxuICBpZiAodGFyZ2V0U2VsZWN0aW9uLmlzQ29sbGFwc2VkKCkpIHtcbiAgICB2YXIga2V5ID0gdGFyZ2V0U2VsZWN0aW9uLmdldEFuY2hvcktleSgpO1xuICAgIHZhciBvZmZzZXQgPSB0YXJnZXRTZWxlY3Rpb24uZ2V0QW5jaG9yT2Zmc2V0KCk7XG4gICAgaWYgKG9mZnNldCA+IDApIHtcbiAgICAgIGVudGl0eUtleSA9IGNvbnRlbnRTdGF0ZS5nZXRCbG9ja0ZvcktleShrZXkpLmdldEVudGl0eUF0KG9mZnNldCAtIDEpO1xuICAgICAgaWYgKGVudGl0eUtleSAhPT0gY29udGVudFN0YXRlLmdldEJsb2NrRm9yS2V5KGtleSkuZ2V0RW50aXR5QXQob2Zmc2V0KSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICAgIHJldHVybiBmaWx0ZXJLZXkoY29udGVudFN0YXRlLmdldEVudGl0eU1hcCgpLCBlbnRpdHlLZXkpO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHZhciBzdGFydEtleSA9IHRhcmdldFNlbGVjdGlvbi5nZXRTdGFydEtleSgpO1xuICB2YXIgc3RhcnRPZmZzZXQgPSB0YXJnZXRTZWxlY3Rpb24uZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIHN0YXJ0QmxvY2sgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tGb3JLZXkoc3RhcnRLZXkpO1xuXG4gIGVudGl0eUtleSA9IHN0YXJ0T2Zmc2V0ID09PSBzdGFydEJsb2NrLmdldExlbmd0aCgpID8gbnVsbCA6IHN0YXJ0QmxvY2suZ2V0RW50aXR5QXQoc3RhcnRPZmZzZXQpO1xuXG4gIHJldHVybiBmaWx0ZXJLZXkoY29udGVudFN0YXRlLmdldEVudGl0eU1hcCgpLCBlbnRpdHlLZXkpO1xufVxuXG4vKipcbiAqIERldGVybWluZSB3aGV0aGVyIGFuIGVudGl0eSBrZXkgY29ycmVzcG9uZHMgdG8gYSBgTVVUQUJMRWAgZW50aXR5LiBJZiBzbyxcbiAqIHJldHVybiBpdC4gSWYgbm90LCByZXR1cm4gbnVsbC5cbiAqL1xuZnVuY3Rpb24gZmlsdGVyS2V5KGVudGl0eU1hcCwgZW50aXR5S2V5KSB7XG4gIGlmIChlbnRpdHlLZXkpIHtcbiAgICB2YXIgZW50aXR5ID0gZW50aXR5TWFwLl9fZ2V0KGVudGl0eUtleSk7XG4gICAgcmV0dXJuIGVudGl0eS5nZXRNdXRhYmlsaXR5KCkgPT09ICdNVVRBQkxFJyA/IGVudGl0eUtleSA6IG51bGw7XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0RW50aXR5S2V5Rm9yU2VsZWN0aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBRUE7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getEntityKeyForSelection.js
