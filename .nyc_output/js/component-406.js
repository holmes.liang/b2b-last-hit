/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var _clazz = __webpack_require__(/*! ./clazz */ "./node_modules/echarts/lib/util/clazz.js");

var parseClassType = _clazz.parseClassType;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var base = 0;
/**
 * @public
 * @param {string} type
 * @return {string}
 */

function getUID(type) {
  // Considering the case of crossing js context,
  // use Math.random to make id as unique as possible.
  return [type || '', base++, Math.random().toFixed(5)].join('_');
}
/**
 * @inner
 */


function enableSubTypeDefaulter(entity) {
  var subTypeDefaulters = {};

  entity.registerSubTypeDefaulter = function (componentType, defaulter) {
    componentType = parseClassType(componentType);
    subTypeDefaulters[componentType.main] = defaulter;
  };

  entity.determineSubType = function (componentType, option) {
    var type = option.type;

    if (!type) {
      var componentTypeMain = parseClassType(componentType).main;

      if (entity.hasSubTypes(componentType) && subTypeDefaulters[componentTypeMain]) {
        type = subTypeDefaulters[componentTypeMain](option);
      }
    }

    return type;
  };

  return entity;
}
/**
 * Topological travel on Activity Network (Activity On Vertices).
 * Dependencies is defined in Model.prototype.dependencies, like ['xAxis', 'yAxis'].
 *
 * If 'xAxis' or 'yAxis' is absent in componentTypeList, just ignore it in topology.
 *
 * If there is circle dependencey, Error will be thrown.
 *
 */


function enableTopologicalTravel(entity, dependencyGetter) {
  /**
   * @public
   * @param {Array.<string>} targetNameList Target Component type list.
   *                                           Can be ['aa', 'bb', 'aa.xx']
   * @param {Array.<string>} fullNameList By which we can build dependency graph.
   * @param {Function} callback Params: componentType, dependencies.
   * @param {Object} context Scope of callback.
   */
  entity.topologicalTravel = function (targetNameList, fullNameList, callback, context) {
    if (!targetNameList.length) {
      return;
    }

    var result = makeDepndencyGraph(fullNameList);
    var graph = result.graph;
    var stack = result.noEntryList;
    var targetNameSet = {};
    zrUtil.each(targetNameList, function (name) {
      targetNameSet[name] = true;
    });

    while (stack.length) {
      var currComponentType = stack.pop();
      var currVertex = graph[currComponentType];
      var isInTargetNameSet = !!targetNameSet[currComponentType];

      if (isInTargetNameSet) {
        callback.call(context, currComponentType, currVertex.originalDeps.slice());
        delete targetNameSet[currComponentType];
      }

      zrUtil.each(currVertex.successor, isInTargetNameSet ? removeEdgeAndAdd : removeEdge);
    }

    zrUtil.each(targetNameSet, function () {
      throw new Error('Circle dependency may exists');
    });

    function removeEdge(succComponentType) {
      graph[succComponentType].entryCount--;

      if (graph[succComponentType].entryCount === 0) {
        stack.push(succComponentType);
      }
    } // Consider this case: legend depends on series, and we call
    // chart.setOption({series: [...]}), where only series is in option.
    // If we do not have 'removeEdgeAndAdd', legendModel.mergeOption will
    // not be called, but only sereis.mergeOption is called. Thus legend
    // have no chance to update its local record about series (like which
    // name of series is available in legend).


    function removeEdgeAndAdd(succComponentType) {
      targetNameSet[succComponentType] = true;
      removeEdge(succComponentType);
    }
  };
  /**
   * DepndencyGraph: {Object}
   * key: conponentType,
   * value: {
   *     successor: [conponentTypes...],
   *     originalDeps: [conponentTypes...],
   *     entryCount: {number}
   * }
   */


  function makeDepndencyGraph(fullNameList) {
    var graph = {};
    var noEntryList = [];
    zrUtil.each(fullNameList, function (name) {
      var thisItem = createDependencyGraphItem(graph, name);
      var originalDeps = thisItem.originalDeps = dependencyGetter(name);
      var availableDeps = getAvailableDependencies(originalDeps, fullNameList);
      thisItem.entryCount = availableDeps.length;

      if (thisItem.entryCount === 0) {
        noEntryList.push(name);
      }

      zrUtil.each(availableDeps, function (dependentName) {
        if (zrUtil.indexOf(thisItem.predecessor, dependentName) < 0) {
          thisItem.predecessor.push(dependentName);
        }

        var thatItem = createDependencyGraphItem(graph, dependentName);

        if (zrUtil.indexOf(thatItem.successor, dependentName) < 0) {
          thatItem.successor.push(name);
        }
      });
    });
    return {
      graph: graph,
      noEntryList: noEntryList
    };
  }

  function createDependencyGraphItem(graph, name) {
    if (!graph[name]) {
      graph[name] = {
        predecessor: [],
        successor: []
      };
    }

    return graph[name];
  }

  function getAvailableDependencies(originalDeps, fullNameList) {
    var availableDeps = [];
    zrUtil.each(originalDeps, function (dep) {
      zrUtil.indexOf(fullNameList, dep) >= 0 && availableDeps.push(dep);
    });
    return availableDeps;
  }
}

exports.getUID = getUID;
exports.enableSubTypeDefaulter = enableSubTypeDefaulter;
exports.enableTopologicalTravel = enableTopologicalTravel;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9jb21wb25lbnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi91dGlsL2NvbXBvbmVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBfY2xhenogPSByZXF1aXJlKFwiLi9jbGF6elwiKTtcblxudmFyIHBhcnNlQ2xhc3NUeXBlID0gX2NsYXp6LnBhcnNlQ2xhc3NUeXBlO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgYmFzZSA9IDA7XG4vKipcbiAqIEBwdWJsaWNcbiAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuZnVuY3Rpb24gZ2V0VUlEKHR5cGUpIHtcbiAgLy8gQ29uc2lkZXJpbmcgdGhlIGNhc2Ugb2YgY3Jvc3NpbmcganMgY29udGV4dCxcbiAgLy8gdXNlIE1hdGgucmFuZG9tIHRvIG1ha2UgaWQgYXMgdW5pcXVlIGFzIHBvc3NpYmxlLlxuICByZXR1cm4gW3R5cGUgfHwgJycsIGJhc2UrKywgTWF0aC5yYW5kb20oKS50b0ZpeGVkKDUpXS5qb2luKCdfJyk7XG59XG4vKipcbiAqIEBpbm5lclxuICovXG5cblxuZnVuY3Rpb24gZW5hYmxlU3ViVHlwZURlZmF1bHRlcihlbnRpdHkpIHtcbiAgdmFyIHN1YlR5cGVEZWZhdWx0ZXJzID0ge307XG5cbiAgZW50aXR5LnJlZ2lzdGVyU3ViVHlwZURlZmF1bHRlciA9IGZ1bmN0aW9uIChjb21wb25lbnRUeXBlLCBkZWZhdWx0ZXIpIHtcbiAgICBjb21wb25lbnRUeXBlID0gcGFyc2VDbGFzc1R5cGUoY29tcG9uZW50VHlwZSk7XG4gICAgc3ViVHlwZURlZmF1bHRlcnNbY29tcG9uZW50VHlwZS5tYWluXSA9IGRlZmF1bHRlcjtcbiAgfTtcblxuICBlbnRpdHkuZGV0ZXJtaW5lU3ViVHlwZSA9IGZ1bmN0aW9uIChjb21wb25lbnRUeXBlLCBvcHRpb24pIHtcbiAgICB2YXIgdHlwZSA9IG9wdGlvbi50eXBlO1xuXG4gICAgaWYgKCF0eXBlKSB7XG4gICAgICB2YXIgY29tcG9uZW50VHlwZU1haW4gPSBwYXJzZUNsYXNzVHlwZShjb21wb25lbnRUeXBlKS5tYWluO1xuXG4gICAgICBpZiAoZW50aXR5Lmhhc1N1YlR5cGVzKGNvbXBvbmVudFR5cGUpICYmIHN1YlR5cGVEZWZhdWx0ZXJzW2NvbXBvbmVudFR5cGVNYWluXSkge1xuICAgICAgICB0eXBlID0gc3ViVHlwZURlZmF1bHRlcnNbY29tcG9uZW50VHlwZU1haW5dKG9wdGlvbik7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHR5cGU7XG4gIH07XG5cbiAgcmV0dXJuIGVudGl0eTtcbn1cbi8qKlxuICogVG9wb2xvZ2ljYWwgdHJhdmVsIG9uIEFjdGl2aXR5IE5ldHdvcmsgKEFjdGl2aXR5IE9uIFZlcnRpY2VzKS5cbiAqIERlcGVuZGVuY2llcyBpcyBkZWZpbmVkIGluIE1vZGVsLnByb3RvdHlwZS5kZXBlbmRlbmNpZXMsIGxpa2UgWyd4QXhpcycsICd5QXhpcyddLlxuICpcbiAqIElmICd4QXhpcycgb3IgJ3lBeGlzJyBpcyBhYnNlbnQgaW4gY29tcG9uZW50VHlwZUxpc3QsIGp1c3QgaWdub3JlIGl0IGluIHRvcG9sb2d5LlxuICpcbiAqIElmIHRoZXJlIGlzIGNpcmNsZSBkZXBlbmRlbmNleSwgRXJyb3Igd2lsbCBiZSB0aHJvd24uXG4gKlxuICovXG5cblxuZnVuY3Rpb24gZW5hYmxlVG9wb2xvZ2ljYWxUcmF2ZWwoZW50aXR5LCBkZXBlbmRlbmN5R2V0dGVyKSB7XG4gIC8qKlxuICAgKiBAcHVibGljXG4gICAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IHRhcmdldE5hbWVMaXN0IFRhcmdldCBDb21wb25lbnQgdHlwZSBsaXN0LlxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBDYW4gYmUgWydhYScsICdiYicsICdhYS54eCddXG4gICAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IGZ1bGxOYW1lTGlzdCBCeSB3aGljaCB3ZSBjYW4gYnVpbGQgZGVwZW5kZW5jeSBncmFwaC5cbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gY2FsbGJhY2sgUGFyYW1zOiBjb21wb25lbnRUeXBlLCBkZXBlbmRlbmNpZXMuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBjb250ZXh0IFNjb3BlIG9mIGNhbGxiYWNrLlxuICAgKi9cbiAgZW50aXR5LnRvcG9sb2dpY2FsVHJhdmVsID0gZnVuY3Rpb24gKHRhcmdldE5hbWVMaXN0LCBmdWxsTmFtZUxpc3QsIGNhbGxiYWNrLCBjb250ZXh0KSB7XG4gICAgaWYgKCF0YXJnZXROYW1lTGlzdC5sZW5ndGgpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcmVzdWx0ID0gbWFrZURlcG5kZW5jeUdyYXBoKGZ1bGxOYW1lTGlzdCk7XG4gICAgdmFyIGdyYXBoID0gcmVzdWx0LmdyYXBoO1xuICAgIHZhciBzdGFjayA9IHJlc3VsdC5ub0VudHJ5TGlzdDtcbiAgICB2YXIgdGFyZ2V0TmFtZVNldCA9IHt9O1xuICAgIHpyVXRpbC5lYWNoKHRhcmdldE5hbWVMaXN0LCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgdGFyZ2V0TmFtZVNldFtuYW1lXSA9IHRydWU7XG4gICAgfSk7XG5cbiAgICB3aGlsZSAoc3RhY2subGVuZ3RoKSB7XG4gICAgICB2YXIgY3VyckNvbXBvbmVudFR5cGUgPSBzdGFjay5wb3AoKTtcbiAgICAgIHZhciBjdXJyVmVydGV4ID0gZ3JhcGhbY3VyckNvbXBvbmVudFR5cGVdO1xuICAgICAgdmFyIGlzSW5UYXJnZXROYW1lU2V0ID0gISF0YXJnZXROYW1lU2V0W2N1cnJDb21wb25lbnRUeXBlXTtcblxuICAgICAgaWYgKGlzSW5UYXJnZXROYW1lU2V0KSB7XG4gICAgICAgIGNhbGxiYWNrLmNhbGwoY29udGV4dCwgY3VyckNvbXBvbmVudFR5cGUsIGN1cnJWZXJ0ZXgub3JpZ2luYWxEZXBzLnNsaWNlKCkpO1xuICAgICAgICBkZWxldGUgdGFyZ2V0TmFtZVNldFtjdXJyQ29tcG9uZW50VHlwZV07XG4gICAgICB9XG5cbiAgICAgIHpyVXRpbC5lYWNoKGN1cnJWZXJ0ZXguc3VjY2Vzc29yLCBpc0luVGFyZ2V0TmFtZVNldCA/IHJlbW92ZUVkZ2VBbmRBZGQgOiByZW1vdmVFZGdlKTtcbiAgICB9XG5cbiAgICB6clV0aWwuZWFjaCh0YXJnZXROYW1lU2V0LCBmdW5jdGlvbiAoKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NpcmNsZSBkZXBlbmRlbmN5IG1heSBleGlzdHMnKTtcbiAgICB9KTtcblxuICAgIGZ1bmN0aW9uIHJlbW92ZUVkZ2Uoc3VjY0NvbXBvbmVudFR5cGUpIHtcbiAgICAgIGdyYXBoW3N1Y2NDb21wb25lbnRUeXBlXS5lbnRyeUNvdW50LS07XG5cbiAgICAgIGlmIChncmFwaFtzdWNjQ29tcG9uZW50VHlwZV0uZW50cnlDb3VudCA9PT0gMCkge1xuICAgICAgICBzdGFjay5wdXNoKHN1Y2NDb21wb25lbnRUeXBlKTtcbiAgICAgIH1cbiAgICB9IC8vIENvbnNpZGVyIHRoaXMgY2FzZTogbGVnZW5kIGRlcGVuZHMgb24gc2VyaWVzLCBhbmQgd2UgY2FsbFxuICAgIC8vIGNoYXJ0LnNldE9wdGlvbih7c2VyaWVzOiBbLi4uXX0pLCB3aGVyZSBvbmx5IHNlcmllcyBpcyBpbiBvcHRpb24uXG4gICAgLy8gSWYgd2UgZG8gbm90IGhhdmUgJ3JlbW92ZUVkZ2VBbmRBZGQnLCBsZWdlbmRNb2RlbC5tZXJnZU9wdGlvbiB3aWxsXG4gICAgLy8gbm90IGJlIGNhbGxlZCwgYnV0IG9ubHkgc2VyZWlzLm1lcmdlT3B0aW9uIGlzIGNhbGxlZC4gVGh1cyBsZWdlbmRcbiAgICAvLyBoYXZlIG5vIGNoYW5jZSB0byB1cGRhdGUgaXRzIGxvY2FsIHJlY29yZCBhYm91dCBzZXJpZXMgKGxpa2Ugd2hpY2hcbiAgICAvLyBuYW1lIG9mIHNlcmllcyBpcyBhdmFpbGFibGUgaW4gbGVnZW5kKS5cblxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlRWRnZUFuZEFkZChzdWNjQ29tcG9uZW50VHlwZSkge1xuICAgICAgdGFyZ2V0TmFtZVNldFtzdWNjQ29tcG9uZW50VHlwZV0gPSB0cnVlO1xuICAgICAgcmVtb3ZlRWRnZShzdWNjQ29tcG9uZW50VHlwZSk7XG4gICAgfVxuICB9O1xuICAvKipcbiAgICogRGVwbmRlbmN5R3JhcGg6IHtPYmplY3R9XG4gICAqIGtleTogY29ucG9uZW50VHlwZSxcbiAgICogdmFsdWU6IHtcbiAgICogICAgIHN1Y2Nlc3NvcjogW2NvbnBvbmVudFR5cGVzLi4uXSxcbiAgICogICAgIG9yaWdpbmFsRGVwczogW2NvbnBvbmVudFR5cGVzLi4uXSxcbiAgICogICAgIGVudHJ5Q291bnQ6IHtudW1iZXJ9XG4gICAqIH1cbiAgICovXG5cblxuICBmdW5jdGlvbiBtYWtlRGVwbmRlbmN5R3JhcGgoZnVsbE5hbWVMaXN0KSB7XG4gICAgdmFyIGdyYXBoID0ge307XG4gICAgdmFyIG5vRW50cnlMaXN0ID0gW107XG4gICAgenJVdGlsLmVhY2goZnVsbE5hbWVMaXN0LCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgdmFyIHRoaXNJdGVtID0gY3JlYXRlRGVwZW5kZW5jeUdyYXBoSXRlbShncmFwaCwgbmFtZSk7XG4gICAgICB2YXIgb3JpZ2luYWxEZXBzID0gdGhpc0l0ZW0ub3JpZ2luYWxEZXBzID0gZGVwZW5kZW5jeUdldHRlcihuYW1lKTtcbiAgICAgIHZhciBhdmFpbGFibGVEZXBzID0gZ2V0QXZhaWxhYmxlRGVwZW5kZW5jaWVzKG9yaWdpbmFsRGVwcywgZnVsbE5hbWVMaXN0KTtcbiAgICAgIHRoaXNJdGVtLmVudHJ5Q291bnQgPSBhdmFpbGFibGVEZXBzLmxlbmd0aDtcblxuICAgICAgaWYgKHRoaXNJdGVtLmVudHJ5Q291bnQgPT09IDApIHtcbiAgICAgICAgbm9FbnRyeUxpc3QucHVzaChuYW1lKTtcbiAgICAgIH1cblxuICAgICAgenJVdGlsLmVhY2goYXZhaWxhYmxlRGVwcywgZnVuY3Rpb24gKGRlcGVuZGVudE5hbWUpIHtcbiAgICAgICAgaWYgKHpyVXRpbC5pbmRleE9mKHRoaXNJdGVtLnByZWRlY2Vzc29yLCBkZXBlbmRlbnROYW1lKSA8IDApIHtcbiAgICAgICAgICB0aGlzSXRlbS5wcmVkZWNlc3Nvci5wdXNoKGRlcGVuZGVudE5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHRoYXRJdGVtID0gY3JlYXRlRGVwZW5kZW5jeUdyYXBoSXRlbShncmFwaCwgZGVwZW5kZW50TmFtZSk7XG5cbiAgICAgICAgaWYgKHpyVXRpbC5pbmRleE9mKHRoYXRJdGVtLnN1Y2Nlc3NvciwgZGVwZW5kZW50TmFtZSkgPCAwKSB7XG4gICAgICAgICAgdGhhdEl0ZW0uc3VjY2Vzc29yLnB1c2gobmFtZSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHJldHVybiB7XG4gICAgICBncmFwaDogZ3JhcGgsXG4gICAgICBub0VudHJ5TGlzdDogbm9FbnRyeUxpc3RcbiAgICB9O1xuICB9XG5cbiAgZnVuY3Rpb24gY3JlYXRlRGVwZW5kZW5jeUdyYXBoSXRlbShncmFwaCwgbmFtZSkge1xuICAgIGlmICghZ3JhcGhbbmFtZV0pIHtcbiAgICAgIGdyYXBoW25hbWVdID0ge1xuICAgICAgICBwcmVkZWNlc3NvcjogW10sXG4gICAgICAgIHN1Y2Nlc3NvcjogW11cbiAgICAgIH07XG4gICAgfVxuXG4gICAgcmV0dXJuIGdyYXBoW25hbWVdO1xuICB9XG5cbiAgZnVuY3Rpb24gZ2V0QXZhaWxhYmxlRGVwZW5kZW5jaWVzKG9yaWdpbmFsRGVwcywgZnVsbE5hbWVMaXN0KSB7XG4gICAgdmFyIGF2YWlsYWJsZURlcHMgPSBbXTtcbiAgICB6clV0aWwuZWFjaChvcmlnaW5hbERlcHMsIGZ1bmN0aW9uIChkZXApIHtcbiAgICAgIHpyVXRpbC5pbmRleE9mKGZ1bGxOYW1lTGlzdCwgZGVwKSA+PSAwICYmIGF2YWlsYWJsZURlcHMucHVzaChkZXApO1xuICAgIH0pO1xuICAgIHJldHVybiBhdmFpbGFibGVEZXBzO1xuICB9XG59XG5cbmV4cG9ydHMuZ2V0VUlEID0gZ2V0VUlEO1xuZXhwb3J0cy5lbmFibGVTdWJUeXBlRGVmYXVsdGVyID0gZW5hYmxlU3ViVHlwZURlZmF1bHRlcjtcbmV4cG9ydHMuZW5hYmxlVG9wb2xvZ2ljYWxUcmF2ZWwgPSBlbmFibGVUb3BvbG9naWNhbFRyYXZlbDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/component.js
