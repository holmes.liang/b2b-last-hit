__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    .filter-select-item {\n      &__label {\n        font-size: 11px;\n        min-height: 30px;\n        // line-height: 30px;\n        color: rgba(0,0,0,.45);\n        text-transform: uppercase;\n        line-height: 20px;\n        padding: 5px 0;\n      }\n\n      &__select {\n        min-width: 250px;\n        margin-bottom: 8px;\n        color: #333;\n      }\n    }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/* harmony default export */ __webpack_exports__["default"] = ({
  Scope: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZpbHRlci9maWx0ZXItc2VsZWN0LXN0eWxlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9maWx0ZXIvZmlsdGVyLXNlbGVjdC1zdHlsZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgU2NvcGU6IFN0eWxlZC5kaXZgXG4gICAgLmZpbHRlci1zZWxlY3QtaXRlbSB7XG4gICAgICAmX19sYWJlbCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICAgICAgbWluLWhlaWdodDogMzBweDtcbiAgICAgICAgLy8gbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgICAgIGNvbG9yOiByZ2JhKDAsMCwwLC40NSk7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICBwYWRkaW5nOiA1cHggMDtcbiAgICAgIH1cblxuICAgICAgJl9fc2VsZWN0IHtcbiAgICAgICAgbWluLXdpZHRoOiAyNTBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xuICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgIH1cbiAgICB9XG4gIGAsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/filter/filter-select-style.tsx
