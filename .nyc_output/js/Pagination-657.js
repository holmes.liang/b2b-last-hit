__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Pagination; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_pagination__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-pagination */ "./node_modules/rc-pagination/es/index.js");
/* harmony import */ var rc_pagination_es_locale_en_US__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-pagination/es/locale/en_US */ "./node_modules/rc-pagination/es/locale/en_US.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _MiniSelect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./MiniSelect */ "./node_modules/antd/es/pagination/MiniSelect.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../select */ "./node_modules/antd/es/select/index.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};











var Pagination =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Pagination, _React$Component);

  function Pagination() {
    var _this;

    _classCallCheck(this, Pagination);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Pagination).apply(this, arguments));

    _this.getIconsProps = function (prefixCls) {
      var prevIcon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        className: "".concat(prefixCls, "-item-link")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: "left"
      }));
      var nextIcon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        className: "".concat(prefixCls, "-item-link")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: "right"
      }));
      var jumpPrevIcon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        className: "".concat(prefixCls, "-item-link")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-item-container")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        className: "".concat(prefixCls, "-item-link-icon"),
        type: "double-left"
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-item-ellipsis")
      }, "\u2022\u2022\u2022")));
      var jumpNextIcon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        className: "".concat(prefixCls, "-item-link")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-item-container")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        className: "".concat(prefixCls, "-item-link-icon"),
        type: "double-right"
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-item-ellipsis")
      }, "\u2022\u2022\u2022")));
      return {
        prevIcon: prevIcon,
        nextIcon: nextIcon,
        jumpPrevIcon: jumpPrevIcon,
        jumpNextIcon: jumpNextIcon
      };
    };

    _this.renderPagination = function (contextLocale) {
      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          customizeSelectPrefixCls = _a.selectPrefixCls,
          className = _a.className,
          size = _a.size,
          customLocale = _a.locale,
          restProps = __rest(_a, ["prefixCls", "selectPrefixCls", "className", "size", "locale"]);

      var locale = _extends(_extends({}, contextLocale), customLocale);

      var isSmall = size === 'small';
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_8__["ConfigConsumer"], null, function (_ref) {
        var getPrefixCls = _ref.getPrefixCls;
        var prefixCls = getPrefixCls('pagination', customizePrefixCls);
        var selectPrefixCls = getPrefixCls('select', customizeSelectPrefixCls);
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_pagination__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, restProps, {
          prefixCls: prefixCls,
          selectPrefixCls: selectPrefixCls
        }, _this.getIconsProps(prefixCls), {
          className: classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, {
            mini: isSmall
          }),
          selectComponentClass: isSmall ? _MiniSelect__WEBPACK_IMPORTED_MODULE_4__["default"] : _select__WEBPACK_IMPORTED_MODULE_6__["default"],
          locale: locale
        }));
      });
    };

    return _this;
  }

  _createClass(Pagination, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_7__["default"], {
        componentName: "Pagination",
        defaultLocale: rc_pagination_es_locale_en_US__WEBPACK_IMPORTED_MODULE_2__["default"]
      }, this.renderPagination);
    }
  }]);

  return Pagination;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9wYWdpbmF0aW9uL1BhZ2luYXRpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3BhZ2luYXRpb24vUGFnaW5hdGlvbi5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJjUGFnaW5hdGlvbiBmcm9tICdyYy1wYWdpbmF0aW9uJztcbmltcG9ydCBlblVTIGZyb20gJ3JjLXBhZ2luYXRpb24vbGliL2xvY2FsZS9lbl9VUyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBNaW5pU2VsZWN0IGZyb20gJy4vTWluaVNlbGVjdCc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCBTZWxlY3QgZnJvbSAnLi4vc2VsZWN0JztcbmltcG9ydCBMb2NhbGVSZWNlaXZlciBmcm9tICcuLi9sb2NhbGUtcHJvdmlkZXIvTG9jYWxlUmVjZWl2ZXInO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGFnaW5hdGlvbiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuZ2V0SWNvbnNQcm9wcyA9IChwcmVmaXhDbHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHByZXZJY29uID0gKDxhIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pdGVtLWxpbmtgfT5cbiAgICAgICAgPEljb24gdHlwZT1cImxlZnRcIi8+XG4gICAgICA8L2E+KTtcbiAgICAgICAgICAgIGNvbnN0IG5leHRJY29uID0gKDxhIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pdGVtLWxpbmtgfT5cbiAgICAgICAgPEljb24gdHlwZT1cInJpZ2h0XCIvPlxuICAgICAgPC9hPik7XG4gICAgICAgICAgICBjb25zdCBqdW1wUHJldkljb24gPSAoPGEgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tbGlua2B9PlxuICAgICAgICBcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1jb250YWluZXJgfT5cbiAgICAgICAgICA8SWNvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1saW5rLWljb25gfSB0eXBlPVwiZG91YmxlLWxlZnRcIi8+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tZWxsaXBzaXNgfT7igKLigKLigKI8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9hPik7XG4gICAgICAgICAgICBjb25zdCBqdW1wTmV4dEljb24gPSAoPGEgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tbGlua2B9PlxuICAgICAgICBcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1jb250YWluZXJgfT5cbiAgICAgICAgICA8SWNvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taXRlbS1saW5rLWljb25gfSB0eXBlPVwiZG91YmxlLXJpZ2h0XCIvPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pdGVtLWVsbGlwc2lzYH0+4oCi4oCi4oCiPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvYT4pO1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBwcmV2SWNvbixcbiAgICAgICAgICAgICAgICBuZXh0SWNvbixcbiAgICAgICAgICAgICAgICBqdW1wUHJldkljb24sXG4gICAgICAgICAgICAgICAganVtcE5leHRJY29uLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJQYWdpbmF0aW9uID0gKGNvbnRleHRMb2NhbGUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc2VsZWN0UHJlZml4Q2xzOiBjdXN0b21pemVTZWxlY3RQcmVmaXhDbHMsIGNsYXNzTmFtZSwgc2l6ZSwgbG9jYWxlOiBjdXN0b21Mb2NhbGUgfSA9IF9hLCByZXN0UHJvcHMgPSBfX3Jlc3QoX2EsIFtcInByZWZpeENsc1wiLCBcInNlbGVjdFByZWZpeENsc1wiLCBcImNsYXNzTmFtZVwiLCBcInNpemVcIiwgXCJsb2NhbGVcIl0pO1xuICAgICAgICAgICAgY29uc3QgbG9jYWxlID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBjb250ZXh0TG9jYWxlKSwgY3VzdG9tTG9jYWxlKTtcbiAgICAgICAgICAgIGNvbnN0IGlzU21hbGwgPSBzaXplID09PSAnc21hbGwnO1xuICAgICAgICAgICAgcmV0dXJuICg8Q29uZmlnQ29uc3VtZXI+XG4gICAgICAgIHsoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygncGFnaW5hdGlvbicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0UHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdzZWxlY3QnLCBjdXN0b21pemVTZWxlY3RQcmVmaXhDbHMpO1xuICAgICAgICAgICAgICAgIHJldHVybiAoPFJjUGFnaW5hdGlvbiB7Li4ucmVzdFByb3BzfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gc2VsZWN0UHJlZml4Q2xzPXtzZWxlY3RQcmVmaXhDbHN9IHsuLi50aGlzLmdldEljb25zUHJvcHMocHJlZml4Q2xzKX0gY2xhc3NOYW1lPXtjbGFzc05hbWVzKGNsYXNzTmFtZSwgeyBtaW5pOiBpc1NtYWxsIH0pfSBzZWxlY3RDb21wb25lbnRDbGFzcz17aXNTbWFsbCA/IE1pbmlTZWxlY3QgOiBTZWxlY3R9IGxvY2FsZT17bG9jYWxlfS8+KTtcbiAgICAgICAgICAgIH19XG4gICAgICA8L0NvbmZpZ0NvbnN1bWVyPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuICg8TG9jYWxlUmVjZWl2ZXIgY29tcG9uZW50TmFtZT1cIlBhZ2luYXRpb25cIiBkZWZhdWx0TG9jYWxlPXtlblVTfT5cbiAgICAgICAge3RoaXMucmVuZGVyUGFnaW5hdGlvbn1cbiAgICAgIDwvTG9jYWxlUmVjZWl2ZXI+KTtcbiAgICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBckJBO0FBQ0E7QUEyQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBSkE7QUFDQTtBQS9CQTtBQTBDQTtBQUNBOzs7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7Ozs7QUFoREE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/pagination/Pagination.js
