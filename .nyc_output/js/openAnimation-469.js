__webpack_require__.r(__webpack_exports__);
/* harmony import */ var css_animation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! css-animation */ "./node_modules/css-animation/es/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_1__);
/**
 * Deprecated. We should replace the animation with pure react motion instead of modify style directly.
 * If you are creating new component with animation, please use `./motion`.
 */



function animate(node, show, done) {
  var height;
  var requestAnimationFrameId;
  return Object(css_animation__WEBPACK_IMPORTED_MODULE_0__["default"])(node, 'ant-motion-collapse-legacy', {
    start: function start() {
      if (!show) {
        node.style.height = "".concat(node.offsetHeight, "px");
        node.style.opacity = '1';
      } else {
        height = node.offsetHeight;
        node.style.height = '0px';
        node.style.opacity = '0';
      }
    },
    active: function active() {
      if (requestAnimationFrameId) {
        raf__WEBPACK_IMPORTED_MODULE_1___default.a.cancel(requestAnimationFrameId);
      }

      requestAnimationFrameId = raf__WEBPACK_IMPORTED_MODULE_1___default()(function () {
        node.style.height = "".concat(show ? height : 0, "px");
        node.style.opacity = show ? '1' : '0';
      });
    },
    end: function end() {
      if (requestAnimationFrameId) {
        raf__WEBPACK_IMPORTED_MODULE_1___default.a.cancel(requestAnimationFrameId);
      }

      node.style.height = '';
      node.style.opacity = '';
      done();
    }
  });
}

var animation = {
  enter: function enter(node, done) {
    return animate(node, true, done);
  },
  leave: function leave(node, done) {
    return animate(node, false, done);
  },
  appear: function appear(node, done) {
    return animate(node, true, done);
  }
};
/* harmony default export */ __webpack_exports__["default"] = (animation);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9vcGVuQW5pbWF0aW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9fdXRpbC9vcGVuQW5pbWF0aW9uLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIERlcHJlY2F0ZWQuIFdlIHNob3VsZCByZXBsYWNlIHRoZSBhbmltYXRpb24gd2l0aCBwdXJlIHJlYWN0IG1vdGlvbiBpbnN0ZWFkIG9mIG1vZGlmeSBzdHlsZSBkaXJlY3RseS5cbiAqIElmIHlvdSBhcmUgY3JlYXRpbmcgbmV3IGNvbXBvbmVudCB3aXRoIGFuaW1hdGlvbiwgcGxlYXNlIHVzZSBgLi9tb3Rpb25gLlxuICovXG5pbXBvcnQgY3NzQW5pbWF0aW9uIGZyb20gJ2Nzcy1hbmltYXRpb24nO1xuaW1wb3J0IHJhZiBmcm9tICdyYWYnO1xuZnVuY3Rpb24gYW5pbWF0ZShub2RlLCBzaG93LCBkb25lKSB7XG4gICAgbGV0IGhlaWdodDtcbiAgICBsZXQgcmVxdWVzdEFuaW1hdGlvbkZyYW1lSWQ7XG4gICAgcmV0dXJuIGNzc0FuaW1hdGlvbihub2RlLCAnYW50LW1vdGlvbi1jb2xsYXBzZS1sZWdhY3knLCB7XG4gICAgICAgIHN0YXJ0KCkge1xuICAgICAgICAgICAgaWYgKCFzaG93KSB7XG4gICAgICAgICAgICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSBgJHtub2RlLm9mZnNldEhlaWdodH1weGA7XG4gICAgICAgICAgICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzEnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgaGVpZ2h0ID0gbm9kZS5vZmZzZXRIZWlnaHQ7XG4gICAgICAgICAgICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSAnMHB4JztcbiAgICAgICAgICAgICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGFjdGl2ZSgpIHtcbiAgICAgICAgICAgIGlmIChyZXF1ZXN0QW5pbWF0aW9uRnJhbWVJZCkge1xuICAgICAgICAgICAgICAgIHJhZi5jYW5jZWwocmVxdWVzdEFuaW1hdGlvbkZyYW1lSWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lSWQgPSByYWYoKCkgPT4ge1xuICAgICAgICAgICAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gYCR7c2hvdyA/IGhlaWdodCA6IDB9cHhgO1xuICAgICAgICAgICAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9IHNob3cgPyAnMScgOiAnMCc7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcbiAgICAgICAgZW5kKCkge1xuICAgICAgICAgICAgaWYgKHJlcXVlc3RBbmltYXRpb25GcmFtZUlkKSB7XG4gICAgICAgICAgICAgICAgcmFmLmNhbmNlbChyZXF1ZXN0QW5pbWF0aW9uRnJhbWVJZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBub2RlLnN0eWxlLmhlaWdodCA9ICcnO1xuICAgICAgICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJyc7XG4gICAgICAgICAgICBkb25lKCk7XG4gICAgICAgIH0sXG4gICAgfSk7XG59XG5jb25zdCBhbmltYXRpb24gPSB7XG4gICAgZW50ZXIobm9kZSwgZG9uZSkge1xuICAgICAgICByZXR1cm4gYW5pbWF0ZShub2RlLCB0cnVlLCBkb25lKTtcbiAgICB9LFxuICAgIGxlYXZlKG5vZGUsIGRvbmUpIHtcbiAgICAgICAgcmV0dXJuIGFuaW1hdGUobm9kZSwgZmFsc2UsIGRvbmUpO1xuICAgIH0sXG4gICAgYXBwZWFyKG5vZGUsIGRvbmUpIHtcbiAgICAgICAgcmV0dXJuIGFuaW1hdGUobm9kZSwgdHJ1ZSwgZG9uZSk7XG4gICAgfSxcbn07XG5leHBvcnQgZGVmYXVsdCBhbmltYXRpb247XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBaEJBO0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFUQTtBQVdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/openAnimation.js
