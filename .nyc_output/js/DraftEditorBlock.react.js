/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorBlock.react
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DraftEditorLeaf = __webpack_require__(/*! ./DraftEditorLeaf.react */ "./node_modules/draft-js/lib/DraftEditorLeaf.react.js");

var DraftOffsetKey = __webpack_require__(/*! ./DraftOffsetKey */ "./node_modules/draft-js/lib/DraftOffsetKey.js");

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var Scroll = __webpack_require__(/*! fbjs/lib/Scroll */ "./node_modules/fbjs/lib/Scroll.js");

var Style = __webpack_require__(/*! fbjs/lib/Style */ "./node_modules/fbjs/lib/Style.js");

var UnicodeBidi = __webpack_require__(/*! fbjs/lib/UnicodeBidi */ "./node_modules/fbjs/lib/UnicodeBidi.js");

var UnicodeBidiDirection = __webpack_require__(/*! fbjs/lib/UnicodeBidiDirection */ "./node_modules/fbjs/lib/UnicodeBidiDirection.js");

var cx = __webpack_require__(/*! fbjs/lib/cx */ "./node_modules/fbjs/lib/cx.js");

var getElementPosition = __webpack_require__(/*! fbjs/lib/getElementPosition */ "./node_modules/fbjs/lib/getElementPosition.js");

var getScrollPosition = __webpack_require__(/*! fbjs/lib/getScrollPosition */ "./node_modules/fbjs/lib/getScrollPosition.js");

var getViewportDimensions = __webpack_require__(/*! fbjs/lib/getViewportDimensions */ "./node_modules/fbjs/lib/getViewportDimensions.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var SCROLL_BUFFER = 10;
/**
 * Return whether a block overlaps with either edge of the `SelectionState`.
 */

var isBlockOnSelectionEdge = function isBlockOnSelectionEdge(selection, key) {
  return selection.getAnchorKey() === key || selection.getFocusKey() === key;
};
/**
 * The default block renderer for a `DraftEditor` component.
 *
 * A `DraftEditorBlock` is able to render a given `ContentBlock` to its
 * appropriate decorator and inline style components.
 */


var DraftEditorBlock = function (_React$Component) {
  _inherits(DraftEditorBlock, _React$Component);

  function DraftEditorBlock() {
    _classCallCheck(this, DraftEditorBlock);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  DraftEditorBlock.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
    return this.props.block !== nextProps.block || this.props.tree !== nextProps.tree || this.props.direction !== nextProps.direction || isBlockOnSelectionEdge(nextProps.selection, nextProps.block.getKey()) && nextProps.forceSelection;
  };
  /**
   * When a block is mounted and overlaps the selection state, we need to make
   * sure that the cursor is visible to match native behavior. This may not
   * be the case if the user has pressed `RETURN` or pasted some content, since
   * programatically creating these new blocks and setting the DOM selection
   * will miss out on the browser natively scrolling to that position.
   *
   * To replicate native behavior, if the block overlaps the selection state
   * on mount, force the scroll position. Check the scroll state of the scroll
   * parent, and adjust it to align the entire block to the bottom of the
   * scroll parent.
   */


  DraftEditorBlock.prototype.componentDidMount = function componentDidMount() {
    var selection = this.props.selection;
    var endKey = selection.getEndKey();

    if (!selection.getHasFocus() || endKey !== this.props.block.getKey()) {
      return;
    }

    var blockNode = ReactDOM.findDOMNode(this);
    var scrollParent = Style.getScrollParent(blockNode);
    var scrollPosition = getScrollPosition(scrollParent);
    var scrollDelta = void 0;

    if (scrollParent === window) {
      var nodePosition = getElementPosition(blockNode);
      var nodeBottom = nodePosition.y + nodePosition.height;
      var viewportHeight = getViewportDimensions().height;
      scrollDelta = nodeBottom - viewportHeight;

      if (scrollDelta > 0) {
        window.scrollTo(scrollPosition.x, scrollPosition.y + scrollDelta + SCROLL_BUFFER);
      }
    } else {
      !(blockNode instanceof HTMLElement) ?  true ? invariant(false, 'blockNode is not an HTMLElement') : undefined : void 0;
      var blockBottom = blockNode.offsetHeight + blockNode.offsetTop;
      var scrollBottom = scrollParent.offsetHeight + scrollPosition.y;
      scrollDelta = blockBottom - scrollBottom;

      if (scrollDelta > 0) {
        Scroll.setTop(scrollParent, Scroll.getTop(scrollParent) + scrollDelta + SCROLL_BUFFER);
      }
    }
  };

  DraftEditorBlock.prototype._renderChildren = function _renderChildren() {
    var _this2 = this;

    var block = this.props.block;
    var blockKey = block.getKey();
    var text = block.getText();
    var lastLeafSet = this.props.tree.size - 1;
    var hasSelection = isBlockOnSelectionEdge(this.props.selection, blockKey);
    return this.props.tree.map(function (leafSet, ii) {
      var leavesForLeafSet = leafSet.get('leaves');
      var lastLeaf = leavesForLeafSet.size - 1;
      var leaves = leavesForLeafSet.map(function (leaf, jj) {
        var offsetKey = DraftOffsetKey.encode(blockKey, ii, jj);
        var start = leaf.get('start');
        var end = leaf.get('end');
        return React.createElement(DraftEditorLeaf, {
          key: offsetKey,
          offsetKey: offsetKey,
          block: block,
          start: start,
          selection: hasSelection ? _this2.props.selection : null,
          forceSelection: _this2.props.forceSelection,
          text: text.slice(start, end),
          styleSet: block.getInlineStyleAt(start),
          customStyleMap: _this2.props.customStyleMap,
          customStyleFn: _this2.props.customStyleFn,
          isLast: ii === lastLeafSet && jj === lastLeaf
        });
      }).toArray();
      var decoratorKey = leafSet.get('decoratorKey');

      if (decoratorKey == null) {
        return leaves;
      }

      if (!_this2.props.decorator) {
        return leaves;
      }

      var decorator = nullthrows(_this2.props.decorator);
      var DecoratorComponent = decorator.getComponentForKey(decoratorKey);

      if (!DecoratorComponent) {
        return leaves;
      }

      var decoratorProps = decorator.getPropsForKey(decoratorKey);
      var decoratorOffsetKey = DraftOffsetKey.encode(blockKey, ii, 0);
      var decoratedText = text.slice(leavesForLeafSet.first().get('start'), leavesForLeafSet.last().get('end')); // Resetting dir to the same value on a child node makes Chrome/Firefox
      // confused on cursor movement. See http://jsfiddle.net/d157kLck/3/

      var dir = UnicodeBidiDirection.getHTMLDirIfDifferent(UnicodeBidi.getDirection(decoratedText), _this2.props.direction);
      return React.createElement(DecoratorComponent, _extends({}, decoratorProps, {
        contentState: _this2.props.contentState,
        decoratedText: decoratedText,
        dir: dir,
        key: decoratorOffsetKey,
        entityKey: block.getEntityAt(leafSet.get('start')),
        offsetKey: decoratorOffsetKey
      }), leaves);
    }).toArray();
  };

  DraftEditorBlock.prototype.render = function render() {
    var _props = this.props,
        direction = _props.direction,
        offsetKey = _props.offsetKey;
    var className = cx({
      'public/DraftStyleDefault/block': true,
      'public/DraftStyleDefault/ltr': direction === 'LTR',
      'public/DraftStyleDefault/rtl': direction === 'RTL'
    });
    return React.createElement('div', {
      'data-offset-key': offsetKey,
      className: className
    }, this._renderChildren());
  };

  return DraftEditorBlock;
}(React.Component);

module.exports = DraftEditorBlock;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yQmxvY2sucmVhY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFZGl0b3JCbG9jay5yZWFjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RWRpdG9yQmxvY2sucmVhY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIERyYWZ0RWRpdG9yTGVhZiA9IHJlcXVpcmUoJy4vRHJhZnRFZGl0b3JMZWFmLnJlYWN0Jyk7XG52YXIgRHJhZnRPZmZzZXRLZXkgPSByZXF1aXJlKCcuL0RyYWZ0T2Zmc2V0S2V5Jyk7XG52YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xudmFyIFJlYWN0RE9NID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG52YXIgU2Nyb2xsID0gcmVxdWlyZSgnZmJqcy9saWIvU2Nyb2xsJyk7XG52YXIgU3R5bGUgPSByZXF1aXJlKCdmYmpzL2xpYi9TdHlsZScpO1xudmFyIFVuaWNvZGVCaWRpID0gcmVxdWlyZSgnZmJqcy9saWIvVW5pY29kZUJpZGknKTtcbnZhciBVbmljb2RlQmlkaURpcmVjdGlvbiA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVCaWRpRGlyZWN0aW9uJyk7XG5cbnZhciBjeCA9IHJlcXVpcmUoJ2ZianMvbGliL2N4Jyk7XG52YXIgZ2V0RWxlbWVudFBvc2l0aW9uID0gcmVxdWlyZSgnZmJqcy9saWIvZ2V0RWxlbWVudFBvc2l0aW9uJyk7XG52YXIgZ2V0U2Nyb2xsUG9zaXRpb24gPSByZXF1aXJlKCdmYmpzL2xpYi9nZXRTY3JvbGxQb3NpdGlvbicpO1xudmFyIGdldFZpZXdwb3J0RGltZW5zaW9ucyA9IHJlcXVpcmUoJ2ZianMvbGliL2dldFZpZXdwb3J0RGltZW5zaW9ucycpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xudmFyIG51bGx0aHJvd3MgPSByZXF1aXJlKCdmYmpzL2xpYi9udWxsdGhyb3dzJyk7XG5cbnZhciBTQ1JPTExfQlVGRkVSID0gMTA7XG5cbi8qKlxuICogUmV0dXJuIHdoZXRoZXIgYSBibG9jayBvdmVybGFwcyB3aXRoIGVpdGhlciBlZGdlIG9mIHRoZSBgU2VsZWN0aW9uU3RhdGVgLlxuICovXG52YXIgaXNCbG9ja09uU2VsZWN0aW9uRWRnZSA9IGZ1bmN0aW9uIGlzQmxvY2tPblNlbGVjdGlvbkVkZ2Uoc2VsZWN0aW9uLCBrZXkpIHtcbiAgcmV0dXJuIHNlbGVjdGlvbi5nZXRBbmNob3JLZXkoKSA9PT0ga2V5IHx8IHNlbGVjdGlvbi5nZXRGb2N1c0tleSgpID09PSBrZXk7XG59O1xuXG4vKipcbiAqIFRoZSBkZWZhdWx0IGJsb2NrIHJlbmRlcmVyIGZvciBhIGBEcmFmdEVkaXRvcmAgY29tcG9uZW50LlxuICpcbiAqIEEgYERyYWZ0RWRpdG9yQmxvY2tgIGlzIGFibGUgdG8gcmVuZGVyIGEgZ2l2ZW4gYENvbnRlbnRCbG9ja2AgdG8gaXRzXG4gKiBhcHByb3ByaWF0ZSBkZWNvcmF0b3IgYW5kIGlubGluZSBzdHlsZSBjb21wb25lbnRzLlxuICovXG5cbnZhciBEcmFmdEVkaXRvckJsb2NrID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKERyYWZ0RWRpdG9yQmxvY2ssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERyYWZ0RWRpdG9yQmxvY2soKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIERyYWZ0RWRpdG9yQmxvY2spO1xuXG4gICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICBEcmFmdEVkaXRvckJsb2NrLnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMuYmxvY2sgIT09IG5leHRQcm9wcy5ibG9jayB8fCB0aGlzLnByb3BzLnRyZWUgIT09IG5leHRQcm9wcy50cmVlIHx8IHRoaXMucHJvcHMuZGlyZWN0aW9uICE9PSBuZXh0UHJvcHMuZGlyZWN0aW9uIHx8IGlzQmxvY2tPblNlbGVjdGlvbkVkZ2UobmV4dFByb3BzLnNlbGVjdGlvbiwgbmV4dFByb3BzLmJsb2NrLmdldEtleSgpKSAmJiBuZXh0UHJvcHMuZm9yY2VTZWxlY3Rpb247XG4gIH07XG5cbiAgLyoqXG4gICAqIFdoZW4gYSBibG9jayBpcyBtb3VudGVkIGFuZCBvdmVybGFwcyB0aGUgc2VsZWN0aW9uIHN0YXRlLCB3ZSBuZWVkIHRvIG1ha2VcbiAgICogc3VyZSB0aGF0IHRoZSBjdXJzb3IgaXMgdmlzaWJsZSB0byBtYXRjaCBuYXRpdmUgYmVoYXZpb3IuIFRoaXMgbWF5IG5vdFxuICAgKiBiZSB0aGUgY2FzZSBpZiB0aGUgdXNlciBoYXMgcHJlc3NlZCBgUkVUVVJOYCBvciBwYXN0ZWQgc29tZSBjb250ZW50LCBzaW5jZVxuICAgKiBwcm9ncmFtYXRpY2FsbHkgY3JlYXRpbmcgdGhlc2UgbmV3IGJsb2NrcyBhbmQgc2V0dGluZyB0aGUgRE9NIHNlbGVjdGlvblxuICAgKiB3aWxsIG1pc3Mgb3V0IG9uIHRoZSBicm93c2VyIG5hdGl2ZWx5IHNjcm9sbGluZyB0byB0aGF0IHBvc2l0aW9uLlxuICAgKlxuICAgKiBUbyByZXBsaWNhdGUgbmF0aXZlIGJlaGF2aW9yLCBpZiB0aGUgYmxvY2sgb3ZlcmxhcHMgdGhlIHNlbGVjdGlvbiBzdGF0ZVxuICAgKiBvbiBtb3VudCwgZm9yY2UgdGhlIHNjcm9sbCBwb3NpdGlvbi4gQ2hlY2sgdGhlIHNjcm9sbCBzdGF0ZSBvZiB0aGUgc2Nyb2xsXG4gICAqIHBhcmVudCwgYW5kIGFkanVzdCBpdCB0byBhbGlnbiB0aGUgZW50aXJlIGJsb2NrIHRvIHRoZSBib3R0b20gb2YgdGhlXG4gICAqIHNjcm9sbCBwYXJlbnQuXG4gICAqL1xuXG5cbiAgRHJhZnRFZGl0b3JCbG9jay5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB2YXIgc2VsZWN0aW9uID0gdGhpcy5wcm9wcy5zZWxlY3Rpb247XG4gICAgdmFyIGVuZEtleSA9IHNlbGVjdGlvbi5nZXRFbmRLZXkoKTtcbiAgICBpZiAoIXNlbGVjdGlvbi5nZXRIYXNGb2N1cygpIHx8IGVuZEtleSAhPT0gdGhpcy5wcm9wcy5ibG9jay5nZXRLZXkoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBibG9ja05vZGUgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKTtcbiAgICB2YXIgc2Nyb2xsUGFyZW50ID0gU3R5bGUuZ2V0U2Nyb2xsUGFyZW50KGJsb2NrTm9kZSk7XG4gICAgdmFyIHNjcm9sbFBvc2l0aW9uID0gZ2V0U2Nyb2xsUG9zaXRpb24oc2Nyb2xsUGFyZW50KTtcbiAgICB2YXIgc2Nyb2xsRGVsdGEgPSB2b2lkIDA7XG5cbiAgICBpZiAoc2Nyb2xsUGFyZW50ID09PSB3aW5kb3cpIHtcbiAgICAgIHZhciBub2RlUG9zaXRpb24gPSBnZXRFbGVtZW50UG9zaXRpb24oYmxvY2tOb2RlKTtcbiAgICAgIHZhciBub2RlQm90dG9tID0gbm9kZVBvc2l0aW9uLnkgKyBub2RlUG9zaXRpb24uaGVpZ2h0O1xuICAgICAgdmFyIHZpZXdwb3J0SGVpZ2h0ID0gZ2V0Vmlld3BvcnREaW1lbnNpb25zKCkuaGVpZ2h0O1xuICAgICAgc2Nyb2xsRGVsdGEgPSBub2RlQm90dG9tIC0gdmlld3BvcnRIZWlnaHQ7XG4gICAgICBpZiAoc2Nyb2xsRGVsdGEgPiAwKSB7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbyhzY3JvbGxQb3NpdGlvbi54LCBzY3JvbGxQb3NpdGlvbi55ICsgc2Nyb2xsRGVsdGEgKyBTQ1JPTExfQlVGRkVSKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgIShibG9ja05vZGUgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnYmxvY2tOb2RlIGlzIG5vdCBhbiBIVE1MRWxlbWVudCcpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgICAgIHZhciBibG9ja0JvdHRvbSA9IGJsb2NrTm9kZS5vZmZzZXRIZWlnaHQgKyBibG9ja05vZGUub2Zmc2V0VG9wO1xuICAgICAgdmFyIHNjcm9sbEJvdHRvbSA9IHNjcm9sbFBhcmVudC5vZmZzZXRIZWlnaHQgKyBzY3JvbGxQb3NpdGlvbi55O1xuICAgICAgc2Nyb2xsRGVsdGEgPSBibG9ja0JvdHRvbSAtIHNjcm9sbEJvdHRvbTtcbiAgICAgIGlmIChzY3JvbGxEZWx0YSA+IDApIHtcbiAgICAgICAgU2Nyb2xsLnNldFRvcChzY3JvbGxQYXJlbnQsIFNjcm9sbC5nZXRUb3Aoc2Nyb2xsUGFyZW50KSArIHNjcm9sbERlbHRhICsgU0NST0xMX0JVRkZFUik7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIERyYWZ0RWRpdG9yQmxvY2sucHJvdG90eXBlLl9yZW5kZXJDaGlsZHJlbiA9IGZ1bmN0aW9uIF9yZW5kZXJDaGlsZHJlbigpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHZhciBibG9jayA9IHRoaXMucHJvcHMuYmxvY2s7XG4gICAgdmFyIGJsb2NrS2V5ID0gYmxvY2suZ2V0S2V5KCk7XG4gICAgdmFyIHRleHQgPSBibG9jay5nZXRUZXh0KCk7XG4gICAgdmFyIGxhc3RMZWFmU2V0ID0gdGhpcy5wcm9wcy50cmVlLnNpemUgLSAxO1xuICAgIHZhciBoYXNTZWxlY3Rpb24gPSBpc0Jsb2NrT25TZWxlY3Rpb25FZGdlKHRoaXMucHJvcHMuc2VsZWN0aW9uLCBibG9ja0tleSk7XG5cbiAgICByZXR1cm4gdGhpcy5wcm9wcy50cmVlLm1hcChmdW5jdGlvbiAobGVhZlNldCwgaWkpIHtcbiAgICAgIHZhciBsZWF2ZXNGb3JMZWFmU2V0ID0gbGVhZlNldC5nZXQoJ2xlYXZlcycpO1xuICAgICAgdmFyIGxhc3RMZWFmID0gbGVhdmVzRm9yTGVhZlNldC5zaXplIC0gMTtcbiAgICAgIHZhciBsZWF2ZXMgPSBsZWF2ZXNGb3JMZWFmU2V0Lm1hcChmdW5jdGlvbiAobGVhZiwgamopIHtcbiAgICAgICAgdmFyIG9mZnNldEtleSA9IERyYWZ0T2Zmc2V0S2V5LmVuY29kZShibG9ja0tleSwgaWksIGpqKTtcbiAgICAgICAgdmFyIHN0YXJ0ID0gbGVhZi5nZXQoJ3N0YXJ0Jyk7XG4gICAgICAgIHZhciBlbmQgPSBsZWFmLmdldCgnZW5kJyk7XG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KERyYWZ0RWRpdG9yTGVhZiwge1xuICAgICAgICAgIGtleTogb2Zmc2V0S2V5LFxuICAgICAgICAgIG9mZnNldEtleTogb2Zmc2V0S2V5LFxuICAgICAgICAgIGJsb2NrOiBibG9jayxcbiAgICAgICAgICBzdGFydDogc3RhcnQsXG4gICAgICAgICAgc2VsZWN0aW9uOiBoYXNTZWxlY3Rpb24gPyBfdGhpczIucHJvcHMuc2VsZWN0aW9uIDogbnVsbCxcbiAgICAgICAgICBmb3JjZVNlbGVjdGlvbjogX3RoaXMyLnByb3BzLmZvcmNlU2VsZWN0aW9uLFxuICAgICAgICAgIHRleHQ6IHRleHQuc2xpY2Uoc3RhcnQsIGVuZCksXG4gICAgICAgICAgc3R5bGVTZXQ6IGJsb2NrLmdldElubGluZVN0eWxlQXQoc3RhcnQpLFxuICAgICAgICAgIGN1c3RvbVN0eWxlTWFwOiBfdGhpczIucHJvcHMuY3VzdG9tU3R5bGVNYXAsXG4gICAgICAgICAgY3VzdG9tU3R5bGVGbjogX3RoaXMyLnByb3BzLmN1c3RvbVN0eWxlRm4sXG4gICAgICAgICAgaXNMYXN0OiBpaSA9PT0gbGFzdExlYWZTZXQgJiYgamogPT09IGxhc3RMZWFmXG4gICAgICAgIH0pO1xuICAgICAgfSkudG9BcnJheSgpO1xuXG4gICAgICB2YXIgZGVjb3JhdG9yS2V5ID0gbGVhZlNldC5nZXQoJ2RlY29yYXRvcktleScpO1xuICAgICAgaWYgKGRlY29yYXRvcktleSA9PSBudWxsKSB7XG4gICAgICAgIHJldHVybiBsZWF2ZXM7XG4gICAgICB9XG5cbiAgICAgIGlmICghX3RoaXMyLnByb3BzLmRlY29yYXRvcikge1xuICAgICAgICByZXR1cm4gbGVhdmVzO1xuICAgICAgfVxuXG4gICAgICB2YXIgZGVjb3JhdG9yID0gbnVsbHRocm93cyhfdGhpczIucHJvcHMuZGVjb3JhdG9yKTtcblxuICAgICAgdmFyIERlY29yYXRvckNvbXBvbmVudCA9IGRlY29yYXRvci5nZXRDb21wb25lbnRGb3JLZXkoZGVjb3JhdG9yS2V5KTtcbiAgICAgIGlmICghRGVjb3JhdG9yQ29tcG9uZW50KSB7XG4gICAgICAgIHJldHVybiBsZWF2ZXM7XG4gICAgICB9XG5cbiAgICAgIHZhciBkZWNvcmF0b3JQcm9wcyA9IGRlY29yYXRvci5nZXRQcm9wc0ZvcktleShkZWNvcmF0b3JLZXkpO1xuICAgICAgdmFyIGRlY29yYXRvck9mZnNldEtleSA9IERyYWZ0T2Zmc2V0S2V5LmVuY29kZShibG9ja0tleSwgaWksIDApO1xuICAgICAgdmFyIGRlY29yYXRlZFRleHQgPSB0ZXh0LnNsaWNlKGxlYXZlc0ZvckxlYWZTZXQuZmlyc3QoKS5nZXQoJ3N0YXJ0JyksIGxlYXZlc0ZvckxlYWZTZXQubGFzdCgpLmdldCgnZW5kJykpO1xuXG4gICAgICAvLyBSZXNldHRpbmcgZGlyIHRvIHRoZSBzYW1lIHZhbHVlIG9uIGEgY2hpbGQgbm9kZSBtYWtlcyBDaHJvbWUvRmlyZWZveFxuICAgICAgLy8gY29uZnVzZWQgb24gY3Vyc29yIG1vdmVtZW50LiBTZWUgaHR0cDovL2pzZmlkZGxlLm5ldC9kMTU3a0xjay8zL1xuICAgICAgdmFyIGRpciA9IFVuaWNvZGVCaWRpRGlyZWN0aW9uLmdldEhUTUxEaXJJZkRpZmZlcmVudChVbmljb2RlQmlkaS5nZXREaXJlY3Rpb24oZGVjb3JhdGVkVGV4dCksIF90aGlzMi5wcm9wcy5kaXJlY3Rpb24pO1xuXG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgRGVjb3JhdG9yQ29tcG9uZW50LFxuICAgICAgICBfZXh0ZW5kcyh7fSwgZGVjb3JhdG9yUHJvcHMsIHtcbiAgICAgICAgICBjb250ZW50U3RhdGU6IF90aGlzMi5wcm9wcy5jb250ZW50U3RhdGUsXG4gICAgICAgICAgZGVjb3JhdGVkVGV4dDogZGVjb3JhdGVkVGV4dCxcbiAgICAgICAgICBkaXI6IGRpcixcbiAgICAgICAgICBrZXk6IGRlY29yYXRvck9mZnNldEtleSxcbiAgICAgICAgICBlbnRpdHlLZXk6IGJsb2NrLmdldEVudGl0eUF0KGxlYWZTZXQuZ2V0KCdzdGFydCcpKSxcbiAgICAgICAgICBvZmZzZXRLZXk6IGRlY29yYXRvck9mZnNldEtleSB9KSxcbiAgICAgICAgbGVhdmVzXG4gICAgICApO1xuICAgIH0pLnRvQXJyYXkoKTtcbiAgfTtcblxuICBEcmFmdEVkaXRvckJsb2NrLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIGRpcmVjdGlvbiA9IF9wcm9wcy5kaXJlY3Rpb24sXG4gICAgICAgIG9mZnNldEtleSA9IF9wcm9wcy5vZmZzZXRLZXk7XG5cbiAgICB2YXIgY2xhc3NOYW1lID0gY3goe1xuICAgICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9ibG9jayc6IHRydWUsXG4gICAgICAncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L2x0cic6IGRpcmVjdGlvbiA9PT0gJ0xUUicsXG4gICAgICAncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L3J0bCc6IGRpcmVjdGlvbiA9PT0gJ1JUTCdcbiAgICB9KTtcblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ2RpdicsXG4gICAgICB7ICdkYXRhLW9mZnNldC1rZXknOiBvZmZzZXRLZXksIGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sXG4gICAgICB0aGlzLl9yZW5kZXJDaGlsZHJlbigpXG4gICAgKTtcbiAgfTtcblxuICByZXR1cm4gRHJhZnRFZGl0b3JCbG9jaztcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxubW9kdWxlLmV4cG9ydHMgPSBEcmFmdEVkaXRvckJsb2NrOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTs7OztBQUdBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFFQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorBlock.react.js
