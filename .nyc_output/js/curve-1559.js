var _vector = __webpack_require__(/*! ./vector */ "./node_modules/zrender/lib/core/vector.js");

var v2Create = _vector.create;
var v2DistSquare = _vector.distSquare;
/**
 * 曲线辅助模块
 * @module zrender/core/curve
 * @author pissang(https://www.github.com/pissang)
 */

var mathPow = Math.pow;
var mathSqrt = Math.sqrt;
var EPSILON = 1e-8;
var EPSILON_NUMERIC = 1e-4;
var THREE_SQRT = mathSqrt(3);
var ONE_THIRD = 1 / 3; // 临时变量

var _v0 = v2Create();

var _v1 = v2Create();

var _v2 = v2Create();

function isAroundZero(val) {
  return val > -EPSILON && val < EPSILON;
}

function isNotAroundZero(val) {
  return val > EPSILON || val < -EPSILON;
}
/**
 * 计算三次贝塞尔值
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {number} t
 * @return {number}
 */


function cubicAt(p0, p1, p2, p3, t) {
  var onet = 1 - t;
  return onet * onet * (onet * p0 + 3 * t * p1) + t * t * (t * p3 + 3 * onet * p2);
}
/**
 * 计算三次贝塞尔导数值
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {number} t
 * @return {number}
 */


function cubicDerivativeAt(p0, p1, p2, p3, t) {
  var onet = 1 - t;
  return 3 * (((p1 - p0) * onet + 2 * (p2 - p1) * t) * onet + (p3 - p2) * t * t);
}
/**
 * 计算三次贝塞尔方程根，使用盛金公式
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {number} val
 * @param  {Array.<number>} roots
 * @return {number} 有效根数目
 */


function cubicRootAt(p0, p1, p2, p3, val, roots) {
  // Evaluate roots of cubic functions
  var a = p3 + 3 * (p1 - p2) - p0;
  var b = 3 * (p2 - p1 * 2 + p0);
  var c = 3 * (p1 - p0);
  var d = p0 - val;
  var A = b * b - 3 * a * c;
  var B = b * c - 9 * a * d;
  var C = c * c - 3 * b * d;
  var n = 0;

  if (isAroundZero(A) && isAroundZero(B)) {
    if (isAroundZero(b)) {
      roots[0] = 0;
    } else {
      var t1 = -c / b; //t1, t2, t3, b is not zero

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }
    }
  } else {
    var disc = B * B - 4 * A * C;

    if (isAroundZero(disc)) {
      var K = B / A;
      var t1 = -b / a + K; // t1, a is not zero

      var t2 = -K / 2; // t2, t3

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }

      if (t2 >= 0 && t2 <= 1) {
        roots[n++] = t2;
      }
    } else if (disc > 0) {
      var discSqrt = mathSqrt(disc);
      var Y1 = A * b + 1.5 * a * (-B + discSqrt);
      var Y2 = A * b + 1.5 * a * (-B - discSqrt);

      if (Y1 < 0) {
        Y1 = -mathPow(-Y1, ONE_THIRD);
      } else {
        Y1 = mathPow(Y1, ONE_THIRD);
      }

      if (Y2 < 0) {
        Y2 = -mathPow(-Y2, ONE_THIRD);
      } else {
        Y2 = mathPow(Y2, ONE_THIRD);
      }

      var t1 = (-b - (Y1 + Y2)) / (3 * a);

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }
    } else {
      var T = (2 * A * b - 3 * a * B) / (2 * mathSqrt(A * A * A));
      var theta = Math.acos(T) / 3;
      var ASqrt = mathSqrt(A);
      var tmp = Math.cos(theta);
      var t1 = (-b - 2 * ASqrt * tmp) / (3 * a);
      var t2 = (-b + ASqrt * (tmp + THREE_SQRT * Math.sin(theta))) / (3 * a);
      var t3 = (-b + ASqrt * (tmp - THREE_SQRT * Math.sin(theta))) / (3 * a);

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }

      if (t2 >= 0 && t2 <= 1) {
        roots[n++] = t2;
      }

      if (t3 >= 0 && t3 <= 1) {
        roots[n++] = t3;
      }
    }
  }

  return n;
}
/**
 * 计算三次贝塞尔方程极限值的位置
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {Array.<number>} extrema
 * @return {number} 有效数目
 */


function cubicExtrema(p0, p1, p2, p3, extrema) {
  var b = 6 * p2 - 12 * p1 + 6 * p0;
  var a = 9 * p1 + 3 * p3 - 3 * p0 - 9 * p2;
  var c = 3 * p1 - 3 * p0;
  var n = 0;

  if (isAroundZero(a)) {
    if (isNotAroundZero(b)) {
      var t1 = -c / b;

      if (t1 >= 0 && t1 <= 1) {
        extrema[n++] = t1;
      }
    }
  } else {
    var disc = b * b - 4 * a * c;

    if (isAroundZero(disc)) {
      extrema[0] = -b / (2 * a);
    } else if (disc > 0) {
      var discSqrt = mathSqrt(disc);
      var t1 = (-b + discSqrt) / (2 * a);
      var t2 = (-b - discSqrt) / (2 * a);

      if (t1 >= 0 && t1 <= 1) {
        extrema[n++] = t1;
      }

      if (t2 >= 0 && t2 <= 1) {
        extrema[n++] = t2;
      }
    }
  }

  return n;
}
/**
 * 细分三次贝塞尔曲线
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {number} t
 * @param  {Array.<number>} out
 */


function cubicSubdivide(p0, p1, p2, p3, t, out) {
  var p01 = (p1 - p0) * t + p0;
  var p12 = (p2 - p1) * t + p1;
  var p23 = (p3 - p2) * t + p2;
  var p012 = (p12 - p01) * t + p01;
  var p123 = (p23 - p12) * t + p12;
  var p0123 = (p123 - p012) * t + p012; // Seg0

  out[0] = p0;
  out[1] = p01;
  out[2] = p012;
  out[3] = p0123; // Seg1

  out[4] = p0123;
  out[5] = p123;
  out[6] = p23;
  out[7] = p3;
}
/**
 * 投射点到三次贝塞尔曲线上，返回投射距离。
 * 投射点有可能会有一个或者多个，这里只返回其中距离最短的一个。
 * @param {number} x0
 * @param {number} y0
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @param {number} x3
 * @param {number} y3
 * @param {number} x
 * @param {number} y
 * @param {Array.<number>} [out] 投射点
 * @return {number}
 */


function cubicProjectPoint(x0, y0, x1, y1, x2, y2, x3, y3, x, y, out) {
  // http://pomax.github.io/bezierinfo/#projections
  var t;
  var interval = 0.005;
  var d = Infinity;
  var prev;
  var next;
  var d1;
  var d2;
  _v0[0] = x;
  _v0[1] = y; // 先粗略估计一下可能的最小距离的 t 值
  // PENDING

  for (var _t = 0; _t < 1; _t += 0.05) {
    _v1[0] = cubicAt(x0, x1, x2, x3, _t);
    _v1[1] = cubicAt(y0, y1, y2, y3, _t);
    d1 = v2DistSquare(_v0, _v1);

    if (d1 < d) {
      t = _t;
      d = d1;
    }
  }

  d = Infinity; // At most 32 iteration

  for (var i = 0; i < 32; i++) {
    if (interval < EPSILON_NUMERIC) {
      break;
    }

    prev = t - interval;
    next = t + interval; // t - interval

    _v1[0] = cubicAt(x0, x1, x2, x3, prev);
    _v1[1] = cubicAt(y0, y1, y2, y3, prev);
    d1 = v2DistSquare(_v1, _v0);

    if (prev >= 0 && d1 < d) {
      t = prev;
      d = d1;
    } else {
      // t + interval
      _v2[0] = cubicAt(x0, x1, x2, x3, next);
      _v2[1] = cubicAt(y0, y1, y2, y3, next);
      d2 = v2DistSquare(_v2, _v0);

      if (next <= 1 && d2 < d) {
        t = next;
        d = d2;
      } else {
        interval *= 0.5;
      }
    }
  } // t


  if (out) {
    out[0] = cubicAt(x0, x1, x2, x3, t);
    out[1] = cubicAt(y0, y1, y2, y3, t);
  } // console.log(interval, i);


  return mathSqrt(d);
}
/**
 * 计算二次方贝塞尔值
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} t
 * @return {number}
 */


function quadraticAt(p0, p1, p2, t) {
  var onet = 1 - t;
  return onet * (onet * p0 + 2 * t * p1) + t * t * p2;
}
/**
 * 计算二次方贝塞尔导数值
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} t
 * @return {number}
 */


function quadraticDerivativeAt(p0, p1, p2, t) {
  return 2 * ((1 - t) * (p1 - p0) + t * (p2 - p1));
}
/**
 * 计算二次方贝塞尔方程根
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} t
 * @param  {Array.<number>} roots
 * @return {number} 有效根数目
 */


function quadraticRootAt(p0, p1, p2, val, roots) {
  var a = p0 - 2 * p1 + p2;
  var b = 2 * (p1 - p0);
  var c = p0 - val;
  var n = 0;

  if (isAroundZero(a)) {
    if (isNotAroundZero(b)) {
      var t1 = -c / b;

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }
    }
  } else {
    var disc = b * b - 4 * a * c;

    if (isAroundZero(disc)) {
      var t1 = -b / (2 * a);

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }
    } else if (disc > 0) {
      var discSqrt = mathSqrt(disc);
      var t1 = (-b + discSqrt) / (2 * a);
      var t2 = (-b - discSqrt) / (2 * a);

      if (t1 >= 0 && t1 <= 1) {
        roots[n++] = t1;
      }

      if (t2 >= 0 && t2 <= 1) {
        roots[n++] = t2;
      }
    }
  }

  return n;
}
/**
 * 计算二次贝塞尔方程极限值
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @return {number}
 */


function quadraticExtremum(p0, p1, p2) {
  var divider = p0 + p2 - 2 * p1;

  if (divider === 0) {
    // p1 is center of p0 and p2
    return 0.5;
  } else {
    return (p0 - p1) / divider;
  }
}
/**
 * 细分二次贝塞尔曲线
 * @memberOf module:zrender/core/curve
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} t
 * @param  {Array.<number>} out
 */


function quadraticSubdivide(p0, p1, p2, t, out) {
  var p01 = (p1 - p0) * t + p0;
  var p12 = (p2 - p1) * t + p1;
  var p012 = (p12 - p01) * t + p01; // Seg0

  out[0] = p0;
  out[1] = p01;
  out[2] = p012; // Seg1

  out[3] = p012;
  out[4] = p12;
  out[5] = p2;
}
/**
 * 投射点到二次贝塞尔曲线上，返回投射距离。
 * 投射点有可能会有一个或者多个，这里只返回其中距离最短的一个。
 * @param {number} x0
 * @param {number} y0
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @param {number} x
 * @param {number} y
 * @param {Array.<number>} out 投射点
 * @return {number}
 */


function quadraticProjectPoint(x0, y0, x1, y1, x2, y2, x, y, out) {
  // http://pomax.github.io/bezierinfo/#projections
  var t;
  var interval = 0.005;
  var d = Infinity;
  _v0[0] = x;
  _v0[1] = y; // 先粗略估计一下可能的最小距离的 t 值
  // PENDING

  for (var _t = 0; _t < 1; _t += 0.05) {
    _v1[0] = quadraticAt(x0, x1, x2, _t);
    _v1[1] = quadraticAt(y0, y1, y2, _t);
    var d1 = v2DistSquare(_v0, _v1);

    if (d1 < d) {
      t = _t;
      d = d1;
    }
  }

  d = Infinity; // At most 32 iteration

  for (var i = 0; i < 32; i++) {
    if (interval < EPSILON_NUMERIC) {
      break;
    }

    var prev = t - interval;
    var next = t + interval; // t - interval

    _v1[0] = quadraticAt(x0, x1, x2, prev);
    _v1[1] = quadraticAt(y0, y1, y2, prev);
    var d1 = v2DistSquare(_v1, _v0);

    if (prev >= 0 && d1 < d) {
      t = prev;
      d = d1;
    } else {
      // t + interval
      _v2[0] = quadraticAt(x0, x1, x2, next);
      _v2[1] = quadraticAt(y0, y1, y2, next);
      var d2 = v2DistSquare(_v2, _v0);

      if (next <= 1 && d2 < d) {
        t = next;
        d = d2;
      } else {
        interval *= 0.5;
      }
    }
  } // t


  if (out) {
    out[0] = quadraticAt(x0, x1, x2, t);
    out[1] = quadraticAt(y0, y1, y2, t);
  } // console.log(interval, i);


  return mathSqrt(d);
}

exports.cubicAt = cubicAt;
exports.cubicDerivativeAt = cubicDerivativeAt;
exports.cubicRootAt = cubicRootAt;
exports.cubicExtrema = cubicExtrema;
exports.cubicSubdivide = cubicSubdivide;
exports.cubicProjectPoint = cubicProjectPoint;
exports.quadraticAt = quadraticAt;
exports.quadraticDerivativeAt = quadraticDerivativeAt;
exports.quadraticRootAt = quadraticRootAt;
exports.quadraticExtremum = quadraticExtremum;
exports.quadraticSubdivide = quadraticSubdivide;
exports.quadraticProjectPoint = quadraticProjectPoint;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9jdXJ2ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2NvcmUvY3VydmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF92ZWN0b3IgPSByZXF1aXJlKFwiLi92ZWN0b3JcIik7XG5cbnZhciB2MkNyZWF0ZSA9IF92ZWN0b3IuY3JlYXRlO1xudmFyIHYyRGlzdFNxdWFyZSA9IF92ZWN0b3IuZGlzdFNxdWFyZTtcblxuLyoqXG4gKiDmm7Lnur/ovoXliqnmqKHlnZdcbiAqIEBtb2R1bGUgenJlbmRlci9jb3JlL2N1cnZlXG4gKiBAYXV0aG9yIHBpc3NhbmcoaHR0cHM6Ly93d3cuZ2l0aHViLmNvbS9waXNzYW5nKVxuICovXG52YXIgbWF0aFBvdyA9IE1hdGgucG93O1xudmFyIG1hdGhTcXJ0ID0gTWF0aC5zcXJ0O1xudmFyIEVQU0lMT04gPSAxZS04O1xudmFyIEVQU0lMT05fTlVNRVJJQyA9IDFlLTQ7XG52YXIgVEhSRUVfU1FSVCA9IG1hdGhTcXJ0KDMpO1xudmFyIE9ORV9USElSRCA9IDEgLyAzOyAvLyDkuLTml7blj5jph49cblxudmFyIF92MCA9IHYyQ3JlYXRlKCk7XG5cbnZhciBfdjEgPSB2MkNyZWF0ZSgpO1xuXG52YXIgX3YyID0gdjJDcmVhdGUoKTtcblxuZnVuY3Rpb24gaXNBcm91bmRaZXJvKHZhbCkge1xuICByZXR1cm4gdmFsID4gLUVQU0lMT04gJiYgdmFsIDwgRVBTSUxPTjtcbn1cblxuZnVuY3Rpb24gaXNOb3RBcm91bmRaZXJvKHZhbCkge1xuICByZXR1cm4gdmFsID4gRVBTSUxPTiB8fCB2YWwgPCAtRVBTSUxPTjtcbn1cbi8qKlxuICog6K6h566X5LiJ5qyh6LSd5aGe5bCU5YC8XG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS9jdXJ2ZVxuICogQHBhcmFtICB7bnVtYmVyfSBwMFxuICogQHBhcmFtICB7bnVtYmVyfSBwMVxuICogQHBhcmFtICB7bnVtYmVyfSBwMlxuICogQHBhcmFtICB7bnVtYmVyfSBwM1xuICogQHBhcmFtICB7bnVtYmVyfSB0XG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBjdWJpY0F0KHAwLCBwMSwgcDIsIHAzLCB0KSB7XG4gIHZhciBvbmV0ID0gMSAtIHQ7XG4gIHJldHVybiBvbmV0ICogb25ldCAqIChvbmV0ICogcDAgKyAzICogdCAqIHAxKSArIHQgKiB0ICogKHQgKiBwMyArIDMgKiBvbmV0ICogcDIpO1xufVxuLyoqXG4gKiDorqHnrpfkuInmrKHotJ3loZ7lsJTlr7zmlbDlgLxcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL2N1cnZlXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAwXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAzXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHRcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGN1YmljRGVyaXZhdGl2ZUF0KHAwLCBwMSwgcDIsIHAzLCB0KSB7XG4gIHZhciBvbmV0ID0gMSAtIHQ7XG4gIHJldHVybiAzICogKCgocDEgLSBwMCkgKiBvbmV0ICsgMiAqIChwMiAtIHAxKSAqIHQpICogb25ldCArIChwMyAtIHAyKSAqIHQgKiB0KTtcbn1cbi8qKlxuICog6K6h566X5LiJ5qyh6LSd5aGe5bCU5pa556iL5qC577yM5L2/55So55ub6YeR5YWs5byPXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS9jdXJ2ZVxuICogQHBhcmFtICB7bnVtYmVyfSBwMFxuICogQHBhcmFtICB7bnVtYmVyfSBwMVxuICogQHBhcmFtICB7bnVtYmVyfSBwMlxuICogQHBhcmFtICB7bnVtYmVyfSBwM1xuICogQHBhcmFtICB7bnVtYmVyfSB2YWxcbiAqIEBwYXJhbSAge0FycmF5LjxudW1iZXI+fSByb290c1xuICogQHJldHVybiB7bnVtYmVyfSDmnInmlYjmoLnmlbDnm65cbiAqL1xuXG5cbmZ1bmN0aW9uIGN1YmljUm9vdEF0KHAwLCBwMSwgcDIsIHAzLCB2YWwsIHJvb3RzKSB7XG4gIC8vIEV2YWx1YXRlIHJvb3RzIG9mIGN1YmljIGZ1bmN0aW9uc1xuICB2YXIgYSA9IHAzICsgMyAqIChwMSAtIHAyKSAtIHAwO1xuICB2YXIgYiA9IDMgKiAocDIgLSBwMSAqIDIgKyBwMCk7XG4gIHZhciBjID0gMyAqIChwMSAtIHAwKTtcbiAgdmFyIGQgPSBwMCAtIHZhbDtcbiAgdmFyIEEgPSBiICogYiAtIDMgKiBhICogYztcbiAgdmFyIEIgPSBiICogYyAtIDkgKiBhICogZDtcbiAgdmFyIEMgPSBjICogYyAtIDMgKiBiICogZDtcbiAgdmFyIG4gPSAwO1xuXG4gIGlmIChpc0Fyb3VuZFplcm8oQSkgJiYgaXNBcm91bmRaZXJvKEIpKSB7XG4gICAgaWYgKGlzQXJvdW5kWmVybyhiKSkge1xuICAgICAgcm9vdHNbMF0gPSAwO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgdDEgPSAtYyAvIGI7IC8vdDEsIHQyLCB0MywgYiBpcyBub3QgemVyb1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIHJvb3RzW24rK10gPSB0MTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIGRpc2MgPSBCICogQiAtIDQgKiBBICogQztcblxuICAgIGlmIChpc0Fyb3VuZFplcm8oZGlzYykpIHtcbiAgICAgIHZhciBLID0gQiAvIEE7XG4gICAgICB2YXIgdDEgPSAtYiAvIGEgKyBLOyAvLyB0MSwgYSBpcyBub3QgemVyb1xuXG4gICAgICB2YXIgdDIgPSAtSyAvIDI7IC8vIHQyLCB0M1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIHJvb3RzW24rK10gPSB0MTtcbiAgICAgIH1cblxuICAgICAgaWYgKHQyID49IDAgJiYgdDIgPD0gMSkge1xuICAgICAgICByb290c1tuKytdID0gdDI7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChkaXNjID4gMCkge1xuICAgICAgdmFyIGRpc2NTcXJ0ID0gbWF0aFNxcnQoZGlzYyk7XG4gICAgICB2YXIgWTEgPSBBICogYiArIDEuNSAqIGEgKiAoLUIgKyBkaXNjU3FydCk7XG4gICAgICB2YXIgWTIgPSBBICogYiArIDEuNSAqIGEgKiAoLUIgLSBkaXNjU3FydCk7XG5cbiAgICAgIGlmIChZMSA8IDApIHtcbiAgICAgICAgWTEgPSAtbWF0aFBvdygtWTEsIE9ORV9USElSRCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBZMSA9IG1hdGhQb3coWTEsIE9ORV9USElSRCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChZMiA8IDApIHtcbiAgICAgICAgWTIgPSAtbWF0aFBvdygtWTIsIE9ORV9USElSRCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBZMiA9IG1hdGhQb3coWTIsIE9ORV9USElSRCk7XG4gICAgICB9XG5cbiAgICAgIHZhciB0MSA9ICgtYiAtIChZMSArIFkyKSkgLyAoMyAqIGEpO1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIHJvb3RzW24rK10gPSB0MTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIFQgPSAoMiAqIEEgKiBiIC0gMyAqIGEgKiBCKSAvICgyICogbWF0aFNxcnQoQSAqIEEgKiBBKSk7XG4gICAgICB2YXIgdGhldGEgPSBNYXRoLmFjb3MoVCkgLyAzO1xuICAgICAgdmFyIEFTcXJ0ID0gbWF0aFNxcnQoQSk7XG4gICAgICB2YXIgdG1wID0gTWF0aC5jb3ModGhldGEpO1xuICAgICAgdmFyIHQxID0gKC1iIC0gMiAqIEFTcXJ0ICogdG1wKSAvICgzICogYSk7XG4gICAgICB2YXIgdDIgPSAoLWIgKyBBU3FydCAqICh0bXAgKyBUSFJFRV9TUVJUICogTWF0aC5zaW4odGhldGEpKSkgLyAoMyAqIGEpO1xuICAgICAgdmFyIHQzID0gKC1iICsgQVNxcnQgKiAodG1wIC0gVEhSRUVfU1FSVCAqIE1hdGguc2luKHRoZXRhKSkpIC8gKDMgKiBhKTtcblxuICAgICAgaWYgKHQxID49IDAgJiYgdDEgPD0gMSkge1xuICAgICAgICByb290c1tuKytdID0gdDE7XG4gICAgICB9XG5cbiAgICAgIGlmICh0MiA+PSAwICYmIHQyIDw9IDEpIHtcbiAgICAgICAgcm9vdHNbbisrXSA9IHQyO1xuICAgICAgfVxuXG4gICAgICBpZiAodDMgPj0gMCAmJiB0MyA8PSAxKSB7XG4gICAgICAgIHJvb3RzW24rK10gPSB0MztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gbjtcbn1cbi8qKlxuICog6K6h566X5LiJ5qyh6LSd5aGe5bCU5pa556iL5p6B6ZmQ5YC855qE5L2N572uXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS9jdXJ2ZVxuICogQHBhcmFtICB7bnVtYmVyfSBwMFxuICogQHBhcmFtICB7bnVtYmVyfSBwMVxuICogQHBhcmFtICB7bnVtYmVyfSBwMlxuICogQHBhcmFtICB7bnVtYmVyfSBwM1xuICogQHBhcmFtICB7QXJyYXkuPG51bWJlcj59IGV4dHJlbWFcbiAqIEByZXR1cm4ge251bWJlcn0g5pyJ5pWI5pWw55uuXG4gKi9cblxuXG5mdW5jdGlvbiBjdWJpY0V4dHJlbWEocDAsIHAxLCBwMiwgcDMsIGV4dHJlbWEpIHtcbiAgdmFyIGIgPSA2ICogcDIgLSAxMiAqIHAxICsgNiAqIHAwO1xuICB2YXIgYSA9IDkgKiBwMSArIDMgKiBwMyAtIDMgKiBwMCAtIDkgKiBwMjtcbiAgdmFyIGMgPSAzICogcDEgLSAzICogcDA7XG4gIHZhciBuID0gMDtcblxuICBpZiAoaXNBcm91bmRaZXJvKGEpKSB7XG4gICAgaWYgKGlzTm90QXJvdW5kWmVybyhiKSkge1xuICAgICAgdmFyIHQxID0gLWMgLyBiO1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIGV4dHJlbWFbbisrXSA9IHQxO1xuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIHtcbiAgICB2YXIgZGlzYyA9IGIgKiBiIC0gNCAqIGEgKiBjO1xuXG4gICAgaWYgKGlzQXJvdW5kWmVybyhkaXNjKSkge1xuICAgICAgZXh0cmVtYVswXSA9IC1iIC8gKDIgKiBhKTtcbiAgICB9IGVsc2UgaWYgKGRpc2MgPiAwKSB7XG4gICAgICB2YXIgZGlzY1NxcnQgPSBtYXRoU3FydChkaXNjKTtcbiAgICAgIHZhciB0MSA9ICgtYiArIGRpc2NTcXJ0KSAvICgyICogYSk7XG4gICAgICB2YXIgdDIgPSAoLWIgLSBkaXNjU3FydCkgLyAoMiAqIGEpO1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIGV4dHJlbWFbbisrXSA9IHQxO1xuICAgICAgfVxuXG4gICAgICBpZiAodDIgPj0gMCAmJiB0MiA8PSAxKSB7XG4gICAgICAgIGV4dHJlbWFbbisrXSA9IHQyO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBuO1xufVxuLyoqXG4gKiDnu4bliIbkuInmrKHotJ3loZ7lsJTmm7Lnur9cbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL2N1cnZlXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAwXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAzXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHRcbiAqIEBwYXJhbSAge0FycmF5LjxudW1iZXI+fSBvdXRcbiAqL1xuXG5cbmZ1bmN0aW9uIGN1YmljU3ViZGl2aWRlKHAwLCBwMSwgcDIsIHAzLCB0LCBvdXQpIHtcbiAgdmFyIHAwMSA9IChwMSAtIHAwKSAqIHQgKyBwMDtcbiAgdmFyIHAxMiA9IChwMiAtIHAxKSAqIHQgKyBwMTtcbiAgdmFyIHAyMyA9IChwMyAtIHAyKSAqIHQgKyBwMjtcbiAgdmFyIHAwMTIgPSAocDEyIC0gcDAxKSAqIHQgKyBwMDE7XG4gIHZhciBwMTIzID0gKHAyMyAtIHAxMikgKiB0ICsgcDEyO1xuICB2YXIgcDAxMjMgPSAocDEyMyAtIHAwMTIpICogdCArIHAwMTI7IC8vIFNlZzBcblxuICBvdXRbMF0gPSBwMDtcbiAgb3V0WzFdID0gcDAxO1xuICBvdXRbMl0gPSBwMDEyO1xuICBvdXRbM10gPSBwMDEyMzsgLy8gU2VnMVxuXG4gIG91dFs0XSA9IHAwMTIzO1xuICBvdXRbNV0gPSBwMTIzO1xuICBvdXRbNl0gPSBwMjM7XG4gIG91dFs3XSA9IHAzO1xufVxuLyoqXG4gKiDmipXlsITngrnliLDkuInmrKHotJ3loZ7lsJTmm7Lnur/kuIrvvIzov5Tlm57mipXlsITot53nprvjgIJcbiAqIOaKleWwhOeCueacieWPr+iDveS8muacieS4gOS4quaIluiAheWkmuS4qu+8jOi/memHjOWPqui/lOWbnuWFtuS4rei3neemu+acgOefreeahOS4gOS4quOAglxuICogQHBhcmFtIHtudW1iZXJ9IHgwXG4gKiBAcGFyYW0ge251bWJlcn0geTBcbiAqIEBwYXJhbSB7bnVtYmVyfSB4MVxuICogQHBhcmFtIHtudW1iZXJ9IHkxXG4gKiBAcGFyYW0ge251bWJlcn0geDJcbiAqIEBwYXJhbSB7bnVtYmVyfSB5MlxuICogQHBhcmFtIHtudW1iZXJ9IHgzXG4gKiBAcGFyYW0ge251bWJlcn0geTNcbiAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gKiBAcGFyYW0ge251bWJlcn0geVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW291dF0g5oqV5bCE54K5XG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBjdWJpY1Byb2plY3RQb2ludCh4MCwgeTAsIHgxLCB5MSwgeDIsIHkyLCB4MywgeTMsIHgsIHksIG91dCkge1xuICAvLyBodHRwOi8vcG9tYXguZ2l0aHViLmlvL2JlemllcmluZm8vI3Byb2plY3Rpb25zXG4gIHZhciB0O1xuICB2YXIgaW50ZXJ2YWwgPSAwLjAwNTtcbiAgdmFyIGQgPSBJbmZpbml0eTtcbiAgdmFyIHByZXY7XG4gIHZhciBuZXh0O1xuICB2YXIgZDE7XG4gIHZhciBkMjtcbiAgX3YwWzBdID0geDtcbiAgX3YwWzFdID0geTsgLy8g5YWI57KX55Wl5Lyw6K6h5LiA5LiL5Y+v6IO955qE5pyA5bCP6Led56a755qEIHQg5YC8XG4gIC8vIFBFTkRJTkdcblxuICBmb3IgKHZhciBfdCA9IDA7IF90IDwgMTsgX3QgKz0gMC4wNSkge1xuICAgIF92MVswXSA9IGN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIF90KTtcbiAgICBfdjFbMV0gPSBjdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCBfdCk7XG4gICAgZDEgPSB2MkRpc3RTcXVhcmUoX3YwLCBfdjEpO1xuXG4gICAgaWYgKGQxIDwgZCkge1xuICAgICAgdCA9IF90O1xuICAgICAgZCA9IGQxO1xuICAgIH1cbiAgfVxuXG4gIGQgPSBJbmZpbml0eTsgLy8gQXQgbW9zdCAzMiBpdGVyYXRpb25cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IDMyOyBpKyspIHtcbiAgICBpZiAoaW50ZXJ2YWwgPCBFUFNJTE9OX05VTUVSSUMpIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHByZXYgPSB0IC0gaW50ZXJ2YWw7XG4gICAgbmV4dCA9IHQgKyBpbnRlcnZhbDsgLy8gdCAtIGludGVydmFsXG5cbiAgICBfdjFbMF0gPSBjdWJpY0F0KHgwLCB4MSwgeDIsIHgzLCBwcmV2KTtcbiAgICBfdjFbMV0gPSBjdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCBwcmV2KTtcbiAgICBkMSA9IHYyRGlzdFNxdWFyZShfdjEsIF92MCk7XG5cbiAgICBpZiAocHJldiA+PSAwICYmIGQxIDwgZCkge1xuICAgICAgdCA9IHByZXY7XG4gICAgICBkID0gZDE7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHQgKyBpbnRlcnZhbFxuICAgICAgX3YyWzBdID0gY3ViaWNBdCh4MCwgeDEsIHgyLCB4MywgbmV4dCk7XG4gICAgICBfdjJbMV0gPSBjdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCBuZXh0KTtcbiAgICAgIGQyID0gdjJEaXN0U3F1YXJlKF92MiwgX3YwKTtcblxuICAgICAgaWYgKG5leHQgPD0gMSAmJiBkMiA8IGQpIHtcbiAgICAgICAgdCA9IG5leHQ7XG4gICAgICAgIGQgPSBkMjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGludGVydmFsICo9IDAuNTtcbiAgICAgIH1cbiAgICB9XG4gIH0gLy8gdFxuXG5cbiAgaWYgKG91dCkge1xuICAgIG91dFswXSA9IGN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIHQpO1xuICAgIG91dFsxXSA9IGN1YmljQXQoeTAsIHkxLCB5MiwgeTMsIHQpO1xuICB9IC8vIGNvbnNvbGUubG9nKGludGVydmFsLCBpKTtcblxuXG4gIHJldHVybiBtYXRoU3FydChkKTtcbn1cbi8qKlxuICog6K6h566X5LqM5qyh5pa56LSd5aGe5bCU5YC8XG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAwXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAyXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHRcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIHF1YWRyYXRpY0F0KHAwLCBwMSwgcDIsIHQpIHtcbiAgdmFyIG9uZXQgPSAxIC0gdDtcbiAgcmV0dXJuIG9uZXQgKiAob25ldCAqIHAwICsgMiAqIHQgKiBwMSkgKyB0ICogdCAqIHAyO1xufVxuLyoqXG4gKiDorqHnrpfkuozmrKHmlrnotJ3loZ7lsJTlr7zmlbDlgLxcbiAqIEBwYXJhbSAge251bWJlcn0gcDBcbiAqIEBwYXJhbSAge251bWJlcn0gcDFcbiAqIEBwYXJhbSAge251bWJlcn0gcDJcbiAqIEBwYXJhbSAge251bWJlcn0gdFxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxuZnVuY3Rpb24gcXVhZHJhdGljRGVyaXZhdGl2ZUF0KHAwLCBwMSwgcDIsIHQpIHtcbiAgcmV0dXJuIDIgKiAoKDEgLSB0KSAqIChwMSAtIHAwKSArIHQgKiAocDIgLSBwMSkpO1xufVxuLyoqXG4gKiDorqHnrpfkuozmrKHmlrnotJ3loZ7lsJTmlrnnqIvmoLlcbiAqIEBwYXJhbSAge251bWJlcn0gcDBcbiAqIEBwYXJhbSAge251bWJlcn0gcDFcbiAqIEBwYXJhbSAge251bWJlcn0gcDJcbiAqIEBwYXJhbSAge251bWJlcn0gdFxuICogQHBhcmFtICB7QXJyYXkuPG51bWJlcj59IHJvb3RzXG4gKiBAcmV0dXJuIHtudW1iZXJ9IOacieaViOagueaVsOebrlxuICovXG5cblxuZnVuY3Rpb24gcXVhZHJhdGljUm9vdEF0KHAwLCBwMSwgcDIsIHZhbCwgcm9vdHMpIHtcbiAgdmFyIGEgPSBwMCAtIDIgKiBwMSArIHAyO1xuICB2YXIgYiA9IDIgKiAocDEgLSBwMCk7XG4gIHZhciBjID0gcDAgLSB2YWw7XG4gIHZhciBuID0gMDtcblxuICBpZiAoaXNBcm91bmRaZXJvKGEpKSB7XG4gICAgaWYgKGlzTm90QXJvdW5kWmVybyhiKSkge1xuICAgICAgdmFyIHQxID0gLWMgLyBiO1xuXG4gICAgICBpZiAodDEgPj0gMCAmJiB0MSA8PSAxKSB7XG4gICAgICAgIHJvb3RzW24rK10gPSB0MTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIGRpc2MgPSBiICogYiAtIDQgKiBhICogYztcblxuICAgIGlmIChpc0Fyb3VuZFplcm8oZGlzYykpIHtcbiAgICAgIHZhciB0MSA9IC1iIC8gKDIgKiBhKTtcblxuICAgICAgaWYgKHQxID49IDAgJiYgdDEgPD0gMSkge1xuICAgICAgICByb290c1tuKytdID0gdDE7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChkaXNjID4gMCkge1xuICAgICAgdmFyIGRpc2NTcXJ0ID0gbWF0aFNxcnQoZGlzYyk7XG4gICAgICB2YXIgdDEgPSAoLWIgKyBkaXNjU3FydCkgLyAoMiAqIGEpO1xuICAgICAgdmFyIHQyID0gKC1iIC0gZGlzY1NxcnQpIC8gKDIgKiBhKTtcblxuICAgICAgaWYgKHQxID49IDAgJiYgdDEgPD0gMSkge1xuICAgICAgICByb290c1tuKytdID0gdDE7XG4gICAgICB9XG5cbiAgICAgIGlmICh0MiA+PSAwICYmIHQyIDw9IDEpIHtcbiAgICAgICAgcm9vdHNbbisrXSA9IHQyO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBuO1xufVxuLyoqXG4gKiDorqHnrpfkuozmrKHotJ3loZ7lsJTmlrnnqIvmnoHpmZDlgLxcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL2N1cnZlXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAwXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAyXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBxdWFkcmF0aWNFeHRyZW11bShwMCwgcDEsIHAyKSB7XG4gIHZhciBkaXZpZGVyID0gcDAgKyBwMiAtIDIgKiBwMTtcblxuICBpZiAoZGl2aWRlciA9PT0gMCkge1xuICAgIC8vIHAxIGlzIGNlbnRlciBvZiBwMCBhbmQgcDJcbiAgICByZXR1cm4gMC41O1xuICB9IGVsc2Uge1xuICAgIHJldHVybiAocDAgLSBwMSkgLyBkaXZpZGVyO1xuICB9XG59XG4vKipcbiAqIOe7huWIhuS6jOasoei0neWhnuWwlOabsue6v1xuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvY3VydmVcbiAqIEBwYXJhbSAge251bWJlcn0gcDBcbiAqIEBwYXJhbSAge251bWJlcn0gcDFcbiAqIEBwYXJhbSAge251bWJlcn0gcDJcbiAqIEBwYXJhbSAge251bWJlcn0gdFxuICogQHBhcmFtICB7QXJyYXkuPG51bWJlcj59IG91dFxuICovXG5cblxuZnVuY3Rpb24gcXVhZHJhdGljU3ViZGl2aWRlKHAwLCBwMSwgcDIsIHQsIG91dCkge1xuICB2YXIgcDAxID0gKHAxIC0gcDApICogdCArIHAwO1xuICB2YXIgcDEyID0gKHAyIC0gcDEpICogdCArIHAxO1xuICB2YXIgcDAxMiA9IChwMTIgLSBwMDEpICogdCArIHAwMTsgLy8gU2VnMFxuXG4gIG91dFswXSA9IHAwO1xuICBvdXRbMV0gPSBwMDE7XG4gIG91dFsyXSA9IHAwMTI7IC8vIFNlZzFcblxuICBvdXRbM10gPSBwMDEyO1xuICBvdXRbNF0gPSBwMTI7XG4gIG91dFs1XSA9IHAyO1xufVxuLyoqXG4gKiDmipXlsITngrnliLDkuozmrKHotJ3loZ7lsJTmm7Lnur/kuIrvvIzov5Tlm57mipXlsITot53nprvjgIJcbiAqIOaKleWwhOeCueacieWPr+iDveS8muacieS4gOS4quaIluiAheWkmuS4qu+8jOi/memHjOWPqui/lOWbnuWFtuS4rei3neemu+acgOefreeahOS4gOS4quOAglxuICogQHBhcmFtIHtudW1iZXJ9IHgwXG4gKiBAcGFyYW0ge251bWJlcn0geTBcbiAqIEBwYXJhbSB7bnVtYmVyfSB4MVxuICogQHBhcmFtIHtudW1iZXJ9IHkxXG4gKiBAcGFyYW0ge251bWJlcn0geDJcbiAqIEBwYXJhbSB7bnVtYmVyfSB5MlxuICogQHBhcmFtIHtudW1iZXJ9IHhcbiAqIEBwYXJhbSB7bnVtYmVyfSB5XG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBvdXQg5oqV5bCE54K5XG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBxdWFkcmF0aWNQcm9qZWN0UG9pbnQoeDAsIHkwLCB4MSwgeTEsIHgyLCB5MiwgeCwgeSwgb3V0KSB7XG4gIC8vIGh0dHA6Ly9wb21heC5naXRodWIuaW8vYmV6aWVyaW5mby8jcHJvamVjdGlvbnNcbiAgdmFyIHQ7XG4gIHZhciBpbnRlcnZhbCA9IDAuMDA1O1xuICB2YXIgZCA9IEluZmluaXR5O1xuICBfdjBbMF0gPSB4O1xuICBfdjBbMV0gPSB5OyAvLyDlhYjnspfnlaXkvLDorqHkuIDkuIvlj6/og73nmoTmnIDlsI/ot53nprvnmoQgdCDlgLxcbiAgLy8gUEVORElOR1xuXG4gIGZvciAodmFyIF90ID0gMDsgX3QgPCAxOyBfdCArPSAwLjA1KSB7XG4gICAgX3YxWzBdID0gcXVhZHJhdGljQXQoeDAsIHgxLCB4MiwgX3QpO1xuICAgIF92MVsxXSA9IHF1YWRyYXRpY0F0KHkwLCB5MSwgeTIsIF90KTtcbiAgICB2YXIgZDEgPSB2MkRpc3RTcXVhcmUoX3YwLCBfdjEpO1xuXG4gICAgaWYgKGQxIDwgZCkge1xuICAgICAgdCA9IF90O1xuICAgICAgZCA9IGQxO1xuICAgIH1cbiAgfVxuXG4gIGQgPSBJbmZpbml0eTsgLy8gQXQgbW9zdCAzMiBpdGVyYXRpb25cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IDMyOyBpKyspIHtcbiAgICBpZiAoaW50ZXJ2YWwgPCBFUFNJTE9OX05VTUVSSUMpIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBwcmV2ID0gdCAtIGludGVydmFsO1xuICAgIHZhciBuZXh0ID0gdCArIGludGVydmFsOyAvLyB0IC0gaW50ZXJ2YWxcblxuICAgIF92MVswXSA9IHF1YWRyYXRpY0F0KHgwLCB4MSwgeDIsIHByZXYpO1xuICAgIF92MVsxXSA9IHF1YWRyYXRpY0F0KHkwLCB5MSwgeTIsIHByZXYpO1xuICAgIHZhciBkMSA9IHYyRGlzdFNxdWFyZShfdjEsIF92MCk7XG5cbiAgICBpZiAocHJldiA+PSAwICYmIGQxIDwgZCkge1xuICAgICAgdCA9IHByZXY7XG4gICAgICBkID0gZDE7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHQgKyBpbnRlcnZhbFxuICAgICAgX3YyWzBdID0gcXVhZHJhdGljQXQoeDAsIHgxLCB4MiwgbmV4dCk7XG4gICAgICBfdjJbMV0gPSBxdWFkcmF0aWNBdCh5MCwgeTEsIHkyLCBuZXh0KTtcbiAgICAgIHZhciBkMiA9IHYyRGlzdFNxdWFyZShfdjIsIF92MCk7XG5cbiAgICAgIGlmIChuZXh0IDw9IDEgJiYgZDIgPCBkKSB7XG4gICAgICAgIHQgPSBuZXh0O1xuICAgICAgICBkID0gZDI7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbnRlcnZhbCAqPSAwLjU7XG4gICAgICB9XG4gICAgfVxuICB9IC8vIHRcblxuXG4gIGlmIChvdXQpIHtcbiAgICBvdXRbMF0gPSBxdWFkcmF0aWNBdCh4MCwgeDEsIHgyLCB0KTtcbiAgICBvdXRbMV0gPSBxdWFkcmF0aWNBdCh5MCwgeTEsIHkyLCB0KTtcbiAgfSAvLyBjb25zb2xlLmxvZyhpbnRlcnZhbCwgaSk7XG5cblxuICByZXR1cm4gbWF0aFNxcnQoZCk7XG59XG5cbmV4cG9ydHMuY3ViaWNBdCA9IGN1YmljQXQ7XG5leHBvcnRzLmN1YmljRGVyaXZhdGl2ZUF0ID0gY3ViaWNEZXJpdmF0aXZlQXQ7XG5leHBvcnRzLmN1YmljUm9vdEF0ID0gY3ViaWNSb290QXQ7XG5leHBvcnRzLmN1YmljRXh0cmVtYSA9IGN1YmljRXh0cmVtYTtcbmV4cG9ydHMuY3ViaWNTdWJkaXZpZGUgPSBjdWJpY1N1YmRpdmlkZTtcbmV4cG9ydHMuY3ViaWNQcm9qZWN0UG9pbnQgPSBjdWJpY1Byb2plY3RQb2ludDtcbmV4cG9ydHMucXVhZHJhdGljQXQgPSBxdWFkcmF0aWNBdDtcbmV4cG9ydHMucXVhZHJhdGljRGVyaXZhdGl2ZUF0ID0gcXVhZHJhdGljRGVyaXZhdGl2ZUF0O1xuZXhwb3J0cy5xdWFkcmF0aWNSb290QXQgPSBxdWFkcmF0aWNSb290QXQ7XG5leHBvcnRzLnF1YWRyYXRpY0V4dHJlbXVtID0gcXVhZHJhdGljRXh0cmVtdW07XG5leHBvcnRzLnF1YWRyYXRpY1N1YmRpdmlkZSA9IHF1YWRyYXRpY1N1YmRpdmlkZTtcbmV4cG9ydHMucXVhZHJhdGljUHJvamVjdFBvaW50ID0gcXVhZHJhdGljUHJvamVjdFBvaW50OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/curve.js
