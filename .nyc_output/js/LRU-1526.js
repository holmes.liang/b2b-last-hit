// Simple LRU cache use doubly linked list
// @module zrender/core/LRU

/**
 * Simple double linked list. Compared with array, it has O(1) remove operation.
 * @constructor
 */
var LinkedList = function LinkedList() {
  /**
   * @type {module:zrender/core/LRU~Entry}
   */
  this.head = null;
  /**
   * @type {module:zrender/core/LRU~Entry}
   */

  this.tail = null;
  this._len = 0;
};

var linkedListProto = LinkedList.prototype;
/**
 * Insert a new value at the tail
 * @param  {} val
 * @return {module:zrender/core/LRU~Entry}
 */

linkedListProto.insert = function (val) {
  var entry = new Entry(val);
  this.insertEntry(entry);
  return entry;
};
/**
 * Insert an entry at the tail
 * @param  {module:zrender/core/LRU~Entry} entry
 */


linkedListProto.insertEntry = function (entry) {
  if (!this.head) {
    this.head = this.tail = entry;
  } else {
    this.tail.next = entry;
    entry.prev = this.tail;
    entry.next = null;
    this.tail = entry;
  }

  this._len++;
};
/**
 * Remove entry.
 * @param  {module:zrender/core/LRU~Entry} entry
 */


linkedListProto.remove = function (entry) {
  var prev = entry.prev;
  var next = entry.next;

  if (prev) {
    prev.next = next;
  } else {
    // Is head
    this.head = next;
  }

  if (next) {
    next.prev = prev;
  } else {
    // Is tail
    this.tail = prev;
  }

  entry.next = entry.prev = null;
  this._len--;
};
/**
 * @return {number}
 */


linkedListProto.len = function () {
  return this._len;
};
/**
 * Clear list
 */


linkedListProto.clear = function () {
  this.head = this.tail = null;
  this._len = 0;
};
/**
 * @constructor
 * @param {} val
 */


var Entry = function Entry(val) {
  /**
   * @type {}
   */
  this.value = val;
  /**
   * @type {module:zrender/core/LRU~Entry}
   */

  this.next;
  /**
   * @type {module:zrender/core/LRU~Entry}
   */

  this.prev;
};
/**
 * LRU Cache
 * @constructor
 * @alias module:zrender/core/LRU
 */


var LRU = function LRU(maxSize) {
  this._list = new LinkedList();
  this._map = {};
  this._maxSize = maxSize || 10;
  this._lastRemovedEntry = null;
};

var LRUProto = LRU.prototype;
/**
 * @param  {string} key
 * @param  {} value
 * @return {} Removed value
 */

LRUProto.put = function (key, value) {
  var list = this._list;
  var map = this._map;
  var removed = null;

  if (map[key] == null) {
    var len = list.len(); // Reuse last removed entry

    var entry = this._lastRemovedEntry;

    if (len >= this._maxSize && len > 0) {
      // Remove the least recently used
      var leastUsedEntry = list.head;
      list.remove(leastUsedEntry);
      delete map[leastUsedEntry.key];
      removed = leastUsedEntry.value;
      this._lastRemovedEntry = leastUsedEntry;
    }

    if (entry) {
      entry.value = value;
    } else {
      entry = new Entry(value);
    }

    entry.key = key;
    list.insertEntry(entry);
    map[key] = entry;
  }

  return removed;
};
/**
 * @param  {string} key
 * @return {}
 */


LRUProto.get = function (key) {
  var entry = this._map[key];
  var list = this._list;

  if (entry != null) {
    // Put the latest used entry in the tail
    if (entry !== list.tail) {
      list.remove(entry);
      list.insertEntry(entry);
    }

    return entry.value;
  }
};
/**
 * Clear the cache
 */


LRUProto.clear = function () {
  this._list.clear();

  this._map = {};
};

var _default = LRU;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9MUlUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL0xSVS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBTaW1wbGUgTFJVIGNhY2hlIHVzZSBkb3VibHkgbGlua2VkIGxpc3Rcbi8vIEBtb2R1bGUgenJlbmRlci9jb3JlL0xSVVxuXG4vKipcbiAqIFNpbXBsZSBkb3VibGUgbGlua2VkIGxpc3QuIENvbXBhcmVkIHdpdGggYXJyYXksIGl0IGhhcyBPKDEpIHJlbW92ZSBvcGVyYXRpb24uXG4gKiBAY29uc3RydWN0b3JcbiAqL1xudmFyIExpbmtlZExpc3QgPSBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvY29yZS9MUlV+RW50cnl9XG4gICAqL1xuICB0aGlzLmhlYWQgPSBudWxsO1xuICAvKipcbiAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL2NvcmUvTFJVfkVudHJ5fVxuICAgKi9cblxuICB0aGlzLnRhaWwgPSBudWxsO1xuICB0aGlzLl9sZW4gPSAwO1xufTtcblxudmFyIGxpbmtlZExpc3RQcm90byA9IExpbmtlZExpc3QucHJvdG90eXBlO1xuLyoqXG4gKiBJbnNlcnQgYSBuZXcgdmFsdWUgYXQgdGhlIHRhaWxcbiAqIEBwYXJhbSAge30gdmFsXG4gKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL0xSVX5FbnRyeX1cbiAqL1xuXG5saW5rZWRMaXN0UHJvdG8uaW5zZXJ0ID0gZnVuY3Rpb24gKHZhbCkge1xuICB2YXIgZW50cnkgPSBuZXcgRW50cnkodmFsKTtcbiAgdGhpcy5pbnNlcnRFbnRyeShlbnRyeSk7XG4gIHJldHVybiBlbnRyeTtcbn07XG4vKipcbiAqIEluc2VydCBhbiBlbnRyeSBhdCB0aGUgdGFpbFxuICogQHBhcmFtICB7bW9kdWxlOnpyZW5kZXIvY29yZS9MUlV+RW50cnl9IGVudHJ5XG4gKi9cblxuXG5saW5rZWRMaXN0UHJvdG8uaW5zZXJ0RW50cnkgPSBmdW5jdGlvbiAoZW50cnkpIHtcbiAgaWYgKCF0aGlzLmhlYWQpIHtcbiAgICB0aGlzLmhlYWQgPSB0aGlzLnRhaWwgPSBlbnRyeTtcbiAgfSBlbHNlIHtcbiAgICB0aGlzLnRhaWwubmV4dCA9IGVudHJ5O1xuICAgIGVudHJ5LnByZXYgPSB0aGlzLnRhaWw7XG4gICAgZW50cnkubmV4dCA9IG51bGw7XG4gICAgdGhpcy50YWlsID0gZW50cnk7XG4gIH1cblxuICB0aGlzLl9sZW4rKztcbn07XG4vKipcbiAqIFJlbW92ZSBlbnRyeS5cbiAqIEBwYXJhbSAge21vZHVsZTp6cmVuZGVyL2NvcmUvTFJVfkVudHJ5fSBlbnRyeVxuICovXG5cblxubGlua2VkTGlzdFByb3RvLnJlbW92ZSA9IGZ1bmN0aW9uIChlbnRyeSkge1xuICB2YXIgcHJldiA9IGVudHJ5LnByZXY7XG4gIHZhciBuZXh0ID0gZW50cnkubmV4dDtcblxuICBpZiAocHJldikge1xuICAgIHByZXYubmV4dCA9IG5leHQ7XG4gIH0gZWxzZSB7XG4gICAgLy8gSXMgaGVhZFxuICAgIHRoaXMuaGVhZCA9IG5leHQ7XG4gIH1cblxuICBpZiAobmV4dCkge1xuICAgIG5leHQucHJldiA9IHByZXY7XG4gIH0gZWxzZSB7XG4gICAgLy8gSXMgdGFpbFxuICAgIHRoaXMudGFpbCA9IHByZXY7XG4gIH1cblxuICBlbnRyeS5uZXh0ID0gZW50cnkucHJldiA9IG51bGw7XG4gIHRoaXMuX2xlbi0tO1xufTtcbi8qKlxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxubGlua2VkTGlzdFByb3RvLmxlbiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuX2xlbjtcbn07XG4vKipcbiAqIENsZWFyIGxpc3RcbiAqL1xuXG5cbmxpbmtlZExpc3RQcm90by5jbGVhciA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5oZWFkID0gdGhpcy50YWlsID0gbnVsbDtcbiAgdGhpcy5fbGVuID0gMDtcbn07XG4vKipcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHt9IHZhbFxuICovXG5cblxudmFyIEVudHJ5ID0gZnVuY3Rpb24gKHZhbCkge1xuICAvKipcbiAgICogQHR5cGUge31cbiAgICovXG4gIHRoaXMudmFsdWUgPSB2YWw7XG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvY29yZS9MUlV+RW50cnl9XG4gICAqL1xuXG4gIHRoaXMubmV4dDtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9jb3JlL0xSVX5FbnRyeX1cbiAgICovXG5cbiAgdGhpcy5wcmV2O1xufTtcbi8qKlxuICogTFJVIENhY2hlXG4gKiBAY29uc3RydWN0b3JcbiAqIEBhbGlhcyBtb2R1bGU6enJlbmRlci9jb3JlL0xSVVxuICovXG5cblxudmFyIExSVSA9IGZ1bmN0aW9uIChtYXhTaXplKSB7XG4gIHRoaXMuX2xpc3QgPSBuZXcgTGlua2VkTGlzdCgpO1xuICB0aGlzLl9tYXAgPSB7fTtcbiAgdGhpcy5fbWF4U2l6ZSA9IG1heFNpemUgfHwgMTA7XG4gIHRoaXMuX2xhc3RSZW1vdmVkRW50cnkgPSBudWxsO1xufTtcblxudmFyIExSVVByb3RvID0gTFJVLnByb3RvdHlwZTtcbi8qKlxuICogQHBhcmFtICB7c3RyaW5nfSBrZXlcbiAqIEBwYXJhbSAge30gdmFsdWVcbiAqIEByZXR1cm4ge30gUmVtb3ZlZCB2YWx1ZVxuICovXG5cbkxSVVByb3RvLnB1dCA9IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gIHZhciBsaXN0ID0gdGhpcy5fbGlzdDtcbiAgdmFyIG1hcCA9IHRoaXMuX21hcDtcbiAgdmFyIHJlbW92ZWQgPSBudWxsO1xuXG4gIGlmIChtYXBba2V5XSA9PSBudWxsKSB7XG4gICAgdmFyIGxlbiA9IGxpc3QubGVuKCk7IC8vIFJldXNlIGxhc3QgcmVtb3ZlZCBlbnRyeVxuXG4gICAgdmFyIGVudHJ5ID0gdGhpcy5fbGFzdFJlbW92ZWRFbnRyeTtcblxuICAgIGlmIChsZW4gPj0gdGhpcy5fbWF4U2l6ZSAmJiBsZW4gPiAwKSB7XG4gICAgICAvLyBSZW1vdmUgdGhlIGxlYXN0IHJlY2VudGx5IHVzZWRcbiAgICAgIHZhciBsZWFzdFVzZWRFbnRyeSA9IGxpc3QuaGVhZDtcbiAgICAgIGxpc3QucmVtb3ZlKGxlYXN0VXNlZEVudHJ5KTtcbiAgICAgIGRlbGV0ZSBtYXBbbGVhc3RVc2VkRW50cnkua2V5XTtcbiAgICAgIHJlbW92ZWQgPSBsZWFzdFVzZWRFbnRyeS52YWx1ZTtcbiAgICAgIHRoaXMuX2xhc3RSZW1vdmVkRW50cnkgPSBsZWFzdFVzZWRFbnRyeTtcbiAgICB9XG5cbiAgICBpZiAoZW50cnkpIHtcbiAgICAgIGVudHJ5LnZhbHVlID0gdmFsdWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIGVudHJ5ID0gbmV3IEVudHJ5KHZhbHVlKTtcbiAgICB9XG5cbiAgICBlbnRyeS5rZXkgPSBrZXk7XG4gICAgbGlzdC5pbnNlcnRFbnRyeShlbnRyeSk7XG4gICAgbWFwW2tleV0gPSBlbnRyeTtcbiAgfVxuXG4gIHJldHVybiByZW1vdmVkO1xufTtcbi8qKlxuICogQHBhcmFtICB7c3RyaW5nfSBrZXlcbiAqIEByZXR1cm4ge31cbiAqL1xuXG5cbkxSVVByb3RvLmdldCA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgdmFyIGVudHJ5ID0gdGhpcy5fbWFwW2tleV07XG4gIHZhciBsaXN0ID0gdGhpcy5fbGlzdDtcblxuICBpZiAoZW50cnkgIT0gbnVsbCkge1xuICAgIC8vIFB1dCB0aGUgbGF0ZXN0IHVzZWQgZW50cnkgaW4gdGhlIHRhaWxcbiAgICBpZiAoZW50cnkgIT09IGxpc3QudGFpbCkge1xuICAgICAgbGlzdC5yZW1vdmUoZW50cnkpO1xuICAgICAgbGlzdC5pbnNlcnRFbnRyeShlbnRyeSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGVudHJ5LnZhbHVlO1xuICB9XG59O1xuLyoqXG4gKiBDbGVhciB0aGUgY2FjaGVcbiAqL1xuXG5cbkxSVVByb3RvLmNsZWFyID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLl9saXN0LmNsZWFyKCk7XG5cbiAgdGhpcy5fbWFwID0ge307XG59O1xuXG52YXIgX2RlZmF1bHQgPSBMUlU7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7QUFHQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBOzs7QUFHQTtBQUNBOzs7O0FBSUE7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/LRU.js
