var Displayable = __webpack_require__(/*! ./Displayable */ "./node_modules/zrender/lib/graphic/Displayable.js");

var BoundingRect = __webpack_require__(/*! ../core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var imageHelper = __webpack_require__(/*! ./helper/image */ "./node_modules/zrender/lib/graphic/helper/image.js");
/**
 * @alias zrender/graphic/Image
 * @extends module:zrender/graphic/Displayable
 * @constructor
 * @param {Object} opts
 */


function ZImage(opts) {
  Displayable.call(this, opts);
}

ZImage.prototype = {
  constructor: ZImage,
  type: 'image',
  brush: function brush(ctx, prevEl) {
    var style = this.style;
    var src = style.image; // Must bind each time

    style.bind(ctx, this, prevEl);
    var image = this._image = imageHelper.createOrUpdateImage(src, this._image, this, this.onload);

    if (!image || !imageHelper.isImageReady(image)) {
      return;
    } // 图片已经加载完成
    // if (image.nodeName.toUpperCase() == 'IMG') {
    //     if (!image.complete) {
    //         return;
    //     }
    // }
    // Else is canvas


    var x = style.x || 0;
    var y = style.y || 0;
    var width = style.width;
    var height = style.height;
    var aspect = image.width / image.height;

    if (width == null && height != null) {
      // Keep image/height ratio
      width = height * aspect;
    } else if (height == null && width != null) {
      height = width / aspect;
    } else if (width == null && height == null) {
      width = image.width;
      height = image.height;
    } // 设置transform


    this.setTransform(ctx);

    if (style.sWidth && style.sHeight) {
      var sx = style.sx || 0;
      var sy = style.sy || 0;
      ctx.drawImage(image, sx, sy, style.sWidth, style.sHeight, x, y, width, height);
    } else if (style.sx && style.sy) {
      var sx = style.sx;
      var sy = style.sy;
      var sWidth = width - sx;
      var sHeight = height - sy;
      ctx.drawImage(image, sx, sy, sWidth, sHeight, x, y, width, height);
    } else {
      ctx.drawImage(image, x, y, width, height);
    } // Draw rect text


    if (style.text != null) {
      // Only restore transform when needs draw text.
      this.restoreTransform(ctx);
      this.drawRectText(ctx, this.getBoundingRect());
    }
  },
  getBoundingRect: function getBoundingRect() {
    var style = this.style;

    if (!this._rect) {
      this._rect = new BoundingRect(style.x || 0, style.y || 0, style.width || 0, style.height || 0);
    }

    return this._rect;
  }
};
zrUtil.inherits(ZImage, Displayable);
var _default = ZImage;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9JbWFnZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvSW1hZ2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIERpc3BsYXlhYmxlID0gcmVxdWlyZShcIi4vRGlzcGxheWFibGVcIik7XG5cbnZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwiLi4vY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwiLi4vY29yZS91dGlsXCIpO1xuXG52YXIgaW1hZ2VIZWxwZXIgPSByZXF1aXJlKFwiLi9oZWxwZXIvaW1hZ2VcIik7XG5cbi8qKlxuICogQGFsaWFzIHpyZW5kZXIvZ3JhcGhpYy9JbWFnZVxuICogQGV4dGVuZHMgbW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZVxuICogQGNvbnN0cnVjdG9yXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0c1xuICovXG5mdW5jdGlvbiBaSW1hZ2Uob3B0cykge1xuICBEaXNwbGF5YWJsZS5jYWxsKHRoaXMsIG9wdHMpO1xufVxuXG5aSW1hZ2UucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogWkltYWdlLFxuICB0eXBlOiAnaW1hZ2UnLFxuICBicnVzaDogZnVuY3Rpb24gKGN0eCwgcHJldkVsKSB7XG4gICAgdmFyIHN0eWxlID0gdGhpcy5zdHlsZTtcbiAgICB2YXIgc3JjID0gc3R5bGUuaW1hZ2U7IC8vIE11c3QgYmluZCBlYWNoIHRpbWVcblxuICAgIHN0eWxlLmJpbmQoY3R4LCB0aGlzLCBwcmV2RWwpO1xuICAgIHZhciBpbWFnZSA9IHRoaXMuX2ltYWdlID0gaW1hZ2VIZWxwZXIuY3JlYXRlT3JVcGRhdGVJbWFnZShzcmMsIHRoaXMuX2ltYWdlLCB0aGlzLCB0aGlzLm9ubG9hZCk7XG5cbiAgICBpZiAoIWltYWdlIHx8ICFpbWFnZUhlbHBlci5pc0ltYWdlUmVhZHkoaW1hZ2UpKSB7XG4gICAgICByZXR1cm47XG4gICAgfSAvLyDlm77niYflt7Lnu4/liqDovb3lrozmiJBcbiAgICAvLyBpZiAoaW1hZ2Uubm9kZU5hbWUudG9VcHBlckNhc2UoKSA9PSAnSU1HJykge1xuICAgIC8vICAgICBpZiAoIWltYWdlLmNvbXBsZXRlKSB7XG4gICAgLy8gICAgICAgICByZXR1cm47XG4gICAgLy8gICAgIH1cbiAgICAvLyB9XG4gICAgLy8gRWxzZSBpcyBjYW52YXNcblxuXG4gICAgdmFyIHggPSBzdHlsZS54IHx8IDA7XG4gICAgdmFyIHkgPSBzdHlsZS55IHx8IDA7XG4gICAgdmFyIHdpZHRoID0gc3R5bGUud2lkdGg7XG4gICAgdmFyIGhlaWdodCA9IHN0eWxlLmhlaWdodDtcbiAgICB2YXIgYXNwZWN0ID0gaW1hZ2Uud2lkdGggLyBpbWFnZS5oZWlnaHQ7XG5cbiAgICBpZiAod2lkdGggPT0gbnVsbCAmJiBoZWlnaHQgIT0gbnVsbCkge1xuICAgICAgLy8gS2VlcCBpbWFnZS9oZWlnaHQgcmF0aW9cbiAgICAgIHdpZHRoID0gaGVpZ2h0ICogYXNwZWN0O1xuICAgIH0gZWxzZSBpZiAoaGVpZ2h0ID09IG51bGwgJiYgd2lkdGggIT0gbnVsbCkge1xuICAgICAgaGVpZ2h0ID0gd2lkdGggLyBhc3BlY3Q7XG4gICAgfSBlbHNlIGlmICh3aWR0aCA9PSBudWxsICYmIGhlaWdodCA9PSBudWxsKSB7XG4gICAgICB3aWR0aCA9IGltYWdlLndpZHRoO1xuICAgICAgaGVpZ2h0ID0gaW1hZ2UuaGVpZ2h0O1xuICAgIH0gLy8g6K6+572udHJhbnNmb3JtXG5cblxuICAgIHRoaXMuc2V0VHJhbnNmb3JtKGN0eCk7XG5cbiAgICBpZiAoc3R5bGUuc1dpZHRoICYmIHN0eWxlLnNIZWlnaHQpIHtcbiAgICAgIHZhciBzeCA9IHN0eWxlLnN4IHx8IDA7XG4gICAgICB2YXIgc3kgPSBzdHlsZS5zeSB8fCAwO1xuICAgICAgY3R4LmRyYXdJbWFnZShpbWFnZSwgc3gsIHN5LCBzdHlsZS5zV2lkdGgsIHN0eWxlLnNIZWlnaHQsIHgsIHksIHdpZHRoLCBoZWlnaHQpO1xuICAgIH0gZWxzZSBpZiAoc3R5bGUuc3ggJiYgc3R5bGUuc3kpIHtcbiAgICAgIHZhciBzeCA9IHN0eWxlLnN4O1xuICAgICAgdmFyIHN5ID0gc3R5bGUuc3k7XG4gICAgICB2YXIgc1dpZHRoID0gd2lkdGggLSBzeDtcbiAgICAgIHZhciBzSGVpZ2h0ID0gaGVpZ2h0IC0gc3k7XG4gICAgICBjdHguZHJhd0ltYWdlKGltYWdlLCBzeCwgc3ksIHNXaWR0aCwgc0hlaWdodCwgeCwgeSwgd2lkdGgsIGhlaWdodCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGN0eC5kcmF3SW1hZ2UoaW1hZ2UsIHgsIHksIHdpZHRoLCBoZWlnaHQpO1xuICAgIH0gLy8gRHJhdyByZWN0IHRleHRcblxuXG4gICAgaWYgKHN0eWxlLnRleHQgIT0gbnVsbCkge1xuICAgICAgLy8gT25seSByZXN0b3JlIHRyYW5zZm9ybSB3aGVuIG5lZWRzIGRyYXcgdGV4dC5cbiAgICAgIHRoaXMucmVzdG9yZVRyYW5zZm9ybShjdHgpO1xuICAgICAgdGhpcy5kcmF3UmVjdFRleHQoY3R4LCB0aGlzLmdldEJvdW5kaW5nUmVjdCgpKTtcbiAgICB9XG4gIH0sXG4gIGdldEJvdW5kaW5nUmVjdDogZnVuY3Rpb24gKCkge1xuICAgIHZhciBzdHlsZSA9IHRoaXMuc3R5bGU7XG5cbiAgICBpZiAoIXRoaXMuX3JlY3QpIHtcbiAgICAgIHRoaXMuX3JlY3QgPSBuZXcgQm91bmRpbmdSZWN0KHN0eWxlLnggfHwgMCwgc3R5bGUueSB8fCAwLCBzdHlsZS53aWR0aCB8fCAwLCBzdHlsZS5oZWlnaHQgfHwgMCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuX3JlY3Q7XG4gIH1cbn07XG56clV0aWwuaW5oZXJpdHMoWkltYWdlLCBEaXNwbGF5YWJsZSk7XG52YXIgX2RlZmF1bHQgPSBaSW1hZ2U7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckVBO0FBdUVBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Image.js
