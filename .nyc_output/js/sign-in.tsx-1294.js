__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component/page */ "./src/component/page/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _sign_in_style__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./sign-in-style */ "./src/app/desk/login/sign-in-style.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _styles_login_style__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @styles/login-style */ "./src/styles/login-style.tsx");
/* harmony import */ var _common_qrcode_generator__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @common/qrcode-generator */ "./src/common/qrcode-generator.tsx");
/* harmony import */ var _site_icon_png__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./site-icon.png */ "./src/app/desk/login/site-icon.png");
/* harmony import */ var _site_icon_png__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(_site_icon_png__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @common/utils */ "./src/common/utils.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _components_bind_user__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/bind-user */ "./src/app/desk/login/components/bind-user.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");










var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/login/sign-in.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      .ant-input-lg {\n        background: transparent !important;\n      }\n\n      .ant-input {\n        background: transparent !important;\n      }\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}















var isMobile = _common_utils__WEBPACK_IMPORTED_MODULE_20__["default"].getIsMobile();
var itemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 24
    }
  }
};

var SignIn =
/*#__PURE__*/
function (_Page) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(SignIn, _Page);

  function SignIn(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, SignIn);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(SignIn).call(this, props, state));

    _this.generateQrCode = function (options) {
      var url = options.url,
          lib = options.lib;

      var handle = function handle(resolve, reject, image) {
        var qrCodeSetting = {
          size: 400,
          ecLevel: lib.ecLevel.QUARTILE,
          minVersion: 8,
          background: "#fff",
          mode: image ? lib.modes.DRAW_WITH_IMAGE_BOX : undefined,
          radius: 0.5,
          image: image,
          mSize: 0.25
        };
        var element = document.createElement("CANVAS");
        var qrCode = new lib.qrcode(element);
        qrCode.generate(url, qrCodeSetting).then(function () {
          resolve(qrCode.getImage());
        }).catch(function () {
          reject();
        });
      };

      return new Promise(function (resolve, reject) {
        handle(resolve, reject, _site_icon_png__WEBPACK_IMPORTED_MODULE_18___default.a);
      });
    };

    _this.showLine = function () {
      _this.setState({
        isLineShow: true
      });

      _this.generateLineQR();
    };

    _this.showQrLogin = function () {
      _this.setState({
        isShowQrLogin: true
      });

      _this.generateAppQR();
    };

    _this.generateLineQR = function () {
      Object(_common_qrcode_generator__WEBPACK_IMPORTED_MODULE_17__["default"])().then(function (lib) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].ASK_LINE).then(function (response) {
          var _respData = (response.body || {}).respData,
              token = _respData.token,
              qrContent = _respData.qrContent;

          _this.generateQrCode({
            url: qrContent,
            lib: lib
          }).then(function (dataUrl) {
            _this.setState({
              lineQR: dataUrl
            }); // 5秒后第一次轮询


            setTimeout(function () {
              return _this.pollingAfterQRScan({
                token: token
              });
            }, 5000);
          });
        });
      });
    };

    _this.generateAppQR = function () {
      Object(_common_qrcode_generator__WEBPACK_IMPORTED_MODULE_17__["default"])().then(function (lib) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].ASK_APP_QR, {}, {
          headers: {
            userChannel: "B"
          }
        }).then(function (response) {
          var token = lodash__WEBPACK_IMPORTED_MODULE_19___default.a.get(response, "body.respData", "").replace("LOGIN:", "");

          _this.generateQrCode({
            url: token,
            lib: lib
          }).then(function (dataUrl) {
            _this.setState({
              lineQR: dataUrl
            }); // 3秒后第一次轮询


            setTimeout(function () {
              return _this.pollingAfterQRScan({
                token: token
              });
            }, 3000);
          });
        });
      });
    };

    _this.pollingAfterQRScan = function (options) {
      var token = options.token;

      if (_this.state.isLineShow || _this.state.isShowQrLogin) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].get("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].QR_LOGIN_POLL, "/").concat(token)).then(function (response) {
          var respData = lodash__WEBPACK_IMPORTED_MODULE_19___default.a.get(response.body, "respData", {});

          var qrStatus = respData.qrStatus;

          switch (qrStatus) {
            case "AUTHORIZED":
              _this.setState({
                qrSigned: true
              }, function () {
                setTimeout(function () {
                  _this.onSigninSuccess(respData);
                }, 3000);
              });

              break;

            case "EXPIRED":
              _this.generateLineQR();

              break;

            case "AUTHORIZED_WITHOUT_BOUND_USER":
              _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_21__["default"].create({
                Component: function Component(_ref) {
                  var onCancel = _ref.onCancel,
                      onOk = _ref.onOk;
                  return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Modal"], {
                    width: "40%",
                    maskClosable: false,
                    visible: true,
                    title: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Binding").thai("Binding").getMessage(),
                    onCancel: onCancel,
                    footer: null,
                    onOk: onOk,
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 428
                    },
                    __self: this
                  }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_components_bind_user__WEBPACK_IMPORTED_MODULE_22__["default"], {
                    onSigninSuccess: _this.onSigninSuccess,
                    token: token,
                    onCancel: onCancel,
                    model: _model__WEBPACK_IMPORTED_MODULE_23__["Modeller"].asProxied({}),
                    __source: {
                      fileName: _jsxFileName,
                      lineNumber: 438
                    },
                    __self: this
                  }));
                }
              });
              break;

            case "PENDING":
            case "SCANNED":
            default:
              setTimeout(function () {
                return _this.pollingAfterQRScan(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_3__["default"])({}, options));
              }, 500);
          }
        });
      }
    };

    _this.handleLink = function () {
      _this.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].FORGET_PASS);
    };

    _this.handleSubmit = function () {
      try {
        _this.setState({
          submitLoading: true
        });

        _this.props.form.validateFields({
          force: true
        },
        /*#__PURE__*/
        function () {
          var _ref2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
          /*#__PURE__*/
          _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(err, values) {
            var reToken, gmt, timeZone;
            return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (err) {
                      _context.next = 11;
                      break;
                    }

                    _context.next = 3;
                    return _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].tokenRecaptcha("Login");

                  case 3:
                    reToken = _context.sent;
                    gmt = _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].GlobalParams.session().get("timeZone");

                    if (gmt) {
                      _context.next = 10;
                      break;
                    }

                    _context.next = 8;
                    return _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].get("/timezone");

                  case 8:
                    timeZone = _context.sent;
                    _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].GlobalParams.session().set("timeZone", timeZone.body.respData);

                  case 10:
                    _this.getHelpers().getAjax().post(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].LOGIN_IN, values, {
                      ignoreAuth: true,
                      loading: false,
                      headers: {
                        "recaptcha-token": reToken
                      }
                    }).then(function (response) {
                      var respData = response.body.respData;

                      _this.onSigninSuccess(respData);
                    }).catch(function (error) {});

                  case 11:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));

          return function (_x, _x2) {
            return _ref2.apply(this, arguments);
          };
        }());
      } finally {
        setTimeout(function () {
          _this.setState({
            submitLoading: false
          });
        }, 100);
      }
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(SignIn, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(SignIn.prototype), "initState", this).call(this), {
        loading: false,
        src: "",
        checked: true,
        submitLoading: false,
        isLineShow: false,
        isShowQrLogin: false,
        lineQR: "",
        qrSigned: false
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(SignIn.prototype), "initComponents", this).call(this), Object.assign(_styles_login_style__WEBPACK_IMPORTED_MODULE_16__["default"], _sign_in_style__WEBPACK_IMPORTED_MODULE_14__["default"]));
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      var C = this.getComponents();
      var _this$state = this.state,
          isLineShow = _this$state.isLineShow,
          isShowQrLogin = _this$state.isShowQrLogin;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component_page__WEBPACK_IMPORTED_MODULE_11__["PageBody"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.Container, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.ContainerInner, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LoginLeft, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LoginP, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Hello!\n It is good to\n see you again").thai("สร้างคน!\n สร้างงาน\n สร้างคุณค่า").my("ဟယ်လို!\n နောက်တဖန်သင်တို့ကိုတွေ့မြင\n ်ကောင်းသောကြောင့်").getMessage())), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.Main, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.Login, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LoginHeader, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(SignIn.prototype), "isStateLogo", this).call(this) && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("img", {
        style: {
          maxHeight: "100%"
        },
        src: this.getSessionLogo(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.H3, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        type: "flex",
        justify: "center",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, this.renderTitle())), this.renderLoginArea(), this.renderLoginBar(), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("summary", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        type: "flex",
        justify: "center",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120
        },
        __self: this
      }, "@", new Date().getFullYear(), " Bytesforce"))))))));
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      var _this$state2 = this.state,
          isLineShow = _this$state2.isLineShow,
          isShowQrLogin = _this$state2.isShowQrLogin;
      if (isShowQrLogin) return "Scan QR below with mobile app";
      return _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Login to your account").thai("ลงชื่อเข้าใช้บัญชีของคุณ").my("လော့ဂ်အင်").getMessage();
    }
  }, {
    key: "renderLoginArea",
    value: function renderLoginArea() {
      var _this$state3 = this.state,
          isLineShow = _this$state3.isLineShow,
          isShowQrLogin = _this$state3.isShowQrLogin;
      if (isLineShow) return this.renderLine();
      if (isShowQrLogin) return this.renderQrLogin();
      return this.renderPasswordLogin();
    }
  }, {
    key: "renderPasswordLogin",
    value: function renderPasswordLogin() {
      var _this2 = this;

      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.FormBot, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 149
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: isMobile ? "mobile-sign" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_15__["NFormItem"], {
        rules: [{
          required: true,
          message: "UserName is required"
        }],
        form: this.props.form,
        propName: "userName",
        layoutCol: itemLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        type: "flex",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 157
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 3,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 158
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "user",
        style: {
          fontSize: "18px",
          margin: "0 2px",
          color: "#1890ff"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 21,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(InputUsername, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LoginInput, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Input"], {
        size: "large",
        autoFocus: true,
        placeholder: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("user account").thai("บัญชีผู้ใช้").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 171
        },
        __self: this
      })))))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_15__["NFormItem"], {
        form: this.props.form,
        rules: [{
          required: true,
          message: "Password is required"
        }],
        propName: "pwd",
        password: true,
        layoutCol: itemLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        type: "flex",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 3,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "lock",
        style: {
          fontSize: "18px",
          margin: "0 2px",
          color: "#1890ff"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 21,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LoginInput, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 202
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Input"].Password, {
        onPressEnter: function onPressEnter() {
          _this2.handleSubmit();
        },
        type: "password",
        placeholder: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("password").thai("รหัสผ่าน").getMessage(),
        size: "large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      })))))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Button"], {
        loading: this.state.submitLoading,
        style: {
          width: "100%",
          margin: "2rem 0"
        },
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this2.handleSubmit();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 218
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Login").thai("เข้าสู่ระบบ").my("လော့ဂ်အင်").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.FormCheckbox, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_15__["NFormItem"], {
        form: this.props.form,
        propName: "remember",
        layoutCol: itemLayout,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 231
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 232
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Checkbox"], {
        checked: this.state.checked,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 233
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Remember me").thai("จดจำฉัน").my("ငါ့ကိုသတိရပါ").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("label", {
        style: {
          float: "right",
          color: "#fff",
          cursor: "pointer"
        },
        onClick: function onClick() {
          return _this2.handleLink();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 239
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Forgot password?").thai("ลืมรหัสผ่าน?").my("စကားဝှက်ကိုမေ့နေပါသလား?").getMessage())))));
    }
  }, {
    key: "renderLine",
    value: function renderLine() {
      var _this3 = this;

      var _this$state4 = this.state,
          qrSigned = _this$state4.qrSigned,
          lineQR = _this$state4.lineQR;
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.CodeContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 257
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "or-code-content",
        "data-signed": qrSigned,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 258
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("img", {
        src: lineQR,
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 259
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-times",
        onClick: function onClick() {
          _this3.setState({
            lineQR: "",
            isShowQrLogin: false,
            isLineShow: false
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 260
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-redo-alt",
        onClick: function onClick() {
          return _this3.generateLineQR();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-check",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 275
        },
        __self: this
      })));
    }
  }, {
    key: "renderQrLogin",
    value: function renderQrLogin() {
      var _this4 = this;

      var _this$state5 = this.state,
          qrSigned = _this$state5.qrSigned,
          lineQR = _this$state5.lineQR;
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.CodeContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 283
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "or-code-content",
        "data-signed": qrSigned,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 284
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("img", {
        src: lineQR,
        alt: "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 285
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-times",
        onClick: function onClick() {
          _this4.setState({
            lineQR: "",
            isShowQrLogin: false,
            isLineShow: false
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 286
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-redo-alt",
        onClick: function onClick() {
          return _this4.generateAppQR();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 296
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fas fa-check",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 301
        },
        __self: this
      })));
    }
  }, {
    key: "renderLoginBar",
    value: function renderLoginBar() {
      var _this5 = this;

      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.LineContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "or-line-div",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309
        },
        __self: this
      }, "OR"), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "or-line-icon",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 310
        },
        __self: this
      }, !isMobile && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "iconfont icon-saomiao1",
        style: {
          fontSize: 31,
          paddingTop: 3
        },
        onClick: function onClick() {
          return _this5.showQrLogin();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fab fa-line",
        onClick: function onClick() {
          return _this5.showLine();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 313
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fab fa-linkedin",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 314
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fab fa-twitter-square",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 315
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        className: "fab fa-facebook-square",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 316
        },
        __self: this
      })));
    } // methods

  }, {
    key: "getSessionLogo",
    value: function getSessionLogo() {
      return _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].LOGO_KEY);
    }
  }, {
    key: "onSigninSuccess",
    value: function onSigninSuccess(userAccount) {
      var accessKey = userAccount.accessKey,
          user = userAccount.user,
          adminShortcutsMenuItemAuthorities = userAccount.adminShortcutsMenuItemAuthorities,
          mainMenuItemAuthorities = userAccount.mainMenuItemAuthorities,
          clientCompanyCode = userAccount.clientCompanyCode,
          allowToManageMiniSite = userAccount.allowToManageMiniSite,
          productAuthorities = userAccount.productAuthorities,
          localCurrencyCode = userAccount.localCurrencyCode,
          dashboardId = userAccount.dashboardId;
      var profile = user.profile;
      _common__WEBPACK_IMPORTED_MODULE_12__["Language"].language = !profile ? _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].LANGUAGE_EN : profile.preferredLang || _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].LANGUAGE_EN;
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ACCOUNT_KEY, user);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].DASHBOARD_ID, dashboardId);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].LOCAL_CURRENCY, localCurrencyCode);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Auth.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].AUTH_KEY, accessKey);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].AllowToManageMiniSite.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].MINI_SITE, allowToManageMiniSite);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].MAIN_MENU_ITEM_AUTHORITIES, mainMenuItemAuthorities);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ADMIN_SHORTCUTS_MENU_ITEM_AUTHORITIES, adminShortcutsMenuItemAuthorities);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].CLIENT_COMPANY_CODE, clientCompanyCode);
      _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Account.session().set(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].PRODUCT_AUTHORITIES, productAuthorities);
      _common__WEBPACK_IMPORTED_MODULE_12__["Authority"].initUser();
      this.getHelpers().getRouter().pushRedirect(_common__WEBPACK_IMPORTED_MODULE_12__["PATH"].DESK_HOME);
    }
  }]);

  return SignIn;
}(_component_page__WEBPACK_IMPORTED_MODULE_11__["Page"]);

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_13__["Form"].create()(SignIn));
var InputUsername = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svbG9naW4vc2lnbi1pbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9sb2dpbi9zaWduLWluLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBQYWdlLCBQYWdlQm9keSB9IGZyb20gXCJAY29tcG9uZW50L3BhZ2VcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgUGFnZUNvbXBvbmVudHMsIFFSQ29kZUdlbmVyYXRvckZ1bmMsIFFSQ29kZUdlbmVyYXRvckxpYiB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIEF1dGhvcml0eSwgQ29uc3RzLCBMYW5ndWFnZSwgUEFUSCwgU3RvcmFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgQnV0dG9uLCBDaGVja2JveCwgQ29sLCBGb3JtLCBJY29uLCBJbnB1dCwgTW9kYWwsIFJvdyB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuaW1wb3J0IHNpZ25TdHlsZSBmcm9tIFwiLi9zaWduLWluLXN0eWxlXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBORm9ybUl0ZW0gfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IExvZ2luU3R5bGUgZnJvbSBcIkBzdHlsZXMvbG9naW4tc3R5bGVcIjtcbmltcG9ydCBRUkNvZGVHZW5lcmF0b3IgZnJvbSBcIkBjb21tb24vcXJjb2RlLWdlbmVyYXRvclwiO1xuaW1wb3J0IExvZ29JbWFnZSBmcm9tIFwiLi9zaXRlLWljb24ucG5nXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgdXRpbHMgZnJvbSBcIkBjb21tb24vdXRpbHNcIjtcbmltcG9ydCBNYXNrIGZyb20gXCJAZGVzay1jb21wb25lbnQvY3JlYXRlLWRpYWxvZ1wiO1xuaW1wb3J0IEJpbmRVc2VyIGZyb20gXCIuL2NvbXBvbmVudHMvYmluZC11c2VyXCI7XG5pbXBvcnQgeyBNb2RlbGxlciB9IGZyb20gXCJAbW9kZWxcIjtcblxuY29uc3QgaXNNb2JpbGUgPSB1dGlscy5nZXRJc01vYmlsZSgpO1xuXG5leHBvcnQgdHlwZSBMb2dpblN0YXRlID0ge1xuICBsb2FkaW5nOiBib29sZWFuO1xuICBzcmM6IHN0cmluZztcbiAgY2hlY2tlZDogYm9vbGVhbjtcbiAgc3VibWl0TG9hZGluZzogYm9vbGVhbjtcbiAgaXNMaW5lU2hvdzogYm9vbGVhbjtcbiAgaXNTaG93UXJMb2dpbjogYm9vbGVhbjtcbiAgbGluZVFSOiBhbnk7XG4gIHFyU2lnbmVkOiBib29sZWFuO1xufTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU3R5bGVkSU5QVVQgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiaW5wdXRcIiwgYW55LCB7IHJlYWRPbmx5PzogYm9vbGVhbiB9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBTdHlsZWRQID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcInBcIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU3R5bGVkSGVhZGVyID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImhlYWRlclwiLCBhbnksIHt9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBTdHlsZWRIMyA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJoM1wiLCBhbnksIHt9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBTdHlsZWRTcGFuID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcInNwYW5cIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU2lnblBhZ2VDb21wb25lbnRzID0ge1xuICBIMzogU3R5bGVkSDM7XG4gIFRvcEJsYW5rOiBTdHlsZWRESVY7XG4gIExvZ2luOiBTdHlsZWRESVY7XG4gIExvZ2luTGVmdDogU3R5bGVkRElWO1xuICBDb250YWluZXI6IFN0eWxlZERJVjtcbiAgQ29udGFpbmVySW5uZXI6IFN0eWxlZERJVjtcbiAgTWFpbjogU3R5bGVkRElWO1xuICBMb2dpblA6IFN0eWxlZFA7XG4gIExvZ2luSGVhZGVyOiBTdHlsZWRIZWFkZXI7XG4gIFByZWZpeEljb246IFN0eWxlZERJVjtcbiAgTG9naW5JbnB1dDogU3R5bGVkSU5QVVQ7XG4gIEZvcm1Cb3Q6IFN0eWxlZERJVjtcbiAgRm9ybUNoZWNrYm94OiBTdHlsZWRESVY7XG4gIENoZWNrRGl2OiBTdHlsZWRESVY7XG4gIEljb25EaXY6IFN0eWxlZERJVjtcbiAgRmFpbGVkVGlwOiBTdHlsZWRTcGFuO1xuICBMaW5lQ29udGVudDogU3R5bGVkRElWO1xuICBDb2RlQ29udGVudDogU3R5bGVkRElWO1xufSAmIFBhZ2VDb21wb25lbnRzO1xuY29uc3QgaXRlbUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTYgfSxcbiAgICBzbTogeyBzcGFuOiAyNCB9LFxuICB9LFxufTtcblxuY2xhc3MgU2lnbkluPFAgZXh0ZW5kcyBGb3JtQ29tcG9uZW50UHJvcHMsIFMgZXh0ZW5kcyBMb2dpblN0YXRlLCBDIGV4dGVuZHMgU2lnblBhZ2VDb21wb25lbnRzPiBleHRlbmRzIFBhZ2U8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBzdGF0ZT86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBzdGF0ZSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgIHNyYzogXCJcIixcbiAgICAgIGNoZWNrZWQ6IHRydWUsXG4gICAgICBzdWJtaXRMb2FkaW5nOiBmYWxzZSxcbiAgICAgIGlzTGluZVNob3c6IGZhbHNlLFxuICAgICAgaXNTaG93UXJMb2dpbjogZmFsc2UsXG4gICAgICBsaW5lUVI6IFwiXCIsXG4gICAgICBxclNpZ25lZDogZmFsc2UsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0Q29tcG9uZW50cygpLCBPYmplY3QuYXNzaWduKExvZ2luU3R5bGUsIHNpZ25TdHlsZSkpIGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyUGFnZUJvZHkoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgaXNMaW5lU2hvdywgaXNTaG93UXJMb2dpbiB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8UGFnZUJvZHk+XG4gICAgICAgIDxDLkNvbnRhaW5lcj5cbiAgICAgICAgICA8Qy5Db250YWluZXJJbm5lcj5cbiAgICAgICAgICAgIDxDLkxvZ2luTGVmdD5cbiAgICAgICAgICAgICAgPEMuTG9naW5QPlxuICAgICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkhlbGxvIVxcbiBJdCBpcyBnb29kIHRvXFxuIHNlZSB5b3UgYWdhaW5cIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Liq4Lij4LmJ4Liy4LiH4LiE4LiZIVxcbiDguKrguKPguYnguLLguIfguIfguLLguJlcXG4g4Liq4Lij4LmJ4Liy4LiH4LiE4Li44LiT4LiE4LmI4LiyXCIpXG4gICAgICAgICAgICAgICAgICAubXkoXCLhgJ/hgJrhgLrhgJzhgK3hgK8hXFxuIOGAlOGAseGArOGAgOGAuuGAkOGAluGAlOGAuuGAnuGAhOGAuuGAkOGAreGAr+GAt+GAgOGAreGAr+GAkOGAveGAseGAt+GAmeGAvOGAhFxcbiDhgLrhgIDhgLHhgKzhgIThgLrhgLjhgJ7hgLHhgKzhgIDhgLzhgLHhgKzhgIThgLrhgLdcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIDwvQy5Mb2dpblA+XG4gICAgICAgICAgICA8L0MuTG9naW5MZWZ0PlxuICAgICAgICAgICAgPEMuTWFpbj5cbiAgICAgICAgICAgICAgPEMuTG9naW4+XG4gICAgICAgICAgICAgICAgPEMuTG9naW5IZWFkZXI+XG4gICAgICAgICAgICAgICAgICB7c3VwZXIuaXNTdGF0ZUxvZ28oKSAmJiA8aW1nIHN0eWxlPXt7IG1heEhlaWdodDogXCIxMDAlXCIgfX0gc3JjPXt0aGlzLmdldFNlc3Npb25Mb2dvKCl9Lz59XG4gICAgICAgICAgICAgICAgPC9DLkxvZ2luSGVhZGVyPlxuICAgICAgICAgICAgICAgIDxDLkgzPlxuICAgICAgICAgICAgICAgICAgPFJvdyB0eXBlPVwiZmxleFwiIGp1c3RpZnk9XCJjZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAge3RoaXMucmVuZGVyVGl0bGUoKX1cbiAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgIDwvQy5IMz5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJMb2dpbkFyZWEoKX1cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJMb2dpbkJhcigpfVxuICAgICAgICAgICAgICAgIDxzdW1tYXJ5PlxuICAgICAgICAgICAgICAgICAgPFJvdyB0eXBlPVwiZmxleFwiIGp1c3RpZnk9XCJjZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGg1PkB7bmV3IERhdGUoKS5nZXRGdWxsWWVhcigpfSBCeXRlc2ZvcmNlPC9oNT5cbiAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgIDwvc3VtbWFyeT5cbiAgICAgICAgICAgICAgPC9DLkxvZ2luPlxuICAgICAgICAgICAgPC9DLk1haW4+XG4gICAgICAgICAgPC9DLkNvbnRhaW5lcklubmVyPlxuICAgICAgICA8L0MuQ29udGFpbmVyPlxuICAgICAgPC9QYWdlQm9keT5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyVGl0bGUoKSB7XG4gICAgY29uc3QgeyBpc0xpbmVTaG93LCBpc1Nob3dRckxvZ2luIH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmIChpc1Nob3dRckxvZ2luKSByZXR1cm4gXCJTY2FuIFFSIGJlbG93IHdpdGggbW9iaWxlIGFwcFwiO1xuICAgIHJldHVybiBMYW5ndWFnZS5lbihcIkxvZ2luIHRvIHlvdXIgYWNjb3VudFwiKVxuICAgICAgLnRoYWkoXCLguKXguIfguIrguLfguYjguK3guYDguILguYnguLLguYPguIrguYnguJrguLHguI3guIrguLXguILguK3guIfguITguLjguJNcIilcbiAgICAgIC5teShcIuGAnOGAseGArOGAt+GAguGAuuGAoeGAhOGAulwiKVxuICAgICAgLmdldE1lc3NhZ2UoKTtcbiAgfVxuXG4gIHJlbmRlckxvZ2luQXJlYSgpIHtcbiAgICBjb25zdCB7IGlzTGluZVNob3csIGlzU2hvd1FyTG9naW4gfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKGlzTGluZVNob3cpIHJldHVybiB0aGlzLnJlbmRlckxpbmUoKTtcbiAgICBpZiAoaXNTaG93UXJMb2dpbikgcmV0dXJuIHRoaXMucmVuZGVyUXJMb2dpbigpO1xuICAgIHJldHVybiB0aGlzLnJlbmRlclBhc3N3b3JkTG9naW4oKTtcbiAgfVxuXG4gIHJlbmRlclBhc3N3b3JkTG9naW4oKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiA8Qy5Gb3JtQm90PlxuICAgICAgPGRpdiBjbGFzc05hbWU9e2lzTW9iaWxlID8gXCJtb2JpbGUtc2lnblwiIDogXCJcIn0+XG4gICAgICAgIDxORm9ybUl0ZW1cbiAgICAgICAgICBydWxlcz17W3sgcmVxdWlyZWQ6IHRydWUsIG1lc3NhZ2U6IFwiVXNlck5hbWUgaXMgcmVxdWlyZWRcIiB9XX1cbiAgICAgICAgICBmb3JtPXt0aGlzLnByb3BzLmZvcm19XG4gICAgICAgICAgcHJvcE5hbWU9XCJ1c2VyTmFtZVwiXG4gICAgICAgICAgbGF5b3V0Q29sPXtpdGVtTGF5b3V0fVxuICAgICAgICA+XG4gICAgICAgICAgPFJvdyB0eXBlPVwiZmxleFwiPlxuICAgICAgICAgICAgPENvbCBzcGFuPXszfT5cbiAgICAgICAgICAgICAgPEljb25cbiAgICAgICAgICAgICAgICB0eXBlPVwidXNlclwiXG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjE4cHhcIixcbiAgICAgICAgICAgICAgICAgIG1hcmdpbjogXCIwIDJweFwiLFxuICAgICAgICAgICAgICAgICAgY29sb3I6IFwiIzE4OTBmZlwiLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17MjF9PlxuICAgICAgICAgICAgICA8SW5wdXRVc2VybmFtZT5cbiAgICAgICAgICAgICAgICA8Qy5Mb2dpbklucHV0PlxuICAgICAgICAgICAgICAgICAgPElucHV0XG4gICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1c1xuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17TGFuZ3VhZ2UuZW4oXCJ1c2VyIGFjY291bnRcIilcbiAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4muC4seC4jeC4iuC4teC4nOC4ueC5ieC5g+C4iuC5iVwiKVxuICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvQy5Mb2dpbklucHV0PlxuICAgICAgICAgICAgICA8L0lucHV0VXNlcm5hbWU+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgPC9ORm9ybUl0ZW0+XG4gICAgICAgIDxORm9ybUl0ZW1cbiAgICAgICAgICBmb3JtPXt0aGlzLnByb3BzLmZvcm19XG4gICAgICAgICAgcnVsZXM9e1t7IHJlcXVpcmVkOiB0cnVlLCBtZXNzYWdlOiBcIlBhc3N3b3JkIGlzIHJlcXVpcmVkXCIgfV19XG4gICAgICAgICAgcHJvcE5hbWU9XCJwd2RcIlxuICAgICAgICAgIHBhc3N3b3JkPXt0cnVlfVxuICAgICAgICAgIGxheW91dENvbD17aXRlbUxheW91dH1cbiAgICAgICAgPlxuICAgICAgICAgIDxSb3cgdHlwZT1cImZsZXhcIj5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17M30+XG4gICAgICAgICAgICAgIDxJY29uXG4gICAgICAgICAgICAgICAgdHlwZT1cImxvY2tcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxOHB4XCIsXG4gICAgICAgICAgICAgICAgICBtYXJnaW46IFwiMCAycHhcIixcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiBcIiMxODkwZmZcIixcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8Q29sIHNwYW49ezIxfT5cbiAgICAgICAgICAgICAgPEMuTG9naW5JbnB1dD5cbiAgICAgICAgICAgICAgICA8SW5wdXQuUGFzc3dvcmRcbiAgICAgICAgICAgICAgICAgIG9uUHJlc3NFbnRlcj17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZVN1Ym1pdCgpO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17TGFuZ3VhZ2UuZW4oXCJwYXNzd29yZFwiKVxuICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4quC4nOC5iOC4suC4mVwiKVxuICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L0MuTG9naW5JbnB1dD5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgIDwvUm93PlxuICAgICAgICA8L05Gb3JtSXRlbT5cbiAgICAgIDwvZGl2PlxuICAgICAgPEJ1dHRvblxuICAgICAgICBsb2FkaW5nPXt0aGlzLnN0YXRlLnN1Ym1pdExvYWRpbmd9XG4gICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiwgbWFyZ2luOiBcIjJyZW0gMFwiIH19XG4gICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgIHR5cGU9XCJwcmltYXJ5XCJcbiAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVTdWJtaXQoKX1cbiAgICAgID5cbiAgICAgICAge0xhbmd1YWdlLmVuKFwiTG9naW5cIilcbiAgICAgICAgICAudGhhaShcIuC5gOC4guC5ieC4suC4quC4ueC5iOC4o+C4sOC4muC4mlwiKVxuICAgICAgICAgIC5teShcIuGAnOGAseGArOGAt+GAguGAuuGAoeGAhOGAulwiKVxuICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICA8L0J1dHRvbj5cbiAgICAgIDxDLkZvcm1DaGVja2JveD5cbiAgICAgICAgPE5Gb3JtSXRlbSBmb3JtPXt0aGlzLnByb3BzLmZvcm19IHByb3BOYW1lPVwicmVtZW1iZXJcIiBsYXlvdXRDb2w9e2l0ZW1MYXlvdXR9PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8Q2hlY2tib3ggY2hlY2tlZD17dGhpcy5zdGF0ZS5jaGVja2VkfT5cbiAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiUmVtZW1iZXIgbWVcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4iOC4lOC4iOC4s+C4ieC4seC4mVwiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAhOGAq+GAt+GAgOGAreGAr+GAnuGAkOGAreGAm+GAleGAq1wiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA8L0NoZWNrYm94PlxuICAgICAgICAgICAgPGxhYmVsXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGZsb2F0OiBcInJpZ2h0XCIsIGNvbG9yOiBcIiNmZmZcIiwgY3Vyc29yOiBcInBvaW50ZXJcIiB9fVxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUxpbmsoKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiRm9yZ290IHBhc3N3b3JkP1wiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwi4Lil4Li34Lih4Lij4Lir4Lix4Liq4Lic4LmI4Liy4LiZP1wiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAheGAgOGArOGAuOGAneGAvuGAgOGAuuGAgOGAreGAr+GAmeGAseGAt+GAlOGAseGAleGAq+GAnuGAnOGArOGAuD9cIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9ORm9ybUl0ZW0+XG4gICAgICA8L0MuRm9ybUNoZWNrYm94PlxuICAgIDwvQy5Gb3JtQm90PjtcbiAgfVxuXG4gIHJlbmRlckxpbmUoKSB7XG4gICAgY29uc3QgeyBxclNpZ25lZCwgbGluZVFSIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICByZXR1cm4gPEMuQ29kZUNvbnRlbnQ+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nb3ItY29kZS1jb250ZW50JyBkYXRhLXNpZ25lZD17cXJTaWduZWR9PlxuICAgICAgICA8aW1nIHNyYz17bGluZVFSfSBhbHQ9XCJcIi8+XG4gICAgICAgIDxpXG4gICAgICAgICAgY2xhc3NOYW1lPVwiZmFzIGZhLXRpbWVzXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgbGluZVFSOiBcIlwiLFxuICAgICAgICAgICAgICBpc1Nob3dRckxvZ2luOiBmYWxzZSxcbiAgICAgICAgICAgICAgaXNMaW5lU2hvdzogZmFsc2UsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9fVxuICAgICAgICAvPlxuICAgICAgICA8aVxuICAgICAgICAgIGNsYXNzTmFtZT1cImZhcyBmYS1yZWRvLWFsdFwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5nZW5lcmF0ZUxpbmVRUigpXG4gICAgICAgICAgfVxuICAgICAgICAvPlxuICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtY2hlY2tcIi8+XG4gICAgICA8L2Rpdj5cbiAgICA8L0MuQ29kZUNvbnRlbnQ+O1xuICB9XG5cbiAgcmVuZGVyUXJMb2dpbigpIHtcbiAgICBjb25zdCB7IHFyU2lnbmVkLCBsaW5lUVIgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiA8Qy5Db2RlQ29udGVudD5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPSdvci1jb2RlLWNvbnRlbnQnIGRhdGEtc2lnbmVkPXtxclNpZ25lZH0+XG4gICAgICAgIDxpbWcgc3JjPXtsaW5lUVJ9IGFsdD1cIlwiLz5cbiAgICAgICAgPGlcbiAgICAgICAgICBjbGFzc05hbWU9XCJmYXMgZmEtdGltZXNcIlxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBsaW5lUVI6IFwiXCIsXG4gICAgICAgICAgICAgIGlzU2hvd1FyTG9naW46IGZhbHNlLFxuICAgICAgICAgICAgICBpc0xpbmVTaG93OiBmYWxzZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH19XG4gICAgICAgIC8+XG4gICAgICAgIDxpXG4gICAgICAgICAgY2xhc3NOYW1lPVwiZmFzIGZhLXJlZG8tYWx0XCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmdlbmVyYXRlQXBwUVIoKVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cbiAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWNoZWNrXCIvPlxuICAgICAgPC9kaXY+XG4gICAgPC9DLkNvZGVDb250ZW50PjtcbiAgfVxuXG4gIHJlbmRlckxvZ2luQmFyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICByZXR1cm4gPEMuTGluZUNvbnRlbnQ+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nb3ItbGluZS1kaXYnPk9SPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT0nb3ItbGluZS1pY29uJz5cbiAgICAgICAgeyFpc01vYmlsZSAmJiA8aSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLXNhb21pYW8xXCIgc3R5bGU9e3sgZm9udFNpemU6IDMxLCBwYWRkaW5nVG9wOiAzIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5zaG93UXJMb2dpbigpfS8+fVxuICAgICAgICA8aSBjbGFzc05hbWU9XCJmYWIgZmEtbGluZVwiIG9uQ2xpY2s9eygpID0+IHRoaXMuc2hvd0xpbmUoKX0vPlxuICAgICAgICA8aSBjbGFzc05hbWU9XCJmYWIgZmEtbGlua2VkaW5cIi8+XG4gICAgICAgIDxpIGNsYXNzTmFtZT1cImZhYiBmYS10d2l0dGVyLXNxdWFyZVwiLz5cbiAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFiIGZhLWZhY2Vib29rLXNxdWFyZVwiLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvQy5MaW5lQ29udGVudD47XG4gIH1cblxuICAvLyBtZXRob2RzXG4gIHByb3RlY3RlZCBnZXRTZXNzaW9uTG9nbygpIHtcbiAgICByZXR1cm4gU3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuTE9HT19LRVkpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZVFyQ29kZSA9IChvcHRpb25zOiB7IHVybDogc3RyaW5nOyBsaWI6IFFSQ29kZUdlbmVyYXRvckxpYiB9KTogUHJvbWlzZTxzdHJpbmc+ID0+IHtcbiAgICBjb25zdCB7IHVybCwgbGliIH0gPSBvcHRpb25zO1xuXG4gICAgY29uc3QgaGFuZGxlID0gKHJlc29sdmU6IEZ1bmN0aW9uLCByZWplY3Q6IEZ1bmN0aW9uLCBpbWFnZTogYW55KTogdm9pZCA9PiB7XG4gICAgICBjb25zdCBxckNvZGVTZXR0aW5nID0ge1xuICAgICAgICBzaXplOiA0MDAsXG4gICAgICAgIGVjTGV2ZWw6IGxpYi5lY0xldmVsLlFVQVJUSUxFLFxuICAgICAgICBtaW5WZXJzaW9uOiA4LFxuICAgICAgICBiYWNrZ3JvdW5kOiBcIiNmZmZcIixcbiAgICAgICAgbW9kZTogaW1hZ2UgPyBsaWIubW9kZXMuRFJBV19XSVRIX0lNQUdFX0JPWCA6IHVuZGVmaW5lZCxcbiAgICAgICAgcmFkaXVzOiAwLjUsXG4gICAgICAgIGltYWdlOiBpbWFnZSxcbiAgICAgICAgbVNpemU6IDAuMjUsXG4gICAgICB9O1xuICAgICAgY29uc3QgZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJDQU5WQVNcIikgYXMgSFRNTENhbnZhc0VsZW1lbnQ7XG4gICAgICBjb25zdCBxckNvZGUgPSBuZXcgbGliLnFyY29kZShlbGVtZW50KSBhcyBRUkNvZGVHZW5lcmF0b3JGdW5jO1xuICAgICAgcXJDb2RlXG4gICAgICAgIC5nZW5lcmF0ZSh1cmwsIHFyQ29kZVNldHRpbmcpXG4gICAgICAgIC50aGVuKCgpID0+IHtcbiAgICAgICAgICByZXNvbHZlKHFyQ29kZS5nZXRJbWFnZSgpKTtcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKCgpID0+IHtcbiAgICAgICAgICByZWplY3QoKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgaGFuZGxlKHJlc29sdmUsIHJlamVjdCwgTG9nb0ltYWdlKTtcbiAgICB9KTtcbiAgfTtcblxuICAvLyBhY3Rpb25zXG4gIHByb3RlY3RlZCBzaG93TGluZSA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGlzTGluZVNob3c6IHRydWUsXG4gICAgfSk7XG4gICAgdGhpcy5nZW5lcmF0ZUxpbmVRUigpO1xuICB9O1xuXG4gIHByb3RlY3RlZCBzaG93UXJMb2dpbiA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGlzU2hvd1FyTG9naW46IHRydWUsXG4gICAgfSk7XG4gICAgdGhpcy5nZW5lcmF0ZUFwcFFSKCk7XG4gIH07XG5cbiAgcHJvdGVjdGVkIGdlbmVyYXRlTGluZVFSID0gKCkgPT4ge1xuICAgIFFSQ29kZUdlbmVyYXRvcigpLnRoZW4oKGxpYjogUVJDb2RlR2VuZXJhdG9yTGliKSA9PiB7XG4gICAgICBBamF4LnBvc3QoQXBpcy5BU0tfTElORSkudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHRva2VuLCBxckNvbnRlbnQgfSA9IChyZXNwb25zZS5ib2R5IHx8IHt9KS5yZXNwRGF0YTtcbiAgICAgICAgdGhpcy5nZW5lcmF0ZVFyQ29kZSh7IHVybDogcXJDb250ZW50LCBsaWIgfSkudGhlbihkYXRhVXJsID0+IHtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGxpbmVRUjogZGF0YVVybCxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyA156eS5ZCO56ys5LiA5qyh6L2u6K+iXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnBvbGxpbmdBZnRlclFSU2Nhbih7IHRva2VuIH0pLCA1MDAwKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfTtcblxuICBwcm90ZWN0ZWQgZ2VuZXJhdGVBcHBRUiA9ICgpID0+IHtcbiAgICBRUkNvZGVHZW5lcmF0b3IoKS50aGVuKChsaWI6IFFSQ29kZUdlbmVyYXRvckxpYikgPT4ge1xuICAgICAgQWpheC5wb3N0KEFwaXMuQVNLX0FQUF9RUiwge30sIHtcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIHVzZXJDaGFubmVsOiBcIkJcIixcbiAgICAgICAgfSxcbiAgICAgIH0pLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgdG9rZW4gPSBfLmdldChyZXNwb25zZSwgXCJib2R5LnJlc3BEYXRhXCIsIFwiXCIpLnJlcGxhY2UoXCJMT0dJTjpcIiwgXCJcIik7XG4gICAgICAgIHRoaXMuZ2VuZXJhdGVRckNvZGUoeyB1cmw6IHRva2VuLCBsaWIgfSkudGhlbihkYXRhVXJsID0+IHtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGxpbmVRUjogZGF0YVVybCxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyAz56eS5ZCO56ys5LiA5qyh6L2u6K+iXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnBvbGxpbmdBZnRlclFSU2Nhbih7IHRva2VuIH0pLCAzMDAwKTtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfTtcblxuICBwcml2YXRlIHBvbGxpbmdBZnRlclFSU2NhbiA9IChvcHRpb25zOiB7XG4gICAgdG9rZW46IGFueTtcbiAgfSk6IHZvaWQgPT4ge1xuICAgIGNvbnN0IHsgdG9rZW4gfSA9IG9wdGlvbnM7XG4gICAgaWYgKHRoaXMuc3RhdGUuaXNMaW5lU2hvdyB8fCB0aGlzLnN0YXRlLmlzU2hvd1FyTG9naW4pIHtcbiAgICAgIEFqYXguZ2V0KGAke0FwaXMuUVJfTE9HSU5fUE9MTH0vJHt0b2tlbn1gKS50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHJlc3BEYXRhID0gXy5nZXQocmVzcG9uc2UuYm9keSwgXCJyZXNwRGF0YVwiLCB7fSk7XG4gICAgICAgIGNvbnN0IHsgcXJTdGF0dXMgfSA9IHJlc3BEYXRhO1xuICAgICAgICBzd2l0Y2ggKHFyU3RhdHVzKSB7XG4gICAgICAgICAgY2FzZSBcIkFVVEhPUklaRURcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBxclNpZ25lZDogdHJ1ZSxcbiAgICAgICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5vblNpZ25pblN1Y2Nlc3MocmVzcERhdGEpO1xuICAgICAgICAgICAgICB9LCAzMDAwKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIkVYUElSRURcIjpcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVMaW5lUVIoKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCJBVVRIT1JJWkVEX1dJVEhPVVRfQk9VTkRfVVNFUlwiOlxuICAgICAgICAgICAgTWFzay5jcmVhdGUoe1xuICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPE1vZGFsXG4gICAgICAgICAgICAgICAgd2lkdGg9XCI0MCVcIlxuICAgICAgICAgICAgICAgIG1hc2tDbG9zYWJsZT17ZmFsc2V9XG4gICAgICAgICAgICAgICAgdmlzaWJsZT17dHJ1ZX1cbiAgICAgICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJCaW5kaW5nXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIkJpbmRpbmdcIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICAgICAgICAgIGZvb3Rlcj17bnVsbH1cbiAgICAgICAgICAgICAgICBvbk9rPXtvbk9rfT5cbiAgICAgICAgICAgICAgICA8QmluZFVzZXIgb25TaWduaW5TdWNjZXNzPXt0aGlzLm9uU2lnbmluU3VjY2Vzc30gdG9rZW49e3Rva2VufSBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQoe30pfS8+XG4gICAgICAgICAgICAgIDwvTW9kYWw+LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiUEVORElOR1wiOlxuICAgICAgICAgIGNhc2UgXCJTQ0FOTkVEXCI6XG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy5wb2xsaW5nQWZ0ZXJRUlNjYW4oeyAuLi5vcHRpb25zIH0pLCA1MDApO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgcHJpdmF0ZSBvblNpZ25pblN1Y2Nlc3ModXNlckFjY291bnQ6IGFueSk6IHZvaWQge1xuICAgIGNvbnN0IHtcbiAgICAgIGFjY2Vzc0tleSxcbiAgICAgIHVzZXIsXG4gICAgICBhZG1pblNob3J0Y3V0c01lbnVJdGVtQXV0aG9yaXRpZXMsXG4gICAgICBtYWluTWVudUl0ZW1BdXRob3JpdGllcyxcbiAgICAgIGNsaWVudENvbXBhbnlDb2RlLFxuICAgICAgYWxsb3dUb01hbmFnZU1pbmlTaXRlLFxuICAgICAgcHJvZHVjdEF1dGhvcml0aWVzLFxuICAgICAgbG9jYWxDdXJyZW5jeUNvZGUsXG4gICAgICBkYXNoYm9hcmRJZCxcbiAgICB9ID0gdXNlckFjY291bnQ7XG4gICAgY29uc3QgeyBwcm9maWxlIH0gPSB1c2VyO1xuICAgIExhbmd1YWdlLmxhbmd1YWdlID0gIXByb2ZpbGUgPyBDb25zdHMuTEFOR1VBR0VfRU4gOiBwcm9maWxlLnByZWZlcnJlZExhbmcgfHwgQ29uc3RzLkxBTkdVQUdFX0VOO1xuICAgIFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuc2V0KENvbnN0cy5BQ0NPVU5UX0tFWSwgdXNlcik7XG4gICAgU3RvcmFnZS5BY2NvdW50LnNlc3Npb24oKS5zZXQoQ29uc3RzLkRBU0hCT0FSRF9JRCwgZGFzaGJvYXJkSWQpO1xuICAgIFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuc2V0KENvbnN0cy5MT0NBTF9DVVJSRU5DWSwgbG9jYWxDdXJyZW5jeUNvZGUpO1xuICAgIFN0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuc2V0KENvbnN0cy5BVVRIX0tFWSwgYWNjZXNzS2V5KTtcbiAgICBTdG9yYWdlLkFsbG93VG9NYW5hZ2VNaW5pU2l0ZS5zZXNzaW9uKCkuc2V0KENvbnN0cy5NSU5JX1NJVEUsIGFsbG93VG9NYW5hZ2VNaW5pU2l0ZSk7XG4gICAgU3RvcmFnZS5BY2NvdW50LnNlc3Npb24oKS5zZXQoQ29uc3RzLk1BSU5fTUVOVV9JVEVNX0FVVEhPUklUSUVTLCBtYWluTWVudUl0ZW1BdXRob3JpdGllcyk7XG4gICAgU3RvcmFnZS5BY2NvdW50LnNlc3Npb24oKS5zZXQoQ29uc3RzLkFETUlOX1NIT1JUQ1VUU19NRU5VX0lURU1fQVVUSE9SSVRJRVMsIGFkbWluU2hvcnRjdXRzTWVudUl0ZW1BdXRob3JpdGllcyk7XG4gICAgU3RvcmFnZS5BY2NvdW50LnNlc3Npb24oKS5zZXQoQ29uc3RzLkNMSUVOVF9DT01QQU5ZX0NPREUsIGNsaWVudENvbXBhbnlDb2RlKTtcbiAgICBTdG9yYWdlLkFjY291bnQuc2Vzc2lvbigpLnNldChDb25zdHMuUFJPRFVDVF9BVVRIT1JJVElFUywgcHJvZHVjdEF1dGhvcml0aWVzKTtcbiAgICBBdXRob3JpdHkuaW5pdFVzZXIoKTtcbiAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgLmdldFJvdXRlcigpXG4gICAgICAucHVzaFJlZGlyZWN0KFBBVEguREVTS19IT01FKTtcbiAgfVxuXG4gIHByaXZhdGUgaGFuZGxlTGluayA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgLmdldFJvdXRlcigpXG4gICAgICAucmVwbGFjZVJlZGlyZWN0KFBBVEguRk9SR0VUX1BBU1MpO1xuICB9O1xuXG4gIHByaXZhdGUgaGFuZGxlU3VibWl0ID0gKCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgc3VibWl0TG9hZGluZzogdHJ1ZSB9KTtcbiAgICAgIHRoaXMucHJvcHMuZm9ybS52YWxpZGF0ZUZpZWxkcyh7IGZvcmNlOiB0cnVlIH0sIGFzeW5jIChlcnI6IGFueSwgdmFsdWVzOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKCFlcnIpIHtcbiAgICAgICAgICBjb25zdCByZVRva2VuID0gYXdhaXQgVXRpbHMudG9rZW5SZWNhcHRjaGEoXCJMb2dpblwiKTtcbiAgICAgICAgICBjb25zdCBnbXQgPSBTdG9yYWdlLkdsb2JhbFBhcmFtcy5zZXNzaW9uKCkuZ2V0KFwidGltZVpvbmVcIik7XG4gICAgICAgICAgaWYgKCFnbXQpIHtcbiAgICAgICAgICAgIGNvbnN0IHRpbWVab25lID0gYXdhaXQgQWpheC5nZXQoXCIvdGltZXpvbmVcIik7XG4gICAgICAgICAgICBTdG9yYWdlLkdsb2JhbFBhcmFtcy5zZXNzaW9uKCkuc2V0KFwidGltZVpvbmVcIiwgdGltZVpvbmUuYm9keS5yZXNwRGF0YSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgICAucG9zdChBcGlzLkxPR0lOX0lOLCB2YWx1ZXMsIHtcbiAgICAgICAgICAgICAgaWdub3JlQXV0aDogdHJ1ZSxcbiAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICBcInJlY2FwdGNoYS10b2tlblwiOiByZVRva2VuLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICAgICAgICAgIHRoaXMub25TaWduaW5TdWNjZXNzKHJlc3BEYXRhKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZmluYWxseSB7XG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgc3VibWl0TG9hZGluZzogZmFsc2UsXG4gICAgICAgIH0pO1xuICAgICAgfSwgMTAwKTtcbiAgICB9XG4gIH07XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgRm9ybS5jcmVhdGUoKShTaWduSW4pO1xuXG5cbmNvbnN0IElucHV0VXNlcm5hbWUgPSBTdHlsZWQuZGl2YFxuICAgICAgLmFudC1pbnB1dC1sZyB7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICB9XG5cbiAgICAgIC5hbnQtaW5wdXQge1xuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgICAgfVxuXG5gO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQXNDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFpUUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE5UkE7QUFpU0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUF0U0E7QUF3U0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUE3U0E7QUErU0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1VEE7QUE4VEE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL1VBO0FBa1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZBO0FBREE7QUFlQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBakNBO0FBbUNBO0FBQ0E7QUFDQTtBQUNBO0FBOVhBO0FBNlpBO0FBR0E7QUFDQTtBQWphQTtBQW1hQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFQQTtBQVFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBdGNBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBSUE7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBZ0lBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTs7OztBQTNaQTtBQUNBO0FBeWNBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/login/sign-in.tsx
