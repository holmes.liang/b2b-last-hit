/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var modelUtil = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var ComponentModel = __webpack_require__(/*! ./Component */ "./node_modules/echarts/lib/model/Component.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * ECharts option manager
 *
 * @module {echarts/model/OptionManager}
 */


var each = zrUtil.each;
var clone = zrUtil.clone;
var map = zrUtil.map;
var merge = zrUtil.merge;
var QUERY_REG = /^(min|max)?(.+)$/;
/**
 * TERM EXPLANATIONS:
 *
 * [option]:
 *
 *     An object that contains definitions of components. For example:
 *     var option = {
 *         title: {...},
 *         legend: {...},
 *         visualMap: {...},
 *         series: [
 *             {data: [...]},
 *             {data: [...]},
 *             ...
 *         ]
 *     };
 *
 * [rawOption]:
 *
 *     An object input to echarts.setOption. 'rawOption' may be an
 *     'option', or may be an object contains multi-options. For example:
 *     var option = {
 *         baseOption: {
 *             title: {...},
 *             legend: {...},
 *             series: [
 *                 {data: [...]},
 *                 {data: [...]},
 *                 ...
 *             ]
 *         },
 *         timeline: {...},
 *         options: [
 *             {title: {...}, series: {data: [...]}},
 *             {title: {...}, series: {data: [...]}},
 *             ...
 *         ],
 *         media: [
 *             {
 *                 query: {maxWidth: 320},
 *                 option: {series: {x: 20}, visualMap: {show: false}}
 *             },
 *             {
 *                 query: {minWidth: 320, maxWidth: 720},
 *                 option: {series: {x: 500}, visualMap: {show: true}}
 *             },
 *             {
 *                 option: {series: {x: 1200}, visualMap: {show: true}}
 *             }
 *         ]
 *     };
 *
 * @alias module:echarts/model/OptionManager
 * @param {module:echarts/ExtensionAPI} api
 */

function OptionManager(api) {
  /**
   * @private
   * @type {module:echarts/ExtensionAPI}
   */
  this._api = api;
  /**
   * @private
   * @type {Array.<number>}
   */

  this._timelineOptions = [];
  /**
   * @private
   * @type {Array.<Object>}
   */

  this._mediaList = [];
  /**
   * @private
   * @type {Object}
   */

  this._mediaDefault;
  /**
   * -1, means default.
   * empty means no media.
   * @private
   * @type {Array.<number>}
   */

  this._currentMediaIndices = [];
  /**
   * @private
   * @type {Object}
   */

  this._optionBackup;
  /**
   * @private
   * @type {Object}
   */

  this._newBaseOption;
} // timeline.notMerge is not supported in ec3. Firstly there is rearly
// case that notMerge is needed. Secondly supporting 'notMerge' requires
// rawOption cloned and backuped when timeline changed, which does no
// good to performance. What's more, that both timeline and setOption
// method supply 'notMerge' brings complex and some problems.
// Consider this case:
// (step1) chart.setOption({timeline: {notMerge: false}, ...}, false);
// (step2) chart.setOption({timeline: {notMerge: true}, ...}, false);


OptionManager.prototype = {
  constructor: OptionManager,

  /**
   * @public
   * @param {Object} rawOption Raw option.
   * @param {module:echarts/model/Global} ecModel
   * @param {Array.<Function>} optionPreprocessorFuncs
   * @return {Object} Init option
   */
  setOption: function setOption(rawOption, optionPreprocessorFuncs) {
    if (rawOption) {
      // That set dat primitive is dangerous if user reuse the data when setOption again.
      zrUtil.each(modelUtil.normalizeToArray(rawOption.series), function (series) {
        series && series.data && zrUtil.isTypedArray(series.data) && zrUtil.setAsPrimitive(series.data);
      });
    } // Caution: some series modify option data, if do not clone,
    // it should ensure that the repeat modify correctly
    // (create a new object when modify itself).


    rawOption = clone(rawOption, true); // FIXME
    // 如果 timeline options 或者 media 中设置了某个属性，而baseOption中没有设置，则进行警告。

    var oldOptionBackup = this._optionBackup;
    var newParsedOption = parseRawOption.call(this, rawOption, optionPreprocessorFuncs, !oldOptionBackup);
    this._newBaseOption = newParsedOption.baseOption; // For setOption at second time (using merge mode);

    if (oldOptionBackup) {
      // Only baseOption can be merged.
      mergeOption(oldOptionBackup.baseOption, newParsedOption.baseOption); // For simplicity, timeline options and media options do not support merge,
      // that is, if you `setOption` twice and both has timeline options, the latter
      // timeline opitons will not be merged to the formers, but just substitude them.

      if (newParsedOption.timelineOptions.length) {
        oldOptionBackup.timelineOptions = newParsedOption.timelineOptions;
      }

      if (newParsedOption.mediaList.length) {
        oldOptionBackup.mediaList = newParsedOption.mediaList;
      }

      if (newParsedOption.mediaDefault) {
        oldOptionBackup.mediaDefault = newParsedOption.mediaDefault;
      }
    } else {
      this._optionBackup = newParsedOption;
    }
  },

  /**
   * @param {boolean} isRecreate
   * @return {Object}
   */
  mountOption: function mountOption(isRecreate) {
    var optionBackup = this._optionBackup; // TODO
    // 如果没有reset功能则不clone。

    this._timelineOptions = map(optionBackup.timelineOptions, clone);
    this._mediaList = map(optionBackup.mediaList, clone);
    this._mediaDefault = clone(optionBackup.mediaDefault);
    this._currentMediaIndices = [];
    return clone(isRecreate // this._optionBackup.baseOption, which is created at the first `setOption`
    // called, and is merged into every new option by inner method `mergeOption`
    // each time `setOption` called, can be only used in `isRecreate`, because
    // its reliability is under suspicion. In other cases option merge is
    // performed by `model.mergeOption`.
    ? optionBackup.baseOption : this._newBaseOption);
  },

  /**
   * @param {module:echarts/model/Global} ecModel
   * @return {Object}
   */
  getTimelineOption: function getTimelineOption(ecModel) {
    var option;
    var timelineOptions = this._timelineOptions;

    if (timelineOptions.length) {
      // getTimelineOption can only be called after ecModel inited,
      // so we can get currentIndex from timelineModel.
      var timelineModel = ecModel.getComponent('timeline');

      if (timelineModel) {
        option = clone(timelineOptions[timelineModel.getCurrentIndex()], true);
      }
    }

    return option;
  },

  /**
   * @param {module:echarts/model/Global} ecModel
   * @return {Array.<Object>}
   */
  getMediaOption: function getMediaOption(ecModel) {
    var ecWidth = this._api.getWidth();

    var ecHeight = this._api.getHeight();

    var mediaList = this._mediaList;
    var mediaDefault = this._mediaDefault;
    var indices = [];
    var result = []; // No media defined.

    if (!mediaList.length && !mediaDefault) {
      return result;
    } // Multi media may be applied, the latter defined media has higher priority.


    for (var i = 0, len = mediaList.length; i < len; i++) {
      if (applyMediaQuery(mediaList[i].query, ecWidth, ecHeight)) {
        indices.push(i);
      }
    } // FIXME
    // 是否mediaDefault应该强制用户设置，否则可能修改不能回归。


    if (!indices.length && mediaDefault) {
      indices = [-1];
    }

    if (indices.length && !indicesEquals(indices, this._currentMediaIndices)) {
      result = map(indices, function (index) {
        return clone(index === -1 ? mediaDefault.option : mediaList[index].option);
      });
    } // Otherwise return nothing.


    this._currentMediaIndices = indices;
    return result;
  }
};

function parseRawOption(rawOption, optionPreprocessorFuncs, isNew) {
  var timelineOptions = [];
  var mediaList = [];
  var mediaDefault;
  var baseOption; // Compatible with ec2.

  var timelineOpt = rawOption.timeline;

  if (rawOption.baseOption) {
    baseOption = rawOption.baseOption;
  } // For timeline


  if (timelineOpt || rawOption.options) {
    baseOption = baseOption || {};
    timelineOptions = (rawOption.options || []).slice();
  } // For media query


  if (rawOption.media) {
    baseOption = baseOption || {};
    var media = rawOption.media;
    each(media, function (singleMedia) {
      if (singleMedia && singleMedia.option) {
        if (singleMedia.query) {
          mediaList.push(singleMedia);
        } else if (!mediaDefault) {
          // Use the first media default.
          mediaDefault = singleMedia;
        }
      }
    });
  } // For normal option


  if (!baseOption) {
    baseOption = rawOption;
  } // Set timelineOpt to baseOption in ec3,
  // which is convenient for merge option.


  if (!baseOption.timeline) {
    baseOption.timeline = timelineOpt;
  } // Preprocess.


  each([baseOption].concat(timelineOptions).concat(zrUtil.map(mediaList, function (media) {
    return media.option;
  })), function (option) {
    each(optionPreprocessorFuncs, function (preProcess) {
      preProcess(option, isNew);
    });
  });
  return {
    baseOption: baseOption,
    timelineOptions: timelineOptions,
    mediaDefault: mediaDefault,
    mediaList: mediaList
  };
}
/**
 * @see <http://www.w3.org/TR/css3-mediaqueries/#media1>
 * Support: width, height, aspectRatio
 * Can use max or min as prefix.
 */


function applyMediaQuery(query, ecWidth, ecHeight) {
  var realMap = {
    width: ecWidth,
    height: ecHeight,
    aspectratio: ecWidth / ecHeight // lowser case for convenientce.

  };
  var applicatable = true;
  zrUtil.each(query, function (value, attr) {
    var matched = attr.match(QUERY_REG);

    if (!matched || !matched[1] || !matched[2]) {
      return;
    }

    var operator = matched[1];
    var realAttr = matched[2].toLowerCase();

    if (!compare(realMap[realAttr], value, operator)) {
      applicatable = false;
    }
  });
  return applicatable;
}

function compare(real, expect, operator) {
  if (operator === 'min') {
    return real >= expect;
  } else if (operator === 'max') {
    return real <= expect;
  } else {
    // Equals
    return real === expect;
  }
}

function indicesEquals(indices1, indices2) {
  // indices is always order by asc and has only finite number.
  return indices1.join(',') === indices2.join(',');
}
/**
 * Consider case:
 * `chart.setOption(opt1);`
 * Then user do some interaction like dataZoom, dataView changing.
 * `chart.setOption(opt2);`
 * Then user press 'reset button' in toolbox.
 *
 * After doing that all of the interaction effects should be reset, the
 * chart should be the same as the result of invoke
 * `chart.setOption(opt1); chart.setOption(opt2);`.
 *
 * Although it is not able ensure that
 * `chart.setOption(opt1); chart.setOption(opt2);` is equivalents to
 * `chart.setOption(merge(opt1, opt2));` exactly,
 * this might be the only simple way to implement that feature.
 *
 * MEMO: We've considered some other approaches:
 * 1. Each model handle its self restoration but not uniform treatment.
 *     (Too complex in logic and error-prone)
 * 2. Use a shadow ecModel. (Performace expensive)
 */


function mergeOption(oldOption, newOption) {
  newOption = newOption || {};
  each(newOption, function (newCptOpt, mainType) {
    if (newCptOpt == null) {
      return;
    }

    var oldCptOpt = oldOption[mainType];

    if (!ComponentModel.hasClass(mainType)) {
      oldOption[mainType] = merge(oldCptOpt, newCptOpt, true);
    } else {
      newCptOpt = modelUtil.normalizeToArray(newCptOpt);
      oldCptOpt = modelUtil.normalizeToArray(oldCptOpt);
      var mapResult = modelUtil.mappingToExists(oldCptOpt, newCptOpt);
      oldOption[mainType] = map(mapResult, function (item) {
        return item.option && item.exist ? merge(item.exist, item.option, true) : item.exist || item.option;
      });
    }
  });
}

var _default = OptionManager;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvT3B0aW9uTWFuYWdlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL21vZGVsL09wdGlvbk1hbmFnZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgbW9kZWxVdGlsID0gcmVxdWlyZShcIi4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBDb21wb25lbnRNb2RlbCA9IHJlcXVpcmUoXCIuL0NvbXBvbmVudFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEVDaGFydHMgb3B0aW9uIG1hbmFnZXJcbiAqXG4gKiBAbW9kdWxlIHtlY2hhcnRzL21vZGVsL09wdGlvbk1hbmFnZXJ9XG4gKi9cbnZhciBlYWNoID0genJVdGlsLmVhY2g7XG52YXIgY2xvbmUgPSB6clV0aWwuY2xvbmU7XG52YXIgbWFwID0genJVdGlsLm1hcDtcbnZhciBtZXJnZSA9IHpyVXRpbC5tZXJnZTtcbnZhciBRVUVSWV9SRUcgPSAvXihtaW58bWF4KT8oLispJC87XG4vKipcbiAqIFRFUk0gRVhQTEFOQVRJT05TOlxuICpcbiAqIFtvcHRpb25dOlxuICpcbiAqICAgICBBbiBvYmplY3QgdGhhdCBjb250YWlucyBkZWZpbml0aW9ucyBvZiBjb21wb25lbnRzLiBGb3IgZXhhbXBsZTpcbiAqICAgICB2YXIgb3B0aW9uID0ge1xuICogICAgICAgICB0aXRsZTogey4uLn0sXG4gKiAgICAgICAgIGxlZ2VuZDogey4uLn0sXG4gKiAgICAgICAgIHZpc3VhbE1hcDogey4uLn0sXG4gKiAgICAgICAgIHNlcmllczogW1xuICogICAgICAgICAgICAge2RhdGE6IFsuLi5dfSxcbiAqICAgICAgICAgICAgIHtkYXRhOiBbLi4uXX0sXG4gKiAgICAgICAgICAgICAuLi5cbiAqICAgICAgICAgXVxuICogICAgIH07XG4gKlxuICogW3Jhd09wdGlvbl06XG4gKlxuICogICAgIEFuIG9iamVjdCBpbnB1dCB0byBlY2hhcnRzLnNldE9wdGlvbi4gJ3Jhd09wdGlvbicgbWF5IGJlIGFuXG4gKiAgICAgJ29wdGlvbicsIG9yIG1heSBiZSBhbiBvYmplY3QgY29udGFpbnMgbXVsdGktb3B0aW9ucy4gRm9yIGV4YW1wbGU6XG4gKiAgICAgdmFyIG9wdGlvbiA9IHtcbiAqICAgICAgICAgYmFzZU9wdGlvbjoge1xuICogICAgICAgICAgICAgdGl0bGU6IHsuLi59LFxuICogICAgICAgICAgICAgbGVnZW5kOiB7Li4ufSxcbiAqICAgICAgICAgICAgIHNlcmllczogW1xuICogICAgICAgICAgICAgICAgIHtkYXRhOiBbLi4uXX0sXG4gKiAgICAgICAgICAgICAgICAge2RhdGE6IFsuLi5dfSxcbiAqICAgICAgICAgICAgICAgICAuLi5cbiAqICAgICAgICAgICAgIF1cbiAqICAgICAgICAgfSxcbiAqICAgICAgICAgdGltZWxpbmU6IHsuLi59LFxuICogICAgICAgICBvcHRpb25zOiBbXG4gKiAgICAgICAgICAgICB7dGl0bGU6IHsuLi59LCBzZXJpZXM6IHtkYXRhOiBbLi4uXX19LFxuICogICAgICAgICAgICAge3RpdGxlOiB7Li4ufSwgc2VyaWVzOiB7ZGF0YTogWy4uLl19fSxcbiAqICAgICAgICAgICAgIC4uLlxuICogICAgICAgICBdLFxuICogICAgICAgICBtZWRpYTogW1xuICogICAgICAgICAgICAge1xuICogICAgICAgICAgICAgICAgIHF1ZXJ5OiB7bWF4V2lkdGg6IDMyMH0sXG4gKiAgICAgICAgICAgICAgICAgb3B0aW9uOiB7c2VyaWVzOiB7eDogMjB9LCB2aXN1YWxNYXA6IHtzaG93OiBmYWxzZX19XG4gKiAgICAgICAgICAgICB9LFxuICogICAgICAgICAgICAge1xuICogICAgICAgICAgICAgICAgIHF1ZXJ5OiB7bWluV2lkdGg6IDMyMCwgbWF4V2lkdGg6IDcyMH0sXG4gKiAgICAgICAgICAgICAgICAgb3B0aW9uOiB7c2VyaWVzOiB7eDogNTAwfSwgdmlzdWFsTWFwOiB7c2hvdzogdHJ1ZX19XG4gKiAgICAgICAgICAgICB9LFxuICogICAgICAgICAgICAge1xuICogICAgICAgICAgICAgICAgIG9wdGlvbjoge3Nlcmllczoge3g6IDEyMDB9LCB2aXN1YWxNYXA6IHtzaG93OiB0cnVlfX1cbiAqICAgICAgICAgICAgIH1cbiAqICAgICAgICAgXVxuICogICAgIH07XG4gKlxuICogQGFsaWFzIG1vZHVsZTplY2hhcnRzL21vZGVsL09wdGlvbk1hbmFnZXJcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvRXh0ZW5zaW9uQVBJfSBhcGlcbiAqL1xuXG5mdW5jdGlvbiBPcHRpb25NYW5hZ2VyKGFwaSkge1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX1cbiAgICovXG4gIHRoaXMuX2FwaSA9IGFwaTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG5cbiAgdGhpcy5fdGltZWxpbmVPcHRpb25zID0gW107XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAdHlwZSB7QXJyYXkuPE9iamVjdD59XG4gICAqL1xuXG4gIHRoaXMuX21lZGlhTGlzdCA9IFtdO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG5cbiAgdGhpcy5fbWVkaWFEZWZhdWx0O1xuICAvKipcbiAgICogLTEsIG1lYW5zIGRlZmF1bHQuXG4gICAqIGVtcHR5IG1lYW5zIG5vIG1lZGlhLlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAdHlwZSB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuXG4gIHRoaXMuX2N1cnJlbnRNZWRpYUluZGljZXMgPSBbXTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuXG4gIHRoaXMuX29wdGlvbkJhY2t1cDtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuXG4gIHRoaXMuX25ld0Jhc2VPcHRpb247XG59IC8vIHRpbWVsaW5lLm5vdE1lcmdlIGlzIG5vdCBzdXBwb3J0ZWQgaW4gZWMzLiBGaXJzdGx5IHRoZXJlIGlzIHJlYXJseVxuLy8gY2FzZSB0aGF0IG5vdE1lcmdlIGlzIG5lZWRlZC4gU2Vjb25kbHkgc3VwcG9ydGluZyAnbm90TWVyZ2UnIHJlcXVpcmVzXG4vLyByYXdPcHRpb24gY2xvbmVkIGFuZCBiYWNrdXBlZCB3aGVuIHRpbWVsaW5lIGNoYW5nZWQsIHdoaWNoIGRvZXMgbm9cbi8vIGdvb2QgdG8gcGVyZm9ybWFuY2UuIFdoYXQncyBtb3JlLCB0aGF0IGJvdGggdGltZWxpbmUgYW5kIHNldE9wdGlvblxuLy8gbWV0aG9kIHN1cHBseSAnbm90TWVyZ2UnIGJyaW5ncyBjb21wbGV4IGFuZCBzb21lIHByb2JsZW1zLlxuLy8gQ29uc2lkZXIgdGhpcyBjYXNlOlxuLy8gKHN0ZXAxKSBjaGFydC5zZXRPcHRpb24oe3RpbWVsaW5lOiB7bm90TWVyZ2U6IGZhbHNlfSwgLi4ufSwgZmFsc2UpO1xuLy8gKHN0ZXAyKSBjaGFydC5zZXRPcHRpb24oe3RpbWVsaW5lOiB7bm90TWVyZ2U6IHRydWV9LCAuLi59LCBmYWxzZSk7XG5cblxuT3B0aW9uTWFuYWdlci5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBPcHRpb25NYW5hZ2VyLFxuXG4gIC8qKlxuICAgKiBAcHVibGljXG4gICAqIEBwYXJhbSB7T2JqZWN0fSByYXdPcHRpb24gUmF3IG9wdGlvbi5cbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAgICogQHBhcmFtIHtBcnJheS48RnVuY3Rpb24+fSBvcHRpb25QcmVwcm9jZXNzb3JGdW5jc1xuICAgKiBAcmV0dXJuIHtPYmplY3R9IEluaXQgb3B0aW9uXG4gICAqL1xuICBzZXRPcHRpb246IGZ1bmN0aW9uIChyYXdPcHRpb24sIG9wdGlvblByZXByb2Nlc3NvckZ1bmNzKSB7XG4gICAgaWYgKHJhd09wdGlvbikge1xuICAgICAgLy8gVGhhdCBzZXQgZGF0IHByaW1pdGl2ZSBpcyBkYW5nZXJvdXMgaWYgdXNlciByZXVzZSB0aGUgZGF0YSB3aGVuIHNldE9wdGlvbiBhZ2Fpbi5cbiAgICAgIHpyVXRpbC5lYWNoKG1vZGVsVXRpbC5ub3JtYWxpemVUb0FycmF5KHJhd09wdGlvbi5zZXJpZXMpLCBmdW5jdGlvbiAoc2VyaWVzKSB7XG4gICAgICAgIHNlcmllcyAmJiBzZXJpZXMuZGF0YSAmJiB6clV0aWwuaXNUeXBlZEFycmF5KHNlcmllcy5kYXRhKSAmJiB6clV0aWwuc2V0QXNQcmltaXRpdmUoc2VyaWVzLmRhdGEpO1xuICAgICAgfSk7XG4gICAgfSAvLyBDYXV0aW9uOiBzb21lIHNlcmllcyBtb2RpZnkgb3B0aW9uIGRhdGEsIGlmIGRvIG5vdCBjbG9uZSxcbiAgICAvLyBpdCBzaG91bGQgZW5zdXJlIHRoYXQgdGhlIHJlcGVhdCBtb2RpZnkgY29ycmVjdGx5XG4gICAgLy8gKGNyZWF0ZSBhIG5ldyBvYmplY3Qgd2hlbiBtb2RpZnkgaXRzZWxmKS5cblxuXG4gICAgcmF3T3B0aW9uID0gY2xvbmUocmF3T3B0aW9uLCB0cnVlKTsgLy8gRklYTUVcbiAgICAvLyDlpoLmnpwgdGltZWxpbmUgb3B0aW9ucyDmiJbogIUgbWVkaWEg5Lit6K6+572u5LqG5p+Q5Liq5bGe5oCn77yM6ICMYmFzZU9wdGlvbuS4reayoeacieiuvue9ru+8jOWImei/m+ihjOitpuWRiuOAglxuXG4gICAgdmFyIG9sZE9wdGlvbkJhY2t1cCA9IHRoaXMuX29wdGlvbkJhY2t1cDtcbiAgICB2YXIgbmV3UGFyc2VkT3B0aW9uID0gcGFyc2VSYXdPcHRpb24uY2FsbCh0aGlzLCByYXdPcHRpb24sIG9wdGlvblByZXByb2Nlc3NvckZ1bmNzLCAhb2xkT3B0aW9uQmFja3VwKTtcbiAgICB0aGlzLl9uZXdCYXNlT3B0aW9uID0gbmV3UGFyc2VkT3B0aW9uLmJhc2VPcHRpb247IC8vIEZvciBzZXRPcHRpb24gYXQgc2Vjb25kIHRpbWUgKHVzaW5nIG1lcmdlIG1vZGUpO1xuXG4gICAgaWYgKG9sZE9wdGlvbkJhY2t1cCkge1xuICAgICAgLy8gT25seSBiYXNlT3B0aW9uIGNhbiBiZSBtZXJnZWQuXG4gICAgICBtZXJnZU9wdGlvbihvbGRPcHRpb25CYWNrdXAuYmFzZU9wdGlvbiwgbmV3UGFyc2VkT3B0aW9uLmJhc2VPcHRpb24pOyAvLyBGb3Igc2ltcGxpY2l0eSwgdGltZWxpbmUgb3B0aW9ucyBhbmQgbWVkaWEgb3B0aW9ucyBkbyBub3Qgc3VwcG9ydCBtZXJnZSxcbiAgICAgIC8vIHRoYXQgaXMsIGlmIHlvdSBgc2V0T3B0aW9uYCB0d2ljZSBhbmQgYm90aCBoYXMgdGltZWxpbmUgb3B0aW9ucywgdGhlIGxhdHRlclxuICAgICAgLy8gdGltZWxpbmUgb3BpdG9ucyB3aWxsIG5vdCBiZSBtZXJnZWQgdG8gdGhlIGZvcm1lcnMsIGJ1dCBqdXN0IHN1YnN0aXR1ZGUgdGhlbS5cblxuICAgICAgaWYgKG5ld1BhcnNlZE9wdGlvbi50aW1lbGluZU9wdGlvbnMubGVuZ3RoKSB7XG4gICAgICAgIG9sZE9wdGlvbkJhY2t1cC50aW1lbGluZU9wdGlvbnMgPSBuZXdQYXJzZWRPcHRpb24udGltZWxpbmVPcHRpb25zO1xuICAgICAgfVxuXG4gICAgICBpZiAobmV3UGFyc2VkT3B0aW9uLm1lZGlhTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgb2xkT3B0aW9uQmFja3VwLm1lZGlhTGlzdCA9IG5ld1BhcnNlZE9wdGlvbi5tZWRpYUxpc3Q7XG4gICAgICB9XG5cbiAgICAgIGlmIChuZXdQYXJzZWRPcHRpb24ubWVkaWFEZWZhdWx0KSB7XG4gICAgICAgIG9sZE9wdGlvbkJhY2t1cC5tZWRpYURlZmF1bHQgPSBuZXdQYXJzZWRPcHRpb24ubWVkaWFEZWZhdWx0O1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLl9vcHRpb25CYWNrdXAgPSBuZXdQYXJzZWRPcHRpb247XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGlzUmVjcmVhdGVcbiAgICogQHJldHVybiB7T2JqZWN0fVxuICAgKi9cbiAgbW91bnRPcHRpb246IGZ1bmN0aW9uIChpc1JlY3JlYXRlKSB7XG4gICAgdmFyIG9wdGlvbkJhY2t1cCA9IHRoaXMuX29wdGlvbkJhY2t1cDsgLy8gVE9ET1xuICAgIC8vIOWmguaenOayoeaciXJlc2V05Yqf6IO95YiZ5LiNY2xvbmXjgIJcblxuICAgIHRoaXMuX3RpbWVsaW5lT3B0aW9ucyA9IG1hcChvcHRpb25CYWNrdXAudGltZWxpbmVPcHRpb25zLCBjbG9uZSk7XG4gICAgdGhpcy5fbWVkaWFMaXN0ID0gbWFwKG9wdGlvbkJhY2t1cC5tZWRpYUxpc3QsIGNsb25lKTtcbiAgICB0aGlzLl9tZWRpYURlZmF1bHQgPSBjbG9uZShvcHRpb25CYWNrdXAubWVkaWFEZWZhdWx0KTtcbiAgICB0aGlzLl9jdXJyZW50TWVkaWFJbmRpY2VzID0gW107XG4gICAgcmV0dXJuIGNsb25lKGlzUmVjcmVhdGUgLy8gdGhpcy5fb3B0aW9uQmFja3VwLmJhc2VPcHRpb24sIHdoaWNoIGlzIGNyZWF0ZWQgYXQgdGhlIGZpcnN0IGBzZXRPcHRpb25gXG4gICAgLy8gY2FsbGVkLCBhbmQgaXMgbWVyZ2VkIGludG8gZXZlcnkgbmV3IG9wdGlvbiBieSBpbm5lciBtZXRob2QgYG1lcmdlT3B0aW9uYFxuICAgIC8vIGVhY2ggdGltZSBgc2V0T3B0aW9uYCBjYWxsZWQsIGNhbiBiZSBvbmx5IHVzZWQgaW4gYGlzUmVjcmVhdGVgLCBiZWNhdXNlXG4gICAgLy8gaXRzIHJlbGlhYmlsaXR5IGlzIHVuZGVyIHN1c3BpY2lvbi4gSW4gb3RoZXIgY2FzZXMgb3B0aW9uIG1lcmdlIGlzXG4gICAgLy8gcGVyZm9ybWVkIGJ5IGBtb2RlbC5tZXJnZU9wdGlvbmAuXG4gICAgPyBvcHRpb25CYWNrdXAuYmFzZU9wdGlvbiA6IHRoaXMuX25ld0Jhc2VPcHRpb24pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAqL1xuICBnZXRUaW1lbGluZU9wdGlvbjogZnVuY3Rpb24gKGVjTW9kZWwpIHtcbiAgICB2YXIgb3B0aW9uO1xuICAgIHZhciB0aW1lbGluZU9wdGlvbnMgPSB0aGlzLl90aW1lbGluZU9wdGlvbnM7XG5cbiAgICBpZiAodGltZWxpbmVPcHRpb25zLmxlbmd0aCkge1xuICAgICAgLy8gZ2V0VGltZWxpbmVPcHRpb24gY2FuIG9ubHkgYmUgY2FsbGVkIGFmdGVyIGVjTW9kZWwgaW5pdGVkLFxuICAgICAgLy8gc28gd2UgY2FuIGdldCBjdXJyZW50SW5kZXggZnJvbSB0aW1lbGluZU1vZGVsLlxuICAgICAgdmFyIHRpbWVsaW5lTW9kZWwgPSBlY01vZGVsLmdldENvbXBvbmVudCgndGltZWxpbmUnKTtcblxuICAgICAgaWYgKHRpbWVsaW5lTW9kZWwpIHtcbiAgICAgICAgb3B0aW9uID0gY2xvbmUodGltZWxpbmVPcHRpb25zW3RpbWVsaW5lTW9kZWwuZ2V0Q3VycmVudEluZGV4KCldLCB0cnVlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gb3B0aW9uO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcmV0dXJuIHtBcnJheS48T2JqZWN0Pn1cbiAgICovXG4gIGdldE1lZGlhT3B0aW9uOiBmdW5jdGlvbiAoZWNNb2RlbCkge1xuICAgIHZhciBlY1dpZHRoID0gdGhpcy5fYXBpLmdldFdpZHRoKCk7XG5cbiAgICB2YXIgZWNIZWlnaHQgPSB0aGlzLl9hcGkuZ2V0SGVpZ2h0KCk7XG5cbiAgICB2YXIgbWVkaWFMaXN0ID0gdGhpcy5fbWVkaWFMaXN0O1xuICAgIHZhciBtZWRpYURlZmF1bHQgPSB0aGlzLl9tZWRpYURlZmF1bHQ7XG4gICAgdmFyIGluZGljZXMgPSBbXTtcbiAgICB2YXIgcmVzdWx0ID0gW107IC8vIE5vIG1lZGlhIGRlZmluZWQuXG5cbiAgICBpZiAoIW1lZGlhTGlzdC5sZW5ndGggJiYgIW1lZGlhRGVmYXVsdCkge1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9IC8vIE11bHRpIG1lZGlhIG1heSBiZSBhcHBsaWVkLCB0aGUgbGF0dGVyIGRlZmluZWQgbWVkaWEgaGFzIGhpZ2hlciBwcmlvcml0eS5cblxuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IG1lZGlhTGlzdC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgaWYgKGFwcGx5TWVkaWFRdWVyeShtZWRpYUxpc3RbaV0ucXVlcnksIGVjV2lkdGgsIGVjSGVpZ2h0KSkge1xuICAgICAgICBpbmRpY2VzLnB1c2goaSk7XG4gICAgICB9XG4gICAgfSAvLyBGSVhNRVxuICAgIC8vIOaYr+WQpm1lZGlhRGVmYXVsdOW6lOivpeW8uuWItueUqOaIt+iuvue9ru+8jOWQpuWImeWPr+iDveS/ruaUueS4jeiDveWbnuW9kuOAglxuXG5cbiAgICBpZiAoIWluZGljZXMubGVuZ3RoICYmIG1lZGlhRGVmYXVsdCkge1xuICAgICAgaW5kaWNlcyA9IFstMV07XG4gICAgfVxuXG4gICAgaWYgKGluZGljZXMubGVuZ3RoICYmICFpbmRpY2VzRXF1YWxzKGluZGljZXMsIHRoaXMuX2N1cnJlbnRNZWRpYUluZGljZXMpKSB7XG4gICAgICByZXN1bHQgPSBtYXAoaW5kaWNlcywgZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICAgIHJldHVybiBjbG9uZShpbmRleCA9PT0gLTEgPyBtZWRpYURlZmF1bHQub3B0aW9uIDogbWVkaWFMaXN0W2luZGV4XS5vcHRpb24pO1xuICAgICAgfSk7XG4gICAgfSAvLyBPdGhlcndpc2UgcmV0dXJuIG5vdGhpbmcuXG5cblxuICAgIHRoaXMuX2N1cnJlbnRNZWRpYUluZGljZXMgPSBpbmRpY2VzO1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIHBhcnNlUmF3T3B0aW9uKHJhd09wdGlvbiwgb3B0aW9uUHJlcHJvY2Vzc29yRnVuY3MsIGlzTmV3KSB7XG4gIHZhciB0aW1lbGluZU9wdGlvbnMgPSBbXTtcbiAgdmFyIG1lZGlhTGlzdCA9IFtdO1xuICB2YXIgbWVkaWFEZWZhdWx0O1xuICB2YXIgYmFzZU9wdGlvbjsgLy8gQ29tcGF0aWJsZSB3aXRoIGVjMi5cblxuICB2YXIgdGltZWxpbmVPcHQgPSByYXdPcHRpb24udGltZWxpbmU7XG5cbiAgaWYgKHJhd09wdGlvbi5iYXNlT3B0aW9uKSB7XG4gICAgYmFzZU9wdGlvbiA9IHJhd09wdGlvbi5iYXNlT3B0aW9uO1xuICB9IC8vIEZvciB0aW1lbGluZVxuXG5cbiAgaWYgKHRpbWVsaW5lT3B0IHx8IHJhd09wdGlvbi5vcHRpb25zKSB7XG4gICAgYmFzZU9wdGlvbiA9IGJhc2VPcHRpb24gfHwge307XG4gICAgdGltZWxpbmVPcHRpb25zID0gKHJhd09wdGlvbi5vcHRpb25zIHx8IFtdKS5zbGljZSgpO1xuICB9IC8vIEZvciBtZWRpYSBxdWVyeVxuXG5cbiAgaWYgKHJhd09wdGlvbi5tZWRpYSkge1xuICAgIGJhc2VPcHRpb24gPSBiYXNlT3B0aW9uIHx8IHt9O1xuICAgIHZhciBtZWRpYSA9IHJhd09wdGlvbi5tZWRpYTtcbiAgICBlYWNoKG1lZGlhLCBmdW5jdGlvbiAoc2luZ2xlTWVkaWEpIHtcbiAgICAgIGlmIChzaW5nbGVNZWRpYSAmJiBzaW5nbGVNZWRpYS5vcHRpb24pIHtcbiAgICAgICAgaWYgKHNpbmdsZU1lZGlhLnF1ZXJ5KSB7XG4gICAgICAgICAgbWVkaWFMaXN0LnB1c2goc2luZ2xlTWVkaWEpO1xuICAgICAgICB9IGVsc2UgaWYgKCFtZWRpYURlZmF1bHQpIHtcbiAgICAgICAgICAvLyBVc2UgdGhlIGZpcnN0IG1lZGlhIGRlZmF1bHQuXG4gICAgICAgICAgbWVkaWFEZWZhdWx0ID0gc2luZ2xlTWVkaWE7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfSAvLyBGb3Igbm9ybWFsIG9wdGlvblxuXG5cbiAgaWYgKCFiYXNlT3B0aW9uKSB7XG4gICAgYmFzZU9wdGlvbiA9IHJhd09wdGlvbjtcbiAgfSAvLyBTZXQgdGltZWxpbmVPcHQgdG8gYmFzZU9wdGlvbiBpbiBlYzMsXG4gIC8vIHdoaWNoIGlzIGNvbnZlbmllbnQgZm9yIG1lcmdlIG9wdGlvbi5cblxuXG4gIGlmICghYmFzZU9wdGlvbi50aW1lbGluZSkge1xuICAgIGJhc2VPcHRpb24udGltZWxpbmUgPSB0aW1lbGluZU9wdDtcbiAgfSAvLyBQcmVwcm9jZXNzLlxuXG5cbiAgZWFjaChbYmFzZU9wdGlvbl0uY29uY2F0KHRpbWVsaW5lT3B0aW9ucykuY29uY2F0KHpyVXRpbC5tYXAobWVkaWFMaXN0LCBmdW5jdGlvbiAobWVkaWEpIHtcbiAgICByZXR1cm4gbWVkaWEub3B0aW9uO1xuICB9KSksIGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgICBlYWNoKG9wdGlvblByZXByb2Nlc3NvckZ1bmNzLCBmdW5jdGlvbiAocHJlUHJvY2Vzcykge1xuICAgICAgcHJlUHJvY2VzcyhvcHRpb24sIGlzTmV3KTtcbiAgICB9KTtcbiAgfSk7XG4gIHJldHVybiB7XG4gICAgYmFzZU9wdGlvbjogYmFzZU9wdGlvbixcbiAgICB0aW1lbGluZU9wdGlvbnM6IHRpbWVsaW5lT3B0aW9ucyxcbiAgICBtZWRpYURlZmF1bHQ6IG1lZGlhRGVmYXVsdCxcbiAgICBtZWRpYUxpc3Q6IG1lZGlhTGlzdFxuICB9O1xufVxuLyoqXG4gKiBAc2VlIDxodHRwOi8vd3d3LnczLm9yZy9UUi9jc3MzLW1lZGlhcXVlcmllcy8jbWVkaWExPlxuICogU3VwcG9ydDogd2lkdGgsIGhlaWdodCwgYXNwZWN0UmF0aW9cbiAqIENhbiB1c2UgbWF4IG9yIG1pbiBhcyBwcmVmaXguXG4gKi9cblxuXG5mdW5jdGlvbiBhcHBseU1lZGlhUXVlcnkocXVlcnksIGVjV2lkdGgsIGVjSGVpZ2h0KSB7XG4gIHZhciByZWFsTWFwID0ge1xuICAgIHdpZHRoOiBlY1dpZHRoLFxuICAgIGhlaWdodDogZWNIZWlnaHQsXG4gICAgYXNwZWN0cmF0aW86IGVjV2lkdGggLyBlY0hlaWdodCAvLyBsb3dzZXIgY2FzZSBmb3IgY29udmVuaWVudGNlLlxuXG4gIH07XG4gIHZhciBhcHBsaWNhdGFibGUgPSB0cnVlO1xuICB6clV0aWwuZWFjaChxdWVyeSwgZnVuY3Rpb24gKHZhbHVlLCBhdHRyKSB7XG4gICAgdmFyIG1hdGNoZWQgPSBhdHRyLm1hdGNoKFFVRVJZX1JFRyk7XG5cbiAgICBpZiAoIW1hdGNoZWQgfHwgIW1hdGNoZWRbMV0gfHwgIW1hdGNoZWRbMl0pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgb3BlcmF0b3IgPSBtYXRjaGVkWzFdO1xuICAgIHZhciByZWFsQXR0ciA9IG1hdGNoZWRbMl0udG9Mb3dlckNhc2UoKTtcblxuICAgIGlmICghY29tcGFyZShyZWFsTWFwW3JlYWxBdHRyXSwgdmFsdWUsIG9wZXJhdG9yKSkge1xuICAgICAgYXBwbGljYXRhYmxlID0gZmFsc2U7XG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIGFwcGxpY2F0YWJsZTtcbn1cblxuZnVuY3Rpb24gY29tcGFyZShyZWFsLCBleHBlY3QsIG9wZXJhdG9yKSB7XG4gIGlmIChvcGVyYXRvciA9PT0gJ21pbicpIHtcbiAgICByZXR1cm4gcmVhbCA+PSBleHBlY3Q7XG4gIH0gZWxzZSBpZiAob3BlcmF0b3IgPT09ICdtYXgnKSB7XG4gICAgcmV0dXJuIHJlYWwgPD0gZXhwZWN0O1xuICB9IGVsc2Uge1xuICAgIC8vIEVxdWFsc1xuICAgIHJldHVybiByZWFsID09PSBleHBlY3Q7XG4gIH1cbn1cblxuZnVuY3Rpb24gaW5kaWNlc0VxdWFscyhpbmRpY2VzMSwgaW5kaWNlczIpIHtcbiAgLy8gaW5kaWNlcyBpcyBhbHdheXMgb3JkZXIgYnkgYXNjIGFuZCBoYXMgb25seSBmaW5pdGUgbnVtYmVyLlxuICByZXR1cm4gaW5kaWNlczEuam9pbignLCcpID09PSBpbmRpY2VzMi5qb2luKCcsJyk7XG59XG4vKipcbiAqIENvbnNpZGVyIGNhc2U6XG4gKiBgY2hhcnQuc2V0T3B0aW9uKG9wdDEpO2BcbiAqIFRoZW4gdXNlciBkbyBzb21lIGludGVyYWN0aW9uIGxpa2UgZGF0YVpvb20sIGRhdGFWaWV3IGNoYW5naW5nLlxuICogYGNoYXJ0LnNldE9wdGlvbihvcHQyKTtgXG4gKiBUaGVuIHVzZXIgcHJlc3MgJ3Jlc2V0IGJ1dHRvbicgaW4gdG9vbGJveC5cbiAqXG4gKiBBZnRlciBkb2luZyB0aGF0IGFsbCBvZiB0aGUgaW50ZXJhY3Rpb24gZWZmZWN0cyBzaG91bGQgYmUgcmVzZXQsIHRoZVxuICogY2hhcnQgc2hvdWxkIGJlIHRoZSBzYW1lIGFzIHRoZSByZXN1bHQgb2YgaW52b2tlXG4gKiBgY2hhcnQuc2V0T3B0aW9uKG9wdDEpOyBjaGFydC5zZXRPcHRpb24ob3B0Mik7YC5cbiAqXG4gKiBBbHRob3VnaCBpdCBpcyBub3QgYWJsZSBlbnN1cmUgdGhhdFxuICogYGNoYXJ0LnNldE9wdGlvbihvcHQxKTsgY2hhcnQuc2V0T3B0aW9uKG9wdDIpO2AgaXMgZXF1aXZhbGVudHMgdG9cbiAqIGBjaGFydC5zZXRPcHRpb24obWVyZ2Uob3B0MSwgb3B0MikpO2AgZXhhY3RseSxcbiAqIHRoaXMgbWlnaHQgYmUgdGhlIG9ubHkgc2ltcGxlIHdheSB0byBpbXBsZW1lbnQgdGhhdCBmZWF0dXJlLlxuICpcbiAqIE1FTU86IFdlJ3ZlIGNvbnNpZGVyZWQgc29tZSBvdGhlciBhcHByb2FjaGVzOlxuICogMS4gRWFjaCBtb2RlbCBoYW5kbGUgaXRzIHNlbGYgcmVzdG9yYXRpb24gYnV0IG5vdCB1bmlmb3JtIHRyZWF0bWVudC5cbiAqICAgICAoVG9vIGNvbXBsZXggaW4gbG9naWMgYW5kIGVycm9yLXByb25lKVxuICogMi4gVXNlIGEgc2hhZG93IGVjTW9kZWwuIChQZXJmb3JtYWNlIGV4cGVuc2l2ZSlcbiAqL1xuXG5cbmZ1bmN0aW9uIG1lcmdlT3B0aW9uKG9sZE9wdGlvbiwgbmV3T3B0aW9uKSB7XG4gIG5ld09wdGlvbiA9IG5ld09wdGlvbiB8fCB7fTtcbiAgZWFjaChuZXdPcHRpb24sIGZ1bmN0aW9uIChuZXdDcHRPcHQsIG1haW5UeXBlKSB7XG4gICAgaWYgKG5ld0NwdE9wdCA9PSBudWxsKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIG9sZENwdE9wdCA9IG9sZE9wdGlvblttYWluVHlwZV07XG5cbiAgICBpZiAoIUNvbXBvbmVudE1vZGVsLmhhc0NsYXNzKG1haW5UeXBlKSkge1xuICAgICAgb2xkT3B0aW9uW21haW5UeXBlXSA9IG1lcmdlKG9sZENwdE9wdCwgbmV3Q3B0T3B0LCB0cnVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmV3Q3B0T3B0ID0gbW9kZWxVdGlsLm5vcm1hbGl6ZVRvQXJyYXkobmV3Q3B0T3B0KTtcbiAgICAgIG9sZENwdE9wdCA9IG1vZGVsVXRpbC5ub3JtYWxpemVUb0FycmF5KG9sZENwdE9wdCk7XG4gICAgICB2YXIgbWFwUmVzdWx0ID0gbW9kZWxVdGlsLm1hcHBpbmdUb0V4aXN0cyhvbGRDcHRPcHQsIG5ld0NwdE9wdCk7XG4gICAgICBvbGRPcHRpb25bbWFpblR5cGVdID0gbWFwKG1hcFJlc3VsdCwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgcmV0dXJuIGl0ZW0ub3B0aW9uICYmIGl0ZW0uZXhpc3QgPyBtZXJnZShpdGVtLmV4aXN0LCBpdGVtLm9wdGlvbiwgdHJ1ZSkgOiBpdGVtLmV4aXN0IHx8IGl0ZW0ub3B0aW9uO1xuICAgICAgfSk7XG4gICAgfVxuICB9KTtcbn1cblxudmFyIF9kZWZhdWx0ID0gT3B0aW9uTWFuYWdlcjtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3REE7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5JQTtBQUNBO0FBcUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/OptionManager.js
