__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _standard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./standard */ "./src/styles/themes/standard.tsx");
/* harmony import */ var _antd_base_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./antd/base.json */ "./src/styles/themes/antd/base.json");
var _antd_base_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./antd/base.json */ "./src/styles/themes/antd/base.json", 1);
/* harmony import */ var _antd_china_red_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./antd/china-red.json */ "./src/styles/themes/antd/china-red.json");
var _antd_china_red_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./antd/china-red.json */ "./src/styles/themes/antd/china-red.json", 1);





var antdConfig = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _antd_base_json__WEBPACK_IMPORTED_MODULE_2__, {}, _antd_china_red_json__WEBPACK_IMPORTED_MODULE_3__);

/* harmony default export */ __webpack_exports__["default"] = (Object.assign({}, _standard__WEBPACK_IMPORTED_MODULE_1__["default"], {
  COLOR_PRIMARY: antdConfig["primary-color"],
  BACKGROUND_COLOR: antdConfig["primary-color"],
  BORDER_COLOR: antdConfig["primary-color"],
  HEADER_COLOR: antdConfig["primary-color"],
  HIGHLIGHT_COLOR: antdConfig["primary-color"],
  HREF_A: antdConfig["link-color"],
  COLOR_BACKGROUND_BTN: antdConfig["primary-color"],
  COLOR_A: antdConfig["primary-color"],
  UPLOAD_BOARD_COLOR: antdConfig["primary-color"]
}));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc3R5bGVzL3RoZW1lcy9jaGluYS1yZWQudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvc3R5bGVzL3RoZW1lcy9jaGluYS1yZWQudHN4Il0sInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IHsgU3R5bGVkUHJvcHNUaGVtZSB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCBwYXJlbnQgZnJvbSBcIi4vc3RhbmRhcmRcIjtcbmltcG9ydCBBbnRkQmFzZUNvbmZpZyBmcm9tIFwiLi9hbnRkL2Jhc2UuanNvblwiO1xuaW1wb3J0IEFudGRUaGVtZUNvbmZpZyBmcm9tIFwiLi9hbnRkL2NoaW5hLXJlZC5qc29uXCI7XG5cbmNvbnN0IGFudGRDb25maWcgPSB7XG4gIC4uLkFudGRCYXNlQ29uZmlnLFxuICAuLi5BbnRkVGhlbWVDb25maWcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBPYmplY3QuYXNzaWduKHt9LCBwYXJlbnQsIHtcbiAgQ09MT1JfUFJJTUFSWTogYW50ZENvbmZpZ1tcInByaW1hcnktY29sb3JcIl0sXG4gIEJBQ0tHUk9VTkRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBCT1JERVJfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBIRUFERVJfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBISUdITElHSFRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBIUkVGX0E6IGFudGRDb25maWdbXCJsaW5rLWNvbG9yXCJdLFxuICBDT0xPUl9CQUNLR1JPVU5EX0JUTjogYW50ZENvbmZpZ1tcInByaW1hcnktY29sb3JcIl0sXG4gIENPTE9SX0E6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBVUExPQURfQk9BUkRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxufSkgYXMgU3R5bGVkUHJvcHNUaGVtZTsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/styles/themes/china-red.tsx
