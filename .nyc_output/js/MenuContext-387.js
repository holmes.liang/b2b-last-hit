__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js");
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__);

var MenuContext = _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default()({
  inlineCollapsed: false
});
/* harmony default export */ __webpack_exports__["default"] = (MenuContext);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZW51L01lbnVDb250ZXh0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9tZW51L01lbnVDb250ZXh0LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY3JlYXRlQ29udGV4dCBmcm9tICdAYW50LWRlc2lnbi9jcmVhdGUtcmVhY3QtY29udGV4dCc7XG5jb25zdCBNZW51Q29udGV4dCA9IGNyZWF0ZUNvbnRleHQoe1xuICAgIGlubGluZUNvbGxhcHNlZDogZmFsc2UsXG59KTtcbmV4cG9ydCBkZWZhdWx0IE1lbnVDb250ZXh0O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/menu/MenuContext.js
