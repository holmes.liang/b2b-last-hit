__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js");
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__);

var FormContext = _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default()({
  labelAlign: 'right',
  vertical: false
});
/* harmony default export */ __webpack_exports__["default"] = (FormContext);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9mb3JtL2NvbnRleHQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2Zvcm0vY29udGV4dC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgY3JlYXRlUmVhY3RDb250ZXh0IGZyb20gJ0BhbnQtZGVzaWduL2NyZWF0ZS1yZWFjdC1jb250ZXh0JztcbmNvbnN0IEZvcm1Db250ZXh0ID0gY3JlYXRlUmVhY3RDb250ZXh0KHtcbiAgICBsYWJlbEFsaWduOiAncmlnaHQnLFxuICAgIHZlcnRpY2FsOiBmYWxzZSxcbn0pO1xuZXhwb3J0IGRlZmF1bHQgRm9ybUNvbnRleHQ7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/form/context.js
