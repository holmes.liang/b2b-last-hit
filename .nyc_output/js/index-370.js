__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Alert; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_getDataOrAriaProps__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/getDataOrAriaProps */ "./node_modules/antd/es/_util/getDataOrAriaProps.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}










function noop() {}

var Alert =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Alert, _React$Component);

  function Alert(props) {
    var _this;

    _classCallCheck(this, Alert);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Alert).call(this, props));

    _this.handleClose = function (e) {
      e.preventDefault();
      var dom = react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"](_assertThisInitialized(_this));
      dom.style.height = "".concat(dom.offsetHeight, "px"); // Magic code
      // 重复一次后才能正确设置 height

      dom.style.height = "".concat(dom.offsetHeight, "px");

      _this.setState({
        closing: true
      });

      (_this.props.onClose || noop)(e);
    };

    _this.animationEnd = function () {
      _this.setState({
        closing: false,
        closed: true
      });

      (_this.props.afterClose || noop)();
    };

    _this.renderAlert = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          description = _this$props.description,
          customizePrefixCls = _this$props.prefixCls,
          message = _this$props.message,
          closeText = _this$props.closeText,
          banner = _this$props.banner,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          style = _this$props.style,
          icon = _this$props.icon;
      var _this$props2 = _this.props,
          closable = _this$props2.closable,
          type = _this$props2.type,
          showIcon = _this$props2.showIcon,
          iconType = _this$props2.iconType;
      var _this$state = _this.state,
          closing = _this$state.closing,
          closed = _this$state.closed;
      var prefixCls = getPrefixCls('alert', customizePrefixCls); // banner模式默认有 Icon

      showIcon = banner && showIcon === undefined ? true : showIcon; // banner模式默认为警告

      type = banner && type === undefined ? 'warning' : type || 'info';
      var iconTheme = 'filled';

      if (!iconType) {
        switch (type) {
          case 'success':
            iconType = 'check-circle';
            break;

          case 'info':
            iconType = 'info-circle';
            break;

          case 'error':
            iconType = 'close-circle';
            break;

          case 'warning':
            iconType = 'exclamation-circle';
            break;

          default:
            iconType = 'default';
        } // use outline icon in alert with description


        if (description) {
          iconTheme = 'outlined';
        }
      } // closeable when closeText is assigned


      if (closeText) {
        closable = true;
      }

      var alertCls = classnames__WEBPACK_IMPORTED_MODULE_3___default()(prefixCls, "".concat(prefixCls, "-").concat(type), (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-closing"), closing), _defineProperty(_classNames, "".concat(prefixCls, "-with-description"), !!description), _defineProperty(_classNames, "".concat(prefixCls, "-no-icon"), !showIcon), _defineProperty(_classNames, "".concat(prefixCls, "-banner"), !!banner), _defineProperty(_classNames, "".concat(prefixCls, "-closable"), closable), _classNames), className);
      var closeIcon = closable ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", {
        type: "button",
        onClick: _this.handleClose,
        className: "".concat(prefixCls, "-close-icon"),
        tabIndex: 0
      }, closeText ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-close-text")
      }, closeText) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "close"
      })) : null;
      var dataOrAriaProps = Object(_util_getDataOrAriaProps__WEBPACK_IMPORTED_MODULE_6__["default"])(_this.props);
      var iconNode = icon && (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](icon) ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](icon, {
        className: classnames__WEBPACK_IMPORTED_MODULE_3___default()("".concat(prefixCls, "-icon"), _defineProperty({}, icon.props.className, icon.props.className))
      }) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-icon")
      }, icon)) || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        className: "".concat(prefixCls, "-icon"),
        type: iconType,
        theme: iconTheme
      });
      return closed ? null : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate__WEBPACK_IMPORTED_MODULE_2__["default"], {
        component: "",
        showProp: "data-show",
        transitionName: "".concat(prefixCls, "-slide-up"),
        onEnd: _this.animationEnd
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({
        "data-show": !closing,
        className: alertCls,
        style: style
      }, dataOrAriaProps), showIcon ? iconNode : null, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-message")
      }, message), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-description")
      }, description), closeIcon));
    };

    Object(_util_warning__WEBPACK_IMPORTED_MODULE_7__["default"])(!('iconType' in props), 'Alert', '`iconType` is deprecated. Please use `icon` instead.');
    _this.state = {
      closing: false,
      closed: false
    };
    return _this;
  }

  _createClass(Alert, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderAlert);
    }
  }]);

  return Alert;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hbGVydC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvYWxlcnQvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgQW5pbWF0ZSBmcm9tICdyYy1hbmltYXRlJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgZ2V0RGF0YU9yQXJpYVByb3BzIGZyb20gJy4uL191dGlsL2dldERhdGFPckFyaWFQcm9wcyc7XG5pbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbmZ1bmN0aW9uIG5vb3AoKSB7IH1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFsZXJ0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuaGFuZGxlQ2xvc2UgPSAoZSkgPT4ge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgY29uc3QgZG9tID0gUmVhY3RET00uZmluZERPTU5vZGUodGhpcyk7XG4gICAgICAgICAgICBkb20uc3R5bGUuaGVpZ2h0ID0gYCR7ZG9tLm9mZnNldEhlaWdodH1weGA7XG4gICAgICAgICAgICAvLyBNYWdpYyBjb2RlXG4gICAgICAgICAgICAvLyDph43lpI3kuIDmrKHlkI7miY3og73mraPnoa7orr7nva4gaGVpZ2h0XG4gICAgICAgICAgICBkb20uc3R5bGUuaGVpZ2h0ID0gYCR7ZG9tLm9mZnNldEhlaWdodH1weGA7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBjbG9zaW5nOiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAodGhpcy5wcm9wcy5vbkNsb3NlIHx8IG5vb3ApKGUpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmFuaW1hdGlvbkVuZCA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGNsb3Npbmc6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGNsb3NlZDogdHJ1ZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgKHRoaXMucHJvcHMuYWZ0ZXJDbG9zZSB8fCBub29wKSgpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckFsZXJ0ID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgZGVzY3JpcHRpb24sIHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBtZXNzYWdlLCBjbG9zZVRleHQsIGJhbm5lciwgY2xhc3NOYW1lID0gJycsIHN0eWxlLCBpY29uLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGxldCB7IGNsb3NhYmxlLCB0eXBlLCBzaG93SWNvbiwgaWNvblR5cGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCB7IGNsb3NpbmcsIGNsb3NlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnYWxlcnQnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgLy8gYmFubmVy5qih5byP6buY6K6k5pyJIEljb25cbiAgICAgICAgICAgIHNob3dJY29uID0gYmFubmVyICYmIHNob3dJY29uID09PSB1bmRlZmluZWQgPyB0cnVlIDogc2hvd0ljb247XG4gICAgICAgICAgICAvLyBiYW5uZXLmqKHlvI/pu5jorqTkuLrorablkYpcbiAgICAgICAgICAgIHR5cGUgPSBiYW5uZXIgJiYgdHlwZSA9PT0gdW5kZWZpbmVkID8gJ3dhcm5pbmcnIDogdHlwZSB8fCAnaW5mbyc7XG4gICAgICAgICAgICBsZXQgaWNvblRoZW1lID0gJ2ZpbGxlZCc7XG4gICAgICAgICAgICBpZiAoIWljb25UeXBlKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3N1Y2Nlc3MnOlxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnY2hlY2stY2lyY2xlJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdpbmZvJzpcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb25UeXBlID0gJ2luZm8tY2lyY2xlJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdlcnJvcic6XG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uVHlwZSA9ICdjbG9zZS1jaXJjbGUnO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnZXhjbGFtYXRpb24tY2lyY2xlJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgaWNvblR5cGUgPSAnZGVmYXVsdCc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIHVzZSBvdXRsaW5lIGljb24gaW4gYWxlcnQgd2l0aCBkZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgIGlmIChkZXNjcmlwdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBpY29uVGhlbWUgPSAnb3V0bGluZWQnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIGNsb3NlYWJsZSB3aGVuIGNsb3NlVGV4dCBpcyBhc3NpZ25lZFxuICAgICAgICAgICAgaWYgKGNsb3NlVGV4dCkge1xuICAgICAgICAgICAgICAgIGNsb3NhYmxlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0Q2xzID0gY2xhc3NOYW1lcyhwcmVmaXhDbHMsIGAke3ByZWZpeENsc30tJHt0eXBlfWAsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jbG9zaW5nYF06IGNsb3NpbmcsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30td2l0aC1kZXNjcmlwdGlvbmBdOiAhIWRlc2NyaXB0aW9uLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LW5vLWljb25gXTogIXNob3dJY29uLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWJhbm5lcmBdOiAhIWJhbm5lcixcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jbG9zYWJsZWBdOiBjbG9zYWJsZSxcbiAgICAgICAgICAgIH0sIGNsYXNzTmFtZSk7XG4gICAgICAgICAgICBjb25zdCBjbG9zZUljb24gPSBjbG9zYWJsZSA/ICg8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBvbkNsaWNrPXt0aGlzLmhhbmRsZUNsb3NlfSBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY2xvc2UtaWNvbmB9IHRhYkluZGV4PXswfT5cbiAgICAgICAge2Nsb3NlVGV4dCA/ICg8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY2xvc2UtdGV4dGB9PntjbG9zZVRleHR9PC9zcGFuPikgOiAoPEljb24gdHlwZT1cImNsb3NlXCIvPil9XG4gICAgICA8L2J1dHRvbj4pIDogbnVsbDtcbiAgICAgICAgICAgIGNvbnN0IGRhdGFPckFyaWFQcm9wcyA9IGdldERhdGFPckFyaWFQcm9wcyh0aGlzLnByb3BzKTtcbiAgICAgICAgICAgIGNvbnN0IGljb25Ob2RlID0gKGljb24gJiZcbiAgICAgICAgICAgICAgICAoUmVhY3QuaXNWYWxpZEVsZW1lbnQoaWNvbikgPyAoUmVhY3QuY2xvbmVFbGVtZW50KGljb24sIHtcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKGAke3ByZWZpeENsc30taWNvbmAsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIFtpY29uLnByb3BzLmNsYXNzTmFtZV06IGljb24ucHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICB9KSkgOiAoPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWljb25gfT57aWNvbn08L3NwYW4+KSkpIHx8IDxJY29uIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pY29uYH0gdHlwZT17aWNvblR5cGV9IHRoZW1lPXtpY29uVGhlbWV9Lz47XG4gICAgICAgICAgICByZXR1cm4gY2xvc2VkID8gbnVsbCA6ICg8QW5pbWF0ZSBjb21wb25lbnQ9XCJcIiBzaG93UHJvcD1cImRhdGEtc2hvd1wiIHRyYW5zaXRpb25OYW1lPXtgJHtwcmVmaXhDbHN9LXNsaWRlLXVwYH0gb25FbmQ9e3RoaXMuYW5pbWF0aW9uRW5kfT5cbiAgICAgICAgPGRpdiBkYXRhLXNob3c9eyFjbG9zaW5nfSBjbGFzc05hbWU9e2FsZXJ0Q2xzfSBzdHlsZT17c3R5bGV9IHsuLi5kYXRhT3JBcmlhUHJvcHN9PlxuICAgICAgICAgIHtzaG93SWNvbiA/IGljb25Ob2RlIDogbnVsbH1cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tbWVzc2FnZWB9PnttZXNzYWdlfTwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tZGVzY3JpcHRpb25gfT57ZGVzY3JpcHRpb259PC9zcGFuPlxuICAgICAgICAgIHtjbG9zZUljb259XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9BbmltYXRlPik7XG4gICAgICAgIH07XG4gICAgICAgIHdhcm5pbmcoISgnaWNvblR5cGUnIGluIHByb3BzKSwgJ0FsZXJ0JywgJ2BpY29uVHlwZWAgaXMgZGVwcmVjYXRlZC4gUGxlYXNlIHVzZSBgaWNvbmAgaW5zdGVhZC4nKTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGNsb3Npbmc6IGZhbHNlLFxuICAgICAgICAgICAgY2xvc2VkOiBmYWxzZSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckFsZXJ0fTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFWQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFMQTtBQUNBO0FBTUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFkQTtBQUNBO0FBQ0E7QUFlQTtBQUNBO0FBQ0E7QUE5QkE7QUFDQTtBQUNBO0FBK0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQURBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQXpEQTtBQUNBO0FBNkRBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFwRkE7QUF3RkE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7O0FBNUZBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/alert/index.js
