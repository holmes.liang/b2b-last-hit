/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorTextNode.react
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js"); // In IE, spans with <br> tags render as two newlines. By rendering a span
// with only a newline character, we can be sure to render a single line.


var useNewlineChar = UserAgent.isBrowser('IE <= 11');
/**
 * Check whether the node should be considered a newline.
 */

function isNewline(node) {
  return useNewlineChar ? node.textContent === '\n' : node.tagName === 'BR';
}
/**
 * Placeholder elements for empty text content.
 *
 * What is this `data-text` attribute, anyway? It turns out that we need to
 * put an attribute on the lowest-level text node in order to preserve correct
 * spellcheck handling. If the <span> is naked, Chrome and Safari may do
 * bizarre things to do the DOM -- split text nodes, create extra spans, etc.
 * If the <span> has an attribute, this appears not to happen.
 * See http://jsfiddle.net/9khdavod/ for the failure case, and
 * http://jsfiddle.net/7pg143f7/ for the fixed case.
 */


var NEWLINE_A = useNewlineChar ? React.createElement('span', {
  key: 'A',
  'data-text': 'true'
}, '\n') : React.createElement('br', {
  key: 'A',
  'data-text': 'true'
});
var NEWLINE_B = useNewlineChar ? React.createElement('span', {
  key: 'B',
  'data-text': 'true'
}, '\n') : React.createElement('br', {
  key: 'B',
  'data-text': 'true'
});
/**
 * The lowest-level component in a `DraftEditor`, the text node component
 * replaces the default React text node implementation. This allows us to
 * perform custom handling of newline behavior and avoid re-rendering text
 * nodes with DOM state that already matches the expectations of our immutable
 * editor state.
 */

var DraftEditorTextNode = function (_React$Component) {
  _inherits(DraftEditorTextNode, _React$Component);

  function DraftEditorTextNode(props) {
    _classCallCheck(this, DraftEditorTextNode); // By flipping this flag, we also keep flipping keys which forces
    // React to remount this node every time it rerenders.


    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    _this._forceFlag = false;
    return _this;
  }

  DraftEditorTextNode.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
    var node = ReactDOM.findDOMNode(this);
    var shouldBeNewline = nextProps.children === '';
    !(node instanceof Element) ?  true ? invariant(false, 'node is not an Element') : undefined : void 0;

    if (shouldBeNewline) {
      return !isNewline(node);
    }

    return node.textContent !== nextProps.children;
  };

  DraftEditorTextNode.prototype.componentDidMount = function componentDidMount() {
    this._forceFlag = !this._forceFlag;
  };

  DraftEditorTextNode.prototype.componentDidUpdate = function componentDidUpdate() {
    this._forceFlag = !this._forceFlag;
  };

  DraftEditorTextNode.prototype.render = function render() {
    if (this.props.children === '') {
      return this._forceFlag ? NEWLINE_A : NEWLINE_B;
    }

    return React.createElement('span', {
      key: this._forceFlag ? 'A' : 'B',
      'data-text': 'true'
    }, this.props.children);
  };

  return DraftEditorTextNode;
}(React.Component);

module.exports = DraftEditorTextNode;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yVGV4dE5vZGUucmVhY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFZGl0b3JUZXh0Tm9kZS5yZWFjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RWRpdG9yVGV4dE5vZGUucmVhY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xudmFyIFJlYWN0RE9NID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG52YXIgVXNlckFnZW50ID0gcmVxdWlyZSgnZmJqcy9saWIvVXNlckFnZW50Jyk7XG5cbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcblxuLy8gSW4gSUUsIHNwYW5zIHdpdGggPGJyPiB0YWdzIHJlbmRlciBhcyB0d28gbmV3bGluZXMuIEJ5IHJlbmRlcmluZyBhIHNwYW5cbi8vIHdpdGggb25seSBhIG5ld2xpbmUgY2hhcmFjdGVyLCB3ZSBjYW4gYmUgc3VyZSB0byByZW5kZXIgYSBzaW5nbGUgbGluZS5cbnZhciB1c2VOZXdsaW5lQ2hhciA9IFVzZXJBZ2VudC5pc0Jyb3dzZXIoJ0lFIDw9IDExJyk7XG5cbi8qKlxuICogQ2hlY2sgd2hldGhlciB0aGUgbm9kZSBzaG91bGQgYmUgY29uc2lkZXJlZCBhIG5ld2xpbmUuXG4gKi9cbmZ1bmN0aW9uIGlzTmV3bGluZShub2RlKSB7XG4gIHJldHVybiB1c2VOZXdsaW5lQ2hhciA/IG5vZGUudGV4dENvbnRlbnQgPT09ICdcXG4nIDogbm9kZS50YWdOYW1lID09PSAnQlInO1xufVxuXG4vKipcbiAqIFBsYWNlaG9sZGVyIGVsZW1lbnRzIGZvciBlbXB0eSB0ZXh0IGNvbnRlbnQuXG4gKlxuICogV2hhdCBpcyB0aGlzIGBkYXRhLXRleHRgIGF0dHJpYnV0ZSwgYW55d2F5PyBJdCB0dXJucyBvdXQgdGhhdCB3ZSBuZWVkIHRvXG4gKiBwdXQgYW4gYXR0cmlidXRlIG9uIHRoZSBsb3dlc3QtbGV2ZWwgdGV4dCBub2RlIGluIG9yZGVyIHRvIHByZXNlcnZlIGNvcnJlY3RcbiAqIHNwZWxsY2hlY2sgaGFuZGxpbmcuIElmIHRoZSA8c3Bhbj4gaXMgbmFrZWQsIENocm9tZSBhbmQgU2FmYXJpIG1heSBkb1xuICogYml6YXJyZSB0aGluZ3MgdG8gZG8gdGhlIERPTSAtLSBzcGxpdCB0ZXh0IG5vZGVzLCBjcmVhdGUgZXh0cmEgc3BhbnMsIGV0Yy5cbiAqIElmIHRoZSA8c3Bhbj4gaGFzIGFuIGF0dHJpYnV0ZSwgdGhpcyBhcHBlYXJzIG5vdCB0byBoYXBwZW4uXG4gKiBTZWUgaHR0cDovL2pzZmlkZGxlLm5ldC85a2hkYXZvZC8gZm9yIHRoZSBmYWlsdXJlIGNhc2UsIGFuZFxuICogaHR0cDovL2pzZmlkZGxlLm5ldC83cGcxNDNmNy8gZm9yIHRoZSBmaXhlZCBjYXNlLlxuICovXG52YXIgTkVXTElORV9BID0gdXNlTmV3bGluZUNoYXIgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAnc3BhbicsXG4gIHsga2V5OiAnQScsICdkYXRhLXRleHQnOiAndHJ1ZScgfSxcbiAgJ1xcbidcbikgOiBSZWFjdC5jcmVhdGVFbGVtZW50KCdicicsIHsga2V5OiAnQScsICdkYXRhLXRleHQnOiAndHJ1ZScgfSk7XG5cbnZhciBORVdMSU5FX0IgPSB1c2VOZXdsaW5lQ2hhciA/IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICdzcGFuJyxcbiAgeyBrZXk6ICdCJywgJ2RhdGEtdGV4dCc6ICd0cnVlJyB9LFxuICAnXFxuJ1xuKSA6IFJlYWN0LmNyZWF0ZUVsZW1lbnQoJ2JyJywgeyBrZXk6ICdCJywgJ2RhdGEtdGV4dCc6ICd0cnVlJyB9KTtcblxuLyoqXG4gKiBUaGUgbG93ZXN0LWxldmVsIGNvbXBvbmVudCBpbiBhIGBEcmFmdEVkaXRvcmAsIHRoZSB0ZXh0IG5vZGUgY29tcG9uZW50XG4gKiByZXBsYWNlcyB0aGUgZGVmYXVsdCBSZWFjdCB0ZXh0IG5vZGUgaW1wbGVtZW50YXRpb24uIFRoaXMgYWxsb3dzIHVzIHRvXG4gKiBwZXJmb3JtIGN1c3RvbSBoYW5kbGluZyBvZiBuZXdsaW5lIGJlaGF2aW9yIGFuZCBhdm9pZCByZS1yZW5kZXJpbmcgdGV4dFxuICogbm9kZXMgd2l0aCBET00gc3RhdGUgdGhhdCBhbHJlYWR5IG1hdGNoZXMgdGhlIGV4cGVjdGF0aW9ucyBvZiBvdXIgaW1tdXRhYmxlXG4gKiBlZGl0b3Igc3RhdGUuXG4gKi9cbnZhciBEcmFmdEVkaXRvclRleHROb2RlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKERyYWZ0RWRpdG9yVGV4dE5vZGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERyYWZ0RWRpdG9yVGV4dE5vZGUocHJvcHMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRHJhZnRFZGl0b3JUZXh0Tm9kZSk7XG5cbiAgICAvLyBCeSBmbGlwcGluZyB0aGlzIGZsYWcsIHdlIGFsc28ga2VlcCBmbGlwcGluZyBrZXlzIHdoaWNoIGZvcmNlc1xuICAgIC8vIFJlYWN0IHRvIHJlbW91bnQgdGhpcyBub2RlIGV2ZXJ5IHRpbWUgaXQgcmVyZW5kZXJzLlxuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX3RoaXMuX2ZvcmNlRmxhZyA9IGZhbHNlO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIERyYWZ0RWRpdG9yVGV4dE5vZGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICB2YXIgbm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMpO1xuICAgIHZhciBzaG91bGRCZU5ld2xpbmUgPSBuZXh0UHJvcHMuY2hpbGRyZW4gPT09ICcnO1xuICAgICEobm9kZSBpbnN0YW5jZW9mIEVsZW1lbnQpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ25vZGUgaXMgbm90IGFuIEVsZW1lbnQnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgaWYgKHNob3VsZEJlTmV3bGluZSkge1xuICAgICAgcmV0dXJuICFpc05ld2xpbmUobm9kZSk7XG4gICAgfVxuICAgIHJldHVybiBub2RlLnRleHRDb250ZW50ICE9PSBuZXh0UHJvcHMuY2hpbGRyZW47XG4gIH07XG5cbiAgRHJhZnRFZGl0b3JUZXh0Tm9kZS5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLl9mb3JjZUZsYWcgPSAhdGhpcy5fZm9yY2VGbGFnO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yVGV4dE5vZGUucHJvdG90eXBlLmNvbXBvbmVudERpZFVwZGF0ZSA9IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICB0aGlzLl9mb3JjZUZsYWcgPSAhdGhpcy5fZm9yY2VGbGFnO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yVGV4dE5vZGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICBpZiAodGhpcy5wcm9wcy5jaGlsZHJlbiA9PT0gJycpIHtcbiAgICAgIHJldHVybiB0aGlzLl9mb3JjZUZsYWcgPyBORVdMSU5FX0EgOiBORVdMSU5FX0I7XG4gICAgfVxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ3NwYW4nLFxuICAgICAgeyBrZXk6IHRoaXMuX2ZvcmNlRmxhZyA/ICdBJyA6ICdCJywgJ2RhdGEtdGV4dCc6ICd0cnVlJyB9LFxuICAgICAgdGhpcy5wcm9wcy5jaGlsZHJlblxuICAgICk7XG4gIH07XG5cbiAgcmV0dXJuIERyYWZ0RWRpdG9yVGV4dE5vZGU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbm1vZHVsZS5leHBvcnRzID0gRHJhZnRFZGl0b3JUZXh0Tm9kZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUVBOzs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFXQTtBQUVBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBRUE7Ozs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorTextNode.react.js
