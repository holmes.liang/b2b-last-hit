/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnFocus
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

function editOnFocus(editor, e) {
  var editorState = editor._latestEditorState;
  var currentSelection = editorState.getSelection();

  if (currentSelection.getHasFocus()) {
    return;
  }

  var selection = currentSelection.set('hasFocus', true);
  editor.props.onFocus && editor.props.onFocus(e); // When the tab containing this text editor is hidden and the user does a
  // find-in-page in a _different_ tab, Chrome on Mac likes to forget what the
  // selection was right after sending this focus event and (if you let it)
  // moves the cursor back to the beginning of the editor, so we force the
  // selection here instead of simply accepting it in order to preserve the
  // old cursor position. See https://crbug.com/540004.
  // But it looks like this is fixed in Chrome 60.0.3081.0.
  // Other browsers also don't have this bug, so we prefer to acceptSelection
  // when possible, to ensure that unfocusing and refocusing a Draft editor
  // doesn't preserve the selection, matching how textareas work.

  if (UserAgent.isBrowser('Chrome < 60.0.3081.0')) {
    editor.update(EditorState.forceSelection(editorState, selection));
  } else {
    editor.update(EditorState.acceptSelection(editorState, selection));
  }
}

module.exports = editOnFocus;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkZvY3VzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkZvY3VzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZWRpdE9uRm9jdXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xudmFyIFVzZXJBZ2VudCA9IHJlcXVpcmUoJ2ZianMvbGliL1VzZXJBZ2VudCcpO1xuXG5mdW5jdGlvbiBlZGl0T25Gb2N1cyhlZGl0b3IsIGUpIHtcbiAgdmFyIGVkaXRvclN0YXRlID0gZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZTtcbiAgdmFyIGN1cnJlbnRTZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgaWYgKGN1cnJlbnRTZWxlY3Rpb24uZ2V0SGFzRm9jdXMoKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzZWxlY3Rpb24gPSBjdXJyZW50U2VsZWN0aW9uLnNldCgnaGFzRm9jdXMnLCB0cnVlKTtcbiAgZWRpdG9yLnByb3BzLm9uRm9jdXMgJiYgZWRpdG9yLnByb3BzLm9uRm9jdXMoZSk7XG5cbiAgLy8gV2hlbiB0aGUgdGFiIGNvbnRhaW5pbmcgdGhpcyB0ZXh0IGVkaXRvciBpcyBoaWRkZW4gYW5kIHRoZSB1c2VyIGRvZXMgYVxuICAvLyBmaW5kLWluLXBhZ2UgaW4gYSBfZGlmZmVyZW50XyB0YWIsIENocm9tZSBvbiBNYWMgbGlrZXMgdG8gZm9yZ2V0IHdoYXQgdGhlXG4gIC8vIHNlbGVjdGlvbiB3YXMgcmlnaHQgYWZ0ZXIgc2VuZGluZyB0aGlzIGZvY3VzIGV2ZW50IGFuZCAoaWYgeW91IGxldCBpdClcbiAgLy8gbW92ZXMgdGhlIGN1cnNvciBiYWNrIHRvIHRoZSBiZWdpbm5pbmcgb2YgdGhlIGVkaXRvciwgc28gd2UgZm9yY2UgdGhlXG4gIC8vIHNlbGVjdGlvbiBoZXJlIGluc3RlYWQgb2Ygc2ltcGx5IGFjY2VwdGluZyBpdCBpbiBvcmRlciB0byBwcmVzZXJ2ZSB0aGVcbiAgLy8gb2xkIGN1cnNvciBwb3NpdGlvbi4gU2VlIGh0dHBzOi8vY3JidWcuY29tLzU0MDAwNC5cbiAgLy8gQnV0IGl0IGxvb2tzIGxpa2UgdGhpcyBpcyBmaXhlZCBpbiBDaHJvbWUgNjAuMC4zMDgxLjAuXG4gIC8vIE90aGVyIGJyb3dzZXJzIGFsc28gZG9uJ3QgaGF2ZSB0aGlzIGJ1Zywgc28gd2UgcHJlZmVyIHRvIGFjY2VwdFNlbGVjdGlvblxuICAvLyB3aGVuIHBvc3NpYmxlLCB0byBlbnN1cmUgdGhhdCB1bmZvY3VzaW5nIGFuZCByZWZvY3VzaW5nIGEgRHJhZnQgZWRpdG9yXG4gIC8vIGRvZXNuJ3QgcHJlc2VydmUgdGhlIHNlbGVjdGlvbiwgbWF0Y2hpbmcgaG93IHRleHRhcmVhcyB3b3JrLlxuICBpZiAoVXNlckFnZW50LmlzQnJvd3NlcignQ2hyb21lIDwgNjAuMC4zMDgxLjAnKSkge1xuICAgIGVkaXRvci51cGRhdGUoRWRpdG9yU3RhdGUuZm9yY2VTZWxlY3Rpb24oZWRpdG9yU3RhdGUsIHNlbGVjdGlvbikpO1xuICB9IGVsc2Uge1xuICAgIGVkaXRvci51cGRhdGUoRWRpdG9yU3RhdGUuYWNjZXB0U2VsZWN0aW9uKGVkaXRvclN0YXRlLCBzZWxlY3Rpb24pKTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGVkaXRPbkZvY3VzOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnFocus.js
