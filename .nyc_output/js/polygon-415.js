var windingLine = __webpack_require__(/*! ./windingLine */ "./node_modules/zrender/lib/contain/windingLine.js");

var EPSILON = 1e-8;

function isAroundEqual(a, b) {
  return Math.abs(a - b) < EPSILON;
}

function contain(points, x, y) {
  var w = 0;
  var p = points[0];

  if (!p) {
    return false;
  }

  for (var i = 1; i < points.length; i++) {
    var p2 = points[i];
    w += windingLine(p[0], p[1], p2[0], p2[1], x, y);
    p = p2;
  } // Close polygon


  var p0 = points[0];

  if (!isAroundEqual(p[0], p0[0]) || !isAroundEqual(p[1], p0[1])) {
    w += windingLine(p[0], p[1], p0[0], p0[1], x, y);
  }

  return w !== 0;
}

exports.contain = contain;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9wb2x5Z29uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9wb2x5Z29uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciB3aW5kaW5nTGluZSA9IHJlcXVpcmUoXCIuL3dpbmRpbmdMaW5lXCIpO1xuXG52YXIgRVBTSUxPTiA9IDFlLTg7XG5cbmZ1bmN0aW9uIGlzQXJvdW5kRXF1YWwoYSwgYikge1xuICByZXR1cm4gTWF0aC5hYnMoYSAtIGIpIDwgRVBTSUxPTjtcbn1cblxuZnVuY3Rpb24gY29udGFpbihwb2ludHMsIHgsIHkpIHtcbiAgdmFyIHcgPSAwO1xuICB2YXIgcCA9IHBvaW50c1swXTtcblxuICBpZiAoIXApIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBmb3IgKHZhciBpID0gMTsgaSA8IHBvaW50cy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBwMiA9IHBvaW50c1tpXTtcbiAgICB3ICs9IHdpbmRpbmdMaW5lKHBbMF0sIHBbMV0sIHAyWzBdLCBwMlsxXSwgeCwgeSk7XG4gICAgcCA9IHAyO1xuICB9IC8vIENsb3NlIHBvbHlnb25cblxuXG4gIHZhciBwMCA9IHBvaW50c1swXTtcblxuICBpZiAoIWlzQXJvdW5kRXF1YWwocFswXSwgcDBbMF0pIHx8ICFpc0Fyb3VuZEVxdWFsKHBbMV0sIHAwWzFdKSkge1xuICAgIHcgKz0gd2luZGluZ0xpbmUocFswXSwgcFsxXSwgcDBbMF0sIHAwWzFdLCB4LCB5KTtcbiAgfVxuXG4gIHJldHVybiB3ICE9PSAwO1xufVxuXG5leHBvcnRzLmNvbnRhaW4gPSBjb250YWluOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/polygon.js
