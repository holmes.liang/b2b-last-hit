__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Menu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_menu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-menu */ "./node_modules/rc-menu/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _SubMenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./SubMenu */ "./node_modules/antd/es/menu/SubMenu.js");
/* harmony import */ var _MenuItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./MenuItem */ "./node_modules/antd/es/menu/MenuItem.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _layout_Sider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../layout/Sider */ "./node_modules/antd/es/layout/Sider.js");
/* harmony import */ var _util_raf__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../_util/raf */ "./node_modules/antd/es/_util/raf.js");
/* harmony import */ var _util_motion__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../_util/motion */ "./node_modules/antd/es/_util/motion.js");
/* harmony import */ var _MenuContext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./MenuContext */ "./node_modules/antd/es/menu/MenuContext.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}















var InternalMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InternalMenu, _React$Component);

  function InternalMenu(props) {
    var _this;

    _classCallCheck(this, InternalMenu);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(InternalMenu).call(this, props)); // Restore vertical mode when menu is collapsed responsively when mounted
    // https://github.com/ant-design/ant-design/issues/13104
    // TODO: not a perfect solution, looking a new way to avoid setting switchingModeFromInline in this situation

    _this.handleMouseEnter = function (e) {
      _this.restoreModeVerticalFromInline();

      var onMouseEnter = _this.props.onMouseEnter;

      if (onMouseEnter) {
        onMouseEnter(e);
      }
    };

    _this.handleTransitionEnd = function (e) {
      // when inlineCollapsed menu width animation finished
      // https://github.com/ant-design/ant-design/issues/12864
      var widthCollapsed = e.propertyName === 'width' && e.target === e.currentTarget; // Fix SVGElement e.target.className.indexOf is not a function
      // https://github.com/ant-design/ant-design/issues/15699

      var className = e.target.className; // SVGAnimatedString.animVal should be identical to SVGAnimatedString.baseVal, unless during an animation.

      var classNameValue = Object.prototype.toString.call(className) === '[object SVGAnimatedString]' ? className.animVal : className; // Fix for <Menu style={{ width: '100%' }} />, the width transition won't trigger when menu is collapsed
      // https://github.com/ant-design/ant-design-pro/issues/2783

      var iconScaled = e.propertyName === 'font-size' && classNameValue.indexOf('anticon') >= 0;

      if (widthCollapsed || iconScaled) {
        _this.restoreModeVerticalFromInline();
      }
    };

    _this.handleClick = function (e) {
      _this.handleOpenChange([]);

      var onClick = _this.props.onClick;

      if (onClick) {
        onClick(e);
      }
    };

    _this.handleOpenChange = function (openKeys) {
      _this.setOpenKeys(openKeys);

      var onOpenChange = _this.props.onOpenChange;

      if (onOpenChange) {
        onOpenChange(openKeys);
      }
    };

    _this.renderMenu = function (_ref) {
      var getPopupContainer = _ref.getPopupContainer,
          getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          className = _this$props.className,
          theme = _this$props.theme,
          collapsedWidth = _this$props.collapsedWidth;
      var passProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_this.props, ['collapsedWidth', 'siderCollapsed']);

      var menuMode = _this.getRealMenuMode();

      var menuOpenMotion = _this.getOpenMotionProps(menuMode);

      var prefixCls = getPrefixCls('menu', customizePrefixCls);
      var menuClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, "".concat(prefixCls, "-").concat(theme), _defineProperty({}, "".concat(prefixCls, "-inline-collapsed"), _this.getInlineCollapsed()));

      var menuProps = _extends({
        openKeys: _this.state.openKeys,
        onOpenChange: _this.handleOpenChange,
        className: menuClassName,
        mode: menuMode
      }, menuOpenMotion);

      if (menuMode !== 'inline') {
        // closing vertical popup submenu after click it
        menuProps.onClick = _this.handleClick;
      } // https://github.com/ant-design/ant-design/issues/8587


      var hideMenu = _this.getInlineCollapsed() && (collapsedWidth === 0 || collapsedWidth === '0' || collapsedWidth === '0px');

      if (hideMenu) {
        menuProps.openKeys = [];
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_menu__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
        getPopupContainer: getPopupContainer
      }, passProps, menuProps, {
        prefixCls: prefixCls,
        onTransitionEnd: _this.handleTransitionEnd,
        onMouseEnter: _this.handleMouseEnter
      }));
    };

    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!('onOpen' in props || 'onClose' in props), 'Menu', '`onOpen` and `onClose` are removed, please use `onOpenChange` instead, ' + 'see: https://u.ant.design/menu-on-open-change.');
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!('inlineCollapsed' in props && props.mode !== 'inline'), 'Menu', '`inlineCollapsed` should only be used when `mode` is inline.');
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!(props.siderCollapsed !== undefined && 'inlineCollapsed' in props), 'Menu', '`inlineCollapsed` not control Menu under Sider. Should set `collapsed` on Sider instead.');
    var openKeys;

    if ('openKeys' in props) {
      openKeys = props.openKeys;
    } else if ('defaultOpenKeys' in props) {
      openKeys = props.defaultOpenKeys;
    }

    _this.state = {
      openKeys: openKeys || [],
      switchingModeFromInline: false,
      inlineOpenKeys: [],
      prevProps: props
    };
    return _this;
  }

  _createClass(InternalMenu, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _util_raf__WEBPACK_IMPORTED_MODULE_10__["default"].cancel(this.mountRafId);
    }
  }, {
    key: "setOpenKeys",
    value: function setOpenKeys(openKeys) {
      if (!('openKeys' in this.props)) {
        this.setState({
          openKeys: openKeys
        });
      }
    }
  }, {
    key: "getRealMenuMode",
    value: function getRealMenuMode() {
      var inlineCollapsed = this.getInlineCollapsed();

      if (this.state.switchingModeFromInline && inlineCollapsed) {
        return 'inline';
      }

      var mode = this.props.mode;
      return inlineCollapsed ? 'vertical' : mode;
    }
  }, {
    key: "getInlineCollapsed",
    value: function getInlineCollapsed() {
      var inlineCollapsed = this.props.inlineCollapsed;

      if (this.props.siderCollapsed !== undefined) {
        return this.props.siderCollapsed;
      }

      return inlineCollapsed;
    }
  }, {
    key: "getOpenMotionProps",
    value: function getOpenMotionProps(menuMode) {
      var _this$props2 = this.props,
          openTransitionName = _this$props2.openTransitionName,
          openAnimation = _this$props2.openAnimation,
          motion = _this$props2.motion; // Provides by user

      if (motion) {
        return {
          motion: motion
        };
      }

      if (openAnimation) {
        Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(typeof openAnimation === 'string', 'Menu', '`openAnimation` do not support object. Please use `motion` instead.');
        return {
          openAnimation: openAnimation
        };
      }

      if (openTransitionName) {
        return {
          openTransitionName: openTransitionName
        };
      } // Default logic


      if (menuMode === 'horizontal') {
        return {
          motion: {
            motionName: 'slide-up'
          }
        };
      }

      if (menuMode === 'inline') {
        return {
          motion: _util_motion__WEBPACK_IMPORTED_MODULE_11__["default"]
        };
      } // When mode switch from inline
      // submenu should hide without animation


      return {
        motion: {
          motionName: this.state.switchingModeFromInline ? '' : 'zoom-big'
        }
      };
    }
  }, {
    key: "restoreModeVerticalFromInline",
    value: function restoreModeVerticalFromInline() {
      var switchingModeFromInline = this.state.switchingModeFromInline;

      if (switchingModeFromInline) {
        this.setState({
          switchingModeFromInline: false
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_MenuContext__WEBPACK_IMPORTED_MODULE_12__["default"].Provider, {
        value: {
          inlineCollapsed: this.getInlineCollapsed() || false,
          antdMenuTheme: this.props.theme
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderMenu));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var prevProps = prevState.prevProps;
      var newState = {
        prevProps: nextProps
      };

      if (prevProps.mode === 'inline' && nextProps.mode !== 'inline') {
        newState.switchingModeFromInline = true;
      }

      if ('openKeys' in nextProps) {
        newState.openKeys = nextProps.openKeys;
      } else {
        // [Legacy] Old code will return after `openKeys` changed.
        // Not sure the reason, we should keep this logic still.
        if (nextProps.inlineCollapsed && !prevProps.inlineCollapsed || nextProps.siderCollapsed && !prevProps.siderCollapsed) {
          newState.switchingModeFromInline = true;
          newState.inlineOpenKeys = prevState.openKeys;
          newState.openKeys = [];
        }

        if (!nextProps.inlineCollapsed && prevProps.inlineCollapsed || !nextProps.siderCollapsed && prevProps.siderCollapsed) {
          newState.openKeys = prevState.inlineOpenKeys;
          newState.inlineOpenKeys = [];
        }
      }

      return newState;
    }
  }]);

  return InternalMenu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

InternalMenu.defaultProps = {
  className: '',
  theme: 'light',
  focusable: false
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__["polyfill"])(InternalMenu); // We should keep this as ref-able

var Menu =
/*#__PURE__*/
function (_React$Component2) {
  _inherits(Menu, _React$Component2);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, _getPrototypeOf(Menu).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_layout_Sider__WEBPACK_IMPORTED_MODULE_9__["SiderContext"].Consumer, null, function (context) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](InternalMenu, _extends({}, _this2.props, context));
      });
    }
  }]);

  return Menu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Menu.Divider = rc_menu__WEBPACK_IMPORTED_MODULE_1__["Divider"];
Menu.Item = _MenuItem__WEBPACK_IMPORTED_MODULE_6__["default"];
Menu.SubMenu = _SubMenu__WEBPACK_IMPORTED_MODULE_5__["default"];
Menu.ItemGroup = rc_menu__WEBPACK_IMPORTED_MODULE_1__["ItemGroup"];//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZW51L2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9tZW51L2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUmNNZW51LCB7IERpdmlkZXIsIEl0ZW1Hcm91cCB9IGZyb20gJ3JjLW1lbnUnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IFN1Yk1lbnUgZnJvbSAnLi9TdWJNZW51JztcbmltcG9ydCBJdGVtIGZyb20gJy4vTWVudUl0ZW0nO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5pbXBvcnQgeyBTaWRlckNvbnRleHQgfSBmcm9tICcuLi9sYXlvdXQvU2lkZXInO1xuaW1wb3J0IHJhZiBmcm9tICcuLi9fdXRpbC9yYWYnO1xuaW1wb3J0IGNvbGxhcHNlTW90aW9uIGZyb20gJy4uL191dGlsL21vdGlvbic7XG5pbXBvcnQgTWVudUNvbnRleHQgZnJvbSAnLi9NZW51Q29udGV4dCc7XG5jbGFzcyBJbnRlcm5hbE1lbnUgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgLy8gUmVzdG9yZSB2ZXJ0aWNhbCBtb2RlIHdoZW4gbWVudSBpcyBjb2xsYXBzZWQgcmVzcG9uc2l2ZWx5IHdoZW4gbW91bnRlZFxuICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMzEwNFxuICAgICAgICAvLyBUT0RPOiBub3QgYSBwZXJmZWN0IHNvbHV0aW9uLCBsb29raW5nIGEgbmV3IHdheSB0byBhdm9pZCBzZXR0aW5nIHN3aXRjaGluZ01vZGVGcm9tSW5saW5lIGluIHRoaXMgc2l0dWF0aW9uXG4gICAgICAgIHRoaXMuaGFuZGxlTW91c2VFbnRlciA9IChlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnJlc3RvcmVNb2RlVmVydGljYWxGcm9tSW5saW5lKCk7XG4gICAgICAgICAgICBjb25zdCB7IG9uTW91c2VFbnRlciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbk1vdXNlRW50ZXIpIHtcbiAgICAgICAgICAgICAgICBvbk1vdXNlRW50ZXIoZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlVHJhbnNpdGlvbkVuZCA9IChlKSA9PiB7XG4gICAgICAgICAgICAvLyB3aGVuIGlubGluZUNvbGxhcHNlZCBtZW51IHdpZHRoIGFuaW1hdGlvbiBmaW5pc2hlZFxuICAgICAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTI4NjRcbiAgICAgICAgICAgIGNvbnN0IHdpZHRoQ29sbGFwc2VkID0gZS5wcm9wZXJ0eU5hbWUgPT09ICd3aWR0aCcgJiYgZS50YXJnZXQgPT09IGUuY3VycmVudFRhcmdldDtcbiAgICAgICAgICAgIC8vIEZpeCBTVkdFbGVtZW50IGUudGFyZ2V0LmNsYXNzTmFtZS5pbmRleE9mIGlzIG5vdCBhIGZ1bmN0aW9uXG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNTY5OVxuICAgICAgICAgICAgY29uc3QgeyBjbGFzc05hbWUgfSA9IGUudGFyZ2V0O1xuICAgICAgICAgICAgLy8gU1ZHQW5pbWF0ZWRTdHJpbmcuYW5pbVZhbCBzaG91bGQgYmUgaWRlbnRpY2FsIHRvIFNWR0FuaW1hdGVkU3RyaW5nLmJhc2VWYWwsIHVubGVzcyBkdXJpbmcgYW4gYW5pbWF0aW9uLlxuICAgICAgICAgICAgY29uc3QgY2xhc3NOYW1lVmFsdWUgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoY2xhc3NOYW1lKSA9PT0gJ1tvYmplY3QgU1ZHQW5pbWF0ZWRTdHJpbmddJ1xuICAgICAgICAgICAgICAgID8gY2xhc3NOYW1lLmFuaW1WYWxcbiAgICAgICAgICAgICAgICA6IGNsYXNzTmFtZTtcbiAgICAgICAgICAgIC8vIEZpeCBmb3IgPE1lbnUgc3R5bGU9e3sgd2lkdGg6ICcxMDAlJyB9fSAvPiwgdGhlIHdpZHRoIHRyYW5zaXRpb24gd29uJ3QgdHJpZ2dlciB3aGVuIG1lbnUgaXMgY29sbGFwc2VkXG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduLXByby9pc3N1ZXMvMjc4M1xuICAgICAgICAgICAgY29uc3QgaWNvblNjYWxlZCA9IGUucHJvcGVydHlOYW1lID09PSAnZm9udC1zaXplJyAmJiBjbGFzc05hbWVWYWx1ZS5pbmRleE9mKCdhbnRpY29uJykgPj0gMDtcbiAgICAgICAgICAgIGlmICh3aWR0aENvbGxhcHNlZCB8fCBpY29uU2NhbGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXN0b3JlTW9kZVZlcnRpY2FsRnJvbUlubGluZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZUNsaWNrID0gKGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlT3BlbkNoYW5nZShbXSk7XG4gICAgICAgICAgICBjb25zdCB7IG9uQ2xpY2sgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAob25DbGljaykge1xuICAgICAgICAgICAgICAgIG9uQ2xpY2soZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlT3BlbkNoYW5nZSA9IChvcGVuS2V5cykgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRPcGVuS2V5cyhvcGVuS2V5cyk7XG4gICAgICAgICAgICBjb25zdCB7IG9uT3BlbkNoYW5nZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbk9wZW5DaGFuZ2UpIHtcbiAgICAgICAgICAgICAgICBvbk9wZW5DaGFuZ2Uob3BlbktleXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlck1lbnUgPSAoeyBnZXRQb3B1cENvbnRhaW5lciwgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNsYXNzTmFtZSwgdGhlbWUsIGNvbGxhcHNlZFdpZHRoIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcGFzc1Byb3BzID0gb21pdCh0aGlzLnByb3BzLCBbJ2NvbGxhcHNlZFdpZHRoJywgJ3NpZGVyQ29sbGFwc2VkJ10pO1xuICAgICAgICAgICAgY29uc3QgbWVudU1vZGUgPSB0aGlzLmdldFJlYWxNZW51TW9kZSgpO1xuICAgICAgICAgICAgY29uc3QgbWVudU9wZW5Nb3Rpb24gPSB0aGlzLmdldE9wZW5Nb3Rpb25Qcm9wcyhtZW51TW9kZSk7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ21lbnUnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgbWVudUNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCBgJHtwcmVmaXhDbHN9LSR7dGhlbWV9YCwge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWlubGluZS1jb2xsYXBzZWRgXTogdGhpcy5nZXRJbmxpbmVDb2xsYXBzZWQoKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc3QgbWVudVByb3BzID0gT2JqZWN0LmFzc2lnbih7IG9wZW5LZXlzOiB0aGlzLnN0YXRlLm9wZW5LZXlzLCBvbk9wZW5DaGFuZ2U6IHRoaXMuaGFuZGxlT3BlbkNoYW5nZSwgY2xhc3NOYW1lOiBtZW51Q2xhc3NOYW1lLCBtb2RlOiBtZW51TW9kZSB9LCBtZW51T3Blbk1vdGlvbik7XG4gICAgICAgICAgICBpZiAobWVudU1vZGUgIT09ICdpbmxpbmUnKSB7XG4gICAgICAgICAgICAgICAgLy8gY2xvc2luZyB2ZXJ0aWNhbCBwb3B1cCBzdWJtZW51IGFmdGVyIGNsaWNrIGl0XG4gICAgICAgICAgICAgICAgbWVudVByb3BzLm9uQ2xpY2sgPSB0aGlzLmhhbmRsZUNsaWNrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvODU4N1xuICAgICAgICAgICAgY29uc3QgaGlkZU1lbnUgPSB0aGlzLmdldElubGluZUNvbGxhcHNlZCgpICYmXG4gICAgICAgICAgICAgICAgKGNvbGxhcHNlZFdpZHRoID09PSAwIHx8IGNvbGxhcHNlZFdpZHRoID09PSAnMCcgfHwgY29sbGFwc2VkV2lkdGggPT09ICcwcHgnKTtcbiAgICAgICAgICAgIGlmIChoaWRlTWVudSkge1xuICAgICAgICAgICAgICAgIG1lbnVQcm9wcy5vcGVuS2V5cyA9IFtdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICg8UmNNZW51IGdldFBvcHVwQ29udGFpbmVyPXtnZXRQb3B1cENvbnRhaW5lcn0gey4uLnBhc3NQcm9wc30gey4uLm1lbnVQcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9IG9uVHJhbnNpdGlvbkVuZD17dGhpcy5oYW5kbGVUcmFuc2l0aW9uRW5kfSBvbk1vdXNlRW50ZXI9e3RoaXMuaGFuZGxlTW91c2VFbnRlcn0vPik7XG4gICAgICAgIH07XG4gICAgICAgIHdhcm5pbmcoISgnb25PcGVuJyBpbiBwcm9wcyB8fCAnb25DbG9zZScgaW4gcHJvcHMpLCAnTWVudScsICdgb25PcGVuYCBhbmQgYG9uQ2xvc2VgIGFyZSByZW1vdmVkLCBwbGVhc2UgdXNlIGBvbk9wZW5DaGFuZ2VgIGluc3RlYWQsICcgK1xuICAgICAgICAgICAgJ3NlZTogaHR0cHM6Ly91LmFudC5kZXNpZ24vbWVudS1vbi1vcGVuLWNoYW5nZS4nKTtcbiAgICAgICAgd2FybmluZyghKCdpbmxpbmVDb2xsYXBzZWQnIGluIHByb3BzICYmIHByb3BzLm1vZGUgIT09ICdpbmxpbmUnKSwgJ01lbnUnLCAnYGlubGluZUNvbGxhcHNlZGAgc2hvdWxkIG9ubHkgYmUgdXNlZCB3aGVuIGBtb2RlYCBpcyBpbmxpbmUuJyk7XG4gICAgICAgIHdhcm5pbmcoIShwcm9wcy5zaWRlckNvbGxhcHNlZCAhPT0gdW5kZWZpbmVkICYmICdpbmxpbmVDb2xsYXBzZWQnIGluIHByb3BzKSwgJ01lbnUnLCAnYGlubGluZUNvbGxhcHNlZGAgbm90IGNvbnRyb2wgTWVudSB1bmRlciBTaWRlci4gU2hvdWxkIHNldCBgY29sbGFwc2VkYCBvbiBTaWRlciBpbnN0ZWFkLicpO1xuICAgICAgICBsZXQgb3BlbktleXM7XG4gICAgICAgIGlmICgnb3BlbktleXMnIGluIHByb3BzKSB7XG4gICAgICAgICAgICBvcGVuS2V5cyA9IHByb3BzLm9wZW5LZXlzO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKCdkZWZhdWx0T3BlbktleXMnIGluIHByb3BzKSB7XG4gICAgICAgICAgICBvcGVuS2V5cyA9IHByb3BzLmRlZmF1bHRPcGVuS2V5cztcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgICAgICAgb3BlbktleXM6IG9wZW5LZXlzIHx8IFtdLFxuICAgICAgICAgICAgc3dpdGNoaW5nTW9kZUZyb21JbmxpbmU6IGZhbHNlLFxuICAgICAgICAgICAgaW5saW5lT3BlbktleXM6IFtdLFxuICAgICAgICAgICAgcHJldlByb3BzOiBwcm9wcyxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgICBjb25zdCB7IHByZXZQcm9wcyB9ID0gcHJldlN0YXRlO1xuICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHtcbiAgICAgICAgICAgIHByZXZQcm9wczogbmV4dFByb3BzLFxuICAgICAgICB9O1xuICAgICAgICBpZiAocHJldlByb3BzLm1vZGUgPT09ICdpbmxpbmUnICYmIG5leHRQcm9wcy5tb2RlICE9PSAnaW5saW5lJykge1xuICAgICAgICAgICAgbmV3U3RhdGUuc3dpdGNoaW5nTW9kZUZyb21JbmxpbmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICgnb3BlbktleXMnIGluIG5leHRQcm9wcykge1xuICAgICAgICAgICAgbmV3U3RhdGUub3BlbktleXMgPSBuZXh0UHJvcHMub3BlbktleXM7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAvLyBbTGVnYWN5XSBPbGQgY29kZSB3aWxsIHJldHVybiBhZnRlciBgb3BlbktleXNgIGNoYW5nZWQuXG4gICAgICAgICAgICAvLyBOb3Qgc3VyZSB0aGUgcmVhc29uLCB3ZSBzaG91bGQga2VlcCB0aGlzIGxvZ2ljIHN0aWxsLlxuICAgICAgICAgICAgaWYgKChuZXh0UHJvcHMuaW5saW5lQ29sbGFwc2VkICYmICFwcmV2UHJvcHMuaW5saW5lQ29sbGFwc2VkKSB8fFxuICAgICAgICAgICAgICAgIChuZXh0UHJvcHMuc2lkZXJDb2xsYXBzZWQgJiYgIXByZXZQcm9wcy5zaWRlckNvbGxhcHNlZCkpIHtcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5zd2l0Y2hpbmdNb2RlRnJvbUlubGluZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgbmV3U3RhdGUuaW5saW5lT3BlbktleXMgPSBwcmV2U3RhdGUub3BlbktleXM7XG4gICAgICAgICAgICAgICAgbmV3U3RhdGUub3BlbktleXMgPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICgoIW5leHRQcm9wcy5pbmxpbmVDb2xsYXBzZWQgJiYgcHJldlByb3BzLmlubGluZUNvbGxhcHNlZCkgfHxcbiAgICAgICAgICAgICAgICAoIW5leHRQcm9wcy5zaWRlckNvbGxhcHNlZCAmJiBwcmV2UHJvcHMuc2lkZXJDb2xsYXBzZWQpKSB7XG4gICAgICAgICAgICAgICAgbmV3U3RhdGUub3BlbktleXMgPSBwcmV2U3RhdGUuaW5saW5lT3BlbktleXM7XG4gICAgICAgICAgICAgICAgbmV3U3RhdGUuaW5saW5lT3BlbktleXMgPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbmV3U3RhdGU7XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICByYWYuY2FuY2VsKHRoaXMubW91bnRSYWZJZCk7XG4gICAgfVxuICAgIHNldE9wZW5LZXlzKG9wZW5LZXlzKSB7XG4gICAgICAgIGlmICghKCdvcGVuS2V5cycgaW4gdGhpcy5wcm9wcykpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBvcGVuS2V5cyB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBnZXRSZWFsTWVudU1vZGUoKSB7XG4gICAgICAgIGNvbnN0IGlubGluZUNvbGxhcHNlZCA9IHRoaXMuZ2V0SW5saW5lQ29sbGFwc2VkKCk7XG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnN3aXRjaGluZ01vZGVGcm9tSW5saW5lICYmIGlubGluZUNvbGxhcHNlZCkge1xuICAgICAgICAgICAgcmV0dXJuICdpbmxpbmUnO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IHsgbW9kZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIGlubGluZUNvbGxhcHNlZCA/ICd2ZXJ0aWNhbCcgOiBtb2RlO1xuICAgIH1cbiAgICBnZXRJbmxpbmVDb2xsYXBzZWQoKSB7XG4gICAgICAgIGNvbnN0IHsgaW5saW5lQ29sbGFwc2VkIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAodGhpcy5wcm9wcy5zaWRlckNvbGxhcHNlZCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wcm9wcy5zaWRlckNvbGxhcHNlZDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaW5saW5lQ29sbGFwc2VkO1xuICAgIH1cbiAgICBnZXRPcGVuTW90aW9uUHJvcHMobWVudU1vZGUpIHtcbiAgICAgICAgY29uc3QgeyBvcGVuVHJhbnNpdGlvbk5hbWUsIG9wZW5BbmltYXRpb24sIG1vdGlvbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgLy8gUHJvdmlkZXMgYnkgdXNlclxuICAgICAgICBpZiAobW90aW9uKSB7XG4gICAgICAgICAgICByZXR1cm4geyBtb3Rpb24gfTtcbiAgICAgICAgfVxuICAgICAgICBpZiAob3BlbkFuaW1hdGlvbikge1xuICAgICAgICAgICAgd2FybmluZyh0eXBlb2Ygb3BlbkFuaW1hdGlvbiA9PT0gJ3N0cmluZycsICdNZW51JywgJ2BvcGVuQW5pbWF0aW9uYCBkbyBub3Qgc3VwcG9ydCBvYmplY3QuIFBsZWFzZSB1c2UgYG1vdGlvbmAgaW5zdGVhZC4nKTtcbiAgICAgICAgICAgIHJldHVybiB7IG9wZW5BbmltYXRpb24gfTtcbiAgICAgICAgfVxuICAgICAgICBpZiAob3BlblRyYW5zaXRpb25OYW1lKSB7XG4gICAgICAgICAgICByZXR1cm4geyBvcGVuVHJhbnNpdGlvbk5hbWUgfTtcbiAgICAgICAgfVxuICAgICAgICAvLyBEZWZhdWx0IGxvZ2ljXG4gICAgICAgIGlmIChtZW51TW9kZSA9PT0gJ2hvcml6b250YWwnKSB7XG4gICAgICAgICAgICByZXR1cm4geyBtb3Rpb246IHsgbW90aW9uTmFtZTogJ3NsaWRlLXVwJyB9IH07XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG1lbnVNb2RlID09PSAnaW5saW5lJykge1xuICAgICAgICAgICAgcmV0dXJuIHsgbW90aW9uOiBjb2xsYXBzZU1vdGlvbiB9O1xuICAgICAgICB9XG4gICAgICAgIC8vIFdoZW4gbW9kZSBzd2l0Y2ggZnJvbSBpbmxpbmVcbiAgICAgICAgLy8gc3VibWVudSBzaG91bGQgaGlkZSB3aXRob3V0IGFuaW1hdGlvblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbW90aW9uOiB7XG4gICAgICAgICAgICAgICAgbW90aW9uTmFtZTogdGhpcy5zdGF0ZS5zd2l0Y2hpbmdNb2RlRnJvbUlubGluZSA/ICcnIDogJ3pvb20tYmlnJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlc3RvcmVNb2RlVmVydGljYWxGcm9tSW5saW5lKCkge1xuICAgICAgICBjb25zdCB7IHN3aXRjaGluZ01vZGVGcm9tSW5saW5lIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBpZiAoc3dpdGNoaW5nTW9kZUZyb21JbmxpbmUpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHN3aXRjaGluZ01vZGVGcm9tSW5saW5lOiBmYWxzZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuICg8TWVudUNvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3tcbiAgICAgICAgICAgIGlubGluZUNvbGxhcHNlZDogdGhpcy5nZXRJbmxpbmVDb2xsYXBzZWQoKSB8fCBmYWxzZSxcbiAgICAgICAgICAgIGFudGRNZW51VGhlbWU6IHRoaXMucHJvcHMudGhlbWUsXG4gICAgICAgIH19PlxuICAgICAgICA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyTWVudX08L0NvbmZpZ0NvbnN1bWVyPlxuICAgICAgPC9NZW51Q29udGV4dC5Qcm92aWRlcj4pO1xuICAgIH1cbn1cbkludGVybmFsTWVudS5kZWZhdWx0UHJvcHMgPSB7XG4gICAgY2xhc3NOYW1lOiAnJyxcbiAgICB0aGVtZTogJ2xpZ2h0JyxcbiAgICBmb2N1c2FibGU6IGZhbHNlLFxufTtcbnBvbHlmaWxsKEludGVybmFsTWVudSk7XG4vLyBXZSBzaG91bGQga2VlcCB0aGlzIGFzIHJlZi1hYmxlXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNZW51IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiAoPFNpZGVyQ29udGV4dC5Db25zdW1lcj5cbiAgICAgICAgeyhjb250ZXh0KSA9PiA8SW50ZXJuYWxNZW51IHsuLi50aGlzLnByb3BzfSB7Li4uY29udGV4dH0vPn1cbiAgICAgIDwvU2lkZXJDb250ZXh0LkNvbnN1bWVyPik7XG4gICAgfVxufVxuTWVudS5EaXZpZGVyID0gRGl2aWRlcjtcbk1lbnUuSXRlbSA9IEl0ZW07XG5NZW51LlN1Yk1lbnUgPSBTdWJNZW51O1xuTWVudS5JdGVtR3JvdXAgPSBJdGVtR3JvdXA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFOQTtBQUNBO0FBT0E7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWhCQTtBQUNBO0FBaUJBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFaQTtBQUNBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXBCQTtBQUNBO0FBcUJBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUE3RUE7QUFtRkE7QUFDQTs7O0FBNEJBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFNQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFYQTtBQUNBO0FBQ0E7QUFZQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFsQkE7QUFxQkE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBTUE7OztBQTlGQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBaEhBO0FBQ0E7QUFvTEE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBOzs7O0FBTEE7QUFDQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/menu/index.js
