/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var clazzUtil = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * // Scale class management
 * @module echarts/scale/Scale
 */

/**
 * @param {Object} [setting]
 */


function Scale(setting) {
  this._setting = setting || {};
  /**
   * Extent
   * @type {Array.<number>}
   * @protected
   */

  this._extent = [Infinity, -Infinity];
  /**
   * Step is calculated in adjustExtent
   * @type {Array.<number>}
   * @protected
   */

  this._interval = 0;
  this.init && this.init.apply(this, arguments);
}
/**
 * Parse input val to valid inner number.
 * @param {*} val
 * @return {number}
 */


Scale.prototype.parse = function (val) {
  // Notice: This would be a trap here, If the implementation
  // of this method depends on extent, and this method is used
  // before extent set (like in dataZoom), it would be wrong.
  // Nevertheless, parse does not depend on extent generally.
  return val;
};

Scale.prototype.getSetting = function (name) {
  return this._setting[name];
};

Scale.prototype.contain = function (val) {
  var extent = this._extent;
  return val >= extent[0] && val <= extent[1];
};
/**
 * Normalize value to linear [0, 1], return 0.5 if extent span is 0
 * @param {number} val
 * @return {number}
 */


Scale.prototype.normalize = function (val) {
  var extent = this._extent;

  if (extent[1] === extent[0]) {
    return 0.5;
  }

  return (val - extent[0]) / (extent[1] - extent[0]);
};
/**
 * Scale normalized value
 * @param {number} val
 * @return {number}
 */


Scale.prototype.scale = function (val) {
  var extent = this._extent;
  return val * (extent[1] - extent[0]) + extent[0];
};
/**
 * Set extent from data
 * @param {Array.<number>} other
 */


Scale.prototype.unionExtent = function (other) {
  var extent = this._extent;
  other[0] < extent[0] && (extent[0] = other[0]);
  other[1] > extent[1] && (extent[1] = other[1]); // not setExtent because in log axis it may transformed to power
  // this.setExtent(extent[0], extent[1]);
};
/**
 * Set extent from data
 * @param {module:echarts/data/List} data
 * @param {string} dim
 */


Scale.prototype.unionExtentFromData = function (data, dim) {
  this.unionExtent(data.getApproximateExtent(dim));
};
/**
 * Get extent
 * @return {Array.<number>}
 */


Scale.prototype.getExtent = function () {
  return this._extent.slice();
};
/**
 * Set extent
 * @param {number} start
 * @param {number} end
 */


Scale.prototype.setExtent = function (start, end) {
  var thisExtent = this._extent;

  if (!isNaN(start)) {
    thisExtent[0] = start;
  }

  if (!isNaN(end)) {
    thisExtent[1] = end;
  }
};
/**
 * When axis extent depends on data and no data exists,
 * axis ticks should not be drawn, which is named 'blank'.
 */


Scale.prototype.isBlank = function () {
  return this._isBlank;
},
/**
 * When axis extent depends on data and no data exists,
 * axis ticks should not be drawn, which is named 'blank'.
 */
Scale.prototype.setBlank = function (isBlank) {
  this._isBlank = isBlank;
};
/**
 * @abstract
 * @param {*} tick
 * @return {string} label of the tick.
 */

Scale.prototype.getLabel = null;
clazzUtil.enableClassExtend(Scale);
clazzUtil.enableClassManagement(Scale, {
  registerWhenExtend: true
});
var _default = Scale;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvU2NhbGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9zY2FsZS9TY2FsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIGNsYXp6VXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL2NsYXp6XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogLy8gU2NhbGUgY2xhc3MgbWFuYWdlbWVudFxuICogQG1vZHVsZSBlY2hhcnRzL3NjYWxlL1NjYWxlXG4gKi9cblxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gW3NldHRpbmddXG4gKi9cbmZ1bmN0aW9uIFNjYWxlKHNldHRpbmcpIHtcbiAgdGhpcy5fc2V0dGluZyA9IHNldHRpbmcgfHwge307XG4gIC8qKlxuICAgKiBFeHRlbnRcbiAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuXG4gIHRoaXMuX2V4dGVudCA9IFtJbmZpbml0eSwgLUluZmluaXR5XTtcbiAgLyoqXG4gICAqIFN0ZXAgaXMgY2FsY3VsYXRlZCBpbiBhZGp1c3RFeHRlbnRcbiAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuXG4gIHRoaXMuX2ludGVydmFsID0gMDtcbiAgdGhpcy5pbml0ICYmIHRoaXMuaW5pdC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufVxuLyoqXG4gKiBQYXJzZSBpbnB1dCB2YWwgdG8gdmFsaWQgaW5uZXIgbnVtYmVyLlxuICogQHBhcmFtIHsqfSB2YWxcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cblNjYWxlLnByb3RvdHlwZS5wYXJzZSA9IGZ1bmN0aW9uICh2YWwpIHtcbiAgLy8gTm90aWNlOiBUaGlzIHdvdWxkIGJlIGEgdHJhcCBoZXJlLCBJZiB0aGUgaW1wbGVtZW50YXRpb25cbiAgLy8gb2YgdGhpcyBtZXRob2QgZGVwZW5kcyBvbiBleHRlbnQsIGFuZCB0aGlzIG1ldGhvZCBpcyB1c2VkXG4gIC8vIGJlZm9yZSBleHRlbnQgc2V0IChsaWtlIGluIGRhdGFab29tKSwgaXQgd291bGQgYmUgd3JvbmcuXG4gIC8vIE5ldmVydGhlbGVzcywgcGFyc2UgZG9lcyBub3QgZGVwZW5kIG9uIGV4dGVudCBnZW5lcmFsbHkuXG4gIHJldHVybiB2YWw7XG59O1xuXG5TY2FsZS5wcm90b3R5cGUuZ2V0U2V0dGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gIHJldHVybiB0aGlzLl9zZXR0aW5nW25hbWVdO1xufTtcblxuU2NhbGUucHJvdG90eXBlLmNvbnRhaW4gPSBmdW5jdGlvbiAodmFsKSB7XG4gIHZhciBleHRlbnQgPSB0aGlzLl9leHRlbnQ7XG4gIHJldHVybiB2YWwgPj0gZXh0ZW50WzBdICYmIHZhbCA8PSBleHRlbnRbMV07XG59O1xuLyoqXG4gKiBOb3JtYWxpemUgdmFsdWUgdG8gbGluZWFyIFswLCAxXSwgcmV0dXJuIDAuNSBpZiBleHRlbnQgc3BhbiBpcyAwXG4gKiBAcGFyYW0ge251bWJlcn0gdmFsXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5TY2FsZS5wcm90b3R5cGUubm9ybWFsaXplID0gZnVuY3Rpb24gKHZhbCkge1xuICB2YXIgZXh0ZW50ID0gdGhpcy5fZXh0ZW50O1xuXG4gIGlmIChleHRlbnRbMV0gPT09IGV4dGVudFswXSkge1xuICAgIHJldHVybiAwLjU7XG4gIH1cblxuICByZXR1cm4gKHZhbCAtIGV4dGVudFswXSkgLyAoZXh0ZW50WzFdIC0gZXh0ZW50WzBdKTtcbn07XG4vKipcbiAqIFNjYWxlIG5vcm1hbGl6ZWQgdmFsdWVcbiAqIEBwYXJhbSB7bnVtYmVyfSB2YWxcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cblNjYWxlLnByb3RvdHlwZS5zY2FsZSA9IGZ1bmN0aW9uICh2YWwpIHtcbiAgdmFyIGV4dGVudCA9IHRoaXMuX2V4dGVudDtcbiAgcmV0dXJuIHZhbCAqIChleHRlbnRbMV0gLSBleHRlbnRbMF0pICsgZXh0ZW50WzBdO1xufTtcbi8qKlxuICogU2V0IGV4dGVudCBmcm9tIGRhdGFcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG90aGVyXG4gKi9cblxuXG5TY2FsZS5wcm90b3R5cGUudW5pb25FeHRlbnQgPSBmdW5jdGlvbiAob3RoZXIpIHtcbiAgdmFyIGV4dGVudCA9IHRoaXMuX2V4dGVudDtcbiAgb3RoZXJbMF0gPCBleHRlbnRbMF0gJiYgKGV4dGVudFswXSA9IG90aGVyWzBdKTtcbiAgb3RoZXJbMV0gPiBleHRlbnRbMV0gJiYgKGV4dGVudFsxXSA9IG90aGVyWzFdKTsgLy8gbm90IHNldEV4dGVudCBiZWNhdXNlIGluIGxvZyBheGlzIGl0IG1heSB0cmFuc2Zvcm1lZCB0byBwb3dlclxuICAvLyB0aGlzLnNldEV4dGVudChleHRlbnRbMF0sIGV4dGVudFsxXSk7XG59O1xuLyoqXG4gKiBTZXQgZXh0ZW50IGZyb20gZGF0YVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9IGRhdGFcbiAqIEBwYXJhbSB7c3RyaW5nfSBkaW1cbiAqL1xuXG5cblNjYWxlLnByb3RvdHlwZS51bmlvbkV4dGVudEZyb21EYXRhID0gZnVuY3Rpb24gKGRhdGEsIGRpbSkge1xuICB0aGlzLnVuaW9uRXh0ZW50KGRhdGEuZ2V0QXBwcm94aW1hdGVFeHRlbnQoZGltKSk7XG59O1xuLyoqXG4gKiBHZXQgZXh0ZW50XG4gKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAqL1xuXG5cblNjYWxlLnByb3RvdHlwZS5nZXRFeHRlbnQgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl9leHRlbnQuc2xpY2UoKTtcbn07XG4vKipcbiAqIFNldCBleHRlbnRcbiAqIEBwYXJhbSB7bnVtYmVyfSBzdGFydFxuICogQHBhcmFtIHtudW1iZXJ9IGVuZFxuICovXG5cblxuU2NhbGUucHJvdG90eXBlLnNldEV4dGVudCA9IGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gIHZhciB0aGlzRXh0ZW50ID0gdGhpcy5fZXh0ZW50O1xuXG4gIGlmICghaXNOYU4oc3RhcnQpKSB7XG4gICAgdGhpc0V4dGVudFswXSA9IHN0YXJ0O1xuICB9XG5cbiAgaWYgKCFpc05hTihlbmQpKSB7XG4gICAgdGhpc0V4dGVudFsxXSA9IGVuZDtcbiAgfVxufTtcbi8qKlxuICogV2hlbiBheGlzIGV4dGVudCBkZXBlbmRzIG9uIGRhdGEgYW5kIG5vIGRhdGEgZXhpc3RzLFxuICogYXhpcyB0aWNrcyBzaG91bGQgbm90IGJlIGRyYXduLCB3aGljaCBpcyBuYW1lZCAnYmxhbmsnLlxuICovXG5cblxuU2NhbGUucHJvdG90eXBlLmlzQmxhbmsgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLl9pc0JsYW5rO1xufSxcbi8qKlxuICogV2hlbiBheGlzIGV4dGVudCBkZXBlbmRzIG9uIGRhdGEgYW5kIG5vIGRhdGEgZXhpc3RzLFxuICogYXhpcyB0aWNrcyBzaG91bGQgbm90IGJlIGRyYXduLCB3aGljaCBpcyBuYW1lZCAnYmxhbmsnLlxuICovXG5TY2FsZS5wcm90b3R5cGUuc2V0QmxhbmsgPSBmdW5jdGlvbiAoaXNCbGFuaykge1xuICB0aGlzLl9pc0JsYW5rID0gaXNCbGFuaztcbn07XG4vKipcbiAqIEBhYnN0cmFjdFxuICogQHBhcmFtIHsqfSB0aWNrXG4gKiBAcmV0dXJuIHtzdHJpbmd9IGxhYmVsIG9mIHRoZSB0aWNrLlxuICovXG5cblNjYWxlLnByb3RvdHlwZS5nZXRMYWJlbCA9IG51bGw7XG5jbGF6elV0aWwuZW5hYmxlQ2xhc3NFeHRlbmQoU2NhbGUpO1xuY2xhenpVdGlsLmVuYWJsZUNsYXNzTWFuYWdlbWVudChTY2FsZSwge1xuICByZWdpc3RlcldoZW5FeHRlbmQ6IHRydWVcbn0pO1xudmFyIF9kZWZhdWx0ID0gU2NhbGU7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7OztBQUtBOzs7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/Scale.js
