var curve = __webpack_require__(/*! ./curve */ "./node_modules/zrender/lib/core/curve.js");

var vec2 = __webpack_require__(/*! ./vector */ "./node_modules/zrender/lib/core/vector.js");

var bbox = __webpack_require__(/*! ./bbox */ "./node_modules/zrender/lib/core/bbox.js");

var BoundingRect = __webpack_require__(/*! ./BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var _config = __webpack_require__(/*! ../config */ "./node_modules/zrender/lib/config.js");

var dpr = _config.devicePixelRatio;
/**
 * Path 代理，可以在`buildPath`中用于替代`ctx`, 会保存每个path操作的命令到pathCommands属性中
 * 可以用于 isInsidePath 判断以及获取boundingRect
 *
 * @module zrender/core/PathProxy
 * @author Yi Shen (http://www.github.com/pissang)
 */
// TODO getTotalLength, getPointAtLength

var CMD = {
  M: 1,
  L: 2,
  C: 3,
  Q: 4,
  A: 5,
  Z: 6,
  // Rect
  R: 7
}; // var CMD_MEM_SIZE = {
//     M: 3,
//     L: 3,
//     C: 7,
//     Q: 5,
//     A: 9,
//     R: 5,
//     Z: 1
// };

var min = [];
var max = [];
var min2 = [];
var max2 = [];
var mathMin = Math.min;
var mathMax = Math.max;
var mathCos = Math.cos;
var mathSin = Math.sin;
var mathSqrt = Math.sqrt;
var mathAbs = Math.abs;
var hasTypedArray = typeof Float32Array !== 'undefined';
/**
 * @alias module:zrender/core/PathProxy
 * @constructor
 */

var PathProxy = function PathProxy(notSaveData) {
  this._saveData = !(notSaveData || false);

  if (this._saveData) {
    /**
     * Path data. Stored as flat array
     * @type {Array.<Object>}
     */
    this.data = [];
  }

  this._ctx = null;
};
/**
 * 快速计算Path包围盒（并不是最小包围盒）
 * @return {Object}
 */


PathProxy.prototype = {
  constructor: PathProxy,
  _xi: 0,
  _yi: 0,
  _x0: 0,
  _y0: 0,
  // Unit x, Unit y. Provide for avoiding drawing that too short line segment
  _ux: 0,
  _uy: 0,
  _len: 0,
  _lineDash: null,
  _dashOffset: 0,
  _dashIdx: 0,
  _dashSum: 0,

  /**
   * @readOnly
   */
  setScale: function setScale(sx, sy) {
    this._ux = mathAbs(1 / dpr / sx) || 0;
    this._uy = mathAbs(1 / dpr / sy) || 0;
  },
  getContext: function getContext() {
    return this._ctx;
  },

  /**
   * @param  {CanvasRenderingContext2D} ctx
   * @return {module:zrender/core/PathProxy}
   */
  beginPath: function beginPath(ctx) {
    this._ctx = ctx;
    ctx && ctx.beginPath();
    ctx && (this.dpr = ctx.dpr); // Reset

    if (this._saveData) {
      this._len = 0;
    }

    if (this._lineDash) {
      this._lineDash = null;
      this._dashOffset = 0;
    }

    return this;
  },

  /**
   * @param  {number} x
   * @param  {number} y
   * @return {module:zrender/core/PathProxy}
   */
  moveTo: function moveTo(x, y) {
    this.addData(CMD.M, x, y);
    this._ctx && this._ctx.moveTo(x, y); // x0, y0, xi, yi 是记录在 _dashedXXXXTo 方法中使用
    // xi, yi 记录当前点, x0, y0 在 closePath 的时候回到起始点。
    // 有可能在 beginPath 之后直接调用 lineTo，这时候 x0, y0 需要
    // 在 lineTo 方法中记录，这里先不考虑这种情况，dashed line 也只在 IE10- 中不支持

    this._x0 = x;
    this._y0 = y;
    this._xi = x;
    this._yi = y;
    return this;
  },

  /**
   * @param  {number} x
   * @param  {number} y
   * @return {module:zrender/core/PathProxy}
   */
  lineTo: function lineTo(x, y) {
    var exceedUnit = mathAbs(x - this._xi) > this._ux || mathAbs(y - this._yi) > this._uy // Force draw the first segment
    || this._len < 5;
    this.addData(CMD.L, x, y);

    if (this._ctx && exceedUnit) {
      this._needsDash() ? this._dashedLineTo(x, y) : this._ctx.lineTo(x, y);
    }

    if (exceedUnit) {
      this._xi = x;
      this._yi = y;
    }

    return this;
  },

  /**
   * @param  {number} x1
   * @param  {number} y1
   * @param  {number} x2
   * @param  {number} y2
   * @param  {number} x3
   * @param  {number} y3
   * @return {module:zrender/core/PathProxy}
   */
  bezierCurveTo: function bezierCurveTo(x1, y1, x2, y2, x3, y3) {
    this.addData(CMD.C, x1, y1, x2, y2, x3, y3);

    if (this._ctx) {
      this._needsDash() ? this._dashedBezierTo(x1, y1, x2, y2, x3, y3) : this._ctx.bezierCurveTo(x1, y1, x2, y2, x3, y3);
    }

    this._xi = x3;
    this._yi = y3;
    return this;
  },

  /**
   * @param  {number} x1
   * @param  {number} y1
   * @param  {number} x2
   * @param  {number} y2
   * @return {module:zrender/core/PathProxy}
   */
  quadraticCurveTo: function quadraticCurveTo(x1, y1, x2, y2) {
    this.addData(CMD.Q, x1, y1, x2, y2);

    if (this._ctx) {
      this._needsDash() ? this._dashedQuadraticTo(x1, y1, x2, y2) : this._ctx.quadraticCurveTo(x1, y1, x2, y2);
    }

    this._xi = x2;
    this._yi = y2;
    return this;
  },

  /**
   * @param  {number} cx
   * @param  {number} cy
   * @param  {number} r
   * @param  {number} startAngle
   * @param  {number} endAngle
   * @param  {boolean} anticlockwise
   * @return {module:zrender/core/PathProxy}
   */
  arc: function arc(cx, cy, r, startAngle, endAngle, anticlockwise) {
    this.addData(CMD.A, cx, cy, r, r, startAngle, endAngle - startAngle, 0, anticlockwise ? 0 : 1);
    this._ctx && this._ctx.arc(cx, cy, r, startAngle, endAngle, anticlockwise);
    this._xi = mathCos(endAngle) * r + cx;
    this._yi = mathSin(endAngle) * r + cy;
    return this;
  },
  // TODO
  arcTo: function arcTo(x1, y1, x2, y2, radius) {
    if (this._ctx) {
      this._ctx.arcTo(x1, y1, x2, y2, radius);
    }

    return this;
  },
  // TODO
  rect: function rect(x, y, w, h) {
    this._ctx && this._ctx.rect(x, y, w, h);
    this.addData(CMD.R, x, y, w, h);
    return this;
  },

  /**
   * @return {module:zrender/core/PathProxy}
   */
  closePath: function closePath() {
    this.addData(CMD.Z);
    var ctx = this._ctx;
    var x0 = this._x0;
    var y0 = this._y0;

    if (ctx) {
      this._needsDash() && this._dashedLineTo(x0, y0);
      ctx.closePath();
    }

    this._xi = x0;
    this._yi = y0;
    return this;
  },

  /**
   * Context 从外部传入，因为有可能是 rebuildPath 完之后再 fill。
   * stroke 同样
   * @param {CanvasRenderingContext2D} ctx
   * @return {module:zrender/core/PathProxy}
   */
  fill: function fill(ctx) {
    ctx && ctx.fill();
    this.toStatic();
  },

  /**
   * @param {CanvasRenderingContext2D} ctx
   * @return {module:zrender/core/PathProxy}
   */
  stroke: function stroke(ctx) {
    ctx && ctx.stroke();
    this.toStatic();
  },

  /**
   * 必须在其它绘制命令前调用
   * Must be invoked before all other path drawing methods
   * @return {module:zrender/core/PathProxy}
   */
  setLineDash: function setLineDash(lineDash) {
    if (lineDash instanceof Array) {
      this._lineDash = lineDash;
      this._dashIdx = 0;
      var lineDashSum = 0;

      for (var i = 0; i < lineDash.length; i++) {
        lineDashSum += lineDash[i];
      }

      this._dashSum = lineDashSum;
    }

    return this;
  },

  /**
   * 必须在其它绘制命令前调用
   * Must be invoked before all other path drawing methods
   * @return {module:zrender/core/PathProxy}
   */
  setLineDashOffset: function setLineDashOffset(offset) {
    this._dashOffset = offset;
    return this;
  },

  /**
   *
   * @return {boolean}
   */
  len: function len() {
    return this._len;
  },

  /**
   * 直接设置 Path 数据
   */
  setData: function setData(data) {
    var len = data.length;

    if (!(this.data && this.data.length === len) && hasTypedArray) {
      this.data = new Float32Array(len);
    }

    for (var i = 0; i < len; i++) {
      this.data[i] = data[i];
    }

    this._len = len;
  },

  /**
   * 添加子路径
   * @param {module:zrender/core/PathProxy|Array.<module:zrender/core/PathProxy>} path
   */
  appendPath: function appendPath(path) {
    if (!(path instanceof Array)) {
      path = [path];
    }

    var len = path.length;
    var appendSize = 0;
    var offset = this._len;

    for (var i = 0; i < len; i++) {
      appendSize += path[i].len();
    }

    if (hasTypedArray && this.data instanceof Float32Array) {
      this.data = new Float32Array(offset + appendSize);
    }

    for (var i = 0; i < len; i++) {
      var appendPathData = path[i].data;

      for (var k = 0; k < appendPathData.length; k++) {
        this.data[offset++] = appendPathData[k];
      }
    }

    this._len = offset;
  },

  /**
   * 填充 Path 数据。
   * 尽量复用而不申明新的数组。大部分图形重绘的指令数据长度都是不变的。
   */
  addData: function addData(cmd) {
    if (!this._saveData) {
      return;
    }

    var data = this.data;

    if (this._len + arguments.length > data.length) {
      // 因为之前的数组已经转换成静态的 Float32Array
      // 所以不够用时需要扩展一个新的动态数组
      this._expandData();

      data = this.data;
    }

    for (var i = 0; i < arguments.length; i++) {
      data[this._len++] = arguments[i];
    }

    this._prevCmd = cmd;
  },
  _expandData: function _expandData() {
    // Only if data is Float32Array
    if (!(this.data instanceof Array)) {
      var newData = [];

      for (var i = 0; i < this._len; i++) {
        newData[i] = this.data[i];
      }

      this.data = newData;
    }
  },

  /**
   * If needs js implemented dashed line
   * @return {boolean}
   * @private
   */
  _needsDash: function _needsDash() {
    return this._lineDash;
  },
  _dashedLineTo: function _dashedLineTo(x1, y1) {
    var dashSum = this._dashSum;
    var offset = this._dashOffset;
    var lineDash = this._lineDash;
    var ctx = this._ctx;
    var x0 = this._xi;
    var y0 = this._yi;
    var dx = x1 - x0;
    var dy = y1 - y0;
    var dist = mathSqrt(dx * dx + dy * dy);
    var x = x0;
    var y = y0;
    var dash;
    var nDash = lineDash.length;
    var idx;
    dx /= dist;
    dy /= dist;

    if (offset < 0) {
      // Convert to positive offset
      offset = dashSum + offset;
    }

    offset %= dashSum;
    x -= offset * dx;
    y -= offset * dy;

    while (dx > 0 && x <= x1 || dx < 0 && x >= x1 || dx === 0 && (dy > 0 && y <= y1 || dy < 0 && y >= y1)) {
      idx = this._dashIdx;
      dash = lineDash[idx];
      x += dx * dash;
      y += dy * dash;
      this._dashIdx = (idx + 1) % nDash; // Skip positive offset

      if (dx > 0 && x < x0 || dx < 0 && x > x0 || dy > 0 && y < y0 || dy < 0 && y > y0) {
        continue;
      }

      ctx[idx % 2 ? 'moveTo' : 'lineTo'](dx >= 0 ? mathMin(x, x1) : mathMax(x, x1), dy >= 0 ? mathMin(y, y1) : mathMax(y, y1));
    } // Offset for next lineTo


    dx = x - x1;
    dy = y - y1;
    this._dashOffset = -mathSqrt(dx * dx + dy * dy);
  },
  // Not accurate dashed line to
  _dashedBezierTo: function _dashedBezierTo(x1, y1, x2, y2, x3, y3) {
    var dashSum = this._dashSum;
    var offset = this._dashOffset;
    var lineDash = this._lineDash;
    var ctx = this._ctx;
    var x0 = this._xi;
    var y0 = this._yi;
    var t;
    var dx;
    var dy;
    var cubicAt = curve.cubicAt;
    var bezierLen = 0;
    var idx = this._dashIdx;
    var nDash = lineDash.length;
    var x;
    var y;
    var tmpLen = 0;

    if (offset < 0) {
      // Convert to positive offset
      offset = dashSum + offset;
    }

    offset %= dashSum; // Bezier approx length

    for (t = 0; t < 1; t += 0.1) {
      dx = cubicAt(x0, x1, x2, x3, t + 0.1) - cubicAt(x0, x1, x2, x3, t);
      dy = cubicAt(y0, y1, y2, y3, t + 0.1) - cubicAt(y0, y1, y2, y3, t);
      bezierLen += mathSqrt(dx * dx + dy * dy);
    } // Find idx after add offset


    for (; idx < nDash; idx++) {
      tmpLen += lineDash[idx];

      if (tmpLen > offset) {
        break;
      }
    }

    t = (tmpLen - offset) / bezierLen;

    while (t <= 1) {
      x = cubicAt(x0, x1, x2, x3, t);
      y = cubicAt(y0, y1, y2, y3, t); // Use line to approximate dashed bezier
      // Bad result if dash is long

      idx % 2 ? ctx.moveTo(x, y) : ctx.lineTo(x, y);
      t += lineDash[idx] / bezierLen;
      idx = (idx + 1) % nDash;
    } // Finish the last segment and calculate the new offset


    idx % 2 !== 0 && ctx.lineTo(x3, y3);
    dx = x3 - x;
    dy = y3 - y;
    this._dashOffset = -mathSqrt(dx * dx + dy * dy);
  },
  _dashedQuadraticTo: function _dashedQuadraticTo(x1, y1, x2, y2) {
    // Convert quadratic to cubic using degree elevation
    var x3 = x2;
    var y3 = y2;
    x2 = (x2 + 2 * x1) / 3;
    y2 = (y2 + 2 * y1) / 3;
    x1 = (this._xi + 2 * x1) / 3;
    y1 = (this._yi + 2 * y1) / 3;

    this._dashedBezierTo(x1, y1, x2, y2, x3, y3);
  },

  /**
   * 转成静态的 Float32Array 减少堆内存占用
   * Convert dynamic array to static Float32Array
   */
  toStatic: function toStatic() {
    var data = this.data;

    if (data instanceof Array) {
      data.length = this._len;

      if (hasTypedArray) {
        this.data = new Float32Array(data);
      }
    }
  },

  /**
   * @return {module:zrender/core/BoundingRect}
   */
  getBoundingRect: function getBoundingRect() {
    min[0] = min[1] = min2[0] = min2[1] = Number.MAX_VALUE;
    max[0] = max[1] = max2[0] = max2[1] = -Number.MAX_VALUE;
    var data = this.data;
    var xi = 0;
    var yi = 0;
    var x0 = 0;
    var y0 = 0;

    for (var i = 0; i < data.length;) {
      var cmd = data[i++];

      if (i === 1) {
        // 如果第一个命令是 L, C, Q
        // 则 previous point 同绘制命令的第一个 point
        //
        // 第一个命令为 Arc 的情况下会在后面特殊处理
        xi = data[i];
        yi = data[i + 1];
        x0 = xi;
        y0 = yi;
      }

      switch (cmd) {
        case CMD.M:
          // moveTo 命令重新创建一个新的 subpath, 并且更新新的起点
          // 在 closePath 的时候使用
          x0 = data[i++];
          y0 = data[i++];
          xi = x0;
          yi = y0;
          min2[0] = x0;
          min2[1] = y0;
          max2[0] = x0;
          max2[1] = y0;
          break;

        case CMD.L:
          bbox.fromLine(xi, yi, data[i], data[i + 1], min2, max2);
          xi = data[i++];
          yi = data[i++];
          break;

        case CMD.C:
          bbox.fromCubic(xi, yi, data[i++], data[i++], data[i++], data[i++], data[i], data[i + 1], min2, max2);
          xi = data[i++];
          yi = data[i++];
          break;

        case CMD.Q:
          bbox.fromQuadratic(xi, yi, data[i++], data[i++], data[i], data[i + 1], min2, max2);
          xi = data[i++];
          yi = data[i++];
          break;

        case CMD.A:
          // TODO Arc 判断的开销比较大
          var cx = data[i++];
          var cy = data[i++];
          var rx = data[i++];
          var ry = data[i++];
          var startAngle = data[i++];
          var endAngle = data[i++] + startAngle; // TODO Arc 旋转

          i += 1;
          var anticlockwise = 1 - data[i++];

          if (i === 1) {
            // 直接使用 arc 命令
            // 第一个命令起点还未定义
            x0 = mathCos(startAngle) * rx + cx;
            y0 = mathSin(startAngle) * ry + cy;
          }

          bbox.fromArc(cx, cy, rx, ry, startAngle, endAngle, anticlockwise, min2, max2);
          xi = mathCos(endAngle) * rx + cx;
          yi = mathSin(endAngle) * ry + cy;
          break;

        case CMD.R:
          x0 = xi = data[i++];
          y0 = yi = data[i++];
          var width = data[i++];
          var height = data[i++]; // Use fromLine

          bbox.fromLine(x0, y0, x0 + width, y0 + height, min2, max2);
          break;

        case CMD.Z:
          xi = x0;
          yi = y0;
          break;
      } // Union


      vec2.min(min, min, min2);
      vec2.max(max, max, max2);
    } // No data


    if (i === 0) {
      min[0] = min[1] = max[0] = max[1] = 0;
    }

    return new BoundingRect(min[0], min[1], max[0] - min[0], max[1] - min[1]);
  },

  /**
   * Rebuild path from current data
   * Rebuild path will not consider javascript implemented line dash.
   * @param {CanvasRenderingContext2D} ctx
   */
  rebuildPath: function rebuildPath(ctx) {
    var d = this.data;
    var x0, y0;
    var xi, yi;
    var x, y;
    var ux = this._ux;
    var uy = this._uy;
    var len = this._len;

    for (var i = 0; i < len;) {
      var cmd = d[i++];

      if (i === 1) {
        // 如果第一个命令是 L, C, Q
        // 则 previous point 同绘制命令的第一个 point
        //
        // 第一个命令为 Arc 的情况下会在后面特殊处理
        xi = d[i];
        yi = d[i + 1];
        x0 = xi;
        y0 = yi;
      }

      switch (cmd) {
        case CMD.M:
          x0 = xi = d[i++];
          y0 = yi = d[i++];
          ctx.moveTo(xi, yi);
          break;

        case CMD.L:
          x = d[i++];
          y = d[i++]; // Not draw too small seg between

          if (mathAbs(x - xi) > ux || mathAbs(y - yi) > uy || i === len - 1) {
            ctx.lineTo(x, y);
            xi = x;
            yi = y;
          }

          break;

        case CMD.C:
          ctx.bezierCurveTo(d[i++], d[i++], d[i++], d[i++], d[i++], d[i++]);
          xi = d[i - 2];
          yi = d[i - 1];
          break;

        case CMD.Q:
          ctx.quadraticCurveTo(d[i++], d[i++], d[i++], d[i++]);
          xi = d[i - 2];
          yi = d[i - 1];
          break;

        case CMD.A:
          var cx = d[i++];
          var cy = d[i++];
          var rx = d[i++];
          var ry = d[i++];
          var theta = d[i++];
          var dTheta = d[i++];
          var psi = d[i++];
          var fs = d[i++];
          var r = rx > ry ? rx : ry;
          var scaleX = rx > ry ? 1 : rx / ry;
          var scaleY = rx > ry ? ry / rx : 1;
          var isEllipse = Math.abs(rx - ry) > 1e-3;
          var endAngle = theta + dTheta;

          if (isEllipse) {
            ctx.translate(cx, cy);
            ctx.rotate(psi);
            ctx.scale(scaleX, scaleY);
            ctx.arc(0, 0, r, theta, endAngle, 1 - fs);
            ctx.scale(1 / scaleX, 1 / scaleY);
            ctx.rotate(-psi);
            ctx.translate(-cx, -cy);
          } else {
            ctx.arc(cx, cy, r, theta, endAngle, 1 - fs);
          }

          if (i === 1) {
            // 直接使用 arc 命令
            // 第一个命令起点还未定义
            x0 = mathCos(theta) * rx + cx;
            y0 = mathSin(theta) * ry + cy;
          }

          xi = mathCos(endAngle) * rx + cx;
          yi = mathSin(endAngle) * ry + cy;
          break;

        case CMD.R:
          x0 = xi = d[i];
          y0 = yi = d[i + 1];
          ctx.rect(d[i++], d[i++], d[i++], d[i++]);
          break;

        case CMD.Z:
          ctx.closePath();
          xi = x0;
          yi = y0;
      }
    }
  }
};
PathProxy.CMD = CMD;
var _default = PathProxy;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9QYXRoUHJveHkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL1BhdGhQcm94eS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgY3VydmUgPSByZXF1aXJlKFwiLi9jdXJ2ZVwiKTtcblxudmFyIHZlYzIgPSByZXF1aXJlKFwiLi92ZWN0b3JcIik7XG5cbnZhciBiYm94ID0gcmVxdWlyZShcIi4vYmJveFwiKTtcblxudmFyIEJvdW5kaW5nUmVjdCA9IHJlcXVpcmUoXCIuL0JvdW5kaW5nUmVjdFwiKTtcblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vY29uZmlnXCIpO1xuXG52YXIgZHByID0gX2NvbmZpZy5kZXZpY2VQaXhlbFJhdGlvO1xuXG4vKipcbiAqIFBhdGgg5Luj55CG77yM5Y+v5Lul5ZyoYGJ1aWxkUGF0aGDkuK3nlKjkuo7mm7/ku6NgY3R4YCwg5Lya5L+d5a2Y5q+P5LiqcGF0aOaTjeS9nOeahOWRveS7pOWIsHBhdGhDb21tYW5kc+WxnuaAp+S4rVxuICog5Y+v5Lul55So5LqOIGlzSW5zaWRlUGF0aCDliKTmlq3ku6Xlj4rojrflj5Zib3VuZGluZ1JlY3RcbiAqXG4gKiBAbW9kdWxlIHpyZW5kZXIvY29yZS9QYXRoUHJveHlcbiAqIEBhdXRob3IgWWkgU2hlbiAoaHR0cDovL3d3dy5naXRodWIuY29tL3Bpc3NhbmcpXG4gKi9cbi8vIFRPRE8gZ2V0VG90YWxMZW5ndGgsIGdldFBvaW50QXRMZW5ndGhcbnZhciBDTUQgPSB7XG4gIE06IDEsXG4gIEw6IDIsXG4gIEM6IDMsXG4gIFE6IDQsXG4gIEE6IDUsXG4gIFo6IDYsXG4gIC8vIFJlY3RcbiAgUjogN1xufTsgLy8gdmFyIENNRF9NRU1fU0laRSA9IHtcbi8vICAgICBNOiAzLFxuLy8gICAgIEw6IDMsXG4vLyAgICAgQzogNyxcbi8vICAgICBROiA1LFxuLy8gICAgIEE6IDksXG4vLyAgICAgUjogNSxcbi8vICAgICBaOiAxXG4vLyB9O1xuXG52YXIgbWluID0gW107XG52YXIgbWF4ID0gW107XG52YXIgbWluMiA9IFtdO1xudmFyIG1heDIgPSBbXTtcbnZhciBtYXRoTWluID0gTWF0aC5taW47XG52YXIgbWF0aE1heCA9IE1hdGgubWF4O1xudmFyIG1hdGhDb3MgPSBNYXRoLmNvcztcbnZhciBtYXRoU2luID0gTWF0aC5zaW47XG52YXIgbWF0aFNxcnQgPSBNYXRoLnNxcnQ7XG52YXIgbWF0aEFicyA9IE1hdGguYWJzO1xudmFyIGhhc1R5cGVkQXJyYXkgPSB0eXBlb2YgRmxvYXQzMkFycmF5ICE9PSAndW5kZWZpbmVkJztcbi8qKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL2NvcmUvUGF0aFByb3h5XG4gKiBAY29uc3RydWN0b3JcbiAqL1xuXG52YXIgUGF0aFByb3h5ID0gZnVuY3Rpb24gKG5vdFNhdmVEYXRhKSB7XG4gIHRoaXMuX3NhdmVEYXRhID0gIShub3RTYXZlRGF0YSB8fCBmYWxzZSk7XG5cbiAgaWYgKHRoaXMuX3NhdmVEYXRhKSB7XG4gICAgLyoqXG4gICAgICogUGF0aCBkYXRhLiBTdG9yZWQgYXMgZmxhdCBhcnJheVxuICAgICAqIEB0eXBlIHtBcnJheS48T2JqZWN0Pn1cbiAgICAgKi9cbiAgICB0aGlzLmRhdGEgPSBbXTtcbiAgfVxuXG4gIHRoaXMuX2N0eCA9IG51bGw7XG59O1xuLyoqXG4gKiDlv6vpgJ/orqHnrpdQYXRo5YyF5Zu055uS77yI5bm25LiN5piv5pyA5bCP5YyF5Zu055uS77yJXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cblxuXG5QYXRoUHJveHkucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogUGF0aFByb3h5LFxuICBfeGk6IDAsXG4gIF95aTogMCxcbiAgX3gwOiAwLFxuICBfeTA6IDAsXG4gIC8vIFVuaXQgeCwgVW5pdCB5LiBQcm92aWRlIGZvciBhdm9pZGluZyBkcmF3aW5nIHRoYXQgdG9vIHNob3J0IGxpbmUgc2VnbWVudFxuICBfdXg6IDAsXG4gIF91eTogMCxcbiAgX2xlbjogMCxcbiAgX2xpbmVEYXNoOiBudWxsLFxuICBfZGFzaE9mZnNldDogMCxcbiAgX2Rhc2hJZHg6IDAsXG4gIF9kYXNoU3VtOiAwLFxuXG4gIC8qKlxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIHNldFNjYWxlOiBmdW5jdGlvbiAoc3gsIHN5KSB7XG4gICAgdGhpcy5fdXggPSBtYXRoQWJzKDEgLyBkcHIgLyBzeCkgfHwgMDtcbiAgICB0aGlzLl91eSA9IG1hdGhBYnMoMSAvIGRwciAvIHN5KSB8fCAwO1xuICB9LFxuICBnZXRDb250ZXh0OiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2N0eDtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtICB7Q2FudmFzUmVuZGVyaW5nQ29udGV4dDJEfSBjdHhcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9QYXRoUHJveHl9XG4gICAqL1xuICBiZWdpblBhdGg6IGZ1bmN0aW9uIChjdHgpIHtcbiAgICB0aGlzLl9jdHggPSBjdHg7XG4gICAgY3R4ICYmIGN0eC5iZWdpblBhdGgoKTtcbiAgICBjdHggJiYgKHRoaXMuZHByID0gY3R4LmRwcik7IC8vIFJlc2V0XG5cbiAgICBpZiAodGhpcy5fc2F2ZURhdGEpIHtcbiAgICAgIHRoaXMuX2xlbiA9IDA7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2xpbmVEYXNoKSB7XG4gICAgICB0aGlzLl9saW5lRGFzaCA9IG51bGw7XG4gICAgICB0aGlzLl9kYXNoT2Zmc2V0ID0gMDtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtICB7bnVtYmVyfSB4XG4gICAqIEBwYXJhbSAge251bWJlcn0geVxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL1BhdGhQcm94eX1cbiAgICovXG4gIG1vdmVUbzogZnVuY3Rpb24gKHgsIHkpIHtcbiAgICB0aGlzLmFkZERhdGEoQ01ELk0sIHgsIHkpO1xuICAgIHRoaXMuX2N0eCAmJiB0aGlzLl9jdHgubW92ZVRvKHgsIHkpOyAvLyB4MCwgeTAsIHhpLCB5aSDmmK/orrDlvZXlnKggX2Rhc2hlZFhYWFhUbyDmlrnms5XkuK3kvb/nlKhcbiAgICAvLyB4aSwgeWkg6K6w5b2V5b2T5YmN54K5LCB4MCwgeTAg5ZyoIGNsb3NlUGF0aCDnmoTml7blgJnlm57liLDotbflp4vngrnjgIJcbiAgICAvLyDmnInlj6/og73lnKggYmVnaW5QYXRoIOS5i+WQjuebtOaOpeiwg+eUqCBsaW5lVG/vvIzov5nml7blgJkgeDAsIHkwIOmcgOimgVxuICAgIC8vIOWcqCBsaW5lVG8g5pa55rOV5Lit6K6w5b2V77yM6L+Z6YeM5YWI5LiN6ICD6JmR6L+Z56eN5oOF5Ya177yMZGFzaGVkIGxpbmUg5Lmf5Y+q5ZyoIElFMTAtIOS4reS4jeaUr+aMgVxuXG4gICAgdGhpcy5feDAgPSB4O1xuICAgIHRoaXMuX3kwID0geTtcbiAgICB0aGlzLl94aSA9IHg7XG4gICAgdGhpcy5feWkgPSB5O1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHhcbiAgICogQHBhcmFtICB7bnVtYmVyfSB5XG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvUGF0aFByb3h5fVxuICAgKi9cbiAgbGluZVRvOiBmdW5jdGlvbiAoeCwgeSkge1xuICAgIHZhciBleGNlZWRVbml0ID0gbWF0aEFicyh4IC0gdGhpcy5feGkpID4gdGhpcy5fdXggfHwgbWF0aEFicyh5IC0gdGhpcy5feWkpID4gdGhpcy5fdXkgLy8gRm9yY2UgZHJhdyB0aGUgZmlyc3Qgc2VnbWVudFxuICAgIHx8IHRoaXMuX2xlbiA8IDU7XG4gICAgdGhpcy5hZGREYXRhKENNRC5MLCB4LCB5KTtcblxuICAgIGlmICh0aGlzLl9jdHggJiYgZXhjZWVkVW5pdCkge1xuICAgICAgdGhpcy5fbmVlZHNEYXNoKCkgPyB0aGlzLl9kYXNoZWRMaW5lVG8oeCwgeSkgOiB0aGlzLl9jdHgubGluZVRvKHgsIHkpO1xuICAgIH1cblxuICAgIGlmIChleGNlZWRVbml0KSB7XG4gICAgICB0aGlzLl94aSA9IHg7XG4gICAgICB0aGlzLl95aSA9IHk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSAge251bWJlcn0geDFcbiAgICogQHBhcmFtICB7bnVtYmVyfSB5MVxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHgyXG4gICAqIEBwYXJhbSAge251bWJlcn0geTJcbiAgICogQHBhcmFtICB7bnVtYmVyfSB4M1xuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHkzXG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvUGF0aFByb3h5fVxuICAgKi9cbiAgYmV6aWVyQ3VydmVUbzogZnVuY3Rpb24gKHgxLCB5MSwgeDIsIHkyLCB4MywgeTMpIHtcbiAgICB0aGlzLmFkZERhdGEoQ01ELkMsIHgxLCB5MSwgeDIsIHkyLCB4MywgeTMpO1xuXG4gICAgaWYgKHRoaXMuX2N0eCkge1xuICAgICAgdGhpcy5fbmVlZHNEYXNoKCkgPyB0aGlzLl9kYXNoZWRCZXppZXJUbyh4MSwgeTEsIHgyLCB5MiwgeDMsIHkzKSA6IHRoaXMuX2N0eC5iZXppZXJDdXJ2ZVRvKHgxLCB5MSwgeDIsIHkyLCB4MywgeTMpO1xuICAgIH1cblxuICAgIHRoaXMuX3hpID0geDM7XG4gICAgdGhpcy5feWkgPSB5MztcbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtICB7bnVtYmVyfSB4MVxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHkxXG4gICAqIEBwYXJhbSAge251bWJlcn0geDJcbiAgICogQHBhcmFtICB7bnVtYmVyfSB5MlxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL1BhdGhQcm94eX1cbiAgICovXG4gIHF1YWRyYXRpY0N1cnZlVG86IGZ1bmN0aW9uICh4MSwgeTEsIHgyLCB5Mikge1xuICAgIHRoaXMuYWRkRGF0YShDTUQuUSwgeDEsIHkxLCB4MiwgeTIpO1xuXG4gICAgaWYgKHRoaXMuX2N0eCkge1xuICAgICAgdGhpcy5fbmVlZHNEYXNoKCkgPyB0aGlzLl9kYXNoZWRRdWFkcmF0aWNUbyh4MSwgeTEsIHgyLCB5MikgOiB0aGlzLl9jdHgucXVhZHJhdGljQ3VydmVUbyh4MSwgeTEsIHgyLCB5Mik7XG4gICAgfVxuXG4gICAgdGhpcy5feGkgPSB4MjtcbiAgICB0aGlzLl95aSA9IHkyO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IGN4XG4gICAqIEBwYXJhbSAge251bWJlcn0gY3lcbiAgICogQHBhcmFtICB7bnVtYmVyfSByXG4gICAqIEBwYXJhbSAge251bWJlcn0gc3RhcnRBbmdsZVxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IGVuZEFuZ2xlXG4gICAqIEBwYXJhbSAge2Jvb2xlYW59IGFudGljbG9ja3dpc2VcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9QYXRoUHJveHl9XG4gICAqL1xuICBhcmM6IGZ1bmN0aW9uIChjeCwgY3ksIHIsIHN0YXJ0QW5nbGUsIGVuZEFuZ2xlLCBhbnRpY2xvY2t3aXNlKSB7XG4gICAgdGhpcy5hZGREYXRhKENNRC5BLCBjeCwgY3ksIHIsIHIsIHN0YXJ0QW5nbGUsIGVuZEFuZ2xlIC0gc3RhcnRBbmdsZSwgMCwgYW50aWNsb2Nrd2lzZSA/IDAgOiAxKTtcbiAgICB0aGlzLl9jdHggJiYgdGhpcy5fY3R4LmFyYyhjeCwgY3ksIHIsIHN0YXJ0QW5nbGUsIGVuZEFuZ2xlLCBhbnRpY2xvY2t3aXNlKTtcbiAgICB0aGlzLl94aSA9IG1hdGhDb3MoZW5kQW5nbGUpICogciArIGN4O1xuICAgIHRoaXMuX3lpID0gbWF0aFNpbihlbmRBbmdsZSkgKiByICsgY3k7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG4gIC8vIFRPRE9cbiAgYXJjVG86IGZ1bmN0aW9uICh4MSwgeTEsIHgyLCB5MiwgcmFkaXVzKSB7XG4gICAgaWYgKHRoaXMuX2N0eCkge1xuICAgICAgdGhpcy5fY3R4LmFyY1RvKHgxLCB5MSwgeDIsIHkyLCByYWRpdXMpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuICAvLyBUT0RPXG4gIHJlY3Q6IGZ1bmN0aW9uICh4LCB5LCB3LCBoKSB7XG4gICAgdGhpcy5fY3R4ICYmIHRoaXMuX2N0eC5yZWN0KHgsIHksIHcsIGgpO1xuICAgIHRoaXMuYWRkRGF0YShDTUQuUiwgeCwgeSwgdywgaCk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvUGF0aFByb3h5fVxuICAgKi9cbiAgY2xvc2VQYXRoOiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5hZGREYXRhKENNRC5aKTtcbiAgICB2YXIgY3R4ID0gdGhpcy5fY3R4O1xuICAgIHZhciB4MCA9IHRoaXMuX3gwO1xuICAgIHZhciB5MCA9IHRoaXMuX3kwO1xuXG4gICAgaWYgKGN0eCkge1xuICAgICAgdGhpcy5fbmVlZHNEYXNoKCkgJiYgdGhpcy5fZGFzaGVkTGluZVRvKHgwLCB5MCk7XG4gICAgICBjdHguY2xvc2VQYXRoKCk7XG4gICAgfVxuXG4gICAgdGhpcy5feGkgPSB4MDtcbiAgICB0aGlzLl95aSA9IHkwO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDb250ZXh0IOS7juWklumDqOS8oOWFpe+8jOWboOS4uuacieWPr+iDveaYryByZWJ1aWxkUGF0aCDlrozkuYvlkI7lho0gZmlsbOOAglxuICAgKiBzdHJva2Ug5ZCM5qC3XG4gICAqIEBwYXJhbSB7Q2FudmFzUmVuZGVyaW5nQ29udGV4dDJEfSBjdHhcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9QYXRoUHJveHl9XG4gICAqL1xuICBmaWxsOiBmdW5jdGlvbiAoY3R4KSB7XG4gICAgY3R4ICYmIGN0eC5maWxsKCk7XG4gICAgdGhpcy50b1N0YXRpYygpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0NhbnZhc1JlbmRlcmluZ0NvbnRleHQyRH0gY3R4XG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvUGF0aFByb3h5fVxuICAgKi9cbiAgc3Ryb2tlOiBmdW5jdGlvbiAoY3R4KSB7XG4gICAgY3R4ICYmIGN0eC5zdHJva2UoKTtcbiAgICB0aGlzLnRvU3RhdGljKCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOW/hemhu+WcqOWFtuWug+e7mOWItuWRveS7pOWJjeiwg+eUqFxuICAgKiBNdXN0IGJlIGludm9rZWQgYmVmb3JlIGFsbCBvdGhlciBwYXRoIGRyYXdpbmcgbWV0aG9kc1xuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL1BhdGhQcm94eX1cbiAgICovXG4gIHNldExpbmVEYXNoOiBmdW5jdGlvbiAobGluZURhc2gpIHtcbiAgICBpZiAobGluZURhc2ggaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgdGhpcy5fbGluZURhc2ggPSBsaW5lRGFzaDtcbiAgICAgIHRoaXMuX2Rhc2hJZHggPSAwO1xuICAgICAgdmFyIGxpbmVEYXNoU3VtID0gMDtcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaW5lRGFzaC5sZW5ndGg7IGkrKykge1xuICAgICAgICBsaW5lRGFzaFN1bSArPSBsaW5lRGFzaFtpXTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5fZGFzaFN1bSA9IGxpbmVEYXNoU3VtO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiDlv4XpobvlnKjlhbblroPnu5jliLblkb3ku6TliY3osIPnlKhcbiAgICogTXVzdCBiZSBpbnZva2VkIGJlZm9yZSBhbGwgb3RoZXIgcGF0aCBkcmF3aW5nIG1ldGhvZHNcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9QYXRoUHJveHl9XG4gICAqL1xuICBzZXRMaW5lRGFzaE9mZnNldDogZnVuY3Rpb24gKG9mZnNldCkge1xuICAgIHRoaXMuX2Rhc2hPZmZzZXQgPSBvZmZzZXQ7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBsZW46IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fbGVuO1xuICB9LFxuXG4gIC8qKlxuICAgKiDnm7TmjqXorr7nva4gUGF0aCDmlbDmja5cbiAgICovXG4gIHNldERhdGE6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgdmFyIGxlbiA9IGRhdGEubGVuZ3RoO1xuXG4gICAgaWYgKCEodGhpcy5kYXRhICYmIHRoaXMuZGF0YS5sZW5ndGggPT09IGxlbikgJiYgaGFzVHlwZWRBcnJheSkge1xuICAgICAgdGhpcy5kYXRhID0gbmV3IEZsb2F0MzJBcnJheShsZW4pO1xuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIHRoaXMuZGF0YVtpXSA9IGRhdGFbaV07XG4gICAgfVxuXG4gICAgdGhpcy5fbGVuID0gbGVuO1xuICB9LFxuXG4gIC8qKlxuICAgKiDmt7vliqDlrZDot6/lvoRcbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9jb3JlL1BhdGhQcm94eXxBcnJheS48bW9kdWxlOnpyZW5kZXIvY29yZS9QYXRoUHJveHk+fSBwYXRoXG4gICAqL1xuICBhcHBlbmRQYXRoOiBmdW5jdGlvbiAocGF0aCkge1xuICAgIGlmICghKHBhdGggaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgIHBhdGggPSBbcGF0aF07XG4gICAgfVxuXG4gICAgdmFyIGxlbiA9IHBhdGgubGVuZ3RoO1xuICAgIHZhciBhcHBlbmRTaXplID0gMDtcbiAgICB2YXIgb2Zmc2V0ID0gdGhpcy5fbGVuO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgYXBwZW5kU2l6ZSArPSBwYXRoW2ldLmxlbigpO1xuICAgIH1cblxuICAgIGlmIChoYXNUeXBlZEFycmF5ICYmIHRoaXMuZGF0YSBpbnN0YW5jZW9mIEZsb2F0MzJBcnJheSkge1xuICAgICAgdGhpcy5kYXRhID0gbmV3IEZsb2F0MzJBcnJheShvZmZzZXQgKyBhcHBlbmRTaXplKTtcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICB2YXIgYXBwZW5kUGF0aERhdGEgPSBwYXRoW2ldLmRhdGE7XG5cbiAgICAgIGZvciAodmFyIGsgPSAwOyBrIDwgYXBwZW5kUGF0aERhdGEubGVuZ3RoOyBrKyspIHtcbiAgICAgICAgdGhpcy5kYXRhW29mZnNldCsrXSA9IGFwcGVuZFBhdGhEYXRhW2tdO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuX2xlbiA9IG9mZnNldDtcbiAgfSxcblxuICAvKipcbiAgICog5aGr5YWFIFBhdGgg5pWw5o2u44CCXG4gICAqIOWwvemHj+WkjeeUqOiAjOS4jeeUs+aYjuaWsOeahOaVsOe7hOOAguWkp+mDqOWIhuWbvuW9oumHjee7mOeahOaMh+S7pOaVsOaNrumVv+W6pumDveaYr+S4jeWPmOeahOOAglxuICAgKi9cbiAgYWRkRGF0YTogZnVuY3Rpb24gKGNtZCkge1xuICAgIGlmICghdGhpcy5fc2F2ZURhdGEpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZGF0YSA9IHRoaXMuZGF0YTtcblxuICAgIGlmICh0aGlzLl9sZW4gKyBhcmd1bWVudHMubGVuZ3RoID4gZGF0YS5sZW5ndGgpIHtcbiAgICAgIC8vIOWboOS4uuS5i+WJjeeahOaVsOe7hOW3sue7j+i9rOaNouaIkOmdmeaAgeeahCBGbG9hdDMyQXJyYXlcbiAgICAgIC8vIOaJgOS7peS4jeWkn+eUqOaXtumcgOimgeaJqeWxleS4gOS4quaWsOeahOWKqOaAgeaVsOe7hFxuICAgICAgdGhpcy5fZXhwYW5kRGF0YSgpO1xuXG4gICAgICBkYXRhID0gdGhpcy5kYXRhO1xuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBkYXRhW3RoaXMuX2xlbisrXSA9IGFyZ3VtZW50c1tpXTtcbiAgICB9XG5cbiAgICB0aGlzLl9wcmV2Q21kID0gY21kO1xuICB9LFxuICBfZXhwYW5kRGF0YTogZnVuY3Rpb24gKCkge1xuICAgIC8vIE9ubHkgaWYgZGF0YSBpcyBGbG9hdDMyQXJyYXlcbiAgICBpZiAoISh0aGlzLmRhdGEgaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgIHZhciBuZXdEYXRhID0gW107XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fbGVuOyBpKyspIHtcbiAgICAgICAgbmV3RGF0YVtpXSA9IHRoaXMuZGF0YVtpXTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5kYXRhID0gbmV3RGF0YTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIElmIG5lZWRzIGpzIGltcGxlbWVudGVkIGRhc2hlZCBsaW5lXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfbmVlZHNEYXNoOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2xpbmVEYXNoO1xuICB9LFxuICBfZGFzaGVkTGluZVRvOiBmdW5jdGlvbiAoeDEsIHkxKSB7XG4gICAgdmFyIGRhc2hTdW0gPSB0aGlzLl9kYXNoU3VtO1xuICAgIHZhciBvZmZzZXQgPSB0aGlzLl9kYXNoT2Zmc2V0O1xuICAgIHZhciBsaW5lRGFzaCA9IHRoaXMuX2xpbmVEYXNoO1xuICAgIHZhciBjdHggPSB0aGlzLl9jdHg7XG4gICAgdmFyIHgwID0gdGhpcy5feGk7XG4gICAgdmFyIHkwID0gdGhpcy5feWk7XG4gICAgdmFyIGR4ID0geDEgLSB4MDtcbiAgICB2YXIgZHkgPSB5MSAtIHkwO1xuICAgIHZhciBkaXN0ID0gbWF0aFNxcnQoZHggKiBkeCArIGR5ICogZHkpO1xuICAgIHZhciB4ID0geDA7XG4gICAgdmFyIHkgPSB5MDtcbiAgICB2YXIgZGFzaDtcbiAgICB2YXIgbkRhc2ggPSBsaW5lRGFzaC5sZW5ndGg7XG4gICAgdmFyIGlkeDtcbiAgICBkeCAvPSBkaXN0O1xuICAgIGR5IC89IGRpc3Q7XG5cbiAgICBpZiAob2Zmc2V0IDwgMCkge1xuICAgICAgLy8gQ29udmVydCB0byBwb3NpdGl2ZSBvZmZzZXRcbiAgICAgIG9mZnNldCA9IGRhc2hTdW0gKyBvZmZzZXQ7XG4gICAgfVxuXG4gICAgb2Zmc2V0ICU9IGRhc2hTdW07XG4gICAgeCAtPSBvZmZzZXQgKiBkeDtcbiAgICB5IC09IG9mZnNldCAqIGR5O1xuXG4gICAgd2hpbGUgKGR4ID4gMCAmJiB4IDw9IHgxIHx8IGR4IDwgMCAmJiB4ID49IHgxIHx8IGR4ID09PSAwICYmIChkeSA+IDAgJiYgeSA8PSB5MSB8fCBkeSA8IDAgJiYgeSA+PSB5MSkpIHtcbiAgICAgIGlkeCA9IHRoaXMuX2Rhc2hJZHg7XG4gICAgICBkYXNoID0gbGluZURhc2hbaWR4XTtcbiAgICAgIHggKz0gZHggKiBkYXNoO1xuICAgICAgeSArPSBkeSAqIGRhc2g7XG4gICAgICB0aGlzLl9kYXNoSWR4ID0gKGlkeCArIDEpICUgbkRhc2g7IC8vIFNraXAgcG9zaXRpdmUgb2Zmc2V0XG5cbiAgICAgIGlmIChkeCA+IDAgJiYgeCA8IHgwIHx8IGR4IDwgMCAmJiB4ID4geDAgfHwgZHkgPiAwICYmIHkgPCB5MCB8fCBkeSA8IDAgJiYgeSA+IHkwKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBjdHhbaWR4ICUgMiA/ICdtb3ZlVG8nIDogJ2xpbmVUbyddKGR4ID49IDAgPyBtYXRoTWluKHgsIHgxKSA6IG1hdGhNYXgoeCwgeDEpLCBkeSA+PSAwID8gbWF0aE1pbih5LCB5MSkgOiBtYXRoTWF4KHksIHkxKSk7XG4gICAgfSAvLyBPZmZzZXQgZm9yIG5leHQgbGluZVRvXG5cblxuICAgIGR4ID0geCAtIHgxO1xuICAgIGR5ID0geSAtIHkxO1xuICAgIHRoaXMuX2Rhc2hPZmZzZXQgPSAtbWF0aFNxcnQoZHggKiBkeCArIGR5ICogZHkpO1xuICB9LFxuICAvLyBOb3QgYWNjdXJhdGUgZGFzaGVkIGxpbmUgdG9cbiAgX2Rhc2hlZEJlemllclRvOiBmdW5jdGlvbiAoeDEsIHkxLCB4MiwgeTIsIHgzLCB5Mykge1xuICAgIHZhciBkYXNoU3VtID0gdGhpcy5fZGFzaFN1bTtcbiAgICB2YXIgb2Zmc2V0ID0gdGhpcy5fZGFzaE9mZnNldDtcbiAgICB2YXIgbGluZURhc2ggPSB0aGlzLl9saW5lRGFzaDtcbiAgICB2YXIgY3R4ID0gdGhpcy5fY3R4O1xuICAgIHZhciB4MCA9IHRoaXMuX3hpO1xuICAgIHZhciB5MCA9IHRoaXMuX3lpO1xuICAgIHZhciB0O1xuICAgIHZhciBkeDtcbiAgICB2YXIgZHk7XG4gICAgdmFyIGN1YmljQXQgPSBjdXJ2ZS5jdWJpY0F0O1xuICAgIHZhciBiZXppZXJMZW4gPSAwO1xuICAgIHZhciBpZHggPSB0aGlzLl9kYXNoSWR4O1xuICAgIHZhciBuRGFzaCA9IGxpbmVEYXNoLmxlbmd0aDtcbiAgICB2YXIgeDtcbiAgICB2YXIgeTtcbiAgICB2YXIgdG1wTGVuID0gMDtcblxuICAgIGlmIChvZmZzZXQgPCAwKSB7XG4gICAgICAvLyBDb252ZXJ0IHRvIHBvc2l0aXZlIG9mZnNldFxuICAgICAgb2Zmc2V0ID0gZGFzaFN1bSArIG9mZnNldDtcbiAgICB9XG5cbiAgICBvZmZzZXQgJT0gZGFzaFN1bTsgLy8gQmV6aWVyIGFwcHJveCBsZW5ndGhcblxuICAgIGZvciAodCA9IDA7IHQgPCAxOyB0ICs9IDAuMSkge1xuICAgICAgZHggPSBjdWJpY0F0KHgwLCB4MSwgeDIsIHgzLCB0ICsgMC4xKSAtIGN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIHQpO1xuICAgICAgZHkgPSBjdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCB0ICsgMC4xKSAtIGN1YmljQXQoeTAsIHkxLCB5MiwgeTMsIHQpO1xuICAgICAgYmV6aWVyTGVuICs9IG1hdGhTcXJ0KGR4ICogZHggKyBkeSAqIGR5KTtcbiAgICB9IC8vIEZpbmQgaWR4IGFmdGVyIGFkZCBvZmZzZXRcblxuXG4gICAgZm9yICg7IGlkeCA8IG5EYXNoOyBpZHgrKykge1xuICAgICAgdG1wTGVuICs9IGxpbmVEYXNoW2lkeF07XG5cbiAgICAgIGlmICh0bXBMZW4gPiBvZmZzZXQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdCA9ICh0bXBMZW4gLSBvZmZzZXQpIC8gYmV6aWVyTGVuO1xuXG4gICAgd2hpbGUgKHQgPD0gMSkge1xuICAgICAgeCA9IGN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIHQpO1xuICAgICAgeSA9IGN1YmljQXQoeTAsIHkxLCB5MiwgeTMsIHQpOyAvLyBVc2UgbGluZSB0byBhcHByb3hpbWF0ZSBkYXNoZWQgYmV6aWVyXG4gICAgICAvLyBCYWQgcmVzdWx0IGlmIGRhc2ggaXMgbG9uZ1xuXG4gICAgICBpZHggJSAyID8gY3R4Lm1vdmVUbyh4LCB5KSA6IGN0eC5saW5lVG8oeCwgeSk7XG4gICAgICB0ICs9IGxpbmVEYXNoW2lkeF0gLyBiZXppZXJMZW47XG4gICAgICBpZHggPSAoaWR4ICsgMSkgJSBuRGFzaDtcbiAgICB9IC8vIEZpbmlzaCB0aGUgbGFzdCBzZWdtZW50IGFuZCBjYWxjdWxhdGUgdGhlIG5ldyBvZmZzZXRcblxuXG4gICAgaWR4ICUgMiAhPT0gMCAmJiBjdHgubGluZVRvKHgzLCB5Myk7XG4gICAgZHggPSB4MyAtIHg7XG4gICAgZHkgPSB5MyAtIHk7XG4gICAgdGhpcy5fZGFzaE9mZnNldCA9IC1tYXRoU3FydChkeCAqIGR4ICsgZHkgKiBkeSk7XG4gIH0sXG4gIF9kYXNoZWRRdWFkcmF0aWNUbzogZnVuY3Rpb24gKHgxLCB5MSwgeDIsIHkyKSB7XG4gICAgLy8gQ29udmVydCBxdWFkcmF0aWMgdG8gY3ViaWMgdXNpbmcgZGVncmVlIGVsZXZhdGlvblxuICAgIHZhciB4MyA9IHgyO1xuICAgIHZhciB5MyA9IHkyO1xuICAgIHgyID0gKHgyICsgMiAqIHgxKSAvIDM7XG4gICAgeTIgPSAoeTIgKyAyICogeTEpIC8gMztcbiAgICB4MSA9ICh0aGlzLl94aSArIDIgKiB4MSkgLyAzO1xuICAgIHkxID0gKHRoaXMuX3lpICsgMiAqIHkxKSAvIDM7XG5cbiAgICB0aGlzLl9kYXNoZWRCZXppZXJUbyh4MSwgeTEsIHgyLCB5MiwgeDMsIHkzKTtcbiAgfSxcblxuICAvKipcbiAgICog6L2s5oiQ6Z2Z5oCB55qEIEZsb2F0MzJBcnJheSDlh4/lsJHloIblhoXlrZjljaDnlKhcbiAgICogQ29udmVydCBkeW5hbWljIGFycmF5IHRvIHN0YXRpYyBGbG9hdDMyQXJyYXlcbiAgICovXG4gIHRvU3RhdGljOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGRhdGEgPSB0aGlzLmRhdGE7XG5cbiAgICBpZiAoZGF0YSBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICBkYXRhLmxlbmd0aCA9IHRoaXMuX2xlbjtcblxuICAgICAgaWYgKGhhc1R5cGVkQXJyYXkpIHtcbiAgICAgICAgdGhpcy5kYXRhID0gbmV3IEZsb2F0MzJBcnJheShkYXRhKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvcmUvQm91bmRpbmdSZWN0fVxuICAgKi9cbiAgZ2V0Qm91bmRpbmdSZWN0OiBmdW5jdGlvbiAoKSB7XG4gICAgbWluWzBdID0gbWluWzFdID0gbWluMlswXSA9IG1pbjJbMV0gPSBOdW1iZXIuTUFYX1ZBTFVFO1xuICAgIG1heFswXSA9IG1heFsxXSA9IG1heDJbMF0gPSBtYXgyWzFdID0gLU51bWJlci5NQVhfVkFMVUU7XG4gICAgdmFyIGRhdGEgPSB0aGlzLmRhdGE7XG4gICAgdmFyIHhpID0gMDtcbiAgICB2YXIgeWkgPSAwO1xuICAgIHZhciB4MCA9IDA7XG4gICAgdmFyIHkwID0gMDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7KSB7XG4gICAgICB2YXIgY21kID0gZGF0YVtpKytdO1xuXG4gICAgICBpZiAoaSA9PT0gMSkge1xuICAgICAgICAvLyDlpoLmnpznrKzkuIDkuKrlkb3ku6TmmK8gTCwgQywgUVxuICAgICAgICAvLyDliJkgcHJldmlvdXMgcG9pbnQg5ZCM57uY5Yi25ZG95Luk55qE56ys5LiA5LiqIHBvaW50XG4gICAgICAgIC8vXG4gICAgICAgIC8vIOesrOS4gOS4quWRveS7pOS4uiBBcmMg55qE5oOF5Ya15LiL5Lya5Zyo5ZCO6Z2i54m55q6K5aSE55CGXG4gICAgICAgIHhpID0gZGF0YVtpXTtcbiAgICAgICAgeWkgPSBkYXRhW2kgKyAxXTtcbiAgICAgICAgeDAgPSB4aTtcbiAgICAgICAgeTAgPSB5aTtcbiAgICAgIH1cblxuICAgICAgc3dpdGNoIChjbWQpIHtcbiAgICAgICAgY2FzZSBDTUQuTTpcbiAgICAgICAgICAvLyBtb3ZlVG8g5ZG95Luk6YeN5paw5Yib5bu65LiA5Liq5paw55qEIHN1YnBhdGgsIOW5tuS4lOabtOaWsOaWsOeahOi1t+eCuVxuICAgICAgICAgIC8vIOWcqCBjbG9zZVBhdGgg55qE5pe25YCZ5L2/55SoXG4gICAgICAgICAgeDAgPSBkYXRhW2krK107XG4gICAgICAgICAgeTAgPSBkYXRhW2krK107XG4gICAgICAgICAgeGkgPSB4MDtcbiAgICAgICAgICB5aSA9IHkwO1xuICAgICAgICAgIG1pbjJbMF0gPSB4MDtcbiAgICAgICAgICBtaW4yWzFdID0geTA7XG4gICAgICAgICAgbWF4MlswXSA9IHgwO1xuICAgICAgICAgIG1heDJbMV0gPSB5MDtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIENNRC5MOlxuICAgICAgICAgIGJib3guZnJvbUxpbmUoeGksIHlpLCBkYXRhW2ldLCBkYXRhW2kgKyAxXSwgbWluMiwgbWF4Mik7XG4gICAgICAgICAgeGkgPSBkYXRhW2krK107XG4gICAgICAgICAgeWkgPSBkYXRhW2krK107XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuQzpcbiAgICAgICAgICBiYm94LmZyb21DdWJpYyh4aSwgeWksIGRhdGFbaSsrXSwgZGF0YVtpKytdLCBkYXRhW2krK10sIGRhdGFbaSsrXSwgZGF0YVtpXSwgZGF0YVtpICsgMV0sIG1pbjIsIG1heDIpO1xuICAgICAgICAgIHhpID0gZGF0YVtpKytdO1xuICAgICAgICAgIHlpID0gZGF0YVtpKytdO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgQ01ELlE6XG4gICAgICAgICAgYmJveC5mcm9tUXVhZHJhdGljKHhpLCB5aSwgZGF0YVtpKytdLCBkYXRhW2krK10sIGRhdGFbaV0sIGRhdGFbaSArIDFdLCBtaW4yLCBtYXgyKTtcbiAgICAgICAgICB4aSA9IGRhdGFbaSsrXTtcbiAgICAgICAgICB5aSA9IGRhdGFbaSsrXTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIENNRC5BOlxuICAgICAgICAgIC8vIFRPRE8gQXJjIOWIpOaWreeahOW8gOmUgOavlOi+g+Wkp1xuICAgICAgICAgIHZhciBjeCA9IGRhdGFbaSsrXTtcbiAgICAgICAgICB2YXIgY3kgPSBkYXRhW2krK107XG4gICAgICAgICAgdmFyIHJ4ID0gZGF0YVtpKytdO1xuICAgICAgICAgIHZhciByeSA9IGRhdGFbaSsrXTtcbiAgICAgICAgICB2YXIgc3RhcnRBbmdsZSA9IGRhdGFbaSsrXTtcbiAgICAgICAgICB2YXIgZW5kQW5nbGUgPSBkYXRhW2krK10gKyBzdGFydEFuZ2xlOyAvLyBUT0RPIEFyYyDml4vovaxcblxuICAgICAgICAgIGkgKz0gMTtcbiAgICAgICAgICB2YXIgYW50aWNsb2Nrd2lzZSA9IDEgLSBkYXRhW2krK107XG5cbiAgICAgICAgICBpZiAoaSA9PT0gMSkge1xuICAgICAgICAgICAgLy8g55u05o6l5L2/55SoIGFyYyDlkb3ku6RcbiAgICAgICAgICAgIC8vIOesrOS4gOS4quWRveS7pOi1t+eCuei/mOacquWumuS5iVxuICAgICAgICAgICAgeDAgPSBtYXRoQ29zKHN0YXJ0QW5nbGUpICogcnggKyBjeDtcbiAgICAgICAgICAgIHkwID0gbWF0aFNpbihzdGFydEFuZ2xlKSAqIHJ5ICsgY3k7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgYmJveC5mcm9tQXJjKGN4LCBjeSwgcngsIHJ5LCBzdGFydEFuZ2xlLCBlbmRBbmdsZSwgYW50aWNsb2Nrd2lzZSwgbWluMiwgbWF4Mik7XG4gICAgICAgICAgeGkgPSBtYXRoQ29zKGVuZEFuZ2xlKSAqIHJ4ICsgY3g7XG4gICAgICAgICAgeWkgPSBtYXRoU2luKGVuZEFuZ2xlKSAqIHJ5ICsgY3k7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuUjpcbiAgICAgICAgICB4MCA9IHhpID0gZGF0YVtpKytdO1xuICAgICAgICAgIHkwID0geWkgPSBkYXRhW2krK107XG4gICAgICAgICAgdmFyIHdpZHRoID0gZGF0YVtpKytdO1xuICAgICAgICAgIHZhciBoZWlnaHQgPSBkYXRhW2krK107IC8vIFVzZSBmcm9tTGluZVxuXG4gICAgICAgICAgYmJveC5mcm9tTGluZSh4MCwgeTAsIHgwICsgd2lkdGgsIHkwICsgaGVpZ2h0LCBtaW4yLCBtYXgyKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIENNRC5aOlxuICAgICAgICAgIHhpID0geDA7XG4gICAgICAgICAgeWkgPSB5MDtcbiAgICAgICAgICBicmVhaztcbiAgICAgIH0gLy8gVW5pb25cblxuXG4gICAgICB2ZWMyLm1pbihtaW4sIG1pbiwgbWluMik7XG4gICAgICB2ZWMyLm1heChtYXgsIG1heCwgbWF4Mik7XG4gICAgfSAvLyBObyBkYXRhXG5cblxuICAgIGlmIChpID09PSAwKSB7XG4gICAgICBtaW5bMF0gPSBtaW5bMV0gPSBtYXhbMF0gPSBtYXhbMV0gPSAwO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgQm91bmRpbmdSZWN0KG1pblswXSwgbWluWzFdLCBtYXhbMF0gLSBtaW5bMF0sIG1heFsxXSAtIG1pblsxXSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFJlYnVpbGQgcGF0aCBmcm9tIGN1cnJlbnQgZGF0YVxuICAgKiBSZWJ1aWxkIHBhdGggd2lsbCBub3QgY29uc2lkZXIgamF2YXNjcmlwdCBpbXBsZW1lbnRlZCBsaW5lIGRhc2guXG4gICAqIEBwYXJhbSB7Q2FudmFzUmVuZGVyaW5nQ29udGV4dDJEfSBjdHhcbiAgICovXG4gIHJlYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4KSB7XG4gICAgdmFyIGQgPSB0aGlzLmRhdGE7XG4gICAgdmFyIHgwLCB5MDtcbiAgICB2YXIgeGksIHlpO1xuICAgIHZhciB4LCB5O1xuICAgIHZhciB1eCA9IHRoaXMuX3V4O1xuICAgIHZhciB1eSA9IHRoaXMuX3V5O1xuICAgIHZhciBsZW4gPSB0aGlzLl9sZW47XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjspIHtcbiAgICAgIHZhciBjbWQgPSBkW2krK107XG5cbiAgICAgIGlmIChpID09PSAxKSB7XG4gICAgICAgIC8vIOWmguaenOesrOS4gOS4quWRveS7pOaYryBMLCBDLCBRXG4gICAgICAgIC8vIOWImSBwcmV2aW91cyBwb2ludCDlkIznu5jliLblkb3ku6TnmoTnrKzkuIDkuKogcG9pbnRcbiAgICAgICAgLy9cbiAgICAgICAgLy8g56ys5LiA5Liq5ZG95Luk5Li6IEFyYyDnmoTmg4XlhrXkuIvkvJrlnKjlkI7pnaLnibnmrorlpITnkIZcbiAgICAgICAgeGkgPSBkW2ldO1xuICAgICAgICB5aSA9IGRbaSArIDFdO1xuICAgICAgICB4MCA9IHhpO1xuICAgICAgICB5MCA9IHlpO1xuICAgICAgfVxuXG4gICAgICBzd2l0Y2ggKGNtZCkge1xuICAgICAgICBjYXNlIENNRC5NOlxuICAgICAgICAgIHgwID0geGkgPSBkW2krK107XG4gICAgICAgICAgeTAgPSB5aSA9IGRbaSsrXTtcbiAgICAgICAgICBjdHgubW92ZVRvKHhpLCB5aSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuTDpcbiAgICAgICAgICB4ID0gZFtpKytdO1xuICAgICAgICAgIHkgPSBkW2krK107IC8vIE5vdCBkcmF3IHRvbyBzbWFsbCBzZWcgYmV0d2VlblxuXG4gICAgICAgICAgaWYgKG1hdGhBYnMoeCAtIHhpKSA+IHV4IHx8IG1hdGhBYnMoeSAtIHlpKSA+IHV5IHx8IGkgPT09IGxlbiAtIDEpIHtcbiAgICAgICAgICAgIGN0eC5saW5lVG8oeCwgeSk7XG4gICAgICAgICAgICB4aSA9IHg7XG4gICAgICAgICAgICB5aSA9IHk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuQzpcbiAgICAgICAgICBjdHguYmV6aWVyQ3VydmVUbyhkW2krK10sIGRbaSsrXSwgZFtpKytdLCBkW2krK10sIGRbaSsrXSwgZFtpKytdKTtcbiAgICAgICAgICB4aSA9IGRbaSAtIDJdO1xuICAgICAgICAgIHlpID0gZFtpIC0gMV07XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuUTpcbiAgICAgICAgICBjdHgucXVhZHJhdGljQ3VydmVUbyhkW2krK10sIGRbaSsrXSwgZFtpKytdLCBkW2krK10pO1xuICAgICAgICAgIHhpID0gZFtpIC0gMl07XG4gICAgICAgICAgeWkgPSBkW2kgLSAxXTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlIENNRC5BOlxuICAgICAgICAgIHZhciBjeCA9IGRbaSsrXTtcbiAgICAgICAgICB2YXIgY3kgPSBkW2krK107XG4gICAgICAgICAgdmFyIHJ4ID0gZFtpKytdO1xuICAgICAgICAgIHZhciByeSA9IGRbaSsrXTtcbiAgICAgICAgICB2YXIgdGhldGEgPSBkW2krK107XG4gICAgICAgICAgdmFyIGRUaGV0YSA9IGRbaSsrXTtcbiAgICAgICAgICB2YXIgcHNpID0gZFtpKytdO1xuICAgICAgICAgIHZhciBmcyA9IGRbaSsrXTtcbiAgICAgICAgICB2YXIgciA9IHJ4ID4gcnkgPyByeCA6IHJ5O1xuICAgICAgICAgIHZhciBzY2FsZVggPSByeCA+IHJ5ID8gMSA6IHJ4IC8gcnk7XG4gICAgICAgICAgdmFyIHNjYWxlWSA9IHJ4ID4gcnkgPyByeSAvIHJ4IDogMTtcbiAgICAgICAgICB2YXIgaXNFbGxpcHNlID0gTWF0aC5hYnMocnggLSByeSkgPiAxZS0zO1xuICAgICAgICAgIHZhciBlbmRBbmdsZSA9IHRoZXRhICsgZFRoZXRhO1xuXG4gICAgICAgICAgaWYgKGlzRWxsaXBzZSkge1xuICAgICAgICAgICAgY3R4LnRyYW5zbGF0ZShjeCwgY3kpO1xuICAgICAgICAgICAgY3R4LnJvdGF0ZShwc2kpO1xuICAgICAgICAgICAgY3R4LnNjYWxlKHNjYWxlWCwgc2NhbGVZKTtcbiAgICAgICAgICAgIGN0eC5hcmMoMCwgMCwgciwgdGhldGEsIGVuZEFuZ2xlLCAxIC0gZnMpO1xuICAgICAgICAgICAgY3R4LnNjYWxlKDEgLyBzY2FsZVgsIDEgLyBzY2FsZVkpO1xuICAgICAgICAgICAgY3R4LnJvdGF0ZSgtcHNpKTtcbiAgICAgICAgICAgIGN0eC50cmFuc2xhdGUoLWN4LCAtY3kpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjdHguYXJjKGN4LCBjeSwgciwgdGhldGEsIGVuZEFuZ2xlLCAxIC0gZnMpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChpID09PSAxKSB7XG4gICAgICAgICAgICAvLyDnm7TmjqXkvb/nlKggYXJjIOWRveS7pFxuICAgICAgICAgICAgLy8g56ys5LiA5Liq5ZG95Luk6LW354K56L+Y5pyq5a6a5LmJXG4gICAgICAgICAgICB4MCA9IG1hdGhDb3ModGhldGEpICogcnggKyBjeDtcbiAgICAgICAgICAgIHkwID0gbWF0aFNpbih0aGV0YSkgKiByeSArIGN5O1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHhpID0gbWF0aENvcyhlbmRBbmdsZSkgKiByeCArIGN4O1xuICAgICAgICAgIHlpID0gbWF0aFNpbihlbmRBbmdsZSkgKiByeSArIGN5O1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgQ01ELlI6XG4gICAgICAgICAgeDAgPSB4aSA9IGRbaV07XG4gICAgICAgICAgeTAgPSB5aSA9IGRbaSArIDFdO1xuICAgICAgICAgIGN0eC5yZWN0KGRbaSsrXSwgZFtpKytdLCBkW2krK10sIGRbaSsrXSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSBDTUQuWjpcbiAgICAgICAgICBjdHguY2xvc2VQYXRoKCk7XG4gICAgICAgICAgeGkgPSB4MDtcbiAgICAgICAgICB5aSA9IHkwO1xuICAgICAgfVxuICAgIH1cbiAgfVxufTtcblBhdGhQcm94eS5DTUQgPSBDTUQ7XG52YXIgX2RlZmF1bHQgPSBQYXRoUHJveHk7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBFQTtBQUNBO0FBQ0E7QUFzRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBOUVBO0FBZ0ZBO0FBQ0E7QUE1cUJBO0FBOHFCQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/PathProxy.js
