__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-editor-mention */ "./node_modules/rc-editor-mention/es/index.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}









var Mention =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Mention, _React$Component);

  function Mention(props) {
    var _this;

    _classCallCheck(this, Mention);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Mention).call(this, props));

    _this.mentionRef = function (ele) {
      _this.mentionEle = ele;
    };

    _this.onSearchChange = function (value, prefix) {
      if (_this.props.onSearchChange) {
        return _this.props.onSearchChange(value, prefix);
      }

      return _this.defaultSearchChange(value);
    };

    _this.onChange = function (editorState) {
      if (_this.props.onChange) {
        _this.props.onChange(editorState);
      }
    };

    _this.onFocus = function (ev) {
      _this.setState({
        focus: true
      });

      if (_this.props.onFocus) {
        _this.props.onFocus(ev);
      }
    };

    _this.onBlur = function (ev) {
      _this.setState({
        focus: false
      });

      if (_this.props.onBlur) {
        _this.props.onBlur(ev);
      }
    };

    _this.focus = function () {
      _this.mentionEle._editor.focusEditor();
    };

    _this.renderMention = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          loading = _this$props.loading,
          placement = _this$props.placement,
          suggestions = _this$props.suggestions;
      var _this$state = _this.state,
          filteredSuggestions = _this$state.filteredSuggestions,
          focus = _this$state.focus;
      var prefixCls = getPrefixCls('mention', customizePrefixCls);
      var cls = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-active"), focus), _defineProperty(_classNames, "".concat(prefixCls, "-placement-top"), placement === 'top'), _classNames));
      var notFoundContent = loading ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "loading"
      }) : _this.props.notFoundContent;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({}, _this.props, {
        prefixCls: prefixCls,
        className: cls,
        ref: _this.mentionRef,
        onSearchChange: _this.onSearchChange,
        onChange: _this.onChange,
        onFocus: _this.onFocus,
        onBlur: _this.onBlur,
        suggestions: suggestions || filteredSuggestions,
        notFoundContent: notFoundContent
      }));
    };

    _this.state = {
      filteredSuggestions: props.defaultSuggestions,
      focus: false
    };
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_5__["default"])(false, 'Mention', 'Mention component is deprecated. Please use Mentions component instead.');
    return _this;
  }

  _createClass(Mention, [{
    key: "defaultSearchChange",
    value: function defaultSearchChange(value) {
      var searchValue = value.toLowerCase();
      var filteredSuggestions = (this.props.defaultSuggestions || []).filter(function (suggestion) {
        if (typeof suggestion === 'string') {
          return suggestion.toLowerCase().indexOf(searchValue) !== -1;
        }

        if (suggestion.type && suggestion.type === rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["Nav"]) {
          return suggestion.props.value ? suggestion.props.value.toLowerCase().indexOf(searchValue) !== -1 : true;
        }

        return false;
      });
      this.setState({
        filteredSuggestions: filteredSuggestions
      });
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderMention);
    }
  }]);

  return Mention;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Mention.getMentions = rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["getMentions"];
Mention.defaultProps = {
  notFoundContent: 'No matches found',
  loading: false,
  multiLines: false,
  placement: 'bottom'
};
Mention.Nav = rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["Nav"];
Mention.toString = rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["toString"];
Mention.toContentState = rc_editor_mention__WEBPACK_IMPORTED_MODULE_1__["toEditorState"];
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(Mention);
/* harmony default export */ __webpack_exports__["default"] = (Mention);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tZW50aW9uL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9tZW50aW9uL2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUmNNZW50aW9uLCB7IE5hdiwgdG9TdHJpbmcsIHRvRWRpdG9yU3RhdGUsIGdldE1lbnRpb25zIH0gZnJvbSAncmMtZWRpdG9yLW1lbnRpb24nO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5jbGFzcyBNZW50aW9uIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMubWVudGlvblJlZiA9IChlbGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMubWVudGlvbkVsZSA9IGVsZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5vblNlYXJjaENoYW5nZSA9ICh2YWx1ZSwgcHJlZml4KSA9PiB7XG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vblNlYXJjaENoYW5nZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3BzLm9uU2VhcmNoQ2hhbmdlKHZhbHVlLCBwcmVmaXgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdFNlYXJjaENoYW5nZSh2YWx1ZSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25DaGFuZ2UgPSAoZWRpdG9yU3RhdGUpID0+IHtcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uQ2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZShlZGl0b3JTdGF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25Gb2N1cyA9IChldikgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgZm9jdXM6IHRydWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLm9uRm9jdXMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uRm9jdXMoZXYpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uQmx1ciA9IChldikgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgZm9jdXM6IGZhbHNlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5vbkJsdXIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uQmx1cihldik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuZm9jdXMgPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm1lbnRpb25FbGUuX2VkaXRvci5mb2N1c0VkaXRvcigpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlck1lbnRpb24gPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lID0gJycsIGxvYWRpbmcsIHBsYWNlbWVudCwgc3VnZ2VzdGlvbnMsIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgeyBmaWx0ZXJlZFN1Z2dlc3Rpb25zLCBmb2N1cyB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnbWVudGlvbicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBjbHMgPSBjbGFzc05hbWVzKGNsYXNzTmFtZSwge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWFjdGl2ZWBdOiBmb2N1cyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1wbGFjZW1lbnQtdG9wYF06IHBsYWNlbWVudCA9PT0gJ3RvcCcsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvbnN0IG5vdEZvdW5kQ29udGVudCA9IGxvYWRpbmcgPyA8SWNvbiB0eXBlPVwibG9hZGluZ1wiLz4gOiB0aGlzLnByb3BzLm5vdEZvdW5kQ29udGVudDtcbiAgICAgICAgICAgIHJldHVybiAoPFJjTWVudGlvbiB7Li4udGhpcy5wcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9IGNsYXNzTmFtZT17Y2xzfSByZWY9e3RoaXMubWVudGlvblJlZn0gb25TZWFyY2hDaGFuZ2U9e3RoaXMub25TZWFyY2hDaGFuZ2V9IG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfSBvbkZvY3VzPXt0aGlzLm9uRm9jdXN9IG9uQmx1cj17dGhpcy5vbkJsdXJ9IHN1Z2dlc3Rpb25zPXtzdWdnZXN0aW9ucyB8fCBmaWx0ZXJlZFN1Z2dlc3Rpb25zfSBub3RGb3VuZENvbnRlbnQ9e25vdEZvdW5kQ29udGVudH0vPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBmaWx0ZXJlZFN1Z2dlc3Rpb25zOiBwcm9wcy5kZWZhdWx0U3VnZ2VzdGlvbnMsXG4gICAgICAgICAgICBmb2N1czogZmFsc2UsXG4gICAgICAgIH07XG4gICAgICAgIHdhcm5pbmcoZmFsc2UsICdNZW50aW9uJywgJ01lbnRpb24gY29tcG9uZW50IGlzIGRlcHJlY2F0ZWQuIFBsZWFzZSB1c2UgTWVudGlvbnMgY29tcG9uZW50IGluc3RlYWQuJyk7XG4gICAgfVxuICAgIGRlZmF1bHRTZWFyY2hDaGFuZ2UodmFsdWUpIHtcbiAgICAgICAgY29uc3Qgc2VhcmNoVmFsdWUgPSB2YWx1ZS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICBjb25zdCBmaWx0ZXJlZFN1Z2dlc3Rpb25zID0gKHRoaXMucHJvcHMuZGVmYXVsdFN1Z2dlc3Rpb25zIHx8IFtdKS5maWx0ZXIoc3VnZ2VzdGlvbiA9PiB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHN1Z2dlc3Rpb24gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Z2dlc3Rpb24udG9Mb3dlckNhc2UoKS5pbmRleE9mKHNlYXJjaFZhbHVlKSAhPT0gLTE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoc3VnZ2VzdGlvbi50eXBlICYmIHN1Z2dlc3Rpb24udHlwZSA9PT0gTmF2KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHN1Z2dlc3Rpb24ucHJvcHMudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgPyBzdWdnZXN0aW9uLnByb3BzLnZhbHVlLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihzZWFyY2hWYWx1ZSkgIT09IC0xXG4gICAgICAgICAgICAgICAgICAgIDogdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgZmlsdGVyZWRTdWdnZXN0aW9ucyxcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJNZW50aW9ufTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbk1lbnRpb24uZ2V0TWVudGlvbnMgPSBnZXRNZW50aW9ucztcbk1lbnRpb24uZGVmYXVsdFByb3BzID0ge1xuICAgIG5vdEZvdW5kQ29udGVudDogJ05vIG1hdGNoZXMgZm91bmQnLFxuICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgIG11bHRpTGluZXM6IGZhbHNlLFxuICAgIHBsYWNlbWVudDogJ2JvdHRvbScsXG59O1xuTWVudGlvbi5OYXYgPSBOYXY7XG5NZW50aW9uLnRvU3RyaW5nID0gdG9TdHJpbmc7XG5NZW50aW9uLnRvQ29udGVudFN0YXRlID0gdG9FZGl0b3JTdGF0ZTtcbnBvbHlmaWxsKE1lbnRpb24pO1xuZXhwb3J0IGRlZmF1bHQgTWVudGlvbjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQU5BO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQU9BO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFsREE7QUFtREE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBVEE7QUFXQTtBQUNBO0FBREE7QUFHQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBeEVBO0FBQ0E7QUF5RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/mention/index.js
