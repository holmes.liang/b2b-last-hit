__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_wave__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/wave */ "./node_modules/antd/es/_util/wave.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};
/* eslint-disable react/button-has-type */











var rxTwoCNChar = /^[\u4e00-\u9fa5]{2}$/;
var isTwoCNChar = rxTwoCNChar.test.bind(rxTwoCNChar);

function isString(str) {
  return typeof str === 'string';
} // Insert one space between two chinese characters automatically.


function insertSpace(child, needInserted) {
  // Check the child if is undefined or null.
  if (child == null) {
    return;
  }

  var SPACE = needInserted ? ' ' : ''; // strictNullChecks oops.

  if (typeof child !== 'string' && typeof child !== 'number' && isString(child.type) && isTwoCNChar(child.props.children)) {
    return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](child, {}, child.props.children.split('').join(SPACE));
  }

  if (typeof child === 'string') {
    if (isTwoCNChar(child)) {
      child = child.split('').join(SPACE);
    }

    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, child);
  }

  return child;
}

function spaceChildren(children, needInserted) {
  var isPrevChildPure = false;
  var childList = [];
  react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(children, function (child) {
    var type = _typeof(child);

    var isCurrentChildPure = type === 'string' || type === 'number';

    if (isPrevChildPure && isCurrentChildPure) {
      var lastIndex = childList.length - 1;
      var lastChild = childList[lastIndex];
      childList[lastIndex] = "".concat(lastChild).concat(child);
    } else {
      childList.push(child);
    }

    isPrevChildPure = isCurrentChildPure;
  }); // Pass to React.Children.map to auto fill key

  return react__WEBPACK_IMPORTED_MODULE_0__["Children"].map(childList, function (child) {
    return insertSpace(child, needInserted);
  });
}

var ButtonTypes = Object(_util_type__WEBPACK_IMPORTED_MODULE_8__["tuple"])('default', 'primary', 'ghost', 'dashed', 'danger', 'link');
var ButtonShapes = Object(_util_type__WEBPACK_IMPORTED_MODULE_8__["tuple"])('circle', 'circle-outline', 'round');
var ButtonSizes = Object(_util_type__WEBPACK_IMPORTED_MODULE_8__["tuple"])('large', 'default', 'small');
var ButtonHTMLTypes = Object(_util_type__WEBPACK_IMPORTED_MODULE_8__["tuple"])('submit', 'button', 'reset');

var Button =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Button, _React$Component);

  function Button(props) {
    var _this;

    _classCallCheck(this, Button);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Button).call(this, props));

    _this.saveButtonRef = function (node) {
      _this.buttonNode = node;
    };

    _this.handleClick = function (e) {
      var loading = _this.state.loading;
      var onClick = _this.props.onClick;

      if (loading) {
        return;
      }

      if (onClick) {
        onClick(e);
      }
    };

    _this.renderButton = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls,
          autoInsertSpaceInButton = _ref.autoInsertSpaceInButton;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          type = _a.type,
          shape = _a.shape,
          size = _a.size,
          className = _a.className,
          children = _a.children,
          icon = _a.icon,
          ghost = _a.ghost,
          block = _a.block,
          rest = __rest(_a, ["prefixCls", "type", "shape", "size", "className", "children", "icon", "ghost", "block"]);

      var _this$state = _this.state,
          loading = _this$state.loading,
          hasTwoCNChar = _this$state.hasTwoCNChar;
      var prefixCls = getPrefixCls('btn', customizePrefixCls);
      var autoInsertSpace = autoInsertSpaceInButton !== false; // large => lg
      // small => sm

      var sizeCls = '';

      switch (size) {
        case 'large':
          sizeCls = 'lg';
          break;

        case 'small':
          sizeCls = 'sm';
          break;

        default:
          break;
      }

      var iconType = loading ? 'loading' : icon;
      var classes = classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(type), type), _defineProperty(_classNames, "".concat(prefixCls, "-").concat(shape), shape), _defineProperty(_classNames, "".concat(prefixCls, "-").concat(sizeCls), sizeCls), _defineProperty(_classNames, "".concat(prefixCls, "-icon-only"), !children && children !== 0 && iconType), _defineProperty(_classNames, "".concat(prefixCls, "-loading"), !!loading), _defineProperty(_classNames, "".concat(prefixCls, "-background-ghost"), ghost), _defineProperty(_classNames, "".concat(prefixCls, "-two-chinese-chars"), hasTwoCNChar && autoInsertSpace), _defineProperty(_classNames, "".concat(prefixCls, "-block"), block), _classNames));
      var iconNode = iconType ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: iconType
      }) : null;
      var kids = children || children === 0 ? spaceChildren(children, _this.isNeedInserted() && autoInsertSpace) : null;
      var linkButtonRestProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(rest, ['htmlType', 'loading']);

      if (linkButtonRestProps.href !== undefined) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", _extends({}, linkButtonRestProps, {
          className: classes,
          onClick: _this.handleClick,
          ref: _this.saveButtonRef
        }), iconNode, kids);
      } // React does not recognize the `htmlType` prop on a DOM element. Here we pick it out of `rest`.


      var _b = rest,
          htmlType = _b.htmlType,
          otherProps = __rest(_b, ["htmlType"]);

      var buttonNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("button", _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(otherProps, ['loading']), {
        type: htmlType,
        className: classes,
        onClick: _this.handleClick,
        ref: _this.saveButtonRef
      }), iconNode, kids);

      if (type === 'link') {
        return buttonNode;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_wave__WEBPACK_IMPORTED_MODULE_7__["default"], null, buttonNode);
    };

    _this.state = {
      loading: props.loading,
      hasTwoCNChar: false
    };
    return _this;
  }

  _createClass(Button, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.fixTwoCNChar();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this2 = this;

      this.fixTwoCNChar();

      if (prevProps.loading && typeof prevProps.loading !== 'boolean') {
        clearTimeout(this.delayTimeout);
      }

      var loading = this.props.loading;

      if (loading && typeof loading !== 'boolean' && loading.delay) {
        this.delayTimeout = window.setTimeout(function () {
          _this2.setState({
            loading: loading
          });
        }, loading.delay);
      } else if (prevProps.loading !== loading) {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState({
          loading: loading
        });
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.delayTimeout) {
        clearTimeout(this.delayTimeout);
      }
    }
  }, {
    key: "fixTwoCNChar",
    value: function fixTwoCNChar() {
      // Fix for HOC usage like <FormatMessage />
      if (!this.buttonNode) {
        return;
      }

      var buttonText = this.buttonNode.textContent || this.buttonNode.innerText;

      if (this.isNeedInserted() && isTwoCNChar(buttonText)) {
        if (!this.state.hasTwoCNChar) {
          this.setState({
            hasTwoCNChar: true
          });
        }
      } else if (this.state.hasTwoCNChar) {
        this.setState({
          hasTwoCNChar: false
        });
      }
    }
  }, {
    key: "isNeedInserted",
    value: function isNeedInserted() {
      var _this$props = this.props,
          icon = _this$props.icon,
          children = _this$props.children,
          type = _this$props.type;
      return react__WEBPACK_IMPORTED_MODULE_0__["Children"].count(children) === 1 && !icon && type !== 'link';
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderButton);
    }
  }]);

  return Button;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Button.__ANT_BUTTON = true;
Button.defaultProps = {
  loading: false,
  ghost: false,
  block: false,
  htmlType: 'button'
};
Button.propTypes = {
  type: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  shape: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](ButtonShapes),
  size: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](ButtonSizes),
  htmlType: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](ButtonHTMLTypes),
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  loading: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"], prop_types__WEBPACK_IMPORTED_MODULE_1__["object"]]),
  className: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  icon: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  block: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  title: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"]
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(Button);
/* harmony default export */ __webpack_exports__["default"] = (Button);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9idXR0b24vYnV0dG9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9idXR0b24vYnV0dG9uLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG4vKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9idXR0b24taGFzLXR5cGUgKi9cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IFdhdmUgZnJvbSAnLi4vX3V0aWwvd2F2ZSc7XG5pbXBvcnQgeyB0dXBsZSB9IGZyb20gJy4uL191dGlsL3R5cGUnO1xuY29uc3QgcnhUd29DTkNoYXIgPSAvXltcXHU0ZTAwLVxcdTlmYTVdezJ9JC87XG5jb25zdCBpc1R3b0NOQ2hhciA9IHJ4VHdvQ05DaGFyLnRlc3QuYmluZChyeFR3b0NOQ2hhcik7XG5mdW5jdGlvbiBpc1N0cmluZyhzdHIpIHtcbiAgICByZXR1cm4gdHlwZW9mIHN0ciA9PT0gJ3N0cmluZyc7XG59XG4vLyBJbnNlcnQgb25lIHNwYWNlIGJldHdlZW4gdHdvIGNoaW5lc2UgY2hhcmFjdGVycyBhdXRvbWF0aWNhbGx5LlxuZnVuY3Rpb24gaW5zZXJ0U3BhY2UoY2hpbGQsIG5lZWRJbnNlcnRlZCkge1xuICAgIC8vIENoZWNrIHRoZSBjaGlsZCBpZiBpcyB1bmRlZmluZWQgb3IgbnVsbC5cbiAgICBpZiAoY2hpbGQgPT0gbnVsbCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IFNQQUNFID0gbmVlZEluc2VydGVkID8gJyAnIDogJyc7XG4gICAgLy8gc3RyaWN0TnVsbENoZWNrcyBvb3BzLlxuICAgIGlmICh0eXBlb2YgY2hpbGQgIT09ICdzdHJpbmcnICYmXG4gICAgICAgIHR5cGVvZiBjaGlsZCAhPT0gJ251bWJlcicgJiZcbiAgICAgICAgaXNTdHJpbmcoY2hpbGQudHlwZSkgJiZcbiAgICAgICAgaXNUd29DTkNoYXIoY2hpbGQucHJvcHMuY2hpbGRyZW4pKSB7XG4gICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHt9LCBjaGlsZC5wcm9wcy5jaGlsZHJlbi5zcGxpdCgnJykuam9pbihTUEFDRSkpO1xuICAgIH1cbiAgICBpZiAodHlwZW9mIGNoaWxkID09PSAnc3RyaW5nJykge1xuICAgICAgICBpZiAoaXNUd29DTkNoYXIoY2hpbGQpKSB7XG4gICAgICAgICAgICBjaGlsZCA9IGNoaWxkLnNwbGl0KCcnKS5qb2luKFNQQUNFKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gPHNwYW4+e2NoaWxkfTwvc3Bhbj47XG4gICAgfVxuICAgIHJldHVybiBjaGlsZDtcbn1cbmZ1bmN0aW9uIHNwYWNlQ2hpbGRyZW4oY2hpbGRyZW4sIG5lZWRJbnNlcnRlZCkge1xuICAgIGxldCBpc1ByZXZDaGlsZFB1cmUgPSBmYWxzZTtcbiAgICBjb25zdCBjaGlsZExpc3QgPSBbXTtcbiAgICBSZWFjdC5DaGlsZHJlbi5mb3JFYWNoKGNoaWxkcmVuLCBjaGlsZCA9PiB7XG4gICAgICAgIGNvbnN0IHR5cGUgPSB0eXBlb2YgY2hpbGQ7XG4gICAgICAgIGNvbnN0IGlzQ3VycmVudENoaWxkUHVyZSA9IHR5cGUgPT09ICdzdHJpbmcnIHx8IHR5cGUgPT09ICdudW1iZXInO1xuICAgICAgICBpZiAoaXNQcmV2Q2hpbGRQdXJlICYmIGlzQ3VycmVudENoaWxkUHVyZSkge1xuICAgICAgICAgICAgY29uc3QgbGFzdEluZGV4ID0gY2hpbGRMaXN0Lmxlbmd0aCAtIDE7XG4gICAgICAgICAgICBjb25zdCBsYXN0Q2hpbGQgPSBjaGlsZExpc3RbbGFzdEluZGV4XTtcbiAgICAgICAgICAgIGNoaWxkTGlzdFtsYXN0SW5kZXhdID0gYCR7bGFzdENoaWxkfSR7Y2hpbGR9YDtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNoaWxkTGlzdC5wdXNoKGNoaWxkKTtcbiAgICAgICAgfVxuICAgICAgICBpc1ByZXZDaGlsZFB1cmUgPSBpc0N1cnJlbnRDaGlsZFB1cmU7XG4gICAgfSk7XG4gICAgLy8gUGFzcyB0byBSZWFjdC5DaGlsZHJlbi5tYXAgdG8gYXV0byBmaWxsIGtleVxuICAgIHJldHVybiBSZWFjdC5DaGlsZHJlbi5tYXAoY2hpbGRMaXN0LCBjaGlsZCA9PiBpbnNlcnRTcGFjZShjaGlsZCwgbmVlZEluc2VydGVkKSk7XG59XG5jb25zdCBCdXR0b25UeXBlcyA9IHR1cGxlKCdkZWZhdWx0JywgJ3ByaW1hcnknLCAnZ2hvc3QnLCAnZGFzaGVkJywgJ2RhbmdlcicsICdsaW5rJyk7XG5jb25zdCBCdXR0b25TaGFwZXMgPSB0dXBsZSgnY2lyY2xlJywgJ2NpcmNsZS1vdXRsaW5lJywgJ3JvdW5kJyk7XG5jb25zdCBCdXR0b25TaXplcyA9IHR1cGxlKCdsYXJnZScsICdkZWZhdWx0JywgJ3NtYWxsJyk7XG5jb25zdCBCdXR0b25IVE1MVHlwZXMgPSB0dXBsZSgnc3VibWl0JywgJ2J1dHRvbicsICdyZXNldCcpO1xuY2xhc3MgQnV0dG9uIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc2F2ZUJ1dHRvblJlZiA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmJ1dHRvbk5vZGUgPSBub2RlO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZUNsaWNrID0gZSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGxvYWRpbmcgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCB7IG9uQ2xpY2sgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAobG9hZGluZykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChvbkNsaWNrKSB7XG4gICAgICAgICAgICAgICAgb25DbGljayhlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJCdXR0b24gPSAoeyBnZXRQcmVmaXhDbHMsIGF1dG9JbnNlcnRTcGFjZUluQnV0dG9uIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgdHlwZSwgc2hhcGUsIHNpemUsIGNsYXNzTmFtZSwgY2hpbGRyZW4sIGljb24sIGdob3N0LCBibG9jayB9ID0gX2EsIHJlc3QgPSBfX3Jlc3QoX2EsIFtcInByZWZpeENsc1wiLCBcInR5cGVcIiwgXCJzaGFwZVwiLCBcInNpemVcIiwgXCJjbGFzc05hbWVcIiwgXCJjaGlsZHJlblwiLCBcImljb25cIiwgXCJnaG9zdFwiLCBcImJsb2NrXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IHsgbG9hZGluZywgaGFzVHdvQ05DaGFyIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdidG4nLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgYXV0b0luc2VydFNwYWNlID0gYXV0b0luc2VydFNwYWNlSW5CdXR0b24gIT09IGZhbHNlO1xuICAgICAgICAgICAgLy8gbGFyZ2UgPT4gbGdcbiAgICAgICAgICAgIC8vIHNtYWxsID0+IHNtXG4gICAgICAgICAgICBsZXQgc2l6ZUNscyA9ICcnO1xuICAgICAgICAgICAgc3dpdGNoIChzaXplKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnbGFyZ2UnOlxuICAgICAgICAgICAgICAgICAgICBzaXplQ2xzID0gJ2xnJztcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnc21hbGwnOlxuICAgICAgICAgICAgICAgICAgICBzaXplQ2xzID0gJ3NtJztcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBpY29uVHlwZSA9IGxvYWRpbmcgPyAnbG9hZGluZycgOiBpY29uO1xuICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke3R5cGV9YF06IHR5cGUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tJHtzaGFwZX1gXTogc2hhcGUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tJHtzaXplQ2xzfWBdOiBzaXplQ2xzLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWljb24tb25seWBdOiAhY2hpbGRyZW4gJiYgY2hpbGRyZW4gIT09IDAgJiYgaWNvblR5cGUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tbG9hZGluZ2BdOiAhIWxvYWRpbmcsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tYmFja2dyb3VuZC1naG9zdGBdOiBnaG9zdCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS10d28tY2hpbmVzZS1jaGFyc2BdOiBoYXNUd29DTkNoYXIgJiYgYXV0b0luc2VydFNwYWNlLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWJsb2NrYF06IGJsb2NrLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBpY29uTm9kZSA9IGljb25UeXBlID8gPEljb24gdHlwZT17aWNvblR5cGV9Lz4gOiBudWxsO1xuICAgICAgICAgICAgY29uc3Qga2lkcyA9IGNoaWxkcmVuIHx8IGNoaWxkcmVuID09PSAwXG4gICAgICAgICAgICAgICAgPyBzcGFjZUNoaWxkcmVuKGNoaWxkcmVuLCB0aGlzLmlzTmVlZEluc2VydGVkKCkgJiYgYXV0b0luc2VydFNwYWNlKVxuICAgICAgICAgICAgICAgIDogbnVsbDtcbiAgICAgICAgICAgIGNvbnN0IGxpbmtCdXR0b25SZXN0UHJvcHMgPSBvbWl0KHJlc3QsIFsnaHRtbFR5cGUnLCAnbG9hZGluZyddKTtcbiAgICAgICAgICAgIGlmIChsaW5rQnV0dG9uUmVzdFByb3BzLmhyZWYgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAoPGEgey4uLmxpbmtCdXR0b25SZXN0UHJvcHN9IGNsYXNzTmFtZT17Y2xhc3Nlc30gb25DbGljaz17dGhpcy5oYW5kbGVDbGlja30gcmVmPXt0aGlzLnNhdmVCdXR0b25SZWZ9PlxuICAgICAgICAgIHtpY29uTm9kZX1cbiAgICAgICAgICB7a2lkc31cbiAgICAgICAgPC9hPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBSZWFjdCBkb2VzIG5vdCByZWNvZ25pemUgdGhlIGBodG1sVHlwZWAgcHJvcCBvbiBhIERPTSBlbGVtZW50LiBIZXJlIHdlIHBpY2sgaXQgb3V0IG9mIGByZXN0YC5cbiAgICAgICAgICAgIGNvbnN0IF9iID0gcmVzdCwgeyBodG1sVHlwZSB9ID0gX2IsIG90aGVyUHJvcHMgPSBfX3Jlc3QoX2IsIFtcImh0bWxUeXBlXCJdKTtcbiAgICAgICAgICAgIGNvbnN0IGJ1dHRvbk5vZGUgPSAoPGJ1dHRvbiB7Li4ub21pdChvdGhlclByb3BzLCBbJ2xvYWRpbmcnXSl9IHR5cGU9e2h0bWxUeXBlfSBjbGFzc05hbWU9e2NsYXNzZXN9IG9uQ2xpY2s9e3RoaXMuaGFuZGxlQ2xpY2t9IHJlZj17dGhpcy5zYXZlQnV0dG9uUmVmfT5cbiAgICAgICAge2ljb25Ob2RlfVxuICAgICAgICB7a2lkc31cbiAgICAgIDwvYnV0dG9uPik7XG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ2xpbmsnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGJ1dHRvbk5vZGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gPFdhdmU+e2J1dHRvbk5vZGV9PC9XYXZlPjtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIGxvYWRpbmc6IHByb3BzLmxvYWRpbmcsXG4gICAgICAgICAgICBoYXNUd29DTkNoYXI6IGZhbHNlLFxuICAgICAgICB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgdGhpcy5maXhUd29DTkNoYXIoKTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgICB0aGlzLmZpeFR3b0NOQ2hhcigpO1xuICAgICAgICBpZiAocHJldlByb3BzLmxvYWRpbmcgJiYgdHlwZW9mIHByZXZQcm9wcy5sb2FkaW5nICE9PSAnYm9vbGVhbicpIHtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLmRlbGF5VGltZW91dCk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgeyBsb2FkaW5nIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAobG9hZGluZyAmJiB0eXBlb2YgbG9hZGluZyAhPT0gJ2Jvb2xlYW4nICYmIGxvYWRpbmcuZGVsYXkpIHtcbiAgICAgICAgICAgIHRoaXMuZGVsYXlUaW1lb3V0ID0gd2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBsb2FkaW5nIH0pO1xuICAgICAgICAgICAgfSwgbG9hZGluZy5kZWxheSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocHJldlByb3BzLmxvYWRpbmcgIT09IGxvYWRpbmcpIHtcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC9uby1kaWQtdXBkYXRlLXNldC1zdGF0ZVxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmcgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmRlbGF5VGltZW91dCkge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuZGVsYXlUaW1lb3V0KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBmaXhUd29DTkNoYXIoKSB7XG4gICAgICAgIC8vIEZpeCBmb3IgSE9DIHVzYWdlIGxpa2UgPEZvcm1hdE1lc3NhZ2UgLz5cbiAgICAgICAgaWYgKCF0aGlzLmJ1dHRvbk5vZGUpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBidXR0b25UZXh0ID0gdGhpcy5idXR0b25Ob2RlLnRleHRDb250ZW50IHx8IHRoaXMuYnV0dG9uTm9kZS5pbm5lclRleHQ7XG4gICAgICAgIGlmICh0aGlzLmlzTmVlZEluc2VydGVkKCkgJiYgaXNUd29DTkNoYXIoYnV0dG9uVGV4dCkpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5zdGF0ZS5oYXNUd29DTkNoYXIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgaGFzVHdvQ05DaGFyOiB0cnVlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuc3RhdGUuaGFzVHdvQ05DaGFyKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBoYXNUd29DTkNoYXI6IGZhbHNlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaXNOZWVkSW5zZXJ0ZWQoKSB7XG4gICAgICAgIGNvbnN0IHsgaWNvbiwgY2hpbGRyZW4sIHR5cGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiBSZWFjdC5DaGlsZHJlbi5jb3VudChjaGlsZHJlbikgPT09IDEgJiYgIWljb24gJiYgdHlwZSAhPT0gJ2xpbmsnO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQnV0dG9ufTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbkJ1dHRvbi5fX0FOVF9CVVRUT04gPSB0cnVlO1xuQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICBnaG9zdDogZmFsc2UsXG4gICAgYmxvY2s6IGZhbHNlLFxuICAgIGh0bWxUeXBlOiAnYnV0dG9uJyxcbn07XG5CdXR0b24ucHJvcFR5cGVzID0ge1xuICAgIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2hhcGU6IFByb3BUeXBlcy5vbmVPZihCdXR0b25TaGFwZXMpLFxuICAgIHNpemU6IFByb3BUeXBlcy5vbmVPZihCdXR0b25TaXplcyksXG4gICAgaHRtbFR5cGU6IFByb3BUeXBlcy5vbmVPZihCdXR0b25IVE1MVHlwZXMpLFxuICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxuICAgIGxvYWRpbmc6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ib29sLCBQcm9wVHlwZXMub2JqZWN0XSksXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGljb246IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgYmxvY2s6IFByb3BUeXBlcy5ib29sLFxuICAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcbnBvbHlmaWxsKEJ1dHRvbik7XG5leHBvcnQgZGVmYXVsdCBCdXR0b247XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQWRBO0FBQ0E7QUFnQkE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVJBO0FBQ0E7QUFTQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBUkE7QUFDQTtBQVNBO0FBQ0E7QUFVQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFuQ0E7QUFDQTtBQUNBO0FBdUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBakRBO0FBQ0E7QUFrREE7QUFDQTtBQUNBO0FBRkE7QUFsRUE7QUFzRUE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBREE7QUFNQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFMQTtBQVFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUF6SEE7QUFDQTtBQTBIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/button/button.js
