/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getCharacterRemovalRange
 * @format
 * 
 */


var DraftEntitySegments = __webpack_require__(/*! ./DraftEntitySegments */ "./node_modules/draft-js/lib/DraftEntitySegments.js");

var getRangesForDraftEntity = __webpack_require__(/*! ./getRangesForDraftEntity */ "./node_modules/draft-js/lib/getRangesForDraftEntity.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");
/**
 * Given a SelectionState and a removal direction, determine the entire range
 * that should be removed from a ContentState. This is based on any entities
 * within the target, with their `mutability` values taken into account.
 *
 * For instance, if we are attempting to remove part of an "immutable" entity
 * range, the entire entity must be removed. The returned `SelectionState`
 * will be adjusted accordingly.
 */


function getCharacterRemovalRange(entityMap, startBlock, endBlock, selectionState, direction) {
  var start = selectionState.getStartOffset();
  var end = selectionState.getEndOffset();
  var startEntityKey = startBlock.getEntityAt(start);
  var endEntityKey = endBlock.getEntityAt(end - 1);

  if (!startEntityKey && !endEntityKey) {
    return selectionState;
  }

  var newSelectionState = selectionState;

  if (startEntityKey && startEntityKey === endEntityKey) {
    newSelectionState = getEntityRemovalRange(entityMap, startBlock, newSelectionState, direction, startEntityKey, true, true);
  } else if (startEntityKey && endEntityKey) {
    var startSelectionState = getEntityRemovalRange(entityMap, startBlock, newSelectionState, direction, startEntityKey, false, true);
    var endSelectionState = getEntityRemovalRange(entityMap, endBlock, newSelectionState, direction, endEntityKey, false, false);
    newSelectionState = newSelectionState.merge({
      anchorOffset: startSelectionState.getAnchorOffset(),
      focusOffset: endSelectionState.getFocusOffset(),
      isBackward: false
    });
  } else if (startEntityKey) {
    var _startSelectionState = getEntityRemovalRange(entityMap, startBlock, newSelectionState, direction, startEntityKey, false, true);

    newSelectionState = newSelectionState.merge({
      anchorOffset: _startSelectionState.getStartOffset(),
      isBackward: false
    });
  } else if (endEntityKey) {
    var _endSelectionState = getEntityRemovalRange(entityMap, endBlock, newSelectionState, direction, endEntityKey, false, false);

    newSelectionState = newSelectionState.merge({
      focusOffset: _endSelectionState.getEndOffset(),
      isBackward: false
    });
  }

  return newSelectionState;
}

function getEntityRemovalRange(entityMap, block, selectionState, direction, entityKey, isEntireSelectionWithinEntity, isEntityAtStart) {
  var start = selectionState.getStartOffset();
  var end = selectionState.getEndOffset();

  var entity = entityMap.__get(entityKey);

  var mutability = entity.getMutability();
  var sideToConsider = isEntityAtStart ? start : end; // `MUTABLE` entities can just have the specified range of text removed
  // directly. No adjustments are needed.

  if (mutability === 'MUTABLE') {
    return selectionState;
  } // Find the entity range that overlaps with our removal range.


  var entityRanges = getRangesForDraftEntity(block, entityKey).filter(function (range) {
    return sideToConsider <= range.end && sideToConsider >= range.start;
  });
  !(entityRanges.length == 1) ?  true ? invariant(false, 'There should only be one entity range within this removal range.') : undefined : void 0;
  var entityRange = entityRanges[0]; // For `IMMUTABLE` entity types, we will remove the entire entity range.

  if (mutability === 'IMMUTABLE') {
    return selectionState.merge({
      anchorOffset: entityRange.start,
      focusOffset: entityRange.end,
      isBackward: false
    });
  } // For `SEGMENTED` entity types, determine the appropriate segment to
  // remove.


  if (!isEntireSelectionWithinEntity) {
    if (isEntityAtStart) {
      end = entityRange.end;
    } else {
      start = entityRange.start;
    }
  }

  var removalRange = DraftEntitySegments.getRemovalRange(start, end, block.getText().slice(entityRange.start, entityRange.end), entityRange.start, direction);
  return selectionState.merge({
    anchorOffset: removalRange.start,
    focusOffset: removalRange.end,
    isBackward: false
  });
}

module.exports = getCharacterRemovalRange;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldENoYXJhY3RlclJlbW92YWxSYW5nZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9nZXRDaGFyYWN0ZXJSZW1vdmFsUmFuZ2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBnZXRDaGFyYWN0ZXJSZW1vdmFsUmFuZ2VcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0RW50aXR5U2VnbWVudHMgPSByZXF1aXJlKCcuL0RyYWZ0RW50aXR5U2VnbWVudHMnKTtcblxudmFyIGdldFJhbmdlc0ZvckRyYWZ0RW50aXR5ID0gcmVxdWlyZSgnLi9nZXRSYW5nZXNGb3JEcmFmdEVudGl0eScpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG4vKipcbiAqIEdpdmVuIGEgU2VsZWN0aW9uU3RhdGUgYW5kIGEgcmVtb3ZhbCBkaXJlY3Rpb24sIGRldGVybWluZSB0aGUgZW50aXJlIHJhbmdlXG4gKiB0aGF0IHNob3VsZCBiZSByZW1vdmVkIGZyb20gYSBDb250ZW50U3RhdGUuIFRoaXMgaXMgYmFzZWQgb24gYW55IGVudGl0aWVzXG4gKiB3aXRoaW4gdGhlIHRhcmdldCwgd2l0aCB0aGVpciBgbXV0YWJpbGl0eWAgdmFsdWVzIHRha2VuIGludG8gYWNjb3VudC5cbiAqXG4gKiBGb3IgaW5zdGFuY2UsIGlmIHdlIGFyZSBhdHRlbXB0aW5nIHRvIHJlbW92ZSBwYXJ0IG9mIGFuIFwiaW1tdXRhYmxlXCIgZW50aXR5XG4gKiByYW5nZSwgdGhlIGVudGlyZSBlbnRpdHkgbXVzdCBiZSByZW1vdmVkLiBUaGUgcmV0dXJuZWQgYFNlbGVjdGlvblN0YXRlYFxuICogd2lsbCBiZSBhZGp1c3RlZCBhY2NvcmRpbmdseS5cbiAqL1xuZnVuY3Rpb24gZ2V0Q2hhcmFjdGVyUmVtb3ZhbFJhbmdlKGVudGl0eU1hcCwgc3RhcnRCbG9jaywgZW5kQmxvY2ssIHNlbGVjdGlvblN0YXRlLCBkaXJlY3Rpb24pIHtcbiAgdmFyIHN0YXJ0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIGVuZCA9IHNlbGVjdGlvblN0YXRlLmdldEVuZE9mZnNldCgpO1xuICB2YXIgc3RhcnRFbnRpdHlLZXkgPSBzdGFydEJsb2NrLmdldEVudGl0eUF0KHN0YXJ0KTtcbiAgdmFyIGVuZEVudGl0eUtleSA9IGVuZEJsb2NrLmdldEVudGl0eUF0KGVuZCAtIDEpO1xuICBpZiAoIXN0YXJ0RW50aXR5S2V5ICYmICFlbmRFbnRpdHlLZXkpIHtcbiAgICByZXR1cm4gc2VsZWN0aW9uU3RhdGU7XG4gIH1cbiAgdmFyIG5ld1NlbGVjdGlvblN0YXRlID0gc2VsZWN0aW9uU3RhdGU7XG4gIGlmIChzdGFydEVudGl0eUtleSAmJiBzdGFydEVudGl0eUtleSA9PT0gZW5kRW50aXR5S2V5KSB7XG4gICAgbmV3U2VsZWN0aW9uU3RhdGUgPSBnZXRFbnRpdHlSZW1vdmFsUmFuZ2UoZW50aXR5TWFwLCBzdGFydEJsb2NrLCBuZXdTZWxlY3Rpb25TdGF0ZSwgZGlyZWN0aW9uLCBzdGFydEVudGl0eUtleSwgdHJ1ZSwgdHJ1ZSk7XG4gIH0gZWxzZSBpZiAoc3RhcnRFbnRpdHlLZXkgJiYgZW5kRW50aXR5S2V5KSB7XG4gICAgdmFyIHN0YXJ0U2VsZWN0aW9uU3RhdGUgPSBnZXRFbnRpdHlSZW1vdmFsUmFuZ2UoZW50aXR5TWFwLCBzdGFydEJsb2NrLCBuZXdTZWxlY3Rpb25TdGF0ZSwgZGlyZWN0aW9uLCBzdGFydEVudGl0eUtleSwgZmFsc2UsIHRydWUpO1xuICAgIHZhciBlbmRTZWxlY3Rpb25TdGF0ZSA9IGdldEVudGl0eVJlbW92YWxSYW5nZShlbnRpdHlNYXAsIGVuZEJsb2NrLCBuZXdTZWxlY3Rpb25TdGF0ZSwgZGlyZWN0aW9uLCBlbmRFbnRpdHlLZXksIGZhbHNlLCBmYWxzZSk7XG4gICAgbmV3U2VsZWN0aW9uU3RhdGUgPSBuZXdTZWxlY3Rpb25TdGF0ZS5tZXJnZSh7XG4gICAgICBhbmNob3JPZmZzZXQ6IHN0YXJ0U2VsZWN0aW9uU3RhdGUuZ2V0QW5jaG9yT2Zmc2V0KCksXG4gICAgICBmb2N1c09mZnNldDogZW5kU2VsZWN0aW9uU3RhdGUuZ2V0Rm9jdXNPZmZzZXQoKSxcbiAgICAgIGlzQmFja3dhcmQ6IGZhbHNlXG4gICAgfSk7XG4gIH0gZWxzZSBpZiAoc3RhcnRFbnRpdHlLZXkpIHtcbiAgICB2YXIgX3N0YXJ0U2VsZWN0aW9uU3RhdGUgPSBnZXRFbnRpdHlSZW1vdmFsUmFuZ2UoZW50aXR5TWFwLCBzdGFydEJsb2NrLCBuZXdTZWxlY3Rpb25TdGF0ZSwgZGlyZWN0aW9uLCBzdGFydEVudGl0eUtleSwgZmFsc2UsIHRydWUpO1xuICAgIG5ld1NlbGVjdGlvblN0YXRlID0gbmV3U2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgICAgYW5jaG9yT2Zmc2V0OiBfc3RhcnRTZWxlY3Rpb25TdGF0ZS5nZXRTdGFydE9mZnNldCgpLFxuICAgICAgaXNCYWNrd2FyZDogZmFsc2VcbiAgICB9KTtcbiAgfSBlbHNlIGlmIChlbmRFbnRpdHlLZXkpIHtcbiAgICB2YXIgX2VuZFNlbGVjdGlvblN0YXRlID0gZ2V0RW50aXR5UmVtb3ZhbFJhbmdlKGVudGl0eU1hcCwgZW5kQmxvY2ssIG5ld1NlbGVjdGlvblN0YXRlLCBkaXJlY3Rpb24sIGVuZEVudGl0eUtleSwgZmFsc2UsIGZhbHNlKTtcbiAgICBuZXdTZWxlY3Rpb25TdGF0ZSA9IG5ld1NlbGVjdGlvblN0YXRlLm1lcmdlKHtcbiAgICAgIGZvY3VzT2Zmc2V0OiBfZW5kU2VsZWN0aW9uU3RhdGUuZ2V0RW5kT2Zmc2V0KCksXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pO1xuICB9XG4gIHJldHVybiBuZXdTZWxlY3Rpb25TdGF0ZTtcbn1cblxuZnVuY3Rpb24gZ2V0RW50aXR5UmVtb3ZhbFJhbmdlKGVudGl0eU1hcCwgYmxvY2ssIHNlbGVjdGlvblN0YXRlLCBkaXJlY3Rpb24sIGVudGl0eUtleSwgaXNFbnRpcmVTZWxlY3Rpb25XaXRoaW5FbnRpdHksIGlzRW50aXR5QXRTdGFydCkge1xuICB2YXIgc3RhcnQgPSBzZWxlY3Rpb25TdGF0ZS5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgZW5kID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kT2Zmc2V0KCk7XG4gIHZhciBlbnRpdHkgPSBlbnRpdHlNYXAuX19nZXQoZW50aXR5S2V5KTtcbiAgdmFyIG11dGFiaWxpdHkgPSBlbnRpdHkuZ2V0TXV0YWJpbGl0eSgpO1xuICB2YXIgc2lkZVRvQ29uc2lkZXIgPSBpc0VudGl0eUF0U3RhcnQgPyBzdGFydCA6IGVuZDtcblxuICAvLyBgTVVUQUJMRWAgZW50aXRpZXMgY2FuIGp1c3QgaGF2ZSB0aGUgc3BlY2lmaWVkIHJhbmdlIG9mIHRleHQgcmVtb3ZlZFxuICAvLyBkaXJlY3RseS4gTm8gYWRqdXN0bWVudHMgYXJlIG5lZWRlZC5cbiAgaWYgKG11dGFiaWxpdHkgPT09ICdNVVRBQkxFJykge1xuICAgIHJldHVybiBzZWxlY3Rpb25TdGF0ZTtcbiAgfVxuXG4gIC8vIEZpbmQgdGhlIGVudGl0eSByYW5nZSB0aGF0IG92ZXJsYXBzIHdpdGggb3VyIHJlbW92YWwgcmFuZ2UuXG4gIHZhciBlbnRpdHlSYW5nZXMgPSBnZXRSYW5nZXNGb3JEcmFmdEVudGl0eShibG9jaywgZW50aXR5S2V5KS5maWx0ZXIoZnVuY3Rpb24gKHJhbmdlKSB7XG4gICAgcmV0dXJuIHNpZGVUb0NvbnNpZGVyIDw9IHJhbmdlLmVuZCAmJiBzaWRlVG9Db25zaWRlciA+PSByYW5nZS5zdGFydDtcbiAgfSk7XG5cbiAgIShlbnRpdHlSYW5nZXMubGVuZ3RoID09IDEpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ1RoZXJlIHNob3VsZCBvbmx5IGJlIG9uZSBlbnRpdHkgcmFuZ2Ugd2l0aGluIHRoaXMgcmVtb3ZhbCByYW5nZS4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgdmFyIGVudGl0eVJhbmdlID0gZW50aXR5UmFuZ2VzWzBdO1xuXG4gIC8vIEZvciBgSU1NVVRBQkxFYCBlbnRpdHkgdHlwZXMsIHdlIHdpbGwgcmVtb3ZlIHRoZSBlbnRpcmUgZW50aXR5IHJhbmdlLlxuICBpZiAobXV0YWJpbGl0eSA9PT0gJ0lNTVVUQUJMRScpIHtcbiAgICByZXR1cm4gc2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgICAgYW5jaG9yT2Zmc2V0OiBlbnRpdHlSYW5nZS5zdGFydCxcbiAgICAgIGZvY3VzT2Zmc2V0OiBlbnRpdHlSYW5nZS5lbmQsXG4gICAgICBpc0JhY2t3YXJkOiBmYWxzZVxuICAgIH0pO1xuICB9XG5cbiAgLy8gRm9yIGBTRUdNRU5URURgIGVudGl0eSB0eXBlcywgZGV0ZXJtaW5lIHRoZSBhcHByb3ByaWF0ZSBzZWdtZW50IHRvXG4gIC8vIHJlbW92ZS5cbiAgaWYgKCFpc0VudGlyZVNlbGVjdGlvbldpdGhpbkVudGl0eSkge1xuICAgIGlmIChpc0VudGl0eUF0U3RhcnQpIHtcbiAgICAgIGVuZCA9IGVudGl0eVJhbmdlLmVuZDtcbiAgICB9IGVsc2Uge1xuICAgICAgc3RhcnQgPSBlbnRpdHlSYW5nZS5zdGFydDtcbiAgICB9XG4gIH1cblxuICB2YXIgcmVtb3ZhbFJhbmdlID0gRHJhZnRFbnRpdHlTZWdtZW50cy5nZXRSZW1vdmFsUmFuZ2Uoc3RhcnQsIGVuZCwgYmxvY2suZ2V0VGV4dCgpLnNsaWNlKGVudGl0eVJhbmdlLnN0YXJ0LCBlbnRpdHlSYW5nZS5lbmQpLCBlbnRpdHlSYW5nZS5zdGFydCwgZGlyZWN0aW9uKTtcblxuICByZXR1cm4gc2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgIGFuY2hvck9mZnNldDogcmVtb3ZhbFJhbmdlLnN0YXJ0LFxuICAgIGZvY3VzT2Zmc2V0OiByZW1vdmFsUmFuZ2UuZW5kLFxuICAgIGlzQmFja3dhcmQ6IGZhbHNlXG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldENoYXJhY3RlclJlbW92YWxSYW5nZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getCharacterRemovalRange.js
