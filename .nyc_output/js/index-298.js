

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.create = exports.connect = exports.Provider = undefined;

var _Provider2 = __webpack_require__(/*! ./Provider */ "./node_modules/mini-store/lib/Provider.js");

var _Provider3 = _interopRequireDefault(_Provider2);

var _connect2 = __webpack_require__(/*! ./connect */ "./node_modules/mini-store/lib/connect.js");

var _connect3 = _interopRequireDefault(_connect2);

var _create2 = __webpack_require__(/*! ./create */ "./node_modules/mini-store/lib/create.js");

var _create3 = _interopRequireDefault(_create2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

exports.Provider = _Provider3.default;
exports.connect = _connect3.default;
exports.create = _create3.default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvbWluaS1zdG9yZS9saWIvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9taW5pLXN0b3JlL2xpYi9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmNyZWF0ZSA9IGV4cG9ydHMuY29ubmVjdCA9IGV4cG9ydHMuUHJvdmlkZXIgPSB1bmRlZmluZWQ7XG5cbnZhciBfUHJvdmlkZXIyID0gcmVxdWlyZSgnLi9Qcm92aWRlcicpO1xuXG52YXIgX1Byb3ZpZGVyMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1Byb3ZpZGVyMik7XG5cbnZhciBfY29ubmVjdDIgPSByZXF1aXJlKCcuL2Nvbm5lY3QnKTtcblxudmFyIF9jb25uZWN0MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Nvbm5lY3QyKTtcblxudmFyIF9jcmVhdGUyID0gcmVxdWlyZSgnLi9jcmVhdGUnKTtcblxudmFyIF9jcmVhdGUzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlMik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuUHJvdmlkZXIgPSBfUHJvdmlkZXIzLmRlZmF1bHQ7XG5leHBvcnRzLmNvbm5lY3QgPSBfY29ubmVjdDMuZGVmYXVsdDtcbmV4cG9ydHMuY3JlYXRlID0gX2NyZWF0ZTMuZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/mini-store/lib/index.js
