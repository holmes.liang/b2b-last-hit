__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");
/* harmony import */ var _desk_styles_common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-styles/common */ "./src/app/desk/styles/common.tsx");
/* harmony import */ var _components_policy_item__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/policy-item */ "./src/app/desk/quote/components/success/components/policy-item.tsx");
/* harmony import */ var _components_also_buy__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/also-buy */ "./src/app/desk/quote/components/success/components/also-buy.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/success/index.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      .product {\n        text-align: center;\n        ", "\n        a {\n          width: ", ";\n          text-align: center;\n          display: inline-block\n          color: #2c2c2c;\n          margin-bottom: 20px;\n          i {\n          font-size: 40px;\n          }\n          span {\n            display: block;\n          }\n          &:hover {\n            color: ", ";\n          }\n        }\n      }\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}














var isMobile = _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsMobile();

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_19__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var Result =
/*#__PURE__*/
function (_DeskPage) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Result, _DeskPage);

  function Result() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Result);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Result)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.interval = void 0;

    _this.initData = function () {
      var transId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_this.props.match, "params.transId");

      if (!transId) {
        clearInterval(_this.interval);
      }

      return _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].get("/cart/".concat(transId, "/issue/status"), {}, {
        silent: true
      }).then(function (response) {
        var resData = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(response, "body.respData");

        if (!resData) return;

        _this.setState({
          model: resData
        });

        if (resData && !resData.toBeRetried) {
          clearInterval(_this.interval);
        }
      }).catch(function (error) {});
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Result, [{
    key: "smeCrossSellProducts",
    value: function smeCrossSellProducts() {
      var scId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "scId");

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "mainPolicyId");

      var products = [{
        id: "EFC",
        code: "EFC",
        text: "Employee FlexCare",
        url: "/quote/normal/SAIC/EFC"
      }, {
        id: "GRPHEALTH",
        code: "GHS",
        text: "Group Health And Surgical",
        url: "/quote/normal/SAIC/GHS"
      }, {
        id: "GTL",
        code: "GTL",
        text: "Group Term Life",
        url: "/quote/normal/SAIC/GTL"
      }, {
        id: "GPA",
        code: "GPA",
        text: "Group Personal Accident",
        url: "/quote/normal/SAIC/GPA"
      }, {
        id: "FWB",
        code: "FWB",
        text: "Foreign Worker Bond",
        url: "/quote/normal/SAIC/FWB"
      }];
      products.forEach(function (item) {
        item.url = "".concat(item.url, "?scId=").concat(scId, "&policyId=").concat(policyId);
      });
      return products;
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        model: _model__WEBPACK_IMPORTED_MODULE_13__["Modeller"].asProxied({}),
        loading: false,
        isECard: true,
        policyId: ""
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Result.prototype), "componentDidMount", this).call(this);

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.props.match, "params.policyId");

      if (lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(policyId)) {
        this.setState({
          isECard: false
        });
      } else {
        this.setState({
          policyId: policyId
        });
      }

      this.interval = setInterval(this.initData, 5000);
      this.setState({
        loading: true
      });
      this.initData().finally(function () {
        _this2.setState({
          loading: false
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(this.interval);
    }
  }, {
    key: "renderProduct",
    value: function renderProduct(productCate) {
      var _this3 = this;

      var products;

      var scId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "scId");

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "mainPolicyId");

      if (productCate === "SME") {
        products = this.smeCrossSellProducts();
      } else {
        products = [{
          id: "BOSS",
          code: "SME",
          text: "Business Owner Insurance",
          url: "/quote/normal/SAIC/BOSS?scId=".concat(scId, "&policyId=").concat(policyId)
        }, {
          id: "GRPHEALTH",
          code: "GHS",
          text: "Group Health And Surgical",
          url: "/quote/normal/SAIC/GHS?scId=".concat(scId, "&policyId=").concat(policyId)
        }];
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(Style, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "page-title page-title--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("You might also want to buy").thai("คุณอาจสนใจซื้อประกันเหล่านี้เพิ่มเติม").my("သင်တို့သည်လည်းဝယ်ချင်ပေလိမ့်မည်").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Divider"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 161
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "product",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      }, products.map(function (every, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
          key: index,
          onClick: function onClick() {
            _this3.getHelpers().getRouter().pushRedirect(every.url);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 164
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
          className: "iconfont icon-".concat(every.code.toLowerCase()),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 169
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 170
          },
          __self: this
        }, every.text));
      })));
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      var message = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "message", "") || "";
      var crossSellProducts = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "crossSellProducts", []) || [];

      var productCate = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(this.state.model, "productCate");

      if (this.state.loading) return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("section", {
        style: {
          height: "800px",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 185
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_14__["DeskPageBox"], {
        style: {
          padding: 0
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 199
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_16__["ResultPageIcon"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 200
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "check-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 201
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          marginBottom: 20
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_16__["ResultPageParagraph"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, message, " ")), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          marginBottom: 120
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 206
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_components_policy_item__WEBPACK_IMPORTED_MODULE_17__["PolicyItems"], {
        router: this.getHelpers().getRouter(),
        policyId: this.state.policyId,
        isECard: this.state.isECard,
        model: this.state.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 207
        },
        __self: this
      })), crossSellProducts.length > 0 && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_components_also_buy__WEBPACK_IMPORTED_MODULE_18__["AlsoBuyItems"], {
        model: this.state.model,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }), ["TC", "SURETY", "EB", "LIABILITY", "SME"].includes(productCate) && this.renderProduct(productCate), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          marginTop: 20
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 212
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_16__["StyleAction"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 213
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 214
        },
        __self: this
      }, !_common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsInApp() && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__["ButtonBack"], {
        handel: function handel(history) {
          history.push(_common__WEBPACK_IMPORTED_MODULE_11__["PATH"].POLICIES_QUERY, {
            currentTab: "issued"
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 215
        },
        __self: this
      })))));
    }
  }]);

  return Result;
}(_desk_component__WEBPACK_IMPORTED_MODULE_12__["DeskPage"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_10__["withRouter"])(Result));
var Style = _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject(), isMobile && "display: flex;flex-wrap: wrap;", isMobile ? "50%" : "16.6%", COLOR_A);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9zdWNjZXNzL2luZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL2NvbXBvbmVudHMvc3VjY2Vzcy9pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgRGl2aWRlciwgSWNvbiwgU3BpbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSb3V0ZUNvbXBvbmVudFByb3BzLCB3aXRoUm91dGVyIH0gZnJvbSBcInJlYWN0LXJvdXRlclwiO1xuXG5pbXBvcnQgeyBBamF4LCBMYW5ndWFnZSwgUEFUSCwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgUGFnZUNvbXBvbmVudHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBEZXNrUGFnZSB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuaW1wb3J0IHsgRGVza1BhZ2VCb3ggfSBmcm9tIFwiQGRlc2stc3R5bGVzL2dsb2JhbFwiO1xuaW1wb3J0IHsgQnV0dG9uQmFjayB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYW50ZC9idXR0b24tYmFja1wiO1xuaW1wb3J0IHsgUmVzdWx0UGFnZUljb24sIFJlc3VsdFBhZ2VQYXJhZ3JhcGgsIFN0eWxlQWN0aW9uIH0gZnJvbSBcIkBkZXNrLXN0eWxlcy9jb21tb25cIjtcblxuXG5pbXBvcnQgeyBQb2xpY3lJdGVtcyB9IGZyb20gXCIuL2NvbXBvbmVudHMvcG9saWN5LWl0ZW1cIjtcbmltcG9ydCB7IEFsc29CdXlJdGVtcyB9IGZyb20gXCIuL2NvbXBvbmVudHMvYWxzby1idXlcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlc1wiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG50eXBlIElQcm9wcyA9IHt9ICYgUm91dGVDb21wb25lbnRQcm9wcztcblxuaW50ZXJmYWNlIElTdGF0ZSB7XG4gIG1vZGVsOiBhbnk7XG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIGlzRUNhcmQ6IGJvb2xlYW47XG4gIHBvbGljeUlkOiBhbnk7XG59XG5cblxuY2xhc3MgUmVzdWx0PFAgZXh0ZW5kcyBJUHJvcHMsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBEZXNrUGFnZTxQLCBTLCBDPiB7XG4gIGludGVydmFsOiBhbnk7XG5cbiAgc21lQ3Jvc3NTZWxsUHJvZHVjdHMoKSB7XG4gICAgY29uc3Qgc2NJZCA9IF8uZ2V0KHRoaXMuc3RhdGUubW9kZWwsIFwic2NJZFwiKTtcbiAgICBjb25zdCBwb2xpY3lJZCA9IF8uZ2V0KHRoaXMuc3RhdGUubW9kZWwsIFwibWFpblBvbGljeUlkXCIpO1xuICAgIGxldCBwcm9kdWN0cyA9IFtcbiAgICAgIHtcbiAgICAgICAgaWQ6IFwiRUZDXCIsXG4gICAgICAgIGNvZGU6IFwiRUZDXCIsXG4gICAgICAgIHRleHQ6IFwiRW1wbG95ZWUgRmxleENhcmVcIixcbiAgICAgICAgdXJsOiBcIi9xdW90ZS9ub3JtYWwvU0FJQy9FRkNcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiBcIkdSUEhFQUxUSFwiLFxuICAgICAgICBjb2RlOiBcIkdIU1wiLFxuICAgICAgICB0ZXh0OiBcIkdyb3VwIEhlYWx0aCBBbmQgU3VyZ2ljYWxcIixcbiAgICAgICAgdXJsOiBcIi9xdW90ZS9ub3JtYWwvU0FJQy9HSFNcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiBcIkdUTFwiLFxuICAgICAgICBjb2RlOiBcIkdUTFwiLFxuICAgICAgICB0ZXh0OiBcIkdyb3VwIFRlcm0gTGlmZVwiLFxuICAgICAgICB1cmw6IFwiL3F1b3RlL25vcm1hbC9TQUlDL0dUTFwiLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgaWQ6IFwiR1BBXCIsXG4gICAgICAgIGNvZGU6IFwiR1BBXCIsXG4gICAgICAgIHRleHQ6IFwiR3JvdXAgUGVyc29uYWwgQWNjaWRlbnRcIixcbiAgICAgICAgdXJsOiBcIi9xdW90ZS9ub3JtYWwvU0FJQy9HUEFcIixcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGlkOiBcIkZXQlwiLFxuICAgICAgICBjb2RlOiBcIkZXQlwiLFxuICAgICAgICB0ZXh0OiBcIkZvcmVpZ24gV29ya2VyIEJvbmRcIixcbiAgICAgICAgdXJsOiBcIi9xdW90ZS9ub3JtYWwvU0FJQy9GV0JcIixcbiAgICAgIH0sXG4gICAgXTtcbiAgICBwcm9kdWN0cy5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIGl0ZW0udXJsID0gYCR7aXRlbS51cmx9P3NjSWQ9JHtzY0lkfSZwb2xpY3lJZD0ke3BvbGljeUlkfWA7XG4gICAgfSk7XG4gICAgcmV0dXJuIHByb2R1Y3RzO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4ge1xuICAgICAgbW9kZWw6IE1vZGVsbGVyLmFzUHJveGllZCh7fSksXG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgIGlzRUNhcmQ6IHRydWUsXG4gICAgICBwb2xpY3lJZDogXCJcIixcbiAgICB9IGFzIFM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBzdXBlci5jb21wb25lbnREaWRNb3VudCgpO1xuICAgIGxldCBwb2xpY3lJZCA9IF8uZ2V0KHRoaXMucHJvcHMubWF0Y2gsIFwicGFyYW1zLnBvbGljeUlkXCIpO1xuICAgIGlmIChfLmlzRW1wdHkocG9saWN5SWQpKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaXNFQ2FyZDogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHBvbGljeUlkLFxuICAgICAgfSk7XG4gICAgfVxuICAgIHRoaXMuaW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCh0aGlzLmluaXREYXRhLCA1MDAwKTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbG9hZGluZzogdHJ1ZSxcbiAgICB9KTtcbiAgICB0aGlzLmluaXREYXRhKCkuZmluYWxseSgoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XG4gIH1cblxuICBpbml0RGF0YSA9ICgpID0+IHtcbiAgICBjb25zdCB0cmFuc0lkID0gXy5nZXQodGhpcy5wcm9wcy5tYXRjaCwgXCJwYXJhbXMudHJhbnNJZFwiKTtcbiAgICBpZiAoIXRyYW5zSWQpIHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIEFqYXguZ2V0KGAvY2FydC8ke3RyYW5zSWR9L2lzc3VlL3N0YXR1c2AsIHt9LCB7IHNpbGVudDogdHJ1ZSB9KVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICBjb25zdCByZXNEYXRhID0gXy5nZXQocmVzcG9uc2UsIFwiYm9keS5yZXNwRGF0YVwiKTtcbiAgICAgICAgaWYgKCFyZXNEYXRhKSByZXR1cm47XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIG1vZGVsOiByZXNEYXRhLFxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKHJlc0RhdGEgJiYgIXJlc0RhdGEudG9CZVJldHJpZWQpIHtcbiAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuaW50ZXJ2YWwpO1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIH0pO1xuICB9O1xuXG4gIHByb3RlY3RlZCByZW5kZXJQcm9kdWN0KHByb2R1Y3RDYXRlOiBhbnkpIHtcbiAgICBsZXQgcHJvZHVjdHM7XG4gICAgY29uc3Qgc2NJZCA9IF8uZ2V0KHRoaXMuc3RhdGUubW9kZWwsIFwic2NJZFwiKTtcbiAgICBjb25zdCBwb2xpY3lJZCA9IF8uZ2V0KHRoaXMuc3RhdGUubW9kZWwsIFwibWFpblBvbGljeUlkXCIpO1xuXG4gICAgaWYgKHByb2R1Y3RDYXRlID09PSBcIlNNRVwiKSB7XG4gICAgICBwcm9kdWN0cyA9IHRoaXMuc21lQ3Jvc3NTZWxsUHJvZHVjdHMoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcHJvZHVjdHMgPSBbe1xuICAgICAgICBpZDogXCJCT1NTXCIsXG4gICAgICAgIGNvZGU6IFwiU01FXCIsXG4gICAgICAgIHRleHQ6IFwiQnVzaW5lc3MgT3duZXIgSW5zdXJhbmNlXCIsXG4gICAgICAgIHVybDogYC9xdW90ZS9ub3JtYWwvU0FJQy9CT1NTP3NjSWQ9JHtzY0lkfSZwb2xpY3lJZD0ke3BvbGljeUlkfWAsXG4gICAgICB9LCB7XG4gICAgICAgIGlkOiBcIkdSUEhFQUxUSFwiLFxuICAgICAgICBjb2RlOiBcIkdIU1wiLFxuICAgICAgICB0ZXh0OiBcIkdyb3VwIEhlYWx0aCBBbmQgU3VyZ2ljYWxcIixcbiAgICAgICAgdXJsOiBgL3F1b3RlL25vcm1hbC9TQUlDL0dIUz9zY0lkPSR7c2NJZH0mcG9saWN5SWQ9JHtwb2xpY3lJZH1gLFxuICAgICAgfV07XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICA8U3R5bGU+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFnZS10aXRsZSBwYWdlLXRpdGxlLS1tYWluXCI+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiWW91IG1pZ2h0IGFsc28gd2FudCB0byBidXlcIilcbiAgICAgICAgICAgIC50aGFpKFwi4LiE4Li44LiT4Lit4Liy4LiI4Liq4LiZ4LmD4LiI4LiL4Li34LmJ4Lit4Lib4Lij4Liw4LiB4Lix4LiZ4LmA4Lir4Lil4LmI4Liy4LiZ4Li14LmJ4LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihXCIpXG4gICAgICAgICAgICAubXkoXCLhgJ7hgIThgLrhgJDhgK3hgK/hgLfhgJ7hgIrhgLrhgJzhgIrhgLrhgLjhgJ3hgJrhgLrhgIHhgLvhgIThgLrhgJXhgLHhgJzhgK3hgJnhgLrhgLfhgJnhgIrhgLpcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8RGl2aWRlci8+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdFwiPlxuICAgICAgICAgIHtwcm9kdWN0cy5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgIHJldHVybiA8YSBrZXk9e2luZGV4fSBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAgICAgLmdldFJvdXRlcigpXG4gICAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdCgoZXZlcnkudXJsKSk7XG4gICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPXtgaWNvbmZvbnQgaWNvbi0ke2V2ZXJ5LmNvZGUudG9Mb3dlckNhc2UoKX1gfS8+XG4gICAgICAgICAgICAgIDxzcGFuPntldmVyeS50ZXh0fTwvc3Bhbj5cbiAgICAgICAgICAgIDwvYT47XG4gICAgICAgICAgfSl9XG5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L1N0eWxlPlxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyUGFnZUJvZHkoKTogYW55IHtcbiAgICBjb25zdCBtZXNzYWdlID0gXy5nZXQodGhpcy5zdGF0ZS5tb2RlbCwgXCJtZXNzYWdlXCIsIFwiXCIpIHx8IFwiXCI7XG4gICAgY29uc3QgY3Jvc3NTZWxsUHJvZHVjdHM6IGFueSA9IF8uZ2V0KHRoaXMuc3RhdGUubW9kZWwsIFwiY3Jvc3NTZWxsUHJvZHVjdHNcIiwgW10pIHx8IFtdO1xuICAgIGNvbnN0IHByb2R1Y3RDYXRlID0gXy5nZXQodGhpcy5zdGF0ZS5tb2RlbCwgXCJwcm9kdWN0Q2F0ZVwiKTtcbiAgICBpZiAodGhpcy5zdGF0ZS5sb2FkaW5nKVxuICAgICAgcmV0dXJuIChcbiAgICAgICAgPHNlY3Rpb25cbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgaGVpZ2h0OiBcIjgwMHB4XCIsXG4gICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXG4gICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxuICAgICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPFNwaW4gc2l6ZT1cImxhcmdlXCIgc3Bpbm5pbmc9e3RydWV9Lz5cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8RGVza1BhZ2VCb3ggc3R5bGU9e3sgcGFkZGluZzogMCB9fT5cbiAgICAgICAgPFJlc3VsdFBhZ2VJY29uPlxuICAgICAgICAgIDxJY29uIHR5cGU9XCJjaGVjay1jaXJjbGVcIi8+XG4gICAgICAgIDwvUmVzdWx0UGFnZUljb24+XG4gICAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luQm90dG9tOiAyMCB9fT5cbiAgICAgICAgICA8UmVzdWx0UGFnZVBhcmFncmFwaD57bWVzc2FnZX0gPC9SZXN1bHRQYWdlUGFyYWdyYXBoPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBtYXJnaW5Cb3R0b206IDEyMCB9fT5cbiAgICAgICAgICA8UG9saWN5SXRlbXMgcm91dGVyPXt0aGlzLmdldEhlbHBlcnMoKS5nZXRSb3V0ZXIoKX0gcG9saWN5SWQ9e3RoaXMuc3RhdGUucG9saWN5SWR9XG4gICAgICAgICAgICAgICAgICAgICAgIGlzRUNhcmQ9e3RoaXMuc3RhdGUuaXNFQ2FyZH0gbW9kZWw9e3RoaXMuc3RhdGUubW9kZWx9Lz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHtjcm9zc1NlbGxQcm9kdWN0cy5sZW5ndGggPiAwICYmIDxBbHNvQnV5SXRlbXMgbW9kZWw9e3RoaXMuc3RhdGUubW9kZWx9Lz59XG4gICAgICAgIHtbXCJUQ1wiLCBcIlNVUkVUWVwiLCBcIkVCXCIsIFwiTElBQklMSVRZXCIsIFwiU01FXCJdLmluY2x1ZGVzKHByb2R1Y3RDYXRlKSAmJiB0aGlzLnJlbmRlclByb2R1Y3QocHJvZHVjdENhdGUpfVxuICAgICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpblRvcDogMjAgfX0+XG4gICAgICAgICAgPFN0eWxlQWN0aW9uPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgICAgICAgeyFVdGlscy5nZXRJc0luQXBwKCkgJiYgPEJ1dHRvbkJhY2tcbiAgICAgICAgICAgICAgICBoYW5kZWw9eyhoaXN0b3J5OiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaChQQVRILlBPTElDSUVTX1FVRVJZLCB7IGN1cnJlbnRUYWI6IFwiaXNzdWVkXCIgfSk7XG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgLz59XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L1N0eWxlQWN0aW9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvRGVza1BhZ2VCb3g+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoUm91dGVyKFJlc3VsdCk7XG5cblxuY29uc3QgU3R5bGUgPSBTdHlsZWQuZGl2YFxuICAgICAgLnByb2R1Y3Qge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICR7aXNNb2JpbGUgJiYgXCJkaXNwbGF5OiBmbGV4O2ZsZXgtd3JhcDogd3JhcDtcIn1cbiAgICAgICAgYSB7XG4gICAgICAgICAgd2lkdGg6ICR7aXNNb2JpbGUgPyBcIjUwJVwiIDogXCIxNi42JVwifTtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrXG4gICAgICAgICAgY29sb3I6ICMyYzJjMmM7XG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgICAgICBpIHtcbiAgICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIHNwYW4ge1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgfVxuICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgY29sb3I6ICR7Q09MT1JfQX07XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgYDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBK0VBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTs7Ozs7O0FBbEdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQXVCQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBS0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7OztBQWxNQTtBQUNBO0FBb01BO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/success/index.tsx
