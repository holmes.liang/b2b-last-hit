/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getUpdatedSelectionState
 * @format
 * 
 */


var DraftOffsetKey = __webpack_require__(/*! ./DraftOffsetKey */ "./node_modules/draft-js/lib/DraftOffsetKey.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

function getUpdatedSelectionState(editorState, anchorKey, anchorOffset, focusKey, focusOffset) {
  var selection = nullthrows(editorState.getSelection());

  if (true) {
    if (!anchorKey || !focusKey) {
      /*eslint-disable no-console */
      console.warn('Invalid selection state.', arguments, editorState.toJS());
      /*eslint-enable no-console */

      return selection;
    }
  }

  var anchorPath = DraftOffsetKey.decode(anchorKey);
  var anchorBlockKey = anchorPath.blockKey;
  var anchorLeaf = editorState.getBlockTree(anchorBlockKey).getIn([anchorPath.decoratorKey, 'leaves', anchorPath.leafKey]);
  var focusPath = DraftOffsetKey.decode(focusKey);
  var focusBlockKey = focusPath.blockKey;
  var focusLeaf = editorState.getBlockTree(focusBlockKey).getIn([focusPath.decoratorKey, 'leaves', focusPath.leafKey]);
  var anchorLeafStart = anchorLeaf.get('start');
  var focusLeafStart = focusLeaf.get('start');
  var anchorBlockOffset = anchorLeaf ? anchorLeafStart + anchorOffset : null;
  var focusBlockOffset = focusLeaf ? focusLeafStart + focusOffset : null;
  var areEqual = selection.getAnchorKey() === anchorBlockKey && selection.getAnchorOffset() === anchorBlockOffset && selection.getFocusKey() === focusBlockKey && selection.getFocusOffset() === focusBlockOffset;

  if (areEqual) {
    return selection;
  }

  var isBackward = false;

  if (anchorBlockKey === focusBlockKey) {
    var anchorLeafEnd = anchorLeaf.get('end');
    var focusLeafEnd = focusLeaf.get('end');

    if (focusLeafStart === anchorLeafStart && focusLeafEnd === anchorLeafEnd) {
      isBackward = focusOffset < anchorOffset;
    } else {
      isBackward = focusLeafStart < anchorLeafStart;
    }
  } else {
    var startKey = editorState.getCurrentContent().getBlockMap().keySeq().skipUntil(function (v) {
      return v === anchorBlockKey || v === focusBlockKey;
    }).first();
    isBackward = startKey === focusBlockKey;
  }

  return selection.merge({
    anchorKey: anchorBlockKey,
    anchorOffset: anchorBlockOffset,
    focusKey: focusBlockKey,
    focusOffset: focusBlockOffset,
    isBackward: isBackward
  });
}

module.exports = getUpdatedSelectionState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFVwZGF0ZWRTZWxlY3Rpb25TdGF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9nZXRVcGRhdGVkU2VsZWN0aW9uU3RhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBnZXRVcGRhdGVkU2VsZWN0aW9uU3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0T2Zmc2V0S2V5ID0gcmVxdWlyZSgnLi9EcmFmdE9mZnNldEtleScpO1xuXG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcblxuZnVuY3Rpb24gZ2V0VXBkYXRlZFNlbGVjdGlvblN0YXRlKGVkaXRvclN0YXRlLCBhbmNob3JLZXksIGFuY2hvck9mZnNldCwgZm9jdXNLZXksIGZvY3VzT2Zmc2V0KSB7XG4gIHZhciBzZWxlY3Rpb24gPSBudWxsdGhyb3dzKGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpKTtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBpZiAoIWFuY2hvcktleSB8fCAhZm9jdXNLZXkpIHtcbiAgICAgIC8qZXNsaW50LWRpc2FibGUgbm8tY29uc29sZSAqL1xuICAgICAgY29uc29sZS53YXJuKCdJbnZhbGlkIHNlbGVjdGlvbiBzdGF0ZS4nLCBhcmd1bWVudHMsIGVkaXRvclN0YXRlLnRvSlMoKSk7XG4gICAgICAvKmVzbGludC1lbmFibGUgbm8tY29uc29sZSAqL1xuICAgICAgcmV0dXJuIHNlbGVjdGlvbjtcbiAgICB9XG4gIH1cblxuICB2YXIgYW5jaG9yUGF0aCA9IERyYWZ0T2Zmc2V0S2V5LmRlY29kZShhbmNob3JLZXkpO1xuICB2YXIgYW5jaG9yQmxvY2tLZXkgPSBhbmNob3JQYXRoLmJsb2NrS2V5O1xuICB2YXIgYW5jaG9yTGVhZiA9IGVkaXRvclN0YXRlLmdldEJsb2NrVHJlZShhbmNob3JCbG9ja0tleSkuZ2V0SW4oW2FuY2hvclBhdGguZGVjb3JhdG9yS2V5LCAnbGVhdmVzJywgYW5jaG9yUGF0aC5sZWFmS2V5XSk7XG5cbiAgdmFyIGZvY3VzUGF0aCA9IERyYWZ0T2Zmc2V0S2V5LmRlY29kZShmb2N1c0tleSk7XG4gIHZhciBmb2N1c0Jsb2NrS2V5ID0gZm9jdXNQYXRoLmJsb2NrS2V5O1xuICB2YXIgZm9jdXNMZWFmID0gZWRpdG9yU3RhdGUuZ2V0QmxvY2tUcmVlKGZvY3VzQmxvY2tLZXkpLmdldEluKFtmb2N1c1BhdGguZGVjb3JhdG9yS2V5LCAnbGVhdmVzJywgZm9jdXNQYXRoLmxlYWZLZXldKTtcblxuICB2YXIgYW5jaG9yTGVhZlN0YXJ0ID0gYW5jaG9yTGVhZi5nZXQoJ3N0YXJ0Jyk7XG4gIHZhciBmb2N1c0xlYWZTdGFydCA9IGZvY3VzTGVhZi5nZXQoJ3N0YXJ0Jyk7XG5cbiAgdmFyIGFuY2hvckJsb2NrT2Zmc2V0ID0gYW5jaG9yTGVhZiA/IGFuY2hvckxlYWZTdGFydCArIGFuY2hvck9mZnNldCA6IG51bGw7XG4gIHZhciBmb2N1c0Jsb2NrT2Zmc2V0ID0gZm9jdXNMZWFmID8gZm9jdXNMZWFmU3RhcnQgKyBmb2N1c09mZnNldCA6IG51bGw7XG5cbiAgdmFyIGFyZUVxdWFsID0gc2VsZWN0aW9uLmdldEFuY2hvcktleSgpID09PSBhbmNob3JCbG9ja0tleSAmJiBzZWxlY3Rpb24uZ2V0QW5jaG9yT2Zmc2V0KCkgPT09IGFuY2hvckJsb2NrT2Zmc2V0ICYmIHNlbGVjdGlvbi5nZXRGb2N1c0tleSgpID09PSBmb2N1c0Jsb2NrS2V5ICYmIHNlbGVjdGlvbi5nZXRGb2N1c09mZnNldCgpID09PSBmb2N1c0Jsb2NrT2Zmc2V0O1xuXG4gIGlmIChhcmVFcXVhbCkge1xuICAgIHJldHVybiBzZWxlY3Rpb247XG4gIH1cblxuICB2YXIgaXNCYWNrd2FyZCA9IGZhbHNlO1xuICBpZiAoYW5jaG9yQmxvY2tLZXkgPT09IGZvY3VzQmxvY2tLZXkpIHtcbiAgICB2YXIgYW5jaG9yTGVhZkVuZCA9IGFuY2hvckxlYWYuZ2V0KCdlbmQnKTtcbiAgICB2YXIgZm9jdXNMZWFmRW5kID0gZm9jdXNMZWFmLmdldCgnZW5kJyk7XG4gICAgaWYgKGZvY3VzTGVhZlN0YXJ0ID09PSBhbmNob3JMZWFmU3RhcnQgJiYgZm9jdXNMZWFmRW5kID09PSBhbmNob3JMZWFmRW5kKSB7XG4gICAgICBpc0JhY2t3YXJkID0gZm9jdXNPZmZzZXQgPCBhbmNob3JPZmZzZXQ7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlzQmFja3dhcmQgPSBmb2N1c0xlYWZTdGFydCA8IGFuY2hvckxlYWZTdGFydDtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIHN0YXJ0S2V5ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKS5nZXRCbG9ja01hcCgpLmtleVNlcSgpLnNraXBVbnRpbChmdW5jdGlvbiAodikge1xuICAgICAgcmV0dXJuIHYgPT09IGFuY2hvckJsb2NrS2V5IHx8IHYgPT09IGZvY3VzQmxvY2tLZXk7XG4gICAgfSkuZmlyc3QoKTtcbiAgICBpc0JhY2t3YXJkID0gc3RhcnRLZXkgPT09IGZvY3VzQmxvY2tLZXk7XG4gIH1cblxuICByZXR1cm4gc2VsZWN0aW9uLm1lcmdlKHtcbiAgICBhbmNob3JLZXk6IGFuY2hvckJsb2NrS2V5LFxuICAgIGFuY2hvck9mZnNldDogYW5jaG9yQmxvY2tPZmZzZXQsXG4gICAgZm9jdXNLZXk6IGZvY3VzQmxvY2tLZXksXG4gICAgZm9jdXNPZmZzZXQ6IGZvY3VzQmxvY2tPZmZzZXQsXG4gICAgaXNCYWNrd2FyZDogaXNCYWNrd2FyZFxuICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRVcGRhdGVkU2VsZWN0aW9uU3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getUpdatedSelectionState.js
