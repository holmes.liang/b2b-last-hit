/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var textContain = __webpack_require__(/*! zrender/lib/contain/text */ "./node_modules/zrender/lib/contain/text.js");

var _model = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;

var _axisHelper = __webpack_require__(/*! ./axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var makeLabelFormatter = _axisHelper.makeLabelFormatter;
var getOptionCategoryInterval = _axisHelper.getOptionCategoryInterval;
var shouldShowAllLabels = _axisHelper.shouldShowAllLabels;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = makeInner();
/**
 * @param {module:echats/coord/Axis} axis
 * @return {Object} {
 *     labels: [{
 *         formattedLabel: string,
 *         rawLabel: string,
 *         tickValue: number
 *     }, ...],
 *     labelCategoryInterval: number
 * }
 */

function createAxisLabels(axis) {
  // Only ordinal scale support tick interval
  return axis.type === 'category' ? makeCategoryLabels(axis) : makeRealNumberLabels(axis);
}
/**
 * @param {module:echats/coord/Axis} axis
 * @param {module:echarts/model/Model} tickModel For example, can be axisTick, splitLine, splitArea.
 * @return {Object} {
 *     ticks: Array.<number>
 *     tickCategoryInterval: number
 * }
 */


function createAxisTicks(axis, tickModel) {
  // Only ordinal scale support tick interval
  return axis.type === 'category' ? makeCategoryTicks(axis, tickModel) : {
    ticks: axis.scale.getTicks()
  };
}

function makeCategoryLabels(axis) {
  var labelModel = axis.getLabelModel();
  var result = makeCategoryLabelsActually(axis, labelModel);
  return !labelModel.get('show') || axis.scale.isBlank() ? {
    labels: [],
    labelCategoryInterval: result.labelCategoryInterval
  } : result;
}

function makeCategoryLabelsActually(axis, labelModel) {
  var labelsCache = getListCache(axis, 'labels');
  var optionLabelInterval = getOptionCategoryInterval(labelModel);
  var result = listCacheGet(labelsCache, optionLabelInterval);

  if (result) {
    return result;
  }

  var labels;
  var numericLabelInterval;

  if (zrUtil.isFunction(optionLabelInterval)) {
    labels = makeLabelsByCustomizedCategoryInterval(axis, optionLabelInterval);
  } else {
    numericLabelInterval = optionLabelInterval === 'auto' ? makeAutoCategoryInterval(axis) : optionLabelInterval;
    labels = makeLabelsByNumericCategoryInterval(axis, numericLabelInterval);
  } // Cache to avoid calling interval function repeatly.


  return listCacheSet(labelsCache, optionLabelInterval, {
    labels: labels,
    labelCategoryInterval: numericLabelInterval
  });
}

function makeCategoryTicks(axis, tickModel) {
  var ticksCache = getListCache(axis, 'ticks');
  var optionTickInterval = getOptionCategoryInterval(tickModel);
  var result = listCacheGet(ticksCache, optionTickInterval);

  if (result) {
    return result;
  }

  var ticks;
  var tickCategoryInterval; // Optimize for the case that large category data and no label displayed,
  // we should not return all ticks.

  if (!tickModel.get('show') || axis.scale.isBlank()) {
    ticks = [];
  }

  if (zrUtil.isFunction(optionTickInterval)) {
    ticks = makeLabelsByCustomizedCategoryInterval(axis, optionTickInterval, true);
  } // Always use label interval by default despite label show. Consider this
  // scenario, Use multiple grid with the xAxis sync, and only one xAxis shows
  // labels. `splitLine` and `axisTick` should be consistent in this case.
  else if (optionTickInterval === 'auto') {
      var labelsResult = makeCategoryLabelsActually(axis, axis.getLabelModel());
      tickCategoryInterval = labelsResult.labelCategoryInterval;
      ticks = zrUtil.map(labelsResult.labels, function (labelItem) {
        return labelItem.tickValue;
      });
    } else {
      tickCategoryInterval = optionTickInterval;
      ticks = makeLabelsByNumericCategoryInterval(axis, tickCategoryInterval, true);
    } // Cache to avoid calling interval function repeatly.


  return listCacheSet(ticksCache, optionTickInterval, {
    ticks: ticks,
    tickCategoryInterval: tickCategoryInterval
  });
}

function makeRealNumberLabels(axis) {
  var ticks = axis.scale.getTicks();
  var labelFormatter = makeLabelFormatter(axis);
  return {
    labels: zrUtil.map(ticks, function (tickValue, idx) {
      return {
        formattedLabel: labelFormatter(tickValue, idx),
        rawLabel: axis.scale.getLabel(tickValue),
        tickValue: tickValue
      };
    })
  };
} // Large category data calculation is performence sensitive, and ticks and label
// probably be fetched by multiple times. So we cache the result.
// axis is created each time during a ec process, so we do not need to clear cache.


function getListCache(axis, prop) {
  // Because key can be funciton, and cache size always be small, we use array cache.
  return inner(axis)[prop] || (inner(axis)[prop] = []);
}

function listCacheGet(cache, key) {
  for (var i = 0; i < cache.length; i++) {
    if (cache[i].key === key) {
      return cache[i].value;
    }
  }
}

function listCacheSet(cache, key, value) {
  cache.push({
    key: key,
    value: value
  });
  return value;
}

function makeAutoCategoryInterval(axis) {
  var result = inner(axis).autoInterval;
  return result != null ? result : inner(axis).autoInterval = axis.calculateCategoryInterval();
}
/**
 * Calculate interval for category axis ticks and labels.
 * To get precise result, at least one of `getRotate` and `isHorizontal`
 * should be implemented in axis.
 */


function calculateCategoryInterval(axis) {
  var params = fetchAutoCategoryIntervalCalculationParams(axis);
  var labelFormatter = makeLabelFormatter(axis);
  var rotation = (params.axisRotate - params.labelRotate) / 180 * Math.PI;
  var ordinalScale = axis.scale;
  var ordinalExtent = ordinalScale.getExtent(); // Providing this method is for optimization:
  // avoid generating a long array by `getTicks`
  // in large category data case.

  var tickCount = ordinalScale.count();

  if (ordinalExtent[1] - ordinalExtent[0] < 1) {
    return 0;
  }

  var step = 1; // Simple optimization. Empirical value: tick count should less than 40.

  if (tickCount > 40) {
    step = Math.max(1, Math.floor(tickCount / 40));
  }

  var tickValue = ordinalExtent[0];
  var unitSpan = axis.dataToCoord(tickValue + 1) - axis.dataToCoord(tickValue);
  var unitW = Math.abs(unitSpan * Math.cos(rotation));
  var unitH = Math.abs(unitSpan * Math.sin(rotation));
  var maxW = 0;
  var maxH = 0; // Caution: Performance sensitive for large category data.
  // Consider dataZoom, we should make appropriate step to avoid O(n) loop.

  for (; tickValue <= ordinalExtent[1]; tickValue += step) {
    var width = 0;
    var height = 0; // Not precise, do not consider align and vertical align
    // and each distance from axis line yet.

    var rect = textContain.getBoundingRect(labelFormatter(tickValue), params.font, 'center', 'top'); // Magic number

    width = rect.width * 1.3;
    height = rect.height * 1.3; // Min size, void long loop.

    maxW = Math.max(maxW, width, 7);
    maxH = Math.max(maxH, height, 7);
  }

  var dw = maxW / unitW;
  var dh = maxH / unitH; // 0/0 is NaN, 1/0 is Infinity.

  isNaN(dw) && (dw = Infinity);
  isNaN(dh) && (dh = Infinity);
  var interval = Math.max(0, Math.floor(Math.min(dw, dh)));
  var cache = inner(axis.model);
  var lastAutoInterval = cache.lastAutoInterval;
  var lastTickCount = cache.lastTickCount; // Use cache to keep interval stable while moving zoom window,
  // otherwise the calculated interval might jitter when the zoom
  // window size is close to the interval-changing size.

  if (lastAutoInterval != null && lastTickCount != null && Math.abs(lastAutoInterval - interval) <= 1 && Math.abs(lastTickCount - tickCount) <= 1 // Always choose the bigger one, otherwise the critical
  // point is not the same when zooming in or zooming out.
  && lastAutoInterval > interval) {
    interval = lastAutoInterval;
  } // Only update cache if cache not used, otherwise the
  // changing of interval is too insensitive.
  else {
      cache.lastTickCount = tickCount;
      cache.lastAutoInterval = interval;
    }

  return interval;
}

function fetchAutoCategoryIntervalCalculationParams(axis) {
  var labelModel = axis.getLabelModel();
  return {
    axisRotate: axis.getRotate ? axis.getRotate() : axis.isHorizontal && !axis.isHorizontal() ? 90 : 0,
    labelRotate: labelModel.get('rotate') || 0,
    font: labelModel.getFont()
  };
}

function makeLabelsByNumericCategoryInterval(axis, categoryInterval, onlyTick) {
  var labelFormatter = makeLabelFormatter(axis);
  var ordinalScale = axis.scale;
  var ordinalExtent = ordinalScale.getExtent();
  var labelModel = axis.getLabelModel();
  var result = []; // TODO: axisType: ordinalTime, pick the tick from each month/day/year/...

  var step = Math.max((categoryInterval || 0) + 1, 1);
  var startTick = ordinalExtent[0];
  var tickCount = ordinalScale.count(); // Calculate start tick based on zero if possible to keep label consistent
  // while zooming and moving while interval > 0. Otherwise the selection
  // of displayable ticks and symbols probably keep changing.
  // 3 is empirical value.

  if (startTick !== 0 && step > 1 && tickCount / step > 2) {
    startTick = Math.round(Math.ceil(startTick / step) * step);
  } // (1) Only add min max label here but leave overlap checking
  // to render stage, which also ensure the returned list
  // suitable for splitLine and splitArea rendering.
  // (2) Scales except category always contain min max label so
  // do not need to perform this process.


  var showAllLabel = shouldShowAllLabels(axis);
  var includeMinLabel = labelModel.get('showMinLabel') || showAllLabel;
  var includeMaxLabel = labelModel.get('showMaxLabel') || showAllLabel;

  if (includeMinLabel && startTick !== ordinalExtent[0]) {
    addItem(ordinalExtent[0]);
  } // Optimize: avoid generating large array by `ordinalScale.getTicks()`.


  var tickValue = startTick;

  for (; tickValue <= ordinalExtent[1]; tickValue += step) {
    addItem(tickValue);
  }

  if (includeMaxLabel && tickValue !== ordinalExtent[1]) {
    addItem(ordinalExtent[1]);
  }

  function addItem(tVal) {
    result.push(onlyTick ? tVal : {
      formattedLabel: labelFormatter(tVal),
      rawLabel: ordinalScale.getLabel(tVal),
      tickValue: tVal
    });
  }

  return result;
} // When interval is function, the result `false` means ignore the tick.
// It is time consuming for large category data.


function makeLabelsByCustomizedCategoryInterval(axis, categoryInterval, onlyTick) {
  var ordinalScale = axis.scale;
  var labelFormatter = makeLabelFormatter(axis);
  var result = [];
  zrUtil.each(ordinalScale.getTicks(), function (tickValue) {
    var rawLabel = ordinalScale.getLabel(tickValue);

    if (categoryInterval(tickValue, rawLabel)) {
      result.push(onlyTick ? tickValue : {
        formattedLabel: labelFormatter(tickValue),
        rawLabel: rawLabel,
        tickValue: tickValue
      });
    }
  });
  return result;
}

exports.createAxisLabels = createAxisLabels;
exports.createAxisTicks = createAxisTicks;
exports.calculateCategoryInterval = calculateCategoryInterval;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvYXhpc1RpY2tMYWJlbEJ1aWxkZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb29yZC9heGlzVGlja0xhYmVsQnVpbGRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciB0ZXh0Q29udGFpbiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb250YWluL3RleHRcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIG1ha2VJbm5lciA9IF9tb2RlbC5tYWtlSW5uZXI7XG5cbnZhciBfYXhpc0hlbHBlciA9IHJlcXVpcmUoXCIuL2F4aXNIZWxwZXJcIik7XG5cbnZhciBtYWtlTGFiZWxGb3JtYXR0ZXIgPSBfYXhpc0hlbHBlci5tYWtlTGFiZWxGb3JtYXR0ZXI7XG52YXIgZ2V0T3B0aW9uQ2F0ZWdvcnlJbnRlcnZhbCA9IF9heGlzSGVscGVyLmdldE9wdGlvbkNhdGVnb3J5SW50ZXJ2YWw7XG52YXIgc2hvdWxkU2hvd0FsbExhYmVscyA9IF9heGlzSGVscGVyLnNob3VsZFNob3dBbGxMYWJlbHM7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBpbm5lciA9IG1ha2VJbm5lcigpO1xuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhdHMvY29vcmQvQXhpc30gYXhpc1xuICogQHJldHVybiB7T2JqZWN0fSB7XG4gKiAgICAgbGFiZWxzOiBbe1xuICogICAgICAgICBmb3JtYXR0ZWRMYWJlbDogc3RyaW5nLFxuICogICAgICAgICByYXdMYWJlbDogc3RyaW5nLFxuICogICAgICAgICB0aWNrVmFsdWU6IG51bWJlclxuICogICAgIH0sIC4uLl0sXG4gKiAgICAgbGFiZWxDYXRlZ29yeUludGVydmFsOiBudW1iZXJcbiAqIH1cbiAqL1xuXG5mdW5jdGlvbiBjcmVhdGVBeGlzTGFiZWxzKGF4aXMpIHtcbiAgLy8gT25seSBvcmRpbmFsIHNjYWxlIHN1cHBvcnQgdGljayBpbnRlcnZhbFxuICByZXR1cm4gYXhpcy50eXBlID09PSAnY2F0ZWdvcnknID8gbWFrZUNhdGVnb3J5TGFiZWxzKGF4aXMpIDogbWFrZVJlYWxOdW1iZXJMYWJlbHMoYXhpcyk7XG59XG4vKipcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGF0cy9jb29yZC9BeGlzfSBheGlzXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSB0aWNrTW9kZWwgRm9yIGV4YW1wbGUsIGNhbiBiZSBheGlzVGljaywgc3BsaXRMaW5lLCBzcGxpdEFyZWEuXG4gKiBAcmV0dXJuIHtPYmplY3R9IHtcbiAqICAgICB0aWNrczogQXJyYXkuPG51bWJlcj5cbiAqICAgICB0aWNrQ2F0ZWdvcnlJbnRlcnZhbDogbnVtYmVyXG4gKiB9XG4gKi9cblxuXG5mdW5jdGlvbiBjcmVhdGVBeGlzVGlja3MoYXhpcywgdGlja01vZGVsKSB7XG4gIC8vIE9ubHkgb3JkaW5hbCBzY2FsZSBzdXBwb3J0IHRpY2sgaW50ZXJ2YWxcbiAgcmV0dXJuIGF4aXMudHlwZSA9PT0gJ2NhdGVnb3J5JyA/IG1ha2VDYXRlZ29yeVRpY2tzKGF4aXMsIHRpY2tNb2RlbCkgOiB7XG4gICAgdGlja3M6IGF4aXMuc2NhbGUuZ2V0VGlja3MoKVxuICB9O1xufVxuXG5mdW5jdGlvbiBtYWtlQ2F0ZWdvcnlMYWJlbHMoYXhpcykge1xuICB2YXIgbGFiZWxNb2RlbCA9IGF4aXMuZ2V0TGFiZWxNb2RlbCgpO1xuICB2YXIgcmVzdWx0ID0gbWFrZUNhdGVnb3J5TGFiZWxzQWN0dWFsbHkoYXhpcywgbGFiZWxNb2RlbCk7XG4gIHJldHVybiAhbGFiZWxNb2RlbC5nZXQoJ3Nob3cnKSB8fCBheGlzLnNjYWxlLmlzQmxhbmsoKSA/IHtcbiAgICBsYWJlbHM6IFtdLFxuICAgIGxhYmVsQ2F0ZWdvcnlJbnRlcnZhbDogcmVzdWx0LmxhYmVsQ2F0ZWdvcnlJbnRlcnZhbFxuICB9IDogcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBtYWtlQ2F0ZWdvcnlMYWJlbHNBY3R1YWxseShheGlzLCBsYWJlbE1vZGVsKSB7XG4gIHZhciBsYWJlbHNDYWNoZSA9IGdldExpc3RDYWNoZShheGlzLCAnbGFiZWxzJyk7XG4gIHZhciBvcHRpb25MYWJlbEludGVydmFsID0gZ2V0T3B0aW9uQ2F0ZWdvcnlJbnRlcnZhbChsYWJlbE1vZGVsKTtcbiAgdmFyIHJlc3VsdCA9IGxpc3RDYWNoZUdldChsYWJlbHNDYWNoZSwgb3B0aW9uTGFiZWxJbnRlcnZhbCk7XG5cbiAgaWYgKHJlc3VsdCkge1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cblxuICB2YXIgbGFiZWxzO1xuICB2YXIgbnVtZXJpY0xhYmVsSW50ZXJ2YWw7XG5cbiAgaWYgKHpyVXRpbC5pc0Z1bmN0aW9uKG9wdGlvbkxhYmVsSW50ZXJ2YWwpKSB7XG4gICAgbGFiZWxzID0gbWFrZUxhYmVsc0J5Q3VzdG9taXplZENhdGVnb3J5SW50ZXJ2YWwoYXhpcywgb3B0aW9uTGFiZWxJbnRlcnZhbCk7XG4gIH0gZWxzZSB7XG4gICAgbnVtZXJpY0xhYmVsSW50ZXJ2YWwgPSBvcHRpb25MYWJlbEludGVydmFsID09PSAnYXV0bycgPyBtYWtlQXV0b0NhdGVnb3J5SW50ZXJ2YWwoYXhpcykgOiBvcHRpb25MYWJlbEludGVydmFsO1xuICAgIGxhYmVscyA9IG1ha2VMYWJlbHNCeU51bWVyaWNDYXRlZ29yeUludGVydmFsKGF4aXMsIG51bWVyaWNMYWJlbEludGVydmFsKTtcbiAgfSAvLyBDYWNoZSB0byBhdm9pZCBjYWxsaW5nIGludGVydmFsIGZ1bmN0aW9uIHJlcGVhdGx5LlxuXG5cbiAgcmV0dXJuIGxpc3RDYWNoZVNldChsYWJlbHNDYWNoZSwgb3B0aW9uTGFiZWxJbnRlcnZhbCwge1xuICAgIGxhYmVsczogbGFiZWxzLFxuICAgIGxhYmVsQ2F0ZWdvcnlJbnRlcnZhbDogbnVtZXJpY0xhYmVsSW50ZXJ2YWxcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIG1ha2VDYXRlZ29yeVRpY2tzKGF4aXMsIHRpY2tNb2RlbCkge1xuICB2YXIgdGlja3NDYWNoZSA9IGdldExpc3RDYWNoZShheGlzLCAndGlja3MnKTtcbiAgdmFyIG9wdGlvblRpY2tJbnRlcnZhbCA9IGdldE9wdGlvbkNhdGVnb3J5SW50ZXJ2YWwodGlja01vZGVsKTtcbiAgdmFyIHJlc3VsdCA9IGxpc3RDYWNoZUdldCh0aWNrc0NhY2hlLCBvcHRpb25UaWNrSW50ZXJ2YWwpO1xuXG4gIGlmIChyZXN1bHQpIHtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG5cbiAgdmFyIHRpY2tzO1xuICB2YXIgdGlja0NhdGVnb3J5SW50ZXJ2YWw7IC8vIE9wdGltaXplIGZvciB0aGUgY2FzZSB0aGF0IGxhcmdlIGNhdGVnb3J5IGRhdGEgYW5kIG5vIGxhYmVsIGRpc3BsYXllZCxcbiAgLy8gd2Ugc2hvdWxkIG5vdCByZXR1cm4gYWxsIHRpY2tzLlxuXG4gIGlmICghdGlja01vZGVsLmdldCgnc2hvdycpIHx8IGF4aXMuc2NhbGUuaXNCbGFuaygpKSB7XG4gICAgdGlja3MgPSBbXTtcbiAgfVxuXG4gIGlmICh6clV0aWwuaXNGdW5jdGlvbihvcHRpb25UaWNrSW50ZXJ2YWwpKSB7XG4gICAgdGlja3MgPSBtYWtlTGFiZWxzQnlDdXN0b21pemVkQ2F0ZWdvcnlJbnRlcnZhbChheGlzLCBvcHRpb25UaWNrSW50ZXJ2YWwsIHRydWUpO1xuICB9IC8vIEFsd2F5cyB1c2UgbGFiZWwgaW50ZXJ2YWwgYnkgZGVmYXVsdCBkZXNwaXRlIGxhYmVsIHNob3cuIENvbnNpZGVyIHRoaXNcbiAgLy8gc2NlbmFyaW8sIFVzZSBtdWx0aXBsZSBncmlkIHdpdGggdGhlIHhBeGlzIHN5bmMsIGFuZCBvbmx5IG9uZSB4QXhpcyBzaG93c1xuICAvLyBsYWJlbHMuIGBzcGxpdExpbmVgIGFuZCBgYXhpc1RpY2tgIHNob3VsZCBiZSBjb25zaXN0ZW50IGluIHRoaXMgY2FzZS5cbiAgZWxzZSBpZiAob3B0aW9uVGlja0ludGVydmFsID09PSAnYXV0bycpIHtcbiAgICAgIHZhciBsYWJlbHNSZXN1bHQgPSBtYWtlQ2F0ZWdvcnlMYWJlbHNBY3R1YWxseShheGlzLCBheGlzLmdldExhYmVsTW9kZWwoKSk7XG4gICAgICB0aWNrQ2F0ZWdvcnlJbnRlcnZhbCA9IGxhYmVsc1Jlc3VsdC5sYWJlbENhdGVnb3J5SW50ZXJ2YWw7XG4gICAgICB0aWNrcyA9IHpyVXRpbC5tYXAobGFiZWxzUmVzdWx0LmxhYmVscywgZnVuY3Rpb24gKGxhYmVsSXRlbSkge1xuICAgICAgICByZXR1cm4gbGFiZWxJdGVtLnRpY2tWYWx1ZTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aWNrQ2F0ZWdvcnlJbnRlcnZhbCA9IG9wdGlvblRpY2tJbnRlcnZhbDtcbiAgICAgIHRpY2tzID0gbWFrZUxhYmVsc0J5TnVtZXJpY0NhdGVnb3J5SW50ZXJ2YWwoYXhpcywgdGlja0NhdGVnb3J5SW50ZXJ2YWwsIHRydWUpO1xuICAgIH0gLy8gQ2FjaGUgdG8gYXZvaWQgY2FsbGluZyBpbnRlcnZhbCBmdW5jdGlvbiByZXBlYXRseS5cblxuXG4gIHJldHVybiBsaXN0Q2FjaGVTZXQodGlja3NDYWNoZSwgb3B0aW9uVGlja0ludGVydmFsLCB7XG4gICAgdGlja3M6IHRpY2tzLFxuICAgIHRpY2tDYXRlZ29yeUludGVydmFsOiB0aWNrQ2F0ZWdvcnlJbnRlcnZhbFxuICB9KTtcbn1cblxuZnVuY3Rpb24gbWFrZVJlYWxOdW1iZXJMYWJlbHMoYXhpcykge1xuICB2YXIgdGlja3MgPSBheGlzLnNjYWxlLmdldFRpY2tzKCk7XG4gIHZhciBsYWJlbEZvcm1hdHRlciA9IG1ha2VMYWJlbEZvcm1hdHRlcihheGlzKTtcbiAgcmV0dXJuIHtcbiAgICBsYWJlbHM6IHpyVXRpbC5tYXAodGlja3MsIGZ1bmN0aW9uICh0aWNrVmFsdWUsIGlkeCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZm9ybWF0dGVkTGFiZWw6IGxhYmVsRm9ybWF0dGVyKHRpY2tWYWx1ZSwgaWR4KSxcbiAgICAgICAgcmF3TGFiZWw6IGF4aXMuc2NhbGUuZ2V0TGFiZWwodGlja1ZhbHVlKSxcbiAgICAgICAgdGlja1ZhbHVlOiB0aWNrVmFsdWVcbiAgICAgIH07XG4gICAgfSlcbiAgfTtcbn0gLy8gTGFyZ2UgY2F0ZWdvcnkgZGF0YSBjYWxjdWxhdGlvbiBpcyBwZXJmb3JtZW5jZSBzZW5zaXRpdmUsIGFuZCB0aWNrcyBhbmQgbGFiZWxcbi8vIHByb2JhYmx5IGJlIGZldGNoZWQgYnkgbXVsdGlwbGUgdGltZXMuIFNvIHdlIGNhY2hlIHRoZSByZXN1bHQuXG4vLyBheGlzIGlzIGNyZWF0ZWQgZWFjaCB0aW1lIGR1cmluZyBhIGVjIHByb2Nlc3MsIHNvIHdlIGRvIG5vdCBuZWVkIHRvIGNsZWFyIGNhY2hlLlxuXG5cbmZ1bmN0aW9uIGdldExpc3RDYWNoZShheGlzLCBwcm9wKSB7XG4gIC8vIEJlY2F1c2Uga2V5IGNhbiBiZSBmdW5jaXRvbiwgYW5kIGNhY2hlIHNpemUgYWx3YXlzIGJlIHNtYWxsLCB3ZSB1c2UgYXJyYXkgY2FjaGUuXG4gIHJldHVybiBpbm5lcihheGlzKVtwcm9wXSB8fCAoaW5uZXIoYXhpcylbcHJvcF0gPSBbXSk7XG59XG5cbmZ1bmN0aW9uIGxpc3RDYWNoZUdldChjYWNoZSwga2V5KSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY2FjaGUubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoY2FjaGVbaV0ua2V5ID09PSBrZXkpIHtcbiAgICAgIHJldHVybiBjYWNoZVtpXS52YWx1ZTtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gbGlzdENhY2hlU2V0KGNhY2hlLCBrZXksIHZhbHVlKSB7XG4gIGNhY2hlLnB1c2goe1xuICAgIGtleToga2V5LFxuICAgIHZhbHVlOiB2YWx1ZVxuICB9KTtcbiAgcmV0dXJuIHZhbHVlO1xufVxuXG5mdW5jdGlvbiBtYWtlQXV0b0NhdGVnb3J5SW50ZXJ2YWwoYXhpcykge1xuICB2YXIgcmVzdWx0ID0gaW5uZXIoYXhpcykuYXV0b0ludGVydmFsO1xuICByZXR1cm4gcmVzdWx0ICE9IG51bGwgPyByZXN1bHQgOiBpbm5lcihheGlzKS5hdXRvSW50ZXJ2YWwgPSBheGlzLmNhbGN1bGF0ZUNhdGVnb3J5SW50ZXJ2YWwoKTtcbn1cbi8qKlxuICogQ2FsY3VsYXRlIGludGVydmFsIGZvciBjYXRlZ29yeSBheGlzIHRpY2tzIGFuZCBsYWJlbHMuXG4gKiBUbyBnZXQgcHJlY2lzZSByZXN1bHQsIGF0IGxlYXN0IG9uZSBvZiBgZ2V0Um90YXRlYCBhbmQgYGlzSG9yaXpvbnRhbGBcbiAqIHNob3VsZCBiZSBpbXBsZW1lbnRlZCBpbiBheGlzLlxuICovXG5cblxuZnVuY3Rpb24gY2FsY3VsYXRlQ2F0ZWdvcnlJbnRlcnZhbChheGlzKSB7XG4gIHZhciBwYXJhbXMgPSBmZXRjaEF1dG9DYXRlZ29yeUludGVydmFsQ2FsY3VsYXRpb25QYXJhbXMoYXhpcyk7XG4gIHZhciBsYWJlbEZvcm1hdHRlciA9IG1ha2VMYWJlbEZvcm1hdHRlcihheGlzKTtcbiAgdmFyIHJvdGF0aW9uID0gKHBhcmFtcy5heGlzUm90YXRlIC0gcGFyYW1zLmxhYmVsUm90YXRlKSAvIDE4MCAqIE1hdGguUEk7XG4gIHZhciBvcmRpbmFsU2NhbGUgPSBheGlzLnNjYWxlO1xuICB2YXIgb3JkaW5hbEV4dGVudCA9IG9yZGluYWxTY2FsZS5nZXRFeHRlbnQoKTsgLy8gUHJvdmlkaW5nIHRoaXMgbWV0aG9kIGlzIGZvciBvcHRpbWl6YXRpb246XG4gIC8vIGF2b2lkIGdlbmVyYXRpbmcgYSBsb25nIGFycmF5IGJ5IGBnZXRUaWNrc2BcbiAgLy8gaW4gbGFyZ2UgY2F0ZWdvcnkgZGF0YSBjYXNlLlxuXG4gIHZhciB0aWNrQ291bnQgPSBvcmRpbmFsU2NhbGUuY291bnQoKTtcblxuICBpZiAob3JkaW5hbEV4dGVudFsxXSAtIG9yZGluYWxFeHRlbnRbMF0gPCAxKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB2YXIgc3RlcCA9IDE7IC8vIFNpbXBsZSBvcHRpbWl6YXRpb24uIEVtcGlyaWNhbCB2YWx1ZTogdGljayBjb3VudCBzaG91bGQgbGVzcyB0aGFuIDQwLlxuXG4gIGlmICh0aWNrQ291bnQgPiA0MCkge1xuICAgIHN0ZXAgPSBNYXRoLm1heCgxLCBNYXRoLmZsb29yKHRpY2tDb3VudCAvIDQwKSk7XG4gIH1cblxuICB2YXIgdGlja1ZhbHVlID0gb3JkaW5hbEV4dGVudFswXTtcbiAgdmFyIHVuaXRTcGFuID0gYXhpcy5kYXRhVG9Db29yZCh0aWNrVmFsdWUgKyAxKSAtIGF4aXMuZGF0YVRvQ29vcmQodGlja1ZhbHVlKTtcbiAgdmFyIHVuaXRXID0gTWF0aC5hYnModW5pdFNwYW4gKiBNYXRoLmNvcyhyb3RhdGlvbikpO1xuICB2YXIgdW5pdEggPSBNYXRoLmFicyh1bml0U3BhbiAqIE1hdGguc2luKHJvdGF0aW9uKSk7XG4gIHZhciBtYXhXID0gMDtcbiAgdmFyIG1heEggPSAwOyAvLyBDYXV0aW9uOiBQZXJmb3JtYW5jZSBzZW5zaXRpdmUgZm9yIGxhcmdlIGNhdGVnb3J5IGRhdGEuXG4gIC8vIENvbnNpZGVyIGRhdGFab29tLCB3ZSBzaG91bGQgbWFrZSBhcHByb3ByaWF0ZSBzdGVwIHRvIGF2b2lkIE8obikgbG9vcC5cblxuICBmb3IgKDsgdGlja1ZhbHVlIDw9IG9yZGluYWxFeHRlbnRbMV07IHRpY2tWYWx1ZSArPSBzdGVwKSB7XG4gICAgdmFyIHdpZHRoID0gMDtcbiAgICB2YXIgaGVpZ2h0ID0gMDsgLy8gTm90IHByZWNpc2UsIGRvIG5vdCBjb25zaWRlciBhbGlnbiBhbmQgdmVydGljYWwgYWxpZ25cbiAgICAvLyBhbmQgZWFjaCBkaXN0YW5jZSBmcm9tIGF4aXMgbGluZSB5ZXQuXG5cbiAgICB2YXIgcmVjdCA9IHRleHRDb250YWluLmdldEJvdW5kaW5nUmVjdChsYWJlbEZvcm1hdHRlcih0aWNrVmFsdWUpLCBwYXJhbXMuZm9udCwgJ2NlbnRlcicsICd0b3AnKTsgLy8gTWFnaWMgbnVtYmVyXG5cbiAgICB3aWR0aCA9IHJlY3Qud2lkdGggKiAxLjM7XG4gICAgaGVpZ2h0ID0gcmVjdC5oZWlnaHQgKiAxLjM7IC8vIE1pbiBzaXplLCB2b2lkIGxvbmcgbG9vcC5cblxuICAgIG1heFcgPSBNYXRoLm1heChtYXhXLCB3aWR0aCwgNyk7XG4gICAgbWF4SCA9IE1hdGgubWF4KG1heEgsIGhlaWdodCwgNyk7XG4gIH1cblxuICB2YXIgZHcgPSBtYXhXIC8gdW5pdFc7XG4gIHZhciBkaCA9IG1heEggLyB1bml0SDsgLy8gMC8wIGlzIE5hTiwgMS8wIGlzIEluZmluaXR5LlxuXG4gIGlzTmFOKGR3KSAmJiAoZHcgPSBJbmZpbml0eSk7XG4gIGlzTmFOKGRoKSAmJiAoZGggPSBJbmZpbml0eSk7XG4gIHZhciBpbnRlcnZhbCA9IE1hdGgubWF4KDAsIE1hdGguZmxvb3IoTWF0aC5taW4oZHcsIGRoKSkpO1xuICB2YXIgY2FjaGUgPSBpbm5lcihheGlzLm1vZGVsKTtcbiAgdmFyIGxhc3RBdXRvSW50ZXJ2YWwgPSBjYWNoZS5sYXN0QXV0b0ludGVydmFsO1xuICB2YXIgbGFzdFRpY2tDb3VudCA9IGNhY2hlLmxhc3RUaWNrQ291bnQ7IC8vIFVzZSBjYWNoZSB0byBrZWVwIGludGVydmFsIHN0YWJsZSB3aGlsZSBtb3Zpbmcgem9vbSB3aW5kb3csXG4gIC8vIG90aGVyd2lzZSB0aGUgY2FsY3VsYXRlZCBpbnRlcnZhbCBtaWdodCBqaXR0ZXIgd2hlbiB0aGUgem9vbVxuICAvLyB3aW5kb3cgc2l6ZSBpcyBjbG9zZSB0byB0aGUgaW50ZXJ2YWwtY2hhbmdpbmcgc2l6ZS5cblxuICBpZiAobGFzdEF1dG9JbnRlcnZhbCAhPSBudWxsICYmIGxhc3RUaWNrQ291bnQgIT0gbnVsbCAmJiBNYXRoLmFicyhsYXN0QXV0b0ludGVydmFsIC0gaW50ZXJ2YWwpIDw9IDEgJiYgTWF0aC5hYnMobGFzdFRpY2tDb3VudCAtIHRpY2tDb3VudCkgPD0gMSAvLyBBbHdheXMgY2hvb3NlIHRoZSBiaWdnZXIgb25lLCBvdGhlcndpc2UgdGhlIGNyaXRpY2FsXG4gIC8vIHBvaW50IGlzIG5vdCB0aGUgc2FtZSB3aGVuIHpvb21pbmcgaW4gb3Igem9vbWluZyBvdXQuXG4gICYmIGxhc3RBdXRvSW50ZXJ2YWwgPiBpbnRlcnZhbCkge1xuICAgIGludGVydmFsID0gbGFzdEF1dG9JbnRlcnZhbDtcbiAgfSAvLyBPbmx5IHVwZGF0ZSBjYWNoZSBpZiBjYWNoZSBub3QgdXNlZCwgb3RoZXJ3aXNlIHRoZVxuICAvLyBjaGFuZ2luZyBvZiBpbnRlcnZhbCBpcyB0b28gaW5zZW5zaXRpdmUuXG4gIGVsc2Uge1xuICAgICAgY2FjaGUubGFzdFRpY2tDb3VudCA9IHRpY2tDb3VudDtcbiAgICAgIGNhY2hlLmxhc3RBdXRvSW50ZXJ2YWwgPSBpbnRlcnZhbDtcbiAgICB9XG5cbiAgcmV0dXJuIGludGVydmFsO1xufVxuXG5mdW5jdGlvbiBmZXRjaEF1dG9DYXRlZ29yeUludGVydmFsQ2FsY3VsYXRpb25QYXJhbXMoYXhpcykge1xuICB2YXIgbGFiZWxNb2RlbCA9IGF4aXMuZ2V0TGFiZWxNb2RlbCgpO1xuICByZXR1cm4ge1xuICAgIGF4aXNSb3RhdGU6IGF4aXMuZ2V0Um90YXRlID8gYXhpcy5nZXRSb3RhdGUoKSA6IGF4aXMuaXNIb3Jpem9udGFsICYmICFheGlzLmlzSG9yaXpvbnRhbCgpID8gOTAgOiAwLFxuICAgIGxhYmVsUm90YXRlOiBsYWJlbE1vZGVsLmdldCgncm90YXRlJykgfHwgMCxcbiAgICBmb250OiBsYWJlbE1vZGVsLmdldEZvbnQoKVxuICB9O1xufVxuXG5mdW5jdGlvbiBtYWtlTGFiZWxzQnlOdW1lcmljQ2F0ZWdvcnlJbnRlcnZhbChheGlzLCBjYXRlZ29yeUludGVydmFsLCBvbmx5VGljaykge1xuICB2YXIgbGFiZWxGb3JtYXR0ZXIgPSBtYWtlTGFiZWxGb3JtYXR0ZXIoYXhpcyk7XG4gIHZhciBvcmRpbmFsU2NhbGUgPSBheGlzLnNjYWxlO1xuICB2YXIgb3JkaW5hbEV4dGVudCA9IG9yZGluYWxTY2FsZS5nZXRFeHRlbnQoKTtcbiAgdmFyIGxhYmVsTW9kZWwgPSBheGlzLmdldExhYmVsTW9kZWwoKTtcbiAgdmFyIHJlc3VsdCA9IFtdOyAvLyBUT0RPOiBheGlzVHlwZTogb3JkaW5hbFRpbWUsIHBpY2sgdGhlIHRpY2sgZnJvbSBlYWNoIG1vbnRoL2RheS95ZWFyLy4uLlxuXG4gIHZhciBzdGVwID0gTWF0aC5tYXgoKGNhdGVnb3J5SW50ZXJ2YWwgfHwgMCkgKyAxLCAxKTtcbiAgdmFyIHN0YXJ0VGljayA9IG9yZGluYWxFeHRlbnRbMF07XG4gIHZhciB0aWNrQ291bnQgPSBvcmRpbmFsU2NhbGUuY291bnQoKTsgLy8gQ2FsY3VsYXRlIHN0YXJ0IHRpY2sgYmFzZWQgb24gemVybyBpZiBwb3NzaWJsZSB0byBrZWVwIGxhYmVsIGNvbnNpc3RlbnRcbiAgLy8gd2hpbGUgem9vbWluZyBhbmQgbW92aW5nIHdoaWxlIGludGVydmFsID4gMC4gT3RoZXJ3aXNlIHRoZSBzZWxlY3Rpb25cbiAgLy8gb2YgZGlzcGxheWFibGUgdGlja3MgYW5kIHN5bWJvbHMgcHJvYmFibHkga2VlcCBjaGFuZ2luZy5cbiAgLy8gMyBpcyBlbXBpcmljYWwgdmFsdWUuXG5cbiAgaWYgKHN0YXJ0VGljayAhPT0gMCAmJiBzdGVwID4gMSAmJiB0aWNrQ291bnQgLyBzdGVwID4gMikge1xuICAgIHN0YXJ0VGljayA9IE1hdGgucm91bmQoTWF0aC5jZWlsKHN0YXJ0VGljayAvIHN0ZXApICogc3RlcCk7XG4gIH0gLy8gKDEpIE9ubHkgYWRkIG1pbiBtYXggbGFiZWwgaGVyZSBidXQgbGVhdmUgb3ZlcmxhcCBjaGVja2luZ1xuICAvLyB0byByZW5kZXIgc3RhZ2UsIHdoaWNoIGFsc28gZW5zdXJlIHRoZSByZXR1cm5lZCBsaXN0XG4gIC8vIHN1aXRhYmxlIGZvciBzcGxpdExpbmUgYW5kIHNwbGl0QXJlYSByZW5kZXJpbmcuXG4gIC8vICgyKSBTY2FsZXMgZXhjZXB0IGNhdGVnb3J5IGFsd2F5cyBjb250YWluIG1pbiBtYXggbGFiZWwgc29cbiAgLy8gZG8gbm90IG5lZWQgdG8gcGVyZm9ybSB0aGlzIHByb2Nlc3MuXG5cblxuICB2YXIgc2hvd0FsbExhYmVsID0gc2hvdWxkU2hvd0FsbExhYmVscyhheGlzKTtcbiAgdmFyIGluY2x1ZGVNaW5MYWJlbCA9IGxhYmVsTW9kZWwuZ2V0KCdzaG93TWluTGFiZWwnKSB8fCBzaG93QWxsTGFiZWw7XG4gIHZhciBpbmNsdWRlTWF4TGFiZWwgPSBsYWJlbE1vZGVsLmdldCgnc2hvd01heExhYmVsJykgfHwgc2hvd0FsbExhYmVsO1xuXG4gIGlmIChpbmNsdWRlTWluTGFiZWwgJiYgc3RhcnRUaWNrICE9PSBvcmRpbmFsRXh0ZW50WzBdKSB7XG4gICAgYWRkSXRlbShvcmRpbmFsRXh0ZW50WzBdKTtcbiAgfSAvLyBPcHRpbWl6ZTogYXZvaWQgZ2VuZXJhdGluZyBsYXJnZSBhcnJheSBieSBgb3JkaW5hbFNjYWxlLmdldFRpY2tzKClgLlxuXG5cbiAgdmFyIHRpY2tWYWx1ZSA9IHN0YXJ0VGljaztcblxuICBmb3IgKDsgdGlja1ZhbHVlIDw9IG9yZGluYWxFeHRlbnRbMV07IHRpY2tWYWx1ZSArPSBzdGVwKSB7XG4gICAgYWRkSXRlbSh0aWNrVmFsdWUpO1xuICB9XG5cbiAgaWYgKGluY2x1ZGVNYXhMYWJlbCAmJiB0aWNrVmFsdWUgIT09IG9yZGluYWxFeHRlbnRbMV0pIHtcbiAgICBhZGRJdGVtKG9yZGluYWxFeHRlbnRbMV0pO1xuICB9XG5cbiAgZnVuY3Rpb24gYWRkSXRlbSh0VmFsKSB7XG4gICAgcmVzdWx0LnB1c2gob25seVRpY2sgPyB0VmFsIDoge1xuICAgICAgZm9ybWF0dGVkTGFiZWw6IGxhYmVsRm9ybWF0dGVyKHRWYWwpLFxuICAgICAgcmF3TGFiZWw6IG9yZGluYWxTY2FsZS5nZXRMYWJlbCh0VmFsKSxcbiAgICAgIHRpY2tWYWx1ZTogdFZhbFxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn0gLy8gV2hlbiBpbnRlcnZhbCBpcyBmdW5jdGlvbiwgdGhlIHJlc3VsdCBgZmFsc2VgIG1lYW5zIGlnbm9yZSB0aGUgdGljay5cbi8vIEl0IGlzIHRpbWUgY29uc3VtaW5nIGZvciBsYXJnZSBjYXRlZ29yeSBkYXRhLlxuXG5cbmZ1bmN0aW9uIG1ha2VMYWJlbHNCeUN1c3RvbWl6ZWRDYXRlZ29yeUludGVydmFsKGF4aXMsIGNhdGVnb3J5SW50ZXJ2YWwsIG9ubHlUaWNrKSB7XG4gIHZhciBvcmRpbmFsU2NhbGUgPSBheGlzLnNjYWxlO1xuICB2YXIgbGFiZWxGb3JtYXR0ZXIgPSBtYWtlTGFiZWxGb3JtYXR0ZXIoYXhpcyk7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgenJVdGlsLmVhY2gob3JkaW5hbFNjYWxlLmdldFRpY2tzKCksIGZ1bmN0aW9uICh0aWNrVmFsdWUpIHtcbiAgICB2YXIgcmF3TGFiZWwgPSBvcmRpbmFsU2NhbGUuZ2V0TGFiZWwodGlja1ZhbHVlKTtcblxuICAgIGlmIChjYXRlZ29yeUludGVydmFsKHRpY2tWYWx1ZSwgcmF3TGFiZWwpKSB7XG4gICAgICByZXN1bHQucHVzaChvbmx5VGljayA/IHRpY2tWYWx1ZSA6IHtcbiAgICAgICAgZm9ybWF0dGVkTGFiZWw6IGxhYmVsRm9ybWF0dGVyKHRpY2tWYWx1ZSksXG4gICAgICAgIHJhd0xhYmVsOiByYXdMYWJlbCxcbiAgICAgICAgdGlja1ZhbHVlOiB0aWNrVmFsdWVcbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmV4cG9ydHMuY3JlYXRlQXhpc0xhYmVscyA9IGNyZWF0ZUF4aXNMYWJlbHM7XG5leHBvcnRzLmNyZWF0ZUF4aXNUaWNrcyA9IGNyZWF0ZUF4aXNUaWNrcztcbmV4cG9ydHMuY2FsY3VsYXRlQ2F0ZWdvcnlJbnRlcnZhbCA9IGNhbGN1bGF0ZUNhdGVnb3J5SW50ZXJ2YWw7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/axisTickLabelBuilder.js
