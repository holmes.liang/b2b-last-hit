/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var zrColor = __webpack_require__(/*! zrender/lib/tool/color */ "./node_modules/zrender/lib/tool/color.js");

var eventUtil = __webpack_require__(/*! zrender/lib/core/event */ "./node_modules/zrender/lib/core/event.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var formatUtil = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var each = zrUtil.each;
var toCamelCase = formatUtil.toCamelCase;
var vendors = ['', '-webkit-', '-moz-', '-o-'];
var gCssText = 'position:absolute;display:block;border-style:solid;white-space:nowrap;z-index:9999999;';
/**
 * @param {number} duration
 * @return {string}
 * @inner
 */

function assembleTransition(duration) {
  var transitionCurve = 'cubic-bezier(0.23, 1, 0.32, 1)';
  var transitionText = 'left ' + duration + 's ' + transitionCurve + ',' + 'top ' + duration + 's ' + transitionCurve;
  return zrUtil.map(vendors, function (vendorPrefix) {
    return vendorPrefix + 'transition:' + transitionText;
  }).join(';');
}
/**
 * @param {Object} textStyle
 * @return {string}
 * @inner
 */


function assembleFont(textStyleModel) {
  var cssText = [];
  var fontSize = textStyleModel.get('fontSize');
  var color = textStyleModel.getTextColor();
  color && cssText.push('color:' + color);
  cssText.push('font:' + textStyleModel.getFont());
  fontSize && cssText.push('line-height:' + Math.round(fontSize * 3 / 2) + 'px');
  each(['decoration', 'align'], function (name) {
    var val = textStyleModel.get(name);
    val && cssText.push('text-' + name + ':' + val);
  });
  return cssText.join(';');
}
/**
 * @param {Object} tooltipModel
 * @return {string}
 * @inner
 */


function assembleCssText(tooltipModel) {
  var cssText = [];
  var transitionDuration = tooltipModel.get('transitionDuration');
  var backgroundColor = tooltipModel.get('backgroundColor');
  var textStyleModel = tooltipModel.getModel('textStyle');
  var padding = tooltipModel.get('padding'); // Animation transition. Do not animate when transitionDuration is 0.

  transitionDuration && cssText.push(assembleTransition(transitionDuration));

  if (backgroundColor) {
    if (env.canvasSupported) {
      cssText.push('background-Color:' + backgroundColor);
    } else {
      // for ie
      cssText.push('background-Color:#' + zrColor.toHex(backgroundColor));
      cssText.push('filter:alpha(opacity=70)');
    }
  } // Border style


  each(['width', 'color', 'radius'], function (name) {
    var borderName = 'border-' + name;
    var camelCase = toCamelCase(borderName);
    var val = tooltipModel.get(camelCase);
    val != null && cssText.push(borderName + ':' + val + (name === 'color' ? '' : 'px'));
  }); // Text style

  cssText.push(assembleFont(textStyleModel)); // Padding

  if (padding != null) {
    cssText.push('padding:' + formatUtil.normalizeCssArray(padding).join('px ') + 'px');
  }

  return cssText.join(';') + ';';
}
/**
 * @alias module:echarts/component/tooltip/TooltipContent
 * @constructor
 */


function TooltipContent(container, api) {
  if (env.wxa) {
    return null;
  }

  var el = document.createElement('div');
  var zr = this._zr = api.getZr();
  this.el = el;
  this._x = api.getWidth() / 2;
  this._y = api.getHeight() / 2;
  container.appendChild(el);
  this._container = container;
  this._show = false;
  /**
   * @private
   */

  this._hideTimeout;
  var self = this;

  el.onmouseenter = function () {
    // clear the timeout in hideLater and keep showing tooltip
    if (self._enterable) {
      clearTimeout(self._hideTimeout);
      self._show = true;
    }

    self._inContent = true;
  };

  el.onmousemove = function (e) {
    e = e || window.event;

    if (!self._enterable) {
      // Try trigger zrender event to avoid mouse
      // in and out shape too frequently
      var handler = zr.handler;
      eventUtil.normalizeEvent(container, e, true);
      handler.dispatch('mousemove', e);
    }
  };

  el.onmouseleave = function () {
    if (self._enterable) {
      if (self._show) {
        self.hideLater(self._hideDelay);
      }
    }

    self._inContent = false;
  };
}

TooltipContent.prototype = {
  constructor: TooltipContent,

  /**
   * @private
   * @type {boolean}
   */
  _enterable: true,

  /**
   * Update when tooltip is rendered
   */
  update: function update() {
    // FIXME
    // Move this logic to ec main?
    var container = this._container;
    var stl = container.currentStyle || document.defaultView.getComputedStyle(container);
    var domStyle = container.style;

    if (domStyle.position !== 'absolute' && stl.position !== 'absolute') {
      domStyle.position = 'relative';
    } // Hide the tooltip
    // PENDING
    // this.hide();

  },
  show: function show(tooltipModel) {
    clearTimeout(this._hideTimeout);
    var el = this.el;
    el.style.cssText = gCssText + assembleCssText(tooltipModel) // http://stackoverflow.com/questions/21125587/css3-transition-not-working-in-chrome-anymore
    + ';left:' + this._x + 'px;top:' + this._y + 'px;' + (tooltipModel.get('extraCssText') || '');
    el.style.display = el.innerHTML ? 'block' : 'none'; // If mouse occsionally move over the tooltip, a mouseout event will be
    // triggered by canvas, and cuase some unexpectable result like dragging
    // stop, "unfocusAdjacency". Here `pointer-events: none` is used to solve
    // it. Although it is not suppored by IE8~IE10, fortunately it is a rare
    // scenario.

    el.style.pointerEvents = this._enterable ? 'auto' : 'none';
    this._show = true;
  },
  setContent: function setContent(content) {
    this.el.innerHTML = content == null ? '' : content;
  },
  setEnterable: function setEnterable(enterable) {
    this._enterable = enterable;
  },
  getSize: function getSize() {
    var el = this.el;
    return [el.clientWidth, el.clientHeight];
  },
  moveTo: function moveTo(x, y) {
    // xy should be based on canvas root. But tooltipContent is
    // the sibling of canvas root. So padding of ec container
    // should be considered here.
    var zr = this._zr;
    var viewportRootOffset;

    if (zr && zr.painter && (viewportRootOffset = zr.painter.getViewportRootOffset())) {
      x += viewportRootOffset.offsetLeft;
      y += viewportRootOffset.offsetTop;
    }

    var style = this.el.style;
    style.left = x + 'px';
    style.top = y + 'px';
    this._x = x;
    this._y = y;
  },
  hide: function hide() {
    this.el.style.display = 'none';
    this._show = false;
  },
  hideLater: function hideLater(time) {
    if (this._show && !(this._inContent && this._enterable)) {
      if (time) {
        this._hideDelay = time; // Set show false to avoid invoke hideLater mutiple times

        this._show = false;
        this._hideTimeout = setTimeout(zrUtil.bind(this.hide, this), time);
      } else {
        this.hide();
      }
    }
  },
  isShow: function isShow() {
    return this._show;
  },
  getOuterSize: function getOuterSize() {
    var width = this.el.clientWidth;
    var height = this.el.clientHeight; // Consider browser compatibility.
    // IE8 does not support getComputedStyle.

    if (document.defaultView && document.defaultView.getComputedStyle) {
      var stl = document.defaultView.getComputedStyle(this.el);

      if (stl) {
        width += parseInt(stl.paddingLeft, 10) + parseInt(stl.paddingRight, 10) + parseInt(stl.borderLeftWidth, 10) + parseInt(stl.borderRightWidth, 10);
        height += parseInt(stl.paddingTop, 10) + parseInt(stl.paddingBottom, 10) + parseInt(stl.borderTopWidth, 10) + parseInt(stl.borderBottomWidth, 10);
      }
    }

    return {
      width: width,
      height: height
    };
  }
};
var _default = TooltipContent;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcENvbnRlbnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvdG9vbHRpcC9Ub29sdGlwQ29udGVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciB6ckNvbG9yID0gcmVxdWlyZShcInpyZW5kZXIvbGliL3Rvb2wvY29sb3JcIik7XG5cbnZhciBldmVudFV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9ldmVudFwiKTtcblxudmFyIGVudiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL2VudlwiKTtcblxudmFyIGZvcm1hdFV0aWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9mb3JtYXRcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBlYWNoID0genJVdGlsLmVhY2g7XG52YXIgdG9DYW1lbENhc2UgPSBmb3JtYXRVdGlsLnRvQ2FtZWxDYXNlO1xudmFyIHZlbmRvcnMgPSBbJycsICctd2Via2l0LScsICctbW96LScsICctby0nXTtcbnZhciBnQ3NzVGV4dCA9ICdwb3NpdGlvbjphYnNvbHV0ZTtkaXNwbGF5OmJsb2NrO2JvcmRlci1zdHlsZTpzb2xpZDt3aGl0ZS1zcGFjZTpub3dyYXA7ei1pbmRleDo5OTk5OTk5Oyc7XG4vKipcbiAqIEBwYXJhbSB7bnVtYmVyfSBkdXJhdGlvblxuICogQHJldHVybiB7c3RyaW5nfVxuICogQGlubmVyXG4gKi9cblxuZnVuY3Rpb24gYXNzZW1ibGVUcmFuc2l0aW9uKGR1cmF0aW9uKSB7XG4gIHZhciB0cmFuc2l0aW9uQ3VydmUgPSAnY3ViaWMtYmV6aWVyKDAuMjMsIDEsIDAuMzIsIDEpJztcbiAgdmFyIHRyYW5zaXRpb25UZXh0ID0gJ2xlZnQgJyArIGR1cmF0aW9uICsgJ3MgJyArIHRyYW5zaXRpb25DdXJ2ZSArICcsJyArICd0b3AgJyArIGR1cmF0aW9uICsgJ3MgJyArIHRyYW5zaXRpb25DdXJ2ZTtcbiAgcmV0dXJuIHpyVXRpbC5tYXAodmVuZG9ycywgZnVuY3Rpb24gKHZlbmRvclByZWZpeCkge1xuICAgIHJldHVybiB2ZW5kb3JQcmVmaXggKyAndHJhbnNpdGlvbjonICsgdHJhbnNpdGlvblRleHQ7XG4gIH0pLmpvaW4oJzsnKTtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R9IHRleHRTdHlsZVxuICogQHJldHVybiB7c3RyaW5nfVxuICogQGlubmVyXG4gKi9cblxuXG5mdW5jdGlvbiBhc3NlbWJsZUZvbnQodGV4dFN0eWxlTW9kZWwpIHtcbiAgdmFyIGNzc1RleHQgPSBbXTtcbiAgdmFyIGZvbnRTaXplID0gdGV4dFN0eWxlTW9kZWwuZ2V0KCdmb250U2l6ZScpO1xuICB2YXIgY29sb3IgPSB0ZXh0U3R5bGVNb2RlbC5nZXRUZXh0Q29sb3IoKTtcbiAgY29sb3IgJiYgY3NzVGV4dC5wdXNoKCdjb2xvcjonICsgY29sb3IpO1xuICBjc3NUZXh0LnB1c2goJ2ZvbnQ6JyArIHRleHRTdHlsZU1vZGVsLmdldEZvbnQoKSk7XG4gIGZvbnRTaXplICYmIGNzc1RleHQucHVzaCgnbGluZS1oZWlnaHQ6JyArIE1hdGgucm91bmQoZm9udFNpemUgKiAzIC8gMikgKyAncHgnKTtcbiAgZWFjaChbJ2RlY29yYXRpb24nLCAnYWxpZ24nXSwgZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB2YXIgdmFsID0gdGV4dFN0eWxlTW9kZWwuZ2V0KG5hbWUpO1xuICAgIHZhbCAmJiBjc3NUZXh0LnB1c2goJ3RleHQtJyArIG5hbWUgKyAnOicgKyB2YWwpO1xuICB9KTtcbiAgcmV0dXJuIGNzc1RleHQuam9pbignOycpO1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gdG9vbHRpcE1vZGVsXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKiBAaW5uZXJcbiAqL1xuXG5cbmZ1bmN0aW9uIGFzc2VtYmxlQ3NzVGV4dCh0b29sdGlwTW9kZWwpIHtcbiAgdmFyIGNzc1RleHQgPSBbXTtcbiAgdmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9IHRvb2x0aXBNb2RlbC5nZXQoJ3RyYW5zaXRpb25EdXJhdGlvbicpO1xuICB2YXIgYmFja2dyb3VuZENvbG9yID0gdG9vbHRpcE1vZGVsLmdldCgnYmFja2dyb3VuZENvbG9yJyk7XG4gIHZhciB0ZXh0U3R5bGVNb2RlbCA9IHRvb2x0aXBNb2RlbC5nZXRNb2RlbCgndGV4dFN0eWxlJyk7XG4gIHZhciBwYWRkaW5nID0gdG9vbHRpcE1vZGVsLmdldCgncGFkZGluZycpOyAvLyBBbmltYXRpb24gdHJhbnNpdGlvbi4gRG8gbm90IGFuaW1hdGUgd2hlbiB0cmFuc2l0aW9uRHVyYXRpb24gaXMgMC5cblxuICB0cmFuc2l0aW9uRHVyYXRpb24gJiYgY3NzVGV4dC5wdXNoKGFzc2VtYmxlVHJhbnNpdGlvbih0cmFuc2l0aW9uRHVyYXRpb24pKTtcblxuICBpZiAoYmFja2dyb3VuZENvbG9yKSB7XG4gICAgaWYgKGVudi5jYW52YXNTdXBwb3J0ZWQpIHtcbiAgICAgIGNzc1RleHQucHVzaCgnYmFja2dyb3VuZC1Db2xvcjonICsgYmFja2dyb3VuZENvbG9yKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gZm9yIGllXG4gICAgICBjc3NUZXh0LnB1c2goJ2JhY2tncm91bmQtQ29sb3I6IycgKyB6ckNvbG9yLnRvSGV4KGJhY2tncm91bmRDb2xvcikpO1xuICAgICAgY3NzVGV4dC5wdXNoKCdmaWx0ZXI6YWxwaGEob3BhY2l0eT03MCknKTtcbiAgICB9XG4gIH0gLy8gQm9yZGVyIHN0eWxlXG5cblxuICBlYWNoKFsnd2lkdGgnLCAnY29sb3InLCAncmFkaXVzJ10sIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdmFyIGJvcmRlck5hbWUgPSAnYm9yZGVyLScgKyBuYW1lO1xuICAgIHZhciBjYW1lbENhc2UgPSB0b0NhbWVsQ2FzZShib3JkZXJOYW1lKTtcbiAgICB2YXIgdmFsID0gdG9vbHRpcE1vZGVsLmdldChjYW1lbENhc2UpO1xuICAgIHZhbCAhPSBudWxsICYmIGNzc1RleHQucHVzaChib3JkZXJOYW1lICsgJzonICsgdmFsICsgKG5hbWUgPT09ICdjb2xvcicgPyAnJyA6ICdweCcpKTtcbiAgfSk7IC8vIFRleHQgc3R5bGVcblxuICBjc3NUZXh0LnB1c2goYXNzZW1ibGVGb250KHRleHRTdHlsZU1vZGVsKSk7IC8vIFBhZGRpbmdcblxuICBpZiAocGFkZGluZyAhPSBudWxsKSB7XG4gICAgY3NzVGV4dC5wdXNoKCdwYWRkaW5nOicgKyBmb3JtYXRVdGlsLm5vcm1hbGl6ZUNzc0FycmF5KHBhZGRpbmcpLmpvaW4oJ3B4ICcpICsgJ3B4Jyk7XG4gIH1cblxuICByZXR1cm4gY3NzVGV4dC5qb2luKCc7JykgKyAnOyc7XG59XG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6ZWNoYXJ0cy9jb21wb25lbnQvdG9vbHRpcC9Ub29sdGlwQ29udGVudFxuICogQGNvbnN0cnVjdG9yXG4gKi9cblxuXG5mdW5jdGlvbiBUb29sdGlwQ29udGVudChjb250YWluZXIsIGFwaSkge1xuICBpZiAoZW52Lnd4YSkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgdmFyIGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIHZhciB6ciA9IHRoaXMuX3pyID0gYXBpLmdldFpyKCk7XG4gIHRoaXMuZWwgPSBlbDtcbiAgdGhpcy5feCA9IGFwaS5nZXRXaWR0aCgpIC8gMjtcbiAgdGhpcy5feSA9IGFwaS5nZXRIZWlnaHQoKSAvIDI7XG4gIGNvbnRhaW5lci5hcHBlbmRDaGlsZChlbCk7XG4gIHRoaXMuX2NvbnRhaW5lciA9IGNvbnRhaW5lcjtcbiAgdGhpcy5fc2hvdyA9IGZhbHNlO1xuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5faGlkZVRpbWVvdXQ7XG4gIHZhciBzZWxmID0gdGhpcztcblxuICBlbC5vbm1vdXNlZW50ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgLy8gY2xlYXIgdGhlIHRpbWVvdXQgaW4gaGlkZUxhdGVyIGFuZCBrZWVwIHNob3dpbmcgdG9vbHRpcFxuICAgIGlmIChzZWxmLl9lbnRlcmFibGUpIHtcbiAgICAgIGNsZWFyVGltZW91dChzZWxmLl9oaWRlVGltZW91dCk7XG4gICAgICBzZWxmLl9zaG93ID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBzZWxmLl9pbkNvbnRlbnQgPSB0cnVlO1xuICB9O1xuXG4gIGVsLm9ubW91c2Vtb3ZlID0gZnVuY3Rpb24gKGUpIHtcbiAgICBlID0gZSB8fCB3aW5kb3cuZXZlbnQ7XG5cbiAgICBpZiAoIXNlbGYuX2VudGVyYWJsZSkge1xuICAgICAgLy8gVHJ5IHRyaWdnZXIgenJlbmRlciBldmVudCB0byBhdm9pZCBtb3VzZVxuICAgICAgLy8gaW4gYW5kIG91dCBzaGFwZSB0b28gZnJlcXVlbnRseVxuICAgICAgdmFyIGhhbmRsZXIgPSB6ci5oYW5kbGVyO1xuICAgICAgZXZlbnRVdGlsLm5vcm1hbGl6ZUV2ZW50KGNvbnRhaW5lciwgZSwgdHJ1ZSk7XG4gICAgICBoYW5kbGVyLmRpc3BhdGNoKCdtb3VzZW1vdmUnLCBlKTtcbiAgICB9XG4gIH07XG5cbiAgZWwub25tb3VzZWxlYXZlID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChzZWxmLl9lbnRlcmFibGUpIHtcbiAgICAgIGlmIChzZWxmLl9zaG93KSB7XG4gICAgICAgIHNlbGYuaGlkZUxhdGVyKHNlbGYuX2hpZGVEZWxheSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgc2VsZi5faW5Db250ZW50ID0gZmFsc2U7XG4gIH07XG59XG5cblRvb2x0aXBDb250ZW50LnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IFRvb2x0aXBDb250ZW50LFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG4gIF9lbnRlcmFibGU6IHRydWUsXG5cbiAgLyoqXG4gICAqIFVwZGF0ZSB3aGVuIHRvb2x0aXAgaXMgcmVuZGVyZWRcbiAgICovXG4gIHVwZGF0ZTogZnVuY3Rpb24gKCkge1xuICAgIC8vIEZJWE1FXG4gICAgLy8gTW92ZSB0aGlzIGxvZ2ljIHRvIGVjIG1haW4/XG4gICAgdmFyIGNvbnRhaW5lciA9IHRoaXMuX2NvbnRhaW5lcjtcbiAgICB2YXIgc3RsID0gY29udGFpbmVyLmN1cnJlbnRTdHlsZSB8fCBkb2N1bWVudC5kZWZhdWx0Vmlldy5nZXRDb21wdXRlZFN0eWxlKGNvbnRhaW5lcik7XG4gICAgdmFyIGRvbVN0eWxlID0gY29udGFpbmVyLnN0eWxlO1xuXG4gICAgaWYgKGRvbVN0eWxlLnBvc2l0aW9uICE9PSAnYWJzb2x1dGUnICYmIHN0bC5wb3NpdGlvbiAhPT0gJ2Fic29sdXRlJykge1xuICAgICAgZG9tU3R5bGUucG9zaXRpb24gPSAncmVsYXRpdmUnO1xuICAgIH0gLy8gSGlkZSB0aGUgdG9vbHRpcFxuICAgIC8vIFBFTkRJTkdcbiAgICAvLyB0aGlzLmhpZGUoKTtcblxuICB9LFxuICBzaG93OiBmdW5jdGlvbiAodG9vbHRpcE1vZGVsKSB7XG4gICAgY2xlYXJUaW1lb3V0KHRoaXMuX2hpZGVUaW1lb3V0KTtcbiAgICB2YXIgZWwgPSB0aGlzLmVsO1xuICAgIGVsLnN0eWxlLmNzc1RleHQgPSBnQ3NzVGV4dCArIGFzc2VtYmxlQ3NzVGV4dCh0b29sdGlwTW9kZWwpIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMjExMjU1ODcvY3NzMy10cmFuc2l0aW9uLW5vdC13b3JraW5nLWluLWNocm9tZS1hbnltb3JlXG4gICAgKyAnO2xlZnQ6JyArIHRoaXMuX3ggKyAncHg7dG9wOicgKyB0aGlzLl95ICsgJ3B4OycgKyAodG9vbHRpcE1vZGVsLmdldCgnZXh0cmFDc3NUZXh0JykgfHwgJycpO1xuICAgIGVsLnN0eWxlLmRpc3BsYXkgPSBlbC5pbm5lckhUTUwgPyAnYmxvY2snIDogJ25vbmUnOyAvLyBJZiBtb3VzZSBvY2NzaW9uYWxseSBtb3ZlIG92ZXIgdGhlIHRvb2x0aXAsIGEgbW91c2VvdXQgZXZlbnQgd2lsbCBiZVxuICAgIC8vIHRyaWdnZXJlZCBieSBjYW52YXMsIGFuZCBjdWFzZSBzb21lIHVuZXhwZWN0YWJsZSByZXN1bHQgbGlrZSBkcmFnZ2luZ1xuICAgIC8vIHN0b3AsIFwidW5mb2N1c0FkamFjZW5jeVwiLiBIZXJlIGBwb2ludGVyLWV2ZW50czogbm9uZWAgaXMgdXNlZCB0byBzb2x2ZVxuICAgIC8vIGl0LiBBbHRob3VnaCBpdCBpcyBub3Qgc3VwcG9yZWQgYnkgSUU4fklFMTAsIGZvcnR1bmF0ZWx5IGl0IGlzIGEgcmFyZVxuICAgIC8vIHNjZW5hcmlvLlxuXG4gICAgZWwuc3R5bGUucG9pbnRlckV2ZW50cyA9IHRoaXMuX2VudGVyYWJsZSA/ICdhdXRvJyA6ICdub25lJztcbiAgICB0aGlzLl9zaG93ID0gdHJ1ZTtcbiAgfSxcbiAgc2V0Q29udGVudDogZnVuY3Rpb24gKGNvbnRlbnQpIHtcbiAgICB0aGlzLmVsLmlubmVySFRNTCA9IGNvbnRlbnQgPT0gbnVsbCA/ICcnIDogY29udGVudDtcbiAgfSxcbiAgc2V0RW50ZXJhYmxlOiBmdW5jdGlvbiAoZW50ZXJhYmxlKSB7XG4gICAgdGhpcy5fZW50ZXJhYmxlID0gZW50ZXJhYmxlO1xuICB9LFxuICBnZXRTaXplOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGVsID0gdGhpcy5lbDtcbiAgICByZXR1cm4gW2VsLmNsaWVudFdpZHRoLCBlbC5jbGllbnRIZWlnaHRdO1xuICB9LFxuICBtb3ZlVG86IGZ1bmN0aW9uICh4LCB5KSB7XG4gICAgLy8geHkgc2hvdWxkIGJlIGJhc2VkIG9uIGNhbnZhcyByb290LiBCdXQgdG9vbHRpcENvbnRlbnQgaXNcbiAgICAvLyB0aGUgc2libGluZyBvZiBjYW52YXMgcm9vdC4gU28gcGFkZGluZyBvZiBlYyBjb250YWluZXJcbiAgICAvLyBzaG91bGQgYmUgY29uc2lkZXJlZCBoZXJlLlxuICAgIHZhciB6ciA9IHRoaXMuX3pyO1xuICAgIHZhciB2aWV3cG9ydFJvb3RPZmZzZXQ7XG5cbiAgICBpZiAoenIgJiYgenIucGFpbnRlciAmJiAodmlld3BvcnRSb290T2Zmc2V0ID0genIucGFpbnRlci5nZXRWaWV3cG9ydFJvb3RPZmZzZXQoKSkpIHtcbiAgICAgIHggKz0gdmlld3BvcnRSb290T2Zmc2V0Lm9mZnNldExlZnQ7XG4gICAgICB5ICs9IHZpZXdwb3J0Um9vdE9mZnNldC5vZmZzZXRUb3A7XG4gICAgfVxuXG4gICAgdmFyIHN0eWxlID0gdGhpcy5lbC5zdHlsZTtcbiAgICBzdHlsZS5sZWZ0ID0geCArICdweCc7XG4gICAgc3R5bGUudG9wID0geSArICdweCc7XG4gICAgdGhpcy5feCA9IHg7XG4gICAgdGhpcy5feSA9IHk7XG4gIH0sXG4gIGhpZGU6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmVsLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgdGhpcy5fc2hvdyA9IGZhbHNlO1xuICB9LFxuICBoaWRlTGF0ZXI6IGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgaWYgKHRoaXMuX3Nob3cgJiYgISh0aGlzLl9pbkNvbnRlbnQgJiYgdGhpcy5fZW50ZXJhYmxlKSkge1xuICAgICAgaWYgKHRpbWUpIHtcbiAgICAgICAgdGhpcy5faGlkZURlbGF5ID0gdGltZTsgLy8gU2V0IHNob3cgZmFsc2UgdG8gYXZvaWQgaW52b2tlIGhpZGVMYXRlciBtdXRpcGxlIHRpbWVzXG5cbiAgICAgICAgdGhpcy5fc2hvdyA9IGZhbHNlO1xuICAgICAgICB0aGlzLl9oaWRlVGltZW91dCA9IHNldFRpbWVvdXQoenJVdGlsLmJpbmQodGhpcy5oaWRlLCB0aGlzKSwgdGltZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIGlzU2hvdzogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9zaG93O1xuICB9LFxuICBnZXRPdXRlclNpemU6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgd2lkdGggPSB0aGlzLmVsLmNsaWVudFdpZHRoO1xuICAgIHZhciBoZWlnaHQgPSB0aGlzLmVsLmNsaWVudEhlaWdodDsgLy8gQ29uc2lkZXIgYnJvd3NlciBjb21wYXRpYmlsaXR5LlxuICAgIC8vIElFOCBkb2VzIG5vdCBzdXBwb3J0IGdldENvbXB1dGVkU3R5bGUuXG5cbiAgICBpZiAoZG9jdW1lbnQuZGVmYXVsdFZpZXcgJiYgZG9jdW1lbnQuZGVmYXVsdFZpZXcuZ2V0Q29tcHV0ZWRTdHlsZSkge1xuICAgICAgdmFyIHN0bCA9IGRvY3VtZW50LmRlZmF1bHRWaWV3LmdldENvbXB1dGVkU3R5bGUodGhpcy5lbCk7XG5cbiAgICAgIGlmIChzdGwpIHtcbiAgICAgICAgd2lkdGggKz0gcGFyc2VJbnQoc3RsLnBhZGRpbmdMZWZ0LCAxMCkgKyBwYXJzZUludChzdGwucGFkZGluZ1JpZ2h0LCAxMCkgKyBwYXJzZUludChzdGwuYm9yZGVyTGVmdFdpZHRoLCAxMCkgKyBwYXJzZUludChzdGwuYm9yZGVyUmlnaHRXaWR0aCwgMTApO1xuICAgICAgICBoZWlnaHQgKz0gcGFyc2VJbnQoc3RsLnBhZGRpbmdUb3AsIDEwKSArIHBhcnNlSW50KHN0bC5wYWRkaW5nQm90dG9tLCAxMCkgKyBwYXJzZUludChzdGwuYm9yZGVyVG9wV2lkdGgsIDEwKSArIHBhcnNlSW50KHN0bC5ib3JkZXJCb3R0b21XaWR0aCwgMTApO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICB3aWR0aDogd2lkdGgsXG4gICAgICBoZWlnaHQ6IGhlaWdodFxuICAgIH07XG4gIH1cbn07XG52YXIgX2RlZmF1bHQgPSBUb29sdGlwQ29udGVudDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQXpHQTtBQTJHQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/tooltip/TooltipContent.js
