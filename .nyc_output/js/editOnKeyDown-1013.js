/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnKeyDown
 * @format
 * 
 */


var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var KeyBindingUtil = __webpack_require__(/*! ./KeyBindingUtil */ "./node_modules/draft-js/lib/KeyBindingUtil.js");

var Keys = __webpack_require__(/*! fbjs/lib/Keys */ "./node_modules/fbjs/lib/Keys.js");

var SecondaryClipboard = __webpack_require__(/*! ./SecondaryClipboard */ "./node_modules/draft-js/lib/SecondaryClipboard.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var isEventHandled = __webpack_require__(/*! ./isEventHandled */ "./node_modules/draft-js/lib/isEventHandled.js");

var keyCommandBackspaceToStartOfLine = __webpack_require__(/*! ./keyCommandBackspaceToStartOfLine */ "./node_modules/draft-js/lib/keyCommandBackspaceToStartOfLine.js");

var keyCommandBackspaceWord = __webpack_require__(/*! ./keyCommandBackspaceWord */ "./node_modules/draft-js/lib/keyCommandBackspaceWord.js");

var keyCommandDeleteWord = __webpack_require__(/*! ./keyCommandDeleteWord */ "./node_modules/draft-js/lib/keyCommandDeleteWord.js");

var keyCommandInsertNewline = __webpack_require__(/*! ./keyCommandInsertNewline */ "./node_modules/draft-js/lib/keyCommandInsertNewline.js");

var keyCommandMoveSelectionToEndOfBlock = __webpack_require__(/*! ./keyCommandMoveSelectionToEndOfBlock */ "./node_modules/draft-js/lib/keyCommandMoveSelectionToEndOfBlock.js");

var keyCommandMoveSelectionToStartOfBlock = __webpack_require__(/*! ./keyCommandMoveSelectionToStartOfBlock */ "./node_modules/draft-js/lib/keyCommandMoveSelectionToStartOfBlock.js");

var keyCommandPlainBackspace = __webpack_require__(/*! ./keyCommandPlainBackspace */ "./node_modules/draft-js/lib/keyCommandPlainBackspace.js");

var keyCommandPlainDelete = __webpack_require__(/*! ./keyCommandPlainDelete */ "./node_modules/draft-js/lib/keyCommandPlainDelete.js");

var keyCommandTransposeCharacters = __webpack_require__(/*! ./keyCommandTransposeCharacters */ "./node_modules/draft-js/lib/keyCommandTransposeCharacters.js");

var keyCommandUndo = __webpack_require__(/*! ./keyCommandUndo */ "./node_modules/draft-js/lib/keyCommandUndo.js");

var isOptionKeyCommand = KeyBindingUtil.isOptionKeyCommand;
var isChrome = UserAgent.isBrowser('Chrome');
/**
 * Map a `DraftEditorCommand` command value to a corresponding function.
 */

function onKeyCommand(command, editorState) {
  switch (command) {
    case 'redo':
      return EditorState.redo(editorState);

    case 'delete':
      return keyCommandPlainDelete(editorState);

    case 'delete-word':
      return keyCommandDeleteWord(editorState);

    case 'backspace':
      return keyCommandPlainBackspace(editorState);

    case 'backspace-word':
      return keyCommandBackspaceWord(editorState);

    case 'backspace-to-start-of-line':
      return keyCommandBackspaceToStartOfLine(editorState);

    case 'split-block':
      return keyCommandInsertNewline(editorState);

    case 'transpose-characters':
      return keyCommandTransposeCharacters(editorState);

    case 'move-selection-to-start-of-block':
      return keyCommandMoveSelectionToStartOfBlock(editorState);

    case 'move-selection-to-end-of-block':
      return keyCommandMoveSelectionToEndOfBlock(editorState);

    case 'secondary-cut':
      return SecondaryClipboard.cut(editorState);

    case 'secondary-paste':
      return SecondaryClipboard.paste(editorState);

    default:
      return editorState;
  }
}
/**
 * Intercept keydown behavior to handle keys and commands manually, if desired.
 *
 * Keydown combinations may be mapped to `DraftCommand` values, which may
 * correspond to command functions that modify the editor or its contents.
 *
 * See `getDefaultKeyBinding` for defaults. Alternatively, the top-level
 * component may provide a custom mapping via the `keyBindingFn` prop.
 */


function editOnKeyDown(editor, e) {
  var keyCode = e.which;
  var editorState = editor._latestEditorState;

  switch (keyCode) {
    case Keys.RETURN:
      e.preventDefault(); // The top-level component may manually handle newline insertion. If
      // no special handling is performed, fall through to command handling.

      if (editor.props.handleReturn && isEventHandled(editor.props.handleReturn(e, editorState))) {
        return;
      }

      break;

    case Keys.ESC:
      e.preventDefault();
      editor.props.onEscape && editor.props.onEscape(e);
      return;

    case Keys.TAB:
      editor.props.onTab && editor.props.onTab(e);
      return;

    case Keys.UP:
      editor.props.onUpArrow && editor.props.onUpArrow(e);
      return;

    case Keys.RIGHT:
      editor.props.onRightArrow && editor.props.onRightArrow(e);
      return;

    case Keys.DOWN:
      editor.props.onDownArrow && editor.props.onDownArrow(e);
      return;

    case Keys.LEFT:
      editor.props.onLeftArrow && editor.props.onLeftArrow(e);
      return;

    case Keys.SPACE:
      // Handling for OSX where option + space scrolls.
      if (isChrome && isOptionKeyCommand(e)) {
        e.preventDefault(); // Insert a nbsp into the editor.

        var contentState = DraftModifier.replaceText(editorState.getCurrentContent(), editorState.getSelection(), '\xA0');
        editor.update(EditorState.push(editorState, contentState, 'insert-characters'));
        return;
      }

  }

  var command = editor.props.keyBindingFn(e); // If no command is specified, allow keydown event to continue.

  if (!command) {
    return;
  }

  if (command === 'undo') {
    // Since undo requires some special updating behavior to keep the editor
    // in sync, handle it separately.
    keyCommandUndo(e, editorState, editor.update);
    return;
  } // At this point, we know that we're handling a command of some kind, so
  // we don't want to insert a character following the keydown.


  e.preventDefault(); // Allow components higher up the tree to handle the command first.

  if (editor.props.handleKeyCommand && isEventHandled(editor.props.handleKeyCommand(command, editorState))) {
    return;
  }

  var newState = onKeyCommand(command, editorState);

  if (newState !== editorState) {
    editor.update(newState);
  }
}

module.exports = editOnKeyDown;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbktleURvd24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZWRpdE9uS2V5RG93bi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGVkaXRPbktleURvd25cbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBLZXlCaW5kaW5nVXRpbCA9IHJlcXVpcmUoJy4vS2V5QmluZGluZ1V0aWwnKTtcbnZhciBLZXlzID0gcmVxdWlyZSgnZmJqcy9saWIvS2V5cycpO1xudmFyIFNlY29uZGFyeUNsaXBib2FyZCA9IHJlcXVpcmUoJy4vU2Vjb25kYXJ5Q2xpcGJvYXJkJyk7XG52YXIgVXNlckFnZW50ID0gcmVxdWlyZSgnZmJqcy9saWIvVXNlckFnZW50Jyk7XG5cbnZhciBpc0V2ZW50SGFuZGxlZCA9IHJlcXVpcmUoJy4vaXNFdmVudEhhbmRsZWQnKTtcbnZhciBrZXlDb21tYW5kQmFja3NwYWNlVG9TdGFydE9mTGluZSA9IHJlcXVpcmUoJy4va2V5Q29tbWFuZEJhY2tzcGFjZVRvU3RhcnRPZkxpbmUnKTtcbnZhciBrZXlDb21tYW5kQmFja3NwYWNlV29yZCA9IHJlcXVpcmUoJy4va2V5Q29tbWFuZEJhY2tzcGFjZVdvcmQnKTtcbnZhciBrZXlDb21tYW5kRGVsZXRlV29yZCA9IHJlcXVpcmUoJy4va2V5Q29tbWFuZERlbGV0ZVdvcmQnKTtcbnZhciBrZXlDb21tYW5kSW5zZXJ0TmV3bGluZSA9IHJlcXVpcmUoJy4va2V5Q29tbWFuZEluc2VydE5ld2xpbmUnKTtcbnZhciBrZXlDb21tYW5kTW92ZVNlbGVjdGlvblRvRW5kT2ZCbG9jayA9IHJlcXVpcmUoJy4va2V5Q29tbWFuZE1vdmVTZWxlY3Rpb25Ub0VuZE9mQmxvY2snKTtcbnZhciBrZXlDb21tYW5kTW92ZVNlbGVjdGlvblRvU3RhcnRPZkJsb2NrID0gcmVxdWlyZSgnLi9rZXlDb21tYW5kTW92ZVNlbGVjdGlvblRvU3RhcnRPZkJsb2NrJyk7XG52YXIga2V5Q29tbWFuZFBsYWluQmFja3NwYWNlID0gcmVxdWlyZSgnLi9rZXlDb21tYW5kUGxhaW5CYWNrc3BhY2UnKTtcbnZhciBrZXlDb21tYW5kUGxhaW5EZWxldGUgPSByZXF1aXJlKCcuL2tleUNvbW1hbmRQbGFpbkRlbGV0ZScpO1xudmFyIGtleUNvbW1hbmRUcmFuc3Bvc2VDaGFyYWN0ZXJzID0gcmVxdWlyZSgnLi9rZXlDb21tYW5kVHJhbnNwb3NlQ2hhcmFjdGVycycpO1xudmFyIGtleUNvbW1hbmRVbmRvID0gcmVxdWlyZSgnLi9rZXlDb21tYW5kVW5kbycpO1xuXG52YXIgaXNPcHRpb25LZXlDb21tYW5kID0gS2V5QmluZGluZ1V0aWwuaXNPcHRpb25LZXlDb21tYW5kO1xuXG52YXIgaXNDaHJvbWUgPSBVc2VyQWdlbnQuaXNCcm93c2VyKCdDaHJvbWUnKTtcblxuLyoqXG4gKiBNYXAgYSBgRHJhZnRFZGl0b3JDb21tYW5kYCBjb21tYW5kIHZhbHVlIHRvIGEgY29ycmVzcG9uZGluZyBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gb25LZXlDb21tYW5kKGNvbW1hbmQsIGVkaXRvclN0YXRlKSB7XG4gIHN3aXRjaCAoY29tbWFuZCkge1xuICAgIGNhc2UgJ3JlZG8nOlxuICAgICAgcmV0dXJuIEVkaXRvclN0YXRlLnJlZG8oZWRpdG9yU3RhdGUpO1xuICAgIGNhc2UgJ2RlbGV0ZSc6XG4gICAgICByZXR1cm4ga2V5Q29tbWFuZFBsYWluRGVsZXRlKGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICdkZWxldGUtd29yZCc6XG4gICAgICByZXR1cm4ga2V5Q29tbWFuZERlbGV0ZVdvcmQoZWRpdG9yU3RhdGUpO1xuICAgIGNhc2UgJ2JhY2tzcGFjZSc6XG4gICAgICByZXR1cm4ga2V5Q29tbWFuZFBsYWluQmFja3NwYWNlKGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICdiYWNrc3BhY2Utd29yZCc6XG4gICAgICByZXR1cm4ga2V5Q29tbWFuZEJhY2tzcGFjZVdvcmQoZWRpdG9yU3RhdGUpO1xuICAgIGNhc2UgJ2JhY2tzcGFjZS10by1zdGFydC1vZi1saW5lJzpcbiAgICAgIHJldHVybiBrZXlDb21tYW5kQmFja3NwYWNlVG9TdGFydE9mTGluZShlZGl0b3JTdGF0ZSk7XG4gICAgY2FzZSAnc3BsaXQtYmxvY2snOlxuICAgICAgcmV0dXJuIGtleUNvbW1hbmRJbnNlcnROZXdsaW5lKGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICd0cmFuc3Bvc2UtY2hhcmFjdGVycyc6XG4gICAgICByZXR1cm4ga2V5Q29tbWFuZFRyYW5zcG9zZUNoYXJhY3RlcnMoZWRpdG9yU3RhdGUpO1xuICAgIGNhc2UgJ21vdmUtc2VsZWN0aW9uLXRvLXN0YXJ0LW9mLWJsb2NrJzpcbiAgICAgIHJldHVybiBrZXlDb21tYW5kTW92ZVNlbGVjdGlvblRvU3RhcnRPZkJsb2NrKGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICdtb3ZlLXNlbGVjdGlvbi10by1lbmQtb2YtYmxvY2snOlxuICAgICAgcmV0dXJuIGtleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9FbmRPZkJsb2NrKGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICdzZWNvbmRhcnktY3V0JzpcbiAgICAgIHJldHVybiBTZWNvbmRhcnlDbGlwYm9hcmQuY3V0KGVkaXRvclN0YXRlKTtcbiAgICBjYXNlICdzZWNvbmRhcnktcGFzdGUnOlxuICAgICAgcmV0dXJuIFNlY29uZGFyeUNsaXBib2FyZC5wYXN0ZShlZGl0b3JTdGF0ZSk7XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgfVxufVxuXG4vKipcbiAqIEludGVyY2VwdCBrZXlkb3duIGJlaGF2aW9yIHRvIGhhbmRsZSBrZXlzIGFuZCBjb21tYW5kcyBtYW51YWxseSwgaWYgZGVzaXJlZC5cbiAqXG4gKiBLZXlkb3duIGNvbWJpbmF0aW9ucyBtYXkgYmUgbWFwcGVkIHRvIGBEcmFmdENvbW1hbmRgIHZhbHVlcywgd2hpY2ggbWF5XG4gKiBjb3JyZXNwb25kIHRvIGNvbW1hbmQgZnVuY3Rpb25zIHRoYXQgbW9kaWZ5IHRoZSBlZGl0b3Igb3IgaXRzIGNvbnRlbnRzLlxuICpcbiAqIFNlZSBgZ2V0RGVmYXVsdEtleUJpbmRpbmdgIGZvciBkZWZhdWx0cy4gQWx0ZXJuYXRpdmVseSwgdGhlIHRvcC1sZXZlbFxuICogY29tcG9uZW50IG1heSBwcm92aWRlIGEgY3VzdG9tIG1hcHBpbmcgdmlhIHRoZSBga2V5QmluZGluZ0ZuYCBwcm9wLlxuICovXG5mdW5jdGlvbiBlZGl0T25LZXlEb3duKGVkaXRvciwgZSkge1xuICB2YXIga2V5Q29kZSA9IGUud2hpY2g7XG4gIHZhciBlZGl0b3JTdGF0ZSA9IGVkaXRvci5fbGF0ZXN0RWRpdG9yU3RhdGU7XG5cbiAgc3dpdGNoIChrZXlDb2RlKSB7XG4gICAgY2FzZSBLZXlzLlJFVFVSTjpcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIC8vIFRoZSB0b3AtbGV2ZWwgY29tcG9uZW50IG1heSBtYW51YWxseSBoYW5kbGUgbmV3bGluZSBpbnNlcnRpb24uIElmXG4gICAgICAvLyBubyBzcGVjaWFsIGhhbmRsaW5nIGlzIHBlcmZvcm1lZCwgZmFsbCB0aHJvdWdoIHRvIGNvbW1hbmQgaGFuZGxpbmcuXG4gICAgICBpZiAoZWRpdG9yLnByb3BzLmhhbmRsZVJldHVybiAmJiBpc0V2ZW50SGFuZGxlZChlZGl0b3IucHJvcHMuaGFuZGxlUmV0dXJuKGUsIGVkaXRvclN0YXRlKSkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgYnJlYWs7XG4gICAgY2FzZSBLZXlzLkVTQzpcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGVkaXRvci5wcm9wcy5vbkVzY2FwZSAmJiBlZGl0b3IucHJvcHMub25Fc2NhcGUoZSk7XG4gICAgICByZXR1cm47XG4gICAgY2FzZSBLZXlzLlRBQjpcbiAgICAgIGVkaXRvci5wcm9wcy5vblRhYiAmJiBlZGl0b3IucHJvcHMub25UYWIoZSk7XG4gICAgICByZXR1cm47XG4gICAgY2FzZSBLZXlzLlVQOlxuICAgICAgZWRpdG9yLnByb3BzLm9uVXBBcnJvdyAmJiBlZGl0b3IucHJvcHMub25VcEFycm93KGUpO1xuICAgICAgcmV0dXJuO1xuICAgIGNhc2UgS2V5cy5SSUdIVDpcbiAgICAgIGVkaXRvci5wcm9wcy5vblJpZ2h0QXJyb3cgJiYgZWRpdG9yLnByb3BzLm9uUmlnaHRBcnJvdyhlKTtcbiAgICAgIHJldHVybjtcbiAgICBjYXNlIEtleXMuRE9XTjpcbiAgICAgIGVkaXRvci5wcm9wcy5vbkRvd25BcnJvdyAmJiBlZGl0b3IucHJvcHMub25Eb3duQXJyb3coZSk7XG4gICAgICByZXR1cm47XG4gICAgY2FzZSBLZXlzLkxFRlQ6XG4gICAgICBlZGl0b3IucHJvcHMub25MZWZ0QXJyb3cgJiYgZWRpdG9yLnByb3BzLm9uTGVmdEFycm93KGUpO1xuICAgICAgcmV0dXJuO1xuICAgIGNhc2UgS2V5cy5TUEFDRTpcbiAgICAgIC8vIEhhbmRsaW5nIGZvciBPU1ggd2hlcmUgb3B0aW9uICsgc3BhY2Ugc2Nyb2xscy5cbiAgICAgIGlmIChpc0Nocm9tZSAmJiBpc09wdGlvbktleUNvbW1hbmQoZSkpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAvLyBJbnNlcnQgYSBuYnNwIGludG8gdGhlIGVkaXRvci5cbiAgICAgICAgdmFyIGNvbnRlbnRTdGF0ZSA9IERyYWZ0TW9kaWZpZXIucmVwbGFjZVRleHQoZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSwgZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCksICdcXHhBMCcpO1xuICAgICAgICBlZGl0b3IudXBkYXRlKEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGNvbnRlbnRTdGF0ZSwgJ2luc2VydC1jaGFyYWN0ZXJzJykpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gIH1cblxuICB2YXIgY29tbWFuZCA9IGVkaXRvci5wcm9wcy5rZXlCaW5kaW5nRm4oZSk7XG5cbiAgLy8gSWYgbm8gY29tbWFuZCBpcyBzcGVjaWZpZWQsIGFsbG93IGtleWRvd24gZXZlbnQgdG8gY29udGludWUuXG4gIGlmICghY29tbWFuZCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChjb21tYW5kID09PSAndW5kbycpIHtcbiAgICAvLyBTaW5jZSB1bmRvIHJlcXVpcmVzIHNvbWUgc3BlY2lhbCB1cGRhdGluZyBiZWhhdmlvciB0byBrZWVwIHRoZSBlZGl0b3JcbiAgICAvLyBpbiBzeW5jLCBoYW5kbGUgaXQgc2VwYXJhdGVseS5cbiAgICBrZXlDb21tYW5kVW5kbyhlLCBlZGl0b3JTdGF0ZSwgZWRpdG9yLnVwZGF0ZSk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gQXQgdGhpcyBwb2ludCwgd2Uga25vdyB0aGF0IHdlJ3JlIGhhbmRsaW5nIGEgY29tbWFuZCBvZiBzb21lIGtpbmQsIHNvXG4gIC8vIHdlIGRvbid0IHdhbnQgdG8gaW5zZXJ0IGEgY2hhcmFjdGVyIGZvbGxvd2luZyB0aGUga2V5ZG93bi5cbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gIC8vIEFsbG93IGNvbXBvbmVudHMgaGlnaGVyIHVwIHRoZSB0cmVlIHRvIGhhbmRsZSB0aGUgY29tbWFuZCBmaXJzdC5cbiAgaWYgKGVkaXRvci5wcm9wcy5oYW5kbGVLZXlDb21tYW5kICYmIGlzRXZlbnRIYW5kbGVkKGVkaXRvci5wcm9wcy5oYW5kbGVLZXlDb21tYW5kKGNvbW1hbmQsIGVkaXRvclN0YXRlKSkpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgbmV3U3RhdGUgPSBvbktleUNvbW1hbmQoY29tbWFuZCwgZWRpdG9yU3RhdGUpO1xuICBpZiAobmV3U3RhdGUgIT09IGVkaXRvclN0YXRlKSB7XG4gICAgZWRpdG9yLnVwZGF0ZShuZXdTdGF0ZSk7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlZGl0T25LZXlEb3duOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBMUJBO0FBNEJBO0FBRUE7Ozs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJDQTtBQUNBO0FBc0NBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnKeyDown.js
