__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/crosss-sell/sme.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n       \n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var isMobile = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile();

var Sme =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Sme, _ModelWidget);

  function Sme(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Sme);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Sme).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Sme, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        CrossSellSME: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Sme.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      if (!this.props.policy) return null;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.CrossSellSME, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Button"], {
        type: "primary",
        style: {
          width: isMobile ? "100%" : "auto"
        },
        size: "large",
        onClick: function onClick() {},
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Get a Quote").thai("Get a Quote").getMessage()));
    }
  }]);

  return Sme;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_9__["withRouter"])(antd__WEBPACK_IMPORTED_MODULE_8__["Form"].create()(Sme)));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2Nyb3Nzcy1zZWxsL3NtZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvY3Jvc3NzLXNlbGwvc21lLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBGb3JtLCBCdXR0b24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgd2l0aFJvdXRlciwgUm91dGVDb21wb25lbnRQcm9wcyB9IGZyb20gXCJyZWFjdC1yb3V0ZXJcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgU3R5bGVkRElWIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgVXRpbHMsIExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEZvcm1Db21wb25lbnRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9mb3JtXCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmV4cG9ydCB0eXBlIENyb3NzU2VsbFNNRVByb3BzID0ge1xuICBwb2xpY3k6IGFueTtcbn0gJiBGb3JtQ29tcG9uZW50UHJvcHMgJiBSb3V0ZUNvbXBvbmVudFByb3BzO1xuZXhwb3J0IHR5cGUgQ3Jvc3NTZWxsU01FU3RhdGUgPSB7fTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBDcm9zc1NlbGxTTUVDb21wb25lbnRzID0ge1xuICBDcm9zc1NlbGxTTUU6IFN0eWxlZERJVjtcbn07XG5cbmNsYXNzIFNtZTxQIGV4dGVuZHMgQ3Jvc3NTZWxsU01FUHJvcHMsXG4gIFMgZXh0ZW5kcyBDcm9zc1NlbGxTTUVTdGF0ZSxcbiAgQyBleHRlbmRzIENyb3NzU2VsbFNNRUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogQ3Jvc3NTZWxsU01FUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIENyb3NzU2VsbFNNRTogU3R5bGVkLmRpdmBcbiAgICAgICBcbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge30pIGFzIFM7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGlmICghdGhpcy5wcm9wcy5wb2xpY3kpIHJldHVybiBudWxsO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Dcm9zc1NlbGxTTUU+XG4gICAgICAgIDxCdXR0b24gdHlwZT1cInByaW1hcnlcIiBzdHlsZT17eyB3aWR0aDogaXNNb2JpbGUgPyBcIjEwMCVcIiA6IFwiYXV0b1wiIH19IHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiR2V0IGEgUXVvdGVcIilcbiAgICAgICAgICAgIC50aGFpKFwiR2V0IGEgUXVvdGVcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgPC9DLkNyb3NzU2VsbFNNRT5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhSb3V0ZXIoRm9ybS5jcmVhdGU8Q3Jvc3NTZWxsU01FUHJvcHM+KCkoU21lKSk7Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFHQTtBQUNBO0FBVUE7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUtBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7Ozs7QUFoQ0E7QUFDQTtBQWtDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/crosss-sell/sme.tsx
