__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var unsafeLifecyclesPolyfill = function unsafeLifecyclesPolyfill(Component) {
  var prototype = Component.prototype;

  if (!prototype || !prototype.isReactComponent) {
    throw new Error('Can only polyfill class components');
  } // only handle componentWillReceiveProps


  if (typeof prototype.componentWillReceiveProps !== 'function') {
    return Component;
  } // In React 16.9, React.Profiler was introduced together with UNSAFE_componentWillReceiveProps
  // https://reactjs.org/blog/2019/08/08/react-v16.9.0.html#performance-measurements-with-reactprofiler


  if (!react__WEBPACK_IMPORTED_MODULE_0___default.a.Profiler) {
    return Component;
  } // Here polyfill get started


  prototype.UNSAFE_componentWillReceiveProps = prototype.componentWillReceiveProps;
  delete prototype.componentWillReceiveProps;
  return Component;
};

/* harmony default export */ __webpack_exports__["default"] = (unsafeLifecyclesPolyfill);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9lcy91bnNhZmVMaWZlY3ljbGVzUG9seWZpbGwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11dGlsL2VzL3Vuc2FmZUxpZmVjeWNsZXNQb2x5ZmlsbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuXG52YXIgdW5zYWZlTGlmZWN5Y2xlc1BvbHlmaWxsID0gZnVuY3Rpb24gdW5zYWZlTGlmZWN5Y2xlc1BvbHlmaWxsKENvbXBvbmVudCkge1xuICB2YXIgcHJvdG90eXBlID0gQ29tcG9uZW50LnByb3RvdHlwZTtcblxuICBpZiAoIXByb3RvdHlwZSB8fCAhcHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0NhbiBvbmx5IHBvbHlmaWxsIGNsYXNzIGNvbXBvbmVudHMnKTtcbiAgfSAvLyBvbmx5IGhhbmRsZSBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzXG5cblxuICBpZiAodHlwZW9mIHByb3RvdHlwZS5jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfSAvLyBJbiBSZWFjdCAxNi45LCBSZWFjdC5Qcm9maWxlciB3YXMgaW50cm9kdWNlZCB0b2dldGhlciB3aXRoIFVOU0FGRV9jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzXG4gIC8vIGh0dHBzOi8vcmVhY3Rqcy5vcmcvYmxvZy8yMDE5LzA4LzA4L3JlYWN0LXYxNi45LjAuaHRtbCNwZXJmb3JtYW5jZS1tZWFzdXJlbWVudHMtd2l0aC1yZWFjdHByb2ZpbGVyXG5cblxuICBpZiAoIVJlYWN0LlByb2ZpbGVyKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfSAvLyBIZXJlIHBvbHlmaWxsIGdldCBzdGFydGVkXG5cblxuICBwcm90b3R5cGUuVU5TQUZFX2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSBwcm90b3R5cGUuY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcztcbiAgZGVsZXRlIHByb3RvdHlwZS5jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzO1xuICByZXR1cm4gQ29tcG9uZW50O1xufTtcblxuZXhwb3J0IGRlZmF1bHQgdW5zYWZlTGlmZWN5Y2xlc1BvbHlmaWxsOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-util/es/unsafeLifecyclesPolyfill.js
