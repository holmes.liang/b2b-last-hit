__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AutoComplete; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-select */ "./node_modules/rc-select/es/index.js");
/* harmony import */ var rc_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rc_select__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _InputElement__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./InputElement */ "./node_modules/antd/es/auto-complete/InputElement.js");
/* harmony import */ var _input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../input */ "./node_modules/antd/es/input/index.js");
/* harmony import */ var _select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../select */ "./node_modules/antd/es/select/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}









function isSelectOptionOrSelectOptGroup(child) {
  return child && child.type && (child.type.isSelectOption || child.type.isSelectOptGroup);
}

var AutoComplete =
/*#__PURE__*/
function (_React$Component) {
  _inherits(AutoComplete, _React$Component);

  function AutoComplete() {
    var _this;

    _classCallCheck(this, AutoComplete);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AutoComplete).apply(this, arguments));

    _this.saveSelect = function (node) {
      _this.select = node;
    };

    _this.getInputElement = function () {
      var children = _this.props.children;
      var element = children && react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](children) && children.type !== rc_select__WEBPACK_IMPORTED_MODULE_1__["Option"] ? react__WEBPACK_IMPORTED_MODULE_0__["Children"].only(_this.props.children) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_input__WEBPACK_IMPORTED_MODULE_4__["default"], null);

      var elementProps = _extends({}, element.props); // https://github.com/ant-design/ant-design/pull/7742


      delete elementProps.children;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_InputElement__WEBPACK_IMPORTED_MODULE_3__["default"], elementProps, element);
    };

    _this.renderAutoComplete = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          size = _this$props.size,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          notFoundContent = _this$props.notFoundContent,
          optionLabelProp = _this$props.optionLabelProp,
          dataSource = _this$props.dataSource,
          children = _this$props.children;
      var prefixCls = getPrefixCls('select', customizePrefixCls);
      var cls = classnames__WEBPACK_IMPORTED_MODULE_2___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-lg"), size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-sm"), size === 'small'), _defineProperty(_classNames, className, !!className), _defineProperty(_classNames, "".concat(prefixCls, "-show-search"), true), _defineProperty(_classNames, "".concat(prefixCls, "-auto-complete"), true), _classNames));
      var options;
      var childArray = react__WEBPACK_IMPORTED_MODULE_0__["Children"].toArray(children);

      if (childArray.length && isSelectOptionOrSelectOptGroup(childArray[0])) {
        options = children;
      } else {
        options = dataSource ? dataSource.map(function (item) {
          if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](item)) {
            return item;
          }

          switch (_typeof(item)) {
            case 'string':
              return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_select__WEBPACK_IMPORTED_MODULE_1__["Option"], {
                key: item
              }, item);

            case 'object':
              return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_select__WEBPACK_IMPORTED_MODULE_1__["Option"], {
                key: item.value
              }, item.text);

            default:
              throw new Error('AutoComplete[dataSource] only supports type `string[] | Object[]`.');
          }
        }) : [];
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_select__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({}, _this.props, {
        className: cls,
        mode: _select__WEBPACK_IMPORTED_MODULE_5__["default"].SECRET_COMBOBOX_MODE_DO_NOT_USE,
        optionLabelProp: optionLabelProp,
        getInputElement: _this.getInputElement,
        notFoundContent: notFoundContent,
        ref: _this.saveSelect
      }), options);
    };

    return _this;
  }

  _createClass(AutoComplete, [{
    key: "focus",
    value: function focus() {
      this.select.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.select.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderAutoComplete);
    }
  }]);

  return AutoComplete;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


AutoComplete.Option = rc_select__WEBPACK_IMPORTED_MODULE_1__["Option"];
AutoComplete.OptGroup = rc_select__WEBPACK_IMPORTED_MODULE_1__["OptGroup"];
AutoComplete.defaultProps = {
  transitionName: 'slide-up',
  optionLabelProp: 'children',
  choiceTransitionName: 'zoom',
  showSearch: false,
  filterOption: false
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hdXRvLWNvbXBsZXRlL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9hdXRvLWNvbXBsZXRlL2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBPcHRpb24sIE9wdEdyb3VwIH0gZnJvbSAncmMtc2VsZWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IElucHV0RWxlbWVudCBmcm9tICcuL0lucHV0RWxlbWVudCc7XG5pbXBvcnQgSW5wdXQgZnJvbSAnLi4vaW5wdXQnO1xuaW1wb3J0IFNlbGVjdCBmcm9tICcuLi9zZWxlY3QnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuZnVuY3Rpb24gaXNTZWxlY3RPcHRpb25PclNlbGVjdE9wdEdyb3VwKGNoaWxkKSB7XG4gICAgcmV0dXJuIGNoaWxkICYmIGNoaWxkLnR5cGUgJiYgKGNoaWxkLnR5cGUuaXNTZWxlY3RPcHRpb24gfHwgY2hpbGQudHlwZS5pc1NlbGVjdE9wdEdyb3VwKTtcbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEF1dG9Db21wbGV0ZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuc2F2ZVNlbGVjdCA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNlbGVjdCA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuZ2V0SW5wdXRFbGVtZW50ID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBjaGlsZHJlbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBjaGlsZHJlbiAmJiBSZWFjdC5pc1ZhbGlkRWxlbWVudChjaGlsZHJlbikgJiYgY2hpbGRyZW4udHlwZSAhPT0gT3B0aW9uID8gKFJlYWN0LkNoaWxkcmVuLm9ubHkodGhpcy5wcm9wcy5jaGlsZHJlbikpIDogKDxJbnB1dCAvPik7XG4gICAgICAgICAgICBjb25zdCBlbGVtZW50UHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCBlbGVtZW50LnByb3BzKTtcbiAgICAgICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vcHVsbC83NzQyXG4gICAgICAgICAgICBkZWxldGUgZWxlbWVudFByb3BzLmNoaWxkcmVuO1xuICAgICAgICAgICAgcmV0dXJuIDxJbnB1dEVsZW1lbnQgey4uLmVsZW1lbnRQcm9wc30+e2VsZW1lbnR9PC9JbnB1dEVsZW1lbnQ+O1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckF1dG9Db21wbGV0ZSA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBzaXplLCBjbGFzc05hbWUgPSAnJywgbm90Rm91bmRDb250ZW50LCBvcHRpb25MYWJlbFByb3AsIGRhdGFTb3VyY2UsIGNoaWxkcmVuLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnc2VsZWN0JywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGNscyA9IGNsYXNzTmFtZXMoe1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWxnYF06IHNpemUgPT09ICdsYXJnZScsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc21gXTogc2l6ZSA9PT0gJ3NtYWxsJyxcbiAgICAgICAgICAgICAgICBbY2xhc3NOYW1lXTogISFjbGFzc05hbWUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc2hvdy1zZWFyY2hgXTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1hdXRvLWNvbXBsZXRlYF06IHRydWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGxldCBvcHRpb25zO1xuICAgICAgICAgICAgY29uc3QgY2hpbGRBcnJheSA9IFJlYWN0LkNoaWxkcmVuLnRvQXJyYXkoY2hpbGRyZW4pO1xuICAgICAgICAgICAgaWYgKGNoaWxkQXJyYXkubGVuZ3RoICYmIGlzU2VsZWN0T3B0aW9uT3JTZWxlY3RPcHRHcm91cChjaGlsZEFycmF5WzBdKSkge1xuICAgICAgICAgICAgICAgIG9wdGlvbnMgPSBjaGlsZHJlbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIG9wdGlvbnMgPSBkYXRhU291cmNlXG4gICAgICAgICAgICAgICAgICAgID8gZGF0YVNvdXJjZS5tYXAoaXRlbSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoUmVhY3QuaXNWYWxpZEVsZW1lbnQoaXRlbSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gaXRlbTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN3aXRjaCAodHlwZW9mIGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdzdHJpbmcnOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gPE9wdGlvbiBrZXk9e2l0ZW19PntpdGVtfTwvT3B0aW9uPjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdvYmplY3QnOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKDxPcHRpb24ga2V5PXtpdGVtLnZhbHVlfT5cbiAgICAgICAgICAgICAgICAgICAge2l0ZW0udGV4dH1cbiAgICAgICAgICAgICAgICAgIDwvT3B0aW9uPik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBdXRvQ29tcGxldGVbZGF0YVNvdXJjZV0gb25seSBzdXBwb3J0cyB0eXBlIGBzdHJpbmdbXSB8IE9iamVjdFtdYC4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgOiBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPFNlbGVjdCB7Li4udGhpcy5wcm9wc30gY2xhc3NOYW1lPXtjbHN9IG1vZGU9e1NlbGVjdC5TRUNSRVRfQ09NQk9CT1hfTU9ERV9ET19OT1RfVVNFfSBvcHRpb25MYWJlbFByb3A9e29wdGlvbkxhYmVsUHJvcH0gZ2V0SW5wdXRFbGVtZW50PXt0aGlzLmdldElucHV0RWxlbWVudH0gbm90Rm91bmRDb250ZW50PXtub3RGb3VuZENvbnRlbnR9IHJlZj17dGhpcy5zYXZlU2VsZWN0fT5cbiAgICAgICAge29wdGlvbnN9XG4gICAgICA8L1NlbGVjdD4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBmb2N1cygpIHtcbiAgICAgICAgdGhpcy5zZWxlY3QuZm9jdXMoKTtcbiAgICB9XG4gICAgYmx1cigpIHtcbiAgICAgICAgdGhpcy5zZWxlY3QuYmx1cigpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQXV0b0NvbXBsZXRlfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbkF1dG9Db21wbGV0ZS5PcHRpb24gPSBPcHRpb247XG5BdXRvQ29tcGxldGUuT3B0R3JvdXAgPSBPcHRHcm91cDtcbkF1dG9Db21wbGV0ZS5kZWZhdWx0UHJvcHMgPSB7XG4gICAgdHJhbnNpdGlvbk5hbWU6ICdzbGlkZS11cCcsXG4gICAgb3B0aW9uTGFiZWxQcm9wOiAnY2hpbGRyZW4nLFxuICAgIGNob2ljZVRyYW5zaXRpb25OYW1lOiAnem9vbScsXG4gICAgc2hvd1NlYXJjaDogZmFsc2UsXG4gICAgZmlsdGVyT3B0aW9uOiBmYWxzZSxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQU5BO0FBQ0E7QUFPQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFSQTtBQUxBO0FBaUJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbENBO0FBQ0E7QUFkQTtBQW1EQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBN0RBO0FBQ0E7QUFEQTtBQStEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/auto-complete/index.js
