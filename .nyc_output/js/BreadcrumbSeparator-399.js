__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BreadcrumbSeparator; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}




var BreadcrumbSeparator =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BreadcrumbSeparator, _React$Component);

  function BreadcrumbSeparator() {
    var _this;

    _classCallCheck(this, BreadcrumbSeparator);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BreadcrumbSeparator).apply(this, arguments));

    _this.renderSeparator = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var children = _this.props.children;
      var prefixCls = getPrefixCls('breadcrumb');
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-separator")
      }, children || '/');
    };

    return _this;
  }

  _createClass(BreadcrumbSeparator, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_1__["ConfigConsumer"], null, this.renderSeparator);
    }
  }]);

  return BreadcrumbSeparator;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


BreadcrumbSeparator.__ANT_BREADCRUMB_SEPARATOR = true;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9icmVhZGNydW1iL0JyZWFkY3J1bWJTZXBhcmF0b3IuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2JyZWFkY3J1bWIvQnJlYWRjcnVtYlNlcGFyYXRvci5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnJlYWRjcnVtYlNlcGFyYXRvciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMucmVuZGVyU2VwYXJhdG9yID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgY2hpbGRyZW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2JyZWFkY3J1bWInKTtcbiAgICAgICAgICAgIHJldHVybiA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tc2VwYXJhdG9yYH0+e2NoaWxkcmVuIHx8ICcvJ308L3NwYW4+O1xuICAgICAgICB9O1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyU2VwYXJhdG9yfTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbkJyZWFkY3J1bWJTZXBhcmF0b3IuX19BTlRfQlJFQURDUlVNQl9TRVBBUkFUT1IgPSB0cnVlO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUhBO0FBQ0E7QUFIQTtBQU9BO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7OztBQVhBO0FBQ0E7QUFEQTtBQWFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/breadcrumb/BreadcrumbSeparator.js
