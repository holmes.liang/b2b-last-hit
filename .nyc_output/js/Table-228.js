

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var shallowequal_1 = __importDefault(__webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js"));

var addEventListener_1 = __importDefault(__webpack_require__(/*! rc-util/lib/Dom/addEventListener */ "./node_modules/rc-util/lib/Dom/addEventListener.js"));

var warning_1 = __importDefault(__webpack_require__(/*! rc-util/lib/warning */ "./node_modules/rc-util/lib/warning.js"));

var mini_store_1 = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");

var merge_1 = __importDefault(__webpack_require__(/*! lodash/merge */ "./node_modules/lodash/merge.js"));

var component_classes_1 = __importDefault(__webpack_require__(/*! component-classes */ "./node_modules/component-classes/index.js"));

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var react_lifecycles_compat_1 = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");

var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/rc-table/es/utils.js");

var ColumnManager_1 = __importDefault(__webpack_require__(/*! ./ColumnManager */ "./node_modules/rc-table/es/ColumnManager.js"));

var HeadTable_1 = __importDefault(__webpack_require__(/*! ./HeadTable */ "./node_modules/rc-table/es/HeadTable.js"));

var BodyTable_1 = __importDefault(__webpack_require__(/*! ./BodyTable */ "./node_modules/rc-table/es/BodyTable.js"));

var Column_1 = __importDefault(__webpack_require__(/*! ./Column */ "./node_modules/rc-table/es/Column.js"));

var ColumnGroup_1 = __importDefault(__webpack_require__(/*! ./ColumnGroup */ "./node_modules/rc-table/es/ColumnGroup.js"));

var ExpandableTable_1 = __importDefault(__webpack_require__(/*! ./ExpandableTable */ "./node_modules/rc-table/es/ExpandableTable.js"));

var Table =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Table, _React$Component);

  function Table(props) {
    var _this;

    _classCallCheck(this, Table);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Table).call(this, props));
    _this.state = {};

    _this.getRowKey = function (record, index) {
      var rowKey = _this.props.rowKey;
      var key = typeof rowKey === 'function' ? rowKey(record, index) : record[rowKey];
      warning_1.default(key !== undefined, 'Each record in table should have a unique `key` prop,' + 'or set `rowKey` to an unique primary key.');
      return key === undefined ? index : key;
    };

    _this.handleWindowResize = function () {
      _this.syncFixedTableRowHeight();

      _this.setScrollPositionClassName();
    };

    _this.syncFixedTableRowHeight = function () {
      var tableRect = _this.tableNode.getBoundingClientRect(); // If tableNode's height less than 0, suppose it is hidden and don't recalculate rowHeight.
      // see: https://github.com/ant-design/ant-design/issues/4836


      if (tableRect.height !== undefined && tableRect.height <= 0) {
        return;
      }

      var prefixCls = _this.props.prefixCls;
      var headRows = _this.headTable ? _this.headTable.querySelectorAll('thead') : _this.bodyTable.querySelectorAll('thead');
      var bodyRows = _this.bodyTable.querySelectorAll(".".concat(prefixCls, "-row")) || [];
      var fixedColumnsHeadRowsHeight = [].map.call(headRows, function (row) {
        return row.getBoundingClientRect().height || 'auto';
      });

      var state = _this.store.getState();

      var fixedColumnsBodyRowsHeight = [].reduce.call(bodyRows, function (acc, row) {
        var rowKey = row.getAttribute('data-row-key');
        var height = row.getBoundingClientRect().height || state.fixedColumnsBodyRowsHeight[rowKey] || 'auto';
        acc[rowKey] = height;
        return acc;
      }, {});

      if (shallowequal_1.default(state.fixedColumnsHeadRowsHeight, fixedColumnsHeadRowsHeight) && shallowequal_1.default(state.fixedColumnsBodyRowsHeight, fixedColumnsBodyRowsHeight)) {
        return;
      }

      _this.store.setState({
        fixedColumnsHeadRowsHeight: fixedColumnsHeadRowsHeight,
        fixedColumnsBodyRowsHeight: fixedColumnsBodyRowsHeight
      });
    };

    _this.handleBodyScrollLeft = function (e) {
      // Fix https://github.com/ant-design/ant-design/issues/7635
      if (e.currentTarget !== e.target) {
        return;
      }

      var target = e.target;
      var _this$props$scroll = _this.props.scroll,
          scroll = _this$props$scroll === void 0 ? {} : _this$props$scroll;

      var _assertThisInitialize = _assertThisInitialized(_this),
          headTable = _assertThisInitialize.headTable,
          bodyTable = _assertThisInitialize.bodyTable;

      if (target.scrollLeft !== _this.lastScrollLeft && scroll.x) {
        if (target === bodyTable && headTable) {
          headTable.scrollLeft = target.scrollLeft;
        } else if (target === headTable && bodyTable) {
          bodyTable.scrollLeft = target.scrollLeft;
        }

        _this.setScrollPositionClassName();
      } // Remember last scrollLeft for scroll direction detecting.


      _this.lastScrollLeft = target.scrollLeft;
    };

    _this.handleBodyScrollTop = function (e) {
      var target = e.target; // Fix https://github.com/ant-design/ant-design/issues/9033

      if (e.currentTarget !== target) {
        return;
      }

      var _this$props$scroll2 = _this.props.scroll,
          scroll = _this$props$scroll2 === void 0 ? {} : _this$props$scroll2;

      var _assertThisInitialize2 = _assertThisInitialized(_this),
          headTable = _assertThisInitialize2.headTable,
          bodyTable = _assertThisInitialize2.bodyTable,
          fixedColumnsBodyLeft = _assertThisInitialize2.fixedColumnsBodyLeft,
          fixedColumnsBodyRight = _assertThisInitialize2.fixedColumnsBodyRight;

      if (target.scrollTop !== _this.lastScrollTop && scroll.y && target !== headTable) {
        var scrollTop = target.scrollTop;

        if (fixedColumnsBodyLeft && target !== fixedColumnsBodyLeft) {
          fixedColumnsBodyLeft.scrollTop = scrollTop;
        }

        if (fixedColumnsBodyRight && target !== fixedColumnsBodyRight) {
          fixedColumnsBodyRight.scrollTop = scrollTop;
        }

        if (bodyTable && target !== bodyTable) {
          bodyTable.scrollTop = scrollTop;
        }
      } // Remember last scrollTop for scroll direction detecting.


      _this.lastScrollTop = target.scrollTop;
    };

    _this.handleBodyScroll = function (e) {
      _this.handleBodyScrollLeft(e);

      _this.handleBodyScrollTop(e);
    };

    _this.handleWheel = function (event) {
      var _this$props$scroll3 = _this.props.scroll,
          scroll = _this$props$scroll3 === void 0 ? {} : _this$props$scroll3;

      if (window.navigator.userAgent.match(/Trident\/7\./) && scroll.y) {
        event.preventDefault();
        var wd = event.deltaY;
        var target = event.target;

        var _assertThisInitialize3 = _assertThisInitialized(_this),
            bodyTable = _assertThisInitialize3.bodyTable,
            fixedColumnsBodyLeft = _assertThisInitialize3.fixedColumnsBodyLeft,
            fixedColumnsBodyRight = _assertThisInitialize3.fixedColumnsBodyRight;

        var scrollTop = 0;

        if (_this.lastScrollTop) {
          scrollTop = _this.lastScrollTop + wd;
        } else {
          scrollTop = wd;
        }

        if (fixedColumnsBodyLeft && target !== fixedColumnsBodyLeft) {
          fixedColumnsBodyLeft.scrollTop = scrollTop;
        }

        if (fixedColumnsBodyRight && target !== fixedColumnsBodyRight) {
          fixedColumnsBodyRight.scrollTop = scrollTop;
        }

        if (bodyTable && target !== bodyTable) {
          bodyTable.scrollTop = scrollTop;
        }
      }
    };

    _this.saveRef = function (name) {
      return function (node) {
        _this[name] = node;
      };
    };

    _this.saveTableNodeRef = function (node) {
      _this.tableNode = node;
    };

    ['onRowClick', 'onRowDoubleClick', 'onRowContextMenu', 'onRowMouseEnter', 'onRowMouseLeave'].forEach(function (name) {
      warning_1.default(props[name] === undefined, "".concat(name, " is deprecated, please use onRow instead."));
    });
    warning_1.default(props.getBodyWrapper === undefined, 'getBodyWrapper is deprecated, please use custom components instead.');
    _this.columnManager = new ColumnManager_1.default(props.columns, props.children);
    _this.store = mini_store_1.create({
      currentHoverKey: null,
      fixedColumnsHeadRowsHeight: [],
      fixedColumnsBodyRowsHeight: {}
    });

    _this.setScrollPosition('left');

    _this.debouncedWindowResize = utils_1.debounce(_this.handleWindowResize, 150);
    return _this;
  }

  _createClass(Table, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        table: {
          props: this.props,
          columnManager: this.columnManager,
          saveRef: this.saveRef,
          components: merge_1.default({
            table: 'table',
            header: {
              wrapper: 'thead',
              row: 'tr',
              cell: 'th'
            },
            body: {
              wrapper: 'tbody',
              row: 'tr',
              cell: 'td'
            }
          }, this.props.components)
        }
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.columnManager.isAnyColumnsFixed()) {
        this.handleWindowResize();
        this.resizeEvent = addEventListener_1.default(window, 'resize', this.debouncedWindowResize);
      } // https://github.com/ant-design/ant-design/issues/11635


      if (this.headTable) {
        this.headTable.scrollLeft = 0;
      }

      if (this.bodyTable) {
        this.bodyTable.scrollLeft = 0;
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.columnManager.isAnyColumnsFixed()) {
        this.handleWindowResize();

        if (!this.resizeEvent) {
          this.resizeEvent = addEventListener_1.default(window, 'resize', this.debouncedWindowResize);
        }
      } // when table changes to empty, reset scrollLeft


      if (prevProps.data.length > 0 && this.props.data.length === 0 && this.hasScrollX()) {
        this.resetScrollX();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.resizeEvent) {
        this.resizeEvent.remove();
      }

      if (this.debouncedWindowResize) {
        this.debouncedWindowResize.cancel();
      }
    }
  }, {
    key: "setScrollPosition",
    value: function setScrollPosition(position) {
      this.scrollPosition = position;

      if (this.tableNode) {
        var prefixCls = this.props.prefixCls;

        if (position === 'both') {
          component_classes_1.default(this.tableNode).remove(new RegExp("^".concat(prefixCls, "-scroll-position-.+$"))).add("".concat(prefixCls, "-scroll-position-left")).add("".concat(prefixCls, "-scroll-position-right"));
        } else {
          component_classes_1.default(this.tableNode).remove(new RegExp("^".concat(prefixCls, "-scroll-position-.+$"))).add("".concat(prefixCls, "-scroll-position-").concat(position));
        }
      }
    }
  }, {
    key: "setScrollPositionClassName",
    value: function setScrollPositionClassName() {
      var node = this.bodyTable;
      var scrollToLeft = node.scrollLeft === 0;
      var scrollToRight = node.scrollLeft + 1 >= node.children[0].getBoundingClientRect().width - node.getBoundingClientRect().width;

      if (scrollToLeft && scrollToRight) {
        this.setScrollPosition('both');
      } else if (scrollToLeft) {
        this.setScrollPosition('left');
      } else if (scrollToRight) {
        this.setScrollPosition('right');
      } else if (this.scrollPosition !== 'middle') {
        this.setScrollPosition('middle');
      }
    }
  }, {
    key: "isTableLayoutFixed",
    value: function isTableLayoutFixed() {
      var _this$props = this.props,
          tableLayout = _this$props.tableLayout,
          _this$props$columns = _this$props.columns,
          columns = _this$props$columns === void 0 ? [] : _this$props$columns,
          useFixedHeader = _this$props.useFixedHeader,
          _this$props$scroll4 = _this$props.scroll,
          scroll = _this$props$scroll4 === void 0 ? {} : _this$props$scroll4;

      if (typeof tableLayout !== 'undefined') {
        return tableLayout === 'fixed';
      } // if one column is ellipsis, use fixed table layout to fix align issue


      if (columns.some(function (_ref) {
        var ellipsis = _ref.ellipsis;
        return !!ellipsis;
      })) {
        return true;
      } // if header fixed, use fixed table layout to fix align issue


      if (useFixedHeader || scroll.y) {
        return true;
      } // if scroll.x is number/px/% width value, we should fixed table layout
      // to avoid long word layout broken issue


      if (scroll.x && scroll.x !== true && scroll.x !== 'max-content') {
        return true;
      }

      return false;
    }
  }, {
    key: "resetScrollX",
    value: function resetScrollX() {
      if (this.headTable) {
        this.headTable.scrollLeft = 0;
      }

      if (this.bodyTable) {
        this.bodyTable.scrollLeft = 0;
      }
    }
  }, {
    key: "hasScrollX",
    value: function hasScrollX() {
      var _this$props$scroll5 = this.props.scroll,
          scroll = _this$props$scroll5 === void 0 ? {} : _this$props$scroll5;
      return 'x' in scroll;
    }
  }, {
    key: "renderMainTable",
    value: function renderMainTable() {
      var _this$props2 = this.props,
          scroll = _this$props2.scroll,
          prefixCls = _this$props2.prefixCls;
      var isAnyColumnsFixed = this.columnManager.isAnyColumnsFixed();
      var scrollable = isAnyColumnsFixed || scroll.x || scroll.y;
      var table = [this.renderTable({
        columns: this.columnManager.groupedColumns(),
        isAnyColumnsFixed: isAnyColumnsFixed
      }), this.renderEmptyText(), this.renderFooter()];
      return scrollable ? React.createElement("div", {
        className: "".concat(prefixCls, "-scroll")
      }, table) : table;
    }
  }, {
    key: "renderLeftFixedTable",
    value: function renderLeftFixedTable() {
      var prefixCls = this.props.prefixCls;
      return React.createElement("div", {
        className: "".concat(prefixCls, "-fixed-left")
      }, this.renderTable({
        columns: this.columnManager.leftColumns(),
        fixed: 'left'
      }));
    }
  }, {
    key: "renderRightFixedTable",
    value: function renderRightFixedTable() {
      var prefixCls = this.props.prefixCls;
      return React.createElement("div", {
        className: "".concat(prefixCls, "-fixed-right")
      }, this.renderTable({
        columns: this.columnManager.rightColumns(),
        fixed: 'right'
      }));
    }
  }, {
    key: "renderTable",
    value: function renderTable(options) {
      var columns = options.columns,
          fixed = options.fixed,
          isAnyColumnsFixed = options.isAnyColumnsFixed;
      var _this$props3 = this.props,
          prefixCls = _this$props3.prefixCls,
          _this$props3$scroll = _this$props3.scroll,
          scroll = _this$props3$scroll === void 0 ? {} : _this$props3$scroll;
      var tableClassName = scroll.x || fixed ? "".concat(prefixCls, "-fixed") : '';
      var headTable = React.createElement(HeadTable_1.default, {
        key: "head",
        columns: columns,
        fixed: fixed,
        tableClassName: tableClassName,
        handleBodyScrollLeft: this.handleBodyScrollLeft,
        expander: this.expander
      });
      var bodyTable = React.createElement(BodyTable_1.default, {
        key: "body",
        columns: columns,
        fixed: fixed,
        tableClassName: tableClassName,
        getRowKey: this.getRowKey,
        handleWheel: this.handleWheel,
        handleBodyScroll: this.handleBodyScroll,
        expander: this.expander,
        isAnyColumnsFixed: isAnyColumnsFixed
      });
      return [headTable, bodyTable];
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      var _this$props4 = this.props,
          title = _this$props4.title,
          prefixCls = _this$props4.prefixCls;
      return title ? React.createElement("div", {
        className: "".concat(prefixCls, "-title"),
        key: "title"
      }, title(this.props.data)) : null;
    }
  }, {
    key: "renderFooter",
    value: function renderFooter() {
      var _this$props5 = this.props,
          footer = _this$props5.footer,
          prefixCls = _this$props5.prefixCls;
      return footer ? React.createElement("div", {
        className: "".concat(prefixCls, "-footer"),
        key: "footer"
      }, footer(this.props.data)) : null;
    }
  }, {
    key: "renderEmptyText",
    value: function renderEmptyText() {
      var _this$props6 = this.props,
          emptyText = _this$props6.emptyText,
          prefixCls = _this$props6.prefixCls,
          data = _this$props6.data;

      if (data.length) {
        return null;
      }

      var emptyClassName = "".concat(prefixCls, "-placeholder");
      return React.createElement("div", {
        className: emptyClassName,
        key: "emptyText"
      }, typeof emptyText === 'function' ? emptyText() : emptyText);
    }
  }, {
    key: "render",
    value: function render() {
      var _classnames_1$default,
          _this2 = this;

      var props = this.props;
      var prefixCls = props.prefixCls;

      if (this.state.columns) {
        this.columnManager.reset(props.columns);
      } else if (this.state.children) {
        this.columnManager.reset(null, props.children);
      }

      var tableClassName = classnames_1.default(props.prefixCls, props.className, (_classnames_1$default = {}, _defineProperty(_classnames_1$default, "".concat(prefixCls, "-fixed-header"), props.useFixedHeader || props.scroll && props.scroll.y), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-scroll-position-left ").concat(prefixCls, "-scroll-position-right"), this.scrollPosition === 'both'), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-scroll-position-").concat(this.scrollPosition), this.scrollPosition !== 'both'), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-layout-fixed"), this.isTableLayoutFixed()), _classnames_1$default));
      var hasLeftFixed = this.columnManager.isAnyColumnsLeftFixed();
      var hasRightFixed = this.columnManager.isAnyColumnsRightFixed();
      var dataAndAriaProps = utils_1.getDataAndAriaProps(props);
      return React.createElement(mini_store_1.Provider, {
        store: this.store
      }, React.createElement(ExpandableTable_1.default, Object.assign({}, props, {
        columnManager: this.columnManager,
        getRowKey: this.getRowKey
      }), function (expander) {
        _this2.expander = expander;
        return React.createElement("div", Object.assign({
          ref: _this2.saveTableNodeRef,
          className: tableClassName,
          style: props.style,
          id: props.id
        }, dataAndAriaProps), _this2.renderTitle(), React.createElement("div", {
          className: "".concat(prefixCls, "-content")
        }, _this2.renderMainTable(), hasLeftFixed && _this2.renderLeftFixedTable(), hasRightFixed && _this2.renderRightFixedTable()));
      }));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      if (nextProps.columns && nextProps.columns !== prevState.columns) {
        return {
          columns: nextProps.columns,
          children: null
        };
      }

      if (nextProps.children !== prevState.children) {
        return {
          columns: null,
          children: nextProps.children
        };
      }

      return null;
    }
  }]);

  return Table;
}(React.Component);

Table.childContextTypes = {
  table: PropTypes.any,
  components: PropTypes.any
};
Table.Column = Column_1.default;
Table.ColumnGroup = ColumnGroup_1.default;
Table.defaultProps = {
  data: [],
  useFixedHeader: false,
  rowKey: 'key',
  rowClassName: function rowClassName() {
    return '';
  },
  onRow: function onRow() {},
  onHeaderRow: function onHeaderRow() {},
  prefixCls: 'rc-table',
  bodyStyle: {},
  style: {},
  showHeader: true,
  scroll: {},
  rowRef: function rowRef() {
    return null;
  },
  emptyText: function emptyText() {
    return 'No Data';
  }
};
react_lifecycles_compat_1.polyfill(Table);
exports.default = Table;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvVGFibGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10YWJsZS9lcy9UYWJsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfTsgfSBlbHNlIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9OyB9IHJldHVybiBfdHlwZW9mKG9iaik7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IF9nZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5nZXRQcm90b3R5cGVPZiA6IGZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7IH07IHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7IH1cblxuZnVuY3Rpb24gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKSB7IGlmIChzZWxmID09PSB2b2lkIDApIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgUHJvcFR5cGVzID0gX19pbXBvcnRTdGFyKHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpKTtcblxudmFyIHNoYWxsb3dlcXVhbF8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJzaGFsbG93ZXF1YWxcIikpO1xuXG52YXIgYWRkRXZlbnRMaXN0ZW5lcl8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJyYy11dGlsL2xpYi9Eb20vYWRkRXZlbnRMaXN0ZW5lclwiKSk7XG5cbnZhciB3YXJuaW5nXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcInJjLXV0aWwvbGliL3dhcm5pbmdcIikpO1xuXG52YXIgbWluaV9zdG9yZV8xID0gcmVxdWlyZShcIm1pbmktc3RvcmVcIik7XG5cbnZhciBtZXJnZV8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJsb2Rhc2gvbWVyZ2VcIikpO1xuXG52YXIgY29tcG9uZW50X2NsYXNzZXNfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiY29tcG9uZW50LWNsYXNzZXNcIikpO1xuXG52YXIgY2xhc3NuYW1lc18xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJjbGFzc25hbWVzXCIpKTtcblxudmFyIHJlYWN0X2xpZmVjeWNsZXNfY29tcGF0XzEgPSByZXF1aXJlKFwicmVhY3QtbGlmZWN5Y2xlcy1jb21wYXRcIik7XG5cbnZhciB1dGlsc18xID0gcmVxdWlyZShcIi4vdXRpbHNcIik7XG5cbnZhciBDb2x1bW5NYW5hZ2VyXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vQ29sdW1uTWFuYWdlclwiKSk7XG5cbnZhciBIZWFkVGFibGVfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiLi9IZWFkVGFibGVcIikpO1xuXG52YXIgQm9keVRhYmxlXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vQm9keVRhYmxlXCIpKTtcblxudmFyIENvbHVtbl8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL0NvbHVtblwiKSk7XG5cbnZhciBDb2x1bW5Hcm91cF8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL0NvbHVtbkdyb3VwXCIpKTtcblxudmFyIEV4cGFuZGFibGVUYWJsZV8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCIuL0V4cGFuZGFibGVUYWJsZVwiKSk7XG5cbnZhciBUYWJsZSA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoVGFibGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRhYmxlKHByb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFRhYmxlKTtcblxuICAgIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX2dldFByb3RvdHlwZU9mKFRhYmxlKS5jYWxsKHRoaXMsIHByb3BzKSk7XG4gICAgX3RoaXMuc3RhdGUgPSB7fTtcblxuICAgIF90aGlzLmdldFJvd0tleSA9IGZ1bmN0aW9uIChyZWNvcmQsIGluZGV4KSB7XG4gICAgICB2YXIgcm93S2V5ID0gX3RoaXMucHJvcHMucm93S2V5O1xuICAgICAgdmFyIGtleSA9IHR5cGVvZiByb3dLZXkgPT09ICdmdW5jdGlvbicgPyByb3dLZXkocmVjb3JkLCBpbmRleCkgOiByZWNvcmRbcm93S2V5XTtcbiAgICAgIHdhcm5pbmdfMS5kZWZhdWx0KGtleSAhPT0gdW5kZWZpbmVkLCAnRWFjaCByZWNvcmQgaW4gdGFibGUgc2hvdWxkIGhhdmUgYSB1bmlxdWUgYGtleWAgcHJvcCwnICsgJ29yIHNldCBgcm93S2V5YCB0byBhbiB1bmlxdWUgcHJpbWFyeSBrZXkuJyk7XG4gICAgICByZXR1cm4ga2V5ID09PSB1bmRlZmluZWQgPyBpbmRleCA6IGtleTtcbiAgICB9O1xuXG4gICAgX3RoaXMuaGFuZGxlV2luZG93UmVzaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuc3luY0ZpeGVkVGFibGVSb3dIZWlnaHQoKTtcblxuICAgICAgX3RoaXMuc2V0U2Nyb2xsUG9zaXRpb25DbGFzc05hbWUoKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc3luY0ZpeGVkVGFibGVSb3dIZWlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgdGFibGVSZWN0ID0gX3RoaXMudGFibGVOb2RlLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpOyAvLyBJZiB0YWJsZU5vZGUncyBoZWlnaHQgbGVzcyB0aGFuIDAsIHN1cHBvc2UgaXQgaXMgaGlkZGVuIGFuZCBkb24ndCByZWNhbGN1bGF0ZSByb3dIZWlnaHQuXG4gICAgICAvLyBzZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzQ4MzZcblxuXG4gICAgICBpZiAodGFibGVSZWN0LmhlaWdodCAhPT0gdW5kZWZpbmVkICYmIHRhYmxlUmVjdC5oZWlnaHQgPD0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBwcmVmaXhDbHMgPSBfdGhpcy5wcm9wcy5wcmVmaXhDbHM7XG4gICAgICB2YXIgaGVhZFJvd3MgPSBfdGhpcy5oZWFkVGFibGUgPyBfdGhpcy5oZWFkVGFibGUucXVlcnlTZWxlY3RvckFsbCgndGhlYWQnKSA6IF90aGlzLmJvZHlUYWJsZS5xdWVyeVNlbGVjdG9yQWxsKCd0aGVhZCcpO1xuICAgICAgdmFyIGJvZHlSb3dzID0gX3RoaXMuYm9keVRhYmxlLnF1ZXJ5U2VsZWN0b3JBbGwoXCIuXCIuY29uY2F0KHByZWZpeENscywgXCItcm93XCIpKSB8fCBbXTtcbiAgICAgIHZhciBmaXhlZENvbHVtbnNIZWFkUm93c0hlaWdodCA9IFtdLm1hcC5jYWxsKGhlYWRSb3dzLCBmdW5jdGlvbiAocm93KSB7XG4gICAgICAgIHJldHVybiByb3cuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkuaGVpZ2h0IHx8ICdhdXRvJztcbiAgICAgIH0pO1xuXG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5zdG9yZS5nZXRTdGF0ZSgpO1xuXG4gICAgICB2YXIgZml4ZWRDb2x1bW5zQm9keVJvd3NIZWlnaHQgPSBbXS5yZWR1Y2UuY2FsbChib2R5Um93cywgZnVuY3Rpb24gKGFjYywgcm93KSB7XG4gICAgICAgIHZhciByb3dLZXkgPSByb3cuZ2V0QXR0cmlidXRlKCdkYXRhLXJvdy1rZXknKTtcbiAgICAgICAgdmFyIGhlaWdodCA9IHJvdy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5oZWlnaHQgfHwgc3RhdGUuZml4ZWRDb2x1bW5zQm9keVJvd3NIZWlnaHRbcm93S2V5XSB8fCAnYXV0byc7XG4gICAgICAgIGFjY1tyb3dLZXldID0gaGVpZ2h0O1xuICAgICAgICByZXR1cm4gYWNjO1xuICAgICAgfSwge30pO1xuXG4gICAgICBpZiAoc2hhbGxvd2VxdWFsXzEuZGVmYXVsdChzdGF0ZS5maXhlZENvbHVtbnNIZWFkUm93c0hlaWdodCwgZml4ZWRDb2x1bW5zSGVhZFJvd3NIZWlnaHQpICYmIHNoYWxsb3dlcXVhbF8xLmRlZmF1bHQoc3RhdGUuZml4ZWRDb2x1bW5zQm9keVJvd3NIZWlnaHQsIGZpeGVkQ29sdW1uc0JvZHlSb3dzSGVpZ2h0KSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgZml4ZWRDb2x1bW5zSGVhZFJvd3NIZWlnaHQ6IGZpeGVkQ29sdW1uc0hlYWRSb3dzSGVpZ2h0LFxuICAgICAgICBmaXhlZENvbHVtbnNCb2R5Um93c0hlaWdodDogZml4ZWRDb2x1bW5zQm9keVJvd3NIZWlnaHRcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5oYW5kbGVCb2R5U2Nyb2xsTGVmdCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAvLyBGaXggaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvNzYzNVxuICAgICAgaWYgKGUuY3VycmVudFRhcmdldCAhPT0gZS50YXJnZXQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgdGFyZ2V0ID0gZS50YXJnZXQ7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMkc2Nyb2xsID0gX3RoaXMucHJvcHMuc2Nyb2xsLFxuICAgICAgICAgIHNjcm9sbCA9IF90aGlzJHByb3BzJHNjcm9sbCA9PT0gdm9pZCAwID8ge30gOiBfdGhpcyRwcm9wcyRzY3JvbGw7XG5cbiAgICAgIHZhciBfYXNzZXJ0VGhpc0luaXRpYWxpemUgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSxcbiAgICAgICAgICBoZWFkVGFibGUgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUuaGVhZFRhYmxlLFxuICAgICAgICAgIGJvZHlUYWJsZSA9IF9hc3NlcnRUaGlzSW5pdGlhbGl6ZS5ib2R5VGFibGU7XG5cbiAgICAgIGlmICh0YXJnZXQuc2Nyb2xsTGVmdCAhPT0gX3RoaXMubGFzdFNjcm9sbExlZnQgJiYgc2Nyb2xsLngpIHtcbiAgICAgICAgaWYgKHRhcmdldCA9PT0gYm9keVRhYmxlICYmIGhlYWRUYWJsZSkge1xuICAgICAgICAgIGhlYWRUYWJsZS5zY3JvbGxMZWZ0ID0gdGFyZ2V0LnNjcm9sbExlZnQ7XG4gICAgICAgIH0gZWxzZSBpZiAodGFyZ2V0ID09PSBoZWFkVGFibGUgJiYgYm9keVRhYmxlKSB7XG4gICAgICAgICAgYm9keVRhYmxlLnNjcm9sbExlZnQgPSB0YXJnZXQuc2Nyb2xsTGVmdDtcbiAgICAgICAgfVxuXG4gICAgICAgIF90aGlzLnNldFNjcm9sbFBvc2l0aW9uQ2xhc3NOYW1lKCk7XG4gICAgICB9IC8vIFJlbWVtYmVyIGxhc3Qgc2Nyb2xsTGVmdCBmb3Igc2Nyb2xsIGRpcmVjdGlvbiBkZXRlY3RpbmcuXG5cblxuICAgICAgX3RoaXMubGFzdFNjcm9sbExlZnQgPSB0YXJnZXQuc2Nyb2xsTGVmdDtcbiAgICB9O1xuXG4gICAgX3RoaXMuaGFuZGxlQm9keVNjcm9sbFRvcCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgdGFyZ2V0ID0gZS50YXJnZXQ7IC8vIEZpeCBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy85MDMzXG5cbiAgICAgIGlmIChlLmN1cnJlbnRUYXJnZXQgIT09IHRhcmdldCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBfdGhpcyRwcm9wcyRzY3JvbGwyID0gX3RoaXMucHJvcHMuc2Nyb2xsLFxuICAgICAgICAgIHNjcm9sbCA9IF90aGlzJHByb3BzJHNjcm9sbDIgPT09IHZvaWQgMCA/IHt9IDogX3RoaXMkcHJvcHMkc2Nyb2xsMjtcblxuICAgICAgdmFyIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZTIgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSxcbiAgICAgICAgICBoZWFkVGFibGUgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUyLmhlYWRUYWJsZSxcbiAgICAgICAgICBib2R5VGFibGUgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUyLmJvZHlUYWJsZSxcbiAgICAgICAgICBmaXhlZENvbHVtbnNCb2R5TGVmdCA9IF9hc3NlcnRUaGlzSW5pdGlhbGl6ZTIuZml4ZWRDb2x1bW5zQm9keUxlZnQsXG4gICAgICAgICAgZml4ZWRDb2x1bW5zQm9keVJpZ2h0ID0gX2Fzc2VydFRoaXNJbml0aWFsaXplMi5maXhlZENvbHVtbnNCb2R5UmlnaHQ7XG5cbiAgICAgIGlmICh0YXJnZXQuc2Nyb2xsVG9wICE9PSBfdGhpcy5sYXN0U2Nyb2xsVG9wICYmIHNjcm9sbC55ICYmIHRhcmdldCAhPT0gaGVhZFRhYmxlKSB7XG4gICAgICAgIHZhciBzY3JvbGxUb3AgPSB0YXJnZXQuc2Nyb2xsVG9wO1xuXG4gICAgICAgIGlmIChmaXhlZENvbHVtbnNCb2R5TGVmdCAmJiB0YXJnZXQgIT09IGZpeGVkQ29sdW1uc0JvZHlMZWZ0KSB7XG4gICAgICAgICAgZml4ZWRDb2x1bW5zQm9keUxlZnQuc2Nyb2xsVG9wID0gc2Nyb2xsVG9wO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGZpeGVkQ29sdW1uc0JvZHlSaWdodCAmJiB0YXJnZXQgIT09IGZpeGVkQ29sdW1uc0JvZHlSaWdodCkge1xuICAgICAgICAgIGZpeGVkQ29sdW1uc0JvZHlSaWdodC5zY3JvbGxUb3AgPSBzY3JvbGxUb3A7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoYm9keVRhYmxlICYmIHRhcmdldCAhPT0gYm9keVRhYmxlKSB7XG4gICAgICAgICAgYm9keVRhYmxlLnNjcm9sbFRvcCA9IHNjcm9sbFRvcDtcbiAgICAgICAgfVxuICAgICAgfSAvLyBSZW1lbWJlciBsYXN0IHNjcm9sbFRvcCBmb3Igc2Nyb2xsIGRpcmVjdGlvbiBkZXRlY3RpbmcuXG5cblxuICAgICAgX3RoaXMubGFzdFNjcm9sbFRvcCA9IHRhcmdldC5zY3JvbGxUb3A7XG4gICAgfTtcblxuICAgIF90aGlzLmhhbmRsZUJvZHlTY3JvbGwgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgX3RoaXMuaGFuZGxlQm9keVNjcm9sbExlZnQoZSk7XG5cbiAgICAgIF90aGlzLmhhbmRsZUJvZHlTY3JvbGxUb3AoZSk7XG4gICAgfTtcblxuICAgIF90aGlzLmhhbmRsZVdoZWVsID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMkc2Nyb2xsMyA9IF90aGlzLnByb3BzLnNjcm9sbCxcbiAgICAgICAgICBzY3JvbGwgPSBfdGhpcyRwcm9wcyRzY3JvbGwzID09PSB2b2lkIDAgPyB7fSA6IF90aGlzJHByb3BzJHNjcm9sbDM7XG5cbiAgICAgIGlmICh3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvVHJpZGVudFxcLzdcXC4vKSAmJiBzY3JvbGwueSkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB2YXIgd2QgPSBldmVudC5kZWx0YVk7XG4gICAgICAgIHZhciB0YXJnZXQgPSBldmVudC50YXJnZXQ7XG5cbiAgICAgICAgdmFyIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZTMgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSxcbiAgICAgICAgICAgIGJvZHlUYWJsZSA9IF9hc3NlcnRUaGlzSW5pdGlhbGl6ZTMuYm9keVRhYmxlLFxuICAgICAgICAgICAgZml4ZWRDb2x1bW5zQm9keUxlZnQgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUzLmZpeGVkQ29sdW1uc0JvZHlMZWZ0LFxuICAgICAgICAgICAgZml4ZWRDb2x1bW5zQm9keVJpZ2h0ID0gX2Fzc2VydFRoaXNJbml0aWFsaXplMy5maXhlZENvbHVtbnNCb2R5UmlnaHQ7XG5cbiAgICAgICAgdmFyIHNjcm9sbFRvcCA9IDA7XG5cbiAgICAgICAgaWYgKF90aGlzLmxhc3RTY3JvbGxUb3ApIHtcbiAgICAgICAgICBzY3JvbGxUb3AgPSBfdGhpcy5sYXN0U2Nyb2xsVG9wICsgd2Q7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc2Nyb2xsVG9wID0gd2Q7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZml4ZWRDb2x1bW5zQm9keUxlZnQgJiYgdGFyZ2V0ICE9PSBmaXhlZENvbHVtbnNCb2R5TGVmdCkge1xuICAgICAgICAgIGZpeGVkQ29sdW1uc0JvZHlMZWZ0LnNjcm9sbFRvcCA9IHNjcm9sbFRvcDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChmaXhlZENvbHVtbnNCb2R5UmlnaHQgJiYgdGFyZ2V0ICE9PSBmaXhlZENvbHVtbnNCb2R5UmlnaHQpIHtcbiAgICAgICAgICBmaXhlZENvbHVtbnNCb2R5UmlnaHQuc2Nyb2xsVG9wID0gc2Nyb2xsVG9wO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGJvZHlUYWJsZSAmJiB0YXJnZXQgIT09IGJvZHlUYWJsZSkge1xuICAgICAgICAgIGJvZHlUYWJsZS5zY3JvbGxUb3AgPSBzY3JvbGxUb3A7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMuc2F2ZVJlZiA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgX3RoaXNbbmFtZV0gPSBub2RlO1xuICAgICAgfTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc2F2ZVRhYmxlTm9kZVJlZiA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICBfdGhpcy50YWJsZU5vZGUgPSBub2RlO1xuICAgIH07XG5cbiAgICBbJ29uUm93Q2xpY2snLCAnb25Sb3dEb3VibGVDbGljaycsICdvblJvd0NvbnRleHRNZW51JywgJ29uUm93TW91c2VFbnRlcicsICdvblJvd01vdXNlTGVhdmUnXS5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICB3YXJuaW5nXzEuZGVmYXVsdChwcm9wc1tuYW1lXSA9PT0gdW5kZWZpbmVkLCBcIlwiLmNvbmNhdChuYW1lLCBcIiBpcyBkZXByZWNhdGVkLCBwbGVhc2UgdXNlIG9uUm93IGluc3RlYWQuXCIpKTtcbiAgICB9KTtcbiAgICB3YXJuaW5nXzEuZGVmYXVsdChwcm9wcy5nZXRCb2R5V3JhcHBlciA9PT0gdW5kZWZpbmVkLCAnZ2V0Qm9keVdyYXBwZXIgaXMgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSBjdXN0b20gY29tcG9uZW50cyBpbnN0ZWFkLicpO1xuICAgIF90aGlzLmNvbHVtbk1hbmFnZXIgPSBuZXcgQ29sdW1uTWFuYWdlcl8xLmRlZmF1bHQocHJvcHMuY29sdW1ucywgcHJvcHMuY2hpbGRyZW4pO1xuICAgIF90aGlzLnN0b3JlID0gbWluaV9zdG9yZV8xLmNyZWF0ZSh7XG4gICAgICBjdXJyZW50SG92ZXJLZXk6IG51bGwsXG4gICAgICBmaXhlZENvbHVtbnNIZWFkUm93c0hlaWdodDogW10sXG4gICAgICBmaXhlZENvbHVtbnNCb2R5Um93c0hlaWdodDoge31cbiAgICB9KTtcblxuICAgIF90aGlzLnNldFNjcm9sbFBvc2l0aW9uKCdsZWZ0Jyk7XG5cbiAgICBfdGhpcy5kZWJvdW5jZWRXaW5kb3dSZXNpemUgPSB1dGlsc18xLmRlYm91bmNlKF90aGlzLmhhbmRsZVdpbmRvd1Jlc2l6ZSwgMTUwKTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoVGFibGUsIFt7XG4gICAga2V5OiBcImdldENoaWxkQ29udGV4dFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICB0YWJsZToge1xuICAgICAgICAgIHByb3BzOiB0aGlzLnByb3BzLFxuICAgICAgICAgIGNvbHVtbk1hbmFnZXI6IHRoaXMuY29sdW1uTWFuYWdlcixcbiAgICAgICAgICBzYXZlUmVmOiB0aGlzLnNhdmVSZWYsXG4gICAgICAgICAgY29tcG9uZW50czogbWVyZ2VfMS5kZWZhdWx0KHtcbiAgICAgICAgICAgIHRhYmxlOiAndGFibGUnLFxuICAgICAgICAgICAgaGVhZGVyOiB7XG4gICAgICAgICAgICAgIHdyYXBwZXI6ICd0aGVhZCcsXG4gICAgICAgICAgICAgIHJvdzogJ3RyJyxcbiAgICAgICAgICAgICAgY2VsbDogJ3RoJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJvZHk6IHtcbiAgICAgICAgICAgICAgd3JhcHBlcjogJ3Rib2R5JyxcbiAgICAgICAgICAgICAgcm93OiAndHInLFxuICAgICAgICAgICAgICBjZWxsOiAndGQnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSwgdGhpcy5wcm9wcy5jb21wb25lbnRzKVxuICAgICAgICB9XG4gICAgICB9O1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnREaWRNb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLmNvbHVtbk1hbmFnZXIuaXNBbnlDb2x1bW5zRml4ZWQoKSkge1xuICAgICAgICB0aGlzLmhhbmRsZVdpbmRvd1Jlc2l6ZSgpO1xuICAgICAgICB0aGlzLnJlc2l6ZUV2ZW50ID0gYWRkRXZlbnRMaXN0ZW5lcl8xLmRlZmF1bHQod2luZG93LCAncmVzaXplJywgdGhpcy5kZWJvdW5jZWRXaW5kb3dSZXNpemUpO1xuICAgICAgfSAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMTYzNVxuXG5cbiAgICAgIGlmICh0aGlzLmhlYWRUYWJsZSkge1xuICAgICAgICB0aGlzLmhlYWRUYWJsZS5zY3JvbGxMZWZ0ID0gMDtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuYm9keVRhYmxlKSB7XG4gICAgICAgIHRoaXMuYm9keVRhYmxlLnNjcm9sbExlZnQgPSAwO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnREaWRVcGRhdGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgaWYgKHRoaXMuY29sdW1uTWFuYWdlci5pc0FueUNvbHVtbnNGaXhlZCgpKSB7XG4gICAgICAgIHRoaXMuaGFuZGxlV2luZG93UmVzaXplKCk7XG5cbiAgICAgICAgaWYgKCF0aGlzLnJlc2l6ZUV2ZW50KSB7XG4gICAgICAgICAgdGhpcy5yZXNpemVFdmVudCA9IGFkZEV2ZW50TGlzdGVuZXJfMS5kZWZhdWx0KHdpbmRvdywgJ3Jlc2l6ZScsIHRoaXMuZGVib3VuY2VkV2luZG93UmVzaXplKTtcbiAgICAgICAgfVxuICAgICAgfSAvLyB3aGVuIHRhYmxlIGNoYW5nZXMgdG8gZW1wdHksIHJlc2V0IHNjcm9sbExlZnRcblxuXG4gICAgICBpZiAocHJldlByb3BzLmRhdGEubGVuZ3RoID4gMCAmJiB0aGlzLnByb3BzLmRhdGEubGVuZ3RoID09PSAwICYmIHRoaXMuaGFzU2Nyb2xsWCgpKSB7XG4gICAgICAgIHRoaXMucmVzZXRTY3JvbGxYKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImNvbXBvbmVudFdpbGxVbm1vdW50XCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgaWYgKHRoaXMucmVzaXplRXZlbnQpIHtcbiAgICAgICAgdGhpcy5yZXNpemVFdmVudC5yZW1vdmUoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuZGVib3VuY2VkV2luZG93UmVzaXplKSB7XG4gICAgICAgIHRoaXMuZGVib3VuY2VkV2luZG93UmVzaXplLmNhbmNlbCgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJzZXRTY3JvbGxQb3NpdGlvblwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRTY3JvbGxQb3NpdGlvbihwb3NpdGlvbikge1xuICAgICAgdGhpcy5zY3JvbGxQb3NpdGlvbiA9IHBvc2l0aW9uO1xuXG4gICAgICBpZiAodGhpcy50YWJsZU5vZGUpIHtcbiAgICAgICAgdmFyIHByZWZpeENscyA9IHRoaXMucHJvcHMucHJlZml4Q2xzO1xuXG4gICAgICAgIGlmIChwb3NpdGlvbiA9PT0gJ2JvdGgnKSB7XG4gICAgICAgICAgY29tcG9uZW50X2NsYXNzZXNfMS5kZWZhdWx0KHRoaXMudGFibGVOb2RlKS5yZW1vdmUobmV3IFJlZ0V4cChcIl5cIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zY3JvbGwtcG9zaXRpb24tLiskXCIpKSkuYWRkKFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2Nyb2xsLXBvc2l0aW9uLWxlZnRcIikpLmFkZChcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNjcm9sbC1wb3NpdGlvbi1yaWdodFwiKSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29tcG9uZW50X2NsYXNzZXNfMS5kZWZhdWx0KHRoaXMudGFibGVOb2RlKS5yZW1vdmUobmV3IFJlZ0V4cChcIl5cIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zY3JvbGwtcG9zaXRpb24tLiskXCIpKSkuYWRkKFwiXCIuY29uY2F0KHByZWZpeENscywgXCItc2Nyb2xsLXBvc2l0aW9uLVwiKS5jb25jYXQocG9zaXRpb24pKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJzZXRTY3JvbGxQb3NpdGlvbkNsYXNzTmFtZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRTY3JvbGxQb3NpdGlvbkNsYXNzTmFtZSgpIHtcbiAgICAgIHZhciBub2RlID0gdGhpcy5ib2R5VGFibGU7XG4gICAgICB2YXIgc2Nyb2xsVG9MZWZ0ID0gbm9kZS5zY3JvbGxMZWZ0ID09PSAwO1xuICAgICAgdmFyIHNjcm9sbFRvUmlnaHQgPSBub2RlLnNjcm9sbExlZnQgKyAxID49IG5vZGUuY2hpbGRyZW5bMF0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkud2lkdGggLSBub2RlLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLndpZHRoO1xuXG4gICAgICBpZiAoc2Nyb2xsVG9MZWZ0ICYmIHNjcm9sbFRvUmlnaHQpIHtcbiAgICAgICAgdGhpcy5zZXRTY3JvbGxQb3NpdGlvbignYm90aCcpO1xuICAgICAgfSBlbHNlIGlmIChzY3JvbGxUb0xlZnQpIHtcbiAgICAgICAgdGhpcy5zZXRTY3JvbGxQb3NpdGlvbignbGVmdCcpO1xuICAgICAgfSBlbHNlIGlmIChzY3JvbGxUb1JpZ2h0KSB7XG4gICAgICAgIHRoaXMuc2V0U2Nyb2xsUG9zaXRpb24oJ3JpZ2h0Jyk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMuc2Nyb2xsUG9zaXRpb24gIT09ICdtaWRkbGUnKSB7XG4gICAgICAgIHRoaXMuc2V0U2Nyb2xsUG9zaXRpb24oJ21pZGRsZScpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJpc1RhYmxlTGF5b3V0Rml4ZWRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNUYWJsZUxheW91dEZpeGVkKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICB0YWJsZUxheW91dCA9IF90aGlzJHByb3BzLnRhYmxlTGF5b3V0LFxuICAgICAgICAgIF90aGlzJHByb3BzJGNvbHVtbnMgPSBfdGhpcyRwcm9wcy5jb2x1bW5zLFxuICAgICAgICAgIGNvbHVtbnMgPSBfdGhpcyRwcm9wcyRjb2x1bW5zID09PSB2b2lkIDAgPyBbXSA6IF90aGlzJHByb3BzJGNvbHVtbnMsXG4gICAgICAgICAgdXNlRml4ZWRIZWFkZXIgPSBfdGhpcyRwcm9wcy51c2VGaXhlZEhlYWRlcixcbiAgICAgICAgICBfdGhpcyRwcm9wcyRzY3JvbGw0ID0gX3RoaXMkcHJvcHMuc2Nyb2xsLFxuICAgICAgICAgIHNjcm9sbCA9IF90aGlzJHByb3BzJHNjcm9sbDQgPT09IHZvaWQgMCA/IHt9IDogX3RoaXMkcHJvcHMkc2Nyb2xsNDtcblxuICAgICAgaWYgKHR5cGVvZiB0YWJsZUxheW91dCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgcmV0dXJuIHRhYmxlTGF5b3V0ID09PSAnZml4ZWQnO1xuICAgICAgfSAvLyBpZiBvbmUgY29sdW1uIGlzIGVsbGlwc2lzLCB1c2UgZml4ZWQgdGFibGUgbGF5b3V0IHRvIGZpeCBhbGlnbiBpc3N1ZVxuXG5cbiAgICAgIGlmIChjb2x1bW5zLnNvbWUoZnVuY3Rpb24gKF9yZWYpIHtcbiAgICAgICAgdmFyIGVsbGlwc2lzID0gX3JlZi5lbGxpcHNpcztcbiAgICAgICAgcmV0dXJuICEhZWxsaXBzaXM7XG4gICAgICB9KSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0gLy8gaWYgaGVhZGVyIGZpeGVkLCB1c2UgZml4ZWQgdGFibGUgbGF5b3V0IHRvIGZpeCBhbGlnbiBpc3N1ZVxuXG5cbiAgICAgIGlmICh1c2VGaXhlZEhlYWRlciB8fCBzY3JvbGwueSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0gLy8gaWYgc2Nyb2xsLnggaXMgbnVtYmVyL3B4LyUgd2lkdGggdmFsdWUsIHdlIHNob3VsZCBmaXhlZCB0YWJsZSBsYXlvdXRcbiAgICAgIC8vIHRvIGF2b2lkIGxvbmcgd29yZCBsYXlvdXQgYnJva2VuIGlzc3VlXG5cblxuICAgICAgaWYgKHNjcm9sbC54ICYmIHNjcm9sbC54ICE9PSB0cnVlICYmIHNjcm9sbC54ICE9PSAnbWF4LWNvbnRlbnQnKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlc2V0U2Nyb2xsWFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNldFNjcm9sbFgoKSB7XG4gICAgICBpZiAodGhpcy5oZWFkVGFibGUpIHtcbiAgICAgICAgdGhpcy5oZWFkVGFibGUuc2Nyb2xsTGVmdCA9IDA7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmJvZHlUYWJsZSkge1xuICAgICAgICB0aGlzLmJvZHlUYWJsZS5zY3JvbGxMZWZ0ID0gMDtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiaGFzU2Nyb2xsWFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYXNTY3JvbGxYKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzJHNjcm9sbDUgPSB0aGlzLnByb3BzLnNjcm9sbCxcbiAgICAgICAgICBzY3JvbGwgPSBfdGhpcyRwcm9wcyRzY3JvbGw1ID09PSB2b2lkIDAgPyB7fSA6IF90aGlzJHByb3BzJHNjcm9sbDU7XG4gICAgICByZXR1cm4gJ3gnIGluIHNjcm9sbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyTWFpblRhYmxlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlck1haW5UYWJsZSgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIHNjcm9sbCA9IF90aGlzJHByb3BzMi5zY3JvbGwsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHMyLnByZWZpeENscztcbiAgICAgIHZhciBpc0FueUNvbHVtbnNGaXhlZCA9IHRoaXMuY29sdW1uTWFuYWdlci5pc0FueUNvbHVtbnNGaXhlZCgpO1xuICAgICAgdmFyIHNjcm9sbGFibGUgPSBpc0FueUNvbHVtbnNGaXhlZCB8fCBzY3JvbGwueCB8fCBzY3JvbGwueTtcbiAgICAgIHZhciB0YWJsZSA9IFt0aGlzLnJlbmRlclRhYmxlKHtcbiAgICAgICAgY29sdW1uczogdGhpcy5jb2x1bW5NYW5hZ2VyLmdyb3VwZWRDb2x1bW5zKCksXG4gICAgICAgIGlzQW55Q29sdW1uc0ZpeGVkOiBpc0FueUNvbHVtbnNGaXhlZFxuICAgICAgfSksIHRoaXMucmVuZGVyRW1wdHlUZXh0KCksIHRoaXMucmVuZGVyRm9vdGVyKCldO1xuICAgICAgcmV0dXJuIHNjcm9sbGFibGUgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNjcm9sbFwiKVxuICAgICAgfSwgdGFibGUpIDogdGFibGU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlckxlZnRGaXhlZFRhYmxlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckxlZnRGaXhlZFRhYmxlKCkge1xuICAgICAgdmFyIHByZWZpeENscyA9IHRoaXMucHJvcHMucHJlZml4Q2xzO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge1xuICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItZml4ZWQtbGVmdFwiKVxuICAgICAgfSwgdGhpcy5yZW5kZXJUYWJsZSh7XG4gICAgICAgIGNvbHVtbnM6IHRoaXMuY29sdW1uTWFuYWdlci5sZWZ0Q29sdW1ucygpLFxuICAgICAgICBmaXhlZDogJ2xlZnQnXG4gICAgICB9KSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlclJpZ2h0Rml4ZWRUYWJsZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXJSaWdodEZpeGVkVGFibGUoKSB7XG4gICAgICB2YXIgcHJlZml4Q2xzID0gdGhpcy5wcm9wcy5wcmVmaXhDbHM7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1maXhlZC1yaWdodFwiKVxuICAgICAgfSwgdGhpcy5yZW5kZXJUYWJsZSh7XG4gICAgICAgIGNvbHVtbnM6IHRoaXMuY29sdW1uTWFuYWdlci5yaWdodENvbHVtbnMoKSxcbiAgICAgICAgZml4ZWQ6ICdyaWdodCdcbiAgICAgIH0pKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyVGFibGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyVGFibGUob3B0aW9ucykge1xuICAgICAgdmFyIGNvbHVtbnMgPSBvcHRpb25zLmNvbHVtbnMsXG4gICAgICAgICAgZml4ZWQgPSBvcHRpb25zLmZpeGVkLFxuICAgICAgICAgIGlzQW55Q29sdW1uc0ZpeGVkID0gb3B0aW9ucy5pc0FueUNvbHVtbnNGaXhlZDtcbiAgICAgIHZhciBfdGhpcyRwcm9wczMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzMy5wcmVmaXhDbHMsXG4gICAgICAgICAgX3RoaXMkcHJvcHMzJHNjcm9sbCA9IF90aGlzJHByb3BzMy5zY3JvbGwsXG4gICAgICAgICAgc2Nyb2xsID0gX3RoaXMkcHJvcHMzJHNjcm9sbCA9PT0gdm9pZCAwID8ge30gOiBfdGhpcyRwcm9wczMkc2Nyb2xsO1xuICAgICAgdmFyIHRhYmxlQ2xhc3NOYW1lID0gc2Nyb2xsLnggfHwgZml4ZWQgPyBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWZpeGVkXCIpIDogJyc7XG4gICAgICB2YXIgaGVhZFRhYmxlID0gUmVhY3QuY3JlYXRlRWxlbWVudChIZWFkVGFibGVfMS5kZWZhdWx0LCB7XG4gICAgICAgIGtleTogXCJoZWFkXCIsXG4gICAgICAgIGNvbHVtbnM6IGNvbHVtbnMsXG4gICAgICAgIGZpeGVkOiBmaXhlZCxcbiAgICAgICAgdGFibGVDbGFzc05hbWU6IHRhYmxlQ2xhc3NOYW1lLFxuICAgICAgICBoYW5kbGVCb2R5U2Nyb2xsTGVmdDogdGhpcy5oYW5kbGVCb2R5U2Nyb2xsTGVmdCxcbiAgICAgICAgZXhwYW5kZXI6IHRoaXMuZXhwYW5kZXJcbiAgICAgIH0pO1xuICAgICAgdmFyIGJvZHlUYWJsZSA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoQm9keVRhYmxlXzEuZGVmYXVsdCwge1xuICAgICAgICBrZXk6IFwiYm9keVwiLFxuICAgICAgICBjb2x1bW5zOiBjb2x1bW5zLFxuICAgICAgICBmaXhlZDogZml4ZWQsXG4gICAgICAgIHRhYmxlQ2xhc3NOYW1lOiB0YWJsZUNsYXNzTmFtZSxcbiAgICAgICAgZ2V0Um93S2V5OiB0aGlzLmdldFJvd0tleSxcbiAgICAgICAgaGFuZGxlV2hlZWw6IHRoaXMuaGFuZGxlV2hlZWwsXG4gICAgICAgIGhhbmRsZUJvZHlTY3JvbGw6IHRoaXMuaGFuZGxlQm9keVNjcm9sbCxcbiAgICAgICAgZXhwYW5kZXI6IHRoaXMuZXhwYW5kZXIsXG4gICAgICAgIGlzQW55Q29sdW1uc0ZpeGVkOiBpc0FueUNvbHVtbnNGaXhlZFxuICAgICAgfSk7XG4gICAgICByZXR1cm4gW2hlYWRUYWJsZSwgYm9keVRhYmxlXTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyVGl0bGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyVGl0bGUoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM0ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aXRsZSA9IF90aGlzJHByb3BzNC50aXRsZSxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfdGhpcyRwcm9wczQucHJlZml4Q2xzO1xuICAgICAgcmV0dXJuIHRpdGxlID8gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10aXRsZVwiKSxcbiAgICAgICAga2V5OiBcInRpdGxlXCJcbiAgICAgIH0sIHRpdGxlKHRoaXMucHJvcHMuZGF0YSkpIDogbnVsbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwicmVuZGVyRm9vdGVyXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckZvb3RlcigpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczUgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGZvb3RlciA9IF90aGlzJHByb3BzNS5mb290ZXIsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHM1LnByZWZpeENscztcbiAgICAgIHJldHVybiBmb290ZXIgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWZvb3RlclwiKSxcbiAgICAgICAga2V5OiBcImZvb3RlclwiXG4gICAgICB9LCBmb290ZXIodGhpcy5wcm9wcy5kYXRhKSkgOiBudWxsO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJyZW5kZXJFbXB0eVRleHRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyRW1wdHlUZXh0KCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgZW1wdHlUZXh0ID0gX3RoaXMkcHJvcHM2LmVtcHR5VGV4dCxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfdGhpcyRwcm9wczYucHJlZml4Q2xzLFxuICAgICAgICAgIGRhdGEgPSBfdGhpcyRwcm9wczYuZGF0YTtcblxuICAgICAgaWYgKGRhdGEubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgZW1wdHlDbGFzc05hbWUgPSBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXBsYWNlaG9sZGVyXCIpO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge1xuICAgICAgICBjbGFzc05hbWU6IGVtcHR5Q2xhc3NOYW1lLFxuICAgICAgICBrZXk6IFwiZW1wdHlUZXh0XCJcbiAgICAgIH0sIHR5cGVvZiBlbXB0eVRleHQgPT09ICdmdW5jdGlvbicgPyBlbXB0eVRleHQoKSA6IGVtcHR5VGV4dCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlclwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzbmFtZXNfMSRkZWZhdWx0LFxuICAgICAgICAgIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgICB2YXIgcHJlZml4Q2xzID0gcHJvcHMucHJlZml4Q2xzO1xuXG4gICAgICBpZiAodGhpcy5zdGF0ZS5jb2x1bW5zKSB7XG4gICAgICAgIHRoaXMuY29sdW1uTWFuYWdlci5yZXNldChwcm9wcy5jb2x1bW5zKTtcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5jaGlsZHJlbikge1xuICAgICAgICB0aGlzLmNvbHVtbk1hbmFnZXIucmVzZXQobnVsbCwgcHJvcHMuY2hpbGRyZW4pO1xuICAgICAgfVxuXG4gICAgICB2YXIgdGFibGVDbGFzc05hbWUgPSBjbGFzc25hbWVzXzEuZGVmYXVsdChwcm9wcy5wcmVmaXhDbHMsIHByb3BzLmNsYXNzTmFtZSwgKF9jbGFzc25hbWVzXzEkZGVmYXVsdCA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXNfMSRkZWZhdWx0LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWZpeGVkLWhlYWRlclwiKSwgcHJvcHMudXNlRml4ZWRIZWFkZXIgfHwgcHJvcHMuc2Nyb2xsICYmIHByb3BzLnNjcm9sbC55KSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzXzEkZGVmYXVsdCwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zY3JvbGwtcG9zaXRpb24tbGVmdCBcIikuY29uY2F0KHByZWZpeENscywgXCItc2Nyb2xsLXBvc2l0aW9uLXJpZ2h0XCIpLCB0aGlzLnNjcm9sbFBvc2l0aW9uID09PSAnYm90aCcpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXNfMSRkZWZhdWx0LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXNjcm9sbC1wb3NpdGlvbi1cIikuY29uY2F0KHRoaXMuc2Nyb2xsUG9zaXRpb24pLCB0aGlzLnNjcm9sbFBvc2l0aW9uICE9PSAnYm90aCcpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXNfMSRkZWZhdWx0LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWxheW91dC1maXhlZFwiKSwgdGhpcy5pc1RhYmxlTGF5b3V0Rml4ZWQoKSksIF9jbGFzc25hbWVzXzEkZGVmYXVsdCkpO1xuICAgICAgdmFyIGhhc0xlZnRGaXhlZCA9IHRoaXMuY29sdW1uTWFuYWdlci5pc0FueUNvbHVtbnNMZWZ0Rml4ZWQoKTtcbiAgICAgIHZhciBoYXNSaWdodEZpeGVkID0gdGhpcy5jb2x1bW5NYW5hZ2VyLmlzQW55Q29sdW1uc1JpZ2h0Rml4ZWQoKTtcbiAgICAgIHZhciBkYXRhQW5kQXJpYVByb3BzID0gdXRpbHNfMS5nZXREYXRhQW5kQXJpYVByb3BzKHByb3BzKTtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KG1pbmlfc3RvcmVfMS5Qcm92aWRlciwge1xuICAgICAgICBzdG9yZTogdGhpcy5zdG9yZVxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChFeHBhbmRhYmxlVGFibGVfMS5kZWZhdWx0LCBPYmplY3QuYXNzaWduKHt9LCBwcm9wcywge1xuICAgICAgICBjb2x1bW5NYW5hZ2VyOiB0aGlzLmNvbHVtbk1hbmFnZXIsXG4gICAgICAgIGdldFJvd0tleTogdGhpcy5nZXRSb3dLZXlcbiAgICAgIH0pLCBmdW5jdGlvbiAoZXhwYW5kZXIpIHtcbiAgICAgICAgX3RoaXMyLmV4cGFuZGVyID0gZXhwYW5kZXI7XG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIE9iamVjdC5hc3NpZ24oe1xuICAgICAgICAgIHJlZjogX3RoaXMyLnNhdmVUYWJsZU5vZGVSZWYsXG4gICAgICAgICAgY2xhc3NOYW1lOiB0YWJsZUNsYXNzTmFtZSxcbiAgICAgICAgICBzdHlsZTogcHJvcHMuc3R5bGUsXG4gICAgICAgICAgaWQ6IHByb3BzLmlkXG4gICAgICAgIH0sIGRhdGFBbmRBcmlhUHJvcHMpLCBfdGhpczIucmVuZGVyVGl0bGUoKSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWNvbnRlbnRcIilcbiAgICAgICAgfSwgX3RoaXMyLnJlbmRlck1haW5UYWJsZSgpLCBoYXNMZWZ0Rml4ZWQgJiYgX3RoaXMyLnJlbmRlckxlZnRGaXhlZFRhYmxlKCksIGhhc1JpZ2h0Rml4ZWQgJiYgX3RoaXMyLnJlbmRlclJpZ2h0Rml4ZWRUYWJsZSgpKSk7XG4gICAgICB9KSk7XG4gICAgfVxuICB9XSwgW3tcbiAgICBrZXk6IFwiZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgaWYgKG5leHRQcm9wcy5jb2x1bW5zICYmIG5leHRQcm9wcy5jb2x1bW5zICE9PSBwcmV2U3RhdGUuY29sdW1ucykge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNvbHVtbnM6IG5leHRQcm9wcy5jb2x1bW5zLFxuICAgICAgICAgIGNoaWxkcmVuOiBudWxsXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIGlmIChuZXh0UHJvcHMuY2hpbGRyZW4gIT09IHByZXZTdGF0ZS5jaGlsZHJlbikge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNvbHVtbnM6IG51bGwsXG4gICAgICAgICAgY2hpbGRyZW46IG5leHRQcm9wcy5jaGlsZHJlblxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gVGFibGU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cblRhYmxlLmNoaWxkQ29udGV4dFR5cGVzID0ge1xuICB0YWJsZTogUHJvcFR5cGVzLmFueSxcbiAgY29tcG9uZW50czogUHJvcFR5cGVzLmFueVxufTtcblRhYmxlLkNvbHVtbiA9IENvbHVtbl8xLmRlZmF1bHQ7XG5UYWJsZS5Db2x1bW5Hcm91cCA9IENvbHVtbkdyb3VwXzEuZGVmYXVsdDtcblRhYmxlLmRlZmF1bHRQcm9wcyA9IHtcbiAgZGF0YTogW10sXG4gIHVzZUZpeGVkSGVhZGVyOiBmYWxzZSxcbiAgcm93S2V5OiAna2V5JyxcbiAgcm93Q2xhc3NOYW1lOiBmdW5jdGlvbiByb3dDbGFzc05hbWUoKSB7XG4gICAgcmV0dXJuICcnO1xuICB9LFxuICBvblJvdzogZnVuY3Rpb24gb25Sb3coKSB7fSxcbiAgb25IZWFkZXJSb3c6IGZ1bmN0aW9uIG9uSGVhZGVyUm93KCkge30sXG4gIHByZWZpeENsczogJ3JjLXRhYmxlJyxcbiAgYm9keVN0eWxlOiB7fSxcbiAgc3R5bGU6IHt9LFxuICBzaG93SGVhZGVyOiB0cnVlLFxuICBzY3JvbGw6IHt9LFxuICByb3dSZWY6IGZ1bmN0aW9uIHJvd1JlZigpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfSxcbiAgZW1wdHlUZXh0OiBmdW5jdGlvbiBlbXB0eVRleHQoKSB7XG4gICAgcmV0dXJuICdObyBEYXRhJztcbiAgfVxufTtcbnJlYWN0X2xpZmVjeWNsZXNfY29tcGF0XzEucG9seWZpbGwoVGFibGUpO1xuZXhwb3J0cy5kZWZhdWx0ID0gVGFibGU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBUEE7QUFKQTtBQURBO0FBb0JBO0FBdkJBO0FBeUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFrQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5DQTtBQXFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFmQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRkE7QUFJQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUZBO0FBSUE7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBL0JBO0FBaUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBREE7QUFHQTtBQUNBO0FBbkNBO0FBcUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQWxCQTtBQUNBO0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUFxQkE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/Table.js
