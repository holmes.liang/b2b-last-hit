var vec2 = __webpack_require__(/*! ./vector */ "./node_modules/zrender/lib/core/vector.js");

var matrix = __webpack_require__(/*! ./matrix */ "./node_modules/zrender/lib/core/matrix.js");
/**
 * @module echarts/core/BoundingRect
 */


var v2ApplyTransform = vec2.applyTransform;
var mathMin = Math.min;
var mathMax = Math.max;
/**
 * @alias module:echarts/core/BoundingRect
 */

function BoundingRect(x, y, width, height) {
  if (width < 0) {
    x = x + width;
    width = -width;
  }

  if (height < 0) {
    y = y + height;
    height = -height;
  }
  /**
   * @type {number}
   */


  this.x = x;
  /**
   * @type {number}
   */

  this.y = y;
  /**
   * @type {number}
   */

  this.width = width;
  /**
   * @type {number}
   */

  this.height = height;
}

BoundingRect.prototype = {
  constructor: BoundingRect,

  /**
   * @param {module:echarts/core/BoundingRect} other
   */
  union: function union(other) {
    var x = mathMin(other.x, this.x);
    var y = mathMin(other.y, this.y);
    this.width = mathMax(other.x + other.width, this.x + this.width) - x;
    this.height = mathMax(other.y + other.height, this.y + this.height) - y;
    this.x = x;
    this.y = y;
  },

  /**
   * @param {Array.<number>} m
   * @methods
   */
  applyTransform: function () {
    var lt = [];
    var rb = [];
    var lb = [];
    var rt = [];
    return function (m) {
      // In case usage like this
      // el.getBoundingRect().applyTransform(el.transform)
      // And element has no transform
      if (!m) {
        return;
      }

      lt[0] = lb[0] = this.x;
      lt[1] = rt[1] = this.y;
      rb[0] = rt[0] = this.x + this.width;
      rb[1] = lb[1] = this.y + this.height;
      v2ApplyTransform(lt, lt, m);
      v2ApplyTransform(rb, rb, m);
      v2ApplyTransform(lb, lb, m);
      v2ApplyTransform(rt, rt, m);
      this.x = mathMin(lt[0], rb[0], lb[0], rt[0]);
      this.y = mathMin(lt[1], rb[1], lb[1], rt[1]);
      var maxX = mathMax(lt[0], rb[0], lb[0], rt[0]);
      var maxY = mathMax(lt[1], rb[1], lb[1], rt[1]);
      this.width = maxX - this.x;
      this.height = maxY - this.y;
    };
  }(),

  /**
   * Calculate matrix of transforming from self to target rect
   * @param  {module:zrender/core/BoundingRect} b
   * @return {Array.<number>}
   */
  calculateTransform: function calculateTransform(b) {
    var a = this;
    var sx = b.width / a.width;
    var sy = b.height / a.height;
    var m = matrix.create(); // 矩阵右乘

    matrix.translate(m, m, [-a.x, -a.y]);
    matrix.scale(m, m, [sx, sy]);
    matrix.translate(m, m, [b.x, b.y]);
    return m;
  },

  /**
   * @param {(module:echarts/core/BoundingRect|Object)} b
   * @return {boolean}
   */
  intersect: function intersect(b) {
    if (!b) {
      return false;
    }

    if (!(b instanceof BoundingRect)) {
      // Normalize negative width/height.
      b = BoundingRect.create(b);
    }

    var a = this;
    var ax0 = a.x;
    var ax1 = a.x + a.width;
    var ay0 = a.y;
    var ay1 = a.y + a.height;
    var bx0 = b.x;
    var bx1 = b.x + b.width;
    var by0 = b.y;
    var by1 = b.y + b.height;
    return !(ax1 < bx0 || bx1 < ax0 || ay1 < by0 || by1 < ay0);
  },
  contain: function contain(x, y) {
    var rect = this;
    return x >= rect.x && x <= rect.x + rect.width && y >= rect.y && y <= rect.y + rect.height;
  },

  /**
   * @return {module:echarts/core/BoundingRect}
   */
  clone: function clone() {
    return new BoundingRect(this.x, this.y, this.width, this.height);
  },

  /**
   * Copy from another rect
   */
  copy: function copy(other) {
    this.x = other.x;
    this.y = other.y;
    this.width = other.width;
    this.height = other.height;
  },
  plain: function plain() {
    return {
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height
    };
  }
};
/**
 * @param {Object|module:zrender/core/BoundingRect} rect
 * @param {number} rect.x
 * @param {number} rect.y
 * @param {number} rect.width
 * @param {number} rect.height
 * @return {module:zrender/core/BoundingRect}
 */

BoundingRect.create = function (rect) {
  return new BoundingRect(rect.x, rect.y, rect.width, rect.height);
};

var _default = BoundingRect;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9Cb3VuZGluZ1JlY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL0JvdW5kaW5nUmVjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgdmVjMiA9IHJlcXVpcmUoXCIuL3ZlY3RvclwiKTtcblxudmFyIG1hdHJpeCA9IHJlcXVpcmUoXCIuL21hdHJpeFwiKTtcblxuLyoqXG4gKiBAbW9kdWxlIGVjaGFydHMvY29yZS9Cb3VuZGluZ1JlY3RcbiAqL1xudmFyIHYyQXBwbHlUcmFuc2Zvcm0gPSB2ZWMyLmFwcGx5VHJhbnNmb3JtO1xudmFyIG1hdGhNaW4gPSBNYXRoLm1pbjtcbnZhciBtYXRoTWF4ID0gTWF0aC5tYXg7XG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6ZWNoYXJ0cy9jb3JlL0JvdW5kaW5nUmVjdFxuICovXG5cbmZ1bmN0aW9uIEJvdW5kaW5nUmVjdCh4LCB5LCB3aWR0aCwgaGVpZ2h0KSB7XG4gIGlmICh3aWR0aCA8IDApIHtcbiAgICB4ID0geCArIHdpZHRoO1xuICAgIHdpZHRoID0gLXdpZHRoO1xuICB9XG5cbiAgaWYgKGhlaWdodCA8IDApIHtcbiAgICB5ID0geSArIGhlaWdodDtcbiAgICBoZWlnaHQgPSAtaGVpZ2h0O1xuICB9XG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cblxuXG4gIHRoaXMueCA9IHg7XG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cblxuICB0aGlzLnkgPSB5O1xuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG5cbiAgdGhpcy53aWR0aCA9IHdpZHRoO1xuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG5cbiAgdGhpcy5oZWlnaHQgPSBoZWlnaHQ7XG59XG5cbkJvdW5kaW5nUmVjdC5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBCb3VuZGluZ1JlY3QsXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvY29yZS9Cb3VuZGluZ1JlY3R9IG90aGVyXG4gICAqL1xuICB1bmlvbjogZnVuY3Rpb24gKG90aGVyKSB7XG4gICAgdmFyIHggPSBtYXRoTWluKG90aGVyLngsIHRoaXMueCk7XG4gICAgdmFyIHkgPSBtYXRoTWluKG90aGVyLnksIHRoaXMueSk7XG4gICAgdGhpcy53aWR0aCA9IG1hdGhNYXgob3RoZXIueCArIG90aGVyLndpZHRoLCB0aGlzLnggKyB0aGlzLndpZHRoKSAtIHg7XG4gICAgdGhpcy5oZWlnaHQgPSBtYXRoTWF4KG90aGVyLnkgKyBvdGhlci5oZWlnaHQsIHRoaXMueSArIHRoaXMuaGVpZ2h0KSAtIHk7XG4gICAgdGhpcy54ID0geDtcbiAgICB0aGlzLnkgPSB5O1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBtXG4gICAqIEBtZXRob2RzXG4gICAqL1xuICBhcHBseVRyYW5zZm9ybTogZnVuY3Rpb24gKCkge1xuICAgIHZhciBsdCA9IFtdO1xuICAgIHZhciByYiA9IFtdO1xuICAgIHZhciBsYiA9IFtdO1xuICAgIHZhciBydCA9IFtdO1xuICAgIHJldHVybiBmdW5jdGlvbiAobSkge1xuICAgICAgLy8gSW4gY2FzZSB1c2FnZSBsaWtlIHRoaXNcbiAgICAgIC8vIGVsLmdldEJvdW5kaW5nUmVjdCgpLmFwcGx5VHJhbnNmb3JtKGVsLnRyYW5zZm9ybSlcbiAgICAgIC8vIEFuZCBlbGVtZW50IGhhcyBubyB0cmFuc2Zvcm1cbiAgICAgIGlmICghbSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGx0WzBdID0gbGJbMF0gPSB0aGlzLng7XG4gICAgICBsdFsxXSA9IHJ0WzFdID0gdGhpcy55O1xuICAgICAgcmJbMF0gPSBydFswXSA9IHRoaXMueCArIHRoaXMud2lkdGg7XG4gICAgICByYlsxXSA9IGxiWzFdID0gdGhpcy55ICsgdGhpcy5oZWlnaHQ7XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKGx0LCBsdCwgbSk7XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKHJiLCByYiwgbSk7XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKGxiLCBsYiwgbSk7XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKHJ0LCBydCwgbSk7XG4gICAgICB0aGlzLnggPSBtYXRoTWluKGx0WzBdLCByYlswXSwgbGJbMF0sIHJ0WzBdKTtcbiAgICAgIHRoaXMueSA9IG1hdGhNaW4obHRbMV0sIHJiWzFdLCBsYlsxXSwgcnRbMV0pO1xuICAgICAgdmFyIG1heFggPSBtYXRoTWF4KGx0WzBdLCByYlswXSwgbGJbMF0sIHJ0WzBdKTtcbiAgICAgIHZhciBtYXhZID0gbWF0aE1heChsdFsxXSwgcmJbMV0sIGxiWzFdLCBydFsxXSk7XG4gICAgICB0aGlzLndpZHRoID0gbWF4WCAtIHRoaXMueDtcbiAgICAgIHRoaXMuaGVpZ2h0ID0gbWF4WSAtIHRoaXMueTtcbiAgICB9O1xuICB9KCksXG5cbiAgLyoqXG4gICAqIENhbGN1bGF0ZSBtYXRyaXggb2YgdHJhbnNmb3JtaW5nIGZyb20gc2VsZiB0byB0YXJnZXQgcmVjdFxuICAgKiBAcGFyYW0gIHttb2R1bGU6enJlbmRlci9jb3JlL0JvdW5kaW5nUmVjdH0gYlxuICAgKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIGNhbGN1bGF0ZVRyYW5zZm9ybTogZnVuY3Rpb24gKGIpIHtcbiAgICB2YXIgYSA9IHRoaXM7XG4gICAgdmFyIHN4ID0gYi53aWR0aCAvIGEud2lkdGg7XG4gICAgdmFyIHN5ID0gYi5oZWlnaHQgLyBhLmhlaWdodDtcbiAgICB2YXIgbSA9IG1hdHJpeC5jcmVhdGUoKTsgLy8g55+p6Zi15Y+z5LmYXG5cbiAgICBtYXRyaXgudHJhbnNsYXRlKG0sIG0sIFstYS54LCAtYS55XSk7XG4gICAgbWF0cml4LnNjYWxlKG0sIG0sIFtzeCwgc3ldKTtcbiAgICBtYXRyaXgudHJhbnNsYXRlKG0sIG0sIFtiLngsIGIueV0pO1xuICAgIHJldHVybiBtO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0geyhtb2R1bGU6ZWNoYXJ0cy9jb3JlL0JvdW5kaW5nUmVjdHxPYmplY3QpfSBiXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpbnRlcnNlY3Q6IGZ1bmN0aW9uIChiKSB7XG4gICAgaWYgKCFiKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKCEoYiBpbnN0YW5jZW9mIEJvdW5kaW5nUmVjdCkpIHtcbiAgICAgIC8vIE5vcm1hbGl6ZSBuZWdhdGl2ZSB3aWR0aC9oZWlnaHQuXG4gICAgICBiID0gQm91bmRpbmdSZWN0LmNyZWF0ZShiKTtcbiAgICB9XG5cbiAgICB2YXIgYSA9IHRoaXM7XG4gICAgdmFyIGF4MCA9IGEueDtcbiAgICB2YXIgYXgxID0gYS54ICsgYS53aWR0aDtcbiAgICB2YXIgYXkwID0gYS55O1xuICAgIHZhciBheTEgPSBhLnkgKyBhLmhlaWdodDtcbiAgICB2YXIgYngwID0gYi54O1xuICAgIHZhciBieDEgPSBiLnggKyBiLndpZHRoO1xuICAgIHZhciBieTAgPSBiLnk7XG4gICAgdmFyIGJ5MSA9IGIueSArIGIuaGVpZ2h0O1xuICAgIHJldHVybiAhKGF4MSA8IGJ4MCB8fCBieDEgPCBheDAgfHwgYXkxIDwgYnkwIHx8IGJ5MSA8IGF5MCk7XG4gIH0sXG4gIGNvbnRhaW46IGZ1bmN0aW9uICh4LCB5KSB7XG4gICAgdmFyIHJlY3QgPSB0aGlzO1xuICAgIHJldHVybiB4ID49IHJlY3QueCAmJiB4IDw9IHJlY3QueCArIHJlY3Qud2lkdGggJiYgeSA+PSByZWN0LnkgJiYgeSA8PSByZWN0LnkgKyByZWN0LmhlaWdodDtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvY29yZS9Cb3VuZGluZ1JlY3R9XG4gICAqL1xuICBjbG9uZTogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBuZXcgQm91bmRpbmdSZWN0KHRoaXMueCwgdGhpcy55LCB0aGlzLndpZHRoLCB0aGlzLmhlaWdodCk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIENvcHkgZnJvbSBhbm90aGVyIHJlY3RcbiAgICovXG4gIGNvcHk6IGZ1bmN0aW9uIChvdGhlcikge1xuICAgIHRoaXMueCA9IG90aGVyLng7XG4gICAgdGhpcy55ID0gb3RoZXIueTtcbiAgICB0aGlzLndpZHRoID0gb3RoZXIud2lkdGg7XG4gICAgdGhpcy5oZWlnaHQgPSBvdGhlci5oZWlnaHQ7XG4gIH0sXG4gIHBsYWluOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHg6IHRoaXMueCxcbiAgICAgIHk6IHRoaXMueSxcbiAgICAgIHdpZHRoOiB0aGlzLndpZHRoLFxuICAgICAgaGVpZ2h0OiB0aGlzLmhlaWdodFxuICAgIH07XG4gIH1cbn07XG4vKipcbiAqIEBwYXJhbSB7T2JqZWN0fG1vZHVsZTp6cmVuZGVyL2NvcmUvQm91bmRpbmdSZWN0fSByZWN0XG4gKiBAcGFyYW0ge251bWJlcn0gcmVjdC54XG4gKiBAcGFyYW0ge251bWJlcn0gcmVjdC55XG4gKiBAcGFyYW0ge251bWJlcn0gcmVjdC53aWR0aFxuICogQHBhcmFtIHtudW1iZXJ9IHJlY3QuaGVpZ2h0XG4gKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9jb3JlL0JvdW5kaW5nUmVjdH1cbiAqL1xuXG5Cb3VuZGluZ1JlY3QuY3JlYXRlID0gZnVuY3Rpb24gKHJlY3QpIHtcbiAgcmV0dXJuIG5ldyBCb3VuZGluZ1JlY3QocmVjdC54LCByZWN0LnksIHJlY3Qud2lkdGgsIHJlY3QuaGVpZ2h0KTtcbn07XG5cbnZhciBfZGVmYXVsdCA9IEJvdW5kaW5nUmVjdDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7OztBQUlBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUF2SEE7QUF5SEE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/BoundingRect.js
