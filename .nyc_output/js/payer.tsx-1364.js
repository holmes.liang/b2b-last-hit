__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _desk_component_ph__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/ph */ "./src/app/desk/component/ph/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/payer.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var Payer =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(Payer, _ModelWidget);

  //TODO
  // dataIdPrefix重做
  function Payer(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Payer);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Payer).call(this, props, context));
    _this.dataIdPrefix = _this.props.dataIdPrefix || "ext.payer";
    _this.groupCustomerPage = void 0;
    _this.timeout = null;

    _this.groupCustomerRef = function (ref) {
      _this.groupCustomerPage = ref;
    };

    _this.updatePayerFromInsured = function () {
      var _this$setValuesToMode;

      var insuredDataIdPrefix = _this.generateInsuredPropName("ext.insured.individual", _this.props.insuredDataIdPrefix);

      _this.setValuesToModel((_this$setValuesToMode = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("title"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".title"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("name"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".name"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("idType"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".idType"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("idNo"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".idNo"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("mobileNationCode"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".mobileNationCode"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("mobile"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".mobile"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("email"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".email"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("addressType"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".addressType"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("address"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".address"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("address.street"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".address.street"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$setValuesToMode, _this.generatePropName("address.postalCode"), _this.getValueFromModel("".concat(insuredDataIdPrefix, ".address.postalCode"))), _this$setValuesToMode));
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Payer, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Payer.prototype), "initState", this).call(this), {
        entityOptions: []
      });
    }
  }, {
    key: "generateInsuredPropName",
    value: function generateInsuredPropName(propName, insuredDataIdPrefix) {
      if (!insuredDataIdPrefix) return propName;
      return "".concat(insuredDataIdPrefix, ".").concat(propName);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var insuredDataIdPrefix = this.generateInsuredPropName("ext.insured.individual", this.props.insuredDataIdPrefix);

      var preInsured = lodash__WEBPACK_IMPORTED_MODULE_13___default.a.cloneDeep(this.getValueFromModel(insuredDataIdPrefix));

      var updatePayer = function updatePayer() {
        var isEqual = lodash__WEBPACK_IMPORTED_MODULE_13___default.a.isEqual(preInsured, _this2.getValueFromModel(insuredDataIdPrefix));

        var isN = _this2.getValueFromModel(_this2.generatePropName("sameAsPolicyholder")) === "N";

        if (isEqual || isN) {
          return;
        }

        _this2.updatePayerFromInsured();

        preInsured = lodash__WEBPACK_IMPORTED_MODULE_13___default.a.cloneDeep(_this2.getValueFromModel(insuredDataIdPrefix));
      };

      this.timeout = setInterval(updatePayer, 1000);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.timeout) clearInterval(this.timeout);
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      return "".concat(this.dataIdPrefix, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          insuredDataIdPrefix = _this$props.insuredDataIdPrefix,
          type = _this$props.type,
          defaultCountryCode = _this$props.defaultCountryCode,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["model", "form", "insuredDataIdPrefix", "type", "defaultCountryCode"]);

      var moneyType = {
        MMK: "MMR",
        THB: "THA",
        SGD: "SGP"
      }; //TODO 这行代码不知道有问题没

      if (this.getValueFromModel(this.generateInsuredPropName("ext.insured.partyType", this.props.insuredDataIdPrefix)) !== _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ORG_TYPES.INDIVIDUAL) {
        this.setValuesToModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, this.generatePropName("sameAsPolicyholder"), "N"));
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }), this.getValueFromModel(this.generateInsuredPropName("ext.insured.partyType", this.props.insuredDataIdPrefix)) === _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ORG_TYPES.INDIVIDUAL && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSwitch"], {
        form: form,
        model: model,
        propName: this.generatePropName("sameAsPolicyholder"),
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Same As Insured").thai("บุคคลเดียวกับผู้เอาประกัน").my("အာမခံထားအတိုင်းအတူတူ").getMessage(),
        checked: this.getValueFromModel(this.generatePropName("sameAsPolicyholder")) === "Y",
        onChange: function onChange(checked) {
          if (checked === "Y") {
            _this3.updatePayerFromInsured();
          } else {
            var _this3$setValuesToMod;

            _this3.setValuesToModel((_this3$setValuesToMod = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("name"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("idNo"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("mobile"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("email"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("addressType"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("address"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("address.street"), ""), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this3$setValuesToMod, _this3.generatePropName("address.postalCode"), ""), _this3$setValuesToMod));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }), this.getValueFromModel(this.generatePropName("sameAsPolicyholder")) === "N" && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component_ph__WEBPACK_IMPORTED_MODULE_14__["PolicyHolder"], {
        isPartyType: false,
        model: model,
        form: form,
        propsNameFixed: "ext.payer",
        partyType: "INDI",
        defaultCountryCode: defaultCountryCode,
        dataFixed: this.props.insuredDataIdPrefix ? "policy" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NRadio"], {
        form: form,
        model: model,
        tableName: "policy_delivery_to",
        propName: this.props.insuredDataIdPrefix ? "policy.ext.deliverPolicyTo" : "ext.deliverPolicyTo",
        dataFixed: this.props.insuredDataIdPrefix ? "policy" : "",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Delivery Policy To").thai("ส่งมอบกรมธรรม์ไปที่").my("Delivery မူဝါဒ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }));
    }
  }]);

  return Payer;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Payer);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BheWVyLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9wYXllci50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSZWFjdCwgU3R5bGVkfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7TW9kZWxXaWRnZXR9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQge05SYWRpbywgTlN3aXRjaH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQge0NvbnN0cywgTGFuZ3VhZ2V9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQge01vZGVsV2lkZ2V0UHJvcHN9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7UG9saWN5SG9sZGVyfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3BoXCI7XG5cbnR5cGUgUGF5ZXJTdGF0ZSA9IHt9O1xuXG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgUGF5ZXJQYWdlQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcbmV4cG9ydCB0eXBlIFBheWVyUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgdHlwZT86IGFueTtcbiAgZGF0YUlkUHJlZml4PzogYW55O1xuICBpbnN1cmVkRGF0YUlkUHJlZml4PzogYW55O1xuICBkZWZhdWx0Q291bnRyeUNvZGU/OiBhbnk7XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcblxuY2xhc3MgUGF5ZXI8UCBleHRlbmRzIFBheWVyUHJvcHMsIFMgZXh0ZW5kcyBQYXllclN0YXRlLCBDIGV4dGVuZHMgUGF5ZXJQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIC8vVE9ET1xuXG4gIC8vIGRhdGFJZFByZWZpeOmHjeWBmlxuICBwcml2YXRlIGRhdGFJZFByZWZpeDogYW55ID0gdGhpcy5wcm9wcy5kYXRhSWRQcmVmaXggfHwgXCJleHQucGF5ZXJcIjtcbiAgcHJvdGVjdGVkIGdyb3VwQ3VzdG9tZXJQYWdlOiBhbnk7XG4gIHByb3RlY3RlZCB0aW1lb3V0OiBhbnkgPSBudWxsO1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBQYXllclByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgZ3JvdXBDdXN0b21lclJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgIHRoaXMuZ3JvdXBDdXN0b21lclBhZ2UgPSByZWY7XG4gIH07XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBlbnRpdHlPcHRpb25zOiBbXSxcbiAgICB9KSBhcyBTO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZUluc3VyZWRQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBpbnN1cmVkRGF0YUlkUHJlZml4PzogYW55KTogc3RyaW5nIHtcbiAgICBpZiAoIWluc3VyZWREYXRhSWRQcmVmaXgpIHJldHVybiBwcm9wTmFtZTtcbiAgICByZXR1cm4gYCR7aW5zdXJlZERhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgaW5zdXJlZERhdGFJZFByZWZpeCA9IHRoaXMuZ2VuZXJhdGVJbnN1cmVkUHJvcE5hbWUoXCJleHQuaW5zdXJlZC5pbmRpdmlkdWFsXCIsIHRoaXMucHJvcHMuaW5zdXJlZERhdGFJZFByZWZpeCk7XG4gICAgbGV0IHByZUluc3VyZWQgPSBfLmNsb25lRGVlcCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGluc3VyZWREYXRhSWRQcmVmaXgpKTtcbiAgICBjb25zdCB1cGRhdGVQYXllciA9ICgpID0+IHtcbiAgICAgIGNvbnN0IGlzRXF1YWwgPSBfLmlzRXF1YWwocHJlSW5zdXJlZCwgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChpbnN1cmVkRGF0YUlkUHJlZml4KSk7XG4gICAgICBjb25zdCBpc04gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInNhbWVBc1BvbGljeWhvbGRlclwiKSkgPT09IFwiTlwiO1xuICAgICAgaWYgKGlzRXF1YWwgfHwgaXNOKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMudXBkYXRlUGF5ZXJGcm9tSW5zdXJlZCgpO1xuICAgICAgcHJlSW5zdXJlZCA9IF8uY2xvbmVEZWVwKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoaW5zdXJlZERhdGFJZFByZWZpeCkpO1xuICAgIH07XG4gICAgdGhpcy50aW1lb3V0ID0gc2V0SW50ZXJ2YWwodXBkYXRlUGF5ZXIsIDEwMDApO1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgaWYgKHRoaXMudGltZW91dCkgY2xlYXJJbnRlcnZhbCh0aGlzLnRpbWVvdXQpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiBgJHt0aGlzLmRhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgdXBkYXRlUGF5ZXJGcm9tSW5zdXJlZCA9ICgpID0+IHtcbiAgICBjb25zdCBpbnN1cmVkRGF0YUlkUHJlZml4ID0gdGhpcy5nZW5lcmF0ZUluc3VyZWRQcm9wTmFtZShcImV4dC5pbnN1cmVkLmluZGl2aWR1YWxcIiwgdGhpcy5wcm9wcy5pbnN1cmVkRGF0YUlkUHJlZml4KTtcbiAgICB0aGlzLnNldFZhbHVlc1RvTW9kZWwoe1xuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInRpdGxlXCIpXTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgJHtpbnN1cmVkRGF0YUlkUHJlZml4fS50aXRsZWApLFxuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm5hbWVcIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke2luc3VyZWREYXRhSWRQcmVmaXh9Lm5hbWVgKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZFR5cGVcIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke2luc3VyZWREYXRhSWRQcmVmaXh9LmlkVHlwZWApLFxuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke2luc3VyZWREYXRhSWRQcmVmaXh9LmlkTm9gKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJtb2JpbGVOYXRpb25Db2RlXCIpXTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgJHtpbnN1cmVkRGF0YUlkUHJlZml4fS5tb2JpbGVOYXRpb25Db2RlYCksXG4gICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibW9iaWxlXCIpXTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgJHtpbnN1cmVkRGF0YUlkUHJlZml4fS5tb2JpbGVgKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJlbWFpbFwiKV06IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYCR7aW5zdXJlZERhdGFJZFByZWZpeH0uZW1haWxgKSxcblxuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NUeXBlXCIpXTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgJHtpbnN1cmVkRGF0YUlkUHJlZml4fS5hZGRyZXNzVHlwZWApLFxuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NcIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke2luc3VyZWREYXRhSWRQcmVmaXh9LmFkZHJlc3NgKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnN0cmVldFwiKV06IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYCR7aW5zdXJlZERhdGFJZFByZWZpeH0uYWRkcmVzcy5zdHJlZXRgKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnBvc3RhbENvZGVcIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFxuICAgICAgICBgJHtpbnN1cmVkRGF0YUlkUHJlZml4fS5hZGRyZXNzLnBvc3RhbENvZGVgLFxuICAgICAgKSxcbiAgICB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgbW9kZWwsIGZvcm0sIGluc3VyZWREYXRhSWRQcmVmaXgsIHR5cGUsIGRlZmF1bHRDb3VudHJ5Q29kZSwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICBsZXQgbW9uZXlUeXBlOiBhbnkgPSB7IE1NSzogXCJNTVJcIiwgVEhCOiBcIlRIQVwiLCBTR0Q6IFwiU0dQXCIgfTtcbiAgICAvL1RPRE8g6L+Z6KGM5Luj56CB5LiN55+l6YGT5pyJ6Zeu6aKY5rKhXG5cbiAgICBpZiAodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlSW5zdXJlZFByb3BOYW1lKFwiZXh0Lmluc3VyZWQucGFydHlUeXBlXCIsIHRoaXMucHJvcHMuaW5zdXJlZERhdGFJZFByZWZpeCkpICE9PSBDb25zdHMuT1JHX1RZUEVTLklORElWSURVQUwpIHtcbiAgICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJzYW1lQXNQb2xpY3lob2xkZXJcIildOiBcIk5cIixcbiAgICAgIH0pO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgIHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVJbnN1cmVkUHJvcE5hbWUoXCJleHQuaW5zdXJlZC5wYXJ0eVR5cGVcIiwgdGhpcy5wcm9wcy5pbnN1cmVkRGF0YUlkUHJlZml4KSkgPT09IENvbnN0cy5PUkdfVFlQRVMuSU5ESVZJRFVBTCAmJiAoXG4gICAgICAgICAgPE5Td2l0Y2hcbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwic2FtZUFzUG9saWN5aG9sZGVyXCIpfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiU2FtZSBBcyBJbnN1cmVkXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4Lia4Li44LiE4LiE4Lil4LmA4LiU4Li14Lii4Lin4LiB4Lix4Lia4Lic4Li54LmJ4LmA4Lit4Liy4Lib4Lij4Liw4LiB4Lix4LiZXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAoeGArOGAmeGAgeGAtuGAkeGArOGAuOGAoeGAkOGAreGAr+GAhOGAuuGAuOGAoeGAkOGAsOGAkOGAsFwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgY2hlY2tlZD17dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJzYW1lQXNQb2xpY3lob2xkZXJcIikpID09PSBcIllcIn1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoY2hlY2tlZDogYW55KTogdm9pZCA9PiB7XG4gICAgICAgICAgICAgIGlmIChjaGVja2VkID09PSBcIllcIikge1xuICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlUGF5ZXJGcm9tSW5zdXJlZCgpO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgICAgICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibmFtZVwiKV06IFwiXCIsXG4gICAgICAgICAgICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWROb1wiKV06IFwiXCIsXG4gICAgICAgICAgICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibW9iaWxlXCIpXTogXCJcIixcbiAgICAgICAgICAgICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJlbWFpbFwiKV06IFwiXCIsXG4gICAgICAgICAgICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1R5cGVcIildOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NcIildOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3Muc3RyZWV0XCIpXTogXCJcIixcbiAgICAgICAgICAgICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnBvc3RhbENvZGVcIildOiBcIlwiLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgICl9XG4gICAgICAgIHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInNhbWVBc1BvbGljeWhvbGRlclwiKSkgPT09IFwiTlwiICYmIChcbiAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgIDxQb2xpY3lIb2xkZXIgaXNQYXJ0eVR5cGU9e2ZhbHNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BzTmFtZUZpeGVkPXtcImV4dC5wYXllclwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcnR5VHlwZT17XCJJTkRJXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdENvdW50cnlDb2RlPXtkZWZhdWx0Q291bnRyeUNvZGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YUZpeGVkPXt0aGlzLnByb3BzLmluc3VyZWREYXRhSWRQcmVmaXggPyBcInBvbGljeVwiIDogXCJcIn0vPlxuICAgICAgICAgICAgPC8+XG4gICAgICAgICl9XG4gICAgICAgIDxOUmFkaW9cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICB0YWJsZU5hbWU9XCJwb2xpY3lfZGVsaXZlcnlfdG9cIlxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLnByb3BzLmluc3VyZWREYXRhSWRQcmVmaXggPyBcInBvbGljeS5leHQuZGVsaXZlclBvbGljeVRvXCIgOiBcImV4dC5kZWxpdmVyUG9saWN5VG9cIn1cbiAgICAgICAgICBkYXRhRml4ZWQ9e3RoaXMucHJvcHMuaW5zdXJlZERhdGFJZFByZWZpeCA/IFwicG9saWN5XCIgOiBcIlwifVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkRlbGl2ZXJ5IFBvbGljeSBUb1wiKVxuICAgICAgICAgICAgLnRoYWkoXCLguKrguYjguIfguKHguK3guJrguIHguKPguKHguJjguKPguKPguKHguYzguYTguJvguJfguLXguYhcIilcbiAgICAgICAgICAgIC5teShcIkRlbGl2ZXJ5IOGAmeGAsOGAneGAq+GAklwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgLz5cbiAgICAgIDwvQy5Cb3hDb250ZW50PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgUGF5ZXI7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFpQkE7Ozs7O0FBQ0E7QUFFQTtBQUtBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFQQTtBQWlEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBZ0JBO0FBQ0E7QUFwRUE7QUFFQTtBQUNBOzs7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQXNCQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFVQTtBQUNBO0FBeEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQTZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7Ozs7QUEvSUE7QUFDQTtBQWlKQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/payer.tsx
