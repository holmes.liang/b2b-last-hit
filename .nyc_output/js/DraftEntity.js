

var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEntity
 * @format
 * 
 */


var DraftEntityInstance = __webpack_require__(/*! ./DraftEntityInstance */ "./node_modules/draft-js/lib/DraftEntityInstance.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var Map = Immutable.Map;
var instances = Map();
var instanceKey = 0;
/**
 * Temporary utility for generating the warnings
 */

function logWarning(oldMethodCall, newMethodCall) {
  console.warn('WARNING: ' + oldMethodCall + ' will be deprecated soon!\nPlease use "' + newMethodCall + '" instead.');
}
/**
 * A "document entity" is an object containing metadata associated with a
 * piece of text in a ContentBlock.
 *
 * For example, a `link` entity might include a `uri` property. When a
 * ContentBlock is rendered in the browser, text that refers to that link
 * entity may be rendered as an anchor, with the `uri` as the href value.
 *
 * In a ContentBlock, every position in the text may correspond to zero
 * or one entities. This correspondence is tracked using a key string,
 * generated via DraftEntity.create() and used to obtain entity metadata
 * via DraftEntity.get().
 */


var DraftEntity = {
  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.getLastCreatedEntityKey' instead.
   * ---
   * Get the random key string from whatever entity was last created.
   * We need this to support the new API, as part of transitioning to put Entity
   * storage in contentState.
   */
  getLastCreatedEntityKey: function getLastCreatedEntityKey() {
    logWarning('DraftEntity.getLastCreatedEntityKey', 'contentState.getLastCreatedEntityKey');
    return DraftEntity.__getLastCreatedEntityKey();
  },

  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.createEntity' instead.
   * ---
   * Create a DraftEntityInstance and store it for later retrieval.
   *
   * A random key string will be generated and returned. This key may
   * be used to track the entity's usage in a ContentBlock, and for
   * retrieving data about the entity at render time.
   */
  create: function create(type, mutability, data) {
    logWarning('DraftEntity.create', 'contentState.createEntity');
    return DraftEntity.__create(type, mutability, data);
  },

  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.addEntity' instead.
   * ---
   * Add an existing DraftEntityInstance to the DraftEntity map. This is
   * useful when restoring instances from the server.
   */
  add: function add(instance) {
    logWarning('DraftEntity.add', 'contentState.addEntity');
    return DraftEntity.__add(instance);
  },

  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.getEntity' instead.
   * ---
   * Retrieve the entity corresponding to the supplied key string.
   */
  get: function get(key) {
    logWarning('DraftEntity.get', 'contentState.getEntity');
    return DraftEntity.__get(key);
  },

  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.mergeEntityData' instead.
   * ---
   * Entity instances are immutable. If you need to update the data for an
   * instance, this method will merge your data updates and return a new
   * instance.
   */
  mergeData: function mergeData(key, toMerge) {
    logWarning('DraftEntity.mergeData', 'contentState.mergeEntityData');
    return DraftEntity.__mergeData(key, toMerge);
  },

  /**
   * WARNING: This method will be deprecated soon!
   * Please use 'contentState.replaceEntityData' instead.
   * ---
   * Completely replace the data for a given instance.
   */
  replaceData: function replaceData(key, newData) {
    logWarning('DraftEntity.replaceData', 'contentState.replaceEntityData');
    return DraftEntity.__replaceData(key, newData);
  },
  // ***********************************WARNING******************************
  // --- the above public API will be deprecated in the next version of Draft!
  // The methods below this line are private - don't call them directly.

  /**
   * Get the random key string from whatever entity was last created.
   * We need this to support the new API, as part of transitioning to put Entity
   * storage in contentState.
   */
  __getLastCreatedEntityKey: function __getLastCreatedEntityKey() {
    return '' + instanceKey;
  },

  /**
   * Create a DraftEntityInstance and store it for later retrieval.
   *
   * A random key string will be generated and returned. This key may
   * be used to track the entity's usage in a ContentBlock, and for
   * retrieving data about the entity at render time.
   */
  __create: function __create(type, mutability, data) {
    return DraftEntity.__add(new DraftEntityInstance({
      type: type,
      mutability: mutability,
      data: data || {}
    }));
  },

  /**
   * Add an existing DraftEntityInstance to the DraftEntity map. This is
   * useful when restoring instances from the server.
   */
  __add: function __add(instance) {
    var key = '' + ++instanceKey;
    instances = instances.set(key, instance);
    return key;
  },

  /**
   * Retrieve the entity corresponding to the supplied key string.
   */
  __get: function __get(key) {
    var instance = instances.get(key);
    !!!instance ?  true ? invariant(false, 'Unknown DraftEntity key: %s.', key) : undefined : void 0;
    return instance;
  },

  /**
   * Entity instances are immutable. If you need to update the data for an
   * instance, this method will merge your data updates and return a new
   * instance.
   */
  __mergeData: function __mergeData(key, toMerge) {
    var instance = DraftEntity.__get(key);

    var newData = _extends({}, instance.getData(), toMerge);

    var newInstance = instance.set('data', newData);
    instances = instances.set(key, newInstance);
    return newInstance;
  },

  /**
   * Completely replace the data for a given instance.
   */
  __replaceData: function __replaceData(key, newData) {
    var instance = DraftEntity.__get(key);

    var newInstance = instance.set('data', newData);
    instances = instances.set(key, newInstance);
    return newInstance;
  }
};
module.exports = DraftEntity;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RW50aXR5LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RW50aXR5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRHJhZnRFbnRpdHlcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbnZhciBEcmFmdEVudGl0eUluc3RhbmNlID0gcmVxdWlyZSgnLi9EcmFmdEVudGl0eUluc3RhbmNlJyk7XG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XG5cbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCdmYmpzL2xpYi9pbnZhcmlhbnQnKTtcblxudmFyIE1hcCA9IEltbXV0YWJsZS5NYXA7XG5cblxudmFyIGluc3RhbmNlcyA9IE1hcCgpO1xudmFyIGluc3RhbmNlS2V5ID0gMDtcblxuLyoqXG4gKiBUZW1wb3JhcnkgdXRpbGl0eSBmb3IgZ2VuZXJhdGluZyB0aGUgd2FybmluZ3NcbiAqL1xuZnVuY3Rpb24gbG9nV2FybmluZyhvbGRNZXRob2RDYWxsLCBuZXdNZXRob2RDYWxsKSB7XG4gIGNvbnNvbGUud2FybignV0FSTklORzogJyArIG9sZE1ldGhvZENhbGwgKyAnIHdpbGwgYmUgZGVwcmVjYXRlZCBzb29uIVxcblBsZWFzZSB1c2UgXCInICsgbmV3TWV0aG9kQ2FsbCArICdcIiBpbnN0ZWFkLicpO1xufVxuXG4vKipcbiAqIEEgXCJkb2N1bWVudCBlbnRpdHlcIiBpcyBhbiBvYmplY3QgY29udGFpbmluZyBtZXRhZGF0YSBhc3NvY2lhdGVkIHdpdGggYVxuICogcGllY2Ugb2YgdGV4dCBpbiBhIENvbnRlbnRCbG9jay5cbiAqXG4gKiBGb3IgZXhhbXBsZSwgYSBgbGlua2AgZW50aXR5IG1pZ2h0IGluY2x1ZGUgYSBgdXJpYCBwcm9wZXJ0eS4gV2hlbiBhXG4gKiBDb250ZW50QmxvY2sgaXMgcmVuZGVyZWQgaW4gdGhlIGJyb3dzZXIsIHRleHQgdGhhdCByZWZlcnMgdG8gdGhhdCBsaW5rXG4gKiBlbnRpdHkgbWF5IGJlIHJlbmRlcmVkIGFzIGFuIGFuY2hvciwgd2l0aCB0aGUgYHVyaWAgYXMgdGhlIGhyZWYgdmFsdWUuXG4gKlxuICogSW4gYSBDb250ZW50QmxvY2ssIGV2ZXJ5IHBvc2l0aW9uIGluIHRoZSB0ZXh0IG1heSBjb3JyZXNwb25kIHRvIHplcm9cbiAqIG9yIG9uZSBlbnRpdGllcy4gVGhpcyBjb3JyZXNwb25kZW5jZSBpcyB0cmFja2VkIHVzaW5nIGEga2V5IHN0cmluZyxcbiAqIGdlbmVyYXRlZCB2aWEgRHJhZnRFbnRpdHkuY3JlYXRlKCkgYW5kIHVzZWQgdG8gb2J0YWluIGVudGl0eSBtZXRhZGF0YVxuICogdmlhIERyYWZ0RW50aXR5LmdldCgpLlxuICovXG52YXIgRHJhZnRFbnRpdHkgPSB7XG4gIC8qKlxuICAgKiBXQVJOSU5HOiBUaGlzIG1ldGhvZCB3aWxsIGJlIGRlcHJlY2F0ZWQgc29vbiFcbiAgICogUGxlYXNlIHVzZSAnY29udGVudFN0YXRlLmdldExhc3RDcmVhdGVkRW50aXR5S2V5JyBpbnN0ZWFkLlxuICAgKiAtLS1cbiAgICogR2V0IHRoZSByYW5kb20ga2V5IHN0cmluZyBmcm9tIHdoYXRldmVyIGVudGl0eSB3YXMgbGFzdCBjcmVhdGVkLlxuICAgKiBXZSBuZWVkIHRoaXMgdG8gc3VwcG9ydCB0aGUgbmV3IEFQSSwgYXMgcGFydCBvZiB0cmFuc2l0aW9uaW5nIHRvIHB1dCBFbnRpdHlcbiAgICogc3RvcmFnZSBpbiBjb250ZW50U3RhdGUuXG4gICAqL1xuICBnZXRMYXN0Q3JlYXRlZEVudGl0eUtleTogZnVuY3Rpb24gZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKSB7XG4gICAgbG9nV2FybmluZygnRHJhZnRFbnRpdHkuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXknLCAnY29udGVudFN0YXRlLmdldExhc3RDcmVhdGVkRW50aXR5S2V5Jyk7XG4gICAgcmV0dXJuIERyYWZ0RW50aXR5Ll9fZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKTtcbiAgfSxcblxuICAvKipcbiAgICogV0FSTklORzogVGhpcyBtZXRob2Qgd2lsbCBiZSBkZXByZWNhdGVkIHNvb24hXG4gICAqIFBsZWFzZSB1c2UgJ2NvbnRlbnRTdGF0ZS5jcmVhdGVFbnRpdHknIGluc3RlYWQuXG4gICAqIC0tLVxuICAgKiBDcmVhdGUgYSBEcmFmdEVudGl0eUluc3RhbmNlIGFuZCBzdG9yZSBpdCBmb3IgbGF0ZXIgcmV0cmlldmFsLlxuICAgKlxuICAgKiBBIHJhbmRvbSBrZXkgc3RyaW5nIHdpbGwgYmUgZ2VuZXJhdGVkIGFuZCByZXR1cm5lZC4gVGhpcyBrZXkgbWF5XG4gICAqIGJlIHVzZWQgdG8gdHJhY2sgdGhlIGVudGl0eSdzIHVzYWdlIGluIGEgQ29udGVudEJsb2NrLCBhbmQgZm9yXG4gICAqIHJldHJpZXZpbmcgZGF0YSBhYm91dCB0aGUgZW50aXR5IGF0IHJlbmRlciB0aW1lLlxuICAgKi9cbiAgY3JlYXRlOiBmdW5jdGlvbiBjcmVhdGUodHlwZSwgbXV0YWJpbGl0eSwgZGF0YSkge1xuICAgIGxvZ1dhcm5pbmcoJ0RyYWZ0RW50aXR5LmNyZWF0ZScsICdjb250ZW50U3RhdGUuY3JlYXRlRW50aXR5Jyk7XG4gICAgcmV0dXJuIERyYWZ0RW50aXR5Ll9fY3JlYXRlKHR5cGUsIG11dGFiaWxpdHksIGRhdGEpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBXQVJOSU5HOiBUaGlzIG1ldGhvZCB3aWxsIGJlIGRlcHJlY2F0ZWQgc29vbiFcbiAgICogUGxlYXNlIHVzZSAnY29udGVudFN0YXRlLmFkZEVudGl0eScgaW5zdGVhZC5cbiAgICogLS0tXG4gICAqIEFkZCBhbiBleGlzdGluZyBEcmFmdEVudGl0eUluc3RhbmNlIHRvIHRoZSBEcmFmdEVudGl0eSBtYXAuIFRoaXMgaXNcbiAgICogdXNlZnVsIHdoZW4gcmVzdG9yaW5nIGluc3RhbmNlcyBmcm9tIHRoZSBzZXJ2ZXIuXG4gICAqL1xuICBhZGQ6IGZ1bmN0aW9uIGFkZChpbnN0YW5jZSkge1xuICAgIGxvZ1dhcm5pbmcoJ0RyYWZ0RW50aXR5LmFkZCcsICdjb250ZW50U3RhdGUuYWRkRW50aXR5Jyk7XG4gICAgcmV0dXJuIERyYWZ0RW50aXR5Ll9fYWRkKGluc3RhbmNlKTtcbiAgfSxcblxuICAvKipcbiAgICogV0FSTklORzogVGhpcyBtZXRob2Qgd2lsbCBiZSBkZXByZWNhdGVkIHNvb24hXG4gICAqIFBsZWFzZSB1c2UgJ2NvbnRlbnRTdGF0ZS5nZXRFbnRpdHknIGluc3RlYWQuXG4gICAqIC0tLVxuICAgKiBSZXRyaWV2ZSB0aGUgZW50aXR5IGNvcnJlc3BvbmRpbmcgdG8gdGhlIHN1cHBsaWVkIGtleSBzdHJpbmcuXG4gICAqL1xuICBnZXQ6IGZ1bmN0aW9uIGdldChrZXkpIHtcbiAgICBsb2dXYXJuaW5nKCdEcmFmdEVudGl0eS5nZXQnLCAnY29udGVudFN0YXRlLmdldEVudGl0eScpO1xuICAgIHJldHVybiBEcmFmdEVudGl0eS5fX2dldChrZXkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBXQVJOSU5HOiBUaGlzIG1ldGhvZCB3aWxsIGJlIGRlcHJlY2F0ZWQgc29vbiFcbiAgICogUGxlYXNlIHVzZSAnY29udGVudFN0YXRlLm1lcmdlRW50aXR5RGF0YScgaW5zdGVhZC5cbiAgICogLS0tXG4gICAqIEVudGl0eSBpbnN0YW5jZXMgYXJlIGltbXV0YWJsZS4gSWYgeW91IG5lZWQgdG8gdXBkYXRlIHRoZSBkYXRhIGZvciBhblxuICAgKiBpbnN0YW5jZSwgdGhpcyBtZXRob2Qgd2lsbCBtZXJnZSB5b3VyIGRhdGEgdXBkYXRlcyBhbmQgcmV0dXJuIGEgbmV3XG4gICAqIGluc3RhbmNlLlxuICAgKi9cbiAgbWVyZ2VEYXRhOiBmdW5jdGlvbiBtZXJnZURhdGEoa2V5LCB0b01lcmdlKSB7XG4gICAgbG9nV2FybmluZygnRHJhZnRFbnRpdHkubWVyZ2VEYXRhJywgJ2NvbnRlbnRTdGF0ZS5tZXJnZUVudGl0eURhdGEnKTtcbiAgICByZXR1cm4gRHJhZnRFbnRpdHkuX19tZXJnZURhdGEoa2V5LCB0b01lcmdlKTtcbiAgfSxcblxuICAvKipcbiAgICogV0FSTklORzogVGhpcyBtZXRob2Qgd2lsbCBiZSBkZXByZWNhdGVkIHNvb24hXG4gICAqIFBsZWFzZSB1c2UgJ2NvbnRlbnRTdGF0ZS5yZXBsYWNlRW50aXR5RGF0YScgaW5zdGVhZC5cbiAgICogLS0tXG4gICAqIENvbXBsZXRlbHkgcmVwbGFjZSB0aGUgZGF0YSBmb3IgYSBnaXZlbiBpbnN0YW5jZS5cbiAgICovXG4gIHJlcGxhY2VEYXRhOiBmdW5jdGlvbiByZXBsYWNlRGF0YShrZXksIG5ld0RhdGEpIHtcbiAgICBsb2dXYXJuaW5nKCdEcmFmdEVudGl0eS5yZXBsYWNlRGF0YScsICdjb250ZW50U3RhdGUucmVwbGFjZUVudGl0eURhdGEnKTtcbiAgICByZXR1cm4gRHJhZnRFbnRpdHkuX19yZXBsYWNlRGF0YShrZXksIG5ld0RhdGEpO1xuICB9LFxuXG4gIC8vICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqV0FSTklORyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAvLyAtLS0gdGhlIGFib3ZlIHB1YmxpYyBBUEkgd2lsbCBiZSBkZXByZWNhdGVkIGluIHRoZSBuZXh0IHZlcnNpb24gb2YgRHJhZnQhXG4gIC8vIFRoZSBtZXRob2RzIGJlbG93IHRoaXMgbGluZSBhcmUgcHJpdmF0ZSAtIGRvbid0IGNhbGwgdGhlbSBkaXJlY3RseS5cblxuICAvKipcbiAgICogR2V0IHRoZSByYW5kb20ga2V5IHN0cmluZyBmcm9tIHdoYXRldmVyIGVudGl0eSB3YXMgbGFzdCBjcmVhdGVkLlxuICAgKiBXZSBuZWVkIHRoaXMgdG8gc3VwcG9ydCB0aGUgbmV3IEFQSSwgYXMgcGFydCBvZiB0cmFuc2l0aW9uaW5nIHRvIHB1dCBFbnRpdHlcbiAgICogc3RvcmFnZSBpbiBjb250ZW50U3RhdGUuXG4gICAqL1xuICBfX2dldExhc3RDcmVhdGVkRW50aXR5S2V5OiBmdW5jdGlvbiBfX2dldExhc3RDcmVhdGVkRW50aXR5S2V5KCkge1xuICAgIHJldHVybiAnJyArIGluc3RhbmNlS2V5O1xuICB9LFxuXG4gIC8qKlxuICAgKiBDcmVhdGUgYSBEcmFmdEVudGl0eUluc3RhbmNlIGFuZCBzdG9yZSBpdCBmb3IgbGF0ZXIgcmV0cmlldmFsLlxuICAgKlxuICAgKiBBIHJhbmRvbSBrZXkgc3RyaW5nIHdpbGwgYmUgZ2VuZXJhdGVkIGFuZCByZXR1cm5lZC4gVGhpcyBrZXkgbWF5XG4gICAqIGJlIHVzZWQgdG8gdHJhY2sgdGhlIGVudGl0eSdzIHVzYWdlIGluIGEgQ29udGVudEJsb2NrLCBhbmQgZm9yXG4gICAqIHJldHJpZXZpbmcgZGF0YSBhYm91dCB0aGUgZW50aXR5IGF0IHJlbmRlciB0aW1lLlxuICAgKi9cbiAgX19jcmVhdGU6IGZ1bmN0aW9uIF9fY3JlYXRlKHR5cGUsIG11dGFiaWxpdHksIGRhdGEpIHtcbiAgICByZXR1cm4gRHJhZnRFbnRpdHkuX19hZGQobmV3IERyYWZ0RW50aXR5SW5zdGFuY2UoeyB0eXBlOiB0eXBlLCBtdXRhYmlsaXR5OiBtdXRhYmlsaXR5LCBkYXRhOiBkYXRhIHx8IHt9IH0pKTtcbiAgfSxcblxuICAvKipcbiAgICogQWRkIGFuIGV4aXN0aW5nIERyYWZ0RW50aXR5SW5zdGFuY2UgdG8gdGhlIERyYWZ0RW50aXR5IG1hcC4gVGhpcyBpc1xuICAgKiB1c2VmdWwgd2hlbiByZXN0b3JpbmcgaW5zdGFuY2VzIGZyb20gdGhlIHNlcnZlci5cbiAgICovXG4gIF9fYWRkOiBmdW5jdGlvbiBfX2FkZChpbnN0YW5jZSkge1xuICAgIHZhciBrZXkgPSAnJyArICsraW5zdGFuY2VLZXk7XG4gICAgaW5zdGFuY2VzID0gaW5zdGFuY2VzLnNldChrZXksIGluc3RhbmNlKTtcbiAgICByZXR1cm4ga2V5O1xuICB9LFxuXG4gIC8qKlxuICAgKiBSZXRyaWV2ZSB0aGUgZW50aXR5IGNvcnJlc3BvbmRpbmcgdG8gdGhlIHN1cHBsaWVkIGtleSBzdHJpbmcuXG4gICAqL1xuICBfX2dldDogZnVuY3Rpb24gX19nZXQoa2V5KSB7XG4gICAgdmFyIGluc3RhbmNlID0gaW5zdGFuY2VzLmdldChrZXkpO1xuICAgICEhIWluc3RhbmNlID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ1Vua25vd24gRHJhZnRFbnRpdHkga2V5OiAlcy4nLCBrZXkpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgICByZXR1cm4gaW5zdGFuY2U7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEVudGl0eSBpbnN0YW5jZXMgYXJlIGltbXV0YWJsZS4gSWYgeW91IG5lZWQgdG8gdXBkYXRlIHRoZSBkYXRhIGZvciBhblxuICAgKiBpbnN0YW5jZSwgdGhpcyBtZXRob2Qgd2lsbCBtZXJnZSB5b3VyIGRhdGEgdXBkYXRlcyBhbmQgcmV0dXJuIGEgbmV3XG4gICAqIGluc3RhbmNlLlxuICAgKi9cbiAgX19tZXJnZURhdGE6IGZ1bmN0aW9uIF9fbWVyZ2VEYXRhKGtleSwgdG9NZXJnZSkge1xuICAgIHZhciBpbnN0YW5jZSA9IERyYWZ0RW50aXR5Ll9fZ2V0KGtleSk7XG4gICAgdmFyIG5ld0RhdGEgPSBfZXh0ZW5kcyh7fSwgaW5zdGFuY2UuZ2V0RGF0YSgpLCB0b01lcmdlKTtcbiAgICB2YXIgbmV3SW5zdGFuY2UgPSBpbnN0YW5jZS5zZXQoJ2RhdGEnLCBuZXdEYXRhKTtcbiAgICBpbnN0YW5jZXMgPSBpbnN0YW5jZXMuc2V0KGtleSwgbmV3SW5zdGFuY2UpO1xuICAgIHJldHVybiBuZXdJbnN0YW5jZTtcbiAgfSxcblxuICAvKipcbiAgICogQ29tcGxldGVseSByZXBsYWNlIHRoZSBkYXRhIGZvciBhIGdpdmVuIGluc3RhbmNlLlxuICAgKi9cbiAgX19yZXBsYWNlRGF0YTogZnVuY3Rpb24gX19yZXBsYWNlRGF0YShrZXksIG5ld0RhdGEpIHtcbiAgICB2YXIgaW5zdGFuY2UgPSBEcmFmdEVudGl0eS5fX2dldChrZXkpO1xuICAgIHZhciBuZXdJbnN0YW5jZSA9IGluc3RhbmNlLnNldCgnZGF0YScsIG5ld0RhdGEpO1xuICAgIGluc3RhbmNlcyA9IGluc3RhbmNlcy5zZXQoa2V5LCBuZXdJbnN0YW5jZSk7XG4gICAgcmV0dXJuIG5ld0luc3RhbmNlO1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RW50aXR5OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUVBOzs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQTVJQTtBQStJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEntity.js
