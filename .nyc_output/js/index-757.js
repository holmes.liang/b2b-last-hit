__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Switch; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_switch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-switch */ "./node_modules/rc-switch/es/index.js");
/* harmony import */ var rc_switch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rc_switch__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _util_wave__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_util/wave */ "./node_modules/antd/es/_util/wave.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}











var Switch =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Switch, _React$Component);

  function Switch(props) {
    var _this;

    _classCallCheck(this, Switch);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Switch).call(this, props));

    _this.saveSwitch = function (node) {
      _this.rcSwitch = node;
    };

    _this.renderSwitch = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          size = _this$props.size,
          loading = _this$props.loading,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          disabled = _this$props.disabled;
      var prefixCls = getPrefixCls('switch', customizePrefixCls);
      var classes = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-small"), size === 'small'), _defineProperty(_classNames, "".concat(prefixCls, "-loading"), loading), _classNames));
      var loadingIcon = loading ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_6__["default"], {
        type: "loading",
        className: "".concat(prefixCls, "-loading-icon")
      }) : null;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_wave__WEBPACK_IMPORTED_MODULE_5__["default"], {
        insertExtraNode: true
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_switch__WEBPACK_IMPORTED_MODULE_2___default.a, _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_4__["default"])(_this.props, ['loading']), {
        prefixCls: prefixCls,
        className: classes,
        disabled: disabled || loading,
        ref: _this.saveSwitch,
        loadingIcon: loadingIcon
      })));
    };

    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])('checked' in props || !('value' in props), 'Switch', '`value` is not validate prop, do you mean `checked`?');
    return _this;
  }

  _createClass(Switch, [{
    key: "focus",
    value: function focus() {
      this.rcSwitch.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.rcSwitch.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_7__["ConfigConsumer"], null, this.renderSwitch);
    }
  }]);

  return Switch;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Switch.__ANT_SWITCH = true;
Switch.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  // HACK: https://github.com/ant-design/ant-design/issues/5368
  // size=default and size=large are the same
  size: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](['small', 'default', 'large']),
  className: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zd2l0Y2gvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3N3aXRjaC9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IFJjU3dpdGNoIGZyb20gJ3JjLXN3aXRjaCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IFdhdmUgZnJvbSAnLi4vX3V0aWwvd2F2ZSc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3dpdGNoIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc2F2ZVN3aXRjaCA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnJjU3dpdGNoID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJTd2l0Y2ggPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc2l6ZSwgbG9hZGluZywgY2xhc3NOYW1lID0gJycsIGRpc2FibGVkIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdzd2l0Y2gnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgY2xhc3NlcyA9IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCB7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc21hbGxgXTogc2l6ZSA9PT0gJ3NtYWxsJyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1sb2FkaW5nYF06IGxvYWRpbmcsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvbnN0IGxvYWRpbmdJY29uID0gbG9hZGluZyA/ICg8SWNvbiB0eXBlPVwibG9hZGluZ1wiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1sb2FkaW5nLWljb25gfS8+KSA6IG51bGw7XG4gICAgICAgICAgICByZXR1cm4gKDxXYXZlIGluc2VydEV4dHJhTm9kZT5cbiAgICAgICAgPFJjU3dpdGNoIHsuLi5vbWl0KHRoaXMucHJvcHMsIFsnbG9hZGluZyddKX0gcHJlZml4Q2xzPXtwcmVmaXhDbHN9IGNsYXNzTmFtZT17Y2xhc3Nlc30gZGlzYWJsZWQ9e2Rpc2FibGVkIHx8IGxvYWRpbmd9IHJlZj17dGhpcy5zYXZlU3dpdGNofSBsb2FkaW5nSWNvbj17bG9hZGluZ0ljb259Lz5cbiAgICAgIDwvV2F2ZT4pO1xuICAgICAgICB9O1xuICAgICAgICB3YXJuaW5nKCdjaGVja2VkJyBpbiBwcm9wcyB8fCAhKCd2YWx1ZScgaW4gcHJvcHMpLCAnU3dpdGNoJywgJ2B2YWx1ZWAgaXMgbm90IHZhbGlkYXRlIHByb3AsIGRvIHlvdSBtZWFuIGBjaGVja2VkYD8nKTtcbiAgICB9XG4gICAgZm9jdXMoKSB7XG4gICAgICAgIHRoaXMucmNTd2l0Y2guZm9jdXMoKTtcbiAgICB9XG4gICAgYmx1cigpIHtcbiAgICAgICAgdGhpcy5yY1N3aXRjaC5ibHVyKCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJTd2l0Y2h9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuU3dpdGNoLl9fQU5UX1NXSVRDSCA9IHRydWU7XG5Td2l0Y2gucHJvcFR5cGVzID0ge1xuICAgIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgICAvLyBIQUNLOiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy81MzY4XG4gICAgLy8gc2l6ZT1kZWZhdWx0IGFuZCBzaXplPWxhcmdlIGFyZSB0aGUgc2FtZVxuICAgIHNpemU6IFByb3BUeXBlcy5vbmVPZihbJ3NtYWxsJywgJ2RlZmF1bHQnLCAnbGFyZ2UnXSksXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVEE7QUFDQTtBQVdBO0FBakJBO0FBa0JBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUE1QkE7QUFDQTtBQURBO0FBOEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/switch/index.js
