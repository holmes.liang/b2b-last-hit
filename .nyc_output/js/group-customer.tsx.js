__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _field_group__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../field-group */ "./src/app/desk/component/field-group.tsx");
/* harmony import */ var _NSelect_filter__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./NSelect-filter */ "./src/app/desk/component/group-customer-company/NSelect-filter.tsx");
/* harmony import */ var _choice_search_list__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./choice-search-list */ "./src/app/desk/component/group-customer-company/choice-search-list.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _ph_ocr__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../ph/ocr */ "./src/app/desk/component/ph/ocr.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_ekyc_upload__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk/ekyc/upload */ "./src/app/desk/ekyc/upload.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/group-customer-company/group-customer.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    position: relative;\n     .ocr-mobile {\n          position: absolute;\n          margin-left: calc(25% + 10px);\n          margin-top: 0px;\n          cursor: pointer;\n          i {\n            font-size: 26px;\n          }\n          z-index: 999;\n     }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            position: relative;\n             .ocr {\n                  position: absolute;\n                  margin-left: calc(80% + 10px);\n                  margin-top: -65px;\n                  cursor: pointer;\n                  i {\n                    font-size: 26px;\n                    &.icon-identify {\n                      padding-left: 10px;\n                      color: ", "\n                    }\n                  }\n             }\n        "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}















var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_21__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var GroupCustomer =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(GroupCustomer, _ModelWidget);

  function GroupCustomer(props, context) {
    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, GroupCustomer);

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(GroupCustomer).call(this, props, context));
    _this2.idNoPage = void 0;
    _this2.namePage = void 0;

    _this2.initIdNoRef = function (ref) {
      _this2.idNoPage = ref;
    };

    _this2.initNameRef = function (ref) {
      _this2.namePage = ref;
    };

    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(GroupCustomer, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.showNameIdNo();
    }
  }, {
    key: "showNameIdNo",
    value: function showNameIdNo() {
      this.idNoPage && this.idNoPage.setShowLabel(this.getValueFromModel(this.generatePropName("idNo")));
      this.namePage && this.namePage.setShowLabel(this.getValueFromModel(this.generatePropName("name")));
    }
  }, {
    key: "isBeforeValue",
    value: function isBeforeValue() {
      return this.getValueFromModel(this.generatePropName("idNo")) || this.getValueFromModel(this.generatePropName("name"));
    }
  }, {
    key: "postData",
    value: function postData() {
      var model = this.props.model;
      return {
        bizId: lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policyId") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.policyId"),
        bizType: lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "bizType") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.bizType"),
        itntCode: lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "itntCode") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.itntCode")
      };
    }
  }, {
    key: "getKycStatus",
    value: function getKycStatus() {
      var _this3 = this;

      _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post("/kyc/status", this.postData(), {
        loading: true
      }).then(function (res) {
        var respData = (res.body || {}).respData;

        _this3.setState({
          kycStatus: respData
        });
      });
    }
  }, {
    key: "renderIndiNameWithOcr",
    value: function renderIndiNameWithOcr() {
      var _this4 = this;

      var model = this.props.model;

      var productCode = lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.productCode") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "productCode");

      var kycStatus = this.state.kycStatus;
      var kycStatusObj = {
        PENDING: "#e6863c",
        SUCCESS: "green",
        FAILED: "ff0000"
      };

      if (_common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile()) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(OcrMobileStyle, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 107
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "ocr-mobile",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 108
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("i", {
          onClick: function onClick() {
            _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_19__["default"].create({
              Component: function Component(_ref) {
                var onCancel = _ref.onCancel;
                return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_ph_ocr__WEBPACK_IMPORTED_MODULE_18__["default"], {
                  onCancel: onCancel,
                  onOk: function onOk(data) {
                    _this4.setFormValues(data, true);

                    onCancel();
                  },
                  model: _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].asProxied({
                    policyId: lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policyId") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.policyId")
                  }),
                  __source: {
                    fileName: _jsxFileName,
                    lineNumber: 112
                  },
                  __self: this
                });
              },
              onCancel: function onCancel() {}
            });
          },
          className: "iconfont icon-ocr",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        }))), this._renderIndiName());
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, this._renderIndiName(), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "ocr",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("i", {
        onClick: function onClick() {
          _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_19__["default"].create({
            Component: function Component(_ref2) {
              var onCancel = _ref2.onCancel;
              return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_ph_ocr__WEBPACK_IMPORTED_MODULE_18__["default"], {
                onCancel: onCancel,
                onOk: function onOk(data) {
                  _this4.setFormValues(data, true);

                  onCancel();
                },
                model: _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].asProxied({
                  policyId: lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policyId") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.policyId")
                }),
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 138
                },
                __self: this
              });
            },
            onCancel: function onCancel() {}
          });
        },
        className: "iconfont icon-ocr",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }), productCode !== "PHS" && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("i", {
        onClick: function onClick() {
          if (kycStatus === "SUCCESS") {
            return;
          }

          _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_19__["default"].create({
            Component: function Component(_ref3) {
              var _onCancel = _ref3.onCancel;
              return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_ekyc_upload__WEBPACK_IMPORTED_MODULE_20__["default"], {
                onCancel: function onCancel(data) {
                  if (!_this4.isBeforeValue()) {
                    _this4.setFormValues(data, true);
                  }

                  _onCancel();

                  _this4.getKycStatus();
                },
                model: _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].asProxied(_this4.postData()),
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 156
                },
                __self: this
              });
            },
            onCancel: function onCancel() {}
          });
        },
        className: "iconfont icon-identify",
        style: {
          color: kycStatus ? kycStatusObj[kycStatus] : "#e6863c"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150
        },
        __self: this
      })));
    }
  }, {
    key: "initState",
    value: function initState() {
      var model = this.props.model;

      var kycStatus = lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "kycStatus") || lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "policy.kycStatus");

      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(GroupCustomer.prototype), "initState", this).call(this), {
        kycStatus: kycStatus
      });
    } //renders

  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var C = this.getComponents();

      var _this = this;

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          isNotSearh = _this$props.isNotSearh,
          dataFixed = _this$props.dataFixed,
          _this$props$groupType = _this$props.groupType,
          groupType = _this$props$groupType === void 0 ? "INDI" : _this$props$groupType,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$props, ["model", "form", "isNotSearh", "dataFixed", "groupType"]);

      var currencyCode = lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(model, "currencyCode");

      var partyType = this.props.partyType || this.getValueFromModel(this.generatePropName("partyType"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191
        },
        __self: this
      }), partyType === "INDI" && this.renderIndiNameWithOcr(), partyType !== "INDI" && this.renderNameOrg(), partyType && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_14__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID Type / No.").thai("ประเภท ID / หมายเลข.").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      }, partyType === "INDI" && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_NSelect_filter__WEBPACK_IMPORTED_MODULE_15__["default"], {
        size: "large",
        form: form,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID Type").thai("ประเภท ID").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        model: model,
        tableName: "idtype",
        dataFixed: dataFixed,
        propName: this.generatePropName("idType"),
        onChange: function onChange(value) {
          _this5.props.form.validateFields([_this5.generatePropName("idNo")], {
            force: true
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 205
        },
        __self: this
      }), partyType === "ORG" && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_NSelect_filter__WEBPACK_IMPORTED_MODULE_15__["default"], {
        size: "large",
        form: form,
        required: true,
        isPF: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID Type").thai("ประเภท ID").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        model: model,
        dataFixed: dataFixed,
        params: {
          partyType: partyType
        },
        url: "/pf/mastertable/id_types",
        propName: this.generatePropName("idType"),
        onChange: function onChange(value) {
          _this5.props.form.validateFields([_this5.generatePropName("idNo")], {
            force: true
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 221
        },
        __self: this
      }), isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        size: "large",
        form: form,
        model: model,
        propName: this.generatePropName("idNo"),
        required: true,
        transform: function transform() {
          return (_this5.getValueFromModel(_this5.generatePropName("idNo")) || "").toUpperCase();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No.").thai("หมายเลข.").getMessage(),
        rules: [{
          validator: function validator(rule, value, callback) {
            var idType = _this.getValueFromModel(_this.generatePropName("idType"));

            if (idType === _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ID_TYPE_IC && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isValidThaiIdNo(value) && currencyCode === "THB") {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else if (idType === "NRIC" && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isNricValid(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else if (idType === "FIN" && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isFinValid(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 240
        },
        __self: this
      }), !isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], {
        form: form,
        model: model,
        propName: this.generatePropName("idNo"),
        required: true,
        transform: function transform() {
          return (_this5.getValueFromModel(_this5.generatePropName("idNo")) || "").toUpperCase();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No.").thai("หมายเลข.").getMessage(),
        rules: [{
          validator: function validator(rule, value, callback) {
            var idType = _this.getValueFromModel(_this.generatePropName("idType"));

            if (idType === _common__WEBPACK_IMPORTED_MODULE_12__["Consts"].ID_TYPE_IC && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isValidThaiIdNo(value) && currencyCode === "THB") {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else if (idType === "NRIC" && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isNricValid(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else if (idType === "FIN" && !_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isFinValid(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage());
            } else {
              callback();
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 284
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_choice_search_list__WEBPACK_IMPORTED_MODULE_16__["default"], {
        type: "idNo",
        isNotSearh: isNotSearh,
        groupType: groupType,
        model: this.getValueFromModel(this.generatePropName("idNo")),
        ref: this.initIdNoRef,
        isValid: function isValid() {
          form.validateFields([_this5.generatePropName("idNo")], {
            force: true
          });
        },
        onSelectList: function onSelectList(item) {
          if (item && item.blur && item.blur === "blur") {
            _this5.setValueToModel(item.idNo, _this5.generatePropName("idNo"));

            _this5.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this5.generatePropName("idNo"), item.idNo));
          } else {
            _this5.namePage.setShowLabel(item.name);

            _this5.setFormValues(item);

            _this5.props.onSelectChange && _this5.props.onSelectChange(item);
          }

          form.validateFields([_this5.generatePropName("idNo")], {
            force: true
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 327
        },
        __self: this
      }))), !partyType && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        form: form,
        model: model,
        required: true,
        propName: this.generatePropName("idNo"),
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("ID Type / No.").thai("ID Type / No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 352
        },
        __self: this
      }));
    }
  }, {
    key: "_renderIndiName",
    value: function _renderIndiName() {
      var _this6 = this;

      var _this = this;

      var _this$props2 = this.props,
          model = _this$props2.model,
          form = _this$props2.form,
          isNotSearh = _this$props2.isNotSearh,
          dataFixed = _this$props2.dataFixed,
          _this$props2$groupTyp = _this$props2.groupType,
          groupType = _this$props2$groupTyp === void 0 ? "INDI" : _this$props2$groupTyp,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$props2, ["model", "form", "isNotSearh", "dataFixed", "groupType"]);

      var partyType = this.props.partyType || this.getValueFromModel(this.generatePropName("partyType"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_14__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Full Name").thai("ชื่อ-สกุล").my("နာမည်အပြည့်အစုံ").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 370
        },
        __self: this
      }, partyType === "INDI" && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_NSelect_filter__WEBPACK_IMPORTED_MODULE_15__["default"], {
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Title").thai("หัวเรื่อง").my("ခေါင်းစဉ်").getMessage(),
        form: form,
        tableName: "title",
        dataFixed: dataFixed,
        model: model,
        required: true,
        propName: this.generatePropName("title"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 380
        },
        __self: this
      }), isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        size: "large",
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
        propName: this.generatePropName("name"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 393
        },
        __self: this
      }), !isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
        propName: this.generatePropName("name"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 404
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_choice_search_list__WEBPACK_IMPORTED_MODULE_16__["default"], {
        ref: this.initNameRef,
        type: "name",
        isNotSearh: isNotSearh,
        groupType: groupType,
        onSelectList: function onSelectList(item) {
          if (item && item.blur && item.blur === "blur") {
            _this6.setValueToModel(item.name, _this6.generatePropName("name"));

            _this6.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this6.generatePropName("name"), item.name));
          } else {
            _this6.idNoPage.setShowLabel(item.idNo);

            _this6.setFormValues(item);

            _this6.props.onSelectChange && _this6.props.onSelectChange(item);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 414
        },
        __self: this
      })));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject(), COLOR_A)
      };
    }
  }, {
    key: "renderNameOrg",
    value: function renderNameOrg() {
      var _this7 = this;

      var _this$props3 = this.props,
          model = _this$props3.model,
          form = _this$props3.form,
          isNotSearh = _this$props3.isNotSearh,
          dataFixed = _this$props3.dataFixed,
          _this$props3$groupTyp = _this$props3.groupType,
          groupType = _this$props3$groupTyp === void 0 ? "INDI" : _this$props3$groupTyp,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_this$props3, ["model", "form", "isNotSearh", "dataFixed", "groupType"]);

      if (isNotSearh) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
          size: "large",
          form: form,
          model: model,
          label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
          propName: this.generatePropName("name"),
          required: true,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 461
          },
          __self: this
        });
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
        propName: this.generatePropName("name"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_choice_search_list__WEBPACK_IMPORTED_MODULE_16__["default"], {
        ref: this.initNameRef,
        type: "name",
        isNotSearh: isNotSearh,
        groupType: groupType,
        onSelectList: function onSelectList(item) {
          if (item && item.blur && item.blur === "blur") {
            _this7.setValueToModel(item.name, _this7.generatePropName("name"));

            _this7.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this7.generatePropName("name"), item.name));
          } else {
            _this7.idNoPage.setShowLabel(item.idNo);

            _this7.setFormValues(item);

            _this7.props.onSelectChange && _this7.props.onSelectChange(item);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 484
        },
        __self: this
      }));
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataId) {
      if (!!dataId) return "".concat(dataId, ".").concat(propName);
      return "".concat(this.props.dataId, ".").concat(propName);
    }
  }, {
    key: "generatePropNameLine",
    value: function generatePropNameLine(propName, dataId) {
      if (!!dataId) return "".concat(dataId, "-").concat(propName);
      return "".concat(this.props.dataId, "-").concat(propName);
    } // actions

  }, {
    key: "setFormValues",
    value: function setFormValues(item, isShow) {
      var _this8 = this;

      var extraArrObj = this.props.extraArrObj;
      if (!item) return;
      var arr = ["name", "title", "idType", "idNo", "mobile", "mobileNationCode", "email"].concat(extraArrObj || []);

      if (!this.props.notDob) {
        arr.push("dob");
      }

      arr.map(function (every) {
        _this8.setValueToModel(every === "name" ? item["name"] : item[every], _this8.generatePropName(every));

        _this8.props.form.getFieldDecorator([_this8.generatePropName(every)], {
          initialValue: ""
        });

        _this8.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, _this8.generatePropName(every), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, every)));
      });
      this.setValueToModel(lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "addressType"), this.generatePropName("addressType"));
      this.props.form.getFieldDecorator([this.generatePropName("addressType")], {
        initialValue: ""
      });
      this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.generatePropName("addressType"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "addressType")));

      if (this.props.countryCode && this.props.countryCode === lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address.countryCode")) {
        var _this$setValuesToMode, _this$props$form$setF2;

        this.setValuesToModel((_this$setValuesToMode = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$setValuesToMode, this.generatePropName("address"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$setValuesToMode, this.generatePropNameLine("address.street"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address.street")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$setValuesToMode, this.generatePropName("address.postalCode"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address.postalCode")), _this$setValuesToMode));
        this.props.form.getFieldDecorator([this.generatePropName("address")], {
          initialValue: ""
        });
        this.props.form.getFieldDecorator([this.generatePropName("address.street")], {
          initialValue: ""
        });
        this.props.form.getFieldDecorator([this.generatePropName("address.postalCode")], {
          initialValue: ""
        });
        this.props.form.setFieldsValue((_this$props$form$setF2 = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props$form$setF2, this.generatePropName("address"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props$form$setF2, this.generatePropName("address.street"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address.street")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props$form$setF2, this.generatePropName("address.postalCode"), lodash__WEBPACK_IMPORTED_MODULE_17___default.a.get(item, "address.postalCode")), _this$props$form$setF2));
      }

      if (isShow) {
        this.props.onChangeAddress && this.props.onChangeAddress();
      }
    }
  }]);

  return GroupCustomer;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

GroupCustomer.defaultProps = {
  notDob: false
};
/* harmony default export */ __webpack_exports__["default"] = (GroupCustomer);
var OcrMobileStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject2());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2dyb3VwLWN1c3RvbWVyLWNvbXBhbnkvZ3JvdXAtY3VzdG9tZXIudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2dyb3VwLWN1c3RvbWVyLWNvbXBhbnkvZ3JvdXAtY3VzdG9tZXIudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQWpheCwgQ29uc3RzLCBMYW5ndWFnZSwgUnVsZXMsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5UZXh0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgRmllbGRHcm91cCBmcm9tIFwiLi4vZmllbGQtZ3JvdXBcIjtcbmltcG9ydCBOU2VsZWN0IGZyb20gXCIuL05TZWxlY3QtZmlsdGVyXCI7XG5pbXBvcnQgQ2hvaWNlU2VhcmNoTGlzdCBmcm9tIFwiLi9jaG9pY2Utc2VhcmNoLWxpc3RcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCBPY3IgZnJvbSBcIi4uL3BoL29jclwiO1xuaW1wb3J0IE1hc2sgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9jcmVhdGUtZGlhbG9nXCI7XG5pbXBvcnQgRWt5Y0lkZW50aWZ5IGZyb20gXCJAZGVzay9la3ljL3VwbG9hZFwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzXCI7XG5cbmludGVyZmFjZSBJRFR5cGVOYW1lU3RhdGUge1xuICBreWNTdGF0dXM6IGFueTtcbn1cblxuY29uc3QgeyBDT0xPUl9BIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xudHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcbnR5cGUgSURUeXBlTmFtZVBhZ2VDb21wb25lbnRzID0ge1xuICBCb3hDb250ZW50OiBTdHlsZWRESVY7XG59O1xuZXhwb3J0IHR5cGUgSURUeXBlTmFtZVByb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBmb3JtOiBhbnk7XG4gIGRhdGFJZDogYW55O1xuICBkYXRhRml4ZWQ/OiBhbnk7XG4gIGlzTm90U2Vhcmg/OiBhbnk7XG4gIGNvdW50cnlDb2RlPzogc3RyaW5nO1xuICBub3REb2I/OiBib29sZWFuO1xuICBvblNlbGVjdENoYW5nZT86IGFueTtcbiAgZ3JvdXBUeXBlPzogYW55O1xuICBvbkNoYW5nZUFkZHJlc3M/OiBhbnk7XG4gIHBhcnR5VHlwZT86IFwiSU5ESVwiIHwgXCJPUkdcIiB8IHVuZGVmaW5lZDtcbiAgZXh0cmFBcnJPYmo/OiBhbnlbXTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5jbGFzcyBHcm91cEN1c3RvbWVyPFAgZXh0ZW5kcyBJRFR5cGVOYW1lUHJvcHMsXG4gIFMgZXh0ZW5kcyBJRFR5cGVOYW1lU3RhdGUsXG4gIEMgZXh0ZW5kcyBJRFR5cGVOYW1lUGFnZUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIG5vdERvYjogZmFsc2UsXG4gIH07XG4gIHB1YmxpYyBpZE5vUGFnZTogYW55O1xuICBwdWJsaWMgbmFtZVBhZ2U6IGFueTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogSURUeXBlTmFtZVByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5zaG93TmFtZUlkTm8oKTtcbiAgfVxuXG4gIHNob3dOYW1lSWRObygpIHtcbiAgICB0aGlzLmlkTm9QYWdlICYmIHRoaXMuaWROb1BhZ2Uuc2V0U2hvd0xhYmVsKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWROb1wiKSkpO1xuICAgIHRoaXMubmFtZVBhZ2UgJiYgdGhpcy5uYW1lUGFnZS5zZXRTaG93TGFiZWwodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpKSk7XG4gIH1cblxuICBpc0JlZm9yZVZhbHVlKCkge1xuICAgIHJldHVybiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIikpXG4gICAgICB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm5hbWVcIikpO1xuICB9XG5cbiAgaW5pdElkTm9SZWYgPSAocmVmOiBhbnkpID0+IHtcbiAgICB0aGlzLmlkTm9QYWdlID0gcmVmO1xuICB9O1xuXG4gIGluaXROYW1lUmVmID0gKHJlZjogYW55KSA9PiB7XG4gICAgdGhpcy5uYW1lUGFnZSA9IHJlZjtcbiAgfTtcblxuICBwb3N0RGF0YSgpIHtcbiAgICBjb25zdCB7IG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiB7XG4gICAgICBiaXpJZDogXy5nZXQobW9kZWwsIFwicG9saWN5SWRcIikgfHwgXy5nZXQobW9kZWwsIFwicG9saWN5LnBvbGljeUlkXCIpLFxuICAgICAgYml6VHlwZTogXy5nZXQobW9kZWwsIFwiYml6VHlwZVwiKSB8fCBfLmdldChtb2RlbCwgXCJwb2xpY3kuYml6VHlwZVwiKSxcbiAgICAgIGl0bnRDb2RlOiBfLmdldChtb2RlbCwgXCJpdG50Q29kZVwiKSB8fCBfLmdldChtb2RlbCwgXCJwb2xpY3kuaXRudENvZGVcIiksXG4gICAgfTtcbiAgfVxuXG4gIGdldEt5Y1N0YXR1cygpIHtcbiAgICBBamF4LnBvc3QoYC9reWMvc3RhdHVzYCwgdGhpcy5wb3N0RGF0YSgpLCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgIC50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgICBjb25zdCByZXNwRGF0YSA9IChyZXMuYm9keSB8fCB7fSkucmVzcERhdGE7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGt5Y1N0YXR1czogcmVzcERhdGEsXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gIH1cblxuICByZW5kZXJJbmRpTmFtZVdpdGhPY3IoKSB7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwcm9kdWN0Q29kZSA9IF8uZ2V0KG1vZGVsLCBcInBvbGljeS5wcm9kdWN0Q29kZVwiKSB8fCBfLmdldChtb2RlbCwgXCJwcm9kdWN0Q29kZVwiKTtcbiAgICBjb25zdCB7IGt5Y1N0YXR1cyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBreWNTdGF0dXNPYmogPSB7XG4gICAgICBQRU5ESU5HOiBcIiNlNjg2M2NcIixcbiAgICAgIFNVQ0NFU1M6IFwiZ3JlZW5cIixcbiAgICAgIEZBSUxFRDogXCJmZjAwMDBcIixcbiAgICB9O1xuICAgIGlmIChVdGlscy5nZXRJc01vYmlsZSgpKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8PlxuICAgICAgICAgIDxPY3JNb2JpbGVTdHlsZT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcIm9jci1tb2JpbGVcIn0+XG4gICAgICAgICAgICAgIDxpIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsIH06IGFueSkgPT5cbiAgICAgICAgICAgICAgICAgICAgPE9jciBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgb25Paz17KGRhdGE6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRGb3JtVmFsdWVzKGRhdGEsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9saWN5SWQ6IF8uZ2V0KG1vZGVsLCBcInBvbGljeUlkXCIpIHx8IF8uZ2V0KG1vZGVsLCBcInBvbGljeS5wb2xpY3lJZFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICB9KX0vPixcbiAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9fSBjbGFzc05hbWU9e1wiaWNvbmZvbnQgaWNvbi1vY3JcIn0vPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9PY3JNb2JpbGVTdHlsZT5cbiAgICAgICAgICB7dGhpcy5fcmVuZGVySW5kaU5hbWUoKX1cbiAgICAgICAgPC8+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICA8PlxuICAgICAgICB7dGhpcy5fcmVuZGVySW5kaU5hbWUoKX1cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wib2NyXCJ9PlxuICAgICAgICAgIDxpIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIE1hc2suY3JlYXRlKHtcbiAgICAgICAgICAgICAgQ29tcG9uZW50OiAoeyBvbkNhbmNlbCB9OiBhbnkpID0+XG4gICAgICAgICAgICAgICAgPE9jciBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgICAgICAgICAgICAgICBvbk9rPXsoZGF0YTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0Rm9ybVZhbHVlcyhkYXRhLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgb25DYW5jZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICBtb2RlbD17TW9kZWxsZXIuYXNQcm94aWVkKHtcbiAgICAgICAgICAgICAgICAgICAgICAgcG9saWN5SWQ6IF8uZ2V0KG1vZGVsLCBcInBvbGljeUlkXCIpIHx8IF8uZ2V0KG1vZGVsLCBcInBvbGljeS5wb2xpY3lJZFwiKSxcbiAgICAgICAgICAgICAgICAgICAgIH0pfS8+LFxuICAgICAgICAgICAgICBvbkNhbmNlbDogKCkgPT4ge1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfX0gY2xhc3NOYW1lPXtcImljb25mb250IGljb24tb2NyXCJ9Lz5cbiAgICAgICAgICB7cHJvZHVjdENvZGUgIT09IFwiUEhTXCIgJiYgPGkgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgaWYgKGt5Y1N0YXR1cyA9PT0gXCJTVUNDRVNTXCIpIHtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgTWFzay5jcmVhdGUoe1xuICAgICAgICAgICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsIH06IGFueSkgPT5cbiAgICAgICAgICAgICAgICA8RWt5Y0lkZW50aWZ5XG4gICAgICAgICAgICAgICAgICBvbkNhbmNlbD17KGRhdGE6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNCZWZvcmVWYWx1ZSgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRGb3JtVmFsdWVzKGRhdGEsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsKCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0S3ljU3RhdHVzKCk7XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgbW9kZWw9e01vZGVsbGVyLmFzUHJveGllZCh0aGlzLnBvc3REYXRhKCkpfS8+LFxuICAgICAgICAgICAgICBvbkNhbmNlbDogKCkgPT4ge1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfX0gY2xhc3NOYW1lPXtcImljb25mb250IGljb24taWRlbnRpZnlcIn0gc3R5bGU9e3sgY29sb3I6IGt5Y1N0YXR1cyA/IGt5Y1N0YXR1c09ialtreWNTdGF0dXNdIDogXCIjZTY4NjNjXCIgfX0vPn1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8Lz5cbiAgICApO1xuXG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3Qga3ljU3RhdHVzID0gXy5nZXQobW9kZWwsIFwia3ljU3RhdHVzXCIpIHx8IF8uZ2V0KG1vZGVsLCBcInBvbGljeS5reWNTdGF0dXNcIik7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIGt5Y1N0YXR1czoga3ljU3RhdHVzLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICAvL3JlbmRlcnNcbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCBfdGhpcyA9IHRoaXM7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgaXNOb3RTZWFyaCwgZGF0YUZpeGVkLCBncm91cFR5cGUgPSBcIklORElcIiwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBjdXJyZW5jeUNvZGUgPSBfLmdldChtb2RlbCwgXCJjdXJyZW5jeUNvZGVcIik7XG4gICAgY29uc3QgcGFydHlUeXBlID0gdGhpcy5wcm9wcy5wYXJ0eVR5cGUgfHwgdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwYXJ0eVR5cGVcIikpO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hDb250ZW50IHsuLi5yZXN0fT5cbiAgICAgICAge3BhcnR5VHlwZSA9PT0gXCJJTkRJXCIgJiYgdGhpcy5yZW5kZXJJbmRpTmFtZVdpdGhPY3IoKX1cbiAgICAgICAge3BhcnR5VHlwZSAhPT0gXCJJTkRJXCIgJiYgdGhpcy5yZW5kZXJOYW1lT3JnKCl9XG5cbiAgICAgICAge3BhcnR5VHlwZSAmJiA8RmllbGRHcm91cFxuICAgICAgICAgIGNsYXNzTmFtZT1cInVzYWdlLWdyb3VwXCJcbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJJRCBUeXBlIC8gTm8uXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRCAvIOC4q+C4oeC4suC4ouC5gOC4peC4gi5cIilcbiAgICAgICAgICAgIC5teShcIuGAoeGAreGAr+GAhOGAuuGAkuGAruGAoeGAmeGAu+GAreGAr+GAuOGAoeGAheGArOGAuCAvIE5PIOGBi1wiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fVxuICAgICAgICAgIHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19XG4gICAgICAgICAgbWluV2lkdGg9XCIxNDBweFwiXG4gICAgICAgID5cbiAgICAgICAgICB7cGFydHlUeXBlID09PSBcIklORElcIiAmJiA8TlNlbGVjdFxuICAgICAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgVHlwZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRFwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgK3hgK/hgIThgLrhgJLhgK7hgKHhgJnhgLvhgK3hgK/hgLjhgKHhgIXhgKzhgLggLyBOTyDhgYtcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIHRhYmxlTmFtZT17XCJpZHR5cGVcIn1cbiAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkVHlwZVwiKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWU6IGFueSk6IHZvaWQgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAvPn1cbiAgICAgICAgICB7cGFydHlUeXBlID09PSBcIk9SR1wiICYmIDxOU2VsZWN0XG4gICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICBpc1BGPXt0cnVlfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgVHlwZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRFwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgK3hgK/hgIThgLrhgJLhgK7hgKHhgJnhgLvhgK3hgK/hgLjhgKHhgIXhgKzhgLggLyBOTyDhgYtcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIGRhdGFGaXhlZD17ZGF0YUZpeGVkfVxuICAgICAgICAgICAgcGFyYW1zPXt7IHBhcnR5VHlwZTogcGFydHlUeXBlIH19XG4gICAgICAgICAgICB1cmw9e2AvcGYvbWFzdGVydGFibGUvaWRfdHlwZXNgfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkVHlwZVwiKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWU6IGFueSk6IHZvaWQgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIH1cbiAgICAgICAgICB7aXNOb3RTZWFyaCAmJiA8TlRleHRcbiAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWROb1wiKX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgdHJhbnNmb3JtPXsoKTogYW55ID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuICh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIikpIHx8IFwiXCIpLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgTm8uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiCLlwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgcnVsZXM9e1tcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgIGxldCBpZFR5cGUgPSBfdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChfdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWRUeXBlXCIpKTtcbiAgICAgICAgICAgICAgICAgIGlmICgoaWRUeXBlID09PSBDb25zdHMuSURfVFlQRV9JQyAmJiAhUnVsZXMuaXNWYWxpZFRoYWlJZE5vKHZhbHVlKSAmJiBjdXJyZW5jeUNvZGUgPT09IFwiVEhCXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFxuICAgICAgICAgICAgICAgICAgICAgIExhbmd1YWdlLmVuKFwiSUQgTm8uIGlzIGludmFsaWRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiC4Lij4Lir4Lix4Liq4LmE4Lih4LmI4LiW4Li54LiB4LiV4LmJ4Lit4LiHXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAubXkoXCJJRCDhgIDhgK3hgK/hgJThgLbhgJXhgKvhgJDhgLrhgJzhgK3hgK/hgKHhgJXhgLrhgJ7hgIrhgLpcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKChpZFR5cGUgPT09IFwiTlJJQ1wiICYmICFSdWxlcy5pc05yaWNWYWxpZCh2YWx1ZSkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFxuICAgICAgICAgICAgICAgICAgICAgIExhbmd1YWdlLmVuKFwiSUQgTm8uIGlzIGludmFsaWRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiC4Lij4Lir4Lix4Liq4LmE4Lih4LmI4LiW4Li54LiB4LiV4LmJ4Lit4LiHXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAubXkoXCJJRCDhgIDhgK3hgK/hgJThgLbhgJXhgKvhgJDhgLrhgJzhgK3hgK/hgKHhgJXhgLrhgJ7hgIrhgLpcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKChpZFR5cGUgPT09IFwiRklOXCIgJiYgIVJ1bGVzLmlzRmluVmFsaWQodmFsdWUpKSkge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhcbiAgICAgICAgICAgICAgICAgICAgICBMYW5ndWFnZS5lbihcIklEIE5vLiBpcyBpbnZhbGlkXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4q+C4oeC4suC4ouC5gOC4peC4guC4o+C4q+C4seC4quC5hOC4oeC5iOC4luC4ueC4geC4leC5ieC4reC4h1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLm15KFwiSUQg4YCA4YCt4YCv4YCU4YC24YCV4YCr4YCQ4YC64YCc4YCt4YCv4YCh4YCV4YC64YCe4YCK4YC6XCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXX1cbiAgICAgICAgICAvPn1cbiAgICAgICAgICB7IWlzTm90U2VhcmggJiYgPE5Gb3JtSXRlbVxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZE5vXCIpfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICB0cmFuc2Zvcm09eygpOiBhbnkgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWROb1wiKSkgfHwgXCJcIikudG9VcHBlckNhc2UoKTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJJRCBOby5cIilcbiAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguIIuXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFsaWRhdG9yKHJ1bGU6IGFueSwgdmFsdWU6IGFueSwgY2FsbGJhY2s6IGFueSkge1xuICAgICAgICAgICAgICAgICAgbGV0IGlkVHlwZSA9IF90aGlzLmdldFZhbHVlRnJvbU1vZGVsKF90aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZFR5cGVcIikpO1xuICAgICAgICAgICAgICAgICAgaWYgKChpZFR5cGUgPT09IENvbnN0cy5JRF9UWVBFX0lDICYmICFSdWxlcy5pc1ZhbGlkVGhhaUlkTm8odmFsdWUpICYmIGN1cnJlbmN5Q29kZSA9PT0gXCJUSEJcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soXG4gICAgICAgICAgICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJJRCBOby4gaXMgaW52YWxpZFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguKPguKvguLHguKrguYTguKHguYjguJbguLnguIHguJXguYnguK3guIdcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5teShcIklEIOGAgOGAreGAr+GAlOGAtuGAleGAq+GAkOGAuuGAnOGAreGAr+GAoeGAleGAuuGAnuGAiuGAulwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoKGlkVHlwZSA9PT0gXCJOUklDXCIgJiYgIVJ1bGVzLmlzTnJpY1ZhbGlkKHZhbHVlKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soXG4gICAgICAgICAgICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJJRCBOby4gaXMgaW52YWxpZFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguKPguKvguLHguKrguYTguKHguYjguJbguLnguIHguJXguYnguK3guIdcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5teShcIklEIOGAgOGAreGAr+GAlOGAtuGAleGAq+GAkOGAuuGAnOGAreGAr+GAoeGAleGAuuGAnuGAiuGAulwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoKGlkVHlwZSA9PT0gXCJGSU5cIiAmJiAhUnVsZXMuaXNGaW5WYWxpZCh2YWx1ZSkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFxuICAgICAgICAgICAgICAgICAgICAgIExhbmd1YWdlLmVuKFwiSUQgTm8uIGlzIGludmFsaWRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiC4Lij4Lir4Lix4Liq4LmE4Lih4LmI4LiW4Li54LiB4LiV4LmJ4Lit4LiHXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAubXkoXCJJRCDhgIDhgK3hgK/hgJThgLbhgJXhgKvhgJDhgLrhgJzhgK3hgK/hgKHhgJXhgLrhgJ7hgIrhgLpcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxDaG9pY2VTZWFyY2hMaXN0XG4gICAgICAgICAgICAgIHR5cGU9e1wiaWROb1wifVxuICAgICAgICAgICAgICBpc05vdFNlYXJoPXtpc05vdFNlYXJofVxuICAgICAgICAgICAgICBncm91cFR5cGU9e2dyb3VwVHlwZX1cbiAgICAgICAgICAgICAgbW9kZWw9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaWROb1wiKSl9XG4gICAgICAgICAgICAgIHJlZj17dGhpcy5pbml0SWROb1JlZn1cbiAgICAgICAgICAgICAgaXNWYWxpZD17KCkgPT4ge1xuICAgICAgICAgICAgICAgIGZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvblNlbGVjdExpc3Q9eyhpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoaXRlbSAmJiBpdGVtLmJsdXIgJiYgaXRlbS5ibHVyID09PSBcImJsdXJcIikge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbS5pZE5vLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZE5vXCIpKTtcbiAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7XG4gICAgICAgICAgICAgICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZE5vXCIpXTogaXRlbS5pZE5vLFxuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMubmFtZVBhZ2Uuc2V0U2hvd0xhYmVsKGl0ZW0ubmFtZSk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnNldEZvcm1WYWx1ZXMoaXRlbSk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uU2VsZWN0Q2hhbmdlICYmIHRoaXMucHJvcHMub25TZWxlY3RDaGFuZ2UoaXRlbSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlkTm9cIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L05Gb3JtSXRlbT59XG4gICAgICAgIDwvRmllbGRHcm91cD59XG4gICAgICAgIHshcGFydHlUeXBlICYmIDxOVGV4dFxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZE5vXCIpfVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIklEIFR5cGUgLyBOby5cIilcbiAgICAgICAgICAgIC50aGFpKFwiSUQgVHlwZSAvIE5vLlwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgLz59XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG5cbiAgX3JlbmRlckluZGlOYW1lKCkge1xuICAgIGNvbnN0IF90aGlzID0gdGhpcztcbiAgICBjb25zdCB7IG1vZGVsLCBmb3JtLCBpc05vdFNlYXJoLCBkYXRhRml4ZWQsIGdyb3VwVHlwZSA9IFwiSU5ESVwiLCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHBhcnR5VHlwZSA9IHRoaXMucHJvcHMucGFydHlUeXBlIHx8IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGFydHlUeXBlXCIpKTtcbiAgICByZXR1cm4gKFxuICAgICAgPEZpZWxkR3JvdXBcbiAgICAgICAgY2xhc3NOYW1lPVwidXNhZ2UtZ3JvdXBcIlxuICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJGdWxsIE5hbWVcIilcbiAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rS3guKrguIHguLjguKVcIilcbiAgICAgICAgICAubXkoXCLhgJThgKzhgJnhgIrhgLrhgKHhgJXhgLzhgIrhgLrhgLfhgKHhgIXhgK/hgLZcIilcbiAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fVxuICAgICAgICB0ZXh0WHNTbT17eyB4czogMTYsIHNtOiAxMyB9fVxuICAgICAgICBtaW5XaWR0aD1cIjE0MHB4XCJcbiAgICAgID5cbiAgICAgICAge3BhcnR5VHlwZSA9PT0gXCJJTkRJXCIgJiYgPE5TZWxlY3RcbiAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVGl0bGVcIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lir4Lix4Lin4LmA4Lij4Li34LmI4Lit4LiHXCIpXG4gICAgICAgICAgICAubXkoXCLhgIHhgLHhgKvhgIThgLrhgLjhgIXhgInhgLpcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICB0YWJsZU5hbWU9XCJ0aXRsZVwiXG4gICAgICAgICAgZGF0YUZpeGVkPXtkYXRhRml4ZWR9XG4gICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ0aXRsZVwiKX1cbiAgICAgICAgLz59XG4gICAgICAgIHtpc05vdFNlYXJoICYmIDxOVGV4dFxuICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJOYW1lXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rVwiKVxuICAgICAgICAgICAgLm15KFwi4YCU4YCs4YCZ4YCQ4YC74YCx4YCs4YCA4YCt4YCvXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpfVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAvPn1cbiAgICAgICAgeyFpc05vdFNlYXJoICYmIDxORm9ybUl0ZW1cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJOYW1lXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rVwiKVxuICAgICAgICAgICAgLm15KFwi4YCU4YCs4YCZ4YCQ4YC74YCx4YCs4YCA4YCt4YCvXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpfVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICA+XG4gICAgICAgICAgPENob2ljZVNlYXJjaExpc3RcbiAgICAgICAgICAgIHJlZj17dGhpcy5pbml0TmFtZVJlZn1cbiAgICAgICAgICAgIHR5cGU9e1wibmFtZVwifVxuICAgICAgICAgICAgaXNOb3RTZWFyaD17aXNOb3RTZWFyaH1cbiAgICAgICAgICAgIGdyb3VwVHlwZT17Z3JvdXBUeXBlfVxuICAgICAgICAgICAgb25TZWxlY3RMaXN0PXsoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgICAgIGlmIChpdGVtICYmIGl0ZW0uYmx1ciAmJiBpdGVtLmJsdXIgPT09IFwiYmx1clwiKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbS5uYW1lLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpKTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoe1xuICAgICAgICAgICAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm5hbWVcIildOiBpdGVtLm5hbWUsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pZE5vUGFnZS5zZXRTaG93TGFiZWwoaXRlbS5pZE5vKTtcbiAgICAgICAgICAgICAgICB0aGlzLnNldEZvcm1WYWx1ZXMoaXRlbSk7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vblNlbGVjdENoYW5nZSAmJiB0aGlzLnByb3BzLm9uU2VsZWN0Q2hhbmdlKGl0ZW0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvTkZvcm1JdGVtPn1cbiAgICAgIDwvRmllbGRHcm91cD5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgICAgIC5vY3Ige1xuICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IGNhbGMoODAlICsgMTBweCk7XG4gICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtNjVweDtcbiAgICAgICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICAgICAgICAgICAgICAgICYuaWNvbi1pZGVudGlmeSB7XG4gICAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAke0NPTE9SX0F9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICB9XG4gICAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcmVuZGVyTmFtZU9yZygpIHtcbiAgICBjb25zdCB7IG1vZGVsLCBmb3JtLCBpc05vdFNlYXJoLCBkYXRhRml4ZWQsIGdyb3VwVHlwZSA9IFwiSU5ESVwiLCAuLi5yZXN0IH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChpc05vdFNlYXJoKSB7XG4gICAgICByZXR1cm4gPE5UZXh0XG4gICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJOYW1lXCIpXG4gICAgICAgICAgLnRoYWkoXCLguIrguLfguYjguK1cIilcbiAgICAgICAgICAubXkoXCLhgJThgKzhgJnhgJDhgLvhgLHhgKzhgIDhgK3hgK9cIilcbiAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibmFtZVwiKX1cbiAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAvPjtcbiAgICB9XG5cbiAgICByZXR1cm4gPE5Gb3JtSXRlbVxuICAgICAgZm9ybT17Zm9ybX1cbiAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk5hbWVcIilcbiAgICAgICAgLnRoYWkoXCLguIrguLfguYjguK1cIilcbiAgICAgICAgLm15KFwi4YCU4YCs4YCZ4YCQ4YC74YCx4YCs4YCA4YCt4YCvXCIpXG4gICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibmFtZVwiKX1cbiAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgID5cbiAgICAgIDxDaG9pY2VTZWFyY2hMaXN0XG4gICAgICAgIHJlZj17dGhpcy5pbml0TmFtZVJlZn1cbiAgICAgICAgdHlwZT17XCJuYW1lXCJ9XG4gICAgICAgIGlzTm90U2Vhcmg9e2lzTm90U2Vhcmh9XG4gICAgICAgIGdyb3VwVHlwZT17Z3JvdXBUeXBlfVxuICAgICAgICBvblNlbGVjdExpc3Q9eyhpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICBpZiAoaXRlbSAmJiBpdGVtLmJsdXIgJiYgaXRlbS5ibHVyID09PSBcImJsdXJcIikge1xuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbS5uYW1lLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpKTtcbiAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7XG4gICAgICAgICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpXTogaXRlbS5uYW1lLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuaWROb1BhZ2Uuc2V0U2hvd0xhYmVsKGl0ZW0uaWRObyk7XG4gICAgICAgICAgICB0aGlzLnNldEZvcm1WYWx1ZXMoaXRlbSk7XG4gICAgICAgICAgICB0aGlzLnByb3BzLm9uU2VsZWN0Q2hhbmdlICYmIHRoaXMucHJvcHMub25TZWxlY3RDaGFuZ2UoaXRlbSk7XG4gICAgICAgICAgfVxuICAgICAgICB9fVxuICAgICAgLz5cbiAgICA8L05Gb3JtSXRlbT47XG5cbiAgfVxuXG4gIHByb3RlY3RlZCBnZW5lcmF0ZVByb3BOYW1lKHByb3BOYW1lOiBzdHJpbmcsIGRhdGFJZD86IHN0cmluZykge1xuICAgIGlmICghIWRhdGFJZCkgcmV0dXJuIGAke2RhdGFJZH0uJHtwcm9wTmFtZX1gO1xuICAgIHJldHVybiBgJHt0aGlzLnByb3BzLmRhdGFJZH0uJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdlbmVyYXRlUHJvcE5hbWVMaW5lKHByb3BOYW1lOiBzdHJpbmcsIGRhdGFJZD86IHN0cmluZykge1xuICAgIGlmICghIWRhdGFJZCkgcmV0dXJuIGAke2RhdGFJZH0tJHtwcm9wTmFtZX1gO1xuICAgIHJldHVybiBgJHt0aGlzLnByb3BzLmRhdGFJZH0tJHtwcm9wTmFtZX1gO1xuICB9XG5cbiAgLy8gYWN0aW9uc1xuICBzZXRGb3JtVmFsdWVzKGl0ZW06IGFueSwgaXNTaG93PzogYW55KSB7XG4gICAgY29uc3QgeyBleHRyYUFyck9iaiB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoIWl0ZW0pIHJldHVybjtcbiAgICBsZXQgYXJyID0gW1wibmFtZVwiLCBcInRpdGxlXCIsIFwiaWRUeXBlXCIsIFwiaWROb1wiLCBcIm1vYmlsZVwiLCBcIm1vYmlsZU5hdGlvbkNvZGVcIiwgXCJlbWFpbFwiXS5jb25jYXQoKGV4dHJhQXJyT2JqIHx8IFtdKSBhcyBhbnkpO1xuICAgIGlmICghdGhpcy5wcm9wcy5ub3REb2IpIHtcbiAgICAgIGFyci5wdXNoKFwiZG9iXCIpO1xuICAgIH1cbiAgICBhcnIubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChldmVyeSA9PT0gXCJuYW1lXCIgPyBpdGVtW1wibmFtZVwiXSA6IGl0ZW1bZXZlcnldLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoZXZlcnkpKTtcbiAgICAgIHRoaXMucHJvcHMuZm9ybS5nZXRGaWVsZERlY29yYXRvcihbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGV2ZXJ5KV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShldmVyeSldOiBfLmdldChpdGVtLCBldmVyeSkgfSk7XG4gICAgfSk7XG4gICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXy5nZXQoaXRlbSwgXCJhZGRyZXNzVHlwZVwiKSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1R5cGVcIikpO1xuICAgIHRoaXMucHJvcHMuZm9ybS5nZXRGaWVsZERlY29yYXRvcihbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1R5cGVcIildLCB7IGluaXRpYWxWYWx1ZTogXCJcIiB9KTtcbiAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoe1xuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NUeXBlXCIpXTogXy5nZXQoaXRlbSwgXCJhZGRyZXNzVHlwZVwiKSxcbiAgICB9KTtcbiAgICBpZiAodGhpcy5wcm9wcy5jb3VudHJ5Q29kZSAmJiAodGhpcy5wcm9wcy5jb3VudHJ5Q29kZSA9PT0gXy5nZXQoaXRlbSwgXCJhZGRyZXNzLmNvdW50cnlDb2RlXCIpKSkge1xuICAgICAgdGhpcy5zZXRWYWx1ZXNUb01vZGVsKHtcbiAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NcIildOiBfLmdldChpdGVtLCBcImFkZHJlc3NcIiksXG4gICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWVMaW5lKFwiYWRkcmVzcy5zdHJlZXRcIildOiBfLmdldChpdGVtLCBcImFkZHJlc3Muc3RyZWV0XCIpLFxuICAgICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzcy5wb3N0YWxDb2RlXCIpXTogXy5nZXQoaXRlbSwgXCJhZGRyZXNzLnBvc3RhbENvZGVcIiksXG4gICAgICB9KTtcbiAgICAgIHRoaXMucHJvcHMuZm9ybS5nZXRGaWVsZERlY29yYXRvcihbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzc1wiKV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLmdldEZpZWxkRGVjb3JhdG9yKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnN0cmVldFwiKV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLmdldEZpZWxkRGVjb3JhdG9yKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnBvc3RhbENvZGVcIildLCB7IGluaXRpYWxWYWx1ZTogXCJcIiB9KTtcblxuICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHtcbiAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NcIildOiBfLmdldChpdGVtLCBcImFkZHJlc3NcIiksXG4gICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnN0cmVldFwiKV06IF8uZ2V0KGl0ZW0sIFwiYWRkcmVzcy5zdHJlZXRcIiksXG4gICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnBvc3RhbENvZGVcIildOiBfLmdldChpdGVtLCBcImFkZHJlc3MucG9zdGFsQ29kZVwiKSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoaXNTaG93KSB7XG4gICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlQWRkcmVzcyAmJiB0aGlzLnByb3BzLm9uQ2hhbmdlQWRkcmVzcygpO1xuICAgIH1cbiAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IEdyb3VwQ3VzdG9tZXI7XG5cbmNvbnN0IE9jck1vYmlsZVN0eWxlID0gU3R5bGVkLmRpdmBcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgIC5vY3ItbW9iaWxlIHtcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IGNhbGMoMjUlICsgMTBweCk7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICBpIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgei1pbmRleDogOTk5O1xuICAgICB9XG5gO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBQUE7QUFDQTtBQW1CQTs7Ozs7QUFTQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQW1CQTtBQUNBO0FBQ0E7QUFyQkE7QUF1QkE7QUFDQTtBQUNBO0FBekJBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUVBOzs7QUFVQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVNBO0FBVkE7QUFhQTtBQUFBO0FBZEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBb0JBO0FBQ0E7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBU0E7QUFWQTtBQWFBO0FBQUE7QUFkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBVUE7QUFYQTtBQWNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBdUJBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQWRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBTUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBM0JBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBTUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBM0JBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQXJCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBcUJBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQWtCQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBaEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW9CQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBOWZBO0FBQ0E7QUFIQTtBQUlBO0FBREE7QUFpZ0JBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/group-customer-company/group-customer.tsx
