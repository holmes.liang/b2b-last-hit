// TODO Draggable for group
// FIXME Draggable on element which has parent rotation or scale
function Draggable() {
  this.on('mousedown', this._dragStart, this);
  this.on('mousemove', this._drag, this);
  this.on('mouseup', this._dragEnd, this);
  this.on('globalout', this._dragEnd, this); // this._dropTarget = null;
  // this._draggingTarget = null;
  // this._x = 0;
  // this._y = 0;
}

Draggable.prototype = {
  constructor: Draggable,
  _dragStart: function _dragStart(e) {
    var draggingTarget = e.target;

    if (draggingTarget && draggingTarget.draggable) {
      this._draggingTarget = draggingTarget;
      draggingTarget.dragging = true;
      this._x = e.offsetX;
      this._y = e.offsetY;
      this.dispatchToElement(param(draggingTarget, e), 'dragstart', e.event);
    }
  },
  _drag: function _drag(e) {
    var draggingTarget = this._draggingTarget;

    if (draggingTarget) {
      var x = e.offsetX;
      var y = e.offsetY;
      var dx = x - this._x;
      var dy = y - this._y;
      this._x = x;
      this._y = y;
      draggingTarget.drift(dx, dy, e);
      this.dispatchToElement(param(draggingTarget, e), 'drag', e.event);
      var dropTarget = this.findHover(x, y, draggingTarget).target;
      var lastDropTarget = this._dropTarget;
      this._dropTarget = dropTarget;

      if (draggingTarget !== dropTarget) {
        if (lastDropTarget && dropTarget !== lastDropTarget) {
          this.dispatchToElement(param(lastDropTarget, e), 'dragleave', e.event);
        }

        if (dropTarget && dropTarget !== lastDropTarget) {
          this.dispatchToElement(param(dropTarget, e), 'dragenter', e.event);
        }
      }
    }
  },
  _dragEnd: function _dragEnd(e) {
    var draggingTarget = this._draggingTarget;

    if (draggingTarget) {
      draggingTarget.dragging = false;
    }

    this.dispatchToElement(param(draggingTarget, e), 'dragend', e.event);

    if (this._dropTarget) {
      this.dispatchToElement(param(this._dropTarget, e), 'drop', e.event);
    }

    this._draggingTarget = null;
    this._dropTarget = null;
  }
};

function param(target, e) {
  return {
    target: target,
    topTarget: e && e.topTarget
  };
}

var _default = Draggable;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvbWl4aW4vRHJhZ2dhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvbWl4aW4vRHJhZ2dhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRPRE8gRHJhZ2dhYmxlIGZvciBncm91cFxuLy8gRklYTUUgRHJhZ2dhYmxlIG9uIGVsZW1lbnQgd2hpY2ggaGFzIHBhcmVudCByb3RhdGlvbiBvciBzY2FsZVxuZnVuY3Rpb24gRHJhZ2dhYmxlKCkge1xuICB0aGlzLm9uKCdtb3VzZWRvd24nLCB0aGlzLl9kcmFnU3RhcnQsIHRoaXMpO1xuICB0aGlzLm9uKCdtb3VzZW1vdmUnLCB0aGlzLl9kcmFnLCB0aGlzKTtcbiAgdGhpcy5vbignbW91c2V1cCcsIHRoaXMuX2RyYWdFbmQsIHRoaXMpO1xuICB0aGlzLm9uKCdnbG9iYWxvdXQnLCB0aGlzLl9kcmFnRW5kLCB0aGlzKTsgLy8gdGhpcy5fZHJvcFRhcmdldCA9IG51bGw7XG4gIC8vIHRoaXMuX2RyYWdnaW5nVGFyZ2V0ID0gbnVsbDtcbiAgLy8gdGhpcy5feCA9IDA7XG4gIC8vIHRoaXMuX3kgPSAwO1xufVxuXG5EcmFnZ2FibGUucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogRHJhZ2dhYmxlLFxuICBfZHJhZ1N0YXJ0OiBmdW5jdGlvbiAoZSkge1xuICAgIHZhciBkcmFnZ2luZ1RhcmdldCA9IGUudGFyZ2V0O1xuXG4gICAgaWYgKGRyYWdnaW5nVGFyZ2V0ICYmIGRyYWdnaW5nVGFyZ2V0LmRyYWdnYWJsZSkge1xuICAgICAgdGhpcy5fZHJhZ2dpbmdUYXJnZXQgPSBkcmFnZ2luZ1RhcmdldDtcbiAgICAgIGRyYWdnaW5nVGFyZ2V0LmRyYWdnaW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMuX3ggPSBlLm9mZnNldFg7XG4gICAgICB0aGlzLl95ID0gZS5vZmZzZXRZO1xuICAgICAgdGhpcy5kaXNwYXRjaFRvRWxlbWVudChwYXJhbShkcmFnZ2luZ1RhcmdldCwgZSksICdkcmFnc3RhcnQnLCBlLmV2ZW50KTtcbiAgICB9XG4gIH0sXG4gIF9kcmFnOiBmdW5jdGlvbiAoZSkge1xuICAgIHZhciBkcmFnZ2luZ1RhcmdldCA9IHRoaXMuX2RyYWdnaW5nVGFyZ2V0O1xuXG4gICAgaWYgKGRyYWdnaW5nVGFyZ2V0KSB7XG4gICAgICB2YXIgeCA9IGUub2Zmc2V0WDtcbiAgICAgIHZhciB5ID0gZS5vZmZzZXRZO1xuICAgICAgdmFyIGR4ID0geCAtIHRoaXMuX3g7XG4gICAgICB2YXIgZHkgPSB5IC0gdGhpcy5feTtcbiAgICAgIHRoaXMuX3ggPSB4O1xuICAgICAgdGhpcy5feSA9IHk7XG4gICAgICBkcmFnZ2luZ1RhcmdldC5kcmlmdChkeCwgZHksIGUpO1xuICAgICAgdGhpcy5kaXNwYXRjaFRvRWxlbWVudChwYXJhbShkcmFnZ2luZ1RhcmdldCwgZSksICdkcmFnJywgZS5ldmVudCk7XG4gICAgICB2YXIgZHJvcFRhcmdldCA9IHRoaXMuZmluZEhvdmVyKHgsIHksIGRyYWdnaW5nVGFyZ2V0KS50YXJnZXQ7XG4gICAgICB2YXIgbGFzdERyb3BUYXJnZXQgPSB0aGlzLl9kcm9wVGFyZ2V0O1xuICAgICAgdGhpcy5fZHJvcFRhcmdldCA9IGRyb3BUYXJnZXQ7XG5cbiAgICAgIGlmIChkcmFnZ2luZ1RhcmdldCAhPT0gZHJvcFRhcmdldCkge1xuICAgICAgICBpZiAobGFzdERyb3BUYXJnZXQgJiYgZHJvcFRhcmdldCAhPT0gbGFzdERyb3BUYXJnZXQpIHtcbiAgICAgICAgICB0aGlzLmRpc3BhdGNoVG9FbGVtZW50KHBhcmFtKGxhc3REcm9wVGFyZ2V0LCBlKSwgJ2RyYWdsZWF2ZScsIGUuZXZlbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGRyb3BUYXJnZXQgJiYgZHJvcFRhcmdldCAhPT0gbGFzdERyb3BUYXJnZXQpIHtcbiAgICAgICAgICB0aGlzLmRpc3BhdGNoVG9FbGVtZW50KHBhcmFtKGRyb3BUYXJnZXQsIGUpLCAnZHJhZ2VudGVyJywgZS5ldmVudCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIF9kcmFnRW5kOiBmdW5jdGlvbiAoZSkge1xuICAgIHZhciBkcmFnZ2luZ1RhcmdldCA9IHRoaXMuX2RyYWdnaW5nVGFyZ2V0O1xuXG4gICAgaWYgKGRyYWdnaW5nVGFyZ2V0KSB7XG4gICAgICBkcmFnZ2luZ1RhcmdldC5kcmFnZ2luZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIHRoaXMuZGlzcGF0Y2hUb0VsZW1lbnQocGFyYW0oZHJhZ2dpbmdUYXJnZXQsIGUpLCAnZHJhZ2VuZCcsIGUuZXZlbnQpO1xuXG4gICAgaWYgKHRoaXMuX2Ryb3BUYXJnZXQpIHtcbiAgICAgIHRoaXMuZGlzcGF0Y2hUb0VsZW1lbnQocGFyYW0odGhpcy5fZHJvcFRhcmdldCwgZSksICdkcm9wJywgZS5ldmVudCk7XG4gICAgfVxuXG4gICAgdGhpcy5fZHJhZ2dpbmdUYXJnZXQgPSBudWxsO1xuICAgIHRoaXMuX2Ryb3BUYXJnZXQgPSBudWxsO1xuICB9XG59O1xuXG5mdW5jdGlvbiBwYXJhbSh0YXJnZXQsIGUpIHtcbiAgcmV0dXJuIHtcbiAgICB0YXJnZXQ6IHRhcmdldCxcbiAgICB0b3BUYXJnZXQ6IGUgJiYgZS50b3BUYXJnZXRcbiAgfTtcbn1cblxudmFyIF9kZWZhdWx0ID0gRHJhZ2dhYmxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF2REE7QUFDQTtBQXlEQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/mixin/Draggable.js
