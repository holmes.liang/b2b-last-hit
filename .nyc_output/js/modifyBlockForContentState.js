/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule modifyBlockForContentState
 * @format
 * 
 */


var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var Map = Immutable.Map;

function modifyBlockForContentState(contentState, selectionState, operation) {
  var startKey = selectionState.getStartKey();
  var endKey = selectionState.getEndKey();
  var blockMap = contentState.getBlockMap();
  var newBlocks = blockMap.toSeq().skipUntil(function (_, k) {
    return k === startKey;
  }).takeUntil(function (_, k) {
    return k === endKey;
  }).concat(Map([[endKey, blockMap.get(endKey)]])).map(operation);
  return contentState.merge({
    blockMap: blockMap.merge(newBlocks),
    selectionBefore: selectionState,
    selectionAfter: selectionState
  });
}

module.exports = modifyBlockForContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vZGlmeUJsb2NrRm9yQ29udGVudFN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vZGlmeUJsb2NrRm9yQ29udGVudFN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgbW9kaWZ5QmxvY2tGb3JDb250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgTWFwID0gSW1tdXRhYmxlLk1hcDtcblxuXG5mdW5jdGlvbiBtb2RpZnlCbG9ja0ZvckNvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCBvcGVyYXRpb24pIHtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIGVuZEtleSA9IHNlbGVjdGlvblN0YXRlLmdldEVuZEtleSgpO1xuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIG5ld0Jsb2NrcyA9IGJsb2NrTWFwLnRvU2VxKCkuc2tpcFVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IHN0YXJ0S2V5O1xuICB9KS50YWtlVW50aWwoZnVuY3Rpb24gKF8sIGspIHtcbiAgICByZXR1cm4gayA9PT0gZW5kS2V5O1xuICB9KS5jb25jYXQoTWFwKFtbZW5kS2V5LCBibG9ja01hcC5nZXQoZW5kS2V5KV1dKSkubWFwKG9wZXJhdGlvbik7XG5cbiAgcmV0dXJuIGNvbnRlbnRTdGF0ZS5tZXJnZSh7XG4gICAgYmxvY2tNYXA6IGJsb2NrTWFwLm1lcmdlKG5ld0Jsb2NrcyksXG4gICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGVcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbW9kaWZ5QmxvY2tGb3JDb250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/modifyBlockForContentState.js
