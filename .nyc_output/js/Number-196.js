__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_padEnd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/padEnd */ "./node_modules/lodash/padEnd.js");
/* harmony import */ var lodash_padEnd__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_padEnd__WEBPACK_IMPORTED_MODULE_1__);



var StatisticNumber = function StatisticNumber(props) {
  var value = props.value,
      formatter = props.formatter,
      precision = props.precision,
      decimalSeparator = props.decimalSeparator,
      _props$groupSeparator = props.groupSeparator,
      groupSeparator = _props$groupSeparator === void 0 ? '' : _props$groupSeparator,
      prefixCls = props.prefixCls;
  var valueNode;

  if (typeof formatter === 'function') {
    // Customize formatter
    valueNode = formatter(value);
  } else {
    // Internal formatter
    var val = String(value);
    var cells = val.match(/^(-?)(\d*)(\.(\d+))?$/); // Process if illegal number

    if (!cells) {
      valueNode = val;
    } else {
      var negative = cells[1];

      var _int = cells[2] || '0';

      var decimal = cells[4] || '';
      _int = _int.replace(/\B(?=(\d{3})+(?!\d))/g, groupSeparator);

      if (typeof precision === 'number') {
        decimal = lodash_padEnd__WEBPACK_IMPORTED_MODULE_1___default()(decimal, precision, '0').slice(0, precision);
      }

      if (decimal) {
        decimal = "".concat(decimalSeparator).concat(decimal);
      }

      valueNode = [react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        key: "int",
        className: "".concat(prefixCls, "-content-value-int")
      }, negative, _int), decimal && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        key: "decimal",
        className: "".concat(prefixCls, "-content-value-decimal")
      }, decimal)];
    }
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-content-value")
  }, valueNode);
};

/* harmony default export */ __webpack_exports__["default"] = (StatisticNumber);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zdGF0aXN0aWMvTnVtYmVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zdGF0aXN0aWMvTnVtYmVyLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgcGFkRW5kIGZyb20gJ2xvZGFzaC9wYWRFbmQnO1xuY29uc3QgU3RhdGlzdGljTnVtYmVyID0gcHJvcHMgPT4ge1xuICAgIGNvbnN0IHsgdmFsdWUsIGZvcm1hdHRlciwgcHJlY2lzaW9uLCBkZWNpbWFsU2VwYXJhdG9yLCBncm91cFNlcGFyYXRvciA9ICcnLCBwcmVmaXhDbHMgfSA9IHByb3BzO1xuICAgIGxldCB2YWx1ZU5vZGU7XG4gICAgaWYgKHR5cGVvZiBmb3JtYXR0ZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gQ3VzdG9taXplIGZvcm1hdHRlclxuICAgICAgICB2YWx1ZU5vZGUgPSBmb3JtYXR0ZXIodmFsdWUpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgLy8gSW50ZXJuYWwgZm9ybWF0dGVyXG4gICAgICAgIGNvbnN0IHZhbCA9IFN0cmluZyh2YWx1ZSk7XG4gICAgICAgIGNvbnN0IGNlbGxzID0gdmFsLm1hdGNoKC9eKC0/KShcXGQqKShcXC4oXFxkKykpPyQvKTtcbiAgICAgICAgLy8gUHJvY2VzcyBpZiBpbGxlZ2FsIG51bWJlclxuICAgICAgICBpZiAoIWNlbGxzKSB7XG4gICAgICAgICAgICB2YWx1ZU5vZGUgPSB2YWw7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBjb25zdCBuZWdhdGl2ZSA9IGNlbGxzWzFdO1xuICAgICAgICAgICAgbGV0IGludCA9IGNlbGxzWzJdIHx8ICcwJztcbiAgICAgICAgICAgIGxldCBkZWNpbWFsID0gY2VsbHNbNF0gfHwgJyc7XG4gICAgICAgICAgICBpbnQgPSBpbnQucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgZ3JvdXBTZXBhcmF0b3IpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBwcmVjaXNpb24gPT09ICdudW1iZXInKSB7XG4gICAgICAgICAgICAgICAgZGVjaW1hbCA9IHBhZEVuZChkZWNpbWFsLCBwcmVjaXNpb24sICcwJykuc2xpY2UoMCwgcHJlY2lzaW9uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChkZWNpbWFsKSB7XG4gICAgICAgICAgICAgICAgZGVjaW1hbCA9IGAke2RlY2ltYWxTZXBhcmF0b3J9JHtkZWNpbWFsfWA7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YWx1ZU5vZGUgPSBbXG4gICAgICAgICAgICAgICAgPHNwYW4ga2V5PVwiaW50XCIgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtdmFsdWUtaW50YH0+XG4gICAgICAgICAge25lZ2F0aXZlfVxuICAgICAgICAgIHtpbnR9XG4gICAgICAgIDwvc3Bhbj4sXG4gICAgICAgICAgICAgICAgZGVjaW1hbCAmJiAoPHNwYW4ga2V5PVwiZGVjaW1hbFwiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50LXZhbHVlLWRlY2ltYWxgfT5cbiAgICAgICAgICAgIHtkZWNpbWFsfVxuICAgICAgICAgIDwvc3Bhbj4pLFxuICAgICAgICAgICAgXTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtdmFsdWVgfT57dmFsdWVOb2RlfTwvc3Bhbj47XG59O1xuZXhwb3J0IGRlZmF1bHQgU3RhdGlzdGljTnVtYmVyO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFyQ0E7QUFDQTtBQXNDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/statistic/Number.js
