__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/group-customer-company/choice-search-list.tsx";

function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        cursor: pointer;\n        font-size: 14px;\n        height: 35px;\n        &:hover, &[data-highlight=true] {\n          background: ", ";\n          color: #fff\n        }\n        span {\n          padding: 0 10px;\n          line-height: 35px\n          width: 33%;\n          display: inline-block;\n          overflow: hidden;\n          text-overflow:ellipsis;\n          white-space: nowrap;\n        }\n        i {\n          padding-right: 5px;\n        }\n      "]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        max-height: 360px;\n        min-height: 200px;\n        // overflow: auto;\n        list-style: none;\n        margin-left: 0;\n        border-top: 1px solid #ebebeb;\n        padding-left: 0;\n        font-size: 14px;\n        margin-bottom: 0;\n        padding: 10px 0;\n        &.mobile-ul {\n          li {\n            height: auto;\n            margin-bottom: 10px;\n            span {\n              width: 100%;\n              line-height: 25px;\n              display: block;\n            }\n          }\n        }\n      "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        &[data-full=false] {\n          border: 1px solid @error-color;\n        }\n      "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      position: absolute;\n      left: 0;\n      top: 43px;\n      width: 100%;\n      border: 1px solid #ebebeb;\n      background: #fff;\n      z-index: 103;\n      margin-bottom: 20px;\n      a {\n        font-size: 16px;\n      }\n      .ant-spin-nested-loading {\n          position: relative;\n          width: calc(100% - 2px);\n          z-index: 102;\n          border: 0;\n      }\n      "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      width: 100%;\n      position: relative;\n        .ant-spin-nested-loading{\n            position: absolute;\n            top: 0;\n            left: 0;\n            width: 100% !important;\n            border: 1px solid #ebebeb;\n            background: #fff;\n            z-index: 103;\n            margin-bottom: 1px;\n        }\n        .layer {\n          position: fixed;\n          width: 100%;\n          height: 100%;\n          top: 0;\n          left: 0;\n          z-index: 100;\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var isMobile = _common__WEBPACK_IMPORTED_MODULE_12__["Utils"].getIsMobile();

var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_14__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY,
    HIGHLIGHT_COLOR = _Theme$getTheme.HIGHLIGHT_COLOR;

var pageSize = 10;

var ChoiceSearchList =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(ChoiceSearchList, _Widget);

  function ChoiceSearchList(props, state) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, ChoiceSearchList);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(ChoiceSearchList).call(this, props, state));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(ChoiceSearchList, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        showLabel: this.props.model
      });
      this.getIDType = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.debounce(this.getIDType, 300);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // 卸载异步操作设置状态
      this.setState = function (state, callback) {
        return;
      };
    }
  }, {
    key: "setShowLabel",
    value: function setShowLabel(value) {
      this.setState({
        showLabel: value
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          visible = _this$state.visible,
          placeholder = _this$state.placeholder,
          zIndex = _this$state.zIndex,
          showManyFound = _this$state.showManyFound,
          IdTypeOptions = _this$state.IdTypeOptions;
      var _this$props = this.props,
          style = _this$props.style,
          type = _this$props.type,
          isNotSearh = _this$props.isNotSearh;
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Box, {
        style: style,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Input, {
        style: {
          zIndex: zIndex
        },
        className: type,
        placeholder: placeholder,
        value: this.state.showLabel,
        size: "large",
        onClick: this.handleSelect.bind(this),
        onChange: function onChange(e) {
          _this2.setState({
            showLabel: e.target.value,
            visible: true
          }, function () {
            if (!isNotSearh) {
              _this2.getIDType(_this2.state.showLabel);
            }

            _this2.props.isValid && _this2.props.isValid();
          });
        },
        onFocus: function onFocus() {
          _this2.keyDownEvent();
        },
        onBlur: function onBlur() {
          setTimeout(function () {
            if (!_this2.state.showLabel) {
              _this2.setState({
                showLabel: _this2.state.placeholder // visible: false

              }, function () {
                var _this2$props$onSelect;

                _this2.props.onSelectList && _this2.props.onSelectList((_this2$props$onSelect = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this2$props$onSelect, type, _this2.state.placeholder), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this2$props$onSelect, "blur", "blur"), _this2$props$onSelect));
              });
            } else {
              var _this2$props$onSelect2;

              // this.setState({visible: false});
              _this2.props.onSelectList && _this2.props.onSelectList((_this2$props$onSelect2 = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this2$props$onSelect2, type, _this2.state.showLabel), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_this2$props$onSelect2, "blur", "blur"), _this2$props$onSelect2));
            }
          }, 50);
          setTimeout(function () {
            _this2.setState({
              visible: false
            });
          }, 300);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }), visible && !isNotSearh && IdTypeOptions && IdTypeOptions.length > 0 && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Dropdown, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }, this.listRender(), showManyFound && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        style: {
          borderTop: "1px solid #ebebeb",
          color: COLOR_PRIMARY,
          margin: "0 10px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("many more records found ...").thai("พบบันทึกเพิ่มเติมอีกมากมาย ...").getMessage())));
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(ChoiceSearchList.prototype), "initState", this).call(this), {
        visible: false,
        loading: true,
        error: null,
        IdTypeOptions: [],
        placeholder: this.props.placeholder,
        showLabel: "",
        zIndex: 99,
        showManyFound: false,
        activeIndex: -1,
        total: 0
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject()),
        Dropdown: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject2()),
        Input: Object(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"])(antd__WEBPACK_IMPORTED_MODULE_13__["Input"])(_templateObject3()),
        Ul: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].ul(_templateObject4()),
        Li: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].li(_templateObject5(), HIGHLIGHT_COLOR)
      };
    }
  }, {
    key: "handleSelect",
    value: function handleSelect(event) {
      if (!this.state.showLabel) {
        this.setState({
          visible: !this.state.visible,
          zIndex: 102
        });
        return;
      }

      this.setState({
        visible: !this.state.visible,
        placeholder: this.state.showLabel,
        showLabel: "",
        zIndex: 102
      }, function () {});
    }
  }, {
    key: "keyDownEvent",
    value: function keyDownEvent() {
      var _this = this; //添加键盘事件
      // window.addEventListener('keydown',function (e) {
      // });


      document.onkeydown = function (e) {
        e = e || window.event; // if(_this.state.IdTypeOptions.length > 0) {
        //下键盘

        if (e.keyCode === 40) {
          var activeIndex = _this.state.activeIndex + 1;
          activeIndex = activeIndex >= _this.state.total ? 0 : activeIndex;

          _this.setState({
            activeIndex: activeIndex
          });
        } //上键盘


        if (e.keyCode === 38) {
          var _activeIndex = _this.state.activeIndex - 1;

          _activeIndex = _activeIndex < 0 ? _this.state.total - 1 : _activeIndex;

          _this.setState({
            activeIndex: _activeIndex
          });
        } //回车键


        if (e.keyCode == 13 && _this.state.activeIndex != -1) {
          var type = _this.props.type;
          var item = _this.state.IdTypeOptions[_this.state.activeIndex];
          _this.props.onSelectList && _this.props.onSelectList(item);

          _this.setState({
            showLabel: type === "name" ? item.name : item.idNo,
            visible: false,
            zIndex: 99
          });
        } // }

      };
    }
  }, {
    key: "getIDType",
    value: function getIDType(key) {
      var _this3 = this;

      var _this$props2 = this.props,
          _this$props2$groupTyp = _this$props2.groupType,
          groupType = _this$props2$groupTyp === void 0 ? "INDI" : _this$props2$groupTyp,
          isCedant = _this$props2.isCedant;
      this.setState({
        showManyFound: false
      });
      if (!key) return;

      if (isCedant) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].RI_STAKEHOLDERSQ), {
          key: key,
          stakeholderTypes: ["INSURER"],
          pageSize: pageSize
        }).then(function (response) {
          var _ref = response.body || {
            respData: {},
            totalRecords: 0
          },
              respData = _ref.respData,
              totalRecords = _ref.totalRecords;

          var items = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.cloneDeep((respData || {}).items || []);

          var filter = items.map(function (item) {
            return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {}, item.contact);
          });
          filter = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.cloneDeep(filter);

          _this3.setState({
            IdTypeOptions: filter,
            loading: false,
            showManyFound: totalRecords > pageSize,
            total: (respData || {}).items.length
          });
        }).catch(function (error) {
          _this3.setError(error);
        });
      } else {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post("".concat(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].CUSTOMERSQ), {
          key: key,
          countNumOfClaims: false,
          countNumOfPolicies: false,
          pageSize: pageSize
        }).then(function (response) {
          var _ref2 = response.body || {
            respData: {},
            totalRecords: 0
          },
              respData = _ref2.respData,
              totalRecords = _ref2.totalRecords;

          var items = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.cloneDeep((respData || {}).items || []);

          var filter = items.filter(function (every) {
            return every.custType === groupType;
          });

          _this3.setState({
            IdTypeOptions: filter,
            loading: false,
            showManyFound: totalRecords > pageSize,
            total: (respData || {}).items.length
          });
        }).catch(function (error) {
          _this3.setError(error);
        });
      }
    }
  }, {
    key: "setLoading",
    value: function setLoading() {
      this.setState({
        loading: true,
        error: null
      });
    }
  }, {
    key: "setError",
    value: function setError(err) {
      this.setState({
        loading: false,
        error: err,
        IdTypeOptions: []
      });
    }
  }, {
    key: "listRender",
    value: function listRender() {
      var _this4 = this;

      var _this$state2 = this.state,
          loading = _this$state2.loading,
          error = _this$state2.error,
          IdTypeOptions = _this$state2.IdTypeOptions; // if (error || !IdTypeOptions || IdTypeOptions.length === 0) {

      if (error) {
        var message = !error ? null : error.message;
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 394
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 396
          },
          __self: this
        }, error.message));
      }

      var C = this.getComponents();
      var type = this.props.type;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Spin"], {
        size: "large",
        spinning: loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 403
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Ul, {
        className: isMobile ? "mobile-ul" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 404
        },
        __self: this
      }, this.state.IdTypeOptions.map(function (item, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Li, {
          key: index,
          style: {
            backgroundColor: _this4.state.activeIndex === index ? HIGHLIGHT_COLOR : "#fff",
            color: _this4.state.activeIndex === index ? "#fff" : "rgba(0, 0, 0, 0.65)"
          },
          onClick: function onClick() {
            _this4.props.onSelectList && _this4.props.onSelectList(item);

            _this4.setState({
              showLabel: type === "name" ? item.name : item.idNo,
              visible: false,
              zIndex: 99
            });
          },
          onMouseOver: function onMouseOver() {
            _this4.setState({
              activeIndex: index
            });
          },
          onMouseOut: function onMouseOut() {},
          __source: {
            fileName: _jsxFileName,
            lineNumber: 406
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 428
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "user",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 429
          },
          __self: this
        }), item.name), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 432
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "idcard",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 433
          },
          __self: this
        }), item.idNo), !isMobile && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 437
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "mobile",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 438
          },
          __self: this
        }), item.mobile));
      })));
    }
  }]);

  return ChoiceSearchList;
}(_component__WEBPACK_IMPORTED_MODULE_11__["Widget"]);

/* harmony default export */ __webpack_exports__["default"] = (ChoiceSearchList);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2dyb3VwLWN1c3RvbWVyLWNvbXBhbnkvY2hvaWNlLXNlYXJjaC1saXN0LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9ncm91cC1jdXN0b21lci1jb21wYW55L2Nob2ljZS1zZWFyY2gtbGlzdC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5pbXBvcnQgeyBXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBTdHlsZWRESVYsIFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24sIElucHV0LCBTcGluIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuXG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmNvbnN0IHsgQ09MT1JfUFJJTUFSWSwgSElHSExJR0hUX0NPTE9SIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgcGFnZVNpemUgPSAxMDtcbmV4cG9ydCB0eXBlIENob2ljZVNlYXJjaExpc3RQcm9wcyA9IHtcbiAgcGxhY2Vob2xkZXI/OiBhbnk7XG4gIHN0eWxlPzogYW55O1xuICB0eXBlPzogc3RyaW5nO1xuICBvblNlbGVjdExpc3Q/OiAodmFsdWU6IGFueSkgPT4gdm9pZDtcbiAgbW9kZWw/OiBhbnk7XG4gIGlzVmFsaWQ/OiBhbnk7XG4gIGlzTm90U2Vhcmg/OiBib29sZWFuO1xuICBpc0NlZGFudD86IGFueTtcbiAgZ3JvdXBUeXBlPzogYW55O1xufSAmIFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBDaG9pY2VTZWFyY2hMaXN0U3RhdGUgPSB7XG4gIHZpc2libGU/OiBib29sZWFuO1xuICBsb2FkaW5nPzogYm9vbGVhbjtcbiAgc2hvd0xhYmVsOiBhbnk7XG4gIGVycm9yPzogYW55O1xuICBJZFR5cGVPcHRpb25zOiBhbnlbXTtcbiAgcGxhY2Vob2xkZXI6IGFueTtcbiAgekluZGV4OiBudW1iZXI7XG4gIHNob3dNYW55Rm91bmQ6IGJvb2xlYW47XG4gIGFjdGl2ZUluZGV4OiBudW1iZXI7XG4gIHRvdGFsOiBudW1iZXI7XG59O1xuXG5leHBvcnQgdHlwZSBTdHlsZWRVbCA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJ1bFwiLCBhbnksIHt9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBTdHlsZWRMaSA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJsaVwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmV4cG9ydCB0eXBlIENob2ljZVNlYXJjaExpc3RDb21wb25lbnRzID0ge1xuICBCb3g6IFN0eWxlZERJVjtcbiAgRHJvcGRvd246IFN0eWxlZERJVjtcbiAgVWw6IFN0eWxlZFVsO1xuICBMaTogU3R5bGVkTGk7XG4gIElucHV0OiBhbnk7XG59O1xuXG5jbGFzcyBDaG9pY2VTZWFyY2hMaXN0PFAgZXh0ZW5kcyBDaG9pY2VTZWFyY2hMaXN0UHJvcHMsXG4gIFMgZXh0ZW5kcyBDaG9pY2VTZWFyY2hMaXN0U3RhdGUsXG4gIEMgZXh0ZW5kcyBDaG9pY2VTZWFyY2hMaXN0Q29tcG9uZW50cz4gZXh0ZW5kcyBXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogQ2hvaWNlU2VhcmNoTGlzdFByb3BzLCBzdGF0ZT86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBzdGF0ZSk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dMYWJlbDogdGhpcy5wcm9wcy5tb2RlbCxcbiAgICB9KTtcbiAgICB0aGlzLmdldElEVHlwZSA9IF8uZGVib3VuY2UodGhpcy5nZXRJRFR5cGUsIDMwMCk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAvLyDljbjovb3lvILmraXmk43kvZzorr7nva7nirbmgIFcbiAgICB0aGlzLnNldFN0YXRlID0gKHN0YXRlLCBjYWxsYmFjaykgPT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH07XG4gIH1cblxuICBzZXRTaG93TGFiZWwodmFsdWU6IGFueSkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd0xhYmVsOiB2YWx1ZSxcbiAgICB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IHZpc2libGUsIHBsYWNlaG9sZGVyLCB6SW5kZXgsIHNob3dNYW55Rm91bmQsIElkVHlwZU9wdGlvbnMgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBzdHlsZSwgdHlwZSwgaXNOb3RTZWFyaCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveCBzdHlsZT17c3R5bGV9PlxuICAgICAgICA8Qy5JbnB1dFxuICAgICAgICAgIHN0eWxlPXt7IHpJbmRleDogekluZGV4IH19XG4gICAgICAgICAgY2xhc3NOYW1lPXt0eXBlfVxuICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5zaG93TGFiZWx9XG4gICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKHRoaXMpfVxuICAgICAgICAgIG9uQ2hhbmdlPXsoZTogYW55KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgc2hvd0xhYmVsOiBlLnRhcmdldC52YWx1ZSxcbiAgICAgICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFpc05vdFNlYXJoKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLmdldElEVHlwZSh0aGlzLnN0YXRlLnNob3dMYWJlbCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuaXNWYWxpZCAmJiB0aGlzLnByb3BzLmlzVmFsaWQoKTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfX1cbiAgICAgICAgICBvbkZvY3VzPXsoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmtleURvd25FdmVudCgpO1xuICAgICAgICAgIH19XG4gICAgICAgICAgb25CbHVyPXsoKSA9PiB7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgaWYgKCF0aGlzLnN0YXRlLnNob3dMYWJlbCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoXG4gICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHNob3dMYWJlbDogdGhpcy5zdGF0ZS5wbGFjZWhvbGRlcixcbiAgICAgICAgICAgICAgICAgICAgLy8gdmlzaWJsZTogZmFsc2VcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25TZWxlY3RMaXN0ICYmXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25TZWxlY3RMaXN0KHsgW3R5cGUgYXMgYW55XTogdGhpcy5zdGF0ZS5wbGFjZWhvbGRlciwgYmx1cjogXCJibHVyXCIgfSk7XG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gdGhpcy5zZXRTdGF0ZSh7dmlzaWJsZTogZmFsc2V9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uU2VsZWN0TGlzdCAmJlxuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25TZWxlY3RMaXN0KHsgW3R5cGUgYXMgYW55XTogdGhpcy5zdGF0ZS5zaG93TGFiZWwsIGJsdXI6IFwiYmx1clwiIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCA1MCk7XG4gICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHZpc2libGU6IGZhbHNlIH0pO1xuICAgICAgICAgICAgfSwgMzAwKTtcbiAgICAgICAgICB9fVxuICAgICAgICAvPlxuICAgICAgICB7KHZpc2libGUgJiYgIWlzTm90U2VhcmgpICYmIChJZFR5cGVPcHRpb25zICYmIElkVHlwZU9wdGlvbnMubGVuZ3RoID4gMCkgJiYgKFxuICAgICAgICAgIDxDLkRyb3Bkb3duPlxuICAgICAgICAgICAge3RoaXMubGlzdFJlbmRlcigpfVxuICAgICAgICAgICAge3Nob3dNYW55Rm91bmQgJiYgKFxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlclRvcDogXCIxcHggc29saWQgI2ViZWJlYlwiLFxuICAgICAgICAgICAgICAgICAgY29sb3I6IENPTE9SX1BSSU1BUlksXG4gICAgICAgICAgICAgICAgICBtYXJnaW46IFwiMCAxMHB4XCIsXG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIm1hbnkgbW9yZSByZWNvcmRzIGZvdW5kIC4uLlwiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguJ7guJrguJrguLHguJnguJfguLbguIHguYDguJ7guLTguYjguKHguYDguJXguLTguKHguK3guLXguIHguKHguLLguIHguKHguLLguKIgLi4uXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgPC9DLkRyb3Bkb3duPlxuICAgICAgICApfVxuICAgICAgPC9DLkJveD5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBsb2FkaW5nOiB0cnVlLFxuICAgICAgZXJyb3I6IG51bGwsXG4gICAgICBJZFR5cGVPcHRpb25zOiBbXSxcbiAgICAgIHBsYWNlaG9sZGVyOiB0aGlzLnByb3BzLnBsYWNlaG9sZGVyLFxuICAgICAgc2hvd0xhYmVsOiBcIlwiLFxuICAgICAgekluZGV4OiA5OSxcbiAgICAgIHNob3dNYW55Rm91bmQ6IGZhbHNlLFxuICAgICAgYWN0aXZlSW5kZXg6IC0xLFxuICAgICAgdG90YWw6IDAsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YFxuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5hbnQtc3Bpbi1uZXN0ZWQtbG9hZGluZ3tcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICAgIGxlZnQ6IDA7XG4gICAgICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICB6LWluZGV4OiAxMDM7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmxheWVyIHtcbiAgICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAgIHRvcDogMDtcbiAgICAgICAgICBsZWZ0OiAwO1xuICAgICAgICAgIHotaW5kZXg6IDEwMDtcbiAgICAgICAgfVxuICAgICAgYCxcbiAgICAgIERyb3Bkb3duOiBTdHlsZWQuZGl2YFxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgbGVmdDogMDtcbiAgICAgIHRvcDogNDNweDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICB6LWluZGV4OiAxMDM7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgYSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgIH1cbiAgICAgIC5hbnQtc3Bpbi1uZXN0ZWQtbG9hZGluZyB7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAycHgpO1xuICAgICAgICAgIHotaW5kZXg6IDEwMjtcbiAgICAgICAgICBib3JkZXI6IDA7XG4gICAgICB9XG4gICAgICBgLFxuICAgICAgSW5wdXQ6IFN0eWxlZChJbnB1dClgXG4gICAgICAgICZbZGF0YS1mdWxsPWZhbHNlXSB7XG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgQGVycm9yLWNvbG9yO1xuICAgICAgICB9XG4gICAgICBgLFxuICAgICAgVWw6IFN0eWxlZC51bGBcbiAgICAgICAgbWF4LWhlaWdodDogMzYwcHg7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgICAgICAvLyBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZWJlYmViO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgcGFkZGluZzogMTBweCAwO1xuICAgICAgICAmLm1vYmlsZS11bCB7XG4gICAgICAgICAgbGkge1xuICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgIHNwYW4ge1xuICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgYCxcbiAgICAgIExpOiBTdHlsZWQubGlgXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgICY6aG92ZXIsICZbZGF0YS1oaWdobGlnaHQ9dHJ1ZV0ge1xuICAgICAgICAgIGJhY2tncm91bmQ6ICR7SElHSExJR0hUX0NPTE9SfTtcbiAgICAgICAgICBjb2xvcjogI2ZmZlxuICAgICAgICB9XG4gICAgICAgIHNwYW4ge1xuICAgICAgICAgIHBhZGRpbmc6IDAgMTBweDtcbiAgICAgICAgICBsaW5lLWhlaWdodDogMzVweFxuICAgICAgICAgIHdpZHRoOiAzMyU7XG4gICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgdGV4dC1vdmVyZmxvdzplbGxpcHNpcztcbiAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICB9XG4gICAgICAgIGkge1xuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaGFuZGxlU2VsZWN0KGV2ZW50OiBhbnkpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuc3RhdGUuc2hvd0xhYmVsKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdmlzaWJsZTogIXRoaXMuc3RhdGUudmlzaWJsZSxcbiAgICAgICAgekluZGV4OiAxMDIsXG4gICAgICB9KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZXRTdGF0ZShcbiAgICAgIHtcbiAgICAgICAgdmlzaWJsZTogIXRoaXMuc3RhdGUudmlzaWJsZSxcbiAgICAgICAgcGxhY2Vob2xkZXI6IHRoaXMuc3RhdGUuc2hvd0xhYmVsLFxuICAgICAgICBzaG93TGFiZWw6IFwiXCIsXG4gICAgICAgIHpJbmRleDogMTAyLFxuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgIH0sXG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBrZXlEb3duRXZlbnQoKSB7XG4gICAgY29uc3QgX3RoaXMgPSB0aGlzO1xuICAgIC8v5re75Yqg6ZSu55uY5LqL5Lu2XG4gICAgLy8gd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLGZ1bmN0aW9uIChlKSB7XG4gICAgLy8gfSk7XG4gICAgZG9jdW1lbnQub25rZXlkb3duID0gZnVuY3Rpb24oZSkge1xuICAgICAgZSA9IGUgfHwgd2luZG93LmV2ZW50O1xuICAgICAgLy8gaWYoX3RoaXMuc3RhdGUuSWRUeXBlT3B0aW9ucy5sZW5ndGggPiAwKSB7XG4gICAgICAvL+S4i+mUruebmFxuICAgICAgaWYgKGUua2V5Q29kZSA9PT0gNDApIHtcbiAgICAgICAgbGV0IGFjdGl2ZUluZGV4ID0gX3RoaXMuc3RhdGUuYWN0aXZlSW5kZXggKyAxO1xuICAgICAgICBhY3RpdmVJbmRleCA9IGFjdGl2ZUluZGV4ID49IF90aGlzLnN0YXRlLnRvdGFsID8gMCA6IGFjdGl2ZUluZGV4O1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgYWN0aXZlSW5kZXg6IGFjdGl2ZUluZGV4LFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIC8v5LiK6ZSu55uYXG4gICAgICBpZiAoZS5rZXlDb2RlID09PSAzOCkge1xuICAgICAgICBsZXQgYWN0aXZlSW5kZXggPSBfdGhpcy5zdGF0ZS5hY3RpdmVJbmRleCAtIDE7XG4gICAgICAgIGFjdGl2ZUluZGV4ID0gYWN0aXZlSW5kZXggPCAwID8gX3RoaXMuc3RhdGUudG90YWwgLSAxIDogYWN0aXZlSW5kZXg7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBhY3RpdmVJbmRleDogYWN0aXZlSW5kZXgsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgLy/lm57ovabplK5cbiAgICAgIGlmIChlLmtleUNvZGUgPT0gMTMgJiYgX3RoaXMuc3RhdGUuYWN0aXZlSW5kZXggIT0gLTEpIHtcbiAgICAgICAgY29uc3QgeyB0eXBlIH0gPSBfdGhpcy5wcm9wcztcbiAgICAgICAgbGV0IGl0ZW0gPSBfdGhpcy5zdGF0ZS5JZFR5cGVPcHRpb25zW190aGlzLnN0YXRlLmFjdGl2ZUluZGV4XTtcbiAgICAgICAgX3RoaXMucHJvcHMub25TZWxlY3RMaXN0ICYmIF90aGlzLnByb3BzLm9uU2VsZWN0TGlzdChpdGVtKTtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHNob3dMYWJlbDogdHlwZSA9PT0gXCJuYW1lXCIgPyBpdGVtLm5hbWUgOiBpdGVtLmlkTm8sXG4gICAgICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICAgICAgekluZGV4OiA5OSxcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICAvLyB9XG4gICAgfTtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRJRFR5cGUoa2V5OiBzdHJpbmcpIHtcbiAgICBjb25zdCB7IGdyb3VwVHlwZSA9IFwiSU5ESVwiLCBpc0NlZGFudCB9ID0gdGhpcy5wcm9wcztcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHNob3dNYW55Rm91bmQ6IGZhbHNlLFxuICAgIH0pO1xuICAgIGlmICgha2V5KSByZXR1cm47XG4gICAgaWYgKGlzQ2VkYW50KSB7XG4gICAgICBBamF4LnBvc3QoYCR7QXBpcy5SSV9TVEFLRUhPTERFUlNRfWAsIHtcbiAgICAgICAga2V5LFxuICAgICAgICBzdGFrZWhvbGRlclR5cGVzOiBbXCJJTlNVUkVSXCJdLFxuICAgICAgICBwYWdlU2l6ZTogcGFnZVNpemUsXG4gICAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgcmVzcERhdGEsIHRvdGFsUmVjb3JkcyB9ID0gcmVzcG9uc2UuYm9keSB8fCB7IHJlc3BEYXRhOiB7fSwgdG90YWxSZWNvcmRzOiAwIH07XG4gICAgICAgICAgY29uc3QgaXRlbXMgPSBfLmNsb25lRGVlcCgoKHJlc3BEYXRhIHx8IHt9KS5pdGVtcyB8fCBbXSkpO1xuICAgICAgICAgIGxldCBmaWx0ZXIgPSBpdGVtcy5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgLi4uaXRlbS5jb250YWN0LFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBmaWx0ZXIgPSBfLmNsb25lRGVlcChmaWx0ZXIpO1xuICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgSWRUeXBlT3B0aW9uczogZmlsdGVyLFxuICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgICAgICBzaG93TWFueUZvdW5kOiB0b3RhbFJlY29yZHMgPiBwYWdlU2l6ZSxcbiAgICAgICAgICAgIHRvdGFsOiAocmVzcERhdGEgfHwge30pLml0ZW1zLmxlbmd0aCxcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgICB0aGlzLnNldEVycm9yKGVycm9yKTtcbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIEFqYXgucG9zdChgJHtBcGlzLkNVU1RPTUVSU1F9YCwge1xuICAgICAgICBrZXksXG4gICAgICAgIGNvdW50TnVtT2ZDbGFpbXM6IGZhbHNlLFxuICAgICAgICBjb3VudE51bU9mUG9saWNpZXM6IGZhbHNlLFxuICAgICAgICBwYWdlU2l6ZTogcGFnZVNpemUsXG4gICAgICB9KVxuICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgcmVzcERhdGEsIHRvdGFsUmVjb3JkcyB9ID0gcmVzcG9uc2UuYm9keSB8fCB7IHJlc3BEYXRhOiB7fSwgdG90YWxSZWNvcmRzOiAwIH07XG4gICAgICAgICAgY29uc3QgaXRlbXMgPSBfLmNsb25lRGVlcCgoKHJlc3BEYXRhIHx8IHt9KS5pdGVtcyB8fCBbXSkpO1xuICAgICAgICAgIGxldCBmaWx0ZXI6IGFueVtdID0gaXRlbXMuZmlsdGVyKChldmVyeTogYW55KSA9PiBldmVyeS5jdXN0VHlwZSA9PT0gZ3JvdXBUeXBlKTtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIElkVHlwZU9wdGlvbnM6IGZpbHRlcixcbiAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgc2hvd01hbnlGb3VuZDogdG90YWxSZWNvcmRzID4gcGFnZVNpemUsXG4gICAgICAgICAgICB0b3RhbDogKHJlc3BEYXRhIHx8IHt9KS5pdGVtcy5sZW5ndGgsXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgdGhpcy5zZXRFcnJvcihlcnJvcik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICB9XG5cbiAgcHJvdGVjdGVkIHNldExvYWRpbmcoKTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IHRydWUsIGVycm9yOiBudWxsIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIHNldEVycm9yKGVycjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IGZhbHNlLCBlcnJvcjogZXJyLCBJZFR5cGVPcHRpb25zOiBbXSB9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBsaXN0UmVuZGVyKCk6IGFueSB7XG4gICAgY29uc3QgeyBsb2FkaW5nLCBlcnJvciwgSWRUeXBlT3B0aW9ucyB9ID0gdGhpcy5zdGF0ZTtcbiAgICAvLyBpZiAoZXJyb3IgfHwgIUlkVHlwZU9wdGlvbnMgfHwgSWRUeXBlT3B0aW9ucy5sZW5ndGggPT09IDApIHtcbiAgICBpZiAoZXJyb3IpIHtcbiAgICAgIGNvbnN0IG1lc3NhZ2UgPSAhZXJyb3IgPyBudWxsIDogZXJyb3IubWVzc2FnZTtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgey8qPHNwYW4+eyEhbWVzc2FnZSA/IG1lc3NhZ2UgOiBcIm5vIGRhdGFcIn08L3NwYW4+Ki99XG4gICAgICAgICAgPHNwYW4+e2Vycm9yLm1lc3NhZ2V9PC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICk7XG4gICAgfVxuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IHR5cGUgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXtsb2FkaW5nfT5cbiAgICAgICAgPEMuVWwgY2xhc3NOYW1lPXtpc01vYmlsZSA/IFwibW9iaWxlLXVsXCIgOiBcIlwifT5cbiAgICAgICAgICB7dGhpcy5zdGF0ZS5JZFR5cGVPcHRpb25zLm1hcCgoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiAoXG4gICAgICAgICAgICA8Qy5MaVxuICAgICAgICAgICAgICBrZXk9e2luZGV4fVxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogdGhpcy5zdGF0ZS5hY3RpdmVJbmRleCA9PT0gaW5kZXggPyBISUdITElHSFRfQ09MT1IgOiBcIiNmZmZcIixcbiAgICAgICAgICAgICAgICBjb2xvcjogdGhpcy5zdGF0ZS5hY3RpdmVJbmRleCA9PT0gaW5kZXggPyBcIiNmZmZcIiA6IFwicmdiYSgwLCAwLCAwLCAwLjY1KVwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vblNlbGVjdExpc3QgJiYgdGhpcy5wcm9wcy5vblNlbGVjdExpc3QoaXRlbSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBzaG93TGFiZWw6IHR5cGUgPT09IFwibmFtZVwiID8gaXRlbS5uYW1lIDogaXRlbS5pZE5vLFxuICAgICAgICAgICAgICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICB6SW5kZXg6IDk5LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvbk1vdXNlT3Zlcj17KCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgYWN0aXZlSW5kZXg6IGluZGV4LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvbk1vdXNlT3V0PXsoKSA9PiB7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgIDxJY29uIHR5cGU9XCJ1c2VyXCIvPlxuICAgICAgICAgICAgICAgIHtpdGVtLm5hbWV9XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImlkY2FyZFwiLz5cbiAgICAgICAgICAgICAgICB7aXRlbS5pZE5vfVxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIHshaXNNb2JpbGUgJiYgKFxuICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cIm1vYmlsZVwiLz5cbiAgICAgICAgICAgICAgICAgIHtpdGVtLm1vYmlsZX1cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L0MuTGk+XG4gICAgICAgICAgKSl9XG4gICAgICAgIDwvQy5VbD5cbiAgICAgIDwvU3Bpbj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENob2ljZVNlYXJjaExpc3Q7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUdBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFvQ0E7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFFQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBOUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFzQkE7QUFtQkE7QUFLQTtBQXVCQTtBQXRFQTtBQTRGQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBU0E7OztBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUVBO0FBQ0E7QUFDQTtBQUVBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBWkE7QUFjQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQW5CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBakNBO0FBMENBOzs7O0FBMVlBO0FBQ0E7QUE0WUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/group-customer-company/choice-search-list.tsx
