/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditorContents-core.react
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DraftEditorBlock = __webpack_require__(/*! ./DraftEditorBlock.react */ "./node_modules/draft-js/lib/DraftEditorBlock.react.js");

var DraftOffsetKey = __webpack_require__(/*! ./DraftOffsetKey */ "./node_modules/draft-js/lib/DraftOffsetKey.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var cx = __webpack_require__(/*! fbjs/lib/cx */ "./node_modules/fbjs/lib/cx.js");

var joinClasses = __webpack_require__(/*! fbjs/lib/joinClasses */ "./node_modules/fbjs/lib/joinClasses.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");
/**
 * Provide default styling for list items. This way, lists will be styled with
 * proper counters and indentation even if the caller does not specify
 * their own styling at all. If more than five levels of nesting are needed,
 * the necessary CSS classes can be provided via `blockStyleFn` configuration.
 */


var getListItemClasses = function getListItemClasses(type, depth, shouldResetCount, direction) {
  return cx({
    'public/DraftStyleDefault/unorderedListItem': type === 'unordered-list-item',
    'public/DraftStyleDefault/orderedListItem': type === 'ordered-list-item',
    'public/DraftStyleDefault/reset': shouldResetCount,
    'public/DraftStyleDefault/depth0': depth === 0,
    'public/DraftStyleDefault/depth1': depth === 1,
    'public/DraftStyleDefault/depth2': depth === 2,
    'public/DraftStyleDefault/depth3': depth === 3,
    'public/DraftStyleDefault/depth4': depth === 4,
    'public/DraftStyleDefault/listLTR': direction === 'LTR',
    'public/DraftStyleDefault/listRTL': direction === 'RTL'
  });
};
/**
 * `DraftEditorContents` is the container component for all block components
 * rendered for a `DraftEditor`. It is optimized to aggressively avoid
 * re-rendering blocks whenever possible.
 *
 * This component is separate from `DraftEditor` because certain props
 * (for instance, ARIA props) must be allowed to update without affecting
 * the contents of the editor.
 */


var DraftEditorContents = function (_React$Component) {
  _inherits(DraftEditorContents, _React$Component);

  function DraftEditorContents() {
    _classCallCheck(this, DraftEditorContents);

    return _possibleConstructorReturn(this, _React$Component.apply(this, arguments));
  }

  DraftEditorContents.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
    var prevEditorState = this.props.editorState;
    var nextEditorState = nextProps.editorState;
    var prevDirectionMap = prevEditorState.getDirectionMap();
    var nextDirectionMap = nextEditorState.getDirectionMap(); // Text direction has changed for one or more blocks. We must re-render.

    if (prevDirectionMap !== nextDirectionMap) {
      return true;
    }

    var didHaveFocus = prevEditorState.getSelection().getHasFocus();
    var nowHasFocus = nextEditorState.getSelection().getHasFocus();

    if (didHaveFocus !== nowHasFocus) {
      return true;
    }

    var nextNativeContent = nextEditorState.getNativelyRenderedContent();
    var wasComposing = prevEditorState.isInCompositionMode();
    var nowComposing = nextEditorState.isInCompositionMode(); // If the state is unchanged or we're currently rendering a natively
    // rendered state, there's nothing new to be done.

    if (prevEditorState === nextEditorState || nextNativeContent !== null && nextEditorState.getCurrentContent() === nextNativeContent || wasComposing && nowComposing) {
      return false;
    }

    var prevContent = prevEditorState.getCurrentContent();
    var nextContent = nextEditorState.getCurrentContent();
    var prevDecorator = prevEditorState.getDecorator();
    var nextDecorator = nextEditorState.getDecorator();
    return wasComposing !== nowComposing || prevContent !== nextContent || prevDecorator !== nextDecorator || nextEditorState.mustForceSelection();
  };

  DraftEditorContents.prototype.render = function render() {
    var _props = this.props,
        blockRenderMap = _props.blockRenderMap,
        blockRendererFn = _props.blockRendererFn,
        blockStyleFn = _props.blockStyleFn,
        customStyleMap = _props.customStyleMap,
        customStyleFn = _props.customStyleFn,
        editorState = _props.editorState,
        editorKey = _props.editorKey,
        textDirectionality = _props.textDirectionality;
    var content = editorState.getCurrentContent();
    var selection = editorState.getSelection();
    var forceSelection = editorState.mustForceSelection();
    var decorator = editorState.getDecorator();
    var directionMap = nullthrows(editorState.getDirectionMap());
    var blocksAsArray = content.getBlocksAsArray();
    var processedBlocks = [];
    var currentDepth = null;
    var lastWrapperTemplate = null;

    for (var ii = 0; ii < blocksAsArray.length; ii++) {
      var _block = blocksAsArray[ii];

      var key = _block.getKey();

      var blockType = _block.getType();

      var customRenderer = blockRendererFn(_block);
      var CustomComponent = void 0,
          customProps = void 0,
          customEditable = void 0;

      if (customRenderer) {
        CustomComponent = customRenderer.component;
        customProps = customRenderer.props;
        customEditable = customRenderer.editable;
      }

      var direction = textDirectionality ? textDirectionality : directionMap.get(key);
      var offsetKey = DraftOffsetKey.encode(key, 0, 0);
      var componentProps = {
        contentState: content,
        block: _block,
        blockProps: customProps,
        blockStyleFn: blockStyleFn,
        customStyleMap: customStyleMap,
        customStyleFn: customStyleFn,
        decorator: decorator,
        direction: direction,
        forceSelection: forceSelection,
        key: key,
        offsetKey: offsetKey,
        selection: selection,
        tree: editorState.getBlockTree(key)
      };
      var configForType = blockRenderMap.get(blockType) || blockRenderMap.get('unstyled');
      var wrapperTemplate = configForType.wrapper;
      var Element = configForType.element || blockRenderMap.get('unstyled').element;

      var depth = _block.getDepth();

      var className = '';

      if (blockStyleFn) {
        className = blockStyleFn(_block);
      } // List items are special snowflakes, since we handle nesting and
      // counters manually.


      if (Element === 'li') {
        var shouldResetCount = lastWrapperTemplate !== wrapperTemplate || currentDepth === null || depth > currentDepth;
        className = joinClasses(className, getListItemClasses(blockType, depth, shouldResetCount, direction));
      }

      var Component = CustomComponent || DraftEditorBlock;
      var childProps = {
        className: className,
        'data-block': true,
        'data-editor': editorKey,
        'data-offset-key': offsetKey,
        key: key
      };

      if (customEditable !== undefined) {
        childProps = _extends({}, childProps, {
          contentEditable: customEditable,
          suppressContentEditableWarning: true
        });
      }

      var child = React.createElement(Element, childProps, React.createElement(Component, componentProps));
      processedBlocks.push({
        block: child,
        wrapperTemplate: wrapperTemplate,
        key: key,
        offsetKey: offsetKey
      });

      if (wrapperTemplate) {
        currentDepth = _block.getDepth();
      } else {
        currentDepth = null;
      }

      lastWrapperTemplate = wrapperTemplate;
    } // Group contiguous runs of blocks that have the same wrapperTemplate


    var outputBlocks = [];

    for (var _ii = 0; _ii < processedBlocks.length;) {
      var info = processedBlocks[_ii];

      if (info.wrapperTemplate) {
        var blocks = [];

        do {
          blocks.push(processedBlocks[_ii].block);
          _ii++;
        } while (_ii < processedBlocks.length && processedBlocks[_ii].wrapperTemplate === info.wrapperTemplate);

        var wrapperElement = React.cloneElement(info.wrapperTemplate, {
          key: info.key + '-wrap',
          'data-offset-key': info.offsetKey
        }, blocks);
        outputBlocks.push(wrapperElement);
      } else {
        outputBlocks.push(info.block);
        _ii++;
      }
    }

    return React.createElement('div', {
      'data-contents': 'true'
    }, outputBlocks);
  };

  return DraftEditorContents;
}(React.Component);

module.exports = DraftEditorContents;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yQ29udGVudHMtY29yZS5yZWFjdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9EcmFmdEVkaXRvckNvbnRlbnRzLWNvcmUucmVhY3QuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBEcmFmdEVkaXRvckNvbnRlbnRzLWNvcmUucmVhY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIERyYWZ0RWRpdG9yQmxvY2sgPSByZXF1aXJlKCcuL0RyYWZ0RWRpdG9yQmxvY2sucmVhY3QnKTtcbnZhciBEcmFmdE9mZnNldEtleSA9IHJlcXVpcmUoJy4vRHJhZnRPZmZzZXRLZXknKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBjeCA9IHJlcXVpcmUoJ2ZianMvbGliL2N4Jyk7XG52YXIgam9pbkNsYXNzZXMgPSByZXF1aXJlKCdmYmpzL2xpYi9qb2luQ2xhc3NlcycpO1xudmFyIG51bGx0aHJvd3MgPSByZXF1aXJlKCdmYmpzL2xpYi9udWxsdGhyb3dzJyk7XG5cbi8qKlxuICogUHJvdmlkZSBkZWZhdWx0IHN0eWxpbmcgZm9yIGxpc3QgaXRlbXMuIFRoaXMgd2F5LCBsaXN0cyB3aWxsIGJlIHN0eWxlZCB3aXRoXG4gKiBwcm9wZXIgY291bnRlcnMgYW5kIGluZGVudGF0aW9uIGV2ZW4gaWYgdGhlIGNhbGxlciBkb2VzIG5vdCBzcGVjaWZ5XG4gKiB0aGVpciBvd24gc3R5bGluZyBhdCBhbGwuIElmIG1vcmUgdGhhbiBmaXZlIGxldmVscyBvZiBuZXN0aW5nIGFyZSBuZWVkZWQsXG4gKiB0aGUgbmVjZXNzYXJ5IENTUyBjbGFzc2VzIGNhbiBiZSBwcm92aWRlZCB2aWEgYGJsb2NrU3R5bGVGbmAgY29uZmlndXJhdGlvbi5cbiAqL1xudmFyIGdldExpc3RJdGVtQ2xhc3NlcyA9IGZ1bmN0aW9uIGdldExpc3RJdGVtQ2xhc3Nlcyh0eXBlLCBkZXB0aCwgc2hvdWxkUmVzZXRDb3VudCwgZGlyZWN0aW9uKSB7XG4gIHJldHVybiBjeCh7XG4gICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC91bm9yZGVyZWRMaXN0SXRlbSc6IHR5cGUgPT09ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICAncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L29yZGVyZWRMaXN0SXRlbSc6IHR5cGUgPT09ICdvcmRlcmVkLWxpc3QtaXRlbScsXG4gICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9yZXNldCc6IHNob3VsZFJlc2V0Q291bnQsXG4gICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9kZXB0aDAnOiBkZXB0aCA9PT0gMCxcbiAgICAncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L2RlcHRoMSc6IGRlcHRoID09PSAxLFxuICAgICdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvZGVwdGgyJzogZGVwdGggPT09IDIsXG4gICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9kZXB0aDMnOiBkZXB0aCA9PT0gMyxcbiAgICAncHVibGljL0RyYWZ0U3R5bGVEZWZhdWx0L2RlcHRoNCc6IGRlcHRoID09PSA0LFxuICAgICdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvbGlzdExUUic6IGRpcmVjdGlvbiA9PT0gJ0xUUicsXG4gICAgJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9saXN0UlRMJzogZGlyZWN0aW9uID09PSAnUlRMJ1xuICB9KTtcbn07XG5cbi8qKlxuICogYERyYWZ0RWRpdG9yQ29udGVudHNgIGlzIHRoZSBjb250YWluZXIgY29tcG9uZW50IGZvciBhbGwgYmxvY2sgY29tcG9uZW50c1xuICogcmVuZGVyZWQgZm9yIGEgYERyYWZ0RWRpdG9yYC4gSXQgaXMgb3B0aW1pemVkIHRvIGFnZ3Jlc3NpdmVseSBhdm9pZFxuICogcmUtcmVuZGVyaW5nIGJsb2NrcyB3aGVuZXZlciBwb3NzaWJsZS5cbiAqXG4gKiBUaGlzIGNvbXBvbmVudCBpcyBzZXBhcmF0ZSBmcm9tIGBEcmFmdEVkaXRvcmAgYmVjYXVzZSBjZXJ0YWluIHByb3BzXG4gKiAoZm9yIGluc3RhbmNlLCBBUklBIHByb3BzKSBtdXN0IGJlIGFsbG93ZWQgdG8gdXBkYXRlIHdpdGhvdXQgYWZmZWN0aW5nXG4gKiB0aGUgY29udGVudHMgb2YgdGhlIGVkaXRvci5cbiAqL1xuXG52YXIgRHJhZnRFZGl0b3JDb250ZW50cyA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhEcmFmdEVkaXRvckNvbnRlbnRzLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBEcmFmdEVkaXRvckNvbnRlbnRzKCkge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEcmFmdEVkaXRvckNvbnRlbnRzKTtcblxuICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgRHJhZnRFZGl0b3JDb250ZW50cy5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgIHZhciBwcmV2RWRpdG9yU3RhdGUgPSB0aGlzLnByb3BzLmVkaXRvclN0YXRlO1xuICAgIHZhciBuZXh0RWRpdG9yU3RhdGUgPSBuZXh0UHJvcHMuZWRpdG9yU3RhdGU7XG5cbiAgICB2YXIgcHJldkRpcmVjdGlvbk1hcCA9IHByZXZFZGl0b3JTdGF0ZS5nZXREaXJlY3Rpb25NYXAoKTtcbiAgICB2YXIgbmV4dERpcmVjdGlvbk1hcCA9IG5leHRFZGl0b3JTdGF0ZS5nZXREaXJlY3Rpb25NYXAoKTtcblxuICAgIC8vIFRleHQgZGlyZWN0aW9uIGhhcyBjaGFuZ2VkIGZvciBvbmUgb3IgbW9yZSBibG9ja3MuIFdlIG11c3QgcmUtcmVuZGVyLlxuICAgIGlmIChwcmV2RGlyZWN0aW9uTWFwICE9PSBuZXh0RGlyZWN0aW9uTWFwKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICB2YXIgZGlkSGF2ZUZvY3VzID0gcHJldkVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLmdldEhhc0ZvY3VzKCk7XG4gICAgdmFyIG5vd0hhc0ZvY3VzID0gbmV4dEVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLmdldEhhc0ZvY3VzKCk7XG5cbiAgICBpZiAoZGlkSGF2ZUZvY3VzICE9PSBub3dIYXNGb2N1cykge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgdmFyIG5leHROYXRpdmVDb250ZW50ID0gbmV4dEVkaXRvclN0YXRlLmdldE5hdGl2ZWx5UmVuZGVyZWRDb250ZW50KCk7XG5cbiAgICB2YXIgd2FzQ29tcG9zaW5nID0gcHJldkVkaXRvclN0YXRlLmlzSW5Db21wb3NpdGlvbk1vZGUoKTtcbiAgICB2YXIgbm93Q29tcG9zaW5nID0gbmV4dEVkaXRvclN0YXRlLmlzSW5Db21wb3NpdGlvbk1vZGUoKTtcblxuICAgIC8vIElmIHRoZSBzdGF0ZSBpcyB1bmNoYW5nZWQgb3Igd2UncmUgY3VycmVudGx5IHJlbmRlcmluZyBhIG5hdGl2ZWx5XG4gICAgLy8gcmVuZGVyZWQgc3RhdGUsIHRoZXJlJ3Mgbm90aGluZyBuZXcgdG8gYmUgZG9uZS5cbiAgICBpZiAocHJldkVkaXRvclN0YXRlID09PSBuZXh0RWRpdG9yU3RhdGUgfHwgbmV4dE5hdGl2ZUNvbnRlbnQgIT09IG51bGwgJiYgbmV4dEVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCkgPT09IG5leHROYXRpdmVDb250ZW50IHx8IHdhc0NvbXBvc2luZyAmJiBub3dDb21wb3NpbmcpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgcHJldkNvbnRlbnQgPSBwcmV2RWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgbmV4dENvbnRlbnQgPSBuZXh0RWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgcHJldkRlY29yYXRvciA9IHByZXZFZGl0b3JTdGF0ZS5nZXREZWNvcmF0b3IoKTtcbiAgICB2YXIgbmV4dERlY29yYXRvciA9IG5leHRFZGl0b3JTdGF0ZS5nZXREZWNvcmF0b3IoKTtcbiAgICByZXR1cm4gd2FzQ29tcG9zaW5nICE9PSBub3dDb21wb3NpbmcgfHwgcHJldkNvbnRlbnQgIT09IG5leHRDb250ZW50IHx8IHByZXZEZWNvcmF0b3IgIT09IG5leHREZWNvcmF0b3IgfHwgbmV4dEVkaXRvclN0YXRlLm11c3RGb3JjZVNlbGVjdGlvbigpO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yQ29udGVudHMucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgYmxvY2tSZW5kZXJNYXAgPSBfcHJvcHMuYmxvY2tSZW5kZXJNYXAsXG4gICAgICAgIGJsb2NrUmVuZGVyZXJGbiA9IF9wcm9wcy5ibG9ja1JlbmRlcmVyRm4sXG4gICAgICAgIGJsb2NrU3R5bGVGbiA9IF9wcm9wcy5ibG9ja1N0eWxlRm4sXG4gICAgICAgIGN1c3RvbVN0eWxlTWFwID0gX3Byb3BzLmN1c3RvbVN0eWxlTWFwLFxuICAgICAgICBjdXN0b21TdHlsZUZuID0gX3Byb3BzLmN1c3RvbVN0eWxlRm4sXG4gICAgICAgIGVkaXRvclN0YXRlID0gX3Byb3BzLmVkaXRvclN0YXRlLFxuICAgICAgICBlZGl0b3JLZXkgPSBfcHJvcHMuZWRpdG9yS2V5LFxuICAgICAgICB0ZXh0RGlyZWN0aW9uYWxpdHkgPSBfcHJvcHMudGV4dERpcmVjdGlvbmFsaXR5O1xuXG5cbiAgICB2YXIgY29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciBmb3JjZVNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLm11c3RGb3JjZVNlbGVjdGlvbigpO1xuICAgIHZhciBkZWNvcmF0b3IgPSBlZGl0b3JTdGF0ZS5nZXREZWNvcmF0b3IoKTtcbiAgICB2YXIgZGlyZWN0aW9uTWFwID0gbnVsbHRocm93cyhlZGl0b3JTdGF0ZS5nZXREaXJlY3Rpb25NYXAoKSk7XG5cbiAgICB2YXIgYmxvY2tzQXNBcnJheSA9IGNvbnRlbnQuZ2V0QmxvY2tzQXNBcnJheSgpO1xuICAgIHZhciBwcm9jZXNzZWRCbG9ja3MgPSBbXTtcblxuICAgIHZhciBjdXJyZW50RGVwdGggPSBudWxsO1xuICAgIHZhciBsYXN0V3JhcHBlclRlbXBsYXRlID0gbnVsbDtcblxuICAgIGZvciAodmFyIGlpID0gMDsgaWkgPCBibG9ja3NBc0FycmF5Lmxlbmd0aDsgaWkrKykge1xuICAgICAgdmFyIF9ibG9jayA9IGJsb2Nrc0FzQXJyYXlbaWldO1xuICAgICAgdmFyIGtleSA9IF9ibG9jay5nZXRLZXkoKTtcbiAgICAgIHZhciBibG9ja1R5cGUgPSBfYmxvY2suZ2V0VHlwZSgpO1xuXG4gICAgICB2YXIgY3VzdG9tUmVuZGVyZXIgPSBibG9ja1JlbmRlcmVyRm4oX2Jsb2NrKTtcbiAgICAgIHZhciBDdXN0b21Db21wb25lbnQgPSB2b2lkIDAsXG4gICAgICAgICAgY3VzdG9tUHJvcHMgPSB2b2lkIDAsXG4gICAgICAgICAgY3VzdG9tRWRpdGFibGUgPSB2b2lkIDA7XG4gICAgICBpZiAoY3VzdG9tUmVuZGVyZXIpIHtcbiAgICAgICAgQ3VzdG9tQ29tcG9uZW50ID0gY3VzdG9tUmVuZGVyZXIuY29tcG9uZW50O1xuICAgICAgICBjdXN0b21Qcm9wcyA9IGN1c3RvbVJlbmRlcmVyLnByb3BzO1xuICAgICAgICBjdXN0b21FZGl0YWJsZSA9IGN1c3RvbVJlbmRlcmVyLmVkaXRhYmxlO1xuICAgICAgfVxuXG4gICAgICB2YXIgZGlyZWN0aW9uID0gdGV4dERpcmVjdGlvbmFsaXR5ID8gdGV4dERpcmVjdGlvbmFsaXR5IDogZGlyZWN0aW9uTWFwLmdldChrZXkpO1xuICAgICAgdmFyIG9mZnNldEtleSA9IERyYWZ0T2Zmc2V0S2V5LmVuY29kZShrZXksIDAsIDApO1xuICAgICAgdmFyIGNvbXBvbmVudFByb3BzID0ge1xuICAgICAgICBjb250ZW50U3RhdGU6IGNvbnRlbnQsXG4gICAgICAgIGJsb2NrOiBfYmxvY2ssXG4gICAgICAgIGJsb2NrUHJvcHM6IGN1c3RvbVByb3BzLFxuICAgICAgICBibG9ja1N0eWxlRm46IGJsb2NrU3R5bGVGbixcbiAgICAgICAgY3VzdG9tU3R5bGVNYXA6IGN1c3RvbVN0eWxlTWFwLFxuICAgICAgICBjdXN0b21TdHlsZUZuOiBjdXN0b21TdHlsZUZuLFxuICAgICAgICBkZWNvcmF0b3I6IGRlY29yYXRvcixcbiAgICAgICAgZGlyZWN0aW9uOiBkaXJlY3Rpb24sXG4gICAgICAgIGZvcmNlU2VsZWN0aW9uOiBmb3JjZVNlbGVjdGlvbixcbiAgICAgICAga2V5OiBrZXksXG4gICAgICAgIG9mZnNldEtleTogb2Zmc2V0S2V5LFxuICAgICAgICBzZWxlY3Rpb246IHNlbGVjdGlvbixcbiAgICAgICAgdHJlZTogZWRpdG9yU3RhdGUuZ2V0QmxvY2tUcmVlKGtleSlcbiAgICAgIH07XG5cbiAgICAgIHZhciBjb25maWdGb3JUeXBlID0gYmxvY2tSZW5kZXJNYXAuZ2V0KGJsb2NrVHlwZSkgfHwgYmxvY2tSZW5kZXJNYXAuZ2V0KCd1bnN0eWxlZCcpO1xuICAgICAgdmFyIHdyYXBwZXJUZW1wbGF0ZSA9IGNvbmZpZ0ZvclR5cGUud3JhcHBlcjtcblxuICAgICAgdmFyIEVsZW1lbnQgPSBjb25maWdGb3JUeXBlLmVsZW1lbnQgfHwgYmxvY2tSZW5kZXJNYXAuZ2V0KCd1bnN0eWxlZCcpLmVsZW1lbnQ7XG5cbiAgICAgIHZhciBkZXB0aCA9IF9ibG9jay5nZXREZXB0aCgpO1xuICAgICAgdmFyIGNsYXNzTmFtZSA9ICcnO1xuICAgICAgaWYgKGJsb2NrU3R5bGVGbikge1xuICAgICAgICBjbGFzc05hbWUgPSBibG9ja1N0eWxlRm4oX2Jsb2NrKTtcbiAgICAgIH1cblxuICAgICAgLy8gTGlzdCBpdGVtcyBhcmUgc3BlY2lhbCBzbm93Zmxha2VzLCBzaW5jZSB3ZSBoYW5kbGUgbmVzdGluZyBhbmRcbiAgICAgIC8vIGNvdW50ZXJzIG1hbnVhbGx5LlxuICAgICAgaWYgKEVsZW1lbnQgPT09ICdsaScpIHtcbiAgICAgICAgdmFyIHNob3VsZFJlc2V0Q291bnQgPSBsYXN0V3JhcHBlclRlbXBsYXRlICE9PSB3cmFwcGVyVGVtcGxhdGUgfHwgY3VycmVudERlcHRoID09PSBudWxsIHx8IGRlcHRoID4gY3VycmVudERlcHRoO1xuICAgICAgICBjbGFzc05hbWUgPSBqb2luQ2xhc3NlcyhjbGFzc05hbWUsIGdldExpc3RJdGVtQ2xhc3NlcyhibG9ja1R5cGUsIGRlcHRoLCBzaG91bGRSZXNldENvdW50LCBkaXJlY3Rpb24pKTtcbiAgICAgIH1cblxuICAgICAgdmFyIENvbXBvbmVudCA9IEN1c3RvbUNvbXBvbmVudCB8fCBEcmFmdEVkaXRvckJsb2NrO1xuICAgICAgdmFyIGNoaWxkUHJvcHMgPSB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAnZGF0YS1ibG9jayc6IHRydWUsXG4gICAgICAgICdkYXRhLWVkaXRvcic6IGVkaXRvcktleSxcbiAgICAgICAgJ2RhdGEtb2Zmc2V0LWtleSc6IG9mZnNldEtleSxcbiAgICAgICAga2V5OiBrZXlcbiAgICAgIH07XG4gICAgICBpZiAoY3VzdG9tRWRpdGFibGUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjaGlsZFByb3BzID0gX2V4dGVuZHMoe30sIGNoaWxkUHJvcHMsIHtcbiAgICAgICAgICBjb250ZW50RWRpdGFibGU6IGN1c3RvbUVkaXRhYmxlLFxuICAgICAgICAgIHN1cHByZXNzQ29udGVudEVkaXRhYmxlV2FybmluZzogdHJ1ZVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgdmFyIGNoaWxkID0gUmVhY3QuY3JlYXRlRWxlbWVudChFbGVtZW50LCBjaGlsZFByb3BzLCBSZWFjdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgY29tcG9uZW50UHJvcHMpKTtcblxuICAgICAgcHJvY2Vzc2VkQmxvY2tzLnB1c2goe1xuICAgICAgICBibG9jazogY2hpbGQsXG4gICAgICAgIHdyYXBwZXJUZW1wbGF0ZTogd3JhcHBlclRlbXBsYXRlLFxuICAgICAgICBrZXk6IGtleSxcbiAgICAgICAgb2Zmc2V0S2V5OiBvZmZzZXRLZXlcbiAgICAgIH0pO1xuXG4gICAgICBpZiAod3JhcHBlclRlbXBsYXRlKSB7XG4gICAgICAgIGN1cnJlbnREZXB0aCA9IF9ibG9jay5nZXREZXB0aCgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY3VycmVudERlcHRoID0gbnVsbDtcbiAgICAgIH1cbiAgICAgIGxhc3RXcmFwcGVyVGVtcGxhdGUgPSB3cmFwcGVyVGVtcGxhdGU7XG4gICAgfVxuXG4gICAgLy8gR3JvdXAgY29udGlndW91cyBydW5zIG9mIGJsb2NrcyB0aGF0IGhhdmUgdGhlIHNhbWUgd3JhcHBlclRlbXBsYXRlXG4gICAgdmFyIG91dHB1dEJsb2NrcyA9IFtdO1xuICAgIGZvciAodmFyIF9paSA9IDA7IF9paSA8IHByb2Nlc3NlZEJsb2Nrcy5sZW5ndGg7KSB7XG4gICAgICB2YXIgaW5mbyA9IHByb2Nlc3NlZEJsb2Nrc1tfaWldO1xuICAgICAgaWYgKGluZm8ud3JhcHBlclRlbXBsYXRlKSB7XG4gICAgICAgIHZhciBibG9ja3MgPSBbXTtcbiAgICAgICAgZG8ge1xuICAgICAgICAgIGJsb2Nrcy5wdXNoKHByb2Nlc3NlZEJsb2Nrc1tfaWldLmJsb2NrKTtcbiAgICAgICAgICBfaWkrKztcbiAgICAgICAgfSB3aGlsZSAoX2lpIDwgcHJvY2Vzc2VkQmxvY2tzLmxlbmd0aCAmJiBwcm9jZXNzZWRCbG9ja3NbX2lpXS53cmFwcGVyVGVtcGxhdGUgPT09IGluZm8ud3JhcHBlclRlbXBsYXRlKTtcbiAgICAgICAgdmFyIHdyYXBwZXJFbGVtZW50ID0gUmVhY3QuY2xvbmVFbGVtZW50KGluZm8ud3JhcHBlclRlbXBsYXRlLCB7XG4gICAgICAgICAga2V5OiBpbmZvLmtleSArICctd3JhcCcsXG4gICAgICAgICAgJ2RhdGEtb2Zmc2V0LWtleSc6IGluZm8ub2Zmc2V0S2V5XG4gICAgICAgIH0sIGJsb2Nrcyk7XG4gICAgICAgIG91dHB1dEJsb2Nrcy5wdXNoKHdyYXBwZXJFbGVtZW50KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG91dHB1dEJsb2Nrcy5wdXNoKGluZm8uYmxvY2spO1xuICAgICAgICBfaWkrKztcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICdkaXYnLFxuICAgICAgeyAnZGF0YS1jb250ZW50cyc6ICd0cnVlJyB9LFxuICAgICAgb3V0cHV0QmxvY2tzXG4gICAgKTtcbiAgfTtcblxuICByZXR1cm4gRHJhZnRFZGl0b3JDb250ZW50cztcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxubW9kdWxlLmV4cG9ydHMgPSBEcmFmdEVkaXRvckNvbnRlbnRzOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUVBOzs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBZ0JBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditorContents-core.react.js
