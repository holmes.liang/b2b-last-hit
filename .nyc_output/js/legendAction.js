/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


function legendSelectActionHandler(methodName, payload, ecModel) {
  var selectedMap = {};
  var isToggleSelect = methodName === 'toggleSelected';
  var isSelected; // Update all legend components

  ecModel.eachComponent('legend', function (legendModel) {
    if (isToggleSelect && isSelected != null) {
      // Force other legend has same selected status
      // Or the first is toggled to true and other are toggled to false
      // In the case one legend has some item unSelected in option. And if other legend
      // doesn't has the item, they will assume it is selected.
      legendModel[isSelected ? 'select' : 'unSelect'](payload.name);
    } else {
      legendModel[methodName](payload.name);
      isSelected = legendModel.isSelected(payload.name);
    }

    var legendData = legendModel.getData();
    zrUtil.each(legendData, function (model) {
      var name = model.get('name'); // Wrap element

      if (name === '\n' || name === '') {
        return;
      }

      var isItemSelected = legendModel.isSelected(name);

      if (selectedMap.hasOwnProperty(name)) {
        // Unselected if any legend is unselected
        selectedMap[name] = selectedMap[name] && isItemSelected;
      } else {
        selectedMap[name] = isItemSelected;
      }
    });
  }); // Return the event explicitly

  return {
    name: payload.name,
    selected: selectedMap
  };
}
/**
 * @event legendToggleSelect
 * @type {Object}
 * @property {string} type 'legendToggleSelect'
 * @property {string} [from]
 * @property {string} name Series name or data item name
 */


echarts.registerAction('legendToggleSelect', 'legendselectchanged', zrUtil.curry(legendSelectActionHandler, 'toggleSelected'));
/**
 * @event legendSelect
 * @type {Object}
 * @property {string} type 'legendSelect'
 * @property {string} name Series name or data item name
 */

echarts.registerAction('legendSelect', 'legendselected', zrUtil.curry(legendSelectActionHandler, 'select'));
/**
 * @event legendUnSelect
 * @type {Object}
 * @property {string} type 'legendUnSelect'
 * @property {string} name Series name or data item name
 */

echarts.registerAction('legendUnSelect', 'legendunselected', zrUtil.curry(legendSelectActionHandler, 'unSelect'));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZC9sZWdlbmRBY3Rpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvbGVnZW5kL2xlZ2VuZEFjdGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIGVjaGFydHMgPSByZXF1aXJlKFwiLi4vLi4vZWNoYXJ0c1wiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbmZ1bmN0aW9uIGxlZ2VuZFNlbGVjdEFjdGlvbkhhbmRsZXIobWV0aG9kTmFtZSwgcGF5bG9hZCwgZWNNb2RlbCkge1xuICB2YXIgc2VsZWN0ZWRNYXAgPSB7fTtcbiAgdmFyIGlzVG9nZ2xlU2VsZWN0ID0gbWV0aG9kTmFtZSA9PT0gJ3RvZ2dsZVNlbGVjdGVkJztcbiAgdmFyIGlzU2VsZWN0ZWQ7IC8vIFVwZGF0ZSBhbGwgbGVnZW5kIGNvbXBvbmVudHNcblxuICBlY01vZGVsLmVhY2hDb21wb25lbnQoJ2xlZ2VuZCcsIGZ1bmN0aW9uIChsZWdlbmRNb2RlbCkge1xuICAgIGlmIChpc1RvZ2dsZVNlbGVjdCAmJiBpc1NlbGVjdGVkICE9IG51bGwpIHtcbiAgICAgIC8vIEZvcmNlIG90aGVyIGxlZ2VuZCBoYXMgc2FtZSBzZWxlY3RlZCBzdGF0dXNcbiAgICAgIC8vIE9yIHRoZSBmaXJzdCBpcyB0b2dnbGVkIHRvIHRydWUgYW5kIG90aGVyIGFyZSB0b2dnbGVkIHRvIGZhbHNlXG4gICAgICAvLyBJbiB0aGUgY2FzZSBvbmUgbGVnZW5kIGhhcyBzb21lIGl0ZW0gdW5TZWxlY3RlZCBpbiBvcHRpb24uIEFuZCBpZiBvdGhlciBsZWdlbmRcbiAgICAgIC8vIGRvZXNuJ3QgaGFzIHRoZSBpdGVtLCB0aGV5IHdpbGwgYXNzdW1lIGl0IGlzIHNlbGVjdGVkLlxuICAgICAgbGVnZW5kTW9kZWxbaXNTZWxlY3RlZCA/ICdzZWxlY3QnIDogJ3VuU2VsZWN0J10ocGF5bG9hZC5uYW1lKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGVnZW5kTW9kZWxbbWV0aG9kTmFtZV0ocGF5bG9hZC5uYW1lKTtcbiAgICAgIGlzU2VsZWN0ZWQgPSBsZWdlbmRNb2RlbC5pc1NlbGVjdGVkKHBheWxvYWQubmFtZSk7XG4gICAgfVxuXG4gICAgdmFyIGxlZ2VuZERhdGEgPSBsZWdlbmRNb2RlbC5nZXREYXRhKCk7XG4gICAgenJVdGlsLmVhY2gobGVnZW5kRGF0YSwgZnVuY3Rpb24gKG1vZGVsKSB7XG4gICAgICB2YXIgbmFtZSA9IG1vZGVsLmdldCgnbmFtZScpOyAvLyBXcmFwIGVsZW1lbnRcblxuICAgICAgaWYgKG5hbWUgPT09ICdcXG4nIHx8IG5hbWUgPT09ICcnKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGlzSXRlbVNlbGVjdGVkID0gbGVnZW5kTW9kZWwuaXNTZWxlY3RlZChuYW1lKTtcblxuICAgICAgaWYgKHNlbGVjdGVkTWFwLmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgIC8vIFVuc2VsZWN0ZWQgaWYgYW55IGxlZ2VuZCBpcyB1bnNlbGVjdGVkXG4gICAgICAgIHNlbGVjdGVkTWFwW25hbWVdID0gc2VsZWN0ZWRNYXBbbmFtZV0gJiYgaXNJdGVtU2VsZWN0ZWQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzZWxlY3RlZE1hcFtuYW1lXSA9IGlzSXRlbVNlbGVjdGVkO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTsgLy8gUmV0dXJuIHRoZSBldmVudCBleHBsaWNpdGx5XG5cbiAgcmV0dXJuIHtcbiAgICBuYW1lOiBwYXlsb2FkLm5hbWUsXG4gICAgc2VsZWN0ZWQ6IHNlbGVjdGVkTWFwXG4gIH07XG59XG4vKipcbiAqIEBldmVudCBsZWdlbmRUb2dnbGVTZWxlY3RcbiAqIEB0eXBlIHtPYmplY3R9XG4gKiBAcHJvcGVydHkge3N0cmluZ30gdHlwZSAnbGVnZW5kVG9nZ2xlU2VsZWN0J1xuICogQHByb3BlcnR5IHtzdHJpbmd9IFtmcm9tXVxuICogQHByb3BlcnR5IHtzdHJpbmd9IG5hbWUgU2VyaWVzIG5hbWUgb3IgZGF0YSBpdGVtIG5hbWVcbiAqL1xuXG5cbmVjaGFydHMucmVnaXN0ZXJBY3Rpb24oJ2xlZ2VuZFRvZ2dsZVNlbGVjdCcsICdsZWdlbmRzZWxlY3RjaGFuZ2VkJywgenJVdGlsLmN1cnJ5KGxlZ2VuZFNlbGVjdEFjdGlvbkhhbmRsZXIsICd0b2dnbGVTZWxlY3RlZCcpKTtcbi8qKlxuICogQGV2ZW50IGxlZ2VuZFNlbGVjdFxuICogQHR5cGUge09iamVjdH1cbiAqIEBwcm9wZXJ0eSB7c3RyaW5nfSB0eXBlICdsZWdlbmRTZWxlY3QnXG4gKiBAcHJvcGVydHkge3N0cmluZ30gbmFtZSBTZXJpZXMgbmFtZSBvciBkYXRhIGl0ZW0gbmFtZVxuICovXG5cbmVjaGFydHMucmVnaXN0ZXJBY3Rpb24oJ2xlZ2VuZFNlbGVjdCcsICdsZWdlbmRzZWxlY3RlZCcsIHpyVXRpbC5jdXJyeShsZWdlbmRTZWxlY3RBY3Rpb25IYW5kbGVyLCAnc2VsZWN0JykpO1xuLyoqXG4gKiBAZXZlbnQgbGVnZW5kVW5TZWxlY3RcbiAqIEB0eXBlIHtPYmplY3R9XG4gKiBAcHJvcGVydHkge3N0cmluZ30gdHlwZSAnbGVnZW5kVW5TZWxlY3QnXG4gKiBAcHJvcGVydHkge3N0cmluZ30gbmFtZSBTZXJpZXMgbmFtZSBvciBkYXRhIGl0ZW0gbmFtZVxuICovXG5cbmVjaGFydHMucmVnaXN0ZXJBY3Rpb24oJ2xlZ2VuZFVuU2VsZWN0JywgJ2xlZ2VuZHVuc2VsZWN0ZWQnLCB6clV0aWwuY3VycnkobGVnZW5kU2VsZWN0QWN0aW9uSGFuZGxlciwgJ3VuU2VsZWN0JykpOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTs7Ozs7OztBQU9BIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/legend/legendAction.js
