__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common */ "./src/app/desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/policyholder-view.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItem = function ViewItem(props) {
  return Object(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_13__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, props, {
    layout: defaultLayout
  }));
};

var PolicyholderView =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(PolicyholderView, _ModelWidget);

  function PolicyholderView(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, PolicyholderView);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PolicyholderView).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(PolicyholderView, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadCodeTables(this, ["tel_nation_codes"], true);
      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadCodeTables(this, ["gender", "title", "idtype"], false);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "genePropName",
    value: function genePropName(propName) {
      var propNameFixed = this.props.propNameFixed;
      var propsName = propNameFixed || "ext.Policyholder";
      if (!propName) return propsName;
      return "".concat(propsName, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          propNameFixed = _this$props.propNameFixed,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props, ["model", "propNameFixed"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Full Name").thai("ชื่อ-สกุล").my("နာမည်အပြည့်အစုံ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("title", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("title")), this), " ", " ", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("name"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("ID Type/No.").thai("ประเภท ID / หมายเลข").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("idtype", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("idType")), this), " ", " ", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("idNo"))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Date of Birth").thai("วันเกิด").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].toDateString(lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("dob")))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Gender").thai("เพศ").my("ကျား, မ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_14__["default"].getMasterTableItemText("gender", lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(model, this.genePropName("gender")), this)), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address").thai("ที่อยู่").my("လိပ်စာ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_12__["ShowAddress"], {
        model: model,
        addressFixed: this.genePropName("address"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PolicyholderView.prototype), "initState", this).call(this), {
        codeTables: []
      });
    }
  }]);

  return PolicyholderView;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (PolicyholderView);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL3BvbGljeWhvbGRlci12aWV3LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC9wb2xpY3lob2xkZXItdmlldy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSZWFjdCwgU3R5bGVkfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7TW9kZWxXaWRnZXR9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQge0xhbmd1YWdlLCBVdGlsc30gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7TW9kZWxXaWRnZXRQcm9wc30gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHtTaG93QWRkcmVzc30gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuaW1wb3J0IFZpZXdJdGVtMSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3ZpZXctaXRlbVwiO1xuaW1wb3J0IENvbW1vbkdwY0NsYWltIGZyb20gXCJAZGVzay9jbGFpbXMvaGFuZGluZy9kZXRhaWxzL1NBSUMvZ3BjL3ZpZXcvc2VjdGlvbnMvY29uc2VxdWVuY2UvY29tcG9uZW50cy9jb21tb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgICBsYWJlbENvbDoge1xuICAgICAgICB4czoge3NwYW46IDh9LFxuICAgICAgICBzbToge3NwYW46IDZ9LFxuICAgIH0sXG4gICAgd3JhcHBlckNvbDoge1xuICAgICAgICB4czoge3NwYW46IDE2fSxcbiAgICAgICAgc206IHtzcGFuOiAxM30sXG4gICAgfSxcbn07XG5cbmNvbnN0IFZpZXdJdGVtID0gKHByb3BzOiBhbnkpID0+IFZpZXdJdGVtMSh7Li4ucHJvcHMsIGxheW91dDogZGVmYXVsdExheW91dH0pO1xudHlwZSBQYXllclN0YXRlID0ge1xuICAgIGNvZGVUYWJsZXM6IGFueTtcbn07XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBQYXllclBhZ2VDb21wb25lbnRzID0ge1xuICAgIEJveENvbnRlbnQ6IFN0eWxlZERJVjtcbn07XG5leHBvcnQgdHlwZSBQYXllclByb3BzID0ge1xuICAgIG1vZGVsOiBhbnk7XG4gICAgcHJvcE5hbWVGaXhlZD86IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5jbGFzcyBQb2xpY3lob2xkZXJWaWV3PFAgZXh0ZW5kcyBQYXllclByb3BzLCBTIGV4dGVuZHMgUGF5ZXJTdGF0ZSwgQyBleHRlbmRzIFBheWVyUGFnZUNvbXBvbmVudHM+XG4gICAgZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogUGF5ZXJQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgICAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gICAgfVxuXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIENvbW1vbkdwY0NsYWltLmxvYWRDb2RlVGFibGVzKHRoaXMsIFtcInRlbF9uYXRpb25fY29kZXNcIl0sIHRydWUpO1xuICAgICAgICBDb21tb25HcGNDbGFpbS5sb2FkQ29kZVRhYmxlcyh0aGlzLCBbXCJnZW5kZXJcIiwgXCJ0aXRsZVwiLCBcImlkdHlwZVwiXSwgZmFsc2UpO1xuICAgIH1cblxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIH1cblxuICAgIGdlbmVQcm9wTmFtZShwcm9wTmFtZT86IHN0cmluZykge1xuICAgICAgICBjb25zdCB7cHJvcE5hbWVGaXhlZH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBwcm9wc05hbWUgPSBwcm9wTmFtZUZpeGVkIHx8IFwiZXh0LlBvbGljeWhvbGRlclwiO1xuICAgICAgICBpZiAoIXByb3BOYW1lKSByZXR1cm4gcHJvcHNOYW1lO1xuICAgICAgICByZXR1cm4gYCR7cHJvcHNOYW1lfS4ke3Byb3BOYW1lfWBcbiAgICB9XG5cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICAgICAgY29uc3Qge21vZGVsLCBwcm9wTmFtZUZpeGVkLCAuLi5yZXN0fSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8Qy5Cb3hDb250ZW50IHsuLi5yZXN0fT5cbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkZ1bGwgTmFtZVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LiK4Li34LmI4LitLeC4quC4geC4uOC4pVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5teShcIuGAlOGArOGAmeGAiuGAuuGAoeGAleGAvOGAiuGAuuGAt+GAoeGAheGAr+GAtlwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtDb21tb25HcGNDbGFpbS5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwidGl0bGVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJ0aXRsZVwiKSksIHRoaXMpfSB7XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICB7Xy5nZXQobW9kZWwsIHRoaXMuZ2VuZVByb3BOYW1lKFwibmFtZVwiKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgICAgICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiSUQgVHlwZS9Oby5cIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRCAvIOC4q+C4oeC4suC4ouC5gOC4peC4glwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5teShcIuGAoeGAreGAr+GAhOGAuuGAkuGAruGAoeGAmeGAu+GAreGAr+GAuOGAoeGAheGArOGAuCAvIE5PIOGBi1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtDb21tb25HcGNDbGFpbS5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwiaWR0eXBlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXy5nZXQobW9kZWwsIHRoaXMuZ2VuZVByb3BOYW1lKFwiaWRUeXBlXCIpKSwgdGhpcyl9IHtcIiBcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHtfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJpZE5vXCIpKX1cbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgICAgICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJEYXRlIG9mIEJpcnRoXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnRoYWkoXCLguKfguLHguJnguYDguIHguLTguJRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7VXRpbHMudG9EYXRlU3RyaW5nKF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcImRvYlwiKSkpfVxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkdlbmRlclwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LmA4Lie4LioXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLm15KFwi4YCA4YC74YCs4YC4LCDhgJlcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Q29tbW9uR3BjQ2xhaW0uZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImdlbmRlclwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcImdlbmRlclwiKSksIHRoaXMpfVxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkFkZHJlc3NcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAudGhhaShcIuC4l+C4teC5iOC4reC4ouC4ueC5iFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5teShcIuGAnOGAreGAleGAuuGAheGArFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxTaG93QWRkcmVzcyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkcmVzc0ZpeGVkPXt0aGlzLmdlbmVQcm9wTmFtZShcImFkZHJlc3NcIil9Lz5cbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgIDwvQy5Cb3hDb250ZW50PlxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIEJveENvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAgICAgICBgLFxuICAgICAgICB9IGFzIEM7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgICAgICAgIGNvZGVUYWJsZXM6IFtdLFxuICAgICAgICB9KSBhcyBTO1xuICAgIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgUG9saWN5aG9sZGVyVmlldztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWNBOzs7OztBQUdBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBOzs7QUFHQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7Ozs7QUF4RkE7QUFDQTtBQTBGQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/policyholder-view.tsx
