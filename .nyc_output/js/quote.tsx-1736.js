__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Quote", function() { return Quote; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _basic_quote__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./basic-quote */ "./src/app/desk/quote/SAIC/iar/basic-quote.tsx");
/* harmony import */ var _desk_quote_SAIC_iar_component_render_basic_info__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk/quote/SAIC/iar/component/render-basic-info */ "./src/app/desk/quote/SAIC/iar/component/render-basic-info.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk/quote/SAIC/iar/index */ "./src/app/desk/quote/SAIC/iar/index.tsx");
/* harmony import */ var _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk/quote/SAIC/all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/quote/SAIC/phs/components/side-info */ "./src/app/desk/quote/SAIC/phs/components/side-info.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/quote.tsx";









var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};

var Quote =
/*#__PURE__*/
function (_BasicQuote) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Quote, _BasicQuote);

  function Quote() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Quote);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Quote)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.onUseVoucher = function (campaignCode) {
      _this.setValueToModel(campaignCode, _this.getProp("promo.promoCode"));

      var newCart = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(_this.props.mergePolicyToServiceModel());

      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].CART_MERGE, newCart, {
        loading: true
      }).then(function (response) {
        _this.props.mergePolicyToUIModel((response.body || {}).respData);
      }).catch(function (error) {
        lodash__WEBPACK_IMPORTED_MODULE_8___default.a.set(newCart, _this.getProp("promo.promoCode"), "");

        _this.props.mergePolicyToUIModel(newCart);

        console.error(error);
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Quote, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.getValueFromModel(this.generatePropName("insuranceFund"))) {
        this.setValueToModel("SIF", this.generatePropName("insuranceFund"));
      }

      if (!this.getValueFromModel(this.generatePropName("gstRate"))) {
        this.setValueToModel("0.07", this.generatePropName("gstRate"));
      }

      if (!this.getValueFromModel(this.getProp("siCurrencyCode"))) {
        this.setValueToModel("SGD", this.getProp("siCurrencyCode"));
      }

      if (!this.getValueFromModel(this.getProp("premCurrencyCode"))) {
        this.setValueToModel("SGD", this.getProp("premCurrencyCode"));
      }
    }
  }, {
    key: "renderBasicInfo",
    value: function renderBasicInfo() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_quote_SAIC_iar_component_render_basic_info__WEBPACK_IMPORTED_MODULE_6__["default"], {
        that: this,
        form: form,
        model: model,
        getProp: this.getProp,
        formDateLayout: formDateLayout,
        dataFixed: "policy",
        generatePropName: this.generatePropName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      });
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      var model = this.props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_quote_SAIC_phs_components_side_info__WEBPACK_IMPORTED_MODULE_13__["default"], {
        lineList: _desk_quote_SAIC_iar_index__WEBPACK_IMPORTED_MODULE_9__["list"],
        premiumId: "cartPremium.totalPremium",
        model: model,
        onUseVoucher: this.onUseVoucher,
        dataFixed: "policy",
        activeStep: 'Basic',
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      });
    }
  }, {
    key: "handleNext",
    value: function handleNext() {
      var _this2 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        var newCart = _this2.props.mergePolicyToServiceModel();

        newCart["policy.ext._ui.step"] = _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__["AllSteps"].BUSINESS;
        _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].CART_MERGE, newCart, {
          loading: true
        }).then(function (response) {
          _this2.props.mergePolicyToUIModel(response.body.respData || {});

          if (response.body.respCode !== "0000") return;
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this2, _desk_quote_SAIC_all_steps__WEBPACK_IMPORTED_MODULE_10__["AllSteps"].BUSINESS);
        }).catch(function (error) {
          return console.error(error);
        });
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "policy.ext.".concat(dataIdPrefix, ".").concat(propName);
      return "policy.ext.".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }]);

  return Quote;
}(_basic_quote__WEBPACK_IMPORTED_MODULE_5__["BasicQuote"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvcXVvdGUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvcXVvdGUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJhc2ljUXVvdGUgfSBmcm9tIFwiLi9iYXNpYy1xdW90ZVwiO1xuaW1wb3J0IEJhc2ljSW5mb0NvbXBvbmVudCBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1iYXNpYy1pbmZvXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgbGlzdCB9IGZyb20gXCJAZGVzay9xdW90ZS9TQUlDL2lhci9pbmRleFwiO1xuaW1wb3J0IHsgQWxsU3RlcHMgfSBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9hbGwtc3RlcHNcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgcGFnZVRvIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC93aXphcmRcIjtcbmltcG9ydCBTaWRlSW5mb1BocyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9waHMvY29tcG9uZW50cy9zaWRlLWluZm9cIjtcblxuY29uc3QgZm9ybURhdGVMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxufTtcblxuY2xhc3MgUXVvdGUgZXh0ZW5kcyBCYXNpY1F1b3RlPGFueSwgYW55LCBhbnk+IHtcbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgaWYgKCF0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImluc3VyYW5jZUZ1bmRcIikpKSB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIlNJRlwiLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpbnN1cmFuY2VGdW5kXCIpKTtcbiAgICB9XG4gICAgaWYgKCF0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImdzdFJhdGVcIikpKSB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIjAuMDdcIiwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiZ3N0UmF0ZVwiKSk7XG4gICAgfVxuICAgIGlmICghdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdldFByb3AoXCJzaUN1cnJlbmN5Q29kZVwiKSkpIHtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiU0dEXCIsIHRoaXMuZ2V0UHJvcChcInNpQ3VycmVuY3lDb2RlXCIpKTtcbiAgICB9XG4gICAgaWYgKCF0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2V0UHJvcChcInByZW1DdXJyZW5jeUNvZGVcIikpKSB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIlNHRFwiLCB0aGlzLmdldFByb3AoXCJwcmVtQ3VycmVuY3lDb2RlXCIpKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXJCYXNpY0luZm8oKTogYW55IHtcbiAgICBjb25zdCB7XG4gICAgICBmb3JtLFxuICAgICAgbW9kZWwsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIDxCYXNpY0luZm9Db21wb25lbnRcbiAgICAgIHRoYXQ9e3RoaXN9XG4gICAgICBmb3JtPXtmb3JtfVxuICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgZ2V0UHJvcD17dGhpcy5nZXRQcm9wfVxuICAgICAgZm9ybURhdGVMYXlvdXQ9e2Zvcm1EYXRlTGF5b3V0fVxuICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgZ2VuZXJhdGVQcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lfVxuICAgIC8+O1xuICB9XG5cbiAgcmVuZGVyU2lkZUluZm8oKTogYW55IHtcbiAgICBjb25zdCB7IG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiA8U2lkZUluZm9QaHNcbiAgICAgICAgbGluZUxpc3Q9e2xpc3R9XG4gICAgICAgIHByZW1pdW1JZD17XCJjYXJ0UHJlbWl1bS50b3RhbFByZW1pdW1cIn1cbiAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICBvblVzZVZvdWNoZXI9e3RoaXMub25Vc2VWb3VjaGVyfVxuICAgICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICAgIGFjdGl2ZVN0ZXA9eydCYXNpYyd9Lz5cbiAgfVxuXG4gIGhhbmRsZU5leHQoKSB7XG4gICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKChlcnI6IGFueSwgZmllbGRzVmFsdWU6IGFueSkgPT4ge1xuICAgICAgaWYgKGVycikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb25zdCBuZXdDYXJ0ID0gdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk7XG4gICAgICBuZXdDYXJ0W1wicG9saWN5LmV4dC5fdWkuc3RlcFwiXSA9IEFsbFN0ZXBzLkJVU0lORVNTO1xuICAgICAgQWpheC5wb3N0KEFwaXMuQ0FSVF9NRVJHRSwgbmV3Q2FydCwgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgICB0aGlzLnByb3BzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHJlc3BvbnNlLmJvZHkucmVzcERhdGEgfHwge30pO1xuICAgICAgICAgIGlmIChyZXNwb25zZS5ib2R5LnJlc3BDb2RlICE9PSBcIjAwMDBcIikgcmV0dXJuO1xuICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5CVVNJTkVTUyk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmVycm9yKGVycm9yKSk7XG4gICAgfSk7XG4gIH1cblxuICBvblVzZVZvdWNoZXIgPSAoY2FtcGFpZ25Db2RlOiBhbnkpID0+IHtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChjYW1wYWlnbkNvZGUsIHRoaXMuZ2V0UHJvcChcInByb21vLnByb21vQ29kZVwiKSk7XG4gICAgY29uc3QgbmV3Q2FydCA9IF8uY2xvbmVEZWVwKHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpKTtcbiAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdDYXJ0LCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcbiAgICAgICAgdGhpcy5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbCgocmVzcG9uc2UuYm9keSB8fCB7fSkucmVzcERhdGEpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIF8uc2V0KG5ld0NhcnQsIHRoaXMuZ2V0UHJvcChcInByb21vLnByb21vQ29kZVwiKSwgXCJcIik7XG4gICAgICAgIHRoaXMucHJvcHMubWVyZ2VQb2xpY3lUb1VJTW9kZWwobmV3Q2FydCk7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgfSk7XG4gIH07XG5cbiAgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIGlmIChkYXRhSWRQcmVmaXgpIHJldHVybiBgcG9saWN5LmV4dC4ke2RhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICAgIHJldHVybiBgcG9saWN5LmV4dC4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcm9wKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKF8uaXNFbXB0eShwcm9wTmFtZSkpIHtcbiAgICAgIHJldHVybiBcInBvbGljeVwiO1xuICAgIH1cbiAgICByZXR1cm4gYHBvbGljeS4ke3Byb3BOYW1lfWA7XG4gIH1cbn1cblxuZXhwb3J0IHsgUXVvdGUgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0REE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUF4RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBcEZBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/quote.tsx
