/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _helper = __webpack_require__(/*! ./helper */ "./node_modules/echarts/lib/chart/line/helper.js");

var prepareDataCoordInfo = _helper.prepareDataCoordInfo;
var getStackedOnPoint = _helper.getStackedOnPoint;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// var arrayDiff = require('zrender/src/core/arrayDiff');
// 'zrender/src/core/arrayDiff' has been used before, but it did
// not do well in performance when roam with fixed dataZoom window.
// function convertToIntId(newIdList, oldIdList) {
//     // Generate int id instead of string id.
//     // Compare string maybe slow in score function of arrDiff
//     // Assume id in idList are all unique
//     var idIndicesMap = {};
//     var idx = 0;
//     for (var i = 0; i < newIdList.length; i++) {
//         idIndicesMap[newIdList[i]] = idx;
//         newIdList[i] = idx++;
//     }
//     for (var i = 0; i < oldIdList.length; i++) {
//         var oldId = oldIdList[i];
//         // Same with newIdList
//         if (idIndicesMap[oldId]) {
//             oldIdList[i] = idIndicesMap[oldId];
//         }
//         else {
//             oldIdList[i] = idx++;
//         }
//     }
// }

function diffData(oldData, newData) {
  var diffResult = [];
  newData.diff(oldData).add(function (idx) {
    diffResult.push({
      cmd: '+',
      idx: idx
    });
  }).update(function (newIdx, oldIdx) {
    diffResult.push({
      cmd: '=',
      idx: oldIdx,
      idx1: newIdx
    });
  }).remove(function (idx) {
    diffResult.push({
      cmd: '-',
      idx: idx
    });
  }).execute();
  return diffResult;
}

function _default(oldData, newData, oldStackedOnPoints, newStackedOnPoints, oldCoordSys, newCoordSys, oldValueOrigin, newValueOrigin) {
  var diff = diffData(oldData, newData); // var newIdList = newData.mapArray(newData.getId);
  // var oldIdList = oldData.mapArray(oldData.getId);
  // convertToIntId(newIdList, oldIdList);
  // // FIXME One data ?
  // diff = arrayDiff(oldIdList, newIdList);

  var currPoints = [];
  var nextPoints = []; // Points for stacking base line

  var currStackedPoints = [];
  var nextStackedPoints = [];
  var status = [];
  var sortedIndices = [];
  var rawIndices = [];
  var newDataOldCoordInfo = prepareDataCoordInfo(oldCoordSys, newData, oldValueOrigin);
  var oldDataNewCoordInfo = prepareDataCoordInfo(newCoordSys, oldData, newValueOrigin);

  for (var i = 0; i < diff.length; i++) {
    var diffItem = diff[i];
    var pointAdded = true; // FIXME, animation is not so perfect when dataZoom window moves fast
    // Which is in case remvoing or add more than one data in the tail or head

    switch (diffItem.cmd) {
      case '=':
        var currentPt = oldData.getItemLayout(diffItem.idx);
        var nextPt = newData.getItemLayout(diffItem.idx1); // If previous data is NaN, use next point directly

        if (isNaN(currentPt[0]) || isNaN(currentPt[1])) {
          currentPt = nextPt.slice();
        }

        currPoints.push(currentPt);
        nextPoints.push(nextPt);
        currStackedPoints.push(oldStackedOnPoints[diffItem.idx]);
        nextStackedPoints.push(newStackedOnPoints[diffItem.idx1]);
        rawIndices.push(newData.getRawIndex(diffItem.idx1));
        break;

      case '+':
        var idx = diffItem.idx;
        currPoints.push(oldCoordSys.dataToPoint([newData.get(newDataOldCoordInfo.dataDimsForPoint[0], idx), newData.get(newDataOldCoordInfo.dataDimsForPoint[1], idx)]));
        nextPoints.push(newData.getItemLayout(idx).slice());
        currStackedPoints.push(getStackedOnPoint(newDataOldCoordInfo, oldCoordSys, newData, idx));
        nextStackedPoints.push(newStackedOnPoints[idx]);
        rawIndices.push(newData.getRawIndex(idx));
        break;

      case '-':
        var idx = diffItem.idx;
        var rawIndex = oldData.getRawIndex(idx); // Data is replaced. In the case of dynamic data queue
        // FIXME FIXME FIXME

        if (rawIndex !== idx) {
          currPoints.push(oldData.getItemLayout(idx));
          nextPoints.push(newCoordSys.dataToPoint([oldData.get(oldDataNewCoordInfo.dataDimsForPoint[0], idx), oldData.get(oldDataNewCoordInfo.dataDimsForPoint[1], idx)]));
          currStackedPoints.push(oldStackedOnPoints[idx]);
          nextStackedPoints.push(getStackedOnPoint(oldDataNewCoordInfo, newCoordSys, oldData, idx));
          rawIndices.push(rawIndex);
        } else {
          pointAdded = false;
        }

    } // Original indices


    if (pointAdded) {
      status.push(diffItem);
      sortedIndices.push(sortedIndices.length);
    }
  } // Diff result may be crossed if all items are changed
  // Sort by data index


  sortedIndices.sort(function (a, b) {
    return rawIndices[a] - rawIndices[b];
  });
  var sortedCurrPoints = [];
  var sortedNextPoints = [];
  var sortedCurrStackedPoints = [];
  var sortedNextStackedPoints = [];
  var sortedStatus = [];

  for (var i = 0; i < sortedIndices.length; i++) {
    var idx = sortedIndices[i];
    sortedCurrPoints[i] = currPoints[idx];
    sortedNextPoints[i] = nextPoints[idx];
    sortedCurrStackedPoints[i] = currStackedPoints[idx];
    sortedNextStackedPoints[i] = nextStackedPoints[idx];
    sortedStatus[i] = status[idx];
  }

  return {
    current: sortedCurrPoints,
    next: sortedNextPoints,
    stackedOnCurrent: sortedCurrStackedPoints,
    stackedOnNext: sortedNextStackedPoints,
    status: sortedStatus
  };
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9saW5lQW5pbWF0aW9uRGlmZi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NoYXJ0L2xpbmUvbGluZUFuaW1hdGlvbkRpZmYuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfaGVscGVyID0gcmVxdWlyZShcIi4vaGVscGVyXCIpO1xuXG52YXIgcHJlcGFyZURhdGFDb29yZEluZm8gPSBfaGVscGVyLnByZXBhcmVEYXRhQ29vcmRJbmZvO1xudmFyIGdldFN0YWNrZWRPblBvaW50ID0gX2hlbHBlci5nZXRTdGFja2VkT25Qb2ludDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gdmFyIGFycmF5RGlmZiA9IHJlcXVpcmUoJ3pyZW5kZXIvc3JjL2NvcmUvYXJyYXlEaWZmJyk7XG4vLyAnenJlbmRlci9zcmMvY29yZS9hcnJheURpZmYnIGhhcyBiZWVuIHVzZWQgYmVmb3JlLCBidXQgaXQgZGlkXG4vLyBub3QgZG8gd2VsbCBpbiBwZXJmb3JtYW5jZSB3aGVuIHJvYW0gd2l0aCBmaXhlZCBkYXRhWm9vbSB3aW5kb3cuXG4vLyBmdW5jdGlvbiBjb252ZXJ0VG9JbnRJZChuZXdJZExpc3QsIG9sZElkTGlzdCkge1xuLy8gICAgIC8vIEdlbmVyYXRlIGludCBpZCBpbnN0ZWFkIG9mIHN0cmluZyBpZC5cbi8vICAgICAvLyBDb21wYXJlIHN0cmluZyBtYXliZSBzbG93IGluIHNjb3JlIGZ1bmN0aW9uIG9mIGFyckRpZmZcbi8vICAgICAvLyBBc3N1bWUgaWQgaW4gaWRMaXN0IGFyZSBhbGwgdW5pcXVlXG4vLyAgICAgdmFyIGlkSW5kaWNlc01hcCA9IHt9O1xuLy8gICAgIHZhciBpZHggPSAwO1xuLy8gICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbmV3SWRMaXN0Lmxlbmd0aDsgaSsrKSB7XG4vLyAgICAgICAgIGlkSW5kaWNlc01hcFtuZXdJZExpc3RbaV1dID0gaWR4O1xuLy8gICAgICAgICBuZXdJZExpc3RbaV0gPSBpZHgrKztcbi8vICAgICB9XG4vLyAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBvbGRJZExpc3QubGVuZ3RoOyBpKyspIHtcbi8vICAgICAgICAgdmFyIG9sZElkID0gb2xkSWRMaXN0W2ldO1xuLy8gICAgICAgICAvLyBTYW1lIHdpdGggbmV3SWRMaXN0XG4vLyAgICAgICAgIGlmIChpZEluZGljZXNNYXBbb2xkSWRdKSB7XG4vLyAgICAgICAgICAgICBvbGRJZExpc3RbaV0gPSBpZEluZGljZXNNYXBbb2xkSWRdO1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIGVsc2Uge1xuLy8gICAgICAgICAgICAgb2xkSWRMaXN0W2ldID0gaWR4Kys7XG4vLyAgICAgICAgIH1cbi8vICAgICB9XG4vLyB9XG5mdW5jdGlvbiBkaWZmRGF0YShvbGREYXRhLCBuZXdEYXRhKSB7XG4gIHZhciBkaWZmUmVzdWx0ID0gW107XG4gIG5ld0RhdGEuZGlmZihvbGREYXRhKS5hZGQoZnVuY3Rpb24gKGlkeCkge1xuICAgIGRpZmZSZXN1bHQucHVzaCh7XG4gICAgICBjbWQ6ICcrJyxcbiAgICAgIGlkeDogaWR4XG4gICAgfSk7XG4gIH0pLnVwZGF0ZShmdW5jdGlvbiAobmV3SWR4LCBvbGRJZHgpIHtcbiAgICBkaWZmUmVzdWx0LnB1c2goe1xuICAgICAgY21kOiAnPScsXG4gICAgICBpZHg6IG9sZElkeCxcbiAgICAgIGlkeDE6IG5ld0lkeFxuICAgIH0pO1xuICB9KS5yZW1vdmUoZnVuY3Rpb24gKGlkeCkge1xuICAgIGRpZmZSZXN1bHQucHVzaCh7XG4gICAgICBjbWQ6ICctJyxcbiAgICAgIGlkeDogaWR4XG4gICAgfSk7XG4gIH0pLmV4ZWN1dGUoKTtcbiAgcmV0dXJuIGRpZmZSZXN1bHQ7XG59XG5cbmZ1bmN0aW9uIF9kZWZhdWx0KG9sZERhdGEsIG5ld0RhdGEsIG9sZFN0YWNrZWRPblBvaW50cywgbmV3U3RhY2tlZE9uUG9pbnRzLCBvbGRDb29yZFN5cywgbmV3Q29vcmRTeXMsIG9sZFZhbHVlT3JpZ2luLCBuZXdWYWx1ZU9yaWdpbikge1xuICB2YXIgZGlmZiA9IGRpZmZEYXRhKG9sZERhdGEsIG5ld0RhdGEpOyAvLyB2YXIgbmV3SWRMaXN0ID0gbmV3RGF0YS5tYXBBcnJheShuZXdEYXRhLmdldElkKTtcbiAgLy8gdmFyIG9sZElkTGlzdCA9IG9sZERhdGEubWFwQXJyYXkob2xkRGF0YS5nZXRJZCk7XG4gIC8vIGNvbnZlcnRUb0ludElkKG5ld0lkTGlzdCwgb2xkSWRMaXN0KTtcbiAgLy8gLy8gRklYTUUgT25lIGRhdGEgP1xuICAvLyBkaWZmID0gYXJyYXlEaWZmKG9sZElkTGlzdCwgbmV3SWRMaXN0KTtcblxuICB2YXIgY3VyclBvaW50cyA9IFtdO1xuICB2YXIgbmV4dFBvaW50cyA9IFtdOyAvLyBQb2ludHMgZm9yIHN0YWNraW5nIGJhc2UgbGluZVxuXG4gIHZhciBjdXJyU3RhY2tlZFBvaW50cyA9IFtdO1xuICB2YXIgbmV4dFN0YWNrZWRQb2ludHMgPSBbXTtcbiAgdmFyIHN0YXR1cyA9IFtdO1xuICB2YXIgc29ydGVkSW5kaWNlcyA9IFtdO1xuICB2YXIgcmF3SW5kaWNlcyA9IFtdO1xuICB2YXIgbmV3RGF0YU9sZENvb3JkSW5mbyA9IHByZXBhcmVEYXRhQ29vcmRJbmZvKG9sZENvb3JkU3lzLCBuZXdEYXRhLCBvbGRWYWx1ZU9yaWdpbik7XG4gIHZhciBvbGREYXRhTmV3Q29vcmRJbmZvID0gcHJlcGFyZURhdGFDb29yZEluZm8obmV3Q29vcmRTeXMsIG9sZERhdGEsIG5ld1ZhbHVlT3JpZ2luKTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGRpZmYubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgZGlmZkl0ZW0gPSBkaWZmW2ldO1xuICAgIHZhciBwb2ludEFkZGVkID0gdHJ1ZTsgLy8gRklYTUUsIGFuaW1hdGlvbiBpcyBub3Qgc28gcGVyZmVjdCB3aGVuIGRhdGFab29tIHdpbmRvdyBtb3ZlcyBmYXN0XG4gICAgLy8gV2hpY2ggaXMgaW4gY2FzZSByZW12b2luZyBvciBhZGQgbW9yZSB0aGFuIG9uZSBkYXRhIGluIHRoZSB0YWlsIG9yIGhlYWRcblxuICAgIHN3aXRjaCAoZGlmZkl0ZW0uY21kKSB7XG4gICAgICBjYXNlICc9JzpcbiAgICAgICAgdmFyIGN1cnJlbnRQdCA9IG9sZERhdGEuZ2V0SXRlbUxheW91dChkaWZmSXRlbS5pZHgpO1xuICAgICAgICB2YXIgbmV4dFB0ID0gbmV3RGF0YS5nZXRJdGVtTGF5b3V0KGRpZmZJdGVtLmlkeDEpOyAvLyBJZiBwcmV2aW91cyBkYXRhIGlzIE5hTiwgdXNlIG5leHQgcG9pbnQgZGlyZWN0bHlcblxuICAgICAgICBpZiAoaXNOYU4oY3VycmVudFB0WzBdKSB8fCBpc05hTihjdXJyZW50UHRbMV0pKSB7XG4gICAgICAgICAgY3VycmVudFB0ID0gbmV4dFB0LnNsaWNlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBjdXJyUG9pbnRzLnB1c2goY3VycmVudFB0KTtcbiAgICAgICAgbmV4dFBvaW50cy5wdXNoKG5leHRQdCk7XG4gICAgICAgIGN1cnJTdGFja2VkUG9pbnRzLnB1c2gob2xkU3RhY2tlZE9uUG9pbnRzW2RpZmZJdGVtLmlkeF0pO1xuICAgICAgICBuZXh0U3RhY2tlZFBvaW50cy5wdXNoKG5ld1N0YWNrZWRPblBvaW50c1tkaWZmSXRlbS5pZHgxXSk7XG4gICAgICAgIHJhd0luZGljZXMucHVzaChuZXdEYXRhLmdldFJhd0luZGV4KGRpZmZJdGVtLmlkeDEpKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJysnOlxuICAgICAgICB2YXIgaWR4ID0gZGlmZkl0ZW0uaWR4O1xuICAgICAgICBjdXJyUG9pbnRzLnB1c2gob2xkQ29vcmRTeXMuZGF0YVRvUG9pbnQoW25ld0RhdGEuZ2V0KG5ld0RhdGFPbGRDb29yZEluZm8uZGF0YURpbXNGb3JQb2ludFswXSwgaWR4KSwgbmV3RGF0YS5nZXQobmV3RGF0YU9sZENvb3JkSW5mby5kYXRhRGltc0ZvclBvaW50WzFdLCBpZHgpXSkpO1xuICAgICAgICBuZXh0UG9pbnRzLnB1c2gobmV3RGF0YS5nZXRJdGVtTGF5b3V0KGlkeCkuc2xpY2UoKSk7XG4gICAgICAgIGN1cnJTdGFja2VkUG9pbnRzLnB1c2goZ2V0U3RhY2tlZE9uUG9pbnQobmV3RGF0YU9sZENvb3JkSW5mbywgb2xkQ29vcmRTeXMsIG5ld0RhdGEsIGlkeCkpO1xuICAgICAgICBuZXh0U3RhY2tlZFBvaW50cy5wdXNoKG5ld1N0YWNrZWRPblBvaW50c1tpZHhdKTtcbiAgICAgICAgcmF3SW5kaWNlcy5wdXNoKG5ld0RhdGEuZ2V0UmF3SW5kZXgoaWR4KSk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlICctJzpcbiAgICAgICAgdmFyIGlkeCA9IGRpZmZJdGVtLmlkeDtcbiAgICAgICAgdmFyIHJhd0luZGV4ID0gb2xkRGF0YS5nZXRSYXdJbmRleChpZHgpOyAvLyBEYXRhIGlzIHJlcGxhY2VkLiBJbiB0aGUgY2FzZSBvZiBkeW5hbWljIGRhdGEgcXVldWVcbiAgICAgICAgLy8gRklYTUUgRklYTUUgRklYTUVcblxuICAgICAgICBpZiAocmF3SW5kZXggIT09IGlkeCkge1xuICAgICAgICAgIGN1cnJQb2ludHMucHVzaChvbGREYXRhLmdldEl0ZW1MYXlvdXQoaWR4KSk7XG4gICAgICAgICAgbmV4dFBvaW50cy5wdXNoKG5ld0Nvb3JkU3lzLmRhdGFUb1BvaW50KFtvbGREYXRhLmdldChvbGREYXRhTmV3Q29vcmRJbmZvLmRhdGFEaW1zRm9yUG9pbnRbMF0sIGlkeCksIG9sZERhdGEuZ2V0KG9sZERhdGFOZXdDb29yZEluZm8uZGF0YURpbXNGb3JQb2ludFsxXSwgaWR4KV0pKTtcbiAgICAgICAgICBjdXJyU3RhY2tlZFBvaW50cy5wdXNoKG9sZFN0YWNrZWRPblBvaW50c1tpZHhdKTtcbiAgICAgICAgICBuZXh0U3RhY2tlZFBvaW50cy5wdXNoKGdldFN0YWNrZWRPblBvaW50KG9sZERhdGFOZXdDb29yZEluZm8sIG5ld0Nvb3JkU3lzLCBvbGREYXRhLCBpZHgpKTtcbiAgICAgICAgICByYXdJbmRpY2VzLnB1c2gocmF3SW5kZXgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHBvaW50QWRkZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgfSAvLyBPcmlnaW5hbCBpbmRpY2VzXG5cblxuICAgIGlmIChwb2ludEFkZGVkKSB7XG4gICAgICBzdGF0dXMucHVzaChkaWZmSXRlbSk7XG4gICAgICBzb3J0ZWRJbmRpY2VzLnB1c2goc29ydGVkSW5kaWNlcy5sZW5ndGgpO1xuICAgIH1cbiAgfSAvLyBEaWZmIHJlc3VsdCBtYXkgYmUgY3Jvc3NlZCBpZiBhbGwgaXRlbXMgYXJlIGNoYW5nZWRcbiAgLy8gU29ydCBieSBkYXRhIGluZGV4XG5cblxuICBzb3J0ZWRJbmRpY2VzLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gcmF3SW5kaWNlc1thXSAtIHJhd0luZGljZXNbYl07XG4gIH0pO1xuICB2YXIgc29ydGVkQ3VyclBvaW50cyA9IFtdO1xuICB2YXIgc29ydGVkTmV4dFBvaW50cyA9IFtdO1xuICB2YXIgc29ydGVkQ3VyclN0YWNrZWRQb2ludHMgPSBbXTtcbiAgdmFyIHNvcnRlZE5leHRTdGFja2VkUG9pbnRzID0gW107XG4gIHZhciBzb3J0ZWRTdGF0dXMgPSBbXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHNvcnRlZEluZGljZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgaWR4ID0gc29ydGVkSW5kaWNlc1tpXTtcbiAgICBzb3J0ZWRDdXJyUG9pbnRzW2ldID0gY3VyclBvaW50c1tpZHhdO1xuICAgIHNvcnRlZE5leHRQb2ludHNbaV0gPSBuZXh0UG9pbnRzW2lkeF07XG4gICAgc29ydGVkQ3VyclN0YWNrZWRQb2ludHNbaV0gPSBjdXJyU3RhY2tlZFBvaW50c1tpZHhdO1xuICAgIHNvcnRlZE5leHRTdGFja2VkUG9pbnRzW2ldID0gbmV4dFN0YWNrZWRQb2ludHNbaWR4XTtcbiAgICBzb3J0ZWRTdGF0dXNbaV0gPSBzdGF0dXNbaWR4XTtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgY3VycmVudDogc29ydGVkQ3VyclBvaW50cyxcbiAgICBuZXh0OiBzb3J0ZWROZXh0UG9pbnRzLFxuICAgIHN0YWNrZWRPbkN1cnJlbnQ6IHNvcnRlZEN1cnJTdGFja2VkUG9pbnRzLFxuICAgIHN0YWNrZWRPbk5leHQ6IHNvcnRlZE5leHRTdGFja2VkUG9pbnRzLFxuICAgIHN0YXR1czogc29ydGVkU3RhdHVzXG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBdkNBO0FBQ0E7QUFDQTtBQXlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/line/lineAnimationDiff.js
