__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toArray", function() { return toArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getActiveIndex", function() { return getActiveIndex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getActiveKey", function() { return getActiveKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTransform", function() { return setTransform; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTransform3dSupported", function() { return isTransform3dSupported; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTransition", function() { return setTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTransformPropValue", function() { return getTransformPropValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isVertical", function() { return isVertical; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTransformByIndex", function() { return getTransformByIndex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMarginStyle", function() { return getMarginStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getStyle", function() { return getStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setPxStyle", function() { return setPxStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDataAttr", function() { return getDataAttr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLeft", function() { return getLeft; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTop", function() { return getTop; });
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


function toArray(children) {
  // allow [c,[a,b]]
  var c = [];
  react__WEBPACK_IMPORTED_MODULE_1___default.a.Children.forEach(children, function (child) {
    if (child) {
      c.push(child);
    }
  });
  return c;
}
function getActiveIndex(children, activeKey) {
  var c = toArray(children);

  for (var i = 0; i < c.length; i++) {
    if (c[i].key === activeKey) {
      return i;
    }
  }

  return -1;
}
function getActiveKey(children, index) {
  var c = toArray(children);
  return c[index].key;
}
function setTransform(style, v) {
  style.transform = v;
  style.webkitTransform = v;
  style.mozTransform = v;
}
function isTransform3dSupported(style) {
  return ('transform' in style || 'webkitTransform' in style || 'MozTransform' in style) && window.atob;
}
function setTransition(style, v) {
  style.transition = v;
  style.webkitTransition = v;
  style.MozTransition = v;
}
function getTransformPropValue(v) {
  return {
    transform: v,
    WebkitTransform: v,
    MozTransform: v
  };
}
function isVertical(tabBarPosition) {
  return tabBarPosition === 'left' || tabBarPosition === 'right';
}
function getTransformByIndex(index, tabBarPosition) {
  var direction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'ltr';
  var translate = isVertical(tabBarPosition) ? 'translateY' : 'translateX';

  if (!isVertical(tabBarPosition) && direction === 'rtl') {
    return translate + '(' + index * 100 + '%) translateZ(0)';
  }

  return translate + '(' + -index * 100 + '%) translateZ(0)';
}
function getMarginStyle(index, tabBarPosition) {
  var marginDirection = isVertical(tabBarPosition) ? 'marginTop' : 'marginLeft';
  return babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, marginDirection, -index * 100 + '%');
}
function getStyle(el, property) {
  return +window.getComputedStyle(el).getPropertyValue(property).replace('px', '');
}
function setPxStyle(el, value, vertical) {
  value = vertical ? '0px, ' + value + 'px, 0px' : value + 'px, 0px, 0px';
  setTransform(el.style, 'translate3d(' + value + ')');
}
function getDataAttr(props) {
  return Object.keys(props).reduce(function (prev, key) {
    if (key.substr(0, 5) === 'aria-' || key.substr(0, 5) === 'data-' || key === 'role') {
      prev[key] = props[key];
    }

    return prev;
  }, {});
}

function toNum(style, property) {
  return +style.getPropertyValue(property).replace('px', '');
}

function getTypeValue(start, current, end, tabNode, wrapperNode) {
  var total = getStyle(wrapperNode, 'padding-' + start);

  if (!tabNode || !tabNode.parentNode) {
    return total;
  }

  var childNodes = tabNode.parentNode.childNodes;
  Array.prototype.some.call(childNodes, function (node) {
    var style = window.getComputedStyle(node);

    if (node !== tabNode) {
      total += toNum(style, 'margin-' + start);
      total += node[current];
      total += toNum(style, 'margin-' + end);

      if (style.boxSizing === 'content-box') {
        total += toNum(style, 'border-' + start + '-width') + toNum(style, 'border-' + end + '-width');
      }

      return false;
    } // We need count current node margin
    // ref: https://github.com/react-component/tabs/pull/139#issuecomment-431005262


    total += toNum(style, 'margin-' + start);
    return true;
  });
  return total;
}

function getLeft(tabNode, wrapperNode) {
  return getTypeValue('left', 'offsetWidth', 'right', tabNode, wrapperNode);
}
function getTop(tabNode, wrapperNode) {
  return getTypeValue('top', 'offsetHeight', 'bottom', tabNode, wrapperNode);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFicy9lcy91dGlscy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRhYnMvZXMvdXRpbHMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9kZWZpbmVQcm9wZXJ0eSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcblxuZXhwb3J0IGZ1bmN0aW9uIHRvQXJyYXkoY2hpbGRyZW4pIHtcbiAgLy8gYWxsb3cgW2MsW2EsYl1dXG4gIHZhciBjID0gW107XG4gIFJlYWN0LkNoaWxkcmVuLmZvckVhY2goY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgIGlmIChjaGlsZCkge1xuICAgICAgYy5wdXNoKGNoaWxkKTtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gYztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEFjdGl2ZUluZGV4KGNoaWxkcmVuLCBhY3RpdmVLZXkpIHtcbiAgdmFyIGMgPSB0b0FycmF5KGNoaWxkcmVuKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBjLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKGNbaV0ua2V5ID09PSBhY3RpdmVLZXkpIHtcbiAgICAgIHJldHVybiBpO1xuICAgIH1cbiAgfVxuICByZXR1cm4gLTE7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRBY3RpdmVLZXkoY2hpbGRyZW4sIGluZGV4KSB7XG4gIHZhciBjID0gdG9BcnJheShjaGlsZHJlbik7XG4gIHJldHVybiBjW2luZGV4XS5rZXk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRUcmFuc2Zvcm0oc3R5bGUsIHYpIHtcbiAgc3R5bGUudHJhbnNmb3JtID0gdjtcbiAgc3R5bGUud2Via2l0VHJhbnNmb3JtID0gdjtcbiAgc3R5bGUubW96VHJhbnNmb3JtID0gdjtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzVHJhbnNmb3JtM2RTdXBwb3J0ZWQoc3R5bGUpIHtcbiAgcmV0dXJuICgndHJhbnNmb3JtJyBpbiBzdHlsZSB8fCAnd2Via2l0VHJhbnNmb3JtJyBpbiBzdHlsZSB8fCAnTW96VHJhbnNmb3JtJyBpbiBzdHlsZSkgJiYgd2luZG93LmF0b2I7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRUcmFuc2l0aW9uKHN0eWxlLCB2KSB7XG4gIHN0eWxlLnRyYW5zaXRpb24gPSB2O1xuICBzdHlsZS53ZWJraXRUcmFuc2l0aW9uID0gdjtcbiAgc3R5bGUuTW96VHJhbnNpdGlvbiA9IHY7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRUcmFuc2Zvcm1Qcm9wVmFsdWUodikge1xuICByZXR1cm4ge1xuICAgIHRyYW5zZm9ybTogdixcbiAgICBXZWJraXRUcmFuc2Zvcm06IHYsXG4gICAgTW96VHJhbnNmb3JtOiB2XG4gIH07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1ZlcnRpY2FsKHRhYkJhclBvc2l0aW9uKSB7XG4gIHJldHVybiB0YWJCYXJQb3NpdGlvbiA9PT0gJ2xlZnQnIHx8IHRhYkJhclBvc2l0aW9uID09PSAncmlnaHQnO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VHJhbnNmb3JtQnlJbmRleChpbmRleCwgdGFiQmFyUG9zaXRpb24pIHtcbiAgdmFyIGRpcmVjdGlvbiA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogJ2x0cic7XG5cbiAgdmFyIHRyYW5zbGF0ZSA9IGlzVmVydGljYWwodGFiQmFyUG9zaXRpb24pID8gJ3RyYW5zbGF0ZVknIDogJ3RyYW5zbGF0ZVgnO1xuXG4gIGlmICghaXNWZXJ0aWNhbCh0YWJCYXJQb3NpdGlvbikgJiYgZGlyZWN0aW9uID09PSAncnRsJykge1xuICAgIHJldHVybiB0cmFuc2xhdGUgKyAnKCcgKyBpbmRleCAqIDEwMCArICclKSB0cmFuc2xhdGVaKDApJztcbiAgfVxuICByZXR1cm4gdHJhbnNsYXRlICsgJygnICsgLWluZGV4ICogMTAwICsgJyUpIHRyYW5zbGF0ZVooMCknO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TWFyZ2luU3R5bGUoaW5kZXgsIHRhYkJhclBvc2l0aW9uKSB7XG4gIHZhciBtYXJnaW5EaXJlY3Rpb24gPSBpc1ZlcnRpY2FsKHRhYkJhclBvc2l0aW9uKSA/ICdtYXJnaW5Ub3AnIDogJ21hcmdpbkxlZnQnO1xuICByZXR1cm4gX2RlZmluZVByb3BlcnR5KHt9LCBtYXJnaW5EaXJlY3Rpb24sIC1pbmRleCAqIDEwMCArICclJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTdHlsZShlbCwgcHJvcGVydHkpIHtcbiAgcmV0dXJuICt3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbCkuZ2V0UHJvcGVydHlWYWx1ZShwcm9wZXJ0eSkucmVwbGFjZSgncHgnLCAnJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzZXRQeFN0eWxlKGVsLCB2YWx1ZSwgdmVydGljYWwpIHtcbiAgdmFsdWUgPSB2ZXJ0aWNhbCA/ICcwcHgsICcgKyB2YWx1ZSArICdweCwgMHB4JyA6IHZhbHVlICsgJ3B4LCAwcHgsIDBweCc7XG4gIHNldFRyYW5zZm9ybShlbC5zdHlsZSwgJ3RyYW5zbGF0ZTNkKCcgKyB2YWx1ZSArICcpJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREYXRhQXR0cihwcm9wcykge1xuICByZXR1cm4gT2JqZWN0LmtleXMocHJvcHMpLnJlZHVjZShmdW5jdGlvbiAocHJldiwga2V5KSB7XG4gICAgaWYgKGtleS5zdWJzdHIoMCwgNSkgPT09ICdhcmlhLScgfHwga2V5LnN1YnN0cigwLCA1KSA9PT0gJ2RhdGEtJyB8fCBrZXkgPT09ICdyb2xlJykge1xuICAgICAgcHJldltrZXldID0gcHJvcHNba2V5XTtcbiAgICB9XG4gICAgcmV0dXJuIHByZXY7XG4gIH0sIHt9KTtcbn1cblxuZnVuY3Rpb24gdG9OdW0oc3R5bGUsIHByb3BlcnR5KSB7XG4gIHJldHVybiArc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShwcm9wZXJ0eSkucmVwbGFjZSgncHgnLCAnJyk7XG59XG5cbmZ1bmN0aW9uIGdldFR5cGVWYWx1ZShzdGFydCwgY3VycmVudCwgZW5kLCB0YWJOb2RlLCB3cmFwcGVyTm9kZSkge1xuICB2YXIgdG90YWwgPSBnZXRTdHlsZSh3cmFwcGVyTm9kZSwgJ3BhZGRpbmctJyArIHN0YXJ0KTtcbiAgaWYgKCF0YWJOb2RlIHx8ICF0YWJOb2RlLnBhcmVudE5vZGUpIHtcbiAgICByZXR1cm4gdG90YWw7XG4gIH1cblxuICB2YXIgY2hpbGROb2RlcyA9IHRhYk5vZGUucGFyZW50Tm9kZS5jaGlsZE5vZGVzO1xuXG4gIEFycmF5LnByb3RvdHlwZS5zb21lLmNhbGwoY2hpbGROb2RlcywgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICB2YXIgc3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShub2RlKTtcblxuICAgIGlmIChub2RlICE9PSB0YWJOb2RlKSB7XG4gICAgICB0b3RhbCArPSB0b051bShzdHlsZSwgJ21hcmdpbi0nICsgc3RhcnQpO1xuICAgICAgdG90YWwgKz0gbm9kZVtjdXJyZW50XTtcbiAgICAgIHRvdGFsICs9IHRvTnVtKHN0eWxlLCAnbWFyZ2luLScgKyBlbmQpO1xuXG4gICAgICBpZiAoc3R5bGUuYm94U2l6aW5nID09PSAnY29udGVudC1ib3gnKSB7XG4gICAgICAgIHRvdGFsICs9IHRvTnVtKHN0eWxlLCAnYm9yZGVyLScgKyBzdGFydCArICctd2lkdGgnKSArIHRvTnVtKHN0eWxlLCAnYm9yZGVyLScgKyBlbmQgKyAnLXdpZHRoJyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gV2UgbmVlZCBjb3VudCBjdXJyZW50IG5vZGUgbWFyZ2luXG4gICAgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtY29tcG9uZW50L3RhYnMvcHVsbC8xMzkjaXNzdWVjb21tZW50LTQzMTAwNTI2MlxuICAgIHRvdGFsICs9IHRvTnVtKHN0eWxlLCAnbWFyZ2luLScgKyBzdGFydCk7XG5cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfSk7XG5cbiAgcmV0dXJuIHRvdGFsO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0TGVmdCh0YWJOb2RlLCB3cmFwcGVyTm9kZSkge1xuICByZXR1cm4gZ2V0VHlwZVZhbHVlKCdsZWZ0JywgJ29mZnNldFdpZHRoJywgJ3JpZ2h0JywgdGFiTm9kZSwgd3JhcHBlck5vZGUpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VG9wKHRhYk5vZGUsIHdyYXBwZXJOb2RlKSB7XG4gIHJldHVybiBnZXRUeXBlVmFsdWUoJ3RvcCcsICdvZmZzZXRIZWlnaHQnLCAnYm90dG9tJywgdGFiTm9kZSwgd3JhcHBlck5vZGUpO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-tabs/es/utils.js
