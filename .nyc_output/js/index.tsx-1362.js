__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");




var CommonGpcClaim = function CommonGpcClaim() {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, CommonGpcClaim);

  this.loadCodeTables =
  /*#__PURE__*/
  function () {
    var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_this, getArr, isPF) {
      var codeTables, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              arr = [];
              (getArr || []).forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _this.getHelpers().getAjax().get(isPF ? "/pf/mastertable/".concat(item) : "/mastertable", isPF ? {} : {
                    itntCode: _this.getValueFromModel("itntCode"),
                    productCode: _this.MRC ? _this.MRC : _this.getValueFromModel("productCode"),
                    productVersion: _this.getValueFromModel("productVersion"),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = isPF ? respData || [] : respData.items || [];
                    resolve();
                  });
                }));
              });
              Promise.all(arr).then(function (values) {
                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x, _x2, _x3) {
      return _ref.apply(this, arguments);
    };
  }();

  this.getMasterTableItemText = function (tableName, itemId, _this) {
    var codeTables = _this.state.codeTables;

    if (itemId) {
      var item = (codeTables[tableName] || []).find(function (option) {
        return itemId.toString() === option.id.toString();
      }) || {};
      return item.text || "";
    } else {
      return "";
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (new CommonGpcClaim());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY2xhaW1zL2hhbmRpbmcvZGV0YWlscy9TQUlDL2dwYy92aWV3L3NlY3Rpb25zL2NvbnNlcXVlbmNlL2NvbXBvbmVudHMvY29tbW9uL2luZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NsYWltcy9oYW5kaW5nL2RldGFpbHMvU0FJQy9ncGMvdmlldy9zZWN0aW9ucy9jb25zZXF1ZW5jZS9jb21wb25lbnRzL2NvbW1vbi9pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuXG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuY2xhc3MgQ29tbW9uR3BjQ2xhaW0ge1xuXG4gIGxvYWRDb2RlVGFibGVzID0gYXN5bmMgKF90aGlzOiBhbnksIGdldEFycjogYW55W10sIGlzUEY/OiBib29sZWFuKSA9PiB7XG4gICAgbGV0IHsgY29kZVRhYmxlcyB9ID0gX3RoaXMuc3RhdGU7XG4gICAgbGV0IGFycjogYW55ID0gW107XG4gICAgKGdldEFyciB8fCBbXSkuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgIGFyci5wdXNoKFxuICAgICAgICBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgX3RoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgICAuZ2V0KFxuICAgICAgICAgICAgICBpc1BGID8gYC9wZi9tYXN0ZXJ0YWJsZS8ke2l0ZW19YCA6IGAvbWFzdGVydGFibGVgLFxuICAgICAgICAgICAgICBpc1BGID8ge30gOiB7XG4gICAgICAgICAgICAgICAgaXRudENvZGU6IF90aGlzLmdldFZhbHVlRnJvbU1vZGVsKChcIml0bnRDb2RlXCIpKSxcbiAgICAgICAgICAgICAgICBwcm9kdWN0Q29kZTogX3RoaXMuTVJDID8gX3RoaXMuTVJDIDogX3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoKFwicHJvZHVjdENvZGVcIikpLFxuICAgICAgICAgICAgICAgIHByb2R1Y3RWZXJzaW9uOiBfdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCgoXCJwcm9kdWN0VmVyc2lvblwiKSksXG4gICAgICAgICAgICAgICAgdGFibGVOYW1lOiBpdGVtLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7fSxcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICAgICAgICAgIGNvZGVUYWJsZXNbaXRlbV0gPSBpc1BGID8gKHJlc3BEYXRhIHx8IFtdKSA6IChyZXNwRGF0YS5pdGVtcyB8fCBbXSk7XG4gICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgICB9KTtcbiAgICBQcm9taXNlLmFsbChhcnIpLnRoZW4oXG4gICAgICB2YWx1ZXMgPT4ge1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7IGNvZGVUYWJsZXM6IGNvZGVUYWJsZXMgfSk7XG4gICAgICB9LFxuICAgICAgcmVhc29uID0+IHtcbiAgICAgICAgY29uc29sZS5sb2cocmVhc29uKTtcbiAgICAgIH0sXG4gICAgKTtcbiAgfTtcblxuICBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0ID0gKHRhYmxlTmFtZTogc3RyaW5nLCBpdGVtSWQ6IHN0cmluZywgX3RoaXM6IGFueSkgPT4ge1xuICAgIGNvbnN0IHsgY29kZVRhYmxlcyB9ID0gX3RoaXMuc3RhdGU7XG4gICAgaWYgKGl0ZW1JZCkge1xuICAgICAgY29uc3QgaXRlbSA9XG4gICAgICAgIChjb2RlVGFibGVzW3RhYmxlTmFtZV0gfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1JZC50b1N0cmluZygpID09PSBvcHRpb24uaWQudG9TdHJpbmcoKTtcbiAgICAgICAgfSkgfHwge307XG4gICAgICByZXR1cm4gaXRlbS50ZXh0IHx8IFwiXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgfTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgbmV3IENvbW1vbkdwY0NsYWltKCk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQU1BOzs7QUFFQTs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFsQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7Ozs7O0FBb0NBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common/index.tsx
