

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withConfigConsumer = withConfigConsumer;
exports.ConfigConsumer = exports.ConfigContext = void 0;

var React = _interopRequireWildcard(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var _createReactContext = _interopRequireDefault(__webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js"));

var _renderEmpty = _interopRequireDefault(__webpack_require__(/*! ./renderEmpty */ "./node_modules/antd/lib/config-provider/renderEmpty.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var ConfigContext = (0, _createReactContext["default"])({
  // We provide a default function for Context without provider
  getPrefixCls: function getPrefixCls(suffixCls, customizePrefixCls) {
    if (customizePrefixCls) return customizePrefixCls;
    return "ant-".concat(suffixCls);
  },
  renderEmpty: _renderEmpty["default"]
});
exports.ConfigContext = ConfigContext;
var ConfigConsumer = ConfigContext.Consumer;
exports.ConfigConsumer = ConfigConsumer;

function withConfigConsumer(config) {
  return function withConfigConsumerFunc(Component) {
    // Wrap with ConfigConsumer. Since we need compatible with react 15, be care when using ref methods
    var SFC = function SFC(props) {
      return React.createElement(ConfigConsumer, null, function (configProps) {
        var basicPrefixCls = config.prefixCls;
        var getPrefixCls = configProps.getPrefixCls;
        var customizePrefixCls = props.prefixCls;
        var prefixCls = getPrefixCls(basicPrefixCls, customizePrefixCls);
        return React.createElement(Component, _extends({}, configProps, props, {
          prefixCls: prefixCls
        }));
      });
    };

    var cons = Component.constructor;
    var name = cons && cons.displayName || Component.name || 'Component';
    SFC.displayName = "withConfigConsumer(".concat(name, ")");
    return SFC;
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvY29uZmlnLXByb3ZpZGVyL2NvbnRleHQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2NvbmZpZy1wcm92aWRlci9jb250ZXh0LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY3JlYXRlUmVhY3RDb250ZXh0IGZyb20gJ0BhbnQtZGVzaWduL2NyZWF0ZS1yZWFjdC1jb250ZXh0JztcbmltcG9ydCBkZWZhdWx0UmVuZGVyRW1wdHkgZnJvbSAnLi9yZW5kZXJFbXB0eSc7XG5leHBvcnQgY29uc3QgQ29uZmlnQ29udGV4dCA9IGNyZWF0ZVJlYWN0Q29udGV4dCh7XG4gICAgLy8gV2UgcHJvdmlkZSBhIGRlZmF1bHQgZnVuY3Rpb24gZm9yIENvbnRleHQgd2l0aG91dCBwcm92aWRlclxuICAgIGdldFByZWZpeENsczogKHN1ZmZpeENscywgY3VzdG9taXplUHJlZml4Q2xzKSA9PiB7XG4gICAgICAgIGlmIChjdXN0b21pemVQcmVmaXhDbHMpXG4gICAgICAgICAgICByZXR1cm4gY3VzdG9taXplUHJlZml4Q2xzO1xuICAgICAgICByZXR1cm4gYGFudC0ke3N1ZmZpeENsc31gO1xuICAgIH0sXG4gICAgcmVuZGVyRW1wdHk6IGRlZmF1bHRSZW5kZXJFbXB0eSxcbn0pO1xuZXhwb3J0IGNvbnN0IENvbmZpZ0NvbnN1bWVyID0gQ29uZmlnQ29udGV4dC5Db25zdW1lcjtcbmV4cG9ydCBmdW5jdGlvbiB3aXRoQ29uZmlnQ29uc3VtZXIoY29uZmlnKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIHdpdGhDb25maWdDb25zdW1lckZ1bmMoQ29tcG9uZW50KSB7XG4gICAgICAgIC8vIFdyYXAgd2l0aCBDb25maWdDb25zdW1lci4gU2luY2Ugd2UgbmVlZCBjb21wYXRpYmxlIHdpdGggcmVhY3QgMTUsIGJlIGNhcmUgd2hlbiB1c2luZyByZWYgbWV0aG9kc1xuICAgICAgICBjb25zdCBTRkMgPSAoKHByb3BzKSA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgICAgICB7KGNvbmZpZ1Byb3BzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogYmFzaWNQcmVmaXhDbHMgfSA9IGNvbmZpZztcbiAgICAgICAgICAgIGNvbnN0IHsgZ2V0UHJlZml4Q2xzIH0gPSBjb25maWdQcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMgfSA9IHByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKGJhc2ljUHJlZml4Q2xzLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgcmV0dXJuIDxDb21wb25lbnQgey4uLmNvbmZpZ1Byb3BzfSB7Li4ucHJvcHN9IHByZWZpeENscz17cHJlZml4Q2xzfS8+O1xuICAgICAgICB9fVxuICAgICAgPC9Db25maWdDb25zdW1lcj4pKTtcbiAgICAgICAgY29uc3QgY29ucyA9IENvbXBvbmVudC5jb25zdHJ1Y3RvcjtcbiAgICAgICAgY29uc3QgbmFtZSA9IChjb25zICYmIGNvbnMuZGlzcGxheU5hbWUpIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xuICAgICAgICBTRkMuZGlzcGxheU5hbWUgPSBgd2l0aENvbmZpZ0NvbnN1bWVyKCR7bmFtZX0pYDtcbiAgICAgICAgcmV0dXJuIFNGQztcbiAgICB9O1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFMQTtBQU9BO0FBUEE7O0FBU0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQU5BO0FBQUE7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFnQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/lib/config-provider/context.js
