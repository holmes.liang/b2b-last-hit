/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var createHashMap = _util.createHashMap;
var assert = _util.assert;

var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var OTHER_DIMENSIONS = createHashMap(['tooltip', 'label', 'itemName', 'itemId', 'seriesName']);

function summarizeDimensions(data) {
  var summary = {};
  var encode = summary.encode = {};
  var notExtraCoordDimMap = createHashMap();
  var defaultedLabel = [];
  var defaultedTooltip = [];
  each(data.dimensions, function (dimName) {
    var dimItem = data.getDimensionInfo(dimName);
    var coordDim = dimItem.coordDim;

    if (coordDim) {
      var coordDimArr = encode[coordDim];

      if (!encode.hasOwnProperty(coordDim)) {
        coordDimArr = encode[coordDim] = [];
      }

      coordDimArr[dimItem.coordDimIndex] = dimName;

      if (!dimItem.isExtraCoord) {
        notExtraCoordDimMap.set(coordDim, 1); // Use the last coord dim (and label friendly) as default label,
        // because when dataset is used, it is hard to guess which dimension
        // can be value dimension. If both show x, y on label is not look good,
        // and conventionally y axis is focused more.

        if (mayLabelDimType(dimItem.type)) {
          defaultedLabel[0] = dimName;
        }
      }

      if (dimItem.defaultTooltip) {
        defaultedTooltip.push(dimName);
      }
    }

    OTHER_DIMENSIONS.each(function (v, otherDim) {
      var otherDimArr = encode[otherDim];

      if (!encode.hasOwnProperty(otherDim)) {
        otherDimArr = encode[otherDim] = [];
      }

      var dimIndex = dimItem.otherDims[otherDim];

      if (dimIndex != null && dimIndex !== false) {
        otherDimArr[dimIndex] = dimItem.name;
      }
    });
  });
  var dataDimsOnCoord = [];
  var encodeFirstDimNotExtra = {};
  notExtraCoordDimMap.each(function (v, coordDim) {
    var dimArr = encode[coordDim]; // ??? FIXME extra coord should not be set in dataDimsOnCoord.
    // But should fix the case that radar axes: simplify the logic
    // of `completeDimension`, remove `extraPrefix`.

    encodeFirstDimNotExtra[coordDim] = dimArr[0]; // Not necessary to remove duplicate, because a data
    // dim canot on more than one coordDim.

    dataDimsOnCoord = dataDimsOnCoord.concat(dimArr);
  });
  summary.dataDimsOnCoord = dataDimsOnCoord;
  summary.encodeFirstDimNotExtra = encodeFirstDimNotExtra;
  var encodeLabel = encode.label; // FIXME `encode.label` is not recommanded, because formatter can not be set
  // in this way. Use label.formatter instead. May be remove this approach someday.

  if (encodeLabel && encodeLabel.length) {
    defaultedLabel = encodeLabel.slice();
  }

  var encodeTooltip = encode.tooltip;

  if (encodeTooltip && encodeTooltip.length) {
    defaultedTooltip = encodeTooltip.slice();
  } else if (!defaultedTooltip.length) {
    defaultedTooltip = defaultedLabel.slice();
  }

  encode.defaultedLabel = defaultedLabel;
  encode.defaultedTooltip = defaultedTooltip;
  return summary;
}

function getDimensionTypeByAxis(axisType) {
  return axisType === 'category' ? 'ordinal' : axisType === 'time' ? 'time' : 'float';
}

function mayLabelDimType(dimType) {
  // In most cases, ordinal and time do not suitable for label.
  // Ordinal info can be displayed on axis. Time is too long.
  return !(dimType === 'ordinal' || dimType === 'time');
} // function findTheLastDimMayLabel(data) {
//     // Get last value dim
//     var dimensions = data.dimensions.slice();
//     var valueType;
//     var valueDim;
//     while (dimensions.length && (
//         valueDim = dimensions.pop(),
//         valueType = data.getDimensionInfo(valueDim).type,
//         valueType === 'ordinal' || valueType === 'time'
//     )) {} // jshint ignore:line
//     return valueDim;
// }


exports.OTHER_DIMENSIONS = OTHER_DIMENSIONS;
exports.summarizeDimensions = summarizeDimensions;
exports.getDimensionTypeByAxis = getDimensionTypeByAxis;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGltZW5zaW9uSGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvZGltZW5zaW9uSGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgY3JlYXRlSGFzaE1hcCA9IF91dGlsLmNyZWF0ZUhhc2hNYXA7XG52YXIgYXNzZXJ0ID0gX3V0aWwuYXNzZXJ0O1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgT1RIRVJfRElNRU5TSU9OUyA9IGNyZWF0ZUhhc2hNYXAoWyd0b29sdGlwJywgJ2xhYmVsJywgJ2l0ZW1OYW1lJywgJ2l0ZW1JZCcsICdzZXJpZXNOYW1lJ10pO1xuXG5mdW5jdGlvbiBzdW1tYXJpemVEaW1lbnNpb25zKGRhdGEpIHtcbiAgdmFyIHN1bW1hcnkgPSB7fTtcbiAgdmFyIGVuY29kZSA9IHN1bW1hcnkuZW5jb2RlID0ge307XG4gIHZhciBub3RFeHRyYUNvb3JkRGltTWFwID0gY3JlYXRlSGFzaE1hcCgpO1xuICB2YXIgZGVmYXVsdGVkTGFiZWwgPSBbXTtcbiAgdmFyIGRlZmF1bHRlZFRvb2x0aXAgPSBbXTtcbiAgZWFjaChkYXRhLmRpbWVuc2lvbnMsIGZ1bmN0aW9uIChkaW1OYW1lKSB7XG4gICAgdmFyIGRpbUl0ZW0gPSBkYXRhLmdldERpbWVuc2lvbkluZm8oZGltTmFtZSk7XG4gICAgdmFyIGNvb3JkRGltID0gZGltSXRlbS5jb29yZERpbTtcblxuICAgIGlmIChjb29yZERpbSkge1xuICAgICAgdmFyIGNvb3JkRGltQXJyID0gZW5jb2RlW2Nvb3JkRGltXTtcblxuICAgICAgaWYgKCFlbmNvZGUuaGFzT3duUHJvcGVydHkoY29vcmREaW0pKSB7XG4gICAgICAgIGNvb3JkRGltQXJyID0gZW5jb2RlW2Nvb3JkRGltXSA9IFtdO1xuICAgICAgfVxuXG4gICAgICBjb29yZERpbUFycltkaW1JdGVtLmNvb3JkRGltSW5kZXhdID0gZGltTmFtZTtcblxuICAgICAgaWYgKCFkaW1JdGVtLmlzRXh0cmFDb29yZCkge1xuICAgICAgICBub3RFeHRyYUNvb3JkRGltTWFwLnNldChjb29yZERpbSwgMSk7IC8vIFVzZSB0aGUgbGFzdCBjb29yZCBkaW0gKGFuZCBsYWJlbCBmcmllbmRseSkgYXMgZGVmYXVsdCBsYWJlbCxcbiAgICAgICAgLy8gYmVjYXVzZSB3aGVuIGRhdGFzZXQgaXMgdXNlZCwgaXQgaXMgaGFyZCB0byBndWVzcyB3aGljaCBkaW1lbnNpb25cbiAgICAgICAgLy8gY2FuIGJlIHZhbHVlIGRpbWVuc2lvbi4gSWYgYm90aCBzaG93IHgsIHkgb24gbGFiZWwgaXMgbm90IGxvb2sgZ29vZCxcbiAgICAgICAgLy8gYW5kIGNvbnZlbnRpb25hbGx5IHkgYXhpcyBpcyBmb2N1c2VkIG1vcmUuXG5cbiAgICAgICAgaWYgKG1heUxhYmVsRGltVHlwZShkaW1JdGVtLnR5cGUpKSB7XG4gICAgICAgICAgZGVmYXVsdGVkTGFiZWxbMF0gPSBkaW1OYW1lO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChkaW1JdGVtLmRlZmF1bHRUb29sdGlwKSB7XG4gICAgICAgIGRlZmF1bHRlZFRvb2x0aXAucHVzaChkaW1OYW1lKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBPVEhFUl9ESU1FTlNJT05TLmVhY2goZnVuY3Rpb24gKHYsIG90aGVyRGltKSB7XG4gICAgICB2YXIgb3RoZXJEaW1BcnIgPSBlbmNvZGVbb3RoZXJEaW1dO1xuXG4gICAgICBpZiAoIWVuY29kZS5oYXNPd25Qcm9wZXJ0eShvdGhlckRpbSkpIHtcbiAgICAgICAgb3RoZXJEaW1BcnIgPSBlbmNvZGVbb3RoZXJEaW1dID0gW107XG4gICAgICB9XG5cbiAgICAgIHZhciBkaW1JbmRleCA9IGRpbUl0ZW0ub3RoZXJEaW1zW290aGVyRGltXTtcblxuICAgICAgaWYgKGRpbUluZGV4ICE9IG51bGwgJiYgZGltSW5kZXggIT09IGZhbHNlKSB7XG4gICAgICAgIG90aGVyRGltQXJyW2RpbUluZGV4XSA9IGRpbUl0ZW0ubmFtZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG4gIHZhciBkYXRhRGltc09uQ29vcmQgPSBbXTtcbiAgdmFyIGVuY29kZUZpcnN0RGltTm90RXh0cmEgPSB7fTtcbiAgbm90RXh0cmFDb29yZERpbU1hcC5lYWNoKGZ1bmN0aW9uICh2LCBjb29yZERpbSkge1xuICAgIHZhciBkaW1BcnIgPSBlbmNvZGVbY29vcmREaW1dOyAvLyA/Pz8gRklYTUUgZXh0cmEgY29vcmQgc2hvdWxkIG5vdCBiZSBzZXQgaW4gZGF0YURpbXNPbkNvb3JkLlxuICAgIC8vIEJ1dCBzaG91bGQgZml4IHRoZSBjYXNlIHRoYXQgcmFkYXIgYXhlczogc2ltcGxpZnkgdGhlIGxvZ2ljXG4gICAgLy8gb2YgYGNvbXBsZXRlRGltZW5zaW9uYCwgcmVtb3ZlIGBleHRyYVByZWZpeGAuXG5cbiAgICBlbmNvZGVGaXJzdERpbU5vdEV4dHJhW2Nvb3JkRGltXSA9IGRpbUFyclswXTsgLy8gTm90IG5lY2Vzc2FyeSB0byByZW1vdmUgZHVwbGljYXRlLCBiZWNhdXNlIGEgZGF0YVxuICAgIC8vIGRpbSBjYW5vdCBvbiBtb3JlIHRoYW4gb25lIGNvb3JkRGltLlxuXG4gICAgZGF0YURpbXNPbkNvb3JkID0gZGF0YURpbXNPbkNvb3JkLmNvbmNhdChkaW1BcnIpO1xuICB9KTtcbiAgc3VtbWFyeS5kYXRhRGltc09uQ29vcmQgPSBkYXRhRGltc09uQ29vcmQ7XG4gIHN1bW1hcnkuZW5jb2RlRmlyc3REaW1Ob3RFeHRyYSA9IGVuY29kZUZpcnN0RGltTm90RXh0cmE7XG4gIHZhciBlbmNvZGVMYWJlbCA9IGVuY29kZS5sYWJlbDsgLy8gRklYTUUgYGVuY29kZS5sYWJlbGAgaXMgbm90IHJlY29tbWFuZGVkLCBiZWNhdXNlIGZvcm1hdHRlciBjYW4gbm90IGJlIHNldFxuICAvLyBpbiB0aGlzIHdheS4gVXNlIGxhYmVsLmZvcm1hdHRlciBpbnN0ZWFkLiBNYXkgYmUgcmVtb3ZlIHRoaXMgYXBwcm9hY2ggc29tZWRheS5cblxuICBpZiAoZW5jb2RlTGFiZWwgJiYgZW5jb2RlTGFiZWwubGVuZ3RoKSB7XG4gICAgZGVmYXVsdGVkTGFiZWwgPSBlbmNvZGVMYWJlbC5zbGljZSgpO1xuICB9XG5cbiAgdmFyIGVuY29kZVRvb2x0aXAgPSBlbmNvZGUudG9vbHRpcDtcblxuICBpZiAoZW5jb2RlVG9vbHRpcCAmJiBlbmNvZGVUb29sdGlwLmxlbmd0aCkge1xuICAgIGRlZmF1bHRlZFRvb2x0aXAgPSBlbmNvZGVUb29sdGlwLnNsaWNlKCk7XG4gIH0gZWxzZSBpZiAoIWRlZmF1bHRlZFRvb2x0aXAubGVuZ3RoKSB7XG4gICAgZGVmYXVsdGVkVG9vbHRpcCA9IGRlZmF1bHRlZExhYmVsLnNsaWNlKCk7XG4gIH1cblxuICBlbmNvZGUuZGVmYXVsdGVkTGFiZWwgPSBkZWZhdWx0ZWRMYWJlbDtcbiAgZW5jb2RlLmRlZmF1bHRlZFRvb2x0aXAgPSBkZWZhdWx0ZWRUb29sdGlwO1xuICByZXR1cm4gc3VtbWFyeTtcbn1cblxuZnVuY3Rpb24gZ2V0RGltZW5zaW9uVHlwZUJ5QXhpcyhheGlzVHlwZSkge1xuICByZXR1cm4gYXhpc1R5cGUgPT09ICdjYXRlZ29yeScgPyAnb3JkaW5hbCcgOiBheGlzVHlwZSA9PT0gJ3RpbWUnID8gJ3RpbWUnIDogJ2Zsb2F0Jztcbn1cblxuZnVuY3Rpb24gbWF5TGFiZWxEaW1UeXBlKGRpbVR5cGUpIHtcbiAgLy8gSW4gbW9zdCBjYXNlcywgb3JkaW5hbCBhbmQgdGltZSBkbyBub3Qgc3VpdGFibGUgZm9yIGxhYmVsLlxuICAvLyBPcmRpbmFsIGluZm8gY2FuIGJlIGRpc3BsYXllZCBvbiBheGlzLiBUaW1lIGlzIHRvbyBsb25nLlxuICByZXR1cm4gIShkaW1UeXBlID09PSAnb3JkaW5hbCcgfHwgZGltVHlwZSA9PT0gJ3RpbWUnKTtcbn0gLy8gZnVuY3Rpb24gZmluZFRoZUxhc3REaW1NYXlMYWJlbChkYXRhKSB7XG4vLyAgICAgLy8gR2V0IGxhc3QgdmFsdWUgZGltXG4vLyAgICAgdmFyIGRpbWVuc2lvbnMgPSBkYXRhLmRpbWVuc2lvbnMuc2xpY2UoKTtcbi8vICAgICB2YXIgdmFsdWVUeXBlO1xuLy8gICAgIHZhciB2YWx1ZURpbTtcbi8vICAgICB3aGlsZSAoZGltZW5zaW9ucy5sZW5ndGggJiYgKFxuLy8gICAgICAgICB2YWx1ZURpbSA9IGRpbWVuc2lvbnMucG9wKCksXG4vLyAgICAgICAgIHZhbHVlVHlwZSA9IGRhdGEuZ2V0RGltZW5zaW9uSW5mbyh2YWx1ZURpbSkudHlwZSxcbi8vICAgICAgICAgdmFsdWVUeXBlID09PSAnb3JkaW5hbCcgfHwgdmFsdWVUeXBlID09PSAndGltZSdcbi8vICAgICApKSB7fSAvLyBqc2hpbnQgaWdub3JlOmxpbmVcbi8vICAgICByZXR1cm4gdmFsdWVEaW07XG4vLyB9XG5cblxuZXhwb3J0cy5PVEhFUl9ESU1FTlNJT05TID0gT1RIRVJfRElNRU5TSU9OUztcbmV4cG9ydHMuc3VtbWFyaXplRGltZW5zaW9ucyA9IHN1bW1hcml6ZURpbWVuc2lvbnM7XG5leHBvcnRzLmdldERpbWVuc2lvblR5cGVCeUF4aXMgPSBnZXREaW1lbnNpb25UeXBlQnlBeGlzOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/helper/dimensionHelper.js
