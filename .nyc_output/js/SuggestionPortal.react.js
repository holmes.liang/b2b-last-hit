__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils_getOffset__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/getOffset */ "./node_modules/rc-editor-mention/es/utils/getOffset.js");







var SuggestionPortal = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(SuggestionPortal, _React$Component);

  function SuggestionPortal() {
    var _temp, _this, _ret;

    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, SuggestionPortal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.matchDecorates = function (props) {
      var callbacks = props.callbacks,
          suggestionRegex = props.suggestionRegex,
          decoratedText = props.decoratedText;
      var matches = suggestionRegex.exec(decoratedText);
      _this.trigger = matches[2];

      _this.updatePortalPosition(_this.props);

      callbacks.setEditorState(callbacks.getEditorState());
    }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(_this, _ret);
  }

  SuggestionPortal.prototype.componentWillMount = function componentWillMount() {
    this.matchDecorates(this.props);
  };

  SuggestionPortal.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (nextProps.decoratedText !== this.props.decoratedText) {
      this.matchDecorates(nextProps);
    }

    this.updatePortalPosition(nextProps);
  };

  SuggestionPortal.prototype.componentWillUnmount = function componentWillUnmount() {
    var _props = this.props,
        offsetKey = _props.offsetKey,
        mentionStore = _props.mentionStore;
    mentionStore.inActiveSuggestion({
      offsetKey: offsetKey
    });
  };

  SuggestionPortal.prototype.updatePortalPosition = function updatePortalPosition(props) {
    var _this2 = this;

    var offsetKey = props.offsetKey,
        mentionStore = props.mentionStore;
    mentionStore.updateSuggestion({
      offsetKey: offsetKey,
      trigger: this.trigger,
      position: function position() {
        var element = _this2.searchPortal;
        var rect = Object(_utils_getOffset__WEBPACK_IMPORTED_MODULE_5__["default"])(element);
        return {
          left: rect.left,
          top: rect.top,
          width: element.offsetWidth,
          height: element.offsetHeight
        };
      }
    });
  };

  SuggestionPortal.prototype.render = function render() {
    var _this3 = this;

    return react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement('span', {
      ref: function ref(node) {
        _this3.searchPortal = node;
      },
      style: this.props.style
    }, this.props.children);
  };

  return SuggestionPortal;
}(react__WEBPACK_IMPORTED_MODULE_3___default.a.Component);

SuggestionPortal.propTypes = {
  offsetKey: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any,
  mentionStore: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.object,
  decoratedText: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.string,
  children: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any,
  callbacks: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any,
  suggestionRegex: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any
};
/* harmony default export */ __webpack_exports__["default"] = (SuggestionPortal);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L1N1Z2dlc3Rpb25Qb3J0YWwucmVhY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItbWVudGlvbi9lcy9jb21wb25lbnQvU3VnZ2VzdGlvblBvcnRhbC5yZWFjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgZ2V0T2Zmc2V0IGZyb20gJy4uL3V0aWxzL2dldE9mZnNldCc7XG5cbnZhciBTdWdnZXN0aW9uUG9ydGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFN1Z2dlc3Rpb25Qb3J0YWwsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFN1Z2dlc3Rpb25Qb3J0YWwoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTdWdnZXN0aW9uUG9ydGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5tYXRjaERlY29yYXRlcyA9IGZ1bmN0aW9uIChwcm9wcykge1xuICAgICAgdmFyIGNhbGxiYWNrcyA9IHByb3BzLmNhbGxiYWNrcyxcbiAgICAgICAgICBzdWdnZXN0aW9uUmVnZXggPSBwcm9wcy5zdWdnZXN0aW9uUmVnZXgsXG4gICAgICAgICAgZGVjb3JhdGVkVGV4dCA9IHByb3BzLmRlY29yYXRlZFRleHQ7XG5cbiAgICAgIHZhciBtYXRjaGVzID0gc3VnZ2VzdGlvblJlZ2V4LmV4ZWMoZGVjb3JhdGVkVGV4dCk7XG4gICAgICBfdGhpcy50cmlnZ2VyID0gbWF0Y2hlc1syXTtcbiAgICAgIF90aGlzLnVwZGF0ZVBvcnRhbFBvc2l0aW9uKF90aGlzLnByb3BzKTtcbiAgICAgIGNhbGxiYWNrcy5zZXRFZGl0b3JTdGF0ZShjYWxsYmFja3MuZ2V0RWRpdG9yU3RhdGUoKSk7XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBTdWdnZXN0aW9uUG9ydGFsLnByb3RvdHlwZS5jb21wb25lbnRXaWxsTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgdGhpcy5tYXRjaERlY29yYXRlcyh0aGlzLnByb3BzKTtcbiAgfTtcblxuICBTdWdnZXN0aW9uUG9ydGFsLnByb3RvdHlwZS5jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICBpZiAobmV4dFByb3BzLmRlY29yYXRlZFRleHQgIT09IHRoaXMucHJvcHMuZGVjb3JhdGVkVGV4dCkge1xuICAgICAgdGhpcy5tYXRjaERlY29yYXRlcyhuZXh0UHJvcHMpO1xuICAgIH1cbiAgICB0aGlzLnVwZGF0ZVBvcnRhbFBvc2l0aW9uKG5leHRQcm9wcyk7XG4gIH07XG5cbiAgU3VnZ2VzdGlvblBvcnRhbC5wcm90b3R5cGUuY29tcG9uZW50V2lsbFVubW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgb2Zmc2V0S2V5ID0gX3Byb3BzLm9mZnNldEtleSxcbiAgICAgICAgbWVudGlvblN0b3JlID0gX3Byb3BzLm1lbnRpb25TdG9yZTtcblxuICAgIG1lbnRpb25TdG9yZS5pbkFjdGl2ZVN1Z2dlc3Rpb24oeyBvZmZzZXRLZXk6IG9mZnNldEtleSB9KTtcbiAgfTtcblxuICBTdWdnZXN0aW9uUG9ydGFsLnByb3RvdHlwZS51cGRhdGVQb3J0YWxQb3NpdGlvbiA9IGZ1bmN0aW9uIHVwZGF0ZVBvcnRhbFBvc2l0aW9uKHByb3BzKSB7XG4gICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICB2YXIgb2Zmc2V0S2V5ID0gcHJvcHMub2Zmc2V0S2V5LFxuICAgICAgICBtZW50aW9uU3RvcmUgPSBwcm9wcy5tZW50aW9uU3RvcmU7XG5cbiAgICBtZW50aW9uU3RvcmUudXBkYXRlU3VnZ2VzdGlvbih7XG4gICAgICBvZmZzZXRLZXk6IG9mZnNldEtleSxcbiAgICAgIHRyaWdnZXI6IHRoaXMudHJpZ2dlcixcbiAgICAgIHBvc2l0aW9uOiBmdW5jdGlvbiBwb3NpdGlvbigpIHtcbiAgICAgICAgdmFyIGVsZW1lbnQgPSBfdGhpczIuc2VhcmNoUG9ydGFsO1xuICAgICAgICB2YXIgcmVjdCA9IGdldE9mZnNldChlbGVtZW50KTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBsZWZ0OiByZWN0LmxlZnQsXG4gICAgICAgICAgdG9wOiByZWN0LnRvcCxcbiAgICAgICAgICB3aWR0aDogZWxlbWVudC5vZmZzZXRXaWR0aCxcbiAgICAgICAgICBoZWlnaHQ6IGVsZW1lbnQub2Zmc2V0SGVpZ2h0XG4gICAgICAgIH07XG4gICAgICB9XG4gICAgfSk7XG4gIH07XG5cbiAgU3VnZ2VzdGlvblBvcnRhbC5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAnc3BhbicsXG4gICAgICB7IHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICBfdGhpczMuc2VhcmNoUG9ydGFsID0gbm9kZTtcbiAgICAgICAgfSwgc3R5bGU6IHRoaXMucHJvcHMuc3R5bGUgfSxcbiAgICAgIHRoaXMucHJvcHMuY2hpbGRyZW5cbiAgICApO1xuICB9O1xuXG4gIHJldHVybiBTdWdnZXN0aW9uUG9ydGFsO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5TdWdnZXN0aW9uUG9ydGFsLnByb3BUeXBlcyA9IHtcbiAgb2Zmc2V0S2V5OiBQcm9wVHlwZXMuYW55LFxuICBtZW50aW9uU3RvcmU6IFByb3BUeXBlcy5vYmplY3QsXG4gIGRlY29yYXRlZFRleHQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMuYW55LFxuICBjYWxsYmFja3M6IFByb3BUeXBlcy5hbnksXG4gIHN1Z2dlc3Rpb25SZWdleDogUHJvcFR5cGVzLmFueVxufTtcbmV4cG9ydCBkZWZhdWx0IFN1Z2dlc3Rpb25Qb3J0YWw7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBWkE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/component/SuggestionPortal.react.js
