

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setTwoToneColor = setTwoToneColor;
exports.getTwoToneColor = getTwoToneColor;

var _iconsReact = _interopRequireDefault(__webpack_require__(/*! @ant-design/icons-react */ "./node_modules/@ant-design/icons-react/es/index.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function setTwoToneColor(primaryColor) {
  return _iconsReact["default"].setTwoToneColors({
    primaryColor: primaryColor
  });
}

function getTwoToneColor() {
  var colors = _iconsReact["default"].getTwoToneColors();

  return colors.primaryColor;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvaWNvbi90d29Ub25lUHJpbWFyeUNvbG9yLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9pY29uL3R3b1RvbmVQcmltYXJ5Q29sb3IuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0SWNvbiBmcm9tICdAYW50LWRlc2lnbi9pY29ucy1yZWFjdCc7XG5leHBvcnQgZnVuY3Rpb24gc2V0VHdvVG9uZUNvbG9yKHByaW1hcnlDb2xvcikge1xuICAgIHJldHVybiBSZWFjdEljb24uc2V0VHdvVG9uZUNvbG9ycyh7XG4gICAgICAgIHByaW1hcnlDb2xvcixcbiAgICB9KTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBnZXRUd29Ub25lQ29sb3IoKSB7XG4gICAgY29uc3QgY29sb3JzID0gUmVhY3RJY29uLmdldFR3b1RvbmVDb2xvcnMoKTtcbiAgICByZXR1cm4gY29sb3JzLnByaW1hcnlDb2xvcjtcbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/lib/icon/twoTonePrimaryColor.js
