__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _component_card_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../component/card-list */ "./src/app/desk/component/card-list/index.tsx");
/* harmony import */ var _component_filter__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../component/filter */ "./src/app/desk/component/filter/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/admin/common/QueryPage.tsx";






var isMobile = _common__WEBPACK_IMPORTED_MODULE_13__["Utils"].getIsMobile();

var QueryPage =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(QueryPage, _Widget);

  function QueryPage() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, QueryPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(QueryPage)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.filterContainer = {};

    _this.updateProgressItem = function (newItems) {
      _this.setState({
        items: newItems
      });
    };

    _this.handleQueryChange = function (values, field) {
      if (values && (values.key || values.key === "")) {
        _this.setState({
          searchKey: values.key
        });
      }

      var pagination = _this.state.pagination; // 每次搜索则pageIndex为1

      _this.setState({
        pagination: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, pagination, {
          pageIndex: 1
        }),
        changedField: field
      }, function () {
        _this.refreshCardList(values);
      });
    };

    _this.handlePageChange = function (current, pageSize) {
      _this.refreshCardList(null, {
        pageIndex: current,
        pageSize: pageSize
      });
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(QueryPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.refreshCardList();
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "initState",
    value: function initState() {
      var initialPagination = this.props.initialPagination;

      if (isMobile) {
        return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(QueryPage.prototype), "initState", this).call(this), {
          pagination: {
            pageIndex: 1,
            pageSize: 5000
          },
          isAjaxFinished: false,
          searchKey: this.props.searchKey || ""
        });
      }

      return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(QueryPage.prototype), "initState", this).call(this), {
        pagination: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({
          pageIndex: 1
        }, initialPagination),
        isAjaxFinished: false,
        searchKey: this.props.searchKey || ""
      });
    }
  }, {
    key: "refreshCardList",
    value: function refreshCardList(newQuery, newPagination) {
      var _this$props = this.props,
          apiUrl = _this$props.apiUrl,
          onBeforeRefresh = _this$props.onBeforeRefresh,
          isSearch = _this$props.isSearch,
          endoId = _this$props.endoId;
      var _this$state = this.state,
          _this$state$query = _this$state.query,
          query = _this$state$query === void 0 ? {} : _this$state$query,
          _this$state$paginatio = _this$state.pagination,
          pagination = _this$state$paginatio === void 0 ? {} : _this$state$paginatio,
          changedField = _this$state.changedField;
      var pageIndex = pagination.pageIndex,
          pageSize = pagination.pageSize;

      var finalQuery = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, query, {}, newQuery);

      if (onBeforeRefresh) {
        finalQuery = onBeforeRefresh(finalQuery, changedField);
      }

      if (isSearch) {
        if (endoId) {
          this.fetchData(apiUrl, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, finalQuery, {
            pageIndex: pageIndex,
            pageSize: pageSize
          }, newPagination));
        }
      } else {
        this.fetchData(apiUrl, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, finalQuery, {
          pageIndex: pageIndex,
          pageSize: pageSize
        }, newPagination));
      }
    }
  }, {
    key: "getSelectedIds",
    value: function getSelectedIds() {
      var selectedItems = this.props.selectedItems;

      var copySelectedItems = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(selectedItems);

      var selectedIds = [];
      (copySelectedItems || []).forEach(function (item) {
        if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "id")) {
          selectedIds.push(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "id"));
        }
      });
      return selectedIds;
    }
  }, {
    key: "rebuildTableData",
    value: function rebuildTableData(respData) {
      var _this2 = this;

      var itemKey = this.props.itemKey;

      var _respData$items = respData.items,
          items = _respData$items === void 0 ? [] : _respData$items,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(respData, ["items"]);

      if (this.getSelectedIds().length === 0) return respData;
      var newRespData = [];
      items.forEach(function (item) {
        if (item[itemKey] && _this2.getSelectedIds().includes(item[itemKey])) {
          newRespData.push(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, item, {}, {
            opers: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "opers", {}), {}, {
              isAddItem: false,
              isCancelItem: true
            })
          }));
        } else {
          newRespData.push(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, item, {}, {
            isAddItem: true,
            isCancelItem: false
          }));
        }
      });
      return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, {
        items: newRespData
      }, {}, rest);
    }
  }, {
    key: "fetchData",
    value: function fetchData(apiUrl, params) {
      var _this3 = this;

      this.setState({
        isAjaxFinished: false
      });
      this.getHelpers().getAjax().post(apiUrl, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({
        key: this.state.searchKey
      }, params)).then(function (response) {
        var _ref = response.body || {},
            _ref$respData = _ref.respData,
            respData = _ref$respData === void 0 ? {} : _ref$respData;

        var _this3$rebuildTableDa = _this3.rebuildTableData(respData),
            _this3$rebuildTableDa2 = _this3$rebuildTableDa.items,
            items = _this3$rebuildTableDa2 === void 0 ? [] : _this3$rebuildTableDa2,
            rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this3$rebuildTableDa, ["items"]);

        _this3.props.handelRespData && _this3.props.handelRespData(_this3.rebuildTableData(respData));

        _this3.setState({
          items: items,
          //加前端data数据
          query: params,
          pagination: rest,
          isAjaxFinished: true
        }, function () {});
      });
    }
  }, {
    key: "renderFilterContent",
    value: function renderFilterContent() {
      if (!this.props.filter) return;
      var items = this.props.filter.items;

      if (!items || !items.length) {
        return null;
      }

      return items.map(function (item) {
        if (!item) return;
        var _item$props = item.props,
            props = _item$props === void 0 ? {} : _item$props,
            type = item.type;
        var component = _component_filter__WEBPACK_IMPORTED_MODULE_12__["FilterComponentMap"][type]; // @ts-ignore

        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(component, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({
          key: props.field
        }, props, {
          values: {}
        }));
      });
    } //TODO this will be refactor again

  }, {
    key: "resetFilterValues",
    value: function resetFilterValues() {
      if (this.filterContainer) {
        this.filterContainer.resetValues({});
      }
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var type = this.props.type;
      var _this$state2 = this.state,
          pagination = _this$state2.pagination,
          _this$state2$items = _this$state2.items,
          items = _this$state2$items === void 0 ? [] : _this$state2$items,
          isAjaxFinished = _this$state2.isAjaxFinished;
      var _this$props2 = this.props,
          columns = _this$props2.columns,
          renderCardHead = _this$props2.renderCardHead,
          footer = _this$props2.footer,
          itemKey = _this$props2.itemKey,
          showHeader = _this$props2.showHeader,
          handleRow = _this$props2.handleRow,
          renderRawItem = _this$props2.renderRawItem,
          bordered = _this$props2.bordered;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_component_card_list__WEBPACK_IMPORTED_MODULE_11__["default"], {
        cardKey: itemKey,
        columns: columns,
        items: items,
        type: type,
        showHeader: showHeader,
        renderRawItem: renderRawItem,
        pagination: pagination,
        handleRow: handleRow,
        renderCardHead: renderCardHead,
        footer: footer,
        bordered: bordered,
        isAjaxFinished: isAjaxFinished,
        onPageChange: this.handlePageChange,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 262
        },
        __self: this
      }, this.props.filter && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_component_filter__WEBPACK_IMPORTED_MODULE_12__["default"].FilterContainer, {
        ref: function ref(_ref2) {
          return _this4.filterContainer = _ref2;
        },
        onChange: this.handleQueryChange,
        onChangeKey: function onChangeKey(values) {
          if (values && (values.key || values.key === "")) {
            _this4.setState({
              searchKey: values.key
            }, function () {});
          }
        },
        defaultValues: {
          key: this.props.searchKey
        },
        searchBoxVisible: this.props.searchBoxVisible,
        advancedVisible: this.props.advancedVisible,
        panelVisible: this.props.panelVisible,
        placeholder: this.props.placeholder,
        enterButton: this.props.enterButton,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 263
        },
        __self: this
      }, this.renderFilterContent()), this.renderContent());
    }
  }]);

  return QueryPage;
}(_component__WEBPACK_IMPORTED_MODULE_10__["Widget"]); // @ts-ignore


QueryPage.defaultProps = {
  initialPagination: {
    pageIndex: 1,
    pageSize: 5
  },
  onBeforeRefresh: function onBeforeRefresh(query) {
    return query;
  },
  type: "list",
  searchBoxVisible: true,
  advancedVisible: true,
  panelVisible: false,
  showHeader: true,
  bordered: false
};
/* harmony default export */ __webpack_exports__["default"] = (QueryPage);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svYWRtaW4vY29tbW9uL1F1ZXJ5UGFnZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9hZG1pbi9jb21tb24vUXVlcnlQYWdlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBQYWdlQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgQ2FyZExpc3QgZnJvbSBcIi4uLy4uL2NvbXBvbmVudC9jYXJkLWxpc3RcIjtcbmltcG9ydCBGaWx0ZXIsIHsgRmlsdGVyQ29tcG9uZW50TWFwLCBGaWx0ZXJJdGVtVHlwZXMgfSBmcm9tIFwiLi4vLi4vY29tcG9uZW50L2ZpbHRlclwiO1xuaW1wb3J0IHsgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG50eXBlIFF1ZXJ5UGFnZVN0YXRlID0ge1xuICBxdWVyeTogYW55O1xuICBpdGVtczogYW55O1xuICBwYWdpbmF0aW9uOiBhbnk7XG4gIGNoYW5nZWRGaWVsZD86IHN0cmluZztcbiAgaXNBamF4RmluaXNoZWQ6IGJvb2xlYW47XG4gIHNlYXJjaEtleTogc3RyaW5nO1xufTtcblxudHlwZSBRdWVyeVBhZ2VQcm9wcyA9IHtcbiAgYXBpVXJsOiBzdHJpbmc7XG4gIGNvbHVtbnM6IGFueVtdO1xuICBmaWx0ZXI/OiBhbnk7XG4gIGl0ZW1LZXk6IHN0cmluZztcbiAgc2VhcmNoS2V5Pzogc3RyaW5nO1xuICBmb290ZXI/OiBSZWFjdC5SZWFjdE5vZGU7XG4gIHNob3dIZWFkZXI/OiBib29sZWFuO1xuICB0eXBlPzogc3RyaW5nIHwgXCJ0YWJsZVwiIHwgXCJjYXJkXCIgfCBcInJhd1wiO1xuICByZW5kZXJSYXdJdGVtPzogRnVuY3Rpb247XG4gIGluaXRpYWxQYWdpbmF0aW9uPzogeyBwYWdlSW5kZXg/OiBudW1iZXI7IHBhZ2VTaXplOiBudW1iZXIgfTtcbiAgb25CZWZvcmVSZWZyZXNoPzogKHF1ZXJ5OiBhbnksIGZpZWxkPzogc3RyaW5nKSA9PiBhbnk7XG4gIG9uQWZ0ZXJSZWZyZXNoPzogKHF1ZXJ5OiBhbnkpID0+IGFueTtcbiAgcmVuZGVyQ2FyZEhlYWQ/OiAoaXRlbTogYW55KSA9PiBzdHJpbmcgfCBSZWFjdC5SZWFjdE5vZGU7XG4gIHNlYXJjaEJveFZpc2libGU/OiBib29sZWFuO1xuICBwYW5lbFZpc2libGU/OiBib29sZWFuO1xuICBhZHZhbmNlZFZpc2libGU/OiBib29sZWFuO1xuICBoYW5kZWxSZXNwRGF0YT86IEZ1bmN0aW9uO1xuICBoYW5kbGVSb3c/OiBGdW5jdGlvbjtcbiAgcGxhY2Vob2xkZXI/OiBzdHJpbmc7XG4gIGVuZG9JZD86IGFueTtcbiAgaXNTZWFyY2g/OiBhbnk7XG4gIHNlbGVjdGVkSXRlbXM/OiBhbnlbXTtcbiAgZW50ZXJCdXR0b24/OiBhbnk7XG4gIGJvcmRlcmVkPzogYm9vbGVhbjtcbn07XG5cbmNsYXNzIFF1ZXJ5UGFnZTxQIGV4dGVuZHMgUXVlcnlQYWdlUHJvcHMsIFMgZXh0ZW5kcyBRdWVyeVBhZ2VTdGF0ZSwgQyBleHRlbmRzIFBhZ2VDb21wb25lbnRzPiBleHRlbmRzIFdpZGdldDxQLCBTLCBDPiB7XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgaW5pdGlhbFBhZ2luYXRpb246IHsgcGFnZUluZGV4OiAxLCBwYWdlU2l6ZTogNSB9LFxuICAgIG9uQmVmb3JlUmVmcmVzaDogKHF1ZXJ5OiBhbnkpID0+IHF1ZXJ5LFxuICAgIHR5cGU6IFwibGlzdFwiLFxuICAgIHNlYXJjaEJveFZpc2libGU6IHRydWUsXG4gICAgYWR2YW5jZWRWaXNpYmxlOiB0cnVlLFxuICAgIHBhbmVsVmlzaWJsZTogZmFsc2UsXG4gICAgc2hvd0hlYWRlcjogdHJ1ZSxcbiAgICBib3JkZXJlZDogZmFsc2UsXG4gIH07XG5cbiAgcHJvdGVjdGVkIGZpbHRlckNvbnRhaW5lcjogYW55ID0ge307XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5yZWZyZXNoQ2FyZExpc3QoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgY29uc3QgeyBpbml0aWFsUGFnaW5hdGlvbiB9ID0gdGhpcy5wcm9wcztcblxuICAgIGlmIChpc01vYmlsZSkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3VwZXIuaW5pdFN0YXRlKCksXG4gICAgICAgIHBhZ2luYXRpb246IHsgcGFnZUluZGV4OiAxLCBwYWdlU2l6ZTogNTAwMCB9LFxuICAgICAgICBpc0FqYXhGaW5pc2hlZDogZmFsc2UsXG4gICAgICAgIHNlYXJjaEtleTogdGhpcy5wcm9wcy5zZWFyY2hLZXkgfHwgXCJcIixcbiAgICAgIH07XG4gICAgfVxuICAgIHJldHVybiB7XG4gICAgICAuLi5zdXBlci5pbml0U3RhdGUoKSxcbiAgICAgIHBhZ2luYXRpb246IHsgcGFnZUluZGV4OiAxLCAuLi5pbml0aWFsUGFnaW5hdGlvbiB9LFxuICAgICAgaXNBamF4RmluaXNoZWQ6IGZhbHNlLFxuICAgICAgc2VhcmNoS2V5OiB0aGlzLnByb3BzLnNlYXJjaEtleSB8fCBcIlwiLFxuICAgIH07XG4gIH1cblxuICBwdWJsaWMgcmVmcmVzaENhcmRMaXN0KG5ld1F1ZXJ5PzogYW55LCBuZXdQYWdpbmF0aW9uPzogYW55KSB7XG4gICAgY29uc3QgeyBhcGlVcmwsIG9uQmVmb3JlUmVmcmVzaCwgaXNTZWFyY2gsIGVuZG9JZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHF1ZXJ5ID0ge30sIHBhZ2luYXRpb24gPSB7fSwgY2hhbmdlZEZpZWxkIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgcGFnZUluZGV4LCBwYWdlU2l6ZSB9ID0gcGFnaW5hdGlvbjtcblxuICAgIGxldCBmaW5hbFF1ZXJ5ID0ge1xuICAgICAgLi4ucXVlcnksXG4gICAgICAuLi5uZXdRdWVyeSxcbiAgICB9O1xuICAgIGlmIChvbkJlZm9yZVJlZnJlc2gpIHtcbiAgICAgIGZpbmFsUXVlcnkgPSBvbkJlZm9yZVJlZnJlc2goZmluYWxRdWVyeSwgY2hhbmdlZEZpZWxkKTtcbiAgICB9XG4gICAgaWYgKGlzU2VhcmNoKSB7XG4gICAgICBpZiAoZW5kb0lkKSB7XG4gICAgICAgIHRoaXMuZmV0Y2hEYXRhKGFwaVVybCwge1xuICAgICAgICAgIC4uLmZpbmFsUXVlcnksXG4gICAgICAgICAgcGFnZUluZGV4LFxuICAgICAgICAgIHBhZ2VTaXplLFxuICAgICAgICAgIC4uLm5ld1BhZ2luYXRpb24sXG4gICAgICAgIH0gYXMgYW55KTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mZXRjaERhdGEoYXBpVXJsLCB7XG4gICAgICAgIC4uLmZpbmFsUXVlcnksXG4gICAgICAgIHBhZ2VJbmRleCxcbiAgICAgICAgcGFnZVNpemUsXG4gICAgICAgIC4uLm5ld1BhZ2luYXRpb24sXG4gICAgICB9IGFzIGFueSk7XG4gICAgfVxuXG4gIH1cblxuICBnZXRTZWxlY3RlZElkcygpIHtcbiAgICBjb25zdCB7IHNlbGVjdGVkSXRlbXMgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGNvcHlTZWxlY3RlZEl0ZW1zOiBhbnkgPSBfLmNsb25lRGVlcChzZWxlY3RlZEl0ZW1zKTtcbiAgICBsZXQgc2VsZWN0ZWRJZHM6IGFueVtdID0gW107XG4gICAgKGNvcHlTZWxlY3RlZEl0ZW1zIHx8IFtdKS5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIGlmIChfLmdldChpdGVtLCBcImlkXCIpKSB7XG4gICAgICAgIHNlbGVjdGVkSWRzLnB1c2goXy5nZXQoaXRlbSwgXCJpZFwiKSk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIHNlbGVjdGVkSWRzO1xuICB9XG5cbiAgcmVidWlsZFRhYmxlRGF0YShyZXNwRGF0YTogYW55KSB7XG4gICAgY29uc3QgeyBpdGVtS2V5IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaXRlbXMgPSBbXSwgLi4ucmVzdCB9ID0gcmVzcERhdGE7XG4gICAgaWYgKHRoaXMuZ2V0U2VsZWN0ZWRJZHMoKS5sZW5ndGggPT09IDApIHJldHVybiByZXNwRGF0YTtcbiAgICBsZXQgbmV3UmVzcERhdGE6IGFueVtdID0gW107XG4gICAgKGl0ZW1zKS5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIGlmIChpdGVtW2l0ZW1LZXldICYmIHRoaXMuZ2V0U2VsZWN0ZWRJZHMoKS5pbmNsdWRlcyhpdGVtW2l0ZW1LZXldKSkge1xuICAgICAgICBuZXdSZXNwRGF0YS5wdXNoKHtcbiAgICAgICAgICAuLi5pdGVtLFxuICAgICAgICAgIC4uLntcbiAgICAgICAgICAgIG9wZXJzOiB7XG4gICAgICAgICAgICAgIC4uLl8uZ2V0KGl0ZW0sIFwib3BlcnNcIiwge30pLFxuICAgICAgICAgICAgICAuLi57IGlzQWRkSXRlbTogZmFsc2UsIGlzQ2FuY2VsSXRlbTogdHJ1ZSB9LFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICB9LFxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG5ld1Jlc3BEYXRhLnB1c2goe1xuICAgICAgICAgIC4uLml0ZW0sXG4gICAgICAgICAgLi4ueyBpc0FkZEl0ZW06IHRydWUsIGlzQ2FuY2VsSXRlbTogZmFsc2UgfSxcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIHtcbiAgICAgIC4uLnsgaXRlbXM6IG5ld1Jlc3BEYXRhIH0sXG4gICAgICAuLi5yZXN0LFxuICAgIH07XG4gIH1cblxuICBwdWJsaWMgZmV0Y2hEYXRhKGFwaVVybDogc3RyaW5nLCBwYXJhbXM6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgaXNBamF4RmluaXNoZWQ6IGZhbHNlLFxuICAgIH0pO1xuICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAuZ2V0QWpheCgpXG4gICAgICAucG9zdChhcGlVcmwsIHsga2V5OiB0aGlzLnN0YXRlLnNlYXJjaEtleSwgLi4ucGFyYW1zIH0pXG4gICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhID0ge30gfSA9IHJlc3BvbnNlLmJvZHkgfHwge307XG4gICAgICAgIGNvbnN0IHsgaXRlbXMgPSBbXSwgLi4ucmVzdCB9ID0gdGhpcy5yZWJ1aWxkVGFibGVEYXRhKHJlc3BEYXRhKTtcbiAgICAgICAgdGhpcy5wcm9wcy5oYW5kZWxSZXNwRGF0YSAmJiB0aGlzLnByb3BzLmhhbmRlbFJlc3BEYXRhKHRoaXMucmVidWlsZFRhYmxlRGF0YShyZXNwRGF0YSkpO1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBpdGVtcywvL+WKoOWJjeerr2RhdGHmlbDmja5cbiAgICAgICAgICBxdWVyeTogcGFyYW1zLFxuICAgICAgICAgIHBhZ2luYXRpb246IHJlc3QsXG4gICAgICAgICAgaXNBamF4RmluaXNoZWQ6IHRydWUsXG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVByb2dyZXNzSXRlbSA9IChuZXdJdGVtczogYW55KSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBpdGVtczogbmV3SXRlbXMsXG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlUXVlcnlDaGFuZ2UgPSAodmFsdWVzOiBhbnksIGZpZWxkPzogc3RyaW5nKSA9PiB7XG4gICAgaWYgKHZhbHVlcyAmJiAodmFsdWVzLmtleSB8fCB2YWx1ZXMua2V5ID09PSBcIlwiKSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNlYXJjaEtleTogdmFsdWVzLmtleSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBjb25zdCB7IHBhZ2luYXRpb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgLy8g5q+P5qyh5pCc57Si5YiZcGFnZUluZGV45Li6MVxuICAgIHRoaXMuc2V0U3RhdGUoXG4gICAgICB7XG4gICAgICAgIHBhZ2luYXRpb246IHtcbiAgICAgICAgICAuLi5wYWdpbmF0aW9uLFxuICAgICAgICAgIHBhZ2VJbmRleDogMSxcbiAgICAgICAgfSxcbiAgICAgICAgY2hhbmdlZEZpZWxkOiBmaWVsZCxcbiAgICAgIH0sXG4gICAgICAoKSA9PiB7XG4gICAgICAgIHRoaXMucmVmcmVzaENhcmRMaXN0KHZhbHVlcyk7XG4gICAgICB9LFxuICAgICk7XG4gIH07XG5cbiAgaGFuZGxlUGFnZUNoYW5nZSA9IChjdXJyZW50OiBudW1iZXIsIHBhZ2VTaXplPzogbnVtYmVyKSA9PiB7XG4gICAgdGhpcy5yZWZyZXNoQ2FyZExpc3QobnVsbCwgeyBwYWdlSW5kZXg6IGN1cnJlbnQsIHBhZ2VTaXplIH0pO1xuICB9O1xuXG4gIHJlbmRlckZpbHRlckNvbnRlbnQoKSB7XG4gICAgaWYgKCF0aGlzLnByb3BzLmZpbHRlcikgcmV0dXJuO1xuICAgIGNvbnN0IHsgaXRlbXMgfSA9IHRoaXMucHJvcHMuZmlsdGVyO1xuICAgIGlmICghaXRlbXMgfHwgIWl0ZW1zLmxlbmd0aCkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIHJldHVybiBpdGVtcy5tYXAoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgaWYgKCFpdGVtKSByZXR1cm47XG5cbiAgICAgIGNvbnN0IHsgcHJvcHMgPSB7fSwgdHlwZSB9ID0gaXRlbTtcbiAgICAgIGNvbnN0IGNvbXBvbmVudCA9IEZpbHRlckNvbXBvbmVudE1hcFt0eXBlIGFzIEZpbHRlckl0ZW1UeXBlc107XG4gICAgICAvLyBAdHMtaWdub3JlXG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChjb21wb25lbnQsIHsga2V5OiBwcm9wcy5maWVsZCwgLi4ucHJvcHMsIHZhbHVlczoge30gfSk7XG4gICAgfSk7XG4gIH1cblxuICAvL1RPRE8gdGhpcyB3aWxsIGJlIHJlZmFjdG9yIGFnYWluXG4gIHB1YmxpYyByZXNldEZpbHRlclZhbHVlcygpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5maWx0ZXJDb250YWluZXIpIHtcbiAgICAgIHRoaXMuZmlsdGVyQ29udGFpbmVyLnJlc2V0VmFsdWVzKHt9KTtcbiAgICB9XG4gIH1cblxuICByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IHsgdHlwZSB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHBhZ2luYXRpb24sIGl0ZW1zID0gW10sIGlzQWpheEZpbmlzaGVkIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgY29sdW1ucywgcmVuZGVyQ2FyZEhlYWQsIGZvb3RlciwgaXRlbUtleSwgc2hvd0hlYWRlciwgaGFuZGxlUm93LCByZW5kZXJSYXdJdGVtLCBib3JkZXJlZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8Q2FyZExpc3RcbiAgICAgICAgY2FyZEtleT17aXRlbUtleX1cbiAgICAgICAgY29sdW1ucz17Y29sdW1uc31cbiAgICAgICAgaXRlbXM9e2l0ZW1zfVxuICAgICAgICB0eXBlPXt0eXBlfVxuICAgICAgICBzaG93SGVhZGVyPXtzaG93SGVhZGVyfVxuICAgICAgICByZW5kZXJSYXdJdGVtPXtyZW5kZXJSYXdJdGVtfVxuICAgICAgICBwYWdpbmF0aW9uPXtwYWdpbmF0aW9ufVxuICAgICAgICBoYW5kbGVSb3c9e2hhbmRsZVJvd31cbiAgICAgICAgcmVuZGVyQ2FyZEhlYWQ9e3JlbmRlckNhcmRIZWFkfVxuICAgICAgICBmb290ZXI9e2Zvb3Rlcn1cbiAgICAgICAgYm9yZGVyZWQ9e2JvcmRlcmVkfVxuICAgICAgICBpc0FqYXhGaW5pc2hlZD17aXNBamF4RmluaXNoZWR9XG4gICAgICAgIG9uUGFnZUNoYW5nZT17dGhpcy5oYW5kbGVQYWdlQ2hhbmdlfVxuICAgICAgLz5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICB7dGhpcy5wcm9wcy5maWx0ZXIgJiYgPEZpbHRlci5GaWx0ZXJDb250YWluZXJcbiAgICAgICAgICByZWY9eyhyZWY6IGFueSkgPT4gKHRoaXMuZmlsdGVyQ29udGFpbmVyID0gcmVmKX1cbiAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVRdWVyeUNoYW5nZX1cbiAgICAgICAgICBvbkNoYW5nZUtleT17KHZhbHVlczogYW55KSA9PiB7XG4gICAgICAgICAgICBpZiAodmFsdWVzICYmICh2YWx1ZXMua2V5IHx8IHZhbHVlcy5rZXkgPT09IFwiXCIpKSB7XG4gICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHNlYXJjaEtleTogdmFsdWVzLmtleSxcbiAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9fVxuICAgICAgICAgIGRlZmF1bHRWYWx1ZXM9e3tcbiAgICAgICAgICAgIGtleTogdGhpcy5wcm9wcy5zZWFyY2hLZXksXG4gICAgICAgICAgfX1cbiAgICAgICAgICBzZWFyY2hCb3hWaXNpYmxlPXt0aGlzLnByb3BzLnNlYXJjaEJveFZpc2libGV9XG4gICAgICAgICAgYWR2YW5jZWRWaXNpYmxlPXt0aGlzLnByb3BzLmFkdmFuY2VkVmlzaWJsZX1cbiAgICAgICAgICBwYW5lbFZpc2libGU9e3RoaXMucHJvcHMucGFuZWxWaXNpYmxlfVxuICAgICAgICAgIHBsYWNlaG9sZGVyPXt0aGlzLnByb3BzLnBsYWNlaG9sZGVyfVxuICAgICAgICAgIGVudGVyQnV0dG9uPXt0aGlzLnByb3BzLmVudGVyQnV0dG9ufVxuICAgICAgICA+XG4gICAgICAgICAge3RoaXMucmVuZGVyRmlsdGVyQ29udGVudCgpfVxuICAgICAgICA8L0ZpbHRlci5GaWx0ZXJDb250YWluZXI+fVxuICAgICAgICB7dGhpcy5yZW5kZXJDb250ZW50KCl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbi8vIEB0cy1pZ25vcmVcbmV4cG9ydCBkZWZhdWx0IFF1ZXJ5UGFnZTsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBb0NBOzs7Ozs7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUEwSEE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFOQTtBQUNBO0FBT0E7QUFFQTtBQUVBO0FBRkE7QUFJQTtBQUxBO0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7Ozs7O0FBeEpBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUhBO0FBTUE7QUFFQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUVBO0FBQUE7QUFBQTtBQUhBO0FBT0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUZBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTs7O0FBa0NBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF5QkE7Ozs7QUFsUEE7QUFDQTtBQUNBO0FBRkE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBcVBBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/admin/common/QueryPage.tsx
