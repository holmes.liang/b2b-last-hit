__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common */ "./src/app/desk/claims/handing/details/SAIC/gpc/view/sections/consequence/components/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/componnets/extra-info-view.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var defaultLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItem = function ViewItem(props) {
  return Object(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_12__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_8__["default"])({}, props, {
    layout: props.layoutCol || defaultLayout
  }));
};

var ExtraInfoView =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ExtraInfoView, _ModelWidget);

  function ExtraInfoView(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, ExtraInfoView);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ExtraInfoView).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(ExtraInfoView, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_13__["default"].loadCodeTables(this, ["gender", "nationality"], false);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {}
  }, {
    key: "genePropName",
    value: function genePropName(propName) {
      var propNameFixed = this.props.propNameFixed;
      var propsName = propNameFixed || "ext.policyholder";
      if (!propName) return propsName;
      return "".concat(propsName, ".").concat(propName);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          layoutCol = _this$props.layoutCol,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props, ["model", "layoutCol"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Gender").thai("เพศ").my("ကျား, မ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_13__["default"].getMasterTableItemText("gender", lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(model, this.genePropName("gender")), this)), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Date of Birth").thai("วันเกิด").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].toDateString(lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(model, this.genePropName("dob")))), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(ViewItem, {
        layoutCol: layoutCol,
        title: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Nationality").thai("Nationality").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, _desk_claims_handing_details_SAIC_gpc_view_sections_consequence_components_common__WEBPACK_IMPORTED_MODULE_13__["default"].getMasterTableItemText("nationality", lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(model, this.genePropName("nationality")), this))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ExtraInfoView.prototype), "initState", this).call(this), {
        codeTables: []
      });
    }
  }]);

  return ExtraInfoView;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ExtraInfoView);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL2NvbXBvbm5ldHMvZXh0cmEtaW5mby12aWV3LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9waC9jb21wb25uZXRzL2V4dHJhLWluZm8tdmlldy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSZWFjdCwgU3R5bGVkfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7TW9kZWxXaWRnZXR9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQge0xhbmd1YWdlLCBVdGlsc30gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7TW9kZWxXaWRnZXRQcm9wc30gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IFZpZXdJdGVtMSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3ZpZXctaXRlbVwiO1xuaW1wb3J0IENvbW1vbkdwY0NsYWltIGZyb20gXCJAZGVzay9jbGFpbXMvaGFuZGluZy9kZXRhaWxzL1NBSUMvZ3BjL3ZpZXcvc2VjdGlvbnMvY29uc2VxdWVuY2UvY29tcG9uZW50cy9jb21tb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgICBsYWJlbENvbDoge1xuICAgICAgICB4czoge3NwYW46IDh9LFxuICAgICAgICBzbToge3NwYW46IDZ9LFxuICAgIH0sXG4gICAgd3JhcHBlckNvbDoge1xuICAgICAgICB4czoge3NwYW46IDE2fSxcbiAgICAgICAgc206IHtzcGFuOiAxM30sXG4gICAgfSxcbn07XG5cbmNvbnN0IFZpZXdJdGVtID0gKHByb3BzOiBhbnkpID0+IFZpZXdJdGVtMSh7Li4ucHJvcHMsIGxheW91dDogcHJvcHMubGF5b3V0Q29sIHx8IGRlZmF1bHRMYXlvdXR9KTtcbnR5cGUgUGF5ZXJTdGF0ZSA9IHtcbiAgICBjb2RlVGFibGVzOiBhbnk7XG59O1xuXG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgUGF5ZXJQYWdlQ29tcG9uZW50cyA9IHtcbiAgICBCb3hDb250ZW50OiBTdHlsZWRESVY7XG59O1xuZXhwb3J0IHR5cGUgUGF5ZXJQcm9wcyA9IHtcbiAgICBtb2RlbDogYW55O1xuICAgIHByb3BOYW1lRml4ZWQ/OiBhbnk7XG4gICAgbGF5b3V0Q29sPzogYW55O1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmNsYXNzIEV4dHJhSW5mb1ZpZXc8UCBleHRlbmRzIFBheWVyUHJvcHMsIFMgZXh0ZW5kcyBQYXllclN0YXRlLCBDIGV4dGVuZHMgUGF5ZXJQYWdlQ29tcG9uZW50cz5cbiAgICBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcblxuICAgIGNvbnN0cnVjdG9yKHByb3BzOiBQYXllclByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgQ29tbW9uR3BjQ2xhaW0ubG9hZENvZGVUYWJsZXModGhpcywgW1wiZ2VuZGVyXCIsIFwibmF0aW9uYWxpdHlcIl0sIGZhbHNlKTtcbiAgICB9XG5cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB9XG5cbiAgICBnZW5lUHJvcE5hbWUocHJvcE5hbWU/OiBzdHJpbmcpIHtcbiAgICAgICAgY29uc3Qge3Byb3BOYW1lRml4ZWR9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgcHJvcHNOYW1lID0gcHJvcE5hbWVGaXhlZCB8fCBcImV4dC5wb2xpY3lob2xkZXJcIjtcbiAgICAgICAgaWYgKCFwcm9wTmFtZSkgcmV0dXJuIHByb3BzTmFtZTtcbiAgICAgICAgcmV0dXJuIGAke3Byb3BzTmFtZX0uJHtwcm9wTmFtZX1gO1xuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgICAgICBjb25zdCB7bW9kZWwsIGxheW91dENvbCwgLi4ucmVzdH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIkdlbmRlclwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4LmA4Lie4LioXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLm15KFwi4YCA4YC74YCs4YC4LCDhgJlcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Q29tbW9uR3BjQ2xhaW0uZ2V0TWFzdGVyVGFibGVJdGVtVGV4dChcImdlbmRlclwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uZ2V0KG1vZGVsLCB0aGlzLmdlbmVQcm9wTmFtZShcImdlbmRlclwiKSksIHRoaXMpfVxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiRGF0ZSBvZiBCaXJ0aFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC50aGFpKFwi4Lin4Lix4LiZ4LmA4LiB4Li04LiUXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAge1V0aWxzLnRvRGF0ZVN0cmluZyhfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJkb2JcIikpKX1cbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgICAgICAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIk5hdGlvbmFsaXR5XCIpLnRoYWkoXCJOYXRpb25hbGl0eVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtDb21tb25HcGNDbGFpbS5nZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwibmF0aW9uYWxpdHlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfLmdldChtb2RlbCwgdGhpcy5nZW5lUHJvcE5hbWUoXCJuYXRpb25hbGl0eVwiKSksIHRoaXMpfVxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgPC9DLkJveENvbnRlbnQ+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgQm94Q29udGVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgICAgIGAsXG4gICAgICAgIH0gYXMgQztcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgICAgICAgY29kZVRhYmxlczogW10sXG4gICAgICAgIH0pIGFzIFM7XG4gICAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBFeHRyYUluZm9WaWV3O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBZUE7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBOzs7QUFHQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7O0FBbkVBO0FBQ0E7QUFxRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/componnets/extra-info-view.tsx
