/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;

var modelHelper = __webpack_require__(/*! ./modelHelper */ "./node_modules/echarts/lib/component/axisPointer/modelHelper.js");

var findPointFromSeries = __webpack_require__(/*! ./findPointFromSeries */ "./node_modules/echarts/lib/component/axisPointer/findPointFromSeries.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var each = zrUtil.each;
var curry = zrUtil.curry;
var inner = makeInner();
/**
 * Basic logic: check all axis, if they do not demand show/highlight,
 * then hide/downplay them.
 *
 * @param {Object} coordSysAxesInfo
 * @param {Object} payload
 * @param {string} [payload.currTrigger] 'click' | 'mousemove' | 'leave'
 * @param {Array.<number>} [payload.x] x and y, which are mandatory, specify a point to
 *              trigger axisPointer and tooltip.
 * @param {Array.<number>} [payload.y] x and y, which are mandatory, specify a point to
 *              trigger axisPointer and tooltip.
 * @param {Object} [payload.seriesIndex] finder, optional, restrict target axes.
 * @param {Object} [payload.dataIndex] finder, restrict target axes.
 * @param {Object} [payload.axesInfo] finder, restrict target axes.
 *        [{
 *          axisDim: 'x'|'y'|'angle'|...,
 *          axisIndex: ...,
 *          value: ...
 *        }, ...]
 * @param {Function} [payload.dispatchAction]
 * @param {Object} [payload.tooltipOption]
 * @param {Object|Array.<number>|Function} [payload.position] Tooltip position,
 *        which can be specified in dispatchAction
 * @param {module:echarts/model/Global} ecModel
 * @param {module:echarts/ExtensionAPI} api
 * @return {Object} content of event obj for echarts.connect.
 */

function _default(payload, ecModel, api) {
  var currTrigger = payload.currTrigger;
  var point = [payload.x, payload.y];
  var finder = payload;
  var dispatchAction = payload.dispatchAction || zrUtil.bind(api.dispatchAction, api);
  var coordSysAxesInfo = ecModel.getComponent('axisPointer').coordSysAxesInfo; // Pending
  // See #6121. But we are not able to reproduce it yet.

  if (!coordSysAxesInfo) {
    return;
  }

  if (illegalPoint(point)) {
    // Used in the default behavior of `connection`: use the sample seriesIndex
    // and dataIndex. And also used in the tooltipView trigger.
    point = findPointFromSeries({
      seriesIndex: finder.seriesIndex,
      // Do not use dataIndexInside from other ec instance.
      // FIXME: auto detect it?
      dataIndex: finder.dataIndex
    }, ecModel).point;
  }

  var isIllegalPoint = illegalPoint(point); // Axis and value can be specified when calling dispatchAction({type: 'updateAxisPointer'}).
  // Notice: In this case, it is difficult to get the `point` (which is necessary to show
  // tooltip, so if point is not given, we just use the point found by sample seriesIndex
  // and dataIndex.

  var inputAxesInfo = finder.axesInfo;
  var axesInfo = coordSysAxesInfo.axesInfo;
  var shouldHide = currTrigger === 'leave' || illegalPoint(point);
  var outputFinder = {};
  var showValueMap = {};
  var dataByCoordSys = {
    list: [],
    map: {}
  };
  var updaters = {
    showPointer: curry(showPointer, showValueMap),
    showTooltip: curry(showTooltip, dataByCoordSys)
  }; // Process for triggered axes.

  each(coordSysAxesInfo.coordSysMap, function (coordSys, coordSysKey) {
    // If a point given, it must be contained by the coordinate system.
    var coordSysContainsPoint = isIllegalPoint || coordSys.containPoint(point);
    each(coordSysAxesInfo.coordSysAxesInfo[coordSysKey], function (axisInfo, key) {
      var axis = axisInfo.axis;
      var inputAxisInfo = findInputAxisInfo(inputAxesInfo, axisInfo); // If no inputAxesInfo, no axis is restricted.

      if (!shouldHide && coordSysContainsPoint && (!inputAxesInfo || inputAxisInfo)) {
        var val = inputAxisInfo && inputAxisInfo.value;

        if (val == null && !isIllegalPoint) {
          val = axis.pointToData(point);
        }

        val != null && processOnAxis(axisInfo, val, updaters, false, outputFinder);
      }
    });
  }); // Process for linked axes.

  var linkTriggers = {};
  each(axesInfo, function (tarAxisInfo, tarKey) {
    var linkGroup = tarAxisInfo.linkGroup; // If axis has been triggered in the previous stage, it should not be triggered by link.

    if (linkGroup && !showValueMap[tarKey]) {
      each(linkGroup.axesInfo, function (srcAxisInfo, srcKey) {
        var srcValItem = showValueMap[srcKey]; // If srcValItem exist, source axis is triggered, so link to target axis.

        if (srcAxisInfo !== tarAxisInfo && srcValItem) {
          var val = srcValItem.value;
          linkGroup.mapper && (val = tarAxisInfo.axis.scale.parse(linkGroup.mapper(val, makeMapperParam(srcAxisInfo), makeMapperParam(tarAxisInfo))));
          linkTriggers[tarAxisInfo.key] = val;
        }
      });
    }
  });
  each(linkTriggers, function (val, tarKey) {
    processOnAxis(axesInfo[tarKey], val, updaters, true, outputFinder);
  });
  updateModelActually(showValueMap, axesInfo, outputFinder);
  dispatchTooltipActually(dataByCoordSys, point, payload, dispatchAction);
  dispatchHighDownActually(axesInfo, dispatchAction, api);
  return outputFinder;
}

function processOnAxis(axisInfo, newValue, updaters, dontSnap, outputFinder) {
  var axis = axisInfo.axis;

  if (axis.scale.isBlank() || !axis.containData(newValue)) {
    return;
  }

  if (!axisInfo.involveSeries) {
    updaters.showPointer(axisInfo, newValue);
    return;
  } // Heavy calculation. So put it after axis.containData checking.


  var payloadInfo = buildPayloadsBySeries(newValue, axisInfo);
  var payloadBatch = payloadInfo.payloadBatch;
  var snapToValue = payloadInfo.snapToValue; // Fill content of event obj for echarts.connect.
  // By defualt use the first involved series data as a sample to connect.

  if (payloadBatch[0] && outputFinder.seriesIndex == null) {
    zrUtil.extend(outputFinder, payloadBatch[0]);
  } // If no linkSource input, this process is for collecting link
  // target, where snap should not be accepted.


  if (!dontSnap && axisInfo.snap) {
    if (axis.containData(snapToValue) && snapToValue != null) {
      newValue = snapToValue;
    }
  }

  updaters.showPointer(axisInfo, newValue, payloadBatch, outputFinder); // Tooltip should always be snapToValue, otherwise there will be
  // incorrect "axis value ~ series value" mapping displayed in tooltip.

  updaters.showTooltip(axisInfo, payloadInfo, snapToValue);
}

function buildPayloadsBySeries(value, axisInfo) {
  var axis = axisInfo.axis;
  var dim = axis.dim;
  var snapToValue = value;
  var payloadBatch = [];
  var minDist = Number.MAX_VALUE;
  var minDiff = -1;
  each(axisInfo.seriesModels, function (series, idx) {
    var dataDim = series.getData().mapDimension(dim, true);
    var seriesNestestValue;
    var dataIndices;

    if (series.getAxisTooltipData) {
      var result = series.getAxisTooltipData(dataDim, value, axis);
      dataIndices = result.dataIndices;
      seriesNestestValue = result.nestestValue;
    } else {
      dataIndices = series.getData().indicesOfNearest(dataDim[0], value, // Add a threshold to avoid find the wrong dataIndex
      // when data length is not same.
      // false,
      axis.type === 'category' ? 0.5 : null);

      if (!dataIndices.length) {
        return;
      }

      seriesNestestValue = series.getData().get(dataDim[0], dataIndices[0]);
    }

    if (seriesNestestValue == null || !isFinite(seriesNestestValue)) {
      return;
    }

    var diff = value - seriesNestestValue;
    var dist = Math.abs(diff); // Consider category case

    if (dist <= minDist) {
      if (dist < minDist || diff >= 0 && minDiff < 0) {
        minDist = dist;
        minDiff = diff;
        snapToValue = seriesNestestValue;
        payloadBatch.length = 0;
      }

      each(dataIndices, function (dataIndex) {
        payloadBatch.push({
          seriesIndex: series.seriesIndex,
          dataIndexInside: dataIndex,
          dataIndex: series.getData().getRawIndex(dataIndex)
        });
      });
    }
  });
  return {
    payloadBatch: payloadBatch,
    snapToValue: snapToValue
  };
}

function showPointer(showValueMap, axisInfo, value, payloadBatch) {
  showValueMap[axisInfo.key] = {
    value: value,
    payloadBatch: payloadBatch
  };
}

function showTooltip(dataByCoordSys, axisInfo, payloadInfo, value) {
  var payloadBatch = payloadInfo.payloadBatch;
  var axis = axisInfo.axis;
  var axisModel = axis.model;
  var axisPointerModel = axisInfo.axisPointerModel; // If no data, do not create anything in dataByCoordSys,
  // whose length will be used to judge whether dispatch action.

  if (!axisInfo.triggerTooltip || !payloadBatch.length) {
    return;
  }

  var coordSysModel = axisInfo.coordSys.model;
  var coordSysKey = modelHelper.makeKey(coordSysModel);
  var coordSysItem = dataByCoordSys.map[coordSysKey];

  if (!coordSysItem) {
    coordSysItem = dataByCoordSys.map[coordSysKey] = {
      coordSysId: coordSysModel.id,
      coordSysIndex: coordSysModel.componentIndex,
      coordSysType: coordSysModel.type,
      coordSysMainType: coordSysModel.mainType,
      dataByAxis: []
    };
    dataByCoordSys.list.push(coordSysItem);
  }

  coordSysItem.dataByAxis.push({
    axisDim: axis.dim,
    axisIndex: axisModel.componentIndex,
    axisType: axisModel.type,
    axisId: axisModel.id,
    value: value,
    // Caustion: viewHelper.getValueLabel is actually on "view stage", which
    // depends that all models have been updated. So it should not be performed
    // here. Considering axisPointerModel used here is volatile, which is hard
    // to be retrieve in TooltipView, we prepare parameters here.
    valueLabelOpt: {
      precision: axisPointerModel.get('label.precision'),
      formatter: axisPointerModel.get('label.formatter')
    },
    seriesDataIndices: payloadBatch.slice()
  });
}

function updateModelActually(showValueMap, axesInfo, outputFinder) {
  var outputAxesInfo = outputFinder.axesInfo = []; // Basic logic: If no 'show' required, 'hide' this axisPointer.

  each(axesInfo, function (axisInfo, key) {
    var option = axisInfo.axisPointerModel.option;
    var valItem = showValueMap[key];

    if (valItem) {
      !axisInfo.useHandle && (option.status = 'show');
      option.value = valItem.value; // For label formatter param and highlight.

      option.seriesDataIndices = (valItem.payloadBatch || []).slice();
    } // When always show (e.g., handle used), remain
    // original value and status.
    else {
        // If hide, value still need to be set, consider
        // click legend to toggle axis blank.
        !axisInfo.useHandle && (option.status = 'hide');
      } // If status is 'hide', should be no info in payload.


    option.status === 'show' && outputAxesInfo.push({
      axisDim: axisInfo.axis.dim,
      axisIndex: axisInfo.axis.model.componentIndex,
      value: option.value
    });
  });
}

function dispatchTooltipActually(dataByCoordSys, point, payload, dispatchAction) {
  // Basic logic: If no showTip required, hideTip will be dispatched.
  if (illegalPoint(point) || !dataByCoordSys.list.length) {
    dispatchAction({
      type: 'hideTip'
    });
    return;
  } // In most case only one axis (or event one series is used). It is
  // convinient to fetch payload.seriesIndex and payload.dataIndex
  // dirtectly. So put the first seriesIndex and dataIndex of the first
  // axis on the payload.


  var sampleItem = ((dataByCoordSys.list[0].dataByAxis[0] || {}).seriesDataIndices || [])[0] || {};
  dispatchAction({
    type: 'showTip',
    escapeConnect: true,
    x: point[0],
    y: point[1],
    tooltipOption: payload.tooltipOption,
    position: payload.position,
    dataIndexInside: sampleItem.dataIndexInside,
    dataIndex: sampleItem.dataIndex,
    seriesIndex: sampleItem.seriesIndex,
    dataByCoordSys: dataByCoordSys.list
  });
}

function dispatchHighDownActually(axesInfo, dispatchAction, api) {
  // FIXME
  // highlight status modification shoule be a stage of main process?
  // (Consider confilct (e.g., legend and axisPointer) and setOption)
  var zr = api.getZr();
  var highDownKey = 'axisPointerLastHighlights';
  var lastHighlights = inner(zr)[highDownKey] || {};
  var newHighlights = inner(zr)[highDownKey] = {}; // Update highlight/downplay status according to axisPointer model.
  // Build hash map and remove duplicate incidentally.

  each(axesInfo, function (axisInfo, key) {
    var option = axisInfo.axisPointerModel.option;
    option.status === 'show' && each(option.seriesDataIndices, function (batchItem) {
      var key = batchItem.seriesIndex + ' | ' + batchItem.dataIndex;
      newHighlights[key] = batchItem;
    });
  }); // Diff.

  var toHighlight = [];
  var toDownplay = [];
  zrUtil.each(lastHighlights, function (batchItem, key) {
    !newHighlights[key] && toDownplay.push(batchItem);
  });
  zrUtil.each(newHighlights, function (batchItem, key) {
    !lastHighlights[key] && toHighlight.push(batchItem);
  });
  toDownplay.length && api.dispatchAction({
    type: 'downplay',
    escapeConnect: true,
    batch: toDownplay
  });
  toHighlight.length && api.dispatchAction({
    type: 'highlight',
    escapeConnect: true,
    batch: toHighlight
  });
}

function findInputAxisInfo(inputAxesInfo, axisInfo) {
  for (var i = 0; i < (inputAxesInfo || []).length; i++) {
    var inputAxisInfo = inputAxesInfo[i];

    if (axisInfo.axis.dim === inputAxisInfo.axisDim && axisInfo.axis.model.componentIndex === inputAxisInfo.axisIndex) {
      return inputAxisInfo;
    }
  }
}

function makeMapperParam(axisInfo) {
  var axisModel = axisInfo.axis.model;
  var item = {};
  var dim = item.axisDim = axisInfo.axis.dim;
  item.axisIndex = item[dim + 'AxisIndex'] = axisModel.componentIndex;
  item.axisName = item[dim + 'AxisName'] = axisModel.name;
  item.axisId = item[dim + 'AxisId'] = axisModel.id;
  return item;
}

function illegalPoint(point) {
  return !point || point[0] == null || isNaN(point[0]) || point[1] == null || isNaN(point[1]);
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL2F4aXNUcmlnZ2VyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL2F4aXNUcmlnZ2VyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIF9tb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL21vZGVsXCIpO1xuXG52YXIgbWFrZUlubmVyID0gX21vZGVsLm1ha2VJbm5lcjtcblxudmFyIG1vZGVsSGVscGVyID0gcmVxdWlyZShcIi4vbW9kZWxIZWxwZXJcIik7XG5cbnZhciBmaW5kUG9pbnRGcm9tU2VyaWVzID0gcmVxdWlyZShcIi4vZmluZFBvaW50RnJvbVNlcmllc1wiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGVhY2ggPSB6clV0aWwuZWFjaDtcbnZhciBjdXJyeSA9IHpyVXRpbC5jdXJyeTtcbnZhciBpbm5lciA9IG1ha2VJbm5lcigpO1xuLyoqXG4gKiBCYXNpYyBsb2dpYzogY2hlY2sgYWxsIGF4aXMsIGlmIHRoZXkgZG8gbm90IGRlbWFuZCBzaG93L2hpZ2hsaWdodCxcbiAqIHRoZW4gaGlkZS9kb3ducGxheSB0aGVtLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBjb29yZFN5c0F4ZXNJbmZvXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IFtwYXlsb2FkLmN1cnJUcmlnZ2VyXSAnY2xpY2snIHwgJ21vdXNlbW92ZScgfCAnbGVhdmUnXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBbcGF5bG9hZC54XSB4IGFuZCB5LCB3aGljaCBhcmUgbWFuZGF0b3J5LCBzcGVjaWZ5IGEgcG9pbnQgdG9cbiAqICAgICAgICAgICAgICB0cmlnZ2VyIGF4aXNQb2ludGVyIGFuZCB0b29sdGlwLlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW3BheWxvYWQueV0geCBhbmQgeSwgd2hpY2ggYXJlIG1hbmRhdG9yeSwgc3BlY2lmeSBhIHBvaW50IHRvXG4gKiAgICAgICAgICAgICAgdHJpZ2dlciBheGlzUG9pbnRlciBhbmQgdG9vbHRpcC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbcGF5bG9hZC5zZXJpZXNJbmRleF0gZmluZGVyLCBvcHRpb25hbCwgcmVzdHJpY3QgdGFyZ2V0IGF4ZXMuXG4gKiBAcGFyYW0ge09iamVjdH0gW3BheWxvYWQuZGF0YUluZGV4XSBmaW5kZXIsIHJlc3RyaWN0IHRhcmdldCBheGVzLlxuICogQHBhcmFtIHtPYmplY3R9IFtwYXlsb2FkLmF4ZXNJbmZvXSBmaW5kZXIsIHJlc3RyaWN0IHRhcmdldCBheGVzLlxuICogICAgICAgIFt7XG4gKiAgICAgICAgICBheGlzRGltOiAneCd8J3knfCdhbmdsZSd8Li4uLFxuICogICAgICAgICAgYXhpc0luZGV4OiAuLi4sXG4gKiAgICAgICAgICB2YWx1ZTogLi4uXG4gKiAgICAgICAgfSwgLi4uXVxuICogQHBhcmFtIHtGdW5jdGlvbn0gW3BheWxvYWQuZGlzcGF0Y2hBY3Rpb25dXG4gKiBAcGFyYW0ge09iamVjdH0gW3BheWxvYWQudG9vbHRpcE9wdGlvbl1cbiAqIEBwYXJhbSB7T2JqZWN0fEFycmF5LjxudW1iZXI+fEZ1bmN0aW9ufSBbcGF5bG9hZC5wb3NpdGlvbl0gVG9vbHRpcCBwb3NpdGlvbixcbiAqICAgICAgICB3aGljaCBjYW4gYmUgc3BlY2lmaWVkIGluIGRpc3BhdGNoQWN0aW9uXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICogQHJldHVybiB7T2JqZWN0fSBjb250ZW50IG9mIGV2ZW50IG9iaiBmb3IgZWNoYXJ0cy5jb25uZWN0LlxuICovXG5cbmZ1bmN0aW9uIF9kZWZhdWx0KHBheWxvYWQsIGVjTW9kZWwsIGFwaSkge1xuICB2YXIgY3VyclRyaWdnZXIgPSBwYXlsb2FkLmN1cnJUcmlnZ2VyO1xuICB2YXIgcG9pbnQgPSBbcGF5bG9hZC54LCBwYXlsb2FkLnldO1xuICB2YXIgZmluZGVyID0gcGF5bG9hZDtcbiAgdmFyIGRpc3BhdGNoQWN0aW9uID0gcGF5bG9hZC5kaXNwYXRjaEFjdGlvbiB8fCB6clV0aWwuYmluZChhcGkuZGlzcGF0Y2hBY3Rpb24sIGFwaSk7XG4gIHZhciBjb29yZFN5c0F4ZXNJbmZvID0gZWNNb2RlbC5nZXRDb21wb25lbnQoJ2F4aXNQb2ludGVyJykuY29vcmRTeXNBeGVzSW5mbzsgLy8gUGVuZGluZ1xuICAvLyBTZWUgIzYxMjEuIEJ1dCB3ZSBhcmUgbm90IGFibGUgdG8gcmVwcm9kdWNlIGl0IHlldC5cblxuICBpZiAoIWNvb3JkU3lzQXhlc0luZm8pIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAoaWxsZWdhbFBvaW50KHBvaW50KSkge1xuICAgIC8vIFVzZWQgaW4gdGhlIGRlZmF1bHQgYmVoYXZpb3Igb2YgYGNvbm5lY3Rpb25gOiB1c2UgdGhlIHNhbXBsZSBzZXJpZXNJbmRleFxuICAgIC8vIGFuZCBkYXRhSW5kZXguIEFuZCBhbHNvIHVzZWQgaW4gdGhlIHRvb2x0aXBWaWV3IHRyaWdnZXIuXG4gICAgcG9pbnQgPSBmaW5kUG9pbnRGcm9tU2VyaWVzKHtcbiAgICAgIHNlcmllc0luZGV4OiBmaW5kZXIuc2VyaWVzSW5kZXgsXG4gICAgICAvLyBEbyBub3QgdXNlIGRhdGFJbmRleEluc2lkZSBmcm9tIG90aGVyIGVjIGluc3RhbmNlLlxuICAgICAgLy8gRklYTUU6IGF1dG8gZGV0ZWN0IGl0P1xuICAgICAgZGF0YUluZGV4OiBmaW5kZXIuZGF0YUluZGV4XG4gICAgfSwgZWNNb2RlbCkucG9pbnQ7XG4gIH1cblxuICB2YXIgaXNJbGxlZ2FsUG9pbnQgPSBpbGxlZ2FsUG9pbnQocG9pbnQpOyAvLyBBeGlzIGFuZCB2YWx1ZSBjYW4gYmUgc3BlY2lmaWVkIHdoZW4gY2FsbGluZyBkaXNwYXRjaEFjdGlvbih7dHlwZTogJ3VwZGF0ZUF4aXNQb2ludGVyJ30pLlxuICAvLyBOb3RpY2U6IEluIHRoaXMgY2FzZSwgaXQgaXMgZGlmZmljdWx0IHRvIGdldCB0aGUgYHBvaW50YCAod2hpY2ggaXMgbmVjZXNzYXJ5IHRvIHNob3dcbiAgLy8gdG9vbHRpcCwgc28gaWYgcG9pbnQgaXMgbm90IGdpdmVuLCB3ZSBqdXN0IHVzZSB0aGUgcG9pbnQgZm91bmQgYnkgc2FtcGxlIHNlcmllc0luZGV4XG4gIC8vIGFuZCBkYXRhSW5kZXguXG5cbiAgdmFyIGlucHV0QXhlc0luZm8gPSBmaW5kZXIuYXhlc0luZm87XG4gIHZhciBheGVzSW5mbyA9IGNvb3JkU3lzQXhlc0luZm8uYXhlc0luZm87XG4gIHZhciBzaG91bGRIaWRlID0gY3VyclRyaWdnZXIgPT09ICdsZWF2ZScgfHwgaWxsZWdhbFBvaW50KHBvaW50KTtcbiAgdmFyIG91dHB1dEZpbmRlciA9IHt9O1xuICB2YXIgc2hvd1ZhbHVlTWFwID0ge307XG4gIHZhciBkYXRhQnlDb29yZFN5cyA9IHtcbiAgICBsaXN0OiBbXSxcbiAgICBtYXA6IHt9XG4gIH07XG4gIHZhciB1cGRhdGVycyA9IHtcbiAgICBzaG93UG9pbnRlcjogY3Vycnkoc2hvd1BvaW50ZXIsIHNob3dWYWx1ZU1hcCksXG4gICAgc2hvd1Rvb2x0aXA6IGN1cnJ5KHNob3dUb29sdGlwLCBkYXRhQnlDb29yZFN5cylcbiAgfTsgLy8gUHJvY2VzcyBmb3IgdHJpZ2dlcmVkIGF4ZXMuXG5cbiAgZWFjaChjb29yZFN5c0F4ZXNJbmZvLmNvb3JkU3lzTWFwLCBmdW5jdGlvbiAoY29vcmRTeXMsIGNvb3JkU3lzS2V5KSB7XG4gICAgLy8gSWYgYSBwb2ludCBnaXZlbiwgaXQgbXVzdCBiZSBjb250YWluZWQgYnkgdGhlIGNvb3JkaW5hdGUgc3lzdGVtLlxuICAgIHZhciBjb29yZFN5c0NvbnRhaW5zUG9pbnQgPSBpc0lsbGVnYWxQb2ludCB8fCBjb29yZFN5cy5jb250YWluUG9pbnQocG9pbnQpO1xuICAgIGVhY2goY29vcmRTeXNBeGVzSW5mby5jb29yZFN5c0F4ZXNJbmZvW2Nvb3JkU3lzS2V5XSwgZnVuY3Rpb24gKGF4aXNJbmZvLCBrZXkpIHtcbiAgICAgIHZhciBheGlzID0gYXhpc0luZm8uYXhpcztcbiAgICAgIHZhciBpbnB1dEF4aXNJbmZvID0gZmluZElucHV0QXhpc0luZm8oaW5wdXRBeGVzSW5mbywgYXhpc0luZm8pOyAvLyBJZiBubyBpbnB1dEF4ZXNJbmZvLCBubyBheGlzIGlzIHJlc3RyaWN0ZWQuXG5cbiAgICAgIGlmICghc2hvdWxkSGlkZSAmJiBjb29yZFN5c0NvbnRhaW5zUG9pbnQgJiYgKCFpbnB1dEF4ZXNJbmZvIHx8IGlucHV0QXhpc0luZm8pKSB7XG4gICAgICAgIHZhciB2YWwgPSBpbnB1dEF4aXNJbmZvICYmIGlucHV0QXhpc0luZm8udmFsdWU7XG5cbiAgICAgICAgaWYgKHZhbCA9PSBudWxsICYmICFpc0lsbGVnYWxQb2ludCkge1xuICAgICAgICAgIHZhbCA9IGF4aXMucG9pbnRUb0RhdGEocG9pbnQpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFsICE9IG51bGwgJiYgcHJvY2Vzc09uQXhpcyhheGlzSW5mbywgdmFsLCB1cGRhdGVycywgZmFsc2UsIG91dHB1dEZpbmRlcik7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pOyAvLyBQcm9jZXNzIGZvciBsaW5rZWQgYXhlcy5cblxuICB2YXIgbGlua1RyaWdnZXJzID0ge307XG4gIGVhY2goYXhlc0luZm8sIGZ1bmN0aW9uICh0YXJBeGlzSW5mbywgdGFyS2V5KSB7XG4gICAgdmFyIGxpbmtHcm91cCA9IHRhckF4aXNJbmZvLmxpbmtHcm91cDsgLy8gSWYgYXhpcyBoYXMgYmVlbiB0cmlnZ2VyZWQgaW4gdGhlIHByZXZpb3VzIHN0YWdlLCBpdCBzaG91bGQgbm90IGJlIHRyaWdnZXJlZCBieSBsaW5rLlxuXG4gICAgaWYgKGxpbmtHcm91cCAmJiAhc2hvd1ZhbHVlTWFwW3RhcktleV0pIHtcbiAgICAgIGVhY2gobGlua0dyb3VwLmF4ZXNJbmZvLCBmdW5jdGlvbiAoc3JjQXhpc0luZm8sIHNyY0tleSkge1xuICAgICAgICB2YXIgc3JjVmFsSXRlbSA9IHNob3dWYWx1ZU1hcFtzcmNLZXldOyAvLyBJZiBzcmNWYWxJdGVtIGV4aXN0LCBzb3VyY2UgYXhpcyBpcyB0cmlnZ2VyZWQsIHNvIGxpbmsgdG8gdGFyZ2V0IGF4aXMuXG5cbiAgICAgICAgaWYgKHNyY0F4aXNJbmZvICE9PSB0YXJBeGlzSW5mbyAmJiBzcmNWYWxJdGVtKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IHNyY1ZhbEl0ZW0udmFsdWU7XG4gICAgICAgICAgbGlua0dyb3VwLm1hcHBlciAmJiAodmFsID0gdGFyQXhpc0luZm8uYXhpcy5zY2FsZS5wYXJzZShsaW5rR3JvdXAubWFwcGVyKHZhbCwgbWFrZU1hcHBlclBhcmFtKHNyY0F4aXNJbmZvKSwgbWFrZU1hcHBlclBhcmFtKHRhckF4aXNJbmZvKSkpKTtcbiAgICAgICAgICBsaW5rVHJpZ2dlcnNbdGFyQXhpc0luZm8ua2V5XSA9IHZhbDtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9KTtcbiAgZWFjaChsaW5rVHJpZ2dlcnMsIGZ1bmN0aW9uICh2YWwsIHRhcktleSkge1xuICAgIHByb2Nlc3NPbkF4aXMoYXhlc0luZm9bdGFyS2V5XSwgdmFsLCB1cGRhdGVycywgdHJ1ZSwgb3V0cHV0RmluZGVyKTtcbiAgfSk7XG4gIHVwZGF0ZU1vZGVsQWN0dWFsbHkoc2hvd1ZhbHVlTWFwLCBheGVzSW5mbywgb3V0cHV0RmluZGVyKTtcbiAgZGlzcGF0Y2hUb29sdGlwQWN0dWFsbHkoZGF0YUJ5Q29vcmRTeXMsIHBvaW50LCBwYXlsb2FkLCBkaXNwYXRjaEFjdGlvbik7XG4gIGRpc3BhdGNoSGlnaERvd25BY3R1YWxseShheGVzSW5mbywgZGlzcGF0Y2hBY3Rpb24sIGFwaSk7XG4gIHJldHVybiBvdXRwdXRGaW5kZXI7XG59XG5cbmZ1bmN0aW9uIHByb2Nlc3NPbkF4aXMoYXhpc0luZm8sIG5ld1ZhbHVlLCB1cGRhdGVycywgZG9udFNuYXAsIG91dHB1dEZpbmRlcikge1xuICB2YXIgYXhpcyA9IGF4aXNJbmZvLmF4aXM7XG5cbiAgaWYgKGF4aXMuc2NhbGUuaXNCbGFuaygpIHx8ICFheGlzLmNvbnRhaW5EYXRhKG5ld1ZhbHVlKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmICghYXhpc0luZm8uaW52b2x2ZVNlcmllcykge1xuICAgIHVwZGF0ZXJzLnNob3dQb2ludGVyKGF4aXNJbmZvLCBuZXdWYWx1ZSk7XG4gICAgcmV0dXJuO1xuICB9IC8vIEhlYXZ5IGNhbGN1bGF0aW9uLiBTbyBwdXQgaXQgYWZ0ZXIgYXhpcy5jb250YWluRGF0YSBjaGVja2luZy5cblxuXG4gIHZhciBwYXlsb2FkSW5mbyA9IGJ1aWxkUGF5bG9hZHNCeVNlcmllcyhuZXdWYWx1ZSwgYXhpc0luZm8pO1xuICB2YXIgcGF5bG9hZEJhdGNoID0gcGF5bG9hZEluZm8ucGF5bG9hZEJhdGNoO1xuICB2YXIgc25hcFRvVmFsdWUgPSBwYXlsb2FkSW5mby5zbmFwVG9WYWx1ZTsgLy8gRmlsbCBjb250ZW50IG9mIGV2ZW50IG9iaiBmb3IgZWNoYXJ0cy5jb25uZWN0LlxuICAvLyBCeSBkZWZ1YWx0IHVzZSB0aGUgZmlyc3QgaW52b2x2ZWQgc2VyaWVzIGRhdGEgYXMgYSBzYW1wbGUgdG8gY29ubmVjdC5cblxuICBpZiAocGF5bG9hZEJhdGNoWzBdICYmIG91dHB1dEZpbmRlci5zZXJpZXNJbmRleCA9PSBudWxsKSB7XG4gICAgenJVdGlsLmV4dGVuZChvdXRwdXRGaW5kZXIsIHBheWxvYWRCYXRjaFswXSk7XG4gIH0gLy8gSWYgbm8gbGlua1NvdXJjZSBpbnB1dCwgdGhpcyBwcm9jZXNzIGlzIGZvciBjb2xsZWN0aW5nIGxpbmtcbiAgLy8gdGFyZ2V0LCB3aGVyZSBzbmFwIHNob3VsZCBub3QgYmUgYWNjZXB0ZWQuXG5cblxuICBpZiAoIWRvbnRTbmFwICYmIGF4aXNJbmZvLnNuYXApIHtcbiAgICBpZiAoYXhpcy5jb250YWluRGF0YShzbmFwVG9WYWx1ZSkgJiYgc25hcFRvVmFsdWUgIT0gbnVsbCkge1xuICAgICAgbmV3VmFsdWUgPSBzbmFwVG9WYWx1ZTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGVycy5zaG93UG9pbnRlcihheGlzSW5mbywgbmV3VmFsdWUsIHBheWxvYWRCYXRjaCwgb3V0cHV0RmluZGVyKTsgLy8gVG9vbHRpcCBzaG91bGQgYWx3YXlzIGJlIHNuYXBUb1ZhbHVlLCBvdGhlcndpc2UgdGhlcmUgd2lsbCBiZVxuICAvLyBpbmNvcnJlY3QgXCJheGlzIHZhbHVlIH4gc2VyaWVzIHZhbHVlXCIgbWFwcGluZyBkaXNwbGF5ZWQgaW4gdG9vbHRpcC5cblxuICB1cGRhdGVycy5zaG93VG9vbHRpcChheGlzSW5mbywgcGF5bG9hZEluZm8sIHNuYXBUb1ZhbHVlKTtcbn1cblxuZnVuY3Rpb24gYnVpbGRQYXlsb2Fkc0J5U2VyaWVzKHZhbHVlLCBheGlzSW5mbykge1xuICB2YXIgYXhpcyA9IGF4aXNJbmZvLmF4aXM7XG4gIHZhciBkaW0gPSBheGlzLmRpbTtcbiAgdmFyIHNuYXBUb1ZhbHVlID0gdmFsdWU7XG4gIHZhciBwYXlsb2FkQmF0Y2ggPSBbXTtcbiAgdmFyIG1pbkRpc3QgPSBOdW1iZXIuTUFYX1ZBTFVFO1xuICB2YXIgbWluRGlmZiA9IC0xO1xuICBlYWNoKGF4aXNJbmZvLnNlcmllc01vZGVscywgZnVuY3Rpb24gKHNlcmllcywgaWR4KSB7XG4gICAgdmFyIGRhdGFEaW0gPSBzZXJpZXMuZ2V0RGF0YSgpLm1hcERpbWVuc2lvbihkaW0sIHRydWUpO1xuICAgIHZhciBzZXJpZXNOZXN0ZXN0VmFsdWU7XG4gICAgdmFyIGRhdGFJbmRpY2VzO1xuXG4gICAgaWYgKHNlcmllcy5nZXRBeGlzVG9vbHRpcERhdGEpIHtcbiAgICAgIHZhciByZXN1bHQgPSBzZXJpZXMuZ2V0QXhpc1Rvb2x0aXBEYXRhKGRhdGFEaW0sIHZhbHVlLCBheGlzKTtcbiAgICAgIGRhdGFJbmRpY2VzID0gcmVzdWx0LmRhdGFJbmRpY2VzO1xuICAgICAgc2VyaWVzTmVzdGVzdFZhbHVlID0gcmVzdWx0Lm5lc3Rlc3RWYWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgZGF0YUluZGljZXMgPSBzZXJpZXMuZ2V0RGF0YSgpLmluZGljZXNPZk5lYXJlc3QoZGF0YURpbVswXSwgdmFsdWUsIC8vIEFkZCBhIHRocmVzaG9sZCB0byBhdm9pZCBmaW5kIHRoZSB3cm9uZyBkYXRhSW5kZXhcbiAgICAgIC8vIHdoZW4gZGF0YSBsZW5ndGggaXMgbm90IHNhbWUuXG4gICAgICAvLyBmYWxzZSxcbiAgICAgIGF4aXMudHlwZSA9PT0gJ2NhdGVnb3J5JyA/IDAuNSA6IG51bGwpO1xuXG4gICAgICBpZiAoIWRhdGFJbmRpY2VzLmxlbmd0aCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHNlcmllc05lc3Rlc3RWYWx1ZSA9IHNlcmllcy5nZXREYXRhKCkuZ2V0KGRhdGFEaW1bMF0sIGRhdGFJbmRpY2VzWzBdKTtcbiAgICB9XG5cbiAgICBpZiAoc2VyaWVzTmVzdGVzdFZhbHVlID09IG51bGwgfHwgIWlzRmluaXRlKHNlcmllc05lc3Rlc3RWYWx1ZSkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZGlmZiA9IHZhbHVlIC0gc2VyaWVzTmVzdGVzdFZhbHVlO1xuICAgIHZhciBkaXN0ID0gTWF0aC5hYnMoZGlmZik7IC8vIENvbnNpZGVyIGNhdGVnb3J5IGNhc2VcblxuICAgIGlmIChkaXN0IDw9IG1pbkRpc3QpIHtcbiAgICAgIGlmIChkaXN0IDwgbWluRGlzdCB8fCBkaWZmID49IDAgJiYgbWluRGlmZiA8IDApIHtcbiAgICAgICAgbWluRGlzdCA9IGRpc3Q7XG4gICAgICAgIG1pbkRpZmYgPSBkaWZmO1xuICAgICAgICBzbmFwVG9WYWx1ZSA9IHNlcmllc05lc3Rlc3RWYWx1ZTtcbiAgICAgICAgcGF5bG9hZEJhdGNoLmxlbmd0aCA9IDA7XG4gICAgICB9XG5cbiAgICAgIGVhY2goZGF0YUluZGljZXMsIGZ1bmN0aW9uIChkYXRhSW5kZXgpIHtcbiAgICAgICAgcGF5bG9hZEJhdGNoLnB1c2goe1xuICAgICAgICAgIHNlcmllc0luZGV4OiBzZXJpZXMuc2VyaWVzSW5kZXgsXG4gICAgICAgICAgZGF0YUluZGV4SW5zaWRlOiBkYXRhSW5kZXgsXG4gICAgICAgICAgZGF0YUluZGV4OiBzZXJpZXMuZ2V0RGF0YSgpLmdldFJhd0luZGV4KGRhdGFJbmRleClcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4ge1xuICAgIHBheWxvYWRCYXRjaDogcGF5bG9hZEJhdGNoLFxuICAgIHNuYXBUb1ZhbHVlOiBzbmFwVG9WYWx1ZVxuICB9O1xufVxuXG5mdW5jdGlvbiBzaG93UG9pbnRlcihzaG93VmFsdWVNYXAsIGF4aXNJbmZvLCB2YWx1ZSwgcGF5bG9hZEJhdGNoKSB7XG4gIHNob3dWYWx1ZU1hcFtheGlzSW5mby5rZXldID0ge1xuICAgIHZhbHVlOiB2YWx1ZSxcbiAgICBwYXlsb2FkQmF0Y2g6IHBheWxvYWRCYXRjaFxuICB9O1xufVxuXG5mdW5jdGlvbiBzaG93VG9vbHRpcChkYXRhQnlDb29yZFN5cywgYXhpc0luZm8sIHBheWxvYWRJbmZvLCB2YWx1ZSkge1xuICB2YXIgcGF5bG9hZEJhdGNoID0gcGF5bG9hZEluZm8ucGF5bG9hZEJhdGNoO1xuICB2YXIgYXhpcyA9IGF4aXNJbmZvLmF4aXM7XG4gIHZhciBheGlzTW9kZWwgPSBheGlzLm1vZGVsO1xuICB2YXIgYXhpc1BvaW50ZXJNb2RlbCA9IGF4aXNJbmZvLmF4aXNQb2ludGVyTW9kZWw7IC8vIElmIG5vIGRhdGEsIGRvIG5vdCBjcmVhdGUgYW55dGhpbmcgaW4gZGF0YUJ5Q29vcmRTeXMsXG4gIC8vIHdob3NlIGxlbmd0aCB3aWxsIGJlIHVzZWQgdG8ganVkZ2Ugd2hldGhlciBkaXNwYXRjaCBhY3Rpb24uXG5cbiAgaWYgKCFheGlzSW5mby50cmlnZ2VyVG9vbHRpcCB8fCAhcGF5bG9hZEJhdGNoLmxlbmd0aCkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBjb29yZFN5c01vZGVsID0gYXhpc0luZm8uY29vcmRTeXMubW9kZWw7XG4gIHZhciBjb29yZFN5c0tleSA9IG1vZGVsSGVscGVyLm1ha2VLZXkoY29vcmRTeXNNb2RlbCk7XG4gIHZhciBjb29yZFN5c0l0ZW0gPSBkYXRhQnlDb29yZFN5cy5tYXBbY29vcmRTeXNLZXldO1xuXG4gIGlmICghY29vcmRTeXNJdGVtKSB7XG4gICAgY29vcmRTeXNJdGVtID0gZGF0YUJ5Q29vcmRTeXMubWFwW2Nvb3JkU3lzS2V5XSA9IHtcbiAgICAgIGNvb3JkU3lzSWQ6IGNvb3JkU3lzTW9kZWwuaWQsXG4gICAgICBjb29yZFN5c0luZGV4OiBjb29yZFN5c01vZGVsLmNvbXBvbmVudEluZGV4LFxuICAgICAgY29vcmRTeXNUeXBlOiBjb29yZFN5c01vZGVsLnR5cGUsXG4gICAgICBjb29yZFN5c01haW5UeXBlOiBjb29yZFN5c01vZGVsLm1haW5UeXBlLFxuICAgICAgZGF0YUJ5QXhpczogW11cbiAgICB9O1xuICAgIGRhdGFCeUNvb3JkU3lzLmxpc3QucHVzaChjb29yZFN5c0l0ZW0pO1xuICB9XG5cbiAgY29vcmRTeXNJdGVtLmRhdGFCeUF4aXMucHVzaCh7XG4gICAgYXhpc0RpbTogYXhpcy5kaW0sXG4gICAgYXhpc0luZGV4OiBheGlzTW9kZWwuY29tcG9uZW50SW5kZXgsXG4gICAgYXhpc1R5cGU6IGF4aXNNb2RlbC50eXBlLFxuICAgIGF4aXNJZDogYXhpc01vZGVsLmlkLFxuICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAvLyBDYXVzdGlvbjogdmlld0hlbHBlci5nZXRWYWx1ZUxhYmVsIGlzIGFjdHVhbGx5IG9uIFwidmlldyBzdGFnZVwiLCB3aGljaFxuICAgIC8vIGRlcGVuZHMgdGhhdCBhbGwgbW9kZWxzIGhhdmUgYmVlbiB1cGRhdGVkLiBTbyBpdCBzaG91bGQgbm90IGJlIHBlcmZvcm1lZFxuICAgIC8vIGhlcmUuIENvbnNpZGVyaW5nIGF4aXNQb2ludGVyTW9kZWwgdXNlZCBoZXJlIGlzIHZvbGF0aWxlLCB3aGljaCBpcyBoYXJkXG4gICAgLy8gdG8gYmUgcmV0cmlldmUgaW4gVG9vbHRpcFZpZXcsIHdlIHByZXBhcmUgcGFyYW1ldGVycyBoZXJlLlxuICAgIHZhbHVlTGFiZWxPcHQ6IHtcbiAgICAgIHByZWNpc2lvbjogYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ2xhYmVsLnByZWNpc2lvbicpLFxuICAgICAgZm9ybWF0dGVyOiBheGlzUG9pbnRlck1vZGVsLmdldCgnbGFiZWwuZm9ybWF0dGVyJylcbiAgICB9LFxuICAgIHNlcmllc0RhdGFJbmRpY2VzOiBwYXlsb2FkQmF0Y2guc2xpY2UoKVxuICB9KTtcbn1cblxuZnVuY3Rpb24gdXBkYXRlTW9kZWxBY3R1YWxseShzaG93VmFsdWVNYXAsIGF4ZXNJbmZvLCBvdXRwdXRGaW5kZXIpIHtcbiAgdmFyIG91dHB1dEF4ZXNJbmZvID0gb3V0cHV0RmluZGVyLmF4ZXNJbmZvID0gW107IC8vIEJhc2ljIGxvZ2ljOiBJZiBubyAnc2hvdycgcmVxdWlyZWQsICdoaWRlJyB0aGlzIGF4aXNQb2ludGVyLlxuXG4gIGVhY2goYXhlc0luZm8sIGZ1bmN0aW9uIChheGlzSW5mbywga2V5KSB7XG4gICAgdmFyIG9wdGlvbiA9IGF4aXNJbmZvLmF4aXNQb2ludGVyTW9kZWwub3B0aW9uO1xuICAgIHZhciB2YWxJdGVtID0gc2hvd1ZhbHVlTWFwW2tleV07XG5cbiAgICBpZiAodmFsSXRlbSkge1xuICAgICAgIWF4aXNJbmZvLnVzZUhhbmRsZSAmJiAob3B0aW9uLnN0YXR1cyA9ICdzaG93Jyk7XG4gICAgICBvcHRpb24udmFsdWUgPSB2YWxJdGVtLnZhbHVlOyAvLyBGb3IgbGFiZWwgZm9ybWF0dGVyIHBhcmFtIGFuZCBoaWdobGlnaHQuXG5cbiAgICAgIG9wdGlvbi5zZXJpZXNEYXRhSW5kaWNlcyA9ICh2YWxJdGVtLnBheWxvYWRCYXRjaCB8fCBbXSkuc2xpY2UoKTtcbiAgICB9IC8vIFdoZW4gYWx3YXlzIHNob3cgKGUuZy4sIGhhbmRsZSB1c2VkKSwgcmVtYWluXG4gICAgLy8gb3JpZ2luYWwgdmFsdWUgYW5kIHN0YXR1cy5cbiAgICBlbHNlIHtcbiAgICAgICAgLy8gSWYgaGlkZSwgdmFsdWUgc3RpbGwgbmVlZCB0byBiZSBzZXQsIGNvbnNpZGVyXG4gICAgICAgIC8vIGNsaWNrIGxlZ2VuZCB0byB0b2dnbGUgYXhpcyBibGFuay5cbiAgICAgICAgIWF4aXNJbmZvLnVzZUhhbmRsZSAmJiAob3B0aW9uLnN0YXR1cyA9ICdoaWRlJyk7XG4gICAgICB9IC8vIElmIHN0YXR1cyBpcyAnaGlkZScsIHNob3VsZCBiZSBubyBpbmZvIGluIHBheWxvYWQuXG5cblxuICAgIG9wdGlvbi5zdGF0dXMgPT09ICdzaG93JyAmJiBvdXRwdXRBeGVzSW5mby5wdXNoKHtcbiAgICAgIGF4aXNEaW06IGF4aXNJbmZvLmF4aXMuZGltLFxuICAgICAgYXhpc0luZGV4OiBheGlzSW5mby5heGlzLm1vZGVsLmNvbXBvbmVudEluZGV4LFxuICAgICAgdmFsdWU6IG9wdGlvbi52YWx1ZVxuICAgIH0pO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gZGlzcGF0Y2hUb29sdGlwQWN0dWFsbHkoZGF0YUJ5Q29vcmRTeXMsIHBvaW50LCBwYXlsb2FkLCBkaXNwYXRjaEFjdGlvbikge1xuICAvLyBCYXNpYyBsb2dpYzogSWYgbm8gc2hvd1RpcCByZXF1aXJlZCwgaGlkZVRpcCB3aWxsIGJlIGRpc3BhdGNoZWQuXG4gIGlmIChpbGxlZ2FsUG9pbnQocG9pbnQpIHx8ICFkYXRhQnlDb29yZFN5cy5saXN0Lmxlbmd0aCkge1xuICAgIGRpc3BhdGNoQWN0aW9uKHtcbiAgICAgIHR5cGU6ICdoaWRlVGlwJ1xuICAgIH0pO1xuICAgIHJldHVybjtcbiAgfSAvLyBJbiBtb3N0IGNhc2Ugb25seSBvbmUgYXhpcyAob3IgZXZlbnQgb25lIHNlcmllcyBpcyB1c2VkKS4gSXQgaXNcbiAgLy8gY29udmluaWVudCB0byBmZXRjaCBwYXlsb2FkLnNlcmllc0luZGV4IGFuZCBwYXlsb2FkLmRhdGFJbmRleFxuICAvLyBkaXJ0ZWN0bHkuIFNvIHB1dCB0aGUgZmlyc3Qgc2VyaWVzSW5kZXggYW5kIGRhdGFJbmRleCBvZiB0aGUgZmlyc3RcbiAgLy8gYXhpcyBvbiB0aGUgcGF5bG9hZC5cblxuXG4gIHZhciBzYW1wbGVJdGVtID0gKChkYXRhQnlDb29yZFN5cy5saXN0WzBdLmRhdGFCeUF4aXNbMF0gfHwge30pLnNlcmllc0RhdGFJbmRpY2VzIHx8IFtdKVswXSB8fCB7fTtcbiAgZGlzcGF0Y2hBY3Rpb24oe1xuICAgIHR5cGU6ICdzaG93VGlwJyxcbiAgICBlc2NhcGVDb25uZWN0OiB0cnVlLFxuICAgIHg6IHBvaW50WzBdLFxuICAgIHk6IHBvaW50WzFdLFxuICAgIHRvb2x0aXBPcHRpb246IHBheWxvYWQudG9vbHRpcE9wdGlvbixcbiAgICBwb3NpdGlvbjogcGF5bG9hZC5wb3NpdGlvbixcbiAgICBkYXRhSW5kZXhJbnNpZGU6IHNhbXBsZUl0ZW0uZGF0YUluZGV4SW5zaWRlLFxuICAgIGRhdGFJbmRleDogc2FtcGxlSXRlbS5kYXRhSW5kZXgsXG4gICAgc2VyaWVzSW5kZXg6IHNhbXBsZUl0ZW0uc2VyaWVzSW5kZXgsXG4gICAgZGF0YUJ5Q29vcmRTeXM6IGRhdGFCeUNvb3JkU3lzLmxpc3RcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGRpc3BhdGNoSGlnaERvd25BY3R1YWxseShheGVzSW5mbywgZGlzcGF0Y2hBY3Rpb24sIGFwaSkge1xuICAvLyBGSVhNRVxuICAvLyBoaWdobGlnaHQgc3RhdHVzIG1vZGlmaWNhdGlvbiBzaG91bGUgYmUgYSBzdGFnZSBvZiBtYWluIHByb2Nlc3M/XG4gIC8vIChDb25zaWRlciBjb25maWxjdCAoZS5nLiwgbGVnZW5kIGFuZCBheGlzUG9pbnRlcikgYW5kIHNldE9wdGlvbilcbiAgdmFyIHpyID0gYXBpLmdldFpyKCk7XG4gIHZhciBoaWdoRG93bktleSA9ICdheGlzUG9pbnRlckxhc3RIaWdobGlnaHRzJztcbiAgdmFyIGxhc3RIaWdobGlnaHRzID0gaW5uZXIoenIpW2hpZ2hEb3duS2V5XSB8fCB7fTtcbiAgdmFyIG5ld0hpZ2hsaWdodHMgPSBpbm5lcih6cilbaGlnaERvd25LZXldID0ge307IC8vIFVwZGF0ZSBoaWdobGlnaHQvZG93bnBsYXkgc3RhdHVzIGFjY29yZGluZyB0byBheGlzUG9pbnRlciBtb2RlbC5cbiAgLy8gQnVpbGQgaGFzaCBtYXAgYW5kIHJlbW92ZSBkdXBsaWNhdGUgaW5jaWRlbnRhbGx5LlxuXG4gIGVhY2goYXhlc0luZm8sIGZ1bmN0aW9uIChheGlzSW5mbywga2V5KSB7XG4gICAgdmFyIG9wdGlvbiA9IGF4aXNJbmZvLmF4aXNQb2ludGVyTW9kZWwub3B0aW9uO1xuICAgIG9wdGlvbi5zdGF0dXMgPT09ICdzaG93JyAmJiBlYWNoKG9wdGlvbi5zZXJpZXNEYXRhSW5kaWNlcywgZnVuY3Rpb24gKGJhdGNoSXRlbSkge1xuICAgICAgdmFyIGtleSA9IGJhdGNoSXRlbS5zZXJpZXNJbmRleCArICcgfCAnICsgYmF0Y2hJdGVtLmRhdGFJbmRleDtcbiAgICAgIG5ld0hpZ2hsaWdodHNba2V5XSA9IGJhdGNoSXRlbTtcbiAgICB9KTtcbiAgfSk7IC8vIERpZmYuXG5cbiAgdmFyIHRvSGlnaGxpZ2h0ID0gW107XG4gIHZhciB0b0Rvd25wbGF5ID0gW107XG4gIHpyVXRpbC5lYWNoKGxhc3RIaWdobGlnaHRzLCBmdW5jdGlvbiAoYmF0Y2hJdGVtLCBrZXkpIHtcbiAgICAhbmV3SGlnaGxpZ2h0c1trZXldICYmIHRvRG93bnBsYXkucHVzaChiYXRjaEl0ZW0pO1xuICB9KTtcbiAgenJVdGlsLmVhY2gobmV3SGlnaGxpZ2h0cywgZnVuY3Rpb24gKGJhdGNoSXRlbSwga2V5KSB7XG4gICAgIWxhc3RIaWdobGlnaHRzW2tleV0gJiYgdG9IaWdobGlnaHQucHVzaChiYXRjaEl0ZW0pO1xuICB9KTtcbiAgdG9Eb3ducGxheS5sZW5ndGggJiYgYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICB0eXBlOiAnZG93bnBsYXknLFxuICAgIGVzY2FwZUNvbm5lY3Q6IHRydWUsXG4gICAgYmF0Y2g6IHRvRG93bnBsYXlcbiAgfSk7XG4gIHRvSGlnaGxpZ2h0Lmxlbmd0aCAmJiBhcGkuZGlzcGF0Y2hBY3Rpb24oe1xuICAgIHR5cGU6ICdoaWdobGlnaHQnLFxuICAgIGVzY2FwZUNvbm5lY3Q6IHRydWUsXG4gICAgYmF0Y2g6IHRvSGlnaGxpZ2h0XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBmaW5kSW5wdXRBeGlzSW5mbyhpbnB1dEF4ZXNJbmZvLCBheGlzSW5mbykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IChpbnB1dEF4ZXNJbmZvIHx8IFtdKS5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpbnB1dEF4aXNJbmZvID0gaW5wdXRBeGVzSW5mb1tpXTtcblxuICAgIGlmIChheGlzSW5mby5heGlzLmRpbSA9PT0gaW5wdXRBeGlzSW5mby5heGlzRGltICYmIGF4aXNJbmZvLmF4aXMubW9kZWwuY29tcG9uZW50SW5kZXggPT09IGlucHV0QXhpc0luZm8uYXhpc0luZGV4KSB7XG4gICAgICByZXR1cm4gaW5wdXRBeGlzSW5mbztcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gbWFrZU1hcHBlclBhcmFtKGF4aXNJbmZvKSB7XG4gIHZhciBheGlzTW9kZWwgPSBheGlzSW5mby5heGlzLm1vZGVsO1xuICB2YXIgaXRlbSA9IHt9O1xuICB2YXIgZGltID0gaXRlbS5heGlzRGltID0gYXhpc0luZm8uYXhpcy5kaW07XG4gIGl0ZW0uYXhpc0luZGV4ID0gaXRlbVtkaW0gKyAnQXhpc0luZGV4J10gPSBheGlzTW9kZWwuY29tcG9uZW50SW5kZXg7XG4gIGl0ZW0uYXhpc05hbWUgPSBpdGVtW2RpbSArICdBeGlzTmFtZSddID0gYXhpc01vZGVsLm5hbWU7XG4gIGl0ZW0uYXhpc0lkID0gaXRlbVtkaW0gKyAnQXhpc0lkJ10gPSBheGlzTW9kZWwuaWQ7XG4gIHJldHVybiBpdGVtO1xufVxuXG5mdW5jdGlvbiBpbGxlZ2FsUG9pbnQocG9pbnQpIHtcbiAgcmV0dXJuICFwb2ludCB8fCBwb2ludFswXSA9PSBudWxsIHx8IGlzTmFOKHBvaW50WzBdKSB8fCBwb2ludFsxXSA9PSBudWxsIHx8IGlzTmFOKHBvaW50WzFdKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBZEE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/axisTrigger.js
