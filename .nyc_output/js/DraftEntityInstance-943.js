/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEntityInstance
 * @legacyServerCallableInstance
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var Record = Immutable.Record;
var DraftEntityInstanceRecord = Record({
  type: 'TOKEN',
  mutability: 'IMMUTABLE',
  data: Object
});
/**
 * An instance of a document entity, consisting of a `type` and relevant
 * `data`, metadata about the entity.
 *
 * For instance, a "link" entity might provide a URI, and a "mention"
 * entity might provide the mentioned user's ID. These pieces of data
 * may be used when rendering the entity as part of a ContentBlock DOM
 * representation. For a link, the data would be used as an href for
 * the rendered anchor. For a mention, the ID could be used to retrieve
 * a hovercard.
 */

var DraftEntityInstance = function (_DraftEntityInstanceR) {
  _inherits(DraftEntityInstance, _DraftEntityInstanceR);

  function DraftEntityInstance() {
    _classCallCheck(this, DraftEntityInstance);

    return _possibleConstructorReturn(this, _DraftEntityInstanceR.apply(this, arguments));
  }

  DraftEntityInstance.prototype.getType = function getType() {
    return this.get('type');
  };

  DraftEntityInstance.prototype.getMutability = function getMutability() {
    return this.get('mutability');
  };

  DraftEntityInstance.prototype.getData = function getData() {
    return this.get('data');
  };

  return DraftEntityInstance;
}(DraftEntityInstanceRecord);

module.exports = DraftEntityInstance;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RW50aXR5SW5zdGFuY2UuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFbnRpdHlJbnN0YW5jZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RW50aXR5SW5zdGFuY2VcbiAqIEBsZWdhY3lTZXJ2ZXJDYWxsYWJsZUluc3RhbmNlXG4gKiBAZm9ybWF0XG4gKiBcbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgUmVjb3JkID0gSW1tdXRhYmxlLlJlY29yZDtcblxuXG52YXIgRHJhZnRFbnRpdHlJbnN0YW5jZVJlY29yZCA9IFJlY29yZCh7XG4gIHR5cGU6ICdUT0tFTicsXG4gIG11dGFiaWxpdHk6ICdJTU1VVEFCTEUnLFxuICBkYXRhOiBPYmplY3Rcbn0pO1xuXG4vKipcbiAqIEFuIGluc3RhbmNlIG9mIGEgZG9jdW1lbnQgZW50aXR5LCBjb25zaXN0aW5nIG9mIGEgYHR5cGVgIGFuZCByZWxldmFudFxuICogYGRhdGFgLCBtZXRhZGF0YSBhYm91dCB0aGUgZW50aXR5LlxuICpcbiAqIEZvciBpbnN0YW5jZSwgYSBcImxpbmtcIiBlbnRpdHkgbWlnaHQgcHJvdmlkZSBhIFVSSSwgYW5kIGEgXCJtZW50aW9uXCJcbiAqIGVudGl0eSBtaWdodCBwcm92aWRlIHRoZSBtZW50aW9uZWQgdXNlcidzIElELiBUaGVzZSBwaWVjZXMgb2YgZGF0YVxuICogbWF5IGJlIHVzZWQgd2hlbiByZW5kZXJpbmcgdGhlIGVudGl0eSBhcyBwYXJ0IG9mIGEgQ29udGVudEJsb2NrIERPTVxuICogcmVwcmVzZW50YXRpb24uIEZvciBhIGxpbmssIHRoZSBkYXRhIHdvdWxkIGJlIHVzZWQgYXMgYW4gaHJlZiBmb3JcbiAqIHRoZSByZW5kZXJlZCBhbmNob3IuIEZvciBhIG1lbnRpb24sIHRoZSBJRCBjb3VsZCBiZSB1c2VkIHRvIHJldHJpZXZlXG4gKiBhIGhvdmVyY2FyZC5cbiAqL1xuXG52YXIgRHJhZnRFbnRpdHlJbnN0YW5jZSA9IGZ1bmN0aW9uIChfRHJhZnRFbnRpdHlJbnN0YW5jZVIpIHtcbiAgX2luaGVyaXRzKERyYWZ0RW50aXR5SW5zdGFuY2UsIF9EcmFmdEVudGl0eUluc3RhbmNlUik7XG5cbiAgZnVuY3Rpb24gRHJhZnRFbnRpdHlJbnN0YW5jZSgpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRHJhZnRFbnRpdHlJbnN0YW5jZSk7XG5cbiAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0RyYWZ0RW50aXR5SW5zdGFuY2VSLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgRHJhZnRFbnRpdHlJbnN0YW5jZS5wcm90b3R5cGUuZ2V0VHlwZSA9IGZ1bmN0aW9uIGdldFR5cGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCd0eXBlJyk7XG4gIH07XG5cbiAgRHJhZnRFbnRpdHlJbnN0YW5jZS5wcm90b3R5cGUuZ2V0TXV0YWJpbGl0eSA9IGZ1bmN0aW9uIGdldE11dGFiaWxpdHkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdtdXRhYmlsaXR5Jyk7XG4gIH07XG5cbiAgRHJhZnRFbnRpdHlJbnN0YW5jZS5wcm90b3R5cGUuZ2V0RGF0YSA9IGZ1bmN0aW9uIGdldERhdGEoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdkYXRhJyk7XG4gIH07XG5cbiAgcmV0dXJuIERyYWZ0RW50aXR5SW5zdGFuY2U7XG59KERyYWZ0RW50aXR5SW5zdGFuY2VSZWNvcmQpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RW50aXR5SW5zdGFuY2U7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEntityInstance.js
