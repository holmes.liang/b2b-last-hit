__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return exportContent; });
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_1__);



function encodeContent(text) {
  return text.split('&').join('&amp;').split('<').join('&lt;').split('>').join('&gt;').split('\xA0').join('&nbsp;').split('\n').join('<br > \n');
}

var MentionGenerator = function () {
  function MentionGenerator(contentState, options) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, MentionGenerator);

    this.contentState = contentState;
    this.options = options;
  }

  MentionGenerator.prototype.generate = function generate() {
    var contentRaw = Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertToRaw"])(this.contentState);
    return this.processContent(contentRaw);
  };

  MentionGenerator.prototype.processContent = function processContent(contentRaw) {
    var blocks = contentRaw.blocks;
    var encode = this.options.encode;
    return blocks.map(function (block) {
      return encode ? encodeContent(block.text) : block.text;
    }).join(encode ? '<br />\n' : '\n');
  };

  return MentionGenerator;
}();

function exportContent(contentState) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return new MentionGenerator(contentState, options).generate();
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZXhwb3J0Q29udGVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1tZW50aW9uL2VzL3V0aWxzL2V4cG9ydENvbnRlbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IHsgY29udmVydFRvUmF3IH0gZnJvbSAnZHJhZnQtanMnO1xuXG5mdW5jdGlvbiBlbmNvZGVDb250ZW50KHRleHQpIHtcbiAgcmV0dXJuIHRleHQuc3BsaXQoJyYnKS5qb2luKCcmYW1wOycpLnNwbGl0KCc8Jykuam9pbignJmx0OycpLnNwbGl0KCc+Jykuam9pbignJmd0OycpLnNwbGl0KCdcXHhBMCcpLmpvaW4oJyZuYnNwOycpLnNwbGl0KCdcXG4nKS5qb2luKCc8YnIgPiBcXG4nKTtcbn1cblxudmFyIE1lbnRpb25HZW5lcmF0b3IgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIE1lbnRpb25HZW5lcmF0b3IoY29udGVudFN0YXRlLCBvcHRpb25zKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIE1lbnRpb25HZW5lcmF0b3IpO1xuXG4gICAgdGhpcy5jb250ZW50U3RhdGUgPSBjb250ZW50U3RhdGU7XG4gICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgfVxuXG4gIE1lbnRpb25HZW5lcmF0b3IucHJvdG90eXBlLmdlbmVyYXRlID0gZnVuY3Rpb24gZ2VuZXJhdGUoKSB7XG4gICAgdmFyIGNvbnRlbnRSYXcgPSBjb252ZXJ0VG9SYXcodGhpcy5jb250ZW50U3RhdGUpO1xuICAgIHJldHVybiB0aGlzLnByb2Nlc3NDb250ZW50KGNvbnRlbnRSYXcpO1xuICB9O1xuXG4gIE1lbnRpb25HZW5lcmF0b3IucHJvdG90eXBlLnByb2Nlc3NDb250ZW50ID0gZnVuY3Rpb24gcHJvY2Vzc0NvbnRlbnQoY29udGVudFJhdykge1xuICAgIHZhciBibG9ja3MgPSBjb250ZW50UmF3LmJsb2NrcztcbiAgICB2YXIgZW5jb2RlID0gdGhpcy5vcHRpb25zLmVuY29kZTtcblxuICAgIHJldHVybiBibG9ja3MubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGVuY29kZSA/IGVuY29kZUNvbnRlbnQoYmxvY2sudGV4dCkgOiBibG9jay50ZXh0O1xuICAgIH0pLmpvaW4oZW5jb2RlID8gJzxiciAvPlxcbicgOiAnXFxuJyk7XG4gIH07XG5cbiAgcmV0dXJuIE1lbnRpb25HZW5lcmF0b3I7XG59KCk7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGV4cG9ydENvbnRlbnQoY29udGVudFN0YXRlKSB7XG4gIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICByZXR1cm4gbmV3IE1lbnRpb25HZW5lcmF0b3IoY29udGVudFN0YXRlLCBvcHRpb25zKS5nZW5lcmF0ZSgpO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/exportContent.js
