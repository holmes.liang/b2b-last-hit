__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formItemLayout", function() { return formItemLayout; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/claims/handing/details/SAIC/components/settlement/components/style/table */ "./src/app/desk/claims/handing/details/SAIC/components/settlement/components/style/table.tsx");
/* harmony import */ var antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd/lib/table/Column */ "./node_modules/antd/lib/table/Column.js");
/* harmony import */ var antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13__);







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/panyment-arramgement/component/view-arrangement.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .ant-table-thead {\n         text-transform: uppercase;\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var formItemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewArrangement =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(ViewArrangement, _ModelWidget);

  function ViewArrangement(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ViewArrangement);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ViewArrangement).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ViewArrangement, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ViewArrangement.prototype), "initState", this).call(this), {
        editIndex: 0
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {} // render

  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          onCancel = _this$props.onCancel,
          onOK = _this$props.onOK;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Modal"], {
        width: "60%",
        maskClosable: false,
        visible: true,
        centered: true,
        title: _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Payment Plan Details").thai("Payment Plan Details").getMessage(),
        onCancel: onCancel,
        onOk: onOK,
        footer: [_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Button"], {
          key: "cancel",
          onClick: onCancel,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 79
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Cancel").thai("ยกเลิก").getMessage())],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.BoxContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_claims_handing_details_SAIC_components_settlement_components_style_table__WEBPACK_IMPORTED_MODULE_12__["TableStyleForDefaultSize"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, this.renderTable())));
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      var _this = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Table"], {
        dataSource: this.tableData,
        pagination: false,
        bordered: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13___default.a, {
        title: "Installment Seq.",
        key: "seqNo",
        width: 200,
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return dataIndex + 1;
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13___default.a, {
        title: "Amount",
        key: "amount",
        dataIndex: "amount",
        className: "amount-right",
        render: function render(text, record, dataIndex) {
          return _this.renderAmount(text, dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd_lib_table_Column__WEBPACK_IMPORTED_MODULE_13___default.a, {
        title: "Payment Date",
        key: "paymentDate",
        dataIndex: "paymentDate",
        render: function render(text, record, dataIndex) {
          return _this.renderDate(text, dataIndex);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }));
    }
  }, {
    key: "renderAmount",
    value: function renderAmount(text, dataIndex) {
      var oldModel = this.props.oldModel;
      var premCurrency = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(oldModel, this.getProp("premCurrencyCode")) || "SGD";
      var currencySymbol = _common__WEBPACK_IMPORTED_MODULE_8__["Consts"].CURRENCY_SYMBOL_TABLE[premCurrency];
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        style: {
          paddingRight: 4
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134
        },
        __self: this
      }, currencySymbol, " ", this.formmatCurrency(text));
    }
  }, {
    key: "renderDate",
    value: function renderDate(text, dataIndex) {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        style: {
          paddingRight: 4
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, text ? _common__WEBPACK_IMPORTED_MODULE_8__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_8__["DateUtils"].toDate(text)) : "");
    } // actions
    // methods

  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (this.props.dataFixed) {
        return "".concat(this.props.dataFixed, ".").concat(propName);
      }

      return propName;
    }
  }, {
    key: "formmatCurrency",
    value: function formmatCurrency(money) {
      var _money = parseFloat(money || 0).toFixed(2);

      return "".concat(_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].toThousands(_money || 0));
    } // getters

  }, {
    key: "tableData",
    get: function get() {
      return this.getValueFromModel("details") || [];
    }
  }]);

  return ViewArrangement;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ViewArrangement);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9wYW55bWVudC1hcnJhbWdlbWVudC9jb21wb25lbnQvdmlldy1hcnJhbmdlbWVudC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9jb21wb25lbnRzL3BhbnltZW50LWFycmFtZ2VtZW50L2NvbXBvbmVudC92aWV3LWFycmFuZ2VtZW50LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIENvbnN0cywgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgUnVsZXMsIFN0b3JhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0LCBORm9ybUl0ZW0gfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQnV0dG9uLCBDb2wsIEZvcm0sIElucHV0LCBNb2RhbCwgUm93LCBTcGluLCBUYWJsZSwgVXBsb2FkIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IFRhYmxlU3R5bGVGb3JEZWZhdWx0U2l6ZSB9IGZyb20gXCJAZGVzay9jbGFpbXMvaGFuZGluZy9kZXRhaWxzL1NBSUMvY29tcG9uZW50cy9zZXR0bGVtZW50L2NvbXBvbmVudHMvc3R5bGUvdGFibGVcIjtcbmltcG9ydCBDb2x1bW4gZnJvbSBcImFudGQvbGliL3RhYmxlL0NvbHVtblwiO1xuXG5leHBvcnQgY29uc3QgZm9ybUl0ZW1MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG50eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xudHlwZSBJQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcblxudHlwZSBhcnJhbmdlbWVudFN0YXRlID0ge1xuICBlZGl0SW5kZXg6IG51bWJlcjtcbn07XG5cbmV4cG9ydCB0eXBlIGFycmFuZ2VtZW50UHJvcHMgPSB7XG4gIG9uQ2FuY2VsOiBhbnk7XG4gIG9uT0s6IGFueTtcbiAgbW9kZWw6IGFueTtcbiAgb2xkTW9kZWw6IGFueTtcbiAgZGF0YUZpeGVkPzogYW55O1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmNsYXNzIFZpZXdBcnJhbmdlbWVudDxQIGV4dGVuZHMgYXJyYW5nZW1lbnRQcm9wcywgUyBleHRlbmRzIGFycmFuZ2VtZW50U3RhdGUsIEMgZXh0ZW5kcyBJQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBhcnJhbmdlbWVudFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgZWRpdEluZGV4OiAwLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEJveENvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAgIC5hbnQtdGFibGUtdGhlYWQge1xuICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcblxuICB9XG5cbiAgLy8gcmVuZGVyXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgb25DYW5jZWwsIG9uT0sgfSA9IHRoaXMucHJvcHM7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPE1vZGFsXG4gICAgICAgIHdpZHRoPXtcIjYwJVwifVxuICAgICAgICBtYXNrQ2xvc2FibGU9e2ZhbHNlfVxuICAgICAgICB2aXNpYmxlPXt0cnVlfVxuICAgICAgICBjZW50ZXJlZD17dHJ1ZX1cbiAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiUGF5bWVudCBQbGFuIERldGFpbHNcIikudGhhaShcIlBheW1lbnQgUGxhbiBEZXRhaWxzXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICBvbk9rPXtvbk9LfVxuICAgICAgICBmb290ZXI9e1tcbiAgICAgICAgICA8QnV0dG9uIGtleT1cImNhbmNlbFwiIG9uQ2xpY2s9e29uQ2FuY2VsfT5cbiAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkNhbmNlbFwiKS50aGFpKFwi4Lii4LiB4LmA4Lil4Li04LiBXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA8L0J1dHRvbj4sXG4gICAgICAgIF19XG4gICAgICA+XG4gICAgICAgIDxDLkJveENvbnRlbnQ+XG4gICAgICAgICAgPFRhYmxlU3R5bGVGb3JEZWZhdWx0U2l6ZT5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlclRhYmxlKCl9XG4gICAgICAgICAgPC9UYWJsZVN0eWxlRm9yRGVmYXVsdFNpemU+XG4gICAgICAgIDwvQy5Cb3hDb250ZW50PlxuICAgICAgPC9Nb2RhbD5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyVGFibGUoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxUYWJsZSBkYXRhU291cmNlPXt0aGlzLnRhYmxlRGF0YX1cbiAgICAgICAgICAgICBwYWdpbmF0aW9uPXtmYWxzZX1cbiAgICAgICAgICAgICBib3JkZXJlZD17dHJ1ZX1cbiAgICAgID5cbiAgICAgICAgPENvbHVtblxuICAgICAgICAgIHRpdGxlPVwiSW5zdGFsbG1lbnQgU2VxLlwiXG4gICAgICAgICAga2V5PVwic2VxTm9cIlxuICAgICAgICAgIHdpZHRoPXsyMDB9XG4gICAgICAgICAgY2xhc3NOYW1lPVwiYW1vdW50LXJpZ2h0XCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0LCByZWNvcmQ6IGFueSwgZGF0YUluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBkYXRhSW5kZXggKyAxO1xuICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIC8+XG4gICAgICAgIDxDb2x1bW5cbiAgICAgICAgICB0aXRsZT1cIkFtb3VudFwiXG4gICAgICAgICAga2V5PVwiYW1vdW50XCJcbiAgICAgICAgICBkYXRhSW5kZXg9e1wiYW1vdW50XCJ9XG4gICAgICAgICAgY2xhc3NOYW1lPVwiYW1vdW50LXJpZ2h0XCJcbiAgICAgICAgICByZW5kZXI9eyh0ZXh0OiBhbnksIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+XG4gICAgICAgICAgICB0aGlzLnJlbmRlckFtb3VudCh0ZXh0LCBkYXRhSW5kZXgpXG4gICAgICAgICAgfVxuICAgICAgICAvPlxuICAgICAgICA8Q29sdW1uXG4gICAgICAgICAgdGl0bGU9XCJQYXltZW50IERhdGVcIlxuICAgICAgICAgIGtleT1cInBheW1lbnREYXRlXCJcbiAgICAgICAgICBkYXRhSW5kZXg9e1wicGF5bWVudERhdGVcIn1cbiAgICAgICAgICByZW5kZXI9eyh0ZXh0OiBhbnksIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+XG4gICAgICAgICAgICB0aGlzLnJlbmRlckRhdGUodGV4dCwgZGF0YUluZGV4KVxuICAgICAgICAgIH1cbiAgICAgICAgLz5cbiAgICAgIDwvVGFibGU+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlckFtb3VudCh0ZXh0OiBhbnksIGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgY29uc3QgeyBvbGRNb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwcmVtQ3VycmVuY3kgPSBfLmdldChvbGRNb2RlbCwgdGhpcy5nZXRQcm9wKFwicHJlbUN1cnJlbmN5Q29kZVwiKSkgfHwgXCJTR0RcIjtcbiAgICBjb25zdCBjdXJyZW5jeVN5bWJvbCA9IENvbnN0cy5DVVJSRU5DWV9TWU1CT0xfVEFCTEVbcHJlbUN1cnJlbmN5XTtcbiAgICByZXR1cm4gPGRpdlxuICAgICAgc3R5bGU9e3sgcGFkZGluZ1JpZ2h0OiA0IH19PntjdXJyZW5jeVN5bWJvbH0ge3RoaXMuZm9ybW1hdEN1cnJlbmN5KHRleHQpfTwvZGl2PjtcblxuICB9XG5cbiAgcmVuZGVyRGF0ZSh0ZXh0OiBhbnksIGRhdGFJbmRleDogbnVtYmVyKSB7XG4gICAgcmV0dXJuIDxkaXZcbiAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogNCB9fT57dGV4dCA/IERhdGVVdGlscy5mb3JtYXREYXRlKERhdGVVdGlscy50b0RhdGUodGV4dCkpIDogXCJcIn1cbiAgICA8L2Rpdj47XG4gIH1cblxuICAvLyBhY3Rpb25zXG5cbiAgLy8gbWV0aG9kc1xuICBnZXRQcm9wKHByb3BOYW1lOiBhbnkpIHtcbiAgICBpZiAodGhpcy5wcm9wcy5kYXRhRml4ZWQpIHtcbiAgICAgIHJldHVybiBgJHt0aGlzLnByb3BzLmRhdGFGaXhlZH0uJHtwcm9wTmFtZX1gO1xuICAgIH1cbiAgICByZXR1cm4gcHJvcE5hbWU7XG4gIH1cblxuICBmb3JtbWF0Q3VycmVuY3kobW9uZXk6IGFueSkge1xuICAgIGNvbnN0IF9tb25leSA9IHBhcnNlRmxvYXQobW9uZXkgfHwgMCkudG9GaXhlZCgyKTtcbiAgICByZXR1cm4gYCR7VXRpbHMudG9UaG91c2FuZHMoX21vbmV5IHx8IDApfWA7XG4gIH1cblxuICAvLyBnZXR0ZXJzXG4gIGdldCB0YWJsZURhdGEoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJkZXRhaWxzXCIpIHx8IFtdO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFZpZXdBcnJhbmdlbWVudDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBQ0E7QUEwQkE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQU9BOzs7QUFFQTtBQUNBOzs7QUFJQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBSUE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7O0FBNUhBO0FBQ0E7QUE4SEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/panyment-arramgement/component/view-arrangement.tsx
