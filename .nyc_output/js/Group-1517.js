var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var Element = __webpack_require__(/*! ../Element */ "./node_modules/zrender/lib/Element.js");

var BoundingRect = __webpack_require__(/*! ../core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");
/**
 * Group是一个容器，可以插入子节点，Group的变换也会被应用到子节点上
 * @module zrender/graphic/Group
 * @example
 *     var Group = require('zrender/container/Group');
 *     var Circle = require('zrender/graphic/shape/Circle');
 *     var g = new Group();
 *     g.position[0] = 100;
 *     g.position[1] = 100;
 *     g.add(new Circle({
 *         style: {
 *             x: 100,
 *             y: 100,
 *             r: 20,
 *         }
 *     }));
 *     zr.add(g);
 */

/**
 * @alias module:zrender/graphic/Group
 * @constructor
 * @extends module:zrender/mixin/Transformable
 * @extends module:zrender/mixin/Eventful
 */


var Group = function Group(opts) {
  opts = opts || {};
  Element.call(this, opts);

  for (var key in opts) {
    if (opts.hasOwnProperty(key)) {
      this[key] = opts[key];
    }
  }

  this._children = [];
  this.__storage = null;
  this.__dirty = true;
};

Group.prototype = {
  constructor: Group,
  isGroup: true,

  /**
   * @type {string}
   */
  type: 'group',

  /**
   * 所有子孙元素是否响应鼠标事件
   * @name module:/zrender/container/Group#silent
   * @type {boolean}
   * @default false
   */
  silent: false,

  /**
   * @return {Array.<module:zrender/Element>}
   */
  children: function children() {
    return this._children.slice();
  },

  /**
   * 获取指定 index 的儿子节点
   * @param  {number} idx
   * @return {module:zrender/Element}
   */
  childAt: function childAt(idx) {
    return this._children[idx];
  },

  /**
   * 获取指定名字的儿子节点
   * @param  {string} name
   * @return {module:zrender/Element}
   */
  childOfName: function childOfName(name) {
    var children = this._children;

    for (var i = 0; i < children.length; i++) {
      if (children[i].name === name) {
        return children[i];
      }
    }
  },

  /**
   * @return {number}
   */
  childCount: function childCount() {
    return this._children.length;
  },

  /**
   * 添加子节点到最后
   * @param {module:zrender/Element} child
   */
  add: function add(child) {
    if (child && child !== this && child.parent !== this) {
      this._children.push(child);

      this._doAdd(child);
    }

    return this;
  },

  /**
   * 添加子节点在 nextSibling 之前
   * @param {module:zrender/Element} child
   * @param {module:zrender/Element} nextSibling
   */
  addBefore: function addBefore(child, nextSibling) {
    if (child && child !== this && child.parent !== this && nextSibling && nextSibling.parent === this) {
      var children = this._children;
      var idx = children.indexOf(nextSibling);

      if (idx >= 0) {
        children.splice(idx, 0, child);

        this._doAdd(child);
      }
    }

    return this;
  },
  _doAdd: function _doAdd(child) {
    if (child.parent) {
      child.parent.remove(child);
    }

    child.parent = this;
    var storage = this.__storage;
    var zr = this.__zr;

    if (storage && storage !== child.__storage) {
      storage.addToStorage(child);

      if (child instanceof Group) {
        child.addChildrenToStorage(storage);
      }
    }

    zr && zr.refresh();
  },

  /**
   * 移除子节点
   * @param {module:zrender/Element} child
   */
  remove: function remove(child) {
    var zr = this.__zr;
    var storage = this.__storage;
    var children = this._children;
    var idx = zrUtil.indexOf(children, child);

    if (idx < 0) {
      return this;
    }

    children.splice(idx, 1);
    child.parent = null;

    if (storage) {
      storage.delFromStorage(child);

      if (child instanceof Group) {
        child.delChildrenFromStorage(storage);
      }
    }

    zr && zr.refresh();
    return this;
  },

  /**
   * 移除所有子节点
   */
  removeAll: function removeAll() {
    var children = this._children;
    var storage = this.__storage;
    var child;
    var i;

    for (i = 0; i < children.length; i++) {
      child = children[i];

      if (storage) {
        storage.delFromStorage(child);

        if (child instanceof Group) {
          child.delChildrenFromStorage(storage);
        }
      }

      child.parent = null;
    }

    children.length = 0;
    return this;
  },

  /**
   * 遍历所有子节点
   * @param  {Function} cb
   * @param  {}   context
   */
  eachChild: function eachChild(cb, context) {
    var children = this._children;

    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      cb.call(context, child, i);
    }

    return this;
  },

  /**
   * 深度优先遍历所有子孙节点
   * @param  {Function} cb
   * @param  {}   context
   */
  traverse: function traverse(cb, context) {
    for (var i = 0; i < this._children.length; i++) {
      var child = this._children[i];
      cb.call(context, child);

      if (child.type === 'group') {
        child.traverse(cb, context);
      }
    }

    return this;
  },
  addChildrenToStorage: function addChildrenToStorage(storage) {
    for (var i = 0; i < this._children.length; i++) {
      var child = this._children[i];
      storage.addToStorage(child);

      if (child instanceof Group) {
        child.addChildrenToStorage(storage);
      }
    }
  },
  delChildrenFromStorage: function delChildrenFromStorage(storage) {
    for (var i = 0; i < this._children.length; i++) {
      var child = this._children[i];
      storage.delFromStorage(child);

      if (child instanceof Group) {
        child.delChildrenFromStorage(storage);
      }
    }
  },
  dirty: function dirty() {
    this.__dirty = true;
    this.__zr && this.__zr.refresh();
    return this;
  },

  /**
   * @return {module:zrender/core/BoundingRect}
   */
  getBoundingRect: function getBoundingRect(includeChildren) {
    // TODO Caching
    var rect = null;
    var tmpRect = new BoundingRect(0, 0, 0, 0);
    var children = includeChildren || this._children;
    var tmpMat = [];

    for (var i = 0; i < children.length; i++) {
      var child = children[i];

      if (child.ignore || child.invisible) {
        continue;
      }

      var childRect = child.getBoundingRect();
      var transform = child.getLocalTransform(tmpMat); // TODO
      // The boundingRect cacluated by transforming original
      // rect may be bigger than the actual bundingRect when rotation
      // is used. (Consider a circle rotated aginst its center, where
      // the actual boundingRect should be the same as that not be
      // rotated.) But we can not find better approach to calculate
      // actual boundingRect yet, considering performance.

      if (transform) {
        tmpRect.copy(childRect);
        tmpRect.applyTransform(transform);
        rect = rect || tmpRect.clone();
        rect.union(tmpRect);
      } else {
        rect = rect || childRect.clone();
        rect.union(childRect);
      }
    }

    return rect || tmpRect;
  }
};
zrUtil.inherits(Group, Element);
var _default = Group;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbmVyL0dyb3VwLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbmVyL0dyb3VwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciB6clV0aWwgPSByZXF1aXJlKFwiLi4vY29yZS91dGlsXCIpO1xuXG52YXIgRWxlbWVudCA9IHJlcXVpcmUoXCIuLi9FbGVtZW50XCIpO1xuXG52YXIgQm91bmRpbmdSZWN0ID0gcmVxdWlyZShcIi4uL2NvcmUvQm91bmRpbmdSZWN0XCIpO1xuXG4vKipcbiAqIEdyb3Vw5piv5LiA5Liq5a655Zmo77yM5Y+v5Lul5o+S5YWl5a2Q6IqC54K577yMR3JvdXDnmoTlj5jmjaLkuZ/kvJrooqvlupTnlKjliLDlrZDoioLngrnkuIpcbiAqIEBtb2R1bGUgenJlbmRlci9ncmFwaGljL0dyb3VwXG4gKiBAZXhhbXBsZVxuICogICAgIHZhciBHcm91cCA9IHJlcXVpcmUoJ3pyZW5kZXIvY29udGFpbmVyL0dyb3VwJyk7XG4gKiAgICAgdmFyIENpcmNsZSA9IHJlcXVpcmUoJ3pyZW5kZXIvZ3JhcGhpYy9zaGFwZS9DaXJjbGUnKTtcbiAqICAgICB2YXIgZyA9IG5ldyBHcm91cCgpO1xuICogICAgIGcucG9zaXRpb25bMF0gPSAxMDA7XG4gKiAgICAgZy5wb3NpdGlvblsxXSA9IDEwMDtcbiAqICAgICBnLmFkZChuZXcgQ2lyY2xlKHtcbiAqICAgICAgICAgc3R5bGU6IHtcbiAqICAgICAgICAgICAgIHg6IDEwMCxcbiAqICAgICAgICAgICAgIHk6IDEwMCxcbiAqICAgICAgICAgICAgIHI6IDIwLFxuICogICAgICAgICB9XG4gKiAgICAgfSkpO1xuICogICAgIHpyLmFkZChnKTtcbiAqL1xuXG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6enJlbmRlci9ncmFwaGljL0dyb3VwXG4gKiBAY29uc3RydWN0b3JcbiAqIEBleHRlbmRzIG1vZHVsZTp6cmVuZGVyL21peGluL1RyYW5zZm9ybWFibGVcbiAqIEBleHRlbmRzIG1vZHVsZTp6cmVuZGVyL21peGluL0V2ZW50ZnVsXG4gKi9cbnZhciBHcm91cCA9IGZ1bmN0aW9uIChvcHRzKSB7XG4gIG9wdHMgPSBvcHRzIHx8IHt9O1xuICBFbGVtZW50LmNhbGwodGhpcywgb3B0cyk7XG5cbiAgZm9yICh2YXIga2V5IGluIG9wdHMpIHtcbiAgICBpZiAob3B0cy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICB0aGlzW2tleV0gPSBvcHRzW2tleV07XG4gICAgfVxuICB9XG5cbiAgdGhpcy5fY2hpbGRyZW4gPSBbXTtcbiAgdGhpcy5fX3N0b3JhZ2UgPSBudWxsO1xuICB0aGlzLl9fZGlydHkgPSB0cnVlO1xufTtcblxuR3JvdXAucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogR3JvdXAsXG4gIGlzR3JvdXA6IHRydWUsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICB0eXBlOiAnZ3JvdXAnLFxuXG4gIC8qKlxuICAgKiDmiYDmnInlrZDlrZnlhYPntKDmmK/lkKblk43lupTpvKDmoIfkuovku7ZcbiAgICogQG5hbWUgbW9kdWxlOi96cmVuZGVyL2NvbnRhaW5lci9Hcm91cCNzaWxlbnRcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBzaWxlbnQ6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtBcnJheS48bW9kdWxlOnpyZW5kZXIvRWxlbWVudD59XG4gICAqL1xuICBjaGlsZHJlbjogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9jaGlsZHJlbi5zbGljZSgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiDojrflj5bmjIflrpogaW5kZXgg55qE5YS/5a2Q6IqC54K5XG4gICAqIEBwYXJhbSAge251bWJlcn0gaWR4XG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9XG4gICAqL1xuICBjaGlsZEF0OiBmdW5jdGlvbiAoaWR4KSB7XG4gICAgcmV0dXJuIHRoaXMuX2NoaWxkcmVuW2lkeF07XG4gIH0sXG5cbiAgLyoqXG4gICAqIOiOt+WPluaMh+WumuWQjeWtl+eahOWEv+WtkOiKgueCuVxuICAgKiBAcGFyYW0gIHtzdHJpbmd9IG5hbWVcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvRWxlbWVudH1cbiAgICovXG4gIGNoaWxkT2ZOYW1lOiBmdW5jdGlvbiAobmFtZSkge1xuICAgIHZhciBjaGlsZHJlbiA9IHRoaXMuX2NoaWxkcmVuO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgaWYgKGNoaWxkcmVuW2ldLm5hbWUgPT09IG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkcmVuW2ldO1xuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7bnVtYmVyfVxuICAgKi9cbiAgY2hpbGRDb3VudDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9jaGlsZHJlbi5sZW5ndGg7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOa3u+WKoOWtkOiKgueCueWIsOacgOWQjlxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGNoaWxkXG4gICAqL1xuICBhZGQ6IGZ1bmN0aW9uIChjaGlsZCkge1xuICAgIGlmIChjaGlsZCAmJiBjaGlsZCAhPT0gdGhpcyAmJiBjaGlsZC5wYXJlbnQgIT09IHRoaXMpIHtcbiAgICAgIHRoaXMuX2NoaWxkcmVuLnB1c2goY2hpbGQpO1xuXG4gICAgICB0aGlzLl9kb0FkZChjaGlsZCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOa3u+WKoOWtkOiKgueCueWcqCBuZXh0U2libGluZyDkuYvliY1cbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBjaGlsZFxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IG5leHRTaWJsaW5nXG4gICAqL1xuICBhZGRCZWZvcmU6IGZ1bmN0aW9uIChjaGlsZCwgbmV4dFNpYmxpbmcpIHtcbiAgICBpZiAoY2hpbGQgJiYgY2hpbGQgIT09IHRoaXMgJiYgY2hpbGQucGFyZW50ICE9PSB0aGlzICYmIG5leHRTaWJsaW5nICYmIG5leHRTaWJsaW5nLnBhcmVudCA9PT0gdGhpcykge1xuICAgICAgdmFyIGNoaWxkcmVuID0gdGhpcy5fY2hpbGRyZW47XG4gICAgICB2YXIgaWR4ID0gY2hpbGRyZW4uaW5kZXhPZihuZXh0U2libGluZyk7XG5cbiAgICAgIGlmIChpZHggPj0gMCkge1xuICAgICAgICBjaGlsZHJlbi5zcGxpY2UoaWR4LCAwLCBjaGlsZCk7XG5cbiAgICAgICAgdGhpcy5fZG9BZGQoY2hpbGQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0aGlzO1xuICB9LFxuICBfZG9BZGQ6IGZ1bmN0aW9uIChjaGlsZCkge1xuICAgIGlmIChjaGlsZC5wYXJlbnQpIHtcbiAgICAgIGNoaWxkLnBhcmVudC5yZW1vdmUoY2hpbGQpO1xuICAgIH1cblxuICAgIGNoaWxkLnBhcmVudCA9IHRoaXM7XG4gICAgdmFyIHN0b3JhZ2UgPSB0aGlzLl9fc3RvcmFnZTtcbiAgICB2YXIgenIgPSB0aGlzLl9fenI7XG5cbiAgICBpZiAoc3RvcmFnZSAmJiBzdG9yYWdlICE9PSBjaGlsZC5fX3N0b3JhZ2UpIHtcbiAgICAgIHN0b3JhZ2UuYWRkVG9TdG9yYWdlKGNoaWxkKTtcblxuICAgICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgR3JvdXApIHtcbiAgICAgICAgY2hpbGQuYWRkQ2hpbGRyZW5Ub1N0b3JhZ2Uoc3RvcmFnZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgenIgJiYgenIucmVmcmVzaCgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiDnp7vpmaTlrZDoioLngrlcbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBjaGlsZFxuICAgKi9cbiAgcmVtb3ZlOiBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICB2YXIgenIgPSB0aGlzLl9fenI7XG4gICAgdmFyIHN0b3JhZ2UgPSB0aGlzLl9fc3RvcmFnZTtcbiAgICB2YXIgY2hpbGRyZW4gPSB0aGlzLl9jaGlsZHJlbjtcbiAgICB2YXIgaWR4ID0genJVdGlsLmluZGV4T2YoY2hpbGRyZW4sIGNoaWxkKTtcblxuICAgIGlmIChpZHggPCAwKSB7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBjaGlsZHJlbi5zcGxpY2UoaWR4LCAxKTtcbiAgICBjaGlsZC5wYXJlbnQgPSBudWxsO1xuXG4gICAgaWYgKHN0b3JhZ2UpIHtcbiAgICAgIHN0b3JhZ2UuZGVsRnJvbVN0b3JhZ2UoY2hpbGQpO1xuXG4gICAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBHcm91cCkge1xuICAgICAgICBjaGlsZC5kZWxDaGlsZHJlbkZyb21TdG9yYWdlKHN0b3JhZ2UpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHpyICYmIHpyLnJlZnJlc2goKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICog56e76Zmk5omA5pyJ5a2Q6IqC54K5XG4gICAqL1xuICByZW1vdmVBbGw6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgY2hpbGRyZW4gPSB0aGlzLl9jaGlsZHJlbjtcbiAgICB2YXIgc3RvcmFnZSA9IHRoaXMuX19zdG9yYWdlO1xuICAgIHZhciBjaGlsZDtcbiAgICB2YXIgaTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgY2hpbGQgPSBjaGlsZHJlbltpXTtcblxuICAgICAgaWYgKHN0b3JhZ2UpIHtcbiAgICAgICAgc3RvcmFnZS5kZWxGcm9tU3RvcmFnZShjaGlsZCk7XG5cbiAgICAgICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgR3JvdXApIHtcbiAgICAgICAgICBjaGlsZC5kZWxDaGlsZHJlbkZyb21TdG9yYWdlKHN0b3JhZ2UpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGNoaWxkLnBhcmVudCA9IG51bGw7XG4gICAgfVxuXG4gICAgY2hpbGRyZW4ubGVuZ3RoID0gMDtcbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICog6YGN5Y6G5omA5pyJ5a2Q6IqC54K5XG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBjYlxuICAgKiBAcGFyYW0gIHt9ICAgY29udGV4dFxuICAgKi9cbiAgZWFjaENoaWxkOiBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHtcbiAgICB2YXIgY2hpbGRyZW4gPSB0aGlzLl9jaGlsZHJlbjtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBjaGlsZCA9IGNoaWxkcmVuW2ldO1xuICAgICAgY2IuY2FsbChjb250ZXh0LCBjaGlsZCwgaSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOa3seW6puS8mOWFiOmBjeWOhuaJgOacieWtkOWtmeiKgueCuVxuICAgKiBAcGFyYW0gIHtGdW5jdGlvbn0gY2JcbiAgICogQHBhcmFtICB7fSAgIGNvbnRleHRcbiAgICovXG4gIHRyYXZlcnNlOiBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuX2NoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgY2hpbGQgPSB0aGlzLl9jaGlsZHJlbltpXTtcbiAgICAgIGNiLmNhbGwoY29udGV4dCwgY2hpbGQpO1xuXG4gICAgICBpZiAoY2hpbGQudHlwZSA9PT0gJ2dyb3VwJykge1xuICAgICAgICBjaGlsZC50cmF2ZXJzZShjYiwgY29udGV4dCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG4gIGFkZENoaWxkcmVuVG9TdG9yYWdlOiBmdW5jdGlvbiAoc3RvcmFnZSkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBjaGlsZCA9IHRoaXMuX2NoaWxkcmVuW2ldO1xuICAgICAgc3RvcmFnZS5hZGRUb1N0b3JhZ2UoY2hpbGQpO1xuXG4gICAgICBpZiAoY2hpbGQgaW5zdGFuY2VvZiBHcm91cCkge1xuICAgICAgICBjaGlsZC5hZGRDaGlsZHJlblRvU3RvcmFnZShzdG9yYWdlKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIGRlbENoaWxkcmVuRnJvbVN0b3JhZ2U6IGZ1bmN0aW9uIChzdG9yYWdlKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl9jaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGNoaWxkID0gdGhpcy5fY2hpbGRyZW5baV07XG4gICAgICBzdG9yYWdlLmRlbEZyb21TdG9yYWdlKGNoaWxkKTtcblxuICAgICAgaWYgKGNoaWxkIGluc3RhbmNlb2YgR3JvdXApIHtcbiAgICAgICAgY2hpbGQuZGVsQ2hpbGRyZW5Gcm9tU3RvcmFnZShzdG9yYWdlKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIGRpcnR5OiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5fX2RpcnR5ID0gdHJ1ZTtcbiAgICB0aGlzLl9fenIgJiYgdGhpcy5fX3pyLnJlZnJlc2goKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9Cb3VuZGluZ1JlY3R9XG4gICAqL1xuICBnZXRCb3VuZGluZ1JlY3Q6IGZ1bmN0aW9uIChpbmNsdWRlQ2hpbGRyZW4pIHtcbiAgICAvLyBUT0RPIENhY2hpbmdcbiAgICB2YXIgcmVjdCA9IG51bGw7XG4gICAgdmFyIHRtcFJlY3QgPSBuZXcgQm91bmRpbmdSZWN0KDAsIDAsIDAsIDApO1xuICAgIHZhciBjaGlsZHJlbiA9IGluY2x1ZGVDaGlsZHJlbiB8fCB0aGlzLl9jaGlsZHJlbjtcbiAgICB2YXIgdG1wTWF0ID0gW107XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgY2hpbGQgPSBjaGlsZHJlbltpXTtcblxuICAgICAgaWYgKGNoaWxkLmlnbm9yZSB8fCBjaGlsZC5pbnZpc2libGUpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBjaGlsZFJlY3QgPSBjaGlsZC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICAgIHZhciB0cmFuc2Zvcm0gPSBjaGlsZC5nZXRMb2NhbFRyYW5zZm9ybSh0bXBNYXQpOyAvLyBUT0RPXG4gICAgICAvLyBUaGUgYm91bmRpbmdSZWN0IGNhY2x1YXRlZCBieSB0cmFuc2Zvcm1pbmcgb3JpZ2luYWxcbiAgICAgIC8vIHJlY3QgbWF5IGJlIGJpZ2dlciB0aGFuIHRoZSBhY3R1YWwgYnVuZGluZ1JlY3Qgd2hlbiByb3RhdGlvblxuICAgICAgLy8gaXMgdXNlZC4gKENvbnNpZGVyIGEgY2lyY2xlIHJvdGF0ZWQgYWdpbnN0IGl0cyBjZW50ZXIsIHdoZXJlXG4gICAgICAvLyB0aGUgYWN0dWFsIGJvdW5kaW5nUmVjdCBzaG91bGQgYmUgdGhlIHNhbWUgYXMgdGhhdCBub3QgYmVcbiAgICAgIC8vIHJvdGF0ZWQuKSBCdXQgd2UgY2FuIG5vdCBmaW5kIGJldHRlciBhcHByb2FjaCB0byBjYWxjdWxhdGVcbiAgICAgIC8vIGFjdHVhbCBib3VuZGluZ1JlY3QgeWV0LCBjb25zaWRlcmluZyBwZXJmb3JtYW5jZS5cblxuICAgICAgaWYgKHRyYW5zZm9ybSkge1xuICAgICAgICB0bXBSZWN0LmNvcHkoY2hpbGRSZWN0KTtcbiAgICAgICAgdG1wUmVjdC5hcHBseVRyYW5zZm9ybSh0cmFuc2Zvcm0pO1xuICAgICAgICByZWN0ID0gcmVjdCB8fCB0bXBSZWN0LmNsb25lKCk7XG4gICAgICAgIHJlY3QudW5pb24odG1wUmVjdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZWN0ID0gcmVjdCB8fCBjaGlsZFJlY3QuY2xvbmUoKTtcbiAgICAgICAgcmVjdC51bmlvbihjaGlsZFJlY3QpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZWN0IHx8IHRtcFJlY3Q7XG4gIH1cbn07XG56clV0aWwuaW5oZXJpdHMoR3JvdXAsIEVsZW1lbnQpO1xudmFyIF9kZWZhdWx0ID0gR3JvdXA7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJRQTtBQXVRQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/container/Group.js
