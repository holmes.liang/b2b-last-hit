

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var createClass = __webpack_require__(/*! create-react-class */ "./node_modules/create-react-class/index.js");

var QuillMixin = __webpack_require__(/*! ./mixin */ "./node_modules/react-quill/lib/mixin.js");

var find = __webpack_require__(/*! lodash/find */ "./node_modules/lodash/find.js");

var some = __webpack_require__(/*! lodash/some */ "./node_modules/lodash/some.js");

var isEqual = __webpack_require__(/*! lodash/isEqual */ "./node_modules/lodash/isEqual.js");

var T = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var DOM = __webpack_require__(/*! react-dom-factories */ "./node_modules/react-dom-factories/index.js");

var QuillComponent = createClass({
  displayName: 'Quill',
  mixins: [QuillMixin],
  propTypes: {
    id: T.string,
    className: T.string,
    theme: T.string,
    style: T.object,
    readOnly: T.bool,
    value: T.oneOfType([T.string, T.shape({
      ops: T.array
    })]),
    defaultValue: T.oneOfType([T.string, T.shape({
      ops: T.array
    })]),
    placeholder: T.string,
    tabIndex: T.number,
    bounds: T.oneOfType([T.string, T.element]),
    onChange: T.func,
    onChangeSelection: T.func,
    onFocus: T.func,
    onBlur: T.func,
    onKeyPress: T.func,
    onKeyDown: T.func,
    onKeyUp: T.func,
    preserveWhitespace: T.bool,
    modules: function modules(props) {
      var isNotObject = T.object.apply(this, arguments);
      if (isNotObject) return isNotObject;
      if (props.modules && props.modules.toolbar && props.modules.toolbar[0] && props.modules.toolbar[0].type) return new Error('Since v1.0.0, React Quill will not create a custom toolbar for you ' + 'anymore. Create a toolbar explictly, or let Quill create one. ' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v100');
    },
    toolbar: function toolbar(props) {
      if ('toolbar' in props) return new Error('The `toolbar` prop has been deprecated. Use `modules.toolbar` instead. ' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v100');
    },
    formats: function formats(props) {
      var isNotArrayOfString = T.arrayOf(T.string).apply(this, arguments);
      if (isNotArrayOfString) return new Error('You cannot specify custom `formats` anymore. Use Parchment instead.  ' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v100.');
    },
    styles: function styles(props) {
      if ('styles' in props) return new Error('The `styles` prop has been deprecated. Use custom stylesheets instead. ' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v100.');
    },
    pollInterval: function pollInterval(props) {
      if ('pollInterval' in props) return new Error('The `pollInterval` property does not have any effect anymore. ' + 'You can safely remove it from your props.' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v100.');
    },
    children: function children(props) {
      // Validate that the editor has only one child element and it is not a <textarea>
      var isNotASingleElement = T.element.apply(this, arguments);
      if (isNotASingleElement) return new Error('The Quill editing area can only be composed of a single React element.');

      if (React.Children.count(props.children)) {
        var child = React.Children.only(props.children);
        if (child.type === 'textarea') return new Error('Quill does not support editing on a <textarea>. Use a <div> instead.');
      }
    }
  },

  /*
  Changing one of these props should cause a full re-render.
  */
  dirtyProps: ['modules', 'formats', 'bounds', 'theme', 'children'],

  /*
  Changing one of these props should cause a regular update.
  */
  cleanProps: ['id', 'className', 'style', 'placeholder', 'tabIndex', 'onChange', 'onChangeSelection', 'onFocus', 'onBlur', 'onKeyPress', 'onKeyDown', 'onKeyUp'],
  getDefaultProps: function getDefaultProps() {
    return {
      theme: 'snow',
      modules: {}
    };
  },

  /*
  We consider the component to be controlled if `value` is being sent in props.
  */
  isControlled: function isControlled() {
    return 'value' in this.props;
  },
  getInitialState: function getInitialState() {
    return {
      generation: 0,
      value: this.isControlled() ? this.props.value : this.props.defaultValue
    };
  },
  componentWillReceiveProps: function componentWillReceiveProps(nextProps, nextState) {
    var editor = this.editor; // If the component is unmounted and mounted too quickly
    // an error is thrown in setEditorContents since editor is
    // still undefined. Must check if editor is undefined
    // before performing this call.

    if (!editor) return; // Update only if we've been passed a new `value`.
    // This leaves components using `defaultValue` alone.

    if ('value' in nextProps) {
      var currentContents = this.getEditorContents();
      var nextContents = nextProps.value;
      if (nextContents === this.lastDeltaChangeSet) throw new Error('You are passing the `delta` object from the `onChange` event back ' + 'as `value`. You most probably want `editor.getContents()` instead. ' + 'See: https://github.com/zenoamaro/react-quill#using-deltas'); // NOTE: Seeing that Quill is missing a way to prevent
      //       edits, we have to settle for a hybrid between
      //       controlled and uncontrolled mode. We can't prevent
      //       the change, but we'll still override content
      //       whenever `value` differs from current state.
      // NOTE: Comparing an HTML string and a Quill Delta will always trigger 
      //       a change, regardless of whether they represent the same document.

      if (!this.isEqualValue(nextContents, currentContents)) {
        this.setEditorContents(editor, nextContents);
      }
    } // We can update readOnly state in-place.


    if ('readOnly' in nextProps) {
      if (nextProps.readOnly !== this.props.readOnly) {
        this.setEditorReadOnly(editor, nextProps.readOnly);
      }
    } // If we need to regenerate the component, we can avoid a detailed
    // in-place update step, and just let everything rerender.


    if (this.shouldComponentRegenerate(nextProps, nextState)) {
      return this.regenerate();
    }
  },
  componentDidMount: function componentDidMount() {
    this.editor = this.createEditor(this.getEditingArea(), this.getEditorConfig()); // Restore editor from Quill's native formats in regeneration scenario

    if (this.quillDelta) {
      this.editor.setContents(this.quillDelta);
      this.editor.setSelection(this.quillSelection);
      this.editor.focus();
      this.quillDelta = this.quillSelection = null;
      return;
    }

    if (this.state.value) {
      this.setEditorContents(this.editor, this.state.value);
      return;
    }
  },
  componentWillUnmount: function componentWillUnmount() {
    var editor;

    if (editor = this.getEditor()) {
      this.unhookEditor(editor);
      this.editor = null;
    }
  },
  shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
    var self = this; // If the component has been regenerated, we already know we should update.

    if (this.state.generation !== nextState.generation) {
      return true;
    } // Compare props that require React updating the DOM.


    return some(this.cleanProps, function (prop) {
      // Note that `isEqual` compares deeply, making it safe to perform
      // non-immutable updates, at the cost of performance.
      return !isEqual(nextProps[prop], self.props[prop]);
    });
  },
  shouldComponentRegenerate: function shouldComponentRegenerate(nextProps, nextState) {
    var self = this; // Whenever a `dirtyProp` changes, the editor needs reinstantiation.

    return some(this.dirtyProps, function (prop) {
      // Note that `isEqual` compares deeply, making it safe to perform
      // non-immutable updates, at the cost of performance.
      return !isEqual(nextProps[prop], self.props[prop]);
    });
  },

  /*
  If we could not update settings from the new props in-place, we have to tear
  down everything and re-render from scratch.
  */
  componentWillUpdate: function componentWillUpdate(nextProps, nextState) {
    if (this.state.generation !== nextState.generation) {
      this.componentWillUnmount();
    }
  },
  componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
    if (this.state.generation !== prevState.generation) {
      this.componentDidMount();
    }
  },
  getEditorConfig: function getEditorConfig() {
    return {
      bounds: this.props.bounds,
      formats: this.props.formats,
      modules: this.props.modules,
      placeholder: this.props.placeholder,
      readOnly: this.props.readOnly,
      scrollingContainer: this.props.scrollingContainer,
      tabIndex: this.props.tabIndex,
      theme: this.props.theme
    };
  },
  getEditor: function getEditor() {
    return this.editor;
  },
  getEditingArea: function getEditingArea() {
    return ReactDOM.findDOMNode(this.editingArea);
  },
  getEditorContents: function getEditorContents() {
    return this.state.value;
  },
  getEditorSelection: function getEditorSelection() {
    return this.state.selection;
  },

  /*
  True if the value is a Delta instance or a Delta look-alike.
  */
  isDelta: function isDelta(value) {
    return value && value.ops;
  },

  /*
  Special comparison function that knows how to compare Deltas.
  */
  isEqualValue: function isEqualValue(value, nextValue) {
    if (this.isDelta(value) && this.isDelta(nextValue)) {
      return isEqual(value.ops, nextValue.ops);
    } else {
      return isEqual(value, nextValue);
    }
  },

  /*
  Regenerating the editor will cause the whole tree, including the container,
  to be cleaned up and re-rendered from scratch.
  */
  regenerate: function regenerate() {
    // Cache selection and contents in Quill's native format to be restored later
    this.quillDelta = this.editor.getContents();
    this.quillSelection = this.editor.getSelection();
    this.setState({
      generation: this.state.generation + 1
    });
  },

  /*
  Renders an editor area, unless it has been provided one to clone.
  */
  renderEditingArea: function renderEditingArea() {
    var self = this;
    var children = this.props.children;
    var preserveWhitespace = this.props.preserveWhitespace;
    var properties = {
      key: this.state.generation,
      tabIndex: this.props.tabIndex,
      ref: function ref(element) {
        self.editingArea = element;
      }
    };
    var customElement = React.Children.count(children) ? React.Children.only(children) : null;
    var defaultElement = preserveWhitespace ? DOM.pre : DOM.div;
    var editingArea = customElement ? React.cloneElement(customElement, properties) : defaultElement(properties);
    return editingArea;
  },
  render: function render() {
    return DOM.div({
      id: this.props.id,
      style: this.props.style,
      key: this.state.generation,
      className: ['quill'].concat(this.props.className).join(' '),
      onKeyPress: this.props.onKeyPress,
      onKeyDown: this.props.onKeyDown,
      onKeyUp: this.props.onKeyUp
    }, this.renderEditingArea());
  },
  onEditorChangeText: function onEditorChangeText(value, delta, source, editor) {
    var currentContents = this.getEditorContents(); // We keep storing the same type of value as what the user gives us,
    // so that value comparisons will be more stable and predictable.

    var nextContents = this.isDelta(currentContents) ? editor.getContents() : editor.getHTML();

    if (!this.isEqualValue(nextContents, currentContents)) {
      // Taint this `delta` object, so we can recognize whether the user
      // is trying to send it back as `value`, preventing a likely loop.
      this.lastDeltaChangeSet = delta;
      this.setState({
        value: nextContents
      });

      if (this.props.onChange) {
        this.props.onChange(value, delta, source, editor);
      }
    }
  },
  onEditorChangeSelection: function onEditorChangeSelection(nextSelection, source, editor) {
    var currentSelection = this.getEditorSelection();
    var hasGainedFocus = !currentSelection && nextSelection;
    var hasLostFocus = currentSelection && !nextSelection;

    if (isEqual(nextSelection, currentSelection)) {
      return;
    }

    this.setState({
      selection: nextSelection
    });

    if (this.props.onChangeSelection) {
      this.props.onChangeSelection(nextSelection, source, editor);
    }

    if (hasGainedFocus && this.props.onFocus) {
      this.props.onFocus(nextSelection, source, editor);
    } else if (hasLostFocus && this.props.onBlur) {
      this.props.onBlur(currentSelection, source, editor);
    }
  },
  focus: function focus() {
    this.editor.focus();
  },
  blur: function blur() {
    this.setEditorSelection(this.editor, null);
  }
});
module.exports = QuillComponent;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtcXVpbGwvbGliL2NvbXBvbmVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LXF1aWxsL2xpYi9jb21wb25lbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG52YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xudmFyIFJlYWN0RE9NID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG52YXIgY3JlYXRlQ2xhc3MgPSByZXF1aXJlKCdjcmVhdGUtcmVhY3QtY2xhc3MnKTtcbnZhciBRdWlsbE1peGluID0gcmVxdWlyZSgnLi9taXhpbicpO1xudmFyIGZpbmQgPSByZXF1aXJlKCdsb2Rhc2gvZmluZCcpO1xudmFyIHNvbWUgPSByZXF1aXJlKCdsb2Rhc2gvc29tZScpO1xudmFyIGlzRXF1YWwgPSByZXF1aXJlKCdsb2Rhc2gvaXNFcXVhbCcpO1xudmFyIFQgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG52YXIgRE9NID0gcmVxdWlyZSgncmVhY3QtZG9tLWZhY3RvcmllcycpO1xuXG52YXIgUXVpbGxDb21wb25lbnQgPSBjcmVhdGVDbGFzcyh7XG5cblx0ZGlzcGxheU5hbWU6ICdRdWlsbCcsXG5cblx0bWl4aW5zOiBbIFF1aWxsTWl4aW4gXSxcblxuXHRwcm9wVHlwZXM6IHtcblx0XHRpZDogVC5zdHJpbmcsXG5cdFx0Y2xhc3NOYW1lOiBULnN0cmluZyxcblx0XHR0aGVtZTogVC5zdHJpbmcsXG5cdFx0c3R5bGU6IFQub2JqZWN0LFxuXHRcdHJlYWRPbmx5OiBULmJvb2wsXG5cdFx0dmFsdWU6IFQub25lT2ZUeXBlKFtULnN0cmluZywgVC5zaGFwZSh7b3BzOiBULmFycmF5fSldKSxcblx0XHRkZWZhdWx0VmFsdWU6IFQub25lT2ZUeXBlKFtULnN0cmluZywgVC5zaGFwZSh7b3BzOiBULmFycmF5fSldKSxcblx0XHRwbGFjZWhvbGRlcjogVC5zdHJpbmcsXG5cdFx0dGFiSW5kZXg6IFQubnVtYmVyLFxuXHRcdGJvdW5kczogVC5vbmVPZlR5cGUoW1Quc3RyaW5nLCBULmVsZW1lbnRdKSxcblx0XHRvbkNoYW5nZTogVC5mdW5jLFxuXHRcdG9uQ2hhbmdlU2VsZWN0aW9uOiBULmZ1bmMsXG5cdFx0b25Gb2N1czogVC5mdW5jLFxuXHRcdG9uQmx1cjogVC5mdW5jLFxuXHRcdG9uS2V5UHJlc3M6IFQuZnVuYyxcblx0XHRvbktleURvd246IFQuZnVuYyxcblx0XHRvbktleVVwOiBULmZ1bmMsXG5cdFx0cHJlc2VydmVXaGl0ZXNwYWNlOiBULmJvb2wsXG5cblx0XHRtb2R1bGVzOiBmdW5jdGlvbihwcm9wcykge1xuXHRcdFx0dmFyIGlzTm90T2JqZWN0ID0gVC5vYmplY3QuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0XHRcdGlmIChpc05vdE9iamVjdCkgcmV0dXJuIGlzTm90T2JqZWN0O1xuXG5cdFx0XHRpZiAoXG5cdFx0XHRcdHByb3BzLm1vZHVsZXMgJiYgXG5cdFx0XHRcdHByb3BzLm1vZHVsZXMudG9vbGJhciAmJlxuXHRcdFx0XHRwcm9wcy5tb2R1bGVzLnRvb2xiYXJbMF0gJiZcblx0XHRcdFx0cHJvcHMubW9kdWxlcy50b29sYmFyWzBdLnR5cGVcblx0XHRcdCkgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0J1NpbmNlIHYxLjAuMCwgUmVhY3QgUXVpbGwgd2lsbCBub3QgY3JlYXRlIGEgY3VzdG9tIHRvb2xiYXIgZm9yIHlvdSAnICtcblx0XHRcdFx0J2FueW1vcmUuIENyZWF0ZSBhIHRvb2xiYXIgZXhwbGljdGx5LCBvciBsZXQgUXVpbGwgY3JlYXRlIG9uZS4gJyArXG5cdFx0XHRcdCdTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS96ZW5vYW1hcm8vcmVhY3QtcXVpbGwjdXBncmFkaW5nLXRvLXJlYWN0LXF1aWxsLXYxMDAnXG5cdFx0XHQpO1xuXHRcdH0sXG5cblx0XHR0b29sYmFyOiBmdW5jdGlvbihwcm9wcykge1xuXHRcdFx0aWYgKCd0b29sYmFyJyBpbiBwcm9wcykgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0J1RoZSBgdG9vbGJhcmAgcHJvcCBoYXMgYmVlbiBkZXByZWNhdGVkLiBVc2UgYG1vZHVsZXMudG9vbGJhcmAgaW5zdGVhZC4gJyArXG5cdFx0XHRcdCdTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS96ZW5vYW1hcm8vcmVhY3QtcXVpbGwjdXBncmFkaW5nLXRvLXJlYWN0LXF1aWxsLXYxMDAnXG5cdFx0XHQpO1xuXHRcdH0sXG5cblx0XHRmb3JtYXRzOiBmdW5jdGlvbihwcm9wcykge1xuXHRcdFx0dmFyIGlzTm90QXJyYXlPZlN0cmluZyA9IFQuYXJyYXlPZihULnN0cmluZykuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblxuXHRcdFx0aWYgKGlzTm90QXJyYXlPZlN0cmluZykgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0J1lvdSBjYW5ub3Qgc3BlY2lmeSBjdXN0b20gYGZvcm1hdHNgIGFueW1vcmUuIFVzZSBQYXJjaG1lbnQgaW5zdGVhZC4gICcgK1xuXHRcdFx0XHQnU2VlOiBodHRwczovL2dpdGh1Yi5jb20vemVub2FtYXJvL3JlYWN0LXF1aWxsI3VwZ3JhZGluZy10by1yZWFjdC1xdWlsbC12MTAwLidcblx0XHRcdCk7XG5cdFx0fSxcblxuXHRcdHN0eWxlczogZnVuY3Rpb24ocHJvcHMpIHtcblx0XHRcdGlmICgnc3R5bGVzJyBpbiBwcm9wcykgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0J1RoZSBgc3R5bGVzYCBwcm9wIGhhcyBiZWVuIGRlcHJlY2F0ZWQuIFVzZSBjdXN0b20gc3R5bGVzaGVldHMgaW5zdGVhZC4gJyArXG5cdFx0XHRcdCdTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS96ZW5vYW1hcm8vcmVhY3QtcXVpbGwjdXBncmFkaW5nLXRvLXJlYWN0LXF1aWxsLXYxMDAuJ1xuXHRcdFx0KTtcblx0XHR9LFxuXG5cdFx0cG9sbEludGVydmFsOiBmdW5jdGlvbihwcm9wcykge1xuXHRcdFx0aWYgKCdwb2xsSW50ZXJ2YWwnIGluIHByb3BzKSByZXR1cm4gbmV3IEVycm9yKFxuXHRcdFx0XHQnVGhlIGBwb2xsSW50ZXJ2YWxgIHByb3BlcnR5IGRvZXMgbm90IGhhdmUgYW55IGVmZmVjdCBhbnltb3JlLiAnICtcblx0XHRcdFx0J1lvdSBjYW4gc2FmZWx5IHJlbW92ZSBpdCBmcm9tIHlvdXIgcHJvcHMuJyArXG5cdFx0XHRcdCdTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS96ZW5vYW1hcm8vcmVhY3QtcXVpbGwjdXBncmFkaW5nLXRvLXJlYWN0LXF1aWxsLXYxMDAuJ1xuXHRcdFx0KTtcblx0XHR9LFxuXG5cdFx0Y2hpbGRyZW46IGZ1bmN0aW9uKHByb3BzKSB7XG5cdFx0XHQvLyBWYWxpZGF0ZSB0aGF0IHRoZSBlZGl0b3IgaGFzIG9ubHkgb25lIGNoaWxkIGVsZW1lbnQgYW5kIGl0IGlzIG5vdCBhIDx0ZXh0YXJlYT5cblx0XHRcdHZhciBpc05vdEFTaW5nbGVFbGVtZW50ID0gVC5lbGVtZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cdFx0XHRpZiAoaXNOb3RBU2luZ2xlRWxlbWVudCkgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0J1RoZSBRdWlsbCBlZGl0aW5nIGFyZWEgY2FuIG9ubHkgYmUgY29tcG9zZWQgb2YgYSBzaW5nbGUgUmVhY3QgZWxlbWVudC4nXG5cdFx0XHQpO1xuXG5cdFx0XHRpZiAoUmVhY3QuQ2hpbGRyZW4uY291bnQocHJvcHMuY2hpbGRyZW4pKSB7XG5cdFx0XHRcdHZhciBjaGlsZCA9IFJlYWN0LkNoaWxkcmVuLm9ubHkocHJvcHMuY2hpbGRyZW4pO1xuXHRcdFx0XHRpZiAoY2hpbGQudHlwZSA9PT0gJ3RleHRhcmVhJykgcmV0dXJuIG5ldyBFcnJvcihcblx0XHRcdFx0XHQnUXVpbGwgZG9lcyBub3Qgc3VwcG9ydCBlZGl0aW5nIG9uIGEgPHRleHRhcmVhPi4gVXNlIGEgPGRpdj4gaW5zdGVhZC4nXG5cdFx0XHRcdCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9LFxuXHRcdFxuXHQvKlxuXHRDaGFuZ2luZyBvbmUgb2YgdGhlc2UgcHJvcHMgc2hvdWxkIGNhdXNlIGEgZnVsbCByZS1yZW5kZXIuXG5cdCovXG5cdGRpcnR5UHJvcHM6IFtcblx0XHQnbW9kdWxlcycsXG5cdFx0J2Zvcm1hdHMnLFxuXHRcdCdib3VuZHMnLFxuXHRcdCd0aGVtZScsXG5cdFx0J2NoaWxkcmVuJyxcblx0XSxcblxuXHQvKlxuXHRDaGFuZ2luZyBvbmUgb2YgdGhlc2UgcHJvcHMgc2hvdWxkIGNhdXNlIGEgcmVndWxhciB1cGRhdGUuXG5cdCovXG5cdGNsZWFuUHJvcHM6IFtcblx0XHQnaWQnLFxuXHRcdCdjbGFzc05hbWUnLFxuXHRcdCdzdHlsZScsXG5cdFx0J3BsYWNlaG9sZGVyJyxcblx0XHQndGFiSW5kZXgnLFxuXHRcdCdvbkNoYW5nZScsXG5cdFx0J29uQ2hhbmdlU2VsZWN0aW9uJyxcblx0XHQnb25Gb2N1cycsXG5cdFx0J29uQmx1cicsXG5cdFx0J29uS2V5UHJlc3MnLFxuXHRcdCdvbktleURvd24nLFxuXHRcdCdvbktleVVwJyxcblx0XSxcblxuXHRnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiB7XG5cdFx0XHR0aGVtZTogJ3Nub3cnLFxuXHRcdFx0bW9kdWxlczoge30sXG5cdFx0fTtcblx0fSxcblxuXHQvKlxuXHRXZSBjb25zaWRlciB0aGUgY29tcG9uZW50IHRvIGJlIGNvbnRyb2xsZWQgaWYgYHZhbHVlYCBpcyBiZWluZyBzZW50IGluIHByb3BzLlxuXHQqL1xuXHRpc0NvbnRyb2xsZWQ6IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiAndmFsdWUnIGluIHRoaXMucHJvcHM7XG5cdH0sXG5cblx0Z2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4ge1xuXHRcdFx0Z2VuZXJhdGlvbjogMCxcblx0XHRcdHZhbHVlOiB0aGlzLmlzQ29udHJvbGxlZCgpXG5cdFx0XHRcdD8gdGhpcy5wcm9wcy52YWx1ZVxuXHRcdFx0XHQ6IHRoaXMucHJvcHMuZGVmYXVsdFZhbHVlXG5cdFx0fTtcblx0fSxcblxuXHRjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzOiBmdW5jdGlvbihuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuXHRcdHZhciBlZGl0b3IgPSB0aGlzLmVkaXRvcjtcblxuXHRcdC8vIElmIHRoZSBjb21wb25lbnQgaXMgdW5tb3VudGVkIGFuZCBtb3VudGVkIHRvbyBxdWlja2x5XG5cdFx0Ly8gYW4gZXJyb3IgaXMgdGhyb3duIGluIHNldEVkaXRvckNvbnRlbnRzIHNpbmNlIGVkaXRvciBpc1xuXHRcdC8vIHN0aWxsIHVuZGVmaW5lZC4gTXVzdCBjaGVjayBpZiBlZGl0b3IgaXMgdW5kZWZpbmVkXG5cdFx0Ly8gYmVmb3JlIHBlcmZvcm1pbmcgdGhpcyBjYWxsLlxuXHRcdGlmICghZWRpdG9yKSByZXR1cm47XG5cdFx0XG5cdFx0Ly8gVXBkYXRlIG9ubHkgaWYgd2UndmUgYmVlbiBwYXNzZWQgYSBuZXcgYHZhbHVlYC5cblx0XHQvLyBUaGlzIGxlYXZlcyBjb21wb25lbnRzIHVzaW5nIGBkZWZhdWx0VmFsdWVgIGFsb25lLlxuXHRcdGlmICgndmFsdWUnIGluIG5leHRQcm9wcykge1xuXHRcdFx0dmFyIGN1cnJlbnRDb250ZW50cyA9IHRoaXMuZ2V0RWRpdG9yQ29udGVudHMoKTtcblx0XHRcdHZhciBuZXh0Q29udGVudHMgPSBuZXh0UHJvcHMudmFsdWU7XG5cblx0XHRcdGlmIChuZXh0Q29udGVudHMgPT09IHRoaXMubGFzdERlbHRhQ2hhbmdlU2V0KSB0aHJvdyBuZXcgRXJyb3IoXG5cdFx0XHRcdCdZb3UgYXJlIHBhc3NpbmcgdGhlIGBkZWx0YWAgb2JqZWN0IGZyb20gdGhlIGBvbkNoYW5nZWAgZXZlbnQgYmFjayAnICtcblx0XHRcdFx0J2FzIGB2YWx1ZWAuIFlvdSBtb3N0IHByb2JhYmx5IHdhbnQgYGVkaXRvci5nZXRDb250ZW50cygpYCBpbnN0ZWFkLiAnICtcblx0XHRcdFx0J1NlZTogaHR0cHM6Ly9naXRodWIuY29tL3plbm9hbWFyby9yZWFjdC1xdWlsbCN1c2luZy1kZWx0YXMnXG5cdFx0XHQpO1xuXG5cdFx0XHQvLyBOT1RFOiBTZWVpbmcgdGhhdCBRdWlsbCBpcyBtaXNzaW5nIGEgd2F5IHRvIHByZXZlbnRcblx0XHRcdC8vICAgICAgIGVkaXRzLCB3ZSBoYXZlIHRvIHNldHRsZSBmb3IgYSBoeWJyaWQgYmV0d2VlblxuXHRcdFx0Ly8gICAgICAgY29udHJvbGxlZCBhbmQgdW5jb250cm9sbGVkIG1vZGUuIFdlIGNhbid0IHByZXZlbnRcblx0XHRcdC8vICAgICAgIHRoZSBjaGFuZ2UsIGJ1dCB3ZSdsbCBzdGlsbCBvdmVycmlkZSBjb250ZW50XG5cdFx0XHQvLyAgICAgICB3aGVuZXZlciBgdmFsdWVgIGRpZmZlcnMgZnJvbSBjdXJyZW50IHN0YXRlLlxuXHRcdFx0Ly8gTk9URTogQ29tcGFyaW5nIGFuIEhUTUwgc3RyaW5nIGFuZCBhIFF1aWxsIERlbHRhIHdpbGwgYWx3YXlzIHRyaWdnZXIgXG5cdFx0XHQvLyAgICAgICBhIGNoYW5nZSwgcmVnYXJkbGVzcyBvZiB3aGV0aGVyIHRoZXkgcmVwcmVzZW50IHRoZSBzYW1lIGRvY3VtZW50LlxuXHRcdFx0aWYgKCF0aGlzLmlzRXF1YWxWYWx1ZShuZXh0Q29udGVudHMsIGN1cnJlbnRDb250ZW50cykpIHtcblx0XHRcdFx0dGhpcy5zZXRFZGl0b3JDb250ZW50cyhlZGl0b3IsIG5leHRDb250ZW50cyk7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdFxuXHRcdC8vIFdlIGNhbiB1cGRhdGUgcmVhZE9ubHkgc3RhdGUgaW4tcGxhY2UuXG5cdFx0aWYgKCdyZWFkT25seScgaW4gbmV4dFByb3BzKSB7XG5cdFx0XHRpZiAobmV4dFByb3BzLnJlYWRPbmx5ICE9PSB0aGlzLnByb3BzLnJlYWRPbmx5KSB7XG5cdFx0XHRcdHRoaXMuc2V0RWRpdG9yUmVhZE9ubHkoZWRpdG9yLCBuZXh0UHJvcHMucmVhZE9ubHkpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdC8vIElmIHdlIG5lZWQgdG8gcmVnZW5lcmF0ZSB0aGUgY29tcG9uZW50LCB3ZSBjYW4gYXZvaWQgYSBkZXRhaWxlZFxuXHRcdC8vIGluLXBsYWNlIHVwZGF0ZSBzdGVwLCBhbmQganVzdCBsZXQgZXZlcnl0aGluZyByZXJlbmRlci5cblx0XHRpZiAodGhpcy5zaG91bGRDb21wb25lbnRSZWdlbmVyYXRlKG5leHRQcm9wcywgbmV4dFN0YXRlKSkge1xuXHRcdFx0cmV0dXJuIHRoaXMucmVnZW5lcmF0ZSgpO1xuXHRcdH1cblx0fSxcblxuXHRjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG5cdFx0dGhpcy5lZGl0b3IgPSB0aGlzLmNyZWF0ZUVkaXRvcihcblx0XHRcdHRoaXMuZ2V0RWRpdGluZ0FyZWEoKSxcblx0XHRcdHRoaXMuZ2V0RWRpdG9yQ29uZmlnKClcblx0XHQpO1xuXHRcdC8vIFJlc3RvcmUgZWRpdG9yIGZyb20gUXVpbGwncyBuYXRpdmUgZm9ybWF0cyBpbiByZWdlbmVyYXRpb24gc2NlbmFyaW9cblx0XHRpZiAodGhpcy5xdWlsbERlbHRhKSB7XG5cdFx0XHR0aGlzLmVkaXRvci5zZXRDb250ZW50cyh0aGlzLnF1aWxsRGVsdGEpO1xuXHRcdFx0dGhpcy5lZGl0b3Iuc2V0U2VsZWN0aW9uKHRoaXMucXVpbGxTZWxlY3Rpb24pO1x0XHRcblx0XHRcdHRoaXMuZWRpdG9yLmZvY3VzKCk7XG5cdFx0XHR0aGlzLnF1aWxsRGVsdGEgPSB0aGlzLnF1aWxsU2VsZWN0aW9uID0gbnVsbDtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0aWYgKHRoaXMuc3RhdGUudmFsdWUpIHtcblx0XHRcdHRoaXMuc2V0RWRpdG9yQ29udGVudHModGhpcy5lZGl0b3IsIHRoaXMuc3RhdGUudmFsdWUpO1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblx0fSxcblxuXHRjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGVkaXRvcjsgaWYgKChlZGl0b3IgPSB0aGlzLmdldEVkaXRvcigpKSkge1xuXHRcdFx0dGhpcy51bmhvb2tFZGl0b3IoZWRpdG9yKTtcblx0XHRcdHRoaXMuZWRpdG9yID0gbnVsbDtcblx0XHR9XG5cdH0sXG5cblx0c2hvdWxkQ29tcG9uZW50VXBkYXRlOiBmdW5jdGlvbihuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuXHRcdHZhciBzZWxmID0gdGhpcztcblxuXHRcdC8vIElmIHRoZSBjb21wb25lbnQgaGFzIGJlZW4gcmVnZW5lcmF0ZWQsIHdlIGFscmVhZHkga25vdyB3ZSBzaG91bGQgdXBkYXRlLlxuXHRcdGlmICh0aGlzLnN0YXRlLmdlbmVyYXRpb24gIT09IG5leHRTdGF0ZS5nZW5lcmF0aW9uKSB7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gQ29tcGFyZSBwcm9wcyB0aGF0IHJlcXVpcmUgUmVhY3QgdXBkYXRpbmcgdGhlIERPTS5cblx0XHRyZXR1cm4gc29tZSh0aGlzLmNsZWFuUHJvcHMsIGZ1bmN0aW9uKHByb3ApIHtcblx0XHRcdC8vIE5vdGUgdGhhdCBgaXNFcXVhbGAgY29tcGFyZXMgZGVlcGx5LCBtYWtpbmcgaXQgc2FmZSB0byBwZXJmb3JtXG5cdFx0XHQvLyBub24taW1tdXRhYmxlIHVwZGF0ZXMsIGF0IHRoZSBjb3N0IG9mIHBlcmZvcm1hbmNlLlxuXHRcdFx0cmV0dXJuICFpc0VxdWFsKG5leHRQcm9wc1twcm9wXSwgc2VsZi5wcm9wc1twcm9wXSk7XG5cdFx0fSk7XG5cdH0sXG5cblx0c2hvdWxkQ29tcG9uZW50UmVnZW5lcmF0ZTogZnVuY3Rpb24obmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcblx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0Ly8gV2hlbmV2ZXIgYSBgZGlydHlQcm9wYCBjaGFuZ2VzLCB0aGUgZWRpdG9yIG5lZWRzIHJlaW5zdGFudGlhdGlvbi5cblx0XHRyZXR1cm4gc29tZSh0aGlzLmRpcnR5UHJvcHMsIGZ1bmN0aW9uKHByb3ApIHtcblx0XHRcdC8vIE5vdGUgdGhhdCBgaXNFcXVhbGAgY29tcGFyZXMgZGVlcGx5LCBtYWtpbmcgaXQgc2FmZSB0byBwZXJmb3JtXG5cdFx0XHQvLyBub24taW1tdXRhYmxlIHVwZGF0ZXMsIGF0IHRoZSBjb3N0IG9mIHBlcmZvcm1hbmNlLlxuXHRcdFx0cmV0dXJuICFpc0VxdWFsKG5leHRQcm9wc1twcm9wXSwgc2VsZi5wcm9wc1twcm9wXSk7XG5cdFx0fSk7XG5cdH0sXG5cblx0Lypcblx0SWYgd2UgY291bGQgbm90IHVwZGF0ZSBzZXR0aW5ncyBmcm9tIHRoZSBuZXcgcHJvcHMgaW4tcGxhY2UsIHdlIGhhdmUgdG8gdGVhclxuXHRkb3duIGV2ZXJ5dGhpbmcgYW5kIHJlLXJlbmRlciBmcm9tIHNjcmF0Y2guXG5cdCovXG5cdGNvbXBvbmVudFdpbGxVcGRhdGU6IGZ1bmN0aW9uKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG5cdFx0aWYgKHRoaXMuc3RhdGUuZ2VuZXJhdGlvbiAhPT0gbmV4dFN0YXRlLmdlbmVyYXRpb24pIHtcblx0XHRcdHRoaXMuY29tcG9uZW50V2lsbFVubW91bnQoKTtcblx0XHR9XG5cdH0sXG5cblx0Y29tcG9uZW50RGlkVXBkYXRlOiBmdW5jdGlvbihwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuXHRcdGlmICh0aGlzLnN0YXRlLmdlbmVyYXRpb24gIT09IHByZXZTdGF0ZS5nZW5lcmF0aW9uKSB7XG5cdFx0XHR0aGlzLmNvbXBvbmVudERpZE1vdW50KCk7XG5cdFx0fVxuXHR9LFxuXG5cdGdldEVkaXRvckNvbmZpZzogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGJvdW5kczogICAgICAgdGhpcy5wcm9wcy5ib3VuZHMsXG5cdFx0XHRmb3JtYXRzOiAgICAgIHRoaXMucHJvcHMuZm9ybWF0cyxcblx0XHRcdG1vZHVsZXM6ICAgICAgdGhpcy5wcm9wcy5tb2R1bGVzLFxuXHRcdFx0cGxhY2Vob2xkZXI6ICB0aGlzLnByb3BzLnBsYWNlaG9sZGVyLFxuXHRcdFx0cmVhZE9ubHk6ICAgICB0aGlzLnByb3BzLnJlYWRPbmx5LFxuICAgICAgXHRcdFx0c2Nyb2xsaW5nQ29udGFpbmVyOiB0aGlzLnByb3BzLnNjcm9sbGluZ0NvbnRhaW5lcixcblx0XHRcdHRhYkluZGV4OiAgICAgdGhpcy5wcm9wcy50YWJJbmRleCxcblx0XHRcdHRoZW1lOiAgICAgICAgdGhpcy5wcm9wcy50aGVtZSxcblx0XHR9O1xuXHR9LFxuXG5cdGdldEVkaXRvcjogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHRoaXMuZWRpdG9yO1xuXHR9LFxuXG5cdGdldEVkaXRpbmdBcmVhOiBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuIFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMuZWRpdGluZ0FyZWEpO1xuXHR9LFxuXG5cdGdldEVkaXRvckNvbnRlbnRzOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gdGhpcy5zdGF0ZS52YWx1ZTtcblx0fSxcblxuXHRnZXRFZGl0b3JTZWxlY3Rpb246IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiB0aGlzLnN0YXRlLnNlbGVjdGlvbjtcblx0fSxcblxuXHQvKlxuXHRUcnVlIGlmIHRoZSB2YWx1ZSBpcyBhIERlbHRhIGluc3RhbmNlIG9yIGEgRGVsdGEgbG9vay1hbGlrZS5cblx0Ki9cblx0aXNEZWx0YTogZnVuY3Rpb24odmFsdWUpIHtcblx0XHRyZXR1cm4gdmFsdWUgJiYgdmFsdWUub3BzO1xuXHR9LFxuXG5cdC8qXG5cdFNwZWNpYWwgY29tcGFyaXNvbiBmdW5jdGlvbiB0aGF0IGtub3dzIGhvdyB0byBjb21wYXJlIERlbHRhcy5cblx0Ki9cblx0aXNFcXVhbFZhbHVlOiBmdW5jdGlvbih2YWx1ZSwgbmV4dFZhbHVlKSB7XG5cdFx0aWYgKHRoaXMuaXNEZWx0YSh2YWx1ZSkgJiYgdGhpcy5pc0RlbHRhKG5leHRWYWx1ZSkpIHtcblx0XHRcdHJldHVybiBpc0VxdWFsKHZhbHVlLm9wcywgbmV4dFZhbHVlLm9wcyk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJldHVybiBpc0VxdWFsKHZhbHVlLCBuZXh0VmFsdWUpO1xuXHRcdH1cblx0fSxcblxuXHQvKlxuXHRSZWdlbmVyYXRpbmcgdGhlIGVkaXRvciB3aWxsIGNhdXNlIHRoZSB3aG9sZSB0cmVlLCBpbmNsdWRpbmcgdGhlIGNvbnRhaW5lcixcblx0dG8gYmUgY2xlYW5lZCB1cCBhbmQgcmUtcmVuZGVyZWQgZnJvbSBzY3JhdGNoLlxuXHQqL1xuXHRyZWdlbmVyYXRlOiBmdW5jdGlvbigpIHtcblx0XHQvLyBDYWNoZSBzZWxlY3Rpb24gYW5kIGNvbnRlbnRzIGluIFF1aWxsJ3MgbmF0aXZlIGZvcm1hdCB0byBiZSByZXN0b3JlZCBsYXRlclxuXHRcdHRoaXMucXVpbGxEZWx0YSA9IHRoaXMuZWRpdG9yLmdldENvbnRlbnRzKCk7XG5cdFx0dGhpcy5xdWlsbFNlbGVjdGlvbiA9IHRoaXMuZWRpdG9yLmdldFNlbGVjdGlvbigpO1xuXHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0Z2VuZXJhdGlvbjogdGhpcy5zdGF0ZS5nZW5lcmF0aW9uICsgMSxcblx0XHR9KTtcblx0fSxcblxuXHQvKlxuXHRSZW5kZXJzIGFuIGVkaXRvciBhcmVhLCB1bmxlc3MgaXQgaGFzIGJlZW4gcHJvdmlkZWQgb25lIHRvIGNsb25lLlxuXHQqL1xuXHRyZW5kZXJFZGl0aW5nQXJlYTogZnVuY3Rpb24oKSB7XG5cdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdHZhciBjaGlsZHJlbiA9IHRoaXMucHJvcHMuY2hpbGRyZW47XG5cdFx0dmFyIHByZXNlcnZlV2hpdGVzcGFjZSA9IHRoaXMucHJvcHMucHJlc2VydmVXaGl0ZXNwYWNlO1xuXG5cdFx0dmFyIHByb3BlcnRpZXMgPSB7XG5cdFx0XHRrZXk6IHRoaXMuc3RhdGUuZ2VuZXJhdGlvbixcblx0XHRcdHRhYkluZGV4OiB0aGlzLnByb3BzLnRhYkluZGV4LFxuXHRcdFx0cmVmOiBmdW5jdGlvbihlbGVtZW50KSB7IHNlbGYuZWRpdGluZ0FyZWEgPSBlbGVtZW50IH0sXG5cdFx0fTtcblxuXHRcdHZhciBjdXN0b21FbGVtZW50ID0gUmVhY3QuQ2hpbGRyZW4uY291bnQoY2hpbGRyZW4pXG5cdFx0XHQ/IFJlYWN0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pXG5cdFx0XHQ6IG51bGw7XG5cdFx0dmFyIGRlZmF1bHRFbGVtZW50ID0gcHJlc2VydmVXaGl0ZXNwYWNlID8gRE9NLnByZSA6IERPTS5kaXY7XG5cdFx0dmFyIGVkaXRpbmdBcmVhID0gY3VzdG9tRWxlbWVudFxuXHRcdFx0PyBSZWFjdC5jbG9uZUVsZW1lbnQoY3VzdG9tRWxlbWVudCwgcHJvcGVydGllcylcblx0XHRcdDogZGVmYXVsdEVsZW1lbnQocHJvcGVydGllcyk7XG5cblx0XHRyZXR1cm4gZWRpdGluZ0FyZWE7XG5cdH0sXG5cblx0cmVuZGVyOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gRE9NLmRpdih7XG5cdFx0XHRpZDogdGhpcy5wcm9wcy5pZCxcblx0XHRcdHN0eWxlOiB0aGlzLnByb3BzLnN0eWxlLFxuXHRcdFx0a2V5OiB0aGlzLnN0YXRlLmdlbmVyYXRpb24sXG5cdFx0XHRjbGFzc05hbWU6IFsncXVpbGwnXS5jb25jYXQodGhpcy5wcm9wcy5jbGFzc05hbWUpLmpvaW4oJyAnKSxcblx0XHRcdG9uS2V5UHJlc3M6IHRoaXMucHJvcHMub25LZXlQcmVzcyxcblx0XHRcdG9uS2V5RG93bjogdGhpcy5wcm9wcy5vbktleURvd24sXG5cdFx0XHRvbktleVVwOiB0aGlzLnByb3BzLm9uS2V5VXAgfSxcblx0XHRcdHRoaXMucmVuZGVyRWRpdGluZ0FyZWEoKVxuXHRcdCk7XG5cdH0sXG5cblx0b25FZGl0b3JDaGFuZ2VUZXh0OiBmdW5jdGlvbih2YWx1ZSwgZGVsdGEsIHNvdXJjZSwgZWRpdG9yKSB7XG5cdFx0dmFyIGN1cnJlbnRDb250ZW50cyA9IHRoaXMuZ2V0RWRpdG9yQ29udGVudHMoKTtcblxuXHRcdC8vIFdlIGtlZXAgc3RvcmluZyB0aGUgc2FtZSB0eXBlIG9mIHZhbHVlIGFzIHdoYXQgdGhlIHVzZXIgZ2l2ZXMgdXMsXG5cdFx0Ly8gc28gdGhhdCB2YWx1ZSBjb21wYXJpc29ucyB3aWxsIGJlIG1vcmUgc3RhYmxlIGFuZCBwcmVkaWN0YWJsZS5cblx0XHR2YXIgbmV4dENvbnRlbnRzID0gdGhpcy5pc0RlbHRhKGN1cnJlbnRDb250ZW50cylcblx0XHRcdD8gZWRpdG9yLmdldENvbnRlbnRzKClcblx0XHRcdDogZWRpdG9yLmdldEhUTUwoKTtcblx0XHRcblx0XHRpZiAoIXRoaXMuaXNFcXVhbFZhbHVlKG5leHRDb250ZW50cywgY3VycmVudENvbnRlbnRzKSkge1xuXHRcdFx0Ly8gVGFpbnQgdGhpcyBgZGVsdGFgIG9iamVjdCwgc28gd2UgY2FuIHJlY29nbml6ZSB3aGV0aGVyIHRoZSB1c2VyXG5cdFx0XHQvLyBpcyB0cnlpbmcgdG8gc2VuZCBpdCBiYWNrIGFzIGB2YWx1ZWAsIHByZXZlbnRpbmcgYSBsaWtlbHkgbG9vcC5cblx0XHRcdHRoaXMubGFzdERlbHRhQ2hhbmdlU2V0ID0gZGVsdGE7XG5cblx0XHRcdHRoaXMuc2V0U3RhdGUoeyB2YWx1ZTogbmV4dENvbnRlbnRzIH0pO1xuXG5cdFx0XHRpZiAodGhpcy5wcm9wcy5vbkNoYW5nZSkge1xuXHRcdFx0XHR0aGlzLnByb3BzLm9uQ2hhbmdlKHZhbHVlLCBkZWx0YSwgc291cmNlLCBlZGl0b3IpO1xuXHRcdFx0fVxuXHRcdH1cblx0fSxcblxuXHRvbkVkaXRvckNoYW5nZVNlbGVjdGlvbjogZnVuY3Rpb24obmV4dFNlbGVjdGlvbiwgc291cmNlLCBlZGl0b3IpIHtcblx0XHR2YXIgY3VycmVudFNlbGVjdGlvbiA9IHRoaXMuZ2V0RWRpdG9yU2VsZWN0aW9uKCk7XG5cdFx0dmFyIGhhc0dhaW5lZEZvY3VzID0gIWN1cnJlbnRTZWxlY3Rpb24gJiYgbmV4dFNlbGVjdGlvbjtcblx0XHR2YXIgaGFzTG9zdEZvY3VzID0gY3VycmVudFNlbGVjdGlvbiAmJiAhbmV4dFNlbGVjdGlvbjtcblxuXHRcdGlmIChpc0VxdWFsKG5leHRTZWxlY3Rpb24sIGN1cnJlbnRTZWxlY3Rpb24pKSB7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXHRcdFxuXHRcdHRoaXMuc2V0U3RhdGUoeyBzZWxlY3Rpb246IG5leHRTZWxlY3Rpb24gfSk7XG5cdFx0XG5cdFx0aWYgKHRoaXMucHJvcHMub25DaGFuZ2VTZWxlY3Rpb24pIHtcblx0XHRcdHRoaXMucHJvcHMub25DaGFuZ2VTZWxlY3Rpb24obmV4dFNlbGVjdGlvbiwgc291cmNlLCBlZGl0b3IpO1xuXHRcdH1cblxuXHRcdGlmIChoYXNHYWluZWRGb2N1cyAmJiB0aGlzLnByb3BzLm9uRm9jdXMpIHtcblx0XHRcdHRoaXMucHJvcHMub25Gb2N1cyhuZXh0U2VsZWN0aW9uLCBzb3VyY2UsIGVkaXRvcik7XG5cdFx0fSBlbHNlIGlmIChoYXNMb3N0Rm9jdXMgJiYgdGhpcy5wcm9wcy5vbkJsdXIpIHtcblx0XHRcdHRoaXMucHJvcHMub25CbHVyKGN1cnJlbnRTZWxlY3Rpb24sIHNvdXJjZSwgZWRpdG9yKTtcblx0XHR9XG5cdH0sXG5cblx0Zm9jdXM6IGZ1bmN0aW9uKCkge1xuXHRcdHRoaXMuZWRpdG9yLmZvY3VzKCk7XG5cdH0sXG5cblx0Ymx1cjogZnVuY3Rpb24oKSB7XG5cdFx0dGhpcy5zZXRFZGl0b3JTZWxlY3Rpb24odGhpcy5lZGl0b3IsIG51bGwpO1xuXHR9XG5cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFF1aWxsQ29tcG9uZW50O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBVUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBRUE7QUFJQTtBQUVBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBaEZBO0FBQ0E7QUFrRkE7OztBQUdBO0FBQ0E7QUFPQTs7O0FBR0E7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBTUE7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUhBO0FBTUE7QUFHQTtBQUNBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBVUE7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFwWkE7QUF3WkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-quill/lib/component.js
