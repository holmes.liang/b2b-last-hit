__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Coverage; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk/styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _location__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../location */ "./src/app/desk/quote/SAIC/iar/location.tsx");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! number-precision */ "./node_modules/number-precision/build/index.js");
/* harmony import */ var number_precision__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(number_precision__WEBPACK_IMPORTED_MODULE_16__);








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/coverage.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n    color: #333\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n  margin-bottom: 20px;\n  .coverage-title {\n    overflow: hidden;\n    margin-bottom: 10px;\n  }\n  .ant-table-wrapper {\n    margin-bottom: 24px\n  }\n  .ant-checkbox-wrapper {\n    padding-right: 0 !important;\n  }\n  .header-checkbox {\n    display: flex;\n    align-items: center;\n    .ant-form-item {\n      margin-bottom: 0;\n      padding-right: 10px;\n      .ant-form-item-control {\n        line-height: 20px;\n        label {\n          line-height: 20px;\n        }\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var CoverageDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject());
var BlackSpan = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].span(_templateObject2());
var Column = antd__WEBPACK_IMPORTED_MODULE_9__["Table"].Column,
    ColumnGroup = antd__WEBPACK_IMPORTED_MODULE_9__["Table"].ColumnGroup;

var Coverage =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Coverage, _ModelWidget);

  function Coverage() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Coverage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Coverage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.calcRate = function (dataIndex) {
      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".premium"))) && _common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured")))) {
        var rate = (parseFloat(_this.getValueFromModel("benefits.".concat(dataIndex, ".premium"))) / parseFloat(_this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured")))).toFixed(4);

        _this.setValueToModel(rate, "benefits.".concat(dataIndex, ".rate"));

        return (rate * 100).toFixed(2);
      }

      return "";
    };

    _this.calcPOIRate = function () {
      var _this$props = _this.props,
          expDate = _this$props.expDate,
          effDate = _this$props.effDate;
      var startDate = _common__WEBPACK_IMPORTED_MODULE_14__["DateUtils"].toDate(effDate).startOf("day");
      var endDate = _common__WEBPACK_IMPORTED_MODULE_14__["DateUtils"].toDate(expDate).startOf("day");

      var annualDate = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.cloneDeep(endDate);

      annualDate = annualDate.add(1, "years");
      var diffDays = endDate.diff(startDate, "days");
      diffDays = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.plus(diffDays, 1);
      var annualDays = annualDate.diff(endDate, "days");
      var POIRate = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.divide(diffDays, annualDays); // POIRate = NP.round(POIRate, 2);

      return POIRate;
    };

    _this.calcPre = function (dataIndex) {
      var premiumCurrency = _this.props.premiumCurrency;

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".rate"))) && _common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured")))) {
        var sumInsured = _this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured"));

        var rate = _this.getValueFromModel("benefits.".concat(dataIndex, ".rate"));

        var premium = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.times(sumInsured, rate);
        premium = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.round(premium, 2);

        _this.setValueToModel(premium, "benefits.".concat(dataIndex, ".premium"));

        _this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, "benefits.".concat(dataIndex, ".premium"), premium));

        var data = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo(premium, premiumCurrency);
        return "".concat(data.amount);
      }

      return "";
    };

    _this.calcEndoPre = function (dataIndex) {
      var premiumCurrency = _this.props.premiumCurrency;

      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".rate"))) && _common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(_this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured")))) {
        var sumInsured = _this.getValueFromModel("benefits.".concat(dataIndex, ".sumInsured"));

        var rate = _this.getValueFromModel("benefits.".concat(dataIndex, ".rate"));

        var accPremiun = _this.getValueFromModel("benefits.".concat(dataIndex, ".accumulatedPremium")) || 0;

        var POIRate = _this.calcPOIRate();

        var calcValue = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.times(sumInsured, rate);
        calcValue = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.minus(calcValue, accPremiun);
        var premium = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.times(calcValue, POIRate);
        premium = number_precision__WEBPACK_IMPORTED_MODULE_16___default.a.round(premium, 2);

        _this.setValueToModel(premium, "benefits.".concat(dataIndex, ".premium"));

        _this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, "benefits.".concat(dataIndex, ".premium"), premium));

        var data = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo(premium, premiumCurrency);
        return "".concat(data.amount);
      }

      return "";
    };

    _this.formatterPrice = function (value) {
      if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isFloatNumber(value)) {
        return _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].toThousands(value);
      }

      return value;
    };

    _this.parserPrice = function (value) {
      return value.replace(/\S\$\s?|(,*)/g, "");
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Coverage, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props2 = this.props,
          _this$props2$breakDow = _this$props2.breakDown,
          breakDown = _this$props2$breakDow === void 0 ? [] : _this$props2$breakDow,
          isEndo = _this$props2.isEndo;
      var dataPropsIndex = this.props.dataIndex; // const dataFixed = `member.ext.coverages.${dataPropsIndex}`;
      // const coverage = _.get(this.props.model, dataFixed);

      var coverage = this.props.model;

      if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.isEmpty(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "selected")) && dataPropsIndex === 0) {
        // this.setValueToModel("Y", `${dataFixed}.selected`);
        this.setValueToModel("Y", "selected");
      }

      if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.isEmpty(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "selected")) && dataPropsIndex != 0) {
        this.setValueToModel("N", "selected");
      }

      var benefits = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "benefits", []) || [];

      if (isEndo) {
        benefits.forEach(function (item, i) {
          breakDown.forEach(function (every, index) {
            if (every.type === "COVER_PREMIUM" && every.name === item.benefitName) {
              _this2.setValueToModel(every.amount, "benefits.".concat(i, ".accumulatedPremium"));
            }
          });

          if (!_this2.getValueFromModel("benefits.".concat(i, ".accumulatedPremium"))) {
            _this2.setValueToModel(0, "benefits.".concat(i, ".accumulatedPremium"));
          }
        });
      }

      this.setAllEditalble();
    }
  }, {
    key: "setValueYtoN",
    value: function setValueYtoN(value) {
      return value === "Y" ? "Y" : "N";
    }
  }, {
    key: "setAllEditalble",
    value: function setAllEditalble() {
      var benefits = this.getValueFromModel("benefits") || [];
      var benefitsLen = benefits.length;
      var benefitsSelectedLen = 0;
      benefits.forEach(function (item) {
        if (item.selected === "Y") {
          benefitsSelectedLen++;
        }
      });

      if (benefitsLen === benefitsSelectedLen) {
        this.setValueToModel("Y", "benefits.selected");
      } else {
        this.setValueToModel("N", "benefits.selected");
      }
    }
  }, {
    key: "initRow",
    value: function initRow(value, dataIndex) {
      if (value === "N" || !value) {
        this.setValueToModel("", "benefits.".concat(dataIndex, ".sumInsured"));
        this.setValueToModel("", "benefits.".concat(dataIndex, ".ratingMode"));
        this.setValueToModel("", "benefits.".concat(dataIndex, ".rate"));
        this.setValueToModel("", "benefits.".concat(dataIndex, ".premium"));
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var dataPropsIndex = this.props.dataIndex; // const dataFixed = `member.ext.coverages.${dataPropsIndex}`;
      // const coverage = _.get(this.props.model, dataFixed);

      var siData = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo("", this.props.siCurrency);
      var premiumData = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo("", this.props.premiumCurrency);
      var coverage = this.props.model;
      var _this$props3 = this.props,
          form = _this$props3.form,
          model = _this$props3.model,
          isEndo = _this$props3.isEndo;

      var dataSource = lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "benefits", []);

      dataSource.forEach(function (item, i) {
        if (lodash__WEBPACK_IMPORTED_MODULE_8___default.a.isEmpty(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "selected"))) {
          lodash__WEBPACK_IMPORTED_MODULE_8___default.a.set(dataSource[i], "selected", "Y");
        }
      });
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(CoverageDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 209
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        className: "coverage-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 210
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
        style: {
          fontWeight: 600
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 211
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "coverName", "")), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Switch"], {
        style: {
          float: "right"
        },
        checked: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "selected") === "Y",
        onChange: function onChange(checked) {
          if (checked) {
            _this3.setValueToModel("Y", "selected");
          } else {
            _this3.setValueToModel("N", "selected");
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 216
        },
        __self: this
      })), lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(coverage, "selected") === "Y" && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Table"], {
        bordered: true,
        dataSource: [].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(dataSource), [{}]),
        pagination: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 230
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Column, {
        title: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
          className: "header-checkbox",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 235
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NCheckbox"], {
          model: model,
          form: form,
          checked: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(this.props.model, "benefits.selected") === "Y",
          required: true,
          label: "",
          onChange: function onChange(value) {
            _this3.setValueToModel(_this3.setValueYtoN(value), "benefits.selected");

            var benefits = _this3.getValueFromModel("benefits") || [];
            benefits.forEach(function (item, index) {
              _this3.setValueToModel(_this3.setValueYtoN(value), "benefits.".concat(index, ".selected"));

              _this3.initRow(value, index);
            });
          },
          propName: "benefits.selected",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 236
          },
          __self: this
        }), "BENEFIT"),
        dataIndex: "benefitName",
        key: "benefitName",
        render: function render(text, record, dataIndex) {
          if (dataIndex === dataSource.length) {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(BlackSpan, {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 254
              },
              __self: this
            }, "Total");
          } else if (dataSource[dataIndex].selected === "N") {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
              style: {
                display: "flex"
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 256
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 259
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NCheckbox"], {
              model: model,
              form: form,
              checked: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(_this3.props.model, "benefits.".concat(dataIndex, ".selected")) === "Y",
              required: true,
              label: "",
              onChange: function onChange(value) {
                _this3.initRow(value, dataIndex);

                _this3.setAllEditalble();
              },
              propName: "benefits.".concat(dataIndex, ".selected"),
              __source: {
                fileName: _jsxFileName,
                lineNumber: 260
              },
              __self: this
            })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
              style: {
                lineHeight: "42px",
                marginLeft: 10
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 273
              },
              __self: this
            }, text));
          } else {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
              style: {
                display: "flex"
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 282
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 285
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NCheckbox"], {
              model: model,
              form: form,
              checked: lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(_this3.props.model, "benefits.".concat(dataIndex, ".selected")) === "Y",
              required: true,
              label: "",
              onChange: function onChange(value) {
                _this3.initRow(value, dataIndex);

                _this3.setAllEditalble();
              },
              propName: "benefits.".concat(dataIndex, ".selected"),
              __source: {
                fileName: _jsxFileName,
                lineNumber: 286
              },
              __self: this
            })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("span", {
              style: {
                lineHeight: "42px",
                marginLeft: 10
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 298
              },
              __self: this
            }, text, "\xA0", _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Popover"], {
              placement: "top",
              title: text,
              content: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 303
                },
                __self: this
              }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_11__["NFormItem"], {
                model: model,
                form: form,
                required: true,
                label: "",
                propName: "benefits.".concat(dataIndex, ".benefitDesc"),
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 304
                },
                __self: this
              }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Input"].TextArea, {
                rows: 5,
                style: {
                  resize: "none",
                  width: 360
                },
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 309
                },
                __self: this
              }))),
              trigger: "click",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 302
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
              style: {
                cursor: "pointer"
              },
              className: "iconfont icon-work-on",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 313
              },
              __self: this
            })))), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 320
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("p", {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 328
              },
              __self: this
            }, record.benefitDesc)));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 235
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Column, {
        width: 180,
        className: "amount-right",
        title: "SUM INSURED (".concat(siData.currencyText, ")"),
        dataIndex: "sumInsured",
        key: "sumInsured",
        render: function render(text, record, dataIndex) {
          if (dataIndex === dataSource.length) {
            var total = 0;
            dataSource.forEach(function (item) {
              if (item.selected === "Y") {
                total += parseFloat(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "sumInsured", 0) || 0);
              }
            });
            var siCurrency = _this3.props.siCurrency;
            total = parseFloat(total).toFixed(0);
            return _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].toThousands(total); // const data = Utils.formatCurrencyInfo(total, siCurrency);
            // return <BlackSpan>{data.amount}</BlackSpan>;
          } else if (dataSource[dataIndex].selected === "N") {
            return "";
          } else {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 352
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_location__WEBPACK_IMPORTED_MODULE_15__["RightDiv"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 353
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NNumber"], {
              model: model,
              form: form,
              label: "",
              precision: 0,
              size: "large",
              formatter: _this3.formatterPrice,
              parser: _this3.parserPrice,
              propName: "benefits.".concat(dataIndex, ".sumInsured"),
              required: true // onBlur={() => {
              //   if (!this.getValueFromModel(`benefits.${dataIndex}.premium`)) {
              //     this.calcPre(dataIndex);
              //   }
              // }}
              ,
              onChange: function onChange(value) {
                if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isNum(value)) {
                  var _total = 0;
                  dataSource.forEach(function (item) {
                    if (item.selected === "Y") {
                      _total += parseFloat(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "sumInsured", "0") || "0");
                    }
                  });

                  _this3.setValueToModel(parseFloat(_total), "aggregatedSi");

                  _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, "aggregatedSi", parseFloat(_total))); // if (this.getValueFromModel(`benefits.${dataIndex}.ratingMode`) === "FLAT" && Rules.isNum(this.getValueFromModel(`benefits.${dataIndex}.premium`))) {
                  //   this.calcRate(dataIndex);
                  // }

                }
              },
              __source: {
                fileName: _jsxFileName,
                lineNumber: 354
              },
              __self: this
            })));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 334
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Column, {
        width: 160,
        className: "amount-right",
        title: "RATE",
        dataIndex: "rate",
        key: "rate",
        render: function render(text, record, dataIndex) {
          if (dataIndex === dataSource.length) {
            return "";
          } else if (dataSource[dataIndex].selected === "N") {
            return "";
          } else {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 394
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_location__WEBPACK_IMPORTED_MODULE_15__["RightDiv"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 395
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NRate"], {
              label: "",
              propName: "benefits.".concat(dataIndex, ".rate"),
              style: {
                width: "100%"
              },
              precision: 4,
              size: "large",
              model: model,
              form: form,
              required: false // onChange={(value: any) => {
              //   const modelPremium = this.getValueFromModel(`benefits.${dataIndex}.premium`);
              //   if (!modelPremium) {
              //     this.calcPre(dataIndex);
              //   }
              // }}
              // onBlur={() => {
              //   const modelPremium = this.getValueFromModel(`benefits.${dataIndex}.premium`);
              //   if (!modelPremium) {
              //     this.calcPre(dataIndex);
              //   }
              // }}
              ,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 396
              },
              __self: this
            })));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 387
        },
        __self: this
      }), isEndo && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Column, {
        className: "amount-right",
        title: "ACCUMULATED PREMIUM (".concat(premiumData.currencyText, ")"),
        dataIndex: "accumulatedPremium",
        key: "accumulatedPremium",
        render: function render(text, record, dataIndex) {
          if (dataIndex === dataSource.length) {
            return "";
          } else if (dataSource[dataIndex].selected === "N") {
            return "";
          } else if (_common__WEBPACK_IMPORTED_MODULE_14__["Rules"].isFloatNumber(text)) {
            var data = text.toFixed(2);
            return _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].toThousands(data);
          } else {
            return "0.00";
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 425
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(Column, {
        width: 180,
        className: "amount-right",
        title: isEndo ? "DELTA PREMIUM (".concat(premiumData.currencyText, ")") : "PREMIUM (".concat(premiumData.currencyText, ")"),
        dataIndex: "premium",
        key: "premium",
        render: function render(text, record, dataIndex) {
          if (dataIndex === dataSource.length) {
            var total = 0;
            dataSource.forEach(function (item) {
              if (item.selected === "Y") {
                total += parseFloat(lodash__WEBPACK_IMPORTED_MODULE_8___default.a.get(item, "premium", 0) || 0);
              }
            });
            var premiumCurrency = _this3.props.premiumCurrency;
            var data = _common__WEBPACK_IMPORTED_MODULE_14__["Utils"].formatCurrencyInfo(total, premiumCurrency);
            return "".concat(data.amount);
          } else if (dataSource[dataIndex].selected === "N") {
            return "";
          } else {
            return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Popover"], {
              content: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("a", {
                onClick: function onClick() {
                  if (isEndo) {
                    _this3.calcEndoPre(dataIndex);
                  } else {
                    _this3.calcPre(dataIndex);
                  }
                },
                style: {
                  cursor: "pointer"
                },
                __source: {
                  fileName: _jsxFileName,
                  lineNumber: 462
                },
                __self: this
              }, "Calculate"),
              trigger: "focus",
              __source: {
                fileName: _jsxFileName,
                lineNumber: 461
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["StyleWithoutMarginBottom"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 475
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_location__WEBPACK_IMPORTED_MODULE_15__["RightDiv"], {
              __source: {
                fileName: _jsxFileName,
                lineNumber: 476
              },
              __self: this
            }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NNumber"], {
              model: model,
              form: form,
              label: "",
              precision: 2,
              formatter: _this3.formatterPrice,
              parser: _this3.parserPrice // onChange={() => {
              //   this.calcRate(dataIndex);
              // }}
              ,
              size: "large",
              propName: "benefits.".concat(dataIndex, ".premium"),
              required: !isEndo,
              __source: {
                fileName: _jsxFileName,
                lineNumber: 477
              },
              __self: this
            }))));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 442
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NPrice"], {
        model: model,
        form: form,
        precision: 0,
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("AGGREGATED SI").thai("AGGREGATED SI").getMessage(),
        propName: "aggregatedSi",
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 495
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NRate"], {
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("PML").thai("PML").getMessage(),
        propName: "pml",
        required: true,
        style: {
          width: "100%"
        },
        precision: 2,
        size: "large",
        model: model,
        form: form,
        originalValue: this.getValueFromModel("pml") || 1,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 502
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_11__["NFormItem"], {
        model: model,
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_14__["Language"].en("EXCESS").thai("EXCESS").getMessage(),
        propName: "excess",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 540
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Input"].TextArea, {
        rows: 3,
        style: {
          resize: "none"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 544
        },
        __self: this
      }))));
    }
  }]);

  return Coverage;
}(_component__WEBPACK_IMPORTED_MODULE_11__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2NvdmVyYWdlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2NvbXBvbmVudC9jb3ZlcmFnZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgSW5wdXQsIFBvcG92ZXIsIFN3aXRjaCwgVGFibGUgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTkNoZWNrYm94LCBOTnVtYmVyLCBOUHJpY2UsIE5SYXRlIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20gfSBmcm9tIFwiQGRlc2svc3R5bGVzL2dsb2JhbFwiO1xuaW1wb3J0IHsgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgUnVsZXMsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IFJpZ2h0RGl2IH0gZnJvbSBcIi4uL2xvY2F0aW9uXCI7XG5pbXBvcnQgTlAgZnJvbSBcIm51bWJlci1wcmVjaXNpb25cIjtcblxuaW50ZXJmYWNlIElQcm9wcyBleHRlbmRzIE1vZGVsV2lkZ2V0UHJvcHMge1xuICBtb2RlbDogYW55LFxuICBmb3JtOiBhbnksXG4gIGRhdGFJbmRleDogYW55LFxuICBzaUN1cnJlbmN5OiBhbnksXG4gIHByZW1pdW1DdXJyZW5jeTogYW55LFxuICBpc0VuZG8/OiBib29sZWFuLFxuICBlZmZEYXRlOiBhbnksXG4gIGV4cERhdGU6IGFueSxcbiAgYnJlYWtEb3duPzogYW55XG59XG5cbmNvbnN0IENvdmVyYWdlRGl2ID0gU3R5bGVkLmRpdmBcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgLmNvdmVyYWdlLXRpdGxlIHtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIH1cbiAgLmFudC10YWJsZS13cmFwcGVyIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyNHB4XG4gIH1cbiAgLmFudC1jaGVja2JveC13cmFwcGVyIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmhlYWRlci1jaGVja2JveCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIC5hbnQtZm9ybS1pdGVtIHtcbiAgICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgICAgLmFudC1mb3JtLWl0ZW0tY29udHJvbCB7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICBsYWJlbCB7XG4gICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbmA7XG5cbmNvbnN0IEJsYWNrU3BhbiA9IFN0eWxlZC5zcGFuYFxuICAgIGNvbG9yOiAjMzMzXG5gO1xuXG5jb25zdCB7IENvbHVtbiwgQ29sdW1uR3JvdXAgfSA9IFRhYmxlO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ292ZXJhZ2U8UCBleHRlbmRzIElQcm9wcywgUywgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHsgYnJlYWtEb3duID0gW10sIGlzRW5kbyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBkYXRhUHJvcHNJbmRleCA9IHRoaXMucHJvcHMuZGF0YUluZGV4O1xuICAgIC8vIGNvbnN0IGRhdGFGaXhlZCA9IGBtZW1iZXIuZXh0LmNvdmVyYWdlcy4ke2RhdGFQcm9wc0luZGV4fWA7XG4gICAgLy8gY29uc3QgY292ZXJhZ2UgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBkYXRhRml4ZWQpO1xuICAgIGNvbnN0IGNvdmVyYWdlID0gdGhpcy5wcm9wcy5tb2RlbDtcbiAgICBpZiAoXy5pc0VtcHR5KF8uZ2V0KGNvdmVyYWdlLCBcInNlbGVjdGVkXCIpKSAmJiBkYXRhUHJvcHNJbmRleCA9PT0gMCkge1xuICAgICAgLy8gdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJZXCIsIGAke2RhdGFGaXhlZH0uc2VsZWN0ZWRgKTtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiWVwiLCBgc2VsZWN0ZWRgKTtcbiAgICB9XG4gICAgaWYgKF8uaXNFbXB0eShfLmdldChjb3ZlcmFnZSwgXCJzZWxlY3RlZFwiKSkgJiYgZGF0YVByb3BzSW5kZXggIT0gMCkge1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJOXCIsIGBzZWxlY3RlZGApO1xuICAgIH1cbiAgICBjb25zdCBiZW5lZml0czogYW55ID0gXy5nZXQoY292ZXJhZ2UsIFwiYmVuZWZpdHNcIiwgW10pIHx8IFtdO1xuXG4gICAgaWYgKGlzRW5kbykge1xuICAgICAgYmVuZWZpdHMuZm9yRWFjaCgoaXRlbTogYW55LCBpOiBudW1iZXIpID0+IHtcbiAgICAgICAgYnJlYWtEb3duLmZvckVhY2goKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICBpZiAoZXZlcnkudHlwZSA9PT0gXCJDT1ZFUl9QUkVNSVVNXCIgJiYgZXZlcnkubmFtZSA9PT0gaXRlbS5iZW5lZml0TmFtZSkge1xuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoZXZlcnkuYW1vdW50LCBgYmVuZWZpdHMuJHtpfS5hY2N1bXVsYXRlZFByZW1pdW1gKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAoIXRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7aX0uYWNjdW11bGF0ZWRQcmVtaXVtYCkpIHtcbiAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbCgwLCBgYmVuZWZpdHMuJHtpfS5hY2N1bXVsYXRlZFByZW1pdW1gKTtcbiAgICAgICAgfVxuXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0aGlzLnNldEFsbEVkaXRhbGJsZSgpO1xuXG4gIH1cblxuICBjYWxjUmF0ZSA9IChkYXRhSW5kZXg6IGFueSkgPT4ge1xuICAgIGlmIChSdWxlcy5pc051bSh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0ucHJlbWl1bWApKSAmJiBSdWxlcy5pc051bSh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0uc3VtSW5zdXJlZGApKSkge1xuICAgICAgY29uc3QgcmF0ZTogYW55ID0gKHBhcnNlRmxvYXQodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnByZW1pdW1gKSkgLyBwYXJzZUZsb2F0KHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5zdW1JbnN1cmVkYCkpKS50b0ZpeGVkKDQpO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwocmF0ZSwgYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5yYXRlYCk7XG4gICAgICByZXR1cm4gKHJhdGUgKiAxMDApLnRvRml4ZWQoMik7XG4gICAgfVxuICAgIHJldHVybiBcIlwiO1xuICB9O1xuXG4gIGNhbGNQT0lSYXRlID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgZXhwRGF0ZSwgZWZmRGF0ZSB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBzdGFydERhdGUgPSBEYXRlVXRpbHMudG9EYXRlKGVmZkRhdGUpLnN0YXJ0T2YoXCJkYXlcIik7XG4gICAgY29uc3QgZW5kRGF0ZSA9IERhdGVVdGlscy50b0RhdGUoZXhwRGF0ZSkuc3RhcnRPZihcImRheVwiKTtcbiAgICBsZXQgYW5udWFsRGF0ZSA9IF8uY2xvbmVEZWVwKGVuZERhdGUpO1xuICAgIGFubnVhbERhdGUgPSBhbm51YWxEYXRlLmFkZCgxLCBcInllYXJzXCIpO1xuICAgIGxldCBkaWZmRGF5cyA9IGVuZERhdGUuZGlmZihzdGFydERhdGUsIFwiZGF5c1wiKTtcblxuICAgIGRpZmZEYXlzID0gTlAucGx1cyhkaWZmRGF5cywgMSk7XG4gICAgY29uc3QgYW5udWFsRGF5cyA9IGFubnVhbERhdGUuZGlmZihlbmREYXRlLCBcImRheXNcIik7XG4gICAgY29uc3QgUE9JUmF0ZSA9IE5QLmRpdmlkZShkaWZmRGF5cywgYW5udWFsRGF5cyk7XG4gICAgLy8gUE9JUmF0ZSA9IE5QLnJvdW5kKFBPSVJhdGUsIDIpO1xuICAgIHJldHVybiBQT0lSYXRlO1xuICB9O1xuXG4gIGNhbGNQcmUgPSAoZGF0YUluZGV4OiBhbnkpID0+IHtcbiAgICBjb25zdCB7IHByZW1pdW1DdXJyZW5jeSB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoUnVsZXMuaXNOdW0odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnJhdGVgKSkgJiYgUnVsZXMuaXNOdW0odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnN1bUluc3VyZWRgKSkpIHtcbiAgICAgIGNvbnN0IHN1bUluc3VyZWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0uc3VtSW5zdXJlZGApO1xuICAgICAgY29uc3QgcmF0ZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5yYXRlYCk7XG4gICAgICBsZXQgcHJlbWl1bSA9IE5QLnRpbWVzKHN1bUluc3VyZWQsIHJhdGUpO1xuICAgICAgcHJlbWl1bSA9IE5QLnJvdW5kKHByZW1pdW0sIDIpO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwocHJlbWl1bSwgYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5wcmVtaXVtYCk7XG4gICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoeyBbYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5wcmVtaXVtYF06IHByZW1pdW0gfSk7XG4gICAgICBjb25zdCBkYXRhID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKHByZW1pdW0sIHByZW1pdW1DdXJyZW5jeSk7XG4gICAgICByZXR1cm4gYCR7ZGF0YS5hbW91bnR9YDtcbiAgICB9XG4gICAgcmV0dXJuIFwiXCI7XG4gIH07XG5cbiAgY2FsY0VuZG9QcmUgPSAoZGF0YUluZGV4OiBhbnkpID0+IHtcbiAgICBjb25zdCB7IHByZW1pdW1DdXJyZW5jeSB9ID0gdGhpcy5wcm9wcztcbiAgICBpZiAoUnVsZXMuaXNOdW0odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnJhdGVgKSkgJiYgUnVsZXMuaXNOdW0odGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnN1bUluc3VyZWRgKSkpIHtcbiAgICAgIGNvbnN0IHN1bUluc3VyZWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0uc3VtSW5zdXJlZGApO1xuICAgICAgY29uc3QgcmF0ZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5yYXRlYCk7XG4gICAgICBjb25zdCBhY2NQcmVtaXVuID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LmFjY3VtdWxhdGVkUHJlbWl1bWApIHx8IDA7XG4gICAgICBjb25zdCBQT0lSYXRlID0gdGhpcy5jYWxjUE9JUmF0ZSgpO1xuICAgICAgbGV0IGNhbGNWYWx1ZSA9IE5QLnRpbWVzKHN1bUluc3VyZWQsIHJhdGUpO1xuICAgICAgY2FsY1ZhbHVlID0gTlAubWludXMoY2FsY1ZhbHVlLCBhY2NQcmVtaXVuKTtcbiAgICAgIGxldCBwcmVtaXVtID0gTlAudGltZXMoY2FsY1ZhbHVlLCBQT0lSYXRlKTtcbiAgICAgIHByZW1pdW0gPSBOUC5yb3VuZChwcmVtaXVtLCAyKTtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHByZW1pdW0sIGBiZW5lZml0cy4ke2RhdGFJbmRleH0ucHJlbWl1bWApO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW2BiZW5lZml0cy4ke2RhdGFJbmRleH0ucHJlbWl1bWBdOiBwcmVtaXVtIH0pO1xuICAgICAgY29uc3QgZGF0YSA9IFV0aWxzLmZvcm1hdEN1cnJlbmN5SW5mbyhwcmVtaXVtLCBwcmVtaXVtQ3VycmVuY3kpO1xuICAgICAgcmV0dXJuIGAke2RhdGEuYW1vdW50fWA7XG4gICAgfVxuICAgIHJldHVybiBcIlwiO1xuICB9O1xuXG4gIGZvcm1hdHRlclByaWNlOiBhbnkgPSAodmFsdWU6IGFueSkgPT4ge1xuICAgIGlmIChSdWxlcy5pc0Zsb2F0TnVtYmVyKHZhbHVlKSkge1xuICAgICAgcmV0dXJuIFV0aWxzLnRvVGhvdXNhbmRzKHZhbHVlKTtcbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9O1xuXG5cbiAgcGFyc2VyUHJpY2U6IGFueSA9ICh2YWx1ZTogYW55KSA9PiB7XG4gICAgcmV0dXJuIHZhbHVlLnJlcGxhY2UoL1xcU1xcJFxccz98KCwqKS9nLCBcIlwiKTtcbiAgfTtcblxuICBzZXRWYWx1ZVl0b04odmFsdWU6IGFueSkge1xuICAgIHJldHVybiB2YWx1ZSA9PT0gXCJZXCIgPyBcIllcIiA6IFwiTlwiO1xuICB9XG5cbiAgc2V0QWxsRWRpdGFsYmxlKCkge1xuICAgIGxldCBiZW5lZml0czogYW55W10gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0c2ApIHx8IFtdO1xuICAgIGxldCBiZW5lZml0c0xlbjogbnVtYmVyID0gYmVuZWZpdHMubGVuZ3RoO1xuICAgIGxldCBiZW5lZml0c1NlbGVjdGVkTGVuOiBudW1iZXIgPSAwO1xuICAgIGJlbmVmaXRzLmZvckVhY2goKGl0ZW06IGFueSkgPT4ge1xuICAgICAgaWYgKGl0ZW0uc2VsZWN0ZWQgPT09IFwiWVwiKSB7XG4gICAgICAgIGJlbmVmaXRzU2VsZWN0ZWRMZW4rKztcbiAgICAgIH1cbiAgICB9KTtcbiAgICBpZiAoYmVuZWZpdHNMZW4gPT09IGJlbmVmaXRzU2VsZWN0ZWRMZW4pIHtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiWVwiLCBgYmVuZWZpdHMuc2VsZWN0ZWRgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJOXCIsIGBiZW5lZml0cy5zZWxlY3RlZGApO1xuICAgIH1cbiAgfVxuXG4gIGluaXRSb3codmFsdWU6IGFueSwgZGF0YUluZGV4OiBudW1iZXIpIHtcbiAgICBpZiAodmFsdWUgPT09IFwiTlwiIHx8ICF2YWx1ZSkge1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJcIiwgYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5zdW1JbnN1cmVkYCk7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChcIlwiLCBgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnJhdGluZ01vZGVgKTtcbiAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiXCIsIGBiZW5lZml0cy4ke2RhdGFJbmRleH0ucmF0ZWApO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJcIiwgYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5wcmVtaXVtYCk7XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IGRhdGFQcm9wc0luZGV4ID0gdGhpcy5wcm9wcy5kYXRhSW5kZXg7XG4gICAgLy8gY29uc3QgZGF0YUZpeGVkID0gYG1lbWJlci5leHQuY292ZXJhZ2VzLiR7ZGF0YVByb3BzSW5kZXh9YDtcbiAgICAvLyBjb25zdCBjb3ZlcmFnZSA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGRhdGFGaXhlZCk7XG4gICAgY29uc3Qgc2lEYXRhID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKFwiXCIsIHRoaXMucHJvcHMuc2lDdXJyZW5jeSk7XG4gICAgY29uc3QgcHJlbWl1bURhdGEgPSBVdGlscy5mb3JtYXRDdXJyZW5jeUluZm8oXCJcIiwgdGhpcy5wcm9wcy5wcmVtaXVtQ3VycmVuY3kpO1xuICAgIGNvbnN0IGNvdmVyYWdlID0gdGhpcy5wcm9wcy5tb2RlbDtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsLCBpc0VuZG8gfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IGRhdGFTb3VyY2U6IGFueSA9IF8uZ2V0KGNvdmVyYWdlLCBcImJlbmVmaXRzXCIsIFtdKTtcbiAgICBkYXRhU291cmNlLmZvckVhY2goKGl0ZW06IGFueSwgaTogbnVtYmVyKSA9PiB7XG4gICAgICBpZiAoXy5pc0VtcHR5KF8uZ2V0KGl0ZW0sIFwic2VsZWN0ZWRcIikpKSB7XG4gICAgICAgIF8uc2V0KGRhdGFTb3VyY2VbaV0sIFwic2VsZWN0ZWRcIiwgXCJZXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiAoXG4gICAgICA8Q292ZXJhZ2VEaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcImNvdmVyYWdlLXRpdGxlXCJ9PlxuICAgICAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgICAgICBmb250V2VpZ2h0OiA2MDAsXG4gICAgICAgICAgfX0+XG4gICAgICAgICAgICB7Xy5nZXQoY292ZXJhZ2UsIFwiY292ZXJOYW1lXCIsIFwiXCIpfVxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8U3dpdGNoXG4gICAgICAgICAgICBzdHlsZT17eyBmbG9hdDogXCJyaWdodFwiIH19XG4gICAgICAgICAgICBjaGVja2VkPXtfLmdldChjb3ZlcmFnZSwgYHNlbGVjdGVkYCkgPT09IFwiWVwifVxuICAgICAgICAgICAgb25DaGFuZ2U9eyhjaGVja2VkKSA9PiB7XG4gICAgICAgICAgICAgIGlmIChjaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoXCJZXCIsIGBzZWxlY3RlZGApO1xuICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiTlwiLCBgc2VsZWN0ZWRgKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAge18uZ2V0KGNvdmVyYWdlLCBgc2VsZWN0ZWRgKSA9PT0gXCJZXCIgJiZcbiAgICAgICAgPD5cbiAgICAgICAgICA8VGFibGVcbiAgICAgICAgICAgIGJvcmRlcmVkXG4gICAgICAgICAgICBkYXRhU291cmNlPXtbLi4uZGF0YVNvdXJjZSwge31dfVxuICAgICAgICAgICAgcGFnaW5hdGlvbj17ZmFsc2V9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPENvbHVtbiB0aXRsZT17PGRpdiBjbGFzc05hbWU9e1wiaGVhZGVyLWNoZWNrYm94XCJ9PlxuICAgICAgICAgICAgICA8TkNoZWNrYm94IG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e18uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGBiZW5lZml0cy5zZWxlY3RlZGApID09PSBcIllcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHRoaXMuc2V0VmFsdWVZdG9OKHZhbHVlKSwgXCJiZW5lZml0cy5zZWxlY3RlZFwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBiZW5lZml0czogYW55W10gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiYmVuZWZpdHNcIikgfHwgW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBiZW5lZml0cy5mb3JFYWNoKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodGhpcy5zZXRWYWx1ZVl0b04odmFsdWUpLCBgYmVuZWZpdHMuJHtpbmRleH0uc2VsZWN0ZWRgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0Um93KHZhbHVlLCBpbmRleCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXtgYmVuZWZpdHMuc2VsZWN0ZWRgfS8+XG4gICAgICAgICAgICAgIEJFTkVGSVRcbiAgICAgICAgICAgIDwvZGl2Pn0gZGF0YUluZGV4PVwiYmVuZWZpdE5hbWVcIiBrZXk9XCJiZW5lZml0TmFtZVwiXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YUluZGV4ID09PSBkYXRhU291cmNlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxCbGFja1NwYW4+VG90YWw8L0JsYWNrU3Bhbj47XG4gICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhU291cmNlW2RhdGFJbmRleF0uc2VsZWN0ZWQgPT09IFwiTlwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gPGRpdiBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxOQ2hlY2tib3ggbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e18uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGBiZW5lZml0cy4ke2RhdGFJbmRleH0uc2VsZWN0ZWRgKSA9PT0gXCJZXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdFJvdyh2YWx1ZSwgZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRBbGxFZGl0YWxibGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXtgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnNlbGVjdGVkYH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9OQ2hlY2tib3g+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IFwiNDJweFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbkxlZnQ6IDEwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB9fT57dGV4dH1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PjtcbiAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxOQ2hlY2tib3ggbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17Xy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5zZWxlY3RlZGApID09PSBcIllcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdFJvdyh2YWx1ZSwgZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsRWRpdGFsYmxlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17YGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5zZWxlY3RlZGB9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L05DaGVja2JveD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluZUhlaWdodDogXCI0MnB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbkxlZnQ6IDEwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0+e3RleHR9Jm5ic3A7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxQb3BvdmVyIHBsYWNlbWVudD1cInRvcFwiIHRpdGxlPXt0ZXh0fSBjb250ZW50PXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPE5Gb3JtSXRlbSBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17XCJcIn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e2BiZW5lZml0cy4ke2RhdGFJbmRleH0uYmVuZWZpdERlc2NgfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPElucHV0LlRleHRBcmVhIHJvd3M9ezV9IHN0eWxlPXt7IHJlc2l6ZTogXCJub25lXCIsIHdpZHRoOiAzNjAgfX0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9ORm9ybUl0ZW0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmlnZ2VyPVwiY2xpY2tcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fSBjbGFzc05hbWU9e1wiaWNvbmZvbnQgaWNvbi13b3JrLW9uXCJ9Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Qb3BvdmVyPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlV2l0aG91dE1hcmdpbkJvdHRvbT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKjxORm9ybUl0ZW0gbW9kZWw9e21vZGVsfSovfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICAgICBmb3JtPXtmb3JtfSovfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX0qL31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgICAgbGFiZWw9e1wiXCJ9Ki99XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgICAgIHByb3BOYW1lPXtgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LmJlbmVmaXREZXNjYH0+Ki99XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyogIDxJbnB1dC5UZXh0QXJlYSByb3dzPXszfSBzdHlsZT17eyByZXNpemU6IFwibm9uZVwiIH19Lz4qL31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKjwvTkZvcm1JdGVtPiovfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+e3JlY29yZC5iZW5lZml0RGVzY308L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgIDxDb2x1bW4gd2lkdGg9ezE4MH0gY2xhc3NOYW1lPXtcImFtb3VudC1yaWdodFwifSB0aXRsZT17YFNVTSBJTlNVUkVEICgke3NpRGF0YS5jdXJyZW5jeVRleHR9KWB9XG4gICAgICAgICAgICAgICAgICAgIGRhdGFJbmRleD1cInN1bUluc3VyZWRcIiBrZXk9XCJzdW1JbnN1cmVkXCJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyPXsodGV4dCwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhSW5kZXggPT09IGRhdGFTb3VyY2UubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgdG90YWw6IGFueSA9IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhU291cmNlLmZvckVhY2goKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS5zZWxlY3RlZCA9PT0gXCJZXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3RhbCArPSBwYXJzZUZsb2F0KF8uZ2V0KGl0ZW0sIFwic3VtSW5zdXJlZFwiLCAwKSB8fCAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB7IHNpQ3VycmVuY3kgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICAgICAgICAgICAgICB0b3RhbCA9IHBhcnNlRmxvYXQodG90YWwpLnRvRml4ZWQoMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gVXRpbHMudG9UaG91c2FuZHModG90YWwpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc3QgZGF0YSA9IFV0aWxzLmZvcm1hdEN1cnJlbmN5SW5mbyh0b3RhbCwgc2lDdXJyZW5jeSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXR1cm4gPEJsYWNrU3Bhbj57ZGF0YS5hbW91bnR9PC9CbGFja1NwYW4+O1xuICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YVNvdXJjZVtkYXRhSW5kZXhdLnNlbGVjdGVkID09PSBcIk5cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8U3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8UmlnaHREaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPE5OdW1iZXIgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e1wiXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJlY2lzaW9uPXswfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0dGVyPXt0aGlzLmZvcm1hdHRlclByaWNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcnNlcj17dGhpcy5wYXJzZXJQcmljZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17YGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5zdW1JbnN1cmVkYH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9uQmx1cj17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICBpZiAoIXRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5wcmVtaXVtYCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLmNhbGNQcmUoZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChSdWxlcy5pc051bSh2YWx1ZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRvdGFsOiBhbnkgPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhU291cmNlLmZvckVhY2goKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpdGVtLnNlbGVjdGVkID09PSBcIllcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG90YWwgKz0gcGFyc2VGbG9hdChfLmdldChpdGVtLCBcInN1bUluc3VyZWRcIiwgXCIwXCIpIHx8IFwiMFwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChwYXJzZUZsb2F0KHRvdGFsKSwgXCJhZ2dyZWdhdGVkU2lcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7IFtcImFnZ3JlZ2F0ZWRTaVwiXTogcGFyc2VGbG9hdCh0b3RhbCkgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmICh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0ucmF0aW5nTW9kZWApID09PSBcIkZMQVRcIiAmJiBSdWxlcy5pc051bSh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGBiZW5lZml0cy4ke2RhdGFJbmRleH0ucHJlbWl1bWApKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIHRoaXMuY2FsY1JhdGUoZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9SaWdodERpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPjtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgIDxDb2x1bW4gd2lkdGg9ezE2MH0gY2xhc3NOYW1lPXtcImFtb3VudC1yaWdodFwifSB0aXRsZT1cIlJBVEVcIiBkYXRhSW5kZXg9XCJyYXRlXCIga2V5PVwicmF0ZVwiXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YUluZGV4ID09PSBkYXRhU291cmNlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhU291cmNlW2RhdGFJbmRleF0uc2VsZWN0ZWQgPT09IFwiTlwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxTdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxSaWdodERpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TlJhdGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e2BiZW5lZml0cy4ke2RhdGFJbmRleH0ucmF0ZWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByZWNpc2lvbj17NH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e2ZhbHNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIGNvbnN0IG1vZGVsUHJlbWl1bSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoYGJlbmVmaXRzLiR7ZGF0YUluZGV4fS5wcmVtaXVtYCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIGlmICghbW9kZWxQcmVtaXVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy5jYWxjUHJlKGRhdGFJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBvbkJsdXI9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgY29uc3QgbW9kZWxQcmVtaXVtID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChgYmVuZWZpdHMuJHtkYXRhSW5kZXh9LnByZW1pdW1gKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgaWYgKCFtb2RlbFByZW1pdW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLmNhbGNQcmUoZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L1JpZ2h0RGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdHlsZVdpdGhvdXRNYXJnaW5Cb3R0b20+O1xuICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB9fS8+XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGlzRW5kbyAmJlxuICAgICAgICAgICAgICA8Q29sdW1uIGNsYXNzTmFtZT17XCJhbW91bnQtcmlnaHRcIn0gdGl0bGU9e2BBQ0NVTVVMQVRFRCBQUkVNSVVNICgke3ByZW1pdW1EYXRhLmN1cnJlbmN5VGV4dH0pYH1cbiAgICAgICAgICAgICAgICAgICAgICBkYXRhSW5kZXg9XCJhY2N1bXVsYXRlZFByZW1pdW1cIlxuICAgICAgICAgICAgICAgICAgICAgIGtleT1cImFjY3VtdWxhdGVkUHJlbWl1bVwiXG4gICAgICAgICAgICAgICAgICAgICAgcmVuZGVyPXsodGV4dCwgcmVjb3JkOiBhbnksIGRhdGFJbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGFJbmRleCA9PT0gZGF0YVNvdXJjZS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRhdGFTb3VyY2VbZGF0YUluZGV4XS5zZWxlY3RlZCA9PT0gXCJOXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFJ1bGVzLmlzRmxvYXROdW1iZXIodGV4dCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGEgPSB0ZXh0LnRvRml4ZWQoMik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBVdGlscy50b1Rob3VzYW5kcyhkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBcIjAuMDBcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIDxDb2x1bW4gd2lkdGg9ezE4MH0gY2xhc3NOYW1lPXtcImFtb3VudC1yaWdodFwifVxuICAgICAgICAgICAgICAgICAgICB0aXRsZT17aXNFbmRvID8gYERFTFRBIFBSRU1JVU0gKCR7cHJlbWl1bURhdGEuY3VycmVuY3lUZXh0fSlgIDogYFBSRU1JVU0gKCR7cHJlbWl1bURhdGEuY3VycmVuY3lUZXh0fSlgfVxuICAgICAgICAgICAgICAgICAgICBkYXRhSW5kZXg9e2BwcmVtaXVtYH1cbiAgICAgICAgICAgICAgICAgICAga2V5PVwicHJlbWl1bVwiXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlcj17KHRleHQsIHJlY29yZDogYW55LCBkYXRhSW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YUluZGV4ID09PSBkYXRhU291cmNlLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRvdGFsOiBhbnkgPSAwO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YVNvdXJjZS5mb3JFYWNoKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0uc2VsZWN0ZWQgPT09IFwiWVwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG90YWwgKz0gcGFyc2VGbG9hdChfLmdldChpdGVtLCBcInByZW1pdW1cIiwgMCkgfHwgMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgeyBwcmVtaXVtQ3VycmVuY3kgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkYXRhID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKHRvdGFsLCBwcmVtaXVtQ3VycmVuY3kpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGAke2RhdGEuYW1vdW50fWA7XG4gICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhU291cmNlW2RhdGFJbmRleF0uc2VsZWN0ZWQgPT09IFwiTlwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPFBvcG92ZXIgY29udGVudD17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzRW5kbykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FsY0VuZG9QcmUoZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbGNQcmUoZGF0YUluZGV4KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBcInBvaW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PkNhbGN1bGF0ZTwvYT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSB0cmlnZ2VyPVwiZm9jdXNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFJpZ2h0RGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Tk51bWJlciBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmVjaXNpb249ezJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdHRlcj17dGhpcy5mb3JtYXR0ZXJQcmljZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyc2VyPXt0aGlzLnBhcnNlclByaWNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIG9uQ2hhbmdlPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gICB0aGlzLmNhbGNSYXRlKGRhdGFJbmRleCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e2BiZW5lZml0cy4ke2RhdGFJbmRleH0ucHJlbWl1bWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXshaXNFbmRvfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1JpZ2h0RGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVXaXRob3V0TWFyZ2luQm90dG9tPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L1BvcG92ZXI+KTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICA8L1RhYmxlPlxuICAgICAgICAgIDxOUHJpY2UgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgIHByZWNpc2lvbj17MH1cbiAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJBR0dSRUdBVEVEIFNJXCIpLnRoYWkoXCJBR0dSRUdBVEVEIFNJXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXtgYWdncmVnYXRlZFNpYH1cbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfS8+XG4gICAgICAgICAgPE5SYXRlXG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQTUxcIikudGhhaShcIlBNTFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBwcm9wTmFtZT17YHBtbGB9XG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiB9fVxuICAgICAgICAgICAgcHJlY2lzaW9uPXsyfVxuICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBvcmlnaW5hbFZhbHVlPXt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG1sXCIpIHx8IDF9XG4gICAgICAgICAgLz5cbiAgICAgICAgICB7Lyo8Tk51bWJlciBtb2RlbD17bW9kZWx9Ki99XG4gICAgICAgICAgey8qICAgICAgICAgZm9ybT17Zm9ybX0qL31cbiAgICAgICAgICB7LyogICAgICAgICBzaXplPVwibGFyZ2VcIiovfVxuICAgICAgICAgIHsvKiAgICAgICAgIHByZWNpc2lvbj17NH0qL31cbiAgICAgICAgICB7LyogICAgICAgICBtaW49ezB9Ki99XG4gICAgICAgICAgey8qICAgICAgICAgbWF4PXsxfSovfVxuICAgICAgICAgIHsvKiAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlBNTCAoJSlcIikudGhhaShcIlBNTCAoJSlcIikuZ2V0TWVzc2FnZSgpfSovfVxuICAgICAgICAgIHsvKiAgICAgICAgIHByb3BOYW1lPXtgcG1sYH0qL31cbiAgICAgICAgICB7LyogICAgICAgICBmb3JtYXR0ZXI9eyh2YWx1ZTogYW55KSA9PiB7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICBpZiAoUnVsZXMuaXNOdW0odmFsdWUpKSB7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICAgIHJldHVybiBgJHtOUC50aW1lcyh2YWx1ZSwgMTAwKX1gOyovfVxuICAgICAgICAgIHsvKiAgICAgICAgICAgfSBlbHNlIGlmICh2YWx1ZS5pbmRleE9mKFwiLlwiKSAhPSAtMSkgeyovfVxuICAgICAgICAgIHsvKiAgICAgICAgICAgICByZXR1cm4gdmFsdWU7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICB9IGVsc2UgeyovfVxuICAgICAgICAgIHsvKiAgICAgICAgICAgICByZXR1cm4gXCIwXCI7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICB9Ki99XG4gICAgICAgICAgey8qICAgICAgICAgfX0qL31cbiAgICAgICAgICB7LyogICAgICAgICBwYXJzZXI9eyh2YWx1ZTogYW55KSA9PiB7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICBpZiAoUnVsZXMuaXNOdW0odmFsdWUpKSB7Ki99XG4gICAgICAgICAgey8qICAgICAgICAgICAgIHJldHVybiBgJHtOUC5kaXZpZGUodmFsdWUsIDEwMCl9YDsqL31cbiAgICAgICAgICB7LyogICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaW5kZXhPZihcIi5cIikgIT0gLTEpIHsqL31cbiAgICAgICAgICB7LyogICAgICAgICAgICAgcmV0dXJuIHZhbHVlOyovfVxuICAgICAgICAgIHsvKiAgICAgICAgICAgfSBlbHNlIHsqL31cbiAgICAgICAgICB7LyogICAgICAgICAgICAgcmV0dXJuIFwiMFwiOyovfVxuICAgICAgICAgIHsvKiAgICAgICAgICAgfSovfVxuICAgICAgICAgIHsvKiAgICAgICAgIH19Ki99XG4gICAgICAgICAgey8qICAgICAgICAgcmVxdWlyZWQ9e3RydWV9Lz4qL31cbiAgICAgICAgICA8TkZvcm1JdGVtIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJFWENFU1NcIikudGhhaShcIkVYQ0VTU1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17YGV4Y2Vzc2B9PlxuICAgICAgICAgICAgPElucHV0LlRleHRBcmVhIHJvd3M9ezN9IHN0eWxlPXt7IHJlc2l6ZTogXCJub25lXCIgfX0vPlxuICAgICAgICAgIDwvTkZvcm1JdGVtPlxuICAgICAgICA8Lz5cbiAgICAgICAgfVxuXG4gICAgICA8L0NvdmVyYWdlRGl2PlxuICAgICk7XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBY0E7QUE0QkE7QUFJQTtBQUFBO0FBQ0E7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBc0NBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQTFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7O0FBd0VBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFPQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFEQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQWxHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtR0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUE1QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBK0JBO0FBQ0E7QUFwREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBcURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXdCQTtBQUVBO0FBbkNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXNDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQWZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQVJBO0FBU0E7QUFDQTtBQUNBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7QUFDQTtBQW5EQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBc0NBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7OztBQTllQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/coverage.tsx
