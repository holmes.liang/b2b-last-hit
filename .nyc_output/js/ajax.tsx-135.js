__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./consts */ "./src/common/consts.tsx");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./src/common/utils.tsx");
/* harmony import */ var _envs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./envs */ "./src/common/envs.tsx");
/* harmony import */ var _language__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./language */ "./src/common/language.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_ajax_before__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/ajax-before */ "./src/common/ajax-before.ts");
/* harmony import */ var _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../app/desk/component/spin */ "./src/app/desk/component/spin.tsx");
/* harmony import */ var _desk_component_session_login__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk-component/session-login */ "./src/app/desk/component/session-login.tsx");
/* harmony import */ var _common_sys_log__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common/sys-log */ "./src/common/sys-log/index.tsx");














var Ajax =
/*#__PURE__*/
function () {
  function Ajax() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Ajax);

    this.rejectDe = lodash__WEBPACK_IMPORTED_MODULE_3___default.a.debounce(this.rejectDe, 800);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Ajax, [{
    key: "rejectDe",
    value: function rejectDe(ex, failHandler, isError, options) {
      Ajax.rejectWithError(ex, failHandler);

      if (isError) {
        if (!options.silent) {
          this.errorOther("Something is wrong. Please come back and retry after a few minutes.");
        }
      }
    }
  }, {
    key: "all",
    value: function all(promises) {
      return Promise.all(promises);
    }
  }, {
    key: "put",
    value: function put(url, data, options) {
      return this.ajax(url, data, Object.assign({}, options, {
        method: "PUT"
      }));
    }
  }, {
    key: "post",
    value: function post(url, data, options, callBack) {
      return this.ajax(url, data, Object.assign({}, options, {
        method: "POST"
      }), callBack);
    }
  }, {
    key: "patch",
    value: function patch(url, data, options) {
      return this.ajax(url, data, Object.assign({}, options, {
        method: "PATCH"
      }));
    }
  }, {
    key: "delete",
    value: function _delete(url, data, options) {
      return this.ajax(url, data, Object.assign({}, options, {
        method: "DELETE"
      }));
    }
  }, {
    key: "get",
    value: function get(url, data, options, callBack) {
      return this.ajax(url, data, Object.assign({}, options, {
        method: "GET"
      }), callBack);
    }
  }, {
    key: "ajax",
    value: function ajax(url, data, options, callBack) {
      var _this2 = this;

      var headers = options.headers || {};
      var isFileUpload = false;

      var _this = this;

      if (headers["Content-Type"] == null) {
        headers["Content-Type"] = "application/json";
      } else if (headers["Content-Type"] === false) {
        delete headers["Content-Type"];
        isFileUpload = true;
      }

      headers = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, headers, {}, Object(_common_ajax_before__WEBPACK_IMPORTED_MODULE_9__["getClientInfo"])(isFileUpload));
      this.appendAuth(headers, options.ignoreAuth);
      var opts = {
        method: (options.method || "GET").toUpperCase(),
        headers: headers,
        credentials: options.credentials || "same-origin",
        mode: options.mode || "cors",
        redirect: options.redirect || "follow",
        cache: options.cache || "default",
        body: ""
      };
      var responseType = options.dataType || "json";

      if (opts.method === "GET") {
        if (data) {
          url += "?".concat(this.generateQueryString(data));
        }

        delete opts.body;
      } else if (_utils__WEBPACK_IMPORTED_MODULE_5__["default"].isFormData(data)) {
        opts.body = data;
      } else {
        opts.body = JSON.stringify(data || {});
      }

      var isLoading = options && options.loading;
      var wrapClassName = (options || {}).wrapClassName || "";
      var opacityNum = (options || {}).opacityNum || 0;

      if (isLoading) {
        _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinShow(wrapClassName, opacityNum);
      }

      return new Promise(function (resolve, reject) {
        fetch(_this2.appendLang(_this2.getServiceLocation(url)), opts).then(function (response) {
          _this2.error401(response.status);

          if (!options.silent) {
            _this2.error404(response.status);

            _this2.error500(response.status);

            _this2.error502(response.status);
          }

          if (response.ok) {
            Ajax.handleResponse(response, responseType, resolve, reject, options, callBack, _this);
          } else {
            Ajax.handleResponse(response, responseType, reject, reject, options, callBack, _this);
          }
        }).catch(function (e) {
          if (url.includes("/syslogs/write")) {
            //todo
            Object(_common_sys_log__WEBPACK_IMPORTED_MODULE_12__["writeSyslog"])({
              traceId: lodash__WEBPACK_IMPORTED_MODULE_3___default.a.get(opts, "headers.x-insmate-traceid"),
              logType: "Ajax Log",
              logLevel: "INFO",
              message: lodash__WEBPACK_IMPORTED_MODULE_3___default.a.toString(e)
            });
          } // Ajax.rejectWithError(e, reject);


          _this2.rejectDe(e, reject, true, options); // if (!options.silent) {
          //   console.log("errorother");
          //   this.errorOther("Something is wrong. Please come back and retry after a few minutes.");
          // }


          if (isLoading) {
            _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinClose();
          }
        });
      });
    }
  }, {
    key: "error404",
    value: function error404(status) {
      if (status && status === 404) {
        antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
          message: "404"
        });
      }
    }
  }, {
    key: "error401",
    value: function error401(status) {
      if (status && status === 401) {
        _desk_component_session_login__WEBPACK_IMPORTED_MODULE_11__["default"].showConfirm(); // 10秒之后自动跳入登录页面

        setTimeout(function () {
          _desk_component_session_login__WEBPACK_IMPORTED_MODULE_11__["default"].routerSign();
        }, 10000);
      }
    }
  }, {
    key: "error500",
    value: function error500(status) {
      if (status && status === 500) {
        antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
          message: "Network error"
        });
      }
    }
  }, {
    key: "error502",
    value: function error502(status) {
      if (status && status === 502) {
        antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
          message: "Connection timeout"
        });
      }
    }
  }, {
    key: "errorOther",
    value: function errorOther(msg) {
      antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
        message: msg || "error"
      });
    }
  }, {
    key: "generateQueryString",
    value: function generateQueryString(obj) {
      return _utils__WEBPACK_IMPORTED_MODULE_5__["default"].toQueryString(obj);
    }
  }, {
    key: "getServiceLocation",
    value: function getServiceLocation(relativePath) {
      if (relativePath && (relativePath.startsWith("https://") || relativePath.startsWith("http://"))) {
        return relativePath;
      }

      var url = window.location;
      var port = url.port;

      if (true) {
        port = ":".concat("4000");
      } else {}

      var hostname = url.hostname;

      if (true) {
        hostname = "http://localhost";
      }

      var context = Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_AJAX_SERVER_CONTEXT || ""; // let location = `${url.protocol}//${hostname}${port}${context}`;

      var location = "".concat(hostname).concat(port).concat(context);

      if (!location.startsWith("http://") && !location.startsWith("https://")) {
        location = "".concat(url.protocol, "//").concat(hostname).concat(port).concat(context);
      }

      if (relativePath) {
        return location + relativePath;
      } else {
        return location;
      }
    }
  }, {
    key: "appendLang",
    value: function appendLang(url) {
      if (!url) {
        return url;
      }

      var joiner = url.indexOf("?") === -1 ? "?" : "&";
      return "".concat(url).concat(joiner, "lang=").concat(_language__WEBPACK_IMPORTED_MODULE_7__["default"].language);
    }
  }, {
    key: "appendAuth",
    value: function appendAuth(headers) {
      var ignoreAuth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (ignoreAuth === true) {
        return;
      }

      if (!headers[_consts__WEBPACK_IMPORTED_MODULE_4__["default"].AUTH_KEY]) {
        headers[_consts__WEBPACK_IMPORTED_MODULE_4__["default"].AUTH_KEY] = _envs__WEBPACK_IMPORTED_MODULE_6__["default"].getAuth() || "";
      }
    }
  }, {
    key: "appendAuthToUrl",
    value: function appendAuthToUrl(url) {
      var apiUrl = this.getServiceLocation(url);
      var accessKey = _envs__WEBPACK_IMPORTED_MODULE_6__["default"].getAuth();
      var separator = apiUrl.indexOf("?") === -1 ? "?" : "&";
      return "".concat(apiUrl).concat(separator, "access_token=").concat(encodeURIComponent(accessKey));
    }
  }]);

  return Ajax;
}();

Ajax.buildResponseHeaders = function (response) {
  var headers = {};
  response.headers.forEach(function (value, key) {
    headers[key] = value;
  });
  return headers;
};

Ajax.buildResponseData = function (response, body) {
  return {
    headers: Ajax.buildResponseHeaders(response),
    body: body,
    status: response.status,
    statusText: response.statusText
  };
};

Ajax.handleResponse = function (response, responseType, successHandler, failHandler, options, callBack, _this) {
  var contentType = response.headers.get("content-type") || "";
  var isLoading = options && options.loading;
  var isSilent = options && options.silent;

  if (contentType.includes("json")) {
    responseType = "json";
  } else if (contentType.includes("xml") || contentType.includes("text") || contentType.includes("html")) {
    responseType = "text";
  }

  var extractor = null;

  switch (responseType) {
    case "text":
      extractor = Response.prototype.text;
      break;

    case "blob":
      extractor = Response.prototype.blob;
      break;

    default:
      // json
      extractor = Response.prototype.json;
      break;
  }

  extractor.call(response).then(function (data) {
    if (!["text", "blob"].includes(responseType)) {
      //TODO
      if (!data.respCode) {
        data = {
          respCode: "0000",
          respData: data
        };
      }

      var _data = data,
          respCode = _data.respCode,
          respMessage = _data.respMessage;

      if (respCode === "0000") {
        successHandler(Ajax.buildResponseData(response, data));

        if (isLoading) {
          _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinClose();
        }
      } else {
        if (!isSilent) {
          antd__WEBPACK_IMPORTED_MODULE_8__["notification"].error({
            message: respCode,
            description: respMessage
          });
        }

        if (isSilent) {
          callBack && callBack(respMessage);
        }

        failHandler({
          headers: {},
          body: data,
          status: 200,
          statusText: "System error occurred."
        });

        if (isLoading) {
          _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinClose();
        }
      }
    } else {
      successHandler(Ajax.buildResponseData(response, data));

      if (isLoading) {
        _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinClose();
      }
    }
  }).catch(function (ex) {
    // Ajax.rejectWithError(ex, failHandler);
    _this && _this.rejectDe(ex, failHandler);

    if (isLoading) {
      _app_desk_component_spin__WEBPACK_IMPORTED_MODULE_10__["default"].spinClose();
    }
  });
};

Ajax.rejectWithError = function (error, reject) {
  reject({
    headers: {},
    body: {
      respCode: _consts__WEBPACK_IMPORTED_MODULE_4__["default"].FETCH_ERROR,
      respMessage: "".concat(error.name, ": ").concat(error.message),
      respData: null
    },
    status: 0,
    statusText: "Something is wrong. Please come back and retry after a few minutes.",
    // keep original error here
    error: error
  });
};

/* harmony default export */ __webpack_exports__["default"] = (new Ajax());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2FqYXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL2FqYXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmltcG9ydCB7IEFqYXhIYW5kbGVyLCBBamF4SGVhZGVycywgQWpheE9wdGlvbnMsIEFqYXhSZXNwb25zZSB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCBDb25zdHMgZnJvbSBcIi4vY29uc3RzXCI7XG5pbXBvcnQgVXRpbHMgZnJvbSBcIi4vdXRpbHNcIjtcbmltcG9ydCBFbnZzIGZyb20gXCIuL2VudnNcIjtcbmltcG9ydCBMYW5ndWFnZSBmcm9tIFwiLi9sYW5ndWFnZVwiO1xuaW1wb3J0IHsgbm90aWZpY2F0aW9uIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IGdldENsaWVudEluZm8gfSBmcm9tIFwiQGNvbW1vbi9hamF4LWJlZm9yZVwiO1xuaW1wb3J0IHNwaW4gZnJvbSBcIi4uL2FwcC9kZXNrL2NvbXBvbmVudC9zcGluXCI7XG5pbXBvcnQgU2Vzc2lvbkxvZ2luIGZyb20gXCJAZGVzay1jb21wb25lbnQvc2Vzc2lvbi1sb2dpblwiO1xuaW1wb3J0IHsgd3JpdGVTeXNsb2cgfSBmcm9tIFwiQGNvbW1vbi9zeXMtbG9nXCI7XG5cblxuY2xhc3MgQWpheCB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucmVqZWN0RGUgPSBfLmRlYm91bmNlKHRoaXMucmVqZWN0RGUsIDgwMCk7XG4gIH1cblxuICBwcml2YXRlIHN0YXRpYyBidWlsZFJlc3BvbnNlSGVhZGVycyA9IChyZXNwb25zZTogUmVzcG9uc2UpOiBBamF4SGVhZGVycyA9PiB7XG4gICAgY29uc3QgaGVhZGVyczogQWpheEhlYWRlcnMgPSB7fTtcbiAgICByZXNwb25zZS5oZWFkZXJzLmZvckVhY2goKHZhbHVlOiBzdHJpbmcsIGtleTogc3RyaW5nKSA9PiB7XG4gICAgICBoZWFkZXJzW2tleV0gPSB2YWx1ZTtcbiAgICB9KTtcbiAgICByZXR1cm4gaGVhZGVycztcbiAgfTtcblxuICBwcml2YXRlIHN0YXRpYyBidWlsZFJlc3BvbnNlRGF0YSA9IChyZXNwb25zZTogUmVzcG9uc2UsIGJvZHk6IGFueSk6IEFqYXhSZXNwb25zZSA9PiB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGhlYWRlcnM6IEFqYXguYnVpbGRSZXNwb25zZUhlYWRlcnMocmVzcG9uc2UpLFxuICAgICAgYm9keTogYm9keSxcbiAgICAgIHN0YXR1czogcmVzcG9uc2Uuc3RhdHVzLFxuICAgICAgc3RhdHVzVGV4dDogcmVzcG9uc2Uuc3RhdHVzVGV4dCxcbiAgICB9O1xuICB9O1xuXG4gIHByaXZhdGUgc3RhdGljIGhhbmRsZVJlc3BvbnNlID0gKFxuICAgIHJlc3BvbnNlOiBSZXNwb25zZSxcbiAgICByZXNwb25zZVR5cGU6IHN0cmluZyB8IG51bGwsXG4gICAgc3VjY2Vzc0hhbmRsZXI6IEFqYXhIYW5kbGVyLFxuICAgIGZhaWxIYW5kbGVyOiBBamF4SGFuZGxlcixcbiAgICBvcHRpb25zPzogYW55LFxuICAgIGNhbGxCYWNrPzogYW55LFxuICAgIF90aGlzPzogYW55LFxuICApOiB2b2lkID0+IHtcbiAgICBjb25zdCBjb250ZW50VHlwZSA9IHJlc3BvbnNlLmhlYWRlcnMuZ2V0KFwiY29udGVudC10eXBlXCIpIHx8IFwiXCI7XG4gICAgY29uc3QgaXNMb2FkaW5nID0gb3B0aW9ucyAmJiBvcHRpb25zLmxvYWRpbmc7XG4gICAgY29uc3QgaXNTaWxlbnQgPSBvcHRpb25zICYmIG9wdGlvbnMuc2lsZW50O1xuICAgIGlmIChjb250ZW50VHlwZS5pbmNsdWRlcyhcImpzb25cIikpIHtcbiAgICAgIHJlc3BvbnNlVHlwZSA9IFwianNvblwiO1xuICAgIH0gZWxzZSBpZiAoY29udGVudFR5cGUuaW5jbHVkZXMoXCJ4bWxcIikgfHwgY29udGVudFR5cGUuaW5jbHVkZXMoXCJ0ZXh0XCIpIHx8IGNvbnRlbnRUeXBlLmluY2x1ZGVzKFwiaHRtbFwiKSkge1xuICAgICAgcmVzcG9uc2VUeXBlID0gXCJ0ZXh0XCI7XG4gICAgfVxuXG4gICAgbGV0IGV4dHJhY3RvciA9IG51bGw7XG4gICAgc3dpdGNoIChyZXNwb25zZVR5cGUpIHtcbiAgICAgIGNhc2UgXCJ0ZXh0XCI6XG4gICAgICAgIGV4dHJhY3RvciA9IFJlc3BvbnNlLnByb3RvdHlwZS50ZXh0O1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgXCJibG9iXCI6XG4gICAgICAgIGV4dHJhY3RvciA9IFJlc3BvbnNlLnByb3RvdHlwZS5ibG9iO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIC8vIGpzb25cbiAgICAgICAgZXh0cmFjdG9yID0gUmVzcG9uc2UucHJvdG90eXBlLmpzb247XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICBleHRyYWN0b3JcbiAgICAgIC5jYWxsKHJlc3BvbnNlKVxuICAgICAgLnRoZW4oZGF0YSA9PiB7XG4gICAgICAgIGlmICghW1widGV4dFwiLCBcImJsb2JcIl0uaW5jbHVkZXMocmVzcG9uc2VUeXBlISkpIHtcbiAgICAgICAgICAvL1RPRE9cbiAgICAgICAgICBpZiAoIWRhdGEucmVzcENvZGUpIHtcbiAgICAgICAgICAgIGRhdGEgPSB7XG4gICAgICAgICAgICAgIHJlc3BDb2RlOiBcIjAwMDBcIixcbiAgICAgICAgICAgICAgcmVzcERhdGE6IGRhdGEsXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnN0IHsgcmVzcENvZGUsIHJlc3BNZXNzYWdlIH0gPSBkYXRhO1xuICAgICAgICAgIGlmIChyZXNwQ29kZSA9PT0gXCIwMDAwXCIpIHtcbiAgICAgICAgICAgIHN1Y2Nlc3NIYW5kbGVyKEFqYXguYnVpbGRSZXNwb25zZURhdGEocmVzcG9uc2UsIGRhdGEpKTtcbiAgICAgICAgICAgIGlmIChpc0xvYWRpbmcpIHtcbiAgICAgICAgICAgICAgc3Bpbi5zcGluQ2xvc2UoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKCFpc1NpbGVudCkge1xuICAgICAgICAgICAgICBub3RpZmljYXRpb24uZXJyb3Ioe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHJlc3BDb2RlLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiByZXNwTWVzc2FnZSxcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoaXNTaWxlbnQpIHtcbiAgICAgICAgICAgICAgY2FsbEJhY2sgJiYgY2FsbEJhY2socmVzcE1lc3NhZ2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZmFpbEhhbmRsZXIoe1xuICAgICAgICAgICAgICBoZWFkZXJzOiB7fSxcbiAgICAgICAgICAgICAgYm9keTogZGF0YSxcbiAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXG4gICAgICAgICAgICAgIHN0YXR1c1RleHQ6IFwiU3lzdGVtIGVycm9yIG9jY3VycmVkLlwiLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAoaXNMb2FkaW5nKSB7XG4gICAgICAgICAgICAgIHNwaW4uc3BpbkNsb3NlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN1Y2Nlc3NIYW5kbGVyKEFqYXguYnVpbGRSZXNwb25zZURhdGEocmVzcG9uc2UsIGRhdGEpKTtcbiAgICAgICAgICBpZiAoaXNMb2FkaW5nKSB7XG4gICAgICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5jYXRjaChleCA9PiB7XG4gICAgICAgIC8vIEFqYXgucmVqZWN0V2l0aEVycm9yKGV4LCBmYWlsSGFuZGxlcik7XG4gICAgICAgIF90aGlzICYmIF90aGlzLnJlamVjdERlKGV4LCBmYWlsSGFuZGxlcik7XG4gICAgICAgIGlmIChpc0xvYWRpbmcpIHtcbiAgICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgfTtcblxuICByZWplY3REZShleDogRXJyb3IsIGZhaWxIYW5kbGVyOiBBamF4SGFuZGxlciwgaXNFcnJvcj86IGJvb2xlYW4sIG9wdGlvbnM/OiBhbnkpIHtcbiAgICBBamF4LnJlamVjdFdpdGhFcnJvcihleCwgZmFpbEhhbmRsZXIpO1xuICAgIGlmIChpc0Vycm9yKSB7XG4gICAgICBpZiAoIW9wdGlvbnMuc2lsZW50KSB7XG4gICAgICAgIHRoaXMuZXJyb3JPdGhlcihcIlNvbWV0aGluZyBpcyB3cm9uZy4gUGxlYXNlIGNvbWUgYmFjayBhbmQgcmV0cnkgYWZ0ZXIgYSBmZXcgbWludXRlcy5cIik7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgcmVqZWN0V2l0aEVycm9yID0gKGVycm9yOiBFcnJvciwgcmVqZWN0OiBBamF4SGFuZGxlcik6IHZvaWQgPT4ge1xuICAgIHJlamVjdCh7XG4gICAgICBoZWFkZXJzOiB7fSxcbiAgICAgIGJvZHk6IHtcbiAgICAgICAgcmVzcENvZGU6IENvbnN0cy5GRVRDSF9FUlJPUixcbiAgICAgICAgcmVzcE1lc3NhZ2U6IGAke2Vycm9yLm5hbWV9OiAke2Vycm9yLm1lc3NhZ2V9YCxcbiAgICAgICAgcmVzcERhdGE6IG51bGwsXG4gICAgICB9LFxuICAgICAgc3RhdHVzOiAwLFxuICAgICAgc3RhdHVzVGV4dDogXCJTb21ldGhpbmcgaXMgd3JvbmcuIFBsZWFzZSBjb21lIGJhY2sgYW5kIHJldHJ5IGFmdGVyIGEgZmV3IG1pbnV0ZXMuXCIsXG4gICAgICAvLyBrZWVwIG9yaWdpbmFsIGVycm9yIGhlcmVcbiAgICAgIGVycm9yOiBlcnJvcixcbiAgICB9KTtcbiAgfTtcblxuICBhbGw8VD4ocHJvbWlzZXM6IChUIHwgUHJvbWlzZUxpa2U8VD4pW10pOiBQcm9taXNlPFRbXT4ge1xuICAgIHJldHVybiBQcm9taXNlLmFsbChwcm9taXNlcyk7XG4gIH1cblxuICBwdXQodXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIG9wdGlvbnM/OiBBamF4T3B0aW9ucyk6IFByb21pc2U8QWpheFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuIHRoaXMuYWpheCh1cmwsIGRhdGEsIE9iamVjdC5hc3NpZ24oe30sIG9wdGlvbnMsIHsgbWV0aG9kOiBcIlBVVFwiIH0pKTtcbiAgfVxuXG4gIHBvc3QodXJsOiBzdHJpbmcsIGRhdGE/OiBhbnksIG9wdGlvbnM/OiBBamF4T3B0aW9ucywgY2FsbEJhY2s/OiBhbnkpOiBQcm9taXNlPEFqYXhSZXNwb25zZT4ge1xuICAgIHJldHVybiB0aGlzLmFqYXgodXJsLCBkYXRhLCBPYmplY3QuYXNzaWduKHt9LCBvcHRpb25zLCB7IG1ldGhvZDogXCJQT1NUXCIgfSksIGNhbGxCYWNrKTtcbiAgfVxuXG4gIHBhdGNoKHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBvcHRpb25zPzogQWpheE9wdGlvbnMpOiBQcm9taXNlPEFqYXhSZXNwb25zZT4ge1xuICAgIHJldHVybiB0aGlzLmFqYXgodXJsLCBkYXRhLCBPYmplY3QuYXNzaWduKHt9LCBvcHRpb25zLCB7IG1ldGhvZDogXCJQQVRDSFwiIH0pKTtcbiAgfVxuXG4gIGRlbGV0ZSh1cmw6IHN0cmluZywgZGF0YT86IGFueSwgb3B0aW9ucz86IEFqYXhPcHRpb25zKTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICByZXR1cm4gdGhpcy5hamF4KHVybCwgZGF0YSwgT2JqZWN0LmFzc2lnbih7fSwgb3B0aW9ucywgeyBtZXRob2Q6IFwiREVMRVRFXCIgfSkpO1xuICB9XG5cbiAgZ2V0KHVybDogc3RyaW5nLCBkYXRhPzogYW55LCBvcHRpb25zPzogQWpheE9wdGlvbnMsIGNhbGxCYWNrPzogYW55KTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICByZXR1cm4gdGhpcy5hamF4KHVybCwgZGF0YSwgT2JqZWN0LmFzc2lnbih7fSwgb3B0aW9ucywgeyBtZXRob2Q6IFwiR0VUXCIgfSksIGNhbGxCYWNrKTtcbiAgfVxuXG4gIGFqYXgodXJsOiBzdHJpbmcsIGRhdGE6IGFueSwgb3B0aW9uczogQWpheE9wdGlvbnMsIGNhbGxCYWNrPzogYW55KTogUHJvbWlzZTxBamF4UmVzcG9uc2U+IHtcbiAgICBsZXQgaGVhZGVycyA9IG9wdGlvbnMuaGVhZGVycyB8fCB7fTtcbiAgICBsZXQgaXNGaWxlVXBsb2FkOiBib29sZWFuID0gZmFsc2U7XG4gICAgbGV0IF90aGlzID0gdGhpcztcbiAgICBpZiAoaGVhZGVyc1tcIkNvbnRlbnQtVHlwZVwiXSA9PSBudWxsKSB7XG4gICAgICBoZWFkZXJzW1wiQ29udGVudC1UeXBlXCJdID0gXCJhcHBsaWNhdGlvbi9qc29uXCI7XG4gICAgfSBlbHNlIGlmIChoZWFkZXJzW1wiQ29udGVudC1UeXBlXCJdID09PSBmYWxzZSkge1xuICAgICAgZGVsZXRlIGhlYWRlcnNbXCJDb250ZW50LVR5cGVcIl07XG4gICAgICBpc0ZpbGVVcGxvYWQgPSB0cnVlO1xuICAgIH1cbiAgICBoZWFkZXJzID0ge1xuICAgICAgLi4uaGVhZGVycyxcbiAgICAgIC4uLmdldENsaWVudEluZm8oaXNGaWxlVXBsb2FkKSxcbiAgICB9O1xuICAgIHRoaXMuYXBwZW5kQXV0aChoZWFkZXJzLCBvcHRpb25zLmlnbm9yZUF1dGgpO1xuICAgIGNvbnN0IG9wdHMgPSB7XG4gICAgICBtZXRob2Q6IChvcHRpb25zLm1ldGhvZCB8fCBcIkdFVFwiKS50b1VwcGVyQ2FzZSgpLFxuICAgICAgaGVhZGVyczogaGVhZGVycyxcbiAgICAgIGNyZWRlbnRpYWxzOiBvcHRpb25zLmNyZWRlbnRpYWxzIHx8IFwic2FtZS1vcmlnaW5cIixcbiAgICAgIG1vZGU6IG9wdGlvbnMubW9kZSB8fCBcImNvcnNcIixcbiAgICAgIHJlZGlyZWN0OiBvcHRpb25zLnJlZGlyZWN0IHx8IFwiZm9sbG93XCIsXG4gICAgICBjYWNoZTogb3B0aW9ucy5jYWNoZSB8fCBcImRlZmF1bHRcIixcbiAgICAgIGJvZHk6IFwiXCIgYXMgYW55LFxuICAgIH07XG5cbiAgICBjb25zdCByZXNwb25zZVR5cGUgPSBvcHRpb25zLmRhdGFUeXBlIHx8IFwianNvblwiO1xuICAgIGlmIChvcHRzLm1ldGhvZCA9PT0gXCJHRVRcIikge1xuICAgICAgaWYgKGRhdGEpIHtcbiAgICAgICAgdXJsICs9IGA/JHt0aGlzLmdlbmVyYXRlUXVlcnlTdHJpbmcoZGF0YSl9YDtcbiAgICAgIH1cbiAgICAgIGRlbGV0ZSBvcHRzLmJvZHk7XG4gICAgfSBlbHNlIGlmIChVdGlscy5pc0Zvcm1EYXRhKGRhdGEpKSB7XG4gICAgICBvcHRzLmJvZHkgPSBkYXRhO1xuICAgIH0gZWxzZSB7XG4gICAgICBvcHRzLmJvZHkgPSBKU09OLnN0cmluZ2lmeShkYXRhIHx8IHt9KTtcbiAgICB9XG4gICAgY29uc3QgaXNMb2FkaW5nID0gb3B0aW9ucyAmJiBvcHRpb25zLmxvYWRpbmc7XG4gICAgY29uc3Qgd3JhcENsYXNzTmFtZSA9ICgob3B0aW9ucyB8fCB7fSkgYXMgYW55KS53cmFwQ2xhc3NOYW1lIHx8IFwiXCI7XG4gICAgY29uc3Qgb3BhY2l0eU51bSA9ICgob3B0aW9ucyB8fCB7fSkgYXMgYW55KS5vcGFjaXR5TnVtIHx8IDA7XG4gICAgaWYgKGlzTG9hZGluZykge1xuICAgICAgc3Bpbi5zcGluU2hvdyh3cmFwQ2xhc3NOYW1lLCBvcGFjaXR5TnVtKTtcbiAgICB9XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGZldGNoKHRoaXMuYXBwZW5kTGFuZyh0aGlzLmdldFNlcnZpY2VMb2NhdGlvbih1cmwpKSwgb3B0cyBhcyBhbnkpXG4gICAgICAgIC50aGVuKChyZXNwb25zZTogUmVzcG9uc2UpID0+IHtcbiAgICAgICAgICB0aGlzLmVycm9yNDAxKHJlc3BvbnNlLnN0YXR1cyk7XG4gICAgICAgICAgaWYgKCFvcHRpb25zLnNpbGVudCkge1xuICAgICAgICAgICAgdGhpcy5lcnJvcjQwNChyZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgICAgdGhpcy5lcnJvcjUwMChyZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgICAgdGhpcy5lcnJvcjUwMihyZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChyZXNwb25zZS5vaykge1xuICAgICAgICAgICAgQWpheC5oYW5kbGVSZXNwb25zZShyZXNwb25zZSwgcmVzcG9uc2VUeXBlLCByZXNvbHZlLCByZWplY3QsIG9wdGlvbnMsIGNhbGxCYWNrLCBfdGhpcyk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIEFqYXguaGFuZGxlUmVzcG9uc2UocmVzcG9uc2UsIHJlc3BvbnNlVHlwZSwgcmVqZWN0LCByZWplY3QsIG9wdGlvbnMsIGNhbGxCYWNrLCBfdGhpcyk7XG4gICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGU6IEVycm9yKSA9PiB7XG4gICAgICAgICAgaWYgKHVybC5pbmNsdWRlcyhcIi9zeXNsb2dzL3dyaXRlXCIpKSB7XG4gICAgICAgICAgICAvL3RvZG9cbiAgICAgICAgICAgIHdyaXRlU3lzbG9nKHtcbiAgICAgICAgICAgICAgdHJhY2VJZDogXy5nZXQob3B0cywgXCJoZWFkZXJzLngtaW5zbWF0ZS10cmFjZWlkXCIpLFxuICAgICAgICAgICAgICBsb2dUeXBlOiBcIkFqYXggTG9nXCIsXG4gICAgICAgICAgICAgIGxvZ0xldmVsOiBcIklORk9cIixcbiAgICAgICAgICAgICAgbWVzc2FnZTogXy50b1N0cmluZyhlKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBBamF4LnJlamVjdFdpdGhFcnJvcihlLCByZWplY3QpO1xuICAgICAgICAgIHRoaXMucmVqZWN0RGUoZSwgcmVqZWN0LCB0cnVlLCBvcHRpb25zKTtcbiAgICAgICAgICAvLyBpZiAoIW9wdGlvbnMuc2lsZW50KSB7XG4gICAgICAgICAgLy8gICBjb25zb2xlLmxvZyhcImVycm9yb3RoZXJcIik7XG4gICAgICAgICAgLy8gICB0aGlzLmVycm9yT3RoZXIoXCJTb21ldGhpbmcgaXMgd3JvbmcuIFBsZWFzZSBjb21lIGJhY2sgYW5kIHJldHJ5IGFmdGVyIGEgZmV3IG1pbnV0ZXMuXCIpO1xuICAgICAgICAgIC8vIH1cbiAgICAgICAgICBpZiAoaXNMb2FkaW5nKSB7XG4gICAgICAgICAgICBzcGluLnNwaW5DbG9zZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGVycm9yNDA0KHN0YXR1czogbnVtYmVyKSB7XG4gICAgaWYgKHN0YXR1cyAmJiBzdGF0dXMgPT09IDQwNCkge1xuICAgICAgbm90aWZpY2F0aW9uLmVycm9yKHtcbiAgICAgICAgbWVzc2FnZTogXCI0MDRcIixcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgZXJyb3I0MDEoc3RhdHVzOiBudW1iZXIpIHtcbiAgICBpZiAoc3RhdHVzICYmIHN0YXR1cyA9PT0gNDAxKSB7XG4gICAgICBTZXNzaW9uTG9naW4uc2hvd0NvbmZpcm0oKTtcbiAgICAgIC8vIDEw56eS5LmL5ZCO6Ieq5Yqo6Lez5YWl55m75b2V6aG16Z2iXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgU2Vzc2lvbkxvZ2luLnJvdXRlclNpZ24oKTtcbiAgICAgIH0sIDEwMDAwKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGVycm9yNTAwKHN0YXR1czogbnVtYmVyKSB7XG4gICAgaWYgKHN0YXR1cyAmJiBzdGF0dXMgPT09IDUwMCkge1xuICAgICAgbm90aWZpY2F0aW9uLmVycm9yKHtcbiAgICAgICAgbWVzc2FnZTogXCJOZXR3b3JrIGVycm9yXCIsXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGVycm9yNTAyKHN0YXR1czogbnVtYmVyKSB7XG4gICAgaWYgKHN0YXR1cyAmJiBzdGF0dXMgPT09IDUwMikge1xuICAgICAgbm90aWZpY2F0aW9uLmVycm9yKHtcbiAgICAgICAgbWVzc2FnZTogXCJDb25uZWN0aW9uIHRpbWVvdXRcIixcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgZXJyb3JPdGhlcihtc2c6IHN0cmluZykge1xuICAgIG5vdGlmaWNhdGlvbi5lcnJvcih7XG4gICAgICBtZXNzYWdlOiBtc2cgfHwgXCJlcnJvclwiLFxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZVF1ZXJ5U3RyaW5nKG9iajogYW55KTogc3RyaW5nIHtcbiAgICByZXR1cm4gVXRpbHMudG9RdWVyeVN0cmluZyhvYmopO1xuICB9XG5cbiAgZ2V0U2VydmljZUxvY2F0aW9uKHJlbGF0aXZlUGF0aD86IHN0cmluZyB8IG51bGwpOiBzdHJpbmcge1xuICAgIGlmIChyZWxhdGl2ZVBhdGggJiYgKHJlbGF0aXZlUGF0aC5zdGFydHNXaXRoKFwiaHR0cHM6Ly9cIikgfHwgcmVsYXRpdmVQYXRoLnN0YXJ0c1dpdGgoXCJodHRwOi8vXCIpKSkge1xuICAgICAgcmV0dXJuIHJlbGF0aXZlUGF0aDtcbiAgICB9XG4gICAgbGV0IHVybCA9IHdpbmRvdy5sb2NhdGlvbjtcbiAgICBsZXQgcG9ydCA9IHVybC5wb3J0O1xuICAgIGlmIChwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfUE9SVCkge1xuICAgICAgcG9ydCA9IGA6JHtwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfUE9SVH1gO1xuICAgIH0gZWxzZSBpZiAocG9ydCkge1xuICAgICAgcG9ydCA9IGA6JHtwb3J0fWA7XG4gICAgfVxuICAgIGxldCBob3N0bmFtZSA9IHVybC5ob3N0bmFtZTtcbiAgICBpZiAocHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0FKQVhfU0VSVkVSX0hPU1QpIHtcbiAgICAgIGhvc3RuYW1lID0gcHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0FKQVhfU0VSVkVSX0hPU1Q7XG4gICAgfVxuICAgIGxldCBjb250ZXh0ID0gcHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0FKQVhfU0VSVkVSX0NPTlRFWFQgfHwgXCJcIjtcbiAgICAvLyBsZXQgbG9jYXRpb24gPSBgJHt1cmwucHJvdG9jb2x9Ly8ke2hvc3RuYW1lfSR7cG9ydH0ke2NvbnRleHR9YDtcbiAgICBsZXQgbG9jYXRpb24gPSBgJHtob3N0bmFtZX0ke3BvcnR9JHtjb250ZXh0fWA7XG4gICAgaWYgKCFsb2NhdGlvbi5zdGFydHNXaXRoKFwiaHR0cDovL1wiKSAmJiAhbG9jYXRpb24uc3RhcnRzV2l0aChcImh0dHBzOi8vXCIpKSB7XG4gICAgICBsb2NhdGlvbiA9IGAke3VybC5wcm90b2NvbH0vLyR7aG9zdG5hbWV9JHtwb3J0fSR7Y29udGV4dH1gO1xuICAgIH1cblxuICAgIGlmIChyZWxhdGl2ZVBhdGgpIHtcbiAgICAgIHJldHVybiBsb2NhdGlvbiArIHJlbGF0aXZlUGF0aDtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGxvY2F0aW9uO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYXBwZW5kTGFuZyh1cmw6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgaWYgKCF1cmwpIHtcbiAgICAgIHJldHVybiB1cmw7XG4gICAgfVxuICAgIGNvbnN0IGpvaW5lciA9IHVybC5pbmRleE9mKFwiP1wiKSA9PT0gLTEgPyBcIj9cIiA6IFwiJlwiO1xuICAgIHJldHVybiBgJHt1cmx9JHtqb2luZXJ9bGFuZz0ke0xhbmd1YWdlLmxhbmd1YWdlfWA7XG4gIH1cblxuICBwcml2YXRlIGFwcGVuZEF1dGgoaGVhZGVyczogQWpheEhlYWRlcnMsIGlnbm9yZUF1dGg6IGJvb2xlYW4gPSBmYWxzZSk6IHZvaWQge1xuICAgIGlmIChpZ25vcmVBdXRoID09PSB0cnVlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICghaGVhZGVyc1tDb25zdHMuQVVUSF9LRVldKSB7XG4gICAgICBoZWFkZXJzW0NvbnN0cy5BVVRIX0tFWV0gPSBFbnZzLmdldEF1dGgoKSB8fCBcIlwiO1xuICAgIH1cbiAgfVxuXG4gIGFwcGVuZEF1dGhUb1VybCh1cmw6IHN0cmluZykge1xuICAgIGNvbnN0IGFwaVVybCA9IHRoaXMuZ2V0U2VydmljZUxvY2F0aW9uKHVybCk7XG4gICAgY29uc3QgYWNjZXNzS2V5ID0gRW52cy5nZXRBdXRoKCk7XG4gICAgY29uc3Qgc2VwYXJhdG9yID0gYXBpVXJsLmluZGV4T2YoXCI/XCIpID09PSAtMSA/IFwiP1wiIDogXCImXCI7XG4gICAgcmV0dXJuIGAke2FwaVVybH0ke3NlcGFyYXRvcn1hY2Nlc3NfdG9rZW49JHtlbmNvZGVVUklDb21wb25lbnQoYWNjZXNzS2V5KX1gO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBBamF4KCk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUF1R0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFpQkE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBVUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQSxlQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQTNVQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBckJBO0FBK0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFDQTtBQVdBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTFHQTtBQXFIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBNE1BIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/common/ajax.tsx
