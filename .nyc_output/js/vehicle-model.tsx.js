__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/vehicle-model.tsx";

function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      padding: 0 6px;\n      line-height: 42px;\n      cursor: pointer;\n\n      &:hover, &[data-highlight=true] {\n        background: ", ";\n        color: #fff;\n      }\n      "]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        max-height: 337px;\n        min-height: 200px;\n        overflow: auto;\n        list-style: none;\n        margin-left: 0;\n        padding-left: 0;\n        font-size: 16px;\n      "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      &[data-full=false] {\n        border: 1px solid @error-color;\n      }\n      "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      position: absolute;\n      left: 0;\n      top: 43px;\n      width: 100%;\n      border: 1px solid #ebebeb;\n      background: #fff;\n      z-index: 100;\n        a {\n          color: ", "\n        }\n      "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      width: 100%;\n      position: relative;\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_12__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A,
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var VehicleModel =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(VehicleModel, _Widget);

  function VehicleModel(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, VehicleModel);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(VehicleModel).call(this, props, state));
    _this.isLeaf = false;
    _this._refs = {
      dropdown: null,
      elem: null
    };
    _this.initialValues = [];

    _this.hideDropdown = function (event) {
      if (_this._refs.elem.contains(event.target)) {
        return;
      }

      _this.setState({
        visible: false
      });
    };

    _this.handleSelect = function () {
      _this.setState({
        visible: !_this.state.visible
      });
    };

    _this.isLeaf = !!_this.initialValues.length;
    _this._refs = {
      dropdown: {},
      elem: {}
    };
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(VehicleModel, [{
    key: "initState",
    value: function initState() {
      this.initialValues = this.getInitValues();
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(VehicleModel.prototype), "initState", this).call(this), {
        visible: false,
        loading: true,
        error: null,
        list: [],
        values: this.initialValues,
        index: this.initialValues.length > 0 ? this.initialValues.length - 1 : 0,
        showLabel: this.getShowLabel(this.props.value)
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject()),
        Dropdown: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject2(), COLOR_A),
        Input: Object(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"])(antd__WEBPACK_IMPORTED_MODULE_11__["Input"])(_templateObject3()),
        Ul: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].ul(_templateObject4()),
        Li: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].li(_templateObject5(), COLOR_PRIMARY)
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getVmodesData(this.state.values.slice(0, -1), true);
      document.body.addEventListener("click", this.hideDropdown);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.body.removeEventListener("click", this.hideDropdown);
    }
  }, {
    key: "setElemRef",
    value: function setElemRef(ref) {
      if (ref) {
        this._refs.elem = ref;
      }
    }
  }, {
    key: "getInitValues",
    value: function getInitValues() {
      var modal = this.props.value;
      var values = [];

      if (modal) {
        for (var i = 1; i <= 4; i++) {
          if (modal["k".concat(i)]) {
            values.push({
              id: modal["k".concat(i)],
              text: modal["k".concat(i, "name")]
            });
          }
        }
      }

      return values;
    }
  }, {
    key: "getShowLabel",
    value: function getShowLabel(modal) {
      var showLabel = [];

      if (modal) {
        // 从k1开始, 最多到k4
        for (var i = 1; i <= 4; i++) {
          if (modal["k".concat(i, "name")]) {
            showLabel.push(modal["k".concat(i, "name")]);
          }
        }
      }

      return showLabel.join(" ");
    }
  }, {
    key: "setLoading",
    value: function setLoading() {
      this.setState({
        loading: true,
        error: null
      });
    }
  }, {
    key: "setError",
    value: function setError(err) {
      this.setState({
        loading: false,
        error: err,
        list: []
      });
    }
  }, {
    key: "setData",
    value: function setData(list, level) {
      var values = this.state.values;
      var showLabel = values.map(function (item) {
        return item.text;
      }).join(" ");

      if (level === 3) {
        list.sort(function (a, b) {
          return b.id - a.id;
        });
      }

      this.setState({
        loading: false,
        error: null,
        list: list,
        showLabel: showLabel
      });
    }
  }, {
    key: "setDataFinished",
    value: function setDataFinished(modal) {
      var showLabel = this.getShowLabel(modal);
      this.setState({
        loading: false,
        visible: false,
        index: this.state.index - 1,
        showLabel: showLabel
      });
    }
  }, {
    key: "isHighlight",
    value: function isHighlight(id) {
      var _this$state = this.state,
          values = _this$state.values,
          index = _this$state.index;
      var value = values[index];
      return value && value.id === id;
    }
  }, {
    key: "getVmodesData",
    value: function getVmodesData(values) {
      var _this2 = this;

      var inited = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      this.setLoading();
      var param = this.props.param;
      var level = values.length + 1; //TODO the itnt code will be define an constant

      var params = {};

      if (this.props.itntCode) {
        params = {
          itntCode: this.props.itntCode,
          filter: "GPC",
          level: level
        };
      } else {
        params = {
          itntCode: "".concat(param, "_THAI"),
          filter: this.props.prdtCode,
          level: level
        };
      }

      (values || []).forEach(function (item, index) {
        params["k".concat(index + 1)] = item.id;
      });
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/vmodelsq", params).then(function (response) {
        var respData = response.body.respData;
        var isLeaf = respData.leaf;
        _this2.isLeaf = inited || isLeaf;

        if (!isLeaf) {
          _this2.setData(respData.keys, level);
        } else {
          _this2.props.onChange && _this2.props.onChange(respData.model);

          _this2.setDataFinished(respData.model);
        }
      }).catch(function (error) {
        _this2.setError(error);
      });
    }
  }, {
    key: "handleValueClick",
    value: function handleValueClick(index) {
      var newValues = this.state.values.slice(0, index + 1);
      var paramsValues = this.state.values.slice(0, index); // 查询倒数第二级

      this.setState({
        values: newValues || [],
        index: index
      });
      this.getVmodesData(paramsValues);
    }
  }, {
    key: "handleListItemClick",
    value: function handleListItemClick(item) {
      var _this$state2 = this.state,
          values = _this$state2.values,
          index = _this$state2.index;

      var newValues = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(values);

      newValues[index] = item;
      this.setState({
        values: newValues,
        index: index + 1
      });
      this.getVmodesData(newValues);
    }
  }, {
    key: "selectedRender",
    value: function selectedRender() {
      var _this3 = this;

      var values = this.state.values;
      var length = values.length;
      var views = [];
      values.forEach(function (value, index) {
        views.push(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
          key: value.id,
          onClick: function onClick() {
            return _this3.handleValueClick(index);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 264
          },
          __self: this
        }, value.text));

        if (!(index === length - 1)) {
          views.push(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
            key: "separator-".concat(index),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 269
            },
            __self: this
          }, " / "));
        }
      });
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          margin: "0 6px",
          fontSize: "16px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 272
        },
        __self: this
      }, views);
    }
  }, {
    key: "listRender",
    value: function listRender() {
      var _this4 = this;

      var _this$state3 = this.state,
          loading = _this$state3.loading,
          error = _this$state3.error,
          list = _this$state3.list;

      if (error || !list || list.length === 0) {
        var message = !error ? "" : error["body.respMessage"];
        console.error(error);
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 281
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 282
          },
          __self: this
        }, !!message ? message : "no data"));
      }

      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Spin"], {
        size: "large",
        spinning: loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 288
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Ul, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289
        },
        __self: this
      }, list.map(function (item) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Li, {
          key: item.id,
          "data-highlight": _this4.isHighlight(item.id),
          onClick: function onClick() {
            return _this4.handleListItemClick(item);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 291
          },
          __self: this
        }, item.text);
      })));
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state4 = this.state,
          visible = _this$state4.visible,
          showLabel = _this$state4.showLabel;
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Box, {
        ref: this.setElemRef.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Input, {
        readOnly: true,
        placeholder: this.props.placeholder,
        value: showLabel,
        onClick: this.handleSelect,
        size: "large",
        "data-full": this.isLeaf || visible || !showLabel,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309
        },
        __self: this
      }), visible && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Dropdown, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 318
        },
        __self: this
      }, this.selectedRender(), this.listRender()));
    }
  }]);

  return VehicleModel;
}(_component__WEBPACK_IMPORTED_MODULE_9__["Widget"]);

VehicleModel.defaultProps = {
  param: "PF"
};
/* harmony default export */ __webpack_exports__["default"] = (VehicleModel);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3ZlaGljbGUtbW9kZWwudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3ZlaGljbGUtbW9kZWwudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UsIFN0eWxlZERJViwgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBBamF4IH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IElucHV0LCBTcGluIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuXG5jb25zdCB7IENPTE9SX0EsIENPTE9SX1BSSU1BUlkgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5cbmV4cG9ydCB0eXBlIFZlaGljbGVNb2RlbFByb3BzID0ge1xuICBwbGFjZWhvbGRlcj86IHN0cmluZztcbiAgcHJkdENvZGU6IHN0cmluZztcbiAgaXRudENvZGU/OiBzdHJpbmc7XG4gIHZhbHVlPzogYW55O1xuICBwYXJhbT86IGFueTtcbiAgb25DaGFuZ2U/OiAodmFsdWU6IGFueSkgPT4gdm9pZDtcbn0gJiBXaWRnZXRQcm9wcztcblxuZXhwb3J0IHR5cGUgVmVoaWNsZU1vZGVsU3RhdGUgPSB7XG4gIHZpc2libGU6IGJvb2xlYW47XG4gIGxvYWRpbmc6IGJvb2xlYW47XG4gIGVycm9yOiBhbnk7XG4gIGxpc3Q6IGFueVtdO1xuICB2YWx1ZXM6IGFueVtdO1xuICBpbmRleDogbnVtYmVyO1xuICBzaG93TGFiZWw6IHN0cmluZztcbn07XG5cbmV4cG9ydCB0eXBlIFN0eWxlZFVsID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcInVsXCIsIGFueSwge30sIG5ldmVyPjtcbmV4cG9ydCB0eXBlIFN0eWxlZExpID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImxpXCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgVmVoaWNsZU1vZGVsQ29tcG9uZW50cyA9IHtcbiAgQm94OiBTdHlsZWRESVY7XG4gIERyb3Bkb3duOiBTdHlsZWRESVY7XG4gIFVsOiBTdHlsZWRVbDtcbiAgTGk6IFN0eWxlZExpO1xuICBJbnB1dDogYW55O1xufTtcblxuY2xhc3MgVmVoaWNsZU1vZGVsPFAgZXh0ZW5kcyBWZWhpY2xlTW9kZWxQcm9wcyxcbiAgUyBleHRlbmRzIFZlaGljbGVNb2RlbFN0YXRlLFxuICBDIGV4dGVuZHMgVmVoaWNsZU1vZGVsQ29tcG9uZW50cz4gZXh0ZW5kcyBXaWRnZXQ8UCwgUywgQz4ge1xuICBwcml2YXRlIGlzTGVhZjogYm9vbGVhbiA9IGZhbHNlO1xuICBwcml2YXRlIF9yZWZzOiBhbnkgPSB7IGRyb3Bkb3duOiBudWxsLCBlbGVtOiBudWxsIH0gYXMgYW55O1xuICBwcml2YXRlIGluaXRpYWxWYWx1ZXM6IGFueVtdID0gW107XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICBwYXJhbTogXCJQRlwiLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBhbnksIHN0YXRlPzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIHN0YXRlKTtcbiAgICB0aGlzLmlzTGVhZiA9ICEhdGhpcy5pbml0aWFsVmFsdWVzLmxlbmd0aDtcbiAgICB0aGlzLl9yZWZzID0geyBkcm9wZG93bjoge30sIGVsZW06IHt9IH0gYXMgYW55O1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICB0aGlzLmluaXRpYWxWYWx1ZXMgPSB0aGlzLmdldEluaXRWYWx1ZXMoKTtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBsb2FkaW5nOiB0cnVlLFxuICAgICAgZXJyb3I6IG51bGwsXG4gICAgICBsaXN0OiBbXSxcbiAgICAgIHZhbHVlczogdGhpcy5pbml0aWFsVmFsdWVzLFxuICAgICAgaW5kZXg6IHRoaXMuaW5pdGlhbFZhbHVlcy5sZW5ndGggPiAwID8gdGhpcy5pbml0aWFsVmFsdWVzLmxlbmd0aCAtIDEgOiAwLFxuICAgICAgc2hvd0xhYmVsOiB0aGlzLmdldFNob3dMYWJlbCh0aGlzLnByb3BzLnZhbHVlKSxcbiAgICB9KSBhcyBTO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3g6IFN0eWxlZC5kaXZgXG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIGAsXG4gICAgICBEcm9wZG93bjogU3R5bGVkLmRpdmBcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgICB0b3A6IDQzcHg7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgei1pbmRleDogMTAwO1xuICAgICAgICBhIHtcbiAgICAgICAgICBjb2xvcjogJHtDT0xPUl9BfVxuICAgICAgICB9XG4gICAgICBgLFxuICAgICAgSW5wdXQ6IFN0eWxlZChJbnB1dClgXG4gICAgICAmW2RhdGEtZnVsbD1mYWxzZV0ge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCBAZXJyb3ItY29sb3I7XG4gICAgICB9XG4gICAgICBgLFxuICAgICAgVWw6IFN0eWxlZC51bGBcbiAgICAgICAgbWF4LWhlaWdodDogMzM3cHg7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgICAgICBvdmVyZmxvdzogYXV0bztcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgYCxcbiAgICAgIExpOiBTdHlsZWQubGlgXG4gICAgICBwYWRkaW5nOiAwIDZweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA0MnB4O1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuXG4gICAgICAmOmhvdmVyLCAmW2RhdGEtaGlnaGxpZ2h0PXRydWVdIHtcbiAgICAgICAgYmFja2dyb3VuZDogJHtDT0xPUl9QUklNQVJZfTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICB9XG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCk6IHZvaWQge1xuICAgIHRoaXMuZ2V0Vm1vZGVzRGF0YSh0aGlzLnN0YXRlLnZhbHVlcy5zbGljZSgwLCAtMSksIHRydWUpO1xuICAgIGRvY3VtZW50LmJvZHkuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuaGlkZURyb3Bkb3duKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMuaGlkZURyb3Bkb3duKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0RWxlbVJlZihyZWY6IGFueSk6IHZvaWQge1xuICAgIGlmIChyZWYpIHtcbiAgICAgIHRoaXMuX3JlZnMuZWxlbSA9IHJlZjtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGhpZGVEcm9wZG93biA9IChldmVudDogYW55KTogdm9pZCA9PiB7XG4gICAgaWYgKHRoaXMuX3JlZnMuZWxlbS5jb250YWlucyhldmVudC50YXJnZXQpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuc2V0U3RhdGUoeyB2aXNpYmxlOiBmYWxzZSB9KTtcbiAgfTtcblxuICBwcml2YXRlIGdldEluaXRWYWx1ZXMoKTogYW55W10ge1xuICAgIGNvbnN0IG1vZGFsID0gdGhpcy5wcm9wcy52YWx1ZTtcbiAgICBjb25zdCB2YWx1ZXMgPSBbXTtcbiAgICBpZiAobW9kYWwpIHtcbiAgICAgIGZvciAobGV0IGkgPSAxOyBpIDw9IDQ7IGkrKykge1xuICAgICAgICBpZiAobW9kYWxbYGske2l9YF0pIHtcbiAgICAgICAgICB2YWx1ZXMucHVzaCh7XG4gICAgICAgICAgICBpZDogbW9kYWxbYGske2l9YF0sXG4gICAgICAgICAgICB0ZXh0OiBtb2RhbFtgayR7aX1uYW1lYF0sXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlcztcbiAgfVxuXG4gIHByaXZhdGUgZ2V0U2hvd0xhYmVsKG1vZGFsOiBhbnkpOiBzdHJpbmcge1xuICAgIGxldCBzaG93TGFiZWwgPSBbXTtcbiAgICBpZiAobW9kYWwpIHtcbiAgICAgIC8vIOS7jmsx5byA5aeLLCDmnIDlpJrliLBrNFxuICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gNDsgaSsrKSB7XG4gICAgICAgIGlmIChtb2RhbFtgayR7aX1uYW1lYF0pIHtcbiAgICAgICAgICBzaG93TGFiZWwucHVzaChtb2RhbFtgayR7aX1uYW1lYF0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBzaG93TGFiZWwuam9pbihcIiBcIik7XG4gIH1cblxuICBwcml2YXRlIHNldExvYWRpbmcoKTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IHRydWUsIGVycm9yOiBudWxsIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRFcnJvcihlcnI6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBsb2FkaW5nOiBmYWxzZSwgZXJyb3I6IGVyciwgbGlzdDogW10gfSk7XG4gIH1cblxuICBwcml2YXRlIHNldERhdGEobGlzdDogYW55W10sIGxldmVsOiBudW1iZXIpOiB2b2lkIHtcbiAgICBjb25zdCB7IHZhbHVlcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBzaG93TGFiZWwgPSB2YWx1ZXMubWFwKGl0ZW0gPT4gaXRlbS50ZXh0KS5qb2luKFwiIFwiKTtcbiAgICBpZiAobGV2ZWwgPT09IDMpIHtcbiAgICAgIGxpc3Quc29ydCgoYTogYW55LCBiOiBhbnkpID0+IGIuaWQgLSBhLmlkKTtcbiAgICB9XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IGZhbHNlLCBlcnJvcjogbnVsbCwgbGlzdDogbGlzdCwgc2hvd0xhYmVsIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXREYXRhRmluaXNoZWQobW9kYWw6IGFueSk6IHZvaWQge1xuICAgIGNvbnN0IHNob3dMYWJlbCA9IHRoaXMuZ2V0U2hvd0xhYmVsKG1vZGFsKTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgdmlzaWJsZTogZmFsc2UsXG4gICAgICBpbmRleDogdGhpcy5zdGF0ZS5pbmRleCAtIDEsXG4gICAgICBzaG93TGFiZWwsXG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGlzSGlnaGxpZ2h0KGlkOiBhbnkpOiBib29sZWFuIHtcbiAgICBjb25zdCB7IHZhbHVlcywgaW5kZXggfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgdmFsdWUgPSB2YWx1ZXNbaW5kZXhdO1xuICAgIHJldHVybiB2YWx1ZSAmJiB2YWx1ZS5pZCA9PT0gaWQ7XG4gIH1cblxuICBwcml2YXRlIGdldFZtb2Rlc0RhdGEodmFsdWVzOiBhbnlbXSwgaW5pdGVkID0gZmFsc2UpOiB2b2lkIHtcbiAgICB0aGlzLnNldExvYWRpbmcoKTtcbiAgICBjb25zdCB7IHBhcmFtIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGxldmVsID0gdmFsdWVzLmxlbmd0aCArIDE7XG4gICAgLy9UT0RPIHRoZSBpdG50IGNvZGUgd2lsbCBiZSBkZWZpbmUgYW4gY29uc3RhbnRcbiAgICBsZXQgcGFyYW1zID0ge30gYXMgYW55O1xuICAgIGlmICh0aGlzLnByb3BzLml0bnRDb2RlKSB7XG4gICAgICBwYXJhbXMgPSB7IGl0bnRDb2RlOiB0aGlzLnByb3BzLml0bnRDb2RlLCBmaWx0ZXI6IFwiR1BDXCIsIGxldmVsIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcmFtcyA9IHsgaXRudENvZGU6IGAke3BhcmFtfV9USEFJYCwgZmlsdGVyOiB0aGlzLnByb3BzLnByZHRDb2RlLCBsZXZlbCB9O1xuICAgIH1cbiAgICAodmFsdWVzIHx8IFtdKS5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xuICAgICAgcGFyYW1zW2BrJHtpbmRleCArIDF9YF0gPSBpdGVtLmlkO1xuICAgIH0pO1xuICAgIEFqYXguZ2V0KGAvdm1vZGVsc3FgLCBwYXJhbXMpXG4gICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICBjb25zdCBpc0xlYWYgPSByZXNwRGF0YS5sZWFmO1xuICAgICAgICB0aGlzLmlzTGVhZiA9IGluaXRlZCB8fCBpc0xlYWY7XG4gICAgICAgIGlmICghaXNMZWFmKSB7XG4gICAgICAgICAgdGhpcy5zZXREYXRhKHJlc3BEYXRhLmtleXMsIGxldmVsKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlICYmIHRoaXMucHJvcHMub25DaGFuZ2UocmVzcERhdGEubW9kZWwpO1xuICAgICAgICAgIHRoaXMuc2V0RGF0YUZpbmlzaGVkKHJlc3BEYXRhLm1vZGVsKTtcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHRoaXMuc2V0RXJyb3IoZXJyb3IpO1xuICAgICAgfSk7XG4gIH1cblxuICBwcml2YXRlIGhhbmRsZVNlbGVjdCA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHZpc2libGU6ICF0aGlzLnN0YXRlLnZpc2libGUsXG4gICAgfSk7XG4gIH07XG5cbiAgcHJpdmF0ZSBoYW5kbGVWYWx1ZUNsaWNrKGluZGV4OiBudW1iZXIpOiB2b2lkIHtcbiAgICBjb25zdCBuZXdWYWx1ZXMgPSB0aGlzLnN0YXRlLnZhbHVlcy5zbGljZSgwLCBpbmRleCArIDEpO1xuICAgIGNvbnN0IHBhcmFtc1ZhbHVlcyA9IHRoaXMuc3RhdGUudmFsdWVzLnNsaWNlKDAsIGluZGV4KTsgLy8g5p+l6K+i5YCS5pWw56ys5LqM57qnXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2YWx1ZXM6IG5ld1ZhbHVlcyB8fCBbXSxcbiAgICAgIGluZGV4OiBpbmRleCxcbiAgICB9KTtcbiAgICB0aGlzLmdldFZtb2Rlc0RhdGEocGFyYW1zVmFsdWVzKTtcbiAgfVxuXG4gIHByaXZhdGUgaGFuZGxlTGlzdEl0ZW1DbGljayhpdGVtOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zdCB7IHZhbHVlcywgaW5kZXggfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgbmV3VmFsdWVzID0gWy4uLnZhbHVlc107XG4gICAgbmV3VmFsdWVzW2luZGV4XSA9IGl0ZW07XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2YWx1ZXM6IG5ld1ZhbHVlcyxcbiAgICAgIGluZGV4OiBpbmRleCArIDEsXG4gICAgfSk7XG4gICAgdGhpcy5nZXRWbW9kZXNEYXRhKG5ld1ZhbHVlcyk7XG4gIH1cblxuICBwcml2YXRlIHNlbGVjdGVkUmVuZGVyKCk6IGFueSB7XG4gICAgY29uc3QgeyB2YWx1ZXMgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgbGVuZ3RoID0gdmFsdWVzLmxlbmd0aDtcbiAgICBjb25zdCB2aWV3czogYW55W10gPSBbXTtcbiAgICB2YWx1ZXMuZm9yRWFjaCgodmFsdWUsIGluZGV4KSA9PiB7XG4gICAgICB2aWV3cy5wdXNoKFxuICAgICAgICA8YSBrZXk9e3ZhbHVlLmlkfSBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZVZhbHVlQ2xpY2soaW5kZXgpfT5cbiAgICAgICAgICB7dmFsdWUudGV4dH1cbiAgICAgICAgPC9hPixcbiAgICAgICk7XG4gICAgICBpZiAoIShpbmRleCA9PT0gbGVuZ3RoIC0gMSkpIHtcbiAgICAgICAgdmlld3MucHVzaCg8c3BhbiBrZXk9e2BzZXBhcmF0b3ItJHtpbmRleH1gfT4gLyA8L3NwYW4+KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gPGRpdiBzdHlsZT17eyBtYXJnaW46IFwiMCA2cHhcIiwgZm9udFNpemU6IFwiMTZweFwiIH19Pnt2aWV3c308L2Rpdj47XG4gIH1cblxuICBwcm90ZWN0ZWQgbGlzdFJlbmRlcigpOiBhbnkge1xuICAgIGNvbnN0IHsgbG9hZGluZywgZXJyb3IsIGxpc3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgaWYgKGVycm9yIHx8ICFsaXN0IHx8IGxpc3QubGVuZ3RoID09PSAwKSB7XG4gICAgICBjb25zdCBtZXNzYWdlID0gIWVycm9yID8gXCJcIiA6IGVycm9yW1wiYm9keS5yZXNwTWVzc2FnZVwiXTtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8c3Bhbj57ISFtZXNzYWdlID8gbWVzc2FnZSA6IFwibm8gZGF0YVwifTwvc3Bhbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXtsb2FkaW5nfT5cbiAgICAgICAgPEMuVWw+XG4gICAgICAgICAge2xpc3QubWFwKGl0ZW0gPT4gKFxuICAgICAgICAgICAgPEMuTGlcbiAgICAgICAgICAgICAga2V5PXtpdGVtLmlkfVxuICAgICAgICAgICAgICBkYXRhLWhpZ2hsaWdodD17dGhpcy5pc0hpZ2hsaWdodChpdGVtLmlkKX1cbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVMaXN0SXRlbUNsaWNrKGl0ZW0pfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7aXRlbS50ZXh0fVxuICAgICAgICAgICAgPC9DLkxpPlxuICAgICAgICAgICkpfVxuICAgICAgICA8L0MuVWw+XG4gICAgICA8L1NwaW4+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IHZpc2libGUsIHNob3dMYWJlbCB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveCByZWY9e3RoaXMuc2V0RWxlbVJlZi5iaW5kKHRoaXMpfT5cbiAgICAgICAgPEMuSW5wdXRcbiAgICAgICAgICByZWFkT25seVxuICAgICAgICAgIHBsYWNlaG9sZGVyPXt0aGlzLnByb3BzLnBsYWNlaG9sZGVyfVxuICAgICAgICAgIHZhbHVlPXtzaG93TGFiZWx9XG4gICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVTZWxlY3R9XG4gICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICBkYXRhLWZ1bGw9e3RoaXMuaXNMZWFmIHx8IHZpc2libGUgfHwgIXNob3dMYWJlbH1cbiAgICAgICAgLz5cbiAgICAgICAge3Zpc2libGUgJiYgKFxuICAgICAgICAgIDxDLkRyb3Bkb3duPlxuICAgICAgICAgICAge3RoaXMuc2VsZWN0ZWRSZW5kZXIoKX1cbiAgICAgICAgICAgIHt0aGlzLmxpc3RSZW5kZXIoKX1cbiAgICAgICAgICA8L0MuRHJvcGRvd24+XG4gICAgICAgICl9XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVmVoaWNsZU1vZGVsO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBK0JBOzs7OztBQVdBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFQQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBREE7QUFnRkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBckZBO0FBb0xBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUF0TEE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBSUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFJQTtBQVlBO0FBS0E7QUFTQTtBQS9CQTtBQTBDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVlBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7O0FBMVJBO0FBQ0E7QUFIQTtBQVFBO0FBREE7QUF3UkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/vehicle-model.tsx
