__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var _InputHandler__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./InputHandler */ "./node_modules/rc-input-number/es/InputHandler.js");











function noop() {}

function preventDefault(e) {
  e.preventDefault();
}

function defaultParser(input) {
  return input.replace(/[^\w\.-]+/g, '');
}
/**
 * When click and hold on a button - the speed of auto changin the value.
 */


var SPEED = 200;
/**
 * When click and hold on a button - the delay before auto changin the value.
 */

var DELAY = 600;
/**
 * Max Safe Integer -- on IE this is not available, so manually set the number in that case.
 * The reason this is used, instead of Infinity is because numbers above the MSI are unstable
 */

var MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || Math.pow(2, 53) - 1;

var isValidProps = function isValidProps(value) {
  return value !== undefined && value !== null;
};

var isEqual = function isEqual(oldValue, newValue) {
  return newValue === oldValue || typeof newValue === 'number' && typeof oldValue === 'number' && isNaN(newValue) && isNaN(oldValue);
};

var InputNumber = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(InputNumber, _React$Component);

  function InputNumber(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, InputNumber);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, _React$Component.call(this, props));

    _initialiseProps.call(_this);

    var value = void 0;

    if ('value' in props) {
      value = props.value;
    } else {
      value = props.defaultValue;
    }

    _this.state = {
      focused: props.autoFocus
    };

    var validValue = _this.getValidValue(_this.toNumber(value));

    _this.state = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, _this.state, {
      inputValue: _this.toPrecisionAsStep(validValue),
      value: validValue
    });
    return _this;
  }

  InputNumber.prototype.componentDidMount = function componentDidMount() {
    this.componentDidUpdate();
  };

  InputNumber.prototype.componentDidUpdate = function componentDidUpdate(prevProps) {
    var _props = this.props,
        value = _props.value,
        onChange = _props.onChange,
        max = _props.max,
        min = _props.min;
    var focused = this.state.focused; // Don't trigger in componentDidMount

    if (prevProps) {
      if (!isEqual(prevProps.value, value) || !isEqual(prevProps.max, max) || !isEqual(prevProps.min, min)) {
        var validValue = focused ? value : this.getValidValue(value);
        var nextInputValue = void 0;

        if (this.pressingUpOrDown) {
          nextInputValue = validValue;
        } else if (this.inputting) {
          nextInputValue = this.rawInput;
        } else {
          nextInputValue = this.toPrecisionAsStep(validValue);
        }

        this.setState({
          // eslint-disable-line
          value: validValue,
          inputValue: nextInputValue
        });
      } // Trigger onChange when max or min change
      // https://github.com/ant-design/ant-design/issues/11574


      var nextValue = 'value' in this.props ? value : this.state.value; // ref: null < 20 === true
      // https://github.com/ant-design/ant-design/issues/14277

      if ('max' in this.props && prevProps.max !== max && typeof nextValue === 'number' && nextValue > max && onChange) {
        onChange(max);
      }

      if ('min' in this.props && prevProps.min !== min && typeof nextValue === 'number' && nextValue < min && onChange) {
        onChange(min);
      }
    } // Restore cursor


    try {
      // Firefox set the input cursor after it get focused.
      // This caused that if an input didn't init with the selection,
      // set will cause cursor not correct when first focus.
      // Safari will focus input if set selection. We need skip this.
      if (this.cursorStart !== undefined && this.state.focused) {
        // In most cases, the string after cursor is stable.
        // We can move the cursor before it
        if ( // If not match full str, try to match part of str
        !this.partRestoreByAfter(this.cursorAfter) && this.state.value !== this.props.value) {
          // If not match any of then, let's just keep the position
          // TODO: Logic should not reach here, need check if happens
          var pos = this.cursorStart + 1; // If not have last string, just position to the end

          if (!this.cursorAfter) {
            pos = this.input.value.length;
          } else if (this.lastKeyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].BACKSPACE) {
            pos = this.cursorStart - 1;
          } else if (this.lastKeyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].DELETE) {
            pos = this.cursorStart;
          }

          this.fixCaret(pos, pos);
        } else if (this.currentValue === this.input.value) {
          // Handle some special key code
          switch (this.lastKeyCode) {
            case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].BACKSPACE:
              this.fixCaret(this.cursorStart - 1, this.cursorStart - 1);
              break;

            case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].DELETE:
              this.fixCaret(this.cursorStart + 1, this.cursorStart + 1);
              break;

            default: // Do nothing

          }
        }
      }
    } catch (e) {} // Do nothing
    // Reset last key


    this.lastKeyCode = null; // pressingUpOrDown is true means that someone just click up or down button

    if (!this.pressingUpOrDown) {
      return;
    }

    if (this.props.focusOnUpDown && this.state.focused) {
      if (document.activeElement !== this.input) {
        this.focus();
      }
    }

    this.pressingUpOrDown = false;
  };

  InputNumber.prototype.componentWillUnmount = function componentWillUnmount() {
    this.stop();
  };

  InputNumber.prototype.getCurrentValidValue = function getCurrentValidValue(value) {
    var val = value;

    if (val === '') {
      val = '';
    } else if (!this.isNotCompleteNumber(parseFloat(val, 10))) {
      val = this.getValidValue(val);
    } else {
      val = this.state.value;
    }

    return this.toNumber(val);
  };

  InputNumber.prototype.getRatio = function getRatio(e) {
    var ratio = 1;

    if (e.metaKey || e.ctrlKey) {
      ratio = 0.1;
    } else if (e.shiftKey) {
      ratio = 10;
    }

    return ratio;
  };

  InputNumber.prototype.getValueFromEvent = function getValueFromEvent(e) {
    // optimize for chinese input expierence
    // https://github.com/ant-design/ant-design/issues/8196
    var value = e.target.value.trim().replace(/。/g, '.');

    if (isValidProps(this.props.decimalSeparator)) {
      value = value.replace(this.props.decimalSeparator, '.');
    }

    return value;
  };

  InputNumber.prototype.getValidValue = function getValidValue(value) {
    var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.props.min;
    var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : this.props.max;
    var val = parseFloat(value, 10); // https://github.com/ant-design/ant-design/issues/7358

    if (isNaN(val)) {
      return value;
    }

    if (val < min) {
      val = min;
    }

    if (val > max) {
      val = max;
    }

    return val;
  };

  InputNumber.prototype.setValue = function setValue(v, callback) {
    // trigger onChange
    var precision = this.props.precision;
    var newValue = this.isNotCompleteNumber(parseFloat(v, 10)) ? null : parseFloat(v, 10);
    var _state = this.state,
        _state$value = _state.value,
        value = _state$value === undefined ? null : _state$value,
        _state$inputValue = _state.inputValue,
        inputValue = _state$inputValue === undefined ? null : _state$inputValue; // https://github.com/ant-design/ant-design/issues/7363
    // https://github.com/ant-design/ant-design/issues/16622

    var newValueInString = typeof newValue === 'number' ? newValue.toFixed(precision) : '' + newValue;
    var changed = newValue !== value || newValueInString !== '' + inputValue;

    if (!('value' in this.props)) {
      this.setState({
        value: newValue,
        inputValue: this.toPrecisionAsStep(v)
      }, callback);
    } else {
      // always set input value same as value
      this.setState({
        inputValue: this.toPrecisionAsStep(this.state.value)
      }, callback);
    }

    if (changed) {
      this.props.onChange(newValue);
    }

    return newValue;
  };

  InputNumber.prototype.getPrecision = function getPrecision(value) {
    if (isValidProps(this.props.precision)) {
      return this.props.precision;
    }

    var valueString = value.toString();

    if (valueString.indexOf('e-') >= 0) {
      return parseInt(valueString.slice(valueString.indexOf('e-') + 2), 10);
    }

    var precision = 0;

    if (valueString.indexOf('.') >= 0) {
      precision = valueString.length - valueString.indexOf('.') - 1;
    }

    return precision;
  }; // step={1.0} value={1.51}
  // press +
  // then value should be 2.51, rather than 2.5
  // if this.props.precision is undefined
  // https://github.com/react-component/input-number/issues/39


  InputNumber.prototype.getMaxPrecision = function getMaxPrecision(currentValue) {
    var ratio = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
    var _props2 = this.props,
        precision = _props2.precision,
        step = _props2.step;

    if (isValidProps(precision)) {
      return precision;
    }

    var ratioPrecision = this.getPrecision(ratio);
    var stepPrecision = this.getPrecision(step);
    var currentValuePrecision = this.getPrecision(currentValue);

    if (!currentValue) {
      return ratioPrecision + stepPrecision;
    }

    return Math.max(currentValuePrecision, ratioPrecision + stepPrecision);
  };

  InputNumber.prototype.getPrecisionFactor = function getPrecisionFactor(currentValue) {
    var ratio = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
    var precision = this.getMaxPrecision(currentValue, ratio);
    return Math.pow(10, precision);
  };

  InputNumber.prototype.fixCaret = function fixCaret(start, end) {
    if (start === undefined || end === undefined || !this.input || !this.input.value) {
      return;
    }

    try {
      var currentStart = this.input.selectionStart;
      var currentEnd = this.input.selectionEnd;

      if (start !== currentStart || end !== currentEnd) {
        this.input.setSelectionRange(start, end);
      }
    } catch (e) {// Fix error in Chrome:
      // Failed to read the 'selectionStart' property from 'HTMLInputElement'
      // http://stackoverflow.com/q/21177489/3040605
    }
  };

  InputNumber.prototype.focus = function focus() {
    this.input.focus();
    this.recordCursorPosition();
  };

  InputNumber.prototype.blur = function blur() {
    this.input.blur();
  };

  InputNumber.prototype.formatWrapper = function formatWrapper(num) {
    // http://2ality.com/2012/03/signedzero.html
    // https://github.com/ant-design/ant-design/issues/9439
    if (this.props.formatter) {
      return this.props.formatter(num);
    }

    return num;
  };

  InputNumber.prototype.toPrecisionAsStep = function toPrecisionAsStep(num) {
    if (this.isNotCompleteNumber(num) || num === '') {
      return num;
    }

    var precision = Math.abs(this.getMaxPrecision(num));

    if (!isNaN(precision)) {
      return Number(num).toFixed(precision);
    }

    return num.toString();
  }; // '1.' '1x' 'xx' '' => are not complete numbers


  InputNumber.prototype.isNotCompleteNumber = function isNotCompleteNumber(num) {
    return isNaN(num) || num === '' || num === null || num && num.toString().indexOf('.') === num.toString().length - 1;
  };

  InputNumber.prototype.toNumber = function toNumber(num) {
    var precision = this.props.precision;
    var focused = this.state.focused; // num.length > 16 => This is to prevent input of large numbers

    var numberIsTooLarge = num && num.length > 16 && focused;

    if (this.isNotCompleteNumber(num) || numberIsTooLarge) {
      return num;
    }

    if (isValidProps(precision)) {
      return Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision);
    }

    return Number(num);
  };

  InputNumber.prototype.upStep = function upStep(val, rat) {
    var step = this.props.step;
    var precisionFactor = this.getPrecisionFactor(val, rat);
    var precision = Math.abs(this.getMaxPrecision(val, rat));
    var result = ((precisionFactor * val + precisionFactor * step * rat) / precisionFactor).toFixed(precision);
    return this.toNumber(result);
  };

  InputNumber.prototype.downStep = function downStep(val, rat) {
    var step = this.props.step;
    var precisionFactor = this.getPrecisionFactor(val, rat);
    var precision = Math.abs(this.getMaxPrecision(val, rat));
    var result = ((precisionFactor * val - precisionFactor * step * rat) / precisionFactor).toFixed(precision);
    return this.toNumber(result);
  };

  InputNumber.prototype.step = function step(type, e) {
    var _this2 = this;

    var ratio = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
    var recursive = arguments[3];
    this.stop();

    if (e) {
      e.persist();
      e.preventDefault();
    }

    var props = this.props;

    if (props.disabled) {
      return;
    }

    var value = this.getCurrentValidValue(this.state.inputValue) || 0;

    if (this.isNotCompleteNumber(value)) {
      return;
    }

    var val = this[type + 'Step'](value, ratio);
    var outOfRange = val > props.max || val < props.min;

    if (val > props.max) {
      val = props.max;
    } else if (val < props.min) {
      val = props.min;
    }

    this.setValue(val);
    this.setState({
      focused: true
    });

    if (outOfRange) {
      return;
    }

    this.autoStepTimer = setTimeout(function () {
      _this2[type](e, ratio, true);
    }, recursive ? SPEED : DELAY);
  };

  InputNumber.prototype.render = function render() {
    var _classNames;

    var props = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, this.props);

    var prefixCls = props.prefixCls,
        disabled = props.disabled,
        readOnly = props.readOnly,
        useTouch = props.useTouch,
        autoComplete = props.autoComplete,
        upHandler = props.upHandler,
        downHandler = props.downHandler,
        rest = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default()(props, ['prefixCls', 'disabled', 'readOnly', 'useTouch', 'autoComplete', 'upHandler', 'downHandler']);

    var classes = classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classNames = {}, _classNames[prefixCls] = true, _classNames[props.className] = !!props.className, _classNames[prefixCls + '-disabled'] = disabled, _classNames[prefixCls + '-focused'] = this.state.focused, _classNames));
    var upDisabledClass = '';
    var downDisabledClass = '';
    var value = this.state.value;

    if (value || value === 0) {
      if (!isNaN(value)) {
        var val = Number(value);

        if (val >= props.max) {
          upDisabledClass = prefixCls + '-handler-up-disabled';
        }

        if (val <= props.min) {
          downDisabledClass = prefixCls + '-handler-down-disabled';
        }
      } else {
        upDisabledClass = prefixCls + '-handler-up-disabled';
        downDisabledClass = prefixCls + '-handler-down-disabled';
      }
    }

    var dataOrAriaAttributeProps = {};

    for (var key in props) {
      if (props.hasOwnProperty(key) && (key.substr(0, 5) === 'data-' || key.substr(0, 5) === 'aria-' || key === 'role')) {
        dataOrAriaAttributeProps[key] = props[key];
      }
    }

    var editable = !props.readOnly && !props.disabled; // focus state, show input value
    // unfocus state, show valid value

    var inputDisplayValue = this.getInputDisplayValue();
    var upEvents = void 0;
    var downEvents = void 0;

    if (useTouch) {
      upEvents = {
        onTouchStart: editable && !upDisabledClass ? this.up : noop,
        onTouchEnd: this.stop
      };
      downEvents = {
        onTouchStart: editable && !downDisabledClass ? this.down : noop,
        onTouchEnd: this.stop
      };
    } else {
      upEvents = {
        onMouseDown: editable && !upDisabledClass ? this.up : noop,
        onMouseUp: this.stop,
        onMouseLeave: this.stop
      };
      downEvents = {
        onMouseDown: editable && !downDisabledClass ? this.down : noop,
        onMouseUp: this.stop,
        onMouseLeave: this.stop
      };
    }

    var isUpDisabled = !!upDisabledClass || disabled || readOnly;
    var isDownDisabled = !!downDisabledClass || disabled || readOnly; // ref for test

    return react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
      className: classes,
      style: props.style,
      title: props.title,
      onMouseEnter: props.onMouseEnter,
      onMouseLeave: props.onMouseLeave,
      onMouseOver: props.onMouseOver,
      onMouseOut: props.onMouseOut
    }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
      className: prefixCls + '-handler-wrap'
    }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_InputHandler__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
      ref: this.saveUp,
      disabled: isUpDisabled,
      prefixCls: prefixCls,
      unselectable: 'unselectable'
    }, upEvents, {
      role: 'button',
      'aria-label': 'Increase Value',
      'aria-disabled': !!isUpDisabled,
      className: prefixCls + '-handler ' + prefixCls + '-handler-up ' + upDisabledClass
    }), upHandler || react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
      unselectable: 'unselectable',
      className: prefixCls + '-handler-up-inner',
      onClick: preventDefault
    })), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_InputHandler__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
      ref: this.saveDown,
      disabled: isDownDisabled,
      prefixCls: prefixCls,
      unselectable: 'unselectable'
    }, downEvents, {
      role: 'button',
      'aria-label': 'Decrease Value',
      'aria-disabled': !!isDownDisabled,
      className: prefixCls + '-handler ' + prefixCls + '-handler-down ' + downDisabledClass
    }), downHandler || react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('span', {
      unselectable: 'unselectable',
      className: prefixCls + '-handler-down-inner',
      onClick: preventDefault
    }))), react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('div', {
      className: prefixCls + '-input-wrap'
    }, react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement('input', babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
      role: 'spinbutton',
      'aria-valuemin': props.min,
      'aria-valuemax': props.max,
      'aria-valuenow': value,
      required: props.required,
      type: props.type,
      placeholder: props.placeholder,
      onClick: props.onClick,
      onMouseUp: this.onMouseUp,
      className: prefixCls + '-input',
      tabIndex: props.tabIndex,
      autoComplete: autoComplete,
      onFocus: this.onFocus,
      onBlur: this.onBlur,
      onKeyDown: editable ? this.onKeyDown : noop,
      onKeyUp: editable ? this.onKeyUp : noop,
      autoFocus: props.autoFocus,
      maxLength: props.maxLength,
      readOnly: props.readOnly,
      disabled: props.disabled,
      max: props.max,
      min: props.min,
      step: props.step,
      name: props.name,
      id: props.id,
      onChange: this.onChange,
      ref: this.saveInput,
      value: inputDisplayValue,
      pattern: props.pattern
    }, dataOrAriaAttributeProps))));
  };

  return InputNumber;
}(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

InputNumber.propTypes = {
  value: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string]),
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string]),
  focusOnUpDown: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  autoFocus: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onPressEnter: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onKeyUp: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  tabIndex: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number]),
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  onFocus: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onBlur: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  readOnly: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  max: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  min: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  step: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number, prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string]),
  upHandler: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
  downHandler: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.node,
  useTouch: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  formatter: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  parser: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onMouseEnter: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onMouseLeave: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onMouseOver: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onMouseOut: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  onMouseUp: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  precision: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  required: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  pattern: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  decimalSeparator: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string
};
InputNumber.defaultProps = {
  focusOnUpDown: true,
  useTouch: false,
  prefixCls: 'rc-input-number',
  min: -MAX_SAFE_INTEGER,
  step: 1,
  style: {},
  onChange: noop,
  onKeyDown: noop,
  onPressEnter: noop,
  onFocus: noop,
  onBlur: noop,
  parser: defaultParser,
  required: false,
  autoComplete: 'off'
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.onKeyDown = function (e) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var _props3 = _this3.props,
        onKeyDown = _props3.onKeyDown,
        onPressEnter = _props3.onPressEnter;

    if (e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].UP) {
      var ratio = _this3.getRatio(e);

      _this3.up(e, ratio);

      _this3.stop();
    } else if (e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].DOWN) {
      var _ratio = _this3.getRatio(e);

      _this3.down(e, _ratio);

      _this3.stop();
    } else if (e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_8__["default"].ENTER && onPressEnter) {
      onPressEnter(e);
    } // Trigger user key down


    _this3.recordCursorPosition();

    _this3.lastKeyCode = e.keyCode;

    if (onKeyDown) {
      onKeyDown.apply(undefined, [e].concat(args));
    }
  };

  this.onKeyUp = function (e) {
    for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    var onKeyUp = _this3.props.onKeyUp;

    _this3.stop();

    _this3.recordCursorPosition(); // Trigger user key up


    if (onKeyUp) {
      onKeyUp.apply(undefined, [e].concat(args));
    }
  };

  this.onChange = function (e) {
    var onChange = _this3.props.onChange;

    if (_this3.state.focused) {
      _this3.inputting = true;
    }

    _this3.rawInput = _this3.props.parser(_this3.getValueFromEvent(e));

    _this3.setState({
      inputValue: _this3.rawInput
    });

    onChange(_this3.toNumber(_this3.rawInput)); // valid number or invalid string
  };

  this.onMouseUp = function () {
    var onMouseUp = _this3.props.onMouseUp;

    _this3.recordCursorPosition();

    if (onMouseUp) {
      onMouseUp.apply(undefined, arguments);
    }
  };

  this.onFocus = function () {
    var _props4;

    _this3.setState({
      focused: true
    });

    (_props4 = _this3.props).onFocus.apply(_props4, arguments);
  };

  this.onBlur = function (e) {
    for (var _len3 = arguments.length, args = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
      args[_key3 - 1] = arguments[_key3];
    }

    var onBlur = _this3.props.onBlur;
    _this3.inputting = false;

    _this3.setState({
      focused: false
    });

    var value = _this3.getCurrentValidValue(_this3.state.inputValue);

    e.persist(); // fix https://github.com/react-component/input-number/issues/51

    var newValue = _this3.setValue(value);

    if (onBlur) {
      var originValue = _this3.input.value;

      var inputValue = _this3.getInputDisplayValue({
        focus: false,
        value: newValue
      });

      _this3.input.value = inputValue;
      onBlur.apply(undefined, [e].concat(args));
      _this3.input.value = originValue;
    }
  };

  this.getInputDisplayValue = function (state) {
    var _ref = state || _this3.state,
        focused = _ref.focused,
        inputValue = _ref.inputValue,
        value = _ref.value;

    var inputDisplayValue = void 0;

    if (focused) {
      inputDisplayValue = inputValue;
    } else {
      inputDisplayValue = _this3.toPrecisionAsStep(value);
    }

    if (inputDisplayValue === undefined || inputDisplayValue === null) {
      inputDisplayValue = '';
    }

    var inputDisplayValueFormat = _this3.formatWrapper(inputDisplayValue);

    if (isValidProps(_this3.props.decimalSeparator)) {
      inputDisplayValueFormat = inputDisplayValueFormat.toString().replace('.', _this3.props.decimalSeparator);
    }

    return inputDisplayValueFormat;
  };

  this.recordCursorPosition = function () {
    // Record position
    try {
      _this3.cursorStart = _this3.input.selectionStart;
      _this3.cursorEnd = _this3.input.selectionEnd;
      _this3.currentValue = _this3.input.value;
      _this3.cursorBefore = _this3.input.value.substring(0, _this3.cursorStart);
      _this3.cursorAfter = _this3.input.value.substring(_this3.cursorEnd);
    } catch (e) {// Fix error in Chrome:
      // Failed to read the 'selectionStart' property from 'HTMLInputElement'
      // http://stackoverflow.com/q/21177489/3040605
    }
  };

  this.restoreByAfter = function (str) {
    if (str === undefined) return false;
    var fullStr = _this3.input.value;
    var index = fullStr.lastIndexOf(str);
    if (index === -1) return false;

    if (index + str.length === fullStr.length) {
      _this3.fixCaret(index, index);

      return true;
    }

    return false;
  };

  this.partRestoreByAfter = function (str) {
    if (str === undefined) return false; // For loop from full str to the str with last char to map. e.g. 123
    // -> 123
    // -> 23
    // -> 3

    return Array.prototype.some.call(str, function (_, start) {
      var partStr = str.substring(start);
      return _this3.restoreByAfter(partStr);
    });
  };

  this.stop = function () {
    if (_this3.autoStepTimer) {
      clearTimeout(_this3.autoStepTimer);
    }
  };

  this.down = function (e, ratio, recursive) {
    _this3.pressingUpOrDown = true;

    _this3.step('down', e, ratio, recursive);
  };

  this.up = function (e, ratio, recursive) {
    _this3.pressingUpOrDown = true;

    _this3.step('up', e, ratio, recursive);
  };

  this.saveUp = function (node) {
    _this3.upHandler = node;
  };

  this.saveDown = function (node) {
    _this3.downHandler = node;
  };

  this.saveInput = function (node) {
    _this3.input = node;
  };
};

/* harmony default export */ __webpack_exports__["default"] = (InputNumber);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtaW5wdXQtbnVtYmVyL2VzL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtaW5wdXQtbnVtYmVyL2VzL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJztcbmltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBLZXlDb2RlIGZyb20gJ3JjLXV0aWwvZXMvS2V5Q29kZSc7XG5pbXBvcnQgSW5wdXRIYW5kbGVyIGZyb20gJy4vSW5wdXRIYW5kbGVyJztcblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbmZ1bmN0aW9uIHByZXZlbnREZWZhdWx0KGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xufVxuXG5mdW5jdGlvbiBkZWZhdWx0UGFyc2VyKGlucHV0KSB7XG4gIHJldHVybiBpbnB1dC5yZXBsYWNlKC9bXlxcd1xcLi1dKy9nLCAnJyk7XG59XG5cbi8qKlxuICogV2hlbiBjbGljayBhbmQgaG9sZCBvbiBhIGJ1dHRvbiAtIHRoZSBzcGVlZCBvZiBhdXRvIGNoYW5naW4gdGhlIHZhbHVlLlxuICovXG52YXIgU1BFRUQgPSAyMDA7XG5cbi8qKlxuICogV2hlbiBjbGljayBhbmQgaG9sZCBvbiBhIGJ1dHRvbiAtIHRoZSBkZWxheSBiZWZvcmUgYXV0byBjaGFuZ2luIHRoZSB2YWx1ZS5cbiAqL1xudmFyIERFTEFZID0gNjAwO1xuXG4vKipcbiAqIE1heCBTYWZlIEludGVnZXIgLS0gb24gSUUgdGhpcyBpcyBub3QgYXZhaWxhYmxlLCBzbyBtYW51YWxseSBzZXQgdGhlIG51bWJlciBpbiB0aGF0IGNhc2UuXG4gKiBUaGUgcmVhc29uIHRoaXMgaXMgdXNlZCwgaW5zdGVhZCBvZiBJbmZpbml0eSBpcyBiZWNhdXNlIG51bWJlcnMgYWJvdmUgdGhlIE1TSSBhcmUgdW5zdGFibGVcbiAqL1xudmFyIE1BWF9TQUZFX0lOVEVHRVIgPSBOdW1iZXIuTUFYX1NBRkVfSU5URUdFUiB8fCBNYXRoLnBvdygyLCA1MykgLSAxO1xuXG52YXIgaXNWYWxpZFByb3BzID0gZnVuY3Rpb24gaXNWYWxpZFByb3BzKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPT0gdW5kZWZpbmVkICYmIHZhbHVlICE9PSBudWxsO1xufTtcblxudmFyIGlzRXF1YWwgPSBmdW5jdGlvbiBpc0VxdWFsKG9sZFZhbHVlLCBuZXdWYWx1ZSkge1xuICByZXR1cm4gbmV3VmFsdWUgPT09IG9sZFZhbHVlIHx8IHR5cGVvZiBuZXdWYWx1ZSA9PT0gJ251bWJlcicgJiYgdHlwZW9mIG9sZFZhbHVlID09PSAnbnVtYmVyJyAmJiBpc05hTihuZXdWYWx1ZSkgJiYgaXNOYU4ob2xkVmFsdWUpO1xufTtcblxudmFyIElucHV0TnVtYmVyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKElucHV0TnVtYmVyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJbnB1dE51bWJlcihwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBJbnB1dE51bWJlcik7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgIF9pbml0aWFsaXNlUHJvcHMuY2FsbChfdGhpcyk7XG5cbiAgICB2YXIgdmFsdWUgPSB2b2lkIDA7XG4gICAgaWYgKCd2YWx1ZScgaW4gcHJvcHMpIHtcbiAgICAgIHZhbHVlID0gcHJvcHMudmFsdWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlID0gcHJvcHMuZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGZvY3VzZWQ6IHByb3BzLmF1dG9Gb2N1c1xuICAgIH07XG4gICAgdmFyIHZhbGlkVmFsdWUgPSBfdGhpcy5nZXRWYWxpZFZhbHVlKF90aGlzLnRvTnVtYmVyKHZhbHVlKSk7XG4gICAgX3RoaXMuc3RhdGUgPSBfZXh0ZW5kcyh7fSwgX3RoaXMuc3RhdGUsIHtcbiAgICAgIGlucHV0VmFsdWU6IF90aGlzLnRvUHJlY2lzaW9uQXNTdGVwKHZhbGlkVmFsdWUpLFxuICAgICAgdmFsdWU6IHZhbGlkVmFsdWVcbiAgICB9KTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmNvbXBvbmVudERpZFVwZGF0ZSgpO1xuICB9O1xuXG4gIElucHV0TnVtYmVyLnByb3RvdHlwZS5jb21wb25lbnREaWRVcGRhdGUgPSBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHZhbHVlID0gX3Byb3BzLnZhbHVlLFxuICAgICAgICBvbkNoYW5nZSA9IF9wcm9wcy5vbkNoYW5nZSxcbiAgICAgICAgbWF4ID0gX3Byb3BzLm1heCxcbiAgICAgICAgbWluID0gX3Byb3BzLm1pbjtcbiAgICB2YXIgZm9jdXNlZCA9IHRoaXMuc3RhdGUuZm9jdXNlZDtcblxuICAgIC8vIERvbid0IHRyaWdnZXIgaW4gY29tcG9uZW50RGlkTW91bnRcblxuICAgIGlmIChwcmV2UHJvcHMpIHtcbiAgICAgIGlmICghaXNFcXVhbChwcmV2UHJvcHMudmFsdWUsIHZhbHVlKSB8fCAhaXNFcXVhbChwcmV2UHJvcHMubWF4LCBtYXgpIHx8ICFpc0VxdWFsKHByZXZQcm9wcy5taW4sIG1pbikpIHtcbiAgICAgICAgdmFyIHZhbGlkVmFsdWUgPSBmb2N1c2VkID8gdmFsdWUgOiB0aGlzLmdldFZhbGlkVmFsdWUodmFsdWUpO1xuICAgICAgICB2YXIgbmV4dElucHV0VmFsdWUgPSB2b2lkIDA7XG4gICAgICAgIGlmICh0aGlzLnByZXNzaW5nVXBPckRvd24pIHtcbiAgICAgICAgICBuZXh0SW5wdXRWYWx1ZSA9IHZhbGlkVmFsdWU7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5pbnB1dHRpbmcpIHtcbiAgICAgICAgICBuZXh0SW5wdXRWYWx1ZSA9IHRoaXMucmF3SW5wdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbmV4dElucHV0VmFsdWUgPSB0aGlzLnRvUHJlY2lzaW9uQXNTdGVwKHZhbGlkVmFsdWUpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgICAgICAgdmFsdWU6IHZhbGlkVmFsdWUsXG4gICAgICAgICAgaW5wdXRWYWx1ZTogbmV4dElucHV0VmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIFRyaWdnZXIgb25DaGFuZ2Ugd2hlbiBtYXggb3IgbWluIGNoYW5nZVxuICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTE1NzRcbiAgICAgIHZhciBuZXh0VmFsdWUgPSAndmFsdWUnIGluIHRoaXMucHJvcHMgPyB2YWx1ZSA6IHRoaXMuc3RhdGUudmFsdWU7XG4gICAgICAvLyByZWY6IG51bGwgPCAyMCA9PT0gdHJ1ZVxuICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTQyNzdcbiAgICAgIGlmICgnbWF4JyBpbiB0aGlzLnByb3BzICYmIHByZXZQcm9wcy5tYXggIT09IG1heCAmJiB0eXBlb2YgbmV4dFZhbHVlID09PSAnbnVtYmVyJyAmJiBuZXh0VmFsdWUgPiBtYXggJiYgb25DaGFuZ2UpIHtcbiAgICAgICAgb25DaGFuZ2UobWF4KTtcbiAgICAgIH1cbiAgICAgIGlmICgnbWluJyBpbiB0aGlzLnByb3BzICYmIHByZXZQcm9wcy5taW4gIT09IG1pbiAmJiB0eXBlb2YgbmV4dFZhbHVlID09PSAnbnVtYmVyJyAmJiBuZXh0VmFsdWUgPCBtaW4gJiYgb25DaGFuZ2UpIHtcbiAgICAgICAgb25DaGFuZ2UobWluKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBSZXN0b3JlIGN1cnNvclxuICAgIHRyeSB7XG4gICAgICAvLyBGaXJlZm94IHNldCB0aGUgaW5wdXQgY3Vyc29yIGFmdGVyIGl0IGdldCBmb2N1c2VkLlxuICAgICAgLy8gVGhpcyBjYXVzZWQgdGhhdCBpZiBhbiBpbnB1dCBkaWRuJ3QgaW5pdCB3aXRoIHRoZSBzZWxlY3Rpb24sXG4gICAgICAvLyBzZXQgd2lsbCBjYXVzZSBjdXJzb3Igbm90IGNvcnJlY3Qgd2hlbiBmaXJzdCBmb2N1cy5cbiAgICAgIC8vIFNhZmFyaSB3aWxsIGZvY3VzIGlucHV0IGlmIHNldCBzZWxlY3Rpb24uIFdlIG5lZWQgc2tpcCB0aGlzLlxuICAgICAgaWYgKHRoaXMuY3Vyc29yU3RhcnQgIT09IHVuZGVmaW5lZCAmJiB0aGlzLnN0YXRlLmZvY3VzZWQpIHtcbiAgICAgICAgLy8gSW4gbW9zdCBjYXNlcywgdGhlIHN0cmluZyBhZnRlciBjdXJzb3IgaXMgc3RhYmxlLlxuICAgICAgICAvLyBXZSBjYW4gbW92ZSB0aGUgY3Vyc29yIGJlZm9yZSBpdFxuXG4gICAgICAgIGlmIChcbiAgICAgICAgLy8gSWYgbm90IG1hdGNoIGZ1bGwgc3RyLCB0cnkgdG8gbWF0Y2ggcGFydCBvZiBzdHJcbiAgICAgICAgIXRoaXMucGFydFJlc3RvcmVCeUFmdGVyKHRoaXMuY3Vyc29yQWZ0ZXIpICYmIHRoaXMuc3RhdGUudmFsdWUgIT09IHRoaXMucHJvcHMudmFsdWUpIHtcbiAgICAgICAgICAvLyBJZiBub3QgbWF0Y2ggYW55IG9mIHRoZW4sIGxldCdzIGp1c3Qga2VlcCB0aGUgcG9zaXRpb25cbiAgICAgICAgICAvLyBUT0RPOiBMb2dpYyBzaG91bGQgbm90IHJlYWNoIGhlcmUsIG5lZWQgY2hlY2sgaWYgaGFwcGVuc1xuICAgICAgICAgIHZhciBwb3MgPSB0aGlzLmN1cnNvclN0YXJ0ICsgMTtcblxuICAgICAgICAgIC8vIElmIG5vdCBoYXZlIGxhc3Qgc3RyaW5nLCBqdXN0IHBvc2l0aW9uIHRvIHRoZSBlbmRcbiAgICAgICAgICBpZiAoIXRoaXMuY3Vyc29yQWZ0ZXIpIHtcbiAgICAgICAgICAgIHBvcyA9IHRoaXMuaW5wdXQudmFsdWUubGVuZ3RoO1xuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5sYXN0S2V5Q29kZSA9PT0gS2V5Q29kZS5CQUNLU1BBQ0UpIHtcbiAgICAgICAgICAgIHBvcyA9IHRoaXMuY3Vyc29yU3RhcnQgLSAxO1xuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5sYXN0S2V5Q29kZSA9PT0gS2V5Q29kZS5ERUxFVEUpIHtcbiAgICAgICAgICAgIHBvcyA9IHRoaXMuY3Vyc29yU3RhcnQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuZml4Q2FyZXQocG9zLCBwb3MpO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFZhbHVlID09PSB0aGlzLmlucHV0LnZhbHVlKSB7XG4gICAgICAgICAgLy8gSGFuZGxlIHNvbWUgc3BlY2lhbCBrZXkgY29kZVxuICAgICAgICAgIHN3aXRjaCAodGhpcy5sYXN0S2V5Q29kZSkge1xuICAgICAgICAgICAgY2FzZSBLZXlDb2RlLkJBQ0tTUEFDRTpcbiAgICAgICAgICAgICAgdGhpcy5maXhDYXJldCh0aGlzLmN1cnNvclN0YXJ0IC0gMSwgdGhpcy5jdXJzb3JTdGFydCAtIDEpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgS2V5Q29kZS5ERUxFVEU6XG4gICAgICAgICAgICAgIHRoaXMuZml4Q2FyZXQodGhpcy5jdXJzb3JTdGFydCArIDEsIHRoaXMuY3Vyc29yU3RhcnQgKyAxKTtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgLy8gRG8gbm90aGluZ1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHt9XG4gICAgLy8gRG8gbm90aGluZ1xuXG5cbiAgICAvLyBSZXNldCBsYXN0IGtleVxuICAgIHRoaXMubGFzdEtleUNvZGUgPSBudWxsO1xuXG4gICAgLy8gcHJlc3NpbmdVcE9yRG93biBpcyB0cnVlIG1lYW5zIHRoYXQgc29tZW9uZSBqdXN0IGNsaWNrIHVwIG9yIGRvd24gYnV0dG9uXG4gICAgaWYgKCF0aGlzLnByZXNzaW5nVXBPckRvd24pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaWYgKHRoaXMucHJvcHMuZm9jdXNPblVwRG93biAmJiB0aGlzLnN0YXRlLmZvY3VzZWQpIHtcbiAgICAgIGlmIChkb2N1bWVudC5hY3RpdmVFbGVtZW50ICE9PSB0aGlzLmlucHV0KSB7XG4gICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnByZXNzaW5nVXBPckRvd24gPSBmYWxzZTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuY29tcG9uZW50V2lsbFVubW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICB0aGlzLnN0b3AoKTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZ2V0Q3VycmVudFZhbGlkVmFsdWUgPSBmdW5jdGlvbiBnZXRDdXJyZW50VmFsaWRWYWx1ZSh2YWx1ZSkge1xuICAgIHZhciB2YWwgPSB2YWx1ZTtcbiAgICBpZiAodmFsID09PSAnJykge1xuICAgICAgdmFsID0gJyc7XG4gICAgfSBlbHNlIGlmICghdGhpcy5pc05vdENvbXBsZXRlTnVtYmVyKHBhcnNlRmxvYXQodmFsLCAxMCkpKSB7XG4gICAgICB2YWwgPSB0aGlzLmdldFZhbGlkVmFsdWUodmFsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFsID0gdGhpcy5zdGF0ZS52YWx1ZTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMudG9OdW1iZXIodmFsKTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZ2V0UmF0aW8gPSBmdW5jdGlvbiBnZXRSYXRpbyhlKSB7XG4gICAgdmFyIHJhdGlvID0gMTtcbiAgICBpZiAoZS5tZXRhS2V5IHx8IGUuY3RybEtleSkge1xuICAgICAgcmF0aW8gPSAwLjE7XG4gICAgfSBlbHNlIGlmIChlLnNoaWZ0S2V5KSB7XG4gICAgICByYXRpbyA9IDEwO1xuICAgIH1cbiAgICByZXR1cm4gcmF0aW87XG4gIH07XG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLmdldFZhbHVlRnJvbUV2ZW50ID0gZnVuY3Rpb24gZ2V0VmFsdWVGcm9tRXZlbnQoZSkge1xuICAgIC8vIG9wdGltaXplIGZvciBjaGluZXNlIGlucHV0IGV4cGllcmVuY2VcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy84MTk2XG4gICAgdmFyIHZhbHVlID0gZS50YXJnZXQudmFsdWUudHJpbSgpLnJlcGxhY2UoL+OAgi9nLCAnLicpO1xuXG4gICAgaWYgKGlzVmFsaWRQcm9wcyh0aGlzLnByb3BzLmRlY2ltYWxTZXBhcmF0b3IpKSB7XG4gICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UodGhpcy5wcm9wcy5kZWNpbWFsU2VwYXJhdG9yLCAnLicpO1xuICAgIH1cblxuICAgIHJldHVybiB2YWx1ZTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZ2V0VmFsaWRWYWx1ZSA9IGZ1bmN0aW9uIGdldFZhbGlkVmFsdWUodmFsdWUpIHtcbiAgICB2YXIgbWluID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB0aGlzLnByb3BzLm1pbjtcbiAgICB2YXIgbWF4ID0gYXJndW1lbnRzLmxlbmd0aCA+IDIgJiYgYXJndW1lbnRzWzJdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMl0gOiB0aGlzLnByb3BzLm1heDtcblxuICAgIHZhciB2YWwgPSBwYXJzZUZsb2F0KHZhbHVlLCAxMCk7XG4gICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvNzM1OFxuICAgIGlmIChpc05hTih2YWwpKSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuICAgIGlmICh2YWwgPCBtaW4pIHtcbiAgICAgIHZhbCA9IG1pbjtcbiAgICB9XG4gICAgaWYgKHZhbCA+IG1heCkge1xuICAgICAgdmFsID0gbWF4O1xuICAgIH1cbiAgICByZXR1cm4gdmFsO1xuICB9O1xuXG4gIElucHV0TnVtYmVyLnByb3RvdHlwZS5zZXRWYWx1ZSA9IGZ1bmN0aW9uIHNldFZhbHVlKHYsIGNhbGxiYWNrKSB7XG4gICAgLy8gdHJpZ2dlciBvbkNoYW5nZVxuICAgIHZhciBwcmVjaXNpb24gPSB0aGlzLnByb3BzLnByZWNpc2lvbjtcblxuICAgIHZhciBuZXdWYWx1ZSA9IHRoaXMuaXNOb3RDb21wbGV0ZU51bWJlcihwYXJzZUZsb2F0KHYsIDEwKSkgPyBudWxsIDogcGFyc2VGbG9hdCh2LCAxMCk7XG4gICAgdmFyIF9zdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgIF9zdGF0ZSR2YWx1ZSA9IF9zdGF0ZS52YWx1ZSxcbiAgICAgICAgdmFsdWUgPSBfc3RhdGUkdmFsdWUgPT09IHVuZGVmaW5lZCA/IG51bGwgOiBfc3RhdGUkdmFsdWUsXG4gICAgICAgIF9zdGF0ZSRpbnB1dFZhbHVlID0gX3N0YXRlLmlucHV0VmFsdWUsXG4gICAgICAgIGlucHV0VmFsdWUgPSBfc3RhdGUkaW5wdXRWYWx1ZSA9PT0gdW5kZWZpbmVkID8gbnVsbCA6IF9zdGF0ZSRpbnB1dFZhbHVlO1xuICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzczNjNcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNjYyMlxuXG4gICAgdmFyIG5ld1ZhbHVlSW5TdHJpbmcgPSB0eXBlb2YgbmV3VmFsdWUgPT09ICdudW1iZXInID8gbmV3VmFsdWUudG9GaXhlZChwcmVjaXNpb24pIDogJycgKyBuZXdWYWx1ZTtcbiAgICB2YXIgY2hhbmdlZCA9IG5ld1ZhbHVlICE9PSB2YWx1ZSB8fCBuZXdWYWx1ZUluU3RyaW5nICE9PSAnJyArIGlucHV0VmFsdWU7XG4gICAgaWYgKCEoJ3ZhbHVlJyBpbiB0aGlzLnByb3BzKSkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHZhbHVlOiBuZXdWYWx1ZSxcbiAgICAgICAgaW5wdXRWYWx1ZTogdGhpcy50b1ByZWNpc2lvbkFzU3RlcCh2KVxuICAgICAgfSwgY2FsbGJhY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBhbHdheXMgc2V0IGlucHV0IHZhbHVlIHNhbWUgYXMgdmFsdWVcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBpbnB1dFZhbHVlOiB0aGlzLnRvUHJlY2lzaW9uQXNTdGVwKHRoaXMuc3RhdGUudmFsdWUpXG4gICAgICB9LCBjYWxsYmFjayk7XG4gICAgfVxuICAgIGlmIChjaGFuZ2VkKSB7XG4gICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlKG5ld1ZhbHVlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbmV3VmFsdWU7XG4gIH07XG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLmdldFByZWNpc2lvbiA9IGZ1bmN0aW9uIGdldFByZWNpc2lvbih2YWx1ZSkge1xuICAgIGlmIChpc1ZhbGlkUHJvcHModGhpcy5wcm9wcy5wcmVjaXNpb24pKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy5wcmVjaXNpb247XG4gICAgfVxuICAgIHZhciB2YWx1ZVN0cmluZyA9IHZhbHVlLnRvU3RyaW5nKCk7XG4gICAgaWYgKHZhbHVlU3RyaW5nLmluZGV4T2YoJ2UtJykgPj0gMCkge1xuICAgICAgcmV0dXJuIHBhcnNlSW50KHZhbHVlU3RyaW5nLnNsaWNlKHZhbHVlU3RyaW5nLmluZGV4T2YoJ2UtJykgKyAyKSwgMTApO1xuICAgIH1cbiAgICB2YXIgcHJlY2lzaW9uID0gMDtcbiAgICBpZiAodmFsdWVTdHJpbmcuaW5kZXhPZignLicpID49IDApIHtcbiAgICAgIHByZWNpc2lvbiA9IHZhbHVlU3RyaW5nLmxlbmd0aCAtIHZhbHVlU3RyaW5nLmluZGV4T2YoJy4nKSAtIDE7XG4gICAgfVxuICAgIHJldHVybiBwcmVjaXNpb247XG4gIH07XG5cbiAgLy8gc3RlcD17MS4wfSB2YWx1ZT17MS41MX1cbiAgLy8gcHJlc3MgK1xuICAvLyB0aGVuIHZhbHVlIHNob3VsZCBiZSAyLjUxLCByYXRoZXIgdGhhbiAyLjVcbiAgLy8gaWYgdGhpcy5wcm9wcy5wcmVjaXNpb24gaXMgdW5kZWZpbmVkXG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvaW5wdXQtbnVtYmVyL2lzc3Vlcy8zOVxuXG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLmdldE1heFByZWNpc2lvbiA9IGZ1bmN0aW9uIGdldE1heFByZWNpc2lvbihjdXJyZW50VmFsdWUpIHtcbiAgICB2YXIgcmF0aW8gPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IDE7XG4gICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICBwcmVjaXNpb24gPSBfcHJvcHMyLnByZWNpc2lvbixcbiAgICAgICAgc3RlcCA9IF9wcm9wczIuc3RlcDtcblxuICAgIGlmIChpc1ZhbGlkUHJvcHMocHJlY2lzaW9uKSkge1xuICAgICAgcmV0dXJuIHByZWNpc2lvbjtcbiAgICB9XG4gICAgdmFyIHJhdGlvUHJlY2lzaW9uID0gdGhpcy5nZXRQcmVjaXNpb24ocmF0aW8pO1xuICAgIHZhciBzdGVwUHJlY2lzaW9uID0gdGhpcy5nZXRQcmVjaXNpb24oc3RlcCk7XG4gICAgdmFyIGN1cnJlbnRWYWx1ZVByZWNpc2lvbiA9IHRoaXMuZ2V0UHJlY2lzaW9uKGN1cnJlbnRWYWx1ZSk7XG4gICAgaWYgKCFjdXJyZW50VmFsdWUpIHtcbiAgICAgIHJldHVybiByYXRpb1ByZWNpc2lvbiArIHN0ZXBQcmVjaXNpb247XG4gICAgfVxuICAgIHJldHVybiBNYXRoLm1heChjdXJyZW50VmFsdWVQcmVjaXNpb24sIHJhdGlvUHJlY2lzaW9uICsgc3RlcFByZWNpc2lvbik7XG4gIH07XG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLmdldFByZWNpc2lvbkZhY3RvciA9IGZ1bmN0aW9uIGdldFByZWNpc2lvbkZhY3RvcihjdXJyZW50VmFsdWUpIHtcbiAgICB2YXIgcmF0aW8gPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IDE7XG5cbiAgICB2YXIgcHJlY2lzaW9uID0gdGhpcy5nZXRNYXhQcmVjaXNpb24oY3VycmVudFZhbHVlLCByYXRpbyk7XG4gICAgcmV0dXJuIE1hdGgucG93KDEwLCBwcmVjaXNpb24pO1xuICB9O1xuXG4gIElucHV0TnVtYmVyLnByb3RvdHlwZS5maXhDYXJldCA9IGZ1bmN0aW9uIGZpeENhcmV0KHN0YXJ0LCBlbmQpIHtcbiAgICBpZiAoc3RhcnQgPT09IHVuZGVmaW5lZCB8fCBlbmQgPT09IHVuZGVmaW5lZCB8fCAhdGhpcy5pbnB1dCB8fCAhdGhpcy5pbnB1dC52YWx1ZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRyeSB7XG4gICAgICB2YXIgY3VycmVudFN0YXJ0ID0gdGhpcy5pbnB1dC5zZWxlY3Rpb25TdGFydDtcbiAgICAgIHZhciBjdXJyZW50RW5kID0gdGhpcy5pbnB1dC5zZWxlY3Rpb25FbmQ7XG5cbiAgICAgIGlmIChzdGFydCAhPT0gY3VycmVudFN0YXJ0IHx8IGVuZCAhPT0gY3VycmVudEVuZCkge1xuICAgICAgICB0aGlzLmlucHV0LnNldFNlbGVjdGlvblJhbmdlKHN0YXJ0LCBlbmQpO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIC8vIEZpeCBlcnJvciBpbiBDaHJvbWU6XG4gICAgICAvLyBGYWlsZWQgdG8gcmVhZCB0aGUgJ3NlbGVjdGlvblN0YXJ0JyBwcm9wZXJ0eSBmcm9tICdIVE1MSW5wdXRFbGVtZW50J1xuICAgICAgLy8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3EvMjExNzc0ODkvMzA0MDYwNVxuICAgIH1cbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZm9jdXMgPSBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICB0aGlzLmlucHV0LmZvY3VzKCk7XG4gICAgdGhpcy5yZWNvcmRDdXJzb3JQb3NpdGlvbigpO1xuICB9O1xuXG4gIElucHV0TnVtYmVyLnByb3RvdHlwZS5ibHVyID0gZnVuY3Rpb24gYmx1cigpIHtcbiAgICB0aGlzLmlucHV0LmJsdXIoKTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZm9ybWF0V3JhcHBlciA9IGZ1bmN0aW9uIGZvcm1hdFdyYXBwZXIobnVtKSB7XG4gICAgLy8gaHR0cDovLzJhbGl0eS5jb20vMjAxMi8wMy9zaWduZWR6ZXJvLmh0bWxcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy85NDM5XG4gICAgaWYgKHRoaXMucHJvcHMuZm9ybWF0dGVyKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy5mb3JtYXR0ZXIobnVtKTtcbiAgICB9XG4gICAgcmV0dXJuIG51bTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUudG9QcmVjaXNpb25Bc1N0ZXAgPSBmdW5jdGlvbiB0b1ByZWNpc2lvbkFzU3RlcChudW0pIHtcbiAgICBpZiAodGhpcy5pc05vdENvbXBsZXRlTnVtYmVyKG51bSkgfHwgbnVtID09PSAnJykge1xuICAgICAgcmV0dXJuIG51bTtcbiAgICB9XG4gICAgdmFyIHByZWNpc2lvbiA9IE1hdGguYWJzKHRoaXMuZ2V0TWF4UHJlY2lzaW9uKG51bSkpO1xuICAgIGlmICghaXNOYU4ocHJlY2lzaW9uKSkge1xuICAgICAgcmV0dXJuIE51bWJlcihudW0pLnRvRml4ZWQocHJlY2lzaW9uKTtcbiAgICB9XG4gICAgcmV0dXJuIG51bS50b1N0cmluZygpO1xuICB9O1xuXG4gIC8vICcxLicgJzF4JyAneHgnICcnID0+IGFyZSBub3QgY29tcGxldGUgbnVtYmVyc1xuXG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLmlzTm90Q29tcGxldGVOdW1iZXIgPSBmdW5jdGlvbiBpc05vdENvbXBsZXRlTnVtYmVyKG51bSkge1xuICAgIHJldHVybiBpc05hTihudW0pIHx8IG51bSA9PT0gJycgfHwgbnVtID09PSBudWxsIHx8IG51bSAmJiBudW0udG9TdHJpbmcoKS5pbmRleE9mKCcuJykgPT09IG51bS50b1N0cmluZygpLmxlbmd0aCAtIDE7XG4gIH07XG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLnRvTnVtYmVyID0gZnVuY3Rpb24gdG9OdW1iZXIobnVtKSB7XG4gICAgdmFyIHByZWNpc2lvbiA9IHRoaXMucHJvcHMucHJlY2lzaW9uO1xuICAgIHZhciBmb2N1c2VkID0gdGhpcy5zdGF0ZS5mb2N1c2VkO1xuICAgIC8vIG51bS5sZW5ndGggPiAxNiA9PiBUaGlzIGlzIHRvIHByZXZlbnQgaW5wdXQgb2YgbGFyZ2UgbnVtYmVyc1xuXG4gICAgdmFyIG51bWJlcklzVG9vTGFyZ2UgPSBudW0gJiYgbnVtLmxlbmd0aCA+IDE2ICYmIGZvY3VzZWQ7XG4gICAgaWYgKHRoaXMuaXNOb3RDb21wbGV0ZU51bWJlcihudW0pIHx8IG51bWJlcklzVG9vTGFyZ2UpIHtcbiAgICAgIHJldHVybiBudW07XG4gICAgfVxuICAgIGlmIChpc1ZhbGlkUHJvcHMocHJlY2lzaW9uKSkge1xuICAgICAgcmV0dXJuIE1hdGgucm91bmQobnVtICogTWF0aC5wb3coMTAsIHByZWNpc2lvbikpIC8gTWF0aC5wb3coMTAsIHByZWNpc2lvbik7XG4gICAgfVxuICAgIHJldHVybiBOdW1iZXIobnVtKTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUudXBTdGVwID0gZnVuY3Rpb24gdXBTdGVwKHZhbCwgcmF0KSB7XG4gICAgdmFyIHN0ZXAgPSB0aGlzLnByb3BzLnN0ZXA7XG5cbiAgICB2YXIgcHJlY2lzaW9uRmFjdG9yID0gdGhpcy5nZXRQcmVjaXNpb25GYWN0b3IodmFsLCByYXQpO1xuICAgIHZhciBwcmVjaXNpb24gPSBNYXRoLmFicyh0aGlzLmdldE1heFByZWNpc2lvbih2YWwsIHJhdCkpO1xuICAgIHZhciByZXN1bHQgPSAoKHByZWNpc2lvbkZhY3RvciAqIHZhbCArIHByZWNpc2lvbkZhY3RvciAqIHN0ZXAgKiByYXQpIC8gcHJlY2lzaW9uRmFjdG9yKS50b0ZpeGVkKHByZWNpc2lvbik7XG4gICAgcmV0dXJuIHRoaXMudG9OdW1iZXIocmVzdWx0KTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUuZG93blN0ZXAgPSBmdW5jdGlvbiBkb3duU3RlcCh2YWwsIHJhdCkge1xuICAgIHZhciBzdGVwID0gdGhpcy5wcm9wcy5zdGVwO1xuXG4gICAgdmFyIHByZWNpc2lvbkZhY3RvciA9IHRoaXMuZ2V0UHJlY2lzaW9uRmFjdG9yKHZhbCwgcmF0KTtcbiAgICB2YXIgcHJlY2lzaW9uID0gTWF0aC5hYnModGhpcy5nZXRNYXhQcmVjaXNpb24odmFsLCByYXQpKTtcbiAgICB2YXIgcmVzdWx0ID0gKChwcmVjaXNpb25GYWN0b3IgKiB2YWwgLSBwcmVjaXNpb25GYWN0b3IgKiBzdGVwICogcmF0KSAvIHByZWNpc2lvbkZhY3RvcikudG9GaXhlZChwcmVjaXNpb24pO1xuICAgIHJldHVybiB0aGlzLnRvTnVtYmVyKHJlc3VsdCk7XG4gIH07XG5cbiAgSW5wdXROdW1iZXIucHJvdG90eXBlLnN0ZXAgPSBmdW5jdGlvbiBzdGVwKHR5cGUsIGUpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHZhciByYXRpbyA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogMTtcbiAgICB2YXIgcmVjdXJzaXZlID0gYXJndW1lbnRzWzNdO1xuXG4gICAgdGhpcy5zdG9wKCk7XG4gICAgaWYgKGUpIHtcbiAgICAgIGUucGVyc2lzdCgpO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cbiAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgIGlmIChwcm9wcy5kaXNhYmxlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgdmFsdWUgPSB0aGlzLmdldEN1cnJlbnRWYWxpZFZhbHVlKHRoaXMuc3RhdGUuaW5wdXRWYWx1ZSkgfHwgMDtcbiAgICBpZiAodGhpcy5pc05vdENvbXBsZXRlTnVtYmVyKHZhbHVlKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgdmFsID0gdGhpc1t0eXBlICsgJ1N0ZXAnXSh2YWx1ZSwgcmF0aW8pO1xuICAgIHZhciBvdXRPZlJhbmdlID0gdmFsID4gcHJvcHMubWF4IHx8IHZhbCA8IHByb3BzLm1pbjtcbiAgICBpZiAodmFsID4gcHJvcHMubWF4KSB7XG4gICAgICB2YWwgPSBwcm9wcy5tYXg7XG4gICAgfSBlbHNlIGlmICh2YWwgPCBwcm9wcy5taW4pIHtcbiAgICAgIHZhbCA9IHByb3BzLm1pbjtcbiAgICB9XG4gICAgdGhpcy5zZXRWYWx1ZSh2YWwpO1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgZm9jdXNlZDogdHJ1ZVxuICAgIH0pO1xuICAgIGlmIChvdXRPZlJhbmdlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuYXV0b1N0ZXBUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMyW3R5cGVdKGUsIHJhdGlvLCB0cnVlKTtcbiAgICB9LCByZWN1cnNpdmUgPyBTUEVFRCA6IERFTEFZKTtcbiAgfTtcblxuICBJbnB1dE51bWJlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgIHZhciBwcm9wcyA9IF9leHRlbmRzKHt9LCB0aGlzLnByb3BzKTtcblxuICAgIHZhciBwcmVmaXhDbHMgPSBwcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgIGRpc2FibGVkID0gcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgIHJlYWRPbmx5ID0gcHJvcHMucmVhZE9ubHksXG4gICAgICAgIHVzZVRvdWNoID0gcHJvcHMudXNlVG91Y2gsXG4gICAgICAgIGF1dG9Db21wbGV0ZSA9IHByb3BzLmF1dG9Db21wbGV0ZSxcbiAgICAgICAgdXBIYW5kbGVyID0gcHJvcHMudXBIYW5kbGVyLFxuICAgICAgICBkb3duSGFuZGxlciA9IHByb3BzLmRvd25IYW5kbGVyLFxuICAgICAgICByZXN0ID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKHByb3BzLCBbJ3ByZWZpeENscycsICdkaXNhYmxlZCcsICdyZWFkT25seScsICd1c2VUb3VjaCcsICdhdXRvQ29tcGxldGUnLCAndXBIYW5kbGVyJywgJ2Rvd25IYW5kbGVyJ10pO1xuXG4gICAgdmFyIGNsYXNzZXMgPSBjbGFzc05hbWVzKChfY2xhc3NOYW1lcyA9IHt9LCBfY2xhc3NOYW1lc1twcmVmaXhDbHNdID0gdHJ1ZSwgX2NsYXNzTmFtZXNbcHJvcHMuY2xhc3NOYW1lXSA9ICEhcHJvcHMuY2xhc3NOYW1lLCBfY2xhc3NOYW1lc1twcmVmaXhDbHMgKyAnLWRpc2FibGVkJ10gPSBkaXNhYmxlZCwgX2NsYXNzTmFtZXNbcHJlZml4Q2xzICsgJy1mb2N1c2VkJ10gPSB0aGlzLnN0YXRlLmZvY3VzZWQsIF9jbGFzc05hbWVzKSk7XG4gICAgdmFyIHVwRGlzYWJsZWRDbGFzcyA9ICcnO1xuICAgIHZhciBkb3duRGlzYWJsZWRDbGFzcyA9ICcnO1xuICAgIHZhciB2YWx1ZSA9IHRoaXMuc3RhdGUudmFsdWU7XG5cbiAgICBpZiAodmFsdWUgfHwgdmFsdWUgPT09IDApIHtcbiAgICAgIGlmICghaXNOYU4odmFsdWUpKSB7XG4gICAgICAgIHZhciB2YWwgPSBOdW1iZXIodmFsdWUpO1xuICAgICAgICBpZiAodmFsID49IHByb3BzLm1heCkge1xuICAgICAgICAgIHVwRGlzYWJsZWRDbGFzcyA9IHByZWZpeENscyArICctaGFuZGxlci11cC1kaXNhYmxlZCc7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHZhbCA8PSBwcm9wcy5taW4pIHtcbiAgICAgICAgICBkb3duRGlzYWJsZWRDbGFzcyA9IHByZWZpeENscyArICctaGFuZGxlci1kb3duLWRpc2FibGVkJztcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdXBEaXNhYmxlZENsYXNzID0gcHJlZml4Q2xzICsgJy1oYW5kbGVyLXVwLWRpc2FibGVkJztcbiAgICAgICAgZG93bkRpc2FibGVkQ2xhc3MgPSBwcmVmaXhDbHMgKyAnLWhhbmRsZXItZG93bi1kaXNhYmxlZCc7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGRhdGFPckFyaWFBdHRyaWJ1dGVQcm9wcyA9IHt9O1xuICAgIGZvciAodmFyIGtleSBpbiBwcm9wcykge1xuICAgICAgaWYgKHByb3BzLmhhc093blByb3BlcnR5KGtleSkgJiYgKGtleS5zdWJzdHIoMCwgNSkgPT09ICdkYXRhLScgfHwga2V5LnN1YnN0cigwLCA1KSA9PT0gJ2FyaWEtJyB8fCBrZXkgPT09ICdyb2xlJykpIHtcbiAgICAgICAgZGF0YU9yQXJpYUF0dHJpYnV0ZVByb3BzW2tleV0gPSBwcm9wc1trZXldO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBlZGl0YWJsZSA9ICFwcm9wcy5yZWFkT25seSAmJiAhcHJvcHMuZGlzYWJsZWQ7XG5cbiAgICAvLyBmb2N1cyBzdGF0ZSwgc2hvdyBpbnB1dCB2YWx1ZVxuICAgIC8vIHVuZm9jdXMgc3RhdGUsIHNob3cgdmFsaWQgdmFsdWVcbiAgICB2YXIgaW5wdXREaXNwbGF5VmFsdWUgPSB0aGlzLmdldElucHV0RGlzcGxheVZhbHVlKCk7XG5cbiAgICB2YXIgdXBFdmVudHMgPSB2b2lkIDA7XG4gICAgdmFyIGRvd25FdmVudHMgPSB2b2lkIDA7XG4gICAgaWYgKHVzZVRvdWNoKSB7XG4gICAgICB1cEV2ZW50cyA9IHtcbiAgICAgICAgb25Ub3VjaFN0YXJ0OiBlZGl0YWJsZSAmJiAhdXBEaXNhYmxlZENsYXNzID8gdGhpcy51cCA6IG5vb3AsXG4gICAgICAgIG9uVG91Y2hFbmQ6IHRoaXMuc3RvcFxuICAgICAgfTtcbiAgICAgIGRvd25FdmVudHMgPSB7XG4gICAgICAgIG9uVG91Y2hTdGFydDogZWRpdGFibGUgJiYgIWRvd25EaXNhYmxlZENsYXNzID8gdGhpcy5kb3duIDogbm9vcCxcbiAgICAgICAgb25Ub3VjaEVuZDogdGhpcy5zdG9wXG4gICAgICB9O1xuICAgIH0gZWxzZSB7XG4gICAgICB1cEV2ZW50cyA9IHtcbiAgICAgICAgb25Nb3VzZURvd246IGVkaXRhYmxlICYmICF1cERpc2FibGVkQ2xhc3MgPyB0aGlzLnVwIDogbm9vcCxcbiAgICAgICAgb25Nb3VzZVVwOiB0aGlzLnN0b3AsXG4gICAgICAgIG9uTW91c2VMZWF2ZTogdGhpcy5zdG9wXG4gICAgICB9O1xuICAgICAgZG93bkV2ZW50cyA9IHtcbiAgICAgICAgb25Nb3VzZURvd246IGVkaXRhYmxlICYmICFkb3duRGlzYWJsZWRDbGFzcyA/IHRoaXMuZG93biA6IG5vb3AsXG4gICAgICAgIG9uTW91c2VVcDogdGhpcy5zdG9wLFxuICAgICAgICBvbk1vdXNlTGVhdmU6IHRoaXMuc3RvcFxuICAgICAgfTtcbiAgICB9XG5cbiAgICB2YXIgaXNVcERpc2FibGVkID0gISF1cERpc2FibGVkQ2xhc3MgfHwgZGlzYWJsZWQgfHwgcmVhZE9ubHk7XG4gICAgdmFyIGlzRG93bkRpc2FibGVkID0gISFkb3duRGlzYWJsZWRDbGFzcyB8fCBkaXNhYmxlZCB8fCByZWFkT25seTtcbiAgICAvLyByZWYgZm9yIHRlc3RcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICdkaXYnLFxuICAgICAge1xuICAgICAgICBjbGFzc05hbWU6IGNsYXNzZXMsXG4gICAgICAgIHN0eWxlOiBwcm9wcy5zdHlsZSxcbiAgICAgICAgdGl0bGU6IHByb3BzLnRpdGxlLFxuICAgICAgICBvbk1vdXNlRW50ZXI6IHByb3BzLm9uTW91c2VFbnRlcixcbiAgICAgICAgb25Nb3VzZUxlYXZlOiBwcm9wcy5vbk1vdXNlTGVhdmUsXG4gICAgICAgIG9uTW91c2VPdmVyOiBwcm9wcy5vbk1vdXNlT3ZlcixcbiAgICAgICAgb25Nb3VzZU91dDogcHJvcHMub25Nb3VzZU91dFxuICAgICAgfSxcbiAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1oYW5kbGVyLXdyYXAnIH0sXG4gICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgSW5wdXRIYW5kbGVyLFxuICAgICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICAgIHJlZjogdGhpcy5zYXZlVXAsXG4gICAgICAgICAgICBkaXNhYmxlZDogaXNVcERpc2FibGVkLFxuICAgICAgICAgICAgcHJlZml4Q2xzOiBwcmVmaXhDbHMsXG4gICAgICAgICAgICB1bnNlbGVjdGFibGU6ICd1bnNlbGVjdGFibGUnXG4gICAgICAgICAgfSwgdXBFdmVudHMsIHtcbiAgICAgICAgICAgIHJvbGU6ICdidXR0b24nLFxuICAgICAgICAgICAgJ2FyaWEtbGFiZWwnOiAnSW5jcmVhc2UgVmFsdWUnLFxuICAgICAgICAgICAgJ2FyaWEtZGlzYWJsZWQnOiAhIWlzVXBEaXNhYmxlZCxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1oYW5kbGVyICcgKyBwcmVmaXhDbHMgKyAnLWhhbmRsZXItdXAgJyArIHVwRGlzYWJsZWRDbGFzc1xuICAgICAgICAgIH0pLFxuICAgICAgICAgIHVwSGFuZGxlciB8fCBSZWFjdC5jcmVhdGVFbGVtZW50KCdzcGFuJywge1xuICAgICAgICAgICAgdW5zZWxlY3RhYmxlOiAndW5zZWxlY3RhYmxlJyxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1oYW5kbGVyLXVwLWlubmVyJyxcbiAgICAgICAgICAgIG9uQ2xpY2s6IHByZXZlbnREZWZhdWx0XG4gICAgICAgICAgfSlcbiAgICAgICAgKSxcbiAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBJbnB1dEhhbmRsZXIsXG4gICAgICAgICAgX2V4dGVuZHMoe1xuICAgICAgICAgICAgcmVmOiB0aGlzLnNhdmVEb3duLFxuICAgICAgICAgICAgZGlzYWJsZWQ6IGlzRG93bkRpc2FibGVkLFxuICAgICAgICAgICAgcHJlZml4Q2xzOiBwcmVmaXhDbHMsXG4gICAgICAgICAgICB1bnNlbGVjdGFibGU6ICd1bnNlbGVjdGFibGUnXG4gICAgICAgICAgfSwgZG93bkV2ZW50cywge1xuICAgICAgICAgICAgcm9sZTogJ2J1dHRvbicsXG4gICAgICAgICAgICAnYXJpYS1sYWJlbCc6ICdEZWNyZWFzZSBWYWx1ZScsXG4gICAgICAgICAgICAnYXJpYS1kaXNhYmxlZCc6ICEhaXNEb3duRGlzYWJsZWQsXG4gICAgICAgICAgICBjbGFzc05hbWU6IHByZWZpeENscyArICctaGFuZGxlciAnICsgcHJlZml4Q2xzICsgJy1oYW5kbGVyLWRvd24gJyArIGRvd25EaXNhYmxlZENsYXNzXG4gICAgICAgICAgfSksXG4gICAgICAgICAgZG93bkhhbmRsZXIgfHwgUmVhY3QuY3JlYXRlRWxlbWVudCgnc3BhbicsIHtcbiAgICAgICAgICAgIHVuc2VsZWN0YWJsZTogJ3Vuc2VsZWN0YWJsZScsXG4gICAgICAgICAgICBjbGFzc05hbWU6IHByZWZpeENscyArICctaGFuZGxlci1kb3duLWlubmVyJyxcbiAgICAgICAgICAgIG9uQ2xpY2s6IHByZXZlbnREZWZhdWx0XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKSxcbiAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWlucHV0LXdyYXAnXG4gICAgICAgIH0sXG4gICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoJ2lucHV0JywgX2V4dGVuZHMoe1xuICAgICAgICAgIHJvbGU6ICdzcGluYnV0dG9uJyxcbiAgICAgICAgICAnYXJpYS12YWx1ZW1pbic6IHByb3BzLm1pbixcbiAgICAgICAgICAnYXJpYS12YWx1ZW1heCc6IHByb3BzLm1heCxcbiAgICAgICAgICAnYXJpYS12YWx1ZW5vdyc6IHZhbHVlLFxuICAgICAgICAgIHJlcXVpcmVkOiBwcm9wcy5yZXF1aXJlZCxcbiAgICAgICAgICB0eXBlOiBwcm9wcy50eXBlLFxuICAgICAgICAgIHBsYWNlaG9sZGVyOiBwcm9wcy5wbGFjZWhvbGRlcixcbiAgICAgICAgICBvbkNsaWNrOiBwcm9wcy5vbkNsaWNrLFxuICAgICAgICAgIG9uTW91c2VVcDogdGhpcy5vbk1vdXNlVXAsXG4gICAgICAgICAgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWlucHV0JyxcbiAgICAgICAgICB0YWJJbmRleDogcHJvcHMudGFiSW5kZXgsXG4gICAgICAgICAgYXV0b0NvbXBsZXRlOiBhdXRvQ29tcGxldGUsXG4gICAgICAgICAgb25Gb2N1czogdGhpcy5vbkZvY3VzLFxuICAgICAgICAgIG9uQmx1cjogdGhpcy5vbkJsdXIsXG4gICAgICAgICAgb25LZXlEb3duOiBlZGl0YWJsZSA/IHRoaXMub25LZXlEb3duIDogbm9vcCxcbiAgICAgICAgICBvbktleVVwOiBlZGl0YWJsZSA/IHRoaXMub25LZXlVcCA6IG5vb3AsXG4gICAgICAgICAgYXV0b0ZvY3VzOiBwcm9wcy5hdXRvRm9jdXMsXG4gICAgICAgICAgbWF4TGVuZ3RoOiBwcm9wcy5tYXhMZW5ndGgsXG4gICAgICAgICAgcmVhZE9ubHk6IHByb3BzLnJlYWRPbmx5LFxuICAgICAgICAgIGRpc2FibGVkOiBwcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICBtYXg6IHByb3BzLm1heCxcbiAgICAgICAgICBtaW46IHByb3BzLm1pbixcbiAgICAgICAgICBzdGVwOiBwcm9wcy5zdGVwLFxuICAgICAgICAgIG5hbWU6IHByb3BzLm5hbWUsXG4gICAgICAgICAgaWQ6IHByb3BzLmlkLFxuICAgICAgICAgIG9uQ2hhbmdlOiB0aGlzLm9uQ2hhbmdlLFxuICAgICAgICAgIHJlZjogdGhpcy5zYXZlSW5wdXQsXG4gICAgICAgICAgdmFsdWU6IGlucHV0RGlzcGxheVZhbHVlLFxuICAgICAgICAgIHBhdHRlcm46IHByb3BzLnBhdHRlcm5cbiAgICAgICAgfSwgZGF0YU9yQXJpYUF0dHJpYnV0ZVByb3BzKSlcbiAgICAgIClcbiAgICApO1xuICB9O1xuXG4gIHJldHVybiBJbnB1dE51bWJlcjtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuSW5wdXROdW1iZXIucHJvcFR5cGVzID0ge1xuICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICBkZWZhdWx0VmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5udW1iZXIsIFByb3BUeXBlcy5zdHJpbmddKSxcbiAgZm9jdXNPblVwRG93bjogUHJvcFR5cGVzLmJvb2wsXG4gIGF1dG9Gb2N1czogUHJvcFR5cGVzLmJvb2wsXG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25QcmVzc0VudGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25LZXlEb3duOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25LZXlVcDogUHJvcFR5cGVzLmZ1bmMsXG4gIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgdGFiSW5kZXg6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKSxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICBvbkZvY3VzOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25CbHVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgcmVhZE9ubHk6IFByb3BUeXBlcy5ib29sLFxuICBtYXg6IFByb3BUeXBlcy5udW1iZXIsXG4gIG1pbjogUHJvcFR5cGVzLm51bWJlcixcbiAgc3RlcDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICB1cEhhbmRsZXI6IFByb3BUeXBlcy5ub2RlLFxuICBkb3duSGFuZGxlcjogUHJvcFR5cGVzLm5vZGUsXG4gIHVzZVRvdWNoOiBQcm9wVHlwZXMuYm9vbCxcbiAgZm9ybWF0dGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgcGFyc2VyOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Nb3VzZUVudGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Nb3VzZUxlYXZlOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Nb3VzZU92ZXI6IFByb3BUeXBlcy5mdW5jLFxuICBvbk1vdXNlT3V0OiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Nb3VzZVVwOiBQcm9wVHlwZXMuZnVuYyxcbiAgcHJlY2lzaW9uOiBQcm9wVHlwZXMubnVtYmVyLFxuICByZXF1aXJlZDogUHJvcFR5cGVzLmJvb2wsXG4gIHBhdHRlcm46IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRlY2ltYWxTZXBhcmF0b3I6IFByb3BUeXBlcy5zdHJpbmdcbn07XG5JbnB1dE51bWJlci5kZWZhdWx0UHJvcHMgPSB7XG4gIGZvY3VzT25VcERvd246IHRydWUsXG4gIHVzZVRvdWNoOiBmYWxzZSxcbiAgcHJlZml4Q2xzOiAncmMtaW5wdXQtbnVtYmVyJyxcbiAgbWluOiAtTUFYX1NBRkVfSU5URUdFUixcbiAgc3RlcDogMSxcbiAgc3R5bGU6IHt9LFxuICBvbkNoYW5nZTogbm9vcCxcbiAgb25LZXlEb3duOiBub29wLFxuICBvblByZXNzRW50ZXI6IG5vb3AsXG4gIG9uRm9jdXM6IG5vb3AsXG4gIG9uQmx1cjogbm9vcCxcbiAgcGFyc2VyOiBkZWZhdWx0UGFyc2VyLFxuICByZXF1aXJlZDogZmFsc2UsXG4gIGF1dG9Db21wbGV0ZTogJ29mZidcbn07XG5cbnZhciBfaW5pdGlhbGlzZVByb3BzID0gZnVuY3Rpb24gX2luaXRpYWxpc2VQcm9wcygpIHtcbiAgdmFyIF90aGlzMyA9IHRoaXM7XG5cbiAgdGhpcy5vbktleURvd24gPSBmdW5jdGlvbiAoZSkge1xuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiA+IDEgPyBfbGVuIC0gMSA6IDApLCBfa2V5ID0gMTsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgdmFyIF9wcm9wczMgPSBfdGhpczMucHJvcHMsXG4gICAgICAgIG9uS2V5RG93biA9IF9wcm9wczMub25LZXlEb3duLFxuICAgICAgICBvblByZXNzRW50ZXIgPSBfcHJvcHMzLm9uUHJlc3NFbnRlcjtcblxuXG4gICAgaWYgKGUua2V5Q29kZSA9PT0gS2V5Q29kZS5VUCkge1xuICAgICAgdmFyIHJhdGlvID0gX3RoaXMzLmdldFJhdGlvKGUpO1xuICAgICAgX3RoaXMzLnVwKGUsIHJhdGlvKTtcbiAgICAgIF90aGlzMy5zdG9wKCk7XG4gICAgfSBlbHNlIGlmIChlLmtleUNvZGUgPT09IEtleUNvZGUuRE9XTikge1xuICAgICAgdmFyIF9yYXRpbyA9IF90aGlzMy5nZXRSYXRpbyhlKTtcbiAgICAgIF90aGlzMy5kb3duKGUsIF9yYXRpbyk7XG4gICAgICBfdGhpczMuc3RvcCgpO1xuICAgIH0gZWxzZSBpZiAoZS5rZXlDb2RlID09PSBLZXlDb2RlLkVOVEVSICYmIG9uUHJlc3NFbnRlcikge1xuICAgICAgb25QcmVzc0VudGVyKGUpO1xuICAgIH1cblxuICAgIC8vIFRyaWdnZXIgdXNlciBrZXkgZG93blxuICAgIF90aGlzMy5yZWNvcmRDdXJzb3JQb3NpdGlvbigpO1xuICAgIF90aGlzMy5sYXN0S2V5Q29kZSA9IGUua2V5Q29kZTtcbiAgICBpZiAob25LZXlEb3duKSB7XG4gICAgICBvbktleURvd24uYXBwbHkodW5kZWZpbmVkLCBbZV0uY29uY2F0KGFyZ3MpKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5vbktleVVwID0gZnVuY3Rpb24gKGUpIHtcbiAgICBmb3IgKHZhciBfbGVuMiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuMiA+IDEgPyBfbGVuMiAtIDEgOiAwKSwgX2tleTIgPSAxOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICBhcmdzW19rZXkyIC0gMV0gPSBhcmd1bWVudHNbX2tleTJdO1xuICAgIH1cblxuICAgIHZhciBvbktleVVwID0gX3RoaXMzLnByb3BzLm9uS2V5VXA7XG5cblxuICAgIF90aGlzMy5zdG9wKCk7XG5cbiAgICBfdGhpczMucmVjb3JkQ3Vyc29yUG9zaXRpb24oKTtcblxuICAgIC8vIFRyaWdnZXIgdXNlciBrZXkgdXBcbiAgICBpZiAob25LZXlVcCkge1xuICAgICAgb25LZXlVcC5hcHBseSh1bmRlZmluZWQsIFtlXS5jb25jYXQoYXJncykpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLm9uQ2hhbmdlID0gZnVuY3Rpb24gKGUpIHtcbiAgICB2YXIgb25DaGFuZ2UgPSBfdGhpczMucHJvcHMub25DaGFuZ2U7XG5cblxuICAgIGlmIChfdGhpczMuc3RhdGUuZm9jdXNlZCkge1xuICAgICAgX3RoaXMzLmlucHV0dGluZyA9IHRydWU7XG4gICAgfVxuICAgIF90aGlzMy5yYXdJbnB1dCA9IF90aGlzMy5wcm9wcy5wYXJzZXIoX3RoaXMzLmdldFZhbHVlRnJvbUV2ZW50KGUpKTtcbiAgICBfdGhpczMuc2V0U3RhdGUoeyBpbnB1dFZhbHVlOiBfdGhpczMucmF3SW5wdXQgfSk7XG4gICAgb25DaGFuZ2UoX3RoaXMzLnRvTnVtYmVyKF90aGlzMy5yYXdJbnB1dCkpOyAvLyB2YWxpZCBudW1iZXIgb3IgaW52YWxpZCBzdHJpbmdcbiAgfTtcblxuICB0aGlzLm9uTW91c2VVcCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgb25Nb3VzZVVwID0gX3RoaXMzLnByb3BzLm9uTW91c2VVcDtcblxuXG4gICAgX3RoaXMzLnJlY29yZEN1cnNvclBvc2l0aW9uKCk7XG5cbiAgICBpZiAob25Nb3VzZVVwKSB7XG4gICAgICBvbk1vdXNlVXAuYXBwbHkodW5kZWZpbmVkLCBhcmd1bWVudHMpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLm9uRm9jdXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF9wcm9wczQ7XG5cbiAgICBfdGhpczMuc2V0U3RhdGUoe1xuICAgICAgZm9jdXNlZDogdHJ1ZVxuICAgIH0pO1xuICAgIChfcHJvcHM0ID0gX3RoaXMzLnByb3BzKS5vbkZvY3VzLmFwcGx5KF9wcm9wczQsIGFyZ3VtZW50cyk7XG4gIH07XG5cbiAgdGhpcy5vbkJsdXIgPSBmdW5jdGlvbiAoZSkge1xuICAgIGZvciAodmFyIF9sZW4zID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4zID4gMSA/IF9sZW4zIC0gMSA6IDApLCBfa2V5MyA9IDE7IF9rZXkzIDwgX2xlbjM7IF9rZXkzKyspIHtcbiAgICAgIGFyZ3NbX2tleTMgLSAxXSA9IGFyZ3VtZW50c1tfa2V5M107XG4gICAgfVxuXG4gICAgdmFyIG9uQmx1ciA9IF90aGlzMy5wcm9wcy5vbkJsdXI7XG5cbiAgICBfdGhpczMuaW5wdXR0aW5nID0gZmFsc2U7XG4gICAgX3RoaXMzLnNldFN0YXRlKHtcbiAgICAgIGZvY3VzZWQ6IGZhbHNlXG4gICAgfSk7XG4gICAgdmFyIHZhbHVlID0gX3RoaXMzLmdldEN1cnJlbnRWYWxpZFZhbHVlKF90aGlzMy5zdGF0ZS5pbnB1dFZhbHVlKTtcbiAgICBlLnBlcnNpc3QoKTsgLy8gZml4IGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvaW5wdXQtbnVtYmVyL2lzc3Vlcy81MVxuICAgIHZhciBuZXdWYWx1ZSA9IF90aGlzMy5zZXRWYWx1ZSh2YWx1ZSk7XG5cbiAgICBpZiAob25CbHVyKSB7XG4gICAgICB2YXIgb3JpZ2luVmFsdWUgPSBfdGhpczMuaW5wdXQudmFsdWU7XG4gICAgICB2YXIgaW5wdXRWYWx1ZSA9IF90aGlzMy5nZXRJbnB1dERpc3BsYXlWYWx1ZSh7IGZvY3VzOiBmYWxzZSwgdmFsdWU6IG5ld1ZhbHVlIH0pO1xuICAgICAgX3RoaXMzLmlucHV0LnZhbHVlID0gaW5wdXRWYWx1ZTtcbiAgICAgIG9uQmx1ci5hcHBseSh1bmRlZmluZWQsIFtlXS5jb25jYXQoYXJncykpO1xuICAgICAgX3RoaXMzLmlucHV0LnZhbHVlID0gb3JpZ2luVmFsdWU7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuZ2V0SW5wdXREaXNwbGF5VmFsdWUgPSBmdW5jdGlvbiAoc3RhdGUpIHtcbiAgICB2YXIgX3JlZiA9IHN0YXRlIHx8IF90aGlzMy5zdGF0ZSxcbiAgICAgICAgZm9jdXNlZCA9IF9yZWYuZm9jdXNlZCxcbiAgICAgICAgaW5wdXRWYWx1ZSA9IF9yZWYuaW5wdXRWYWx1ZSxcbiAgICAgICAgdmFsdWUgPSBfcmVmLnZhbHVlO1xuXG4gICAgdmFyIGlucHV0RGlzcGxheVZhbHVlID0gdm9pZCAwO1xuICAgIGlmIChmb2N1c2VkKSB7XG4gICAgICBpbnB1dERpc3BsYXlWYWx1ZSA9IGlucHV0VmFsdWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlucHV0RGlzcGxheVZhbHVlID0gX3RoaXMzLnRvUHJlY2lzaW9uQXNTdGVwKHZhbHVlKTtcbiAgICB9XG5cbiAgICBpZiAoaW5wdXREaXNwbGF5VmFsdWUgPT09IHVuZGVmaW5lZCB8fCBpbnB1dERpc3BsYXlWYWx1ZSA9PT0gbnVsbCkge1xuICAgICAgaW5wdXREaXNwbGF5VmFsdWUgPSAnJztcbiAgICB9XG5cbiAgICB2YXIgaW5wdXREaXNwbGF5VmFsdWVGb3JtYXQgPSBfdGhpczMuZm9ybWF0V3JhcHBlcihpbnB1dERpc3BsYXlWYWx1ZSk7XG4gICAgaWYgKGlzVmFsaWRQcm9wcyhfdGhpczMucHJvcHMuZGVjaW1hbFNlcGFyYXRvcikpIHtcbiAgICAgIGlucHV0RGlzcGxheVZhbHVlRm9ybWF0ID0gaW5wdXREaXNwbGF5VmFsdWVGb3JtYXQudG9TdHJpbmcoKS5yZXBsYWNlKCcuJywgX3RoaXMzLnByb3BzLmRlY2ltYWxTZXBhcmF0b3IpO1xuICAgIH1cblxuICAgIHJldHVybiBpbnB1dERpc3BsYXlWYWx1ZUZvcm1hdDtcbiAgfTtcblxuICB0aGlzLnJlY29yZEN1cnNvclBvc2l0aW9uID0gZnVuY3Rpb24gKCkge1xuICAgIC8vIFJlY29yZCBwb3NpdGlvblxuICAgIHRyeSB7XG4gICAgICBfdGhpczMuY3Vyc29yU3RhcnQgPSBfdGhpczMuaW5wdXQuc2VsZWN0aW9uU3RhcnQ7XG4gICAgICBfdGhpczMuY3Vyc29yRW5kID0gX3RoaXMzLmlucHV0LnNlbGVjdGlvbkVuZDtcbiAgICAgIF90aGlzMy5jdXJyZW50VmFsdWUgPSBfdGhpczMuaW5wdXQudmFsdWU7XG4gICAgICBfdGhpczMuY3Vyc29yQmVmb3JlID0gX3RoaXMzLmlucHV0LnZhbHVlLnN1YnN0cmluZygwLCBfdGhpczMuY3Vyc29yU3RhcnQpO1xuICAgICAgX3RoaXMzLmN1cnNvckFmdGVyID0gX3RoaXMzLmlucHV0LnZhbHVlLnN1YnN0cmluZyhfdGhpczMuY3Vyc29yRW5kKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICAvLyBGaXggZXJyb3IgaW4gQ2hyb21lOlxuICAgICAgLy8gRmFpbGVkIHRvIHJlYWQgdGhlICdzZWxlY3Rpb25TdGFydCcgcHJvcGVydHkgZnJvbSAnSFRNTElucHV0RWxlbWVudCdcbiAgICAgIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xLzIxMTc3NDg5LzMwNDA2MDVcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5yZXN0b3JlQnlBZnRlciA9IGZ1bmN0aW9uIChzdHIpIHtcbiAgICBpZiAoc3RyID09PSB1bmRlZmluZWQpIHJldHVybiBmYWxzZTtcblxuICAgIHZhciBmdWxsU3RyID0gX3RoaXMzLmlucHV0LnZhbHVlO1xuICAgIHZhciBpbmRleCA9IGZ1bGxTdHIubGFzdEluZGV4T2Yoc3RyKTtcblxuICAgIGlmIChpbmRleCA9PT0gLTEpIHJldHVybiBmYWxzZTtcblxuICAgIGlmIChpbmRleCArIHN0ci5sZW5ndGggPT09IGZ1bGxTdHIubGVuZ3RoKSB7XG4gICAgICBfdGhpczMuZml4Q2FyZXQoaW5kZXgsIGluZGV4KTtcblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfTtcblxuICB0aGlzLnBhcnRSZXN0b3JlQnlBZnRlciA9IGZ1bmN0aW9uIChzdHIpIHtcbiAgICBpZiAoc3RyID09PSB1bmRlZmluZWQpIHJldHVybiBmYWxzZTtcblxuICAgIC8vIEZvciBsb29wIGZyb20gZnVsbCBzdHIgdG8gdGhlIHN0ciB3aXRoIGxhc3QgY2hhciB0byBtYXAuIGUuZy4gMTIzXG4gICAgLy8gLT4gMTIzXG4gICAgLy8gLT4gMjNcbiAgICAvLyAtPiAzXG4gICAgcmV0dXJuIEFycmF5LnByb3RvdHlwZS5zb21lLmNhbGwoc3RyLCBmdW5jdGlvbiAoXywgc3RhcnQpIHtcbiAgICAgIHZhciBwYXJ0U3RyID0gc3RyLnN1YnN0cmluZyhzdGFydCk7XG5cbiAgICAgIHJldHVybiBfdGhpczMucmVzdG9yZUJ5QWZ0ZXIocGFydFN0cik7XG4gICAgfSk7XG4gIH07XG5cbiAgdGhpcy5zdG9wID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChfdGhpczMuYXV0b1N0ZXBUaW1lcikge1xuICAgICAgY2xlYXJUaW1lb3V0KF90aGlzMy5hdXRvU3RlcFRpbWVyKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5kb3duID0gZnVuY3Rpb24gKGUsIHJhdGlvLCByZWN1cnNpdmUpIHtcbiAgICBfdGhpczMucHJlc3NpbmdVcE9yRG93biA9IHRydWU7XG4gICAgX3RoaXMzLnN0ZXAoJ2Rvd24nLCBlLCByYXRpbywgcmVjdXJzaXZlKTtcbiAgfTtcblxuICB0aGlzLnVwID0gZnVuY3Rpb24gKGUsIHJhdGlvLCByZWN1cnNpdmUpIHtcbiAgICBfdGhpczMucHJlc3NpbmdVcE9yRG93biA9IHRydWU7XG4gICAgX3RoaXMzLnN0ZXAoJ3VwJywgZSwgcmF0aW8sIHJlY3Vyc2l2ZSk7XG4gIH07XG5cbiAgdGhpcy5zYXZlVXAgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgIF90aGlzMy51cEhhbmRsZXIgPSBub2RlO1xuICB9O1xuXG4gIHRoaXMuc2F2ZURvd24gPSBmdW5jdGlvbiAobm9kZSkge1xuICAgIF90aGlzMy5kb3duSGFuZGxlciA9IG5vZGU7XG4gIH07XG5cbiAgdGhpcy5zYXZlSW5wdXQgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgIF90aGlzMy5pbnB1dCA9IG5vZGU7XG4gIH07XG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbnB1dE51bWJlcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUdBO0FBRUE7Ozs7QUFHQTtBQUVBOzs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBV0E7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUhBO0FBVUE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE3QkE7QUFpQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEvQkE7QUFpQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-input-number/es/index.js
