__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuoteIarService", function() { return QuoteIarService; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");




var QuoteIarService =
/*#__PURE__*/
function () {
  function QuoteIarService() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, QuoteIarService);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(QuoteIarService, null, [{
    key: "fetchLeaderList",
    value: function fetchLeaderList() {
      var postData = {
        key: "",
        stakeholderTypes: ["INSURER"],
        pageSize: 20
      };
      return _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].post("".concat(_common__WEBPACK_IMPORTED_MODULE_3__["Apis"].RI_STAKEHOLDERSQ), postData).then(function (res) {
        return lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(res, "body.respData.items").map(function (item) {
          return {
            id: item.stakeholderId,
            text: item.name
          };
        });
      });
    }
  }]);

  return QuoteIarService;
}();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvc2VydmljZS50cy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL3NlcnZpY2UudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcyB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmV4cG9ydCBjbGFzcyBRdW90ZUlhclNlcnZpY2Uge1xuICBzdGF0aWMgZmV0Y2hMZWFkZXJMaXN0KCkge1xuICAgIGNvbnN0IHBvc3REYXRhOiBhbnkgPSB7XG4gICAgICBrZXk6IFwiXCIsXG4gICAgICBzdGFrZWhvbGRlclR5cGVzOiBbXCJJTlNVUkVSXCJdLFxuICAgICAgcGFnZVNpemU6IDIwLFxuICAgIH07XG4gICAgcmV0dXJuIEFqYXgucG9zdChgJHtBcGlzLlJJX1NUQUtFSE9MREVSU1F9YCwgcG9zdERhdGEpLnRoZW4ocmVzID0+IF8uZ2V0KHJlcywgXCJib2R5LnJlc3BEYXRhLml0ZW1zXCIpLm1hcCgoaXRlbTogYW55KSA9PiAoe1xuICAgICAgaWQ6IGl0ZW0uc3Rha2Vob2xkZXJJZCxcbiAgICAgIHRleHQ6IGl0ZW0ubmFtZSxcbiAgICB9KSkpO1xuICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBSUE7QUFYQTtBQUNBO0FBREE7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/service.ts
