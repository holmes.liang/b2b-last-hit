/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Text = __webpack_require__(/*! zrender/lib/graphic/Text */ "./node_modules/zrender/lib/graphic/Text.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// import Group from 'zrender/src/container/Group';

/**
 * @alias module:echarts/component/tooltip/TooltipRichContent
 * @constructor
 */


function TooltipRichContent(api) {
  this._zr = api.getZr();
  this._show = false;
  /**
   * @private
   */

  this._hideTimeout;
}

TooltipRichContent.prototype = {
  constructor: TooltipRichContent,

  /**
   * @private
   * @type {boolean}
   */
  _enterable: true,

  /**
   * Update when tooltip is rendered
   */
  update: function update() {// noop
  },
  show: function show(tooltipModel) {
    if (this._hideTimeout) {
      clearTimeout(this._hideTimeout);
    }

    this.el.attr('show', true);
    this._show = true;
  },

  /**
   * Set tooltip content
   *
   * @param {string} content rich text string of content
   * @param {Object} markerRich rich text style
   * @param {Object} tooltipModel tooltip model
   */
  setContent: function setContent(content, markerRich, tooltipModel) {
    if (this.el) {
      this._zr.remove(this.el);
    }

    var markers = {};
    var text = content;
    var prefix = '{marker';
    var suffix = '|}';
    var startId = text.indexOf(prefix);

    while (startId >= 0) {
      var endId = text.indexOf(suffix);
      var name = text.substr(startId + prefix.length, endId - startId - prefix.length);

      if (name.indexOf('sub') > -1) {
        markers['marker' + name] = {
          textWidth: 4,
          textHeight: 4,
          textBorderRadius: 2,
          textBackgroundColor: markerRich[name],
          // TODO: textOffset is not implemented for rich text
          textOffset: [3, 0]
        };
      } else {
        markers['marker' + name] = {
          textWidth: 10,
          textHeight: 10,
          textBorderRadius: 5,
          textBackgroundColor: markerRich[name]
        };
      }

      text = text.substr(endId + 1);
      startId = text.indexOf('{marker');
    }

    this.el = new Text({
      style: {
        rich: markers,
        text: content,
        textLineHeight: 20,
        textBackgroundColor: tooltipModel.get('backgroundColor'),
        textBorderRadius: tooltipModel.get('borderRadius'),
        textFill: tooltipModel.get('textStyle.color'),
        textPadding: tooltipModel.get('padding')
      },
      z: tooltipModel.get('z')
    });

    this._zr.add(this.el);

    var self = this;
    this.el.on('mouseover', function () {
      // clear the timeout in hideLater and keep showing tooltip
      if (self._enterable) {
        clearTimeout(self._hideTimeout);
        self._show = true;
      }

      self._inContent = true;
    });
    this.el.on('mouseout', function () {
      if (self._enterable) {
        if (self._show) {
          self.hideLater(self._hideDelay);
        }
      }

      self._inContent = false;
    });
  },
  setEnterable: function setEnterable(enterable) {
    this._enterable = enterable;
  },
  getSize: function getSize() {
    var bounding = this.el.getBoundingRect();
    return [bounding.width, bounding.height];
  },
  moveTo: function moveTo(x, y) {
    if (this.el) {
      this.el.attr('position', [x, y]);
    }
  },
  hide: function hide() {
    this.el.hide();
    this._show = false;
  },
  hideLater: function hideLater(time) {
    if (this._show && !(this._inContent && this._enterable)) {
      if (time) {
        this._hideDelay = time; // Set show false to avoid invoke hideLater mutiple times

        this._show = false;
        this._hideTimeout = setTimeout(zrUtil.bind(this.hide, this), time);
      } else {
        this.hide();
      }
    }
  },
  isShow: function isShow() {
    return this._show;
  },
  getOuterSize: function getOuterSize() {
    return this.getSize();
  }
};
var _default = TooltipRichContent;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcFJpY2hDb250ZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcFJpY2hDb250ZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIFRleHQgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9UZXh0XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG4vLyBpbXBvcnQgR3JvdXAgZnJvbSAnenJlbmRlci9zcmMvY29udGFpbmVyL0dyb3VwJztcblxuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvY29tcG9uZW50L3Rvb2x0aXAvVG9vbHRpcFJpY2hDb250ZW50XG4gKiBAY29uc3RydWN0b3JcbiAqL1xuZnVuY3Rpb24gVG9vbHRpcFJpY2hDb250ZW50KGFwaSkge1xuICB0aGlzLl96ciA9IGFwaS5nZXRacigpO1xuICB0aGlzLl9zaG93ID0gZmFsc2U7XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB0aGlzLl9oaWRlVGltZW91dDtcbn1cblxuVG9vbHRpcFJpY2hDb250ZW50LnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IFRvb2x0aXBSaWNoQ29udGVudCxcblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqL1xuICBfZW50ZXJhYmxlOiB0cnVlLFxuXG4gIC8qKlxuICAgKiBVcGRhdGUgd2hlbiB0b29sdGlwIGlzIHJlbmRlcmVkXG4gICAqL1xuICB1cGRhdGU6IGZ1bmN0aW9uICgpIHsvLyBub29wXG4gIH0sXG4gIHNob3c6IGZ1bmN0aW9uICh0b29sdGlwTW9kZWwpIHtcbiAgICBpZiAodGhpcy5faGlkZVRpbWVvdXQpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl9oaWRlVGltZW91dCk7XG4gICAgfVxuXG4gICAgdGhpcy5lbC5hdHRyKCdzaG93JywgdHJ1ZSk7XG4gICAgdGhpcy5fc2hvdyA9IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIFNldCB0b29sdGlwIGNvbnRlbnRcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbnRlbnQgcmljaCB0ZXh0IHN0cmluZyBvZiBjb250ZW50XG4gICAqIEBwYXJhbSB7T2JqZWN0fSBtYXJrZXJSaWNoIHJpY2ggdGV4dCBzdHlsZVxuICAgKiBAcGFyYW0ge09iamVjdH0gdG9vbHRpcE1vZGVsIHRvb2x0aXAgbW9kZWxcbiAgICovXG4gIHNldENvbnRlbnQ6IGZ1bmN0aW9uIChjb250ZW50LCBtYXJrZXJSaWNoLCB0b29sdGlwTW9kZWwpIHtcbiAgICBpZiAodGhpcy5lbCkge1xuICAgICAgdGhpcy5fenIucmVtb3ZlKHRoaXMuZWwpO1xuICAgIH1cblxuICAgIHZhciBtYXJrZXJzID0ge307XG4gICAgdmFyIHRleHQgPSBjb250ZW50O1xuICAgIHZhciBwcmVmaXggPSAne21hcmtlcic7XG4gICAgdmFyIHN1ZmZpeCA9ICd8fSc7XG4gICAgdmFyIHN0YXJ0SWQgPSB0ZXh0LmluZGV4T2YocHJlZml4KTtcblxuICAgIHdoaWxlIChzdGFydElkID49IDApIHtcbiAgICAgIHZhciBlbmRJZCA9IHRleHQuaW5kZXhPZihzdWZmaXgpO1xuICAgICAgdmFyIG5hbWUgPSB0ZXh0LnN1YnN0cihzdGFydElkICsgcHJlZml4Lmxlbmd0aCwgZW5kSWQgLSBzdGFydElkIC0gcHJlZml4Lmxlbmd0aCk7XG5cbiAgICAgIGlmIChuYW1lLmluZGV4T2YoJ3N1YicpID4gLTEpIHtcbiAgICAgICAgbWFya2Vyc1snbWFya2VyJyArIG5hbWVdID0ge1xuICAgICAgICAgIHRleHRXaWR0aDogNCxcbiAgICAgICAgICB0ZXh0SGVpZ2h0OiA0LFxuICAgICAgICAgIHRleHRCb3JkZXJSYWRpdXM6IDIsXG4gICAgICAgICAgdGV4dEJhY2tncm91bmRDb2xvcjogbWFya2VyUmljaFtuYW1lXSxcbiAgICAgICAgICAvLyBUT0RPOiB0ZXh0T2Zmc2V0IGlzIG5vdCBpbXBsZW1lbnRlZCBmb3IgcmljaCB0ZXh0XG4gICAgICAgICAgdGV4dE9mZnNldDogWzMsIDBdXG4gICAgICAgIH07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBtYXJrZXJzWydtYXJrZXInICsgbmFtZV0gPSB7XG4gICAgICAgICAgdGV4dFdpZHRoOiAxMCxcbiAgICAgICAgICB0ZXh0SGVpZ2h0OiAxMCxcbiAgICAgICAgICB0ZXh0Qm9yZGVyUmFkaXVzOiA1LFxuICAgICAgICAgIHRleHRCYWNrZ3JvdW5kQ29sb3I6IG1hcmtlclJpY2hbbmFtZV1cbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgdGV4dCA9IHRleHQuc3Vic3RyKGVuZElkICsgMSk7XG4gICAgICBzdGFydElkID0gdGV4dC5pbmRleE9mKCd7bWFya2VyJyk7XG4gICAgfVxuXG4gICAgdGhpcy5lbCA9IG5ldyBUZXh0KHtcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIHJpY2g6IG1hcmtlcnMsXG4gICAgICAgIHRleHQ6IGNvbnRlbnQsXG4gICAgICAgIHRleHRMaW5lSGVpZ2h0OiAyMCxcbiAgICAgICAgdGV4dEJhY2tncm91bmRDb2xvcjogdG9vbHRpcE1vZGVsLmdldCgnYmFja2dyb3VuZENvbG9yJyksXG4gICAgICAgIHRleHRCb3JkZXJSYWRpdXM6IHRvb2x0aXBNb2RlbC5nZXQoJ2JvcmRlclJhZGl1cycpLFxuICAgICAgICB0ZXh0RmlsbDogdG9vbHRpcE1vZGVsLmdldCgndGV4dFN0eWxlLmNvbG9yJyksXG4gICAgICAgIHRleHRQYWRkaW5nOiB0b29sdGlwTW9kZWwuZ2V0KCdwYWRkaW5nJylcbiAgICAgIH0sXG4gICAgICB6OiB0b29sdGlwTW9kZWwuZ2V0KCd6JylcbiAgICB9KTtcblxuICAgIHRoaXMuX3pyLmFkZCh0aGlzLmVsKTtcblxuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICB0aGlzLmVsLm9uKCdtb3VzZW92ZXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBjbGVhciB0aGUgdGltZW91dCBpbiBoaWRlTGF0ZXIgYW5kIGtlZXAgc2hvd2luZyB0b29sdGlwXG4gICAgICBpZiAoc2VsZi5fZW50ZXJhYmxlKSB7XG4gICAgICAgIGNsZWFyVGltZW91dChzZWxmLl9oaWRlVGltZW91dCk7XG4gICAgICAgIHNlbGYuX3Nob3cgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICBzZWxmLl9pbkNvbnRlbnQgPSB0cnVlO1xuICAgIH0pO1xuICAgIHRoaXMuZWwub24oJ21vdXNlb3V0JywgZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHNlbGYuX2VudGVyYWJsZSkge1xuICAgICAgICBpZiAoc2VsZi5fc2hvdykge1xuICAgICAgICAgIHNlbGYuaGlkZUxhdGVyKHNlbGYuX2hpZGVEZWxheSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgc2VsZi5faW5Db250ZW50ID0gZmFsc2U7XG4gICAgfSk7XG4gIH0sXG4gIHNldEVudGVyYWJsZTogZnVuY3Rpb24gKGVudGVyYWJsZSkge1xuICAgIHRoaXMuX2VudGVyYWJsZSA9IGVudGVyYWJsZTtcbiAgfSxcbiAgZ2V0U2l6ZTogZnVuY3Rpb24gKCkge1xuICAgIHZhciBib3VuZGluZyA9IHRoaXMuZWwuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgcmV0dXJuIFtib3VuZGluZy53aWR0aCwgYm91bmRpbmcuaGVpZ2h0XTtcbiAgfSxcbiAgbW92ZVRvOiBmdW5jdGlvbiAoeCwgeSkge1xuICAgIGlmICh0aGlzLmVsKSB7XG4gICAgICB0aGlzLmVsLmF0dHIoJ3Bvc2l0aW9uJywgW3gsIHldKTtcbiAgICB9XG4gIH0sXG4gIGhpZGU6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmVsLmhpZGUoKTtcbiAgICB0aGlzLl9zaG93ID0gZmFsc2U7XG4gIH0sXG4gIGhpZGVMYXRlcjogZnVuY3Rpb24gKHRpbWUpIHtcbiAgICBpZiAodGhpcy5fc2hvdyAmJiAhKHRoaXMuX2luQ29udGVudCAmJiB0aGlzLl9lbnRlcmFibGUpKSB7XG4gICAgICBpZiAodGltZSkge1xuICAgICAgICB0aGlzLl9oaWRlRGVsYXkgPSB0aW1lOyAvLyBTZXQgc2hvdyBmYWxzZSB0byBhdm9pZCBpbnZva2UgaGlkZUxhdGVyIG11dGlwbGUgdGltZXNcblxuICAgICAgICB0aGlzLl9zaG93ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2hpZGVUaW1lb3V0ID0gc2V0VGltZW91dCh6clV0aWwuYmluZCh0aGlzLmhpZGUsIHRoaXMpLCB0aW1lKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgaXNTaG93OiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3Nob3c7XG4gIH0sXG4gIGdldE91dGVyU2l6ZTogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLmdldFNpemUoKTtcbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IFRvb2x0aXBSaWNoQ29udGVudDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFWQTtBQUNBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZJQTtBQXlJQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/tooltip/TooltipRichContent.js
