__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};








var SpinSizes = Object(_util_type__WEBPACK_IMPORTED_MODULE_6__["tuple"])('small', 'default', 'large'); // Render indicator

var defaultIndicator = null;

function renderIndicator(prefixCls, props) {
  var indicator = props.indicator;
  var dotClassName = "".concat(prefixCls, "-dot"); // should not be render default indicator when indicator value is null

  if (indicator === null) {
    return null;
  }

  if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](indicator)) {
    return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](indicator, {
      className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(indicator.props.className, dotClassName)
    });
  }

  if (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](defaultIndicator)) {
    return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](defaultIndicator, {
      className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(defaultIndicator.props.className, dotClassName)
    });
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(dotClassName, "".concat(prefixCls, "-dot-spin"))
  }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("i", {
    className: "".concat(prefixCls, "-dot-item")
  }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("i", {
    className: "".concat(prefixCls, "-dot-item")
  }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("i", {
    className: "".concat(prefixCls, "-dot-item")
  }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("i", {
    className: "".concat(prefixCls, "-dot-item")
  }));
}

function shouldDelay(spinning, delay) {
  return !!spinning && !!delay && !isNaN(Number(delay));
}

var Spin =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Spin, _React$Component);

  function Spin(props) {
    var _this;

    _classCallCheck(this, Spin);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Spin).call(this, props));

    _this.debouncifyUpdateSpinning = function (props) {
      var _ref = props || _this.props,
          delay = _ref.delay;

      if (delay) {
        _this.cancelExistingSpin();

        _this.updateSpinning = lodash_debounce__WEBPACK_IMPORTED_MODULE_4___default()(_this.originalUpdateSpinning, delay);
      }
    };

    _this.updateSpinning = function () {
      var spinning = _this.props.spinning;
      var currentSpinning = _this.state.spinning;

      if (currentSpinning !== spinning) {
        _this.setState({
          spinning: spinning
        });
      }
    };

    _this.renderSpin = function (_ref2) {
      var _classNames;

      var getPrefixCls = _ref2.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          className = _a.className,
          size = _a.size,
          tip = _a.tip,
          wrapperClassName = _a.wrapperClassName,
          style = _a.style,
          restProps = __rest(_a, ["prefixCls", "className", "size", "tip", "wrapperClassName", "style"]);

      var spinning = _this.state.spinning;
      var prefixCls = getPrefixCls('spin', customizePrefixCls);
      var spinClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-sm"), size === 'small'), _defineProperty(_classNames, "".concat(prefixCls, "-lg"), size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-spinning"), spinning), _defineProperty(_classNames, "".concat(prefixCls, "-show-text"), !!tip), _classNames), className); // fix https://fb.me/react-unknown-prop

      var divProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(restProps, ['spinning', 'delay', 'indicator']);
      var spinElement = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, divProps, {
        style: style,
        className: spinClassName
      }), renderIndicator(prefixCls, _this.props), tip ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-text")
      }, tip) : null);

      if (_this.isNestedPattern()) {
        var containerClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-container"), _defineProperty({}, "".concat(prefixCls, "-blur"), spinning));
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, divProps, {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-nested-loading"), wrapperClassName)
        }), spinning && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          key: "loading"
        }, spinElement), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
          className: containerClassName,
          key: "container"
        }, _this.props.children));
      }

      return spinElement;
    };

    var spinning = props.spinning,
        delay = props.delay;
    var shouldBeDelayed = shouldDelay(spinning, delay);
    _this.state = {
      spinning: spinning && !shouldBeDelayed
    };
    _this.originalUpdateSpinning = _this.updateSpinning;

    _this.debouncifyUpdateSpinning(props);

    return _this;
  }

  _createClass(Spin, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.updateSpinning();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.debouncifyUpdateSpinning();
      this.updateSpinning();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.cancelExistingSpin();
    }
  }, {
    key: "cancelExistingSpin",
    value: function cancelExistingSpin() {
      var updateSpinning = this.updateSpinning;

      if (updateSpinning && updateSpinning.cancel) {
        updateSpinning.cancel();
      }
    }
  }, {
    key: "isNestedPattern",
    value: function isNestedPattern() {
      return !!(this.props && this.props.children);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderSpin);
    }
  }], [{
    key: "setDefaultIndicator",
    value: function setDefaultIndicator(indicator) {
      defaultIndicator = indicator;
    }
  }]);

  return Spin;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Spin.defaultProps = {
  spinning: true,
  size: 'default',
  wrapperClassName: ''
};
Spin.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  className: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  spinning: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  size: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](SpinSizes),
  wrapperClassName: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  indicator: prop_types__WEBPACK_IMPORTED_MODULE_1__["element"]
};
/* harmony default export */ __webpack_exports__["default"] = (Spin);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zcGluL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcGluL2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgKiBhcyBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IGRlYm91bmNlIGZyb20gJ2xvZGFzaC9kZWJvdW5jZSc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgeyB0dXBsZSB9IGZyb20gJy4uL191dGlsL3R5cGUnO1xuY29uc3QgU3BpblNpemVzID0gdHVwbGUoJ3NtYWxsJywgJ2RlZmF1bHQnLCAnbGFyZ2UnKTtcbi8vIFJlbmRlciBpbmRpY2F0b3JcbmxldCBkZWZhdWx0SW5kaWNhdG9yID0gbnVsbDtcbmZ1bmN0aW9uIHJlbmRlckluZGljYXRvcihwcmVmaXhDbHMsIHByb3BzKSB7XG4gICAgY29uc3QgeyBpbmRpY2F0b3IgfSA9IHByb3BzO1xuICAgIGNvbnN0IGRvdENsYXNzTmFtZSA9IGAke3ByZWZpeENsc30tZG90YDtcbiAgICAvLyBzaG91bGQgbm90IGJlIHJlbmRlciBkZWZhdWx0IGluZGljYXRvciB3aGVuIGluZGljYXRvciB2YWx1ZSBpcyBudWxsXG4gICAgaWYgKGluZGljYXRvciA9PT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGluZGljYXRvcikpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChpbmRpY2F0b3IsIHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhpbmRpY2F0b3IucHJvcHMuY2xhc3NOYW1lLCBkb3RDbGFzc05hbWUpLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgaWYgKFJlYWN0LmlzVmFsaWRFbGVtZW50KGRlZmF1bHRJbmRpY2F0b3IpKSB7XG4gICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoZGVmYXVsdEluZGljYXRvciwge1xuICAgICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKGRlZmF1bHRJbmRpY2F0b3IucHJvcHMuY2xhc3NOYW1lLCBkb3RDbGFzc05hbWUpLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuICg8c3BhbiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoZG90Q2xhc3NOYW1lLCBgJHtwcmVmaXhDbHN9LWRvdC1zcGluYCl9PlxuICAgICAgPGkgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWRvdC1pdGVtYH0vPlxuICAgICAgPGkgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWRvdC1pdGVtYH0vPlxuICAgICAgPGkgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWRvdC1pdGVtYH0vPlxuICAgICAgPGkgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWRvdC1pdGVtYH0vPlxuICAgIDwvc3Bhbj4pO1xufVxuZnVuY3Rpb24gc2hvdWxkRGVsYXkoc3Bpbm5pbmcsIGRlbGF5KSB7XG4gICAgcmV0dXJuICEhc3Bpbm5pbmcgJiYgISFkZWxheSAmJiAhaXNOYU4oTnVtYmVyKGRlbGF5KSk7XG59XG5jbGFzcyBTcGluIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuZGVib3VuY2lmeVVwZGF0ZVNwaW5uaW5nID0gKHByb3BzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGRlbGF5IH0gPSBwcm9wcyB8fCB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGRlbGF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jYW5jZWxFeGlzdGluZ1NwaW4oKTtcbiAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVNwaW5uaW5nID0gZGVib3VuY2UodGhpcy5vcmlnaW5hbFVwZGF0ZVNwaW5uaW5nLCBkZWxheSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMudXBkYXRlU3Bpbm5pbmcgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHNwaW5uaW5nIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgeyBzcGlubmluZzogY3VycmVudFNwaW5uaW5nIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgaWYgKGN1cnJlbnRTcGlubmluZyAhPT0gc3Bpbm5pbmcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgc3Bpbm5pbmcgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyU3BpbiA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNsYXNzTmFtZSwgc2l6ZSwgdGlwLCB3cmFwcGVyQ2xhc3NOYW1lLCBzdHlsZSB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwiY2xhc3NOYW1lXCIsIFwic2l6ZVwiLCBcInRpcFwiLCBcIndyYXBwZXJDbGFzc05hbWVcIiwgXCJzdHlsZVwiXSk7XG4gICAgICAgICAgICBjb25zdCB7IHNwaW5uaW5nIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdzcGluJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IHNwaW5DbGFzc05hbWUgPSBjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXNtYF06IHNpemUgPT09ICdzbWFsbCcsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tbGdgXTogc2l6ZSA9PT0gJ2xhcmdlJyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1zcGlubmluZ2BdOiBzcGlubmluZyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1zaG93LXRleHRgXTogISF0aXAsXG4gICAgICAgICAgICB9LCBjbGFzc05hbWUpO1xuICAgICAgICAgICAgLy8gZml4IGh0dHBzOi8vZmIubWUvcmVhY3QtdW5rbm93bi1wcm9wXG4gICAgICAgICAgICBjb25zdCBkaXZQcm9wcyA9IG9taXQocmVzdFByb3BzLCBbJ3NwaW5uaW5nJywgJ2RlbGF5JywgJ2luZGljYXRvciddKTtcbiAgICAgICAgICAgIGNvbnN0IHNwaW5FbGVtZW50ID0gKDxkaXYgey4uLmRpdlByb3BzfSBzdHlsZT17c3R5bGV9IGNsYXNzTmFtZT17c3BpbkNsYXNzTmFtZX0+XG4gICAgICAgIHtyZW5kZXJJbmRpY2F0b3IocHJlZml4Q2xzLCB0aGlzLnByb3BzKX1cbiAgICAgICAge3RpcCA/IDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXRleHRgfT57dGlwfTwvZGl2PiA6IG51bGx9XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgaWYgKHRoaXMuaXNOZXN0ZWRQYXR0ZXJuKCkpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjb250YWluZXJDbGFzc05hbWUgPSBjbGFzc05hbWVzKGAke3ByZWZpeENsc30tY29udGFpbmVyYCwge1xuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1ibHVyYF06IHNwaW5uaW5nLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiAoPGRpdiB7Li4uZGl2UHJvcHN9IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LW5lc3RlZC1sb2FkaW5nYCwgd3JhcHBlckNsYXNzTmFtZSl9PlxuICAgICAgICAgIHtzcGlubmluZyAmJiA8ZGl2IGtleT1cImxvYWRpbmdcIj57c3BpbkVsZW1lbnR9PC9kaXY+fVxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjb250YWluZXJDbGFzc05hbWV9IGtleT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2Pik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gc3BpbkVsZW1lbnQ7XG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHsgc3Bpbm5pbmcsIGRlbGF5IH0gPSBwcm9wcztcbiAgICAgICAgY29uc3Qgc2hvdWxkQmVEZWxheWVkID0gc2hvdWxkRGVsYXkoc3Bpbm5pbmcsIGRlbGF5KTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHNwaW5uaW5nOiBzcGlubmluZyAmJiAhc2hvdWxkQmVEZWxheWVkLFxuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9yaWdpbmFsVXBkYXRlU3Bpbm5pbmcgPSB0aGlzLnVwZGF0ZVNwaW5uaW5nO1xuICAgICAgICB0aGlzLmRlYm91bmNpZnlVcGRhdGVTcGlubmluZyhwcm9wcyk7XG4gICAgfVxuICAgIHN0YXRpYyBzZXREZWZhdWx0SW5kaWNhdG9yKGluZGljYXRvcikge1xuICAgICAgICBkZWZhdWx0SW5kaWNhdG9yID0gaW5kaWNhdG9yO1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgdGhpcy51cGRhdGVTcGlubmluZygpO1xuICAgIH1cbiAgICBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICAgIHRoaXMuZGVib3VuY2lmeVVwZGF0ZVNwaW5uaW5nKCk7XG4gICAgICAgIHRoaXMudXBkYXRlU3Bpbm5pbmcoKTtcbiAgICB9XG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIHRoaXMuY2FuY2VsRXhpc3RpbmdTcGluKCk7XG4gICAgfVxuICAgIGNhbmNlbEV4aXN0aW5nU3BpbigpIHtcbiAgICAgICAgY29uc3QgeyB1cGRhdGVTcGlubmluZyB9ID0gdGhpcztcbiAgICAgICAgaWYgKHVwZGF0ZVNwaW5uaW5nICYmIHVwZGF0ZVNwaW5uaW5nLmNhbmNlbCkge1xuICAgICAgICAgICAgdXBkYXRlU3Bpbm5pbmcuY2FuY2VsKCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaXNOZXN0ZWRQYXR0ZXJuKCkge1xuICAgICAgICByZXR1cm4gISEodGhpcy5wcm9wcyAmJiB0aGlzLnByb3BzLmNoaWxkcmVuKTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlclNwaW59PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuU3Bpbi5kZWZhdWx0UHJvcHMgPSB7XG4gICAgc3Bpbm5pbmc6IHRydWUsXG4gICAgc2l6ZTogJ2RlZmF1bHQnLFxuICAgIHdyYXBwZXJDbGFzc05hbWU6ICcnLFxufTtcblNwaW4ucHJvcFR5cGVzID0ge1xuICAgIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3Bpbm5pbmc6IFByb3BUeXBlcy5ib29sLFxuICAgIHNpemU6IFByb3BUeXBlcy5vbmVPZihTcGluU2l6ZXMpLFxuICAgIHdyYXBwZXJDbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaW5kaWNhdG9yOiBQcm9wVHlwZXMuZWxlbWVudCxcbn07XG5leHBvcnQgZGVmYXVsdCBTcGluO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBTEE7QUFDQTtBQU1BO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUdBO0FBQ0E7QUFDQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQTNCQTtBQUNBO0FBakJBO0FBQUE7QUE4Q0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQXBEQTtBQW9EQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBeEJBO0FBQ0E7QUFDQTs7OztBQXhEQTtBQUNBO0FBK0VBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/spin/index.js
