__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createToolbar", function() { return createToolbar; });
/* harmony import */ var _Toolbar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Toolbar */ "./node_modules/rc-editor-core/es/Toolbar/Toolbar.js");


function noop(_) {}

;
function createToolbar() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  function editorStateChange(editorState) {// console.log('>> editorStateChange', editorState);
  }

  var callbacks = {
    onChange: editorStateChange,
    onUpArrow: noop,
    onDownArrow: noop,
    getEditorState: noop,
    setEditorState: noop,
    handleReturn: noop
  };
  return {
    name: 'toolbar',
    decorators: [],
    callbacks: callbacks,
    onChange: function onChange(editorState) {
      return callbacks.onChange ? callbacks.onChange(editorState) : editorState;
    },
    component: _Toolbar__WEBPACK_IMPORTED_MODULE_0__["default"]
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvVG9vbGJhci9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1jb3JlL2VzL1Rvb2xiYXIvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFRvb2xiYXIgZnJvbSAnLi9Ub29sYmFyJztcbmZ1bmN0aW9uIG5vb3AoXykge31cbjtcbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVUb29sYmFyKCkge1xuICAgIHZhciBjb25maWcgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xuXG4gICAgZnVuY3Rpb24gZWRpdG9yU3RhdGVDaGFuZ2UoZWRpdG9yU3RhdGUpIHtcbiAgICAgICAgLy8gY29uc29sZS5sb2coJz4+IGVkaXRvclN0YXRlQ2hhbmdlJywgZWRpdG9yU3RhdGUpO1xuICAgIH1cbiAgICB2YXIgY2FsbGJhY2tzID0ge1xuICAgICAgICBvbkNoYW5nZTogZWRpdG9yU3RhdGVDaGFuZ2UsXG4gICAgICAgIG9uVXBBcnJvdzogbm9vcCxcbiAgICAgICAgb25Eb3duQXJyb3c6IG5vb3AsXG4gICAgICAgIGdldEVkaXRvclN0YXRlOiBub29wLFxuICAgICAgICBzZXRFZGl0b3JTdGF0ZTogbm9vcCxcbiAgICAgICAgaGFuZGxlUmV0dXJuOiBub29wXG4gICAgfTtcbiAgICByZXR1cm4ge1xuICAgICAgICBuYW1lOiAndG9vbGJhcicsXG4gICAgICAgIGRlY29yYXRvcnM6IFtdLFxuICAgICAgICBjYWxsYmFja3M6IGNhbGxiYWNrcyxcbiAgICAgICAgb25DaGFuZ2U6IGZ1bmN0aW9uIG9uQ2hhbmdlKGVkaXRvclN0YXRlKSB7XG4gICAgICAgICAgICByZXR1cm4gY2FsbGJhY2tzLm9uQ2hhbmdlID8gY2FsbGJhY2tzLm9uQ2hhbmdlKGVkaXRvclN0YXRlKSA6IGVkaXRvclN0YXRlO1xuICAgICAgICB9LFxuXG4gICAgICAgIGNvbXBvbmVudDogVG9vbGJhclxuICAgIH07XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFSQTtBQVVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/Toolbar/index.js
