__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Phone", function() { return Phone; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");



function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n\t\t\t@media ", " {\n\t\t\t\t", "\n\t\t\t}\n\t\t"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/**
 * 手机媒体查询
 */

var MobilePhone = function MobilePhone() {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, MobilePhone);

  this.only = function (first) {
    for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      rest[_key - 1] = arguments[_key];
    }

    return styled_components__WEBPACK_IMPORTED_MODULE_2__["css"](_templateObject(), MobilePhone.ONLY, styled_components__WEBPACK_IMPORTED_MODULE_2__["css"].apply(styled_components__WEBPACK_IMPORTED_MODULE_2__, [first].concat(rest)));
  };
};

MobilePhone.ONLY = "(max-width: 599px)";
var Phone = new MobilePhone();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svdXNlci1zZXR0aW5ncy9teS13ZWJzaXRlL2hvbWUvY29tcG9uZW50cy9waG9uZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay91c2VyLXNldHRpbmdzL215LXdlYnNpdGUvaG9tZS9jb21wb25lbnRzL3Bob25lLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBTdHlsZWRGIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG4vKipcbiAqIOaJi+acuuWqkuS9k+afpeivolxuICovXG5jbGFzcyBNb2JpbGVQaG9uZSB7XG4gIC8qKiA2MDDku6XkuIvorqTkuLrmiYvmnLosIOaXoOiuuuWuvemrmOavlCAqL1xuICAvKiog5a69PumrmCwgOTAw5Lul5LiL6K6k5Li65omL5py6ICovXG4gIHByaXZhdGUgc3RhdGljIE9OTFkgPSBcIihtYXgtd2lkdGg6IDU5OXB4KVwiO1xuICBvbmx5ID0gKGZpcnN0OiBhbnksIC4uLnJlc3Q6IGFueVtdKSA9PiB7XG4gICAgcmV0dXJuIFN0eWxlZEYuY3NzYFxuXHRcdFx0QG1lZGlhICR7TW9iaWxlUGhvbmUuT05MWX0ge1xuXHRcdFx0XHQke1N0eWxlZEYuY3NzKGZpcnN0LCAuLi5yZXN0KX1cblx0XHRcdH1cblx0XHRgO1xuICB9O1xufVxuXG5leHBvcnQgY29uc3QgUGhvbmUgPSBuZXcgTW9iaWxlUGhvbmUoKTsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBRUE7Ozs7QUFHQTs7O0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBS0E7OztBQVZBO0FBYUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/user-settings/my-website/home/components/phone.tsx
