/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule convertFromDraftStateToRaw
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var DraftStringKey = __webpack_require__(/*! ./DraftStringKey */ "./node_modules/draft-js/lib/DraftStringKey.js");

var encodeEntityRanges = __webpack_require__(/*! ./encodeEntityRanges */ "./node_modules/draft-js/lib/encodeEntityRanges.js");

var encodeInlineStyleRanges = __webpack_require__(/*! ./encodeInlineStyleRanges */ "./node_modules/draft-js/lib/encodeInlineStyleRanges.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var createRawBlock = function createRawBlock(block, entityStorageMap) {
  return {
    key: block.getKey(),
    text: block.getText(),
    type: block.getType(),
    depth: block.getDepth(),
    inlineStyleRanges: encodeInlineStyleRanges(block),
    entityRanges: encodeEntityRanges(block, entityStorageMap),
    data: block.getData().toObject()
  };
};

var insertRawBlock = function insertRawBlock(block, entityMap, rawBlocks, blockCacheRef) {
  if (block instanceof ContentBlock) {
    rawBlocks.push(createRawBlock(block, entityMap));
    return;
  }

  !(block instanceof ContentBlockNode) ?  true ? invariant(false, 'block is not a BlockNode') : undefined : void 0;
  var parentKey = block.getParentKey();

  var rawBlock = blockCacheRef[block.getKey()] = _extends({}, createRawBlock(block, entityMap), {
    children: []
  });

  if (parentKey) {
    blockCacheRef[parentKey].children.push(rawBlock);
    return;
  }

  rawBlocks.push(rawBlock);
};

var encodeRawBlocks = function encodeRawBlocks(contentState, rawState) {
  var entityMap = rawState.entityMap;
  var rawBlocks = [];
  var blockCacheRef = {};
  var entityCacheRef = {};
  var entityStorageKey = 0;
  contentState.getBlockMap().forEach(function (block) {
    block.findEntityRanges(function (character) {
      return character.getEntity() !== null;
    }, function (start) {
      var entityKey = block.getEntityAt(start); // Stringify to maintain order of otherwise numeric keys.

      var stringifiedEntityKey = DraftStringKey.stringify(entityKey); // This makes this function resilient to two entities
      // erroneously having the same key

      if (entityCacheRef[stringifiedEntityKey]) {
        return;
      }

      entityCacheRef[stringifiedEntityKey] = entityKey; // we need the `any` casting here since this is a temporary state
      // where we will later on flip the entity map and populate it with
      // real entity, at this stage we just need to map back the entity
      // key used by the BlockNode

      entityMap[stringifiedEntityKey] = '' + entityStorageKey;
      entityStorageKey++;
    });
    insertRawBlock(block, entityMap, rawBlocks, blockCacheRef);
  });
  return {
    blocks: rawBlocks,
    entityMap: entityMap
  };
}; // Flip storage map so that our storage keys map to global
// DraftEntity keys.


var encodeRawEntityMap = function encodeRawEntityMap(contentState, rawState) {
  var blocks = rawState.blocks,
      entityMap = rawState.entityMap;
  var rawEntityMap = {};
  Object.keys(entityMap).forEach(function (key, index) {
    var entity = contentState.getEntity(DraftStringKey.unstringify(key));
    rawEntityMap[index] = {
      type: entity.getType(),
      mutability: entity.getMutability(),
      data: entity.getData()
    };
  });
  return {
    blocks: blocks,
    entityMap: rawEntityMap
  };
};

var convertFromDraftStateToRaw = function convertFromDraftStateToRaw(contentState) {
  var rawDraftContentState = {
    entityMap: {},
    blocks: []
  }; // add blocks

  rawDraftContentState = encodeRawBlocks(contentState, rawDraftContentState); // add entities

  rawDraftContentState = encodeRawEntityMap(contentState, rawDraftContentState);
  return rawDraftContentState;
};

module.exports = convertFromDraftStateToRaw;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NvbnZlcnRGcm9tRHJhZnRTdGF0ZVRvUmF3LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2NvbnZlcnRGcm9tRHJhZnRTdGF0ZVRvUmF3LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgY29udmVydEZyb21EcmFmdFN0YXRlVG9SYXdcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBDb250ZW50QmxvY2sgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9jaycpO1xudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBEcmFmdFN0cmluZ0tleSA9IHJlcXVpcmUoJy4vRHJhZnRTdHJpbmdLZXknKTtcblxudmFyIGVuY29kZUVudGl0eVJhbmdlcyA9IHJlcXVpcmUoJy4vZW5jb2RlRW50aXR5UmFuZ2VzJyk7XG52YXIgZW5jb2RlSW5saW5lU3R5bGVSYW5nZXMgPSByZXF1aXJlKCcuL2VuY29kZUlubGluZVN0eWxlUmFuZ2VzJyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbnZhciBjcmVhdGVSYXdCbG9jayA9IGZ1bmN0aW9uIGNyZWF0ZVJhd0Jsb2NrKGJsb2NrLCBlbnRpdHlTdG9yYWdlTWFwKSB7XG4gIHJldHVybiB7XG4gICAga2V5OiBibG9jay5nZXRLZXkoKSxcbiAgICB0ZXh0OiBibG9jay5nZXRUZXh0KCksXG4gICAgdHlwZTogYmxvY2suZ2V0VHlwZSgpLFxuICAgIGRlcHRoOiBibG9jay5nZXREZXB0aCgpLFxuICAgIGlubGluZVN0eWxlUmFuZ2VzOiBlbmNvZGVJbmxpbmVTdHlsZVJhbmdlcyhibG9jayksXG4gICAgZW50aXR5UmFuZ2VzOiBlbmNvZGVFbnRpdHlSYW5nZXMoYmxvY2ssIGVudGl0eVN0b3JhZ2VNYXApLFxuICAgIGRhdGE6IGJsb2NrLmdldERhdGEoKS50b09iamVjdCgpXG4gIH07XG59O1xuXG52YXIgaW5zZXJ0UmF3QmxvY2sgPSBmdW5jdGlvbiBpbnNlcnRSYXdCbG9jayhibG9jaywgZW50aXR5TWFwLCByYXdCbG9ja3MsIGJsb2NrQ2FjaGVSZWYpIHtcbiAgaWYgKGJsb2NrIGluc3RhbmNlb2YgQ29udGVudEJsb2NrKSB7XG4gICAgcmF3QmxvY2tzLnB1c2goY3JlYXRlUmF3QmxvY2soYmxvY2ssIGVudGl0eU1hcCkpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gICEoYmxvY2sgaW5zdGFuY2VvZiBDb250ZW50QmxvY2tOb2RlKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdibG9jayBpcyBub3QgYSBCbG9ja05vZGUnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgdmFyIHBhcmVudEtleSA9IGJsb2NrLmdldFBhcmVudEtleSgpO1xuICB2YXIgcmF3QmxvY2sgPSBibG9ja0NhY2hlUmVmW2Jsb2NrLmdldEtleSgpXSA9IF9leHRlbmRzKHt9LCBjcmVhdGVSYXdCbG9jayhibG9jaywgZW50aXR5TWFwKSwge1xuICAgIGNoaWxkcmVuOiBbXVxuICB9KTtcblxuICBpZiAocGFyZW50S2V5KSB7XG4gICAgYmxvY2tDYWNoZVJlZltwYXJlbnRLZXldLmNoaWxkcmVuLnB1c2gocmF3QmxvY2spO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHJhd0Jsb2Nrcy5wdXNoKHJhd0Jsb2NrKTtcbn07XG5cbnZhciBlbmNvZGVSYXdCbG9ja3MgPSBmdW5jdGlvbiBlbmNvZGVSYXdCbG9ja3MoY29udGVudFN0YXRlLCByYXdTdGF0ZSkge1xuICB2YXIgZW50aXR5TWFwID0gcmF3U3RhdGUuZW50aXR5TWFwO1xuXG5cbiAgdmFyIHJhd0Jsb2NrcyA9IFtdO1xuXG4gIHZhciBibG9ja0NhY2hlUmVmID0ge307XG4gIHZhciBlbnRpdHlDYWNoZVJlZiA9IHt9O1xuICB2YXIgZW50aXR5U3RvcmFnZUtleSA9IDA7XG5cbiAgY29udGVudFN0YXRlLmdldEJsb2NrTWFwKCkuZm9yRWFjaChmdW5jdGlvbiAoYmxvY2spIHtcbiAgICBibG9jay5maW5kRW50aXR5UmFuZ2VzKGZ1bmN0aW9uIChjaGFyYWN0ZXIpIHtcbiAgICAgIHJldHVybiBjaGFyYWN0ZXIuZ2V0RW50aXR5KCkgIT09IG51bGw7XG4gICAgfSwgZnVuY3Rpb24gKHN0YXJ0KSB7XG4gICAgICB2YXIgZW50aXR5S2V5ID0gYmxvY2suZ2V0RW50aXR5QXQoc3RhcnQpO1xuICAgICAgLy8gU3RyaW5naWZ5IHRvIG1haW50YWluIG9yZGVyIG9mIG90aGVyd2lzZSBudW1lcmljIGtleXMuXG4gICAgICB2YXIgc3RyaW5naWZpZWRFbnRpdHlLZXkgPSBEcmFmdFN0cmluZ0tleS5zdHJpbmdpZnkoZW50aXR5S2V5KTtcbiAgICAgIC8vIFRoaXMgbWFrZXMgdGhpcyBmdW5jdGlvbiByZXNpbGllbnQgdG8gdHdvIGVudGl0aWVzXG4gICAgICAvLyBlcnJvbmVvdXNseSBoYXZpbmcgdGhlIHNhbWUga2V5XG4gICAgICBpZiAoZW50aXR5Q2FjaGVSZWZbc3RyaW5naWZpZWRFbnRpdHlLZXldKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGVudGl0eUNhY2hlUmVmW3N0cmluZ2lmaWVkRW50aXR5S2V5XSA9IGVudGl0eUtleTtcbiAgICAgIC8vIHdlIG5lZWQgdGhlIGBhbnlgIGNhc3RpbmcgaGVyZSBzaW5jZSB0aGlzIGlzIGEgdGVtcG9yYXJ5IHN0YXRlXG4gICAgICAvLyB3aGVyZSB3ZSB3aWxsIGxhdGVyIG9uIGZsaXAgdGhlIGVudGl0eSBtYXAgYW5kIHBvcHVsYXRlIGl0IHdpdGhcbiAgICAgIC8vIHJlYWwgZW50aXR5LCBhdCB0aGlzIHN0YWdlIHdlIGp1c3QgbmVlZCB0byBtYXAgYmFjayB0aGUgZW50aXR5XG4gICAgICAvLyBrZXkgdXNlZCBieSB0aGUgQmxvY2tOb2RlXG4gICAgICBlbnRpdHlNYXBbc3RyaW5naWZpZWRFbnRpdHlLZXldID0gJycgKyBlbnRpdHlTdG9yYWdlS2V5O1xuICAgICAgZW50aXR5U3RvcmFnZUtleSsrO1xuICAgIH0pO1xuXG4gICAgaW5zZXJ0UmF3QmxvY2soYmxvY2ssIGVudGl0eU1hcCwgcmF3QmxvY2tzLCBibG9ja0NhY2hlUmVmKTtcbiAgfSk7XG5cbiAgcmV0dXJuIHtcbiAgICBibG9ja3M6IHJhd0Jsb2NrcyxcbiAgICBlbnRpdHlNYXA6IGVudGl0eU1hcFxuICB9O1xufTtcblxuLy8gRmxpcCBzdG9yYWdlIG1hcCBzbyB0aGF0IG91ciBzdG9yYWdlIGtleXMgbWFwIHRvIGdsb2JhbFxuLy8gRHJhZnRFbnRpdHkga2V5cy5cbnZhciBlbmNvZGVSYXdFbnRpdHlNYXAgPSBmdW5jdGlvbiBlbmNvZGVSYXdFbnRpdHlNYXAoY29udGVudFN0YXRlLCByYXdTdGF0ZSkge1xuICB2YXIgYmxvY2tzID0gcmF3U3RhdGUuYmxvY2tzLFxuICAgICAgZW50aXR5TWFwID0gcmF3U3RhdGUuZW50aXR5TWFwO1xuXG5cbiAgdmFyIHJhd0VudGl0eU1hcCA9IHt9O1xuXG4gIE9iamVjdC5rZXlzKGVudGl0eU1hcCkuZm9yRWFjaChmdW5jdGlvbiAoa2V5LCBpbmRleCkge1xuICAgIHZhciBlbnRpdHkgPSBjb250ZW50U3RhdGUuZ2V0RW50aXR5KERyYWZ0U3RyaW5nS2V5LnVuc3RyaW5naWZ5KGtleSkpO1xuICAgIHJhd0VudGl0eU1hcFtpbmRleF0gPSB7XG4gICAgICB0eXBlOiBlbnRpdHkuZ2V0VHlwZSgpLFxuICAgICAgbXV0YWJpbGl0eTogZW50aXR5LmdldE11dGFiaWxpdHkoKSxcbiAgICAgIGRhdGE6IGVudGl0eS5nZXREYXRhKClcbiAgICB9O1xuICB9KTtcblxuICByZXR1cm4ge1xuICAgIGJsb2NrczogYmxvY2tzLFxuICAgIGVudGl0eU1hcDogcmF3RW50aXR5TWFwXG4gIH07XG59O1xuXG52YXIgY29udmVydEZyb21EcmFmdFN0YXRlVG9SYXcgPSBmdW5jdGlvbiBjb252ZXJ0RnJvbURyYWZ0U3RhdGVUb1Jhdyhjb250ZW50U3RhdGUpIHtcbiAgdmFyIHJhd0RyYWZ0Q29udGVudFN0YXRlID0ge1xuICAgIGVudGl0eU1hcDoge30sXG4gICAgYmxvY2tzOiBbXVxuICB9O1xuXG4gIC8vIGFkZCBibG9ja3NcbiAgcmF3RHJhZnRDb250ZW50U3RhdGUgPSBlbmNvZGVSYXdCbG9ja3MoY29udGVudFN0YXRlLCByYXdEcmFmdENvbnRlbnRTdGF0ZSk7XG5cbiAgLy8gYWRkIGVudGl0aWVzXG4gIHJhd0RyYWZ0Q29udGVudFN0YXRlID0gZW5jb2RlUmF3RW50aXR5TWFwKGNvbnRlbnRTdGF0ZSwgcmF3RHJhZnRDb250ZW50U3RhdGUpO1xuXG4gIHJldHVybiByYXdEcmFmdENvbnRlbnRTdGF0ZTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gY29udmVydEZyb21EcmFmdFN0YXRlVG9SYXc7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFJQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFLQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/convertFromDraftStateToRaw.js
