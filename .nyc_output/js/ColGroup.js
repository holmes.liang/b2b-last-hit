

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/rc-table/es/utils.js");

var ColGroup = function ColGroup(props, _ref) {
  var table = _ref.table;
  var _table$props = table.props,
      prefixCls = _table$props.prefixCls,
      expandIconAsCell = _table$props.expandIconAsCell;
  var fixed = props.fixed;
  var cols = [];

  if (expandIconAsCell && fixed !== 'right') {
    cols.push(React.createElement("col", {
      className: "".concat(prefixCls, "-expand-icon-col"),
      key: "rc-table-expand-icon-col"
    }));
  }

  var leafColumns;

  if (fixed === 'left') {
    leafColumns = table.columnManager.leftLeafColumns();
  } else if (fixed === 'right') {
    leafColumns = table.columnManager.rightLeafColumns();
  } else {
    leafColumns = table.columnManager.leafColumns();
  }

  cols = cols.concat(leafColumns.map(function (_ref2) {
    var key = _ref2.key,
        dataIndex = _ref2.dataIndex,
        width = _ref2.width,
        additionalProps = _ref2[utils_1.INTERNAL_COL_DEFINE];
    var mergedKey = key !== undefined ? key : dataIndex;
    return React.createElement("col", Object.assign({
      key: mergedKey,
      style: {
        width: width,
        minWidth: width
      }
    }, additionalProps));
  }));
  return React.createElement("colgroup", null, cols);
};

ColGroup.contextTypes = {
  table: PropTypes.any
};
exports.default = ColGroup;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvQ29sR3JvdXAuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10YWJsZS9lcy9Db2xHcm91cC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxudmFyIF9faW1wb3J0U3RhciA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydFN0YXIgfHwgZnVuY3Rpb24gKG1vZCkge1xuICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xuICB2YXIgcmVzdWx0ID0ge307XG4gIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIHtcbiAgICBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xuICB9XG4gIHJlc3VsdFtcImRlZmF1bHRcIl0gPSBtb2Q7XG4gIHJldHVybiByZXN1bHQ7XG59O1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgUmVhY3QgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInJlYWN0XCIpKTtcblxudmFyIFByb3BUeXBlcyA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicHJvcC10eXBlc1wiKSk7XG5cbnZhciB1dGlsc18xID0gcmVxdWlyZShcIi4vdXRpbHNcIik7XG5cbnZhciBDb2xHcm91cCA9IGZ1bmN0aW9uIENvbEdyb3VwKHByb3BzLCBfcmVmKSB7XG4gIHZhciB0YWJsZSA9IF9yZWYudGFibGU7XG4gIHZhciBfdGFibGUkcHJvcHMgPSB0YWJsZS5wcm9wcyxcbiAgICAgIHByZWZpeENscyA9IF90YWJsZSRwcm9wcy5wcmVmaXhDbHMsXG4gICAgICBleHBhbmRJY29uQXNDZWxsID0gX3RhYmxlJHByb3BzLmV4cGFuZEljb25Bc0NlbGw7XG4gIHZhciBmaXhlZCA9IHByb3BzLmZpeGVkO1xuICB2YXIgY29scyA9IFtdO1xuXG4gIGlmIChleHBhbmRJY29uQXNDZWxsICYmIGZpeGVkICE9PSAncmlnaHQnKSB7XG4gICAgY29scy5wdXNoKFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjb2xcIiwge1xuICAgICAgY2xhc3NOYW1lOiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWV4cGFuZC1pY29uLWNvbFwiKSxcbiAgICAgIGtleTogXCJyYy10YWJsZS1leHBhbmQtaWNvbi1jb2xcIlxuICAgIH0pKTtcbiAgfVxuXG4gIHZhciBsZWFmQ29sdW1ucztcblxuICBpZiAoZml4ZWQgPT09ICdsZWZ0Jykge1xuICAgIGxlYWZDb2x1bW5zID0gdGFibGUuY29sdW1uTWFuYWdlci5sZWZ0TGVhZkNvbHVtbnMoKTtcbiAgfSBlbHNlIGlmIChmaXhlZCA9PT0gJ3JpZ2h0Jykge1xuICAgIGxlYWZDb2x1bW5zID0gdGFibGUuY29sdW1uTWFuYWdlci5yaWdodExlYWZDb2x1bW5zKCk7XG4gIH0gZWxzZSB7XG4gICAgbGVhZkNvbHVtbnMgPSB0YWJsZS5jb2x1bW5NYW5hZ2VyLmxlYWZDb2x1bW5zKCk7XG4gIH1cblxuICBjb2xzID0gY29scy5jb25jYXQobGVhZkNvbHVtbnMubWFwKGZ1bmN0aW9uIChfcmVmMikge1xuICAgIHZhciBrZXkgPSBfcmVmMi5rZXksXG4gICAgICAgIGRhdGFJbmRleCA9IF9yZWYyLmRhdGFJbmRleCxcbiAgICAgICAgd2lkdGggPSBfcmVmMi53aWR0aCxcbiAgICAgICAgYWRkaXRpb25hbFByb3BzID0gX3JlZjJbdXRpbHNfMS5JTlRFUk5BTF9DT0xfREVGSU5FXTtcbiAgICB2YXIgbWVyZ2VkS2V5ID0ga2V5ICE9PSB1bmRlZmluZWQgPyBrZXkgOiBkYXRhSW5kZXg7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjb2xcIiwgT2JqZWN0LmFzc2lnbih7XG4gICAgICBrZXk6IG1lcmdlZEtleSxcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIHdpZHRoOiB3aWR0aCxcbiAgICAgICAgbWluV2lkdGg6IHdpZHRoXG4gICAgICB9XG4gICAgfSwgYWRkaXRpb25hbFByb3BzKSk7XG4gIH0pKTtcbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJjb2xncm91cFwiLCBudWxsLCBjb2xzKTtcbn07XG5cbkNvbEdyb3VwLmNvbnRleHRUeXBlcyA9IHtcbiAgdGFibGU6IFByb3BUeXBlcy5hbnlcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBDb2xHcm91cDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/ColGroup.js
