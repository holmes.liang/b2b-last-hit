/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var AxisBuilder = __webpack_require__(/*! ./AxisBuilder */ "./node_modules/echarts/lib/component/axis/AxisBuilder.js");

var AxisView = __webpack_require__(/*! ./AxisView */ "./node_modules/echarts/lib/component/axis/AxisView.js");

var cartesianAxisHelper = __webpack_require__(/*! ../../coord/cartesian/cartesianAxisHelper */ "./node_modules/echarts/lib/coord/cartesian/cartesianAxisHelper.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var axisBuilderAttrs = ['axisLine', 'axisTickLabel', 'axisName'];
var selfBuilderAttrs = ['splitArea', 'splitLine']; // function getAlignWithLabel(model, axisModel) {
//     var alignWithLabel = model.get('alignWithLabel');
//     if (alignWithLabel === 'auto') {
//         alignWithLabel = axisModel.get('axisTick.alignWithLabel');
//     }
//     return alignWithLabel;
// }

var CartesianAxisView = AxisView.extend({
  type: 'cartesianAxis',
  axisPointerClass: 'CartesianAxisPointer',

  /**
   * @override
   */
  render: function render(axisModel, ecModel, api, payload) {
    this.group.removeAll();
    var oldAxisGroup = this._axisGroup;
    this._axisGroup = new graphic.Group();
    this.group.add(this._axisGroup);

    if (!axisModel.get('show')) {
      return;
    }

    var gridModel = axisModel.getCoordSysModel();
    var layout = cartesianAxisHelper.layout(gridModel, axisModel);
    var axisBuilder = new AxisBuilder(axisModel, layout);
    zrUtil.each(axisBuilderAttrs, axisBuilder.add, axisBuilder);

    this._axisGroup.add(axisBuilder.getGroup());

    zrUtil.each(selfBuilderAttrs, function (name) {
      if (axisModel.get(name + '.show')) {
        this['_' + name](axisModel, gridModel);
      }
    }, this);
    graphic.groupTransition(oldAxisGroup, this._axisGroup, axisModel);
    CartesianAxisView.superCall(this, 'render', axisModel, ecModel, api, payload);
  },
  remove: function remove() {
    this._splitAreaColors = null;
  },

  /**
   * @param {module:echarts/coord/cartesian/AxisModel} axisModel
   * @param {module:echarts/coord/cartesian/GridModel} gridModel
   * @private
   */
  _splitLine: function _splitLine(axisModel, gridModel) {
    var axis = axisModel.axis;

    if (axis.scale.isBlank()) {
      return;
    }

    var splitLineModel = axisModel.getModel('splitLine');
    var lineStyleModel = splitLineModel.getModel('lineStyle');
    var lineColors = lineStyleModel.get('color');
    lineColors = zrUtil.isArray(lineColors) ? lineColors : [lineColors];
    var gridRect = gridModel.coordinateSystem.getRect();
    var isHorizontal = axis.isHorizontal();
    var lineCount = 0;
    var ticksCoords = axis.getTicksCoords({
      tickModel: splitLineModel
    });
    var p1 = [];
    var p2 = []; // Simple optimization
    // Batching the lines if color are the same

    var lineStyle = lineStyleModel.getLineStyle();

    for (var i = 0; i < ticksCoords.length; i++) {
      var tickCoord = axis.toGlobalCoord(ticksCoords[i].coord);

      if (isHorizontal) {
        p1[0] = tickCoord;
        p1[1] = gridRect.y;
        p2[0] = tickCoord;
        p2[1] = gridRect.y + gridRect.height;
      } else {
        p1[0] = gridRect.x;
        p1[1] = tickCoord;
        p2[0] = gridRect.x + gridRect.width;
        p2[1] = tickCoord;
      }

      var colorIndex = lineCount++ % lineColors.length;
      var tickValue = ticksCoords[i].tickValue;

      this._axisGroup.add(new graphic.Line(graphic.subPixelOptimizeLine({
        anid: tickValue != null ? 'line_' + ticksCoords[i].tickValue : null,
        shape: {
          x1: p1[0],
          y1: p1[1],
          x2: p2[0],
          y2: p2[1]
        },
        style: zrUtil.defaults({
          stroke: lineColors[colorIndex]
        }, lineStyle),
        silent: true
      })));
    }
  },

  /**
   * @param {module:echarts/coord/cartesian/AxisModel} axisModel
   * @param {module:echarts/coord/cartesian/GridModel} gridModel
   * @private
   */
  _splitArea: function _splitArea(axisModel, gridModel) {
    var axis = axisModel.axis;

    if (axis.scale.isBlank()) {
      return;
    }

    var splitAreaModel = axisModel.getModel('splitArea');
    var areaStyleModel = splitAreaModel.getModel('areaStyle');
    var areaColors = areaStyleModel.get('color');
    var gridRect = gridModel.coordinateSystem.getRect();
    var ticksCoords = axis.getTicksCoords({
      tickModel: splitAreaModel,
      clamp: true
    });

    if (!ticksCoords.length) {
      return;
    } // For Making appropriate splitArea animation, the color and anid
    // should be corresponding to previous one if possible.


    var areaColorsLen = areaColors.length;
    var lastSplitAreaColors = this._splitAreaColors;
    var newSplitAreaColors = zrUtil.createHashMap();
    var colorIndex = 0;

    if (lastSplitAreaColors) {
      for (var i = 0; i < ticksCoords.length; i++) {
        var cIndex = lastSplitAreaColors.get(ticksCoords[i].tickValue);

        if (cIndex != null) {
          colorIndex = (cIndex + (areaColorsLen - 1) * i) % areaColorsLen;
          break;
        }
      }
    }

    var prev = axis.toGlobalCoord(ticksCoords[0].coord);
    var areaStyle = areaStyleModel.getAreaStyle();
    areaColors = zrUtil.isArray(areaColors) ? areaColors : [areaColors];

    for (var i = 1; i < ticksCoords.length; i++) {
      var tickCoord = axis.toGlobalCoord(ticksCoords[i].coord);
      var x;
      var y;
      var width;
      var height;

      if (axis.isHorizontal()) {
        x = prev;
        y = gridRect.y;
        width = tickCoord - x;
        height = gridRect.height;
        prev = x + width;
      } else {
        x = gridRect.x;
        y = prev;
        width = gridRect.width;
        height = tickCoord - y;
        prev = y + height;
      }

      var tickValue = ticksCoords[i - 1].tickValue;
      tickValue != null && newSplitAreaColors.set(tickValue, colorIndex);

      this._axisGroup.add(new graphic.Rect({
        anid: tickValue != null ? 'area_' + tickValue : null,
        shape: {
          x: x,
          y: y,
          width: width,
          height: height
        },
        style: zrUtil.defaults({
          fill: areaColors[colorIndex]
        }, areaStyle),
        silent: true
      }));

      colorIndex = (colorIndex + 1) % areaColorsLen;
    }

    this._splitAreaColors = newSplitAreaColors;
  }
});
CartesianAxisView.extend({
  type: 'xAxis'
});
CartesianAxisView.extend({
  type: 'yAxis'
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXMvQ2FydGVzaWFuQXhpc1ZpZXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvYXhpcy9DYXJ0ZXNpYW5BeGlzVmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIEF4aXNCdWlsZGVyID0gcmVxdWlyZShcIi4vQXhpc0J1aWxkZXJcIik7XG5cbnZhciBBeGlzVmlldyA9IHJlcXVpcmUoXCIuL0F4aXNWaWV3XCIpO1xuXG52YXIgY2FydGVzaWFuQXhpc0hlbHBlciA9IHJlcXVpcmUoXCIuLi8uLi9jb29yZC9jYXJ0ZXNpYW4vY2FydGVzaWFuQXhpc0hlbHBlclwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGF4aXNCdWlsZGVyQXR0cnMgPSBbJ2F4aXNMaW5lJywgJ2F4aXNUaWNrTGFiZWwnLCAnYXhpc05hbWUnXTtcbnZhciBzZWxmQnVpbGRlckF0dHJzID0gWydzcGxpdEFyZWEnLCAnc3BsaXRMaW5lJ107IC8vIGZ1bmN0aW9uIGdldEFsaWduV2l0aExhYmVsKG1vZGVsLCBheGlzTW9kZWwpIHtcbi8vICAgICB2YXIgYWxpZ25XaXRoTGFiZWwgPSBtb2RlbC5nZXQoJ2FsaWduV2l0aExhYmVsJyk7XG4vLyAgICAgaWYgKGFsaWduV2l0aExhYmVsID09PSAnYXV0bycpIHtcbi8vICAgICAgICAgYWxpZ25XaXRoTGFiZWwgPSBheGlzTW9kZWwuZ2V0KCdheGlzVGljay5hbGlnbldpdGhMYWJlbCcpO1xuLy8gICAgIH1cbi8vICAgICByZXR1cm4gYWxpZ25XaXRoTGFiZWw7XG4vLyB9XG5cbnZhciBDYXJ0ZXNpYW5BeGlzVmlldyA9IEF4aXNWaWV3LmV4dGVuZCh7XG4gIHR5cGU6ICdjYXJ0ZXNpYW5BeGlzJyxcbiAgYXhpc1BvaW50ZXJDbGFzczogJ0NhcnRlc2lhbkF4aXNQb2ludGVyJyxcblxuICAvKipcbiAgICogQG92ZXJyaWRlXG4gICAqL1xuICByZW5kZXI6IGZ1bmN0aW9uIChheGlzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkge1xuICAgIHRoaXMuZ3JvdXAucmVtb3ZlQWxsKCk7XG4gICAgdmFyIG9sZEF4aXNHcm91cCA9IHRoaXMuX2F4aXNHcm91cDtcbiAgICB0aGlzLl9heGlzR3JvdXAgPSBuZXcgZ3JhcGhpYy5Hcm91cCgpO1xuICAgIHRoaXMuZ3JvdXAuYWRkKHRoaXMuX2F4aXNHcm91cCk7XG5cbiAgICBpZiAoIWF4aXNNb2RlbC5nZXQoJ3Nob3cnKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBncmlkTW9kZWwgPSBheGlzTW9kZWwuZ2V0Q29vcmRTeXNNb2RlbCgpO1xuICAgIHZhciBsYXlvdXQgPSBjYXJ0ZXNpYW5BeGlzSGVscGVyLmxheW91dChncmlkTW9kZWwsIGF4aXNNb2RlbCk7XG4gICAgdmFyIGF4aXNCdWlsZGVyID0gbmV3IEF4aXNCdWlsZGVyKGF4aXNNb2RlbCwgbGF5b3V0KTtcbiAgICB6clV0aWwuZWFjaChheGlzQnVpbGRlckF0dHJzLCBheGlzQnVpbGRlci5hZGQsIGF4aXNCdWlsZGVyKTtcblxuICAgIHRoaXMuX2F4aXNHcm91cC5hZGQoYXhpc0J1aWxkZXIuZ2V0R3JvdXAoKSk7XG5cbiAgICB6clV0aWwuZWFjaChzZWxmQnVpbGRlckF0dHJzLCBmdW5jdGlvbiAobmFtZSkge1xuICAgICAgaWYgKGF4aXNNb2RlbC5nZXQobmFtZSArICcuc2hvdycpKSB7XG4gICAgICAgIHRoaXNbJ18nICsgbmFtZV0oYXhpc01vZGVsLCBncmlkTW9kZWwpO1xuICAgICAgfVxuICAgIH0sIHRoaXMpO1xuICAgIGdyYXBoaWMuZ3JvdXBUcmFuc2l0aW9uKG9sZEF4aXNHcm91cCwgdGhpcy5fYXhpc0dyb3VwLCBheGlzTW9kZWwpO1xuICAgIENhcnRlc2lhbkF4aXNWaWV3LnN1cGVyQ2FsbCh0aGlzLCAncmVuZGVyJywgYXhpc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpO1xuICB9LFxuICByZW1vdmU6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLl9zcGxpdEFyZWFDb2xvcnMgPSBudWxsO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9BeGlzTW9kZWx9IGF4aXNNb2RlbFxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9HcmlkTW9kZWx9IGdyaWRNb2RlbFxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX3NwbGl0TGluZTogZnVuY3Rpb24gKGF4aXNNb2RlbCwgZ3JpZE1vZGVsKSB7XG4gICAgdmFyIGF4aXMgPSBheGlzTW9kZWwuYXhpcztcblxuICAgIGlmIChheGlzLnNjYWxlLmlzQmxhbmsoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBzcGxpdExpbmVNb2RlbCA9IGF4aXNNb2RlbC5nZXRNb2RlbCgnc3BsaXRMaW5lJyk7XG4gICAgdmFyIGxpbmVTdHlsZU1vZGVsID0gc3BsaXRMaW5lTW9kZWwuZ2V0TW9kZWwoJ2xpbmVTdHlsZScpO1xuICAgIHZhciBsaW5lQ29sb3JzID0gbGluZVN0eWxlTW9kZWwuZ2V0KCdjb2xvcicpO1xuICAgIGxpbmVDb2xvcnMgPSB6clV0aWwuaXNBcnJheShsaW5lQ29sb3JzKSA/IGxpbmVDb2xvcnMgOiBbbGluZUNvbG9yc107XG4gICAgdmFyIGdyaWRSZWN0ID0gZ3JpZE1vZGVsLmNvb3JkaW5hdGVTeXN0ZW0uZ2V0UmVjdCgpO1xuICAgIHZhciBpc0hvcml6b250YWwgPSBheGlzLmlzSG9yaXpvbnRhbCgpO1xuICAgIHZhciBsaW5lQ291bnQgPSAwO1xuICAgIHZhciB0aWNrc0Nvb3JkcyA9IGF4aXMuZ2V0VGlja3NDb29yZHMoe1xuICAgICAgdGlja01vZGVsOiBzcGxpdExpbmVNb2RlbFxuICAgIH0pO1xuICAgIHZhciBwMSA9IFtdO1xuICAgIHZhciBwMiA9IFtdOyAvLyBTaW1wbGUgb3B0aW1pemF0aW9uXG4gICAgLy8gQmF0Y2hpbmcgdGhlIGxpbmVzIGlmIGNvbG9yIGFyZSB0aGUgc2FtZVxuXG4gICAgdmFyIGxpbmVTdHlsZSA9IGxpbmVTdHlsZU1vZGVsLmdldExpbmVTdHlsZSgpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aWNrc0Nvb3Jkcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHRpY2tDb29yZCA9IGF4aXMudG9HbG9iYWxDb29yZCh0aWNrc0Nvb3Jkc1tpXS5jb29yZCk7XG5cbiAgICAgIGlmIChpc0hvcml6b250YWwpIHtcbiAgICAgICAgcDFbMF0gPSB0aWNrQ29vcmQ7XG4gICAgICAgIHAxWzFdID0gZ3JpZFJlY3QueTtcbiAgICAgICAgcDJbMF0gPSB0aWNrQ29vcmQ7XG4gICAgICAgIHAyWzFdID0gZ3JpZFJlY3QueSArIGdyaWRSZWN0LmhlaWdodDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHAxWzBdID0gZ3JpZFJlY3QueDtcbiAgICAgICAgcDFbMV0gPSB0aWNrQ29vcmQ7XG4gICAgICAgIHAyWzBdID0gZ3JpZFJlY3QueCArIGdyaWRSZWN0LndpZHRoO1xuICAgICAgICBwMlsxXSA9IHRpY2tDb29yZDtcbiAgICAgIH1cblxuICAgICAgdmFyIGNvbG9ySW5kZXggPSBsaW5lQ291bnQrKyAlIGxpbmVDb2xvcnMubGVuZ3RoO1xuICAgICAgdmFyIHRpY2tWYWx1ZSA9IHRpY2tzQ29vcmRzW2ldLnRpY2tWYWx1ZTtcblxuICAgICAgdGhpcy5fYXhpc0dyb3VwLmFkZChuZXcgZ3JhcGhpYy5MaW5lKGdyYXBoaWMuc3ViUGl4ZWxPcHRpbWl6ZUxpbmUoe1xuICAgICAgICBhbmlkOiB0aWNrVmFsdWUgIT0gbnVsbCA/ICdsaW5lXycgKyB0aWNrc0Nvb3Jkc1tpXS50aWNrVmFsdWUgOiBudWxsLFxuICAgICAgICBzaGFwZToge1xuICAgICAgICAgIHgxOiBwMVswXSxcbiAgICAgICAgICB5MTogcDFbMV0sXG4gICAgICAgICAgeDI6IHAyWzBdLFxuICAgICAgICAgIHkyOiBwMlsxXVxuICAgICAgICB9LFxuICAgICAgICBzdHlsZTogenJVdGlsLmRlZmF1bHRzKHtcbiAgICAgICAgICBzdHJva2U6IGxpbmVDb2xvcnNbY29sb3JJbmRleF1cbiAgICAgICAgfSwgbGluZVN0eWxlKSxcbiAgICAgICAgc2lsZW50OiB0cnVlXG4gICAgICB9KSkpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vQXhpc01vZGVsfSBheGlzTW9kZWxcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9jYXJ0ZXNpYW4vR3JpZE1vZGVsfSBncmlkTW9kZWxcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9zcGxpdEFyZWE6IGZ1bmN0aW9uIChheGlzTW9kZWwsIGdyaWRNb2RlbCkge1xuICAgIHZhciBheGlzID0gYXhpc01vZGVsLmF4aXM7XG5cbiAgICBpZiAoYXhpcy5zY2FsZS5pc0JsYW5rKCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgc3BsaXRBcmVhTW9kZWwgPSBheGlzTW9kZWwuZ2V0TW9kZWwoJ3NwbGl0QXJlYScpO1xuICAgIHZhciBhcmVhU3R5bGVNb2RlbCA9IHNwbGl0QXJlYU1vZGVsLmdldE1vZGVsKCdhcmVhU3R5bGUnKTtcbiAgICB2YXIgYXJlYUNvbG9ycyA9IGFyZWFTdHlsZU1vZGVsLmdldCgnY29sb3InKTtcbiAgICB2YXIgZ3JpZFJlY3QgPSBncmlkTW9kZWwuY29vcmRpbmF0ZVN5c3RlbS5nZXRSZWN0KCk7XG4gICAgdmFyIHRpY2tzQ29vcmRzID0gYXhpcy5nZXRUaWNrc0Nvb3Jkcyh7XG4gICAgICB0aWNrTW9kZWw6IHNwbGl0QXJlYU1vZGVsLFxuICAgICAgY2xhbXA6IHRydWVcbiAgICB9KTtcblxuICAgIGlmICghdGlja3NDb29yZHMubGVuZ3RoKSB7XG4gICAgICByZXR1cm47XG4gICAgfSAvLyBGb3IgTWFraW5nIGFwcHJvcHJpYXRlIHNwbGl0QXJlYSBhbmltYXRpb24sIHRoZSBjb2xvciBhbmQgYW5pZFxuICAgIC8vIHNob3VsZCBiZSBjb3JyZXNwb25kaW5nIHRvIHByZXZpb3VzIG9uZSBpZiBwb3NzaWJsZS5cblxuXG4gICAgdmFyIGFyZWFDb2xvcnNMZW4gPSBhcmVhQ29sb3JzLmxlbmd0aDtcbiAgICB2YXIgbGFzdFNwbGl0QXJlYUNvbG9ycyA9IHRoaXMuX3NwbGl0QXJlYUNvbG9ycztcbiAgICB2YXIgbmV3U3BsaXRBcmVhQ29sb3JzID0genJVdGlsLmNyZWF0ZUhhc2hNYXAoKTtcbiAgICB2YXIgY29sb3JJbmRleCA9IDA7XG5cbiAgICBpZiAobGFzdFNwbGl0QXJlYUNvbG9ycykge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aWNrc0Nvb3Jkcy5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgY0luZGV4ID0gbGFzdFNwbGl0QXJlYUNvbG9ycy5nZXQodGlja3NDb29yZHNbaV0udGlja1ZhbHVlKTtcblxuICAgICAgICBpZiAoY0luZGV4ICE9IG51bGwpIHtcbiAgICAgICAgICBjb2xvckluZGV4ID0gKGNJbmRleCArIChhcmVhQ29sb3JzTGVuIC0gMSkgKiBpKSAlIGFyZWFDb2xvcnNMZW47XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgcHJldiA9IGF4aXMudG9HbG9iYWxDb29yZCh0aWNrc0Nvb3Jkc1swXS5jb29yZCk7XG4gICAgdmFyIGFyZWFTdHlsZSA9IGFyZWFTdHlsZU1vZGVsLmdldEFyZWFTdHlsZSgpO1xuICAgIGFyZWFDb2xvcnMgPSB6clV0aWwuaXNBcnJheShhcmVhQ29sb3JzKSA/IGFyZWFDb2xvcnMgOiBbYXJlYUNvbG9yc107XG5cbiAgICBmb3IgKHZhciBpID0gMTsgaSA8IHRpY2tzQ29vcmRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdGlja0Nvb3JkID0gYXhpcy50b0dsb2JhbENvb3JkKHRpY2tzQ29vcmRzW2ldLmNvb3JkKTtcbiAgICAgIHZhciB4O1xuICAgICAgdmFyIHk7XG4gICAgICB2YXIgd2lkdGg7XG4gICAgICB2YXIgaGVpZ2h0O1xuXG4gICAgICBpZiAoYXhpcy5pc0hvcml6b250YWwoKSkge1xuICAgICAgICB4ID0gcHJldjtcbiAgICAgICAgeSA9IGdyaWRSZWN0Lnk7XG4gICAgICAgIHdpZHRoID0gdGlja0Nvb3JkIC0geDtcbiAgICAgICAgaGVpZ2h0ID0gZ3JpZFJlY3QuaGVpZ2h0O1xuICAgICAgICBwcmV2ID0geCArIHdpZHRoO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgeCA9IGdyaWRSZWN0Lng7XG4gICAgICAgIHkgPSBwcmV2O1xuICAgICAgICB3aWR0aCA9IGdyaWRSZWN0LndpZHRoO1xuICAgICAgICBoZWlnaHQgPSB0aWNrQ29vcmQgLSB5O1xuICAgICAgICBwcmV2ID0geSArIGhlaWdodDtcbiAgICAgIH1cblxuICAgICAgdmFyIHRpY2tWYWx1ZSA9IHRpY2tzQ29vcmRzW2kgLSAxXS50aWNrVmFsdWU7XG4gICAgICB0aWNrVmFsdWUgIT0gbnVsbCAmJiBuZXdTcGxpdEFyZWFDb2xvcnMuc2V0KHRpY2tWYWx1ZSwgY29sb3JJbmRleCk7XG5cbiAgICAgIHRoaXMuX2F4aXNHcm91cC5hZGQobmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgICAgIGFuaWQ6IHRpY2tWYWx1ZSAhPSBudWxsID8gJ2FyZWFfJyArIHRpY2tWYWx1ZSA6IG51bGwsXG4gICAgICAgIHNoYXBlOiB7XG4gICAgICAgICAgeDogeCxcbiAgICAgICAgICB5OiB5LFxuICAgICAgICAgIHdpZHRoOiB3aWR0aCxcbiAgICAgICAgICBoZWlnaHQ6IGhlaWdodFxuICAgICAgICB9LFxuICAgICAgICBzdHlsZTogenJVdGlsLmRlZmF1bHRzKHtcbiAgICAgICAgICBmaWxsOiBhcmVhQ29sb3JzW2NvbG9ySW5kZXhdXG4gICAgICAgIH0sIGFyZWFTdHlsZSksXG4gICAgICAgIHNpbGVudDogdHJ1ZVxuICAgICAgfSkpO1xuXG4gICAgICBjb2xvckluZGV4ID0gKGNvbG9ySW5kZXggKyAxKSAlIGFyZWFDb2xvcnNMZW47XG4gICAgfVxuXG4gICAgdGhpcy5fc3BsaXRBcmVhQ29sb3JzID0gbmV3U3BsaXRBcmVhQ29sb3JzO1xuICB9XG59KTtcbkNhcnRlc2lhbkF4aXNWaWV3LmV4dGVuZCh7XG4gIHR5cGU6ICd4QXhpcydcbn0pO1xuQ2FydGVzaWFuQXhpc1ZpZXcuZXh0ZW5kKHtcbiAgdHlwZTogJ3lBeGlzJ1xufSk7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBREE7QUFHQTtBQVhBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBWEE7QUFDQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEzTEE7QUE2TEE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axis/CartesianAxisView.js
