__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NCheckbox", function() { return NCheckbox; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NCheckbox.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      .ant-form-item-control-wrapper {\n        width: auto;\n      }\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var NCheckbox =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(NCheckbox, _ModelWidget);

  function NCheckbox(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, NCheckbox);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(NCheckbox).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(NCheckbox, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxDiv: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          label = _this$props.label,
          propName = _this$props.propName,
          onChange = _this$props.onChange,
          rules = _this$props.rules,
          required = _this$props.required,
          layoutCol = _this$props.layoutCol,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["form", "model", "label", "propName", "onChange", "rules", "required", "layoutCol"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.BoxDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], Object.assign({
        form: form,
        model: model,
        label: label,
        propName: propName,
        onChange: onChange,
        rules: rules,
        required: required,
        layoutCol: layoutCol
      }, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Checkbox"], Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }))));
    }
  }]);

  return NCheckbox;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9OQ2hlY2tib3gudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2NvbXBvbmVudC9OQ2hlY2tib3gudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IENoZWNrYm94IH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IENoZWNrYm94UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvY2hlY2tib3hcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBQYWdlQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IE5Gb3JtSXRlbVByb3BzIH0gZnJvbSBcIkBjb21wb25lbnQvTkZvcm1JdGVtXCI7XG5cbmV4cG9ydCB0eXBlIE5DaGVja2JveFByb3BzID0ge30gJiBORm9ybUl0ZW1Qcm9wcyAmIENoZWNrYm94UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFN0eWxlZERpdiA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7IHJlYWRPbmx5PzogYm9vbGVhbiB9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBOQ2hlY2tib3hDb21wb25lbnRzID0ge1xuICBCb3hEaXY6IFN0eWxlZERpdlxufSAmIFBhZ2VDb21wb25lbnRzO1xuXG5jbGFzcyBOQ2hlY2tib3g8UCBleHRlbmRzIE5DaGVja2JveFByb3BzLCBTLCBDIGV4dGVuZHMgTkNoZWNrYm94Q29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBOQ2hlY2tib3hQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94RGl2OiBTdHlsZWQuZGl2YFxuICAgICAgLmFudC1mb3JtLWl0ZW0tY29udHJvbC13cmFwcGVyIHtcbiAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICB9XG4gICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHtcbiAgICAgIGZvcm0sIG1vZGVsLCBsYWJlbCwgcHJvcE5hbWUsIG9uQ2hhbmdlLCBydWxlcywgcmVxdWlyZWQsIGxheW91dENvbCwgLi4ucmVzdFxuICAgIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hEaXY+XG4gICAgICAgIDxORm9ybUl0ZW0gey4uLnsgZm9ybSwgbW9kZWwsIGxhYmVsLCBwcm9wTmFtZSwgb25DaGFuZ2UsIHJ1bGVzLCByZXF1aXJlZCwgbGF5b3V0Q29sIH19PlxuICAgICAgICAgIDxDaGVja2JveCB7Li4ucmVzdH0vPlxuICAgICAgICA8L05Gb3JtSXRlbT5cbiAgICAgIDwvQy5Cb3hEaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBOQ2hlY2tib3ggfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBVUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQU9BOzs7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7OztBQTNCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/component/NCheckbox.tsx
