__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicBusiness", function() { return BasicBusiness; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-business.tsx";








var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var BasicBusiness =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(BasicBusiness, _QuoteStep);

  function BasicBusiness() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicBusiness);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicBusiness).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicBusiness, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicBusiness.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false
      };
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }, {
    key: "handleNext",
    value: function handleNext() {}
  }, {
    key: "renderActions",
    value: function renderActions() {}
  }, {
    key: "renderCompanyInfo",
    value: function renderCompanyInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderSubsidiary",
    value: function renderSubsidiary() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this, _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].QUOTE);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Save and Continue").thai("Save and Continue").my("Save and Continue").getMessage()));
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("BUSINESS DETAILS").thai("BUSINESS DETAILS").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["1", "2"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("COMPANY INFO").thai("COMPANY INFO").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }, this.renderCompanyInfo())), this.renderRightActions())));
    }
  }]);

  return BasicBusiness;
}(_quote_step__WEBPACK_IMPORTED_MODULE_13__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtYnVzaW5lc3MudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtYnVzaW5lc3MudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7UmVhY3R9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHtCdXR0b24sIENvbCwgUm93LCBTcGlufSBmcm9tIFwiYW50ZFwiO1xuXG5pbXBvcnQge0xhbmd1YWdlLCBVdGlsc30gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7TkNvbGxhcHNlLCBOUGFuZWx9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHtwYWdlVG99IGZyb20gXCJAZGVzay1jb21wb25lbnQvd2l6YXJkXCI7XG5cbmltcG9ydCBRdW90ZVN0ZXAsIHtTdGVwQ29tcG9uZW50cywgU3RlcFByb3BzfSBmcm9tIFwiLi4vLi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IHtBbGxTdGVwc30gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHtCdXR0b25CYWNrfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2FudGQvYnV0dG9uLWJhY2tcIjtcblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgbG9hZGluZzogYm9vbGVhbjtcbn1cblxuY2xhc3MgQmFzaWNCdXNpbmVzczxQIGV4dGVuZHMgU3RlcFByb3BzLCBTIGV4dGVuZHMgSVN0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCkpIGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiB7XG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICB9IGFzIFM7XG4gIH1cblxuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJUaXRsZSgpIHtcbiAgfVxuXG4gIGhhbmRsZU5leHQoKSB7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQWN0aW9ucygpIHtcbiAgfVxuXG4gIHJlbmRlckNvbXBhbnlJbmZvKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlclN1YnNpZGlhcnkoKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyU2lkZUluZm8oKSB7XG4gICAgcmV0dXJuIDw+PC8+O1xuICB9XG5cbiAgcmVuZGVyUmlnaHRBY3Rpb25zKCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgYWN0aW9uICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1hY3Rpb24tbGFyZ2VcIiA6IFwiXCJ9YH0+XG4gICAgICAgIDxCdXR0b25CYWNrIGhhbmRlbD17KCkgPT4ge1xuICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5RVU9URSk7XG4gICAgICAgIH19Lz5cbiAgICAgICAgPEJ1dHRvbiBzaXplPVwibGFyZ2VcIiB0eXBlPVwicHJpbWFyeVwiIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlTmV4dCgpfT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTYXZlIGFuZCBDb250aW51ZVwiKS50aGFpKFwiU2F2ZSBhbmQgQ29udGludWVcIikubXkoXCJTYXZlIGFuZCBDb250aW51ZVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIGdldFByb3AocHJvcE5hbWU/OiBzdHJpbmcpIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXt0aGlzLnN0YXRlLmxvYWRpbmd9PlxuICAgICAgICA8Um93IHR5cGU9XCJmbGV4XCIganVzdGlmeT17XCJzcGFjZS1iZXR3ZWVuXCJ9PlxuICAgICAgICAgIHt0aGlzLnJlbmRlclNpZGVJbmZvKCl9XG4gICAgICAgICAgPENvbCBzbT17MTh9IHhzPXsyM30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkJVU0lORVNTIERFVEFJTFNcIikudGhhaShcIkJVU0lORVNTIERFVEFJTFNcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8TkNvbGxhcHNlIGRlZmF1bHRBY3RpdmVLZXk9e1tcIjFcIiwgXCIyXCJdfT5cbiAgICAgICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAgICAgIGtleT1cIjFcIlxuICAgICAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJDT01QQU5ZIElORk9cIilcbiAgICAgICAgICAgICAgICAgIC50aGFpKFwiQ09NUEFOWSBJTkZPXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVyQ29tcGFueUluZm8oKX1cbiAgICAgICAgICAgICAgPC9OUGFuZWw+XG4gICAgICAgICAgICA8L05Db2xsYXBzZT5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlclJpZ2h0QWN0aW9ucygpfVxuICAgICAgICAgIDwvQ29sPlxuICAgICAgICA8L1Jvdz5cbiAgICAgIDwvU3Bpbj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCB7IEJhc2ljQnVzaW5lc3MgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBS0E7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0E7OztBQUdBOzs7QUFHQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBOzs7O0FBbEZBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-business.tsx
