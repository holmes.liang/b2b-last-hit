

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _eventlistener = __webpack_require__(/*! eventlistener */ "./node_modules/eventlistener/eventlistener.js");

var _lodash = __webpack_require__(/*! lodash.debounce */ "./node_modules/lodash.debounce/index.js");

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = __webpack_require__(/*! lodash.throttle */ "./node_modules/lodash.throttle/index.js");

var _lodash4 = _interopRequireDefault(_lodash3);

var _parentScroll = __webpack_require__(/*! ./utils/parentScroll */ "./node_modules/react-lazy-load/lib/utils/parentScroll.js");

var _parentScroll2 = _interopRequireDefault(_parentScroll);

var _inViewport = __webpack_require__(/*! ./utils/inViewport */ "./node_modules/react-lazy-load/lib/utils/inViewport.js");

var _inViewport2 = _interopRequireDefault(_inViewport);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var LazyLoad = function (_Component) {
  _inherits(LazyLoad, _Component);

  function LazyLoad(props) {
    _classCallCheck(this, LazyLoad);

    var _this = _possibleConstructorReturn(this, (LazyLoad.__proto__ || Object.getPrototypeOf(LazyLoad)).call(this, props));

    _this.lazyLoadHandler = _this.lazyLoadHandler.bind(_this);

    if (props.throttle > 0) {
      if (props.debounce) {
        _this.lazyLoadHandler = (0, _lodash2.default)(_this.lazyLoadHandler, props.throttle);
      } else {
        _this.lazyLoadHandler = (0, _lodash4.default)(_this.lazyLoadHandler, props.throttle);
      }
    }

    _this.state = {
      visible: false
    };
    return _this;
  }

  _createClass(LazyLoad, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this._mounted = true;
      var eventNode = this.getEventNode();
      this.lazyLoadHandler();

      if (this.lazyLoadHandler.flush) {
        this.lazyLoadHandler.flush();
      }

      (0, _eventlistener.add)(window, 'resize', this.lazyLoadHandler);
      (0, _eventlistener.add)(eventNode, 'scroll', this.lazyLoadHandler);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps() {
      if (!this.state.visible) {
        this.lazyLoadHandler();
      }
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(_nextProps, nextState) {
      return nextState.visible;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this._mounted = false;

      if (this.lazyLoadHandler.cancel) {
        this.lazyLoadHandler.cancel();
      }

      this.detachListeners();
    }
  }, {
    key: 'getEventNode',
    value: function getEventNode() {
      return (0, _parentScroll2.default)((0, _reactDom.findDOMNode)(this));
    }
  }, {
    key: 'getOffset',
    value: function getOffset() {
      var _props = this.props,
          offset = _props.offset,
          offsetVertical = _props.offsetVertical,
          offsetHorizontal = _props.offsetHorizontal,
          offsetTop = _props.offsetTop,
          offsetBottom = _props.offsetBottom,
          offsetLeft = _props.offsetLeft,
          offsetRight = _props.offsetRight,
          threshold = _props.threshold;

      var _offsetAll = threshold || offset;

      var _offsetVertical = offsetVertical || _offsetAll;

      var _offsetHorizontal = offsetHorizontal || _offsetAll;

      return {
        top: offsetTop || _offsetVertical,
        bottom: offsetBottom || _offsetVertical,
        left: offsetLeft || _offsetHorizontal,
        right: offsetRight || _offsetHorizontal
      };
    }
  }, {
    key: 'lazyLoadHandler',
    value: function lazyLoadHandler() {
      if (!this._mounted) {
        return;
      }

      var offset = this.getOffset();
      var node = (0, _reactDom.findDOMNode)(this);
      var eventNode = this.getEventNode();

      if ((0, _inViewport2.default)(node, eventNode, offset)) {
        var onContentVisible = this.props.onContentVisible;
        this.setState({
          visible: true
        }, function () {
          if (onContentVisible) {
            onContentVisible();
          }
        });
        this.detachListeners();
      }
    }
  }, {
    key: 'detachListeners',
    value: function detachListeners() {
      var eventNode = this.getEventNode();
      (0, _eventlistener.remove)(window, 'resize', this.lazyLoadHandler);
      (0, _eventlistener.remove)(eventNode, 'scroll', this.lazyLoadHandler);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          className = _props2.className,
          height = _props2.height,
          width = _props2.width;
      var visible = this.state.visible;
      var elStyles = {
        height: height,
        width: width
      };
      var elClasses = 'LazyLoad' + (visible ? ' is-visible' : '') + (className ? ' ' + className : '');
      return _react2.default.createElement(this.props.elementType, {
        className: elClasses,
        style: elStyles
      }, visible && _react.Children.only(children));
    }
  }]);

  return LazyLoad;
}(_react.Component);

exports.default = LazyLoad;
LazyLoad.propTypes = {
  children: _propTypes2.default.node.isRequired,
  className: _propTypes2.default.string,
  debounce: _propTypes2.default.bool,
  elementType: _propTypes2.default.string,
  height: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  offset: _propTypes2.default.number,
  offsetBottom: _propTypes2.default.number,
  offsetHorizontal: _propTypes2.default.number,
  offsetLeft: _propTypes2.default.number,
  offsetRight: _propTypes2.default.number,
  offsetTop: _propTypes2.default.number,
  offsetVertical: _propTypes2.default.number,
  threshold: _propTypes2.default.number,
  throttle: _propTypes2.default.number,
  width: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
  onContentVisible: _propTypes2.default.func
};
LazyLoad.defaultProps = {
  elementType: 'div',
  debounce: true,
  offset: 0,
  offsetBottom: 0,
  offsetHorizontal: 0,
  offsetLeft: 0,
  offsetRight: 0,
  offsetTop: 0,
  offsetVertical: 0,
  throttle: 250
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtbGF6eS1sb2FkL2xpYi9MYXp5TG9hZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWxhenktbG9hZC9saWIvTGF6eUxvYWQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX3JlYWN0RG9tID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBfZXZlbnRsaXN0ZW5lciA9IHJlcXVpcmUoJ2V2ZW50bGlzdGVuZXInKTtcblxudmFyIF9sb2Rhc2ggPSByZXF1aXJlKCdsb2Rhc2guZGVib3VuY2UnKTtcblxudmFyIF9sb2Rhc2gyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfbG9kYXNoKTtcblxudmFyIF9sb2Rhc2gzID0gcmVxdWlyZSgnbG9kYXNoLnRocm90dGxlJyk7XG5cbnZhciBfbG9kYXNoNCA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2xvZGFzaDMpO1xuXG52YXIgX3BhcmVudFNjcm9sbCA9IHJlcXVpcmUoJy4vdXRpbHMvcGFyZW50U2Nyb2xsJyk7XG5cbnZhciBfcGFyZW50U2Nyb2xsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3BhcmVudFNjcm9sbCk7XG5cbnZhciBfaW5WaWV3cG9ydCA9IHJlcXVpcmUoJy4vdXRpbHMvaW5WaWV3cG9ydCcpO1xuXG52YXIgX2luVmlld3BvcnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5WaWV3cG9ydCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIExhenlMb2FkID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExhenlMb2FkLCBfQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMYXp5TG9hZChwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMYXp5TG9hZCk7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoTGF6eUxvYWQuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihMYXp5TG9hZCkpLmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgIF90aGlzLmxhenlMb2FkSGFuZGxlciA9IF90aGlzLmxhenlMb2FkSGFuZGxlci5iaW5kKF90aGlzKTtcblxuICAgIGlmIChwcm9wcy50aHJvdHRsZSA+IDApIHtcbiAgICAgIGlmIChwcm9wcy5kZWJvdW5jZSkge1xuICAgICAgICBfdGhpcy5sYXp5TG9hZEhhbmRsZXIgPSAoMCwgX2xvZGFzaDIuZGVmYXVsdCkoX3RoaXMubGF6eUxvYWRIYW5kbGVyLCBwcm9wcy50aHJvdHRsZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfdGhpcy5sYXp5TG9hZEhhbmRsZXIgPSAoMCwgX2xvZGFzaDQuZGVmYXVsdCkoX3RoaXMubGF6eUxvYWRIYW5kbGVyLCBwcm9wcy50aHJvdHRsZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgX3RoaXMuc3RhdGUgPSB7IHZpc2libGU6IGZhbHNlIH07XG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKExhenlMb2FkLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB0aGlzLl9tb3VudGVkID0gdHJ1ZTtcbiAgICAgIHZhciBldmVudE5vZGUgPSB0aGlzLmdldEV2ZW50Tm9kZSgpO1xuXG4gICAgICB0aGlzLmxhenlMb2FkSGFuZGxlcigpO1xuXG4gICAgICBpZiAodGhpcy5sYXp5TG9hZEhhbmRsZXIuZmx1c2gpIHtcbiAgICAgICAgdGhpcy5sYXp5TG9hZEhhbmRsZXIuZmx1c2goKTtcbiAgICAgIH1cblxuICAgICAgKDAsIF9ldmVudGxpc3RlbmVyLmFkZCkod2luZG93LCAncmVzaXplJywgdGhpcy5sYXp5TG9hZEhhbmRsZXIpO1xuICAgICAgKDAsIF9ldmVudGxpc3RlbmVyLmFkZCkoZXZlbnROb2RlLCAnc2Nyb2xsJywgdGhpcy5sYXp5TG9hZEhhbmRsZXIpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKCkge1xuICAgICAgaWYgKCF0aGlzLnN0YXRlLnZpc2libGUpIHtcbiAgICAgICAgdGhpcy5sYXp5TG9hZEhhbmRsZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzaG91bGRDb21wb25lbnRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUoX25leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gICAgICByZXR1cm4gbmV4dFN0YXRlLnZpc2libGU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMuX21vdW50ZWQgPSBmYWxzZTtcbiAgICAgIGlmICh0aGlzLmxhenlMb2FkSGFuZGxlci5jYW5jZWwpIHtcbiAgICAgICAgdGhpcy5sYXp5TG9hZEhhbmRsZXIuY2FuY2VsKCk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZGV0YWNoTGlzdGVuZXJzKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0RXZlbnROb2RlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0RXZlbnROb2RlKCkge1xuICAgICAgcmV0dXJuICgwLCBfcGFyZW50U2Nyb2xsMi5kZWZhdWx0KSgoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKSh0aGlzKSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0T2Zmc2V0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0T2Zmc2V0KCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgb2Zmc2V0ID0gX3Byb3BzLm9mZnNldCxcbiAgICAgICAgICBvZmZzZXRWZXJ0aWNhbCA9IF9wcm9wcy5vZmZzZXRWZXJ0aWNhbCxcbiAgICAgICAgICBvZmZzZXRIb3Jpem9udGFsID0gX3Byb3BzLm9mZnNldEhvcml6b250YWwsXG4gICAgICAgICAgb2Zmc2V0VG9wID0gX3Byb3BzLm9mZnNldFRvcCxcbiAgICAgICAgICBvZmZzZXRCb3R0b20gPSBfcHJvcHMub2Zmc2V0Qm90dG9tLFxuICAgICAgICAgIG9mZnNldExlZnQgPSBfcHJvcHMub2Zmc2V0TGVmdCxcbiAgICAgICAgICBvZmZzZXRSaWdodCA9IF9wcm9wcy5vZmZzZXRSaWdodCxcbiAgICAgICAgICB0aHJlc2hvbGQgPSBfcHJvcHMudGhyZXNob2xkO1xuXG5cbiAgICAgIHZhciBfb2Zmc2V0QWxsID0gdGhyZXNob2xkIHx8IG9mZnNldDtcbiAgICAgIHZhciBfb2Zmc2V0VmVydGljYWwgPSBvZmZzZXRWZXJ0aWNhbCB8fCBfb2Zmc2V0QWxsO1xuICAgICAgdmFyIF9vZmZzZXRIb3Jpem9udGFsID0gb2Zmc2V0SG9yaXpvbnRhbCB8fCBfb2Zmc2V0QWxsO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICB0b3A6IG9mZnNldFRvcCB8fCBfb2Zmc2V0VmVydGljYWwsXG4gICAgICAgIGJvdHRvbTogb2Zmc2V0Qm90dG9tIHx8IF9vZmZzZXRWZXJ0aWNhbCxcbiAgICAgICAgbGVmdDogb2Zmc2V0TGVmdCB8fCBfb2Zmc2V0SG9yaXpvbnRhbCxcbiAgICAgICAgcmlnaHQ6IG9mZnNldFJpZ2h0IHx8IF9vZmZzZXRIb3Jpem9udGFsXG4gICAgICB9O1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2xhenlMb2FkSGFuZGxlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGxhenlMb2FkSGFuZGxlcigpIHtcbiAgICAgIGlmICghdGhpcy5fbW91bnRlZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB2YXIgb2Zmc2V0ID0gdGhpcy5nZXRPZmZzZXQoKTtcbiAgICAgIHZhciBub2RlID0gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkodGhpcyk7XG4gICAgICB2YXIgZXZlbnROb2RlID0gdGhpcy5nZXRFdmVudE5vZGUoKTtcblxuICAgICAgaWYgKCgwLCBfaW5WaWV3cG9ydDIuZGVmYXVsdCkobm9kZSwgZXZlbnROb2RlLCBvZmZzZXQpKSB7XG4gICAgICAgIHZhciBvbkNvbnRlbnRWaXNpYmxlID0gdGhpcy5wcm9wcy5vbkNvbnRlbnRWaXNpYmxlO1xuXG5cbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHZpc2libGU6IHRydWUgfSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmIChvbkNvbnRlbnRWaXNpYmxlKSB7XG4gICAgICAgICAgICBvbkNvbnRlbnRWaXNpYmxlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5kZXRhY2hMaXN0ZW5lcnMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdkZXRhY2hMaXN0ZW5lcnMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBkZXRhY2hMaXN0ZW5lcnMoKSB7XG4gICAgICB2YXIgZXZlbnROb2RlID0gdGhpcy5nZXRFdmVudE5vZGUoKTtcblxuICAgICAgKDAsIF9ldmVudGxpc3RlbmVyLnJlbW92ZSkod2luZG93LCAncmVzaXplJywgdGhpcy5sYXp5TG9hZEhhbmRsZXIpO1xuICAgICAgKDAsIF9ldmVudGxpc3RlbmVyLnJlbW92ZSkoZXZlbnROb2RlLCAnc2Nyb2xsJywgdGhpcy5sYXp5TG9hZEhhbmRsZXIpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wczIuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzMi5jbGFzc05hbWUsXG4gICAgICAgICAgaGVpZ2h0ID0gX3Byb3BzMi5oZWlnaHQsXG4gICAgICAgICAgd2lkdGggPSBfcHJvcHMyLndpZHRoO1xuICAgICAgdmFyIHZpc2libGUgPSB0aGlzLnN0YXRlLnZpc2libGU7XG5cblxuICAgICAgdmFyIGVsU3R5bGVzID0geyBoZWlnaHQ6IGhlaWdodCwgd2lkdGg6IHdpZHRoIH07XG4gICAgICB2YXIgZWxDbGFzc2VzID0gJ0xhenlMb2FkJyArICh2aXNpYmxlID8gJyBpcy12aXNpYmxlJyA6ICcnKSArIChjbGFzc05hbWUgPyAnICcgKyBjbGFzc05hbWUgOiAnJyk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCh0aGlzLnByb3BzLmVsZW1lbnRUeXBlLCB7XG4gICAgICAgIGNsYXNzTmFtZTogZWxDbGFzc2VzLFxuICAgICAgICBzdHlsZTogZWxTdHlsZXNcbiAgICAgIH0sIHZpc2libGUgJiYgX3JlYWN0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pKTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gTGF6eUxvYWQ7XG59KF9yZWFjdC5Db21wb25lbnQpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBMYXp5TG9hZDtcblxuXG5MYXp5TG9hZC5wcm9wVHlwZXMgPSB7XG4gIGNoaWxkcmVuOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm5vZGUuaXNSZXF1aXJlZCxcbiAgY2xhc3NOYW1lOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgZGVib3VuY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgZWxlbWVudFR5cGU6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICBoZWlnaHQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXJdKSxcbiAgb2Zmc2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcbiAgb2Zmc2V0Qm90dG9tOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcbiAgb2Zmc2V0SG9yaXpvbnRhbDogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG4gIG9mZnNldExlZnQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQubnVtYmVyLFxuICBvZmZzZXRSaWdodDogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG4gIG9mZnNldFRvcDogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG4gIG9mZnNldFZlcnRpY2FsOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcbiAgdGhyZXNob2xkOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcbiAgdGhyb3R0bGU6IF9wcm9wVHlwZXMyLmRlZmF1bHQubnVtYmVyLFxuICB3aWR0aDogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcl0pLFxuICBvbkNvbnRlbnRWaXNpYmxlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNcbn07XG5cbkxhenlMb2FkLmRlZmF1bHRQcm9wcyA9IHtcbiAgZWxlbWVudFR5cGU6ICdkaXYnLFxuICBkZWJvdW5jZTogdHJ1ZSxcbiAgb2Zmc2V0OiAwLFxuICBvZmZzZXRCb3R0b206IDAsXG4gIG9mZnNldEhvcml6b250YWw6IDAsXG4gIG9mZnNldExlZnQ6IDAsXG4gIG9mZnNldFJpZ2h0OiAwLFxuICBvZmZzZXRUb3A6IDAsXG4gIG9mZnNldFZlcnRpY2FsOiAwLFxuICB0aHJvdHRsZTogMjUwXG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQXhCQTtBQTBCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJCQTtBQXVCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQWxCQTtBQUNBO0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-lazy-load/lib/LazyLoad.js
