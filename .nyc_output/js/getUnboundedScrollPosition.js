/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

/**
 * Gets the scroll position of the supplied element or window.
 *
 * The return values are unbounded, unlike `getScrollPosition`. This means they
 * may be negative or exceed the element boundaries (which is possible using
 * inertial scrolling).
 *
 * @param {DOMWindow|DOMElement} scrollable
 * @return {object} Map with `x` and `y` keys.
 */

function getUnboundedScrollPosition(scrollable) {
  if (scrollable.Window && scrollable instanceof scrollable.Window) {
    return {
      x: scrollable.pageXOffset || scrollable.document.documentElement.scrollLeft,
      y: scrollable.pageYOffset || scrollable.document.documentElement.scrollTop
    };
  }

  return {
    x: scrollable.scrollLeft,
    y: scrollable.scrollTop
  };
}

module.exports = getUnboundedScrollPosition;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0VW5ib3VuZGVkU2Nyb2xsUG9zaXRpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9nZXRVbmJvdW5kZWRTY3JvbGxQb3NpdGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICpcbiAqIEB0eXBlY2hlY2tzXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIEdldHMgdGhlIHNjcm9sbCBwb3NpdGlvbiBvZiB0aGUgc3VwcGxpZWQgZWxlbWVudCBvciB3aW5kb3cuXG4gKlxuICogVGhlIHJldHVybiB2YWx1ZXMgYXJlIHVuYm91bmRlZCwgdW5saWtlIGBnZXRTY3JvbGxQb3NpdGlvbmAuIFRoaXMgbWVhbnMgdGhleVxuICogbWF5IGJlIG5lZ2F0aXZlIG9yIGV4Y2VlZCB0aGUgZWxlbWVudCBib3VuZGFyaWVzICh3aGljaCBpcyBwb3NzaWJsZSB1c2luZ1xuICogaW5lcnRpYWwgc2Nyb2xsaW5nKS5cbiAqXG4gKiBAcGFyYW0ge0RPTVdpbmRvd3xET01FbGVtZW50fSBzY3JvbGxhYmxlXG4gKiBAcmV0dXJuIHtvYmplY3R9IE1hcCB3aXRoIGB4YCBhbmQgYHlgIGtleXMuXG4gKi9cblxuZnVuY3Rpb24gZ2V0VW5ib3VuZGVkU2Nyb2xsUG9zaXRpb24oc2Nyb2xsYWJsZSkge1xuICBpZiAoc2Nyb2xsYWJsZS5XaW5kb3cgJiYgc2Nyb2xsYWJsZSBpbnN0YW5jZW9mIHNjcm9sbGFibGUuV2luZG93KSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHg6IHNjcm9sbGFibGUucGFnZVhPZmZzZXQgfHwgc2Nyb2xsYWJsZS5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdCxcbiAgICAgIHk6IHNjcm9sbGFibGUucGFnZVlPZmZzZXQgfHwgc2Nyb2xsYWJsZS5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wXG4gICAgfTtcbiAgfVxuICByZXR1cm4ge1xuICAgIHg6IHNjcm9sbGFibGUuc2Nyb2xsTGVmdCxcbiAgICB5OiBzY3JvbGxhYmxlLnNjcm9sbFRvcFxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFVuYm91bmRlZFNjcm9sbFBvc2l0aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7O0FBU0E7QUFFQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getUnboundedScrollPosition.js
