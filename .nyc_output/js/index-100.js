__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Breadcrumb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Breadcrumb */ "./node_modules/antd/es/breadcrumb/Breadcrumb.js");
/* harmony import */ var _BreadcrumbItem__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BreadcrumbItem */ "./node_modules/antd/es/breadcrumb/BreadcrumbItem.js");
/* harmony import */ var _BreadcrumbSeparator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BreadcrumbSeparator */ "./node_modules/antd/es/breadcrumb/BreadcrumbSeparator.js");



_Breadcrumb__WEBPACK_IMPORTED_MODULE_0__["default"].Item = _BreadcrumbItem__WEBPACK_IMPORTED_MODULE_1__["default"];
_Breadcrumb__WEBPACK_IMPORTED_MODULE_0__["default"].Separator = _BreadcrumbSeparator__WEBPACK_IMPORTED_MODULE_2__["default"];
/* harmony default export */ __webpack_exports__["default"] = (_Breadcrumb__WEBPACK_IMPORTED_MODULE_0__["default"]);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9icmVhZGNydW1iL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9icmVhZGNydW1iL2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQnJlYWRjcnVtYiBmcm9tICcuL0JyZWFkY3J1bWInO1xuaW1wb3J0IEJyZWFkY3J1bWJJdGVtIGZyb20gJy4vQnJlYWRjcnVtYkl0ZW0nO1xuaW1wb3J0IEJyZWFkY3J1bWJTZXBhcmF0b3IgZnJvbSAnLi9CcmVhZGNydW1iU2VwYXJhdG9yJztcbkJyZWFkY3J1bWIuSXRlbSA9IEJyZWFkY3J1bWJJdGVtO1xuQnJlYWRjcnVtYi5TZXBhcmF0b3IgPSBCcmVhZGNydW1iU2VwYXJhdG9yO1xuZXhwb3J0IGRlZmF1bHQgQnJlYWRjcnVtYjtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/breadcrumb/index.js
