/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
 // This alternative WebpackDevServer combines the functionality of:
// https://github.com/webpack/webpack-dev-server/blob/webpack-1/client/index.js
// https://github.com/webpack/webpack/blob/webpack-1/hot/dev-server.js
// It only supports their simplest configuration (hot updates on same server).
// It makes some opinionated choices on top, like adding a syntax error overlay
// that looks similar to our console output. The error overlay is inspired by:
// https://github.com/glenjamin/webpack-hot-middleware

var stripAnsi = __webpack_require__(/*! strip-ansi */ "./node_modules/react-dev-utils/node_modules/strip-ansi/index.js");

var url = __webpack_require__(/*! url */ "./node_modules/url/url.js");

var launchEditorEndpoint = __webpack_require__(/*! ./launchEditorEndpoint */ "./node_modules/react-dev-utils/launchEditorEndpoint.js");

var formatWebpackMessages = __webpack_require__(/*! ./formatWebpackMessages */ "./node_modules/react-dev-utils/formatWebpackMessages.js");

var ErrorOverlay = __webpack_require__(/*! react-error-overlay */ "./node_modules/react-dev-utils/node_modules/react-error-overlay/lib/index.js");

ErrorOverlay.setEditorHandler(function editorHandler(errorLocation) {
  // Keep this sync with errorOverlayMiddleware.js
  fetch(launchEditorEndpoint + '?fileName=' + window.encodeURIComponent(errorLocation.fileName) + '&lineNumber=' + window.encodeURIComponent(errorLocation.lineNumber || 1) + '&colNumber=' + window.encodeURIComponent(errorLocation.colNumber || 1));
}); // We need to keep track of if there has been a runtime error.
// Essentially, we cannot guarantee application state was not corrupted by the
// runtime error. To prevent confusing behavior, we forcibly reload the entire
// application. This is handled below when we are notified of a compile (code
// change).
// See https://github.com/facebook/create-react-app/issues/3096

var hadRuntimeError = false;
ErrorOverlay.startReportingRuntimeErrors({
  onError: function onError() {
    hadRuntimeError = true;
  },
  filename: '/static/js/bundle.js'
});

if ( true && typeof module.hot.dispose === 'function') {
  module.hot.dispose(function () {
    // TODO: why do we need this?
    ErrorOverlay.stopReportingRuntimeErrors();
  });
} // Connect to WebpackDevServer via a socket.


var connection = new WebSocket(url.format({
  protocol: window.location.protocol === 'https:' ? 'wss' : 'ws',
  hostname: window.location.hostname,
  port: window.location.port,
  // Hardcoded in WebpackDevServer
  pathname: '/sockjs-node',
  slashes: true
})); // Unlike WebpackDevServer client, we won't try to reconnect
// to avoid spamming the console. Disconnect usually happens
// when developer stops the server.

connection.onclose = function () {
  if (typeof console !== 'undefined' && typeof console.info === 'function') {
    console.info('The development server has disconnected.\nRefresh the page if necessary.');
  }
}; // Remember some state related to hot module replacement.


var isFirstCompilation = true;
var mostRecentCompilationHash = null;
var hasCompileErrors = false;

function clearOutdatedErrors() {
  // Clean up outdated compile errors, if any.
  if (typeof console !== 'undefined' && typeof console.clear === 'function') {
    if (hasCompileErrors) {
      console.clear();
    }
  }
} // Successful compilation.


function handleSuccess() {
  clearOutdatedErrors();
  var isHotUpdate = !isFirstCompilation;
  isFirstCompilation = false;
  hasCompileErrors = false; // Attempt to apply hot updates or reload.

  if (isHotUpdate) {
    tryApplyUpdates(function onHotUpdateSuccess() {
      // Only dismiss it when we're sure it's a hot update.
      // Otherwise it would flicker right before the reload.
      tryDismissErrorOverlay();
    });
  }
} // Compilation with warnings (e.g. ESLint).


function handleWarnings(warnings) {
  clearOutdatedErrors();
  var isHotUpdate = !isFirstCompilation;
  isFirstCompilation = false;
  hasCompileErrors = false;

  function printWarnings() {
    // Print warnings to the console.
    var formatted = formatWebpackMessages({
      warnings: warnings,
      errors: []
    });

    if (typeof console !== 'undefined' && typeof console.warn === 'function') {
      for (var i = 0; i < formatted.warnings.length; i++) {
        if (i === 5) {
          console.warn('There were more warnings in other files.\n' + 'You can find a complete log in the terminal.');
          break;
        }

        console.warn(stripAnsi(formatted.warnings[i]));
      }
    }
  }

  printWarnings(); // Attempt to apply hot updates or reload.

  if (isHotUpdate) {
    tryApplyUpdates(function onSuccessfulHotUpdate() {
      // Only dismiss it when we're sure it's a hot update.
      // Otherwise it would flicker right before the reload.
      tryDismissErrorOverlay();
    });
  }
} // Compilation with errors (e.g. syntax error or missing modules).


function handleErrors(errors) {
  clearOutdatedErrors();
  isFirstCompilation = false;
  hasCompileErrors = true; // "Massage" webpack messages.

  var formatted = formatWebpackMessages({
    errors: errors,
    warnings: []
  }); // Only show the first error.

  ErrorOverlay.reportBuildError(formatted.errors[0]); // Also log them to the console.

  if (typeof console !== 'undefined' && typeof console.error === 'function') {
    for (var i = 0; i < formatted.errors.length; i++) {
      console.error(stripAnsi(formatted.errors[i]));
    }
  } // Do not attempt to reload now.
  // We will reload on next success instead.

}

function tryDismissErrorOverlay() {
  if (!hasCompileErrors) {
    ErrorOverlay.dismissBuildError();
  }
} // There is a newer version of the code available.


function handleAvailableHash(hash) {
  // Update last known compilation hash.
  mostRecentCompilationHash = hash;
} // Handle messages from the server.


connection.onmessage = function (e) {
  var message = JSON.parse(e.data);

  switch (message.type) {
    case 'hash':
      handleAvailableHash(message.data);
      break;

    case 'still-ok':
    case 'ok':
      handleSuccess();
      break;

    case 'content-changed':
      // Triggered when a file from `contentBase` changed.
      window.location.reload();
      break;

    case 'warnings':
      handleWarnings(message.data);
      break;

    case 'errors':
      handleErrors(message.data);
      break;

    default: // Do nothing.

  }
}; // Is there a newer version of this code available?


function isUpdateAvailable() {
  /* globals __webpack_hash__ */
  // __webpack_hash__ is the hash of the current compilation.
  // It's a global variable injected by Webpack.
  return mostRecentCompilationHash !== __webpack_require__.h();
} // Webpack disallows updates in other states.


function canApplyUpdates() {
  return module.hot.status() === 'idle';
} // Attempt to update code on the fly, fall back to a hard reload.


function tryApplyUpdates(onHotUpdateSuccess) {
  if (false) {}

  if (!isUpdateAvailable() || !canApplyUpdates()) {
    return;
  }

  function handleApplyUpdates(err, updatedModules) {
    if (err || !updatedModules || hadRuntimeError) {
      window.location.reload();
      return;
    }

    if (typeof onHotUpdateSuccess === 'function') {
      // Maybe we want to do something.
      onHotUpdateSuccess();
    }

    if (isUpdateAvailable()) {
      // While we were updating, there was a new update! Do it again.
      tryApplyUpdates();
    }
  } // https://webpack.github.io/docs/hot-module-replacement.html#check


  var result = module.hot.check(
  /* autoApply */
  true, handleApplyUpdates); // // Webpack 2 returns a Promise instead of invoking a callback

  if (result && result.then) {
    result.then(function (updatedModules) {
      handleApplyUpdates(null, updatedModules);
    }, function (err) {
      handleApplyUpdates(err, null);
    });
  }
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtZGV2LXV0aWxzL3dlYnBhY2tIb3REZXZDbGllbnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1kZXYtdXRpbHMvd2VicGFja0hvdERldkNsaWVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxNS1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLy8gVGhpcyBhbHRlcm5hdGl2ZSBXZWJwYWNrRGV2U2VydmVyIGNvbWJpbmVzIHRoZSBmdW5jdGlvbmFsaXR5IG9mOlxuLy8gaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2svd2VicGFjay1kZXYtc2VydmVyL2Jsb2Ivd2VicGFjay0xL2NsaWVudC9pbmRleC5qc1xuLy8gaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2svd2VicGFjay9ibG9iL3dlYnBhY2stMS9ob3QvZGV2LXNlcnZlci5qc1xuXG4vLyBJdCBvbmx5IHN1cHBvcnRzIHRoZWlyIHNpbXBsZXN0IGNvbmZpZ3VyYXRpb24gKGhvdCB1cGRhdGVzIG9uIHNhbWUgc2VydmVyKS5cbi8vIEl0IG1ha2VzIHNvbWUgb3BpbmlvbmF0ZWQgY2hvaWNlcyBvbiB0b3AsIGxpa2UgYWRkaW5nIGEgc3ludGF4IGVycm9yIG92ZXJsYXlcbi8vIHRoYXQgbG9va3Mgc2ltaWxhciB0byBvdXIgY29uc29sZSBvdXRwdXQuIFRoZSBlcnJvciBvdmVybGF5IGlzIGluc3BpcmVkIGJ5OlxuLy8gaHR0cHM6Ly9naXRodWIuY29tL2dsZW5qYW1pbi93ZWJwYWNrLWhvdC1taWRkbGV3YXJlXG5cbnZhciBzdHJpcEFuc2kgPSByZXF1aXJlKCdzdHJpcC1hbnNpJyk7XG52YXIgdXJsID0gcmVxdWlyZSgndXJsJyk7XG52YXIgbGF1bmNoRWRpdG9yRW5kcG9pbnQgPSByZXF1aXJlKCcuL2xhdW5jaEVkaXRvckVuZHBvaW50Jyk7XG52YXIgZm9ybWF0V2VicGFja01lc3NhZ2VzID0gcmVxdWlyZSgnLi9mb3JtYXRXZWJwYWNrTWVzc2FnZXMnKTtcbnZhciBFcnJvck92ZXJsYXkgPSByZXF1aXJlKCdyZWFjdC1lcnJvci1vdmVybGF5Jyk7XG5cbkVycm9yT3ZlcmxheS5zZXRFZGl0b3JIYW5kbGVyKGZ1bmN0aW9uIGVkaXRvckhhbmRsZXIoZXJyb3JMb2NhdGlvbikge1xuICAvLyBLZWVwIHRoaXMgc3luYyB3aXRoIGVycm9yT3ZlcmxheU1pZGRsZXdhcmUuanNcbiAgZmV0Y2goXG4gICAgbGF1bmNoRWRpdG9yRW5kcG9pbnQgK1xuICAgICAgJz9maWxlTmFtZT0nICtcbiAgICAgIHdpbmRvdy5lbmNvZGVVUklDb21wb25lbnQoZXJyb3JMb2NhdGlvbi5maWxlTmFtZSkgK1xuICAgICAgJyZsaW5lTnVtYmVyPScgK1xuICAgICAgd2luZG93LmVuY29kZVVSSUNvbXBvbmVudChlcnJvckxvY2F0aW9uLmxpbmVOdW1iZXIgfHwgMSkgK1xuICAgICAgJyZjb2xOdW1iZXI9JyArXG4gICAgICB3aW5kb3cuZW5jb2RlVVJJQ29tcG9uZW50KGVycm9yTG9jYXRpb24uY29sTnVtYmVyIHx8IDEpXG4gICk7XG59KTtcblxuLy8gV2UgbmVlZCB0byBrZWVwIHRyYWNrIG9mIGlmIHRoZXJlIGhhcyBiZWVuIGEgcnVudGltZSBlcnJvci5cbi8vIEVzc2VudGlhbGx5LCB3ZSBjYW5ub3QgZ3VhcmFudGVlIGFwcGxpY2F0aW9uIHN0YXRlIHdhcyBub3QgY29ycnVwdGVkIGJ5IHRoZVxuLy8gcnVudGltZSBlcnJvci4gVG8gcHJldmVudCBjb25mdXNpbmcgYmVoYXZpb3IsIHdlIGZvcmNpYmx5IHJlbG9hZCB0aGUgZW50aXJlXG4vLyBhcHBsaWNhdGlvbi4gVGhpcyBpcyBoYW5kbGVkIGJlbG93IHdoZW4gd2UgYXJlIG5vdGlmaWVkIG9mIGEgY29tcGlsZSAoY29kZVxuLy8gY2hhbmdlKS5cbi8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svY3JlYXRlLXJlYWN0LWFwcC9pc3N1ZXMvMzA5NlxudmFyIGhhZFJ1bnRpbWVFcnJvciA9IGZhbHNlO1xuRXJyb3JPdmVybGF5LnN0YXJ0UmVwb3J0aW5nUnVudGltZUVycm9ycyh7XG4gIG9uRXJyb3I6IGZ1bmN0aW9uKCkge1xuICAgIGhhZFJ1bnRpbWVFcnJvciA9IHRydWU7XG4gIH0sXG4gIGZpbGVuYW1lOiAnL3N0YXRpYy9qcy9idW5kbGUuanMnLFxufSk7XG5cbmlmIChtb2R1bGUuaG90ICYmIHR5cGVvZiBtb2R1bGUuaG90LmRpc3Bvc2UgPT09ICdmdW5jdGlvbicpIHtcbiAgbW9kdWxlLmhvdC5kaXNwb3NlKGZ1bmN0aW9uKCkge1xuICAgIC8vIFRPRE86IHdoeSBkbyB3ZSBuZWVkIHRoaXM/XG4gICAgRXJyb3JPdmVybGF5LnN0b3BSZXBvcnRpbmdSdW50aW1lRXJyb3JzKCk7XG4gIH0pO1xufVxuXG4vLyBDb25uZWN0IHRvIFdlYnBhY2tEZXZTZXJ2ZXIgdmlhIGEgc29ja2V0LlxudmFyIGNvbm5lY3Rpb24gPSBuZXcgV2ViU29ja2V0KFxuICB1cmwuZm9ybWF0KHtcbiAgICBwcm90b2NvbDogd2luZG93LmxvY2F0aW9uLnByb3RvY29sID09PSAnaHR0cHM6JyA/ICd3c3MnIDogJ3dzJyxcbiAgICBob3N0bmFtZTogd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lLFxuICAgIHBvcnQ6IHdpbmRvdy5sb2NhdGlvbi5wb3J0LFxuICAgIC8vIEhhcmRjb2RlZCBpbiBXZWJwYWNrRGV2U2VydmVyXG4gICAgcGF0aG5hbWU6ICcvc29ja2pzLW5vZGUnLFxuICAgIHNsYXNoZXM6IHRydWUsXG4gIH0pXG4pO1xuXG4vLyBVbmxpa2UgV2VicGFja0RldlNlcnZlciBjbGllbnQsIHdlIHdvbid0IHRyeSB0byByZWNvbm5lY3Rcbi8vIHRvIGF2b2lkIHNwYW1taW5nIHRoZSBjb25zb2xlLiBEaXNjb25uZWN0IHVzdWFsbHkgaGFwcGVuc1xuLy8gd2hlbiBkZXZlbG9wZXIgc3RvcHMgdGhlIHNlcnZlci5cbmNvbm5lY3Rpb24ub25jbG9zZSA9IGZ1bmN0aW9uKCkge1xuICBpZiAodHlwZW9mIGNvbnNvbGUgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBjb25zb2xlLmluZm8gPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb25zb2xlLmluZm8oXG4gICAgICAnVGhlIGRldmVsb3BtZW50IHNlcnZlciBoYXMgZGlzY29ubmVjdGVkLlxcblJlZnJlc2ggdGhlIHBhZ2UgaWYgbmVjZXNzYXJ5LidcbiAgICApO1xuICB9XG59O1xuXG4vLyBSZW1lbWJlciBzb21lIHN0YXRlIHJlbGF0ZWQgdG8gaG90IG1vZHVsZSByZXBsYWNlbWVudC5cbnZhciBpc0ZpcnN0Q29tcGlsYXRpb24gPSB0cnVlO1xudmFyIG1vc3RSZWNlbnRDb21waWxhdGlvbkhhc2ggPSBudWxsO1xudmFyIGhhc0NvbXBpbGVFcnJvcnMgPSBmYWxzZTtcblxuZnVuY3Rpb24gY2xlYXJPdXRkYXRlZEVycm9ycygpIHtcbiAgLy8gQ2xlYW4gdXAgb3V0ZGF0ZWQgY29tcGlsZSBlcnJvcnMsIGlmIGFueS5cbiAgaWYgKHR5cGVvZiBjb25zb2xlICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgY29uc29sZS5jbGVhciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGlmIChoYXNDb21waWxlRXJyb3JzKSB7XG4gICAgICBjb25zb2xlLmNsZWFyKCk7XG4gICAgfVxuICB9XG59XG5cbi8vIFN1Y2Nlc3NmdWwgY29tcGlsYXRpb24uXG5mdW5jdGlvbiBoYW5kbGVTdWNjZXNzKCkge1xuICBjbGVhck91dGRhdGVkRXJyb3JzKCk7XG5cbiAgdmFyIGlzSG90VXBkYXRlID0gIWlzRmlyc3RDb21waWxhdGlvbjtcbiAgaXNGaXJzdENvbXBpbGF0aW9uID0gZmFsc2U7XG4gIGhhc0NvbXBpbGVFcnJvcnMgPSBmYWxzZTtcblxuICAvLyBBdHRlbXB0IHRvIGFwcGx5IGhvdCB1cGRhdGVzIG9yIHJlbG9hZC5cbiAgaWYgKGlzSG90VXBkYXRlKSB7XG4gICAgdHJ5QXBwbHlVcGRhdGVzKGZ1bmN0aW9uIG9uSG90VXBkYXRlU3VjY2VzcygpIHtcbiAgICAgIC8vIE9ubHkgZGlzbWlzcyBpdCB3aGVuIHdlJ3JlIHN1cmUgaXQncyBhIGhvdCB1cGRhdGUuXG4gICAgICAvLyBPdGhlcndpc2UgaXQgd291bGQgZmxpY2tlciByaWdodCBiZWZvcmUgdGhlIHJlbG9hZC5cbiAgICAgIHRyeURpc21pc3NFcnJvck92ZXJsYXkoKTtcbiAgICB9KTtcbiAgfVxufVxuXG4vLyBDb21waWxhdGlvbiB3aXRoIHdhcm5pbmdzIChlLmcuIEVTTGludCkuXG5mdW5jdGlvbiBoYW5kbGVXYXJuaW5ncyh3YXJuaW5ncykge1xuICBjbGVhck91dGRhdGVkRXJyb3JzKCk7XG5cbiAgdmFyIGlzSG90VXBkYXRlID0gIWlzRmlyc3RDb21waWxhdGlvbjtcbiAgaXNGaXJzdENvbXBpbGF0aW9uID0gZmFsc2U7XG4gIGhhc0NvbXBpbGVFcnJvcnMgPSBmYWxzZTtcblxuICBmdW5jdGlvbiBwcmludFdhcm5pbmdzKCkge1xuICAgIC8vIFByaW50IHdhcm5pbmdzIHRvIHRoZSBjb25zb2xlLlxuICAgIHZhciBmb3JtYXR0ZWQgPSBmb3JtYXRXZWJwYWNrTWVzc2FnZXMoe1xuICAgICAgd2FybmluZ3M6IHdhcm5pbmdzLFxuICAgICAgZXJyb3JzOiBbXSxcbiAgICB9KTtcblxuICAgIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGNvbnNvbGUud2FybiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmb3JtYXR0ZWQud2FybmluZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGkgPT09IDUpIHtcbiAgICAgICAgICBjb25zb2xlLndhcm4oXG4gICAgICAgICAgICAnVGhlcmUgd2VyZSBtb3JlIHdhcm5pbmdzIGluIG90aGVyIGZpbGVzLlxcbicgK1xuICAgICAgICAgICAgICAnWW91IGNhbiBmaW5kIGEgY29tcGxldGUgbG9nIGluIHRoZSB0ZXJtaW5hbC4nXG4gICAgICAgICAgKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjb25zb2xlLndhcm4oc3RyaXBBbnNpKGZvcm1hdHRlZC53YXJuaW5nc1tpXSkpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaW50V2FybmluZ3MoKTtcblxuICAvLyBBdHRlbXB0IHRvIGFwcGx5IGhvdCB1cGRhdGVzIG9yIHJlbG9hZC5cbiAgaWYgKGlzSG90VXBkYXRlKSB7XG4gICAgdHJ5QXBwbHlVcGRhdGVzKGZ1bmN0aW9uIG9uU3VjY2Vzc2Z1bEhvdFVwZGF0ZSgpIHtcbiAgICAgIC8vIE9ubHkgZGlzbWlzcyBpdCB3aGVuIHdlJ3JlIHN1cmUgaXQncyBhIGhvdCB1cGRhdGUuXG4gICAgICAvLyBPdGhlcndpc2UgaXQgd291bGQgZmxpY2tlciByaWdodCBiZWZvcmUgdGhlIHJlbG9hZC5cbiAgICAgIHRyeURpc21pc3NFcnJvck92ZXJsYXkoKTtcbiAgICB9KTtcbiAgfVxufVxuXG4vLyBDb21waWxhdGlvbiB3aXRoIGVycm9ycyAoZS5nLiBzeW50YXggZXJyb3Igb3IgbWlzc2luZyBtb2R1bGVzKS5cbmZ1bmN0aW9uIGhhbmRsZUVycm9ycyhlcnJvcnMpIHtcbiAgY2xlYXJPdXRkYXRlZEVycm9ycygpO1xuXG4gIGlzRmlyc3RDb21waWxhdGlvbiA9IGZhbHNlO1xuICBoYXNDb21waWxlRXJyb3JzID0gdHJ1ZTtcblxuICAvLyBcIk1hc3NhZ2VcIiB3ZWJwYWNrIG1lc3NhZ2VzLlxuICB2YXIgZm9ybWF0dGVkID0gZm9ybWF0V2VicGFja01lc3NhZ2VzKHtcbiAgICBlcnJvcnM6IGVycm9ycyxcbiAgICB3YXJuaW5nczogW10sXG4gIH0pO1xuXG4gIC8vIE9ubHkgc2hvdyB0aGUgZmlyc3QgZXJyb3IuXG4gIEVycm9yT3ZlcmxheS5yZXBvcnRCdWlsZEVycm9yKGZvcm1hdHRlZC5lcnJvcnNbMF0pO1xuXG4gIC8vIEFsc28gbG9nIHRoZW0gdG8gdGhlIGNvbnNvbGUuXG4gIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGNvbnNvbGUuZXJyb3IgPT09ICdmdW5jdGlvbicpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZvcm1hdHRlZC5lcnJvcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGNvbnNvbGUuZXJyb3Ioc3RyaXBBbnNpKGZvcm1hdHRlZC5lcnJvcnNbaV0pKTtcbiAgICB9XG4gIH1cblxuICAvLyBEbyBub3QgYXR0ZW1wdCB0byByZWxvYWQgbm93LlxuICAvLyBXZSB3aWxsIHJlbG9hZCBvbiBuZXh0IHN1Y2Nlc3MgaW5zdGVhZC5cbn1cblxuZnVuY3Rpb24gdHJ5RGlzbWlzc0Vycm9yT3ZlcmxheSgpIHtcbiAgaWYgKCFoYXNDb21waWxlRXJyb3JzKSB7XG4gICAgRXJyb3JPdmVybGF5LmRpc21pc3NCdWlsZEVycm9yKCk7XG4gIH1cbn1cblxuLy8gVGhlcmUgaXMgYSBuZXdlciB2ZXJzaW9uIG9mIHRoZSBjb2RlIGF2YWlsYWJsZS5cbmZ1bmN0aW9uIGhhbmRsZUF2YWlsYWJsZUhhc2goaGFzaCkge1xuICAvLyBVcGRhdGUgbGFzdCBrbm93biBjb21waWxhdGlvbiBoYXNoLlxuICBtb3N0UmVjZW50Q29tcGlsYXRpb25IYXNoID0gaGFzaDtcbn1cblxuLy8gSGFuZGxlIG1lc3NhZ2VzIGZyb20gdGhlIHNlcnZlci5cbmNvbm5lY3Rpb24ub25tZXNzYWdlID0gZnVuY3Rpb24oZSkge1xuICB2YXIgbWVzc2FnZSA9IEpTT04ucGFyc2UoZS5kYXRhKTtcbiAgc3dpdGNoIChtZXNzYWdlLnR5cGUpIHtcbiAgICBjYXNlICdoYXNoJzpcbiAgICAgIGhhbmRsZUF2YWlsYWJsZUhhc2gobWVzc2FnZS5kYXRhKTtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ3N0aWxsLW9rJzpcbiAgICBjYXNlICdvayc6XG4gICAgICBoYW5kbGVTdWNjZXNzKCk7XG4gICAgICBicmVhaztcbiAgICBjYXNlICdjb250ZW50LWNoYW5nZWQnOlxuICAgICAgLy8gVHJpZ2dlcmVkIHdoZW4gYSBmaWxlIGZyb20gYGNvbnRlbnRCYXNlYCBjaGFuZ2VkLlxuICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xuICAgICAgYnJlYWs7XG4gICAgY2FzZSAnd2FybmluZ3MnOlxuICAgICAgaGFuZGxlV2FybmluZ3MobWVzc2FnZS5kYXRhKTtcbiAgICAgIGJyZWFrO1xuICAgIGNhc2UgJ2Vycm9ycyc6XG4gICAgICBoYW5kbGVFcnJvcnMobWVzc2FnZS5kYXRhKTtcbiAgICAgIGJyZWFrO1xuICAgIGRlZmF1bHQ6XG4gICAgLy8gRG8gbm90aGluZy5cbiAgfVxufTtcblxuLy8gSXMgdGhlcmUgYSBuZXdlciB2ZXJzaW9uIG9mIHRoaXMgY29kZSBhdmFpbGFibGU/XG5mdW5jdGlvbiBpc1VwZGF0ZUF2YWlsYWJsZSgpIHtcbiAgLyogZ2xvYmFscyBfX3dlYnBhY2tfaGFzaF9fICovXG4gIC8vIF9fd2VicGFja19oYXNoX18gaXMgdGhlIGhhc2ggb2YgdGhlIGN1cnJlbnQgY29tcGlsYXRpb24uXG4gIC8vIEl0J3MgYSBnbG9iYWwgdmFyaWFibGUgaW5qZWN0ZWQgYnkgV2VicGFjay5cbiAgcmV0dXJuIG1vc3RSZWNlbnRDb21waWxhdGlvbkhhc2ggIT09IF9fd2VicGFja19oYXNoX187XG59XG5cbi8vIFdlYnBhY2sgZGlzYWxsb3dzIHVwZGF0ZXMgaW4gb3RoZXIgc3RhdGVzLlxuZnVuY3Rpb24gY2FuQXBwbHlVcGRhdGVzKCkge1xuICByZXR1cm4gbW9kdWxlLmhvdC5zdGF0dXMoKSA9PT0gJ2lkbGUnO1xufVxuXG4vLyBBdHRlbXB0IHRvIHVwZGF0ZSBjb2RlIG9uIHRoZSBmbHksIGZhbGwgYmFjayB0byBhIGhhcmQgcmVsb2FkLlxuZnVuY3Rpb24gdHJ5QXBwbHlVcGRhdGVzKG9uSG90VXBkYXRlU3VjY2Vzcykge1xuICBpZiAoIW1vZHVsZS5ob3QpIHtcbiAgICAvLyBIb3RNb2R1bGVSZXBsYWNlbWVudFBsdWdpbiBpcyBub3QgaW4gV2VicGFjayBjb25maWd1cmF0aW9uLlxuICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAoIWlzVXBkYXRlQXZhaWxhYmxlKCkgfHwgIWNhbkFwcGx5VXBkYXRlcygpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgZnVuY3Rpb24gaGFuZGxlQXBwbHlVcGRhdGVzKGVyciwgdXBkYXRlZE1vZHVsZXMpIHtcbiAgICBpZiAoZXJyIHx8ICF1cGRhdGVkTW9kdWxlcyB8fCBoYWRSdW50aW1lRXJyb3IpIHtcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIG9uSG90VXBkYXRlU3VjY2VzcyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgLy8gTWF5YmUgd2Ugd2FudCB0byBkbyBzb21ldGhpbmcuXG4gICAgICBvbkhvdFVwZGF0ZVN1Y2Nlc3MoKTtcbiAgICB9XG5cbiAgICBpZiAoaXNVcGRhdGVBdmFpbGFibGUoKSkge1xuICAgICAgLy8gV2hpbGUgd2Ugd2VyZSB1cGRhdGluZywgdGhlcmUgd2FzIGEgbmV3IHVwZGF0ZSEgRG8gaXQgYWdhaW4uXG4gICAgICB0cnlBcHBseVVwZGF0ZXMoKTtcbiAgICB9XG4gIH1cblxuICAvLyBodHRwczovL3dlYnBhY2suZ2l0aHViLmlvL2RvY3MvaG90LW1vZHVsZS1yZXBsYWNlbWVudC5odG1sI2NoZWNrXG4gIHZhciByZXN1bHQgPSBtb2R1bGUuaG90LmNoZWNrKC8qIGF1dG9BcHBseSAqLyB0cnVlLCBoYW5kbGVBcHBseVVwZGF0ZXMpO1xuXG4gIC8vIC8vIFdlYnBhY2sgMiByZXR1cm5zIGEgUHJvbWlzZSBpbnN0ZWFkIG9mIGludm9raW5nIGEgY2FsbGJhY2tcbiAgaWYgKHJlc3VsdCAmJiByZXN1bHQudGhlbikge1xuICAgIHJlc3VsdC50aGVuKFxuICAgICAgZnVuY3Rpb24odXBkYXRlZE1vZHVsZXMpIHtcbiAgICAgICAgaGFuZGxlQXBwbHlVcGRhdGVzKG51bGwsIHVwZGF0ZWRNb2R1bGVzKTtcbiAgICAgIH0sXG4gICAgICBmdW5jdGlvbihlcnIpIHtcbiAgICAgICAgaGFuZGxlQXBwbHlVcGRhdGVzKGVyciwgbnVsbCk7XG4gICAgICB9XG4gICAgKTtcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7O0FBT0E7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBU0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFXQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFLQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQW5CQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-dev-utils/webpackHotDevClient.js
