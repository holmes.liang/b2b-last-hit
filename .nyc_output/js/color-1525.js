var LRU = __webpack_require__(/*! ../core/LRU */ "./node_modules/zrender/lib/core/LRU.js");

var kCSSColorTable = {
  'transparent': [0, 0, 0, 0],
  'aliceblue': [240, 248, 255, 1],
  'antiquewhite': [250, 235, 215, 1],
  'aqua': [0, 255, 255, 1],
  'aquamarine': [127, 255, 212, 1],
  'azure': [240, 255, 255, 1],
  'beige': [245, 245, 220, 1],
  'bisque': [255, 228, 196, 1],
  'black': [0, 0, 0, 1],
  'blanchedalmond': [255, 235, 205, 1],
  'blue': [0, 0, 255, 1],
  'blueviolet': [138, 43, 226, 1],
  'brown': [165, 42, 42, 1],
  'burlywood': [222, 184, 135, 1],
  'cadetblue': [95, 158, 160, 1],
  'chartreuse': [127, 255, 0, 1],
  'chocolate': [210, 105, 30, 1],
  'coral': [255, 127, 80, 1],
  'cornflowerblue': [100, 149, 237, 1],
  'cornsilk': [255, 248, 220, 1],
  'crimson': [220, 20, 60, 1],
  'cyan': [0, 255, 255, 1],
  'darkblue': [0, 0, 139, 1],
  'darkcyan': [0, 139, 139, 1],
  'darkgoldenrod': [184, 134, 11, 1],
  'darkgray': [169, 169, 169, 1],
  'darkgreen': [0, 100, 0, 1],
  'darkgrey': [169, 169, 169, 1],
  'darkkhaki': [189, 183, 107, 1],
  'darkmagenta': [139, 0, 139, 1],
  'darkolivegreen': [85, 107, 47, 1],
  'darkorange': [255, 140, 0, 1],
  'darkorchid': [153, 50, 204, 1],
  'darkred': [139, 0, 0, 1],
  'darksalmon': [233, 150, 122, 1],
  'darkseagreen': [143, 188, 143, 1],
  'darkslateblue': [72, 61, 139, 1],
  'darkslategray': [47, 79, 79, 1],
  'darkslategrey': [47, 79, 79, 1],
  'darkturquoise': [0, 206, 209, 1],
  'darkviolet': [148, 0, 211, 1],
  'deeppink': [255, 20, 147, 1],
  'deepskyblue': [0, 191, 255, 1],
  'dimgray': [105, 105, 105, 1],
  'dimgrey': [105, 105, 105, 1],
  'dodgerblue': [30, 144, 255, 1],
  'firebrick': [178, 34, 34, 1],
  'floralwhite': [255, 250, 240, 1],
  'forestgreen': [34, 139, 34, 1],
  'fuchsia': [255, 0, 255, 1],
  'gainsboro': [220, 220, 220, 1],
  'ghostwhite': [248, 248, 255, 1],
  'gold': [255, 215, 0, 1],
  'goldenrod': [218, 165, 32, 1],
  'gray': [128, 128, 128, 1],
  'green': [0, 128, 0, 1],
  'greenyellow': [173, 255, 47, 1],
  'grey': [128, 128, 128, 1],
  'honeydew': [240, 255, 240, 1],
  'hotpink': [255, 105, 180, 1],
  'indianred': [205, 92, 92, 1],
  'indigo': [75, 0, 130, 1],
  'ivory': [255, 255, 240, 1],
  'khaki': [240, 230, 140, 1],
  'lavender': [230, 230, 250, 1],
  'lavenderblush': [255, 240, 245, 1],
  'lawngreen': [124, 252, 0, 1],
  'lemonchiffon': [255, 250, 205, 1],
  'lightblue': [173, 216, 230, 1],
  'lightcoral': [240, 128, 128, 1],
  'lightcyan': [224, 255, 255, 1],
  'lightgoldenrodyellow': [250, 250, 210, 1],
  'lightgray': [211, 211, 211, 1],
  'lightgreen': [144, 238, 144, 1],
  'lightgrey': [211, 211, 211, 1],
  'lightpink': [255, 182, 193, 1],
  'lightsalmon': [255, 160, 122, 1],
  'lightseagreen': [32, 178, 170, 1],
  'lightskyblue': [135, 206, 250, 1],
  'lightslategray': [119, 136, 153, 1],
  'lightslategrey': [119, 136, 153, 1],
  'lightsteelblue': [176, 196, 222, 1],
  'lightyellow': [255, 255, 224, 1],
  'lime': [0, 255, 0, 1],
  'limegreen': [50, 205, 50, 1],
  'linen': [250, 240, 230, 1],
  'magenta': [255, 0, 255, 1],
  'maroon': [128, 0, 0, 1],
  'mediumaquamarine': [102, 205, 170, 1],
  'mediumblue': [0, 0, 205, 1],
  'mediumorchid': [186, 85, 211, 1],
  'mediumpurple': [147, 112, 219, 1],
  'mediumseagreen': [60, 179, 113, 1],
  'mediumslateblue': [123, 104, 238, 1],
  'mediumspringgreen': [0, 250, 154, 1],
  'mediumturquoise': [72, 209, 204, 1],
  'mediumvioletred': [199, 21, 133, 1],
  'midnightblue': [25, 25, 112, 1],
  'mintcream': [245, 255, 250, 1],
  'mistyrose': [255, 228, 225, 1],
  'moccasin': [255, 228, 181, 1],
  'navajowhite': [255, 222, 173, 1],
  'navy': [0, 0, 128, 1],
  'oldlace': [253, 245, 230, 1],
  'olive': [128, 128, 0, 1],
  'olivedrab': [107, 142, 35, 1],
  'orange': [255, 165, 0, 1],
  'orangered': [255, 69, 0, 1],
  'orchid': [218, 112, 214, 1],
  'palegoldenrod': [238, 232, 170, 1],
  'palegreen': [152, 251, 152, 1],
  'paleturquoise': [175, 238, 238, 1],
  'palevioletred': [219, 112, 147, 1],
  'papayawhip': [255, 239, 213, 1],
  'peachpuff': [255, 218, 185, 1],
  'peru': [205, 133, 63, 1],
  'pink': [255, 192, 203, 1],
  'plum': [221, 160, 221, 1],
  'powderblue': [176, 224, 230, 1],
  'purple': [128, 0, 128, 1],
  'red': [255, 0, 0, 1],
  'rosybrown': [188, 143, 143, 1],
  'royalblue': [65, 105, 225, 1],
  'saddlebrown': [139, 69, 19, 1],
  'salmon': [250, 128, 114, 1],
  'sandybrown': [244, 164, 96, 1],
  'seagreen': [46, 139, 87, 1],
  'seashell': [255, 245, 238, 1],
  'sienna': [160, 82, 45, 1],
  'silver': [192, 192, 192, 1],
  'skyblue': [135, 206, 235, 1],
  'slateblue': [106, 90, 205, 1],
  'slategray': [112, 128, 144, 1],
  'slategrey': [112, 128, 144, 1],
  'snow': [255, 250, 250, 1],
  'springgreen': [0, 255, 127, 1],
  'steelblue': [70, 130, 180, 1],
  'tan': [210, 180, 140, 1],
  'teal': [0, 128, 128, 1],
  'thistle': [216, 191, 216, 1],
  'tomato': [255, 99, 71, 1],
  'turquoise': [64, 224, 208, 1],
  'violet': [238, 130, 238, 1],
  'wheat': [245, 222, 179, 1],
  'white': [255, 255, 255, 1],
  'whitesmoke': [245, 245, 245, 1],
  'yellow': [255, 255, 0, 1],
  'yellowgreen': [154, 205, 50, 1]
};

function clampCssByte(i) {
  // Clamp to integer 0 .. 255.
  i = Math.round(i); // Seems to be what Chrome does (vs truncation).

  return i < 0 ? 0 : i > 255 ? 255 : i;
}

function clampCssAngle(i) {
  // Clamp to integer 0 .. 360.
  i = Math.round(i); // Seems to be what Chrome does (vs truncation).

  return i < 0 ? 0 : i > 360 ? 360 : i;
}

function clampCssFloat(f) {
  // Clamp to float 0.0 .. 1.0.
  return f < 0 ? 0 : f > 1 ? 1 : f;
}

function parseCssInt(str) {
  // int or percentage.
  if (str.length && str.charAt(str.length - 1) === '%') {
    return clampCssByte(parseFloat(str) / 100 * 255);
  }

  return clampCssByte(parseInt(str, 10));
}

function parseCssFloat(str) {
  // float or percentage.
  if (str.length && str.charAt(str.length - 1) === '%') {
    return clampCssFloat(parseFloat(str) / 100);
  }

  return clampCssFloat(parseFloat(str));
}

function cssHueToRgb(m1, m2, h) {
  if (h < 0) {
    h += 1;
  } else if (h > 1) {
    h -= 1;
  }

  if (h * 6 < 1) {
    return m1 + (m2 - m1) * h * 6;
  }

  if (h * 2 < 1) {
    return m2;
  }

  if (h * 3 < 2) {
    return m1 + (m2 - m1) * (2 / 3 - h) * 6;
  }

  return m1;
}

function lerpNumber(a, b, p) {
  return a + (b - a) * p;
}

function setRgba(out, r, g, b, a) {
  out[0] = r;
  out[1] = g;
  out[2] = b;
  out[3] = a;
  return out;
}

function copyRgba(out, a) {
  out[0] = a[0];
  out[1] = a[1];
  out[2] = a[2];
  out[3] = a[3];
  return out;
}

var colorCache = new LRU(20);
var lastRemovedArr = null;

function putToCache(colorStr, rgbaArr) {
  // Reuse removed array
  if (lastRemovedArr) {
    copyRgba(lastRemovedArr, rgbaArr);
  }

  lastRemovedArr = colorCache.put(colorStr, lastRemovedArr || rgbaArr.slice());
}
/**
 * @param {string} colorStr
 * @param {Array.<number>} out
 * @return {Array.<number>}
 * @memberOf module:zrender/util/color
 */


function parse(colorStr, rgbaArr) {
  if (!colorStr) {
    return;
  }

  rgbaArr = rgbaArr || [];
  var cached = colorCache.get(colorStr);

  if (cached) {
    return copyRgba(rgbaArr, cached);
  } // colorStr may be not string


  colorStr = colorStr + ''; // Remove all whitespace, not compliant, but should just be more accepting.

  var str = colorStr.replace(/ /g, '').toLowerCase(); // Color keywords (and transparent) lookup.

  if (str in kCSSColorTable) {
    copyRgba(rgbaArr, kCSSColorTable[str]);
    putToCache(colorStr, rgbaArr);
    return rgbaArr;
  } // #abc and #abc123 syntax.


  if (str.charAt(0) === '#') {
    if (str.length === 4) {
      var iv = parseInt(str.substr(1), 16); // TODO(deanm): Stricter parsing.

      if (!(iv >= 0 && iv <= 0xfff)) {
        setRgba(rgbaArr, 0, 0, 0, 1);
        return; // Covers NaN.
      }

      setRgba(rgbaArr, (iv & 0xf00) >> 4 | (iv & 0xf00) >> 8, iv & 0xf0 | (iv & 0xf0) >> 4, iv & 0xf | (iv & 0xf) << 4, 1);
      putToCache(colorStr, rgbaArr);
      return rgbaArr;
    } else if (str.length === 7) {
      var iv = parseInt(str.substr(1), 16); // TODO(deanm): Stricter parsing.

      if (!(iv >= 0 && iv <= 0xffffff)) {
        setRgba(rgbaArr, 0, 0, 0, 1);
        return; // Covers NaN.
      }

      setRgba(rgbaArr, (iv & 0xff0000) >> 16, (iv & 0xff00) >> 8, iv & 0xff, 1);
      putToCache(colorStr, rgbaArr);
      return rgbaArr;
    }

    return;
  }

  var op = str.indexOf('(');
  var ep = str.indexOf(')');

  if (op !== -1 && ep + 1 === str.length) {
    var fname = str.substr(0, op);
    var params = str.substr(op + 1, ep - (op + 1)).split(',');
    var alpha = 1; // To allow case fallthrough.

    switch (fname) {
      case 'rgba':
        if (params.length !== 4) {
          setRgba(rgbaArr, 0, 0, 0, 1);
          return;
        }

        alpha = parseCssFloat(params.pop());
      // jshint ignore:line
      // Fall through.

      case 'rgb':
        if (params.length !== 3) {
          setRgba(rgbaArr, 0, 0, 0, 1);
          return;
        }

        setRgba(rgbaArr, parseCssInt(params[0]), parseCssInt(params[1]), parseCssInt(params[2]), alpha);
        putToCache(colorStr, rgbaArr);
        return rgbaArr;

      case 'hsla':
        if (params.length !== 4) {
          setRgba(rgbaArr, 0, 0, 0, 1);
          return;
        }

        params[3] = parseCssFloat(params[3]);
        hsla2rgba(params, rgbaArr);
        putToCache(colorStr, rgbaArr);
        return rgbaArr;

      case 'hsl':
        if (params.length !== 3) {
          setRgba(rgbaArr, 0, 0, 0, 1);
          return;
        }

        hsla2rgba(params, rgbaArr);
        putToCache(colorStr, rgbaArr);
        return rgbaArr;

      default:
        return;
    }
  }

  setRgba(rgbaArr, 0, 0, 0, 1);
  return;
}
/**
 * @param {Array.<number>} hsla
 * @param {Array.<number>} rgba
 * @return {Array.<number>} rgba
 */


function hsla2rgba(hsla, rgba) {
  var h = (parseFloat(hsla[0]) % 360 + 360) % 360 / 360; // 0 .. 1
  // NOTE(deanm): According to the CSS spec s/l should only be
  // percentages, but we don't bother and let float or percentage.

  var s = parseCssFloat(hsla[1]);
  var l = parseCssFloat(hsla[2]);
  var m2 = l <= 0.5 ? l * (s + 1) : l + s - l * s;
  var m1 = l * 2 - m2;
  rgba = rgba || [];
  setRgba(rgba, clampCssByte(cssHueToRgb(m1, m2, h + 1 / 3) * 255), clampCssByte(cssHueToRgb(m1, m2, h) * 255), clampCssByte(cssHueToRgb(m1, m2, h - 1 / 3) * 255), 1);

  if (hsla.length === 4) {
    rgba[3] = hsla[3];
  }

  return rgba;
}
/**
 * @param {Array.<number>} rgba
 * @return {Array.<number>} hsla
 */


function rgba2hsla(rgba) {
  if (!rgba) {
    return;
  } // RGB from 0 to 255


  var R = rgba[0] / 255;
  var G = rgba[1] / 255;
  var B = rgba[2] / 255;
  var vMin = Math.min(R, G, B); // Min. value of RGB

  var vMax = Math.max(R, G, B); // Max. value of RGB

  var delta = vMax - vMin; // Delta RGB value

  var L = (vMax + vMin) / 2;
  var H;
  var S; // HSL results from 0 to 1

  if (delta === 0) {
    H = 0;
    S = 0;
  } else {
    if (L < 0.5) {
      S = delta / (vMax + vMin);
    } else {
      S = delta / (2 - vMax - vMin);
    }

    var deltaR = ((vMax - R) / 6 + delta / 2) / delta;
    var deltaG = ((vMax - G) / 6 + delta / 2) / delta;
    var deltaB = ((vMax - B) / 6 + delta / 2) / delta;

    if (R === vMax) {
      H = deltaB - deltaG;
    } else if (G === vMax) {
      H = 1 / 3 + deltaR - deltaB;
    } else if (B === vMax) {
      H = 2 / 3 + deltaG - deltaR;
    }

    if (H < 0) {
      H += 1;
    }

    if (H > 1) {
      H -= 1;
    }
  }

  var hsla = [H * 360, S, L];

  if (rgba[3] != null) {
    hsla.push(rgba[3]);
  }

  return hsla;
}
/**
 * @param {string} color
 * @param {number} level
 * @return {string}
 * @memberOf module:zrender/util/color
 */


function lift(color, level) {
  var colorArr = parse(color);

  if (colorArr) {
    for (var i = 0; i < 3; i++) {
      if (level < 0) {
        colorArr[i] = colorArr[i] * (1 - level) | 0;
      } else {
        colorArr[i] = (255 - colorArr[i]) * level + colorArr[i] | 0;
      }

      if (colorArr[i] > 255) {
        colorArr[i] = 255;
      } else if (color[i] < 0) {
        colorArr[i] = 0;
      }
    }

    return stringify(colorArr, colorArr.length === 4 ? 'rgba' : 'rgb');
  }
}
/**
 * @param {string} color
 * @return {string}
 * @memberOf module:zrender/util/color
 */


function toHex(color) {
  var colorArr = parse(color);

  if (colorArr) {
    return ((1 << 24) + (colorArr[0] << 16) + (colorArr[1] << 8) + +colorArr[2]).toString(16).slice(1);
  }
}
/**
 * Map value to color. Faster than lerp methods because color is represented by rgba array.
 * @param {number} normalizedValue A float between 0 and 1.
 * @param {Array.<Array.<number>>} colors List of rgba color array
 * @param {Array.<number>} [out] Mapped gba color array
 * @return {Array.<number>} will be null/undefined if input illegal.
 */


function fastLerp(normalizedValue, colors, out) {
  if (!(colors && colors.length) || !(normalizedValue >= 0 && normalizedValue <= 1)) {
    return;
  }

  out = out || [];
  var value = normalizedValue * (colors.length - 1);
  var leftIndex = Math.floor(value);
  var rightIndex = Math.ceil(value);
  var leftColor = colors[leftIndex];
  var rightColor = colors[rightIndex];
  var dv = value - leftIndex;
  out[0] = clampCssByte(lerpNumber(leftColor[0], rightColor[0], dv));
  out[1] = clampCssByte(lerpNumber(leftColor[1], rightColor[1], dv));
  out[2] = clampCssByte(lerpNumber(leftColor[2], rightColor[2], dv));
  out[3] = clampCssFloat(lerpNumber(leftColor[3], rightColor[3], dv));
  return out;
}
/**
 * @deprecated
 */


var fastMapToColor = fastLerp;
/**
 * @param {number} normalizedValue A float between 0 and 1.
 * @param {Array.<string>} colors Color list.
 * @param {boolean=} fullOutput Default false.
 * @return {(string|Object)} Result color. If fullOutput,
 *                           return {color: ..., leftIndex: ..., rightIndex: ..., value: ...},
 * @memberOf module:zrender/util/color
 */

function lerp(normalizedValue, colors, fullOutput) {
  if (!(colors && colors.length) || !(normalizedValue >= 0 && normalizedValue <= 1)) {
    return;
  }

  var value = normalizedValue * (colors.length - 1);
  var leftIndex = Math.floor(value);
  var rightIndex = Math.ceil(value);
  var leftColor = parse(colors[leftIndex]);
  var rightColor = parse(colors[rightIndex]);
  var dv = value - leftIndex;
  var color = stringify([clampCssByte(lerpNumber(leftColor[0], rightColor[0], dv)), clampCssByte(lerpNumber(leftColor[1], rightColor[1], dv)), clampCssByte(lerpNumber(leftColor[2], rightColor[2], dv)), clampCssFloat(lerpNumber(leftColor[3], rightColor[3], dv))], 'rgba');
  return fullOutput ? {
    color: color,
    leftIndex: leftIndex,
    rightIndex: rightIndex,
    value: value
  } : color;
}
/**
 * @deprecated
 */


var mapToColor = lerp;
/**
 * @param {string} color
 * @param {number=} h 0 ~ 360, ignore when null.
 * @param {number=} s 0 ~ 1, ignore when null.
 * @param {number=} l 0 ~ 1, ignore when null.
 * @return {string} Color string in rgba format.
 * @memberOf module:zrender/util/color
 */

function modifyHSL(color, h, s, l) {
  color = parse(color);

  if (color) {
    color = rgba2hsla(color);
    h != null && (color[0] = clampCssAngle(h));
    s != null && (color[1] = parseCssFloat(s));
    l != null && (color[2] = parseCssFloat(l));
    return stringify(hsla2rgba(color), 'rgba');
  }
}
/**
 * @param {string} color
 * @param {number=} alpha 0 ~ 1
 * @return {string} Color string in rgba format.
 * @memberOf module:zrender/util/color
 */


function modifyAlpha(color, alpha) {
  color = parse(color);

  if (color && alpha != null) {
    color[3] = clampCssFloat(alpha);
    return stringify(color, 'rgba');
  }
}
/**
 * @param {Array.<number>} arrColor like [12,33,44,0.4]
 * @param {string} type 'rgba', 'hsva', ...
 * @return {string} Result color. (If input illegal, return undefined).
 */


function stringify(arrColor, type) {
  if (!arrColor || !arrColor.length) {
    return;
  }

  var colorStr = arrColor[0] + ',' + arrColor[1] + ',' + arrColor[2];

  if (type === 'rgba' || type === 'hsva' || type === 'hsla') {
    colorStr += ',' + arrColor[3];
  }

  return type + '(' + colorStr + ')';
}

exports.parse = parse;
exports.lift = lift;
exports.toHex = toHex;
exports.fastLerp = fastLerp;
exports.fastMapToColor = fastMapToColor;
exports.lerp = lerp;
exports.mapToColor = mapToColor;
exports.modifyHSL = modifyHSL;
exports.modifyAlpha = modifyAlpha;
exports.stringify = stringify;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC9jb2xvci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL3Rvb2wvY29sb3IuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIExSVSA9IHJlcXVpcmUoXCIuLi9jb3JlL0xSVVwiKTtcblxudmFyIGtDU1NDb2xvclRhYmxlID0ge1xuICAndHJhbnNwYXJlbnQnOiBbMCwgMCwgMCwgMF0sXG4gICdhbGljZWJsdWUnOiBbMjQwLCAyNDgsIDI1NSwgMV0sXG4gICdhbnRpcXVld2hpdGUnOiBbMjUwLCAyMzUsIDIxNSwgMV0sXG4gICdhcXVhJzogWzAsIDI1NSwgMjU1LCAxXSxcbiAgJ2FxdWFtYXJpbmUnOiBbMTI3LCAyNTUsIDIxMiwgMV0sXG4gICdhenVyZSc6IFsyNDAsIDI1NSwgMjU1LCAxXSxcbiAgJ2JlaWdlJzogWzI0NSwgMjQ1LCAyMjAsIDFdLFxuICAnYmlzcXVlJzogWzI1NSwgMjI4LCAxOTYsIDFdLFxuICAnYmxhY2snOiBbMCwgMCwgMCwgMV0sXG4gICdibGFuY2hlZGFsbW9uZCc6IFsyNTUsIDIzNSwgMjA1LCAxXSxcbiAgJ2JsdWUnOiBbMCwgMCwgMjU1LCAxXSxcbiAgJ2JsdWV2aW9sZXQnOiBbMTM4LCA0MywgMjI2LCAxXSxcbiAgJ2Jyb3duJzogWzE2NSwgNDIsIDQyLCAxXSxcbiAgJ2J1cmx5d29vZCc6IFsyMjIsIDE4NCwgMTM1LCAxXSxcbiAgJ2NhZGV0Ymx1ZSc6IFs5NSwgMTU4LCAxNjAsIDFdLFxuICAnY2hhcnRyZXVzZSc6IFsxMjcsIDI1NSwgMCwgMV0sXG4gICdjaG9jb2xhdGUnOiBbMjEwLCAxMDUsIDMwLCAxXSxcbiAgJ2NvcmFsJzogWzI1NSwgMTI3LCA4MCwgMV0sXG4gICdjb3JuZmxvd2VyYmx1ZSc6IFsxMDAsIDE0OSwgMjM3LCAxXSxcbiAgJ2Nvcm5zaWxrJzogWzI1NSwgMjQ4LCAyMjAsIDFdLFxuICAnY3JpbXNvbic6IFsyMjAsIDIwLCA2MCwgMV0sXG4gICdjeWFuJzogWzAsIDI1NSwgMjU1LCAxXSxcbiAgJ2RhcmtibHVlJzogWzAsIDAsIDEzOSwgMV0sXG4gICdkYXJrY3lhbic6IFswLCAxMzksIDEzOSwgMV0sXG4gICdkYXJrZ29sZGVucm9kJzogWzE4NCwgMTM0LCAxMSwgMV0sXG4gICdkYXJrZ3JheSc6IFsxNjksIDE2OSwgMTY5LCAxXSxcbiAgJ2RhcmtncmVlbic6IFswLCAxMDAsIDAsIDFdLFxuICAnZGFya2dyZXknOiBbMTY5LCAxNjksIDE2OSwgMV0sXG4gICdkYXJra2hha2knOiBbMTg5LCAxODMsIDEwNywgMV0sXG4gICdkYXJrbWFnZW50YSc6IFsxMzksIDAsIDEzOSwgMV0sXG4gICdkYXJrb2xpdmVncmVlbic6IFs4NSwgMTA3LCA0NywgMV0sXG4gICdkYXJrb3JhbmdlJzogWzI1NSwgMTQwLCAwLCAxXSxcbiAgJ2RhcmtvcmNoaWQnOiBbMTUzLCA1MCwgMjA0LCAxXSxcbiAgJ2RhcmtyZWQnOiBbMTM5LCAwLCAwLCAxXSxcbiAgJ2RhcmtzYWxtb24nOiBbMjMzLCAxNTAsIDEyMiwgMV0sXG4gICdkYXJrc2VhZ3JlZW4nOiBbMTQzLCAxODgsIDE0MywgMV0sXG4gICdkYXJrc2xhdGVibHVlJzogWzcyLCA2MSwgMTM5LCAxXSxcbiAgJ2RhcmtzbGF0ZWdyYXknOiBbNDcsIDc5LCA3OSwgMV0sXG4gICdkYXJrc2xhdGVncmV5JzogWzQ3LCA3OSwgNzksIDFdLFxuICAnZGFya3R1cnF1b2lzZSc6IFswLCAyMDYsIDIwOSwgMV0sXG4gICdkYXJrdmlvbGV0JzogWzE0OCwgMCwgMjExLCAxXSxcbiAgJ2RlZXBwaW5rJzogWzI1NSwgMjAsIDE0NywgMV0sXG4gICdkZWVwc2t5Ymx1ZSc6IFswLCAxOTEsIDI1NSwgMV0sXG4gICdkaW1ncmF5JzogWzEwNSwgMTA1LCAxMDUsIDFdLFxuICAnZGltZ3JleSc6IFsxMDUsIDEwNSwgMTA1LCAxXSxcbiAgJ2RvZGdlcmJsdWUnOiBbMzAsIDE0NCwgMjU1LCAxXSxcbiAgJ2ZpcmVicmljayc6IFsxNzgsIDM0LCAzNCwgMV0sXG4gICdmbG9yYWx3aGl0ZSc6IFsyNTUsIDI1MCwgMjQwLCAxXSxcbiAgJ2ZvcmVzdGdyZWVuJzogWzM0LCAxMzksIDM0LCAxXSxcbiAgJ2Z1Y2hzaWEnOiBbMjU1LCAwLCAyNTUsIDFdLFxuICAnZ2FpbnNib3JvJzogWzIyMCwgMjIwLCAyMjAsIDFdLFxuICAnZ2hvc3R3aGl0ZSc6IFsyNDgsIDI0OCwgMjU1LCAxXSxcbiAgJ2dvbGQnOiBbMjU1LCAyMTUsIDAsIDFdLFxuICAnZ29sZGVucm9kJzogWzIxOCwgMTY1LCAzMiwgMV0sXG4gICdncmF5JzogWzEyOCwgMTI4LCAxMjgsIDFdLFxuICAnZ3JlZW4nOiBbMCwgMTI4LCAwLCAxXSxcbiAgJ2dyZWVueWVsbG93JzogWzE3MywgMjU1LCA0NywgMV0sXG4gICdncmV5JzogWzEyOCwgMTI4LCAxMjgsIDFdLFxuICAnaG9uZXlkZXcnOiBbMjQwLCAyNTUsIDI0MCwgMV0sXG4gICdob3RwaW5rJzogWzI1NSwgMTA1LCAxODAsIDFdLFxuICAnaW5kaWFucmVkJzogWzIwNSwgOTIsIDkyLCAxXSxcbiAgJ2luZGlnbyc6IFs3NSwgMCwgMTMwLCAxXSxcbiAgJ2l2b3J5JzogWzI1NSwgMjU1LCAyNDAsIDFdLFxuICAna2hha2knOiBbMjQwLCAyMzAsIDE0MCwgMV0sXG4gICdsYXZlbmRlcic6IFsyMzAsIDIzMCwgMjUwLCAxXSxcbiAgJ2xhdmVuZGVyYmx1c2gnOiBbMjU1LCAyNDAsIDI0NSwgMV0sXG4gICdsYXduZ3JlZW4nOiBbMTI0LCAyNTIsIDAsIDFdLFxuICAnbGVtb25jaGlmZm9uJzogWzI1NSwgMjUwLCAyMDUsIDFdLFxuICAnbGlnaHRibHVlJzogWzE3MywgMjE2LCAyMzAsIDFdLFxuICAnbGlnaHRjb3JhbCc6IFsyNDAsIDEyOCwgMTI4LCAxXSxcbiAgJ2xpZ2h0Y3lhbic6IFsyMjQsIDI1NSwgMjU1LCAxXSxcbiAgJ2xpZ2h0Z29sZGVucm9keWVsbG93JzogWzI1MCwgMjUwLCAyMTAsIDFdLFxuICAnbGlnaHRncmF5JzogWzIxMSwgMjExLCAyMTEsIDFdLFxuICAnbGlnaHRncmVlbic6IFsxNDQsIDIzOCwgMTQ0LCAxXSxcbiAgJ2xpZ2h0Z3JleSc6IFsyMTEsIDIxMSwgMjExLCAxXSxcbiAgJ2xpZ2h0cGluayc6IFsyNTUsIDE4MiwgMTkzLCAxXSxcbiAgJ2xpZ2h0c2FsbW9uJzogWzI1NSwgMTYwLCAxMjIsIDFdLFxuICAnbGlnaHRzZWFncmVlbic6IFszMiwgMTc4LCAxNzAsIDFdLFxuICAnbGlnaHRza3libHVlJzogWzEzNSwgMjA2LCAyNTAsIDFdLFxuICAnbGlnaHRzbGF0ZWdyYXknOiBbMTE5LCAxMzYsIDE1MywgMV0sXG4gICdsaWdodHNsYXRlZ3JleSc6IFsxMTksIDEzNiwgMTUzLCAxXSxcbiAgJ2xpZ2h0c3RlZWxibHVlJzogWzE3NiwgMTk2LCAyMjIsIDFdLFxuICAnbGlnaHR5ZWxsb3cnOiBbMjU1LCAyNTUsIDIyNCwgMV0sXG4gICdsaW1lJzogWzAsIDI1NSwgMCwgMV0sXG4gICdsaW1lZ3JlZW4nOiBbNTAsIDIwNSwgNTAsIDFdLFxuICAnbGluZW4nOiBbMjUwLCAyNDAsIDIzMCwgMV0sXG4gICdtYWdlbnRhJzogWzI1NSwgMCwgMjU1LCAxXSxcbiAgJ21hcm9vbic6IFsxMjgsIDAsIDAsIDFdLFxuICAnbWVkaXVtYXF1YW1hcmluZSc6IFsxMDIsIDIwNSwgMTcwLCAxXSxcbiAgJ21lZGl1bWJsdWUnOiBbMCwgMCwgMjA1LCAxXSxcbiAgJ21lZGl1bW9yY2hpZCc6IFsxODYsIDg1LCAyMTEsIDFdLFxuICAnbWVkaXVtcHVycGxlJzogWzE0NywgMTEyLCAyMTksIDFdLFxuICAnbWVkaXVtc2VhZ3JlZW4nOiBbNjAsIDE3OSwgMTEzLCAxXSxcbiAgJ21lZGl1bXNsYXRlYmx1ZSc6IFsxMjMsIDEwNCwgMjM4LCAxXSxcbiAgJ21lZGl1bXNwcmluZ2dyZWVuJzogWzAsIDI1MCwgMTU0LCAxXSxcbiAgJ21lZGl1bXR1cnF1b2lzZSc6IFs3MiwgMjA5LCAyMDQsIDFdLFxuICAnbWVkaXVtdmlvbGV0cmVkJzogWzE5OSwgMjEsIDEzMywgMV0sXG4gICdtaWRuaWdodGJsdWUnOiBbMjUsIDI1LCAxMTIsIDFdLFxuICAnbWludGNyZWFtJzogWzI0NSwgMjU1LCAyNTAsIDFdLFxuICAnbWlzdHlyb3NlJzogWzI1NSwgMjI4LCAyMjUsIDFdLFxuICAnbW9jY2FzaW4nOiBbMjU1LCAyMjgsIDE4MSwgMV0sXG4gICduYXZham93aGl0ZSc6IFsyNTUsIDIyMiwgMTczLCAxXSxcbiAgJ25hdnknOiBbMCwgMCwgMTI4LCAxXSxcbiAgJ29sZGxhY2UnOiBbMjUzLCAyNDUsIDIzMCwgMV0sXG4gICdvbGl2ZSc6IFsxMjgsIDEyOCwgMCwgMV0sXG4gICdvbGl2ZWRyYWInOiBbMTA3LCAxNDIsIDM1LCAxXSxcbiAgJ29yYW5nZSc6IFsyNTUsIDE2NSwgMCwgMV0sXG4gICdvcmFuZ2VyZWQnOiBbMjU1LCA2OSwgMCwgMV0sXG4gICdvcmNoaWQnOiBbMjE4LCAxMTIsIDIxNCwgMV0sXG4gICdwYWxlZ29sZGVucm9kJzogWzIzOCwgMjMyLCAxNzAsIDFdLFxuICAncGFsZWdyZWVuJzogWzE1MiwgMjUxLCAxNTIsIDFdLFxuICAncGFsZXR1cnF1b2lzZSc6IFsxNzUsIDIzOCwgMjM4LCAxXSxcbiAgJ3BhbGV2aW9sZXRyZWQnOiBbMjE5LCAxMTIsIDE0NywgMV0sXG4gICdwYXBheWF3aGlwJzogWzI1NSwgMjM5LCAyMTMsIDFdLFxuICAncGVhY2hwdWZmJzogWzI1NSwgMjE4LCAxODUsIDFdLFxuICAncGVydSc6IFsyMDUsIDEzMywgNjMsIDFdLFxuICAncGluayc6IFsyNTUsIDE5MiwgMjAzLCAxXSxcbiAgJ3BsdW0nOiBbMjIxLCAxNjAsIDIyMSwgMV0sXG4gICdwb3dkZXJibHVlJzogWzE3NiwgMjI0LCAyMzAsIDFdLFxuICAncHVycGxlJzogWzEyOCwgMCwgMTI4LCAxXSxcbiAgJ3JlZCc6IFsyNTUsIDAsIDAsIDFdLFxuICAncm9zeWJyb3duJzogWzE4OCwgMTQzLCAxNDMsIDFdLFxuICAncm95YWxibHVlJzogWzY1LCAxMDUsIDIyNSwgMV0sXG4gICdzYWRkbGVicm93bic6IFsxMzksIDY5LCAxOSwgMV0sXG4gICdzYWxtb24nOiBbMjUwLCAxMjgsIDExNCwgMV0sXG4gICdzYW5keWJyb3duJzogWzI0NCwgMTY0LCA5NiwgMV0sXG4gICdzZWFncmVlbic6IFs0NiwgMTM5LCA4NywgMV0sXG4gICdzZWFzaGVsbCc6IFsyNTUsIDI0NSwgMjM4LCAxXSxcbiAgJ3NpZW5uYSc6IFsxNjAsIDgyLCA0NSwgMV0sXG4gICdzaWx2ZXInOiBbMTkyLCAxOTIsIDE5MiwgMV0sXG4gICdza3libHVlJzogWzEzNSwgMjA2LCAyMzUsIDFdLFxuICAnc2xhdGVibHVlJzogWzEwNiwgOTAsIDIwNSwgMV0sXG4gICdzbGF0ZWdyYXknOiBbMTEyLCAxMjgsIDE0NCwgMV0sXG4gICdzbGF0ZWdyZXknOiBbMTEyLCAxMjgsIDE0NCwgMV0sXG4gICdzbm93JzogWzI1NSwgMjUwLCAyNTAsIDFdLFxuICAnc3ByaW5nZ3JlZW4nOiBbMCwgMjU1LCAxMjcsIDFdLFxuICAnc3RlZWxibHVlJzogWzcwLCAxMzAsIDE4MCwgMV0sXG4gICd0YW4nOiBbMjEwLCAxODAsIDE0MCwgMV0sXG4gICd0ZWFsJzogWzAsIDEyOCwgMTI4LCAxXSxcbiAgJ3RoaXN0bGUnOiBbMjE2LCAxOTEsIDIxNiwgMV0sXG4gICd0b21hdG8nOiBbMjU1LCA5OSwgNzEsIDFdLFxuICAndHVycXVvaXNlJzogWzY0LCAyMjQsIDIwOCwgMV0sXG4gICd2aW9sZXQnOiBbMjM4LCAxMzAsIDIzOCwgMV0sXG4gICd3aGVhdCc6IFsyNDUsIDIyMiwgMTc5LCAxXSxcbiAgJ3doaXRlJzogWzI1NSwgMjU1LCAyNTUsIDFdLFxuICAnd2hpdGVzbW9rZSc6IFsyNDUsIDI0NSwgMjQ1LCAxXSxcbiAgJ3llbGxvdyc6IFsyNTUsIDI1NSwgMCwgMV0sXG4gICd5ZWxsb3dncmVlbic6IFsxNTQsIDIwNSwgNTAsIDFdXG59O1xuXG5mdW5jdGlvbiBjbGFtcENzc0J5dGUoaSkge1xuICAvLyBDbGFtcCB0byBpbnRlZ2VyIDAgLi4gMjU1LlxuICBpID0gTWF0aC5yb3VuZChpKTsgLy8gU2VlbXMgdG8gYmUgd2hhdCBDaHJvbWUgZG9lcyAodnMgdHJ1bmNhdGlvbikuXG5cbiAgcmV0dXJuIGkgPCAwID8gMCA6IGkgPiAyNTUgPyAyNTUgOiBpO1xufVxuXG5mdW5jdGlvbiBjbGFtcENzc0FuZ2xlKGkpIHtcbiAgLy8gQ2xhbXAgdG8gaW50ZWdlciAwIC4uIDM2MC5cbiAgaSA9IE1hdGgucm91bmQoaSk7IC8vIFNlZW1zIHRvIGJlIHdoYXQgQ2hyb21lIGRvZXMgKHZzIHRydW5jYXRpb24pLlxuXG4gIHJldHVybiBpIDwgMCA/IDAgOiBpID4gMzYwID8gMzYwIDogaTtcbn1cblxuZnVuY3Rpb24gY2xhbXBDc3NGbG9hdChmKSB7XG4gIC8vIENsYW1wIHRvIGZsb2F0IDAuMCAuLiAxLjAuXG4gIHJldHVybiBmIDwgMCA/IDAgOiBmID4gMSA/IDEgOiBmO1xufVxuXG5mdW5jdGlvbiBwYXJzZUNzc0ludChzdHIpIHtcbiAgLy8gaW50IG9yIHBlcmNlbnRhZ2UuXG4gIGlmIChzdHIubGVuZ3RoICYmIHN0ci5jaGFyQXQoc3RyLmxlbmd0aCAtIDEpID09PSAnJScpIHtcbiAgICByZXR1cm4gY2xhbXBDc3NCeXRlKHBhcnNlRmxvYXQoc3RyKSAvIDEwMCAqIDI1NSk7XG4gIH1cblxuICByZXR1cm4gY2xhbXBDc3NCeXRlKHBhcnNlSW50KHN0ciwgMTApKTtcbn1cblxuZnVuY3Rpb24gcGFyc2VDc3NGbG9hdChzdHIpIHtcbiAgLy8gZmxvYXQgb3IgcGVyY2VudGFnZS5cbiAgaWYgKHN0ci5sZW5ndGggJiYgc3RyLmNoYXJBdChzdHIubGVuZ3RoIC0gMSkgPT09ICclJykge1xuICAgIHJldHVybiBjbGFtcENzc0Zsb2F0KHBhcnNlRmxvYXQoc3RyKSAvIDEwMCk7XG4gIH1cblxuICByZXR1cm4gY2xhbXBDc3NGbG9hdChwYXJzZUZsb2F0KHN0cikpO1xufVxuXG5mdW5jdGlvbiBjc3NIdWVUb1JnYihtMSwgbTIsIGgpIHtcbiAgaWYgKGggPCAwKSB7XG4gICAgaCArPSAxO1xuICB9IGVsc2UgaWYgKGggPiAxKSB7XG4gICAgaCAtPSAxO1xuICB9XG5cbiAgaWYgKGggKiA2IDwgMSkge1xuICAgIHJldHVybiBtMSArIChtMiAtIG0xKSAqIGggKiA2O1xuICB9XG5cbiAgaWYgKGggKiAyIDwgMSkge1xuICAgIHJldHVybiBtMjtcbiAgfVxuXG4gIGlmIChoICogMyA8IDIpIHtcbiAgICByZXR1cm4gbTEgKyAobTIgLSBtMSkgKiAoMiAvIDMgLSBoKSAqIDY7XG4gIH1cblxuICByZXR1cm4gbTE7XG59XG5cbmZ1bmN0aW9uIGxlcnBOdW1iZXIoYSwgYiwgcCkge1xuICByZXR1cm4gYSArIChiIC0gYSkgKiBwO1xufVxuXG5mdW5jdGlvbiBzZXRSZ2JhKG91dCwgciwgZywgYiwgYSkge1xuICBvdXRbMF0gPSByO1xuICBvdXRbMV0gPSBnO1xuICBvdXRbMl0gPSBiO1xuICBvdXRbM10gPSBhO1xuICByZXR1cm4gb3V0O1xufVxuXG5mdW5jdGlvbiBjb3B5UmdiYShvdXQsIGEpIHtcbiAgb3V0WzBdID0gYVswXTtcbiAgb3V0WzFdID0gYVsxXTtcbiAgb3V0WzJdID0gYVsyXTtcbiAgb3V0WzNdID0gYVszXTtcbiAgcmV0dXJuIG91dDtcbn1cblxudmFyIGNvbG9yQ2FjaGUgPSBuZXcgTFJVKDIwKTtcbnZhciBsYXN0UmVtb3ZlZEFyciA9IG51bGw7XG5cbmZ1bmN0aW9uIHB1dFRvQ2FjaGUoY29sb3JTdHIsIHJnYmFBcnIpIHtcbiAgLy8gUmV1c2UgcmVtb3ZlZCBhcnJheVxuICBpZiAobGFzdFJlbW92ZWRBcnIpIHtcbiAgICBjb3B5UmdiYShsYXN0UmVtb3ZlZEFyciwgcmdiYUFycik7XG4gIH1cblxuICBsYXN0UmVtb3ZlZEFyciA9IGNvbG9yQ2FjaGUucHV0KGNvbG9yU3RyLCBsYXN0UmVtb3ZlZEFyciB8fCByZ2JhQXJyLnNsaWNlKCkpO1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gY29sb3JTdHJcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG91dFxuICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvdXRpbC9jb2xvclxuICovXG5cblxuZnVuY3Rpb24gcGFyc2UoY29sb3JTdHIsIHJnYmFBcnIpIHtcbiAgaWYgKCFjb2xvclN0cikge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHJnYmFBcnIgPSByZ2JhQXJyIHx8IFtdO1xuICB2YXIgY2FjaGVkID0gY29sb3JDYWNoZS5nZXQoY29sb3JTdHIpO1xuXG4gIGlmIChjYWNoZWQpIHtcbiAgICByZXR1cm4gY29weVJnYmEocmdiYUFyciwgY2FjaGVkKTtcbiAgfSAvLyBjb2xvclN0ciBtYXkgYmUgbm90IHN0cmluZ1xuXG5cbiAgY29sb3JTdHIgPSBjb2xvclN0ciArICcnOyAvLyBSZW1vdmUgYWxsIHdoaXRlc3BhY2UsIG5vdCBjb21wbGlhbnQsIGJ1dCBzaG91bGQganVzdCBiZSBtb3JlIGFjY2VwdGluZy5cblxuICB2YXIgc3RyID0gY29sb3JTdHIucmVwbGFjZSgvIC9nLCAnJykudG9Mb3dlckNhc2UoKTsgLy8gQ29sb3Iga2V5d29yZHMgKGFuZCB0cmFuc3BhcmVudCkgbG9va3VwLlxuXG4gIGlmIChzdHIgaW4ga0NTU0NvbG9yVGFibGUpIHtcbiAgICBjb3B5UmdiYShyZ2JhQXJyLCBrQ1NTQ29sb3JUYWJsZVtzdHJdKTtcbiAgICBwdXRUb0NhY2hlKGNvbG9yU3RyLCByZ2JhQXJyKTtcbiAgICByZXR1cm4gcmdiYUFycjtcbiAgfSAvLyAjYWJjIGFuZCAjYWJjMTIzIHN5bnRheC5cblxuXG4gIGlmIChzdHIuY2hhckF0KDApID09PSAnIycpIHtcbiAgICBpZiAoc3RyLmxlbmd0aCA9PT0gNCkge1xuICAgICAgdmFyIGl2ID0gcGFyc2VJbnQoc3RyLnN1YnN0cigxKSwgMTYpOyAvLyBUT0RPKGRlYW5tKTogU3RyaWN0ZXIgcGFyc2luZy5cblxuICAgICAgaWYgKCEoaXYgPj0gMCAmJiBpdiA8PSAweGZmZikpIHtcbiAgICAgICAgc2V0UmdiYShyZ2JhQXJyLCAwLCAwLCAwLCAxKTtcbiAgICAgICAgcmV0dXJuOyAvLyBDb3ZlcnMgTmFOLlxuICAgICAgfVxuXG4gICAgICBzZXRSZ2JhKHJnYmFBcnIsIChpdiAmIDB4ZjAwKSA+PiA0IHwgKGl2ICYgMHhmMDApID4+IDgsIGl2ICYgMHhmMCB8IChpdiAmIDB4ZjApID4+IDQsIGl2ICYgMHhmIHwgKGl2ICYgMHhmKSA8PCA0LCAxKTtcbiAgICAgIHB1dFRvQ2FjaGUoY29sb3JTdHIsIHJnYmFBcnIpO1xuICAgICAgcmV0dXJuIHJnYmFBcnI7XG4gICAgfSBlbHNlIGlmIChzdHIubGVuZ3RoID09PSA3KSB7XG4gICAgICB2YXIgaXYgPSBwYXJzZUludChzdHIuc3Vic3RyKDEpLCAxNik7IC8vIFRPRE8oZGVhbm0pOiBTdHJpY3RlciBwYXJzaW5nLlxuXG4gICAgICBpZiAoIShpdiA+PSAwICYmIGl2IDw9IDB4ZmZmZmZmKSkge1xuICAgICAgICBzZXRSZ2JhKHJnYmFBcnIsIDAsIDAsIDAsIDEpO1xuICAgICAgICByZXR1cm47IC8vIENvdmVycyBOYU4uXG4gICAgICB9XG5cbiAgICAgIHNldFJnYmEocmdiYUFyciwgKGl2ICYgMHhmZjAwMDApID4+IDE2LCAoaXYgJiAweGZmMDApID4+IDgsIGl2ICYgMHhmZiwgMSk7XG4gICAgICBwdXRUb0NhY2hlKGNvbG9yU3RyLCByZ2JhQXJyKTtcbiAgICAgIHJldHVybiByZ2JhQXJyO1xuICAgIH1cblxuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBvcCA9IHN0ci5pbmRleE9mKCcoJyk7XG4gIHZhciBlcCA9IHN0ci5pbmRleE9mKCcpJyk7XG5cbiAgaWYgKG9wICE9PSAtMSAmJiBlcCArIDEgPT09IHN0ci5sZW5ndGgpIHtcbiAgICB2YXIgZm5hbWUgPSBzdHIuc3Vic3RyKDAsIG9wKTtcbiAgICB2YXIgcGFyYW1zID0gc3RyLnN1YnN0cihvcCArIDEsIGVwIC0gKG9wICsgMSkpLnNwbGl0KCcsJyk7XG4gICAgdmFyIGFscGhhID0gMTsgLy8gVG8gYWxsb3cgY2FzZSBmYWxsdGhyb3VnaC5cblxuICAgIHN3aXRjaCAoZm5hbWUpIHtcbiAgICAgIGNhc2UgJ3JnYmEnOlxuICAgICAgICBpZiAocGFyYW1zLmxlbmd0aCAhPT0gNCkge1xuICAgICAgICAgIHNldFJnYmEocmdiYUFyciwgMCwgMCwgMCwgMSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgYWxwaGEgPSBwYXJzZUNzc0Zsb2F0KHBhcmFtcy5wb3AoKSk7XG4gICAgICAvLyBqc2hpbnQgaWdub3JlOmxpbmVcbiAgICAgIC8vIEZhbGwgdGhyb3VnaC5cblxuICAgICAgY2FzZSAncmdiJzpcbiAgICAgICAgaWYgKHBhcmFtcy5sZW5ndGggIT09IDMpIHtcbiAgICAgICAgICBzZXRSZ2JhKHJnYmFBcnIsIDAsIDAsIDAsIDEpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHNldFJnYmEocmdiYUFyciwgcGFyc2VDc3NJbnQocGFyYW1zWzBdKSwgcGFyc2VDc3NJbnQocGFyYW1zWzFdKSwgcGFyc2VDc3NJbnQocGFyYW1zWzJdKSwgYWxwaGEpO1xuICAgICAgICBwdXRUb0NhY2hlKGNvbG9yU3RyLCByZ2JhQXJyKTtcbiAgICAgICAgcmV0dXJuIHJnYmFBcnI7XG5cbiAgICAgIGNhc2UgJ2hzbGEnOlxuICAgICAgICBpZiAocGFyYW1zLmxlbmd0aCAhPT0gNCkge1xuICAgICAgICAgIHNldFJnYmEocmdiYUFyciwgMCwgMCwgMCwgMSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgcGFyYW1zWzNdID0gcGFyc2VDc3NGbG9hdChwYXJhbXNbM10pO1xuICAgICAgICBoc2xhMnJnYmEocGFyYW1zLCByZ2JhQXJyKTtcbiAgICAgICAgcHV0VG9DYWNoZShjb2xvclN0ciwgcmdiYUFycik7XG4gICAgICAgIHJldHVybiByZ2JhQXJyO1xuXG4gICAgICBjYXNlICdoc2wnOlxuICAgICAgICBpZiAocGFyYW1zLmxlbmd0aCAhPT0gMykge1xuICAgICAgICAgIHNldFJnYmEocmdiYUFyciwgMCwgMCwgMCwgMSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaHNsYTJyZ2JhKHBhcmFtcywgcmdiYUFycik7XG4gICAgICAgIHB1dFRvQ2FjaGUoY29sb3JTdHIsIHJnYmFBcnIpO1xuICAgICAgICByZXR1cm4gcmdiYUFycjtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgfVxuXG4gIHNldFJnYmEocmdiYUFyciwgMCwgMCwgMCwgMSk7XG4gIHJldHVybjtcbn1cbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gaHNsYVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gcmdiYVxuICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59IHJnYmFcbiAqL1xuXG5cbmZ1bmN0aW9uIGhzbGEycmdiYShoc2xhLCByZ2JhKSB7XG4gIHZhciBoID0gKHBhcnNlRmxvYXQoaHNsYVswXSkgJSAzNjAgKyAzNjApICUgMzYwIC8gMzYwOyAvLyAwIC4uIDFcbiAgLy8gTk9URShkZWFubSk6IEFjY29yZGluZyB0byB0aGUgQ1NTIHNwZWMgcy9sIHNob3VsZCBvbmx5IGJlXG4gIC8vIHBlcmNlbnRhZ2VzLCBidXQgd2UgZG9uJ3QgYm90aGVyIGFuZCBsZXQgZmxvYXQgb3IgcGVyY2VudGFnZS5cblxuICB2YXIgcyA9IHBhcnNlQ3NzRmxvYXQoaHNsYVsxXSk7XG4gIHZhciBsID0gcGFyc2VDc3NGbG9hdChoc2xhWzJdKTtcbiAgdmFyIG0yID0gbCA8PSAwLjUgPyBsICogKHMgKyAxKSA6IGwgKyBzIC0gbCAqIHM7XG4gIHZhciBtMSA9IGwgKiAyIC0gbTI7XG4gIHJnYmEgPSByZ2JhIHx8IFtdO1xuICBzZXRSZ2JhKHJnYmEsIGNsYW1wQ3NzQnl0ZShjc3NIdWVUb1JnYihtMSwgbTIsIGggKyAxIC8gMykgKiAyNTUpLCBjbGFtcENzc0J5dGUoY3NzSHVlVG9SZ2IobTEsIG0yLCBoKSAqIDI1NSksIGNsYW1wQ3NzQnl0ZShjc3NIdWVUb1JnYihtMSwgbTIsIGggLSAxIC8gMykgKiAyNTUpLCAxKTtcblxuICBpZiAoaHNsYS5sZW5ndGggPT09IDQpIHtcbiAgICByZ2JhWzNdID0gaHNsYVszXTtcbiAgfVxuXG4gIHJldHVybiByZ2JhO1xufVxuLyoqXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSByZ2JhXG4gKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn0gaHNsYVxuICovXG5cblxuZnVuY3Rpb24gcmdiYTJoc2xhKHJnYmEpIHtcbiAgaWYgKCFyZ2JhKSB7XG4gICAgcmV0dXJuO1xuICB9IC8vIFJHQiBmcm9tIDAgdG8gMjU1XG5cblxuICB2YXIgUiA9IHJnYmFbMF0gLyAyNTU7XG4gIHZhciBHID0gcmdiYVsxXSAvIDI1NTtcbiAgdmFyIEIgPSByZ2JhWzJdIC8gMjU1O1xuICB2YXIgdk1pbiA9IE1hdGgubWluKFIsIEcsIEIpOyAvLyBNaW4uIHZhbHVlIG9mIFJHQlxuXG4gIHZhciB2TWF4ID0gTWF0aC5tYXgoUiwgRywgQik7IC8vIE1heC4gdmFsdWUgb2YgUkdCXG5cbiAgdmFyIGRlbHRhID0gdk1heCAtIHZNaW47IC8vIERlbHRhIFJHQiB2YWx1ZVxuXG4gIHZhciBMID0gKHZNYXggKyB2TWluKSAvIDI7XG4gIHZhciBIO1xuICB2YXIgUzsgLy8gSFNMIHJlc3VsdHMgZnJvbSAwIHRvIDFcblxuICBpZiAoZGVsdGEgPT09IDApIHtcbiAgICBIID0gMDtcbiAgICBTID0gMDtcbiAgfSBlbHNlIHtcbiAgICBpZiAoTCA8IDAuNSkge1xuICAgICAgUyA9IGRlbHRhIC8gKHZNYXggKyB2TWluKTtcbiAgICB9IGVsc2Uge1xuICAgICAgUyA9IGRlbHRhIC8gKDIgLSB2TWF4IC0gdk1pbik7XG4gICAgfVxuXG4gICAgdmFyIGRlbHRhUiA9ICgodk1heCAtIFIpIC8gNiArIGRlbHRhIC8gMikgLyBkZWx0YTtcbiAgICB2YXIgZGVsdGFHID0gKCh2TWF4IC0gRykgLyA2ICsgZGVsdGEgLyAyKSAvIGRlbHRhO1xuICAgIHZhciBkZWx0YUIgPSAoKHZNYXggLSBCKSAvIDYgKyBkZWx0YSAvIDIpIC8gZGVsdGE7XG5cbiAgICBpZiAoUiA9PT0gdk1heCkge1xuICAgICAgSCA9IGRlbHRhQiAtIGRlbHRhRztcbiAgICB9IGVsc2UgaWYgKEcgPT09IHZNYXgpIHtcbiAgICAgIEggPSAxIC8gMyArIGRlbHRhUiAtIGRlbHRhQjtcbiAgICB9IGVsc2UgaWYgKEIgPT09IHZNYXgpIHtcbiAgICAgIEggPSAyIC8gMyArIGRlbHRhRyAtIGRlbHRhUjtcbiAgICB9XG5cbiAgICBpZiAoSCA8IDApIHtcbiAgICAgIEggKz0gMTtcbiAgICB9XG5cbiAgICBpZiAoSCA+IDEpIHtcbiAgICAgIEggLT0gMTtcbiAgICB9XG4gIH1cblxuICB2YXIgaHNsYSA9IFtIICogMzYwLCBTLCBMXTtcblxuICBpZiAocmdiYVszXSAhPSBudWxsKSB7XG4gICAgaHNsYS5wdXNoKHJnYmFbM10pO1xuICB9XG5cbiAgcmV0dXJuIGhzbGE7XG59XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSBjb2xvclxuICogQHBhcmFtIHtudW1iZXJ9IGxldmVsXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvdXRpbC9jb2xvclxuICovXG5cblxuZnVuY3Rpb24gbGlmdChjb2xvciwgbGV2ZWwpIHtcbiAgdmFyIGNvbG9yQXJyID0gcGFyc2UoY29sb3IpO1xuXG4gIGlmIChjb2xvckFycikge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgMzsgaSsrKSB7XG4gICAgICBpZiAobGV2ZWwgPCAwKSB7XG4gICAgICAgIGNvbG9yQXJyW2ldID0gY29sb3JBcnJbaV0gKiAoMSAtIGxldmVsKSB8IDA7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb2xvckFycltpXSA9ICgyNTUgLSBjb2xvckFycltpXSkgKiBsZXZlbCArIGNvbG9yQXJyW2ldIHwgMDtcbiAgICAgIH1cblxuICAgICAgaWYgKGNvbG9yQXJyW2ldID4gMjU1KSB7XG4gICAgICAgIGNvbG9yQXJyW2ldID0gMjU1O1xuICAgICAgfSBlbHNlIGlmIChjb2xvcltpXSA8IDApIHtcbiAgICAgICAgY29sb3JBcnJbaV0gPSAwO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBzdHJpbmdpZnkoY29sb3JBcnIsIGNvbG9yQXJyLmxlbmd0aCA9PT0gNCA/ICdyZ2JhJyA6ICdyZ2InKTtcbiAgfVxufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gY29sb3JcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci91dGlsL2NvbG9yXG4gKi9cblxuXG5mdW5jdGlvbiB0b0hleChjb2xvcikge1xuICB2YXIgY29sb3JBcnIgPSBwYXJzZShjb2xvcik7XG5cbiAgaWYgKGNvbG9yQXJyKSB7XG4gICAgcmV0dXJuICgoMSA8PCAyNCkgKyAoY29sb3JBcnJbMF0gPDwgMTYpICsgKGNvbG9yQXJyWzFdIDw8IDgpICsgK2NvbG9yQXJyWzJdKS50b1N0cmluZygxNikuc2xpY2UoMSk7XG4gIH1cbn1cbi8qKlxuICogTWFwIHZhbHVlIHRvIGNvbG9yLiBGYXN0ZXIgdGhhbiBsZXJwIG1ldGhvZHMgYmVjYXVzZSBjb2xvciBpcyByZXByZXNlbnRlZCBieSByZ2JhIGFycmF5LlxuICogQHBhcmFtIHtudW1iZXJ9IG5vcm1hbGl6ZWRWYWx1ZSBBIGZsb2F0IGJldHdlZW4gMCBhbmQgMS5cbiAqIEBwYXJhbSB7QXJyYXkuPEFycmF5LjxudW1iZXI+Pn0gY29sb3JzIExpc3Qgb2YgcmdiYSBjb2xvciBhcnJheVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW291dF0gTWFwcGVkIGdiYSBjb2xvciBhcnJheVxuICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59IHdpbGwgYmUgbnVsbC91bmRlZmluZWQgaWYgaW5wdXQgaWxsZWdhbC5cbiAqL1xuXG5cbmZ1bmN0aW9uIGZhc3RMZXJwKG5vcm1hbGl6ZWRWYWx1ZSwgY29sb3JzLCBvdXQpIHtcbiAgaWYgKCEoY29sb3JzICYmIGNvbG9ycy5sZW5ndGgpIHx8ICEobm9ybWFsaXplZFZhbHVlID49IDAgJiYgbm9ybWFsaXplZFZhbHVlIDw9IDEpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgb3V0ID0gb3V0IHx8IFtdO1xuICB2YXIgdmFsdWUgPSBub3JtYWxpemVkVmFsdWUgKiAoY29sb3JzLmxlbmd0aCAtIDEpO1xuICB2YXIgbGVmdEluZGV4ID0gTWF0aC5mbG9vcih2YWx1ZSk7XG4gIHZhciByaWdodEluZGV4ID0gTWF0aC5jZWlsKHZhbHVlKTtcbiAgdmFyIGxlZnRDb2xvciA9IGNvbG9yc1tsZWZ0SW5kZXhdO1xuICB2YXIgcmlnaHRDb2xvciA9IGNvbG9yc1tyaWdodEluZGV4XTtcbiAgdmFyIGR2ID0gdmFsdWUgLSBsZWZ0SW5kZXg7XG4gIG91dFswXSA9IGNsYW1wQ3NzQnl0ZShsZXJwTnVtYmVyKGxlZnRDb2xvclswXSwgcmlnaHRDb2xvclswXSwgZHYpKTtcbiAgb3V0WzFdID0gY2xhbXBDc3NCeXRlKGxlcnBOdW1iZXIobGVmdENvbG9yWzFdLCByaWdodENvbG9yWzFdLCBkdikpO1xuICBvdXRbMl0gPSBjbGFtcENzc0J5dGUobGVycE51bWJlcihsZWZ0Q29sb3JbMl0sIHJpZ2h0Q29sb3JbMl0sIGR2KSk7XG4gIG91dFszXSA9IGNsYW1wQ3NzRmxvYXQobGVycE51bWJlcihsZWZ0Q29sb3JbM10sIHJpZ2h0Q29sb3JbM10sIGR2KSk7XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIEBkZXByZWNhdGVkXG4gKi9cblxuXG52YXIgZmFzdE1hcFRvQ29sb3IgPSBmYXN0TGVycDtcbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IG5vcm1hbGl6ZWRWYWx1ZSBBIGZsb2F0IGJldHdlZW4gMCBhbmQgMS5cbiAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IGNvbG9ycyBDb2xvciBsaXN0LlxuICogQHBhcmFtIHtib29sZWFuPX0gZnVsbE91dHB1dCBEZWZhdWx0IGZhbHNlLlxuICogQHJldHVybiB7KHN0cmluZ3xPYmplY3QpfSBSZXN1bHQgY29sb3IuIElmIGZ1bGxPdXRwdXQsXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7Y29sb3I6IC4uLiwgbGVmdEluZGV4OiAuLi4sIHJpZ2h0SW5kZXg6IC4uLiwgdmFsdWU6IC4uLn0sXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvdXRpbC9jb2xvclxuICovXG5cbmZ1bmN0aW9uIGxlcnAobm9ybWFsaXplZFZhbHVlLCBjb2xvcnMsIGZ1bGxPdXRwdXQpIHtcbiAgaWYgKCEoY29sb3JzICYmIGNvbG9ycy5sZW5ndGgpIHx8ICEobm9ybWFsaXplZFZhbHVlID49IDAgJiYgbm9ybWFsaXplZFZhbHVlIDw9IDEpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHZhbHVlID0gbm9ybWFsaXplZFZhbHVlICogKGNvbG9ycy5sZW5ndGggLSAxKTtcbiAgdmFyIGxlZnRJbmRleCA9IE1hdGguZmxvb3IodmFsdWUpO1xuICB2YXIgcmlnaHRJbmRleCA9IE1hdGguY2VpbCh2YWx1ZSk7XG4gIHZhciBsZWZ0Q29sb3IgPSBwYXJzZShjb2xvcnNbbGVmdEluZGV4XSk7XG4gIHZhciByaWdodENvbG9yID0gcGFyc2UoY29sb3JzW3JpZ2h0SW5kZXhdKTtcbiAgdmFyIGR2ID0gdmFsdWUgLSBsZWZ0SW5kZXg7XG4gIHZhciBjb2xvciA9IHN0cmluZ2lmeShbY2xhbXBDc3NCeXRlKGxlcnBOdW1iZXIobGVmdENvbG9yWzBdLCByaWdodENvbG9yWzBdLCBkdikpLCBjbGFtcENzc0J5dGUobGVycE51bWJlcihsZWZ0Q29sb3JbMV0sIHJpZ2h0Q29sb3JbMV0sIGR2KSksIGNsYW1wQ3NzQnl0ZShsZXJwTnVtYmVyKGxlZnRDb2xvclsyXSwgcmlnaHRDb2xvclsyXSwgZHYpKSwgY2xhbXBDc3NGbG9hdChsZXJwTnVtYmVyKGxlZnRDb2xvclszXSwgcmlnaHRDb2xvclszXSwgZHYpKV0sICdyZ2JhJyk7XG4gIHJldHVybiBmdWxsT3V0cHV0ID8ge1xuICAgIGNvbG9yOiBjb2xvcixcbiAgICBsZWZ0SW5kZXg6IGxlZnRJbmRleCxcbiAgICByaWdodEluZGV4OiByaWdodEluZGV4LFxuICAgIHZhbHVlOiB2YWx1ZVxuICB9IDogY29sb3I7XG59XG4vKipcbiAqIEBkZXByZWNhdGVkXG4gKi9cblxuXG52YXIgbWFwVG9Db2xvciA9IGxlcnA7XG4vKipcbiAqIEBwYXJhbSB7c3RyaW5nfSBjb2xvclxuICogQHBhcmFtIHtudW1iZXI9fSBoIDAgfiAzNjAsIGlnbm9yZSB3aGVuIG51bGwuXG4gKiBAcGFyYW0ge251bWJlcj19IHMgMCB+IDEsIGlnbm9yZSB3aGVuIG51bGwuXG4gKiBAcGFyYW0ge251bWJlcj19IGwgMCB+IDEsIGlnbm9yZSB3aGVuIG51bGwuXG4gKiBAcmV0dXJuIHtzdHJpbmd9IENvbG9yIHN0cmluZyBpbiByZ2JhIGZvcm1hdC5cbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci91dGlsL2NvbG9yXG4gKi9cblxuZnVuY3Rpb24gbW9kaWZ5SFNMKGNvbG9yLCBoLCBzLCBsKSB7XG4gIGNvbG9yID0gcGFyc2UoY29sb3IpO1xuXG4gIGlmIChjb2xvcikge1xuICAgIGNvbG9yID0gcmdiYTJoc2xhKGNvbG9yKTtcbiAgICBoICE9IG51bGwgJiYgKGNvbG9yWzBdID0gY2xhbXBDc3NBbmdsZShoKSk7XG4gICAgcyAhPSBudWxsICYmIChjb2xvclsxXSA9IHBhcnNlQ3NzRmxvYXQocykpO1xuICAgIGwgIT0gbnVsbCAmJiAoY29sb3JbMl0gPSBwYXJzZUNzc0Zsb2F0KGwpKTtcbiAgICByZXR1cm4gc3RyaW5naWZ5KGhzbGEycmdiYShjb2xvciksICdyZ2JhJyk7XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IGNvbG9yXG4gKiBAcGFyYW0ge251bWJlcj19IGFscGhhIDAgfiAxXG4gKiBAcmV0dXJuIHtzdHJpbmd9IENvbG9yIHN0cmluZyBpbiByZ2JhIGZvcm1hdC5cbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci91dGlsL2NvbG9yXG4gKi9cblxuXG5mdW5jdGlvbiBtb2RpZnlBbHBoYShjb2xvciwgYWxwaGEpIHtcbiAgY29sb3IgPSBwYXJzZShjb2xvcik7XG5cbiAgaWYgKGNvbG9yICYmIGFscGhhICE9IG51bGwpIHtcbiAgICBjb2xvclszXSA9IGNsYW1wQ3NzRmxvYXQoYWxwaGEpO1xuICAgIHJldHVybiBzdHJpbmdpZnkoY29sb3IsICdyZ2JhJyk7XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gYXJyQ29sb3IgbGlrZSBbMTIsMzMsNDQsMC40XVxuICogQHBhcmFtIHtzdHJpbmd9IHR5cGUgJ3JnYmEnLCAnaHN2YScsIC4uLlxuICogQHJldHVybiB7c3RyaW5nfSBSZXN1bHQgY29sb3IuIChJZiBpbnB1dCBpbGxlZ2FsLCByZXR1cm4gdW5kZWZpbmVkKS5cbiAqL1xuXG5cbmZ1bmN0aW9uIHN0cmluZ2lmeShhcnJDb2xvciwgdHlwZSkge1xuICBpZiAoIWFyckNvbG9yIHx8ICFhcnJDb2xvci5sZW5ndGgpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgY29sb3JTdHIgPSBhcnJDb2xvclswXSArICcsJyArIGFyckNvbG9yWzFdICsgJywnICsgYXJyQ29sb3JbMl07XG5cbiAgaWYgKHR5cGUgPT09ICdyZ2JhJyB8fCB0eXBlID09PSAnaHN2YScgfHwgdHlwZSA9PT0gJ2hzbGEnKSB7XG4gICAgY29sb3JTdHIgKz0gJywnICsgYXJyQ29sb3JbM107XG4gIH1cblxuICByZXR1cm4gdHlwZSArICcoJyArIGNvbG9yU3RyICsgJyknO1xufVxuXG5leHBvcnRzLnBhcnNlID0gcGFyc2U7XG5leHBvcnRzLmxpZnQgPSBsaWZ0O1xuZXhwb3J0cy50b0hleCA9IHRvSGV4O1xuZXhwb3J0cy5mYXN0TGVycCA9IGZhc3RMZXJwO1xuZXhwb3J0cy5mYXN0TWFwVG9Db2xvciA9IGZhc3RNYXBUb0NvbG9yO1xuZXhwb3J0cy5sZXJwID0gbGVycDtcbmV4cG9ydHMubWFwVG9Db2xvciA9IG1hcFRvQ29sb3I7XG5leHBvcnRzLm1vZGlmeUhTTCA9IG1vZGlmeUhTTDtcbmV4cG9ydHMubW9kaWZ5QWxwaGEgPSBtb2RpZnlBbHBoYTtcbmV4cG9ydHMuc3RyaW5naWZ5ID0gc3RyaW5naWZ5OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFwSkE7QUFDQTtBQXNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0NBO0FBNkNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/tool/color.js
