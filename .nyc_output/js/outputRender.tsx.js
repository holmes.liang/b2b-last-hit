__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/spin */ "./src/app/desk/component/spin.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/outputRender.tsx";

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["  \n      "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["\n  span.ant-upload {\n    line-height: normal;\n  }\n  div.ant-upload.ant-upload-select.ant-upload-select-text{\n    margin-top: -3px;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_8__["default"])(["\n  display: flex;\n  justify-content: space-evenly;\n  i {\n     font-size: 18px;\n      padding-right: 8px; \n      &.icon-card {\n        padding-right: 10px; \n      }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_11__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var PdfDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject());
var DocSpan = _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].span(_templateObject2());

var OutputRender =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(OutputRender, _ModelWidget);

  function OutputRender(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, OutputRender);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(OutputRender).call(this, props, state));
    _this.getOutputs =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var dataFixed, policyId;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              dataFixed = _this.props.dataFixed;
              policyId = _this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".policyId") : "policyId");

              _this.getHelpers().getAjax().get("/policies/".concat(policyId, "/outputs"), {}, {}).then(function (response) {
                var _ref2 = response.body || [],
                    respData = _ref2.respData;

                if (_this.props.afterGetData) {
                  _this.props.afterGetData(respData);
                }

                _this.setState({
                  pdfList: respData
                });
              }).catch(function (error) {});

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(OutputRender, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(OutputRender.prototype), "initState", this).call(this), {
        pdfList: []
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_9__["Styled"].div(_templateObject3())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getOutputs();
    }
  }, {
    key: "handleFileChange",
    value: function handleFileChange(info) {
      var dataFixed = this.props.dataFixed;

      if (lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(info, "file.response")) {
        var fid = lodash__WEBPACK_IMPORTED_MODULE_14___default.a.get(info, "file.response.respData");

        this.setValueToModel(fid, dataFixed ? "".concat(dataFixed, ".ext.policyDocFid") : "ext.policyDocFid");
        this.props.afterUploadPolicyDoc && this.props.afterUploadPolicyDoc();
      }
    }
  }, {
    key: "handleLink",
    value: function handleLink(url) {
      var _this2 = this;

      var _this$props = this.props,
          beforeDownload = _this$props.beforeDownload,
          that = _this$props.that;

      if (beforeDownload && that) {
        var newModel = that.props.mergePolicyToServiceModel();
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_12__["Apis"].CART_MERGE, newModel, {
          loading: true
        }).then(function (response) {
          if (response.body.respCode !== "0000") return;
          that.props.mergePolicyToUIModel(response.body.respData || {});

          _this2.downloadFire(url);
        });
      } else {
        this.downloadFire(url);
      }
    }
  }, {
    key: "downloadFire",
    value: function downloadFire(url) {
      var link = document.createElement("a");
      link.style.display = "none";
      link.href = url; // link.setAttribute("download", "template.xlsx");

      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link); // 下载完成移除元素
    }
  }, {
    key: "renderPdfList",
    value: function renderPdfList() {
      var dataFixed = this.props.dataFixed;
      var policyId = this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".policyId") : "policyId");
      var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].appendAuthToUrl("/policies/".concat(policyId, "/passkit/applewallet"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
        href: pdfUrl,
        className: "pdf-link-item",
        key: "E-card",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("i", {
        style: {
          lineHeight: "normal"
        },
        className: "iconfont icon-card",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
        style: {
          verticalAlign: "top"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, "E-card"));
    }
  }, {
    key: "renderPolicyDoc",
    value: function renderPolicyDoc() {
      var _this3 = this;

      var dataFixed = this.props.dataFixed;
      var policyDocFid = this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".ext.policyDocFid") : "ext.policyDocFid");
      var allowToUploadPolicyDoc = this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".opers.allowToUploadPolicyDoc") : "opers.allowToUploadPolicyDoc");
      var authKey = _common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].AUTH_KEY);
      var props = {
        action: function action(file) {
          return _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].getServiceLocation("/blob/files");
        },
        headers: {
          Authorization: authKey
        },
        beforeUpload: function beforeUpload(file, fileList) {
          _desk_component_spin__WEBPACK_IMPORTED_MODULE_15__["default"].spinShow();
          return true;
        },
        onChange: function onChange(info) {
          return _this3.handleFileChange(info);
        },
        showUploadList: false
      }; // if (policyDocFid) {

      var url = _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].getServiceLocation("/blob/files/".concat(policyDocFid, "?access_token=").concat(authKey));

      if (!policyDocFid && !allowToUploadPolicyDoc) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Fragment, null);
      }

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(DocSpan, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }, policyDocFid ? _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
        style: {
          marginRight: 10,
          verticalAlign: "top"
        },
        href: url,
        className: "pdf-link-item",
        key: "policyDoc",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "file-pdf",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 175
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 176
        },
        __self: this
      }, "Policy Doc")) : _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
        style: {
          marginRight: 10,
          verticalAlign: "top"
        },
        className: "pdf-link-item",
        key: "policyDoc",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 179
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
        type: "file-pdf",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 180
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 181
        },
        __self: this
      }, "Policy Doc")), allowToUploadPolicyDoc && _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Upload"], Object.assign({}, props, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 185
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
        className: "pdf-link-item",
        key: "upload",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 186
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("i", {
        style: {
          color: "rgba(0, 0, 0, .65)",
          fontSize: 24
        },
        className: "iconfont icon-upload",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 187
        },
        __self: this
      }))));
    }
  }, {
    key: "outPutsRender",
    value: function outPutsRender() {
      var _this4 = this;

      var dataFixed = this.props.dataFixed;
      var pdfList = this.state.pdfList;
      var policyId = this.getValueFromModel(dataFixed ? "".concat(dataFixed, ".policyId") : "policyId");
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(PdfDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 204
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].getServiceLocation("/policies/".concat(policyId, "/pdf/").concat(name, "?access_token=").concat(_common__WEBPACK_IMPORTED_MODULE_12__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_12__["Consts"].AUTH_KEY)));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("a", {
          style: {
            color: COLOR_A
          },
          onClick: function onClick() {
            return _this4.handleLink(pdfUrl);
          },
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 212
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 213
          },
          __self: this
        }), "".concat(displayName));
      }), this.props.isECard && this.renderPdfList(), this.props.isPolicyDoc && this.renderPolicyDoc());
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 227
        },
        __self: this
      }, this.outPutsRender());
    }
  }]);

  return OutputRender;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (OutputRender);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L291dHB1dFJlbmRlci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvb3V0cHV0UmVuZGVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UsIFN0eWxlZERJViwgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXMvaW5kZXhcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIENvbnN0cywgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBJY29uLCBVcGxvYWQgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgRGVza1BhZ2VCb3ggfSBmcm9tIFwiQGRlc2stc3R5bGVzL2dsb2JhbFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmNGaWxlIH0gZnJvbSBcImFudGQvbGliL3VwbG9hZFwiO1xuaW1wb3J0IHNwaW4gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9zcGluXCI7XG5cbmNvbnN0IHsgQ09MT1JfQSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcbmV4cG9ydCB0eXBlIE91dHB1dFJlbmRlclByb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBhZnRlckdldERhdGE/OiBhbnksXG4gIGRhdGFGaXhlZD86IGFueTtcbiAgaXNQb2xpY3lEb2M/OiBib29sZWFuXG4gIGlzRUNhcmQ/OiBib29sZWFuXG4gIGFmdGVyVXBsb2FkUG9saWN5RG9jPzogYW55O1xuICBiZWZvcmVEb3dubG9hZD86IGJvb2xlYW47XG4gIHRoYXQ/OiBhbnlcbn0gJiBXaWRnZXRQcm9wcztcblxuZXhwb3J0IHR5cGUgT3V0cHV0UmVuZGVyU3RhdGUgPSB7XG4gIHBkZkxpc3Q6IGFueTtcbn07XG5cbmV4cG9ydCB0eXBlIE91dHB1dFJlbmRlckNvbXBvbmVudHMgPSB7XG4gIEJveDogU3R5bGVkRElWO1xufTtcblxuY29uc3QgUGRmRGl2ID0gU3R5bGVkLmRpdmBcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIGkge1xuICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiA4cHg7IFxuICAgICAgJi5pY29uLWNhcmQge1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4OyBcbiAgICAgIH1cbiAgfVxuYDtcblxuY29uc3QgRG9jU3BhbiA9IFN0eWxlZC5zcGFuYFxuICBzcGFuLmFudC11cGxvYWQge1xuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gIH1cbiAgZGl2LmFudC11cGxvYWQuYW50LXVwbG9hZC1zZWxlY3QuYW50LXVwbG9hZC1zZWxlY3QtdGV4dHtcbiAgICBtYXJnaW4tdG9wOiAtM3B4O1xuICB9XG5gO1xuXG5jbGFzcyBPdXRwdXRSZW5kZXI8UCBleHRlbmRzIE91dHB1dFJlbmRlclByb3BzLCBTIGV4dGVuZHMgT3V0cHV0UmVuZGVyU3RhdGUsIEMgZXh0ZW5kcyBPdXRwdXRSZW5kZXJDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgc3RhdGU/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgc3RhdGUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgcGRmTGlzdDogW10sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YCAgXG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuZ2V0T3V0cHV0cygpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRPdXRwdXRzID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHBvbGljeUlkID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChkYXRhRml4ZWQgPyBgJHtkYXRhRml4ZWR9LnBvbGljeUlkYCA6IFwicG9saWN5SWRcIik7XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5nZXQoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9vdXRwdXRzYCwge30sIHt9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keSB8fCBbXTtcbiAgICAgICAgaWYgKHRoaXMucHJvcHMuYWZ0ZXJHZXREYXRhKSB7XG4gICAgICAgICAgdGhpcy5wcm9wcy5hZnRlckdldERhdGEocmVzcERhdGEpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHBkZkxpc3Q6IHJlc3BEYXRhLFxuICAgICAgICB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgfSk7XG4gIH07XG5cbiAgcHJpdmF0ZSBoYW5kbGVGaWxlQ2hhbmdlKGluZm86IGFueSkge1xuICAgIGNvbnN0IHsgZGF0YUZpeGVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChfLmdldChpbmZvLCBcImZpbGUucmVzcG9uc2VcIikpIHtcblxuICAgICAgY29uc3QgZmlkID0gXy5nZXQoaW5mbywgXCJmaWxlLnJlc3BvbnNlLnJlc3BEYXRhXCIpO1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoZmlkLCBkYXRhRml4ZWQgPyBgJHtkYXRhRml4ZWR9LmV4dC5wb2xpY3lEb2NGaWRgIDogXCJleHQucG9saWN5RG9jRmlkXCIpO1xuICAgICAgdGhpcy5wcm9wcy5hZnRlclVwbG9hZFBvbGljeURvYyAmJiB0aGlzLnByb3BzLmFmdGVyVXBsb2FkUG9saWN5RG9jKCk7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlTGluayh1cmw6IGFueSkge1xuICAgIGNvbnN0IHsgYmVmb3JlRG93bmxvYWQsIHRoYXQgfSA9IHRoaXMucHJvcHM7XG4gICAgaWYgKGJlZm9yZURvd25sb2FkICYmIHRoYXQpIHtcbiAgICAgIGNvbnN0IG5ld01vZGVsID0gdGhhdC5wcm9wcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk7XG4gICAgICBBamF4LnBvc3QoQXBpcy5DQVJUX01FUkdFLCBuZXdNb2RlbCwgeyBsb2FkaW5nOiB0cnVlIH0pXG4gICAgICAgIC50aGVuKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKHJlc3BvbnNlLmJvZHkucmVzcENvZGUgIT09IFwiMDAwMFwiKSByZXR1cm47XG4gICAgICAgICAgdGhhdC5wcm9wcy5tZXJnZVBvbGljeVRvVUlNb2RlbChyZXNwb25zZS5ib2R5LnJlc3BEYXRhIHx8IHt9KTtcbiAgICAgICAgICB0aGlzLmRvd25sb2FkRmlyZSh1cmwpO1xuICAgICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5kb3dubG9hZEZpcmUodXJsKTtcbiAgICB9XG4gIH1cblxuICBkb3dubG9hZEZpcmUodXJsOiBhbnkpIHtcbiAgICBsZXQgbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJhXCIpO1xuICAgIGxpbmsuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgIGxpbmsuaHJlZiA9IHVybDtcbiAgICAvLyBsaW5rLnNldEF0dHJpYnV0ZShcImRvd25sb2FkXCIsIFwidGVtcGxhdGUueGxzeFwiKTtcbiAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGxpbmspO1xuICAgIGxpbmsuY2xpY2soKTtcbiAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKGxpbmspOyAvLyDkuIvovb3lrozmiJDnp7vpmaTlhYPntKBcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUGRmTGlzdCgpIHtcbiAgICBjb25zdCB7IGRhdGFGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwb2xpY3lJZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoZGF0YUZpeGVkID8gYCR7ZGF0YUZpeGVkfS5wb2xpY3lJZGAgOiBcInBvbGljeUlkXCIpO1xuXG4gICAgY29uc3QgcGRmVXJsID0gQWpheC5hcHBlbmRBdXRoVG9VcmwoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9wYXNza2l0L2FwcGxld2FsbGV0YCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxhIGhyZWY9e3BkZlVybH0gY2xhc3NOYW1lPVwicGRmLWxpbmstaXRlbVwiIGtleT17XCJFLWNhcmRcIn0+XG4gICAgICAgIDxpIHN0eWxlPXt7XG4gICAgICAgICAgbGluZUhlaWdodDogXCJub3JtYWxcIixcbiAgICAgICAgfX0gY2xhc3NOYW1lPVwiaWNvbmZvbnQgaWNvbi1jYXJkXCIvPlxuICAgICAgICA8c3BhbiBzdHlsZT17eyB2ZXJ0aWNhbEFsaWduOiBcInRvcFwiIH19PntgRS1jYXJkYH08L3NwYW4+XG4gICAgICA8L2E+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclBvbGljeURvYygpIHtcbiAgICBjb25zdCB7IGRhdGFGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBwb2xpY3lEb2NGaWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0uZXh0LnBvbGljeURvY0ZpZGAgOiBcImV4dC5wb2xpY3lEb2NGaWRcIik7XG4gICAgY29uc3QgYWxsb3dUb1VwbG9hZFBvbGljeURvYyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoZGF0YUZpeGVkID8gYCR7ZGF0YUZpeGVkfS5vcGVycy5hbGxvd1RvVXBsb2FkUG9saWN5RG9jYCA6IFwib3BlcnMuYWxsb3dUb1VwbG9hZFBvbGljeURvY1wiKTtcbiAgICBjb25zdCBhdXRoS2V5ID0gU3RvcmFnZS5BdXRoLnNlc3Npb24oKS5nZXQoQ29uc3RzLkFVVEhfS0VZKTtcbiAgICBjb25zdCBwcm9wcyA9IHtcbiAgICAgIGFjdGlvbjogKGZpbGU6IFJjRmlsZSkgPT4ge1xuICAgICAgICByZXR1cm4gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oYC9ibG9iL2ZpbGVzYCk7XG4gICAgICB9LFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBdXRob3JpemF0aW9uOiBhdXRoS2V5LFxuICAgICAgfSxcbiAgICAgIGJlZm9yZVVwbG9hZDogKGZpbGU6IFJjRmlsZSwgZmlsZUxpc3Q6IFJjRmlsZVtdKSA9PiB7XG4gICAgICAgIHNwaW4uc3BpblNob3coKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9LFxuICAgICAgb25DaGFuZ2U6IChpbmZvOiBhbnkpID0+IHRoaXMuaGFuZGxlRmlsZUNoYW5nZShpbmZvKSxcblxuICAgICAgc2hvd1VwbG9hZExpc3Q6IGZhbHNlLFxuICAgIH07XG4gICAgLy8gaWYgKHBvbGljeURvY0ZpZCkge1xuICAgIGNvbnN0IHVybCA9IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKGAvYmxvYi9maWxlcy8ke3BvbGljeURvY0ZpZH0/YWNjZXNzX3Rva2VuPSR7YXV0aEtleX1gKTtcbiAgICBpZiAoIXBvbGljeURvY0ZpZCAmJiAhYWxsb3dUb1VwbG9hZFBvbGljeURvYykge1xuICAgICAgcmV0dXJuIDw+PC8+O1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgPERvY1NwYW4+XG4gICAgICAgIHtcbiAgICAgICAgICBwb2xpY3lEb2NGaWQgP1xuICAgICAgICAgICAgPGEgc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IDEwLCB2ZXJ0aWNhbEFsaWduOiBcInRvcFwiIH19IGhyZWY9e3VybH0gY2xhc3NOYW1lPVwicGRmLWxpbmstaXRlbVwiXG4gICAgICAgICAgICAgICBrZXk9e1wicG9saWN5RG9jXCJ9PlxuICAgICAgICAgICAgICA8SWNvbiB0eXBlPVwiZmlsZS1wZGZcIi8+XG4gICAgICAgICAgICAgIDxzcGFuPntgUG9saWN5IERvY2B9PC9zcGFuPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgOlxuICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IDEwLCB2ZXJ0aWNhbEFsaWduOiBcInRvcFwiIH19IGNsYXNzTmFtZT1cInBkZi1saW5rLWl0ZW1cIiBrZXk9e1wicG9saWN5RG9jXCJ9PlxuICAgICAgICAgICAgICAgIDxJY29uIHR5cGU9XCJmaWxlLXBkZlwiLz5cbiAgICAgICAgICAgICAgICA8c3Bhbj57YFBvbGljeSBEb2NgfTwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICB9XG5cbiAgICAgICAge2FsbG93VG9VcGxvYWRQb2xpY3lEb2MgJiYgPFVwbG9hZCAgey4uLnByb3BzfT5cbiAgICAgICAgICA8YSBjbGFzc05hbWU9XCJwZGYtbGluay1pdGVtXCIga2V5PXtcInVwbG9hZFwifT5cbiAgICAgICAgICAgIDxpIHN0eWxlPXt7XG4gICAgICAgICAgICAgIGNvbG9yOiBcInJnYmEoMCwgMCwgMCwgLjY1KVwiLFxuICAgICAgICAgICAgICBmb250U2l6ZTogMjQsXG4gICAgICAgICAgICB9fSBjbGFzc05hbWU9XCJpY29uZm9udCBpY29uLXVwbG9hZFwiLz5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvVXBsb2FkPlxuICAgICAgICB9XG4gICAgICA8L0RvY1NwYW4+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgb3V0UHV0c1JlbmRlcigpIHtcbiAgICBjb25zdCB7IGRhdGFGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHBkZkxpc3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgcG9saWN5SWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGRhdGFGaXhlZCA/IGAke2RhdGFGaXhlZH0ucG9saWN5SWRgIDogXCJwb2xpY3lJZFwiKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8UGRmRGl2PlxuICAgICAgICB7KHBkZkxpc3QgfHwgW10pLm1hcCgocGRmOiBhbnkpID0+IHtcbiAgICAgICAgICBjb25zdCB7IGRpc3BsYXlOYW1lLCBuYW1lLCBmaWxlTmFtZSB9ID0gcGRmO1xuICAgICAgICAgIGNvbnN0IHBkZlVybCA9IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKFxuICAgICAgICAgICAgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9wZGYvJHtuYW1lfT9hY2Nlc3NfdG9rZW49JHtTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpfWAsXG4gICAgICAgICAgKTtcblxuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8YSBzdHlsZT17eyBjb2xvcjogQ09MT1JfQSB9fSBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUxpbmsocGRmVXJsKX0ga2V5PXtmaWxlTmFtZX0+XG4gICAgICAgICAgICAgIDxJY29uIHR5cGU9XCJmaWxlLXBkZlwiLz5cbiAgICAgICAgICAgICAge2Ake2Rpc3BsYXlOYW1lfWB9XG4gICAgICAgICAgICA8L2E+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICAgIHt0aGlzLnByb3BzLmlzRUNhcmQgJiYgdGhpcy5yZW5kZXJQZGZMaXN0KCl9XG4gICAgICAgIHt0aGlzLnByb3BzLmlzUG9saWN5RG9jICYmIHRoaXMucmVuZGVyUG9saWN5RG9jKCl9XG4gICAgICA8L1BkZkRpdj5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94PlxuICAgICAgICB7dGhpcy5vdXRQdXRzUmVuZGVyKCl9XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgT3V0cHV0UmVuZGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBbUJBO0FBWUE7QUFDQTtBQVFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBZkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFyQkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQXFCQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFFQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQWJBO0FBQ0E7QUFlQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUtBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7OztBQWxMQTtBQUNBO0FBb0xBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/outputRender.tsx
