var Clip = __webpack_require__(/*! ./Clip */ "./node_modules/zrender/lib/animation/Clip.js");

var color = __webpack_require__(/*! ../tool/color */ "./node_modules/zrender/lib/tool/color.js");

var _util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var isArrayLike = _util.isArrayLike;
/**
 * @module echarts/animation/Animator
 */

var arraySlice = Array.prototype.slice;

function defaultGetter(target, key) {
  return target[key];
}

function defaultSetter(target, key, value) {
  target[key] = value;
}
/**
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} percent
 * @return {number}
 */


function interpolateNumber(p0, p1, percent) {
  return (p1 - p0) * percent + p0;
}
/**
 * @param  {string} p0
 * @param  {string} p1
 * @param  {number} percent
 * @return {string}
 */


function interpolateString(p0, p1, percent) {
  return percent > 0.5 ? p1 : p0;
}
/**
 * @param  {Array} p0
 * @param  {Array} p1
 * @param  {number} percent
 * @param  {Array} out
 * @param  {number} arrDim
 */


function interpolateArray(p0, p1, percent, out, arrDim) {
  var len = p0.length;

  if (arrDim === 1) {
    for (var i = 0; i < len; i++) {
      out[i] = interpolateNumber(p0[i], p1[i], percent);
    }
  } else {
    var len2 = len && p0[0].length;

    for (var i = 0; i < len; i++) {
      for (var j = 0; j < len2; j++) {
        out[i][j] = interpolateNumber(p0[i][j], p1[i][j], percent);
      }
    }
  }
} // arr0 is source array, arr1 is target array.
// Do some preprocess to avoid error happened when interpolating from arr0 to arr1


function fillArr(arr0, arr1, arrDim) {
  var arr0Len = arr0.length;
  var arr1Len = arr1.length;

  if (arr0Len !== arr1Len) {
    // FIXME Not work for TypedArray
    var isPreviousLarger = arr0Len > arr1Len;

    if (isPreviousLarger) {
      // Cut the previous
      arr0.length = arr1Len;
    } else {
      // Fill the previous
      for (var i = arr0Len; i < arr1Len; i++) {
        arr0.push(arrDim === 1 ? arr1[i] : arraySlice.call(arr1[i]));
      }
    }
  } // Handling NaN value


  var len2 = arr0[0] && arr0[0].length;

  for (var i = 0; i < arr0.length; i++) {
    if (arrDim === 1) {
      if (isNaN(arr0[i])) {
        arr0[i] = arr1[i];
      }
    } else {
      for (var j = 0; j < len2; j++) {
        if (isNaN(arr0[i][j])) {
          arr0[i][j] = arr1[i][j];
        }
      }
    }
  }
}
/**
 * @param  {Array} arr0
 * @param  {Array} arr1
 * @param  {number} arrDim
 * @return {boolean}
 */


function isArraySame(arr0, arr1, arrDim) {
  if (arr0 === arr1) {
    return true;
  }

  var len = arr0.length;

  if (len !== arr1.length) {
    return false;
  }

  if (arrDim === 1) {
    for (var i = 0; i < len; i++) {
      if (arr0[i] !== arr1[i]) {
        return false;
      }
    }
  } else {
    var len2 = arr0[0].length;

    for (var i = 0; i < len; i++) {
      for (var j = 0; j < len2; j++) {
        if (arr0[i][j] !== arr1[i][j]) {
          return false;
        }
      }
    }
  }

  return true;
}
/**
 * Catmull Rom interpolate array
 * @param  {Array} p0
 * @param  {Array} p1
 * @param  {Array} p2
 * @param  {Array} p3
 * @param  {number} t
 * @param  {number} t2
 * @param  {number} t3
 * @param  {Array} out
 * @param  {number} arrDim
 */


function catmullRomInterpolateArray(p0, p1, p2, p3, t, t2, t3, out, arrDim) {
  var len = p0.length;

  if (arrDim === 1) {
    for (var i = 0; i < len; i++) {
      out[i] = catmullRomInterpolate(p0[i], p1[i], p2[i], p3[i], t, t2, t3);
    }
  } else {
    var len2 = p0[0].length;

    for (var i = 0; i < len; i++) {
      for (var j = 0; j < len2; j++) {
        out[i][j] = catmullRomInterpolate(p0[i][j], p1[i][j], p2[i][j], p3[i][j], t, t2, t3);
      }
    }
  }
}
/**
 * Catmull Rom interpolate number
 * @param  {number} p0
 * @param  {number} p1
 * @param  {number} p2
 * @param  {number} p3
 * @param  {number} t
 * @param  {number} t2
 * @param  {number} t3
 * @return {number}
 */


function catmullRomInterpolate(p0, p1, p2, p3, t, t2, t3) {
  var v0 = (p2 - p0) * 0.5;
  var v1 = (p3 - p1) * 0.5;
  return (2 * (p1 - p2) + v0 + v1) * t3 + (-3 * (p1 - p2) - 2 * v0 - v1) * t2 + v0 * t + p1;
}

function cloneValue(value) {
  if (isArrayLike(value)) {
    var len = value.length;

    if (isArrayLike(value[0])) {
      var ret = [];

      for (var i = 0; i < len; i++) {
        ret.push(arraySlice.call(value[i]));
      }

      return ret;
    }

    return arraySlice.call(value);
  }

  return value;
}

function rgba2String(rgba) {
  rgba[0] = Math.floor(rgba[0]);
  rgba[1] = Math.floor(rgba[1]);
  rgba[2] = Math.floor(rgba[2]);
  return 'rgba(' + rgba.join(',') + ')';
}

function getArrayDim(keyframes) {
  var lastValue = keyframes[keyframes.length - 1].value;
  return isArrayLike(lastValue && lastValue[0]) ? 2 : 1;
}

function createTrackClip(animator, easing, oneTrackDone, keyframes, propName, forceAnimate) {
  var getter = animator._getter;
  var setter = animator._setter;
  var useSpline = easing === 'spline';
  var trackLen = keyframes.length;

  if (!trackLen) {
    return;
  } // Guess data type


  var firstVal = keyframes[0].value;
  var isValueArray = isArrayLike(firstVal);
  var isValueColor = false;
  var isValueString = false; // For vertices morphing

  var arrDim = isValueArray ? getArrayDim(keyframes) : 0;
  var trackMaxTime; // Sort keyframe as ascending

  keyframes.sort(function (a, b) {
    return a.time - b.time;
  });
  trackMaxTime = keyframes[trackLen - 1].time; // Percents of each keyframe

  var kfPercents = []; // Value of each keyframe

  var kfValues = [];
  var prevValue = keyframes[0].value;
  var isAllValueEqual = true;

  for (var i = 0; i < trackLen; i++) {
    kfPercents.push(keyframes[i].time / trackMaxTime); // Assume value is a color when it is a string

    var value = keyframes[i].value; // Check if value is equal, deep check if value is array

    if (!(isValueArray && isArraySame(value, prevValue, arrDim) || !isValueArray && value === prevValue)) {
      isAllValueEqual = false;
    }

    prevValue = value; // Try converting a string to a color array

    if (typeof value === 'string') {
      var colorArray = color.parse(value);

      if (colorArray) {
        value = colorArray;
        isValueColor = true;
      } else {
        isValueString = true;
      }
    }

    kfValues.push(value);
  }

  if (!forceAnimate && isAllValueEqual) {
    return;
  }

  var lastValue = kfValues[trackLen - 1]; // Polyfill array and NaN value

  for (var i = 0; i < trackLen - 1; i++) {
    if (isValueArray) {
      fillArr(kfValues[i], lastValue, arrDim);
    } else {
      if (isNaN(kfValues[i]) && !isNaN(lastValue) && !isValueString && !isValueColor) {
        kfValues[i] = lastValue;
      }
    }
  }

  isValueArray && fillArr(getter(animator._target, propName), lastValue, arrDim); // Cache the key of last frame to speed up when
  // animation playback is sequency

  var lastFrame = 0;
  var lastFramePercent = 0;
  var start;
  var w;
  var p0;
  var p1;
  var p2;
  var p3;

  if (isValueColor) {
    var rgba = [0, 0, 0, 0];
  }

  var onframe = function onframe(target, percent) {
    // Find the range keyframes
    // kf1-----kf2---------current--------kf3
    // find kf2 and kf3 and do interpolation
    var frame; // In the easing function like elasticOut, percent may less than 0

    if (percent < 0) {
      frame = 0;
    } else if (percent < lastFramePercent) {
      // Start from next key
      // PENDING start from lastFrame ?
      start = Math.min(lastFrame + 1, trackLen - 1);

      for (frame = start; frame >= 0; frame--) {
        if (kfPercents[frame] <= percent) {
          break;
        }
      } // PENDING really need to do this ?


      frame = Math.min(frame, trackLen - 2);
    } else {
      for (frame = lastFrame; frame < trackLen; frame++) {
        if (kfPercents[frame] > percent) {
          break;
        }
      }

      frame = Math.min(frame - 1, trackLen - 2);
    }

    lastFrame = frame;
    lastFramePercent = percent;
    var range = kfPercents[frame + 1] - kfPercents[frame];

    if (range === 0) {
      return;
    } else {
      w = (percent - kfPercents[frame]) / range;
    }

    if (useSpline) {
      p1 = kfValues[frame];
      p0 = kfValues[frame === 0 ? frame : frame - 1];
      p2 = kfValues[frame > trackLen - 2 ? trackLen - 1 : frame + 1];
      p3 = kfValues[frame > trackLen - 3 ? trackLen - 1 : frame + 2];

      if (isValueArray) {
        catmullRomInterpolateArray(p0, p1, p2, p3, w, w * w, w * w * w, getter(target, propName), arrDim);
      } else {
        var value;

        if (isValueColor) {
          value = catmullRomInterpolateArray(p0, p1, p2, p3, w, w * w, w * w * w, rgba, 1);
          value = rgba2String(rgba);
        } else if (isValueString) {
          // String is step(0.5)
          return interpolateString(p1, p2, w);
        } else {
          value = catmullRomInterpolate(p0, p1, p2, p3, w, w * w, w * w * w);
        }

        setter(target, propName, value);
      }
    } else {
      if (isValueArray) {
        interpolateArray(kfValues[frame], kfValues[frame + 1], w, getter(target, propName), arrDim);
      } else {
        var value;

        if (isValueColor) {
          interpolateArray(kfValues[frame], kfValues[frame + 1], w, rgba, 1);
          value = rgba2String(rgba);
        } else if (isValueString) {
          // String is step(0.5)
          return interpolateString(kfValues[frame], kfValues[frame + 1], w);
        } else {
          value = interpolateNumber(kfValues[frame], kfValues[frame + 1], w);
        }

        setter(target, propName, value);
      }
    }
  };

  var clip = new Clip({
    target: animator._target,
    life: trackMaxTime,
    loop: animator._loop,
    delay: animator._delay,
    onframe: onframe,
    ondestroy: oneTrackDone
  });

  if (easing && easing !== 'spline') {
    clip.easing = easing;
  }

  return clip;
}
/**
 * @alias module:zrender/animation/Animator
 * @constructor
 * @param {Object} target
 * @param {boolean} loop
 * @param {Function} getter
 * @param {Function} setter
 */


var Animator = function Animator(target, loop, getter, setter) {
  this._tracks = {};
  this._target = target;
  this._loop = loop || false;
  this._getter = getter || defaultGetter;
  this._setter = setter || defaultSetter;
  this._clipCount = 0;
  this._delay = 0;
  this._doneList = [];
  this._onframeList = [];
  this._clipList = [];
};

Animator.prototype = {
  /**
   * 设置动画关键帧
   * @param  {number} time 关键帧时间，单位是ms
   * @param  {Object} props 关键帧的属性值，key-value表示
   * @return {module:zrender/animation/Animator}
   */
  when: function when(time
  /* ms */
  , props) {
    var tracks = this._tracks;

    for (var propName in props) {
      if (!props.hasOwnProperty(propName)) {
        continue;
      }

      if (!tracks[propName]) {
        tracks[propName] = []; // Invalid value

        var value = this._getter(this._target, propName);

        if (value == null) {
          // zrLog('Invalid property ' + propName);
          continue;
        } // If time is 0
        //  Then props is given initialize value
        // Else
        //  Initialize value from current prop value


        if (time !== 0) {
          tracks[propName].push({
            time: 0,
            value: cloneValue(value)
          });
        }
      }

      tracks[propName].push({
        time: time,
        value: props[propName]
      });
    }

    return this;
  },

  /**
   * 添加动画每一帧的回调函数
   * @param  {Function} callback
   * @return {module:zrender/animation/Animator}
   */
  during: function during(callback) {
    this._onframeList.push(callback);

    return this;
  },
  pause: function pause() {
    for (var i = 0; i < this._clipList.length; i++) {
      this._clipList[i].pause();
    }

    this._paused = true;
  },
  resume: function resume() {
    for (var i = 0; i < this._clipList.length; i++) {
      this._clipList[i].resume();
    }

    this._paused = false;
  },
  isPaused: function isPaused() {
    return !!this._paused;
  },
  _doneCallback: function _doneCallback() {
    // Clear all tracks
    this._tracks = {}; // Clear all clips

    this._clipList.length = 0;
    var doneList = this._doneList;
    var len = doneList.length;

    for (var i = 0; i < len; i++) {
      doneList[i].call(this);
    }
  },

  /**
   * 开始执行动画
   * @param  {string|Function} [easing]
   *         动画缓动函数，详见{@link module:zrender/animation/easing}
   * @param  {boolean} forceAnimate
   * @return {module:zrender/animation/Animator}
   */
  start: function start(easing, forceAnimate) {
    var self = this;
    var clipCount = 0;

    var oneTrackDone = function oneTrackDone() {
      clipCount--;

      if (!clipCount) {
        self._doneCallback();
      }
    };

    var lastClip;

    for (var propName in this._tracks) {
      if (!this._tracks.hasOwnProperty(propName)) {
        continue;
      }

      var clip = createTrackClip(this, easing, oneTrackDone, this._tracks[propName], propName, forceAnimate);

      if (clip) {
        this._clipList.push(clip);

        clipCount++; // If start after added to animation

        if (this.animation) {
          this.animation.addClip(clip);
        }

        lastClip = clip;
      }
    } // Add during callback on the last clip


    if (lastClip) {
      var oldOnFrame = lastClip.onframe;

      lastClip.onframe = function (target, percent) {
        oldOnFrame(target, percent);

        for (var i = 0; i < self._onframeList.length; i++) {
          self._onframeList[i](target, percent);
        }
      };
    } // This optimization will help the case that in the upper application
    // the view may be refreshed frequently, where animation will be
    // called repeatly but nothing changed.


    if (!clipCount) {
      this._doneCallback();
    }

    return this;
  },

  /**
   * 停止动画
   * @param {boolean} forwardToLast If move to last frame before stop
   */
  stop: function stop(forwardToLast) {
    var clipList = this._clipList;
    var animation = this.animation;

    for (var i = 0; i < clipList.length; i++) {
      var clip = clipList[i];

      if (forwardToLast) {
        // Move to last frame before stop
        clip.onframe(this._target, 1);
      }

      animation && animation.removeClip(clip);
    }

    clipList.length = 0;
  },

  /**
   * 设置动画延迟开始的时间
   * @param  {number} time 单位ms
   * @return {module:zrender/animation/Animator}
   */
  delay: function delay(time) {
    this._delay = time;
    return this;
  },

  /**
   * 添加动画结束的回调
   * @param  {Function} cb
   * @return {module:zrender/animation/Animator}
   */
  done: function done(cb) {
    if (cb) {
      this._doneList.push(cb);
    }

    return this;
  },

  /**
   * @return {Array.<module:zrender/animation/Clip>}
   */
  getClips: function getClips() {
    return this._clipList;
  }
};
var _default = Animator;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvYW5pbWF0aW9uL0FuaW1hdG9yLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvYW5pbWF0aW9uL0FuaW1hdG9yLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBDbGlwID0gcmVxdWlyZShcIi4vQ2xpcFwiKTtcblxudmFyIGNvbG9yID0gcmVxdWlyZShcIi4uL3Rvb2wvY29sb3JcIik7XG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBpc0FycmF5TGlrZSA9IF91dGlsLmlzQXJyYXlMaWtlO1xuXG4vKipcbiAqIEBtb2R1bGUgZWNoYXJ0cy9hbmltYXRpb24vQW5pbWF0b3JcbiAqL1xudmFyIGFycmF5U2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG5cbmZ1bmN0aW9uIGRlZmF1bHRHZXR0ZXIodGFyZ2V0LCBrZXkpIHtcbiAgcmV0dXJuIHRhcmdldFtrZXldO1xufVxuXG5mdW5jdGlvbiBkZWZhdWx0U2V0dGVyKHRhcmdldCwga2V5LCB2YWx1ZSkge1xuICB0YXJnZXRba2V5XSA9IHZhbHVlO1xufVxuLyoqXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAwXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHBlcmNlbnRcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGludGVycG9sYXRlTnVtYmVyKHAwLCBwMSwgcGVyY2VudCkge1xuICByZXR1cm4gKHAxIC0gcDApICogcGVyY2VudCArIHAwO1xufVxuLyoqXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHAwXG4gKiBAcGFyYW0gIHtzdHJpbmd9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHBlcmNlbnRcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuXG5cbmZ1bmN0aW9uIGludGVycG9sYXRlU3RyaW5nKHAwLCBwMSwgcGVyY2VudCkge1xuICByZXR1cm4gcGVyY2VudCA+IDAuNSA/IHAxIDogcDA7XG59XG4vKipcbiAqIEBwYXJhbSAge0FycmF5fSBwMFxuICogQHBhcmFtICB7QXJyYXl9IHAxXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHBlcmNlbnRcbiAqIEBwYXJhbSAge0FycmF5fSBvdXRcbiAqIEBwYXJhbSAge251bWJlcn0gYXJyRGltXG4gKi9cblxuXG5mdW5jdGlvbiBpbnRlcnBvbGF0ZUFycmF5KHAwLCBwMSwgcGVyY2VudCwgb3V0LCBhcnJEaW0pIHtcbiAgdmFyIGxlbiA9IHAwLmxlbmd0aDtcblxuICBpZiAoYXJyRGltID09PSAxKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgb3V0W2ldID0gaW50ZXJwb2xhdGVOdW1iZXIocDBbaV0sIHAxW2ldLCBwZXJjZW50KTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIGxlbjIgPSBsZW4gJiYgcDBbMF0ubGVuZ3RoO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBsZW4yOyBqKyspIHtcbiAgICAgICAgb3V0W2ldW2pdID0gaW50ZXJwb2xhdGVOdW1iZXIocDBbaV1bal0sIHAxW2ldW2pdLCBwZXJjZW50KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn0gLy8gYXJyMCBpcyBzb3VyY2UgYXJyYXksIGFycjEgaXMgdGFyZ2V0IGFycmF5LlxuLy8gRG8gc29tZSBwcmVwcm9jZXNzIHRvIGF2b2lkIGVycm9yIGhhcHBlbmVkIHdoZW4gaW50ZXJwb2xhdGluZyBmcm9tIGFycjAgdG8gYXJyMVxuXG5cbmZ1bmN0aW9uIGZpbGxBcnIoYXJyMCwgYXJyMSwgYXJyRGltKSB7XG4gIHZhciBhcnIwTGVuID0gYXJyMC5sZW5ndGg7XG4gIHZhciBhcnIxTGVuID0gYXJyMS5sZW5ndGg7XG5cbiAgaWYgKGFycjBMZW4gIT09IGFycjFMZW4pIHtcbiAgICAvLyBGSVhNRSBOb3Qgd29yayBmb3IgVHlwZWRBcnJheVxuICAgIHZhciBpc1ByZXZpb3VzTGFyZ2VyID0gYXJyMExlbiA+IGFycjFMZW47XG5cbiAgICBpZiAoaXNQcmV2aW91c0xhcmdlcikge1xuICAgICAgLy8gQ3V0IHRoZSBwcmV2aW91c1xuICAgICAgYXJyMC5sZW5ndGggPSBhcnIxTGVuO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBGaWxsIHRoZSBwcmV2aW91c1xuICAgICAgZm9yICh2YXIgaSA9IGFycjBMZW47IGkgPCBhcnIxTGVuOyBpKyspIHtcbiAgICAgICAgYXJyMC5wdXNoKGFyckRpbSA9PT0gMSA/IGFycjFbaV0gOiBhcnJheVNsaWNlLmNhbGwoYXJyMVtpXSkpO1xuICAgICAgfVxuICAgIH1cbiAgfSAvLyBIYW5kbGluZyBOYU4gdmFsdWVcblxuXG4gIHZhciBsZW4yID0gYXJyMFswXSAmJiBhcnIwWzBdLmxlbmd0aDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGFycjAubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoYXJyRGltID09PSAxKSB7XG4gICAgICBpZiAoaXNOYU4oYXJyMFtpXSkpIHtcbiAgICAgICAgYXJyMFtpXSA9IGFycjFbaV07XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgbGVuMjsgaisrKSB7XG4gICAgICAgIGlmIChpc05hTihhcnIwW2ldW2pdKSkge1xuICAgICAgICAgIGFycjBbaV1bal0gPSBhcnIxW2ldW2pdO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4vKipcbiAqIEBwYXJhbSAge0FycmF5fSBhcnIwXG4gKiBAcGFyYW0gIHtBcnJheX0gYXJyMVxuICogQHBhcmFtICB7bnVtYmVyfSBhcnJEaW1cbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG5mdW5jdGlvbiBpc0FycmF5U2FtZShhcnIwLCBhcnIxLCBhcnJEaW0pIHtcbiAgaWYgKGFycjAgPT09IGFycjEpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHZhciBsZW4gPSBhcnIwLmxlbmd0aDtcblxuICBpZiAobGVuICE9PSBhcnIxLmxlbmd0aCkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGlmIChhcnJEaW0gPT09IDEpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICBpZiAoYXJyMFtpXSAhPT0gYXJyMVtpXSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHZhciBsZW4yID0gYXJyMFswXS5sZW5ndGg7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IGxlbjI7IGorKykge1xuICAgICAgICBpZiAoYXJyMFtpXVtqXSAhPT0gYXJyMVtpXVtqXSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufVxuLyoqXG4gKiBDYXRtdWxsIFJvbSBpbnRlcnBvbGF0ZSBhcnJheVxuICogQHBhcmFtICB7QXJyYXl9IHAwXG4gKiBAcGFyYW0gIHtBcnJheX0gcDFcbiAqIEBwYXJhbSAge0FycmF5fSBwMlxuICogQHBhcmFtICB7QXJyYXl9IHAzXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHRcbiAqIEBwYXJhbSAge251bWJlcn0gdDJcbiAqIEBwYXJhbSAge251bWJlcn0gdDNcbiAqIEBwYXJhbSAge0FycmF5fSBvdXRcbiAqIEBwYXJhbSAge251bWJlcn0gYXJyRGltXG4gKi9cblxuXG5mdW5jdGlvbiBjYXRtdWxsUm9tSW50ZXJwb2xhdGVBcnJheShwMCwgcDEsIHAyLCBwMywgdCwgdDIsIHQzLCBvdXQsIGFyckRpbSkge1xuICB2YXIgbGVuID0gcDAubGVuZ3RoO1xuXG4gIGlmIChhcnJEaW0gPT09IDEpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICBvdXRbaV0gPSBjYXRtdWxsUm9tSW50ZXJwb2xhdGUocDBbaV0sIHAxW2ldLCBwMltpXSwgcDNbaV0sIHQsIHQyLCB0Myk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHZhciBsZW4yID0gcDBbMF0ubGVuZ3RoO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCBsZW4yOyBqKyspIHtcbiAgICAgICAgb3V0W2ldW2pdID0gY2F0bXVsbFJvbUludGVycG9sYXRlKHAwW2ldW2pdLCBwMVtpXVtqXSwgcDJbaV1bal0sIHAzW2ldW2pdLCB0LCB0MiwgdDMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuLyoqXG4gKiBDYXRtdWxsIFJvbSBpbnRlcnBvbGF0ZSBudW1iZXJcbiAqIEBwYXJhbSAge251bWJlcn0gcDBcbiAqIEBwYXJhbSAge251bWJlcn0gcDFcbiAqIEBwYXJhbSAge251bWJlcn0gcDJcbiAqIEBwYXJhbSAge251bWJlcn0gcDNcbiAqIEBwYXJhbSAge251bWJlcn0gdFxuICogQHBhcmFtICB7bnVtYmVyfSB0MlxuICogQHBhcmFtICB7bnVtYmVyfSB0M1xuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxuZnVuY3Rpb24gY2F0bXVsbFJvbUludGVycG9sYXRlKHAwLCBwMSwgcDIsIHAzLCB0LCB0MiwgdDMpIHtcbiAgdmFyIHYwID0gKHAyIC0gcDApICogMC41O1xuICB2YXIgdjEgPSAocDMgLSBwMSkgKiAwLjU7XG4gIHJldHVybiAoMiAqIChwMSAtIHAyKSArIHYwICsgdjEpICogdDMgKyAoLTMgKiAocDEgLSBwMikgLSAyICogdjAgLSB2MSkgKiB0MiArIHYwICogdCArIHAxO1xufVxuXG5mdW5jdGlvbiBjbG9uZVZhbHVlKHZhbHVlKSB7XG4gIGlmIChpc0FycmF5TGlrZSh2YWx1ZSkpIHtcbiAgICB2YXIgbGVuID0gdmFsdWUubGVuZ3RoO1xuXG4gICAgaWYgKGlzQXJyYXlMaWtlKHZhbHVlWzBdKSkge1xuICAgICAgdmFyIHJldCA9IFtdO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIHJldC5wdXNoKGFycmF5U2xpY2UuY2FsbCh2YWx1ZVtpXSkpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmV0O1xuICAgIH1cblxuICAgIHJldHVybiBhcnJheVNsaWNlLmNhbGwodmFsdWUpO1xuICB9XG5cbiAgcmV0dXJuIHZhbHVlO1xufVxuXG5mdW5jdGlvbiByZ2JhMlN0cmluZyhyZ2JhKSB7XG4gIHJnYmFbMF0gPSBNYXRoLmZsb29yKHJnYmFbMF0pO1xuICByZ2JhWzFdID0gTWF0aC5mbG9vcihyZ2JhWzFdKTtcbiAgcmdiYVsyXSA9IE1hdGguZmxvb3IocmdiYVsyXSk7XG4gIHJldHVybiAncmdiYSgnICsgcmdiYS5qb2luKCcsJykgKyAnKSc7XG59XG5cbmZ1bmN0aW9uIGdldEFycmF5RGltKGtleWZyYW1lcykge1xuICB2YXIgbGFzdFZhbHVlID0ga2V5ZnJhbWVzW2tleWZyYW1lcy5sZW5ndGggLSAxXS52YWx1ZTtcbiAgcmV0dXJuIGlzQXJyYXlMaWtlKGxhc3RWYWx1ZSAmJiBsYXN0VmFsdWVbMF0pID8gMiA6IDE7XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVRyYWNrQ2xpcChhbmltYXRvciwgZWFzaW5nLCBvbmVUcmFja0RvbmUsIGtleWZyYW1lcywgcHJvcE5hbWUsIGZvcmNlQW5pbWF0ZSkge1xuICB2YXIgZ2V0dGVyID0gYW5pbWF0b3IuX2dldHRlcjtcbiAgdmFyIHNldHRlciA9IGFuaW1hdG9yLl9zZXR0ZXI7XG4gIHZhciB1c2VTcGxpbmUgPSBlYXNpbmcgPT09ICdzcGxpbmUnO1xuICB2YXIgdHJhY2tMZW4gPSBrZXlmcmFtZXMubGVuZ3RoO1xuXG4gIGlmICghdHJhY2tMZW4pIHtcbiAgICByZXR1cm47XG4gIH0gLy8gR3Vlc3MgZGF0YSB0eXBlXG5cblxuICB2YXIgZmlyc3RWYWwgPSBrZXlmcmFtZXNbMF0udmFsdWU7XG4gIHZhciBpc1ZhbHVlQXJyYXkgPSBpc0FycmF5TGlrZShmaXJzdFZhbCk7XG4gIHZhciBpc1ZhbHVlQ29sb3IgPSBmYWxzZTtcbiAgdmFyIGlzVmFsdWVTdHJpbmcgPSBmYWxzZTsgLy8gRm9yIHZlcnRpY2VzIG1vcnBoaW5nXG5cbiAgdmFyIGFyckRpbSA9IGlzVmFsdWVBcnJheSA/IGdldEFycmF5RGltKGtleWZyYW1lcykgOiAwO1xuICB2YXIgdHJhY2tNYXhUaW1lOyAvLyBTb3J0IGtleWZyYW1lIGFzIGFzY2VuZGluZ1xuXG4gIGtleWZyYW1lcy5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgcmV0dXJuIGEudGltZSAtIGIudGltZTtcbiAgfSk7XG4gIHRyYWNrTWF4VGltZSA9IGtleWZyYW1lc1t0cmFja0xlbiAtIDFdLnRpbWU7IC8vIFBlcmNlbnRzIG9mIGVhY2gga2V5ZnJhbWVcblxuICB2YXIga2ZQZXJjZW50cyA9IFtdOyAvLyBWYWx1ZSBvZiBlYWNoIGtleWZyYW1lXG5cbiAgdmFyIGtmVmFsdWVzID0gW107XG4gIHZhciBwcmV2VmFsdWUgPSBrZXlmcmFtZXNbMF0udmFsdWU7XG4gIHZhciBpc0FsbFZhbHVlRXF1YWwgPSB0cnVlO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdHJhY2tMZW47IGkrKykge1xuICAgIGtmUGVyY2VudHMucHVzaChrZXlmcmFtZXNbaV0udGltZSAvIHRyYWNrTWF4VGltZSk7IC8vIEFzc3VtZSB2YWx1ZSBpcyBhIGNvbG9yIHdoZW4gaXQgaXMgYSBzdHJpbmdcblxuICAgIHZhciB2YWx1ZSA9IGtleWZyYW1lc1tpXS52YWx1ZTsgLy8gQ2hlY2sgaWYgdmFsdWUgaXMgZXF1YWwsIGRlZXAgY2hlY2sgaWYgdmFsdWUgaXMgYXJyYXlcblxuICAgIGlmICghKGlzVmFsdWVBcnJheSAmJiBpc0FycmF5U2FtZSh2YWx1ZSwgcHJldlZhbHVlLCBhcnJEaW0pIHx8ICFpc1ZhbHVlQXJyYXkgJiYgdmFsdWUgPT09IHByZXZWYWx1ZSkpIHtcbiAgICAgIGlzQWxsVmFsdWVFcXVhbCA9IGZhbHNlO1xuICAgIH1cblxuICAgIHByZXZWYWx1ZSA9IHZhbHVlOyAvLyBUcnkgY29udmVydGluZyBhIHN0cmluZyB0byBhIGNvbG9yIGFycmF5XG5cbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgdmFyIGNvbG9yQXJyYXkgPSBjb2xvci5wYXJzZSh2YWx1ZSk7XG5cbiAgICAgIGlmIChjb2xvckFycmF5KSB7XG4gICAgICAgIHZhbHVlID0gY29sb3JBcnJheTtcbiAgICAgICAgaXNWYWx1ZUNvbG9yID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlzVmFsdWVTdHJpbmcgPSB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIGtmVmFsdWVzLnB1c2godmFsdWUpO1xuICB9XG5cbiAgaWYgKCFmb3JjZUFuaW1hdGUgJiYgaXNBbGxWYWx1ZUVxdWFsKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGxhc3RWYWx1ZSA9IGtmVmFsdWVzW3RyYWNrTGVuIC0gMV07IC8vIFBvbHlmaWxsIGFycmF5IGFuZCBOYU4gdmFsdWVcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRyYWNrTGVuIC0gMTsgaSsrKSB7XG4gICAgaWYgKGlzVmFsdWVBcnJheSkge1xuICAgICAgZmlsbEFycihrZlZhbHVlc1tpXSwgbGFzdFZhbHVlLCBhcnJEaW0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoaXNOYU4oa2ZWYWx1ZXNbaV0pICYmICFpc05hTihsYXN0VmFsdWUpICYmICFpc1ZhbHVlU3RyaW5nICYmICFpc1ZhbHVlQ29sb3IpIHtcbiAgICAgICAga2ZWYWx1ZXNbaV0gPSBsYXN0VmFsdWU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaXNWYWx1ZUFycmF5ICYmIGZpbGxBcnIoZ2V0dGVyKGFuaW1hdG9yLl90YXJnZXQsIHByb3BOYW1lKSwgbGFzdFZhbHVlLCBhcnJEaW0pOyAvLyBDYWNoZSB0aGUga2V5IG9mIGxhc3QgZnJhbWUgdG8gc3BlZWQgdXAgd2hlblxuICAvLyBhbmltYXRpb24gcGxheWJhY2sgaXMgc2VxdWVuY3lcblxuICB2YXIgbGFzdEZyYW1lID0gMDtcbiAgdmFyIGxhc3RGcmFtZVBlcmNlbnQgPSAwO1xuICB2YXIgc3RhcnQ7XG4gIHZhciB3O1xuICB2YXIgcDA7XG4gIHZhciBwMTtcbiAgdmFyIHAyO1xuICB2YXIgcDM7XG5cbiAgaWYgKGlzVmFsdWVDb2xvcikge1xuICAgIHZhciByZ2JhID0gWzAsIDAsIDAsIDBdO1xuICB9XG5cbiAgdmFyIG9uZnJhbWUgPSBmdW5jdGlvbiAodGFyZ2V0LCBwZXJjZW50KSB7XG4gICAgLy8gRmluZCB0aGUgcmFuZ2Uga2V5ZnJhbWVzXG4gICAgLy8ga2YxLS0tLS1rZjItLS0tLS0tLS1jdXJyZW50LS0tLS0tLS1rZjNcbiAgICAvLyBmaW5kIGtmMiBhbmQga2YzIGFuZCBkbyBpbnRlcnBvbGF0aW9uXG4gICAgdmFyIGZyYW1lOyAvLyBJbiB0aGUgZWFzaW5nIGZ1bmN0aW9uIGxpa2UgZWxhc3RpY091dCwgcGVyY2VudCBtYXkgbGVzcyB0aGFuIDBcblxuICAgIGlmIChwZXJjZW50IDwgMCkge1xuICAgICAgZnJhbWUgPSAwO1xuICAgIH0gZWxzZSBpZiAocGVyY2VudCA8IGxhc3RGcmFtZVBlcmNlbnQpIHtcbiAgICAgIC8vIFN0YXJ0IGZyb20gbmV4dCBrZXlcbiAgICAgIC8vIFBFTkRJTkcgc3RhcnQgZnJvbSBsYXN0RnJhbWUgP1xuICAgICAgc3RhcnQgPSBNYXRoLm1pbihsYXN0RnJhbWUgKyAxLCB0cmFja0xlbiAtIDEpO1xuXG4gICAgICBmb3IgKGZyYW1lID0gc3RhcnQ7IGZyYW1lID49IDA7IGZyYW1lLS0pIHtcbiAgICAgICAgaWYgKGtmUGVyY2VudHNbZnJhbWVdIDw9IHBlcmNlbnQpIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfSAvLyBQRU5ESU5HIHJlYWxseSBuZWVkIHRvIGRvIHRoaXMgP1xuXG5cbiAgICAgIGZyYW1lID0gTWF0aC5taW4oZnJhbWUsIHRyYWNrTGVuIC0gMik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAoZnJhbWUgPSBsYXN0RnJhbWU7IGZyYW1lIDwgdHJhY2tMZW47IGZyYW1lKyspIHtcbiAgICAgICAgaWYgKGtmUGVyY2VudHNbZnJhbWVdID4gcGVyY2VudCkge1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGZyYW1lID0gTWF0aC5taW4oZnJhbWUgLSAxLCB0cmFja0xlbiAtIDIpO1xuICAgIH1cblxuICAgIGxhc3RGcmFtZSA9IGZyYW1lO1xuICAgIGxhc3RGcmFtZVBlcmNlbnQgPSBwZXJjZW50O1xuICAgIHZhciByYW5nZSA9IGtmUGVyY2VudHNbZnJhbWUgKyAxXSAtIGtmUGVyY2VudHNbZnJhbWVdO1xuXG4gICAgaWYgKHJhbmdlID09PSAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfSBlbHNlIHtcbiAgICAgIHcgPSAocGVyY2VudCAtIGtmUGVyY2VudHNbZnJhbWVdKSAvIHJhbmdlO1xuICAgIH1cblxuICAgIGlmICh1c2VTcGxpbmUpIHtcbiAgICAgIHAxID0ga2ZWYWx1ZXNbZnJhbWVdO1xuICAgICAgcDAgPSBrZlZhbHVlc1tmcmFtZSA9PT0gMCA/IGZyYW1lIDogZnJhbWUgLSAxXTtcbiAgICAgIHAyID0ga2ZWYWx1ZXNbZnJhbWUgPiB0cmFja0xlbiAtIDIgPyB0cmFja0xlbiAtIDEgOiBmcmFtZSArIDFdO1xuICAgICAgcDMgPSBrZlZhbHVlc1tmcmFtZSA+IHRyYWNrTGVuIC0gMyA/IHRyYWNrTGVuIC0gMSA6IGZyYW1lICsgMl07XG5cbiAgICAgIGlmIChpc1ZhbHVlQXJyYXkpIHtcbiAgICAgICAgY2F0bXVsbFJvbUludGVycG9sYXRlQXJyYXkocDAsIHAxLCBwMiwgcDMsIHcsIHcgKiB3LCB3ICogdyAqIHcsIGdldHRlcih0YXJnZXQsIHByb3BOYW1lKSwgYXJyRGltKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciB2YWx1ZTtcblxuICAgICAgICBpZiAoaXNWYWx1ZUNvbG9yKSB7XG4gICAgICAgICAgdmFsdWUgPSBjYXRtdWxsUm9tSW50ZXJwb2xhdGVBcnJheShwMCwgcDEsIHAyLCBwMywgdywgdyAqIHcsIHcgKiB3ICogdywgcmdiYSwgMSk7XG4gICAgICAgICAgdmFsdWUgPSByZ2JhMlN0cmluZyhyZ2JhKTtcbiAgICAgICAgfSBlbHNlIGlmIChpc1ZhbHVlU3RyaW5nKSB7XG4gICAgICAgICAgLy8gU3RyaW5nIGlzIHN0ZXAoMC41KVxuICAgICAgICAgIHJldHVybiBpbnRlcnBvbGF0ZVN0cmluZyhwMSwgcDIsIHcpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHZhbHVlID0gY2F0bXVsbFJvbUludGVycG9sYXRlKHAwLCBwMSwgcDIsIHAzLCB3LCB3ICogdywgdyAqIHcgKiB3KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNldHRlcih0YXJnZXQsIHByb3BOYW1lLCB2YWx1ZSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChpc1ZhbHVlQXJyYXkpIHtcbiAgICAgICAgaW50ZXJwb2xhdGVBcnJheShrZlZhbHVlc1tmcmFtZV0sIGtmVmFsdWVzW2ZyYW1lICsgMV0sIHcsIGdldHRlcih0YXJnZXQsIHByb3BOYW1lKSwgYXJyRGltKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciB2YWx1ZTtcblxuICAgICAgICBpZiAoaXNWYWx1ZUNvbG9yKSB7XG4gICAgICAgICAgaW50ZXJwb2xhdGVBcnJheShrZlZhbHVlc1tmcmFtZV0sIGtmVmFsdWVzW2ZyYW1lICsgMV0sIHcsIHJnYmEsIDEpO1xuICAgICAgICAgIHZhbHVlID0gcmdiYTJTdHJpbmcocmdiYSk7XG4gICAgICAgIH0gZWxzZSBpZiAoaXNWYWx1ZVN0cmluZykge1xuICAgICAgICAgIC8vIFN0cmluZyBpcyBzdGVwKDAuNSlcbiAgICAgICAgICByZXR1cm4gaW50ZXJwb2xhdGVTdHJpbmcoa2ZWYWx1ZXNbZnJhbWVdLCBrZlZhbHVlc1tmcmFtZSArIDFdLCB3KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWx1ZSA9IGludGVycG9sYXRlTnVtYmVyKGtmVmFsdWVzW2ZyYW1lXSwga2ZWYWx1ZXNbZnJhbWUgKyAxXSwgdyk7XG4gICAgICAgIH1cblxuICAgICAgICBzZXR0ZXIodGFyZ2V0LCBwcm9wTmFtZSwgdmFsdWUpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICB2YXIgY2xpcCA9IG5ldyBDbGlwKHtcbiAgICB0YXJnZXQ6IGFuaW1hdG9yLl90YXJnZXQsXG4gICAgbGlmZTogdHJhY2tNYXhUaW1lLFxuICAgIGxvb3A6IGFuaW1hdG9yLl9sb29wLFxuICAgIGRlbGF5OiBhbmltYXRvci5fZGVsYXksXG4gICAgb25mcmFtZTogb25mcmFtZSxcbiAgICBvbmRlc3Ryb3k6IG9uZVRyYWNrRG9uZVxuICB9KTtcblxuICBpZiAoZWFzaW5nICYmIGVhc2luZyAhPT0gJ3NwbGluZScpIHtcbiAgICBjbGlwLmVhc2luZyA9IGVhc2luZztcbiAgfVxuXG4gIHJldHVybiBjbGlwO1xufVxuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOnpyZW5kZXIvYW5pbWF0aW9uL0FuaW1hdG9yXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7T2JqZWN0fSB0YXJnZXRcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gbG9vcFxuICogQHBhcmFtIHtGdW5jdGlvbn0gZ2V0dGVyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBzZXR0ZXJcbiAqL1xuXG5cbnZhciBBbmltYXRvciA9IGZ1bmN0aW9uICh0YXJnZXQsIGxvb3AsIGdldHRlciwgc2V0dGVyKSB7XG4gIHRoaXMuX3RyYWNrcyA9IHt9O1xuICB0aGlzLl90YXJnZXQgPSB0YXJnZXQ7XG4gIHRoaXMuX2xvb3AgPSBsb29wIHx8IGZhbHNlO1xuICB0aGlzLl9nZXR0ZXIgPSBnZXR0ZXIgfHwgZGVmYXVsdEdldHRlcjtcbiAgdGhpcy5fc2V0dGVyID0gc2V0dGVyIHx8IGRlZmF1bHRTZXR0ZXI7XG4gIHRoaXMuX2NsaXBDb3VudCA9IDA7XG4gIHRoaXMuX2RlbGF5ID0gMDtcbiAgdGhpcy5fZG9uZUxpc3QgPSBbXTtcbiAgdGhpcy5fb25mcmFtZUxpc3QgPSBbXTtcbiAgdGhpcy5fY2xpcExpc3QgPSBbXTtcbn07XG5cbkFuaW1hdG9yLnByb3RvdHlwZSA9IHtcbiAgLyoqXG4gICAqIOiuvue9ruWKqOeUu+WFs+mUruW4p1xuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHRpbWUg5YWz6ZSu5bin5pe26Ze077yM5Y2V5L2N5pivbXNcbiAgICogQHBhcmFtICB7T2JqZWN0fSBwcm9wcyDlhbPplK7luKfnmoTlsZ7mgKflgLzvvIxrZXktdmFsdWXooajnpLpcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvYW5pbWF0aW9uL0FuaW1hdG9yfVxuICAgKi9cbiAgd2hlbjogZnVuY3Rpb24gKHRpbWVcbiAgLyogbXMgKi9cbiAgLCBwcm9wcykge1xuICAgIHZhciB0cmFja3MgPSB0aGlzLl90cmFja3M7XG5cbiAgICBmb3IgKHZhciBwcm9wTmFtZSBpbiBwcm9wcykge1xuICAgICAgaWYgKCFwcm9wcy5oYXNPd25Qcm9wZXJ0eShwcm9wTmFtZSkpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGlmICghdHJhY2tzW3Byb3BOYW1lXSkge1xuICAgICAgICB0cmFja3NbcHJvcE5hbWVdID0gW107IC8vIEludmFsaWQgdmFsdWVcblxuICAgICAgICB2YXIgdmFsdWUgPSB0aGlzLl9nZXR0ZXIodGhpcy5fdGFyZ2V0LCBwcm9wTmFtZSk7XG5cbiAgICAgICAgaWYgKHZhbHVlID09IG51bGwpIHtcbiAgICAgICAgICAvLyB6ckxvZygnSW52YWxpZCBwcm9wZXJ0eSAnICsgcHJvcE5hbWUpO1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9IC8vIElmIHRpbWUgaXMgMFxuICAgICAgICAvLyAgVGhlbiBwcm9wcyBpcyBnaXZlbiBpbml0aWFsaXplIHZhbHVlXG4gICAgICAgIC8vIEVsc2VcbiAgICAgICAgLy8gIEluaXRpYWxpemUgdmFsdWUgZnJvbSBjdXJyZW50IHByb3AgdmFsdWVcblxuXG4gICAgICAgIGlmICh0aW1lICE9PSAwKSB7XG4gICAgICAgICAgdHJhY2tzW3Byb3BOYW1lXS5wdXNoKHtcbiAgICAgICAgICAgIHRpbWU6IDAsXG4gICAgICAgICAgICB2YWx1ZTogY2xvbmVWYWx1ZSh2YWx1ZSlcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB0cmFja3NbcHJvcE5hbWVdLnB1c2goe1xuICAgICAgICB0aW1lOiB0aW1lLFxuICAgICAgICB2YWx1ZTogcHJvcHNbcHJvcE5hbWVdXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICog5re75Yqg5Yqo55S75q+P5LiA5bin55qE5Zue6LCD5Ye95pWwXG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBjYWxsYmFja1xuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9hbmltYXRpb24vQW5pbWF0b3J9XG4gICAqL1xuICBkdXJpbmc6IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgIHRoaXMuX29uZnJhbWVMaXN0LnB1c2goY2FsbGJhY2spO1xuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG4gIHBhdXNlOiBmdW5jdGlvbiAoKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl9jbGlwTGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgdGhpcy5fY2xpcExpc3RbaV0ucGF1c2UoKTtcbiAgICB9XG5cbiAgICB0aGlzLl9wYXVzZWQgPSB0cnVlO1xuICB9LFxuICByZXN1bWU6IGZ1bmN0aW9uICgpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuX2NsaXBMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICB0aGlzLl9jbGlwTGlzdFtpXS5yZXN1bWUoKTtcbiAgICB9XG5cbiAgICB0aGlzLl9wYXVzZWQgPSBmYWxzZTtcbiAgfSxcbiAgaXNQYXVzZWQ6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gISF0aGlzLl9wYXVzZWQ7XG4gIH0sXG4gIF9kb25lQ2FsbGJhY2s6IGZ1bmN0aW9uICgpIHtcbiAgICAvLyBDbGVhciBhbGwgdHJhY2tzXG4gICAgdGhpcy5fdHJhY2tzID0ge307IC8vIENsZWFyIGFsbCBjbGlwc1xuXG4gICAgdGhpcy5fY2xpcExpc3QubGVuZ3RoID0gMDtcbiAgICB2YXIgZG9uZUxpc3QgPSB0aGlzLl9kb25lTGlzdDtcbiAgICB2YXIgbGVuID0gZG9uZUxpc3QubGVuZ3RoO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgICAgZG9uZUxpc3RbaV0uY2FsbCh0aGlzKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIOW8gOWni+aJp+ihjOWKqOeUu1xuICAgKiBAcGFyYW0gIHtzdHJpbmd8RnVuY3Rpb259IFtlYXNpbmddXG4gICAqICAgICAgICAg5Yqo55S757yT5Yqo5Ye95pWw77yM6K+m6KeBe0BsaW5rIG1vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9lYXNpbmd9XG4gICAqIEBwYXJhbSAge2Jvb2xlYW59IGZvcmNlQW5pbWF0ZVxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9hbmltYXRpb24vQW5pbWF0b3J9XG4gICAqL1xuICBzdGFydDogZnVuY3Rpb24gKGVhc2luZywgZm9yY2VBbmltYXRlKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHZhciBjbGlwQ291bnQgPSAwO1xuXG4gICAgdmFyIG9uZVRyYWNrRG9uZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGNsaXBDb3VudC0tO1xuXG4gICAgICBpZiAoIWNsaXBDb3VudCkge1xuICAgICAgICBzZWxmLl9kb25lQ2FsbGJhY2soKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIGxhc3RDbGlwO1xuXG4gICAgZm9yICh2YXIgcHJvcE5hbWUgaW4gdGhpcy5fdHJhY2tzKSB7XG4gICAgICBpZiAoIXRoaXMuX3RyYWNrcy5oYXNPd25Qcm9wZXJ0eShwcm9wTmFtZSkpIHtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBjbGlwID0gY3JlYXRlVHJhY2tDbGlwKHRoaXMsIGVhc2luZywgb25lVHJhY2tEb25lLCB0aGlzLl90cmFja3NbcHJvcE5hbWVdLCBwcm9wTmFtZSwgZm9yY2VBbmltYXRlKTtcblxuICAgICAgaWYgKGNsaXApIHtcbiAgICAgICAgdGhpcy5fY2xpcExpc3QucHVzaChjbGlwKTtcblxuICAgICAgICBjbGlwQ291bnQrKzsgLy8gSWYgc3RhcnQgYWZ0ZXIgYWRkZWQgdG8gYW5pbWF0aW9uXG5cbiAgICAgICAgaWYgKHRoaXMuYW5pbWF0aW9uKSB7XG4gICAgICAgICAgdGhpcy5hbmltYXRpb24uYWRkQ2xpcChjbGlwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGxhc3RDbGlwID0gY2xpcDtcbiAgICAgIH1cbiAgICB9IC8vIEFkZCBkdXJpbmcgY2FsbGJhY2sgb24gdGhlIGxhc3QgY2xpcFxuXG5cbiAgICBpZiAobGFzdENsaXApIHtcbiAgICAgIHZhciBvbGRPbkZyYW1lID0gbGFzdENsaXAub25mcmFtZTtcblxuICAgICAgbGFzdENsaXAub25mcmFtZSA9IGZ1bmN0aW9uICh0YXJnZXQsIHBlcmNlbnQpIHtcbiAgICAgICAgb2xkT25GcmFtZSh0YXJnZXQsIHBlcmNlbnQpO1xuXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2VsZi5fb25mcmFtZUxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICBzZWxmLl9vbmZyYW1lTGlzdFtpXSh0YXJnZXQsIHBlcmNlbnQpO1xuICAgICAgICB9XG4gICAgICB9O1xuICAgIH0gLy8gVGhpcyBvcHRpbWl6YXRpb24gd2lsbCBoZWxwIHRoZSBjYXNlIHRoYXQgaW4gdGhlIHVwcGVyIGFwcGxpY2F0aW9uXG4gICAgLy8gdGhlIHZpZXcgbWF5IGJlIHJlZnJlc2hlZCBmcmVxdWVudGx5LCB3aGVyZSBhbmltYXRpb24gd2lsbCBiZVxuICAgIC8vIGNhbGxlZCByZXBlYXRseSBidXQgbm90aGluZyBjaGFuZ2VkLlxuXG5cbiAgICBpZiAoIWNsaXBDb3VudCkge1xuICAgICAgdGhpcy5fZG9uZUNhbGxiYWNrKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOWBnOatouWKqOeUu1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGZvcndhcmRUb0xhc3QgSWYgbW92ZSB0byBsYXN0IGZyYW1lIGJlZm9yZSBzdG9wXG4gICAqL1xuICBzdG9wOiBmdW5jdGlvbiAoZm9yd2FyZFRvTGFzdCkge1xuICAgIHZhciBjbGlwTGlzdCA9IHRoaXMuX2NsaXBMaXN0O1xuICAgIHZhciBhbmltYXRpb24gPSB0aGlzLmFuaW1hdGlvbjtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2xpcExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBjbGlwID0gY2xpcExpc3RbaV07XG5cbiAgICAgIGlmIChmb3J3YXJkVG9MYXN0KSB7XG4gICAgICAgIC8vIE1vdmUgdG8gbGFzdCBmcmFtZSBiZWZvcmUgc3RvcFxuICAgICAgICBjbGlwLm9uZnJhbWUodGhpcy5fdGFyZ2V0LCAxKTtcbiAgICAgIH1cblxuICAgICAgYW5pbWF0aW9uICYmIGFuaW1hdGlvbi5yZW1vdmVDbGlwKGNsaXApO1xuICAgIH1cblxuICAgIGNsaXBMaXN0Lmxlbmd0aCA9IDA7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOiuvue9ruWKqOeUu+W7tui/n+W8gOWni+eahOaXtumXtFxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHRpbWUg5Y2V5L2NbXNcbiAgICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvYW5pbWF0aW9uL0FuaW1hdG9yfVxuICAgKi9cbiAgZGVsYXk6IGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgdGhpcy5fZGVsYXkgPSB0aW1lO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuXG4gIC8qKlxuICAgKiDmt7vliqDliqjnlLvnu5PmnZ/nmoTlm57osINcbiAgICogQHBhcmFtICB7RnVuY3Rpb259IGNiXG4gICAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2FuaW1hdGlvbi9BbmltYXRvcn1cbiAgICovXG4gIGRvbmU6IGZ1bmN0aW9uIChjYikge1xuICAgIGlmIChjYikge1xuICAgICAgdGhpcy5fZG9uZUxpc3QucHVzaChjYik7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEByZXR1cm4ge0FycmF5Ljxtb2R1bGU6enJlbmRlci9hbmltYXRpb24vQ2xpcD59XG4gICAqL1xuICBnZXRDbGlwczogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9jbGlwTGlzdDtcbiAgfVxufTtcbnZhciBfZGVmYXVsdCA9IEFuaW1hdG9yO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQTFNQTtBQTRNQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/animation/Animator.js
