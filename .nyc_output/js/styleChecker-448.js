__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isFlexSupported", function() { return isFlexSupported; });
var isStyleSupport = function isStyleSupport(styleName) {
  if (typeof window !== 'undefined' && window.document && window.document.documentElement) {
    var styleNameList = Array.isArray(styleName) ? styleName : [styleName];
    var documentElement = window.document.documentElement;
    return styleNameList.some(function (name) {
      return name in documentElement.style;
    });
  }

  return false;
};

var isFlexSupported = isStyleSupport(['flex', 'webkitFlex', 'Flex', 'msFlex']);
/* harmony default export */ __webpack_exports__["default"] = (isStyleSupport);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9zdHlsZUNoZWNrZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL191dGlsL3N0eWxlQ2hlY2tlci5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgaXNTdHlsZVN1cHBvcnQgPSAoc3R5bGVOYW1lKSA9PiB7XG4gICAgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCAmJiB3aW5kb3cuZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgICAgIGNvbnN0IHN0eWxlTmFtZUxpc3QgPSBBcnJheS5pc0FycmF5KHN0eWxlTmFtZSkgPyBzdHlsZU5hbWUgOiBbc3R5bGVOYW1lXTtcbiAgICAgICAgY29uc3QgeyBkb2N1bWVudEVsZW1lbnQgfSA9IHdpbmRvdy5kb2N1bWVudDtcbiAgICAgICAgcmV0dXJuIHN0eWxlTmFtZUxpc3Quc29tZShuYW1lID0+IG5hbWUgaW4gZG9jdW1lbnRFbGVtZW50LnN0eWxlKTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xufTtcbmV4cG9ydCBjb25zdCBpc0ZsZXhTdXBwb3J0ZWQgPSBpc1N0eWxlU3VwcG9ydChbJ2ZsZXgnLCAnd2Via2l0RmxleCcsICdGbGV4JywgJ21zRmxleCddKTtcbmV4cG9ydCBkZWZhdWx0IGlzU3R5bGVTdXBwb3J0O1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFOQTtBQUNBO0FBT0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/styleChecker.js
