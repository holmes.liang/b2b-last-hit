__webpack_require__.r(__webpack_exports__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/rc-editor-core/node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}



var ConfigStore = function () {
  function ConfigStore() {
    _classCallCheck(this, ConfigStore);

    this._store = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])();
  }

  ConfigStore.prototype.set = function set(key, value) {
    this._store = this._store.set(key, value);
  };

  ConfigStore.prototype.get = function get(key) {
    return this._store.get(key);
  };

  return ConfigStore;
}();

/* harmony default export */ __webpack_exports__["default"] = (ConfigStore);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvRWRpdG9yQ29yZS9Db25maWdTdG9yZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1jb3JlL2VzL0VkaXRvckNvcmUvQ29uZmlnU3RvcmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuaW1wb3J0IHsgTWFwIH0gZnJvbSAnaW1tdXRhYmxlJztcblxudmFyIENvbmZpZ1N0b3JlID0gZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIENvbmZpZ1N0b3JlKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29uZmlnU3RvcmUpO1xuXG4gICAgICAgIHRoaXMuX3N0b3JlID0gTWFwKCk7XG4gICAgfVxuXG4gICAgQ29uZmlnU3RvcmUucHJvdG90eXBlLnNldCA9IGZ1bmN0aW9uIHNldChrZXksIHZhbHVlKSB7XG4gICAgICAgIHRoaXMuX3N0b3JlID0gdGhpcy5fc3RvcmUuc2V0KGtleSwgdmFsdWUpO1xuICAgIH07XG5cbiAgICBDb25maWdTdG9yZS5wcm90b3R5cGUuZ2V0ID0gZnVuY3Rpb24gZ2V0KGtleSkge1xuICAgICAgICByZXR1cm4gdGhpcy5fc3RvcmUuZ2V0KGtleSk7XG4gICAgfTtcblxuICAgIHJldHVybiBDb25maWdTdG9yZTtcbn0oKTtcblxuZXhwb3J0IGRlZmF1bHQgQ29uZmlnU3RvcmU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/EditorCore/ConfigStore.js
