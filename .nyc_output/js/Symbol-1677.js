/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var _symbol = __webpack_require__(/*! ../../util/symbol */ "./node_modules/echarts/lib/util/symbol.js");

var createSymbol = _symbol.createSymbol;

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var _number = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var parsePercent = _number.parsePercent;

var _labelHelper = __webpack_require__(/*! ./labelHelper */ "./node_modules/echarts/lib/chart/helper/labelHelper.js");

var getDefaultLabel = _labelHelper.getDefaultLabel;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @module echarts/chart/helper/Symbol
 */

/**
 * @constructor
 * @alias {module:echarts/chart/helper/Symbol}
 * @param {module:echarts/data/List} data
 * @param {number} idx
 * @extends {module:zrender/graphic/Group}
 */

function SymbolClz(data, idx, seriesScope) {
  graphic.Group.call(this);
  this.updateData(data, idx, seriesScope);
}

var symbolProto = SymbolClz.prototype;
/**
 * @public
 * @static
 * @param {module:echarts/data/List} data
 * @param {number} dataIndex
 * @return {Array.<number>} [width, height]
 */

var getSymbolSize = SymbolClz.getSymbolSize = function (data, idx) {
  var symbolSize = data.getItemVisual(idx, 'symbolSize');
  return symbolSize instanceof Array ? symbolSize.slice() : [+symbolSize, +symbolSize];
};

function getScale(symbolSize) {
  return [symbolSize[0] / 2, symbolSize[1] / 2];
}

function driftSymbol(dx, dy) {
  this.parent.drift(dx, dy);
}

symbolProto._createSymbol = function (symbolType, data, idx, symbolSize, keepAspect) {
  // Remove paths created before
  this.removeAll();
  var color = data.getItemVisual(idx, 'color'); // var symbolPath = createSymbol(
  //     symbolType, -0.5, -0.5, 1, 1, color
  // );
  // If width/height are set too small (e.g., set to 1) on ios10
  // and macOS Sierra, a circle stroke become a rect, no matter what
  // the scale is set. So we set width/height as 2. See #4150.

  var symbolPath = createSymbol(symbolType, -1, -1, 2, 2, color, keepAspect);
  symbolPath.attr({
    z2: 100,
    culling: true,
    scale: getScale(symbolSize)
  }); // Rewrite drift method

  symbolPath.drift = driftSymbol;
  this._symbolType = symbolType;
  this.add(symbolPath);
};
/**
 * Stop animation
 * @param {boolean} toLastFrame
 */


symbolProto.stopSymbolAnimation = function (toLastFrame) {
  this.childAt(0).stopAnimation(toLastFrame);
};
/**
 * FIXME:
 * Caution: This method breaks the encapsulation of this module,
 * but it indeed brings convenience. So do not use the method
 * unless you detailedly know all the implements of `Symbol`,
 * especially animation.
 *
 * Get symbol path element.
 */


symbolProto.getSymbolPath = function () {
  return this.childAt(0);
};
/**
 * Get scale(aka, current symbol size).
 * Including the change caused by animation
 */


symbolProto.getScale = function () {
  return this.childAt(0).scale;
};
/**
 * Highlight symbol
 */


symbolProto.highlight = function () {
  this.childAt(0).trigger('emphasis');
};
/**
 * Downplay symbol
 */


symbolProto.downplay = function () {
  this.childAt(0).trigger('normal');
};
/**
 * @param {number} zlevel
 * @param {number} z
 */


symbolProto.setZ = function (zlevel, z) {
  var symbolPath = this.childAt(0);
  symbolPath.zlevel = zlevel;
  symbolPath.z = z;
};

symbolProto.setDraggable = function (draggable) {
  var symbolPath = this.childAt(0);
  symbolPath.draggable = draggable;
  symbolPath.cursor = draggable ? 'move' : 'pointer';
};
/**
 * Update symbol properties
 * @param {module:echarts/data/List} data
 * @param {number} idx
 * @param {Object} [seriesScope]
 * @param {Object} [seriesScope.itemStyle]
 * @param {Object} [seriesScope.hoverItemStyle]
 * @param {Object} [seriesScope.symbolRotate]
 * @param {Object} [seriesScope.symbolOffset]
 * @param {module:echarts/model/Model} [seriesScope.labelModel]
 * @param {module:echarts/model/Model} [seriesScope.hoverLabelModel]
 * @param {boolean} [seriesScope.hoverAnimation]
 * @param {Object} [seriesScope.cursorStyle]
 * @param {module:echarts/model/Model} [seriesScope.itemModel]
 * @param {string} [seriesScope.symbolInnerColor]
 * @param {Object} [seriesScope.fadeIn=false]
 */


symbolProto.updateData = function (data, idx, seriesScope) {
  this.silent = false;
  var symbolType = data.getItemVisual(idx, 'symbol') || 'circle';
  var seriesModel = data.hostModel;
  var symbolSize = getSymbolSize(data, idx);
  var isInit = symbolType !== this._symbolType;

  if (isInit) {
    var keepAspect = data.getItemVisual(idx, 'symbolKeepAspect');

    this._createSymbol(symbolType, data, idx, symbolSize, keepAspect);
  } else {
    var symbolPath = this.childAt(0);
    symbolPath.silent = false;
    graphic.updateProps(symbolPath, {
      scale: getScale(symbolSize)
    }, seriesModel, idx);
  }

  this._updateCommon(data, idx, symbolSize, seriesScope);

  if (isInit) {
    var symbolPath = this.childAt(0);
    var fadeIn = seriesScope && seriesScope.fadeIn;
    var target = {
      scale: symbolPath.scale.slice()
    };
    fadeIn && (target.style = {
      opacity: symbolPath.style.opacity
    });
    symbolPath.scale = [0, 0];
    fadeIn && (symbolPath.style.opacity = 0);
    graphic.initProps(symbolPath, target, seriesModel, idx);
  }

  this._seriesModel = seriesModel;
}; // Update common properties


var normalStyleAccessPath = ['itemStyle'];
var emphasisStyleAccessPath = ['emphasis', 'itemStyle'];
var normalLabelAccessPath = ['label'];
var emphasisLabelAccessPath = ['emphasis', 'label'];
/**
 * @param {module:echarts/data/List} data
 * @param {number} idx
 * @param {Array.<number>} symbolSize
 * @param {Object} [seriesScope]
 */

symbolProto._updateCommon = function (data, idx, symbolSize, seriesScope) {
  var symbolPath = this.childAt(0);
  var seriesModel = data.hostModel;
  var color = data.getItemVisual(idx, 'color'); // Reset style

  if (symbolPath.type !== 'image') {
    symbolPath.useStyle({
      strokeNoScale: true
    });
  }

  var itemStyle = seriesScope && seriesScope.itemStyle;
  var hoverItemStyle = seriesScope && seriesScope.hoverItemStyle;
  var symbolRotate = seriesScope && seriesScope.symbolRotate;
  var symbolOffset = seriesScope && seriesScope.symbolOffset;
  var labelModel = seriesScope && seriesScope.labelModel;
  var hoverLabelModel = seriesScope && seriesScope.hoverLabelModel;
  var hoverAnimation = seriesScope && seriesScope.hoverAnimation;
  var cursorStyle = seriesScope && seriesScope.cursorStyle;

  if (!seriesScope || data.hasItemOption) {
    var itemModel = seriesScope && seriesScope.itemModel ? seriesScope.itemModel : data.getItemModel(idx); // Color must be excluded.
    // Because symbol provide setColor individually to set fill and stroke

    itemStyle = itemModel.getModel(normalStyleAccessPath).getItemStyle(['color']);
    hoverItemStyle = itemModel.getModel(emphasisStyleAccessPath).getItemStyle();
    symbolRotate = itemModel.getShallow('symbolRotate');
    symbolOffset = itemModel.getShallow('symbolOffset');
    labelModel = itemModel.getModel(normalLabelAccessPath);
    hoverLabelModel = itemModel.getModel(emphasisLabelAccessPath);
    hoverAnimation = itemModel.getShallow('hoverAnimation');
    cursorStyle = itemModel.getShallow('cursor');
  } else {
    hoverItemStyle = zrUtil.extend({}, hoverItemStyle);
  }

  var elStyle = symbolPath.style;
  symbolPath.attr('rotation', (symbolRotate || 0) * Math.PI / 180 || 0);

  if (symbolOffset) {
    symbolPath.attr('position', [parsePercent(symbolOffset[0], symbolSize[0]), parsePercent(symbolOffset[1], symbolSize[1])]);
  }

  cursorStyle && symbolPath.attr('cursor', cursorStyle); // PENDING setColor before setStyle!!!

  symbolPath.setColor(color, seriesScope && seriesScope.symbolInnerColor);
  symbolPath.setStyle(itemStyle);
  var opacity = data.getItemVisual(idx, 'opacity');

  if (opacity != null) {
    elStyle.opacity = opacity;
  }

  var liftZ = data.getItemVisual(idx, 'liftZ');
  var z2Origin = symbolPath.__z2Origin;

  if (liftZ != null) {
    if (z2Origin == null) {
      symbolPath.__z2Origin = symbolPath.z2;
      symbolPath.z2 += liftZ;
    }
  } else if (z2Origin != null) {
    symbolPath.z2 = z2Origin;
    symbolPath.__z2Origin = null;
  }

  var useNameLabel = seriesScope && seriesScope.useNameLabel;
  graphic.setLabelStyle(elStyle, hoverItemStyle, labelModel, hoverLabelModel, {
    labelFetcher: seriesModel,
    labelDataIndex: idx,
    defaultText: getLabelDefaultText,
    isRectText: true,
    autoColor: color
  }); // Do not execute util needed.

  function getLabelDefaultText(idx, opt) {
    return useNameLabel ? data.getName(idx) : getDefaultLabel(data, idx);
  }

  symbolPath.off('mouseover').off('mouseout').off('emphasis').off('normal');
  symbolPath.hoverStyle = hoverItemStyle; // FIXME
  // Do not use symbol.trigger('emphasis'), but use symbol.highlight() instead.

  graphic.setHoverStyle(symbolPath);
  symbolPath.__symbolOriginalScale = getScale(symbolSize);

  if (hoverAnimation && seriesModel.isAnimationEnabled()) {
    // Note: consider `off`, should use static function here.
    symbolPath.on('mouseover', onMouseOver).on('mouseout', onMouseOut).on('emphasis', onEmphasis).on('normal', onNormal);
  }
};

function onMouseOver() {
  // see comment in `graphic.isInEmphasis`
  !graphic.isInEmphasis(this) && onEmphasis.call(this);
}

function onMouseOut() {
  // see comment in `graphic.isInEmphasis`
  !graphic.isInEmphasis(this) && onNormal.call(this);
}

function onEmphasis() {
  // Do not support this hover animation util some scenario required.
  // Animation can only be supported in hover layer when using `el.incremetal`.
  if (this.incremental || this.useHoverLayer) {
    return;
  }

  var scale = this.__symbolOriginalScale;
  var ratio = scale[1] / scale[0];
  this.animateTo({
    scale: [Math.max(scale[0] * 1.1, scale[0] + 3), Math.max(scale[1] * 1.1, scale[1] + 3 * ratio)]
  }, 400, 'elasticOut');
}

function onNormal() {
  if (this.incremental || this.useHoverLayer) {
    return;
  }

  this.animateTo({
    scale: this.__symbolOriginalScale
  }, 400, 'elasticOut');
}
/**
 * @param {Function} cb
 * @param {Object} [opt]
 * @param {Object} [opt.keepLabel=true]
 */


symbolProto.fadeOut = function (cb, opt) {
  var symbolPath = this.childAt(0); // Avoid mistaken hover when fading out

  this.silent = symbolPath.silent = true; // Not show text when animating

  !(opt && opt.keepLabel) && (symbolPath.style.text = null);
  graphic.updateProps(symbolPath, {
    style: {
      opacity: 0
    },
    scale: [0, 0]
  }, this._seriesModel, this.dataIndex, cb);
};

zrUtil.inherits(SymbolClz, graphic.Group);
var _default = SymbolClz;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvaGVscGVyL1N5bWJvbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NoYXJ0L2hlbHBlci9TeW1ib2wuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgX3N5bWJvbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL3N5bWJvbFwiKTtcblxudmFyIGNyZWF0ZVN5bWJvbCA9IF9zeW1ib2wuY3JlYXRlU3ltYm9sO1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2dyYXBoaWNcIik7XG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgcGFyc2VQZXJjZW50ID0gX251bWJlci5wYXJzZVBlcmNlbnQ7XG5cbnZhciBfbGFiZWxIZWxwZXIgPSByZXF1aXJlKFwiLi9sYWJlbEhlbHBlclwiKTtcblxudmFyIGdldERlZmF1bHRMYWJlbCA9IF9sYWJlbEhlbHBlci5nZXREZWZhdWx0TGFiZWw7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBAbW9kdWxlIGVjaGFydHMvY2hhcnQvaGVscGVyL1N5bWJvbFxuICovXG5cbi8qKlxuICogQGNvbnN0cnVjdG9yXG4gKiBAYWxpYXMge21vZHVsZTplY2hhcnRzL2NoYXJ0L2hlbHBlci9TeW1ib2x9XG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICogQGV4dGVuZHMge21vZHVsZTp6cmVuZGVyL2dyYXBoaWMvR3JvdXB9XG4gKi9cbmZ1bmN0aW9uIFN5bWJvbENseihkYXRhLCBpZHgsIHNlcmllc1Njb3BlKSB7XG4gIGdyYXBoaWMuR3JvdXAuY2FsbCh0aGlzKTtcbiAgdGhpcy51cGRhdGVEYXRhKGRhdGEsIGlkeCwgc2VyaWVzU2NvcGUpO1xufVxuXG52YXIgc3ltYm9sUHJvdG8gPSBTeW1ib2xDbHoucHJvdG90eXBlO1xuLyoqXG4gKiBAcHVibGljXG4gKiBAc3RhdGljXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2RhdGEvTGlzdH0gZGF0YVxuICogQHBhcmFtIHtudW1iZXJ9IGRhdGFJbmRleFxuICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59IFt3aWR0aCwgaGVpZ2h0XVxuICovXG5cbnZhciBnZXRTeW1ib2xTaXplID0gU3ltYm9sQ2x6LmdldFN5bWJvbFNpemUgPSBmdW5jdGlvbiAoZGF0YSwgaWR4KSB7XG4gIHZhciBzeW1ib2xTaXplID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ3N5bWJvbFNpemUnKTtcbiAgcmV0dXJuIHN5bWJvbFNpemUgaW5zdGFuY2VvZiBBcnJheSA/IHN5bWJvbFNpemUuc2xpY2UoKSA6IFsrc3ltYm9sU2l6ZSwgK3N5bWJvbFNpemVdO1xufTtcblxuZnVuY3Rpb24gZ2V0U2NhbGUoc3ltYm9sU2l6ZSkge1xuICByZXR1cm4gW3N5bWJvbFNpemVbMF0gLyAyLCBzeW1ib2xTaXplWzFdIC8gMl07XG59XG5cbmZ1bmN0aW9uIGRyaWZ0U3ltYm9sKGR4LCBkeSkge1xuICB0aGlzLnBhcmVudC5kcmlmdChkeCwgZHkpO1xufVxuXG5zeW1ib2xQcm90by5fY3JlYXRlU3ltYm9sID0gZnVuY3Rpb24gKHN5bWJvbFR5cGUsIGRhdGEsIGlkeCwgc3ltYm9sU2l6ZSwga2VlcEFzcGVjdCkge1xuICAvLyBSZW1vdmUgcGF0aHMgY3JlYXRlZCBiZWZvcmVcbiAgdGhpcy5yZW1vdmVBbGwoKTtcbiAgdmFyIGNvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ2NvbG9yJyk7IC8vIHZhciBzeW1ib2xQYXRoID0gY3JlYXRlU3ltYm9sKFxuICAvLyAgICAgc3ltYm9sVHlwZSwgLTAuNSwgLTAuNSwgMSwgMSwgY29sb3JcbiAgLy8gKTtcbiAgLy8gSWYgd2lkdGgvaGVpZ2h0IGFyZSBzZXQgdG9vIHNtYWxsIChlLmcuLCBzZXQgdG8gMSkgb24gaW9zMTBcbiAgLy8gYW5kIG1hY09TIFNpZXJyYSwgYSBjaXJjbGUgc3Ryb2tlIGJlY29tZSBhIHJlY3QsIG5vIG1hdHRlciB3aGF0XG4gIC8vIHRoZSBzY2FsZSBpcyBzZXQuIFNvIHdlIHNldCB3aWR0aC9oZWlnaHQgYXMgMi4gU2VlICM0MTUwLlxuXG4gIHZhciBzeW1ib2xQYXRoID0gY3JlYXRlU3ltYm9sKHN5bWJvbFR5cGUsIC0xLCAtMSwgMiwgMiwgY29sb3IsIGtlZXBBc3BlY3QpO1xuICBzeW1ib2xQYXRoLmF0dHIoe1xuICAgIHoyOiAxMDAsXG4gICAgY3VsbGluZzogdHJ1ZSxcbiAgICBzY2FsZTogZ2V0U2NhbGUoc3ltYm9sU2l6ZSlcbiAgfSk7IC8vIFJld3JpdGUgZHJpZnQgbWV0aG9kXG5cbiAgc3ltYm9sUGF0aC5kcmlmdCA9IGRyaWZ0U3ltYm9sO1xuICB0aGlzLl9zeW1ib2xUeXBlID0gc3ltYm9sVHlwZTtcbiAgdGhpcy5hZGQoc3ltYm9sUGF0aCk7XG59O1xuLyoqXG4gKiBTdG9wIGFuaW1hdGlvblxuICogQHBhcmFtIHtib29sZWFufSB0b0xhc3RGcmFtZVxuICovXG5cblxuc3ltYm9sUHJvdG8uc3RvcFN5bWJvbEFuaW1hdGlvbiA9IGZ1bmN0aW9uICh0b0xhc3RGcmFtZSkge1xuICB0aGlzLmNoaWxkQXQoMCkuc3RvcEFuaW1hdGlvbih0b0xhc3RGcmFtZSk7XG59O1xuLyoqXG4gKiBGSVhNRTpcbiAqIENhdXRpb246IFRoaXMgbWV0aG9kIGJyZWFrcyB0aGUgZW5jYXBzdWxhdGlvbiBvZiB0aGlzIG1vZHVsZSxcbiAqIGJ1dCBpdCBpbmRlZWQgYnJpbmdzIGNvbnZlbmllbmNlLiBTbyBkbyBub3QgdXNlIHRoZSBtZXRob2RcbiAqIHVubGVzcyB5b3UgZGV0YWlsZWRseSBrbm93IGFsbCB0aGUgaW1wbGVtZW50cyBvZiBgU3ltYm9sYCxcbiAqIGVzcGVjaWFsbHkgYW5pbWF0aW9uLlxuICpcbiAqIEdldCBzeW1ib2wgcGF0aCBlbGVtZW50LlxuICovXG5cblxuc3ltYm9sUHJvdG8uZ2V0U3ltYm9sUGF0aCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuY2hpbGRBdCgwKTtcbn07XG4vKipcbiAqIEdldCBzY2FsZShha2EsIGN1cnJlbnQgc3ltYm9sIHNpemUpLlxuICogSW5jbHVkaW5nIHRoZSBjaGFuZ2UgY2F1c2VkIGJ5IGFuaW1hdGlvblxuICovXG5cblxuc3ltYm9sUHJvdG8uZ2V0U2NhbGUgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLmNoaWxkQXQoMCkuc2NhbGU7XG59O1xuLyoqXG4gKiBIaWdobGlnaHQgc3ltYm9sXG4gKi9cblxuXG5zeW1ib2xQcm90by5oaWdobGlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMuY2hpbGRBdCgwKS50cmlnZ2VyKCdlbXBoYXNpcycpO1xufTtcbi8qKlxuICogRG93bnBsYXkgc3ltYm9sXG4gKi9cblxuXG5zeW1ib2xQcm90by5kb3ducGxheSA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5jaGlsZEF0KDApLnRyaWdnZXIoJ25vcm1hbCcpO1xufTtcbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IHpsZXZlbFxuICogQHBhcmFtIHtudW1iZXJ9IHpcbiAqL1xuXG5cbnN5bWJvbFByb3RvLnNldFogPSBmdW5jdGlvbiAoemxldmVsLCB6KSB7XG4gIHZhciBzeW1ib2xQYXRoID0gdGhpcy5jaGlsZEF0KDApO1xuICBzeW1ib2xQYXRoLnpsZXZlbCA9IHpsZXZlbDtcbiAgc3ltYm9sUGF0aC56ID0gejtcbn07XG5cbnN5bWJvbFByb3RvLnNldERyYWdnYWJsZSA9IGZ1bmN0aW9uIChkcmFnZ2FibGUpIHtcbiAgdmFyIHN5bWJvbFBhdGggPSB0aGlzLmNoaWxkQXQoMCk7XG4gIHN5bWJvbFBhdGguZHJhZ2dhYmxlID0gZHJhZ2dhYmxlO1xuICBzeW1ib2xQYXRoLmN1cnNvciA9IGRyYWdnYWJsZSA/ICdtb3ZlJyA6ICdwb2ludGVyJztcbn07XG4vKipcbiAqIFVwZGF0ZSBzeW1ib2wgcHJvcGVydGllc1xuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9IGRhdGFcbiAqIEBwYXJhbSB7bnVtYmVyfSBpZHhcbiAqIEBwYXJhbSB7T2JqZWN0fSBbc2VyaWVzU2NvcGVdXG4gKiBAcGFyYW0ge09iamVjdH0gW3Nlcmllc1Njb3BlLml0ZW1TdHlsZV1cbiAqIEBwYXJhbSB7T2JqZWN0fSBbc2VyaWVzU2NvcGUuaG92ZXJJdGVtU3R5bGVdXG4gKiBAcGFyYW0ge09iamVjdH0gW3Nlcmllc1Njb3BlLnN5bWJvbFJvdGF0ZV1cbiAqIEBwYXJhbSB7T2JqZWN0fSBbc2VyaWVzU2NvcGUuc3ltYm9sT2Zmc2V0XVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gW3Nlcmllc1Njb3BlLmxhYmVsTW9kZWxdXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBbc2VyaWVzU2NvcGUuaG92ZXJMYWJlbE1vZGVsXVxuICogQHBhcmFtIHtib29sZWFufSBbc2VyaWVzU2NvcGUuaG92ZXJBbmltYXRpb25dXG4gKiBAcGFyYW0ge09iamVjdH0gW3Nlcmllc1Njb3BlLmN1cnNvclN0eWxlXVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gW3Nlcmllc1Njb3BlLml0ZW1Nb2RlbF1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbc2VyaWVzU2NvcGUuc3ltYm9sSW5uZXJDb2xvcl1cbiAqIEBwYXJhbSB7T2JqZWN0fSBbc2VyaWVzU2NvcGUuZmFkZUluPWZhbHNlXVxuICovXG5cblxuc3ltYm9sUHJvdG8udXBkYXRlRGF0YSA9IGZ1bmN0aW9uIChkYXRhLCBpZHgsIHNlcmllc1Njb3BlKSB7XG4gIHRoaXMuc2lsZW50ID0gZmFsc2U7XG4gIHZhciBzeW1ib2xUeXBlID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ3N5bWJvbCcpIHx8ICdjaXJjbGUnO1xuICB2YXIgc2VyaWVzTW9kZWwgPSBkYXRhLmhvc3RNb2RlbDtcbiAgdmFyIHN5bWJvbFNpemUgPSBnZXRTeW1ib2xTaXplKGRhdGEsIGlkeCk7XG4gIHZhciBpc0luaXQgPSBzeW1ib2xUeXBlICE9PSB0aGlzLl9zeW1ib2xUeXBlO1xuXG4gIGlmIChpc0luaXQpIHtcbiAgICB2YXIga2VlcEFzcGVjdCA9IGRhdGEuZ2V0SXRlbVZpc3VhbChpZHgsICdzeW1ib2xLZWVwQXNwZWN0Jyk7XG5cbiAgICB0aGlzLl9jcmVhdGVTeW1ib2woc3ltYm9sVHlwZSwgZGF0YSwgaWR4LCBzeW1ib2xTaXplLCBrZWVwQXNwZWN0KTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgc3ltYm9sUGF0aCA9IHRoaXMuY2hpbGRBdCgwKTtcbiAgICBzeW1ib2xQYXRoLnNpbGVudCA9IGZhbHNlO1xuICAgIGdyYXBoaWMudXBkYXRlUHJvcHMoc3ltYm9sUGF0aCwge1xuICAgICAgc2NhbGU6IGdldFNjYWxlKHN5bWJvbFNpemUpXG4gICAgfSwgc2VyaWVzTW9kZWwsIGlkeCk7XG4gIH1cblxuICB0aGlzLl91cGRhdGVDb21tb24oZGF0YSwgaWR4LCBzeW1ib2xTaXplLCBzZXJpZXNTY29wZSk7XG5cbiAgaWYgKGlzSW5pdCkge1xuICAgIHZhciBzeW1ib2xQYXRoID0gdGhpcy5jaGlsZEF0KDApO1xuICAgIHZhciBmYWRlSW4gPSBzZXJpZXNTY29wZSAmJiBzZXJpZXNTY29wZS5mYWRlSW47XG4gICAgdmFyIHRhcmdldCA9IHtcbiAgICAgIHNjYWxlOiBzeW1ib2xQYXRoLnNjYWxlLnNsaWNlKClcbiAgICB9O1xuICAgIGZhZGVJbiAmJiAodGFyZ2V0LnN0eWxlID0ge1xuICAgICAgb3BhY2l0eTogc3ltYm9sUGF0aC5zdHlsZS5vcGFjaXR5XG4gICAgfSk7XG4gICAgc3ltYm9sUGF0aC5zY2FsZSA9IFswLCAwXTtcbiAgICBmYWRlSW4gJiYgKHN5bWJvbFBhdGguc3R5bGUub3BhY2l0eSA9IDApO1xuICAgIGdyYXBoaWMuaW5pdFByb3BzKHN5bWJvbFBhdGgsIHRhcmdldCwgc2VyaWVzTW9kZWwsIGlkeCk7XG4gIH1cblxuICB0aGlzLl9zZXJpZXNNb2RlbCA9IHNlcmllc01vZGVsO1xufTsgLy8gVXBkYXRlIGNvbW1vbiBwcm9wZXJ0aWVzXG5cblxudmFyIG5vcm1hbFN0eWxlQWNjZXNzUGF0aCA9IFsnaXRlbVN0eWxlJ107XG52YXIgZW1waGFzaXNTdHlsZUFjY2Vzc1BhdGggPSBbJ2VtcGhhc2lzJywgJ2l0ZW1TdHlsZSddO1xudmFyIG5vcm1hbExhYmVsQWNjZXNzUGF0aCA9IFsnbGFiZWwnXTtcbnZhciBlbXBoYXNpc0xhYmVsQWNjZXNzUGF0aCA9IFsnZW1waGFzaXMnLCAnbGFiZWwnXTtcbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9IGRhdGFcbiAqIEBwYXJhbSB7bnVtYmVyfSBpZHhcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHN5bWJvbFNpemVcbiAqIEBwYXJhbSB7T2JqZWN0fSBbc2VyaWVzU2NvcGVdXG4gKi9cblxuc3ltYm9sUHJvdG8uX3VwZGF0ZUNvbW1vbiA9IGZ1bmN0aW9uIChkYXRhLCBpZHgsIHN5bWJvbFNpemUsIHNlcmllc1Njb3BlKSB7XG4gIHZhciBzeW1ib2xQYXRoID0gdGhpcy5jaGlsZEF0KDApO1xuICB2YXIgc2VyaWVzTW9kZWwgPSBkYXRhLmhvc3RNb2RlbDtcbiAgdmFyIGNvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ2NvbG9yJyk7IC8vIFJlc2V0IHN0eWxlXG5cbiAgaWYgKHN5bWJvbFBhdGgudHlwZSAhPT0gJ2ltYWdlJykge1xuICAgIHN5bWJvbFBhdGgudXNlU3R5bGUoe1xuICAgICAgc3Ryb2tlTm9TY2FsZTogdHJ1ZVxuICAgIH0pO1xuICB9XG5cbiAgdmFyIGl0ZW1TdHlsZSA9IHNlcmllc1Njb3BlICYmIHNlcmllc1Njb3BlLml0ZW1TdHlsZTtcbiAgdmFyIGhvdmVySXRlbVN0eWxlID0gc2VyaWVzU2NvcGUgJiYgc2VyaWVzU2NvcGUuaG92ZXJJdGVtU3R5bGU7XG4gIHZhciBzeW1ib2xSb3RhdGUgPSBzZXJpZXNTY29wZSAmJiBzZXJpZXNTY29wZS5zeW1ib2xSb3RhdGU7XG4gIHZhciBzeW1ib2xPZmZzZXQgPSBzZXJpZXNTY29wZSAmJiBzZXJpZXNTY29wZS5zeW1ib2xPZmZzZXQ7XG4gIHZhciBsYWJlbE1vZGVsID0gc2VyaWVzU2NvcGUgJiYgc2VyaWVzU2NvcGUubGFiZWxNb2RlbDtcbiAgdmFyIGhvdmVyTGFiZWxNb2RlbCA9IHNlcmllc1Njb3BlICYmIHNlcmllc1Njb3BlLmhvdmVyTGFiZWxNb2RlbDtcbiAgdmFyIGhvdmVyQW5pbWF0aW9uID0gc2VyaWVzU2NvcGUgJiYgc2VyaWVzU2NvcGUuaG92ZXJBbmltYXRpb247XG4gIHZhciBjdXJzb3JTdHlsZSA9IHNlcmllc1Njb3BlICYmIHNlcmllc1Njb3BlLmN1cnNvclN0eWxlO1xuXG4gIGlmICghc2VyaWVzU2NvcGUgfHwgZGF0YS5oYXNJdGVtT3B0aW9uKSB7XG4gICAgdmFyIGl0ZW1Nb2RlbCA9IHNlcmllc1Njb3BlICYmIHNlcmllc1Njb3BlLml0ZW1Nb2RlbCA/IHNlcmllc1Njb3BlLml0ZW1Nb2RlbCA6IGRhdGEuZ2V0SXRlbU1vZGVsKGlkeCk7IC8vIENvbG9yIG11c3QgYmUgZXhjbHVkZWQuXG4gICAgLy8gQmVjYXVzZSBzeW1ib2wgcHJvdmlkZSBzZXRDb2xvciBpbmRpdmlkdWFsbHkgdG8gc2V0IGZpbGwgYW5kIHN0cm9rZVxuXG4gICAgaXRlbVN0eWxlID0gaXRlbU1vZGVsLmdldE1vZGVsKG5vcm1hbFN0eWxlQWNjZXNzUGF0aCkuZ2V0SXRlbVN0eWxlKFsnY29sb3InXSk7XG4gICAgaG92ZXJJdGVtU3R5bGUgPSBpdGVtTW9kZWwuZ2V0TW9kZWwoZW1waGFzaXNTdHlsZUFjY2Vzc1BhdGgpLmdldEl0ZW1TdHlsZSgpO1xuICAgIHN5bWJvbFJvdGF0ZSA9IGl0ZW1Nb2RlbC5nZXRTaGFsbG93KCdzeW1ib2xSb3RhdGUnKTtcbiAgICBzeW1ib2xPZmZzZXQgPSBpdGVtTW9kZWwuZ2V0U2hhbGxvdygnc3ltYm9sT2Zmc2V0Jyk7XG4gICAgbGFiZWxNb2RlbCA9IGl0ZW1Nb2RlbC5nZXRNb2RlbChub3JtYWxMYWJlbEFjY2Vzc1BhdGgpO1xuICAgIGhvdmVyTGFiZWxNb2RlbCA9IGl0ZW1Nb2RlbC5nZXRNb2RlbChlbXBoYXNpc0xhYmVsQWNjZXNzUGF0aCk7XG4gICAgaG92ZXJBbmltYXRpb24gPSBpdGVtTW9kZWwuZ2V0U2hhbGxvdygnaG92ZXJBbmltYXRpb24nKTtcbiAgICBjdXJzb3JTdHlsZSA9IGl0ZW1Nb2RlbC5nZXRTaGFsbG93KCdjdXJzb3InKTtcbiAgfSBlbHNlIHtcbiAgICBob3Zlckl0ZW1TdHlsZSA9IHpyVXRpbC5leHRlbmQoe30sIGhvdmVySXRlbVN0eWxlKTtcbiAgfVxuXG4gIHZhciBlbFN0eWxlID0gc3ltYm9sUGF0aC5zdHlsZTtcbiAgc3ltYm9sUGF0aC5hdHRyKCdyb3RhdGlvbicsIChzeW1ib2xSb3RhdGUgfHwgMCkgKiBNYXRoLlBJIC8gMTgwIHx8IDApO1xuXG4gIGlmIChzeW1ib2xPZmZzZXQpIHtcbiAgICBzeW1ib2xQYXRoLmF0dHIoJ3Bvc2l0aW9uJywgW3BhcnNlUGVyY2VudChzeW1ib2xPZmZzZXRbMF0sIHN5bWJvbFNpemVbMF0pLCBwYXJzZVBlcmNlbnQoc3ltYm9sT2Zmc2V0WzFdLCBzeW1ib2xTaXplWzFdKV0pO1xuICB9XG5cbiAgY3Vyc29yU3R5bGUgJiYgc3ltYm9sUGF0aC5hdHRyKCdjdXJzb3InLCBjdXJzb3JTdHlsZSk7IC8vIFBFTkRJTkcgc2V0Q29sb3IgYmVmb3JlIHNldFN0eWxlISEhXG5cbiAgc3ltYm9sUGF0aC5zZXRDb2xvcihjb2xvciwgc2VyaWVzU2NvcGUgJiYgc2VyaWVzU2NvcGUuc3ltYm9sSW5uZXJDb2xvcik7XG4gIHN5bWJvbFBhdGguc2V0U3R5bGUoaXRlbVN0eWxlKTtcbiAgdmFyIG9wYWNpdHkgPSBkYXRhLmdldEl0ZW1WaXN1YWwoaWR4LCAnb3BhY2l0eScpO1xuXG4gIGlmIChvcGFjaXR5ICE9IG51bGwpIHtcbiAgICBlbFN0eWxlLm9wYWNpdHkgPSBvcGFjaXR5O1xuICB9XG5cbiAgdmFyIGxpZnRaID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ2xpZnRaJyk7XG4gIHZhciB6Mk9yaWdpbiA9IHN5bWJvbFBhdGguX196Mk9yaWdpbjtcblxuICBpZiAobGlmdFogIT0gbnVsbCkge1xuICAgIGlmICh6Mk9yaWdpbiA9PSBudWxsKSB7XG4gICAgICBzeW1ib2xQYXRoLl9fejJPcmlnaW4gPSBzeW1ib2xQYXRoLnoyO1xuICAgICAgc3ltYm9sUGF0aC56MiArPSBsaWZ0WjtcbiAgICB9XG4gIH0gZWxzZSBpZiAoejJPcmlnaW4gIT0gbnVsbCkge1xuICAgIHN5bWJvbFBhdGguejIgPSB6Mk9yaWdpbjtcbiAgICBzeW1ib2xQYXRoLl9fejJPcmlnaW4gPSBudWxsO1xuICB9XG5cbiAgdmFyIHVzZU5hbWVMYWJlbCA9IHNlcmllc1Njb3BlICYmIHNlcmllc1Njb3BlLnVzZU5hbWVMYWJlbDtcbiAgZ3JhcGhpYy5zZXRMYWJlbFN0eWxlKGVsU3R5bGUsIGhvdmVySXRlbVN0eWxlLCBsYWJlbE1vZGVsLCBob3ZlckxhYmVsTW9kZWwsIHtcbiAgICBsYWJlbEZldGNoZXI6IHNlcmllc01vZGVsLFxuICAgIGxhYmVsRGF0YUluZGV4OiBpZHgsXG4gICAgZGVmYXVsdFRleHQ6IGdldExhYmVsRGVmYXVsdFRleHQsXG4gICAgaXNSZWN0VGV4dDogdHJ1ZSxcbiAgICBhdXRvQ29sb3I6IGNvbG9yXG4gIH0pOyAvLyBEbyBub3QgZXhlY3V0ZSB1dGlsIG5lZWRlZC5cblxuICBmdW5jdGlvbiBnZXRMYWJlbERlZmF1bHRUZXh0KGlkeCwgb3B0KSB7XG4gICAgcmV0dXJuIHVzZU5hbWVMYWJlbCA/IGRhdGEuZ2V0TmFtZShpZHgpIDogZ2V0RGVmYXVsdExhYmVsKGRhdGEsIGlkeCk7XG4gIH1cblxuICBzeW1ib2xQYXRoLm9mZignbW91c2VvdmVyJykub2ZmKCdtb3VzZW91dCcpLm9mZignZW1waGFzaXMnKS5vZmYoJ25vcm1hbCcpO1xuICBzeW1ib2xQYXRoLmhvdmVyU3R5bGUgPSBob3Zlckl0ZW1TdHlsZTsgLy8gRklYTUVcbiAgLy8gRG8gbm90IHVzZSBzeW1ib2wudHJpZ2dlcignZW1waGFzaXMnKSwgYnV0IHVzZSBzeW1ib2wuaGlnaGxpZ2h0KCkgaW5zdGVhZC5cblxuICBncmFwaGljLnNldEhvdmVyU3R5bGUoc3ltYm9sUGF0aCk7XG4gIHN5bWJvbFBhdGguX19zeW1ib2xPcmlnaW5hbFNjYWxlID0gZ2V0U2NhbGUoc3ltYm9sU2l6ZSk7XG5cbiAgaWYgKGhvdmVyQW5pbWF0aW9uICYmIHNlcmllc01vZGVsLmlzQW5pbWF0aW9uRW5hYmxlZCgpKSB7XG4gICAgLy8gTm90ZTogY29uc2lkZXIgYG9mZmAsIHNob3VsZCB1c2Ugc3RhdGljIGZ1bmN0aW9uIGhlcmUuXG4gICAgc3ltYm9sUGF0aC5vbignbW91c2VvdmVyJywgb25Nb3VzZU92ZXIpLm9uKCdtb3VzZW91dCcsIG9uTW91c2VPdXQpLm9uKCdlbXBoYXNpcycsIG9uRW1waGFzaXMpLm9uKCdub3JtYWwnLCBvbk5vcm1hbCk7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIG9uTW91c2VPdmVyKCkge1xuICAvLyBzZWUgY29tbWVudCBpbiBgZ3JhcGhpYy5pc0luRW1waGFzaXNgXG4gICFncmFwaGljLmlzSW5FbXBoYXNpcyh0aGlzKSAmJiBvbkVtcGhhc2lzLmNhbGwodGhpcyk7XG59XG5cbmZ1bmN0aW9uIG9uTW91c2VPdXQoKSB7XG4gIC8vIHNlZSBjb21tZW50IGluIGBncmFwaGljLmlzSW5FbXBoYXNpc2BcbiAgIWdyYXBoaWMuaXNJbkVtcGhhc2lzKHRoaXMpICYmIG9uTm9ybWFsLmNhbGwodGhpcyk7XG59XG5cbmZ1bmN0aW9uIG9uRW1waGFzaXMoKSB7XG4gIC8vIERvIG5vdCBzdXBwb3J0IHRoaXMgaG92ZXIgYW5pbWF0aW9uIHV0aWwgc29tZSBzY2VuYXJpbyByZXF1aXJlZC5cbiAgLy8gQW5pbWF0aW9uIGNhbiBvbmx5IGJlIHN1cHBvcnRlZCBpbiBob3ZlciBsYXllciB3aGVuIHVzaW5nIGBlbC5pbmNyZW1ldGFsYC5cbiAgaWYgKHRoaXMuaW5jcmVtZW50YWwgfHwgdGhpcy51c2VIb3ZlckxheWVyKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHNjYWxlID0gdGhpcy5fX3N5bWJvbE9yaWdpbmFsU2NhbGU7XG4gIHZhciByYXRpbyA9IHNjYWxlWzFdIC8gc2NhbGVbMF07XG4gIHRoaXMuYW5pbWF0ZVRvKHtcbiAgICBzY2FsZTogW01hdGgubWF4KHNjYWxlWzBdICogMS4xLCBzY2FsZVswXSArIDMpLCBNYXRoLm1heChzY2FsZVsxXSAqIDEuMSwgc2NhbGVbMV0gKyAzICogcmF0aW8pXVxuICB9LCA0MDAsICdlbGFzdGljT3V0Jyk7XG59XG5cbmZ1bmN0aW9uIG9uTm9ybWFsKCkge1xuICBpZiAodGhpcy5pbmNyZW1lbnRhbCB8fCB0aGlzLnVzZUhvdmVyTGF5ZXIpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB0aGlzLmFuaW1hdGVUbyh7XG4gICAgc2NhbGU6IHRoaXMuX19zeW1ib2xPcmlnaW5hbFNjYWxlXG4gIH0sIDQwMCwgJ2VsYXN0aWNPdXQnKTtcbn1cbi8qKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0XVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHQua2VlcExhYmVsPXRydWVdXG4gKi9cblxuXG5zeW1ib2xQcm90by5mYWRlT3V0ID0gZnVuY3Rpb24gKGNiLCBvcHQpIHtcbiAgdmFyIHN5bWJvbFBhdGggPSB0aGlzLmNoaWxkQXQoMCk7IC8vIEF2b2lkIG1pc3Rha2VuIGhvdmVyIHdoZW4gZmFkaW5nIG91dFxuXG4gIHRoaXMuc2lsZW50ID0gc3ltYm9sUGF0aC5zaWxlbnQgPSB0cnVlOyAvLyBOb3Qgc2hvdyB0ZXh0IHdoZW4gYW5pbWF0aW5nXG5cbiAgIShvcHQgJiYgb3B0LmtlZXBMYWJlbCkgJiYgKHN5bWJvbFBhdGguc3R5bGUudGV4dCA9IG51bGwpO1xuICBncmFwaGljLnVwZGF0ZVByb3BzKHN5bWJvbFBhdGgsIHtcbiAgICBzdHlsZToge1xuICAgICAgb3BhY2l0eTogMFxuICAgIH0sXG4gICAgc2NhbGU6IFswLCAwXVxuICB9LCB0aGlzLl9zZXJpZXNNb2RlbCwgdGhpcy5kYXRhSW5kZXgsIGNiKTtcbn07XG5cbnpyVXRpbC5pbmhlcml0cyhTeW1ib2xDbHosIGdyYXBoaWMuR3JvdXApO1xudmFyIF9kZWZhdWx0ID0gU3ltYm9sQ2x6O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7QUFJQTs7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/helper/Symbol.js
