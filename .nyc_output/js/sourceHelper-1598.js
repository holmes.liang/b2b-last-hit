/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;
var getDataItemValue = _model.getDataItemValue;

var _referHelper = __webpack_require__(/*! ../../model/referHelper */ "./node_modules/echarts/lib/model/referHelper.js");

var getCoordSysDefineBySeries = _referHelper.getCoordSysDefineBySeries;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var each = _util.each;
var map = _util.map;
var isArray = _util.isArray;
var isString = _util.isString;
var isObject = _util.isObject;
var isTypedArray = _util.isTypedArray;
var isArrayLike = _util.isArrayLike;
var extend = _util.extend;
var assert = _util.assert;

var Source = __webpack_require__(/*! ../Source */ "./node_modules/echarts/lib/data/Source.js");

var _sourceType = __webpack_require__(/*! ./sourceType */ "./node_modules/echarts/lib/data/helper/sourceType.js");

var SOURCE_FORMAT_ORIGINAL = _sourceType.SOURCE_FORMAT_ORIGINAL;
var SOURCE_FORMAT_ARRAY_ROWS = _sourceType.SOURCE_FORMAT_ARRAY_ROWS;
var SOURCE_FORMAT_OBJECT_ROWS = _sourceType.SOURCE_FORMAT_OBJECT_ROWS;
var SOURCE_FORMAT_KEYED_COLUMNS = _sourceType.SOURCE_FORMAT_KEYED_COLUMNS;
var SOURCE_FORMAT_UNKNOWN = _sourceType.SOURCE_FORMAT_UNKNOWN;
var SOURCE_FORMAT_TYPED_ARRAY = _sourceType.SOURCE_FORMAT_TYPED_ARRAY;
var SERIES_LAYOUT_BY_ROW = _sourceType.SERIES_LAYOUT_BY_ROW;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = makeInner();
/**
 * @see {module:echarts/data/Source}
 * @param {module:echarts/component/dataset/DatasetModel} datasetModel
 * @return {string} sourceFormat
 */

function detectSourceFormat(datasetModel) {
  var data = datasetModel.option.source;
  var sourceFormat = SOURCE_FORMAT_UNKNOWN;

  if (isTypedArray(data)) {
    sourceFormat = SOURCE_FORMAT_TYPED_ARRAY;
  } else if (isArray(data)) {
    // FIXME Whether tolerate null in top level array?
    if (data.length === 0) {
      sourceFormat = SOURCE_FORMAT_ARRAY_ROWS;
    }

    for (var i = 0, len = data.length; i < len; i++) {
      var item = data[i];

      if (item == null) {
        continue;
      } else if (isArray(item)) {
        sourceFormat = SOURCE_FORMAT_ARRAY_ROWS;
        break;
      } else if (isObject(item)) {
        sourceFormat = SOURCE_FORMAT_OBJECT_ROWS;
        break;
      }
    }
  } else if (isObject(data)) {
    for (var key in data) {
      if (data.hasOwnProperty(key) && isArrayLike(data[key])) {
        sourceFormat = SOURCE_FORMAT_KEYED_COLUMNS;
        break;
      }
    }
  } else if (data != null) {
    throw new Error('Invalid data');
  }

  inner(datasetModel).sourceFormat = sourceFormat;
}
/**
 * [Scenarios]:
 * (1) Provide source data directly:
 *     series: {
 *         encode: {...},
 *         dimensions: [...]
 *         seriesLayoutBy: 'row',
 *         data: [[...]]
 *     }
 * (2) Refer to datasetModel.
 *     series: [{
 *         encode: {...}
 *         // Ignore datasetIndex means `datasetIndex: 0`
 *         // and the dimensions defination in dataset is used
 *     }, {
 *         encode: {...},
 *         seriesLayoutBy: 'column',
 *         datasetIndex: 1
 *     }]
 *
 * Get data from series itself or datset.
 * @return {module:echarts/data/Source} source
 */


function getSource(seriesModel) {
  return inner(seriesModel).source;
}
/**
 * MUST be called before mergeOption of all series.
 * @param {module:echarts/model/Global} ecModel
 */


function resetSourceDefaulter(ecModel) {
  // `datasetMap` is used to make default encode.
  inner(ecModel).datasetMap = createHashMap();
}
/**
 * [Caution]:
 * MUST be called after series option merged and
 * before "series.getInitailData()" called.
 *
 * [The rule of making default encode]:
 * Category axis (if exists) alway map to the first dimension.
 * Each other axis occupies a subsequent dimension.
 *
 * [Why make default encode]:
 * Simplify the typing of encode in option, avoiding the case like that:
 * series: [{encode: {x: 0, y: 1}}, {encode: {x: 0, y: 2}}, {encode: {x: 0, y: 3}}],
 * where the "y" have to be manually typed as "1, 2, 3, ...".
 *
 * @param {module:echarts/model/Series} seriesModel
 */


function prepareSource(seriesModel) {
  var seriesOption = seriesModel.option;
  var data = seriesOption.data;
  var sourceFormat = isTypedArray(data) ? SOURCE_FORMAT_TYPED_ARRAY : SOURCE_FORMAT_ORIGINAL;
  var fromDataset = false;
  var seriesLayoutBy = seriesOption.seriesLayoutBy;
  var sourceHeader = seriesOption.sourceHeader;
  var dimensionsDefine = seriesOption.dimensions;
  var datasetModel = getDatasetModel(seriesModel);

  if (datasetModel) {
    var datasetOption = datasetModel.option;
    data = datasetOption.source;
    sourceFormat = inner(datasetModel).sourceFormat;
    fromDataset = true; // These settings from series has higher priority.

    seriesLayoutBy = seriesLayoutBy || datasetOption.seriesLayoutBy;
    sourceHeader == null && (sourceHeader = datasetOption.sourceHeader);
    dimensionsDefine = dimensionsDefine || datasetOption.dimensions;
  }

  var completeResult = completeBySourceData(data, sourceFormat, seriesLayoutBy, sourceHeader, dimensionsDefine); // Note: dataset option does not have `encode`.

  var encodeDefine = seriesOption.encode;

  if (!encodeDefine && datasetModel) {
    encodeDefine = makeDefaultEncode(seriesModel, datasetModel, data, sourceFormat, seriesLayoutBy, completeResult);
  }

  inner(seriesModel).source = new Source({
    data: data,
    fromDataset: fromDataset,
    seriesLayoutBy: seriesLayoutBy,
    sourceFormat: sourceFormat,
    dimensionsDefine: completeResult.dimensionsDefine,
    startIndex: completeResult.startIndex,
    dimensionsDetectCount: completeResult.dimensionsDetectCount,
    encodeDefine: encodeDefine
  });
} // return {startIndex, dimensionsDefine, dimensionsCount}


function completeBySourceData(data, sourceFormat, seriesLayoutBy, sourceHeader, dimensionsDefine) {
  if (!data) {
    return {
      dimensionsDefine: normalizeDimensionsDefine(dimensionsDefine)
    };
  }

  var dimensionsDetectCount;
  var startIndex;
  var findPotentialName;

  if (sourceFormat === SOURCE_FORMAT_ARRAY_ROWS) {
    // Rule: Most of the first line are string: it is header.
    // Caution: consider a line with 5 string and 1 number,
    // it still can not be sure it is a head, because the
    // 5 string may be 5 values of category columns.
    if (sourceHeader === 'auto' || sourceHeader == null) {
      arrayRowsTravelFirst(function (val) {
        // '-' is regarded as null/undefined.
        if (val != null && val !== '-') {
          if (isString(val)) {
            startIndex == null && (startIndex = 1);
          } else {
            startIndex = 0;
          }
        } // 10 is an experience number, avoid long loop.

      }, seriesLayoutBy, data, 10);
    } else {
      startIndex = sourceHeader ? 1 : 0;
    }

    if (!dimensionsDefine && startIndex === 1) {
      dimensionsDefine = [];
      arrayRowsTravelFirst(function (val, index) {
        dimensionsDefine[index] = val != null ? val : '';
      }, seriesLayoutBy, data);
    }

    dimensionsDetectCount = dimensionsDefine ? dimensionsDefine.length : seriesLayoutBy === SERIES_LAYOUT_BY_ROW ? data.length : data[0] ? data[0].length : null;
  } else if (sourceFormat === SOURCE_FORMAT_OBJECT_ROWS) {
    if (!dimensionsDefine) {
      dimensionsDefine = objectRowsCollectDimensions(data);
      findPotentialName = true;
    }
  } else if (sourceFormat === SOURCE_FORMAT_KEYED_COLUMNS) {
    if (!dimensionsDefine) {
      dimensionsDefine = [];
      findPotentialName = true;
      each(data, function (colArr, key) {
        dimensionsDefine.push(key);
      });
    }
  } else if (sourceFormat === SOURCE_FORMAT_ORIGINAL) {
    var value0 = getDataItemValue(data[0]);
    dimensionsDetectCount = isArray(value0) && value0.length || 1;
  } else if (sourceFormat === SOURCE_FORMAT_TYPED_ARRAY) {}

  var potentialNameDimIndex;

  if (findPotentialName) {
    each(dimensionsDefine, function (dim, idx) {
      if ((isObject(dim) ? dim.name : dim) === 'name') {
        potentialNameDimIndex = idx;
      }
    });
  }

  return {
    startIndex: startIndex,
    dimensionsDefine: normalizeDimensionsDefine(dimensionsDefine),
    dimensionsDetectCount: dimensionsDetectCount,
    potentialNameDimIndex: potentialNameDimIndex // TODO: potentialIdDimIdx

  };
} // Consider dimensions defined like ['A', 'price', 'B', 'price', 'C', 'price'],
// which is reasonable. But dimension name is duplicated.
// Returns undefined or an array contains only object without null/undefiend or string.


function normalizeDimensionsDefine(dimensionsDefine) {
  if (!dimensionsDefine) {
    // The meaning of null/undefined is different from empty array.
    return;
  }

  var nameMap = createHashMap();
  return map(dimensionsDefine, function (item, index) {
    item = extend({}, isObject(item) ? item : {
      name: item
    }); // User can set null in dimensions.
    // We dont auto specify name, othewise a given name may
    // cause it be refered unexpectedly.

    if (item.name == null) {
      return item;
    } // Also consider number form like 2012.


    item.name += ''; // User may also specify displayName.
    // displayName will always exists except user not
    // specified or dim name is not specified or detected.
    // (A auto generated dim name will not be used as
    // displayName).

    if (item.displayName == null) {
      item.displayName = item.name;
    }

    var exist = nameMap.get(item.name);

    if (!exist) {
      nameMap.set(item.name, {
        count: 1
      });
    } else {
      item.name += '-' + exist.count++;
    }

    return item;
  });
}

function arrayRowsTravelFirst(cb, seriesLayoutBy, data, maxLoop) {
  maxLoop == null && (maxLoop = Infinity);

  if (seriesLayoutBy === SERIES_LAYOUT_BY_ROW) {
    for (var i = 0; i < data.length && i < maxLoop; i++) {
      cb(data[i] ? data[i][0] : null, i);
    }
  } else {
    var value0 = data[0] || [];

    for (var i = 0; i < value0.length && i < maxLoop; i++) {
      cb(value0[i], i);
    }
  }
}

function objectRowsCollectDimensions(data) {
  var firstIndex = 0;
  var obj;

  while (firstIndex < data.length && !(obj = data[firstIndex++])) {} // jshint ignore: line


  if (obj) {
    var dimensions = [];
    each(obj, function (value, key) {
      dimensions.push(key);
    });
    return dimensions;
  }
} // ??? TODO merge to completedimensions, where also has
// default encode making logic. And the default rule
// should depends on series? consider 'map'.


function makeDefaultEncode(seriesModel, datasetModel, data, sourceFormat, seriesLayoutBy, completeResult) {
  var coordSysDefine = getCoordSysDefineBySeries(seriesModel);
  var encode = {}; // var encodeTooltip = [];
  // var encodeLabel = [];

  var encodeItemName = [];
  var encodeSeriesName = [];
  var seriesType = seriesModel.subType; // ??? TODO refactor: provide by series itself.
  // Consider the case: 'map' series is based on geo coordSys,
  // 'graph', 'heatmap' can be based on cartesian. But can not
  // give default rule simply here.

  var nSeriesMap = createHashMap(['pie', 'map', 'funnel']);
  var cSeriesMap = createHashMap(['line', 'bar', 'pictorialBar', 'scatter', 'effectScatter', 'candlestick', 'boxplot']); // Usually in this case series will use the first data
  // dimension as the "value" dimension, or other default
  // processes respectively.

  if (coordSysDefine && cSeriesMap.get(seriesType) != null) {
    var ecModel = seriesModel.ecModel;
    var datasetMap = inner(ecModel).datasetMap;
    var key = datasetModel.uid + '_' + seriesLayoutBy;
    var datasetRecord = datasetMap.get(key) || datasetMap.set(key, {
      categoryWayDim: 1,
      valueWayDim: 0
    }); // TODO
    // Auto detect first time axis and do arrangement.

    each(coordSysDefine.coordSysDims, function (coordDim) {
      // In value way.
      if (coordSysDefine.firstCategoryDimIndex == null) {
        var dataDim = datasetRecord.valueWayDim++;
        encode[coordDim] = dataDim; // ??? TODO give a better default series name rule?
        // especially when encode x y specified.
        // consider: when mutiple series share one dimension
        // category axis, series name should better use
        // the other dimsion name. On the other hand, use
        // both dimensions name.

        encodeSeriesName.push(dataDim); // encodeTooltip.push(dataDim);
        // encodeLabel.push(dataDim);
      } // In category way, category axis.
      else if (coordSysDefine.categoryAxisMap.get(coordDim)) {
          encode[coordDim] = 0;
          encodeItemName.push(0);
        } // In category way, non-category axis.
        else {
            var dataDim = datasetRecord.categoryWayDim++;
            encode[coordDim] = dataDim; // encodeTooltip.push(dataDim);
            // encodeLabel.push(dataDim);

            encodeSeriesName.push(dataDim);
          }
    });
  } // Do not make a complex rule! Hard to code maintain and not necessary.
  // ??? TODO refactor: provide by series itself.
  // [{name: ..., value: ...}, ...] like:
  else if (nSeriesMap.get(seriesType) != null) {
      // Find the first not ordinal. (5 is an experience value)
      var firstNotOrdinal;

      for (var i = 0; i < 5 && firstNotOrdinal == null; i++) {
        if (!doGuessOrdinal(data, sourceFormat, seriesLayoutBy, completeResult.dimensionsDefine, completeResult.startIndex, i)) {
          firstNotOrdinal = i;
        }
      }

      if (firstNotOrdinal != null) {
        encode.value = firstNotOrdinal;
        var nameDimIndex = completeResult.potentialNameDimIndex || Math.max(firstNotOrdinal - 1, 0); // By default, label use itemName in charts.
        // So we dont set encodeLabel here.

        encodeSeriesName.push(nameDimIndex);
        encodeItemName.push(nameDimIndex); // encodeTooltip.push(firstNotOrdinal);
      }
    } // encodeTooltip.length && (encode.tooltip = encodeTooltip);
  // encodeLabel.length && (encode.label = encodeLabel);


  encodeItemName.length && (encode.itemName = encodeItemName);
  encodeSeriesName.length && (encode.seriesName = encodeSeriesName);
  return encode;
}
/**
 * If return null/undefined, indicate that should not use datasetModel.
 */


function getDatasetModel(seriesModel) {
  var option = seriesModel.option; // Caution: consider the scenario:
  // A dataset is declared and a series is not expected to use the dataset,
  // and at the beginning `setOption({series: { noData })` (just prepare other
  // option but no data), then `setOption({series: {data: [...]}); In this case,
  // the user should set an empty array to avoid that dataset is used by default.

  var thisData = option.data;

  if (!thisData) {
    return seriesModel.ecModel.getComponent('dataset', option.datasetIndex || 0);
  }
}
/**
 * The rule should not be complex, otherwise user might not
 * be able to known where the data is wrong.
 * The code is ugly, but how to make it neat?
 *
 * @param {module:echars/data/Source} source
 * @param {number} dimIndex
 * @return {boolean} Whether ordinal.
 */


function guessOrdinal(source, dimIndex) {
  return doGuessOrdinal(source.data, source.sourceFormat, source.seriesLayoutBy, source.dimensionsDefine, source.startIndex, dimIndex);
} // dimIndex may be overflow source data.


function doGuessOrdinal(data, sourceFormat, seriesLayoutBy, dimensionsDefine, startIndex, dimIndex) {
  var result; // Experience value.

  var maxLoop = 5;

  if (isTypedArray(data)) {
    return false;
  } // When sourceType is 'objectRows' or 'keyedColumns', dimensionsDefine
  // always exists in source.


  var dimName;

  if (dimensionsDefine) {
    dimName = dimensionsDefine[dimIndex];
    dimName = isObject(dimName) ? dimName.name : dimName;
  }

  if (sourceFormat === SOURCE_FORMAT_ARRAY_ROWS) {
    if (seriesLayoutBy === SERIES_LAYOUT_BY_ROW) {
      var sample = data[dimIndex];

      for (var i = 0; i < (sample || []).length && i < maxLoop; i++) {
        if ((result = detectValue(sample[startIndex + i])) != null) {
          return result;
        }
      }
    } else {
      for (var i = 0; i < data.length && i < maxLoop; i++) {
        var row = data[startIndex + i];

        if (row && (result = detectValue(row[dimIndex])) != null) {
          return result;
        }
      }
    }
  } else if (sourceFormat === SOURCE_FORMAT_OBJECT_ROWS) {
    if (!dimName) {
      return;
    }

    for (var i = 0; i < data.length && i < maxLoop; i++) {
      var item = data[i];

      if (item && (result = detectValue(item[dimName])) != null) {
        return result;
      }
    }
  } else if (sourceFormat === SOURCE_FORMAT_KEYED_COLUMNS) {
    if (!dimName) {
      return;
    }

    var sample = data[dimName];

    if (!sample || isTypedArray(sample)) {
      return false;
    }

    for (var i = 0; i < sample.length && i < maxLoop; i++) {
      if ((result = detectValue(sample[i])) != null) {
        return result;
      }
    }
  } else if (sourceFormat === SOURCE_FORMAT_ORIGINAL) {
    for (var i = 0; i < data.length && i < maxLoop; i++) {
      var item = data[i];
      var val = getDataItemValue(item);

      if (!isArray(val)) {
        return false;
      }

      if ((result = detectValue(val[dimIndex])) != null) {
        return result;
      }
    }
  }

  function detectValue(val) {
    // Consider usage convenience, '1', '2' will be treated as "number".
    // `isFinit('')` get `true`.
    if (val != null && isFinite(val) && val !== '') {
      return false;
    } else if (isString(val) && val !== '-') {
      return true;
    }
  }

  return false;
}

exports.detectSourceFormat = detectSourceFormat;
exports.getSource = getSource;
exports.resetSourceDefaulter = resetSourceDefaulter;
exports.prepareSource = prepareSource;
exports.guessOrdinal = guessOrdinal;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvc291cmNlSGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvc291cmNlSGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgX21vZGVsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBtYWtlSW5uZXIgPSBfbW9kZWwubWFrZUlubmVyO1xudmFyIGdldERhdGFJdGVtVmFsdWUgPSBfbW9kZWwuZ2V0RGF0YUl0ZW1WYWx1ZTtcblxudmFyIF9yZWZlckhlbHBlciA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9yZWZlckhlbHBlclwiKTtcblxudmFyIGdldENvb3JkU3lzRGVmaW5lQnlTZXJpZXMgPSBfcmVmZXJIZWxwZXIuZ2V0Q29vcmRTeXNEZWZpbmVCeVNlcmllcztcblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGNyZWF0ZUhhc2hNYXAgPSBfdXRpbC5jcmVhdGVIYXNoTWFwO1xudmFyIGVhY2ggPSBfdXRpbC5lYWNoO1xudmFyIG1hcCA9IF91dGlsLm1hcDtcbnZhciBpc0FycmF5ID0gX3V0aWwuaXNBcnJheTtcbnZhciBpc1N0cmluZyA9IF91dGlsLmlzU3RyaW5nO1xudmFyIGlzT2JqZWN0ID0gX3V0aWwuaXNPYmplY3Q7XG52YXIgaXNUeXBlZEFycmF5ID0gX3V0aWwuaXNUeXBlZEFycmF5O1xudmFyIGlzQXJyYXlMaWtlID0gX3V0aWwuaXNBcnJheUxpa2U7XG52YXIgZXh0ZW5kID0gX3V0aWwuZXh0ZW5kO1xudmFyIGFzc2VydCA9IF91dGlsLmFzc2VydDtcblxudmFyIFNvdXJjZSA9IHJlcXVpcmUoXCIuLi9Tb3VyY2VcIik7XG5cbnZhciBfc291cmNlVHlwZSA9IHJlcXVpcmUoXCIuL3NvdXJjZVR5cGVcIik7XG5cbnZhciBTT1VSQ0VfRk9STUFUX09SSUdJTkFMID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9PUklHSU5BTDtcbnZhciBTT1VSQ0VfRk9STUFUX0FSUkFZX1JPV1MgPSBfc291cmNlVHlwZS5TT1VSQ0VfRk9STUFUX0FSUkFZX1JPV1M7XG52YXIgU09VUkNFX0ZPUk1BVF9PQkpFQ1RfUk9XUyA9IF9zb3VyY2VUeXBlLlNPVVJDRV9GT1JNQVRfT0JKRUNUX1JPV1M7XG52YXIgU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TO1xudmFyIFNPVVJDRV9GT1JNQVRfVU5LTk9XTiA9IF9zb3VyY2VUeXBlLlNPVVJDRV9GT1JNQVRfVU5LTk9XTjtcbnZhciBTT1VSQ0VfRk9STUFUX1RZUEVEX0FSUkFZID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9UWVBFRF9BUlJBWTtcbnZhciBTRVJJRVNfTEFZT1VUX0JZX1JPVyA9IF9zb3VyY2VUeXBlLlNFUklFU19MQVlPVVRfQllfUk9XO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgaW5uZXIgPSBtYWtlSW5uZXIoKTtcbi8qKlxuICogQHNlZSB7bW9kdWxlOmVjaGFydHMvZGF0YS9Tb3VyY2V9XG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2NvbXBvbmVudC9kYXRhc2V0L0RhdGFzZXRNb2RlbH0gZGF0YXNldE1vZGVsXG4gKiBAcmV0dXJuIHtzdHJpbmd9IHNvdXJjZUZvcm1hdFxuICovXG5cbmZ1bmN0aW9uIGRldGVjdFNvdXJjZUZvcm1hdChkYXRhc2V0TW9kZWwpIHtcbiAgdmFyIGRhdGEgPSBkYXRhc2V0TW9kZWwub3B0aW9uLnNvdXJjZTtcbiAgdmFyIHNvdXJjZUZvcm1hdCA9IFNPVVJDRV9GT1JNQVRfVU5LTk9XTjtcblxuICBpZiAoaXNUeXBlZEFycmF5KGRhdGEpKSB7XG4gICAgc291cmNlRm9ybWF0ID0gU09VUkNFX0ZPUk1BVF9UWVBFRF9BUlJBWTtcbiAgfSBlbHNlIGlmIChpc0FycmF5KGRhdGEpKSB7XG4gICAgLy8gRklYTUUgV2hldGhlciB0b2xlcmF0ZSBudWxsIGluIHRvcCBsZXZlbCBhcnJheT9cbiAgICBpZiAoZGF0YS5sZW5ndGggPT09IDApIHtcbiAgICAgIHNvdXJjZUZvcm1hdCA9IFNPVVJDRV9GT1JNQVRfQVJSQVlfUk9XUztcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gZGF0YS5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgdmFyIGl0ZW0gPSBkYXRhW2ldO1xuXG4gICAgICBpZiAoaXRlbSA9PSBudWxsKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfSBlbHNlIGlmIChpc0FycmF5KGl0ZW0pKSB7XG4gICAgICAgIHNvdXJjZUZvcm1hdCA9IFNPVVJDRV9GT1JNQVRfQVJSQVlfUk9XUztcbiAgICAgICAgYnJlYWs7XG4gICAgICB9IGVsc2UgaWYgKGlzT2JqZWN0KGl0ZW0pKSB7XG4gICAgICAgIHNvdXJjZUZvcm1hdCA9IFNPVVJDRV9GT1JNQVRfT0JKRUNUX1JPV1M7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmIChpc09iamVjdChkYXRhKSkge1xuICAgIGZvciAodmFyIGtleSBpbiBkYXRhKSB7XG4gICAgICBpZiAoZGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIGlzQXJyYXlMaWtlKGRhdGFba2V5XSkpIHtcbiAgICAgICAgc291cmNlRm9ybWF0ID0gU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSBpZiAoZGF0YSAhPSBudWxsKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGRhdGEnKTtcbiAgfVxuXG4gIGlubmVyKGRhdGFzZXRNb2RlbCkuc291cmNlRm9ybWF0ID0gc291cmNlRm9ybWF0O1xufVxuLyoqXG4gKiBbU2NlbmFyaW9zXTpcbiAqICgxKSBQcm92aWRlIHNvdXJjZSBkYXRhIGRpcmVjdGx5OlxuICogICAgIHNlcmllczoge1xuICogICAgICAgICBlbmNvZGU6IHsuLi59LFxuICogICAgICAgICBkaW1lbnNpb25zOiBbLi4uXVxuICogICAgICAgICBzZXJpZXNMYXlvdXRCeTogJ3JvdycsXG4gKiAgICAgICAgIGRhdGE6IFtbLi4uXV1cbiAqICAgICB9XG4gKiAoMikgUmVmZXIgdG8gZGF0YXNldE1vZGVsLlxuICogICAgIHNlcmllczogW3tcbiAqICAgICAgICAgZW5jb2RlOiB7Li4ufVxuICogICAgICAgICAvLyBJZ25vcmUgZGF0YXNldEluZGV4IG1lYW5zIGBkYXRhc2V0SW5kZXg6IDBgXG4gKiAgICAgICAgIC8vIGFuZCB0aGUgZGltZW5zaW9ucyBkZWZpbmF0aW9uIGluIGRhdGFzZXQgaXMgdXNlZFxuICogICAgIH0sIHtcbiAqICAgICAgICAgZW5jb2RlOiB7Li4ufSxcbiAqICAgICAgICAgc2VyaWVzTGF5b3V0Qnk6ICdjb2x1bW4nLFxuICogICAgICAgICBkYXRhc2V0SW5kZXg6IDFcbiAqICAgICB9XVxuICpcbiAqIEdldCBkYXRhIGZyb20gc2VyaWVzIGl0c2VsZiBvciBkYXRzZXQuXG4gKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL1NvdXJjZX0gc291cmNlXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRTb3VyY2Uoc2VyaWVzTW9kZWwpIHtcbiAgcmV0dXJuIGlubmVyKHNlcmllc01vZGVsKS5zb3VyY2U7XG59XG4vKipcbiAqIE1VU1QgYmUgY2FsbGVkIGJlZm9yZSBtZXJnZU9wdGlvbiBvZiBhbGwgc2VyaWVzLlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAqL1xuXG5cbmZ1bmN0aW9uIHJlc2V0U291cmNlRGVmYXVsdGVyKGVjTW9kZWwpIHtcbiAgLy8gYGRhdGFzZXRNYXBgIGlzIHVzZWQgdG8gbWFrZSBkZWZhdWx0IGVuY29kZS5cbiAgaW5uZXIoZWNNb2RlbCkuZGF0YXNldE1hcCA9IGNyZWF0ZUhhc2hNYXAoKTtcbn1cbi8qKlxuICogW0NhdXRpb25dOlxuICogTVVTVCBiZSBjYWxsZWQgYWZ0ZXIgc2VyaWVzIG9wdGlvbiBtZXJnZWQgYW5kXG4gKiBiZWZvcmUgXCJzZXJpZXMuZ2V0SW5pdGFpbERhdGEoKVwiIGNhbGxlZC5cbiAqXG4gKiBbVGhlIHJ1bGUgb2YgbWFraW5nIGRlZmF1bHQgZW5jb2RlXTpcbiAqIENhdGVnb3J5IGF4aXMgKGlmIGV4aXN0cykgYWx3YXkgbWFwIHRvIHRoZSBmaXJzdCBkaW1lbnNpb24uXG4gKiBFYWNoIG90aGVyIGF4aXMgb2NjdXBpZXMgYSBzdWJzZXF1ZW50IGRpbWVuc2lvbi5cbiAqXG4gKiBbV2h5IG1ha2UgZGVmYXVsdCBlbmNvZGVdOlxuICogU2ltcGxpZnkgdGhlIHR5cGluZyBvZiBlbmNvZGUgaW4gb3B0aW9uLCBhdm9pZGluZyB0aGUgY2FzZSBsaWtlIHRoYXQ6XG4gKiBzZXJpZXM6IFt7ZW5jb2RlOiB7eDogMCwgeTogMX19LCB7ZW5jb2RlOiB7eDogMCwgeTogMn19LCB7ZW5jb2RlOiB7eDogMCwgeTogM319XSxcbiAqIHdoZXJlIHRoZSBcInlcIiBoYXZlIHRvIGJlIG1hbnVhbGx5IHR5cGVkIGFzIFwiMSwgMiwgMywgLi4uXCIuXG4gKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXN9IHNlcmllc01vZGVsXG4gKi9cblxuXG5mdW5jdGlvbiBwcmVwYXJlU291cmNlKHNlcmllc01vZGVsKSB7XG4gIHZhciBzZXJpZXNPcHRpb24gPSBzZXJpZXNNb2RlbC5vcHRpb247XG4gIHZhciBkYXRhID0gc2VyaWVzT3B0aW9uLmRhdGE7XG4gIHZhciBzb3VyY2VGb3JtYXQgPSBpc1R5cGVkQXJyYXkoZGF0YSkgPyBTT1VSQ0VfRk9STUFUX1RZUEVEX0FSUkFZIDogU09VUkNFX0ZPUk1BVF9PUklHSU5BTDtcbiAgdmFyIGZyb21EYXRhc2V0ID0gZmFsc2U7XG4gIHZhciBzZXJpZXNMYXlvdXRCeSA9IHNlcmllc09wdGlvbi5zZXJpZXNMYXlvdXRCeTtcbiAgdmFyIHNvdXJjZUhlYWRlciA9IHNlcmllc09wdGlvbi5zb3VyY2VIZWFkZXI7XG4gIHZhciBkaW1lbnNpb25zRGVmaW5lID0gc2VyaWVzT3B0aW9uLmRpbWVuc2lvbnM7XG4gIHZhciBkYXRhc2V0TW9kZWwgPSBnZXREYXRhc2V0TW9kZWwoc2VyaWVzTW9kZWwpO1xuXG4gIGlmIChkYXRhc2V0TW9kZWwpIHtcbiAgICB2YXIgZGF0YXNldE9wdGlvbiA9IGRhdGFzZXRNb2RlbC5vcHRpb247XG4gICAgZGF0YSA9IGRhdGFzZXRPcHRpb24uc291cmNlO1xuICAgIHNvdXJjZUZvcm1hdCA9IGlubmVyKGRhdGFzZXRNb2RlbCkuc291cmNlRm9ybWF0O1xuICAgIGZyb21EYXRhc2V0ID0gdHJ1ZTsgLy8gVGhlc2Ugc2V0dGluZ3MgZnJvbSBzZXJpZXMgaGFzIGhpZ2hlciBwcmlvcml0eS5cblxuICAgIHNlcmllc0xheW91dEJ5ID0gc2VyaWVzTGF5b3V0QnkgfHwgZGF0YXNldE9wdGlvbi5zZXJpZXNMYXlvdXRCeTtcbiAgICBzb3VyY2VIZWFkZXIgPT0gbnVsbCAmJiAoc291cmNlSGVhZGVyID0gZGF0YXNldE9wdGlvbi5zb3VyY2VIZWFkZXIpO1xuICAgIGRpbWVuc2lvbnNEZWZpbmUgPSBkaW1lbnNpb25zRGVmaW5lIHx8IGRhdGFzZXRPcHRpb24uZGltZW5zaW9ucztcbiAgfVxuXG4gIHZhciBjb21wbGV0ZVJlc3VsdCA9IGNvbXBsZXRlQnlTb3VyY2VEYXRhKGRhdGEsIHNvdXJjZUZvcm1hdCwgc2VyaWVzTGF5b3V0QnksIHNvdXJjZUhlYWRlciwgZGltZW5zaW9uc0RlZmluZSk7IC8vIE5vdGU6IGRhdGFzZXQgb3B0aW9uIGRvZXMgbm90IGhhdmUgYGVuY29kZWAuXG5cbiAgdmFyIGVuY29kZURlZmluZSA9IHNlcmllc09wdGlvbi5lbmNvZGU7XG5cbiAgaWYgKCFlbmNvZGVEZWZpbmUgJiYgZGF0YXNldE1vZGVsKSB7XG4gICAgZW5jb2RlRGVmaW5lID0gbWFrZURlZmF1bHRFbmNvZGUoc2VyaWVzTW9kZWwsIGRhdGFzZXRNb2RlbCwgZGF0YSwgc291cmNlRm9ybWF0LCBzZXJpZXNMYXlvdXRCeSwgY29tcGxldGVSZXN1bHQpO1xuICB9XG5cbiAgaW5uZXIoc2VyaWVzTW9kZWwpLnNvdXJjZSA9IG5ldyBTb3VyY2Uoe1xuICAgIGRhdGE6IGRhdGEsXG4gICAgZnJvbURhdGFzZXQ6IGZyb21EYXRhc2V0LFxuICAgIHNlcmllc0xheW91dEJ5OiBzZXJpZXNMYXlvdXRCeSxcbiAgICBzb3VyY2VGb3JtYXQ6IHNvdXJjZUZvcm1hdCxcbiAgICBkaW1lbnNpb25zRGVmaW5lOiBjb21wbGV0ZVJlc3VsdC5kaW1lbnNpb25zRGVmaW5lLFxuICAgIHN0YXJ0SW5kZXg6IGNvbXBsZXRlUmVzdWx0LnN0YXJ0SW5kZXgsXG4gICAgZGltZW5zaW9uc0RldGVjdENvdW50OiBjb21wbGV0ZVJlc3VsdC5kaW1lbnNpb25zRGV0ZWN0Q291bnQsXG4gICAgZW5jb2RlRGVmaW5lOiBlbmNvZGVEZWZpbmVcbiAgfSk7XG59IC8vIHJldHVybiB7c3RhcnRJbmRleCwgZGltZW5zaW9uc0RlZmluZSwgZGltZW5zaW9uc0NvdW50fVxuXG5cbmZ1bmN0aW9uIGNvbXBsZXRlQnlTb3VyY2VEYXRhKGRhdGEsIHNvdXJjZUZvcm1hdCwgc2VyaWVzTGF5b3V0QnksIHNvdXJjZUhlYWRlciwgZGltZW5zaW9uc0RlZmluZSkge1xuICBpZiAoIWRhdGEpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZGltZW5zaW9uc0RlZmluZTogbm9ybWFsaXplRGltZW5zaW9uc0RlZmluZShkaW1lbnNpb25zRGVmaW5lKVxuICAgIH07XG4gIH1cblxuICB2YXIgZGltZW5zaW9uc0RldGVjdENvdW50O1xuICB2YXIgc3RhcnRJbmRleDtcbiAgdmFyIGZpbmRQb3RlbnRpYWxOYW1lO1xuXG4gIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfQVJSQVlfUk9XUykge1xuICAgIC8vIFJ1bGU6IE1vc3Qgb2YgdGhlIGZpcnN0IGxpbmUgYXJlIHN0cmluZzogaXQgaXMgaGVhZGVyLlxuICAgIC8vIENhdXRpb246IGNvbnNpZGVyIGEgbGluZSB3aXRoIDUgc3RyaW5nIGFuZCAxIG51bWJlcixcbiAgICAvLyBpdCBzdGlsbCBjYW4gbm90IGJlIHN1cmUgaXQgaXMgYSBoZWFkLCBiZWNhdXNlIHRoZVxuICAgIC8vIDUgc3RyaW5nIG1heSBiZSA1IHZhbHVlcyBvZiBjYXRlZ29yeSBjb2x1bW5zLlxuICAgIGlmIChzb3VyY2VIZWFkZXIgPT09ICdhdXRvJyB8fCBzb3VyY2VIZWFkZXIgPT0gbnVsbCkge1xuICAgICAgYXJyYXlSb3dzVHJhdmVsRmlyc3QoZnVuY3Rpb24gKHZhbCkge1xuICAgICAgICAvLyAnLScgaXMgcmVnYXJkZWQgYXMgbnVsbC91bmRlZmluZWQuXG4gICAgICAgIGlmICh2YWwgIT0gbnVsbCAmJiB2YWwgIT09ICctJykge1xuICAgICAgICAgIGlmIChpc1N0cmluZyh2YWwpKSB7XG4gICAgICAgICAgICBzdGFydEluZGV4ID09IG51bGwgJiYgKHN0YXJ0SW5kZXggPSAxKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc3RhcnRJbmRleCA9IDA7XG4gICAgICAgICAgfVxuICAgICAgICB9IC8vIDEwIGlzIGFuIGV4cGVyaWVuY2UgbnVtYmVyLCBhdm9pZCBsb25nIGxvb3AuXG5cbiAgICAgIH0sIHNlcmllc0xheW91dEJ5LCBkYXRhLCAxMCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN0YXJ0SW5kZXggPSBzb3VyY2VIZWFkZXIgPyAxIDogMDtcbiAgICB9XG5cbiAgICBpZiAoIWRpbWVuc2lvbnNEZWZpbmUgJiYgc3RhcnRJbmRleCA9PT0gMSkge1xuICAgICAgZGltZW5zaW9uc0RlZmluZSA9IFtdO1xuICAgICAgYXJyYXlSb3dzVHJhdmVsRmlyc3QoZnVuY3Rpb24gKHZhbCwgaW5kZXgpIHtcbiAgICAgICAgZGltZW5zaW9uc0RlZmluZVtpbmRleF0gPSB2YWwgIT0gbnVsbCA/IHZhbCA6ICcnO1xuICAgICAgfSwgc2VyaWVzTGF5b3V0QnksIGRhdGEpO1xuICAgIH1cblxuICAgIGRpbWVuc2lvbnNEZXRlY3RDb3VudCA9IGRpbWVuc2lvbnNEZWZpbmUgPyBkaW1lbnNpb25zRGVmaW5lLmxlbmd0aCA6IHNlcmllc0xheW91dEJ5ID09PSBTRVJJRVNfTEFZT1VUX0JZX1JPVyA/IGRhdGEubGVuZ3RoIDogZGF0YVswXSA/IGRhdGFbMF0ubGVuZ3RoIDogbnVsbDtcbiAgfSBlbHNlIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfT0JKRUNUX1JPV1MpIHtcbiAgICBpZiAoIWRpbWVuc2lvbnNEZWZpbmUpIHtcbiAgICAgIGRpbWVuc2lvbnNEZWZpbmUgPSBvYmplY3RSb3dzQ29sbGVjdERpbWVuc2lvbnMoZGF0YSk7XG4gICAgICBmaW5kUG90ZW50aWFsTmFtZSA9IHRydWU7XG4gICAgfVxuICB9IGVsc2UgaWYgKHNvdXJjZUZvcm1hdCA9PT0gU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TKSB7XG4gICAgaWYgKCFkaW1lbnNpb25zRGVmaW5lKSB7XG4gICAgICBkaW1lbnNpb25zRGVmaW5lID0gW107XG4gICAgICBmaW5kUG90ZW50aWFsTmFtZSA9IHRydWU7XG4gICAgICBlYWNoKGRhdGEsIGZ1bmN0aW9uIChjb2xBcnIsIGtleSkge1xuICAgICAgICBkaW1lbnNpb25zRGVmaW5lLnB1c2goa2V5KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSBlbHNlIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfT1JJR0lOQUwpIHtcbiAgICB2YXIgdmFsdWUwID0gZ2V0RGF0YUl0ZW1WYWx1ZShkYXRhWzBdKTtcbiAgICBkaW1lbnNpb25zRGV0ZWN0Q291bnQgPSBpc0FycmF5KHZhbHVlMCkgJiYgdmFsdWUwLmxlbmd0aCB8fCAxO1xuICB9IGVsc2UgaWYgKHNvdXJjZUZvcm1hdCA9PT0gU09VUkNFX0ZPUk1BVF9UWVBFRF9BUlJBWSkge31cblxuICB2YXIgcG90ZW50aWFsTmFtZURpbUluZGV4O1xuXG4gIGlmIChmaW5kUG90ZW50aWFsTmFtZSkge1xuICAgIGVhY2goZGltZW5zaW9uc0RlZmluZSwgZnVuY3Rpb24gKGRpbSwgaWR4KSB7XG4gICAgICBpZiAoKGlzT2JqZWN0KGRpbSkgPyBkaW0ubmFtZSA6IGRpbSkgPT09ICduYW1lJykge1xuICAgICAgICBwb3RlbnRpYWxOYW1lRGltSW5kZXggPSBpZHg7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICByZXR1cm4ge1xuICAgIHN0YXJ0SW5kZXg6IHN0YXJ0SW5kZXgsXG4gICAgZGltZW5zaW9uc0RlZmluZTogbm9ybWFsaXplRGltZW5zaW9uc0RlZmluZShkaW1lbnNpb25zRGVmaW5lKSxcbiAgICBkaW1lbnNpb25zRGV0ZWN0Q291bnQ6IGRpbWVuc2lvbnNEZXRlY3RDb3VudCxcbiAgICBwb3RlbnRpYWxOYW1lRGltSW5kZXg6IHBvdGVudGlhbE5hbWVEaW1JbmRleCAvLyBUT0RPOiBwb3RlbnRpYWxJZERpbUlkeFxuXG4gIH07XG59IC8vIENvbnNpZGVyIGRpbWVuc2lvbnMgZGVmaW5lZCBsaWtlIFsnQScsICdwcmljZScsICdCJywgJ3ByaWNlJywgJ0MnLCAncHJpY2UnXSxcbi8vIHdoaWNoIGlzIHJlYXNvbmFibGUuIEJ1dCBkaW1lbnNpb24gbmFtZSBpcyBkdXBsaWNhdGVkLlxuLy8gUmV0dXJucyB1bmRlZmluZWQgb3IgYW4gYXJyYXkgY29udGFpbnMgb25seSBvYmplY3Qgd2l0aG91dCBudWxsL3VuZGVmaWVuZCBvciBzdHJpbmcuXG5cblxuZnVuY3Rpb24gbm9ybWFsaXplRGltZW5zaW9uc0RlZmluZShkaW1lbnNpb25zRGVmaW5lKSB7XG4gIGlmICghZGltZW5zaW9uc0RlZmluZSkge1xuICAgIC8vIFRoZSBtZWFuaW5nIG9mIG51bGwvdW5kZWZpbmVkIGlzIGRpZmZlcmVudCBmcm9tIGVtcHR5IGFycmF5LlxuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBuYW1lTWFwID0gY3JlYXRlSGFzaE1hcCgpO1xuICByZXR1cm4gbWFwKGRpbWVuc2lvbnNEZWZpbmUsIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xuICAgIGl0ZW0gPSBleHRlbmQoe30sIGlzT2JqZWN0KGl0ZW0pID8gaXRlbSA6IHtcbiAgICAgIG5hbWU6IGl0ZW1cbiAgICB9KTsgLy8gVXNlciBjYW4gc2V0IG51bGwgaW4gZGltZW5zaW9ucy5cbiAgICAvLyBXZSBkb250IGF1dG8gc3BlY2lmeSBuYW1lLCBvdGhld2lzZSBhIGdpdmVuIG5hbWUgbWF5XG4gICAgLy8gY2F1c2UgaXQgYmUgcmVmZXJlZCB1bmV4cGVjdGVkbHkuXG5cbiAgICBpZiAoaXRlbS5uYW1lID09IG51bGwpIHtcbiAgICAgIHJldHVybiBpdGVtO1xuICAgIH0gLy8gQWxzbyBjb25zaWRlciBudW1iZXIgZm9ybSBsaWtlIDIwMTIuXG5cblxuICAgIGl0ZW0ubmFtZSArPSAnJzsgLy8gVXNlciBtYXkgYWxzbyBzcGVjaWZ5IGRpc3BsYXlOYW1lLlxuICAgIC8vIGRpc3BsYXlOYW1lIHdpbGwgYWx3YXlzIGV4aXN0cyBleGNlcHQgdXNlciBub3RcbiAgICAvLyBzcGVjaWZpZWQgb3IgZGltIG5hbWUgaXMgbm90IHNwZWNpZmllZCBvciBkZXRlY3RlZC5cbiAgICAvLyAoQSBhdXRvIGdlbmVyYXRlZCBkaW0gbmFtZSB3aWxsIG5vdCBiZSB1c2VkIGFzXG4gICAgLy8gZGlzcGxheU5hbWUpLlxuXG4gICAgaWYgKGl0ZW0uZGlzcGxheU5hbWUgPT0gbnVsbCkge1xuICAgICAgaXRlbS5kaXNwbGF5TmFtZSA9IGl0ZW0ubmFtZTtcbiAgICB9XG5cbiAgICB2YXIgZXhpc3QgPSBuYW1lTWFwLmdldChpdGVtLm5hbWUpO1xuXG4gICAgaWYgKCFleGlzdCkge1xuICAgICAgbmFtZU1hcC5zZXQoaXRlbS5uYW1lLCB7XG4gICAgICAgIGNvdW50OiAxXG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgaXRlbS5uYW1lICs9ICctJyArIGV4aXN0LmNvdW50Kys7XG4gICAgfVxuXG4gICAgcmV0dXJuIGl0ZW07XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBhcnJheVJvd3NUcmF2ZWxGaXJzdChjYiwgc2VyaWVzTGF5b3V0QnksIGRhdGEsIG1heExvb3ApIHtcbiAgbWF4TG9vcCA9PSBudWxsICYmIChtYXhMb29wID0gSW5maW5pdHkpO1xuXG4gIGlmIChzZXJpZXNMYXlvdXRCeSA9PT0gU0VSSUVTX0xBWU9VVF9CWV9ST1cpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoICYmIGkgPCBtYXhMb29wOyBpKyspIHtcbiAgICAgIGNiKGRhdGFbaV0gPyBkYXRhW2ldWzBdIDogbnVsbCwgaSk7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIHZhciB2YWx1ZTAgPSBkYXRhWzBdIHx8IFtdO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2YWx1ZTAubGVuZ3RoICYmIGkgPCBtYXhMb29wOyBpKyspIHtcbiAgICAgIGNiKHZhbHVlMFtpXSwgaSk7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIG9iamVjdFJvd3NDb2xsZWN0RGltZW5zaW9ucyhkYXRhKSB7XG4gIHZhciBmaXJzdEluZGV4ID0gMDtcbiAgdmFyIG9iajtcblxuICB3aGlsZSAoZmlyc3RJbmRleCA8IGRhdGEubGVuZ3RoICYmICEob2JqID0gZGF0YVtmaXJzdEluZGV4KytdKSkge30gLy8ganNoaW50IGlnbm9yZTogbGluZVxuXG5cbiAgaWYgKG9iaikge1xuICAgIHZhciBkaW1lbnNpb25zID0gW107XG4gICAgZWFjaChvYmosIGZ1bmN0aW9uICh2YWx1ZSwga2V5KSB7XG4gICAgICBkaW1lbnNpb25zLnB1c2goa2V5KTtcbiAgICB9KTtcbiAgICByZXR1cm4gZGltZW5zaW9ucztcbiAgfVxufSAvLyA/Pz8gVE9ETyBtZXJnZSB0byBjb21wbGV0ZWRpbWVuc2lvbnMsIHdoZXJlIGFsc28gaGFzXG4vLyBkZWZhdWx0IGVuY29kZSBtYWtpbmcgbG9naWMuIEFuZCB0aGUgZGVmYXVsdCBydWxlXG4vLyBzaG91bGQgZGVwZW5kcyBvbiBzZXJpZXM/IGNvbnNpZGVyICdtYXAnLlxuXG5cbmZ1bmN0aW9uIG1ha2VEZWZhdWx0RW5jb2RlKHNlcmllc01vZGVsLCBkYXRhc2V0TW9kZWwsIGRhdGEsIHNvdXJjZUZvcm1hdCwgc2VyaWVzTGF5b3V0QnksIGNvbXBsZXRlUmVzdWx0KSB7XG4gIHZhciBjb29yZFN5c0RlZmluZSA9IGdldENvb3JkU3lzRGVmaW5lQnlTZXJpZXMoc2VyaWVzTW9kZWwpO1xuICB2YXIgZW5jb2RlID0ge307IC8vIHZhciBlbmNvZGVUb29sdGlwID0gW107XG4gIC8vIHZhciBlbmNvZGVMYWJlbCA9IFtdO1xuXG4gIHZhciBlbmNvZGVJdGVtTmFtZSA9IFtdO1xuICB2YXIgZW5jb2RlU2VyaWVzTmFtZSA9IFtdO1xuICB2YXIgc2VyaWVzVHlwZSA9IHNlcmllc01vZGVsLnN1YlR5cGU7IC8vID8/PyBUT0RPIHJlZmFjdG9yOiBwcm92aWRlIGJ5IHNlcmllcyBpdHNlbGYuXG4gIC8vIENvbnNpZGVyIHRoZSBjYXNlOiAnbWFwJyBzZXJpZXMgaXMgYmFzZWQgb24gZ2VvIGNvb3JkU3lzLFxuICAvLyAnZ3JhcGgnLCAnaGVhdG1hcCcgY2FuIGJlIGJhc2VkIG9uIGNhcnRlc2lhbi4gQnV0IGNhbiBub3RcbiAgLy8gZ2l2ZSBkZWZhdWx0IHJ1bGUgc2ltcGx5IGhlcmUuXG5cbiAgdmFyIG5TZXJpZXNNYXAgPSBjcmVhdGVIYXNoTWFwKFsncGllJywgJ21hcCcsICdmdW5uZWwnXSk7XG4gIHZhciBjU2VyaWVzTWFwID0gY3JlYXRlSGFzaE1hcChbJ2xpbmUnLCAnYmFyJywgJ3BpY3RvcmlhbEJhcicsICdzY2F0dGVyJywgJ2VmZmVjdFNjYXR0ZXInLCAnY2FuZGxlc3RpY2snLCAnYm94cGxvdCddKTsgLy8gVXN1YWxseSBpbiB0aGlzIGNhc2Ugc2VyaWVzIHdpbGwgdXNlIHRoZSBmaXJzdCBkYXRhXG4gIC8vIGRpbWVuc2lvbiBhcyB0aGUgXCJ2YWx1ZVwiIGRpbWVuc2lvbiwgb3Igb3RoZXIgZGVmYXVsdFxuICAvLyBwcm9jZXNzZXMgcmVzcGVjdGl2ZWx5LlxuXG4gIGlmIChjb29yZFN5c0RlZmluZSAmJiBjU2VyaWVzTWFwLmdldChzZXJpZXNUeXBlKSAhPSBudWxsKSB7XG4gICAgdmFyIGVjTW9kZWwgPSBzZXJpZXNNb2RlbC5lY01vZGVsO1xuICAgIHZhciBkYXRhc2V0TWFwID0gaW5uZXIoZWNNb2RlbCkuZGF0YXNldE1hcDtcbiAgICB2YXIga2V5ID0gZGF0YXNldE1vZGVsLnVpZCArICdfJyArIHNlcmllc0xheW91dEJ5O1xuICAgIHZhciBkYXRhc2V0UmVjb3JkID0gZGF0YXNldE1hcC5nZXQoa2V5KSB8fCBkYXRhc2V0TWFwLnNldChrZXksIHtcbiAgICAgIGNhdGVnb3J5V2F5RGltOiAxLFxuICAgICAgdmFsdWVXYXlEaW06IDBcbiAgICB9KTsgLy8gVE9ET1xuICAgIC8vIEF1dG8gZGV0ZWN0IGZpcnN0IHRpbWUgYXhpcyBhbmQgZG8gYXJyYW5nZW1lbnQuXG5cbiAgICBlYWNoKGNvb3JkU3lzRGVmaW5lLmNvb3JkU3lzRGltcywgZnVuY3Rpb24gKGNvb3JkRGltKSB7XG4gICAgICAvLyBJbiB2YWx1ZSB3YXkuXG4gICAgICBpZiAoY29vcmRTeXNEZWZpbmUuZmlyc3RDYXRlZ29yeURpbUluZGV4ID09IG51bGwpIHtcbiAgICAgICAgdmFyIGRhdGFEaW0gPSBkYXRhc2V0UmVjb3JkLnZhbHVlV2F5RGltKys7XG4gICAgICAgIGVuY29kZVtjb29yZERpbV0gPSBkYXRhRGltOyAvLyA/Pz8gVE9ETyBnaXZlIGEgYmV0dGVyIGRlZmF1bHQgc2VyaWVzIG5hbWUgcnVsZT9cbiAgICAgICAgLy8gZXNwZWNpYWxseSB3aGVuIGVuY29kZSB4IHkgc3BlY2lmaWVkLlxuICAgICAgICAvLyBjb25zaWRlcjogd2hlbiBtdXRpcGxlIHNlcmllcyBzaGFyZSBvbmUgZGltZW5zaW9uXG4gICAgICAgIC8vIGNhdGVnb3J5IGF4aXMsIHNlcmllcyBuYW1lIHNob3VsZCBiZXR0ZXIgdXNlXG4gICAgICAgIC8vIHRoZSBvdGhlciBkaW1zaW9uIG5hbWUuIE9uIHRoZSBvdGhlciBoYW5kLCB1c2VcbiAgICAgICAgLy8gYm90aCBkaW1lbnNpb25zIG5hbWUuXG5cbiAgICAgICAgZW5jb2RlU2VyaWVzTmFtZS5wdXNoKGRhdGFEaW0pOyAvLyBlbmNvZGVUb29sdGlwLnB1c2goZGF0YURpbSk7XG4gICAgICAgIC8vIGVuY29kZUxhYmVsLnB1c2goZGF0YURpbSk7XG4gICAgICB9IC8vIEluIGNhdGVnb3J5IHdheSwgY2F0ZWdvcnkgYXhpcy5cbiAgICAgIGVsc2UgaWYgKGNvb3JkU3lzRGVmaW5lLmNhdGVnb3J5QXhpc01hcC5nZXQoY29vcmREaW0pKSB7XG4gICAgICAgICAgZW5jb2RlW2Nvb3JkRGltXSA9IDA7XG4gICAgICAgICAgZW5jb2RlSXRlbU5hbWUucHVzaCgwKTtcbiAgICAgICAgfSAvLyBJbiBjYXRlZ29yeSB3YXksIG5vbi1jYXRlZ29yeSBheGlzLlxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHZhciBkYXRhRGltID0gZGF0YXNldFJlY29yZC5jYXRlZ29yeVdheURpbSsrO1xuICAgICAgICAgICAgZW5jb2RlW2Nvb3JkRGltXSA9IGRhdGFEaW07IC8vIGVuY29kZVRvb2x0aXAucHVzaChkYXRhRGltKTtcbiAgICAgICAgICAgIC8vIGVuY29kZUxhYmVsLnB1c2goZGF0YURpbSk7XG5cbiAgICAgICAgICAgIGVuY29kZVNlcmllc05hbWUucHVzaChkYXRhRGltKTtcbiAgICAgICAgICB9XG4gICAgfSk7XG4gIH0gLy8gRG8gbm90IG1ha2UgYSBjb21wbGV4IHJ1bGUhIEhhcmQgdG8gY29kZSBtYWludGFpbiBhbmQgbm90IG5lY2Vzc2FyeS5cbiAgLy8gPz8/IFRPRE8gcmVmYWN0b3I6IHByb3ZpZGUgYnkgc2VyaWVzIGl0c2VsZi5cbiAgLy8gW3tuYW1lOiAuLi4sIHZhbHVlOiAuLi59LCAuLi5dIGxpa2U6XG4gIGVsc2UgaWYgKG5TZXJpZXNNYXAuZ2V0KHNlcmllc1R5cGUpICE9IG51bGwpIHtcbiAgICAgIC8vIEZpbmQgdGhlIGZpcnN0IG5vdCBvcmRpbmFsLiAoNSBpcyBhbiBleHBlcmllbmNlIHZhbHVlKVxuICAgICAgdmFyIGZpcnN0Tm90T3JkaW5hbDtcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCA1ICYmIGZpcnN0Tm90T3JkaW5hbCA9PSBudWxsOyBpKyspIHtcbiAgICAgICAgaWYgKCFkb0d1ZXNzT3JkaW5hbChkYXRhLCBzb3VyY2VGb3JtYXQsIHNlcmllc0xheW91dEJ5LCBjb21wbGV0ZVJlc3VsdC5kaW1lbnNpb25zRGVmaW5lLCBjb21wbGV0ZVJlc3VsdC5zdGFydEluZGV4LCBpKSkge1xuICAgICAgICAgIGZpcnN0Tm90T3JkaW5hbCA9IGk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGZpcnN0Tm90T3JkaW5hbCAhPSBudWxsKSB7XG4gICAgICAgIGVuY29kZS52YWx1ZSA9IGZpcnN0Tm90T3JkaW5hbDtcbiAgICAgICAgdmFyIG5hbWVEaW1JbmRleCA9IGNvbXBsZXRlUmVzdWx0LnBvdGVudGlhbE5hbWVEaW1JbmRleCB8fCBNYXRoLm1heChmaXJzdE5vdE9yZGluYWwgLSAxLCAwKTsgLy8gQnkgZGVmYXVsdCwgbGFiZWwgdXNlIGl0ZW1OYW1lIGluIGNoYXJ0cy5cbiAgICAgICAgLy8gU28gd2UgZG9udCBzZXQgZW5jb2RlTGFiZWwgaGVyZS5cblxuICAgICAgICBlbmNvZGVTZXJpZXNOYW1lLnB1c2gobmFtZURpbUluZGV4KTtcbiAgICAgICAgZW5jb2RlSXRlbU5hbWUucHVzaChuYW1lRGltSW5kZXgpOyAvLyBlbmNvZGVUb29sdGlwLnB1c2goZmlyc3ROb3RPcmRpbmFsKTtcbiAgICAgIH1cbiAgICB9IC8vIGVuY29kZVRvb2x0aXAubGVuZ3RoICYmIChlbmNvZGUudG9vbHRpcCA9IGVuY29kZVRvb2x0aXApO1xuICAvLyBlbmNvZGVMYWJlbC5sZW5ndGggJiYgKGVuY29kZS5sYWJlbCA9IGVuY29kZUxhYmVsKTtcblxuXG4gIGVuY29kZUl0ZW1OYW1lLmxlbmd0aCAmJiAoZW5jb2RlLml0ZW1OYW1lID0gZW5jb2RlSXRlbU5hbWUpO1xuICBlbmNvZGVTZXJpZXNOYW1lLmxlbmd0aCAmJiAoZW5jb2RlLnNlcmllc05hbWUgPSBlbmNvZGVTZXJpZXNOYW1lKTtcbiAgcmV0dXJuIGVuY29kZTtcbn1cbi8qKlxuICogSWYgcmV0dXJuIG51bGwvdW5kZWZpbmVkLCBpbmRpY2F0ZSB0aGF0IHNob3VsZCBub3QgdXNlIGRhdGFzZXRNb2RlbC5cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldERhdGFzZXRNb2RlbChzZXJpZXNNb2RlbCkge1xuICB2YXIgb3B0aW9uID0gc2VyaWVzTW9kZWwub3B0aW9uOyAvLyBDYXV0aW9uOiBjb25zaWRlciB0aGUgc2NlbmFyaW86XG4gIC8vIEEgZGF0YXNldCBpcyBkZWNsYXJlZCBhbmQgYSBzZXJpZXMgaXMgbm90IGV4cGVjdGVkIHRvIHVzZSB0aGUgZGF0YXNldCxcbiAgLy8gYW5kIGF0IHRoZSBiZWdpbm5pbmcgYHNldE9wdGlvbih7c2VyaWVzOiB7IG5vRGF0YSB9KWAgKGp1c3QgcHJlcGFyZSBvdGhlclxuICAvLyBvcHRpb24gYnV0IG5vIGRhdGEpLCB0aGVuIGBzZXRPcHRpb24oe3Nlcmllczoge2RhdGE6IFsuLi5dfSk7IEluIHRoaXMgY2FzZSxcbiAgLy8gdGhlIHVzZXIgc2hvdWxkIHNldCBhbiBlbXB0eSBhcnJheSB0byBhdm9pZCB0aGF0IGRhdGFzZXQgaXMgdXNlZCBieSBkZWZhdWx0LlxuXG4gIHZhciB0aGlzRGF0YSA9IG9wdGlvbi5kYXRhO1xuXG4gIGlmICghdGhpc0RhdGEpIHtcbiAgICByZXR1cm4gc2VyaWVzTW9kZWwuZWNNb2RlbC5nZXRDb21wb25lbnQoJ2RhdGFzZXQnLCBvcHRpb24uZGF0YXNldEluZGV4IHx8IDApO1xuICB9XG59XG4vKipcbiAqIFRoZSBydWxlIHNob3VsZCBub3QgYmUgY29tcGxleCwgb3RoZXJ3aXNlIHVzZXIgbWlnaHQgbm90XG4gKiBiZSBhYmxlIHRvIGtub3duIHdoZXJlIHRoZSBkYXRhIGlzIHdyb25nLlxuICogVGhlIGNvZGUgaXMgdWdseSwgYnV0IGhvdyB0byBtYWtlIGl0IG5lYXQ/XG4gKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJzL2RhdGEvU291cmNlfSBzb3VyY2VcbiAqIEBwYXJhbSB7bnVtYmVyfSBkaW1JbmRleFxuICogQHJldHVybiB7Ym9vbGVhbn0gV2hldGhlciBvcmRpbmFsLlxuICovXG5cblxuZnVuY3Rpb24gZ3Vlc3NPcmRpbmFsKHNvdXJjZSwgZGltSW5kZXgpIHtcbiAgcmV0dXJuIGRvR3Vlc3NPcmRpbmFsKHNvdXJjZS5kYXRhLCBzb3VyY2Uuc291cmNlRm9ybWF0LCBzb3VyY2Uuc2VyaWVzTGF5b3V0QnksIHNvdXJjZS5kaW1lbnNpb25zRGVmaW5lLCBzb3VyY2Uuc3RhcnRJbmRleCwgZGltSW5kZXgpO1xufSAvLyBkaW1JbmRleCBtYXkgYmUgb3ZlcmZsb3cgc291cmNlIGRhdGEuXG5cblxuZnVuY3Rpb24gZG9HdWVzc09yZGluYWwoZGF0YSwgc291cmNlRm9ybWF0LCBzZXJpZXNMYXlvdXRCeSwgZGltZW5zaW9uc0RlZmluZSwgc3RhcnRJbmRleCwgZGltSW5kZXgpIHtcbiAgdmFyIHJlc3VsdDsgLy8gRXhwZXJpZW5jZSB2YWx1ZS5cblxuICB2YXIgbWF4TG9vcCA9IDU7XG5cbiAgaWYgKGlzVHlwZWRBcnJheShkYXRhKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfSAvLyBXaGVuIHNvdXJjZVR5cGUgaXMgJ29iamVjdFJvd3MnIG9yICdrZXllZENvbHVtbnMnLCBkaW1lbnNpb25zRGVmaW5lXG4gIC8vIGFsd2F5cyBleGlzdHMgaW4gc291cmNlLlxuXG5cbiAgdmFyIGRpbU5hbWU7XG5cbiAgaWYgKGRpbWVuc2lvbnNEZWZpbmUpIHtcbiAgICBkaW1OYW1lID0gZGltZW5zaW9uc0RlZmluZVtkaW1JbmRleF07XG4gICAgZGltTmFtZSA9IGlzT2JqZWN0KGRpbU5hbWUpID8gZGltTmFtZS5uYW1lIDogZGltTmFtZTtcbiAgfVxuXG4gIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfQVJSQVlfUk9XUykge1xuICAgIGlmIChzZXJpZXNMYXlvdXRCeSA9PT0gU0VSSUVTX0xBWU9VVF9CWV9ST1cpIHtcbiAgICAgIHZhciBzYW1wbGUgPSBkYXRhW2RpbUluZGV4XTtcblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAoc2FtcGxlIHx8IFtdKS5sZW5ndGggJiYgaSA8IG1heExvb3A7IGkrKykge1xuICAgICAgICBpZiAoKHJlc3VsdCA9IGRldGVjdFZhbHVlKHNhbXBsZVtzdGFydEluZGV4ICsgaV0pKSAhPSBudWxsKSB7XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoICYmIGkgPCBtYXhMb29wOyBpKyspIHtcbiAgICAgICAgdmFyIHJvdyA9IGRhdGFbc3RhcnRJbmRleCArIGldO1xuXG4gICAgICAgIGlmIChyb3cgJiYgKHJlc3VsdCA9IGRldGVjdFZhbHVlKHJvd1tkaW1JbmRleF0pKSAhPSBudWxsKSB7XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmIChzb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfT0JKRUNUX1JPV1MpIHtcbiAgICBpZiAoIWRpbU5hbWUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoICYmIGkgPCBtYXhMb29wOyBpKyspIHtcbiAgICAgIHZhciBpdGVtID0gZGF0YVtpXTtcblxuICAgICAgaWYgKGl0ZW0gJiYgKHJlc3VsdCA9IGRldGVjdFZhbHVlKGl0ZW1bZGltTmFtZV0pKSAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICB9XG4gICAgfVxuICB9IGVsc2UgaWYgKHNvdXJjZUZvcm1hdCA9PT0gU09VUkNFX0ZPUk1BVF9LRVlFRF9DT0xVTU5TKSB7XG4gICAgaWYgKCFkaW1OYW1lKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHNhbXBsZSA9IGRhdGFbZGltTmFtZV07XG5cbiAgICBpZiAoIXNhbXBsZSB8fCBpc1R5cGVkQXJyYXkoc2FtcGxlKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2FtcGxlLmxlbmd0aCAmJiBpIDwgbWF4TG9vcDsgaSsrKSB7XG4gICAgICBpZiAoKHJlc3VsdCA9IGRldGVjdFZhbHVlKHNhbXBsZVtpXSkpICE9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSBpZiAoc291cmNlRm9ybWF0ID09PSBTT1VSQ0VfRk9STUFUX09SSUdJTkFMKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aCAmJiBpIDwgbWF4TG9vcDsgaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IGRhdGFbaV07XG4gICAgICB2YXIgdmFsID0gZ2V0RGF0YUl0ZW1WYWx1ZShpdGVtKTtcblxuICAgICAgaWYgKCFpc0FycmF5KHZhbCkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBpZiAoKHJlc3VsdCA9IGRldGVjdFZhbHVlKHZhbFtkaW1JbmRleF0pKSAhPSBudWxsKSB7XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gZGV0ZWN0VmFsdWUodmFsKSB7XG4gICAgLy8gQ29uc2lkZXIgdXNhZ2UgY29udmVuaWVuY2UsICcxJywgJzInIHdpbGwgYmUgdHJlYXRlZCBhcyBcIm51bWJlclwiLlxuICAgIC8vIGBpc0Zpbml0KCcnKWAgZ2V0IGB0cnVlYC5cbiAgICBpZiAodmFsICE9IG51bGwgJiYgaXNGaW5pdGUodmFsKSAmJiB2YWwgIT09ICcnKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSBlbHNlIGlmIChpc1N0cmluZyh2YWwpICYmIHZhbCAhPT0gJy0nKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZmFsc2U7XG59XG5cbmV4cG9ydHMuZGV0ZWN0U291cmNlRm9ybWF0ID0gZGV0ZWN0U291cmNlRm9ybWF0O1xuZXhwb3J0cy5nZXRTb3VyY2UgPSBnZXRTb3VyY2U7XG5leHBvcnRzLnJlc2V0U291cmNlRGVmYXVsdGVyID0gcmVzZXRTb3VyY2VEZWZhdWx0ZXI7XG5leHBvcnRzLnByZXBhcmVTb3VyY2UgPSBwcmVwYXJlU291cmNlO1xuZXhwb3J0cy5ndWVzc09yZGluYWwgPSBndWVzc09yZGluYWw7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0Q0E7QUF3Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/helper/sourceHelper.js
