/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* A third-party license is embeded for some of the code in this file:
* The method "quantile" was copied from "d3.js".
* (See more details in the comment of the method below.)
* The use of the source code of this file is also subject to the terms
* and consitions of the license of "d3.js" (BSD-3Clause, see
* </licenses/LICENSE-d3>).
*/


var RADIAN_EPSILON = 1e-4;

function _trim(str) {
  return str.replace(/^\s+/, '').replace(/\s+$/, '');
}
/**
 * Linear mapping a value from domain to range
 * @memberOf module:echarts/util/number
 * @param  {(number|Array.<number>)} val
 * @param  {Array.<number>} domain Domain extent domain[0] can be bigger than domain[1]
 * @param  {Array.<number>} range  Range extent range[0] can be bigger than range[1]
 * @param  {boolean} clamp
 * @return {(number|Array.<number>}
 */


function linearMap(val, domain, range, clamp) {
  var subDomain = domain[1] - domain[0];
  var subRange = range[1] - range[0];

  if (subDomain === 0) {
    return subRange === 0 ? range[0] : (range[0] + range[1]) / 2;
  } // Avoid accuracy problem in edge, such as
  // 146.39 - 62.83 === 83.55999999999999.
  // See echarts/test/ut/spec/util/number.js#linearMap#accuracyError
  // It is a little verbose for efficiency considering this method
  // is a hotspot.


  if (clamp) {
    if (subDomain > 0) {
      if (val <= domain[0]) {
        return range[0];
      } else if (val >= domain[1]) {
        return range[1];
      }
    } else {
      if (val >= domain[0]) {
        return range[0];
      } else if (val <= domain[1]) {
        return range[1];
      }
    }
  } else {
    if (val === domain[0]) {
      return range[0];
    }

    if (val === domain[1]) {
      return range[1];
    }
  }

  return (val - domain[0]) / subDomain * subRange + range[0];
}
/**
 * Convert a percent string to absolute number.
 * Returns NaN if percent is not a valid string or number
 * @memberOf module:echarts/util/number
 * @param {string|number} percent
 * @param {number} all
 * @return {number}
 */


function parsePercent(percent, all) {
  switch (percent) {
    case 'center':
    case 'middle':
      percent = '50%';
      break;

    case 'left':
    case 'top':
      percent = '0%';
      break;

    case 'right':
    case 'bottom':
      percent = '100%';
      break;
  }

  if (typeof percent === 'string') {
    if (_trim(percent).match(/%$/)) {
      return parseFloat(percent) / 100 * all;
    }

    return parseFloat(percent);
  }

  return percent == null ? NaN : +percent;
}
/**
 * (1) Fix rounding error of float numbers.
 * (2) Support return string to avoid scientific notation like '3.5e-7'.
 *
 * @param {number} x
 * @param {number} [precision]
 * @param {boolean} [returnStr]
 * @return {number|string}
 */


function round(x, precision, returnStr) {
  if (precision == null) {
    precision = 10;
  } // Avoid range error


  precision = Math.min(Math.max(0, precision), 20);
  x = (+x).toFixed(precision);
  return returnStr ? x : +x;
}

function asc(arr) {
  arr.sort(function (a, b) {
    return a - b;
  });
  return arr;
}
/**
 * Get precision
 * @param {number} val
 */


function getPrecision(val) {
  val = +val;

  if (isNaN(val)) {
    return 0;
  } // It is much faster than methods converting number to string as follows
  //      var tmp = val.toString();
  //      return tmp.length - 1 - tmp.indexOf('.');
  // especially when precision is low


  var e = 1;
  var count = 0;

  while (Math.round(val * e) / e !== val) {
    e *= 10;
    count++;
  }

  return count;
}
/**
 * @param {string|number} val
 * @return {number}
 */


function getPrecisionSafe(val) {
  var str = val.toString(); // Consider scientific notation: '3.4e-12' '3.4e+12'

  var eIndex = str.indexOf('e');

  if (eIndex > 0) {
    var precision = +str.slice(eIndex + 1);
    return precision < 0 ? -precision : 0;
  } else {
    var dotIndex = str.indexOf('.');
    return dotIndex < 0 ? 0 : str.length - 1 - dotIndex;
  }
}
/**
 * Minimal dicernible data precisioin according to a single pixel.
 *
 * @param {Array.<number>} dataExtent
 * @param {Array.<number>} pixelExtent
 * @return {number} precision
 */


function getPixelPrecision(dataExtent, pixelExtent) {
  var log = Math.log;
  var LN10 = Math.LN10;
  var dataQuantity = Math.floor(log(dataExtent[1] - dataExtent[0]) / LN10);
  var sizeQuantity = Math.round(log(Math.abs(pixelExtent[1] - pixelExtent[0])) / LN10); // toFixed() digits argument must be between 0 and 20.

  var precision = Math.min(Math.max(-dataQuantity + sizeQuantity, 0), 20);
  return !isFinite(precision) ? 20 : precision;
}
/**
 * Get a data of given precision, assuring the sum of percentages
 * in valueList is 1.
 * The largest remainer method is used.
 * https://en.wikipedia.org/wiki/Largest_remainder_method
 *
 * @param {Array.<number>} valueList a list of all data
 * @param {number} idx index of the data to be processed in valueList
 * @param {number} precision integer number showing digits of precision
 * @return {number} percent ranging from 0 to 100
 */


function getPercentWithPrecision(valueList, idx, precision) {
  if (!valueList[idx]) {
    return 0;
  }

  var sum = zrUtil.reduce(valueList, function (acc, val) {
    return acc + (isNaN(val) ? 0 : val);
  }, 0);

  if (sum === 0) {
    return 0;
  }

  var digits = Math.pow(10, precision);
  var votesPerQuota = zrUtil.map(valueList, function (val) {
    return (isNaN(val) ? 0 : val) / sum * digits * 100;
  });
  var targetSeats = digits * 100;
  var seats = zrUtil.map(votesPerQuota, function (votes) {
    // Assign automatic seats.
    return Math.floor(votes);
  });
  var currentSum = zrUtil.reduce(seats, function (acc, val) {
    return acc + val;
  }, 0);
  var remainder = zrUtil.map(votesPerQuota, function (votes, idx) {
    return votes - seats[idx];
  }); // Has remainding votes.

  while (currentSum < targetSeats) {
    // Find next largest remainder.
    var max = Number.NEGATIVE_INFINITY;
    var maxId = null;

    for (var i = 0, len = remainder.length; i < len; ++i) {
      if (remainder[i] > max) {
        max = remainder[i];
        maxId = i;
      }
    } // Add a vote to max remainder.


    ++seats[maxId];
    remainder[maxId] = 0;
    ++currentSum;
  }

  return seats[idx] / digits;
} // Number.MAX_SAFE_INTEGER, ie do not support.


var MAX_SAFE_INTEGER = 9007199254740991;
/**
 * To 0 - 2 * PI, considering negative radian.
 * @param {number} radian
 * @return {number}
 */

function remRadian(radian) {
  var pi2 = Math.PI * 2;
  return (radian % pi2 + pi2) % pi2;
}
/**
 * @param {type} radian
 * @return {boolean}
 */


function isRadianAroundZero(val) {
  return val > -RADIAN_EPSILON && val < RADIAN_EPSILON;
}
/* eslint-disable */


var TIME_REG = /^(?:(\d{4})(?:[-\/](\d{1,2})(?:[-\/](\d{1,2})(?:[T ](\d{1,2})(?::(\d\d)(?::(\d\d)(?:[.,](\d+))?)?)?(Z|[\+\-]\d\d:?\d\d)?)?)?)?)?$/; // jshint ignore:line

/* eslint-enable */

/**
 * @param {string|Date|number} value These values can be accepted:
 *   + An instance of Date, represent a time in its own time zone.
 *   + Or string in a subset of ISO 8601, only including:
 *     + only year, month, date: '2012-03', '2012-03-01', '2012-03-01 05', '2012-03-01 05:06',
 *     + separated with T or space: '2012-03-01T12:22:33.123', '2012-03-01 12:22:33.123',
 *     + time zone: '2012-03-01T12:22:33Z', '2012-03-01T12:22:33+8000', '2012-03-01T12:22:33-05:00',
 *     all of which will be treated as local time if time zone is not specified
 *     (see <https://momentjs.com/>).
 *   + Or other string format, including (all of which will be treated as loacal time):
 *     '2012', '2012-3-1', '2012/3/1', '2012/03/01',
 *     '2009/6/12 2:00', '2009/6/12 2:05:08', '2009/6/12 2:05:08.123'
 *   + a timestamp, which represent a time in UTC.
 * @return {Date} date
 */

function parseDate(value) {
  if (value instanceof Date) {
    return value;
  } else if (typeof value === 'string') {
    // Different browsers parse date in different way, so we parse it manually.
    // Some other issues:
    // new Date('1970-01-01') is UTC,
    // new Date('1970/01/01') and new Date('1970-1-01') is local.
    // See issue #3623
    var match = TIME_REG.exec(value);

    if (!match) {
      // return Invalid Date.
      return new Date(NaN);
    } // Use local time when no timezone offset specifed.


    if (!match[8]) {
      // match[n] can only be string or undefined.
      // But take care of '12' + 1 => '121'.
      return new Date(+match[1], +(match[2] || 1) - 1, +match[3] || 1, +match[4] || 0, +(match[5] || 0), +match[6] || 0, +match[7] || 0);
    } // Timezoneoffset of Javascript Date has considered DST (Daylight Saving Time,
    // https://tc39.github.io/ecma262/#sec-daylight-saving-time-adjustment).
    // For example, system timezone is set as "Time Zone: America/Toronto",
    // then these code will get different result:
    // `new Date(1478411999999).getTimezoneOffset();  // get 240`
    // `new Date(1478412000000).getTimezoneOffset();  // get 300`
    // So we should not use `new Date`, but use `Date.UTC`.
    else {
        var hour = +match[4] || 0;

        if (match[8].toUpperCase() !== 'Z') {
          hour -= match[8].slice(0, 3);
        }

        return new Date(Date.UTC(+match[1], +(match[2] || 1) - 1, +match[3] || 1, hour, +(match[5] || 0), +match[6] || 0, +match[7] || 0));
      }
  } else if (value == null) {
    return new Date(NaN);
  }

  return new Date(Math.round(value));
}
/**
 * Quantity of a number. e.g. 0.1, 1, 10, 100
 *
 * @param  {number} val
 * @return {number}
 */


function quantity(val) {
  return Math.pow(10, quantityExponent(val));
}

function quantityExponent(val) {
  return Math.floor(Math.log(val) / Math.LN10);
}
/**
 * find a “nice” number approximately equal to x. Round the number if round = true,
 * take ceiling if round = false. The primary observation is that the “nicest”
 * numbers in decimal are 1, 2, and 5, and all power-of-ten multiples of these numbers.
 *
 * See "Nice Numbers for Graph Labels" of Graphic Gems.
 *
 * @param  {number} val Non-negative value.
 * @param  {boolean} round
 * @return {number}
 */


function nice(val, round) {
  var exponent = quantityExponent(val);
  var exp10 = Math.pow(10, exponent);
  var f = val / exp10; // 1 <= f < 10

  var nf;

  if (round) {
    if (f < 1.5) {
      nf = 1;
    } else if (f < 2.5) {
      nf = 2;
    } else if (f < 4) {
      nf = 3;
    } else if (f < 7) {
      nf = 5;
    } else {
      nf = 10;
    }
  } else {
    if (f < 1) {
      nf = 1;
    } else if (f < 2) {
      nf = 2;
    } else if (f < 3) {
      nf = 3;
    } else if (f < 5) {
      nf = 5;
    } else {
      nf = 10;
    }
  }

  val = nf * exp10; // Fix 3 * 0.1 === 0.30000000000000004 issue (see IEEE 754).
  // 20 is the uppper bound of toFixed.

  return exponent >= -20 ? +val.toFixed(exponent < 0 ? -exponent : 0) : val;
}
/**
 * This code was copied from "d3.js"
 * <https://github.com/d3/d3/blob/9cc9a875e636a1dcf36cc1e07bdf77e1ad6e2c74/src/arrays/quantile.js>.
 * See the license statement at the head of this file.
 * @param {Array.<number>} ascArr
 */


function quantile(ascArr, p) {
  var H = (ascArr.length - 1) * p + 1;
  var h = Math.floor(H);
  var v = +ascArr[h - 1];
  var e = H - h;
  return e ? v + e * (ascArr[h] - v) : v;
}
/**
 * Order intervals asc, and split them when overlap.
 * expect(numberUtil.reformIntervals([
 *     {interval: [18, 62], close: [1, 1]},
 *     {interval: [-Infinity, -70], close: [0, 0]},
 *     {interval: [-70, -26], close: [1, 1]},
 *     {interval: [-26, 18], close: [1, 1]},
 *     {interval: [62, 150], close: [1, 1]},
 *     {interval: [106, 150], close: [1, 1]},
 *     {interval: [150, Infinity], close: [0, 0]}
 * ])).toEqual([
 *     {interval: [-Infinity, -70], close: [0, 0]},
 *     {interval: [-70, -26], close: [1, 1]},
 *     {interval: [-26, 18], close: [0, 1]},
 *     {interval: [18, 62], close: [0, 1]},
 *     {interval: [62, 150], close: [0, 1]},
 *     {interval: [150, Infinity], close: [0, 0]}
 * ]);
 * @param {Array.<Object>} list, where `close` mean open or close
 *        of the interval, and Infinity can be used.
 * @return {Array.<Object>} The origin list, which has been reformed.
 */


function reformIntervals(list) {
  list.sort(function (a, b) {
    return littleThan(a, b, 0) ? -1 : 1;
  });
  var curr = -Infinity;
  var currClose = 1;

  for (var i = 0; i < list.length;) {
    var interval = list[i].interval;
    var close = list[i].close;

    for (var lg = 0; lg < 2; lg++) {
      if (interval[lg] <= curr) {
        interval[lg] = curr;
        close[lg] = !lg ? 1 - currClose : 1;
      }

      curr = interval[lg];
      currClose = close[lg];
    }

    if (interval[0] === interval[1] && close[0] * close[1] !== 1) {
      list.splice(i, 1);
    } else {
      i++;
    }
  }

  return list;

  function littleThan(a, b, lg) {
    return a.interval[lg] < b.interval[lg] || a.interval[lg] === b.interval[lg] && (a.close[lg] - b.close[lg] === (!lg ? 1 : -1) || !lg && littleThan(a, b, 1));
  }
}
/**
 * parseFloat NaNs numeric-cast false positives (null|true|false|"")
 * ...but misinterprets leading-number strings, particularly hex literals ("0x...")
 * subtraction forces infinities to NaN
 *
 * @param {*} v
 * @return {boolean}
 */


function isNumeric(v) {
  return v - parseFloat(v) >= 0;
}

exports.linearMap = linearMap;
exports.parsePercent = parsePercent;
exports.round = round;
exports.asc = asc;
exports.getPrecision = getPrecision;
exports.getPrecisionSafe = getPrecisionSafe;
exports.getPixelPrecision = getPixelPrecision;
exports.getPercentWithPrecision = getPercentWithPrecision;
exports.MAX_SAFE_INTEGER = MAX_SAFE_INTEGER;
exports.remRadian = remRadian;
exports.isRadianAroundZero = isRadianAroundZero;
exports.parseDate = parseDate;
exports.quantity = quantity;
exports.nice = nice;
exports.quantile = quantile;
exports.reformIntervals = reformIntervals;
exports.isNumeric = isNumeric;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9udW1iZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi91dGlsL251bWJlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLypcbiogQSB0aGlyZC1wYXJ0eSBsaWNlbnNlIGlzIGVtYmVkZWQgZm9yIHNvbWUgb2YgdGhlIGNvZGUgaW4gdGhpcyBmaWxlOlxuKiBUaGUgbWV0aG9kIFwicXVhbnRpbGVcIiB3YXMgY29waWVkIGZyb20gXCJkMy5qc1wiLlxuKiAoU2VlIG1vcmUgZGV0YWlscyBpbiB0aGUgY29tbWVudCBvZiB0aGUgbWV0aG9kIGJlbG93LilcbiogVGhlIHVzZSBvZiB0aGUgc291cmNlIGNvZGUgb2YgdGhpcyBmaWxlIGlzIGFsc28gc3ViamVjdCB0byB0aGUgdGVybXNcbiogYW5kIGNvbnNpdGlvbnMgb2YgdGhlIGxpY2Vuc2Ugb2YgXCJkMy5qc1wiIChCU0QtM0NsYXVzZSwgc2VlXG4qIDwvbGljZW5zZXMvTElDRU5TRS1kMz4pLlxuKi9cbnZhciBSQURJQU5fRVBTSUxPTiA9IDFlLTQ7XG5cbmZ1bmN0aW9uIF90cmltKHN0cikge1xuICByZXR1cm4gc3RyLnJlcGxhY2UoL15cXHMrLywgJycpLnJlcGxhY2UoL1xccyskLywgJycpO1xufVxuLyoqXG4gKiBMaW5lYXIgbWFwcGluZyBhIHZhbHVlIGZyb20gZG9tYWluIHRvIHJhbmdlXG4gKiBAbWVtYmVyT2YgbW9kdWxlOmVjaGFydHMvdXRpbC9udW1iZXJcbiAqIEBwYXJhbSAgeyhudW1iZXJ8QXJyYXkuPG51bWJlcj4pfSB2YWxcbiAqIEBwYXJhbSAge0FycmF5LjxudW1iZXI+fSBkb21haW4gRG9tYWluIGV4dGVudCBkb21haW5bMF0gY2FuIGJlIGJpZ2dlciB0aGFuIGRvbWFpblsxXVxuICogQHBhcmFtICB7QXJyYXkuPG51bWJlcj59IHJhbmdlICBSYW5nZSBleHRlbnQgcmFuZ2VbMF0gY2FuIGJlIGJpZ2dlciB0aGFuIHJhbmdlWzFdXG4gKiBAcGFyYW0gIHtib29sZWFufSBjbGFtcFxuICogQHJldHVybiB7KG51bWJlcnxBcnJheS48bnVtYmVyPn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGxpbmVhck1hcCh2YWwsIGRvbWFpbiwgcmFuZ2UsIGNsYW1wKSB7XG4gIHZhciBzdWJEb21haW4gPSBkb21haW5bMV0gLSBkb21haW5bMF07XG4gIHZhciBzdWJSYW5nZSA9IHJhbmdlWzFdIC0gcmFuZ2VbMF07XG5cbiAgaWYgKHN1YkRvbWFpbiA9PT0gMCkge1xuICAgIHJldHVybiBzdWJSYW5nZSA9PT0gMCA/IHJhbmdlWzBdIDogKHJhbmdlWzBdICsgcmFuZ2VbMV0pIC8gMjtcbiAgfSAvLyBBdm9pZCBhY2N1cmFjeSBwcm9ibGVtIGluIGVkZ2UsIHN1Y2ggYXNcbiAgLy8gMTQ2LjM5IC0gNjIuODMgPT09IDgzLjU1OTk5OTk5OTk5OTk5LlxuICAvLyBTZWUgZWNoYXJ0cy90ZXN0L3V0L3NwZWMvdXRpbC9udW1iZXIuanMjbGluZWFyTWFwI2FjY3VyYWN5RXJyb3JcbiAgLy8gSXQgaXMgYSBsaXR0bGUgdmVyYm9zZSBmb3IgZWZmaWNpZW5jeSBjb25zaWRlcmluZyB0aGlzIG1ldGhvZFxuICAvLyBpcyBhIGhvdHNwb3QuXG5cblxuICBpZiAoY2xhbXApIHtcbiAgICBpZiAoc3ViRG9tYWluID4gMCkge1xuICAgICAgaWYgKHZhbCA8PSBkb21haW5bMF0pIHtcbiAgICAgICAgcmV0dXJuIHJhbmdlWzBdO1xuICAgICAgfSBlbHNlIGlmICh2YWwgPj0gZG9tYWluWzFdKSB7XG4gICAgICAgIHJldHVybiByYW5nZVsxXTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHZhbCA+PSBkb21haW5bMF0pIHtcbiAgICAgICAgcmV0dXJuIHJhbmdlWzBdO1xuICAgICAgfSBlbHNlIGlmICh2YWwgPD0gZG9tYWluWzFdKSB7XG4gICAgICAgIHJldHVybiByYW5nZVsxXTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgaWYgKHZhbCA9PT0gZG9tYWluWzBdKSB7XG4gICAgICByZXR1cm4gcmFuZ2VbMF07XG4gICAgfVxuXG4gICAgaWYgKHZhbCA9PT0gZG9tYWluWzFdKSB7XG4gICAgICByZXR1cm4gcmFuZ2VbMV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuICh2YWwgLSBkb21haW5bMF0pIC8gc3ViRG9tYWluICogc3ViUmFuZ2UgKyByYW5nZVswXTtcbn1cbi8qKlxuICogQ29udmVydCBhIHBlcmNlbnQgc3RyaW5nIHRvIGFic29sdXRlIG51bWJlci5cbiAqIFJldHVybnMgTmFOIGlmIHBlcmNlbnQgaXMgbm90IGEgdmFsaWQgc3RyaW5nIG9yIG51bWJlclxuICogQG1lbWJlck9mIG1vZHVsZTplY2hhcnRzL3V0aWwvbnVtYmVyXG4gKiBAcGFyYW0ge3N0cmluZ3xudW1iZXJ9IHBlcmNlbnRcbiAqIEBwYXJhbSB7bnVtYmVyfSBhbGxcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIHBhcnNlUGVyY2VudChwZXJjZW50LCBhbGwpIHtcbiAgc3dpdGNoIChwZXJjZW50KSB7XG4gICAgY2FzZSAnY2VudGVyJzpcbiAgICBjYXNlICdtaWRkbGUnOlxuICAgICAgcGVyY2VudCA9ICc1MCUnO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdsZWZ0JzpcbiAgICBjYXNlICd0b3AnOlxuICAgICAgcGVyY2VudCA9ICcwJSc7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ3JpZ2h0JzpcbiAgICBjYXNlICdib3R0b20nOlxuICAgICAgcGVyY2VudCA9ICcxMDAlJztcbiAgICAgIGJyZWFrO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBwZXJjZW50ID09PSAnc3RyaW5nJykge1xuICAgIGlmIChfdHJpbShwZXJjZW50KS5tYXRjaCgvJSQvKSkge1xuICAgICAgcmV0dXJuIHBhcnNlRmxvYXQocGVyY2VudCkgLyAxMDAgKiBhbGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhcnNlRmxvYXQocGVyY2VudCk7XG4gIH1cblxuICByZXR1cm4gcGVyY2VudCA9PSBudWxsID8gTmFOIDogK3BlcmNlbnQ7XG59XG4vKipcbiAqICgxKSBGaXggcm91bmRpbmcgZXJyb3Igb2YgZmxvYXQgbnVtYmVycy5cbiAqICgyKSBTdXBwb3J0IHJldHVybiBzdHJpbmcgdG8gYXZvaWQgc2NpZW50aWZpYyBub3RhdGlvbiBsaWtlICczLjVlLTcnLlxuICpcbiAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gKiBAcGFyYW0ge251bWJlcn0gW3ByZWNpc2lvbl1cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW3JldHVyblN0cl1cbiAqIEByZXR1cm4ge251bWJlcnxzdHJpbmd9XG4gKi9cblxuXG5mdW5jdGlvbiByb3VuZCh4LCBwcmVjaXNpb24sIHJldHVyblN0cikge1xuICBpZiAocHJlY2lzaW9uID09IG51bGwpIHtcbiAgICBwcmVjaXNpb24gPSAxMDtcbiAgfSAvLyBBdm9pZCByYW5nZSBlcnJvclxuXG5cbiAgcHJlY2lzaW9uID0gTWF0aC5taW4oTWF0aC5tYXgoMCwgcHJlY2lzaW9uKSwgMjApO1xuICB4ID0gKCt4KS50b0ZpeGVkKHByZWNpc2lvbik7XG4gIHJldHVybiByZXR1cm5TdHIgPyB4IDogK3g7XG59XG5cbmZ1bmN0aW9uIGFzYyhhcnIpIHtcbiAgYXJyLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gYSAtIGI7XG4gIH0pO1xuICByZXR1cm4gYXJyO1xufVxuLyoqXG4gKiBHZXQgcHJlY2lzaW9uXG4gKiBAcGFyYW0ge251bWJlcn0gdmFsXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRQcmVjaXNpb24odmFsKSB7XG4gIHZhbCA9ICt2YWw7XG5cbiAgaWYgKGlzTmFOKHZhbCkpIHtcbiAgICByZXR1cm4gMDtcbiAgfSAvLyBJdCBpcyBtdWNoIGZhc3RlciB0aGFuIG1ldGhvZHMgY29udmVydGluZyBudW1iZXIgdG8gc3RyaW5nIGFzIGZvbGxvd3NcbiAgLy8gICAgICB2YXIgdG1wID0gdmFsLnRvU3RyaW5nKCk7XG4gIC8vICAgICAgcmV0dXJuIHRtcC5sZW5ndGggLSAxIC0gdG1wLmluZGV4T2YoJy4nKTtcbiAgLy8gZXNwZWNpYWxseSB3aGVuIHByZWNpc2lvbiBpcyBsb3dcblxuXG4gIHZhciBlID0gMTtcbiAgdmFyIGNvdW50ID0gMDtcblxuICB3aGlsZSAoTWF0aC5yb3VuZCh2YWwgKiBlKSAvIGUgIT09IHZhbCkge1xuICAgIGUgKj0gMTA7XG4gICAgY291bnQrKztcbiAgfVxuXG4gIHJldHVybiBjb3VudDtcbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd8bnVtYmVyfSB2YWxcbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFByZWNpc2lvblNhZmUodmFsKSB7XG4gIHZhciBzdHIgPSB2YWwudG9TdHJpbmcoKTsgLy8gQ29uc2lkZXIgc2NpZW50aWZpYyBub3RhdGlvbjogJzMuNGUtMTInICczLjRlKzEyJ1xuXG4gIHZhciBlSW5kZXggPSBzdHIuaW5kZXhPZignZScpO1xuXG4gIGlmIChlSW5kZXggPiAwKSB7XG4gICAgdmFyIHByZWNpc2lvbiA9ICtzdHIuc2xpY2UoZUluZGV4ICsgMSk7XG4gICAgcmV0dXJuIHByZWNpc2lvbiA8IDAgPyAtcHJlY2lzaW9uIDogMDtcbiAgfSBlbHNlIHtcbiAgICB2YXIgZG90SW5kZXggPSBzdHIuaW5kZXhPZignLicpO1xuICAgIHJldHVybiBkb3RJbmRleCA8IDAgPyAwIDogc3RyLmxlbmd0aCAtIDEgLSBkb3RJbmRleDtcbiAgfVxufVxuLyoqXG4gKiBNaW5pbWFsIGRpY2VybmlibGUgZGF0YSBwcmVjaXNpb2luIGFjY29yZGluZyB0byBhIHNpbmdsZSBwaXhlbC5cbiAqXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBkYXRhRXh0ZW50XG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBwaXhlbEV4dGVudFxuICogQHJldHVybiB7bnVtYmVyfSBwcmVjaXNpb25cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFBpeGVsUHJlY2lzaW9uKGRhdGFFeHRlbnQsIHBpeGVsRXh0ZW50KSB7XG4gIHZhciBsb2cgPSBNYXRoLmxvZztcbiAgdmFyIExOMTAgPSBNYXRoLkxOMTA7XG4gIHZhciBkYXRhUXVhbnRpdHkgPSBNYXRoLmZsb29yKGxvZyhkYXRhRXh0ZW50WzFdIC0gZGF0YUV4dGVudFswXSkgLyBMTjEwKTtcbiAgdmFyIHNpemVRdWFudGl0eSA9IE1hdGgucm91bmQobG9nKE1hdGguYWJzKHBpeGVsRXh0ZW50WzFdIC0gcGl4ZWxFeHRlbnRbMF0pKSAvIExOMTApOyAvLyB0b0ZpeGVkKCkgZGlnaXRzIGFyZ3VtZW50IG11c3QgYmUgYmV0d2VlbiAwIGFuZCAyMC5cblxuICB2YXIgcHJlY2lzaW9uID0gTWF0aC5taW4oTWF0aC5tYXgoLWRhdGFRdWFudGl0eSArIHNpemVRdWFudGl0eSwgMCksIDIwKTtcbiAgcmV0dXJuICFpc0Zpbml0ZShwcmVjaXNpb24pID8gMjAgOiBwcmVjaXNpb247XG59XG4vKipcbiAqIEdldCBhIGRhdGEgb2YgZ2l2ZW4gcHJlY2lzaW9uLCBhc3N1cmluZyB0aGUgc3VtIG9mIHBlcmNlbnRhZ2VzXG4gKiBpbiB2YWx1ZUxpc3QgaXMgMS5cbiAqIFRoZSBsYXJnZXN0IHJlbWFpbmVyIG1ldGhvZCBpcyB1c2VkLlxuICogaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTGFyZ2VzdF9yZW1haW5kZXJfbWV0aG9kXG4gKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gdmFsdWVMaXN0IGEgbGlzdCBvZiBhbGwgZGF0YVxuICogQHBhcmFtIHtudW1iZXJ9IGlkeCBpbmRleCBvZiB0aGUgZGF0YSB0byBiZSBwcm9jZXNzZWQgaW4gdmFsdWVMaXN0XG4gKiBAcGFyYW0ge251bWJlcn0gcHJlY2lzaW9uIGludGVnZXIgbnVtYmVyIHNob3dpbmcgZGlnaXRzIG9mIHByZWNpc2lvblxuICogQHJldHVybiB7bnVtYmVyfSBwZXJjZW50IHJhbmdpbmcgZnJvbSAwIHRvIDEwMFxuICovXG5cblxuZnVuY3Rpb24gZ2V0UGVyY2VudFdpdGhQcmVjaXNpb24odmFsdWVMaXN0LCBpZHgsIHByZWNpc2lvbikge1xuICBpZiAoIXZhbHVlTGlzdFtpZHhdKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB2YXIgc3VtID0genJVdGlsLnJlZHVjZSh2YWx1ZUxpc3QsIGZ1bmN0aW9uIChhY2MsIHZhbCkge1xuICAgIHJldHVybiBhY2MgKyAoaXNOYU4odmFsKSA/IDAgOiB2YWwpO1xuICB9LCAwKTtcblxuICBpZiAoc3VtID09PSAwKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB2YXIgZGlnaXRzID0gTWF0aC5wb3coMTAsIHByZWNpc2lvbik7XG4gIHZhciB2b3Rlc1BlclF1b3RhID0genJVdGlsLm1hcCh2YWx1ZUxpc3QsIGZ1bmN0aW9uICh2YWwpIHtcbiAgICByZXR1cm4gKGlzTmFOKHZhbCkgPyAwIDogdmFsKSAvIHN1bSAqIGRpZ2l0cyAqIDEwMDtcbiAgfSk7XG4gIHZhciB0YXJnZXRTZWF0cyA9IGRpZ2l0cyAqIDEwMDtcbiAgdmFyIHNlYXRzID0genJVdGlsLm1hcCh2b3Rlc1BlclF1b3RhLCBmdW5jdGlvbiAodm90ZXMpIHtcbiAgICAvLyBBc3NpZ24gYXV0b21hdGljIHNlYXRzLlxuICAgIHJldHVybiBNYXRoLmZsb29yKHZvdGVzKTtcbiAgfSk7XG4gIHZhciBjdXJyZW50U3VtID0genJVdGlsLnJlZHVjZShzZWF0cywgZnVuY3Rpb24gKGFjYywgdmFsKSB7XG4gICAgcmV0dXJuIGFjYyArIHZhbDtcbiAgfSwgMCk7XG4gIHZhciByZW1haW5kZXIgPSB6clV0aWwubWFwKHZvdGVzUGVyUXVvdGEsIGZ1bmN0aW9uICh2b3RlcywgaWR4KSB7XG4gICAgcmV0dXJuIHZvdGVzIC0gc2VhdHNbaWR4XTtcbiAgfSk7IC8vIEhhcyByZW1haW5kaW5nIHZvdGVzLlxuXG4gIHdoaWxlIChjdXJyZW50U3VtIDwgdGFyZ2V0U2VhdHMpIHtcbiAgICAvLyBGaW5kIG5leHQgbGFyZ2VzdCByZW1haW5kZXIuXG4gICAgdmFyIG1heCA9IE51bWJlci5ORUdBVElWRV9JTkZJTklUWTtcbiAgICB2YXIgbWF4SWQgPSBudWxsO1xuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHJlbWFpbmRlci5sZW5ndGg7IGkgPCBsZW47ICsraSkge1xuICAgICAgaWYgKHJlbWFpbmRlcltpXSA+IG1heCkge1xuICAgICAgICBtYXggPSByZW1haW5kZXJbaV07XG4gICAgICAgIG1heElkID0gaTtcbiAgICAgIH1cbiAgICB9IC8vIEFkZCBhIHZvdGUgdG8gbWF4IHJlbWFpbmRlci5cblxuXG4gICAgKytzZWF0c1ttYXhJZF07XG4gICAgcmVtYWluZGVyW21heElkXSA9IDA7XG4gICAgKytjdXJyZW50U3VtO1xuICB9XG5cbiAgcmV0dXJuIHNlYXRzW2lkeF0gLyBkaWdpdHM7XG59IC8vIE51bWJlci5NQVhfU0FGRV9JTlRFR0VSLCBpZSBkbyBub3Qgc3VwcG9ydC5cblxuXG52YXIgTUFYX1NBRkVfSU5URUdFUiA9IDkwMDcxOTkyNTQ3NDA5OTE7XG4vKipcbiAqIFRvIDAgLSAyICogUEksIGNvbnNpZGVyaW5nIG5lZ2F0aXZlIHJhZGlhbi5cbiAqIEBwYXJhbSB7bnVtYmVyfSByYWRpYW5cbiAqIEByZXR1cm4ge251bWJlcn1cbiAqL1xuXG5mdW5jdGlvbiByZW1SYWRpYW4ocmFkaWFuKSB7XG4gIHZhciBwaTIgPSBNYXRoLlBJICogMjtcbiAgcmV0dXJuIChyYWRpYW4gJSBwaTIgKyBwaTIpICUgcGkyO1xufVxuLyoqXG4gKiBAcGFyYW0ge3R5cGV9IHJhZGlhblxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGlzUmFkaWFuQXJvdW5kWmVybyh2YWwpIHtcbiAgcmV0dXJuIHZhbCA+IC1SQURJQU5fRVBTSUxPTiAmJiB2YWwgPCBSQURJQU5fRVBTSUxPTjtcbn1cbi8qIGVzbGludC1kaXNhYmxlICovXG5cblxudmFyIFRJTUVfUkVHID0gL14oPzooXFxkezR9KSg/OlstXFwvXShcXGR7MSwyfSkoPzpbLVxcL10oXFxkezEsMn0pKD86W1QgXShcXGR7MSwyfSkoPzo6KFxcZFxcZCkoPzo6KFxcZFxcZCkoPzpbLixdKFxcZCspKT8pPyk/KFp8W1xcK1xcLV1cXGRcXGQ6P1xcZFxcZCk/KT8pPyk/KT8kLzsgLy8ganNoaW50IGlnbm9yZTpsaW5lXG5cbi8qIGVzbGludC1lbmFibGUgKi9cblxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ3xEYXRlfG51bWJlcn0gdmFsdWUgVGhlc2UgdmFsdWVzIGNhbiBiZSBhY2NlcHRlZDpcbiAqICAgKyBBbiBpbnN0YW5jZSBvZiBEYXRlLCByZXByZXNlbnQgYSB0aW1lIGluIGl0cyBvd24gdGltZSB6b25lLlxuICogICArIE9yIHN0cmluZyBpbiBhIHN1YnNldCBvZiBJU08gODYwMSwgb25seSBpbmNsdWRpbmc6XG4gKiAgICAgKyBvbmx5IHllYXIsIG1vbnRoLCBkYXRlOiAnMjAxMi0wMycsICcyMDEyLTAzLTAxJywgJzIwMTItMDMtMDEgMDUnLCAnMjAxMi0wMy0wMSAwNTowNicsXG4gKiAgICAgKyBzZXBhcmF0ZWQgd2l0aCBUIG9yIHNwYWNlOiAnMjAxMi0wMy0wMVQxMjoyMjozMy4xMjMnLCAnMjAxMi0wMy0wMSAxMjoyMjozMy4xMjMnLFxuICogICAgICsgdGltZSB6b25lOiAnMjAxMi0wMy0wMVQxMjoyMjozM1onLCAnMjAxMi0wMy0wMVQxMjoyMjozMys4MDAwJywgJzIwMTItMDMtMDFUMTI6MjI6MzMtMDU6MDAnLFxuICogICAgIGFsbCBvZiB3aGljaCB3aWxsIGJlIHRyZWF0ZWQgYXMgbG9jYWwgdGltZSBpZiB0aW1lIHpvbmUgaXMgbm90IHNwZWNpZmllZFxuICogICAgIChzZWUgPGh0dHBzOi8vbW9tZW50anMuY29tLz4pLlxuICogICArIE9yIG90aGVyIHN0cmluZyBmb3JtYXQsIGluY2x1ZGluZyAoYWxsIG9mIHdoaWNoIHdpbGwgYmUgdHJlYXRlZCBhcyBsb2FjYWwgdGltZSk6XG4gKiAgICAgJzIwMTInLCAnMjAxMi0zLTEnLCAnMjAxMi8zLzEnLCAnMjAxMi8wMy8wMScsXG4gKiAgICAgJzIwMDkvNi8xMiAyOjAwJywgJzIwMDkvNi8xMiAyOjA1OjA4JywgJzIwMDkvNi8xMiAyOjA1OjA4LjEyMydcbiAqICAgKyBhIHRpbWVzdGFtcCwgd2hpY2ggcmVwcmVzZW50IGEgdGltZSBpbiBVVEMuXG4gKiBAcmV0dXJuIHtEYXRlfSBkYXRlXG4gKi9cblxuZnVuY3Rpb24gcGFyc2VEYXRlKHZhbHVlKSB7XG4gIGlmICh2YWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICByZXR1cm4gdmFsdWU7XG4gIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgIC8vIERpZmZlcmVudCBicm93c2VycyBwYXJzZSBkYXRlIGluIGRpZmZlcmVudCB3YXksIHNvIHdlIHBhcnNlIGl0IG1hbnVhbGx5LlxuICAgIC8vIFNvbWUgb3RoZXIgaXNzdWVzOlxuICAgIC8vIG5ldyBEYXRlKCcxOTcwLTAxLTAxJykgaXMgVVRDLFxuICAgIC8vIG5ldyBEYXRlKCcxOTcwLzAxLzAxJykgYW5kIG5ldyBEYXRlKCcxOTcwLTEtMDEnKSBpcyBsb2NhbC5cbiAgICAvLyBTZWUgaXNzdWUgIzM2MjNcbiAgICB2YXIgbWF0Y2ggPSBUSU1FX1JFRy5leGVjKHZhbHVlKTtcblxuICAgIGlmICghbWF0Y2gpIHtcbiAgICAgIC8vIHJldHVybiBJbnZhbGlkIERhdGUuXG4gICAgICByZXR1cm4gbmV3IERhdGUoTmFOKTtcbiAgICB9IC8vIFVzZSBsb2NhbCB0aW1lIHdoZW4gbm8gdGltZXpvbmUgb2Zmc2V0IHNwZWNpZmVkLlxuXG5cbiAgICBpZiAoIW1hdGNoWzhdKSB7XG4gICAgICAvLyBtYXRjaFtuXSBjYW4gb25seSBiZSBzdHJpbmcgb3IgdW5kZWZpbmVkLlxuICAgICAgLy8gQnV0IHRha2UgY2FyZSBvZiAnMTInICsgMSA9PiAnMTIxJy5cbiAgICAgIHJldHVybiBuZXcgRGF0ZSgrbWF0Y2hbMV0sICsobWF0Y2hbMl0gfHwgMSkgLSAxLCArbWF0Y2hbM10gfHwgMSwgK21hdGNoWzRdIHx8IDAsICsobWF0Y2hbNV0gfHwgMCksICttYXRjaFs2XSB8fCAwLCArbWF0Y2hbN10gfHwgMCk7XG4gICAgfSAvLyBUaW1lem9uZW9mZnNldCBvZiBKYXZhc2NyaXB0IERhdGUgaGFzIGNvbnNpZGVyZWQgRFNUIChEYXlsaWdodCBTYXZpbmcgVGltZSxcbiAgICAvLyBodHRwczovL3RjMzkuZ2l0aHViLmlvL2VjbWEyNjIvI3NlYy1kYXlsaWdodC1zYXZpbmctdGltZS1hZGp1c3RtZW50KS5cbiAgICAvLyBGb3IgZXhhbXBsZSwgc3lzdGVtIHRpbWV6b25lIGlzIHNldCBhcyBcIlRpbWUgWm9uZTogQW1lcmljYS9Ub3JvbnRvXCIsXG4gICAgLy8gdGhlbiB0aGVzZSBjb2RlIHdpbGwgZ2V0IGRpZmZlcmVudCByZXN1bHQ6XG4gICAgLy8gYG5ldyBEYXRlKDE0Nzg0MTE5OTk5OTkpLmdldFRpbWV6b25lT2Zmc2V0KCk7ICAvLyBnZXQgMjQwYFxuICAgIC8vIGBuZXcgRGF0ZSgxNDc4NDEyMDAwMDAwKS5nZXRUaW1lem9uZU9mZnNldCgpOyAgLy8gZ2V0IDMwMGBcbiAgICAvLyBTbyB3ZSBzaG91bGQgbm90IHVzZSBgbmV3IERhdGVgLCBidXQgdXNlIGBEYXRlLlVUQ2AuXG4gICAgZWxzZSB7XG4gICAgICAgIHZhciBob3VyID0gK21hdGNoWzRdIHx8IDA7XG5cbiAgICAgICAgaWYgKG1hdGNoWzhdLnRvVXBwZXJDYXNlKCkgIT09ICdaJykge1xuICAgICAgICAgIGhvdXIgLT0gbWF0Y2hbOF0uc2xpY2UoMCwgMyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbmV3IERhdGUoRGF0ZS5VVEMoK21hdGNoWzFdLCArKG1hdGNoWzJdIHx8IDEpIC0gMSwgK21hdGNoWzNdIHx8IDEsIGhvdXIsICsobWF0Y2hbNV0gfHwgMCksICttYXRjaFs2XSB8fCAwLCArbWF0Y2hbN10gfHwgMCkpO1xuICAgICAgfVxuICB9IGVsc2UgaWYgKHZhbHVlID09IG51bGwpIHtcbiAgICByZXR1cm4gbmV3IERhdGUoTmFOKTtcbiAgfVxuXG4gIHJldHVybiBuZXcgRGF0ZShNYXRoLnJvdW5kKHZhbHVlKSk7XG59XG4vKipcbiAqIFF1YW50aXR5IG9mIGEgbnVtYmVyLiBlLmcuIDAuMSwgMSwgMTAsIDEwMFxuICpcbiAqIEBwYXJhbSAge251bWJlcn0gdmFsXG4gKiBAcmV0dXJuIHtudW1iZXJ9XG4gKi9cblxuXG5mdW5jdGlvbiBxdWFudGl0eSh2YWwpIHtcbiAgcmV0dXJuIE1hdGgucG93KDEwLCBxdWFudGl0eUV4cG9uZW50KHZhbCkpO1xufVxuXG5mdW5jdGlvbiBxdWFudGl0eUV4cG9uZW50KHZhbCkge1xuICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLmxvZyh2YWwpIC8gTWF0aC5MTjEwKTtcbn1cbi8qKlxuICogZmluZCBhIOKAnG5pY2XigJ0gbnVtYmVyIGFwcHJveGltYXRlbHkgZXF1YWwgdG8geC4gUm91bmQgdGhlIG51bWJlciBpZiByb3VuZCA9IHRydWUsXG4gKiB0YWtlIGNlaWxpbmcgaWYgcm91bmQgPSBmYWxzZS4gVGhlIHByaW1hcnkgb2JzZXJ2YXRpb24gaXMgdGhhdCB0aGUg4oCcbmljZXN04oCdXG4gKiBudW1iZXJzIGluIGRlY2ltYWwgYXJlIDEsIDIsIGFuZCA1LCBhbmQgYWxsIHBvd2VyLW9mLXRlbiBtdWx0aXBsZXMgb2YgdGhlc2UgbnVtYmVycy5cbiAqXG4gKiBTZWUgXCJOaWNlIE51bWJlcnMgZm9yIEdyYXBoIExhYmVsc1wiIG9mIEdyYXBoaWMgR2Vtcy5cbiAqXG4gKiBAcGFyYW0gIHtudW1iZXJ9IHZhbCBOb24tbmVnYXRpdmUgdmFsdWUuXG4gKiBAcGFyYW0gIHtib29sZWFufSByb3VuZFxuICogQHJldHVybiB7bnVtYmVyfVxuICovXG5cblxuZnVuY3Rpb24gbmljZSh2YWwsIHJvdW5kKSB7XG4gIHZhciBleHBvbmVudCA9IHF1YW50aXR5RXhwb25lbnQodmFsKTtcbiAgdmFyIGV4cDEwID0gTWF0aC5wb3coMTAsIGV4cG9uZW50KTtcbiAgdmFyIGYgPSB2YWwgLyBleHAxMDsgLy8gMSA8PSBmIDwgMTBcblxuICB2YXIgbmY7XG5cbiAgaWYgKHJvdW5kKSB7XG4gICAgaWYgKGYgPCAxLjUpIHtcbiAgICAgIG5mID0gMTtcbiAgICB9IGVsc2UgaWYgKGYgPCAyLjUpIHtcbiAgICAgIG5mID0gMjtcbiAgICB9IGVsc2UgaWYgKGYgPCA0KSB7XG4gICAgICBuZiA9IDM7XG4gICAgfSBlbHNlIGlmIChmIDwgNykge1xuICAgICAgbmYgPSA1O1xuICAgIH0gZWxzZSB7XG4gICAgICBuZiA9IDEwO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoZiA8IDEpIHtcbiAgICAgIG5mID0gMTtcbiAgICB9IGVsc2UgaWYgKGYgPCAyKSB7XG4gICAgICBuZiA9IDI7XG4gICAgfSBlbHNlIGlmIChmIDwgMykge1xuICAgICAgbmYgPSAzO1xuICAgIH0gZWxzZSBpZiAoZiA8IDUpIHtcbiAgICAgIG5mID0gNTtcbiAgICB9IGVsc2Uge1xuICAgICAgbmYgPSAxMDtcbiAgICB9XG4gIH1cblxuICB2YWwgPSBuZiAqIGV4cDEwOyAvLyBGaXggMyAqIDAuMSA9PT0gMC4zMDAwMDAwMDAwMDAwMDAwNCBpc3N1ZSAoc2VlIElFRUUgNzU0KS5cbiAgLy8gMjAgaXMgdGhlIHVwcHBlciBib3VuZCBvZiB0b0ZpeGVkLlxuXG4gIHJldHVybiBleHBvbmVudCA+PSAtMjAgPyArdmFsLnRvRml4ZWQoZXhwb25lbnQgPCAwID8gLWV4cG9uZW50IDogMCkgOiB2YWw7XG59XG4vKipcbiAqIFRoaXMgY29kZSB3YXMgY29waWVkIGZyb20gXCJkMy5qc1wiXG4gKiA8aHR0cHM6Ly9naXRodWIuY29tL2QzL2QzL2Jsb2IvOWNjOWE4NzVlNjM2YTFkY2YzNmNjMWUwN2JkZjc3ZTFhZDZlMmM3NC9zcmMvYXJyYXlzL3F1YW50aWxlLmpzPi5cbiAqIFNlZSB0aGUgbGljZW5zZSBzdGF0ZW1lbnQgYXQgdGhlIGhlYWQgb2YgdGhpcyBmaWxlLlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gYXNjQXJyXG4gKi9cblxuXG5mdW5jdGlvbiBxdWFudGlsZShhc2NBcnIsIHApIHtcbiAgdmFyIEggPSAoYXNjQXJyLmxlbmd0aCAtIDEpICogcCArIDE7XG4gIHZhciBoID0gTWF0aC5mbG9vcihIKTtcbiAgdmFyIHYgPSArYXNjQXJyW2ggLSAxXTtcbiAgdmFyIGUgPSBIIC0gaDtcbiAgcmV0dXJuIGUgPyB2ICsgZSAqIChhc2NBcnJbaF0gLSB2KSA6IHY7XG59XG4vKipcbiAqIE9yZGVyIGludGVydmFscyBhc2MsIGFuZCBzcGxpdCB0aGVtIHdoZW4gb3ZlcmxhcC5cbiAqIGV4cGVjdChudW1iZXJVdGlsLnJlZm9ybUludGVydmFscyhbXG4gKiAgICAge2ludGVydmFsOiBbMTgsIDYyXSwgY2xvc2U6IFsxLCAxXX0sXG4gKiAgICAge2ludGVydmFsOiBbLUluZmluaXR5LCAtNzBdLCBjbG9zZTogWzAsIDBdfSxcbiAqICAgICB7aW50ZXJ2YWw6IFstNzAsIC0yNl0sIGNsb3NlOiBbMSwgMV19LFxuICogICAgIHtpbnRlcnZhbDogWy0yNiwgMThdLCBjbG9zZTogWzEsIDFdfSxcbiAqICAgICB7aW50ZXJ2YWw6IFs2MiwgMTUwXSwgY2xvc2U6IFsxLCAxXX0sXG4gKiAgICAge2ludGVydmFsOiBbMTA2LCAxNTBdLCBjbG9zZTogWzEsIDFdfSxcbiAqICAgICB7aW50ZXJ2YWw6IFsxNTAsIEluZmluaXR5XSwgY2xvc2U6IFswLCAwXX1cbiAqIF0pKS50b0VxdWFsKFtcbiAqICAgICB7aW50ZXJ2YWw6IFstSW5maW5pdHksIC03MF0sIGNsb3NlOiBbMCwgMF19LFxuICogICAgIHtpbnRlcnZhbDogWy03MCwgLTI2XSwgY2xvc2U6IFsxLCAxXX0sXG4gKiAgICAge2ludGVydmFsOiBbLTI2LCAxOF0sIGNsb3NlOiBbMCwgMV19LFxuICogICAgIHtpbnRlcnZhbDogWzE4LCA2Ml0sIGNsb3NlOiBbMCwgMV19LFxuICogICAgIHtpbnRlcnZhbDogWzYyLCAxNTBdLCBjbG9zZTogWzAsIDFdfSxcbiAqICAgICB7aW50ZXJ2YWw6IFsxNTAsIEluZmluaXR5XSwgY2xvc2U6IFswLCAwXX1cbiAqIF0pO1xuICogQHBhcmFtIHtBcnJheS48T2JqZWN0Pn0gbGlzdCwgd2hlcmUgYGNsb3NlYCBtZWFuIG9wZW4gb3IgY2xvc2VcbiAqICAgICAgICBvZiB0aGUgaW50ZXJ2YWwsIGFuZCBJbmZpbml0eSBjYW4gYmUgdXNlZC5cbiAqIEByZXR1cm4ge0FycmF5LjxPYmplY3Q+fSBUaGUgb3JpZ2luIGxpc3QsIHdoaWNoIGhhcyBiZWVuIHJlZm9ybWVkLlxuICovXG5cblxuZnVuY3Rpb24gcmVmb3JtSW50ZXJ2YWxzKGxpc3QpIHtcbiAgbGlzdC5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgcmV0dXJuIGxpdHRsZVRoYW4oYSwgYiwgMCkgPyAtMSA6IDE7XG4gIH0pO1xuICB2YXIgY3VyciA9IC1JbmZpbml0eTtcbiAgdmFyIGN1cnJDbG9zZSA9IDE7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDspIHtcbiAgICB2YXIgaW50ZXJ2YWwgPSBsaXN0W2ldLmludGVydmFsO1xuICAgIHZhciBjbG9zZSA9IGxpc3RbaV0uY2xvc2U7XG5cbiAgICBmb3IgKHZhciBsZyA9IDA7IGxnIDwgMjsgbGcrKykge1xuICAgICAgaWYgKGludGVydmFsW2xnXSA8PSBjdXJyKSB7XG4gICAgICAgIGludGVydmFsW2xnXSA9IGN1cnI7XG4gICAgICAgIGNsb3NlW2xnXSA9ICFsZyA/IDEgLSBjdXJyQ2xvc2UgOiAxO1xuICAgICAgfVxuXG4gICAgICBjdXJyID0gaW50ZXJ2YWxbbGddO1xuICAgICAgY3VyckNsb3NlID0gY2xvc2VbbGddO1xuICAgIH1cblxuICAgIGlmIChpbnRlcnZhbFswXSA9PT0gaW50ZXJ2YWxbMV0gJiYgY2xvc2VbMF0gKiBjbG9zZVsxXSAhPT0gMSkge1xuICAgICAgbGlzdC5zcGxpY2UoaSwgMSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGkrKztcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbGlzdDtcblxuICBmdW5jdGlvbiBsaXR0bGVUaGFuKGEsIGIsIGxnKSB7XG4gICAgcmV0dXJuIGEuaW50ZXJ2YWxbbGddIDwgYi5pbnRlcnZhbFtsZ10gfHwgYS5pbnRlcnZhbFtsZ10gPT09IGIuaW50ZXJ2YWxbbGddICYmIChhLmNsb3NlW2xnXSAtIGIuY2xvc2VbbGddID09PSAoIWxnID8gMSA6IC0xKSB8fCAhbGcgJiYgbGl0dGxlVGhhbihhLCBiLCAxKSk7XG4gIH1cbn1cbi8qKlxuICogcGFyc2VGbG9hdCBOYU5zIG51bWVyaWMtY2FzdCBmYWxzZSBwb3NpdGl2ZXMgKG51bGx8dHJ1ZXxmYWxzZXxcIlwiKVxuICogLi4uYnV0IG1pc2ludGVycHJldHMgbGVhZGluZy1udW1iZXIgc3RyaW5ncywgcGFydGljdWxhcmx5IGhleCBsaXRlcmFscyAoXCIweC4uLlwiKVxuICogc3VidHJhY3Rpb24gZm9yY2VzIGluZmluaXRpZXMgdG8gTmFOXG4gKlxuICogQHBhcmFtIHsqfSB2XG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gaXNOdW1lcmljKHYpIHtcbiAgcmV0dXJuIHYgLSBwYXJzZUZsb2F0KHYpID49IDA7XG59XG5cbmV4cG9ydHMubGluZWFyTWFwID0gbGluZWFyTWFwO1xuZXhwb3J0cy5wYXJzZVBlcmNlbnQgPSBwYXJzZVBlcmNlbnQ7XG5leHBvcnRzLnJvdW5kID0gcm91bmQ7XG5leHBvcnRzLmFzYyA9IGFzYztcbmV4cG9ydHMuZ2V0UHJlY2lzaW9uID0gZ2V0UHJlY2lzaW9uO1xuZXhwb3J0cy5nZXRQcmVjaXNpb25TYWZlID0gZ2V0UHJlY2lzaW9uU2FmZTtcbmV4cG9ydHMuZ2V0UGl4ZWxQcmVjaXNpb24gPSBnZXRQaXhlbFByZWNpc2lvbjtcbmV4cG9ydHMuZ2V0UGVyY2VudFdpdGhQcmVjaXNpb24gPSBnZXRQZXJjZW50V2l0aFByZWNpc2lvbjtcbmV4cG9ydHMuTUFYX1NBRkVfSU5URUdFUiA9IE1BWF9TQUZFX0lOVEVHRVI7XG5leHBvcnRzLnJlbVJhZGlhbiA9IHJlbVJhZGlhbjtcbmV4cG9ydHMuaXNSYWRpYW5Bcm91bmRaZXJvID0gaXNSYWRpYW5Bcm91bmRaZXJvO1xuZXhwb3J0cy5wYXJzZURhdGUgPSBwYXJzZURhdGU7XG5leHBvcnRzLnF1YW50aXR5ID0gcXVhbnRpdHk7XG5leHBvcnRzLm5pY2UgPSBuaWNlO1xuZXhwb3J0cy5xdWFudGlsZSA9IHF1YW50aWxlO1xuZXhwb3J0cy5yZWZvcm1JbnRlcnZhbHMgPSByZWZvcm1JbnRlcnZhbHM7XG5leHBvcnRzLmlzTnVtZXJpYyA9IGlzTnVtZXJpYzsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/number.js
