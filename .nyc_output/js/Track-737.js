__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);


/* eslint-disable react/prop-types */



var Track = function Track(props) {
  var _ref, _ref2;

  var className = props.className,
      included = props.included,
      vertical = props.vertical,
      offset = props.offset,
      length = props.length,
      style = props.style,
      reverse = props.reverse;
  var positonStyle = vertical ? (_ref = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref, reverse ? 'top' : 'bottom', offset + '%'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref, reverse ? 'bottom' : 'top', 'auto'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref, 'height', length + '%'), _ref) : (_ref2 = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref2, reverse ? 'right' : 'left', offset + '%'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref2, reverse ? 'left' : 'right', 'auto'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(_ref2, 'width', length + '%'), _ref2);

  var elStyle = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, style, positonStyle);

  return included ? react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement('div', {
    className: className,
    style: elStyle
  }) : null;
};

/* harmony default export */ __webpack_exports__["default"] = (Track);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL2NvbW1vbi9UcmFjay5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNsaWRlci9lcy9jb21tb24vVHJhY2suanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jztcbi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L3Byb3AtdHlwZXMgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5cbnZhciBUcmFjayA9IGZ1bmN0aW9uIFRyYWNrKHByb3BzKSB7XG4gIHZhciBfcmVmLCBfcmVmMjtcblxuICB2YXIgY2xhc3NOYW1lID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgaW5jbHVkZWQgPSBwcm9wcy5pbmNsdWRlZCxcbiAgICAgIHZlcnRpY2FsID0gcHJvcHMudmVydGljYWwsXG4gICAgICBvZmZzZXQgPSBwcm9wcy5vZmZzZXQsXG4gICAgICBsZW5ndGggPSBwcm9wcy5sZW5ndGgsXG4gICAgICBzdHlsZSA9IHByb3BzLnN0eWxlLFxuICAgICAgcmV2ZXJzZSA9IHByb3BzLnJldmVyc2U7XG5cbiAgdmFyIHBvc2l0b25TdHlsZSA9IHZlcnRpY2FsID8gKF9yZWYgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9yZWYsIHJldmVyc2UgPyAndG9wJyA6ICdib3R0b20nLCBvZmZzZXQgKyAnJScpLCBfZGVmaW5lUHJvcGVydHkoX3JlZiwgcmV2ZXJzZSA/ICdib3R0b20nIDogJ3RvcCcsICdhdXRvJyksIF9kZWZpbmVQcm9wZXJ0eShfcmVmLCAnaGVpZ2h0JywgbGVuZ3RoICsgJyUnKSwgX3JlZikgOiAoX3JlZjIgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9yZWYyLCByZXZlcnNlID8gJ3JpZ2h0JyA6ICdsZWZ0Jywgb2Zmc2V0ICsgJyUnKSwgX2RlZmluZVByb3BlcnR5KF9yZWYyLCByZXZlcnNlID8gJ2xlZnQnIDogJ3JpZ2h0JywgJ2F1dG8nKSwgX2RlZmluZVByb3BlcnR5KF9yZWYyLCAnd2lkdGgnLCBsZW5ndGggKyAnJScpLCBfcmVmMik7XG5cbiAgdmFyIGVsU3R5bGUgPSBfZXh0ZW5kcyh7fSwgc3R5bGUsIHBvc2l0b25TdHlsZSk7XG4gIHJldHVybiBpbmNsdWRlZCA/IFJlYWN0LmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsIHN0eWxlOiBlbFN0eWxlIH0pIDogbnVsbDtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRyYWNrOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/common/Track.js
