__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ANT_MARK", function() { return ANT_MARK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LocaleProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _util_interopDefault__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/interopDefault */ "./node_modules/antd/es/_util/interopDefault.js");
/* harmony import */ var _modal_locale__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modal/locale */ "./node_modules/antd/es/modal/locale.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var ANT_MARK = 'internalMark';

function setMomentLocale(locale) {
  if (locale && locale.locale) {
    Object(_util_interopDefault__WEBPACK_IMPORTED_MODULE_3__["default"])(moment__WEBPACK_IMPORTED_MODULE_2__).locale(locale.locale);
  } else {
    Object(_util_interopDefault__WEBPACK_IMPORTED_MODULE_3__["default"])(moment__WEBPACK_IMPORTED_MODULE_2__).locale('en');
  }
}

var LocaleProvider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LocaleProvider, _React$Component);

  function LocaleProvider(props) {
    var _this;

    _classCallCheck(this, LocaleProvider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LocaleProvider).call(this, props));
    setMomentLocale(props.locale);
    Object(_modal_locale__WEBPACK_IMPORTED_MODULE_4__["changeConfirmLocale"])(props.locale && props.locale.Modal);
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_5__["default"])(props._ANT_MARK__ === ANT_MARK, 'LocaleProvider', '`LocaleProvider` is deprecated. Please use `locale` with `ConfigProvider` instead: http://u.ant.design/locale');
    return _this;
  }

  _createClass(LocaleProvider, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        antLocale: _extends(_extends({}, this.props.locale), {
          exist: true
        })
      };
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var locale = this.props.locale;

      if (prevProps.locale !== locale) {
        setMomentLocale(locale);
        Object(_modal_locale__WEBPACK_IMPORTED_MODULE_4__["changeConfirmLocale"])(locale && locale.Modal);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      Object(_modal_locale__WEBPACK_IMPORTED_MODULE_4__["changeConfirmLocale"])();
    }
  }, {
    key: "render",
    value: function render() {
      return this.props.children;
    }
  }]);

  return LocaleProvider;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


LocaleProvider.propTypes = {
  locale: prop_types__WEBPACK_IMPORTED_MODULE_1__["object"]
};
LocaleProvider.defaultProps = {
  locale: {}
};
LocaleProvider.childContextTypes = {
  antLocale: prop_types__WEBPACK_IMPORTED_MODULE_1__["object"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9sb2NhbGUtcHJvdmlkZXIvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2xvY2FsZS1wcm92aWRlci9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudCc7XG5pbXBvcnQgaW50ZXJvcERlZmF1bHQgZnJvbSAnLi4vX3V0aWwvaW50ZXJvcERlZmF1bHQnO1xuaW1wb3J0IHsgY2hhbmdlQ29uZmlybUxvY2FsZSB9IGZyb20gJy4uL21vZGFsL2xvY2FsZSc7XG5pbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbmV4cG9ydCBjb25zdCBBTlRfTUFSSyA9ICdpbnRlcm5hbE1hcmsnO1xuZnVuY3Rpb24gc2V0TW9tZW50TG9jYWxlKGxvY2FsZSkge1xuICAgIGlmIChsb2NhbGUgJiYgbG9jYWxlLmxvY2FsZSkge1xuICAgICAgICBpbnRlcm9wRGVmYXVsdChtb21lbnQpLmxvY2FsZShsb2NhbGUubG9jYWxlKTtcbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICAgIGludGVyb3BEZWZhdWx0KG1vbWVudCkubG9jYWxlKCdlbicpO1xuICAgIH1cbn1cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIExvY2FsZVByb3ZpZGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHNldE1vbWVudExvY2FsZShwcm9wcy5sb2NhbGUpO1xuICAgICAgICBjaGFuZ2VDb25maXJtTG9jYWxlKHByb3BzLmxvY2FsZSAmJiBwcm9wcy5sb2NhbGUuTW9kYWwpO1xuICAgICAgICB3YXJuaW5nKHByb3BzLl9BTlRfTUFSS19fID09PSBBTlRfTUFSSywgJ0xvY2FsZVByb3ZpZGVyJywgJ2BMb2NhbGVQcm92aWRlcmAgaXMgZGVwcmVjYXRlZC4gUGxlYXNlIHVzZSBgbG9jYWxlYCB3aXRoIGBDb25maWdQcm92aWRlcmAgaW5zdGVhZDogaHR0cDovL3UuYW50LmRlc2lnbi9sb2NhbGUnKTtcbiAgICB9XG4gICAgZ2V0Q2hpbGRDb250ZXh0KCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgYW50TG9jYWxlOiBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHRoaXMucHJvcHMubG9jYWxlKSwgeyBleGlzdDogdHJ1ZSB9KSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgICBjb25zdCB7IGxvY2FsZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKHByZXZQcm9wcy5sb2NhbGUgIT09IGxvY2FsZSkge1xuICAgICAgICAgICAgc2V0TW9tZW50TG9jYWxlKGxvY2FsZSk7XG4gICAgICAgICAgICBjaGFuZ2VDb25maXJtTG9jYWxlKGxvY2FsZSAmJiBsb2NhbGUuTW9kYWwpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICBjaGFuZ2VDb25maXJtTG9jYWxlKCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcHMuY2hpbGRyZW47XG4gICAgfVxufVxuTG9jYWxlUHJvdmlkZXIucHJvcFR5cGVzID0ge1xuICAgIGxvY2FsZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5Mb2NhbGVQcm92aWRlci5kZWZhdWx0UHJvcHMgPSB7XG4gICAgbG9jYWxlOiB7fSxcbn07XG5Mb2NhbGVQcm92aWRlci5jaGlsZENvbnRleHRUeXBlcyA9IHtcbiAgICBhbnRMb2NhbGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFHQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUF4QkE7QUFDQTtBQURBO0FBMEJBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/locale-provider/index.js
