/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule ContentBlockNode
 * @format
 * 
 *
 * This file is a fork of ContentBlock adding support for nesting references by
 * providing links to children, parent, prevSibling, and nextSibling.
 *
 * This is unstable and not part of the public API and should not be used by
 * production systems. This file may be update/removed without notice.
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var findRangesImmutable = __webpack_require__(/*! ./findRangesImmutable */ "./node_modules/draft-js/lib/findRangesImmutable.js");

var List = Immutable.List,
    Map = Immutable.Map,
    OrderedSet = Immutable.OrderedSet,
    Record = Immutable.Record,
    Repeat = Immutable.Repeat;
var EMPTY_SET = OrderedSet();
var defaultRecord = {
  parent: null,
  characterList: List(),
  data: Map(),
  depth: 0,
  key: '',
  text: '',
  type: 'unstyled',
  children: List(),
  prevSibling: null,
  nextSibling: null
};

var haveEqualStyle = function haveEqualStyle(charA, charB) {
  return charA.getStyle() === charB.getStyle();
};

var haveEqualEntity = function haveEqualEntity(charA, charB) {
  return charA.getEntity() === charB.getEntity();
};

var decorateCharacterList = function decorateCharacterList(config) {
  if (!config) {
    return config;
  }

  var characterList = config.characterList,
      text = config.text;

  if (text && !characterList) {
    config.characterList = List(Repeat(CharacterMetadata.EMPTY, text.length));
  }

  return config;
};

var ContentBlockNode = function (_Record) {
  _inherits(ContentBlockNode, _Record);

  function ContentBlockNode() {
    var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultRecord;

    _classCallCheck(this, ContentBlockNode);

    return _possibleConstructorReturn(this, _Record.call(this, decorateCharacterList(props)));
  }

  ContentBlockNode.prototype.getKey = function getKey() {
    return this.get('key');
  };

  ContentBlockNode.prototype.getType = function getType() {
    return this.get('type');
  };

  ContentBlockNode.prototype.getText = function getText() {
    return this.get('text');
  };

  ContentBlockNode.prototype.getCharacterList = function getCharacterList() {
    return this.get('characterList');
  };

  ContentBlockNode.prototype.getLength = function getLength() {
    return this.getText().length;
  };

  ContentBlockNode.prototype.getDepth = function getDepth() {
    return this.get('depth');
  };

  ContentBlockNode.prototype.getData = function getData() {
    return this.get('data');
  };

  ContentBlockNode.prototype.getInlineStyleAt = function getInlineStyleAt(offset) {
    var character = this.getCharacterList().get(offset);
    return character ? character.getStyle() : EMPTY_SET;
  };

  ContentBlockNode.prototype.getEntityAt = function getEntityAt(offset) {
    var character = this.getCharacterList().get(offset);
    return character ? character.getEntity() : null;
  };

  ContentBlockNode.prototype.getChildKeys = function getChildKeys() {
    return this.get('children');
  };

  ContentBlockNode.prototype.getParentKey = function getParentKey() {
    return this.get('parent');
  };

  ContentBlockNode.prototype.getPrevSiblingKey = function getPrevSiblingKey() {
    return this.get('prevSibling');
  };

  ContentBlockNode.prototype.getNextSiblingKey = function getNextSiblingKey() {
    return this.get('nextSibling');
  };

  ContentBlockNode.prototype.findStyleRanges = function findStyleRanges(filterFn, callback) {
    findRangesImmutable(this.getCharacterList(), haveEqualStyle, filterFn, callback);
  };

  ContentBlockNode.prototype.findEntityRanges = function findEntityRanges(filterFn, callback) {
    findRangesImmutable(this.getCharacterList(), haveEqualEntity, filterFn, callback);
  };

  return ContentBlockNode;
}(Record(defaultRecord));

module.exports = ContentBlockNode;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbnRlbnRCbG9ja05vZGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvQ29udGVudEJsb2NrTm9kZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIENvbnRlbnRCbG9ja05vZGVcbiAqIEBmb3JtYXRcbiAqIFxuICpcbiAqIFRoaXMgZmlsZSBpcyBhIGZvcmsgb2YgQ29udGVudEJsb2NrIGFkZGluZyBzdXBwb3J0IGZvciBuZXN0aW5nIHJlZmVyZW5jZXMgYnlcbiAqIHByb3ZpZGluZyBsaW5rcyB0byBjaGlsZHJlbiwgcGFyZW50LCBwcmV2U2libGluZywgYW5kIG5leHRTaWJsaW5nLlxuICpcbiAqIFRoaXMgaXMgdW5zdGFibGUgYW5kIG5vdCBwYXJ0IG9mIHRoZSBwdWJsaWMgQVBJIGFuZCBzaG91bGQgbm90IGJlIHVzZWQgYnlcbiAqIHByb2R1Y3Rpb24gc3lzdGVtcy4gVGhpcyBmaWxlIG1heSBiZSB1cGRhdGUvcmVtb3ZlZCB3aXRob3V0IG5vdGljZS5cbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgZmluZFJhbmdlc0ltbXV0YWJsZSA9IHJlcXVpcmUoJy4vZmluZFJhbmdlc0ltbXV0YWJsZScpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0LFxuICAgIE1hcCA9IEltbXV0YWJsZS5NYXAsXG4gICAgT3JkZXJlZFNldCA9IEltbXV0YWJsZS5PcmRlcmVkU2V0LFxuICAgIFJlY29yZCA9IEltbXV0YWJsZS5SZWNvcmQsXG4gICAgUmVwZWF0ID0gSW1tdXRhYmxlLlJlcGVhdDtcblxuXG52YXIgRU1QVFlfU0VUID0gT3JkZXJlZFNldCgpO1xuXG52YXIgZGVmYXVsdFJlY29yZCA9IHtcbiAgcGFyZW50OiBudWxsLFxuICBjaGFyYWN0ZXJMaXN0OiBMaXN0KCksXG4gIGRhdGE6IE1hcCgpLFxuICBkZXB0aDogMCxcbiAga2V5OiAnJyxcbiAgdGV4dDogJycsXG4gIHR5cGU6ICd1bnN0eWxlZCcsXG4gIGNoaWxkcmVuOiBMaXN0KCksXG4gIHByZXZTaWJsaW5nOiBudWxsLFxuICBuZXh0U2libGluZzogbnVsbFxufTtcblxudmFyIGhhdmVFcXVhbFN0eWxlID0gZnVuY3Rpb24gaGF2ZUVxdWFsU3R5bGUoY2hhckEsIGNoYXJCKSB7XG4gIHJldHVybiBjaGFyQS5nZXRTdHlsZSgpID09PSBjaGFyQi5nZXRTdHlsZSgpO1xufTtcblxudmFyIGhhdmVFcXVhbEVudGl0eSA9IGZ1bmN0aW9uIGhhdmVFcXVhbEVudGl0eShjaGFyQSwgY2hhckIpIHtcbiAgcmV0dXJuIGNoYXJBLmdldEVudGl0eSgpID09PSBjaGFyQi5nZXRFbnRpdHkoKTtcbn07XG5cbnZhciBkZWNvcmF0ZUNoYXJhY3Rlckxpc3QgPSBmdW5jdGlvbiBkZWNvcmF0ZUNoYXJhY3Rlckxpc3QoY29uZmlnKSB7XG4gIGlmICghY29uZmlnKSB7XG4gICAgcmV0dXJuIGNvbmZpZztcbiAgfVxuXG4gIHZhciBjaGFyYWN0ZXJMaXN0ID0gY29uZmlnLmNoYXJhY3Rlckxpc3QsXG4gICAgICB0ZXh0ID0gY29uZmlnLnRleHQ7XG5cblxuICBpZiAodGV4dCAmJiAhY2hhcmFjdGVyTGlzdCkge1xuICAgIGNvbmZpZy5jaGFyYWN0ZXJMaXN0ID0gTGlzdChSZXBlYXQoQ2hhcmFjdGVyTWV0YWRhdGEuRU1QVFksIHRleHQubGVuZ3RoKSk7XG4gIH1cblxuICByZXR1cm4gY29uZmlnO1xufTtcblxudmFyIENvbnRlbnRCbG9ja05vZGUgPSBmdW5jdGlvbiAoX1JlY29yZCkge1xuICBfaW5oZXJpdHMoQ29udGVudEJsb2NrTm9kZSwgX1JlY29yZCk7XG5cbiAgZnVuY3Rpb24gQ29udGVudEJsb2NrTm9kZSgpIHtcbiAgICB2YXIgcHJvcHMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IGRlZmF1bHRSZWNvcmQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29udGVudEJsb2NrTm9kZSk7XG5cbiAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlY29yZC5jYWxsKHRoaXMsIGRlY29yYXRlQ2hhcmFjdGVyTGlzdChwcm9wcykpKTtcbiAgfVxuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldEtleSA9IGZ1bmN0aW9uIGdldEtleSgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXQoJ2tleScpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldFR5cGUgPSBmdW5jdGlvbiBnZXRUeXBlKCkge1xuICAgIHJldHVybiB0aGlzLmdldCgndHlwZScpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldFRleHQgPSBmdW5jdGlvbiBnZXRUZXh0KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgndGV4dCcpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldENoYXJhY3Rlckxpc3QgPSBmdW5jdGlvbiBnZXRDaGFyYWN0ZXJMaXN0KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnY2hhcmFjdGVyTGlzdCcpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldExlbmd0aCA9IGZ1bmN0aW9uIGdldExlbmd0aCgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRUZXh0KCkubGVuZ3RoO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldERlcHRoID0gZnVuY3Rpb24gZ2V0RGVwdGgoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdkZXB0aCcpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldERhdGEgPSBmdW5jdGlvbiBnZXREYXRhKCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnZGF0YScpO1xuICB9O1xuXG4gIENvbnRlbnRCbG9ja05vZGUucHJvdG90eXBlLmdldElubGluZVN0eWxlQXQgPSBmdW5jdGlvbiBnZXRJbmxpbmVTdHlsZUF0KG9mZnNldCkge1xuICAgIHZhciBjaGFyYWN0ZXIgPSB0aGlzLmdldENoYXJhY3Rlckxpc3QoKS5nZXQob2Zmc2V0KTtcbiAgICByZXR1cm4gY2hhcmFjdGVyID8gY2hhcmFjdGVyLmdldFN0eWxlKCkgOiBFTVBUWV9TRVQ7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrTm9kZS5wcm90b3R5cGUuZ2V0RW50aXR5QXQgPSBmdW5jdGlvbiBnZXRFbnRpdHlBdChvZmZzZXQpIHtcbiAgICB2YXIgY2hhcmFjdGVyID0gdGhpcy5nZXRDaGFyYWN0ZXJMaXN0KCkuZ2V0KG9mZnNldCk7XG4gICAgcmV0dXJuIGNoYXJhY3RlciA/IGNoYXJhY3Rlci5nZXRFbnRpdHkoKSA6IG51bGw7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrTm9kZS5wcm90b3R5cGUuZ2V0Q2hpbGRLZXlzID0gZnVuY3Rpb24gZ2V0Q2hpbGRLZXlzKCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnY2hpbGRyZW4nKTtcbiAgfTtcblxuICBDb250ZW50QmxvY2tOb2RlLnByb3RvdHlwZS5nZXRQYXJlbnRLZXkgPSBmdW5jdGlvbiBnZXRQYXJlbnRLZXkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdwYXJlbnQnKTtcbiAgfTtcblxuICBDb250ZW50QmxvY2tOb2RlLnByb3RvdHlwZS5nZXRQcmV2U2libGluZ0tleSA9IGZ1bmN0aW9uIGdldFByZXZTaWJsaW5nS2V5KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgncHJldlNpYmxpbmcnKTtcbiAgfTtcblxuICBDb250ZW50QmxvY2tOb2RlLnByb3RvdHlwZS5nZXROZXh0U2libGluZ0tleSA9IGZ1bmN0aW9uIGdldE5leHRTaWJsaW5nS2V5KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnbmV4dFNpYmxpbmcnKTtcbiAgfTtcblxuICBDb250ZW50QmxvY2tOb2RlLnByb3RvdHlwZS5maW5kU3R5bGVSYW5nZXMgPSBmdW5jdGlvbiBmaW5kU3R5bGVSYW5nZXMoZmlsdGVyRm4sIGNhbGxiYWNrKSB7XG4gICAgZmluZFJhbmdlc0ltbXV0YWJsZSh0aGlzLmdldENoYXJhY3Rlckxpc3QoKSwgaGF2ZUVxdWFsU3R5bGUsIGZpbHRlckZuLCBjYWxsYmFjayk7XG4gIH07XG5cbiAgQ29udGVudEJsb2NrTm9kZS5wcm90b3R5cGUuZmluZEVudGl0eVJhbmdlcyA9IGZ1bmN0aW9uIGZpbmRFbnRpdHlSYW5nZXMoZmlsdGVyRm4sIGNhbGxiYWNrKSB7XG4gICAgZmluZFJhbmdlc0ltbXV0YWJsZSh0aGlzLmdldENoYXJhY3Rlckxpc3QoKSwgaGF2ZUVxdWFsRW50aXR5LCBmaWx0ZXJGbiwgY2FsbGJhY2spO1xuICB9O1xuXG4gIHJldHVybiBDb250ZW50QmxvY2tOb2RlO1xufShSZWNvcmQoZGVmYXVsdFJlY29yZCkpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvbnRlbnRCbG9ja05vZGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/ContentBlockNode.js
