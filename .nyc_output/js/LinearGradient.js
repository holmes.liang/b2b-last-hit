var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var Gradient = __webpack_require__(/*! ./Gradient */ "./node_modules/zrender/lib/graphic/Gradient.js");
/**
 * x, y, x2, y2 are all percent from 0 to 1
 * @param {number} [x=0]
 * @param {number} [y=0]
 * @param {number} [x2=1]
 * @param {number} [y2=0]
 * @param {Array.<Object>} colorStops
 * @param {boolean} [globalCoord=false]
 */


var LinearGradient = function LinearGradient(x, y, x2, y2, colorStops, globalCoord) {
  // Should do nothing more in this constructor. Because gradient can be
  // declard by `color: {type: 'linear', colorStops: ...}`, where
  // this constructor will not be called.
  this.x = x == null ? 0 : x;
  this.y = y == null ? 0 : y;
  this.x2 = x2 == null ? 1 : x2;
  this.y2 = y2 == null ? 0 : y2; // Can be cloned

  this.type = 'linear'; // If use global coord

  this.global = globalCoord || false;
  Gradient.call(this, colorStops);
};

LinearGradient.prototype = {
  constructor: LinearGradient
};
zrUtil.inherits(LinearGradient, Gradient);
var _default = LinearGradient;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9MaW5lYXJHcmFkaWVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvTGluZWFyR3JhZGllbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBHcmFkaWVudCA9IHJlcXVpcmUoXCIuL0dyYWRpZW50XCIpO1xuXG4vKipcbiAqIHgsIHksIHgyLCB5MiBhcmUgYWxsIHBlcmNlbnQgZnJvbSAwIHRvIDFcbiAqIEBwYXJhbSB7bnVtYmVyfSBbeD0wXVxuICogQHBhcmFtIHtudW1iZXJ9IFt5PTBdXG4gKiBAcGFyYW0ge251bWJlcn0gW3gyPTFdXG4gKiBAcGFyYW0ge251bWJlcn0gW3kyPTBdXG4gKiBAcGFyYW0ge0FycmF5LjxPYmplY3Q+fSBjb2xvclN0b3BzXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtnbG9iYWxDb29yZD1mYWxzZV1cbiAqL1xudmFyIExpbmVhckdyYWRpZW50ID0gZnVuY3Rpb24gKHgsIHksIHgyLCB5MiwgY29sb3JTdG9wcywgZ2xvYmFsQ29vcmQpIHtcbiAgLy8gU2hvdWxkIGRvIG5vdGhpbmcgbW9yZSBpbiB0aGlzIGNvbnN0cnVjdG9yLiBCZWNhdXNlIGdyYWRpZW50IGNhbiBiZVxuICAvLyBkZWNsYXJkIGJ5IGBjb2xvcjoge3R5cGU6ICdsaW5lYXInLCBjb2xvclN0b3BzOiAuLi59YCwgd2hlcmVcbiAgLy8gdGhpcyBjb25zdHJ1Y3RvciB3aWxsIG5vdCBiZSBjYWxsZWQuXG4gIHRoaXMueCA9IHggPT0gbnVsbCA/IDAgOiB4O1xuICB0aGlzLnkgPSB5ID09IG51bGwgPyAwIDogeTtcbiAgdGhpcy54MiA9IHgyID09IG51bGwgPyAxIDogeDI7XG4gIHRoaXMueTIgPSB5MiA9PSBudWxsID8gMCA6IHkyOyAvLyBDYW4gYmUgY2xvbmVkXG5cbiAgdGhpcy50eXBlID0gJ2xpbmVhcic7IC8vIElmIHVzZSBnbG9iYWwgY29vcmRcblxuICB0aGlzLmdsb2JhbCA9IGdsb2JhbENvb3JkIHx8IGZhbHNlO1xuICBHcmFkaWVudC5jYWxsKHRoaXMsIGNvbG9yU3RvcHMpO1xufTtcblxuTGluZWFyR3JhZGllbnQucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogTGluZWFyR3JhZGllbnRcbn07XG56clV0aWwuaW5oZXJpdHMoTGluZWFyR3JhZGllbnQsIEdyYWRpZW50KTtcbnZhciBfZGVmYXVsdCA9IExpbmVhckdyYWRpZW50O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/LinearGradient.js
