__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "popupContextTypes", function() { return popupContextTypes; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_tree__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-tree */ "./node_modules/rc-tree/es/index.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../util */ "./node_modules/rc-tree-select/es/util.js");
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}






var popupContextTypes = {
  onPopupKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  onTreeNodeSelect: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired,
  onTreeNodeCheck: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func.isRequired
};

var BasePopup =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BasePopup, _React$Component);

  function BasePopup(props) {
    var _this;

    _classCallCheck(this, BasePopup);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BasePopup).call(this));

    _defineProperty(_assertThisInitialized(_this), "onTreeExpand", function (expandedKeyList) {
      var _this$props = _this.props,
          treeExpandedKeys = _this$props.treeExpandedKeys,
          onTreeExpand = _this$props.onTreeExpand,
          onTreeExpanded = _this$props.onTreeExpanded; // Set uncontrolled state

      if (!treeExpandedKeys) {
        _this.setState({
          expandedKeyList: expandedKeyList
        }, onTreeExpanded);
      }

      if (onTreeExpand) {
        onTreeExpand(expandedKeyList);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onLoad", function (loadedKeys) {
      _this.setState({
        loadedKeys: loadedKeys
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getTree", function () {
      return _this.treeRef.current;
    });

    _defineProperty(_assertThisInitialized(_this), "getLoadData", function () {
      var _this$props2 = _this.props,
          loadData = _this$props2.loadData,
          upperSearchValue = _this$props2.upperSearchValue;
      if (upperSearchValue) return null;
      return loadData;
    });

    _defineProperty(_assertThisInitialized(_this), "filterTreeNode", function (treeNode) {
      var _this$props3 = _this.props,
          upperSearchValue = _this$props3.upperSearchValue,
          treeNodeFilterProp = _this$props3.treeNodeFilterProp;
      var filterVal = treeNode.props[treeNodeFilterProp];

      if (typeof filterVal === 'string') {
        return upperSearchValue && filterVal.toUpperCase().indexOf(upperSearchValue) !== -1;
      }

      return false;
    });

    _defineProperty(_assertThisInitialized(_this), "renderNotFound", function () {
      var _this$props4 = _this.props,
          prefixCls = _this$props4.prefixCls,
          notFoundContent = _this$props4.notFoundContent;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "".concat(prefixCls, "-not-found")
      }, notFoundContent);
    });

    var treeDefaultExpandAll = props.treeDefaultExpandAll,
        treeDefaultExpandedKeys = props.treeDefaultExpandedKeys,
        keyEntities = props.keyEntities; // TODO: make `expandedKeyList` control

    var _expandedKeyList = treeDefaultExpandedKeys;

    if (treeDefaultExpandAll) {
      _expandedKeyList = Object.keys(keyEntities);
    }

    _this.state = {
      keyList: [],
      expandedKeyList: _expandedKeyList,
      // Cache `expandedKeyList` when tree is in filter. This is used in `getDerivedStateFromProps`
      cachedExpandedKeyList: [],
      // eslint-disable-line react/no-unused-state
      loadedKeys: []
    };
    _this.treeRef = Object(_util__WEBPACK_IMPORTED_MODULE_4__["createRef"])();
    return _this;
  }

  _createClass(BasePopup, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          keyList = _this$state.keyList,
          expandedKeyList = _this$state.expandedKeyList,
          loadedKeys = _this$state.loadedKeys;
      var _this$props5 = this.props,
          prefixCls = _this$props5.prefixCls,
          treeNodes = _this$props5.treeNodes,
          filteredTreeNodes = _this$props5.filteredTreeNodes,
          treeIcon = _this$props5.treeIcon,
          treeLine = _this$props5.treeLine,
          treeCheckable = _this$props5.treeCheckable,
          treeCheckStrictly = _this$props5.treeCheckStrictly,
          multiple = _this$props5.multiple,
          ariaId = _this$props5.ariaId,
          renderSearch = _this$props5.renderSearch,
          switcherIcon = _this$props5.switcherIcon,
          searchHalfCheckedKeys = _this$props5.searchHalfCheckedKeys;
      var _this$context$rcTreeS = this.context.rcTreeSelect,
          onPopupKeyDown = _this$context$rcTreeS.onPopupKeyDown,
          onTreeNodeSelect = _this$context$rcTreeS.onTreeNodeSelect,
          onTreeNodeCheck = _this$context$rcTreeS.onTreeNodeCheck;
      var loadData = this.getLoadData();
      var treeProps = {};

      if (treeCheckable) {
        treeProps.checkedKeys = keyList;
      } else {
        treeProps.selectedKeys = keyList;
      }

      var $notFound;
      var $treeNodes;

      if (filteredTreeNodes) {
        if (filteredTreeNodes.length) {
          treeProps.checkStrictly = true;
          $treeNodes = filteredTreeNodes; // Fill halfCheckedKeys

          if (treeCheckable && !treeCheckStrictly) {
            treeProps.checkedKeys = {
              checked: keyList,
              halfChecked: searchHalfCheckedKeys
            };
          }
        } else {
          $notFound = this.renderNotFound();
        }
      } else if (!treeNodes || !treeNodes.length) {
        $notFound = this.renderNotFound();
      } else {
        $treeNodes = treeNodes;
      }

      var $tree;

      if ($notFound) {
        $tree = $notFound;
      } else {
        $tree = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(rc_tree__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({
          ref: this.treeRef,
          prefixCls: "".concat(prefixCls, "-tree"),
          showIcon: treeIcon,
          showLine: treeLine,
          selectable: !treeCheckable,
          checkable: treeCheckable,
          checkStrictly: treeCheckStrictly,
          multiple: multiple,
          loadData: loadData,
          loadedKeys: loadedKeys,
          expandedKeys: expandedKeyList,
          filterTreeNode: this.filterTreeNode,
          onSelect: onTreeNodeSelect,
          onCheck: onTreeNodeCheck,
          onExpand: this.onTreeExpand,
          onLoad: this.onLoad,
          switcherIcon: switcherIcon
        }, treeProps), $treeNodes);
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        role: "listbox",
        id: ariaId,
        onKeyDown: onPopupKeyDown,
        tabIndex: -1
      }, renderSearch ? renderSearch() : null, $tree);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var _ref = prevState || {},
          _ref$prevProps = _ref.prevProps,
          prevProps = _ref$prevProps === void 0 ? {} : _ref$prevProps,
          loadedKeys = _ref.loadedKeys,
          expandedKeyList = _ref.expandedKeyList,
          cachedExpandedKeyList = _ref.cachedExpandedKeyList;

      var valueList = nextProps.valueList,
          valueEntities = nextProps.valueEntities,
          keyEntities = nextProps.keyEntities,
          treeExpandedKeys = nextProps.treeExpandedKeys,
          filteredTreeNodes = nextProps.filteredTreeNodes,
          upperSearchValue = nextProps.upperSearchValue;
      var newState = {
        prevProps: nextProps
      }; // Check value update

      if (valueList !== prevProps.valueList) {
        newState.keyList = valueList.map(function (_ref2) {
          var value = _ref2.value;
          return valueEntities[value];
        }).filter(function (entity) {
          return entity;
        }).map(function (_ref3) {
          var key = _ref3.key;
          return key;
        });
      } // Show all when tree is in filter mode


      if (!treeExpandedKeys && filteredTreeNodes && filteredTreeNodes.length && filteredTreeNodes !== prevProps.filteredTreeNodes) {
        newState.expandedKeyList = Object.keys(keyEntities);
      } // Cache `expandedKeyList` when filter set


      if (upperSearchValue && !prevProps.upperSearchValue) {
        newState.cachedExpandedKeyList = expandedKeyList;
      } else if (!upperSearchValue && prevProps.upperSearchValue && !treeExpandedKeys) {
        newState.expandedKeyList = cachedExpandedKeyList || [];
        newState.cachedExpandedKeyList = [];
      } // Use expandedKeys if provided


      if (prevProps.treeExpandedKeys !== treeExpandedKeys) {
        newState.expandedKeyList = treeExpandedKeys;
      } // Clean loadedKeys if key not exist in keyEntities anymore


      if (nextProps.loadData) {
        newState.loadedKeys = loadedKeys.filter(function (key) {
          return key in keyEntities;
        });
      }

      return newState;
    }
  }]);

  return BasePopup;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

_defineProperty(BasePopup, "propTypes", {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  upperSearchValue: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  valueList: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  searchHalfCheckedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  valueEntities: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  keyEntities: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  treeIcon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  treeLine: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  treeNodeFilterProp: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  treeCheckable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node]),
  treeCheckStrictly: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  treeDefaultExpandAll: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  treeDefaultExpandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  treeExpandedKeys: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.array,
  loadData: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  multiple: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  onTreeExpand: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  treeNodes: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  filteredTreeNodes: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  notFoundContent: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  ariaId: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  switcherIcon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func]),
  // HOC
  renderSearch: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  onTreeExpanded: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func
});

_defineProperty(BasePopup, "contextTypes", {
  rcTreeSelect: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.shape(_objectSpread({}, popupContextTypes))
});

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(BasePopup);
/* harmony default export */ __webpack_exports__["default"] = (BasePopup);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvQmFzZS9CYXNlUG9wdXAuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10cmVlLXNlbGVjdC9lcy9CYXNlL0Jhc2VQb3B1cC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBvd25LZXlzKG9iamVjdCwgZW51bWVyYWJsZU9ubHkpIHsgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhvYmplY3QpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc3ltYm9scyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMob2JqZWN0KTsgaWYgKGVudW1lcmFibGVPbmx5KSBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkgeyByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTsgfSk7IGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTsgfSByZXR1cm4ga2V5czsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0U3ByZWFkKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTsgaWYgKGkgJSAyKSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSksIHRydWUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBfZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHNvdXJjZVtrZXldKTsgfSk7IH0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMpIHsgT2JqZWN0LmRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycyhzb3VyY2UpKTsgfSBlbHNlIHsgb3duS2V5cyhPYmplY3Qoc291cmNlKSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSkgeyByZXR1cm4gY2FsbDsgfSByZXR1cm4gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKTsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb25cIik7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKTsgfVxuXG5mdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBfc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgby5fX3Byb3RvX18gPSBwOyByZXR1cm4gbzsgfTsgcmV0dXJuIF9zZXRQcm90b3R5cGVPZihvLCBwKTsgfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IFRyZWUgZnJvbSAncmMtdHJlZSc7XG5pbXBvcnQgeyBjcmVhdGVSZWYgfSBmcm9tICcuLi91dGlsJztcbmV4cG9ydCB2YXIgcG9wdXBDb250ZXh0VHlwZXMgPSB7XG4gIG9uUG9wdXBLZXlEb3duOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvblRyZWVOb2RlU2VsZWN0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvblRyZWVOb2RlQ2hlY2s6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWRcbn07XG5cbnZhciBCYXNlUG9wdXAgPVxuLyojX19QVVJFX18qL1xuZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKEJhc2VQb3B1cCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQmFzZVBvcHVwKHByb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEJhc2VQb3B1cCk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihCYXNlUG9wdXApLmNhbGwodGhpcykpO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcIm9uVHJlZUV4cGFuZFwiLCBmdW5jdGlvbiAoZXhwYW5kZWRLZXlMaXN0KSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0cmVlRXhwYW5kZWRLZXlzID0gX3RoaXMkcHJvcHMudHJlZUV4cGFuZGVkS2V5cyxcbiAgICAgICAgICBvblRyZWVFeHBhbmQgPSBfdGhpcyRwcm9wcy5vblRyZWVFeHBhbmQsXG4gICAgICAgICAgb25UcmVlRXhwYW5kZWQgPSBfdGhpcyRwcm9wcy5vblRyZWVFeHBhbmRlZDsgLy8gU2V0IHVuY29udHJvbGxlZCBzdGF0ZVxuXG4gICAgICBpZiAoIXRyZWVFeHBhbmRlZEtleXMpIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGV4cGFuZGVkS2V5TGlzdDogZXhwYW5kZWRLZXlMaXN0XG4gICAgICAgIH0sIG9uVHJlZUV4cGFuZGVkKTtcbiAgICAgIH1cblxuICAgICAgaWYgKG9uVHJlZUV4cGFuZCkge1xuICAgICAgICBvblRyZWVFeHBhbmQoZXhwYW5kZWRLZXlMaXN0KTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIF9kZWZpbmVQcm9wZXJ0eShfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSwgXCJvbkxvYWRcIiwgZnVuY3Rpb24gKGxvYWRlZEtleXMpIHtcbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbG9hZGVkS2V5czogbG9hZGVkS2V5c1xuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiZ2V0VHJlZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMudHJlZVJlZi5jdXJyZW50O1xuICAgIH0pO1xuXG4gICAgX2RlZmluZVByb3BlcnR5KF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpLCBcImdldExvYWREYXRhXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBsb2FkRGF0YSA9IF90aGlzJHByb3BzMi5sb2FkRGF0YSxcbiAgICAgICAgICB1cHBlclNlYXJjaFZhbHVlID0gX3RoaXMkcHJvcHMyLnVwcGVyU2VhcmNoVmFsdWU7XG4gICAgICBpZiAodXBwZXJTZWFyY2hWYWx1ZSkgcmV0dXJuIG51bGw7XG4gICAgICByZXR1cm4gbG9hZERhdGE7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwiZmlsdGVyVHJlZU5vZGVcIiwgZnVuY3Rpb24gKHRyZWVOb2RlKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdXBwZXJTZWFyY2hWYWx1ZSA9IF90aGlzJHByb3BzMy51cHBlclNlYXJjaFZhbHVlLFxuICAgICAgICAgIHRyZWVOb2RlRmlsdGVyUHJvcCA9IF90aGlzJHByb3BzMy50cmVlTm9kZUZpbHRlclByb3A7XG4gICAgICB2YXIgZmlsdGVyVmFsID0gdHJlZU5vZGUucHJvcHNbdHJlZU5vZGVGaWx0ZXJQcm9wXTtcblxuICAgICAgaWYgKHR5cGVvZiBmaWx0ZXJWYWwgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiB1cHBlclNlYXJjaFZhbHVlICYmIGZpbHRlclZhbC50b1VwcGVyQ2FzZSgpLmluZGV4T2YodXBwZXJTZWFyY2hWYWx1ZSkgIT09IC0xO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSk7XG5cbiAgICBfZGVmaW5lUHJvcGVydHkoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIFwicmVuZGVyTm90Rm91bmRcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNCA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzNC5wcmVmaXhDbHMsXG4gICAgICAgICAgbm90Rm91bmRDb250ZW50ID0gX3RoaXMkcHJvcHM0Lm5vdEZvdW5kQ29udGVudDtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1ub3QtZm91bmRcIilcbiAgICAgIH0sIG5vdEZvdW5kQ29udGVudCk7XG4gICAgfSk7XG5cbiAgICB2YXIgdHJlZURlZmF1bHRFeHBhbmRBbGwgPSBwcm9wcy50cmVlRGVmYXVsdEV4cGFuZEFsbCxcbiAgICAgICAgdHJlZURlZmF1bHRFeHBhbmRlZEtleXMgPSBwcm9wcy50cmVlRGVmYXVsdEV4cGFuZGVkS2V5cyxcbiAgICAgICAga2V5RW50aXRpZXMgPSBwcm9wcy5rZXlFbnRpdGllczsgLy8gVE9ETzogbWFrZSBgZXhwYW5kZWRLZXlMaXN0YCBjb250cm9sXG5cbiAgICB2YXIgX2V4cGFuZGVkS2V5TGlzdCA9IHRyZWVEZWZhdWx0RXhwYW5kZWRLZXlzO1xuXG4gICAgaWYgKHRyZWVEZWZhdWx0RXhwYW5kQWxsKSB7XG4gICAgICBfZXhwYW5kZWRLZXlMaXN0ID0gT2JqZWN0LmtleXMoa2V5RW50aXRpZXMpO1xuICAgIH1cblxuICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAga2V5TGlzdDogW10sXG4gICAgICBleHBhbmRlZEtleUxpc3Q6IF9leHBhbmRlZEtleUxpc3QsXG4gICAgICAvLyBDYWNoZSBgZXhwYW5kZWRLZXlMaXN0YCB3aGVuIHRyZWUgaXMgaW4gZmlsdGVyLiBUaGlzIGlzIHVzZWQgaW4gYGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wc2BcbiAgICAgIGNhY2hlZEV4cGFuZGVkS2V5TGlzdDogW10sXG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIHJlYWN0L25vLXVudXNlZC1zdGF0ZVxuICAgICAgbG9hZGVkS2V5czogW11cbiAgICB9O1xuICAgIF90aGlzLnRyZWVSZWYgPSBjcmVhdGVSZWYoKTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoQmFzZVBvcHVwLCBbe1xuICAgIGtleTogXCJyZW5kZXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzJHN0YXRlID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICBrZXlMaXN0ID0gX3RoaXMkc3RhdGUua2V5TGlzdCxcbiAgICAgICAgICBleHBhbmRlZEtleUxpc3QgPSBfdGhpcyRzdGF0ZS5leHBhbmRlZEtleUxpc3QsXG4gICAgICAgICAgbG9hZGVkS2V5cyA9IF90aGlzJHN0YXRlLmxvYWRlZEtleXM7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM1ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfdGhpcyRwcm9wczUucHJlZml4Q2xzLFxuICAgICAgICAgIHRyZWVOb2RlcyA9IF90aGlzJHByb3BzNS50cmVlTm9kZXMsXG4gICAgICAgICAgZmlsdGVyZWRUcmVlTm9kZXMgPSBfdGhpcyRwcm9wczUuZmlsdGVyZWRUcmVlTm9kZXMsXG4gICAgICAgICAgdHJlZUljb24gPSBfdGhpcyRwcm9wczUudHJlZUljb24sXG4gICAgICAgICAgdHJlZUxpbmUgPSBfdGhpcyRwcm9wczUudHJlZUxpbmUsXG4gICAgICAgICAgdHJlZUNoZWNrYWJsZSA9IF90aGlzJHByb3BzNS50cmVlQ2hlY2thYmxlLFxuICAgICAgICAgIHRyZWVDaGVja1N0cmljdGx5ID0gX3RoaXMkcHJvcHM1LnRyZWVDaGVja1N0cmljdGx5LFxuICAgICAgICAgIG11bHRpcGxlID0gX3RoaXMkcHJvcHM1Lm11bHRpcGxlLFxuICAgICAgICAgIGFyaWFJZCA9IF90aGlzJHByb3BzNS5hcmlhSWQsXG4gICAgICAgICAgcmVuZGVyU2VhcmNoID0gX3RoaXMkcHJvcHM1LnJlbmRlclNlYXJjaCxcbiAgICAgICAgICBzd2l0Y2hlckljb24gPSBfdGhpcyRwcm9wczUuc3dpdGNoZXJJY29uLFxuICAgICAgICAgIHNlYXJjaEhhbGZDaGVja2VkS2V5cyA9IF90aGlzJHByb3BzNS5zZWFyY2hIYWxmQ2hlY2tlZEtleXM7XG4gICAgICB2YXIgX3RoaXMkY29udGV4dCRyY1RyZWVTID0gdGhpcy5jb250ZXh0LnJjVHJlZVNlbGVjdCxcbiAgICAgICAgICBvblBvcHVwS2V5RG93biA9IF90aGlzJGNvbnRleHQkcmNUcmVlUy5vblBvcHVwS2V5RG93bixcbiAgICAgICAgICBvblRyZWVOb2RlU2VsZWN0ID0gX3RoaXMkY29udGV4dCRyY1RyZWVTLm9uVHJlZU5vZGVTZWxlY3QsXG4gICAgICAgICAgb25UcmVlTm9kZUNoZWNrID0gX3RoaXMkY29udGV4dCRyY1RyZWVTLm9uVHJlZU5vZGVDaGVjaztcbiAgICAgIHZhciBsb2FkRGF0YSA9IHRoaXMuZ2V0TG9hZERhdGEoKTtcbiAgICAgIHZhciB0cmVlUHJvcHMgPSB7fTtcblxuICAgICAgaWYgKHRyZWVDaGVja2FibGUpIHtcbiAgICAgICAgdHJlZVByb3BzLmNoZWNrZWRLZXlzID0ga2V5TGlzdDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRyZWVQcm9wcy5zZWxlY3RlZEtleXMgPSBrZXlMaXN0O1xuICAgICAgfVxuXG4gICAgICB2YXIgJG5vdEZvdW5kO1xuICAgICAgdmFyICR0cmVlTm9kZXM7XG5cbiAgICAgIGlmIChmaWx0ZXJlZFRyZWVOb2Rlcykge1xuICAgICAgICBpZiAoZmlsdGVyZWRUcmVlTm9kZXMubGVuZ3RoKSB7XG4gICAgICAgICAgdHJlZVByb3BzLmNoZWNrU3RyaWN0bHkgPSB0cnVlO1xuICAgICAgICAgICR0cmVlTm9kZXMgPSBmaWx0ZXJlZFRyZWVOb2RlczsgLy8gRmlsbCBoYWxmQ2hlY2tlZEtleXNcblxuICAgICAgICAgIGlmICh0cmVlQ2hlY2thYmxlICYmICF0cmVlQ2hlY2tTdHJpY3RseSkge1xuICAgICAgICAgICAgdHJlZVByb3BzLmNoZWNrZWRLZXlzID0ge1xuICAgICAgICAgICAgICBjaGVja2VkOiBrZXlMaXN0LFxuICAgICAgICAgICAgICBoYWxmQ2hlY2tlZDogc2VhcmNoSGFsZkNoZWNrZWRLZXlzXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAkbm90Rm91bmQgPSB0aGlzLnJlbmRlck5vdEZvdW5kKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoIXRyZWVOb2RlcyB8fCAhdHJlZU5vZGVzLmxlbmd0aCkge1xuICAgICAgICAkbm90Rm91bmQgPSB0aGlzLnJlbmRlck5vdEZvdW5kKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkdHJlZU5vZGVzID0gdHJlZU5vZGVzO1xuICAgICAgfVxuXG4gICAgICB2YXIgJHRyZWU7XG5cbiAgICAgIGlmICgkbm90Rm91bmQpIHtcbiAgICAgICAgJHRyZWUgPSAkbm90Rm91bmQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkdHJlZSA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoVHJlZSwgX2V4dGVuZHMoe1xuICAgICAgICAgIHJlZjogdGhpcy50cmVlUmVmLFxuICAgICAgICAgIHByZWZpeENsczogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10cmVlXCIpLFxuICAgICAgICAgIHNob3dJY29uOiB0cmVlSWNvbixcbiAgICAgICAgICBzaG93TGluZTogdHJlZUxpbmUsXG4gICAgICAgICAgc2VsZWN0YWJsZTogIXRyZWVDaGVja2FibGUsXG4gICAgICAgICAgY2hlY2thYmxlOiB0cmVlQ2hlY2thYmxlLFxuICAgICAgICAgIGNoZWNrU3RyaWN0bHk6IHRyZWVDaGVja1N0cmljdGx5LFxuICAgICAgICAgIG11bHRpcGxlOiBtdWx0aXBsZSxcbiAgICAgICAgICBsb2FkRGF0YTogbG9hZERhdGEsXG4gICAgICAgICAgbG9hZGVkS2V5czogbG9hZGVkS2V5cyxcbiAgICAgICAgICBleHBhbmRlZEtleXM6IGV4cGFuZGVkS2V5TGlzdCxcbiAgICAgICAgICBmaWx0ZXJUcmVlTm9kZTogdGhpcy5maWx0ZXJUcmVlTm9kZSxcbiAgICAgICAgICBvblNlbGVjdDogb25UcmVlTm9kZVNlbGVjdCxcbiAgICAgICAgICBvbkNoZWNrOiBvblRyZWVOb2RlQ2hlY2ssXG4gICAgICAgICAgb25FeHBhbmQ6IHRoaXMub25UcmVlRXhwYW5kLFxuICAgICAgICAgIG9uTG9hZDogdGhpcy5vbkxvYWQsXG4gICAgICAgICAgc3dpdGNoZXJJY29uOiBzd2l0Y2hlckljb25cbiAgICAgICAgfSwgdHJlZVByb3BzKSwgJHRyZWVOb2Rlcyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIHtcbiAgICAgICAgcm9sZTogXCJsaXN0Ym94XCIsXG4gICAgICAgIGlkOiBhcmlhSWQsXG4gICAgICAgIG9uS2V5RG93bjogb25Qb3B1cEtleURvd24sXG4gICAgICAgIHRhYkluZGV4OiAtMVxuICAgICAgfSwgcmVuZGVyU2VhcmNoID8gcmVuZGVyU2VhcmNoKCkgOiBudWxsLCAkdHJlZSk7XG4gICAgfVxuICB9XSwgW3tcbiAgICBrZXk6IFwiZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgdmFyIF9yZWYgPSBwcmV2U3RhdGUgfHwge30sXG4gICAgICAgICAgX3JlZiRwcmV2UHJvcHMgPSBfcmVmLnByZXZQcm9wcyxcbiAgICAgICAgICBwcmV2UHJvcHMgPSBfcmVmJHByZXZQcm9wcyA9PT0gdm9pZCAwID8ge30gOiBfcmVmJHByZXZQcm9wcyxcbiAgICAgICAgICBsb2FkZWRLZXlzID0gX3JlZi5sb2FkZWRLZXlzLFxuICAgICAgICAgIGV4cGFuZGVkS2V5TGlzdCA9IF9yZWYuZXhwYW5kZWRLZXlMaXN0LFxuICAgICAgICAgIGNhY2hlZEV4cGFuZGVkS2V5TGlzdCA9IF9yZWYuY2FjaGVkRXhwYW5kZWRLZXlMaXN0O1xuXG4gICAgICB2YXIgdmFsdWVMaXN0ID0gbmV4dFByb3BzLnZhbHVlTGlzdCxcbiAgICAgICAgICB2YWx1ZUVudGl0aWVzID0gbmV4dFByb3BzLnZhbHVlRW50aXRpZXMsXG4gICAgICAgICAga2V5RW50aXRpZXMgPSBuZXh0UHJvcHMua2V5RW50aXRpZXMsXG4gICAgICAgICAgdHJlZUV4cGFuZGVkS2V5cyA9IG5leHRQcm9wcy50cmVlRXhwYW5kZWRLZXlzLFxuICAgICAgICAgIGZpbHRlcmVkVHJlZU5vZGVzID0gbmV4dFByb3BzLmZpbHRlcmVkVHJlZU5vZGVzLFxuICAgICAgICAgIHVwcGVyU2VhcmNoVmFsdWUgPSBuZXh0UHJvcHMudXBwZXJTZWFyY2hWYWx1ZTtcbiAgICAgIHZhciBuZXdTdGF0ZSA9IHtcbiAgICAgICAgcHJldlByb3BzOiBuZXh0UHJvcHNcbiAgICAgIH07IC8vIENoZWNrIHZhbHVlIHVwZGF0ZVxuXG4gICAgICBpZiAodmFsdWVMaXN0ICE9PSBwcmV2UHJvcHMudmFsdWVMaXN0KSB7XG4gICAgICAgIG5ld1N0YXRlLmtleUxpc3QgPSB2YWx1ZUxpc3QubWFwKGZ1bmN0aW9uIChfcmVmMikge1xuICAgICAgICAgIHZhciB2YWx1ZSA9IF9yZWYyLnZhbHVlO1xuICAgICAgICAgIHJldHVybiB2YWx1ZUVudGl0aWVzW3ZhbHVlXTtcbiAgICAgICAgfSkuZmlsdGVyKGZ1bmN0aW9uIChlbnRpdHkpIHtcbiAgICAgICAgICByZXR1cm4gZW50aXR5O1xuICAgICAgICB9KS5tYXAoZnVuY3Rpb24gKF9yZWYzKSB7XG4gICAgICAgICAgdmFyIGtleSA9IF9yZWYzLmtleTtcbiAgICAgICAgICByZXR1cm4ga2V5O1xuICAgICAgICB9KTtcbiAgICAgIH0gLy8gU2hvdyBhbGwgd2hlbiB0cmVlIGlzIGluIGZpbHRlciBtb2RlXG5cblxuICAgICAgaWYgKCF0cmVlRXhwYW5kZWRLZXlzICYmIGZpbHRlcmVkVHJlZU5vZGVzICYmIGZpbHRlcmVkVHJlZU5vZGVzLmxlbmd0aCAmJiBmaWx0ZXJlZFRyZWVOb2RlcyAhPT0gcHJldlByb3BzLmZpbHRlcmVkVHJlZU5vZGVzKSB7XG4gICAgICAgIG5ld1N0YXRlLmV4cGFuZGVkS2V5TGlzdCA9IE9iamVjdC5rZXlzKGtleUVudGl0aWVzKTtcbiAgICAgIH0gLy8gQ2FjaGUgYGV4cGFuZGVkS2V5TGlzdGAgd2hlbiBmaWx0ZXIgc2V0XG5cblxuICAgICAgaWYgKHVwcGVyU2VhcmNoVmFsdWUgJiYgIXByZXZQcm9wcy51cHBlclNlYXJjaFZhbHVlKSB7XG4gICAgICAgIG5ld1N0YXRlLmNhY2hlZEV4cGFuZGVkS2V5TGlzdCA9IGV4cGFuZGVkS2V5TGlzdDtcbiAgICAgIH0gZWxzZSBpZiAoIXVwcGVyU2VhcmNoVmFsdWUgJiYgcHJldlByb3BzLnVwcGVyU2VhcmNoVmFsdWUgJiYgIXRyZWVFeHBhbmRlZEtleXMpIHtcbiAgICAgICAgbmV3U3RhdGUuZXhwYW5kZWRLZXlMaXN0ID0gY2FjaGVkRXhwYW5kZWRLZXlMaXN0IHx8IFtdO1xuICAgICAgICBuZXdTdGF0ZS5jYWNoZWRFeHBhbmRlZEtleUxpc3QgPSBbXTtcbiAgICAgIH0gLy8gVXNlIGV4cGFuZGVkS2V5cyBpZiBwcm92aWRlZFxuXG5cbiAgICAgIGlmIChwcmV2UHJvcHMudHJlZUV4cGFuZGVkS2V5cyAhPT0gdHJlZUV4cGFuZGVkS2V5cykge1xuICAgICAgICBuZXdTdGF0ZS5leHBhbmRlZEtleUxpc3QgPSB0cmVlRXhwYW5kZWRLZXlzO1xuICAgICAgfSAvLyBDbGVhbiBsb2FkZWRLZXlzIGlmIGtleSBub3QgZXhpc3QgaW4ga2V5RW50aXRpZXMgYW55bW9yZVxuXG5cbiAgICAgIGlmIChuZXh0UHJvcHMubG9hZERhdGEpIHtcbiAgICAgICAgbmV3U3RhdGUubG9hZGVkS2V5cyA9IGxvYWRlZEtleXMuZmlsdGVyKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICByZXR1cm4ga2V5IGluIGtleUVudGl0aWVzO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ld1N0YXRlO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBCYXNlUG9wdXA7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbl9kZWZpbmVQcm9wZXJ0eShCYXNlUG9wdXAsIFwicHJvcFR5cGVzXCIsIHtcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB1cHBlclNlYXJjaFZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB2YWx1ZUxpc3Q6IFByb3BUeXBlcy5hcnJheSxcbiAgc2VhcmNoSGFsZkNoZWNrZWRLZXlzOiBQcm9wVHlwZXMuYXJyYXksXG4gIHZhbHVlRW50aXRpZXM6IFByb3BUeXBlcy5vYmplY3QsXG4gIGtleUVudGl0aWVzOiBQcm9wVHlwZXMub2JqZWN0LFxuICB0cmVlSWNvbjogUHJvcFR5cGVzLmJvb2wsXG4gIHRyZWVMaW5lOiBQcm9wVHlwZXMuYm9vbCxcbiAgdHJlZU5vZGVGaWx0ZXJQcm9wOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0cmVlQ2hlY2thYmxlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuYm9vbCwgUHJvcFR5cGVzLm5vZGVdKSxcbiAgdHJlZUNoZWNrU3RyaWN0bHk6IFByb3BUeXBlcy5ib29sLFxuICB0cmVlRGVmYXVsdEV4cGFuZEFsbDogUHJvcFR5cGVzLmJvb2wsXG4gIHRyZWVEZWZhdWx0RXhwYW5kZWRLZXlzOiBQcm9wVHlwZXMuYXJyYXksXG4gIHRyZWVFeHBhbmRlZEtleXM6IFByb3BUeXBlcy5hcnJheSxcbiAgbG9hZERhdGE6IFByb3BUeXBlcy5mdW5jLFxuICBtdWx0aXBsZTogUHJvcFR5cGVzLmJvb2wsXG4gIG9uVHJlZUV4cGFuZDogUHJvcFR5cGVzLmZ1bmMsXG4gIHRyZWVOb2RlczogUHJvcFR5cGVzLm5vZGUsXG4gIGZpbHRlcmVkVHJlZU5vZGVzOiBQcm9wVHlwZXMubm9kZSxcbiAgbm90Rm91bmRDb250ZW50OiBQcm9wVHlwZXMubm9kZSxcbiAgYXJpYUlkOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBzd2l0Y2hlckljb246IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ub2RlLCBQcm9wVHlwZXMuZnVuY10pLFxuICAvLyBIT0NcbiAgcmVuZGVyU2VhcmNoOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25UcmVlRXhwYW5kZWQ6IFByb3BUeXBlcy5mdW5jXG59KTtcblxuX2RlZmluZVByb3BlcnR5KEJhc2VQb3B1cCwgXCJjb250ZXh0VHlwZXNcIiwge1xuICByY1RyZWVTZWxlY3Q6IFByb3BUeXBlcy5zaGFwZShfb2JqZWN0U3ByZWFkKHt9LCBwb3B1cENvbnRleHRUeXBlcykpXG59KTtcblxucG9seWZpbGwoQmFzZVBvcHVwKTtcbmV4cG9ydCBkZWZhdWx0IEJhc2VQb3B1cDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqQkE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBeEZBO0FBMEZBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMURBO0FBQ0E7QUE0REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpCQTtBQUNBO0FBMkJBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/es/Base/BasePopup.js
