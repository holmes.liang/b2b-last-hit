__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @common/index */ "./src/common/index.tsx");
// for antd


var isRightIdNo = function isRightIdNo(idType, idNo, currencyCode, callback) {
  var invalidMsg = _common_index__WEBPACK_IMPORTED_MODULE_0__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").my("ID ကိုနံပါတ်လိုအပ်သည်").getMessage();

  if (idType === _common_index__WEBPACK_IMPORTED_MODULE_0__["Consts"].ID_TYPE_IC && !_common_index__WEBPACK_IMPORTED_MODULE_0__["Rules"].isValidThaiIdNo(idNo) && currencyCode === "THB") {
    callback(invalidMsg);
  } else if (idType === "NRIC" && !_common_index__WEBPACK_IMPORTED_MODULE_0__["Rules"].isNricValid(idNo)) {
    callback(invalidMsg);
  } else if (idType === "FIN" && !_common_index__WEBPACK_IMPORTED_MODULE_0__["Rules"].isFinValid(idNo)) {
    callback(invalidMsg);
  } else {
    callback();
  }
};

var validators = {
  isRightIdNo: isRightIdNo
};
/* harmony default export */ __webpack_exports__["default"] = (validators);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3ZhbGlkYXRvci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21tb24vdmFsaWRhdG9yLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBmb3IgYW50ZFxuXG5pbXBvcnQgeyBDb25zdHMsIExhbmd1YWdlLCBSdWxlcyB9IGZyb20gXCJAY29tbW9uL2luZGV4XCI7XG5cbmNvbnN0IGlzUmlnaHRJZE5vID0gKGlkVHlwZTogc3RyaW5nLCBpZE5vOiBhbnksIGN1cnJlbmN5Q29kZTogYW55LCBjYWxsYmFjazogYW55KSA9PiB7XG4gIGNvbnN0IGludmFsaWRNc2c6IHN0cmluZyA9IExhbmd1YWdlLmVuKFwiSUQgTm8uIGlzIGludmFsaWRcIilcbiAgICAudGhhaShcIuC4q+C4oeC4suC4ouC5gOC4peC4guC4o+C4q+C4seC4quC5hOC4oeC5iOC4luC4ueC4geC4leC5ieC4reC4h1wiKVxuICAgIC5teShcIklEIOGAgOGAreGAr+GAlOGAtuGAleGAq+GAkOGAuuGAnOGAreGAr+GAoeGAleGAuuGAnuGAiuGAulwiKVxuICAgIC5nZXRNZXNzYWdlKCk7XG4gIGlmICgoaWRUeXBlID09PSBDb25zdHMuSURfVFlQRV9JQyAmJiAhUnVsZXMuaXNWYWxpZFRoYWlJZE5vKGlkTm8pICYmIGN1cnJlbmN5Q29kZSA9PT0gXCJUSEJcIikpIHtcbiAgICBjYWxsYmFjayhpbnZhbGlkTXNnKTtcbiAgfSBlbHNlIGlmICgoaWRUeXBlID09PSBcIk5SSUNcIiAmJiAhUnVsZXMuaXNOcmljVmFsaWQoaWRObykpKSB7XG4gICAgY2FsbGJhY2soaW52YWxpZE1zZyk7XG4gIH0gZWxzZSBpZiAoKGlkVHlwZSA9PT0gXCJGSU5cIiAmJiAhUnVsZXMuaXNGaW5WYWxpZChpZE5vKSkpIHtcbiAgICBjYWxsYmFjayhpbnZhbGlkTXNnKTtcbiAgfSBlbHNlIHtcbiAgICBjYWxsYmFjaygpO1xuICB9XG59O1xuXG5cbmNvbnN0IHZhbGlkYXRvcnMgPSB7XG4gIGlzUmlnaHRJZE5vLFxufTtcbmV4cG9ydCBkZWZhdWx0IHZhbGlkYXRvcnM7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/validator.tsx
