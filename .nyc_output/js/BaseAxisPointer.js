/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var clazzUtil = __webpack_require__(/*! ../../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var axisPointerModelHelper = __webpack_require__(/*! ./modelHelper */ "./node_modules/echarts/lib/component/axisPointer/modelHelper.js");

var eventTool = __webpack_require__(/*! zrender/lib/core/event */ "./node_modules/zrender/lib/core/event.js");

var throttleUtil = __webpack_require__(/*! ../../util/throttle */ "./node_modules/echarts/lib/util/throttle.js");

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = makeInner();
var clone = zrUtil.clone;
var bind = zrUtil.bind;
/**
 * Base axis pointer class in 2D.
 * Implemenents {module:echarts/component/axis/IAxisPointer}.
 */

function BaseAxisPointer() {}

BaseAxisPointer.prototype = {
  /**
   * @private
   */
  _group: null,

  /**
   * @private
   */
  _lastGraphicKey: null,

  /**
   * @private
   */
  _handle: null,

  /**
   * @private
   */
  _dragging: false,

  /**
   * @private
   */
  _lastValue: null,

  /**
   * @private
   */
  _lastStatus: null,

  /**
   * @private
   */
  _payloadInfo: null,

  /**
   * In px, arbitrary value. Do not set too small,
   * no animation is ok for most cases.
   * @protected
   */
  animationThreshold: 15,

  /**
   * @implement
   */
  render: function render(axisModel, axisPointerModel, api, forceRender) {
    var value = axisPointerModel.get('value');
    var status = axisPointerModel.get('status'); // Bind them to `this`, not in closure, otherwise they will not
    // be replaced when user calling setOption in not merge mode.

    this._axisModel = axisModel;
    this._axisPointerModel = axisPointerModel;
    this._api = api; // Optimize: `render` will be called repeatly during mouse move.
    // So it is power consuming if performing `render` each time,
    // especially on mobile device.

    if (!forceRender && this._lastValue === value && this._lastStatus === status) {
      return;
    }

    this._lastValue = value;
    this._lastStatus = status;
    var group = this._group;
    var handle = this._handle;

    if (!status || status === 'hide') {
      // Do not clear here, for animation better.
      group && group.hide();
      handle && handle.hide();
      return;
    }

    group && group.show();
    handle && handle.show(); // Otherwise status is 'show'

    var elOption = {};
    this.makeElOption(elOption, value, axisModel, axisPointerModel, api); // Enable change axis pointer type.

    var graphicKey = elOption.graphicKey;

    if (graphicKey !== this._lastGraphicKey) {
      this.clear(api);
    }

    this._lastGraphicKey = graphicKey;
    var moveAnimation = this._moveAnimation = this.determineAnimation(axisModel, axisPointerModel);

    if (!group) {
      group = this._group = new graphic.Group();
      this.createPointerEl(group, elOption, axisModel, axisPointerModel);
      this.createLabelEl(group, elOption, axisModel, axisPointerModel);
      api.getZr().add(group);
    } else {
      var doUpdateProps = zrUtil.curry(updateProps, axisPointerModel, moveAnimation);
      this.updatePointerEl(group, elOption, doUpdateProps, axisPointerModel);
      this.updateLabelEl(group, elOption, doUpdateProps, axisPointerModel);
    }

    updateMandatoryProps(group, axisPointerModel, true);

    this._renderHandle(value);
  },

  /**
   * @implement
   */
  remove: function remove(api) {
    this.clear(api);
  },

  /**
   * @implement
   */
  dispose: function dispose(api) {
    this.clear(api);
  },

  /**
   * @protected
   */
  determineAnimation: function determineAnimation(axisModel, axisPointerModel) {
    var animation = axisPointerModel.get('animation');
    var axis = axisModel.axis;
    var isCategoryAxis = axis.type === 'category';
    var useSnap = axisPointerModel.get('snap'); // Value axis without snap always do not snap.

    if (!useSnap && !isCategoryAxis) {
      return false;
    }

    if (animation === 'auto' || animation == null) {
      var animationThreshold = this.animationThreshold;

      if (isCategoryAxis && axis.getBandWidth() > animationThreshold) {
        return true;
      } // It is important to auto animation when snap used. Consider if there is
      // a dataZoom, animation will be disabled when too many points exist, while
      // it will be enabled for better visual effect when little points exist.


      if (useSnap) {
        var seriesDataCount = axisPointerModelHelper.getAxisInfo(axisModel).seriesDataCount;
        var axisExtent = axis.getExtent(); // Approximate band width

        return Math.abs(axisExtent[0] - axisExtent[1]) / seriesDataCount > animationThreshold;
      }

      return false;
    }

    return animation === true;
  },

  /**
   * add {pointer, label, graphicKey} to elOption
   * @protected
   */
  makeElOption: function makeElOption(elOption, value, axisModel, axisPointerModel, api) {// Shoule be implemenented by sub-class.
  },

  /**
   * @protected
   */
  createPointerEl: function createPointerEl(group, elOption, axisModel, axisPointerModel) {
    var pointerOption = elOption.pointer;

    if (pointerOption) {
      var pointerEl = inner(group).pointerEl = new graphic[pointerOption.type](clone(elOption.pointer));
      group.add(pointerEl);
    }
  },

  /**
   * @protected
   */
  createLabelEl: function createLabelEl(group, elOption, axisModel, axisPointerModel) {
    if (elOption.label) {
      var labelEl = inner(group).labelEl = new graphic.Rect(clone(elOption.label));
      group.add(labelEl);
      updateLabelShowHide(labelEl, axisPointerModel);
    }
  },

  /**
   * @protected
   */
  updatePointerEl: function updatePointerEl(group, elOption, updateProps) {
    var pointerEl = inner(group).pointerEl;

    if (pointerEl) {
      pointerEl.setStyle(elOption.pointer.style);
      updateProps(pointerEl, {
        shape: elOption.pointer.shape
      });
    }
  },

  /**
   * @protected
   */
  updateLabelEl: function updateLabelEl(group, elOption, updateProps, axisPointerModel) {
    var labelEl = inner(group).labelEl;

    if (labelEl) {
      labelEl.setStyle(elOption.label.style);
      updateProps(labelEl, {
        // Consider text length change in vertical axis, animation should
        // be used on shape, otherwise the effect will be weird.
        shape: elOption.label.shape,
        position: elOption.label.position
      });
      updateLabelShowHide(labelEl, axisPointerModel);
    }
  },

  /**
   * @private
   */
  _renderHandle: function _renderHandle(value) {
    if (this._dragging || !this.updateHandleTransform) {
      return;
    }

    var axisPointerModel = this._axisPointerModel;

    var zr = this._api.getZr();

    var handle = this._handle;
    var handleModel = axisPointerModel.getModel('handle');
    var status = axisPointerModel.get('status');

    if (!handleModel.get('show') || !status || status === 'hide') {
      handle && zr.remove(handle);
      this._handle = null;
      return;
    }

    var isInit;

    if (!this._handle) {
      isInit = true;
      handle = this._handle = graphic.createIcon(handleModel.get('icon'), {
        cursor: 'move',
        draggable: true,
        onmousemove: function onmousemove(e) {
          // Fot mobile devicem, prevent screen slider on the button.
          eventTool.stop(e.event);
        },
        onmousedown: bind(this._onHandleDragMove, this, 0, 0),
        drift: bind(this._onHandleDragMove, this),
        ondragend: bind(this._onHandleDragEnd, this)
      });
      zr.add(handle);
    }

    updateMandatoryProps(handle, axisPointerModel, false); // update style

    var includeStyles = ['color', 'borderColor', 'borderWidth', 'opacity', 'shadowColor', 'shadowBlur', 'shadowOffsetX', 'shadowOffsetY'];
    handle.setStyle(handleModel.getItemStyle(null, includeStyles)); // update position

    var handleSize = handleModel.get('size');

    if (!zrUtil.isArray(handleSize)) {
      handleSize = [handleSize, handleSize];
    }

    handle.attr('scale', [handleSize[0] / 2, handleSize[1] / 2]);
    throttleUtil.createOrUpdate(this, '_doDispatchAxisPointer', handleModel.get('throttle') || 0, 'fixRate');

    this._moveHandleToValue(value, isInit);
  },

  /**
   * @private
   */
  _moveHandleToValue: function _moveHandleToValue(value, isInit) {
    updateProps(this._axisPointerModel, !isInit && this._moveAnimation, this._handle, getHandleTransProps(this.getHandleTransform(value, this._axisModel, this._axisPointerModel)));
  },

  /**
   * @private
   */
  _onHandleDragMove: function _onHandleDragMove(dx, dy) {
    var handle = this._handle;

    if (!handle) {
      return;
    }

    this._dragging = true; // Persistent for throttle.

    var trans = this.updateHandleTransform(getHandleTransProps(handle), [dx, dy], this._axisModel, this._axisPointerModel);
    this._payloadInfo = trans;
    handle.stopAnimation();
    handle.attr(getHandleTransProps(trans));
    inner(handle).lastProp = null;

    this._doDispatchAxisPointer();
  },

  /**
   * Throttled method.
   * @private
   */
  _doDispatchAxisPointer: function _doDispatchAxisPointer() {
    var handle = this._handle;

    if (!handle) {
      return;
    }

    var payloadInfo = this._payloadInfo;
    var axisModel = this._axisModel;

    this._api.dispatchAction({
      type: 'updateAxisPointer',
      x: payloadInfo.cursorPoint[0],
      y: payloadInfo.cursorPoint[1],
      tooltipOption: payloadInfo.tooltipOption,
      axesInfo: [{
        axisDim: axisModel.axis.dim,
        axisIndex: axisModel.componentIndex
      }]
    });
  },

  /**
   * @private
   */
  _onHandleDragEnd: function _onHandleDragEnd(moveAnimation) {
    this._dragging = false;
    var handle = this._handle;

    if (!handle) {
      return;
    }

    var value = this._axisPointerModel.get('value'); // Consider snap or categroy axis, handle may be not consistent with
    // axisPointer. So move handle to align the exact value position when
    // drag ended.


    this._moveHandleToValue(value); // For the effect: tooltip will be shown when finger holding on handle
    // button, and will be hidden after finger left handle button.


    this._api.dispatchAction({
      type: 'hideTip'
    });
  },

  /**
   * Should be implemenented by sub-class if support `handle`.
   * @protected
   * @param {number} value
   * @param {module:echarts/model/Model} axisModel
   * @param {module:echarts/model/Model} axisPointerModel
   * @return {Object} {position: [x, y], rotation: 0}
   */
  getHandleTransform: null,

  /**
   * * Should be implemenented by sub-class if support `handle`.
   * @protected
   * @param {Object} transform {position, rotation}
   * @param {Array.<number>} delta [dx, dy]
   * @param {module:echarts/model/Model} axisModel
   * @param {module:echarts/model/Model} axisPointerModel
   * @return {Object} {position: [x, y], rotation: 0, cursorPoint: [x, y]}
   */
  updateHandleTransform: null,

  /**
   * @private
   */
  clear: function clear(api) {
    this._lastValue = null;
    this._lastStatus = null;
    var zr = api.getZr();
    var group = this._group;
    var handle = this._handle;

    if (zr && group) {
      this._lastGraphicKey = null;
      group && zr.remove(group);
      handle && zr.remove(handle);
      this._group = null;
      this._handle = null;
      this._payloadInfo = null;
    }
  },

  /**
   * @protected
   */
  doClear: function doClear() {// Implemented by sub-class if necessary.
  },

  /**
   * @protected
   * @param {Array.<number>} xy
   * @param {Array.<number>} wh
   * @param {number} [xDimIndex=0] or 1
   */
  buildLabel: function buildLabel(xy, wh, xDimIndex) {
    xDimIndex = xDimIndex || 0;
    return {
      x: xy[xDimIndex],
      y: xy[1 - xDimIndex],
      width: wh[xDimIndex],
      height: wh[1 - xDimIndex]
    };
  }
};
BaseAxisPointer.prototype.constructor = BaseAxisPointer;

function updateProps(animationModel, moveAnimation, el, props) {
  // Animation optimize.
  if (!propsEqual(inner(el).lastProp, props)) {
    inner(el).lastProp = props;
    moveAnimation ? graphic.updateProps(el, props, animationModel) : (el.stopAnimation(), el.attr(props));
  }
}

function propsEqual(lastProps, newProps) {
  if (zrUtil.isObject(lastProps) && zrUtil.isObject(newProps)) {
    var equals = true;
    zrUtil.each(newProps, function (item, key) {
      equals = equals && propsEqual(lastProps[key], item);
    });
    return !!equals;
  } else {
    return lastProps === newProps;
  }
}

function updateLabelShowHide(labelEl, axisPointerModel) {
  labelEl[axisPointerModel.get('label.show') ? 'show' : 'hide']();
}

function getHandleTransProps(trans) {
  return {
    position: trans.position.slice(),
    rotation: trans.rotation || 0
  };
}

function updateMandatoryProps(group, axisPointerModel, silent) {
  var z = axisPointerModel.get('z');
  var zlevel = axisPointerModel.get('zlevel');
  group && group.traverse(function (el) {
    if (el.type !== 'group') {
      z != null && (el.z = z);
      zlevel != null && (el.zlevel = zlevel);
      el.silent = silent;
    }
  });
}

clazzUtil.enableClassExtend(BaseAxisPointer);
var _default = BaseAxisPointer;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL0Jhc2VBeGlzUG9pbnRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NvbXBvbmVudC9heGlzUG9pbnRlci9CYXNlQXhpc1BvaW50ZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgY2xhenpVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvY2xhenpcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIGF4aXNQb2ludGVyTW9kZWxIZWxwZXIgPSByZXF1aXJlKFwiLi9tb2RlbEhlbHBlclwiKTtcblxudmFyIGV2ZW50VG9vbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL2V2ZW50XCIpO1xuXG52YXIgdGhyb3R0bGVVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvdGhyb3R0bGVcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIG1ha2VJbm5lciA9IF9tb2RlbC5tYWtlSW5uZXI7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBpbm5lciA9IG1ha2VJbm5lcigpO1xudmFyIGNsb25lID0genJVdGlsLmNsb25lO1xudmFyIGJpbmQgPSB6clV0aWwuYmluZDtcbi8qKlxuICogQmFzZSBheGlzIHBvaW50ZXIgY2xhc3MgaW4gMkQuXG4gKiBJbXBsZW1lbmVudHMge21vZHVsZTplY2hhcnRzL2NvbXBvbmVudC9heGlzL0lBeGlzUG9pbnRlcn0uXG4gKi9cblxuZnVuY3Rpb24gQmFzZUF4aXNQb2ludGVyKCkge31cblxuQmFzZUF4aXNQb2ludGVyLnByb3RvdHlwZSA9IHtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfZ3JvdXA6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfbGFzdEdyYXBoaWNLZXk6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfaGFuZGxlOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX2RyYWdnaW5nOiBmYWxzZSxcblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9sYXN0VmFsdWU6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfbGFzdFN0YXR1czogbnVsbCxcblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9wYXlsb2FkSW5mbzogbnVsbCxcblxuICAvKipcbiAgICogSW4gcHgsIGFyYml0cmFyeSB2YWx1ZS4gRG8gbm90IHNldCB0b28gc21hbGwsXG4gICAqIG5vIGFuaW1hdGlvbiBpcyBvayBmb3IgbW9zdCBjYXNlcy5cbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgYW5pbWF0aW9uVGhyZXNob2xkOiAxNSxcblxuICAvKipcbiAgICogQGltcGxlbWVudFxuICAgKi9cbiAgcmVuZGVyOiBmdW5jdGlvbiAoYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsLCBhcGksIGZvcmNlUmVuZGVyKSB7XG4gICAgdmFyIHZhbHVlID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3ZhbHVlJyk7XG4gICAgdmFyIHN0YXR1cyA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdzdGF0dXMnKTsgLy8gQmluZCB0aGVtIHRvIGB0aGlzYCwgbm90IGluIGNsb3N1cmUsIG90aGVyd2lzZSB0aGV5IHdpbGwgbm90XG4gICAgLy8gYmUgcmVwbGFjZWQgd2hlbiB1c2VyIGNhbGxpbmcgc2V0T3B0aW9uIGluIG5vdCBtZXJnZSBtb2RlLlxuXG4gICAgdGhpcy5fYXhpc01vZGVsID0gYXhpc01vZGVsO1xuICAgIHRoaXMuX2F4aXNQb2ludGVyTW9kZWwgPSBheGlzUG9pbnRlck1vZGVsO1xuICAgIHRoaXMuX2FwaSA9IGFwaTsgLy8gT3B0aW1pemU6IGByZW5kZXJgIHdpbGwgYmUgY2FsbGVkIHJlcGVhdGx5IGR1cmluZyBtb3VzZSBtb3ZlLlxuICAgIC8vIFNvIGl0IGlzIHBvd2VyIGNvbnN1bWluZyBpZiBwZXJmb3JtaW5nIGByZW5kZXJgIGVhY2ggdGltZSxcbiAgICAvLyBlc3BlY2lhbGx5IG9uIG1vYmlsZSBkZXZpY2UuXG5cbiAgICBpZiAoIWZvcmNlUmVuZGVyICYmIHRoaXMuX2xhc3RWYWx1ZSA9PT0gdmFsdWUgJiYgdGhpcy5fbGFzdFN0YXR1cyA9PT0gc3RhdHVzKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5fbGFzdFZhbHVlID0gdmFsdWU7XG4gICAgdGhpcy5fbGFzdFN0YXR1cyA9IHN0YXR1cztcbiAgICB2YXIgZ3JvdXAgPSB0aGlzLl9ncm91cDtcbiAgICB2YXIgaGFuZGxlID0gdGhpcy5faGFuZGxlO1xuXG4gICAgaWYgKCFzdGF0dXMgfHwgc3RhdHVzID09PSAnaGlkZScpIHtcbiAgICAgIC8vIERvIG5vdCBjbGVhciBoZXJlLCBmb3IgYW5pbWF0aW9uIGJldHRlci5cbiAgICAgIGdyb3VwICYmIGdyb3VwLmhpZGUoKTtcbiAgICAgIGhhbmRsZSAmJiBoYW5kbGUuaGlkZSgpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGdyb3VwICYmIGdyb3VwLnNob3coKTtcbiAgICBoYW5kbGUgJiYgaGFuZGxlLnNob3coKTsgLy8gT3RoZXJ3aXNlIHN0YXR1cyBpcyAnc2hvdydcblxuICAgIHZhciBlbE9wdGlvbiA9IHt9O1xuICAgIHRoaXMubWFrZUVsT3B0aW9uKGVsT3B0aW9uLCB2YWx1ZSwgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsLCBhcGkpOyAvLyBFbmFibGUgY2hhbmdlIGF4aXMgcG9pbnRlciB0eXBlLlxuXG4gICAgdmFyIGdyYXBoaWNLZXkgPSBlbE9wdGlvbi5ncmFwaGljS2V5O1xuXG4gICAgaWYgKGdyYXBoaWNLZXkgIT09IHRoaXMuX2xhc3RHcmFwaGljS2V5KSB7XG4gICAgICB0aGlzLmNsZWFyKGFwaSk7XG4gICAgfVxuXG4gICAgdGhpcy5fbGFzdEdyYXBoaWNLZXkgPSBncmFwaGljS2V5O1xuICAgIHZhciBtb3ZlQW5pbWF0aW9uID0gdGhpcy5fbW92ZUFuaW1hdGlvbiA9IHRoaXMuZGV0ZXJtaW5lQW5pbWF0aW9uKGF4aXNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbCk7XG5cbiAgICBpZiAoIWdyb3VwKSB7XG4gICAgICBncm91cCA9IHRoaXMuX2dyb3VwID0gbmV3IGdyYXBoaWMuR3JvdXAoKTtcbiAgICAgIHRoaXMuY3JlYXRlUG9pbnRlckVsKGdyb3VwLCBlbE9wdGlvbiwgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsKTtcbiAgICAgIHRoaXMuY3JlYXRlTGFiZWxFbChncm91cCwgZWxPcHRpb24sIGF4aXNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbCk7XG4gICAgICBhcGkuZ2V0WnIoKS5hZGQoZ3JvdXApO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgZG9VcGRhdGVQcm9wcyA9IHpyVXRpbC5jdXJyeSh1cGRhdGVQcm9wcywgYXhpc1BvaW50ZXJNb2RlbCwgbW92ZUFuaW1hdGlvbik7XG4gICAgICB0aGlzLnVwZGF0ZVBvaW50ZXJFbChncm91cCwgZWxPcHRpb24sIGRvVXBkYXRlUHJvcHMsIGF4aXNQb2ludGVyTW9kZWwpO1xuICAgICAgdGhpcy51cGRhdGVMYWJlbEVsKGdyb3VwLCBlbE9wdGlvbiwgZG9VcGRhdGVQcm9wcywgYXhpc1BvaW50ZXJNb2RlbCk7XG4gICAgfVxuXG4gICAgdXBkYXRlTWFuZGF0b3J5UHJvcHMoZ3JvdXAsIGF4aXNQb2ludGVyTW9kZWwsIHRydWUpO1xuXG4gICAgdGhpcy5fcmVuZGVySGFuZGxlKHZhbHVlKTtcbiAgfSxcblxuICAvKipcbiAgICogQGltcGxlbWVudFxuICAgKi9cbiAgcmVtb3ZlOiBmdW5jdGlvbiAoYXBpKSB7XG4gICAgdGhpcy5jbGVhcihhcGkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAaW1wbGVtZW50XG4gICAqL1xuICBkaXNwb3NlOiBmdW5jdGlvbiAoYXBpKSB7XG4gICAgdGhpcy5jbGVhcihhcGkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBkZXRlcm1pbmVBbmltYXRpb246IGZ1bmN0aW9uIChheGlzTW9kZWwsIGF4aXNQb2ludGVyTW9kZWwpIHtcbiAgICB2YXIgYW5pbWF0aW9uID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ2FuaW1hdGlvbicpO1xuICAgIHZhciBheGlzID0gYXhpc01vZGVsLmF4aXM7XG4gICAgdmFyIGlzQ2F0ZWdvcnlBeGlzID0gYXhpcy50eXBlID09PSAnY2F0ZWdvcnknO1xuICAgIHZhciB1c2VTbmFwID0gYXhpc1BvaW50ZXJNb2RlbC5nZXQoJ3NuYXAnKTsgLy8gVmFsdWUgYXhpcyB3aXRob3V0IHNuYXAgYWx3YXlzIGRvIG5vdCBzbmFwLlxuXG4gICAgaWYgKCF1c2VTbmFwICYmICFpc0NhdGVnb3J5QXhpcykge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChhbmltYXRpb24gPT09ICdhdXRvJyB8fCBhbmltYXRpb24gPT0gbnVsbCkge1xuICAgICAgdmFyIGFuaW1hdGlvblRocmVzaG9sZCA9IHRoaXMuYW5pbWF0aW9uVGhyZXNob2xkO1xuXG4gICAgICBpZiAoaXNDYXRlZ29yeUF4aXMgJiYgYXhpcy5nZXRCYW5kV2lkdGgoKSA+IGFuaW1hdGlvblRocmVzaG9sZCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0gLy8gSXQgaXMgaW1wb3J0YW50IHRvIGF1dG8gYW5pbWF0aW9uIHdoZW4gc25hcCB1c2VkLiBDb25zaWRlciBpZiB0aGVyZSBpc1xuICAgICAgLy8gYSBkYXRhWm9vbSwgYW5pbWF0aW9uIHdpbGwgYmUgZGlzYWJsZWQgd2hlbiB0b28gbWFueSBwb2ludHMgZXhpc3QsIHdoaWxlXG4gICAgICAvLyBpdCB3aWxsIGJlIGVuYWJsZWQgZm9yIGJldHRlciB2aXN1YWwgZWZmZWN0IHdoZW4gbGl0dGxlIHBvaW50cyBleGlzdC5cblxuXG4gICAgICBpZiAodXNlU25hcCkge1xuICAgICAgICB2YXIgc2VyaWVzRGF0YUNvdW50ID0gYXhpc1BvaW50ZXJNb2RlbEhlbHBlci5nZXRBeGlzSW5mbyhheGlzTW9kZWwpLnNlcmllc0RhdGFDb3VudDtcbiAgICAgICAgdmFyIGF4aXNFeHRlbnQgPSBheGlzLmdldEV4dGVudCgpOyAvLyBBcHByb3hpbWF0ZSBiYW5kIHdpZHRoXG5cbiAgICAgICAgcmV0dXJuIE1hdGguYWJzKGF4aXNFeHRlbnRbMF0gLSBheGlzRXh0ZW50WzFdKSAvIHNlcmllc0RhdGFDb3VudCA+IGFuaW1hdGlvblRocmVzaG9sZDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHJldHVybiBhbmltYXRpb24gPT09IHRydWU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIGFkZCB7cG9pbnRlciwgbGFiZWwsIGdyYXBoaWNLZXl9IHRvIGVsT3B0aW9uXG4gICAqIEBwcm90ZWN0ZWRcbiAgICovXG4gIG1ha2VFbE9wdGlvbjogZnVuY3Rpb24gKGVsT3B0aW9uLCB2YWx1ZSwgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsLCBhcGkpIHsvLyBTaG91bGUgYmUgaW1wbGVtZW5lbnRlZCBieSBzdWItY2xhc3MuXG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcm90ZWN0ZWRcbiAgICovXG4gIGNyZWF0ZVBvaW50ZXJFbDogZnVuY3Rpb24gKGdyb3VwLCBlbE9wdGlvbiwgYXhpc01vZGVsLCBheGlzUG9pbnRlck1vZGVsKSB7XG4gICAgdmFyIHBvaW50ZXJPcHRpb24gPSBlbE9wdGlvbi5wb2ludGVyO1xuXG4gICAgaWYgKHBvaW50ZXJPcHRpb24pIHtcbiAgICAgIHZhciBwb2ludGVyRWwgPSBpbm5lcihncm91cCkucG9pbnRlckVsID0gbmV3IGdyYXBoaWNbcG9pbnRlck9wdGlvbi50eXBlXShjbG9uZShlbE9wdGlvbi5wb2ludGVyKSk7XG4gICAgICBncm91cC5hZGQocG9pbnRlckVsKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcm90ZWN0ZWRcbiAgICovXG4gIGNyZWF0ZUxhYmVsRWw6IGZ1bmN0aW9uIChncm91cCwgZWxPcHRpb24sIGF4aXNNb2RlbCwgYXhpc1BvaW50ZXJNb2RlbCkge1xuICAgIGlmIChlbE9wdGlvbi5sYWJlbCkge1xuICAgICAgdmFyIGxhYmVsRWwgPSBpbm5lcihncm91cCkubGFiZWxFbCA9IG5ldyBncmFwaGljLlJlY3QoY2xvbmUoZWxPcHRpb24ubGFiZWwpKTtcbiAgICAgIGdyb3VwLmFkZChsYWJlbEVsKTtcbiAgICAgIHVwZGF0ZUxhYmVsU2hvd0hpZGUobGFiZWxFbCwgYXhpc1BvaW50ZXJNb2RlbCk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICB1cGRhdGVQb2ludGVyRWw6IGZ1bmN0aW9uIChncm91cCwgZWxPcHRpb24sIHVwZGF0ZVByb3BzKSB7XG4gICAgdmFyIHBvaW50ZXJFbCA9IGlubmVyKGdyb3VwKS5wb2ludGVyRWw7XG5cbiAgICBpZiAocG9pbnRlckVsKSB7XG4gICAgICBwb2ludGVyRWwuc2V0U3R5bGUoZWxPcHRpb24ucG9pbnRlci5zdHlsZSk7XG4gICAgICB1cGRhdGVQcm9wcyhwb2ludGVyRWwsIHtcbiAgICAgICAgc2hhcGU6IGVsT3B0aW9uLnBvaW50ZXIuc2hhcGVcbiAgICAgIH0pO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgdXBkYXRlTGFiZWxFbDogZnVuY3Rpb24gKGdyb3VwLCBlbE9wdGlvbiwgdXBkYXRlUHJvcHMsIGF4aXNQb2ludGVyTW9kZWwpIHtcbiAgICB2YXIgbGFiZWxFbCA9IGlubmVyKGdyb3VwKS5sYWJlbEVsO1xuXG4gICAgaWYgKGxhYmVsRWwpIHtcbiAgICAgIGxhYmVsRWwuc2V0U3R5bGUoZWxPcHRpb24ubGFiZWwuc3R5bGUpO1xuICAgICAgdXBkYXRlUHJvcHMobGFiZWxFbCwge1xuICAgICAgICAvLyBDb25zaWRlciB0ZXh0IGxlbmd0aCBjaGFuZ2UgaW4gdmVydGljYWwgYXhpcywgYW5pbWF0aW9uIHNob3VsZFxuICAgICAgICAvLyBiZSB1c2VkIG9uIHNoYXBlLCBvdGhlcndpc2UgdGhlIGVmZmVjdCB3aWxsIGJlIHdlaXJkLlxuICAgICAgICBzaGFwZTogZWxPcHRpb24ubGFiZWwuc2hhcGUsXG4gICAgICAgIHBvc2l0aW9uOiBlbE9wdGlvbi5sYWJlbC5wb3NpdGlvblxuICAgICAgfSk7XG4gICAgICB1cGRhdGVMYWJlbFNob3dIaWRlKGxhYmVsRWwsIGF4aXNQb2ludGVyTW9kZWwpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9yZW5kZXJIYW5kbGU6IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIGlmICh0aGlzLl9kcmFnZ2luZyB8fCAhdGhpcy51cGRhdGVIYW5kbGVUcmFuc2Zvcm0pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgYXhpc1BvaW50ZXJNb2RlbCA9IHRoaXMuX2F4aXNQb2ludGVyTW9kZWw7XG5cbiAgICB2YXIgenIgPSB0aGlzLl9hcGkuZ2V0WnIoKTtcblxuICAgIHZhciBoYW5kbGUgPSB0aGlzLl9oYW5kbGU7XG4gICAgdmFyIGhhbmRsZU1vZGVsID0gYXhpc1BvaW50ZXJNb2RlbC5nZXRNb2RlbCgnaGFuZGxlJyk7XG4gICAgdmFyIHN0YXR1cyA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCdzdGF0dXMnKTtcblxuICAgIGlmICghaGFuZGxlTW9kZWwuZ2V0KCdzaG93JykgfHwgIXN0YXR1cyB8fCBzdGF0dXMgPT09ICdoaWRlJykge1xuICAgICAgaGFuZGxlICYmIHpyLnJlbW92ZShoYW5kbGUpO1xuICAgICAgdGhpcy5faGFuZGxlID0gbnVsbDtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgaXNJbml0O1xuXG4gICAgaWYgKCF0aGlzLl9oYW5kbGUpIHtcbiAgICAgIGlzSW5pdCA9IHRydWU7XG4gICAgICBoYW5kbGUgPSB0aGlzLl9oYW5kbGUgPSBncmFwaGljLmNyZWF0ZUljb24oaGFuZGxlTW9kZWwuZ2V0KCdpY29uJyksIHtcbiAgICAgICAgY3Vyc29yOiAnbW92ZScsXG4gICAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcbiAgICAgICAgb25tb3VzZW1vdmU6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgLy8gRm90IG1vYmlsZSBkZXZpY2VtLCBwcmV2ZW50IHNjcmVlbiBzbGlkZXIgb24gdGhlIGJ1dHRvbi5cbiAgICAgICAgICBldmVudFRvb2wuc3RvcChlLmV2ZW50KTtcbiAgICAgICAgfSxcbiAgICAgICAgb25tb3VzZWRvd246IGJpbmQodGhpcy5fb25IYW5kbGVEcmFnTW92ZSwgdGhpcywgMCwgMCksXG4gICAgICAgIGRyaWZ0OiBiaW5kKHRoaXMuX29uSGFuZGxlRHJhZ01vdmUsIHRoaXMpLFxuICAgICAgICBvbmRyYWdlbmQ6IGJpbmQodGhpcy5fb25IYW5kbGVEcmFnRW5kLCB0aGlzKVxuICAgICAgfSk7XG4gICAgICB6ci5hZGQoaGFuZGxlKTtcbiAgICB9XG5cbiAgICB1cGRhdGVNYW5kYXRvcnlQcm9wcyhoYW5kbGUsIGF4aXNQb2ludGVyTW9kZWwsIGZhbHNlKTsgLy8gdXBkYXRlIHN0eWxlXG5cbiAgICB2YXIgaW5jbHVkZVN0eWxlcyA9IFsnY29sb3InLCAnYm9yZGVyQ29sb3InLCAnYm9yZGVyV2lkdGgnLCAnb3BhY2l0eScsICdzaGFkb3dDb2xvcicsICdzaGFkb3dCbHVyJywgJ3NoYWRvd09mZnNldFgnLCAnc2hhZG93T2Zmc2V0WSddO1xuICAgIGhhbmRsZS5zZXRTdHlsZShoYW5kbGVNb2RlbC5nZXRJdGVtU3R5bGUobnVsbCwgaW5jbHVkZVN0eWxlcykpOyAvLyB1cGRhdGUgcG9zaXRpb25cblxuICAgIHZhciBoYW5kbGVTaXplID0gaGFuZGxlTW9kZWwuZ2V0KCdzaXplJyk7XG5cbiAgICBpZiAoIXpyVXRpbC5pc0FycmF5KGhhbmRsZVNpemUpKSB7XG4gICAgICBoYW5kbGVTaXplID0gW2hhbmRsZVNpemUsIGhhbmRsZVNpemVdO1xuICAgIH1cblxuICAgIGhhbmRsZS5hdHRyKCdzY2FsZScsIFtoYW5kbGVTaXplWzBdIC8gMiwgaGFuZGxlU2l6ZVsxXSAvIDJdKTtcbiAgICB0aHJvdHRsZVV0aWwuY3JlYXRlT3JVcGRhdGUodGhpcywgJ19kb0Rpc3BhdGNoQXhpc1BvaW50ZXInLCBoYW5kbGVNb2RlbC5nZXQoJ3Rocm90dGxlJykgfHwgMCwgJ2ZpeFJhdGUnKTtcblxuICAgIHRoaXMuX21vdmVIYW5kbGVUb1ZhbHVlKHZhbHVlLCBpc0luaXQpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX21vdmVIYW5kbGVUb1ZhbHVlOiBmdW5jdGlvbiAodmFsdWUsIGlzSW5pdCkge1xuICAgIHVwZGF0ZVByb3BzKHRoaXMuX2F4aXNQb2ludGVyTW9kZWwsICFpc0luaXQgJiYgdGhpcy5fbW92ZUFuaW1hdGlvbiwgdGhpcy5faGFuZGxlLCBnZXRIYW5kbGVUcmFuc1Byb3BzKHRoaXMuZ2V0SGFuZGxlVHJhbnNmb3JtKHZhbHVlLCB0aGlzLl9heGlzTW9kZWwsIHRoaXMuX2F4aXNQb2ludGVyTW9kZWwpKSk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfb25IYW5kbGVEcmFnTW92ZTogZnVuY3Rpb24gKGR4LCBkeSkge1xuICAgIHZhciBoYW5kbGUgPSB0aGlzLl9oYW5kbGU7XG5cbiAgICBpZiAoIWhhbmRsZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuX2RyYWdnaW5nID0gdHJ1ZTsgLy8gUGVyc2lzdGVudCBmb3IgdGhyb3R0bGUuXG5cbiAgICB2YXIgdHJhbnMgPSB0aGlzLnVwZGF0ZUhhbmRsZVRyYW5zZm9ybShnZXRIYW5kbGVUcmFuc1Byb3BzKGhhbmRsZSksIFtkeCwgZHldLCB0aGlzLl9heGlzTW9kZWwsIHRoaXMuX2F4aXNQb2ludGVyTW9kZWwpO1xuICAgIHRoaXMuX3BheWxvYWRJbmZvID0gdHJhbnM7XG4gICAgaGFuZGxlLnN0b3BBbmltYXRpb24oKTtcbiAgICBoYW5kbGUuYXR0cihnZXRIYW5kbGVUcmFuc1Byb3BzKHRyYW5zKSk7XG4gICAgaW5uZXIoaGFuZGxlKS5sYXN0UHJvcCA9IG51bGw7XG5cbiAgICB0aGlzLl9kb0Rpc3BhdGNoQXhpc1BvaW50ZXIoKTtcbiAgfSxcblxuICAvKipcbiAgICogVGhyb3R0bGVkIG1ldGhvZC5cbiAgICogQHByaXZhdGVcbiAgICovXG4gIF9kb0Rpc3BhdGNoQXhpc1BvaW50ZXI6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgaGFuZGxlID0gdGhpcy5faGFuZGxlO1xuXG4gICAgaWYgKCFoYW5kbGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcGF5bG9hZEluZm8gPSB0aGlzLl9wYXlsb2FkSW5mbztcbiAgICB2YXIgYXhpc01vZGVsID0gdGhpcy5fYXhpc01vZGVsO1xuXG4gICAgdGhpcy5fYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICAgIHR5cGU6ICd1cGRhdGVBeGlzUG9pbnRlcicsXG4gICAgICB4OiBwYXlsb2FkSW5mby5jdXJzb3JQb2ludFswXSxcbiAgICAgIHk6IHBheWxvYWRJbmZvLmN1cnNvclBvaW50WzFdLFxuICAgICAgdG9vbHRpcE9wdGlvbjogcGF5bG9hZEluZm8udG9vbHRpcE9wdGlvbixcbiAgICAgIGF4ZXNJbmZvOiBbe1xuICAgICAgICBheGlzRGltOiBheGlzTW9kZWwuYXhpcy5kaW0sXG4gICAgICAgIGF4aXNJbmRleDogYXhpc01vZGVsLmNvbXBvbmVudEluZGV4XG4gICAgICB9XVxuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX29uSGFuZGxlRHJhZ0VuZDogZnVuY3Rpb24gKG1vdmVBbmltYXRpb24pIHtcbiAgICB0aGlzLl9kcmFnZ2luZyA9IGZhbHNlO1xuICAgIHZhciBoYW5kbGUgPSB0aGlzLl9oYW5kbGU7XG5cbiAgICBpZiAoIWhhbmRsZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciB2YWx1ZSA9IHRoaXMuX2F4aXNQb2ludGVyTW9kZWwuZ2V0KCd2YWx1ZScpOyAvLyBDb25zaWRlciBzbmFwIG9yIGNhdGVncm95IGF4aXMsIGhhbmRsZSBtYXkgYmUgbm90IGNvbnNpc3RlbnQgd2l0aFxuICAgIC8vIGF4aXNQb2ludGVyLiBTbyBtb3ZlIGhhbmRsZSB0byBhbGlnbiB0aGUgZXhhY3QgdmFsdWUgcG9zaXRpb24gd2hlblxuICAgIC8vIGRyYWcgZW5kZWQuXG5cblxuICAgIHRoaXMuX21vdmVIYW5kbGVUb1ZhbHVlKHZhbHVlKTsgLy8gRm9yIHRoZSBlZmZlY3Q6IHRvb2x0aXAgd2lsbCBiZSBzaG93biB3aGVuIGZpbmdlciBob2xkaW5nIG9uIGhhbmRsZVxuICAgIC8vIGJ1dHRvbiwgYW5kIHdpbGwgYmUgaGlkZGVuIGFmdGVyIGZpbmdlciBsZWZ0IGhhbmRsZSBidXR0b24uXG5cblxuICAgIHRoaXMuX2FwaS5kaXNwYXRjaEFjdGlvbih7XG4gICAgICB0eXBlOiAnaGlkZVRpcCdcbiAgICB9KTtcbiAgfSxcblxuICAvKipcbiAgICogU2hvdWxkIGJlIGltcGxlbWVuZW50ZWQgYnkgc3ViLWNsYXNzIGlmIHN1cHBvcnQgYGhhbmRsZWAuXG4gICAqIEBwcm90ZWN0ZWRcbiAgICogQHBhcmFtIHtudW1iZXJ9IHZhbHVlXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IGF4aXNNb2RlbFxuICAgKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBheGlzUG9pbnRlck1vZGVsXG4gICAqIEByZXR1cm4ge09iamVjdH0ge3Bvc2l0aW9uOiBbeCwgeV0sIHJvdGF0aW9uOiAwfVxuICAgKi9cbiAgZ2V0SGFuZGxlVHJhbnNmb3JtOiBudWxsLFxuXG4gIC8qKlxuICAgKiAqIFNob3VsZCBiZSBpbXBsZW1lbmVudGVkIGJ5IHN1Yi1jbGFzcyBpZiBzdXBwb3J0IGBoYW5kbGVgLlxuICAgKiBAcHJvdGVjdGVkXG4gICAqIEBwYXJhbSB7T2JqZWN0fSB0cmFuc2Zvcm0ge3Bvc2l0aW9uLCByb3RhdGlvbn1cbiAgICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gZGVsdGEgW2R4LCBkeV1cbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Nb2RlbH0gYXhpc01vZGVsXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvTW9kZWx9IGF4aXNQb2ludGVyTW9kZWxcbiAgICogQHJldHVybiB7T2JqZWN0fSB7cG9zaXRpb246IFt4LCB5XSwgcm90YXRpb246IDAsIGN1cnNvclBvaW50OiBbeCwgeV19XG4gICAqL1xuICB1cGRhdGVIYW5kbGVUcmFuc2Zvcm06IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBjbGVhcjogZnVuY3Rpb24gKGFwaSkge1xuICAgIHRoaXMuX2xhc3RWYWx1ZSA9IG51bGw7XG4gICAgdGhpcy5fbGFzdFN0YXR1cyA9IG51bGw7XG4gICAgdmFyIHpyID0gYXBpLmdldFpyKCk7XG4gICAgdmFyIGdyb3VwID0gdGhpcy5fZ3JvdXA7XG4gICAgdmFyIGhhbmRsZSA9IHRoaXMuX2hhbmRsZTtcblxuICAgIGlmICh6ciAmJiBncm91cCkge1xuICAgICAgdGhpcy5fbGFzdEdyYXBoaWNLZXkgPSBudWxsO1xuICAgICAgZ3JvdXAgJiYgenIucmVtb3ZlKGdyb3VwKTtcbiAgICAgIGhhbmRsZSAmJiB6ci5yZW1vdmUoaGFuZGxlKTtcbiAgICAgIHRoaXMuX2dyb3VwID0gbnVsbDtcbiAgICAgIHRoaXMuX2hhbmRsZSA9IG51bGw7XG4gICAgICB0aGlzLl9wYXlsb2FkSW5mbyA9IG51bGw7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBkb0NsZWFyOiBmdW5jdGlvbiAoKSB7Ly8gSW1wbGVtZW50ZWQgYnkgc3ViLWNsYXNzIGlmIG5lY2Vzc2FyeS5cbiAgfSxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSB4eVxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSB3aFxuICAgKiBAcGFyYW0ge251bWJlcn0gW3hEaW1JbmRleD0wXSBvciAxXG4gICAqL1xuICBidWlsZExhYmVsOiBmdW5jdGlvbiAoeHksIHdoLCB4RGltSW5kZXgpIHtcbiAgICB4RGltSW5kZXggPSB4RGltSW5kZXggfHwgMDtcbiAgICByZXR1cm4ge1xuICAgICAgeDogeHlbeERpbUluZGV4XSxcbiAgICAgIHk6IHh5WzEgLSB4RGltSW5kZXhdLFxuICAgICAgd2lkdGg6IHdoW3hEaW1JbmRleF0sXG4gICAgICBoZWlnaHQ6IHdoWzEgLSB4RGltSW5kZXhdXG4gICAgfTtcbiAgfVxufTtcbkJhc2VBeGlzUG9pbnRlci5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBCYXNlQXhpc1BvaW50ZXI7XG5cbmZ1bmN0aW9uIHVwZGF0ZVByb3BzKGFuaW1hdGlvbk1vZGVsLCBtb3ZlQW5pbWF0aW9uLCBlbCwgcHJvcHMpIHtcbiAgLy8gQW5pbWF0aW9uIG9wdGltaXplLlxuICBpZiAoIXByb3BzRXF1YWwoaW5uZXIoZWwpLmxhc3RQcm9wLCBwcm9wcykpIHtcbiAgICBpbm5lcihlbCkubGFzdFByb3AgPSBwcm9wcztcbiAgICBtb3ZlQW5pbWF0aW9uID8gZ3JhcGhpYy51cGRhdGVQcm9wcyhlbCwgcHJvcHMsIGFuaW1hdGlvbk1vZGVsKSA6IChlbC5zdG9wQW5pbWF0aW9uKCksIGVsLmF0dHIocHJvcHMpKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBwcm9wc0VxdWFsKGxhc3RQcm9wcywgbmV3UHJvcHMpIHtcbiAgaWYgKHpyVXRpbC5pc09iamVjdChsYXN0UHJvcHMpICYmIHpyVXRpbC5pc09iamVjdChuZXdQcm9wcykpIHtcbiAgICB2YXIgZXF1YWxzID0gdHJ1ZTtcbiAgICB6clV0aWwuZWFjaChuZXdQcm9wcywgZnVuY3Rpb24gKGl0ZW0sIGtleSkge1xuICAgICAgZXF1YWxzID0gZXF1YWxzICYmIHByb3BzRXF1YWwobGFzdFByb3BzW2tleV0sIGl0ZW0pO1xuICAgIH0pO1xuICAgIHJldHVybiAhIWVxdWFscztcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gbGFzdFByb3BzID09PSBuZXdQcm9wcztcbiAgfVxufVxuXG5mdW5jdGlvbiB1cGRhdGVMYWJlbFNob3dIaWRlKGxhYmVsRWwsIGF4aXNQb2ludGVyTW9kZWwpIHtcbiAgbGFiZWxFbFtheGlzUG9pbnRlck1vZGVsLmdldCgnbGFiZWwuc2hvdycpID8gJ3Nob3cnIDogJ2hpZGUnXSgpO1xufVxuXG5mdW5jdGlvbiBnZXRIYW5kbGVUcmFuc1Byb3BzKHRyYW5zKSB7XG4gIHJldHVybiB7XG4gICAgcG9zaXRpb246IHRyYW5zLnBvc2l0aW9uLnNsaWNlKCksXG4gICAgcm90YXRpb246IHRyYW5zLnJvdGF0aW9uIHx8IDBcbiAgfTtcbn1cblxuZnVuY3Rpb24gdXBkYXRlTWFuZGF0b3J5UHJvcHMoZ3JvdXAsIGF4aXNQb2ludGVyTW9kZWwsIHNpbGVudCkge1xuICB2YXIgeiA9IGF4aXNQb2ludGVyTW9kZWwuZ2V0KCd6Jyk7XG4gIHZhciB6bGV2ZWwgPSBheGlzUG9pbnRlck1vZGVsLmdldCgnemxldmVsJyk7XG4gIGdyb3VwICYmIGdyb3VwLnRyYXZlcnNlKGZ1bmN0aW9uIChlbCkge1xuICAgIGlmIChlbC50eXBlICE9PSAnZ3JvdXAnKSB7XG4gICAgICB6ICE9IG51bGwgJiYgKGVsLnogPSB6KTtcbiAgICAgIHpsZXZlbCAhPSBudWxsICYmIChlbC56bGV2ZWwgPSB6bGV2ZWwpO1xuICAgICAgZWwuc2lsZW50ID0gc2lsZW50O1xuICAgIH1cbiAgfSk7XG59XG5cbmNsYXp6VXRpbC5lbmFibGVDbGFzc0V4dGVuZChCYXNlQXhpc1BvaW50ZXIpO1xudmFyIF9kZWZhdWx0ID0gQmFzZUF4aXNQb2ludGVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBTEE7QUFVQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQTdaQTtBQStaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/BaseAxisPointer.js
