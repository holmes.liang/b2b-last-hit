/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _layout = __webpack_require__(/*! ../../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutRect = _layout.getLayoutRect;
var layoutBox = _layout.box;
var positionElement = _layout.positionElement;

var formatUtil = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Layout list like component.
 * It will box layout each items in group of component and then position the whole group in the viewport
 * @param {module:zrender/group/Group} group
 * @param {module:echarts/model/Component} componentModel
 * @param {module:echarts/ExtensionAPI}
 */


function layout(group, componentModel, api) {
  var boxLayoutParams = componentModel.getBoxLayoutParams();
  var padding = componentModel.get('padding');
  var viewportSize = {
    width: api.getWidth(),
    height: api.getHeight()
  };
  var rect = getLayoutRect(boxLayoutParams, viewportSize, padding);
  layoutBox(componentModel.get('orient'), group, componentModel.get('itemGap'), rect.width, rect.height);
  positionElement(group, boxLayoutParams, viewportSize, padding);
}

function makeBackground(rect, componentModel) {
  var padding = formatUtil.normalizeCssArray(componentModel.get('padding'));
  var style = componentModel.getItemStyle(['color', 'opacity']);
  style.fill = componentModel.get('backgroundColor');
  var rect = new graphic.Rect({
    shape: {
      x: rect.x - padding[3],
      y: rect.y - padding[0],
      width: rect.width + padding[1] + padding[3],
      height: rect.height + padding[0] + padding[2],
      r: componentModel.get('borderRadius')
    },
    style: style,
    silent: true,
    z2: -1
  }); // FIXME
  // `subPixelOptimizeRect` may bring some gap between edge of viewpart
  // and background rect when setting like `left: 0`, `top: 0`.
  // graphic.subPixelOptimizeRect(rect);

  return rect;
}

exports.layout = layout;
exports.makeBackground = makeBackground;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2hlbHBlci9saXN0Q29tcG9uZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2hlbHBlci9saXN0Q29tcG9uZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2xheW91dCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2xheW91dFwiKTtcblxudmFyIGdldExheW91dFJlY3QgPSBfbGF5b3V0LmdldExheW91dFJlY3Q7XG52YXIgbGF5b3V0Qm94ID0gX2xheW91dC5ib3g7XG52YXIgcG9zaXRpb25FbGVtZW50ID0gX2xheW91dC5wb3NpdGlvbkVsZW1lbnQ7XG5cbnZhciBmb3JtYXRVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZm9ybWF0XCIpO1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2dyYXBoaWNcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBMYXlvdXQgbGlzdCBsaWtlIGNvbXBvbmVudC5cbiAqIEl0IHdpbGwgYm94IGxheW91dCBlYWNoIGl0ZW1zIGluIGdyb3VwIG9mIGNvbXBvbmVudCBhbmQgdGhlbiBwb3NpdGlvbiB0aGUgd2hvbGUgZ3JvdXAgaW4gdGhlIHZpZXdwb3J0XG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2dyb3VwL0dyb3VwfSBncm91cFxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9Db21wb25lbnR9IGNvbXBvbmVudE1vZGVsXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX1cbiAqL1xuZnVuY3Rpb24gbGF5b3V0KGdyb3VwLCBjb21wb25lbnRNb2RlbCwgYXBpKSB7XG4gIHZhciBib3hMYXlvdXRQYXJhbXMgPSBjb21wb25lbnRNb2RlbC5nZXRCb3hMYXlvdXRQYXJhbXMoKTtcbiAgdmFyIHBhZGRpbmcgPSBjb21wb25lbnRNb2RlbC5nZXQoJ3BhZGRpbmcnKTtcbiAgdmFyIHZpZXdwb3J0U2l6ZSA9IHtcbiAgICB3aWR0aDogYXBpLmdldFdpZHRoKCksXG4gICAgaGVpZ2h0OiBhcGkuZ2V0SGVpZ2h0KClcbiAgfTtcbiAgdmFyIHJlY3QgPSBnZXRMYXlvdXRSZWN0KGJveExheW91dFBhcmFtcywgdmlld3BvcnRTaXplLCBwYWRkaW5nKTtcbiAgbGF5b3V0Qm94KGNvbXBvbmVudE1vZGVsLmdldCgnb3JpZW50JyksIGdyb3VwLCBjb21wb25lbnRNb2RlbC5nZXQoJ2l0ZW1HYXAnKSwgcmVjdC53aWR0aCwgcmVjdC5oZWlnaHQpO1xuICBwb3NpdGlvbkVsZW1lbnQoZ3JvdXAsIGJveExheW91dFBhcmFtcywgdmlld3BvcnRTaXplLCBwYWRkaW5nKTtcbn1cblxuZnVuY3Rpb24gbWFrZUJhY2tncm91bmQocmVjdCwgY29tcG9uZW50TW9kZWwpIHtcbiAgdmFyIHBhZGRpbmcgPSBmb3JtYXRVdGlsLm5vcm1hbGl6ZUNzc0FycmF5KGNvbXBvbmVudE1vZGVsLmdldCgncGFkZGluZycpKTtcbiAgdmFyIHN0eWxlID0gY29tcG9uZW50TW9kZWwuZ2V0SXRlbVN0eWxlKFsnY29sb3InLCAnb3BhY2l0eSddKTtcbiAgc3R5bGUuZmlsbCA9IGNvbXBvbmVudE1vZGVsLmdldCgnYmFja2dyb3VuZENvbG9yJyk7XG4gIHZhciByZWN0ID0gbmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgc2hhcGU6IHtcbiAgICAgIHg6IHJlY3QueCAtIHBhZGRpbmdbM10sXG4gICAgICB5OiByZWN0LnkgLSBwYWRkaW5nWzBdLFxuICAgICAgd2lkdGg6IHJlY3Qud2lkdGggKyBwYWRkaW5nWzFdICsgcGFkZGluZ1szXSxcbiAgICAgIGhlaWdodDogcmVjdC5oZWlnaHQgKyBwYWRkaW5nWzBdICsgcGFkZGluZ1syXSxcbiAgICAgIHI6IGNvbXBvbmVudE1vZGVsLmdldCgnYm9yZGVyUmFkaXVzJylcbiAgICB9LFxuICAgIHN0eWxlOiBzdHlsZSxcbiAgICBzaWxlbnQ6IHRydWUsXG4gICAgejI6IC0xXG4gIH0pOyAvLyBGSVhNRVxuICAvLyBgc3ViUGl4ZWxPcHRpbWl6ZVJlY3RgIG1heSBicmluZyBzb21lIGdhcCBiZXR3ZWVuIGVkZ2Ugb2Ygdmlld3BhcnRcbiAgLy8gYW5kIGJhY2tncm91bmQgcmVjdCB3aGVuIHNldHRpbmcgbGlrZSBgbGVmdDogMGAsIGB0b3A6IDBgLlxuICAvLyBncmFwaGljLnN1YlBpeGVsT3B0aW1pemVSZWN0KHJlY3QpO1xuXG4gIHJldHVybiByZWN0O1xufVxuXG5leHBvcnRzLmxheW91dCA9IGxheW91dDtcbmV4cG9ydHMubWFrZUJhY2tncm91bmQgPSBtYWtlQmFja2dyb3VuZDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/helper/listComponent.js
