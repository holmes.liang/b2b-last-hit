__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Anchor; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/Dom/addEventListener */ "./node_modules/rc-util/es/Dom/addEventListener.js");
/* harmony import */ var _affix__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../affix */ "./node_modules/antd/es/affix/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_scrollTo__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/scrollTo */ "./node_modules/antd/es/_util/scrollTo.js");
/* harmony import */ var _util_getScroll__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/getScroll */ "./node_modules/antd/es/_util/getScroll.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}











function getDefaultContainer() {
  return window;
}

function getOffsetTop(element, container) {
  if (!element) {
    return 0;
  }

  if (!element.getClientRects().length) {
    return 0;
  }

  var rect = element.getBoundingClientRect();

  if (rect.width || rect.height) {
    if (container === window) {
      container = element.ownerDocument.documentElement;
      return rect.top - container.clientTop;
    }

    return rect.top - container.getBoundingClientRect().top;
  }

  return rect.top;
}

var sharpMatcherRegx = /#([^#]+)$/;

var Anchor =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Anchor, _React$Component);

  function Anchor() {
    var _this;

    _classCallCheck(this, Anchor);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Anchor).apply(this, arguments));
    _this.state = {
      activeLink: null
    };
    _this.links = [];

    _this.handleScrollTo = function (link) {
      var _this$props = _this.props,
          offsetTop = _this$props.offsetTop,
          getContainer = _this$props.getContainer,
          targetOffset = _this$props.targetOffset;

      _this.setCurrentActiveLink(link);

      var container = getContainer();
      var scrollTop = Object(_util_getScroll__WEBPACK_IMPORTED_MODULE_8__["default"])(container, true);
      var sharpLinkMatch = sharpMatcherRegx.exec(link);

      if (!sharpLinkMatch) {
        return;
      }

      var targetElement = document.getElementById(sharpLinkMatch[1]);

      if (!targetElement) {
        return;
      }

      var eleOffsetTop = getOffsetTop(targetElement, container);
      var y = scrollTop + eleOffsetTop;
      y -= targetOffset !== undefined ? targetOffset : offsetTop || 0;
      _this.animating = true;
      Object(_util_scrollTo__WEBPACK_IMPORTED_MODULE_7__["default"])(y, {
        callback: function callback() {
          _this.animating = false;
        },
        getContainer: getContainer
      });
    };

    _this.saveInkNode = function (node) {
      _this.inkNode = node;
    };

    _this.setCurrentActiveLink = function (link) {
      var activeLink = _this.state.activeLink;
      var onChange = _this.props.onChange;

      if (activeLink !== link) {
        _this.setState({
          activeLink: link
        });

        if (onChange) {
          onChange(link);
        }
      }
    };

    _this.handleScroll = function () {
      if (_this.animating) {
        return;
      }

      var _this$props2 = _this.props,
          offsetTop = _this$props2.offsetTop,
          bounds = _this$props2.bounds,
          targetOffset = _this$props2.targetOffset;

      var currentActiveLink = _this.getCurrentAnchor(targetOffset !== undefined ? targetOffset : offsetTop || 0, bounds);

      _this.setCurrentActiveLink(currentActiveLink);
    };

    _this.updateInk = function () {
      if (typeof document === 'undefined') {
        return;
      }

      var _assertThisInitialize = _assertThisInitialized(_this),
          prefixCls = _assertThisInitialize.prefixCls;

      var anchorNode = react_dom__WEBPACK_IMPORTED_MODULE_1__["findDOMNode"](_assertThisInitialized(_this));
      var linkNode = anchorNode.getElementsByClassName("".concat(prefixCls, "-link-title-active"))[0];

      if (linkNode) {
        _this.inkNode.style.top = "".concat(linkNode.offsetTop + linkNode.clientHeight / 2 - 4.5, "px");
      }
    };

    _this.renderAnchor = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$props3 = _this.props,
          customizePrefixCls = _this$props3.prefixCls,
          _this$props3$classNam = _this$props3.className,
          className = _this$props3$classNam === void 0 ? '' : _this$props3$classNam,
          style = _this$props3.style,
          offsetTop = _this$props3.offsetTop,
          affix = _this$props3.affix,
          showInkInFixed = _this$props3.showInkInFixed,
          children = _this$props3.children,
          getContainer = _this$props3.getContainer;
      var activeLink = _this.state.activeLink;
      var prefixCls = getPrefixCls('anchor', customizePrefixCls); // To support old version react.
      // Have to add prefixCls on the instance.
      // https://github.com/facebook/react/issues/12397

      _this.prefixCls = prefixCls;
      var inkClass = classnames__WEBPACK_IMPORTED_MODULE_3___default()("".concat(prefixCls, "-ink-ball"), {
        visible: activeLink
      });
      var wrapperClass = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, "".concat(prefixCls, "-wrapper"));
      var anchorClass = classnames__WEBPACK_IMPORTED_MODULE_3___default()(prefixCls, {
        fixed: !affix && !showInkInFixed
      });

      var wrapperStyle = _extends({
        maxHeight: offsetTop ? "calc(100vh - ".concat(offsetTop, "px)") : '100vh'
      }, style);

      var anchorContent = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: wrapperClass,
        style: wrapperStyle
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: anchorClass
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-ink")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: inkClass,
        ref: _this.saveInkNode
      })), children));
      return !affix ? anchorContent : react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_affix__WEBPACK_IMPORTED_MODULE_5__["default"], {
        offsetTop: offsetTop,
        target: getContainer
      }, anchorContent);
    };

    return _this;
  }

  _createClass(Anchor, [{
    key: "getChildContext",
    value: function getChildContext() {
      var _this2 = this;

      var antAnchor = {
        registerLink: function registerLink(link) {
          if (!_this2.links.includes(link)) {
            _this2.links.push(link);
          }
        },
        unregisterLink: function unregisterLink(link) {
          var index = _this2.links.indexOf(link);

          if (index !== -1) {
            _this2.links.splice(index, 1);
          }
        },
        activeLink: this.state.activeLink,
        scrollTo: this.handleScrollTo,
        onClick: this.props.onClick
      };
      return {
        antAnchor: antAnchor
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var getContainer = this.props.getContainer;
      this.scrollContainer = getContainer();
      this.scrollEvent = Object(rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_4__["default"])(this.scrollContainer, 'scroll', this.handleScroll);
      this.handleScroll();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.scrollEvent) {
        var getContainer = this.props.getContainer;
        var currentContainer = getContainer();

        if (this.scrollContainer !== currentContainer) {
          this.scrollContainer = currentContainer;
          this.scrollEvent.remove();
          this.scrollEvent = Object(rc_util_es_Dom_addEventListener__WEBPACK_IMPORTED_MODULE_4__["default"])(this.scrollContainer, 'scroll', this.handleScroll);
          this.handleScroll();
        }
      }

      this.updateInk();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.scrollEvent) {
        this.scrollEvent.remove();
      }
    }
  }, {
    key: "getCurrentAnchor",
    value: function getCurrentAnchor() {
      var offsetTop = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var bounds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 5;
      var getCurrentAnchor = this.props.getCurrentAnchor;

      if (typeof getCurrentAnchor === 'function') {
        return getCurrentAnchor();
      }

      var activeLink = '';

      if (typeof document === 'undefined') {
        return activeLink;
      }

      var linkSections = [];
      var getContainer = this.props.getContainer;
      var container = getContainer();
      this.links.forEach(function (link) {
        var sharpLinkMatch = sharpMatcherRegx.exec(link.toString());

        if (!sharpLinkMatch) {
          return;
        }

        var target = document.getElementById(sharpLinkMatch[1]);

        if (target) {
          var top = getOffsetTop(target, container);

          if (top < offsetTop + bounds) {
            linkSections.push({
              link: link,
              top: top
            });
          }
        }
      });

      if (linkSections.length) {
        var maxSection = linkSections.reduce(function (prev, curr) {
          return curr.top > prev.top ? curr : prev;
        });
        return maxSection.link;
      }

      return '';
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderAnchor);
    }
  }]);

  return Anchor;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Anchor.defaultProps = {
  affix: true,
  showInkInFixed: false,
  getContainer: getDefaultContainer
};
Anchor.childContextTypes = {
  antAnchor: prop_types__WEBPACK_IMPORTED_MODULE_2__["object"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hbmNob3IvQW5jaG9yLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9hbmNob3IvQW5jaG9yLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgKiBhcyBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgYWRkRXZlbnRMaXN0ZW5lciBmcm9tICdyYy11dGlsL2xpYi9Eb20vYWRkRXZlbnRMaXN0ZW5lcic7XG5pbXBvcnQgQWZmaXggZnJvbSAnLi4vYWZmaXgnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHNjcm9sbFRvIGZyb20gJy4uL191dGlsL3Njcm9sbFRvJztcbmltcG9ydCBnZXRTY3JvbGwgZnJvbSAnLi4vX3V0aWwvZ2V0U2Nyb2xsJztcbmZ1bmN0aW9uIGdldERlZmF1bHRDb250YWluZXIoKSB7XG4gICAgcmV0dXJuIHdpbmRvdztcbn1cbmZ1bmN0aW9uIGdldE9mZnNldFRvcChlbGVtZW50LCBjb250YWluZXIpIHtcbiAgICBpZiAoIWVsZW1lbnQpIHtcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgfVxuICAgIGlmICghZWxlbWVudC5nZXRDbGllbnRSZWN0cygpLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gMDtcbiAgICB9XG4gICAgY29uc3QgcmVjdCA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgaWYgKHJlY3Qud2lkdGggfHwgcmVjdC5oZWlnaHQpIHtcbiAgICAgICAgaWYgKGNvbnRhaW5lciA9PT0gd2luZG93KSB7XG4gICAgICAgICAgICBjb250YWluZXIgPSBlbGVtZW50Lm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICAgICAgcmV0dXJuIHJlY3QudG9wIC0gY29udGFpbmVyLmNsaWVudFRvcDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVjdC50b3AgLSBjb250YWluZXIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wO1xuICAgIH1cbiAgICByZXR1cm4gcmVjdC50b3A7XG59XG5jb25zdCBzaGFycE1hdGNoZXJSZWd4ID0gLyMoW14jXSspJC87XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBbmNob3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgICAgICAgYWN0aXZlTGluazogbnVsbCxcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5saW5rcyA9IFtdO1xuICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbFRvID0gKGxpbmspID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb2Zmc2V0VG9wLCBnZXRDb250YWluZXIsIHRhcmdldE9mZnNldCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIHRoaXMuc2V0Q3VycmVudEFjdGl2ZUxpbmsobGluayk7XG4gICAgICAgICAgICBjb25zdCBjb250YWluZXIgPSBnZXRDb250YWluZXIoKTtcbiAgICAgICAgICAgIGNvbnN0IHNjcm9sbFRvcCA9IGdldFNjcm9sbChjb250YWluZXIsIHRydWUpO1xuICAgICAgICAgICAgY29uc3Qgc2hhcnBMaW5rTWF0Y2ggPSBzaGFycE1hdGNoZXJSZWd4LmV4ZWMobGluayk7XG4gICAgICAgICAgICBpZiAoIXNoYXJwTGlua01hdGNoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgdGFyZ2V0RWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHNoYXJwTGlua01hdGNoWzFdKTtcbiAgICAgICAgICAgIGlmICghdGFyZ2V0RWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGVsZU9mZnNldFRvcCA9IGdldE9mZnNldFRvcCh0YXJnZXRFbGVtZW50LCBjb250YWluZXIpO1xuICAgICAgICAgICAgbGV0IHkgPSBzY3JvbGxUb3AgKyBlbGVPZmZzZXRUb3A7XG4gICAgICAgICAgICB5IC09IHRhcmdldE9mZnNldCAhPT0gdW5kZWZpbmVkID8gdGFyZ2V0T2Zmc2V0IDogb2Zmc2V0VG9wIHx8IDA7XG4gICAgICAgICAgICB0aGlzLmFuaW1hdGluZyA9IHRydWU7XG4gICAgICAgICAgICBzY3JvbGxUbyh5LCB7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2s6ICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hbmltYXRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGdldENvbnRhaW5lcixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNhdmVJbmtOb2RlID0gKG5vZGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5rTm9kZSA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2V0Q3VycmVudEFjdGl2ZUxpbmsgPSAobGluaykgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBhY3RpdmVMaW5rIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgeyBvbkNoYW5nZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChhY3RpdmVMaW5rICE9PSBsaW5rKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZUxpbms6IGxpbmssXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlKGxpbmspO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgICAgICAgICBpZiAodGhpcy5hbmltYXRpbmcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCB7IG9mZnNldFRvcCwgYm91bmRzLCB0YXJnZXRPZmZzZXQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBjdXJyZW50QWN0aXZlTGluayA9IHRoaXMuZ2V0Q3VycmVudEFuY2hvcih0YXJnZXRPZmZzZXQgIT09IHVuZGVmaW5lZCA/IHRhcmdldE9mZnNldCA6IG9mZnNldFRvcCB8fCAwLCBib3VuZHMpO1xuICAgICAgICAgICAgdGhpcy5zZXRDdXJyZW50QWN0aXZlTGluayhjdXJyZW50QWN0aXZlTGluayk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMudXBkYXRlSW5rID0gKCkgPT4ge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBkb2N1bWVudCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENscyB9ID0gdGhpcztcbiAgICAgICAgICAgIGNvbnN0IGFuY2hvck5vZGUgPSBSZWFjdERPTS5maW5kRE9NTm9kZSh0aGlzKTtcbiAgICAgICAgICAgIGNvbnN0IGxpbmtOb2RlID0gYW5jaG9yTm9kZS5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKGAke3ByZWZpeENsc30tbGluay10aXRsZS1hY3RpdmVgKVswXTtcbiAgICAgICAgICAgIGlmIChsaW5rTm9kZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuaW5rTm9kZS5zdHlsZS50b3AgPSBgJHtsaW5rTm9kZS5vZmZzZXRUb3AgKyBsaW5rTm9kZS5jbGllbnRIZWlnaHQgLyAyIC0gNC41fXB4YDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJBbmNob3IgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lID0gJycsIHN0eWxlLCBvZmZzZXRUb3AsIGFmZml4LCBzaG93SW5rSW5GaXhlZCwgY2hpbGRyZW4sIGdldENvbnRhaW5lciwgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCB7IGFjdGl2ZUxpbmsgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2FuY2hvcicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICAvLyBUbyBzdXBwb3J0IG9sZCB2ZXJzaW9uIHJlYWN0LlxuICAgICAgICAgICAgLy8gSGF2ZSB0byBhZGQgcHJlZml4Q2xzIG9uIHRoZSBpbnN0YW5jZS5cbiAgICAgICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC9pc3N1ZXMvMTIzOTdcbiAgICAgICAgICAgIHRoaXMucHJlZml4Q2xzID0gcHJlZml4Q2xzO1xuICAgICAgICAgICAgY29uc3QgaW5rQ2xhc3MgPSBjbGFzc05hbWVzKGAke3ByZWZpeENsc30taW5rLWJhbGxgLCB7XG4gICAgICAgICAgICAgICAgdmlzaWJsZTogYWN0aXZlTGluayxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc3Qgd3JhcHBlckNsYXNzID0gY2xhc3NOYW1lcyhjbGFzc05hbWUsIGAke3ByZWZpeENsc30td3JhcHBlcmApO1xuICAgICAgICAgICAgY29uc3QgYW5jaG9yQ2xhc3MgPSBjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICAgICAgICAgIGZpeGVkOiAhYWZmaXggJiYgIXNob3dJbmtJbkZpeGVkLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCB3cmFwcGVyU3R5bGUgPSBPYmplY3QuYXNzaWduKHsgbWF4SGVpZ2h0OiBvZmZzZXRUb3AgPyBgY2FsYygxMDB2aCAtICR7b2Zmc2V0VG9wfXB4KWAgOiAnMTAwdmgnIH0sIHN0eWxlKTtcbiAgICAgICAgICAgIGNvbnN0IGFuY2hvckNvbnRlbnQgPSAoPGRpdiBjbGFzc05hbWU9e3dyYXBwZXJDbGFzc30gc3R5bGU9e3dyYXBwZXJTdHlsZX0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXthbmNob3JDbGFzc30+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taW5rYH0+XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9e2lua0NsYXNzfSByZWY9e3RoaXMuc2F2ZUlua05vZGV9Lz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+KTtcbiAgICAgICAgICAgIHJldHVybiAhYWZmaXggPyAoYW5jaG9yQ29udGVudCkgOiAoPEFmZml4IG9mZnNldFRvcD17b2Zmc2V0VG9wfSB0YXJnZXQ9e2dldENvbnRhaW5lcn0+XG4gICAgICAgIHthbmNob3JDb250ZW50fVxuICAgICAgPC9BZmZpeD4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgICAgIGNvbnN0IGFudEFuY2hvciA9IHtcbiAgICAgICAgICAgIHJlZ2lzdGVyTGluazogKGxpbmspID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMubGlua3MuaW5jbHVkZXMobGluaykpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saW5rcy5wdXNoKGxpbmspO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1bnJlZ2lzdGVyTGluazogKGxpbmspID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMubGlua3MuaW5kZXhPZihsaW5rKTtcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlua3Muc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYWN0aXZlTGluazogdGhpcy5zdGF0ZS5hY3RpdmVMaW5rLFxuICAgICAgICAgICAgc2Nyb2xsVG86IHRoaXMuaGFuZGxlU2Nyb2xsVG8sXG4gICAgICAgICAgICBvbkNsaWNrOiB0aGlzLnByb3BzLm9uQ2xpY2ssXG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiB7IGFudEFuY2hvciB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgY29uc3QgeyBnZXRDb250YWluZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyID0gZ2V0Q29udGFpbmVyKCk7XG4gICAgICAgIHRoaXMuc2Nyb2xsRXZlbnQgPSBhZGRFdmVudExpc3RlbmVyKHRoaXMuc2Nyb2xsQ29udGFpbmVyLCAnc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgICAgICB0aGlzLmhhbmRsZVNjcm9sbCgpO1xuICAgIH1cbiAgICBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICAgIGlmICh0aGlzLnNjcm9sbEV2ZW50KSB7XG4gICAgICAgICAgICBjb25zdCB7IGdldENvbnRhaW5lciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRDb250YWluZXIgPSBnZXRDb250YWluZXIoKTtcbiAgICAgICAgICAgIGlmICh0aGlzLnNjcm9sbENvbnRhaW5lciAhPT0gY3VycmVudENvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsQ29udGFpbmVyID0gY3VycmVudENvbnRhaW5lcjtcbiAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbEV2ZW50LnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsRXZlbnQgPSBhZGRFdmVudExpc3RlbmVyKHRoaXMuc2Nyb2xsQ29udGFpbmVyLCAnc2Nyb2xsJywgdGhpcy5oYW5kbGVTY3JvbGwpO1xuICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlU2Nyb2xsKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy51cGRhdGVJbmsoKTtcbiAgICB9XG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIGlmICh0aGlzLnNjcm9sbEV2ZW50KSB7XG4gICAgICAgICAgICB0aGlzLnNjcm9sbEV2ZW50LnJlbW92ZSgpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldEN1cnJlbnRBbmNob3Iob2Zmc2V0VG9wID0gMCwgYm91bmRzID0gNSkge1xuICAgICAgICBjb25zdCB7IGdldEN1cnJlbnRBbmNob3IgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmICh0eXBlb2YgZ2V0Q3VycmVudEFuY2hvciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmV0dXJuIGdldEN1cnJlbnRBbmNob3IoKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBhY3RpdmVMaW5rID0gJyc7XG4gICAgICAgIGlmICh0eXBlb2YgZG9jdW1lbnQgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICByZXR1cm4gYWN0aXZlTGluaztcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBsaW5rU2VjdGlvbnMgPSBbXTtcbiAgICAgICAgY29uc3QgeyBnZXRDb250YWluZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgICAgICB0aGlzLmxpbmtzLmZvckVhY2gobGluayA9PiB7XG4gICAgICAgICAgICBjb25zdCBzaGFycExpbmtNYXRjaCA9IHNoYXJwTWF0Y2hlclJlZ3guZXhlYyhsaW5rLnRvU3RyaW5nKCkpO1xuICAgICAgICAgICAgaWYgKCFzaGFycExpbmtNYXRjaCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHRhcmdldCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHNoYXJwTGlua01hdGNoWzFdKTtcbiAgICAgICAgICAgIGlmICh0YXJnZXQpIHtcbiAgICAgICAgICAgICAgICBjb25zdCB0b3AgPSBnZXRPZmZzZXRUb3AodGFyZ2V0LCBjb250YWluZXIpO1xuICAgICAgICAgICAgICAgIGlmICh0b3AgPCBvZmZzZXRUb3AgKyBib3VuZHMpIHtcbiAgICAgICAgICAgICAgICAgICAgbGlua1NlY3Rpb25zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgbGluayxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKGxpbmtTZWN0aW9ucy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNvbnN0IG1heFNlY3Rpb24gPSBsaW5rU2VjdGlvbnMucmVkdWNlKChwcmV2LCBjdXJyKSA9PiAoY3Vyci50b3AgPiBwcmV2LnRvcCA/IGN1cnIgOiBwcmV2KSk7XG4gICAgICAgICAgICByZXR1cm4gbWF4U2VjdGlvbi5saW5rO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnJztcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckFuY2hvcn08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5BbmNob3IuZGVmYXVsdFByb3BzID0ge1xuICAgIGFmZml4OiB0cnVlLFxuICAgIHNob3dJbmtJbkZpeGVkOiBmYWxzZSxcbiAgICBnZXRDb250YWluZXI6IGdldERlZmF1bHRDb250YWluZXIsXG59O1xuQW5jaG9yLmNoaWxkQ29udGV4dFR5cGVzID0ge1xuICAgIGFudEFuY2hvcjogUHJvcFR5cGVzLm9iamVjdCxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBSkE7QUFqQkE7QUFDQTtBQXVCQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBVkE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBTkE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBeEJBO0FBQ0E7QUFqRUE7QUE0RkE7QUFDQTs7O0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBZEE7QUFDQTtBQWVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBN0tBO0FBQ0E7QUFEQTtBQStLQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/anchor/Anchor.js
