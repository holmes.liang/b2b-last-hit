/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule EditorState
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var BlockTree = __webpack_require__(/*! ./BlockTree */ "./node_modules/draft-js/lib/BlockTree.js");

var ContentState = __webpack_require__(/*! ./ContentState */ "./node_modules/draft-js/lib/ContentState.js");

var EditorBidiService = __webpack_require__(/*! ./EditorBidiService */ "./node_modules/draft-js/lib/EditorBidiService.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var SelectionState = __webpack_require__(/*! ./SelectionState */ "./node_modules/draft-js/lib/SelectionState.js");

var OrderedSet = Immutable.OrderedSet,
    Record = Immutable.Record,
    Stack = Immutable.Stack;
var defaultRecord = {
  allowUndo: true,
  currentContent: null,
  decorator: null,
  directionMap: null,
  forceSelection: false,
  inCompositionMode: false,
  inlineStyleOverride: null,
  lastChangeType: null,
  nativelyRenderedContent: null,
  redoStack: Stack(),
  selection: null,
  treeMap: null,
  undoStack: Stack()
};
var EditorStateRecord = Record(defaultRecord);

var EditorState = function () {
  EditorState.createEmpty = function createEmpty(decorator) {
    return EditorState.createWithContent(ContentState.createFromText(''), decorator);
  };

  EditorState.createWithContent = function createWithContent(contentState, decorator) {
    var firstKey = contentState.getBlockMap().first().getKey();
    return EditorState.create({
      currentContent: contentState,
      undoStack: Stack(),
      redoStack: Stack(),
      decorator: decorator || null,
      selection: SelectionState.createEmpty(firstKey)
    });
  };

  EditorState.create = function create(config) {
    var currentContent = config.currentContent,
        decorator = config.decorator;

    var recordConfig = _extends({}, config, {
      treeMap: generateNewTreeMap(currentContent, decorator),
      directionMap: EditorBidiService.getDirectionMap(currentContent)
    });

    return new EditorState(new EditorStateRecord(recordConfig));
  };

  EditorState.set = function set(editorState, put) {
    var map = editorState.getImmutable().withMutations(function (state) {
      var existingDecorator = state.get('decorator');
      var decorator = existingDecorator;

      if (put.decorator === null) {
        decorator = null;
      } else if (put.decorator) {
        decorator = put.decorator;
      }

      var newContent = put.currentContent || editorState.getCurrentContent();

      if (decorator !== existingDecorator) {
        var treeMap = state.get('treeMap');
        var newTreeMap;

        if (decorator && existingDecorator) {
          newTreeMap = regenerateTreeForNewDecorator(newContent, newContent.getBlockMap(), treeMap, decorator, existingDecorator);
        } else {
          newTreeMap = generateNewTreeMap(newContent, decorator);
        }

        state.merge({
          decorator: decorator,
          treeMap: newTreeMap,
          nativelyRenderedContent: null
        });
        return;
      }

      var existingContent = editorState.getCurrentContent();

      if (newContent !== existingContent) {
        state.set('treeMap', regenerateTreeForNewBlocks(editorState, newContent.getBlockMap(), newContent.getEntityMap(), decorator));
      }

      state.merge(put);
    });
    return new EditorState(map);
  };

  EditorState.prototype.toJS = function toJS() {
    return this.getImmutable().toJS();
  };

  EditorState.prototype.getAllowUndo = function getAllowUndo() {
    return this.getImmutable().get('allowUndo');
  };

  EditorState.prototype.getCurrentContent = function getCurrentContent() {
    return this.getImmutable().get('currentContent');
  };

  EditorState.prototype.getUndoStack = function getUndoStack() {
    return this.getImmutable().get('undoStack');
  };

  EditorState.prototype.getRedoStack = function getRedoStack() {
    return this.getImmutable().get('redoStack');
  };

  EditorState.prototype.getSelection = function getSelection() {
    return this.getImmutable().get('selection');
  };

  EditorState.prototype.getDecorator = function getDecorator() {
    return this.getImmutable().get('decorator');
  };

  EditorState.prototype.isInCompositionMode = function isInCompositionMode() {
    return this.getImmutable().get('inCompositionMode');
  };

  EditorState.prototype.mustForceSelection = function mustForceSelection() {
    return this.getImmutable().get('forceSelection');
  };

  EditorState.prototype.getNativelyRenderedContent = function getNativelyRenderedContent() {
    return this.getImmutable().get('nativelyRenderedContent');
  };

  EditorState.prototype.getLastChangeType = function getLastChangeType() {
    return this.getImmutable().get('lastChangeType');
  };
  /**
   * While editing, the user may apply inline style commands with a collapsed
   * cursor, intending to type text that adopts the specified style. In this
   * case, we track the specified style as an "override" that takes precedence
   * over the inline style of the text adjacent to the cursor.
   *
   * If null, there is no override in place.
   */


  EditorState.prototype.getInlineStyleOverride = function getInlineStyleOverride() {
    return this.getImmutable().get('inlineStyleOverride');
  };

  EditorState.setInlineStyleOverride = function setInlineStyleOverride(editorState, inlineStyleOverride) {
    return EditorState.set(editorState, {
      inlineStyleOverride: inlineStyleOverride
    });
  };
  /**
   * Get the appropriate inline style for the editor state. If an
   * override is in place, use it. Otherwise, the current style is
   * based on the location of the selection state.
   */


  EditorState.prototype.getCurrentInlineStyle = function getCurrentInlineStyle() {
    var override = this.getInlineStyleOverride();

    if (override != null) {
      return override;
    }

    var content = this.getCurrentContent();
    var selection = this.getSelection();

    if (selection.isCollapsed()) {
      return getInlineStyleForCollapsedSelection(content, selection);
    }

    return getInlineStyleForNonCollapsedSelection(content, selection);
  };

  EditorState.prototype.getBlockTree = function getBlockTree(blockKey) {
    return this.getImmutable().getIn(['treeMap', blockKey]);
  };

  EditorState.prototype.isSelectionAtStartOfContent = function isSelectionAtStartOfContent() {
    var firstKey = this.getCurrentContent().getBlockMap().first().getKey();
    return this.getSelection().hasEdgeWithin(firstKey, 0, 0);
  };

  EditorState.prototype.isSelectionAtEndOfContent = function isSelectionAtEndOfContent() {
    var content = this.getCurrentContent();
    var blockMap = content.getBlockMap();
    var last = blockMap.last();
    var end = last.getLength();
    return this.getSelection().hasEdgeWithin(last.getKey(), end, end);
  };

  EditorState.prototype.getDirectionMap = function getDirectionMap() {
    return this.getImmutable().get('directionMap');
  };
  /**
   * Incorporate native DOM selection changes into the EditorState. This
   * method can be used when we simply want to accept whatever the DOM
   * has given us to represent selection, and we do not need to re-render
   * the editor.
   *
   * To forcibly move the DOM selection, see `EditorState.forceSelection`.
   */


  EditorState.acceptSelection = function acceptSelection(editorState, selection) {
    return updateSelection(editorState, selection, false);
  };
  /**
   * At times, we need to force the DOM selection to be where we
   * need it to be. This can occur when the anchor or focus nodes
   * are non-text nodes, for instance. In this case, we want to trigger
   * a re-render of the editor, which in turn forces selection into
   * the correct place in the DOM. The `forceSelection` method
   * accomplishes this.
   *
   * This method should be used in cases where you need to explicitly
   * move the DOM selection from one place to another without a change
   * in ContentState.
   */


  EditorState.forceSelection = function forceSelection(editorState, selection) {
    if (!selection.getHasFocus()) {
      selection = selection.set('hasFocus', true);
    }

    return updateSelection(editorState, selection, true);
  };
  /**
   * Move selection to the end of the editor without forcing focus.
   */


  EditorState.moveSelectionToEnd = function moveSelectionToEnd(editorState) {
    var content = editorState.getCurrentContent();
    var lastBlock = content.getLastBlock();
    var lastKey = lastBlock.getKey();
    var length = lastBlock.getLength();
    return EditorState.acceptSelection(editorState, new SelectionState({
      anchorKey: lastKey,
      anchorOffset: length,
      focusKey: lastKey,
      focusOffset: length,
      isBackward: false
    }));
  };
  /**
   * Force focus to the end of the editor. This is useful in scenarios
   * where we want to programmatically focus the input and it makes sense
   * to allow the user to continue working seamlessly.
   */


  EditorState.moveFocusToEnd = function moveFocusToEnd(editorState) {
    var afterSelectionMove = EditorState.moveSelectionToEnd(editorState);
    return EditorState.forceSelection(afterSelectionMove, afterSelectionMove.getSelection());
  };
  /**
   * Push the current ContentState onto the undo stack if it should be
   * considered a boundary state, and set the provided ContentState as the
   * new current content.
   */


  EditorState.push = function push(editorState, contentState, changeType) {
    if (editorState.getCurrentContent() === contentState) {
      return editorState;
    }

    var forceSelection = changeType !== 'insert-characters';
    var directionMap = EditorBidiService.getDirectionMap(contentState, editorState.getDirectionMap());

    if (!editorState.getAllowUndo()) {
      return EditorState.set(editorState, {
        currentContent: contentState,
        directionMap: directionMap,
        lastChangeType: changeType,
        selection: contentState.getSelectionAfter(),
        forceSelection: forceSelection,
        inlineStyleOverride: null
      });
    }

    var selection = editorState.getSelection();
    var currentContent = editorState.getCurrentContent();
    var undoStack = editorState.getUndoStack();
    var newContent = contentState;

    if (selection !== currentContent.getSelectionAfter() || mustBecomeBoundary(editorState, changeType)) {
      undoStack = undoStack.push(currentContent);
      newContent = newContent.set('selectionBefore', selection);
    } else if (changeType === 'insert-characters' || changeType === 'backspace-character' || changeType === 'delete-character') {
      // Preserve the previous selection.
      newContent = newContent.set('selectionBefore', currentContent.getSelectionBefore());
    }

    var inlineStyleOverride = editorState.getInlineStyleOverride(); // Don't discard inline style overrides for the following change types:

    var overrideChangeTypes = ['adjust-depth', 'change-block-type', 'split-block'];

    if (overrideChangeTypes.indexOf(changeType) === -1) {
      inlineStyleOverride = null;
    }

    var editorStateChanges = {
      currentContent: newContent,
      directionMap: directionMap,
      undoStack: undoStack,
      redoStack: Stack(),
      lastChangeType: changeType,
      selection: contentState.getSelectionAfter(),
      forceSelection: forceSelection,
      inlineStyleOverride: inlineStyleOverride
    };
    return EditorState.set(editorState, editorStateChanges);
  };
  /**
   * Make the top ContentState in the undo stack the new current content and
   * push the current content onto the redo stack.
   */


  EditorState.undo = function undo(editorState) {
    if (!editorState.getAllowUndo()) {
      return editorState;
    }

    var undoStack = editorState.getUndoStack();
    var newCurrentContent = undoStack.peek();

    if (!newCurrentContent) {
      return editorState;
    }

    var currentContent = editorState.getCurrentContent();
    var directionMap = EditorBidiService.getDirectionMap(newCurrentContent, editorState.getDirectionMap());
    return EditorState.set(editorState, {
      currentContent: newCurrentContent,
      directionMap: directionMap,
      undoStack: undoStack.shift(),
      redoStack: editorState.getRedoStack().push(currentContent),
      forceSelection: true,
      inlineStyleOverride: null,
      lastChangeType: 'undo',
      nativelyRenderedContent: null,
      selection: currentContent.getSelectionBefore()
    });
  };
  /**
   * Make the top ContentState in the redo stack the new current content and
   * push the current content onto the undo stack.
   */


  EditorState.redo = function redo(editorState) {
    if (!editorState.getAllowUndo()) {
      return editorState;
    }

    var redoStack = editorState.getRedoStack();
    var newCurrentContent = redoStack.peek();

    if (!newCurrentContent) {
      return editorState;
    }

    var currentContent = editorState.getCurrentContent();
    var directionMap = EditorBidiService.getDirectionMap(newCurrentContent, editorState.getDirectionMap());
    return EditorState.set(editorState, {
      currentContent: newCurrentContent,
      directionMap: directionMap,
      undoStack: editorState.getUndoStack().push(currentContent),
      redoStack: redoStack.shift(),
      forceSelection: true,
      inlineStyleOverride: null,
      lastChangeType: 'redo',
      nativelyRenderedContent: null,
      selection: newCurrentContent.getSelectionAfter()
    });
  };
  /**
   * Not for public consumption.
   */


  function EditorState(immutable) {
    _classCallCheck(this, EditorState);

    this._immutable = immutable;
  }
  /**
   * Not for public consumption.
   */


  EditorState.prototype.getImmutable = function getImmutable() {
    return this._immutable;
  };

  return EditorState;
}();
/**
 * Set the supplied SelectionState as the new current selection, and set
 * the `force` flag to trigger manual selection placement by the view.
 */


function updateSelection(editorState, selection, forceSelection) {
  return EditorState.set(editorState, {
    selection: selection,
    forceSelection: forceSelection,
    nativelyRenderedContent: null,
    inlineStyleOverride: null
  });
}
/**
 * Regenerate the entire tree map for a given ContentState and decorator.
 * Returns an OrderedMap that maps all available ContentBlock objects.
 */


function generateNewTreeMap(contentState, decorator) {
  return contentState.getBlockMap().map(function (block) {
    return BlockTree.generate(contentState, block, decorator);
  }).toOrderedMap();
}
/**
 * Regenerate tree map objects for all ContentBlocks that have changed
 * between the current editorState and newContent. Returns an OrderedMap
 * with only changed regenerated tree map objects.
 */


function regenerateTreeForNewBlocks(editorState, newBlockMap, newEntityMap, decorator) {
  var contentState = editorState.getCurrentContent().set('entityMap', newEntityMap);
  var prevBlockMap = contentState.getBlockMap();
  var prevTreeMap = editorState.getImmutable().get('treeMap');
  return prevTreeMap.merge(newBlockMap.toSeq().filter(function (block, key) {
    return block !== prevBlockMap.get(key);
  }).map(function (block) {
    return BlockTree.generate(contentState, block, decorator);
  }));
}
/**
 * Generate tree map objects for a new decorator object, preserving any
 * decorations that are unchanged from the previous decorator.
 *
 * Note that in order for this to perform optimally, decoration Lists for
 * decorators should be preserved when possible to allow for direct immutable
 * List comparison.
 */


function regenerateTreeForNewDecorator(content, blockMap, previousTreeMap, decorator, existingDecorator) {
  return previousTreeMap.merge(blockMap.toSeq().filter(function (block) {
    return decorator.getDecorations(block, content) !== existingDecorator.getDecorations(block, content);
  }).map(function (block) {
    return BlockTree.generate(content, block, decorator);
  }));
}
/**
 * Return whether a change should be considered a boundary state, given
 * the previous change type. Allows us to discard potential boundary states
 * during standard typing or deletion behavior.
 */


function mustBecomeBoundary(editorState, changeType) {
  var lastChangeType = editorState.getLastChangeType();
  return changeType !== lastChangeType || changeType !== 'insert-characters' && changeType !== 'backspace-character' && changeType !== 'delete-character';
}

function getInlineStyleForCollapsedSelection(content, selection) {
  var startKey = selection.getStartKey();
  var startOffset = selection.getStartOffset();
  var startBlock = content.getBlockForKey(startKey); // If the cursor is not at the start of the block, look backward to
  // preserve the style of the preceding character.

  if (startOffset > 0) {
    return startBlock.getInlineStyleAt(startOffset - 1);
  } // The caret is at position zero in this block. If the block has any
  // text at all, use the style of the first character.


  if (startBlock.getLength()) {
    return startBlock.getInlineStyleAt(0);
  } // Otherwise, look upward in the document to find the closest character.


  return lookUpwardForInlineStyle(content, startKey);
}

function getInlineStyleForNonCollapsedSelection(content, selection) {
  var startKey = selection.getStartKey();
  var startOffset = selection.getStartOffset();
  var startBlock = content.getBlockForKey(startKey); // If there is a character just inside the selection, use its style.

  if (startOffset < startBlock.getLength()) {
    return startBlock.getInlineStyleAt(startOffset);
  } // Check if the selection at the end of a non-empty block. Use the last
  // style in the block.


  if (startOffset > 0) {
    return startBlock.getInlineStyleAt(startOffset - 1);
  } // Otherwise, look upward in the document to find the closest character.


  return lookUpwardForInlineStyle(content, startKey);
}

function lookUpwardForInlineStyle(content, fromKey) {
  var lastNonEmpty = content.getBlockMap().reverse().skipUntil(function (_, k) {
    return k === fromKey;
  }).skip(1).skipUntil(function (block, _) {
    return block.getLength();
  }).first();
  if (lastNonEmpty) return lastNonEmpty.getInlineStyleAt(lastNonEmpty.getLength() - 1);
  return OrderedSet();
}

module.exports = EditorState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0VkaXRvclN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0VkaXRvclN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRWRpdG9yU3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbnZhciBCbG9ja1RyZWUgPSByZXF1aXJlKCcuL0Jsb2NrVHJlZScpO1xudmFyIENvbnRlbnRTdGF0ZSA9IHJlcXVpcmUoJy4vQ29udGVudFN0YXRlJyk7XG52YXIgRWRpdG9yQmlkaVNlcnZpY2UgPSByZXF1aXJlKCcuL0VkaXRvckJpZGlTZXJ2aWNlJyk7XG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XG52YXIgU2VsZWN0aW9uU3RhdGUgPSByZXF1aXJlKCcuL1NlbGVjdGlvblN0YXRlJyk7XG5cbnZhciBPcmRlcmVkU2V0ID0gSW1tdXRhYmxlLk9yZGVyZWRTZXQsXG4gICAgUmVjb3JkID0gSW1tdXRhYmxlLlJlY29yZCxcbiAgICBTdGFjayA9IEltbXV0YWJsZS5TdGFjaztcblxuXG52YXIgZGVmYXVsdFJlY29yZCA9IHtcbiAgYWxsb3dVbmRvOiB0cnVlLFxuICBjdXJyZW50Q29udGVudDogbnVsbCxcbiAgZGVjb3JhdG9yOiBudWxsLFxuICBkaXJlY3Rpb25NYXA6IG51bGwsXG4gIGZvcmNlU2VsZWN0aW9uOiBmYWxzZSxcbiAgaW5Db21wb3NpdGlvbk1vZGU6IGZhbHNlLFxuICBpbmxpbmVTdHlsZU92ZXJyaWRlOiBudWxsLFxuICBsYXN0Q2hhbmdlVHlwZTogbnVsbCxcbiAgbmF0aXZlbHlSZW5kZXJlZENvbnRlbnQ6IG51bGwsXG4gIHJlZG9TdGFjazogU3RhY2soKSxcbiAgc2VsZWN0aW9uOiBudWxsLFxuICB0cmVlTWFwOiBudWxsLFxuICB1bmRvU3RhY2s6IFN0YWNrKClcbn07XG5cbnZhciBFZGl0b3JTdGF0ZVJlY29yZCA9IFJlY29yZChkZWZhdWx0UmVjb3JkKTtcblxudmFyIEVkaXRvclN0YXRlID0gZnVuY3Rpb24gKCkge1xuICBFZGl0b3JTdGF0ZS5jcmVhdGVFbXB0eSA9IGZ1bmN0aW9uIGNyZWF0ZUVtcHR5KGRlY29yYXRvcikge1xuICAgIHJldHVybiBFZGl0b3JTdGF0ZS5jcmVhdGVXaXRoQ29udGVudChDb250ZW50U3RhdGUuY3JlYXRlRnJvbVRleHQoJycpLCBkZWNvcmF0b3IpO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50ID0gZnVuY3Rpb24gY3JlYXRlV2l0aENvbnRlbnQoY29udGVudFN0YXRlLCBkZWNvcmF0b3IpIHtcbiAgICB2YXIgZmlyc3RLZXkgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKS5maXJzdCgpLmdldEtleSgpO1xuICAgIHJldHVybiBFZGl0b3JTdGF0ZS5jcmVhdGUoe1xuICAgICAgY3VycmVudENvbnRlbnQ6IGNvbnRlbnRTdGF0ZSxcbiAgICAgIHVuZG9TdGFjazogU3RhY2soKSxcbiAgICAgIHJlZG9TdGFjazogU3RhY2soKSxcbiAgICAgIGRlY29yYXRvcjogZGVjb3JhdG9yIHx8IG51bGwsXG4gICAgICBzZWxlY3Rpb246IFNlbGVjdGlvblN0YXRlLmNyZWF0ZUVtcHR5KGZpcnN0S2V5KVxuICAgIH0pO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShjb25maWcpIHtcbiAgICB2YXIgY3VycmVudENvbnRlbnQgPSBjb25maWcuY3VycmVudENvbnRlbnQsXG4gICAgICAgIGRlY29yYXRvciA9IGNvbmZpZy5kZWNvcmF0b3I7XG5cbiAgICB2YXIgcmVjb3JkQ29uZmlnID0gX2V4dGVuZHMoe30sIGNvbmZpZywge1xuICAgICAgdHJlZU1hcDogZ2VuZXJhdGVOZXdUcmVlTWFwKGN1cnJlbnRDb250ZW50LCBkZWNvcmF0b3IpLFxuICAgICAgZGlyZWN0aW9uTWFwOiBFZGl0b3JCaWRpU2VydmljZS5nZXREaXJlY3Rpb25NYXAoY3VycmVudENvbnRlbnQpXG4gICAgfSk7XG4gICAgcmV0dXJuIG5ldyBFZGl0b3JTdGF0ZShuZXcgRWRpdG9yU3RhdGVSZWNvcmQocmVjb3JkQ29uZmlnKSk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUuc2V0ID0gZnVuY3Rpb24gc2V0KGVkaXRvclN0YXRlLCBwdXQpIHtcbiAgICB2YXIgbWFwID0gZWRpdG9yU3RhdGUuZ2V0SW1tdXRhYmxlKCkud2l0aE11dGF0aW9ucyhmdW5jdGlvbiAoc3RhdGUpIHtcbiAgICAgIHZhciBleGlzdGluZ0RlY29yYXRvciA9IHN0YXRlLmdldCgnZGVjb3JhdG9yJyk7XG4gICAgICB2YXIgZGVjb3JhdG9yID0gZXhpc3RpbmdEZWNvcmF0b3I7XG4gICAgICBpZiAocHV0LmRlY29yYXRvciA9PT0gbnVsbCkge1xuICAgICAgICBkZWNvcmF0b3IgPSBudWxsO1xuICAgICAgfSBlbHNlIGlmIChwdXQuZGVjb3JhdG9yKSB7XG4gICAgICAgIGRlY29yYXRvciA9IHB1dC5kZWNvcmF0b3I7XG4gICAgICB9XG5cbiAgICAgIHZhciBuZXdDb250ZW50ID0gcHV0LmN1cnJlbnRDb250ZW50IHx8IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG5cbiAgICAgIGlmIChkZWNvcmF0b3IgIT09IGV4aXN0aW5nRGVjb3JhdG9yKSB7XG4gICAgICAgIHZhciB0cmVlTWFwID0gc3RhdGUuZ2V0KCd0cmVlTWFwJyk7XG4gICAgICAgIHZhciBuZXdUcmVlTWFwO1xuICAgICAgICBpZiAoZGVjb3JhdG9yICYmIGV4aXN0aW5nRGVjb3JhdG9yKSB7XG4gICAgICAgICAgbmV3VHJlZU1hcCA9IHJlZ2VuZXJhdGVUcmVlRm9yTmV3RGVjb3JhdG9yKG5ld0NvbnRlbnQsIG5ld0NvbnRlbnQuZ2V0QmxvY2tNYXAoKSwgdHJlZU1hcCwgZGVjb3JhdG9yLCBleGlzdGluZ0RlY29yYXRvcik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbmV3VHJlZU1hcCA9IGdlbmVyYXRlTmV3VHJlZU1hcChuZXdDb250ZW50LCBkZWNvcmF0b3IpO1xuICAgICAgICB9XG5cbiAgICAgICAgc3RhdGUubWVyZ2Uoe1xuICAgICAgICAgIGRlY29yYXRvcjogZGVjb3JhdG9yLFxuICAgICAgICAgIHRyZWVNYXA6IG5ld1RyZWVNYXAsXG4gICAgICAgICAgbmF0aXZlbHlSZW5kZXJlZENvbnRlbnQ6IG51bGxcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGV4aXN0aW5nQ29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgICBpZiAobmV3Q29udGVudCAhPT0gZXhpc3RpbmdDb250ZW50KSB7XG4gICAgICAgIHN0YXRlLnNldCgndHJlZU1hcCcsIHJlZ2VuZXJhdGVUcmVlRm9yTmV3QmxvY2tzKGVkaXRvclN0YXRlLCBuZXdDb250ZW50LmdldEJsb2NrTWFwKCksIG5ld0NvbnRlbnQuZ2V0RW50aXR5TWFwKCksIGRlY29yYXRvcikpO1xuICAgICAgfVxuXG4gICAgICBzdGF0ZS5tZXJnZShwdXQpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIG5ldyBFZGl0b3JTdGF0ZShtYXApO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS50b0pTID0gZnVuY3Rpb24gdG9KUygpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJbW11dGFibGUoKS50b0pTKCk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmdldEFsbG93VW5kbyA9IGZ1bmN0aW9uIGdldEFsbG93VW5kbygpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJbW11dGFibGUoKS5nZXQoJ2FsbG93VW5kbycpO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS5nZXRDdXJyZW50Q29udGVudCA9IGZ1bmN0aW9uIGdldEN1cnJlbnRDb250ZW50KCkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldCgnY3VycmVudENvbnRlbnQnKTtcbiAgfTtcblxuICBFZGl0b3JTdGF0ZS5wcm90b3R5cGUuZ2V0VW5kb1N0YWNrID0gZnVuY3Rpb24gZ2V0VW5kb1N0YWNrKCkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldCgndW5kb1N0YWNrJyk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmdldFJlZG9TdGFjayA9IGZ1bmN0aW9uIGdldFJlZG9TdGFjaygpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJbW11dGFibGUoKS5nZXQoJ3JlZG9TdGFjaycpO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS5nZXRTZWxlY3Rpb24gPSBmdW5jdGlvbiBnZXRTZWxlY3Rpb24oKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0SW1tdXRhYmxlKCkuZ2V0KCdzZWxlY3Rpb24nKTtcbiAgfTtcblxuICBFZGl0b3JTdGF0ZS5wcm90b3R5cGUuZ2V0RGVjb3JhdG9yID0gZnVuY3Rpb24gZ2V0RGVjb3JhdG9yKCkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldCgnZGVjb3JhdG9yJyk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmlzSW5Db21wb3NpdGlvbk1vZGUgPSBmdW5jdGlvbiBpc0luQ29tcG9zaXRpb25Nb2RlKCkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldCgnaW5Db21wb3NpdGlvbk1vZGUnKTtcbiAgfTtcblxuICBFZGl0b3JTdGF0ZS5wcm90b3R5cGUubXVzdEZvcmNlU2VsZWN0aW9uID0gZnVuY3Rpb24gbXVzdEZvcmNlU2VsZWN0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldCgnZm9yY2VTZWxlY3Rpb24nKTtcbiAgfTtcblxuICBFZGl0b3JTdGF0ZS5wcm90b3R5cGUuZ2V0TmF0aXZlbHlSZW5kZXJlZENvbnRlbnQgPSBmdW5jdGlvbiBnZXROYXRpdmVseVJlbmRlcmVkQ29udGVudCgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJbW11dGFibGUoKS5nZXQoJ25hdGl2ZWx5UmVuZGVyZWRDb250ZW50Jyk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmdldExhc3RDaGFuZ2VUeXBlID0gZnVuY3Rpb24gZ2V0TGFzdENoYW5nZVR5cGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0SW1tdXRhYmxlKCkuZ2V0KCdsYXN0Q2hhbmdlVHlwZScpO1xuICB9O1xuXG4gIC8qKlxuICAgKiBXaGlsZSBlZGl0aW5nLCB0aGUgdXNlciBtYXkgYXBwbHkgaW5saW5lIHN0eWxlIGNvbW1hbmRzIHdpdGggYSBjb2xsYXBzZWRcbiAgICogY3Vyc29yLCBpbnRlbmRpbmcgdG8gdHlwZSB0ZXh0IHRoYXQgYWRvcHRzIHRoZSBzcGVjaWZpZWQgc3R5bGUuIEluIHRoaXNcbiAgICogY2FzZSwgd2UgdHJhY2sgdGhlIHNwZWNpZmllZCBzdHlsZSBhcyBhbiBcIm92ZXJyaWRlXCIgdGhhdCB0YWtlcyBwcmVjZWRlbmNlXG4gICAqIG92ZXIgdGhlIGlubGluZSBzdHlsZSBvZiB0aGUgdGV4dCBhZGphY2VudCB0byB0aGUgY3Vyc29yLlxuICAgKlxuICAgKiBJZiBudWxsLCB0aGVyZSBpcyBubyBvdmVycmlkZSBpbiBwbGFjZS5cbiAgICovXG5cblxuICBFZGl0b3JTdGF0ZS5wcm90b3R5cGUuZ2V0SW5saW5lU3R5bGVPdmVycmlkZSA9IGZ1bmN0aW9uIGdldElubGluZVN0eWxlT3ZlcnJpZGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0SW1tdXRhYmxlKCkuZ2V0KCdpbmxpbmVTdHlsZU92ZXJyaWRlJyk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUuc2V0SW5saW5lU3R5bGVPdmVycmlkZSA9IGZ1bmN0aW9uIHNldElubGluZVN0eWxlT3ZlcnJpZGUoZWRpdG9yU3RhdGUsIGlubGluZVN0eWxlT3ZlcnJpZGUpIHtcbiAgICByZXR1cm4gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7IGlubGluZVN0eWxlT3ZlcnJpZGU6IGlubGluZVN0eWxlT3ZlcnJpZGUgfSk7XG4gIH07XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgYXBwcm9wcmlhdGUgaW5saW5lIHN0eWxlIGZvciB0aGUgZWRpdG9yIHN0YXRlLiBJZiBhblxuICAgKiBvdmVycmlkZSBpcyBpbiBwbGFjZSwgdXNlIGl0LiBPdGhlcndpc2UsIHRoZSBjdXJyZW50IHN0eWxlIGlzXG4gICAqIGJhc2VkIG9uIHRoZSBsb2NhdGlvbiBvZiB0aGUgc2VsZWN0aW9uIHN0YXRlLlxuICAgKi9cblxuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS5nZXRDdXJyZW50SW5saW5lU3R5bGUgPSBmdW5jdGlvbiBnZXRDdXJyZW50SW5saW5lU3R5bGUoKSB7XG4gICAgdmFyIG92ZXJyaWRlID0gdGhpcy5nZXRJbmxpbmVTdHlsZU92ZXJyaWRlKCk7XG4gICAgaWYgKG92ZXJyaWRlICE9IG51bGwpIHtcbiAgICAgIHJldHVybiBvdmVycmlkZTtcbiAgICB9XG5cbiAgICB2YXIgY29udGVudCA9IHRoaXMuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgc2VsZWN0aW9uID0gdGhpcy5nZXRTZWxlY3Rpb24oKTtcblxuICAgIGlmIChzZWxlY3Rpb24uaXNDb2xsYXBzZWQoKSkge1xuICAgICAgcmV0dXJuIGdldElubGluZVN0eWxlRm9yQ29sbGFwc2VkU2VsZWN0aW9uKGNvbnRlbnQsIHNlbGVjdGlvbik7XG4gICAgfVxuXG4gICAgcmV0dXJuIGdldElubGluZVN0eWxlRm9yTm9uQ29sbGFwc2VkU2VsZWN0aW9uKGNvbnRlbnQsIHNlbGVjdGlvbik7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmdldEJsb2NrVHJlZSA9IGZ1bmN0aW9uIGdldEJsb2NrVHJlZShibG9ja0tleSkge1xuICAgIHJldHVybiB0aGlzLmdldEltbXV0YWJsZSgpLmdldEluKFsndHJlZU1hcCcsIGJsb2NrS2V5XSk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmlzU2VsZWN0aW9uQXRTdGFydE9mQ29udGVudCA9IGZ1bmN0aW9uIGlzU2VsZWN0aW9uQXRTdGFydE9mQ29udGVudCgpIHtcbiAgICB2YXIgZmlyc3RLZXkgPSB0aGlzLmdldEN1cnJlbnRDb250ZW50KCkuZ2V0QmxvY2tNYXAoKS5maXJzdCgpLmdldEtleSgpO1xuICAgIHJldHVybiB0aGlzLmdldFNlbGVjdGlvbigpLmhhc0VkZ2VXaXRoaW4oZmlyc3RLZXksIDAsIDApO1xuICB9O1xuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS5pc1NlbGVjdGlvbkF0RW5kT2ZDb250ZW50ID0gZnVuY3Rpb24gaXNTZWxlY3Rpb25BdEVuZE9mQ29udGVudCgpIHtcbiAgICB2YXIgY29udGVudCA9IHRoaXMuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgYmxvY2tNYXAgPSBjb250ZW50LmdldEJsb2NrTWFwKCk7XG4gICAgdmFyIGxhc3QgPSBibG9ja01hcC5sYXN0KCk7XG4gICAgdmFyIGVuZCA9IGxhc3QuZ2V0TGVuZ3RoKCk7XG4gICAgcmV0dXJuIHRoaXMuZ2V0U2VsZWN0aW9uKCkuaGFzRWRnZVdpdGhpbihsYXN0LmdldEtleSgpLCBlbmQsIGVuZCk7XG4gIH07XG5cbiAgRWRpdG9yU3RhdGUucHJvdG90eXBlLmdldERpcmVjdGlvbk1hcCA9IGZ1bmN0aW9uIGdldERpcmVjdGlvbk1hcCgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJbW11dGFibGUoKS5nZXQoJ2RpcmVjdGlvbk1hcCcpO1xuICB9O1xuXG4gIC8qKlxuICAgKiBJbmNvcnBvcmF0ZSBuYXRpdmUgRE9NIHNlbGVjdGlvbiBjaGFuZ2VzIGludG8gdGhlIEVkaXRvclN0YXRlLiBUaGlzXG4gICAqIG1ldGhvZCBjYW4gYmUgdXNlZCB3aGVuIHdlIHNpbXBseSB3YW50IHRvIGFjY2VwdCB3aGF0ZXZlciB0aGUgRE9NXG4gICAqIGhhcyBnaXZlbiB1cyB0byByZXByZXNlbnQgc2VsZWN0aW9uLCBhbmQgd2UgZG8gbm90IG5lZWQgdG8gcmUtcmVuZGVyXG4gICAqIHRoZSBlZGl0b3IuXG4gICAqXG4gICAqIFRvIGZvcmNpYmx5IG1vdmUgdGhlIERPTSBzZWxlY3Rpb24sIHNlZSBgRWRpdG9yU3RhdGUuZm9yY2VTZWxlY3Rpb25gLlxuICAgKi9cblxuXG4gIEVkaXRvclN0YXRlLmFjY2VwdFNlbGVjdGlvbiA9IGZ1bmN0aW9uIGFjY2VwdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgc2VsZWN0aW9uKSB7XG4gICAgcmV0dXJuIHVwZGF0ZVNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgc2VsZWN0aW9uLCBmYWxzZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqIEF0IHRpbWVzLCB3ZSBuZWVkIHRvIGZvcmNlIHRoZSBET00gc2VsZWN0aW9uIHRvIGJlIHdoZXJlIHdlXG4gICAqIG5lZWQgaXQgdG8gYmUuIFRoaXMgY2FuIG9jY3VyIHdoZW4gdGhlIGFuY2hvciBvciBmb2N1cyBub2Rlc1xuICAgKiBhcmUgbm9uLXRleHQgbm9kZXMsIGZvciBpbnN0YW5jZS4gSW4gdGhpcyBjYXNlLCB3ZSB3YW50IHRvIHRyaWdnZXJcbiAgICogYSByZS1yZW5kZXIgb2YgdGhlIGVkaXRvciwgd2hpY2ggaW4gdHVybiBmb3JjZXMgc2VsZWN0aW9uIGludG9cbiAgICogdGhlIGNvcnJlY3QgcGxhY2UgaW4gdGhlIERPTS4gVGhlIGBmb3JjZVNlbGVjdGlvbmAgbWV0aG9kXG4gICAqIGFjY29tcGxpc2hlcyB0aGlzLlxuICAgKlxuICAgKiBUaGlzIG1ldGhvZCBzaG91bGQgYmUgdXNlZCBpbiBjYXNlcyB3aGVyZSB5b3UgbmVlZCB0byBleHBsaWNpdGx5XG4gICAqIG1vdmUgdGhlIERPTSBzZWxlY3Rpb24gZnJvbSBvbmUgcGxhY2UgdG8gYW5vdGhlciB3aXRob3V0IGEgY2hhbmdlXG4gICAqIGluIENvbnRlbnRTdGF0ZS5cbiAgICovXG5cblxuICBFZGl0b3JTdGF0ZS5mb3JjZVNlbGVjdGlvbiA9IGZ1bmN0aW9uIGZvcmNlU2VsZWN0aW9uKGVkaXRvclN0YXRlLCBzZWxlY3Rpb24pIHtcbiAgICBpZiAoIXNlbGVjdGlvbi5nZXRIYXNGb2N1cygpKSB7XG4gICAgICBzZWxlY3Rpb24gPSBzZWxlY3Rpb24uc2V0KCdoYXNGb2N1cycsIHRydWUpO1xuICAgIH1cbiAgICByZXR1cm4gdXBkYXRlU2VsZWN0aW9uKGVkaXRvclN0YXRlLCBzZWxlY3Rpb24sIHRydWUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiBNb3ZlIHNlbGVjdGlvbiB0byB0aGUgZW5kIG9mIHRoZSBlZGl0b3Igd2l0aG91dCBmb3JjaW5nIGZvY3VzLlxuICAgKi9cblxuXG4gIEVkaXRvclN0YXRlLm1vdmVTZWxlY3Rpb25Ub0VuZCA9IGZ1bmN0aW9uIG1vdmVTZWxlY3Rpb25Ub0VuZChlZGl0b3JTdGF0ZSkge1xuICAgIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgbGFzdEJsb2NrID0gY29udGVudC5nZXRMYXN0QmxvY2soKTtcbiAgICB2YXIgbGFzdEtleSA9IGxhc3RCbG9jay5nZXRLZXkoKTtcbiAgICB2YXIgbGVuZ3RoID0gbGFzdEJsb2NrLmdldExlbmd0aCgpO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLmFjY2VwdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgbmV3IFNlbGVjdGlvblN0YXRlKHtcbiAgICAgIGFuY2hvcktleTogbGFzdEtleSxcbiAgICAgIGFuY2hvck9mZnNldDogbGVuZ3RoLFxuICAgICAgZm9jdXNLZXk6IGxhc3RLZXksXG4gICAgICBmb2N1c09mZnNldDogbGVuZ3RoLFxuICAgICAgaXNCYWNrd2FyZDogZmFsc2VcbiAgICB9KSk7XG4gIH07XG5cbiAgLyoqXG4gICAqIEZvcmNlIGZvY3VzIHRvIHRoZSBlbmQgb2YgdGhlIGVkaXRvci4gVGhpcyBpcyB1c2VmdWwgaW4gc2NlbmFyaW9zXG4gICAqIHdoZXJlIHdlIHdhbnQgdG8gcHJvZ3JhbW1hdGljYWxseSBmb2N1cyB0aGUgaW5wdXQgYW5kIGl0IG1ha2VzIHNlbnNlXG4gICAqIHRvIGFsbG93IHRoZSB1c2VyIHRvIGNvbnRpbnVlIHdvcmtpbmcgc2VhbWxlc3NseS5cbiAgICovXG5cblxuICBFZGl0b3JTdGF0ZS5tb3ZlRm9jdXNUb0VuZCA9IGZ1bmN0aW9uIG1vdmVGb2N1c1RvRW5kKGVkaXRvclN0YXRlKSB7XG4gICAgdmFyIGFmdGVyU2VsZWN0aW9uTW92ZSA9IEVkaXRvclN0YXRlLm1vdmVTZWxlY3Rpb25Ub0VuZChlZGl0b3JTdGF0ZSk7XG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKGFmdGVyU2VsZWN0aW9uTW92ZSwgYWZ0ZXJTZWxlY3Rpb25Nb3ZlLmdldFNlbGVjdGlvbigpKTtcbiAgfTtcblxuICAvKipcbiAgICogUHVzaCB0aGUgY3VycmVudCBDb250ZW50U3RhdGUgb250byB0aGUgdW5kbyBzdGFjayBpZiBpdCBzaG91bGQgYmVcbiAgICogY29uc2lkZXJlZCBhIGJvdW5kYXJ5IHN0YXRlLCBhbmQgc2V0IHRoZSBwcm92aWRlZCBDb250ZW50U3RhdGUgYXMgdGhlXG4gICAqIG5ldyBjdXJyZW50IGNvbnRlbnQuXG4gICAqL1xuXG5cbiAgRWRpdG9yU3RhdGUucHVzaCA9IGZ1bmN0aW9uIHB1c2goZWRpdG9yU3RhdGUsIGNvbnRlbnRTdGF0ZSwgY2hhbmdlVHlwZSkge1xuICAgIGlmIChlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpID09PSBjb250ZW50U3RhdGUpIHtcbiAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgICB9XG5cbiAgICB2YXIgZm9yY2VTZWxlY3Rpb24gPSBjaGFuZ2VUeXBlICE9PSAnaW5zZXJ0LWNoYXJhY3RlcnMnO1xuICAgIHZhciBkaXJlY3Rpb25NYXAgPSBFZGl0b3JCaWRpU2VydmljZS5nZXREaXJlY3Rpb25NYXAoY29udGVudFN0YXRlLCBlZGl0b3JTdGF0ZS5nZXREaXJlY3Rpb25NYXAoKSk7XG5cbiAgICBpZiAoIWVkaXRvclN0YXRlLmdldEFsbG93VW5kbygpKSB7XG4gICAgICByZXR1cm4gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7XG4gICAgICAgIGN1cnJlbnRDb250ZW50OiBjb250ZW50U3RhdGUsXG4gICAgICAgIGRpcmVjdGlvbk1hcDogZGlyZWN0aW9uTWFwLFxuICAgICAgICBsYXN0Q2hhbmdlVHlwZTogY2hhbmdlVHlwZSxcbiAgICAgICAgc2VsZWN0aW9uOiBjb250ZW50U3RhdGUuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSxcbiAgICAgICAgZm9yY2VTZWxlY3Rpb246IGZvcmNlU2VsZWN0aW9uLFxuICAgICAgICBpbmxpbmVTdHlsZU92ZXJyaWRlOiBudWxsXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gICAgdmFyIGN1cnJlbnRDb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICB2YXIgdW5kb1N0YWNrID0gZWRpdG9yU3RhdGUuZ2V0VW5kb1N0YWNrKCk7XG4gICAgdmFyIG5ld0NvbnRlbnQgPSBjb250ZW50U3RhdGU7XG5cbiAgICBpZiAoc2VsZWN0aW9uICE9PSBjdXJyZW50Q29udGVudC5nZXRTZWxlY3Rpb25BZnRlcigpIHx8IG11c3RCZWNvbWVCb3VuZGFyeShlZGl0b3JTdGF0ZSwgY2hhbmdlVHlwZSkpIHtcbiAgICAgIHVuZG9TdGFjayA9IHVuZG9TdGFjay5wdXNoKGN1cnJlbnRDb250ZW50KTtcbiAgICAgIG5ld0NvbnRlbnQgPSBuZXdDb250ZW50LnNldCgnc2VsZWN0aW9uQmVmb3JlJywgc2VsZWN0aW9uKTtcbiAgICB9IGVsc2UgaWYgKGNoYW5nZVR5cGUgPT09ICdpbnNlcnQtY2hhcmFjdGVycycgfHwgY2hhbmdlVHlwZSA9PT0gJ2JhY2tzcGFjZS1jaGFyYWN0ZXInIHx8IGNoYW5nZVR5cGUgPT09ICdkZWxldGUtY2hhcmFjdGVyJykge1xuICAgICAgLy8gUHJlc2VydmUgdGhlIHByZXZpb3VzIHNlbGVjdGlvbi5cbiAgICAgIG5ld0NvbnRlbnQgPSBuZXdDb250ZW50LnNldCgnc2VsZWN0aW9uQmVmb3JlJywgY3VycmVudENvbnRlbnQuZ2V0U2VsZWN0aW9uQmVmb3JlKCkpO1xuICAgIH1cblxuICAgIHZhciBpbmxpbmVTdHlsZU92ZXJyaWRlID0gZWRpdG9yU3RhdGUuZ2V0SW5saW5lU3R5bGVPdmVycmlkZSgpO1xuXG4gICAgLy8gRG9uJ3QgZGlzY2FyZCBpbmxpbmUgc3R5bGUgb3ZlcnJpZGVzIGZvciB0aGUgZm9sbG93aW5nIGNoYW5nZSB0eXBlczpcbiAgICB2YXIgb3ZlcnJpZGVDaGFuZ2VUeXBlcyA9IFsnYWRqdXN0LWRlcHRoJywgJ2NoYW5nZS1ibG9jay10eXBlJywgJ3NwbGl0LWJsb2NrJ107XG5cbiAgICBpZiAob3ZlcnJpZGVDaGFuZ2VUeXBlcy5pbmRleE9mKGNoYW5nZVR5cGUpID09PSAtMSkge1xuICAgICAgaW5saW5lU3R5bGVPdmVycmlkZSA9IG51bGw7XG4gICAgfVxuXG4gICAgdmFyIGVkaXRvclN0YXRlQ2hhbmdlcyA9IHtcbiAgICAgIGN1cnJlbnRDb250ZW50OiBuZXdDb250ZW50LFxuICAgICAgZGlyZWN0aW9uTWFwOiBkaXJlY3Rpb25NYXAsXG4gICAgICB1bmRvU3RhY2s6IHVuZG9TdGFjayxcbiAgICAgIHJlZG9TdGFjazogU3RhY2soKSxcbiAgICAgIGxhc3RDaGFuZ2VUeXBlOiBjaGFuZ2VUeXBlLFxuICAgICAgc2VsZWN0aW9uOiBjb250ZW50U3RhdGUuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSxcbiAgICAgIGZvcmNlU2VsZWN0aW9uOiBmb3JjZVNlbGVjdGlvbixcbiAgICAgIGlubGluZVN0eWxlT3ZlcnJpZGU6IGlubGluZVN0eWxlT3ZlcnJpZGVcbiAgICB9O1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnNldChlZGl0b3JTdGF0ZSwgZWRpdG9yU3RhdGVDaGFuZ2VzKTtcbiAgfTtcblxuICAvKipcbiAgICogTWFrZSB0aGUgdG9wIENvbnRlbnRTdGF0ZSBpbiB0aGUgdW5kbyBzdGFjayB0aGUgbmV3IGN1cnJlbnQgY29udGVudCBhbmRcbiAgICogcHVzaCB0aGUgY3VycmVudCBjb250ZW50IG9udG8gdGhlIHJlZG8gc3RhY2suXG4gICAqL1xuXG5cbiAgRWRpdG9yU3RhdGUudW5kbyA9IGZ1bmN0aW9uIHVuZG8oZWRpdG9yU3RhdGUpIHtcbiAgICBpZiAoIWVkaXRvclN0YXRlLmdldEFsbG93VW5kbygpKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgdmFyIHVuZG9TdGFjayA9IGVkaXRvclN0YXRlLmdldFVuZG9TdGFjaygpO1xuICAgIHZhciBuZXdDdXJyZW50Q29udGVudCA9IHVuZG9TdGFjay5wZWVrKCk7XG4gICAgaWYgKCFuZXdDdXJyZW50Q29udGVudCkge1xuICAgICAgcmV0dXJuIGVkaXRvclN0YXRlO1xuICAgIH1cblxuICAgIHZhciBjdXJyZW50Q29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIGRpcmVjdGlvbk1hcCA9IEVkaXRvckJpZGlTZXJ2aWNlLmdldERpcmVjdGlvbk1hcChuZXdDdXJyZW50Q29udGVudCwgZWRpdG9yU3RhdGUuZ2V0RGlyZWN0aW9uTWFwKCkpO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnNldChlZGl0b3JTdGF0ZSwge1xuICAgICAgY3VycmVudENvbnRlbnQ6IG5ld0N1cnJlbnRDb250ZW50LFxuICAgICAgZGlyZWN0aW9uTWFwOiBkaXJlY3Rpb25NYXAsXG4gICAgICB1bmRvU3RhY2s6IHVuZG9TdGFjay5zaGlmdCgpLFxuICAgICAgcmVkb1N0YWNrOiBlZGl0b3JTdGF0ZS5nZXRSZWRvU3RhY2soKS5wdXNoKGN1cnJlbnRDb250ZW50KSxcbiAgICAgIGZvcmNlU2VsZWN0aW9uOiB0cnVlLFxuICAgICAgaW5saW5lU3R5bGVPdmVycmlkZTogbnVsbCxcbiAgICAgIGxhc3RDaGFuZ2VUeXBlOiAndW5kbycsXG4gICAgICBuYXRpdmVseVJlbmRlcmVkQ29udGVudDogbnVsbCxcbiAgICAgIHNlbGVjdGlvbjogY3VycmVudENvbnRlbnQuZ2V0U2VsZWN0aW9uQmVmb3JlKClcbiAgICB9KTtcbiAgfTtcblxuICAvKipcbiAgICogTWFrZSB0aGUgdG9wIENvbnRlbnRTdGF0ZSBpbiB0aGUgcmVkbyBzdGFjayB0aGUgbmV3IGN1cnJlbnQgY29udGVudCBhbmRcbiAgICogcHVzaCB0aGUgY3VycmVudCBjb250ZW50IG9udG8gdGhlIHVuZG8gc3RhY2suXG4gICAqL1xuXG5cbiAgRWRpdG9yU3RhdGUucmVkbyA9IGZ1bmN0aW9uIHJlZG8oZWRpdG9yU3RhdGUpIHtcbiAgICBpZiAoIWVkaXRvclN0YXRlLmdldEFsbG93VW5kbygpKSB7XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfVxuXG4gICAgdmFyIHJlZG9TdGFjayA9IGVkaXRvclN0YXRlLmdldFJlZG9TdGFjaygpO1xuICAgIHZhciBuZXdDdXJyZW50Q29udGVudCA9IHJlZG9TdGFjay5wZWVrKCk7XG4gICAgaWYgKCFuZXdDdXJyZW50Q29udGVudCkge1xuICAgICAgcmV0dXJuIGVkaXRvclN0YXRlO1xuICAgIH1cblxuICAgIHZhciBjdXJyZW50Q29udGVudCA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIGRpcmVjdGlvbk1hcCA9IEVkaXRvckJpZGlTZXJ2aWNlLmdldERpcmVjdGlvbk1hcChuZXdDdXJyZW50Q29udGVudCwgZWRpdG9yU3RhdGUuZ2V0RGlyZWN0aW9uTWFwKCkpO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnNldChlZGl0b3JTdGF0ZSwge1xuICAgICAgY3VycmVudENvbnRlbnQ6IG5ld0N1cnJlbnRDb250ZW50LFxuICAgICAgZGlyZWN0aW9uTWFwOiBkaXJlY3Rpb25NYXAsXG4gICAgICB1bmRvU3RhY2s6IGVkaXRvclN0YXRlLmdldFVuZG9TdGFjaygpLnB1c2goY3VycmVudENvbnRlbnQpLFxuICAgICAgcmVkb1N0YWNrOiByZWRvU3RhY2suc2hpZnQoKSxcbiAgICAgIGZvcmNlU2VsZWN0aW9uOiB0cnVlLFxuICAgICAgaW5saW5lU3R5bGVPdmVycmlkZTogbnVsbCxcbiAgICAgIGxhc3RDaGFuZ2VUeXBlOiAncmVkbycsXG4gICAgICBuYXRpdmVseVJlbmRlcmVkQ29udGVudDogbnVsbCxcbiAgICAgIHNlbGVjdGlvbjogbmV3Q3VycmVudENvbnRlbnQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKVxuICAgIH0pO1xuICB9O1xuXG4gIC8qKlxuICAgKiBOb3QgZm9yIHB1YmxpYyBjb25zdW1wdGlvbi5cbiAgICovXG5cblxuICBmdW5jdGlvbiBFZGl0b3JTdGF0ZShpbW11dGFibGUpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRWRpdG9yU3RhdGUpO1xuXG4gICAgdGhpcy5faW1tdXRhYmxlID0gaW1tdXRhYmxlO1xuICB9XG5cbiAgLyoqXG4gICAqIE5vdCBmb3IgcHVibGljIGNvbnN1bXB0aW9uLlxuICAgKi9cblxuXG4gIEVkaXRvclN0YXRlLnByb3RvdHlwZS5nZXRJbW11dGFibGUgPSBmdW5jdGlvbiBnZXRJbW11dGFibGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2ltbXV0YWJsZTtcbiAgfTtcblxuICByZXR1cm4gRWRpdG9yU3RhdGU7XG59KCk7XG5cbi8qKlxuICogU2V0IHRoZSBzdXBwbGllZCBTZWxlY3Rpb25TdGF0ZSBhcyB0aGUgbmV3IGN1cnJlbnQgc2VsZWN0aW9uLCBhbmQgc2V0XG4gKiB0aGUgYGZvcmNlYCBmbGFnIHRvIHRyaWdnZXIgbWFudWFsIHNlbGVjdGlvbiBwbGFjZW1lbnQgYnkgdGhlIHZpZXcuXG4gKi9cblxuXG5mdW5jdGlvbiB1cGRhdGVTZWxlY3Rpb24oZWRpdG9yU3RhdGUsIHNlbGVjdGlvbiwgZm9yY2VTZWxlY3Rpb24pIHtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnNldChlZGl0b3JTdGF0ZSwge1xuICAgIHNlbGVjdGlvbjogc2VsZWN0aW9uLFxuICAgIGZvcmNlU2VsZWN0aW9uOiBmb3JjZVNlbGVjdGlvbixcbiAgICBuYXRpdmVseVJlbmRlcmVkQ29udGVudDogbnVsbCxcbiAgICBpbmxpbmVTdHlsZU92ZXJyaWRlOiBudWxsXG4gIH0pO1xufVxuXG4vKipcbiAqIFJlZ2VuZXJhdGUgdGhlIGVudGlyZSB0cmVlIG1hcCBmb3IgYSBnaXZlbiBDb250ZW50U3RhdGUgYW5kIGRlY29yYXRvci5cbiAqIFJldHVybnMgYW4gT3JkZXJlZE1hcCB0aGF0IG1hcHMgYWxsIGF2YWlsYWJsZSBDb250ZW50QmxvY2sgb2JqZWN0cy5cbiAqL1xuZnVuY3Rpb24gZ2VuZXJhdGVOZXdUcmVlTWFwKGNvbnRlbnRTdGF0ZSwgZGVjb3JhdG9yKSB7XG4gIHJldHVybiBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKS5tYXAoZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgcmV0dXJuIEJsb2NrVHJlZS5nZW5lcmF0ZShjb250ZW50U3RhdGUsIGJsb2NrLCBkZWNvcmF0b3IpO1xuICB9KS50b09yZGVyZWRNYXAoKTtcbn1cblxuLyoqXG4gKiBSZWdlbmVyYXRlIHRyZWUgbWFwIG9iamVjdHMgZm9yIGFsbCBDb250ZW50QmxvY2tzIHRoYXQgaGF2ZSBjaGFuZ2VkXG4gKiBiZXR3ZWVuIHRoZSBjdXJyZW50IGVkaXRvclN0YXRlIGFuZCBuZXdDb250ZW50LiBSZXR1cm5zIGFuIE9yZGVyZWRNYXBcbiAqIHdpdGggb25seSBjaGFuZ2VkIHJlZ2VuZXJhdGVkIHRyZWUgbWFwIG9iamVjdHMuXG4gKi9cbmZ1bmN0aW9uIHJlZ2VuZXJhdGVUcmVlRm9yTmV3QmxvY2tzKGVkaXRvclN0YXRlLCBuZXdCbG9ja01hcCwgbmV3RW50aXR5TWFwLCBkZWNvcmF0b3IpIHtcbiAgdmFyIGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCkuc2V0KCdlbnRpdHlNYXAnLCBuZXdFbnRpdHlNYXApO1xuICB2YXIgcHJldkJsb2NrTWFwID0gY29udGVudFN0YXRlLmdldEJsb2NrTWFwKCk7XG4gIHZhciBwcmV2VHJlZU1hcCA9IGVkaXRvclN0YXRlLmdldEltbXV0YWJsZSgpLmdldCgndHJlZU1hcCcpO1xuICByZXR1cm4gcHJldlRyZWVNYXAubWVyZ2UobmV3QmxvY2tNYXAudG9TZXEoKS5maWx0ZXIoZnVuY3Rpb24gKGJsb2NrLCBrZXkpIHtcbiAgICByZXR1cm4gYmxvY2sgIT09IHByZXZCbG9ja01hcC5nZXQoa2V5KTtcbiAgfSkubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgIHJldHVybiBCbG9ja1RyZWUuZ2VuZXJhdGUoY29udGVudFN0YXRlLCBibG9jaywgZGVjb3JhdG9yKTtcbiAgfSkpO1xufVxuXG4vKipcbiAqIEdlbmVyYXRlIHRyZWUgbWFwIG9iamVjdHMgZm9yIGEgbmV3IGRlY29yYXRvciBvYmplY3QsIHByZXNlcnZpbmcgYW55XG4gKiBkZWNvcmF0aW9ucyB0aGF0IGFyZSB1bmNoYW5nZWQgZnJvbSB0aGUgcHJldmlvdXMgZGVjb3JhdG9yLlxuICpcbiAqIE5vdGUgdGhhdCBpbiBvcmRlciBmb3IgdGhpcyB0byBwZXJmb3JtIG9wdGltYWxseSwgZGVjb3JhdGlvbiBMaXN0cyBmb3JcbiAqIGRlY29yYXRvcnMgc2hvdWxkIGJlIHByZXNlcnZlZCB3aGVuIHBvc3NpYmxlIHRvIGFsbG93IGZvciBkaXJlY3QgaW1tdXRhYmxlXG4gKiBMaXN0IGNvbXBhcmlzb24uXG4gKi9cbmZ1bmN0aW9uIHJlZ2VuZXJhdGVUcmVlRm9yTmV3RGVjb3JhdG9yKGNvbnRlbnQsIGJsb2NrTWFwLCBwcmV2aW91c1RyZWVNYXAsIGRlY29yYXRvciwgZXhpc3RpbmdEZWNvcmF0b3IpIHtcbiAgcmV0dXJuIHByZXZpb3VzVHJlZU1hcC5tZXJnZShibG9ja01hcC50b1NlcSgpLmZpbHRlcihmdW5jdGlvbiAoYmxvY2spIHtcbiAgICByZXR1cm4gZGVjb3JhdG9yLmdldERlY29yYXRpb25zKGJsb2NrLCBjb250ZW50KSAhPT0gZXhpc3RpbmdEZWNvcmF0b3IuZ2V0RGVjb3JhdGlvbnMoYmxvY2ssIGNvbnRlbnQpO1xuICB9KS5tYXAoZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgcmV0dXJuIEJsb2NrVHJlZS5nZW5lcmF0ZShjb250ZW50LCBibG9jaywgZGVjb3JhdG9yKTtcbiAgfSkpO1xufVxuXG4vKipcbiAqIFJldHVybiB3aGV0aGVyIGEgY2hhbmdlIHNob3VsZCBiZSBjb25zaWRlcmVkIGEgYm91bmRhcnkgc3RhdGUsIGdpdmVuXG4gKiB0aGUgcHJldmlvdXMgY2hhbmdlIHR5cGUuIEFsbG93cyB1cyB0byBkaXNjYXJkIHBvdGVudGlhbCBib3VuZGFyeSBzdGF0ZXNcbiAqIGR1cmluZyBzdGFuZGFyZCB0eXBpbmcgb3IgZGVsZXRpb24gYmVoYXZpb3IuXG4gKi9cbmZ1bmN0aW9uIG11c3RCZWNvbWVCb3VuZGFyeShlZGl0b3JTdGF0ZSwgY2hhbmdlVHlwZSkge1xuICB2YXIgbGFzdENoYW5nZVR5cGUgPSBlZGl0b3JTdGF0ZS5nZXRMYXN0Q2hhbmdlVHlwZSgpO1xuICByZXR1cm4gY2hhbmdlVHlwZSAhPT0gbGFzdENoYW5nZVR5cGUgfHwgY2hhbmdlVHlwZSAhPT0gJ2luc2VydC1jaGFyYWN0ZXJzJyAmJiBjaGFuZ2VUeXBlICE9PSAnYmFja3NwYWNlLWNoYXJhY3RlcicgJiYgY2hhbmdlVHlwZSAhPT0gJ2RlbGV0ZS1jaGFyYWN0ZXInO1xufVxuXG5mdW5jdGlvbiBnZXRJbmxpbmVTdHlsZUZvckNvbGxhcHNlZFNlbGVjdGlvbihjb250ZW50LCBzZWxlY3Rpb24pIHtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uLmdldFN0YXJ0S2V5KCk7XG4gIHZhciBzdGFydE9mZnNldCA9IHNlbGVjdGlvbi5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgc3RhcnRCbG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tGb3JLZXkoc3RhcnRLZXkpO1xuXG4gIC8vIElmIHRoZSBjdXJzb3IgaXMgbm90IGF0IHRoZSBzdGFydCBvZiB0aGUgYmxvY2ssIGxvb2sgYmFja3dhcmQgdG9cbiAgLy8gcHJlc2VydmUgdGhlIHN0eWxlIG9mIHRoZSBwcmVjZWRpbmcgY2hhcmFjdGVyLlxuICBpZiAoc3RhcnRPZmZzZXQgPiAwKSB7XG4gICAgcmV0dXJuIHN0YXJ0QmxvY2suZ2V0SW5saW5lU3R5bGVBdChzdGFydE9mZnNldCAtIDEpO1xuICB9XG5cbiAgLy8gVGhlIGNhcmV0IGlzIGF0IHBvc2l0aW9uIHplcm8gaW4gdGhpcyBibG9jay4gSWYgdGhlIGJsb2NrIGhhcyBhbnlcbiAgLy8gdGV4dCBhdCBhbGwsIHVzZSB0aGUgc3R5bGUgb2YgdGhlIGZpcnN0IGNoYXJhY3Rlci5cbiAgaWYgKHN0YXJ0QmxvY2suZ2V0TGVuZ3RoKCkpIHtcbiAgICByZXR1cm4gc3RhcnRCbG9jay5nZXRJbmxpbmVTdHlsZUF0KDApO1xuICB9XG5cbiAgLy8gT3RoZXJ3aXNlLCBsb29rIHVwd2FyZCBpbiB0aGUgZG9jdW1lbnQgdG8gZmluZCB0aGUgY2xvc2VzdCBjaGFyYWN0ZXIuXG4gIHJldHVybiBsb29rVXB3YXJkRm9ySW5saW5lU3R5bGUoY29udGVudCwgc3RhcnRLZXkpO1xufVxuXG5mdW5jdGlvbiBnZXRJbmxpbmVTdHlsZUZvck5vbkNvbGxhcHNlZFNlbGVjdGlvbihjb250ZW50LCBzZWxlY3Rpb24pIHtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uLmdldFN0YXJ0S2V5KCk7XG4gIHZhciBzdGFydE9mZnNldCA9IHNlbGVjdGlvbi5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgc3RhcnRCbG9jayA9IGNvbnRlbnQuZ2V0QmxvY2tGb3JLZXkoc3RhcnRLZXkpO1xuXG4gIC8vIElmIHRoZXJlIGlzIGEgY2hhcmFjdGVyIGp1c3QgaW5zaWRlIHRoZSBzZWxlY3Rpb24sIHVzZSBpdHMgc3R5bGUuXG4gIGlmIChzdGFydE9mZnNldCA8IHN0YXJ0QmxvY2suZ2V0TGVuZ3RoKCkpIHtcbiAgICByZXR1cm4gc3RhcnRCbG9jay5nZXRJbmxpbmVTdHlsZUF0KHN0YXJ0T2Zmc2V0KTtcbiAgfVxuXG4gIC8vIENoZWNrIGlmIHRoZSBzZWxlY3Rpb24gYXQgdGhlIGVuZCBvZiBhIG5vbi1lbXB0eSBibG9jay4gVXNlIHRoZSBsYXN0XG4gIC8vIHN0eWxlIGluIHRoZSBibG9jay5cbiAgaWYgKHN0YXJ0T2Zmc2V0ID4gMCkge1xuICAgIHJldHVybiBzdGFydEJsb2NrLmdldElubGluZVN0eWxlQXQoc3RhcnRPZmZzZXQgLSAxKTtcbiAgfVxuXG4gIC8vIE90aGVyd2lzZSwgbG9vayB1cHdhcmQgaW4gdGhlIGRvY3VtZW50IHRvIGZpbmQgdGhlIGNsb3Nlc3QgY2hhcmFjdGVyLlxuICByZXR1cm4gbG9va1Vwd2FyZEZvcklubGluZVN0eWxlKGNvbnRlbnQsIHN0YXJ0S2V5KTtcbn1cblxuZnVuY3Rpb24gbG9va1Vwd2FyZEZvcklubGluZVN0eWxlKGNvbnRlbnQsIGZyb21LZXkpIHtcbiAgdmFyIGxhc3ROb25FbXB0eSA9IGNvbnRlbnQuZ2V0QmxvY2tNYXAoKS5yZXZlcnNlKCkuc2tpcFVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IGZyb21LZXk7XG4gIH0pLnNraXAoMSkuc2tpcFVudGlsKGZ1bmN0aW9uIChibG9jaywgXykge1xuICAgIHJldHVybiBibG9jay5nZXRMZW5ndGgoKTtcbiAgfSkuZmlyc3QoKTtcblxuICBpZiAobGFzdE5vbkVtcHR5KSByZXR1cm4gbGFzdE5vbkVtcHR5LmdldElubGluZVN0eWxlQXQobGFzdE5vbkVtcHR5LmdldExlbmd0aCgpIC0gMSk7XG4gIHJldHVybiBPcmRlcmVkU2V0KCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gRWRpdG9yU3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUVBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFFQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7QUFDQTtBQUVBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFFQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/EditorState.js
