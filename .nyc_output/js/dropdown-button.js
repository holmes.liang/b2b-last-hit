__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DropdownButton; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../button */ "./node_modules/antd/es/button/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dropdown */ "./node_modules/antd/es/dropdown/dropdown.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};







var ButtonGroup = _button__WEBPACK_IMPORTED_MODULE_2__["default"].Group;

var DropdownButton =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DropdownButton, _React$Component);

  function DropdownButton() {
    var _this;

    _classCallCheck(this, DropdownButton);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DropdownButton).apply(this, arguments));

    _this.renderButton = function (_ref) {
      var getContextPopupContainer = _ref.getPopupContainer,
          getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          type = _a.type,
          disabled = _a.disabled,
          onClick = _a.onClick,
          htmlType = _a.htmlType,
          children = _a.children,
          className = _a.className,
          overlay = _a.overlay,
          trigger = _a.trigger,
          align = _a.align,
          visible = _a.visible,
          onVisibleChange = _a.onVisibleChange,
          placement = _a.placement,
          getPopupContainer = _a.getPopupContainer,
          href = _a.href,
          _a$icon = _a.icon,
          icon = _a$icon === void 0 ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: "ellipsis"
      }) : _a$icon,
          title = _a.title,
          restProps = __rest(_a, ["prefixCls", "type", "disabled", "onClick", "htmlType", "children", "className", "overlay", "trigger", "align", "visible", "onVisibleChange", "placement", "getPopupContainer", "href", "icon", "title"]);

      var prefixCls = getPrefixCls('dropdown-button', customizePrefixCls);
      var dropdownProps = {
        align: align,
        overlay: overlay,
        disabled: disabled,
        trigger: disabled ? [] : trigger,
        onVisibleChange: onVisibleChange,
        placement: placement,
        getPopupContainer: getPopupContainer || getContextPopupContainer
      };

      if ('visible' in _this.props) {
        dropdownProps.visible = visible;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](ButtonGroup, _extends({}, restProps, {
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className)
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_button__WEBPACK_IMPORTED_MODULE_2__["default"], {
        type: type,
        disabled: disabled,
        onClick: onClick,
        htmlType: htmlType,
        href: href,
        title: title
      }, children), react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_dropdown__WEBPACK_IMPORTED_MODULE_4__["default"], dropdownProps, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_button__WEBPACK_IMPORTED_MODULE_2__["default"], {
        type: type
      }, icon)));
    };

    return _this;
  }

  _createClass(DropdownButton, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"], null, this.renderButton);
    }
  }]);

  return DropdownButton;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


DropdownButton.defaultProps = {
  placement: 'bottomRight',
  type: 'default'
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9kcm9wZG93bi9kcm9wZG93bi1idXR0b24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2Ryb3Bkb3duL2Ryb3Bkb3duLWJ1dHRvbi5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJy4uL2J1dHRvbic7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgRHJvcGRvd24gZnJvbSAnLi9kcm9wZG93bic7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmNvbnN0IEJ1dHRvbkdyb3VwID0gQnV0dG9uLkdyb3VwO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRHJvcGRvd25CdXR0b24gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnJlbmRlckJ1dHRvbiA9ICh7IGdldFBvcHVwQ29udGFpbmVyOiBnZXRDb250ZXh0UG9wdXBDb250YWluZXIsIGdldFByZWZpeENscywgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCB0eXBlLCBkaXNhYmxlZCwgb25DbGljaywgaHRtbFR5cGUsIGNoaWxkcmVuLCBjbGFzc05hbWUsIG92ZXJsYXksIHRyaWdnZXIsIGFsaWduLCB2aXNpYmxlLCBvblZpc2libGVDaGFuZ2UsIHBsYWNlbWVudCwgZ2V0UG9wdXBDb250YWluZXIsIGhyZWYsIGljb24gPSA8SWNvbiB0eXBlPVwiZWxsaXBzaXNcIi8+LCB0aXRsZSB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwidHlwZVwiLCBcImRpc2FibGVkXCIsIFwib25DbGlja1wiLCBcImh0bWxUeXBlXCIsIFwiY2hpbGRyZW5cIiwgXCJjbGFzc05hbWVcIiwgXCJvdmVybGF5XCIsIFwidHJpZ2dlclwiLCBcImFsaWduXCIsIFwidmlzaWJsZVwiLCBcIm9uVmlzaWJsZUNoYW5nZVwiLCBcInBsYWNlbWVudFwiLCBcImdldFBvcHVwQ29udGFpbmVyXCIsIFwiaHJlZlwiLCBcImljb25cIiwgXCJ0aXRsZVwiXSk7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2Ryb3Bkb3duLWJ1dHRvbicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBkcm9wZG93blByb3BzID0ge1xuICAgICAgICAgICAgICAgIGFsaWduLFxuICAgICAgICAgICAgICAgIG92ZXJsYXksXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgdHJpZ2dlcjogZGlzYWJsZWQgPyBbXSA6IHRyaWdnZXIsXG4gICAgICAgICAgICAgICAgb25WaXNpYmxlQ2hhbmdlLFxuICAgICAgICAgICAgICAgIHBsYWNlbWVudCxcbiAgICAgICAgICAgICAgICBnZXRQb3B1cENvbnRhaW5lcjogZ2V0UG9wdXBDb250YWluZXIgfHwgZ2V0Q29udGV4dFBvcHVwQ29udGFpbmVyLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmICgndmlzaWJsZScgaW4gdGhpcy5wcm9wcykge1xuICAgICAgICAgICAgICAgIGRyb3Bkb3duUHJvcHMudmlzaWJsZSA9IHZpc2libGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gKDxCdXR0b25Hcm91cCB7Li4ucmVzdFByb3BzfSBjbGFzc05hbWU9e2NsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUpfT5cbiAgICAgICAgPEJ1dHRvbiB0eXBlPXt0eXBlfSBkaXNhYmxlZD17ZGlzYWJsZWR9IG9uQ2xpY2s9e29uQ2xpY2t9IGh0bWxUeXBlPXtodG1sVHlwZX0gaHJlZj17aHJlZn0gdGl0bGU9e3RpdGxlfT5cbiAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgICA8RHJvcGRvd24gey4uLmRyb3Bkb3duUHJvcHN9PlxuICAgICAgICAgIDxCdXR0b24gdHlwZT17dHlwZX0+e2ljb259PC9CdXR0b24+XG4gICAgICAgIDwvRHJvcGRvd24+XG4gICAgICA8L0J1dHRvbkdyb3VwPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJCdXR0b259PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuRHJvcGRvd25CdXR0b24uZGVmYXVsdFByb3BzID0ge1xuICAgIHBsYWNlbWVudDogJ2JvdHRvbVJpZ2h0JyxcbiAgICB0eXBlOiAnZGVmYXVsdCcsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQXBCQTtBQUNBO0FBSEE7QUEwQkE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7O0FBOUJBO0FBQ0E7QUFEQTtBQWdDQTtBQUNBO0FBQ0E7QUFGQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/dropdown/dropdown-button.js
