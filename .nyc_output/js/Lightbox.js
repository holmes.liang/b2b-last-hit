

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _aphrodite = __webpack_require__(/*! aphrodite */ "./node_modules/aphrodite/lib/index.js");

var _reactScrolllock = __webpack_require__(/*! react-scrolllock */ "./node_modules/react-scrolllock/dist/index.js");

var _reactScrolllock2 = _interopRequireDefault(_reactScrolllock);

var _theme = __webpack_require__(/*! ./theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _Arrow = __webpack_require__(/*! ./components/Arrow */ "./node_modules/react-images/lib/components/Arrow.js");

var _Arrow2 = _interopRequireDefault(_Arrow);

var _Container = __webpack_require__(/*! ./components/Container */ "./node_modules/react-images/lib/components/Container.js");

var _Container2 = _interopRequireDefault(_Container);

var _Footer = __webpack_require__(/*! ./components/Footer */ "./node_modules/react-images/lib/components/Footer.js");

var _Footer2 = _interopRequireDefault(_Footer);

var _Header = __webpack_require__(/*! ./components/Header */ "./node_modules/react-images/lib/components/Header.js");

var _Header2 = _interopRequireDefault(_Header);

var _PaginatedThumbnails = __webpack_require__(/*! ./components/PaginatedThumbnails */ "./node_modules/react-images/lib/components/PaginatedThumbnails.js");

var _PaginatedThumbnails2 = _interopRequireDefault(_PaginatedThumbnails);

var _Portal = __webpack_require__(/*! ./components/Portal */ "./node_modules/react-images/lib/components/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

var _Spinner = __webpack_require__(/*! ./components/Spinner */ "./node_modules/react-images/lib/components/Spinner.js");

var _Spinner2 = _interopRequireDefault(_Spinner);

var _bindFunctions = __webpack_require__(/*! ./utils/bindFunctions */ "./node_modules/react-images/lib/utils/bindFunctions.js");

var _bindFunctions2 = _interopRequireDefault(_bindFunctions);

var _canUseDom = __webpack_require__(/*! ./utils/canUseDom */ "./node_modules/react-images/lib/utils/canUseDom.js");

var _canUseDom2 = _interopRequireDefault(_canUseDom);

var _deepMerge = __webpack_require__(/*! ./utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
} // consumers sometimes provide incorrect type or casing


function normalizeSourceSet(data) {
  var sourceSet = data.srcSet || data.srcset;

  if (Array.isArray(sourceSet)) {
    return sourceSet.join();
  }

  return sourceSet;
}

var Lightbox = function (_Component) {
  _inherits(Lightbox, _Component);

  function Lightbox(props) {
    _classCallCheck(this, Lightbox);

    var _this = _possibleConstructorReturn(this, (Lightbox.__proto__ || Object.getPrototypeOf(Lightbox)).call(this, props));

    _this.theme = (0, _deepMerge2.default)(_theme2.default, props.theme);
    _this.classes = _aphrodite.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, _this.theme));
    _this.state = {
      imageLoaded: false
    };

    _bindFunctions2.default.call(_this, ['gotoNext', 'gotoPrev', 'closeBackdrop', 'handleKeyboardInput', 'handleImageLoaded']);

    return _this;
  }

  _createClass(Lightbox, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        theme: this.theme
      };
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.isOpen) {
        if (this.props.enableKeyboardInput) {
          window.addEventListener('keydown', this.handleKeyboardInput);
        }

        if (typeof this.props.currentImage === 'number') {
          this.preloadImage(this.props.currentImage, this.handleImageLoaded);
        }
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (!_canUseDom2.default) return; // preload images

      if (nextProps.preloadNextImage) {
        var currentIndex = this.props.currentImage;
        var nextIndex = nextProps.currentImage + 1;
        var prevIndex = nextProps.currentImage - 1;
        var preloadIndex = void 0;

        if (currentIndex && nextProps.currentImage > currentIndex) {
          preloadIndex = nextIndex;
        } else if (currentIndex && nextProps.currentImage < currentIndex) {
          preloadIndex = prevIndex;
        } // if we know the user's direction just get one image
        // otherwise, to be safe, we need to grab one in each direction


        if (preloadIndex) {
          this.preloadImage(preloadIndex);
        } else {
          this.preloadImage(prevIndex);
          this.preloadImage(nextIndex);
        }
      } // preload current image


      if (this.props.currentImage !== nextProps.currentImage || !this.props.isOpen && nextProps.isOpen) {
        var img = this.preloadImageData(nextProps.images[nextProps.currentImage], this.handleImageLoaded);
        if (img) this.setState({
          imageLoaded: img.complete
        });
      } // add/remove event listeners


      if (!this.props.isOpen && nextProps.isOpen && nextProps.enableKeyboardInput) {
        window.addEventListener('keydown', this.handleKeyboardInput);
      }

      if (!nextProps.isOpen && nextProps.enableKeyboardInput) {
        window.removeEventListener('keydown', this.handleKeyboardInput);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.enableKeyboardInput) {
        window.removeEventListener('keydown', this.handleKeyboardInput);
      }
    } // ==============================
    // METHODS
    // ==============================

  }, {
    key: 'preloadImage',
    value: function preloadImage(idx, onload) {
      return this.preloadImageData(this.props.images[idx], onload);
    }
  }, {
    key: 'preloadImageData',
    value: function preloadImageData(data, onload) {
      if (!data) return;
      var img = new Image();
      var sourceSet = normalizeSourceSet(data); // TODO: add error handling for missing images

      img.onerror = onload;
      img.onload = onload;
      img.src = data.src;
      if (sourceSet) img.srcset = sourceSet;
      return img;
    }
  }, {
    key: 'gotoNext',
    value: function gotoNext(event) {
      var _props = this.props,
          currentImage = _props.currentImage,
          images = _props.images;
      var imageLoaded = this.state.imageLoaded;
      if (!imageLoaded || currentImage === images.length - 1) return;

      if (event) {
        event.preventDefault();
        event.stopPropagation();
      }

      this.props.onClickNext();
    }
  }, {
    key: 'gotoPrev',
    value: function gotoPrev(event) {
      var currentImage = this.props.currentImage;
      var imageLoaded = this.state.imageLoaded;
      if (!imageLoaded || currentImage === 0) return;

      if (event) {
        event.preventDefault();
        event.stopPropagation();
      }

      this.props.onClickPrev();
    }
  }, {
    key: 'closeBackdrop',
    value: function closeBackdrop(event) {
      // make sure event only happens if they click the backdrop
      // and if the caption is widening the figure element let that respond too
      if (event.target.id === 'lightboxBackdrop' || event.target.tagName === 'FIGURE') {
        this.props.onClose();
      }
    }
  }, {
    key: 'handleKeyboardInput',
    value: function handleKeyboardInput(event) {
      if (event.keyCode === 37) {
        // left
        this.gotoPrev(event);
        return true;
      } else if (event.keyCode === 39) {
        // right
        this.gotoNext(event);
        return true;
      } else if (event.keyCode === 27) {
        // esc
        this.props.onClose();
        return true;
      }

      return false;
    }
  }, {
    key: 'handleImageLoaded',
    value: function handleImageLoaded() {
      this.setState({
        imageLoaded: true
      });
    } // ==============================
    // RENDERERS
    // ==============================

  }, {
    key: 'renderArrowPrev',
    value: function renderArrowPrev() {
      if (this.props.currentImage === 0) return null;
      return _react2.default.createElement(_Arrow2.default, {
        direction: 'left',
        icon: 'arrowLeft',
        onClick: this.gotoPrev,
        title: this.props.leftArrowTitle,
        type: 'button'
      });
    }
  }, {
    key: 'renderArrowNext',
    value: function renderArrowNext() {
      if (this.props.currentImage === this.props.images.length - 1) return null;
      return _react2.default.createElement(_Arrow2.default, {
        direction: 'right',
        icon: 'arrowRight',
        onClick: this.gotoNext,
        title: this.props.rightArrowTitle,
        type: 'button'
      });
    }
  }, {
    key: 'renderDialog',
    value: function renderDialog() {
      var _props2 = this.props,
          backdropClosesModal = _props2.backdropClosesModal,
          isOpen = _props2.isOpen,
          showThumbnails = _props2.showThumbnails,
          width = _props2.width;
      var imageLoaded = this.state.imageLoaded;
      if (!isOpen) return _react2.default.createElement('span', {
        key: 'closed'
      });
      var offsetThumbnails = 0;

      if (showThumbnails) {
        offsetThumbnails = this.theme.thumbnail.size + this.theme.container.gutter.vertical;
      }

      return _react2.default.createElement(_Container2.default, {
        key: 'open',
        onClick: backdropClosesModal && this.closeBackdrop,
        onTouchEnd: backdropClosesModal && this.closeBackdrop
      }, _react2.default.createElement('div', null, _react2.default.createElement('div', {
        className: (0, _aphrodite.css)(this.classes.content),
        style: {
          marginBottom: offsetThumbnails,
          maxWidth: width
        }
      }, imageLoaded && this.renderHeader(), this.renderImages(), this.renderSpinner(), imageLoaded && this.renderFooter()), imageLoaded && this.renderThumbnails(), imageLoaded && this.renderArrowPrev(), imageLoaded && this.renderArrowNext(), this.props.preventScroll && _react2.default.createElement(_reactScrolllock2.default, null)));
    }
  }, {
    key: 'renderImages',
    value: function renderImages() {
      var _props3 = this.props,
          currentImage = _props3.currentImage,
          images = _props3.images,
          onClickImage = _props3.onClickImage,
          showThumbnails = _props3.showThumbnails;
      var imageLoaded = this.state.imageLoaded;
      if (!images || !images.length) return null;
      var image = images[currentImage];
      var sourceSet = normalizeSourceSet(image);
      var sizes = sourceSet ? '100vw' : null;
      var thumbnailsSize = showThumbnails ? this.theme.thumbnail.size : 0;
      var heightOffset = this.theme.header.height + this.theme.footer.height + thumbnailsSize + this.theme.container.gutter.vertical + 'px';
      return _react2.default.createElement('figure', {
        className: (0, _aphrodite.css)(this.classes.figure)
      }, _react2.default.createElement('img', {
        className: (0, _aphrodite.css)(this.classes.image, imageLoaded && this.classes.imageLoaded),
        onClick: onClickImage,
        sizes: sizes,
        alt: image.alt,
        src: image.src,
        srcSet: sourceSet,
        style: {
          cursor: onClickImage ? 'pointer' : 'auto',
          maxHeight: 'calc(100vh - ' + heightOffset + ')'
        }
      }));
    }
  }, {
    key: 'renderThumbnails',
    value: function renderThumbnails() {
      var _props4 = this.props,
          images = _props4.images,
          currentImage = _props4.currentImage,
          onClickThumbnail = _props4.onClickThumbnail,
          showThumbnails = _props4.showThumbnails,
          thumbnailOffset = _props4.thumbnailOffset;
      if (!showThumbnails) return;
      return _react2.default.createElement(_PaginatedThumbnails2.default, {
        currentImage: currentImage,
        images: images,
        offset: thumbnailOffset,
        onClickThumbnail: onClickThumbnail
      });
    }
  }, {
    key: 'renderHeader',
    value: function renderHeader() {
      var _props5 = this.props,
          closeButtonTitle = _props5.closeButtonTitle,
          customControls = _props5.customControls,
          onClose = _props5.onClose,
          showCloseButton = _props5.showCloseButton;
      return _react2.default.createElement(_Header2.default, {
        customControls: customControls,
        onClose: onClose,
        showCloseButton: showCloseButton,
        closeButtonTitle: closeButtonTitle
      });
    }
  }, {
    key: 'renderFooter',
    value: function renderFooter() {
      var _props6 = this.props,
          currentImage = _props6.currentImage,
          images = _props6.images,
          imageCountSeparator = _props6.imageCountSeparator,
          showImageCount = _props6.showImageCount;
      if (!images || !images.length) return null;
      return _react2.default.createElement(_Footer2.default, {
        caption: images[currentImage].caption,
        countCurrent: currentImage + 1,
        countSeparator: imageCountSeparator,
        countTotal: images.length,
        showCount: showImageCount
      });
    }
  }, {
    key: 'renderSpinner',
    value: function renderSpinner() {
      var _props7 = this.props,
          spinner = _props7.spinner,
          spinnerColor = _props7.spinnerColor,
          spinnerSize = _props7.spinnerSize;
      var imageLoaded = this.state.imageLoaded;
      var Spinner = spinner;
      return _react2.default.createElement('div', {
        className: (0, _aphrodite.css)(this.classes.spinner, !imageLoaded && this.classes.spinnerActive)
      }, _react2.default.createElement(Spinner, {
        color: spinnerColor,
        size: spinnerSize
      }));
    }
  }, {
    key: 'render',
    value: function render() {
      return _react2.default.createElement(_Portal2.default, null, this.renderDialog());
    }
  }]);

  return Lightbox;
}(_react.Component);

Lightbox.propTypes = {
  backdropClosesModal: _propTypes2.default.bool,
  closeButtonTitle: _propTypes2.default.string,
  currentImage: _propTypes2.default.number,
  customControls: _propTypes2.default.arrayOf(_propTypes2.default.node),
  enableKeyboardInput: _propTypes2.default.bool,
  imageCountSeparator: _propTypes2.default.string,
  images: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    src: _propTypes2.default.string.isRequired,
    srcSet: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.arrayOf(_propTypes2.default.string)]),
    caption: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.element]),
    thumbnail: _propTypes2.default.string
  })).isRequired,
  isOpen: _propTypes2.default.bool,
  leftArrowTitle: _propTypes2.default.string,
  onClickImage: _propTypes2.default.func,
  onClickNext: _propTypes2.default.func,
  onClickPrev: _propTypes2.default.func,
  onClose: _propTypes2.default.func.isRequired,
  preloadNextImage: _propTypes2.default.bool,
  preventScroll: _propTypes2.default.bool,
  rightArrowTitle: _propTypes2.default.string,
  showCloseButton: _propTypes2.default.bool,
  showImageCount: _propTypes2.default.bool,
  showThumbnails: _propTypes2.default.bool,
  spinner: _propTypes2.default.func,
  spinnerColor: _propTypes2.default.string,
  spinnerSize: _propTypes2.default.number,
  theme: _propTypes2.default.object,
  thumbnailOffset: _propTypes2.default.number,
  width: _propTypes2.default.number
};
Lightbox.defaultProps = {
  closeButtonTitle: 'Close (Esc)',
  currentImage: 0,
  enableKeyboardInput: true,
  imageCountSeparator: ' of ',
  leftArrowTitle: 'Previous (Left arrow key)',
  onClickShowNextImage: true,
  preloadNextImage: true,
  preventScroll: true,
  rightArrowTitle: 'Next (Right arrow key)',
  showCloseButton: true,
  showImageCount: true,
  spinner: _Spinner2.default,
  spinnerColor: 'white',
  spinnerSize: 100,
  theme: {},
  thumbnailOffset: 2,
  width: 1024
};
Lightbox.childContextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  content: {
    position: 'relative'
  },
  figure: {
    margin: 0 // remove browser default

  },
  image: {
    display: 'block',
    // removes browser default gutter
    height: 'auto',
    margin: '0 auto',
    // maintain center on very short screens OR very narrow image
    maxWidth: '100%',
    // disable user select
    WebkitTouchCallout: 'none',
    userSelect: 'none',
    // opacity animation on image load
    opacity: 0,
    transition: 'opacity 0.3s'
  },
  imageLoaded: {
    opacity: 1
  },
  spinner: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    // opacity animation to make spinner appear with delay
    opacity: 0,
    transition: 'opacity 0.3s',
    pointerEvents: 'none'
  },
  spinnerActive: {
    opacity: 1
  }
};
exports.default = Lightbox;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9MaWdodGJveC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvTGlnaHRib3guanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2FwaHJvZGl0ZSA9IHJlcXVpcmUoJ2FwaHJvZGl0ZScpO1xuXG52YXIgX3JlYWN0U2Nyb2xsbG9jayA9IHJlcXVpcmUoJ3JlYWN0LXNjcm9sbGxvY2snKTtcblxudmFyIF9yZWFjdFNjcm9sbGxvY2syID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3RTY3JvbGxsb2NrKTtcblxudmFyIF90aGVtZSA9IHJlcXVpcmUoJy4vdGhlbWUnKTtcblxudmFyIF90aGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aGVtZSk7XG5cbnZhciBfQXJyb3cgPSByZXF1aXJlKCcuL2NvbXBvbmVudHMvQXJyb3cnKTtcblxudmFyIF9BcnJvdzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BcnJvdyk7XG5cbnZhciBfQ29udGFpbmVyID0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0NvbnRhaW5lcicpO1xuXG52YXIgX0NvbnRhaW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Db250YWluZXIpO1xuXG52YXIgX0Zvb3RlciA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9Gb290ZXInKTtcblxudmFyIF9Gb290ZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRm9vdGVyKTtcblxudmFyIF9IZWFkZXIgPSByZXF1aXJlKCcuL2NvbXBvbmVudHMvSGVhZGVyJyk7XG5cbnZhciBfSGVhZGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0hlYWRlcik7XG5cbnZhciBfUGFnaW5hdGVkVGh1bWJuYWlscyA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9QYWdpbmF0ZWRUaHVtYm5haWxzJyk7XG5cbnZhciBfUGFnaW5hdGVkVGh1bWJuYWlsczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYWdpbmF0ZWRUaHVtYm5haWxzKTtcblxudmFyIF9Qb3J0YWwgPSByZXF1aXJlKCcuL2NvbXBvbmVudHMvUG9ydGFsJyk7XG5cbnZhciBfUG9ydGFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BvcnRhbCk7XG5cbnZhciBfU3Bpbm5lciA9IHJlcXVpcmUoJy4vY29tcG9uZW50cy9TcGlubmVyJyk7XG5cbnZhciBfU3Bpbm5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TcGlubmVyKTtcblxudmFyIF9iaW5kRnVuY3Rpb25zID0gcmVxdWlyZSgnLi91dGlscy9iaW5kRnVuY3Rpb25zJyk7XG5cbnZhciBfYmluZEZ1bmN0aW9uczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9iaW5kRnVuY3Rpb25zKTtcblxudmFyIF9jYW5Vc2VEb20gPSByZXF1aXJlKCcuL3V0aWxzL2NhblVzZURvbScpO1xuXG52YXIgX2NhblVzZURvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jYW5Vc2VEb20pO1xuXG52YXIgX2RlZXBNZXJnZSA9IHJlcXVpcmUoJy4vdXRpbHMvZGVlcE1lcmdlJyk7XG5cbnZhciBfZGVlcE1lcmdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZXBNZXJnZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxuLy8gY29uc3VtZXJzIHNvbWV0aW1lcyBwcm92aWRlIGluY29ycmVjdCB0eXBlIG9yIGNhc2luZ1xuZnVuY3Rpb24gbm9ybWFsaXplU291cmNlU2V0KGRhdGEpIHtcblx0dmFyIHNvdXJjZVNldCA9IGRhdGEuc3JjU2V0IHx8IGRhdGEuc3Jjc2V0O1xuXG5cdGlmIChBcnJheS5pc0FycmF5KHNvdXJjZVNldCkpIHtcblx0XHRyZXR1cm4gc291cmNlU2V0LmpvaW4oKTtcblx0fVxuXG5cdHJldHVybiBzb3VyY2VTZXQ7XG59XG5cbnZhciBMaWdodGJveCA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG5cdF9pbmhlcml0cyhMaWdodGJveCwgX0NvbXBvbmVudCk7XG5cblx0ZnVuY3Rpb24gTGlnaHRib3gocHJvcHMpIHtcblx0XHRfY2xhc3NDYWxsQ2hlY2sodGhpcywgTGlnaHRib3gpO1xuXG5cdFx0dmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKExpZ2h0Ym94Ll9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoTGlnaHRib3gpKS5jYWxsKHRoaXMsIHByb3BzKSk7XG5cblx0XHRfdGhpcy50aGVtZSA9ICgwLCBfZGVlcE1lcmdlMi5kZWZhdWx0KShfdGhlbWUyLmRlZmF1bHQsIHByb3BzLnRoZW1lKTtcblx0XHRfdGhpcy5jbGFzc2VzID0gX2FwaHJvZGl0ZS5TdHlsZVNoZWV0LmNyZWF0ZSgoMCwgX2RlZXBNZXJnZTIuZGVmYXVsdCkoZGVmYXVsdFN0eWxlcywgX3RoaXMudGhlbWUpKTtcblx0XHRfdGhpcy5zdGF0ZSA9IHsgaW1hZ2VMb2FkZWQ6IGZhbHNlIH07XG5cblx0XHRfYmluZEZ1bmN0aW9uczIuZGVmYXVsdC5jYWxsKF90aGlzLCBbJ2dvdG9OZXh0JywgJ2dvdG9QcmV2JywgJ2Nsb3NlQmFja2Ryb3AnLCAnaGFuZGxlS2V5Ym9hcmRJbnB1dCcsICdoYW5kbGVJbWFnZUxvYWRlZCddKTtcblx0XHRyZXR1cm4gX3RoaXM7XG5cdH1cblxuXHRfY3JlYXRlQ2xhc3MoTGlnaHRib3gsIFt7XG5cdFx0a2V5OiAnZ2V0Q2hpbGRDb250ZXh0Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ2V0Q2hpbGRDb250ZXh0KCkge1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0dGhlbWU6IHRoaXMudGhlbWVcblx0XHRcdH07XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcblx0XHRcdGlmICh0aGlzLnByb3BzLmlzT3Blbikge1xuXHRcdFx0XHRpZiAodGhpcy5wcm9wcy5lbmFibGVLZXlib2FyZElucHV0KSB7XG5cdFx0XHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLmhhbmRsZUtleWJvYXJkSW5wdXQpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICh0eXBlb2YgdGhpcy5wcm9wcy5jdXJyZW50SW1hZ2UgPT09ICdudW1iZXInKSB7XG5cdFx0XHRcdFx0dGhpcy5wcmVsb2FkSW1hZ2UodGhpcy5wcm9wcy5jdXJyZW50SW1hZ2UsIHRoaXMuaGFuZGxlSW1hZ2VMb2FkZWQpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcycsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzKSB7XG5cdFx0XHRpZiAoIV9jYW5Vc2VEb20yLmRlZmF1bHQpIHJldHVybjtcblxuXHRcdFx0Ly8gcHJlbG9hZCBpbWFnZXNcblx0XHRcdGlmIChuZXh0UHJvcHMucHJlbG9hZE5leHRJbWFnZSkge1xuXHRcdFx0XHR2YXIgY3VycmVudEluZGV4ID0gdGhpcy5wcm9wcy5jdXJyZW50SW1hZ2U7XG5cdFx0XHRcdHZhciBuZXh0SW5kZXggPSBuZXh0UHJvcHMuY3VycmVudEltYWdlICsgMTtcblx0XHRcdFx0dmFyIHByZXZJbmRleCA9IG5leHRQcm9wcy5jdXJyZW50SW1hZ2UgLSAxO1xuXHRcdFx0XHR2YXIgcHJlbG9hZEluZGV4ID0gdm9pZCAwO1xuXG5cdFx0XHRcdGlmIChjdXJyZW50SW5kZXggJiYgbmV4dFByb3BzLmN1cnJlbnRJbWFnZSA+IGN1cnJlbnRJbmRleCkge1xuXHRcdFx0XHRcdHByZWxvYWRJbmRleCA9IG5leHRJbmRleDtcblx0XHRcdFx0fSBlbHNlIGlmIChjdXJyZW50SW5kZXggJiYgbmV4dFByb3BzLmN1cnJlbnRJbWFnZSA8IGN1cnJlbnRJbmRleCkge1xuXHRcdFx0XHRcdHByZWxvYWRJbmRleCA9IHByZXZJbmRleDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIGlmIHdlIGtub3cgdGhlIHVzZXIncyBkaXJlY3Rpb24ganVzdCBnZXQgb25lIGltYWdlXG5cdFx0XHRcdC8vIG90aGVyd2lzZSwgdG8gYmUgc2FmZSwgd2UgbmVlZCB0byBncmFiIG9uZSBpbiBlYWNoIGRpcmVjdGlvblxuXHRcdFx0XHRpZiAocHJlbG9hZEluZGV4KSB7XG5cdFx0XHRcdFx0dGhpcy5wcmVsb2FkSW1hZ2UocHJlbG9hZEluZGV4KTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR0aGlzLnByZWxvYWRJbWFnZShwcmV2SW5kZXgpO1xuXHRcdFx0XHRcdHRoaXMucHJlbG9hZEltYWdlKG5leHRJbmRleCk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly8gcHJlbG9hZCBjdXJyZW50IGltYWdlXG5cdFx0XHRpZiAodGhpcy5wcm9wcy5jdXJyZW50SW1hZ2UgIT09IG5leHRQcm9wcy5jdXJyZW50SW1hZ2UgfHwgIXRoaXMucHJvcHMuaXNPcGVuICYmIG5leHRQcm9wcy5pc09wZW4pIHtcblx0XHRcdFx0dmFyIGltZyA9IHRoaXMucHJlbG9hZEltYWdlRGF0YShuZXh0UHJvcHMuaW1hZ2VzW25leHRQcm9wcy5jdXJyZW50SW1hZ2VdLCB0aGlzLmhhbmRsZUltYWdlTG9hZGVkKTtcblx0XHRcdFx0aWYgKGltZykgdGhpcy5zZXRTdGF0ZSh7IGltYWdlTG9hZGVkOiBpbWcuY29tcGxldGUgfSk7XG5cdFx0XHR9XG5cblx0XHRcdC8vIGFkZC9yZW1vdmUgZXZlbnQgbGlzdGVuZXJzXG5cdFx0XHRpZiAoIXRoaXMucHJvcHMuaXNPcGVuICYmIG5leHRQcm9wcy5pc09wZW4gJiYgbmV4dFByb3BzLmVuYWJsZUtleWJvYXJkSW5wdXQpIHtcblx0XHRcdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCB0aGlzLmhhbmRsZUtleWJvYXJkSW5wdXQpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKCFuZXh0UHJvcHMuaXNPcGVuICYmIG5leHRQcm9wcy5lbmFibGVLZXlib2FyZElucHV0KSB7XG5cdFx0XHRcdHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKCdrZXlkb3duJywgdGhpcy5oYW5kbGVLZXlib2FyZElucHV0KTtcblx0XHRcdH1cblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuXHRcdFx0aWYgKHRoaXMucHJvcHMuZW5hYmxlS2V5Ym9hcmRJbnB1dCkge1xuXHRcdFx0XHR3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIHRoaXMuaGFuZGxlS2V5Ym9hcmRJbnB1dCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cdFx0Ly8gTUVUSE9EU1xuXHRcdC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG5cdH0sIHtcblx0XHRrZXk6ICdwcmVsb2FkSW1hZ2UnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBwcmVsb2FkSW1hZ2UoaWR4LCBvbmxvYWQpIHtcblx0XHRcdHJldHVybiB0aGlzLnByZWxvYWRJbWFnZURhdGEodGhpcy5wcm9wcy5pbWFnZXNbaWR4XSwgb25sb2FkKTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdwcmVsb2FkSW1hZ2VEYXRhJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gcHJlbG9hZEltYWdlRGF0YShkYXRhLCBvbmxvYWQpIHtcblx0XHRcdGlmICghZGF0YSkgcmV0dXJuO1xuXHRcdFx0dmFyIGltZyA9IG5ldyBJbWFnZSgpO1xuXHRcdFx0dmFyIHNvdXJjZVNldCA9IG5vcm1hbGl6ZVNvdXJjZVNldChkYXRhKTtcblxuXHRcdFx0Ly8gVE9ETzogYWRkIGVycm9yIGhhbmRsaW5nIGZvciBtaXNzaW5nIGltYWdlc1xuXHRcdFx0aW1nLm9uZXJyb3IgPSBvbmxvYWQ7XG5cdFx0XHRpbWcub25sb2FkID0gb25sb2FkO1xuXHRcdFx0aW1nLnNyYyA9IGRhdGEuc3JjO1xuXG5cdFx0XHRpZiAoc291cmNlU2V0KSBpbWcuc3Jjc2V0ID0gc291cmNlU2V0O1xuXG5cdFx0XHRyZXR1cm4gaW1nO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dvdG9OZXh0Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ290b05leHQoZXZlbnQpIHtcblx0XHRcdHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuXHRcdFx0ICAgIGN1cnJlbnRJbWFnZSA9IF9wcm9wcy5jdXJyZW50SW1hZ2UsXG5cdFx0XHQgICAgaW1hZ2VzID0gX3Byb3BzLmltYWdlcztcblx0XHRcdHZhciBpbWFnZUxvYWRlZCA9IHRoaXMuc3RhdGUuaW1hZ2VMb2FkZWQ7XG5cblxuXHRcdFx0aWYgKCFpbWFnZUxvYWRlZCB8fCBjdXJyZW50SW1hZ2UgPT09IGltYWdlcy5sZW5ndGggLSAxKSByZXR1cm47XG5cblx0XHRcdGlmIChldmVudCkge1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdH1cblxuXHRcdFx0dGhpcy5wcm9wcy5vbkNsaWNrTmV4dCgpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dvdG9QcmV2Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ290b1ByZXYoZXZlbnQpIHtcblx0XHRcdHZhciBjdXJyZW50SW1hZ2UgPSB0aGlzLnByb3BzLmN1cnJlbnRJbWFnZTtcblx0XHRcdHZhciBpbWFnZUxvYWRlZCA9IHRoaXMuc3RhdGUuaW1hZ2VMb2FkZWQ7XG5cblxuXHRcdFx0aWYgKCFpbWFnZUxvYWRlZCB8fCBjdXJyZW50SW1hZ2UgPT09IDApIHJldHVybjtcblxuXHRcdFx0aWYgKGV2ZW50KSB7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLnByb3BzLm9uQ2xpY2tQcmV2KCk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnY2xvc2VCYWNrZHJvcCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNsb3NlQmFja2Ryb3AoZXZlbnQpIHtcblx0XHRcdC8vIG1ha2Ugc3VyZSBldmVudCBvbmx5IGhhcHBlbnMgaWYgdGhleSBjbGljayB0aGUgYmFja2Ryb3Bcblx0XHRcdC8vIGFuZCBpZiB0aGUgY2FwdGlvbiBpcyB3aWRlbmluZyB0aGUgZmlndXJlIGVsZW1lbnQgbGV0IHRoYXQgcmVzcG9uZCB0b29cblx0XHRcdGlmIChldmVudC50YXJnZXQuaWQgPT09ICdsaWdodGJveEJhY2tkcm9wJyB8fCBldmVudC50YXJnZXQudGFnTmFtZSA9PT0gJ0ZJR1VSRScpIHtcblx0XHRcdFx0dGhpcy5wcm9wcy5vbkNsb3NlKCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnaGFuZGxlS2V5Ym9hcmRJbnB1dCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGhhbmRsZUtleWJvYXJkSW5wdXQoZXZlbnQpIHtcblx0XHRcdGlmIChldmVudC5rZXlDb2RlID09PSAzNykge1xuXHRcdFx0XHQvLyBsZWZ0XG5cdFx0XHRcdHRoaXMuZ290b1ByZXYoZXZlbnQpO1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH0gZWxzZSBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzkpIHtcblx0XHRcdFx0Ly8gcmlnaHRcblx0XHRcdFx0dGhpcy5nb3RvTmV4dChldmVudCk7XG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0fSBlbHNlIGlmIChldmVudC5rZXlDb2RlID09PSAyNykge1xuXHRcdFx0XHQvLyBlc2Ncblx0XHRcdFx0dGhpcy5wcm9wcy5vbkNsb3NlKCk7XG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2hhbmRsZUltYWdlTG9hZGVkJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gaGFuZGxlSW1hZ2VMb2FkZWQoKSB7XG5cdFx0XHR0aGlzLnNldFN0YXRlKHsgaW1hZ2VMb2FkZWQ6IHRydWUgfSk7XG5cdFx0fVxuXG5cdFx0Ly8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cdFx0Ly8gUkVOREVSRVJTXG5cdFx0Ly8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckFycm93UHJldicsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckFycm93UHJldigpIHtcblx0XHRcdGlmICh0aGlzLnByb3BzLmN1cnJlbnRJbWFnZSA9PT0gMCkgcmV0dXJuIG51bGw7XG5cblx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfQXJyb3cyLmRlZmF1bHQsIHtcblx0XHRcdFx0ZGlyZWN0aW9uOiAnbGVmdCcsXG5cdFx0XHRcdGljb246ICdhcnJvd0xlZnQnLFxuXHRcdFx0XHRvbkNsaWNrOiB0aGlzLmdvdG9QcmV2LFxuXHRcdFx0XHR0aXRsZTogdGhpcy5wcm9wcy5sZWZ0QXJyb3dUaXRsZSxcblx0XHRcdFx0dHlwZTogJ2J1dHRvbidcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckFycm93TmV4dCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckFycm93TmV4dCgpIHtcblx0XHRcdGlmICh0aGlzLnByb3BzLmN1cnJlbnRJbWFnZSA9PT0gdGhpcy5wcm9wcy5pbWFnZXMubGVuZ3RoIC0gMSkgcmV0dXJuIG51bGw7XG5cblx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfQXJyb3cyLmRlZmF1bHQsIHtcblx0XHRcdFx0ZGlyZWN0aW9uOiAncmlnaHQnLFxuXHRcdFx0XHRpY29uOiAnYXJyb3dSaWdodCcsXG5cdFx0XHRcdG9uQ2xpY2s6IHRoaXMuZ290b05leHQsXG5cdFx0XHRcdHRpdGxlOiB0aGlzLnByb3BzLnJpZ2h0QXJyb3dUaXRsZSxcblx0XHRcdFx0dHlwZTogJ2J1dHRvbidcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckRpYWxvZycsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckRpYWxvZygpIHtcblx0XHRcdHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBiYWNrZHJvcENsb3Nlc01vZGFsID0gX3Byb3BzMi5iYWNrZHJvcENsb3Nlc01vZGFsLFxuXHRcdFx0ICAgIGlzT3BlbiA9IF9wcm9wczIuaXNPcGVuLFxuXHRcdFx0ICAgIHNob3dUaHVtYm5haWxzID0gX3Byb3BzMi5zaG93VGh1bWJuYWlscyxcblx0XHRcdCAgICB3aWR0aCA9IF9wcm9wczIud2lkdGg7XG5cdFx0XHR2YXIgaW1hZ2VMb2FkZWQgPSB0aGlzLnN0YXRlLmltYWdlTG9hZGVkO1xuXG5cblx0XHRcdGlmICghaXNPcGVuKSByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCB7IGtleTogJ2Nsb3NlZCcgfSk7XG5cblx0XHRcdHZhciBvZmZzZXRUaHVtYm5haWxzID0gMDtcblx0XHRcdGlmIChzaG93VGh1bWJuYWlscykge1xuXHRcdFx0XHRvZmZzZXRUaHVtYm5haWxzID0gdGhpcy50aGVtZS50aHVtYm5haWwuc2l6ZSArIHRoaXMudGhlbWUuY29udGFpbmVyLmd1dHRlci52ZXJ0aWNhbDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuXHRcdFx0XHRfQ29udGFpbmVyMi5kZWZhdWx0LFxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0a2V5OiAnb3BlbicsXG5cdFx0XHRcdFx0b25DbGljazogYmFja2Ryb3BDbG9zZXNNb2RhbCAmJiB0aGlzLmNsb3NlQmFja2Ryb3AsXG5cdFx0XHRcdFx0b25Ub3VjaEVuZDogYmFja2Ryb3BDbG9zZXNNb2RhbCAmJiB0aGlzLmNsb3NlQmFja2Ryb3Bcblx0XHRcdFx0fSxcblx0XHRcdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0XHRcdFx0J2RpdicsXG5cdFx0XHRcdFx0bnVsbCxcblx0XHRcdFx0XHRfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcblx0XHRcdFx0XHRcdCdkaXYnLFxuXHRcdFx0XHRcdFx0eyBjbGFzc05hbWU6ICgwLCBfYXBocm9kaXRlLmNzcykodGhpcy5jbGFzc2VzLmNvbnRlbnQpLCBzdHlsZTogeyBtYXJnaW5Cb3R0b206IG9mZnNldFRodW1ibmFpbHMsIG1heFdpZHRoOiB3aWR0aCB9IH0sXG5cdFx0XHRcdFx0XHRpbWFnZUxvYWRlZCAmJiB0aGlzLnJlbmRlckhlYWRlcigpLFxuXHRcdFx0XHRcdFx0dGhpcy5yZW5kZXJJbWFnZXMoKSxcblx0XHRcdFx0XHRcdHRoaXMucmVuZGVyU3Bpbm5lcigpLFxuXHRcdFx0XHRcdFx0aW1hZ2VMb2FkZWQgJiYgdGhpcy5yZW5kZXJGb290ZXIoKVxuXHRcdFx0XHRcdCksXG5cdFx0XHRcdFx0aW1hZ2VMb2FkZWQgJiYgdGhpcy5yZW5kZXJUaHVtYm5haWxzKCksXG5cdFx0XHRcdFx0aW1hZ2VMb2FkZWQgJiYgdGhpcy5yZW5kZXJBcnJvd1ByZXYoKSxcblx0XHRcdFx0XHRpbWFnZUxvYWRlZCAmJiB0aGlzLnJlbmRlckFycm93TmV4dCgpLFxuXHRcdFx0XHRcdHRoaXMucHJvcHMucHJldmVudFNjcm9sbCAmJiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfcmVhY3RTY3JvbGxsb2NrMi5kZWZhdWx0LCBudWxsKVxuXHRcdFx0XHQpXG5cdFx0XHQpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckltYWdlcycsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckltYWdlcygpIHtcblx0XHRcdHZhciBfcHJvcHMzID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBjdXJyZW50SW1hZ2UgPSBfcHJvcHMzLmN1cnJlbnRJbWFnZSxcblx0XHRcdCAgICBpbWFnZXMgPSBfcHJvcHMzLmltYWdlcyxcblx0XHRcdCAgICBvbkNsaWNrSW1hZ2UgPSBfcHJvcHMzLm9uQ2xpY2tJbWFnZSxcblx0XHRcdCAgICBzaG93VGh1bWJuYWlscyA9IF9wcm9wczMuc2hvd1RodW1ibmFpbHM7XG5cdFx0XHR2YXIgaW1hZ2VMb2FkZWQgPSB0aGlzLnN0YXRlLmltYWdlTG9hZGVkO1xuXG5cblx0XHRcdGlmICghaW1hZ2VzIHx8ICFpbWFnZXMubGVuZ3RoKSByZXR1cm4gbnVsbDtcblxuXHRcdFx0dmFyIGltYWdlID0gaW1hZ2VzW2N1cnJlbnRJbWFnZV07XG5cdFx0XHR2YXIgc291cmNlU2V0ID0gbm9ybWFsaXplU291cmNlU2V0KGltYWdlKTtcblx0XHRcdHZhciBzaXplcyA9IHNvdXJjZVNldCA/ICcxMDB2dycgOiBudWxsO1xuXG5cdFx0XHR2YXIgdGh1bWJuYWlsc1NpemUgPSBzaG93VGh1bWJuYWlscyA/IHRoaXMudGhlbWUudGh1bWJuYWlsLnNpemUgOiAwO1xuXHRcdFx0dmFyIGhlaWdodE9mZnNldCA9IHRoaXMudGhlbWUuaGVhZGVyLmhlaWdodCArIHRoaXMudGhlbWUuZm9vdGVyLmhlaWdodCArIHRodW1ibmFpbHNTaXplICsgdGhpcy50aGVtZS5jb250YWluZXIuZ3V0dGVyLnZlcnRpY2FsICsgJ3B4JztcblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuXHRcdFx0XHQnZmlndXJlJyxcblx0XHRcdFx0eyBjbGFzc05hbWU6ICgwLCBfYXBocm9kaXRlLmNzcykodGhpcy5jbGFzc2VzLmZpZ3VyZSkgfSxcblx0XHRcdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2ltZycsIHtcblx0XHRcdFx0XHRjbGFzc05hbWU6ICgwLCBfYXBocm9kaXRlLmNzcykodGhpcy5jbGFzc2VzLmltYWdlLCBpbWFnZUxvYWRlZCAmJiB0aGlzLmNsYXNzZXMuaW1hZ2VMb2FkZWQpLFxuXHRcdFx0XHRcdG9uQ2xpY2s6IG9uQ2xpY2tJbWFnZSxcblx0XHRcdFx0XHRzaXplczogc2l6ZXMsXG5cdFx0XHRcdFx0YWx0OiBpbWFnZS5hbHQsXG5cdFx0XHRcdFx0c3JjOiBpbWFnZS5zcmMsXG5cdFx0XHRcdFx0c3JjU2V0OiBzb3VyY2VTZXQsXG5cdFx0XHRcdFx0c3R5bGU6IHtcblx0XHRcdFx0XHRcdGN1cnNvcjogb25DbGlja0ltYWdlID8gJ3BvaW50ZXInIDogJ2F1dG8nLFxuXHRcdFx0XHRcdFx0bWF4SGVpZ2h0OiAnY2FsYygxMDB2aCAtICcgKyBoZWlnaHRPZmZzZXQgKyAnKSdcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pXG5cdFx0XHQpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlclRodW1ibmFpbHMnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiByZW5kZXJUaHVtYm5haWxzKCkge1xuXHRcdFx0dmFyIF9wcm9wczQgPSB0aGlzLnByb3BzLFxuXHRcdFx0ICAgIGltYWdlcyA9IF9wcm9wczQuaW1hZ2VzLFxuXHRcdFx0ICAgIGN1cnJlbnRJbWFnZSA9IF9wcm9wczQuY3VycmVudEltYWdlLFxuXHRcdFx0ICAgIG9uQ2xpY2tUaHVtYm5haWwgPSBfcHJvcHM0Lm9uQ2xpY2tUaHVtYm5haWwsXG5cdFx0XHQgICAgc2hvd1RodW1ibmFpbHMgPSBfcHJvcHM0LnNob3dUaHVtYm5haWxzLFxuXHRcdFx0ICAgIHRodW1ibmFpbE9mZnNldCA9IF9wcm9wczQudGh1bWJuYWlsT2Zmc2V0O1xuXG5cblx0XHRcdGlmICghc2hvd1RodW1ibmFpbHMpIHJldHVybjtcblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9QYWdpbmF0ZWRUaHVtYm5haWxzMi5kZWZhdWx0LCB7XG5cdFx0XHRcdGN1cnJlbnRJbWFnZTogY3VycmVudEltYWdlLFxuXHRcdFx0XHRpbWFnZXM6IGltYWdlcyxcblx0XHRcdFx0b2Zmc2V0OiB0aHVtYm5haWxPZmZzZXQsXG5cdFx0XHRcdG9uQ2xpY2tUaHVtYm5haWw6IG9uQ2xpY2tUaHVtYm5haWxcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckhlYWRlcicsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckhlYWRlcigpIHtcblx0XHRcdHZhciBfcHJvcHM1ID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBjbG9zZUJ1dHRvblRpdGxlID0gX3Byb3BzNS5jbG9zZUJ1dHRvblRpdGxlLFxuXHRcdFx0ICAgIGN1c3RvbUNvbnRyb2xzID0gX3Byb3BzNS5jdXN0b21Db250cm9scyxcblx0XHRcdCAgICBvbkNsb3NlID0gX3Byb3BzNS5vbkNsb3NlLFxuXHRcdFx0ICAgIHNob3dDbG9zZUJ1dHRvbiA9IF9wcm9wczUuc2hvd0Nsb3NlQnV0dG9uO1xuXG5cblx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfSGVhZGVyMi5kZWZhdWx0LCB7XG5cdFx0XHRcdGN1c3RvbUNvbnRyb2xzOiBjdXN0b21Db250cm9scyxcblx0XHRcdFx0b25DbG9zZTogb25DbG9zZSxcblx0XHRcdFx0c2hvd0Nsb3NlQnV0dG9uOiBzaG93Q2xvc2VCdXR0b24sXG5cdFx0XHRcdGNsb3NlQnV0dG9uVGl0bGU6IGNsb3NlQnV0dG9uVGl0bGVcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckZvb3RlcicsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckZvb3RlcigpIHtcblx0XHRcdHZhciBfcHJvcHM2ID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBjdXJyZW50SW1hZ2UgPSBfcHJvcHM2LmN1cnJlbnRJbWFnZSxcblx0XHRcdCAgICBpbWFnZXMgPSBfcHJvcHM2LmltYWdlcyxcblx0XHRcdCAgICBpbWFnZUNvdW50U2VwYXJhdG9yID0gX3Byb3BzNi5pbWFnZUNvdW50U2VwYXJhdG9yLFxuXHRcdFx0ICAgIHNob3dJbWFnZUNvdW50ID0gX3Byb3BzNi5zaG93SW1hZ2VDb3VudDtcblxuXG5cdFx0XHRpZiAoIWltYWdlcyB8fCAhaW1hZ2VzLmxlbmd0aCkgcmV0dXJuIG51bGw7XG5cblx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfRm9vdGVyMi5kZWZhdWx0LCB7XG5cdFx0XHRcdGNhcHRpb246IGltYWdlc1tjdXJyZW50SW1hZ2VdLmNhcHRpb24sXG5cdFx0XHRcdGNvdW50Q3VycmVudDogY3VycmVudEltYWdlICsgMSxcblx0XHRcdFx0Y291bnRTZXBhcmF0b3I6IGltYWdlQ291bnRTZXBhcmF0b3IsXG5cdFx0XHRcdGNvdW50VG90YWw6IGltYWdlcy5sZW5ndGgsXG5cdFx0XHRcdHNob3dDb3VudDogc2hvd0ltYWdlQ291bnRcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlclNwaW5uZXInLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiByZW5kZXJTcGlubmVyKCkge1xuXHRcdFx0dmFyIF9wcm9wczcgPSB0aGlzLnByb3BzLFxuXHRcdFx0ICAgIHNwaW5uZXIgPSBfcHJvcHM3LnNwaW5uZXIsXG5cdFx0XHQgICAgc3Bpbm5lckNvbG9yID0gX3Byb3BzNy5zcGlubmVyQ29sb3IsXG5cdFx0XHQgICAgc3Bpbm5lclNpemUgPSBfcHJvcHM3LnNwaW5uZXJTaXplO1xuXHRcdFx0dmFyIGltYWdlTG9hZGVkID0gdGhpcy5zdGF0ZS5pbWFnZUxvYWRlZDtcblxuXHRcdFx0dmFyIFNwaW5uZXIgPSBzcGlubmVyO1xuXG5cdFx0XHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0XHRcdCdkaXYnLFxuXHRcdFx0XHR7IGNsYXNzTmFtZTogKDAsIF9hcGhyb2RpdGUuY3NzKSh0aGlzLmNsYXNzZXMuc3Bpbm5lciwgIWltYWdlTG9hZGVkICYmIHRoaXMuY2xhc3Nlcy5zcGlubmVyQWN0aXZlKSB9LFxuXHRcdFx0XHRfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChTcGlubmVyLCB7XG5cdFx0XHRcdFx0Y29sb3I6IHNwaW5uZXJDb2xvcixcblx0XHRcdFx0XHRzaXplOiBzcGlubmVyU2l6ZVxuXHRcdFx0XHR9KVxuXHRcdFx0KTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdyZW5kZXInLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG5cdFx0XHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0XHRcdF9Qb3J0YWwyLmRlZmF1bHQsXG5cdFx0XHRcdG51bGwsXG5cdFx0XHRcdHRoaXMucmVuZGVyRGlhbG9nKClcblx0XHRcdCk7XG5cdFx0fVxuXHR9XSk7XG5cblx0cmV0dXJuIExpZ2h0Ym94O1xufShfcmVhY3QuQ29tcG9uZW50KTtcblxuTGlnaHRib3gucHJvcFR5cGVzID0ge1xuXHRiYWNrZHJvcENsb3Nlc01vZGFsOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdGNsb3NlQnV0dG9uVGl0bGU6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuXHRjdXJyZW50SW1hZ2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQubnVtYmVyLFxuXHRjdXN0b21Db250cm9sczogX3Byb3BUeXBlczIuZGVmYXVsdC5hcnJheU9mKF9wcm9wVHlwZXMyLmRlZmF1bHQubm9kZSksXG5cdGVuYWJsZUtleWJvYXJkSW5wdXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcblx0aW1hZ2VDb3VudFNlcGFyYXRvcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG5cdGltYWdlczogX3Byb3BUeXBlczIuZGVmYXVsdC5hcnJheU9mKF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuXHRcdHNyYzogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcuaXNSZXF1aXJlZCxcblx0XHRzcmNTZXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5hcnJheU9mKF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nKV0pLFxuXHRcdGNhcHRpb246IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5lbGVtZW50XSksXG5cdFx0dGh1bWJuYWlsOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZ1xuXHR9KSkuaXNSZXF1aXJlZCxcblx0aXNPcGVuOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdGxlZnRBcnJvd1RpdGxlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcblx0b25DbGlja0ltYWdlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG5cdG9uQ2xpY2tOZXh0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG5cdG9uQ2xpY2tQcmV2OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG5cdG9uQ2xvc2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuXHRwcmVsb2FkTmV4dEltYWdlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdHByZXZlbnRTY3JvbGw6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcblx0cmlnaHRBcnJvd1RpdGxlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcblx0c2hvd0Nsb3NlQnV0dG9uOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdHNob3dJbWFnZUNvdW50OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdHNob3dUaHVtYm5haWxzOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG5cdHNwaW5uZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcblx0c3Bpbm5lckNvbG9yOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcblx0c3Bpbm5lclNpemU6IF9wcm9wVHlwZXMyLmRlZmF1bHQubnVtYmVyLFxuXHR0aGVtZTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QsXG5cdHRodW1ibmFpbE9mZnNldDogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG5cdHdpZHRoOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlclxufTtcbkxpZ2h0Ym94LmRlZmF1bHRQcm9wcyA9IHtcblx0Y2xvc2VCdXR0b25UaXRsZTogJ0Nsb3NlIChFc2MpJyxcblx0Y3VycmVudEltYWdlOiAwLFxuXHRlbmFibGVLZXlib2FyZElucHV0OiB0cnVlLFxuXHRpbWFnZUNvdW50U2VwYXJhdG9yOiAnIG9mICcsXG5cdGxlZnRBcnJvd1RpdGxlOiAnUHJldmlvdXMgKExlZnQgYXJyb3cga2V5KScsXG5cdG9uQ2xpY2tTaG93TmV4dEltYWdlOiB0cnVlLFxuXHRwcmVsb2FkTmV4dEltYWdlOiB0cnVlLFxuXHRwcmV2ZW50U2Nyb2xsOiB0cnVlLFxuXHRyaWdodEFycm93VGl0bGU6ICdOZXh0IChSaWdodCBhcnJvdyBrZXkpJyxcblx0c2hvd0Nsb3NlQnV0dG9uOiB0cnVlLFxuXHRzaG93SW1hZ2VDb3VudDogdHJ1ZSxcblx0c3Bpbm5lcjogX1NwaW5uZXIyLmRlZmF1bHQsXG5cdHNwaW5uZXJDb2xvcjogJ3doaXRlJyxcblx0c3Bpbm5lclNpemU6IDEwMCxcblx0dGhlbWU6IHt9LFxuXHR0aHVtYm5haWxPZmZzZXQ6IDIsXG5cdHdpZHRoOiAxMDI0XG59O1xuTGlnaHRib3guY2hpbGRDb250ZXh0VHlwZXMgPSB7XG5cdHRoZW1lOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdC5pc1JlcXVpcmVkXG59O1xuXG52YXIgZGVmYXVsdFN0eWxlcyA9IHtcblx0Y29udGVudDoge1xuXHRcdHBvc2l0aW9uOiAncmVsYXRpdmUnXG5cdH0sXG5cdGZpZ3VyZToge1xuXHRcdG1hcmdpbjogMCAvLyByZW1vdmUgYnJvd3NlciBkZWZhdWx0XG5cdH0sXG5cdGltYWdlOiB7XG5cdFx0ZGlzcGxheTogJ2Jsb2NrJywgLy8gcmVtb3ZlcyBicm93c2VyIGRlZmF1bHQgZ3V0dGVyXG5cdFx0aGVpZ2h0OiAnYXV0bycsXG5cdFx0bWFyZ2luOiAnMCBhdXRvJywgLy8gbWFpbnRhaW4gY2VudGVyIG9uIHZlcnkgc2hvcnQgc2NyZWVucyBPUiB2ZXJ5IG5hcnJvdyBpbWFnZVxuXHRcdG1heFdpZHRoOiAnMTAwJScsXG5cblx0XHQvLyBkaXNhYmxlIHVzZXIgc2VsZWN0XG5cdFx0V2Via2l0VG91Y2hDYWxsb3V0OiAnbm9uZScsXG5cdFx0dXNlclNlbGVjdDogJ25vbmUnLFxuXG5cdFx0Ly8gb3BhY2l0eSBhbmltYXRpb24gb24gaW1hZ2UgbG9hZFxuXHRcdG9wYWNpdHk6IDAsXG5cdFx0dHJhbnNpdGlvbjogJ29wYWNpdHkgMC4zcydcblx0fSxcblx0aW1hZ2VMb2FkZWQ6IHtcblx0XHRvcGFjaXR5OiAxXG5cdH0sXG5cdHNwaW5uZXI6IHtcblx0XHRwb3NpdGlvbjogJ2Fic29sdXRlJyxcblx0XHR0b3A6ICc1MCUnLFxuXHRcdGxlZnQ6ICc1MCUnLFxuXHRcdHRyYW5zZm9ybTogJ3RyYW5zbGF0ZSgtNTAlLCAtNTAlKScsXG5cblx0XHQvLyBvcGFjaXR5IGFuaW1hdGlvbiB0byBtYWtlIHNwaW5uZXIgYXBwZWFyIHdpdGggZGVsYXlcblx0XHRvcGFjaXR5OiAwLFxuXHRcdHRyYW5zaXRpb246ICdvcGFjaXR5IDAuM3MnLFxuXHRcdHBvaW50ZXJFdmVudHM6ICdub25lJ1xuXHR9LFxuXHRzcGlubmVyQWN0aXZlOiB7XG5cdFx0b3BhY2l0eTogMVxuXHR9XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBMaWdodGJveDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUF6Q0E7QUEyQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBakJBO0FBbUJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUdBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTtBQTFDQTtBQTRDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBR0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFQQTtBQWFBO0FBcENBO0FBc0NBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBbkJBO0FBcUJBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFoQkE7QUFrQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFuQkE7QUFxQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFFQTtBQUVBO0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUtBO0FBbkJBO0FBcUJBO0FBQ0E7QUFDQTtBQUtBO0FBUkE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBOUJBO0FBZ0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQW1CQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQURBO0FBbkNBO0FBd0NBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/Lightbox.js
