var _config = __webpack_require__(/*! ../config */ "./node_modules/zrender/lib/config.js");

var debugMode = _config.debugMode;

var log = function log() {};

if (debugMode === 1) {
  log = function log() {
    for (var k in arguments) {
      throw new Error(arguments[k]);
    }
  };
} else if (debugMode > 1) {
  log = function log() {
    for (var k in arguments) {
      console.log(arguments[k]);
    }
  };
}

var _default = log;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9sb2cuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL2xvZy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi9jb25maWdcIik7XG5cbnZhciBkZWJ1Z01vZGUgPSBfY29uZmlnLmRlYnVnTW9kZTtcblxudmFyIGxvZyA9IGZ1bmN0aW9uICgpIHt9O1xuXG5pZiAoZGVidWdNb2RlID09PSAxKSB7XG4gIGxvZyA9IGZ1bmN0aW9uICgpIHtcbiAgICBmb3IgKHZhciBrIGluIGFyZ3VtZW50cykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGFyZ3VtZW50c1trXSk7XG4gICAgfVxuICB9O1xufSBlbHNlIGlmIChkZWJ1Z01vZGUgPiAxKSB7XG4gIGxvZyA9IGZ1bmN0aW9uICgpIHtcbiAgICBmb3IgKHZhciBrIGluIGFyZ3VtZW50cykge1xuICAgICAgY29uc29sZS5sb2coYXJndW1lbnRzW2tdKTtcbiAgICB9XG4gIH07XG59XG5cbnZhciBfZGVmYXVsdCA9IGxvZztcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/log.js
