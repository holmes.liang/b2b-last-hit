

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

var utils_1 = __webpack_require__(/*! ./utils */ "./node_modules/rc-table/es/utils.js");

var BaseTable_1 = __importDefault(__webpack_require__(/*! ./BaseTable */ "./node_modules/rc-table/es/BaseTable.js"));

function HeadTable(props, _ref) {
  var table = _ref.table;
  var _table$props = table.props,
      prefixCls = _table$props.prefixCls,
      scroll = _table$props.scroll,
      showHeader = _table$props.showHeader;
  var columns = props.columns,
      fixed = props.fixed,
      tableClassName = props.tableClassName,
      handleBodyScrollLeft = props.handleBodyScrollLeft,
      expander = props.expander;
  var saveRef = table.saveRef;
  var useFixedHeader = table.props.useFixedHeader;
  var headStyle = {};
  var scrollbarWidth = utils_1.measureScrollbar({
    direction: 'vertical'
  });

  if (scroll.y) {
    useFixedHeader = true; // https://github.com/ant-design/ant-design/issues/17051

    var scrollbarWidthOfHeader = utils_1.measureScrollbar({
      direction: 'horizontal',
      prefixCls: prefixCls
    }); // Add negative margin bottom for scroll bar overflow bug

    if (scrollbarWidthOfHeader > 0 && !fixed) {
      headStyle.marginBottom = "-".concat(scrollbarWidthOfHeader, "px");
      headStyle.paddingBottom = '0px'; // https://github.com/ant-design/ant-design/pull/19986

      headStyle.minWidth = "".concat(scrollbarWidth, "px"); // https://github.com/ant-design/ant-design/issues/17051

      headStyle.overflowX = 'scroll';
      headStyle.overflowY = scrollbarWidth === 0 ? 'hidden' : 'scroll';
    }
  }

  if (!useFixedHeader || !showHeader) {
    return null;
  }

  return React.createElement("div", {
    key: "headTable",
    ref: fixed ? null : saveRef('headTable'),
    className: classnames_1.default("".concat(prefixCls, "-header"), _defineProperty({}, "".concat(prefixCls, "-hide-scrollbar"), scrollbarWidth > 0)),
    style: headStyle,
    onScroll: handleBodyScrollLeft
  }, React.createElement(BaseTable_1.default, {
    tableClassName: tableClassName,
    hasHead: true,
    hasBody: false,
    fixed: fixed,
    columns: columns,
    expander: expander
  }));
}

exports.default = HeadTable;
HeadTable.contextTypes = {
  table: PropTypes.any
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvSGVhZFRhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvSGVhZFRhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7IGlmIChrZXkgaW4gb2JqKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgeyB2YWx1ZTogdmFsdWUsIGVudW1lcmFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSwgd3JpdGFibGU6IHRydWUgfSk7IH0gZWxzZSB7IG9ialtrZXldID0gdmFsdWU7IH0gcmV0dXJuIG9iajsgfVxuXG52YXIgX19pbXBvcnRTdGFyID0gdGhpcyAmJiB0aGlzLl9faW1wb3J0U3RhciB8fCBmdW5jdGlvbiAobW9kKSB7XG4gIGlmIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpIHJldHVybiBtb2Q7XG4gIHZhciByZXN1bHQgPSB7fTtcbiAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkge1xuICAgIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XG4gIH1cbiAgcmVzdWx0W1wiZGVmYXVsdFwiXSA9IG1vZDtcbiAgcmV0dXJuIHJlc3VsdDtcbn07XG5cbnZhciBfX2ltcG9ydERlZmF1bHQgPSB0aGlzICYmIHRoaXMuX19pbXBvcnREZWZhdWx0IHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgcmV0dXJuIG1vZCAmJiBtb2QuX19lc01vZHVsZSA/IG1vZCA6IHtcbiAgICBcImRlZmF1bHRcIjogbW9kXG4gIH07XG59O1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgUmVhY3QgPSBfX2ltcG9ydFN0YXIocmVxdWlyZShcInJlYWN0XCIpKTtcblxudmFyIFByb3BUeXBlcyA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicHJvcC10eXBlc1wiKSk7XG5cbnZhciBjbGFzc25hbWVzXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcImNsYXNzbmFtZXNcIikpO1xuXG52YXIgdXRpbHNfMSA9IHJlcXVpcmUoXCIuL3V0aWxzXCIpO1xuXG52YXIgQmFzZVRhYmxlXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcIi4vQmFzZVRhYmxlXCIpKTtcblxuZnVuY3Rpb24gSGVhZFRhYmxlKHByb3BzLCBfcmVmKSB7XG4gIHZhciB0YWJsZSA9IF9yZWYudGFibGU7XG4gIHZhciBfdGFibGUkcHJvcHMgPSB0YWJsZS5wcm9wcyxcbiAgICAgIHByZWZpeENscyA9IF90YWJsZSRwcm9wcy5wcmVmaXhDbHMsXG4gICAgICBzY3JvbGwgPSBfdGFibGUkcHJvcHMuc2Nyb2xsLFxuICAgICAgc2hvd0hlYWRlciA9IF90YWJsZSRwcm9wcy5zaG93SGVhZGVyO1xuICB2YXIgY29sdW1ucyA9IHByb3BzLmNvbHVtbnMsXG4gICAgICBmaXhlZCA9IHByb3BzLmZpeGVkLFxuICAgICAgdGFibGVDbGFzc05hbWUgPSBwcm9wcy50YWJsZUNsYXNzTmFtZSxcbiAgICAgIGhhbmRsZUJvZHlTY3JvbGxMZWZ0ID0gcHJvcHMuaGFuZGxlQm9keVNjcm9sbExlZnQsXG4gICAgICBleHBhbmRlciA9IHByb3BzLmV4cGFuZGVyO1xuICB2YXIgc2F2ZVJlZiA9IHRhYmxlLnNhdmVSZWY7XG4gIHZhciB1c2VGaXhlZEhlYWRlciA9IHRhYmxlLnByb3BzLnVzZUZpeGVkSGVhZGVyO1xuICB2YXIgaGVhZFN0eWxlID0ge307XG4gIHZhciBzY3JvbGxiYXJXaWR0aCA9IHV0aWxzXzEubWVhc3VyZVNjcm9sbGJhcih7XG4gICAgZGlyZWN0aW9uOiAndmVydGljYWwnXG4gIH0pO1xuXG4gIGlmIChzY3JvbGwueSkge1xuICAgIHVzZUZpeGVkSGVhZGVyID0gdHJ1ZTsgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTcwNTFcblxuICAgIHZhciBzY3JvbGxiYXJXaWR0aE9mSGVhZGVyID0gdXRpbHNfMS5tZWFzdXJlU2Nyb2xsYmFyKHtcbiAgICAgIGRpcmVjdGlvbjogJ2hvcml6b250YWwnLFxuICAgICAgcHJlZml4Q2xzOiBwcmVmaXhDbHNcbiAgICB9KTsgLy8gQWRkIG5lZ2F0aXZlIG1hcmdpbiBib3R0b20gZm9yIHNjcm9sbCBiYXIgb3ZlcmZsb3cgYnVnXG5cbiAgICBpZiAoc2Nyb2xsYmFyV2lkdGhPZkhlYWRlciA+IDAgJiYgIWZpeGVkKSB7XG4gICAgICBoZWFkU3R5bGUubWFyZ2luQm90dG9tID0gXCItXCIuY29uY2F0KHNjcm9sbGJhcldpZHRoT2ZIZWFkZXIsIFwicHhcIik7XG4gICAgICBoZWFkU3R5bGUucGFkZGluZ0JvdHRvbSA9ICcwcHgnOyAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL3B1bGwvMTk5ODZcblxuICAgICAgaGVhZFN0eWxlLm1pbldpZHRoID0gXCJcIi5jb25jYXQoc2Nyb2xsYmFyV2lkdGgsIFwicHhcIik7IC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE3MDUxXG5cbiAgICAgIGhlYWRTdHlsZS5vdmVyZmxvd1ggPSAnc2Nyb2xsJztcbiAgICAgIGhlYWRTdHlsZS5vdmVyZmxvd1kgPSBzY3JvbGxiYXJXaWR0aCA9PT0gMCA/ICdoaWRkZW4nIDogJ3Njcm9sbCc7XG4gICAgfVxuICB9XG5cbiAgaWYgKCF1c2VGaXhlZEhlYWRlciB8fCAhc2hvd0hlYWRlcikge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiwge1xuICAgIGtleTogXCJoZWFkVGFibGVcIixcbiAgICByZWY6IGZpeGVkID8gbnVsbCA6IHNhdmVSZWYoJ2hlYWRUYWJsZScpLFxuICAgIGNsYXNzTmFtZTogY2xhc3NuYW1lc18xLmRlZmF1bHQoXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1oZWFkZXJcIiksIF9kZWZpbmVQcm9wZXJ0eSh7fSwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1oaWRlLXNjcm9sbGJhclwiKSwgc2Nyb2xsYmFyV2lkdGggPiAwKSksXG4gICAgc3R5bGU6IGhlYWRTdHlsZSxcbiAgICBvblNjcm9sbDogaGFuZGxlQm9keVNjcm9sbExlZnRcbiAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChCYXNlVGFibGVfMS5kZWZhdWx0LCB7XG4gICAgdGFibGVDbGFzc05hbWU6IHRhYmxlQ2xhc3NOYW1lLFxuICAgIGhhc0hlYWQ6IHRydWUsXG4gICAgaGFzQm9keTogZmFsc2UsXG4gICAgZml4ZWQ6IGZpeGVkLFxuICAgIGNvbHVtbnM6IGNvbHVtbnMsXG4gICAgZXhwYW5kZXI6IGV4cGFuZGVyXG4gIH0pKTtcbn1cblxuZXhwb3J0cy5kZWZhdWx0ID0gSGVhZFRhYmxlO1xuSGVhZFRhYmxlLmNvbnRleHRUeXBlcyA9IHtcbiAgdGFibGU6IFByb3BUeXBlcy5hbnlcbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/HeadTable.js
