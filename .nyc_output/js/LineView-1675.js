/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var SymbolDraw = __webpack_require__(/*! ../helper/SymbolDraw */ "./node_modules/echarts/lib/chart/helper/SymbolDraw.js");

var SymbolClz = __webpack_require__(/*! ../helper/Symbol */ "./node_modules/echarts/lib/chart/helper/Symbol.js");

var lineAnimationDiff = __webpack_require__(/*! ./lineAnimationDiff */ "./node_modules/echarts/lib/chart/line/lineAnimationDiff.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var modelUtil = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var _poly = __webpack_require__(/*! ./poly */ "./node_modules/echarts/lib/chart/line/poly.js");

var Polyline = _poly.Polyline;
var Polygon = _poly.Polygon;

var ChartView = __webpack_require__(/*! ../../view/Chart */ "./node_modules/echarts/lib/view/Chart.js");

var _number = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var round = _number.round;

var _helper = __webpack_require__(/*! ./helper */ "./node_modules/echarts/lib/chart/line/helper.js");

var prepareDataCoordInfo = _helper.prepareDataCoordInfo;
var getStackedOnPoint = _helper.getStackedOnPoint;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// FIXME step not support polar

function isPointsSame(points1, points2) {
  if (points1.length !== points2.length) {
    return;
  }

  for (var i = 0; i < points1.length; i++) {
    var p1 = points1[i];
    var p2 = points2[i];

    if (p1[0] !== p2[0] || p1[1] !== p2[1]) {
      return;
    }
  }

  return true;
}

function getSmooth(smooth) {
  return typeof smooth === 'number' ? smooth : smooth ? 0.5 : 0;
}

function getAxisExtentWithGap(axis) {
  var extent = axis.getGlobalExtent();

  if (axis.onBand) {
    // Remove extra 1px to avoid line miter in clipped edge
    var halfBandWidth = axis.getBandWidth() / 2 - 1;
    var dir = extent[1] > extent[0] ? 1 : -1;
    extent[0] += dir * halfBandWidth;
    extent[1] -= dir * halfBandWidth;
  }

  return extent;
}
/**
 * @param {module:echarts/coord/cartesian/Cartesian2D|module:echarts/coord/polar/Polar} coordSys
 * @param {module:echarts/data/List} data
 * @param {Object} dataCoordInfo
 * @param {Array.<Array.<number>>} points
 */


function getStackedOnPoints(coordSys, data, dataCoordInfo) {
  if (!dataCoordInfo.valueDim) {
    return [];
  }

  var points = [];

  for (var idx = 0, len = data.count(); idx < len; idx++) {
    points.push(getStackedOnPoint(dataCoordInfo, coordSys, data, idx));
  }

  return points;
}

function createGridClipShape(cartesian, hasAnimation, forSymbol, seriesModel) {
  var xExtent = getAxisExtentWithGap(cartesian.getAxis('x'));
  var yExtent = getAxisExtentWithGap(cartesian.getAxis('y'));
  var isHorizontal = cartesian.getBaseAxis().isHorizontal();
  var x = Math.min(xExtent[0], xExtent[1]);
  var y = Math.min(yExtent[0], yExtent[1]);
  var width = Math.max(xExtent[0], xExtent[1]) - x;
  var height = Math.max(yExtent[0], yExtent[1]) - y; // Avoid float number rounding error for symbol on the edge of axis extent.
  // See #7913 and `test/dataZoom-clip.html`.

  if (forSymbol) {
    x -= 0.5;
    width += 0.5;
    y -= 0.5;
    height += 0.5;
  } else {
    var lineWidth = seriesModel.get('lineStyle.width') || 2; // Expand clip shape to avoid clipping when line value exceeds axis

    var expandSize = seriesModel.get('clipOverflow') ? lineWidth / 2 : Math.max(width, height);

    if (isHorizontal) {
      y -= expandSize;
      height += expandSize * 2;
    } else {
      x -= expandSize;
      width += expandSize * 2;
    }
  }

  var clipPath = new graphic.Rect({
    shape: {
      x: x,
      y: y,
      width: width,
      height: height
    }
  });

  if (hasAnimation) {
    clipPath.shape[isHorizontal ? 'width' : 'height'] = 0;
    graphic.initProps(clipPath, {
      shape: {
        width: width,
        height: height
      }
    }, seriesModel);
  }

  return clipPath;
}

function createPolarClipShape(polar, hasAnimation, forSymbol, seriesModel) {
  var angleAxis = polar.getAngleAxis();
  var radiusAxis = polar.getRadiusAxis();
  var radiusExtent = radiusAxis.getExtent().slice();
  radiusExtent[0] > radiusExtent[1] && radiusExtent.reverse();
  var angleExtent = angleAxis.getExtent();
  var RADIAN = Math.PI / 180; // Avoid float number rounding error for symbol on the edge of axis extent.

  if (forSymbol) {
    radiusExtent[0] -= 0.5;
    radiusExtent[1] += 0.5;
  }

  var clipPath = new graphic.Sector({
    shape: {
      cx: round(polar.cx, 1),
      cy: round(polar.cy, 1),
      r0: round(radiusExtent[0], 1),
      r: round(radiusExtent[1], 1),
      startAngle: -angleExtent[0] * RADIAN,
      endAngle: -angleExtent[1] * RADIAN,
      clockwise: angleAxis.inverse
    }
  });

  if (hasAnimation) {
    clipPath.shape.endAngle = -angleExtent[0] * RADIAN;
    graphic.initProps(clipPath, {
      shape: {
        endAngle: -angleExtent[1] * RADIAN
      }
    }, seriesModel);
  }

  return clipPath;
}

function createClipShape(coordSys, hasAnimation, forSymbol, seriesModel) {
  return coordSys.type === 'polar' ? createPolarClipShape(coordSys, hasAnimation, forSymbol, seriesModel) : createGridClipShape(coordSys, hasAnimation, forSymbol, seriesModel);
}

function turnPointsIntoStep(points, coordSys, stepTurnAt) {
  var baseAxis = coordSys.getBaseAxis();
  var baseIndex = baseAxis.dim === 'x' || baseAxis.dim === 'radius' ? 0 : 1;
  var stepPoints = [];

  for (var i = 0; i < points.length - 1; i++) {
    var nextPt = points[i + 1];
    var pt = points[i];
    stepPoints.push(pt);
    var stepPt = [];

    switch (stepTurnAt) {
      case 'end':
        stepPt[baseIndex] = nextPt[baseIndex];
        stepPt[1 - baseIndex] = pt[1 - baseIndex]; // default is start

        stepPoints.push(stepPt);
        break;

      case 'middle':
        // default is start
        var middle = (pt[baseIndex] + nextPt[baseIndex]) / 2;
        var stepPt2 = [];
        stepPt[baseIndex] = stepPt2[baseIndex] = middle;
        stepPt[1 - baseIndex] = pt[1 - baseIndex];
        stepPt2[1 - baseIndex] = nextPt[1 - baseIndex];
        stepPoints.push(stepPt);
        stepPoints.push(stepPt2);
        break;

      default:
        stepPt[baseIndex] = pt[baseIndex];
        stepPt[1 - baseIndex] = nextPt[1 - baseIndex]; // default is start

        stepPoints.push(stepPt);
    }
  } // Last points


  points[i] && stepPoints.push(points[i]);
  return stepPoints;
}

function getVisualGradient(data, coordSys) {
  var visualMetaList = data.getVisual('visualMeta');

  if (!visualMetaList || !visualMetaList.length || !data.count()) {
    // When data.count() is 0, gradient range can not be calculated.
    return;
  }

  if (coordSys.type !== 'cartesian2d') {
    return;
  }

  var coordDim;
  var visualMeta;

  for (var i = visualMetaList.length - 1; i >= 0; i--) {
    var dimIndex = visualMetaList[i].dimension;
    var dimName = data.dimensions[dimIndex];
    var dimInfo = data.getDimensionInfo(dimName);
    coordDim = dimInfo && dimInfo.coordDim; // Can only be x or y

    if (coordDim === 'x' || coordDim === 'y') {
      visualMeta = visualMetaList[i];
      break;
    }
  }

  if (!visualMeta) {
    return;
  } // If the area to be rendered is bigger than area defined by LinearGradient,
  // the canvas spec prescribes that the color of the first stop and the last
  // stop should be used. But if two stops are added at offset 0, in effect
  // browsers use the color of the second stop to render area outside
  // LinearGradient. So we can only infinitesimally extend area defined in
  // LinearGradient to render `outerColors`.


  var axis = coordSys.getAxis(coordDim); // dataToCoor mapping may not be linear, but must be monotonic.

  var colorStops = zrUtil.map(visualMeta.stops, function (stop) {
    return {
      coord: axis.toGlobalCoord(axis.dataToCoord(stop.value)),
      color: stop.color
    };
  });
  var stopLen = colorStops.length;
  var outerColors = visualMeta.outerColors.slice();

  if (stopLen && colorStops[0].coord > colorStops[stopLen - 1].coord) {
    colorStops.reverse();
    outerColors.reverse();
  }

  var tinyExtent = 10; // Arbitrary value: 10px

  var minCoord = colorStops[0].coord - tinyExtent;
  var maxCoord = colorStops[stopLen - 1].coord + tinyExtent;
  var coordSpan = maxCoord - minCoord;

  if (coordSpan < 1e-3) {
    return 'transparent';
  }

  zrUtil.each(colorStops, function (stop) {
    stop.offset = (stop.coord - minCoord) / coordSpan;
  });
  colorStops.push({
    offset: stopLen ? colorStops[stopLen - 1].offset : 0.5,
    color: outerColors[1] || 'transparent'
  });
  colorStops.unshift({
    // notice colorStops.length have been changed.
    offset: stopLen ? colorStops[0].offset : 0.5,
    color: outerColors[0] || 'transparent'
  }); // zrUtil.each(colorStops, function (colorStop) {
  //     // Make sure each offset has rounded px to avoid not sharp edge
  //     colorStop.offset = (Math.round(colorStop.offset * (end - start) + start) - start) / (end - start);
  // });

  var gradient = new graphic.LinearGradient(0, 0, 0, 0, colorStops, true);
  gradient[coordDim] = minCoord;
  gradient[coordDim + '2'] = maxCoord;
  return gradient;
}

function getIsIgnoreFunc(seriesModel, data, coordSys) {
  var showAllSymbol = seriesModel.get('showAllSymbol');
  var isAuto = showAllSymbol === 'auto';

  if (showAllSymbol && !isAuto) {
    return;
  }

  var categoryAxis = coordSys.getAxesByScale('ordinal')[0];

  if (!categoryAxis) {
    return;
  } // Note that category label interval strategy might bring some weird effect
  // in some scenario: users may wonder why some of the symbols are not
  // displayed. So we show all symbols as possible as we can.


  if (isAuto // Simplify the logic, do not determine label overlap here.
  && canShowAllSymbolForCategory(categoryAxis, data)) {
    return;
  } // Otherwise follow the label interval strategy on category axis.


  var categoryDataDim = data.mapDimension(categoryAxis.dim);
  var labelMap = {};
  zrUtil.each(categoryAxis.getViewLabels(), function (labelItem) {
    labelMap[labelItem.tickValue] = 1;
  });
  return function (dataIndex) {
    return !labelMap.hasOwnProperty(data.get(categoryDataDim, dataIndex));
  };
}

function canShowAllSymbolForCategory(categoryAxis, data) {
  // In mose cases, line is monotonous on category axis, and the label size
  // is close with each other. So we check the symbol size and some of the
  // label size alone with the category axis to estimate whether all symbol
  // can be shown without overlap.
  var axisExtent = categoryAxis.getExtent();
  var availSize = Math.abs(axisExtent[1] - axisExtent[0]) / categoryAxis.scale.count();
  isNaN(availSize) && (availSize = 0); // 0/0 is NaN.
  // Sampling some points, max 5.

  var dataLen = data.count();
  var step = Math.max(1, Math.round(dataLen / 5));

  for (var dataIndex = 0; dataIndex < dataLen; dataIndex += step) {
    if (SymbolClz.getSymbolSize(data, dataIndex // Only for cartesian, where `isHorizontal` exists.
    )[categoryAxis.isHorizontal() ? 1 : 0] // Empirical number
    * 1.5 > availSize) {
      return false;
    }
  }

  return true;
}

var _default = ChartView.extend({
  type: 'line',
  init: function init() {
    var lineGroup = new graphic.Group();
    var symbolDraw = new SymbolDraw();
    this.group.add(symbolDraw.group);
    this._symbolDraw = symbolDraw;
    this._lineGroup = lineGroup;
  },
  render: function render(seriesModel, ecModel, api) {
    var coordSys = seriesModel.coordinateSystem;
    var group = this.group;
    var data = seriesModel.getData();
    var lineStyleModel = seriesModel.getModel('lineStyle');
    var areaStyleModel = seriesModel.getModel('areaStyle');
    var points = data.mapArray(data.getItemLayout);
    var isCoordSysPolar = coordSys.type === 'polar';
    var prevCoordSys = this._coordSys;
    var symbolDraw = this._symbolDraw;
    var polyline = this._polyline;
    var polygon = this._polygon;
    var lineGroup = this._lineGroup;
    var hasAnimation = seriesModel.get('animation');
    var isAreaChart = !areaStyleModel.isEmpty();
    var valueOrigin = areaStyleModel.get('origin');
    var dataCoordInfo = prepareDataCoordInfo(coordSys, data, valueOrigin);
    var stackedOnPoints = getStackedOnPoints(coordSys, data, dataCoordInfo);
    var showSymbol = seriesModel.get('showSymbol');
    var isIgnoreFunc = showSymbol && !isCoordSysPolar && getIsIgnoreFunc(seriesModel, data, coordSys); // Remove temporary symbols

    var oldData = this._data;
    oldData && oldData.eachItemGraphicEl(function (el, idx) {
      if (el.__temp) {
        group.remove(el);
        oldData.setItemGraphicEl(idx, null);
      }
    }); // Remove previous created symbols if showSymbol changed to false

    if (!showSymbol) {
      symbolDraw.remove();
    }

    group.add(lineGroup); // FIXME step not support polar

    var step = !isCoordSysPolar && seriesModel.get('step'); // Initialization animation or coordinate system changed

    if (!(polyline && prevCoordSys.type === coordSys.type && step === this._step)) {
      showSymbol && symbolDraw.updateData(data, {
        isIgnore: isIgnoreFunc,
        clipShape: createClipShape(coordSys, false, true, seriesModel)
      });

      if (step) {
        // TODO If stacked series is not step
        points = turnPointsIntoStep(points, coordSys, step);
        stackedOnPoints = turnPointsIntoStep(stackedOnPoints, coordSys, step);
      }

      polyline = this._newPolyline(points, coordSys, hasAnimation);

      if (isAreaChart) {
        polygon = this._newPolygon(points, stackedOnPoints, coordSys, hasAnimation);
      }

      lineGroup.setClipPath(createClipShape(coordSys, true, false, seriesModel));
    } else {
      if (isAreaChart && !polygon) {
        // If areaStyle is added
        polygon = this._newPolygon(points, stackedOnPoints, coordSys, hasAnimation);
      } else if (polygon && !isAreaChart) {
        // If areaStyle is removed
        lineGroup.remove(polygon);
        polygon = this._polygon = null;
      } // Update clipPath


      lineGroup.setClipPath(createClipShape(coordSys, false, false, seriesModel)); // Always update, or it is wrong in the case turning on legend
      // because points are not changed

      showSymbol && symbolDraw.updateData(data, {
        isIgnore: isIgnoreFunc,
        clipShape: createClipShape(coordSys, false, true, seriesModel)
      }); // Stop symbol animation and sync with line points
      // FIXME performance?

      data.eachItemGraphicEl(function (el) {
        el.stopAnimation(true);
      }); // In the case data zoom triggerred refreshing frequently
      // Data may not change if line has a category axis. So it should animate nothing

      if (!isPointsSame(this._stackedOnPoints, stackedOnPoints) || !isPointsSame(this._points, points)) {
        if (hasAnimation) {
          this._updateAnimation(data, stackedOnPoints, coordSys, api, step, valueOrigin);
        } else {
          // Not do it in update with animation
          if (step) {
            // TODO If stacked series is not step
            points = turnPointsIntoStep(points, coordSys, step);
            stackedOnPoints = turnPointsIntoStep(stackedOnPoints, coordSys, step);
          }

          polyline.setShape({
            points: points
          });
          polygon && polygon.setShape({
            points: points,
            stackedOnPoints: stackedOnPoints
          });
        }
      }
    }

    var visualColor = getVisualGradient(data, coordSys) || data.getVisual('color');
    polyline.useStyle(zrUtil.defaults( // Use color in lineStyle first
    lineStyleModel.getLineStyle(), {
      fill: 'none',
      stroke: visualColor,
      lineJoin: 'bevel'
    }));
    var smooth = seriesModel.get('smooth');
    smooth = getSmooth(seriesModel.get('smooth'));
    polyline.setShape({
      smooth: smooth,
      smoothMonotone: seriesModel.get('smoothMonotone'),
      connectNulls: seriesModel.get('connectNulls')
    });

    if (polygon) {
      var stackedOnSeries = data.getCalculationInfo('stackedOnSeries');
      var stackedOnSmooth = 0;
      polygon.useStyle(zrUtil.defaults(areaStyleModel.getAreaStyle(), {
        fill: visualColor,
        opacity: 0.7,
        lineJoin: 'bevel'
      }));

      if (stackedOnSeries) {
        stackedOnSmooth = getSmooth(stackedOnSeries.get('smooth'));
      }

      polygon.setShape({
        smooth: smooth,
        stackedOnSmooth: stackedOnSmooth,
        smoothMonotone: seriesModel.get('smoothMonotone'),
        connectNulls: seriesModel.get('connectNulls')
      });
    }

    this._data = data; // Save the coordinate system for transition animation when data changed

    this._coordSys = coordSys;
    this._stackedOnPoints = stackedOnPoints;
    this._points = points;
    this._step = step;
    this._valueOrigin = valueOrigin;
  },
  dispose: function dispose() {},
  highlight: function highlight(seriesModel, ecModel, api, payload) {
    var data = seriesModel.getData();
    var dataIndex = modelUtil.queryDataIndex(data, payload);

    if (!(dataIndex instanceof Array) && dataIndex != null && dataIndex >= 0) {
      var symbol = data.getItemGraphicEl(dataIndex);

      if (!symbol) {
        // Create a temporary symbol if it is not exists
        var pt = data.getItemLayout(dataIndex);

        if (!pt) {
          // Null data
          return;
        }

        symbol = new SymbolClz(data, dataIndex);
        symbol.position = pt;
        symbol.setZ(seriesModel.get('zlevel'), seriesModel.get('z'));
        symbol.ignore = isNaN(pt[0]) || isNaN(pt[1]);
        symbol.__temp = true;
        data.setItemGraphicEl(dataIndex, symbol); // Stop scale animation

        symbol.stopSymbolAnimation(true);
        this.group.add(symbol);
      }

      symbol.highlight();
    } else {
      // Highlight whole series
      ChartView.prototype.highlight.call(this, seriesModel, ecModel, api, payload);
    }
  },
  downplay: function downplay(seriesModel, ecModel, api, payload) {
    var data = seriesModel.getData();
    var dataIndex = modelUtil.queryDataIndex(data, payload);

    if (dataIndex != null && dataIndex >= 0) {
      var symbol = data.getItemGraphicEl(dataIndex);

      if (symbol) {
        if (symbol.__temp) {
          data.setItemGraphicEl(dataIndex, null);
          this.group.remove(symbol);
        } else {
          symbol.downplay();
        }
      }
    } else {
      // FIXME
      // can not downplay completely.
      // Downplay whole series
      ChartView.prototype.downplay.call(this, seriesModel, ecModel, api, payload);
    }
  },

  /**
   * @param {module:zrender/container/Group} group
   * @param {Array.<Array.<number>>} points
   * @private
   */
  _newPolyline: function _newPolyline(points) {
    var polyline = this._polyline; // Remove previous created polyline

    if (polyline) {
      this._lineGroup.remove(polyline);
    }

    polyline = new Polyline({
      shape: {
        points: points
      },
      silent: true,
      z2: 10
    });

    this._lineGroup.add(polyline);

    this._polyline = polyline;
    return polyline;
  },

  /**
   * @param {module:zrender/container/Group} group
   * @param {Array.<Array.<number>>} stackedOnPoints
   * @param {Array.<Array.<number>>} points
   * @private
   */
  _newPolygon: function _newPolygon(points, stackedOnPoints) {
    var polygon = this._polygon; // Remove previous created polygon

    if (polygon) {
      this._lineGroup.remove(polygon);
    }

    polygon = new Polygon({
      shape: {
        points: points,
        stackedOnPoints: stackedOnPoints
      },
      silent: true
    });

    this._lineGroup.add(polygon);

    this._polygon = polygon;
    return polygon;
  },

  /**
   * @private
   */
  // FIXME Two value axis
  _updateAnimation: function _updateAnimation(data, stackedOnPoints, coordSys, api, step, valueOrigin) {
    var polyline = this._polyline;
    var polygon = this._polygon;
    var seriesModel = data.hostModel;
    var diff = lineAnimationDiff(this._data, data, this._stackedOnPoints, stackedOnPoints, this._coordSys, coordSys, this._valueOrigin, valueOrigin);
    var current = diff.current;
    var stackedOnCurrent = diff.stackedOnCurrent;
    var next = diff.next;
    var stackedOnNext = diff.stackedOnNext;

    if (step) {
      // TODO If stacked series is not step
      current = turnPointsIntoStep(diff.current, coordSys, step);
      stackedOnCurrent = turnPointsIntoStep(diff.stackedOnCurrent, coordSys, step);
      next = turnPointsIntoStep(diff.next, coordSys, step);
      stackedOnNext = turnPointsIntoStep(diff.stackedOnNext, coordSys, step);
    } // `diff.current` is subset of `current` (which should be ensured by
    // turnPointsIntoStep), so points in `__points` can be updated when
    // points in `current` are update during animation.


    polyline.shape.__points = diff.current;
    polyline.shape.points = current;
    graphic.updateProps(polyline, {
      shape: {
        points: next
      }
    }, seriesModel);

    if (polygon) {
      polygon.setShape({
        points: current,
        stackedOnPoints: stackedOnCurrent
      });
      graphic.updateProps(polygon, {
        shape: {
          points: next,
          stackedOnPoints: stackedOnNext
        }
      }, seriesModel);
    }

    var updatedDataInfo = [];
    var diffStatus = diff.status;

    for (var i = 0; i < diffStatus.length; i++) {
      var cmd = diffStatus[i].cmd;

      if (cmd === '=') {
        var el = data.getItemGraphicEl(diffStatus[i].idx1);

        if (el) {
          updatedDataInfo.push({
            el: el,
            ptIdx: i // Index of points

          });
        }
      }
    }

    if (polyline.animators && polyline.animators.length) {
      polyline.animators[0].during(function () {
        for (var i = 0; i < updatedDataInfo.length; i++) {
          var el = updatedDataInfo[i].el;
          el.attr('position', polyline.shape.__points[updatedDataInfo[i].ptIdx]);
        }
      });
    }
  },
  remove: function remove(ecModel) {
    var group = this.group;
    var oldData = this._data;

    this._lineGroup.removeAll();

    this._symbolDraw.remove(true); // Remove temporary created elements when highlighting


    oldData && oldData.eachItemGraphicEl(function (el, idx) {
      if (el.__temp) {
        group.remove(el);
        oldData.setItemGraphicEl(idx, null);
      }
    });
    this._polyline = this._polygon = this._coordSys = this._points = this._stackedOnPoints = this._data = null;
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9MaW5lVmlldy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NoYXJ0L2xpbmUvTGluZVZpZXcuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfY29uZmlnID0gcmVxdWlyZShcIi4uLy4uL2NvbmZpZ1wiKTtcblxudmFyIF9fREVWX18gPSBfY29uZmlnLl9fREVWX187XG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgU3ltYm9sRHJhdyA9IHJlcXVpcmUoXCIuLi9oZWxwZXIvU3ltYm9sRHJhd1wiKTtcblxudmFyIFN5bWJvbENseiA9IHJlcXVpcmUoXCIuLi9oZWxwZXIvU3ltYm9sXCIpO1xuXG52YXIgbGluZUFuaW1hdGlvbkRpZmYgPSByZXF1aXJlKFwiLi9saW5lQW5pbWF0aW9uRGlmZlwiKTtcblxudmFyIGdyYXBoaWMgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9ncmFwaGljXCIpO1xuXG52YXIgbW9kZWxVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBfcG9seSA9IHJlcXVpcmUoXCIuL3BvbHlcIik7XG5cbnZhciBQb2x5bGluZSA9IF9wb2x5LlBvbHlsaW5lO1xudmFyIFBvbHlnb24gPSBfcG9seS5Qb2x5Z29uO1xuXG52YXIgQ2hhcnRWaWV3ID0gcmVxdWlyZShcIi4uLy4uL3ZpZXcvQ2hhcnRcIik7XG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgcm91bmQgPSBfbnVtYmVyLnJvdW5kO1xuXG52YXIgX2hlbHBlciA9IHJlcXVpcmUoXCIuL2hlbHBlclwiKTtcblxudmFyIHByZXBhcmVEYXRhQ29vcmRJbmZvID0gX2hlbHBlci5wcmVwYXJlRGF0YUNvb3JkSW5mbztcbnZhciBnZXRTdGFja2VkT25Qb2ludCA9IF9oZWxwZXIuZ2V0U3RhY2tlZE9uUG9pbnQ7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIEZJWE1FIHN0ZXAgbm90IHN1cHBvcnQgcG9sYXJcbmZ1bmN0aW9uIGlzUG9pbnRzU2FtZShwb2ludHMxLCBwb2ludHMyKSB7XG4gIGlmIChwb2ludHMxLmxlbmd0aCAhPT0gcG9pbnRzMi5sZW5ndGgpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHBvaW50czEubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgcDEgPSBwb2ludHMxW2ldO1xuICAgIHZhciBwMiA9IHBvaW50czJbaV07XG5cbiAgICBpZiAocDFbMF0gIT09IHAyWzBdIHx8IHAxWzFdICE9PSBwMlsxXSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufVxuXG5mdW5jdGlvbiBnZXRTbW9vdGgoc21vb3RoKSB7XG4gIHJldHVybiB0eXBlb2Ygc21vb3RoID09PSAnbnVtYmVyJyA/IHNtb290aCA6IHNtb290aCA/IDAuNSA6IDA7XG59XG5cbmZ1bmN0aW9uIGdldEF4aXNFeHRlbnRXaXRoR2FwKGF4aXMpIHtcbiAgdmFyIGV4dGVudCA9IGF4aXMuZ2V0R2xvYmFsRXh0ZW50KCk7XG5cbiAgaWYgKGF4aXMub25CYW5kKSB7XG4gICAgLy8gUmVtb3ZlIGV4dHJhIDFweCB0byBhdm9pZCBsaW5lIG1pdGVyIGluIGNsaXBwZWQgZWRnZVxuICAgIHZhciBoYWxmQmFuZFdpZHRoID0gYXhpcy5nZXRCYW5kV2lkdGgoKSAvIDIgLSAxO1xuICAgIHZhciBkaXIgPSBleHRlbnRbMV0gPiBleHRlbnRbMF0gPyAxIDogLTE7XG4gICAgZXh0ZW50WzBdICs9IGRpciAqIGhhbGZCYW5kV2lkdGg7XG4gICAgZXh0ZW50WzFdIC09IGRpciAqIGhhbGZCYW5kV2lkdGg7XG4gIH1cblxuICByZXR1cm4gZXh0ZW50O1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL2NhcnRlc2lhbi9DYXJ0ZXNpYW4yRHxtb2R1bGU6ZWNoYXJ0cy9jb29yZC9wb2xhci9Qb2xhcn0gY29vcmRTeXNcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fSBkYXRhXG4gKiBAcGFyYW0ge09iamVjdH0gZGF0YUNvb3JkSW5mb1xuICogQHBhcmFtIHtBcnJheS48QXJyYXkuPG51bWJlcj4+fSBwb2ludHNcbiAqL1xuXG5cbmZ1bmN0aW9uIGdldFN0YWNrZWRPblBvaW50cyhjb29yZFN5cywgZGF0YSwgZGF0YUNvb3JkSW5mbykge1xuICBpZiAoIWRhdGFDb29yZEluZm8udmFsdWVEaW0pIHtcbiAgICByZXR1cm4gW107XG4gIH1cblxuICB2YXIgcG9pbnRzID0gW107XG5cbiAgZm9yICh2YXIgaWR4ID0gMCwgbGVuID0gZGF0YS5jb3VudCgpOyBpZHggPCBsZW47IGlkeCsrKSB7XG4gICAgcG9pbnRzLnB1c2goZ2V0U3RhY2tlZE9uUG9pbnQoZGF0YUNvb3JkSW5mbywgY29vcmRTeXMsIGRhdGEsIGlkeCkpO1xuICB9XG5cbiAgcmV0dXJuIHBvaW50cztcbn1cblxuZnVuY3Rpb24gY3JlYXRlR3JpZENsaXBTaGFwZShjYXJ0ZXNpYW4sIGhhc0FuaW1hdGlvbiwgZm9yU3ltYm9sLCBzZXJpZXNNb2RlbCkge1xuICB2YXIgeEV4dGVudCA9IGdldEF4aXNFeHRlbnRXaXRoR2FwKGNhcnRlc2lhbi5nZXRBeGlzKCd4JykpO1xuICB2YXIgeUV4dGVudCA9IGdldEF4aXNFeHRlbnRXaXRoR2FwKGNhcnRlc2lhbi5nZXRBeGlzKCd5JykpO1xuICB2YXIgaXNIb3Jpem9udGFsID0gY2FydGVzaWFuLmdldEJhc2VBeGlzKCkuaXNIb3Jpem9udGFsKCk7XG4gIHZhciB4ID0gTWF0aC5taW4oeEV4dGVudFswXSwgeEV4dGVudFsxXSk7XG4gIHZhciB5ID0gTWF0aC5taW4oeUV4dGVudFswXSwgeUV4dGVudFsxXSk7XG4gIHZhciB3aWR0aCA9IE1hdGgubWF4KHhFeHRlbnRbMF0sIHhFeHRlbnRbMV0pIC0geDtcbiAgdmFyIGhlaWdodCA9IE1hdGgubWF4KHlFeHRlbnRbMF0sIHlFeHRlbnRbMV0pIC0geTsgLy8gQXZvaWQgZmxvYXQgbnVtYmVyIHJvdW5kaW5nIGVycm9yIGZvciBzeW1ib2wgb24gdGhlIGVkZ2Ugb2YgYXhpcyBleHRlbnQuXG4gIC8vIFNlZSAjNzkxMyBhbmQgYHRlc3QvZGF0YVpvb20tY2xpcC5odG1sYC5cblxuICBpZiAoZm9yU3ltYm9sKSB7XG4gICAgeCAtPSAwLjU7XG4gICAgd2lkdGggKz0gMC41O1xuICAgIHkgLT0gMC41O1xuICAgIGhlaWdodCArPSAwLjU7XG4gIH0gZWxzZSB7XG4gICAgdmFyIGxpbmVXaWR0aCA9IHNlcmllc01vZGVsLmdldCgnbGluZVN0eWxlLndpZHRoJykgfHwgMjsgLy8gRXhwYW5kIGNsaXAgc2hhcGUgdG8gYXZvaWQgY2xpcHBpbmcgd2hlbiBsaW5lIHZhbHVlIGV4Y2VlZHMgYXhpc1xuXG4gICAgdmFyIGV4cGFuZFNpemUgPSBzZXJpZXNNb2RlbC5nZXQoJ2NsaXBPdmVyZmxvdycpID8gbGluZVdpZHRoIC8gMiA6IE1hdGgubWF4KHdpZHRoLCBoZWlnaHQpO1xuXG4gICAgaWYgKGlzSG9yaXpvbnRhbCkge1xuICAgICAgeSAtPSBleHBhbmRTaXplO1xuICAgICAgaGVpZ2h0ICs9IGV4cGFuZFNpemUgKiAyO1xuICAgIH0gZWxzZSB7XG4gICAgICB4IC09IGV4cGFuZFNpemU7XG4gICAgICB3aWR0aCArPSBleHBhbmRTaXplICogMjtcbiAgICB9XG4gIH1cblxuICB2YXIgY2xpcFBhdGggPSBuZXcgZ3JhcGhpYy5SZWN0KHtcbiAgICBzaGFwZToge1xuICAgICAgeDogeCxcbiAgICAgIHk6IHksXG4gICAgICB3aWR0aDogd2lkdGgsXG4gICAgICBoZWlnaHQ6IGhlaWdodFxuICAgIH1cbiAgfSk7XG5cbiAgaWYgKGhhc0FuaW1hdGlvbikge1xuICAgIGNsaXBQYXRoLnNoYXBlW2lzSG9yaXpvbnRhbCA/ICd3aWR0aCcgOiAnaGVpZ2h0J10gPSAwO1xuICAgIGdyYXBoaWMuaW5pdFByb3BzKGNsaXBQYXRoLCB7XG4gICAgICBzaGFwZToge1xuICAgICAgICB3aWR0aDogd2lkdGgsXG4gICAgICAgIGhlaWdodDogaGVpZ2h0XG4gICAgICB9XG4gICAgfSwgc2VyaWVzTW9kZWwpO1xuICB9XG5cbiAgcmV0dXJuIGNsaXBQYXRoO1xufVxuXG5mdW5jdGlvbiBjcmVhdGVQb2xhckNsaXBTaGFwZShwb2xhciwgaGFzQW5pbWF0aW9uLCBmb3JTeW1ib2wsIHNlcmllc01vZGVsKSB7XG4gIHZhciBhbmdsZUF4aXMgPSBwb2xhci5nZXRBbmdsZUF4aXMoKTtcbiAgdmFyIHJhZGl1c0F4aXMgPSBwb2xhci5nZXRSYWRpdXNBeGlzKCk7XG4gIHZhciByYWRpdXNFeHRlbnQgPSByYWRpdXNBeGlzLmdldEV4dGVudCgpLnNsaWNlKCk7XG4gIHJhZGl1c0V4dGVudFswXSA+IHJhZGl1c0V4dGVudFsxXSAmJiByYWRpdXNFeHRlbnQucmV2ZXJzZSgpO1xuICB2YXIgYW5nbGVFeHRlbnQgPSBhbmdsZUF4aXMuZ2V0RXh0ZW50KCk7XG4gIHZhciBSQURJQU4gPSBNYXRoLlBJIC8gMTgwOyAvLyBBdm9pZCBmbG9hdCBudW1iZXIgcm91bmRpbmcgZXJyb3IgZm9yIHN5bWJvbCBvbiB0aGUgZWRnZSBvZiBheGlzIGV4dGVudC5cblxuICBpZiAoZm9yU3ltYm9sKSB7XG4gICAgcmFkaXVzRXh0ZW50WzBdIC09IDAuNTtcbiAgICByYWRpdXNFeHRlbnRbMV0gKz0gMC41O1xuICB9XG5cbiAgdmFyIGNsaXBQYXRoID0gbmV3IGdyYXBoaWMuU2VjdG9yKHtcbiAgICBzaGFwZToge1xuICAgICAgY3g6IHJvdW5kKHBvbGFyLmN4LCAxKSxcbiAgICAgIGN5OiByb3VuZChwb2xhci5jeSwgMSksXG4gICAgICByMDogcm91bmQocmFkaXVzRXh0ZW50WzBdLCAxKSxcbiAgICAgIHI6IHJvdW5kKHJhZGl1c0V4dGVudFsxXSwgMSksXG4gICAgICBzdGFydEFuZ2xlOiAtYW5nbGVFeHRlbnRbMF0gKiBSQURJQU4sXG4gICAgICBlbmRBbmdsZTogLWFuZ2xlRXh0ZW50WzFdICogUkFESUFOLFxuICAgICAgY2xvY2t3aXNlOiBhbmdsZUF4aXMuaW52ZXJzZVxuICAgIH1cbiAgfSk7XG5cbiAgaWYgKGhhc0FuaW1hdGlvbikge1xuICAgIGNsaXBQYXRoLnNoYXBlLmVuZEFuZ2xlID0gLWFuZ2xlRXh0ZW50WzBdICogUkFESUFOO1xuICAgIGdyYXBoaWMuaW5pdFByb3BzKGNsaXBQYXRoLCB7XG4gICAgICBzaGFwZToge1xuICAgICAgICBlbmRBbmdsZTogLWFuZ2xlRXh0ZW50WzFdICogUkFESUFOXG4gICAgICB9XG4gICAgfSwgc2VyaWVzTW9kZWwpO1xuICB9XG5cbiAgcmV0dXJuIGNsaXBQYXRoO1xufVxuXG5mdW5jdGlvbiBjcmVhdGVDbGlwU2hhcGUoY29vcmRTeXMsIGhhc0FuaW1hdGlvbiwgZm9yU3ltYm9sLCBzZXJpZXNNb2RlbCkge1xuICByZXR1cm4gY29vcmRTeXMudHlwZSA9PT0gJ3BvbGFyJyA/IGNyZWF0ZVBvbGFyQ2xpcFNoYXBlKGNvb3JkU3lzLCBoYXNBbmltYXRpb24sIGZvclN5bWJvbCwgc2VyaWVzTW9kZWwpIDogY3JlYXRlR3JpZENsaXBTaGFwZShjb29yZFN5cywgaGFzQW5pbWF0aW9uLCBmb3JTeW1ib2wsIHNlcmllc01vZGVsKTtcbn1cblxuZnVuY3Rpb24gdHVyblBvaW50c0ludG9TdGVwKHBvaW50cywgY29vcmRTeXMsIHN0ZXBUdXJuQXQpIHtcbiAgdmFyIGJhc2VBeGlzID0gY29vcmRTeXMuZ2V0QmFzZUF4aXMoKTtcbiAgdmFyIGJhc2VJbmRleCA9IGJhc2VBeGlzLmRpbSA9PT0gJ3gnIHx8IGJhc2VBeGlzLmRpbSA9PT0gJ3JhZGl1cycgPyAwIDogMTtcbiAgdmFyIHN0ZXBQb2ludHMgPSBbXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHBvaW50cy5sZW5ndGggLSAxOyBpKyspIHtcbiAgICB2YXIgbmV4dFB0ID0gcG9pbnRzW2kgKyAxXTtcbiAgICB2YXIgcHQgPSBwb2ludHNbaV07XG4gICAgc3RlcFBvaW50cy5wdXNoKHB0KTtcbiAgICB2YXIgc3RlcFB0ID0gW107XG5cbiAgICBzd2l0Y2ggKHN0ZXBUdXJuQXQpIHtcbiAgICAgIGNhc2UgJ2VuZCc6XG4gICAgICAgIHN0ZXBQdFtiYXNlSW5kZXhdID0gbmV4dFB0W2Jhc2VJbmRleF07XG4gICAgICAgIHN0ZXBQdFsxIC0gYmFzZUluZGV4XSA9IHB0WzEgLSBiYXNlSW5kZXhdOyAvLyBkZWZhdWx0IGlzIHN0YXJ0XG5cbiAgICAgICAgc3RlcFBvaW50cy5wdXNoKHN0ZXBQdCk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlICdtaWRkbGUnOlxuICAgICAgICAvLyBkZWZhdWx0IGlzIHN0YXJ0XG4gICAgICAgIHZhciBtaWRkbGUgPSAocHRbYmFzZUluZGV4XSArIG5leHRQdFtiYXNlSW5kZXhdKSAvIDI7XG4gICAgICAgIHZhciBzdGVwUHQyID0gW107XG4gICAgICAgIHN0ZXBQdFtiYXNlSW5kZXhdID0gc3RlcFB0MltiYXNlSW5kZXhdID0gbWlkZGxlO1xuICAgICAgICBzdGVwUHRbMSAtIGJhc2VJbmRleF0gPSBwdFsxIC0gYmFzZUluZGV4XTtcbiAgICAgICAgc3RlcFB0MlsxIC0gYmFzZUluZGV4XSA9IG5leHRQdFsxIC0gYmFzZUluZGV4XTtcbiAgICAgICAgc3RlcFBvaW50cy5wdXNoKHN0ZXBQdCk7XG4gICAgICAgIHN0ZXBQb2ludHMucHVzaChzdGVwUHQyKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHN0ZXBQdFtiYXNlSW5kZXhdID0gcHRbYmFzZUluZGV4XTtcbiAgICAgICAgc3RlcFB0WzEgLSBiYXNlSW5kZXhdID0gbmV4dFB0WzEgLSBiYXNlSW5kZXhdOyAvLyBkZWZhdWx0IGlzIHN0YXJ0XG5cbiAgICAgICAgc3RlcFBvaW50cy5wdXNoKHN0ZXBQdCk7XG4gICAgfVxuICB9IC8vIExhc3QgcG9pbnRzXG5cblxuICBwb2ludHNbaV0gJiYgc3RlcFBvaW50cy5wdXNoKHBvaW50c1tpXSk7XG4gIHJldHVybiBzdGVwUG9pbnRzO1xufVxuXG5mdW5jdGlvbiBnZXRWaXN1YWxHcmFkaWVudChkYXRhLCBjb29yZFN5cykge1xuICB2YXIgdmlzdWFsTWV0YUxpc3QgPSBkYXRhLmdldFZpc3VhbCgndmlzdWFsTWV0YScpO1xuXG4gIGlmICghdmlzdWFsTWV0YUxpc3QgfHwgIXZpc3VhbE1ldGFMaXN0Lmxlbmd0aCB8fCAhZGF0YS5jb3VudCgpKSB7XG4gICAgLy8gV2hlbiBkYXRhLmNvdW50KCkgaXMgMCwgZ3JhZGllbnQgcmFuZ2UgY2FuIG5vdCBiZSBjYWxjdWxhdGVkLlxuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChjb29yZFN5cy50eXBlICE9PSAnY2FydGVzaWFuMmQnKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGNvb3JkRGltO1xuICB2YXIgdmlzdWFsTWV0YTtcblxuICBmb3IgKHZhciBpID0gdmlzdWFsTWV0YUxpc3QubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICB2YXIgZGltSW5kZXggPSB2aXN1YWxNZXRhTGlzdFtpXS5kaW1lbnNpb247XG4gICAgdmFyIGRpbU5hbWUgPSBkYXRhLmRpbWVuc2lvbnNbZGltSW5kZXhdO1xuICAgIHZhciBkaW1JbmZvID0gZGF0YS5nZXREaW1lbnNpb25JbmZvKGRpbU5hbWUpO1xuICAgIGNvb3JkRGltID0gZGltSW5mbyAmJiBkaW1JbmZvLmNvb3JkRGltOyAvLyBDYW4gb25seSBiZSB4IG9yIHlcblxuICAgIGlmIChjb29yZERpbSA9PT0gJ3gnIHx8IGNvb3JkRGltID09PSAneScpIHtcbiAgICAgIHZpc3VhbE1ldGEgPSB2aXN1YWxNZXRhTGlzdFtpXTtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIGlmICghdmlzdWFsTWV0YSkge1xuICAgIHJldHVybjtcbiAgfSAvLyBJZiB0aGUgYXJlYSB0byBiZSByZW5kZXJlZCBpcyBiaWdnZXIgdGhhbiBhcmVhIGRlZmluZWQgYnkgTGluZWFyR3JhZGllbnQsXG4gIC8vIHRoZSBjYW52YXMgc3BlYyBwcmVzY3JpYmVzIHRoYXQgdGhlIGNvbG9yIG9mIHRoZSBmaXJzdCBzdG9wIGFuZCB0aGUgbGFzdFxuICAvLyBzdG9wIHNob3VsZCBiZSB1c2VkLiBCdXQgaWYgdHdvIHN0b3BzIGFyZSBhZGRlZCBhdCBvZmZzZXQgMCwgaW4gZWZmZWN0XG4gIC8vIGJyb3dzZXJzIHVzZSB0aGUgY29sb3Igb2YgdGhlIHNlY29uZCBzdG9wIHRvIHJlbmRlciBhcmVhIG91dHNpZGVcbiAgLy8gTGluZWFyR3JhZGllbnQuIFNvIHdlIGNhbiBvbmx5IGluZmluaXRlc2ltYWxseSBleHRlbmQgYXJlYSBkZWZpbmVkIGluXG4gIC8vIExpbmVhckdyYWRpZW50IHRvIHJlbmRlciBgb3V0ZXJDb2xvcnNgLlxuXG5cbiAgdmFyIGF4aXMgPSBjb29yZFN5cy5nZXRBeGlzKGNvb3JkRGltKTsgLy8gZGF0YVRvQ29vciBtYXBwaW5nIG1heSBub3QgYmUgbGluZWFyLCBidXQgbXVzdCBiZSBtb25vdG9uaWMuXG5cbiAgdmFyIGNvbG9yU3RvcHMgPSB6clV0aWwubWFwKHZpc3VhbE1ldGEuc3RvcHMsIGZ1bmN0aW9uIChzdG9wKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGNvb3JkOiBheGlzLnRvR2xvYmFsQ29vcmQoYXhpcy5kYXRhVG9Db29yZChzdG9wLnZhbHVlKSksXG4gICAgICBjb2xvcjogc3RvcC5jb2xvclxuICAgIH07XG4gIH0pO1xuICB2YXIgc3RvcExlbiA9IGNvbG9yU3RvcHMubGVuZ3RoO1xuICB2YXIgb3V0ZXJDb2xvcnMgPSB2aXN1YWxNZXRhLm91dGVyQ29sb3JzLnNsaWNlKCk7XG5cbiAgaWYgKHN0b3BMZW4gJiYgY29sb3JTdG9wc1swXS5jb29yZCA+IGNvbG9yU3RvcHNbc3RvcExlbiAtIDFdLmNvb3JkKSB7XG4gICAgY29sb3JTdG9wcy5yZXZlcnNlKCk7XG4gICAgb3V0ZXJDb2xvcnMucmV2ZXJzZSgpO1xuICB9XG5cbiAgdmFyIHRpbnlFeHRlbnQgPSAxMDsgLy8gQXJiaXRyYXJ5IHZhbHVlOiAxMHB4XG5cbiAgdmFyIG1pbkNvb3JkID0gY29sb3JTdG9wc1swXS5jb29yZCAtIHRpbnlFeHRlbnQ7XG4gIHZhciBtYXhDb29yZCA9IGNvbG9yU3RvcHNbc3RvcExlbiAtIDFdLmNvb3JkICsgdGlueUV4dGVudDtcbiAgdmFyIGNvb3JkU3BhbiA9IG1heENvb3JkIC0gbWluQ29vcmQ7XG5cbiAgaWYgKGNvb3JkU3BhbiA8IDFlLTMpIHtcbiAgICByZXR1cm4gJ3RyYW5zcGFyZW50JztcbiAgfVxuXG4gIHpyVXRpbC5lYWNoKGNvbG9yU3RvcHMsIGZ1bmN0aW9uIChzdG9wKSB7XG4gICAgc3RvcC5vZmZzZXQgPSAoc3RvcC5jb29yZCAtIG1pbkNvb3JkKSAvIGNvb3JkU3BhbjtcbiAgfSk7XG4gIGNvbG9yU3RvcHMucHVzaCh7XG4gICAgb2Zmc2V0OiBzdG9wTGVuID8gY29sb3JTdG9wc1tzdG9wTGVuIC0gMV0ub2Zmc2V0IDogMC41LFxuICAgIGNvbG9yOiBvdXRlckNvbG9yc1sxXSB8fCAndHJhbnNwYXJlbnQnXG4gIH0pO1xuICBjb2xvclN0b3BzLnVuc2hpZnQoe1xuICAgIC8vIG5vdGljZSBjb2xvclN0b3BzLmxlbmd0aCBoYXZlIGJlZW4gY2hhbmdlZC5cbiAgICBvZmZzZXQ6IHN0b3BMZW4gPyBjb2xvclN0b3BzWzBdLm9mZnNldCA6IDAuNSxcbiAgICBjb2xvcjogb3V0ZXJDb2xvcnNbMF0gfHwgJ3RyYW5zcGFyZW50J1xuICB9KTsgLy8genJVdGlsLmVhY2goY29sb3JTdG9wcywgZnVuY3Rpb24gKGNvbG9yU3RvcCkge1xuICAvLyAgICAgLy8gTWFrZSBzdXJlIGVhY2ggb2Zmc2V0IGhhcyByb3VuZGVkIHB4IHRvIGF2b2lkIG5vdCBzaGFycCBlZGdlXG4gIC8vICAgICBjb2xvclN0b3Aub2Zmc2V0ID0gKE1hdGgucm91bmQoY29sb3JTdG9wLm9mZnNldCAqIChlbmQgLSBzdGFydCkgKyBzdGFydCkgLSBzdGFydCkgLyAoZW5kIC0gc3RhcnQpO1xuICAvLyB9KTtcblxuICB2YXIgZ3JhZGllbnQgPSBuZXcgZ3JhcGhpYy5MaW5lYXJHcmFkaWVudCgwLCAwLCAwLCAwLCBjb2xvclN0b3BzLCB0cnVlKTtcbiAgZ3JhZGllbnRbY29vcmREaW1dID0gbWluQ29vcmQ7XG4gIGdyYWRpZW50W2Nvb3JkRGltICsgJzInXSA9IG1heENvb3JkO1xuICByZXR1cm4gZ3JhZGllbnQ7XG59XG5cbmZ1bmN0aW9uIGdldElzSWdub3JlRnVuYyhzZXJpZXNNb2RlbCwgZGF0YSwgY29vcmRTeXMpIHtcbiAgdmFyIHNob3dBbGxTeW1ib2wgPSBzZXJpZXNNb2RlbC5nZXQoJ3Nob3dBbGxTeW1ib2wnKTtcbiAgdmFyIGlzQXV0byA9IHNob3dBbGxTeW1ib2wgPT09ICdhdXRvJztcblxuICBpZiAoc2hvd0FsbFN5bWJvbCAmJiAhaXNBdXRvKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIGNhdGVnb3J5QXhpcyA9IGNvb3JkU3lzLmdldEF4ZXNCeVNjYWxlKCdvcmRpbmFsJylbMF07XG5cbiAgaWYgKCFjYXRlZ29yeUF4aXMpIHtcbiAgICByZXR1cm47XG4gIH0gLy8gTm90ZSB0aGF0IGNhdGVnb3J5IGxhYmVsIGludGVydmFsIHN0cmF0ZWd5IG1pZ2h0IGJyaW5nIHNvbWUgd2VpcmQgZWZmZWN0XG4gIC8vIGluIHNvbWUgc2NlbmFyaW86IHVzZXJzIG1heSB3b25kZXIgd2h5IHNvbWUgb2YgdGhlIHN5bWJvbHMgYXJlIG5vdFxuICAvLyBkaXNwbGF5ZWQuIFNvIHdlIHNob3cgYWxsIHN5bWJvbHMgYXMgcG9zc2libGUgYXMgd2UgY2FuLlxuXG5cbiAgaWYgKGlzQXV0byAvLyBTaW1wbGlmeSB0aGUgbG9naWMsIGRvIG5vdCBkZXRlcm1pbmUgbGFiZWwgb3ZlcmxhcCBoZXJlLlxuICAmJiBjYW5TaG93QWxsU3ltYm9sRm9yQ2F0ZWdvcnkoY2F0ZWdvcnlBeGlzLCBkYXRhKSkge1xuICAgIHJldHVybjtcbiAgfSAvLyBPdGhlcndpc2UgZm9sbG93IHRoZSBsYWJlbCBpbnRlcnZhbCBzdHJhdGVneSBvbiBjYXRlZ29yeSBheGlzLlxuXG5cbiAgdmFyIGNhdGVnb3J5RGF0YURpbSA9IGRhdGEubWFwRGltZW5zaW9uKGNhdGVnb3J5QXhpcy5kaW0pO1xuICB2YXIgbGFiZWxNYXAgPSB7fTtcbiAgenJVdGlsLmVhY2goY2F0ZWdvcnlBeGlzLmdldFZpZXdMYWJlbHMoKSwgZnVuY3Rpb24gKGxhYmVsSXRlbSkge1xuICAgIGxhYmVsTWFwW2xhYmVsSXRlbS50aWNrVmFsdWVdID0gMTtcbiAgfSk7XG4gIHJldHVybiBmdW5jdGlvbiAoZGF0YUluZGV4KSB7XG4gICAgcmV0dXJuICFsYWJlbE1hcC5oYXNPd25Qcm9wZXJ0eShkYXRhLmdldChjYXRlZ29yeURhdGFEaW0sIGRhdGFJbmRleCkpO1xuICB9O1xufVxuXG5mdW5jdGlvbiBjYW5TaG93QWxsU3ltYm9sRm9yQ2F0ZWdvcnkoY2F0ZWdvcnlBeGlzLCBkYXRhKSB7XG4gIC8vIEluIG1vc2UgY2FzZXMsIGxpbmUgaXMgbW9ub3Rvbm91cyBvbiBjYXRlZ29yeSBheGlzLCBhbmQgdGhlIGxhYmVsIHNpemVcbiAgLy8gaXMgY2xvc2Ugd2l0aCBlYWNoIG90aGVyLiBTbyB3ZSBjaGVjayB0aGUgc3ltYm9sIHNpemUgYW5kIHNvbWUgb2YgdGhlXG4gIC8vIGxhYmVsIHNpemUgYWxvbmUgd2l0aCB0aGUgY2F0ZWdvcnkgYXhpcyB0byBlc3RpbWF0ZSB3aGV0aGVyIGFsbCBzeW1ib2xcbiAgLy8gY2FuIGJlIHNob3duIHdpdGhvdXQgb3ZlcmxhcC5cbiAgdmFyIGF4aXNFeHRlbnQgPSBjYXRlZ29yeUF4aXMuZ2V0RXh0ZW50KCk7XG4gIHZhciBhdmFpbFNpemUgPSBNYXRoLmFicyhheGlzRXh0ZW50WzFdIC0gYXhpc0V4dGVudFswXSkgLyBjYXRlZ29yeUF4aXMuc2NhbGUuY291bnQoKTtcbiAgaXNOYU4oYXZhaWxTaXplKSAmJiAoYXZhaWxTaXplID0gMCk7IC8vIDAvMCBpcyBOYU4uXG4gIC8vIFNhbXBsaW5nIHNvbWUgcG9pbnRzLCBtYXggNS5cblxuICB2YXIgZGF0YUxlbiA9IGRhdGEuY291bnQoKTtcbiAgdmFyIHN0ZXAgPSBNYXRoLm1heCgxLCBNYXRoLnJvdW5kKGRhdGFMZW4gLyA1KSk7XG5cbiAgZm9yICh2YXIgZGF0YUluZGV4ID0gMDsgZGF0YUluZGV4IDwgZGF0YUxlbjsgZGF0YUluZGV4ICs9IHN0ZXApIHtcbiAgICBpZiAoU3ltYm9sQ2x6LmdldFN5bWJvbFNpemUoZGF0YSwgZGF0YUluZGV4IC8vIE9ubHkgZm9yIGNhcnRlc2lhbiwgd2hlcmUgYGlzSG9yaXpvbnRhbGAgZXhpc3RzLlxuICAgIClbY2F0ZWdvcnlBeGlzLmlzSG9yaXpvbnRhbCgpID8gMSA6IDBdIC8vIEVtcGlyaWNhbCBudW1iZXJcbiAgICAqIDEuNSA+IGF2YWlsU2l6ZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufVxuXG52YXIgX2RlZmF1bHQgPSBDaGFydFZpZXcuZXh0ZW5kKHtcbiAgdHlwZTogJ2xpbmUnLFxuICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGxpbmVHcm91cCA9IG5ldyBncmFwaGljLkdyb3VwKCk7XG4gICAgdmFyIHN5bWJvbERyYXcgPSBuZXcgU3ltYm9sRHJhdygpO1xuICAgIHRoaXMuZ3JvdXAuYWRkKHN5bWJvbERyYXcuZ3JvdXApO1xuICAgIHRoaXMuX3N5bWJvbERyYXcgPSBzeW1ib2xEcmF3O1xuICAgIHRoaXMuX2xpbmVHcm91cCA9IGxpbmVHcm91cDtcbiAgfSxcbiAgcmVuZGVyOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSkge1xuICAgIHZhciBjb29yZFN5cyA9IHNlcmllc01vZGVsLmNvb3JkaW5hdGVTeXN0ZW07XG4gICAgdmFyIGdyb3VwID0gdGhpcy5ncm91cDtcbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgbGluZVN0eWxlTW9kZWwgPSBzZXJpZXNNb2RlbC5nZXRNb2RlbCgnbGluZVN0eWxlJyk7XG4gICAgdmFyIGFyZWFTdHlsZU1vZGVsID0gc2VyaWVzTW9kZWwuZ2V0TW9kZWwoJ2FyZWFTdHlsZScpO1xuICAgIHZhciBwb2ludHMgPSBkYXRhLm1hcEFycmF5KGRhdGEuZ2V0SXRlbUxheW91dCk7XG4gICAgdmFyIGlzQ29vcmRTeXNQb2xhciA9IGNvb3JkU3lzLnR5cGUgPT09ICdwb2xhcic7XG4gICAgdmFyIHByZXZDb29yZFN5cyA9IHRoaXMuX2Nvb3JkU3lzO1xuICAgIHZhciBzeW1ib2xEcmF3ID0gdGhpcy5fc3ltYm9sRHJhdztcbiAgICB2YXIgcG9seWxpbmUgPSB0aGlzLl9wb2x5bGluZTtcbiAgICB2YXIgcG9seWdvbiA9IHRoaXMuX3BvbHlnb247XG4gICAgdmFyIGxpbmVHcm91cCA9IHRoaXMuX2xpbmVHcm91cDtcbiAgICB2YXIgaGFzQW5pbWF0aW9uID0gc2VyaWVzTW9kZWwuZ2V0KCdhbmltYXRpb24nKTtcbiAgICB2YXIgaXNBcmVhQ2hhcnQgPSAhYXJlYVN0eWxlTW9kZWwuaXNFbXB0eSgpO1xuICAgIHZhciB2YWx1ZU9yaWdpbiA9IGFyZWFTdHlsZU1vZGVsLmdldCgnb3JpZ2luJyk7XG4gICAgdmFyIGRhdGFDb29yZEluZm8gPSBwcmVwYXJlRGF0YUNvb3JkSW5mbyhjb29yZFN5cywgZGF0YSwgdmFsdWVPcmlnaW4pO1xuICAgIHZhciBzdGFja2VkT25Qb2ludHMgPSBnZXRTdGFja2VkT25Qb2ludHMoY29vcmRTeXMsIGRhdGEsIGRhdGFDb29yZEluZm8pO1xuICAgIHZhciBzaG93U3ltYm9sID0gc2VyaWVzTW9kZWwuZ2V0KCdzaG93U3ltYm9sJyk7XG4gICAgdmFyIGlzSWdub3JlRnVuYyA9IHNob3dTeW1ib2wgJiYgIWlzQ29vcmRTeXNQb2xhciAmJiBnZXRJc0lnbm9yZUZ1bmMoc2VyaWVzTW9kZWwsIGRhdGEsIGNvb3JkU3lzKTsgLy8gUmVtb3ZlIHRlbXBvcmFyeSBzeW1ib2xzXG5cbiAgICB2YXIgb2xkRGF0YSA9IHRoaXMuX2RhdGE7XG4gICAgb2xkRGF0YSAmJiBvbGREYXRhLmVhY2hJdGVtR3JhcGhpY0VsKGZ1bmN0aW9uIChlbCwgaWR4KSB7XG4gICAgICBpZiAoZWwuX190ZW1wKSB7XG4gICAgICAgIGdyb3VwLnJlbW92ZShlbCk7XG4gICAgICAgIG9sZERhdGEuc2V0SXRlbUdyYXBoaWNFbChpZHgsIG51bGwpO1xuICAgICAgfVxuICAgIH0pOyAvLyBSZW1vdmUgcHJldmlvdXMgY3JlYXRlZCBzeW1ib2xzIGlmIHNob3dTeW1ib2wgY2hhbmdlZCB0byBmYWxzZVxuXG4gICAgaWYgKCFzaG93U3ltYm9sKSB7XG4gICAgICBzeW1ib2xEcmF3LnJlbW92ZSgpO1xuICAgIH1cblxuICAgIGdyb3VwLmFkZChsaW5lR3JvdXApOyAvLyBGSVhNRSBzdGVwIG5vdCBzdXBwb3J0IHBvbGFyXG5cbiAgICB2YXIgc3RlcCA9ICFpc0Nvb3JkU3lzUG9sYXIgJiYgc2VyaWVzTW9kZWwuZ2V0KCdzdGVwJyk7IC8vIEluaXRpYWxpemF0aW9uIGFuaW1hdGlvbiBvciBjb29yZGluYXRlIHN5c3RlbSBjaGFuZ2VkXG5cbiAgICBpZiAoIShwb2x5bGluZSAmJiBwcmV2Q29vcmRTeXMudHlwZSA9PT0gY29vcmRTeXMudHlwZSAmJiBzdGVwID09PSB0aGlzLl9zdGVwKSkge1xuICAgICAgc2hvd1N5bWJvbCAmJiBzeW1ib2xEcmF3LnVwZGF0ZURhdGEoZGF0YSwge1xuICAgICAgICBpc0lnbm9yZTogaXNJZ25vcmVGdW5jLFxuICAgICAgICBjbGlwU2hhcGU6IGNyZWF0ZUNsaXBTaGFwZShjb29yZFN5cywgZmFsc2UsIHRydWUsIHNlcmllc01vZGVsKVxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGVwKSB7XG4gICAgICAgIC8vIFRPRE8gSWYgc3RhY2tlZCBzZXJpZXMgaXMgbm90IHN0ZXBcbiAgICAgICAgcG9pbnRzID0gdHVyblBvaW50c0ludG9TdGVwKHBvaW50cywgY29vcmRTeXMsIHN0ZXApO1xuICAgICAgICBzdGFja2VkT25Qb2ludHMgPSB0dXJuUG9pbnRzSW50b1N0ZXAoc3RhY2tlZE9uUG9pbnRzLCBjb29yZFN5cywgc3RlcCk7XG4gICAgICB9XG5cbiAgICAgIHBvbHlsaW5lID0gdGhpcy5fbmV3UG9seWxpbmUocG9pbnRzLCBjb29yZFN5cywgaGFzQW5pbWF0aW9uKTtcblxuICAgICAgaWYgKGlzQXJlYUNoYXJ0KSB7XG4gICAgICAgIHBvbHlnb24gPSB0aGlzLl9uZXdQb2x5Z29uKHBvaW50cywgc3RhY2tlZE9uUG9pbnRzLCBjb29yZFN5cywgaGFzQW5pbWF0aW9uKTtcbiAgICAgIH1cblxuICAgICAgbGluZUdyb3VwLnNldENsaXBQYXRoKGNyZWF0ZUNsaXBTaGFwZShjb29yZFN5cywgdHJ1ZSwgZmFsc2UsIHNlcmllc01vZGVsKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChpc0FyZWFDaGFydCAmJiAhcG9seWdvbikge1xuICAgICAgICAvLyBJZiBhcmVhU3R5bGUgaXMgYWRkZWRcbiAgICAgICAgcG9seWdvbiA9IHRoaXMuX25ld1BvbHlnb24ocG9pbnRzLCBzdGFja2VkT25Qb2ludHMsIGNvb3JkU3lzLCBoYXNBbmltYXRpb24pO1xuICAgICAgfSBlbHNlIGlmIChwb2x5Z29uICYmICFpc0FyZWFDaGFydCkge1xuICAgICAgICAvLyBJZiBhcmVhU3R5bGUgaXMgcmVtb3ZlZFxuICAgICAgICBsaW5lR3JvdXAucmVtb3ZlKHBvbHlnb24pO1xuICAgICAgICBwb2x5Z29uID0gdGhpcy5fcG9seWdvbiA9IG51bGw7XG4gICAgICB9IC8vIFVwZGF0ZSBjbGlwUGF0aFxuXG5cbiAgICAgIGxpbmVHcm91cC5zZXRDbGlwUGF0aChjcmVhdGVDbGlwU2hhcGUoY29vcmRTeXMsIGZhbHNlLCBmYWxzZSwgc2VyaWVzTW9kZWwpKTsgLy8gQWx3YXlzIHVwZGF0ZSwgb3IgaXQgaXMgd3JvbmcgaW4gdGhlIGNhc2UgdHVybmluZyBvbiBsZWdlbmRcbiAgICAgIC8vIGJlY2F1c2UgcG9pbnRzIGFyZSBub3QgY2hhbmdlZFxuXG4gICAgICBzaG93U3ltYm9sICYmIHN5bWJvbERyYXcudXBkYXRlRGF0YShkYXRhLCB7XG4gICAgICAgIGlzSWdub3JlOiBpc0lnbm9yZUZ1bmMsXG4gICAgICAgIGNsaXBTaGFwZTogY3JlYXRlQ2xpcFNoYXBlKGNvb3JkU3lzLCBmYWxzZSwgdHJ1ZSwgc2VyaWVzTW9kZWwpXG4gICAgICB9KTsgLy8gU3RvcCBzeW1ib2wgYW5pbWF0aW9uIGFuZCBzeW5jIHdpdGggbGluZSBwb2ludHNcbiAgICAgIC8vIEZJWE1FIHBlcmZvcm1hbmNlP1xuXG4gICAgICBkYXRhLmVhY2hJdGVtR3JhcGhpY0VsKGZ1bmN0aW9uIChlbCkge1xuICAgICAgICBlbC5zdG9wQW5pbWF0aW9uKHRydWUpO1xuICAgICAgfSk7IC8vIEluIHRoZSBjYXNlIGRhdGEgem9vbSB0cmlnZ2VycmVkIHJlZnJlc2hpbmcgZnJlcXVlbnRseVxuICAgICAgLy8gRGF0YSBtYXkgbm90IGNoYW5nZSBpZiBsaW5lIGhhcyBhIGNhdGVnb3J5IGF4aXMuIFNvIGl0IHNob3VsZCBhbmltYXRlIG5vdGhpbmdcblxuICAgICAgaWYgKCFpc1BvaW50c1NhbWUodGhpcy5fc3RhY2tlZE9uUG9pbnRzLCBzdGFja2VkT25Qb2ludHMpIHx8ICFpc1BvaW50c1NhbWUodGhpcy5fcG9pbnRzLCBwb2ludHMpKSB7XG4gICAgICAgIGlmIChoYXNBbmltYXRpb24pIHtcbiAgICAgICAgICB0aGlzLl91cGRhdGVBbmltYXRpb24oZGF0YSwgc3RhY2tlZE9uUG9pbnRzLCBjb29yZFN5cywgYXBpLCBzdGVwLCB2YWx1ZU9yaWdpbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gTm90IGRvIGl0IGluIHVwZGF0ZSB3aXRoIGFuaW1hdGlvblxuICAgICAgICAgIGlmIChzdGVwKSB7XG4gICAgICAgICAgICAvLyBUT0RPIElmIHN0YWNrZWQgc2VyaWVzIGlzIG5vdCBzdGVwXG4gICAgICAgICAgICBwb2ludHMgPSB0dXJuUG9pbnRzSW50b1N0ZXAocG9pbnRzLCBjb29yZFN5cywgc3RlcCk7XG4gICAgICAgICAgICBzdGFja2VkT25Qb2ludHMgPSB0dXJuUG9pbnRzSW50b1N0ZXAoc3RhY2tlZE9uUG9pbnRzLCBjb29yZFN5cywgc3RlcCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcG9seWxpbmUuc2V0U2hhcGUoe1xuICAgICAgICAgICAgcG9pbnRzOiBwb2ludHNcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBwb2x5Z29uICYmIHBvbHlnb24uc2V0U2hhcGUoe1xuICAgICAgICAgICAgcG9pbnRzOiBwb2ludHMsXG4gICAgICAgICAgICBzdGFja2VkT25Qb2ludHM6IHN0YWNrZWRPblBvaW50c1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIHZpc3VhbENvbG9yID0gZ2V0VmlzdWFsR3JhZGllbnQoZGF0YSwgY29vcmRTeXMpIHx8IGRhdGEuZ2V0VmlzdWFsKCdjb2xvcicpO1xuICAgIHBvbHlsaW5lLnVzZVN0eWxlKHpyVXRpbC5kZWZhdWx0cyggLy8gVXNlIGNvbG9yIGluIGxpbmVTdHlsZSBmaXJzdFxuICAgIGxpbmVTdHlsZU1vZGVsLmdldExpbmVTdHlsZSgpLCB7XG4gICAgICBmaWxsOiAnbm9uZScsXG4gICAgICBzdHJva2U6IHZpc3VhbENvbG9yLFxuICAgICAgbGluZUpvaW46ICdiZXZlbCdcbiAgICB9KSk7XG4gICAgdmFyIHNtb290aCA9IHNlcmllc01vZGVsLmdldCgnc21vb3RoJyk7XG4gICAgc21vb3RoID0gZ2V0U21vb3RoKHNlcmllc01vZGVsLmdldCgnc21vb3RoJykpO1xuICAgIHBvbHlsaW5lLnNldFNoYXBlKHtcbiAgICAgIHNtb290aDogc21vb3RoLFxuICAgICAgc21vb3RoTW9ub3RvbmU6IHNlcmllc01vZGVsLmdldCgnc21vb3RoTW9ub3RvbmUnKSxcbiAgICAgIGNvbm5lY3ROdWxsczogc2VyaWVzTW9kZWwuZ2V0KCdjb25uZWN0TnVsbHMnKVxuICAgIH0pO1xuXG4gICAgaWYgKHBvbHlnb24pIHtcbiAgICAgIHZhciBzdGFja2VkT25TZXJpZXMgPSBkYXRhLmdldENhbGN1bGF0aW9uSW5mbygnc3RhY2tlZE9uU2VyaWVzJyk7XG4gICAgICB2YXIgc3RhY2tlZE9uU21vb3RoID0gMDtcbiAgICAgIHBvbHlnb24udXNlU3R5bGUoenJVdGlsLmRlZmF1bHRzKGFyZWFTdHlsZU1vZGVsLmdldEFyZWFTdHlsZSgpLCB7XG4gICAgICAgIGZpbGw6IHZpc3VhbENvbG9yLFxuICAgICAgICBvcGFjaXR5OiAwLjcsXG4gICAgICAgIGxpbmVKb2luOiAnYmV2ZWwnXG4gICAgICB9KSk7XG5cbiAgICAgIGlmIChzdGFja2VkT25TZXJpZXMpIHtcbiAgICAgICAgc3RhY2tlZE9uU21vb3RoID0gZ2V0U21vb3RoKHN0YWNrZWRPblNlcmllcy5nZXQoJ3Ntb290aCcpKTtcbiAgICAgIH1cblxuICAgICAgcG9seWdvbi5zZXRTaGFwZSh7XG4gICAgICAgIHNtb290aDogc21vb3RoLFxuICAgICAgICBzdGFja2VkT25TbW9vdGg6IHN0YWNrZWRPblNtb290aCxcbiAgICAgICAgc21vb3RoTW9ub3RvbmU6IHNlcmllc01vZGVsLmdldCgnc21vb3RoTW9ub3RvbmUnKSxcbiAgICAgICAgY29ubmVjdE51bGxzOiBzZXJpZXNNb2RlbC5nZXQoJ2Nvbm5lY3ROdWxscycpXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB0aGlzLl9kYXRhID0gZGF0YTsgLy8gU2F2ZSB0aGUgY29vcmRpbmF0ZSBzeXN0ZW0gZm9yIHRyYW5zaXRpb24gYW5pbWF0aW9uIHdoZW4gZGF0YSBjaGFuZ2VkXG5cbiAgICB0aGlzLl9jb29yZFN5cyA9IGNvb3JkU3lzO1xuICAgIHRoaXMuX3N0YWNrZWRPblBvaW50cyA9IHN0YWNrZWRPblBvaW50cztcbiAgICB0aGlzLl9wb2ludHMgPSBwb2ludHM7XG4gICAgdGhpcy5fc3RlcCA9IHN0ZXA7XG4gICAgdGhpcy5fdmFsdWVPcmlnaW4gPSB2YWx1ZU9yaWdpbjtcbiAgfSxcbiAgZGlzcG9zZTogZnVuY3Rpb24gKCkge30sXG4gIGhpZ2hsaWdodDogZnVuY3Rpb24gKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpIHtcbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgZGF0YUluZGV4ID0gbW9kZWxVdGlsLnF1ZXJ5RGF0YUluZGV4KGRhdGEsIHBheWxvYWQpO1xuXG4gICAgaWYgKCEoZGF0YUluZGV4IGluc3RhbmNlb2YgQXJyYXkpICYmIGRhdGFJbmRleCAhPSBudWxsICYmIGRhdGFJbmRleCA+PSAwKSB7XG4gICAgICB2YXIgc3ltYm9sID0gZGF0YS5nZXRJdGVtR3JhcGhpY0VsKGRhdGFJbmRleCk7XG5cbiAgICAgIGlmICghc3ltYm9sKSB7XG4gICAgICAgIC8vIENyZWF0ZSBhIHRlbXBvcmFyeSBzeW1ib2wgaWYgaXQgaXMgbm90IGV4aXN0c1xuICAgICAgICB2YXIgcHQgPSBkYXRhLmdldEl0ZW1MYXlvdXQoZGF0YUluZGV4KTtcblxuICAgICAgICBpZiAoIXB0KSB7XG4gICAgICAgICAgLy8gTnVsbCBkYXRhXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgc3ltYm9sID0gbmV3IFN5bWJvbENseihkYXRhLCBkYXRhSW5kZXgpO1xuICAgICAgICBzeW1ib2wucG9zaXRpb24gPSBwdDtcbiAgICAgICAgc3ltYm9sLnNldFooc2VyaWVzTW9kZWwuZ2V0KCd6bGV2ZWwnKSwgc2VyaWVzTW9kZWwuZ2V0KCd6JykpO1xuICAgICAgICBzeW1ib2wuaWdub3JlID0gaXNOYU4ocHRbMF0pIHx8IGlzTmFOKHB0WzFdKTtcbiAgICAgICAgc3ltYm9sLl9fdGVtcCA9IHRydWU7XG4gICAgICAgIGRhdGEuc2V0SXRlbUdyYXBoaWNFbChkYXRhSW5kZXgsIHN5bWJvbCk7IC8vIFN0b3Agc2NhbGUgYW5pbWF0aW9uXG5cbiAgICAgICAgc3ltYm9sLnN0b3BTeW1ib2xBbmltYXRpb24odHJ1ZSk7XG4gICAgICAgIHRoaXMuZ3JvdXAuYWRkKHN5bWJvbCk7XG4gICAgICB9XG5cbiAgICAgIHN5bWJvbC5oaWdobGlnaHQoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gSGlnaGxpZ2h0IHdob2xlIHNlcmllc1xuICAgICAgQ2hhcnRWaWV3LnByb3RvdHlwZS5oaWdobGlnaHQuY2FsbCh0aGlzLCBzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKTtcbiAgICB9XG4gIH0sXG4gIGRvd25wbGF5OiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkge1xuICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgIHZhciBkYXRhSW5kZXggPSBtb2RlbFV0aWwucXVlcnlEYXRhSW5kZXgoZGF0YSwgcGF5bG9hZCk7XG5cbiAgICBpZiAoZGF0YUluZGV4ICE9IG51bGwgJiYgZGF0YUluZGV4ID49IDApIHtcbiAgICAgIHZhciBzeW1ib2wgPSBkYXRhLmdldEl0ZW1HcmFwaGljRWwoZGF0YUluZGV4KTtcblxuICAgICAgaWYgKHN5bWJvbCkge1xuICAgICAgICBpZiAoc3ltYm9sLl9fdGVtcCkge1xuICAgICAgICAgIGRhdGEuc2V0SXRlbUdyYXBoaWNFbChkYXRhSW5kZXgsIG51bGwpO1xuICAgICAgICAgIHRoaXMuZ3JvdXAucmVtb3ZlKHN5bWJvbCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgc3ltYm9sLmRvd25wbGF5KCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gRklYTUVcbiAgICAgIC8vIGNhbiBub3QgZG93bnBsYXkgY29tcGxldGVseS5cbiAgICAgIC8vIERvd25wbGF5IHdob2xlIHNlcmllc1xuICAgICAgQ2hhcnRWaWV3LnByb3RvdHlwZS5kb3ducGxheS5jYWxsKHRoaXMsIHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9IGdyb3VwXG4gICAqIEBwYXJhbSB7QXJyYXkuPEFycmF5LjxudW1iZXI+Pn0gcG9pbnRzXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBfbmV3UG9seWxpbmU6IGZ1bmN0aW9uIChwb2ludHMpIHtcbiAgICB2YXIgcG9seWxpbmUgPSB0aGlzLl9wb2x5bGluZTsgLy8gUmVtb3ZlIHByZXZpb3VzIGNyZWF0ZWQgcG9seWxpbmVcblxuICAgIGlmIChwb2x5bGluZSkge1xuICAgICAgdGhpcy5fbGluZUdyb3VwLnJlbW92ZShwb2x5bGluZSk7XG4gICAgfVxuXG4gICAgcG9seWxpbmUgPSBuZXcgUG9seWxpbmUoe1xuICAgICAgc2hhcGU6IHtcbiAgICAgICAgcG9pbnRzOiBwb2ludHNcbiAgICAgIH0sXG4gICAgICBzaWxlbnQ6IHRydWUsXG4gICAgICB6MjogMTBcbiAgICB9KTtcblxuICAgIHRoaXMuX2xpbmVHcm91cC5hZGQocG9seWxpbmUpO1xuXG4gICAgdGhpcy5fcG9seWxpbmUgPSBwb2x5bGluZTtcbiAgICByZXR1cm4gcG9seWxpbmU7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvY29udGFpbmVyL0dyb3VwfSBncm91cFxuICAgKiBAcGFyYW0ge0FycmF5LjxBcnJheS48bnVtYmVyPj59IHN0YWNrZWRPblBvaW50c1xuICAgKiBAcGFyYW0ge0FycmF5LjxBcnJheS48bnVtYmVyPj59IHBvaW50c1xuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgX25ld1BvbHlnb246IGZ1bmN0aW9uIChwb2ludHMsIHN0YWNrZWRPblBvaW50cykge1xuICAgIHZhciBwb2x5Z29uID0gdGhpcy5fcG9seWdvbjsgLy8gUmVtb3ZlIHByZXZpb3VzIGNyZWF0ZWQgcG9seWdvblxuXG4gICAgaWYgKHBvbHlnb24pIHtcbiAgICAgIHRoaXMuX2xpbmVHcm91cC5yZW1vdmUocG9seWdvbik7XG4gICAgfVxuXG4gICAgcG9seWdvbiA9IG5ldyBQb2x5Z29uKHtcbiAgICAgIHNoYXBlOiB7XG4gICAgICAgIHBvaW50czogcG9pbnRzLFxuICAgICAgICBzdGFja2VkT25Qb2ludHM6IHN0YWNrZWRPblBvaW50c1xuICAgICAgfSxcbiAgICAgIHNpbGVudDogdHJ1ZVxuICAgIH0pO1xuXG4gICAgdGhpcy5fbGluZUdyb3VwLmFkZChwb2x5Z29uKTtcblxuICAgIHRoaXMuX3BvbHlnb24gPSBwb2x5Z29uO1xuICAgIHJldHVybiBwb2x5Z29uO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgLy8gRklYTUUgVHdvIHZhbHVlIGF4aXNcbiAgX3VwZGF0ZUFuaW1hdGlvbjogZnVuY3Rpb24gKGRhdGEsIHN0YWNrZWRPblBvaW50cywgY29vcmRTeXMsIGFwaSwgc3RlcCwgdmFsdWVPcmlnaW4pIHtcbiAgICB2YXIgcG9seWxpbmUgPSB0aGlzLl9wb2x5bGluZTtcbiAgICB2YXIgcG9seWdvbiA9IHRoaXMuX3BvbHlnb247XG4gICAgdmFyIHNlcmllc01vZGVsID0gZGF0YS5ob3N0TW9kZWw7XG4gICAgdmFyIGRpZmYgPSBsaW5lQW5pbWF0aW9uRGlmZih0aGlzLl9kYXRhLCBkYXRhLCB0aGlzLl9zdGFja2VkT25Qb2ludHMsIHN0YWNrZWRPblBvaW50cywgdGhpcy5fY29vcmRTeXMsIGNvb3JkU3lzLCB0aGlzLl92YWx1ZU9yaWdpbiwgdmFsdWVPcmlnaW4pO1xuICAgIHZhciBjdXJyZW50ID0gZGlmZi5jdXJyZW50O1xuICAgIHZhciBzdGFja2VkT25DdXJyZW50ID0gZGlmZi5zdGFja2VkT25DdXJyZW50O1xuICAgIHZhciBuZXh0ID0gZGlmZi5uZXh0O1xuICAgIHZhciBzdGFja2VkT25OZXh0ID0gZGlmZi5zdGFja2VkT25OZXh0O1xuXG4gICAgaWYgKHN0ZXApIHtcbiAgICAgIC8vIFRPRE8gSWYgc3RhY2tlZCBzZXJpZXMgaXMgbm90IHN0ZXBcbiAgICAgIGN1cnJlbnQgPSB0dXJuUG9pbnRzSW50b1N0ZXAoZGlmZi5jdXJyZW50LCBjb29yZFN5cywgc3RlcCk7XG4gICAgICBzdGFja2VkT25DdXJyZW50ID0gdHVyblBvaW50c0ludG9TdGVwKGRpZmYuc3RhY2tlZE9uQ3VycmVudCwgY29vcmRTeXMsIHN0ZXApO1xuICAgICAgbmV4dCA9IHR1cm5Qb2ludHNJbnRvU3RlcChkaWZmLm5leHQsIGNvb3JkU3lzLCBzdGVwKTtcbiAgICAgIHN0YWNrZWRPbk5leHQgPSB0dXJuUG9pbnRzSW50b1N0ZXAoZGlmZi5zdGFja2VkT25OZXh0LCBjb29yZFN5cywgc3RlcCk7XG4gICAgfSAvLyBgZGlmZi5jdXJyZW50YCBpcyBzdWJzZXQgb2YgYGN1cnJlbnRgICh3aGljaCBzaG91bGQgYmUgZW5zdXJlZCBieVxuICAgIC8vIHR1cm5Qb2ludHNJbnRvU3RlcCksIHNvIHBvaW50cyBpbiBgX19wb2ludHNgIGNhbiBiZSB1cGRhdGVkIHdoZW5cbiAgICAvLyBwb2ludHMgaW4gYGN1cnJlbnRgIGFyZSB1cGRhdGUgZHVyaW5nIGFuaW1hdGlvbi5cblxuXG4gICAgcG9seWxpbmUuc2hhcGUuX19wb2ludHMgPSBkaWZmLmN1cnJlbnQ7XG4gICAgcG9seWxpbmUuc2hhcGUucG9pbnRzID0gY3VycmVudDtcbiAgICBncmFwaGljLnVwZGF0ZVByb3BzKHBvbHlsaW5lLCB7XG4gICAgICBzaGFwZToge1xuICAgICAgICBwb2ludHM6IG5leHRcbiAgICAgIH1cbiAgICB9LCBzZXJpZXNNb2RlbCk7XG5cbiAgICBpZiAocG9seWdvbikge1xuICAgICAgcG9seWdvbi5zZXRTaGFwZSh7XG4gICAgICAgIHBvaW50czogY3VycmVudCxcbiAgICAgICAgc3RhY2tlZE9uUG9pbnRzOiBzdGFja2VkT25DdXJyZW50XG4gICAgICB9KTtcbiAgICAgIGdyYXBoaWMudXBkYXRlUHJvcHMocG9seWdvbiwge1xuICAgICAgICBzaGFwZToge1xuICAgICAgICAgIHBvaW50czogbmV4dCxcbiAgICAgICAgICBzdGFja2VkT25Qb2ludHM6IHN0YWNrZWRPbk5leHRcbiAgICAgICAgfVxuICAgICAgfSwgc2VyaWVzTW9kZWwpO1xuICAgIH1cblxuICAgIHZhciB1cGRhdGVkRGF0YUluZm8gPSBbXTtcbiAgICB2YXIgZGlmZlN0YXR1cyA9IGRpZmYuc3RhdHVzO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaWZmU3RhdHVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgY21kID0gZGlmZlN0YXR1c1tpXS5jbWQ7XG5cbiAgICAgIGlmIChjbWQgPT09ICc9Jykge1xuICAgICAgICB2YXIgZWwgPSBkYXRhLmdldEl0ZW1HcmFwaGljRWwoZGlmZlN0YXR1c1tpXS5pZHgxKTtcblxuICAgICAgICBpZiAoZWwpIHtcbiAgICAgICAgICB1cGRhdGVkRGF0YUluZm8ucHVzaCh7XG4gICAgICAgICAgICBlbDogZWwsXG4gICAgICAgICAgICBwdElkeDogaSAvLyBJbmRleCBvZiBwb2ludHNcblxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHBvbHlsaW5lLmFuaW1hdG9ycyAmJiBwb2x5bGluZS5hbmltYXRvcnMubGVuZ3RoKSB7XG4gICAgICBwb2x5bGluZS5hbmltYXRvcnNbMF0uZHVyaW5nKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB1cGRhdGVkRGF0YUluZm8ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICB2YXIgZWwgPSB1cGRhdGVkRGF0YUluZm9baV0uZWw7XG4gICAgICAgICAgZWwuYXR0cigncG9zaXRpb24nLCBwb2x5bGluZS5zaGFwZS5fX3BvaW50c1t1cGRhdGVkRGF0YUluZm9baV0ucHRJZHhdKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9LFxuICByZW1vdmU6IGZ1bmN0aW9uIChlY01vZGVsKSB7XG4gICAgdmFyIGdyb3VwID0gdGhpcy5ncm91cDtcbiAgICB2YXIgb2xkRGF0YSA9IHRoaXMuX2RhdGE7XG5cbiAgICB0aGlzLl9saW5lR3JvdXAucmVtb3ZlQWxsKCk7XG5cbiAgICB0aGlzLl9zeW1ib2xEcmF3LnJlbW92ZSh0cnVlKTsgLy8gUmVtb3ZlIHRlbXBvcmFyeSBjcmVhdGVkIGVsZW1lbnRzIHdoZW4gaGlnaGxpZ2h0aW5nXG5cblxuICAgIG9sZERhdGEgJiYgb2xkRGF0YS5lYWNoSXRlbUdyYXBoaWNFbChmdW5jdGlvbiAoZWwsIGlkeCkge1xuICAgICAgaWYgKGVsLl9fdGVtcCkge1xuICAgICAgICBncm91cC5yZW1vdmUoZWwpO1xuICAgICAgICBvbGREYXRhLnNldEl0ZW1HcmFwaGljRWwoaWR4LCBudWxsKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLl9wb2x5bGluZSA9IHRoaXMuX3BvbHlnb24gPSB0aGlzLl9jb29yZFN5cyA9IHRoaXMuX3BvaW50cyA9IHRoaXMuX3N0YWNrZWRPblBvaW50cyA9IHRoaXMuX2RhdGEgPSBudWxsO1xuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZCQTtBQXlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBTEE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBXQTtBQUNBO0FBc1dBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/line/LineView.js
