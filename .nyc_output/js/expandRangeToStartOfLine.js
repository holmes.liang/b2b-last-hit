
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule expandRangeToStartOfLine
 * @format
 * 
 */

var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var getRangeClientRects = __webpack_require__(/*! ./getRangeClientRects */ "./node_modules/draft-js/lib/getRangeClientRects.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");
/**
 * Return the computed line height, in pixels, for the provided element.
 */


function getLineHeightPx(element) {
  var computed = getComputedStyle(element);
  var div = document.createElement('div');
  div.style.fontFamily = computed.fontFamily;
  div.style.fontSize = computed.fontSize;
  div.style.fontStyle = computed.fontStyle;
  div.style.fontWeight = computed.fontWeight;
  div.style.lineHeight = computed.lineHeight;
  div.style.position = 'absolute';
  div.textContent = 'M';
  var documentBody = document.body;
  !documentBody ?  true ? invariant(false, 'Missing document.body') : undefined : void 0; // forced layout here

  documentBody.appendChild(div);
  var rect = div.getBoundingClientRect();
  documentBody.removeChild(div);
  return rect.height;
}
/**
 * Return whether every ClientRect in the provided list lies on the same line.
 *
 * We assume that the rects on the same line all contain the baseline, so the
 * lowest top line needs to be above the highest bottom line (i.e., if you were
 * to project the rects onto the y-axis, their intersection would be nonempty).
 *
 * In addition, we require that no two boxes are lineHeight (or more) apart at
 * either top or bottom, which helps protect against false positives for fonts
 * with extremely large glyph heights (e.g., with a font size of 17px, Zapfino
 * produces rects of height 58px!).
 */


function areRectsOnOneLine(rects, lineHeight) {
  var minTop = Infinity;
  var minBottom = Infinity;
  var maxTop = -Infinity;
  var maxBottom = -Infinity;

  for (var ii = 0; ii < rects.length; ii++) {
    var rect = rects[ii];

    if (rect.width === 0 || rect.width === 1) {
      // When a range starts or ends a soft wrap, many browsers (Chrome, IE,
      // Safari) include an empty rect on the previous or next line. When the
      // text lies in a container whose position is not integral (e.g., from
      // margin: auto), Safari makes these empty rects have width 1 (instead of
      // 0). Having one-pixel-wide characters seems unlikely (and most browsers
      // report widths in subpixel precision anyway) so it's relatively safe to
      // skip over them.
      continue;
    }

    minTop = Math.min(minTop, rect.top);
    minBottom = Math.min(minBottom, rect.bottom);
    maxTop = Math.max(maxTop, rect.top);
    maxBottom = Math.max(maxBottom, rect.bottom);
  }

  return maxTop <= minBottom && maxTop - minTop < lineHeight && maxBottom - minBottom < lineHeight;
}
/**
 * Return the length of a node, as used by Range offsets.
 */


function getNodeLength(node) {
  // http://www.w3.org/TR/dom/#concept-node-length
  switch (node.nodeType) {
    case Node.DOCUMENT_TYPE_NODE:
      return 0;

    case Node.TEXT_NODE:
    case Node.PROCESSING_INSTRUCTION_NODE:
    case Node.COMMENT_NODE:
      return node.length;

    default:
      return node.childNodes.length;
  }
}
/**
 * Given a collapsed range, move the start position backwards as far as
 * possible while the range still spans only a single line.
 */


function expandRangeToStartOfLine(range) {
  !range.collapsed ?  true ? invariant(false, 'expandRangeToStartOfLine: Provided range is not collapsed.') : undefined : void 0;
  range = range.cloneRange();
  var containingElement = range.startContainer;

  if (containingElement.nodeType !== 1) {
    containingElement = containingElement.parentNode;
  }

  var lineHeight = getLineHeightPx(containingElement); // Imagine our text looks like:
  //   <div><span>once upon a time, there was a <em>boy
  //   who lived</em> </span><q><strong>under^ the
  //   stairs</strong> in a small closet.</q></div>
  // where the caret represents the cursor. First, we crawl up the tree until
  // the range spans multiple lines (setting the start point to before
  // "<strong>", then before "<div>"), then at each level we do a search to
  // find the latest point which is still on a previous line. We'll find that
  // the break point is inside the span, then inside the <em>, then in its text
  // node child, the actual break point before "who".

  var bestContainer = range.endContainer;
  var bestOffset = range.endOffset;
  range.setStart(range.startContainer, 0);

  while (areRectsOnOneLine(getRangeClientRects(range), lineHeight)) {
    bestContainer = range.startContainer;
    bestOffset = range.startOffset;
    !bestContainer.parentNode ?  true ? invariant(false, 'Found unexpected detached subtree when traversing.') : undefined : void 0;
    range.setStartBefore(bestContainer);

    if (bestContainer.nodeType === 1 && getComputedStyle(bestContainer).display !== 'inline') {
      // The start of the line is never in a different block-level container.
      break;
    }
  } // In the above example, range now spans from "<div>" to "under",
  // bestContainer is <div>, and bestOffset is 1 (index of <q> inside <div>)].
  // Picking out which child to recurse into here is a special case since we
  // don't want to check past <q> -- once we find that the final range starts
  // in <span>, we can look at all of its children (and all of their children)
  // to find the break point.
  // At all times, (bestContainer, bestOffset) is the latest single-line start
  // point that we know of.


  var currentContainer = bestContainer;
  var maxIndexToConsider = bestOffset - 1;

  do {
    var nodeValue = currentContainer.nodeValue;

    for (var ii = maxIndexToConsider; ii >= 0; ii--) {
      if (nodeValue != null && ii > 0 && UnicodeUtils.isSurrogatePair(nodeValue, ii - 1)) {
        // We're in the middle of a surrogate pair -- skip over so we never
        // return a range with an endpoint in the middle of a code point.
        continue;
      }

      range.setStart(currentContainer, ii);

      if (areRectsOnOneLine(getRangeClientRects(range), lineHeight)) {
        bestContainer = currentContainer;
        bestOffset = ii;
      } else {
        break;
      }
    }

    if (ii === -1 || currentContainer.childNodes.length === 0) {
      // If ii === -1, then (bestContainer, bestOffset), which is equal to
      // (currentContainer, 0), was a single-line start point but a start
      // point before currentContainer wasn't, so the line break seems to
      // have occurred immediately after currentContainer's start tag
      //
      // If currentContainer.childNodes.length === 0, we're already at a
      // terminal node (e.g., text node) and should return our current best.
      break;
    }

    currentContainer = currentContainer.childNodes[ii];
    maxIndexToConsider = getNodeLength(currentContainer);
  } while (true);

  range.setStart(bestContainer, bestOffset);
  return range;
}

module.exports = expandRangeToStartOfLine;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2V4cGFuZFJhbmdlVG9TdGFydE9mTGluZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9leHBhbmRSYW5nZVRvU3RhcnRPZkxpbmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGV4cGFuZFJhbmdlVG9TdGFydE9mTGluZVxuICogQGZvcm1hdFxuICogXG4gKi9cblxudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgZ2V0UmFuZ2VDbGllbnRSZWN0cyA9IHJlcXVpcmUoJy4vZ2V0UmFuZ2VDbGllbnRSZWN0cycpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG4vKipcbiAqIFJldHVybiB0aGUgY29tcHV0ZWQgbGluZSBoZWlnaHQsIGluIHBpeGVscywgZm9yIHRoZSBwcm92aWRlZCBlbGVtZW50LlxuICovXG5mdW5jdGlvbiBnZXRMaW5lSGVpZ2h0UHgoZWxlbWVudCkge1xuICB2YXIgY29tcHV0ZWQgPSBnZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpO1xuICB2YXIgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIGRpdi5zdHlsZS5mb250RmFtaWx5ID0gY29tcHV0ZWQuZm9udEZhbWlseTtcbiAgZGl2LnN0eWxlLmZvbnRTaXplID0gY29tcHV0ZWQuZm9udFNpemU7XG4gIGRpdi5zdHlsZS5mb250U3R5bGUgPSBjb21wdXRlZC5mb250U3R5bGU7XG4gIGRpdi5zdHlsZS5mb250V2VpZ2h0ID0gY29tcHV0ZWQuZm9udFdlaWdodDtcbiAgZGl2LnN0eWxlLmxpbmVIZWlnaHQgPSBjb21wdXRlZC5saW5lSGVpZ2h0O1xuICBkaXYuc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xuICBkaXYudGV4dENvbnRlbnQgPSAnTSc7XG5cbiAgdmFyIGRvY3VtZW50Qm9keSA9IGRvY3VtZW50LmJvZHk7XG4gICFkb2N1bWVudEJvZHkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnTWlzc2luZyBkb2N1bWVudC5ib2R5JykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIC8vIGZvcmNlZCBsYXlvdXQgaGVyZVxuICBkb2N1bWVudEJvZHkuYXBwZW5kQ2hpbGQoZGl2KTtcbiAgdmFyIHJlY3QgPSBkaXYuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gIGRvY3VtZW50Qm9keS5yZW1vdmVDaGlsZChkaXYpO1xuXG4gIHJldHVybiByZWN0LmhlaWdodDtcbn1cblxuLyoqXG4gKiBSZXR1cm4gd2hldGhlciBldmVyeSBDbGllbnRSZWN0IGluIHRoZSBwcm92aWRlZCBsaXN0IGxpZXMgb24gdGhlIHNhbWUgbGluZS5cbiAqXG4gKiBXZSBhc3N1bWUgdGhhdCB0aGUgcmVjdHMgb24gdGhlIHNhbWUgbGluZSBhbGwgY29udGFpbiB0aGUgYmFzZWxpbmUsIHNvIHRoZVxuICogbG93ZXN0IHRvcCBsaW5lIG5lZWRzIHRvIGJlIGFib3ZlIHRoZSBoaWdoZXN0IGJvdHRvbSBsaW5lIChpLmUuLCBpZiB5b3Ugd2VyZVxuICogdG8gcHJvamVjdCB0aGUgcmVjdHMgb250byB0aGUgeS1heGlzLCB0aGVpciBpbnRlcnNlY3Rpb24gd291bGQgYmUgbm9uZW1wdHkpLlxuICpcbiAqIEluIGFkZGl0aW9uLCB3ZSByZXF1aXJlIHRoYXQgbm8gdHdvIGJveGVzIGFyZSBsaW5lSGVpZ2h0IChvciBtb3JlKSBhcGFydCBhdFxuICogZWl0aGVyIHRvcCBvciBib3R0b20sIHdoaWNoIGhlbHBzIHByb3RlY3QgYWdhaW5zdCBmYWxzZSBwb3NpdGl2ZXMgZm9yIGZvbnRzXG4gKiB3aXRoIGV4dHJlbWVseSBsYXJnZSBnbHlwaCBoZWlnaHRzIChlLmcuLCB3aXRoIGEgZm9udCBzaXplIG9mIDE3cHgsIFphcGZpbm9cbiAqIHByb2R1Y2VzIHJlY3RzIG9mIGhlaWdodCA1OHB4ISkuXG4gKi9cbmZ1bmN0aW9uIGFyZVJlY3RzT25PbmVMaW5lKHJlY3RzLCBsaW5lSGVpZ2h0KSB7XG4gIHZhciBtaW5Ub3AgPSBJbmZpbml0eTtcbiAgdmFyIG1pbkJvdHRvbSA9IEluZmluaXR5O1xuICB2YXIgbWF4VG9wID0gLUluZmluaXR5O1xuICB2YXIgbWF4Qm90dG9tID0gLUluZmluaXR5O1xuXG4gIGZvciAodmFyIGlpID0gMDsgaWkgPCByZWN0cy5sZW5ndGg7IGlpKyspIHtcbiAgICB2YXIgcmVjdCA9IHJlY3RzW2lpXTtcbiAgICBpZiAocmVjdC53aWR0aCA9PT0gMCB8fCByZWN0LndpZHRoID09PSAxKSB7XG4gICAgICAvLyBXaGVuIGEgcmFuZ2Ugc3RhcnRzIG9yIGVuZHMgYSBzb2Z0IHdyYXAsIG1hbnkgYnJvd3NlcnMgKENocm9tZSwgSUUsXG4gICAgICAvLyBTYWZhcmkpIGluY2x1ZGUgYW4gZW1wdHkgcmVjdCBvbiB0aGUgcHJldmlvdXMgb3IgbmV4dCBsaW5lLiBXaGVuIHRoZVxuICAgICAgLy8gdGV4dCBsaWVzIGluIGEgY29udGFpbmVyIHdob3NlIHBvc2l0aW9uIGlzIG5vdCBpbnRlZ3JhbCAoZS5nLiwgZnJvbVxuICAgICAgLy8gbWFyZ2luOiBhdXRvKSwgU2FmYXJpIG1ha2VzIHRoZXNlIGVtcHR5IHJlY3RzIGhhdmUgd2lkdGggMSAoaW5zdGVhZCBvZlxuICAgICAgLy8gMCkuIEhhdmluZyBvbmUtcGl4ZWwtd2lkZSBjaGFyYWN0ZXJzIHNlZW1zIHVubGlrZWx5IChhbmQgbW9zdCBicm93c2Vyc1xuICAgICAgLy8gcmVwb3J0IHdpZHRocyBpbiBzdWJwaXhlbCBwcmVjaXNpb24gYW55d2F5KSBzbyBpdCdzIHJlbGF0aXZlbHkgc2FmZSB0b1xuICAgICAgLy8gc2tpcCBvdmVyIHRoZW0uXG4gICAgICBjb250aW51ZTtcbiAgICB9XG4gICAgbWluVG9wID0gTWF0aC5taW4obWluVG9wLCByZWN0LnRvcCk7XG4gICAgbWluQm90dG9tID0gTWF0aC5taW4obWluQm90dG9tLCByZWN0LmJvdHRvbSk7XG4gICAgbWF4VG9wID0gTWF0aC5tYXgobWF4VG9wLCByZWN0LnRvcCk7XG4gICAgbWF4Qm90dG9tID0gTWF0aC5tYXgobWF4Qm90dG9tLCByZWN0LmJvdHRvbSk7XG4gIH1cblxuICByZXR1cm4gbWF4VG9wIDw9IG1pbkJvdHRvbSAmJiBtYXhUb3AgLSBtaW5Ub3AgPCBsaW5lSGVpZ2h0ICYmIG1heEJvdHRvbSAtIG1pbkJvdHRvbSA8IGxpbmVIZWlnaHQ7XG59XG5cbi8qKlxuICogUmV0dXJuIHRoZSBsZW5ndGggb2YgYSBub2RlLCBhcyB1c2VkIGJ5IFJhbmdlIG9mZnNldHMuXG4gKi9cbmZ1bmN0aW9uIGdldE5vZGVMZW5ndGgobm9kZSkge1xuICAvLyBodHRwOi8vd3d3LnczLm9yZy9UUi9kb20vI2NvbmNlcHQtbm9kZS1sZW5ndGhcbiAgc3dpdGNoIChub2RlLm5vZGVUeXBlKSB7XG4gICAgY2FzZSBOb2RlLkRPQ1VNRU5UX1RZUEVfTk9ERTpcbiAgICAgIHJldHVybiAwO1xuICAgIGNhc2UgTm9kZS5URVhUX05PREU6XG4gICAgY2FzZSBOb2RlLlBST0NFU1NJTkdfSU5TVFJVQ1RJT05fTk9ERTpcbiAgICBjYXNlIE5vZGUuQ09NTUVOVF9OT0RFOlxuICAgICAgcmV0dXJuIG5vZGUubGVuZ3RoO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gbm9kZS5jaGlsZE5vZGVzLmxlbmd0aDtcbiAgfVxufVxuXG4vKipcbiAqIEdpdmVuIGEgY29sbGFwc2VkIHJhbmdlLCBtb3ZlIHRoZSBzdGFydCBwb3NpdGlvbiBiYWNrd2FyZHMgYXMgZmFyIGFzXG4gKiBwb3NzaWJsZSB3aGlsZSB0aGUgcmFuZ2Ugc3RpbGwgc3BhbnMgb25seSBhIHNpbmdsZSBsaW5lLlxuICovXG5mdW5jdGlvbiBleHBhbmRSYW5nZVRvU3RhcnRPZkxpbmUocmFuZ2UpIHtcbiAgIXJhbmdlLmNvbGxhcHNlZCA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdleHBhbmRSYW5nZVRvU3RhcnRPZkxpbmU6IFByb3ZpZGVkIHJhbmdlIGlzIG5vdCBjb2xsYXBzZWQuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICByYW5nZSA9IHJhbmdlLmNsb25lUmFuZ2UoKTtcblxuICB2YXIgY29udGFpbmluZ0VsZW1lbnQgPSByYW5nZS5zdGFydENvbnRhaW5lcjtcbiAgaWYgKGNvbnRhaW5pbmdFbGVtZW50Lm5vZGVUeXBlICE9PSAxKSB7XG4gICAgY29udGFpbmluZ0VsZW1lbnQgPSBjb250YWluaW5nRWxlbWVudC5wYXJlbnROb2RlO1xuICB9XG4gIHZhciBsaW5lSGVpZ2h0ID0gZ2V0TGluZUhlaWdodFB4KGNvbnRhaW5pbmdFbGVtZW50KTtcblxuICAvLyBJbWFnaW5lIG91ciB0ZXh0IGxvb2tzIGxpa2U6XG4gIC8vICAgPGRpdj48c3Bhbj5vbmNlIHVwb24gYSB0aW1lLCB0aGVyZSB3YXMgYSA8ZW0+Ym95XG4gIC8vICAgd2hvIGxpdmVkPC9lbT4gPC9zcGFuPjxxPjxzdHJvbmc+dW5kZXJeIHRoZVxuICAvLyAgIHN0YWlyczwvc3Ryb25nPiBpbiBhIHNtYWxsIGNsb3NldC48L3E+PC9kaXY+XG4gIC8vIHdoZXJlIHRoZSBjYXJldCByZXByZXNlbnRzIHRoZSBjdXJzb3IuIEZpcnN0LCB3ZSBjcmF3bCB1cCB0aGUgdHJlZSB1bnRpbFxuICAvLyB0aGUgcmFuZ2Ugc3BhbnMgbXVsdGlwbGUgbGluZXMgKHNldHRpbmcgdGhlIHN0YXJ0IHBvaW50IHRvIGJlZm9yZVxuICAvLyBcIjxzdHJvbmc+XCIsIHRoZW4gYmVmb3JlIFwiPGRpdj5cIiksIHRoZW4gYXQgZWFjaCBsZXZlbCB3ZSBkbyBhIHNlYXJjaCB0b1xuICAvLyBmaW5kIHRoZSBsYXRlc3QgcG9pbnQgd2hpY2ggaXMgc3RpbGwgb24gYSBwcmV2aW91cyBsaW5lLiBXZSdsbCBmaW5kIHRoYXRcbiAgLy8gdGhlIGJyZWFrIHBvaW50IGlzIGluc2lkZSB0aGUgc3BhbiwgdGhlbiBpbnNpZGUgdGhlIDxlbT4sIHRoZW4gaW4gaXRzIHRleHRcbiAgLy8gbm9kZSBjaGlsZCwgdGhlIGFjdHVhbCBicmVhayBwb2ludCBiZWZvcmUgXCJ3aG9cIi5cblxuICB2YXIgYmVzdENvbnRhaW5lciA9IHJhbmdlLmVuZENvbnRhaW5lcjtcbiAgdmFyIGJlc3RPZmZzZXQgPSByYW5nZS5lbmRPZmZzZXQ7XG4gIHJhbmdlLnNldFN0YXJ0KHJhbmdlLnN0YXJ0Q29udGFpbmVyLCAwKTtcblxuICB3aGlsZSAoYXJlUmVjdHNPbk9uZUxpbmUoZ2V0UmFuZ2VDbGllbnRSZWN0cyhyYW5nZSksIGxpbmVIZWlnaHQpKSB7XG4gICAgYmVzdENvbnRhaW5lciA9IHJhbmdlLnN0YXJ0Q29udGFpbmVyO1xuICAgIGJlc3RPZmZzZXQgPSByYW5nZS5zdGFydE9mZnNldDtcbiAgICAhYmVzdENvbnRhaW5lci5wYXJlbnROb2RlID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0ZvdW5kIHVuZXhwZWN0ZWQgZGV0YWNoZWQgc3VidHJlZSB3aGVuIHRyYXZlcnNpbmcuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgIHJhbmdlLnNldFN0YXJ0QmVmb3JlKGJlc3RDb250YWluZXIpO1xuICAgIGlmIChiZXN0Q29udGFpbmVyLm5vZGVUeXBlID09PSAxICYmIGdldENvbXB1dGVkU3R5bGUoYmVzdENvbnRhaW5lcikuZGlzcGxheSAhPT0gJ2lubGluZScpIHtcbiAgICAgIC8vIFRoZSBzdGFydCBvZiB0aGUgbGluZSBpcyBuZXZlciBpbiBhIGRpZmZlcmVudCBibG9jay1sZXZlbCBjb250YWluZXIuXG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cblxuICAvLyBJbiB0aGUgYWJvdmUgZXhhbXBsZSwgcmFuZ2Ugbm93IHNwYW5zIGZyb20gXCI8ZGl2PlwiIHRvIFwidW5kZXJcIixcbiAgLy8gYmVzdENvbnRhaW5lciBpcyA8ZGl2PiwgYW5kIGJlc3RPZmZzZXQgaXMgMSAoaW5kZXggb2YgPHE+IGluc2lkZSA8ZGl2PildLlxuICAvLyBQaWNraW5nIG91dCB3aGljaCBjaGlsZCB0byByZWN1cnNlIGludG8gaGVyZSBpcyBhIHNwZWNpYWwgY2FzZSBzaW5jZSB3ZVxuICAvLyBkb24ndCB3YW50IHRvIGNoZWNrIHBhc3QgPHE+IC0tIG9uY2Ugd2UgZmluZCB0aGF0IHRoZSBmaW5hbCByYW5nZSBzdGFydHNcbiAgLy8gaW4gPHNwYW4+LCB3ZSBjYW4gbG9vayBhdCBhbGwgb2YgaXRzIGNoaWxkcmVuIChhbmQgYWxsIG9mIHRoZWlyIGNoaWxkcmVuKVxuICAvLyB0byBmaW5kIHRoZSBicmVhayBwb2ludC5cblxuICAvLyBBdCBhbGwgdGltZXMsIChiZXN0Q29udGFpbmVyLCBiZXN0T2Zmc2V0KSBpcyB0aGUgbGF0ZXN0IHNpbmdsZS1saW5lIHN0YXJ0XG4gIC8vIHBvaW50IHRoYXQgd2Uga25vdyBvZi5cbiAgdmFyIGN1cnJlbnRDb250YWluZXIgPSBiZXN0Q29udGFpbmVyO1xuICB2YXIgbWF4SW5kZXhUb0NvbnNpZGVyID0gYmVzdE9mZnNldCAtIDE7XG5cbiAgZG8ge1xuICAgIHZhciBub2RlVmFsdWUgPSBjdXJyZW50Q29udGFpbmVyLm5vZGVWYWx1ZTtcblxuICAgIGZvciAodmFyIGlpID0gbWF4SW5kZXhUb0NvbnNpZGVyOyBpaSA+PSAwOyBpaS0tKSB7XG4gICAgICBpZiAobm9kZVZhbHVlICE9IG51bGwgJiYgaWkgPiAwICYmIFVuaWNvZGVVdGlscy5pc1N1cnJvZ2F0ZVBhaXIobm9kZVZhbHVlLCBpaSAtIDEpKSB7XG4gICAgICAgIC8vIFdlJ3JlIGluIHRoZSBtaWRkbGUgb2YgYSBzdXJyb2dhdGUgcGFpciAtLSBza2lwIG92ZXIgc28gd2UgbmV2ZXJcbiAgICAgICAgLy8gcmV0dXJuIGEgcmFuZ2Ugd2l0aCBhbiBlbmRwb2ludCBpbiB0aGUgbWlkZGxlIG9mIGEgY29kZSBwb2ludC5cbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIHJhbmdlLnNldFN0YXJ0KGN1cnJlbnRDb250YWluZXIsIGlpKTtcbiAgICAgIGlmIChhcmVSZWN0c09uT25lTGluZShnZXRSYW5nZUNsaWVudFJlY3RzKHJhbmdlKSwgbGluZUhlaWdodCkpIHtcbiAgICAgICAgYmVzdENvbnRhaW5lciA9IGN1cnJlbnRDb250YWluZXI7XG4gICAgICAgIGJlc3RPZmZzZXQgPSBpaTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChpaSA9PT0gLTEgfHwgY3VycmVudENvbnRhaW5lci5jaGlsZE5vZGVzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgLy8gSWYgaWkgPT09IC0xLCB0aGVuIChiZXN0Q29udGFpbmVyLCBiZXN0T2Zmc2V0KSwgd2hpY2ggaXMgZXF1YWwgdG9cbiAgICAgIC8vIChjdXJyZW50Q29udGFpbmVyLCAwKSwgd2FzIGEgc2luZ2xlLWxpbmUgc3RhcnQgcG9pbnQgYnV0IGEgc3RhcnRcbiAgICAgIC8vIHBvaW50IGJlZm9yZSBjdXJyZW50Q29udGFpbmVyIHdhc24ndCwgc28gdGhlIGxpbmUgYnJlYWsgc2VlbXMgdG9cbiAgICAgIC8vIGhhdmUgb2NjdXJyZWQgaW1tZWRpYXRlbHkgYWZ0ZXIgY3VycmVudENvbnRhaW5lcidzIHN0YXJ0IHRhZ1xuICAgICAgLy9cbiAgICAgIC8vIElmIGN1cnJlbnRDb250YWluZXIuY2hpbGROb2Rlcy5sZW5ndGggPT09IDAsIHdlJ3JlIGFscmVhZHkgYXQgYVxuICAgICAgLy8gdGVybWluYWwgbm9kZSAoZS5nLiwgdGV4dCBub2RlKSBhbmQgc2hvdWxkIHJldHVybiBvdXIgY3VycmVudCBiZXN0LlxuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgY3VycmVudENvbnRhaW5lciA9IGN1cnJlbnRDb250YWluZXIuY2hpbGROb2Rlc1tpaV07XG4gICAgbWF4SW5kZXhUb0NvbnNpZGVyID0gZ2V0Tm9kZUxlbmd0aChjdXJyZW50Q29udGFpbmVyKTtcbiAgfSB3aGlsZSAodHJ1ZSk7XG5cbiAgcmFuZ2Uuc2V0U3RhcnQoYmVzdENvbnRhaW5lciwgYmVzdE9mZnNldCk7XG4gIHJldHVybiByYW5nZTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBleHBhbmRSYW5nZVRvU3RhcnRPZkxpbmU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFSQTtBQVVBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/expandRangeToStartOfLine.js
