var matrix = __webpack_require__(/*! ../core/matrix */ "./node_modules/zrender/lib/core/matrix.js");

var vector = __webpack_require__(/*! ../core/vector */ "./node_modules/zrender/lib/core/vector.js");
/**
 * 提供变换扩展
 * @module zrender/mixin/Transformable
 * @author pissang (https://www.github.com/pissang)
 */


var mIdentity = matrix.identity;
var EPSILON = 5e-5;

function isNotAroundZero(val) {
  return val > EPSILON || val < -EPSILON;
}
/**
 * @alias module:zrender/mixin/Transformable
 * @constructor
 */


var Transformable = function Transformable(opts) {
  opts = opts || {}; // If there are no given position, rotation, scale

  if (!opts.position) {
    /**
     * 平移
     * @type {Array.<number>}
     * @default [0, 0]
     */
    this.position = [0, 0];
  }

  if (opts.rotation == null) {
    /**
     * 旋转
     * @type {Array.<number>}
     * @default 0
     */
    this.rotation = 0;
  }

  if (!opts.scale) {
    /**
     * 缩放
     * @type {Array.<number>}
     * @default [1, 1]
     */
    this.scale = [1, 1];
  }
  /**
   * 旋转和缩放的原点
   * @type {Array.<number>}
   * @default null
   */


  this.origin = this.origin || null;
};

var transformableProto = Transformable.prototype;
transformableProto.transform = null;
/**
 * 判断是否需要有坐标变换
 * 如果有坐标变换, 则从position, rotation, scale以及父节点的transform计算出自身的transform矩阵
 */

transformableProto.needLocalTransform = function () {
  return isNotAroundZero(this.rotation) || isNotAroundZero(this.position[0]) || isNotAroundZero(this.position[1]) || isNotAroundZero(this.scale[0] - 1) || isNotAroundZero(this.scale[1] - 1);
};

var scaleTmp = [];

transformableProto.updateTransform = function () {
  var parent = this.parent;
  var parentHasTransform = parent && parent.transform;
  var needLocalTransform = this.needLocalTransform();
  var m = this.transform;

  if (!(needLocalTransform || parentHasTransform)) {
    m && mIdentity(m);
    return;
  }

  m = m || matrix.create();

  if (needLocalTransform) {
    this.getLocalTransform(m);
  } else {
    mIdentity(m);
  } // 应用父节点变换


  if (parentHasTransform) {
    if (needLocalTransform) {
      matrix.mul(m, parent.transform, m);
    } else {
      matrix.copy(m, parent.transform);
    }
  } // 保存这个变换矩阵


  this.transform = m;
  var globalScaleRatio = this.globalScaleRatio;

  if (globalScaleRatio != null && globalScaleRatio !== 1) {
    this.getGlobalScale(scaleTmp);
    var relX = scaleTmp[0] < 0 ? -1 : 1;
    var relY = scaleTmp[1] < 0 ? -1 : 1;
    var sx = ((scaleTmp[0] - relX) * globalScaleRatio + relX) / scaleTmp[0] || 0;
    var sy = ((scaleTmp[1] - relY) * globalScaleRatio + relY) / scaleTmp[1] || 0;
    m[0] *= sx;
    m[1] *= sx;
    m[2] *= sy;
    m[3] *= sy;
  }

  this.invTransform = this.invTransform || matrix.create();
  matrix.invert(this.invTransform, m);
};

transformableProto.getLocalTransform = function (m) {
  return Transformable.getLocalTransform(this, m);
};
/**
 * 将自己的transform应用到context上
 * @param {CanvasRenderingContext2D} ctx
 */


transformableProto.setTransform = function (ctx) {
  var m = this.transform;
  var dpr = ctx.dpr || 1;

  if (m) {
    ctx.setTransform(dpr * m[0], dpr * m[1], dpr * m[2], dpr * m[3], dpr * m[4], dpr * m[5]);
  } else {
    ctx.setTransform(dpr, 0, 0, dpr, 0, 0);
  }
};

transformableProto.restoreTransform = function (ctx) {
  var dpr = ctx.dpr || 1;
  ctx.setTransform(dpr, 0, 0, dpr, 0, 0);
};

var tmpTransform = [];
var originTransform = matrix.create();

transformableProto.setLocalTransform = function (m) {
  if (!m) {
    // TODO return or set identity?
    return;
  }

  var sx = m[0] * m[0] + m[1] * m[1];
  var sy = m[2] * m[2] + m[3] * m[3];
  var position = this.position;
  var scale = this.scale;

  if (isNotAroundZero(sx - 1)) {
    sx = Math.sqrt(sx);
  }

  if (isNotAroundZero(sy - 1)) {
    sy = Math.sqrt(sy);
  }

  if (m[0] < 0) {
    sx = -sx;
  }

  if (m[3] < 0) {
    sy = -sy;
  }

  position[0] = m[4];
  position[1] = m[5];
  scale[0] = sx;
  scale[1] = sy;
  this.rotation = Math.atan2(-m[1] / sy, m[0] / sx);
};
/**
 * 分解`transform`矩阵到`position`, `rotation`, `scale`
 */


transformableProto.decomposeTransform = function () {
  if (!this.transform) {
    return;
  }

  var parent = this.parent;
  var m = this.transform;

  if (parent && parent.transform) {
    // Get local transform and decompose them to position, scale, rotation
    matrix.mul(tmpTransform, parent.invTransform, m);
    m = tmpTransform;
  }

  var origin = this.origin;

  if (origin && (origin[0] || origin[1])) {
    originTransform[4] = origin[0];
    originTransform[5] = origin[1];
    matrix.mul(tmpTransform, m, originTransform);
    tmpTransform[4] -= origin[0];
    tmpTransform[5] -= origin[1];
    m = tmpTransform;
  }

  this.setLocalTransform(m);
};
/**
 * Get global scale
 * @return {Array.<number>}
 */


transformableProto.getGlobalScale = function (out) {
  var m = this.transform;
  out = out || [];

  if (!m) {
    out[0] = 1;
    out[1] = 1;
    return out;
  }

  out[0] = Math.sqrt(m[0] * m[0] + m[1] * m[1]);
  out[1] = Math.sqrt(m[2] * m[2] + m[3] * m[3]);

  if (m[0] < 0) {
    out[0] = -out[0];
  }

  if (m[3] < 0) {
    out[1] = -out[1];
  }

  return out;
};
/**
 * 变换坐标位置到 shape 的局部坐标空间
 * @method
 * @param {number} x
 * @param {number} y
 * @return {Array.<number>}
 */


transformableProto.transformCoordToLocal = function (x, y) {
  var v2 = [x, y];
  var invTransform = this.invTransform;

  if (invTransform) {
    vector.applyTransform(v2, v2, invTransform);
  }

  return v2;
};
/**
 * 变换局部坐标位置到全局坐标空间
 * @method
 * @param {number} x
 * @param {number} y
 * @return {Array.<number>}
 */


transformableProto.transformCoordToGlobal = function (x, y) {
  var v2 = [x, y];
  var transform = this.transform;

  if (transform) {
    vector.applyTransform(v2, v2, transform);
  }

  return v2;
};
/**
 * @static
 * @param {Object} target
 * @param {Array.<number>} target.origin
 * @param {number} target.rotation
 * @param {Array.<number>} target.position
 * @param {Array.<number>} [m]
 */


Transformable.getLocalTransform = function (target, m) {
  m = m || [];
  mIdentity(m);
  var origin = target.origin;
  var scale = target.scale || [1, 1];
  var rotation = target.rotation || 0;
  var position = target.position || [0, 0];

  if (origin) {
    // Translate to origin
    m[4] -= origin[0];
    m[5] -= origin[1];
  }

  matrix.scale(m, m, scale);

  if (rotation) {
    matrix.rotate(m, m, rotation);
  }

  if (origin) {
    // Translate back from origin
    m[4] += origin[0];
    m[5] += origin[1];
  }

  m[4] += position[0];
  m[5] += position[1];
  return m;
};

var _default = Transformable;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvbWl4aW4vVHJhbnNmb3JtYWJsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL21peGluL1RyYW5zZm9ybWFibGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIG1hdHJpeCA9IHJlcXVpcmUoXCIuLi9jb3JlL21hdHJpeFwiKTtcblxudmFyIHZlY3RvciA9IHJlcXVpcmUoXCIuLi9jb3JlL3ZlY3RvclwiKTtcblxuLyoqXG4gKiDmj5Dkvpvlj5jmjaLmianlsZVcbiAqIEBtb2R1bGUgenJlbmRlci9taXhpbi9UcmFuc2Zvcm1hYmxlXG4gKiBAYXV0aG9yIHBpc3NhbmcgKGh0dHBzOi8vd3d3LmdpdGh1Yi5jb20vcGlzc2FuZylcbiAqL1xudmFyIG1JZGVudGl0eSA9IG1hdHJpeC5pZGVudGl0eTtcbnZhciBFUFNJTE9OID0gNWUtNTtcblxuZnVuY3Rpb24gaXNOb3RBcm91bmRaZXJvKHZhbCkge1xuICByZXR1cm4gdmFsID4gRVBTSUxPTiB8fCB2YWwgPCAtRVBTSUxPTjtcbn1cbi8qKlxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL21peGluL1RyYW5zZm9ybWFibGVcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblxudmFyIFRyYW5zZm9ybWFibGUgPSBmdW5jdGlvbiAob3B0cykge1xuICBvcHRzID0gb3B0cyB8fCB7fTsgLy8gSWYgdGhlcmUgYXJlIG5vIGdpdmVuIHBvc2l0aW9uLCByb3RhdGlvbiwgc2NhbGVcblxuICBpZiAoIW9wdHMucG9zaXRpb24pIHtcbiAgICAvKipcbiAgICAgKiDlubPnp7tcbiAgICAgKiBAdHlwZSB7QXJyYXkuPG51bWJlcj59XG4gICAgICogQGRlZmF1bHQgWzAsIDBdXG4gICAgICovXG4gICAgdGhpcy5wb3NpdGlvbiA9IFswLCAwXTtcbiAgfVxuXG4gIGlmIChvcHRzLnJvdGF0aW9uID09IG51bGwpIHtcbiAgICAvKipcbiAgICAgKiDml4vovaxcbiAgICAgKiBAdHlwZSB7QXJyYXkuPG51bWJlcj59XG4gICAgICogQGRlZmF1bHQgMFxuICAgICAqL1xuICAgIHRoaXMucm90YXRpb24gPSAwO1xuICB9XG5cbiAgaWYgKCFvcHRzLnNjYWxlKSB7XG4gICAgLyoqXG4gICAgICog57yp5pS+XG4gICAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgICAqIEBkZWZhdWx0IFsxLCAxXVxuICAgICAqL1xuICAgIHRoaXMuc2NhbGUgPSBbMSwgMV07XG4gIH1cbiAgLyoqXG4gICAqIOaXi+i9rOWSjOe8qeaUvueahOWOn+eCuVxuICAgKiBAdHlwZSB7QXJyYXkuPG51bWJlcj59XG4gICAqIEBkZWZhdWx0IG51bGxcbiAgICovXG5cblxuICB0aGlzLm9yaWdpbiA9IHRoaXMub3JpZ2luIHx8IG51bGw7XG59O1xuXG52YXIgdHJhbnNmb3JtYWJsZVByb3RvID0gVHJhbnNmb3JtYWJsZS5wcm90b3R5cGU7XG50cmFuc2Zvcm1hYmxlUHJvdG8udHJhbnNmb3JtID0gbnVsbDtcbi8qKlxuICog5Yik5pat5piv5ZCm6ZyA6KaB5pyJ5Z2Q5qCH5Y+Y5o2iXG4gKiDlpoLmnpzmnInlnZDmoIflj5jmjaIsIOWImeS7jnBvc2l0aW9uLCByb3RhdGlvbiwgc2NhbGXku6Xlj4rniLboioLngrnnmoR0cmFuc2Zvcm3orqHnrpflh7roh6rouqvnmoR0cmFuc2Zvcm3nn6npmLVcbiAqL1xuXG50cmFuc2Zvcm1hYmxlUHJvdG8ubmVlZExvY2FsVHJhbnNmb3JtID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gaXNOb3RBcm91bmRaZXJvKHRoaXMucm90YXRpb24pIHx8IGlzTm90QXJvdW5kWmVybyh0aGlzLnBvc2l0aW9uWzBdKSB8fCBpc05vdEFyb3VuZFplcm8odGhpcy5wb3NpdGlvblsxXSkgfHwgaXNOb3RBcm91bmRaZXJvKHRoaXMuc2NhbGVbMF0gLSAxKSB8fCBpc05vdEFyb3VuZFplcm8odGhpcy5zY2FsZVsxXSAtIDEpO1xufTtcblxudmFyIHNjYWxlVG1wID0gW107XG5cbnRyYW5zZm9ybWFibGVQcm90by51cGRhdGVUcmFuc2Zvcm0gPSBmdW5jdGlvbiAoKSB7XG4gIHZhciBwYXJlbnQgPSB0aGlzLnBhcmVudDtcbiAgdmFyIHBhcmVudEhhc1RyYW5zZm9ybSA9IHBhcmVudCAmJiBwYXJlbnQudHJhbnNmb3JtO1xuICB2YXIgbmVlZExvY2FsVHJhbnNmb3JtID0gdGhpcy5uZWVkTG9jYWxUcmFuc2Zvcm0oKTtcbiAgdmFyIG0gPSB0aGlzLnRyYW5zZm9ybTtcblxuICBpZiAoIShuZWVkTG9jYWxUcmFuc2Zvcm0gfHwgcGFyZW50SGFzVHJhbnNmb3JtKSkge1xuICAgIG0gJiYgbUlkZW50aXR5KG0pO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIG0gPSBtIHx8IG1hdHJpeC5jcmVhdGUoKTtcblxuICBpZiAobmVlZExvY2FsVHJhbnNmb3JtKSB7XG4gICAgdGhpcy5nZXRMb2NhbFRyYW5zZm9ybShtKTtcbiAgfSBlbHNlIHtcbiAgICBtSWRlbnRpdHkobSk7XG4gIH0gLy8g5bqU55So54i26IqC54K55Y+Y5o2iXG5cblxuICBpZiAocGFyZW50SGFzVHJhbnNmb3JtKSB7XG4gICAgaWYgKG5lZWRMb2NhbFRyYW5zZm9ybSkge1xuICAgICAgbWF0cml4Lm11bChtLCBwYXJlbnQudHJhbnNmb3JtLCBtKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbWF0cml4LmNvcHkobSwgcGFyZW50LnRyYW5zZm9ybSk7XG4gICAgfVxuICB9IC8vIOS/neWtmOi/meS4quWPmOaNouefqemYtVxuXG5cbiAgdGhpcy50cmFuc2Zvcm0gPSBtO1xuICB2YXIgZ2xvYmFsU2NhbGVSYXRpbyA9IHRoaXMuZ2xvYmFsU2NhbGVSYXRpbztcblxuICBpZiAoZ2xvYmFsU2NhbGVSYXRpbyAhPSBudWxsICYmIGdsb2JhbFNjYWxlUmF0aW8gIT09IDEpIHtcbiAgICB0aGlzLmdldEdsb2JhbFNjYWxlKHNjYWxlVG1wKTtcbiAgICB2YXIgcmVsWCA9IHNjYWxlVG1wWzBdIDwgMCA/IC0xIDogMTtcbiAgICB2YXIgcmVsWSA9IHNjYWxlVG1wWzFdIDwgMCA/IC0xIDogMTtcbiAgICB2YXIgc3ggPSAoKHNjYWxlVG1wWzBdIC0gcmVsWCkgKiBnbG9iYWxTY2FsZVJhdGlvICsgcmVsWCkgLyBzY2FsZVRtcFswXSB8fCAwO1xuICAgIHZhciBzeSA9ICgoc2NhbGVUbXBbMV0gLSByZWxZKSAqIGdsb2JhbFNjYWxlUmF0aW8gKyByZWxZKSAvIHNjYWxlVG1wWzFdIHx8IDA7XG4gICAgbVswXSAqPSBzeDtcbiAgICBtWzFdICo9IHN4O1xuICAgIG1bMl0gKj0gc3k7XG4gICAgbVszXSAqPSBzeTtcbiAgfVxuXG4gIHRoaXMuaW52VHJhbnNmb3JtID0gdGhpcy5pbnZUcmFuc2Zvcm0gfHwgbWF0cml4LmNyZWF0ZSgpO1xuICBtYXRyaXguaW52ZXJ0KHRoaXMuaW52VHJhbnNmb3JtLCBtKTtcbn07XG5cbnRyYW5zZm9ybWFibGVQcm90by5nZXRMb2NhbFRyYW5zZm9ybSA9IGZ1bmN0aW9uIChtKSB7XG4gIHJldHVybiBUcmFuc2Zvcm1hYmxlLmdldExvY2FsVHJhbnNmb3JtKHRoaXMsIG0pO1xufTtcbi8qKlxuICog5bCG6Ieq5bex55qEdHJhbnNmb3Jt5bqU55So5YiwY29udGV4dOS4ilxuICogQHBhcmFtIHtDYW52YXNSZW5kZXJpbmdDb250ZXh0MkR9IGN0eFxuICovXG5cblxudHJhbnNmb3JtYWJsZVByb3RvLnNldFRyYW5zZm9ybSA9IGZ1bmN0aW9uIChjdHgpIHtcbiAgdmFyIG0gPSB0aGlzLnRyYW5zZm9ybTtcbiAgdmFyIGRwciA9IGN0eC5kcHIgfHwgMTtcblxuICBpZiAobSkge1xuICAgIGN0eC5zZXRUcmFuc2Zvcm0oZHByICogbVswXSwgZHByICogbVsxXSwgZHByICogbVsyXSwgZHByICogbVszXSwgZHByICogbVs0XSwgZHByICogbVs1XSk7XG4gIH0gZWxzZSB7XG4gICAgY3R4LnNldFRyYW5zZm9ybShkcHIsIDAsIDAsIGRwciwgMCwgMCk7XG4gIH1cbn07XG5cbnRyYW5zZm9ybWFibGVQcm90by5yZXN0b3JlVHJhbnNmb3JtID0gZnVuY3Rpb24gKGN0eCkge1xuICB2YXIgZHByID0gY3R4LmRwciB8fCAxO1xuICBjdHguc2V0VHJhbnNmb3JtKGRwciwgMCwgMCwgZHByLCAwLCAwKTtcbn07XG5cbnZhciB0bXBUcmFuc2Zvcm0gPSBbXTtcbnZhciBvcmlnaW5UcmFuc2Zvcm0gPSBtYXRyaXguY3JlYXRlKCk7XG5cbnRyYW5zZm9ybWFibGVQcm90by5zZXRMb2NhbFRyYW5zZm9ybSA9IGZ1bmN0aW9uIChtKSB7XG4gIGlmICghbSkge1xuICAgIC8vIFRPRE8gcmV0dXJuIG9yIHNldCBpZGVudGl0eT9cbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgc3ggPSBtWzBdICogbVswXSArIG1bMV0gKiBtWzFdO1xuICB2YXIgc3kgPSBtWzJdICogbVsyXSArIG1bM10gKiBtWzNdO1xuICB2YXIgcG9zaXRpb24gPSB0aGlzLnBvc2l0aW9uO1xuICB2YXIgc2NhbGUgPSB0aGlzLnNjYWxlO1xuXG4gIGlmIChpc05vdEFyb3VuZFplcm8oc3ggLSAxKSkge1xuICAgIHN4ID0gTWF0aC5zcXJ0KHN4KTtcbiAgfVxuXG4gIGlmIChpc05vdEFyb3VuZFplcm8oc3kgLSAxKSkge1xuICAgIHN5ID0gTWF0aC5zcXJ0KHN5KTtcbiAgfVxuXG4gIGlmIChtWzBdIDwgMCkge1xuICAgIHN4ID0gLXN4O1xuICB9XG5cbiAgaWYgKG1bM10gPCAwKSB7XG4gICAgc3kgPSAtc3k7XG4gIH1cblxuICBwb3NpdGlvblswXSA9IG1bNF07XG4gIHBvc2l0aW9uWzFdID0gbVs1XTtcbiAgc2NhbGVbMF0gPSBzeDtcbiAgc2NhbGVbMV0gPSBzeTtcbiAgdGhpcy5yb3RhdGlvbiA9IE1hdGguYXRhbjIoLW1bMV0gLyBzeSwgbVswXSAvIHN4KTtcbn07XG4vKipcbiAqIOWIhuino2B0cmFuc2Zvcm1g55+p6Zi15YiwYHBvc2l0aW9uYCwgYHJvdGF0aW9uYCwgYHNjYWxlYFxuICovXG5cblxudHJhbnNmb3JtYWJsZVByb3RvLmRlY29tcG9zZVRyYW5zZm9ybSA9IGZ1bmN0aW9uICgpIHtcbiAgaWYgKCF0aGlzLnRyYW5zZm9ybSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBwYXJlbnQgPSB0aGlzLnBhcmVudDtcbiAgdmFyIG0gPSB0aGlzLnRyYW5zZm9ybTtcblxuICBpZiAocGFyZW50ICYmIHBhcmVudC50cmFuc2Zvcm0pIHtcbiAgICAvLyBHZXQgbG9jYWwgdHJhbnNmb3JtIGFuZCBkZWNvbXBvc2UgdGhlbSB0byBwb3NpdGlvbiwgc2NhbGUsIHJvdGF0aW9uXG4gICAgbWF0cml4Lm11bCh0bXBUcmFuc2Zvcm0sIHBhcmVudC5pbnZUcmFuc2Zvcm0sIG0pO1xuICAgIG0gPSB0bXBUcmFuc2Zvcm07XG4gIH1cblxuICB2YXIgb3JpZ2luID0gdGhpcy5vcmlnaW47XG5cbiAgaWYgKG9yaWdpbiAmJiAob3JpZ2luWzBdIHx8IG9yaWdpblsxXSkpIHtcbiAgICBvcmlnaW5UcmFuc2Zvcm1bNF0gPSBvcmlnaW5bMF07XG4gICAgb3JpZ2luVHJhbnNmb3JtWzVdID0gb3JpZ2luWzFdO1xuICAgIG1hdHJpeC5tdWwodG1wVHJhbnNmb3JtLCBtLCBvcmlnaW5UcmFuc2Zvcm0pO1xuICAgIHRtcFRyYW5zZm9ybVs0XSAtPSBvcmlnaW5bMF07XG4gICAgdG1wVHJhbnNmb3JtWzVdIC09IG9yaWdpblsxXTtcbiAgICBtID0gdG1wVHJhbnNmb3JtO1xuICB9XG5cbiAgdGhpcy5zZXRMb2NhbFRyYW5zZm9ybShtKTtcbn07XG4vKipcbiAqIEdldCBnbG9iYWwgc2NhbGVcbiAqIEByZXR1cm4ge0FycmF5LjxudW1iZXI+fVxuICovXG5cblxudHJhbnNmb3JtYWJsZVByb3RvLmdldEdsb2JhbFNjYWxlID0gZnVuY3Rpb24gKG91dCkge1xuICB2YXIgbSA9IHRoaXMudHJhbnNmb3JtO1xuICBvdXQgPSBvdXQgfHwgW107XG5cbiAgaWYgKCFtKSB7XG4gICAgb3V0WzBdID0gMTtcbiAgICBvdXRbMV0gPSAxO1xuICAgIHJldHVybiBvdXQ7XG4gIH1cblxuICBvdXRbMF0gPSBNYXRoLnNxcnQobVswXSAqIG1bMF0gKyBtWzFdICogbVsxXSk7XG4gIG91dFsxXSA9IE1hdGguc3FydChtWzJdICogbVsyXSArIG1bM10gKiBtWzNdKTtcblxuICBpZiAobVswXSA8IDApIHtcbiAgICBvdXRbMF0gPSAtb3V0WzBdO1xuICB9XG5cbiAgaWYgKG1bM10gPCAwKSB7XG4gICAgb3V0WzFdID0gLW91dFsxXTtcbiAgfVxuXG4gIHJldHVybiBvdXQ7XG59O1xuLyoqXG4gKiDlj5jmjaLlnZDmoIfkvY3nva7liLAgc2hhcGUg55qE5bGA6YOo5Z2Q5qCH56m66Ze0XG4gKiBAbWV0aG9kXG4gKiBAcGFyYW0ge251bWJlcn0geFxuICogQHBhcmFtIHtudW1iZXJ9IHlcbiAqIEByZXR1cm4ge0FycmF5LjxudW1iZXI+fVxuICovXG5cblxudHJhbnNmb3JtYWJsZVByb3RvLnRyYW5zZm9ybUNvb3JkVG9Mb2NhbCA9IGZ1bmN0aW9uICh4LCB5KSB7XG4gIHZhciB2MiA9IFt4LCB5XTtcbiAgdmFyIGludlRyYW5zZm9ybSA9IHRoaXMuaW52VHJhbnNmb3JtO1xuXG4gIGlmIChpbnZUcmFuc2Zvcm0pIHtcbiAgICB2ZWN0b3IuYXBwbHlUcmFuc2Zvcm0odjIsIHYyLCBpbnZUcmFuc2Zvcm0pO1xuICB9XG5cbiAgcmV0dXJuIHYyO1xufTtcbi8qKlxuICog5Y+Y5o2i5bGA6YOo5Z2Q5qCH5L2N572u5Yiw5YWo5bGA5Z2Q5qCH56m66Ze0XG4gKiBAbWV0aG9kXG4gKiBAcGFyYW0ge251bWJlcn0geFxuICogQHBhcmFtIHtudW1iZXJ9IHlcbiAqIEByZXR1cm4ge0FycmF5LjxudW1iZXI+fVxuICovXG5cblxudHJhbnNmb3JtYWJsZVByb3RvLnRyYW5zZm9ybUNvb3JkVG9HbG9iYWwgPSBmdW5jdGlvbiAoeCwgeSkge1xuICB2YXIgdjIgPSBbeCwgeV07XG4gIHZhciB0cmFuc2Zvcm0gPSB0aGlzLnRyYW5zZm9ybTtcblxuICBpZiAodHJhbnNmb3JtKSB7XG4gICAgdmVjdG9yLmFwcGx5VHJhbnNmb3JtKHYyLCB2MiwgdHJhbnNmb3JtKTtcbiAgfVxuXG4gIHJldHVybiB2Mjtcbn07XG4vKipcbiAqIEBzdGF0aWNcbiAqIEBwYXJhbSB7T2JqZWN0fSB0YXJnZXRcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHRhcmdldC5vcmlnaW5cbiAqIEBwYXJhbSB7bnVtYmVyfSB0YXJnZXQucm90YXRpb25cbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHRhcmdldC5wb3NpdGlvblxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW21dXG4gKi9cblxuXG5UcmFuc2Zvcm1hYmxlLmdldExvY2FsVHJhbnNmb3JtID0gZnVuY3Rpb24gKHRhcmdldCwgbSkge1xuICBtID0gbSB8fCBbXTtcbiAgbUlkZW50aXR5KG0pO1xuICB2YXIgb3JpZ2luID0gdGFyZ2V0Lm9yaWdpbjtcbiAgdmFyIHNjYWxlID0gdGFyZ2V0LnNjYWxlIHx8IFsxLCAxXTtcbiAgdmFyIHJvdGF0aW9uID0gdGFyZ2V0LnJvdGF0aW9uIHx8IDA7XG4gIHZhciBwb3NpdGlvbiA9IHRhcmdldC5wb3NpdGlvbiB8fCBbMCwgMF07XG5cbiAgaWYgKG9yaWdpbikge1xuICAgIC8vIFRyYW5zbGF0ZSB0byBvcmlnaW5cbiAgICBtWzRdIC09IG9yaWdpblswXTtcbiAgICBtWzVdIC09IG9yaWdpblsxXTtcbiAgfVxuXG4gIG1hdHJpeC5zY2FsZShtLCBtLCBzY2FsZSk7XG5cbiAgaWYgKHJvdGF0aW9uKSB7XG4gICAgbWF0cml4LnJvdGF0ZShtLCBtLCByb3RhdGlvbik7XG4gIH1cblxuICBpZiAob3JpZ2luKSB7XG4gICAgLy8gVHJhbnNsYXRlIGJhY2sgZnJvbSBvcmlnaW5cbiAgICBtWzRdICs9IG9yaWdpblswXTtcbiAgICBtWzVdICs9IG9yaWdpblsxXTtcbiAgfVxuXG4gIG1bNF0gKz0gcG9zaXRpb25bMF07XG4gIG1bNV0gKz0gcG9zaXRpb25bMV07XG4gIHJldHVybiBtO1xufTtcblxudmFyIF9kZWZhdWx0ID0gVHJhbnNmb3JtYWJsZTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/mixin/Transformable.js
