/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getRangeClientRects
 * @format
 * 
 */


var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var isChrome = UserAgent.isBrowser('Chrome'); // In Chrome, the client rects will include the entire bounds of all nodes that
// begin (have a start tag) within the selection, even if the selection does
// not overlap the entire node. To resolve this, we split the range at each
// start tag and join the client rects together.
// https://code.google.com/p/chromium/issues/detail?id=324437

/* eslint-disable consistent-return */

function getRangeClientRectsChrome(range) {
  var tempRange = range.cloneRange();
  var clientRects = [];

  for (var ancestor = range.endContainer; ancestor != null; ancestor = ancestor.parentNode) {
    // If we've climbed up to the common ancestor, we can now use the
    // original start point and stop climbing the tree.
    var atCommonAncestor = ancestor === range.commonAncestorContainer;

    if (atCommonAncestor) {
      tempRange.setStart(range.startContainer, range.startOffset);
    } else {
      tempRange.setStart(tempRange.endContainer, 0);
    }

    var rects = Array.from(tempRange.getClientRects());
    clientRects.push(rects);

    if (atCommonAncestor) {
      var _ref;

      clientRects.reverse();
      return (_ref = []).concat.apply(_ref, clientRects);
    }

    tempRange.setEndBefore(ancestor);
  }

   true ?  true ? invariant(false, 'Found an unexpected detached subtree when getting range client rects.') : undefined : undefined;
}
/* eslint-enable consistent-return */

/**
 * Like range.getClientRects() but normalizes for browser bugs.
 */


var getRangeClientRects = isChrome ? getRangeClientRectsChrome : function (range) {
  return Array.from(range.getClientRects());
};
module.exports = getRangeClientRects;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFJhbmdlQ2xpZW50UmVjdHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZ2V0UmFuZ2VDbGllbnRSZWN0cy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGdldFJhbmdlQ2xpZW50UmVjdHNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVzZXJBZ2VudCA9IHJlcXVpcmUoJ2ZianMvbGliL1VzZXJBZ2VudCcpO1xuXG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbnZhciBpc0Nocm9tZSA9IFVzZXJBZ2VudC5pc0Jyb3dzZXIoJ0Nocm9tZScpO1xuXG4vLyBJbiBDaHJvbWUsIHRoZSBjbGllbnQgcmVjdHMgd2lsbCBpbmNsdWRlIHRoZSBlbnRpcmUgYm91bmRzIG9mIGFsbCBub2RlcyB0aGF0XG4vLyBiZWdpbiAoaGF2ZSBhIHN0YXJ0IHRhZykgd2l0aGluIHRoZSBzZWxlY3Rpb24sIGV2ZW4gaWYgdGhlIHNlbGVjdGlvbiBkb2VzXG4vLyBub3Qgb3ZlcmxhcCB0aGUgZW50aXJlIG5vZGUuIFRvIHJlc29sdmUgdGhpcywgd2Ugc3BsaXQgdGhlIHJhbmdlIGF0IGVhY2hcbi8vIHN0YXJ0IHRhZyBhbmQgam9pbiB0aGUgY2xpZW50IHJlY3RzIHRvZ2V0aGVyLlxuLy8gaHR0cHM6Ly9jb2RlLmdvb2dsZS5jb20vcC9jaHJvbWl1bS9pc3N1ZXMvZGV0YWlsP2lkPTMyNDQzN1xuLyogZXNsaW50LWRpc2FibGUgY29uc2lzdGVudC1yZXR1cm4gKi9cbmZ1bmN0aW9uIGdldFJhbmdlQ2xpZW50UmVjdHNDaHJvbWUocmFuZ2UpIHtcbiAgdmFyIHRlbXBSYW5nZSA9IHJhbmdlLmNsb25lUmFuZ2UoKTtcbiAgdmFyIGNsaWVudFJlY3RzID0gW107XG5cbiAgZm9yICh2YXIgYW5jZXN0b3IgPSByYW5nZS5lbmRDb250YWluZXI7IGFuY2VzdG9yICE9IG51bGw7IGFuY2VzdG9yID0gYW5jZXN0b3IucGFyZW50Tm9kZSkge1xuICAgIC8vIElmIHdlJ3ZlIGNsaW1iZWQgdXAgdG8gdGhlIGNvbW1vbiBhbmNlc3Rvciwgd2UgY2FuIG5vdyB1c2UgdGhlXG4gICAgLy8gb3JpZ2luYWwgc3RhcnQgcG9pbnQgYW5kIHN0b3AgY2xpbWJpbmcgdGhlIHRyZWUuXG4gICAgdmFyIGF0Q29tbW9uQW5jZXN0b3IgPSBhbmNlc3RvciA9PT0gcmFuZ2UuY29tbW9uQW5jZXN0b3JDb250YWluZXI7XG4gICAgaWYgKGF0Q29tbW9uQW5jZXN0b3IpIHtcbiAgICAgIHRlbXBSYW5nZS5zZXRTdGFydChyYW5nZS5zdGFydENvbnRhaW5lciwgcmFuZ2Uuc3RhcnRPZmZzZXQpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0ZW1wUmFuZ2Uuc2V0U3RhcnQodGVtcFJhbmdlLmVuZENvbnRhaW5lciwgMCk7XG4gICAgfVxuICAgIHZhciByZWN0cyA9IEFycmF5LmZyb20odGVtcFJhbmdlLmdldENsaWVudFJlY3RzKCkpO1xuICAgIGNsaWVudFJlY3RzLnB1c2gocmVjdHMpO1xuICAgIGlmIChhdENvbW1vbkFuY2VzdG9yKSB7XG4gICAgICB2YXIgX3JlZjtcblxuICAgICAgY2xpZW50UmVjdHMucmV2ZXJzZSgpO1xuICAgICAgcmV0dXJuIChfcmVmID0gW10pLmNvbmNhdC5hcHBseShfcmVmLCBjbGllbnRSZWN0cyk7XG4gICAgfVxuICAgIHRlbXBSYW5nZS5zZXRFbmRCZWZvcmUoYW5jZXN0b3IpO1xuICB9XG5cbiAgIWZhbHNlID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0ZvdW5kIGFuIHVuZXhwZWN0ZWQgZGV0YWNoZWQgc3VidHJlZSB3aGVuIGdldHRpbmcgcmFuZ2UgY2xpZW50IHJlY3RzLicpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbn1cbi8qIGVzbGludC1lbmFibGUgY29uc2lzdGVudC1yZXR1cm4gKi9cblxuLyoqXG4gKiBMaWtlIHJhbmdlLmdldENsaWVudFJlY3RzKCkgYnV0IG5vcm1hbGl6ZXMgZm9yIGJyb3dzZXIgYnVncy5cbiAqL1xudmFyIGdldFJhbmdlQ2xpZW50UmVjdHMgPSBpc0Nocm9tZSA/IGdldFJhbmdlQ2xpZW50UmVjdHNDaHJvbWUgOiBmdW5jdGlvbiAocmFuZ2UpIHtcbiAgcmV0dXJuIEFycmF5LmZyb20ocmFuZ2UuZ2V0Q2xpZW50UmVjdHMoKSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGdldFJhbmdlQ2xpZW50UmVjdHM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getRangeClientRects.js
