/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var BoundingRect = __webpack_require__(/*! zrender/lib/core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var _number = __webpack_require__(/*! ./number */ "./node_modules/echarts/lib/util/number.js");

var parsePercent = _number.parsePercent;

var formatUtil = __webpack_require__(/*! ./format */ "./node_modules/echarts/lib/util/format.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Layout helpers for each component positioning


var each = zrUtil.each;
/**
 * @public
 */

var LOCATION_PARAMS = ['left', 'right', 'top', 'bottom', 'width', 'height'];
/**
 * @public
 */

var HV_NAMES = [['width', 'left', 'right'], ['height', 'top', 'bottom']];

function boxLayout(orient, group, gap, maxWidth, maxHeight) {
  var x = 0;
  var y = 0;

  if (maxWidth == null) {
    maxWidth = Infinity;
  }

  if (maxHeight == null) {
    maxHeight = Infinity;
  }

  var currentLineMaxSize = 0;
  group.eachChild(function (child, idx) {
    var position = child.position;
    var rect = child.getBoundingRect();
    var nextChild = group.childAt(idx + 1);
    var nextChildRect = nextChild && nextChild.getBoundingRect();
    var nextX;
    var nextY;

    if (orient === 'horizontal') {
      var moveX = rect.width + (nextChildRect ? -nextChildRect.x + rect.x : 0);
      nextX = x + moveX; // Wrap when width exceeds maxWidth or meet a `newline` group
      // FIXME compare before adding gap?

      if (nextX > maxWidth || child.newline) {
        x = 0;
        nextX = moveX;
        y += currentLineMaxSize + gap;
        currentLineMaxSize = rect.height;
      } else {
        // FIXME: consider rect.y is not `0`?
        currentLineMaxSize = Math.max(currentLineMaxSize, rect.height);
      }
    } else {
      var moveY = rect.height + (nextChildRect ? -nextChildRect.y + rect.y : 0);
      nextY = y + moveY; // Wrap when width exceeds maxHeight or meet a `newline` group

      if (nextY > maxHeight || child.newline) {
        x += currentLineMaxSize + gap;
        y = 0;
        nextY = moveY;
        currentLineMaxSize = rect.width;
      } else {
        currentLineMaxSize = Math.max(currentLineMaxSize, rect.width);
      }
    }

    if (child.newline) {
      return;
    }

    position[0] = x;
    position[1] = y;
    orient === 'horizontal' ? x = nextX + gap : y = nextY + gap;
  });
}
/**
 * VBox or HBox layouting
 * @param {string} orient
 * @param {module:zrender/container/Group} group
 * @param {number} gap
 * @param {number} [width=Infinity]
 * @param {number} [height=Infinity]
 */


var box = boxLayout;
/**
 * VBox layouting
 * @param {module:zrender/container/Group} group
 * @param {number} gap
 * @param {number} [width=Infinity]
 * @param {number} [height=Infinity]
 */

var vbox = zrUtil.curry(boxLayout, 'vertical');
/**
 * HBox layouting
 * @param {module:zrender/container/Group} group
 * @param {number} gap
 * @param {number} [width=Infinity]
 * @param {number} [height=Infinity]
 */

var hbox = zrUtil.curry(boxLayout, 'horizontal');
/**
 * If x or x2 is not specified or 'center' 'left' 'right',
 * the width would be as long as possible.
 * If y or y2 is not specified or 'middle' 'top' 'bottom',
 * the height would be as long as possible.
 *
 * @param {Object} positionInfo
 * @param {number|string} [positionInfo.x]
 * @param {number|string} [positionInfo.y]
 * @param {number|string} [positionInfo.x2]
 * @param {number|string} [positionInfo.y2]
 * @param {Object} containerRect {width, height}
 * @param {string|number} margin
 * @return {Object} {width, height}
 */

function getAvailableSize(positionInfo, containerRect, margin) {
  var containerWidth = containerRect.width;
  var containerHeight = containerRect.height;
  var x = parsePercent(positionInfo.x, containerWidth);
  var y = parsePercent(positionInfo.y, containerHeight);
  var x2 = parsePercent(positionInfo.x2, containerWidth);
  var y2 = parsePercent(positionInfo.y2, containerHeight);
  (isNaN(x) || isNaN(parseFloat(positionInfo.x))) && (x = 0);
  (isNaN(x2) || isNaN(parseFloat(positionInfo.x2))) && (x2 = containerWidth);
  (isNaN(y) || isNaN(parseFloat(positionInfo.y))) && (y = 0);
  (isNaN(y2) || isNaN(parseFloat(positionInfo.y2))) && (y2 = containerHeight);
  margin = formatUtil.normalizeCssArray(margin || 0);
  return {
    width: Math.max(x2 - x - margin[1] - margin[3], 0),
    height: Math.max(y2 - y - margin[0] - margin[2], 0)
  };
}
/**
 * Parse position info.
 *
 * @param {Object} positionInfo
 * @param {number|string} [positionInfo.left]
 * @param {number|string} [positionInfo.top]
 * @param {number|string} [positionInfo.right]
 * @param {number|string} [positionInfo.bottom]
 * @param {number|string} [positionInfo.width]
 * @param {number|string} [positionInfo.height]
 * @param {number|string} [positionInfo.aspect] Aspect is width / height
 * @param {Object} containerRect
 * @param {string|number} [margin]
 *
 * @return {module:zrender/core/BoundingRect}
 */


function getLayoutRect(positionInfo, containerRect, margin) {
  margin = formatUtil.normalizeCssArray(margin || 0);
  var containerWidth = containerRect.width;
  var containerHeight = containerRect.height;
  var left = parsePercent(positionInfo.left, containerWidth);
  var top = parsePercent(positionInfo.top, containerHeight);
  var right = parsePercent(positionInfo.right, containerWidth);
  var bottom = parsePercent(positionInfo.bottom, containerHeight);
  var width = parsePercent(positionInfo.width, containerWidth);
  var height = parsePercent(positionInfo.height, containerHeight);
  var verticalMargin = margin[2] + margin[0];
  var horizontalMargin = margin[1] + margin[3];
  var aspect = positionInfo.aspect; // If width is not specified, calculate width from left and right

  if (isNaN(width)) {
    width = containerWidth - right - horizontalMargin - left;
  }

  if (isNaN(height)) {
    height = containerHeight - bottom - verticalMargin - top;
  }

  if (aspect != null) {
    // If width and height are not given
    // 1. Graph should not exceeds the container
    // 2. Aspect must be keeped
    // 3. Graph should take the space as more as possible
    // FIXME
    // Margin is not considered, because there is no case that both
    // using margin and aspect so far.
    if (isNaN(width) && isNaN(height)) {
      if (aspect > containerWidth / containerHeight) {
        width = containerWidth * 0.8;
      } else {
        height = containerHeight * 0.8;
      }
    } // Calculate width or height with given aspect


    if (isNaN(width)) {
      width = aspect * height;
    }

    if (isNaN(height)) {
      height = width / aspect;
    }
  } // If left is not specified, calculate left from right and width


  if (isNaN(left)) {
    left = containerWidth - right - width - horizontalMargin;
  }

  if (isNaN(top)) {
    top = containerHeight - bottom - height - verticalMargin;
  } // Align left and top


  switch (positionInfo.left || positionInfo.right) {
    case 'center':
      left = containerWidth / 2 - width / 2 - margin[3];
      break;

    case 'right':
      left = containerWidth - width - horizontalMargin;
      break;
  }

  switch (positionInfo.top || positionInfo.bottom) {
    case 'middle':
    case 'center':
      top = containerHeight / 2 - height / 2 - margin[0];
      break;

    case 'bottom':
      top = containerHeight - height - verticalMargin;
      break;
  } // If something is wrong and left, top, width, height are calculated as NaN


  left = left || 0;
  top = top || 0;

  if (isNaN(width)) {
    // Width may be NaN if only one value is given except width
    width = containerWidth - horizontalMargin - left - (right || 0);
  }

  if (isNaN(height)) {
    // Height may be NaN if only one value is given except height
    height = containerHeight - verticalMargin - top - (bottom || 0);
  }

  var rect = new BoundingRect(left + margin[3], top + margin[0], width, height);
  rect.margin = margin;
  return rect;
}
/**
 * Position a zr element in viewport
 *  Group position is specified by either
 *  {left, top}, {right, bottom}
 *  If all properties exists, right and bottom will be igonred.
 *
 * Logic:
 *     1. Scale (against origin point in parent coord)
 *     2. Rotate (against origin point in parent coord)
 *     3. Traslate (with el.position by this method)
 * So this method only fixes the last step 'Traslate', which does not affect
 * scaling and rotating.
 *
 * If be called repeatly with the same input el, the same result will be gotten.
 *
 * @param {module:zrender/Element} el Should have `getBoundingRect` method.
 * @param {Object} positionInfo
 * @param {number|string} [positionInfo.left]
 * @param {number|string} [positionInfo.top]
 * @param {number|string} [positionInfo.right]
 * @param {number|string} [positionInfo.bottom]
 * @param {number|string} [positionInfo.width] Only for opt.boundingModel: 'raw'
 * @param {number|string} [positionInfo.height] Only for opt.boundingModel: 'raw'
 * @param {Object} containerRect
 * @param {string|number} margin
 * @param {Object} [opt]
 * @param {Array.<number>} [opt.hv=[1,1]] Only horizontal or only vertical.
 * @param {Array.<number>} [opt.boundingMode='all']
 *        Specify how to calculate boundingRect when locating.
 *        'all': Position the boundingRect that is transformed and uioned
 *               both itself and its descendants.
 *               This mode simplies confine the elements in the bounding
 *               of their container (e.g., using 'right: 0').
 *        'raw': Position the boundingRect that is not transformed and only itself.
 *               This mode is useful when you want a element can overflow its
 *               container. (Consider a rotated circle needs to be located in a corner.)
 *               In this mode positionInfo.width/height can only be number.
 */


function positionElement(el, positionInfo, containerRect, margin, opt) {
  var h = !opt || !opt.hv || opt.hv[0];
  var v = !opt || !opt.hv || opt.hv[1];
  var boundingMode = opt && opt.boundingMode || 'all';

  if (!h && !v) {
    return;
  }

  var rect;

  if (boundingMode === 'raw') {
    rect = el.type === 'group' ? new BoundingRect(0, 0, +positionInfo.width || 0, +positionInfo.height || 0) : el.getBoundingRect();
  } else {
    rect = el.getBoundingRect();

    if (el.needLocalTransform()) {
      var transform = el.getLocalTransform(); // Notice: raw rect may be inner object of el,
      // which should not be modified.

      rect = rect.clone();
      rect.applyTransform(transform);
    }
  } // The real width and height can not be specified but calculated by the given el.


  positionInfo = getLayoutRect(zrUtil.defaults({
    width: rect.width,
    height: rect.height
  }, positionInfo), containerRect, margin); // Because 'tranlate' is the last step in transform
  // (see zrender/core/Transformable#getLocalTransform),
  // we can just only modify el.position to get final result.

  var elPos = el.position;
  var dx = h ? positionInfo.x - rect.x : 0;
  var dy = v ? positionInfo.y - rect.y : 0;
  el.attr('position', boundingMode === 'raw' ? [dx, dy] : [elPos[0] + dx, elPos[1] + dy]);
}
/**
 * @param {Object} option Contains some of the properties in HV_NAMES.
 * @param {number} hvIdx 0: horizontal; 1: vertical.
 */


function sizeCalculable(option, hvIdx) {
  return option[HV_NAMES[hvIdx][0]] != null || option[HV_NAMES[hvIdx][1]] != null && option[HV_NAMES[hvIdx][2]] != null;
}
/**
 * Consider Case:
 * When defulat option has {left: 0, width: 100}, and we set {right: 0}
 * through setOption or media query, using normal zrUtil.merge will cause
 * {right: 0} does not take effect.
 *
 * @example
 * ComponentModel.extend({
 *     init: function () {
 *         ...
 *         var inputPositionParams = layout.getLayoutParams(option);
 *         this.mergeOption(inputPositionParams);
 *     },
 *     mergeOption: function (newOption) {
 *         newOption && zrUtil.merge(thisOption, newOption, true);
 *         layout.mergeLayoutParam(thisOption, newOption);
 *     }
 * });
 *
 * @param {Object} targetOption
 * @param {Object} newOption
 * @param {Object|string} [opt]
 * @param {boolean|Array.<boolean>} [opt.ignoreSize=false] Used for the components
 *  that width (or height) should not be calculated by left and right (or top and bottom).
 */


function mergeLayoutParam(targetOption, newOption, opt) {
  !zrUtil.isObject(opt) && (opt = {});
  var ignoreSize = opt.ignoreSize;
  !zrUtil.isArray(ignoreSize) && (ignoreSize = [ignoreSize, ignoreSize]);
  var hResult = merge(HV_NAMES[0], 0);
  var vResult = merge(HV_NAMES[1], 1);
  copy(HV_NAMES[0], targetOption, hResult);
  copy(HV_NAMES[1], targetOption, vResult);

  function merge(names, hvIdx) {
    var newParams = {};
    var newValueCount = 0;
    var merged = {};
    var mergedValueCount = 0;
    var enoughParamNumber = 2;
    each(names, function (name) {
      merged[name] = targetOption[name];
    });
    each(names, function (name) {
      // Consider case: newOption.width is null, which is
      // set by user for removing width setting.
      hasProp(newOption, name) && (newParams[name] = merged[name] = newOption[name]);
      hasValue(newParams, name) && newValueCount++;
      hasValue(merged, name) && mergedValueCount++;
    });

    if (ignoreSize[hvIdx]) {
      // Only one of left/right is premitted to exist.
      if (hasValue(newOption, names[1])) {
        merged[names[2]] = null;
      } else if (hasValue(newOption, names[2])) {
        merged[names[1]] = null;
      }

      return merged;
    } // Case: newOption: {width: ..., right: ...},
    // or targetOption: {right: ...} and newOption: {width: ...},
    // There is no conflict when merged only has params count
    // little than enoughParamNumber.


    if (mergedValueCount === enoughParamNumber || !newValueCount) {
      return merged;
    } // Case: newOption: {width: ..., right: ...},
    // Than we can make sure user only want those two, and ignore
    // all origin params in targetOption.
    else if (newValueCount >= enoughParamNumber) {
        return newParams;
      } else {
        // Chose another param from targetOption by priority.
        for (var i = 0; i < names.length; i++) {
          var name = names[i];

          if (!hasProp(newParams, name) && hasProp(targetOption, name)) {
            newParams[name] = targetOption[name];
            break;
          }
        }

        return newParams;
      }
  }

  function hasProp(obj, name) {
    return obj.hasOwnProperty(name);
  }

  function hasValue(obj, name) {
    return obj[name] != null && obj[name] !== 'auto';
  }

  function copy(names, target, source) {
    each(names, function (name) {
      target[name] = source[name];
    });
  }
}
/**
 * Retrieve 'left', 'right', 'top', 'bottom', 'width', 'height' from object.
 * @param {Object} source
 * @return {Object} Result contains those props.
 */


function getLayoutParams(source) {
  return copyLayoutParams({}, source);
}
/**
 * Retrieve 'left', 'right', 'top', 'bottom', 'width', 'height' from object.
 * @param {Object} source
 * @return {Object} Result contains those props.
 */


function copyLayoutParams(target, source) {
  source && target && each(LOCATION_PARAMS, function (name) {
    source.hasOwnProperty(name) && (target[name] = source[name]);
  });
  return target;
}

exports.LOCATION_PARAMS = LOCATION_PARAMS;
exports.HV_NAMES = HV_NAMES;
exports.box = box;
exports.vbox = vbox;
exports.hbox = hbox;
exports.getAvailableSize = getAvailableSize;
exports.getLayoutRect = getLayoutRect;
exports.positionElement = positionElement;
exports.sizeCalculable = sizeCalculable;
exports.mergeLayoutParam = mergeLayoutParam;
exports.getLayoutParams = getLayoutParams;
exports.copyLayoutParams = copyLayoutParams;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9sYXlvdXQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi91dGlsL2xheW91dC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4vbnVtYmVyXCIpO1xuXG52YXIgcGFyc2VQZXJjZW50ID0gX251bWJlci5wYXJzZVBlcmNlbnQ7XG5cbnZhciBmb3JtYXRVdGlsID0gcmVxdWlyZShcIi4vZm9ybWF0XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG4vLyBMYXlvdXQgaGVscGVycyBmb3IgZWFjaCBjb21wb25lbnQgcG9zaXRpb25pbmdcbnZhciBlYWNoID0genJVdGlsLmVhY2g7XG4vKipcbiAqIEBwdWJsaWNcbiAqL1xuXG52YXIgTE9DQVRJT05fUEFSQU1TID0gWydsZWZ0JywgJ3JpZ2h0JywgJ3RvcCcsICdib3R0b20nLCAnd2lkdGgnLCAnaGVpZ2h0J107XG4vKipcbiAqIEBwdWJsaWNcbiAqL1xuXG52YXIgSFZfTkFNRVMgPSBbWyd3aWR0aCcsICdsZWZ0JywgJ3JpZ2h0J10sIFsnaGVpZ2h0JywgJ3RvcCcsICdib3R0b20nXV07XG5cbmZ1bmN0aW9uIGJveExheW91dChvcmllbnQsIGdyb3VwLCBnYXAsIG1heFdpZHRoLCBtYXhIZWlnaHQpIHtcbiAgdmFyIHggPSAwO1xuICB2YXIgeSA9IDA7XG5cbiAgaWYgKG1heFdpZHRoID09IG51bGwpIHtcbiAgICBtYXhXaWR0aCA9IEluZmluaXR5O1xuICB9XG5cbiAgaWYgKG1heEhlaWdodCA9PSBudWxsKSB7XG4gICAgbWF4SGVpZ2h0ID0gSW5maW5pdHk7XG4gIH1cblxuICB2YXIgY3VycmVudExpbmVNYXhTaXplID0gMDtcbiAgZ3JvdXAuZWFjaENoaWxkKGZ1bmN0aW9uIChjaGlsZCwgaWR4KSB7XG4gICAgdmFyIHBvc2l0aW9uID0gY2hpbGQucG9zaXRpb247XG4gICAgdmFyIHJlY3QgPSBjaGlsZC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgbmV4dENoaWxkID0gZ3JvdXAuY2hpbGRBdChpZHggKyAxKTtcbiAgICB2YXIgbmV4dENoaWxkUmVjdCA9IG5leHRDaGlsZCAmJiBuZXh0Q2hpbGQuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgdmFyIG5leHRYO1xuICAgIHZhciBuZXh0WTtcblxuICAgIGlmIChvcmllbnQgPT09ICdob3Jpem9udGFsJykge1xuICAgICAgdmFyIG1vdmVYID0gcmVjdC53aWR0aCArIChuZXh0Q2hpbGRSZWN0ID8gLW5leHRDaGlsZFJlY3QueCArIHJlY3QueCA6IDApO1xuICAgICAgbmV4dFggPSB4ICsgbW92ZVg7IC8vIFdyYXAgd2hlbiB3aWR0aCBleGNlZWRzIG1heFdpZHRoIG9yIG1lZXQgYSBgbmV3bGluZWAgZ3JvdXBcbiAgICAgIC8vIEZJWE1FIGNvbXBhcmUgYmVmb3JlIGFkZGluZyBnYXA/XG5cbiAgICAgIGlmIChuZXh0WCA+IG1heFdpZHRoIHx8IGNoaWxkLm5ld2xpbmUpIHtcbiAgICAgICAgeCA9IDA7XG4gICAgICAgIG5leHRYID0gbW92ZVg7XG4gICAgICAgIHkgKz0gY3VycmVudExpbmVNYXhTaXplICsgZ2FwO1xuICAgICAgICBjdXJyZW50TGluZU1heFNpemUgPSByZWN0LmhlaWdodDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEZJWE1FOiBjb25zaWRlciByZWN0LnkgaXMgbm90IGAwYD9cbiAgICAgICAgY3VycmVudExpbmVNYXhTaXplID0gTWF0aC5tYXgoY3VycmVudExpbmVNYXhTaXplLCByZWN0LmhlaWdodCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBtb3ZlWSA9IHJlY3QuaGVpZ2h0ICsgKG5leHRDaGlsZFJlY3QgPyAtbmV4dENoaWxkUmVjdC55ICsgcmVjdC55IDogMCk7XG4gICAgICBuZXh0WSA9IHkgKyBtb3ZlWTsgLy8gV3JhcCB3aGVuIHdpZHRoIGV4Y2VlZHMgbWF4SGVpZ2h0IG9yIG1lZXQgYSBgbmV3bGluZWAgZ3JvdXBcblxuICAgICAgaWYgKG5leHRZID4gbWF4SGVpZ2h0IHx8IGNoaWxkLm5ld2xpbmUpIHtcbiAgICAgICAgeCArPSBjdXJyZW50TGluZU1heFNpemUgKyBnYXA7XG4gICAgICAgIHkgPSAwO1xuICAgICAgICBuZXh0WSA9IG1vdmVZO1xuICAgICAgICBjdXJyZW50TGluZU1heFNpemUgPSByZWN0LndpZHRoO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY3VycmVudExpbmVNYXhTaXplID0gTWF0aC5tYXgoY3VycmVudExpbmVNYXhTaXplLCByZWN0LndpZHRoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY2hpbGQubmV3bGluZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHBvc2l0aW9uWzBdID0geDtcbiAgICBwb3NpdGlvblsxXSA9IHk7XG4gICAgb3JpZW50ID09PSAnaG9yaXpvbnRhbCcgPyB4ID0gbmV4dFggKyBnYXAgOiB5ID0gbmV4dFkgKyBnYXA7XG4gIH0pO1xufVxuLyoqXG4gKiBWQm94IG9yIEhCb3ggbGF5b3V0aW5nXG4gKiBAcGFyYW0ge3N0cmluZ30gb3JpZW50XG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2NvbnRhaW5lci9Hcm91cH0gZ3JvdXBcbiAqIEBwYXJhbSB7bnVtYmVyfSBnYXBcbiAqIEBwYXJhbSB7bnVtYmVyfSBbd2lkdGg9SW5maW5pdHldXG4gKiBAcGFyYW0ge251bWJlcn0gW2hlaWdodD1JbmZpbml0eV1cbiAqL1xuXG5cbnZhciBib3ggPSBib3hMYXlvdXQ7XG4vKipcbiAqIFZCb3ggbGF5b3V0aW5nXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2NvbnRhaW5lci9Hcm91cH0gZ3JvdXBcbiAqIEBwYXJhbSB7bnVtYmVyfSBnYXBcbiAqIEBwYXJhbSB7bnVtYmVyfSBbd2lkdGg9SW5maW5pdHldXG4gKiBAcGFyYW0ge251bWJlcn0gW2hlaWdodD1JbmZpbml0eV1cbiAqL1xuXG52YXIgdmJveCA9IHpyVXRpbC5jdXJyeShib3hMYXlvdXQsICd2ZXJ0aWNhbCcpO1xuLyoqXG4gKiBIQm94IGxheW91dGluZ1xuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9IGdyb3VwXG4gKiBAcGFyYW0ge251bWJlcn0gZ2FwXG4gKiBAcGFyYW0ge251bWJlcn0gW3dpZHRoPUluZmluaXR5XVxuICogQHBhcmFtIHtudW1iZXJ9IFtoZWlnaHQ9SW5maW5pdHldXG4gKi9cblxudmFyIGhib3ggPSB6clV0aWwuY3VycnkoYm94TGF5b3V0LCAnaG9yaXpvbnRhbCcpO1xuLyoqXG4gKiBJZiB4IG9yIHgyIGlzIG5vdCBzcGVjaWZpZWQgb3IgJ2NlbnRlcicgJ2xlZnQnICdyaWdodCcsXG4gKiB0aGUgd2lkdGggd291bGQgYmUgYXMgbG9uZyBhcyBwb3NzaWJsZS5cbiAqIElmIHkgb3IgeTIgaXMgbm90IHNwZWNpZmllZCBvciAnbWlkZGxlJyAndG9wJyAnYm90dG9tJyxcbiAqIHRoZSBoZWlnaHQgd291bGQgYmUgYXMgbG9uZyBhcyBwb3NzaWJsZS5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcG9zaXRpb25JbmZvXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtwb3NpdGlvbkluZm8ueF1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby55XVxuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLngyXVxuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLnkyXVxuICogQHBhcmFtIHtPYmplY3R9IGNvbnRhaW5lclJlY3Qge3dpZHRoLCBoZWlnaHR9XG4gKiBAcGFyYW0ge3N0cmluZ3xudW1iZXJ9IG1hcmdpblxuICogQHJldHVybiB7T2JqZWN0fSB7d2lkdGgsIGhlaWdodH1cbiAqL1xuXG5mdW5jdGlvbiBnZXRBdmFpbGFibGVTaXplKHBvc2l0aW9uSW5mbywgY29udGFpbmVyUmVjdCwgbWFyZ2luKSB7XG4gIHZhciBjb250YWluZXJXaWR0aCA9IGNvbnRhaW5lclJlY3Qud2lkdGg7XG4gIHZhciBjb250YWluZXJIZWlnaHQgPSBjb250YWluZXJSZWN0LmhlaWdodDtcbiAgdmFyIHggPSBwYXJzZVBlcmNlbnQocG9zaXRpb25JbmZvLngsIGNvbnRhaW5lcldpZHRoKTtcbiAgdmFyIHkgPSBwYXJzZVBlcmNlbnQocG9zaXRpb25JbmZvLnksIGNvbnRhaW5lckhlaWdodCk7XG4gIHZhciB4MiA9IHBhcnNlUGVyY2VudChwb3NpdGlvbkluZm8ueDIsIGNvbnRhaW5lcldpZHRoKTtcbiAgdmFyIHkyID0gcGFyc2VQZXJjZW50KHBvc2l0aW9uSW5mby55MiwgY29udGFpbmVySGVpZ2h0KTtcbiAgKGlzTmFOKHgpIHx8IGlzTmFOKHBhcnNlRmxvYXQocG9zaXRpb25JbmZvLngpKSkgJiYgKHggPSAwKTtcbiAgKGlzTmFOKHgyKSB8fCBpc05hTihwYXJzZUZsb2F0KHBvc2l0aW9uSW5mby54MikpKSAmJiAoeDIgPSBjb250YWluZXJXaWR0aCk7XG4gIChpc05hTih5KSB8fCBpc05hTihwYXJzZUZsb2F0KHBvc2l0aW9uSW5mby55KSkpICYmICh5ID0gMCk7XG4gIChpc05hTih5MikgfHwgaXNOYU4ocGFyc2VGbG9hdChwb3NpdGlvbkluZm8ueTIpKSkgJiYgKHkyID0gY29udGFpbmVySGVpZ2h0KTtcbiAgbWFyZ2luID0gZm9ybWF0VXRpbC5ub3JtYWxpemVDc3NBcnJheShtYXJnaW4gfHwgMCk7XG4gIHJldHVybiB7XG4gICAgd2lkdGg6IE1hdGgubWF4KHgyIC0geCAtIG1hcmdpblsxXSAtIG1hcmdpblszXSwgMCksXG4gICAgaGVpZ2h0OiBNYXRoLm1heCh5MiAtIHkgLSBtYXJnaW5bMF0gLSBtYXJnaW5bMl0sIDApXG4gIH07XG59XG4vKipcbiAqIFBhcnNlIHBvc2l0aW9uIGluZm8uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBvc2l0aW9uSW5mb1xuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLmxlZnRdXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtwb3NpdGlvbkluZm8udG9wXVxuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLnJpZ2h0XVxuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLmJvdHRvbV1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby53aWR0aF1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby5oZWlnaHRdXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtwb3NpdGlvbkluZm8uYXNwZWN0XSBBc3BlY3QgaXMgd2lkdGggLyBoZWlnaHRcbiAqIEBwYXJhbSB7T2JqZWN0fSBjb250YWluZXJSZWN0XG4gKiBAcGFyYW0ge3N0cmluZ3xudW1iZXJ9IFttYXJnaW5dXG4gKlxuICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvY29yZS9Cb3VuZGluZ1JlY3R9XG4gKi9cblxuXG5mdW5jdGlvbiBnZXRMYXlvdXRSZWN0KHBvc2l0aW9uSW5mbywgY29udGFpbmVyUmVjdCwgbWFyZ2luKSB7XG4gIG1hcmdpbiA9IGZvcm1hdFV0aWwubm9ybWFsaXplQ3NzQXJyYXkobWFyZ2luIHx8IDApO1xuICB2YXIgY29udGFpbmVyV2lkdGggPSBjb250YWluZXJSZWN0LndpZHRoO1xuICB2YXIgY29udGFpbmVySGVpZ2h0ID0gY29udGFpbmVyUmVjdC5oZWlnaHQ7XG4gIHZhciBsZWZ0ID0gcGFyc2VQZXJjZW50KHBvc2l0aW9uSW5mby5sZWZ0LCBjb250YWluZXJXaWR0aCk7XG4gIHZhciB0b3AgPSBwYXJzZVBlcmNlbnQocG9zaXRpb25JbmZvLnRvcCwgY29udGFpbmVySGVpZ2h0KTtcbiAgdmFyIHJpZ2h0ID0gcGFyc2VQZXJjZW50KHBvc2l0aW9uSW5mby5yaWdodCwgY29udGFpbmVyV2lkdGgpO1xuICB2YXIgYm90dG9tID0gcGFyc2VQZXJjZW50KHBvc2l0aW9uSW5mby5ib3R0b20sIGNvbnRhaW5lckhlaWdodCk7XG4gIHZhciB3aWR0aCA9IHBhcnNlUGVyY2VudChwb3NpdGlvbkluZm8ud2lkdGgsIGNvbnRhaW5lcldpZHRoKTtcbiAgdmFyIGhlaWdodCA9IHBhcnNlUGVyY2VudChwb3NpdGlvbkluZm8uaGVpZ2h0LCBjb250YWluZXJIZWlnaHQpO1xuICB2YXIgdmVydGljYWxNYXJnaW4gPSBtYXJnaW5bMl0gKyBtYXJnaW5bMF07XG4gIHZhciBob3Jpem9udGFsTWFyZ2luID0gbWFyZ2luWzFdICsgbWFyZ2luWzNdO1xuICB2YXIgYXNwZWN0ID0gcG9zaXRpb25JbmZvLmFzcGVjdDsgLy8gSWYgd2lkdGggaXMgbm90IHNwZWNpZmllZCwgY2FsY3VsYXRlIHdpZHRoIGZyb20gbGVmdCBhbmQgcmlnaHRcblxuICBpZiAoaXNOYU4od2lkdGgpKSB7XG4gICAgd2lkdGggPSBjb250YWluZXJXaWR0aCAtIHJpZ2h0IC0gaG9yaXpvbnRhbE1hcmdpbiAtIGxlZnQ7XG4gIH1cblxuICBpZiAoaXNOYU4oaGVpZ2h0KSkge1xuICAgIGhlaWdodCA9IGNvbnRhaW5lckhlaWdodCAtIGJvdHRvbSAtIHZlcnRpY2FsTWFyZ2luIC0gdG9wO1xuICB9XG5cbiAgaWYgKGFzcGVjdCAhPSBudWxsKSB7XG4gICAgLy8gSWYgd2lkdGggYW5kIGhlaWdodCBhcmUgbm90IGdpdmVuXG4gICAgLy8gMS4gR3JhcGggc2hvdWxkIG5vdCBleGNlZWRzIHRoZSBjb250YWluZXJcbiAgICAvLyAyLiBBc3BlY3QgbXVzdCBiZSBrZWVwZWRcbiAgICAvLyAzLiBHcmFwaCBzaG91bGQgdGFrZSB0aGUgc3BhY2UgYXMgbW9yZSBhcyBwb3NzaWJsZVxuICAgIC8vIEZJWE1FXG4gICAgLy8gTWFyZ2luIGlzIG5vdCBjb25zaWRlcmVkLCBiZWNhdXNlIHRoZXJlIGlzIG5vIGNhc2UgdGhhdCBib3RoXG4gICAgLy8gdXNpbmcgbWFyZ2luIGFuZCBhc3BlY3Qgc28gZmFyLlxuICAgIGlmIChpc05hTih3aWR0aCkgJiYgaXNOYU4oaGVpZ2h0KSkge1xuICAgICAgaWYgKGFzcGVjdCA+IGNvbnRhaW5lcldpZHRoIC8gY29udGFpbmVySGVpZ2h0KSB7XG4gICAgICAgIHdpZHRoID0gY29udGFpbmVyV2lkdGggKiAwLjg7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBoZWlnaHQgPSBjb250YWluZXJIZWlnaHQgKiAwLjg7XG4gICAgICB9XG4gICAgfSAvLyBDYWxjdWxhdGUgd2lkdGggb3IgaGVpZ2h0IHdpdGggZ2l2ZW4gYXNwZWN0XG5cblxuICAgIGlmIChpc05hTih3aWR0aCkpIHtcbiAgICAgIHdpZHRoID0gYXNwZWN0ICogaGVpZ2h0O1xuICAgIH1cblxuICAgIGlmIChpc05hTihoZWlnaHQpKSB7XG4gICAgICBoZWlnaHQgPSB3aWR0aCAvIGFzcGVjdDtcbiAgICB9XG4gIH0gLy8gSWYgbGVmdCBpcyBub3Qgc3BlY2lmaWVkLCBjYWxjdWxhdGUgbGVmdCBmcm9tIHJpZ2h0IGFuZCB3aWR0aFxuXG5cbiAgaWYgKGlzTmFOKGxlZnQpKSB7XG4gICAgbGVmdCA9IGNvbnRhaW5lcldpZHRoIC0gcmlnaHQgLSB3aWR0aCAtIGhvcml6b250YWxNYXJnaW47XG4gIH1cblxuICBpZiAoaXNOYU4odG9wKSkge1xuICAgIHRvcCA9IGNvbnRhaW5lckhlaWdodCAtIGJvdHRvbSAtIGhlaWdodCAtIHZlcnRpY2FsTWFyZ2luO1xuICB9IC8vIEFsaWduIGxlZnQgYW5kIHRvcFxuXG5cbiAgc3dpdGNoIChwb3NpdGlvbkluZm8ubGVmdCB8fCBwb3NpdGlvbkluZm8ucmlnaHQpIHtcbiAgICBjYXNlICdjZW50ZXInOlxuICAgICAgbGVmdCA9IGNvbnRhaW5lcldpZHRoIC8gMiAtIHdpZHRoIC8gMiAtIG1hcmdpblszXTtcbiAgICAgIGJyZWFrO1xuXG4gICAgY2FzZSAncmlnaHQnOlxuICAgICAgbGVmdCA9IGNvbnRhaW5lcldpZHRoIC0gd2lkdGggLSBob3Jpem9udGFsTWFyZ2luO1xuICAgICAgYnJlYWs7XG4gIH1cblxuICBzd2l0Y2ggKHBvc2l0aW9uSW5mby50b3AgfHwgcG9zaXRpb25JbmZvLmJvdHRvbSkge1xuICAgIGNhc2UgJ21pZGRsZSc6XG4gICAgY2FzZSAnY2VudGVyJzpcbiAgICAgIHRvcCA9IGNvbnRhaW5lckhlaWdodCAvIDIgLSBoZWlnaHQgLyAyIC0gbWFyZ2luWzBdO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdib3R0b20nOlxuICAgICAgdG9wID0gY29udGFpbmVySGVpZ2h0IC0gaGVpZ2h0IC0gdmVydGljYWxNYXJnaW47XG4gICAgICBicmVhaztcbiAgfSAvLyBJZiBzb21ldGhpbmcgaXMgd3JvbmcgYW5kIGxlZnQsIHRvcCwgd2lkdGgsIGhlaWdodCBhcmUgY2FsY3VsYXRlZCBhcyBOYU5cblxuXG4gIGxlZnQgPSBsZWZ0IHx8IDA7XG4gIHRvcCA9IHRvcCB8fCAwO1xuXG4gIGlmIChpc05hTih3aWR0aCkpIHtcbiAgICAvLyBXaWR0aCBtYXkgYmUgTmFOIGlmIG9ubHkgb25lIHZhbHVlIGlzIGdpdmVuIGV4Y2VwdCB3aWR0aFxuICAgIHdpZHRoID0gY29udGFpbmVyV2lkdGggLSBob3Jpem9udGFsTWFyZ2luIC0gbGVmdCAtIChyaWdodCB8fCAwKTtcbiAgfVxuXG4gIGlmIChpc05hTihoZWlnaHQpKSB7XG4gICAgLy8gSGVpZ2h0IG1heSBiZSBOYU4gaWYgb25seSBvbmUgdmFsdWUgaXMgZ2l2ZW4gZXhjZXB0IGhlaWdodFxuICAgIGhlaWdodCA9IGNvbnRhaW5lckhlaWdodCAtIHZlcnRpY2FsTWFyZ2luIC0gdG9wIC0gKGJvdHRvbSB8fCAwKTtcbiAgfVxuXG4gIHZhciByZWN0ID0gbmV3IEJvdW5kaW5nUmVjdChsZWZ0ICsgbWFyZ2luWzNdLCB0b3AgKyBtYXJnaW5bMF0sIHdpZHRoLCBoZWlnaHQpO1xuICByZWN0Lm1hcmdpbiA9IG1hcmdpbjtcbiAgcmV0dXJuIHJlY3Q7XG59XG4vKipcbiAqIFBvc2l0aW9uIGEgenIgZWxlbWVudCBpbiB2aWV3cG9ydFxuICogIEdyb3VwIHBvc2l0aW9uIGlzIHNwZWNpZmllZCBieSBlaXRoZXJcbiAqICB7bGVmdCwgdG9wfSwge3JpZ2h0LCBib3R0b219XG4gKiAgSWYgYWxsIHByb3BlcnRpZXMgZXhpc3RzLCByaWdodCBhbmQgYm90dG9tIHdpbGwgYmUgaWdvbnJlZC5cbiAqXG4gKiBMb2dpYzpcbiAqICAgICAxLiBTY2FsZSAoYWdhaW5zdCBvcmlnaW4gcG9pbnQgaW4gcGFyZW50IGNvb3JkKVxuICogICAgIDIuIFJvdGF0ZSAoYWdhaW5zdCBvcmlnaW4gcG9pbnQgaW4gcGFyZW50IGNvb3JkKVxuICogICAgIDMuIFRyYXNsYXRlICh3aXRoIGVsLnBvc2l0aW9uIGJ5IHRoaXMgbWV0aG9kKVxuICogU28gdGhpcyBtZXRob2Qgb25seSBmaXhlcyB0aGUgbGFzdCBzdGVwICdUcmFzbGF0ZScsIHdoaWNoIGRvZXMgbm90IGFmZmVjdFxuICogc2NhbGluZyBhbmQgcm90YXRpbmcuXG4gKlxuICogSWYgYmUgY2FsbGVkIHJlcGVhdGx5IHdpdGggdGhlIHNhbWUgaW5wdXQgZWwsIHRoZSBzYW1lIHJlc3VsdCB3aWxsIGJlIGdvdHRlbi5cbiAqXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsIFNob3VsZCBoYXZlIGBnZXRCb3VuZGluZ1JlY3RgIG1ldGhvZC5cbiAqIEBwYXJhbSB7T2JqZWN0fSBwb3NpdGlvbkluZm9cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby5sZWZ0XVxuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLnRvcF1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby5yaWdodF1cbiAqIEBwYXJhbSB7bnVtYmVyfHN0cmluZ30gW3Bvc2l0aW9uSW5mby5ib3R0b21dXG4gKiBAcGFyYW0ge251bWJlcnxzdHJpbmd9IFtwb3NpdGlvbkluZm8ud2lkdGhdIE9ubHkgZm9yIG9wdC5ib3VuZGluZ01vZGVsOiAncmF3J1xuICogQHBhcmFtIHtudW1iZXJ8c3RyaW5nfSBbcG9zaXRpb25JbmZvLmhlaWdodF0gT25seSBmb3Igb3B0LmJvdW5kaW5nTW9kZWw6ICdyYXcnXG4gKiBAcGFyYW0ge09iamVjdH0gY29udGFpbmVyUmVjdFxuICogQHBhcmFtIHtzdHJpbmd8bnVtYmVyfSBtYXJnaW5cbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0XVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gW29wdC5odj1bMSwxXV0gT25seSBob3Jpem9udGFsIG9yIG9ubHkgdmVydGljYWwuXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBbb3B0LmJvdW5kaW5nTW9kZT0nYWxsJ11cbiAqICAgICAgICBTcGVjaWZ5IGhvdyB0byBjYWxjdWxhdGUgYm91bmRpbmdSZWN0IHdoZW4gbG9jYXRpbmcuXG4gKiAgICAgICAgJ2FsbCc6IFBvc2l0aW9uIHRoZSBib3VuZGluZ1JlY3QgdGhhdCBpcyB0cmFuc2Zvcm1lZCBhbmQgdWlvbmVkXG4gKiAgICAgICAgICAgICAgIGJvdGggaXRzZWxmIGFuZCBpdHMgZGVzY2VuZGFudHMuXG4gKiAgICAgICAgICAgICAgIFRoaXMgbW9kZSBzaW1wbGllcyBjb25maW5lIHRoZSBlbGVtZW50cyBpbiB0aGUgYm91bmRpbmdcbiAqICAgICAgICAgICAgICAgb2YgdGhlaXIgY29udGFpbmVyIChlLmcuLCB1c2luZyAncmlnaHQ6IDAnKS5cbiAqICAgICAgICAncmF3JzogUG9zaXRpb24gdGhlIGJvdW5kaW5nUmVjdCB0aGF0IGlzIG5vdCB0cmFuc2Zvcm1lZCBhbmQgb25seSBpdHNlbGYuXG4gKiAgICAgICAgICAgICAgIFRoaXMgbW9kZSBpcyB1c2VmdWwgd2hlbiB5b3Ugd2FudCBhIGVsZW1lbnQgY2FuIG92ZXJmbG93IGl0c1xuICogICAgICAgICAgICAgICBjb250YWluZXIuIChDb25zaWRlciBhIHJvdGF0ZWQgY2lyY2xlIG5lZWRzIHRvIGJlIGxvY2F0ZWQgaW4gYSBjb3JuZXIuKVxuICogICAgICAgICAgICAgICBJbiB0aGlzIG1vZGUgcG9zaXRpb25JbmZvLndpZHRoL2hlaWdodCBjYW4gb25seSBiZSBudW1iZXIuXG4gKi9cblxuXG5mdW5jdGlvbiBwb3NpdGlvbkVsZW1lbnQoZWwsIHBvc2l0aW9uSW5mbywgY29udGFpbmVyUmVjdCwgbWFyZ2luLCBvcHQpIHtcbiAgdmFyIGggPSAhb3B0IHx8ICFvcHQuaHYgfHwgb3B0Lmh2WzBdO1xuICB2YXIgdiA9ICFvcHQgfHwgIW9wdC5odiB8fCBvcHQuaHZbMV07XG4gIHZhciBib3VuZGluZ01vZGUgPSBvcHQgJiYgb3B0LmJvdW5kaW5nTW9kZSB8fCAnYWxsJztcblxuICBpZiAoIWggJiYgIXYpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgcmVjdDtcblxuICBpZiAoYm91bmRpbmdNb2RlID09PSAncmF3Jykge1xuICAgIHJlY3QgPSBlbC50eXBlID09PSAnZ3JvdXAnID8gbmV3IEJvdW5kaW5nUmVjdCgwLCAwLCArcG9zaXRpb25JbmZvLndpZHRoIHx8IDAsICtwb3NpdGlvbkluZm8uaGVpZ2h0IHx8IDApIDogZWwuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gIH0gZWxzZSB7XG4gICAgcmVjdCA9IGVsLmdldEJvdW5kaW5nUmVjdCgpO1xuXG4gICAgaWYgKGVsLm5lZWRMb2NhbFRyYW5zZm9ybSgpKSB7XG4gICAgICB2YXIgdHJhbnNmb3JtID0gZWwuZ2V0TG9jYWxUcmFuc2Zvcm0oKTsgLy8gTm90aWNlOiByYXcgcmVjdCBtYXkgYmUgaW5uZXIgb2JqZWN0IG9mIGVsLFxuICAgICAgLy8gd2hpY2ggc2hvdWxkIG5vdCBiZSBtb2RpZmllZC5cblxuICAgICAgcmVjdCA9IHJlY3QuY2xvbmUoKTtcbiAgICAgIHJlY3QuYXBwbHlUcmFuc2Zvcm0odHJhbnNmb3JtKTtcbiAgICB9XG4gIH0gLy8gVGhlIHJlYWwgd2lkdGggYW5kIGhlaWdodCBjYW4gbm90IGJlIHNwZWNpZmllZCBidXQgY2FsY3VsYXRlZCBieSB0aGUgZ2l2ZW4gZWwuXG5cblxuICBwb3NpdGlvbkluZm8gPSBnZXRMYXlvdXRSZWN0KHpyVXRpbC5kZWZhdWx0cyh7XG4gICAgd2lkdGg6IHJlY3Qud2lkdGgsXG4gICAgaGVpZ2h0OiByZWN0LmhlaWdodFxuICB9LCBwb3NpdGlvbkluZm8pLCBjb250YWluZXJSZWN0LCBtYXJnaW4pOyAvLyBCZWNhdXNlICd0cmFubGF0ZScgaXMgdGhlIGxhc3Qgc3RlcCBpbiB0cmFuc2Zvcm1cbiAgLy8gKHNlZSB6cmVuZGVyL2NvcmUvVHJhbnNmb3JtYWJsZSNnZXRMb2NhbFRyYW5zZm9ybSksXG4gIC8vIHdlIGNhbiBqdXN0IG9ubHkgbW9kaWZ5IGVsLnBvc2l0aW9uIHRvIGdldCBmaW5hbCByZXN1bHQuXG5cbiAgdmFyIGVsUG9zID0gZWwucG9zaXRpb247XG4gIHZhciBkeCA9IGggPyBwb3NpdGlvbkluZm8ueCAtIHJlY3QueCA6IDA7XG4gIHZhciBkeSA9IHYgPyBwb3NpdGlvbkluZm8ueSAtIHJlY3QueSA6IDA7XG4gIGVsLmF0dHIoJ3Bvc2l0aW9uJywgYm91bmRpbmdNb2RlID09PSAncmF3JyA/IFtkeCwgZHldIDogW2VsUG9zWzBdICsgZHgsIGVsUG9zWzFdICsgZHldKTtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbiBDb250YWlucyBzb21lIG9mIHRoZSBwcm9wZXJ0aWVzIGluIEhWX05BTUVTLlxuICogQHBhcmFtIHtudW1iZXJ9IGh2SWR4IDA6IGhvcml6b250YWw7IDE6IHZlcnRpY2FsLlxuICovXG5cblxuZnVuY3Rpb24gc2l6ZUNhbGN1bGFibGUob3B0aW9uLCBodklkeCkge1xuICByZXR1cm4gb3B0aW9uW0hWX05BTUVTW2h2SWR4XVswXV0gIT0gbnVsbCB8fCBvcHRpb25bSFZfTkFNRVNbaHZJZHhdWzFdXSAhPSBudWxsICYmIG9wdGlvbltIVl9OQU1FU1todklkeF1bMl1dICE9IG51bGw7XG59XG4vKipcbiAqIENvbnNpZGVyIENhc2U6XG4gKiBXaGVuIGRlZnVsYXQgb3B0aW9uIGhhcyB7bGVmdDogMCwgd2lkdGg6IDEwMH0sIGFuZCB3ZSBzZXQge3JpZ2h0OiAwfVxuICogdGhyb3VnaCBzZXRPcHRpb24gb3IgbWVkaWEgcXVlcnksIHVzaW5nIG5vcm1hbCB6clV0aWwubWVyZ2Ugd2lsbCBjYXVzZVxuICoge3JpZ2h0OiAwfSBkb2VzIG5vdCB0YWtlIGVmZmVjdC5cbiAqXG4gKiBAZXhhbXBsZVxuICogQ29tcG9uZW50TW9kZWwuZXh0ZW5kKHtcbiAqICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gKiAgICAgICAgIC4uLlxuICogICAgICAgICB2YXIgaW5wdXRQb3NpdGlvblBhcmFtcyA9IGxheW91dC5nZXRMYXlvdXRQYXJhbXMob3B0aW9uKTtcbiAqICAgICAgICAgdGhpcy5tZXJnZU9wdGlvbihpbnB1dFBvc2l0aW9uUGFyYW1zKTtcbiAqICAgICB9LFxuICogICAgIG1lcmdlT3B0aW9uOiBmdW5jdGlvbiAobmV3T3B0aW9uKSB7XG4gKiAgICAgICAgIG5ld09wdGlvbiAmJiB6clV0aWwubWVyZ2UodGhpc09wdGlvbiwgbmV3T3B0aW9uLCB0cnVlKTtcbiAqICAgICAgICAgbGF5b3V0Lm1lcmdlTGF5b3V0UGFyYW0odGhpc09wdGlvbiwgbmV3T3B0aW9uKTtcbiAqICAgICB9XG4gKiB9KTtcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gdGFyZ2V0T3B0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gbmV3T3B0aW9uXG4gKiBAcGFyYW0ge09iamVjdHxzdHJpbmd9IFtvcHRdXG4gKiBAcGFyYW0ge2Jvb2xlYW58QXJyYXkuPGJvb2xlYW4+fSBbb3B0Lmlnbm9yZVNpemU9ZmFsc2VdIFVzZWQgZm9yIHRoZSBjb21wb25lbnRzXG4gKiAgdGhhdCB3aWR0aCAob3IgaGVpZ2h0KSBzaG91bGQgbm90IGJlIGNhbGN1bGF0ZWQgYnkgbGVmdCBhbmQgcmlnaHQgKG9yIHRvcCBhbmQgYm90dG9tKS5cbiAqL1xuXG5cbmZ1bmN0aW9uIG1lcmdlTGF5b3V0UGFyYW0odGFyZ2V0T3B0aW9uLCBuZXdPcHRpb24sIG9wdCkge1xuICAhenJVdGlsLmlzT2JqZWN0KG9wdCkgJiYgKG9wdCA9IHt9KTtcbiAgdmFyIGlnbm9yZVNpemUgPSBvcHQuaWdub3JlU2l6ZTtcbiAgIXpyVXRpbC5pc0FycmF5KGlnbm9yZVNpemUpICYmIChpZ25vcmVTaXplID0gW2lnbm9yZVNpemUsIGlnbm9yZVNpemVdKTtcbiAgdmFyIGhSZXN1bHQgPSBtZXJnZShIVl9OQU1FU1swXSwgMCk7XG4gIHZhciB2UmVzdWx0ID0gbWVyZ2UoSFZfTkFNRVNbMV0sIDEpO1xuICBjb3B5KEhWX05BTUVTWzBdLCB0YXJnZXRPcHRpb24sIGhSZXN1bHQpO1xuICBjb3B5KEhWX05BTUVTWzFdLCB0YXJnZXRPcHRpb24sIHZSZXN1bHQpO1xuXG4gIGZ1bmN0aW9uIG1lcmdlKG5hbWVzLCBodklkeCkge1xuICAgIHZhciBuZXdQYXJhbXMgPSB7fTtcbiAgICB2YXIgbmV3VmFsdWVDb3VudCA9IDA7XG4gICAgdmFyIG1lcmdlZCA9IHt9O1xuICAgIHZhciBtZXJnZWRWYWx1ZUNvdW50ID0gMDtcbiAgICB2YXIgZW5vdWdoUGFyYW1OdW1iZXIgPSAyO1xuICAgIGVhY2gobmFtZXMsIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICBtZXJnZWRbbmFtZV0gPSB0YXJnZXRPcHRpb25bbmFtZV07XG4gICAgfSk7XG4gICAgZWFjaChuYW1lcywgZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgIC8vIENvbnNpZGVyIGNhc2U6IG5ld09wdGlvbi53aWR0aCBpcyBudWxsLCB3aGljaCBpc1xuICAgICAgLy8gc2V0IGJ5IHVzZXIgZm9yIHJlbW92aW5nIHdpZHRoIHNldHRpbmcuXG4gICAgICBoYXNQcm9wKG5ld09wdGlvbiwgbmFtZSkgJiYgKG5ld1BhcmFtc1tuYW1lXSA9IG1lcmdlZFtuYW1lXSA9IG5ld09wdGlvbltuYW1lXSk7XG4gICAgICBoYXNWYWx1ZShuZXdQYXJhbXMsIG5hbWUpICYmIG5ld1ZhbHVlQ291bnQrKztcbiAgICAgIGhhc1ZhbHVlKG1lcmdlZCwgbmFtZSkgJiYgbWVyZ2VkVmFsdWVDb3VudCsrO1xuICAgIH0pO1xuXG4gICAgaWYgKGlnbm9yZVNpemVbaHZJZHhdKSB7XG4gICAgICAvLyBPbmx5IG9uZSBvZiBsZWZ0L3JpZ2h0IGlzIHByZW1pdHRlZCB0byBleGlzdC5cbiAgICAgIGlmIChoYXNWYWx1ZShuZXdPcHRpb24sIG5hbWVzWzFdKSkge1xuICAgICAgICBtZXJnZWRbbmFtZXNbMl1dID0gbnVsbDtcbiAgICAgIH0gZWxzZSBpZiAoaGFzVmFsdWUobmV3T3B0aW9uLCBuYW1lc1syXSkpIHtcbiAgICAgICAgbWVyZ2VkW25hbWVzWzFdXSA9IG51bGw7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBtZXJnZWQ7XG4gICAgfSAvLyBDYXNlOiBuZXdPcHRpb246IHt3aWR0aDogLi4uLCByaWdodDogLi4ufSxcbiAgICAvLyBvciB0YXJnZXRPcHRpb246IHtyaWdodDogLi4ufSBhbmQgbmV3T3B0aW9uOiB7d2lkdGg6IC4uLn0sXG4gICAgLy8gVGhlcmUgaXMgbm8gY29uZmxpY3Qgd2hlbiBtZXJnZWQgb25seSBoYXMgcGFyYW1zIGNvdW50XG4gICAgLy8gbGl0dGxlIHRoYW4gZW5vdWdoUGFyYW1OdW1iZXIuXG5cblxuICAgIGlmIChtZXJnZWRWYWx1ZUNvdW50ID09PSBlbm91Z2hQYXJhbU51bWJlciB8fCAhbmV3VmFsdWVDb3VudCkge1xuICAgICAgcmV0dXJuIG1lcmdlZDtcbiAgICB9IC8vIENhc2U6IG5ld09wdGlvbjoge3dpZHRoOiAuLi4sIHJpZ2h0OiAuLi59LFxuICAgIC8vIFRoYW4gd2UgY2FuIG1ha2Ugc3VyZSB1c2VyIG9ubHkgd2FudCB0aG9zZSB0d28sIGFuZCBpZ25vcmVcbiAgICAvLyBhbGwgb3JpZ2luIHBhcmFtcyBpbiB0YXJnZXRPcHRpb24uXG4gICAgZWxzZSBpZiAobmV3VmFsdWVDb3VudCA+PSBlbm91Z2hQYXJhbU51bWJlcikge1xuICAgICAgICByZXR1cm4gbmV3UGFyYW1zO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gQ2hvc2UgYW5vdGhlciBwYXJhbSBmcm9tIHRhcmdldE9wdGlvbiBieSBwcmlvcml0eS5cbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBuYW1lcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgIHZhciBuYW1lID0gbmFtZXNbaV07XG5cbiAgICAgICAgICBpZiAoIWhhc1Byb3AobmV3UGFyYW1zLCBuYW1lKSAmJiBoYXNQcm9wKHRhcmdldE9wdGlvbiwgbmFtZSkpIHtcbiAgICAgICAgICAgIG5ld1BhcmFtc1tuYW1lXSA9IHRhcmdldE9wdGlvbltuYW1lXTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBuZXdQYXJhbXM7XG4gICAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBoYXNQcm9wKG9iaiwgbmFtZSkge1xuICAgIHJldHVybiBvYmouaGFzT3duUHJvcGVydHkobmFtZSk7XG4gIH1cblxuICBmdW5jdGlvbiBoYXNWYWx1ZShvYmosIG5hbWUpIHtcbiAgICByZXR1cm4gb2JqW25hbWVdICE9IG51bGwgJiYgb2JqW25hbWVdICE9PSAnYXV0byc7XG4gIH1cblxuICBmdW5jdGlvbiBjb3B5KG5hbWVzLCB0YXJnZXQsIHNvdXJjZSkge1xuICAgIGVhY2gobmFtZXMsIGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICB0YXJnZXRbbmFtZV0gPSBzb3VyY2VbbmFtZV07XG4gICAgfSk7XG4gIH1cbn1cbi8qKlxuICogUmV0cmlldmUgJ2xlZnQnLCAncmlnaHQnLCAndG9wJywgJ2JvdHRvbScsICd3aWR0aCcsICdoZWlnaHQnIGZyb20gb2JqZWN0LlxuICogQHBhcmFtIHtPYmplY3R9IHNvdXJjZVxuICogQHJldHVybiB7T2JqZWN0fSBSZXN1bHQgY29udGFpbnMgdGhvc2UgcHJvcHMuXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRMYXlvdXRQYXJhbXMoc291cmNlKSB7XG4gIHJldHVybiBjb3B5TGF5b3V0UGFyYW1zKHt9LCBzb3VyY2UpO1xufVxuLyoqXG4gKiBSZXRyaWV2ZSAnbGVmdCcsICdyaWdodCcsICd0b3AnLCAnYm90dG9tJywgJ3dpZHRoJywgJ2hlaWdodCcgZnJvbSBvYmplY3QuXG4gKiBAcGFyYW0ge09iamVjdH0gc291cmNlXG4gKiBAcmV0dXJuIHtPYmplY3R9IFJlc3VsdCBjb250YWlucyB0aG9zZSBwcm9wcy5cbiAqL1xuXG5cbmZ1bmN0aW9uIGNvcHlMYXlvdXRQYXJhbXModGFyZ2V0LCBzb3VyY2UpIHtcbiAgc291cmNlICYmIHRhcmdldCAmJiBlYWNoKExPQ0FUSU9OX1BBUkFNUywgZnVuY3Rpb24gKG5hbWUpIHtcbiAgICBzb3VyY2UuaGFzT3duUHJvcGVydHkobmFtZSkgJiYgKHRhcmdldFtuYW1lXSA9IHNvdXJjZVtuYW1lXSk7XG4gIH0pO1xuICByZXR1cm4gdGFyZ2V0O1xufVxuXG5leHBvcnRzLkxPQ0FUSU9OX1BBUkFNUyA9IExPQ0FUSU9OX1BBUkFNUztcbmV4cG9ydHMuSFZfTkFNRVMgPSBIVl9OQU1FUztcbmV4cG9ydHMuYm94ID0gYm94O1xuZXhwb3J0cy52Ym94ID0gdmJveDtcbmV4cG9ydHMuaGJveCA9IGhib3g7XG5leHBvcnRzLmdldEF2YWlsYWJsZVNpemUgPSBnZXRBdmFpbGFibGVTaXplO1xuZXhwb3J0cy5nZXRMYXlvdXRSZWN0ID0gZ2V0TGF5b3V0UmVjdDtcbmV4cG9ydHMucG9zaXRpb25FbGVtZW50ID0gcG9zaXRpb25FbGVtZW50O1xuZXhwb3J0cy5zaXplQ2FsY3VsYWJsZSA9IHNpemVDYWxjdWxhYmxlO1xuZXhwb3J0cy5tZXJnZUxheW91dFBhcmFtID0gbWVyZ2VMYXlvdXRQYXJhbTtcbmV4cG9ydHMuZ2V0TGF5b3V0UGFyYW1zID0gZ2V0TGF5b3V0UGFyYW1zO1xuZXhwb3J0cy5jb3B5TGF5b3V0UGFyYW1zID0gY29weUxheW91dFBhcmFtczsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFEQTtBQUNBOzs7O0FBSUE7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBVUE7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBQ0E7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/layout.js
