/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var modelUtil = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var each = zrUtil.each;
var isObject = zrUtil.isObject;
var POSSIBLE_STYLES = ['areaStyle', 'lineStyle', 'nodeStyle', 'linkStyle', 'chordStyle', 'label', 'labelLine'];

function compatEC2ItemStyle(opt) {
  var itemStyleOpt = opt && opt.itemStyle;

  if (!itemStyleOpt) {
    return;
  }

  for (var i = 0, len = POSSIBLE_STYLES.length; i < len; i++) {
    var styleName = POSSIBLE_STYLES[i];
    var normalItemStyleOpt = itemStyleOpt.normal;
    var emphasisItemStyleOpt = itemStyleOpt.emphasis;

    if (normalItemStyleOpt && normalItemStyleOpt[styleName]) {
      opt[styleName] = opt[styleName] || {};

      if (!opt[styleName].normal) {
        opt[styleName].normal = normalItemStyleOpt[styleName];
      } else {
        zrUtil.merge(opt[styleName].normal, normalItemStyleOpt[styleName]);
      }

      normalItemStyleOpt[styleName] = null;
    }

    if (emphasisItemStyleOpt && emphasisItemStyleOpt[styleName]) {
      opt[styleName] = opt[styleName] || {};

      if (!opt[styleName].emphasis) {
        opt[styleName].emphasis = emphasisItemStyleOpt[styleName];
      } else {
        zrUtil.merge(opt[styleName].emphasis, emphasisItemStyleOpt[styleName]);
      }

      emphasisItemStyleOpt[styleName] = null;
    }
  }
}

function convertNormalEmphasis(opt, optType, useExtend) {
  if (opt && opt[optType] && (opt[optType].normal || opt[optType].emphasis)) {
    var normalOpt = opt[optType].normal;
    var emphasisOpt = opt[optType].emphasis;

    if (normalOpt) {
      // Timeline controlStyle has other properties besides normal and emphasis
      if (useExtend) {
        opt[optType].normal = opt[optType].emphasis = null;
        zrUtil.defaults(opt[optType], normalOpt);
      } else {
        opt[optType] = normalOpt;
      }
    }

    if (emphasisOpt) {
      opt.emphasis = opt.emphasis || {};
      opt.emphasis[optType] = emphasisOpt;
    }
  }
}

function removeEC3NormalStatus(opt) {
  convertNormalEmphasis(opt, 'itemStyle');
  convertNormalEmphasis(opt, 'lineStyle');
  convertNormalEmphasis(opt, 'areaStyle');
  convertNormalEmphasis(opt, 'label');
  convertNormalEmphasis(opt, 'labelLine'); // treemap

  convertNormalEmphasis(opt, 'upperLabel'); // graph

  convertNormalEmphasis(opt, 'edgeLabel');
}

function compatTextStyle(opt, propName) {
  // Check whether is not object (string\null\undefined ...)
  var labelOptSingle = isObject(opt) && opt[propName];
  var textStyle = isObject(labelOptSingle) && labelOptSingle.textStyle;

  if (textStyle) {
    for (var i = 0, len = modelUtil.TEXT_STYLE_OPTIONS.length; i < len; i++) {
      var propName = modelUtil.TEXT_STYLE_OPTIONS[i];

      if (textStyle.hasOwnProperty(propName)) {
        labelOptSingle[propName] = textStyle[propName];
      }
    }
  }
}

function compatEC3CommonStyles(opt) {
  if (opt) {
    removeEC3NormalStatus(opt);
    compatTextStyle(opt, 'label');
    opt.emphasis && compatTextStyle(opt.emphasis, 'label');
  }
}

function processSeries(seriesOpt) {
  if (!isObject(seriesOpt)) {
    return;
  }

  compatEC2ItemStyle(seriesOpt);
  removeEC3NormalStatus(seriesOpt);
  compatTextStyle(seriesOpt, 'label'); // treemap

  compatTextStyle(seriesOpt, 'upperLabel'); // graph

  compatTextStyle(seriesOpt, 'edgeLabel');

  if (seriesOpt.emphasis) {
    compatTextStyle(seriesOpt.emphasis, 'label'); // treemap

    compatTextStyle(seriesOpt.emphasis, 'upperLabel'); // graph

    compatTextStyle(seriesOpt.emphasis, 'edgeLabel');
  }

  var markPoint = seriesOpt.markPoint;

  if (markPoint) {
    compatEC2ItemStyle(markPoint);
    compatEC3CommonStyles(markPoint);
  }

  var markLine = seriesOpt.markLine;

  if (markLine) {
    compatEC2ItemStyle(markLine);
    compatEC3CommonStyles(markLine);
  }

  var markArea = seriesOpt.markArea;

  if (markArea) {
    compatEC3CommonStyles(markArea);
  }

  var data = seriesOpt.data; // Break with ec3: if `setOption` again, there may be no `type` in option,
  // then the backward compat based on option type will not be performed.

  if (seriesOpt.type === 'graph') {
    data = data || seriesOpt.nodes;
    var edgeData = seriesOpt.links || seriesOpt.edges;

    if (edgeData && !zrUtil.isTypedArray(edgeData)) {
      for (var i = 0; i < edgeData.length; i++) {
        compatEC3CommonStyles(edgeData[i]);
      }
    }

    zrUtil.each(seriesOpt.categories, function (opt) {
      removeEC3NormalStatus(opt);
    });
  }

  if (data && !zrUtil.isTypedArray(data)) {
    for (var i = 0; i < data.length; i++) {
      compatEC3CommonStyles(data[i]);
    }
  } // mark point data


  var markPoint = seriesOpt.markPoint;

  if (markPoint && markPoint.data) {
    var mpData = markPoint.data;

    for (var i = 0; i < mpData.length; i++) {
      compatEC3CommonStyles(mpData[i]);
    }
  } // mark line data


  var markLine = seriesOpt.markLine;

  if (markLine && markLine.data) {
    var mlData = markLine.data;

    for (var i = 0; i < mlData.length; i++) {
      if (zrUtil.isArray(mlData[i])) {
        compatEC3CommonStyles(mlData[i][0]);
        compatEC3CommonStyles(mlData[i][1]);
      } else {
        compatEC3CommonStyles(mlData[i]);
      }
    }
  } // Series


  if (seriesOpt.type === 'gauge') {
    compatTextStyle(seriesOpt, 'axisLabel');
    compatTextStyle(seriesOpt, 'title');
    compatTextStyle(seriesOpt, 'detail');
  } else if (seriesOpt.type === 'treemap') {
    convertNormalEmphasis(seriesOpt.breadcrumb, 'itemStyle');
    zrUtil.each(seriesOpt.levels, function (opt) {
      removeEC3NormalStatus(opt);
    });
  } else if (seriesOpt.type === 'tree') {
    removeEC3NormalStatus(seriesOpt.leaves);
  } // sunburst starts from ec4, so it does not need to compat levels.

}

function toArr(o) {
  return zrUtil.isArray(o) ? o : o ? [o] : [];
}

function toObj(o) {
  return (zrUtil.isArray(o) ? o[0] : o) || {};
}

function _default(option, isTheme) {
  each(toArr(option.series), function (seriesOpt) {
    isObject(seriesOpt) && processSeries(seriesOpt);
  });
  var axes = ['xAxis', 'yAxis', 'radiusAxis', 'angleAxis', 'singleAxis', 'parallelAxis', 'radar'];
  isTheme && axes.push('valueAxis', 'categoryAxis', 'logAxis', 'timeAxis');
  each(axes, function (axisName) {
    each(toArr(option[axisName]), function (axisOpt) {
      if (axisOpt) {
        compatTextStyle(axisOpt, 'axisLabel');
        compatTextStyle(axisOpt.axisPointer, 'label');
      }
    });
  });
  each(toArr(option.parallel), function (parallelOpt) {
    var parallelAxisDefault = parallelOpt && parallelOpt.parallelAxisDefault;
    compatTextStyle(parallelAxisDefault, 'axisLabel');
    compatTextStyle(parallelAxisDefault && parallelAxisDefault.axisPointer, 'label');
  });
  each(toArr(option.calendar), function (calendarOpt) {
    convertNormalEmphasis(calendarOpt, 'itemStyle');
    compatTextStyle(calendarOpt, 'dayLabel');
    compatTextStyle(calendarOpt, 'monthLabel');
    compatTextStyle(calendarOpt, 'yearLabel');
  }); // radar.name.textStyle

  each(toArr(option.radar), function (radarOpt) {
    compatTextStyle(radarOpt, 'name');
  });
  each(toArr(option.geo), function (geoOpt) {
    if (isObject(geoOpt)) {
      compatEC3CommonStyles(geoOpt);
      each(toArr(geoOpt.regions), function (regionObj) {
        compatEC3CommonStyles(regionObj);
      });
    }
  });
  each(toArr(option.timeline), function (timelineOpt) {
    compatEC3CommonStyles(timelineOpt);
    convertNormalEmphasis(timelineOpt, 'label');
    convertNormalEmphasis(timelineOpt, 'itemStyle');
    convertNormalEmphasis(timelineOpt, 'controlStyle', true);
    var data = timelineOpt.data;
    zrUtil.isArray(data) && zrUtil.each(data, function (item) {
      if (zrUtil.isObject(item)) {
        convertNormalEmphasis(item, 'label');
        convertNormalEmphasis(item, 'itemStyle');
      }
    });
  });
  each(toArr(option.toolbox), function (toolboxOpt) {
    convertNormalEmphasis(toolboxOpt, 'iconStyle');
    each(toolboxOpt.feature, function (featureOpt) {
      convertNormalEmphasis(featureOpt, 'iconStyle');
    });
  });
  compatTextStyle(toObj(option.axisPointer), 'label');
  compatTextStyle(toObj(option.tooltip).axisPointer, 'label');
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvcHJlcHJvY2Vzc29yL2hlbHBlci9jb21wYXRTdHlsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3ByZXByb2Nlc3Nvci9oZWxwZXIvY29tcGF0U3R5bGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgbW9kZWxVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbW9kZWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBlYWNoID0genJVdGlsLmVhY2g7XG52YXIgaXNPYmplY3QgPSB6clV0aWwuaXNPYmplY3Q7XG52YXIgUE9TU0lCTEVfU1RZTEVTID0gWydhcmVhU3R5bGUnLCAnbGluZVN0eWxlJywgJ25vZGVTdHlsZScsICdsaW5rU3R5bGUnLCAnY2hvcmRTdHlsZScsICdsYWJlbCcsICdsYWJlbExpbmUnXTtcblxuZnVuY3Rpb24gY29tcGF0RUMySXRlbVN0eWxlKG9wdCkge1xuICB2YXIgaXRlbVN0eWxlT3B0ID0gb3B0ICYmIG9wdC5pdGVtU3R5bGU7XG5cbiAgaWYgKCFpdGVtU3R5bGVPcHQpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBmb3IgKHZhciBpID0gMCwgbGVuID0gUE9TU0lCTEVfU1RZTEVTLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgdmFyIHN0eWxlTmFtZSA9IFBPU1NJQkxFX1NUWUxFU1tpXTtcbiAgICB2YXIgbm9ybWFsSXRlbVN0eWxlT3B0ID0gaXRlbVN0eWxlT3B0Lm5vcm1hbDtcbiAgICB2YXIgZW1waGFzaXNJdGVtU3R5bGVPcHQgPSBpdGVtU3R5bGVPcHQuZW1waGFzaXM7XG5cbiAgICBpZiAobm9ybWFsSXRlbVN0eWxlT3B0ICYmIG5vcm1hbEl0ZW1TdHlsZU9wdFtzdHlsZU5hbWVdKSB7XG4gICAgICBvcHRbc3R5bGVOYW1lXSA9IG9wdFtzdHlsZU5hbWVdIHx8IHt9O1xuXG4gICAgICBpZiAoIW9wdFtzdHlsZU5hbWVdLm5vcm1hbCkge1xuICAgICAgICBvcHRbc3R5bGVOYW1lXS5ub3JtYWwgPSBub3JtYWxJdGVtU3R5bGVPcHRbc3R5bGVOYW1lXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHpyVXRpbC5tZXJnZShvcHRbc3R5bGVOYW1lXS5ub3JtYWwsIG5vcm1hbEl0ZW1TdHlsZU9wdFtzdHlsZU5hbWVdKTtcbiAgICAgIH1cblxuICAgICAgbm9ybWFsSXRlbVN0eWxlT3B0W3N0eWxlTmFtZV0gPSBudWxsO1xuICAgIH1cblxuICAgIGlmIChlbXBoYXNpc0l0ZW1TdHlsZU9wdCAmJiBlbXBoYXNpc0l0ZW1TdHlsZU9wdFtzdHlsZU5hbWVdKSB7XG4gICAgICBvcHRbc3R5bGVOYW1lXSA9IG9wdFtzdHlsZU5hbWVdIHx8IHt9O1xuXG4gICAgICBpZiAoIW9wdFtzdHlsZU5hbWVdLmVtcGhhc2lzKSB7XG4gICAgICAgIG9wdFtzdHlsZU5hbWVdLmVtcGhhc2lzID0gZW1waGFzaXNJdGVtU3R5bGVPcHRbc3R5bGVOYW1lXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHpyVXRpbC5tZXJnZShvcHRbc3R5bGVOYW1lXS5lbXBoYXNpcywgZW1waGFzaXNJdGVtU3R5bGVPcHRbc3R5bGVOYW1lXSk7XG4gICAgICB9XG5cbiAgICAgIGVtcGhhc2lzSXRlbVN0eWxlT3B0W3N0eWxlTmFtZV0gPSBudWxsO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjb252ZXJ0Tm9ybWFsRW1waGFzaXMob3B0LCBvcHRUeXBlLCB1c2VFeHRlbmQpIHtcbiAgaWYgKG9wdCAmJiBvcHRbb3B0VHlwZV0gJiYgKG9wdFtvcHRUeXBlXS5ub3JtYWwgfHwgb3B0W29wdFR5cGVdLmVtcGhhc2lzKSkge1xuICAgIHZhciBub3JtYWxPcHQgPSBvcHRbb3B0VHlwZV0ubm9ybWFsO1xuICAgIHZhciBlbXBoYXNpc09wdCA9IG9wdFtvcHRUeXBlXS5lbXBoYXNpcztcblxuICAgIGlmIChub3JtYWxPcHQpIHtcbiAgICAgIC8vIFRpbWVsaW5lIGNvbnRyb2xTdHlsZSBoYXMgb3RoZXIgcHJvcGVydGllcyBiZXNpZGVzIG5vcm1hbCBhbmQgZW1waGFzaXNcbiAgICAgIGlmICh1c2VFeHRlbmQpIHtcbiAgICAgICAgb3B0W29wdFR5cGVdLm5vcm1hbCA9IG9wdFtvcHRUeXBlXS5lbXBoYXNpcyA9IG51bGw7XG4gICAgICAgIHpyVXRpbC5kZWZhdWx0cyhvcHRbb3B0VHlwZV0sIG5vcm1hbE9wdCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvcHRbb3B0VHlwZV0gPSBub3JtYWxPcHQ7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGVtcGhhc2lzT3B0KSB7XG4gICAgICBvcHQuZW1waGFzaXMgPSBvcHQuZW1waGFzaXMgfHwge307XG4gICAgICBvcHQuZW1waGFzaXNbb3B0VHlwZV0gPSBlbXBoYXNpc09wdDtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gcmVtb3ZlRUMzTm9ybWFsU3RhdHVzKG9wdCkge1xuICBjb252ZXJ0Tm9ybWFsRW1waGFzaXMob3B0LCAnaXRlbVN0eWxlJyk7XG4gIGNvbnZlcnROb3JtYWxFbXBoYXNpcyhvcHQsICdsaW5lU3R5bGUnKTtcbiAgY29udmVydE5vcm1hbEVtcGhhc2lzKG9wdCwgJ2FyZWFTdHlsZScpO1xuICBjb252ZXJ0Tm9ybWFsRW1waGFzaXMob3B0LCAnbGFiZWwnKTtcbiAgY29udmVydE5vcm1hbEVtcGhhc2lzKG9wdCwgJ2xhYmVsTGluZScpOyAvLyB0cmVlbWFwXG5cbiAgY29udmVydE5vcm1hbEVtcGhhc2lzKG9wdCwgJ3VwcGVyTGFiZWwnKTsgLy8gZ3JhcGhcblxuICBjb252ZXJ0Tm9ybWFsRW1waGFzaXMob3B0LCAnZWRnZUxhYmVsJyk7XG59XG5cbmZ1bmN0aW9uIGNvbXBhdFRleHRTdHlsZShvcHQsIHByb3BOYW1lKSB7XG4gIC8vIENoZWNrIHdoZXRoZXIgaXMgbm90IG9iamVjdCAoc3RyaW5nXFxudWxsXFx1bmRlZmluZWQgLi4uKVxuICB2YXIgbGFiZWxPcHRTaW5nbGUgPSBpc09iamVjdChvcHQpICYmIG9wdFtwcm9wTmFtZV07XG4gIHZhciB0ZXh0U3R5bGUgPSBpc09iamVjdChsYWJlbE9wdFNpbmdsZSkgJiYgbGFiZWxPcHRTaW5nbGUudGV4dFN0eWxlO1xuXG4gIGlmICh0ZXh0U3R5bGUpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gbW9kZWxVdGlsLlRFWFRfU1RZTEVfT1BUSU9OUy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgdmFyIHByb3BOYW1lID0gbW9kZWxVdGlsLlRFWFRfU1RZTEVfT1BUSU9OU1tpXTtcblxuICAgICAgaWYgKHRleHRTdHlsZS5oYXNPd25Qcm9wZXJ0eShwcm9wTmFtZSkpIHtcbiAgICAgICAgbGFiZWxPcHRTaW5nbGVbcHJvcE5hbWVdID0gdGV4dFN0eWxlW3Byb3BOYW1lXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gY29tcGF0RUMzQ29tbW9uU3R5bGVzKG9wdCkge1xuICBpZiAob3B0KSB7XG4gICAgcmVtb3ZlRUMzTm9ybWFsU3RhdHVzKG9wdCk7XG4gICAgY29tcGF0VGV4dFN0eWxlKG9wdCwgJ2xhYmVsJyk7XG4gICAgb3B0LmVtcGhhc2lzICYmIGNvbXBhdFRleHRTdHlsZShvcHQuZW1waGFzaXMsICdsYWJlbCcpO1xuICB9XG59XG5cbmZ1bmN0aW9uIHByb2Nlc3NTZXJpZXMoc2VyaWVzT3B0KSB7XG4gIGlmICghaXNPYmplY3Qoc2VyaWVzT3B0KSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGNvbXBhdEVDMkl0ZW1TdHlsZShzZXJpZXNPcHQpO1xuICByZW1vdmVFQzNOb3JtYWxTdGF0dXMoc2VyaWVzT3B0KTtcbiAgY29tcGF0VGV4dFN0eWxlKHNlcmllc09wdCwgJ2xhYmVsJyk7IC8vIHRyZWVtYXBcblxuICBjb21wYXRUZXh0U3R5bGUoc2VyaWVzT3B0LCAndXBwZXJMYWJlbCcpOyAvLyBncmFwaFxuXG4gIGNvbXBhdFRleHRTdHlsZShzZXJpZXNPcHQsICdlZGdlTGFiZWwnKTtcblxuICBpZiAoc2VyaWVzT3B0LmVtcGhhc2lzKSB7XG4gICAgY29tcGF0VGV4dFN0eWxlKHNlcmllc09wdC5lbXBoYXNpcywgJ2xhYmVsJyk7IC8vIHRyZWVtYXBcblxuICAgIGNvbXBhdFRleHRTdHlsZShzZXJpZXNPcHQuZW1waGFzaXMsICd1cHBlckxhYmVsJyk7IC8vIGdyYXBoXG5cbiAgICBjb21wYXRUZXh0U3R5bGUoc2VyaWVzT3B0LmVtcGhhc2lzLCAnZWRnZUxhYmVsJyk7XG4gIH1cblxuICB2YXIgbWFya1BvaW50ID0gc2VyaWVzT3B0Lm1hcmtQb2ludDtcblxuICBpZiAobWFya1BvaW50KSB7XG4gICAgY29tcGF0RUMySXRlbVN0eWxlKG1hcmtQb2ludCk7XG4gICAgY29tcGF0RUMzQ29tbW9uU3R5bGVzKG1hcmtQb2ludCk7XG4gIH1cblxuICB2YXIgbWFya0xpbmUgPSBzZXJpZXNPcHQubWFya0xpbmU7XG5cbiAgaWYgKG1hcmtMaW5lKSB7XG4gICAgY29tcGF0RUMySXRlbVN0eWxlKG1hcmtMaW5lKTtcbiAgICBjb21wYXRFQzNDb21tb25TdHlsZXMobWFya0xpbmUpO1xuICB9XG5cbiAgdmFyIG1hcmtBcmVhID0gc2VyaWVzT3B0Lm1hcmtBcmVhO1xuXG4gIGlmIChtYXJrQXJlYSkge1xuICAgIGNvbXBhdEVDM0NvbW1vblN0eWxlcyhtYXJrQXJlYSk7XG4gIH1cblxuICB2YXIgZGF0YSA9IHNlcmllc09wdC5kYXRhOyAvLyBCcmVhayB3aXRoIGVjMzogaWYgYHNldE9wdGlvbmAgYWdhaW4sIHRoZXJlIG1heSBiZSBubyBgdHlwZWAgaW4gb3B0aW9uLFxuICAvLyB0aGVuIHRoZSBiYWNrd2FyZCBjb21wYXQgYmFzZWQgb24gb3B0aW9uIHR5cGUgd2lsbCBub3QgYmUgcGVyZm9ybWVkLlxuXG4gIGlmIChzZXJpZXNPcHQudHlwZSA9PT0gJ2dyYXBoJykge1xuICAgIGRhdGEgPSBkYXRhIHx8IHNlcmllc09wdC5ub2RlcztcbiAgICB2YXIgZWRnZURhdGEgPSBzZXJpZXNPcHQubGlua3MgfHwgc2VyaWVzT3B0LmVkZ2VzO1xuXG4gICAgaWYgKGVkZ2VEYXRhICYmICF6clV0aWwuaXNUeXBlZEFycmF5KGVkZ2VEYXRhKSkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBlZGdlRGF0YS5sZW5ndGg7IGkrKykge1xuICAgICAgICBjb21wYXRFQzNDb21tb25TdHlsZXMoZWRnZURhdGFbaV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIHpyVXRpbC5lYWNoKHNlcmllc09wdC5jYXRlZ29yaWVzLCBmdW5jdGlvbiAob3B0KSB7XG4gICAgICByZW1vdmVFQzNOb3JtYWxTdGF0dXMob3B0KTtcbiAgICB9KTtcbiAgfVxuXG4gIGlmIChkYXRhICYmICF6clV0aWwuaXNUeXBlZEFycmF5KGRhdGEpKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjb21wYXRFQzNDb21tb25TdHlsZXMoZGF0YVtpXSk7XG4gICAgfVxuICB9IC8vIG1hcmsgcG9pbnQgZGF0YVxuXG5cbiAgdmFyIG1hcmtQb2ludCA9IHNlcmllc09wdC5tYXJrUG9pbnQ7XG5cbiAgaWYgKG1hcmtQb2ludCAmJiBtYXJrUG9pbnQuZGF0YSkge1xuICAgIHZhciBtcERhdGEgPSBtYXJrUG9pbnQuZGF0YTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbXBEYXRhLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjb21wYXRFQzNDb21tb25TdHlsZXMobXBEYXRhW2ldKTtcbiAgICB9XG4gIH0gLy8gbWFyayBsaW5lIGRhdGFcblxuXG4gIHZhciBtYXJrTGluZSA9IHNlcmllc09wdC5tYXJrTGluZTtcblxuICBpZiAobWFya0xpbmUgJiYgbWFya0xpbmUuZGF0YSkge1xuICAgIHZhciBtbERhdGEgPSBtYXJrTGluZS5kYXRhO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBtbERhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmICh6clV0aWwuaXNBcnJheShtbERhdGFbaV0pKSB7XG4gICAgICAgIGNvbXBhdEVDM0NvbW1vblN0eWxlcyhtbERhdGFbaV1bMF0pO1xuICAgICAgICBjb21wYXRFQzNDb21tb25TdHlsZXMobWxEYXRhW2ldWzFdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbXBhdEVDM0NvbW1vblN0eWxlcyhtbERhdGFbaV0pO1xuICAgICAgfVxuICAgIH1cbiAgfSAvLyBTZXJpZXNcblxuXG4gIGlmIChzZXJpZXNPcHQudHlwZSA9PT0gJ2dhdWdlJykge1xuICAgIGNvbXBhdFRleHRTdHlsZShzZXJpZXNPcHQsICdheGlzTGFiZWwnKTtcbiAgICBjb21wYXRUZXh0U3R5bGUoc2VyaWVzT3B0LCAndGl0bGUnKTtcbiAgICBjb21wYXRUZXh0U3R5bGUoc2VyaWVzT3B0LCAnZGV0YWlsJyk7XG4gIH0gZWxzZSBpZiAoc2VyaWVzT3B0LnR5cGUgPT09ICd0cmVlbWFwJykge1xuICAgIGNvbnZlcnROb3JtYWxFbXBoYXNpcyhzZXJpZXNPcHQuYnJlYWRjcnVtYiwgJ2l0ZW1TdHlsZScpO1xuICAgIHpyVXRpbC5lYWNoKHNlcmllc09wdC5sZXZlbHMsIGZ1bmN0aW9uIChvcHQpIHtcbiAgICAgIHJlbW92ZUVDM05vcm1hbFN0YXR1cyhvcHQpO1xuICAgIH0pO1xuICB9IGVsc2UgaWYgKHNlcmllc09wdC50eXBlID09PSAndHJlZScpIHtcbiAgICByZW1vdmVFQzNOb3JtYWxTdGF0dXMoc2VyaWVzT3B0LmxlYXZlcyk7XG4gIH0gLy8gc3VuYnVyc3Qgc3RhcnRzIGZyb20gZWM0LCBzbyBpdCBkb2VzIG5vdCBuZWVkIHRvIGNvbXBhdCBsZXZlbHMuXG5cbn1cblxuZnVuY3Rpb24gdG9BcnIobykge1xuICByZXR1cm4genJVdGlsLmlzQXJyYXkobykgPyBvIDogbyA/IFtvXSA6IFtdO1xufVxuXG5mdW5jdGlvbiB0b09iaihvKSB7XG4gIHJldHVybiAoenJVdGlsLmlzQXJyYXkobykgPyBvWzBdIDogbykgfHwge307XG59XG5cbmZ1bmN0aW9uIF9kZWZhdWx0KG9wdGlvbiwgaXNUaGVtZSkge1xuICBlYWNoKHRvQXJyKG9wdGlvbi5zZXJpZXMpLCBmdW5jdGlvbiAoc2VyaWVzT3B0KSB7XG4gICAgaXNPYmplY3Qoc2VyaWVzT3B0KSAmJiBwcm9jZXNzU2VyaWVzKHNlcmllc09wdCk7XG4gIH0pO1xuICB2YXIgYXhlcyA9IFsneEF4aXMnLCAneUF4aXMnLCAncmFkaXVzQXhpcycsICdhbmdsZUF4aXMnLCAnc2luZ2xlQXhpcycsICdwYXJhbGxlbEF4aXMnLCAncmFkYXInXTtcbiAgaXNUaGVtZSAmJiBheGVzLnB1c2goJ3ZhbHVlQXhpcycsICdjYXRlZ29yeUF4aXMnLCAnbG9nQXhpcycsICd0aW1lQXhpcycpO1xuICBlYWNoKGF4ZXMsIGZ1bmN0aW9uIChheGlzTmFtZSkge1xuICAgIGVhY2godG9BcnIob3B0aW9uW2F4aXNOYW1lXSksIGZ1bmN0aW9uIChheGlzT3B0KSB7XG4gICAgICBpZiAoYXhpc09wdCkge1xuICAgICAgICBjb21wYXRUZXh0U3R5bGUoYXhpc09wdCwgJ2F4aXNMYWJlbCcpO1xuICAgICAgICBjb21wYXRUZXh0U3R5bGUoYXhpc09wdC5heGlzUG9pbnRlciwgJ2xhYmVsJyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuICBlYWNoKHRvQXJyKG9wdGlvbi5wYXJhbGxlbCksIGZ1bmN0aW9uIChwYXJhbGxlbE9wdCkge1xuICAgIHZhciBwYXJhbGxlbEF4aXNEZWZhdWx0ID0gcGFyYWxsZWxPcHQgJiYgcGFyYWxsZWxPcHQucGFyYWxsZWxBeGlzRGVmYXVsdDtcbiAgICBjb21wYXRUZXh0U3R5bGUocGFyYWxsZWxBeGlzRGVmYXVsdCwgJ2F4aXNMYWJlbCcpO1xuICAgIGNvbXBhdFRleHRTdHlsZShwYXJhbGxlbEF4aXNEZWZhdWx0ICYmIHBhcmFsbGVsQXhpc0RlZmF1bHQuYXhpc1BvaW50ZXIsICdsYWJlbCcpO1xuICB9KTtcbiAgZWFjaCh0b0FycihvcHRpb24uY2FsZW5kYXIpLCBmdW5jdGlvbiAoY2FsZW5kYXJPcHQpIHtcbiAgICBjb252ZXJ0Tm9ybWFsRW1waGFzaXMoY2FsZW5kYXJPcHQsICdpdGVtU3R5bGUnKTtcbiAgICBjb21wYXRUZXh0U3R5bGUoY2FsZW5kYXJPcHQsICdkYXlMYWJlbCcpO1xuICAgIGNvbXBhdFRleHRTdHlsZShjYWxlbmRhck9wdCwgJ21vbnRoTGFiZWwnKTtcbiAgICBjb21wYXRUZXh0U3R5bGUoY2FsZW5kYXJPcHQsICd5ZWFyTGFiZWwnKTtcbiAgfSk7IC8vIHJhZGFyLm5hbWUudGV4dFN0eWxlXG5cbiAgZWFjaCh0b0FycihvcHRpb24ucmFkYXIpLCBmdW5jdGlvbiAocmFkYXJPcHQpIHtcbiAgICBjb21wYXRUZXh0U3R5bGUocmFkYXJPcHQsICduYW1lJyk7XG4gIH0pO1xuICBlYWNoKHRvQXJyKG9wdGlvbi5nZW8pLCBmdW5jdGlvbiAoZ2VvT3B0KSB7XG4gICAgaWYgKGlzT2JqZWN0KGdlb09wdCkpIHtcbiAgICAgIGNvbXBhdEVDM0NvbW1vblN0eWxlcyhnZW9PcHQpO1xuICAgICAgZWFjaCh0b0FycihnZW9PcHQucmVnaW9ucyksIGZ1bmN0aW9uIChyZWdpb25PYmopIHtcbiAgICAgICAgY29tcGF0RUMzQ29tbW9uU3R5bGVzKHJlZ2lvbk9iaik7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuICBlYWNoKHRvQXJyKG9wdGlvbi50aW1lbGluZSksIGZ1bmN0aW9uICh0aW1lbGluZU9wdCkge1xuICAgIGNvbXBhdEVDM0NvbW1vblN0eWxlcyh0aW1lbGluZU9wdCk7XG4gICAgY29udmVydE5vcm1hbEVtcGhhc2lzKHRpbWVsaW5lT3B0LCAnbGFiZWwnKTtcbiAgICBjb252ZXJ0Tm9ybWFsRW1waGFzaXModGltZWxpbmVPcHQsICdpdGVtU3R5bGUnKTtcbiAgICBjb252ZXJ0Tm9ybWFsRW1waGFzaXModGltZWxpbmVPcHQsICdjb250cm9sU3R5bGUnLCB0cnVlKTtcbiAgICB2YXIgZGF0YSA9IHRpbWVsaW5lT3B0LmRhdGE7XG4gICAgenJVdGlsLmlzQXJyYXkoZGF0YSkgJiYgenJVdGlsLmVhY2goZGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgIGlmICh6clV0aWwuaXNPYmplY3QoaXRlbSkpIHtcbiAgICAgICAgY29udmVydE5vcm1hbEVtcGhhc2lzKGl0ZW0sICdsYWJlbCcpO1xuICAgICAgICBjb252ZXJ0Tm9ybWFsRW1waGFzaXMoaXRlbSwgJ2l0ZW1TdHlsZScpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbiAgZWFjaCh0b0FycihvcHRpb24udG9vbGJveCksIGZ1bmN0aW9uICh0b29sYm94T3B0KSB7XG4gICAgY29udmVydE5vcm1hbEVtcGhhc2lzKHRvb2xib3hPcHQsICdpY29uU3R5bGUnKTtcbiAgICBlYWNoKHRvb2xib3hPcHQuZmVhdHVyZSwgZnVuY3Rpb24gKGZlYXR1cmVPcHQpIHtcbiAgICAgIGNvbnZlcnROb3JtYWxFbXBoYXNpcyhmZWF0dXJlT3B0LCAnaWNvblN0eWxlJyk7XG4gICAgfSk7XG4gIH0pO1xuICBjb21wYXRUZXh0U3R5bGUodG9PYmoob3B0aW9uLmF4aXNQb2ludGVyKSwgJ2xhYmVsJyk7XG4gIGNvbXBhdFRleHRTdHlsZSh0b09iaihvcHRpb24udG9vbHRpcCkuYXhpc1BvaW50ZXIsICdsYWJlbCcpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/preprocessor/helper/compatStyle.js
