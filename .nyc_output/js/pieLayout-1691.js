/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _number = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var parsePercent = _number.parsePercent;
var linearMap = _number.linearMap;

var labelLayout = __webpack_require__(/*! ./labelLayout */ "./node_modules/echarts/lib/chart/pie/labelLayout.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var PI2 = Math.PI * 2;
var RADIAN = Math.PI / 180;

function _default(seriesType, ecModel, api, payload) {
  ecModel.eachSeriesByType(seriesType, function (seriesModel) {
    var data = seriesModel.getData();
    var valueDim = data.mapDimension('value');
    var center = seriesModel.get('center');
    var radius = seriesModel.get('radius');

    if (!zrUtil.isArray(radius)) {
      radius = [0, radius];
    }

    if (!zrUtil.isArray(center)) {
      center = [center, center];
    }

    var width = api.getWidth();
    var height = api.getHeight();
    var size = Math.min(width, height);
    var cx = parsePercent(center[0], width);
    var cy = parsePercent(center[1], height);
    var r0 = parsePercent(radius[0], size / 2);
    var r = parsePercent(radius[1], size / 2);
    var startAngle = -seriesModel.get('startAngle') * RADIAN;
    var minAngle = seriesModel.get('minAngle') * RADIAN;
    var validDataCount = 0;
    data.each(valueDim, function (value) {
      !isNaN(value) && validDataCount++;
    });
    var sum = data.getSum(valueDim); // Sum may be 0

    var unitRadian = Math.PI / (sum || validDataCount) * 2;
    var clockwise = seriesModel.get('clockwise');
    var roseType = seriesModel.get('roseType');
    var stillShowZeroSum = seriesModel.get('stillShowZeroSum'); // [0...max]

    var extent = data.getDataExtent(valueDim);
    extent[0] = 0; // In the case some sector angle is smaller than minAngle

    var restAngle = PI2;
    var valueSumLargerThanMinAngle = 0;
    var currentAngle = startAngle;
    var dir = clockwise ? 1 : -1;
    data.each(valueDim, function (value, idx) {
      var angle;

      if (isNaN(value)) {
        data.setItemLayout(idx, {
          angle: NaN,
          startAngle: NaN,
          endAngle: NaN,
          clockwise: clockwise,
          cx: cx,
          cy: cy,
          r0: r0,
          r: roseType ? NaN : r
        });
        return;
      } // FIXME 兼容 2.0 但是 roseType 是 area 的时候才是这样？


      if (roseType !== 'area') {
        angle = sum === 0 && stillShowZeroSum ? unitRadian : value * unitRadian;
      } else {
        angle = PI2 / validDataCount;
      }

      if (angle < minAngle) {
        angle = minAngle;
        restAngle -= minAngle;
      } else {
        valueSumLargerThanMinAngle += value;
      }

      var endAngle = currentAngle + dir * angle;
      data.setItemLayout(idx, {
        angle: angle,
        startAngle: currentAngle,
        endAngle: endAngle,
        clockwise: clockwise,
        cx: cx,
        cy: cy,
        r0: r0,
        r: roseType ? linearMap(value, extent, [r0, r]) : r
      });
      currentAngle = endAngle;
    }); // Some sector is constrained by minAngle
    // Rest sectors needs recalculate angle

    if (restAngle < PI2 && validDataCount) {
      // Average the angle if rest angle is not enough after all angles is
      // Constrained by minAngle
      if (restAngle <= 1e-3) {
        var angle = PI2 / validDataCount;
        data.each(valueDim, function (value, idx) {
          if (!isNaN(value)) {
            var layout = data.getItemLayout(idx);
            layout.angle = angle;
            layout.startAngle = startAngle + dir * idx * angle;
            layout.endAngle = startAngle + dir * (idx + 1) * angle;
          }
        });
      } else {
        unitRadian = restAngle / valueSumLargerThanMinAngle;
        currentAngle = startAngle;
        data.each(valueDim, function (value, idx) {
          if (!isNaN(value)) {
            var layout = data.getItemLayout(idx);
            var angle = layout.angle === minAngle ? minAngle : value * unitRadian;
            layout.startAngle = currentAngle;
            layout.endAngle = currentAngle + dir * angle;
            currentAngle += dir * angle;
          }
        });
      }
    }

    labelLayout(seriesModel, r, width, height);
  });
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvcGllL3BpZUxheW91dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2NoYXJ0L3BpZS9waWVMYXlvdXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgcGFyc2VQZXJjZW50ID0gX251bWJlci5wYXJzZVBlcmNlbnQ7XG52YXIgbGluZWFyTWFwID0gX251bWJlci5saW5lYXJNYXA7XG5cbnZhciBsYWJlbExheW91dCA9IHJlcXVpcmUoXCIuL2xhYmVsTGF5b3V0XCIpO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIFBJMiA9IE1hdGguUEkgKiAyO1xudmFyIFJBRElBTiA9IE1hdGguUEkgLyAxODA7XG5cbmZ1bmN0aW9uIF9kZWZhdWx0KHNlcmllc1R5cGUsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkge1xuICBlY01vZGVsLmVhY2hTZXJpZXNCeVR5cGUoc2VyaWVzVHlwZSwgZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gICAgdmFyIHZhbHVlRGltID0gZGF0YS5tYXBEaW1lbnNpb24oJ3ZhbHVlJyk7XG4gICAgdmFyIGNlbnRlciA9IHNlcmllc01vZGVsLmdldCgnY2VudGVyJyk7XG4gICAgdmFyIHJhZGl1cyA9IHNlcmllc01vZGVsLmdldCgncmFkaXVzJyk7XG5cbiAgICBpZiAoIXpyVXRpbC5pc0FycmF5KHJhZGl1cykpIHtcbiAgICAgIHJhZGl1cyA9IFswLCByYWRpdXNdO1xuICAgIH1cblxuICAgIGlmICghenJVdGlsLmlzQXJyYXkoY2VudGVyKSkge1xuICAgICAgY2VudGVyID0gW2NlbnRlciwgY2VudGVyXTtcbiAgICB9XG5cbiAgICB2YXIgd2lkdGggPSBhcGkuZ2V0V2lkdGgoKTtcbiAgICB2YXIgaGVpZ2h0ID0gYXBpLmdldEhlaWdodCgpO1xuICAgIHZhciBzaXplID0gTWF0aC5taW4od2lkdGgsIGhlaWdodCk7XG4gICAgdmFyIGN4ID0gcGFyc2VQZXJjZW50KGNlbnRlclswXSwgd2lkdGgpO1xuICAgIHZhciBjeSA9IHBhcnNlUGVyY2VudChjZW50ZXJbMV0sIGhlaWdodCk7XG4gICAgdmFyIHIwID0gcGFyc2VQZXJjZW50KHJhZGl1c1swXSwgc2l6ZSAvIDIpO1xuICAgIHZhciByID0gcGFyc2VQZXJjZW50KHJhZGl1c1sxXSwgc2l6ZSAvIDIpO1xuICAgIHZhciBzdGFydEFuZ2xlID0gLXNlcmllc01vZGVsLmdldCgnc3RhcnRBbmdsZScpICogUkFESUFOO1xuICAgIHZhciBtaW5BbmdsZSA9IHNlcmllc01vZGVsLmdldCgnbWluQW5nbGUnKSAqIFJBRElBTjtcbiAgICB2YXIgdmFsaWREYXRhQ291bnQgPSAwO1xuICAgIGRhdGEuZWFjaCh2YWx1ZURpbSwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAhaXNOYU4odmFsdWUpICYmIHZhbGlkRGF0YUNvdW50Kys7XG4gICAgfSk7XG4gICAgdmFyIHN1bSA9IGRhdGEuZ2V0U3VtKHZhbHVlRGltKTsgLy8gU3VtIG1heSBiZSAwXG5cbiAgICB2YXIgdW5pdFJhZGlhbiA9IE1hdGguUEkgLyAoc3VtIHx8IHZhbGlkRGF0YUNvdW50KSAqIDI7XG4gICAgdmFyIGNsb2Nrd2lzZSA9IHNlcmllc01vZGVsLmdldCgnY2xvY2t3aXNlJyk7XG4gICAgdmFyIHJvc2VUeXBlID0gc2VyaWVzTW9kZWwuZ2V0KCdyb3NlVHlwZScpO1xuICAgIHZhciBzdGlsbFNob3daZXJvU3VtID0gc2VyaWVzTW9kZWwuZ2V0KCdzdGlsbFNob3daZXJvU3VtJyk7IC8vIFswLi4ubWF4XVxuXG4gICAgdmFyIGV4dGVudCA9IGRhdGEuZ2V0RGF0YUV4dGVudCh2YWx1ZURpbSk7XG4gICAgZXh0ZW50WzBdID0gMDsgLy8gSW4gdGhlIGNhc2Ugc29tZSBzZWN0b3IgYW5nbGUgaXMgc21hbGxlciB0aGFuIG1pbkFuZ2xlXG5cbiAgICB2YXIgcmVzdEFuZ2xlID0gUEkyO1xuICAgIHZhciB2YWx1ZVN1bUxhcmdlclRoYW5NaW5BbmdsZSA9IDA7XG4gICAgdmFyIGN1cnJlbnRBbmdsZSA9IHN0YXJ0QW5nbGU7XG4gICAgdmFyIGRpciA9IGNsb2Nrd2lzZSA/IDEgOiAtMTtcbiAgICBkYXRhLmVhY2godmFsdWVEaW0sIGZ1bmN0aW9uICh2YWx1ZSwgaWR4KSB7XG4gICAgICB2YXIgYW5nbGU7XG5cbiAgICAgIGlmIChpc05hTih2YWx1ZSkpIHtcbiAgICAgICAgZGF0YS5zZXRJdGVtTGF5b3V0KGlkeCwge1xuICAgICAgICAgIGFuZ2xlOiBOYU4sXG4gICAgICAgICAgc3RhcnRBbmdsZTogTmFOLFxuICAgICAgICAgIGVuZEFuZ2xlOiBOYU4sXG4gICAgICAgICAgY2xvY2t3aXNlOiBjbG9ja3dpc2UsXG4gICAgICAgICAgY3g6IGN4LFxuICAgICAgICAgIGN5OiBjeSxcbiAgICAgICAgICByMDogcjAsXG4gICAgICAgICAgcjogcm9zZVR5cGUgPyBOYU4gOiByXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9IC8vIEZJWE1FIOWFvOWuuSAyLjAg5L2G5pivIHJvc2VUeXBlIOaYryBhcmVhIOeahOaXtuWAmeaJjeaYr+i/meagt++8n1xuXG5cbiAgICAgIGlmIChyb3NlVHlwZSAhPT0gJ2FyZWEnKSB7XG4gICAgICAgIGFuZ2xlID0gc3VtID09PSAwICYmIHN0aWxsU2hvd1plcm9TdW0gPyB1bml0UmFkaWFuIDogdmFsdWUgKiB1bml0UmFkaWFuO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYW5nbGUgPSBQSTIgLyB2YWxpZERhdGFDb3VudDtcbiAgICAgIH1cblxuICAgICAgaWYgKGFuZ2xlIDwgbWluQW5nbGUpIHtcbiAgICAgICAgYW5nbGUgPSBtaW5BbmdsZTtcbiAgICAgICAgcmVzdEFuZ2xlIC09IG1pbkFuZ2xlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFsdWVTdW1MYXJnZXJUaGFuTWluQW5nbGUgKz0gdmFsdWU7XG4gICAgICB9XG5cbiAgICAgIHZhciBlbmRBbmdsZSA9IGN1cnJlbnRBbmdsZSArIGRpciAqIGFuZ2xlO1xuICAgICAgZGF0YS5zZXRJdGVtTGF5b3V0KGlkeCwge1xuICAgICAgICBhbmdsZTogYW5nbGUsXG4gICAgICAgIHN0YXJ0QW5nbGU6IGN1cnJlbnRBbmdsZSxcbiAgICAgICAgZW5kQW5nbGU6IGVuZEFuZ2xlLFxuICAgICAgICBjbG9ja3dpc2U6IGNsb2Nrd2lzZSxcbiAgICAgICAgY3g6IGN4LFxuICAgICAgICBjeTogY3ksXG4gICAgICAgIHIwOiByMCxcbiAgICAgICAgcjogcm9zZVR5cGUgPyBsaW5lYXJNYXAodmFsdWUsIGV4dGVudCwgW3IwLCByXSkgOiByXG4gICAgICB9KTtcbiAgICAgIGN1cnJlbnRBbmdsZSA9IGVuZEFuZ2xlO1xuICAgIH0pOyAvLyBTb21lIHNlY3RvciBpcyBjb25zdHJhaW5lZCBieSBtaW5BbmdsZVxuICAgIC8vIFJlc3Qgc2VjdG9ycyBuZWVkcyByZWNhbGN1bGF0ZSBhbmdsZVxuXG4gICAgaWYgKHJlc3RBbmdsZSA8IFBJMiAmJiB2YWxpZERhdGFDb3VudCkge1xuICAgICAgLy8gQXZlcmFnZSB0aGUgYW5nbGUgaWYgcmVzdCBhbmdsZSBpcyBub3QgZW5vdWdoIGFmdGVyIGFsbCBhbmdsZXMgaXNcbiAgICAgIC8vIENvbnN0cmFpbmVkIGJ5IG1pbkFuZ2xlXG4gICAgICBpZiAocmVzdEFuZ2xlIDw9IDFlLTMpIHtcbiAgICAgICAgdmFyIGFuZ2xlID0gUEkyIC8gdmFsaWREYXRhQ291bnQ7XG4gICAgICAgIGRhdGEuZWFjaCh2YWx1ZURpbSwgZnVuY3Rpb24gKHZhbHVlLCBpZHgpIHtcbiAgICAgICAgICBpZiAoIWlzTmFOKHZhbHVlKSkge1xuICAgICAgICAgICAgdmFyIGxheW91dCA9IGRhdGEuZ2V0SXRlbUxheW91dChpZHgpO1xuICAgICAgICAgICAgbGF5b3V0LmFuZ2xlID0gYW5nbGU7XG4gICAgICAgICAgICBsYXlvdXQuc3RhcnRBbmdsZSA9IHN0YXJ0QW5nbGUgKyBkaXIgKiBpZHggKiBhbmdsZTtcbiAgICAgICAgICAgIGxheW91dC5lbmRBbmdsZSA9IHN0YXJ0QW5nbGUgKyBkaXIgKiAoaWR4ICsgMSkgKiBhbmdsZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdW5pdFJhZGlhbiA9IHJlc3RBbmdsZSAvIHZhbHVlU3VtTGFyZ2VyVGhhbk1pbkFuZ2xlO1xuICAgICAgICBjdXJyZW50QW5nbGUgPSBzdGFydEFuZ2xlO1xuICAgICAgICBkYXRhLmVhY2godmFsdWVEaW0sIGZ1bmN0aW9uICh2YWx1ZSwgaWR4KSB7XG4gICAgICAgICAgaWYgKCFpc05hTih2YWx1ZSkpIHtcbiAgICAgICAgICAgIHZhciBsYXlvdXQgPSBkYXRhLmdldEl0ZW1MYXlvdXQoaWR4KTtcbiAgICAgICAgICAgIHZhciBhbmdsZSA9IGxheW91dC5hbmdsZSA9PT0gbWluQW5nbGUgPyBtaW5BbmdsZSA6IHZhbHVlICogdW5pdFJhZGlhbjtcbiAgICAgICAgICAgIGxheW91dC5zdGFydEFuZ2xlID0gY3VycmVudEFuZ2xlO1xuICAgICAgICAgICAgbGF5b3V0LmVuZEFuZ2xlID0gY3VycmVudEFuZ2xlICsgZGlyICogYW5nbGU7XG4gICAgICAgICAgICBjdXJyZW50QW5nbGUgKz0gZGlyICogYW5nbGU7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBsYWJlbExheW91dChzZXJpZXNNb2RlbCwgciwgd2lkdGgsIGhlaWdodCk7XG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/pie/pieLayout.js
