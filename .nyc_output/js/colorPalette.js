/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var makeInner = _model.makeInner;
var normalizeToArray = _model.normalizeToArray;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = makeInner();

function getNearestColorPalette(colors, requestColorNum) {
  var paletteNum = colors.length; // TODO colors must be in order

  for (var i = 0; i < paletteNum; i++) {
    if (colors[i].length > requestColorNum) {
      return colors[i];
    }
  }

  return colors[paletteNum - 1];
}

var _default = {
  clearColorPalette: function clearColorPalette() {
    inner(this).colorIdx = 0;
    inner(this).colorNameMap = {};
  },

  /**
   * @param {string} name MUST NOT be null/undefined. Otherwise call this function
   *                 twise with the same parameters will get different result.
   * @param {Object} [scope=this]
   * @param {Object} [requestColorNum]
   * @return {string} color string.
   */
  getColorFromPalette: function getColorFromPalette(name, scope, requestColorNum) {
    scope = scope || this;
    var scopeFields = inner(scope);
    var colorIdx = scopeFields.colorIdx || 0;
    var colorNameMap = scopeFields.colorNameMap = scopeFields.colorNameMap || {}; // Use `hasOwnProperty` to avoid conflict with Object.prototype.

    if (colorNameMap.hasOwnProperty(name)) {
      return colorNameMap[name];
    }

    var defaultColorPalette = normalizeToArray(this.get('color', true));
    var layeredColorPalette = this.get('colorLayer', true);
    var colorPalette = requestColorNum == null || !layeredColorPalette ? defaultColorPalette : getNearestColorPalette(layeredColorPalette, requestColorNum); // In case can't find in layered color palette.

    colorPalette = colorPalette || defaultColorPalette;

    if (!colorPalette || !colorPalette.length) {
      return;
    }

    var color = colorPalette[colorIdx];

    if (name) {
      colorNameMap[name] = color;
    }

    scopeFields.colorIdx = (colorIdx + 1) % colorPalette.length;
    return color;
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvbWl4aW4vY29sb3JQYWxldHRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvbWl4aW4vY29sb3JQYWxldHRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX21vZGVsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBtYWtlSW5uZXIgPSBfbW9kZWwubWFrZUlubmVyO1xudmFyIG5vcm1hbGl6ZVRvQXJyYXkgPSBfbW9kZWwubm9ybWFsaXplVG9BcnJheTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGlubmVyID0gbWFrZUlubmVyKCk7XG5cbmZ1bmN0aW9uIGdldE5lYXJlc3RDb2xvclBhbGV0dGUoY29sb3JzLCByZXF1ZXN0Q29sb3JOdW0pIHtcbiAgdmFyIHBhbGV0dGVOdW0gPSBjb2xvcnMubGVuZ3RoOyAvLyBUT0RPIGNvbG9ycyBtdXN0IGJlIGluIG9yZGVyXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYWxldHRlTnVtOyBpKyspIHtcbiAgICBpZiAoY29sb3JzW2ldLmxlbmd0aCA+IHJlcXVlc3RDb2xvck51bSkge1xuICAgICAgcmV0dXJuIGNvbG9yc1tpXTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gY29sb3JzW3BhbGV0dGVOdW0gLSAxXTtcbn1cblxudmFyIF9kZWZhdWx0ID0ge1xuICBjbGVhckNvbG9yUGFsZXR0ZTogZnVuY3Rpb24gKCkge1xuICAgIGlubmVyKHRoaXMpLmNvbG9ySWR4ID0gMDtcbiAgICBpbm5lcih0aGlzKS5jb2xvck5hbWVNYXAgPSB7fTtcbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgTVVTVCBOT1QgYmUgbnVsbC91bmRlZmluZWQuIE90aGVyd2lzZSBjYWxsIHRoaXMgZnVuY3Rpb25cbiAgICogICAgICAgICAgICAgICAgIHR3aXNlIHdpdGggdGhlIHNhbWUgcGFyYW1ldGVycyB3aWxsIGdldCBkaWZmZXJlbnQgcmVzdWx0LlxuICAgKiBAcGFyYW0ge09iamVjdH0gW3Njb3BlPXRoaXNdXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBbcmVxdWVzdENvbG9yTnVtXVxuICAgKiBAcmV0dXJuIHtzdHJpbmd9IGNvbG9yIHN0cmluZy5cbiAgICovXG4gIGdldENvbG9yRnJvbVBhbGV0dGU6IGZ1bmN0aW9uIChuYW1lLCBzY29wZSwgcmVxdWVzdENvbG9yTnVtKSB7XG4gICAgc2NvcGUgPSBzY29wZSB8fCB0aGlzO1xuICAgIHZhciBzY29wZUZpZWxkcyA9IGlubmVyKHNjb3BlKTtcbiAgICB2YXIgY29sb3JJZHggPSBzY29wZUZpZWxkcy5jb2xvcklkeCB8fCAwO1xuICAgIHZhciBjb2xvck5hbWVNYXAgPSBzY29wZUZpZWxkcy5jb2xvck5hbWVNYXAgPSBzY29wZUZpZWxkcy5jb2xvck5hbWVNYXAgfHwge307IC8vIFVzZSBgaGFzT3duUHJvcGVydHlgIHRvIGF2b2lkIGNvbmZsaWN0IHdpdGggT2JqZWN0LnByb3RvdHlwZS5cblxuICAgIGlmIChjb2xvck5hbWVNYXAuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgIHJldHVybiBjb2xvck5hbWVNYXBbbmFtZV07XG4gICAgfVxuXG4gICAgdmFyIGRlZmF1bHRDb2xvclBhbGV0dGUgPSBub3JtYWxpemVUb0FycmF5KHRoaXMuZ2V0KCdjb2xvcicsIHRydWUpKTtcbiAgICB2YXIgbGF5ZXJlZENvbG9yUGFsZXR0ZSA9IHRoaXMuZ2V0KCdjb2xvckxheWVyJywgdHJ1ZSk7XG4gICAgdmFyIGNvbG9yUGFsZXR0ZSA9IHJlcXVlc3RDb2xvck51bSA9PSBudWxsIHx8ICFsYXllcmVkQ29sb3JQYWxldHRlID8gZGVmYXVsdENvbG9yUGFsZXR0ZSA6IGdldE5lYXJlc3RDb2xvclBhbGV0dGUobGF5ZXJlZENvbG9yUGFsZXR0ZSwgcmVxdWVzdENvbG9yTnVtKTsgLy8gSW4gY2FzZSBjYW4ndCBmaW5kIGluIGxheWVyZWQgY29sb3IgcGFsZXR0ZS5cblxuICAgIGNvbG9yUGFsZXR0ZSA9IGNvbG9yUGFsZXR0ZSB8fCBkZWZhdWx0Q29sb3JQYWxldHRlO1xuXG4gICAgaWYgKCFjb2xvclBhbGV0dGUgfHwgIWNvbG9yUGFsZXR0ZS5sZW5ndGgpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgY29sb3IgPSBjb2xvclBhbGV0dGVbY29sb3JJZHhdO1xuXG4gICAgaWYgKG5hbWUpIHtcbiAgICAgIGNvbG9yTmFtZU1hcFtuYW1lXSA9IGNvbG9yO1xuICAgIH1cblxuICAgIHNjb3BlRmllbGRzLmNvbG9ySWR4ID0gKGNvbG9ySWR4ICsgMSkgJSBjb2xvclBhbGV0dGUubGVuZ3RoO1xuICAgIHJldHVybiBjb2xvcjtcbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpDQTtBQTJDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/mixin/colorPalette.js
