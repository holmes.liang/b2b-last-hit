

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var tinycolor2_1 = __importDefault(__webpack_require__(/*! tinycolor2 */ "./node_modules/tinycolor2/tinycolor.js"));

var hueStep = 2; // 色相阶梯

var saturationStep = 16; // 饱和度阶梯，浅色部分

var saturationStep2 = 5; // 饱和度阶梯，深色部分

var brightnessStep1 = 5; // 亮度阶梯，浅色部分

var brightnessStep2 = 15; // 亮度阶梯，深色部分

var lightColorCount = 5; // 浅色数量，主色上

var darkColorCount = 4; // 深色数量，主色下

function getHue(hsv, i, light) {
  var hue; // 根据色相不同，色相转向不同

  if (Math.round(hsv.h) >= 60 && Math.round(hsv.h) <= 240) {
    hue = light ? Math.round(hsv.h) - hueStep * i : Math.round(hsv.h) + hueStep * i;
  } else {
    hue = light ? Math.round(hsv.h) + hueStep * i : Math.round(hsv.h) - hueStep * i;
  }

  if (hue < 0) {
    hue += 360;
  } else if (hue >= 360) {
    hue -= 360;
  }

  return hue;
}

function getSaturation(hsv, i, light) {
  // grey color don't change saturation
  if (hsv.h === 0 && hsv.s === 0) {
    return hsv.s;
  }

  var saturation;

  if (light) {
    saturation = Math.round(hsv.s * 100) - saturationStep * i;
  } else if (i === darkColorCount) {
    saturation = Math.round(hsv.s * 100) + saturationStep;
  } else {
    saturation = Math.round(hsv.s * 100) + saturationStep2 * i;
  } // 边界值修正


  if (saturation > 100) {
    saturation = 100;
  } // 第一格的 s 限制在 6-10 之间


  if (light && i === lightColorCount && saturation > 10) {
    saturation = 10;
  }

  if (saturation < 6) {
    saturation = 6;
  }

  return saturation;
}

function getValue(hsv, i, light) {
  if (light) {
    return Math.round(hsv.v * 100) + brightnessStep1 * i;
  }

  return Math.round(hsv.v * 100) - brightnessStep2 * i;
}

function generate(color) {
  var patterns = [];
  var pColor = tinycolor2_1.default(color);

  for (var i = lightColorCount; i > 0; i -= 1) {
    var hsv = pColor.toHsv();
    var colorString = tinycolor2_1.default({
      h: getHue(hsv, i, true),
      s: getSaturation(hsv, i, true),
      v: getValue(hsv, i, true)
    }).toHexString();
    patterns.push(colorString);
  }

  patterns.push(pColor.toHexString());

  for (var i = 1; i <= darkColorCount; i += 1) {
    var hsv = pColor.toHsv();
    var colorString = tinycolor2_1.default({
      h: getHue(hsv, i),
      s: getSaturation(hsv, i),
      v: getValue(hsv, i)
    }).toHexString();
    patterns.push(colorString);
  }

  return patterns;
}

exports.default = generate;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvQGFudC1kZXNpZ24vY29sb3JzL2xpYi9nZW5lcmF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL0BhbnQtZGVzaWduL2NvbG9ycy9saWIvZ2VuZXJhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19pbXBvcnREZWZhdWx0ID0gKHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQpIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IFwiZGVmYXVsdFwiOiBtb2QgfTtcbn07XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG52YXIgdGlueWNvbG9yMl8xID0gX19pbXBvcnREZWZhdWx0KHJlcXVpcmUoXCJ0aW55Y29sb3IyXCIpKTtcbnZhciBodWVTdGVwID0gMjsgLy8g6Imy55u46Zi25qKvXG52YXIgc2F0dXJhdGlvblN0ZXAgPSAxNjsgLy8g6aWx5ZKM5bqm6Zi25qKv77yM5rWF6Imy6YOo5YiGXG52YXIgc2F0dXJhdGlvblN0ZXAyID0gNTsgLy8g6aWx5ZKM5bqm6Zi25qKv77yM5rex6Imy6YOo5YiGXG52YXIgYnJpZ2h0bmVzc1N0ZXAxID0gNTsgLy8g5Lqu5bqm6Zi25qKv77yM5rWF6Imy6YOo5YiGXG52YXIgYnJpZ2h0bmVzc1N0ZXAyID0gMTU7IC8vIOS6ruW6pumYtuair++8jOa3seiJsumDqOWIhlxudmFyIGxpZ2h0Q29sb3JDb3VudCA9IDU7IC8vIOa1heiJsuaVsOmHj++8jOS4u+iJsuS4ilxudmFyIGRhcmtDb2xvckNvdW50ID0gNDsgLy8g5rex6Imy5pWw6YeP77yM5Li76Imy5LiLXG5mdW5jdGlvbiBnZXRIdWUoaHN2LCBpLCBsaWdodCkge1xuICAgIHZhciBodWU7XG4gICAgLy8g5qC55o2u6Imy55u45LiN5ZCM77yM6Imy55u46L2s5ZCR5LiN5ZCMXG4gICAgaWYgKE1hdGgucm91bmQoaHN2LmgpID49IDYwICYmIE1hdGgucm91bmQoaHN2LmgpIDw9IDI0MCkge1xuICAgICAgICBodWUgPSBsaWdodCA/IE1hdGgucm91bmQoaHN2LmgpIC0gaHVlU3RlcCAqIGkgOiBNYXRoLnJvdW5kKGhzdi5oKSArIGh1ZVN0ZXAgKiBpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgaHVlID0gbGlnaHQgPyBNYXRoLnJvdW5kKGhzdi5oKSArIGh1ZVN0ZXAgKiBpIDogTWF0aC5yb3VuZChoc3YuaCkgLSBodWVTdGVwICogaTtcbiAgICB9XG4gICAgaWYgKGh1ZSA8IDApIHtcbiAgICAgICAgaHVlICs9IDM2MDtcbiAgICB9XG4gICAgZWxzZSBpZiAoaHVlID49IDM2MCkge1xuICAgICAgICBodWUgLT0gMzYwO1xuICAgIH1cbiAgICByZXR1cm4gaHVlO1xufVxuZnVuY3Rpb24gZ2V0U2F0dXJhdGlvbihoc3YsIGksIGxpZ2h0KSB7XG4gICAgLy8gZ3JleSBjb2xvciBkb24ndCBjaGFuZ2Ugc2F0dXJhdGlvblxuICAgIGlmIChoc3YuaCA9PT0gMCAmJiBoc3YucyA9PT0gMCkge1xuICAgICAgICByZXR1cm4gaHN2LnM7XG4gICAgfVxuICAgIHZhciBzYXR1cmF0aW9uO1xuICAgIGlmIChsaWdodCkge1xuICAgICAgICBzYXR1cmF0aW9uID0gTWF0aC5yb3VuZChoc3YucyAqIDEwMCkgLSBzYXR1cmF0aW9uU3RlcCAqIGk7XG4gICAgfVxuICAgIGVsc2UgaWYgKGkgPT09IGRhcmtDb2xvckNvdW50KSB7XG4gICAgICAgIHNhdHVyYXRpb24gPSBNYXRoLnJvdW5kKGhzdi5zICogMTAwKSArIHNhdHVyYXRpb25TdGVwO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgc2F0dXJhdGlvbiA9IE1hdGgucm91bmQoaHN2LnMgKiAxMDApICsgc2F0dXJhdGlvblN0ZXAyICogaTtcbiAgICB9XG4gICAgLy8g6L6555WM5YC85L+u5q2jXG4gICAgaWYgKHNhdHVyYXRpb24gPiAxMDApIHtcbiAgICAgICAgc2F0dXJhdGlvbiA9IDEwMDtcbiAgICB9XG4gICAgLy8g56ys5LiA5qC855qEIHMg6ZmQ5Yi25ZyoIDYtMTAg5LmL6Ze0XG4gICAgaWYgKGxpZ2h0ICYmIGkgPT09IGxpZ2h0Q29sb3JDb3VudCAmJiBzYXR1cmF0aW9uID4gMTApIHtcbiAgICAgICAgc2F0dXJhdGlvbiA9IDEwO1xuICAgIH1cbiAgICBpZiAoc2F0dXJhdGlvbiA8IDYpIHtcbiAgICAgICAgc2F0dXJhdGlvbiA9IDY7XG4gICAgfVxuICAgIHJldHVybiBzYXR1cmF0aW9uO1xufVxuZnVuY3Rpb24gZ2V0VmFsdWUoaHN2LCBpLCBsaWdodCkge1xuICAgIGlmIChsaWdodCkge1xuICAgICAgICByZXR1cm4gTWF0aC5yb3VuZChoc3YudiAqIDEwMCkgKyBicmlnaHRuZXNzU3RlcDEgKiBpO1xuICAgIH1cbiAgICByZXR1cm4gTWF0aC5yb3VuZChoc3YudiAqIDEwMCkgLSBicmlnaHRuZXNzU3RlcDIgKiBpO1xufVxuZnVuY3Rpb24gZ2VuZXJhdGUoY29sb3IpIHtcbiAgICB2YXIgcGF0dGVybnMgPSBbXTtcbiAgICB2YXIgcENvbG9yID0gdGlueWNvbG9yMl8xLmRlZmF1bHQoY29sb3IpO1xuICAgIGZvciAodmFyIGkgPSBsaWdodENvbG9yQ291bnQ7IGkgPiAwOyBpIC09IDEpIHtcbiAgICAgICAgdmFyIGhzdiA9IHBDb2xvci50b0hzdigpO1xuICAgICAgICB2YXIgY29sb3JTdHJpbmcgPSB0aW55Y29sb3IyXzEuZGVmYXVsdCh7XG4gICAgICAgICAgICBoOiBnZXRIdWUoaHN2LCBpLCB0cnVlKSxcbiAgICAgICAgICAgIHM6IGdldFNhdHVyYXRpb24oaHN2LCBpLCB0cnVlKSxcbiAgICAgICAgICAgIHY6IGdldFZhbHVlKGhzdiwgaSwgdHJ1ZSksXG4gICAgICAgIH0pLnRvSGV4U3RyaW5nKCk7XG4gICAgICAgIHBhdHRlcm5zLnB1c2goY29sb3JTdHJpbmcpO1xuICAgIH1cbiAgICBwYXR0ZXJucy5wdXNoKHBDb2xvci50b0hleFN0cmluZygpKTtcbiAgICBmb3IgKHZhciBpID0gMTsgaSA8PSBkYXJrQ29sb3JDb3VudDsgaSArPSAxKSB7XG4gICAgICAgIHZhciBoc3YgPSBwQ29sb3IudG9Ic3YoKTtcbiAgICAgICAgdmFyIGNvbG9yU3RyaW5nID0gdGlueWNvbG9yMl8xLmRlZmF1bHQoe1xuICAgICAgICAgICAgaDogZ2V0SHVlKGhzdiwgaSksXG4gICAgICAgICAgICBzOiBnZXRTYXR1cmF0aW9uKGhzdiwgaSksXG4gICAgICAgICAgICB2OiBnZXRWYWx1ZShoc3YsIGkpLFxuICAgICAgICB9KS50b0hleFN0cmluZygpO1xuICAgICAgICBwYXR0ZXJucy5wdXNoKGNvbG9yU3RyaW5nKTtcbiAgICB9XG4gICAgcmV0dXJuIHBhdHRlcm5zO1xufVxuZXhwb3J0cy5kZWZhdWx0ID0gZ2VuZXJhdGU7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/@ant-design/colors/lib/generate.js
