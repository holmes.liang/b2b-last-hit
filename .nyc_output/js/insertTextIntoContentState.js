/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule insertTextIntoContentState
 * @format
 * 
 */


var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var insertIntoList = __webpack_require__(/*! ./insertIntoList */ "./node_modules/draft-js/lib/insertIntoList.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var Repeat = Immutable.Repeat;

function insertTextIntoContentState(contentState, selectionState, text, characterMetadata) {
  !selectionState.isCollapsed() ?  true ? invariant(false, '`insertText` should only be called with a collapsed range.') : undefined : void 0;
  var len = text.length;

  if (!len) {
    return contentState;
  }

  var blockMap = contentState.getBlockMap();
  var key = selectionState.getStartKey();
  var offset = selectionState.getStartOffset();
  var block = blockMap.get(key);
  var blockText = block.getText();
  var newBlock = block.merge({
    text: blockText.slice(0, offset) + text + blockText.slice(offset, block.getLength()),
    characterList: insertIntoList(block.getCharacterList(), Repeat(characterMetadata, len).toList(), offset)
  });
  var newOffset = offset + len;
  return contentState.merge({
    blockMap: blockMap.set(key, newBlock),
    selectionAfter: selectionState.merge({
      anchorOffset: newOffset,
      focusOffset: newOffset
    })
  });
}

module.exports = insertTextIntoContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2luc2VydFRleHRJbnRvQ29udGVudFN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2luc2VydFRleHRJbnRvQ29udGVudFN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgaW5zZXJ0VGV4dEludG9Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgaW5zZXJ0SW50b0xpc3QgPSByZXF1aXJlKCcuL2luc2VydEludG9MaXN0Jyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbnZhciBSZXBlYXQgPSBJbW11dGFibGUuUmVwZWF0O1xuXG5cbmZ1bmN0aW9uIGluc2VydFRleHRJbnRvQ29udGVudFN0YXRlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIHRleHQsIGNoYXJhY3Rlck1ldGFkYXRhKSB7XG4gICFzZWxlY3Rpb25TdGF0ZS5pc0NvbGxhcHNlZCgpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2BpbnNlcnRUZXh0YCBzaG91bGQgb25seSBiZSBjYWxsZWQgd2l0aCBhIGNvbGxhcHNlZCByYW5nZS4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgdmFyIGxlbiA9IHRleHQubGVuZ3RoO1xuICBpZiAoIWxlbikge1xuICAgIHJldHVybiBjb250ZW50U3RhdGU7XG4gIH1cblxuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIGtleSA9IHNlbGVjdGlvblN0YXRlLmdldFN0YXJ0S2V5KCk7XG4gIHZhciBvZmZzZXQgPSBzZWxlY3Rpb25TdGF0ZS5nZXRTdGFydE9mZnNldCgpO1xuICB2YXIgYmxvY2sgPSBibG9ja01hcC5nZXQoa2V5KTtcbiAgdmFyIGJsb2NrVGV4dCA9IGJsb2NrLmdldFRleHQoKTtcblxuICB2YXIgbmV3QmxvY2sgPSBibG9jay5tZXJnZSh7XG4gICAgdGV4dDogYmxvY2tUZXh0LnNsaWNlKDAsIG9mZnNldCkgKyB0ZXh0ICsgYmxvY2tUZXh0LnNsaWNlKG9mZnNldCwgYmxvY2suZ2V0TGVuZ3RoKCkpLFxuICAgIGNoYXJhY3Rlckxpc3Q6IGluc2VydEludG9MaXN0KGJsb2NrLmdldENoYXJhY3Rlckxpc3QoKSwgUmVwZWF0KGNoYXJhY3Rlck1ldGFkYXRhLCBsZW4pLnRvTGlzdCgpLCBvZmZzZXQpXG4gIH0pO1xuXG4gIHZhciBuZXdPZmZzZXQgPSBvZmZzZXQgKyBsZW47XG5cbiAgcmV0dXJuIGNvbnRlbnRTdGF0ZS5tZXJnZSh7XG4gICAgYmxvY2tNYXA6IGJsb2NrTWFwLnNldChrZXksIG5ld0Jsb2NrKSxcbiAgICBzZWxlY3Rpb25BZnRlcjogc2VsZWN0aW9uU3RhdGUubWVyZ2Uoe1xuICAgICAgYW5jaG9yT2Zmc2V0OiBuZXdPZmZzZXQsXG4gICAgICBmb2N1c09mZnNldDogbmV3T2Zmc2V0XG4gICAgfSlcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gaW5zZXJ0VGV4dEludG9Db250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQU9BO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/insertTextIntoContentState.js
