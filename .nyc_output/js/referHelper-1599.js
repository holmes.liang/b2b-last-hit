/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var retrieve = _util.retrieve;
var each = _util.each;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Helper for model references.
 * There are many manners to refer axis/coordSys.
 */
// TODO
// merge relevant logic to this file?
// check: "modelHelper" of tooltip and "BrushTargetManager".

/**
 * @return {Object} For example:
 * {
 *     coordSysName: 'cartesian2d',
 *     coordSysDims: ['x', 'y', ...],
 *     axisMap: HashMap({
 *         x: xAxisModel,
 *         y: yAxisModel
 *     }),
 *     categoryAxisMap: HashMap({
 *         x: xAxisModel,
 *         y: undefined
 *     }),
 *     // It also indicate that whether there is category axis.
 *     firstCategoryDimIndex: 1,
 *     // To replace user specified encode.
 * }
 */

function getCoordSysDefineBySeries(seriesModel) {
  var coordSysName = seriesModel.get('coordinateSystem');
  var result = {
    coordSysName: coordSysName,
    coordSysDims: [],
    axisMap: createHashMap(),
    categoryAxisMap: createHashMap()
  };
  var fetch = fetchers[coordSysName];

  if (fetch) {
    fetch(seriesModel, result, result.axisMap, result.categoryAxisMap);
    return result;
  }
}

var fetchers = {
  cartesian2d: function cartesian2d(seriesModel, result, axisMap, categoryAxisMap) {
    var xAxisModel = seriesModel.getReferringComponents('xAxis')[0];
    var yAxisModel = seriesModel.getReferringComponents('yAxis')[0];
    result.coordSysDims = ['x', 'y'];
    axisMap.set('x', xAxisModel);
    axisMap.set('y', yAxisModel);

    if (isCategory(xAxisModel)) {
      categoryAxisMap.set('x', xAxisModel);
      result.firstCategoryDimIndex = 0;
    }

    if (isCategory(yAxisModel)) {
      categoryAxisMap.set('y', yAxisModel);
      result.firstCategoryDimIndex = 1;
    }
  },
  singleAxis: function singleAxis(seriesModel, result, axisMap, categoryAxisMap) {
    var singleAxisModel = seriesModel.getReferringComponents('singleAxis')[0];
    result.coordSysDims = ['single'];
    axisMap.set('single', singleAxisModel);

    if (isCategory(singleAxisModel)) {
      categoryAxisMap.set('single', singleAxisModel);
      result.firstCategoryDimIndex = 0;
    }
  },
  polar: function polar(seriesModel, result, axisMap, categoryAxisMap) {
    var polarModel = seriesModel.getReferringComponents('polar')[0];
    var radiusAxisModel = polarModel.findAxisModel('radiusAxis');
    var angleAxisModel = polarModel.findAxisModel('angleAxis');
    result.coordSysDims = ['radius', 'angle'];
    axisMap.set('radius', radiusAxisModel);
    axisMap.set('angle', angleAxisModel);

    if (isCategory(radiusAxisModel)) {
      categoryAxisMap.set('radius', radiusAxisModel);
      result.firstCategoryDimIndex = 0;
    }

    if (isCategory(angleAxisModel)) {
      categoryAxisMap.set('angle', angleAxisModel);
      result.firstCategoryDimIndex = 1;
    }
  },
  geo: function geo(seriesModel, result, axisMap, categoryAxisMap) {
    result.coordSysDims = ['lng', 'lat'];
  },
  parallel: function parallel(seriesModel, result, axisMap, categoryAxisMap) {
    var ecModel = seriesModel.ecModel;
    var parallelModel = ecModel.getComponent('parallel', seriesModel.get('parallelIndex'));
    var coordSysDims = result.coordSysDims = parallelModel.dimensions.slice();
    each(parallelModel.parallelAxisIndex, function (axisIndex, index) {
      var axisModel = ecModel.getComponent('parallelAxis', axisIndex);
      var axisDim = coordSysDims[index];
      axisMap.set(axisDim, axisModel);

      if (isCategory(axisModel) && result.firstCategoryDimIndex == null) {
        categoryAxisMap.set(axisDim, axisModel);
        result.firstCategoryDimIndex = index;
      }
    });
  }
};

function isCategory(axisModel) {
  return axisModel.get('type') === 'category';
}

exports.getCoordSysDefineBySeries = getCoordSysDefineBySeries;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvcmVmZXJIZWxwZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9tb2RlbC9yZWZlckhlbHBlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGNyZWF0ZUhhc2hNYXAgPSBfdXRpbC5jcmVhdGVIYXNoTWFwO1xudmFyIHJldHJpZXZlID0gX3V0aWwucmV0cmlldmU7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxuLyoqXG4gKiBIZWxwZXIgZm9yIG1vZGVsIHJlZmVyZW5jZXMuXG4gKiBUaGVyZSBhcmUgbWFueSBtYW5uZXJzIHRvIHJlZmVyIGF4aXMvY29vcmRTeXMuXG4gKi9cbi8vIFRPRE9cbi8vIG1lcmdlIHJlbGV2YW50IGxvZ2ljIHRvIHRoaXMgZmlsZT9cbi8vIGNoZWNrOiBcIm1vZGVsSGVscGVyXCIgb2YgdG9vbHRpcCBhbmQgXCJCcnVzaFRhcmdldE1hbmFnZXJcIi5cblxuLyoqXG4gKiBAcmV0dXJuIHtPYmplY3R9IEZvciBleGFtcGxlOlxuICoge1xuICogICAgIGNvb3JkU3lzTmFtZTogJ2NhcnRlc2lhbjJkJyxcbiAqICAgICBjb29yZFN5c0RpbXM6IFsneCcsICd5JywgLi4uXSxcbiAqICAgICBheGlzTWFwOiBIYXNoTWFwKHtcbiAqICAgICAgICAgeDogeEF4aXNNb2RlbCxcbiAqICAgICAgICAgeTogeUF4aXNNb2RlbFxuICogICAgIH0pLFxuICogICAgIGNhdGVnb3J5QXhpc01hcDogSGFzaE1hcCh7XG4gKiAgICAgICAgIHg6IHhBeGlzTW9kZWwsXG4gKiAgICAgICAgIHk6IHVuZGVmaW5lZFxuICogICAgIH0pLFxuICogICAgIC8vIEl0IGFsc28gaW5kaWNhdGUgdGhhdCB3aGV0aGVyIHRoZXJlIGlzIGNhdGVnb3J5IGF4aXMuXG4gKiAgICAgZmlyc3RDYXRlZ29yeURpbUluZGV4OiAxLFxuICogICAgIC8vIFRvIHJlcGxhY2UgdXNlciBzcGVjaWZpZWQgZW5jb2RlLlxuICogfVxuICovXG5mdW5jdGlvbiBnZXRDb29yZFN5c0RlZmluZUJ5U2VyaWVzKHNlcmllc01vZGVsKSB7XG4gIHZhciBjb29yZFN5c05hbWUgPSBzZXJpZXNNb2RlbC5nZXQoJ2Nvb3JkaW5hdGVTeXN0ZW0nKTtcbiAgdmFyIHJlc3VsdCA9IHtcbiAgICBjb29yZFN5c05hbWU6IGNvb3JkU3lzTmFtZSxcbiAgICBjb29yZFN5c0RpbXM6IFtdLFxuICAgIGF4aXNNYXA6IGNyZWF0ZUhhc2hNYXAoKSxcbiAgICBjYXRlZ29yeUF4aXNNYXA6IGNyZWF0ZUhhc2hNYXAoKVxuICB9O1xuICB2YXIgZmV0Y2ggPSBmZXRjaGVyc1tjb29yZFN5c05hbWVdO1xuXG4gIGlmIChmZXRjaCkge1xuICAgIGZldGNoKHNlcmllc01vZGVsLCByZXN1bHQsIHJlc3VsdC5heGlzTWFwLCByZXN1bHQuY2F0ZWdvcnlBeGlzTWFwKTtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG59XG5cbnZhciBmZXRjaGVycyA9IHtcbiAgY2FydGVzaWFuMmQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgcmVzdWx0LCBheGlzTWFwLCBjYXRlZ29yeUF4aXNNYXApIHtcbiAgICB2YXIgeEF4aXNNb2RlbCA9IHNlcmllc01vZGVsLmdldFJlZmVycmluZ0NvbXBvbmVudHMoJ3hBeGlzJylbMF07XG4gICAgdmFyIHlBeGlzTW9kZWwgPSBzZXJpZXNNb2RlbC5nZXRSZWZlcnJpbmdDb21wb25lbnRzKCd5QXhpcycpWzBdO1xuICAgIHJlc3VsdC5jb29yZFN5c0RpbXMgPSBbJ3gnLCAneSddO1xuICAgIGF4aXNNYXAuc2V0KCd4JywgeEF4aXNNb2RlbCk7XG4gICAgYXhpc01hcC5zZXQoJ3knLCB5QXhpc01vZGVsKTtcblxuICAgIGlmIChpc0NhdGVnb3J5KHhBeGlzTW9kZWwpKSB7XG4gICAgICBjYXRlZ29yeUF4aXNNYXAuc2V0KCd4JywgeEF4aXNNb2RlbCk7XG4gICAgICByZXN1bHQuZmlyc3RDYXRlZ29yeURpbUluZGV4ID0gMDtcbiAgICB9XG5cbiAgICBpZiAoaXNDYXRlZ29yeSh5QXhpc01vZGVsKSkge1xuICAgICAgY2F0ZWdvcnlBeGlzTWFwLnNldCgneScsIHlBeGlzTW9kZWwpO1xuICAgICAgcmVzdWx0LmZpcnN0Q2F0ZWdvcnlEaW1JbmRleCA9IDE7XG4gICAgfVxuICB9LFxuICBzaW5nbGVBeGlzOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIHJlc3VsdCwgYXhpc01hcCwgY2F0ZWdvcnlBeGlzTWFwKSB7XG4gICAgdmFyIHNpbmdsZUF4aXNNb2RlbCA9IHNlcmllc01vZGVsLmdldFJlZmVycmluZ0NvbXBvbmVudHMoJ3NpbmdsZUF4aXMnKVswXTtcbiAgICByZXN1bHQuY29vcmRTeXNEaW1zID0gWydzaW5nbGUnXTtcbiAgICBheGlzTWFwLnNldCgnc2luZ2xlJywgc2luZ2xlQXhpc01vZGVsKTtcblxuICAgIGlmIChpc0NhdGVnb3J5KHNpbmdsZUF4aXNNb2RlbCkpIHtcbiAgICAgIGNhdGVnb3J5QXhpc01hcC5zZXQoJ3NpbmdsZScsIHNpbmdsZUF4aXNNb2RlbCk7XG4gICAgICByZXN1bHQuZmlyc3RDYXRlZ29yeURpbUluZGV4ID0gMDtcbiAgICB9XG4gIH0sXG4gIHBvbGFyOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIHJlc3VsdCwgYXhpc01hcCwgY2F0ZWdvcnlBeGlzTWFwKSB7XG4gICAgdmFyIHBvbGFyTW9kZWwgPSBzZXJpZXNNb2RlbC5nZXRSZWZlcnJpbmdDb21wb25lbnRzKCdwb2xhcicpWzBdO1xuICAgIHZhciByYWRpdXNBeGlzTW9kZWwgPSBwb2xhck1vZGVsLmZpbmRBeGlzTW9kZWwoJ3JhZGl1c0F4aXMnKTtcbiAgICB2YXIgYW5nbGVBeGlzTW9kZWwgPSBwb2xhck1vZGVsLmZpbmRBeGlzTW9kZWwoJ2FuZ2xlQXhpcycpO1xuICAgIHJlc3VsdC5jb29yZFN5c0RpbXMgPSBbJ3JhZGl1cycsICdhbmdsZSddO1xuICAgIGF4aXNNYXAuc2V0KCdyYWRpdXMnLCByYWRpdXNBeGlzTW9kZWwpO1xuICAgIGF4aXNNYXAuc2V0KCdhbmdsZScsIGFuZ2xlQXhpc01vZGVsKTtcblxuICAgIGlmIChpc0NhdGVnb3J5KHJhZGl1c0F4aXNNb2RlbCkpIHtcbiAgICAgIGNhdGVnb3J5QXhpc01hcC5zZXQoJ3JhZGl1cycsIHJhZGl1c0F4aXNNb2RlbCk7XG4gICAgICByZXN1bHQuZmlyc3RDYXRlZ29yeURpbUluZGV4ID0gMDtcbiAgICB9XG5cbiAgICBpZiAoaXNDYXRlZ29yeShhbmdsZUF4aXNNb2RlbCkpIHtcbiAgICAgIGNhdGVnb3J5QXhpc01hcC5zZXQoJ2FuZ2xlJywgYW5nbGVBeGlzTW9kZWwpO1xuICAgICAgcmVzdWx0LmZpcnN0Q2F0ZWdvcnlEaW1JbmRleCA9IDE7XG4gICAgfVxuICB9LFxuICBnZW86IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgcmVzdWx0LCBheGlzTWFwLCBjYXRlZ29yeUF4aXNNYXApIHtcbiAgICByZXN1bHQuY29vcmRTeXNEaW1zID0gWydsbmcnLCAnbGF0J107XG4gIH0sXG4gIHBhcmFsbGVsOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIHJlc3VsdCwgYXhpc01hcCwgY2F0ZWdvcnlBeGlzTWFwKSB7XG4gICAgdmFyIGVjTW9kZWwgPSBzZXJpZXNNb2RlbC5lY01vZGVsO1xuICAgIHZhciBwYXJhbGxlbE1vZGVsID0gZWNNb2RlbC5nZXRDb21wb25lbnQoJ3BhcmFsbGVsJywgc2VyaWVzTW9kZWwuZ2V0KCdwYXJhbGxlbEluZGV4JykpO1xuICAgIHZhciBjb29yZFN5c0RpbXMgPSByZXN1bHQuY29vcmRTeXNEaW1zID0gcGFyYWxsZWxNb2RlbC5kaW1lbnNpb25zLnNsaWNlKCk7XG4gICAgZWFjaChwYXJhbGxlbE1vZGVsLnBhcmFsbGVsQXhpc0luZGV4LCBmdW5jdGlvbiAoYXhpc0luZGV4LCBpbmRleCkge1xuICAgICAgdmFyIGF4aXNNb2RlbCA9IGVjTW9kZWwuZ2V0Q29tcG9uZW50KCdwYXJhbGxlbEF4aXMnLCBheGlzSW5kZXgpO1xuICAgICAgdmFyIGF4aXNEaW0gPSBjb29yZFN5c0RpbXNbaW5kZXhdO1xuICAgICAgYXhpc01hcC5zZXQoYXhpc0RpbSwgYXhpc01vZGVsKTtcblxuICAgICAgaWYgKGlzQ2F0ZWdvcnkoYXhpc01vZGVsKSAmJiByZXN1bHQuZmlyc3RDYXRlZ29yeURpbUluZGV4ID09IG51bGwpIHtcbiAgICAgICAgY2F0ZWdvcnlBeGlzTWFwLnNldChheGlzRGltLCBheGlzTW9kZWwpO1xuICAgICAgICByZXN1bHQuZmlyc3RDYXRlZ29yeURpbUluZGV4ID0gaW5kZXg7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGlzQ2F0ZWdvcnkoYXhpc01vZGVsKSB7XG4gIHJldHVybiBheGlzTW9kZWwuZ2V0KCd0eXBlJykgPT09ICdjYXRlZ29yeSc7XG59XG5cbmV4cG9ydHMuZ2V0Q29vcmRTeXNEZWZpbmVCeVNlcmllcyA9IGdldENvb3JkU3lzRGVmaW5lQnlTZXJpZXM7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0RBO0FBQ0E7QUFpRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/referHelper.js
