__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_Track__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./common/Track */ "./node_modules/rc-slider/es/common/Track.js");
/* harmony import */ var _common_createSlider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./common/createSlider */ "./node_modules/rc-slider/es/common/createSlider.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-slider/es/utils.js");





/* eslint-disable react/prop-types */








var Slider = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(Slider, _React$Component);

  function Slider(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Slider);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (Slider.__proto__ || Object.getPrototypeOf(Slider)).call(this, props));

    _this.onEnd = function (force) {
      var dragging = _this.state.dragging;

      _this.removeDocumentEvents();

      if (dragging || force) {
        _this.props.onAfterChange(_this.getValue());
      }

      _this.setState({
        dragging: false
      });
    };

    var defaultValue = props.defaultValue !== undefined ? props.defaultValue : props.min;
    var value = props.value !== undefined ? props.value : defaultValue;
    _this.state = {
      value: _this.trimAlignValue(value),
      dragging: false
    };
    warning__WEBPACK_IMPORTED_MODULE_7___default()(!('minimumTrackStyle' in props), 'minimumTrackStyle will be deprecated, please use trackStyle instead.');
    warning__WEBPACK_IMPORTED_MODULE_7___default()(!('maximumTrackStyle' in props), 'maximumTrackStyle will be deprecated, please use railStyle instead.');
    return _this;
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Slider, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (!('value' in this.props || 'min' in this.props || 'max' in this.props)) {
        return;
      }

      var _props = this.props,
          value = _props.value,
          onChange = _props.onChange;
      var theValue = value !== undefined ? value : prevState.value;
      var nextValue = this.trimAlignValue(theValue, this.props);

      if (nextValue !== prevState.value) {
        // eslint-disable-next-line
        this.setState({
          value: nextValue
        });

        if (_utils__WEBPACK_IMPORTED_MODULE_10__["isValueOutOfRange"](theValue, this.props)) {
          onChange(nextValue);
        }
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(state) {
      var props = this.props;
      var isNotControlled = !('value' in props);
      var nextState = state.value > this.props.max ? babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, state, {
        value: this.props.max
      }) : state;

      if (isNotControlled) {
        this.setState(nextState);
      }

      var changedValue = nextState.value;
      props.onChange(changedValue);
    }
  }, {
    key: 'onStart',
    value: function onStart(position) {
      this.setState({
        dragging: true
      });
      var props = this.props;
      var prevValue = this.getValue();
      props.onBeforeChange(prevValue);
      var value = this.calcValueByPos(position);
      this.startValue = value;
      this.startPosition = position;
      if (value === prevValue) return;
      this.prevMovedHandleIndex = 0;
      this.onChange({
        value: value
      });
    }
  }, {
    key: 'onMove',
    value: function onMove(e, position) {
      _utils__WEBPACK_IMPORTED_MODULE_10__["pauseEvent"](e);
      var oldValue = this.state.value;
      var value = this.calcValueByPos(position);
      if (value === oldValue) return;
      this.onChange({
        value: value
      });
    }
  }, {
    key: 'onKeyboard',
    value: function onKeyboard(e) {
      var _props2 = this.props,
          reverse = _props2.reverse,
          vertical = _props2.vertical;
      var valueMutator = _utils__WEBPACK_IMPORTED_MODULE_10__["getKeyboardValueMutator"](e, vertical, reverse);

      if (valueMutator) {
        _utils__WEBPACK_IMPORTED_MODULE_10__["pauseEvent"](e);
        var state = this.state;
        var oldValue = state.value;
        var mutatedValue = valueMutator(oldValue, this.props);
        var value = this.trimAlignValue(mutatedValue);
        if (value === oldValue) return;
        this.onChange({
          value: value
        });
        this.props.onAfterChange(value);
        this.onEnd();
      }
    }
  }, {
    key: 'getValue',
    value: function getValue() {
      return this.state.value;
    }
  }, {
    key: 'getLowerBound',
    value: function getLowerBound() {
      return this.props.min;
    }
  }, {
    key: 'getUpperBound',
    value: function getUpperBound() {
      return this.state.value;
    }
  }, {
    key: 'trimAlignValue',
    value: function trimAlignValue(v) {
      var nextProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (v === null) {
        return null;
      }

      var mergedProps = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, this.props, nextProps);

      var val = _utils__WEBPACK_IMPORTED_MODULE_10__["ensureValueInRange"](v, mergedProps);
      return _utils__WEBPACK_IMPORTED_MODULE_10__["ensureValuePrecision"](val, mergedProps);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props3 = this.props,
          prefixCls = _props3.prefixCls,
          vertical = _props3.vertical,
          included = _props3.included,
          disabled = _props3.disabled,
          minimumTrackStyle = _props3.minimumTrackStyle,
          trackStyle = _props3.trackStyle,
          handleStyle = _props3.handleStyle,
          tabIndex = _props3.tabIndex,
          min = _props3.min,
          max = _props3.max,
          reverse = _props3.reverse,
          handleGenerator = _props3.handle;
      var _state = this.state,
          value = _state.value,
          dragging = _state.dragging;
      var offset = this.calcOffset(value);
      var handle = handleGenerator({
        className: prefixCls + '-handle',
        prefixCls: prefixCls,
        vertical: vertical,
        offset: offset,
        value: value,
        dragging: dragging,
        disabled: disabled,
        min: min,
        max: max,
        reverse: reverse,
        index: 0,
        tabIndex: tabIndex,
        style: handleStyle[0] || handleStyle,
        ref: function ref(h) {
          return _this2.saveHandle(0, h);
        }
      });

      var _trackStyle = trackStyle[0] || trackStyle;

      var track = react__WEBPACK_IMPORTED_MODULE_5___default.a.createElement(_common_Track__WEBPACK_IMPORTED_MODULE_8__["default"], {
        className: prefixCls + '-track',
        vertical: vertical,
        included: included,
        offset: 0,
        reverse: reverse,
        length: offset,
        style: babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, minimumTrackStyle, _trackStyle)
      });
      return {
        tracks: track,
        handles: handle
      };
    }
  }]);

  return Slider;
}(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

Slider.propTypes = {
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  value: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  autoFocus: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  tabIndex: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  reverse: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  min: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number,
  max: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.number
};
/* harmony default export */ __webpack_exports__["default"] = (Object(_common_createSlider__WEBPACK_IMPORTED_MODULE_9__["default"])(Slider));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2xpZGVyL2VzL1NsaWRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNsaWRlci9lcy9TbGlkZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfY3JlYXRlQ2xhc3MgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG4vKiBlc2xpbnQtZGlzYWJsZSByZWFjdC9wcm9wLXR5cGVzICovXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJ3dhcm5pbmcnO1xuaW1wb3J0IFRyYWNrIGZyb20gJy4vY29tbW9uL1RyYWNrJztcbmltcG9ydCBjcmVhdGVTbGlkZXIgZnJvbSAnLi9jb21tb24vY3JlYXRlU2xpZGVyJztcbmltcG9ydCAqIGFzIHV0aWxzIGZyb20gJy4vdXRpbHMnO1xuXG52YXIgU2xpZGVyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFNsaWRlciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU2xpZGVyKHByb3BzKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNsaWRlcik7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoU2xpZGVyLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoU2xpZGVyKSkuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX3RoaXMub25FbmQgPSBmdW5jdGlvbiAoZm9yY2UpIHtcbiAgICAgIHZhciBkcmFnZ2luZyA9IF90aGlzLnN0YXRlLmRyYWdnaW5nO1xuXG4gICAgICBfdGhpcy5yZW1vdmVEb2N1bWVudEV2ZW50cygpO1xuICAgICAgaWYgKGRyYWdnaW5nIHx8IGZvcmNlKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uQWZ0ZXJDaGFuZ2UoX3RoaXMuZ2V0VmFsdWUoKSk7XG4gICAgICB9XG4gICAgICBfdGhpcy5zZXRTdGF0ZSh7IGRyYWdnaW5nOiBmYWxzZSB9KTtcbiAgICB9O1xuXG4gICAgdmFyIGRlZmF1bHRWYWx1ZSA9IHByb3BzLmRlZmF1bHRWYWx1ZSAhPT0gdW5kZWZpbmVkID8gcHJvcHMuZGVmYXVsdFZhbHVlIDogcHJvcHMubWluO1xuICAgIHZhciB2YWx1ZSA9IHByb3BzLnZhbHVlICE9PSB1bmRlZmluZWQgPyBwcm9wcy52YWx1ZSA6IGRlZmF1bHRWYWx1ZTtcblxuICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAgdmFsdWU6IF90aGlzLnRyaW1BbGlnblZhbHVlKHZhbHVlKSxcbiAgICAgIGRyYWdnaW5nOiBmYWxzZVxuICAgIH07XG5cbiAgICB3YXJuaW5nKCEoJ21pbmltdW1UcmFja1N0eWxlJyBpbiBwcm9wcyksICdtaW5pbXVtVHJhY2tTdHlsZSB3aWxsIGJlIGRlcHJlY2F0ZWQsIHBsZWFzZSB1c2UgdHJhY2tTdHlsZSBpbnN0ZWFkLicpO1xuICAgIHdhcm5pbmcoISgnbWF4aW11bVRyYWNrU3R5bGUnIGluIHByb3BzKSwgJ21heGltdW1UcmFja1N0eWxlIHdpbGwgYmUgZGVwcmVjYXRlZCwgcGxlYXNlIHVzZSByYWlsU3R5bGUgaW5zdGVhZC4nKTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoU2xpZGVyLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgaWYgKCEoJ3ZhbHVlJyBpbiB0aGlzLnByb3BzIHx8ICdtaW4nIGluIHRoaXMucHJvcHMgfHwgJ21heCcgaW4gdGhpcy5wcm9wcykpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgdmFsdWUgPSBfcHJvcHMudmFsdWUsXG4gICAgICAgICAgb25DaGFuZ2UgPSBfcHJvcHMub25DaGFuZ2U7XG5cbiAgICAgIHZhciB0aGVWYWx1ZSA9IHZhbHVlICE9PSB1bmRlZmluZWQgPyB2YWx1ZSA6IHByZXZTdGF0ZS52YWx1ZTtcbiAgICAgIHZhciBuZXh0VmFsdWUgPSB0aGlzLnRyaW1BbGlnblZhbHVlKHRoZVZhbHVlLCB0aGlzLnByb3BzKTtcbiAgICAgIGlmIChuZXh0VmFsdWUgIT09IHByZXZTdGF0ZS52YWx1ZSkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHZhbHVlOiBuZXh0VmFsdWUgfSk7XG4gICAgICAgIGlmICh1dGlscy5pc1ZhbHVlT3V0T2ZSYW5nZSh0aGVWYWx1ZSwgdGhpcy5wcm9wcykpIHtcbiAgICAgICAgICBvbkNoYW5nZShuZXh0VmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25DaGFuZ2UnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkNoYW5nZShzdGF0ZSkge1xuICAgICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgIHZhciBpc05vdENvbnRyb2xsZWQgPSAhKCd2YWx1ZScgaW4gcHJvcHMpO1xuICAgICAgdmFyIG5leHRTdGF0ZSA9IHN0YXRlLnZhbHVlID4gdGhpcy5wcm9wcy5tYXggPyBfZXh0ZW5kcyh7fSwgc3RhdGUsIHsgdmFsdWU6IHRoaXMucHJvcHMubWF4IH0pIDogc3RhdGU7XG4gICAgICBpZiAoaXNOb3RDb250cm9sbGVkKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUobmV4dFN0YXRlKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGNoYW5nZWRWYWx1ZSA9IG5leHRTdGF0ZS52YWx1ZTtcbiAgICAgIHByb3BzLm9uQ2hhbmdlKGNoYW5nZWRWYWx1ZSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25TdGFydCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uU3RhcnQocG9zaXRpb24pIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBkcmFnZ2luZzogdHJ1ZSB9KTtcbiAgICAgIHZhciBwcm9wcyA9IHRoaXMucHJvcHM7XG4gICAgICB2YXIgcHJldlZhbHVlID0gdGhpcy5nZXRWYWx1ZSgpO1xuICAgICAgcHJvcHMub25CZWZvcmVDaGFuZ2UocHJldlZhbHVlKTtcblxuICAgICAgdmFyIHZhbHVlID0gdGhpcy5jYWxjVmFsdWVCeVBvcyhwb3NpdGlvbik7XG4gICAgICB0aGlzLnN0YXJ0VmFsdWUgPSB2YWx1ZTtcbiAgICAgIHRoaXMuc3RhcnRQb3NpdGlvbiA9IHBvc2l0aW9uO1xuXG4gICAgICBpZiAodmFsdWUgPT09IHByZXZWYWx1ZSkgcmV0dXJuO1xuXG4gICAgICB0aGlzLnByZXZNb3ZlZEhhbmRsZUluZGV4ID0gMDtcblxuICAgICAgdGhpcy5vbkNoYW5nZSh7IHZhbHVlOiB2YWx1ZSB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbk1vdmUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbk1vdmUoZSwgcG9zaXRpb24pIHtcbiAgICAgIHV0aWxzLnBhdXNlRXZlbnQoZSk7XG4gICAgICB2YXIgb2xkVmFsdWUgPSB0aGlzLnN0YXRlLnZhbHVlO1xuXG4gICAgICB2YXIgdmFsdWUgPSB0aGlzLmNhbGNWYWx1ZUJ5UG9zKHBvc2l0aW9uKTtcbiAgICAgIGlmICh2YWx1ZSA9PT0gb2xkVmFsdWUpIHJldHVybjtcblxuICAgICAgdGhpcy5vbkNoYW5nZSh7IHZhbHVlOiB2YWx1ZSB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbktleWJvYXJkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25LZXlib2FyZChlKSB7XG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcmV2ZXJzZSA9IF9wcm9wczIucmV2ZXJzZSxcbiAgICAgICAgICB2ZXJ0aWNhbCA9IF9wcm9wczIudmVydGljYWw7XG5cbiAgICAgIHZhciB2YWx1ZU11dGF0b3IgPSB1dGlscy5nZXRLZXlib2FyZFZhbHVlTXV0YXRvcihlLCB2ZXJ0aWNhbCwgcmV2ZXJzZSk7XG4gICAgICBpZiAodmFsdWVNdXRhdG9yKSB7XG4gICAgICAgIHV0aWxzLnBhdXNlRXZlbnQoZSk7XG4gICAgICAgIHZhciBzdGF0ZSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIHZhciBvbGRWYWx1ZSA9IHN0YXRlLnZhbHVlO1xuICAgICAgICB2YXIgbXV0YXRlZFZhbHVlID0gdmFsdWVNdXRhdG9yKG9sZFZhbHVlLCB0aGlzLnByb3BzKTtcbiAgICAgICAgdmFyIHZhbHVlID0gdGhpcy50cmltQWxpZ25WYWx1ZShtdXRhdGVkVmFsdWUpO1xuICAgICAgICBpZiAodmFsdWUgPT09IG9sZFZhbHVlKSByZXR1cm47XG5cbiAgICAgICAgdGhpcy5vbkNoYW5nZSh7IHZhbHVlOiB2YWx1ZSB9KTtcbiAgICAgICAgdGhpcy5wcm9wcy5vbkFmdGVyQ2hhbmdlKHZhbHVlKTtcbiAgICAgICAgdGhpcy5vbkVuZCgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldFZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0VmFsdWUoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdGF0ZS52YWx1ZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRMb3dlckJvdW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TG93ZXJCb3VuZCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnByb3BzLm1pbjtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRVcHBlckJvdW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0VXBwZXJCb3VuZCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnN0YXRlLnZhbHVlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3RyaW1BbGlnblZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdHJpbUFsaWduVmFsdWUodikge1xuICAgICAgdmFyIG5leHRQcm9wcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgICAgIGlmICh2ID09PSBudWxsKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgbWVyZ2VkUHJvcHMgPSBfZXh0ZW5kcyh7fSwgdGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIHZhciB2YWwgPSB1dGlscy5lbnN1cmVWYWx1ZUluUmFuZ2UodiwgbWVyZ2VkUHJvcHMpO1xuICAgICAgcmV0dXJuIHV0aWxzLmVuc3VyZVZhbHVlUHJlY2lzaW9uKHZhbCwgbWVyZ2VkUHJvcHMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzMyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzMy5wcmVmaXhDbHMsXG4gICAgICAgICAgdmVydGljYWwgPSBfcHJvcHMzLnZlcnRpY2FsLFxuICAgICAgICAgIGluY2x1ZGVkID0gX3Byb3BzMy5pbmNsdWRlZCxcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wczMuZGlzYWJsZWQsXG4gICAgICAgICAgbWluaW11bVRyYWNrU3R5bGUgPSBfcHJvcHMzLm1pbmltdW1UcmFja1N0eWxlLFxuICAgICAgICAgIHRyYWNrU3R5bGUgPSBfcHJvcHMzLnRyYWNrU3R5bGUsXG4gICAgICAgICAgaGFuZGxlU3R5bGUgPSBfcHJvcHMzLmhhbmRsZVN0eWxlLFxuICAgICAgICAgIHRhYkluZGV4ID0gX3Byb3BzMy50YWJJbmRleCxcbiAgICAgICAgICBtaW4gPSBfcHJvcHMzLm1pbixcbiAgICAgICAgICBtYXggPSBfcHJvcHMzLm1heCxcbiAgICAgICAgICByZXZlcnNlID0gX3Byb3BzMy5yZXZlcnNlLFxuICAgICAgICAgIGhhbmRsZUdlbmVyYXRvciA9IF9wcm9wczMuaGFuZGxlO1xuICAgICAgdmFyIF9zdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgdmFsdWUgPSBfc3RhdGUudmFsdWUsXG4gICAgICAgICAgZHJhZ2dpbmcgPSBfc3RhdGUuZHJhZ2dpbmc7XG5cbiAgICAgIHZhciBvZmZzZXQgPSB0aGlzLmNhbGNPZmZzZXQodmFsdWUpO1xuICAgICAgdmFyIGhhbmRsZSA9IGhhbmRsZUdlbmVyYXRvcih7XG4gICAgICAgIGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1oYW5kbGUnLFxuICAgICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgICAgdmVydGljYWw6IHZlcnRpY2FsLFxuICAgICAgICBvZmZzZXQ6IG9mZnNldCxcbiAgICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgICBkcmFnZ2luZzogZHJhZ2dpbmcsXG4gICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZCxcbiAgICAgICAgbWluOiBtaW4sXG4gICAgICAgIG1heDogbWF4LFxuICAgICAgICByZXZlcnNlOiByZXZlcnNlLFxuICAgICAgICBpbmRleDogMCxcbiAgICAgICAgdGFiSW5kZXg6IHRhYkluZGV4LFxuICAgICAgICBzdHlsZTogaGFuZGxlU3R5bGVbMF0gfHwgaGFuZGxlU3R5bGUsXG4gICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKGgpIHtcbiAgICAgICAgICByZXR1cm4gX3RoaXMyLnNhdmVIYW5kbGUoMCwgaCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICB2YXIgX3RyYWNrU3R5bGUgPSB0cmFja1N0eWxlWzBdIHx8IHRyYWNrU3R5bGU7XG4gICAgICB2YXIgdHJhY2sgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFRyYWNrLCB7XG4gICAgICAgIGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy10cmFjaycsXG4gICAgICAgIHZlcnRpY2FsOiB2ZXJ0aWNhbCxcbiAgICAgICAgaW5jbHVkZWQ6IGluY2x1ZGVkLFxuICAgICAgICBvZmZzZXQ6IDAsXG4gICAgICAgIHJldmVyc2U6IHJldmVyc2UsXG4gICAgICAgIGxlbmd0aDogb2Zmc2V0LFxuICAgICAgICBzdHlsZTogX2V4dGVuZHMoe30sIG1pbmltdW1UcmFja1N0eWxlLCBfdHJhY2tTdHlsZSlcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4geyB0cmFja3M6IHRyYWNrLCBoYW5kbGVzOiBoYW5kbGUgfTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gU2xpZGVyO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5TbGlkZXIucHJvcFR5cGVzID0ge1xuICBkZWZhdWx0VmFsdWU6IFByb3BUeXBlcy5udW1iZXIsXG4gIHZhbHVlOiBQcm9wVHlwZXMubnVtYmVyLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGF1dG9Gb2N1czogUHJvcFR5cGVzLmJvb2wsXG4gIHRhYkluZGV4OiBQcm9wVHlwZXMubnVtYmVyLFxuICByZXZlcnNlOiBQcm9wVHlwZXMuYm9vbCxcbiAgbWluOiBQcm9wVHlwZXMubnVtYmVyLFxuICBtYXg6IFByb3BUeXBlcy5udW1iZXJcbn07XG5cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlU2xpZGVyKFNsaWRlcik7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5CQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFqQkE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBCQTtBQXNCQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQUNBO0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBdERBO0FBQ0E7QUF3REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFZQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-slider/es/Slider.js
