__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DATE_FORMAT", function() { return DATE_FORMAT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formDateLayout", function() { return formDateLayout; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-router */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _quote_compare_all_steps__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../quote/compare/all-steps */ "./src/app/desk/quote/compare/all-steps.tsx");
/* harmony import */ var _desk_component_date__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @desk-component/date */ "./src/app/desk/component/date.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _desk_styles_common__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @desk-styles/common */ "./src/app/desk/styles/common.tsx");










var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/crosss-sell/pa.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n            .congratulation {\n            padding: 44px 0 54px 0;\n            text-align: center;\n            color: ", ";\n            i {\n                font-size: 100px;\n            }\n        }\n        \n         .paragraph {\n            font-size: 16px;\n            margin: 0 0 15px 0;\n            text-align: center;\n            color: rgba(0, 0, 0, 0.85);\n            padding: 0 10px;\n            .policy-no-link {\n                display: inline-block;\n                margin: 0 0 0 3px;\n            }\n            .pdf-link-list {\n                font-size: 14px;\n                margin: 10px 0;\n                .pdf-link-item {\n                    display: block;\n                    margin: 0 15px 10px 0;\n                    line-height: 20px;\n                }\n            }\n        }\n        .page-title--main {\n            font-size: 20px;\n            padding: 14px 24px 24px 24px;\n            text-align: center;\n            text-transform: uppercase;\n            font-weight: bold;\n            color: rgba(0, 0, 0, 0.85);\n            white-space: normal;\n        }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .mobile-sell {\n          .ant-row .ant-col:first-child {\n            text-align: left;\n            width: 100%;\n            padding-bottom: 5px;\n          }\n          .ant-row .ant-col:last-child {\n            text-align: left;\n            width: 100%;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}















var isMobile = _common__WEBPACK_IMPORTED_MODULE_16__["Utils"].getIsMobile();
var DATE_FORMAT = "DD/MM/YYYY";
var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};

var Pa =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(Pa, _ModelWidget);

  function Pa(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Pa);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Pa).call(this, props, context));

    _this.handleIssue =
    /*#__PURE__*/
    function () {
      var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(policy) {
        var callIssue;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                callIssue =
                /*#__PURE__*/
                function () {
                  var _ref2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
                  /*#__PURE__*/
                  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
                    var transId;
                    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            _this.setState({
                              loading: true
                            });

                            try {
                              transId = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(_this.props.match, "params.transId");
                              policy.ext._ui = {
                                web: {
                                  step: _quote_compare_all_steps__WEBPACK_IMPORTED_MODULE_19__["AllSteps"].ISSUE
                                }
                              };

                              _this.getHelpers().getAjax().post("/cart/".concat(transId, "/crosssell/issue"), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_2__["default"])({}, policy), {}).then(function (response) {
                                var _ref3 = response.body || [],
                                    respData = _ref3.respData;

                                var success = respData.success,
                                    suspended = respData.suspended,
                                    nextPolicy = respData.nextPolicy,
                                    message = respData.message,
                                    policyId = respData.policyId,
                                    policy = respData.policy;

                                if (!success && !suspended) {
                                  antd__WEBPACK_IMPORTED_MODULE_12__["notification"].error({
                                    message: message
                                  });
                                } else {
                                  if (success) {
                                    _this.setState({
                                      issued: true,
                                      policy: policy
                                    });

                                    _this.loadPdfList(policyId);

                                    _this.props.handleIssue && _this.props.handleIssue();
                                  } else if (suspended) {
                                    _this.setState({
                                      policy: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(respData, "policy"),
                                      suspended: true
                                    });

                                    _this.props.handleIssue && _this.props.handleIssue();
                                  }
                                }
                              }).catch(function (error) {});
                            } catch (error) {
                              console.error(error);
                            } finally {
                              _this.setState({
                                loading: false
                              });
                            }

                          case 2:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function callIssue() {
                    return _ref2.apply(this, arguments);
                  };
                }();

                _this.props.form.validateFields(function (err, fieldsValue) {
                  if (err) {
                    return;
                  }

                  callIssue();
                });

              case 2:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Pa, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        CrossSellCMI: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Pa.prototype), "initState", this).call(this), {
        issued: false,
        suspended: false,
        policy: this.props.policy,
        loading: false,
        pdfList: [],
        dob: {}
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var policy = this.state.policy;

      var dob = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(_model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy), "ext.dob");

      dob = _common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].toDate(dob));

      if (dob && dob.split("/").length === 3) {
        this.setState({
          dob: {
            day: dob.split("/")[0],
            month: dob.split("/")[1],
            year: dob.split("/")[2]
          }
        });
      }
    }
  }, {
    key: "renderPdfList",
    value: function renderPdfList() {
      var _this2 = this;

      var _this$state = this.state,
          pdfList = _this$state.pdfList,
          policy = _this$state.policy;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        style: {
          display: "inline-flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, (pdfList || []).map(function (pdf) {
        var displayName = pdf.displayName,
            name = pdf.name,
            fileName = pdf.fileName;
        var pdfUrl = _common__WEBPACK_IMPORTED_MODULE_16__["Ajax"].getServiceLocation("/policies/".concat(_this2.getValueFromModel("policyId") || policy.policyId, "/pdf/").concat(name, "?access_token=").concat(_common__WEBPACK_IMPORTED_MODULE_16__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_16__["Consts"].AUTH_KEY)));
        return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
          href: pdfUrl,
          className: "pdf-link-item",
          key: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 113
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
          type: "file-pdf",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 114
          },
          __self: this
        }), "".concat(displayName));
      }));
    }
  }, {
    key: "renderIssued",
    value: function renderIssued() {
      var policy = this.state.policy;
      var policyNo = policy.policyNo;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(StyleIssued, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "congratulation",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Icon"], {
        type: "check-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Thank you for insuring with us.").thai("ขอบคุณที่ทำประกันกับเรา.").my("ကြှနျုပျတို့နှငျ့အတူ insuring အတွက်ကျေးဇူးတင်ပါသည်.").getMessage(), " ", _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Your policy has been issued, policy number").thai("กรมธรรม์ของท่านได้รับการอนุมัติเรียบร้อยแล้ว หมายเลขกรมธรรมของท่านคือ ").my("သင့်ရဲ့မူဝါဒ, မူဝါဒသည်အရေအတွက်ကိုထုတ်ပေးလျက်ရှိသည် ").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
        className: "policy-no-link",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, policyNo)), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "paragraph",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "pdf-link-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, this.renderPdfList())));
    }
  }, {
    key: "renderSuspended",
    value: function renderSuspended() {
      var quoteNo = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(this.state.policy, "quoteNo");

      var policyNo = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(this.state.policy, "policyNo");

      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(StyleIssued, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 154
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "paragraph paragraph--main",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 155
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Thank you for insuring with us. The case has been sent to insurer for underwriting. Reference Number: ".concat(policyNo || quoteNo)).thai("\u0E02\u0E2D\u0E1A\u0E04\u0E38\u0E13\u0E17\u0E35\u0E48\u0E17\u0E33\u0E1B\u0E23\u0E30\u0E01\u0E31\u0E19\u0E01\u0E31\u0E1A\u0E40\u0E23\u0E32 \u0E01\u0E23\u0E13\u0E35\u0E16\u0E39\u0E01\u0E2A\u0E48\u0E07\u0E44\u0E1B\u0E22\u0E31\u0E07 \u0E1A\u0E23\u0E34\u0E29\u0E31\u0E17 \u0E1B\u0E23\u0E30\u0E01\u0E31\u0E19\u0E2A\u0E33\u0E2B\u0E23\u0E31\u0E1A\u0E01\u0E32\u0E23\u0E08\u0E31\u0E14\u0E08\u0E33\u0E2B\u0E19\u0E48\u0E32\u0E22 \u0E2B\u0E21\u0E32\u0E22\u0E40\u0E25\u0E02\u0E2D\u0E49\u0E32\u0E07\u0E2D\u0E34\u0E07: ".concat(policyNo || quoteNo)).my("\u1000\u103C\u103E\u1014\u103B\u102F\u1015\u103B\u1010\u102D\u102F\u1037\u1014\u103E\u1004\u103B\u1037\u1021\u1010\u1030 insuring \u1021\u1010\u103D\u1000\u103A\u1000\u103B\u1031\u1038\u1007\u1030\u1038\u1010\u1004\u103A\u1015\u102B\u101E\u100A\u103A\u104B \u1021\u1006\u102D\u102F\u1015\u102B\u1000\u102D\u1005\u1039\u1005\u1010\u103D\u1004\u103A underwriting \u1019\u103B\u102C\u1038\u1021\u1010\u103D\u1000\u103A\u1021\u102C\u1019\u1001\u1036\u1005\u1031\u1001\u103C\u1004\u103A\u1038\u1004\u103E\u102B\u1005\u1031\u101C\u103D\u103E\u1010\u103A\u1001\u1032\u1037\u101E\u100A\u103A\u104B \u1000\u102D\u102F\u1038\u1000\u102C\u1038\u1005\u101B\u102C\u1021\u101B\u1031\u1021\u1010\u103D\u1000\u103A: ".concat(policyNo || quoteNo)).getMessage()));
    }
  }, {
    key: "loadPdfList",
    value: function loadPdfList(policyId) {
      var _this3 = this;

      if (!policyId) {
        return;
      }

      this.getHelpers().getAjax().get("/policies/".concat(policyId, "/outputs"), {}, {}).then(function (response) {
        var respData = response.body.respData;

        _this3.setState({
          pdfList: respData
        });
      }).catch(function (error) {});
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(dataId) {
      return "ext.".concat(dataId);
    }
  }, {
    key: "renderDateRange",
    value: function renderDateRange() {
      var _this4 = this;

      var policy = this.state.policy;
      var form = this.props.form;
      var openEnd = Boolean(lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(_model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy), "openEnd"));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_styles_common__WEBPACK_IMPORTED_MODULE_23__["StyleDateRange"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 248
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_21__["FieldGroup"], {
        className: "date-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 249
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NDateFilter"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        size: "large",
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Start Date").thai("วันที่เริ่มต้นโดยสมัครใจ").getMessage(),
        propName: this.generatePropName("effDate"),
        format: _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT,
        onChangeStartDate: function onChangeStartDate(value) {
          if (!openEnd) {
            //TODO this will be refactor
            _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].setValue({
              model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy)
            }, _this4.generatePropName("expDate"), _common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].toDate(value).add(1, "years").format(_common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT), true);
            _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].setValue({
              model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy)
            }, _this4.generatePropName("effDate"), _common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].formatDate(_common__WEBPACK_IMPORTED_MODULE_16__["DateUtils"].toDate(value)), true);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }), !openEnd && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("span", {
        className: "to-date",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 285
        },
        __self: this
      }, "~"), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NDate"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        size: "large",
        disabled: true,
        format: _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].DATE_FORMAT.DATE_FORMAT,
        propName: this.generatePropName("expDate"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287
        },
        __self: this
      }))));
    }
  }, {
    key: "renderQuote",
    value: function renderQuote() {
      var _this5 = this;

      var policy = this.state.policy;
      var form = this.props.form;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 306
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: isMobile ? "mobile-sell" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 307
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_desk_component_date__WEBPACK_IMPORTED_MODULE_20__["DateText"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        defaultValue: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(this.state, "dob", {}),
        propName: this.generatePropName("dob"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 308
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NRadio"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        propName: this.generatePropName("gender"),
        required: true,
        tableName: "gender",
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Gender").thai("เพศ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 315
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NSelect"], {
        form: form,
        model: _model__WEBPACK_IMPORTED_MODULE_18__["Modeller"].asProxied(policy),
        propName: this.generatePropName("occupationClass"),
        tableName: "occuclass",
        required: true,
        size: "large",
        style: {
          width: "100%"
        },
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Occupation Class").thai("ชั้นอาชีพ").my("အလုပ်အကိုင်အဆင့်ဆင့်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 323
        },
        __self: this
      }), this.renderDateRange(), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Form"].Item, Object.assign({
        colon: false
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Consts"].FORM_ITEM_LAYOUT, {
        label: " ",
        className: "btn-wrap",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 338
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Button"], {
        type: "primary",
        style: {
          width: isMobile ? "100%" : "auto"
        },
        size: "large",
        onClick: function onClick() {
          return _this5.handleIssue(policy);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 339
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("Confirm And Issue").thai("ยืนยันการออกกรมธรรม์").my("အတည်ပြုပြီး Issue").getMessage()))));
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$state2 = this.state,
          suspended = _this$state2.suspended,
          issued = _this$state2.issued;
      if (!this.props.policy) return null;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.CrossSellCMI, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 358
        },
        __self: this
      }, issued ? this.renderIssued() : suspended ? this.renderSuspended() : this.renderQuote());
    }
  }]);

  return Pa;
}(_component__WEBPACK_IMPORTED_MODULE_14__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router__WEBPACK_IMPORTED_MODULE_13__["withRouter"])(antd__WEBPACK_IMPORTED_MODULE_12__["Form"].create()(Pa)));

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_22__["default"].getTheme(),
    BORDER_COLOR = _Theme$getTheme.BORDER_COLOR;

var StyleIssued = _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject2(), BORDER_COLOR);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2Nyb3Nzcy1zZWxsL3BhLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9jcm9zc3Mtc2VsbC9wYS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IG1vbWVudCwgeyBNb21lbnQgfSBmcm9tIFwibW9tZW50XCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuaW1wb3J0IHsgRm9ybSwgQnV0dG9uLCBTcGluLCBJY29uLCBub3RpZmljYXRpb24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgd2l0aFJvdXRlciwgUm91dGVDb21wb25lbnRQcm9wcyB9IGZyb20gXCJyZWFjdC1yb3V0ZXJcIjtcblxuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBBamF4LCBDb25zdHMsIFN0b3JhZ2UsIFV0aWxzLCBMYW5ndWFnZSwgRGF0ZVV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5EYXRlLCBORGF0ZUZpbHRlciwgTlJhZGlvLCBOU2VsZWN0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbGxlciB9IGZyb20gXCJAbW9kZWxcIjtcbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uLy4uL3F1b3RlL2NvbXBhcmUvYWxsLXN0ZXBzXCI7XG5pbXBvcnQgeyBEYXRlVGV4dCB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvZGF0ZVwiO1xuaW1wb3J0IHsgRmllbGRHcm91cCB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlc1wiO1xuaW1wb3J0IHsgU3R5bGVEYXRlUmFuZ2UgfSBmcm9tIFwiQGRlc2stc3R5bGVzL2NvbW1vblwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5leHBvcnQgdHlwZSBJUHJvcHMgPSB7XG4gIHBvbGljeTogYW55O1xuICBoYW5kbGVJc3N1ZT86ICgpID0+IHZvaWQ7XG59ICYgRm9ybUNvbXBvbmVudFByb3BzICYgUm91dGVDb21wb25lbnRQcm9wcztcbmV4cG9ydCBjb25zdCBEQVRFX0ZPUk1BVCA9IFwiREQvTU0vWVlZWVwiO1xuZXhwb3J0IGNvbnN0IGZvcm1EYXRlTGF5b3V0ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDEyIH0sXG4gICAgc206IHsgc3BhbjogMTIgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDEyIH0sXG4gICAgc206IHsgc3BhbjogMTIgfSxcbiAgfSxcbn07XG5cbmV4cG9ydCB0eXBlIElTdGF0ZSA9IHtcbiAgaXNzdWVkOiBib29sZWFuO1xuICBzdXNwZW5kZWQ6IGJvb2xlYW47XG4gIHBvbGljeTogYW55O1xuICBsb2FkaW5nOiBib29sZWFuO1xuICBwZGZMaXN0OiBhbnk7XG4gIGRvYjogYW55XG59O1xuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmV4cG9ydCB0eXBlIElDb21wb25lbnRzID0ge1xuICBDcm9zc1NlbGxDTUk6IFN0eWxlZERJVjtcbn07XG5cbmNsYXNzIFBhPFAgZXh0ZW5kcyBJUHJvcHMsXG4gIFMgZXh0ZW5kcyBJU3RhdGUsXG4gIEMgZXh0ZW5kcyBJQ29tcG9uZW50cz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIGNvbnN0cnVjdG9yKHByb3BzOiBJUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIENyb3NzU2VsbENNSTogU3R5bGVkLmRpdmBcbiAgICAgICAgLm1vYmlsZS1zZWxsIHtcbiAgICAgICAgICAuYW50LXJvdyAuYW50LWNvbDpmaXJzdC1jaGlsZCB7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICAuYW50LXJvdyAuYW50LWNvbDpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgaXNzdWVkOiBmYWxzZSxcbiAgICAgIHN1c3BlbmRlZDogZmFsc2UsXG4gICAgICBwb2xpY3k6IHRoaXMucHJvcHMucG9saWN5LFxuICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICBwZGZMaXN0OiBbXSxcbiAgICAgIGRvYjoge30sXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHsgcG9saWN5IH0gPSB0aGlzLnN0YXRlO1xuICAgIGxldCBkb2IgPSBfLmdldChNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KSwgXCJleHQuZG9iXCIpO1xuICAgIGRvYiA9IERhdGVVdGlscy5mb3JtYXREYXRlKERhdGVVdGlscy50b0RhdGUoZG9iKSk7XG4gICAgaWYgKGRvYiAmJiBkb2Iuc3BsaXQoXCIvXCIpLmxlbmd0aCA9PT0gMykge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGRvYjoge1xuICAgICAgICAgIGRheTogZG9iLnNwbGl0KFwiL1wiKVswXSxcbiAgICAgICAgICBtb250aDogZG9iLnNwbGl0KFwiL1wiKVsxXSxcbiAgICAgICAgICB5ZWFyOiBkb2Iuc3BsaXQoXCIvXCIpWzJdLFxuICAgICAgICB9LFxuICAgICAgfSBhcyBhbnkpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyUGRmTGlzdCgpIHtcbiAgICBjb25zdCB7IHBkZkxpc3QsIHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBzdHlsZT17eyBkaXNwbGF5OiBcImlubGluZS1mbGV4XCIgfX0+XG4gICAgICAgIHsocGRmTGlzdCB8fCBbXSkubWFwKChwZGY6IGFueSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgZGlzcGxheU5hbWUsIG5hbWUsIGZpbGVOYW1lIH0gPSBwZGY7XG4gICAgICAgICAgY29uc3QgcGRmVXJsID0gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oXG4gICAgICAgICAgICBgL3BvbGljaWVzLyR7dGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeUlkXCIpIHx8XG4gICAgICAgICAgICBwb2xpY3kucG9saWN5SWR9L3BkZi8ke25hbWV9P2FjY2Vzc190b2tlbj0ke1N0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSl9YCxcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8YSBocmVmPXtwZGZVcmx9IGNsYXNzTmFtZT1cInBkZi1saW5rLWl0ZW1cIiBrZXk9e2ZpbGVOYW1lfT5cbiAgICAgICAgICAgICAgPEljb24gdHlwZT1cImZpbGUtcGRmXCIvPlxuICAgICAgICAgICAgICB7YCR7ZGlzcGxheU5hbWV9YH1cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICApO1xuICAgICAgICB9KX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlcklzc3VlZCgpIHtcbiAgICBjb25zdCB7IHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IHBvbGljeU5vIH0gPSBwb2xpY3k7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPFN0eWxlSXNzdWVkPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbmdyYXR1bGF0aW9uXCI+XG4gICAgICAgICAgPEljb24gdHlwZT1cImNoZWNrLWNpcmNsZVwiLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFyYWdyYXBoIHBhcmFncmFwaC0tbWFpblwiPlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlRoYW5rIHlvdSBmb3IgaW5zdXJpbmcgd2l0aCB1cy5cIilcbiAgICAgICAgICAgIC50aGFpKFwi4LiC4Lit4Lia4LiE4Li44LiT4LiX4Li14LmI4LiX4Liz4Lib4Lij4Liw4LiB4Lix4LiZ4LiB4Lix4Lia4LmA4Lij4LiyLlwiKVxuICAgICAgICAgICAgLm15KFwi4YCA4YC84YC+4YCU4YC74YCv4YCV4YC74YCQ4YCt4YCv4YC34YCU4YC+4YCE4YC74YC34YCh4YCQ4YCwIGluc3VyaW5nIOGAoeGAkOGAveGAgOGAuuGAgOGAu+GAseGAuOGAh+GAsOGAuOGAkOGAhOGAuuGAleGAq+GAnuGAiuGAui5cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9e1wiIFwifVxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIllvdXIgcG9saWN5IGhhcyBiZWVuIGlzc3VlZCwgcG9saWN5IG51bWJlclwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguIHguKPguKHguJjguKPguKPguKHguYzguILguK3guIfguJfguYjguLLguJnguYTguJTguYnguKPguLHguJrguIHguLLguKPguK3guJnguLjguKHguLHguJXguLTguYDguKPguLXguKLguJrguKPguYnguK3guKLguYHguKXguYnguKcg4Lir4Lih4Liy4Lii4LmA4Lil4LiC4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LiC4Lit4LiH4LiX4LmI4Liy4LiZ4LiE4Li34LitIFwiKVxuICAgICAgICAgICAgLm15KFwi4YCe4YCE4YC64YC34YCb4YCy4YC34YCZ4YCw4YCd4YCr4YCSLCDhgJnhgLDhgJ3hgKvhgJLhgJ7hgIrhgLrhgKHhgJvhgLHhgKHhgJDhgL3hgIDhgLrhgIDhgK3hgK/hgJHhgK/hgJDhgLrhgJXhgLHhgLjhgJzhgLvhgIDhgLrhgJvhgL7hgK3hgJ7hgIrhgLogXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDxhIGNsYXNzTmFtZT1cInBvbGljeS1uby1saW5rXCI+e3BvbGljeU5vfTwvYT5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFyYWdyYXBoXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwZGYtbGluay1saXN0XCI+e3RoaXMucmVuZGVyUGRmTGlzdCgpfTwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvU3R5bGVJc3N1ZWQ+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyU3VzcGVuZGVkKCkge1xuICAgIGxldCBxdW90ZU5vID0gXy5nZXQodGhpcy5zdGF0ZS5wb2xpY3ksIFwicXVvdGVOb1wiKTtcbiAgICBsZXQgcG9saWN5Tm8gPSBfLmdldCh0aGlzLnN0YXRlLnBvbGljeSwgXCJwb2xpY3lOb1wiKTtcbiAgICByZXR1cm4gKFxuICAgICAgPFN0eWxlSXNzdWVkPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhcmFncmFwaCBwYXJhZ3JhcGgtLW1haW5cIj5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXG4gICAgICAgICAgICBgVGhhbmsgeW91IGZvciBpbnN1cmluZyB3aXRoIHVzLiBUaGUgY2FzZSBoYXMgYmVlbiBzZW50IHRvIGluc3VyZXIgZm9yIHVuZGVyd3JpdGluZy4gUmVmZXJlbmNlIE51bWJlcjogJHtwb2xpY3lObyB8fCBxdW90ZU5vfWAsXG4gICAgICAgICAgKVxuICAgICAgICAgICAgLnRoYWkoYOC4guC4reC4muC4hOC4uOC4k+C4l+C4teC5iOC4l+C4s+C4m+C4o+C4sOC4geC4seC4meC4geC4seC4muC5gOC4o+C4siDguIHguKPguJPguLXguJbguLnguIHguKrguYjguIfguYTguJvguKLguLHguIcg4Lia4Lij4Li04Lip4Lix4LiXIOC4m+C4o+C4sOC4geC4seC4meC4quC4s+C4q+C4o+C4seC4muC4geC4suC4o+C4iOC4seC4lOC4iOC4s+C4q+C4meC5iOC4suC4oiDguKvguKHguLLguKLguYDguKXguILguK3guYnguLLguIfguK3guLTguIc6ICR7cG9saWN5Tm8gfHwgcXVvdGVOb31gKVxuICAgICAgICAgICAgLm15KGDhgIDhgLzhgL7hgJThgLvhgK/hgJXhgLvhgJDhgK3hgK/hgLfhgJThgL7hgIThgLvhgLfhgKHhgJDhgLAgaW5zdXJpbmcg4YCh4YCQ4YC94YCA4YC64YCA4YC74YCx4YC44YCH4YCw4YC44YCQ4YCE4YC64YCV4YCr4YCe4YCK4YC64YGLIOGAoeGAhuGAreGAr+GAleGAq+GAgOGAreGAheGAueGAheGAkOGAveGAhOGAuiB1bmRlcndyaXRpbmcg4YCZ4YC74YCs4YC44YCh4YCQ4YC94YCA4YC64YCh4YCs4YCZ4YCB4YC24YCF4YCx4YCB4YC84YCE4YC64YC44YCE4YC+4YCr4YCF4YCx4YCc4YC94YC+4YCQ4YC64YCB4YCy4YC34YCe4YCK4YC64YGLIOGAgOGAreGAr+GAuOGAgOGArOGAuOGAheGAm+GArOGAoeGAm+GAseGAoeGAkOGAveGAgOGAujogJHtwb2xpY3lObyB8fCBxdW90ZU5vfWApXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvU3R5bGVJc3N1ZWQ+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgbG9hZFBkZkxpc3QocG9saWN5SWQ6IGFueSkge1xuICAgIGlmICghcG9saWN5SWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgIC5nZXRBamF4KClcbiAgICAgIC5nZXQoYC9wb2xpY2llcy8ke3BvbGljeUlkfS9vdXRwdXRzYCwge30sIHt9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgcGRmTGlzdDogcmVzcERhdGEsXG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgaGFuZGxlSXNzdWUgPSBhc3luYyAocG9saWN5OiBhbnkpID0+IHtcbiAgICBjb25zdCBjYWxsSXNzdWUgPSBhc3luYyAoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgbG9hZGluZzogdHJ1ZSB9KTtcbiAgICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IHRyYW5zSWQgPSBfLmdldCh0aGlzLnByb3BzLm1hdGNoLCBcInBhcmFtcy50cmFuc0lkXCIpO1xuICAgICAgICBwb2xpY3kuZXh0Ll91aSA9IHsgd2ViOiB7IHN0ZXA6IEFsbFN0ZXBzLklTU1VFIH0gfTtcbiAgICAgICAgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgLnBvc3QoXG4gICAgICAgICAgICBgL2NhcnQvJHt0cmFuc0lkfS9jcm9zc3NlbGwvaXNzdWVgLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAuLi5wb2xpY3ksXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge30sXG4gICAgICAgICAgKVxuICAgICAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5IHx8IFtdO1xuICAgICAgICAgICAgY29uc3QgeyBzdWNjZXNzLCBzdXNwZW5kZWQsIG5leHRQb2xpY3ksIG1lc3NhZ2UsIHBvbGljeUlkLCBwb2xpY3kgfSA9IHJlc3BEYXRhO1xuICAgICAgICAgICAgaWYgKCFzdWNjZXNzICYmICFzdXNwZW5kZWQpIHtcbiAgICAgICAgICAgICAgbm90aWZpY2F0aW9uLmVycm9yKHsgbWVzc2FnZTogbWVzc2FnZSB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGlmIChzdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBpc3N1ZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICBwb2xpY3k6IHBvbGljeSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRQZGZMaXN0KHBvbGljeUlkKTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmhhbmRsZUlzc3VlICYmIHRoaXMucHJvcHMuaGFuZGxlSXNzdWUoKTtcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChzdXNwZW5kZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgIHBvbGljeTogXy5nZXQocmVzcERhdGEsIFwicG9saWN5XCIpLFxuICAgICAgICAgICAgICAgICAgc3VzcGVuZGVkOiB0cnVlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuaGFuZGxlSXNzdWUgJiYgdGhpcy5wcm9wcy5oYW5kbGVJc3N1ZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcbiAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgIH0pO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICB9IGZpbmFsbHkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfTtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGNhbGxJc3N1ZSgpO1xuICAgIH0pO1xuICB9O1xuXG4gIGdlbmVyYXRlUHJvcE5hbWUoZGF0YUlkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gYGV4dC4ke2RhdGFJZH1gO1xuICB9XG5cbiAgcmVuZGVyRGF0ZVJhbmdlKCkge1xuICAgIGNvbnN0IHsgcG9saWN5IH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgZm9ybSB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBvcGVuRW5kID0gQm9vbGVhbihfLmdldChNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KSwgXCJvcGVuRW5kXCIpKTtcblxuICAgIHJldHVybiA8U3R5bGVEYXRlUmFuZ2U+XG4gICAgICA8RmllbGRHcm91cCBjbGFzc05hbWU9XCJkYXRlLWdyb3VwXCIgc2VsZWN0WHNTbT17eyB4czogOCwgc206IDYgfX0gdGV4dFhzU209e3sgeHM6IDE2LCBzbTogMTMgfX0+XG4gICAgICAgIDxORGF0ZUZpbHRlclxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbW9kZWw9e01vZGVsbGVyLmFzUHJveGllZChwb2xpY3kpfVxuICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJTdGFydCBEYXRlXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4p+C4seC4meC4l+C4teC5iOC5gOC4o+C4tOC5iOC4oeC4leC5ieC4meC5guC4lOC4ouC4quC4oeC4seC4hOC4o+C5g+C4iFwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cblxuICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJlZmZEYXRlXCIpfVxuICAgICAgICAgIGZvcm1hdD17Q29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUfVxuXG4gICAgICAgICAgb25DaGFuZ2VTdGFydERhdGU9eyh2YWx1ZTogTW9tZW50KSA9PiB7XG4gICAgICAgICAgICBpZiAoIW9wZW5FbmQpIHtcbiAgICAgICAgICAgICAgLy9UT0RPIHRoaXMgd2lsbCBiZSByZWZhY3RvclxuICAgICAgICAgICAgICBNb2RlbGxlci5zZXRWYWx1ZShcbiAgICAgICAgICAgICAgICB7IG1vZGVsOiBNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KSB9LFxuICAgICAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImV4cERhdGVcIiksXG4gICAgICAgICAgICAgICAgRGF0ZVV0aWxzLnRvRGF0ZSh2YWx1ZSlcbiAgICAgICAgICAgICAgICAgIC5hZGQoMSwgXCJ5ZWFyc1wiKVxuICAgICAgICAgICAgICAgICAgLmZvcm1hdChDb25zdHMuREFURV9GT1JNQVQuREFURV9GT1JNQVQpLFxuICAgICAgICAgICAgICAgIHRydWUsXG4gICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgIE1vZGVsbGVyLnNldFZhbHVlKFxuICAgICAgICAgICAgICAgIHsgbW9kZWw6IE1vZGVsbGVyLmFzUHJveGllZChwb2xpY3kpIH0sXG4gICAgICAgICAgICAgICAgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiZWZmRGF0ZVwiKSxcbiAgICAgICAgICAgICAgICBEYXRlVXRpbHMuZm9ybWF0RGF0ZShEYXRlVXRpbHMudG9EYXRlKHZhbHVlKSksXG4gICAgICAgICAgICAgICAgdHJ1ZSxcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9fVxuICAgICAgICAvPlxuXG4gICAgICAgIHshb3BlbkVuZCAmJiAoXG4gICAgICAgICAgPD5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInRvLWRhdGVcIj5+PC9zcGFuPlxuXG4gICAgICAgICAgICA8TkRhdGVcbiAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbW9kZWw9e01vZGVsbGVyLmFzUHJveGllZChwb2xpY3kpfVxuICAgICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICAgIGRpc2FibGVkPXt0cnVlfVxuICAgICAgICAgICAgICBmb3JtYXQ9e0NvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX0ZPUk1BVH1cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImV4cERhdGVcIil9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvPlxuICAgICAgICApfVxuICAgICAgPC9GaWVsZEdyb3VwPlxuICAgIDwvU3R5bGVEYXRlUmFuZ2U+O1xuICB9XG5cbiAgcHJpdmF0ZSByZW5kZXJRdW90ZSgpIHtcbiAgICBjb25zdCB7IHBvbGljeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGZvcm0gfSA9IHRoaXMucHJvcHM7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPFNwaW4gc2l6ZT1cImxhcmdlXCIgc3Bpbm5pbmc9e3RoaXMuc3RhdGUubG9hZGluZ30+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtpc01vYmlsZSA/IFwibW9iaWxlLXNlbGxcIiA6IFwiXCJ9PlxuICAgICAgICAgIDxEYXRlVGV4dFxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17Xy5nZXQodGhpcy5zdGF0ZSwgXCJkb2JcIiwge30pfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImRvYlwiKX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPE5SYWRpb1xuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJnZW5kZXJcIil9XG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIHRhYmxlTmFtZT1cImdlbmRlclwiXG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJHZW5kZXJcIikudGhhaShcIuC5gOC4nuC4qFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8TlNlbGVjdFxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXtNb2RlbGxlci5hc1Byb3hpZWQocG9saWN5KX1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJvY2N1cGF0aW9uQ2xhc3NcIil9XG4gICAgICAgICAgICB0YWJsZU5hbWU9XCJvY2N1Y2xhc3NcIlxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJPY2N1cGF0aW9uIENsYXNzXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4LiK4Lix4LmJ4LiZ4Lit4Liy4LiK4Li14LieXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAoeGAnOGAr+GAleGAuuGAoeGAgOGAreGAr+GAhOGAuuGAoeGAhuGAhOGAuuGAt+GAhuGAhOGAuuGAt1wiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIC8+XG4gICAgICAgICAge3RoaXMucmVuZGVyRGF0ZVJhbmdlKCl9XG5cbiAgICAgICAgICA8Rm9ybS5JdGVtIGNvbG9uPXtmYWxzZX0gey4uLkNvbnN0cy5GT1JNX0lURU1fTEFZT1VUfSBsYWJlbD1cIiBcIiBjbGFzc05hbWU9XCJidG4td3JhcFwiPlxuICAgICAgICAgICAgPEJ1dHRvbiB0eXBlPVwicHJpbWFyeVwiIHN0eWxlPXt7IHdpZHRoOiBpc01vYmlsZSA/IFwiMTAwJVwiIDogXCJhdXRvXCIgfX0gc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVJc3N1ZShwb2xpY3kpfT5cbiAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiQ29uZmlybSBBbmQgSXNzdWVcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4ouC4t+C4meC4ouC4seC4meC4geC4suC4o+C4reC4reC4geC4geC4o+C4oeC4mOC4o+C4o+C4oeC5jFwiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAoeGAkOGAiuGAuuGAleGAvOGAr+GAleGAvOGAruGAuCBJc3N1ZVwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICA8L0Zvcm0uSXRlbT5cblxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvU3Bpbj5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IHN1c3BlbmRlZCwgaXNzdWVkIH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmICghdGhpcy5wcm9wcy5wb2xpY3kpIHJldHVybiBudWxsO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Dcm9zc1NlbGxDTUk+XG4gICAgICAgIHtpc3N1ZWQgPyB0aGlzLnJlbmRlcklzc3VlZCgpIDogc3VzcGVuZGVkID8gdGhpcy5yZW5kZXJTdXNwZW5kZWQoKSA6IHRoaXMucmVuZGVyUXVvdGUoKX1cbiAgICAgIDwvQy5Dcm9zc1NlbGxDTUk+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoUm91dGVyKEZvcm0uY3JlYXRlPElQcm9wcz4oKShQYSkpO1xuXG5cbmNvbnN0IHsgQk9SREVSX0NPTE9SIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuY29uc3QgU3R5bGVJc3N1ZWQgPSBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgLmNvbmdyYXR1bGF0aW9uIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDQ0cHggMCA1NHB4IDA7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBjb2xvcjogJHtCT1JERVJfQ09MT1J9O1xuICAgICAgICAgICAgaSB7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgIC5wYXJhZ3JhcGgge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIDAgMTVweCAwO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44NSk7XG4gICAgICAgICAgICBwYWRkaW5nOiAwIDEwcHg7XG4gICAgICAgICAgICAucG9saWN5LW5vLWxpbmsge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgICBtYXJnaW46IDAgMCAwIDNweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5wZGYtbGluay1saXN0IHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAxMHB4IDA7XG4gICAgICAgICAgICAgICAgLnBkZi1saW5rLWl0ZW0ge1xuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIDE1cHggMTBweCAwO1xuICAgICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLnBhZ2UtdGl0bGUtLW1haW4ge1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgcGFkZGluZzogMTRweCAyNHB4IDI0cHggMjRweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICAgICAgfVxuICAgIGA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBd0JBOzs7OztBQUdBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa0lBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFTQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUE3Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUE4Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQXJEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBbklBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQURBO0FBZUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBREE7QUFPQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7OztBQUVBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFHQTs7O0FBeURBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUdBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQU9BO0FBQ0E7QUFBQTtBQUtBO0FBQ0E7QUE5QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWUE7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBOzs7QUFFQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7O0FBclRBO0FBQ0E7QUF1VEE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/crosss-sell/pa.tsx
