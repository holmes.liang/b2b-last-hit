__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}


 // eslint-disable-next-line react/prefer-stateless-function

var SkeletonAvatar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SkeletonAvatar, _React$Component);

  function SkeletonAvatar() {
    _classCallCheck(this, SkeletonAvatar);

    return _possibleConstructorReturn(this, _getPrototypeOf(SkeletonAvatar).apply(this, arguments));
  }

  _createClass(SkeletonAvatar, [{
    key: "render",
    value: function render() {
      var _classNames, _classNames2;

      var _this$props = this.props,
          prefixCls = _this$props.prefixCls,
          className = _this$props.className,
          style = _this$props.style,
          size = _this$props.size,
          shape = _this$props.shape;
      var sizeCls = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-lg"), size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-sm"), size === 'small'), _classNames));
      var shapeCls = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames2 = {}, _defineProperty(_classNames2, "".concat(prefixCls, "-circle"), shape === 'circle'), _defineProperty(_classNames2, "".concat(prefixCls, "-square"), shape === 'square'), _classNames2));
      var sizeStyle = typeof size === 'number' ? {
        width: size,
        height: size,
        lineHeight: "".concat(size, "px")
      } : {};
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className, sizeCls, shapeCls),
        style: _extends(_extends({}, sizeStyle), style)
      });
    }
  }]);

  return SkeletonAvatar;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

SkeletonAvatar.defaultProps = {
  size: 'large'
};
/* harmony default export */ __webpack_exports__["default"] = (SkeletonAvatar);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9za2VsZXRvbi9BdmF0YXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NrZWxldG9uL0F2YXRhci5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcmVhY3QvcHJlZmVyLXN0YXRlbGVzcy1mdW5jdGlvblxuY2xhc3MgU2tlbGV0b25BdmF0YXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgeyBwcmVmaXhDbHMsIGNsYXNzTmFtZSwgc3R5bGUsIHNpemUsIHNoYXBlIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBzaXplQ2xzID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1sZ2BdOiBzaXplID09PSAnbGFyZ2UnLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc21gXTogc2l6ZSA9PT0gJ3NtYWxsJyxcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnN0IHNoYXBlQ2xzID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jaXJjbGVgXTogc2hhcGUgPT09ICdjaXJjbGUnLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc3F1YXJlYF06IHNoYXBlID09PSAnc3F1YXJlJyxcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnN0IHNpemVTdHlsZSA9IHR5cGVvZiBzaXplID09PSAnbnVtYmVyJ1xuICAgICAgICAgICAgPyB7XG4gICAgICAgICAgICAgICAgd2lkdGg6IHNpemUsXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBzaXplLFxuICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IGAke3NpemV9cHhgLFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgOiB7fTtcbiAgICAgICAgcmV0dXJuICg8c3BhbiBjbGFzc05hbWU9e2NsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUsIHNpemVDbHMsIHNoYXBlQ2xzKX0gc3R5bGU9e09iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgc2l6ZVN0eWxlKSwgc3R5bGUpfS8+KTtcbiAgICB9XG59XG5Ta2VsZXRvbkF2YXRhci5kZWZhdWx0UHJvcHMgPSB7XG4gICAgc2l6ZTogJ2xhcmdlJyxcbn07XG5leHBvcnQgZGVmYXVsdCBTa2VsZXRvbkF2YXRhcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFJQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7O0FBbkJBO0FBQ0E7QUFvQkE7QUFDQTtBQURBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/skeleton/Avatar.js
