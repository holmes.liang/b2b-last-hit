/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule removeEntitiesAtEdges
 * @format
 * 
 */


var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var findRangesImmutable = __webpack_require__(/*! ./findRangesImmutable */ "./node_modules/draft-js/lib/findRangesImmutable.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

function removeEntitiesAtEdges(contentState, selectionState) {
  var blockMap = contentState.getBlockMap();
  var entityMap = contentState.getEntityMap();
  var updatedBlocks = {};
  var startKey = selectionState.getStartKey();
  var startOffset = selectionState.getStartOffset();
  var startBlock = blockMap.get(startKey);
  var updatedStart = removeForBlock(entityMap, startBlock, startOffset);

  if (updatedStart !== startBlock) {
    updatedBlocks[startKey] = updatedStart;
  }

  var endKey = selectionState.getEndKey();
  var endOffset = selectionState.getEndOffset();
  var endBlock = blockMap.get(endKey);

  if (startKey === endKey) {
    endBlock = updatedStart;
  }

  var updatedEnd = removeForBlock(entityMap, endBlock, endOffset);

  if (updatedEnd !== endBlock) {
    updatedBlocks[endKey] = updatedEnd;
  }

  if (!Object.keys(updatedBlocks).length) {
    return contentState.set('selectionAfter', selectionState);
  }

  return contentState.merge({
    blockMap: blockMap.merge(updatedBlocks),
    selectionAfter: selectionState
  });
}

function getRemovalRange(characters, key, offset) {
  var removalRange;
  findRangesImmutable(characters, function (a, b) {
    return a.getEntity() === b.getEntity();
  }, function (element) {
    return element.getEntity() === key;
  }, function (start, end) {
    if (start <= offset && end >= offset) {
      removalRange = {
        start: start,
        end: end
      };
    }
  });
  !(typeof removalRange === 'object') ?  true ? invariant(false, 'Removal range must exist within character list.') : undefined : void 0;
  return removalRange;
}

function removeForBlock(entityMap, block, offset) {
  var chars = block.getCharacterList();
  var charBefore = offset > 0 ? chars.get(offset - 1) : undefined;
  var charAfter = offset < chars.count() ? chars.get(offset) : undefined;
  var entityBeforeCursor = charBefore ? charBefore.getEntity() : undefined;
  var entityAfterCursor = charAfter ? charAfter.getEntity() : undefined;

  if (entityAfterCursor && entityAfterCursor === entityBeforeCursor) {
    var entity = entityMap.__get(entityAfterCursor);

    if (entity.getMutability() !== 'MUTABLE') {
      var _getRemovalRange = getRemovalRange(chars, entityAfterCursor, offset),
          start = _getRemovalRange.start,
          end = _getRemovalRange.end;

      var current;

      while (start < end) {
        current = chars.get(start);
        chars = chars.set(start, CharacterMetadata.applyEntity(current, null));
        start++;
      }

      return block.set('characterList', chars);
    }
  }

  return block;
}

module.exports = removeEntitiesAtEdges;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL3JlbW92ZUVudGl0aWVzQXRFZGdlcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9yZW1vdmVFbnRpdGllc0F0RWRnZXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSByZW1vdmVFbnRpdGllc0F0RWRnZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xuXG52YXIgZmluZFJhbmdlc0ltbXV0YWJsZSA9IHJlcXVpcmUoJy4vZmluZFJhbmdlc0ltbXV0YWJsZScpO1xudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG5mdW5jdGlvbiByZW1vdmVFbnRpdGllc0F0RWRnZXMoY29udGVudFN0YXRlLCBzZWxlY3Rpb25TdGF0ZSkge1xuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIGVudGl0eU1hcCA9IGNvbnRlbnRTdGF0ZS5nZXRFbnRpdHlNYXAoKTtcblxuICB2YXIgdXBkYXRlZEJsb2NrcyA9IHt9O1xuXG4gIHZhciBzdGFydEtleSA9IHNlbGVjdGlvblN0YXRlLmdldFN0YXJ0S2V5KCk7XG4gIHZhciBzdGFydE9mZnNldCA9IHNlbGVjdGlvblN0YXRlLmdldFN0YXJ0T2Zmc2V0KCk7XG4gIHZhciBzdGFydEJsb2NrID0gYmxvY2tNYXAuZ2V0KHN0YXJ0S2V5KTtcbiAgdmFyIHVwZGF0ZWRTdGFydCA9IHJlbW92ZUZvckJsb2NrKGVudGl0eU1hcCwgc3RhcnRCbG9jaywgc3RhcnRPZmZzZXQpO1xuXG4gIGlmICh1cGRhdGVkU3RhcnQgIT09IHN0YXJ0QmxvY2spIHtcbiAgICB1cGRhdGVkQmxvY2tzW3N0YXJ0S2V5XSA9IHVwZGF0ZWRTdGFydDtcbiAgfVxuXG4gIHZhciBlbmRLZXkgPSBzZWxlY3Rpb25TdGF0ZS5nZXRFbmRLZXkoKTtcbiAgdmFyIGVuZE9mZnNldCA9IHNlbGVjdGlvblN0YXRlLmdldEVuZE9mZnNldCgpO1xuICB2YXIgZW5kQmxvY2sgPSBibG9ja01hcC5nZXQoZW5kS2V5KTtcbiAgaWYgKHN0YXJ0S2V5ID09PSBlbmRLZXkpIHtcbiAgICBlbmRCbG9jayA9IHVwZGF0ZWRTdGFydDtcbiAgfVxuXG4gIHZhciB1cGRhdGVkRW5kID0gcmVtb3ZlRm9yQmxvY2soZW50aXR5TWFwLCBlbmRCbG9jaywgZW5kT2Zmc2V0KTtcblxuICBpZiAodXBkYXRlZEVuZCAhPT0gZW5kQmxvY2spIHtcbiAgICB1cGRhdGVkQmxvY2tzW2VuZEtleV0gPSB1cGRhdGVkRW5kO1xuICB9XG5cbiAgaWYgKCFPYmplY3Qua2V5cyh1cGRhdGVkQmxvY2tzKS5sZW5ndGgpIHtcbiAgICByZXR1cm4gY29udGVudFN0YXRlLnNldCgnc2VsZWN0aW9uQWZ0ZXInLCBzZWxlY3Rpb25TdGF0ZSk7XG4gIH1cblxuICByZXR1cm4gY29udGVudFN0YXRlLm1lcmdlKHtcbiAgICBibG9ja01hcDogYmxvY2tNYXAubWVyZ2UodXBkYXRlZEJsb2NrcyksXG4gICAgc2VsZWN0aW9uQWZ0ZXI6IHNlbGVjdGlvblN0YXRlXG4gIH0pO1xufVxuXG5mdW5jdGlvbiBnZXRSZW1vdmFsUmFuZ2UoY2hhcmFjdGVycywga2V5LCBvZmZzZXQpIHtcbiAgdmFyIHJlbW92YWxSYW5nZTtcbiAgZmluZFJhbmdlc0ltbXV0YWJsZShjaGFyYWN0ZXJzLCBmdW5jdGlvbiAoYSwgYikge1xuICAgIHJldHVybiBhLmdldEVudGl0eSgpID09PSBiLmdldEVudGl0eSgpO1xuICB9LCBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgIHJldHVybiBlbGVtZW50LmdldEVudGl0eSgpID09PSBrZXk7XG4gIH0sIGZ1bmN0aW9uIChzdGFydCwgZW5kKSB7XG4gICAgaWYgKHN0YXJ0IDw9IG9mZnNldCAmJiBlbmQgPj0gb2Zmc2V0KSB7XG4gICAgICByZW1vdmFsUmFuZ2UgPSB7IHN0YXJ0OiBzdGFydCwgZW5kOiBlbmQgfTtcbiAgICB9XG4gIH0pO1xuICAhKHR5cGVvZiByZW1vdmFsUmFuZ2UgPT09ICdvYmplY3QnKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdSZW1vdmFsIHJhbmdlIG11c3QgZXhpc3Qgd2l0aGluIGNoYXJhY3RlciBsaXN0LicpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgcmV0dXJuIHJlbW92YWxSYW5nZTtcbn1cblxuZnVuY3Rpb24gcmVtb3ZlRm9yQmxvY2soZW50aXR5TWFwLCBibG9jaywgb2Zmc2V0KSB7XG4gIHZhciBjaGFycyA9IGJsb2NrLmdldENoYXJhY3Rlckxpc3QoKTtcbiAgdmFyIGNoYXJCZWZvcmUgPSBvZmZzZXQgPiAwID8gY2hhcnMuZ2V0KG9mZnNldCAtIDEpIDogdW5kZWZpbmVkO1xuICB2YXIgY2hhckFmdGVyID0gb2Zmc2V0IDwgY2hhcnMuY291bnQoKSA/IGNoYXJzLmdldChvZmZzZXQpIDogdW5kZWZpbmVkO1xuICB2YXIgZW50aXR5QmVmb3JlQ3Vyc29yID0gY2hhckJlZm9yZSA/IGNoYXJCZWZvcmUuZ2V0RW50aXR5KCkgOiB1bmRlZmluZWQ7XG4gIHZhciBlbnRpdHlBZnRlckN1cnNvciA9IGNoYXJBZnRlciA/IGNoYXJBZnRlci5nZXRFbnRpdHkoKSA6IHVuZGVmaW5lZDtcblxuICBpZiAoZW50aXR5QWZ0ZXJDdXJzb3IgJiYgZW50aXR5QWZ0ZXJDdXJzb3IgPT09IGVudGl0eUJlZm9yZUN1cnNvcikge1xuICAgIHZhciBlbnRpdHkgPSBlbnRpdHlNYXAuX19nZXQoZW50aXR5QWZ0ZXJDdXJzb3IpO1xuICAgIGlmIChlbnRpdHkuZ2V0TXV0YWJpbGl0eSgpICE9PSAnTVVUQUJMRScpIHtcbiAgICAgIHZhciBfZ2V0UmVtb3ZhbFJhbmdlID0gZ2V0UmVtb3ZhbFJhbmdlKGNoYXJzLCBlbnRpdHlBZnRlckN1cnNvciwgb2Zmc2V0KSxcbiAgICAgICAgICBzdGFydCA9IF9nZXRSZW1vdmFsUmFuZ2Uuc3RhcnQsXG4gICAgICAgICAgZW5kID0gX2dldFJlbW92YWxSYW5nZS5lbmQ7XG5cbiAgICAgIHZhciBjdXJyZW50O1xuICAgICAgd2hpbGUgKHN0YXJ0IDwgZW5kKSB7XG4gICAgICAgIGN1cnJlbnQgPSBjaGFycy5nZXQoc3RhcnQpO1xuICAgICAgICBjaGFycyA9IGNoYXJzLnNldChzdGFydCwgQ2hhcmFjdGVyTWV0YWRhdGEuYXBwbHlFbnRpdHkoY3VycmVudCwgbnVsbCkpO1xuICAgICAgICBzdGFydCsrO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGJsb2NrLnNldCgnY2hhcmFjdGVyTGlzdCcsIGNoYXJzKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gYmxvY2s7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gcmVtb3ZlRW50aXRpZXNBdEVkZ2VzOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/removeEntitiesAtEdges.js
