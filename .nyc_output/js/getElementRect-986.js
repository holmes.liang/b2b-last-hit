
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

var containsNode = __webpack_require__(/*! ./containsNode */ "./node_modules/fbjs/lib/containsNode.js");
/**
 * Gets an element's bounding rect in pixels relative to the viewport.
 *
 * @param {DOMElement} elem
 * @return {object}
 */


function getElementRect(elem) {
  var docElem = elem.ownerDocument.documentElement; // FF 2, Safari 3 and Opera 9.5- do not support getBoundingClientRect().
  // IE9- will throw if the element is not in the document.

  if (!('getBoundingClientRect' in elem) || !containsNode(docElem, elem)) {
    return {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    };
  } // Subtracts clientTop/Left because IE8- added a 2px border to the
  // <html> element (see http://fburl.com/1493213). IE 7 in
  // Quicksmode does not report clientLeft/clientTop so there
  // will be an unaccounted offset of 2px when in quirksmode


  var rect = elem.getBoundingClientRect();
  return {
    left: Math.round(rect.left) - docElem.clientLeft,
    right: Math.round(rect.right) - docElem.clientLeft,
    top: Math.round(rect.top) - docElem.clientTop,
    bottom: Math.round(rect.bottom) - docElem.clientTop
  };
}

module.exports = getElementRect;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0RWxlbWVudFJlY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9nZXRFbGVtZW50UmVjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqL1xuXG52YXIgY29udGFpbnNOb2RlID0gcmVxdWlyZSgnLi9jb250YWluc05vZGUnKTtcblxuLyoqXG4gKiBHZXRzIGFuIGVsZW1lbnQncyBib3VuZGluZyByZWN0IGluIHBpeGVscyByZWxhdGl2ZSB0byB0aGUgdmlld3BvcnQuXG4gKlxuICogQHBhcmFtIHtET01FbGVtZW50fSBlbGVtXG4gKiBAcmV0dXJuIHtvYmplY3R9XG4gKi9cbmZ1bmN0aW9uIGdldEVsZW1lbnRSZWN0KGVsZW0pIHtcbiAgdmFyIGRvY0VsZW0gPSBlbGVtLm93bmVyRG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuXG4gIC8vIEZGIDIsIFNhZmFyaSAzIGFuZCBPcGVyYSA5LjUtIGRvIG5vdCBzdXBwb3J0IGdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLlxuICAvLyBJRTktIHdpbGwgdGhyb3cgaWYgdGhlIGVsZW1lbnQgaXMgbm90IGluIHRoZSBkb2N1bWVudC5cbiAgaWYgKCEoJ2dldEJvdW5kaW5nQ2xpZW50UmVjdCcgaW4gZWxlbSkgfHwgIWNvbnRhaW5zTm9kZShkb2NFbGVtLCBlbGVtKSkge1xuICAgIHJldHVybiB7XG4gICAgICBsZWZ0OiAwLFxuICAgICAgcmlnaHQ6IDAsXG4gICAgICB0b3A6IDAsXG4gICAgICBib3R0b206IDBcbiAgICB9O1xuICB9XG5cbiAgLy8gU3VidHJhY3RzIGNsaWVudFRvcC9MZWZ0IGJlY2F1c2UgSUU4LSBhZGRlZCBhIDJweCBib3JkZXIgdG8gdGhlXG4gIC8vIDxodG1sPiBlbGVtZW50IChzZWUgaHR0cDovL2ZidXJsLmNvbS8xNDkzMjEzKS4gSUUgNyBpblxuICAvLyBRdWlja3Ntb2RlIGRvZXMgbm90IHJlcG9ydCBjbGllbnRMZWZ0L2NsaWVudFRvcCBzbyB0aGVyZVxuICAvLyB3aWxsIGJlIGFuIHVuYWNjb3VudGVkIG9mZnNldCBvZiAycHggd2hlbiBpbiBxdWlya3Ntb2RlXG4gIHZhciByZWN0ID0gZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcblxuICByZXR1cm4ge1xuICAgIGxlZnQ6IE1hdGgucm91bmQocmVjdC5sZWZ0KSAtIGRvY0VsZW0uY2xpZW50TGVmdCxcbiAgICByaWdodDogTWF0aC5yb3VuZChyZWN0LnJpZ2h0KSAtIGRvY0VsZW0uY2xpZW50TGVmdCxcbiAgICB0b3A6IE1hdGgucm91bmQocmVjdC50b3ApIC0gZG9jRWxlbS5jbGllbnRUb3AsXG4gICAgYm90dG9tOiBNYXRoLnJvdW5kKHJlY3QuYm90dG9tKSAtIGRvY0VsZW0uY2xpZW50VG9wXG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0RWxlbWVudFJlY3Q7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7Ozs7QUFTQTtBQUVBOzs7Ozs7OztBQU1BO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getElementRect.js
