__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buffer", function() { return buffer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSamePoint", function() { return isSamePoint; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isWindow", function() { return isWindow; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSimilarValue", function() { return isSimilarValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "restoreFocus", function() { return restoreFocus; });
/* harmony import */ var rc_util_es_Dom_contains__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-util/es/Dom/contains */ "./node_modules/rc-util/es/Dom/contains.js");

function buffer(fn, ms) {
  var timer = void 0;

  function clear() {
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
  }

  function bufferFn() {
    clear();
    timer = setTimeout(fn, ms);
  }

  bufferFn.clear = clear;
  return bufferFn;
}
function isSamePoint(prev, next) {
  if (prev === next) return true;
  if (!prev || !next) return false;

  if ('pageX' in next && 'pageY' in next) {
    return prev.pageX === next.pageX && prev.pageY === next.pageY;
  }

  if ('clientX' in next && 'clientY' in next) {
    return prev.clientX === next.clientX && prev.clientY === next.clientY;
  }

  return false;
}
function isWindow(obj) {
  return obj && typeof obj === 'object' && obj.window === obj;
}
function isSimilarValue(val1, val2) {
  var int1 = Math.floor(val1);
  var int2 = Math.floor(val2);
  return Math.abs(int1 - int2) <= 1;
}
function restoreFocus(activeElement, container) {
  // Focus back if is in the container
  if (activeElement !== document.activeElement && Object(rc_util_es_Dom_contains__WEBPACK_IMPORTED_MODULE_0__["default"])(container, activeElement)) {
    activeElement.focus();
  }
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtYWxpZ24vZXMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWFsaWduL2VzL3V0aWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGNvbnRhaW5zIGZyb20gJ3JjLXV0aWwvZXMvRG9tL2NvbnRhaW5zJztcblxuZXhwb3J0IGZ1bmN0aW9uIGJ1ZmZlcihmbiwgbXMpIHtcbiAgdmFyIHRpbWVyID0gdm9pZCAwO1xuXG4gIGZ1bmN0aW9uIGNsZWFyKCkge1xuICAgIGlmICh0aW1lcikge1xuICAgICAgY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICAgIHRpbWVyID0gbnVsbDtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBidWZmZXJGbigpIHtcbiAgICBjbGVhcigpO1xuICAgIHRpbWVyID0gc2V0VGltZW91dChmbiwgbXMpO1xuICB9XG5cbiAgYnVmZmVyRm4uY2xlYXIgPSBjbGVhcjtcblxuICByZXR1cm4gYnVmZmVyRm47XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1NhbWVQb2ludChwcmV2LCBuZXh0KSB7XG4gIGlmIChwcmV2ID09PSBuZXh0KSByZXR1cm4gdHJ1ZTtcbiAgaWYgKCFwcmV2IHx8ICFuZXh0KSByZXR1cm4gZmFsc2U7XG5cbiAgaWYgKCdwYWdlWCcgaW4gbmV4dCAmJiAncGFnZVknIGluIG5leHQpIHtcbiAgICByZXR1cm4gcHJldi5wYWdlWCA9PT0gbmV4dC5wYWdlWCAmJiBwcmV2LnBhZ2VZID09PSBuZXh0LnBhZ2VZO1xuICB9XG5cbiAgaWYgKCdjbGllbnRYJyBpbiBuZXh0ICYmICdjbGllbnRZJyBpbiBuZXh0KSB7XG4gICAgcmV0dXJuIHByZXYuY2xpZW50WCA9PT0gbmV4dC5jbGllbnRYICYmIHByZXYuY2xpZW50WSA9PT0gbmV4dC5jbGllbnRZO1xuICB9XG5cbiAgcmV0dXJuIGZhbHNlO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNXaW5kb3cob2JqKSB7XG4gIHJldHVybiBvYmogJiYgdHlwZW9mIG9iaiA9PT0gJ29iamVjdCcgJiYgb2JqLndpbmRvdyA9PT0gb2JqO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNTaW1pbGFyVmFsdWUodmFsMSwgdmFsMikge1xuICB2YXIgaW50MSA9IE1hdGguZmxvb3IodmFsMSk7XG4gIHZhciBpbnQyID0gTWF0aC5mbG9vcih2YWwyKTtcbiAgcmV0dXJuIE1hdGguYWJzKGludDEgLSBpbnQyKSA8PSAxO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVzdG9yZUZvY3VzKGFjdGl2ZUVsZW1lbnQsIGNvbnRhaW5lcikge1xuICAvLyBGb2N1cyBiYWNrIGlmIGlzIGluIHRoZSBjb250YWluZXJcbiAgaWYgKGFjdGl2ZUVsZW1lbnQgIT09IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQgJiYgY29udGFpbnMoY29udGFpbmVyLCBhY3RpdmVFbGVtZW50KSkge1xuICAgIGFjdGl2ZUVsZW1lbnQuZm9jdXMoKTtcbiAgfVxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-align/es/util.js
