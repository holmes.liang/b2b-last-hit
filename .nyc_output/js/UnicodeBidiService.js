/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 * 
 */

/**
 * Stateful API for text direction detection
 *
 * This class can be used in applications where you need to detect the
 * direction of a sequence of text blocks, where each direction shall be used
 * as the fallback direction for the next one.
 *
 * NOTE: A default direction, if not provided, is set based on the global
 *       direction, as defined by `UnicodeBidiDirection`.
 *
 * == Example ==
 * ```
 * var UnicodeBidiService = require('UnicodeBidiService');
 *
 * var bidiService = new UnicodeBidiService();
 *
 * ...
 *
 * bidiService.reset();
 * for (var para in paragraphs) {
 *   var dir = bidiService.getDirection(para);
 *   ...
 * }
 * ```
 *
 * Part of our implementation of Unicode Bidirectional Algorithm (UBA)
 * Unicode Standard Annex #9 (UAX9)
 * http://www.unicode.org/reports/tr9/
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var UnicodeBidi = __webpack_require__(/*! ./UnicodeBidi */ "./node_modules/fbjs/lib/UnicodeBidi.js");

var UnicodeBidiDirection = __webpack_require__(/*! ./UnicodeBidiDirection */ "./node_modules/fbjs/lib/UnicodeBidiDirection.js");

var invariant = __webpack_require__(/*! ./invariant */ "./node_modules/fbjs/lib/invariant.js");

var UnicodeBidiService = function () {
  /**
   * Stateful class for paragraph direction detection
   *
   * @param defaultDir  Default direction of the service
   */
  function UnicodeBidiService(defaultDir) {
    _classCallCheck(this, UnicodeBidiService);

    if (!defaultDir) {
      defaultDir = UnicodeBidiDirection.getGlobalDir();
    } else {
      !UnicodeBidiDirection.isStrong(defaultDir) ?  true ? invariant(false, 'Default direction must be a strong direction (LTR or RTL)') : undefined : void 0;
    }

    this._defaultDir = defaultDir;
    this.reset();
  }
  /**
   * Reset the internal state
   *
   * Instead of creating a new instance, you can just reset() your instance
   * everytime you start a new loop.
   */


  UnicodeBidiService.prototype.reset = function reset() {
    this._lastDir = this._defaultDir;
  };
  /**
   * Returns the direction of a block of text, and remembers it as the
   * fall-back direction for the next paragraph.
   *
   * @param str  A text block, e.g. paragraph, table cell, tag
   * @return     The resolved direction
   */


  UnicodeBidiService.prototype.getDirection = function getDirection(str) {
    this._lastDir = UnicodeBidi.getDirection(str, this._lastDir);
    return this._lastDir;
  };

  return UnicodeBidiService;
}();

module.exports = UnicodeBidiService;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVW5pY29kZUJpZGlTZXJ2aWNlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVW5pY29kZUJpZGlTZXJ2aWNlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqIFxuICovXG5cbi8qKlxuICogU3RhdGVmdWwgQVBJIGZvciB0ZXh0IGRpcmVjdGlvbiBkZXRlY3Rpb25cbiAqXG4gKiBUaGlzIGNsYXNzIGNhbiBiZSB1c2VkIGluIGFwcGxpY2F0aW9ucyB3aGVyZSB5b3UgbmVlZCB0byBkZXRlY3QgdGhlXG4gKiBkaXJlY3Rpb24gb2YgYSBzZXF1ZW5jZSBvZiB0ZXh0IGJsb2Nrcywgd2hlcmUgZWFjaCBkaXJlY3Rpb24gc2hhbGwgYmUgdXNlZFxuICogYXMgdGhlIGZhbGxiYWNrIGRpcmVjdGlvbiBmb3IgdGhlIG5leHQgb25lLlxuICpcbiAqIE5PVEU6IEEgZGVmYXVsdCBkaXJlY3Rpb24sIGlmIG5vdCBwcm92aWRlZCwgaXMgc2V0IGJhc2VkIG9uIHRoZSBnbG9iYWxcbiAqICAgICAgIGRpcmVjdGlvbiwgYXMgZGVmaW5lZCBieSBgVW5pY29kZUJpZGlEaXJlY3Rpb25gLlxuICpcbiAqID09IEV4YW1wbGUgPT1cbiAqIGBgYFxuICogdmFyIFVuaWNvZGVCaWRpU2VydmljZSA9IHJlcXVpcmUoJ1VuaWNvZGVCaWRpU2VydmljZScpO1xuICpcbiAqIHZhciBiaWRpU2VydmljZSA9IG5ldyBVbmljb2RlQmlkaVNlcnZpY2UoKTtcbiAqXG4gKiAuLi5cbiAqXG4gKiBiaWRpU2VydmljZS5yZXNldCgpO1xuICogZm9yICh2YXIgcGFyYSBpbiBwYXJhZ3JhcGhzKSB7XG4gKiAgIHZhciBkaXIgPSBiaWRpU2VydmljZS5nZXREaXJlY3Rpb24ocGFyYSk7XG4gKiAgIC4uLlxuICogfVxuICogYGBgXG4gKlxuICogUGFydCBvZiBvdXIgaW1wbGVtZW50YXRpb24gb2YgVW5pY29kZSBCaWRpcmVjdGlvbmFsIEFsZ29yaXRobSAoVUJBKVxuICogVW5pY29kZSBTdGFuZGFyZCBBbm5leCAjOSAoVUFYOSlcbiAqIGh0dHA6Ly93d3cudW5pY29kZS5vcmcvcmVwb3J0cy90cjkvXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG52YXIgVW5pY29kZUJpZGkgPSByZXF1aXJlKCcuL1VuaWNvZGVCaWRpJyk7XG52YXIgVW5pY29kZUJpZGlEaXJlY3Rpb24gPSByZXF1aXJlKCcuL1VuaWNvZGVCaWRpRGlyZWN0aW9uJyk7XG5cbnZhciBpbnZhcmlhbnQgPSByZXF1aXJlKCcuL2ludmFyaWFudCcpO1xuXG52YXIgVW5pY29kZUJpZGlTZXJ2aWNlID0gZnVuY3Rpb24gKCkge1xuXG4gIC8qKlxuICAgKiBTdGF0ZWZ1bCBjbGFzcyBmb3IgcGFyYWdyYXBoIGRpcmVjdGlvbiBkZXRlY3Rpb25cbiAgICpcbiAgICogQHBhcmFtIGRlZmF1bHREaXIgIERlZmF1bHQgZGlyZWN0aW9uIG9mIHRoZSBzZXJ2aWNlXG4gICAqL1xuICBmdW5jdGlvbiBVbmljb2RlQmlkaVNlcnZpY2UoZGVmYXVsdERpcikge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBVbmljb2RlQmlkaVNlcnZpY2UpO1xuXG4gICAgaWYgKCFkZWZhdWx0RGlyKSB7XG4gICAgICBkZWZhdWx0RGlyID0gVW5pY29kZUJpZGlEaXJlY3Rpb24uZ2V0R2xvYmFsRGlyKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICFVbmljb2RlQmlkaURpcmVjdGlvbi5pc1N0cm9uZyhkZWZhdWx0RGlyKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdEZWZhdWx0IGRpcmVjdGlvbiBtdXN0IGJlIGEgc3Ryb25nIGRpcmVjdGlvbiAoTFRSIG9yIFJUTCknKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgfVxuICAgIHRoaXMuX2RlZmF1bHREaXIgPSBkZWZhdWx0RGlyO1xuICAgIHRoaXMucmVzZXQoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXNldCB0aGUgaW50ZXJuYWwgc3RhdGVcbiAgICpcbiAgICogSW5zdGVhZCBvZiBjcmVhdGluZyBhIG5ldyBpbnN0YW5jZSwgeW91IGNhbiBqdXN0IHJlc2V0KCkgeW91ciBpbnN0YW5jZVxuICAgKiBldmVyeXRpbWUgeW91IHN0YXJ0IGEgbmV3IGxvb3AuXG4gICAqL1xuXG5cbiAgVW5pY29kZUJpZGlTZXJ2aWNlLnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uIHJlc2V0KCkge1xuICAgIHRoaXMuX2xhc3REaXIgPSB0aGlzLl9kZWZhdWx0RGlyO1xuICB9O1xuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBkaXJlY3Rpb24gb2YgYSBibG9jayBvZiB0ZXh0LCBhbmQgcmVtZW1iZXJzIGl0IGFzIHRoZVxuICAgKiBmYWxsLWJhY2sgZGlyZWN0aW9uIGZvciB0aGUgbmV4dCBwYXJhZ3JhcGguXG4gICAqXG4gICAqIEBwYXJhbSBzdHIgIEEgdGV4dCBibG9jaywgZS5nLiBwYXJhZ3JhcGgsIHRhYmxlIGNlbGwsIHRhZ1xuICAgKiBAcmV0dXJuICAgICBUaGUgcmVzb2x2ZWQgZGlyZWN0aW9uXG4gICAqL1xuXG5cbiAgVW5pY29kZUJpZGlTZXJ2aWNlLnByb3RvdHlwZS5nZXREaXJlY3Rpb24gPSBmdW5jdGlvbiBnZXREaXJlY3Rpb24oc3RyKSB7XG4gICAgdGhpcy5fbGFzdERpciA9IFVuaWNvZGVCaWRpLmdldERpcmVjdGlvbihzdHIsIHRoaXMuX2xhc3REaXIpO1xuICAgIHJldHVybiB0aGlzLl9sYXN0RGlyO1xuICB9O1xuXG4gIHJldHVybiBVbmljb2RlQmlkaVNlcnZpY2U7XG59KCk7XG5cbm1vZHVsZS5leHBvcnRzID0gVW5pY29kZUJpZGlTZXJ2aWNlOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7QUFVQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE4QkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/UnicodeBidiService.js
