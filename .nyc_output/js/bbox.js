var vec2 = __webpack_require__(/*! ./vector */ "./node_modules/zrender/lib/core/vector.js");

var curve = __webpack_require__(/*! ./curve */ "./node_modules/zrender/lib/core/curve.js");
/**
 * @author Yi Shen(https://github.com/pissang)
 */


var mathMin = Math.min;
var mathMax = Math.max;
var mathSin = Math.sin;
var mathCos = Math.cos;
var PI2 = Math.PI * 2;
var start = vec2.create();
var end = vec2.create();
var extremity = vec2.create();
/**
 * 从顶点数组中计算出最小包围盒，写入`min`和`max`中
 * @module zrender/core/bbox
 * @param {Array<Object>} points 顶点数组
 * @param {number} min
 * @param {number} max
 */

function fromPoints(points, min, max) {
  if (points.length === 0) {
    return;
  }

  var p = points[0];
  var left = p[0];
  var right = p[0];
  var top = p[1];
  var bottom = p[1];
  var i;

  for (i = 1; i < points.length; i++) {
    p = points[i];
    left = mathMin(left, p[0]);
    right = mathMax(right, p[0]);
    top = mathMin(top, p[1]);
    bottom = mathMax(bottom, p[1]);
  }

  min[0] = left;
  min[1] = top;
  max[0] = right;
  max[1] = bottom;
}
/**
 * @memberOf module:zrender/core/bbox
 * @param {number} x0
 * @param {number} y0
 * @param {number} x1
 * @param {number} y1
 * @param {Array.<number>} min
 * @param {Array.<number>} max
 */


function fromLine(x0, y0, x1, y1, min, max) {
  min[0] = mathMin(x0, x1);
  min[1] = mathMin(y0, y1);
  max[0] = mathMax(x0, x1);
  max[1] = mathMax(y0, y1);
}

var xDim = [];
var yDim = [];
/**
 * 从三阶贝塞尔曲线(p0, p1, p2, p3)中计算出最小包围盒，写入`min`和`max`中
 * @memberOf module:zrender/core/bbox
 * @param {number} x0
 * @param {number} y0
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @param {number} x3
 * @param {number} y3
 * @param {Array.<number>} min
 * @param {Array.<number>} max
 */

function fromCubic(x0, y0, x1, y1, x2, y2, x3, y3, min, max) {
  var cubicExtrema = curve.cubicExtrema;
  var cubicAt = curve.cubicAt;
  var i;
  var n = cubicExtrema(x0, x1, x2, x3, xDim);
  min[0] = Infinity;
  min[1] = Infinity;
  max[0] = -Infinity;
  max[1] = -Infinity;

  for (i = 0; i < n; i++) {
    var x = cubicAt(x0, x1, x2, x3, xDim[i]);
    min[0] = mathMin(x, min[0]);
    max[0] = mathMax(x, max[0]);
  }

  n = cubicExtrema(y0, y1, y2, y3, yDim);

  for (i = 0; i < n; i++) {
    var y = cubicAt(y0, y1, y2, y3, yDim[i]);
    min[1] = mathMin(y, min[1]);
    max[1] = mathMax(y, max[1]);
  }

  min[0] = mathMin(x0, min[0]);
  max[0] = mathMax(x0, max[0]);
  min[0] = mathMin(x3, min[0]);
  max[0] = mathMax(x3, max[0]);
  min[1] = mathMin(y0, min[1]);
  max[1] = mathMax(y0, max[1]);
  min[1] = mathMin(y3, min[1]);
  max[1] = mathMax(y3, max[1]);
}
/**
 * 从二阶贝塞尔曲线(p0, p1, p2)中计算出最小包围盒，写入`min`和`max`中
 * @memberOf module:zrender/core/bbox
 * @param {number} x0
 * @param {number} y0
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @param {Array.<number>} min
 * @param {Array.<number>} max
 */


function fromQuadratic(x0, y0, x1, y1, x2, y2, min, max) {
  var quadraticExtremum = curve.quadraticExtremum;
  var quadraticAt = curve.quadraticAt; // Find extremities, where derivative in x dim or y dim is zero

  var tx = mathMax(mathMin(quadraticExtremum(x0, x1, x2), 1), 0);
  var ty = mathMax(mathMin(quadraticExtremum(y0, y1, y2), 1), 0);
  var x = quadraticAt(x0, x1, x2, tx);
  var y = quadraticAt(y0, y1, y2, ty);
  min[0] = mathMin(x0, x2, x);
  min[1] = mathMin(y0, y2, y);
  max[0] = mathMax(x0, x2, x);
  max[1] = mathMax(y0, y2, y);
}
/**
 * 从圆弧中计算出最小包围盒，写入`min`和`max`中
 * @method
 * @memberOf module:zrender/core/bbox
 * @param {number} x
 * @param {number} y
 * @param {number} rx
 * @param {number} ry
 * @param {number} startAngle
 * @param {number} endAngle
 * @param {number} anticlockwise
 * @param {Array.<number>} min
 * @param {Array.<number>} max
 */


function fromArc(x, y, rx, ry, startAngle, endAngle, anticlockwise, min, max) {
  var vec2Min = vec2.min;
  var vec2Max = vec2.max;
  var diff = Math.abs(startAngle - endAngle);

  if (diff % PI2 < 1e-4 && diff > 1e-4) {
    // Is a circle
    min[0] = x - rx;
    min[1] = y - ry;
    max[0] = x + rx;
    max[1] = y + ry;
    return;
  }

  start[0] = mathCos(startAngle) * rx + x;
  start[1] = mathSin(startAngle) * ry + y;
  end[0] = mathCos(endAngle) * rx + x;
  end[1] = mathSin(endAngle) * ry + y;
  vec2Min(min, start, end);
  vec2Max(max, start, end); // Thresh to [0, Math.PI * 2]

  startAngle = startAngle % PI2;

  if (startAngle < 0) {
    startAngle = startAngle + PI2;
  }

  endAngle = endAngle % PI2;

  if (endAngle < 0) {
    endAngle = endAngle + PI2;
  }

  if (startAngle > endAngle && !anticlockwise) {
    endAngle += PI2;
  } else if (startAngle < endAngle && anticlockwise) {
    startAngle += PI2;
  }

  if (anticlockwise) {
    var tmp = endAngle;
    endAngle = startAngle;
    startAngle = tmp;
  } // var number = 0;
  // var step = (anticlockwise ? -Math.PI : Math.PI) / 2;


  for (var angle = 0; angle < endAngle; angle += Math.PI / 2) {
    if (angle > startAngle) {
      extremity[0] = mathCos(angle) * rx + x;
      extremity[1] = mathSin(angle) * ry + y;
      vec2Min(min, extremity, min);
      vec2Max(max, extremity, max);
    }
  }
}

exports.fromPoints = fromPoints;
exports.fromLine = fromLine;
exports.fromCubic = fromCubic;
exports.fromQuadratic = fromQuadratic;
exports.fromArc = fromArc;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9iYm94LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9iYm94LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciB2ZWMyID0gcmVxdWlyZShcIi4vdmVjdG9yXCIpO1xuXG52YXIgY3VydmUgPSByZXF1aXJlKFwiLi9jdXJ2ZVwiKTtcblxuLyoqXG4gKiBAYXV0aG9yIFlpIFNoZW4oaHR0cHM6Ly9naXRodWIuY29tL3Bpc3NhbmcpXG4gKi9cbnZhciBtYXRoTWluID0gTWF0aC5taW47XG52YXIgbWF0aE1heCA9IE1hdGgubWF4O1xudmFyIG1hdGhTaW4gPSBNYXRoLnNpbjtcbnZhciBtYXRoQ29zID0gTWF0aC5jb3M7XG52YXIgUEkyID0gTWF0aC5QSSAqIDI7XG52YXIgc3RhcnQgPSB2ZWMyLmNyZWF0ZSgpO1xudmFyIGVuZCA9IHZlYzIuY3JlYXRlKCk7XG52YXIgZXh0cmVtaXR5ID0gdmVjMi5jcmVhdGUoKTtcbi8qKlxuICog5LuO6aG254K55pWw57uE5Lit6K6h566X5Ye65pyA5bCP5YyF5Zu055uS77yM5YaZ5YWlYG1pbmDlkoxgbWF4YOS4rVxuICogQG1vZHVsZSB6cmVuZGVyL2NvcmUvYmJveFxuICogQHBhcmFtIHtBcnJheTxPYmplY3Q+fSBwb2ludHMg6aG254K55pWw57uEXG4gKiBAcGFyYW0ge251bWJlcn0gbWluXG4gKiBAcGFyYW0ge251bWJlcn0gbWF4XG4gKi9cblxuZnVuY3Rpb24gZnJvbVBvaW50cyhwb2ludHMsIG1pbiwgbWF4KSB7XG4gIGlmIChwb2ludHMubGVuZ3RoID09PSAwKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHAgPSBwb2ludHNbMF07XG4gIHZhciBsZWZ0ID0gcFswXTtcbiAgdmFyIHJpZ2h0ID0gcFswXTtcbiAgdmFyIHRvcCA9IHBbMV07XG4gIHZhciBib3R0b20gPSBwWzFdO1xuICB2YXIgaTtcblxuICBmb3IgKGkgPSAxOyBpIDwgcG9pbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgcCA9IHBvaW50c1tpXTtcbiAgICBsZWZ0ID0gbWF0aE1pbihsZWZ0LCBwWzBdKTtcbiAgICByaWdodCA9IG1hdGhNYXgocmlnaHQsIHBbMF0pO1xuICAgIHRvcCA9IG1hdGhNaW4odG9wLCBwWzFdKTtcbiAgICBib3R0b20gPSBtYXRoTWF4KGJvdHRvbSwgcFsxXSk7XG4gIH1cblxuICBtaW5bMF0gPSBsZWZ0O1xuICBtaW5bMV0gPSB0b3A7XG4gIG1heFswXSA9IHJpZ2h0O1xuICBtYXhbMV0gPSBib3R0b207XG59XG4vKipcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL2Jib3hcbiAqIEBwYXJhbSB7bnVtYmVyfSB4MFxuICogQHBhcmFtIHtudW1iZXJ9IHkwXG4gKiBAcGFyYW0ge251bWJlcn0geDFcbiAqIEBwYXJhbSB7bnVtYmVyfSB5MVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gbWluXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBtYXhcbiAqL1xuXG5cbmZ1bmN0aW9uIGZyb21MaW5lKHgwLCB5MCwgeDEsIHkxLCBtaW4sIG1heCkge1xuICBtaW5bMF0gPSBtYXRoTWluKHgwLCB4MSk7XG4gIG1pblsxXSA9IG1hdGhNaW4oeTAsIHkxKTtcbiAgbWF4WzBdID0gbWF0aE1heCh4MCwgeDEpO1xuICBtYXhbMV0gPSBtYXRoTWF4KHkwLCB5MSk7XG59XG5cbnZhciB4RGltID0gW107XG52YXIgeURpbSA9IFtdO1xuLyoqXG4gKiDku47kuInpmLbotJ3loZ7lsJTmm7Lnur8ocDAsIHAxLCBwMiwgcDMp5Lit6K6h566X5Ye65pyA5bCP5YyF5Zu055uS77yM5YaZ5YWlYG1pbmDlkoxgbWF4YOS4rVxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvYmJveFxuICogQHBhcmFtIHtudW1iZXJ9IHgwXG4gKiBAcGFyYW0ge251bWJlcn0geTBcbiAqIEBwYXJhbSB7bnVtYmVyfSB4MVxuICogQHBhcmFtIHtudW1iZXJ9IHkxXG4gKiBAcGFyYW0ge251bWJlcn0geDJcbiAqIEBwYXJhbSB7bnVtYmVyfSB5MlxuICogQHBhcmFtIHtudW1iZXJ9IHgzXG4gKiBAcGFyYW0ge251bWJlcn0geTNcbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG1pblxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gbWF4XG4gKi9cblxuZnVuY3Rpb24gZnJvbUN1YmljKHgwLCB5MCwgeDEsIHkxLCB4MiwgeTIsIHgzLCB5MywgbWluLCBtYXgpIHtcbiAgdmFyIGN1YmljRXh0cmVtYSA9IGN1cnZlLmN1YmljRXh0cmVtYTtcbiAgdmFyIGN1YmljQXQgPSBjdXJ2ZS5jdWJpY0F0O1xuICB2YXIgaTtcbiAgdmFyIG4gPSBjdWJpY0V4dHJlbWEoeDAsIHgxLCB4MiwgeDMsIHhEaW0pO1xuICBtaW5bMF0gPSBJbmZpbml0eTtcbiAgbWluWzFdID0gSW5maW5pdHk7XG4gIG1heFswXSA9IC1JbmZpbml0eTtcbiAgbWF4WzFdID0gLUluZmluaXR5O1xuXG4gIGZvciAoaSA9IDA7IGkgPCBuOyBpKyspIHtcbiAgICB2YXIgeCA9IGN1YmljQXQoeDAsIHgxLCB4MiwgeDMsIHhEaW1baV0pO1xuICAgIG1pblswXSA9IG1hdGhNaW4oeCwgbWluWzBdKTtcbiAgICBtYXhbMF0gPSBtYXRoTWF4KHgsIG1heFswXSk7XG4gIH1cblxuICBuID0gY3ViaWNFeHRyZW1hKHkwLCB5MSwgeTIsIHkzLCB5RGltKTtcblxuICBmb3IgKGkgPSAwOyBpIDwgbjsgaSsrKSB7XG4gICAgdmFyIHkgPSBjdWJpY0F0KHkwLCB5MSwgeTIsIHkzLCB5RGltW2ldKTtcbiAgICBtaW5bMV0gPSBtYXRoTWluKHksIG1pblsxXSk7XG4gICAgbWF4WzFdID0gbWF0aE1heCh5LCBtYXhbMV0pO1xuICB9XG5cbiAgbWluWzBdID0gbWF0aE1pbih4MCwgbWluWzBdKTtcbiAgbWF4WzBdID0gbWF0aE1heCh4MCwgbWF4WzBdKTtcbiAgbWluWzBdID0gbWF0aE1pbih4MywgbWluWzBdKTtcbiAgbWF4WzBdID0gbWF0aE1heCh4MywgbWF4WzBdKTtcbiAgbWluWzFdID0gbWF0aE1pbih5MCwgbWluWzFdKTtcbiAgbWF4WzFdID0gbWF0aE1heCh5MCwgbWF4WzFdKTtcbiAgbWluWzFdID0gbWF0aE1pbih5MywgbWluWzFdKTtcbiAgbWF4WzFdID0gbWF0aE1heCh5MywgbWF4WzFdKTtcbn1cbi8qKlxuICog5LuO5LqM6Zi26LSd5aGe5bCU5puy57q/KHAwLCBwMSwgcDIp5Lit6K6h566X5Ye65pyA5bCP5YyF5Zu055uS77yM5YaZ5YWlYG1pbmDlkoxgbWF4YOS4rVxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvYmJveFxuICogQHBhcmFtIHtudW1iZXJ9IHgwXG4gKiBAcGFyYW0ge251bWJlcn0geTBcbiAqIEBwYXJhbSB7bnVtYmVyfSB4MVxuICogQHBhcmFtIHtudW1iZXJ9IHkxXG4gKiBAcGFyYW0ge251bWJlcn0geDJcbiAqIEBwYXJhbSB7bnVtYmVyfSB5MlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gbWluXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBtYXhcbiAqL1xuXG5cbmZ1bmN0aW9uIGZyb21RdWFkcmF0aWMoeDAsIHkwLCB4MSwgeTEsIHgyLCB5MiwgbWluLCBtYXgpIHtcbiAgdmFyIHF1YWRyYXRpY0V4dHJlbXVtID0gY3VydmUucXVhZHJhdGljRXh0cmVtdW07XG4gIHZhciBxdWFkcmF0aWNBdCA9IGN1cnZlLnF1YWRyYXRpY0F0OyAvLyBGaW5kIGV4dHJlbWl0aWVzLCB3aGVyZSBkZXJpdmF0aXZlIGluIHggZGltIG9yIHkgZGltIGlzIHplcm9cblxuICB2YXIgdHggPSBtYXRoTWF4KG1hdGhNaW4ocXVhZHJhdGljRXh0cmVtdW0oeDAsIHgxLCB4MiksIDEpLCAwKTtcbiAgdmFyIHR5ID0gbWF0aE1heChtYXRoTWluKHF1YWRyYXRpY0V4dHJlbXVtKHkwLCB5MSwgeTIpLCAxKSwgMCk7XG4gIHZhciB4ID0gcXVhZHJhdGljQXQoeDAsIHgxLCB4MiwgdHgpO1xuICB2YXIgeSA9IHF1YWRyYXRpY0F0KHkwLCB5MSwgeTIsIHR5KTtcbiAgbWluWzBdID0gbWF0aE1pbih4MCwgeDIsIHgpO1xuICBtaW5bMV0gPSBtYXRoTWluKHkwLCB5MiwgeSk7XG4gIG1heFswXSA9IG1hdGhNYXgoeDAsIHgyLCB4KTtcbiAgbWF4WzFdID0gbWF0aE1heCh5MCwgeTIsIHkpO1xufVxuLyoqXG4gKiDku47lnIblvKfkuK3orqHnrpflh7rmnIDlsI/ljIXlm7Tnm5LvvIzlhpnlhaVgbWluYOWSjGBtYXhg5LitXG4gKiBAbWV0aG9kXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS9iYm94XG4gKiBAcGFyYW0ge251bWJlcn0geFxuICogQHBhcmFtIHtudW1iZXJ9IHlcbiAqIEBwYXJhbSB7bnVtYmVyfSByeFxuICogQHBhcmFtIHtudW1iZXJ9IHJ5XG4gKiBAcGFyYW0ge251bWJlcn0gc3RhcnRBbmdsZVxuICogQHBhcmFtIHtudW1iZXJ9IGVuZEFuZ2xlXG4gKiBAcGFyYW0ge251bWJlcn0gYW50aWNsb2Nrd2lzZVxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gbWluXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBtYXhcbiAqL1xuXG5cbmZ1bmN0aW9uIGZyb21BcmMoeCwgeSwgcngsIHJ5LCBzdGFydEFuZ2xlLCBlbmRBbmdsZSwgYW50aWNsb2Nrd2lzZSwgbWluLCBtYXgpIHtcbiAgdmFyIHZlYzJNaW4gPSB2ZWMyLm1pbjtcbiAgdmFyIHZlYzJNYXggPSB2ZWMyLm1heDtcbiAgdmFyIGRpZmYgPSBNYXRoLmFicyhzdGFydEFuZ2xlIC0gZW5kQW5nbGUpO1xuXG4gIGlmIChkaWZmICUgUEkyIDwgMWUtNCAmJiBkaWZmID4gMWUtNCkge1xuICAgIC8vIElzIGEgY2lyY2xlXG4gICAgbWluWzBdID0geCAtIHJ4O1xuICAgIG1pblsxXSA9IHkgLSByeTtcbiAgICBtYXhbMF0gPSB4ICsgcng7XG4gICAgbWF4WzFdID0geSArIHJ5O1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHN0YXJ0WzBdID0gbWF0aENvcyhzdGFydEFuZ2xlKSAqIHJ4ICsgeDtcbiAgc3RhcnRbMV0gPSBtYXRoU2luKHN0YXJ0QW5nbGUpICogcnkgKyB5O1xuICBlbmRbMF0gPSBtYXRoQ29zKGVuZEFuZ2xlKSAqIHJ4ICsgeDtcbiAgZW5kWzFdID0gbWF0aFNpbihlbmRBbmdsZSkgKiByeSArIHk7XG4gIHZlYzJNaW4obWluLCBzdGFydCwgZW5kKTtcbiAgdmVjMk1heChtYXgsIHN0YXJ0LCBlbmQpOyAvLyBUaHJlc2ggdG8gWzAsIE1hdGguUEkgKiAyXVxuXG4gIHN0YXJ0QW5nbGUgPSBzdGFydEFuZ2xlICUgUEkyO1xuXG4gIGlmIChzdGFydEFuZ2xlIDwgMCkge1xuICAgIHN0YXJ0QW5nbGUgPSBzdGFydEFuZ2xlICsgUEkyO1xuICB9XG5cbiAgZW5kQW5nbGUgPSBlbmRBbmdsZSAlIFBJMjtcblxuICBpZiAoZW5kQW5nbGUgPCAwKSB7XG4gICAgZW5kQW5nbGUgPSBlbmRBbmdsZSArIFBJMjtcbiAgfVxuXG4gIGlmIChzdGFydEFuZ2xlID4gZW5kQW5nbGUgJiYgIWFudGljbG9ja3dpc2UpIHtcbiAgICBlbmRBbmdsZSArPSBQSTI7XG4gIH0gZWxzZSBpZiAoc3RhcnRBbmdsZSA8IGVuZEFuZ2xlICYmIGFudGljbG9ja3dpc2UpIHtcbiAgICBzdGFydEFuZ2xlICs9IFBJMjtcbiAgfVxuXG4gIGlmIChhbnRpY2xvY2t3aXNlKSB7XG4gICAgdmFyIHRtcCA9IGVuZEFuZ2xlO1xuICAgIGVuZEFuZ2xlID0gc3RhcnRBbmdsZTtcbiAgICBzdGFydEFuZ2xlID0gdG1wO1xuICB9IC8vIHZhciBudW1iZXIgPSAwO1xuICAvLyB2YXIgc3RlcCA9IChhbnRpY2xvY2t3aXNlID8gLU1hdGguUEkgOiBNYXRoLlBJKSAvIDI7XG5cblxuICBmb3IgKHZhciBhbmdsZSA9IDA7IGFuZ2xlIDwgZW5kQW5nbGU7IGFuZ2xlICs9IE1hdGguUEkgLyAyKSB7XG4gICAgaWYgKGFuZ2xlID4gc3RhcnRBbmdsZSkge1xuICAgICAgZXh0cmVtaXR5WzBdID0gbWF0aENvcyhhbmdsZSkgKiByeCArIHg7XG4gICAgICBleHRyZW1pdHlbMV0gPSBtYXRoU2luKGFuZ2xlKSAqIHJ5ICsgeTtcbiAgICAgIHZlYzJNaW4obWluLCBleHRyZW1pdHksIG1pbik7XG4gICAgICB2ZWMyTWF4KG1heCwgZXh0cmVtaXR5LCBtYXgpO1xuICAgIH1cbiAgfVxufVxuXG5leHBvcnRzLmZyb21Qb2ludHMgPSBmcm9tUG9pbnRzO1xuZXhwb3J0cy5mcm9tTGluZSA9IGZyb21MaW5lO1xuZXhwb3J0cy5mcm9tQ3ViaWMgPSBmcm9tQ3ViaWM7XG5leHBvcnRzLmZyb21RdWFkcmF0aWMgPSBmcm9tUXVhZHJhdGljO1xuZXhwb3J0cy5mcm9tQXJjID0gZnJvbUFyYzsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/bbox.js
