__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return InputIcon; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}




function InputIcon(props) {
  var _classNames;

  var suffixIcon = props.suffixIcon,
      prefixCls = props.prefixCls;
  return suffixIcon && (react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](suffixIcon) ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](suffixIcon, {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames = {}, _defineProperty(_classNames, suffixIcon.props.className, suffixIcon.props.className), _defineProperty(_classNames, "".concat(prefixCls, "-picker-icon"), true), _classNames))
  }) : react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
    className: "".concat(prefixCls, "-picker-icon")
  }, suffixIcon)) || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_2__["default"], {
    type: "calendar",
    className: "".concat(prefixCls, "-picker-icon")
  });
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9kYXRlLXBpY2tlci9JbnB1dEljb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2RhdGUtcGlja2VyL0lucHV0SWNvbi5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIElucHV0SWNvbihwcm9wcykge1xuICAgIGNvbnN0IHsgc3VmZml4SWNvbiwgcHJlZml4Q2xzIH0gPSBwcm9wcztcbiAgICByZXR1cm4gKChzdWZmaXhJY29uICYmXG4gICAgICAgIChSZWFjdC5pc1ZhbGlkRWxlbWVudChzdWZmaXhJY29uKSA/IChSZWFjdC5jbG9uZUVsZW1lbnQoc3VmZml4SWNvbiwge1xuICAgICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHtcbiAgICAgICAgICAgICAgICBbc3VmZml4SWNvbi5wcm9wcy5jbGFzc05hbWVdOiBzdWZmaXhJY29uLnByb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1waWNrZXItaWNvbmBdOiB0cnVlLFxuICAgICAgICAgICAgfSksXG4gICAgICAgIH0pKSA6ICg8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tcGlja2VyLWljb25gfT57c3VmZml4SWNvbn08L3NwYW4+KSkpIHx8IDxJY29uIHR5cGU9XCJjYWxlbmRhclwiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1waWNrZXItaWNvbmB9Lz4pO1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBRUE7QUFEQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/date-picker/InputIcon.js
