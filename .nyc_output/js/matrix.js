/**
 * 3x2矩阵操作类
 * @exports zrender/tool/matrix
 */
var ArrayCtor = typeof Float32Array === 'undefined' ? Array : Float32Array;
/**
 * Create a identity matrix.
 * @return {Float32Array|Array.<number>}
 */

function create() {
  var out = new ArrayCtor(6);
  identity(out);
  return out;
}
/**
 * 设置矩阵为单位矩阵
 * @param {Float32Array|Array.<number>} out
 */


function identity(out) {
  out[0] = 1;
  out[1] = 0;
  out[2] = 0;
  out[3] = 1;
  out[4] = 0;
  out[5] = 0;
  return out;
}
/**
 * 复制矩阵
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} m
 */


function copy(out, m) {
  out[0] = m[0];
  out[1] = m[1];
  out[2] = m[2];
  out[3] = m[3];
  out[4] = m[4];
  out[5] = m[5];
  return out;
}
/**
 * 矩阵相乘
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} m1
 * @param {Float32Array|Array.<number>} m2
 */


function mul(out, m1, m2) {
  // Consider matrix.mul(m, m2, m);
  // where out is the same as m2.
  // So use temp variable to escape error.
  var out0 = m1[0] * m2[0] + m1[2] * m2[1];
  var out1 = m1[1] * m2[0] + m1[3] * m2[1];
  var out2 = m1[0] * m2[2] + m1[2] * m2[3];
  var out3 = m1[1] * m2[2] + m1[3] * m2[3];
  var out4 = m1[0] * m2[4] + m1[2] * m2[5] + m1[4];
  var out5 = m1[1] * m2[4] + m1[3] * m2[5] + m1[5];
  out[0] = out0;
  out[1] = out1;
  out[2] = out2;
  out[3] = out3;
  out[4] = out4;
  out[5] = out5;
  return out;
}
/**
 * 平移变换
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} a
 * @param {Float32Array|Array.<number>} v
 */


function translate(out, a, v) {
  out[0] = a[0];
  out[1] = a[1];
  out[2] = a[2];
  out[3] = a[3];
  out[4] = a[4] + v[0];
  out[5] = a[5] + v[1];
  return out;
}
/**
 * 旋转变换
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} a
 * @param {number} rad
 */


function rotate(out, a, rad) {
  var aa = a[0];
  var ac = a[2];
  var atx = a[4];
  var ab = a[1];
  var ad = a[3];
  var aty = a[5];
  var st = Math.sin(rad);
  var ct = Math.cos(rad);
  out[0] = aa * ct + ab * st;
  out[1] = -aa * st + ab * ct;
  out[2] = ac * ct + ad * st;
  out[3] = -ac * st + ct * ad;
  out[4] = ct * atx + st * aty;
  out[5] = ct * aty - st * atx;
  return out;
}
/**
 * 缩放变换
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} a
 * @param {Float32Array|Array.<number>} v
 */


function scale(out, a, v) {
  var vx = v[0];
  var vy = v[1];
  out[0] = a[0] * vx;
  out[1] = a[1] * vy;
  out[2] = a[2] * vx;
  out[3] = a[3] * vy;
  out[4] = a[4] * vx;
  out[5] = a[5] * vy;
  return out;
}
/**
 * 求逆矩阵
 * @param {Float32Array|Array.<number>} out
 * @param {Float32Array|Array.<number>} a
 */


function invert(out, a) {
  var aa = a[0];
  var ac = a[2];
  var atx = a[4];
  var ab = a[1];
  var ad = a[3];
  var aty = a[5];
  var det = aa * ad - ab * ac;

  if (!det) {
    return null;
  }

  det = 1.0 / det;
  out[0] = ad * det;
  out[1] = -ab * det;
  out[2] = -ac * det;
  out[3] = aa * det;
  out[4] = (ac * aty - ad * atx) * det;
  out[5] = (ab * atx - aa * aty) * det;
  return out;
}
/**
 * Clone a new matrix.
 * @param {Float32Array|Array.<number>} a
 */


function clone(a) {
  var b = create();
  copy(b, a);
  return b;
}

exports.create = create;
exports.identity = identity;
exports.copy = copy;
exports.mul = mul;
exports.translate = translate;
exports.rotate = rotate;
exports.scale = scale;
exports.invert = invert;
exports.clone = clone;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9tYXRyaXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb3JlL21hdHJpeC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIDN4MuefqemYteaTjeS9nOexu1xuICogQGV4cG9ydHMgenJlbmRlci90b29sL21hdHJpeFxuICovXG52YXIgQXJyYXlDdG9yID0gdHlwZW9mIEZsb2F0MzJBcnJheSA9PT0gJ3VuZGVmaW5lZCcgPyBBcnJheSA6IEZsb2F0MzJBcnJheTtcbi8qKlxuICogQ3JlYXRlIGEgaWRlbnRpdHkgbWF0cml4LlxuICogQHJldHVybiB7RmxvYXQzMkFycmF5fEFycmF5LjxudW1iZXI+fVxuICovXG5cbmZ1bmN0aW9uIGNyZWF0ZSgpIHtcbiAgdmFyIG91dCA9IG5ldyBBcnJheUN0b3IoNik7XG4gIGlkZW50aXR5KG91dCk7XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOiuvue9ruefqemYteS4uuWNleS9jeefqemYtVxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG91dFxuICovXG5cblxuZnVuY3Rpb24gaWRlbnRpdHkob3V0KSB7XG4gIG91dFswXSA9IDE7XG4gIG91dFsxXSA9IDA7XG4gIG91dFsyXSA9IDA7XG4gIG91dFszXSA9IDE7XG4gIG91dFs0XSA9IDA7XG4gIG91dFs1XSA9IDA7XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOWkjeWItuefqemYtVxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG91dFxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG1cbiAqL1xuXG5cbmZ1bmN0aW9uIGNvcHkob3V0LCBtKSB7XG4gIG91dFswXSA9IG1bMF07XG4gIG91dFsxXSA9IG1bMV07XG4gIG91dFsyXSA9IG1bMl07XG4gIG91dFszXSA9IG1bM107XG4gIG91dFs0XSA9IG1bNF07XG4gIG91dFs1XSA9IG1bNV07XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOefqemYteebuOS5mFxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG91dFxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG0xXG4gKiBAcGFyYW0ge0Zsb2F0MzJBcnJheXxBcnJheS48bnVtYmVyPn0gbTJcbiAqL1xuXG5cbmZ1bmN0aW9uIG11bChvdXQsIG0xLCBtMikge1xuICAvLyBDb25zaWRlciBtYXRyaXgubXVsKG0sIG0yLCBtKTtcbiAgLy8gd2hlcmUgb3V0IGlzIHRoZSBzYW1lIGFzIG0yLlxuICAvLyBTbyB1c2UgdGVtcCB2YXJpYWJsZSB0byBlc2NhcGUgZXJyb3IuXG4gIHZhciBvdXQwID0gbTFbMF0gKiBtMlswXSArIG0xWzJdICogbTJbMV07XG4gIHZhciBvdXQxID0gbTFbMV0gKiBtMlswXSArIG0xWzNdICogbTJbMV07XG4gIHZhciBvdXQyID0gbTFbMF0gKiBtMlsyXSArIG0xWzJdICogbTJbM107XG4gIHZhciBvdXQzID0gbTFbMV0gKiBtMlsyXSArIG0xWzNdICogbTJbM107XG4gIHZhciBvdXQ0ID0gbTFbMF0gKiBtMls0XSArIG0xWzJdICogbTJbNV0gKyBtMVs0XTtcbiAgdmFyIG91dDUgPSBtMVsxXSAqIG0yWzRdICsgbTFbM10gKiBtMls1XSArIG0xWzVdO1xuICBvdXRbMF0gPSBvdXQwO1xuICBvdXRbMV0gPSBvdXQxO1xuICBvdXRbMl0gPSBvdXQyO1xuICBvdXRbM10gPSBvdXQzO1xuICBvdXRbNF0gPSBvdXQ0O1xuICBvdXRbNV0gPSBvdXQ1O1xuICByZXR1cm4gb3V0O1xufVxuLyoqXG4gKiDlubPnp7vlj5jmjaJcbiAqIEBwYXJhbSB7RmxvYXQzMkFycmF5fEFycmF5LjxudW1iZXI+fSBvdXRcbiAqIEBwYXJhbSB7RmxvYXQzMkFycmF5fEFycmF5LjxudW1iZXI+fSBhXG4gKiBAcGFyYW0ge0Zsb2F0MzJBcnJheXxBcnJheS48bnVtYmVyPn0gdlxuICovXG5cblxuZnVuY3Rpb24gdHJhbnNsYXRlKG91dCwgYSwgdikge1xuICBvdXRbMF0gPSBhWzBdO1xuICBvdXRbMV0gPSBhWzFdO1xuICBvdXRbMl0gPSBhWzJdO1xuICBvdXRbM10gPSBhWzNdO1xuICBvdXRbNF0gPSBhWzRdICsgdlswXTtcbiAgb3V0WzVdID0gYVs1XSArIHZbMV07XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOaXi+i9rOWPmOaNolxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG91dFxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IGFcbiAqIEBwYXJhbSB7bnVtYmVyfSByYWRcbiAqL1xuXG5cbmZ1bmN0aW9uIHJvdGF0ZShvdXQsIGEsIHJhZCkge1xuICB2YXIgYWEgPSBhWzBdO1xuICB2YXIgYWMgPSBhWzJdO1xuICB2YXIgYXR4ID0gYVs0XTtcbiAgdmFyIGFiID0gYVsxXTtcbiAgdmFyIGFkID0gYVszXTtcbiAgdmFyIGF0eSA9IGFbNV07XG4gIHZhciBzdCA9IE1hdGguc2luKHJhZCk7XG4gIHZhciBjdCA9IE1hdGguY29zKHJhZCk7XG4gIG91dFswXSA9IGFhICogY3QgKyBhYiAqIHN0O1xuICBvdXRbMV0gPSAtYWEgKiBzdCArIGFiICogY3Q7XG4gIG91dFsyXSA9IGFjICogY3QgKyBhZCAqIHN0O1xuICBvdXRbM10gPSAtYWMgKiBzdCArIGN0ICogYWQ7XG4gIG91dFs0XSA9IGN0ICogYXR4ICsgc3QgKiBhdHk7XG4gIG91dFs1XSA9IGN0ICogYXR5IC0gc3QgKiBhdHg7XG4gIHJldHVybiBvdXQ7XG59XG4vKipcbiAqIOe8qeaUvuWPmOaNolxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IG91dFxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IGFcbiAqIEBwYXJhbSB7RmxvYXQzMkFycmF5fEFycmF5LjxudW1iZXI+fSB2XG4gKi9cblxuXG5mdW5jdGlvbiBzY2FsZShvdXQsIGEsIHYpIHtcbiAgdmFyIHZ4ID0gdlswXTtcbiAgdmFyIHZ5ID0gdlsxXTtcbiAgb3V0WzBdID0gYVswXSAqIHZ4O1xuICBvdXRbMV0gPSBhWzFdICogdnk7XG4gIG91dFsyXSA9IGFbMl0gKiB2eDtcbiAgb3V0WzNdID0gYVszXSAqIHZ5O1xuICBvdXRbNF0gPSBhWzRdICogdng7XG4gIG91dFs1XSA9IGFbNV0gKiB2eTtcbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICog5rGC6YCG55+p6Zi1XG4gKiBAcGFyYW0ge0Zsb2F0MzJBcnJheXxBcnJheS48bnVtYmVyPn0gb3V0XG4gKiBAcGFyYW0ge0Zsb2F0MzJBcnJheXxBcnJheS48bnVtYmVyPn0gYVxuICovXG5cblxuZnVuY3Rpb24gaW52ZXJ0KG91dCwgYSkge1xuICB2YXIgYWEgPSBhWzBdO1xuICB2YXIgYWMgPSBhWzJdO1xuICB2YXIgYXR4ID0gYVs0XTtcbiAgdmFyIGFiID0gYVsxXTtcbiAgdmFyIGFkID0gYVszXTtcbiAgdmFyIGF0eSA9IGFbNV07XG4gIHZhciBkZXQgPSBhYSAqIGFkIC0gYWIgKiBhYztcblxuICBpZiAoIWRldCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgZGV0ID0gMS4wIC8gZGV0O1xuICBvdXRbMF0gPSBhZCAqIGRldDtcbiAgb3V0WzFdID0gLWFiICogZGV0O1xuICBvdXRbMl0gPSAtYWMgKiBkZXQ7XG4gIG91dFszXSA9IGFhICogZGV0O1xuICBvdXRbNF0gPSAoYWMgKiBhdHkgLSBhZCAqIGF0eCkgKiBkZXQ7XG4gIG91dFs1XSA9IChhYiAqIGF0eCAtIGFhICogYXR5KSAqIGRldDtcbiAgcmV0dXJuIG91dDtcbn1cbi8qKlxuICogQ2xvbmUgYSBuZXcgbWF0cml4LlxuICogQHBhcmFtIHtGbG9hdDMyQXJyYXl8QXJyYXkuPG51bWJlcj59IGFcbiAqL1xuXG5cbmZ1bmN0aW9uIGNsb25lKGEpIHtcbiAgdmFyIGIgPSBjcmVhdGUoKTtcbiAgY29weShiLCBhKTtcbiAgcmV0dXJuIGI7XG59XG5cbmV4cG9ydHMuY3JlYXRlID0gY3JlYXRlO1xuZXhwb3J0cy5pZGVudGl0eSA9IGlkZW50aXR5O1xuZXhwb3J0cy5jb3B5ID0gY29weTtcbmV4cG9ydHMubXVsID0gbXVsO1xuZXhwb3J0cy50cmFuc2xhdGUgPSB0cmFuc2xhdGU7XG5leHBvcnRzLnJvdGF0ZSA9IHJvdGF0ZTtcbmV4cG9ydHMuc2NhbGUgPSBzY2FsZTtcbmV4cG9ydHMuaW52ZXJ0ID0gaW52ZXJ0O1xuZXhwb3J0cy5jbG9uZSA9IGNsb25lOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/matrix.js
