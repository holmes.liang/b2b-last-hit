/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftPasteProcessor
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var convertFromHTMLtoContentBlocks = __webpack_require__(/*! ./convertFromHTMLToContentBlocks */ "./node_modules/draft-js/lib/convertFromHTMLToContentBlocks.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var getSafeBodyFromHTML = __webpack_require__(/*! ./getSafeBodyFromHTML */ "./node_modules/draft-js/lib/getSafeBodyFromHTML.js");

var sanitizeDraftText = __webpack_require__(/*! ./sanitizeDraftText */ "./node_modules/draft-js/lib/sanitizeDraftText.js");

var List = Immutable.List,
    Repeat = Immutable.Repeat;
var experimentalTreeDataSupport = DraftFeatureFlags.draft_tree_data_support;
var ContentBlockRecord = experimentalTreeDataSupport ? ContentBlockNode : ContentBlock;
var DraftPasteProcessor = {
  processHTML: function processHTML(html, blockRenderMap) {
    return convertFromHTMLtoContentBlocks(html, getSafeBodyFromHTML, blockRenderMap);
  },
  processText: function processText(textBlocks, character, type) {
    return textBlocks.reduce(function (acc, textLine, index) {
      textLine = sanitizeDraftText(textLine);
      var key = generateRandomKey();
      var blockNodeConfig = {
        key: key,
        type: type,
        text: textLine,
        characterList: List(Repeat(character, textLine.length))
      }; // next block updates previous block

      if (experimentalTreeDataSupport && index !== 0) {
        var prevSiblingIndex = index - 1; // update previous block

        var previousBlock = acc[prevSiblingIndex] = acc[prevSiblingIndex].merge({
          nextSibling: key
        });
        blockNodeConfig = _extends({}, blockNodeConfig, {
          prevSibling: previousBlock.getKey()
        });
      }

      acc.push(new ContentBlockRecord(blockNodeConfig));
      return acc;
    }, []);
  }
};
module.exports = DraftPasteProcessor;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0UGFzdGVQcm9jZXNzb3IuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRQYXN0ZVByb2Nlc3Nvci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0UGFzdGVQcm9jZXNzb3JcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBDaGFyYWN0ZXJNZXRhZGF0YSA9IHJlcXVpcmUoJy4vQ2hhcmFjdGVyTWV0YWRhdGEnKTtcbnZhciBDb250ZW50QmxvY2sgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9jaycpO1xudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBEcmFmdEZlYXR1cmVGbGFncyA9IHJlcXVpcmUoJy4vRHJhZnRGZWF0dXJlRmxhZ3MnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcblxudmFyIGNvbnZlcnRGcm9tSFRNTHRvQ29udGVudEJsb2NrcyA9IHJlcXVpcmUoJy4vY29udmVydEZyb21IVE1MVG9Db250ZW50QmxvY2tzJyk7XG52YXIgZ2VuZXJhdGVSYW5kb21LZXkgPSByZXF1aXJlKCcuL2dlbmVyYXRlUmFuZG9tS2V5Jyk7XG52YXIgZ2V0U2FmZUJvZHlGcm9tSFRNTCA9IHJlcXVpcmUoJy4vZ2V0U2FmZUJvZHlGcm9tSFRNTCcpO1xudmFyIHNhbml0aXplRHJhZnRUZXh0ID0gcmVxdWlyZSgnLi9zYW5pdGl6ZURyYWZ0VGV4dCcpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0LFxuICAgIFJlcGVhdCA9IEltbXV0YWJsZS5SZXBlYXQ7XG5cblxudmFyIGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCA9IERyYWZ0RmVhdHVyZUZsYWdzLmRyYWZ0X3RyZWVfZGF0YV9zdXBwb3J0O1xudmFyIENvbnRlbnRCbG9ja1JlY29yZCA9IGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCA/IENvbnRlbnRCbG9ja05vZGUgOiBDb250ZW50QmxvY2s7XG5cbnZhciBEcmFmdFBhc3RlUHJvY2Vzc29yID0ge1xuICBwcm9jZXNzSFRNTDogZnVuY3Rpb24gcHJvY2Vzc0hUTUwoaHRtbCwgYmxvY2tSZW5kZXJNYXApIHtcbiAgICByZXR1cm4gY29udmVydEZyb21IVE1MdG9Db250ZW50QmxvY2tzKGh0bWwsIGdldFNhZmVCb2R5RnJvbUhUTUwsIGJsb2NrUmVuZGVyTWFwKTtcbiAgfSxcbiAgcHJvY2Vzc1RleHQ6IGZ1bmN0aW9uIHByb2Nlc3NUZXh0KHRleHRCbG9ja3MsIGNoYXJhY3RlciwgdHlwZSkge1xuICAgIHJldHVybiB0ZXh0QmxvY2tzLnJlZHVjZShmdW5jdGlvbiAoYWNjLCB0ZXh0TGluZSwgaW5kZXgpIHtcbiAgICAgIHRleHRMaW5lID0gc2FuaXRpemVEcmFmdFRleHQodGV4dExpbmUpO1xuICAgICAgdmFyIGtleSA9IGdlbmVyYXRlUmFuZG9tS2V5KCk7XG5cbiAgICAgIHZhciBibG9ja05vZGVDb25maWcgPSB7XG4gICAgICAgIGtleToga2V5LFxuICAgICAgICB0eXBlOiB0eXBlLFxuICAgICAgICB0ZXh0OiB0ZXh0TGluZSxcbiAgICAgICAgY2hhcmFjdGVyTGlzdDogTGlzdChSZXBlYXQoY2hhcmFjdGVyLCB0ZXh0TGluZS5sZW5ndGgpKVxuICAgICAgfTtcblxuICAgICAgLy8gbmV4dCBibG9jayB1cGRhdGVzIHByZXZpb3VzIGJsb2NrXG4gICAgICBpZiAoZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ICYmIGluZGV4ICE9PSAwKSB7XG4gICAgICAgIHZhciBwcmV2U2libGluZ0luZGV4ID0gaW5kZXggLSAxO1xuICAgICAgICAvLyB1cGRhdGUgcHJldmlvdXMgYmxvY2tcbiAgICAgICAgdmFyIHByZXZpb3VzQmxvY2sgPSBhY2NbcHJldlNpYmxpbmdJbmRleF0gPSBhY2NbcHJldlNpYmxpbmdJbmRleF0ubWVyZ2Uoe1xuICAgICAgICAgIG5leHRTaWJsaW5nOiBrZXlcbiAgICAgICAgfSk7XG4gICAgICAgIGJsb2NrTm9kZUNvbmZpZyA9IF9leHRlbmRzKHt9LCBibG9ja05vZGVDb25maWcsIHtcbiAgICAgICAgICBwcmV2U2libGluZzogcHJldmlvdXNCbG9jay5nZXRLZXkoKVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgYWNjLnB1c2gobmV3IENvbnRlbnRCbG9ja1JlY29yZChibG9ja05vZGVDb25maWcpKTtcblxuICAgICAgcmV0dXJuIGFjYztcbiAgICB9LCBbXSk7XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gRHJhZnRQYXN0ZVByb2Nlc3NvcjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQWhDQTtBQW1DQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftPasteProcessor.js
