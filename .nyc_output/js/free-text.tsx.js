__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FreeText", function() { return FreeText; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/free-text.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n\tborder: 1px solid #d9d9d9;\n\tborder-radius: 5px;\n\tpadding: 13px 0 13px 13px;\n\tline-height: 17px;\n\tfont-size: 16px;\n\tfont-weight: 300\n\ttransition: none;\n\tcolor: #333;\n\tmin-width: 300px;\n\t&:focus {\n\t\tborder: 1px solid ", ";\n\t\tpadding: 13px 0 13px 13px;\n\t\t&:not(:placeholder-shown) {\n\t\t\tborder: 1px solid ", ";\n\t\t\tpadding: 13px 0 13px 13px;\n\t\t}\n\t}\n\t&::placeholder {\n\t\tcolor: #bfbfbf;\n\t}\n\t", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_2__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;
/**
 * 无焦点218; 有焦点有内容183; 有焦点无内容255
 */


var Text = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].input.attrs({
  "data-widget": "free-text"
})(_templateObject(), COLOR_PRIMARY, COLOR_PRIMARY, function (props) {
  return props.css ? props.css() : "";
});
var FreeText = _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].forwardRef(function (props, ref) {
  return _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(Text, Object.assign({}, props, {
    type: "number",
    ref: ref,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }));
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2ZyZWUtdGV4dC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvZnJlZS10ZXh0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXMvaW5kZXhcIjtcblxuY29uc3QgeyBDT0xPUl9QUklNQVJZIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xuLyoqXG4gKiDml6DnhKbngrkyMTg7IOacieeEpueCueacieWGheWuuTE4Mzsg5pyJ54Sm54K55peg5YaF5a65MjU1XG4gKi9cbmNvbnN0IFRleHQgPSBTdHlsZWQuaW5wdXQuYXR0cnMoe1xuICBcImRhdGEtd2lkZ2V0XCI6IFwiZnJlZS10ZXh0XCIsXG59KWBcblx0Ym9yZGVyOiAxcHggc29saWQgI2Q5ZDlkOTtcblx0Ym9yZGVyLXJhZGl1czogNXB4O1xuXHRwYWRkaW5nOiAxM3B4IDAgMTNweCAxM3B4O1xuXHRsaW5lLWhlaWdodDogMTdweDtcblx0Zm9udC1zaXplOiAxNnB4O1xuXHRmb250LXdlaWdodDogMzAwXG5cdHRyYW5zaXRpb246IG5vbmU7XG5cdGNvbG9yOiAjMzMzO1xuXHRtaW4td2lkdGg6IDMwMHB4O1xuXHQmOmZvY3VzIHtcblx0XHRib3JkZXI6IDFweCBzb2xpZCAke0NPTE9SX1BSSU1BUll9O1xuXHRcdHBhZGRpbmc6IDEzcHggMCAxM3B4IDEzcHg7XG5cdFx0Jjpub3QoOnBsYWNlaG9sZGVyLXNob3duKSB7XG5cdFx0XHRib3JkZXI6IDFweCBzb2xpZCAke0NPTE9SX1BSSU1BUll9O1xuXHRcdFx0cGFkZGluZzogMTNweCAwIDEzcHggMTNweDtcblx0XHR9XG5cdH1cblx0Jjo6cGxhY2Vob2xkZXIge1xuXHRcdGNvbG9yOiAjYmZiZmJmO1xuXHR9XG5cdCR7KHByb3BzOiB7IGNzcz86ICgpID0+IHN0cmluZyB9KSA9PiAocHJvcHMuY3NzID8gcHJvcHMuY3NzKCkgOiBcIlwiKX1cbmA7XG5cbmV4cG9ydCBjb25zdCBGcmVlVGV4dCA9IFJlYWN0LmZvcndhcmRSZWYoKHByb3BzOiBhbnksIHJlZikgPT4ge1xuICByZXR1cm4gPFRleHQgey4uLnByb3BzfSB0eXBlPVwibnVtYmVyXCIgcmVmPXtyZWZ9Lz47XG59KTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7OztBQUdBO0FBQ0E7QUFEQTtBQXVCQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/free-text.tsx
