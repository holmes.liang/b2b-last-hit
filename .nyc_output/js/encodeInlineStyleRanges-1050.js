/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule encodeInlineStyleRanges
 * @format
 * 
 */


var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var findRangesImmutable = __webpack_require__(/*! ./findRangesImmutable */ "./node_modules/draft-js/lib/findRangesImmutable.js");

var areEqual = function areEqual(a, b) {
  return a === b;
};

var isTruthy = function isTruthy(a) {
  return !!a;
};

var EMPTY_ARRAY = [];
/**
 * Helper function for getting encoded styles for each inline style. Convert
 * to UTF-8 character counts for storage.
 */

function getEncodedInlinesForType(block, styleList, styleToEncode) {
  var ranges = []; // Obtain an array with ranges for only the specified style.

  var filteredInlines = styleList.map(function (style) {
    return style.has(styleToEncode);
  }).toList();
  findRangesImmutable(filteredInlines, areEqual, // We only want to keep ranges with nonzero style values.
  isTruthy, function (start, end) {
    var text = block.getText();
    ranges.push({
      offset: UnicodeUtils.strlen(text.slice(0, start)),
      length: UnicodeUtils.strlen(text.slice(start, end)),
      style: styleToEncode
    });
  });
  return ranges;
}
/*
 * Retrieve the encoded arrays of inline styles, with each individual style
 * treated separately.
 */


function encodeInlineStyleRanges(block) {
  var styleList = block.getCharacterList().map(function (c) {
    return c.getStyle();
  }).toList();
  var ranges = styleList.flatten().toSet().map(function (style) {
    return getEncodedInlinesForType(block, styleList, style);
  });
  return Array.prototype.concat.apply(EMPTY_ARRAY, ranges.toJS());
}

module.exports = encodeInlineStyleRanges;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VuY29kZUlubGluZVN0eWxlUmFuZ2VzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VuY29kZUlubGluZVN0eWxlUmFuZ2VzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZW5jb2RlSW5saW5lU3R5bGVSYW5nZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgZmluZFJhbmdlc0ltbXV0YWJsZSA9IHJlcXVpcmUoJy4vZmluZFJhbmdlc0ltbXV0YWJsZScpO1xuXG52YXIgYXJlRXF1YWwgPSBmdW5jdGlvbiBhcmVFcXVhbChhLCBiKSB7XG4gIHJldHVybiBhID09PSBiO1xufTtcbnZhciBpc1RydXRoeSA9IGZ1bmN0aW9uIGlzVHJ1dGh5KGEpIHtcbiAgcmV0dXJuICEhYTtcbn07XG52YXIgRU1QVFlfQVJSQVkgPSBbXTtcblxuLyoqXG4gKiBIZWxwZXIgZnVuY3Rpb24gZm9yIGdldHRpbmcgZW5jb2RlZCBzdHlsZXMgZm9yIGVhY2ggaW5saW5lIHN0eWxlLiBDb252ZXJ0XG4gKiB0byBVVEYtOCBjaGFyYWN0ZXIgY291bnRzIGZvciBzdG9yYWdlLlxuICovXG5mdW5jdGlvbiBnZXRFbmNvZGVkSW5saW5lc0ZvclR5cGUoYmxvY2ssIHN0eWxlTGlzdCwgc3R5bGVUb0VuY29kZSkge1xuICB2YXIgcmFuZ2VzID0gW107XG5cbiAgLy8gT2J0YWluIGFuIGFycmF5IHdpdGggcmFuZ2VzIGZvciBvbmx5IHRoZSBzcGVjaWZpZWQgc3R5bGUuXG4gIHZhciBmaWx0ZXJlZElubGluZXMgPSBzdHlsZUxpc3QubWFwKGZ1bmN0aW9uIChzdHlsZSkge1xuICAgIHJldHVybiBzdHlsZS5oYXMoc3R5bGVUb0VuY29kZSk7XG4gIH0pLnRvTGlzdCgpO1xuXG4gIGZpbmRSYW5nZXNJbW11dGFibGUoZmlsdGVyZWRJbmxpbmVzLCBhcmVFcXVhbCxcbiAgLy8gV2Ugb25seSB3YW50IHRvIGtlZXAgcmFuZ2VzIHdpdGggbm9uemVybyBzdHlsZSB2YWx1ZXMuXG4gIGlzVHJ1dGh5LCBmdW5jdGlvbiAoc3RhcnQsIGVuZCkge1xuICAgIHZhciB0ZXh0ID0gYmxvY2suZ2V0VGV4dCgpO1xuICAgIHJhbmdlcy5wdXNoKHtcbiAgICAgIG9mZnNldDogVW5pY29kZVV0aWxzLnN0cmxlbih0ZXh0LnNsaWNlKDAsIHN0YXJ0KSksXG4gICAgICBsZW5ndGg6IFVuaWNvZGVVdGlscy5zdHJsZW4odGV4dC5zbGljZShzdGFydCwgZW5kKSksXG4gICAgICBzdHlsZTogc3R5bGVUb0VuY29kZVxuICAgIH0pO1xuICB9KTtcblxuICByZXR1cm4gcmFuZ2VzO1xufVxuXG4vKlxuICogUmV0cmlldmUgdGhlIGVuY29kZWQgYXJyYXlzIG9mIGlubGluZSBzdHlsZXMsIHdpdGggZWFjaCBpbmRpdmlkdWFsIHN0eWxlXG4gKiB0cmVhdGVkIHNlcGFyYXRlbHkuXG4gKi9cbmZ1bmN0aW9uIGVuY29kZUlubGluZVN0eWxlUmFuZ2VzKGJsb2NrKSB7XG4gIHZhciBzdHlsZUxpc3QgPSBibG9jay5nZXRDaGFyYWN0ZXJMaXN0KCkubWFwKGZ1bmN0aW9uIChjKSB7XG4gICAgcmV0dXJuIGMuZ2V0U3R5bGUoKTtcbiAgfSkudG9MaXN0KCk7XG4gIHZhciByYW5nZXMgPSBzdHlsZUxpc3QuZmxhdHRlbigpLnRvU2V0KCkubWFwKGZ1bmN0aW9uIChzdHlsZSkge1xuICAgIHJldHVybiBnZXRFbmNvZGVkSW5saW5lc0ZvclR5cGUoYmxvY2ssIHN0eWxlTGlzdCwgc3R5bGUpO1xuICB9KTtcblxuICByZXR1cm4gQXJyYXkucHJvdG90eXBlLmNvbmNhdC5hcHBseShFTVBUWV9BUlJBWSwgcmFuZ2VzLnRvSlMoKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZW5jb2RlSW5saW5lU3R5bGVSYW5nZXM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBRUE7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/encodeInlineStyleRanges.js
