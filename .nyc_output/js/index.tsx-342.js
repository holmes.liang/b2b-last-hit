__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewItemStyle2", function() { return ViewItemStyle2; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./style */ "./src/app/desk/component/view-item/style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");


var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/view-item/index.tsx";




var isMobile = _common__WEBPACK_IMPORTED_MODULE_5__["Utils"].getIsMobile();
var defaultLayout = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 13
    },
    sm: {
      span: 16
    }
  }
};

var ViewItem = function ViewItem(_ref) {
  var title = _ref.title,
      children = _ref.children,
      _ref$layout = _ref.layout,
      layout = _ref$layout === void 0 ? defaultLayout : _ref$layout,
      align = _ref.align,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_ref, ["title", "children", "layout", "align"]);

  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(_style__WEBPACK_IMPORTED_MODULE_4__["default"].box, Object.assign({}, rest, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    className: "item",
    type: "flex",
    gutter: 24,
    align: align || "middle",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], Object.assign({
    className: "itemTitle ".concat(isMobile ? "mobile-item-title" : "")
  }, layout.labelCol, {
    style: {
      paddingRight: 1
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }), title), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], Object.assign({
    className: "itemContent"
  }, layout.wrapperCol, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }), children)));
};

var defaultLayout2 = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewItemStyle2 = function ViewItemStyle2(props) {
  return ViewItem(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
    layout: defaultLayout2
  }));
};


/* harmony default export */ __webpack_exports__["default"] = (ViewItem);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3ZpZXctaXRlbS9pbmRleC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvdmlldy1pdGVtL2luZGV4LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuXG5pbXBvcnQgU3R5bGUgZnJvbSBcIi4vc3R5bGVcIjtcbmltcG9ydCB7IFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuY29uc3QgZGVmYXVsdExheW91dDogYW55ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDYgfSxcbiAgICBzbTogeyBzcGFuOiA4IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMyB9LFxuICAgIHNtOiB7IHNwYW46IDE2IH0sXG4gIH0sXG59O1xuXG5jb25zdCBWaWV3SXRlbSA9ICh7IHRpdGxlLCBjaGlsZHJlbiwgbGF5b3V0ID0gZGVmYXVsdExheW91dCwgYWxpZ24sIC4uLnJlc3QgfTogYW55KSA9PiB7XG4gIHJldHVybiAoXG4gICAgPFN0eWxlLmJveCB7Li4ucmVzdH0+XG4gICAgICA8Um93IGNsYXNzTmFtZT1cIml0ZW1cIiB0eXBlPVwiZmxleFwiIGd1dHRlcj17MjR9IGFsaWduPXthbGlnbiB8fCBcIm1pZGRsZVwifT5cbiAgICAgICAgPENvbFxuICAgICAgICAgIGNsYXNzTmFtZT17YGl0ZW1UaXRsZSAke2lzTW9iaWxlID8gXCJtb2JpbGUtaXRlbS10aXRsZVwiIDogXCJcIn1gfVxuICAgICAgICAgIHsuLi5sYXlvdXQubGFiZWxDb2x9XG4gICAgICAgICAgc3R5bGU9e3sgcGFkZGluZ1JpZ2h0OiAxIH19XG4gICAgICAgID5cbiAgICAgICAgICB7dGl0bGV9XG4gICAgICAgIDwvQ29sPlxuICAgICAgICA8Q29sIGNsYXNzTmFtZT1cIml0ZW1Db250ZW50XCIgey4uLmxheW91dC53cmFwcGVyQ29sfT5cbiAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgIDwvQ29sPlxuICAgICAgPC9Sb3c+XG4gICAgPC9TdHlsZS5ib3g+XG4gICk7XG59O1xuXG5jb25zdCBkZWZhdWx0TGF5b3V0MiA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTYgfSxcbiAgICBzbTogeyBzcGFuOiAxMyB9LFxuICB9LFxufTtcblxuY29uc3QgVmlld0l0ZW1TdHlsZTIgPSAocHJvcHM6IGFueSkgPT4gVmlld0l0ZW0oeyAuLi5wcm9wcywgbGF5b3V0OiBkZWZhdWx0TGF5b3V0MiB9KTtcblxuZXhwb3J0IHsgVmlld0l0ZW1TdHlsZTIgfTtcblxuZXhwb3J0IGRlZmF1bHQgVmlld0l0ZW07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBVUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/view-item/index.tsx
