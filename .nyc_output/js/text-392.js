var BoundingRect = __webpack_require__(/*! ../core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var imageHelper = __webpack_require__(/*! ../graphic/helper/image */ "./node_modules/zrender/lib/graphic/helper/image.js");

var _util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var getContext = _util.getContext;
var extend = _util.extend;
var retrieve2 = _util.retrieve2;
var retrieve3 = _util.retrieve3;
var trim = _util.trim;
var textWidthCache = {};
var textWidthCacheCounter = 0;
var TEXT_CACHE_MAX = 5000;
var STYLE_REG = /\{([a-zA-Z0-9_]+)\|([^}]*)\}/g;
var DEFAULT_FONT = '12px sans-serif'; // Avoid assign to an exported variable, for transforming to cjs.

var methods = {};

function $override(name, fn) {
  methods[name] = fn;
}
/**
 * @public
 * @param {string} text
 * @param {string} font
 * @return {number} width
 */


function getWidth(text, font) {
  font = font || DEFAULT_FONT;
  var key = text + ':' + font;

  if (textWidthCache[key]) {
    return textWidthCache[key];
  }

  var textLines = (text + '').split('\n');
  var width = 0;

  for (var i = 0, l = textLines.length; i < l; i++) {
    // textContain.measureText may be overrided in SVG or VML
    width = Math.max(measureText(textLines[i], font).width, width);
  }

  if (textWidthCacheCounter > TEXT_CACHE_MAX) {
    textWidthCacheCounter = 0;
    textWidthCache = {};
  }

  textWidthCacheCounter++;
  textWidthCache[key] = width;
  return width;
}
/**
 * @public
 * @param {string} text
 * @param {string} font
 * @param {string} [textAlign='left']
 * @param {string} [textVerticalAlign='top']
 * @param {Array.<number>} [textPadding]
 * @param {Object} [rich]
 * @param {Object} [truncate]
 * @return {Object} {x, y, width, height, lineHeight}
 */


function getBoundingRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, rich, truncate) {
  return rich ? getRichTextRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, rich, truncate) : getPlainTextRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, truncate);
}

function getPlainTextRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, truncate) {
  var contentBlock = parsePlainText(text, font, textPadding, textLineHeight, truncate);
  var outerWidth = getWidth(text, font);

  if (textPadding) {
    outerWidth += textPadding[1] + textPadding[3];
  }

  var outerHeight = contentBlock.outerHeight;
  var x = adjustTextX(0, outerWidth, textAlign);
  var y = adjustTextY(0, outerHeight, textVerticalAlign);
  var rect = new BoundingRect(x, y, outerWidth, outerHeight);
  rect.lineHeight = contentBlock.lineHeight;
  return rect;
}

function getRichTextRect(text, font, textAlign, textVerticalAlign, textPadding, textLineHeight, rich, truncate) {
  var contentBlock = parseRichText(text, {
    rich: rich,
    truncate: truncate,
    font: font,
    textAlign: textAlign,
    textPadding: textPadding,
    textLineHeight: textLineHeight
  });
  var outerWidth = contentBlock.outerWidth;
  var outerHeight = contentBlock.outerHeight;
  var x = adjustTextX(0, outerWidth, textAlign);
  var y = adjustTextY(0, outerHeight, textVerticalAlign);
  return new BoundingRect(x, y, outerWidth, outerHeight);
}
/**
 * @public
 * @param {number} x
 * @param {number} width
 * @param {string} [textAlign='left']
 * @return {number} Adjusted x.
 */


function adjustTextX(x, width, textAlign) {
  // FIXME Right to left language
  if (textAlign === 'right') {
    x -= width;
  } else if (textAlign === 'center') {
    x -= width / 2;
  }

  return x;
}
/**
 * @public
 * @param {number} y
 * @param {number} height
 * @param {string} [textVerticalAlign='top']
 * @return {number} Adjusted y.
 */


function adjustTextY(y, height, textVerticalAlign) {
  if (textVerticalAlign === 'middle') {
    y -= height / 2;
  } else if (textVerticalAlign === 'bottom') {
    y -= height;
  }

  return y;
}
/**
 * @public
 * @param {stirng} textPosition
 * @param {Object} rect {x, y, width, height}
 * @param {number} distance
 * @return {Object} {x, y, textAlign, textVerticalAlign}
 */


function adjustTextPositionOnRect(textPosition, rect, distance) {
  var x = rect.x;
  var y = rect.y;
  var height = rect.height;
  var width = rect.width;
  var halfHeight = height / 2;
  var textAlign = 'left';
  var textVerticalAlign = 'top';

  switch (textPosition) {
    case 'left':
      x -= distance;
      y += halfHeight;
      textAlign = 'right';
      textVerticalAlign = 'middle';
      break;

    case 'right':
      x += distance + width;
      y += halfHeight;
      textVerticalAlign = 'middle';
      break;

    case 'top':
      x += width / 2;
      y -= distance;
      textAlign = 'center';
      textVerticalAlign = 'bottom';
      break;

    case 'bottom':
      x += width / 2;
      y += height + distance;
      textAlign = 'center';
      break;

    case 'inside':
      x += width / 2;
      y += halfHeight;
      textAlign = 'center';
      textVerticalAlign = 'middle';
      break;

    case 'insideLeft':
      x += distance;
      y += halfHeight;
      textVerticalAlign = 'middle';
      break;

    case 'insideRight':
      x += width - distance;
      y += halfHeight;
      textAlign = 'right';
      textVerticalAlign = 'middle';
      break;

    case 'insideTop':
      x += width / 2;
      y += distance;
      textAlign = 'center';
      break;

    case 'insideBottom':
      x += width / 2;
      y += height - distance;
      textAlign = 'center';
      textVerticalAlign = 'bottom';
      break;

    case 'insideTopLeft':
      x += distance;
      y += distance;
      break;

    case 'insideTopRight':
      x += width - distance;
      y += distance;
      textAlign = 'right';
      break;

    case 'insideBottomLeft':
      x += distance;
      y += height - distance;
      textVerticalAlign = 'bottom';
      break;

    case 'insideBottomRight':
      x += width - distance;
      y += height - distance;
      textAlign = 'right';
      textVerticalAlign = 'bottom';
      break;
  }

  return {
    x: x,
    y: y,
    textAlign: textAlign,
    textVerticalAlign: textVerticalAlign
  };
}
/**
 * Show ellipsis if overflow.
 *
 * @public
 * @param  {string} text
 * @param  {string} containerWidth
 * @param  {string} font
 * @param  {number} [ellipsis='...']
 * @param  {Object} [options]
 * @param  {number} [options.maxIterations=3]
 * @param  {number} [options.minChar=0] If truncate result are less
 *                  then minChar, ellipsis will not show, which is
 *                  better for user hint in some cases.
 * @param  {number} [options.placeholder=''] When all truncated, use the placeholder.
 * @return {string}
 */


function truncateText(text, containerWidth, font, ellipsis, options) {
  if (!containerWidth) {
    return '';
  }

  var textLines = (text + '').split('\n');
  options = prepareTruncateOptions(containerWidth, font, ellipsis, options); // FIXME
  // It is not appropriate that every line has '...' when truncate multiple lines.

  for (var i = 0, len = textLines.length; i < len; i++) {
    textLines[i] = truncateSingleLine(textLines[i], options);
  }

  return textLines.join('\n');
}

function prepareTruncateOptions(containerWidth, font, ellipsis, options) {
  options = extend({}, options);
  options.font = font;
  var ellipsis = retrieve2(ellipsis, '...');
  options.maxIterations = retrieve2(options.maxIterations, 2);
  var minChar = options.minChar = retrieve2(options.minChar, 0); // FIXME
  // Other languages?

  options.cnCharWidth = getWidth('国', font); // FIXME
  // Consider proportional font?

  var ascCharWidth = options.ascCharWidth = getWidth('a', font);
  options.placeholder = retrieve2(options.placeholder, ''); // Example 1: minChar: 3, text: 'asdfzxcv', truncate result: 'asdf', but not: 'a...'.
  // Example 2: minChar: 3, text: '维度', truncate result: '维', but not: '...'.

  var contentWidth = containerWidth = Math.max(0, containerWidth - 1); // Reserve some gap.

  for (var i = 0; i < minChar && contentWidth >= ascCharWidth; i++) {
    contentWidth -= ascCharWidth;
  }

  var ellipsisWidth = getWidth(ellipsis, font);

  if (ellipsisWidth > contentWidth) {
    ellipsis = '';
    ellipsisWidth = 0;
  }

  contentWidth = containerWidth - ellipsisWidth;
  options.ellipsis = ellipsis;
  options.ellipsisWidth = ellipsisWidth;
  options.contentWidth = contentWidth;
  options.containerWidth = containerWidth;
  return options;
}

function truncateSingleLine(textLine, options) {
  var containerWidth = options.containerWidth;
  var font = options.font;
  var contentWidth = options.contentWidth;

  if (!containerWidth) {
    return '';
  }

  var lineWidth = getWidth(textLine, font);

  if (lineWidth <= containerWidth) {
    return textLine;
  }

  for (var j = 0;; j++) {
    if (lineWidth <= contentWidth || j >= options.maxIterations) {
      textLine += options.ellipsis;
      break;
    }

    var subLength = j === 0 ? estimateLength(textLine, contentWidth, options.ascCharWidth, options.cnCharWidth) : lineWidth > 0 ? Math.floor(textLine.length * contentWidth / lineWidth) : 0;
    textLine = textLine.substr(0, subLength);
    lineWidth = getWidth(textLine, font);
  }

  if (textLine === '') {
    textLine = options.placeholder;
  }

  return textLine;
}

function estimateLength(text, contentWidth, ascCharWidth, cnCharWidth) {
  var width = 0;
  var i = 0;

  for (var len = text.length; i < len && width < contentWidth; i++) {
    var charCode = text.charCodeAt(i);
    width += 0 <= charCode && charCode <= 127 ? ascCharWidth : cnCharWidth;
  }

  return i;
}
/**
 * @public
 * @param {string} font
 * @return {number} line height
 */


function getLineHeight(font) {
  // FIXME A rough approach.
  return getWidth('国', font);
}
/**
 * @public
 * @param {string} text
 * @param {string} font
 * @return {Object} width
 */


function measureText(text, font) {
  return methods.measureText(text, font);
} // Avoid assign to an exported variable, for transforming to cjs.


methods.measureText = function (text, font) {
  var ctx = getContext();
  ctx.font = font || DEFAULT_FONT;
  return ctx.measureText(text);
};
/**
 * @public
 * @param {string} text
 * @param {string} font
 * @param {Object} [truncate]
 * @return {Object} block: {lineHeight, lines, height, outerHeight}
 *  Notice: for performance, do not calculate outerWidth util needed.
 */


function parsePlainText(text, font, padding, textLineHeight, truncate) {
  text != null && (text += '');
  var lineHeight = retrieve2(textLineHeight, getLineHeight(font));
  var lines = text ? text.split('\n') : [];
  var height = lines.length * lineHeight;
  var outerHeight = height;

  if (padding) {
    outerHeight += padding[0] + padding[2];
  }

  if (text && truncate) {
    var truncOuterHeight = truncate.outerHeight;
    var truncOuterWidth = truncate.outerWidth;

    if (truncOuterHeight != null && outerHeight > truncOuterHeight) {
      text = '';
      lines = [];
    } else if (truncOuterWidth != null) {
      var options = prepareTruncateOptions(truncOuterWidth - (padding ? padding[1] + padding[3] : 0), font, truncate.ellipsis, {
        minChar: truncate.minChar,
        placeholder: truncate.placeholder
      }); // FIXME
      // It is not appropriate that every line has '...' when truncate multiple lines.

      for (var i = 0, len = lines.length; i < len; i++) {
        lines[i] = truncateSingleLine(lines[i], options);
      }
    }
  }

  return {
    lines: lines,
    height: height,
    outerHeight: outerHeight,
    lineHeight: lineHeight
  };
}
/**
 * For example: 'some text {a|some text}other text{b|some text}xxx{c|}xxx'
 * Also consider 'bbbb{a|xxx\nzzz}xxxx\naaaa'.
 *
 * @public
 * @param {string} text
 * @param {Object} style
 * @return {Object} block
 * {
 *      width,
 *      height,
 *      lines: [{
 *          lineHeight,
 *          width,
 *          tokens: [[{
 *              styleName,
 *              text,
 *              width,      // include textPadding
 *              height,     // include textPadding
 *              textWidth, // pure text width
 *              textHeight, // pure text height
 *              lineHeihgt,
 *              font,
 *              textAlign,
 *              textVerticalAlign
 *          }], [...], ...]
 *      }, ...]
 * }
 * If styleName is undefined, it is plain text.
 */


function parseRichText(text, style) {
  var contentBlock = {
    lines: [],
    width: 0,
    height: 0
  };
  text != null && (text += '');

  if (!text) {
    return contentBlock;
  }

  var lastIndex = STYLE_REG.lastIndex = 0;
  var result;

  while ((result = STYLE_REG.exec(text)) != null) {
    var matchedIndex = result.index;

    if (matchedIndex > lastIndex) {
      pushTokens(contentBlock, text.substring(lastIndex, matchedIndex));
    }

    pushTokens(contentBlock, result[2], result[1]);
    lastIndex = STYLE_REG.lastIndex;
  }

  if (lastIndex < text.length) {
    pushTokens(contentBlock, text.substring(lastIndex, text.length));
  }

  var lines = contentBlock.lines;
  var contentHeight = 0;
  var contentWidth = 0; // For `textWidth: 100%`

  var pendingList = [];
  var stlPadding = style.textPadding;
  var truncate = style.truncate;
  var truncateWidth = truncate && truncate.outerWidth;
  var truncateHeight = truncate && truncate.outerHeight;

  if (stlPadding) {
    truncateWidth != null && (truncateWidth -= stlPadding[1] + stlPadding[3]);
    truncateHeight != null && (truncateHeight -= stlPadding[0] + stlPadding[2]);
  } // Calculate layout info of tokens.


  for (var i = 0; i < lines.length; i++) {
    var line = lines[i];
    var lineHeight = 0;
    var lineWidth = 0;

    for (var j = 0; j < line.tokens.length; j++) {
      var token = line.tokens[j];
      var tokenStyle = token.styleName && style.rich[token.styleName] || {}; // textPadding should not inherit from style.

      var textPadding = token.textPadding = tokenStyle.textPadding; // textFont has been asigned to font by `normalizeStyle`.

      var font = token.font = tokenStyle.font || style.font; // textHeight can be used when textVerticalAlign is specified in token.

      var tokenHeight = token.textHeight = retrieve2( // textHeight should not be inherited, consider it can be specified
      // as box height of the block.
      tokenStyle.textHeight, getLineHeight(font));
      textPadding && (tokenHeight += textPadding[0] + textPadding[2]);
      token.height = tokenHeight;
      token.lineHeight = retrieve3(tokenStyle.textLineHeight, style.textLineHeight, tokenHeight);
      token.textAlign = tokenStyle && tokenStyle.textAlign || style.textAlign;
      token.textVerticalAlign = tokenStyle && tokenStyle.textVerticalAlign || 'middle';

      if (truncateHeight != null && contentHeight + token.lineHeight > truncateHeight) {
        return {
          lines: [],
          width: 0,
          height: 0
        };
      }

      token.textWidth = getWidth(token.text, font);
      var tokenWidth = tokenStyle.textWidth;
      var tokenWidthNotSpecified = tokenWidth == null || tokenWidth === 'auto'; // Percent width, can be `100%`, can be used in drawing separate
      // line when box width is needed to be auto.

      if (typeof tokenWidth === 'string' && tokenWidth.charAt(tokenWidth.length - 1) === '%') {
        token.percentWidth = tokenWidth;
        pendingList.push(token);
        tokenWidth = 0; // Do not truncate in this case, because there is no user case
        // and it is too complicated.
      } else {
        if (tokenWidthNotSpecified) {
          tokenWidth = token.textWidth; // FIXME: If image is not loaded and textWidth is not specified, calling
          // `getBoundingRect()` will not get correct result.

          var textBackgroundColor = tokenStyle.textBackgroundColor;
          var bgImg = textBackgroundColor && textBackgroundColor.image; // Use cases:
          // (1) If image is not loaded, it will be loaded at render phase and call
          // `dirty()` and `textBackgroundColor.image` will be replaced with the loaded
          // image, and then the right size will be calculated here at the next tick.
          // See `graphic/helper/text.js`.
          // (2) If image loaded, and `textBackgroundColor.image` is image src string,
          // use `imageHelper.findExistImage` to find cached image.
          // `imageHelper.findExistImage` will always be called here before
          // `imageHelper.createOrUpdateImage` in `graphic/helper/text.js#renderRichText`
          // which ensures that image will not be rendered before correct size calcualted.

          if (bgImg) {
            bgImg = imageHelper.findExistImage(bgImg);

            if (imageHelper.isImageReady(bgImg)) {
              tokenWidth = Math.max(tokenWidth, bgImg.width * tokenHeight / bgImg.height);
            }
          }
        }

        var paddingW = textPadding ? textPadding[1] + textPadding[3] : 0;
        tokenWidth += paddingW;
        var remianTruncWidth = truncateWidth != null ? truncateWidth - lineWidth : null;

        if (remianTruncWidth != null && remianTruncWidth < tokenWidth) {
          if (!tokenWidthNotSpecified || remianTruncWidth < paddingW) {
            token.text = '';
            token.textWidth = tokenWidth = 0;
          } else {
            token.text = truncateText(token.text, remianTruncWidth - paddingW, font, truncate.ellipsis, {
              minChar: truncate.minChar
            });
            token.textWidth = getWidth(token.text, font);
            tokenWidth = token.textWidth + paddingW;
          }
        }
      }

      lineWidth += token.width = tokenWidth;
      tokenStyle && (lineHeight = Math.max(lineHeight, token.lineHeight));
    }

    line.width = lineWidth;
    line.lineHeight = lineHeight;
    contentHeight += lineHeight;
    contentWidth = Math.max(contentWidth, lineWidth);
  }

  contentBlock.outerWidth = contentBlock.width = retrieve2(style.textWidth, contentWidth);
  contentBlock.outerHeight = contentBlock.height = retrieve2(style.textHeight, contentHeight);

  if (stlPadding) {
    contentBlock.outerWidth += stlPadding[1] + stlPadding[3];
    contentBlock.outerHeight += stlPadding[0] + stlPadding[2];
  }

  for (var i = 0; i < pendingList.length; i++) {
    var token = pendingList[i];
    var percentWidth = token.percentWidth; // Should not base on outerWidth, because token can not be placed out of padding.

    token.width = parseInt(percentWidth, 10) / 100 * contentWidth;
  }

  return contentBlock;
}

function pushTokens(block, str, styleName) {
  var isEmptyStr = str === '';
  var strs = str.split('\n');
  var lines = block.lines;

  for (var i = 0; i < strs.length; i++) {
    var text = strs[i];
    var token = {
      styleName: styleName,
      text: text,
      isLineHolder: !text && !isEmptyStr
    }; // The first token should be appended to the last line.

    if (!i) {
      var tokens = (lines[lines.length - 1] || (lines[0] = {
        tokens: []
      })).tokens; // Consider cases:
      // (1) ''.split('\n') => ['', '\n', ''], the '' at the first item
      // (which is a placeholder) should be replaced by new token.
      // (2) A image backage, where token likes {a|}.
      // (3) A redundant '' will affect textAlign in line.
      // (4) tokens with the same tplName should not be merged, because
      // they should be displayed in different box (with border and padding).

      var tokensLen = tokens.length;
      tokensLen === 1 && tokens[0].isLineHolder ? tokens[0] = token : // Consider text is '', only insert when it is the "lineHolder" or
      // "emptyStr". Otherwise a redundant '' will affect textAlign in line.
      (text || !tokensLen || isEmptyStr) && tokens.push(token);
    } // Other tokens always start a new line.
    else {
        // If there is '', insert it as a placeholder.
        lines.push({
          tokens: [token]
        });
      }
  }
}

function makeFont(style) {
  // FIXME in node-canvas fontWeight is before fontStyle
  // Use `fontSize` `fontFamily` to check whether font properties are defined.
  var font = (style.fontSize || style.fontFamily) && [style.fontStyle, style.fontWeight, (style.fontSize || 12) + 'px', // If font properties are defined, `fontFamily` should not be ignored.
  style.fontFamily || 'sans-serif'].join(' ');
  return font && trim(font) || style.textFont || style.font;
}

exports.DEFAULT_FONT = DEFAULT_FONT;
exports.$override = $override;
exports.getWidth = getWidth;
exports.getBoundingRect = getBoundingRect;
exports.adjustTextX = adjustTextX;
exports.adjustTextY = adjustTextY;
exports.adjustTextPositionOnRect = adjustTextPositionOnRect;
exports.truncateText = truncateText;
exports.getLineHeight = getLineHeight;
exports.measureText = measureText;
exports.parsePlainText = parsePlainText;
exports.parseRichText = parseRichText;
exports.makeFont = makeFont;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi90ZXh0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi90ZXh0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBCb3VuZGluZ1JlY3QgPSByZXF1aXJlKFwiLi4vY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciBpbWFnZUhlbHBlciA9IHJlcXVpcmUoXCIuLi9ncmFwaGljL2hlbHBlci9pbWFnZVwiKTtcblxudmFyIF91dGlsID0gcmVxdWlyZShcIi4uL2NvcmUvdXRpbFwiKTtcblxudmFyIGdldENvbnRleHQgPSBfdXRpbC5nZXRDb250ZXh0O1xudmFyIGV4dGVuZCA9IF91dGlsLmV4dGVuZDtcbnZhciByZXRyaWV2ZTIgPSBfdXRpbC5yZXRyaWV2ZTI7XG52YXIgcmV0cmlldmUzID0gX3V0aWwucmV0cmlldmUzO1xudmFyIHRyaW0gPSBfdXRpbC50cmltO1xudmFyIHRleHRXaWR0aENhY2hlID0ge307XG52YXIgdGV4dFdpZHRoQ2FjaGVDb3VudGVyID0gMDtcbnZhciBURVhUX0NBQ0hFX01BWCA9IDUwMDA7XG52YXIgU1RZTEVfUkVHID0gL1xceyhbYS16QS1aMC05X10rKVxcfChbXn1dKilcXH0vZztcbnZhciBERUZBVUxUX0ZPTlQgPSAnMTJweCBzYW5zLXNlcmlmJzsgLy8gQXZvaWQgYXNzaWduIHRvIGFuIGV4cG9ydGVkIHZhcmlhYmxlLCBmb3IgdHJhbnNmb3JtaW5nIHRvIGNqcy5cblxudmFyIG1ldGhvZHMgPSB7fTtcblxuZnVuY3Rpb24gJG92ZXJyaWRlKG5hbWUsIGZuKSB7XG4gIG1ldGhvZHNbbmFtZV0gPSBmbjtcbn1cbi8qKlxuICogQHB1YmxpY1xuICogQHBhcmFtIHtzdHJpbmd9IHRleHRcbiAqIEBwYXJhbSB7c3RyaW5nfSBmb250XG4gKiBAcmV0dXJuIHtudW1iZXJ9IHdpZHRoXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRXaWR0aCh0ZXh0LCBmb250KSB7XG4gIGZvbnQgPSBmb250IHx8IERFRkFVTFRfRk9OVDtcbiAgdmFyIGtleSA9IHRleHQgKyAnOicgKyBmb250O1xuXG4gIGlmICh0ZXh0V2lkdGhDYWNoZVtrZXldKSB7XG4gICAgcmV0dXJuIHRleHRXaWR0aENhY2hlW2tleV07XG4gIH1cblxuICB2YXIgdGV4dExpbmVzID0gKHRleHQgKyAnJykuc3BsaXQoJ1xcbicpO1xuICB2YXIgd2lkdGggPSAwO1xuXG4gIGZvciAodmFyIGkgPSAwLCBsID0gdGV4dExpbmVzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgIC8vIHRleHRDb250YWluLm1lYXN1cmVUZXh0IG1heSBiZSBvdmVycmlkZWQgaW4gU1ZHIG9yIFZNTFxuICAgIHdpZHRoID0gTWF0aC5tYXgobWVhc3VyZVRleHQodGV4dExpbmVzW2ldLCBmb250KS53aWR0aCwgd2lkdGgpO1xuICB9XG5cbiAgaWYgKHRleHRXaWR0aENhY2hlQ291bnRlciA+IFRFWFRfQ0FDSEVfTUFYKSB7XG4gICAgdGV4dFdpZHRoQ2FjaGVDb3VudGVyID0gMDtcbiAgICB0ZXh0V2lkdGhDYWNoZSA9IHt9O1xuICB9XG5cbiAgdGV4dFdpZHRoQ2FjaGVDb3VudGVyKys7XG4gIHRleHRXaWR0aENhY2hlW2tleV0gPSB3aWR0aDtcbiAgcmV0dXJuIHdpZHRoO1xufVxuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge3N0cmluZ30gdGV4dFxuICogQHBhcmFtIHtzdHJpbmd9IGZvbnRcbiAqIEBwYXJhbSB7c3RyaW5nfSBbdGV4dEFsaWduPSdsZWZ0J11cbiAqIEBwYXJhbSB7c3RyaW5nfSBbdGV4dFZlcnRpY2FsQWxpZ249J3RvcCddXG4gKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBbdGV4dFBhZGRpbmddXG4gKiBAcGFyYW0ge09iamVjdH0gW3JpY2hdXG4gKiBAcGFyYW0ge09iamVjdH0gW3RydW5jYXRlXVxuICogQHJldHVybiB7T2JqZWN0fSB7eCwgeSwgd2lkdGgsIGhlaWdodCwgbGluZUhlaWdodH1cbiAqL1xuXG5cbmZ1bmN0aW9uIGdldEJvdW5kaW5nUmVjdCh0ZXh0LCBmb250LCB0ZXh0QWxpZ24sIHRleHRWZXJ0aWNhbEFsaWduLCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHJpY2gsIHRydW5jYXRlKSB7XG4gIHJldHVybiByaWNoID8gZ2V0UmljaFRleHRSZWN0KHRleHQsIGZvbnQsIHRleHRBbGlnbiwgdGV4dFZlcnRpY2FsQWxpZ24sIHRleHRQYWRkaW5nLCB0ZXh0TGluZUhlaWdodCwgcmljaCwgdHJ1bmNhdGUpIDogZ2V0UGxhaW5UZXh0UmVjdCh0ZXh0LCBmb250LCB0ZXh0QWxpZ24sIHRleHRWZXJ0aWNhbEFsaWduLCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHRydW5jYXRlKTtcbn1cblxuZnVuY3Rpb24gZ2V0UGxhaW5UZXh0UmVjdCh0ZXh0LCBmb250LCB0ZXh0QWxpZ24sIHRleHRWZXJ0aWNhbEFsaWduLCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHRydW5jYXRlKSB7XG4gIHZhciBjb250ZW50QmxvY2sgPSBwYXJzZVBsYWluVGV4dCh0ZXh0LCBmb250LCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHRydW5jYXRlKTtcbiAgdmFyIG91dGVyV2lkdGggPSBnZXRXaWR0aCh0ZXh0LCBmb250KTtcblxuICBpZiAodGV4dFBhZGRpbmcpIHtcbiAgICBvdXRlcldpZHRoICs9IHRleHRQYWRkaW5nWzFdICsgdGV4dFBhZGRpbmdbM107XG4gIH1cblxuICB2YXIgb3V0ZXJIZWlnaHQgPSBjb250ZW50QmxvY2sub3V0ZXJIZWlnaHQ7XG4gIHZhciB4ID0gYWRqdXN0VGV4dFgoMCwgb3V0ZXJXaWR0aCwgdGV4dEFsaWduKTtcbiAgdmFyIHkgPSBhZGp1c3RUZXh0WSgwLCBvdXRlckhlaWdodCwgdGV4dFZlcnRpY2FsQWxpZ24pO1xuICB2YXIgcmVjdCA9IG5ldyBCb3VuZGluZ1JlY3QoeCwgeSwgb3V0ZXJXaWR0aCwgb3V0ZXJIZWlnaHQpO1xuICByZWN0LmxpbmVIZWlnaHQgPSBjb250ZW50QmxvY2subGluZUhlaWdodDtcbiAgcmV0dXJuIHJlY3Q7XG59XG5cbmZ1bmN0aW9uIGdldFJpY2hUZXh0UmVjdCh0ZXh0LCBmb250LCB0ZXh0QWxpZ24sIHRleHRWZXJ0aWNhbEFsaWduLCB0ZXh0UGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHJpY2gsIHRydW5jYXRlKSB7XG4gIHZhciBjb250ZW50QmxvY2sgPSBwYXJzZVJpY2hUZXh0KHRleHQsIHtcbiAgICByaWNoOiByaWNoLFxuICAgIHRydW5jYXRlOiB0cnVuY2F0ZSxcbiAgICBmb250OiBmb250LFxuICAgIHRleHRBbGlnbjogdGV4dEFsaWduLFxuICAgIHRleHRQYWRkaW5nOiB0ZXh0UGFkZGluZyxcbiAgICB0ZXh0TGluZUhlaWdodDogdGV4dExpbmVIZWlnaHRcbiAgfSk7XG4gIHZhciBvdXRlcldpZHRoID0gY29udGVudEJsb2NrLm91dGVyV2lkdGg7XG4gIHZhciBvdXRlckhlaWdodCA9IGNvbnRlbnRCbG9jay5vdXRlckhlaWdodDtcbiAgdmFyIHggPSBhZGp1c3RUZXh0WCgwLCBvdXRlcldpZHRoLCB0ZXh0QWxpZ24pO1xuICB2YXIgeSA9IGFkanVzdFRleHRZKDAsIG91dGVySGVpZ2h0LCB0ZXh0VmVydGljYWxBbGlnbik7XG4gIHJldHVybiBuZXcgQm91bmRpbmdSZWN0KHgsIHksIG91dGVyV2lkdGgsIG91dGVySGVpZ2h0KTtcbn1cbi8qKlxuICogQHB1YmxpY1xuICogQHBhcmFtIHtudW1iZXJ9IHhcbiAqIEBwYXJhbSB7bnVtYmVyfSB3aWR0aFxuICogQHBhcmFtIHtzdHJpbmd9IFt0ZXh0QWxpZ249J2xlZnQnXVxuICogQHJldHVybiB7bnVtYmVyfSBBZGp1c3RlZCB4LlxuICovXG5cblxuZnVuY3Rpb24gYWRqdXN0VGV4dFgoeCwgd2lkdGgsIHRleHRBbGlnbikge1xuICAvLyBGSVhNRSBSaWdodCB0byBsZWZ0IGxhbmd1YWdlXG4gIGlmICh0ZXh0QWxpZ24gPT09ICdyaWdodCcpIHtcbiAgICB4IC09IHdpZHRoO1xuICB9IGVsc2UgaWYgKHRleHRBbGlnbiA9PT0gJ2NlbnRlcicpIHtcbiAgICB4IC09IHdpZHRoIC8gMjtcbiAgfVxuXG4gIHJldHVybiB4O1xufVxuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge251bWJlcn0geVxuICogQHBhcmFtIHtudW1iZXJ9IGhlaWdodFxuICogQHBhcmFtIHtzdHJpbmd9IFt0ZXh0VmVydGljYWxBbGlnbj0ndG9wJ11cbiAqIEByZXR1cm4ge251bWJlcn0gQWRqdXN0ZWQgeS5cbiAqL1xuXG5cbmZ1bmN0aW9uIGFkanVzdFRleHRZKHksIGhlaWdodCwgdGV4dFZlcnRpY2FsQWxpZ24pIHtcbiAgaWYgKHRleHRWZXJ0aWNhbEFsaWduID09PSAnbWlkZGxlJykge1xuICAgIHkgLT0gaGVpZ2h0IC8gMjtcbiAgfSBlbHNlIGlmICh0ZXh0VmVydGljYWxBbGlnbiA9PT0gJ2JvdHRvbScpIHtcbiAgICB5IC09IGhlaWdodDtcbiAgfVxuXG4gIHJldHVybiB5O1xufVxuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge3N0aXJuZ30gdGV4dFBvc2l0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcmVjdCB7eCwgeSwgd2lkdGgsIGhlaWdodH1cbiAqIEBwYXJhbSB7bnVtYmVyfSBkaXN0YW5jZVxuICogQHJldHVybiB7T2JqZWN0fSB7eCwgeSwgdGV4dEFsaWduLCB0ZXh0VmVydGljYWxBbGlnbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGFkanVzdFRleHRQb3NpdGlvbk9uUmVjdCh0ZXh0UG9zaXRpb24sIHJlY3QsIGRpc3RhbmNlKSB7XG4gIHZhciB4ID0gcmVjdC54O1xuICB2YXIgeSA9IHJlY3QueTtcbiAgdmFyIGhlaWdodCA9IHJlY3QuaGVpZ2h0O1xuICB2YXIgd2lkdGggPSByZWN0LndpZHRoO1xuICB2YXIgaGFsZkhlaWdodCA9IGhlaWdodCAvIDI7XG4gIHZhciB0ZXh0QWxpZ24gPSAnbGVmdCc7XG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbiA9ICd0b3AnO1xuXG4gIHN3aXRjaCAodGV4dFBvc2l0aW9uKSB7XG4gICAgY2FzZSAnbGVmdCc6XG4gICAgICB4IC09IGRpc3RhbmNlO1xuICAgICAgeSArPSBoYWxmSGVpZ2h0O1xuICAgICAgdGV4dEFsaWduID0gJ3JpZ2h0JztcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduID0gJ21pZGRsZSc7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ3JpZ2h0JzpcbiAgICAgIHggKz0gZGlzdGFuY2UgKyB3aWR0aDtcbiAgICAgIHkgKz0gaGFsZkhlaWdodDtcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduID0gJ21pZGRsZSc7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ3RvcCc6XG4gICAgICB4ICs9IHdpZHRoIC8gMjtcbiAgICAgIHkgLT0gZGlzdGFuY2U7XG4gICAgICB0ZXh0QWxpZ24gPSAnY2VudGVyJztcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduID0gJ2JvdHRvbSc7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ2JvdHRvbSc6XG4gICAgICB4ICs9IHdpZHRoIC8gMjtcbiAgICAgIHkgKz0gaGVpZ2h0ICsgZGlzdGFuY2U7XG4gICAgICB0ZXh0QWxpZ24gPSAnY2VudGVyJztcbiAgICAgIGJyZWFrO1xuXG4gICAgY2FzZSAnaW5zaWRlJzpcbiAgICAgIHggKz0gd2lkdGggLyAyO1xuICAgICAgeSArPSBoYWxmSGVpZ2h0O1xuICAgICAgdGV4dEFsaWduID0gJ2NlbnRlcic7XG4gICAgICB0ZXh0VmVydGljYWxBbGlnbiA9ICdtaWRkbGUnO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdpbnNpZGVMZWZ0JzpcbiAgICAgIHggKz0gZGlzdGFuY2U7XG4gICAgICB5ICs9IGhhbGZIZWlnaHQ7XG4gICAgICB0ZXh0VmVydGljYWxBbGlnbiA9ICdtaWRkbGUnO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdpbnNpZGVSaWdodCc6XG4gICAgICB4ICs9IHdpZHRoIC0gZGlzdGFuY2U7XG4gICAgICB5ICs9IGhhbGZIZWlnaHQ7XG4gICAgICB0ZXh0QWxpZ24gPSAncmlnaHQnO1xuICAgICAgdGV4dFZlcnRpY2FsQWxpZ24gPSAnbWlkZGxlJztcbiAgICAgIGJyZWFrO1xuXG4gICAgY2FzZSAnaW5zaWRlVG9wJzpcbiAgICAgIHggKz0gd2lkdGggLyAyO1xuICAgICAgeSArPSBkaXN0YW5jZTtcbiAgICAgIHRleHRBbGlnbiA9ICdjZW50ZXInO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdpbnNpZGVCb3R0b20nOlxuICAgICAgeCArPSB3aWR0aCAvIDI7XG4gICAgICB5ICs9IGhlaWdodCAtIGRpc3RhbmNlO1xuICAgICAgdGV4dEFsaWduID0gJ2NlbnRlcic7XG4gICAgICB0ZXh0VmVydGljYWxBbGlnbiA9ICdib3R0b20nO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdpbnNpZGVUb3BMZWZ0JzpcbiAgICAgIHggKz0gZGlzdGFuY2U7XG4gICAgICB5ICs9IGRpc3RhbmNlO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdpbnNpZGVUb3BSaWdodCc6XG4gICAgICB4ICs9IHdpZHRoIC0gZGlzdGFuY2U7XG4gICAgICB5ICs9IGRpc3RhbmNlO1xuICAgICAgdGV4dEFsaWduID0gJ3JpZ2h0JztcbiAgICAgIGJyZWFrO1xuXG4gICAgY2FzZSAnaW5zaWRlQm90dG9tTGVmdCc6XG4gICAgICB4ICs9IGRpc3RhbmNlO1xuICAgICAgeSArPSBoZWlnaHQgLSBkaXN0YW5jZTtcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduID0gJ2JvdHRvbSc7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ2luc2lkZUJvdHRvbVJpZ2h0JzpcbiAgICAgIHggKz0gd2lkdGggLSBkaXN0YW5jZTtcbiAgICAgIHkgKz0gaGVpZ2h0IC0gZGlzdGFuY2U7XG4gICAgICB0ZXh0QWxpZ24gPSAncmlnaHQnO1xuICAgICAgdGV4dFZlcnRpY2FsQWxpZ24gPSAnYm90dG9tJztcbiAgICAgIGJyZWFrO1xuICB9XG5cbiAgcmV0dXJuIHtcbiAgICB4OiB4LFxuICAgIHk6IHksXG4gICAgdGV4dEFsaWduOiB0ZXh0QWxpZ24sXG4gICAgdGV4dFZlcnRpY2FsQWxpZ246IHRleHRWZXJ0aWNhbEFsaWduXG4gIH07XG59XG4vKipcbiAqIFNob3cgZWxsaXBzaXMgaWYgb3ZlcmZsb3cuXG4gKlxuICogQHB1YmxpY1xuICogQHBhcmFtICB7c3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0gIHtzdHJpbmd9IGNvbnRhaW5lcldpZHRoXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGZvbnRcbiAqIEBwYXJhbSAge251bWJlcn0gW2VsbGlwc2lzPScuLi4nXVxuICogQHBhcmFtICB7T2JqZWN0fSBbb3B0aW9uc11cbiAqIEBwYXJhbSAge251bWJlcn0gW29wdGlvbnMubWF4SXRlcmF0aW9ucz0zXVxuICogQHBhcmFtICB7bnVtYmVyfSBbb3B0aW9ucy5taW5DaGFyPTBdIElmIHRydW5jYXRlIHJlc3VsdCBhcmUgbGVzc1xuICogICAgICAgICAgICAgICAgICB0aGVuIG1pbkNoYXIsIGVsbGlwc2lzIHdpbGwgbm90IHNob3csIHdoaWNoIGlzXG4gKiAgICAgICAgICAgICAgICAgIGJldHRlciBmb3IgdXNlciBoaW50IGluIHNvbWUgY2FzZXMuXG4gKiBAcGFyYW0gIHtudW1iZXJ9IFtvcHRpb25zLnBsYWNlaG9sZGVyPScnXSBXaGVuIGFsbCB0cnVuY2F0ZWQsIHVzZSB0aGUgcGxhY2Vob2xkZXIuXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cblxuXG5mdW5jdGlvbiB0cnVuY2F0ZVRleHQodGV4dCwgY29udGFpbmVyV2lkdGgsIGZvbnQsIGVsbGlwc2lzLCBvcHRpb25zKSB7XG4gIGlmICghY29udGFpbmVyV2lkdGgpIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICB2YXIgdGV4dExpbmVzID0gKHRleHQgKyAnJykuc3BsaXQoJ1xcbicpO1xuICBvcHRpb25zID0gcHJlcGFyZVRydW5jYXRlT3B0aW9ucyhjb250YWluZXJXaWR0aCwgZm9udCwgZWxsaXBzaXMsIG9wdGlvbnMpOyAvLyBGSVhNRVxuICAvLyBJdCBpcyBub3QgYXBwcm9wcmlhdGUgdGhhdCBldmVyeSBsaW5lIGhhcyAnLi4uJyB3aGVuIHRydW5jYXRlIG11bHRpcGxlIGxpbmVzLlxuXG4gIGZvciAodmFyIGkgPSAwLCBsZW4gPSB0ZXh0TGluZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICB0ZXh0TGluZXNbaV0gPSB0cnVuY2F0ZVNpbmdsZUxpbmUodGV4dExpbmVzW2ldLCBvcHRpb25zKTtcbiAgfVxuXG4gIHJldHVybiB0ZXh0TGluZXMuam9pbignXFxuJyk7XG59XG5cbmZ1bmN0aW9uIHByZXBhcmVUcnVuY2F0ZU9wdGlvbnMoY29udGFpbmVyV2lkdGgsIGZvbnQsIGVsbGlwc2lzLCBvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBleHRlbmQoe30sIG9wdGlvbnMpO1xuICBvcHRpb25zLmZvbnQgPSBmb250O1xuICB2YXIgZWxsaXBzaXMgPSByZXRyaWV2ZTIoZWxsaXBzaXMsICcuLi4nKTtcbiAgb3B0aW9ucy5tYXhJdGVyYXRpb25zID0gcmV0cmlldmUyKG9wdGlvbnMubWF4SXRlcmF0aW9ucywgMik7XG4gIHZhciBtaW5DaGFyID0gb3B0aW9ucy5taW5DaGFyID0gcmV0cmlldmUyKG9wdGlvbnMubWluQ2hhciwgMCk7IC8vIEZJWE1FXG4gIC8vIE90aGVyIGxhbmd1YWdlcz9cblxuICBvcHRpb25zLmNuQ2hhcldpZHRoID0gZ2V0V2lkdGgoJ+WbvScsIGZvbnQpOyAvLyBGSVhNRVxuICAvLyBDb25zaWRlciBwcm9wb3J0aW9uYWwgZm9udD9cblxuICB2YXIgYXNjQ2hhcldpZHRoID0gb3B0aW9ucy5hc2NDaGFyV2lkdGggPSBnZXRXaWR0aCgnYScsIGZvbnQpO1xuICBvcHRpb25zLnBsYWNlaG9sZGVyID0gcmV0cmlldmUyKG9wdGlvbnMucGxhY2Vob2xkZXIsICcnKTsgLy8gRXhhbXBsZSAxOiBtaW5DaGFyOiAzLCB0ZXh0OiAnYXNkZnp4Y3YnLCB0cnVuY2F0ZSByZXN1bHQ6ICdhc2RmJywgYnV0IG5vdDogJ2EuLi4nLlxuICAvLyBFeGFtcGxlIDI6IG1pbkNoYXI6IDMsIHRleHQ6ICfnu7TluqYnLCB0cnVuY2F0ZSByZXN1bHQ6ICfnu7QnLCBidXQgbm90OiAnLi4uJy5cblxuICB2YXIgY29udGVudFdpZHRoID0gY29udGFpbmVyV2lkdGggPSBNYXRoLm1heCgwLCBjb250YWluZXJXaWR0aCAtIDEpOyAvLyBSZXNlcnZlIHNvbWUgZ2FwLlxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbWluQ2hhciAmJiBjb250ZW50V2lkdGggPj0gYXNjQ2hhcldpZHRoOyBpKyspIHtcbiAgICBjb250ZW50V2lkdGggLT0gYXNjQ2hhcldpZHRoO1xuICB9XG5cbiAgdmFyIGVsbGlwc2lzV2lkdGggPSBnZXRXaWR0aChlbGxpcHNpcywgZm9udCk7XG5cbiAgaWYgKGVsbGlwc2lzV2lkdGggPiBjb250ZW50V2lkdGgpIHtcbiAgICBlbGxpcHNpcyA9ICcnO1xuICAgIGVsbGlwc2lzV2lkdGggPSAwO1xuICB9XG5cbiAgY29udGVudFdpZHRoID0gY29udGFpbmVyV2lkdGggLSBlbGxpcHNpc1dpZHRoO1xuICBvcHRpb25zLmVsbGlwc2lzID0gZWxsaXBzaXM7XG4gIG9wdGlvbnMuZWxsaXBzaXNXaWR0aCA9IGVsbGlwc2lzV2lkdGg7XG4gIG9wdGlvbnMuY29udGVudFdpZHRoID0gY29udGVudFdpZHRoO1xuICBvcHRpb25zLmNvbnRhaW5lcldpZHRoID0gY29udGFpbmVyV2lkdGg7XG4gIHJldHVybiBvcHRpb25zO1xufVxuXG5mdW5jdGlvbiB0cnVuY2F0ZVNpbmdsZUxpbmUodGV4dExpbmUsIG9wdGlvbnMpIHtcbiAgdmFyIGNvbnRhaW5lcldpZHRoID0gb3B0aW9ucy5jb250YWluZXJXaWR0aDtcbiAgdmFyIGZvbnQgPSBvcHRpb25zLmZvbnQ7XG4gIHZhciBjb250ZW50V2lkdGggPSBvcHRpb25zLmNvbnRlbnRXaWR0aDtcblxuICBpZiAoIWNvbnRhaW5lcldpZHRoKSB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgdmFyIGxpbmVXaWR0aCA9IGdldFdpZHRoKHRleHRMaW5lLCBmb250KTtcblxuICBpZiAobGluZVdpZHRoIDw9IGNvbnRhaW5lcldpZHRoKSB7XG4gICAgcmV0dXJuIHRleHRMaW5lO1xuICB9XG5cbiAgZm9yICh2YXIgaiA9IDA7OyBqKyspIHtcbiAgICBpZiAobGluZVdpZHRoIDw9IGNvbnRlbnRXaWR0aCB8fCBqID49IG9wdGlvbnMubWF4SXRlcmF0aW9ucykge1xuICAgICAgdGV4dExpbmUgKz0gb3B0aW9ucy5lbGxpcHNpcztcbiAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIHZhciBzdWJMZW5ndGggPSBqID09PSAwID8gZXN0aW1hdGVMZW5ndGgodGV4dExpbmUsIGNvbnRlbnRXaWR0aCwgb3B0aW9ucy5hc2NDaGFyV2lkdGgsIG9wdGlvbnMuY25DaGFyV2lkdGgpIDogbGluZVdpZHRoID4gMCA/IE1hdGguZmxvb3IodGV4dExpbmUubGVuZ3RoICogY29udGVudFdpZHRoIC8gbGluZVdpZHRoKSA6IDA7XG4gICAgdGV4dExpbmUgPSB0ZXh0TGluZS5zdWJzdHIoMCwgc3ViTGVuZ3RoKTtcbiAgICBsaW5lV2lkdGggPSBnZXRXaWR0aCh0ZXh0TGluZSwgZm9udCk7XG4gIH1cblxuICBpZiAodGV4dExpbmUgPT09ICcnKSB7XG4gICAgdGV4dExpbmUgPSBvcHRpb25zLnBsYWNlaG9sZGVyO1xuICB9XG5cbiAgcmV0dXJuIHRleHRMaW5lO1xufVxuXG5mdW5jdGlvbiBlc3RpbWF0ZUxlbmd0aCh0ZXh0LCBjb250ZW50V2lkdGgsIGFzY0NoYXJXaWR0aCwgY25DaGFyV2lkdGgpIHtcbiAgdmFyIHdpZHRoID0gMDtcbiAgdmFyIGkgPSAwO1xuXG4gIGZvciAodmFyIGxlbiA9IHRleHQubGVuZ3RoOyBpIDwgbGVuICYmIHdpZHRoIDwgY29udGVudFdpZHRoOyBpKyspIHtcbiAgICB2YXIgY2hhckNvZGUgPSB0ZXh0LmNoYXJDb2RlQXQoaSk7XG4gICAgd2lkdGggKz0gMCA8PSBjaGFyQ29kZSAmJiBjaGFyQ29kZSA8PSAxMjcgPyBhc2NDaGFyV2lkdGggOiBjbkNoYXJXaWR0aDtcbiAgfVxuXG4gIHJldHVybiBpO1xufVxuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge3N0cmluZ30gZm9udFxuICogQHJldHVybiB7bnVtYmVyfSBsaW5lIGhlaWdodFxuICovXG5cblxuZnVuY3Rpb24gZ2V0TGluZUhlaWdodChmb250KSB7XG4gIC8vIEZJWE1FIEEgcm91Z2ggYXBwcm9hY2guXG4gIHJldHVybiBnZXRXaWR0aCgn5Zu9JywgZm9udCk7XG59XG4vKipcbiAqIEBwdWJsaWNcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0ge3N0cmluZ30gZm9udFxuICogQHJldHVybiB7T2JqZWN0fSB3aWR0aFxuICovXG5cblxuZnVuY3Rpb24gbWVhc3VyZVRleHQodGV4dCwgZm9udCkge1xuICByZXR1cm4gbWV0aG9kcy5tZWFzdXJlVGV4dCh0ZXh0LCBmb250KTtcbn0gLy8gQXZvaWQgYXNzaWduIHRvIGFuIGV4cG9ydGVkIHZhcmlhYmxlLCBmb3IgdHJhbnNmb3JtaW5nIHRvIGNqcy5cblxuXG5tZXRob2RzLm1lYXN1cmVUZXh0ID0gZnVuY3Rpb24gKHRleHQsIGZvbnQpIHtcbiAgdmFyIGN0eCA9IGdldENvbnRleHQoKTtcbiAgY3R4LmZvbnQgPSBmb250IHx8IERFRkFVTFRfRk9OVDtcbiAgcmV0dXJuIGN0eC5tZWFzdXJlVGV4dCh0ZXh0KTtcbn07XG4vKipcbiAqIEBwdWJsaWNcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0ge3N0cmluZ30gZm9udFxuICogQHBhcmFtIHtPYmplY3R9IFt0cnVuY2F0ZV1cbiAqIEByZXR1cm4ge09iamVjdH0gYmxvY2s6IHtsaW5lSGVpZ2h0LCBsaW5lcywgaGVpZ2h0LCBvdXRlckhlaWdodH1cbiAqICBOb3RpY2U6IGZvciBwZXJmb3JtYW5jZSwgZG8gbm90IGNhbGN1bGF0ZSBvdXRlcldpZHRoIHV0aWwgbmVlZGVkLlxuICovXG5cblxuZnVuY3Rpb24gcGFyc2VQbGFpblRleHQodGV4dCwgZm9udCwgcGFkZGluZywgdGV4dExpbmVIZWlnaHQsIHRydW5jYXRlKSB7XG4gIHRleHQgIT0gbnVsbCAmJiAodGV4dCArPSAnJyk7XG4gIHZhciBsaW5lSGVpZ2h0ID0gcmV0cmlldmUyKHRleHRMaW5lSGVpZ2h0LCBnZXRMaW5lSGVpZ2h0KGZvbnQpKTtcbiAgdmFyIGxpbmVzID0gdGV4dCA/IHRleHQuc3BsaXQoJ1xcbicpIDogW107XG4gIHZhciBoZWlnaHQgPSBsaW5lcy5sZW5ndGggKiBsaW5lSGVpZ2h0O1xuICB2YXIgb3V0ZXJIZWlnaHQgPSBoZWlnaHQ7XG5cbiAgaWYgKHBhZGRpbmcpIHtcbiAgICBvdXRlckhlaWdodCArPSBwYWRkaW5nWzBdICsgcGFkZGluZ1syXTtcbiAgfVxuXG4gIGlmICh0ZXh0ICYmIHRydW5jYXRlKSB7XG4gICAgdmFyIHRydW5jT3V0ZXJIZWlnaHQgPSB0cnVuY2F0ZS5vdXRlckhlaWdodDtcbiAgICB2YXIgdHJ1bmNPdXRlcldpZHRoID0gdHJ1bmNhdGUub3V0ZXJXaWR0aDtcblxuICAgIGlmICh0cnVuY091dGVySGVpZ2h0ICE9IG51bGwgJiYgb3V0ZXJIZWlnaHQgPiB0cnVuY091dGVySGVpZ2h0KSB7XG4gICAgICB0ZXh0ID0gJyc7XG4gICAgICBsaW5lcyA9IFtdO1xuICAgIH0gZWxzZSBpZiAodHJ1bmNPdXRlcldpZHRoICE9IG51bGwpIHtcbiAgICAgIHZhciBvcHRpb25zID0gcHJlcGFyZVRydW5jYXRlT3B0aW9ucyh0cnVuY091dGVyV2lkdGggLSAocGFkZGluZyA/IHBhZGRpbmdbMV0gKyBwYWRkaW5nWzNdIDogMCksIGZvbnQsIHRydW5jYXRlLmVsbGlwc2lzLCB7XG4gICAgICAgIG1pbkNoYXI6IHRydW5jYXRlLm1pbkNoYXIsXG4gICAgICAgIHBsYWNlaG9sZGVyOiB0cnVuY2F0ZS5wbGFjZWhvbGRlclxuICAgICAgfSk7IC8vIEZJWE1FXG4gICAgICAvLyBJdCBpcyBub3QgYXBwcm9wcmlhdGUgdGhhdCBldmVyeSBsaW5lIGhhcyAnLi4uJyB3aGVuIHRydW5jYXRlIG11bHRpcGxlIGxpbmVzLlxuXG4gICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gbGluZXMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgbGluZXNbaV0gPSB0cnVuY2F0ZVNpbmdsZUxpbmUobGluZXNbaV0sIG9wdGlvbnMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgbGluZXM6IGxpbmVzLFxuICAgIGhlaWdodDogaGVpZ2h0LFxuICAgIG91dGVySGVpZ2h0OiBvdXRlckhlaWdodCxcbiAgICBsaW5lSGVpZ2h0OiBsaW5lSGVpZ2h0XG4gIH07XG59XG4vKipcbiAqIEZvciBleGFtcGxlOiAnc29tZSB0ZXh0IHthfHNvbWUgdGV4dH1vdGhlciB0ZXh0e2J8c29tZSB0ZXh0fXh4eHtjfH14eHgnXG4gKiBBbHNvIGNvbnNpZGVyICdiYmJie2F8eHh4XFxuenp6fXh4eHhcXG5hYWFhJy5cbiAqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge3N0cmluZ30gdGV4dFxuICogQHBhcmFtIHtPYmplY3R9IHN0eWxlXG4gKiBAcmV0dXJuIHtPYmplY3R9IGJsb2NrXG4gKiB7XG4gKiAgICAgIHdpZHRoLFxuICogICAgICBoZWlnaHQsXG4gKiAgICAgIGxpbmVzOiBbe1xuICogICAgICAgICAgbGluZUhlaWdodCxcbiAqICAgICAgICAgIHdpZHRoLFxuICogICAgICAgICAgdG9rZW5zOiBbW3tcbiAqICAgICAgICAgICAgICBzdHlsZU5hbWUsXG4gKiAgICAgICAgICAgICAgdGV4dCxcbiAqICAgICAgICAgICAgICB3aWR0aCwgICAgICAvLyBpbmNsdWRlIHRleHRQYWRkaW5nXG4gKiAgICAgICAgICAgICAgaGVpZ2h0LCAgICAgLy8gaW5jbHVkZSB0ZXh0UGFkZGluZ1xuICogICAgICAgICAgICAgIHRleHRXaWR0aCwgLy8gcHVyZSB0ZXh0IHdpZHRoXG4gKiAgICAgICAgICAgICAgdGV4dEhlaWdodCwgLy8gcHVyZSB0ZXh0IGhlaWdodFxuICogICAgICAgICAgICAgIGxpbmVIZWloZ3QsXG4gKiAgICAgICAgICAgICAgZm9udCxcbiAqICAgICAgICAgICAgICB0ZXh0QWxpZ24sXG4gKiAgICAgICAgICAgICAgdGV4dFZlcnRpY2FsQWxpZ25cbiAqICAgICAgICAgIH1dLCBbLi4uXSwgLi4uXVxuICogICAgICB9LCAuLi5dXG4gKiB9XG4gKiBJZiBzdHlsZU5hbWUgaXMgdW5kZWZpbmVkLCBpdCBpcyBwbGFpbiB0ZXh0LlxuICovXG5cblxuZnVuY3Rpb24gcGFyc2VSaWNoVGV4dCh0ZXh0LCBzdHlsZSkge1xuICB2YXIgY29udGVudEJsb2NrID0ge1xuICAgIGxpbmVzOiBbXSxcbiAgICB3aWR0aDogMCxcbiAgICBoZWlnaHQ6IDBcbiAgfTtcbiAgdGV4dCAhPSBudWxsICYmICh0ZXh0ICs9ICcnKTtcblxuICBpZiAoIXRleHQpIHtcbiAgICByZXR1cm4gY29udGVudEJsb2NrO1xuICB9XG5cbiAgdmFyIGxhc3RJbmRleCA9IFNUWUxFX1JFRy5sYXN0SW5kZXggPSAwO1xuICB2YXIgcmVzdWx0O1xuXG4gIHdoaWxlICgocmVzdWx0ID0gU1RZTEVfUkVHLmV4ZWModGV4dCkpICE9IG51bGwpIHtcbiAgICB2YXIgbWF0Y2hlZEluZGV4ID0gcmVzdWx0LmluZGV4O1xuXG4gICAgaWYgKG1hdGNoZWRJbmRleCA+IGxhc3RJbmRleCkge1xuICAgICAgcHVzaFRva2Vucyhjb250ZW50QmxvY2ssIHRleHQuc3Vic3RyaW5nKGxhc3RJbmRleCwgbWF0Y2hlZEluZGV4KSk7XG4gICAgfVxuXG4gICAgcHVzaFRva2Vucyhjb250ZW50QmxvY2ssIHJlc3VsdFsyXSwgcmVzdWx0WzFdKTtcbiAgICBsYXN0SW5kZXggPSBTVFlMRV9SRUcubGFzdEluZGV4O1xuICB9XG5cbiAgaWYgKGxhc3RJbmRleCA8IHRleHQubGVuZ3RoKSB7XG4gICAgcHVzaFRva2Vucyhjb250ZW50QmxvY2ssIHRleHQuc3Vic3RyaW5nKGxhc3RJbmRleCwgdGV4dC5sZW5ndGgpKTtcbiAgfVxuXG4gIHZhciBsaW5lcyA9IGNvbnRlbnRCbG9jay5saW5lcztcbiAgdmFyIGNvbnRlbnRIZWlnaHQgPSAwO1xuICB2YXIgY29udGVudFdpZHRoID0gMDsgLy8gRm9yIGB0ZXh0V2lkdGg6IDEwMCVgXG5cbiAgdmFyIHBlbmRpbmdMaXN0ID0gW107XG4gIHZhciBzdGxQYWRkaW5nID0gc3R5bGUudGV4dFBhZGRpbmc7XG4gIHZhciB0cnVuY2F0ZSA9IHN0eWxlLnRydW5jYXRlO1xuICB2YXIgdHJ1bmNhdGVXaWR0aCA9IHRydW5jYXRlICYmIHRydW5jYXRlLm91dGVyV2lkdGg7XG4gIHZhciB0cnVuY2F0ZUhlaWdodCA9IHRydW5jYXRlICYmIHRydW5jYXRlLm91dGVySGVpZ2h0O1xuXG4gIGlmIChzdGxQYWRkaW5nKSB7XG4gICAgdHJ1bmNhdGVXaWR0aCAhPSBudWxsICYmICh0cnVuY2F0ZVdpZHRoIC09IHN0bFBhZGRpbmdbMV0gKyBzdGxQYWRkaW5nWzNdKTtcbiAgICB0cnVuY2F0ZUhlaWdodCAhPSBudWxsICYmICh0cnVuY2F0ZUhlaWdodCAtPSBzdGxQYWRkaW5nWzBdICsgc3RsUGFkZGluZ1syXSk7XG4gIH0gLy8gQ2FsY3VsYXRlIGxheW91dCBpbmZvIG9mIHRva2Vucy5cblxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGluZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgbGluZSA9IGxpbmVzW2ldO1xuICAgIHZhciBsaW5lSGVpZ2h0ID0gMDtcbiAgICB2YXIgbGluZVdpZHRoID0gMDtcblxuICAgIGZvciAodmFyIGogPSAwOyBqIDwgbGluZS50b2tlbnMubGVuZ3RoOyBqKyspIHtcbiAgICAgIHZhciB0b2tlbiA9IGxpbmUudG9rZW5zW2pdO1xuICAgICAgdmFyIHRva2VuU3R5bGUgPSB0b2tlbi5zdHlsZU5hbWUgJiYgc3R5bGUucmljaFt0b2tlbi5zdHlsZU5hbWVdIHx8IHt9OyAvLyB0ZXh0UGFkZGluZyBzaG91bGQgbm90IGluaGVyaXQgZnJvbSBzdHlsZS5cblxuICAgICAgdmFyIHRleHRQYWRkaW5nID0gdG9rZW4udGV4dFBhZGRpbmcgPSB0b2tlblN0eWxlLnRleHRQYWRkaW5nOyAvLyB0ZXh0Rm9udCBoYXMgYmVlbiBhc2lnbmVkIHRvIGZvbnQgYnkgYG5vcm1hbGl6ZVN0eWxlYC5cblxuICAgICAgdmFyIGZvbnQgPSB0b2tlbi5mb250ID0gdG9rZW5TdHlsZS5mb250IHx8IHN0eWxlLmZvbnQ7IC8vIHRleHRIZWlnaHQgY2FuIGJlIHVzZWQgd2hlbiB0ZXh0VmVydGljYWxBbGlnbiBpcyBzcGVjaWZpZWQgaW4gdG9rZW4uXG5cbiAgICAgIHZhciB0b2tlbkhlaWdodCA9IHRva2VuLnRleHRIZWlnaHQgPSByZXRyaWV2ZTIoIC8vIHRleHRIZWlnaHQgc2hvdWxkIG5vdCBiZSBpbmhlcml0ZWQsIGNvbnNpZGVyIGl0IGNhbiBiZSBzcGVjaWZpZWRcbiAgICAgIC8vIGFzIGJveCBoZWlnaHQgb2YgdGhlIGJsb2NrLlxuICAgICAgdG9rZW5TdHlsZS50ZXh0SGVpZ2h0LCBnZXRMaW5lSGVpZ2h0KGZvbnQpKTtcbiAgICAgIHRleHRQYWRkaW5nICYmICh0b2tlbkhlaWdodCArPSB0ZXh0UGFkZGluZ1swXSArIHRleHRQYWRkaW5nWzJdKTtcbiAgICAgIHRva2VuLmhlaWdodCA9IHRva2VuSGVpZ2h0O1xuICAgICAgdG9rZW4ubGluZUhlaWdodCA9IHJldHJpZXZlMyh0b2tlblN0eWxlLnRleHRMaW5lSGVpZ2h0LCBzdHlsZS50ZXh0TGluZUhlaWdodCwgdG9rZW5IZWlnaHQpO1xuICAgICAgdG9rZW4udGV4dEFsaWduID0gdG9rZW5TdHlsZSAmJiB0b2tlblN0eWxlLnRleHRBbGlnbiB8fCBzdHlsZS50ZXh0QWxpZ247XG4gICAgICB0b2tlbi50ZXh0VmVydGljYWxBbGlnbiA9IHRva2VuU3R5bGUgJiYgdG9rZW5TdHlsZS50ZXh0VmVydGljYWxBbGlnbiB8fCAnbWlkZGxlJztcblxuICAgICAgaWYgKHRydW5jYXRlSGVpZ2h0ICE9IG51bGwgJiYgY29udGVudEhlaWdodCArIHRva2VuLmxpbmVIZWlnaHQgPiB0cnVuY2F0ZUhlaWdodCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGxpbmVzOiBbXSxcbiAgICAgICAgICB3aWR0aDogMCxcbiAgICAgICAgICBoZWlnaHQ6IDBcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgdG9rZW4udGV4dFdpZHRoID0gZ2V0V2lkdGgodG9rZW4udGV4dCwgZm9udCk7XG4gICAgICB2YXIgdG9rZW5XaWR0aCA9IHRva2VuU3R5bGUudGV4dFdpZHRoO1xuICAgICAgdmFyIHRva2VuV2lkdGhOb3RTcGVjaWZpZWQgPSB0b2tlbldpZHRoID09IG51bGwgfHwgdG9rZW5XaWR0aCA9PT0gJ2F1dG8nOyAvLyBQZXJjZW50IHdpZHRoLCBjYW4gYmUgYDEwMCVgLCBjYW4gYmUgdXNlZCBpbiBkcmF3aW5nIHNlcGFyYXRlXG4gICAgICAvLyBsaW5lIHdoZW4gYm94IHdpZHRoIGlzIG5lZWRlZCB0byBiZSBhdXRvLlxuXG4gICAgICBpZiAodHlwZW9mIHRva2VuV2lkdGggPT09ICdzdHJpbmcnICYmIHRva2VuV2lkdGguY2hhckF0KHRva2VuV2lkdGgubGVuZ3RoIC0gMSkgPT09ICclJykge1xuICAgICAgICB0b2tlbi5wZXJjZW50V2lkdGggPSB0b2tlbldpZHRoO1xuICAgICAgICBwZW5kaW5nTGlzdC5wdXNoKHRva2VuKTtcbiAgICAgICAgdG9rZW5XaWR0aCA9IDA7IC8vIERvIG5vdCB0cnVuY2F0ZSBpbiB0aGlzIGNhc2UsIGJlY2F1c2UgdGhlcmUgaXMgbm8gdXNlciBjYXNlXG4gICAgICAgIC8vIGFuZCBpdCBpcyB0b28gY29tcGxpY2F0ZWQuXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAodG9rZW5XaWR0aE5vdFNwZWNpZmllZCkge1xuICAgICAgICAgIHRva2VuV2lkdGggPSB0b2tlbi50ZXh0V2lkdGg7IC8vIEZJWE1FOiBJZiBpbWFnZSBpcyBub3QgbG9hZGVkIGFuZCB0ZXh0V2lkdGggaXMgbm90IHNwZWNpZmllZCwgY2FsbGluZ1xuICAgICAgICAgIC8vIGBnZXRCb3VuZGluZ1JlY3QoKWAgd2lsbCBub3QgZ2V0IGNvcnJlY3QgcmVzdWx0LlxuXG4gICAgICAgICAgdmFyIHRleHRCYWNrZ3JvdW5kQ29sb3IgPSB0b2tlblN0eWxlLnRleHRCYWNrZ3JvdW5kQ29sb3I7XG4gICAgICAgICAgdmFyIGJnSW1nID0gdGV4dEJhY2tncm91bmRDb2xvciAmJiB0ZXh0QmFja2dyb3VuZENvbG9yLmltYWdlOyAvLyBVc2UgY2FzZXM6XG4gICAgICAgICAgLy8gKDEpIElmIGltYWdlIGlzIG5vdCBsb2FkZWQsIGl0IHdpbGwgYmUgbG9hZGVkIGF0IHJlbmRlciBwaGFzZSBhbmQgY2FsbFxuICAgICAgICAgIC8vIGBkaXJ0eSgpYCBhbmQgYHRleHRCYWNrZ3JvdW5kQ29sb3IuaW1hZ2VgIHdpbGwgYmUgcmVwbGFjZWQgd2l0aCB0aGUgbG9hZGVkXG4gICAgICAgICAgLy8gaW1hZ2UsIGFuZCB0aGVuIHRoZSByaWdodCBzaXplIHdpbGwgYmUgY2FsY3VsYXRlZCBoZXJlIGF0IHRoZSBuZXh0IHRpY2suXG4gICAgICAgICAgLy8gU2VlIGBncmFwaGljL2hlbHBlci90ZXh0LmpzYC5cbiAgICAgICAgICAvLyAoMikgSWYgaW1hZ2UgbG9hZGVkLCBhbmQgYHRleHRCYWNrZ3JvdW5kQ29sb3IuaW1hZ2VgIGlzIGltYWdlIHNyYyBzdHJpbmcsXG4gICAgICAgICAgLy8gdXNlIGBpbWFnZUhlbHBlci5maW5kRXhpc3RJbWFnZWAgdG8gZmluZCBjYWNoZWQgaW1hZ2UuXG4gICAgICAgICAgLy8gYGltYWdlSGVscGVyLmZpbmRFeGlzdEltYWdlYCB3aWxsIGFsd2F5cyBiZSBjYWxsZWQgaGVyZSBiZWZvcmVcbiAgICAgICAgICAvLyBgaW1hZ2VIZWxwZXIuY3JlYXRlT3JVcGRhdGVJbWFnZWAgaW4gYGdyYXBoaWMvaGVscGVyL3RleHQuanMjcmVuZGVyUmljaFRleHRgXG4gICAgICAgICAgLy8gd2hpY2ggZW5zdXJlcyB0aGF0IGltYWdlIHdpbGwgbm90IGJlIHJlbmRlcmVkIGJlZm9yZSBjb3JyZWN0IHNpemUgY2FsY3VhbHRlZC5cblxuICAgICAgICAgIGlmIChiZ0ltZykge1xuICAgICAgICAgICAgYmdJbWcgPSBpbWFnZUhlbHBlci5maW5kRXhpc3RJbWFnZShiZ0ltZyk7XG5cbiAgICAgICAgICAgIGlmIChpbWFnZUhlbHBlci5pc0ltYWdlUmVhZHkoYmdJbWcpKSB7XG4gICAgICAgICAgICAgIHRva2VuV2lkdGggPSBNYXRoLm1heCh0b2tlbldpZHRoLCBiZ0ltZy53aWR0aCAqIHRva2VuSGVpZ2h0IC8gYmdJbWcuaGVpZ2h0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgcGFkZGluZ1cgPSB0ZXh0UGFkZGluZyA/IHRleHRQYWRkaW5nWzFdICsgdGV4dFBhZGRpbmdbM10gOiAwO1xuICAgICAgICB0b2tlbldpZHRoICs9IHBhZGRpbmdXO1xuICAgICAgICB2YXIgcmVtaWFuVHJ1bmNXaWR0aCA9IHRydW5jYXRlV2lkdGggIT0gbnVsbCA/IHRydW5jYXRlV2lkdGggLSBsaW5lV2lkdGggOiBudWxsO1xuXG4gICAgICAgIGlmIChyZW1pYW5UcnVuY1dpZHRoICE9IG51bGwgJiYgcmVtaWFuVHJ1bmNXaWR0aCA8IHRva2VuV2lkdGgpIHtcbiAgICAgICAgICBpZiAoIXRva2VuV2lkdGhOb3RTcGVjaWZpZWQgfHwgcmVtaWFuVHJ1bmNXaWR0aCA8IHBhZGRpbmdXKSB7XG4gICAgICAgICAgICB0b2tlbi50ZXh0ID0gJyc7XG4gICAgICAgICAgICB0b2tlbi50ZXh0V2lkdGggPSB0b2tlbldpZHRoID0gMDtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdG9rZW4udGV4dCA9IHRydW5jYXRlVGV4dCh0b2tlbi50ZXh0LCByZW1pYW5UcnVuY1dpZHRoIC0gcGFkZGluZ1csIGZvbnQsIHRydW5jYXRlLmVsbGlwc2lzLCB7XG4gICAgICAgICAgICAgIG1pbkNoYXI6IHRydW5jYXRlLm1pbkNoYXJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdG9rZW4udGV4dFdpZHRoID0gZ2V0V2lkdGgodG9rZW4udGV4dCwgZm9udCk7XG4gICAgICAgICAgICB0b2tlbldpZHRoID0gdG9rZW4udGV4dFdpZHRoICsgcGFkZGluZ1c7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGxpbmVXaWR0aCArPSB0b2tlbi53aWR0aCA9IHRva2VuV2lkdGg7XG4gICAgICB0b2tlblN0eWxlICYmIChsaW5lSGVpZ2h0ID0gTWF0aC5tYXgobGluZUhlaWdodCwgdG9rZW4ubGluZUhlaWdodCkpO1xuICAgIH1cblxuICAgIGxpbmUud2lkdGggPSBsaW5lV2lkdGg7XG4gICAgbGluZS5saW5lSGVpZ2h0ID0gbGluZUhlaWdodDtcbiAgICBjb250ZW50SGVpZ2h0ICs9IGxpbmVIZWlnaHQ7XG4gICAgY29udGVudFdpZHRoID0gTWF0aC5tYXgoY29udGVudFdpZHRoLCBsaW5lV2lkdGgpO1xuICB9XG5cbiAgY29udGVudEJsb2NrLm91dGVyV2lkdGggPSBjb250ZW50QmxvY2sud2lkdGggPSByZXRyaWV2ZTIoc3R5bGUudGV4dFdpZHRoLCBjb250ZW50V2lkdGgpO1xuICBjb250ZW50QmxvY2sub3V0ZXJIZWlnaHQgPSBjb250ZW50QmxvY2suaGVpZ2h0ID0gcmV0cmlldmUyKHN0eWxlLnRleHRIZWlnaHQsIGNvbnRlbnRIZWlnaHQpO1xuXG4gIGlmIChzdGxQYWRkaW5nKSB7XG4gICAgY29udGVudEJsb2NrLm91dGVyV2lkdGggKz0gc3RsUGFkZGluZ1sxXSArIHN0bFBhZGRpbmdbM107XG4gICAgY29udGVudEJsb2NrLm91dGVySGVpZ2h0ICs9IHN0bFBhZGRpbmdbMF0gKyBzdGxQYWRkaW5nWzJdO1xuICB9XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwZW5kaW5nTGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZhciB0b2tlbiA9IHBlbmRpbmdMaXN0W2ldO1xuICAgIHZhciBwZXJjZW50V2lkdGggPSB0b2tlbi5wZXJjZW50V2lkdGg7IC8vIFNob3VsZCBub3QgYmFzZSBvbiBvdXRlcldpZHRoLCBiZWNhdXNlIHRva2VuIGNhbiBub3QgYmUgcGxhY2VkIG91dCBvZiBwYWRkaW5nLlxuXG4gICAgdG9rZW4ud2lkdGggPSBwYXJzZUludChwZXJjZW50V2lkdGgsIDEwKSAvIDEwMCAqIGNvbnRlbnRXaWR0aDtcbiAgfVxuXG4gIHJldHVybiBjb250ZW50QmxvY2s7XG59XG5cbmZ1bmN0aW9uIHB1c2hUb2tlbnMoYmxvY2ssIHN0ciwgc3R5bGVOYW1lKSB7XG4gIHZhciBpc0VtcHR5U3RyID0gc3RyID09PSAnJztcbiAgdmFyIHN0cnMgPSBzdHIuc3BsaXQoJ1xcbicpO1xuICB2YXIgbGluZXMgPSBibG9jay5saW5lcztcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0cnMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgdGV4dCA9IHN0cnNbaV07XG4gICAgdmFyIHRva2VuID0ge1xuICAgICAgc3R5bGVOYW1lOiBzdHlsZU5hbWUsXG4gICAgICB0ZXh0OiB0ZXh0LFxuICAgICAgaXNMaW5lSG9sZGVyOiAhdGV4dCAmJiAhaXNFbXB0eVN0clxuICAgIH07IC8vIFRoZSBmaXJzdCB0b2tlbiBzaG91bGQgYmUgYXBwZW5kZWQgdG8gdGhlIGxhc3QgbGluZS5cblxuICAgIGlmICghaSkge1xuICAgICAgdmFyIHRva2VucyA9IChsaW5lc1tsaW5lcy5sZW5ndGggLSAxXSB8fCAobGluZXNbMF0gPSB7XG4gICAgICAgIHRva2VuczogW11cbiAgICAgIH0pKS50b2tlbnM7IC8vIENvbnNpZGVyIGNhc2VzOlxuICAgICAgLy8gKDEpICcnLnNwbGl0KCdcXG4nKSA9PiBbJycsICdcXG4nLCAnJ10sIHRoZSAnJyBhdCB0aGUgZmlyc3QgaXRlbVxuICAgICAgLy8gKHdoaWNoIGlzIGEgcGxhY2Vob2xkZXIpIHNob3VsZCBiZSByZXBsYWNlZCBieSBuZXcgdG9rZW4uXG4gICAgICAvLyAoMikgQSBpbWFnZSBiYWNrYWdlLCB3aGVyZSB0b2tlbiBsaWtlcyB7YXx9LlxuICAgICAgLy8gKDMpIEEgcmVkdW5kYW50ICcnIHdpbGwgYWZmZWN0IHRleHRBbGlnbiBpbiBsaW5lLlxuICAgICAgLy8gKDQpIHRva2VucyB3aXRoIHRoZSBzYW1lIHRwbE5hbWUgc2hvdWxkIG5vdCBiZSBtZXJnZWQsIGJlY2F1c2VcbiAgICAgIC8vIHRoZXkgc2hvdWxkIGJlIGRpc3BsYXllZCBpbiBkaWZmZXJlbnQgYm94ICh3aXRoIGJvcmRlciBhbmQgcGFkZGluZykuXG5cbiAgICAgIHZhciB0b2tlbnNMZW4gPSB0b2tlbnMubGVuZ3RoO1xuICAgICAgdG9rZW5zTGVuID09PSAxICYmIHRva2Vuc1swXS5pc0xpbmVIb2xkZXIgPyB0b2tlbnNbMF0gPSB0b2tlbiA6IC8vIENvbnNpZGVyIHRleHQgaXMgJycsIG9ubHkgaW5zZXJ0IHdoZW4gaXQgaXMgdGhlIFwibGluZUhvbGRlclwiIG9yXG4gICAgICAvLyBcImVtcHR5U3RyXCIuIE90aGVyd2lzZSBhIHJlZHVuZGFudCAnJyB3aWxsIGFmZmVjdCB0ZXh0QWxpZ24gaW4gbGluZS5cbiAgICAgICh0ZXh0IHx8ICF0b2tlbnNMZW4gfHwgaXNFbXB0eVN0cikgJiYgdG9rZW5zLnB1c2godG9rZW4pO1xuICAgIH0gLy8gT3RoZXIgdG9rZW5zIGFsd2F5cyBzdGFydCBhIG5ldyBsaW5lLlxuICAgIGVsc2Uge1xuICAgICAgICAvLyBJZiB0aGVyZSBpcyAnJywgaW5zZXJ0IGl0IGFzIGEgcGxhY2Vob2xkZXIuXG4gICAgICAgIGxpbmVzLnB1c2goe1xuICAgICAgICAgIHRva2VuczogW3Rva2VuXVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBtYWtlRm9udChzdHlsZSkge1xuICAvLyBGSVhNRSBpbiBub2RlLWNhbnZhcyBmb250V2VpZ2h0IGlzIGJlZm9yZSBmb250U3R5bGVcbiAgLy8gVXNlIGBmb250U2l6ZWAgYGZvbnRGYW1pbHlgIHRvIGNoZWNrIHdoZXRoZXIgZm9udCBwcm9wZXJ0aWVzIGFyZSBkZWZpbmVkLlxuICB2YXIgZm9udCA9IChzdHlsZS5mb250U2l6ZSB8fCBzdHlsZS5mb250RmFtaWx5KSAmJiBbc3R5bGUuZm9udFN0eWxlLCBzdHlsZS5mb250V2VpZ2h0LCAoc3R5bGUuZm9udFNpemUgfHwgMTIpICsgJ3B4JywgLy8gSWYgZm9udCBwcm9wZXJ0aWVzIGFyZSBkZWZpbmVkLCBgZm9udEZhbWlseWAgc2hvdWxkIG5vdCBiZSBpZ25vcmVkLlxuICBzdHlsZS5mb250RmFtaWx5IHx8ICdzYW5zLXNlcmlmJ10uam9pbignICcpO1xuICByZXR1cm4gZm9udCAmJiB0cmltKGZvbnQpIHx8IHN0eWxlLnRleHRGb250IHx8IHN0eWxlLmZvbnQ7XG59XG5cbmV4cG9ydHMuREVGQVVMVF9GT05UID0gREVGQVVMVF9GT05UO1xuZXhwb3J0cy4kb3ZlcnJpZGUgPSAkb3ZlcnJpZGU7XG5leHBvcnRzLmdldFdpZHRoID0gZ2V0V2lkdGg7XG5leHBvcnRzLmdldEJvdW5kaW5nUmVjdCA9IGdldEJvdW5kaW5nUmVjdDtcbmV4cG9ydHMuYWRqdXN0VGV4dFggPSBhZGp1c3RUZXh0WDtcbmV4cG9ydHMuYWRqdXN0VGV4dFkgPSBhZGp1c3RUZXh0WTtcbmV4cG9ydHMuYWRqdXN0VGV4dFBvc2l0aW9uT25SZWN0ID0gYWRqdXN0VGV4dFBvc2l0aW9uT25SZWN0O1xuZXhwb3J0cy50cnVuY2F0ZVRleHQgPSB0cnVuY2F0ZVRleHQ7XG5leHBvcnRzLmdldExpbmVIZWlnaHQgPSBnZXRMaW5lSGVpZ2h0O1xuZXhwb3J0cy5tZWFzdXJlVGV4dCA9IG1lYXN1cmVUZXh0O1xuZXhwb3J0cy5wYXJzZVBsYWluVGV4dCA9IHBhcnNlUGxhaW5UZXh0O1xuZXhwb3J0cy5wYXJzZVJpY2hUZXh0ID0gcGFyc2VSaWNoVGV4dDtcbmV4cG9ydHMubWFrZUZvbnQgPSBtYWtlRm9udDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsRkE7QUFDQTtBQW9GQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/text.js
