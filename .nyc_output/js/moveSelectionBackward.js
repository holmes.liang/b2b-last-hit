/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule moveSelectionBackward
 * @format
 * 
 */

/**
 * Given a collapsed selection, move the focus `maxDistance` backward within
 * the selected block. If the selection will go beyond the start of the block,
 * move focus to the end of the previous block, but no further.
 *
 * This function is not Unicode-aware, so surrogate pairs will be treated
 * as having length 2.
 */

function moveSelectionBackward(editorState, maxDistance) {
  var selection = editorState.getSelection();
  var content = editorState.getCurrentContent();
  var key = selection.getStartKey();
  var offset = selection.getStartOffset();
  var focusKey = key;
  var focusOffset = 0;

  if (maxDistance > offset) {
    var keyBefore = content.getKeyBefore(key);

    if (keyBefore == null) {
      focusKey = key;
    } else {
      focusKey = keyBefore;
      var blockBefore = content.getBlockForKey(keyBefore);
      focusOffset = blockBefore.getText().length;
    }
  } else {
    focusOffset = offset - maxDistance;
  }

  return selection.merge({
    focusKey: focusKey,
    focusOffset: focusOffset,
    isBackward: true
  });
}

module.exports = moveSelectionBackward;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vdmVTZWxlY3Rpb25CYWNrd2FyZC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9tb3ZlU2VsZWN0aW9uQmFja3dhcmQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBtb3ZlU2VsZWN0aW9uQmFja3dhcmRcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBHaXZlbiBhIGNvbGxhcHNlZCBzZWxlY3Rpb24sIG1vdmUgdGhlIGZvY3VzIGBtYXhEaXN0YW5jZWAgYmFja3dhcmQgd2l0aGluXG4gKiB0aGUgc2VsZWN0ZWQgYmxvY2suIElmIHRoZSBzZWxlY3Rpb24gd2lsbCBnbyBiZXlvbmQgdGhlIHN0YXJ0IG9mIHRoZSBibG9jayxcbiAqIG1vdmUgZm9jdXMgdG8gdGhlIGVuZCBvZiB0aGUgcHJldmlvdXMgYmxvY2ssIGJ1dCBubyBmdXJ0aGVyLlxuICpcbiAqIFRoaXMgZnVuY3Rpb24gaXMgbm90IFVuaWNvZGUtYXdhcmUsIHNvIHN1cnJvZ2F0ZSBwYWlycyB3aWxsIGJlIHRyZWF0ZWRcbiAqIGFzIGhhdmluZyBsZW5ndGggMi5cbiAqL1xuZnVuY3Rpb24gbW92ZVNlbGVjdGlvbkJhY2t3YXJkKGVkaXRvclN0YXRlLCBtYXhEaXN0YW5jZSkge1xuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgdmFyIGtleSA9IHNlbGVjdGlvbi5nZXRTdGFydEtleSgpO1xuICB2YXIgb2Zmc2V0ID0gc2VsZWN0aW9uLmdldFN0YXJ0T2Zmc2V0KCk7XG5cbiAgdmFyIGZvY3VzS2V5ID0ga2V5O1xuICB2YXIgZm9jdXNPZmZzZXQgPSAwO1xuXG4gIGlmIChtYXhEaXN0YW5jZSA+IG9mZnNldCkge1xuICAgIHZhciBrZXlCZWZvcmUgPSBjb250ZW50LmdldEtleUJlZm9yZShrZXkpO1xuICAgIGlmIChrZXlCZWZvcmUgPT0gbnVsbCkge1xuICAgICAgZm9jdXNLZXkgPSBrZXk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvY3VzS2V5ID0ga2V5QmVmb3JlO1xuICAgICAgdmFyIGJsb2NrQmVmb3JlID0gY29udGVudC5nZXRCbG9ja0ZvcktleShrZXlCZWZvcmUpO1xuICAgICAgZm9jdXNPZmZzZXQgPSBibG9ja0JlZm9yZS5nZXRUZXh0KCkubGVuZ3RoO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBmb2N1c09mZnNldCA9IG9mZnNldCAtIG1heERpc3RhbmNlO1xuICB9XG5cbiAgcmV0dXJuIHNlbGVjdGlvbi5tZXJnZSh7XG4gICAgZm9jdXNLZXk6IGZvY3VzS2V5LFxuICAgIGZvY3VzT2Zmc2V0OiBmb2N1c09mZnNldCxcbiAgICBpc0JhY2t3YXJkOiB0cnVlXG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IG1vdmVTZWxlY3Rpb25CYWNrd2FyZDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUVBOzs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/moveSelectionBackward.js
