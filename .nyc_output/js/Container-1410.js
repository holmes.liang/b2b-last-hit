

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _deepMerge = __webpack_require__(/*! ../utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

function Container(_ref, _ref2) {
  var theme = _ref2.theme;

  var props = _objectWithoutProperties(_ref, []);

  var classes = _noImportant.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, theme));

  return _react2.default.createElement('div', _extends({
    id: 'lightboxBackdrop',
    className: (0, _noImportant.css)(classes.container)
  }, props));
}

Container.contextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  container: {
    alignItems: 'center',
    backgroundColor: _theme2.default.container.background,
    boxSizing: 'border-box',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    left: 0,
    paddingBottom: _theme2.default.container.gutter.vertical,
    paddingLeft: _theme2.default.container.gutter.horizontal,
    paddingRight: _theme2.default.container.gutter.horizontal,
    paddingTop: _theme2.default.container.gutter.vertical,
    position: 'fixed',
    top: 0,
    width: '100%',
    zIndex: _theme2.default.container.zIndex
  }
};
exports.default = Container;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0NvbnRhaW5lci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvY29tcG9uZW50cy9Db250YWluZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX25vSW1wb3J0YW50ID0gcmVxdWlyZSgnYXBocm9kaXRlL25vLWltcG9ydGFudCcpO1xuXG52YXIgX3RoZW1lID0gcmVxdWlyZSgnLi4vdGhlbWUnKTtcblxudmFyIF90aGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aGVtZSk7XG5cbnZhciBfZGVlcE1lcmdlID0gcmVxdWlyZSgnLi4vdXRpbHMvZGVlcE1lcmdlJyk7XG5cbnZhciBfZGVlcE1lcmdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZXBNZXJnZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gQ29udGFpbmVyKF9yZWYsIF9yZWYyKSB7XG5cdHZhciB0aGVtZSA9IF9yZWYyLnRoZW1lO1xuXG5cdHZhciBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcmVmLCBbXSk7XG5cblx0dmFyIGNsYXNzZXMgPSBfbm9JbXBvcnRhbnQuU3R5bGVTaGVldC5jcmVhdGUoKDAsIF9kZWVwTWVyZ2UyLmRlZmF1bHQpKGRlZmF1bHRTdHlsZXMsIHRoZW1lKSk7XG5cblx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdkaXYnLCBfZXh0ZW5kcyh7IGlkOiAnbGlnaHRib3hCYWNrZHJvcCcsXG5cdFx0Y2xhc3NOYW1lOiAoMCwgX25vSW1wb3J0YW50LmNzcykoY2xhc3Nlcy5jb250YWluZXIpXG5cdH0sIHByb3BzKSk7XG59XG5cbkNvbnRhaW5lci5jb250ZXh0VHlwZXMgPSB7XG5cdHRoZW1lOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdC5pc1JlcXVpcmVkXG59O1xuXG52YXIgZGVmYXVsdFN0eWxlcyA9IHtcblx0Y29udGFpbmVyOiB7XG5cdFx0YWxpZ25JdGVtczogJ2NlbnRlcicsXG5cdFx0YmFja2dyb3VuZENvbG9yOiBfdGhlbWUyLmRlZmF1bHQuY29udGFpbmVyLmJhY2tncm91bmQsXG5cdFx0Ym94U2l6aW5nOiAnYm9yZGVyLWJveCcsXG5cdFx0ZGlzcGxheTogJ2ZsZXgnLFxuXHRcdGhlaWdodDogJzEwMCUnLFxuXHRcdGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcblx0XHRsZWZ0OiAwLFxuXHRcdHBhZGRpbmdCb3R0b206IF90aGVtZTIuZGVmYXVsdC5jb250YWluZXIuZ3V0dGVyLnZlcnRpY2FsLFxuXHRcdHBhZGRpbmdMZWZ0OiBfdGhlbWUyLmRlZmF1bHQuY29udGFpbmVyLmd1dHRlci5ob3Jpem9udGFsLFxuXHRcdHBhZGRpbmdSaWdodDogX3RoZW1lMi5kZWZhdWx0LmNvbnRhaW5lci5ndXR0ZXIuaG9yaXpvbnRhbCxcblx0XHRwYWRkaW5nVG9wOiBfdGhlbWUyLmRlZmF1bHQuY29udGFpbmVyLmd1dHRlci52ZXJ0aWNhbCxcblx0XHRwb3NpdGlvbjogJ2ZpeGVkJyxcblx0XHR0b3A6IDAsXG5cdFx0d2lkdGg6ICcxMDAlJyxcblx0XHR6SW5kZXg6IF90aGVtZTIuZGVmYXVsdC5jb250YWluZXIuekluZGV4XG5cdH1cbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvbnRhaW5lcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFmQTtBQURBO0FBb0JBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Container.js
