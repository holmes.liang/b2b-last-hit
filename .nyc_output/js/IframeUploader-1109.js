__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _uid__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./uid */ "./node_modules/rc-upload/es/uid.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_11__);






/* eslint react/sort-comp:0 */







var IFRAME_STYLE = {
  position: 'absolute',
  top: 0,
  opacity: 0,
  filter: 'alpha(opacity=0)',
  left: 0,
  zIndex: 9999
}; // diferent from AjaxUpload, can only upload on at one time, serial seriously

var IframeUploader = function (_Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(IframeUploader, _Component);

  function IframeUploader() {
    var _ref;

    var _temp, _this, _ret;

    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, IframeUploader);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, (_ref = IframeUploader.__proto__ || Object.getPrototypeOf(IframeUploader)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      uploading: false
    }, _this.file = {}, _this.onLoad = function () {
      if (!_this.state.uploading) {
        return;
      }

      var _this2 = _this,
          props = _this2.props,
          file = _this2.file;
      var response = void 0;

      try {
        var doc = _this.getIframeDocument();

        var script = doc.getElementsByTagName('script')[0];

        if (script && script.parentNode === doc.body) {
          doc.body.removeChild(script);
        }

        response = doc.body.innerHTML;
        props.onSuccess(response, file);
      } catch (err) {
        warning__WEBPACK_IMPORTED_MODULE_11___default()(false, 'cross domain error for Upload. Maybe server should return document.domain script. see Note from https://github.com/react-component/upload');
        response = 'cross-domain';
        props.onError(err, null, file);
      }

      _this.endUpload();
    }, _this.onChange = function () {
      var target = _this.getFormInputNode(); // ie8/9 don't support FileList Object
      // http://stackoverflow.com/questions/12830058/ie8-input-type-file-get-files


      var file = _this.file = {
        uid: Object(_uid__WEBPACK_IMPORTED_MODULE_10__["default"])(),
        name: target.value && target.value.substring(target.value.lastIndexOf('\\') + 1, target.value.length)
      };

      _this.startUpload();

      var _this3 = _this,
          props = _this3.props;

      if (!props.beforeUpload) {
        return _this.post(file);
      }

      var before = props.beforeUpload(file);

      if (before && before.then) {
        before.then(function () {
          _this.post(file);
        }, function () {
          _this.endUpload();
        });
      } else if (before !== false) {
        _this.post(file);
      } else {
        _this.endUpload();
      }
    }, _this.saveIframe = function (node) {
      _this.iframe = node;
    }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(_this, _ret);
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(IframeUploader, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.updateIframeWH();
      this.initIframe();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.updateIframeWH();
    }
  }, {
    key: 'getIframeNode',
    value: function getIframeNode() {
      return this.iframe;
    }
  }, {
    key: 'getIframeDocument',
    value: function getIframeDocument() {
      return this.getIframeNode().contentDocument;
    }
  }, {
    key: 'getFormNode',
    value: function getFormNode() {
      return this.getIframeDocument().getElementById('form');
    }
  }, {
    key: 'getFormInputNode',
    value: function getFormInputNode() {
      return this.getIframeDocument().getElementById('input');
    }
  }, {
    key: 'getFormDataNode',
    value: function getFormDataNode() {
      return this.getIframeDocument().getElementById('data');
    }
  }, {
    key: 'getFileForMultiple',
    value: function getFileForMultiple(file) {
      return this.props.multiple ? [file] : file;
    }
  }, {
    key: 'getIframeHTML',
    value: function getIframeHTML(domain) {
      var domainScript = '';
      var domainInput = '';

      if (domain) {
        var script = 'script';
        domainScript = '<' + script + '>document.domain="' + domain + '";</' + script + '>';
        domainInput = '<input name="_documentDomain" value="' + domain + '" />';
      }

      return '\n    <!DOCTYPE html>\n    <html>\n    <head>\n    <meta http-equiv="X-UA-Compatible" content="IE=edge" />\n    <style>\n    body,html {padding:0;margin:0;border:0;overflow:hidden;}\n    </style>\n    ' + domainScript + '\n    </head>\n    <body>\n    <form method="post"\n    encType="multipart/form-data"\n    action="" id="form"\n    style="display:block;height:9999px;position:relative;overflow:hidden;">\n    <input id="input" type="file"\n     name="' + this.props.name + '"\n     style="position:absolute;top:0;right:0;height:9999px;font-size:9999px;cursor:pointer;"/>\n    ' + domainInput + '\n    <span id="data"></span>\n    </form>\n    </body>\n    </html>\n    ';
    }
  }, {
    key: 'initIframeSrc',
    value: function initIframeSrc() {
      if (this.domain) {
        this.getIframeNode().src = 'javascript:void((function(){\n        var d = document;\n        d.open();\n        d.domain=\'' + this.domain + '\';\n        d.write(\'\');\n        d.close();\n      })())';
      }
    }
  }, {
    key: 'initIframe',
    value: function initIframe() {
      var iframeNode = this.getIframeNode();
      var win = iframeNode.contentWindow;
      var doc = void 0;
      this.domain = this.domain || '';
      this.initIframeSrc();

      try {
        doc = win.document;
      } catch (e) {
        this.domain = document.domain;
        this.initIframeSrc();
        win = iframeNode.contentWindow;
        doc = win.document;
      }

      doc.open('text/html', 'replace');
      doc.write(this.getIframeHTML(this.domain));
      doc.close();
      this.getFormInputNode().onchange = this.onChange;
    }
  }, {
    key: 'endUpload',
    value: function endUpload() {
      if (this.state.uploading) {
        this.file = {}; // hack avoid batch

        this.state.uploading = false;
        this.setState({
          uploading: false
        });
        this.initIframe();
      }
    }
  }, {
    key: 'startUpload',
    value: function startUpload() {
      if (!this.state.uploading) {
        this.state.uploading = true;
        this.setState({
          uploading: true
        });
      }
    }
  }, {
    key: 'updateIframeWH',
    value: function updateIframeWH() {
      var rootNode = react_dom__WEBPACK_IMPORTED_MODULE_8___default.a.findDOMNode(this);
      var iframeNode = this.getIframeNode();
      iframeNode.style.height = rootNode.offsetHeight + 'px';
      iframeNode.style.width = rootNode.offsetWidth + 'px';
    }
  }, {
    key: 'abort',
    value: function abort(file) {
      if (file) {
        var uid = file;

        if (file && file.uid) {
          uid = file.uid;
        }

        if (uid === this.file.uid) {
          this.endUpload();
        }
      } else {
        this.endUpload();
      }
    }
  }, {
    key: 'post',
    value: function post(file) {
      var _this4 = this;

      var formNode = this.getFormNode();
      var dataSpan = this.getFormDataNode();
      var data = this.props.data;
      var onStart = this.props.onStart;

      if (typeof data === 'function') {
        data = data(file);
      }

      var inputs = document.createDocumentFragment();

      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          var input = document.createElement('input');
          input.setAttribute('name', key);
          input.value = data[key];
          inputs.appendChild(input);
        }
      }

      dataSpan.appendChild(inputs);
      new Promise(function (resolve) {
        var action = _this4.props.action;

        if (typeof action === 'function') {
          return resolve(action(file));
        }

        resolve(action);
      }).then(function (action) {
        formNode.setAttribute('action', action);
        formNode.submit();
        dataSpan.innerHTML = '';
        onStart(file);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          Tag = _props.component,
          disabled = _props.disabled,
          className = _props.className,
          prefixCls = _props.prefixCls,
          children = _props.children,
          style = _props.style;

      var iframeStyle = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, IFRAME_STYLE, {
        display: this.state.uploading || disabled ? 'none' : ''
      });

      var cls = classnames__WEBPACK_IMPORTED_MODULE_9___default()((_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls, true), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-disabled', disabled), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, className, className), _classNames));
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(Tag, {
        className: cls,
        style: babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
          position: 'relative',
          zIndex: 0
        }, style)
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement('iframe', {
        ref: this.saveIframe,
        onLoad: this.onLoad,
        style: iframeStyle
      }), children);
    }
  }]);

  return IframeUploader;
}(react__WEBPACK_IMPORTED_MODULE_6__["Component"]);

IframeUploader.propTypes = {
  component: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
  style: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
  accept: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
  onStart: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  multiple: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  children: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.any,
  data: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object, prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func]),
  action: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func]),
  name: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (IframeUploader);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXBsb2FkL2VzL0lmcmFtZVVwbG9hZGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdXBsb2FkL2VzL0lmcmFtZVVwbG9hZGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5JztcbmltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuLyogZXNsaW50IHJlYWN0L3NvcnQtY29tcDowICovXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgZ2V0VWlkIGZyb20gJy4vdWlkJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJ3dhcm5pbmcnO1xuXG52YXIgSUZSQU1FX1NUWUxFID0ge1xuICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgdG9wOiAwLFxuICBvcGFjaXR5OiAwLFxuICBmaWx0ZXI6ICdhbHBoYShvcGFjaXR5PTApJyxcbiAgbGVmdDogMCxcbiAgekluZGV4OiA5OTk5XG59O1xuXG4vLyBkaWZlcmVudCBmcm9tIEFqYXhVcGxvYWQsIGNhbiBvbmx5IHVwbG9hZCBvbiBhdCBvbmUgdGltZSwgc2VyaWFsIHNlcmlvdXNseVxuXG52YXIgSWZyYW1lVXBsb2FkZXIgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoSWZyYW1lVXBsb2FkZXIsIF9Db21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIElmcmFtZVVwbG9hZGVyKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBJZnJhbWVVcGxvYWRlcik7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChfcmVmID0gSWZyYW1lVXBsb2FkZXIuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihJZnJhbWVVcGxvYWRlcikpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0geyB1cGxvYWRpbmc6IGZhbHNlIH0sIF90aGlzLmZpbGUgPSB7fSwgX3RoaXMub25Mb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKCFfdGhpcy5zdGF0ZS51cGxvYWRpbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdmFyIF90aGlzMiA9IF90aGlzLFxuICAgICAgICAgIHByb3BzID0gX3RoaXMyLnByb3BzLFxuICAgICAgICAgIGZpbGUgPSBfdGhpczIuZmlsZTtcblxuICAgICAgdmFyIHJlc3BvbnNlID0gdm9pZCAwO1xuICAgICAgdHJ5IHtcbiAgICAgICAgdmFyIGRvYyA9IF90aGlzLmdldElmcmFtZURvY3VtZW50KCk7XG4gICAgICAgIHZhciBzY3JpcHQgPSBkb2MuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpWzBdO1xuICAgICAgICBpZiAoc2NyaXB0ICYmIHNjcmlwdC5wYXJlbnROb2RlID09PSBkb2MuYm9keSkge1xuICAgICAgICAgIGRvYy5ib2R5LnJlbW92ZUNoaWxkKHNjcmlwdCk7XG4gICAgICAgIH1cbiAgICAgICAgcmVzcG9uc2UgPSBkb2MuYm9keS5pbm5lckhUTUw7XG4gICAgICAgIHByb3BzLm9uU3VjY2VzcyhyZXNwb25zZSwgZmlsZSk7XG4gICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgd2FybmluZyhmYWxzZSwgJ2Nyb3NzIGRvbWFpbiBlcnJvciBmb3IgVXBsb2FkLiBNYXliZSBzZXJ2ZXIgc2hvdWxkIHJldHVybiBkb2N1bWVudC5kb21haW4gc2NyaXB0LiBzZWUgTm90ZSBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvdXBsb2FkJyk7XG4gICAgICAgIHJlc3BvbnNlID0gJ2Nyb3NzLWRvbWFpbic7XG4gICAgICAgIHByb3BzLm9uRXJyb3IoZXJyLCBudWxsLCBmaWxlKTtcbiAgICAgIH1cbiAgICAgIF90aGlzLmVuZFVwbG9hZCgpO1xuICAgIH0sIF90aGlzLm9uQ2hhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRhcmdldCA9IF90aGlzLmdldEZvcm1JbnB1dE5vZGUoKTtcbiAgICAgIC8vIGllOC85IGRvbid0IHN1cHBvcnQgRmlsZUxpc3QgT2JqZWN0XG4gICAgICAvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzEyODMwMDU4L2llOC1pbnB1dC10eXBlLWZpbGUtZ2V0LWZpbGVzXG4gICAgICB2YXIgZmlsZSA9IF90aGlzLmZpbGUgPSB7XG4gICAgICAgIHVpZDogZ2V0VWlkKCksXG4gICAgICAgIG5hbWU6IHRhcmdldC52YWx1ZSAmJiB0YXJnZXQudmFsdWUuc3Vic3RyaW5nKHRhcmdldC52YWx1ZS5sYXN0SW5kZXhPZignXFxcXCcpICsgMSwgdGFyZ2V0LnZhbHVlLmxlbmd0aClcbiAgICAgIH07XG4gICAgICBfdGhpcy5zdGFydFVwbG9hZCgpO1xuICAgICAgdmFyIF90aGlzMyA9IF90aGlzLFxuICAgICAgICAgIHByb3BzID0gX3RoaXMzLnByb3BzO1xuXG4gICAgICBpZiAoIXByb3BzLmJlZm9yZVVwbG9hZCkge1xuICAgICAgICByZXR1cm4gX3RoaXMucG9zdChmaWxlKTtcbiAgICAgIH1cbiAgICAgIHZhciBiZWZvcmUgPSBwcm9wcy5iZWZvcmVVcGxvYWQoZmlsZSk7XG4gICAgICBpZiAoYmVmb3JlICYmIGJlZm9yZS50aGVuKSB7XG4gICAgICAgIGJlZm9yZS50aGVuKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBfdGhpcy5wb3N0KGZpbGUpO1xuICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgX3RoaXMuZW5kVXBsb2FkKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIGlmIChiZWZvcmUgIT09IGZhbHNlKSB7XG4gICAgICAgIF90aGlzLnBvc3QoZmlsZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBfdGhpcy5lbmRVcGxvYWQoKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5zYXZlSWZyYW1lID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIF90aGlzLmlmcmFtZSA9IG5vZGU7XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoSWZyYW1lVXBsb2FkZXIsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHRoaXMudXBkYXRlSWZyYW1lV0goKTtcbiAgICAgIHRoaXMuaW5pdElmcmFtZSgpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIHRoaXMudXBkYXRlSWZyYW1lV0goKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRJZnJhbWVOb2RlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0SWZyYW1lTm9kZSgpIHtcbiAgICAgIHJldHVybiB0aGlzLmlmcmFtZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRJZnJhbWVEb2N1bWVudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldElmcmFtZURvY3VtZW50KCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0SWZyYW1lTm9kZSgpLmNvbnRlbnREb2N1bWVudDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGb3JtTm9kZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEZvcm1Ob2RlKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0SWZyYW1lRG9jdW1lbnQoKS5nZXRFbGVtZW50QnlJZCgnZm9ybScpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldEZvcm1JbnB1dE5vZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGb3JtSW5wdXROb2RlKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0SWZyYW1lRG9jdW1lbnQoKS5nZXRFbGVtZW50QnlJZCgnaW5wdXQnKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGb3JtRGF0YU5vZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGb3JtRGF0YU5vZGUoKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXRJZnJhbWVEb2N1bWVudCgpLmdldEVsZW1lbnRCeUlkKCdkYXRhJyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0RmlsZUZvck11bHRpcGxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0RmlsZUZvck11bHRpcGxlKGZpbGUpIHtcbiAgICAgIHJldHVybiB0aGlzLnByb3BzLm11bHRpcGxlID8gW2ZpbGVdIDogZmlsZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRJZnJhbWVIVE1MJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0SWZyYW1lSFRNTChkb21haW4pIHtcbiAgICAgIHZhciBkb21haW5TY3JpcHQgPSAnJztcbiAgICAgIHZhciBkb21haW5JbnB1dCA9ICcnO1xuICAgICAgaWYgKGRvbWFpbikge1xuICAgICAgICB2YXIgc2NyaXB0ID0gJ3NjcmlwdCc7XG4gICAgICAgIGRvbWFpblNjcmlwdCA9ICc8JyArIHNjcmlwdCArICc+ZG9jdW1lbnQuZG9tYWluPVwiJyArIGRvbWFpbiArICdcIjs8LycgKyBzY3JpcHQgKyAnPic7XG4gICAgICAgIGRvbWFpbklucHV0ID0gJzxpbnB1dCBuYW1lPVwiX2RvY3VtZW50RG9tYWluXCIgdmFsdWU9XCInICsgZG9tYWluICsgJ1wiIC8+JztcbiAgICAgIH1cbiAgICAgIHJldHVybiAnXFxuICAgIDwhRE9DVFlQRSBodG1sPlxcbiAgICA8aHRtbD5cXG4gICAgPGhlYWQ+XFxuICAgIDxtZXRhIGh0dHAtZXF1aXY9XCJYLVVBLUNvbXBhdGlibGVcIiBjb250ZW50PVwiSUU9ZWRnZVwiIC8+XFxuICAgIDxzdHlsZT5cXG4gICAgYm9keSxodG1sIHtwYWRkaW5nOjA7bWFyZ2luOjA7Ym9yZGVyOjA7b3ZlcmZsb3c6aGlkZGVuO31cXG4gICAgPC9zdHlsZT5cXG4gICAgJyArIGRvbWFpblNjcmlwdCArICdcXG4gICAgPC9oZWFkPlxcbiAgICA8Ym9keT5cXG4gICAgPGZvcm0gbWV0aG9kPVwicG9zdFwiXFxuICAgIGVuY1R5cGU9XCJtdWx0aXBhcnQvZm9ybS1kYXRhXCJcXG4gICAgYWN0aW9uPVwiXCIgaWQ9XCJmb3JtXCJcXG4gICAgc3R5bGU9XCJkaXNwbGF5OmJsb2NrO2hlaWdodDo5OTk5cHg7cG9zaXRpb246cmVsYXRpdmU7b3ZlcmZsb3c6aGlkZGVuO1wiPlxcbiAgICA8aW5wdXQgaWQ9XCJpbnB1dFwiIHR5cGU9XCJmaWxlXCJcXG4gICAgIG5hbWU9XCInICsgdGhpcy5wcm9wcy5uYW1lICsgJ1wiXFxuICAgICBzdHlsZT1cInBvc2l0aW9uOmFic29sdXRlO3RvcDowO3JpZ2h0OjA7aGVpZ2h0Ojk5OTlweDtmb250LXNpemU6OTk5OXB4O2N1cnNvcjpwb2ludGVyO1wiLz5cXG4gICAgJyArIGRvbWFpbklucHV0ICsgJ1xcbiAgICA8c3BhbiBpZD1cImRhdGFcIj48L3NwYW4+XFxuICAgIDwvZm9ybT5cXG4gICAgPC9ib2R5PlxcbiAgICA8L2h0bWw+XFxuICAgICc7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaW5pdElmcmFtZVNyYycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGluaXRJZnJhbWVTcmMoKSB7XG4gICAgICBpZiAodGhpcy5kb21haW4pIHtcbiAgICAgICAgdGhpcy5nZXRJZnJhbWVOb2RlKCkuc3JjID0gJ2phdmFzY3JpcHQ6dm9pZCgoZnVuY3Rpb24oKXtcXG4gICAgICAgIHZhciBkID0gZG9jdW1lbnQ7XFxuICAgICAgICBkLm9wZW4oKTtcXG4gICAgICAgIGQuZG9tYWluPVxcJycgKyB0aGlzLmRvbWFpbiArICdcXCc7XFxuICAgICAgICBkLndyaXRlKFxcJ1xcJyk7XFxuICAgICAgICBkLmNsb3NlKCk7XFxuICAgICAgfSkoKSknO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2luaXRJZnJhbWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpbml0SWZyYW1lKCkge1xuICAgICAgdmFyIGlmcmFtZU5vZGUgPSB0aGlzLmdldElmcmFtZU5vZGUoKTtcbiAgICAgIHZhciB3aW4gPSBpZnJhbWVOb2RlLmNvbnRlbnRXaW5kb3c7XG4gICAgICB2YXIgZG9jID0gdm9pZCAwO1xuICAgICAgdGhpcy5kb21haW4gPSB0aGlzLmRvbWFpbiB8fCAnJztcbiAgICAgIHRoaXMuaW5pdElmcmFtZVNyYygpO1xuICAgICAgdHJ5IHtcbiAgICAgICAgZG9jID0gd2luLmRvY3VtZW50O1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICB0aGlzLmRvbWFpbiA9IGRvY3VtZW50LmRvbWFpbjtcbiAgICAgICAgdGhpcy5pbml0SWZyYW1lU3JjKCk7XG4gICAgICAgIHdpbiA9IGlmcmFtZU5vZGUuY29udGVudFdpbmRvdztcbiAgICAgICAgZG9jID0gd2luLmRvY3VtZW50O1xuICAgICAgfVxuICAgICAgZG9jLm9wZW4oJ3RleHQvaHRtbCcsICdyZXBsYWNlJyk7XG4gICAgICBkb2Mud3JpdGUodGhpcy5nZXRJZnJhbWVIVE1MKHRoaXMuZG9tYWluKSk7XG4gICAgICBkb2MuY2xvc2UoKTtcbiAgICAgIHRoaXMuZ2V0Rm9ybUlucHV0Tm9kZSgpLm9uY2hhbmdlID0gdGhpcy5vbkNoYW5nZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdlbmRVcGxvYWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBlbmRVcGxvYWQoKSB7XG4gICAgICBpZiAodGhpcy5zdGF0ZS51cGxvYWRpbmcpIHtcbiAgICAgICAgdGhpcy5maWxlID0ge307XG4gICAgICAgIC8vIGhhY2sgYXZvaWQgYmF0Y2hcbiAgICAgICAgdGhpcy5zdGF0ZS51cGxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgdXBsb2FkaW5nOiBmYWxzZVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5pbml0SWZyYW1lKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc3RhcnRVcGxvYWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzdGFydFVwbG9hZCgpIHtcbiAgICAgIGlmICghdGhpcy5zdGF0ZS51cGxvYWRpbmcpIHtcbiAgICAgICAgdGhpcy5zdGF0ZS51cGxvYWRpbmcgPSB0cnVlO1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICB1cGxvYWRpbmc6IHRydWVcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndXBkYXRlSWZyYW1lV0gnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1cGRhdGVJZnJhbWVXSCgpIHtcbiAgICAgIHZhciByb290Tm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMpO1xuICAgICAgdmFyIGlmcmFtZU5vZGUgPSB0aGlzLmdldElmcmFtZU5vZGUoKTtcbiAgICAgIGlmcmFtZU5vZGUuc3R5bGUuaGVpZ2h0ID0gcm9vdE5vZGUub2Zmc2V0SGVpZ2h0ICsgJ3B4JztcbiAgICAgIGlmcmFtZU5vZGUuc3R5bGUud2lkdGggPSByb290Tm9kZS5vZmZzZXRXaWR0aCArICdweCc7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnYWJvcnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBhYm9ydChmaWxlKSB7XG4gICAgICBpZiAoZmlsZSkge1xuICAgICAgICB2YXIgdWlkID0gZmlsZTtcbiAgICAgICAgaWYgKGZpbGUgJiYgZmlsZS51aWQpIHtcbiAgICAgICAgICB1aWQgPSBmaWxlLnVpZDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodWlkID09PSB0aGlzLmZpbGUudWlkKSB7XG4gICAgICAgICAgdGhpcy5lbmRVcGxvYWQoKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5lbmRVcGxvYWQoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdwb3N0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcG9zdChmaWxlKSB7XG4gICAgICB2YXIgX3RoaXM0ID0gdGhpcztcblxuICAgICAgdmFyIGZvcm1Ob2RlID0gdGhpcy5nZXRGb3JtTm9kZSgpO1xuICAgICAgdmFyIGRhdGFTcGFuID0gdGhpcy5nZXRGb3JtRGF0YU5vZGUoKTtcbiAgICAgIHZhciBkYXRhID0gdGhpcy5wcm9wcy5kYXRhO1xuICAgICAgdmFyIG9uU3RhcnQgPSB0aGlzLnByb3BzLm9uU3RhcnQ7XG5cbiAgICAgIGlmICh0eXBlb2YgZGF0YSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBkYXRhID0gZGF0YShmaWxlKTtcbiAgICAgIH1cbiAgICAgIHZhciBpbnB1dHMgPSBkb2N1bWVudC5jcmVhdGVEb2N1bWVudEZyYWdtZW50KCk7XG4gICAgICBmb3IgKHZhciBrZXkgaW4gZGF0YSkge1xuICAgICAgICBpZiAoZGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgdmFyIGlucHV0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKTtcbiAgICAgICAgICBpbnB1dC5zZXRBdHRyaWJ1dGUoJ25hbWUnLCBrZXkpO1xuICAgICAgICAgIGlucHV0LnZhbHVlID0gZGF0YVtrZXldO1xuICAgICAgICAgIGlucHV0cy5hcHBlbmRDaGlsZChpbnB1dCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGRhdGFTcGFuLmFwcGVuZENoaWxkKGlucHV0cyk7XG4gICAgICBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSkge1xuICAgICAgICB2YXIgYWN0aW9uID0gX3RoaXM0LnByb3BzLmFjdGlvbjtcblxuICAgICAgICBpZiAodHlwZW9mIGFjdGlvbiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIHJldHVybiByZXNvbHZlKGFjdGlvbihmaWxlKSk7XG4gICAgICAgIH1cbiAgICAgICAgcmVzb2x2ZShhY3Rpb24pO1xuICAgICAgfSkudGhlbihmdW5jdGlvbiAoYWN0aW9uKSB7XG4gICAgICAgIGZvcm1Ob2RlLnNldEF0dHJpYnV0ZSgnYWN0aW9uJywgYWN0aW9uKTtcbiAgICAgICAgZm9ybU5vZGUuc3VibWl0KCk7XG4gICAgICAgIGRhdGFTcGFuLmlubmVySFRNTCA9ICcnO1xuICAgICAgICBvblN0YXJ0KGZpbGUpO1xuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBUYWcgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzLmRpc2FibGVkLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3Byb3BzLnByZWZpeENscyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZTtcblxuICAgICAgdmFyIGlmcmFtZVN0eWxlID0gX2V4dGVuZHMoe30sIElGUkFNRV9TVFlMRSwge1xuICAgICAgICBkaXNwbGF5OiB0aGlzLnN0YXRlLnVwbG9hZGluZyB8fCBkaXNhYmxlZCA/ICdub25lJyA6ICcnXG4gICAgICB9KTtcbiAgICAgIHZhciBjbHMgPSBjbGFzc05hbWVzKChfY2xhc3NOYW1lcyA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIHByZWZpeENscywgdHJ1ZSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgcHJlZml4Q2xzICsgJy1kaXNhYmxlZCcsIGRpc2FibGVkKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBjbGFzc05hbWUsIGNsYXNzTmFtZSksIF9jbGFzc05hbWVzKSk7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgVGFnLFxuICAgICAgICB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbHMsXG4gICAgICAgICAgc3R5bGU6IF9leHRlbmRzKHsgcG9zaXRpb246ICdyZWxhdGl2ZScsIHpJbmRleDogMCB9LCBzdHlsZSlcbiAgICAgICAgfSxcbiAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudCgnaWZyYW1lJywge1xuICAgICAgICAgIHJlZjogdGhpcy5zYXZlSWZyYW1lLFxuICAgICAgICAgIG9uTG9hZDogdGhpcy5vbkxvYWQsXG4gICAgICAgICAgc3R5bGU6IGlmcmFtZVN0eWxlXG4gICAgICAgIH0pLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gSWZyYW1lVXBsb2FkZXI7XG59KENvbXBvbmVudCk7XG5cbklmcmFtZVVwbG9hZGVyLnByb3BUeXBlcyA9IHtcbiAgY29tcG9uZW50OiBQcm9wVHlwZXMuc3RyaW5nLFxuICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgYWNjZXB0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICBvblN0YXJ0OiBQcm9wVHlwZXMuZnVuYyxcbiAgbXVsdGlwbGU6IFByb3BUeXBlcy5ib29sLFxuICBjaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcbiAgZGF0YTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm9iamVjdCwgUHJvcFR5cGVzLmZ1bmNdKSxcbiAgYWN0aW9uOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMuZnVuY10pLFxuICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nXG59O1xuXG5cbmV4cG9ydCBkZWZhdWx0IElmcmFtZVVwbG9hZGVyOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBcEJBO0FBc0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBDQTtBQXNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFPQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFIQTtBQU9BO0FBOUJBO0FBQ0E7QUFnQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWdCQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-upload/es/IframeUploader.js
