__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getStyleProperty", function() { return getStyleProperty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getStyleValue", function() { return getStyleValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVendorPrefixes", function() { return getVendorPrefixes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVendorPrefixedEventName", function() { return getVendorPrefixedEventName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "animationEndName", function() { return animationEndName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transitionEndName", function() { return transitionEndName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportTransition", function() { return supportTransition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mergeChildren", function() { return mergeChildren; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cloneProps", function() { return cloneProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTransitionName", function() { return getTransitionName; });
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fbjs/lib/ExecutionEnvironment */ "./node_modules/fbjs/lib/ExecutionEnvironment.js");
/* harmony import */ var fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_1__);

 // =================== Style ====================

var stylePrefixes = ['-webkit-', '-moz-', '-o-', 'ms-', ''];
function getStyleProperty(node, name) {
  // old ff need null, https://developer.mozilla.org/en-US/docs/Web/API/Window/getComputedStyle
  var style = window.getComputedStyle(node, null);
  var ret = '';

  for (var i = 0; i < stylePrefixes.length; i++) {
    ret = style.getPropertyValue(stylePrefixes[i] + name);

    if (ret) {
      break;
    }
  }

  return ret;
}
function getStyleValue(node, name) {
  return parseFloat(getStyleProperty(node, name));
} // ================= Transition =================
// Event wrapper. Copy from react source code

function makePrefixMap(styleProp, eventName) {
  var prefixes = {};
  prefixes[styleProp.toLowerCase()] = eventName.toLowerCase();
  prefixes['Webkit' + styleProp] = 'webkit' + eventName;
  prefixes['Moz' + styleProp] = 'moz' + eventName;
  prefixes['ms' + styleProp] = 'MS' + eventName;
  prefixes['O' + styleProp] = 'o' + eventName.toLowerCase();
  return prefixes;
}

function getVendorPrefixes(domSupport, win) {
  var prefixes = {
    animationend: makePrefixMap('Animation', 'AnimationEnd'),
    transitionend: makePrefixMap('Transition', 'TransitionEnd')
  };

  if (domSupport) {
    if (!('AnimationEvent' in win)) {
      delete prefixes.animationend.animation;
    }

    if (!('TransitionEvent' in win)) {
      delete prefixes.transitionend.transition;
    }
  }

  return prefixes;
}
var vendorPrefixes = getVendorPrefixes(fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_1__["canUseDOM"], typeof window !== 'undefined' ? window : {});
var style = {};

if (fbjs_lib_ExecutionEnvironment__WEBPACK_IMPORTED_MODULE_1__["canUseDOM"]) {
  style = document.createElement('div').style;
}

var prefixedEventNames = {};
function getVendorPrefixedEventName(eventName) {
  if (prefixedEventNames[eventName]) {
    return prefixedEventNames[eventName];
  }

  var prefixMap = vendorPrefixes[eventName];

  if (prefixMap) {
    var stylePropList = Object.keys(prefixMap);
    var len = stylePropList.length;

    for (var i = 0; i < len; i += 1) {
      var styleProp = stylePropList[i];

      if (Object.prototype.hasOwnProperty.call(prefixMap, styleProp) && styleProp in style) {
        prefixedEventNames[eventName] = prefixMap[styleProp];
        return prefixedEventNames[eventName];
      }
    }
  }

  return '';
}
var animationEndName = getVendorPrefixedEventName('animationend');
var transitionEndName = getVendorPrefixedEventName('transitionend');
var supportTransition = !!(animationEndName && transitionEndName); // ==================== Node ====================

/**
 * [Legacy] Find the same children in both prev & next list.
 * Insert not find one before the find one, otherwise in the end. For example:
 * - prev: [1,2,3]
 * - next: [2,4]
 * -> [1,2,4,3]
 */

function mergeChildren(prev, next) {
  var prevList = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_0__["default"])(prev);
  var nextList = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_0__["default"])(next); // Skip if is single children

  if (prevList.length === 1 && nextList.length === 1 && prevList[0].key === nextList[0].key) {
    return nextList;
  }

  var mergeList = [];
  var nextChildrenMap = {};
  var missMatchChildrenList = []; // Fill matched prev node into next node map

  prevList.forEach(function (prevNode) {
    if (prevNode && nextList.some(function (_ref) {
      var key = _ref.key;
      return key === prevNode.key;
    })) {
      if (missMatchChildrenList.length) {
        nextChildrenMap[prevNode.key] = missMatchChildrenList;
        missMatchChildrenList = [];
      }
    } else {
      missMatchChildrenList.push(prevNode);
    }
  }); // Insert prev node before the matched next node

  nextList.forEach(function (nextNode) {
    if (nextNode && nextChildrenMap[nextNode.key]) {
      mergeList = mergeList.concat(nextChildrenMap[nextNode.key]);
    }

    mergeList.push(nextNode);
  });
  mergeList = mergeList.concat(missMatchChildrenList);
  return mergeList;
}
function cloneProps(props, propList) {
  var newProps = {};
  propList.forEach(function (prop) {
    if (prop in props) {
      newProps[prop] = props[prop];
    }
  });
  return newProps;
}
function getTransitionName(transitionName, transitionType) {
  if (!transitionName) return null;

  if (typeof transitionName === 'object') {
    var type = transitionType.replace(/-\w/g, function (match) {
      return match[1].toUpperCase();
    });
    return transitionName[type];
  }

  return transitionName + '-' + transitionType;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvbm9kZV9tb2R1bGVzL3JjLWFuaW1hdGUvZXMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRyZWUtc2VsZWN0L25vZGVfbW9kdWxlcy9yYy10cmlnZ2VyL25vZGVfbW9kdWxlcy9yYy1hbmltYXRlL2VzL3V0aWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHRvQXJyYXkgZnJvbSAncmMtdXRpbC9lcy9DaGlsZHJlbi90b0FycmF5JztcbmltcG9ydCB7IGNhblVzZURPTSB9IGZyb20gJ2ZianMvbGliL0V4ZWN1dGlvbkVudmlyb25tZW50JztcblxuLy8gPT09PT09PT09PT09PT09PT09PSBTdHlsZSA9PT09PT09PT09PT09PT09PT09PVxudmFyIHN0eWxlUHJlZml4ZXMgPSBbJy13ZWJraXQtJywgJy1tb3otJywgJy1vLScsICdtcy0nLCAnJ107XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRTdHlsZVByb3BlcnR5KG5vZGUsIG5hbWUpIHtcbiAgLy8gb2xkIGZmIG5lZWQgbnVsbCwgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvQVBJL1dpbmRvdy9nZXRDb21wdXRlZFN0eWxlXG4gIHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKG5vZGUsIG51bGwpO1xuICB2YXIgcmV0ID0gJyc7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVQcmVmaXhlcy5sZW5ndGg7IGkrKykge1xuICAgIHJldCA9IHN0eWxlLmdldFByb3BlcnR5VmFsdWUoc3R5bGVQcmVmaXhlc1tpXSArIG5hbWUpO1xuICAgIGlmIChyZXQpIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuICByZXR1cm4gcmV0O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0U3R5bGVWYWx1ZShub2RlLCBuYW1lKSB7XG4gIHJldHVybiBwYXJzZUZsb2F0KGdldFN0eWxlUHJvcGVydHkobm9kZSwgbmFtZSkpO1xufVxuXG4vLyA9PT09PT09PT09PT09PT09PSBUcmFuc2l0aW9uID09PT09PT09PT09PT09PT09XG4vLyBFdmVudCB3cmFwcGVyLiBDb3B5IGZyb20gcmVhY3Qgc291cmNlIGNvZGVcbmZ1bmN0aW9uIG1ha2VQcmVmaXhNYXAoc3R5bGVQcm9wLCBldmVudE5hbWUpIHtcbiAgdmFyIHByZWZpeGVzID0ge307XG5cbiAgcHJlZml4ZXNbc3R5bGVQcm9wLnRvTG93ZXJDYXNlKCldID0gZXZlbnROYW1lLnRvTG93ZXJDYXNlKCk7XG4gIHByZWZpeGVzWydXZWJraXQnICsgc3R5bGVQcm9wXSA9ICd3ZWJraXQnICsgZXZlbnROYW1lO1xuICBwcmVmaXhlc1snTW96JyArIHN0eWxlUHJvcF0gPSAnbW96JyArIGV2ZW50TmFtZTtcbiAgcHJlZml4ZXNbJ21zJyArIHN0eWxlUHJvcF0gPSAnTVMnICsgZXZlbnROYW1lO1xuICBwcmVmaXhlc1snTycgKyBzdHlsZVByb3BdID0gJ28nICsgZXZlbnROYW1lLnRvTG93ZXJDYXNlKCk7XG5cbiAgcmV0dXJuIHByZWZpeGVzO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VmVuZG9yUHJlZml4ZXMoZG9tU3VwcG9ydCwgd2luKSB7XG4gIHZhciBwcmVmaXhlcyA9IHtcbiAgICBhbmltYXRpb25lbmQ6IG1ha2VQcmVmaXhNYXAoJ0FuaW1hdGlvbicsICdBbmltYXRpb25FbmQnKSxcbiAgICB0cmFuc2l0aW9uZW5kOiBtYWtlUHJlZml4TWFwKCdUcmFuc2l0aW9uJywgJ1RyYW5zaXRpb25FbmQnKVxuICB9O1xuXG4gIGlmIChkb21TdXBwb3J0KSB7XG4gICAgaWYgKCEoJ0FuaW1hdGlvbkV2ZW50JyBpbiB3aW4pKSB7XG4gICAgICBkZWxldGUgcHJlZml4ZXMuYW5pbWF0aW9uZW5kLmFuaW1hdGlvbjtcbiAgICB9XG5cbiAgICBpZiAoISgnVHJhbnNpdGlvbkV2ZW50JyBpbiB3aW4pKSB7XG4gICAgICBkZWxldGUgcHJlZml4ZXMudHJhbnNpdGlvbmVuZC50cmFuc2l0aW9uO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBwcmVmaXhlcztcbn1cblxudmFyIHZlbmRvclByZWZpeGVzID0gZ2V0VmVuZG9yUHJlZml4ZXMoY2FuVXNlRE9NLCB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyA/IHdpbmRvdyA6IHt9KTtcblxudmFyIHN0eWxlID0ge307XG5cbmlmIChjYW5Vc2VET00pIHtcbiAgc3R5bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKS5zdHlsZTtcbn1cblxudmFyIHByZWZpeGVkRXZlbnROYW1lcyA9IHt9O1xuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VmVuZG9yUHJlZml4ZWRFdmVudE5hbWUoZXZlbnROYW1lKSB7XG4gIGlmIChwcmVmaXhlZEV2ZW50TmFtZXNbZXZlbnROYW1lXSkge1xuICAgIHJldHVybiBwcmVmaXhlZEV2ZW50TmFtZXNbZXZlbnROYW1lXTtcbiAgfVxuXG4gIHZhciBwcmVmaXhNYXAgPSB2ZW5kb3JQcmVmaXhlc1tldmVudE5hbWVdO1xuXG4gIGlmIChwcmVmaXhNYXApIHtcbiAgICB2YXIgc3R5bGVQcm9wTGlzdCA9IE9iamVjdC5rZXlzKHByZWZpeE1hcCk7XG4gICAgdmFyIGxlbiA9IHN0eWxlUHJvcExpc3QubGVuZ3RoO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpICs9IDEpIHtcbiAgICAgIHZhciBzdHlsZVByb3AgPSBzdHlsZVByb3BMaXN0W2ldO1xuICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChwcmVmaXhNYXAsIHN0eWxlUHJvcCkgJiYgc3R5bGVQcm9wIGluIHN0eWxlKSB7XG4gICAgICAgIHByZWZpeGVkRXZlbnROYW1lc1tldmVudE5hbWVdID0gcHJlZml4TWFwW3N0eWxlUHJvcF07XG4gICAgICAgIHJldHVybiBwcmVmaXhlZEV2ZW50TmFtZXNbZXZlbnROYW1lXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gJyc7XG59XG5cbmV4cG9ydCB2YXIgYW5pbWF0aW9uRW5kTmFtZSA9IGdldFZlbmRvclByZWZpeGVkRXZlbnROYW1lKCdhbmltYXRpb25lbmQnKTtcbmV4cG9ydCB2YXIgdHJhbnNpdGlvbkVuZE5hbWUgPSBnZXRWZW5kb3JQcmVmaXhlZEV2ZW50TmFtZSgndHJhbnNpdGlvbmVuZCcpO1xuZXhwb3J0IHZhciBzdXBwb3J0VHJhbnNpdGlvbiA9ICEhKGFuaW1hdGlvbkVuZE5hbWUgJiYgdHJhbnNpdGlvbkVuZE5hbWUpO1xuXG4vLyA9PT09PT09PT09PT09PT09PT09PSBOb2RlID09PT09PT09PT09PT09PT09PT09XG4vKipcbiAqIFtMZWdhY3ldIEZpbmQgdGhlIHNhbWUgY2hpbGRyZW4gaW4gYm90aCBwcmV2ICYgbmV4dCBsaXN0LlxuICogSW5zZXJ0IG5vdCBmaW5kIG9uZSBiZWZvcmUgdGhlIGZpbmQgb25lLCBvdGhlcndpc2UgaW4gdGhlIGVuZC4gRm9yIGV4YW1wbGU6XG4gKiAtIHByZXY6IFsxLDIsM11cbiAqIC0gbmV4dDogWzIsNF1cbiAqIC0+IFsxLDIsNCwzXVxuICovXG5leHBvcnQgZnVuY3Rpb24gbWVyZ2VDaGlsZHJlbihwcmV2LCBuZXh0KSB7XG4gIHZhciBwcmV2TGlzdCA9IHRvQXJyYXkocHJldik7XG4gIHZhciBuZXh0TGlzdCA9IHRvQXJyYXkobmV4dCk7XG5cbiAgLy8gU2tpcCBpZiBpcyBzaW5nbGUgY2hpbGRyZW5cbiAgaWYgKHByZXZMaXN0Lmxlbmd0aCA9PT0gMSAmJiBuZXh0TGlzdC5sZW5ndGggPT09IDEgJiYgcHJldkxpc3RbMF0ua2V5ID09PSBuZXh0TGlzdFswXS5rZXkpIHtcbiAgICByZXR1cm4gbmV4dExpc3Q7XG4gIH1cblxuICB2YXIgbWVyZ2VMaXN0ID0gW107XG4gIHZhciBuZXh0Q2hpbGRyZW5NYXAgPSB7fTtcbiAgdmFyIG1pc3NNYXRjaENoaWxkcmVuTGlzdCA9IFtdO1xuXG4gIC8vIEZpbGwgbWF0Y2hlZCBwcmV2IG5vZGUgaW50byBuZXh0IG5vZGUgbWFwXG4gIHByZXZMaXN0LmZvckVhY2goZnVuY3Rpb24gKHByZXZOb2RlKSB7XG4gICAgaWYgKHByZXZOb2RlICYmIG5leHRMaXN0LnNvbWUoZnVuY3Rpb24gKF9yZWYpIHtcbiAgICAgIHZhciBrZXkgPSBfcmVmLmtleTtcbiAgICAgIHJldHVybiBrZXkgPT09IHByZXZOb2RlLmtleTtcbiAgICB9KSkge1xuICAgICAgaWYgKG1pc3NNYXRjaENoaWxkcmVuTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgbmV4dENoaWxkcmVuTWFwW3ByZXZOb2RlLmtleV0gPSBtaXNzTWF0Y2hDaGlsZHJlbkxpc3Q7XG4gICAgICAgIG1pc3NNYXRjaENoaWxkcmVuTGlzdCA9IFtdO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBtaXNzTWF0Y2hDaGlsZHJlbkxpc3QucHVzaChwcmV2Tm9kZSk7XG4gICAgfVxuICB9KTtcblxuICAvLyBJbnNlcnQgcHJldiBub2RlIGJlZm9yZSB0aGUgbWF0Y2hlZCBuZXh0IG5vZGVcbiAgbmV4dExpc3QuZm9yRWFjaChmdW5jdGlvbiAobmV4dE5vZGUpIHtcbiAgICBpZiAobmV4dE5vZGUgJiYgbmV4dENoaWxkcmVuTWFwW25leHROb2RlLmtleV0pIHtcbiAgICAgIG1lcmdlTGlzdCA9IG1lcmdlTGlzdC5jb25jYXQobmV4dENoaWxkcmVuTWFwW25leHROb2RlLmtleV0pO1xuICAgIH1cbiAgICBtZXJnZUxpc3QucHVzaChuZXh0Tm9kZSk7XG4gIH0pO1xuXG4gIG1lcmdlTGlzdCA9IG1lcmdlTGlzdC5jb25jYXQobWlzc01hdGNoQ2hpbGRyZW5MaXN0KTtcblxuICByZXR1cm4gbWVyZ2VMaXN0O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY2xvbmVQcm9wcyhwcm9wcywgcHJvcExpc3QpIHtcbiAgdmFyIG5ld1Byb3BzID0ge307XG4gIHByb3BMaXN0LmZvckVhY2goZnVuY3Rpb24gKHByb3ApIHtcbiAgICBpZiAocHJvcCBpbiBwcm9wcykge1xuICAgICAgbmV3UHJvcHNbcHJvcF0gPSBwcm9wc1twcm9wXTtcbiAgICB9XG4gIH0pO1xuXG4gIHJldHVybiBuZXdQcm9wcztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFRyYW5zaXRpb25OYW1lKHRyYW5zaXRpb25OYW1lLCB0cmFuc2l0aW9uVHlwZSkge1xuICBpZiAoIXRyYW5zaXRpb25OYW1lKSByZXR1cm4gbnVsbDtcblxuICBpZiAodHlwZW9mIHRyYW5zaXRpb25OYW1lID09PSAnb2JqZWN0Jykge1xuICAgIHZhciB0eXBlID0gdHJhbnNpdGlvblR5cGUucmVwbGFjZSgvLVxcdy9nLCBmdW5jdGlvbiAobWF0Y2gpIHtcbiAgICAgIHJldHVybiBtYXRjaFsxXS50b1VwcGVyQ2FzZSgpO1xuICAgIH0pO1xuICAgIHJldHVybiB0cmFuc2l0aW9uTmFtZVt0eXBlXTtcbiAgfVxuXG4gIHJldHVybiB0cmFuc2l0aW9uTmFtZSArICctJyArIHRyYW5zaXRpb25UeXBlO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/util.js
