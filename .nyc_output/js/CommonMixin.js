__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "propType", function() { return propType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultProp", function() { return defaultProp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "commonMixinWrapper", function() { return commonMixinWrapper; });
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _locale_en_US__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../locale/en_US */ "./node_modules/rc-calendar/es/locale/en_US.js");






function noop() {}

var propType = {
  className: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string,
  locale: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  style: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.object,
  visible: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.string,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func,
  onOk: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.func
};
var defaultProp = {
  locale: _locale_en_US__WEBPACK_IMPORTED_MODULE_4__["default"],
  style: {},
  visible: true,
  prefixCls: 'rc-calendar',
  className: '',
  onSelect: noop,
  onChange: noop,
  onClear: noop,
  renderFooter: function renderFooter() {
    return null;
  },
  renderSidebar: function renderSidebar() {
    return null;
  }
};
var commonMixinWrapper = function commonMixinWrapper(ComposeComponent) {
  var _class, _temp2;

  return _temp2 = _class = function (_ComposeComponent) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(_class, _ComposeComponent);

    function _class() {
      var _temp, _this, _ret;

      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, _class);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(this, _ComposeComponent.call.apply(_ComposeComponent, [this].concat(args))), _this), _this.getFormat = function () {
        var format = _this.props.format;
        var _this$props = _this.props,
            locale = _this$props.locale,
            timePicker = _this$props.timePicker;

        if (!format) {
          if (timePicker) {
            format = locale.dateTimeFormat;
          } else {
            format = locale.dateFormat;
          }
        }

        return format;
      }, _this.focus = function () {
        if (_this.focusElement) {
          _this.focusElement.focus();
        } else if (_this.rootInstance) {
          _this.rootInstance.focus();
        }
      }, _this.saveFocusElement = function (focusElement) {
        _this.focusElement = focusElement;
      }, _this.saveRoot = function (root) {
        _this.rootInstance = root;
      }, _temp), babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(_this, _ret);
    }

    _class.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
      return this.props.visible || nextProps.visible;
    };

    return _class;
  }(ComposeComponent), _class.displayName = 'CommonMixinWrapper', _class.defaultProps = ComposeComponent.defaultProps, _class.getDerivedStateFromProps = ComposeComponent.getDerivedStateFromProps, _temp2;
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvbWl4aW4vQ29tbW9uTWl4aW4uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1jYWxlbmRhci9lcy9taXhpbi9Db21tb25NaXhpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBlblVzIGZyb20gJy4uL2xvY2FsZS9lbl9VUyc7XG5cbmZ1bmN0aW9uIG5vb3AoKSB7fVxuXG5leHBvcnQgdmFyIHByb3BUeXBlID0ge1xuICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGxvY2FsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gIHZpc2libGU6IFByb3BUeXBlcy5ib29sLFxuICBvblNlbGVjdDogUHJvcFR5cGVzLmZ1bmMsXG4gIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICBvbk9rOiBQcm9wVHlwZXMuZnVuY1xufTtcblxuZXhwb3J0IHZhciBkZWZhdWx0UHJvcCA9IHtcbiAgbG9jYWxlOiBlblVzLFxuICBzdHlsZToge30sXG4gIHZpc2libGU6IHRydWUsXG4gIHByZWZpeENsczogJ3JjLWNhbGVuZGFyJyxcbiAgY2xhc3NOYW1lOiAnJyxcbiAgb25TZWxlY3Q6IG5vb3AsXG4gIG9uQ2hhbmdlOiBub29wLFxuICBvbkNsZWFyOiBub29wLFxuICByZW5kZXJGb290ZXI6IGZ1bmN0aW9uIHJlbmRlckZvb3RlcigpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfSxcbiAgcmVuZGVyU2lkZWJhcjogZnVuY3Rpb24gcmVuZGVyU2lkZWJhcigpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxufTtcblxuZXhwb3J0IHZhciBjb21tb25NaXhpbldyYXBwZXIgPSBmdW5jdGlvbiBjb21tb25NaXhpbldyYXBwZXIoQ29tcG9zZUNvbXBvbmVudCkge1xuICB2YXIgX2NsYXNzLCBfdGVtcDI7XG5cbiAgcmV0dXJuIF90ZW1wMiA9IF9jbGFzcyA9IGZ1bmN0aW9uIChfQ29tcG9zZUNvbXBvbmVudCkge1xuICAgIF9pbmhlcml0cyhfY2xhc3MsIF9Db21wb3NlQ29tcG9uZW50KTtcblxuICAgIGZ1bmN0aW9uIF9jbGFzcygpIHtcbiAgICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBfY2xhc3MpO1xuXG4gICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb3NlQ29tcG9uZW50LmNhbGwuYXBwbHkoX0NvbXBvc2VDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5nZXRGb3JtYXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBmb3JtYXQgPSBfdGhpcy5wcm9wcy5mb3JtYXQ7XG4gICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgbG9jYWxlID0gX3RoaXMkcHJvcHMubG9jYWxlLFxuICAgICAgICAgICAgdGltZVBpY2tlciA9IF90aGlzJHByb3BzLnRpbWVQaWNrZXI7XG5cbiAgICAgICAgaWYgKCFmb3JtYXQpIHtcbiAgICAgICAgICBpZiAodGltZVBpY2tlcikge1xuICAgICAgICAgICAgZm9ybWF0ID0gbG9jYWxlLmRhdGVUaW1lRm9ybWF0O1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBmb3JtYXQgPSBsb2NhbGUuZGF0ZUZvcm1hdDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZvcm1hdDtcbiAgICAgIH0sIF90aGlzLmZvY3VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoX3RoaXMuZm9jdXNFbGVtZW50KSB7XG4gICAgICAgICAgX3RoaXMuZm9jdXNFbGVtZW50LmZvY3VzKCk7XG4gICAgICAgIH0gZWxzZSBpZiAoX3RoaXMucm9vdEluc3RhbmNlKSB7XG4gICAgICAgICAgX3RoaXMucm9vdEluc3RhbmNlLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0sIF90aGlzLnNhdmVGb2N1c0VsZW1lbnQgPSBmdW5jdGlvbiAoZm9jdXNFbGVtZW50KSB7XG4gICAgICAgIF90aGlzLmZvY3VzRWxlbWVudCA9IGZvY3VzRWxlbWVudDtcbiAgICAgIH0sIF90aGlzLnNhdmVSb290ID0gZnVuY3Rpb24gKHJvb3QpIHtcbiAgICAgICAgX3RoaXMucm9vdEluc3RhbmNlID0gcm9vdDtcbiAgICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICAgIH1cblxuICAgIF9jbGFzcy5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgcmV0dXJuIHRoaXMucHJvcHMudmlzaWJsZSB8fCBuZXh0UHJvcHMudmlzaWJsZTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIF9jbGFzcztcbiAgfShDb21wb3NlQ29tcG9uZW50KSwgX2NsYXNzLmRpc3BsYXlOYW1lID0gJ0NvbW1vbk1peGluV3JhcHBlcicsIF9jbGFzcy5kZWZhdWx0UHJvcHMgPSBDb21wb3NlQ29tcG9uZW50LmRlZmF1bHRQcm9wcywgX2NsYXNzLmdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyA9IENvbXBvc2VDb21wb25lbnQuZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzLCBfdGVtcDI7XG59OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/mixin/CommonMixin.js
