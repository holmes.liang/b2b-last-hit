/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEditor.react
 * @format
 * 
 * @preventMunge
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var DefaultDraftBlockRenderMap = __webpack_require__(/*! ./DefaultDraftBlockRenderMap */ "./node_modules/draft-js/lib/DefaultDraftBlockRenderMap.js");

var DefaultDraftInlineStyle = __webpack_require__(/*! ./DefaultDraftInlineStyle */ "./node_modules/draft-js/lib/DefaultDraftInlineStyle.js");

var DraftEditorCompositionHandler = __webpack_require__(/*! ./DraftEditorCompositionHandler */ "./node_modules/draft-js/lib/DraftEditorCompositionHandler.js");

var DraftEditorContents = __webpack_require__(/*! ./DraftEditorContents.react */ "./node_modules/draft-js/lib/DraftEditorContents.react.js");

var DraftEditorDragHandler = __webpack_require__(/*! ./DraftEditorDragHandler */ "./node_modules/draft-js/lib/DraftEditorDragHandler.js");

var DraftEditorEditHandler = __webpack_require__(/*! ./DraftEditorEditHandler */ "./node_modules/draft-js/lib/DraftEditorEditHandler.js");

var DraftEditorPlaceholder = __webpack_require__(/*! ./DraftEditorPlaceholder.react */ "./node_modules/draft-js/lib/DraftEditorPlaceholder.react.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var Scroll = __webpack_require__(/*! fbjs/lib/Scroll */ "./node_modules/fbjs/lib/Scroll.js");

var Style = __webpack_require__(/*! fbjs/lib/Style */ "./node_modules/fbjs/lib/Style.js");

var UserAgent = __webpack_require__(/*! fbjs/lib/UserAgent */ "./node_modules/fbjs/lib/UserAgent.js");

var cx = __webpack_require__(/*! fbjs/lib/cx */ "./node_modules/fbjs/lib/cx.js");

var emptyFunction = __webpack_require__(/*! fbjs/lib/emptyFunction */ "./node_modules/fbjs/lib/emptyFunction.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var getDefaultKeyBinding = __webpack_require__(/*! ./getDefaultKeyBinding */ "./node_modules/draft-js/lib/getDefaultKeyBinding.js");

var getScrollPosition = __webpack_require__(/*! fbjs/lib/getScrollPosition */ "./node_modules/fbjs/lib/getScrollPosition.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");

var isIE = UserAgent.isBrowser('IE'); // IE does not support the `input` event on contentEditable, so we can't
// observe spellcheck behavior.

var allowSpellCheck = !isIE; // Define a set of handler objects to correspond to each possible `mode`
// of editor behavior.

var handlerMap = {
  edit: DraftEditorEditHandler,
  composite: DraftEditorCompositionHandler,
  drag: DraftEditorDragHandler,
  cut: null,
  render: null
};
/**
 * `DraftEditor` is the root editor component. It composes a `contentEditable`
 * div, and provides a wide variety of useful function props for managing the
 * state of the editor. See `DraftEditorProps` for details.
 */

var DraftEditor = function (_React$Component) {
  _inherits(DraftEditor, _React$Component);

  function DraftEditor(props) {
    _classCallCheck(this, DraftEditor);

    var _this = _possibleConstructorReturn(this, _React$Component.call(this, props));

    _this.focus = function (scrollPosition) {
      var editorState = _this.props.editorState;
      var alreadyHasFocus = editorState.getSelection().getHasFocus();
      var editorNode = ReactDOM.findDOMNode(_this.editor);

      if (!editorNode) {
        // once in a while people call 'focus' in a setTimeout, and the node has
        // been deleted, so it can be null in that case.
        return;
      }

      var scrollParent = Style.getScrollParent(editorNode);

      var _ref = scrollPosition || getScrollPosition(scrollParent),
          x = _ref.x,
          y = _ref.y;

      !(editorNode instanceof HTMLElement) ?  true ? invariant(false, 'editorNode is not an HTMLElement') : undefined : void 0;
      editorNode.focus(); // Restore scroll position

      if (scrollParent === window) {
        window.scrollTo(x, y);
      } else {
        Scroll.setTop(scrollParent, y);
      } // On Chrome and Safari, calling focus on contenteditable focuses the
      // cursor at the first character. This is something you don't expect when
      // you're clicking on an input element but not directly on a character.
      // Put the cursor back where it was before the blur.


      if (!alreadyHasFocus) {
        _this.update(EditorState.forceSelection(editorState, editorState.getSelection()));
      }
    };

    _this.blur = function () {
      var editorNode = ReactDOM.findDOMNode(_this.editor);
      !(editorNode instanceof HTMLElement) ?  true ? invariant(false, 'editorNode is not an HTMLElement') : undefined : void 0;
      editorNode.blur();
    };

    _this.setMode = function (mode) {
      _this._handler = handlerMap[mode];
    };

    _this.exitCurrentMode = function () {
      _this.setMode('edit');
    };

    _this.restoreEditorDOM = function (scrollPosition) {
      _this.setState({
        contentsKey: _this.state.contentsKey + 1
      }, function () {
        _this.focus(scrollPosition);
      });
    };

    _this.setClipboard = function (clipboard) {
      _this._clipboard = clipboard;
    };

    _this.getClipboard = function () {
      return _this._clipboard;
    };

    _this.update = function (editorState) {
      _this._latestEditorState = editorState;

      _this.props.onChange(editorState);
    };

    _this.onDragEnter = function () {
      _this._dragCount++;
    };

    _this.onDragLeave = function () {
      _this._dragCount--;

      if (_this._dragCount === 0) {
        _this.exitCurrentMode();
      }
    };

    _this._blockSelectEvents = false;
    _this._clipboard = null;
    _this._handler = null;
    _this._dragCount = 0;
    _this._editorKey = props.editorKey || generateRandomKey();
    _this._placeholderAccessibilityID = 'placeholder-' + _this._editorKey;
    _this._latestEditorState = props.editorState;
    _this._latestCommittedEditorState = props.editorState;
    _this._onBeforeInput = _this._buildHandler('onBeforeInput');
    _this._onBlur = _this._buildHandler('onBlur');
    _this._onCharacterData = _this._buildHandler('onCharacterData');
    _this._onCompositionEnd = _this._buildHandler('onCompositionEnd');
    _this._onCompositionStart = _this._buildHandler('onCompositionStart');
    _this._onCopy = _this._buildHandler('onCopy');
    _this._onCut = _this._buildHandler('onCut');
    _this._onDragEnd = _this._buildHandler('onDragEnd');
    _this._onDragOver = _this._buildHandler('onDragOver');
    _this._onDragStart = _this._buildHandler('onDragStart');
    _this._onDrop = _this._buildHandler('onDrop');
    _this._onInput = _this._buildHandler('onInput');
    _this._onFocus = _this._buildHandler('onFocus');
    _this._onKeyDown = _this._buildHandler('onKeyDown');
    _this._onKeyPress = _this._buildHandler('onKeyPress');
    _this._onKeyUp = _this._buildHandler('onKeyUp');
    _this._onMouseDown = _this._buildHandler('onMouseDown');
    _this._onMouseUp = _this._buildHandler('onMouseUp');
    _this._onPaste = _this._buildHandler('onPaste');
    _this._onSelect = _this._buildHandler('onSelect');

    _this.getEditorKey = function () {
      return _this._editorKey;
    }; // See `restoreEditorDOM()`.


    _this.state = {
      contentsKey: 0
    };
    return _this;
  }
  /**
   * Build a method that will pass the event to the specified handler method.
   * This allows us to look up the correct handler function for the current
   * editor mode, if any has been specified.
   */

  /**
   * Define proxies that can route events to the current handler.
   */


  DraftEditor.prototype._buildHandler = function _buildHandler(eventName) {
    var _this2 = this;

    return function (e) {
      if (!_this2.props.readOnly) {
        var method = _this2._handler && _this2._handler[eventName];
        method && method(_this2, e);
      }
    };
  };

  DraftEditor.prototype._showPlaceholder = function _showPlaceholder() {
    return !!this.props.placeholder && !this.props.editorState.isInCompositionMode() && !this.props.editorState.getCurrentContent().hasText();
  };

  DraftEditor.prototype._renderPlaceholder = function _renderPlaceholder() {
    if (this._showPlaceholder()) {
      var placeHolderProps = {
        text: nullthrows(this.props.placeholder),
        editorState: this.props.editorState,
        textAlignment: this.props.textAlignment,
        accessibilityID: this._placeholderAccessibilityID
      };
      return React.createElement(DraftEditorPlaceholder, placeHolderProps);
    }

    return null;
  };

  DraftEditor.prototype.render = function render() {
    var _this3 = this;

    var _props = this.props,
        blockRenderMap = _props.blockRenderMap,
        blockRendererFn = _props.blockRendererFn,
        blockStyleFn = _props.blockStyleFn,
        customStyleFn = _props.customStyleFn,
        customStyleMap = _props.customStyleMap,
        editorState = _props.editorState,
        readOnly = _props.readOnly,
        textAlignment = _props.textAlignment,
        textDirectionality = _props.textDirectionality;
    var rootClass = cx({
      'DraftEditor/root': true,
      'DraftEditor/alignLeft': textAlignment === 'left',
      'DraftEditor/alignRight': textAlignment === 'right',
      'DraftEditor/alignCenter': textAlignment === 'center'
    });
    var contentStyle = {
      outline: 'none',
      // fix parent-draggable Safari bug. #1326
      userSelect: 'text',
      WebkitUserSelect: 'text',
      whiteSpace: 'pre-wrap',
      wordWrap: 'break-word'
    }; // The aria-expanded and aria-haspopup properties should only be rendered
    // for a combobox.

    var ariaRole = this.props.role || 'textbox';
    var ariaExpanded = ariaRole === 'combobox' ? !!this.props.ariaExpanded : null;
    var editorContentsProps = {
      blockRenderMap: blockRenderMap,
      blockRendererFn: blockRendererFn,
      blockStyleFn: blockStyleFn,
      customStyleMap: _extends({}, DefaultDraftInlineStyle, customStyleMap),
      customStyleFn: customStyleFn,
      editorKey: this._editorKey,
      editorState: editorState,
      key: 'contents' + this.state.contentsKey,
      textDirectionality: textDirectionality
    };
    return React.createElement('div', {
      className: rootClass
    }, this._renderPlaceholder(), React.createElement('div', {
      className: cx('DraftEditor/editorContainer'),
      ref: function ref(_ref3) {
        return _this3.editorContainer = _ref3;
      }
    }, React.createElement('div', {
      'aria-activedescendant': readOnly ? null : this.props.ariaActiveDescendantID,
      'aria-autocomplete': readOnly ? null : this.props.ariaAutoComplete,
      'aria-controls': readOnly ? null : this.props.ariaControls,
      'aria-describedby': this.props.ariaDescribedBy || this._placeholderAccessibilityID,
      'aria-expanded': readOnly ? null : ariaExpanded,
      'aria-label': this.props.ariaLabel,
      'aria-labelledby': this.props.ariaLabelledBy,
      'aria-multiline': this.props.ariaMultiline,
      autoCapitalize: this.props.autoCapitalize,
      autoComplete: this.props.autoComplete,
      autoCorrect: this.props.autoCorrect,
      className: cx({
        // Chrome's built-in translation feature mutates the DOM in ways
        // that Draft doesn't expect (ex: adding <font> tags inside
        // DraftEditorLeaf spans) and causes problems. We add notranslate
        // here which makes its autotranslation skip over this subtree.
        notranslate: !readOnly,
        'public/DraftEditor/content': true
      }),
      contentEditable: !readOnly,
      'data-testid': this.props.webDriverTestID,
      onBeforeInput: this._onBeforeInput,
      onBlur: this._onBlur,
      onCompositionEnd: this._onCompositionEnd,
      onCompositionStart: this._onCompositionStart,
      onCopy: this._onCopy,
      onCut: this._onCut,
      onDragEnd: this._onDragEnd,
      onDragEnter: this.onDragEnter,
      onDragLeave: this.onDragLeave,
      onDragOver: this._onDragOver,
      onDragStart: this._onDragStart,
      onDrop: this._onDrop,
      onFocus: this._onFocus,
      onInput: this._onInput,
      onKeyDown: this._onKeyDown,
      onKeyPress: this._onKeyPress,
      onKeyUp: this._onKeyUp,
      onMouseUp: this._onMouseUp,
      onPaste: this._onPaste,
      onSelect: this._onSelect,
      ref: function ref(_ref2) {
        return _this3.editor = _ref2;
      },
      role: readOnly ? null : ariaRole,
      spellCheck: allowSpellCheck && this.props.spellCheck,
      style: contentStyle,
      suppressContentEditableWarning: true,
      tabIndex: this.props.tabIndex
    }, React.createElement(DraftEditorContents, editorContentsProps))));
  };

  DraftEditor.prototype.componentDidMount = function componentDidMount() {
    this.setMode('edit');
    /**
     * IE has a hardcoded "feature" that attempts to convert link text into
     * anchors in contentEditable DOM. This breaks the editor's expectations of
     * the DOM, and control is lost. Disable it to make IE behave.
     * See: http://blogs.msdn.com/b/ieinternals/archive/2010/09/15/
     * ie9-beta-minor-change-list.aspx
     */

    if (isIE) {
      document.execCommand('AutoUrlDetect', false, false);
    }
  };
  /**
   * Prevent selection events from affecting the current editor state. This
   * is mostly intended to defend against IE, which fires off `selectionchange`
   * events regardless of whether the selection is set via the browser or
   * programmatically. We only care about selection events that occur because
   * of browser interaction, not re-renders and forced selections.
   */


  DraftEditor.prototype.componentWillUpdate = function componentWillUpdate(nextProps) {
    this._blockSelectEvents = true;
    this._latestEditorState = nextProps.editorState;
  };

  DraftEditor.prototype.componentDidUpdate = function componentDidUpdate() {
    this._blockSelectEvents = false;
    this._latestCommittedEditorState = this.props.editorState;
  };
  /**
   * Used via `this.focus()`.
   *
   * Force focus back onto the editor node.
   *
   * We attempt to preserve scroll position when focusing. You can also pass
   * a specified scroll position (for cases like `cut` behavior where it should
   * be restored to a known position).
   */

  /**
   * Used via `this.setMode(...)`.
   *
   * Set the behavior mode for the editor component. This switches the current
   * handler module to ensure that DOM events are managed appropriately for
   * the active mode.
   */

  /**
   * Used via `this.restoreEditorDOM()`.
   *
   * Force a complete re-render of the DraftEditorContents based on the current
   * EditorState. This is useful when we know we are going to lose control of
   * the DOM state (cut command, IME) and we want to make sure that
   * reconciliation occurs on a version of the DOM that is synchronized with
   * our EditorState.
   */

  /**
   * Used via `this.setClipboard(...)`.
   *
   * Set the clipboard state for a cut/copy event.
   */

  /**
   * Used via `this.getClipboard()`.
   *
   * Retrieve the clipboard state for a cut/copy event.
   */

  /**
   * Used via `this.update(...)`.
   *
   * Propagate a new `EditorState` object to higher-level components. This is
   * the method by which event handlers inform the `DraftEditor` component of
   * state changes. A component that composes a `DraftEditor` **must** provide
   * an `onChange` prop to receive state updates passed along from this
   * function.
   */

  /**
   * Used in conjunction with `onDragLeave()`, by counting the number of times
   * a dragged element enters and leaves the editor (or any of its children),
   * to determine when the dragged element absolutely leaves the editor.
   */

  /**
   * See `onDragEnter()`.
   */


  return DraftEditor;
}(React.Component);

DraftEditor.defaultProps = {
  blockRenderMap: DefaultDraftBlockRenderMap,
  blockRendererFn: emptyFunction.thatReturnsNull,
  blockStyleFn: emptyFunction.thatReturns(''),
  keyBindingFn: getDefaultKeyBinding,
  readOnly: false,
  spellCheck: false,
  stripPastedStyles: false
};
module.exports = DraftEditor;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yLnJlYWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RWRpdG9yLnJlYWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRHJhZnRFZGl0b3IucmVhY3RcbiAqIEBmb3JtYXRcbiAqIFxuICogQHByZXZlbnRNdW5nZVxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIERlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwID0gcmVxdWlyZSgnLi9EZWZhdWx0RHJhZnRCbG9ja1JlbmRlck1hcCcpO1xudmFyIERlZmF1bHREcmFmdElubGluZVN0eWxlID0gcmVxdWlyZSgnLi9EZWZhdWx0RHJhZnRJbmxpbmVTdHlsZScpO1xudmFyIERyYWZ0RWRpdG9yQ29tcG9zaXRpb25IYW5kbGVyID0gcmVxdWlyZSgnLi9EcmFmdEVkaXRvckNvbXBvc2l0aW9uSGFuZGxlcicpO1xudmFyIERyYWZ0RWRpdG9yQ29udGVudHMgPSByZXF1aXJlKCcuL0RyYWZ0RWRpdG9yQ29udGVudHMucmVhY3QnKTtcbnZhciBEcmFmdEVkaXRvckRyYWdIYW5kbGVyID0gcmVxdWlyZSgnLi9EcmFmdEVkaXRvckRyYWdIYW5kbGVyJyk7XG52YXIgRHJhZnRFZGl0b3JFZGl0SGFuZGxlciA9IHJlcXVpcmUoJy4vRHJhZnRFZGl0b3JFZGl0SGFuZGxlcicpO1xudmFyIERyYWZ0RWRpdG9yUGxhY2Vob2xkZXIgPSByZXF1aXJlKCcuL0RyYWZ0RWRpdG9yUGxhY2Vob2xkZXIucmVhY3QnKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG52YXIgUmVhY3RET00gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcbnZhciBTY3JvbGwgPSByZXF1aXJlKCdmYmpzL2xpYi9TY3JvbGwnKTtcbnZhciBTdHlsZSA9IHJlcXVpcmUoJ2ZianMvbGliL1N0eWxlJyk7XG52YXIgVXNlckFnZW50ID0gcmVxdWlyZSgnZmJqcy9saWIvVXNlckFnZW50Jyk7XG5cbnZhciBjeCA9IHJlcXVpcmUoJ2ZianMvbGliL2N4Jyk7XG52YXIgZW1wdHlGdW5jdGlvbiA9IHJlcXVpcmUoJ2ZianMvbGliL2VtcHR5RnVuY3Rpb24nKTtcbnZhciBnZW5lcmF0ZVJhbmRvbUtleSA9IHJlcXVpcmUoJy4vZ2VuZXJhdGVSYW5kb21LZXknKTtcbnZhciBnZXREZWZhdWx0S2V5QmluZGluZyA9IHJlcXVpcmUoJy4vZ2V0RGVmYXVsdEtleUJpbmRpbmcnKTtcbnZhciBnZXRTY3JvbGxQb3NpdGlvbiA9IHJlcXVpcmUoJ2ZianMvbGliL2dldFNjcm9sbFBvc2l0aW9uJyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcblxudmFyIGlzSUUgPSBVc2VyQWdlbnQuaXNCcm93c2VyKCdJRScpO1xuXG4vLyBJRSBkb2VzIG5vdCBzdXBwb3J0IHRoZSBgaW5wdXRgIGV2ZW50IG9uIGNvbnRlbnRFZGl0YWJsZSwgc28gd2UgY2FuJ3Rcbi8vIG9ic2VydmUgc3BlbGxjaGVjayBiZWhhdmlvci5cbnZhciBhbGxvd1NwZWxsQ2hlY2sgPSAhaXNJRTtcblxuLy8gRGVmaW5lIGEgc2V0IG9mIGhhbmRsZXIgb2JqZWN0cyB0byBjb3JyZXNwb25kIHRvIGVhY2ggcG9zc2libGUgYG1vZGVgXG4vLyBvZiBlZGl0b3IgYmVoYXZpb3IuXG52YXIgaGFuZGxlck1hcCA9IHtcbiAgZWRpdDogRHJhZnRFZGl0b3JFZGl0SGFuZGxlcixcbiAgY29tcG9zaXRlOiBEcmFmdEVkaXRvckNvbXBvc2l0aW9uSGFuZGxlcixcbiAgZHJhZzogRHJhZnRFZGl0b3JEcmFnSGFuZGxlcixcbiAgY3V0OiBudWxsLFxuICByZW5kZXI6IG51bGxcbn07XG5cbi8qKlxuICogYERyYWZ0RWRpdG9yYCBpcyB0aGUgcm9vdCBlZGl0b3IgY29tcG9uZW50LiBJdCBjb21wb3NlcyBhIGBjb250ZW50RWRpdGFibGVgXG4gKiBkaXYsIGFuZCBwcm92aWRlcyBhIHdpZGUgdmFyaWV0eSBvZiB1c2VmdWwgZnVuY3Rpb24gcHJvcHMgZm9yIG1hbmFnaW5nIHRoZVxuICogc3RhdGUgb2YgdGhlIGVkaXRvci4gU2VlIGBEcmFmdEVkaXRvclByb3BzYCBmb3IgZGV0YWlscy5cbiAqL1xudmFyIERyYWZ0RWRpdG9yID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKERyYWZ0RWRpdG9yLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBEcmFmdEVkaXRvcihwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEcmFmdEVkaXRvcik7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwodGhpcywgcHJvcHMpKTtcblxuICAgIF90aGlzLmZvY3VzID0gZnVuY3Rpb24gKHNjcm9sbFBvc2l0aW9uKSB7XG4gICAgICB2YXIgZWRpdG9yU3RhdGUgPSBfdGhpcy5wcm9wcy5lZGl0b3JTdGF0ZTtcblxuICAgICAgdmFyIGFscmVhZHlIYXNGb2N1cyA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLmdldEhhc0ZvY3VzKCk7XG4gICAgICB2YXIgZWRpdG9yTm9kZSA9IFJlYWN0RE9NLmZpbmRET01Ob2RlKF90aGlzLmVkaXRvcik7XG5cbiAgICAgIGlmICghZWRpdG9yTm9kZSkge1xuICAgICAgICAvLyBvbmNlIGluIGEgd2hpbGUgcGVvcGxlIGNhbGwgJ2ZvY3VzJyBpbiBhIHNldFRpbWVvdXQsIGFuZCB0aGUgbm9kZSBoYXNcbiAgICAgICAgLy8gYmVlbiBkZWxldGVkLCBzbyBpdCBjYW4gYmUgbnVsbCBpbiB0aGF0IGNhc2UuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIHNjcm9sbFBhcmVudCA9IFN0eWxlLmdldFNjcm9sbFBhcmVudChlZGl0b3JOb2RlKTtcblxuICAgICAgdmFyIF9yZWYgPSBzY3JvbGxQb3NpdGlvbiB8fCBnZXRTY3JvbGxQb3NpdGlvbihzY3JvbGxQYXJlbnQpLFxuICAgICAgICAgIHggPSBfcmVmLngsXG4gICAgICAgICAgeSA9IF9yZWYueTtcblxuICAgICAgIShlZGl0b3JOb2RlIGluc3RhbmNlb2YgSFRNTEVsZW1lbnQpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2VkaXRvck5vZGUgaXMgbm90IGFuIEhUTUxFbGVtZW50JykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgICAgZWRpdG9yTm9kZS5mb2N1cygpO1xuXG4gICAgICAvLyBSZXN0b3JlIHNjcm9sbCBwb3NpdGlvblxuICAgICAgaWYgKHNjcm9sbFBhcmVudCA9PT0gd2luZG93KSB7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbyh4LCB5KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIFNjcm9sbC5zZXRUb3Aoc2Nyb2xsUGFyZW50LCB5KTtcbiAgICAgIH1cblxuICAgICAgLy8gT24gQ2hyb21lIGFuZCBTYWZhcmksIGNhbGxpbmcgZm9jdXMgb24gY29udGVudGVkaXRhYmxlIGZvY3VzZXMgdGhlXG4gICAgICAvLyBjdXJzb3IgYXQgdGhlIGZpcnN0IGNoYXJhY3Rlci4gVGhpcyBpcyBzb21ldGhpbmcgeW91IGRvbid0IGV4cGVjdCB3aGVuXG4gICAgICAvLyB5b3UncmUgY2xpY2tpbmcgb24gYW4gaW5wdXQgZWxlbWVudCBidXQgbm90IGRpcmVjdGx5IG9uIGEgY2hhcmFjdGVyLlxuICAgICAgLy8gUHV0IHRoZSBjdXJzb3IgYmFjayB3aGVyZSBpdCB3YXMgYmVmb3JlIHRoZSBibHVyLlxuICAgICAgaWYgKCFhbHJlYWR5SGFzRm9jdXMpIHtcbiAgICAgICAgX3RoaXMudXBkYXRlKEVkaXRvclN0YXRlLmZvcmNlU2VsZWN0aW9uKGVkaXRvclN0YXRlLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSkpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5ibHVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGVkaXRvck5vZGUgPSBSZWFjdERPTS5maW5kRE9NTm9kZShfdGhpcy5lZGl0b3IpO1xuICAgICAgIShlZGl0b3JOb2RlIGluc3RhbmNlb2YgSFRNTEVsZW1lbnQpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2VkaXRvck5vZGUgaXMgbm90IGFuIEhUTUxFbGVtZW50JykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgICAgZWRpdG9yTm9kZS5ibHVyKCk7XG4gICAgfTtcblxuICAgIF90aGlzLnNldE1vZGUgPSBmdW5jdGlvbiAobW9kZSkge1xuICAgICAgX3RoaXMuX2hhbmRsZXIgPSBoYW5kbGVyTWFwW21vZGVdO1xuICAgIH07XG5cbiAgICBfdGhpcy5leGl0Q3VycmVudE1vZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5zZXRNb2RlKCdlZGl0Jyk7XG4gICAgfTtcblxuICAgIF90aGlzLnJlc3RvcmVFZGl0b3JET00gPSBmdW5jdGlvbiAoc2Nyb2xsUG9zaXRpb24pIHtcbiAgICAgIF90aGlzLnNldFN0YXRlKHsgY29udGVudHNLZXk6IF90aGlzLnN0YXRlLmNvbnRlbnRzS2V5ICsgMSB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIF90aGlzLmZvY3VzKHNjcm9sbFBvc2l0aW9uKTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5zZXRDbGlwYm9hcmQgPSBmdW5jdGlvbiAoY2xpcGJvYXJkKSB7XG4gICAgICBfdGhpcy5fY2xpcGJvYXJkID0gY2xpcGJvYXJkO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRDbGlwYm9hcmQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMuX2NsaXBib2FyZDtcbiAgICB9O1xuXG4gICAgX3RoaXMudXBkYXRlID0gZnVuY3Rpb24gKGVkaXRvclN0YXRlKSB7XG4gICAgICBfdGhpcy5fbGF0ZXN0RWRpdG9yU3RhdGUgPSBlZGl0b3JTdGF0ZTtcbiAgICAgIF90aGlzLnByb3BzLm9uQ2hhbmdlKGVkaXRvclN0YXRlKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25EcmFnRW50ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5fZHJhZ0NvdW50Kys7XG4gICAgfTtcblxuICAgIF90aGlzLm9uRHJhZ0xlYXZlID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuX2RyYWdDb3VudC0tO1xuICAgICAgaWYgKF90aGlzLl9kcmFnQ291bnQgPT09IDApIHtcbiAgICAgICAgX3RoaXMuZXhpdEN1cnJlbnRNb2RlKCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLl9ibG9ja1NlbGVjdEV2ZW50cyA9IGZhbHNlO1xuICAgIF90aGlzLl9jbGlwYm9hcmQgPSBudWxsO1xuICAgIF90aGlzLl9oYW5kbGVyID0gbnVsbDtcbiAgICBfdGhpcy5fZHJhZ0NvdW50ID0gMDtcbiAgICBfdGhpcy5fZWRpdG9yS2V5ID0gcHJvcHMuZWRpdG9yS2V5IHx8IGdlbmVyYXRlUmFuZG9tS2V5KCk7XG4gICAgX3RoaXMuX3BsYWNlaG9sZGVyQWNjZXNzaWJpbGl0eUlEID0gJ3BsYWNlaG9sZGVyLScgKyBfdGhpcy5fZWRpdG9yS2V5O1xuICAgIF90aGlzLl9sYXRlc3RFZGl0b3JTdGF0ZSA9IHByb3BzLmVkaXRvclN0YXRlO1xuICAgIF90aGlzLl9sYXRlc3RDb21taXR0ZWRFZGl0b3JTdGF0ZSA9IHByb3BzLmVkaXRvclN0YXRlO1xuXG4gICAgX3RoaXMuX29uQmVmb3JlSW5wdXQgPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvbkJlZm9yZUlucHV0Jyk7XG4gICAgX3RoaXMuX29uQmx1ciA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uQmx1cicpO1xuICAgIF90aGlzLl9vbkNoYXJhY3RlckRhdGEgPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvbkNoYXJhY3RlckRhdGEnKTtcbiAgICBfdGhpcy5fb25Db21wb3NpdGlvbkVuZCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uQ29tcG9zaXRpb25FbmQnKTtcbiAgICBfdGhpcy5fb25Db21wb3NpdGlvblN0YXJ0ID0gX3RoaXMuX2J1aWxkSGFuZGxlcignb25Db21wb3NpdGlvblN0YXJ0Jyk7XG4gICAgX3RoaXMuX29uQ29weSA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uQ29weScpO1xuICAgIF90aGlzLl9vbkN1dCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uQ3V0Jyk7XG4gICAgX3RoaXMuX29uRHJhZ0VuZCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uRHJhZ0VuZCcpO1xuICAgIF90aGlzLl9vbkRyYWdPdmVyID0gX3RoaXMuX2J1aWxkSGFuZGxlcignb25EcmFnT3ZlcicpO1xuICAgIF90aGlzLl9vbkRyYWdTdGFydCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uRHJhZ1N0YXJ0Jyk7XG4gICAgX3RoaXMuX29uRHJvcCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uRHJvcCcpO1xuICAgIF90aGlzLl9vbklucHV0ID0gX3RoaXMuX2J1aWxkSGFuZGxlcignb25JbnB1dCcpO1xuICAgIF90aGlzLl9vbkZvY3VzID0gX3RoaXMuX2J1aWxkSGFuZGxlcignb25Gb2N1cycpO1xuICAgIF90aGlzLl9vbktleURvd24gPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvbktleURvd24nKTtcbiAgICBfdGhpcy5fb25LZXlQcmVzcyA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uS2V5UHJlc3MnKTtcbiAgICBfdGhpcy5fb25LZXlVcCA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uS2V5VXAnKTtcbiAgICBfdGhpcy5fb25Nb3VzZURvd24gPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvbk1vdXNlRG93bicpO1xuICAgIF90aGlzLl9vbk1vdXNlVXAgPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvbk1vdXNlVXAnKTtcbiAgICBfdGhpcy5fb25QYXN0ZSA9IF90aGlzLl9idWlsZEhhbmRsZXIoJ29uUGFzdGUnKTtcbiAgICBfdGhpcy5fb25TZWxlY3QgPSBfdGhpcy5fYnVpbGRIYW5kbGVyKCdvblNlbGVjdCcpO1xuXG4gICAgX3RoaXMuZ2V0RWRpdG9yS2V5ID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIF90aGlzLl9lZGl0b3JLZXk7XG4gICAgfTtcblxuICAgIC8vIFNlZSBgcmVzdG9yZUVkaXRvckRPTSgpYC5cbiAgICBfdGhpcy5zdGF0ZSA9IHsgY29udGVudHNLZXk6IDAgfTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICAvKipcbiAgICogQnVpbGQgYSBtZXRob2QgdGhhdCB3aWxsIHBhc3MgdGhlIGV2ZW50IHRvIHRoZSBzcGVjaWZpZWQgaGFuZGxlciBtZXRob2QuXG4gICAqIFRoaXMgYWxsb3dzIHVzIHRvIGxvb2sgdXAgdGhlIGNvcnJlY3QgaGFuZGxlciBmdW5jdGlvbiBmb3IgdGhlIGN1cnJlbnRcbiAgICogZWRpdG9yIG1vZGUsIGlmIGFueSBoYXMgYmVlbiBzcGVjaWZpZWQuXG4gICAqL1xuXG5cbiAgLyoqXG4gICAqIERlZmluZSBwcm94aWVzIHRoYXQgY2FuIHJvdXRlIGV2ZW50cyB0byB0aGUgY3VycmVudCBoYW5kbGVyLlxuICAgKi9cblxuXG4gIERyYWZ0RWRpdG9yLnByb3RvdHlwZS5fYnVpbGRIYW5kbGVyID0gZnVuY3Rpb24gX2J1aWxkSGFuZGxlcihldmVudE5hbWUpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHJldHVybiBmdW5jdGlvbiAoZSkge1xuICAgICAgaWYgKCFfdGhpczIucHJvcHMucmVhZE9ubHkpIHtcbiAgICAgICAgdmFyIG1ldGhvZCA9IF90aGlzMi5faGFuZGxlciAmJiBfdGhpczIuX2hhbmRsZXJbZXZlbnROYW1lXTtcbiAgICAgICAgbWV0aG9kICYmIG1ldGhvZChfdGhpczIsIGUpO1xuICAgICAgfVxuICAgIH07XG4gIH07XG5cbiAgRHJhZnRFZGl0b3IucHJvdG90eXBlLl9zaG93UGxhY2Vob2xkZXIgPSBmdW5jdGlvbiBfc2hvd1BsYWNlaG9sZGVyKCkge1xuICAgIHJldHVybiAhIXRoaXMucHJvcHMucGxhY2Vob2xkZXIgJiYgIXRoaXMucHJvcHMuZWRpdG9yU3RhdGUuaXNJbkNvbXBvc2l0aW9uTW9kZSgpICYmICF0aGlzLnByb3BzLmVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCkuaGFzVGV4dCgpO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yLnByb3RvdHlwZS5fcmVuZGVyUGxhY2Vob2xkZXIgPSBmdW5jdGlvbiBfcmVuZGVyUGxhY2Vob2xkZXIoKSB7XG4gICAgaWYgKHRoaXMuX3Nob3dQbGFjZWhvbGRlcigpKSB7XG4gICAgICB2YXIgcGxhY2VIb2xkZXJQcm9wcyA9IHtcbiAgICAgICAgdGV4dDogbnVsbHRocm93cyh0aGlzLnByb3BzLnBsYWNlaG9sZGVyKSxcbiAgICAgICAgZWRpdG9yU3RhdGU6IHRoaXMucHJvcHMuZWRpdG9yU3RhdGUsXG4gICAgICAgIHRleHRBbGlnbm1lbnQ6IHRoaXMucHJvcHMudGV4dEFsaWdubWVudCxcbiAgICAgICAgYWNjZXNzaWJpbGl0eUlEOiB0aGlzLl9wbGFjZWhvbGRlckFjY2Vzc2liaWxpdHlJRFxuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoRHJhZnRFZGl0b3JQbGFjZWhvbGRlciwgcGxhY2VIb2xkZXJQcm9wcyk7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9O1xuXG4gIERyYWZ0RWRpdG9yLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF90aGlzMyA9IHRoaXM7XG5cbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgYmxvY2tSZW5kZXJNYXAgPSBfcHJvcHMuYmxvY2tSZW5kZXJNYXAsXG4gICAgICAgIGJsb2NrUmVuZGVyZXJGbiA9IF9wcm9wcy5ibG9ja1JlbmRlcmVyRm4sXG4gICAgICAgIGJsb2NrU3R5bGVGbiA9IF9wcm9wcy5ibG9ja1N0eWxlRm4sXG4gICAgICAgIGN1c3RvbVN0eWxlRm4gPSBfcHJvcHMuY3VzdG9tU3R5bGVGbixcbiAgICAgICAgY3VzdG9tU3R5bGVNYXAgPSBfcHJvcHMuY3VzdG9tU3R5bGVNYXAsXG4gICAgICAgIGVkaXRvclN0YXRlID0gX3Byb3BzLmVkaXRvclN0YXRlLFxuICAgICAgICByZWFkT25seSA9IF9wcm9wcy5yZWFkT25seSxcbiAgICAgICAgdGV4dEFsaWdubWVudCA9IF9wcm9wcy50ZXh0QWxpZ25tZW50LFxuICAgICAgICB0ZXh0RGlyZWN0aW9uYWxpdHkgPSBfcHJvcHMudGV4dERpcmVjdGlvbmFsaXR5O1xuXG5cbiAgICB2YXIgcm9vdENsYXNzID0gY3goe1xuICAgICAgJ0RyYWZ0RWRpdG9yL3Jvb3QnOiB0cnVlLFxuICAgICAgJ0RyYWZ0RWRpdG9yL2FsaWduTGVmdCc6IHRleHRBbGlnbm1lbnQgPT09ICdsZWZ0JyxcbiAgICAgICdEcmFmdEVkaXRvci9hbGlnblJpZ2h0JzogdGV4dEFsaWdubWVudCA9PT0gJ3JpZ2h0JyxcbiAgICAgICdEcmFmdEVkaXRvci9hbGlnbkNlbnRlcic6IHRleHRBbGlnbm1lbnQgPT09ICdjZW50ZXInXG4gICAgfSk7XG5cbiAgICB2YXIgY29udGVudFN0eWxlID0ge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgLy8gZml4IHBhcmVudC1kcmFnZ2FibGUgU2FmYXJpIGJ1Zy4gIzEzMjZcbiAgICAgIHVzZXJTZWxlY3Q6ICd0ZXh0JyxcbiAgICAgIFdlYmtpdFVzZXJTZWxlY3Q6ICd0ZXh0JyxcbiAgICAgIHdoaXRlU3BhY2U6ICdwcmUtd3JhcCcsXG4gICAgICB3b3JkV3JhcDogJ2JyZWFrLXdvcmQnXG4gICAgfTtcblxuICAgIC8vIFRoZSBhcmlhLWV4cGFuZGVkIGFuZCBhcmlhLWhhc3BvcHVwIHByb3BlcnRpZXMgc2hvdWxkIG9ubHkgYmUgcmVuZGVyZWRcbiAgICAvLyBmb3IgYSBjb21ib2JveC5cbiAgICB2YXIgYXJpYVJvbGUgPSB0aGlzLnByb3BzLnJvbGUgfHwgJ3RleHRib3gnO1xuICAgIHZhciBhcmlhRXhwYW5kZWQgPSBhcmlhUm9sZSA9PT0gJ2NvbWJvYm94JyA/ICEhdGhpcy5wcm9wcy5hcmlhRXhwYW5kZWQgOiBudWxsO1xuXG4gICAgdmFyIGVkaXRvckNvbnRlbnRzUHJvcHMgPSB7XG4gICAgICBibG9ja1JlbmRlck1hcDogYmxvY2tSZW5kZXJNYXAsXG4gICAgICBibG9ja1JlbmRlcmVyRm46IGJsb2NrUmVuZGVyZXJGbixcbiAgICAgIGJsb2NrU3R5bGVGbjogYmxvY2tTdHlsZUZuLFxuICAgICAgY3VzdG9tU3R5bGVNYXA6IF9leHRlbmRzKHt9LCBEZWZhdWx0RHJhZnRJbmxpbmVTdHlsZSwgY3VzdG9tU3R5bGVNYXApLFxuICAgICAgY3VzdG9tU3R5bGVGbjogY3VzdG9tU3R5bGVGbixcbiAgICAgIGVkaXRvcktleTogdGhpcy5fZWRpdG9yS2V5LFxuICAgICAgZWRpdG9yU3RhdGU6IGVkaXRvclN0YXRlLFxuICAgICAga2V5OiAnY29udGVudHMnICsgdGhpcy5zdGF0ZS5jb250ZW50c0tleSxcbiAgICAgIHRleHREaXJlY3Rpb25hbGl0eTogdGV4dERpcmVjdGlvbmFsaXR5XG4gICAgfTtcblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ2RpdicsXG4gICAgICB7IGNsYXNzTmFtZTogcm9vdENsYXNzIH0sXG4gICAgICB0aGlzLl9yZW5kZXJQbGFjZWhvbGRlcigpLFxuICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgIHtcbiAgICAgICAgICBjbGFzc05hbWU6IGN4KCdEcmFmdEVkaXRvci9lZGl0b3JDb250YWluZXInKSxcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihfcmVmMykge1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzMy5lZGl0b3JDb250YWluZXIgPSBfcmVmMztcbiAgICAgICAgICB9IH0sXG4gICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAge1xuICAgICAgICAgICAgJ2FyaWEtYWN0aXZlZGVzY2VuZGFudCc6IHJlYWRPbmx5ID8gbnVsbCA6IHRoaXMucHJvcHMuYXJpYUFjdGl2ZURlc2NlbmRhbnRJRCxcbiAgICAgICAgICAgICdhcmlhLWF1dG9jb21wbGV0ZSc6IHJlYWRPbmx5ID8gbnVsbCA6IHRoaXMucHJvcHMuYXJpYUF1dG9Db21wbGV0ZSxcbiAgICAgICAgICAgICdhcmlhLWNvbnRyb2xzJzogcmVhZE9ubHkgPyBudWxsIDogdGhpcy5wcm9wcy5hcmlhQ29udHJvbHMsXG4gICAgICAgICAgICAnYXJpYS1kZXNjcmliZWRieSc6IHRoaXMucHJvcHMuYXJpYURlc2NyaWJlZEJ5IHx8IHRoaXMuX3BsYWNlaG9sZGVyQWNjZXNzaWJpbGl0eUlELFxuICAgICAgICAgICAgJ2FyaWEtZXhwYW5kZWQnOiByZWFkT25seSA/IG51bGwgOiBhcmlhRXhwYW5kZWQsXG4gICAgICAgICAgICAnYXJpYS1sYWJlbCc6IHRoaXMucHJvcHMuYXJpYUxhYmVsLFxuICAgICAgICAgICAgJ2FyaWEtbGFiZWxsZWRieSc6IHRoaXMucHJvcHMuYXJpYUxhYmVsbGVkQnksXG4gICAgICAgICAgICAnYXJpYS1tdWx0aWxpbmUnOiB0aGlzLnByb3BzLmFyaWFNdWx0aWxpbmUsXG4gICAgICAgICAgICBhdXRvQ2FwaXRhbGl6ZTogdGhpcy5wcm9wcy5hdXRvQ2FwaXRhbGl6ZSxcbiAgICAgICAgICAgIGF1dG9Db21wbGV0ZTogdGhpcy5wcm9wcy5hdXRvQ29tcGxldGUsXG4gICAgICAgICAgICBhdXRvQ29ycmVjdDogdGhpcy5wcm9wcy5hdXRvQ29ycmVjdCxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogY3goe1xuICAgICAgICAgICAgICAvLyBDaHJvbWUncyBidWlsdC1pbiB0cmFuc2xhdGlvbiBmZWF0dXJlIG11dGF0ZXMgdGhlIERPTSBpbiB3YXlzXG4gICAgICAgICAgICAgIC8vIHRoYXQgRHJhZnQgZG9lc24ndCBleHBlY3QgKGV4OiBhZGRpbmcgPGZvbnQ+IHRhZ3MgaW5zaWRlXG4gICAgICAgICAgICAgIC8vIERyYWZ0RWRpdG9yTGVhZiBzcGFucykgYW5kIGNhdXNlcyBwcm9ibGVtcy4gV2UgYWRkIG5vdHJhbnNsYXRlXG4gICAgICAgICAgICAgIC8vIGhlcmUgd2hpY2ggbWFrZXMgaXRzIGF1dG90cmFuc2xhdGlvbiBza2lwIG92ZXIgdGhpcyBzdWJ0cmVlLlxuICAgICAgICAgICAgICBub3RyYW5zbGF0ZTogIXJlYWRPbmx5LFxuICAgICAgICAgICAgICAncHVibGljL0RyYWZ0RWRpdG9yL2NvbnRlbnQnOiB0cnVlXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGNvbnRlbnRFZGl0YWJsZTogIXJlYWRPbmx5LFxuICAgICAgICAgICAgJ2RhdGEtdGVzdGlkJzogdGhpcy5wcm9wcy53ZWJEcml2ZXJUZXN0SUQsXG4gICAgICAgICAgICBvbkJlZm9yZUlucHV0OiB0aGlzLl9vbkJlZm9yZUlucHV0LFxuICAgICAgICAgICAgb25CbHVyOiB0aGlzLl9vbkJsdXIsXG4gICAgICAgICAgICBvbkNvbXBvc2l0aW9uRW5kOiB0aGlzLl9vbkNvbXBvc2l0aW9uRW5kLFxuICAgICAgICAgICAgb25Db21wb3NpdGlvblN0YXJ0OiB0aGlzLl9vbkNvbXBvc2l0aW9uU3RhcnQsXG4gICAgICAgICAgICBvbkNvcHk6IHRoaXMuX29uQ29weSxcbiAgICAgICAgICAgIG9uQ3V0OiB0aGlzLl9vbkN1dCxcbiAgICAgICAgICAgIG9uRHJhZ0VuZDogdGhpcy5fb25EcmFnRW5kLFxuICAgICAgICAgICAgb25EcmFnRW50ZXI6IHRoaXMub25EcmFnRW50ZXIsXG4gICAgICAgICAgICBvbkRyYWdMZWF2ZTogdGhpcy5vbkRyYWdMZWF2ZSxcbiAgICAgICAgICAgIG9uRHJhZ092ZXI6IHRoaXMuX29uRHJhZ092ZXIsXG4gICAgICAgICAgICBvbkRyYWdTdGFydDogdGhpcy5fb25EcmFnU3RhcnQsXG4gICAgICAgICAgICBvbkRyb3A6IHRoaXMuX29uRHJvcCxcbiAgICAgICAgICAgIG9uRm9jdXM6IHRoaXMuX29uRm9jdXMsXG4gICAgICAgICAgICBvbklucHV0OiB0aGlzLl9vbklucHV0LFxuICAgICAgICAgICAgb25LZXlEb3duOiB0aGlzLl9vbktleURvd24sXG4gICAgICAgICAgICBvbktleVByZXNzOiB0aGlzLl9vbktleVByZXNzLFxuICAgICAgICAgICAgb25LZXlVcDogdGhpcy5fb25LZXlVcCxcbiAgICAgICAgICAgIG9uTW91c2VVcDogdGhpcy5fb25Nb3VzZVVwLFxuICAgICAgICAgICAgb25QYXN0ZTogdGhpcy5fb25QYXN0ZSxcbiAgICAgICAgICAgIG9uU2VsZWN0OiB0aGlzLl9vblNlbGVjdCxcbiAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKF9yZWYyKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpczMuZWRpdG9yID0gX3JlZjI7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcm9sZTogcmVhZE9ubHkgPyBudWxsIDogYXJpYVJvbGUsXG4gICAgICAgICAgICBzcGVsbENoZWNrOiBhbGxvd1NwZWxsQ2hlY2sgJiYgdGhpcy5wcm9wcy5zcGVsbENoZWNrLFxuICAgICAgICAgICAgc3R5bGU6IGNvbnRlbnRTdHlsZSxcbiAgICAgICAgICAgIHN1cHByZXNzQ29udGVudEVkaXRhYmxlV2FybmluZzogdHJ1ZSxcbiAgICAgICAgICAgIHRhYkluZGV4OiB0aGlzLnByb3BzLnRhYkluZGV4IH0sXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChEcmFmdEVkaXRvckNvbnRlbnRzLCBlZGl0b3JDb250ZW50c1Byb3BzKVxuICAgICAgICApXG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICBEcmFmdEVkaXRvci5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnNldE1vZGUoJ2VkaXQnKTtcblxuICAgIC8qKlxuICAgICAqIElFIGhhcyBhIGhhcmRjb2RlZCBcImZlYXR1cmVcIiB0aGF0IGF0dGVtcHRzIHRvIGNvbnZlcnQgbGluayB0ZXh0IGludG9cbiAgICAgKiBhbmNob3JzIGluIGNvbnRlbnRFZGl0YWJsZSBET00uIFRoaXMgYnJlYWtzIHRoZSBlZGl0b3IncyBleHBlY3RhdGlvbnMgb2ZcbiAgICAgKiB0aGUgRE9NLCBhbmQgY29udHJvbCBpcyBsb3N0LiBEaXNhYmxlIGl0IHRvIG1ha2UgSUUgYmVoYXZlLlxuICAgICAqIFNlZTogaHR0cDovL2Jsb2dzLm1zZG4uY29tL2IvaWVpbnRlcm5hbHMvYXJjaGl2ZS8yMDEwLzA5LzE1L1xuICAgICAqIGllOS1iZXRhLW1pbm9yLWNoYW5nZS1saXN0LmFzcHhcbiAgICAgKi9cbiAgICBpZiAoaXNJRSkge1xuICAgICAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ0F1dG9VcmxEZXRlY3QnLCBmYWxzZSwgZmFsc2UpO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogUHJldmVudCBzZWxlY3Rpb24gZXZlbnRzIGZyb20gYWZmZWN0aW5nIHRoZSBjdXJyZW50IGVkaXRvciBzdGF0ZS4gVGhpc1xuICAgKiBpcyBtb3N0bHkgaW50ZW5kZWQgdG8gZGVmZW5kIGFnYWluc3QgSUUsIHdoaWNoIGZpcmVzIG9mZiBgc2VsZWN0aW9uY2hhbmdlYFxuICAgKiBldmVudHMgcmVnYXJkbGVzcyBvZiB3aGV0aGVyIHRoZSBzZWxlY3Rpb24gaXMgc2V0IHZpYSB0aGUgYnJvd3NlciBvclxuICAgKiBwcm9ncmFtbWF0aWNhbGx5LiBXZSBvbmx5IGNhcmUgYWJvdXQgc2VsZWN0aW9uIGV2ZW50cyB0aGF0IG9jY3VyIGJlY2F1c2VcbiAgICogb2YgYnJvd3NlciBpbnRlcmFjdGlvbiwgbm90IHJlLXJlbmRlcnMgYW5kIGZvcmNlZCBzZWxlY3Rpb25zLlxuICAgKi9cblxuXG4gIERyYWZ0RWRpdG9yLnByb3RvdHlwZS5jb21wb25lbnRXaWxsVXBkYXRlID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICB0aGlzLl9ibG9ja1NlbGVjdEV2ZW50cyA9IHRydWU7XG4gICAgdGhpcy5fbGF0ZXN0RWRpdG9yU3RhdGUgPSBuZXh0UHJvcHMuZWRpdG9yU3RhdGU7XG4gIH07XG5cbiAgRHJhZnRFZGl0b3IucHJvdG90eXBlLmNvbXBvbmVudERpZFVwZGF0ZSA9IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICB0aGlzLl9ibG9ja1NlbGVjdEV2ZW50cyA9IGZhbHNlO1xuICAgIHRoaXMuX2xhdGVzdENvbW1pdHRlZEVkaXRvclN0YXRlID0gdGhpcy5wcm9wcy5lZGl0b3JTdGF0ZTtcbiAgfTtcblxuICAvKipcbiAgICogVXNlZCB2aWEgYHRoaXMuZm9jdXMoKWAuXG4gICAqXG4gICAqIEZvcmNlIGZvY3VzIGJhY2sgb250byB0aGUgZWRpdG9yIG5vZGUuXG4gICAqXG4gICAqIFdlIGF0dGVtcHQgdG8gcHJlc2VydmUgc2Nyb2xsIHBvc2l0aW9uIHdoZW4gZm9jdXNpbmcuIFlvdSBjYW4gYWxzbyBwYXNzXG4gICAqIGEgc3BlY2lmaWVkIHNjcm9sbCBwb3NpdGlvbiAoZm9yIGNhc2VzIGxpa2UgYGN1dGAgYmVoYXZpb3Igd2hlcmUgaXQgc2hvdWxkXG4gICAqIGJlIHJlc3RvcmVkIHRvIGEga25vd24gcG9zaXRpb24pLlxuICAgKi9cblxuXG4gIC8qKlxuICAgKiBVc2VkIHZpYSBgdGhpcy5zZXRNb2RlKC4uLilgLlxuICAgKlxuICAgKiBTZXQgdGhlIGJlaGF2aW9yIG1vZGUgZm9yIHRoZSBlZGl0b3IgY29tcG9uZW50LiBUaGlzIHN3aXRjaGVzIHRoZSBjdXJyZW50XG4gICAqIGhhbmRsZXIgbW9kdWxlIHRvIGVuc3VyZSB0aGF0IERPTSBldmVudHMgYXJlIG1hbmFnZWQgYXBwcm9wcmlhdGVseSBmb3JcbiAgICogdGhlIGFjdGl2ZSBtb2RlLlxuICAgKi9cblxuXG4gIC8qKlxuICAgKiBVc2VkIHZpYSBgdGhpcy5yZXN0b3JlRWRpdG9yRE9NKClgLlxuICAgKlxuICAgKiBGb3JjZSBhIGNvbXBsZXRlIHJlLXJlbmRlciBvZiB0aGUgRHJhZnRFZGl0b3JDb250ZW50cyBiYXNlZCBvbiB0aGUgY3VycmVudFxuICAgKiBFZGl0b3JTdGF0ZS4gVGhpcyBpcyB1c2VmdWwgd2hlbiB3ZSBrbm93IHdlIGFyZSBnb2luZyB0byBsb3NlIGNvbnRyb2wgb2ZcbiAgICogdGhlIERPTSBzdGF0ZSAoY3V0IGNvbW1hbmQsIElNRSkgYW5kIHdlIHdhbnQgdG8gbWFrZSBzdXJlIHRoYXRcbiAgICogcmVjb25jaWxpYXRpb24gb2NjdXJzIG9uIGEgdmVyc2lvbiBvZiB0aGUgRE9NIHRoYXQgaXMgc3luY2hyb25pemVkIHdpdGhcbiAgICogb3VyIEVkaXRvclN0YXRlLlxuICAgKi9cblxuXG4gIC8qKlxuICAgKiBVc2VkIHZpYSBgdGhpcy5zZXRDbGlwYm9hcmQoLi4uKWAuXG4gICAqXG4gICAqIFNldCB0aGUgY2xpcGJvYXJkIHN0YXRlIGZvciBhIGN1dC9jb3B5IGV2ZW50LlxuICAgKi9cblxuXG4gIC8qKlxuICAgKiBVc2VkIHZpYSBgdGhpcy5nZXRDbGlwYm9hcmQoKWAuXG4gICAqXG4gICAqIFJldHJpZXZlIHRoZSBjbGlwYm9hcmQgc3RhdGUgZm9yIGEgY3V0L2NvcHkgZXZlbnQuXG4gICAqL1xuXG5cbiAgLyoqXG4gICAqIFVzZWQgdmlhIGB0aGlzLnVwZGF0ZSguLi4pYC5cbiAgICpcbiAgICogUHJvcGFnYXRlIGEgbmV3IGBFZGl0b3JTdGF0ZWAgb2JqZWN0IHRvIGhpZ2hlci1sZXZlbCBjb21wb25lbnRzLiBUaGlzIGlzXG4gICAqIHRoZSBtZXRob2QgYnkgd2hpY2ggZXZlbnQgaGFuZGxlcnMgaW5mb3JtIHRoZSBgRHJhZnRFZGl0b3JgIGNvbXBvbmVudCBvZlxuICAgKiBzdGF0ZSBjaGFuZ2VzLiBBIGNvbXBvbmVudCB0aGF0IGNvbXBvc2VzIGEgYERyYWZ0RWRpdG9yYCAqKm11c3QqKiBwcm92aWRlXG4gICAqIGFuIGBvbkNoYW5nZWAgcHJvcCB0byByZWNlaXZlIHN0YXRlIHVwZGF0ZXMgcGFzc2VkIGFsb25nIGZyb20gdGhpc1xuICAgKiBmdW5jdGlvbi5cbiAgICovXG5cblxuICAvKipcbiAgICogVXNlZCBpbiBjb25qdW5jdGlvbiB3aXRoIGBvbkRyYWdMZWF2ZSgpYCwgYnkgY291bnRpbmcgdGhlIG51bWJlciBvZiB0aW1lc1xuICAgKiBhIGRyYWdnZWQgZWxlbWVudCBlbnRlcnMgYW5kIGxlYXZlcyB0aGUgZWRpdG9yIChvciBhbnkgb2YgaXRzIGNoaWxkcmVuKSxcbiAgICogdG8gZGV0ZXJtaW5lIHdoZW4gdGhlIGRyYWdnZWQgZWxlbWVudCBhYnNvbHV0ZWx5IGxlYXZlcyB0aGUgZWRpdG9yLlxuICAgKi9cblxuXG4gIC8qKlxuICAgKiBTZWUgYG9uRHJhZ0VudGVyKClgLlxuICAgKi9cblxuXG4gIHJldHVybiBEcmFmdEVkaXRvcjtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuRHJhZnRFZGl0b3IuZGVmYXVsdFByb3BzID0ge1xuICBibG9ja1JlbmRlck1hcDogRGVmYXVsdERyYWZ0QmxvY2tSZW5kZXJNYXAsXG4gIGJsb2NrUmVuZGVyZXJGbjogZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJuc051bGwsXG4gIGJsb2NrU3R5bGVGbjogZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJucygnJyksXG4gIGtleUJpbmRpbmdGbjogZ2V0RGVmYXVsdEtleUJpbmRpbmcsXG4gIHJlYWRPbmx5OiBmYWxzZSxcbiAgc3BlbGxDaGVjazogZmFsc2UsXG4gIHN0cmlwUGFzdGVkU3R5bGVzOiBmYWxzZVxufTtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RWRpdG9yOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQU9BOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFZQTtBQUVBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqREE7QUFzREE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7OztBQVdBOzs7Ozs7OztBQVNBOzs7Ozs7Ozs7O0FBV0E7Ozs7OztBQU9BOzs7Ozs7QUFPQTs7Ozs7Ozs7OztBQVdBOzs7Ozs7QUFPQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFXQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEditor.react.js
