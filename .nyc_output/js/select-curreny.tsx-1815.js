__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/reinsurance/treaty-definition/common/select-curreny.tsx";






var SelectCurrency =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(SelectCurrency, _ModelWidget);

  function SelectCurrency(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, SelectCurrency);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(SelectCurrency).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(SelectCurrency, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this = this;

      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/currencies").then(function (res) {
        _this.setState({
          currenciesData: (res.body || {}).respData || [],
          copyCurrenciesData: (res.body || {}).respData || []
        });
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(SelectCurrency.prototype), "initState", this).call(this), {
        currenciesData: [],
        copyCurrenciesData: []
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          propName = _this$props.propName,
          label = _this$props.label,
          _onChange = _this$props.onChange,
          layoutCol = _this$props.layoutCol,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["model", "form", "propName", "label", "onChange", "layoutCol"]);

      var _this$state = this.state,
          currenciesData = _this$state.currenciesData,
          copyCurrenciesData = _this$state.copyCurrenciesData;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NSelect"], Object.assign({
        size: "large",
        form: form,
        showSearch: true,
        layoutCol: layoutCol,
        onChange: function onChange() {
          _onChange && _onChange();
        },
        label: label || _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Currency").thai("Currency").getMessage(),
        required: true,
        model: model,
        options: currenciesData.map(function (item, index) {
          return {
            id: item.currencyCode,
            text: item.currencyName
          };
        }),
        filterOption: false,
        onSearch: function onSearch(value) {
          var newCurrenciesData = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.cloneDeep(copyCurrenciesData);

          var filter = newCurrenciesData.filter(function (item) {
            return item.currencyCode.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) >= 0 || item.currencyName.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) >= 0;
          });

          _this2.setState({
            currenciesData: filter
          });
        },
        propName: propName
      }, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      })));
    }
  }]);

  return SelectCurrency;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (SelectCurrency);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcmVpbnN1cmFuY2UvdHJlYXR5LWRlZmluaXRpb24vY29tbW9uL3NlbGVjdC1jdXJyZW55LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3JlaW5zdXJhbmNlL3RyZWF0eS1kZWZpbml0aW9uL2NvbW1vbi9zZWxlY3QtY3VycmVueS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFBsYW5Qcm9wcyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvcGxhblwiO1xuaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IEFqYXgsIExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5TZWxlY3QgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcblxudHlwZSBJUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgcHJvcE5hbWU6IGFueTtcbiAgbGFiZWw/OiBhbnk7XG4gIG9uQ2hhbmdlPzogYW55O1xuICBsYXlvdXRDb2w/OiBhbnk7XG59ICYgTW9kZWxXaWRnZXRQcm9wc1xudHlwZSBJU3RhdGUgPSB7XG4gIGN1cnJlbmNpZXNEYXRhOiBhbnlbXTtcbiAgY29weUN1cnJlbmNpZXNEYXRhOiBhbnlbXTtcbn1cbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBOZXdWaWV3Q29tcG9uZW50cyA9IHtcbiAgU2VsZWN0Q3VycmVuY3k6IFN0eWxlZERJVixcbn1cblxuY2xhc3MgU2VsZWN0Q3VycmVuY3k8UCBleHRlbmRzIElQcm9wcywgUyBleHRlbmRzIElTdGF0ZSwgQyBleHRlbmRzIE5ld1ZpZXdDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IFBsYW5Qcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIEFqYXguZ2V0KFwiL2N1cnJlbmNpZXNcIikudGhlbigocmVzOiBhbnkpID0+IHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBjdXJyZW5jaWVzRGF0YTogKChyZXMuYm9keSB8fCB7fSkucmVzcERhdGEgfHwgW10pLFxuICAgICAgICBjb3B5Q3VycmVuY2llc0RhdGE6ICgocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhIHx8IFtdKSxcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7fSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgY3VycmVuY2llc0RhdGE6IFtdLFxuICAgICAgY29weUN1cnJlbmNpZXNEYXRhOiBbXSxcbiAgICB9KSBhcyBTO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgbW9kZWwsIGZvcm0sIHByb3BOYW1lLCBsYWJlbCwgb25DaGFuZ2UsIGxheW91dENvbCwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGN1cnJlbmNpZXNEYXRhLCBjb3B5Q3VycmVuY2llc0RhdGEgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxOU2VsZWN0IHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgc2hvd1NlYXJjaFxuICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICBvbkNoYW5nZSAmJiBvbkNoYW5nZSgpO1xuICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICBsYWJlbD17bGFiZWwgfHwgTGFuZ3VhZ2UuZW4oXCJDdXJyZW5jeVwiKS50aGFpKFwiQ3VycmVuY3lcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICBvcHRpb25zPXtcbiAgICAgICAgICAgICAgICAgICBjdXJyZW5jaWVzRGF0YS5tYXAoKGl0ZW06IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4geyBpZDogaXRlbS5jdXJyZW5jeUNvZGUsIHRleHQ6IGl0ZW0uY3VycmVuY3lOYW1lIH07XG4gICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICBmaWx0ZXJPcHRpb249e2ZhbHNlfVxuICAgICAgICAgICAgICAgICBvblNlYXJjaD17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICBsZXQgbmV3Q3VycmVuY2llc0RhdGE6IGFueVtdID0gXy5jbG9uZURlZXAoY29weUN1cnJlbmNpZXNEYXRhKTtcbiAgICAgICAgICAgICAgICAgICBsZXQgZmlsdGVyOiBhbnlbXSA9XG4gICAgICAgICAgICAgICAgICAgICBuZXdDdXJyZW5jaWVzRGF0YS5maWx0ZXIoKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKChpdGVtLmN1cnJlbmN5Q29kZSkudG9Mb2NhbGVMb3dlckNhc2UoKSkuaW5kZXhPZih2YWx1ZS50b0xvY2FsZUxvd2VyQ2FzZSgpKSA+PSAwXG4gICAgICAgICAgICAgICAgICAgICAgICAgfHwgKChpdGVtLmN1cnJlbmN5TmFtZSkudG9Mb2NhbGVMb3dlckNhc2UoKSkuaW5kZXhPZih2YWx1ZS50b0xvY2FsZUxvd2VyQ2FzZSgpKSA+PSAwO1xuICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICBjdXJyZW5jaWVzRGF0YTogZmlsdGVyLFxuICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICBwcm9wTmFtZT17cHJvcE5hbWV9IHsuLi5yZXN0fS8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNlbGVjdEN1cnJlbmN5O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBbUJBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUE1QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUErQkE7Ozs7QUE3REE7QUFDQTtBQStEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/reinsurance/treaty-definition/common/select-curreny.tsx
