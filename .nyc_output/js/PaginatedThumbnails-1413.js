

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _Thumbnail = __webpack_require__(/*! ./Thumbnail */ "./node_modules/react-images/lib/components/Thumbnail.js");

var _Thumbnail2 = _interopRequireDefault(_Thumbnail);

var _Arrow = __webpack_require__(/*! ./Arrow */ "./node_modules/react-images/lib/components/Arrow.js");

var _Arrow2 = _interopRequireDefault(_Arrow);

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var classes = _noImportant.StyleSheet.create({
  paginatedThumbnails: {
    bottom: _theme2.default.container.gutter.vertical,
    height: _theme2.default.thumbnail.size,
    padding: '0 50px',
    position: 'absolute',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    left: '50%',
    transform: 'translateX(-50%)'
  }
});

var arrowStyles = {
  height: _theme2.default.thumbnail.size + _theme2.default.thumbnail.gutter * 2,
  width: 40
};

var PaginatedThumbnails = function (_Component) {
  _inherits(PaginatedThumbnails, _Component);

  function PaginatedThumbnails(props) {
    _classCallCheck(this, PaginatedThumbnails);

    var _this = _possibleConstructorReturn(this, (PaginatedThumbnails.__proto__ || Object.getPrototypeOf(PaginatedThumbnails)).call(this, props));

    _this.state = {
      hasCustomPage: false
    };
    _this.gotoPrev = _this.gotoPrev.bind(_this);
    _this.gotoNext = _this.gotoNext.bind(_this);
    return _this;
  }

  _createClass(PaginatedThumbnails, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      // Component should be controlled, flush state when currentImage changes
      if (nextProps.currentImage !== this.props.currentImage) {
        this.setState({
          hasCustomPage: false
        });
      }
    } // ==============================
    // METHODS
    // ==============================

  }, {
    key: 'getFirst',
    value: function getFirst() {
      var _props = this.props,
          currentImage = _props.currentImage,
          offset = _props.offset;

      if (this.state.hasCustomPage) {
        return this.clampFirst(this.state.first);
      }

      return this.clampFirst(currentImage - offset);
    }
  }, {
    key: 'setFirst',
    value: function setFirst(event, newFirst) {
      var first = this.state.first;

      if (event) {
        event.preventDefault();
        event.stopPropagation();
      }

      if (first === newFirst) return;
      this.setState({
        hasCustomPage: true,
        first: newFirst
      });
    }
  }, {
    key: 'gotoPrev',
    value: function gotoPrev(event) {
      this.setFirst(event, this.getFirst() - this.props.offset);
    }
  }, {
    key: 'gotoNext',
    value: function gotoNext(event) {
      this.setFirst(event, this.getFirst() + this.props.offset);
    }
  }, {
    key: 'clampFirst',
    value: function clampFirst(value) {
      var _props2 = this.props,
          images = _props2.images,
          offset = _props2.offset;
      var totalCount = 2 * offset + 1; // show $offset extra thumbnails on each side

      if (value < 0) {
        return 0;
      } else if (value + totalCount > images.length) {
        // Too far
        return images.length - totalCount;
      } else {
        return value;
      }
    } // ==============================
    // RENDERERS
    // ==============================

  }, {
    key: 'renderArrowPrev',
    value: function renderArrowPrev() {
      if (this.getFirst() <= 0) return null;
      return _react2.default.createElement(_Arrow2.default, {
        direction: 'left',
        size: 'small',
        icon: 'arrowLeft',
        onClick: this.gotoPrev,
        style: arrowStyles,
        title: 'Previous (Left arrow key)',
        type: 'button'
      });
    }
  }, {
    key: 'renderArrowNext',
    value: function renderArrowNext() {
      var _props3 = this.props,
          offset = _props3.offset,
          images = _props3.images;
      var totalCount = 2 * offset + 1;
      if (this.getFirst() + totalCount >= images.length) return null;
      return _react2.default.createElement(_Arrow2.default, {
        direction: 'right',
        size: 'small',
        icon: 'arrowRight',
        onClick: this.gotoNext,
        style: arrowStyles,
        title: 'Next (Right arrow key)',
        type: 'button'
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props4 = this.props,
          images = _props4.images,
          currentImage = _props4.currentImage,
          onClickThumbnail = _props4.onClickThumbnail,
          offset = _props4.offset;
      var totalCount = 2 * offset + 1; // show $offset extra thumbnails on each side

      var thumbnails = [];
      var baseOffset = 0;

      if (images.length <= totalCount) {
        thumbnails = images;
      } else {
        // Try to center current image in list
        baseOffset = this.getFirst();
        thumbnails = images.slice(baseOffset, baseOffset + totalCount);
      }

      return _react2.default.createElement('div', {
        className: (0, _noImportant.css)(classes.paginatedThumbnails)
      }, this.renderArrowPrev(), thumbnails.map(function (img, idx) {
        return _react2.default.createElement(_Thumbnail2.default, _extends({
          key: baseOffset + idx
        }, img, {
          index: baseOffset + idx,
          onClick: onClickThumbnail,
          active: baseOffset + idx === currentImage
        }));
      }), this.renderArrowNext());
    }
  }]);

  return PaginatedThumbnails;
}(_react.Component);

exports.default = PaginatedThumbnails;
PaginatedThumbnails.propTypes = {
  currentImage: _propTypes2.default.number,
  images: _propTypes2.default.array,
  offset: _propTypes2.default.number,
  onClickThumbnail: _propTypes2.default.func.isRequired
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL1BhZ2luYXRlZFRodW1ibmFpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1pbWFnZXMvbGliL2NvbXBvbmVudHMvUGFnaW5hdGVkVGh1bWJuYWlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfbm9JbXBvcnRhbnQgPSByZXF1aXJlKCdhcGhyb2RpdGUvbm8taW1wb3J0YW50Jyk7XG5cbnZhciBfVGh1bWJuYWlsID0gcmVxdWlyZSgnLi9UaHVtYm5haWwnKTtcblxudmFyIF9UaHVtYm5haWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVGh1bWJuYWlsKTtcblxudmFyIF9BcnJvdyA9IHJlcXVpcmUoJy4vQXJyb3cnKTtcblxudmFyIF9BcnJvdzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BcnJvdyk7XG5cbnZhciBfdGhlbWUgPSByZXF1aXJlKCcuLi90aGVtZScpO1xuXG52YXIgX3RoZW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3RoZW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgY2xhc3NlcyA9IF9ub0ltcG9ydGFudC5TdHlsZVNoZWV0LmNyZWF0ZSh7XG5cdHBhZ2luYXRlZFRodW1ibmFpbHM6IHtcblx0XHRib3R0b206IF90aGVtZTIuZGVmYXVsdC5jb250YWluZXIuZ3V0dGVyLnZlcnRpY2FsLFxuXHRcdGhlaWdodDogX3RoZW1lMi5kZWZhdWx0LnRodW1ibmFpbC5zaXplLFxuXHRcdHBhZGRpbmc6ICcwIDUwcHgnLFxuXHRcdHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuXHRcdHRleHRBbGlnbjogJ2NlbnRlcicsXG5cdFx0d2hpdGVTcGFjZTogJ25vd3JhcCcsXG5cdFx0bGVmdDogJzUwJScsXG5cdFx0dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtNTAlKSdcblx0fVxufSk7XG5cbnZhciBhcnJvd1N0eWxlcyA9IHtcblx0aGVpZ2h0OiBfdGhlbWUyLmRlZmF1bHQudGh1bWJuYWlsLnNpemUgKyBfdGhlbWUyLmRlZmF1bHQudGh1bWJuYWlsLmd1dHRlciAqIDIsXG5cdHdpZHRoOiA0MFxufTtcblxudmFyIFBhZ2luYXRlZFRodW1ibmFpbHMgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuXHRfaW5oZXJpdHMoUGFnaW5hdGVkVGh1bWJuYWlscywgX0NvbXBvbmVudCk7XG5cblx0ZnVuY3Rpb24gUGFnaW5hdGVkVGh1bWJuYWlscyhwcm9wcykge1xuXHRcdF9jbGFzc0NhbGxDaGVjayh0aGlzLCBQYWdpbmF0ZWRUaHVtYm5haWxzKTtcblxuXHRcdHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChQYWdpbmF0ZWRUaHVtYm5haWxzLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoUGFnaW5hdGVkVGh1bWJuYWlscykpLmNhbGwodGhpcywgcHJvcHMpKTtcblxuXHRcdF90aGlzLnN0YXRlID0ge1xuXHRcdFx0aGFzQ3VzdG9tUGFnZTogZmFsc2Vcblx0XHR9O1xuXG5cdFx0X3RoaXMuZ290b1ByZXYgPSBfdGhpcy5nb3RvUHJldi5iaW5kKF90aGlzKTtcblx0XHRfdGhpcy5nb3RvTmV4dCA9IF90aGlzLmdvdG9OZXh0LmJpbmQoX3RoaXMpO1xuXHRcdHJldHVybiBfdGhpcztcblx0fVxuXG5cdF9jcmVhdGVDbGFzcyhQYWdpbmF0ZWRUaHVtYm5haWxzLCBbe1xuXHRcdGtleTogJ2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuXHRcdFx0Ly8gQ29tcG9uZW50IHNob3VsZCBiZSBjb250cm9sbGVkLCBmbHVzaCBzdGF0ZSB3aGVuIGN1cnJlbnRJbWFnZSBjaGFuZ2VzXG5cdFx0XHRpZiAobmV4dFByb3BzLmN1cnJlbnRJbWFnZSAhPT0gdGhpcy5wcm9wcy5jdXJyZW50SW1hZ2UpIHtcblx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdFx0aGFzQ3VzdG9tUGFnZTogZmFsc2Vcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG5cdFx0Ly8gTUVUSE9EU1xuXHRcdC8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxuXG5cdH0sIHtcblx0XHRrZXk6ICdnZXRGaXJzdCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGdldEZpcnN0KCkge1xuXHRcdFx0dmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG5cdFx0XHQgICAgY3VycmVudEltYWdlID0gX3Byb3BzLmN1cnJlbnRJbWFnZSxcblx0XHRcdCAgICBvZmZzZXQgPSBfcHJvcHMub2Zmc2V0O1xuXG5cdFx0XHRpZiAodGhpcy5zdGF0ZS5oYXNDdXN0b21QYWdlKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmNsYW1wRmlyc3QodGhpcy5zdGF0ZS5maXJzdCk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gdGhpcy5jbGFtcEZpcnN0KGN1cnJlbnRJbWFnZSAtIG9mZnNldCk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnc2V0Rmlyc3QnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBzZXRGaXJzdChldmVudCwgbmV3Rmlyc3QpIHtcblx0XHRcdHZhciBmaXJzdCA9IHRoaXMuc3RhdGUuZmlyc3Q7XG5cblxuXHRcdFx0aWYgKGV2ZW50KSB7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAoZmlyc3QgPT09IG5ld0ZpcnN0KSByZXR1cm47XG5cblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0XHRoYXNDdXN0b21QYWdlOiB0cnVlLFxuXHRcdFx0XHRmaXJzdDogbmV3Rmlyc3Rcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dvdG9QcmV2Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gZ290b1ByZXYoZXZlbnQpIHtcblx0XHRcdHRoaXMuc2V0Rmlyc3QoZXZlbnQsIHRoaXMuZ2V0Rmlyc3QoKSAtIHRoaXMucHJvcHMub2Zmc2V0KTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdnb3RvTmV4dCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGdvdG9OZXh0KGV2ZW50KSB7XG5cdFx0XHR0aGlzLnNldEZpcnN0KGV2ZW50LCB0aGlzLmdldEZpcnN0KCkgKyB0aGlzLnByb3BzLm9mZnNldCk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnY2xhbXBGaXJzdCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNsYW1wRmlyc3QodmFsdWUpIHtcblx0XHRcdHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBpbWFnZXMgPSBfcHJvcHMyLmltYWdlcyxcblx0XHRcdCAgICBvZmZzZXQgPSBfcHJvcHMyLm9mZnNldDtcblxuXG5cdFx0XHR2YXIgdG90YWxDb3VudCA9IDIgKiBvZmZzZXQgKyAxOyAvLyBzaG93ICRvZmZzZXQgZXh0cmEgdGh1bWJuYWlscyBvbiBlYWNoIHNpZGVcblxuXHRcdFx0aWYgKHZhbHVlIDwgMCkge1xuXHRcdFx0XHRyZXR1cm4gMDtcblx0XHRcdH0gZWxzZSBpZiAodmFsdWUgKyB0b3RhbENvdW50ID4gaW1hZ2VzLmxlbmd0aCkge1xuXHRcdFx0XHQvLyBUb28gZmFyXG5cdFx0XHRcdHJldHVybiBpbWFnZXMubGVuZ3RoIC0gdG90YWxDb3VudDtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHJldHVybiB2YWx1ZTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHQvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblx0XHQvLyBSRU5ERVJFUlNcblx0XHQvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuXHR9LCB7XG5cdFx0a2V5OiAncmVuZGVyQXJyb3dQcmV2Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gcmVuZGVyQXJyb3dQcmV2KCkge1xuXHRcdFx0aWYgKHRoaXMuZ2V0Rmlyc3QoKSA8PSAwKSByZXR1cm4gbnVsbDtcblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9BcnJvdzIuZGVmYXVsdCwge1xuXHRcdFx0XHRkaXJlY3Rpb246ICdsZWZ0Jyxcblx0XHRcdFx0c2l6ZTogJ3NtYWxsJyxcblx0XHRcdFx0aWNvbjogJ2Fycm93TGVmdCcsXG5cdFx0XHRcdG9uQ2xpY2s6IHRoaXMuZ290b1ByZXYsXG5cdFx0XHRcdHN0eWxlOiBhcnJvd1N0eWxlcyxcblx0XHRcdFx0dGl0bGU6ICdQcmV2aW91cyAoTGVmdCBhcnJvdyBrZXkpJyxcblx0XHRcdFx0dHlwZTogJ2J1dHRvbidcblx0XHRcdH0pO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlckFycm93TmV4dCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlckFycm93TmV4dCgpIHtcblx0XHRcdHZhciBfcHJvcHMzID0gdGhpcy5wcm9wcyxcblx0XHRcdCAgICBvZmZzZXQgPSBfcHJvcHMzLm9mZnNldCxcblx0XHRcdCAgICBpbWFnZXMgPSBfcHJvcHMzLmltYWdlcztcblxuXHRcdFx0dmFyIHRvdGFsQ291bnQgPSAyICogb2Zmc2V0ICsgMTtcblx0XHRcdGlmICh0aGlzLmdldEZpcnN0KCkgKyB0b3RhbENvdW50ID49IGltYWdlcy5sZW5ndGgpIHJldHVybiBudWxsO1xuXG5cdFx0XHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0Fycm93Mi5kZWZhdWx0LCB7XG5cdFx0XHRcdGRpcmVjdGlvbjogJ3JpZ2h0Jyxcblx0XHRcdFx0c2l6ZTogJ3NtYWxsJyxcblx0XHRcdFx0aWNvbjogJ2Fycm93UmlnaHQnLFxuXHRcdFx0XHRvbkNsaWNrOiB0aGlzLmdvdG9OZXh0LFxuXHRcdFx0XHRzdHlsZTogYXJyb3dTdHlsZXMsXG5cdFx0XHRcdHRpdGxlOiAnTmV4dCAoUmlnaHQgYXJyb3cga2V5KScsXG5cdFx0XHRcdHR5cGU6ICdidXR0b24nXG5cdFx0XHR9KTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdyZW5kZXInLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG5cdFx0XHR2YXIgX3Byb3BzNCA9IHRoaXMucHJvcHMsXG5cdFx0XHQgICAgaW1hZ2VzID0gX3Byb3BzNC5pbWFnZXMsXG5cdFx0XHQgICAgY3VycmVudEltYWdlID0gX3Byb3BzNC5jdXJyZW50SW1hZ2UsXG5cdFx0XHQgICAgb25DbGlja1RodW1ibmFpbCA9IF9wcm9wczQub25DbGlja1RodW1ibmFpbCxcblx0XHRcdCAgICBvZmZzZXQgPSBfcHJvcHM0Lm9mZnNldDtcblxuXG5cdFx0XHR2YXIgdG90YWxDb3VudCA9IDIgKiBvZmZzZXQgKyAxOyAvLyBzaG93ICRvZmZzZXQgZXh0cmEgdGh1bWJuYWlscyBvbiBlYWNoIHNpZGVcblx0XHRcdHZhciB0aHVtYm5haWxzID0gW107XG5cdFx0XHR2YXIgYmFzZU9mZnNldCA9IDA7XG5cdFx0XHRpZiAoaW1hZ2VzLmxlbmd0aCA8PSB0b3RhbENvdW50KSB7XG5cdFx0XHRcdHRodW1ibmFpbHMgPSBpbWFnZXM7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHQvLyBUcnkgdG8gY2VudGVyIGN1cnJlbnQgaW1hZ2UgaW4gbGlzdFxuXHRcdFx0XHRiYXNlT2Zmc2V0ID0gdGhpcy5nZXRGaXJzdCgpO1xuXHRcdFx0XHR0aHVtYm5haWxzID0gaW1hZ2VzLnNsaWNlKGJhc2VPZmZzZXQsIGJhc2VPZmZzZXQgKyB0b3RhbENvdW50KTtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuXHRcdFx0XHQnZGl2Jyxcblx0XHRcdFx0eyBjbGFzc05hbWU6ICgwLCBfbm9JbXBvcnRhbnQuY3NzKShjbGFzc2VzLnBhZ2luYXRlZFRodW1ibmFpbHMpIH0sXG5cdFx0XHRcdHRoaXMucmVuZGVyQXJyb3dQcmV2KCksXG5cdFx0XHRcdHRodW1ibmFpbHMubWFwKGZ1bmN0aW9uIChpbWcsIGlkeCkge1xuXHRcdFx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfVGh1bWJuYWlsMi5kZWZhdWx0LCBfZXh0ZW5kcyh7IGtleTogYmFzZU9mZnNldCArIGlkeFxuXHRcdFx0XHRcdH0sIGltZywge1xuXHRcdFx0XHRcdFx0aW5kZXg6IGJhc2VPZmZzZXQgKyBpZHgsXG5cdFx0XHRcdFx0XHRvbkNsaWNrOiBvbkNsaWNrVGh1bWJuYWlsLFxuXHRcdFx0XHRcdFx0YWN0aXZlOiBiYXNlT2Zmc2V0ICsgaWR4ID09PSBjdXJyZW50SW1hZ2UgfSkpO1xuXHRcdFx0XHR9KSxcblx0XHRcdFx0dGhpcy5yZW5kZXJBcnJvd05leHQoKVxuXHRcdFx0KTtcblx0XHR9XG5cdH1dKTtcblxuXHRyZXR1cm4gUGFnaW5hdGVkVGh1bWJuYWlscztcbn0oX3JlYWN0LkNvbXBvbmVudCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFBhZ2luYXRlZFRodW1ibmFpbHM7XG5cblxuUGFnaW5hdGVkVGh1bWJuYWlscy5wcm9wVHlwZXMgPSB7XG5cdGN1cnJlbnRJbWFnZTogX3Byb3BUeXBlczIuZGVmYXVsdC5udW1iZXIsXG5cdGltYWdlczogX3Byb3BUeXBlczIuZGVmYXVsdC5hcnJheSxcblx0b2Zmc2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcblx0b25DbGlja1RodW1ibmFpbDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWRcbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBREE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBZEE7QUFnQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFqQkE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQXZCQTtBQXlCQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQWRBO0FBZ0JBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFuQkE7QUFxQkE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBSUE7QUFHQTtBQWxDQTtBQUNBO0FBb0NBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/PaginatedThumbnails.js
