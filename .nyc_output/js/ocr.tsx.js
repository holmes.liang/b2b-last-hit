__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _attach_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./attach-card */ "./src/app/desk/component/ph/attach-card.tsx");
/* harmony import */ var _desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/attachment/doc-types */ "./src/app/desk/component/attachment/doc-types.tsx");
/* harmony import */ var _progress__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./progress */ "./src/app/desk/component/ph/progress.tsx");
/* harmony import */ var _desk_component_ph_policyholder_view__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/ph/policyholder-view */ "./src/app/desk/component/ph/policyholder-view.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/ph/ocr.tsx";

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    &.mobile-select {\n      .ant-form-item-label {\n        padding-right: 12px;\n        padding-top: 10px;\n      }\n      .ant-form-item-control-wrapper {\n        width: 135px;\n      }\n    }\n    .ant-form-item-label {\n        text-align: right;\n        width: 120px;\n    }\n    \n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    .action {\n        text-align: right;\n        margin-top: 20px;\n        .ant-btn {\n            &:last-child {\n                margin-left: 10px;\n            }\n        }\n    }\n    .col-label {\n        color: #9e9e9e;\n        text-transform: uppercase;\n        width: 120px;\n        text-align: right;\n        padding-right: 12px;\n        i {\n            display: inline-block;\n            margin-right: 4px;\n            color: #f5222d;\n            font-size: 14px;\n            font-family: SimSun,sans-serif;\n            line-height: 1;\n        }\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}












var isMobile = _common__WEBPACK_IMPORTED_MODULE_16__["Utils"].getIsMobile();
var defaultLayout = {
  labelCol: {
    xs: {
      span: 2
    },
    sm: {
      span: 4
    }
  },
  wrapperCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 10
    }
  }
};

var Ocr =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Ocr, _ModelWidget);

  function Ocr() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Ocr);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Ocr)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.handleOk = function () {
      _this.setState({
        showProgress: true,
        showUploading: false
      });
    };

    _this.handleCancel = function () {
      _this.props.onCancel();
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Ocr, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setValueToModel("NRIC", "idType");
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      var fids = this.getValueFromModel("attachmentFids") || [];
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Modal"], {
        title: "OCR",
        width: _common__WEBPACK_IMPORTED_MODULE_16__["Utils"].getIsMobile() ? "100%" : "50%",
        visible: true,
        centered: true,
        onOk: this.handleOk,
        onCancel: this.handleCancel,
        footer: null,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, !this.state.showProgress && this.state.showUploading && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(SelectStyle, {
        className: "".concat(isMobile ? "mobile-select" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_17__["NSelect"], {
        size: "large",
        form: form,
        layoutCol: defaultLayout,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_16__["Language"].en("ID Type").thai("ประเภท ID").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        model: model,
        style: {
          width: "100%"
        },
        options: [{
          id: "NRIC",
          text: "NRIC"
        }],
        propName: "idType",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(AttachStyle, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        style: {
          display: "flex"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 4,
        className: "col-label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, "*"), "Attachments"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 20,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_attach_card__WEBPACK_IMPORTED_MODULE_11__["default"], {
        model: model,
        form: form,
        donotStoreModel: true,
        removeImg: function removeImg(uid, item) {
          var docsList = _this2.getValueFromModel("attachmentFids");

          var newList = docsList.filter(function (prop) {
            return prop !== item.id;
          });
          docsList = newList;

          _this2.setValueToModel(docsList, "attachmentFids");
        },
        docTypes: Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_12__["getDocTypeForCard"])(),
        uploadImgSuccess: function uploadImgSuccess(res, docType) {
          var fid = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "attachId");

          var docsList = _this2.getValueFromModel("attachmentFids") || [];

          if (!lodash__WEBPACK_IMPORTED_MODULE_7___default.a.isEmpty(fid)) {
            docsList.push(fid);

            _this2.setValueToModel(docsList, "attachmentFids");
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 116
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        key: "onCancel",
        onClick: function onClick() {
          _this2.handleCancel();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, "Cancel"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        key: "ok",
        type: "primary",
        onClick: function onClick() {
          _this2.handleOk();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, "Identify")))), this.state.showProgress && !this.state.showUploading && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_progress__WEBPACK_IMPORTED_MODULE_13__["default"], {
        model: this.props.model,
        postData: {
          frontFid: fids.length > 0 ? fids[0] : "",
          backFid: fids.length > 1 ? fids[1] : ""
        },
        overCallback: function overCallback(endo) {
          if (endo !== "error") {
            if (endo !== "error-cancel") {
              var idNo = endo.idNo,
                  name = endo.name,
                  gender = endo.gender,
                  address = endo.address,
                  fullAddress = endo.fullAddress,
                  title = endo.title,
                  idType = endo.idType,
                  dateOfBirth = endo.dateOfBirth;

              _this2.setState({
                infoData: {
                  address: address,
                  gender: gender,
                  name: name,
                  idType: _this2.getValueFromModel("idType"),
                  title: title,
                  dob: dateOfBirth,
                  idNo: idNo
                },
                isShowInfo: true,
                showUploading: false,
                showProgress: false
              });
            } else {
              _this2.setState({
                showProgress: false,
                showUploading: true
              });
            }
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }), this.state.isShowInfo && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 161
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_ph_policyholder_view__WEBPACK_IMPORTED_MODULE_14__["default"], {
        model: _model__WEBPACK_IMPORTED_MODULE_15__["Modeller"].asProxied({
          ext: {
            Policyholder: this.state.infoData
          }
        }),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action",
        style: {
          textAlign: "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        key: "ok",
        type: "primary",
        onClick: function onClick() {
          _this2.props.onOk(_this2.state.infoData);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168
        },
        __self: this
      }, "Submit"))));
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Ocr.prototype), "initState", this).call(this), {
        showProgress: false,
        showUploading: true,
        infoData: {},
        isShowInfo: false
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }]);

  return Ocr;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

Ocr.defaultProps = {
  onOk: function onOk() {},
  onCancel: function onCancel() {}
};
var AttachStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject());
var SelectStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject2());
/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_9__["Form"].create()(Ocr));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BoL29jci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGgvb2NyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24sIENvbCwgRm9ybSwgTW9kYWwsIFJvdyB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgQXR0YWNoQ2FyZCBmcm9tIFwiLi9hdHRhY2gtY2FyZFwiO1xuaW1wb3J0IHsgZ2V0RG9jVHlwZUZvckNhcmQgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2F0dGFjaG1lbnQvZG9jLXR5cGVzXCI7XG5pbXBvcnQgUHJvZ3Jlc3NEaWFsb2cgZnJvbSBcIi4vcHJvZ3Jlc3NcIjtcbmltcG9ydCBQb2xpY3lob2xkZXJWaWV3IGZyb20gXCJAZGVzay1jb21wb25lbnQvcGgvcG9saWN5aG9sZGVyLXZpZXdcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5TZWxlY3QgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuY29uc3QgZGVmYXVsdExheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiAyIH0sXG4gICAgc206IHsgc3BhbjogNCB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogNiB9LFxuICAgIHNtOiB7IHNwYW46IDEwIH0sXG4gIH0sXG59O1xudHlwZSBPY3JQcm9wcyA9IHtcbiAgb25PazogRnVuY3Rpb24sXG4gIG9uQ2FuY2VsOiBGdW5jdGlvbixcbiAgbW9kZWw6IGFueSxcbn0gJiBGb3JtQ29tcG9uZW50UHJvcHNcblxudHlwZSBJU3RhdGUgPSB7XG4gIHNob3dQcm9ncmVzczogYm9vbGVhbjtcbiAgc2hvd1VwbG9hZGluZzogYm9vbGVhbjtcbiAgaW5mb0RhdGE6IGFueTtcbiAgaXNTaG93SW5mbzogYm9vbGVhbjtcbn1cblxuY2xhc3MgT2NyPFAgZXh0ZW5kcyBPY3JQcm9wcywgUyBleHRlbmRzIElTdGF0ZSwgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgb25PazogKCkgPT4ge1xuICAgIH0sXG4gICAgb25DYW5jZWw6ICgpID0+IHtcbiAgICB9LFxuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKFwiTlJJQ1wiLCBcImlkVHlwZVwiKTtcbiAgfVxuXG4gIGhhbmRsZU9rID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2hvd1Byb2dyZXNzOiB0cnVlLFxuICAgICAgc2hvd1VwbG9hZGluZzogZmFsc2UsXG4gICAgfSk7XG4gIH07XG5cbiAgaGFuZGxlQ2FuY2VsID0gKCkgPT4ge1xuICAgIHRoaXMucHJvcHMub25DYW5jZWwoKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBsZXQgZmlkczogYW55W10gPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiYXR0YWNobWVudEZpZHNcIikgfHwgW107XG4gICAgcmV0dXJuIChcbiAgICAgIDxNb2RhbFxuICAgICAgICB0aXRsZT1cIk9DUlwiXG4gICAgICAgIHdpZHRoPXtVdGlscy5nZXRJc01vYmlsZSgpID8gXCIxMDAlXCIgOiBcIjUwJVwifVxuICAgICAgICB2aXNpYmxlXG4gICAgICAgIGNlbnRlcmVkPXt0cnVlfVxuICAgICAgICBvbk9rPXt0aGlzLmhhbmRsZU9rfVxuICAgICAgICBvbkNhbmNlbD17dGhpcy5oYW5kbGVDYW5jZWx9XG4gICAgICAgIGZvb3Rlcj17bnVsbH1cbiAgICAgID5cbiAgICAgICAgeyF0aGlzLnN0YXRlLnNob3dQcm9ncmVzcyAmJiB0aGlzLnN0YXRlLnNob3dVcGxvYWRpbmcgJiYgPD5cbiAgICAgICAgICA8U2VsZWN0U3R5bGUgY2xhc3NOYW1lPXtgJHtpc01vYmlsZSA/IFwibW9iaWxlLXNlbGVjdFwiIDogXCJcIn1gfT5cbiAgICAgICAgICAgIDxOU2VsZWN0XG4gICAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbGF5b3V0Q29sPXtkZWZhdWx0TGF5b3V0fVxuICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgVHlwZVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwi4Lib4Lij4Liw4LmA4Lig4LiXIElEXCIpXG4gICAgICAgICAgICAgICAgLm15KFwi4YCh4YCt4YCv4YCE4YC64YCS4YCu4YCh4YCZ4YC74YCt4YCv4YC44YCh4YCF4YCs4YC4IC8gTk8g4YGLXCIpXG4gICAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICAgICAgb3B0aW9ucz17W3sgaWQ6IFwiTlJJQ1wiLCB0ZXh0OiBcIk5SSUNcIiB9XX1cbiAgICAgICAgICAgICAgcHJvcE5hbWU9e1wiaWRUeXBlXCJ9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvU2VsZWN0U3R5bGU+XG4gICAgICAgICAgPEF0dGFjaFN0eWxlPjxSb3cgc3R5bGU9e3sgZGlzcGxheTogXCJmbGV4XCIgfX0+XG4gICAgICAgICAgICA8Q29sIHNwYW49ezR9IGNsYXNzTmFtZT17XCJjb2wtbGFiZWxcIn0+PGk+KjwvaT5BdHRhY2htZW50czwvQ29sPlxuICAgICAgICAgICAgPENvbCBzcGFuPXsyMH0+XG4gICAgICAgICAgICAgIDxBdHRhY2hDYXJkXG4gICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgZG9ub3RTdG9yZU1vZGVsPXt0cnVlfVxuICAgICAgICAgICAgICAgIHJlbW92ZUltZz17KHVpZDogYW55LCBpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgIGxldCBkb2NzTGlzdCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJhdHRhY2htZW50Rmlkc1wiKTtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IG5ld0xpc3QgPSBkb2NzTGlzdC5maWx0ZXIoKHByb3A6IGFueSkgPT4gcHJvcCAhPT0gaXRlbS5pZCk7XG4gICAgICAgICAgICAgICAgICBkb2NzTGlzdCA9IG5ld0xpc3Q7XG4gICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChkb2NzTGlzdCwgXCJhdHRhY2htZW50Rmlkc1wiKTtcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgIGRvY1R5cGVzPXtnZXREb2NUeXBlRm9yQ2FyZCgpfVxuICAgICAgICAgICAgICAgIHVwbG9hZEltZ1N1Y2Nlc3M9eyhyZXM6IGFueSwgZG9jVHlwZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICBjb25zdCBmaWQgPSBfLmdldChyZXMsIFwiYXR0YWNoSWRcIik7XG4gICAgICAgICAgICAgICAgICBsZXQgZG9jc0xpc3QgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiYXR0YWNobWVudEZpZHNcIikgfHwgW107XG4gICAgICAgICAgICAgICAgICBpZiAoIV8uaXNFbXB0eShmaWQpKSB7XG4gICAgICAgICAgICAgICAgICAgIGRvY3NMaXN0LnB1c2goZmlkKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoZG9jc0xpc3QsIFwiYXR0YWNobWVudEZpZHNcIik7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvQ29sPjwvUm93PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wiYWN0aW9uXCJ9PlxuICAgICAgICAgICAgICA8QnV0dG9uIGtleT0nb25DYW5jZWwnXG4gICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oYW5kbGVDYW5jZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgICB9fT5DYW5jZWw8L0J1dHRvbj5cbiAgICAgICAgICAgICAgPEJ1dHRvbiBrZXk9J29rJ1xuICAgICAgICAgICAgICAgICAgICAgIHR5cGU9e1wicHJpbWFyeVwifVxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlT2soKTtcbiAgICAgICAgICAgICAgICAgICAgICB9fT5JZGVudGlmeTwvQnV0dG9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9BdHRhY2hTdHlsZT5cbiAgICAgICAgPC8+fVxuICAgICAgICB7dGhpcy5zdGF0ZS5zaG93UHJvZ3Jlc3MgJiYgIXRoaXMuc3RhdGUuc2hvd1VwbG9hZGluZyAmJiA8UHJvZ3Jlc3NEaWFsb2dcbiAgICAgICAgICBtb2RlbD17dGhpcy5wcm9wcy5tb2RlbH1cbiAgICAgICAgICBwb3N0RGF0YT17e1xuICAgICAgICAgICAgZnJvbnRGaWQ6IGZpZHMubGVuZ3RoID4gMCA/IGZpZHNbMF0gOiBcIlwiLFxuICAgICAgICAgICAgYmFja0ZpZDogZmlkcy5sZW5ndGggPiAxID8gZmlkc1sxXSA6IFwiXCIsXG4gICAgICAgICAgfX1cbiAgICAgICAgICBvdmVyQ2FsbGJhY2s9eyhlbmRvOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGlmIChlbmRvICE9PSBcImVycm9yXCIpIHtcbiAgICAgICAgICAgICAgaWYgKGVuZG8gIT09IFwiZXJyb3ItY2FuY2VsXCIpIHtcbiAgICAgICAgICAgICAgICBjb25zdCB7IGlkTm8sIG5hbWUsIGdlbmRlciwgYWRkcmVzcywgZnVsbEFkZHJlc3MsIHRpdGxlLCBpZFR5cGUsIGRhdGVPZkJpcnRoIH0gPSBlbmRvO1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgaW5mb0RhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgZ2VuZGVyLFxuICAgICAgICAgICAgICAgICAgICBuYW1lLFxuICAgICAgICAgICAgICAgICAgICBpZFR5cGU6IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJpZFR5cGVcIiksXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlLFxuICAgICAgICAgICAgICAgICAgICBkb2I6IGRhdGVPZkJpcnRoLFxuICAgICAgICAgICAgICAgICAgICBpZE5vLFxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIGlzU2hvd0luZm86IHRydWUsXG4gICAgICAgICAgICAgICAgICBzaG93VXBsb2FkaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgIHNob3dQcm9ncmVzczogZmFsc2UsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBzaG93UHJvZ3Jlc3M6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgc2hvd1VwbG9hZGluZzogdHJ1ZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH19Lz59XG4gICAgICAgIHt0aGlzLnN0YXRlLmlzU2hvd0luZm8gJiYgPGRpdj5cbiAgICAgICAgICA8UG9saWN5aG9sZGVyVmlldyBtb2RlbD17TW9kZWxsZXIuYXNQcm94aWVkKHtcbiAgICAgICAgICAgIGV4dDoge1xuICAgICAgICAgICAgICBQb2xpY3lob2xkZXI6IHRoaXMuc3RhdGUuaW5mb0RhdGEsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIH0pfS8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wiYWN0aW9uXCJ9IHN0eWxlPXt7IHRleHRBbGlnbjogXCJyaWdodFwiIH19PlxuICAgICAgICAgICAgPEJ1dHRvbiBrZXk9J29rJ1xuICAgICAgICAgICAgICAgICAgICB0eXBlPXtcInByaW1hcnlcIn1cbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25Payh0aGlzLnN0YXRlLmluZm9EYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgfX0+U3VibWl0PC9CdXR0b24+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2Pn1cbiAgICAgIDwvTW9kYWw+XG4gICAgKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIHNob3dQcm9ncmVzczogZmFsc2UsXG4gICAgICBzaG93VXBsb2FkaW5nOiB0cnVlLFxuICAgICAgaW5mb0RhdGE6IHt9LFxuICAgICAgaXNTaG93SW5mbzogZmFsc2UsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxufVxuXG5jb25zdCBBdHRhY2hTdHlsZSA9IFN0eWxlZC5kaXZgXG4gICAgLmFjdGlvbiB7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICAuYW50LWJ0biB7XG4gICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIC5jb2wtbGFiZWwge1xuICAgICAgICBjb2xvcjogIzllOWU5ZTtcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgd2lkdGg6IDEyMHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTJweDtcbiAgICAgICAgaSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcbiAgICAgICAgICAgIGNvbG9yOiAjZjUyMjJkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6IFNpbVN1bixzYW5zLXNlcmlmO1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgICAgIH1cbiAgICB9XG5gO1xuY29uc3QgU2VsZWN0U3R5bGUgPSBTdHlsZWQuZGl2YFxuICAgICYubW9iaWxlLXNlbGVjdCB7XG4gICAgICAuYW50LWZvcm0taXRlbS1sYWJlbCB7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEycHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgfVxuICAgICAgLmFudC1mb3JtLWl0ZW0tY29udHJvbC13cmFwcGVyIHtcbiAgICAgICAgd2lkdGg6IDEzNXB4O1xuICAgICAgfVxuICAgIH1cbiAgICAuYW50LWZvcm0taXRlbS1sYWJlbCB7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICB3aWR0aDogMTIwcHg7XG4gICAgfVxuICAgIFxuYDtcbmV4cG9ydCBkZWZhdWx0IEZvcm0uY3JlYXRlPE9jclByb3BzPigpKE9jcik7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQXNCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBZEE7QUFDQTtBQUNBOzs7QUFhQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWxCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUEvQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0NBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTs7O0FBRUE7QUFDQTtBQUNBOzs7O0FBdkpBO0FBQ0E7QUFEQTtBQUVBO0FBRUE7QUFIQTtBQXlKQTtBQTBCQTtBQWdCQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/ph/ocr.tsx
