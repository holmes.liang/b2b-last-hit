__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EMPTY_SET", function() { return EMPTY_SET; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_ELEMENT", function() { return DEFAULT_ELEMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_INLINE_STYLE", function() { return DEFAULT_INLINE_STYLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return GetHTML; });
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! immutable */ "./node_modules/rc-editor-core/node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _isUnitlessNumber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./isUnitlessNumber */ "./node_modules/rc-editor-core/es/EditorCore/export/isUnitlessNumber.js");
var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};




var EMPTY_SET = Object(immutable__WEBPACK_IMPORTED_MODULE_1__["OrderedSet"])();
var DEFAULT_ELEMENT = 'span';
var DEFAULT_INLINE_STYLE = draft_js__WEBPACK_IMPORTED_MODULE_0__["DefaultDraftInlineStyle"];

function encodeContent(text) {
  return text.split('&').join('&amp;').split('<').join('&lt;').split('>').join('&gt;').split('\xA0').join('&nbsp;').split('\n').join('<br >' + '\n');
}

function encodeAttr(text) {
  return text.split('&').join('&amp;').split('<').join('&lt;').split('>').join('&gt;').split('"').join('&quot;');
}

var VENDOR_PREFIX = /^(moz|ms|o|webkit)-/;
var NUMERIC_STRING = /^\d+$/;
var UPPERCASE_PATTERN = /([A-Z])/g; // Lifted from: https://github.com/facebook/react/blob/master/src/renderers/dom/shared/CSSPropertyOperations.js

function processStyleName(name) {
  return name.replace(UPPERCASE_PATTERN, '-$1').toLowerCase().replace(VENDOR_PREFIX, '-$1-');
} // Lifted from: https://github.com/facebook/react/blob/master/src/renderers/dom/shared/dangerousStyleValue.js


function processStyleValue(name, value) {
  var isNumeric = void 0;

  if (typeof value === 'string') {
    isNumeric = NUMERIC_STRING.test(value);
  } else {
    isNumeric = true;
    value = String(value);
  }

  if (!isNumeric || value === '0' || _isUnitlessNumber__WEBPACK_IMPORTED_MODULE_2__["default"][name] === true) {
    return value;
  } else {
    return value + 'px';
  }
}

function getStyleText(styleObject) {
  if (!styleObject) {
    return '';
  }

  return Object.keys(styleObject).map(function (name) {
    var styleName = processStyleName(name);
    var styleValue = processStyleValue(name, styleObject[name]);
    return styleName + ':' + styleValue;
  }).join(';');
}

function getEntityContent(contentState, entityKey, content) {
  if (entityKey) {
    var entity = contentState.getEntity(entityKey);
    var entityData = entity.getData();

    if (entityData && entityData['export']) {
      return entityData['export'](content, entityData);
    }
  }

  return content;
}

function GetHTML(configStore) {
  return function exportHtml(editorState) {
    var contentState = editorState.getCurrentContent();
    var blockMap = contentState.getBlockMap();
    var customStyleMap = configStore.get('customStyleMap') || {};
    var customBlockRenderMap = configStore.get('blockRenderMap') || {};
    var customStyleFn = configStore.get('customStyleFn');
    var toHTMLList = configStore.get('toHTMLList');

    _extends(customStyleMap, DEFAULT_INLINE_STYLE);

    return blockMap.map(function (block) {
      var resultText = '<div>';
      var closeTag = '</div>';
      var lastPosition = 0;
      var text = block.getText();
      var blockType = block.getType();
      var blockRender = customBlockRenderMap.get(blockType);

      if (blockRender) {
        var element = typeof blockRender.element === 'function' ? blockRender.elementTag || 'div' : 'div';
        resultText = '<' + (element || 'div') + ' style="' + getStyleText(customBlockRenderMap.get(blockType).style || {}) + '">';
        closeTag = '</' + (element || 'div') + '>';
      }

      var charMetaList = block.getCharacterList();
      var charEntity = null;
      var prevCharEntity = null;
      var ranges = [];
      var rangeStart = 0;

      for (var i = 0, len = text.length; i < len; i++) {
        prevCharEntity = charEntity;
        var meta = charMetaList.get(i);
        charEntity = meta ? meta.getEntity() : null;

        if (i > 0 && charEntity !== prevCharEntity) {
          ranges.push([prevCharEntity, getStyleRanges(text.slice(rangeStart, i), charMetaList.slice(rangeStart, i))]);
          rangeStart = i;
        }
      }

      ranges.push([charEntity, getStyleRanges(text.slice(rangeStart), charMetaList.slice(rangeStart))]);
      ranges.map(function (_ref) {
        var entityKey = _ref[0],
            stylePieces = _ref[1];
        var element = DEFAULT_ELEMENT;
        var rawContent = stylePieces.map(function (_ref2) {
          var text = _ref2[0];
          return text;
        }).join('');
        var content = stylePieces.map(function (_ref3) {
          var text = _ref3[0],
              styleSet = _ref3[1];
          var encodedContent = encodeContent(text);

          if (styleSet.size) {
            var inlineStyle = {};
            styleSet.forEach(function (item) {
              if (customStyleMap.hasOwnProperty(item)) {
                var currentStyle = customStyleMap[item];
                inlineStyle = _extends(inlineStyle, currentStyle);
              }
            });
            var customedStyle = customStyleFn(styleSet);
            inlineStyle = _extends(inlineStyle, customedStyle);
            return '<span style="' + getStyleText(inlineStyle) + '">' + encodedContent + '</span>';
          }

          return '<span>' + encodedContent + '</span>';
        }).join('');

        if (entityKey) {
          var entity = contentState.getEntity(entityKey);
          var entityData = entity.getData();

          if (entityData && entityData['export']) {
            resultText += entityData['export'](content, entityData);
          } else {
            var HTMLText = '';
            toHTMLList.forEach(function (toHTML) {
              var text = toHTML(rawContent, entity, contentState);

              if (text) {
                HTMLText = text;
              }
            });

            if (HTMLText) {
              resultText += HTMLText;
            }
          }
        } else {
          resultText += content;
        }
      });
      resultText += closeTag;
      return resultText;
    }).join('\n');
  };
}

function getStyleRanges(text, charMetaList) {
  var charStyle = EMPTY_SET;
  var prevCharStyle = EMPTY_SET;
  var ranges = [];
  var rangeStart = 0;

  for (var i = 0, len = text.length; i < len; i++) {
    prevCharStyle = charStyle;
    var meta = charMetaList.get(i);
    charStyle = meta ? meta.getStyle() : EMPTY_SET;

    if (i > 0 && !Object(immutable__WEBPACK_IMPORTED_MODULE_1__["is"])(charStyle, prevCharStyle)) {
      ranges.push([text.slice(rangeStart, i), prevCharStyle]);
      rangeStart = i;
    }
  }

  ranges.push([text.slice(rangeStart), charStyle]);
  return ranges;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvRWRpdG9yQ29yZS9leHBvcnQvZ2V0SFRNTC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1jb3JlL2VzL0VkaXRvckNvcmUvZXhwb3J0L2dldEhUTUwuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxuaW1wb3J0IHsgRGVmYXVsdERyYWZ0SW5saW5lU3R5bGUgfSBmcm9tIFwiZHJhZnQtanNcIjtcbmltcG9ydCB7IE9yZGVyZWRTZXQsIGlzIH0gZnJvbSAnaW1tdXRhYmxlJztcbmltcG9ydCBpc1VuaXRsZXNzTnVtYmVyIGZyb20gJy4vaXNVbml0bGVzc051bWJlcic7XG5leHBvcnQgdmFyIEVNUFRZX1NFVCA9IE9yZGVyZWRTZXQoKTtcbmV4cG9ydCB2YXIgREVGQVVMVF9FTEVNRU5UID0gJ3NwYW4nO1xuZXhwb3J0IHZhciBERUZBVUxUX0lOTElORV9TVFlMRSA9IERlZmF1bHREcmFmdElubGluZVN0eWxlO1xuZnVuY3Rpb24gZW5jb2RlQ29udGVudCh0ZXh0KSB7XG4gICAgcmV0dXJuIHRleHQuc3BsaXQoJyYnKS5qb2luKCcmYW1wOycpLnNwbGl0KCc8Jykuam9pbignJmx0OycpLnNwbGl0KCc+Jykuam9pbignJmd0OycpLnNwbGl0KCdcXHhBMCcpLmpvaW4oJyZuYnNwOycpLnNwbGl0KCdcXG4nKS5qb2luKCc8YnIgPicgKyAnXFxuJyk7XG59XG5mdW5jdGlvbiBlbmNvZGVBdHRyKHRleHQpIHtcbiAgICByZXR1cm4gdGV4dC5zcGxpdCgnJicpLmpvaW4oJyZhbXA7Jykuc3BsaXQoJzwnKS5qb2luKCcmbHQ7Jykuc3BsaXQoJz4nKS5qb2luKCcmZ3Q7Jykuc3BsaXQoJ1wiJykuam9pbignJnF1b3Q7Jyk7XG59XG52YXIgVkVORE9SX1BSRUZJWCA9IC9eKG1venxtc3xvfHdlYmtpdCktLztcbnZhciBOVU1FUklDX1NUUklORyA9IC9eXFxkKyQvO1xudmFyIFVQUEVSQ0FTRV9QQVRURVJOID0gLyhbQS1aXSkvZztcbi8vIExpZnRlZCBmcm9tOiBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svcmVhY3QvYmxvYi9tYXN0ZXIvc3JjL3JlbmRlcmVycy9kb20vc2hhcmVkL0NTU1Byb3BlcnR5T3BlcmF0aW9ucy5qc1xuZnVuY3Rpb24gcHJvY2Vzc1N0eWxlTmFtZShuYW1lKSB7XG4gICAgcmV0dXJuIG5hbWUucmVwbGFjZShVUFBFUkNBU0VfUEFUVEVSTiwgJy0kMScpLnRvTG93ZXJDYXNlKCkucmVwbGFjZShWRU5ET1JfUFJFRklYLCAnLSQxLScpO1xufVxuLy8gTGlmdGVkIGZyb206IGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC9ibG9iL21hc3Rlci9zcmMvcmVuZGVyZXJzL2RvbS9zaGFyZWQvZGFuZ2Vyb3VzU3R5bGVWYWx1ZS5qc1xuZnVuY3Rpb24gcHJvY2Vzc1N0eWxlVmFsdWUobmFtZSwgdmFsdWUpIHtcbiAgICB2YXIgaXNOdW1lcmljID0gdm9pZCAwO1xuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIGlzTnVtZXJpYyA9IE5VTUVSSUNfU1RSSU5HLnRlc3QodmFsdWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGlzTnVtZXJpYyA9IHRydWU7XG4gICAgICAgIHZhbHVlID0gU3RyaW5nKHZhbHVlKTtcbiAgICB9XG4gICAgaWYgKCFpc051bWVyaWMgfHwgdmFsdWUgPT09ICcwJyB8fCBpc1VuaXRsZXNzTnVtYmVyW25hbWVdID09PSB0cnVlKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdmFsdWUgKyAncHgnO1xuICAgIH1cbn1cbmZ1bmN0aW9uIGdldFN0eWxlVGV4dChzdHlsZU9iamVjdCkge1xuICAgIGlmICghc3R5bGVPYmplY3QpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgIH1cbiAgICByZXR1cm4gT2JqZWN0LmtleXMoc3R5bGVPYmplY3QpLm1hcChmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICB2YXIgc3R5bGVOYW1lID0gcHJvY2Vzc1N0eWxlTmFtZShuYW1lKTtcbiAgICAgICAgdmFyIHN0eWxlVmFsdWUgPSBwcm9jZXNzU3R5bGVWYWx1ZShuYW1lLCBzdHlsZU9iamVjdFtuYW1lXSk7XG4gICAgICAgIHJldHVybiBzdHlsZU5hbWUgKyAnOicgKyBzdHlsZVZhbHVlO1xuICAgIH0pLmpvaW4oJzsnKTtcbn1cbmZ1bmN0aW9uIGdldEVudGl0eUNvbnRlbnQoY29udGVudFN0YXRlLCBlbnRpdHlLZXksIGNvbnRlbnQpIHtcbiAgICBpZiAoZW50aXR5S2V5KSB7XG4gICAgICAgIHZhciBlbnRpdHkgPSBjb250ZW50U3RhdGUuZ2V0RW50aXR5KGVudGl0eUtleSk7XG4gICAgICAgIHZhciBlbnRpdHlEYXRhID0gZW50aXR5LmdldERhdGEoKTtcbiAgICAgICAgaWYgKGVudGl0eURhdGEgJiYgZW50aXR5RGF0YVsnZXhwb3J0J10pIHtcbiAgICAgICAgICAgIHJldHVybiBlbnRpdHlEYXRhWydleHBvcnQnXShjb250ZW50LCBlbnRpdHlEYXRhKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gY29udGVudDtcbn1cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEdldEhUTUwoY29uZmlnU3RvcmUpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gZXhwb3J0SHRtbChlZGl0b3JTdGF0ZSkge1xuICAgICAgICB2YXIgY29udGVudFN0YXRlID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcbiAgICAgICAgdmFyIGJsb2NrTWFwID0gY29udGVudFN0YXRlLmdldEJsb2NrTWFwKCk7XG4gICAgICAgIHZhciBjdXN0b21TdHlsZU1hcCA9IGNvbmZpZ1N0b3JlLmdldCgnY3VzdG9tU3R5bGVNYXAnKSB8fCB7fTtcbiAgICAgICAgdmFyIGN1c3RvbUJsb2NrUmVuZGVyTWFwID0gY29uZmlnU3RvcmUuZ2V0KCdibG9ja1JlbmRlck1hcCcpIHx8IHt9O1xuICAgICAgICB2YXIgY3VzdG9tU3R5bGVGbiA9IGNvbmZpZ1N0b3JlLmdldCgnY3VzdG9tU3R5bGVGbicpO1xuICAgICAgICB2YXIgdG9IVE1MTGlzdCA9IGNvbmZpZ1N0b3JlLmdldCgndG9IVE1MTGlzdCcpO1xuICAgICAgICBfZXh0ZW5kcyhjdXN0b21TdHlsZU1hcCwgREVGQVVMVF9JTkxJTkVfU1RZTEUpO1xuICAgICAgICByZXR1cm4gYmxvY2tNYXAubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgICAgICAgdmFyIHJlc3VsdFRleHQgPSAnPGRpdj4nO1xuICAgICAgICAgICAgdmFyIGNsb3NlVGFnID0gJzwvZGl2Pic7XG4gICAgICAgICAgICB2YXIgbGFzdFBvc2l0aW9uID0gMDtcbiAgICAgICAgICAgIHZhciB0ZXh0ID0gYmxvY2suZ2V0VGV4dCgpO1xuICAgICAgICAgICAgdmFyIGJsb2NrVHlwZSA9IGJsb2NrLmdldFR5cGUoKTtcbiAgICAgICAgICAgIHZhciBibG9ja1JlbmRlciA9IGN1c3RvbUJsb2NrUmVuZGVyTWFwLmdldChibG9ja1R5cGUpO1xuICAgICAgICAgICAgaWYgKGJsb2NrUmVuZGVyKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQgPSB0eXBlb2YgYmxvY2tSZW5kZXIuZWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJsb2NrUmVuZGVyLmVsZW1lbnRUYWcgfHwgJ2RpdicgOiAnZGl2JztcbiAgICAgICAgICAgICAgICByZXN1bHRUZXh0ID0gJzwnICsgKGVsZW1lbnQgfHwgJ2RpdicpICsgJyBzdHlsZT1cIicgKyBnZXRTdHlsZVRleHQoY3VzdG9tQmxvY2tSZW5kZXJNYXAuZ2V0KGJsb2NrVHlwZSkuc3R5bGUgfHwge30pICsgJ1wiPic7XG4gICAgICAgICAgICAgICAgY2xvc2VUYWcgPSAnPC8nICsgKGVsZW1lbnQgfHwgJ2RpdicpICsgJz4nO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGNoYXJNZXRhTGlzdCA9IGJsb2NrLmdldENoYXJhY3Rlckxpc3QoKTtcbiAgICAgICAgICAgIHZhciBjaGFyRW50aXR5ID0gbnVsbDtcbiAgICAgICAgICAgIHZhciBwcmV2Q2hhckVudGl0eSA9IG51bGw7XG4gICAgICAgICAgICB2YXIgcmFuZ2VzID0gW107XG4gICAgICAgICAgICB2YXIgcmFuZ2VTdGFydCA9IDA7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gdGV4dC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgICAgICAgIHByZXZDaGFyRW50aXR5ID0gY2hhckVudGl0eTtcbiAgICAgICAgICAgICAgICB2YXIgbWV0YSA9IGNoYXJNZXRhTGlzdC5nZXQoaSk7XG4gICAgICAgICAgICAgICAgY2hhckVudGl0eSA9IG1ldGEgPyBtZXRhLmdldEVudGl0eSgpIDogbnVsbDtcbiAgICAgICAgICAgICAgICBpZiAoaSA+IDAgJiYgY2hhckVudGl0eSAhPT0gcHJldkNoYXJFbnRpdHkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmFuZ2VzLnB1c2goW3ByZXZDaGFyRW50aXR5LCBnZXRTdHlsZVJhbmdlcyh0ZXh0LnNsaWNlKHJhbmdlU3RhcnQsIGkpLCBjaGFyTWV0YUxpc3Quc2xpY2UocmFuZ2VTdGFydCwgaSkpXSk7XG4gICAgICAgICAgICAgICAgICAgIHJhbmdlU3RhcnQgPSBpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJhbmdlcy5wdXNoKFtjaGFyRW50aXR5LCBnZXRTdHlsZVJhbmdlcyh0ZXh0LnNsaWNlKHJhbmdlU3RhcnQpLCBjaGFyTWV0YUxpc3Quc2xpY2UocmFuZ2VTdGFydCkpXSk7XG4gICAgICAgICAgICByYW5nZXMubWFwKGZ1bmN0aW9uIChfcmVmKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVudGl0eUtleSA9IF9yZWZbMF0sXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlUGllY2VzID0gX3JlZlsxXTtcblxuICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gREVGQVVMVF9FTEVNRU5UO1xuICAgICAgICAgICAgICAgIHZhciByYXdDb250ZW50ID0gc3R5bGVQaWVjZXMubWFwKGZ1bmN0aW9uIChfcmVmMikge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdGV4dCA9IF9yZWYyWzBdO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGV4dDtcbiAgICAgICAgICAgICAgICB9KS5qb2luKCcnKTtcbiAgICAgICAgICAgICAgICB2YXIgY29udGVudCA9IHN0eWxlUGllY2VzLm1hcChmdW5jdGlvbiAoX3JlZjMpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRleHQgPSBfcmVmM1swXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlU2V0ID0gX3JlZjNbMV07XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIGVuY29kZWRDb250ZW50ID0gZW5jb2RlQ29udGVudCh0ZXh0KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHN0eWxlU2V0LnNpemUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbmxpbmVTdHlsZSA9IHt9O1xuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGVTZXQuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjdXN0b21TdHlsZU1hcC5oYXNPd25Qcm9wZXJ0eShpdGVtKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3VycmVudFN0eWxlID0gY3VzdG9tU3R5bGVNYXBbaXRlbV07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlubGluZVN0eWxlID0gX2V4dGVuZHMoaW5saW5lU3R5bGUsIGN1cnJlbnRTdHlsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY3VzdG9tZWRTdHlsZSA9IGN1c3RvbVN0eWxlRm4oc3R5bGVTZXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaW5saW5lU3R5bGUgPSBfZXh0ZW5kcyhpbmxpbmVTdHlsZSwgY3VzdG9tZWRTdHlsZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJzxzcGFuIHN0eWxlPVwiJyArIGdldFN0eWxlVGV4dChpbmxpbmVTdHlsZSkgKyAnXCI+JyArIGVuY29kZWRDb250ZW50ICsgJzwvc3Bhbj4nO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnPHNwYW4+JyArIGVuY29kZWRDb250ZW50ICsgJzwvc3Bhbj4nO1xuICAgICAgICAgICAgICAgIH0pLmpvaW4oJycpO1xuICAgICAgICAgICAgICAgIGlmIChlbnRpdHlLZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVudGl0eSA9IGNvbnRlbnRTdGF0ZS5nZXRFbnRpdHkoZW50aXR5S2V5KTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGVudGl0eURhdGEgPSBlbnRpdHkuZ2V0RGF0YSgpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZW50aXR5RGF0YSAmJiBlbnRpdHlEYXRhWydleHBvcnQnXSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0VGV4dCArPSBlbnRpdHlEYXRhWydleHBvcnQnXShjb250ZW50LCBlbnRpdHlEYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBIVE1MVGV4dCA9ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgdG9IVE1MTGlzdC5mb3JFYWNoKGZ1bmN0aW9uICh0b0hUTUwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGV4dCA9IHRvSFRNTChyYXdDb250ZW50LCBlbnRpdHksIGNvbnRlbnRTdGF0ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRleHQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgSFRNTFRleHQgPSB0ZXh0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEhUTUxUZXh0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0VGV4dCArPSBIVE1MVGV4dDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdFRleHQgKz0gY29udGVudDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJlc3VsdFRleHQgKz0gY2xvc2VUYWc7XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0VGV4dDtcbiAgICAgICAgfSkuam9pbignXFxuJyk7XG4gICAgfTtcbn1cbmZ1bmN0aW9uIGdldFN0eWxlUmFuZ2VzKHRleHQsIGNoYXJNZXRhTGlzdCkge1xuICAgIHZhciBjaGFyU3R5bGUgPSBFTVBUWV9TRVQ7XG4gICAgdmFyIHByZXZDaGFyU3R5bGUgPSBFTVBUWV9TRVQ7XG4gICAgdmFyIHJhbmdlcyA9IFtdO1xuICAgIHZhciByYW5nZVN0YXJ0ID0gMDtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gdGV4dC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBwcmV2Q2hhclN0eWxlID0gY2hhclN0eWxlO1xuICAgICAgICB2YXIgbWV0YSA9IGNoYXJNZXRhTGlzdC5nZXQoaSk7XG4gICAgICAgIGNoYXJTdHlsZSA9IG1ldGEgPyBtZXRhLmdldFN0eWxlKCkgOiBFTVBUWV9TRVQ7XG4gICAgICAgIGlmIChpID4gMCAmJiAhaXMoY2hhclN0eWxlLCBwcmV2Q2hhclN0eWxlKSkge1xuICAgICAgICAgICAgcmFuZ2VzLnB1c2goW3RleHQuc2xpY2UocmFuZ2VTdGFydCwgaSksIHByZXZDaGFyU3R5bGVdKTtcbiAgICAgICAgICAgIHJhbmdlU3RhcnQgPSBpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJhbmdlcy5wdXNoKFt0ZXh0LnNsaWNlKHJhbmdlU3RhcnQpLCBjaGFyU3R5bGVdKTtcbiAgICByZXR1cm4gcmFuZ2VzO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/EditorCore/export/getHTML.js
