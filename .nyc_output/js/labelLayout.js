/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var textContain = __webpack_require__(/*! zrender/lib/contain/text */ "./node_modules/zrender/lib/contain/text.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// FIXME emphasis label position is not same with normal label position


function adjustSingleSide(list, cx, cy, r, dir, viewWidth, viewHeight) {
  list.sort(function (a, b) {
    return a.y - b.y;
  });

  function shiftDown(start, end, delta, dir) {
    for (var j = start; j < end; j++) {
      list[j].y += delta;

      if (j > start && j + 1 < end && list[j + 1].y > list[j].y + list[j].height) {
        shiftUp(j, delta / 2);
        return;
      }
    }

    shiftUp(end - 1, delta / 2);
  }

  function shiftUp(end, delta) {
    for (var j = end; j >= 0; j--) {
      list[j].y -= delta;

      if (j > 0 && list[j].y > list[j - 1].y + list[j - 1].height) {
        break;
      }
    }
  }

  function changeX(list, isDownList, cx, cy, r, dir) {
    var lastDeltaX = dir > 0 ? isDownList // right-side
    ? Number.MAX_VALUE // down
    : 0 // up
    : isDownList // left-side
    ? Number.MAX_VALUE // down
    : 0; // up

    for (var i = 0, l = list.length; i < l; i++) {
      var deltaY = Math.abs(list[i].y - cy);
      var length = list[i].len;
      var length2 = list[i].len2;
      var deltaX = deltaY < r + length ? Math.sqrt((r + length + length2) * (r + length + length2) - deltaY * deltaY) : Math.abs(list[i].x - cx);

      if (isDownList && deltaX >= lastDeltaX) {
        // right-down, left-down
        deltaX = lastDeltaX - 10;
      }

      if (!isDownList && deltaX <= lastDeltaX) {
        // right-up, left-up
        deltaX = lastDeltaX + 10;
      }

      list[i].x = cx + deltaX * dir;
      lastDeltaX = deltaX;
    }
  }

  var lastY = 0;
  var delta;
  var len = list.length;
  var upList = [];
  var downList = [];

  for (var i = 0; i < len; i++) {
    delta = list[i].y - lastY;

    if (delta < 0) {
      shiftDown(i, len, -delta, dir);
    }

    lastY = list[i].y + list[i].height;
  }

  if (viewHeight - lastY < 0) {
    shiftUp(len - 1, lastY - viewHeight);
  }

  for (var i = 0; i < len; i++) {
    if (list[i].y >= cy) {
      downList.push(list[i]);
    } else {
      upList.push(list[i]);
    }
  }

  changeX(upList, false, cx, cy, r, dir);
  changeX(downList, true, cx, cy, r, dir);
}

function avoidOverlap(labelLayoutList, cx, cy, r, viewWidth, viewHeight) {
  var leftList = [];
  var rightList = [];

  for (var i = 0; i < labelLayoutList.length; i++) {
    if (isPositionCenter(labelLayoutList[i])) {
      continue;
    }

    if (labelLayoutList[i].x < cx) {
      leftList.push(labelLayoutList[i]);
    } else {
      rightList.push(labelLayoutList[i]);
    }
  }

  adjustSingleSide(rightList, cx, cy, r, 1, viewWidth, viewHeight);
  adjustSingleSide(leftList, cx, cy, r, -1, viewWidth, viewHeight);

  for (var i = 0; i < labelLayoutList.length; i++) {
    if (isPositionCenter(labelLayoutList[i])) {
      continue;
    }

    var linePoints = labelLayoutList[i].linePoints;

    if (linePoints) {
      var dist = linePoints[1][0] - linePoints[2][0];

      if (labelLayoutList[i].x < cx) {
        linePoints[2][0] = labelLayoutList[i].x + 3;
      } else {
        linePoints[2][0] = labelLayoutList[i].x - 3;
      }

      linePoints[1][1] = linePoints[2][1] = labelLayoutList[i].y;
      linePoints[1][0] = linePoints[2][0] + dist;
    }
  }
}

function isPositionCenter(layout) {
  // Not change x for center label
  return layout.position === 'center';
}

function _default(seriesModel, r, viewWidth, viewHeight) {
  var data = seriesModel.getData();
  var labelLayoutList = [];
  var cx;
  var cy;
  var hasLabelRotate = false;
  data.each(function (idx) {
    var layout = data.getItemLayout(idx);
    var itemModel = data.getItemModel(idx);
    var labelModel = itemModel.getModel('label'); // Use position in normal or emphasis

    var labelPosition = labelModel.get('position') || itemModel.get('emphasis.label.position');
    var labelLineModel = itemModel.getModel('labelLine');
    var labelLineLen = labelLineModel.get('length');
    var labelLineLen2 = labelLineModel.get('length2');
    var midAngle = (layout.startAngle + layout.endAngle) / 2;
    var dx = Math.cos(midAngle);
    var dy = Math.sin(midAngle);
    var textX;
    var textY;
    var linePoints;
    var textAlign;
    cx = layout.cx;
    cy = layout.cy;
    var isLabelInside = labelPosition === 'inside' || labelPosition === 'inner';

    if (labelPosition === 'center') {
      textX = layout.cx;
      textY = layout.cy;
      textAlign = 'center';
    } else {
      var x1 = (isLabelInside ? (layout.r + layout.r0) / 2 * dx : layout.r * dx) + cx;
      var y1 = (isLabelInside ? (layout.r + layout.r0) / 2 * dy : layout.r * dy) + cy;
      textX = x1 + dx * 3;
      textY = y1 + dy * 3;

      if (!isLabelInside) {
        // For roseType
        var x2 = x1 + dx * (labelLineLen + r - layout.r);
        var y2 = y1 + dy * (labelLineLen + r - layout.r);
        var x3 = x2 + (dx < 0 ? -1 : 1) * labelLineLen2;
        var y3 = y2;
        textX = x3 + (dx < 0 ? -5 : 5);
        textY = y3;
        linePoints = [[x1, y1], [x2, y2], [x3, y3]];
      }

      textAlign = isLabelInside ? 'center' : dx > 0 ? 'left' : 'right';
    }

    var font = labelModel.getFont();
    var labelRotate = labelModel.get('rotate') ? dx < 0 ? -midAngle + Math.PI : -midAngle : 0;
    var text = seriesModel.getFormattedLabel(idx, 'normal') || data.getName(idx);
    var textRect = textContain.getBoundingRect(text, font, textAlign, 'top');
    hasLabelRotate = !!labelRotate;
    layout.label = {
      x: textX,
      y: textY,
      position: labelPosition,
      height: textRect.height,
      len: labelLineLen,
      len2: labelLineLen2,
      linePoints: linePoints,
      textAlign: textAlign,
      verticalAlign: 'middle',
      rotation: labelRotate,
      inside: isLabelInside
    }; // Not layout the inside label

    if (!isLabelInside) {
      labelLayoutList.push(layout.label);
    }
  });

  if (!hasLabelRotate && seriesModel.get('avoidLabelOverlap')) {
    avoidOverlap(labelLayoutList, cx, cy, r, viewWidth, viewHeight);
  }
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvcGllL2xhYmVsTGF5b3V0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvcGllL2xhYmVsTGF5b3V0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgdGV4dENvbnRhaW4gPSByZXF1aXJlKFwienJlbmRlci9saWIvY29udGFpbi90ZXh0XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG4vLyBGSVhNRSBlbXBoYXNpcyBsYWJlbCBwb3NpdGlvbiBpcyBub3Qgc2FtZSB3aXRoIG5vcm1hbCBsYWJlbCBwb3NpdGlvblxuZnVuY3Rpb24gYWRqdXN0U2luZ2xlU2lkZShsaXN0LCBjeCwgY3ksIHIsIGRpciwgdmlld1dpZHRoLCB2aWV3SGVpZ2h0KSB7XG4gIGxpc3Quc29ydChmdW5jdGlvbiAoYSwgYikge1xuICAgIHJldHVybiBhLnkgLSBiLnk7XG4gIH0pO1xuXG4gIGZ1bmN0aW9uIHNoaWZ0RG93bihzdGFydCwgZW5kLCBkZWx0YSwgZGlyKSB7XG4gICAgZm9yICh2YXIgaiA9IHN0YXJ0OyBqIDwgZW5kOyBqKyspIHtcbiAgICAgIGxpc3Rbal0ueSArPSBkZWx0YTtcblxuICAgICAgaWYgKGogPiBzdGFydCAmJiBqICsgMSA8IGVuZCAmJiBsaXN0W2ogKyAxXS55ID4gbGlzdFtqXS55ICsgbGlzdFtqXS5oZWlnaHQpIHtcbiAgICAgICAgc2hpZnRVcChqLCBkZWx0YSAvIDIpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuXG4gICAgc2hpZnRVcChlbmQgLSAxLCBkZWx0YSAvIDIpO1xuICB9XG5cbiAgZnVuY3Rpb24gc2hpZnRVcChlbmQsIGRlbHRhKSB7XG4gICAgZm9yICh2YXIgaiA9IGVuZDsgaiA+PSAwOyBqLS0pIHtcbiAgICAgIGxpc3Rbal0ueSAtPSBkZWx0YTtcblxuICAgICAgaWYgKGogPiAwICYmIGxpc3Rbal0ueSA+IGxpc3RbaiAtIDFdLnkgKyBsaXN0W2ogLSAxXS5oZWlnaHQpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gY2hhbmdlWChsaXN0LCBpc0Rvd25MaXN0LCBjeCwgY3ksIHIsIGRpcikge1xuICAgIHZhciBsYXN0RGVsdGFYID0gZGlyID4gMCA/IGlzRG93bkxpc3QgLy8gcmlnaHQtc2lkZVxuICAgID8gTnVtYmVyLk1BWF9WQUxVRSAvLyBkb3duXG4gICAgOiAwIC8vIHVwXG4gICAgOiBpc0Rvd25MaXN0IC8vIGxlZnQtc2lkZVxuICAgID8gTnVtYmVyLk1BWF9WQUxVRSAvLyBkb3duXG4gICAgOiAwOyAvLyB1cFxuXG4gICAgZm9yICh2YXIgaSA9IDAsIGwgPSBsaXN0Lmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgdmFyIGRlbHRhWSA9IE1hdGguYWJzKGxpc3RbaV0ueSAtIGN5KTtcbiAgICAgIHZhciBsZW5ndGggPSBsaXN0W2ldLmxlbjtcbiAgICAgIHZhciBsZW5ndGgyID0gbGlzdFtpXS5sZW4yO1xuICAgICAgdmFyIGRlbHRhWCA9IGRlbHRhWSA8IHIgKyBsZW5ndGggPyBNYXRoLnNxcnQoKHIgKyBsZW5ndGggKyBsZW5ndGgyKSAqIChyICsgbGVuZ3RoICsgbGVuZ3RoMikgLSBkZWx0YVkgKiBkZWx0YVkpIDogTWF0aC5hYnMobGlzdFtpXS54IC0gY3gpO1xuXG4gICAgICBpZiAoaXNEb3duTGlzdCAmJiBkZWx0YVggPj0gbGFzdERlbHRhWCkge1xuICAgICAgICAvLyByaWdodC1kb3duLCBsZWZ0LWRvd25cbiAgICAgICAgZGVsdGFYID0gbGFzdERlbHRhWCAtIDEwO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWlzRG93bkxpc3QgJiYgZGVsdGFYIDw9IGxhc3REZWx0YVgpIHtcbiAgICAgICAgLy8gcmlnaHQtdXAsIGxlZnQtdXBcbiAgICAgICAgZGVsdGFYID0gbGFzdERlbHRhWCArIDEwO1xuICAgICAgfVxuXG4gICAgICBsaXN0W2ldLnggPSBjeCArIGRlbHRhWCAqIGRpcjtcbiAgICAgIGxhc3REZWx0YVggPSBkZWx0YVg7XG4gICAgfVxuICB9XG5cbiAgdmFyIGxhc3RZID0gMDtcbiAgdmFyIGRlbHRhO1xuICB2YXIgbGVuID0gbGlzdC5sZW5ndGg7XG4gIHZhciB1cExpc3QgPSBbXTtcbiAgdmFyIGRvd25MaXN0ID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgIGRlbHRhID0gbGlzdFtpXS55IC0gbGFzdFk7XG5cbiAgICBpZiAoZGVsdGEgPCAwKSB7XG4gICAgICBzaGlmdERvd24oaSwgbGVuLCAtZGVsdGEsIGRpcik7XG4gICAgfVxuXG4gICAgbGFzdFkgPSBsaXN0W2ldLnkgKyBsaXN0W2ldLmhlaWdodDtcbiAgfVxuXG4gIGlmICh2aWV3SGVpZ2h0IC0gbGFzdFkgPCAwKSB7XG4gICAgc2hpZnRVcChsZW4gLSAxLCBsYXN0WSAtIHZpZXdIZWlnaHQpO1xuICB9XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgIGlmIChsaXN0W2ldLnkgPj0gY3kpIHtcbiAgICAgIGRvd25MaXN0LnB1c2gobGlzdFtpXSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHVwTGlzdC5wdXNoKGxpc3RbaV0pO1xuICAgIH1cbiAgfVxuXG4gIGNoYW5nZVgodXBMaXN0LCBmYWxzZSwgY3gsIGN5LCByLCBkaXIpO1xuICBjaGFuZ2VYKGRvd25MaXN0LCB0cnVlLCBjeCwgY3ksIHIsIGRpcik7XG59XG5cbmZ1bmN0aW9uIGF2b2lkT3ZlcmxhcChsYWJlbExheW91dExpc3QsIGN4LCBjeSwgciwgdmlld1dpZHRoLCB2aWV3SGVpZ2h0KSB7XG4gIHZhciBsZWZ0TGlzdCA9IFtdO1xuICB2YXIgcmlnaHRMaXN0ID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsYWJlbExheW91dExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoaXNQb3NpdGlvbkNlbnRlcihsYWJlbExheW91dExpc3RbaV0pKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICBpZiAobGFiZWxMYXlvdXRMaXN0W2ldLnggPCBjeCkge1xuICAgICAgbGVmdExpc3QucHVzaChsYWJlbExheW91dExpc3RbaV0pO1xuICAgIH0gZWxzZSB7XG4gICAgICByaWdodExpc3QucHVzaChsYWJlbExheW91dExpc3RbaV0pO1xuICAgIH1cbiAgfVxuXG4gIGFkanVzdFNpbmdsZVNpZGUocmlnaHRMaXN0LCBjeCwgY3ksIHIsIDEsIHZpZXdXaWR0aCwgdmlld0hlaWdodCk7XG4gIGFkanVzdFNpbmdsZVNpZGUobGVmdExpc3QsIGN4LCBjeSwgciwgLTEsIHZpZXdXaWR0aCwgdmlld0hlaWdodCk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsYWJlbExheW91dExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoaXNQb3NpdGlvbkNlbnRlcihsYWJlbExheW91dExpc3RbaV0pKSB7XG4gICAgICBjb250aW51ZTtcbiAgICB9XG5cbiAgICB2YXIgbGluZVBvaW50cyA9IGxhYmVsTGF5b3V0TGlzdFtpXS5saW5lUG9pbnRzO1xuXG4gICAgaWYgKGxpbmVQb2ludHMpIHtcbiAgICAgIHZhciBkaXN0ID0gbGluZVBvaW50c1sxXVswXSAtIGxpbmVQb2ludHNbMl1bMF07XG5cbiAgICAgIGlmIChsYWJlbExheW91dExpc3RbaV0ueCA8IGN4KSB7XG4gICAgICAgIGxpbmVQb2ludHNbMl1bMF0gPSBsYWJlbExheW91dExpc3RbaV0ueCArIDM7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsaW5lUG9pbnRzWzJdWzBdID0gbGFiZWxMYXlvdXRMaXN0W2ldLnggLSAzO1xuICAgICAgfVxuXG4gICAgICBsaW5lUG9pbnRzWzFdWzFdID0gbGluZVBvaW50c1syXVsxXSA9IGxhYmVsTGF5b3V0TGlzdFtpXS55O1xuICAgICAgbGluZVBvaW50c1sxXVswXSA9IGxpbmVQb2ludHNbMl1bMF0gKyBkaXN0O1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBpc1Bvc2l0aW9uQ2VudGVyKGxheW91dCkge1xuICAvLyBOb3QgY2hhbmdlIHggZm9yIGNlbnRlciBsYWJlbFxuICByZXR1cm4gbGF5b3V0LnBvc2l0aW9uID09PSAnY2VudGVyJztcbn1cblxuZnVuY3Rpb24gX2RlZmF1bHQoc2VyaWVzTW9kZWwsIHIsIHZpZXdXaWR0aCwgdmlld0hlaWdodCkge1xuICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgdmFyIGxhYmVsTGF5b3V0TGlzdCA9IFtdO1xuICB2YXIgY3g7XG4gIHZhciBjeTtcbiAgdmFyIGhhc0xhYmVsUm90YXRlID0gZmFsc2U7XG4gIGRhdGEuZWFjaChmdW5jdGlvbiAoaWR4KSB7XG4gICAgdmFyIGxheW91dCA9IGRhdGEuZ2V0SXRlbUxheW91dChpZHgpO1xuICAgIHZhciBpdGVtTW9kZWwgPSBkYXRhLmdldEl0ZW1Nb2RlbChpZHgpO1xuICAgIHZhciBsYWJlbE1vZGVsID0gaXRlbU1vZGVsLmdldE1vZGVsKCdsYWJlbCcpOyAvLyBVc2UgcG9zaXRpb24gaW4gbm9ybWFsIG9yIGVtcGhhc2lzXG5cbiAgICB2YXIgbGFiZWxQb3NpdGlvbiA9IGxhYmVsTW9kZWwuZ2V0KCdwb3NpdGlvbicpIHx8IGl0ZW1Nb2RlbC5nZXQoJ2VtcGhhc2lzLmxhYmVsLnBvc2l0aW9uJyk7XG4gICAgdmFyIGxhYmVsTGluZU1vZGVsID0gaXRlbU1vZGVsLmdldE1vZGVsKCdsYWJlbExpbmUnKTtcbiAgICB2YXIgbGFiZWxMaW5lTGVuID0gbGFiZWxMaW5lTW9kZWwuZ2V0KCdsZW5ndGgnKTtcbiAgICB2YXIgbGFiZWxMaW5lTGVuMiA9IGxhYmVsTGluZU1vZGVsLmdldCgnbGVuZ3RoMicpO1xuICAgIHZhciBtaWRBbmdsZSA9IChsYXlvdXQuc3RhcnRBbmdsZSArIGxheW91dC5lbmRBbmdsZSkgLyAyO1xuICAgIHZhciBkeCA9IE1hdGguY29zKG1pZEFuZ2xlKTtcbiAgICB2YXIgZHkgPSBNYXRoLnNpbihtaWRBbmdsZSk7XG4gICAgdmFyIHRleHRYO1xuICAgIHZhciB0ZXh0WTtcbiAgICB2YXIgbGluZVBvaW50cztcbiAgICB2YXIgdGV4dEFsaWduO1xuICAgIGN4ID0gbGF5b3V0LmN4O1xuICAgIGN5ID0gbGF5b3V0LmN5O1xuICAgIHZhciBpc0xhYmVsSW5zaWRlID0gbGFiZWxQb3NpdGlvbiA9PT0gJ2luc2lkZScgfHwgbGFiZWxQb3NpdGlvbiA9PT0gJ2lubmVyJztcblxuICAgIGlmIChsYWJlbFBvc2l0aW9uID09PSAnY2VudGVyJykge1xuICAgICAgdGV4dFggPSBsYXlvdXQuY3g7XG4gICAgICB0ZXh0WSA9IGxheW91dC5jeTtcbiAgICAgIHRleHRBbGlnbiA9ICdjZW50ZXInO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgeDEgPSAoaXNMYWJlbEluc2lkZSA/IChsYXlvdXQuciArIGxheW91dC5yMCkgLyAyICogZHggOiBsYXlvdXQuciAqIGR4KSArIGN4O1xuICAgICAgdmFyIHkxID0gKGlzTGFiZWxJbnNpZGUgPyAobGF5b3V0LnIgKyBsYXlvdXQucjApIC8gMiAqIGR5IDogbGF5b3V0LnIgKiBkeSkgKyBjeTtcbiAgICAgIHRleHRYID0geDEgKyBkeCAqIDM7XG4gICAgICB0ZXh0WSA9IHkxICsgZHkgKiAzO1xuXG4gICAgICBpZiAoIWlzTGFiZWxJbnNpZGUpIHtcbiAgICAgICAgLy8gRm9yIHJvc2VUeXBlXG4gICAgICAgIHZhciB4MiA9IHgxICsgZHggKiAobGFiZWxMaW5lTGVuICsgciAtIGxheW91dC5yKTtcbiAgICAgICAgdmFyIHkyID0geTEgKyBkeSAqIChsYWJlbExpbmVMZW4gKyByIC0gbGF5b3V0LnIpO1xuICAgICAgICB2YXIgeDMgPSB4MiArIChkeCA8IDAgPyAtMSA6IDEpICogbGFiZWxMaW5lTGVuMjtcbiAgICAgICAgdmFyIHkzID0geTI7XG4gICAgICAgIHRleHRYID0geDMgKyAoZHggPCAwID8gLTUgOiA1KTtcbiAgICAgICAgdGV4dFkgPSB5MztcbiAgICAgICAgbGluZVBvaW50cyA9IFtbeDEsIHkxXSwgW3gyLCB5Ml0sIFt4MywgeTNdXTtcbiAgICAgIH1cblxuICAgICAgdGV4dEFsaWduID0gaXNMYWJlbEluc2lkZSA/ICdjZW50ZXInIDogZHggPiAwID8gJ2xlZnQnIDogJ3JpZ2h0JztcbiAgICB9XG5cbiAgICB2YXIgZm9udCA9IGxhYmVsTW9kZWwuZ2V0Rm9udCgpO1xuICAgIHZhciBsYWJlbFJvdGF0ZSA9IGxhYmVsTW9kZWwuZ2V0KCdyb3RhdGUnKSA/IGR4IDwgMCA/IC1taWRBbmdsZSArIE1hdGguUEkgOiAtbWlkQW5nbGUgOiAwO1xuICAgIHZhciB0ZXh0ID0gc2VyaWVzTW9kZWwuZ2V0Rm9ybWF0dGVkTGFiZWwoaWR4LCAnbm9ybWFsJykgfHwgZGF0YS5nZXROYW1lKGlkeCk7XG4gICAgdmFyIHRleHRSZWN0ID0gdGV4dENvbnRhaW4uZ2V0Qm91bmRpbmdSZWN0KHRleHQsIGZvbnQsIHRleHRBbGlnbiwgJ3RvcCcpO1xuICAgIGhhc0xhYmVsUm90YXRlID0gISFsYWJlbFJvdGF0ZTtcbiAgICBsYXlvdXQubGFiZWwgPSB7XG4gICAgICB4OiB0ZXh0WCxcbiAgICAgIHk6IHRleHRZLFxuICAgICAgcG9zaXRpb246IGxhYmVsUG9zaXRpb24sXG4gICAgICBoZWlnaHQ6IHRleHRSZWN0LmhlaWdodCxcbiAgICAgIGxlbjogbGFiZWxMaW5lTGVuLFxuICAgICAgbGVuMjogbGFiZWxMaW5lTGVuMixcbiAgICAgIGxpbmVQb2ludHM6IGxpbmVQb2ludHMsXG4gICAgICB0ZXh0QWxpZ246IHRleHRBbGlnbixcbiAgICAgIHZlcnRpY2FsQWxpZ246ICdtaWRkbGUnLFxuICAgICAgcm90YXRpb246IGxhYmVsUm90YXRlLFxuICAgICAgaW5zaWRlOiBpc0xhYmVsSW5zaWRlXG4gICAgfTsgLy8gTm90IGxheW91dCB0aGUgaW5zaWRlIGxhYmVsXG5cbiAgICBpZiAoIWlzTGFiZWxJbnNpZGUpIHtcbiAgICAgIGxhYmVsTGF5b3V0TGlzdC5wdXNoKGxheW91dC5sYWJlbCk7XG4gICAgfVxuICB9KTtcblxuICBpZiAoIWhhc0xhYmVsUm90YXRlICYmIHNlcmllc01vZGVsLmdldCgnYXZvaWRMYWJlbE92ZXJsYXAnKSkge1xuICAgIGF2b2lkT3ZlcmxhcChsYWJlbExheW91dExpc3QsIGN4LCBjeSwgciwgdmlld1dpZHRoLCB2aWV3SGVpZ2h0KTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQUNBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/pie/labelLayout.js
