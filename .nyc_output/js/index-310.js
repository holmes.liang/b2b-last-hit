

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ConfigConsumer", {
  enumerable: true,
  get: function get() {
    return _context.ConfigConsumer;
  }
});
exports["default"] = exports.configConsumerProps = void 0;

var React = _interopRequireWildcard(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var _localeProvider = _interopRequireWildcard(__webpack_require__(/*! ../locale-provider */ "./node_modules/antd/lib/locale-provider/index.js"));

var _LocaleReceiver = _interopRequireDefault(__webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/lib/locale-provider/LocaleReceiver.js"));

var _context = __webpack_require__(/*! ./context */ "./node_modules/antd/lib/config-provider/context.js");

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var configConsumerProps = ['getPopupContainer', 'rootPrefixCls', 'getPrefixCls', 'renderEmpty', 'csp', 'autoInsertSpaceInButton', 'locale', 'pageHeader'];
exports.configConsumerProps = configConsumerProps;

var ConfigProvider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ConfigProvider, _React$Component);

  function ConfigProvider() {
    var _this;

    _classCallCheck(this, ConfigProvider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ConfigProvider).apply(this, arguments));

    _this.getPrefixCls = function (suffixCls, customizePrefixCls) {
      var _this$props$prefixCls = _this.props.prefixCls,
          prefixCls = _this$props$prefixCls === void 0 ? 'ant' : _this$props$prefixCls;
      if (customizePrefixCls) return customizePrefixCls;
      return suffixCls ? "".concat(prefixCls, "-").concat(suffixCls) : prefixCls;
    };

    _this.renderProvider = function (context, legacyLocale) {
      var _this$props = _this.props,
          children = _this$props.children,
          getPopupContainer = _this$props.getPopupContainer,
          renderEmpty = _this$props.renderEmpty,
          csp = _this$props.csp,
          autoInsertSpaceInButton = _this$props.autoInsertSpaceInButton,
          locale = _this$props.locale,
          pageHeader = _this$props.pageHeader;

      var config = _extends(_extends({}, context), {
        getPrefixCls: _this.getPrefixCls,
        csp: csp,
        autoInsertSpaceInButton: autoInsertSpaceInButton
      });

      if (getPopupContainer) {
        config.getPopupContainer = getPopupContainer;
      }

      if (renderEmpty) {
        config.renderEmpty = renderEmpty;
      }

      if (pageHeader) {
        config.pageHeader = pageHeader;
      }

      return React.createElement(_context.ConfigContext.Provider, {
        value: config
      }, React.createElement(_localeProvider["default"], {
        locale: locale || legacyLocale,
        _ANT_MARK__: _localeProvider.ANT_MARK
      }, children));
    };

    return _this;
  }

  _createClass(ConfigProvider, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return React.createElement(_LocaleReceiver["default"], null, function (_, __, legacyLocale) {
        return React.createElement(_context.ConfigConsumer, null, function (context) {
          return _this2.renderProvider(context, legacyLocale);
        });
      });
    }
  }]);

  return ConfigProvider;
}(React.Component);

var _default = ConfigProvider;
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvY29uZmlnLXByb3ZpZGVyL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9jb25maWctcHJvdmlkZXIvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIFRPRE86IHJlbW92ZSB0aGlzIGxpbnRcbi8vIFNGQyBoYXMgc3BlY2lmaWVkIGEgZGlzcGxheU5hbWUsIGJ1dCBub3Qgd29ya2VkLlxuLyogZXNsaW50LWRpc2FibGUgcmVhY3QvZGlzcGxheS1uYW1lICovXG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTG9jYWxlUHJvdmlkZXIsIHsgQU5UX01BUksgfSBmcm9tICcuLi9sb2NhbGUtcHJvdmlkZXInO1xuaW1wb3J0IExvY2FsZVJlY2VpdmVyIGZyb20gJy4uL2xvY2FsZS1wcm92aWRlci9Mb2NhbGVSZWNlaXZlcic7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciwgQ29uZmlnQ29udGV4dCB9IGZyb20gJy4vY29udGV4dCc7XG5leHBvcnQgeyBDb25maWdDb25zdW1lciB9O1xuZXhwb3J0IGNvbnN0IGNvbmZpZ0NvbnN1bWVyUHJvcHMgPSBbXG4gICAgJ2dldFBvcHVwQ29udGFpbmVyJyxcbiAgICAncm9vdFByZWZpeENscycsXG4gICAgJ2dldFByZWZpeENscycsXG4gICAgJ3JlbmRlckVtcHR5JyxcbiAgICAnY3NwJyxcbiAgICAnYXV0b0luc2VydFNwYWNlSW5CdXR0b24nLFxuICAgICdsb2NhbGUnLFxuICAgICdwYWdlSGVhZGVyJyxcbl07XG5jbGFzcyBDb25maWdQcm92aWRlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuZ2V0UHJlZml4Q2xzID0gKHN1ZmZpeENscywgY3VzdG9taXplUHJlZml4Q2xzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENscyA9ICdhbnQnIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGN1c3RvbWl6ZVByZWZpeENscylcbiAgICAgICAgICAgICAgICByZXR1cm4gY3VzdG9taXplUHJlZml4Q2xzO1xuICAgICAgICAgICAgcmV0dXJuIHN1ZmZpeENscyA/IGAke3ByZWZpeENsc30tJHtzdWZmaXhDbHN9YCA6IHByZWZpeENscztcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJQcm92aWRlciA9IChjb250ZXh0LCBsZWdhY3lMb2NhbGUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgY2hpbGRyZW4sIGdldFBvcHVwQ29udGFpbmVyLCByZW5kZXJFbXB0eSwgY3NwLCBhdXRvSW5zZXJ0U3BhY2VJbkJ1dHRvbiwgbG9jYWxlLCBwYWdlSGVhZGVyLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGNvbmZpZyA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgY29udGV4dCksIHsgZ2V0UHJlZml4Q2xzOiB0aGlzLmdldFByZWZpeENscywgY3NwLFxuICAgICAgICAgICAgICAgIGF1dG9JbnNlcnRTcGFjZUluQnV0dG9uIH0pO1xuICAgICAgICAgICAgaWYgKGdldFBvcHVwQ29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgY29uZmlnLmdldFBvcHVwQ29udGFpbmVyID0gZ2V0UG9wdXBDb250YWluZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocmVuZGVyRW1wdHkpIHtcbiAgICAgICAgICAgICAgICBjb25maWcucmVuZGVyRW1wdHkgPSByZW5kZXJFbXB0eTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChwYWdlSGVhZGVyKSB7XG4gICAgICAgICAgICAgICAgY29uZmlnLnBhZ2VIZWFkZXIgPSBwYWdlSGVhZGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICg8Q29uZmlnQ29udGV4dC5Qcm92aWRlciB2YWx1ZT17Y29uZmlnfT5cbiAgICAgICAgPExvY2FsZVByb3ZpZGVyIGxvY2FsZT17bG9jYWxlIHx8IGxlZ2FjeUxvY2FsZX0gX0FOVF9NQVJLX189e0FOVF9NQVJLfT5cbiAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgIDwvTG9jYWxlUHJvdmlkZXI+XG4gICAgICA8L0NvbmZpZ0NvbnRleHQuUHJvdmlkZXI+KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gKDxMb2NhbGVSZWNlaXZlcj5cbiAgICAgICAgeyhfLCBfXywgbGVnYWN5TG9jYWxlKSA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgICAgICAgICAge2NvbnRleHQgPT4gdGhpcy5yZW5kZXJQcm92aWRlcihjb250ZXh0LCBsZWdhY3lMb2NhbGUpfVxuICAgICAgICAgIDwvQ29uZmlnQ29uc3VtZXI+KX1cbiAgICAgIDwvTG9jYWxlUmVjZWl2ZXI+KTtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBDb25maWdQcm92aWRlcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7O0FBVUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBSkE7QUFDQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBZEE7QUFDQTtBQVRBO0FBMkJBO0FBQ0E7OztBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTs7OztBQW5DQTtBQUNBO0FBb0NBO0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/lib/config-provider/index.js
