var _util = __webpack_require__(/*! ../../core/util */ "./node_modules/zrender/lib/core/util.js");

var retrieve2 = _util.retrieve2;
var retrieve3 = _util.retrieve3;
var each = _util.each;
var normalizeCssArray = _util.normalizeCssArray;
var isString = _util.isString;
var isObject = _util.isObject;

var textContain = __webpack_require__(/*! ../../contain/text */ "./node_modules/zrender/lib/contain/text.js");

var roundRectHelper = __webpack_require__(/*! ./roundRect */ "./node_modules/zrender/lib/graphic/helper/roundRect.js");

var imageHelper = __webpack_require__(/*! ./image */ "./node_modules/zrender/lib/graphic/helper/image.js");

var fixShadow = __webpack_require__(/*! ./fixShadow */ "./node_modules/zrender/lib/graphic/helper/fixShadow.js");

var _constant = __webpack_require__(/*! ../constant */ "./node_modules/zrender/lib/graphic/constant.js");

var ContextCachedBy = _constant.ContextCachedBy;
var WILL_BE_RESTORED = _constant.WILL_BE_RESTORED;
var DEFAULT_FONT = textContain.DEFAULT_FONT; // TODO: Have not support 'start', 'end' yet.

var VALID_TEXT_ALIGN = {
  left: 1,
  right: 1,
  center: 1
};
var VALID_TEXT_VERTICAL_ALIGN = {
  top: 1,
  bottom: 1,
  middle: 1
}; // Different from `STYLE_COMMON_PROPS` of `graphic/Style`,
// the default value of shadowColor is `'transparent'`.

var SHADOW_STYLE_COMMON_PROPS = [['textShadowBlur', 'shadowBlur', 0], ['textShadowOffsetX', 'shadowOffsetX', 0], ['textShadowOffsetY', 'shadowOffsetY', 0], ['textShadowColor', 'shadowColor', 'transparent']];
/**
 * @param {module:zrender/graphic/Style} style
 * @return {module:zrender/graphic/Style} The input style.
 */

function normalizeTextStyle(style) {
  normalizeStyle(style);
  each(style.rich, normalizeStyle);
  return style;
}

function normalizeStyle(style) {
  if (style) {
    style.font = textContain.makeFont(style);
    var textAlign = style.textAlign;
    textAlign === 'middle' && (textAlign = 'center');
    style.textAlign = textAlign == null || VALID_TEXT_ALIGN[textAlign] ? textAlign : 'left'; // Compatible with textBaseline.

    var textVerticalAlign = style.textVerticalAlign || style.textBaseline;
    textVerticalAlign === 'center' && (textVerticalAlign = 'middle');
    style.textVerticalAlign = textVerticalAlign == null || VALID_TEXT_VERTICAL_ALIGN[textVerticalAlign] ? textVerticalAlign : 'top';
    var textPadding = style.textPadding;

    if (textPadding) {
      style.textPadding = normalizeCssArray(style.textPadding);
    }
  }
}
/**
 * @param {CanvasRenderingContext2D} ctx
 * @param {string} text
 * @param {module:zrender/graphic/Style} style
 * @param {Object|boolean} [rect] {x, y, width, height}
 *                  If set false, rect text is not used.
 * @param {Element|module:zrender/graphic/helper/constant.WILL_BE_RESTORED} [prevEl] For ctx prop cache.
 */


function renderText(hostEl, ctx, text, style, rect, prevEl) {
  style.rich ? renderRichText(hostEl, ctx, text, style, rect, prevEl) : renderPlainText(hostEl, ctx, text, style, rect, prevEl);
} // Avoid setting to ctx according to prevEl if possible for
// performance in scenarios of large amount text.


function renderPlainText(hostEl, ctx, text, style, rect, prevEl) {
  'use strict';

  var needDrawBg = needDrawBackground(style);
  var prevStyle;
  var checkCache = false;
  var cachedByMe = ctx.__attrCachedBy === ContextCachedBy.PLAIN_TEXT; // Only take and check cache for `Text` el, but not RectText.

  if (prevEl !== WILL_BE_RESTORED) {
    if (prevEl) {
      prevStyle = prevEl.style;
      checkCache = !needDrawBg && cachedByMe && prevStyle;
    } // Prevent from using cache in `Style::bind`, because of the case:
    // ctx property is modified by other properties than `Style::bind`
    // used, and Style::bind is called next.


    ctx.__attrCachedBy = needDrawBg ? ContextCachedBy.NONE : ContextCachedBy.PLAIN_TEXT;
  } // Since this will be restored, prevent from using these props to check cache in the next
  // entering of this method. But do not need to clear other cache like `Style::bind`.
  else if (cachedByMe) {
      ctx.__attrCachedBy = ContextCachedBy.NONE;
    }

  var styleFont = style.font || DEFAULT_FONT; // PENDING
  // Only `Text` el set `font` and keep it (`RectText` will restore). So theoretically
  // we can make font cache on ctx, which can cache for text el that are discontinuous.
  // But layer save/restore needed to be considered.
  // if (styleFont !== ctx.__fontCache) {
  //     ctx.font = styleFont;
  //     if (prevEl !== WILL_BE_RESTORED) {
  //         ctx.__fontCache = styleFont;
  //     }
  // }

  if (!checkCache || styleFont !== (prevStyle.font || DEFAULT_FONT)) {
    ctx.font = styleFont;
  } // Use the final font from context-2d, because the final
  // font might not be the style.font when it is illegal.
  // But get `ctx.font` might be time consuming.


  var computedFont = hostEl.__computedFont;

  if (hostEl.__styleFont !== styleFont) {
    hostEl.__styleFont = styleFont;
    computedFont = hostEl.__computedFont = ctx.font;
  }

  var textPadding = style.textPadding;
  var textLineHeight = style.textLineHeight;
  var contentBlock = hostEl.__textCotentBlock;

  if (!contentBlock || hostEl.__dirtyText) {
    contentBlock = hostEl.__textCotentBlock = textContain.parsePlainText(text, computedFont, textPadding, textLineHeight, style.truncate);
  }

  var outerHeight = contentBlock.outerHeight;
  var textLines = contentBlock.lines;
  var lineHeight = contentBlock.lineHeight;
  var boxPos = getBoxPosition(outerHeight, style, rect);
  var baseX = boxPos.baseX;
  var baseY = boxPos.baseY;
  var textAlign = boxPos.textAlign || 'left';
  var textVerticalAlign = boxPos.textVerticalAlign; // Origin of textRotation should be the base point of text drawing.

  applyTextRotation(ctx, style, rect, baseX, baseY);
  var boxY = textContain.adjustTextY(baseY, outerHeight, textVerticalAlign);
  var textX = baseX;
  var textY = boxY;

  if (needDrawBg || textPadding) {
    // Consider performance, do not call getTextWidth util necessary.
    var textWidth = textContain.getWidth(text, computedFont);
    var outerWidth = textWidth;
    textPadding && (outerWidth += textPadding[1] + textPadding[3]);
    var boxX = textContain.adjustTextX(baseX, outerWidth, textAlign);
    needDrawBg && drawBackground(hostEl, ctx, style, boxX, boxY, outerWidth, outerHeight);

    if (textPadding) {
      textX = getTextXForPadding(baseX, textAlign, textPadding);
      textY += textPadding[0];
    }
  } // Always set textAlign and textBase line, because it is difficute to calculate
  // textAlign from prevEl, and we dont sure whether textAlign will be reset if
  // font set happened.


  ctx.textAlign = textAlign; // Force baseline to be "middle". Otherwise, if using "top", the
  // text will offset downward a little bit in font "Microsoft YaHei".

  ctx.textBaseline = 'middle'; // Set text opacity

  ctx.globalAlpha = style.opacity || 1; // Always set shadowBlur and shadowOffset to avoid leak from displayable.

  for (var i = 0; i < SHADOW_STYLE_COMMON_PROPS.length; i++) {
    var propItem = SHADOW_STYLE_COMMON_PROPS[i];
    var styleProp = propItem[0];
    var ctxProp = propItem[1];
    var val = style[styleProp];

    if (!checkCache || val !== prevStyle[styleProp]) {
      ctx[ctxProp] = fixShadow(ctx, ctxProp, val || propItem[2]);
    }
  } // `textBaseline` is set as 'middle'.


  textY += lineHeight / 2;
  var textStrokeWidth = style.textStrokeWidth;
  var textStrokeWidthPrev = checkCache ? prevStyle.textStrokeWidth : null;
  var strokeWidthChanged = !checkCache || textStrokeWidth !== textStrokeWidthPrev;
  var strokeChanged = !checkCache || strokeWidthChanged || style.textStroke !== prevStyle.textStroke;
  var textStroke = getStroke(style.textStroke, textStrokeWidth);
  var textFill = getFill(style.textFill);

  if (textStroke) {
    if (strokeWidthChanged) {
      ctx.lineWidth = textStrokeWidth;
    }

    if (strokeChanged) {
      ctx.strokeStyle = textStroke;
    }
  }

  if (textFill) {
    if (!checkCache || style.textFill !== prevStyle.textFill) {
      ctx.fillStyle = textFill;
    }
  } // Optimize simply, in most cases only one line exists.


  if (textLines.length === 1) {
    // Fill after stroke so the outline will not cover the main part.
    textStroke && ctx.strokeText(textLines[0], textX, textY);
    textFill && ctx.fillText(textLines[0], textX, textY);
  } else {
    for (var i = 0; i < textLines.length; i++) {
      // Fill after stroke so the outline will not cover the main part.
      textStroke && ctx.strokeText(textLines[i], textX, textY);
      textFill && ctx.fillText(textLines[i], textX, textY);
      textY += lineHeight;
    }
  }
}

function renderRichText(hostEl, ctx, text, style, rect, prevEl) {
  // Do not do cache for rich text because of the complexity.
  // But `RectText` this will be restored, do not need to clear other cache like `Style::bind`.
  if (prevEl !== WILL_BE_RESTORED) {
    ctx.__attrCachedBy = ContextCachedBy.NONE;
  }

  var contentBlock = hostEl.__textCotentBlock;

  if (!contentBlock || hostEl.__dirtyText) {
    contentBlock = hostEl.__textCotentBlock = textContain.parseRichText(text, style);
  }

  drawRichText(hostEl, ctx, contentBlock, style, rect);
}

function drawRichText(hostEl, ctx, contentBlock, style, rect) {
  var contentWidth = contentBlock.width;
  var outerWidth = contentBlock.outerWidth;
  var outerHeight = contentBlock.outerHeight;
  var textPadding = style.textPadding;
  var boxPos = getBoxPosition(outerHeight, style, rect);
  var baseX = boxPos.baseX;
  var baseY = boxPos.baseY;
  var textAlign = boxPos.textAlign;
  var textVerticalAlign = boxPos.textVerticalAlign; // Origin of textRotation should be the base point of text drawing.

  applyTextRotation(ctx, style, rect, baseX, baseY);
  var boxX = textContain.adjustTextX(baseX, outerWidth, textAlign);
  var boxY = textContain.adjustTextY(baseY, outerHeight, textVerticalAlign);
  var xLeft = boxX;
  var lineTop = boxY;

  if (textPadding) {
    xLeft += textPadding[3];
    lineTop += textPadding[0];
  }

  var xRight = xLeft + contentWidth;
  needDrawBackground(style) && drawBackground(hostEl, ctx, style, boxX, boxY, outerWidth, outerHeight);

  for (var i = 0; i < contentBlock.lines.length; i++) {
    var line = contentBlock.lines[i];
    var tokens = line.tokens;
    var tokenCount = tokens.length;
    var lineHeight = line.lineHeight;
    var usedWidth = line.width;
    var leftIndex = 0;
    var lineXLeft = xLeft;
    var lineXRight = xRight;
    var rightIndex = tokenCount - 1;
    var token;

    while (leftIndex < tokenCount && (token = tokens[leftIndex], !token.textAlign || token.textAlign === 'left')) {
      placeToken(hostEl, ctx, token, style, lineHeight, lineTop, lineXLeft, 'left');
      usedWidth -= token.width;
      lineXLeft += token.width;
      leftIndex++;
    }

    while (rightIndex >= 0 && (token = tokens[rightIndex], token.textAlign === 'right')) {
      placeToken(hostEl, ctx, token, style, lineHeight, lineTop, lineXRight, 'right');
      usedWidth -= token.width;
      lineXRight -= token.width;
      rightIndex--;
    } // The other tokens are placed as textAlign 'center' if there is enough space.


    lineXLeft += (contentWidth - (lineXLeft - xLeft) - (xRight - lineXRight) - usedWidth) / 2;

    while (leftIndex <= rightIndex) {
      token = tokens[leftIndex]; // Consider width specified by user, use 'center' rather than 'left'.

      placeToken(hostEl, ctx, token, style, lineHeight, lineTop, lineXLeft + token.width / 2, 'center');
      lineXLeft += token.width;
      leftIndex++;
    }

    lineTop += lineHeight;
  }
}

function applyTextRotation(ctx, style, rect, x, y) {
  // textRotation only apply in RectText.
  if (rect && style.textRotation) {
    var origin = style.textOrigin;

    if (origin === 'center') {
      x = rect.width / 2 + rect.x;
      y = rect.height / 2 + rect.y;
    } else if (origin) {
      x = origin[0] + rect.x;
      y = origin[1] + rect.y;
    }

    ctx.translate(x, y); // Positive: anticlockwise

    ctx.rotate(-style.textRotation);
    ctx.translate(-x, -y);
  }
}

function placeToken(hostEl, ctx, token, style, lineHeight, lineTop, x, textAlign) {
  var tokenStyle = style.rich[token.styleName] || {};
  tokenStyle.text = token.text; // 'ctx.textBaseline' is always set as 'middle', for sake of
  // the bias of "Microsoft YaHei".

  var textVerticalAlign = token.textVerticalAlign;
  var y = lineTop + lineHeight / 2;

  if (textVerticalAlign === 'top') {
    y = lineTop + token.height / 2;
  } else if (textVerticalAlign === 'bottom') {
    y = lineTop + lineHeight - token.height / 2;
  }

  !token.isLineHolder && needDrawBackground(tokenStyle) && drawBackground(hostEl, ctx, tokenStyle, textAlign === 'right' ? x - token.width : textAlign === 'center' ? x - token.width / 2 : x, y - token.height / 2, token.width, token.height);
  var textPadding = token.textPadding;

  if (textPadding) {
    x = getTextXForPadding(x, textAlign, textPadding);
    y -= token.height / 2 - textPadding[2] - token.textHeight / 2;
  }

  setCtx(ctx, 'shadowBlur', retrieve3(tokenStyle.textShadowBlur, style.textShadowBlur, 0));
  setCtx(ctx, 'shadowColor', tokenStyle.textShadowColor || style.textShadowColor || 'transparent');
  setCtx(ctx, 'shadowOffsetX', retrieve3(tokenStyle.textShadowOffsetX, style.textShadowOffsetX, 0));
  setCtx(ctx, 'shadowOffsetY', retrieve3(tokenStyle.textShadowOffsetY, style.textShadowOffsetY, 0));
  setCtx(ctx, 'textAlign', textAlign); // Force baseline to be "middle". Otherwise, if using "top", the
  // text will offset downward a little bit in font "Microsoft YaHei".

  setCtx(ctx, 'textBaseline', 'middle');
  setCtx(ctx, 'font', token.font || DEFAULT_FONT);
  var textStroke = getStroke(tokenStyle.textStroke || style.textStroke, textStrokeWidth);
  var textFill = getFill(tokenStyle.textFill || style.textFill);
  var textStrokeWidth = retrieve2(tokenStyle.textStrokeWidth, style.textStrokeWidth); // Fill after stroke so the outline will not cover the main part.

  if (textStroke) {
    setCtx(ctx, 'lineWidth', textStrokeWidth);
    setCtx(ctx, 'strokeStyle', textStroke);
    ctx.strokeText(token.text, x, y);
  }

  if (textFill) {
    setCtx(ctx, 'fillStyle', textFill);
    ctx.fillText(token.text, x, y);
  }
}

function needDrawBackground(style) {
  return !!(style.textBackgroundColor || style.textBorderWidth && style.textBorderColor);
} // style: {textBackgroundColor, textBorderWidth, textBorderColor, textBorderRadius, text}
// shape: {x, y, width, height}


function drawBackground(hostEl, ctx, style, x, y, width, height) {
  var textBackgroundColor = style.textBackgroundColor;
  var textBorderWidth = style.textBorderWidth;
  var textBorderColor = style.textBorderColor;
  var isPlainBg = isString(textBackgroundColor);
  setCtx(ctx, 'shadowBlur', style.textBoxShadowBlur || 0);
  setCtx(ctx, 'shadowColor', style.textBoxShadowColor || 'transparent');
  setCtx(ctx, 'shadowOffsetX', style.textBoxShadowOffsetX || 0);
  setCtx(ctx, 'shadowOffsetY', style.textBoxShadowOffsetY || 0);

  if (isPlainBg || textBorderWidth && textBorderColor) {
    ctx.beginPath();
    var textBorderRadius = style.textBorderRadius;

    if (!textBorderRadius) {
      ctx.rect(x, y, width, height);
    } else {
      roundRectHelper.buildPath(ctx, {
        x: x,
        y: y,
        width: width,
        height: height,
        r: textBorderRadius
      });
    }

    ctx.closePath();
  }

  if (isPlainBg) {
    setCtx(ctx, 'fillStyle', textBackgroundColor);

    if (style.fillOpacity != null) {
      var originalGlobalAlpha = ctx.globalAlpha;
      ctx.globalAlpha = style.fillOpacity * style.opacity;
      ctx.fill();
      ctx.globalAlpha = originalGlobalAlpha;
    } else {
      ctx.fill();
    }
  } else if (isObject(textBackgroundColor)) {
    var image = textBackgroundColor.image;
    image = imageHelper.createOrUpdateImage(image, null, hostEl, onBgImageLoaded, textBackgroundColor);

    if (image && imageHelper.isImageReady(image)) {
      ctx.drawImage(image, x, y, width, height);
    }
  }

  if (textBorderWidth && textBorderColor) {
    setCtx(ctx, 'lineWidth', textBorderWidth);
    setCtx(ctx, 'strokeStyle', textBorderColor);

    if (style.strokeOpacity != null) {
      var originalGlobalAlpha = ctx.globalAlpha;
      ctx.globalAlpha = style.strokeOpacity * style.opacity;
      ctx.stroke();
      ctx.globalAlpha = originalGlobalAlpha;
    } else {
      ctx.stroke();
    }
  }
}

function onBgImageLoaded(image, textBackgroundColor) {
  // Replace image, so that `contain/text.js#parseRichText`
  // will get correct result in next tick.
  textBackgroundColor.image = image;
}

function getBoxPosition(blockHeiht, style, rect) {
  var baseX = style.x || 0;
  var baseY = style.y || 0;
  var textAlign = style.textAlign;
  var textVerticalAlign = style.textVerticalAlign; // Text position represented by coord

  if (rect) {
    var textPosition = style.textPosition;

    if (textPosition instanceof Array) {
      // Percent
      baseX = rect.x + parsePercent(textPosition[0], rect.width);
      baseY = rect.y + parsePercent(textPosition[1], rect.height);
    } else {
      var res = textContain.adjustTextPositionOnRect(textPosition, rect, style.textDistance);
      baseX = res.x;
      baseY = res.y; // Default align and baseline when has textPosition

      textAlign = textAlign || res.textAlign;
      textVerticalAlign = textVerticalAlign || res.textVerticalAlign;
    } // textOffset is only support in RectText, otherwise
    // we have to adjust boundingRect for textOffset.


    var textOffset = style.textOffset;

    if (textOffset) {
      baseX += textOffset[0];
      baseY += textOffset[1];
    }
  }

  return {
    baseX: baseX,
    baseY: baseY,
    textAlign: textAlign,
    textVerticalAlign: textVerticalAlign
  };
}

function setCtx(ctx, prop, value) {
  ctx[prop] = fixShadow(ctx, prop, value);
  return ctx[prop];
}
/**
 * @param {string} [stroke] If specified, do not check style.textStroke.
 * @param {string} [lineWidth] If specified, do not check style.textStroke.
 * @param {number} style
 */


function getStroke(stroke, lineWidth) {
  return stroke == null || lineWidth <= 0 || stroke === 'transparent' || stroke === 'none' ? null // TODO pattern and gradient?
  : stroke.image || stroke.colorStops ? '#000' : stroke;
}

function getFill(fill) {
  return fill == null || fill === 'none' ? null // TODO pattern and gradient?
  : fill.image || fill.colorStops ? '#000' : fill;
}

function parsePercent(value, maxValue) {
  if (typeof value === 'string') {
    if (value.lastIndexOf('%') >= 0) {
      return parseFloat(value) / 100 * maxValue;
    }

    return parseFloat(value);
  }

  return value;
}

function getTextXForPadding(x, textAlign, textPadding) {
  return textAlign === 'right' ? x - textPadding[1] : textAlign === 'center' ? x + textPadding[3] / 2 - textPadding[1] / 2 : x + textPadding[3];
}
/**
 * @param {string} text
 * @param {module:zrender/Style} style
 * @return {boolean}
 */


function needDrawText(text, style) {
  return text != null && (text || style.textBackgroundColor || style.textBorderWidth && style.textBorderColor || style.textPadding);
}

exports.normalizeTextStyle = normalizeTextStyle;
exports.renderText = renderText;
exports.getStroke = getStroke;
exports.getFill = getFill;
exports.needDrawText = needDrawText;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvdGV4dC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvaGVscGVyL3RleHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF91dGlsID0gcmVxdWlyZShcIi4uLy4uL2NvcmUvdXRpbFwiKTtcblxudmFyIHJldHJpZXZlMiA9IF91dGlsLnJldHJpZXZlMjtcbnZhciByZXRyaWV2ZTMgPSBfdXRpbC5yZXRyaWV2ZTM7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgbm9ybWFsaXplQ3NzQXJyYXkgPSBfdXRpbC5ub3JtYWxpemVDc3NBcnJheTtcbnZhciBpc1N0cmluZyA9IF91dGlsLmlzU3RyaW5nO1xudmFyIGlzT2JqZWN0ID0gX3V0aWwuaXNPYmplY3Q7XG5cbnZhciB0ZXh0Q29udGFpbiA9IHJlcXVpcmUoXCIuLi8uLi9jb250YWluL3RleHRcIik7XG5cbnZhciByb3VuZFJlY3RIZWxwZXIgPSByZXF1aXJlKFwiLi9yb3VuZFJlY3RcIik7XG5cbnZhciBpbWFnZUhlbHBlciA9IHJlcXVpcmUoXCIuL2ltYWdlXCIpO1xuXG52YXIgZml4U2hhZG93ID0gcmVxdWlyZShcIi4vZml4U2hhZG93XCIpO1xuXG52YXIgX2NvbnN0YW50ID0gcmVxdWlyZShcIi4uL2NvbnN0YW50XCIpO1xuXG52YXIgQ29udGV4dENhY2hlZEJ5ID0gX2NvbnN0YW50LkNvbnRleHRDYWNoZWRCeTtcbnZhciBXSUxMX0JFX1JFU1RPUkVEID0gX2NvbnN0YW50LldJTExfQkVfUkVTVE9SRUQ7XG52YXIgREVGQVVMVF9GT05UID0gdGV4dENvbnRhaW4uREVGQVVMVF9GT05UOyAvLyBUT0RPOiBIYXZlIG5vdCBzdXBwb3J0ICdzdGFydCcsICdlbmQnIHlldC5cblxudmFyIFZBTElEX1RFWFRfQUxJR04gPSB7XG4gIGxlZnQ6IDEsXG4gIHJpZ2h0OiAxLFxuICBjZW50ZXI6IDFcbn07XG52YXIgVkFMSURfVEVYVF9WRVJUSUNBTF9BTElHTiA9IHtcbiAgdG9wOiAxLFxuICBib3R0b206IDEsXG4gIG1pZGRsZTogMVxufTsgLy8gRGlmZmVyZW50IGZyb20gYFNUWUxFX0NPTU1PTl9QUk9QU2Agb2YgYGdyYXBoaWMvU3R5bGVgLFxuLy8gdGhlIGRlZmF1bHQgdmFsdWUgb2Ygc2hhZG93Q29sb3IgaXMgYCd0cmFuc3BhcmVudCdgLlxuXG52YXIgU0hBRE9XX1NUWUxFX0NPTU1PTl9QUk9QUyA9IFtbJ3RleHRTaGFkb3dCbHVyJywgJ3NoYWRvd0JsdXInLCAwXSwgWyd0ZXh0U2hhZG93T2Zmc2V0WCcsICdzaGFkb3dPZmZzZXRYJywgMF0sIFsndGV4dFNoYWRvd09mZnNldFknLCAnc2hhZG93T2Zmc2V0WScsIDBdLCBbJ3RleHRTaGFkb3dDb2xvcicsICdzaGFkb3dDb2xvcicsICd0cmFuc3BhcmVudCddXTtcbi8qKlxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9ncmFwaGljL1N0eWxlfSBzdHlsZVxuICogQHJldHVybiB7bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9TdHlsZX0gVGhlIGlucHV0IHN0eWxlLlxuICovXG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZVRleHRTdHlsZShzdHlsZSkge1xuICBub3JtYWxpemVTdHlsZShzdHlsZSk7XG4gIGVhY2goc3R5bGUucmljaCwgbm9ybWFsaXplU3R5bGUpO1xuICByZXR1cm4gc3R5bGU7XG59XG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZVN0eWxlKHN0eWxlKSB7XG4gIGlmIChzdHlsZSkge1xuICAgIHN0eWxlLmZvbnQgPSB0ZXh0Q29udGFpbi5tYWtlRm9udChzdHlsZSk7XG4gICAgdmFyIHRleHRBbGlnbiA9IHN0eWxlLnRleHRBbGlnbjtcbiAgICB0ZXh0QWxpZ24gPT09ICdtaWRkbGUnICYmICh0ZXh0QWxpZ24gPSAnY2VudGVyJyk7XG4gICAgc3R5bGUudGV4dEFsaWduID0gdGV4dEFsaWduID09IG51bGwgfHwgVkFMSURfVEVYVF9BTElHTlt0ZXh0QWxpZ25dID8gdGV4dEFsaWduIDogJ2xlZnQnOyAvLyBDb21wYXRpYmxlIHdpdGggdGV4dEJhc2VsaW5lLlxuXG4gICAgdmFyIHRleHRWZXJ0aWNhbEFsaWduID0gc3R5bGUudGV4dFZlcnRpY2FsQWxpZ24gfHwgc3R5bGUudGV4dEJhc2VsaW5lO1xuICAgIHRleHRWZXJ0aWNhbEFsaWduID09PSAnY2VudGVyJyAmJiAodGV4dFZlcnRpY2FsQWxpZ24gPSAnbWlkZGxlJyk7XG4gICAgc3R5bGUudGV4dFZlcnRpY2FsQWxpZ24gPSB0ZXh0VmVydGljYWxBbGlnbiA9PSBudWxsIHx8IFZBTElEX1RFWFRfVkVSVElDQUxfQUxJR05bdGV4dFZlcnRpY2FsQWxpZ25dID8gdGV4dFZlcnRpY2FsQWxpZ24gOiAndG9wJztcbiAgICB2YXIgdGV4dFBhZGRpbmcgPSBzdHlsZS50ZXh0UGFkZGluZztcblxuICAgIGlmICh0ZXh0UGFkZGluZykge1xuICAgICAgc3R5bGUudGV4dFBhZGRpbmcgPSBub3JtYWxpemVDc3NBcnJheShzdHlsZS50ZXh0UGFkZGluZyk7XG4gICAgfVxuICB9XG59XG4vKipcbiAqIEBwYXJhbSB7Q2FudmFzUmVuZGVyaW5nQ29udGV4dDJEfSBjdHhcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0XG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL2dyYXBoaWMvU3R5bGV9IHN0eWxlXG4gKiBAcGFyYW0ge09iamVjdHxib29sZWFufSBbcmVjdF0ge3gsIHksIHdpZHRoLCBoZWlnaHR9XG4gKiAgICAgICAgICAgICAgICAgIElmIHNldCBmYWxzZSwgcmVjdCB0ZXh0IGlzIG5vdCB1c2VkLlxuICogQHBhcmFtIHtFbGVtZW50fG1vZHVsZTp6cmVuZGVyL2dyYXBoaWMvaGVscGVyL2NvbnN0YW50LldJTExfQkVfUkVTVE9SRUR9IFtwcmV2RWxdIEZvciBjdHggcHJvcCBjYWNoZS5cbiAqL1xuXG5cbmZ1bmN0aW9uIHJlbmRlclRleHQoaG9zdEVsLCBjdHgsIHRleHQsIHN0eWxlLCByZWN0LCBwcmV2RWwpIHtcbiAgc3R5bGUucmljaCA/IHJlbmRlclJpY2hUZXh0KGhvc3RFbCwgY3R4LCB0ZXh0LCBzdHlsZSwgcmVjdCwgcHJldkVsKSA6IHJlbmRlclBsYWluVGV4dChob3N0RWwsIGN0eCwgdGV4dCwgc3R5bGUsIHJlY3QsIHByZXZFbCk7XG59IC8vIEF2b2lkIHNldHRpbmcgdG8gY3R4IGFjY29yZGluZyB0byBwcmV2RWwgaWYgcG9zc2libGUgZm9yXG4vLyBwZXJmb3JtYW5jZSBpbiBzY2VuYXJpb3Mgb2YgbGFyZ2UgYW1vdW50IHRleHQuXG5cblxuZnVuY3Rpb24gcmVuZGVyUGxhaW5UZXh0KGhvc3RFbCwgY3R4LCB0ZXh0LCBzdHlsZSwgcmVjdCwgcHJldkVsKSB7XG4gICd1c2Ugc3RyaWN0JztcblxuICB2YXIgbmVlZERyYXdCZyA9IG5lZWREcmF3QmFja2dyb3VuZChzdHlsZSk7XG4gIHZhciBwcmV2U3R5bGU7XG4gIHZhciBjaGVja0NhY2hlID0gZmFsc2U7XG4gIHZhciBjYWNoZWRCeU1lID0gY3R4Ll9fYXR0ckNhY2hlZEJ5ID09PSBDb250ZXh0Q2FjaGVkQnkuUExBSU5fVEVYVDsgLy8gT25seSB0YWtlIGFuZCBjaGVjayBjYWNoZSBmb3IgYFRleHRgIGVsLCBidXQgbm90IFJlY3RUZXh0LlxuXG4gIGlmIChwcmV2RWwgIT09IFdJTExfQkVfUkVTVE9SRUQpIHtcbiAgICBpZiAocHJldkVsKSB7XG4gICAgICBwcmV2U3R5bGUgPSBwcmV2RWwuc3R5bGU7XG4gICAgICBjaGVja0NhY2hlID0gIW5lZWREcmF3QmcgJiYgY2FjaGVkQnlNZSAmJiBwcmV2U3R5bGU7XG4gICAgfSAvLyBQcmV2ZW50IGZyb20gdXNpbmcgY2FjaGUgaW4gYFN0eWxlOjpiaW5kYCwgYmVjYXVzZSBvZiB0aGUgY2FzZTpcbiAgICAvLyBjdHggcHJvcGVydHkgaXMgbW9kaWZpZWQgYnkgb3RoZXIgcHJvcGVydGllcyB0aGFuIGBTdHlsZTo6YmluZGBcbiAgICAvLyB1c2VkLCBhbmQgU3R5bGU6OmJpbmQgaXMgY2FsbGVkIG5leHQuXG5cblxuICAgIGN0eC5fX2F0dHJDYWNoZWRCeSA9IG5lZWREcmF3QmcgPyBDb250ZXh0Q2FjaGVkQnkuTk9ORSA6IENvbnRleHRDYWNoZWRCeS5QTEFJTl9URVhUO1xuICB9IC8vIFNpbmNlIHRoaXMgd2lsbCBiZSByZXN0b3JlZCwgcHJldmVudCBmcm9tIHVzaW5nIHRoZXNlIHByb3BzIHRvIGNoZWNrIGNhY2hlIGluIHRoZSBuZXh0XG4gIC8vIGVudGVyaW5nIG9mIHRoaXMgbWV0aG9kLiBCdXQgZG8gbm90IG5lZWQgdG8gY2xlYXIgb3RoZXIgY2FjaGUgbGlrZSBgU3R5bGU6OmJpbmRgLlxuICBlbHNlIGlmIChjYWNoZWRCeU1lKSB7XG4gICAgICBjdHguX19hdHRyQ2FjaGVkQnkgPSBDb250ZXh0Q2FjaGVkQnkuTk9ORTtcbiAgICB9XG5cbiAgdmFyIHN0eWxlRm9udCA9IHN0eWxlLmZvbnQgfHwgREVGQVVMVF9GT05UOyAvLyBQRU5ESU5HXG4gIC8vIE9ubHkgYFRleHRgIGVsIHNldCBgZm9udGAgYW5kIGtlZXAgaXQgKGBSZWN0VGV4dGAgd2lsbCByZXN0b3JlKS4gU28gdGhlb3JldGljYWxseVxuICAvLyB3ZSBjYW4gbWFrZSBmb250IGNhY2hlIG9uIGN0eCwgd2hpY2ggY2FuIGNhY2hlIGZvciB0ZXh0IGVsIHRoYXQgYXJlIGRpc2NvbnRpbnVvdXMuXG4gIC8vIEJ1dCBsYXllciBzYXZlL3Jlc3RvcmUgbmVlZGVkIHRvIGJlIGNvbnNpZGVyZWQuXG4gIC8vIGlmIChzdHlsZUZvbnQgIT09IGN0eC5fX2ZvbnRDYWNoZSkge1xuICAvLyAgICAgY3R4LmZvbnQgPSBzdHlsZUZvbnQ7XG4gIC8vICAgICBpZiAocHJldkVsICE9PSBXSUxMX0JFX1JFU1RPUkVEKSB7XG4gIC8vICAgICAgICAgY3R4Ll9fZm9udENhY2hlID0gc3R5bGVGb250O1xuICAvLyAgICAgfVxuICAvLyB9XG5cbiAgaWYgKCFjaGVja0NhY2hlIHx8IHN0eWxlRm9udCAhPT0gKHByZXZTdHlsZS5mb250IHx8IERFRkFVTFRfRk9OVCkpIHtcbiAgICBjdHguZm9udCA9IHN0eWxlRm9udDtcbiAgfSAvLyBVc2UgdGhlIGZpbmFsIGZvbnQgZnJvbSBjb250ZXh0LTJkLCBiZWNhdXNlIHRoZSBmaW5hbFxuICAvLyBmb250IG1pZ2h0IG5vdCBiZSB0aGUgc3R5bGUuZm9udCB3aGVuIGl0IGlzIGlsbGVnYWwuXG4gIC8vIEJ1dCBnZXQgYGN0eC5mb250YCBtaWdodCBiZSB0aW1lIGNvbnN1bWluZy5cblxuXG4gIHZhciBjb21wdXRlZEZvbnQgPSBob3N0RWwuX19jb21wdXRlZEZvbnQ7XG5cbiAgaWYgKGhvc3RFbC5fX3N0eWxlRm9udCAhPT0gc3R5bGVGb250KSB7XG4gICAgaG9zdEVsLl9fc3R5bGVGb250ID0gc3R5bGVGb250O1xuICAgIGNvbXB1dGVkRm9udCA9IGhvc3RFbC5fX2NvbXB1dGVkRm9udCA9IGN0eC5mb250O1xuICB9XG5cbiAgdmFyIHRleHRQYWRkaW5nID0gc3R5bGUudGV4dFBhZGRpbmc7XG4gIHZhciB0ZXh0TGluZUhlaWdodCA9IHN0eWxlLnRleHRMaW5lSGVpZ2h0O1xuICB2YXIgY29udGVudEJsb2NrID0gaG9zdEVsLl9fdGV4dENvdGVudEJsb2NrO1xuXG4gIGlmICghY29udGVudEJsb2NrIHx8IGhvc3RFbC5fX2RpcnR5VGV4dCkge1xuICAgIGNvbnRlbnRCbG9jayA9IGhvc3RFbC5fX3RleHRDb3RlbnRCbG9jayA9IHRleHRDb250YWluLnBhcnNlUGxhaW5UZXh0KHRleHQsIGNvbXB1dGVkRm9udCwgdGV4dFBhZGRpbmcsIHRleHRMaW5lSGVpZ2h0LCBzdHlsZS50cnVuY2F0ZSk7XG4gIH1cblxuICB2YXIgb3V0ZXJIZWlnaHQgPSBjb250ZW50QmxvY2sub3V0ZXJIZWlnaHQ7XG4gIHZhciB0ZXh0TGluZXMgPSBjb250ZW50QmxvY2subGluZXM7XG4gIHZhciBsaW5lSGVpZ2h0ID0gY29udGVudEJsb2NrLmxpbmVIZWlnaHQ7XG4gIHZhciBib3hQb3MgPSBnZXRCb3hQb3NpdGlvbihvdXRlckhlaWdodCwgc3R5bGUsIHJlY3QpO1xuICB2YXIgYmFzZVggPSBib3hQb3MuYmFzZVg7XG4gIHZhciBiYXNlWSA9IGJveFBvcy5iYXNlWTtcbiAgdmFyIHRleHRBbGlnbiA9IGJveFBvcy50ZXh0QWxpZ24gfHwgJ2xlZnQnO1xuICB2YXIgdGV4dFZlcnRpY2FsQWxpZ24gPSBib3hQb3MudGV4dFZlcnRpY2FsQWxpZ247IC8vIE9yaWdpbiBvZiB0ZXh0Um90YXRpb24gc2hvdWxkIGJlIHRoZSBiYXNlIHBvaW50IG9mIHRleHQgZHJhd2luZy5cblxuICBhcHBseVRleHRSb3RhdGlvbihjdHgsIHN0eWxlLCByZWN0LCBiYXNlWCwgYmFzZVkpO1xuICB2YXIgYm94WSA9IHRleHRDb250YWluLmFkanVzdFRleHRZKGJhc2VZLCBvdXRlckhlaWdodCwgdGV4dFZlcnRpY2FsQWxpZ24pO1xuICB2YXIgdGV4dFggPSBiYXNlWDtcbiAgdmFyIHRleHRZID0gYm94WTtcblxuICBpZiAobmVlZERyYXdCZyB8fCB0ZXh0UGFkZGluZykge1xuICAgIC8vIENvbnNpZGVyIHBlcmZvcm1hbmNlLCBkbyBub3QgY2FsbCBnZXRUZXh0V2lkdGggdXRpbCBuZWNlc3NhcnkuXG4gICAgdmFyIHRleHRXaWR0aCA9IHRleHRDb250YWluLmdldFdpZHRoKHRleHQsIGNvbXB1dGVkRm9udCk7XG4gICAgdmFyIG91dGVyV2lkdGggPSB0ZXh0V2lkdGg7XG4gICAgdGV4dFBhZGRpbmcgJiYgKG91dGVyV2lkdGggKz0gdGV4dFBhZGRpbmdbMV0gKyB0ZXh0UGFkZGluZ1szXSk7XG4gICAgdmFyIGJveFggPSB0ZXh0Q29udGFpbi5hZGp1c3RUZXh0WChiYXNlWCwgb3V0ZXJXaWR0aCwgdGV4dEFsaWduKTtcbiAgICBuZWVkRHJhd0JnICYmIGRyYXdCYWNrZ3JvdW5kKGhvc3RFbCwgY3R4LCBzdHlsZSwgYm94WCwgYm94WSwgb3V0ZXJXaWR0aCwgb3V0ZXJIZWlnaHQpO1xuXG4gICAgaWYgKHRleHRQYWRkaW5nKSB7XG4gICAgICB0ZXh0WCA9IGdldFRleHRYRm9yUGFkZGluZyhiYXNlWCwgdGV4dEFsaWduLCB0ZXh0UGFkZGluZyk7XG4gICAgICB0ZXh0WSArPSB0ZXh0UGFkZGluZ1swXTtcbiAgICB9XG4gIH0gLy8gQWx3YXlzIHNldCB0ZXh0QWxpZ24gYW5kIHRleHRCYXNlIGxpbmUsIGJlY2F1c2UgaXQgaXMgZGlmZmljdXRlIHRvIGNhbGN1bGF0ZVxuICAvLyB0ZXh0QWxpZ24gZnJvbSBwcmV2RWwsIGFuZCB3ZSBkb250IHN1cmUgd2hldGhlciB0ZXh0QWxpZ24gd2lsbCBiZSByZXNldCBpZlxuICAvLyBmb250IHNldCBoYXBwZW5lZC5cblxuXG4gIGN0eC50ZXh0QWxpZ24gPSB0ZXh0QWxpZ247IC8vIEZvcmNlIGJhc2VsaW5lIHRvIGJlIFwibWlkZGxlXCIuIE90aGVyd2lzZSwgaWYgdXNpbmcgXCJ0b3BcIiwgdGhlXG4gIC8vIHRleHQgd2lsbCBvZmZzZXQgZG93bndhcmQgYSBsaXR0bGUgYml0IGluIGZvbnQgXCJNaWNyb3NvZnQgWWFIZWlcIi5cblxuICBjdHgudGV4dEJhc2VsaW5lID0gJ21pZGRsZSc7IC8vIFNldCB0ZXh0IG9wYWNpdHlcblxuICBjdHguZ2xvYmFsQWxwaGEgPSBzdHlsZS5vcGFjaXR5IHx8IDE7IC8vIEFsd2F5cyBzZXQgc2hhZG93Qmx1ciBhbmQgc2hhZG93T2Zmc2V0IHRvIGF2b2lkIGxlYWsgZnJvbSBkaXNwbGF5YWJsZS5cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IFNIQURPV19TVFlMRV9DT01NT05fUFJPUFMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgcHJvcEl0ZW0gPSBTSEFET1dfU1RZTEVfQ09NTU9OX1BST1BTW2ldO1xuICAgIHZhciBzdHlsZVByb3AgPSBwcm9wSXRlbVswXTtcbiAgICB2YXIgY3R4UHJvcCA9IHByb3BJdGVtWzFdO1xuICAgIHZhciB2YWwgPSBzdHlsZVtzdHlsZVByb3BdO1xuXG4gICAgaWYgKCFjaGVja0NhY2hlIHx8IHZhbCAhPT0gcHJldlN0eWxlW3N0eWxlUHJvcF0pIHtcbiAgICAgIGN0eFtjdHhQcm9wXSA9IGZpeFNoYWRvdyhjdHgsIGN0eFByb3AsIHZhbCB8fCBwcm9wSXRlbVsyXSk7XG4gICAgfVxuICB9IC8vIGB0ZXh0QmFzZWxpbmVgIGlzIHNldCBhcyAnbWlkZGxlJy5cblxuXG4gIHRleHRZICs9IGxpbmVIZWlnaHQgLyAyO1xuICB2YXIgdGV4dFN0cm9rZVdpZHRoID0gc3R5bGUudGV4dFN0cm9rZVdpZHRoO1xuICB2YXIgdGV4dFN0cm9rZVdpZHRoUHJldiA9IGNoZWNrQ2FjaGUgPyBwcmV2U3R5bGUudGV4dFN0cm9rZVdpZHRoIDogbnVsbDtcbiAgdmFyIHN0cm9rZVdpZHRoQ2hhbmdlZCA9ICFjaGVja0NhY2hlIHx8IHRleHRTdHJva2VXaWR0aCAhPT0gdGV4dFN0cm9rZVdpZHRoUHJldjtcbiAgdmFyIHN0cm9rZUNoYW5nZWQgPSAhY2hlY2tDYWNoZSB8fCBzdHJva2VXaWR0aENoYW5nZWQgfHwgc3R5bGUudGV4dFN0cm9rZSAhPT0gcHJldlN0eWxlLnRleHRTdHJva2U7XG4gIHZhciB0ZXh0U3Ryb2tlID0gZ2V0U3Ryb2tlKHN0eWxlLnRleHRTdHJva2UsIHRleHRTdHJva2VXaWR0aCk7XG4gIHZhciB0ZXh0RmlsbCA9IGdldEZpbGwoc3R5bGUudGV4dEZpbGwpO1xuXG4gIGlmICh0ZXh0U3Ryb2tlKSB7XG4gICAgaWYgKHN0cm9rZVdpZHRoQ2hhbmdlZCkge1xuICAgICAgY3R4LmxpbmVXaWR0aCA9IHRleHRTdHJva2VXaWR0aDtcbiAgICB9XG5cbiAgICBpZiAoc3Ryb2tlQ2hhbmdlZCkge1xuICAgICAgY3R4LnN0cm9rZVN0eWxlID0gdGV4dFN0cm9rZTtcbiAgICB9XG4gIH1cblxuICBpZiAodGV4dEZpbGwpIHtcbiAgICBpZiAoIWNoZWNrQ2FjaGUgfHwgc3R5bGUudGV4dEZpbGwgIT09IHByZXZTdHlsZS50ZXh0RmlsbCkge1xuICAgICAgY3R4LmZpbGxTdHlsZSA9IHRleHRGaWxsO1xuICAgIH1cbiAgfSAvLyBPcHRpbWl6ZSBzaW1wbHksIGluIG1vc3QgY2FzZXMgb25seSBvbmUgbGluZSBleGlzdHMuXG5cblxuICBpZiAodGV4dExpbmVzLmxlbmd0aCA9PT0gMSkge1xuICAgIC8vIEZpbGwgYWZ0ZXIgc3Ryb2tlIHNvIHRoZSBvdXRsaW5lIHdpbGwgbm90IGNvdmVyIHRoZSBtYWluIHBhcnQuXG4gICAgdGV4dFN0cm9rZSAmJiBjdHguc3Ryb2tlVGV4dCh0ZXh0TGluZXNbMF0sIHRleHRYLCB0ZXh0WSk7XG4gICAgdGV4dEZpbGwgJiYgY3R4LmZpbGxUZXh0KHRleHRMaW5lc1swXSwgdGV4dFgsIHRleHRZKTtcbiAgfSBlbHNlIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRleHRMaW5lcy5sZW5ndGg7IGkrKykge1xuICAgICAgLy8gRmlsbCBhZnRlciBzdHJva2Ugc28gdGhlIG91dGxpbmUgd2lsbCBub3QgY292ZXIgdGhlIG1haW4gcGFydC5cbiAgICAgIHRleHRTdHJva2UgJiYgY3R4LnN0cm9rZVRleHQodGV4dExpbmVzW2ldLCB0ZXh0WCwgdGV4dFkpO1xuICAgICAgdGV4dEZpbGwgJiYgY3R4LmZpbGxUZXh0KHRleHRMaW5lc1tpXSwgdGV4dFgsIHRleHRZKTtcbiAgICAgIHRleHRZICs9IGxpbmVIZWlnaHQ7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIHJlbmRlclJpY2hUZXh0KGhvc3RFbCwgY3R4LCB0ZXh0LCBzdHlsZSwgcmVjdCwgcHJldkVsKSB7XG4gIC8vIERvIG5vdCBkbyBjYWNoZSBmb3IgcmljaCB0ZXh0IGJlY2F1c2Ugb2YgdGhlIGNvbXBsZXhpdHkuXG4gIC8vIEJ1dCBgUmVjdFRleHRgIHRoaXMgd2lsbCBiZSByZXN0b3JlZCwgZG8gbm90IG5lZWQgdG8gY2xlYXIgb3RoZXIgY2FjaGUgbGlrZSBgU3R5bGU6OmJpbmRgLlxuICBpZiAocHJldkVsICE9PSBXSUxMX0JFX1JFU1RPUkVEKSB7XG4gICAgY3R4Ll9fYXR0ckNhY2hlZEJ5ID0gQ29udGV4dENhY2hlZEJ5Lk5PTkU7XG4gIH1cblxuICB2YXIgY29udGVudEJsb2NrID0gaG9zdEVsLl9fdGV4dENvdGVudEJsb2NrO1xuXG4gIGlmICghY29udGVudEJsb2NrIHx8IGhvc3RFbC5fX2RpcnR5VGV4dCkge1xuICAgIGNvbnRlbnRCbG9jayA9IGhvc3RFbC5fX3RleHRDb3RlbnRCbG9jayA9IHRleHRDb250YWluLnBhcnNlUmljaFRleHQodGV4dCwgc3R5bGUpO1xuICB9XG5cbiAgZHJhd1JpY2hUZXh0KGhvc3RFbCwgY3R4LCBjb250ZW50QmxvY2ssIHN0eWxlLCByZWN0KTtcbn1cblxuZnVuY3Rpb24gZHJhd1JpY2hUZXh0KGhvc3RFbCwgY3R4LCBjb250ZW50QmxvY2ssIHN0eWxlLCByZWN0KSB7XG4gIHZhciBjb250ZW50V2lkdGggPSBjb250ZW50QmxvY2sud2lkdGg7XG4gIHZhciBvdXRlcldpZHRoID0gY29udGVudEJsb2NrLm91dGVyV2lkdGg7XG4gIHZhciBvdXRlckhlaWdodCA9IGNvbnRlbnRCbG9jay5vdXRlckhlaWdodDtcbiAgdmFyIHRleHRQYWRkaW5nID0gc3R5bGUudGV4dFBhZGRpbmc7XG4gIHZhciBib3hQb3MgPSBnZXRCb3hQb3NpdGlvbihvdXRlckhlaWdodCwgc3R5bGUsIHJlY3QpO1xuICB2YXIgYmFzZVggPSBib3hQb3MuYmFzZVg7XG4gIHZhciBiYXNlWSA9IGJveFBvcy5iYXNlWTtcbiAgdmFyIHRleHRBbGlnbiA9IGJveFBvcy50ZXh0QWxpZ247XG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbiA9IGJveFBvcy50ZXh0VmVydGljYWxBbGlnbjsgLy8gT3JpZ2luIG9mIHRleHRSb3RhdGlvbiBzaG91bGQgYmUgdGhlIGJhc2UgcG9pbnQgb2YgdGV4dCBkcmF3aW5nLlxuXG4gIGFwcGx5VGV4dFJvdGF0aW9uKGN0eCwgc3R5bGUsIHJlY3QsIGJhc2VYLCBiYXNlWSk7XG4gIHZhciBib3hYID0gdGV4dENvbnRhaW4uYWRqdXN0VGV4dFgoYmFzZVgsIG91dGVyV2lkdGgsIHRleHRBbGlnbik7XG4gIHZhciBib3hZID0gdGV4dENvbnRhaW4uYWRqdXN0VGV4dFkoYmFzZVksIG91dGVySGVpZ2h0LCB0ZXh0VmVydGljYWxBbGlnbik7XG4gIHZhciB4TGVmdCA9IGJveFg7XG4gIHZhciBsaW5lVG9wID0gYm94WTtcblxuICBpZiAodGV4dFBhZGRpbmcpIHtcbiAgICB4TGVmdCArPSB0ZXh0UGFkZGluZ1szXTtcbiAgICBsaW5lVG9wICs9IHRleHRQYWRkaW5nWzBdO1xuICB9XG5cbiAgdmFyIHhSaWdodCA9IHhMZWZ0ICsgY29udGVudFdpZHRoO1xuICBuZWVkRHJhd0JhY2tncm91bmQoc3R5bGUpICYmIGRyYXdCYWNrZ3JvdW5kKGhvc3RFbCwgY3R4LCBzdHlsZSwgYm94WCwgYm94WSwgb3V0ZXJXaWR0aCwgb3V0ZXJIZWlnaHQpO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgY29udGVudEJsb2NrLmxpbmVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGxpbmUgPSBjb250ZW50QmxvY2subGluZXNbaV07XG4gICAgdmFyIHRva2VucyA9IGxpbmUudG9rZW5zO1xuICAgIHZhciB0b2tlbkNvdW50ID0gdG9rZW5zLmxlbmd0aDtcbiAgICB2YXIgbGluZUhlaWdodCA9IGxpbmUubGluZUhlaWdodDtcbiAgICB2YXIgdXNlZFdpZHRoID0gbGluZS53aWR0aDtcbiAgICB2YXIgbGVmdEluZGV4ID0gMDtcbiAgICB2YXIgbGluZVhMZWZ0ID0geExlZnQ7XG4gICAgdmFyIGxpbmVYUmlnaHQgPSB4UmlnaHQ7XG4gICAgdmFyIHJpZ2h0SW5kZXggPSB0b2tlbkNvdW50IC0gMTtcbiAgICB2YXIgdG9rZW47XG5cbiAgICB3aGlsZSAobGVmdEluZGV4IDwgdG9rZW5Db3VudCAmJiAodG9rZW4gPSB0b2tlbnNbbGVmdEluZGV4XSwgIXRva2VuLnRleHRBbGlnbiB8fCB0b2tlbi50ZXh0QWxpZ24gPT09ICdsZWZ0JykpIHtcbiAgICAgIHBsYWNlVG9rZW4oaG9zdEVsLCBjdHgsIHRva2VuLCBzdHlsZSwgbGluZUhlaWdodCwgbGluZVRvcCwgbGluZVhMZWZ0LCAnbGVmdCcpO1xuICAgICAgdXNlZFdpZHRoIC09IHRva2VuLndpZHRoO1xuICAgICAgbGluZVhMZWZ0ICs9IHRva2VuLndpZHRoO1xuICAgICAgbGVmdEluZGV4Kys7XG4gICAgfVxuXG4gICAgd2hpbGUgKHJpZ2h0SW5kZXggPj0gMCAmJiAodG9rZW4gPSB0b2tlbnNbcmlnaHRJbmRleF0sIHRva2VuLnRleHRBbGlnbiA9PT0gJ3JpZ2h0JykpIHtcbiAgICAgIHBsYWNlVG9rZW4oaG9zdEVsLCBjdHgsIHRva2VuLCBzdHlsZSwgbGluZUhlaWdodCwgbGluZVRvcCwgbGluZVhSaWdodCwgJ3JpZ2h0Jyk7XG4gICAgICB1c2VkV2lkdGggLT0gdG9rZW4ud2lkdGg7XG4gICAgICBsaW5lWFJpZ2h0IC09IHRva2VuLndpZHRoO1xuICAgICAgcmlnaHRJbmRleC0tO1xuICAgIH0gLy8gVGhlIG90aGVyIHRva2VucyBhcmUgcGxhY2VkIGFzIHRleHRBbGlnbiAnY2VudGVyJyBpZiB0aGVyZSBpcyBlbm91Z2ggc3BhY2UuXG5cblxuICAgIGxpbmVYTGVmdCArPSAoY29udGVudFdpZHRoIC0gKGxpbmVYTGVmdCAtIHhMZWZ0KSAtICh4UmlnaHQgLSBsaW5lWFJpZ2h0KSAtIHVzZWRXaWR0aCkgLyAyO1xuXG4gICAgd2hpbGUgKGxlZnRJbmRleCA8PSByaWdodEluZGV4KSB7XG4gICAgICB0b2tlbiA9IHRva2Vuc1tsZWZ0SW5kZXhdOyAvLyBDb25zaWRlciB3aWR0aCBzcGVjaWZpZWQgYnkgdXNlciwgdXNlICdjZW50ZXInIHJhdGhlciB0aGFuICdsZWZ0Jy5cblxuICAgICAgcGxhY2VUb2tlbihob3N0RWwsIGN0eCwgdG9rZW4sIHN0eWxlLCBsaW5lSGVpZ2h0LCBsaW5lVG9wLCBsaW5lWExlZnQgKyB0b2tlbi53aWR0aCAvIDIsICdjZW50ZXInKTtcbiAgICAgIGxpbmVYTGVmdCArPSB0b2tlbi53aWR0aDtcbiAgICAgIGxlZnRJbmRleCsrO1xuICAgIH1cblxuICAgIGxpbmVUb3AgKz0gbGluZUhlaWdodDtcbiAgfVxufVxuXG5mdW5jdGlvbiBhcHBseVRleHRSb3RhdGlvbihjdHgsIHN0eWxlLCByZWN0LCB4LCB5KSB7XG4gIC8vIHRleHRSb3RhdGlvbiBvbmx5IGFwcGx5IGluIFJlY3RUZXh0LlxuICBpZiAocmVjdCAmJiBzdHlsZS50ZXh0Um90YXRpb24pIHtcbiAgICB2YXIgb3JpZ2luID0gc3R5bGUudGV4dE9yaWdpbjtcblxuICAgIGlmIChvcmlnaW4gPT09ICdjZW50ZXInKSB7XG4gICAgICB4ID0gcmVjdC53aWR0aCAvIDIgKyByZWN0Lng7XG4gICAgICB5ID0gcmVjdC5oZWlnaHQgLyAyICsgcmVjdC55O1xuICAgIH0gZWxzZSBpZiAob3JpZ2luKSB7XG4gICAgICB4ID0gb3JpZ2luWzBdICsgcmVjdC54O1xuICAgICAgeSA9IG9yaWdpblsxXSArIHJlY3QueTtcbiAgICB9XG5cbiAgICBjdHgudHJhbnNsYXRlKHgsIHkpOyAvLyBQb3NpdGl2ZTogYW50aWNsb2Nrd2lzZVxuXG4gICAgY3R4LnJvdGF0ZSgtc3R5bGUudGV4dFJvdGF0aW9uKTtcbiAgICBjdHgudHJhbnNsYXRlKC14LCAteSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcGxhY2VUb2tlbihob3N0RWwsIGN0eCwgdG9rZW4sIHN0eWxlLCBsaW5lSGVpZ2h0LCBsaW5lVG9wLCB4LCB0ZXh0QWxpZ24pIHtcbiAgdmFyIHRva2VuU3R5bGUgPSBzdHlsZS5yaWNoW3Rva2VuLnN0eWxlTmFtZV0gfHwge307XG4gIHRva2VuU3R5bGUudGV4dCA9IHRva2VuLnRleHQ7IC8vICdjdHgudGV4dEJhc2VsaW5lJyBpcyBhbHdheXMgc2V0IGFzICdtaWRkbGUnLCBmb3Igc2FrZSBvZlxuICAvLyB0aGUgYmlhcyBvZiBcIk1pY3Jvc29mdCBZYUhlaVwiLlxuXG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbiA9IHRva2VuLnRleHRWZXJ0aWNhbEFsaWduO1xuICB2YXIgeSA9IGxpbmVUb3AgKyBsaW5lSGVpZ2h0IC8gMjtcblxuICBpZiAodGV4dFZlcnRpY2FsQWxpZ24gPT09ICd0b3AnKSB7XG4gICAgeSA9IGxpbmVUb3AgKyB0b2tlbi5oZWlnaHQgLyAyO1xuICB9IGVsc2UgaWYgKHRleHRWZXJ0aWNhbEFsaWduID09PSAnYm90dG9tJykge1xuICAgIHkgPSBsaW5lVG9wICsgbGluZUhlaWdodCAtIHRva2VuLmhlaWdodCAvIDI7XG4gIH1cblxuICAhdG9rZW4uaXNMaW5lSG9sZGVyICYmIG5lZWREcmF3QmFja2dyb3VuZCh0b2tlblN0eWxlKSAmJiBkcmF3QmFja2dyb3VuZChob3N0RWwsIGN0eCwgdG9rZW5TdHlsZSwgdGV4dEFsaWduID09PSAncmlnaHQnID8geCAtIHRva2VuLndpZHRoIDogdGV4dEFsaWduID09PSAnY2VudGVyJyA/IHggLSB0b2tlbi53aWR0aCAvIDIgOiB4LCB5IC0gdG9rZW4uaGVpZ2h0IC8gMiwgdG9rZW4ud2lkdGgsIHRva2VuLmhlaWdodCk7XG4gIHZhciB0ZXh0UGFkZGluZyA9IHRva2VuLnRleHRQYWRkaW5nO1xuXG4gIGlmICh0ZXh0UGFkZGluZykge1xuICAgIHggPSBnZXRUZXh0WEZvclBhZGRpbmcoeCwgdGV4dEFsaWduLCB0ZXh0UGFkZGluZyk7XG4gICAgeSAtPSB0b2tlbi5oZWlnaHQgLyAyIC0gdGV4dFBhZGRpbmdbMl0gLSB0b2tlbi50ZXh0SGVpZ2h0IC8gMjtcbiAgfVxuXG4gIHNldEN0eChjdHgsICdzaGFkb3dCbHVyJywgcmV0cmlldmUzKHRva2VuU3R5bGUudGV4dFNoYWRvd0JsdXIsIHN0eWxlLnRleHRTaGFkb3dCbHVyLCAwKSk7XG4gIHNldEN0eChjdHgsICdzaGFkb3dDb2xvcicsIHRva2VuU3R5bGUudGV4dFNoYWRvd0NvbG9yIHx8IHN0eWxlLnRleHRTaGFkb3dDb2xvciB8fCAndHJhbnNwYXJlbnQnKTtcbiAgc2V0Q3R4KGN0eCwgJ3NoYWRvd09mZnNldFgnLCByZXRyaWV2ZTModG9rZW5TdHlsZS50ZXh0U2hhZG93T2Zmc2V0WCwgc3R5bGUudGV4dFNoYWRvd09mZnNldFgsIDApKTtcbiAgc2V0Q3R4KGN0eCwgJ3NoYWRvd09mZnNldFknLCByZXRyaWV2ZTModG9rZW5TdHlsZS50ZXh0U2hhZG93T2Zmc2V0WSwgc3R5bGUudGV4dFNoYWRvd09mZnNldFksIDApKTtcbiAgc2V0Q3R4KGN0eCwgJ3RleHRBbGlnbicsIHRleHRBbGlnbik7IC8vIEZvcmNlIGJhc2VsaW5lIHRvIGJlIFwibWlkZGxlXCIuIE90aGVyd2lzZSwgaWYgdXNpbmcgXCJ0b3BcIiwgdGhlXG4gIC8vIHRleHQgd2lsbCBvZmZzZXQgZG93bndhcmQgYSBsaXR0bGUgYml0IGluIGZvbnQgXCJNaWNyb3NvZnQgWWFIZWlcIi5cblxuICBzZXRDdHgoY3R4LCAndGV4dEJhc2VsaW5lJywgJ21pZGRsZScpO1xuICBzZXRDdHgoY3R4LCAnZm9udCcsIHRva2VuLmZvbnQgfHwgREVGQVVMVF9GT05UKTtcbiAgdmFyIHRleHRTdHJva2UgPSBnZXRTdHJva2UodG9rZW5TdHlsZS50ZXh0U3Ryb2tlIHx8IHN0eWxlLnRleHRTdHJva2UsIHRleHRTdHJva2VXaWR0aCk7XG4gIHZhciB0ZXh0RmlsbCA9IGdldEZpbGwodG9rZW5TdHlsZS50ZXh0RmlsbCB8fCBzdHlsZS50ZXh0RmlsbCk7XG4gIHZhciB0ZXh0U3Ryb2tlV2lkdGggPSByZXRyaWV2ZTIodG9rZW5TdHlsZS50ZXh0U3Ryb2tlV2lkdGgsIHN0eWxlLnRleHRTdHJva2VXaWR0aCk7IC8vIEZpbGwgYWZ0ZXIgc3Ryb2tlIHNvIHRoZSBvdXRsaW5lIHdpbGwgbm90IGNvdmVyIHRoZSBtYWluIHBhcnQuXG5cbiAgaWYgKHRleHRTdHJva2UpIHtcbiAgICBzZXRDdHgoY3R4LCAnbGluZVdpZHRoJywgdGV4dFN0cm9rZVdpZHRoKTtcbiAgICBzZXRDdHgoY3R4LCAnc3Ryb2tlU3R5bGUnLCB0ZXh0U3Ryb2tlKTtcbiAgICBjdHguc3Ryb2tlVGV4dCh0b2tlbi50ZXh0LCB4LCB5KTtcbiAgfVxuXG4gIGlmICh0ZXh0RmlsbCkge1xuICAgIHNldEN0eChjdHgsICdmaWxsU3R5bGUnLCB0ZXh0RmlsbCk7XG4gICAgY3R4LmZpbGxUZXh0KHRva2VuLnRleHQsIHgsIHkpO1xuICB9XG59XG5cbmZ1bmN0aW9uIG5lZWREcmF3QmFja2dyb3VuZChzdHlsZSkge1xuICByZXR1cm4gISEoc3R5bGUudGV4dEJhY2tncm91bmRDb2xvciB8fCBzdHlsZS50ZXh0Qm9yZGVyV2lkdGggJiYgc3R5bGUudGV4dEJvcmRlckNvbG9yKTtcbn0gLy8gc3R5bGU6IHt0ZXh0QmFja2dyb3VuZENvbG9yLCB0ZXh0Qm9yZGVyV2lkdGgsIHRleHRCb3JkZXJDb2xvciwgdGV4dEJvcmRlclJhZGl1cywgdGV4dH1cbi8vIHNoYXBlOiB7eCwgeSwgd2lkdGgsIGhlaWdodH1cblxuXG5mdW5jdGlvbiBkcmF3QmFja2dyb3VuZChob3N0RWwsIGN0eCwgc3R5bGUsIHgsIHksIHdpZHRoLCBoZWlnaHQpIHtcbiAgdmFyIHRleHRCYWNrZ3JvdW5kQ29sb3IgPSBzdHlsZS50ZXh0QmFja2dyb3VuZENvbG9yO1xuICB2YXIgdGV4dEJvcmRlcldpZHRoID0gc3R5bGUudGV4dEJvcmRlcldpZHRoO1xuICB2YXIgdGV4dEJvcmRlckNvbG9yID0gc3R5bGUudGV4dEJvcmRlckNvbG9yO1xuICB2YXIgaXNQbGFpbkJnID0gaXNTdHJpbmcodGV4dEJhY2tncm91bmRDb2xvcik7XG4gIHNldEN0eChjdHgsICdzaGFkb3dCbHVyJywgc3R5bGUudGV4dEJveFNoYWRvd0JsdXIgfHwgMCk7XG4gIHNldEN0eChjdHgsICdzaGFkb3dDb2xvcicsIHN0eWxlLnRleHRCb3hTaGFkb3dDb2xvciB8fCAndHJhbnNwYXJlbnQnKTtcbiAgc2V0Q3R4KGN0eCwgJ3NoYWRvd09mZnNldFgnLCBzdHlsZS50ZXh0Qm94U2hhZG93T2Zmc2V0WCB8fCAwKTtcbiAgc2V0Q3R4KGN0eCwgJ3NoYWRvd09mZnNldFknLCBzdHlsZS50ZXh0Qm94U2hhZG93T2Zmc2V0WSB8fCAwKTtcblxuICBpZiAoaXNQbGFpbkJnIHx8IHRleHRCb3JkZXJXaWR0aCAmJiB0ZXh0Qm9yZGVyQ29sb3IpIHtcbiAgICBjdHguYmVnaW5QYXRoKCk7XG4gICAgdmFyIHRleHRCb3JkZXJSYWRpdXMgPSBzdHlsZS50ZXh0Qm9yZGVyUmFkaXVzO1xuXG4gICAgaWYgKCF0ZXh0Qm9yZGVyUmFkaXVzKSB7XG4gICAgICBjdHgucmVjdCh4LCB5LCB3aWR0aCwgaGVpZ2h0KTtcbiAgICB9IGVsc2Uge1xuICAgICAgcm91bmRSZWN0SGVscGVyLmJ1aWxkUGF0aChjdHgsIHtcbiAgICAgICAgeDogeCxcbiAgICAgICAgeTogeSxcbiAgICAgICAgd2lkdGg6IHdpZHRoLFxuICAgICAgICBoZWlnaHQ6IGhlaWdodCxcbiAgICAgICAgcjogdGV4dEJvcmRlclJhZGl1c1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgY3R4LmNsb3NlUGF0aCgpO1xuICB9XG5cbiAgaWYgKGlzUGxhaW5CZykge1xuICAgIHNldEN0eChjdHgsICdmaWxsU3R5bGUnLCB0ZXh0QmFja2dyb3VuZENvbG9yKTtcblxuICAgIGlmIChzdHlsZS5maWxsT3BhY2l0eSAhPSBudWxsKSB7XG4gICAgICB2YXIgb3JpZ2luYWxHbG9iYWxBbHBoYSA9IGN0eC5nbG9iYWxBbHBoYTtcbiAgICAgIGN0eC5nbG9iYWxBbHBoYSA9IHN0eWxlLmZpbGxPcGFjaXR5ICogc3R5bGUub3BhY2l0eTtcbiAgICAgIGN0eC5maWxsKCk7XG4gICAgICBjdHguZ2xvYmFsQWxwaGEgPSBvcmlnaW5hbEdsb2JhbEFscGhhO1xuICAgIH0gZWxzZSB7XG4gICAgICBjdHguZmlsbCgpO1xuICAgIH1cbiAgfSBlbHNlIGlmIChpc09iamVjdCh0ZXh0QmFja2dyb3VuZENvbG9yKSkge1xuICAgIHZhciBpbWFnZSA9IHRleHRCYWNrZ3JvdW5kQ29sb3IuaW1hZ2U7XG4gICAgaW1hZ2UgPSBpbWFnZUhlbHBlci5jcmVhdGVPclVwZGF0ZUltYWdlKGltYWdlLCBudWxsLCBob3N0RWwsIG9uQmdJbWFnZUxvYWRlZCwgdGV4dEJhY2tncm91bmRDb2xvcik7XG5cbiAgICBpZiAoaW1hZ2UgJiYgaW1hZ2VIZWxwZXIuaXNJbWFnZVJlYWR5KGltYWdlKSkge1xuICAgICAgY3R4LmRyYXdJbWFnZShpbWFnZSwgeCwgeSwgd2lkdGgsIGhlaWdodCk7XG4gICAgfVxuICB9XG5cbiAgaWYgKHRleHRCb3JkZXJXaWR0aCAmJiB0ZXh0Qm9yZGVyQ29sb3IpIHtcbiAgICBzZXRDdHgoY3R4LCAnbGluZVdpZHRoJywgdGV4dEJvcmRlcldpZHRoKTtcbiAgICBzZXRDdHgoY3R4LCAnc3Ryb2tlU3R5bGUnLCB0ZXh0Qm9yZGVyQ29sb3IpO1xuXG4gICAgaWYgKHN0eWxlLnN0cm9rZU9wYWNpdHkgIT0gbnVsbCkge1xuICAgICAgdmFyIG9yaWdpbmFsR2xvYmFsQWxwaGEgPSBjdHguZ2xvYmFsQWxwaGE7XG4gICAgICBjdHguZ2xvYmFsQWxwaGEgPSBzdHlsZS5zdHJva2VPcGFjaXR5ICogc3R5bGUub3BhY2l0eTtcbiAgICAgIGN0eC5zdHJva2UoKTtcbiAgICAgIGN0eC5nbG9iYWxBbHBoYSA9IG9yaWdpbmFsR2xvYmFsQWxwaGE7XG4gICAgfSBlbHNlIHtcbiAgICAgIGN0eC5zdHJva2UoKTtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gb25CZ0ltYWdlTG9hZGVkKGltYWdlLCB0ZXh0QmFja2dyb3VuZENvbG9yKSB7XG4gIC8vIFJlcGxhY2UgaW1hZ2UsIHNvIHRoYXQgYGNvbnRhaW4vdGV4dC5qcyNwYXJzZVJpY2hUZXh0YFxuICAvLyB3aWxsIGdldCBjb3JyZWN0IHJlc3VsdCBpbiBuZXh0IHRpY2suXG4gIHRleHRCYWNrZ3JvdW5kQ29sb3IuaW1hZ2UgPSBpbWFnZTtcbn1cblxuZnVuY3Rpb24gZ2V0Qm94UG9zaXRpb24oYmxvY2tIZWlodCwgc3R5bGUsIHJlY3QpIHtcbiAgdmFyIGJhc2VYID0gc3R5bGUueCB8fCAwO1xuICB2YXIgYmFzZVkgPSBzdHlsZS55IHx8IDA7XG4gIHZhciB0ZXh0QWxpZ24gPSBzdHlsZS50ZXh0QWxpZ247XG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbiA9IHN0eWxlLnRleHRWZXJ0aWNhbEFsaWduOyAvLyBUZXh0IHBvc2l0aW9uIHJlcHJlc2VudGVkIGJ5IGNvb3JkXG5cbiAgaWYgKHJlY3QpIHtcbiAgICB2YXIgdGV4dFBvc2l0aW9uID0gc3R5bGUudGV4dFBvc2l0aW9uO1xuXG4gICAgaWYgKHRleHRQb3NpdGlvbiBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAvLyBQZXJjZW50XG4gICAgICBiYXNlWCA9IHJlY3QueCArIHBhcnNlUGVyY2VudCh0ZXh0UG9zaXRpb25bMF0sIHJlY3Qud2lkdGgpO1xuICAgICAgYmFzZVkgPSByZWN0LnkgKyBwYXJzZVBlcmNlbnQodGV4dFBvc2l0aW9uWzFdLCByZWN0LmhlaWdodCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciByZXMgPSB0ZXh0Q29udGFpbi5hZGp1c3RUZXh0UG9zaXRpb25PblJlY3QodGV4dFBvc2l0aW9uLCByZWN0LCBzdHlsZS50ZXh0RGlzdGFuY2UpO1xuICAgICAgYmFzZVggPSByZXMueDtcbiAgICAgIGJhc2VZID0gcmVzLnk7IC8vIERlZmF1bHQgYWxpZ24gYW5kIGJhc2VsaW5lIHdoZW4gaGFzIHRleHRQb3NpdGlvblxuXG4gICAgICB0ZXh0QWxpZ24gPSB0ZXh0QWxpZ24gfHwgcmVzLnRleHRBbGlnbjtcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduID0gdGV4dFZlcnRpY2FsQWxpZ24gfHwgcmVzLnRleHRWZXJ0aWNhbEFsaWduO1xuICAgIH0gLy8gdGV4dE9mZnNldCBpcyBvbmx5IHN1cHBvcnQgaW4gUmVjdFRleHQsIG90aGVyd2lzZVxuICAgIC8vIHdlIGhhdmUgdG8gYWRqdXN0IGJvdW5kaW5nUmVjdCBmb3IgdGV4dE9mZnNldC5cblxuXG4gICAgdmFyIHRleHRPZmZzZXQgPSBzdHlsZS50ZXh0T2Zmc2V0O1xuXG4gICAgaWYgKHRleHRPZmZzZXQpIHtcbiAgICAgIGJhc2VYICs9IHRleHRPZmZzZXRbMF07XG4gICAgICBiYXNlWSArPSB0ZXh0T2Zmc2V0WzFdO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgYmFzZVg6IGJhc2VYLFxuICAgIGJhc2VZOiBiYXNlWSxcbiAgICB0ZXh0QWxpZ246IHRleHRBbGlnbixcbiAgICB0ZXh0VmVydGljYWxBbGlnbjogdGV4dFZlcnRpY2FsQWxpZ25cbiAgfTtcbn1cblxuZnVuY3Rpb24gc2V0Q3R4KGN0eCwgcHJvcCwgdmFsdWUpIHtcbiAgY3R4W3Byb3BdID0gZml4U2hhZG93KGN0eCwgcHJvcCwgdmFsdWUpO1xuICByZXR1cm4gY3R4W3Byb3BdO1xufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gW3N0cm9rZV0gSWYgc3BlY2lmaWVkLCBkbyBub3QgY2hlY2sgc3R5bGUudGV4dFN0cm9rZS5cbiAqIEBwYXJhbSB7c3RyaW5nfSBbbGluZVdpZHRoXSBJZiBzcGVjaWZpZWQsIGRvIG5vdCBjaGVjayBzdHlsZS50ZXh0U3Ryb2tlLlxuICogQHBhcmFtIHtudW1iZXJ9IHN0eWxlXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRTdHJva2Uoc3Ryb2tlLCBsaW5lV2lkdGgpIHtcbiAgcmV0dXJuIHN0cm9rZSA9PSBudWxsIHx8IGxpbmVXaWR0aCA8PSAwIHx8IHN0cm9rZSA9PT0gJ3RyYW5zcGFyZW50JyB8fCBzdHJva2UgPT09ICdub25lJyA/IG51bGwgLy8gVE9ETyBwYXR0ZXJuIGFuZCBncmFkaWVudD9cbiAgOiBzdHJva2UuaW1hZ2UgfHwgc3Ryb2tlLmNvbG9yU3RvcHMgPyAnIzAwMCcgOiBzdHJva2U7XG59XG5cbmZ1bmN0aW9uIGdldEZpbGwoZmlsbCkge1xuICByZXR1cm4gZmlsbCA9PSBudWxsIHx8IGZpbGwgPT09ICdub25lJyA/IG51bGwgLy8gVE9ETyBwYXR0ZXJuIGFuZCBncmFkaWVudD9cbiAgOiBmaWxsLmltYWdlIHx8IGZpbGwuY29sb3JTdG9wcyA/ICcjMDAwJyA6IGZpbGw7XG59XG5cbmZ1bmN0aW9uIHBhcnNlUGVyY2VudCh2YWx1ZSwgbWF4VmFsdWUpIHtcbiAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAodmFsdWUubGFzdEluZGV4T2YoJyUnKSA+PSAwKSB7XG4gICAgICByZXR1cm4gcGFyc2VGbG9hdCh2YWx1ZSkgLyAxMDAgKiBtYXhWYWx1ZTtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFyc2VGbG9hdCh2YWx1ZSk7XG4gIH1cblxuICByZXR1cm4gdmFsdWU7XG59XG5cbmZ1bmN0aW9uIGdldFRleHRYRm9yUGFkZGluZyh4LCB0ZXh0QWxpZ24sIHRleHRQYWRkaW5nKSB7XG4gIHJldHVybiB0ZXh0QWxpZ24gPT09ICdyaWdodCcgPyB4IC0gdGV4dFBhZGRpbmdbMV0gOiB0ZXh0QWxpZ24gPT09ICdjZW50ZXInID8geCArIHRleHRQYWRkaW5nWzNdIC8gMiAtIHRleHRQYWRkaW5nWzFdIC8gMiA6IHggKyB0ZXh0UGFkZGluZ1szXTtcbn1cbi8qKlxuICogQHBhcmFtIHtzdHJpbmd9IHRleHRcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvU3R5bGV9IHN0eWxlXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gbmVlZERyYXdUZXh0KHRleHQsIHN0eWxlKSB7XG4gIHJldHVybiB0ZXh0ICE9IG51bGwgJiYgKHRleHQgfHwgc3R5bGUudGV4dEJhY2tncm91bmRDb2xvciB8fCBzdHlsZS50ZXh0Qm9yZGVyV2lkdGggJiYgc3R5bGUudGV4dEJvcmRlckNvbG9yIHx8IHN0eWxlLnRleHRQYWRkaW5nKTtcbn1cblxuZXhwb3J0cy5ub3JtYWxpemVUZXh0U3R5bGUgPSBub3JtYWxpemVUZXh0U3R5bGU7XG5leHBvcnRzLnJlbmRlclRleHQgPSByZW5kZXJUZXh0O1xuZXhwb3J0cy5nZXRTdHJva2UgPSBnZXRTdHJva2U7XG5leHBvcnRzLmdldEZpbGwgPSBnZXRGaWxsO1xuZXhwb3J0cy5uZWVkRHJhd1RleHQgPSBuZWVkRHJhd1RleHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/text.js
