var util = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! ./core/env */ "./node_modules/zrender/lib/core/env.js");

var Group = __webpack_require__(/*! ./container/Group */ "./node_modules/zrender/lib/container/Group.js");

var timsort = __webpack_require__(/*! ./core/timsort */ "./node_modules/zrender/lib/core/timsort.js"); // Use timsort because in most case elements are partially sorted
// https://jsfiddle.net/pissang/jr4x7mdm/8/


function shapeCompareFunc(a, b) {
  if (a.zlevel === b.zlevel) {
    if (a.z === b.z) {
      // if (a.z2 === b.z2) {
      //     // FIXME Slow has renderidx compare
      //     // http://stackoverflow.com/questions/20883421/sorting-in-javascript-should-every-compare-function-have-a-return-0-statement
      //     // https://github.com/v8/v8/blob/47cce544a31ed5577ffe2963f67acb4144ee0232/src/js/array.js#L1012
      //     return a.__renderidx - b.__renderidx;
      // }
      return a.z2 - b.z2;
    }

    return a.z - b.z;
  }

  return a.zlevel - b.zlevel;
}
/**
 * 内容仓库 (M)
 * @alias module:zrender/Storage
 * @constructor
 */


var Storage = function Storage() {
  // jshint ignore:line
  this._roots = [];
  this._displayList = [];
  this._displayListLen = 0;
};

Storage.prototype = {
  constructor: Storage,

  /**
   * @param  {Function} cb
   *
   */
  traverse: function traverse(cb, context) {
    for (var i = 0; i < this._roots.length; i++) {
      this._roots[i].traverse(cb, context);
    }
  },

  /**
   * 返回所有图形的绘制队列
   * @param {boolean} [update=false] 是否在返回前更新该数组
   * @param {boolean} [includeIgnore=false] 是否包含 ignore 的数组, 在 update 为 true 的时候有效
   *
   * 详见{@link module:zrender/graphic/Displayable.prototype.updateDisplayList}
   * @return {Array.<module:zrender/graphic/Displayable>}
   */
  getDisplayList: function getDisplayList(update, includeIgnore) {
    includeIgnore = includeIgnore || false;

    if (update) {
      this.updateDisplayList(includeIgnore);
    }

    return this._displayList;
  },

  /**
   * 更新图形的绘制队列。
   * 每次绘制前都会调用，该方法会先深度优先遍历整个树，更新所有Group和Shape的变换并且把所有可见的Shape保存到数组中，
   * 最后根据绘制的优先级（zlevel > z > 插入顺序）排序得到绘制队列
   * @param {boolean} [includeIgnore=false] 是否包含 ignore 的数组
   */
  updateDisplayList: function updateDisplayList(includeIgnore) {
    this._displayListLen = 0;
    var roots = this._roots;
    var displayList = this._displayList;

    for (var i = 0, len = roots.length; i < len; i++) {
      this._updateAndAddDisplayable(roots[i], null, includeIgnore);
    }

    displayList.length = this._displayListLen;
    env.canvasSupported && timsort(displayList, shapeCompareFunc);
  },
  _updateAndAddDisplayable: function _updateAndAddDisplayable(el, clipPaths, includeIgnore) {
    if (el.ignore && !includeIgnore) {
      return;
    }

    el.beforeUpdate();

    if (el.__dirty) {
      el.update();
    }

    el.afterUpdate();
    var userSetClipPath = el.clipPath;

    if (userSetClipPath) {
      // FIXME 效率影响
      if (clipPaths) {
        clipPaths = clipPaths.slice();
      } else {
        clipPaths = [];
      }

      var currentClipPath = userSetClipPath;
      var parentClipPath = el; // Recursively add clip path

      while (currentClipPath) {
        // clipPath 的变换是基于使用这个 clipPath 的元素
        currentClipPath.parent = parentClipPath;
        currentClipPath.updateTransform();
        clipPaths.push(currentClipPath);
        parentClipPath = currentClipPath;
        currentClipPath = currentClipPath.clipPath;
      }
    }

    if (el.isGroup) {
      var children = el._children;

      for (var i = 0; i < children.length; i++) {
        var child = children[i]; // Force to mark as dirty if group is dirty
        // FIXME __dirtyPath ?

        if (el.__dirty) {
          child.__dirty = true;
        }

        this._updateAndAddDisplayable(child, clipPaths, includeIgnore);
      } // Mark group clean here


      el.__dirty = false;
    } else {
      el.__clipPaths = clipPaths;
      this._displayList[this._displayListLen++] = el;
    }
  },

  /**
   * 添加图形(Shape)或者组(Group)到根节点
   * @param {module:zrender/Element} el
   */
  addRoot: function addRoot(el) {
    if (el.__storage === this) {
      return;
    }

    if (el instanceof Group) {
      el.addChildrenToStorage(this);
    }

    this.addToStorage(el);

    this._roots.push(el);
  },

  /**
   * 删除指定的图形(Shape)或者组(Group)
   * @param {string|Array.<string>} [el] 如果为空清空整个Storage
   */
  delRoot: function delRoot(el) {
    if (el == null) {
      // 不指定el清空
      for (var i = 0; i < this._roots.length; i++) {
        var root = this._roots[i];

        if (root instanceof Group) {
          root.delChildrenFromStorage(this);
        }
      }

      this._roots = [];
      this._displayList = [];
      this._displayListLen = 0;
      return;
    }

    if (el instanceof Array) {
      for (var i = 0, l = el.length; i < l; i++) {
        this.delRoot(el[i]);
      }

      return;
    }

    var idx = util.indexOf(this._roots, el);

    if (idx >= 0) {
      this.delFromStorage(el);

      this._roots.splice(idx, 1);

      if (el instanceof Group) {
        el.delChildrenFromStorage(this);
      }
    }
  },
  addToStorage: function addToStorage(el) {
    if (el) {
      el.__storage = this;
      el.dirty(false);
    }

    return this;
  },
  delFromStorage: function delFromStorage(el) {
    if (el) {
      el.__storage = null;
    }

    return this;
  },

  /**
   * 清空并且释放Storage
   */
  dispose: function dispose() {
    this._renderList = this._roots = null;
  },
  displayableSortFunc: shapeCompareFunc
};
var _default = Storage;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvU3RvcmFnZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL1N0b3JhZ2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHV0aWwgPSByZXF1aXJlKFwiLi9jb3JlL3V0aWxcIik7XG5cbnZhciBlbnYgPSByZXF1aXJlKFwiLi9jb3JlL2VudlwiKTtcblxudmFyIEdyb3VwID0gcmVxdWlyZShcIi4vY29udGFpbmVyL0dyb3VwXCIpO1xuXG52YXIgdGltc29ydCA9IHJlcXVpcmUoXCIuL2NvcmUvdGltc29ydFwiKTtcblxuLy8gVXNlIHRpbXNvcnQgYmVjYXVzZSBpbiBtb3N0IGNhc2UgZWxlbWVudHMgYXJlIHBhcnRpYWxseSBzb3J0ZWRcbi8vIGh0dHBzOi8vanNmaWRkbGUubmV0L3Bpc3NhbmcvanI0eDdtZG0vOC9cbmZ1bmN0aW9uIHNoYXBlQ29tcGFyZUZ1bmMoYSwgYikge1xuICBpZiAoYS56bGV2ZWwgPT09IGIuemxldmVsKSB7XG4gICAgaWYgKGEueiA9PT0gYi56KSB7XG4gICAgICAvLyBpZiAoYS56MiA9PT0gYi56Mikge1xuICAgICAgLy8gICAgIC8vIEZJWE1FIFNsb3cgaGFzIHJlbmRlcmlkeCBjb21wYXJlXG4gICAgICAvLyAgICAgLy8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8yMDg4MzQyMS9zb3J0aW5nLWluLWphdmFzY3JpcHQtc2hvdWxkLWV2ZXJ5LWNvbXBhcmUtZnVuY3Rpb24taGF2ZS1hLXJldHVybi0wLXN0YXRlbWVudFxuICAgICAgLy8gICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92OC92OC9ibG9iLzQ3Y2NlNTQ0YTMxZWQ1NTc3ZmZlMjk2M2Y2N2FjYjQxNDRlZTAyMzIvc3JjL2pzL2FycmF5LmpzI0wxMDEyXG4gICAgICAvLyAgICAgcmV0dXJuIGEuX19yZW5kZXJpZHggLSBiLl9fcmVuZGVyaWR4O1xuICAgICAgLy8gfVxuICAgICAgcmV0dXJuIGEuejIgLSBiLnoyO1xuICAgIH1cblxuICAgIHJldHVybiBhLnogLSBiLno7XG4gIH1cblxuICByZXR1cm4gYS56bGV2ZWwgLSBiLnpsZXZlbDtcbn1cbi8qKlxuICog5YaF5a655LuT5bqTIChNKVxuICogQGFsaWFzIG1vZHVsZTp6cmVuZGVyL1N0b3JhZ2VcbiAqIEBjb25zdHJ1Y3RvclxuICovXG5cblxudmFyIFN0b3JhZ2UgPSBmdW5jdGlvbiAoKSB7XG4gIC8vIGpzaGludCBpZ25vcmU6bGluZVxuICB0aGlzLl9yb290cyA9IFtdO1xuICB0aGlzLl9kaXNwbGF5TGlzdCA9IFtdO1xuICB0aGlzLl9kaXNwbGF5TGlzdExlbiA9IDA7XG59O1xuXG5TdG9yYWdlLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IFN0b3JhZ2UsXG5cbiAgLyoqXG4gICAqIEBwYXJhbSAge0Z1bmN0aW9ufSBjYlxuICAgKlxuICAgKi9cbiAgdHJhdmVyc2U6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fcm9vdHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHRoaXMuX3Jvb3RzW2ldLnRyYXZlcnNlKGNiLCBjb250ZXh0KTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIOi/lOWbnuaJgOacieWbvuW9oueahOe7mOWItumYn+WIl1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IFt1cGRhdGU9ZmFsc2VdIOaYr+WQpuWcqOi/lOWbnuWJjeabtOaWsOivpeaVsOe7hFxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtpbmNsdWRlSWdub3JlPWZhbHNlXSDmmK/lkKbljIXlkKsgaWdub3JlIOeahOaVsOe7hCwg5ZyoIHVwZGF0ZSDkuLogdHJ1ZSDnmoTml7blgJnmnInmlYhcbiAgICpcbiAgICog6K+m6KeBe0BsaW5rIG1vZHVsZTp6cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGUucHJvdG90eXBlLnVwZGF0ZURpc3BsYXlMaXN0fVxuICAgKiBAcmV0dXJuIHtBcnJheS48bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9EaXNwbGF5YWJsZT59XG4gICAqL1xuICBnZXREaXNwbGF5TGlzdDogZnVuY3Rpb24gKHVwZGF0ZSwgaW5jbHVkZUlnbm9yZSkge1xuICAgIGluY2x1ZGVJZ25vcmUgPSBpbmNsdWRlSWdub3JlIHx8IGZhbHNlO1xuXG4gICAgaWYgKHVwZGF0ZSkge1xuICAgICAgdGhpcy51cGRhdGVEaXNwbGF5TGlzdChpbmNsdWRlSWdub3JlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5fZGlzcGxheUxpc3Q7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOabtOaWsOWbvuW9oueahOe7mOWItumYn+WIl+OAglxuICAgKiDmr4/mrKHnu5jliLbliY3pg73kvJrosIPnlKjvvIzor6Xmlrnms5XkvJrlhYjmt7HluqbkvJjlhYjpgY3ljobmlbTkuKrmoJHvvIzmm7TmlrDmiYDmnIlHcm91cOWSjFNoYXBl55qE5Y+Y5o2i5bm25LiU5oqK5omA5pyJ5Y+v6KeB55qEU2hhcGXkv53lrZjliLDmlbDnu4TkuK3vvIxcbiAgICog5pyA5ZCO5qC55o2u57uY5Yi255qE5LyY5YWI57qn77yIemxldmVsID4geiA+IOaPkuWFpemhuuW6j++8ieaOkuW6j+W+l+WIsOe7mOWItumYn+WIl1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IFtpbmNsdWRlSWdub3JlPWZhbHNlXSDmmK/lkKbljIXlkKsgaWdub3JlIOeahOaVsOe7hFxuICAgKi9cbiAgdXBkYXRlRGlzcGxheUxpc3Q6IGZ1bmN0aW9uIChpbmNsdWRlSWdub3JlKSB7XG4gICAgdGhpcy5fZGlzcGxheUxpc3RMZW4gPSAwO1xuICAgIHZhciByb290cyA9IHRoaXMuX3Jvb3RzO1xuICAgIHZhciBkaXNwbGF5TGlzdCA9IHRoaXMuX2Rpc3BsYXlMaXN0O1xuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IHJvb3RzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICB0aGlzLl91cGRhdGVBbmRBZGREaXNwbGF5YWJsZShyb290c1tpXSwgbnVsbCwgaW5jbHVkZUlnbm9yZSk7XG4gICAgfVxuXG4gICAgZGlzcGxheUxpc3QubGVuZ3RoID0gdGhpcy5fZGlzcGxheUxpc3RMZW47XG4gICAgZW52LmNhbnZhc1N1cHBvcnRlZCAmJiB0aW1zb3J0KGRpc3BsYXlMaXN0LCBzaGFwZUNvbXBhcmVGdW5jKTtcbiAgfSxcbiAgX3VwZGF0ZUFuZEFkZERpc3BsYXlhYmxlOiBmdW5jdGlvbiAoZWwsIGNsaXBQYXRocywgaW5jbHVkZUlnbm9yZSkge1xuICAgIGlmIChlbC5pZ25vcmUgJiYgIWluY2x1ZGVJZ25vcmUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBlbC5iZWZvcmVVcGRhdGUoKTtcblxuICAgIGlmIChlbC5fX2RpcnR5KSB7XG4gICAgICBlbC51cGRhdGUoKTtcbiAgICB9XG5cbiAgICBlbC5hZnRlclVwZGF0ZSgpO1xuICAgIHZhciB1c2VyU2V0Q2xpcFBhdGggPSBlbC5jbGlwUGF0aDtcblxuICAgIGlmICh1c2VyU2V0Q2xpcFBhdGgpIHtcbiAgICAgIC8vIEZJWE1FIOaViOeOh+W9seWTjVxuICAgICAgaWYgKGNsaXBQYXRocykge1xuICAgICAgICBjbGlwUGF0aHMgPSBjbGlwUGF0aHMuc2xpY2UoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNsaXBQYXRocyA9IFtdO1xuICAgICAgfVxuXG4gICAgICB2YXIgY3VycmVudENsaXBQYXRoID0gdXNlclNldENsaXBQYXRoO1xuICAgICAgdmFyIHBhcmVudENsaXBQYXRoID0gZWw7IC8vIFJlY3Vyc2l2ZWx5IGFkZCBjbGlwIHBhdGhcblxuICAgICAgd2hpbGUgKGN1cnJlbnRDbGlwUGF0aCkge1xuICAgICAgICAvLyBjbGlwUGF0aCDnmoTlj5jmjaLmmK/ln7rkuo7kvb/nlKjov5nkuKogY2xpcFBhdGgg55qE5YWD57SgXG4gICAgICAgIGN1cnJlbnRDbGlwUGF0aC5wYXJlbnQgPSBwYXJlbnRDbGlwUGF0aDtcbiAgICAgICAgY3VycmVudENsaXBQYXRoLnVwZGF0ZVRyYW5zZm9ybSgpO1xuICAgICAgICBjbGlwUGF0aHMucHVzaChjdXJyZW50Q2xpcFBhdGgpO1xuICAgICAgICBwYXJlbnRDbGlwUGF0aCA9IGN1cnJlbnRDbGlwUGF0aDtcbiAgICAgICAgY3VycmVudENsaXBQYXRoID0gY3VycmVudENsaXBQYXRoLmNsaXBQYXRoO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChlbC5pc0dyb3VwKSB7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBlbC5fY2hpbGRyZW47XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGNoaWxkID0gY2hpbGRyZW5baV07IC8vIEZvcmNlIHRvIG1hcmsgYXMgZGlydHkgaWYgZ3JvdXAgaXMgZGlydHlcbiAgICAgICAgLy8gRklYTUUgX19kaXJ0eVBhdGggP1xuXG4gICAgICAgIGlmIChlbC5fX2RpcnR5KSB7XG4gICAgICAgICAgY2hpbGQuX19kaXJ0eSA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl91cGRhdGVBbmRBZGREaXNwbGF5YWJsZShjaGlsZCwgY2xpcFBhdGhzLCBpbmNsdWRlSWdub3JlKTtcbiAgICAgIH0gLy8gTWFyayBncm91cCBjbGVhbiBoZXJlXG5cblxuICAgICAgZWwuX19kaXJ0eSA9IGZhbHNlO1xuICAgIH0gZWxzZSB7XG4gICAgICBlbC5fX2NsaXBQYXRocyA9IGNsaXBQYXRocztcbiAgICAgIHRoaXMuX2Rpc3BsYXlMaXN0W3RoaXMuX2Rpc3BsYXlMaXN0TGVuKytdID0gZWw7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiDmt7vliqDlm77lvaIoU2hhcGUp5oiW6ICF57uEKEdyb3VwKeWIsOagueiKgueCuVxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IGVsXG4gICAqL1xuICBhZGRSb290OiBmdW5jdGlvbiAoZWwpIHtcbiAgICBpZiAoZWwuX19zdG9yYWdlID09PSB0aGlzKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKGVsIGluc3RhbmNlb2YgR3JvdXApIHtcbiAgICAgIGVsLmFkZENoaWxkcmVuVG9TdG9yYWdlKHRoaXMpO1xuICAgIH1cblxuICAgIHRoaXMuYWRkVG9TdG9yYWdlKGVsKTtcblxuICAgIHRoaXMuX3Jvb3RzLnB1c2goZWwpO1xuICB9LFxuXG4gIC8qKlxuICAgKiDliKDpmaTmjIflrprnmoTlm77lvaIoU2hhcGUp5oiW6ICF57uEKEdyb3VwKVxuICAgKiBAcGFyYW0ge3N0cmluZ3xBcnJheS48c3RyaW5nPn0gW2VsXSDlpoLmnpzkuLrnqbrmuIXnqbrmlbTkuKpTdG9yYWdlXG4gICAqL1xuICBkZWxSb290OiBmdW5jdGlvbiAoZWwpIHtcbiAgICBpZiAoZWwgPT0gbnVsbCkge1xuICAgICAgLy8g5LiN5oyH5a6aZWzmuIXnqbpcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fcm9vdHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIHJvb3QgPSB0aGlzLl9yb290c1tpXTtcblxuICAgICAgICBpZiAocm9vdCBpbnN0YW5jZW9mIEdyb3VwKSB7XG4gICAgICAgICAgcm9vdC5kZWxDaGlsZHJlbkZyb21TdG9yYWdlKHRoaXMpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMuX3Jvb3RzID0gW107XG4gICAgICB0aGlzLl9kaXNwbGF5TGlzdCA9IFtdO1xuICAgICAgdGhpcy5fZGlzcGxheUxpc3RMZW4gPSAwO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChlbCBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICBmb3IgKHZhciBpID0gMCwgbCA9IGVsLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgICB0aGlzLmRlbFJvb3QoZWxbaV0pO1xuICAgICAgfVxuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGlkeCA9IHV0aWwuaW5kZXhPZih0aGlzLl9yb290cywgZWwpO1xuXG4gICAgaWYgKGlkeCA+PSAwKSB7XG4gICAgICB0aGlzLmRlbEZyb21TdG9yYWdlKGVsKTtcblxuICAgICAgdGhpcy5fcm9vdHMuc3BsaWNlKGlkeCwgMSk7XG5cbiAgICAgIGlmIChlbCBpbnN0YW5jZW9mIEdyb3VwKSB7XG4gICAgICAgIGVsLmRlbENoaWxkcmVuRnJvbVN0b3JhZ2UodGhpcyk7XG4gICAgICB9XG4gICAgfVxuICB9LFxuICBhZGRUb1N0b3JhZ2U6IGZ1bmN0aW9uIChlbCkge1xuICAgIGlmIChlbCkge1xuICAgICAgZWwuX19zdG9yYWdlID0gdGhpcztcbiAgICAgIGVsLmRpcnR5KGZhbHNlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcbiAgZGVsRnJvbVN0b3JhZ2U6IGZ1bmN0aW9uIChlbCkge1xuICAgIGlmIChlbCkge1xuICAgICAgZWwuX19zdG9yYWdlID0gbnVsbDtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICog5riF56m65bm25LiU6YeK5pS+U3RvcmFnZVxuICAgKi9cbiAgZGlzcG9zZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuX3JlbmRlckxpc3QgPSB0aGlzLl9yb290cyA9IG51bGw7XG4gIH0sXG4gIGRpc3BsYXlhYmxlU29ydEZ1bmM6IHNoYXBlQ29tcGFyZUZ1bmNcbn07XG52YXIgX2RlZmF1bHQgPSBTdG9yYWdlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQTNMQTtBQTZMQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/Storage.js
