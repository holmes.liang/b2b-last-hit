/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var axisPointerModelHelper = __webpack_require__(/*! ./axisPointer/modelHelper */ "./node_modules/echarts/lib/component/axisPointer/modelHelper.js");

var axisTrigger = __webpack_require__(/*! ./axisPointer/axisTrigger */ "./node_modules/echarts/lib/component/axisPointer/axisTrigger.js");

__webpack_require__(/*! ./axisPointer/AxisPointerModel */ "./node_modules/echarts/lib/component/axisPointer/AxisPointerModel.js");

__webpack_require__(/*! ./axisPointer/AxisPointerView */ "./node_modules/echarts/lib/component/axisPointer/AxisPointerView.js");

__webpack_require__(/*! ./axisPointer/CartesianAxisPointer */ "./node_modules/echarts/lib/component/axisPointer/CartesianAxisPointer.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// CartesianAxisPointer is not supposed to be required here. But consider
// echarts.simple.js and online build tooltip, which only require gridSimple,
// CartesianAxisPointer should be able to required somewhere.


echarts.registerPreprocessor(function (option) {
  // Always has a global axisPointerModel for default setting.
  if (option) {
    (!option.axisPointer || option.axisPointer.length === 0) && (option.axisPointer = {});
    var link = option.axisPointer.link; // Normalize to array to avoid object mergin. But if link
    // is not set, remain null/undefined, otherwise it will
    // override existent link setting.

    if (link && !zrUtil.isArray(link)) {
      option.axisPointer.link = [link];
    }
  }
}); // This process should proformed after coordinate systems created
// and series data processed. So put it on statistic processing stage.

echarts.registerProcessor(echarts.PRIORITY.PROCESSOR.STATISTIC, function (ecModel, api) {
  // Build axisPointerModel, mergin tooltip.axisPointer model for each axis.
  // allAxesInfo should be updated when setOption performed.
  ecModel.getComponent('axisPointer').coordSysAxesInfo = axisPointerModelHelper.collect(ecModel, api);
}); // Broadcast to all views.

echarts.registerAction({
  type: 'updateAxisPointer',
  event: 'updateAxisPointer',
  update: ':updateAxisPointer'
}, axisTrigger);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgZWNoYXJ0cyA9IHJlcXVpcmUoXCIuLi9lY2hhcnRzXCIpO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGF4aXNQb2ludGVyTW9kZWxIZWxwZXIgPSByZXF1aXJlKFwiLi9heGlzUG9pbnRlci9tb2RlbEhlbHBlclwiKTtcblxudmFyIGF4aXNUcmlnZ2VyID0gcmVxdWlyZShcIi4vYXhpc1BvaW50ZXIvYXhpc1RyaWdnZXJcIik7XG5cbnJlcXVpcmUoXCIuL2F4aXNQb2ludGVyL0F4aXNQb2ludGVyTW9kZWxcIik7XG5cbnJlcXVpcmUoXCIuL2F4aXNQb2ludGVyL0F4aXNQb2ludGVyVmlld1wiKTtcblxucmVxdWlyZShcIi4vYXhpc1BvaW50ZXIvQ2FydGVzaWFuQXhpc1BvaW50ZXJcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIENhcnRlc2lhbkF4aXNQb2ludGVyIGlzIG5vdCBzdXBwb3NlZCB0byBiZSByZXF1aXJlZCBoZXJlLiBCdXQgY29uc2lkZXJcbi8vIGVjaGFydHMuc2ltcGxlLmpzIGFuZCBvbmxpbmUgYnVpbGQgdG9vbHRpcCwgd2hpY2ggb25seSByZXF1aXJlIGdyaWRTaW1wbGUsXG4vLyBDYXJ0ZXNpYW5BeGlzUG9pbnRlciBzaG91bGQgYmUgYWJsZSB0byByZXF1aXJlZCBzb21ld2hlcmUuXG5lY2hhcnRzLnJlZ2lzdGVyUHJlcHJvY2Vzc29yKGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgLy8gQWx3YXlzIGhhcyBhIGdsb2JhbCBheGlzUG9pbnRlck1vZGVsIGZvciBkZWZhdWx0IHNldHRpbmcuXG4gIGlmIChvcHRpb24pIHtcbiAgICAoIW9wdGlvbi5heGlzUG9pbnRlciB8fCBvcHRpb24uYXhpc1BvaW50ZXIubGVuZ3RoID09PSAwKSAmJiAob3B0aW9uLmF4aXNQb2ludGVyID0ge30pO1xuICAgIHZhciBsaW5rID0gb3B0aW9uLmF4aXNQb2ludGVyLmxpbms7IC8vIE5vcm1hbGl6ZSB0byBhcnJheSB0byBhdm9pZCBvYmplY3QgbWVyZ2luLiBCdXQgaWYgbGlua1xuICAgIC8vIGlzIG5vdCBzZXQsIHJlbWFpbiBudWxsL3VuZGVmaW5lZCwgb3RoZXJ3aXNlIGl0IHdpbGxcbiAgICAvLyBvdmVycmlkZSBleGlzdGVudCBsaW5rIHNldHRpbmcuXG5cbiAgICBpZiAobGluayAmJiAhenJVdGlsLmlzQXJyYXkobGluaykpIHtcbiAgICAgIG9wdGlvbi5heGlzUG9pbnRlci5saW5rID0gW2xpbmtdO1xuICAgIH1cbiAgfVxufSk7IC8vIFRoaXMgcHJvY2VzcyBzaG91bGQgcHJvZm9ybWVkIGFmdGVyIGNvb3JkaW5hdGUgc3lzdGVtcyBjcmVhdGVkXG4vLyBhbmQgc2VyaWVzIGRhdGEgcHJvY2Vzc2VkLiBTbyBwdXQgaXQgb24gc3RhdGlzdGljIHByb2Nlc3Npbmcgc3RhZ2UuXG5cbmVjaGFydHMucmVnaXN0ZXJQcm9jZXNzb3IoZWNoYXJ0cy5QUklPUklUWS5QUk9DRVNTT1IuU1RBVElTVElDLCBmdW5jdGlvbiAoZWNNb2RlbCwgYXBpKSB7XG4gIC8vIEJ1aWxkIGF4aXNQb2ludGVyTW9kZWwsIG1lcmdpbiB0b29sdGlwLmF4aXNQb2ludGVyIG1vZGVsIGZvciBlYWNoIGF4aXMuXG4gIC8vIGFsbEF4ZXNJbmZvIHNob3VsZCBiZSB1cGRhdGVkIHdoZW4gc2V0T3B0aW9uIHBlcmZvcm1lZC5cbiAgZWNNb2RlbC5nZXRDb21wb25lbnQoJ2F4aXNQb2ludGVyJykuY29vcmRTeXNBeGVzSW5mbyA9IGF4aXNQb2ludGVyTW9kZWxIZWxwZXIuY29sbGVjdChlY01vZGVsLCBhcGkpO1xufSk7IC8vIEJyb2FkY2FzdCB0byBhbGwgdmlld3MuXG5cbmVjaGFydHMucmVnaXN0ZXJBY3Rpb24oe1xuICB0eXBlOiAndXBkYXRlQXhpc1BvaW50ZXInLFxuICBldmVudDogJ3VwZGF0ZUF4aXNQb2ludGVyJyxcbiAgdXBkYXRlOiAnOnVwZGF0ZUF4aXNQb2ludGVyJ1xufSwgYXhpc1RyaWdnZXIpOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer.js
