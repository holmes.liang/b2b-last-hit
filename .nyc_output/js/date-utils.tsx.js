__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _common_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/index */ "./src/common/index.tsx");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);





var DateUtils =
/*#__PURE__*/
function () {
  function DateUtils() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, DateUtils);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(DateUtils, [{
    key: "now",
    value: function now() {
      if (this.getTimeZone()) return moment__WEBPACK_IMPORTED_MODULE_3___default()().utcOffset(this.getTimeZone());
      return moment__WEBPACK_IMPORTED_MODULE_3___default()();
    }
  }, {
    key: "formatNowWithTimeZone",
    value: function formatNowWithTimeZone() {
      return this.now().format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
    }
  }, {
    key: "formatDateTimeWithTimeZone",
    value: function formatDateTimeWithTimeZone(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
    }
    /**
     * format date to DD/MM/YYYY
     * @param moment
     */

  }, {
    key: "formatDate",
    value: function formatDate(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_FORMAT);
    }
  }, {
    key: "formatDateYYYY",
    value: function formatDateYYYY(moment) {
      // todo 判断moment类型
      return moment.format("YYYY/MM/DD");
    }
  }, {
    key: "formatDateDDMMYYYY",
    value: function formatDateDDMMYYYY(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_DDMMYYYY);
    }
  }, {
    key: "formatDateYYYYMMDD",
    value: function formatDateYYYYMMDD(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.YYYYMMDD_DATE);
    }
  }, {
    key: "formatDateTime",
    value: function formatDateTime(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_TIME_FORMAT);
    }
  }, {
    key: "formatDateMonth",
    value: function formatDateMonth(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_FORMAT_MONTH);
    }
  }, {
    key: "formatDateTimeZone",
    value: function formatDateTimeZone(moment) {
      // todo 判断moment类型
      return moment.format(_common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
    }
  }, {
    key: "nowWithTimeAtStartOfDay",
    value: function nowWithTimeAtStartOfDay() {
      return this.now().startOf("day");
    }
  }, {
    key: "nowWithTimeAtStartOfMonth",
    value: function nowWithTimeAtStartOfMonth() {
      return this.now().startOf("month");
    }
  }, {
    key: "nowWithTimeAtStartOfYear",
    value: function nowWithTimeAtStartOfYear() {
      return this.now().startOf("year");
    }
  }, {
    key: "nowWithTimeAtEndOfYear",
    value: function nowWithTimeAtEndOfYear() {
      return this.now().endOf("year");
    }
  }, {
    key: "nowWithTimeAtEndOfMonth",
    value: function nowWithTimeAtEndOfMonth() {
      return this.now().endOf("month");
    }
  }, {
    key: "plusXDaysFromNow",
    value: function plusXDaysFromNow(days) {
      return this.now().add(days, "days");
    }
  }, {
    key: "plusXWeeksFromNow",
    value: function plusXWeeksFromNow(weeks) {
      return this.now().add(weeks, "weeks");
    }
  }, {
    key: "plusXMonthsFromNow",
    value: function plusXMonthsFromNow(months) {
      return this.now().add(months, "months");
    }
  }, {
    key: "plusXYearsFromNow",
    value: function plusXYearsFromNow(years) {
      return this.now().add(years, "years");
    }
  }, {
    key: "minusXMonthsFromNow",
    value: function minusXMonthsFromNow(months) {
      return this.now().subtract(months, "months");
    }
  }, {
    key: "minusXDaysFromNow",
    value: function minusXDaysFromNow(days) {
      return this.now().subtract(days, "days");
    }
  }, {
    key: "minusXHoursFromNow",
    value: function minusXHoursFromNow(hours) {
      return this.now().subtract(hours, "hours");
    }
  }, {
    key: "minusXWeeksFromNow",
    value: function minusXWeeksFromNow(weeks) {
      return this.now().subtract(weeks, "weeks");
    }
  }, {
    key: "minusXYearsFromNow",
    value: function minusXYearsFromNow(years) {
      return this.now().subtract(years, "years");
    }
  }, {
    key: "toDate",
    value: function toDate(dateStringWithTimeZone) {
      return moment__WEBPACK_IMPORTED_MODULE_3___default()(dateStringWithTimeZone, _common_index__WEBPACK_IMPORTED_MODULE_2__["Consts"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE).utcOffset(this.getTimeZone());
    }
  }, {
    key: "diffDaysFromNow",
    value: function diffDaysFromNow(dateTime) {
      // todo 判断moment类型
      return this.now().diff(dateTime, "days");
    }
  }, {
    key: "getTimeZone",
    value: function getTimeZone() {
      return _common_index__WEBPACK_IMPORTED_MODULE_2__["Storage"].GlobalParams.session().get("timeZone");
    }
  }]);

  return DateUtils;
}();

/* harmony default export */ __webpack_exports__["default"] = (new DateUtils());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2RhdGUtdXRpbHMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL2RhdGUtdXRpbHMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbnN0cywgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uL2luZGV4XCI7XG5pbXBvcnQgbW9tZW50LCB7IE1vbWVudCB9IGZyb20gXCJtb21lbnRcIjtcblxuY2xhc3MgRGF0ZVV0aWxzIHtcbiAgbm93KCkge1xuICAgIGlmICh0aGlzLmdldFRpbWVab25lKCkpIHJldHVybiBtb21lbnQoKS51dGNPZmZzZXQodGhpcy5nZXRUaW1lWm9uZSgpKTtcbiAgICByZXR1cm4gbW9tZW50KCk7XG4gIH1cblxuICBmb3JtYXROb3dXaXRoVGltZVpvbmUoKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuZm9ybWF0KENvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX1RJTUVfV0lUSF9USU1FX1pPTkUpO1xuICB9XG5cbiAgZm9ybWF0RGF0ZVRpbWVXaXRoVGltZVpvbmUobW9tZW50OiBNb21lbnQpIHtcbiAgICAvLyB0b2RvIOWIpOaWrW1vbWVudOexu+Wei1xuICAgIHJldHVybiBtb21lbnQuZm9ybWF0KENvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX1RJTUVfV0lUSF9USU1FX1pPTkUpO1xuICB9XG5cbiAgLyoqXG4gICAqIGZvcm1hdCBkYXRlIHRvIEREL01NL1lZWVlcbiAgICogQHBhcmFtIG1vbWVudFxuICAgKi9cbiAgZm9ybWF0RGF0ZShtb21lbnQ6IE1vbWVudCkge1xuICAgIC8vIHRvZG8g5Yik5patbW9tZW5057G75Z6LXG4gICAgcmV0dXJuIG1vbWVudC5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUKTtcbiAgfVxuXG4gIGZvcm1hdERhdGVZWVlZKG1vbWVudDogTW9tZW50KSB7XG4gICAgLy8gdG9kbyDliKTmlq1tb21lbnTnsbvlnotcbiAgICByZXR1cm4gbW9tZW50LmZvcm1hdChcIllZWVkvTU0vRERcIik7XG4gIH1cblxuICBmb3JtYXREYXRlRERNTVlZWVkobW9tZW50OiBNb21lbnQpIHtcbiAgICAvLyB0b2RvIOWIpOaWrW1vbWVudOexu+Wei1xuICAgIHJldHVybiBtb21lbnQuZm9ybWF0KENvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX0RETU1ZWVlZKTtcbiAgfVxuXG4gIGZvcm1hdERhdGVZWVlZTU1ERChtb21lbnQ6IE1vbWVudCkge1xuICAgIC8vIHRvZG8g5Yik5patbW9tZW5057G75Z6LXG4gICAgcmV0dXJuIG1vbWVudC5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULllZWVlNTUREX0RBVEUpO1xuICB9XG5cbiAgZm9ybWF0RGF0ZVRpbWUobW9tZW50OiBNb21lbnQpIHtcbiAgICAvLyB0b2RvIOWIpOaWrW1vbWVudOexu+Wei1xuICAgIHJldHVybiBtb21lbnQuZm9ybWF0KENvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX1RJTUVfRk9STUFUKTtcbiAgfVxuXG4gIGZvcm1hdERhdGVNb250aChtb21lbnQ6IE1vbWVudCkge1xuICAgIC8vIHRvZG8g5Yik5patbW9tZW5057G75Z6LXG4gICAgcmV0dXJuIG1vbWVudC5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUX01PTlRIKTtcbiAgfVxuXG4gIGZvcm1hdERhdGVUaW1lWm9uZShtb21lbnQ6IE1vbWVudCkge1xuICAgIC8vIHRvZG8g5Yik5patbW9tZW5057G75Z6LXG4gICAgcmV0dXJuIG1vbWVudC5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfVElNRV9XSVRIX1RJTUVfWk9ORSk7XG4gIH1cblxuICBub3dXaXRoVGltZUF0U3RhcnRPZkRheSgpIHtcbiAgICByZXR1cm4gdGhpcy5ub3coKS5zdGFydE9mKFwiZGF5XCIpO1xuICB9XG5cbiAgbm93V2l0aFRpbWVBdFN0YXJ0T2ZNb250aCgpIHtcbiAgICByZXR1cm4gdGhpcy5ub3coKS5zdGFydE9mKFwibW9udGhcIik7XG4gIH1cblxuICBub3dXaXRoVGltZUF0U3RhcnRPZlllYXIoKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuc3RhcnRPZihcInllYXJcIik7XG4gIH1cblxuICBub3dXaXRoVGltZUF0RW5kT2ZZZWFyKCkge1xuICAgIHJldHVybiB0aGlzLm5vdygpLmVuZE9mKFwieWVhclwiKTtcbiAgfVxuXG4gIG5vd1dpdGhUaW1lQXRFbmRPZk1vbnRoKCkge1xuICAgIHJldHVybiB0aGlzLm5vdygpLmVuZE9mKFwibW9udGhcIik7XG4gIH1cblxuICBwbHVzWERheXNGcm9tTm93KGRheXM6IG51bWJlcikge1xuICAgIHJldHVybiB0aGlzLm5vdygpLmFkZChkYXlzLCBcImRheXNcIik7XG4gIH1cblxuICBwbHVzWFdlZWtzRnJvbU5vdyh3ZWVrczogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuYWRkKHdlZWtzLCBcIndlZWtzXCIpO1xuICB9XG5cbiAgcGx1c1hNb250aHNGcm9tTm93KG1vbnRoczogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuYWRkKG1vbnRocywgXCJtb250aHNcIik7XG4gIH1cblxuICBwbHVzWFllYXJzRnJvbU5vdyh5ZWFyczogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuYWRkKHllYXJzLCBcInllYXJzXCIpO1xuICB9XG5cbiAgbWludXNYTW9udGhzRnJvbU5vdyhtb250aHM6IG51bWJlcikge1xuICAgIHJldHVybiB0aGlzLm5vdygpLnN1YnRyYWN0KG1vbnRocywgXCJtb250aHNcIik7XG4gIH1cblxuICBtaW51c1hEYXlzRnJvbU5vdyhkYXlzOiBudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5ub3coKS5zdWJ0cmFjdChkYXlzLCBcImRheXNcIik7XG4gIH1cblxuICBtaW51c1hIb3Vyc0Zyb21Ob3coaG91cnM6IG51bWJlcikge1xuICAgIHJldHVybiB0aGlzLm5vdygpLnN1YnRyYWN0KGhvdXJzLCBcImhvdXJzXCIpO1xuICB9XG5cbiAgbWludXNYV2Vla3NGcm9tTm93KHdlZWtzOiBudW1iZXIpIHtcbiAgICByZXR1cm4gdGhpcy5ub3coKS5zdWJ0cmFjdCh3ZWVrcywgXCJ3ZWVrc1wiKTtcbiAgfVxuXG4gIG1pbnVzWFllYXJzRnJvbU5vdyh5ZWFyczogbnVtYmVyKSB7XG4gICAgcmV0dXJuIHRoaXMubm93KCkuc3VidHJhY3QoeWVhcnMsIFwieWVhcnNcIik7XG4gIH1cblxuICB0b0RhdGUoZGF0ZVN0cmluZ1dpdGhUaW1lWm9uZTogc3RyaW5nIHwgTW9tZW50KSB7XG4gICAgcmV0dXJuIG1vbWVudChkYXRlU3RyaW5nV2l0aFRpbWVab25lLCBDb25zdHMuREFURV9GT1JNQVQuREFURV9USU1FX1dJVEhfVElNRV9aT05FKS51dGNPZmZzZXQodGhpcy5nZXRUaW1lWm9uZSgpKTtcbiAgfVxuXG4gIGRpZmZEYXlzRnJvbU5vdyhkYXRlVGltZTogTW9tZW50KSB7XG4gICAgLy8gdG9kbyDliKTmlq1tb21lbnTnsbvlnotcbiAgICByZXR1cm4gdGhpcy5ub3coKS5kaWZmKGRhdGVUaW1lLCBcImRheXNcIik7XG5cbiAgfVxuXG4gIHByaXZhdGUgZ2V0VGltZVpvbmUoKSB7XG4gICAgcmV0dXJuIFN0b3JhZ2UuR2xvYmFsUGFyYW1zLnNlc3Npb24oKS5nZXQoXCJ0aW1lWm9uZVwiKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgRGF0ZVV0aWxzKCk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7OztBQUVBO0FBQ0E7QUFDQTs7Ozs7O0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/date-utils.tsx
