__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _empty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../empty */ "./node_modules/antd/es/empty/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! . */ "./node_modules/antd/es/config-provider/index.js");




var renderEmpty = function renderEmpty(componentName) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](___WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref) {
    var getPrefixCls = _ref.getPrefixCls;
    var prefix = getPrefixCls('empty');

    switch (componentName) {
      case 'Table':
      case 'List':
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_empty__WEBPACK_IMPORTED_MODULE_1__["default"], {
          image: _empty__WEBPACK_IMPORTED_MODULE_1__["default"].PRESENTED_IMAGE_SIMPLE
        });

      case 'Select':
      case 'TreeSelect':
      case 'Cascader':
      case 'Transfer':
      case 'Mentions':
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_empty__WEBPACK_IMPORTED_MODULE_1__["default"], {
          image: _empty__WEBPACK_IMPORTED_MODULE_1__["default"].PRESENTED_IMAGE_SIMPLE,
          className: "".concat(prefix, "-small")
        });

      default:
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_empty__WEBPACK_IMPORTED_MODULE_1__["default"], null);
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (renderEmpty);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jb25maWctcHJvdmlkZXIvcmVuZGVyRW1wdHkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2NvbmZpZy1wcm92aWRlci9yZW5kZXJFbXB0eS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IEVtcHR5IGZyb20gJy4uL2VtcHR5JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLic7XG5jb25zdCByZW5kZXJFbXB0eSA9IChjb21wb25lbnROYW1lKSA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgIHsoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgIGNvbnN0IHByZWZpeCA9IGdldFByZWZpeENscygnZW1wdHknKTtcbiAgICBzd2l0Y2ggKGNvbXBvbmVudE5hbWUpIHtcbiAgICAgICAgY2FzZSAnVGFibGUnOlxuICAgICAgICBjYXNlICdMaXN0JzpcbiAgICAgICAgICAgIHJldHVybiA8RW1wdHkgaW1hZ2U9e0VtcHR5LlBSRVNFTlRFRF9JTUFHRV9TSU1QTEV9Lz47XG4gICAgICAgIGNhc2UgJ1NlbGVjdCc6XG4gICAgICAgIGNhc2UgJ1RyZWVTZWxlY3QnOlxuICAgICAgICBjYXNlICdDYXNjYWRlcic6XG4gICAgICAgIGNhc2UgJ1RyYW5zZmVyJzpcbiAgICAgICAgY2FzZSAnTWVudGlvbnMnOlxuICAgICAgICAgICAgcmV0dXJuIDxFbXB0eSBpbWFnZT17RW1wdHkuUFJFU0VOVEVEX0lNQUdFX1NJTVBMRX0gY2xhc3NOYW1lPXtgJHtwcmVmaXh9LXNtYWxsYH0vPjtcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJldHVybiA8RW1wdHkgLz47XG4gICAgfVxufX1cbiAgPC9Db25maWdDb25zdW1lcj4pO1xuZXhwb3J0IGRlZmF1bHQgcmVuZGVyRW1wdHk7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQVhBO0FBSEE7QUFBQTtBQUNBO0FBaUJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/config-provider/renderEmpty.js
