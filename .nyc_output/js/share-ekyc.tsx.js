__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var copy_to_clipboard__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! copy-to-clipboard */ "./node_modules/copy-to-clipboard/index.js");
/* harmony import */ var copy_to_clipboard__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(copy_to_clipboard__WEBPACK_IMPORTED_MODULE_15__);










var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/ekyc/manual-processing/share/share-ekyc.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .share-Popover {\n          .ant-popover-inner-content {\n            padding: 0;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var isMobile = _common__WEBPACK_IMPORTED_MODULE_13__["Utils"].getIsMobile();

var Share =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(Share, _ModelWidget);

  function Share(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Share);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Share).call(this, props, context));
    _this.handleShareToLine =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee() {
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              try {
                _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post("/kyc/sharelink", _this.postData(), {
                  loading: true
                }).then(function (response) {
                  var respData = response.body.respData;
                  window.open("https://social-plugins.line.me/lineit/share?url=".concat(encodeURIComponent(respData)));
                }).catch(function (error) {});
              } catch (error) {
                console.error(error.message);
              }

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    _this.handleCopyShareLink =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee2() {
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              try {
                _common__WEBPACK_IMPORTED_MODULE_13__["Ajax"].post("/kyc/sharelink", _this.postData(), {
                  loading: true
                }).then(function (response) {
                  var respData = response.body.respData;
                  copy_to_clipboard__WEBPACK_IMPORTED_MODULE_15___default()(respData);
                  antd__WEBPACK_IMPORTED_MODULE_14__["notification"].success({
                    message: "success",
                    description: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Copied to clipboard").thai("คัดลอกไปที่คลิปบอร์ดแล้ว").getMessage()
                  });
                }).catch(function (error) {});
              } catch (error) {
                console.error(error.message);
              }

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Share, [{
    key: "postData",
    value: function postData() {
      var model = this.props.model;
      return {
        bizId: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, "refId") || lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, "bizId"),
        bizNo: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, "refNo"),
        bizType: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, "bizType"),
        itntCode: lodash__WEBPACK_IMPORTED_MODULE_10___default.a.get(model, "itntCode")
      };
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var C = this.getComponents();
      var content = this.props.content || _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("ul", {
        className: "share-btn",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("li", {
        onClick: function onClick() {
          _this2.handleShareToLine();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("i", {
        className: "iconfont icon-line",
        style: {
          fontSize: "15px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }), _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Send via Line").thai("ส่งผ่าน Line").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("li", {
        onClick: function onClick() {
          _this2.handleCopyShareLink();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Icon"], {
        type: "copy",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }), _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Copy Link").thai("คัดลอกลิงค์").getMessage()));
      var style = isMobile ? Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.props.style, {}, {
        display: "block",
        textAlign: "center",
        marginBottom: "20px"
      }) : this.props.style;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.Share, {
        style: style,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Popover"], {
        overlayClassName: "share-Popover",
        placement: "right",
        title: "",
        content: content,
        trigger: "click",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("img", {
        style: {
          width: "45px",
          cursor: "pointer"
        },
        src: __webpack_require__(/*! ../../../../../assets/share.png */ "./src/assets/share.png"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      })));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Share: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Share.prototype), "initState", this).call(this), {});
    }
  }]);

  return Share;
}(_component__WEBPACK_IMPORTED_MODULE_12__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Share);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svZWt5Yy9tYW51YWwtcHJvY2Vzc2luZy9zaGFyZS9zaGFyZS1la3ljLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2VreWMvbWFudWFsLXByb2Nlc3Npbmcvc2hhcmUvc2hhcmUtZWt5Yy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24sIG5vdGlmaWNhdGlvbiwgUG9wb3ZlciB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgY2xpcGJvYXJkIGZyb20gXCJjb3B5LXRvLWNsaXBib2FyZFwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5leHBvcnQgdHlwZSBTaGFyZVByb3BzID0ge1xuICBzdHlsZT86IGFueTtcbiAgY29udGVudD86IGFueTtcbiAgbW9kZWw6IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBTaGFyZVN0YXRlID0ge31cbnR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbnR5cGUgU2hhcmVDb21wb25lbnRzID0ge1xuICBTaGFyZTogU3R5bGVkRElWLFxufVxuXG5jbGFzcyBTaGFyZTxQIGV4dGVuZHMgU2hhcmVQcm9wcywgUyBleHRlbmRzIFNoYXJlU3RhdGUsIEMgZXh0ZW5kcyBTaGFyZUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogU2hhcmVQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHBvc3REYXRhKCkge1xuICAgIGNvbnN0IHsgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIHtcbiAgICAgIGJpeklkOiBfLmdldChtb2RlbCwgXCJyZWZJZFwiKSB8fCBfLmdldChtb2RlbCwgXCJiaXpJZFwiKSxcbiAgICAgIGJpek5vOiBfLmdldChtb2RlbCwgXCJyZWZOb1wiKSxcbiAgICAgIGJpelR5cGU6IF8uZ2V0KG1vZGVsLCBcImJpelR5cGVcIiksXG4gICAgICBpdG50Q29kZTogXy5nZXQobW9kZWwsIFwiaXRudENvZGVcIiksXG4gICAgfTtcbiAgfVxuXG4gIGhhbmRsZVNoYXJlVG9MaW5lID0gYXN5bmMgKCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBBamF4LnBvc3QoYC9reWMvc2hhcmVsaW5rYCxcbiAgICAgICAgdGhpcy5wb3N0RGF0YSgpLCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgIHdpbmRvdy5vcGVuKGBodHRwczovL3NvY2lhbC1wbHVnaW5zLmxpbmUubWUvbGluZWl0L3NoYXJlP3VybD0ke2VuY29kZVVSSUNvbXBvbmVudChcbiAgICAgICAgICAgIHJlc3BEYXRhLFxuICAgICAgICAgICl9YCk7XG4gICAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yLm1lc3NhZ2UpO1xuICAgIH1cbiAgfTtcblxuICBoYW5kbGVDb3B5U2hhcmVMaW5rID0gYXN5bmMgKCkgPT4ge1xuICAgIHRyeSB7XG4gICAgICBBamF4LnBvc3QoYC9reWMvc2hhcmVsaW5rYCxcbiAgICAgICAgdGhpcy5wb3N0RGF0YSgpLCB7IGxvYWRpbmc6IHRydWUgfSlcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgIGNsaXBib2FyZChyZXNwRGF0YSk7XG4gICAgICAgICAgbm90aWZpY2F0aW9uLnN1Y2Nlc3Moe1xuICAgICAgICAgICAgbWVzc2FnZTogXCJzdWNjZXNzXCIsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogTGFuZ3VhZ2UuZW4oXCJDb3BpZWQgdG8gY2xpcGJvYXJkXCIpLnRoYWkoXCLguITguLHguJTguKXguK3guIHguYTguJvguJfguLXguYjguITguKXguLTguJvguJrguK3guKPguYzguJTguYHguKXguYnguKdcIikuZ2V0TWVzc2FnZSgpLFxuICAgICAgICAgIH0pO1xuICAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvci5tZXNzYWdlKTtcbiAgICB9XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCBjb250ZW50ID0gdGhpcy5wcm9wcy5jb250ZW50IHx8IChcbiAgICAgIDx1bCBjbGFzc05hbWU9XCJzaGFyZS1idG5cIj5cbiAgICAgICAgPGxpIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICB0aGlzLmhhbmRsZVNoYXJlVG9MaW5lKCk7XG4gICAgICAgIH19PlxuICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImljb25mb250IGljb24tbGluZVwiIHN0eWxlPXt7IGZvbnRTaXplOiBcIjE1cHhcIiB9fS8+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2VuZCB2aWEgTGluZVwiKS50aGFpKFwi4Liq4LmI4LiH4Lic4LmI4Liy4LiZIExpbmVcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2xpPlxuICAgICAgICA8bGkgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIHRoaXMuaGFuZGxlQ29weVNoYXJlTGluaygpO1xuICAgICAgICB9fT5cbiAgICAgICAgICA8SWNvbiB0eXBlPVwiY29weVwiLz5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDb3B5IExpbmtcIikudGhhaShcIuC4hOC4seC4lOC4peC4reC4geC4peC4tOC4h+C4hOC5jFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvbGk+XG4gICAgICA8L3VsPlxuICAgICk7XG4gICAgY29uc3Qgc3R5bGUgPSBpc01vYmlsZSA/IHtcbiAgICAgIC4uLnRoaXMucHJvcHMuc3R5bGUsXG4gICAgICAuLi57XG4gICAgICAgIGRpc3BsYXk6IFwiYmxvY2tcIixcbiAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxuICAgICAgICBtYXJnaW5Cb3R0b206IFwiMjBweFwiLFxuICAgICAgfSxcbiAgICB9IDogdGhpcy5wcm9wcy5zdHlsZTtcbiAgICByZXR1cm4gKFxuICAgICAgPEMuU2hhcmUgc3R5bGU9e3N0eWxlfT5cbiAgICAgICAgPFBvcG92ZXIgb3ZlcmxheUNsYXNzTmFtZT1cInNoYXJlLVBvcG92ZXJcIiBwbGFjZW1lbnQ9XCJyaWdodFwiIHRpdGxlPXtcIlwifSBjb250ZW50PXtjb250ZW50fSB0cmlnZ2VyPVwiY2xpY2tcIj5cbiAgICAgICAgICA8aW1nIHN0eWxlPXt7IHdpZHRoOiBcIjQ1cHhcIiwgY3Vyc29yOiBcInBvaW50ZXJcIiB9fSBzcmM9e3JlcXVpcmUoXCIuLi8uLi8uLi8uLi8uLi9hc3NldHMvc2hhcmUucG5nXCIpfS8+XG4gICAgICAgIDwvUG9wb3Zlcj5cbiAgICAgIDwvQy5TaGFyZT5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBTaGFyZTogU3R5bGVkLmRpdmBcbiAgICAgICAgLnNoYXJlLVBvcG92ZXIge1xuICAgICAgICAgIC5hbnQtcG9wb3Zlci1pbm5lci1jb250ZW50IHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHt9KSBhcyBTO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNoYXJlOyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQWFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE4QkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE5QkE7QUFFQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7OztBQW9DQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFHQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQVNBOzs7QUFFQTtBQUNBO0FBQ0E7Ozs7QUFsR0E7QUFDQTtBQW9HQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/ekyc/manual-processing/share/share-ekyc.tsx
