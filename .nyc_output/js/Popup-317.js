__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var rc_align__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rc-align */ "./node_modules/rc-align/es/index.js");
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var _PopupInner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./PopupInner */ "./node_modules/rc-trigger/es/PopupInner.js");
/* harmony import */ var _LazyRenderBox__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./LazyRenderBox */ "./node_modules/rc-trigger/es/LazyRenderBox.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-trigger/es/utils.js");













var Popup = function (_Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Popup, _Component);

  function Popup(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Popup);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _Component.call(this, props));

    _initialiseProps.call(_this);

    _this.state = {
      // Used for stretch
      stretchChecked: false,
      targetWidth: undefined,
      targetHeight: undefined
    };
    _this.savePopupRef = _utils__WEBPACK_IMPORTED_MODULE_11__["saveRef"].bind(_this, 'popupInstance');
    _this.saveAlignRef = _utils__WEBPACK_IMPORTED_MODULE_11__["saveRef"].bind(_this, 'alignInstance');
    return _this;
  }

  Popup.prototype.componentDidMount = function componentDidMount() {
    this.rootNode = this.getPopupDomNode();
    this.setStretchSize();
  };

  Popup.prototype.componentDidUpdate = function componentDidUpdate() {
    this.setStretchSize();
  }; // Record size if stretch needed


  Popup.prototype.getPopupDomNode = function getPopupDomNode() {
    return react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(this.popupInstance);
  }; // `target` on `rc-align` can accept as a function to get the bind element or a point.
  // ref: https://www.npmjs.com/package/rc-align


  Popup.prototype.getMaskTransitionName = function getMaskTransitionName() {
    var props = this.props;
    var transitionName = props.maskTransitionName;
    var animation = props.maskAnimation;

    if (!transitionName && animation) {
      transitionName = props.prefixCls + '-' + animation;
    }

    return transitionName;
  };

  Popup.prototype.getTransitionName = function getTransitionName() {
    var props = this.props;
    var transitionName = props.transitionName;

    if (!transitionName && props.animation) {
      transitionName = props.prefixCls + '-' + props.animation;
    }

    return transitionName;
  };

  Popup.prototype.getClassName = function getClassName(currentAlignClassName) {
    return this.props.prefixCls + ' ' + this.props.className + ' ' + currentAlignClassName;
  };

  Popup.prototype.getPopupElement = function getPopupElement() {
    var _this2 = this;

    var savePopupRef = this.savePopupRef;
    var _state = this.state,
        stretchChecked = _state.stretchChecked,
        targetHeight = _state.targetHeight,
        targetWidth = _state.targetWidth;
    var _props = this.props,
        align = _props.align,
        visible = _props.visible,
        prefixCls = _props.prefixCls,
        style = _props.style,
        getClassNameFromAlign = _props.getClassNameFromAlign,
        destroyPopupOnHide = _props.destroyPopupOnHide,
        stretch = _props.stretch,
        children = _props.children,
        onMouseEnter = _props.onMouseEnter,
        onMouseLeave = _props.onMouseLeave,
        onMouseDown = _props.onMouseDown,
        onTouchStart = _props.onTouchStart;
    var className = this.getClassName(this.currentAlignClassName || getClassNameFromAlign(align));
    var hiddenClassName = prefixCls + '-hidden';

    if (!visible) {
      this.currentAlignClassName = null;
    }

    var sizeStyle = {};

    if (stretch) {
      // Stretch with target
      if (stretch.indexOf('height') !== -1) {
        sizeStyle.height = targetHeight;
      } else if (stretch.indexOf('minHeight') !== -1) {
        sizeStyle.minHeight = targetHeight;
      }

      if (stretch.indexOf('width') !== -1) {
        sizeStyle.width = targetWidth;
      } else if (stretch.indexOf('minWidth') !== -1) {
        sizeStyle.minWidth = targetWidth;
      } // Delay force align to makes ui smooth


      if (!stretchChecked) {
        sizeStyle.visibility = 'hidden';
        setTimeout(function () {
          if (_this2.alignInstance) {
            _this2.alignInstance.forceAlign();
          }
        }, 0);
      }
    }

    var newStyle = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, sizeStyle, style, this.getZIndexStyle());

    var popupInnerProps = {
      className: className,
      prefixCls: prefixCls,
      ref: savePopupRef,
      onMouseEnter: onMouseEnter,
      onMouseLeave: onMouseLeave,
      onMouseDown: onMouseDown,
      onTouchStart: onTouchStart,
      style: newStyle
    };

    if (destroyPopupOnHide) {
      return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
        component: '',
        exclusive: true,
        transitionAppear: true,
        transitionName: this.getTransitionName()
      }, visible ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_align__WEBPACK_IMPORTED_MODULE_7__["default"], {
        target: this.getAlignTarget(),
        key: 'popup',
        ref: this.saveAlignRef,
        monitorWindowResize: true,
        align: align,
        onAlign: this.onAlign
      }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_PopupInner__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
        visible: true
      }, popupInnerProps), children)) : null);
    }

    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
      component: '',
      exclusive: true,
      transitionAppear: true,
      transitionName: this.getTransitionName(),
      showProp: 'xVisible'
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_align__WEBPACK_IMPORTED_MODULE_7__["default"], {
      target: this.getAlignTarget(),
      key: 'popup',
      ref: this.saveAlignRef,
      monitorWindowResize: true,
      xVisible: visible,
      childrenProps: {
        visible: 'xVisible'
      },
      disabled: !visible,
      align: align,
      onAlign: this.onAlign
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_PopupInner__WEBPACK_IMPORTED_MODULE_9__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      hiddenClassName: hiddenClassName
    }, popupInnerProps), children)));
  };

  Popup.prototype.getZIndexStyle = function getZIndexStyle() {
    var style = {};
    var props = this.props;

    if (props.zIndex !== undefined) {
      style.zIndex = props.zIndex;
    }

    return style;
  };

  Popup.prototype.getMaskElement = function getMaskElement() {
    var props = this.props;
    var maskElement = void 0;

    if (props.mask) {
      var maskTransition = this.getMaskTransitionName();
      maskElement = react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_LazyRenderBox__WEBPACK_IMPORTED_MODULE_10__["default"], {
        style: this.getZIndexStyle(),
        key: 'mask',
        className: props.prefixCls + '-mask',
        hiddenClassName: props.prefixCls + '-mask-hidden',
        visible: props.visible
      });

      if (maskTransition) {
        maskElement = react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
          key: 'mask',
          showProp: 'visible',
          transitionAppear: true,
          component: '',
          transitionName: maskTransition
        }, maskElement);
      }
    }

    return maskElement;
  };

  Popup.prototype.render = function render() {
    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', null, this.getMaskElement(), this.getPopupElement());
  };

  return Popup;
}(react__WEBPACK_IMPORTED_MODULE_4__["Component"]);

Popup.propTypes = {
  visible: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  style: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  getClassNameFromAlign: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onAlign: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  getRootDomNode: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  align: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  destroyPopupOnHide: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  className: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  onMouseEnter: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onMouseLeave: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onMouseDown: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onTouchStart: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  stretch: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  children: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.node,
  point: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.shape({
    pageX: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number,
    pageY: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.number
  })
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.onAlign = function (popupDomNode, align) {
    var props = _this3.props;
    var currentAlignClassName = props.getClassNameFromAlign(align); // FIX: https://github.com/react-component/trigger/issues/56
    // FIX: https://github.com/react-component/tooltip/issues/79

    if (_this3.currentAlignClassName !== currentAlignClassName) {
      _this3.currentAlignClassName = currentAlignClassName;
      popupDomNode.className = _this3.getClassName(currentAlignClassName);
    }

    props.onAlign(popupDomNode, align);
  };

  this.setStretchSize = function () {
    var _props2 = _this3.props,
        stretch = _props2.stretch,
        getRootDomNode = _props2.getRootDomNode,
        visible = _props2.visible;
    var _state2 = _this3.state,
        stretchChecked = _state2.stretchChecked,
        targetHeight = _state2.targetHeight,
        targetWidth = _state2.targetWidth;

    if (!stretch || !visible) {
      if (stretchChecked) {
        _this3.setState({
          stretchChecked: false
        });
      }

      return;
    }

    var $ele = getRootDomNode();
    if (!$ele) return;
    var height = $ele.offsetHeight;
    var width = $ele.offsetWidth;

    if (targetHeight !== height || targetWidth !== width || !stretchChecked) {
      _this3.setState({
        stretchChecked: true,
        targetHeight: height,
        targetWidth: width
      });
    }
  };

  this.getTargetElement = function () {
    return _this3.props.getRootDomNode();
  };

  this.getAlignTarget = function () {
    var point = _this3.props.point;

    if (point) {
      return point;
    }

    return _this3.getTargetElement;
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Popup);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJpZ2dlci9lcy9Qb3B1cC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvZXMvUG9wdXAuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IEFsaWduIGZyb20gJ3JjLWFsaWduJztcbmltcG9ydCBBbmltYXRlIGZyb20gJ3JjLWFuaW1hdGUnO1xuaW1wb3J0IFBvcHVwSW5uZXIgZnJvbSAnLi9Qb3B1cElubmVyJztcbmltcG9ydCBMYXp5UmVuZGVyQm94IGZyb20gJy4vTGF6eVJlbmRlckJveCc7XG5pbXBvcnQgeyBzYXZlUmVmIH0gZnJvbSAnLi91dGlscyc7XG5cbnZhciBQb3B1cCA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhQb3B1cCwgX0NvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gUG9wdXAocHJvcHMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUG9wdXApO1xuXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbXBvbmVudC5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICBfaW5pdGlhbGlzZVByb3BzLmNhbGwoX3RoaXMpO1xuXG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICAvLyBVc2VkIGZvciBzdHJldGNoXG4gICAgICBzdHJldGNoQ2hlY2tlZDogZmFsc2UsXG4gICAgICB0YXJnZXRXaWR0aDogdW5kZWZpbmVkLFxuICAgICAgdGFyZ2V0SGVpZ2h0OiB1bmRlZmluZWRcbiAgICB9O1xuXG4gICAgX3RoaXMuc2F2ZVBvcHVwUmVmID0gc2F2ZVJlZi5iaW5kKF90aGlzLCAncG9wdXBJbnN0YW5jZScpO1xuICAgIF90aGlzLnNhdmVBbGlnblJlZiA9IHNhdmVSZWYuYmluZChfdGhpcywgJ2FsaWduSW5zdGFuY2UnKTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBQb3B1cC5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnJvb3ROb2RlID0gdGhpcy5nZXRQb3B1cERvbU5vZGUoKTtcbiAgICB0aGlzLnNldFN0cmV0Y2hTaXplKCk7XG4gIH07XG5cbiAgUG9wdXAucHJvdG90eXBlLmNvbXBvbmVudERpZFVwZGF0ZSA9IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICB0aGlzLnNldFN0cmV0Y2hTaXplKCk7XG4gIH07XG5cbiAgLy8gUmVjb3JkIHNpemUgaWYgc3RyZXRjaCBuZWVkZWRcblxuXG4gIFBvcHVwLnByb3RvdHlwZS5nZXRQb3B1cERvbU5vZGUgPSBmdW5jdGlvbiBnZXRQb3B1cERvbU5vZGUoKSB7XG4gICAgcmV0dXJuIFJlYWN0RE9NLmZpbmRET01Ob2RlKHRoaXMucG9wdXBJbnN0YW5jZSk7XG4gIH07XG5cbiAgLy8gYHRhcmdldGAgb24gYHJjLWFsaWduYCBjYW4gYWNjZXB0IGFzIGEgZnVuY3Rpb24gdG8gZ2V0IHRoZSBiaW5kIGVsZW1lbnQgb3IgYSBwb2ludC5cbiAgLy8gcmVmOiBodHRwczovL3d3dy5ucG1qcy5jb20vcGFja2FnZS9yYy1hbGlnblxuXG5cbiAgUG9wdXAucHJvdG90eXBlLmdldE1hc2tUcmFuc2l0aW9uTmFtZSA9IGZ1bmN0aW9uIGdldE1hc2tUcmFuc2l0aW9uTmFtZSgpIHtcbiAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgIHZhciB0cmFuc2l0aW9uTmFtZSA9IHByb3BzLm1hc2tUcmFuc2l0aW9uTmFtZTtcbiAgICB2YXIgYW5pbWF0aW9uID0gcHJvcHMubWFza0FuaW1hdGlvbjtcbiAgICBpZiAoIXRyYW5zaXRpb25OYW1lICYmIGFuaW1hdGlvbikge1xuICAgICAgdHJhbnNpdGlvbk5hbWUgPSBwcm9wcy5wcmVmaXhDbHMgKyAnLScgKyBhbmltYXRpb247XG4gICAgfVxuICAgIHJldHVybiB0cmFuc2l0aW9uTmFtZTtcbiAgfTtcblxuICBQb3B1cC5wcm90b3R5cGUuZ2V0VHJhbnNpdGlvbk5hbWUgPSBmdW5jdGlvbiBnZXRUcmFuc2l0aW9uTmFtZSgpIHtcbiAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgIHZhciB0cmFuc2l0aW9uTmFtZSA9IHByb3BzLnRyYW5zaXRpb25OYW1lO1xuICAgIGlmICghdHJhbnNpdGlvbk5hbWUgJiYgcHJvcHMuYW5pbWF0aW9uKSB7XG4gICAgICB0cmFuc2l0aW9uTmFtZSA9IHByb3BzLnByZWZpeENscyArICctJyArIHByb3BzLmFuaW1hdGlvbjtcbiAgICB9XG4gICAgcmV0dXJuIHRyYW5zaXRpb25OYW1lO1xuICB9O1xuXG4gIFBvcHVwLnByb3RvdHlwZS5nZXRDbGFzc05hbWUgPSBmdW5jdGlvbiBnZXRDbGFzc05hbWUoY3VycmVudEFsaWduQ2xhc3NOYW1lKSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMucHJlZml4Q2xzICsgJyAnICsgdGhpcy5wcm9wcy5jbGFzc05hbWUgKyAnICcgKyBjdXJyZW50QWxpZ25DbGFzc05hbWU7XG4gIH07XG5cbiAgUG9wdXAucHJvdG90eXBlLmdldFBvcHVwRWxlbWVudCA9IGZ1bmN0aW9uIGdldFBvcHVwRWxlbWVudCgpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHZhciBzYXZlUG9wdXBSZWYgPSB0aGlzLnNhdmVQb3B1cFJlZjtcbiAgICB2YXIgX3N0YXRlID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgc3RyZXRjaENoZWNrZWQgPSBfc3RhdGUuc3RyZXRjaENoZWNrZWQsXG4gICAgICAgIHRhcmdldEhlaWdodCA9IF9zdGF0ZS50YXJnZXRIZWlnaHQsXG4gICAgICAgIHRhcmdldFdpZHRoID0gX3N0YXRlLnRhcmdldFdpZHRoO1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICBhbGlnbiA9IF9wcm9wcy5hbGlnbixcbiAgICAgICAgdmlzaWJsZSA9IF9wcm9wcy52aXNpYmxlLFxuICAgICAgICBwcmVmaXhDbHMgPSBfcHJvcHMucHJlZml4Q2xzLFxuICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgZ2V0Q2xhc3NOYW1lRnJvbUFsaWduID0gX3Byb3BzLmdldENsYXNzTmFtZUZyb21BbGlnbixcbiAgICAgICAgZGVzdHJveVBvcHVwT25IaWRlID0gX3Byb3BzLmRlc3Ryb3lQb3B1cE9uSGlkZSxcbiAgICAgICAgc3RyZXRjaCA9IF9wcm9wcy5zdHJldGNoLFxuICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgb25Nb3VzZUVudGVyID0gX3Byb3BzLm9uTW91c2VFbnRlcixcbiAgICAgICAgb25Nb3VzZUxlYXZlID0gX3Byb3BzLm9uTW91c2VMZWF2ZSxcbiAgICAgICAgb25Nb3VzZURvd24gPSBfcHJvcHMub25Nb3VzZURvd24sXG4gICAgICAgIG9uVG91Y2hTdGFydCA9IF9wcm9wcy5vblRvdWNoU3RhcnQ7XG5cbiAgICB2YXIgY2xhc3NOYW1lID0gdGhpcy5nZXRDbGFzc05hbWUodGhpcy5jdXJyZW50QWxpZ25DbGFzc05hbWUgfHwgZ2V0Q2xhc3NOYW1lRnJvbUFsaWduKGFsaWduKSk7XG4gICAgdmFyIGhpZGRlbkNsYXNzTmFtZSA9IHByZWZpeENscyArICctaGlkZGVuJztcblxuICAgIGlmICghdmlzaWJsZSkge1xuICAgICAgdGhpcy5jdXJyZW50QWxpZ25DbGFzc05hbWUgPSBudWxsO1xuICAgIH1cblxuICAgIHZhciBzaXplU3R5bGUgPSB7fTtcbiAgICBpZiAoc3RyZXRjaCkge1xuICAgICAgLy8gU3RyZXRjaCB3aXRoIHRhcmdldFxuICAgICAgaWYgKHN0cmV0Y2guaW5kZXhPZignaGVpZ2h0JykgIT09IC0xKSB7XG4gICAgICAgIHNpemVTdHlsZS5oZWlnaHQgPSB0YXJnZXRIZWlnaHQ7XG4gICAgICB9IGVsc2UgaWYgKHN0cmV0Y2guaW5kZXhPZignbWluSGVpZ2h0JykgIT09IC0xKSB7XG4gICAgICAgIHNpemVTdHlsZS5taW5IZWlnaHQgPSB0YXJnZXRIZWlnaHQ7XG4gICAgICB9XG4gICAgICBpZiAoc3RyZXRjaC5pbmRleE9mKCd3aWR0aCcpICE9PSAtMSkge1xuICAgICAgICBzaXplU3R5bGUud2lkdGggPSB0YXJnZXRXaWR0aDtcbiAgICAgIH0gZWxzZSBpZiAoc3RyZXRjaC5pbmRleE9mKCdtaW5XaWR0aCcpICE9PSAtMSkge1xuICAgICAgICBzaXplU3R5bGUubWluV2lkdGggPSB0YXJnZXRXaWR0aDtcbiAgICAgIH1cblxuICAgICAgLy8gRGVsYXkgZm9yY2UgYWxpZ24gdG8gbWFrZXMgdWkgc21vb3RoXG4gICAgICBpZiAoIXN0cmV0Y2hDaGVja2VkKSB7XG4gICAgICAgIHNpemVTdHlsZS52aXNpYmlsaXR5ID0gJ2hpZGRlbic7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmIChfdGhpczIuYWxpZ25JbnN0YW5jZSkge1xuICAgICAgICAgICAgX3RoaXMyLmFsaWduSW5zdGFuY2UuZm9yY2VBbGlnbigpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSwgMCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIG5ld1N0eWxlID0gX2V4dGVuZHMoe30sIHNpemVTdHlsZSwgc3R5bGUsIHRoaXMuZ2V0WkluZGV4U3R5bGUoKSk7XG5cbiAgICB2YXIgcG9wdXBJbm5lclByb3BzID0ge1xuICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgIHJlZjogc2F2ZVBvcHVwUmVmLFxuICAgICAgb25Nb3VzZUVudGVyOiBvbk1vdXNlRW50ZXIsXG4gICAgICBvbk1vdXNlTGVhdmU6IG9uTW91c2VMZWF2ZSxcbiAgICAgIG9uTW91c2VEb3duOiBvbk1vdXNlRG93bixcbiAgICAgIG9uVG91Y2hTdGFydDogb25Ub3VjaFN0YXJ0LFxuICAgICAgc3R5bGU6IG5ld1N0eWxlXG4gICAgfTtcbiAgICBpZiAoZGVzdHJveVBvcHVwT25IaWRlKSB7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQW5pbWF0ZSxcbiAgICAgICAge1xuICAgICAgICAgIGNvbXBvbmVudDogJycsXG4gICAgICAgICAgZXhjbHVzaXZlOiB0cnVlLFxuICAgICAgICAgIHRyYW5zaXRpb25BcHBlYXI6IHRydWUsXG4gICAgICAgICAgdHJhbnNpdGlvbk5hbWU6IHRoaXMuZ2V0VHJhbnNpdGlvbk5hbWUoKVxuICAgICAgICB9LFxuICAgICAgICB2aXNpYmxlID8gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBBbGlnbixcbiAgICAgICAgICB7XG4gICAgICAgICAgICB0YXJnZXQ6IHRoaXMuZ2V0QWxpZ25UYXJnZXQoKSxcbiAgICAgICAgICAgIGtleTogJ3BvcHVwJyxcbiAgICAgICAgICAgIHJlZjogdGhpcy5zYXZlQWxpZ25SZWYsXG4gICAgICAgICAgICBtb25pdG9yV2luZG93UmVzaXplOiB0cnVlLFxuICAgICAgICAgICAgYWxpZ246IGFsaWduLFxuICAgICAgICAgICAgb25BbGlnbjogdGhpcy5vbkFsaWduXG4gICAgICAgICAgfSxcbiAgICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgUG9wdXBJbm5lcixcbiAgICAgICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZVxuICAgICAgICAgICAgfSwgcG9wdXBJbm5lclByb3BzKSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKVxuICAgICAgICApIDogbnVsbFxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgIEFuaW1hdGUsXG4gICAgICB7XG4gICAgICAgIGNvbXBvbmVudDogJycsXG4gICAgICAgIGV4Y2x1c2l2ZTogdHJ1ZSxcbiAgICAgICAgdHJhbnNpdGlvbkFwcGVhcjogdHJ1ZSxcbiAgICAgICAgdHJhbnNpdGlvbk5hbWU6IHRoaXMuZ2V0VHJhbnNpdGlvbk5hbWUoKSxcbiAgICAgICAgc2hvd1Byb3A6ICd4VmlzaWJsZSdcbiAgICAgIH0sXG4gICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBBbGlnbixcbiAgICAgICAge1xuICAgICAgICAgIHRhcmdldDogdGhpcy5nZXRBbGlnblRhcmdldCgpLFxuICAgICAgICAgIGtleTogJ3BvcHVwJyxcbiAgICAgICAgICByZWY6IHRoaXMuc2F2ZUFsaWduUmVmLFxuICAgICAgICAgIG1vbml0b3JXaW5kb3dSZXNpemU6IHRydWUsXG4gICAgICAgICAgeFZpc2libGU6IHZpc2libGUsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wczogeyB2aXNpYmxlOiAneFZpc2libGUnIH0sXG4gICAgICAgICAgZGlzYWJsZWQ6ICF2aXNpYmxlLFxuICAgICAgICAgIGFsaWduOiBhbGlnbixcbiAgICAgICAgICBvbkFsaWduOiB0aGlzLm9uQWxpZ25cbiAgICAgICAgfSxcbiAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBQb3B1cElubmVyLFxuICAgICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICAgIGhpZGRlbkNsYXNzTmFtZTogaGlkZGVuQ2xhc3NOYW1lXG4gICAgICAgICAgfSwgcG9wdXBJbm5lclByb3BzKSxcbiAgICAgICAgICBjaGlsZHJlblxuICAgICAgICApXG4gICAgICApXG4gICAgKTtcbiAgfTtcblxuICBQb3B1cC5wcm90b3R5cGUuZ2V0WkluZGV4U3R5bGUgPSBmdW5jdGlvbiBnZXRaSW5kZXhTdHlsZSgpIHtcbiAgICB2YXIgc3R5bGUgPSB7fTtcbiAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgIGlmIChwcm9wcy56SW5kZXggIT09IHVuZGVmaW5lZCkge1xuICAgICAgc3R5bGUuekluZGV4ID0gcHJvcHMuekluZGV4O1xuICAgIH1cbiAgICByZXR1cm4gc3R5bGU7XG4gIH07XG5cbiAgUG9wdXAucHJvdG90eXBlLmdldE1hc2tFbGVtZW50ID0gZnVuY3Rpb24gZ2V0TWFza0VsZW1lbnQoKSB7XG4gICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcztcbiAgICB2YXIgbWFza0VsZW1lbnQgPSB2b2lkIDA7XG4gICAgaWYgKHByb3BzLm1hc2spIHtcbiAgICAgIHZhciBtYXNrVHJhbnNpdGlvbiA9IHRoaXMuZ2V0TWFza1RyYW5zaXRpb25OYW1lKCk7XG4gICAgICBtYXNrRWxlbWVudCA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoTGF6eVJlbmRlckJveCwge1xuICAgICAgICBzdHlsZTogdGhpcy5nZXRaSW5kZXhTdHlsZSgpLFxuICAgICAgICBrZXk6ICdtYXNrJyxcbiAgICAgICAgY2xhc3NOYW1lOiBwcm9wcy5wcmVmaXhDbHMgKyAnLW1hc2snLFxuICAgICAgICBoaWRkZW5DbGFzc05hbWU6IHByb3BzLnByZWZpeENscyArICctbWFzay1oaWRkZW4nLFxuICAgICAgICB2aXNpYmxlOiBwcm9wcy52aXNpYmxlXG4gICAgICB9KTtcbiAgICAgIGlmIChtYXNrVHJhbnNpdGlvbikge1xuICAgICAgICBtYXNrRWxlbWVudCA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgQW5pbWF0ZSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBrZXk6ICdtYXNrJyxcbiAgICAgICAgICAgIHNob3dQcm9wOiAndmlzaWJsZScsXG4gICAgICAgICAgICB0cmFuc2l0aW9uQXBwZWFyOiB0cnVlLFxuICAgICAgICAgICAgY29tcG9uZW50OiAnJyxcbiAgICAgICAgICAgIHRyYW5zaXRpb25OYW1lOiBtYXNrVHJhbnNpdGlvblxuICAgICAgICAgIH0sXG4gICAgICAgICAgbWFza0VsZW1lbnRcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIG1hc2tFbGVtZW50O1xuICB9O1xuXG4gIFBvcHVwLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAnZGl2JyxcbiAgICAgIG51bGwsXG4gICAgICB0aGlzLmdldE1hc2tFbGVtZW50KCksXG4gICAgICB0aGlzLmdldFBvcHVwRWxlbWVudCgpXG4gICAgKTtcbiAgfTtcblxuICByZXR1cm4gUG9wdXA7XG59KENvbXBvbmVudCk7XG5cblBvcHVwLnByb3BUeXBlcyA9IHtcbiAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICBnZXRDbGFzc05hbWVGcm9tQWxpZ246IFByb3BUeXBlcy5mdW5jLFxuICBvbkFsaWduOiBQcm9wVHlwZXMuZnVuYyxcbiAgZ2V0Um9vdERvbU5vZGU6IFByb3BUeXBlcy5mdW5jLFxuICBhbGlnbjogUHJvcFR5cGVzLmFueSxcbiAgZGVzdHJveVBvcHVwT25IaWRlOiBQcm9wVHlwZXMuYm9vbCxcbiAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG9uTW91c2VFbnRlcjogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uTW91c2VMZWF2ZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uTW91c2VEb3duOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25Ub3VjaFN0YXJ0OiBQcm9wVHlwZXMuZnVuYyxcbiAgc3RyZXRjaDogUHJvcFR5cGVzLnN0cmluZyxcbiAgY2hpbGRyZW46IFByb3BUeXBlcy5ub2RlLFxuICBwb2ludDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBwYWdlWDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBwYWdlWTogUHJvcFR5cGVzLm51bWJlclxuICB9KVxufTtcblxudmFyIF9pbml0aWFsaXNlUHJvcHMgPSBmdW5jdGlvbiBfaW5pdGlhbGlzZVByb3BzKCkge1xuICB2YXIgX3RoaXMzID0gdGhpcztcblxuICB0aGlzLm9uQWxpZ24gPSBmdW5jdGlvbiAocG9wdXBEb21Ob2RlLCBhbGlnbikge1xuICAgIHZhciBwcm9wcyA9IF90aGlzMy5wcm9wcztcbiAgICB2YXIgY3VycmVudEFsaWduQ2xhc3NOYW1lID0gcHJvcHMuZ2V0Q2xhc3NOYW1lRnJvbUFsaWduKGFsaWduKTtcbiAgICAvLyBGSVg6IGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvdHJpZ2dlci9pc3N1ZXMvNTZcbiAgICAvLyBGSVg6IGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvdG9vbHRpcC9pc3N1ZXMvNzlcbiAgICBpZiAoX3RoaXMzLmN1cnJlbnRBbGlnbkNsYXNzTmFtZSAhPT0gY3VycmVudEFsaWduQ2xhc3NOYW1lKSB7XG4gICAgICBfdGhpczMuY3VycmVudEFsaWduQ2xhc3NOYW1lID0gY3VycmVudEFsaWduQ2xhc3NOYW1lO1xuICAgICAgcG9wdXBEb21Ob2RlLmNsYXNzTmFtZSA9IF90aGlzMy5nZXRDbGFzc05hbWUoY3VycmVudEFsaWduQ2xhc3NOYW1lKTtcbiAgICB9XG4gICAgcHJvcHMub25BbGlnbihwb3B1cERvbU5vZGUsIGFsaWduKTtcbiAgfTtcblxuICB0aGlzLnNldFN0cmV0Y2hTaXplID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBfcHJvcHMyID0gX3RoaXMzLnByb3BzLFxuICAgICAgICBzdHJldGNoID0gX3Byb3BzMi5zdHJldGNoLFxuICAgICAgICBnZXRSb290RG9tTm9kZSA9IF9wcm9wczIuZ2V0Um9vdERvbU5vZGUsXG4gICAgICAgIHZpc2libGUgPSBfcHJvcHMyLnZpc2libGU7XG4gICAgdmFyIF9zdGF0ZTIgPSBfdGhpczMuc3RhdGUsXG4gICAgICAgIHN0cmV0Y2hDaGVja2VkID0gX3N0YXRlMi5zdHJldGNoQ2hlY2tlZCxcbiAgICAgICAgdGFyZ2V0SGVpZ2h0ID0gX3N0YXRlMi50YXJnZXRIZWlnaHQsXG4gICAgICAgIHRhcmdldFdpZHRoID0gX3N0YXRlMi50YXJnZXRXaWR0aDtcblxuXG4gICAgaWYgKCFzdHJldGNoIHx8ICF2aXNpYmxlKSB7XG4gICAgICBpZiAoc3RyZXRjaENoZWNrZWQpIHtcbiAgICAgICAgX3RoaXMzLnNldFN0YXRlKHsgc3RyZXRjaENoZWNrZWQ6IGZhbHNlIH0pO1xuICAgICAgfVxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciAkZWxlID0gZ2V0Um9vdERvbU5vZGUoKTtcbiAgICBpZiAoISRlbGUpIHJldHVybjtcblxuICAgIHZhciBoZWlnaHQgPSAkZWxlLm9mZnNldEhlaWdodDtcbiAgICB2YXIgd2lkdGggPSAkZWxlLm9mZnNldFdpZHRoO1xuXG4gICAgaWYgKHRhcmdldEhlaWdodCAhPT0gaGVpZ2h0IHx8IHRhcmdldFdpZHRoICE9PSB3aWR0aCB8fCAhc3RyZXRjaENoZWNrZWQpIHtcbiAgICAgIF90aGlzMy5zZXRTdGF0ZSh7XG4gICAgICAgIHN0cmV0Y2hDaGVja2VkOiB0cnVlLFxuICAgICAgICB0YXJnZXRIZWlnaHQ6IGhlaWdodCxcbiAgICAgICAgdGFyZ2V0V2lkdGg6IHdpZHRoXG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5nZXRUYXJnZXRFbGVtZW50ID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBfdGhpczMucHJvcHMuZ2V0Um9vdERvbU5vZGUoKTtcbiAgfTtcblxuICB0aGlzLmdldEFsaWduVGFyZ2V0ID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBwb2ludCA9IF90aGlzMy5wcm9wcy5wb2ludDtcblxuICAgIGlmIChwb2ludCkge1xuICAgICAgcmV0dXJuIHBvaW50O1xuICAgIH1cbiAgICByZXR1cm4gX3RoaXMzLmdldFRhcmdldEVsZW1lbnQ7XG4gIH07XG59O1xuXG5leHBvcnQgZGVmYXVsdCBQb3B1cDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFXQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBY0E7QUFEQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU1BO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFTQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFoQkE7QUFDQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-trigger/es/Popup.js
