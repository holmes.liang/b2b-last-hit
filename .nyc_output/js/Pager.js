__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);





var Pager = function Pager(props) {
  var _classNames;

  var prefixCls = props.rootPrefixCls + '-item';
  var cls = classnames__WEBPACK_IMPORTED_MODULE_3___default()(prefixCls, prefixCls + '-' + props.page, (_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-active', props.active), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, props.className, !!props.className), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, prefixCls + '-disabled', !props.page), _classNames));

  var handleClick = function handleClick() {
    props.onClick(props.page);
  };

  var handleKeyPress = function handleKeyPress(e) {
    props.onKeyPress(e, props.onClick, props.page);
  };

  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement('li', {
    title: props.showTitle ? props.page : null,
    className: cls,
    onClick: handleClick,
    onKeyPress: handleKeyPress,
    tabIndex: '0'
  }, props.itemRender(props.page, 'page', react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement('a', null, props.page)));
};

Pager.propTypes = {
  page: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.number,
  active: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  last: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  locale: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object,
  className: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  showTitle: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
  rootPrefixCls: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
  onClick: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  onKeyPress: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
  itemRender: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func
};
/* harmony default export */ __webpack_exports__["default"] = (Pager);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtcGFnaW5hdGlvbi9lcy9QYWdlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXBhZ2luYXRpb24vZXMvUGFnZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9kZWZpbmVQcm9wZXJ0eSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxudmFyIFBhZ2VyID0gZnVuY3Rpb24gUGFnZXIocHJvcHMpIHtcbiAgdmFyIF9jbGFzc05hbWVzO1xuXG4gIHZhciBwcmVmaXhDbHMgPSBwcm9wcy5yb290UHJlZml4Q2xzICsgJy1pdGVtJztcbiAgdmFyIGNscyA9IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBwcmVmaXhDbHMgKyAnLScgKyBwcm9wcy5wYWdlLCAoX2NsYXNzTmFtZXMgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBwcmVmaXhDbHMgKyAnLWFjdGl2ZScsIHByb3BzLmFjdGl2ZSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgcHJvcHMuY2xhc3NOYW1lLCAhIXByb3BzLmNsYXNzTmFtZSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgcHJlZml4Q2xzICsgJy1kaXNhYmxlZCcsICFwcm9wcy5wYWdlKSwgX2NsYXNzTmFtZXMpKTtcblxuICB2YXIgaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiBoYW5kbGVDbGljaygpIHtcbiAgICBwcm9wcy5vbkNsaWNrKHByb3BzLnBhZ2UpO1xuICB9O1xuXG4gIHZhciBoYW5kbGVLZXlQcmVzcyA9IGZ1bmN0aW9uIGhhbmRsZUtleVByZXNzKGUpIHtcbiAgICBwcm9wcy5vbktleVByZXNzKGUsIHByb3BzLm9uQ2xpY2ssIHByb3BzLnBhZ2UpO1xuICB9O1xuXG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICdsaScsXG4gICAge1xuICAgICAgdGl0bGU6IHByb3BzLnNob3dUaXRsZSA/IHByb3BzLnBhZ2UgOiBudWxsLFxuICAgICAgY2xhc3NOYW1lOiBjbHMsXG4gICAgICBvbkNsaWNrOiBoYW5kbGVDbGljayxcbiAgICAgIG9uS2V5UHJlc3M6IGhhbmRsZUtleVByZXNzLFxuICAgICAgdGFiSW5kZXg6ICcwJ1xuICAgIH0sXG4gICAgcHJvcHMuaXRlbVJlbmRlcihwcm9wcy5wYWdlLCAncGFnZScsIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAnYScsXG4gICAgICBudWxsLFxuICAgICAgcHJvcHMucGFnZVxuICAgICkpXG4gICk7XG59O1xuXG5QYWdlci5wcm9wVHlwZXMgPSB7XG4gIHBhZ2U6IFByb3BUeXBlcy5udW1iZXIsXG4gIGFjdGl2ZTogUHJvcFR5cGVzLmJvb2wsXG4gIGxhc3Q6IFByb3BUeXBlcy5ib29sLFxuICBsb2NhbGU6IFByb3BUeXBlcy5vYmplY3QsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgc2hvd1RpdGxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgcm9vdFByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uS2V5UHJlc3M6IFByb3BUeXBlcy5mdW5jLFxuICBpdGVtUmVuZGVyOiBQcm9wVHlwZXMuZnVuY1xufTtcblxuZXhwb3J0IGRlZmF1bHQgUGFnZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQWFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-pagination/es/Pager.js
