__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return upload; });
function getError(option, xhr) {
  var msg = 'cannot ' + option.method + ' ' + option.action + ' ' + xhr.status + '\'';
  var err = new Error(msg);
  err.status = xhr.status;
  err.method = option.method;
  err.url = option.action;
  return err;
}

function getBody(xhr) {
  var text = xhr.responseText || xhr.response;

  if (!text) {
    return text;
  }

  try {
    return JSON.parse(text);
  } catch (e) {
    return text;
  }
} // option {
//  onProgress: (event: { percent: number }): void,
//  onError: (event: Error, body?: Object): void,
//  onSuccess: (body: Object): void,
//  data: Object,
//  filename: String,
//  file: File,
//  withCredentials: Boolean,
//  action: String,
//  headers: Object,
// }


function upload(option) {
  var xhr = new XMLHttpRequest();

  if (option.onProgress && xhr.upload) {
    xhr.upload.onprogress = function progress(e) {
      if (e.total > 0) {
        e.percent = e.loaded / e.total * 100;
      }

      option.onProgress(e);
    };
  }

  var formData = new FormData();

  if (option.data) {
    Object.keys(option.data).forEach(function (key) {
      var value = option.data[key]; // support key-value array data

      if (Array.isArray(value)) {
        value.forEach(function (item) {
          // { list: [ 11, 22 ] }
          // formData.append('list[]', 11);
          formData.append(key + '[]', item);
        });
        return;
      }

      formData.append(key, option.data[key]);
    });
  }

  formData.append(option.filename, option.file);

  xhr.onerror = function error(e) {
    option.onError(e);
  };

  xhr.onload = function onload() {
    // allow success when 2xx status
    // see https://github.com/react-component/upload/issues/34
    if (xhr.status < 200 || xhr.status >= 300) {
      return option.onError(getError(option, xhr), getBody(xhr));
    }

    option.onSuccess(getBody(xhr), xhr);
  };

  xhr.open(option.method, option.action, true); // Has to be after `.open()`. See https://github.com/enyo/dropzone/issues/179

  if (option.withCredentials && 'withCredentials' in xhr) {
    xhr.withCredentials = true;
  }

  var headers = option.headers || {}; // when set headers['X-Requested-With'] = null , can close default XHR header
  // see https://github.com/react-component/upload/issues/33

  if (headers['X-Requested-With'] !== null) {
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  }

  for (var h in headers) {
    if (headers.hasOwnProperty(h) && headers[h] !== null) {
      xhr.setRequestHeader(h, headers[h]);
    }
  }

  xhr.send(formData);
  return {
    abort: function abort() {
      xhr.abort();
    }
  };
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXBsb2FkL2VzL3JlcXVlc3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11cGxvYWQvZXMvcmVxdWVzdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBnZXRFcnJvcihvcHRpb24sIHhocikge1xuICB2YXIgbXNnID0gJ2Nhbm5vdCAnICsgb3B0aW9uLm1ldGhvZCArICcgJyArIG9wdGlvbi5hY3Rpb24gKyAnICcgKyB4aHIuc3RhdHVzICsgJ1xcJyc7XG4gIHZhciBlcnIgPSBuZXcgRXJyb3IobXNnKTtcbiAgZXJyLnN0YXR1cyA9IHhoci5zdGF0dXM7XG4gIGVyci5tZXRob2QgPSBvcHRpb24ubWV0aG9kO1xuICBlcnIudXJsID0gb3B0aW9uLmFjdGlvbjtcbiAgcmV0dXJuIGVycjtcbn1cblxuZnVuY3Rpb24gZ2V0Qm9keSh4aHIpIHtcbiAgdmFyIHRleHQgPSB4aHIucmVzcG9uc2VUZXh0IHx8IHhoci5yZXNwb25zZTtcbiAgaWYgKCF0ZXh0KSB7XG4gICAgcmV0dXJuIHRleHQ7XG4gIH1cblxuICB0cnkge1xuICAgIHJldHVybiBKU09OLnBhcnNlKHRleHQpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIHRleHQ7XG4gIH1cbn1cblxuLy8gb3B0aW9uIHtcbi8vICBvblByb2dyZXNzOiAoZXZlbnQ6IHsgcGVyY2VudDogbnVtYmVyIH0pOiB2b2lkLFxuLy8gIG9uRXJyb3I6IChldmVudDogRXJyb3IsIGJvZHk/OiBPYmplY3QpOiB2b2lkLFxuLy8gIG9uU3VjY2VzczogKGJvZHk6IE9iamVjdCk6IHZvaWQsXG4vLyAgZGF0YTogT2JqZWN0LFxuLy8gIGZpbGVuYW1lOiBTdHJpbmcsXG4vLyAgZmlsZTogRmlsZSxcbi8vICB3aXRoQ3JlZGVudGlhbHM6IEJvb2xlYW4sXG4vLyAgYWN0aW9uOiBTdHJpbmcsXG4vLyAgaGVhZGVyczogT2JqZWN0LFxuLy8gfVxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gdXBsb2FkKG9wdGlvbikge1xuICB2YXIgeGhyID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbiAgaWYgKG9wdGlvbi5vblByb2dyZXNzICYmIHhoci51cGxvYWQpIHtcbiAgICB4aHIudXBsb2FkLm9ucHJvZ3Jlc3MgPSBmdW5jdGlvbiBwcm9ncmVzcyhlKSB7XG4gICAgICBpZiAoZS50b3RhbCA+IDApIHtcbiAgICAgICAgZS5wZXJjZW50ID0gZS5sb2FkZWQgLyBlLnRvdGFsICogMTAwO1xuICAgICAgfVxuICAgICAgb3B0aW9uLm9uUHJvZ3Jlc3MoZSk7XG4gICAgfTtcbiAgfVxuXG4gIHZhciBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuXG4gIGlmIChvcHRpb24uZGF0YSkge1xuICAgIE9iamVjdC5rZXlzKG9wdGlvbi5kYXRhKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHZhciB2YWx1ZSA9IG9wdGlvbi5kYXRhW2tleV07XG4gICAgICAvLyBzdXBwb3J0IGtleS12YWx1ZSBhcnJheSBkYXRhXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgdmFsdWUuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgIC8vIHsgbGlzdDogWyAxMSwgMjIgXSB9XG4gICAgICAgICAgLy8gZm9ybURhdGEuYXBwZW5kKCdsaXN0W10nLCAxMSk7XG4gICAgICAgICAgZm9ybURhdGEuYXBwZW5kKGtleSArICdbXScsIGl0ZW0pO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBmb3JtRGF0YS5hcHBlbmQoa2V5LCBvcHRpb24uZGF0YVtrZXldKTtcbiAgICB9KTtcbiAgfVxuXG4gIGZvcm1EYXRhLmFwcGVuZChvcHRpb24uZmlsZW5hbWUsIG9wdGlvbi5maWxlKTtcblxuICB4aHIub25lcnJvciA9IGZ1bmN0aW9uIGVycm9yKGUpIHtcbiAgICBvcHRpb24ub25FcnJvcihlKTtcbiAgfTtcblxuICB4aHIub25sb2FkID0gZnVuY3Rpb24gb25sb2FkKCkge1xuICAgIC8vIGFsbG93IHN1Y2Nlc3Mgd2hlbiAyeHggc3RhdHVzXG4gICAgLy8gc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1jb21wb25lbnQvdXBsb2FkL2lzc3Vlcy8zNFxuICAgIGlmICh4aHIuc3RhdHVzIDwgMjAwIHx8IHhoci5zdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4gb3B0aW9uLm9uRXJyb3IoZ2V0RXJyb3Iob3B0aW9uLCB4aHIpLCBnZXRCb2R5KHhocikpO1xuICAgIH1cblxuICAgIG9wdGlvbi5vblN1Y2Nlc3MoZ2V0Qm9keSh4aHIpLCB4aHIpO1xuICB9O1xuXG4gIHhoci5vcGVuKG9wdGlvbi5tZXRob2QsIG9wdGlvbi5hY3Rpb24sIHRydWUpO1xuXG4gIC8vIEhhcyB0byBiZSBhZnRlciBgLm9wZW4oKWAuIFNlZSBodHRwczovL2dpdGh1Yi5jb20vZW55by9kcm9wem9uZS9pc3N1ZXMvMTc5XG4gIGlmIChvcHRpb24ud2l0aENyZWRlbnRpYWxzICYmICd3aXRoQ3JlZGVudGlhbHMnIGluIHhocikge1xuICAgIHhoci53aXRoQ3JlZGVudGlhbHMgPSB0cnVlO1xuICB9XG5cbiAgdmFyIGhlYWRlcnMgPSBvcHRpb24uaGVhZGVycyB8fCB7fTtcblxuICAvLyB3aGVuIHNldCBoZWFkZXJzWydYLVJlcXVlc3RlZC1XaXRoJ10gPSBudWxsICwgY2FuIGNsb3NlIGRlZmF1bHQgWEhSIGhlYWRlclxuICAvLyBzZWUgaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWNvbXBvbmVudC91cGxvYWQvaXNzdWVzLzMzXG4gIGlmIChoZWFkZXJzWydYLVJlcXVlc3RlZC1XaXRoJ10gIT09IG51bGwpIHtcbiAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcignWC1SZXF1ZXN0ZWQtV2l0aCcsICdYTUxIdHRwUmVxdWVzdCcpO1xuICB9XG5cbiAgZm9yICh2YXIgaCBpbiBoZWFkZXJzKSB7XG4gICAgaWYgKGhlYWRlcnMuaGFzT3duUHJvcGVydHkoaCkgJiYgaGVhZGVyc1toXSAhPT0gbnVsbCkge1xuICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoaCwgaGVhZGVyc1toXSk7XG4gICAgfVxuICB9XG4gIHhoci5zZW5kKGZvcm1EYXRhKTtcblxuICByZXR1cm4ge1xuICAgIGFib3J0OiBmdW5jdGlvbiBhYm9ydCgpIHtcbiAgICAgIHhoci5hYm9ydCgpO1xuICAgIH1cbiAgfTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-upload/es/request.js
