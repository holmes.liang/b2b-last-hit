__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return insertMention; });
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _getSearchWord__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./getSearchWord */ "./node_modules/rc-editor-mention/es/utils/getSearchWord.js");


function insertMention(editorState, mention, data, mode) {
  var entityMode = mode === 'immutable' ? 'IMMUTABLE' : 'MUTABLE';
  var selection = editorState.getSelection();
  var contentState = editorState.getCurrentContent();
  contentState.createEntity('mention', entityMode, data || mention);
  var searchWord = Object(_getSearchWord__WEBPACK_IMPORTED_MODULE_1__["default"])(editorState, selection);
  var begin = searchWord.begin,
      end = searchWord.end;
  var replacedContent = draft_js__WEBPACK_IMPORTED_MODULE_0__["Modifier"].replaceText(contentState, selection.merge({
    anchorOffset: begin,
    focusOffset: end
  }), mention, null, contentState.getLastCreatedEntityKey());
  var InsertSpaceContent = draft_js__WEBPACK_IMPORTED_MODULE_0__["Modifier"].insertText(replacedContent, replacedContent.getSelectionAfter(), ' ');
  var newEditorState = draft_js__WEBPACK_IMPORTED_MODULE_0__["EditorState"].push(editorState, InsertSpaceContent, 'insert-mention');
  return draft_js__WEBPACK_IMPORTED_MODULE_0__["EditorState"].forceSelection(newEditorState, InsertSpaceContent.getSelectionAfter());
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvaW5zZXJ0TWVudGlvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWVkaXRvci1tZW50aW9uL2VzL3V0aWxzL2luc2VydE1lbnRpb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kaWZpZXIsIEVkaXRvclN0YXRlIH0gZnJvbSAnZHJhZnQtanMnO1xuaW1wb3J0IGdldFNlYXJjaFdvcmQgZnJvbSAnLi9nZXRTZWFyY2hXb3JkJztcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaW5zZXJ0TWVudGlvbihlZGl0b3JTdGF0ZSwgbWVudGlvbiwgZGF0YSwgbW9kZSkge1xuICB2YXIgZW50aXR5TW9kZSA9IG1vZGUgPT09ICdpbW11dGFibGUnID8gJ0lNTVVUQUJMRScgOiAnTVVUQUJMRSc7XG4gIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgdmFyIGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG5cbiAgY29udGVudFN0YXRlLmNyZWF0ZUVudGl0eSgnbWVudGlvbicsIGVudGl0eU1vZGUsIGRhdGEgfHwgbWVudGlvbik7XG4gIHZhciBzZWFyY2hXb3JkID0gZ2V0U2VhcmNoV29yZChlZGl0b3JTdGF0ZSwgc2VsZWN0aW9uKTtcbiAgdmFyIGJlZ2luID0gc2VhcmNoV29yZC5iZWdpbixcbiAgICAgIGVuZCA9IHNlYXJjaFdvcmQuZW5kO1xuXG4gIHZhciByZXBsYWNlZENvbnRlbnQgPSBNb2RpZmllci5yZXBsYWNlVGV4dChjb250ZW50U3RhdGUsIHNlbGVjdGlvbi5tZXJnZSh7XG4gICAgYW5jaG9yT2Zmc2V0OiBiZWdpbixcbiAgICBmb2N1c09mZnNldDogZW5kXG4gIH0pLCBtZW50aW9uLCBudWxsLCBjb250ZW50U3RhdGUuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKSk7XG5cbiAgdmFyIEluc2VydFNwYWNlQ29udGVudCA9IE1vZGlmaWVyLmluc2VydFRleHQocmVwbGFjZWRDb250ZW50LCByZXBsYWNlZENvbnRlbnQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKSwgJyAnKTtcblxuICB2YXIgbmV3RWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCBJbnNlcnRTcGFjZUNvbnRlbnQsICdpbnNlcnQtbWVudGlvbicpO1xuICByZXR1cm4gRWRpdG9yU3RhdGUuZm9yY2VTZWxlY3Rpb24obmV3RWRpdG9yU3RhdGUsIEluc2VydFNwYWNlQ29udGVudC5nZXRTZWxlY3Rpb25BZnRlcigpKTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFFQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/insertMention.js
