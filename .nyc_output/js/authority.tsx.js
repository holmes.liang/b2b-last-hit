__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./consts */ "./src/common/consts.tsx");
/* harmony import */ var _envs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./envs */ "./src/common/envs.tsx");
/* harmony import */ var _common_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/storage */ "./src/common/storage.tsx");






var Authority =
/*#__PURE__*/
function () {
  function Authority() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Authority);

    this.user = void 0;
    this.tenant = void 0;
    this.dashboardType = void 0;
    this.initUser();
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Authority, [{
    key: "_hasRole",
    value: function _hasRole(role) {
      var authorities = this.user.authorities || [];
      return authorities.some(function (item) {
        return item.authority === role;
      });
    }
  }, {
    key: "isChannelTenant",
    value: function isChannelTenant() {
      return this.isAgent() || this.isBroker();
    }
  }, {
    key: "isChannelTenantWithD2C",
    value: function isChannelTenantWithD2C() {
      return this.isAgent() || this.isBroker() || this.isD2C();
    }
  }, {
    key: "isAgent",
    value: function isAgent() {
      return this.user.tenantType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].TENANT_TYPES.AGENT;
    }
  }, {
    key: "isPlatform",
    value: function isPlatform() {
      return this.user.tenantType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].TENANT_TYPES.PLATFORM;
    }
  }, {
    key: "isInsurer",
    value: function isInsurer() {
      return this.user.tenantType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].TENANT_TYPES.INSURER;
    }
  }, {
    key: "isDashboardMgr",
    value: function isDashboardMgr() {
      return this.dashboardType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].DASHBOARD_TYPE.BF_DASHBOARD_MGR;
    }
  }, {
    key: "isDashboardSales",
    value: function isDashboardSales() {
      return this.dashboardType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].DASHBOARD_TYPE.BF_DASHBOARD_SALES;
    }
  }, {
    key: "isD2C",
    value: function isD2C() {
      return this.user.tenantType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].TENANT_TYPES.D2C;
    }
  }, {
    key: "isBroker",
    value: function isBroker() {
      return this.user.tenantType === _consts__WEBPACK_IMPORTED_MODULE_2__["default"].TENANT_TYPES.BROKER;
    }
  }, {
    key: "isSalesManager",
    value: function isSalesManager() {
      return this.isAgentManager() || this.isBrokerManager();
    }
  }, {
    key: "isNotSales",
    value: function isNotSales() {
      return !this.isSales();
    }
  }, {
    key: "isSales",
    value: function isSales() {
      return this.isBrokerSales() || this.isAgentSales();
    }
  }, {
    key: "isBrokerSales",
    value: function isBrokerSales() {
      return this._hasRole("BRK_SALES");
    }
  }, {
    key: "isAgentSales",
    value: function isAgentSales() {
      return this._hasRole("AGT_SALES");
    }
  }, {
    key: "isPFAdmin",
    value: function isPFAdmin() {
      return this._hasRole("PF_ADMIN");
    }
  }, {
    key: "isAgentManager",
    value: function isAgentManager() {
      return this._hasRole("AGT_MGR");
    }
  }, {
    key: "isBrokerManager",
    value: function isBrokerManager() {
      return this._hasRole("BRK_MGR");
    }
  }, {
    key: "isClaimOfficer",
    value: function isClaimOfficer() {
      return this._hasRole('CLAIM_OFFICER');
    }
  }, {
    key: "isPFCustomerService",
    value: function isPFCustomerService() {
      return this._hasRole("PF_CS");
    }
  }, {
    key: "getStructId",
    value: function getStructId() {
      return this.tenant.headquarter.structId || "";
    }
  }, {
    key: "getTenantCode",
    value: function getTenantCode() {
      return this.tenant.tenantCode;
    }
  }, {
    key: "getTenantId",
    value: function getTenantId() {
      return this.tenant.tenantId;
    }
  }, {
    key: "getTenantType",
    value: function getTenantType() {
      return this.tenant.tenantType;
    }
  }, {
    key: "initUser",
    value: function initUser() {
      this.user = _envs__WEBPACK_IMPORTED_MODULE_3__["default"].findAccount();
      this.tenant = this.user.tenant || {};
      this.dashboardType = _common_storage__WEBPACK_IMPORTED_MODULE_4__["default"].Account.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].DASHBOARD_ID);
    }
  }]);

  return Authority;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Authority());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2F1dGhvcml0eS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21tb24vYXV0aG9yaXR5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ29uc3RzIGZyb20gXCIuL2NvbnN0c1wiO1xuaW1wb3J0IEVudnMgZnJvbSBcIi4vZW52c1wiO1xuaW1wb3J0IFN0b3JhZ2UgZnJvbSBcIkBjb21tb24vc3RvcmFnZVwiO1xuXG5jbGFzcyBBdXRob3JpdHkge1xuICBwcml2YXRlIHVzZXI6IGFueTtcbiAgcHJpdmF0ZSB0ZW5hbnQ6IGFueTtcbiAgcHJpdmF0ZSBkYXNoYm9hcmRUeXBlOiBhbnk7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5pbml0VXNlcigpO1xuICB9XG5cbiAgX2hhc1JvbGUocm9sZTogYW55KTogYm9vbGVhbiB7XG4gICAgY29uc3QgYXV0aG9yaXRpZXMgPSB0aGlzLnVzZXIuYXV0aG9yaXRpZXMgfHwgW107XG4gICAgcmV0dXJuIGF1dGhvcml0aWVzLnNvbWUoKGl0ZW06IGFueSkgPT4gaXRlbS5hdXRob3JpdHkgPT09IHJvbGUpO1xuICB9XG5cbiAgaXNDaGFubmVsVGVuYW50KCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmlzQWdlbnQoKSB8fCB0aGlzLmlzQnJva2VyKCk7XG4gIH1cblxuICBpc0NoYW5uZWxUZW5hbnRXaXRoRDJDKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmlzQWdlbnQoKSB8fCB0aGlzLmlzQnJva2VyKCkgfHwgdGhpcy5pc0QyQygpO1xuICB9XG5cbiAgaXNBZ2VudCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy51c2VyLnRlbmFudFR5cGUgPT09IENvbnN0cy5URU5BTlRfVFlQRVMuQUdFTlQ7XG4gIH1cblxuICBpc1BsYXRmb3JtKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnVzZXIudGVuYW50VHlwZSA9PT0gQ29uc3RzLlRFTkFOVF9UWVBFUy5QTEFURk9STTtcbiAgfVxuXG4gIGlzSW5zdXJlcigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy51c2VyLnRlbmFudFR5cGUgPT09IENvbnN0cy5URU5BTlRfVFlQRVMuSU5TVVJFUjtcbiAgfVxuXG4gIGlzRGFzaGJvYXJkTWdyKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmRhc2hib2FyZFR5cGUgPT09IENvbnN0cy5EQVNIQk9BUkRfVFlQRS5CRl9EQVNIQk9BUkRfTUdSO1xuICB9XG5cbiAgaXNEYXNoYm9hcmRTYWxlcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5kYXNoYm9hcmRUeXBlID09PSBDb25zdHMuREFTSEJPQVJEX1RZUEUuQkZfREFTSEJPQVJEX1NBTEVTO1xuICB9XG5cbiAgaXNEMkMoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMudXNlci50ZW5hbnRUeXBlID09PSBDb25zdHMuVEVOQU5UX1RZUEVTLkQyQztcbiAgfVxuXG4gIGlzQnJva2VyKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnVzZXIudGVuYW50VHlwZSA9PT0gQ29uc3RzLlRFTkFOVF9UWVBFUy5CUk9LRVI7XG4gIH1cblxuICBpc1NhbGVzTWFuYWdlcigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5pc0FnZW50TWFuYWdlcigpIHx8IHRoaXMuaXNCcm9rZXJNYW5hZ2VyKCk7XG4gIH1cblxuICBpc05vdFNhbGVzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAhdGhpcy5pc1NhbGVzKCk7XG4gIH1cblxuICBpc1NhbGVzKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLmlzQnJva2VyU2FsZXMoKSB8fCB0aGlzLmlzQWdlbnRTYWxlcygpO1xuICB9XG5cbiAgaXNCcm9rZXJTYWxlcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faGFzUm9sZShcIkJSS19TQUxFU1wiKTtcbiAgfVxuXG4gIGlzQWdlbnRTYWxlcygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faGFzUm9sZShcIkFHVF9TQUxFU1wiKTtcbiAgfVxuXG4gIGlzUEZBZG1pbigpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faGFzUm9sZShcIlBGX0FETUlOXCIpO1xuICB9XG5cbiAgaXNBZ2VudE1hbmFnZXIoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuX2hhc1JvbGUoXCJBR1RfTUdSXCIpO1xuICB9XG5cbiAgaXNCcm9rZXJNYW5hZ2VyKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9oYXNSb2xlKFwiQlJLX01HUlwiKTtcbiAgfVxuXG4gIGlzQ2xhaW1PZmZpY2VyKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9oYXNSb2xlKCdDTEFJTV9PRkZJQ0VSJyk7XG4gIH1cblxuICBpc1BGQ3VzdG9tZXJTZXJ2aWNlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9oYXNSb2xlKFwiUEZfQ1NcIik7XG4gIH1cblxuICBnZXRTdHJ1Y3RJZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy50ZW5hbnQuaGVhZHF1YXJ0ZXIuc3RydWN0SWQgfHwgXCJcIjtcbiAgfVxuXG4gIGdldFRlbmFudENvZGUoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMudGVuYW50LnRlbmFudENvZGU7XG4gIH1cblxuICBnZXRUZW5hbnRJZCgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy50ZW5hbnQudGVuYW50SWQ7XG4gIH1cblxuICBnZXRUZW5hbnRUeXBlKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLnRlbmFudC50ZW5hbnRUeXBlO1xuICB9XG5cbiAgaW5pdFVzZXIoKTogdm9pZCB7XG4gICAgdGhpcy51c2VyID0gRW52cy5maW5kQWNjb3VudCgpO1xuICAgIHRoaXMudGVuYW50ID0gdGhpcy51c2VyLnRlbmFudCB8fCB7fTtcbiAgICB0aGlzLmRhc2hib2FyZFR5cGUgPSBTdG9yYWdlLkFjY291bnQuc2Vzc2lvbigpLmdldChDb25zdHMuREFTSEJPQVJEX0lEKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgQXV0aG9yaXR5KCk7Il0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUtBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/authority.tsx
