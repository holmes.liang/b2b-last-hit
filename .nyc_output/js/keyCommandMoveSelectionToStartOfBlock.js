/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandMoveSelectionToStartOfBlock
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");
/**
 * Collapse selection at the start of the first selected block. This is used
 * for Firefox versions that attempt to navigate forward/backward instead of
 * moving the cursor. Other browsers are able to move the cursor natively.
 */


function keyCommandMoveSelectionToStartOfBlock(editorState) {
  var selection = editorState.getSelection();
  var startKey = selection.getStartKey();
  return EditorState.set(editorState, {
    selection: selection.merge({
      anchorKey: startKey,
      anchorOffset: 0,
      focusKey: startKey,
      focusOffset: 0,
      isBackward: false
    }),
    forceSelection: true
  });
}

module.exports = keyCommandMoveSelectionToStartOfBlock;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9TdGFydE9mQmxvY2suanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIva2V5Q29tbWFuZE1vdmVTZWxlY3Rpb25Ub1N0YXJ0T2ZCbG9jay5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGtleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9TdGFydE9mQmxvY2tcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG4vKipcbiAqIENvbGxhcHNlIHNlbGVjdGlvbiBhdCB0aGUgc3RhcnQgb2YgdGhlIGZpcnN0IHNlbGVjdGVkIGJsb2NrLiBUaGlzIGlzIHVzZWRcbiAqIGZvciBGaXJlZm94IHZlcnNpb25zIHRoYXQgYXR0ZW1wdCB0byBuYXZpZ2F0ZSBmb3J3YXJkL2JhY2t3YXJkIGluc3RlYWQgb2ZcbiAqIG1vdmluZyB0aGUgY3Vyc29yLiBPdGhlciBicm93c2VycyBhcmUgYWJsZSB0byBtb3ZlIHRoZSBjdXJzb3IgbmF0aXZlbHkuXG4gKi9cbmZ1bmN0aW9uIGtleUNvbW1hbmRNb3ZlU2VsZWN0aW9uVG9TdGFydE9mQmxvY2soZWRpdG9yU3RhdGUpIHtcbiAgdmFyIHNlbGVjdGlvbiA9IGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpO1xuICB2YXIgc3RhcnRLZXkgPSBzZWxlY3Rpb24uZ2V0U3RhcnRLZXkoKTtcbiAgcmV0dXJuIEVkaXRvclN0YXRlLnNldChlZGl0b3JTdGF0ZSwge1xuICAgIHNlbGVjdGlvbjogc2VsZWN0aW9uLm1lcmdlKHtcbiAgICAgIGFuY2hvcktleTogc3RhcnRLZXksXG4gICAgICBhbmNob3JPZmZzZXQ6IDAsXG4gICAgICBmb2N1c0tleTogc3RhcnRLZXksXG4gICAgICBmb2N1c09mZnNldDogMCxcbiAgICAgIGlzQmFja3dhcmQ6IGZhbHNlXG4gICAgfSksXG4gICAgZm9yY2VTZWxlY3Rpb246IHRydWVcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ga2V5Q29tbWFuZE1vdmVTZWxlY3Rpb25Ub1N0YXJ0T2ZCbG9jazsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQVJBO0FBVUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandMoveSelectionToStartOfBlock.js
