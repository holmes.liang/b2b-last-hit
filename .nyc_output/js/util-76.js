__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noop", function() { return noop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getKeyFromChildrenIndex", function() { return getKeyFromChildrenIndex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMenuIdFromSubMenuEventKey", function() { return getMenuIdFromSubMenuEventKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loopMenuItem", function() { return loopMenuItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loopMenuItemRecursively", function() { return loopMenuItemRecursively; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "menuAllProps", function() { return menuAllProps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getWidth", function() { return getWidth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setStyle", function() { return setStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isMobileDevice", function() { return isMobileDevice; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_isMobile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/isMobile */ "./node_modules/rc-menu/es/utils/isMobile.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}



function noop() {}
function getKeyFromChildrenIndex(child, menuEventKey, index) {
  var prefix = menuEventKey || '';
  return child.key || "".concat(prefix, "item_").concat(index);
}
function getMenuIdFromSubMenuEventKey(eventKey) {
  return "".concat(eventKey, "-menu-");
}
function loopMenuItem(children, cb) {
  var index = -1;
  react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(children, function (c) {
    index += 1;

    if (c && c.type && c.type.isMenuItemGroup) {
      react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(c.props.children, function (c2) {
        index += 1;
        cb(c2, index);
      });
    } else {
      cb(c, index);
    }
  });
}
function loopMenuItemRecursively(children, keys, ret) {
  /* istanbul ignore if */
  if (!children || ret.find) {
    return;
  }

  react__WEBPACK_IMPORTED_MODULE_0__["Children"].forEach(children, function (c) {
    if (c) {
      var construct = c.type;

      if (!construct || !(construct.isSubMenu || construct.isMenuItem || construct.isMenuItemGroup)) {
        return;
      }

      if (keys.indexOf(c.key) !== -1) {
        ret.find = true;
      } else if (c.props.children) {
        loopMenuItemRecursively(c.props.children, keys, ret);
      }
    }
  });
}
var menuAllProps = ['defaultSelectedKeys', 'selectedKeys', 'defaultOpenKeys', 'openKeys', 'mode', 'getPopupContainer', 'onSelect', 'onDeselect', 'onDestroy', 'openTransitionName', 'openAnimation', 'subMenuOpenDelay', 'subMenuCloseDelay', 'forceSubMenuRender', 'triggerSubMenuAction', 'level', 'selectable', 'multiple', 'onOpenChange', 'visible', 'focusable', 'defaultActiveFirst', 'prefixCls', 'inlineIndent', 'parentMenu', 'title', 'rootPrefixCls', 'eventKey', 'active', 'onItemHover', 'onTitleMouseEnter', 'onTitleMouseLeave', 'onTitleClick', 'popupAlign', 'popupOffset', 'isOpen', 'renderMenuItem', 'manualRef', 'subMenuKey', 'disabled', 'index', 'isSelected', 'store', 'activeKey', 'builtinPlacements', 'overflowedIndicator', 'motion', // the following keys found need to be removed from test regression
'attribute', 'value', 'popupClassName', 'inlineCollapsed', 'menu', 'theme', 'itemIcon', 'expandIcon']; // ref: https://github.com/ant-design/ant-design/issues/14007
// ref: https://bugs.chromium.org/p/chromium/issues/detail?id=360889
// getBoundingClientRect return the full precision value, which is
// not the same behavior as on chrome. Set the precision to 6 to
// unify their behavior

var getWidth = function getWidth(elem) {
  var width = elem && typeof elem.getBoundingClientRect === 'function' && elem.getBoundingClientRect().width;

  if (width) {
    width = +width.toFixed(6);
  }

  return width || 0;
};
var setStyle = function setStyle(elem, styleProperty, value) {
  if (elem && _typeof(elem.style) === 'object') {
    elem.style[styleProperty] = value;
  }
};
var isMobileDevice = function isMobileDevice() {
  return _utils_isMobile__WEBPACK_IMPORTED_MODULE_1__["default"].any;
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy91dGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy91dGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBpc01vYmlsZSBmcm9tICcuL3V0aWxzL2lzTW9iaWxlJztcbmV4cG9ydCBmdW5jdGlvbiBub29wKCkge31cbmV4cG9ydCBmdW5jdGlvbiBnZXRLZXlGcm9tQ2hpbGRyZW5JbmRleChjaGlsZCwgbWVudUV2ZW50S2V5LCBpbmRleCkge1xuICB2YXIgcHJlZml4ID0gbWVudUV2ZW50S2V5IHx8ICcnO1xuICByZXR1cm4gY2hpbGQua2V5IHx8IFwiXCIuY29uY2F0KHByZWZpeCwgXCJpdGVtX1wiKS5jb25jYXQoaW5kZXgpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldE1lbnVJZEZyb21TdWJNZW51RXZlbnRLZXkoZXZlbnRLZXkpIHtcbiAgcmV0dXJuIFwiXCIuY29uY2F0KGV2ZW50S2V5LCBcIi1tZW51LVwiKTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBsb29wTWVudUl0ZW0oY2hpbGRyZW4sIGNiKSB7XG4gIHZhciBpbmRleCA9IC0xO1xuICBSZWFjdC5DaGlsZHJlbi5mb3JFYWNoKGNoaWxkcmVuLCBmdW5jdGlvbiAoYykge1xuICAgIGluZGV4ICs9IDE7XG5cbiAgICBpZiAoYyAmJiBjLnR5cGUgJiYgYy50eXBlLmlzTWVudUl0ZW1Hcm91cCkge1xuICAgICAgUmVhY3QuQ2hpbGRyZW4uZm9yRWFjaChjLnByb3BzLmNoaWxkcmVuLCBmdW5jdGlvbiAoYzIpIHtcbiAgICAgICAgaW5kZXggKz0gMTtcbiAgICAgICAgY2IoYzIsIGluZGV4KTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjYihjLCBpbmRleCk7XG4gICAgfVxuICB9KTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBsb29wTWVudUl0ZW1SZWN1cnNpdmVseShjaGlsZHJlbiwga2V5cywgcmV0KSB7XG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAqL1xuICBpZiAoIWNoaWxkcmVuIHx8IHJldC5maW5kKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgUmVhY3QuQ2hpbGRyZW4uZm9yRWFjaChjaGlsZHJlbiwgZnVuY3Rpb24gKGMpIHtcbiAgICBpZiAoYykge1xuICAgICAgdmFyIGNvbnN0cnVjdCA9IGMudHlwZTtcblxuICAgICAgaWYgKCFjb25zdHJ1Y3QgfHwgIShjb25zdHJ1Y3QuaXNTdWJNZW51IHx8IGNvbnN0cnVjdC5pc01lbnVJdGVtIHx8IGNvbnN0cnVjdC5pc01lbnVJdGVtR3JvdXApKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKGtleXMuaW5kZXhPZihjLmtleSkgIT09IC0xKSB7XG4gICAgICAgIHJldC5maW5kID0gdHJ1ZTtcbiAgICAgIH0gZWxzZSBpZiAoYy5wcm9wcy5jaGlsZHJlbikge1xuICAgICAgICBsb29wTWVudUl0ZW1SZWN1cnNpdmVseShjLnByb3BzLmNoaWxkcmVuLCBrZXlzLCByZXQpO1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG59XG5leHBvcnQgdmFyIG1lbnVBbGxQcm9wcyA9IFsnZGVmYXVsdFNlbGVjdGVkS2V5cycsICdzZWxlY3RlZEtleXMnLCAnZGVmYXVsdE9wZW5LZXlzJywgJ29wZW5LZXlzJywgJ21vZGUnLCAnZ2V0UG9wdXBDb250YWluZXInLCAnb25TZWxlY3QnLCAnb25EZXNlbGVjdCcsICdvbkRlc3Ryb3knLCAnb3BlblRyYW5zaXRpb25OYW1lJywgJ29wZW5BbmltYXRpb24nLCAnc3ViTWVudU9wZW5EZWxheScsICdzdWJNZW51Q2xvc2VEZWxheScsICdmb3JjZVN1Yk1lbnVSZW5kZXInLCAndHJpZ2dlclN1Yk1lbnVBY3Rpb24nLCAnbGV2ZWwnLCAnc2VsZWN0YWJsZScsICdtdWx0aXBsZScsICdvbk9wZW5DaGFuZ2UnLCAndmlzaWJsZScsICdmb2N1c2FibGUnLCAnZGVmYXVsdEFjdGl2ZUZpcnN0JywgJ3ByZWZpeENscycsICdpbmxpbmVJbmRlbnQnLCAncGFyZW50TWVudScsICd0aXRsZScsICdyb290UHJlZml4Q2xzJywgJ2V2ZW50S2V5JywgJ2FjdGl2ZScsICdvbkl0ZW1Ib3ZlcicsICdvblRpdGxlTW91c2VFbnRlcicsICdvblRpdGxlTW91c2VMZWF2ZScsICdvblRpdGxlQ2xpY2snLCAncG9wdXBBbGlnbicsICdwb3B1cE9mZnNldCcsICdpc09wZW4nLCAncmVuZGVyTWVudUl0ZW0nLCAnbWFudWFsUmVmJywgJ3N1Yk1lbnVLZXknLCAnZGlzYWJsZWQnLCAnaW5kZXgnLCAnaXNTZWxlY3RlZCcsICdzdG9yZScsICdhY3RpdmVLZXknLCAnYnVpbHRpblBsYWNlbWVudHMnLCAnb3ZlcmZsb3dlZEluZGljYXRvcicsICdtb3Rpb24nLCAvLyB0aGUgZm9sbG93aW5nIGtleXMgZm91bmQgbmVlZCB0byBiZSByZW1vdmVkIGZyb20gdGVzdCByZWdyZXNzaW9uXG4nYXR0cmlidXRlJywgJ3ZhbHVlJywgJ3BvcHVwQ2xhc3NOYW1lJywgJ2lubGluZUNvbGxhcHNlZCcsICdtZW51JywgJ3RoZW1lJywgJ2l0ZW1JY29uJywgJ2V4cGFuZEljb24nXTsgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xNDAwN1xuLy8gcmVmOiBodHRwczovL2J1Z3MuY2hyb21pdW0ub3JnL3AvY2hyb21pdW0vaXNzdWVzL2RldGFpbD9pZD0zNjA4ODlcbi8vIGdldEJvdW5kaW5nQ2xpZW50UmVjdCByZXR1cm4gdGhlIGZ1bGwgcHJlY2lzaW9uIHZhbHVlLCB3aGljaCBpc1xuLy8gbm90IHRoZSBzYW1lIGJlaGF2aW9yIGFzIG9uIGNocm9tZS4gU2V0IHRoZSBwcmVjaXNpb24gdG8gNiB0b1xuLy8gdW5pZnkgdGhlaXIgYmVoYXZpb3JcblxuZXhwb3J0IHZhciBnZXRXaWR0aCA9IGZ1bmN0aW9uIGdldFdpZHRoKGVsZW0pIHtcbiAgdmFyIHdpZHRoID0gZWxlbSAmJiB0eXBlb2YgZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QgPT09ICdmdW5jdGlvbicgJiYgZWxlbS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS53aWR0aDtcblxuICBpZiAod2lkdGgpIHtcbiAgICB3aWR0aCA9ICt3aWR0aC50b0ZpeGVkKDYpO1xuICB9XG5cbiAgcmV0dXJuIHdpZHRoIHx8IDA7XG59O1xuZXhwb3J0IHZhciBzZXRTdHlsZSA9IGZ1bmN0aW9uIHNldFN0eWxlKGVsZW0sIHN0eWxlUHJvcGVydHksIHZhbHVlKSB7XG4gIGlmIChlbGVtICYmIF90eXBlb2YoZWxlbS5zdHlsZSkgPT09ICdvYmplY3QnKSB7XG4gICAgZWxlbS5zdHlsZVtzdHlsZVByb3BlcnR5XSA9IHZhbHVlO1xuICB9XG59O1xuZXhwb3J0IHZhciBpc01vYmlsZURldmljZSA9IGZ1bmN0aW9uIGlzTW9iaWxlRGV2aWNlKCkge1xuICByZXR1cm4gaXNNb2JpbGUuYW55O1xufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-menu/es/util.js
