var _curve = __webpack_require__(/*! ../core/curve */ "./node_modules/zrender/lib/core/curve.js");

var quadraticProjectPoint = _curve.quadraticProjectPoint;
/**
 * 二次贝塞尔曲线描边包含判断
 * @param  {number}  x0
 * @param  {number}  y0
 * @param  {number}  x1
 * @param  {number}  y1
 * @param  {number}  x2
 * @param  {number}  y2
 * @param  {number}  lineWidth
 * @param  {number}  x
 * @param  {number}  y
 * @return {boolean}
 */

function containStroke(x0, y0, x1, y1, x2, y2, lineWidth, x, y) {
  if (lineWidth === 0) {
    return false;
  }

  var _l = lineWidth; // Quick reject

  if (y > y0 + _l && y > y1 + _l && y > y2 + _l || y < y0 - _l && y < y1 - _l && y < y2 - _l || x > x0 + _l && x > x1 + _l && x > x2 + _l || x < x0 - _l && x < x1 - _l && x < x2 - _l) {
    return false;
  }

  var d = quadraticProjectPoint(x0, y0, x1, y1, x2, y2, x, y, null);
  return d <= _l / 2;
}

exports.containStroke = containStroke;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi9xdWFkcmF0aWMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9jb250YWluL3F1YWRyYXRpYy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX2N1cnZlID0gcmVxdWlyZShcIi4uL2NvcmUvY3VydmVcIik7XG5cbnZhciBxdWFkcmF0aWNQcm9qZWN0UG9pbnQgPSBfY3VydmUucXVhZHJhdGljUHJvamVjdFBvaW50O1xuXG4vKipcbiAqIOS6jOasoei0neWhnuWwlOabsue6v+aPj+i+ueWMheWQq+WIpOaWrVxuICogQHBhcmFtICB7bnVtYmVyfSAgeDBcbiAqIEBwYXJhbSAge251bWJlcn0gIHkwXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4MVxuICogQHBhcmFtICB7bnVtYmVyfSAgeTFcbiAqIEBwYXJhbSAge251bWJlcn0gIHgyXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB5MlxuICogQHBhcmFtICB7bnVtYmVyfSAgbGluZVdpZHRoXG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB4XG4gKiBAcGFyYW0gIHtudW1iZXJ9ICB5XG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBjb250YWluU3Ryb2tlKHgwLCB5MCwgeDEsIHkxLCB4MiwgeTIsIGxpbmVXaWR0aCwgeCwgeSkge1xuICBpZiAobGluZVdpZHRoID09PSAwKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgdmFyIF9sID0gbGluZVdpZHRoOyAvLyBRdWljayByZWplY3RcblxuICBpZiAoeSA+IHkwICsgX2wgJiYgeSA+IHkxICsgX2wgJiYgeSA+IHkyICsgX2wgfHwgeSA8IHkwIC0gX2wgJiYgeSA8IHkxIC0gX2wgJiYgeSA8IHkyIC0gX2wgfHwgeCA+IHgwICsgX2wgJiYgeCA+IHgxICsgX2wgJiYgeCA+IHgyICsgX2wgfHwgeCA8IHgwIC0gX2wgJiYgeCA8IHgxIC0gX2wgJiYgeCA8IHgyIC0gX2wpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICB2YXIgZCA9IHF1YWRyYXRpY1Byb2plY3RQb2ludCh4MCwgeTAsIHgxLCB5MSwgeDIsIHkyLCB4LCB5LCBudWxsKTtcbiAgcmV0dXJuIGQgPD0gX2wgLyAyO1xufVxuXG5leHBvcnRzLmNvbnRhaW5TdHJva2UgPSBjb250YWluU3Ryb2tlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/quadratic.js
