__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_ADD", function() { return STATUS_ADD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_KEEP", function() { return STATUS_KEEP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_REMOVE", function() { return STATUS_REMOVE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_REMOVED", function() { return STATUS_REMOVED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wrapKeyToObject", function() { return wrapKeyToObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseKeys", function() { return parseKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "diffKeys", function() { return diffKeys; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);

var STATUS_ADD = 'add';
var STATUS_KEEP = 'keep';
var STATUS_REMOVE = 'remove';
var STATUS_REMOVED = 'removed';
function wrapKeyToObject(key) {
  var keyObj = void 0;

  if (key && typeof key === 'object' && 'key' in key) {
    keyObj = key;
  } else {
    keyObj = {
      key: key
    };
  }

  return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, keyObj, {
    key: String(keyObj.key)
  });
}
function parseKeys() {
  var keys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return keys.map(wrapKeyToObject);
}
function diffKeys() {
  var prevKeys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var currentKeys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var list = [];
  var currentIndex = 0;
  var currentLen = currentKeys.length;
  var prevKeyObjects = parseKeys(prevKeys);
  var currentKeyObjects = parseKeys(currentKeys); // Check prev keys to insert or keep

  prevKeyObjects.forEach(function (keyObj) {
    var hit = false;

    for (var i = currentIndex; i < currentLen; i += 1) {
      var currentKeyObj = currentKeyObjects[i];

      if (currentKeyObj.key === keyObj.key) {
        // New added keys should add before current key
        if (currentIndex < i) {
          list = list.concat(currentKeyObjects.slice(currentIndex, i).map(function (obj) {
            return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, obj, {
              status: STATUS_ADD
            });
          }));
          currentIndex = i;
        }

        list.push(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, currentKeyObj, {
          status: STATUS_KEEP
        }));
        currentIndex += 1;
        hit = true;
        break;
      }
    } // If not hit, it means key is removed


    if (!hit) {
      list.push(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, keyObj, {
        status: STATUS_REMOVE
      }));
    }
  }); // Add rest to the list

  if (currentIndex < currentLen) {
    list = list.concat(currentKeyObjects.slice(currentIndex).map(function (obj) {
      return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, obj, {
        status: STATUS_ADD
      });
    }));
  }
  /**
   * Merge same key when it remove and add again:
   *    [1 - add, 2 - keep, 1 - remove] -> [1 - keep, 2 - keep]
   */


  var keys = {};
  list.forEach(function (_ref) {
    var key = _ref.key;
    keys[key] = (keys[key] || 0) + 1;
  });
  var duplicatedKeys = Object.keys(keys).filter(function (key) {
    return keys[key] > 1;
  });
  duplicatedKeys.forEach(function (matchKey) {
    // Remove `STATUS_REMOVE` node.
    list = list.filter(function (_ref2) {
      var key = _ref2.key,
          status = _ref2.status;
      return key !== matchKey || status !== STATUS_REMOVE;
    }); // Update `STATUS_ADD` to `STATUS_KEEP`

    list.forEach(function (node) {
      if (node.key === matchKey) {
        node.status = STATUS_KEEP;
      }
    });
  });
  return list;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtYW5pbWF0ZS9lcy91dGlsL2RpZmYuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1hbmltYXRlL2VzL3V0aWwvZGlmZi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuZXhwb3J0IHZhciBTVEFUVVNfQUREID0gJ2FkZCc7XG5leHBvcnQgdmFyIFNUQVRVU19LRUVQID0gJ2tlZXAnO1xuZXhwb3J0IHZhciBTVEFUVVNfUkVNT1ZFID0gJ3JlbW92ZSc7XG5leHBvcnQgdmFyIFNUQVRVU19SRU1PVkVEID0gJ3JlbW92ZWQnO1xuXG5leHBvcnQgZnVuY3Rpb24gd3JhcEtleVRvT2JqZWN0KGtleSkge1xuICB2YXIga2V5T2JqID0gdm9pZCAwO1xuICBpZiAoa2V5ICYmIHR5cGVvZiBrZXkgPT09ICdvYmplY3QnICYmICdrZXknIGluIGtleSkge1xuICAgIGtleU9iaiA9IGtleTtcbiAgfSBlbHNlIHtcbiAgICBrZXlPYmogPSB7IGtleToga2V5IH07XG4gIH1cbiAgcmV0dXJuIF9leHRlbmRzKHt9LCBrZXlPYmosIHtcbiAgICBrZXk6IFN0cmluZyhrZXlPYmoua2V5KVxuICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHBhcnNlS2V5cygpIHtcbiAgdmFyIGtleXMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IFtdO1xuXG4gIHJldHVybiBrZXlzLm1hcCh3cmFwS2V5VG9PYmplY3QpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZGlmZktleXMoKSB7XG4gIHZhciBwcmV2S2V5cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogW107XG4gIHZhciBjdXJyZW50S2V5cyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogW107XG5cbiAgdmFyIGxpc3QgPSBbXTtcbiAgdmFyIGN1cnJlbnRJbmRleCA9IDA7XG4gIHZhciBjdXJyZW50TGVuID0gY3VycmVudEtleXMubGVuZ3RoO1xuXG4gIHZhciBwcmV2S2V5T2JqZWN0cyA9IHBhcnNlS2V5cyhwcmV2S2V5cyk7XG4gIHZhciBjdXJyZW50S2V5T2JqZWN0cyA9IHBhcnNlS2V5cyhjdXJyZW50S2V5cyk7XG5cbiAgLy8gQ2hlY2sgcHJldiBrZXlzIHRvIGluc2VydCBvciBrZWVwXG4gIHByZXZLZXlPYmplY3RzLmZvckVhY2goZnVuY3Rpb24gKGtleU9iaikge1xuICAgIHZhciBoaXQgPSBmYWxzZTtcblxuICAgIGZvciAodmFyIGkgPSBjdXJyZW50SW5kZXg7IGkgPCBjdXJyZW50TGVuOyBpICs9IDEpIHtcbiAgICAgIHZhciBjdXJyZW50S2V5T2JqID0gY3VycmVudEtleU9iamVjdHNbaV07XG4gICAgICBpZiAoY3VycmVudEtleU9iai5rZXkgPT09IGtleU9iai5rZXkpIHtcbiAgICAgICAgLy8gTmV3IGFkZGVkIGtleXMgc2hvdWxkIGFkZCBiZWZvcmUgY3VycmVudCBrZXlcbiAgICAgICAgaWYgKGN1cnJlbnRJbmRleCA8IGkpIHtcbiAgICAgICAgICBsaXN0ID0gbGlzdC5jb25jYXQoY3VycmVudEtleU9iamVjdHMuc2xpY2UoY3VycmVudEluZGV4LCBpKS5tYXAoZnVuY3Rpb24gKG9iaikge1xuICAgICAgICAgICAgcmV0dXJuIF9leHRlbmRzKHt9LCBvYmosIHsgc3RhdHVzOiBTVEFUVVNfQUREIH0pO1xuICAgICAgICAgIH0pKTtcbiAgICAgICAgICBjdXJyZW50SW5kZXggPSBpO1xuICAgICAgICB9XG4gICAgICAgIGxpc3QucHVzaChfZXh0ZW5kcyh7fSwgY3VycmVudEtleU9iaiwge1xuICAgICAgICAgIHN0YXR1czogU1RBVFVTX0tFRVBcbiAgICAgICAgfSkpO1xuICAgICAgICBjdXJyZW50SW5kZXggKz0gMTtcblxuICAgICAgICBoaXQgPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBJZiBub3QgaGl0LCBpdCBtZWFucyBrZXkgaXMgcmVtb3ZlZFxuICAgIGlmICghaGl0KSB7XG4gICAgICBsaXN0LnB1c2goX2V4dGVuZHMoe30sIGtleU9iaiwge1xuICAgICAgICBzdGF0dXM6IFNUQVRVU19SRU1PVkVcbiAgICAgIH0pKTtcbiAgICB9XG4gIH0pO1xuXG4gIC8vIEFkZCByZXN0IHRvIHRoZSBsaXN0XG4gIGlmIChjdXJyZW50SW5kZXggPCBjdXJyZW50TGVuKSB7XG4gICAgbGlzdCA9IGxpc3QuY29uY2F0KGN1cnJlbnRLZXlPYmplY3RzLnNsaWNlKGN1cnJlbnRJbmRleCkubWFwKGZ1bmN0aW9uIChvYmopIHtcbiAgICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgb2JqLCB7IHN0YXR1czogU1RBVFVTX0FERCB9KTtcbiAgICB9KSk7XG4gIH1cblxuICAvKipcbiAgICogTWVyZ2Ugc2FtZSBrZXkgd2hlbiBpdCByZW1vdmUgYW5kIGFkZCBhZ2FpbjpcbiAgICogICAgWzEgLSBhZGQsIDIgLSBrZWVwLCAxIC0gcmVtb3ZlXSAtPiBbMSAtIGtlZXAsIDIgLSBrZWVwXVxuICAgKi9cbiAgdmFyIGtleXMgPSB7fTtcbiAgbGlzdC5mb3JFYWNoKGZ1bmN0aW9uIChfcmVmKSB7XG4gICAgdmFyIGtleSA9IF9yZWYua2V5O1xuXG4gICAga2V5c1trZXldID0gKGtleXNba2V5XSB8fCAwKSArIDE7XG4gIH0pO1xuICB2YXIgZHVwbGljYXRlZEtleXMgPSBPYmplY3Qua2V5cyhrZXlzKS5maWx0ZXIoZnVuY3Rpb24gKGtleSkge1xuICAgIHJldHVybiBrZXlzW2tleV0gPiAxO1xuICB9KTtcbiAgZHVwbGljYXRlZEtleXMuZm9yRWFjaChmdW5jdGlvbiAobWF0Y2hLZXkpIHtcbiAgICAvLyBSZW1vdmUgYFNUQVRVU19SRU1PVkVgIG5vZGUuXG4gICAgbGlzdCA9IGxpc3QuZmlsdGVyKGZ1bmN0aW9uIChfcmVmMikge1xuICAgICAgdmFyIGtleSA9IF9yZWYyLmtleSxcbiAgICAgICAgICBzdGF0dXMgPSBfcmVmMi5zdGF0dXM7XG4gICAgICByZXR1cm4ga2V5ICE9PSBtYXRjaEtleSB8fCBzdGF0dXMgIT09IFNUQVRVU19SRU1PVkU7XG4gICAgfSk7XG5cbiAgICAvLyBVcGRhdGUgYFNUQVRVU19BRERgIHRvIGBTVEFUVVNfS0VFUGBcbiAgICBsaXN0LmZvckVhY2goZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIGlmIChub2RlLmtleSA9PT0gbWF0Y2hLZXkpIHtcbiAgICAgICAgbm9kZS5zdGF0dXMgPSBTVEFUVVNfS0VFUDtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG5cbiAgcmV0dXJuIGxpc3Q7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBSUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-animate/es/util/diff.js
