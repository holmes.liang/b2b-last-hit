

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getThemeFromTypeName = getThemeFromTypeName;
exports.removeTypeTheme = removeTypeTheme;
exports.withThemeSuffix = withThemeSuffix;
exports.alias = alias;
exports.svgBaseProps = void 0;

var _warning = _interopRequireDefault(__webpack_require__(/*! ../_util/warning */ "./node_modules/antd/lib/_util/warning.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
} // These props make sure that the SVG behaviours like general text.
// Reference: https://blog.prototypr.io/align-svg-icons-to-text-and-say-goodbye-to-font-icons-d44b3d7b26b4


var svgBaseProps = {
  width: '1em',
  height: '1em',
  fill: 'currentColor',
  'aria-hidden': true,
  focusable: 'false'
};
exports.svgBaseProps = svgBaseProps;
var fillTester = /-fill$/;
var outlineTester = /-o$/;
var twoToneTester = /-twotone$/;

function getThemeFromTypeName(type) {
  var result = null;

  if (fillTester.test(type)) {
    result = 'filled';
  } else if (outlineTester.test(type)) {
    result = 'outlined';
  } else if (twoToneTester.test(type)) {
    result = 'twoTone';
  }

  return result;
}

function removeTypeTheme(type) {
  return type.replace(fillTester, '').replace(outlineTester, '').replace(twoToneTester, '');
}

function withThemeSuffix(type, theme) {
  var result = type;

  if (theme === 'filled') {
    result += '-fill';
  } else if (theme === 'outlined') {
    result += '-o';
  } else if (theme === 'twoTone') {
    result += '-twotone';
  } else {
    (0, _warning["default"])(false, 'Icon', "This icon '".concat(type, "' has unknown theme '").concat(theme, "'"));
  }

  return result;
} // For alias or compatibility


function alias(type) {
  var newType = type;

  switch (type) {
    case 'cross':
      newType = 'close';
      break;
    // https://github.com/ant-design/ant-design/issues/13007

    case 'interation':
      newType = 'interaction';
      break;
    // https://github.com/ant-design/ant-design/issues/16810

    case 'canlendar':
      newType = 'calendar';
      break;
    // https://github.com/ant-design/ant-design/issues/17448

    case 'colum-height':
      newType = 'column-height';
      break;

    default:
  }

  (0, _warning["default"])(newType === type, 'Icon', "Icon '".concat(type, "' was a typo and is now deprecated, please use '").concat(newType, "' instead."));
  return newType;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvaWNvbi91dGlscy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvaWNvbi91dGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbi8vIFRoZXNlIHByb3BzIG1ha2Ugc3VyZSB0aGF0IHRoZSBTVkcgYmVoYXZpb3VycyBsaWtlIGdlbmVyYWwgdGV4dC5cbi8vIFJlZmVyZW5jZTogaHR0cHM6Ly9ibG9nLnByb3RvdHlwci5pby9hbGlnbi1zdmctaWNvbnMtdG8tdGV4dC1hbmQtc2F5LWdvb2RieWUtdG8tZm9udC1pY29ucy1kNDRiM2Q3YjI2YjRcbmV4cG9ydCBjb25zdCBzdmdCYXNlUHJvcHMgPSB7XG4gICAgd2lkdGg6ICcxZW0nLFxuICAgIGhlaWdodDogJzFlbScsXG4gICAgZmlsbDogJ2N1cnJlbnRDb2xvcicsXG4gICAgJ2FyaWEtaGlkZGVuJzogdHJ1ZSxcbiAgICBmb2N1c2FibGU6ICdmYWxzZScsXG59O1xuY29uc3QgZmlsbFRlc3RlciA9IC8tZmlsbCQvO1xuY29uc3Qgb3V0bGluZVRlc3RlciA9IC8tbyQvO1xuY29uc3QgdHdvVG9uZVRlc3RlciA9IC8tdHdvdG9uZSQvO1xuZXhwb3J0IGZ1bmN0aW9uIGdldFRoZW1lRnJvbVR5cGVOYW1lKHR5cGUpIHtcbiAgICBsZXQgcmVzdWx0ID0gbnVsbDtcbiAgICBpZiAoZmlsbFRlc3Rlci50ZXN0KHR5cGUpKSB7XG4gICAgICAgIHJlc3VsdCA9ICdmaWxsZWQnO1xuICAgIH1cbiAgICBlbHNlIGlmIChvdXRsaW5lVGVzdGVyLnRlc3QodHlwZSkpIHtcbiAgICAgICAgcmVzdWx0ID0gJ291dGxpbmVkJztcbiAgICB9XG4gICAgZWxzZSBpZiAodHdvVG9uZVRlc3Rlci50ZXN0KHR5cGUpKSB7XG4gICAgICAgIHJlc3VsdCA9ICd0d29Ub25lJztcbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cbmV4cG9ydCBmdW5jdGlvbiByZW1vdmVUeXBlVGhlbWUodHlwZSkge1xuICAgIHJldHVybiB0eXBlXG4gICAgICAgIC5yZXBsYWNlKGZpbGxUZXN0ZXIsICcnKVxuICAgICAgICAucmVwbGFjZShvdXRsaW5lVGVzdGVyLCAnJylcbiAgICAgICAgLnJlcGxhY2UodHdvVG9uZVRlc3RlciwgJycpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIHdpdGhUaGVtZVN1ZmZpeCh0eXBlLCB0aGVtZSkge1xuICAgIGxldCByZXN1bHQgPSB0eXBlO1xuICAgIGlmICh0aGVtZSA9PT0gJ2ZpbGxlZCcpIHtcbiAgICAgICAgcmVzdWx0ICs9ICctZmlsbCc7XG4gICAgfVxuICAgIGVsc2UgaWYgKHRoZW1lID09PSAnb3V0bGluZWQnKSB7XG4gICAgICAgIHJlc3VsdCArPSAnLW8nO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGVtZSA9PT0gJ3R3b1RvbmUnKSB7XG4gICAgICAgIHJlc3VsdCArPSAnLXR3b3RvbmUnO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgd2FybmluZyhmYWxzZSwgJ0ljb24nLCBgVGhpcyBpY29uICcke3R5cGV9JyBoYXMgdW5rbm93biB0aGVtZSAnJHt0aGVtZX0nYCk7XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG59XG4vLyBGb3IgYWxpYXMgb3IgY29tcGF0aWJpbGl0eVxuZXhwb3J0IGZ1bmN0aW9uIGFsaWFzKHR5cGUpIHtcbiAgICBsZXQgbmV3VHlwZSA9IHR5cGU7XG4gICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgIGNhc2UgJ2Nyb3NzJzpcbiAgICAgICAgICAgIG5ld1R5cGUgPSAnY2xvc2UnO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzEzMDA3XG4gICAgICAgIGNhc2UgJ2ludGVyYXRpb24nOlxuICAgICAgICAgICAgbmV3VHlwZSA9ICdpbnRlcmFjdGlvbic7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTY4MTBcbiAgICAgICAgY2FzZSAnY2FubGVuZGFyJzpcbiAgICAgICAgICAgIG5ld1R5cGUgPSAnY2FsZW5kYXInO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE3NDQ4XG4gICAgICAgIGNhc2UgJ2NvbHVtLWhlaWdodCc6XG4gICAgICAgICAgICBuZXdUeXBlID0gJ2NvbHVtbi1oZWlnaHQnO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgfVxuICAgIHdhcm5pbmcobmV3VHlwZSA9PT0gdHlwZSwgJ0ljb24nLCBgSWNvbiAnJHt0eXBlfScgd2FzIGEgdHlwbyBhbmQgaXMgbm93IGRlcHJlY2F0ZWQsIHBsZWFzZSB1c2UgJyR7bmV3VHlwZX0nIGluc3RlYWQuYCk7XG4gICAgcmV0dXJuIG5ld1R5cGU7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBOztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFEQTtBQUlBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQWhCQTtBQUNBO0FBaUJBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/lib/icon/utils.js
