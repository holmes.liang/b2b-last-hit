__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _widget__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../widget */ "./src/component/widget.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/component/page/page-header.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n\t\t\t\tz-index: ", ";\n\t\t\t"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



/**
 * Page header
 */

var PageHeader =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(PageHeader, _Widget);

  function PageHeader() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, PageHeader);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(PageHeader).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(PageHeader, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_6__["Styled"].div.attrs({
          "data-widget": "page-header"
        })(_templateObject(), function (props) {
          return props.theme.PAGE_HEADER_Z_INDEX;
        })
      };
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 21
        },
        __self: this
      });
    }
  }]);

  return PageHeader;
}(_widget__WEBPACK_IMPORTED_MODULE_7__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (PageHeader);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L3BhZ2UvcGFnZS1oZWFkZXIudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tcG9uZW50L3BhZ2UvcGFnZS1oZWFkZXIudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBhZ2VIZWFkZXJDb21wb25lbnRzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IFdpZGdldCBmcm9tIFwiLi4vd2lkZ2V0XCI7XG5cbi8qKlxuICogUGFnZSBoZWFkZXJcbiAqL1xuY2xhc3MgUGFnZUhlYWRlcjxQLCBTLCBDIGV4dGVuZHMgUGFnZUhlYWRlckNvbXBvbmVudHM+IGV4dGVuZHMgV2lkZ2V0PFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3g6IFN0eWxlZC5kaXYuYXR0cnMoe1xuICAgICAgICBcImRhdGEtd2lkZ2V0XCI6IFwicGFnZS1oZWFkZXJcIixcbiAgICAgIH0pYFxuXHRcdFx0XHR6LWluZGV4OiAke3Byb3BzID0+IHByb3BzLnRoZW1lLlBBR0VfSEVBREVSX1pfSU5ERVh9O1xuXHRcdFx0YCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiA8Qy5Cb3gvPjtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBQYWdlSGVhZGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBRUE7Ozs7QUFHQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBSkE7QUFPQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Ozs7QUFkQTtBQUNBO0FBZ0JBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/component/page/page-header.tsx
