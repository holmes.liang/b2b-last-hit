__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./style */ "./src/app/desk/component/view-item/style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/view-item/view-item-copy.tsx";




var isMobile = _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].getIsMobile();
var defaultLayout = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 13
    },
    sm: {
      span: 16
    }
  }
};

var ViewItem = function ViewItem(_ref) {
  var title = _ref.title,
      children = _ref.children,
      _ref$layout = _ref.layout,
      layout = _ref$layout === void 0 ? defaultLayout : _ref$layout,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_ref, ["title", "children", "layout"]);

  return _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(_style__WEBPACK_IMPORTED_MODULE_3__["default"].box, Object.assign({}, rest, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    className: "item",
    type: "flex",
    gutter: 24,
    align: "middle",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], Object.assign({
    className: "itemTitle ".concat(isMobile ? "mobile-item-title" : "")
  }, layout.labelCol, {
    style: {
      paddingRight: 1
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }), title), _common_3rd__WEBPACK_IMPORTED_MODULE_1__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_2__["Col"], Object.assign({
    className: "itemContentRight"
  }, layout.wrapperCol, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }), children)));
};

/* harmony default export */ __webpack_exports__["default"] = (ViewItem);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3ZpZXctaXRlbS92aWV3LWl0ZW0tY29weS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvdmlldy1pdGVtL3ZpZXctaXRlbS1jb3B5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuXG5pbXBvcnQgU3R5bGUgZnJvbSBcIi4vc3R5bGVcIjtcbmltcG9ydCB7IFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcblxuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuY29uc3QgZGVmYXVsdExheW91dDogYW55ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDYgfSxcbiAgICBzbTogeyBzcGFuOiA4IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMyB9LFxuICAgIHNtOiB7IHNwYW46IDE2IH0sXG4gIH0sXG59O1xuXG5jb25zdCBWaWV3SXRlbSA9ICh7IHRpdGxlLCBjaGlsZHJlbiwgbGF5b3V0ID0gZGVmYXVsdExheW91dCwgLi4ucmVzdCB9OiBhbnkpID0+IHtcbiAgcmV0dXJuIChcbiAgICA8U3R5bGUuYm94IHsuLi5yZXN0fT5cbiAgICAgIDxSb3cgY2xhc3NOYW1lPVwiaXRlbVwiIHR5cGU9XCJmbGV4XCIgZ3V0dGVyPXsyNH0gYWxpZ249XCJtaWRkbGVcIj5cbiAgICAgICAgPENvbCBjbGFzc05hbWU9e2BpdGVtVGl0bGUgJHtpc01vYmlsZSA/IFwibW9iaWxlLWl0ZW0tdGl0bGVcIiA6IFwiXCJ9YH0gey4uLmxheW91dC5sYWJlbENvbH1cbiAgICAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nUmlnaHQ6IDEgfX0+XG4gICAgICAgICAge3RpdGxlfVxuICAgICAgICA8L0NvbD5cbiAgICAgICAgPENvbCBjbGFzc05hbWU9XCJpdGVtQ29udGVudFJpZ2h0XCIgey4uLmxheW91dC53cmFwcGVyQ29sfT5cbiAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgIDwvQ29sPlxuICAgICAgPC9Sb3c+XG4gICAgPC9TdHlsZS5ib3g+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBWaWV3SXRlbTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBQ0E7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/view-item/view-item-copy.tsx
