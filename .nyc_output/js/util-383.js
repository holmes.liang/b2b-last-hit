/**
 * @module zrender/core/util
 */
// 用于处理merge时无法遍历Date等对象的问题
var BUILTIN_OBJECT = {
  '[object Function]': 1,
  '[object RegExp]': 1,
  '[object Date]': 1,
  '[object Error]': 1,
  '[object CanvasGradient]': 1,
  '[object CanvasPattern]': 1,
  // For node-canvas
  '[object Image]': 1,
  '[object Canvas]': 1
};
var TYPED_ARRAY = {
  '[object Int8Array]': 1,
  '[object Uint8Array]': 1,
  '[object Uint8ClampedArray]': 1,
  '[object Int16Array]': 1,
  '[object Uint16Array]': 1,
  '[object Int32Array]': 1,
  '[object Uint32Array]': 1,
  '[object Float32Array]': 1,
  '[object Float64Array]': 1
};
var objToString = Object.prototype.toString;
var arrayProto = Array.prototype;
var nativeForEach = arrayProto.forEach;
var nativeFilter = arrayProto.filter;
var nativeSlice = arrayProto.slice;
var nativeMap = arrayProto.map;
var nativeReduce = arrayProto.reduce; // Avoid assign to an exported variable, for transforming to cjs.

var methods = {};

function $override(name, fn) {
  // Clear ctx instance for different environment
  if (name === 'createCanvas') {
    _ctx = null;
  }

  methods[name] = fn;
}
/**
 * Those data types can be cloned:
 *     Plain object, Array, TypedArray, number, string, null, undefined.
 * Those data types will be assgined using the orginal data:
 *     BUILTIN_OBJECT
 * Instance of user defined class will be cloned to a plain object, without
 * properties in prototype.
 * Other data types is not supported (not sure what will happen).
 *
 * Caution: do not support clone Date, for performance consideration.
 * (There might be a large number of date in `series.data`).
 * So date should not be modified in and out of echarts.
 *
 * @param {*} source
 * @return {*} new
 */


function clone(source) {
  if (source == null || typeof source !== 'object') {
    return source;
  }

  var result = source;
  var typeStr = objToString.call(source);

  if (typeStr === '[object Array]') {
    if (!isPrimitive(source)) {
      result = [];

      for (var i = 0, len = source.length; i < len; i++) {
        result[i] = clone(source[i]);
      }
    }
  } else if (TYPED_ARRAY[typeStr]) {
    if (!isPrimitive(source)) {
      var Ctor = source.constructor;

      if (source.constructor.from) {
        result = Ctor.from(source);
      } else {
        result = new Ctor(source.length);

        for (var i = 0, len = source.length; i < len; i++) {
          result[i] = clone(source[i]);
        }
      }
    }
  } else if (!BUILTIN_OBJECT[typeStr] && !isPrimitive(source) && !isDom(source)) {
    result = {};

    for (var key in source) {
      if (source.hasOwnProperty(key)) {
        result[key] = clone(source[key]);
      }
    }
  }

  return result;
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} target
 * @param {*} source
 * @param {boolean} [overwrite=false]
 */


function merge(target, source, overwrite) {
  // We should escapse that source is string
  // and enter for ... in ...
  if (!isObject(source) || !isObject(target)) {
    return overwrite ? clone(source) : target;
  }

  for (var key in source) {
    if (source.hasOwnProperty(key)) {
      var targetProp = target[key];
      var sourceProp = source[key];

      if (isObject(sourceProp) && isObject(targetProp) && !isArray(sourceProp) && !isArray(targetProp) && !isDom(sourceProp) && !isDom(targetProp) && !isBuiltInObject(sourceProp) && !isBuiltInObject(targetProp) && !isPrimitive(sourceProp) && !isPrimitive(targetProp)) {
        // 如果需要递归覆盖，就递归调用merge
        merge(targetProp, sourceProp, overwrite);
      } else if (overwrite || !(key in target)) {
        // 否则只处理overwrite为true，或者在目标对象中没有此属性的情况
        // NOTE，在 target[key] 不存在的时候也是直接覆盖
        target[key] = clone(source[key], true);
      }
    }
  }

  return target;
}
/**
 * @param {Array} targetAndSources The first item is target, and the rests are source.
 * @param {boolean} [overwrite=false]
 * @return {*} target
 */


function mergeAll(targetAndSources, overwrite) {
  var result = targetAndSources[0];

  for (var i = 1, len = targetAndSources.length; i < len; i++) {
    result = merge(result, targetAndSources[i], overwrite);
  }

  return result;
}
/**
 * @param {*} target
 * @param {*} source
 * @memberOf module:zrender/core/util
 */


function extend(target, source) {
  for (var key in source) {
    if (source.hasOwnProperty(key)) {
      target[key] = source[key];
    }
  }

  return target;
}
/**
 * @param {*} target
 * @param {*} source
 * @param {boolean} [overlay=false]
 * @memberOf module:zrender/core/util
 */


function defaults(target, source, overlay) {
  for (var key in source) {
    if (source.hasOwnProperty(key) && (overlay ? source[key] != null : target[key] == null)) {
      target[key] = source[key];
    }
  }

  return target;
}

var createCanvas = function createCanvas() {
  return methods.createCanvas();
};

methods.createCanvas = function () {
  return document.createElement('canvas');
}; // FIXME


var _ctx;

function getContext() {
  if (!_ctx) {
    // Use util.createCanvas instead of createCanvas
    // because createCanvas may be overwritten in different environment
    _ctx = createCanvas().getContext('2d');
  }

  return _ctx;
}
/**
 * 查询数组中元素的index
 * @memberOf module:zrender/core/util
 */


function indexOf(array, value) {
  if (array) {
    if (array.indexOf) {
      return array.indexOf(value);
    }

    for (var i = 0, len = array.length; i < len; i++) {
      if (array[i] === value) {
        return i;
      }
    }
  }

  return -1;
}
/**
 * 构造类继承关系
 *
 * @memberOf module:zrender/core/util
 * @param {Function} clazz 源类
 * @param {Function} baseClazz 基类
 */


function inherits(clazz, baseClazz) {
  var clazzPrototype = clazz.prototype;

  function F() {}

  F.prototype = baseClazz.prototype;
  clazz.prototype = new F();

  for (var prop in clazzPrototype) {
    clazz.prototype[prop] = clazzPrototype[prop];
  }

  clazz.prototype.constructor = clazz;
  clazz.superClass = baseClazz;
}
/**
 * @memberOf module:zrender/core/util
 * @param {Object|Function} target
 * @param {Object|Function} sorce
 * @param {boolean} overlay
 */


function mixin(target, source, overlay) {
  target = 'prototype' in target ? target.prototype : target;
  source = 'prototype' in source ? source.prototype : source;
  defaults(target, source, overlay);
}
/**
 * Consider typed array.
 * @param {Array|TypedArray} data
 */


function isArrayLike(data) {
  if (!data) {
    return;
  }

  if (typeof data === 'string') {
    return false;
  }

  return typeof data.length === 'number';
}
/**
 * 数组或对象遍历
 * @memberOf module:zrender/core/util
 * @param {Object|Array} obj
 * @param {Function} cb
 * @param {*} [context]
 */


function each(obj, cb, context) {
  if (!(obj && cb)) {
    return;
  }

  if (obj.forEach && obj.forEach === nativeForEach) {
    obj.forEach(cb, context);
  } else if (obj.length === +obj.length) {
    for (var i = 0, len = obj.length; i < len; i++) {
      cb.call(context, obj[i], i, obj);
    }
  } else {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        cb.call(context, obj[key], key, obj);
      }
    }
  }
}
/**
 * 数组映射
 * @memberOf module:zrender/core/util
 * @param {Array} obj
 * @param {Function} cb
 * @param {*} [context]
 * @return {Array}
 */


function map(obj, cb, context) {
  if (!(obj && cb)) {
    return;
  }

  if (obj.map && obj.map === nativeMap) {
    return obj.map(cb, context);
  } else {
    var result = [];

    for (var i = 0, len = obj.length; i < len; i++) {
      result.push(cb.call(context, obj[i], i, obj));
    }

    return result;
  }
}
/**
 * @memberOf module:zrender/core/util
 * @param {Array} obj
 * @param {Function} cb
 * @param {Object} [memo]
 * @param {*} [context]
 * @return {Array}
 */


function reduce(obj, cb, memo, context) {
  if (!(obj && cb)) {
    return;
  }

  if (obj.reduce && obj.reduce === nativeReduce) {
    return obj.reduce(cb, memo, context);
  } else {
    for (var i = 0, len = obj.length; i < len; i++) {
      memo = cb.call(context, memo, obj[i], i, obj);
    }

    return memo;
  }
}
/**
 * 数组过滤
 * @memberOf module:zrender/core/util
 * @param {Array} obj
 * @param {Function} cb
 * @param {*} [context]
 * @return {Array}
 */


function filter(obj, cb, context) {
  if (!(obj && cb)) {
    return;
  }

  if (obj.filter && obj.filter === nativeFilter) {
    return obj.filter(cb, context);
  } else {
    var result = [];

    for (var i = 0, len = obj.length; i < len; i++) {
      if (cb.call(context, obj[i], i, obj)) {
        result.push(obj[i]);
      }
    }

    return result;
  }
}
/**
 * 数组项查找
 * @memberOf module:zrender/core/util
 * @param {Array} obj
 * @param {Function} cb
 * @param {*} [context]
 * @return {*}
 */


function find(obj, cb, context) {
  if (!(obj && cb)) {
    return;
  }

  for (var i = 0, len = obj.length; i < len; i++) {
    if (cb.call(context, obj[i], i, obj)) {
      return obj[i];
    }
  }
}
/**
 * @memberOf module:zrender/core/util
 * @param {Function} func
 * @param {*} context
 * @return {Function}
 */


function bind(func, context) {
  var args = nativeSlice.call(arguments, 2);
  return function () {
    return func.apply(context, args.concat(nativeSlice.call(arguments)));
  };
}
/**
 * @memberOf module:zrender/core/util
 * @param {Function} func
 * @return {Function}
 */


function curry(func) {
  var args = nativeSlice.call(arguments, 1);
  return function () {
    return func.apply(this, args.concat(nativeSlice.call(arguments)));
  };
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isArray(value) {
  return objToString.call(value) === '[object Array]';
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isFunction(value) {
  return typeof value === 'function';
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isString(value) {
  return objToString.call(value) === '[object String]';
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isObject(value) {
  // Avoid a V8 JIT bug in Chrome 19-20.
  // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
  var type = typeof value;
  return type === 'function' || !!value && type === 'object';
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isBuiltInObject(value) {
  return !!BUILTIN_OBJECT[objToString.call(value)];
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isTypedArray(value) {
  return !!TYPED_ARRAY[objToString.call(value)];
}
/**
 * @memberOf module:zrender/core/util
 * @param {*} value
 * @return {boolean}
 */


function isDom(value) {
  return typeof value === 'object' && typeof value.nodeType === 'number' && typeof value.ownerDocument === 'object';
}
/**
 * Whether is exactly NaN. Notice isNaN('a') returns true.
 * @param {*} value
 * @return {boolean}
 */


function eqNaN(value) {
  return value !== value;
}
/**
 * If value1 is not null, then return value1, otherwise judget rest of values.
 * Low performance.
 * @memberOf module:zrender/core/util
 * @return {*} Final value
 */


function retrieve(values) {
  for (var i = 0, len = arguments.length; i < len; i++) {
    if (arguments[i] != null) {
      return arguments[i];
    }
  }
}

function retrieve2(value0, value1) {
  return value0 != null ? value0 : value1;
}

function retrieve3(value0, value1, value2) {
  return value0 != null ? value0 : value1 != null ? value1 : value2;
}
/**
 * @memberOf module:zrender/core/util
 * @param {Array} arr
 * @param {number} startIndex
 * @param {number} endIndex
 * @return {Array}
 */


function slice() {
  return Function.call.apply(nativeSlice, arguments);
}
/**
 * Normalize css liked array configuration
 * e.g.
 *  3 => [3, 3, 3, 3]
 *  [4, 2] => [4, 2, 4, 2]
 *  [4, 3, 2] => [4, 3, 2, 3]
 * @param {number|Array.<number>} val
 * @return {Array.<number>}
 */


function normalizeCssArray(val) {
  if (typeof val === 'number') {
    return [val, val, val, val];
  }

  var len = val.length;

  if (len === 2) {
    // vertical | horizontal
    return [val[0], val[1], val[0], val[1]];
  } else if (len === 3) {
    // top | horizontal | bottom
    return [val[0], val[1], val[2], val[1]];
  }

  return val;
}
/**
 * @memberOf module:zrender/core/util
 * @param {boolean} condition
 * @param {string} message
 */


function assert(condition, message) {
  if (!condition) {
    throw new Error(message);
  }
}
/**
 * @memberOf module:zrender/core/util
 * @param {string} str string to be trimed
 * @return {string} trimed string
 */


function trim(str) {
  if (str == null) {
    return null;
  } else if (typeof str.trim === 'function') {
    return str.trim();
  } else {
    return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  }
}

var primitiveKey = '__ec_primitive__';
/**
 * Set an object as primitive to be ignored traversing children in clone or merge
 */

function setAsPrimitive(obj) {
  obj[primitiveKey] = true;
}

function isPrimitive(obj) {
  return obj[primitiveKey];
}
/**
 * @constructor
 * @param {Object} obj Only apply `ownProperty`.
 */


function HashMap(obj) {
  var isArr = isArray(obj); // Key should not be set on this, otherwise
  // methods get/set/... may be overrided.

  this.data = {};
  var thisMap = this;
  obj instanceof HashMap ? obj.each(visit) : obj && each(obj, visit);

  function visit(value, key) {
    isArr ? thisMap.set(value, key) : thisMap.set(key, value);
  }
}

HashMap.prototype = {
  constructor: HashMap,
  // Do not provide `has` method to avoid defining what is `has`.
  // (We usually treat `null` and `undefined` as the same, different
  // from ES6 Map).
  get: function get(key) {
    return this.data.hasOwnProperty(key) ? this.data[key] : null;
  },
  set: function set(key, value) {
    // Comparing with invocation chaining, `return value` is more commonly
    // used in this case: `var someVal = map.set('a', genVal());`
    return this.data[key] = value;
  },
  // Although util.each can be performed on this hashMap directly, user
  // should not use the exposed keys, who are prefixed.
  each: function each(cb, context) {
    context !== void 0 && (cb = bind(cb, context));

    for (var key in this.data) {
      this.data.hasOwnProperty(key) && cb(this.data[key], key);
    }
  },
  // Do not use this method if performance sensitive.
  removeKey: function removeKey(key) {
    delete this.data[key];
  }
};

function createHashMap(obj) {
  return new HashMap(obj);
}

function concatArray(a, b) {
  var newArray = new a.constructor(a.length + b.length);

  for (var i = 0; i < a.length; i++) {
    newArray[i] = a[i];
  }

  var offset = a.length;

  for (i = 0; i < b.length; i++) {
    newArray[i + offset] = b[i];
  }

  return newArray;
}

function noop() {}

exports.$override = $override;
exports.clone = clone;
exports.merge = merge;
exports.mergeAll = mergeAll;
exports.extend = extend;
exports.defaults = defaults;
exports.createCanvas = createCanvas;
exports.getContext = getContext;
exports.indexOf = indexOf;
exports.inherits = inherits;
exports.mixin = mixin;
exports.isArrayLike = isArrayLike;
exports.each = each;
exports.map = map;
exports.reduce = reduce;
exports.filter = filter;
exports.find = find;
exports.bind = bind;
exports.curry = curry;
exports.isArray = isArray;
exports.isFunction = isFunction;
exports.isString = isString;
exports.isObject = isObject;
exports.isBuiltInObject = isBuiltInObject;
exports.isTypedArray = isTypedArray;
exports.isDom = isDom;
exports.eqNaN = eqNaN;
exports.retrieve = retrieve;
exports.retrieve2 = retrieve2;
exports.retrieve3 = retrieve3;
exports.slice = slice;
exports.normalizeCssArray = normalizeCssArray;
exports.assert = assert;
exports.trim = trim;
exports.setAsPrimitive = setAsPrimitive;
exports.isPrimitive = isPrimitive;
exports.createHashMap = createHashMap;
exports.concatArray = concatArray;
exports.noop = noop;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS91dGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS91dGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQG1vZHVsZSB6cmVuZGVyL2NvcmUvdXRpbFxuICovXG4vLyDnlKjkuo7lpITnkIZtZXJnZeaXtuaXoOazlemBjeWOhkRhdGXnrYnlr7nosaHnmoTpl67pophcbnZhciBCVUlMVElOX09CSkVDVCA9IHtcbiAgJ1tvYmplY3QgRnVuY3Rpb25dJzogMSxcbiAgJ1tvYmplY3QgUmVnRXhwXSc6IDEsXG4gICdbb2JqZWN0IERhdGVdJzogMSxcbiAgJ1tvYmplY3QgRXJyb3JdJzogMSxcbiAgJ1tvYmplY3QgQ2FudmFzR3JhZGllbnRdJzogMSxcbiAgJ1tvYmplY3QgQ2FudmFzUGF0dGVybl0nOiAxLFxuICAvLyBGb3Igbm9kZS1jYW52YXNcbiAgJ1tvYmplY3QgSW1hZ2VdJzogMSxcbiAgJ1tvYmplY3QgQ2FudmFzXSc6IDFcbn07XG52YXIgVFlQRURfQVJSQVkgPSB7XG4gICdbb2JqZWN0IEludDhBcnJheV0nOiAxLFxuICAnW29iamVjdCBVaW50OEFycmF5XSc6IDEsXG4gICdbb2JqZWN0IFVpbnQ4Q2xhbXBlZEFycmF5XSc6IDEsXG4gICdbb2JqZWN0IEludDE2QXJyYXldJzogMSxcbiAgJ1tvYmplY3QgVWludDE2QXJyYXldJzogMSxcbiAgJ1tvYmplY3QgSW50MzJBcnJheV0nOiAxLFxuICAnW29iamVjdCBVaW50MzJBcnJheV0nOiAxLFxuICAnW29iamVjdCBGbG9hdDMyQXJyYXldJzogMSxcbiAgJ1tvYmplY3QgRmxvYXQ2NEFycmF5XSc6IDFcbn07XG52YXIgb2JqVG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nO1xudmFyIGFycmF5UHJvdG8gPSBBcnJheS5wcm90b3R5cGU7XG52YXIgbmF0aXZlRm9yRWFjaCA9IGFycmF5UHJvdG8uZm9yRWFjaDtcbnZhciBuYXRpdmVGaWx0ZXIgPSBhcnJheVByb3RvLmZpbHRlcjtcbnZhciBuYXRpdmVTbGljZSA9IGFycmF5UHJvdG8uc2xpY2U7XG52YXIgbmF0aXZlTWFwID0gYXJyYXlQcm90by5tYXA7XG52YXIgbmF0aXZlUmVkdWNlID0gYXJyYXlQcm90by5yZWR1Y2U7IC8vIEF2b2lkIGFzc2lnbiB0byBhbiBleHBvcnRlZCB2YXJpYWJsZSwgZm9yIHRyYW5zZm9ybWluZyB0byBjanMuXG5cbnZhciBtZXRob2RzID0ge307XG5cbmZ1bmN0aW9uICRvdmVycmlkZShuYW1lLCBmbikge1xuICAvLyBDbGVhciBjdHggaW5zdGFuY2UgZm9yIGRpZmZlcmVudCBlbnZpcm9ubWVudFxuICBpZiAobmFtZSA9PT0gJ2NyZWF0ZUNhbnZhcycpIHtcbiAgICBfY3R4ID0gbnVsbDtcbiAgfVxuXG4gIG1ldGhvZHNbbmFtZV0gPSBmbjtcbn1cbi8qKlxuICogVGhvc2UgZGF0YSB0eXBlcyBjYW4gYmUgY2xvbmVkOlxuICogICAgIFBsYWluIG9iamVjdCwgQXJyYXksIFR5cGVkQXJyYXksIG51bWJlciwgc3RyaW5nLCBudWxsLCB1bmRlZmluZWQuXG4gKiBUaG9zZSBkYXRhIHR5cGVzIHdpbGwgYmUgYXNzZ2luZWQgdXNpbmcgdGhlIG9yZ2luYWwgZGF0YTpcbiAqICAgICBCVUlMVElOX09CSkVDVFxuICogSW5zdGFuY2Ugb2YgdXNlciBkZWZpbmVkIGNsYXNzIHdpbGwgYmUgY2xvbmVkIHRvIGEgcGxhaW4gb2JqZWN0LCB3aXRob3V0XG4gKiBwcm9wZXJ0aWVzIGluIHByb3RvdHlwZS5cbiAqIE90aGVyIGRhdGEgdHlwZXMgaXMgbm90IHN1cHBvcnRlZCAobm90IHN1cmUgd2hhdCB3aWxsIGhhcHBlbikuXG4gKlxuICogQ2F1dGlvbjogZG8gbm90IHN1cHBvcnQgY2xvbmUgRGF0ZSwgZm9yIHBlcmZvcm1hbmNlIGNvbnNpZGVyYXRpb24uXG4gKiAoVGhlcmUgbWlnaHQgYmUgYSBsYXJnZSBudW1iZXIgb2YgZGF0ZSBpbiBgc2VyaWVzLmRhdGFgKS5cbiAqIFNvIGRhdGUgc2hvdWxkIG5vdCBiZSBtb2RpZmllZCBpbiBhbmQgb3V0IG9mIGVjaGFydHMuXG4gKlxuICogQHBhcmFtIHsqfSBzb3VyY2VcbiAqIEByZXR1cm4geyp9IG5ld1xuICovXG5cblxuZnVuY3Rpb24gY2xvbmUoc291cmNlKSB7XG4gIGlmIChzb3VyY2UgPT0gbnVsbCB8fCB0eXBlb2Ygc291cmNlICE9PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBzb3VyY2U7XG4gIH1cblxuICB2YXIgcmVzdWx0ID0gc291cmNlO1xuICB2YXIgdHlwZVN0ciA9IG9ialRvU3RyaW5nLmNhbGwoc291cmNlKTtcblxuICBpZiAodHlwZVN0ciA9PT0gJ1tvYmplY3QgQXJyYXldJykge1xuICAgIGlmICghaXNQcmltaXRpdmUoc291cmNlKSkge1xuICAgICAgcmVzdWx0ID0gW107XG5cbiAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBzb3VyY2UubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgcmVzdWx0W2ldID0gY2xvbmUoc291cmNlW2ldKTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSBpZiAoVFlQRURfQVJSQVlbdHlwZVN0cl0pIHtcbiAgICBpZiAoIWlzUHJpbWl0aXZlKHNvdXJjZSkpIHtcbiAgICAgIHZhciBDdG9yID0gc291cmNlLmNvbnN0cnVjdG9yO1xuXG4gICAgICBpZiAoc291cmNlLmNvbnN0cnVjdG9yLmZyb20pIHtcbiAgICAgICAgcmVzdWx0ID0gQ3Rvci5mcm9tKHNvdXJjZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXN1bHQgPSBuZXcgQ3Rvcihzb3VyY2UubGVuZ3RoKTtcblxuICAgICAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gc291cmNlLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgcmVzdWx0W2ldID0gY2xvbmUoc291cmNlW2ldKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIGlmICghQlVJTFRJTl9PQkpFQ1RbdHlwZVN0cl0gJiYgIWlzUHJpbWl0aXZlKHNvdXJjZSkgJiYgIWlzRG9tKHNvdXJjZSkpIHtcbiAgICByZXN1bHQgPSB7fTtcblxuICAgIGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHtcbiAgICAgIGlmIChzb3VyY2UuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICByZXN1bHRba2V5XSA9IGNsb25lKHNvdXJjZVtrZXldKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0geyp9IHRhcmdldFxuICogQHBhcmFtIHsqfSBzb3VyY2VcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW292ZXJ3cml0ZT1mYWxzZV1cbiAqL1xuXG5cbmZ1bmN0aW9uIG1lcmdlKHRhcmdldCwgc291cmNlLCBvdmVyd3JpdGUpIHtcbiAgLy8gV2Ugc2hvdWxkIGVzY2Fwc2UgdGhhdCBzb3VyY2UgaXMgc3RyaW5nXG4gIC8vIGFuZCBlbnRlciBmb3IgLi4uIGluIC4uLlxuICBpZiAoIWlzT2JqZWN0KHNvdXJjZSkgfHwgIWlzT2JqZWN0KHRhcmdldCkpIHtcbiAgICByZXR1cm4gb3ZlcndyaXRlID8gY2xvbmUoc291cmNlKSA6IHRhcmdldDtcbiAgfVxuXG4gIGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHtcbiAgICBpZiAoc291cmNlLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIHZhciB0YXJnZXRQcm9wID0gdGFyZ2V0W2tleV07XG4gICAgICB2YXIgc291cmNlUHJvcCA9IHNvdXJjZVtrZXldO1xuXG4gICAgICBpZiAoaXNPYmplY3Qoc291cmNlUHJvcCkgJiYgaXNPYmplY3QodGFyZ2V0UHJvcCkgJiYgIWlzQXJyYXkoc291cmNlUHJvcCkgJiYgIWlzQXJyYXkodGFyZ2V0UHJvcCkgJiYgIWlzRG9tKHNvdXJjZVByb3ApICYmICFpc0RvbSh0YXJnZXRQcm9wKSAmJiAhaXNCdWlsdEluT2JqZWN0KHNvdXJjZVByb3ApICYmICFpc0J1aWx0SW5PYmplY3QodGFyZ2V0UHJvcCkgJiYgIWlzUHJpbWl0aXZlKHNvdXJjZVByb3ApICYmICFpc1ByaW1pdGl2ZSh0YXJnZXRQcm9wKSkge1xuICAgICAgICAvLyDlpoLmnpzpnIDopoHpgJLlvZLopobnm5bvvIzlsLHpgJLlvZLosIPnlKhtZXJnZVxuICAgICAgICBtZXJnZSh0YXJnZXRQcm9wLCBzb3VyY2VQcm9wLCBvdmVyd3JpdGUpO1xuICAgICAgfSBlbHNlIGlmIChvdmVyd3JpdGUgfHwgIShrZXkgaW4gdGFyZ2V0KSkge1xuICAgICAgICAvLyDlkKbliJnlj6rlpITnkIZvdmVyd3JpdGXkuLp0cnVl77yM5oiW6ICF5Zyo55uu5qCH5a+56LGh5Lit5rKh5pyJ5q2k5bGe5oCn55qE5oOF5Ya1XG4gICAgICAgIC8vIE5PVEXvvIzlnKggdGFyZ2V0W2tleV0g5LiN5a2Y5Zyo55qE5pe25YCZ5Lmf5piv55u05o6l6KaG55uWXG4gICAgICAgIHRhcmdldFtrZXldID0gY2xvbmUoc291cmNlW2tleV0sIHRydWUpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0YXJnZXQ7XG59XG4vKipcbiAqIEBwYXJhbSB7QXJyYXl9IHRhcmdldEFuZFNvdXJjZXMgVGhlIGZpcnN0IGl0ZW0gaXMgdGFyZ2V0LCBhbmQgdGhlIHJlc3RzIGFyZSBzb3VyY2UuXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvdmVyd3JpdGU9ZmFsc2VdXG4gKiBAcmV0dXJuIHsqfSB0YXJnZXRcbiAqL1xuXG5cbmZ1bmN0aW9uIG1lcmdlQWxsKHRhcmdldEFuZFNvdXJjZXMsIG92ZXJ3cml0ZSkge1xuICB2YXIgcmVzdWx0ID0gdGFyZ2V0QW5kU291cmNlc1swXTtcblxuICBmb3IgKHZhciBpID0gMSwgbGVuID0gdGFyZ2V0QW5kU291cmNlcy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgIHJlc3VsdCA9IG1lcmdlKHJlc3VsdCwgdGFyZ2V0QW5kU291cmNlc1tpXSwgb3ZlcndyaXRlKTtcbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59XG4vKipcbiAqIEBwYXJhbSB7Kn0gdGFyZ2V0XG4gKiBAcGFyYW0geyp9IHNvdXJjZVxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICovXG5cblxuZnVuY3Rpb24gZXh0ZW5kKHRhcmdldCwgc291cmNlKSB7XG4gIGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHtcbiAgICBpZiAoc291cmNlLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgIHRhcmdldFtrZXldID0gc291cmNlW2tleV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldDtcbn1cbi8qKlxuICogQHBhcmFtIHsqfSB0YXJnZXRcbiAqIEBwYXJhbSB7Kn0gc291cmNlXG4gKiBAcGFyYW0ge2Jvb2xlYW59IFtvdmVybGF5PWZhbHNlXVxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICovXG5cblxuZnVuY3Rpb24gZGVmYXVsdHModGFyZ2V0LCBzb3VyY2UsIG92ZXJsYXkpIHtcbiAgZm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xuICAgIGlmIChzb3VyY2UuaGFzT3duUHJvcGVydHkoa2V5KSAmJiAob3ZlcmxheSA/IHNvdXJjZVtrZXldICE9IG51bGwgOiB0YXJnZXRba2V5XSA9PSBudWxsKSkge1xuICAgICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGFyZ2V0O1xufVxuXG52YXIgY3JlYXRlQ2FudmFzID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gbWV0aG9kcy5jcmVhdGVDYW52YXMoKTtcbn07XG5cbm1ldGhvZHMuY3JlYXRlQ2FudmFzID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG59OyAvLyBGSVhNRVxuXG5cbnZhciBfY3R4O1xuXG5mdW5jdGlvbiBnZXRDb250ZXh0KCkge1xuICBpZiAoIV9jdHgpIHtcbiAgICAvLyBVc2UgdXRpbC5jcmVhdGVDYW52YXMgaW5zdGVhZCBvZiBjcmVhdGVDYW52YXNcbiAgICAvLyBiZWNhdXNlIGNyZWF0ZUNhbnZhcyBtYXkgYmUgb3ZlcndyaXR0ZW4gaW4gZGlmZmVyZW50IGVudmlyb25tZW50XG4gICAgX2N0eCA9IGNyZWF0ZUNhbnZhcygpLmdldENvbnRleHQoJzJkJyk7XG4gIH1cblxuICByZXR1cm4gX2N0eDtcbn1cbi8qKlxuICog5p+l6K+i5pWw57uE5Lit5YWD57Sg55qEaW5kZXhcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqL1xuXG5cbmZ1bmN0aW9uIGluZGV4T2YoYXJyYXksIHZhbHVlKSB7XG4gIGlmIChhcnJheSkge1xuICAgIGlmIChhcnJheS5pbmRleE9mKSB7XG4gICAgICByZXR1cm4gYXJyYXkuaW5kZXhPZih2YWx1ZSk7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IGFycmF5Lmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICBpZiAoYXJyYXlbaV0gPT09IHZhbHVlKSB7XG4gICAgICAgIHJldHVybiBpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiAtMTtcbn1cbi8qKlxuICog5p6E6YCg57G757un5om/5YWz57O7XG4gKlxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2xhenog5rqQ57G7XG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBiYXNlQ2xhenog5Z+657G7XG4gKi9cblxuXG5mdW5jdGlvbiBpbmhlcml0cyhjbGF6eiwgYmFzZUNsYXp6KSB7XG4gIHZhciBjbGF6elByb3RvdHlwZSA9IGNsYXp6LnByb3RvdHlwZTtcblxuICBmdW5jdGlvbiBGKCkge31cblxuICBGLnByb3RvdHlwZSA9IGJhc2VDbGF6ei5wcm90b3R5cGU7XG4gIGNsYXp6LnByb3RvdHlwZSA9IG5ldyBGKCk7XG5cbiAgZm9yICh2YXIgcHJvcCBpbiBjbGF6elByb3RvdHlwZSkge1xuICAgIGNsYXp6LnByb3RvdHlwZVtwcm9wXSA9IGNsYXp6UHJvdG90eXBlW3Byb3BdO1xuICB9XG5cbiAgY2xhenoucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gY2xheno7XG4gIGNsYXp6LnN1cGVyQ2xhc3MgPSBiYXNlQ2xheno7XG59XG4vKipcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7T2JqZWN0fEZ1bmN0aW9ufSB0YXJnZXRcbiAqIEBwYXJhbSB7T2JqZWN0fEZ1bmN0aW9ufSBzb3JjZVxuICogQHBhcmFtIHtib29sZWFufSBvdmVybGF5XG4gKi9cblxuXG5mdW5jdGlvbiBtaXhpbih0YXJnZXQsIHNvdXJjZSwgb3ZlcmxheSkge1xuICB0YXJnZXQgPSAncHJvdG90eXBlJyBpbiB0YXJnZXQgPyB0YXJnZXQucHJvdG90eXBlIDogdGFyZ2V0O1xuICBzb3VyY2UgPSAncHJvdG90eXBlJyBpbiBzb3VyY2UgPyBzb3VyY2UucHJvdG90eXBlIDogc291cmNlO1xuICBkZWZhdWx0cyh0YXJnZXQsIHNvdXJjZSwgb3ZlcmxheSk7XG59XG4vKipcbiAqIENvbnNpZGVyIHR5cGVkIGFycmF5LlxuICogQHBhcmFtIHtBcnJheXxUeXBlZEFycmF5fSBkYXRhXG4gKi9cblxuXG5mdW5jdGlvbiBpc0FycmF5TGlrZShkYXRhKSB7XG4gIGlmICghZGF0YSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICByZXR1cm4gdHlwZW9mIGRhdGEubGVuZ3RoID09PSAnbnVtYmVyJztcbn1cbi8qKlxuICog5pWw57uE5oiW5a+56LGh6YGN5Y6GXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge09iamVjdHxBcnJheX0gb2JqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBbY29udGV4dF1cbiAqL1xuXG5cbmZ1bmN0aW9uIGVhY2gob2JqLCBjYiwgY29udGV4dCkge1xuICBpZiAoIShvYmogJiYgY2IpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKG9iai5mb3JFYWNoICYmIG9iai5mb3JFYWNoID09PSBuYXRpdmVGb3JFYWNoKSB7XG4gICAgb2JqLmZvckVhY2goY2IsIGNvbnRleHQpO1xuICB9IGVsc2UgaWYgKG9iai5sZW5ndGggPT09ICtvYmoubGVuZ3RoKSB7XG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IG9iai5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgY2IuY2FsbChjb250ZXh0LCBvYmpbaV0sIGksIG9iaik7XG4gICAgfVxuICB9IGVsc2Uge1xuICAgIGZvciAodmFyIGtleSBpbiBvYmopIHtcbiAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBjYi5jYWxsKGNvbnRleHQsIG9ialtrZXldLCBrZXksIG9iaik7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4vKipcbiAqIOaVsOe7hOaYoOWwhFxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHtBcnJheX0gb2JqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYlxuICogQHBhcmFtIHsqfSBbY29udGV4dF1cbiAqIEByZXR1cm4ge0FycmF5fVxuICovXG5cblxuZnVuY3Rpb24gbWFwKG9iaiwgY2IsIGNvbnRleHQpIHtcbiAgaWYgKCEob2JqICYmIGNiKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChvYmoubWFwICYmIG9iai5tYXAgPT09IG5hdGl2ZU1hcCkge1xuICAgIHJldHVybiBvYmoubWFwKGNiLCBjb250ZXh0KTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgcmVzdWx0ID0gW107XG5cbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gb2JqLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICByZXN1bHQucHVzaChjYi5jYWxsKGNvbnRleHQsIG9ialtpXSwgaSwgb2JqKSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge0FycmF5fSBvYmpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNiXG4gKiBAcGFyYW0ge09iamVjdH0gW21lbW9dXG4gKiBAcGFyYW0geyp9IFtjb250ZXh0XVxuICogQHJldHVybiB7QXJyYXl9XG4gKi9cblxuXG5mdW5jdGlvbiByZWR1Y2Uob2JqLCBjYiwgbWVtbywgY29udGV4dCkge1xuICBpZiAoIShvYmogJiYgY2IpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKG9iai5yZWR1Y2UgJiYgb2JqLnJlZHVjZSA9PT0gbmF0aXZlUmVkdWNlKSB7XG4gICAgcmV0dXJuIG9iai5yZWR1Y2UoY2IsIG1lbW8sIGNvbnRleHQpO1xuICB9IGVsc2Uge1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBvYmoubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgIG1lbW8gPSBjYi5jYWxsKGNvbnRleHQsIG1lbW8sIG9ialtpXSwgaSwgb2JqKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbWVtbztcbiAgfVxufVxuLyoqXG4gKiDmlbDnu4Tov4fmu6RcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7QXJyYXl9IG9ialxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAqIEBwYXJhbSB7Kn0gW2NvbnRleHRdXG4gKiBAcmV0dXJuIHtBcnJheX1cbiAqL1xuXG5cbmZ1bmN0aW9uIGZpbHRlcihvYmosIGNiLCBjb250ZXh0KSB7XG4gIGlmICghKG9iaiAmJiBjYikpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBpZiAob2JqLmZpbHRlciAmJiBvYmouZmlsdGVyID09PSBuYXRpdmVGaWx0ZXIpIHtcbiAgICByZXR1cm4gb2JqLmZpbHRlcihjYiwgY29udGV4dCk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuXG4gICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IG9iai5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgaWYgKGNiLmNhbGwoY29udGV4dCwgb2JqW2ldLCBpLCBvYmopKSB7XG4gICAgICAgIHJlc3VsdC5wdXNoKG9ialtpXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufVxuLyoqXG4gKiDmlbDnu4Tpobnmn6Xmib5cbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7QXJyYXl9IG9ialxuICogQHBhcmFtIHtGdW5jdGlvbn0gY2JcbiAqIEBwYXJhbSB7Kn0gW2NvbnRleHRdXG4gKiBAcmV0dXJuIHsqfVxuICovXG5cblxuZnVuY3Rpb24gZmluZChvYmosIGNiLCBjb250ZXh0KSB7XG4gIGlmICghKG9iaiAmJiBjYikpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICBmb3IgKHZhciBpID0gMCwgbGVuID0gb2JqLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgaWYgKGNiLmNhbGwoY29udGV4dCwgb2JqW2ldLCBpLCBvYmopKSB7XG4gICAgICByZXR1cm4gb2JqW2ldO1xuICAgIH1cbiAgfVxufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jXG4gKiBAcGFyYW0geyp9IGNvbnRleHRcbiAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICovXG5cblxuZnVuY3Rpb24gYmluZChmdW5jLCBjb250ZXh0KSB7XG4gIHZhciBhcmdzID0gbmF0aXZlU2xpY2UuY2FsbChhcmd1bWVudHMsIDIpO1xuICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiBmdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MuY29uY2F0KG5hdGl2ZVNsaWNlLmNhbGwoYXJndW1lbnRzKSkpO1xuICB9O1xufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGN1cnJ5KGZ1bmMpIHtcbiAgdmFyIGFyZ3MgPSBuYXRpdmVTbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGZ1bmMuYXBwbHkodGhpcywgYXJncy5jb25jYXQobmF0aXZlU2xpY2UuY2FsbChhcmd1bWVudHMpKSk7XG4gIH07XG59XG4vKipcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG5mdW5jdGlvbiBpc0FycmF5KHZhbHVlKSB7XG4gIHJldHVybiBvYmpUb1N0cmluZy5jYWxsKHZhbHVlKSA9PT0gJ1tvYmplY3QgQXJyYXldJztcbn1cbi8qKlxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHsqfSB2YWx1ZVxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGlzRnVuY3Rpb24odmFsdWUpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PT0gJ2Z1bmN0aW9uJztcbn1cbi8qKlxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHsqfSB2YWx1ZVxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGlzU3RyaW5nKHZhbHVlKSB7XG4gIHJldHVybiBvYmpUb1N0cmluZy5jYWxsKHZhbHVlKSA9PT0gJ1tvYmplY3QgU3RyaW5nXSc7XG59XG4vKipcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG5mdW5jdGlvbiBpc09iamVjdCh2YWx1ZSkge1xuICAvLyBBdm9pZCBhIFY4IEpJVCBidWcgaW4gQ2hyb21lIDE5LTIwLlxuICAvLyBTZWUgaHR0cHM6Ly9jb2RlLmdvb2dsZS5jb20vcC92OC9pc3N1ZXMvZGV0YWlsP2lkPTIyOTEgZm9yIG1vcmUgZGV0YWlscy5cbiAgdmFyIHR5cGUgPSB0eXBlb2YgdmFsdWU7XG4gIHJldHVybiB0eXBlID09PSAnZnVuY3Rpb24nIHx8ICEhdmFsdWUgJiYgdHlwZSA9PT0gJ29iamVjdCc7XG59XG4vKipcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL3V0aWxcbiAqIEBwYXJhbSB7Kn0gdmFsdWVcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuXG5mdW5jdGlvbiBpc0J1aWx0SW5PYmplY3QodmFsdWUpIHtcbiAgcmV0dXJuICEhQlVJTFRJTl9PQkpFQ1Rbb2JqVG9TdHJpbmcuY2FsbCh2YWx1ZSldO1xufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0geyp9IHZhbHVlXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gaXNUeXBlZEFycmF5KHZhbHVlKSB7XG4gIHJldHVybiAhIVRZUEVEX0FSUkFZW29ialRvU3RyaW5nLmNhbGwodmFsdWUpXTtcbn1cbi8qKlxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHsqfSB2YWx1ZVxuICogQHJldHVybiB7Ym9vbGVhbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIGlzRG9tKHZhbHVlKSB7XG4gIHJldHVybiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHR5cGVvZiB2YWx1ZS5ub2RlVHlwZSA9PT0gJ251bWJlcicgJiYgdHlwZW9mIHZhbHVlLm93bmVyRG9jdW1lbnQgPT09ICdvYmplY3QnO1xufVxuLyoqXG4gKiBXaGV0aGVyIGlzIGV4YWN0bHkgTmFOLiBOb3RpY2UgaXNOYU4oJ2EnKSByZXR1cm5zIHRydWUuXG4gKiBAcGFyYW0geyp9IHZhbHVlXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gZXFOYU4odmFsdWUpIHtcbiAgcmV0dXJuIHZhbHVlICE9PSB2YWx1ZTtcbn1cbi8qKlxuICogSWYgdmFsdWUxIGlzIG5vdCBudWxsLCB0aGVuIHJldHVybiB2YWx1ZTEsIG90aGVyd2lzZSBqdWRnZXQgcmVzdCBvZiB2YWx1ZXMuXG4gKiBMb3cgcGVyZm9ybWFuY2UuXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcmV0dXJuIHsqfSBGaW5hbCB2YWx1ZVxuICovXG5cblxuZnVuY3Rpb24gcmV0cmlldmUodmFsdWVzKSB7XG4gIGZvciAodmFyIGkgPSAwLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICBpZiAoYXJndW1lbnRzW2ldICE9IG51bGwpIHtcbiAgICAgIHJldHVybiBhcmd1bWVudHNbaV07XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIHJldHJpZXZlMih2YWx1ZTAsIHZhbHVlMSkge1xuICByZXR1cm4gdmFsdWUwICE9IG51bGwgPyB2YWx1ZTAgOiB2YWx1ZTE7XG59XG5cbmZ1bmN0aW9uIHJldHJpZXZlMyh2YWx1ZTAsIHZhbHVlMSwgdmFsdWUyKSB7XG4gIHJldHVybiB2YWx1ZTAgIT0gbnVsbCA/IHZhbHVlMCA6IHZhbHVlMSAhPSBudWxsID8gdmFsdWUxIDogdmFsdWUyO1xufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge0FycmF5fSBhcnJcbiAqIEBwYXJhbSB7bnVtYmVyfSBzdGFydEluZGV4XG4gKiBAcGFyYW0ge251bWJlcn0gZW5kSW5kZXhcbiAqIEByZXR1cm4ge0FycmF5fVxuICovXG5cblxuZnVuY3Rpb24gc2xpY2UoKSB7XG4gIHJldHVybiBGdW5jdGlvbi5jYWxsLmFwcGx5KG5hdGl2ZVNsaWNlLCBhcmd1bWVudHMpO1xufVxuLyoqXG4gKiBOb3JtYWxpemUgY3NzIGxpa2VkIGFycmF5IGNvbmZpZ3VyYXRpb25cbiAqIGUuZy5cbiAqICAzID0+IFszLCAzLCAzLCAzXVxuICogIFs0LCAyXSA9PiBbNCwgMiwgNCwgMl1cbiAqICBbNCwgMywgMl0gPT4gWzQsIDMsIDIsIDNdXG4gKiBAcGFyYW0ge251bWJlcnxBcnJheS48bnVtYmVyPn0gdmFsXG4gKiBAcmV0dXJuIHtBcnJheS48bnVtYmVyPn1cbiAqL1xuXG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUNzc0FycmF5KHZhbCkge1xuICBpZiAodHlwZW9mIHZhbCA9PT0gJ251bWJlcicpIHtcbiAgICByZXR1cm4gW3ZhbCwgdmFsLCB2YWwsIHZhbF07XG4gIH1cblxuICB2YXIgbGVuID0gdmFsLmxlbmd0aDtcblxuICBpZiAobGVuID09PSAyKSB7XG4gICAgLy8gdmVydGljYWwgfCBob3Jpem9udGFsXG4gICAgcmV0dXJuIFt2YWxbMF0sIHZhbFsxXSwgdmFsWzBdLCB2YWxbMV1dO1xuICB9IGVsc2UgaWYgKGxlbiA9PT0gMykge1xuICAgIC8vIHRvcCB8IGhvcml6b250YWwgfCBib3R0b21cbiAgICByZXR1cm4gW3ZhbFswXSwgdmFsWzFdLCB2YWxbMl0sIHZhbFsxXV07XG4gIH1cblxuICByZXR1cm4gdmFsO1xufVxuLyoqXG4gKiBAbWVtYmVyT2YgbW9kdWxlOnpyZW5kZXIvY29yZS91dGlsXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGNvbmRpdGlvblxuICogQHBhcmFtIHtzdHJpbmd9IG1lc3NhZ2VcbiAqL1xuXG5cbmZ1bmN0aW9uIGFzc2VydChjb25kaXRpb24sIG1lc3NhZ2UpIHtcbiAgaWYgKCFjb25kaXRpb24pIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gIH1cbn1cbi8qKlxuICogQG1lbWJlck9mIG1vZHVsZTp6cmVuZGVyL2NvcmUvdXRpbFxuICogQHBhcmFtIHtzdHJpbmd9IHN0ciBzdHJpbmcgdG8gYmUgdHJpbWVkXG4gKiBAcmV0dXJuIHtzdHJpbmd9IHRyaW1lZCBzdHJpbmdcbiAqL1xuXG5cbmZ1bmN0aW9uIHRyaW0oc3RyKSB7XG4gIGlmIChzdHIgPT0gbnVsbCkge1xuICAgIHJldHVybiBudWxsO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBzdHIudHJpbSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiBzdHIudHJpbSgpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBzdHIucmVwbGFjZSgvXltcXHNcXHVGRUZGXFx4QTBdK3xbXFxzXFx1RkVGRlxceEEwXSskL2csICcnKTtcbiAgfVxufVxuXG52YXIgcHJpbWl0aXZlS2V5ID0gJ19fZWNfcHJpbWl0aXZlX18nO1xuLyoqXG4gKiBTZXQgYW4gb2JqZWN0IGFzIHByaW1pdGl2ZSB0byBiZSBpZ25vcmVkIHRyYXZlcnNpbmcgY2hpbGRyZW4gaW4gY2xvbmUgb3IgbWVyZ2VcbiAqL1xuXG5mdW5jdGlvbiBzZXRBc1ByaW1pdGl2ZShvYmopIHtcbiAgb2JqW3ByaW1pdGl2ZUtleV0gPSB0cnVlO1xufVxuXG5mdW5jdGlvbiBpc1ByaW1pdGl2ZShvYmopIHtcbiAgcmV0dXJuIG9ialtwcmltaXRpdmVLZXldO1xufVxuLyoqXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmogT25seSBhcHBseSBgb3duUHJvcGVydHlgLlxuICovXG5cblxuZnVuY3Rpb24gSGFzaE1hcChvYmopIHtcbiAgdmFyIGlzQXJyID0gaXNBcnJheShvYmopOyAvLyBLZXkgc2hvdWxkIG5vdCBiZSBzZXQgb24gdGhpcywgb3RoZXJ3aXNlXG4gIC8vIG1ldGhvZHMgZ2V0L3NldC8uLi4gbWF5IGJlIG92ZXJyaWRlZC5cblxuICB0aGlzLmRhdGEgPSB7fTtcbiAgdmFyIHRoaXNNYXAgPSB0aGlzO1xuICBvYmogaW5zdGFuY2VvZiBIYXNoTWFwID8gb2JqLmVhY2godmlzaXQpIDogb2JqICYmIGVhY2gob2JqLCB2aXNpdCk7XG5cbiAgZnVuY3Rpb24gdmlzaXQodmFsdWUsIGtleSkge1xuICAgIGlzQXJyID8gdGhpc01hcC5zZXQodmFsdWUsIGtleSkgOiB0aGlzTWFwLnNldChrZXksIHZhbHVlKTtcbiAgfVxufVxuXG5IYXNoTWFwLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IEhhc2hNYXAsXG4gIC8vIERvIG5vdCBwcm92aWRlIGBoYXNgIG1ldGhvZCB0byBhdm9pZCBkZWZpbmluZyB3aGF0IGlzIGBoYXNgLlxuICAvLyAoV2UgdXN1YWxseSB0cmVhdCBgbnVsbGAgYW5kIGB1bmRlZmluZWRgIGFzIHRoZSBzYW1lLCBkaWZmZXJlbnRcbiAgLy8gZnJvbSBFUzYgTWFwKS5cbiAgZ2V0OiBmdW5jdGlvbiAoa2V5KSB7XG4gICAgcmV0dXJuIHRoaXMuZGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpID8gdGhpcy5kYXRhW2tleV0gOiBudWxsO1xuICB9LFxuICBzZXQ6IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gICAgLy8gQ29tcGFyaW5nIHdpdGggaW52b2NhdGlvbiBjaGFpbmluZywgYHJldHVybiB2YWx1ZWAgaXMgbW9yZSBjb21tb25seVxuICAgIC8vIHVzZWQgaW4gdGhpcyBjYXNlOiBgdmFyIHNvbWVWYWwgPSBtYXAuc2V0KCdhJywgZ2VuVmFsKCkpO2BcbiAgICByZXR1cm4gdGhpcy5kYXRhW2tleV0gPSB2YWx1ZTtcbiAgfSxcbiAgLy8gQWx0aG91Z2ggdXRpbC5lYWNoIGNhbiBiZSBwZXJmb3JtZWQgb24gdGhpcyBoYXNoTWFwIGRpcmVjdGx5LCB1c2VyXG4gIC8vIHNob3VsZCBub3QgdXNlIHRoZSBleHBvc2VkIGtleXMsIHdobyBhcmUgcHJlZml4ZWQuXG4gIGVhY2g6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIGNvbnRleHQgIT09IHZvaWQgMCAmJiAoY2IgPSBiaW5kKGNiLCBjb250ZXh0KSk7XG5cbiAgICBmb3IgKHZhciBrZXkgaW4gdGhpcy5kYXRhKSB7XG4gICAgICB0aGlzLmRhdGEuaGFzT3duUHJvcGVydHkoa2V5KSAmJiBjYih0aGlzLmRhdGFba2V5XSwga2V5KTtcbiAgICB9XG4gIH0sXG4gIC8vIERvIG5vdCB1c2UgdGhpcyBtZXRob2QgaWYgcGVyZm9ybWFuY2Ugc2Vuc2l0aXZlLlxuICByZW1vdmVLZXk6IGZ1bmN0aW9uIChrZXkpIHtcbiAgICBkZWxldGUgdGhpcy5kYXRhW2tleV07XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGNyZWF0ZUhhc2hNYXAob2JqKSB7XG4gIHJldHVybiBuZXcgSGFzaE1hcChvYmopO1xufVxuXG5mdW5jdGlvbiBjb25jYXRBcnJheShhLCBiKSB7XG4gIHZhciBuZXdBcnJheSA9IG5ldyBhLmNvbnN0cnVjdG9yKGEubGVuZ3RoICsgYi5sZW5ndGgpO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgYS5sZW5ndGg7IGkrKykge1xuICAgIG5ld0FycmF5W2ldID0gYVtpXTtcbiAgfVxuXG4gIHZhciBvZmZzZXQgPSBhLmxlbmd0aDtcblxuICBmb3IgKGkgPSAwOyBpIDwgYi5sZW5ndGg7IGkrKykge1xuICAgIG5ld0FycmF5W2kgKyBvZmZzZXRdID0gYltpXTtcbiAgfVxuXG4gIHJldHVybiBuZXdBcnJheTtcbn1cblxuZnVuY3Rpb24gbm9vcCgpIHt9XG5cbmV4cG9ydHMuJG92ZXJyaWRlID0gJG92ZXJyaWRlO1xuZXhwb3J0cy5jbG9uZSA9IGNsb25lO1xuZXhwb3J0cy5tZXJnZSA9IG1lcmdlO1xuZXhwb3J0cy5tZXJnZUFsbCA9IG1lcmdlQWxsO1xuZXhwb3J0cy5leHRlbmQgPSBleHRlbmQ7XG5leHBvcnRzLmRlZmF1bHRzID0gZGVmYXVsdHM7XG5leHBvcnRzLmNyZWF0ZUNhbnZhcyA9IGNyZWF0ZUNhbnZhcztcbmV4cG9ydHMuZ2V0Q29udGV4dCA9IGdldENvbnRleHQ7XG5leHBvcnRzLmluZGV4T2YgPSBpbmRleE9mO1xuZXhwb3J0cy5pbmhlcml0cyA9IGluaGVyaXRzO1xuZXhwb3J0cy5taXhpbiA9IG1peGluO1xuZXhwb3J0cy5pc0FycmF5TGlrZSA9IGlzQXJyYXlMaWtlO1xuZXhwb3J0cy5lYWNoID0gZWFjaDtcbmV4cG9ydHMubWFwID0gbWFwO1xuZXhwb3J0cy5yZWR1Y2UgPSByZWR1Y2U7XG5leHBvcnRzLmZpbHRlciA9IGZpbHRlcjtcbmV4cG9ydHMuZmluZCA9IGZpbmQ7XG5leHBvcnRzLmJpbmQgPSBiaW5kO1xuZXhwb3J0cy5jdXJyeSA9IGN1cnJ5O1xuZXhwb3J0cy5pc0FycmF5ID0gaXNBcnJheTtcbmV4cG9ydHMuaXNGdW5jdGlvbiA9IGlzRnVuY3Rpb247XG5leHBvcnRzLmlzU3RyaW5nID0gaXNTdHJpbmc7XG5leHBvcnRzLmlzT2JqZWN0ID0gaXNPYmplY3Q7XG5leHBvcnRzLmlzQnVpbHRJbk9iamVjdCA9IGlzQnVpbHRJbk9iamVjdDtcbmV4cG9ydHMuaXNUeXBlZEFycmF5ID0gaXNUeXBlZEFycmF5O1xuZXhwb3J0cy5pc0RvbSA9IGlzRG9tO1xuZXhwb3J0cy5lcU5hTiA9IGVxTmFOO1xuZXhwb3J0cy5yZXRyaWV2ZSA9IHJldHJpZXZlO1xuZXhwb3J0cy5yZXRyaWV2ZTIgPSByZXRyaWV2ZTI7XG5leHBvcnRzLnJldHJpZXZlMyA9IHJldHJpZXZlMztcbmV4cG9ydHMuc2xpY2UgPSBzbGljZTtcbmV4cG9ydHMubm9ybWFsaXplQ3NzQXJyYXkgPSBub3JtYWxpemVDc3NBcnJheTtcbmV4cG9ydHMuYXNzZXJ0ID0gYXNzZXJ0O1xuZXhwb3J0cy50cmltID0gdHJpbTtcbmV4cG9ydHMuc2V0QXNQcmltaXRpdmUgPSBzZXRBc1ByaW1pdGl2ZTtcbmV4cG9ydHMuaXNQcmltaXRpdmUgPSBpc1ByaW1pdGl2ZTtcbmV4cG9ydHMuY3JlYXRlSGFzaE1hcCA9IGNyZWF0ZUhhc2hNYXA7XG5leHBvcnRzLmNvbmNhdEFycmF5ID0gY29uY2F0QXJyYXk7XG5leHBvcnRzLm5vb3AgPSBub29wOyJdLCJtYXBwaW5ncyI6IkFBQUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXpCQTtBQUNBO0FBMkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/util.js
