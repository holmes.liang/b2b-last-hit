/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 * 
 */

/**
 * Constants to represent text directionality
 *
 * Also defines a *global* direciton, to be used in bidi algorithms as a
 * default fallback direciton, when no better direction is found or provided.
 *
 * NOTE: Use `setGlobalDir()`, or update `initGlobalDir()`, to set the initial
 *       global direction value based on the application.
 *
 * Part of the implementation of Unicode Bidirectional Algorithm (UBA)
 * Unicode Standard Annex #9 (UAX9)
 * http://www.unicode.org/reports/tr9/
 */


var invariant = __webpack_require__(/*! ./invariant */ "./node_modules/fbjs/lib/invariant.js");

var NEUTRAL = 'NEUTRAL'; // No strong direction

var LTR = 'LTR'; // Left-to-Right direction

var RTL = 'RTL'; // Right-to-Left direction

var globalDir = null; // == Helpers ==

/**
 * Check if a directionality value is a Strong one
 */

function isStrong(dir) {
  return dir === LTR || dir === RTL;
}
/**
 * Get string value to be used for `dir` HTML attribute or `direction` CSS
 * property.
 */


function getHTMLDir(dir) {
  !isStrong(dir) ?  true ? invariant(false, '`dir` must be a strong direction to be converted to HTML Direction') : undefined : void 0;
  return dir === LTR ? 'ltr' : 'rtl';
}
/**
 * Get string value to be used for `dir` HTML attribute or `direction` CSS
 * property, but returns null if `dir` has same value as `otherDir`.
 * `null`.
 */


function getHTMLDirIfDifferent(dir, otherDir) {
  !isStrong(dir) ?  true ? invariant(false, '`dir` must be a strong direction to be converted to HTML Direction') : undefined : void 0;
  !isStrong(otherDir) ?  true ? invariant(false, '`otherDir` must be a strong direction to be converted to HTML Direction') : undefined : void 0;
  return dir === otherDir ? null : getHTMLDir(dir);
} // == Global Direction ==

/**
 * Set the global direction.
 */


function setGlobalDir(dir) {
  globalDir = dir;
}
/**
 * Initialize the global direction
 */


function initGlobalDir() {
  setGlobalDir(LTR);
}
/**
 * Get the global direction
 */


function getGlobalDir() {
  if (!globalDir) {
    this.initGlobalDir();
  }

  !globalDir ?  true ? invariant(false, 'Global direction not set.') : undefined : void 0;
  return globalDir;
}

var UnicodeBidiDirection = {
  // Values
  NEUTRAL: NEUTRAL,
  LTR: LTR,
  RTL: RTL,
  // Helpers
  isStrong: isStrong,
  getHTMLDir: getHTMLDir,
  getHTMLDirIfDifferent: getHTMLDirIfDifferent,
  // Global Direction
  setGlobalDir: setGlobalDir,
  initGlobalDir: initGlobalDir,
  getGlobalDir: getGlobalDir
};
module.exports = UnicodeBidiDirection;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVW5pY29kZUJpZGlEaXJlY3Rpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9mYmpzL2xpYi9Vbmljb2RlQmlkaURpcmVjdGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICpcbiAqIEB0eXBlY2hlY2tzXG4gKiBcbiAqL1xuXG4vKipcbiAqIENvbnN0YW50cyB0byByZXByZXNlbnQgdGV4dCBkaXJlY3Rpb25hbGl0eVxuICpcbiAqIEFsc28gZGVmaW5lcyBhICpnbG9iYWwqIGRpcmVjaXRvbiwgdG8gYmUgdXNlZCBpbiBiaWRpIGFsZ29yaXRobXMgYXMgYVxuICogZGVmYXVsdCBmYWxsYmFjayBkaXJlY2l0b24sIHdoZW4gbm8gYmV0dGVyIGRpcmVjdGlvbiBpcyBmb3VuZCBvciBwcm92aWRlZC5cbiAqXG4gKiBOT1RFOiBVc2UgYHNldEdsb2JhbERpcigpYCwgb3IgdXBkYXRlIGBpbml0R2xvYmFsRGlyKClgLCB0byBzZXQgdGhlIGluaXRpYWxcbiAqICAgICAgIGdsb2JhbCBkaXJlY3Rpb24gdmFsdWUgYmFzZWQgb24gdGhlIGFwcGxpY2F0aW9uLlxuICpcbiAqIFBhcnQgb2YgdGhlIGltcGxlbWVudGF0aW9uIG9mIFVuaWNvZGUgQmlkaXJlY3Rpb25hbCBBbGdvcml0aG0gKFVCQSlcbiAqIFVuaWNvZGUgU3RhbmRhcmQgQW5uZXggIzkgKFVBWDkpXG4gKiBodHRwOi8vd3d3LnVuaWNvZGUub3JnL3JlcG9ydHMvdHI5L1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJy4vaW52YXJpYW50Jyk7XG5cbnZhciBORVVUUkFMID0gJ05FVVRSQUwnOyAvLyBObyBzdHJvbmcgZGlyZWN0aW9uXG52YXIgTFRSID0gJ0xUUic7IC8vIExlZnQtdG8tUmlnaHQgZGlyZWN0aW9uXG52YXIgUlRMID0gJ1JUTCc7IC8vIFJpZ2h0LXRvLUxlZnQgZGlyZWN0aW9uXG5cbnZhciBnbG9iYWxEaXIgPSBudWxsO1xuXG4vLyA9PSBIZWxwZXJzID09XG5cbi8qKlxuICogQ2hlY2sgaWYgYSBkaXJlY3Rpb25hbGl0eSB2YWx1ZSBpcyBhIFN0cm9uZyBvbmVcbiAqL1xuZnVuY3Rpb24gaXNTdHJvbmcoZGlyKSB7XG4gIHJldHVybiBkaXIgPT09IExUUiB8fCBkaXIgPT09IFJUTDtcbn1cblxuLyoqXG4gKiBHZXQgc3RyaW5nIHZhbHVlIHRvIGJlIHVzZWQgZm9yIGBkaXJgIEhUTUwgYXR0cmlidXRlIG9yIGBkaXJlY3Rpb25gIENTU1xuICogcHJvcGVydHkuXG4gKi9cbmZ1bmN0aW9uIGdldEhUTUxEaXIoZGlyKSB7XG4gICFpc1N0cm9uZyhkaXIpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2BkaXJgIG11c3QgYmUgYSBzdHJvbmcgZGlyZWN0aW9uIHRvIGJlIGNvbnZlcnRlZCB0byBIVE1MIERpcmVjdGlvbicpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcbiAgcmV0dXJuIGRpciA9PT0gTFRSID8gJ2x0cicgOiAncnRsJztcbn1cblxuLyoqXG4gKiBHZXQgc3RyaW5nIHZhbHVlIHRvIGJlIHVzZWQgZm9yIGBkaXJgIEhUTUwgYXR0cmlidXRlIG9yIGBkaXJlY3Rpb25gIENTU1xuICogcHJvcGVydHksIGJ1dCByZXR1cm5zIG51bGwgaWYgYGRpcmAgaGFzIHNhbWUgdmFsdWUgYXMgYG90aGVyRGlyYC5cbiAqIGBudWxsYC5cbiAqL1xuZnVuY3Rpb24gZ2V0SFRNTERpcklmRGlmZmVyZW50KGRpciwgb3RoZXJEaXIpIHtcbiAgIWlzU3Ryb25nKGRpcikgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnYGRpcmAgbXVzdCBiZSBhIHN0cm9uZyBkaXJlY3Rpb24gdG8gYmUgY29udmVydGVkIHRvIEhUTUwgRGlyZWN0aW9uJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAhaXNTdHJvbmcob3RoZXJEaXIpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2BvdGhlckRpcmAgbXVzdCBiZSBhIHN0cm9uZyBkaXJlY3Rpb24gdG8gYmUgY29udmVydGVkIHRvIEhUTUwgRGlyZWN0aW9uJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICByZXR1cm4gZGlyID09PSBvdGhlckRpciA/IG51bGwgOiBnZXRIVE1MRGlyKGRpcik7XG59XG5cbi8vID09IEdsb2JhbCBEaXJlY3Rpb24gPT1cblxuLyoqXG4gKiBTZXQgdGhlIGdsb2JhbCBkaXJlY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIHNldEdsb2JhbERpcihkaXIpIHtcbiAgZ2xvYmFsRGlyID0gZGlyO1xufVxuXG4vKipcbiAqIEluaXRpYWxpemUgdGhlIGdsb2JhbCBkaXJlY3Rpb25cbiAqL1xuZnVuY3Rpb24gaW5pdEdsb2JhbERpcigpIHtcbiAgc2V0R2xvYmFsRGlyKExUUik7XG59XG5cbi8qKlxuICogR2V0IHRoZSBnbG9iYWwgZGlyZWN0aW9uXG4gKi9cbmZ1bmN0aW9uIGdldEdsb2JhbERpcigpIHtcbiAgaWYgKCFnbG9iYWxEaXIpIHtcbiAgICB0aGlzLmluaXRHbG9iYWxEaXIoKTtcbiAgfVxuICAhZ2xvYmFsRGlyID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0dsb2JhbCBkaXJlY3Rpb24gbm90IHNldC4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gIHJldHVybiBnbG9iYWxEaXI7XG59XG5cbnZhciBVbmljb2RlQmlkaURpcmVjdGlvbiA9IHtcbiAgLy8gVmFsdWVzXG4gIE5FVVRSQUw6IE5FVVRSQUwsXG4gIExUUjogTFRSLFxuICBSVEw6IFJUTCxcbiAgLy8gSGVscGVyc1xuICBpc1N0cm9uZzogaXNTdHJvbmcsXG4gIGdldEhUTUxEaXI6IGdldEhUTUxEaXIsXG4gIGdldEhUTUxEaXJJZkRpZmZlcmVudDogZ2V0SFRNTERpcklmRGlmZmVyZW50LFxuICAvLyBHbG9iYWwgRGlyZWN0aW9uXG4gIHNldEdsb2JhbERpcjogc2V0R2xvYmFsRGlyLFxuICBpbml0R2xvYmFsRGlyOiBpbml0R2xvYmFsRGlyLFxuICBnZXRHbG9iYWxEaXI6IGdldEdsb2JhbERpclxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBVbmljb2RlQmlkaURpcmVjdGlvbjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7O0FBVUE7Ozs7Ozs7Ozs7Ozs7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTs7OztBQUdBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWVBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/UnicodeBidiDirection.js
