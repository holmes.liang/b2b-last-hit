__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app */ "./src/app/index.tsx");
var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/index.tsx";



// add polyfill
if (!Array.prototype.flat) {
  Array.prototype.flat = function () {
    var depth = arguments[0];
    depth = depth === undefined ? 1 : Math.floor(depth);
    if (depth < 1) return Array.prototype.slice.call(this);
    return function flat(arr, depth) {
      var len = arr.length >>> 0;
      var flattened = [];
      var i = 0;

      while (i < len) {
        if (i in arr) {
          var el = arr[i];
          if (Array.isArray(el) && depth > 0) flattened = flattened.concat(flat(el, depth - 1));else flattened.push(el);
        }

        i++;
      }

      return flattened;
    }(this, depth);
  };
}

console.time("Render whole application.");
_common_3rd__WEBPACK_IMPORTED_MODULE_0__["ReactDOM"].render(_common_3rd__WEBPACK_IMPORTED_MODULE_0__["React"].createElement(_app__WEBPACK_IMPORTED_MODULE_1__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 35
  },
  __self: undefined
}), document.getElementById("root"), function () {
  console.log("App start on ".concat("Local"));
  console.timeEnd("Render whole application.");
}); // registerServiceWorker();//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBSZWFjdERPTSB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IEFwcCBmcm9tIFwiLi9hcHBcIjtcbmltcG9ydCByZWdpc3RlclNlcnZpY2VXb3JrZXIgZnJvbSAnLi9yZWdpc3RlclNlcnZpY2VXb3JrZXInO1xuXG5kZWNsYXJlIGdsb2JhbCB7XG4gIGludGVyZmFjZSBXaW5kb3cge1xuICAgIF9fd3hqc19lbnZpcm9ubWVudD86IHN0cmluZztcbiAgfVxufVxuXG4vLyBhZGQgcG9seWZpbGxcbmlmICghQXJyYXkucHJvdG90eXBlLmZsYXQpIHtcbiAgQXJyYXkucHJvdG90eXBlLmZsYXQgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgZGVwdGggPSBhcmd1bWVudHNbMF07XG4gICAgZGVwdGggPSBkZXB0aCA9PT0gdW5kZWZpbmVkID8gMSA6IE1hdGguZmxvb3IoZGVwdGgpO1xuICAgIGlmIChkZXB0aCA8IDEpIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzKTtcbiAgICByZXR1cm4gKGZ1bmN0aW9uIGZsYXQoYXJyLCBkZXB0aCkge1xuICAgICAgdmFyIGxlbiA9IGFyci5sZW5ndGggPj4+IDA7XG4gICAgICB2YXIgZmxhdHRlbmVkID0gW10gYXMgYW55W107XG4gICAgICB2YXIgaSA9IDA7XG4gICAgICB3aGlsZSAoaSA8IGxlbikge1xuICAgICAgICBpZiAoaSBpbiBhcnIpIHtcbiAgICAgICAgICB2YXIgZWwgPSBhcnJbaV07XG4gICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZWwpICYmIGRlcHRoID4gMCkgZmxhdHRlbmVkID0gZmxhdHRlbmVkLmNvbmNhdChmbGF0KGVsLCBkZXB0aCAtIDEpKTtcbiAgICAgICAgICBlbHNlIGZsYXR0ZW5lZC5wdXNoKGVsKTtcbiAgICAgICAgfVxuICAgICAgICBpKys7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmxhdHRlbmVkO1xuICAgIH0pKHRoaXMsIGRlcHRoKTtcbiAgfTtcbn1cblxuY29uc29sZS50aW1lKFwiUmVuZGVyIHdob2xlIGFwcGxpY2F0aW9uLlwiKTtcblJlYWN0RE9NLnJlbmRlcig8QXBwLz4sIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicm9vdFwiKSwgKCkgPT4ge1xuICBjb25zb2xlLmxvZyhgQXBwIHN0YXJ0IG9uICR7cHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0VOVl9OQU1FfWApO1xuICBjb25zb2xlLnRpbWVFbmQoXCJSZW5kZXIgd2hvbGUgYXBwbGljYXRpb24uXCIpO1xufSk7XG5cbi8vIHJlZ2lzdGVyU2VydmljZVdvcmtlcigpO1xuIl0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7QUFDQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/index.tsx
