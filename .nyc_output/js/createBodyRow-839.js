__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return createBodyRow; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}




function createBodyRow() {
  var Component = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'tr';

  var BodyRow =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(BodyRow, _React$Component);

    function BodyRow(props) {
      var _this;

      _classCallCheck(this, BodyRow);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(BodyRow).call(this, props));
      _this.store = props.store;

      var _this$store$getState = _this.store.getState(),
          selectedRowKeys = _this$store$getState.selectedRowKeys;

      _this.state = {
        selected: selectedRowKeys.indexOf(props.rowKey) >= 0
      };
      return _this;
    }

    _createClass(BodyRow, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        this.subscribe();
      }
    }, {
      key: "componentWillUnmount",
      value: function componentWillUnmount() {
        if (this.unsubscribe) {
          this.unsubscribe();
        }
      }
    }, {
      key: "subscribe",
      value: function subscribe() {
        var _this2 = this;

        var _this$props = this.props,
            store = _this$props.store,
            rowKey = _this$props.rowKey;
        this.unsubscribe = store.subscribe(function () {
          var _this2$store$getState = _this2.store.getState(),
              selectedRowKeys = _this2$store$getState.selectedRowKeys;

          var selected = selectedRowKeys.indexOf(rowKey) >= 0;

          if (selected !== _this2.state.selected) {
            _this2.setState({
              selected: selected
            });
          }
        });
      }
    }, {
      key: "render",
      value: function render() {
        var rowProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(this.props, ['prefixCls', 'rowKey', 'store']);
        var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()(this.props.className, _defineProperty({}, "".concat(this.props.prefixCls, "-row-selected"), this.state.selected));
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Component, _extends(_extends({}, rowProps), {
          className: className
        }), this.props.children);
      }
    }]);

    return BodyRow;
  }(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

  return BodyRow;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90YWJsZS9jcmVhdGVCb2R5Um93LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90YWJsZS9jcmVhdGVCb2R5Um93LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY3JlYXRlQm9keVJvdyhDb21wb25lbnQgPSAndHInKSB7XG4gICAgY2xhc3MgQm9keVJvdyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgICAgICB0aGlzLnN0b3JlID0gcHJvcHMuc3RvcmU7XG4gICAgICAgICAgICBjb25zdCB7IHNlbGVjdGVkUm93S2V5cyB9ID0gdGhpcy5zdG9yZS5nZXRTdGF0ZSgpO1xuICAgICAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogc2VsZWN0ZWRSb3dLZXlzLmluZGV4T2YocHJvcHMucm93S2V5KSA+PSAwLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgICAgIHRoaXMuc3Vic2NyaWJlKCk7XG4gICAgICAgIH1cbiAgICAgICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy51bnN1YnNjcmliZSkge1xuICAgICAgICAgICAgICAgIHRoaXMudW5zdWJzY3JpYmUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBzdWJzY3JpYmUoKSB7XG4gICAgICAgICAgICBjb25zdCB7IHN0b3JlLCByb3dLZXkgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICB0aGlzLnVuc3Vic2NyaWJlID0gc3RvcmUuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCB7IHNlbGVjdGVkUm93S2V5cyB9ID0gdGhpcy5zdG9yZS5nZXRTdGF0ZSgpO1xuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkID0gc2VsZWN0ZWRSb3dLZXlzLmluZGV4T2Yocm93S2V5KSA+PSAwO1xuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZCAhPT0gdGhpcy5zdGF0ZS5zZWxlY3RlZCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgc2VsZWN0ZWQgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmVuZGVyKCkge1xuICAgICAgICAgICAgY29uc3Qgcm93UHJvcHMgPSBvbWl0KHRoaXMucHJvcHMsIFsncHJlZml4Q2xzJywgJ3Jvd0tleScsICdzdG9yZSddKTtcbiAgICAgICAgICAgIGNvbnN0IGNsYXNzTmFtZSA9IGNsYXNzbmFtZXModGhpcy5wcm9wcy5jbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7dGhpcy5wcm9wcy5wcmVmaXhDbHN9LXJvdy1zZWxlY3RlZGBdOiB0aGlzLnN0YXRlLnNlbGVjdGVkLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgcm93UHJvcHMpLCB7IGNsYXNzTmFtZSB9KSwgdGhpcy5wcm9wcy5jaGlsZHJlbik7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIEJvZHlSb3c7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBSkE7QUFPQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBQUE7QUFBQTtBQWtCQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUxBO0FBT0E7QUEzQkE7QUFBQTtBQUFBO0FBNkJBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQWxDQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBbUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/table/createBodyRow.js
