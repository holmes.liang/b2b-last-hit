

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */


var PhotosMimeType = __webpack_require__(/*! ./PhotosMimeType */ "./node_modules/fbjs/lib/PhotosMimeType.js");

var createArrayFromMixed = __webpack_require__(/*! ./createArrayFromMixed */ "./node_modules/fbjs/lib/createArrayFromMixed.js");

var emptyFunction = __webpack_require__(/*! ./emptyFunction */ "./node_modules/fbjs/lib/emptyFunction.js");

var CR_LF_REGEX = new RegExp('\r\n', 'g');
var LF_ONLY = '\n';
var RICH_TEXT_TYPES = {
  'text/rtf': 1,
  'text/html': 1
};
/**
 * If DataTransferItem is a file then return the Blob of data.
 *
 * @param {object} item
 * @return {?blob}
 */

function getFileFromDataTransfer(item) {
  if (item.kind == 'file') {
    return item.getAsFile();
  }
}

var DataTransfer = function () {
  /**
   * @param {object} data
   */
  function DataTransfer(data) {
    _classCallCheck(this, DataTransfer);

    this.data = data; // Types could be DOMStringList or array

    this.types = data.types ? createArrayFromMixed(data.types) : [];
  }
  /**
   * Is this likely to be a rich text data transfer?
   *
   * @return {boolean}
   */


  DataTransfer.prototype.isRichText = function isRichText() {
    // If HTML is available, treat this data as rich text. This way, we avoid
    // using a pasted image if it is packaged with HTML -- this may occur with
    // pastes from MS Word, for example.  However this is only rich text if
    // there's accompanying text.
    if (this.getHTML() && this.getText()) {
      return true;
    } // When an image is copied from a preview window, you end up with two
    // DataTransferItems one of which is a file's metadata as text.  Skip those.


    if (this.isImage()) {
      return false;
    }

    return this.types.some(function (type) {
      return RICH_TEXT_TYPES[type];
    });
  };
  /**
   * Get raw text.
   *
   * @return {?string}
   */


  DataTransfer.prototype.getText = function getText() {
    var text;

    if (this.data.getData) {
      if (!this.types.length) {
        text = this.data.getData('Text');
      } else if (this.types.indexOf('text/plain') != -1) {
        text = this.data.getData('text/plain');
      }
    }

    return text ? text.replace(CR_LF_REGEX, LF_ONLY) : null;
  };
  /**
   * Get HTML paste data
   *
   * @return {?string}
   */


  DataTransfer.prototype.getHTML = function getHTML() {
    if (this.data.getData) {
      if (!this.types.length) {
        return this.data.getData('Text');
      } else if (this.types.indexOf('text/html') != -1) {
        return this.data.getData('text/html');
      }
    }
  };
  /**
   * Is this a link data transfer?
   *
   * @return {boolean}
   */


  DataTransfer.prototype.isLink = function isLink() {
    return this.types.some(function (type) {
      return type.indexOf('Url') != -1 || type.indexOf('text/uri-list') != -1 || type.indexOf('text/x-moz-url');
    });
  };
  /**
   * Get a link url.
   *
   * @return {?string}
   */


  DataTransfer.prototype.getLink = function getLink() {
    if (this.data.getData) {
      if (this.types.indexOf('text/x-moz-url') != -1) {
        var url = this.data.getData('text/x-moz-url').split('\n');
        return url[0];
      }

      return this.types.indexOf('text/uri-list') != -1 ? this.data.getData('text/uri-list') : this.data.getData('url');
    }

    return null;
  };
  /**
   * Is this an image data transfer?
   *
   * @return {boolean}
   */


  DataTransfer.prototype.isImage = function isImage() {
    var isImage = this.types.some(function (type) {
      // Firefox will have a type of application/x-moz-file for images during
      // dragging
      return type.indexOf('application/x-moz-file') != -1;
    });

    if (isImage) {
      return true;
    }

    var items = this.getFiles();

    for (var i = 0; i < items.length; i++) {
      var type = items[i].type;

      if (!PhotosMimeType.isImage(type)) {
        return false;
      }
    }

    return true;
  };

  DataTransfer.prototype.getCount = function getCount() {
    if (this.data.hasOwnProperty('items')) {
      return this.data.items.length;
    } else if (this.data.hasOwnProperty('mozItemCount')) {
      return this.data.mozItemCount;
    } else if (this.data.files) {
      return this.data.files.length;
    }

    return null;
  };
  /**
   * Get files.
   *
   * @return {array}
   */


  DataTransfer.prototype.getFiles = function getFiles() {
    if (this.data.items) {
      // createArrayFromMixed doesn't properly handle DataTransferItemLists.
      return Array.prototype.slice.call(this.data.items).map(getFileFromDataTransfer).filter(emptyFunction.thatReturnsArgument);
    } else if (this.data.files) {
      return Array.prototype.slice.call(this.data.files);
    } else {
      return [];
    }
  };
  /**
   * Are there any files to fetch?
   *
   * @return {boolean}
   */


  DataTransfer.prototype.hasFiles = function hasFiles() {
    return this.getFiles().length > 0;
  };

  return DataTransfer;
}();

module.exports = DataTransfer;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvRGF0YVRyYW5zZmVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvRGF0YVRyYW5zZmVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKiBAdHlwZWNoZWNrc1xuICovXG5cbnZhciBQaG90b3NNaW1lVHlwZSA9IHJlcXVpcmUoJy4vUGhvdG9zTWltZVR5cGUnKTtcblxudmFyIGNyZWF0ZUFycmF5RnJvbU1peGVkID0gcmVxdWlyZSgnLi9jcmVhdGVBcnJheUZyb21NaXhlZCcpO1xudmFyIGVtcHR5RnVuY3Rpb24gPSByZXF1aXJlKCcuL2VtcHR5RnVuY3Rpb24nKTtcblxudmFyIENSX0xGX1JFR0VYID0gbmV3IFJlZ0V4cCgnXFxyXFxuJywgJ2cnKTtcbnZhciBMRl9PTkxZID0gJ1xcbic7XG5cbnZhciBSSUNIX1RFWFRfVFlQRVMgPSB7XG4gICd0ZXh0L3J0Zic6IDEsXG4gICd0ZXh0L2h0bWwnOiAxXG59O1xuXG4vKipcbiAqIElmIERhdGFUcmFuc2Zlckl0ZW0gaXMgYSBmaWxlIHRoZW4gcmV0dXJuIHRoZSBCbG9iIG9mIGRhdGEuXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGl0ZW1cbiAqIEByZXR1cm4gez9ibG9ifVxuICovXG5mdW5jdGlvbiBnZXRGaWxlRnJvbURhdGFUcmFuc2ZlcihpdGVtKSB7XG4gIGlmIChpdGVtLmtpbmQgPT0gJ2ZpbGUnKSB7XG4gICAgcmV0dXJuIGl0ZW0uZ2V0QXNGaWxlKCk7XG4gIH1cbn1cblxudmFyIERhdGFUcmFuc2ZlciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBkYXRhXG4gICAqL1xuICBmdW5jdGlvbiBEYXRhVHJhbnNmZXIoZGF0YSkge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEYXRhVHJhbnNmZXIpO1xuXG4gICAgdGhpcy5kYXRhID0gZGF0YTtcblxuICAgIC8vIFR5cGVzIGNvdWxkIGJlIERPTVN0cmluZ0xpc3Qgb3IgYXJyYXlcbiAgICB0aGlzLnR5cGVzID0gZGF0YS50eXBlcyA/IGNyZWF0ZUFycmF5RnJvbU1peGVkKGRhdGEudHlwZXMpIDogW107XG4gIH1cblxuICAvKipcbiAgICogSXMgdGhpcyBsaWtlbHkgdG8gYmUgYSByaWNoIHRleHQgZGF0YSB0cmFuc2Zlcj9cbiAgICpcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG5cblxuICBEYXRhVHJhbnNmZXIucHJvdG90eXBlLmlzUmljaFRleHQgPSBmdW5jdGlvbiBpc1JpY2hUZXh0KCkge1xuICAgIC8vIElmIEhUTUwgaXMgYXZhaWxhYmxlLCB0cmVhdCB0aGlzIGRhdGEgYXMgcmljaCB0ZXh0LiBUaGlzIHdheSwgd2UgYXZvaWRcbiAgICAvLyB1c2luZyBhIHBhc3RlZCBpbWFnZSBpZiBpdCBpcyBwYWNrYWdlZCB3aXRoIEhUTUwgLS0gdGhpcyBtYXkgb2NjdXIgd2l0aFxuICAgIC8vIHBhc3RlcyBmcm9tIE1TIFdvcmQsIGZvciBleGFtcGxlLiAgSG93ZXZlciB0aGlzIGlzIG9ubHkgcmljaCB0ZXh0IGlmXG4gICAgLy8gdGhlcmUncyBhY2NvbXBhbnlpbmcgdGV4dC5cbiAgICBpZiAodGhpcy5nZXRIVE1MKCkgJiYgdGhpcy5nZXRUZXh0KCkpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIC8vIFdoZW4gYW4gaW1hZ2UgaXMgY29waWVkIGZyb20gYSBwcmV2aWV3IHdpbmRvdywgeW91IGVuZCB1cCB3aXRoIHR3b1xuICAgIC8vIERhdGFUcmFuc2Zlckl0ZW1zIG9uZSBvZiB3aGljaCBpcyBhIGZpbGUncyBtZXRhZGF0YSBhcyB0ZXh0LiAgU2tpcCB0aG9zZS5cbiAgICBpZiAodGhpcy5pc0ltYWdlKCkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy50eXBlcy5zb21lKGZ1bmN0aW9uICh0eXBlKSB7XG4gICAgICByZXR1cm4gUklDSF9URVhUX1RZUEVTW3R5cGVdO1xuICAgIH0pO1xuICB9O1xuXG4gIC8qKlxuICAgKiBHZXQgcmF3IHRleHQuXG4gICAqXG4gICAqIEByZXR1cm4gez9zdHJpbmd9XG4gICAqL1xuXG5cbiAgRGF0YVRyYW5zZmVyLnByb3RvdHlwZS5nZXRUZXh0ID0gZnVuY3Rpb24gZ2V0VGV4dCgpIHtcbiAgICB2YXIgdGV4dDtcbiAgICBpZiAodGhpcy5kYXRhLmdldERhdGEpIHtcbiAgICAgIGlmICghdGhpcy50eXBlcy5sZW5ndGgpIHtcbiAgICAgICAgdGV4dCA9IHRoaXMuZGF0YS5nZXREYXRhKCdUZXh0Jyk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMudHlwZXMuaW5kZXhPZigndGV4dC9wbGFpbicpICE9IC0xKSB7XG4gICAgICAgIHRleHQgPSB0aGlzLmRhdGEuZ2V0RGF0YSgndGV4dC9wbGFpbicpO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gdGV4dCA/IHRleHQucmVwbGFjZShDUl9MRl9SRUdFWCwgTEZfT05MWSkgOiBudWxsO1xuICB9O1xuXG4gIC8qKlxuICAgKiBHZXQgSFRNTCBwYXN0ZSBkYXRhXG4gICAqXG4gICAqIEByZXR1cm4gez9zdHJpbmd9XG4gICAqL1xuXG5cbiAgRGF0YVRyYW5zZmVyLnByb3RvdHlwZS5nZXRIVE1MID0gZnVuY3Rpb24gZ2V0SFRNTCgpIHtcbiAgICBpZiAodGhpcy5kYXRhLmdldERhdGEpIHtcbiAgICAgIGlmICghdGhpcy50eXBlcy5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXREYXRhKCdUZXh0Jyk7XG4gICAgICB9IGVsc2UgaWYgKHRoaXMudHlwZXMuaW5kZXhPZigndGV4dC9odG1sJykgIT0gLTEpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXREYXRhKCd0ZXh0L2h0bWwnKTtcbiAgICAgIH1cbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIElzIHRoaXMgYSBsaW5rIGRhdGEgdHJhbnNmZXI/XG4gICAqXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuXG5cbiAgRGF0YVRyYW5zZmVyLnByb3RvdHlwZS5pc0xpbmsgPSBmdW5jdGlvbiBpc0xpbmsoKSB7XG4gICAgcmV0dXJuIHRoaXMudHlwZXMuc29tZShmdW5jdGlvbiAodHlwZSkge1xuICAgICAgcmV0dXJuIHR5cGUuaW5kZXhPZignVXJsJykgIT0gLTEgfHwgdHlwZS5pbmRleE9mKCd0ZXh0L3VyaS1saXN0JykgIT0gLTEgfHwgdHlwZS5pbmRleE9mKCd0ZXh0L3gtbW96LXVybCcpO1xuICAgIH0pO1xuICB9O1xuXG4gIC8qKlxuICAgKiBHZXQgYSBsaW5rIHVybC5cbiAgICpcbiAgICogQHJldHVybiB7P3N0cmluZ31cbiAgICovXG5cblxuICBEYXRhVHJhbnNmZXIucHJvdG90eXBlLmdldExpbmsgPSBmdW5jdGlvbiBnZXRMaW5rKCkge1xuICAgIGlmICh0aGlzLmRhdGEuZ2V0RGF0YSkge1xuICAgICAgaWYgKHRoaXMudHlwZXMuaW5kZXhPZigndGV4dC94LW1vei11cmwnKSAhPSAtMSkge1xuICAgICAgICB2YXIgdXJsID0gdGhpcy5kYXRhLmdldERhdGEoJ3RleHQveC1tb3otdXJsJykuc3BsaXQoJ1xcbicpO1xuICAgICAgICByZXR1cm4gdXJsWzBdO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMudHlwZXMuaW5kZXhPZigndGV4dC91cmktbGlzdCcpICE9IC0xID8gdGhpcy5kYXRhLmdldERhdGEoJ3RleHQvdXJpLWxpc3QnKSA6IHRoaXMuZGF0YS5nZXREYXRhKCd1cmwnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbnVsbDtcbiAgfTtcblxuICAvKipcbiAgICogSXMgdGhpcyBhbiBpbWFnZSBkYXRhIHRyYW5zZmVyP1xuICAgKlxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cblxuXG4gIERhdGFUcmFuc2Zlci5wcm90b3R5cGUuaXNJbWFnZSA9IGZ1bmN0aW9uIGlzSW1hZ2UoKSB7XG4gICAgdmFyIGlzSW1hZ2UgPSB0aGlzLnR5cGVzLnNvbWUoZnVuY3Rpb24gKHR5cGUpIHtcbiAgICAgIC8vIEZpcmVmb3ggd2lsbCBoYXZlIGEgdHlwZSBvZiBhcHBsaWNhdGlvbi94LW1vei1maWxlIGZvciBpbWFnZXMgZHVyaW5nXG4gICAgICAvLyBkcmFnZ2luZ1xuICAgICAgcmV0dXJuIHR5cGUuaW5kZXhPZignYXBwbGljYXRpb24veC1tb3otZmlsZScpICE9IC0xO1xuICAgIH0pO1xuXG4gICAgaWYgKGlzSW1hZ2UpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHZhciBpdGVtcyA9IHRoaXMuZ2V0RmlsZXMoKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGl0ZW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdHlwZSA9IGl0ZW1zW2ldLnR5cGU7XG4gICAgICBpZiAoIVBob3Rvc01pbWVUeXBlLmlzSW1hZ2UodHlwZSkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuICB9O1xuXG4gIERhdGFUcmFuc2Zlci5wcm90b3R5cGUuZ2V0Q291bnQgPSBmdW5jdGlvbiBnZXRDb3VudCgpIHtcbiAgICBpZiAodGhpcy5kYXRhLmhhc093blByb3BlcnR5KCdpdGVtcycpKSB7XG4gICAgICByZXR1cm4gdGhpcy5kYXRhLml0ZW1zLmxlbmd0aDtcbiAgICB9IGVsc2UgaWYgKHRoaXMuZGF0YS5oYXNPd25Qcm9wZXJ0eSgnbW96SXRlbUNvdW50JykpIHtcbiAgICAgIHJldHVybiB0aGlzLmRhdGEubW96SXRlbUNvdW50O1xuICAgIH0gZWxzZSBpZiAodGhpcy5kYXRhLmZpbGVzKSB7XG4gICAgICByZXR1cm4gdGhpcy5kYXRhLmZpbGVzLmxlbmd0aDtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH07XG5cbiAgLyoqXG4gICAqIEdldCBmaWxlcy5cbiAgICpcbiAgICogQHJldHVybiB7YXJyYXl9XG4gICAqL1xuXG5cbiAgRGF0YVRyYW5zZmVyLnByb3RvdHlwZS5nZXRGaWxlcyA9IGZ1bmN0aW9uIGdldEZpbGVzKCkge1xuICAgIGlmICh0aGlzLmRhdGEuaXRlbXMpIHtcbiAgICAgIC8vIGNyZWF0ZUFycmF5RnJvbU1peGVkIGRvZXNuJ3QgcHJvcGVybHkgaGFuZGxlIERhdGFUcmFuc2Zlckl0ZW1MaXN0cy5cbiAgICAgIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzLmRhdGEuaXRlbXMpLm1hcChnZXRGaWxlRnJvbURhdGFUcmFuc2ZlcikuZmlsdGVyKGVtcHR5RnVuY3Rpb24udGhhdFJldHVybnNBcmd1bWVudCk7XG4gICAgfSBlbHNlIGlmICh0aGlzLmRhdGEuZmlsZXMpIHtcbiAgICAgIHJldHVybiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzLmRhdGEuZmlsZXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gW107XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBBcmUgdGhlcmUgYW55IGZpbGVzIHRvIGZldGNoP1xuICAgKlxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cblxuXG4gIERhdGFUcmFuc2Zlci5wcm90b3R5cGUuaGFzRmlsZXMgPSBmdW5jdGlvbiBoYXNGaWxlcygpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRGaWxlcygpLmxlbmd0aCA+IDA7XG4gIH07XG5cbiAgcmV0dXJuIERhdGFUcmFuc2Zlcjtcbn0oKTtcblxubW9kdWxlLmV4cG9ydHMgPSBEYXRhVHJhbnNmZXI7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBOzs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7Ozs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/DataTransfer.js
