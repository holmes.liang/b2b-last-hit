/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */


var isWebkit = typeof navigator !== 'undefined' && navigator.userAgent.indexOf('AppleWebKit') > -1;
/**
 * Gets the element with the document scroll properties such as `scrollLeft` and
 * `scrollHeight`. This may differ across different browsers.
 *
 * NOTE: The return value can be null if the DOM is not yet ready.
 *
 * @param {?DOMDocument} doc Defaults to current document.
 * @return {?DOMElement}
 */

function getDocumentScrollElement(doc) {
  doc = doc || document;

  if (doc.scrollingElement) {
    return doc.scrollingElement;
  }

  return !isWebkit && doc.compatMode === 'CSS1Compat' ? doc.documentElement : doc.body;
}

module.exports = getDocumentScrollElement;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0RG9jdW1lbnRTY3JvbGxFbGVtZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0RG9jdW1lbnRTY3JvbGxFbGVtZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICogQHR5cGVjaGVja3NcbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBpc1dlYmtpdCA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIG5hdmlnYXRvci51c2VyQWdlbnQuaW5kZXhPZignQXBwbGVXZWJLaXQnKSA+IC0xO1xuXG4vKipcbiAqIEdldHMgdGhlIGVsZW1lbnQgd2l0aCB0aGUgZG9jdW1lbnQgc2Nyb2xsIHByb3BlcnRpZXMgc3VjaCBhcyBgc2Nyb2xsTGVmdGAgYW5kXG4gKiBgc2Nyb2xsSGVpZ2h0YC4gVGhpcyBtYXkgZGlmZmVyIGFjcm9zcyBkaWZmZXJlbnQgYnJvd3NlcnMuXG4gKlxuICogTk9URTogVGhlIHJldHVybiB2YWx1ZSBjYW4gYmUgbnVsbCBpZiB0aGUgRE9NIGlzIG5vdCB5ZXQgcmVhZHkuXG4gKlxuICogQHBhcmFtIHs/RE9NRG9jdW1lbnR9IGRvYyBEZWZhdWx0cyB0byBjdXJyZW50IGRvY3VtZW50LlxuICogQHJldHVybiB7P0RPTUVsZW1lbnR9XG4gKi9cbmZ1bmN0aW9uIGdldERvY3VtZW50U2Nyb2xsRWxlbWVudChkb2MpIHtcbiAgZG9jID0gZG9jIHx8IGRvY3VtZW50O1xuICBpZiAoZG9jLnNjcm9sbGluZ0VsZW1lbnQpIHtcbiAgICByZXR1cm4gZG9jLnNjcm9sbGluZ0VsZW1lbnQ7XG4gIH1cbiAgcmV0dXJuICFpc1dlYmtpdCAmJiBkb2MuY29tcGF0TW9kZSA9PT0gJ0NTUzFDb21wYXQnID8gZG9jLmRvY3VtZW50RWxlbWVudCA6IGRvYy5ib2R5O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGdldERvY3VtZW50U2Nyb2xsRWxlbWVudDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getDocumentScrollElement.js
