

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _en_US = _interopRequireDefault(__webpack_require__(/*! rc-pagination/lib/locale/en_US */ "./node_modules/rc-pagination/lib/locale/en_US.js"));

var _en_US2 = _interopRequireDefault(__webpack_require__(/*! ../date-picker/locale/en_US */ "./node_modules/antd/lib/date-picker/locale/en_US.js"));

var _en_US3 = _interopRequireDefault(__webpack_require__(/*! ../time-picker/locale/en_US */ "./node_modules/antd/lib/time-picker/locale/en_US.js"));

var _en_US4 = _interopRequireDefault(__webpack_require__(/*! ../calendar/locale/en_US */ "./node_modules/antd/lib/calendar/locale/en_US.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var _default = {
  locale: 'en',
  Pagination: _en_US["default"],
  DatePicker: _en_US2["default"],
  TimePicker: _en_US3["default"],
  Calendar: _en_US4["default"],
  global: {
    placeholder: 'Please select'
  },
  Table: {
    filterTitle: 'Filter menu',
    filterConfirm: 'OK',
    filterReset: 'Reset',
    selectAll: 'Select current page',
    selectInvert: 'Invert current page',
    sortTitle: 'Sort',
    expand: 'Expand row',
    collapse: 'Collapse row'
  },
  Modal: {
    okText: 'OK',
    cancelText: 'Cancel',
    justOkText: 'OK'
  },
  Popconfirm: {
    okText: 'OK',
    cancelText: 'Cancel'
  },
  Transfer: {
    titles: ['', ''],
    searchPlaceholder: 'Search here',
    itemUnit: 'item',
    itemsUnit: 'items'
  },
  Upload: {
    uploading: 'Uploading...',
    removeFile: 'Remove file',
    uploadError: 'Upload error',
    previewFile: 'Preview file',
    downloadFile: 'Download file'
  },
  Empty: {
    description: 'No Data'
  },
  Icon: {
    icon: 'icon'
  },
  Text: {
    edit: 'Edit',
    copy: 'Copy',
    copied: 'Copied',
    expand: 'Expand'
  },
  PageHeader: {
    back: 'Back'
  }
};
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvbG9jYWxlL2RlZmF1bHQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2xvY2FsZS9kZWZhdWx0LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUGFnaW5hdGlvbiBmcm9tICdyYy1wYWdpbmF0aW9uL2xpYi9sb2NhbGUvZW5fVVMnO1xuaW1wb3J0IERhdGVQaWNrZXIgZnJvbSAnLi4vZGF0ZS1waWNrZXIvbG9jYWxlL2VuX1VTJztcbmltcG9ydCBUaW1lUGlja2VyIGZyb20gJy4uL3RpbWUtcGlja2VyL2xvY2FsZS9lbl9VUyc7XG5pbXBvcnQgQ2FsZW5kYXIgZnJvbSAnLi4vY2FsZW5kYXIvbG9jYWxlL2VuX1VTJztcbmV4cG9ydCBkZWZhdWx0IHtcbiAgICBsb2NhbGU6ICdlbicsXG4gICAgUGFnaW5hdGlvbixcbiAgICBEYXRlUGlja2VyLFxuICAgIFRpbWVQaWNrZXIsXG4gICAgQ2FsZW5kYXIsXG4gICAgZ2xvYmFsOiB7XG4gICAgICAgIHBsYWNlaG9sZGVyOiAnUGxlYXNlIHNlbGVjdCcsXG4gICAgfSxcbiAgICBUYWJsZToge1xuICAgICAgICBmaWx0ZXJUaXRsZTogJ0ZpbHRlciBtZW51JyxcbiAgICAgICAgZmlsdGVyQ29uZmlybTogJ09LJyxcbiAgICAgICAgZmlsdGVyUmVzZXQ6ICdSZXNldCcsXG4gICAgICAgIHNlbGVjdEFsbDogJ1NlbGVjdCBjdXJyZW50IHBhZ2UnLFxuICAgICAgICBzZWxlY3RJbnZlcnQ6ICdJbnZlcnQgY3VycmVudCBwYWdlJyxcbiAgICAgICAgc29ydFRpdGxlOiAnU29ydCcsXG4gICAgICAgIGV4cGFuZDogJ0V4cGFuZCByb3cnLFxuICAgICAgICBjb2xsYXBzZTogJ0NvbGxhcHNlIHJvdycsXG4gICAgfSxcbiAgICBNb2RhbDoge1xuICAgICAgICBva1RleHQ6ICdPSycsXG4gICAgICAgIGNhbmNlbFRleHQ6ICdDYW5jZWwnLFxuICAgICAgICBqdXN0T2tUZXh0OiAnT0snLFxuICAgIH0sXG4gICAgUG9wY29uZmlybToge1xuICAgICAgICBva1RleHQ6ICdPSycsXG4gICAgICAgIGNhbmNlbFRleHQ6ICdDYW5jZWwnLFxuICAgIH0sXG4gICAgVHJhbnNmZXI6IHtcbiAgICAgICAgdGl0bGVzOiBbJycsICcnXSxcbiAgICAgICAgc2VhcmNoUGxhY2Vob2xkZXI6ICdTZWFyY2ggaGVyZScsXG4gICAgICAgIGl0ZW1Vbml0OiAnaXRlbScsXG4gICAgICAgIGl0ZW1zVW5pdDogJ2l0ZW1zJyxcbiAgICB9LFxuICAgIFVwbG9hZDoge1xuICAgICAgICB1cGxvYWRpbmc6ICdVcGxvYWRpbmcuLi4nLFxuICAgICAgICByZW1vdmVGaWxlOiAnUmVtb3ZlIGZpbGUnLFxuICAgICAgICB1cGxvYWRFcnJvcjogJ1VwbG9hZCBlcnJvcicsXG4gICAgICAgIHByZXZpZXdGaWxlOiAnUHJldmlldyBmaWxlJyxcbiAgICAgICAgZG93bmxvYWRGaWxlOiAnRG93bmxvYWQgZmlsZScsXG4gICAgfSxcbiAgICBFbXB0eToge1xuICAgICAgICBkZXNjcmlwdGlvbjogJ05vIERhdGEnLFxuICAgIH0sXG4gICAgSWNvbjoge1xuICAgICAgICBpY29uOiAnaWNvbicsXG4gICAgfSxcbiAgICBUZXh0OiB7XG4gICAgICAgIGVkaXQ6ICdFZGl0JyxcbiAgICAgICAgY29weTogJ0NvcHknLFxuICAgICAgICBjb3BpZWQ6ICdDb3BpZWQnLFxuICAgICAgICBleHBhbmQ6ICdFeHBhbmQnLFxuICAgIH0sXG4gICAgUGFnZUhlYWRlcjoge1xuICAgICAgICBiYWNrOiAnQmFjaycsXG4gICAgfSxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQXJEQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/lib/locale/default.js
