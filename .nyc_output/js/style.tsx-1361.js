__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    .item {\n        min-height: 40px;\n        line-height: 40px;\n        .itemTitle {\n            text-align: right;\n            font-size: 14px;\n            color: #9e9e9e;\n            text-transform: uppercase;\n            padding-left: 12px;line-height: 20px;\n            padding: 10px 0;\n        }\n        .mobile-item-title {\n          line-height: 20px;\n          padding: 10px !important;\n          width: 40%;\n        }\n        .itemContent {\n            font-size: 16px;\n            color: rgba(0, 0, 0, 0.85);\n            width: 60%;\n            line-height: 20px;\n            padding: 10px 0;\n        }\n        .itemContentRight {\n            text-align: right;\n            font-size: 16px;\n            color: rgba(0, 0, 0, 0.85);\n        }\n     }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


/* harmony default export */ __webpack_exports__["default"] = ({
  box: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3ZpZXctaXRlbS9zdHlsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvdmlldy1pdGVtL3N0eWxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdHlsZWQgYXMgc3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgYm94OiBzdHlsZWQuZGl2YFxuICAgIC5pdGVtIHtcbiAgICAgICAgbWluLWhlaWdodDogNDBweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gICAgICAgIC5pdGVtVGl0bGUge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICBjb2xvcjogIzllOWU5ZTtcbiAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHg7bGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgICAgIH1cbiAgICAgICAgLm1vYmlsZS1pdGVtLXRpdGxlIHtcbiAgICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiAxMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgd2lkdGg6IDQwJTtcbiAgICAgICAgfVxuICAgICAgICAuaXRlbUNvbnRlbnQge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44NSk7XG4gICAgICAgICAgICB3aWR0aDogNjAlO1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgICAgIH1cbiAgICAgICAgLml0ZW1Db250ZW50UmlnaHQge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg1KTtcbiAgICAgICAgfVxuICAgICB9XG4gIGAsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/view-item/style.tsx
