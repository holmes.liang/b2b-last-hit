__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _desk_component_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../desk/component/page */ "./src/app/desk/component/page/index.tsx");
/* harmony import */ var _home_style__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home-style */ "./src/app/desk/home/home-style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _manager_index__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./manager-index */ "./src/app/desk/home/manager-index.tsx");
/* harmony import */ var _agent_index__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./agent-index */ "./src/app/desk/home/agent-index.tsx");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! echarts/lib/chart/bar */ "./node_modules/echarts/lib/chart/bar.js");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! echarts/lib/chart/line */ "./node_modules/echarts/lib/chart/line.js");
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! echarts/lib/chart/pie */ "./node_modules/echarts/lib/chart/pie.js");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! echarts/lib/component/legend */ "./node_modules/echarts/lib/component/legend.js");
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! echarts/lib/component/legendScroll */ "./node_modules/echarts/lib/component/legendScroll.js");
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! echarts/lib/component/tooltip */ "./node_modules/echarts/lib/component/tooltip.js");
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__);






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/home/index.tsx";














var Home =
/*#__PURE__*/
function (_DeskPage) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(Home, _DeskPage);

  function Home(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Home);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Home).call(this, props, context));
    _this.html = "";
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Home, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Home.prototype), "initComponents", this).call(this), _home_style__WEBPACK_IMPORTED_MODULE_8__["default"]);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.getHelpers().getRouter().replaceRedirect(_common__WEBPACK_IMPORTED_MODULE_9__["PATH"].DASHBOARD);

      function bodyScale() {
        var scale = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getWhith() > 1200 || _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getWhith() < 768 ? 1 : _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getWhith() / 1200;
        var container = document.querySelector(".page-body .container");

        if (container) {
          try {
            container.style.minHeight = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getHeight() * (1 / scale) - 196 * scale + "px";
          } catch (e) {}
        }
      }

      bodyScale();
      var isSales = _common__WEBPACK_IMPORTED_MODULE_9__["Authority"].isAgent() || _common__WEBPACK_IMPORTED_MODULE_9__["Authority"].isBroker();
      var isInsurer = _common__WEBPACK_IMPORTED_MODULE_9__["Authority"].isInsurer();

      if (!isSales && !isInsurer) {
        _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get("/welcome/img").then(function (res) {
          _this2.setState({
            homeImageUrl: (res.body || {}).respData
          });
        });
      }
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      var C = this.getComponents();
      var isSales = _common__WEBPACK_IMPORTED_MODULE_9__["Authority"].isDashboardSales();
      var isInsurer = _common__WEBPACK_IMPORTED_MODULE_9__["Authority"].isDashboardMgr();

      if (isInsurer) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
          "data-widget": "page-body",
          style: {
            position: "relative"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 78
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Home, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 79
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          className: "container",
          style: {
            padding: "0 0 20px 0"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 80
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_manager_index__WEBPACK_IMPORTED_MODULE_10__["default"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 86
          },
          __self: this
        }))));
      } else if (isSales) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
          "data-widget": "page-body",
          style: {
            position: "relative"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 93
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Home, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 94
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          className: "container",
          style: {
            padding: "0"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 95
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_agent_index__WEBPACK_IMPORTED_MODULE_11__["default"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 101
          },
          __self: this
        }))));
      } else {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
          "data-widget": "page-body",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 108
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Home, {
          style: {
            display: "flex"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          className: "container",
          style: {
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "0 0 20px 0"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 114
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("img", {
          style: {
            width: "500px"
          },
          src: this.state.homeImageUrl,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 123
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 129
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("h1", {
          style: {
            fontSize: "50px"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 130
          },
          __self: this
        }, "WELCOME"), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
          style: {
            marginTop: "20px",
            marginLeft: "10px"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 131
          },
          __self: this
        }, "Have a nice day")))));
      }
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Home.prototype), "initState", this).call(this), {
        products: [],
        premcurrm: {
          policyCount: 0,
          ranking: "",
          gwp: "",
          commission: "",
          currencyCode: "",
          currencySymbol: ""
        },
        policyList: {
          policyCount: 0,
          gwp: "",
          commission: "",
          currencyCode: "",
          currencySymbol: ""
        },
        shouldShowPremiumStructure: false,
        shouldShowPremLast12m: false,
        homeImageUrl: ""
      });
    }
  }]);

  return Home;
}(_desk_component_page__WEBPACK_IMPORTED_MODULE_7__["DeskPage"]);

/* harmony default export */ __webpack_exports__["default"] = (Home);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svaG9tZS9pbmRleC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9ob21lL2luZGV4LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQYWdlQ29tcG9uZW50cyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBEZXNrUGFnZSB9IGZyb20gXCIuLi8uLi9kZXNrL2NvbXBvbmVudC9wYWdlXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgSG9tZVN0eWxlIGZyb20gXCIuL2hvbWUtc3R5bGVcIjtcbmltcG9ydCB7IEFqYXgsIEF1dGhvcml0eSwgUEFUSCwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IE1hbmFnZXJJbmRleCBmcm9tIFwiLi9tYW5hZ2VyLWluZGV4XCI7XG5pbXBvcnQgQWdlbnRJbmRleCBmcm9tIFwiLi9hZ2VudC1pbmRleFwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY2hhcnQvYmFyXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jaGFydC9saW5lXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jaGFydC9waWVcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmRcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmRTY3JvbGxcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC90aXRsZVwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXBcIjtcblxuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5leHBvcnQgdHlwZSBTdHlsZWRTcGFuID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcInNwYW5cIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgSG9tZVBhZ2VDb21wb25lbnRzID0ge1xuICBIb21lOiBTdHlsZWRESVY7XG59ICYgUGFnZUNvbXBvbmVudHM7XG5leHBvcnQgdHlwZSBIb21lU3RhdGUgPSB7XG4gIHByb2R1Y3RzOiBhbnk7XG4gIHByZW1jdXJybTogYW55O1xuICBwb2xpY3lMaXN0OiBhbnk7XG4gIGhvbWVJbWFnZVVybDogYW55O1xuICBzaG91bGRTaG93UHJlbWl1bVN0cnVjdHVyZTogYm9vbGVhbjtcbiAgc2hvdWxkU2hvd1ByZW1MYXN0MTJtOiBib29sZWFuO1xufTtcblxuY2xhc3MgSG9tZTxQLCBTIGV4dGVuZHMgSG9tZVN0YXRlLCBDIGV4dGVuZHMgSG9tZVBhZ2VDb21wb25lbnRzPiBleHRlbmRzIERlc2tQYWdlPFAsIFMsIEM+IHtcbiAgaHRtbDogYW55ID0gXCJcIjtcblxuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdENvbXBvbmVudHMoKSwgSG9tZVN0eWxlKSBhcyBDO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG5cbiAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgLmdldFJvdXRlcigpXG4gICAgICAucmVwbGFjZVJlZGlyZWN0KFBBVEguREFTSEJPQVJEKTtcblxuICAgIGZ1bmN0aW9uIGJvZHlTY2FsZSgpIHtcbiAgICAgIGNvbnN0IHNjYWxlID0gVXRpbHMuZ2V0V2hpdGgoKSA+IDEyMDAgfHwgVXRpbHMuZ2V0V2hpdGgoKSA8IDc2OCA/IDEgOiBVdGlscy5nZXRXaGl0aCgpIC8gMTIwMDtcbiAgICAgIGxldCBjb250YWluZXI6IGFueSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIucGFnZS1ib2R5IC5jb250YWluZXJcIik7XG4gICAgICBpZiAoY29udGFpbmVyKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgY29udGFpbmVyLnN0eWxlLm1pbkhlaWdodCA9IChVdGlscy5nZXRIZWlnaHQoKSAqICgxIC8gc2NhbGUpIC0gMTk2ICogc2NhbGUpICsgXCJweFwiO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG5cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGJvZHlTY2FsZSgpO1xuICAgIGNvbnN0IGlzU2FsZXMgPSBBdXRob3JpdHkuaXNBZ2VudCgpIHx8IEF1dGhvcml0eS5pc0Jyb2tlcigpO1xuICAgIGNvbnN0IGlzSW5zdXJlciA9IEF1dGhvcml0eS5pc0luc3VyZXIoKTtcbiAgICBpZiAoIWlzU2FsZXMgJiYgIWlzSW5zdXJlcikge1xuICAgICAgQWpheC5nZXQoYC93ZWxjb21lL2ltZ2ApLnRoZW4oKHJlczogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGhvbWVJbWFnZVVybDogKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZW5kZXJQYWdlQm9keSgpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgaXNTYWxlcyA9IEF1dGhvcml0eS5pc0Rhc2hib2FyZFNhbGVzKCk7XG4gICAgY29uc3QgaXNJbnN1cmVyID0gQXV0aG9yaXR5LmlzRGFzaGJvYXJkTWdyKCk7XG4gICAgaWYgKGlzSW5zdXJlcikge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPEMuQm94IGRhdGEtd2lkZ2V0PVwicGFnZS1ib2R5XCIgc3R5bGU9e3sgcG9zaXRpb246IFwicmVsYXRpdmVcIiB9fT5cbiAgICAgICAgICA8Qy5Ib21lPlxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJjb250YWluZXJcIlxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IFwiMCAwIDIwcHggMFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8TWFuYWdlckluZGV4Lz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvQy5Ib21lPlxuICAgICAgICA8L0MuQm94PlxuICAgICAgKTtcbiAgICB9IGVsc2UgaWYgKGlzU2FsZXMpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxDLkJveCBkYXRhLXdpZGdldD1cInBhZ2UtYm9keVwiIHN0eWxlPXt7IHBvc2l0aW9uOiBcInJlbGF0aXZlXCIgfX0+XG4gICAgICAgICAgPEMuSG9tZT5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiY29udGFpbmVyXCJcbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiBcIjBcIixcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEFnZW50SW5kZXgvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9DLkhvbWU+XG4gICAgICAgIDwvQy5Cb3g+XG4gICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8Qy5Cb3ggZGF0YS13aWRnZXQ9XCJwYWdlLWJvZHlcIj5cbiAgICAgICAgICA8Qy5Ib21lXG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJjb250YWluZXJcIlxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IFwiZmxleFwiLFxuICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxuICAgICAgICAgICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXG4gICAgICAgICAgICAgICAgcGFkZGluZzogXCIwIDAgMjBweCAwXCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgd2lkdGg6IFwiNTAwcHhcIixcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgIHNyYz17dGhpcy5zdGF0ZS5ob21lSW1hZ2VVcmx9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgPGgxIHN0eWxlPXt7IGZvbnRTaXplOiBcIjUwcHhcIiB9fT5XRUxDT01FPC9oMT5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW5Ub3A6IFwiMjBweFwiLFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW5MZWZ0OiBcIjEwcHhcIixcbiAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgSGF2ZSBhIG5pY2UgZGF5XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9DLkhvbWU+XG4gICAgICAgIDwvQy5Cb3g+XG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIHByb2R1Y3RzOiBbXSxcbiAgICAgIHByZW1jdXJybToge1xuICAgICAgICBwb2xpY3lDb3VudDogMCxcbiAgICAgICAgcmFua2luZzogXCJcIixcbiAgICAgICAgZ3dwOiBcIlwiLFxuICAgICAgICBjb21taXNzaW9uOiBcIlwiLFxuICAgICAgICBjdXJyZW5jeUNvZGU6IFwiXCIsXG4gICAgICAgIGN1cnJlbmN5U3ltYm9sOiBcIlwiLFxuICAgICAgfSxcbiAgICAgIHBvbGljeUxpc3Q6IHtcbiAgICAgICAgcG9saWN5Q291bnQ6IDAsXG4gICAgICAgIGd3cDogXCJcIixcbiAgICAgICAgY29tbWlzc2lvbjogXCJcIixcbiAgICAgICAgY3VycmVuY3lDb2RlOiBcIlwiLFxuICAgICAgICBjdXJyZW5jeVN5bWJvbDogXCJcIixcbiAgICAgIH0sXG4gICAgICBzaG91bGRTaG93UHJlbWl1bVN0cnVjdHVyZTogZmFsc2UsXG4gICAgICBzaG91bGRTaG93UHJlbUxhc3QxMm06IGZhbHNlLFxuICAgICAgaG9tZUltYWdlVXJsOiBcIlwiLFxuICAgIH0pIGFzIFM7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgSG9tZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFlQTs7Ozs7QUFHQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBREE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQ0E7QUFEQTtBQUdBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFuQkE7QUFxQkE7Ozs7QUExSUE7QUFDQTtBQTRJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/home/index.tsx
