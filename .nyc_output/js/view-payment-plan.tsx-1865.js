__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewPaymentPlanStyle2", function() { return ViewPaymentPlanStyle2; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_component_view_item_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/view-item/style */ "./src/app/desk/component/view-item/style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_quote_components_panyment_arramgement_component_view_arrangement__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk/quote/components/panyment-arramgement/component/view-arrangement */ "./src/app/desk/quote/components/panyment-arramgement/component/view-arrangement.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");



var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/components/panyment-arramgement/view-payment-plan.tsx";









var isMobile = _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].getIsMobile();
var defaultLayout = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 13
    },
    sm: {
      span: 16
    }
  }
};

var getProp = function getProp(propName, dataFixed) {
  if (dataFixed) {
    return "".concat(dataFixed, ".").concat(propName);
  }

  return propName;
};

var ViewPaymentPlan = function ViewPaymentPlan(_ref) {
  var title = _ref.title,
      model = _ref.model,
      dataFixed = _ref.dataFixed,
      _ref$layout = _ref.layout,
      layout = _ref$layout === void 0 ? defaultLayout : _ref$layout,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__["default"])(_ref, ["title", "model", "dataFixed", "layout"]);

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_8__["useState"])([]),
      _useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_useState, 2),
      plan = _useState2[0],
      setPlan = _useState2[1];

  var getMasterTableItemText = function getMasterTableItemText() {
    var plans = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.cloneDeep(plan);

    var paymentPlan = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(model, getProp("paymentMethod.installment.paymentPlan", dataFixed));

    if (paymentPlan) {
      var item = (plans || []).find(function (option) {
        return paymentPlan === option.code;
      }) || {};
      return item.name || "";
    } else {
      return "";
    }
  };

  var handleView = function handleView() {
    var oldModel = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.cloneDeep(model);

    var details = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(oldModel, getProp("paymentMethod.installment.details", dataFixed)) || [];
    _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_9__["default"].create({
      Component: function Component(_ref2) {
        var onCancel = _ref2.onCancel,
            onOk = _ref2.onOk;
        return _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(_desk_quote_components_panyment_arramgement_component_view_arrangement__WEBPACK_IMPORTED_MODULE_10__["default"], {
          onCancel: onCancel,
          onOK: onOk,
          model: _model__WEBPACK_IMPORTED_MODULE_11__["Modeller"].asProxied({
            details: details
          }),
          oldModel: oldModel,
          dataFixed: dataFixed,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 47
          },
          __self: this
        });
      }
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_8__["useEffect"])(function () {
    var policyId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(model, getProp("policyId", dataFixed));

    var policy = dataFixed ? lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(model, dataFixed, {}) : model;
    _common__WEBPACK_IMPORTED_MODULE_7__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_7__["Apis"].PAYMENT_PLAN.replace(":policyId", policyId), policy).then(function (res) {
      setPlan((res.body || {}).respData || []);
    });
  }, []);

  var paymentPlan = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(model, getProp("paymentMethod.installment.paymentPlan", dataFixed));

  return _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(_desk_component_view_item_style__WEBPACK_IMPORTED_MODULE_6__["default"].box, Object.assign({}, rest, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Row"], {
    className: "item",
    type: "flex",
    gutter: 24,
    align: "middle",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemTitle ".concat(isMobile ? "mobile-item-title" : "")
  }, layout.labelCol, {
    style: {
      paddingRight: 1
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72
    },
    __self: this
  }), title), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemContent"
  }, layout.wrapperCol, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }), getMasterTableItemText())), paymentPlan && paymentPlan !== "LUMPSUM" && _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Row"], {
    className: "item",
    type: "flex",
    gutter: 24,
    align: "middle",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemTitle ".concat(isMobile ? "mobile-item-title" : "")
  }, layout.labelCol, {
    style: {
      paddingRight: 1
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84
    },
    __self: this
  })), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Col"], Object.assign({
    className: "itemContent"
  }, layout.wrapperCol, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }), _common_3rd__WEBPACK_IMPORTED_MODULE_3__["React"].createElement("a", {
    onClick: function onClick() {
      return handleView();
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91
    },
    __self: this
  }, "view payment plan"))));
};

var defaultLayout2 = {
  labelCol: {
    xs: {
      span: 6
    },
    sm: {
      span: 8
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};

var ViewPaymentPlanStyle2 = function ViewPaymentPlanStyle2(props) {
  return ViewPaymentPlan(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, props, {
    layout: defaultLayout2
  }));
};


/* harmony default export */ __webpack_exports__["default"] = (ViewPaymentPlan);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcG9uZW50cy9wYW55bWVudC1hcnJhbWdlbWVudC92aWV3LXBheW1lbnQtcGxhbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9jb21wb25lbnRzL3BhbnltZW50LWFycmFtZ2VtZW50L3ZpZXctcGF5bWVudC1wbGFuLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IFN0eWxlIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmlldy1pdGVtL3N0eWxlXCI7XG5pbXBvcnQgeyBBamF4LCBBcGlzLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCBWaWV3QXJyYW5nZW1lbnQgZnJvbSBcIkBkZXNrL3F1b3RlL2NvbXBvbmVudHMvcGFueW1lbnQtYXJyYW1nZW1lbnQvY29tcG9uZW50L3ZpZXctYXJyYW5nZW1lbnRcIjtcbmltcG9ydCB7IE1vZGVsbGVyIH0gZnJvbSBcIkBtb2RlbFwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5jb25zdCBkZWZhdWx0TGF5b3V0OiBhbnkgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogNiB9LFxuICAgIHNtOiB7IHNwYW46IDggfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDEzIH0sXG4gICAgc206IHsgc3BhbjogMTYgfSxcbiAgfSxcbn07XG5jb25zdCBnZXRQcm9wID0gKHByb3BOYW1lOiBhbnksIGRhdGFGaXhlZD86IGFueSkgPT4ge1xuICBpZiAoZGF0YUZpeGVkKSB7XG4gICAgcmV0dXJuIGAke2RhdGFGaXhlZH0uJHtwcm9wTmFtZX1gO1xuICB9XG4gIHJldHVybiBwcm9wTmFtZTtcbn07XG5jb25zdCBWaWV3UGF5bWVudFBsYW4gPSAoeyB0aXRsZSwgbW9kZWwsIGRhdGFGaXhlZCwgbGF5b3V0ID0gZGVmYXVsdExheW91dCwgLi4ucmVzdCB9OiBhbnkpID0+IHtcbiAgY29uc3QgW3BsYW4sIHNldFBsYW5dID0gdXNlU3RhdGUoW10pO1xuICBjb25zdCBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0ID0gKCkgPT4ge1xuICAgIGNvbnN0IHBsYW5zOiBhbnkgPSBfLmNsb25lRGVlcChwbGFuKTtcbiAgICBjb25zdCBwYXltZW50UGxhbiA9IF8uZ2V0KG1vZGVsLCBnZXRQcm9wKFwicGF5bWVudE1ldGhvZC5pbnN0YWxsbWVudC5wYXltZW50UGxhblwiLCBkYXRhRml4ZWQpKTtcbiAgICBpZiAocGF5bWVudFBsYW4pIHtcbiAgICAgIGNvbnN0IGl0ZW0gPVxuICAgICAgICAocGxhbnMgfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIHBheW1lbnRQbGFuID09PSBvcHRpb24uY29kZTtcbiAgICAgICAgfSkgfHwge307XG4gICAgICByZXR1cm4gaXRlbS5uYW1lIHx8IFwiXCI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgfTtcbiAgY29uc3QgaGFuZGxlVmlldyA9ICgpID0+IHtcbiAgICBjb25zdCBvbGRNb2RlbCA9IF8uY2xvbmVEZWVwKG1vZGVsKTtcbiAgICBjb25zdCBkZXRhaWxzID0gXy5nZXQob2xkTW9kZWwsIGdldFByb3AoXCJwYXltZW50TWV0aG9kLmluc3RhbGxtZW50LmRldGFpbHNcIiwgZGF0YUZpeGVkKSkgfHwgW107XG4gICAgTWFzay5jcmVhdGUoe1xuICAgICAgQ29tcG9uZW50OiAoeyBvbkNhbmNlbCwgb25PayB9OiBhbnkpID0+IDxWaWV3QXJyYW5nZW1lbnRcbiAgICAgICAgb25DYW5jZWw9e29uQ2FuY2VsfVxuICAgICAgICBvbk9LPXtvbk9rfVxuICAgICAgICBtb2RlbD17TW9kZWxsZXIuYXNQcm94aWVkKHtcbiAgICAgICAgICBkZXRhaWxzLFxuICAgICAgICB9KX1cbiAgICAgICAgb2xkTW9kZWw9e29sZE1vZGVsfVxuICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGNvbnN0IHBvbGljeUlkID0gXy5nZXQobW9kZWwsIGdldFByb3AoXCJwb2xpY3lJZFwiLCBkYXRhRml4ZWQpKTtcbiAgICBjb25zdCBwb2xpY3kgPSBkYXRhRml4ZWQgPyBfLmdldChtb2RlbCwgZGF0YUZpeGVkLCB7fSkgOiBtb2RlbDtcbiAgICBBamF4LnBvc3QoQXBpcy5QQVlNRU5UX1BMQU4ucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZCksIHBvbGljeSkudGhlbigocmVzKSA9PiB7XG4gICAgICBzZXRQbGFuKFxuICAgICAgICAoKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCBbXSksXG4gICAgICApO1xuICAgIH0pO1xuICB9LCBbXSk7XG4gIGNvbnN0IHBheW1lbnRQbGFuID0gXy5nZXQobW9kZWwsIGdldFByb3AoXCJwYXltZW50TWV0aG9kLmluc3RhbGxtZW50LnBheW1lbnRQbGFuXCIsIGRhdGFGaXhlZCkpO1xuXG4gIHJldHVybiAoXG4gICAgPFN0eWxlLmJveCB7Li4ucmVzdH0+XG4gICAgICA8Um93IGNsYXNzTmFtZT1cIml0ZW1cIiB0eXBlPVwiZmxleFwiIGd1dHRlcj17MjR9IGFsaWduPVwibWlkZGxlXCI+XG4gICAgICAgIDxDb2xcbiAgICAgICAgICBjbGFzc05hbWU9e2BpdGVtVGl0bGUgJHtpc01vYmlsZSA/IFwibW9iaWxlLWl0ZW0tdGl0bGVcIiA6IFwiXCJ9YH1cbiAgICAgICAgICB7Li4ubGF5b3V0LmxhYmVsQ29sfVxuICAgICAgICAgIHN0eWxlPXt7IHBhZGRpbmdSaWdodDogMSB9fVxuICAgICAgICA+XG4gICAgICAgICAge3RpdGxlfVxuICAgICAgICA8L0NvbD5cbiAgICAgICAgPENvbCBjbGFzc05hbWU9XCJpdGVtQ29udGVudFwiIHsuLi5sYXlvdXQud3JhcHBlckNvbH0+XG4gICAgICAgICAge2dldE1hc3RlclRhYmxlSXRlbVRleHQoKX1cbiAgICAgICAgPC9Db2w+XG4gICAgICA8L1Jvdz5cbiAgICAgIHtwYXltZW50UGxhbiAmJiBwYXltZW50UGxhbiAhPT0gXCJMVU1QU1VNXCIgJiYgPFJvdyBjbGFzc05hbWU9XCJpdGVtXCIgdHlwZT1cImZsZXhcIiBndXR0ZXI9ezI0fSBhbGlnbj1cIm1pZGRsZVwiPlxuICAgICAgICA8Q29sXG4gICAgICAgICAgY2xhc3NOYW1lPXtgaXRlbVRpdGxlICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1pdGVtLXRpdGxlXCIgOiBcIlwifWB9XG4gICAgICAgICAgey4uLmxheW91dC5sYWJlbENvbH1cbiAgICAgICAgICBzdHlsZT17eyBwYWRkaW5nUmlnaHQ6IDEgfX1cbiAgICAgICAgPlxuICAgICAgICA8L0NvbD5cbiAgICAgICAgPENvbCBjbGFzc05hbWU9XCJpdGVtQ29udGVudFwiIHsuLi5sYXlvdXQud3JhcHBlckNvbH0+XG4gICAgICAgICAgPGEgb25DbGljaz17KCkgPT4gaGFuZGxlVmlldygpfT52aWV3IHBheW1lbnQgcGxhbjwvYT5cbiAgICAgICAgPC9Db2w+XG4gICAgICA8L1Jvdz5cbiAgICAgIH1cbiAgICA8L1N0eWxlLmJveD5cbiAgKTtcbn07XG5cbmNvbnN0IGRlZmF1bHRMYXlvdXQyID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDYgfSxcbiAgICBzbTogeyBzcGFuOiA4IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxNiB9LFxuICAgIHNtOiB7IHNwYW46IDEzIH0sXG4gIH0sXG59O1xuXG5jb25zdCBWaWV3UGF5bWVudFBsYW5TdHlsZTIgPSAocHJvcHM6IGFueSkgPT4gVmlld1BheW1lbnRQbGFuKHsgLi4ucHJvcHMsIGxheW91dDogZGVmYXVsdExheW91dDIgfSk7XG5cbmV4cG9ydCB7IFZpZXdQYXltZW50UGxhblN0eWxlMiB9O1xuXG5leHBvcnQgZGVmYXVsdCBWaWV3UGF5bWVudFBsYW47XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/components/panyment-arramgement/view-payment-plan.tsx
