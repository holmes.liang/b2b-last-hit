__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");








var QuoteCommon =
/*#__PURE__*/
function () {
  function QuoteCommon() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, QuoteCommon);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(QuoteCommon, [{
    key: "tipSaveMsg",
    value: function tipSaveMsg() {
      antd__WEBPACK_IMPORTED_MODULE_4__["notification"].success({
        message: _common__WEBPACK_IMPORTED_MODULE_3__["Language"].en("Saved successfully").thai("บันทึกคำพูดสำเร็จแล้ว").getMessage()
      });
    }
  }, {
    key: "initNewPolicie",
    value: function initNewPolicie(_this) {
      var _this2 = this;

      var identity = _this.props.identity;
      var search = _common__WEBPACK_IMPORTED_MODULE_3__["Utils"].fromQueryString();

      var custId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(search, "custId", "");

      var scId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(search, "scId", "");

      var maId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(search, "maId", "");

      var policyId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(search, "policyId", "");

      _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].post("/policies/new", {
        customerId: custId,
        scId: scId,
        maId: maId,
        itntCode: identity.itntCode,
        productCode: identity.prdtCode.toUpperCase(),
        otherParams: {
          policyId: policyId
        }
      }).then(function (res) {
        var respData = res.body.respData;

        _this2.getCartNew(_this, respData);
      }).catch(function (error) {
        return _this.setState({
          initialized: true
        });
      });
    }
  }, {
    key: "getCartNew",
    value: function getCartNew(_this, policy) {
      _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_3__["Apis"].CART_NEW, policy).then(function (res) {
        var respData = res.body.respData;
        console.log("1111111");

        _this.mergePolicyToUIModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, respData));

        _this.setState({
          initialized: true
        }, function () {
          console.log("0000000");
          _common__WEBPACK_IMPORTED_MODULE_3__["Utils"].bodyScale();
        });
      }).catch(function (error) {
        return _this.setState({
          initialized: true
        });
      });
    }
  }, {
    key: "loadPolicie",
    value: function loadPolicie(_this) {
      var identity = _this.props.identity;
      _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].get(_common__WEBPACK_IMPORTED_MODULE_3__["Apis"].CART_POLICIES_POLICYID.replace(":policyId", identity && identity.policyId), {}).then(function (res) {
        var respData = res.body.respData;

        _this.mergePolicyToUIModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, respData));

        _this.setState({
          initialized: true
        }, function () {
          _common__WEBPACK_IMPORTED_MODULE_3__["Utils"].bodyScale();
        });
      }).catch(function (error) {
        return _this.setState({
          initialized: true
        });
      });
    }
  }, {
    key: "clonePolicie",
    value: function clonePolicie(_this, policyId) {
      _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].post("/cart/clone/".concat(policyId), {}).then(function (res) {
        var respData = res.body.respData;

        _this.mergePolicyToUIModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, respData));

        _this.setState({
          initialized: true
        }, function () {
          _common__WEBPACK_IMPORTED_MODULE_3__["Utils"].bodyScale();
        });
      }).catch(function (error) {
        return _this.setState({
          initialized: true
        });
      });
    }
  }, {
    key: "showRidersNextStep",
    value: function showRidersNextStep(_this, step1, step2) {
      var model = _this.props.model || {};
      var showRiders = model.showRiders;

      if (showRiders) {
        Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_6__["pageTo"])(_this, step1);
      } else {
        Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_6__["pageTo"])(_this, step2);
      }
    }
  }, {
    key: "showRidersPrevStep",
    value: function showRidersPrevStep(_this, step1, step2) {
      var model = _this.props.model || {};
      var showRiders = model.showRiders;

      if (showRiders) {
        Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_6__["pageTo"])(_this, step1);
      } else {
        Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_6__["pageTo"])(_this, step2);
      }
    }
  }, {
    key: "isShowRiders",
    value: function isShowRiders(_this) {
      var model = _this.props.model || {};
      var showRiders = model.showRiders;
      return showRiders;
    }
  }, {
    key: "isShowBusiness",
    value: function isShowBusiness(model, dataFixed) {
      var propName = dataFixed ? "".concat(dataFixed, ".maId") : "maId";

      var maId = lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(model, propName);

      return lodash__WEBPACK_IMPORTED_MODULE_5___default.a.isEmpty(maId);
    }
  }]);

  return QuoteCommon;
}();

/* harmony default export */ __webpack_exports__["default"] = (new QuoteCommon());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tbW9uL2luZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL2NvbW1vbi9pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IG5vdGlmaWNhdGlvbiB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBwYWdlVG8gfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L3dpemFyZFwiO1xuXG5jbGFzcyBRdW90ZUNvbW1vbiB7XG4gIHRpcFNhdmVNc2coKSB7XG4gICAgbm90aWZpY2F0aW9uLnN1Y2Nlc3Moe1xuICAgICAgbWVzc2FnZTogTGFuZ3VhZ2UuZW4oXCJTYXZlZCBzdWNjZXNzZnVsbHlcIilcbiAgICAgICAgLnRoYWkoXCLguJrguLHguJnguJfguLbguIHguITguLPguJ7guLnguJTguKrguLPguYDguKPguYfguIjguYHguKXguYnguKdcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKSxcbiAgICB9KTtcbiAgfVxuXG4gIGluaXROZXdQb2xpY2llKF90aGlzOiBhbnkpIHtcbiAgICBjb25zdCB7IGlkZW50aXR5IH0gPSBfdGhpcy5wcm9wcztcbiAgICBjb25zdCBzZWFyY2ggPSBVdGlscy5mcm9tUXVlcnlTdHJpbmcoKTtcbiAgICBjb25zdCBjdXN0SWQgPSBfLmdldChzZWFyY2gsIFwiY3VzdElkXCIsIFwiXCIpO1xuICAgIGNvbnN0IHNjSWQgPSBfLmdldChzZWFyY2gsIFwic2NJZFwiLCBcIlwiKTtcbiAgICBjb25zdCBtYUlkID0gXy5nZXQoc2VhcmNoLCBcIm1hSWRcIiwgXCJcIik7XG4gICAgY29uc3QgcG9saWN5SWQgPSBfLmdldChzZWFyY2gsIFwicG9saWN5SWRcIiwgXCJcIik7XG5cblxuICAgIEFqYXgucG9zdChgL3BvbGljaWVzL25ld2AsIHtcbiAgICAgIGN1c3RvbWVySWQ6IGN1c3RJZCxcbiAgICAgIHNjSWQsXG4gICAgICBtYUlkLFxuICAgICAgaXRudENvZGU6IGlkZW50aXR5IS5pdG50Q29kZSxcbiAgICAgIHByb2R1Y3RDb2RlOiBpZGVudGl0eSEucHJkdENvZGUudG9VcHBlckNhc2UoKSxcbiAgICAgIG90aGVyUGFyYW1zOiB7XG4gICAgICAgIHBvbGljeUlkLFxuICAgICAgfSxcbiAgICB9KVxuICAgICAgLnRoZW4ocmVzID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzLmJvZHk7XG4gICAgICAgIHRoaXMuZ2V0Q2FydE5ldyhfdGhpcywgcmVzcERhdGEpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiBfdGhpcy5zZXRTdGF0ZSh7IGluaXRpYWxpemVkOiB0cnVlIH0pKTtcbiAgfVxuXG4gIGdldENhcnROZXcoX3RoaXM6IGFueSwgcG9saWN5OiBhbnkpIHtcbiAgICBBamF4LnBvc3QoQXBpcy5DQVJUX05FVywgcG9saWN5KS50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzLmJvZHk7XG4gICAgICBjb25zb2xlLmxvZyhcIjExMTExMTFcIik7XG5cbiAgICAgIF90aGlzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHtcbiAgICAgICAgLi4ucmVzcERhdGEsXG4gICAgICB9KTtcbiAgICAgIF90aGlzLnNldFN0YXRlKHsgaW5pdGlhbGl6ZWQ6IHRydWUgfSwgKCkgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhcIjAwMDAwMDBcIik7XG4gICAgICAgIFV0aWxzLmJvZHlTY2FsZSgpO1xuICAgICAgfSk7XG4gICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiBfdGhpcy5zZXRTdGF0ZSh7IGluaXRpYWxpemVkOiB0cnVlIH0pKTtcbiAgfVxuXG4gIGxvYWRQb2xpY2llKF90aGlzOiBhbnkpIHtcbiAgICBjb25zdCB7IGlkZW50aXR5IH0gPSBfdGhpcy5wcm9wcztcbiAgICBBamF4LmdldChBcGlzLkNBUlRfUE9MSUNJRVNfUE9MSUNZSUQucmVwbGFjZShcIjpwb2xpY3lJZFwiLCAoaWRlbnRpdHkgJiYgaWRlbnRpdHkucG9saWN5SWQpIGFzIGFueSksIHt9KVxuICAgICAgLnRoZW4ocmVzID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzLmJvZHk7XG4gICAgICAgIF90aGlzLm1lcmdlUG9saWN5VG9VSU1vZGVsKHtcbiAgICAgICAgICAuLi5yZXNwRGF0YSxcbiAgICAgICAgfSk7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHsgaW5pdGlhbGl6ZWQ6IHRydWUgfSwgKCkgPT4ge1xuICAgICAgICAgIFV0aWxzLmJvZHlTY2FsZSgpO1xuICAgICAgICB9KTtcbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4gX3RoaXMuc2V0U3RhdGUoeyBpbml0aWFsaXplZDogdHJ1ZSB9KSk7XG4gIH1cblxuICBjbG9uZVBvbGljaWUoX3RoaXM6IGFueSwgcG9saWN5SWQ6IHN0cmluZykge1xuICAgIEFqYXgucG9zdChgL2NhcnQvY2xvbmUvJHtwb2xpY3lJZH1gLCB7fSlcbiAgICAgIC50aGVuKHJlcyA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlcy5ib2R5O1xuICAgICAgICBfdGhpcy5tZXJnZVBvbGljeVRvVUlNb2RlbCh7XG4gICAgICAgICAgLi4ucmVzcERhdGEsXG4gICAgICAgIH0pO1xuICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7IGluaXRpYWxpemVkOiB0cnVlIH0sICgpID0+IHtcbiAgICAgICAgICBVdGlscy5ib2R5U2NhbGUoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KVxuICAgICAgLmNhdGNoKGVycm9yID0+IF90aGlzLnNldFN0YXRlKHsgaW5pdGlhbGl6ZWQ6IHRydWUgfSkpO1xuICB9XG5cbiAgc2hvd1JpZGVyc05leHRTdGVwKF90aGlzOiBhbnksIHN0ZXAxOiBzdHJpbmcsIHN0ZXAyOiBzdHJpbmcpIHtcbiAgICBsZXQgbW9kZWwgPSBfdGhpcy5wcm9wcy5tb2RlbCB8fCB7fTtcbiAgICBjb25zdCB7IHNob3dSaWRlcnMgfSA9IG1vZGVsO1xuICAgIGlmIChzaG93UmlkZXJzKSB7XG4gICAgICBwYWdlVG8oX3RoaXMsIHN0ZXAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFnZVRvKF90aGlzLCBzdGVwMik7XG4gICAgfVxuICB9XG5cbiAgc2hvd1JpZGVyc1ByZXZTdGVwKF90aGlzOiBhbnksIHN0ZXAxOiBzdHJpbmcsIHN0ZXAyOiBzdHJpbmcpIHtcbiAgICBsZXQgbW9kZWwgPSBfdGhpcy5wcm9wcy5tb2RlbCB8fCB7fTtcbiAgICBjb25zdCB7IHNob3dSaWRlcnMgfSA9IG1vZGVsO1xuICAgIGlmIChzaG93UmlkZXJzKSB7XG4gICAgICBwYWdlVG8oX3RoaXMsIHN0ZXAxKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGFnZVRvKF90aGlzLCBzdGVwMik7XG4gICAgfVxuICB9XG5cbiAgaXNTaG93UmlkZXJzKF90aGlzOiBhbnkpIHtcbiAgICBsZXQgbW9kZWwgPSBfdGhpcy5wcm9wcy5tb2RlbCB8fCB7fTtcbiAgICBjb25zdCB7IHNob3dSaWRlcnMgfSA9IG1vZGVsO1xuICAgIHJldHVybiBzaG93UmlkZXJzO1xuICB9XG5cbiAgaXNTaG93QnVzaW5lc3MobW9kZWw6IGFueSwgZGF0YUZpeGVkPzogYW55KSB7XG4gICAgY29uc3QgcHJvcE5hbWUgPSBkYXRhRml4ZWQgPyBgJHtkYXRhRml4ZWR9Lm1hSWRgIDogXCJtYUlkXCI7XG4gICAgY29uc3QgbWFJZCA9IF8uZ2V0KG1vZGVsLCBwcm9wTmFtZSk7XG4gICAgcmV0dXJuIF8uaXNFbXB0eShtYUlkKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgUXVvdGVDb21tb24oKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUtBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBTkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7OztBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/common/index.tsx
