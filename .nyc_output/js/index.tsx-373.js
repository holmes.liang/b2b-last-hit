__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _user_settings_my_website_home_components_phone__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../user-settings/my-website/home/components/phone */ "./src/app/desk/user-settings/my-website/home/components/phone.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var react_quill__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-quill */ "./node_modules/react-quill/lib/index.js");
/* harmony import */ var react_quill__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_quill__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_15__);








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/react-quill/index.tsx";

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n          .ant-form-item-control-wrapper, .ant-col-13 {\n            width: 100%\n          }\n          .ant-form-item-label, .ant-col-6 {\n            margin-bottom: 10px;\n            width: 100%\n          }\n        "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n            text-align: left !important;\n          "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n        margin-bottom: 10px;\n        .ant-form-item-label {\n          text-transform: uppercase;\n        }\n        .col-label {\n          text-transform: uppercase;\n          ", "\n          &:after {\n            content: ' ';\n            position: relative;\n            top: -0.5px;\n            margin: 0 8px 0 2px;\n          }  \n           label {\n              line-height: 40px;\n              text-transform: uppercase;\n              ::after {\n                  content: '';\n                  position: relative;\n                  top: -0.5px;\n                  margin: 0 0 0 2px;\n              }\n              i {\n                  display: inline-block;\n                  margin-right: 4px;\n                  color: #f5222d;\n                  font-size: 14px;\n                  font-family: SimSun, sans-serif;\n                  line-height: 1;\n              }\n           }\n        }\n        .ql-editor {\n          min-height: 200px;\n        }\n        ", "\n      "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_7__["default"])(["\n  .ql-container {\n    height: calc(100% - 43px);\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var toolbarContainer = [[{
  "header": [1, 2, false]
}], ["bold", "italic", "underline", "strike", "blockquote"], [{
  "list": "ordered"
}, {
  "list": "bullet"
}, {
  "indent": "-1"
}, {
  "indent": "+1"
}], ["link", "image"], ["clean"]];
var HeigthDiv = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject());

var ReactQuillContent =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(ReactQuillContent, _ModelWidget);

  function ReactQuillContent(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, ReactQuillContent);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ReactQuillContent).call(this, props, context));
    _this.reactQuillRef = {};
    _this.quillEditor = void 0;
    _this.inputUploader = void 0;

    _this.setRef = function (ref) {
      _this.reactQuillRef = ref;
    };

    _this.inputUploader = _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createRef();
    _this.imageHandler = _this.imageHandler.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_3__["default"])(_this));
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(ReactQuillContent, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ReactQuillContent.prototype), "initState", this).call(this), {
        text: this.props.textValue
      });
    }
  }, {
    key: "requestFullScreen",
    value: function requestFullScreen() {
      var dom = react_dom__WEBPACK_IMPORTED_MODULE_15___default.a.findDOMNode(this.reactQuillRef);

      if (dom) {
        if (dom.requestFullscreen) {
          dom.requestFullscreen();
        } else if (dom.mozRequestFullScreen) {
          dom.mozRequestFullScreen();
        } else if (dom.webkitRequestFullScreen) {
          dom.webkitRequestFullScreen();
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.BoxContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("input", {
        type: "file",
        ref: this.inputUploader,
        accept: "image/*",
        style: {
          display: "none"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Row"], {
        className: "form-react-quill-have-bottom",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      }, this.props.showLabel ? _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 6,
        className: "col-label",
        style: {
          textAlign: "right",
          color: "#9e9e9e"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, this.props.required && _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, "*"), this.props.label || "News Content")), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 13,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(react_quill__WEBPACK_IMPORTED_MODULE_14___default.a, {
        value: this.props.textValue,
        modules: {
          toolbar: {
            container: toolbarContainer,
            handlers: {
              image: this.imageHandler
            }
          }
        },
        id: this.props.id,
        ref: this.setRef,
        onChange: function onChange(value) {
          _this2.props.onChangeContext && _this2.props.onChangeContext(value);

          _this2.setState({
            text: value
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        style: {
          textAlign: "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        style: {
          marginRight: 5
        },
        className: "iconfont icon-full",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("a", {
        onClick: this.requestFullScreen.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, "Edit In Full Screen")))) : _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_13__["Col"], {
        span: 24,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(HeigthDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(react_quill__WEBPACK_IMPORTED_MODULE_14___default.a, {
        value: this.props.textValue,
        style: {
          height: "400px"
        },
        modules: {
          toolbar: {
            container: toolbarContainer,
            handlers: {
              image: this.imageHandler
            }
          }
        },
        ref: this.setRef,
        onChange: function onChange(value) {
          _this2.props.onChangeContext && _this2.props.onChangeContext(value);

          _this2.setState({
            text: value
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("div", {
        style: {
          textAlign: "right"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("i", {
        style: {
          marginRight: 5
        },
        className: "iconfont icon-full",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement("a", {
        onClick: this.requestFullScreen.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 151
        },
        __self: this
      }, "Edit In Full Screen"))))));
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          index = _this$props.index,
          model = _this$props.model;

      if (index || index === 0) {
        this.setState({
          text: lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(model, "newsContent")
        }, function () {});
      }
    }
  }, {
    key: "imageHandler",
    value: function imageHandler() {
      var _this3 = this;

      this.quillEditor = this.reactQuillRef.current.getEditor();

      this.inputUploader.current.onchange = function () {
        var file = (_this3.inputUploader.current.files || []).length > 0 ? _this3.inputUploader.current.files[0] : "";
        var formData = new FormData();
        formData.append("file", file);
        _common__WEBPACK_IMPORTED_MODULE_8__["Ajax"].post("/blob/files", formData, {
          headers: {
            "Content-Type": false
          }
        }).then(function (res) {
          var range = _this3.quillEditor.getSelection();

          var id = (res.body || {}).respData;
          var link = _common__WEBPACK_IMPORTED_MODULE_8__["Ajax"].getServiceLocation("/storage/".concat(id));

          _this3.quillEditor.insertEmbed(range.index, "image", link);
        });
      };

      this.inputUploader.current.click();
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div(_templateObject2(), _user_settings_my_website_home_components_phone__WEBPACK_IMPORTED_MODULE_11__["Phone"].only(_templateObject3()), _user_settings_my_website_home_components_phone__WEBPACK_IMPORTED_MODULE_11__["Phone"].only(_templateObject4()))
      };
    }
  }]);

  return ReactQuillContent;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

ReactQuillContent.defaultProps = {
  showLabel: true,
  required: false
};
/* harmony default export */ __webpack_exports__["default"] = (ReactQuillContent);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3JlYWN0LXF1aWxsL2luZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9yZWFjdC1xdWlsbC9pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWpheCB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgUGhvbmUgfSBmcm9tIFwiLi4vLi4vdXNlci1zZXR0aW5ncy9teS13ZWJzaXRlL2hvbWUvY29tcG9uZW50cy9waG9uZVwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IFJlYWN0UXVpbGwgZnJvbSBcInJlYWN0LXF1aWxsXCI7XG5pbXBvcnQgUmVhY3RET00gZnJvbSBcInJlYWN0LWRvbVwiO1xuXG50eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xudHlwZSBJQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcblxudHlwZSBVcGxvYWRTdGF0ZSA9IHtcbiAgdGV4dDogc3RyaW5nO1xufTtcblxuZXhwb3J0IHR5cGUgVXBsb2FkUHJvcHMgPSB7XG4gIG1vZGVsPzogYW55O1xuICBpbmRleD86IG51bWJlcjtcbiAgb25DaGFuZ2VDb250ZXh0OiBhbnk7XG4gIHRleHRWYWx1ZTogYW55O1xuICBzaG93TGFiZWw/OiBib29sZWFuO1xuICBsYWJlbD86IGJvb2xlYW47XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbiAgLy8gaWQ/OiBhbnk7XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcbmNvbnN0IHRvb2xiYXJDb250YWluZXIgPSBbXG4gIFt7IFwiaGVhZGVyXCI6IFsxLCAyLCBmYWxzZV0gfV0sXG4gIFtcImJvbGRcIiwgXCJpdGFsaWNcIiwgXCJ1bmRlcmxpbmVcIiwgXCJzdHJpa2VcIiwgXCJibG9ja3F1b3RlXCJdLFxuICBbeyBcImxpc3RcIjogXCJvcmRlcmVkXCIgfSwgeyBcImxpc3RcIjogXCJidWxsZXRcIiB9LCB7IFwiaW5kZW50XCI6IFwiLTFcIiB9LCB7IFwiaW5kZW50XCI6IFwiKzFcIiB9XSxcbiAgW1wibGlua1wiLCBcImltYWdlXCJdLFxuICBbXCJjbGVhblwiXSxcbl07XG5cbmNvbnN0IEhlaWd0aERpdiA9IFN0eWxlZC5kaXZgXG4gIC5xbC1jb250YWluZXIge1xuICAgIGhlaWdodDogY2FsYygxMDAlIC0gNDNweCk7XG4gIH1cbmA7XG5cbmNsYXNzIFJlYWN0UXVpbGxDb250ZW50PFAgZXh0ZW5kcyBVcGxvYWRQcm9wcywgUyBleHRlbmRzIFVwbG9hZFN0YXRlLCBDIGV4dGVuZHMgSUNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICByZWFjdFF1aWxsUmVmOiBhbnkgPSB7fTtcbiAgcHJvdGVjdGVkIHF1aWxsRWRpdG9yOiBhbnk7XG4gIHByb3RlY3RlZCBpbnB1dFVwbG9hZGVyOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IFVwbG9hZFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICAgIHRoaXMuaW5wdXRVcGxvYWRlciA9IFJlYWN0LmNyZWF0ZVJlZigpO1xuICAgIHRoaXMuaW1hZ2VIYW5kbGVyID0gdGhpcy5pbWFnZUhhbmRsZXIuYmluZCh0aGlzKTtcbiAgfVxuXG4gIHNldFJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgIHRoaXMucmVhY3RRdWlsbFJlZiA9IHJlZjtcbiAgfTtcblxuICBzdGF0aWMgZGVmYXVsdFByb3BzID0ge1xuICAgIHNob3dMYWJlbDogdHJ1ZSxcbiAgICByZXF1aXJlZDogZmFsc2UsXG4gIH07XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgdGV4dDogdGhpcy5wcm9wcy50ZXh0VmFsdWUsXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHJlcXVlc3RGdWxsU2NyZWVuKCkge1xuICAgIGNvbnN0IGRvbTogYW55ID0gUmVhY3RET00uZmluZERPTU5vZGUodGhpcy5yZWFjdFF1aWxsUmVmKTtcblxuICAgIGlmIChkb20pIHtcbiAgICAgIGlmIChkb20ucmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgICAgZG9tLnJlcXVlc3RGdWxsc2NyZWVuKCk7XG4gICAgICB9IGVsc2UgaWYgKGRvbS5tb3pSZXF1ZXN0RnVsbFNjcmVlbikge1xuICAgICAgICBkb20ubW96UmVxdWVzdEZ1bGxTY3JlZW4oKTtcbiAgICAgIH0gZWxzZSBpZiAoZG9tLndlYmtpdFJlcXVlc3RGdWxsU2NyZWVuKSB7XG4gICAgICAgIGRvbS53ZWJraXRSZXF1ZXN0RnVsbFNjcmVlbigpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hDb250ZW50PlxuICAgICAgICA8aW5wdXQgdHlwZT1cImZpbGVcIiByZWY9e3RoaXMuaW5wdXRVcGxvYWRlcn0gYWNjZXB0PVwiaW1hZ2UvKlwiIHN0eWxlPXt7IGRpc3BsYXk6IFwibm9uZVwiIH19Lz5cbiAgICAgICAgPFJvdyBjbGFzc05hbWU9e1wiZm9ybS1yZWFjdC1xdWlsbC1oYXZlLWJvdHRvbVwifT5cbiAgICAgICAgICB7dGhpcy5wcm9wcy5zaG93TGFiZWwgP1xuICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgPENvbCBzcGFuPXs2fSBjbGFzc05hbWU9XCJjb2wtbGFiZWxcIiBzdHlsZT17e1xuICAgICAgICAgICAgICAgIHRleHRBbGlnbjogXCJyaWdodFwiLFxuICAgICAgICAgICAgICAgIGNvbG9yOiBcIiM5ZTllOWVcIixcbiAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICAgICAgPGxhYmVsPnsodGhpcy5wcm9wcy5yZXF1aXJlZCkgJiYgKDxpPio8L2k+KX17dGhpcy5wcm9wcy5sYWJlbCB8fCBcIk5ld3MgQ29udGVudFwifTwvbGFiZWw+XG4gICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICA8Q29sIHNwYW49ezEzfT5cbiAgICAgICAgICAgICAgICA8UmVhY3RRdWlsbCB2YWx1ZT17dGhpcy5wcm9wcy50ZXh0VmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbW9kdWxlcz17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9vbGJhcjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250YWluZXI6IHRvb2xiYXJDb250YWluZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U6IHRoaXMuaW1hZ2VIYW5kbGVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPXt0aGlzLnByb3BzLmlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17dGhpcy5zZXRSZWZ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlQ29udGV4dCAmJiB0aGlzLnByb3BzLm9uQ2hhbmdlQ29udGV4dCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdGV4dDogdmFsdWUgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0vPlxuICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIHRleHRBbGlnbjogXCJyaWdodFwiLFxuICAgICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgICAgPGkgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luUmlnaHQ6IDUsXG4gICAgICAgICAgICAgICAgICB9fSBjbGFzc05hbWU9e1wiaWNvbmZvbnQgaWNvbi1mdWxsXCJ9Lz5cbiAgICAgICAgICAgICAgICAgIDxhIG9uQ2xpY2s9e3RoaXMucmVxdWVzdEZ1bGxTY3JlZW4uYmluZCh0aGlzKX0+RWRpdCBJbiBGdWxsIFNjcmVlbjwvYT5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgIDpcbiAgICAgICAgICAgIDxDb2wgc3Bhbj17MjR9PlxuICAgICAgICAgICAgICA8SGVpZ3RoRGl2PlxuICAgICAgICAgICAgICAgIDxSZWFjdFF1aWxsIHZhbHVlPXt0aGlzLnByb3BzLnRleHRWYWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBcIjQwMHB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtb2R1bGVzPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b29sYmFyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogdG9vbGJhckNvbnRhaW5lcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWFnZTogdGhpcy5pbWFnZUhhbmRsZXIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVmPXt0aGlzLnNldFJlZn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMub25DaGFuZ2VDb250ZXh0ICYmIHRoaXMucHJvcHMub25DaGFuZ2VDb250ZXh0KHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyB0ZXh0OiB2YWx1ZSB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fS8+XG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgdGV4dEFsaWduOiBcInJpZ2h0XCIsXG4gICAgICAgICAgICAgICAgfX0+XG4gICAgICAgICAgICAgICAgICA8aSBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBtYXJnaW5SaWdodDogNSxcbiAgICAgICAgICAgICAgICAgIH19IGNsYXNzTmFtZT17XCJpY29uZm9udCBpY29uLWZ1bGxcIn0vPlxuICAgICAgICAgICAgICAgICAgPGEgb25DbGljaz17dGhpcy5yZXF1ZXN0RnVsbFNjcmVlbi5iaW5kKHRoaXMpfT5FZGl0IEluIEZ1bGwgU2NyZWVuPC9hPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0hlaWd0aERpdj5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgIH1cbiAgICAgICAgPC9Sb3c+XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgeyBpbmRleCwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgaWYgKGluZGV4IHx8IGluZGV4ID09PSAwKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdGV4dDogXy5nZXQobW9kZWwsIFwibmV3c0NvbnRlbnRcIiksXG4gICAgICB9LCAoKSA9PiB7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBpbWFnZUhhbmRsZXIoKSB7XG4gICAgdGhpcy5xdWlsbEVkaXRvciA9IHRoaXMucmVhY3RRdWlsbFJlZi5jdXJyZW50LmdldEVkaXRvcigpO1xuICAgIHRoaXMuaW5wdXRVcGxvYWRlci5jdXJyZW50Lm9uY2hhbmdlID0gKCkgPT4ge1xuICAgICAgY29uc3QgZmlsZSA9ICh0aGlzLmlucHV0VXBsb2FkZXIuY3VycmVudC5maWxlcyB8fCBbXSkubGVuZ3RoID4gMCA/ICh0aGlzLmlucHV0VXBsb2FkZXIuY3VycmVudC5maWxlcyBhcyBhbnkpWzBdIDogXCJcIjtcbiAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoXCJmaWxlXCIsIGZpbGUpO1xuICAgICAgQWpheC5wb3N0KFwiL2Jsb2IvZmlsZXNcIiwgZm9ybURhdGEsIHtcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IGZhbHNlLFxuICAgICAgICB9LFxuICAgICAgfSkudGhlbigocmVzKSA9PiB7XG4gICAgICAgIGNvbnN0IHJhbmdlID0gdGhpcy5xdWlsbEVkaXRvci5nZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgY29uc3QgaWQgPSAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhO1xuICAgICAgICBjb25zdCBsaW5rID0gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oYC9zdG9yYWdlLyR7aWR9YCk7XG4gICAgICAgIHRoaXMucXVpbGxFZGl0b3IuaW5zZXJ0RW1iZWQocmFuZ2UuaW5kZXgsIFwiaW1hZ2VcIiwgbGluayk7XG4gICAgICB9KTtcbiAgICB9O1xuICAgIHRoaXMuaW5wdXRVcGxvYWRlci5jdXJyZW50LmNsaWNrKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEJveENvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIC5hbnQtZm9ybS1pdGVtLWxhYmVsIHtcbiAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICB9XG4gICAgICAgIC5jb2wtbGFiZWwge1xuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgJHtQaG9uZS5vbmx5YFxuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdCAhaW1wb3J0YW50O1xuICAgICAgICAgIGB9XG4gICAgICAgICAgJjphZnRlciB7XG4gICAgICAgICAgICBjb250ZW50OiAnICc7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICB0b3A6IC0wLjVweDtcbiAgICAgICAgICAgIG1hcmdpbjogMCA4cHggMCAycHg7XG4gICAgICAgICAgfSAgXG4gICAgICAgICAgIGxhYmVsIHtcbiAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICAgIDo6YWZ0ZXIge1xuICAgICAgICAgICAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICB0b3A6IC0wLjVweDtcbiAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCAwIDAgMnB4O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gICAgICAgICAgICAgICAgICBjb2xvcjogI2Y1MjIyZDtcbiAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBTaW1TdW4sIHNhbnMtc2VyaWY7XG4gICAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLnFsLWVkaXRvciB7XG4gICAgICAgICAgbWluLWhlaWdodDogMjAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgJHtQaG9uZS5vbmx5YFxuICAgICAgICAgIC5hbnQtZm9ybS1pdGVtLWNvbnRyb2wtd3JhcHBlciwgLmFudC1jb2wtMTMge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCVcbiAgICAgICAgICB9XG4gICAgICAgICAgLmFudC1mb3JtLWl0ZW0tbGFiZWwsIC5hbnQtY29sLTYge1xuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlXG4gICAgICAgICAgfVxuICAgICAgICBgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgUmVhY3RRdWlsbENvbnRlbnQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBcUJBO0FBQ0E7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBS0E7Ozs7O0FBS0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQU9BO0FBQ0E7QUFDQTtBQVBBO0FBQ0E7QUFIQTtBQUlBO0FBQ0E7OztBQVVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFEQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQWRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFEQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFEQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFEQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBa0RBOzs7O0FBck1BO0FBQ0E7QUFEQTtBQWdCQTtBQUNBO0FBRkE7QUF5TEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/react-quill/index.tsx
