__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_checkbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-checkbox */ "./node_modules/rc-checkbox/es/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};










var Checkbox =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Checkbox, _React$Component);

  function Checkbox() {
    var _this;

    _classCallCheck(this, Checkbox);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Checkbox).apply(this, arguments));

    _this.saveCheckbox = function (node) {
      _this.rcCheckbox = node;
    };

    _this.renderCheckbox = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props,
          context = _assertThisInitialize.context;

      var customizePrefixCls = props.prefixCls,
          className = props.className,
          children = props.children,
          indeterminate = props.indeterminate,
          style = props.style,
          onMouseEnter = props.onMouseEnter,
          onMouseLeave = props.onMouseLeave,
          restProps = __rest(props, ["prefixCls", "className", "children", "indeterminate", "style", "onMouseEnter", "onMouseLeave"]);

      var checkboxGroup = context.checkboxGroup;
      var prefixCls = getPrefixCls('checkbox', customizePrefixCls);

      var checkboxProps = _extends({}, restProps);

      if (checkboxGroup) {
        checkboxProps.onChange = function () {
          if (restProps.onChange) {
            restProps.onChange.apply(restProps, arguments);
          }

          checkboxGroup.toggleOption({
            label: children,
            value: props.value
          });
        };

        checkboxProps.name = checkboxGroup.name;
        checkboxProps.checked = checkboxGroup.value.indexOf(props.value) !== -1;
        checkboxProps.disabled = props.disabled || checkboxGroup.disabled;
      }

      var classString = classnames__WEBPACK_IMPORTED_MODULE_3___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-wrapper"), true), _defineProperty(_classNames, "".concat(prefixCls, "-wrapper-checked"), checkboxProps.checked), _defineProperty(_classNames, "".concat(prefixCls, "-wrapper-disabled"), checkboxProps.disabled), _classNames));
      var checkboxClass = classnames__WEBPACK_IMPORTED_MODULE_3___default()(_defineProperty({}, "".concat(prefixCls, "-indeterminate"), indeterminate));
      return (// eslint-disable-next-line jsx-a11y/label-has-associated-control
        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("label", {
          className: classString,
          style: style,
          onMouseEnter: onMouseEnter,
          onMouseLeave: onMouseLeave
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_checkbox__WEBPACK_IMPORTED_MODULE_4__["default"], _extends({}, checkboxProps, {
          prefixCls: prefixCls,
          className: checkboxClass,
          ref: _this.saveCheckbox
        })), children !== undefined && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", null, children))
      );
    };

    return _this;
  }

  _createClass(Checkbox, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var value = this.props.value;

      var _ref2 = this.context || {},
          _ref2$checkboxGroup = _ref2.checkboxGroup,
          checkboxGroup = _ref2$checkboxGroup === void 0 ? {} : _ref2$checkboxGroup;

      if (checkboxGroup.registerValue) {
        checkboxGroup.registerValue(value);
      }

      Object(_util_warning__WEBPACK_IMPORTED_MODULE_7__["default"])('checked' in this.props || (this.context || {}).checkboxGroup || !('value' in this.props), 'Checkbox', '`value` is not validate prop, do you mean `checked`?');
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps, nextState, nextContext) {
      return !shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(this.props, nextProps) || !shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(this.state, nextState) || !shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(this.context.checkboxGroup, nextContext.checkboxGroup);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(_ref3) {
      var prevValue = _ref3.value;
      var value = this.props.value;

      var _ref4 = this.context || {},
          _ref4$checkboxGroup = _ref4.checkboxGroup,
          checkboxGroup = _ref4$checkboxGroup === void 0 ? {} : _ref4$checkboxGroup;

      if (value !== prevValue && checkboxGroup.registerValue && checkboxGroup.cancelValue) {
        checkboxGroup.cancelValue(prevValue);
        checkboxGroup.registerValue(value);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var value = this.props.value;

      var _ref5 = this.context || {},
          _ref5$checkboxGroup = _ref5.checkboxGroup,
          checkboxGroup = _ref5$checkboxGroup === void 0 ? {} : _ref5$checkboxGroup;

      if (checkboxGroup.cancelValue) {
        checkboxGroup.cancelValue(value);
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      this.rcCheckbox.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.rcCheckbox.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderCheckbox);
    }
  }]);

  return Checkbox;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Checkbox.__ANT_CHECKBOX = true;
Checkbox.defaultProps = {
  indeterminate: false
};
Checkbox.contextTypes = {
  checkboxGroup: prop_types__WEBPACK_IMPORTED_MODULE_1__["any"]
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(Checkbox);
/* harmony default export */ __webpack_exports__["default"] = (Checkbox);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jaGVja2JveC9DaGVja2JveC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvY2hlY2tib3gvQ2hlY2tib3guanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgUmNDaGVja2JveCBmcm9tICdyYy1jaGVja2JveCc7XG5pbXBvcnQgc2hhbGxvd0VxdWFsIGZyb20gJ3NoYWxsb3dlcXVhbCc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbmNsYXNzIENoZWNrYm94IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5zYXZlQ2hlY2tib3ggPSAobm9kZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5yY0NoZWNrYm94ID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJDaGVja2JveCA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByb3BzLCBjb250ZXh0IH0gPSB0aGlzO1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lLCBjaGlsZHJlbiwgaW5kZXRlcm1pbmF0ZSwgc3R5bGUsIG9uTW91c2VFbnRlciwgb25Nb3VzZUxlYXZlIH0gPSBwcm9wcywgcmVzdFByb3BzID0gX19yZXN0KHByb3BzLCBbXCJwcmVmaXhDbHNcIiwgXCJjbGFzc05hbWVcIiwgXCJjaGlsZHJlblwiLCBcImluZGV0ZXJtaW5hdGVcIiwgXCJzdHlsZVwiLCBcIm9uTW91c2VFbnRlclwiLCBcIm9uTW91c2VMZWF2ZVwiXSk7XG4gICAgICAgICAgICBjb25zdCB7IGNoZWNrYm94R3JvdXAgfSA9IGNvbnRleHQ7XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ2NoZWNrYm94JywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGNoZWNrYm94UHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCByZXN0UHJvcHMpO1xuICAgICAgICAgICAgaWYgKGNoZWNrYm94R3JvdXApIHtcbiAgICAgICAgICAgICAgICBjaGVja2JveFByb3BzLm9uQ2hhbmdlID0gKC4uLmFyZ3MpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3RQcm9wcy5vbkNoYW5nZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFByb3BzLm9uQ2hhbmdlKC4uLmFyZ3MpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrYm94R3JvdXAudG9nZ2xlT3B0aW9uKHsgbGFiZWw6IGNoaWxkcmVuLCB2YWx1ZTogcHJvcHMudmFsdWUgfSk7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBjaGVja2JveFByb3BzLm5hbWUgPSBjaGVja2JveEdyb3VwLm5hbWU7XG4gICAgICAgICAgICAgICAgY2hlY2tib3hQcm9wcy5jaGVja2VkID0gY2hlY2tib3hHcm91cC52YWx1ZS5pbmRleE9mKHByb3BzLnZhbHVlKSAhPT0gLTE7XG4gICAgICAgICAgICAgICAgY2hlY2tib3hQcm9wcy5kaXNhYmxlZCA9IHByb3BzLmRpc2FibGVkIHx8IGNoZWNrYm94R3JvdXAuZGlzYWJsZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBjbGFzc1N0cmluZyA9IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCB7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30td3JhcHBlcmBdOiB0cnVlLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXdyYXBwZXItY2hlY2tlZGBdOiBjaGVja2JveFByb3BzLmNoZWNrZWQsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30td3JhcHBlci1kaXNhYmxlZGBdOiBjaGVja2JveFByb3BzLmRpc2FibGVkLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBjaGVja2JveENsYXNzID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taW5kZXRlcm1pbmF0ZWBdOiBpbmRldGVybWluYXRlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGpzeC1hMTF5L2xhYmVsLWhhcy1hc3NvY2lhdGVkLWNvbnRyb2xcbiAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9e2NsYXNzU3RyaW5nfSBzdHlsZT17c3R5bGV9IG9uTW91c2VFbnRlcj17b25Nb3VzZUVudGVyfSBvbk1vdXNlTGVhdmU9e29uTW91c2VMZWF2ZX0+XG4gICAgICAgIDxSY0NoZWNrYm94IHsuLi5jaGVja2JveFByb3BzfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gY2xhc3NOYW1lPXtjaGVja2JveENsYXNzfSByZWY9e3RoaXMuc2F2ZUNoZWNrYm94fS8+XG4gICAgICAgIHtjaGlsZHJlbiAhPT0gdW5kZWZpbmVkICYmIDxzcGFuPntjaGlsZHJlbn08L3NwYW4+fVxuICAgICAgPC9sYWJlbD4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgY29uc3QgeyB2YWx1ZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgeyBjaGVja2JveEdyb3VwID0ge30gfSA9IHRoaXMuY29udGV4dCB8fCB7fTtcbiAgICAgICAgaWYgKGNoZWNrYm94R3JvdXAucmVnaXN0ZXJWYWx1ZSkge1xuICAgICAgICAgICAgY2hlY2tib3hHcm91cC5yZWdpc3RlclZhbHVlKHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICB3YXJuaW5nKCdjaGVja2VkJyBpbiB0aGlzLnByb3BzIHx8ICh0aGlzLmNvbnRleHQgfHwge30pLmNoZWNrYm94R3JvdXAgfHwgISgndmFsdWUnIGluIHRoaXMucHJvcHMpLCAnQ2hlY2tib3gnLCAnYHZhbHVlYCBpcyBub3QgdmFsaWRhdGUgcHJvcCwgZG8geW91IG1lYW4gYGNoZWNrZWRgPycpO1xuICAgIH1cbiAgICBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUsIG5leHRDb250ZXh0KSB7XG4gICAgICAgIHJldHVybiAoIXNoYWxsb3dFcXVhbCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpIHx8XG4gICAgICAgICAgICAhc2hhbGxvd0VxdWFsKHRoaXMuc3RhdGUsIG5leHRTdGF0ZSkgfHxcbiAgICAgICAgICAgICFzaGFsbG93RXF1YWwodGhpcy5jb250ZXh0LmNoZWNrYm94R3JvdXAsIG5leHRDb250ZXh0LmNoZWNrYm94R3JvdXApKTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkVXBkYXRlKHsgdmFsdWU6IHByZXZWYWx1ZSB9KSB7XG4gICAgICAgIGNvbnN0IHsgdmFsdWUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IHsgY2hlY2tib3hHcm91cCA9IHt9IH0gPSB0aGlzLmNvbnRleHQgfHwge307XG4gICAgICAgIGlmICh2YWx1ZSAhPT0gcHJldlZhbHVlICYmIGNoZWNrYm94R3JvdXAucmVnaXN0ZXJWYWx1ZSAmJiBjaGVja2JveEdyb3VwLmNhbmNlbFZhbHVlKSB7XG4gICAgICAgICAgICBjaGVja2JveEdyb3VwLmNhbmNlbFZhbHVlKHByZXZWYWx1ZSk7XG4gICAgICAgICAgICBjaGVja2JveEdyb3VwLnJlZ2lzdGVyVmFsdWUodmFsdWUpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICBjb25zdCB7IHZhbHVlIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCB7IGNoZWNrYm94R3JvdXAgPSB7fSB9ID0gdGhpcy5jb250ZXh0IHx8IHt9O1xuICAgICAgICBpZiAoY2hlY2tib3hHcm91cC5jYW5jZWxWYWx1ZSkge1xuICAgICAgICAgICAgY2hlY2tib3hHcm91cC5jYW5jZWxWYWx1ZSh2YWx1ZSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgZm9jdXMoKSB7XG4gICAgICAgIHRoaXMucmNDaGVja2JveC5mb2N1cygpO1xuICAgIH1cbiAgICBibHVyKCkge1xuICAgICAgICB0aGlzLnJjQ2hlY2tib3guYmx1cigpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQ2hlY2tib3h9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQ2hlY2tib3guX19BTlRfQ0hFQ0tCT1ggPSB0cnVlO1xuQ2hlY2tib3guZGVmYXVsdFByb3BzID0ge1xuICAgIGluZGV0ZXJtaW5hdGU6IGZhbHNlLFxufTtcbkNoZWNrYm94LmNvbnRleHRUeXBlcyA9IHtcbiAgICBjaGVja2JveEdyb3VwOiBQcm9wVHlwZXMuYW55LFxufTtcbnBvbHlmaWxsKENoZWNrYm94KTtcbmV4cG9ydCBkZWZhdWx0IENoZWNrYm94O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBS0E7QUFHQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSEE7QUF6QkE7QUFDQTtBQU5BO0FBcUNBO0FBQ0E7OztBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBR0E7OztBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBM0VBO0FBQ0E7QUE0RUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/checkbox/Checkbox.js
