__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_VALID", function() { return STATUS_VALID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATUS_INVALID", function() { return STATUS_INVALID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tenantType", function() { return tenantType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "enabledType", function() { return enabledType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateStateOptions", function() { return validateStateOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "executionStatus", function() { return executionStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "jobQueryStatus", function() { return jobQueryStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reportFrequencyOptions", function() { return reportFrequencyOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productLineOptions", function() { return productLineOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserTypes", function() { return BrowserTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientTypes", function() { return ClientTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientOSTypes", function() { return ClientOSTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NationTypes", function() { return NationTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_PASS", function() { return DEFAULT_PASS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DECISION_APPROVED", function() { return DECISION_APPROVED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DECISION_REJECT", function() { return DECISION_REJECT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DecisionOptions", function() { return DecisionOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnabledOptions", function() { return EnabledOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resultOfExecutionOptions", function() { return resultOfExecutionOptions; });
var STATUS_VALID = 'VD';
var STATUS_INVALID = 'IVD';
var tenantType = {
  AGENT: 'AGENT',
  BROKER: 'BROKER',
  D2C: 'D2C',
  INSURER: 'INSURER',
  PLATFORM: 'PLATFORM'
};
var enabledType = {
  Y: 'Y',
  N: 'N'
};
var validateStateOptions = [{
  label: 'Valid',
  value: STATUS_VALID
}, {
  label: 'Invalid',
  value: STATUS_INVALID
}];
var executionStatus = {
  PENDING: 'Pending',
  IN_PGRES: 'In Progress',
  SUCC: 'Successful',
  FAIL: 'Fail',
  TO_CONTINUE: 'To Be Continued'
};
var jobQueryStatus = {
  IN_PGRES: 'In Progress',
  SUCC: 'Successful',
  FAIL: 'Fail',
  KILLED: 'killed'
};
var reportFrequencyOptions = [{
  label: 'Daily',
  value: 'DAILY'
}, {
  label: 'Weekly',
  value: 'WEEKLY'
}, {
  label: 'Bi-weekly',
  value: 'BIWEEKLY'
}, {
  label: 'Monthly',
  value: 'MONTHLY'
}, {
  label: 'Quarterly',
  value: 'QUARTERLY'
}, {
  label: 'Half-yearly',
  value: 'HALF_YEARLY'
}, {
  label: 'Yearly',
  value: 'YEARLY'
}];
var productLineOptions = [{
  label: 'Motor',
  value: 'MOTOR'
}, {
  label: 'Travel',
  value: 'TRAVEL'
}, {
  label: 'Personal Accident',
  value: 'PA'
}, {
  label: 'Home',
  value: 'HOME'
}, {
  label: 'Foreign Maid',
  value: 'MAID'
}, {
  label: 'Health',
  value: 'HEALTH'
}, {
  label: 'Liability',
  value: 'LIABILITY'
}];
var BrowserTypes = {
  CHROME: 'Chrome',
  SAFARI: 'Safari',
  OPERA: 'Opera',
  FIREFOX: 'Firefox',
  MSEDGE: 'MS Edge',
  IE: 'MS IE',
  OTH: 'Unknow Browser'
};
var ClientTypes = {
  MBR: 'Mobile Browser',
  PCBR: 'PC Browser',
  MAPP: 'Mobile App',
  FUP: 'File Upload',
  API: 'API',
  OTH: 'Unknow Client'
};
var ClientOSTypes = {
  IOS: 'iOS',
  ANDROID: 'Android',
  WINPHONE: 'Windows Phone',
  MACOS: 'macOS',
  WIND: 'Windows',
  LINUX: 'Linux',
  UNIX: 'Unix',
  OTH: 'Unknow OS'
};
var NationTypes = [{
  label: 'Thai',
  value: '66'
}, {
  label: 'Singapore',
  value: '65'
}, {
  label: 'China',
  value: '86'
}];
var DEFAULT_PASS = 'PasswOrd';
var DECISION_APPROVED = '0';
var DECISION_REJECT = '1';
var DecisionOptions = [{
  label: 'Approve',
  value: DECISION_APPROVED
}, {
  label: 'Reject',
  value: DECISION_REJECT
}, {
  label: 'Send back with comments',
  value: '2'
}];
var EnabledOptions = [{
  label: 'Yes',
  value: enabledType.Y
}, {
  label: 'No',
  value: enabledType.N
}];
var resultOfExecutionOptions = [{
  label: 'Online',
  value: STATUS_VALID
}, {
  label: 'Invalidated',
  value: STATUS_INVALID
}];//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL29wdGlvbnMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL29wdGlvbnMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBTVEFUVVNfVkFMSUQgPSAnVkQnO1xuXG5leHBvcnQgY29uc3QgU1RBVFVTX0lOVkFMSUQgPSAnSVZEJztcblxuZXhwb3J0IGNvbnN0IHRlbmFudFR5cGUgPSB7XG4gIEFHRU5UOiAnQUdFTlQnLFxuICBCUk9LRVI6ICdCUk9LRVInLFxuICBEMkM6ICdEMkMnLFxuICBJTlNVUkVSOiAnSU5TVVJFUicsXG4gIFBMQVRGT1JNOiAnUExBVEZPUk0nLFxufTtcblxuZXhwb3J0IGNvbnN0IGVuYWJsZWRUeXBlID0ge1xuICBZOiAnWScsXG4gIE46ICdOJyxcbn07XG5cbmV4cG9ydCBjb25zdCB2YWxpZGF0ZVN0YXRlT3B0aW9ucyA9IFtcbiAgeyBsYWJlbDogJ1ZhbGlkJywgdmFsdWU6IFNUQVRVU19WQUxJRCB9LFxuICB7IGxhYmVsOiAnSW52YWxpZCcsIHZhbHVlOiBTVEFUVVNfSU5WQUxJRCB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IGV4ZWN1dGlvblN0YXR1cyA9IHtcbiAgUEVORElORzogJ1BlbmRpbmcnLFxuICBJTl9QR1JFUzogJ0luIFByb2dyZXNzJyxcbiAgU1VDQzogJ1N1Y2Nlc3NmdWwnLFxuICBGQUlMOiAnRmFpbCcsXG4gIFRPX0NPTlRJTlVFOiAnVG8gQmUgQ29udGludWVkJyxcbn07XG5cbmV4cG9ydCBjb25zdCBqb2JRdWVyeVN0YXR1cyA9IHtcbiAgSU5fUEdSRVM6ICdJbiBQcm9ncmVzcycsXG4gIFNVQ0M6ICdTdWNjZXNzZnVsJyxcbiAgRkFJTDogJ0ZhaWwnLFxuICBLSUxMRUQ6ICdraWxsZWQnLFxufTtcblxuZXhwb3J0IGNvbnN0IHJlcG9ydEZyZXF1ZW5jeU9wdGlvbnMgPSBbXG4gIHsgbGFiZWw6ICdEYWlseScsIHZhbHVlOiAnREFJTFknIH0sXG4gIHsgbGFiZWw6ICdXZWVrbHknLCB2YWx1ZTogJ1dFRUtMWScgfSxcbiAgeyBsYWJlbDogJ0JpLXdlZWtseScsIHZhbHVlOiAnQklXRUVLTFknIH0sXG4gIHsgbGFiZWw6ICdNb250aGx5JywgdmFsdWU6ICdNT05USExZJyB9LFxuICB7IGxhYmVsOiAnUXVhcnRlcmx5JywgdmFsdWU6ICdRVUFSVEVSTFknIH0sXG4gIHsgbGFiZWw6ICdIYWxmLXllYXJseScsIHZhbHVlOiAnSEFMRl9ZRUFSTFknIH0sXG4gIHsgbGFiZWw6ICdZZWFybHknLCB2YWx1ZTogJ1lFQVJMWScgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBwcm9kdWN0TGluZU9wdGlvbnMgPSBbXG4gIHsgbGFiZWw6ICdNb3RvcicsIHZhbHVlOiAnTU9UT1InIH0sXG4gIHsgbGFiZWw6ICdUcmF2ZWwnLCB2YWx1ZTogJ1RSQVZFTCcgfSxcbiAgeyBsYWJlbDogJ1BlcnNvbmFsIEFjY2lkZW50JywgdmFsdWU6ICdQQScgfSxcbiAgeyBsYWJlbDogJ0hvbWUnLCB2YWx1ZTogJ0hPTUUnIH0sXG4gIHsgbGFiZWw6ICdGb3JlaWduIE1haWQnLCB2YWx1ZTogJ01BSUQnIH0sXG4gIHsgbGFiZWw6ICdIZWFsdGgnLCB2YWx1ZTogJ0hFQUxUSCcgfSxcbiAgeyBsYWJlbDogJ0xpYWJpbGl0eScsIHZhbHVlOiAnTElBQklMSVRZJyB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IEJyb3dzZXJUeXBlcyA9IHtcbiAgQ0hST01FOiAnQ2hyb21lJyxcbiAgU0FGQVJJOiAnU2FmYXJpJyxcbiAgT1BFUkE6ICdPcGVyYScsXG4gIEZJUkVGT1g6ICdGaXJlZm94JyxcbiAgTVNFREdFOiAnTVMgRWRnZScsXG4gIElFOiAnTVMgSUUnLFxuICBPVEg6ICdVbmtub3cgQnJvd3NlcicsXG59O1xuXG5leHBvcnQgY29uc3QgQ2xpZW50VHlwZXMgPSB7XG4gIE1CUjogJ01vYmlsZSBCcm93c2VyJyxcbiAgUENCUjogJ1BDIEJyb3dzZXInLFxuICBNQVBQOiAnTW9iaWxlIEFwcCcsXG4gIEZVUDogJ0ZpbGUgVXBsb2FkJyxcbiAgQVBJOiAnQVBJJyxcbiAgT1RIOiAnVW5rbm93IENsaWVudCcsXG59O1xuXG5leHBvcnQgY29uc3QgQ2xpZW50T1NUeXBlcyA9IHtcbiAgSU9TOiAnaU9TJyxcbiAgQU5EUk9JRDogJ0FuZHJvaWQnLFxuICBXSU5QSE9ORTogJ1dpbmRvd3MgUGhvbmUnLFxuICBNQUNPUzogJ21hY09TJyxcbiAgV0lORDogJ1dpbmRvd3MnLFxuICBMSU5VWDogJ0xpbnV4JyxcbiAgVU5JWDogJ1VuaXgnLFxuICBPVEg6ICdVbmtub3cgT1MnLFxufTtcblxuZXhwb3J0IGNvbnN0IE5hdGlvblR5cGVzID0gW1xuICB7IGxhYmVsOiAnVGhhaScsIHZhbHVlOiAnNjYnIH0sXG4gIHsgbGFiZWw6ICdTaW5nYXBvcmUnLCB2YWx1ZTogJzY1JyB9LFxuICB7IGxhYmVsOiAnQ2hpbmEnLCB2YWx1ZTogJzg2JyB9LFxuXTtcblxuZXhwb3J0IGNvbnN0IERFRkFVTFRfUEFTUyA9ICdQYXNzd09yZCc7XG5cbmV4cG9ydCBjb25zdCBERUNJU0lPTl9BUFBST1ZFRCA9ICcwJztcbmV4cG9ydCBjb25zdCBERUNJU0lPTl9SRUpFQ1QgPSAnMSc7XG5cbmV4cG9ydCBjb25zdCBEZWNpc2lvbk9wdGlvbnMgPSBbXG4gIHsgbGFiZWw6ICdBcHByb3ZlJywgdmFsdWU6IERFQ0lTSU9OX0FQUFJPVkVEIH0sXG4gIHsgbGFiZWw6ICdSZWplY3QnLCB2YWx1ZTogREVDSVNJT05fUkVKRUNUIH0sXG4gIHsgbGFiZWw6ICdTZW5kIGJhY2sgd2l0aCBjb21tZW50cycsIHZhbHVlOiAnMicgfSxcbl07XG5cbmV4cG9ydCBjb25zdCBFbmFibGVkT3B0aW9ucyA9IFtcbiAgeyBsYWJlbDogJ1llcycsIHZhbHVlOiBlbmFibGVkVHlwZS5ZIH0sXG4gIHsgbGFiZWw6ICdObycsIHZhbHVlOiBlbmFibGVkVHlwZS5OIH0sXG5dO1xuXG5leHBvcnQgY29uc3QgcmVzdWx0T2ZFeGVjdXRpb25PcHRpb25zID0gW1xuICB7IGxhYmVsOiAnT25saW5lJywgdmFsdWU6IFNUQVRVU19WQUxJRCB9LFxuICB7IGxhYmVsOiAnSW52YWxpZGF0ZWQnLCB2YWx1ZTogU1RBVFVTX0lOVkFMSUQgfSxcbl07XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/options.tsx
