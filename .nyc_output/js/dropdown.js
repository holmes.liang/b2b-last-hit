__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Dropdown; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_dropdown__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-dropdown */ "./node_modules/rc-dropdown/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}








var Placements = Object(_util_type__WEBPACK_IMPORTED_MODULE_6__["tuple"])('topLeft', 'topCenter', 'topRight', 'bottomLeft', 'bottomCenter', 'bottomRight');

var Dropdown =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Dropdown, _React$Component);

  function Dropdown() {
    var _this;

    _classCallCheck(this, Dropdown);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Dropdown).apply(this, arguments));

    _this.renderOverlay = function (prefixCls) {
      // rc-dropdown already can process the function of overlay, but we have check logic here.
      // So we need render the element to check and pass back to rc-dropdown.
      var overlay = _this.props.overlay;
      var overlayNode;

      if (typeof overlay === 'function') {
        overlayNode = overlay();
      } else {
        overlayNode = overlay;
      }

      overlayNode = react__WEBPACK_IMPORTED_MODULE_0__["Children"].only(overlayNode);
      var overlayProps = overlayNode.props; // Warning if use other mode

      Object(_util_warning__WEBPACK_IMPORTED_MODULE_4__["default"])(!overlayProps.mode || overlayProps.mode === 'vertical', 'Dropdown', "mode=\"".concat(overlayProps.mode, "\" is not supported for Dropdown's Menu.")); // menu cannot be selectable in dropdown defaultly
      // menu should be focusable in dropdown defaultly

      var _overlayProps$selecta = overlayProps.selectable,
          selectable = _overlayProps$selecta === void 0 ? false : _overlayProps$selecta,
          _overlayProps$focusab = overlayProps.focusable,
          focusable = _overlayProps$focusab === void 0 ? true : _overlayProps$focusab;
      var expandIcon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-menu-submenu-arrow")
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_5__["default"], {
        type: "right",
        className: "".concat(prefixCls, "-menu-submenu-arrow-icon")
      }));
      var fixedModeOverlay = typeof overlayNode.type === 'string' ? overlay : react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](overlayNode, {
        mode: 'vertical',
        selectable: selectable,
        focusable: focusable,
        expandIcon: expandIcon
      });
      return fixedModeOverlay;
    };

    _this.renderDropDown = function (_ref) {
      var getContextPopupContainer = _ref.getPopupContainer,
          getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          children = _this$props.children,
          trigger = _this$props.trigger,
          disabled = _this$props.disabled,
          getPopupContainer = _this$props.getPopupContainer;
      var prefixCls = getPrefixCls('dropdown', customizePrefixCls);
      var child = react__WEBPACK_IMPORTED_MODULE_0__["Children"].only(children);
      var dropdownTrigger = react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](child, {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(child.props.className, "".concat(prefixCls, "-trigger")),
        disabled: disabled
      });
      var triggerActions = disabled ? [] : trigger;
      var alignPoint;

      if (triggerActions && triggerActions.indexOf('contextMenu') !== -1) {
        alignPoint = true;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_dropdown__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
        alignPoint: alignPoint
      }, _this.props, {
        prefixCls: prefixCls,
        getPopupContainer: getPopupContainer || getContextPopupContainer,
        transitionName: _this.getTransitionName(),
        trigger: triggerActions,
        overlay: function overlay() {
          return _this.renderOverlay(prefixCls);
        }
      }), dropdownTrigger);
    };

    return _this;
  }

  _createClass(Dropdown, [{
    key: "getTransitionName",
    value: function getTransitionName() {
      var _this$props2 = this.props,
          _this$props2$placemen = _this$props2.placement,
          placement = _this$props2$placemen === void 0 ? '' : _this$props2$placemen,
          transitionName = _this$props2.transitionName;

      if (transitionName !== undefined) {
        return transitionName;
      }

      if (placement.indexOf('top') >= 0) {
        return 'slide-down';
      }

      return 'slide-up';
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"], null, this.renderDropDown);
    }
  }]);

  return Dropdown;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Dropdown.defaultProps = {
  mouseEnterDelay: 0.15,
  mouseLeaveDelay: 0.1,
  placement: 'bottomLeft'
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9kcm9wZG93bi9kcm9wZG93bi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvZHJvcGRvd24vZHJvcGRvd24uanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSY0Ryb3Bkb3duIGZyb20gJ3JjLWRyb3Bkb3duJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IHR1cGxlIH0gZnJvbSAnLi4vX3V0aWwvdHlwZSc7XG5jb25zdCBQbGFjZW1lbnRzID0gdHVwbGUoJ3RvcExlZnQnLCAndG9wQ2VudGVyJywgJ3RvcFJpZ2h0JywgJ2JvdHRvbUxlZnQnLCAnYm90dG9tQ2VudGVyJywgJ2JvdHRvbVJpZ2h0Jyk7XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEcm9wZG93biBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMucmVuZGVyT3ZlcmxheSA9IChwcmVmaXhDbHMpID0+IHtcbiAgICAgICAgICAgIC8vIHJjLWRyb3Bkb3duIGFscmVhZHkgY2FuIHByb2Nlc3MgdGhlIGZ1bmN0aW9uIG9mIG92ZXJsYXksIGJ1dCB3ZSBoYXZlIGNoZWNrIGxvZ2ljIGhlcmUuXG4gICAgICAgICAgICAvLyBTbyB3ZSBuZWVkIHJlbmRlciB0aGUgZWxlbWVudCB0byBjaGVjayBhbmQgcGFzcyBiYWNrIHRvIHJjLWRyb3Bkb3duLlxuICAgICAgICAgICAgY29uc3QgeyBvdmVybGF5IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgbGV0IG92ZXJsYXlOb2RlO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBvdmVybGF5ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgb3ZlcmxheU5vZGUgPSBvdmVybGF5KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBvdmVybGF5Tm9kZSA9IG92ZXJsYXk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvdmVybGF5Tm9kZSA9IFJlYWN0LkNoaWxkcmVuLm9ubHkob3ZlcmxheU5vZGUpO1xuICAgICAgICAgICAgY29uc3Qgb3ZlcmxheVByb3BzID0gb3ZlcmxheU5vZGUucHJvcHM7XG4gICAgICAgICAgICAvLyBXYXJuaW5nIGlmIHVzZSBvdGhlciBtb2RlXG4gICAgICAgICAgICB3YXJuaW5nKCFvdmVybGF5UHJvcHMubW9kZSB8fCBvdmVybGF5UHJvcHMubW9kZSA9PT0gJ3ZlcnRpY2FsJywgJ0Ryb3Bkb3duJywgYG1vZGU9XCIke292ZXJsYXlQcm9wcy5tb2RlfVwiIGlzIG5vdCBzdXBwb3J0ZWQgZm9yIERyb3Bkb3duJ3MgTWVudS5gKTtcbiAgICAgICAgICAgIC8vIG1lbnUgY2Fubm90IGJlIHNlbGVjdGFibGUgaW4gZHJvcGRvd24gZGVmYXVsdGx5XG4gICAgICAgICAgICAvLyBtZW51IHNob3VsZCBiZSBmb2N1c2FibGUgaW4gZHJvcGRvd24gZGVmYXVsdGx5XG4gICAgICAgICAgICBjb25zdCB7IHNlbGVjdGFibGUgPSBmYWxzZSwgZm9jdXNhYmxlID0gdHJ1ZSB9ID0gb3ZlcmxheVByb3BzO1xuICAgICAgICAgICAgY29uc3QgZXhwYW5kSWNvbiA9ICg8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tbWVudS1zdWJtZW51LWFycm93YH0+XG4gICAgICAgIDxJY29uIHR5cGU9XCJyaWdodFwiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1tZW51LXN1Ym1lbnUtYXJyb3ctaWNvbmB9Lz5cbiAgICAgIDwvc3Bhbj4pO1xuICAgICAgICAgICAgY29uc3QgZml4ZWRNb2RlT3ZlcmxheSA9IHR5cGVvZiBvdmVybGF5Tm9kZS50eXBlID09PSAnc3RyaW5nJ1xuICAgICAgICAgICAgICAgID8gb3ZlcmxheVxuICAgICAgICAgICAgICAgIDogUmVhY3QuY2xvbmVFbGVtZW50KG92ZXJsYXlOb2RlLCB7XG4gICAgICAgICAgICAgICAgICAgIG1vZGU6ICd2ZXJ0aWNhbCcsXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGFibGUsXG4gICAgICAgICAgICAgICAgICAgIGZvY3VzYWJsZSxcbiAgICAgICAgICAgICAgICAgICAgZXhwYW5kSWNvbixcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBmaXhlZE1vZGVPdmVybGF5O1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckRyb3BEb3duID0gKHsgZ2V0UG9wdXBDb250YWluZXI6IGdldENvbnRleHRQb3B1cENvbnRhaW5lciwgZ2V0UHJlZml4Q2xzLCB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjaGlsZHJlbiwgdHJpZ2dlciwgZGlzYWJsZWQsIGdldFBvcHVwQ29udGFpbmVyLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnZHJvcGRvd24nLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3QgY2hpbGQgPSBSZWFjdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKTtcbiAgICAgICAgICAgIGNvbnN0IGRyb3Bkb3duVHJpZ2dlciA9IFJlYWN0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhjaGlsZC5wcm9wcy5jbGFzc05hbWUsIGAke3ByZWZpeENsc30tdHJpZ2dlcmApLFxuICAgICAgICAgICAgICAgIGRpc2FibGVkLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCB0cmlnZ2VyQWN0aW9ucyA9IGRpc2FibGVkID8gW10gOiB0cmlnZ2VyO1xuICAgICAgICAgICAgbGV0IGFsaWduUG9pbnQ7XG4gICAgICAgICAgICBpZiAodHJpZ2dlckFjdGlvbnMgJiYgdHJpZ2dlckFjdGlvbnMuaW5kZXhPZignY29udGV4dE1lbnUnKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICBhbGlnblBvaW50ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPFJjRHJvcGRvd24gYWxpZ25Qb2ludD17YWxpZ25Qb2ludH0gey4uLnRoaXMucHJvcHN9IHByZWZpeENscz17cHJlZml4Q2xzfSBnZXRQb3B1cENvbnRhaW5lcj17Z2V0UG9wdXBDb250YWluZXIgfHwgZ2V0Q29udGV4dFBvcHVwQ29udGFpbmVyfSB0cmFuc2l0aW9uTmFtZT17dGhpcy5nZXRUcmFuc2l0aW9uTmFtZSgpfSB0cmlnZ2VyPXt0cmlnZ2VyQWN0aW9uc30gb3ZlcmxheT17KCkgPT4gdGhpcy5yZW5kZXJPdmVybGF5KHByZWZpeENscyl9PlxuICAgICAgICB7ZHJvcGRvd25UcmlnZ2VyfVxuICAgICAgPC9SY0Ryb3Bkb3duPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGdldFRyYW5zaXRpb25OYW1lKCkge1xuICAgICAgICBjb25zdCB7IHBsYWNlbWVudCA9ICcnLCB0cmFuc2l0aW9uTmFtZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKHRyYW5zaXRpb25OYW1lICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB0cmFuc2l0aW9uTmFtZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocGxhY2VtZW50LmluZGV4T2YoJ3RvcCcpID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiAnc2xpZGUtZG93bic7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICdzbGlkZS11cCc7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJEcm9wRG93bn08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5Ecm9wZG93bi5kZWZhdWx0UHJvcHMgPSB7XG4gICAgbW91c2VFbnRlckRlbGF5OiAwLjE1LFxuICAgIG1vdXNlTGVhdmVEZWxheTogMC4xLFxuICAgIHBsYWNlbWVudDogJ2JvdHRvbUxlZnQnLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBakJBO0FBQUE7QUFBQTtBQUFBO0FBa0JBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBN0JBO0FBQ0E7QUE4QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYkE7QUFDQTtBQWxDQTtBQWtEQTtBQUNBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7O0FBaEVBO0FBQ0E7QUFEQTtBQWtFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/dropdown/dropdown.js
