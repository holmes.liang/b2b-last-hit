/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getDraftEditorSelectionWithNodes
 * @format
 * 
 */


var findAncestorOffsetKey = __webpack_require__(/*! ./findAncestorOffsetKey */ "./node_modules/draft-js/lib/findAncestorOffsetKey.js");

var getSelectionOffsetKeyForNode = __webpack_require__(/*! ./getSelectionOffsetKeyForNode */ "./node_modules/draft-js/lib/getSelectionOffsetKeyForNode.js");

var getUpdatedSelectionState = __webpack_require__(/*! ./getUpdatedSelectionState */ "./node_modules/draft-js/lib/getUpdatedSelectionState.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var nullthrows = __webpack_require__(/*! fbjs/lib/nullthrows */ "./node_modules/fbjs/lib/nullthrows.js");
/**
 * Convert the current selection range to an anchor/focus pair of offset keys
 * and values that can be interpreted by components.
 */


function getDraftEditorSelectionWithNodes(editorState, root, anchorNode, anchorOffset, focusNode, focusOffset) {
  var anchorIsTextNode = anchorNode.nodeType === Node.TEXT_NODE;
  var focusIsTextNode = focusNode.nodeType === Node.TEXT_NODE; // If the selection range lies only on text nodes, the task is simple.
  // Find the nearest offset-aware elements and use the
  // offset values supplied by the selection range.

  if (anchorIsTextNode && focusIsTextNode) {
    return {
      selectionState: getUpdatedSelectionState(editorState, nullthrows(findAncestorOffsetKey(anchorNode)), anchorOffset, nullthrows(findAncestorOffsetKey(focusNode)), focusOffset),
      needsRecovery: false
    };
  }

  var anchorPoint = null;
  var focusPoint = null;
  var needsRecovery = true; // An element is selected. Convert this selection range into leaf offset
  // keys and offset values for consumption at the component level. This
  // is common in Firefox, where select-all and triple click behavior leads
  // to entire elements being selected.
  //
  // Note that we use the `needsRecovery` parameter in the callback here. This
  // is because when certain elements are selected, the behavior for subsequent
  // cursor movement (e.g. via arrow keys) is uncertain and may not match
  // expectations at the component level. For example, if an entire <div> is
  // selected and the user presses the right arrow, Firefox keeps the selection
  // on the <div>. If we allow subsequent keypresses to insert characters
  // natively, they will be inserted into a browser-created text node to the
  // right of that <div>. This is obviously undesirable.
  //
  // With the `needsRecovery` flag, we inform the caller that it is responsible
  // for manually setting the selection state on the rendered document to
  // ensure proper selection state maintenance.

  if (anchorIsTextNode) {
    anchorPoint = {
      key: nullthrows(findAncestorOffsetKey(anchorNode)),
      offset: anchorOffset
    };
    focusPoint = getPointForNonTextNode(root, focusNode, focusOffset);
  } else if (focusIsTextNode) {
    focusPoint = {
      key: nullthrows(findAncestorOffsetKey(focusNode)),
      offset: focusOffset
    };
    anchorPoint = getPointForNonTextNode(root, anchorNode, anchorOffset);
  } else {
    anchorPoint = getPointForNonTextNode(root, anchorNode, anchorOffset);
    focusPoint = getPointForNonTextNode(root, focusNode, focusOffset); // If the selection is collapsed on an empty block, don't force recovery.
    // This way, on arrow key selection changes, the browser can move the
    // cursor from a non-zero offset on one block, through empty blocks,
    // to a matching non-zero offset on other text blocks.

    if (anchorNode === focusNode && anchorOffset === focusOffset) {
      needsRecovery = !!anchorNode.firstChild && anchorNode.firstChild.nodeName !== 'BR';
    }
  }

  return {
    selectionState: getUpdatedSelectionState(editorState, anchorPoint.key, anchorPoint.offset, focusPoint.key, focusPoint.offset),
    needsRecovery: needsRecovery
  };
}
/**
 * Identify the first leaf descendant for the given node.
 */


function getFirstLeaf(node) {
  while (node.firstChild && ( // data-blocks has no offset
  node.firstChild instanceof Element && node.firstChild.getAttribute('data-blocks') === 'true' || getSelectionOffsetKeyForNode(node.firstChild))) {
    node = node.firstChild;
  }

  return node;
}
/**
 * Identify the last leaf descendant for the given node.
 */


function getLastLeaf(node) {
  while (node.lastChild && ( // data-blocks has no offset
  node.lastChild instanceof Element && node.lastChild.getAttribute('data-blocks') === 'true' || getSelectionOffsetKeyForNode(node.lastChild))) {
    node = node.lastChild;
  }

  return node;
}

function getPointForNonTextNode(editorRoot, startNode, childOffset) {
  var node = startNode;
  var offsetKey = findAncestorOffsetKey(node);
  !(offsetKey != null || editorRoot && (editorRoot === node || editorRoot.firstChild === node)) ?  true ? invariant(false, 'Unknown node in selection range.') : undefined : void 0; // If the editorRoot is the selection, step downward into the content
  // wrapper.

  if (editorRoot === node) {
    node = node.firstChild;
    !(node instanceof Element && node.getAttribute('data-contents') === 'true') ?  true ? invariant(false, 'Invalid DraftEditorContents structure.') : undefined : void 0;

    if (childOffset > 0) {
      childOffset = node.childNodes.length;
    }
  } // If the child offset is zero and we have an offset key, we're done.
  // If there's no offset key because the entire editor is selected,
  // find the leftmost ("first") leaf in the tree and use that as the offset
  // key.


  if (childOffset === 0) {
    var key = null;

    if (offsetKey != null) {
      key = offsetKey;
    } else {
      var firstLeaf = getFirstLeaf(node);
      key = nullthrows(getSelectionOffsetKeyForNode(firstLeaf));
    }

    return {
      key: key,
      offset: 0
    };
  }

  var nodeBeforeCursor = node.childNodes[childOffset - 1];
  var leafKey = null;
  var textLength = null;

  if (!getSelectionOffsetKeyForNode(nodeBeforeCursor)) {
    // Our target node may be a leaf or a text node, in which case we're
    // already where we want to be and can just use the child's length as
    // our offset.
    leafKey = nullthrows(offsetKey);
    textLength = getTextContentLength(nodeBeforeCursor);
  } else {
    // Otherwise, we'll look at the child to the left of the cursor and find
    // the last leaf node in its subtree.
    var lastLeaf = getLastLeaf(nodeBeforeCursor);
    leafKey = nullthrows(getSelectionOffsetKeyForNode(lastLeaf));
    textLength = getTextContentLength(lastLeaf);
  }

  return {
    key: leafKey,
    offset: textLength
  };
}
/**
 * Return the length of a node's textContent, regarding single newline
 * characters as zero-length. This allows us to avoid problems with identifying
 * the correct selection offset for empty blocks in IE, in which we
 * render newlines instead of break tags.
 */


function getTextContentLength(node) {
  var textContent = node.textContent;
  return textContent === '\n' ? 0 : textContent.length;
}

module.exports = getDraftEditorSelectionWithNodes;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERyYWZ0RWRpdG9yU2VsZWN0aW9uV2l0aE5vZGVzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldERyYWZ0RWRpdG9yU2VsZWN0aW9uV2l0aE5vZGVzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb25XaXRoTm9kZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGZpbmRBbmNlc3Rvck9mZnNldEtleSA9IHJlcXVpcmUoJy4vZmluZEFuY2VzdG9yT2Zmc2V0S2V5Jyk7XG52YXIgZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZSA9IHJlcXVpcmUoJy4vZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZScpO1xudmFyIGdldFVwZGF0ZWRTZWxlY3Rpb25TdGF0ZSA9IHJlcXVpcmUoJy4vZ2V0VXBkYXRlZFNlbGVjdGlvblN0YXRlJyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG52YXIgbnVsbHRocm93cyA9IHJlcXVpcmUoJ2ZianMvbGliL251bGx0aHJvd3MnKTtcblxuLyoqXG4gKiBDb252ZXJ0IHRoZSBjdXJyZW50IHNlbGVjdGlvbiByYW5nZSB0byBhbiBhbmNob3IvZm9jdXMgcGFpciBvZiBvZmZzZXQga2V5c1xuICogYW5kIHZhbHVlcyB0aGF0IGNhbiBiZSBpbnRlcnByZXRlZCBieSBjb21wb25lbnRzLlxuICovXG5mdW5jdGlvbiBnZXREcmFmdEVkaXRvclNlbGVjdGlvbldpdGhOb2RlcyhlZGl0b3JTdGF0ZSwgcm9vdCwgYW5jaG9yTm9kZSwgYW5jaG9yT2Zmc2V0LCBmb2N1c05vZGUsIGZvY3VzT2Zmc2V0KSB7XG4gIHZhciBhbmNob3JJc1RleHROb2RlID0gYW5jaG9yTm9kZS5ub2RlVHlwZSA9PT0gTm9kZS5URVhUX05PREU7XG4gIHZhciBmb2N1c0lzVGV4dE5vZGUgPSBmb2N1c05vZGUubm9kZVR5cGUgPT09IE5vZGUuVEVYVF9OT0RFO1xuXG4gIC8vIElmIHRoZSBzZWxlY3Rpb24gcmFuZ2UgbGllcyBvbmx5IG9uIHRleHQgbm9kZXMsIHRoZSB0YXNrIGlzIHNpbXBsZS5cbiAgLy8gRmluZCB0aGUgbmVhcmVzdCBvZmZzZXQtYXdhcmUgZWxlbWVudHMgYW5kIHVzZSB0aGVcbiAgLy8gb2Zmc2V0IHZhbHVlcyBzdXBwbGllZCBieSB0aGUgc2VsZWN0aW9uIHJhbmdlLlxuICBpZiAoYW5jaG9ySXNUZXh0Tm9kZSAmJiBmb2N1c0lzVGV4dE5vZGUpIHtcbiAgICByZXR1cm4ge1xuICAgICAgc2VsZWN0aW9uU3RhdGU6IGdldFVwZGF0ZWRTZWxlY3Rpb25TdGF0ZShlZGl0b3JTdGF0ZSwgbnVsbHRocm93cyhmaW5kQW5jZXN0b3JPZmZzZXRLZXkoYW5jaG9yTm9kZSkpLCBhbmNob3JPZmZzZXQsIG51bGx0aHJvd3MoZmluZEFuY2VzdG9yT2Zmc2V0S2V5KGZvY3VzTm9kZSkpLCBmb2N1c09mZnNldCksXG4gICAgICBuZWVkc1JlY292ZXJ5OiBmYWxzZVxuICAgIH07XG4gIH1cblxuICB2YXIgYW5jaG9yUG9pbnQgPSBudWxsO1xuICB2YXIgZm9jdXNQb2ludCA9IG51bGw7XG4gIHZhciBuZWVkc1JlY292ZXJ5ID0gdHJ1ZTtcblxuICAvLyBBbiBlbGVtZW50IGlzIHNlbGVjdGVkLiBDb252ZXJ0IHRoaXMgc2VsZWN0aW9uIHJhbmdlIGludG8gbGVhZiBvZmZzZXRcbiAgLy8ga2V5cyBhbmQgb2Zmc2V0IHZhbHVlcyBmb3IgY29uc3VtcHRpb24gYXQgdGhlIGNvbXBvbmVudCBsZXZlbC4gVGhpc1xuICAvLyBpcyBjb21tb24gaW4gRmlyZWZveCwgd2hlcmUgc2VsZWN0LWFsbCBhbmQgdHJpcGxlIGNsaWNrIGJlaGF2aW9yIGxlYWRzXG4gIC8vIHRvIGVudGlyZSBlbGVtZW50cyBiZWluZyBzZWxlY3RlZC5cbiAgLy9cbiAgLy8gTm90ZSB0aGF0IHdlIHVzZSB0aGUgYG5lZWRzUmVjb3ZlcnlgIHBhcmFtZXRlciBpbiB0aGUgY2FsbGJhY2sgaGVyZS4gVGhpc1xuICAvLyBpcyBiZWNhdXNlIHdoZW4gY2VydGFpbiBlbGVtZW50cyBhcmUgc2VsZWN0ZWQsIHRoZSBiZWhhdmlvciBmb3Igc3Vic2VxdWVudFxuICAvLyBjdXJzb3IgbW92ZW1lbnQgKGUuZy4gdmlhIGFycm93IGtleXMpIGlzIHVuY2VydGFpbiBhbmQgbWF5IG5vdCBtYXRjaFxuICAvLyBleHBlY3RhdGlvbnMgYXQgdGhlIGNvbXBvbmVudCBsZXZlbC4gRm9yIGV4YW1wbGUsIGlmIGFuIGVudGlyZSA8ZGl2PiBpc1xuICAvLyBzZWxlY3RlZCBhbmQgdGhlIHVzZXIgcHJlc3NlcyB0aGUgcmlnaHQgYXJyb3csIEZpcmVmb3gga2VlcHMgdGhlIHNlbGVjdGlvblxuICAvLyBvbiB0aGUgPGRpdj4uIElmIHdlIGFsbG93IHN1YnNlcXVlbnQga2V5cHJlc3NlcyB0byBpbnNlcnQgY2hhcmFjdGVyc1xuICAvLyBuYXRpdmVseSwgdGhleSB3aWxsIGJlIGluc2VydGVkIGludG8gYSBicm93c2VyLWNyZWF0ZWQgdGV4dCBub2RlIHRvIHRoZVxuICAvLyByaWdodCBvZiB0aGF0IDxkaXY+LiBUaGlzIGlzIG9idmlvdXNseSB1bmRlc2lyYWJsZS5cbiAgLy9cbiAgLy8gV2l0aCB0aGUgYG5lZWRzUmVjb3ZlcnlgIGZsYWcsIHdlIGluZm9ybSB0aGUgY2FsbGVyIHRoYXQgaXQgaXMgcmVzcG9uc2libGVcbiAgLy8gZm9yIG1hbnVhbGx5IHNldHRpbmcgdGhlIHNlbGVjdGlvbiBzdGF0ZSBvbiB0aGUgcmVuZGVyZWQgZG9jdW1lbnQgdG9cbiAgLy8gZW5zdXJlIHByb3BlciBzZWxlY3Rpb24gc3RhdGUgbWFpbnRlbmFuY2UuXG5cbiAgaWYgKGFuY2hvcklzVGV4dE5vZGUpIHtcbiAgICBhbmNob3JQb2ludCA9IHtcbiAgICAgIGtleTogbnVsbHRocm93cyhmaW5kQW5jZXN0b3JPZmZzZXRLZXkoYW5jaG9yTm9kZSkpLFxuICAgICAgb2Zmc2V0OiBhbmNob3JPZmZzZXRcbiAgICB9O1xuICAgIGZvY3VzUG9pbnQgPSBnZXRQb2ludEZvck5vblRleHROb2RlKHJvb3QsIGZvY3VzTm9kZSwgZm9jdXNPZmZzZXQpO1xuICB9IGVsc2UgaWYgKGZvY3VzSXNUZXh0Tm9kZSkge1xuICAgIGZvY3VzUG9pbnQgPSB7XG4gICAgICBrZXk6IG51bGx0aHJvd3MoZmluZEFuY2VzdG9yT2Zmc2V0S2V5KGZvY3VzTm9kZSkpLFxuICAgICAgb2Zmc2V0OiBmb2N1c09mZnNldFxuICAgIH07XG4gICAgYW5jaG9yUG9pbnQgPSBnZXRQb2ludEZvck5vblRleHROb2RlKHJvb3QsIGFuY2hvck5vZGUsIGFuY2hvck9mZnNldCk7XG4gIH0gZWxzZSB7XG4gICAgYW5jaG9yUG9pbnQgPSBnZXRQb2ludEZvck5vblRleHROb2RlKHJvb3QsIGFuY2hvck5vZGUsIGFuY2hvck9mZnNldCk7XG4gICAgZm9jdXNQb2ludCA9IGdldFBvaW50Rm9yTm9uVGV4dE5vZGUocm9vdCwgZm9jdXNOb2RlLCBmb2N1c09mZnNldCk7XG5cbiAgICAvLyBJZiB0aGUgc2VsZWN0aW9uIGlzIGNvbGxhcHNlZCBvbiBhbiBlbXB0eSBibG9jaywgZG9uJ3QgZm9yY2UgcmVjb3ZlcnkuXG4gICAgLy8gVGhpcyB3YXksIG9uIGFycm93IGtleSBzZWxlY3Rpb24gY2hhbmdlcywgdGhlIGJyb3dzZXIgY2FuIG1vdmUgdGhlXG4gICAgLy8gY3Vyc29yIGZyb20gYSBub24temVybyBvZmZzZXQgb24gb25lIGJsb2NrLCB0aHJvdWdoIGVtcHR5IGJsb2NrcyxcbiAgICAvLyB0byBhIG1hdGNoaW5nIG5vbi16ZXJvIG9mZnNldCBvbiBvdGhlciB0ZXh0IGJsb2Nrcy5cbiAgICBpZiAoYW5jaG9yTm9kZSA9PT0gZm9jdXNOb2RlICYmIGFuY2hvck9mZnNldCA9PT0gZm9jdXNPZmZzZXQpIHtcbiAgICAgIG5lZWRzUmVjb3ZlcnkgPSAhIWFuY2hvck5vZGUuZmlyc3RDaGlsZCAmJiBhbmNob3JOb2RlLmZpcnN0Q2hpbGQubm9kZU5hbWUgIT09ICdCUic7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBzZWxlY3Rpb25TdGF0ZTogZ2V0VXBkYXRlZFNlbGVjdGlvblN0YXRlKGVkaXRvclN0YXRlLCBhbmNob3JQb2ludC5rZXksIGFuY2hvclBvaW50Lm9mZnNldCwgZm9jdXNQb2ludC5rZXksIGZvY3VzUG9pbnQub2Zmc2V0KSxcbiAgICBuZWVkc1JlY292ZXJ5OiBuZWVkc1JlY292ZXJ5XG4gIH07XG59XG5cbi8qKlxuICogSWRlbnRpZnkgdGhlIGZpcnN0IGxlYWYgZGVzY2VuZGFudCBmb3IgdGhlIGdpdmVuIG5vZGUuXG4gKi9cbmZ1bmN0aW9uIGdldEZpcnN0TGVhZihub2RlKSB7XG4gIHdoaWxlIChub2RlLmZpcnN0Q2hpbGQgJiYgKFxuICAvLyBkYXRhLWJsb2NrcyBoYXMgbm8gb2Zmc2V0XG4gIG5vZGUuZmlyc3RDaGlsZCBpbnN0YW5jZW9mIEVsZW1lbnQgJiYgbm9kZS5maXJzdENoaWxkLmdldEF0dHJpYnV0ZSgnZGF0YS1ibG9ja3MnKSA9PT0gJ3RydWUnIHx8IGdldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUobm9kZS5maXJzdENoaWxkKSkpIHtcbiAgICBub2RlID0gbm9kZS5maXJzdENoaWxkO1xuICB9XG4gIHJldHVybiBub2RlO1xufVxuXG4vKipcbiAqIElkZW50aWZ5IHRoZSBsYXN0IGxlYWYgZGVzY2VuZGFudCBmb3IgdGhlIGdpdmVuIG5vZGUuXG4gKi9cbmZ1bmN0aW9uIGdldExhc3RMZWFmKG5vZGUpIHtcbiAgd2hpbGUgKG5vZGUubGFzdENoaWxkICYmIChcbiAgLy8gZGF0YS1ibG9ja3MgaGFzIG5vIG9mZnNldFxuICBub2RlLmxhc3RDaGlsZCBpbnN0YW5jZW9mIEVsZW1lbnQgJiYgbm9kZS5sYXN0Q2hpbGQuZ2V0QXR0cmlidXRlKCdkYXRhLWJsb2NrcycpID09PSAndHJ1ZScgfHwgZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZShub2RlLmxhc3RDaGlsZCkpKSB7XG4gICAgbm9kZSA9IG5vZGUubGFzdENoaWxkO1xuICB9XG4gIHJldHVybiBub2RlO1xufVxuXG5mdW5jdGlvbiBnZXRQb2ludEZvck5vblRleHROb2RlKGVkaXRvclJvb3QsIHN0YXJ0Tm9kZSwgY2hpbGRPZmZzZXQpIHtcbiAgdmFyIG5vZGUgPSBzdGFydE5vZGU7XG4gIHZhciBvZmZzZXRLZXkgPSBmaW5kQW5jZXN0b3JPZmZzZXRLZXkobm9kZSk7XG5cbiAgIShvZmZzZXRLZXkgIT0gbnVsbCB8fCBlZGl0b3JSb290ICYmIChlZGl0b3JSb290ID09PSBub2RlIHx8IGVkaXRvclJvb3QuZmlyc3RDaGlsZCA9PT0gbm9kZSkpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ1Vua25vd24gbm9kZSBpbiBzZWxlY3Rpb24gcmFuZ2UuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIC8vIElmIHRoZSBlZGl0b3JSb290IGlzIHRoZSBzZWxlY3Rpb24sIHN0ZXAgZG93bndhcmQgaW50byB0aGUgY29udGVudFxuICAvLyB3cmFwcGVyLlxuICBpZiAoZWRpdG9yUm9vdCA9PT0gbm9kZSkge1xuICAgIG5vZGUgPSBub2RlLmZpcnN0Q2hpbGQ7XG4gICAgIShub2RlIGluc3RhbmNlb2YgRWxlbWVudCAmJiBub2RlLmdldEF0dHJpYnV0ZSgnZGF0YS1jb250ZW50cycpID09PSAndHJ1ZScpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0ludmFsaWQgRHJhZnRFZGl0b3JDb250ZW50cyBzdHJ1Y3R1cmUuJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuICAgIGlmIChjaGlsZE9mZnNldCA+IDApIHtcbiAgICAgIGNoaWxkT2Zmc2V0ID0gbm9kZS5jaGlsZE5vZGVzLmxlbmd0aDtcbiAgICB9XG4gIH1cblxuICAvLyBJZiB0aGUgY2hpbGQgb2Zmc2V0IGlzIHplcm8gYW5kIHdlIGhhdmUgYW4gb2Zmc2V0IGtleSwgd2UncmUgZG9uZS5cbiAgLy8gSWYgdGhlcmUncyBubyBvZmZzZXQga2V5IGJlY2F1c2UgdGhlIGVudGlyZSBlZGl0b3IgaXMgc2VsZWN0ZWQsXG4gIC8vIGZpbmQgdGhlIGxlZnRtb3N0IChcImZpcnN0XCIpIGxlYWYgaW4gdGhlIHRyZWUgYW5kIHVzZSB0aGF0IGFzIHRoZSBvZmZzZXRcbiAgLy8ga2V5LlxuICBpZiAoY2hpbGRPZmZzZXQgPT09IDApIHtcbiAgICB2YXIga2V5ID0gbnVsbDtcbiAgICBpZiAob2Zmc2V0S2V5ICE9IG51bGwpIHtcbiAgICAgIGtleSA9IG9mZnNldEtleTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIGZpcnN0TGVhZiA9IGdldEZpcnN0TGVhZihub2RlKTtcbiAgICAgIGtleSA9IG51bGx0aHJvd3MoZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZShmaXJzdExlYWYpKTtcbiAgICB9XG4gICAgcmV0dXJuIHsga2V5OiBrZXksIG9mZnNldDogMCB9O1xuICB9XG5cbiAgdmFyIG5vZGVCZWZvcmVDdXJzb3IgPSBub2RlLmNoaWxkTm9kZXNbY2hpbGRPZmZzZXQgLSAxXTtcbiAgdmFyIGxlYWZLZXkgPSBudWxsO1xuICB2YXIgdGV4dExlbmd0aCA9IG51bGw7XG5cbiAgaWYgKCFnZXRTZWxlY3Rpb25PZmZzZXRLZXlGb3JOb2RlKG5vZGVCZWZvcmVDdXJzb3IpKSB7XG4gICAgLy8gT3VyIHRhcmdldCBub2RlIG1heSBiZSBhIGxlYWYgb3IgYSB0ZXh0IG5vZGUsIGluIHdoaWNoIGNhc2Ugd2UncmVcbiAgICAvLyBhbHJlYWR5IHdoZXJlIHdlIHdhbnQgdG8gYmUgYW5kIGNhbiBqdXN0IHVzZSB0aGUgY2hpbGQncyBsZW5ndGggYXNcbiAgICAvLyBvdXIgb2Zmc2V0LlxuICAgIGxlYWZLZXkgPSBudWxsdGhyb3dzKG9mZnNldEtleSk7XG4gICAgdGV4dExlbmd0aCA9IGdldFRleHRDb250ZW50TGVuZ3RoKG5vZGVCZWZvcmVDdXJzb3IpO1xuICB9IGVsc2Uge1xuICAgIC8vIE90aGVyd2lzZSwgd2UnbGwgbG9vayBhdCB0aGUgY2hpbGQgdG8gdGhlIGxlZnQgb2YgdGhlIGN1cnNvciBhbmQgZmluZFxuICAgIC8vIHRoZSBsYXN0IGxlYWYgbm9kZSBpbiBpdHMgc3VidHJlZS5cbiAgICB2YXIgbGFzdExlYWYgPSBnZXRMYXN0TGVhZihub2RlQmVmb3JlQ3Vyc29yKTtcbiAgICBsZWFmS2V5ID0gbnVsbHRocm93cyhnZXRTZWxlY3Rpb25PZmZzZXRLZXlGb3JOb2RlKGxhc3RMZWFmKSk7XG4gICAgdGV4dExlbmd0aCA9IGdldFRleHRDb250ZW50TGVuZ3RoKGxhc3RMZWFmKTtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAga2V5OiBsZWFmS2V5LFxuICAgIG9mZnNldDogdGV4dExlbmd0aFxuICB9O1xufVxuXG4vKipcbiAqIFJldHVybiB0aGUgbGVuZ3RoIG9mIGEgbm9kZSdzIHRleHRDb250ZW50LCByZWdhcmRpbmcgc2luZ2xlIG5ld2xpbmVcbiAqIGNoYXJhY3RlcnMgYXMgemVyby1sZW5ndGguIFRoaXMgYWxsb3dzIHVzIHRvIGF2b2lkIHByb2JsZW1zIHdpdGggaWRlbnRpZnlpbmdcbiAqIHRoZSBjb3JyZWN0IHNlbGVjdGlvbiBvZmZzZXQgZm9yIGVtcHR5IGJsb2NrcyBpbiBJRSwgaW4gd2hpY2ggd2VcbiAqIHJlbmRlciBuZXdsaW5lcyBpbnN0ZWFkIG9mIGJyZWFrIHRhZ3MuXG4gKi9cbmZ1bmN0aW9uIGdldFRleHRDb250ZW50TGVuZ3RoKG5vZGUpIHtcbiAgdmFyIHRleHRDb250ZW50ID0gbm9kZS50ZXh0Q29udGVudDtcbiAgcmV0dXJuIHRleHRDb250ZW50ID09PSAnXFxuJyA/IDAgOiB0ZXh0Q29udGVudC5sZW5ndGg7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0RHJhZnRFZGl0b3JTZWxlY3Rpb25XaXRoTm9kZXM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7Ozs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getDraftEditorSelectionWithNodes.js
