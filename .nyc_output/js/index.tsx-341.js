__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormItemFullName", function() { return FormItemFullName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormItemIdType", function() { return FormItemIdType; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _field_group__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../field-group */ "./src/app/desk/component/field-group.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _mobie__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./mobie */ "./src/app/desk/component/useful-form-item/mobie.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Mobile", function() { return _mobie__WEBPACK_IMPORTED_MODULE_10__["default"]; });

/* harmony import */ var _nationality__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./nationality */ "./src/app/desk/component/useful-form-item/nationality.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormItemNationality", function() { return _nationality__WEBPACK_IMPORTED_MODULE_11__["default"]; });

/* harmony import */ var _email__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./email */ "./src/app/desk/component/useful-form-item/email.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormItemEmail", function() { return _email__WEBPACK_IMPORTED_MODULE_12__["default"]; });

/* harmony import */ var _marital_status__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./marital-status */ "./src/app/desk/component/useful-form-item/marital-status.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormItemMaritalStatus", function() { return _marital_status__WEBPACK_IMPORTED_MODULE_13__["default"]; });

/* harmony import */ var _id_expiry_date__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./id-expiry-date */ "./src/app/desk/component/useful-form-item/id-expiry-date.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FormItemIdExpiryDate", function() { return _id_expiry_date__WEBPACK_IMPORTED_MODULE_14__["default"]; });






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/useful-form-item/index.tsx";










 //TODO will be refactor

var generatePropsName = function generatePropsName(propName, dataId) {
  if (!!dataId) return "".concat(dataId, "-").concat(propName);
  return propName;
}; // full name


var FormItemFullName =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(FormItemFullName, _ModelWidget);

  function FormItemFullName() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, FormItemFullName);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(FormItemFullName).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(FormItemFullName, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    } // render

  }, {
    key: "render",
    value: function render() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_8__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Full Name".toUpperCase()).thai("ชื่อ-สกุล").my("နာမည်အပြည့်အစုံ").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NSelect"], {
        size: "large",
        form: this.props.form,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Title".toUpperCase()).thai("หัวเรื่อง").my("ခေါင်းစဉ်").getMessage(),
        tableName: "title",
        model: this.props.model,
        propName: this.props.getSelectValuePath,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NText"], {
        required: true,
        form: this.props.form,
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Name".toUpperCase()).thai("ชื่อ").my("နာမတျောကို").getMessage(),
        model: this.props.model,
        propName: this.props.getValuePath,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      })));
    }
  }]);

  return FormItemFullName;
}(_component__WEBPACK_IMPORTED_MODULE_6__["ModelWidget"]); // id type

var FormItemIdType =
/*#__PURE__*/
function (_ModelWidget2) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(FormItemIdType, _ModelWidget2);

  function FormItemIdType() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, FormItemIdType);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(FormItemIdType).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(FormItemIdType, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "getIDType",
    value: function getIDType(key) {
      var _this = this;

      _common__WEBPACK_IMPORTED_MODULE_7__["Ajax"].post("".concat(_common__WEBPACK_IMPORTED_MODULE_7__["Apis"].CUSTOMERSQ), {
        key: key,
        countNumOfClaims: false,
        countNumOfPolicies: false,
        pageSize: 200
      }).then(function (response) {
        var respData = response.body.respData;

        _this.setState({
          IdTypeOptions: (respData || {}).items
        });
      });
    } // render

  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var validatorIdNo = function validatorIdNo(rule, value, callback) {
        var idType = _this2.getValueFromModel(_this2.props.getSelectValuePath);

        if (idType === _common__WEBPACK_IMPORTED_MODULE_7__["Consts"].ID_TYPE_IC && !_common__WEBPACK_IMPORTED_MODULE_7__["Rules"].isValidThaiIdNo(value)) {
          callback(_common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("ID No. is invalid").thai("หมายเลขรหัสไม่ถูกต้อง").getMessage());
        } else {
          callback();
        }
      };

      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_field_group__WEBPACK_IMPORTED_MODULE_8__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("ID Type / No.".toUpperCase()).thai("ประเภท ID / หมายเลข.").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 131
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NSelect"], {
        size: "large",
        form: this.props.form,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("ID Type").thai("ประเภท ID").my("အိုင်ဒီအမျိုးအစား / NO ။").getMessage(),
        model: this.props.model,
        tableName: "idtype",
        propName: this.props.getSelectValuePath,
        onChange: function onChange(value) {
          _this2.props.form.validateFields([_this2.props.getValuePath], {
            force: true
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NText"], {
        form: this.props.form,
        model: this.props.model,
        required: true,
        transform: function transform() {
          return (_this2.getValueFromModel(_this2.props.getValuePath) || "").toUpperCase();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("ID No.").thai("หมายเลข.").getMessage(),
        propName: this.props.getValuePath,
        rules: [{
          validator: validatorIdNo
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      })));
    }
  }]);

  return FormItemIdType;
}(_component__WEBPACK_IMPORTED_MODULE_6__["ModelWidget"]);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgQ29uc3RzLCBMYW5ndWFnZSwgUnVsZXMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IEZpZWxkR3JvdXAgZnJvbSBcIi4uL2ZpZWxkLWdyb3VwXCI7XG5pbXBvcnQgeyBOU2VsZWN0LCBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IE1vYmlsZSBmcm9tIFwiLi9tb2JpZVwiO1xuaW1wb3J0IEZvcm1JdGVtTmF0aW9uYWxpdHkgZnJvbSBcIi4vbmF0aW9uYWxpdHlcIjtcbmltcG9ydCBGb3JtSXRlbUVtYWlsIGZyb20gXCIuL2VtYWlsXCI7XG5pbXBvcnQgRm9ybUl0ZW1NYXJpdGFsU3RhdHVzIGZyb20gXCIuL21hcml0YWwtc3RhdHVzXCI7XG5pbXBvcnQgRm9ybUl0ZW1JZEV4cGlyeURhdGUgZnJvbSBcIi4vaWQtZXhwaXJ5LWRhdGVcIjtcblxuZXhwb3J0IHtcbiAgTW9iaWxlLFxuICBGb3JtSXRlbU5hdGlvbmFsaXR5LFxuICBGb3JtSXRlbUVtYWlsLFxuICBGb3JtSXRlbU1hcml0YWxTdGF0dXMsXG4gIEZvcm1JdGVtSWRFeHBpcnlEYXRlLFxufTtcblxuLy9UT0RPIHdpbGwgYmUgcmVmYWN0b3JcbmludGVyZmFjZSBJUHJvcHMgZXh0ZW5kcyBGb3JtQ29tcG9uZW50UHJvcHMge1xuICBtb2RlbDogYW55O1xuICBmb3JtOiBhbnk7XG4gIGdldFZhbHVlUGF0aDogc3RyaW5nO1xuICBnZXRTZWxlY3RWYWx1ZVBhdGg/OiBzdHJpbmc7XG59XG5cbmludGVyZmFjZSBJUHJvcHNTZWxlY3QgZXh0ZW5kcyBJUHJvcHMge1xuICBnZXRTZWxlY3RWYWx1ZVBhdGg6IHN0cmluZztcbn1cblxuXG5jb25zdCBnZW5lcmF0ZVByb3BzTmFtZSA9IChwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWQ/OiBzdHJpbmcpOiBzdHJpbmcgPT4ge1xuICBpZiAoISFkYXRhSWQpIHJldHVybiBgJHtkYXRhSWR9LSR7cHJvcE5hbWV9YDtcbiAgcmV0dXJuIHByb3BOYW1lO1xufTtcblxuXG4vLyBmdWxsIG5hbWVcbmV4cG9ydCBjbGFzcyBGb3JtSXRlbUZ1bGxOYW1lPFAgZXh0ZW5kcyBJUHJvcHNTZWxlY3QsIFMsIEM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHt9IGFzIEM7XG4gIH1cblxuICAvLyByZW5kZXJcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8PlxuICAgICAgICA8RmllbGRHcm91cFxuICAgICAgICAgIGNsYXNzTmFtZT1cInVzYWdlLWdyb3VwXCJcbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJGdWxsIE5hbWVcIi50b1VwcGVyQ2FzZSgpKVxuICAgICAgICAgICAgLnRoYWkoXCLguIrguLfguYjguK0t4Liq4LiB4Li44LilXCIpXG4gICAgICAgICAgICAubXkoXCLhgJThgKzhgJnhgIrhgLrhgKHhgJXhgLzhgIrhgLrhgLfhgKHhgIXhgK/hgLZcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgc2VsZWN0WHNTbT17eyB4czogOCwgc206IDYgfX1cbiAgICAgICAgICB0ZXh0WHNTbT17eyB4czogMTYsIHNtOiAxMyB9fVxuICAgICAgICAgIG1pbldpZHRoPVwiMTQwcHhcIlxuICAgICAgICA+XG4gICAgICAgICAgPE5TZWxlY3RcbiAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgIGZvcm09e3RoaXMucHJvcHMuZm9ybX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVGl0bGVcIi50b1VwcGVyQ2FzZSgpKVxuICAgICAgICAgICAgICAudGhhaShcIuC4q+C4seC4p+C5gOC4o+C4t+C5iOC4reC4h1wiKVxuICAgICAgICAgICAgICAubXkoXCLhgIHhgLHhgKvhgIThgLrhgLjhgIXhgInhgLpcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIHRhYmxlTmFtZT1cInRpdGxlXCJcbiAgICAgICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMucHJvcHMuZ2V0U2VsZWN0VmFsdWVQYXRofVxuICAgICAgICAgIC8+XG5cbiAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgZm9ybT17dGhpcy5wcm9wcy5mb3JtfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiTmFtZVwiLnRvVXBwZXJDYXNlKCkpXG4gICAgICAgICAgICAgIC50aGFpKFwi4LiK4Li34LmI4LitXCIpXG4gICAgICAgICAgICAgIC5teShcIuGAlOGArOGAmeGAkOGAu+GAseGArOGAgOGAreGAr1wiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgbW9kZWw9e3RoaXMucHJvcHMubW9kZWx9XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5wcm9wcy5nZXRWYWx1ZVBhdGh9XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9GaWVsZEdyb3VwPlxuICAgICAgPC8+XG4gICAgKTtcbiAgfVxufVxuXG5cbi8vIGlkIHR5cGVcbnR5cGUgSWRUeXBlU3RhdGUgPSB7XG4gIElkVHlwZU9wdGlvbnM6IGFueTtcbn07XG5cbmV4cG9ydCBjbGFzcyBGb3JtSXRlbUlkVHlwZTxQIGV4dGVuZHMgSVByb3BzU2VsZWN0LCBTIGV4dGVuZHMgSWRUeXBlU3RhdGUsIEM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHt9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0SURUeXBlKGtleTogc3RyaW5nKSB7XG4gICAgQWpheC5wb3N0KGAke0FwaXMuQ1VTVE9NRVJTUX1gLCB7XG4gICAgICBrZXksXG4gICAgICBjb3VudE51bU9mQ2xhaW1zOiBmYWxzZSxcbiAgICAgIGNvdW50TnVtT2ZQb2xpY2llczogZmFsc2UsXG4gICAgICBwYWdlU2l6ZTogMjAwLFxuICAgIH0pLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgSWRUeXBlT3B0aW9uczogKHJlc3BEYXRhIHx8IHt9KS5pdGVtcyxcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLy8gcmVuZGVyXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB2YWxpZGF0b3JJZE5vID0gKHJ1bGU6IGFueSwgdmFsdWU6IGFueSwgY2FsbGJhY2s6IGFueSkgPT4ge1xuICAgICAgbGV0IGlkVHlwZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5wcm9wcy5nZXRTZWxlY3RWYWx1ZVBhdGgpO1xuICAgICAgaWYgKGlkVHlwZSA9PT0gQ29uc3RzLklEX1RZUEVfSUMgJiYgIVJ1bGVzLmlzVmFsaWRUaGFpSWRObyh2YWx1ZSkpIHtcbiAgICAgICAgY2FsbGJhY2soXG4gICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJJRCBOby4gaXMgaW52YWxpZFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguKvguKHguLLguKLguYDguKXguILguKPguKvguLHguKrguYTguKHguYjguJbguLnguIHguJXguYnguK3guIdcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCksXG4gICAgICAgICk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIChcbiAgICAgIDw+XG4gICAgICAgIDxGaWVsZEdyb3VwXG4gICAgICAgICAgY2xhc3NOYW1lPVwidXNhZ2UtZ3JvdXBcIlxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIklEIFR5cGUgLyBOby5cIi50b1VwcGVyQ2FzZSgpKVxuICAgICAgICAgICAgLnRoYWkoXCLguJvguKPguLDguYDguKDguJcgSUQgLyDguKvguKHguLLguKLguYDguKXguIIuXCIpXG4gICAgICAgICAgICAubXkoXCLhgKHhgK3hgK/hgIThgLrhgJLhgK7hgKHhgJnhgLvhgK3hgK/hgLjhgKHhgIXhgKzhgLggLyBOTyDhgYtcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgc2VsZWN0WHNTbT17eyB4czogOCwgc206IDYgfX1cbiAgICAgICAgICB0ZXh0WHNTbT17eyB4czogMTYsIHNtOiAxMyB9fVxuICAgICAgICAgIG1pbldpZHRoPVwiMTQwcHhcIlxuICAgICAgICA+XG4gICAgICAgICAgPE5TZWxlY3RcbiAgICAgICAgICAgIHNpemU9e1wibGFyZ2VcIn1cbiAgICAgICAgICAgIGZvcm09e3RoaXMucHJvcHMuZm9ybX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgVHlwZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4lyBJRFwiKVxuICAgICAgICAgICAgICAubXkoXCLhgKHhgK3hgK/hgIThgLrhgJLhgK7hgKHhgJnhgLvhgK3hgK/hgLjhgKHhgIXhgKzhgLggLyBOTyDhgYtcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICAgICAgdGFibGVOYW1lPVwiaWR0eXBlXCJcbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLnByb3BzLmdldFNlbGVjdFZhbHVlUGF0aH1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWU6IGFueSk6IHZvaWQgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMucHJvcHMuZ2V0VmFsdWVQYXRoXSwgeyBmb3JjZTogdHJ1ZSB9KTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgIGZvcm09e3RoaXMucHJvcHMuZm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICB0cmFuc2Zvcm09eygpOiBhbnkgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5wcm9wcy5nZXRWYWx1ZVBhdGgpIHx8IFwiXCIpLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiSUQgTm8uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwi4Lir4Lih4Liy4Lii4LmA4Lil4LiCLlwiKVxuICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMucHJvcHMuZ2V0VmFsdWVQYXRofVxuICAgICAgICAgICAgcnVsZXM9e1t7IHZhbGlkYXRvcjogdmFsaWRhdG9ySWRObyB9XX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICA8Lz5cbiAgICApO1xuICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQU9BO0FBR0E7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBN0NBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFxREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQWxCQTtBQUFBO0FBQUE7QUFvQkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFnQkE7QUE1RUE7QUFDQTtBQURBO0FBQUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/useful-form-item/index.tsx
