__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component_wizard__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_component_page_desk_page_header__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/page/desk-page-header */ "./src/app/desk/component/page/desk-page-header.tsx");
/* harmony import */ var _desk_quote_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk/quote/common */ "./src/app/desk/quote/common/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/quote-entry.tsx";








var defaultPolicy = {
  bizType: "NB",
  effDate: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].formatNowWithTimeZone(),
  expDate: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].formatDateTimeWithTimeZone(_common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].plusXYearsFromNow(1)),
  proposalDate: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].formatNowWithTimeZone(),
  ext: {
    insured: {},
    payer: {}
  }
};

var QuoteEntry =
/*#__PURE__*/
function (_DeskPage) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(QuoteEntry, _DeskPage);

  function QuoteEntry(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, QuoteEntry);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(QuoteEntry).call(this, props, context));
    _this.policy = _this.initPolicy();
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(QuoteEntry, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(QuoteEntry.prototype), "initComponents", this).call(this), {});
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        initialized: false
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getBasicData();
    }
  }, {
    key: "getBasicData",
    value: function getBasicData() {
      var params = new URLSearchParams(window.location.search);
      var clonePolicyId = params.get("clonePolicyId");

      if (clonePolicyId) {
        _desk_quote_common__WEBPACK_IMPORTED_MODULE_14__["default"].clonePolicie(this, clonePolicyId);
        return;
      }

      var identity = this.props.identity;

      if (identity.policyId) {
        _desk_quote_common__WEBPACK_IMPORTED_MODULE_14__["default"].loadPolicie(this);
      } else {
        _desk_quote_common__WEBPACK_IMPORTED_MODULE_14__["default"].initNewPolicie(this);
      }
    }
  }, {
    key: "getEndoInfo",
    value: function getEndoInfo(step) {
      var _this2 = this;

      var identity = this.props.identity;
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].ENDO_ID.replace(":endoId", identity.endoId)), {}).then(function (response) {
        var respData = response.body.respData;

        _this2.mergePolicyToUIModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, respData || {}, {}, {
          ext: {
            _ui: {
              step: step
            }
          }
        }));

        _this2.setState({
          initialized: true
        }, function () {
          _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].bodyScale();
        });
      }).catch(function (error) {
        return _this2.setState({
          initialized: true
        });
      });
    }
  }, {
    key: "initPolicy",
    value: function initPolicy() {
      return _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied(defaultPolicy);
    }
  }, {
    key: "mergePolicyToUIModel",
    value: function mergePolicyToUIModel(newPolicy) {
      //TODO this is a temporary solution
      this.policy = _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied(newPolicy);
      this.forceUpdate();
    }
  }, {
    key: "mergePolicyToServiceModel",
    value: function mergePolicyToServiceModel() {
      return this.policy;
    }
  }, {
    key: "mergePolicyToServiceModelEndo",
    value: function mergePolicyToServiceModelEndo() {
      var newPolicy = _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied(this.policy);
      var planCoversArr = [];
      var insuredInfo = newPolicy["ext.planCovers"];
      delete newPolicy.ext.planCovers;

      for (var i in insuredInfo) {
        planCoversArr.push({
          code: i,
          value: insuredInfo[i]
        });
      }

      newPolicy["ext.planCovers"] = planCoversArr;
      var optionalCoversArr = [];
      var optionalCoversInfo = newPolicy["ext.optionalCovers"];
      delete newPolicy.ext.optionalCovers;

      for (var _i in optionalCoversInfo) {
        optionalCoversArr.push({
          code: _i,
          value: optionalCoversInfo[_i].value,
          checked: optionalCoversInfo[_i].checked
        });
      }

      newPolicy["ext.optionalCovers"] = optionalCoversArr;
      return newPolicy;
    }
  }, {
    key: "render",
    value: function render() {
      if (!this.state.initialized) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 140
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_page_desk_page_header__WEBPACK_IMPORTED_MODULE_13__["default"], {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 141
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("section", {
          style: {
            height: "600px",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 142
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Spin"], {
          size: "large",
          spinning: true,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 149
          },
          __self: this
        })));
      }

      return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(QuoteEntry.prototype), "render", this).call(this);
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      var _this3 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_component_wizard__WEBPACK_IMPORTED_MODULE_11__["Wizard"], {
        step: this.getCurrentStep(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }, this.getStepComponents().map(function (item, index) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_component_wizard__WEBPACK_IMPORTED_MODULE_11__["WizardPage"], {
          key: index,
          step: item.stepName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 162
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(item.component, {
          model: _this3.policy,
          mergePolicyToUIModel: _this3.mergePolicyToUIModel.bind(_this3),
          mergePolicyToServiceModel: _this3.mergePolicyToServiceModel.bind(_this3),
          form: _this3.props.form,
          identity: _this3.props.identity
        }));
      }));
    }
  }]);

  return QuoteEntry;
}(_desk_component__WEBPACK_IMPORTED_MODULE_7__["DeskPage"]);

function bodyScale() {
  var scale = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getWhith() > 1200 || _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getWhith() < 768 ? 1 : _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getWhith() / 1200;
  var container = document.querySelector(".page-body");

  if (container) {
    try {
      container.childNodes[0].style.minHeight = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getHeight() * (1 / scale) - 196 * scale + "px";
      container.childNodes[0].style.backgroundColor = "#fff";
    } catch (e) {}
  }
}

/* harmony default export */ __webpack_exports__["default"] = (QuoteEntry);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVvdGUtZW50cnkudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVvdGUtZW50cnkudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERlc2tQYWdlIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlLCBQYWdlQ29tcG9uZW50cywgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCIuLi8uLi8uLi9jb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbGxlciB9IGZyb20gXCJAbW9kZWxcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIERhdGVVdGlscywgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgV2l6YXJkLCBXaXphcmRQYWdlIH0gZnJvbSBcIi4uL2NvbXBvbmVudC93aXphcmRcIjtcbmltcG9ydCB7IEZvcm1Db21wb25lbnRQcm9wcyB9IGZyb20gXCJhbnRkL2xpYi9mb3JtXCI7XG5pbXBvcnQgeyBTcGluIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCBEZXNrUGFnZUhlYWRlciBmcm9tIFwiQGRlc2stY29tcG9uZW50L3BhZ2UvZGVzay1wYWdlLWhlYWRlclwiO1xuaW1wb3J0IFF1b3RlQ29tbW9uIGZyb20gXCJAZGVzay9xdW90ZS9jb21tb25cIjtcblxuZXhwb3J0IHR5cGUgU3RlcENvbXBvbmVudCA9IHsgc3RlcE5hbWU6IHN0cmluZzsgY29tcG9uZW50OiBhbnkgfTtcblxuZXhwb3J0IHR5cGUgSWRlbnRpdHkgPSB7XG4gIHBvbGljeUlkPzogc3RyaW5nO1xuICBpdG50Q29kZT86IHN0cmluZztcbiAgcHJkdENvZGU6IHN0cmluZztcbiAgcGF5bWVudFN0YXR1cz86IHN0cmluZztcbiAgYml6VHlwZT86IHN0cmluZztcbiAgZW5kb0lkPzogc3RyaW5nO1xufVxuXG5leHBvcnQgdHlwZSBRdW90ZUVudHJ5UHJvcHMgPSB7XG4gIGlkZW50aXR5PzogSWRlbnRpdHk7XG59ICYgV2lkZ2V0UHJvcHMgJiBGb3JtQ29tcG9uZW50UHJvcHNcblxuZXhwb3J0IHR5cGUgUXVvdGVFbnRyeVN0YXRlID0ge1xuICBpbml0aWFsaXplZDogYm9vbGVhbjtcbn1cbmNvbnN0IGRlZmF1bHRQb2xpY3kgPSB7XG4gIGJpelR5cGU6IFwiTkJcIixcbiAgZWZmRGF0ZTogRGF0ZVV0aWxzLmZvcm1hdE5vd1dpdGhUaW1lWm9uZSgpLFxuICBleHBEYXRlOiBEYXRlVXRpbHMuZm9ybWF0RGF0ZVRpbWVXaXRoVGltZVpvbmUoRGF0ZVV0aWxzLnBsdXNYWWVhcnNGcm9tTm93KDEpKSxcbiAgcHJvcG9zYWxEYXRlOiBEYXRlVXRpbHMuZm9ybWF0Tm93V2l0aFRpbWVab25lKCksXG4gIGV4dDogeyBpbnN1cmVkOiB7fSwgcGF5ZXI6IHt9IH0sXG59O1xuXG5hYnN0cmFjdCBjbGFzcyBRdW90ZUVudHJ5PFAgZXh0ZW5kcyBRdW90ZUVudHJ5UHJvcHMsIFMgZXh0ZW5kcyBRdW90ZUVudHJ5U3RhdGUsIEMgZXh0ZW5kcyBQYWdlQ29tcG9uZW50cz5cbiAgZXh0ZW5kcyBEZXNrUGFnZTxQLCBTLCBDPiB7XG5cbiAgcHJvdGVjdGVkIHBvbGljeTogYW55ID0gdGhpcy5pbml0UG9saWN5KCk7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0Q29tcG9uZW50cygpLCB7fSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiB7IGluaXRpYWxpemVkOiBmYWxzZSB9IGFzIFM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmdldEJhc2ljRGF0YSgpO1xuICB9XG5cbiAgZ2V0QmFzaWNEYXRhKCkge1xuICAgIGxldCBwYXJhbXMgPSBuZXcgVVJMU2VhcmNoUGFyYW1zKHdpbmRvdy5sb2NhdGlvbi5zZWFyY2gpO1xuICAgIGxldCBjbG9uZVBvbGljeUlkID0gcGFyYW1zLmdldChcImNsb25lUG9saWN5SWRcIik7XG4gICAgaWYgKGNsb25lUG9saWN5SWQpIHtcbiAgICAgIFF1b3RlQ29tbW9uLmNsb25lUG9saWNpZSh0aGlzLCBjbG9uZVBvbGljeUlkKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25zdCB7IGlkZW50aXR5IH0gPSB0aGlzLnByb3BzO1xuICAgIGlmIChpZGVudGl0eSEucG9saWN5SWQpIHtcbiAgICAgIFF1b3RlQ29tbW9uLmxvYWRQb2xpY2llKHRoaXMpO1xuICAgIH0gZWxzZSB7XG4gICAgICBRdW90ZUNvbW1vbi5pbml0TmV3UG9saWNpZSh0aGlzKTtcbiAgICB9XG4gIH1cblxuICBnZXRFbmRvSW5mbyhzdGVwOiBzdHJpbmcpIHtcbiAgICBjb25zdCB7IGlkZW50aXR5IH0gPSB0aGlzLnByb3BzO1xuICAgIEFqYXguZ2V0KGAke0FwaXMuRU5ET19JRC5yZXBsYWNlKFwiOmVuZG9JZFwiLCAoaWRlbnRpdHkgYXMgYW55KS5lbmRvSWQpfWAsIHt9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keTtcbiAgICAgICAgdGhpcy5tZXJnZVBvbGljeVRvVUlNb2RlbCh7XG4gICAgICAgICAgLi4uKHJlc3BEYXRhIHx8IHt9KSwgLi4ue1xuICAgICAgICAgICAgZXh0OiB7IF91aTogeyBzdGVwOiBzdGVwIH0gfSxcbiAgICAgICAgICB9LFxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGluaXRpYWxpemVkOiB0cnVlIH0sICgpID0+IHtcbiAgICAgICAgICBVdGlscy5ib2R5U2NhbGUoKTtcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnJvciA9PiB0aGlzLnNldFN0YXRlKHsgaW5pdGlhbGl6ZWQ6IHRydWUgfSkpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IGdldFN0ZXBDb21wb25lbnRzKCk6IFN0ZXBDb21wb25lbnRbXTtcblxuICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0Q3VycmVudFN0ZXAoKTogc3RyaW5nO1xuXG4gIHByb3RlY3RlZCBpbml0UG9saWN5KCk6IGFueSB7XG4gICAgcmV0dXJuIE1vZGVsbGVyLmFzUHJveGllZChkZWZhdWx0UG9saWN5KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBtZXJnZVBvbGljeVRvVUlNb2RlbChuZXdQb2xpY3k6IGFueSk6IHZvaWQge1xuICAgIC8vVE9ETyB0aGlzIGlzIGEgdGVtcG9yYXJ5IHNvbHV0aW9uXG4gICAgdGhpcy5wb2xpY3kgPSBNb2RlbGxlci5hc1Byb3hpZWQobmV3UG9saWN5KTtcbiAgICB0aGlzLmZvcmNlVXBkYXRlKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgbWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbCgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnBvbGljeTtcbiAgfVxuXG4gIHByb3RlY3RlZCBtZXJnZVBvbGljeVRvU2VydmljZU1vZGVsRW5kbygpOiBhbnkge1xuICAgIGNvbnN0IG5ld1BvbGljeSA9IE1vZGVsbGVyLmFzUHJveGllZCh0aGlzLnBvbGljeSk7XG5cbiAgICBsZXQgcGxhbkNvdmVyc0FycjogYW55ID0gW107XG4gICAgY29uc3QgaW5zdXJlZEluZm8gPSBuZXdQb2xpY3lbXCJleHQucGxhbkNvdmVyc1wiXTtcbiAgICBkZWxldGUgbmV3UG9saWN5LmV4dC5wbGFuQ292ZXJzO1xuICAgIGZvciAobGV0IGkgaW4gaW5zdXJlZEluZm8pIHtcbiAgICAgIHBsYW5Db3ZlcnNBcnIucHVzaCh7XG4gICAgICAgIGNvZGU6IGksXG4gICAgICAgIHZhbHVlOiBpbnN1cmVkSW5mb1tpXSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBuZXdQb2xpY3lbXCJleHQucGxhbkNvdmVyc1wiXSA9IHBsYW5Db3ZlcnNBcnI7XG5cbiAgICBsZXQgb3B0aW9uYWxDb3ZlcnNBcnI6IGFueSA9IFtdO1xuICAgIGNvbnN0IG9wdGlvbmFsQ292ZXJzSW5mbyA9IG5ld1BvbGljeVtcImV4dC5vcHRpb25hbENvdmVyc1wiXTtcbiAgICBkZWxldGUgbmV3UG9saWN5LmV4dC5vcHRpb25hbENvdmVycztcbiAgICBmb3IgKGxldCBpIGluIG9wdGlvbmFsQ292ZXJzSW5mbykge1xuICAgICAgb3B0aW9uYWxDb3ZlcnNBcnIucHVzaCh7XG4gICAgICAgIGNvZGU6IGksXG4gICAgICAgIHZhbHVlOiBvcHRpb25hbENvdmVyc0luZm9baV0udmFsdWUsXG4gICAgICAgIGNoZWNrZWQ6IG9wdGlvbmFsQ292ZXJzSW5mb1tpXS5jaGVja2VkLFxuICAgICAgfSk7XG4gICAgfVxuICAgIG5ld1BvbGljeVtcImV4dC5vcHRpb25hbENvdmVyc1wiXSA9IG9wdGlvbmFsQ292ZXJzQXJyO1xuICAgIHJldHVybiBuZXdQb2xpY3k7XG4gIH1cblxuICByZW5kZXIoKTogYW55IHtcbiAgICBpZiAoIXRoaXMuc3RhdGUuaW5pdGlhbGl6ZWQpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPERlc2tQYWdlSGVhZGVyLz5cbiAgICAgICAgICA8c2VjdGlvbiBzdHlsZT17e1xuICAgICAgICAgICAgaGVpZ2h0OiBcIjYwMHB4XCIsXG4gICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXG4gICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxuICAgICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcbiAgICAgICAgICB9fT5cbiAgICAgICAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXt0cnVlfS8+XG4gICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICk7XG4gICAgfVxuICAgIHJldHVybiBzdXBlci5yZW5kZXIoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJQYWdlQm9keSgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFdpemFyZCBzdGVwPXt0aGlzLmdldEN1cnJlbnRTdGVwKCl9PlxuICAgICAgICB7XG4gICAgICAgICAgdGhpcy5nZXRTdGVwQ29tcG9uZW50cygpLm1hcCgoaXRlbSwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIHJldHVybiAoPFdpemFyZFBhZ2Uga2V5PXtpbmRleH0gc3RlcD17aXRlbS5zdGVwTmFtZX0+XG4gICAgICAgICAgICAgIHtSZWFjdC5jcmVhdGVFbGVtZW50KGl0ZW0uY29tcG9uZW50LCB7XG4gICAgICAgICAgICAgICAgbW9kZWw6IHRoaXMucG9saWN5LFxuICAgICAgICAgICAgICAgIG1lcmdlUG9saWN5VG9VSU1vZGVsOiB0aGlzLm1lcmdlUG9saWN5VG9VSU1vZGVsLmJpbmQodGhpcyksXG4gICAgICAgICAgICAgICAgbWVyZ2VQb2xpY3lUb1NlcnZpY2VNb2RlbDogdGhpcy5tZXJnZVBvbGljeVRvU2VydmljZU1vZGVsLmJpbmQodGhpcyksXG4gICAgICAgICAgICAgICAgZm9ybTogdGhpcy5wcm9wcy5mb3JtLFxuICAgICAgICAgICAgICAgIGlkZW50aXR5OiB0aGlzLnByb3BzLmlkZW50aXR5LFxuICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgIDwvV2l6YXJkUGFnZT4pO1xuICAgICAgICAgIH0pXG4gICAgICAgIH1cbiAgICAgIDwvV2l6YXJkPlxuICAgICk7XG4gIH1cbn1cblxuZnVuY3Rpb24gYm9keVNjYWxlKCkge1xuICBjb25zdCBzY2FsZSA9IFV0aWxzLmdldFdoaXRoKCkgPiAxMjAwIHx8IFV0aWxzLmdldFdoaXRoKCkgPCA3NjggPyAxIDogVXRpbHMuZ2V0V2hpdGgoKSAvIDEyMDA7XG4gIGxldCBjb250YWluZXI6IGFueSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIucGFnZS1ib2R5XCIpO1xuICBpZiAoY29udGFpbmVyKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnRhaW5lci5jaGlsZE5vZGVzWzBdLnN0eWxlLm1pbkhlaWdodCA9IChVdGlscy5nZXRIZWlnaHQoKSAqICgxIC8gc2NhbGUpIC0gMTk2ICogc2NhbGUpICsgXCJweFwiO1xuICAgICAgY29udGFpbmVyLmNoaWxkTm9kZXNbMF0uc3R5bGUuYmFja2dyb3VuZENvbG9yID0gXCIjZmZmXCI7XG4gICAgfSBjYXRjaCAoZSkge1xuXG4gICAgfVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFF1b3RlRW50cnk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBTEE7QUFDQTtBQU9BOzs7OztBQUtBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBTUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7QUFJQTs7OztBQXhJQTtBQUNBO0FBMElBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/quote-entry.tsx
