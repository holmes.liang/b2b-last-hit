/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var AxisPointerModel = echarts.extendComponentModel({
  type: 'axisPointer',
  coordSysAxesInfo: null,
  defaultOption: {
    // 'auto' means that show when triggered by tooltip or handle.
    show: 'auto',
    // 'click' | 'mousemove' | 'none'
    triggerOn: null,
    // set default in AxisPonterView.js
    zlevel: 0,
    z: 50,
    type: 'line',
    // 'line' 'shadow' 'cross' 'none'.
    // axispointer triggered by tootip determine snap automatically,
    // see `modelHelper`.
    snap: false,
    triggerTooltip: true,
    value: null,
    status: null,
    // Init value depends on whether handle is used.
    // [group0, group1, ...]
    // Each group can be: {
    //      mapper: function () {},
    //      singleTooltip: 'multiple',  // 'multiple' or 'single'
    //      xAxisId: ...,
    //      yAxisName: ...,
    //      angleAxisIndex: ...
    // }
    // mapper: can be ignored.
    //      input: {axisInfo, value}
    //      output: {axisInfo, value}
    link: [],
    // Do not set 'auto' here, otherwise global animation: false
    // will not effect at this axispointer.
    animation: null,
    animationDurationUpdate: 200,
    lineStyle: {
      color: '#aaa',
      width: 1,
      type: 'solid'
    },
    shadowStyle: {
      color: 'rgba(150,150,150,0.3)'
    },
    label: {
      show: true,
      formatter: null,
      // string | Function
      precision: 'auto',
      // Or a number like 0, 1, 2 ...
      margin: 3,
      color: '#fff',
      padding: [5, 7, 5, 7],
      backgroundColor: 'auto',
      // default: axis line color
      borderColor: null,
      borderWidth: 0,
      shadowBlur: 3,
      shadowColor: '#aaa' // Considering applicability, common style should
      // better not have shadowOffset.
      // shadowOffsetX: 0,
      // shadowOffsetY: 2

    },
    handle: {
      show: false,

      /* eslint-disable */
      icon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4h1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7v-1.2h6.6z M13.3,22H6.7v-1.2h6.6z M13.3,19.6H6.7v-1.2h6.6z',
      // jshint ignore:line

      /* eslint-enable */
      size: 45,
      // handle margin is from symbol center to axis, which is stable when circular move.
      margin: 50,
      // color: '#1b8bbd'
      // color: '#2f4554'
      color: '#333',
      shadowBlur: 3,
      shadowColor: '#aaa',
      shadowOffsetX: 0,
      shadowOffsetY: 2,
      // For mobile performance
      throttle: 40
    }
  }
});
var _default = AxisPointerModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXNQb2ludGVyL0F4aXNQb2ludGVyTW9kZWwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvYXhpc1BvaW50ZXIvQXhpc1BvaW50ZXJNb2RlbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIGVjaGFydHMgPSByZXF1aXJlKFwiLi4vLi4vZWNoYXJ0c1wiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIEF4aXNQb2ludGVyTW9kZWwgPSBlY2hhcnRzLmV4dGVuZENvbXBvbmVudE1vZGVsKHtcbiAgdHlwZTogJ2F4aXNQb2ludGVyJyxcbiAgY29vcmRTeXNBeGVzSW5mbzogbnVsbCxcbiAgZGVmYXVsdE9wdGlvbjoge1xuICAgIC8vICdhdXRvJyBtZWFucyB0aGF0IHNob3cgd2hlbiB0cmlnZ2VyZWQgYnkgdG9vbHRpcCBvciBoYW5kbGUuXG4gICAgc2hvdzogJ2F1dG8nLFxuICAgIC8vICdjbGljaycgfCAnbW91c2Vtb3ZlJyB8ICdub25lJ1xuICAgIHRyaWdnZXJPbjogbnVsbCxcbiAgICAvLyBzZXQgZGVmYXVsdCBpbiBBeGlzUG9udGVyVmlldy5qc1xuICAgIHpsZXZlbDogMCxcbiAgICB6OiA1MCxcbiAgICB0eXBlOiAnbGluZScsXG4gICAgLy8gJ2xpbmUnICdzaGFkb3cnICdjcm9zcycgJ25vbmUnLlxuICAgIC8vIGF4aXNwb2ludGVyIHRyaWdnZXJlZCBieSB0b290aXAgZGV0ZXJtaW5lIHNuYXAgYXV0b21hdGljYWxseSxcbiAgICAvLyBzZWUgYG1vZGVsSGVscGVyYC5cbiAgICBzbmFwOiBmYWxzZSxcbiAgICB0cmlnZ2VyVG9vbHRpcDogdHJ1ZSxcbiAgICB2YWx1ZTogbnVsbCxcbiAgICBzdGF0dXM6IG51bGwsXG4gICAgLy8gSW5pdCB2YWx1ZSBkZXBlbmRzIG9uIHdoZXRoZXIgaGFuZGxlIGlzIHVzZWQuXG4gICAgLy8gW2dyb3VwMCwgZ3JvdXAxLCAuLi5dXG4gICAgLy8gRWFjaCBncm91cCBjYW4gYmU6IHtcbiAgICAvLyAgICAgIG1hcHBlcjogZnVuY3Rpb24gKCkge30sXG4gICAgLy8gICAgICBzaW5nbGVUb29sdGlwOiAnbXVsdGlwbGUnLCAgLy8gJ211bHRpcGxlJyBvciAnc2luZ2xlJ1xuICAgIC8vICAgICAgeEF4aXNJZDogLi4uLFxuICAgIC8vICAgICAgeUF4aXNOYW1lOiAuLi4sXG4gICAgLy8gICAgICBhbmdsZUF4aXNJbmRleDogLi4uXG4gICAgLy8gfVxuICAgIC8vIG1hcHBlcjogY2FuIGJlIGlnbm9yZWQuXG4gICAgLy8gICAgICBpbnB1dDoge2F4aXNJbmZvLCB2YWx1ZX1cbiAgICAvLyAgICAgIG91dHB1dDoge2F4aXNJbmZvLCB2YWx1ZX1cbiAgICBsaW5rOiBbXSxcbiAgICAvLyBEbyBub3Qgc2V0ICdhdXRvJyBoZXJlLCBvdGhlcndpc2UgZ2xvYmFsIGFuaW1hdGlvbjogZmFsc2VcbiAgICAvLyB3aWxsIG5vdCBlZmZlY3QgYXQgdGhpcyBheGlzcG9pbnRlci5cbiAgICBhbmltYXRpb246IG51bGwsXG4gICAgYW5pbWF0aW9uRHVyYXRpb25VcGRhdGU6IDIwMCxcbiAgICBsaW5lU3R5bGU6IHtcbiAgICAgIGNvbG9yOiAnI2FhYScsXG4gICAgICB3aWR0aDogMSxcbiAgICAgIHR5cGU6ICdzb2xpZCdcbiAgICB9LFxuICAgIHNoYWRvd1N0eWxlOiB7XG4gICAgICBjb2xvcjogJ3JnYmEoMTUwLDE1MCwxNTAsMC4zKSdcbiAgICB9LFxuICAgIGxhYmVsOiB7XG4gICAgICBzaG93OiB0cnVlLFxuICAgICAgZm9ybWF0dGVyOiBudWxsLFxuICAgICAgLy8gc3RyaW5nIHwgRnVuY3Rpb25cbiAgICAgIHByZWNpc2lvbjogJ2F1dG8nLFxuICAgICAgLy8gT3IgYSBudW1iZXIgbGlrZSAwLCAxLCAyIC4uLlxuICAgICAgbWFyZ2luOiAzLFxuICAgICAgY29sb3I6ICcjZmZmJyxcbiAgICAgIHBhZGRpbmc6IFs1LCA3LCA1LCA3XSxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ2F1dG8nLFxuICAgICAgLy8gZGVmYXVsdDogYXhpcyBsaW5lIGNvbG9yXG4gICAgICBib3JkZXJDb2xvcjogbnVsbCxcbiAgICAgIGJvcmRlcldpZHRoOiAwLFxuICAgICAgc2hhZG93Qmx1cjogMyxcbiAgICAgIHNoYWRvd0NvbG9yOiAnI2FhYScgLy8gQ29uc2lkZXJpbmcgYXBwbGljYWJpbGl0eSwgY29tbW9uIHN0eWxlIHNob3VsZFxuICAgICAgLy8gYmV0dGVyIG5vdCBoYXZlIHNoYWRvd09mZnNldC5cbiAgICAgIC8vIHNoYWRvd09mZnNldFg6IDAsXG4gICAgICAvLyBzaGFkb3dPZmZzZXRZOiAyXG5cbiAgICB9LFxuICAgIGhhbmRsZToge1xuICAgICAgc2hvdzogZmFsc2UsXG5cbiAgICAgIC8qIGVzbGludC1kaXNhYmxlICovXG4gICAgICBpY29uOiAnTTEwLjcsMTEuOXYtMS4zSDkuM3YxLjNjLTQuOSwwLjMtOC44LDQuNC04LjgsOS40YzAsNSwzLjksOS4xLDguOCw5LjRoMS4zYzQuOS0wLjMsOC44LTQuNCw4LjgtOS40QzE5LjUsMTYuMywxNS42LDEyLjIsMTAuNywxMS45eiBNMTMuMywyNC40SDYuN3YtMS4yaDYuNnogTTEzLjMsMjJINi43di0xLjJoNi42eiBNMTMuMywxOS42SDYuN3YtMS4yaDYuNnonLFxuICAgICAgLy8ganNoaW50IGlnbm9yZTpsaW5lXG5cbiAgICAgIC8qIGVzbGludC1lbmFibGUgKi9cbiAgICAgIHNpemU6IDQ1LFxuICAgICAgLy8gaGFuZGxlIG1hcmdpbiBpcyBmcm9tIHN5bWJvbCBjZW50ZXIgdG8gYXhpcywgd2hpY2ggaXMgc3RhYmxlIHdoZW4gY2lyY3VsYXIgbW92ZS5cbiAgICAgIG1hcmdpbjogNTAsXG4gICAgICAvLyBjb2xvcjogJyMxYjhiYmQnXG4gICAgICAvLyBjb2xvcjogJyMyZjQ1NTQnXG4gICAgICBjb2xvcjogJyMzMzMnLFxuICAgICAgc2hhZG93Qmx1cjogMyxcbiAgICAgIHNoYWRvd0NvbG9yOiAnI2FhYScsXG4gICAgICBzaGFkb3dPZmZzZXRYOiAwLFxuICAgICAgc2hhZG93T2Zmc2V0WTogMixcbiAgICAgIC8vIEZvciBtb2JpbGUgcGVyZm9ybWFuY2VcbiAgICAgIHRocm90dGxlOiA0MFxuICAgIH1cbiAgfVxufSk7XG52YXIgX2RlZmF1bHQgPSBBeGlzUG9pbnRlck1vZGVsO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbEJBO0FBb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUE3REE7QUFIQTtBQXVGQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axisPointer/AxisPointerModel.js
