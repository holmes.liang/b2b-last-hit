/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule BlockTree
 * @format
 * 
 */


var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var emptyFunction = __webpack_require__(/*! fbjs/lib/emptyFunction */ "./node_modules/fbjs/lib/emptyFunction.js");

var findRangesImmutable = __webpack_require__(/*! ./findRangesImmutable */ "./node_modules/draft-js/lib/findRangesImmutable.js");

var List = Immutable.List,
    Repeat = Immutable.Repeat,
    Record = Immutable.Record;
var returnTrue = emptyFunction.thatReturnsTrue;
var FINGERPRINT_DELIMITER = '-';
var defaultLeafRange = {
  start: null,
  end: null
};
var LeafRange = Record(defaultLeafRange);
var defaultDecoratorRange = {
  start: null,
  end: null,
  decoratorKey: null,
  leaves: null
};
var DecoratorRange = Record(defaultDecoratorRange);
var BlockTree = {
  /**
   * Generate a block tree for a given ContentBlock/decorator pair.
   */
  generate: function generate(contentState, block, decorator) {
    var textLength = block.getLength();

    if (!textLength) {
      return List.of(new DecoratorRange({
        start: 0,
        end: 0,
        decoratorKey: null,
        leaves: List.of(new LeafRange({
          start: 0,
          end: 0
        }))
      }));
    }

    var leafSets = [];
    var decorations = decorator ? decorator.getDecorations(block, contentState) : List(Repeat(null, textLength));
    var chars = block.getCharacterList();
    findRangesImmutable(decorations, areEqual, returnTrue, function (start, end) {
      leafSets.push(new DecoratorRange({
        start: start,
        end: end,
        decoratorKey: decorations.get(start),
        leaves: generateLeaves(chars.slice(start, end).toList(), start)
      }));
    });
    return List(leafSets);
  },

  /**
   * Create a string representation of the given tree map. This allows us
   * to rapidly determine whether a tree has undergone a significant
   * structural change.
   */
  getFingerprint: function getFingerprint(tree) {
    return tree.map(function (leafSet) {
      var decoratorKey = leafSet.get('decoratorKey');
      var fingerprintString = decoratorKey !== null ? decoratorKey + '.' + (leafSet.get('end') - leafSet.get('start')) : '';
      return '' + fingerprintString + '.' + leafSet.get('leaves').size;
    }).join(FINGERPRINT_DELIMITER);
  }
};
/**
 * Generate LeafRange records for a given character list.
 */

function generateLeaves(characters, offset) {
  var leaves = [];
  var inlineStyles = characters.map(function (c) {
    return c.getStyle();
  }).toList();
  findRangesImmutable(inlineStyles, areEqual, returnTrue, function (start, end) {
    leaves.push(new LeafRange({
      start: start + offset,
      end: end + offset
    }));
  });
  return List(leaves);
}

function areEqual(a, b) {
  return a === b;
}

module.exports = BlockTree;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0Jsb2NrVHJlZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9CbG9ja1RyZWUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBCbG9ja1RyZWVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgZW1wdHlGdW5jdGlvbiA9IHJlcXVpcmUoJ2ZianMvbGliL2VtcHR5RnVuY3Rpb24nKTtcbnZhciBmaW5kUmFuZ2VzSW1tdXRhYmxlID0gcmVxdWlyZSgnLi9maW5kUmFuZ2VzSW1tdXRhYmxlJyk7XG5cbnZhciBMaXN0ID0gSW1tdXRhYmxlLkxpc3QsXG4gICAgUmVwZWF0ID0gSW1tdXRhYmxlLlJlcGVhdCxcbiAgICBSZWNvcmQgPSBJbW11dGFibGUuUmVjb3JkO1xuXG5cbnZhciByZXR1cm5UcnVlID0gZW1wdHlGdW5jdGlvbi50aGF0UmV0dXJuc1RydWU7XG5cbnZhciBGSU5HRVJQUklOVF9ERUxJTUlURVIgPSAnLSc7XG5cbnZhciBkZWZhdWx0TGVhZlJhbmdlID0ge1xuICBzdGFydDogbnVsbCxcbiAgZW5kOiBudWxsXG59O1xuXG52YXIgTGVhZlJhbmdlID0gUmVjb3JkKGRlZmF1bHRMZWFmUmFuZ2UpO1xuXG52YXIgZGVmYXVsdERlY29yYXRvclJhbmdlID0ge1xuICBzdGFydDogbnVsbCxcbiAgZW5kOiBudWxsLFxuICBkZWNvcmF0b3JLZXk6IG51bGwsXG4gIGxlYXZlczogbnVsbFxufTtcblxudmFyIERlY29yYXRvclJhbmdlID0gUmVjb3JkKGRlZmF1bHREZWNvcmF0b3JSYW5nZSk7XG5cbnZhciBCbG9ja1RyZWUgPSB7XG4gIC8qKlxuICAgKiBHZW5lcmF0ZSBhIGJsb2NrIHRyZWUgZm9yIGEgZ2l2ZW4gQ29udGVudEJsb2NrL2RlY29yYXRvciBwYWlyLlxuICAgKi9cbiAgZ2VuZXJhdGU6IGZ1bmN0aW9uIGdlbmVyYXRlKGNvbnRlbnRTdGF0ZSwgYmxvY2ssIGRlY29yYXRvcikge1xuICAgIHZhciB0ZXh0TGVuZ3RoID0gYmxvY2suZ2V0TGVuZ3RoKCk7XG4gICAgaWYgKCF0ZXh0TGVuZ3RoKSB7XG4gICAgICByZXR1cm4gTGlzdC5vZihuZXcgRGVjb3JhdG9yUmFuZ2Uoe1xuICAgICAgICBzdGFydDogMCxcbiAgICAgICAgZW5kOiAwLFxuICAgICAgICBkZWNvcmF0b3JLZXk6IG51bGwsXG4gICAgICAgIGxlYXZlczogTGlzdC5vZihuZXcgTGVhZlJhbmdlKHsgc3RhcnQ6IDAsIGVuZDogMCB9KSlcbiAgICAgIH0pKTtcbiAgICB9XG5cbiAgICB2YXIgbGVhZlNldHMgPSBbXTtcbiAgICB2YXIgZGVjb3JhdGlvbnMgPSBkZWNvcmF0b3IgPyBkZWNvcmF0b3IuZ2V0RGVjb3JhdGlvbnMoYmxvY2ssIGNvbnRlbnRTdGF0ZSkgOiBMaXN0KFJlcGVhdChudWxsLCB0ZXh0TGVuZ3RoKSk7XG5cbiAgICB2YXIgY2hhcnMgPSBibG9jay5nZXRDaGFyYWN0ZXJMaXN0KCk7XG5cbiAgICBmaW5kUmFuZ2VzSW1tdXRhYmxlKGRlY29yYXRpb25zLCBhcmVFcXVhbCwgcmV0dXJuVHJ1ZSwgZnVuY3Rpb24gKHN0YXJ0LCBlbmQpIHtcbiAgICAgIGxlYWZTZXRzLnB1c2gobmV3IERlY29yYXRvclJhbmdlKHtcbiAgICAgICAgc3RhcnQ6IHN0YXJ0LFxuICAgICAgICBlbmQ6IGVuZCxcbiAgICAgICAgZGVjb3JhdG9yS2V5OiBkZWNvcmF0aW9ucy5nZXQoc3RhcnQpLFxuICAgICAgICBsZWF2ZXM6IGdlbmVyYXRlTGVhdmVzKGNoYXJzLnNsaWNlKHN0YXJ0LCBlbmQpLnRvTGlzdCgpLCBzdGFydClcbiAgICAgIH0pKTtcbiAgICB9KTtcblxuICAgIHJldHVybiBMaXN0KGxlYWZTZXRzKTtcbiAgfSxcblxuICAvKipcbiAgICogQ3JlYXRlIGEgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoZSBnaXZlbiB0cmVlIG1hcC4gVGhpcyBhbGxvd3MgdXNcbiAgICogdG8gcmFwaWRseSBkZXRlcm1pbmUgd2hldGhlciBhIHRyZWUgaGFzIHVuZGVyZ29uZSBhIHNpZ25pZmljYW50XG4gICAqIHN0cnVjdHVyYWwgY2hhbmdlLlxuICAgKi9cbiAgZ2V0RmluZ2VycHJpbnQ6IGZ1bmN0aW9uIGdldEZpbmdlcnByaW50KHRyZWUpIHtcbiAgICByZXR1cm4gdHJlZS5tYXAoZnVuY3Rpb24gKGxlYWZTZXQpIHtcbiAgICAgIHZhciBkZWNvcmF0b3JLZXkgPSBsZWFmU2V0LmdldCgnZGVjb3JhdG9yS2V5Jyk7XG4gICAgICB2YXIgZmluZ2VycHJpbnRTdHJpbmcgPSBkZWNvcmF0b3JLZXkgIT09IG51bGwgPyBkZWNvcmF0b3JLZXkgKyAnLicgKyAobGVhZlNldC5nZXQoJ2VuZCcpIC0gbGVhZlNldC5nZXQoJ3N0YXJ0JykpIDogJyc7XG4gICAgICByZXR1cm4gJycgKyBmaW5nZXJwcmludFN0cmluZyArICcuJyArIGxlYWZTZXQuZ2V0KCdsZWF2ZXMnKS5zaXplO1xuICAgIH0pLmpvaW4oRklOR0VSUFJJTlRfREVMSU1JVEVSKTtcbiAgfVxufTtcblxuLyoqXG4gKiBHZW5lcmF0ZSBMZWFmUmFuZ2UgcmVjb3JkcyBmb3IgYSBnaXZlbiBjaGFyYWN0ZXIgbGlzdC5cbiAqL1xuZnVuY3Rpb24gZ2VuZXJhdGVMZWF2ZXMoY2hhcmFjdGVycywgb2Zmc2V0KSB7XG4gIHZhciBsZWF2ZXMgPSBbXTtcbiAgdmFyIGlubGluZVN0eWxlcyA9IGNoYXJhY3RlcnMubWFwKGZ1bmN0aW9uIChjKSB7XG4gICAgcmV0dXJuIGMuZ2V0U3R5bGUoKTtcbiAgfSkudG9MaXN0KCk7XG4gIGZpbmRSYW5nZXNJbW11dGFibGUoaW5saW5lU3R5bGVzLCBhcmVFcXVhbCwgcmV0dXJuVHJ1ZSwgZnVuY3Rpb24gKHN0YXJ0LCBlbmQpIHtcbiAgICBsZWF2ZXMucHVzaChuZXcgTGVhZlJhbmdlKHtcbiAgICAgIHN0YXJ0OiBzdGFydCArIG9mZnNldCxcbiAgICAgIGVuZDogZW5kICsgb2Zmc2V0XG4gICAgfSkpO1xuICB9KTtcbiAgcmV0dXJuIExpc3QobGVhdmVzKTtcbn1cblxuZnVuY3Rpb24gYXJlRXF1YWwoYSwgYikge1xuICByZXR1cm4gYSA9PT0gYjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBCbG9ja1RyZWU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUtBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFFQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBRUE7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUEzQ0E7QUE4Q0E7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/BlockTree.js
