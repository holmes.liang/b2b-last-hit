

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _noImportant = __webpack_require__(/*! aphrodite/no-important */ "./node_modules/aphrodite/no-important.js");

var _theme = __webpack_require__(/*! ../theme */ "./node_modules/react-images/lib/theme.js");

var _theme2 = _interopRequireDefault(_theme);

var _deepMerge = __webpack_require__(/*! ../utils/deepMerge */ "./node_modules/react-images/lib/utils/deepMerge.js");

var _deepMerge2 = _interopRequireDefault(_deepMerge);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};

  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }

  return target;
}

function Footer(_ref, _ref2) {
  var theme = _ref2.theme;

  var caption = _ref.caption,
      countCurrent = _ref.countCurrent,
      countSeparator = _ref.countSeparator,
      countTotal = _ref.countTotal,
      showCount = _ref.showCount,
      props = _objectWithoutProperties(_ref, ['caption', 'countCurrent', 'countSeparator', 'countTotal', 'showCount']);

  if (!caption && !showCount) return null;

  var classes = _noImportant.StyleSheet.create((0, _deepMerge2.default)(defaultStyles, theme));

  var imageCount = showCount ? _react2.default.createElement('div', {
    className: (0, _noImportant.css)(classes.footerCount)
  }, countCurrent, countSeparator, countTotal) : _react2.default.createElement('span', null);
  return _react2.default.createElement('div', _extends({
    className: (0, _noImportant.css)(classes.footer)
  }, props), caption ? _react2.default.createElement('figcaption', {
    className: (0, _noImportant.css)(classes.footerCaption)
  }, caption) : _react2.default.createElement('span', null), imageCount);
}

Footer.propTypes = {
  caption: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.element]),
  countCurrent: _propTypes2.default.number,
  countSeparator: _propTypes2.default.string,
  countTotal: _propTypes2.default.number,
  showCount: _propTypes2.default.bool
};
Footer.contextTypes = {
  theme: _propTypes2.default.object.isRequired
};
var defaultStyles = {
  footer: {
    boxSizing: 'border-box',
    color: _theme2.default.footer.color,
    cursor: 'auto',
    display: 'flex',
    justifyContent: 'space-between',
    left: 0,
    lineHeight: 1.3,
    paddingBottom: _theme2.default.footer.gutter.vertical,
    paddingLeft: _theme2.default.footer.gutter.horizontal,
    paddingRight: _theme2.default.footer.gutter.horizontal,
    paddingTop: _theme2.default.footer.gutter.vertical
  },
  footerCount: {
    color: _theme2.default.footer.count.color,
    fontSize: _theme2.default.footer.count.fontSize,
    paddingLeft: '1em' // add a small gutter for the caption

  },
  footerCaption: {
    flex: '1 1 0'
  }
};
exports.default = Footer;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtaW1hZ2VzL2xpYi9jb21wb25lbnRzL0Zvb3Rlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JlYWN0LWltYWdlcy9saWIvY29tcG9uZW50cy9Gb290ZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX25vSW1wb3J0YW50ID0gcmVxdWlyZSgnYXBocm9kaXRlL25vLWltcG9ydGFudCcpO1xuXG52YXIgX3RoZW1lID0gcmVxdWlyZSgnLi4vdGhlbWUnKTtcblxudmFyIF90aGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90aGVtZSk7XG5cbnZhciBfZGVlcE1lcmdlID0gcmVxdWlyZSgnLi4vdXRpbHMvZGVlcE1lcmdlJyk7XG5cbnZhciBfZGVlcE1lcmdlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZXBNZXJnZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gRm9vdGVyKF9yZWYsIF9yZWYyKSB7XG5cdHZhciB0aGVtZSA9IF9yZWYyLnRoZW1lO1xuXG5cdHZhciBjYXB0aW9uID0gX3JlZi5jYXB0aW9uLFxuXHQgICAgY291bnRDdXJyZW50ID0gX3JlZi5jb3VudEN1cnJlbnQsXG5cdCAgICBjb3VudFNlcGFyYXRvciA9IF9yZWYuY291bnRTZXBhcmF0b3IsXG5cdCAgICBjb3VudFRvdGFsID0gX3JlZi5jb3VudFRvdGFsLFxuXHQgICAgc2hvd0NvdW50ID0gX3JlZi5zaG93Q291bnQsXG5cdCAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcmVmLCBbJ2NhcHRpb24nLCAnY291bnRDdXJyZW50JywgJ2NvdW50U2VwYXJhdG9yJywgJ2NvdW50VG90YWwnLCAnc2hvd0NvdW50J10pO1xuXG5cdGlmICghY2FwdGlvbiAmJiAhc2hvd0NvdW50KSByZXR1cm4gbnVsbDtcblxuXHR2YXIgY2xhc3NlcyA9IF9ub0ltcG9ydGFudC5TdHlsZVNoZWV0LmNyZWF0ZSgoMCwgX2RlZXBNZXJnZTIuZGVmYXVsdCkoZGVmYXVsdFN0eWxlcywgdGhlbWUpKTtcblxuXHR2YXIgaW1hZ2VDb3VudCA9IHNob3dDb3VudCA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuXHRcdCdkaXYnLFxuXHRcdHsgY2xhc3NOYW1lOiAoMCwgX25vSW1wb3J0YW50LmNzcykoY2xhc3Nlcy5mb290ZXJDb3VudCkgfSxcblx0XHRjb3VudEN1cnJlbnQsXG5cdFx0Y291bnRTZXBhcmF0b3IsXG5cdFx0Y291bnRUb3RhbFxuXHQpIDogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCBudWxsKTtcblxuXHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0J2RpdicsXG5cdFx0X2V4dGVuZHMoeyBjbGFzc05hbWU6ICgwLCBfbm9JbXBvcnRhbnQuY3NzKShjbGFzc2VzLmZvb3RlcikgfSwgcHJvcHMpLFxuXHRcdGNhcHRpb24gPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcblx0XHRcdCdmaWdjYXB0aW9uJyxcblx0XHRcdHsgY2xhc3NOYW1lOiAoMCwgX25vSW1wb3J0YW50LmNzcykoY2xhc3Nlcy5mb290ZXJDYXB0aW9uKSB9LFxuXHRcdFx0Y2FwdGlvblxuXHRcdCkgOiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnc3BhbicsIG51bGwpLFxuXHRcdGltYWdlQ291bnRcblx0KTtcbn1cblxuRm9vdGVyLnByb3BUeXBlcyA9IHtcblx0Y2FwdGlvbjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmVsZW1lbnRdKSxcblx0Y291bnRDdXJyZW50OiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcblx0Y291bnRTZXBhcmF0b3I6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuXHRjb3VudFRvdGFsOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm51bWJlcixcblx0c2hvd0NvdW50OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2xcbn07XG5Gb290ZXIuY29udGV4dFR5cGVzID0ge1xuXHR0aGVtZTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZFxufTtcblxudmFyIGRlZmF1bHRTdHlsZXMgPSB7XG5cdGZvb3Rlcjoge1xuXHRcdGJveFNpemluZzogJ2JvcmRlci1ib3gnLFxuXHRcdGNvbG9yOiBfdGhlbWUyLmRlZmF1bHQuZm9vdGVyLmNvbG9yLFxuXHRcdGN1cnNvcjogJ2F1dG8nLFxuXHRcdGRpc3BsYXk6ICdmbGV4Jyxcblx0XHRqdXN0aWZ5Q29udGVudDogJ3NwYWNlLWJldHdlZW4nLFxuXHRcdGxlZnQ6IDAsXG5cdFx0bGluZUhlaWdodDogMS4zLFxuXHRcdHBhZGRpbmdCb3R0b206IF90aGVtZTIuZGVmYXVsdC5mb290ZXIuZ3V0dGVyLnZlcnRpY2FsLFxuXHRcdHBhZGRpbmdMZWZ0OiBfdGhlbWUyLmRlZmF1bHQuZm9vdGVyLmd1dHRlci5ob3Jpem9udGFsLFxuXHRcdHBhZGRpbmdSaWdodDogX3RoZW1lMi5kZWZhdWx0LmZvb3Rlci5ndXR0ZXIuaG9yaXpvbnRhbCxcblx0XHRwYWRkaW5nVG9wOiBfdGhlbWUyLmRlZmF1bHQuZm9vdGVyLmd1dHRlci52ZXJ0aWNhbFxuXHR9LFxuXHRmb290ZXJDb3VudDoge1xuXHRcdGNvbG9yOiBfdGhlbWUyLmRlZmF1bHQuZm9vdGVyLmNvdW50LmNvbG9yLFxuXHRcdGZvbnRTaXplOiBfdGhlbWUyLmRlZmF1bHQuZm9vdGVyLmNvdW50LmZvbnRTaXplLFxuXHRcdHBhZGRpbmdMZWZ0OiAnMWVtJyAvLyBhZGQgYSBzbWFsbCBndXR0ZXIgZm9yIHRoZSBjYXB0aW9uXG5cdH0sXG5cdGZvb3RlckNhcHRpb246IHtcblx0XHRmbGV4OiAnMSAxIDAnXG5cdH1cbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvb3RlcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBTUE7QUFFQTtBQUFBO0FBR0E7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUNBO0FBREE7QUFuQkE7QUF3QkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-images/lib/components/Footer.js
