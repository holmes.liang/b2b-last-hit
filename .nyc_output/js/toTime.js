__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "goStartMonth", function() { return goStartMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "goEndMonth", function() { return goEndMonth; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "goTime", function() { return goTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "includesTime", function() { return includesTime; });
function goStartMonth(time) {
  return time.clone().startOf('month');
}
function goEndMonth(time) {
  return time.clone().endOf('month');
}
function goTime(time, direction, unit) {
  return time.clone().add(direction, unit);
}
function includesTime() {
  var timeList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var time = arguments[1];
  var unit = arguments[2];
  return timeList.some(function (t) {
    return t.isSame(time, unit);
  });
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvdXRpbC90b1RpbWUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1jYWxlbmRhci9lcy91dGlsL3RvVGltZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gZ29TdGFydE1vbnRoKHRpbWUpIHtcbiAgcmV0dXJuIHRpbWUuY2xvbmUoKS5zdGFydE9mKCdtb250aCcpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ29FbmRNb250aCh0aW1lKSB7XG4gIHJldHVybiB0aW1lLmNsb25lKCkuZW5kT2YoJ21vbnRoJyk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnb1RpbWUodGltZSwgZGlyZWN0aW9uLCB1bml0KSB7XG4gIHJldHVybiB0aW1lLmNsb25lKCkuYWRkKGRpcmVjdGlvbiwgdW5pdCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbmNsdWRlc1RpbWUoKSB7XG4gIHZhciB0aW1lTGlzdCA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogW107XG4gIHZhciB0aW1lID0gYXJndW1lbnRzWzFdO1xuICB2YXIgdW5pdCA9IGFyZ3VtZW50c1syXTtcblxuICByZXR1cm4gdGltZUxpc3Quc29tZShmdW5jdGlvbiAodCkge1xuICAgIHJldHVybiB0LmlzU2FtZSh0aW1lLCB1bml0KTtcbiAgfSk7XG59Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/util/toTime.js
