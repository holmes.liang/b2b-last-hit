__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  .mobile-total {\n      font-size: 13px;\n      text-transform: uppercase;\n      color: rgba(0, 0, 0, 0.55);\n      vertical-align: text-bottom;\n      label {\n        color: ", ";\n        font-weight: 600;\n        font-size: 16px;\n        margin-left: 10px;\n      }\n  }\n  .mobile-detail {\n    margin: 0 0 15px;\n    border: 1px solid #ebebeb;\n    border-radius: 5px;\n    .mobile-title {\n      line-height: 32px;\n      margin: 0;\n      padding: 6px 15px;\n      border-bottom: 1px solid #ebebeb;\n    }\n    .mobile-content {\n      padding: 10px 16px;\n      color: #323232;\n    }\n    .p-mobile {\n       font-size: 16px;\n        line-height: 2;\n        display: -ms-flexbox;\n        display: flex;\n        -ms-flex-pack: justify;\n        justify-content: space-between;\n        -ms-flex-align: center;\n        align-items: center;\n      span {\n        margin-right: .5em;\n        font-size: 13px;\n        text-transform: uppercase;\n        color: rgba(0, 0, 0, 0.55);\n        vertical-align: text-bottom;\n      }\n    }\n  }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



var define = _styles__WEBPACK_IMPORTED_MODULE_2__["default"].getTheme(name);
/* harmony default export */ __webpack_exports__["default"] = ({
  DetailItem: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject(), define.COLOR_A)
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svYmNwL2JpbGwtcGF5bWVudC9kZXRhaWwtaXRlbS1zdHlsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9iY3AvYmlsbC1wYXltZW50L2RldGFpbC1pdGVtLXN0eWxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBTdHlsZXMgZnJvbSBcIkBzdHlsZXNcIjtcblxuY29uc3QgZGVmaW5lID0gU3R5bGVzLmdldFRoZW1lKG5hbWUpO1xuZXhwb3J0IGRlZmF1bHQge1xuICBEZXRhaWxJdGVtOiBTdHlsZWQuZGl2YFxuICAubW9iaWxlLXRvdGFsIHtcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjU1KTtcbiAgICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LWJvdHRvbTtcbiAgICAgIGxhYmVsIHtcbiAgICAgICAgY29sb3I6ICR7ZGVmaW5lLkNPTE9SX0F9O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgfVxuICB9XG4gIC5tb2JpbGUtZGV0YWlsIHtcbiAgICBtYXJnaW46IDAgMCAxNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIC5tb2JpbGUtdGl0bGUge1xuICAgICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgICBtYXJnaW46IDA7XG4gICAgICBwYWRkaW5nOiA2cHggMTVweDtcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJlYmViO1xuICAgIH1cbiAgICAubW9iaWxlLWNvbnRlbnQge1xuICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xuICAgICAgY29sb3I6ICMzMjMyMzI7XG4gICAgfVxuICAgIC5wLW1vYmlsZSB7XG4gICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMjtcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGp1c3RpZnk7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgIHNwYW4ge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IC41ZW07XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41NSk7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiB0ZXh0LWJvdHRvbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgYCxcbn07Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/bcp/bill-payment/detail-item-style.tsx
