__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MyStringUtils; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);




var MyStringUtils =
/*#__PURE__*/
function () {
  function MyStringUtils() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, MyStringUtils);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(MyStringUtils, null, [{
    key: "toPercent",
    value: function toPercent(data) {
      if (!lodash__WEBPACK_IMPORTED_MODULE_2___default.a.isNumber(data)) return "";
      var data1 = (data * 100).toFixed(2);
      return "".concat(data1, "%");
    }
  }]);

  return MyStringUtils;
}();

MyStringUtils.textEllipsis = function (text) {
  var len = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 50;

  if (text && text.length >= len) {
    return text.substr(0, len) + "...";
  }

  return text;
};

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3V0aWxzL3N0cmluZy11dGlscy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21tb24vdXRpbHMvc3RyaW5nLXV0aWxzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE15U3RyaW5nVXRpbHMge1xuXG4gIHN0YXRpYyB0b1BlcmNlbnQoZGF0YTogbnVtYmVyKTogc3RyaW5nIHtcbiAgICBpZiAoIV8uaXNOdW1iZXIoZGF0YSkpIHJldHVybiBcIlwiO1xuICAgIGNvbnN0IGRhdGExID0gKGRhdGEgKiAxMDApLnRvRml4ZWQoMik7XG4gICAgcmV0dXJuIGAke2RhdGExfSVgO1xuICB9XG5cbiAgc3RhdGljIHRleHRFbGxpcHNpcyA9ICh0ZXh0OiBzdHJpbmcsIGxlbjogbnVtYmVyID0gNTApID0+IHtcbiAgICBpZiAodGV4dCAmJiB0ZXh0Lmxlbmd0aCA+PSBsZW4pIHtcbiAgICAgIHJldHVybiB0ZXh0LnN1YnN0cigwLCBsZW4pICsgXCIuLi5cIjtcbiAgICB9XG4gICAgcmV0dXJuIHRleHQ7XG4gIH07XG5cbn1cblxuXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBOzs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFOQTtBQVFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/utils/string-utils.tsx
