__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableActionBars", function() { return TableActionBars; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/table/action-bars.tsx";



var TableActionBars = function TableActionBars(props) {
  var firstSort = ["Work on"];
  var sortActions = props.actions.filter(function (item) {
    return item.isShow !== false;
  }).sort(function (a, b) {
    if (firstSort.includes(a.actionName)) return -1;else if (firstSort.includes(b.actionName)) return 1;
    return 0;
  });

  if (sortActions.length > 2) {
    var menu = sortActions.map(function (item) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"].Item, {
        key: item.actionName,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        },
        __self: this
      }, item.render && item.render(), item.handel && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        className: "button",
        onClick: function onClick() {
          item.handel && item.handel();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        },
        __self: this
      }, item.actionName));
    });
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
      overlay: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Menu"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 36
        },
        __self: this
      }, menu),
      key: "dropdown",
      trigger: ["hover", "click"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        cursor: "pointer"
      },
      className: "ant-dropdown-link button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("More").thai("เรียกดู").getMessage(), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
      type: "down",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 39
      },
      __self: this
    }))));
  }

  var _result = sortActions.map(function (item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, item.render && item.render(), item.handel && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        cursor: "pointer"
      },
      onClick: function onClick() {
        item.handel && item.handel();
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46
      },
      __self: this
    }, item.actionName));
  });

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].joinElements(_result, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_1__["Divider"], {
    type: "vertical",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  })));
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3RhYmxlL2FjdGlvbi1iYXJzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC90YWJsZS9hY3Rpb24tYmFycy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgRGl2aWRlciwgRHJvcGRvd24sIEljb24sIE1lbnUgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcblxuXG5leHBvcnQgdHlwZSBJQWN0aW9uID0ge1xuICBhY3Rpb25OYW1lOiBzdHJpbmc7XG4gIGlzU2hvdz86IGJvb2xlYW47XG4gIGhhbmRlbD86IEZ1bmN0aW9uO1xuICByZW5kZXI/OiBhbnk7XG59XG5cbmludGVyZmFjZSBJVGFibGVBY3Rpb25CYXJzUHJvcHMge1xuICBhY3Rpb25zOiBJQWN0aW9uW11cbn1cblxuZXhwb3J0IGNvbnN0IFRhYmxlQWN0aW9uQmFyczogUmVhY3QuRkM8SVRhYmxlQWN0aW9uQmFyc1Byb3BzPiA9IChwcm9wcykgPT4ge1xuICBjb25zdCBmaXJzdFNvcnQgPSBbXCJXb3JrIG9uXCJdO1xuICBjb25zdCBzb3J0QWN0aW9uczogSUFjdGlvbltdID0gcHJvcHMuYWN0aW9ucy5maWx0ZXIoKGl0ZW06IElBY3Rpb24pID0+IGl0ZW0uaXNTaG93ICE9PSBmYWxzZSkuc29ydCgoYTogSUFjdGlvbiwgYjogSUFjdGlvbikgPT4ge1xuICAgIGlmIChmaXJzdFNvcnQuaW5jbHVkZXMoYS5hY3Rpb25OYW1lKSkgcmV0dXJuIC0xO1xuICAgIGVsc2UgaWYgKGZpcnN0U29ydC5pbmNsdWRlcyhiLmFjdGlvbk5hbWUpKSByZXR1cm4gMTtcbiAgICByZXR1cm4gMDtcbiAgfSk7XG4gIGlmIChzb3J0QWN0aW9ucy5sZW5ndGggPiAyKSB7XG4gICAgY29uc3QgbWVudSA9IHNvcnRBY3Rpb25zLm1hcCgoaXRlbTogSUFjdGlvbikgPT4gKFxuICAgICAgPE1lbnUuSXRlbSBrZXk9e2l0ZW0uYWN0aW9uTmFtZX0+XG4gICAgICAgIHtpdGVtLnJlbmRlciAmJiBpdGVtLnJlbmRlcigpfVxuICAgICAgICB7aXRlbS5oYW5kZWwgJiYgPGEgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIGl0ZW0uaGFuZGVsICYmIGl0ZW0uaGFuZGVsKCk7XG4gICAgICAgIH19PlxuICAgICAgICAgIHtpdGVtLmFjdGlvbk5hbWV9XG4gICAgICAgIDwvYT59XG4gICAgICA8L01lbnUuSXRlbT5cbiAgICApKTtcbiAgICByZXR1cm4gKDxkaXY+XG4gICAgICA8RHJvcGRvd24gb3ZlcmxheT17PE1lbnU+e21lbnV9PC9NZW51Pn0ga2V5PVwiZHJvcGRvd25cIiB0cmlnZ2VyPXtbXCJob3ZlclwiLCBcImNsaWNrXCJdfT5cbiAgICAgICAgPHNwYW4gc3R5bGU9e3sgY3Vyc29yOiBcInBvaW50ZXJcIiB9fSBjbGFzc05hbWU9e2BhbnQtZHJvcGRvd24tbGluayBidXR0b25gfT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJNb3JlXCIpLnRoYWkoXCLguYDguKPguLXguKLguIHguJTguLlcIikuZ2V0TWVzc2FnZSgpfXtcIiBcIn1cbiAgICAgICAgICA8SWNvbiB0eXBlPVwiZG93blwiLz5cbiAgICAgICAgPC9zcGFuPlxuICAgICAgPC9Ecm9wZG93bj5cbiAgICA8L2Rpdj4pO1xuICB9XG4gIGNvbnN0IF9yZXN1bHQgPSBzb3J0QWN0aW9ucy5tYXAoKGl0ZW06IElBY3Rpb24pID0+ICg8PlxuICAgIHtpdGVtLnJlbmRlciAmJiBpdGVtLnJlbmRlcigpfVxuICAgIHtpdGVtLmhhbmRlbCAmJiA8c3BhbiBzdHlsZT17eyBjdXJzb3I6IFwicG9pbnRlclwiIH19IG9uQ2xpY2s9eygpID0+IHtcbiAgICAgIGl0ZW0uaGFuZGVsICYmIGl0ZW0uaGFuZGVsKCk7XG4gICAgfX0+XG4gICAgICB7aXRlbS5hY3Rpb25OYW1lfVxuICAgIDwvc3Bhbj59XG4gIDwvPikpO1xuICByZXR1cm4gKDw+XG4gICAge1V0aWxzLmpvaW5FbGVtZW50cyhfcmVzdWx0LCA8RGl2aWRlciB0eXBlPVwidmVydGljYWxcIi8+KX1cbiAgPC8+KTtcbn07XG5cblxuXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFjQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQ0E7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/table/action-bars.tsx
