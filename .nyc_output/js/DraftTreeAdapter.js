

var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftTreeAdapter
 * @format
 * 
 *
 * This is unstable and not part of the public API and should not be used by
 * production systems. This file may be update/removed without notice.
 */


var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var traverseInDepthOrder = function traverseInDepthOrder(blocks, fn) {
  var stack = [].concat(blocks).reverse();

  while (stack.length) {
    var _block = stack.pop();

    fn(_block);
    var children = _block.children;
    !Array.isArray(children) ?  true ? invariant(false, 'Invalid tree raw block') : undefined : void 0;
    stack = stack.concat([].concat(children.reverse()));
  }
};

var isListBlock = function isListBlock(block) {
  if (!(block && block.type)) {
    return false;
  }

  var type = block.type;
  return type === 'unordered-list-item' || type === 'ordered-list-item';
};

var addDepthToChildren = function addDepthToChildren(block) {
  if (Array.isArray(block.children)) {
    block.children = block.children.map(function (child) {
      return child.type === block.type ? _extends({}, child, {
        depth: (block.depth || 0) + 1
      }) : child;
    });
  }
};
/**
 * This adapter is intended to be be used as an adapter to draft tree data
 *
 * draft state <=====> draft tree state
 */


var DraftTreeAdapter = {
  /**
   * Converts from a tree raw state back to  draft raw state
   */
  fromRawTreeStateToRawState: function fromRawTreeStateToRawState(draftTreeState) {
    var blocks = draftTreeState.blocks;
    var transformedBlocks = [];
    !Array.isArray(blocks) ?  true ? invariant(false, 'Invalid raw state') : undefined : void 0;

    if (!Array.isArray(blocks) || !blocks.length) {
      return draftTreeState;
    }

    traverseInDepthOrder(blocks, function (block) {
      var newBlock = _extends({}, block);

      if (isListBlock(block)) {
        newBlock.depth = newBlock.depth || 0;
        addDepthToChildren(block);
      }

      delete newBlock.children;
      transformedBlocks.push(newBlock);
    });
    draftTreeState.blocks = transformedBlocks;
    return _extends({}, draftTreeState, {
      blocks: transformedBlocks
    });
  },

  /**
   * Converts from draft raw state to tree draft state
   */
  fromRawStateToRawTreeState: function fromRawStateToRawTreeState(draftState) {
    var lastListDepthCacheRef = {};
    var transformedBlocks = [];
    draftState.blocks.forEach(function (block) {
      var isList = isListBlock(block);
      var depth = block.depth || 0;

      var treeBlock = _extends({}, block, {
        children: []
      });

      if (!isList) {
        // reset the cache path
        lastListDepthCacheRef = {};
        transformedBlocks.push(treeBlock);
        return;
      } // update our depth cache reference path


      lastListDepthCacheRef[depth] = treeBlock; // if we are greater than zero we must have seen a parent already

      if (depth > 0) {
        var parent = lastListDepthCacheRef[depth - 1];
        !parent ?  true ? invariant(false, 'Invalid depth for RawDraftContentBlock') : undefined : void 0; // push nested list blocks

        parent.children.push(treeBlock);
        return;
      } // push root list blocks


      transformedBlocks.push(treeBlock);
    });
    return _extends({}, draftState, {
      blocks: transformedBlocks
    });
  }
};
module.exports = DraftTreeAdapter;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0VHJlZUFkYXB0ZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRUcmVlQWRhcHRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbnZhciBfYXNzaWduID0gcmVxdWlyZSgnb2JqZWN0LWFzc2lnbicpO1xuXG52YXIgX2V4dGVuZHMgPSBfYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG4vKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0VHJlZUFkYXB0ZXJcbiAqIEBmb3JtYXRcbiAqIFxuICpcbiAqIFRoaXMgaXMgdW5zdGFibGUgYW5kIG5vdCBwYXJ0IG9mIHRoZSBwdWJsaWMgQVBJIGFuZCBzaG91bGQgbm90IGJlIHVzZWQgYnlcbiAqIHByb2R1Y3Rpb24gc3lzdGVtcy4gVGhpcyBmaWxlIG1heSBiZSB1cGRhdGUvcmVtb3ZlZCB3aXRob3V0IG5vdGljZS5cbiAqL1xuXG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbnZhciB0cmF2ZXJzZUluRGVwdGhPcmRlciA9IGZ1bmN0aW9uIHRyYXZlcnNlSW5EZXB0aE9yZGVyKGJsb2NrcywgZm4pIHtcbiAgdmFyIHN0YWNrID0gW10uY29uY2F0KGJsb2NrcykucmV2ZXJzZSgpO1xuICB3aGlsZSAoc3RhY2subGVuZ3RoKSB7XG4gICAgdmFyIF9ibG9jayA9IHN0YWNrLnBvcCgpO1xuXG4gICAgZm4oX2Jsb2NrKTtcblxuICAgIHZhciBjaGlsZHJlbiA9IF9ibG9jay5jaGlsZHJlbjtcblxuICAgICFBcnJheS5pc0FycmF5KGNoaWxkcmVuKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdJbnZhbGlkIHRyZWUgcmF3IGJsb2NrJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gICAgc3RhY2sgPSBzdGFjay5jb25jYXQoW10uY29uY2F0KGNoaWxkcmVuLnJldmVyc2UoKSkpO1xuICB9XG59O1xuXG52YXIgaXNMaXN0QmxvY2sgPSBmdW5jdGlvbiBpc0xpc3RCbG9jayhibG9jaykge1xuICBpZiAoIShibG9jayAmJiBibG9jay50eXBlKSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICB2YXIgdHlwZSA9IGJsb2NrLnR5cGU7XG5cbiAgcmV0dXJuIHR5cGUgPT09ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyB8fCB0eXBlID09PSAnb3JkZXJlZC1saXN0LWl0ZW0nO1xufTtcblxudmFyIGFkZERlcHRoVG9DaGlsZHJlbiA9IGZ1bmN0aW9uIGFkZERlcHRoVG9DaGlsZHJlbihibG9jaykge1xuICBpZiAoQXJyYXkuaXNBcnJheShibG9jay5jaGlsZHJlbikpIHtcbiAgICBibG9jay5jaGlsZHJlbiA9IGJsb2NrLmNoaWxkcmVuLm1hcChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgIHJldHVybiBjaGlsZC50eXBlID09PSBibG9jay50eXBlID8gX2V4dGVuZHMoe30sIGNoaWxkLCB7IGRlcHRoOiAoYmxvY2suZGVwdGggfHwgMCkgKyAxIH0pIDogY2hpbGQ7XG4gICAgfSk7XG4gIH1cbn07XG5cbi8qKlxuICogVGhpcyBhZGFwdGVyIGlzIGludGVuZGVkIHRvIGJlIGJlIHVzZWQgYXMgYW4gYWRhcHRlciB0byBkcmFmdCB0cmVlIGRhdGFcbiAqXG4gKiBkcmFmdCBzdGF0ZSA8PT09PT0+IGRyYWZ0IHRyZWUgc3RhdGVcbiAqL1xudmFyIERyYWZ0VHJlZUFkYXB0ZXIgPSB7XG4gIC8qKlxuICAgKiBDb252ZXJ0cyBmcm9tIGEgdHJlZSByYXcgc3RhdGUgYmFjayB0byAgZHJhZnQgcmF3IHN0YXRlXG4gICAqL1xuICBmcm9tUmF3VHJlZVN0YXRlVG9SYXdTdGF0ZTogZnVuY3Rpb24gZnJvbVJhd1RyZWVTdGF0ZVRvUmF3U3RhdGUoZHJhZnRUcmVlU3RhdGUpIHtcbiAgICB2YXIgYmxvY2tzID0gZHJhZnRUcmVlU3RhdGUuYmxvY2tzO1xuXG4gICAgdmFyIHRyYW5zZm9ybWVkQmxvY2tzID0gW107XG5cbiAgICAhQXJyYXkuaXNBcnJheShibG9ja3MpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0ludmFsaWQgcmF3IHN0YXRlJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KGJsb2NrcykgfHwgIWJsb2Nrcy5sZW5ndGgpIHtcbiAgICAgIHJldHVybiBkcmFmdFRyZWVTdGF0ZTtcbiAgICB9XG5cbiAgICB0cmF2ZXJzZUluRGVwdGhPcmRlcihibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgdmFyIG5ld0Jsb2NrID0gX2V4dGVuZHMoe30sIGJsb2NrKTtcblxuICAgICAgaWYgKGlzTGlzdEJsb2NrKGJsb2NrKSkge1xuICAgICAgICBuZXdCbG9jay5kZXB0aCA9IG5ld0Jsb2NrLmRlcHRoIHx8IDA7XG4gICAgICAgIGFkZERlcHRoVG9DaGlsZHJlbihibG9jayk7XG4gICAgICB9XG5cbiAgICAgIGRlbGV0ZSBuZXdCbG9jay5jaGlsZHJlbjtcblxuICAgICAgdHJhbnNmb3JtZWRCbG9ja3MucHVzaChuZXdCbG9jayk7XG4gICAgfSk7XG5cbiAgICBkcmFmdFRyZWVTdGF0ZS5ibG9ja3MgPSB0cmFuc2Zvcm1lZEJsb2NrcztcblxuICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgZHJhZnRUcmVlU3RhdGUsIHtcbiAgICAgIGJsb2NrczogdHJhbnNmb3JtZWRCbG9ja3NcbiAgICB9KTtcbiAgfSxcblxuXG4gIC8qKlxuICAgKiBDb252ZXJ0cyBmcm9tIGRyYWZ0IHJhdyBzdGF0ZSB0byB0cmVlIGRyYWZ0IHN0YXRlXG4gICAqL1xuICBmcm9tUmF3U3RhdGVUb1Jhd1RyZWVTdGF0ZTogZnVuY3Rpb24gZnJvbVJhd1N0YXRlVG9SYXdUcmVlU3RhdGUoZHJhZnRTdGF0ZSkge1xuICAgIHZhciBsYXN0TGlzdERlcHRoQ2FjaGVSZWYgPSB7fTtcbiAgICB2YXIgdHJhbnNmb3JtZWRCbG9ja3MgPSBbXTtcblxuICAgIGRyYWZ0U3RhdGUuYmxvY2tzLmZvckVhY2goZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICB2YXIgaXNMaXN0ID0gaXNMaXN0QmxvY2soYmxvY2spO1xuICAgICAgdmFyIGRlcHRoID0gYmxvY2suZGVwdGggfHwgMDtcbiAgICAgIHZhciB0cmVlQmxvY2sgPSBfZXh0ZW5kcyh7fSwgYmxvY2ssIHtcbiAgICAgICAgY2hpbGRyZW46IFtdXG4gICAgICB9KTtcblxuICAgICAgaWYgKCFpc0xpc3QpIHtcbiAgICAgICAgLy8gcmVzZXQgdGhlIGNhY2hlIHBhdGhcbiAgICAgICAgbGFzdExpc3REZXB0aENhY2hlUmVmID0ge307XG4gICAgICAgIHRyYW5zZm9ybWVkQmxvY2tzLnB1c2godHJlZUJsb2NrKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyB1cGRhdGUgb3VyIGRlcHRoIGNhY2hlIHJlZmVyZW5jZSBwYXRoXG4gICAgICBsYXN0TGlzdERlcHRoQ2FjaGVSZWZbZGVwdGhdID0gdHJlZUJsb2NrO1xuXG4gICAgICAvLyBpZiB3ZSBhcmUgZ3JlYXRlciB0aGFuIHplcm8gd2UgbXVzdCBoYXZlIHNlZW4gYSBwYXJlbnQgYWxyZWFkeVxuICAgICAgaWYgKGRlcHRoID4gMCkge1xuICAgICAgICB2YXIgcGFyZW50ID0gbGFzdExpc3REZXB0aENhY2hlUmVmW2RlcHRoIC0gMV07XG5cbiAgICAgICAgIXBhcmVudCA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdJbnZhbGlkIGRlcHRoIGZvciBSYXdEcmFmdENvbnRlbnRCbG9jaycpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICAgICAgICAvLyBwdXNoIG5lc3RlZCBsaXN0IGJsb2Nrc1xuICAgICAgICBwYXJlbnQuY2hpbGRyZW4ucHVzaCh0cmVlQmxvY2spO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIHB1c2ggcm9vdCBsaXN0IGJsb2Nrc1xuICAgICAgdHJhbnNmb3JtZWRCbG9ja3MucHVzaCh0cmVlQmxvY2spO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIF9leHRlbmRzKHt9LCBkcmFmdFN0YXRlLCB7XG4gICAgICBibG9ja3M6IHRyYW5zZm9ybWVkQmxvY2tzXG4gICAgfSk7XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gRHJhZnRUcmVlQWRhcHRlcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFLQTtBQUNBOzs7QUFHQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQTlFQTtBQWlGQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftTreeAdapter.js
