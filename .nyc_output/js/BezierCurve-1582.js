var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var vec2 = __webpack_require__(/*! ../../core/vector */ "./node_modules/zrender/lib/core/vector.js");

var _curve = __webpack_require__(/*! ../../core/curve */ "./node_modules/zrender/lib/core/curve.js");

var quadraticSubdivide = _curve.quadraticSubdivide;
var cubicSubdivide = _curve.cubicSubdivide;
var quadraticAt = _curve.quadraticAt;
var cubicAt = _curve.cubicAt;
var quadraticDerivativeAt = _curve.quadraticDerivativeAt;
var cubicDerivativeAt = _curve.cubicDerivativeAt;
/**
 * 贝塞尔曲线
 * @module zrender/shape/BezierCurve
 */

var out = [];

function someVectorAt(shape, t, isTangent) {
  var cpx2 = shape.cpx2;
  var cpy2 = shape.cpy2;

  if (cpx2 === null || cpy2 === null) {
    return [(isTangent ? cubicDerivativeAt : cubicAt)(shape.x1, shape.cpx1, shape.cpx2, shape.x2, t), (isTangent ? cubicDerivativeAt : cubicAt)(shape.y1, shape.cpy1, shape.cpy2, shape.y2, t)];
  } else {
    return [(isTangent ? quadraticDerivativeAt : quadraticAt)(shape.x1, shape.cpx1, shape.x2, t), (isTangent ? quadraticDerivativeAt : quadraticAt)(shape.y1, shape.cpy1, shape.y2, t)];
  }
}

var _default = Path.extend({
  type: 'bezier-curve',
  shape: {
    x1: 0,
    y1: 0,
    x2: 0,
    y2: 0,
    cpx1: 0,
    cpy1: 0,
    // cpx2: 0,
    // cpy2: 0
    // Curve show percent, for animating
    percent: 1
  },
  style: {
    stroke: '#000',
    fill: null
  },
  buildPath: function buildPath(ctx, shape) {
    var x1 = shape.x1;
    var y1 = shape.y1;
    var x2 = shape.x2;
    var y2 = shape.y2;
    var cpx1 = shape.cpx1;
    var cpy1 = shape.cpy1;
    var cpx2 = shape.cpx2;
    var cpy2 = shape.cpy2;
    var percent = shape.percent;

    if (percent === 0) {
      return;
    }

    ctx.moveTo(x1, y1);

    if (cpx2 == null || cpy2 == null) {
      if (percent < 1) {
        quadraticSubdivide(x1, cpx1, x2, percent, out);
        cpx1 = out[1];
        x2 = out[2];
        quadraticSubdivide(y1, cpy1, y2, percent, out);
        cpy1 = out[1];
        y2 = out[2];
      }

      ctx.quadraticCurveTo(cpx1, cpy1, x2, y2);
    } else {
      if (percent < 1) {
        cubicSubdivide(x1, cpx1, cpx2, x2, percent, out);
        cpx1 = out[1];
        cpx2 = out[2];
        x2 = out[3];
        cubicSubdivide(y1, cpy1, cpy2, y2, percent, out);
        cpy1 = out[1];
        cpy2 = out[2];
        y2 = out[3];
      }

      ctx.bezierCurveTo(cpx1, cpy1, cpx2, cpy2, x2, y2);
    }
  },

  /**
   * Get point at percent
   * @param  {number} t
   * @return {Array.<number>}
   */
  pointAt: function pointAt(t) {
    return someVectorAt(this.shape, t, false);
  },

  /**
   * Get tangent at percent
   * @param  {number} t
   * @return {Array.<number>}
   */
  tangentAt: function tangentAt(t) {
    var p = someVectorAt(this.shape, t, true);
    return vec2.normalize(p, p);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9CZXppZXJDdXJ2ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvc2hhcGUvQmV6aWVyQ3VydmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIFBhdGggPSByZXF1aXJlKFwiLi4vUGF0aFwiKTtcblxudmFyIHZlYzIgPSByZXF1aXJlKFwiLi4vLi4vY29yZS92ZWN0b3JcIik7XG5cbnZhciBfY3VydmUgPSByZXF1aXJlKFwiLi4vLi4vY29yZS9jdXJ2ZVwiKTtcblxudmFyIHF1YWRyYXRpY1N1YmRpdmlkZSA9IF9jdXJ2ZS5xdWFkcmF0aWNTdWJkaXZpZGU7XG52YXIgY3ViaWNTdWJkaXZpZGUgPSBfY3VydmUuY3ViaWNTdWJkaXZpZGU7XG52YXIgcXVhZHJhdGljQXQgPSBfY3VydmUucXVhZHJhdGljQXQ7XG52YXIgY3ViaWNBdCA9IF9jdXJ2ZS5jdWJpY0F0O1xudmFyIHF1YWRyYXRpY0Rlcml2YXRpdmVBdCA9IF9jdXJ2ZS5xdWFkcmF0aWNEZXJpdmF0aXZlQXQ7XG52YXIgY3ViaWNEZXJpdmF0aXZlQXQgPSBfY3VydmUuY3ViaWNEZXJpdmF0aXZlQXQ7XG5cbi8qKlxuICog6LSd5aGe5bCU5puy57q/XG4gKiBAbW9kdWxlIHpyZW5kZXIvc2hhcGUvQmV6aWVyQ3VydmVcbiAqL1xudmFyIG91dCA9IFtdO1xuXG5mdW5jdGlvbiBzb21lVmVjdG9yQXQoc2hhcGUsIHQsIGlzVGFuZ2VudCkge1xuICB2YXIgY3B4MiA9IHNoYXBlLmNweDI7XG4gIHZhciBjcHkyID0gc2hhcGUuY3B5MjtcblxuICBpZiAoY3B4MiA9PT0gbnVsbCB8fCBjcHkyID09PSBudWxsKSB7XG4gICAgcmV0dXJuIFsoaXNUYW5nZW50ID8gY3ViaWNEZXJpdmF0aXZlQXQgOiBjdWJpY0F0KShzaGFwZS54MSwgc2hhcGUuY3B4MSwgc2hhcGUuY3B4Miwgc2hhcGUueDIsIHQpLCAoaXNUYW5nZW50ID8gY3ViaWNEZXJpdmF0aXZlQXQgOiBjdWJpY0F0KShzaGFwZS55MSwgc2hhcGUuY3B5MSwgc2hhcGUuY3B5Miwgc2hhcGUueTIsIHQpXTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gWyhpc1RhbmdlbnQgPyBxdWFkcmF0aWNEZXJpdmF0aXZlQXQgOiBxdWFkcmF0aWNBdCkoc2hhcGUueDEsIHNoYXBlLmNweDEsIHNoYXBlLngyLCB0KSwgKGlzVGFuZ2VudCA/IHF1YWRyYXRpY0Rlcml2YXRpdmVBdCA6IHF1YWRyYXRpY0F0KShzaGFwZS55MSwgc2hhcGUuY3B5MSwgc2hhcGUueTIsIHQpXTtcbiAgfVxufVxuXG52YXIgX2RlZmF1bHQgPSBQYXRoLmV4dGVuZCh7XG4gIHR5cGU6ICdiZXppZXItY3VydmUnLFxuICBzaGFwZToge1xuICAgIHgxOiAwLFxuICAgIHkxOiAwLFxuICAgIHgyOiAwLFxuICAgIHkyOiAwLFxuICAgIGNweDE6IDAsXG4gICAgY3B5MTogMCxcbiAgICAvLyBjcHgyOiAwLFxuICAgIC8vIGNweTI6IDBcbiAgICAvLyBDdXJ2ZSBzaG93IHBlcmNlbnQsIGZvciBhbmltYXRpbmdcbiAgICBwZXJjZW50OiAxXG4gIH0sXG4gIHN0eWxlOiB7XG4gICAgc3Ryb2tlOiAnIzAwMCcsXG4gICAgZmlsbDogbnVsbFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChjdHgsIHNoYXBlKSB7XG4gICAgdmFyIHgxID0gc2hhcGUueDE7XG4gICAgdmFyIHkxID0gc2hhcGUueTE7XG4gICAgdmFyIHgyID0gc2hhcGUueDI7XG4gICAgdmFyIHkyID0gc2hhcGUueTI7XG4gICAgdmFyIGNweDEgPSBzaGFwZS5jcHgxO1xuICAgIHZhciBjcHkxID0gc2hhcGUuY3B5MTtcbiAgICB2YXIgY3B4MiA9IHNoYXBlLmNweDI7XG4gICAgdmFyIGNweTIgPSBzaGFwZS5jcHkyO1xuICAgIHZhciBwZXJjZW50ID0gc2hhcGUucGVyY2VudDtcblxuICAgIGlmIChwZXJjZW50ID09PSAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY3R4Lm1vdmVUbyh4MSwgeTEpO1xuXG4gICAgaWYgKGNweDIgPT0gbnVsbCB8fCBjcHkyID09IG51bGwpIHtcbiAgICAgIGlmIChwZXJjZW50IDwgMSkge1xuICAgICAgICBxdWFkcmF0aWNTdWJkaXZpZGUoeDEsIGNweDEsIHgyLCBwZXJjZW50LCBvdXQpO1xuICAgICAgICBjcHgxID0gb3V0WzFdO1xuICAgICAgICB4MiA9IG91dFsyXTtcbiAgICAgICAgcXVhZHJhdGljU3ViZGl2aWRlKHkxLCBjcHkxLCB5MiwgcGVyY2VudCwgb3V0KTtcbiAgICAgICAgY3B5MSA9IG91dFsxXTtcbiAgICAgICAgeTIgPSBvdXRbMl07XG4gICAgICB9XG5cbiAgICAgIGN0eC5xdWFkcmF0aWNDdXJ2ZVRvKGNweDEsIGNweTEsIHgyLCB5Mik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChwZXJjZW50IDwgMSkge1xuICAgICAgICBjdWJpY1N1YmRpdmlkZSh4MSwgY3B4MSwgY3B4MiwgeDIsIHBlcmNlbnQsIG91dCk7XG4gICAgICAgIGNweDEgPSBvdXRbMV07XG4gICAgICAgIGNweDIgPSBvdXRbMl07XG4gICAgICAgIHgyID0gb3V0WzNdO1xuICAgICAgICBjdWJpY1N1YmRpdmlkZSh5MSwgY3B5MSwgY3B5MiwgeTIsIHBlcmNlbnQsIG91dCk7XG4gICAgICAgIGNweTEgPSBvdXRbMV07XG4gICAgICAgIGNweTIgPSBvdXRbMl07XG4gICAgICAgIHkyID0gb3V0WzNdO1xuICAgICAgfVxuXG4gICAgICBjdHguYmV6aWVyQ3VydmVUbyhjcHgxLCBjcHkxLCBjcHgyLCBjcHkyLCB4MiwgeTIpO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogR2V0IHBvaW50IGF0IHBlcmNlbnRcbiAgICogQHBhcmFtICB7bnVtYmVyfSB0XG4gICAqIEByZXR1cm4ge0FycmF5LjxudW1iZXI+fVxuICAgKi9cbiAgcG9pbnRBdDogZnVuY3Rpb24gKHQpIHtcbiAgICByZXR1cm4gc29tZVZlY3RvckF0KHRoaXMuc2hhcGUsIHQsIGZhbHNlKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IHRhbmdlbnQgYXQgcGVyY2VudFxuICAgKiBAcGFyYW0gIHtudW1iZXJ9IHRcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICB0YW5nZW50QXQ6IGZ1bmN0aW9uICh0KSB7XG4gICAgdmFyIHAgPSBzb21lVmVjdG9yQXQodGhpcy5zaGFwZSwgdCwgdHJ1ZSk7XG4gICAgcmV0dXJuIHZlYzIubm9ybWFsaXplKHAsIHApO1xuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUEvRUE7QUFDQTtBQWlGQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/BezierCurve.js
