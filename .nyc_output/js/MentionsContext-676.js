__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentionsContextProvider", function() { return MentionsContextProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MentionsContextConsumer", function() { return MentionsContextConsumer; });
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js");
/* harmony import */ var _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0__);
/* tslint:disable: no-object-literal-type-assertion */
 // We will never use default, here only to fix TypeScript warning

var MentionsContext = _ant_design_create_react_context__WEBPACK_IMPORTED_MODULE_0___default()(null);
var MentionsContextProvider = MentionsContext.Provider;
var MentionsContextConsumer = MentionsContext.Consumer;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudGlvbnMvZXMvTWVudGlvbnNDb250ZXh0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtbWVudGlvbnMvZXMvTWVudGlvbnNDb250ZXh0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlOiBuby1vYmplY3QtbGl0ZXJhbC10eXBlLWFzc2VydGlvbiAqL1xuaW1wb3J0IGNyZWF0ZVJlYWN0Q29udGV4dCBmcm9tICdAYW50LWRlc2lnbi9jcmVhdGUtcmVhY3QtY29udGV4dCc7IC8vIFdlIHdpbGwgbmV2ZXIgdXNlIGRlZmF1bHQsIGhlcmUgb25seSB0byBmaXggVHlwZVNjcmlwdCB3YXJuaW5nXG5cbnZhciBNZW50aW9uc0NvbnRleHQgPSBjcmVhdGVSZWFjdENvbnRleHQobnVsbCk7XG5leHBvcnQgdmFyIE1lbnRpb25zQ29udGV4dFByb3ZpZGVyID0gTWVudGlvbnNDb250ZXh0LlByb3ZpZGVyO1xuZXhwb3J0IHZhciBNZW50aW9uc0NvbnRleHRDb25zdW1lciA9IE1lbnRpb25zQ29udGV4dC5Db25zdW1lcjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-mentions/es/MentionsContext.js
