/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var retrieve = _util.retrieve;
var defaults = _util.defaults;
var extend = _util.extend;
var each = _util.each;

var formatUtil = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var Model = __webpack_require__(/*! ../../model/Model */ "./node_modules/echarts/lib/model/Model.js");

var _number = __webpack_require__(/*! ../../util/number */ "./node_modules/echarts/lib/util/number.js");

var isRadianAroundZero = _number.isRadianAroundZero;
var remRadian = _number.remRadian;

var _symbol = __webpack_require__(/*! ../../util/symbol */ "./node_modules/echarts/lib/util/symbol.js");

var createSymbol = _symbol.createSymbol;

var matrixUtil = __webpack_require__(/*! zrender/lib/core/matrix */ "./node_modules/zrender/lib/core/matrix.js");

var _vector = __webpack_require__(/*! zrender/lib/core/vector */ "./node_modules/zrender/lib/core/vector.js");

var v2ApplyTransform = _vector.applyTransform;

var _axisHelper = __webpack_require__(/*! ../../coord/axisHelper */ "./node_modules/echarts/lib/coord/axisHelper.js");

var shouldShowAllLabels = _axisHelper.shouldShowAllLabels;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var PI = Math.PI;

function makeAxisEventDataBase(axisModel) {
  var eventData = {
    componentType: axisModel.mainType,
    componentIndex: axisModel.componentIndex
  };
  eventData[axisModel.mainType + 'Index'] = axisModel.componentIndex;
  return eventData;
}
/**
 * A final axis is translated and rotated from a "standard axis".
 * So opt.position and opt.rotation is required.
 *
 * A standard axis is and axis from [0, 0] to [0, axisExtent[1]],
 * for example: (0, 0) ------------> (0, 50)
 *
 * nameDirection or tickDirection or labelDirection is 1 means tick
 * or label is below the standard axis, whereas is -1 means above
 * the standard axis. labelOffset means offset between label and axis,
 * which is useful when 'onZero', where axisLabel is in the grid and
 * label in outside grid.
 *
 * Tips: like always,
 * positive rotation represents anticlockwise, and negative rotation
 * represents clockwise.
 * The direction of position coordinate is the same as the direction
 * of screen coordinate.
 *
 * Do not need to consider axis 'inverse', which is auto processed by
 * axis extent.
 *
 * @param {module:zrender/container/Group} group
 * @param {Object} axisModel
 * @param {Object} opt Standard axis parameters.
 * @param {Array.<number>} opt.position [x, y]
 * @param {number} opt.rotation by radian
 * @param {number} [opt.nameDirection=1] 1 or -1 Used when nameLocation is 'middle' or 'center'.
 * @param {number} [opt.tickDirection=1] 1 or -1
 * @param {number} [opt.labelDirection=1] 1 or -1
 * @param {number} [opt.labelOffset=0] Usefull when onZero.
 * @param {string} [opt.axisLabelShow] default get from axisModel.
 * @param {string} [opt.axisName] default get from axisModel.
 * @param {number} [opt.axisNameAvailableWidth]
 * @param {number} [opt.labelRotate] by degree, default get from axisModel.
 * @param {number} [opt.strokeContainThreshold] Default label interval when label
 * @param {number} [opt.nameTruncateMaxWidth]
 */


var AxisBuilder = function AxisBuilder(axisModel, opt) {
  /**
   * @readOnly
   */
  this.opt = opt;
  /**
   * @readOnly
   */

  this.axisModel = axisModel; // Default value

  defaults(opt, {
    labelOffset: 0,
    nameDirection: 1,
    tickDirection: 1,
    labelDirection: 1,
    silent: true
  });
  /**
   * @readOnly
   */

  this.group = new graphic.Group(); // FIXME Not use a seperate text group?

  var dumbGroup = new graphic.Group({
    position: opt.position.slice(),
    rotation: opt.rotation
  }); // this.group.add(dumbGroup);
  // this._dumbGroup = dumbGroup;

  dumbGroup.updateTransform();
  this._transform = dumbGroup.transform;
  this._dumbGroup = dumbGroup;
};

AxisBuilder.prototype = {
  constructor: AxisBuilder,
  hasBuilder: function hasBuilder(name) {
    return !!builders[name];
  },
  add: function add(name) {
    builders[name].call(this);
  },
  getGroup: function getGroup() {
    return this.group;
  }
};
var builders = {
  /**
   * @private
   */
  axisLine: function axisLine() {
    var opt = this.opt;
    var axisModel = this.axisModel;

    if (!axisModel.get('axisLine.show')) {
      return;
    }

    var extent = this.axisModel.axis.getExtent();
    var matrix = this._transform;
    var pt1 = [extent[0], 0];
    var pt2 = [extent[1], 0];

    if (matrix) {
      v2ApplyTransform(pt1, pt1, matrix);
      v2ApplyTransform(pt2, pt2, matrix);
    }

    var lineStyle = extend({
      lineCap: 'round'
    }, axisModel.getModel('axisLine.lineStyle').getLineStyle());
    this.group.add(new graphic.Line(graphic.subPixelOptimizeLine({
      // Id for animation
      anid: 'line',
      shape: {
        x1: pt1[0],
        y1: pt1[1],
        x2: pt2[0],
        y2: pt2[1]
      },
      style: lineStyle,
      strokeContainThreshold: opt.strokeContainThreshold || 5,
      silent: true,
      z2: 1
    })));
    var arrows = axisModel.get('axisLine.symbol');
    var arrowSize = axisModel.get('axisLine.symbolSize');
    var arrowOffset = axisModel.get('axisLine.symbolOffset') || 0;

    if (typeof arrowOffset === 'number') {
      arrowOffset = [arrowOffset, arrowOffset];
    }

    if (arrows != null) {
      if (typeof arrows === 'string') {
        // Use the same arrow for start and end point
        arrows = [arrows, arrows];
      }

      if (typeof arrowSize === 'string' || typeof arrowSize === 'number') {
        // Use the same size for width and height
        arrowSize = [arrowSize, arrowSize];
      }

      var symbolWidth = arrowSize[0];
      var symbolHeight = arrowSize[1];
      each([{
        rotate: opt.rotation + Math.PI / 2,
        offset: arrowOffset[0],
        r: 0
      }, {
        rotate: opt.rotation - Math.PI / 2,
        offset: arrowOffset[1],
        r: Math.sqrt((pt1[0] - pt2[0]) * (pt1[0] - pt2[0]) + (pt1[1] - pt2[1]) * (pt1[1] - pt2[1]))
      }], function (point, index) {
        if (arrows[index] !== 'none' && arrows[index] != null) {
          var symbol = createSymbol(arrows[index], -symbolWidth / 2, -symbolHeight / 2, symbolWidth, symbolHeight, lineStyle.stroke, true); // Calculate arrow position with offset

          var r = point.r + point.offset;
          var pos = [pt1[0] + r * Math.cos(opt.rotation), pt1[1] - r * Math.sin(opt.rotation)];
          symbol.attr({
            rotation: point.rotate,
            position: pos,
            silent: true,
            z2: 11
          });
          this.group.add(symbol);
        }
      }, this);
    }
  },

  /**
   * @private
   */
  axisTickLabel: function axisTickLabel() {
    var axisModel = this.axisModel;
    var opt = this.opt;
    var tickEls = buildAxisTick(this, axisModel, opt);
    var labelEls = buildAxisLabel(this, axisModel, opt);
    fixMinMaxLabelShow(axisModel, labelEls, tickEls);
  },

  /**
   * @private
   */
  axisName: function axisName() {
    var opt = this.opt;
    var axisModel = this.axisModel;
    var name = retrieve(opt.axisName, axisModel.get('name'));

    if (!name) {
      return;
    }

    var nameLocation = axisModel.get('nameLocation');
    var nameDirection = opt.nameDirection;
    var textStyleModel = axisModel.getModel('nameTextStyle');
    var gap = axisModel.get('nameGap') || 0;
    var extent = this.axisModel.axis.getExtent();
    var gapSignal = extent[0] > extent[1] ? -1 : 1;
    var pos = [nameLocation === 'start' ? extent[0] - gapSignal * gap : nameLocation === 'end' ? extent[1] + gapSignal * gap : (extent[0] + extent[1]) / 2, // 'middle'
    // Reuse labelOffset.
    isNameLocationCenter(nameLocation) ? opt.labelOffset + nameDirection * gap : 0];
    var labelLayout;
    var nameRotation = axisModel.get('nameRotate');

    if (nameRotation != null) {
      nameRotation = nameRotation * PI / 180; // To radian.
    }

    var axisNameAvailableWidth;

    if (isNameLocationCenter(nameLocation)) {
      labelLayout = innerTextLayout(opt.rotation, nameRotation != null ? nameRotation : opt.rotation, // Adapt to axis.
      nameDirection);
    } else {
      labelLayout = endTextLayout(opt, nameLocation, nameRotation || 0, extent);
      axisNameAvailableWidth = opt.axisNameAvailableWidth;

      if (axisNameAvailableWidth != null) {
        axisNameAvailableWidth = Math.abs(axisNameAvailableWidth / Math.sin(labelLayout.rotation));
        !isFinite(axisNameAvailableWidth) && (axisNameAvailableWidth = null);
      }
    }

    var textFont = textStyleModel.getFont();
    var truncateOpt = axisModel.get('nameTruncate', true) || {};
    var ellipsis = truncateOpt.ellipsis;
    var maxWidth = retrieve(opt.nameTruncateMaxWidth, truncateOpt.maxWidth, axisNameAvailableWidth); // FIXME
    // truncate rich text? (consider performance)

    var truncatedText = ellipsis != null && maxWidth != null ? formatUtil.truncateText(name, maxWidth, textFont, ellipsis, {
      minChar: 2,
      placeholder: truncateOpt.placeholder
    }) : name;
    var tooltipOpt = axisModel.get('tooltip', true);
    var mainType = axisModel.mainType;
    var formatterParams = {
      componentType: mainType,
      name: name,
      $vars: ['name']
    };
    formatterParams[mainType + 'Index'] = axisModel.componentIndex;
    var textEl = new graphic.Text({
      // Id for animation
      anid: 'name',
      __fullText: name,
      __truncatedText: truncatedText,
      position: pos,
      rotation: labelLayout.rotation,
      silent: isSilent(axisModel),
      z2: 1,
      tooltip: tooltipOpt && tooltipOpt.show ? extend({
        content: name,
        formatter: function formatter() {
          return name;
        },
        formatterParams: formatterParams
      }, tooltipOpt) : null
    });
    graphic.setTextStyle(textEl.style, textStyleModel, {
      text: truncatedText,
      textFont: textFont,
      textFill: textStyleModel.getTextColor() || axisModel.get('axisLine.lineStyle.color'),
      textAlign: labelLayout.textAlign,
      textVerticalAlign: labelLayout.textVerticalAlign
    });

    if (axisModel.get('triggerEvent')) {
      textEl.eventData = makeAxisEventDataBase(axisModel);
      textEl.eventData.targetType = 'axisName';
      textEl.eventData.name = name;
    } // FIXME


    this._dumbGroup.add(textEl);

    textEl.updateTransform();
    this.group.add(textEl);
    textEl.decomposeTransform();
  }
};
/**
 * @public
 * @static
 * @param {Object} opt
 * @param {number} axisRotation in radian
 * @param {number} textRotation in radian
 * @param {number} direction
 * @return {Object} {
 *  rotation, // according to axis
 *  textAlign,
 *  textVerticalAlign
 * }
 */

var innerTextLayout = AxisBuilder.innerTextLayout = function (axisRotation, textRotation, direction) {
  var rotationDiff = remRadian(textRotation - axisRotation);
  var textAlign;
  var textVerticalAlign;

  if (isRadianAroundZero(rotationDiff)) {
    // Label is parallel with axis line.
    textVerticalAlign = direction > 0 ? 'top' : 'bottom';
    textAlign = 'center';
  } else if (isRadianAroundZero(rotationDiff - PI)) {
    // Label is inverse parallel with axis line.
    textVerticalAlign = direction > 0 ? 'bottom' : 'top';
    textAlign = 'center';
  } else {
    textVerticalAlign = 'middle';

    if (rotationDiff > 0 && rotationDiff < PI) {
      textAlign = direction > 0 ? 'right' : 'left';
    } else {
      textAlign = direction > 0 ? 'left' : 'right';
    }
  }

  return {
    rotation: rotationDiff,
    textAlign: textAlign,
    textVerticalAlign: textVerticalAlign
  };
};

function endTextLayout(opt, textPosition, textRotate, extent) {
  var rotationDiff = remRadian(textRotate - opt.rotation);
  var textAlign;
  var textVerticalAlign;
  var inverse = extent[0] > extent[1];
  var onLeft = textPosition === 'start' && !inverse || textPosition !== 'start' && inverse;

  if (isRadianAroundZero(rotationDiff - PI / 2)) {
    textVerticalAlign = onLeft ? 'bottom' : 'top';
    textAlign = 'center';
  } else if (isRadianAroundZero(rotationDiff - PI * 1.5)) {
    textVerticalAlign = onLeft ? 'top' : 'bottom';
    textAlign = 'center';
  } else {
    textVerticalAlign = 'middle';

    if (rotationDiff < PI * 1.5 && rotationDiff > PI / 2) {
      textAlign = onLeft ? 'left' : 'right';
    } else {
      textAlign = onLeft ? 'right' : 'left';
    }
  }

  return {
    rotation: rotationDiff,
    textAlign: textAlign,
    textVerticalAlign: textVerticalAlign
  };
}

function isSilent(axisModel) {
  var tooltipOpt = axisModel.get('tooltip');
  return axisModel.get('silent') // Consider mouse cursor, add these restrictions.
  || !(axisModel.get('triggerEvent') || tooltipOpt && tooltipOpt.show);
}

function fixMinMaxLabelShow(axisModel, labelEls, tickEls) {
  if (shouldShowAllLabels(axisModel.axis)) {
    return;
  } // If min or max are user set, we need to check
  // If the tick on min(max) are overlap on their neighbour tick
  // If they are overlapped, we need to hide the min(max) tick label


  var showMinLabel = axisModel.get('axisLabel.showMinLabel');
  var showMaxLabel = axisModel.get('axisLabel.showMaxLabel'); // FIXME
  // Have not consider onBand yet, where tick els is more than label els.

  labelEls = labelEls || [];
  tickEls = tickEls || [];
  var firstLabel = labelEls[0];
  var nextLabel = labelEls[1];
  var lastLabel = labelEls[labelEls.length - 1];
  var prevLabel = labelEls[labelEls.length - 2];
  var firstTick = tickEls[0];
  var nextTick = tickEls[1];
  var lastTick = tickEls[tickEls.length - 1];
  var prevTick = tickEls[tickEls.length - 2];

  if (showMinLabel === false) {
    ignoreEl(firstLabel);
    ignoreEl(firstTick);
  } else if (isTwoLabelOverlapped(firstLabel, nextLabel)) {
    if (showMinLabel) {
      ignoreEl(nextLabel);
      ignoreEl(nextTick);
    } else {
      ignoreEl(firstLabel);
      ignoreEl(firstTick);
    }
  }

  if (showMaxLabel === false) {
    ignoreEl(lastLabel);
    ignoreEl(lastTick);
  } else if (isTwoLabelOverlapped(prevLabel, lastLabel)) {
    if (showMaxLabel) {
      ignoreEl(prevLabel);
      ignoreEl(prevTick);
    } else {
      ignoreEl(lastLabel);
      ignoreEl(lastTick);
    }
  }
}

function ignoreEl(el) {
  el && (el.ignore = true);
}

function isTwoLabelOverlapped(current, next, labelLayout) {
  // current and next has the same rotation.
  var firstRect = current && current.getBoundingRect().clone();
  var nextRect = next && next.getBoundingRect().clone();

  if (!firstRect || !nextRect) {
    return;
  } // When checking intersect of two rotated labels, we use mRotationBack
  // to avoid that boundingRect is enlarge when using `boundingRect.applyTransform`.


  var mRotationBack = matrixUtil.identity([]);
  matrixUtil.rotate(mRotationBack, mRotationBack, -current.rotation);
  firstRect.applyTransform(matrixUtil.mul([], mRotationBack, current.getLocalTransform()));
  nextRect.applyTransform(matrixUtil.mul([], mRotationBack, next.getLocalTransform()));
  return firstRect.intersect(nextRect);
}

function isNameLocationCenter(nameLocation) {
  return nameLocation === 'middle' || nameLocation === 'center';
}

function buildAxisTick(axisBuilder, axisModel, opt) {
  var axis = axisModel.axis;

  if (!axisModel.get('axisTick.show') || axis.scale.isBlank()) {
    return;
  }

  var tickModel = axisModel.getModel('axisTick');
  var lineStyleModel = tickModel.getModel('lineStyle');
  var tickLen = tickModel.get('length');
  var ticksCoords = axis.getTicksCoords();
  var pt1 = [];
  var pt2 = [];
  var matrix = axisBuilder._transform;
  var tickEls = [];

  for (var i = 0; i < ticksCoords.length; i++) {
    var tickCoord = ticksCoords[i].coord;
    pt1[0] = tickCoord;
    pt1[1] = 0;
    pt2[0] = tickCoord;
    pt2[1] = opt.tickDirection * tickLen;

    if (matrix) {
      v2ApplyTransform(pt1, pt1, matrix);
      v2ApplyTransform(pt2, pt2, matrix);
    } // Tick line, Not use group transform to have better line draw


    var tickEl = new graphic.Line(graphic.subPixelOptimizeLine({
      // Id for animation
      anid: 'tick_' + ticksCoords[i].tickValue,
      shape: {
        x1: pt1[0],
        y1: pt1[1],
        x2: pt2[0],
        y2: pt2[1]
      },
      style: defaults(lineStyleModel.getLineStyle(), {
        stroke: axisModel.get('axisLine.lineStyle.color')
      }),
      z2: 2,
      silent: true
    }));
    axisBuilder.group.add(tickEl);
    tickEls.push(tickEl);
  }

  return tickEls;
}

function buildAxisLabel(axisBuilder, axisModel, opt) {
  var axis = axisModel.axis;
  var show = retrieve(opt.axisLabelShow, axisModel.get('axisLabel.show'));

  if (!show || axis.scale.isBlank()) {
    return;
  }

  var labelModel = axisModel.getModel('axisLabel');
  var labelMargin = labelModel.get('margin');
  var labels = axis.getViewLabels(); // Special label rotate.

  var labelRotation = (retrieve(opt.labelRotate, labelModel.get('rotate')) || 0) * PI / 180;
  var labelLayout = innerTextLayout(opt.rotation, labelRotation, opt.labelDirection);
  var rawCategoryData = axisModel.getCategories(true);
  var labelEls = [];
  var silent = isSilent(axisModel);
  var triggerEvent = axisModel.get('triggerEvent');
  each(labels, function (labelItem, index) {
    var tickValue = labelItem.tickValue;
    var formattedLabel = labelItem.formattedLabel;
    var rawLabel = labelItem.rawLabel;
    var itemLabelModel = labelModel;

    if (rawCategoryData && rawCategoryData[tickValue] && rawCategoryData[tickValue].textStyle) {
      itemLabelModel = new Model(rawCategoryData[tickValue].textStyle, labelModel, axisModel.ecModel);
    }

    var textColor = itemLabelModel.getTextColor() || axisModel.get('axisLine.lineStyle.color');
    var tickCoord = axis.dataToCoord(tickValue);
    var pos = [tickCoord, opt.labelOffset + opt.labelDirection * labelMargin];
    var textEl = new graphic.Text({
      // Id for animation
      anid: 'label_' + tickValue,
      position: pos,
      rotation: labelLayout.rotation,
      silent: silent,
      z2: 10
    });
    graphic.setTextStyle(textEl.style, itemLabelModel, {
      text: formattedLabel,
      textAlign: itemLabelModel.getShallow('align', true) || labelLayout.textAlign,
      textVerticalAlign: itemLabelModel.getShallow('verticalAlign', true) || itemLabelModel.getShallow('baseline', true) || labelLayout.textVerticalAlign,
      textFill: typeof textColor === 'function' ? textColor( // (1) In category axis with data zoom, tick is not the original
      // index of axis.data. So tick should not be exposed to user
      // in category axis.
      // (2) Compatible with previous version, which always use formatted label as
      // input. But in interval scale the formatted label is like '223,445', which
      // maked user repalce ','. So we modify it to return original val but remain
      // it as 'string' to avoid error in replacing.
      axis.type === 'category' ? rawLabel : axis.type === 'value' ? tickValue + '' : tickValue, index) : textColor
    }); // Pack data for mouse event

    if (triggerEvent) {
      textEl.eventData = makeAxisEventDataBase(axisModel);
      textEl.eventData.targetType = 'axisLabel';
      textEl.eventData.value = rawLabel;
    } // FIXME


    axisBuilder._dumbGroup.add(textEl);

    textEl.updateTransform();
    labelEls.push(textEl);
    axisBuilder.group.add(textEl);
    textEl.decomposeTransform();
  });
  return labelEls;
}

var _default = AxisBuilder;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2F4aXMvQXhpc0J1aWxkZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb21wb25lbnQvYXhpcy9BeGlzQnVpbGRlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF91dGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIHJldHJpZXZlID0gX3V0aWwucmV0cmlldmU7XG52YXIgZGVmYXVsdHMgPSBfdXRpbC5kZWZhdWx0cztcbnZhciBleHRlbmQgPSBfdXRpbC5leHRlbmQ7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG5cbnZhciBmb3JtYXRVdGlsID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZm9ybWF0XCIpO1xuXG52YXIgZ3JhcGhpYyA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2dyYXBoaWNcIik7XG5cbnZhciBNb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9Nb2RlbFwiKTtcblxudmFyIF9udW1iZXIgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9udW1iZXJcIik7XG5cbnZhciBpc1JhZGlhbkFyb3VuZFplcm8gPSBfbnVtYmVyLmlzUmFkaWFuQXJvdW5kWmVybztcbnZhciByZW1SYWRpYW4gPSBfbnVtYmVyLnJlbVJhZGlhbjtcblxudmFyIF9zeW1ib2wgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9zeW1ib2xcIik7XG5cbnZhciBjcmVhdGVTeW1ib2wgPSBfc3ltYm9sLmNyZWF0ZVN5bWJvbDtcblxudmFyIG1hdHJpeFV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS9tYXRyaXhcIik7XG5cbnZhciBfdmVjdG9yID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgdjJBcHBseVRyYW5zZm9ybSA9IF92ZWN0b3IuYXBwbHlUcmFuc2Zvcm07XG5cbnZhciBfYXhpc0hlbHBlciA9IHJlcXVpcmUoXCIuLi8uLi9jb29yZC9heGlzSGVscGVyXCIpO1xuXG52YXIgc2hvdWxkU2hvd0FsbExhYmVscyA9IF9heGlzSGVscGVyLnNob3VsZFNob3dBbGxMYWJlbHM7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBQSSA9IE1hdGguUEk7XG5cbmZ1bmN0aW9uIG1ha2VBeGlzRXZlbnREYXRhQmFzZShheGlzTW9kZWwpIHtcbiAgdmFyIGV2ZW50RGF0YSA9IHtcbiAgICBjb21wb25lbnRUeXBlOiBheGlzTW9kZWwubWFpblR5cGUsXG4gICAgY29tcG9uZW50SW5kZXg6IGF4aXNNb2RlbC5jb21wb25lbnRJbmRleFxuICB9O1xuICBldmVudERhdGFbYXhpc01vZGVsLm1haW5UeXBlICsgJ0luZGV4J10gPSBheGlzTW9kZWwuY29tcG9uZW50SW5kZXg7XG4gIHJldHVybiBldmVudERhdGE7XG59XG4vKipcbiAqIEEgZmluYWwgYXhpcyBpcyB0cmFuc2xhdGVkIGFuZCByb3RhdGVkIGZyb20gYSBcInN0YW5kYXJkIGF4aXNcIi5cbiAqIFNvIG9wdC5wb3NpdGlvbiBhbmQgb3B0LnJvdGF0aW9uIGlzIHJlcXVpcmVkLlxuICpcbiAqIEEgc3RhbmRhcmQgYXhpcyBpcyBhbmQgYXhpcyBmcm9tIFswLCAwXSB0byBbMCwgYXhpc0V4dGVudFsxXV0sXG4gKiBmb3IgZXhhbXBsZTogKDAsIDApIC0tLS0tLS0tLS0tLT4gKDAsIDUwKVxuICpcbiAqIG5hbWVEaXJlY3Rpb24gb3IgdGlja0RpcmVjdGlvbiBvciBsYWJlbERpcmVjdGlvbiBpcyAxIG1lYW5zIHRpY2tcbiAqIG9yIGxhYmVsIGlzIGJlbG93IHRoZSBzdGFuZGFyZCBheGlzLCB3aGVyZWFzIGlzIC0xIG1lYW5zIGFib3ZlXG4gKiB0aGUgc3RhbmRhcmQgYXhpcy4gbGFiZWxPZmZzZXQgbWVhbnMgb2Zmc2V0IGJldHdlZW4gbGFiZWwgYW5kIGF4aXMsXG4gKiB3aGljaCBpcyB1c2VmdWwgd2hlbiAnb25aZXJvJywgd2hlcmUgYXhpc0xhYmVsIGlzIGluIHRoZSBncmlkIGFuZFxuICogbGFiZWwgaW4gb3V0c2lkZSBncmlkLlxuICpcbiAqIFRpcHM6IGxpa2UgYWx3YXlzLFxuICogcG9zaXRpdmUgcm90YXRpb24gcmVwcmVzZW50cyBhbnRpY2xvY2t3aXNlLCBhbmQgbmVnYXRpdmUgcm90YXRpb25cbiAqIHJlcHJlc2VudHMgY2xvY2t3aXNlLlxuICogVGhlIGRpcmVjdGlvbiBvZiBwb3NpdGlvbiBjb29yZGluYXRlIGlzIHRoZSBzYW1lIGFzIHRoZSBkaXJlY3Rpb25cbiAqIG9mIHNjcmVlbiBjb29yZGluYXRlLlxuICpcbiAqIERvIG5vdCBuZWVkIHRvIGNvbnNpZGVyIGF4aXMgJ2ludmVyc2UnLCB3aGljaCBpcyBhdXRvIHByb2Nlc3NlZCBieVxuICogYXhpcyBleHRlbnQuXG4gKlxuICogQHBhcmFtIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9IGdyb3VwXG4gKiBAcGFyYW0ge09iamVjdH0gYXhpc01vZGVsXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0IFN0YW5kYXJkIGF4aXMgcGFyYW1ldGVycy5cbiAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IG9wdC5wb3NpdGlvbiBbeCwgeV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBvcHQucm90YXRpb24gYnkgcmFkaWFuXG4gKiBAcGFyYW0ge251bWJlcn0gW29wdC5uYW1lRGlyZWN0aW9uPTFdIDEgb3IgLTEgVXNlZCB3aGVuIG5hbWVMb2NhdGlvbiBpcyAnbWlkZGxlJyBvciAnY2VudGVyJy5cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LnRpY2tEaXJlY3Rpb249MV0gMSBvciAtMVxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQubGFiZWxEaXJlY3Rpb249MV0gMSBvciAtMVxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQubGFiZWxPZmZzZXQ9MF0gVXNlZnVsbCB3aGVuIG9uWmVyby5cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0LmF4aXNMYWJlbFNob3ddIGRlZmF1bHQgZ2V0IGZyb20gYXhpc01vZGVsLlxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHQuYXhpc05hbWVdIGRlZmF1bHQgZ2V0IGZyb20gYXhpc01vZGVsLlxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQuYXhpc05hbWVBdmFpbGFibGVXaWR0aF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LmxhYmVsUm90YXRlXSBieSBkZWdyZWUsIGRlZmF1bHQgZ2V0IGZyb20gYXhpc01vZGVsLlxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQuc3Ryb2tlQ29udGFpblRocmVzaG9sZF0gRGVmYXVsdCBsYWJlbCBpbnRlcnZhbCB3aGVuIGxhYmVsXG4gKiBAcGFyYW0ge251bWJlcn0gW29wdC5uYW1lVHJ1bmNhdGVNYXhXaWR0aF1cbiAqL1xuXG5cbnZhciBBeGlzQnVpbGRlciA9IGZ1bmN0aW9uIChheGlzTW9kZWwsIG9wdCkge1xuICAvKipcbiAgICogQHJlYWRPbmx5XG4gICAqL1xuICB0aGlzLm9wdCA9IG9wdDtcbiAgLyoqXG4gICAqIEByZWFkT25seVxuICAgKi9cblxuICB0aGlzLmF4aXNNb2RlbCA9IGF4aXNNb2RlbDsgLy8gRGVmYXVsdCB2YWx1ZVxuXG4gIGRlZmF1bHRzKG9wdCwge1xuICAgIGxhYmVsT2Zmc2V0OiAwLFxuICAgIG5hbWVEaXJlY3Rpb246IDEsXG4gICAgdGlja0RpcmVjdGlvbjogMSxcbiAgICBsYWJlbERpcmVjdGlvbjogMSxcbiAgICBzaWxlbnQ6IHRydWVcbiAgfSk7XG4gIC8qKlxuICAgKiBAcmVhZE9ubHlcbiAgICovXG5cbiAgdGhpcy5ncm91cCA9IG5ldyBncmFwaGljLkdyb3VwKCk7IC8vIEZJWE1FIE5vdCB1c2UgYSBzZXBlcmF0ZSB0ZXh0IGdyb3VwP1xuXG4gIHZhciBkdW1iR3JvdXAgPSBuZXcgZ3JhcGhpYy5Hcm91cCh7XG4gICAgcG9zaXRpb246IG9wdC5wb3NpdGlvbi5zbGljZSgpLFxuICAgIHJvdGF0aW9uOiBvcHQucm90YXRpb25cbiAgfSk7IC8vIHRoaXMuZ3JvdXAuYWRkKGR1bWJHcm91cCk7XG4gIC8vIHRoaXMuX2R1bWJHcm91cCA9IGR1bWJHcm91cDtcblxuICBkdW1iR3JvdXAudXBkYXRlVHJhbnNmb3JtKCk7XG4gIHRoaXMuX3RyYW5zZm9ybSA9IGR1bWJHcm91cC50cmFuc2Zvcm07XG4gIHRoaXMuX2R1bWJHcm91cCA9IGR1bWJHcm91cDtcbn07XG5cbkF4aXNCdWlsZGVyLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IEF4aXNCdWlsZGVyLFxuICBoYXNCdWlsZGVyOiBmdW5jdGlvbiAobmFtZSkge1xuICAgIHJldHVybiAhIWJ1aWxkZXJzW25hbWVdO1xuICB9LFxuICBhZGQ6IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgYnVpbGRlcnNbbmFtZV0uY2FsbCh0aGlzKTtcbiAgfSxcbiAgZ2V0R3JvdXA6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5ncm91cDtcbiAgfVxufTtcbnZhciBidWlsZGVycyA9IHtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBheGlzTGluZTogZnVuY3Rpb24gKCkge1xuICAgIHZhciBvcHQgPSB0aGlzLm9wdDtcbiAgICB2YXIgYXhpc01vZGVsID0gdGhpcy5heGlzTW9kZWw7XG5cbiAgICBpZiAoIWF4aXNNb2RlbC5nZXQoJ2F4aXNMaW5lLnNob3cnKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBleHRlbnQgPSB0aGlzLmF4aXNNb2RlbC5heGlzLmdldEV4dGVudCgpO1xuICAgIHZhciBtYXRyaXggPSB0aGlzLl90cmFuc2Zvcm07XG4gICAgdmFyIHB0MSA9IFtleHRlbnRbMF0sIDBdO1xuICAgIHZhciBwdDIgPSBbZXh0ZW50WzFdLCAwXTtcblxuICAgIGlmIChtYXRyaXgpIHtcbiAgICAgIHYyQXBwbHlUcmFuc2Zvcm0ocHQxLCBwdDEsIG1hdHJpeCk7XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKHB0MiwgcHQyLCBtYXRyaXgpO1xuICAgIH1cblxuICAgIHZhciBsaW5lU3R5bGUgPSBleHRlbmQoe1xuICAgICAgbGluZUNhcDogJ3JvdW5kJ1xuICAgIH0sIGF4aXNNb2RlbC5nZXRNb2RlbCgnYXhpc0xpbmUubGluZVN0eWxlJykuZ2V0TGluZVN0eWxlKCkpO1xuICAgIHRoaXMuZ3JvdXAuYWRkKG5ldyBncmFwaGljLkxpbmUoZ3JhcGhpYy5zdWJQaXhlbE9wdGltaXplTGluZSh7XG4gICAgICAvLyBJZCBmb3IgYW5pbWF0aW9uXG4gICAgICBhbmlkOiAnbGluZScsXG4gICAgICBzaGFwZToge1xuICAgICAgICB4MTogcHQxWzBdLFxuICAgICAgICB5MTogcHQxWzFdLFxuICAgICAgICB4MjogcHQyWzBdLFxuICAgICAgICB5MjogcHQyWzFdXG4gICAgICB9LFxuICAgICAgc3R5bGU6IGxpbmVTdHlsZSxcbiAgICAgIHN0cm9rZUNvbnRhaW5UaHJlc2hvbGQ6IG9wdC5zdHJva2VDb250YWluVGhyZXNob2xkIHx8IDUsXG4gICAgICBzaWxlbnQ6IHRydWUsXG4gICAgICB6MjogMVxuICAgIH0pKSk7XG4gICAgdmFyIGFycm93cyA9IGF4aXNNb2RlbC5nZXQoJ2F4aXNMaW5lLnN5bWJvbCcpO1xuICAgIHZhciBhcnJvd1NpemUgPSBheGlzTW9kZWwuZ2V0KCdheGlzTGluZS5zeW1ib2xTaXplJyk7XG4gICAgdmFyIGFycm93T2Zmc2V0ID0gYXhpc01vZGVsLmdldCgnYXhpc0xpbmUuc3ltYm9sT2Zmc2V0JykgfHwgMDtcblxuICAgIGlmICh0eXBlb2YgYXJyb3dPZmZzZXQgPT09ICdudW1iZXInKSB7XG4gICAgICBhcnJvd09mZnNldCA9IFthcnJvd09mZnNldCwgYXJyb3dPZmZzZXRdO1xuICAgIH1cblxuICAgIGlmIChhcnJvd3MgIT0gbnVsbCkge1xuICAgICAgaWYgKHR5cGVvZiBhcnJvd3MgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIC8vIFVzZSB0aGUgc2FtZSBhcnJvdyBmb3Igc3RhcnQgYW5kIGVuZCBwb2ludFxuICAgICAgICBhcnJvd3MgPSBbYXJyb3dzLCBhcnJvd3NdO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIGFycm93U2l6ZSA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIGFycm93U2l6ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgLy8gVXNlIHRoZSBzYW1lIHNpemUgZm9yIHdpZHRoIGFuZCBoZWlnaHRcbiAgICAgICAgYXJyb3dTaXplID0gW2Fycm93U2l6ZSwgYXJyb3dTaXplXTtcbiAgICAgIH1cblxuICAgICAgdmFyIHN5bWJvbFdpZHRoID0gYXJyb3dTaXplWzBdO1xuICAgICAgdmFyIHN5bWJvbEhlaWdodCA9IGFycm93U2l6ZVsxXTtcbiAgICAgIGVhY2goW3tcbiAgICAgICAgcm90YXRlOiBvcHQucm90YXRpb24gKyBNYXRoLlBJIC8gMixcbiAgICAgICAgb2Zmc2V0OiBhcnJvd09mZnNldFswXSxcbiAgICAgICAgcjogMFxuICAgICAgfSwge1xuICAgICAgICByb3RhdGU6IG9wdC5yb3RhdGlvbiAtIE1hdGguUEkgLyAyLFxuICAgICAgICBvZmZzZXQ6IGFycm93T2Zmc2V0WzFdLFxuICAgICAgICByOiBNYXRoLnNxcnQoKHB0MVswXSAtIHB0MlswXSkgKiAocHQxWzBdIC0gcHQyWzBdKSArIChwdDFbMV0gLSBwdDJbMV0pICogKHB0MVsxXSAtIHB0MlsxXSkpXG4gICAgICB9XSwgZnVuY3Rpb24gKHBvaW50LCBpbmRleCkge1xuICAgICAgICBpZiAoYXJyb3dzW2luZGV4XSAhPT0gJ25vbmUnICYmIGFycm93c1tpbmRleF0gIT0gbnVsbCkge1xuICAgICAgICAgIHZhciBzeW1ib2wgPSBjcmVhdGVTeW1ib2woYXJyb3dzW2luZGV4XSwgLXN5bWJvbFdpZHRoIC8gMiwgLXN5bWJvbEhlaWdodCAvIDIsIHN5bWJvbFdpZHRoLCBzeW1ib2xIZWlnaHQsIGxpbmVTdHlsZS5zdHJva2UsIHRydWUpOyAvLyBDYWxjdWxhdGUgYXJyb3cgcG9zaXRpb24gd2l0aCBvZmZzZXRcblxuICAgICAgICAgIHZhciByID0gcG9pbnQuciArIHBvaW50Lm9mZnNldDtcbiAgICAgICAgICB2YXIgcG9zID0gW3B0MVswXSArIHIgKiBNYXRoLmNvcyhvcHQucm90YXRpb24pLCBwdDFbMV0gLSByICogTWF0aC5zaW4ob3B0LnJvdGF0aW9uKV07XG4gICAgICAgICAgc3ltYm9sLmF0dHIoe1xuICAgICAgICAgICAgcm90YXRpb246IHBvaW50LnJvdGF0ZSxcbiAgICAgICAgICAgIHBvc2l0aW9uOiBwb3MsXG4gICAgICAgICAgICBzaWxlbnQ6IHRydWUsXG4gICAgICAgICAgICB6MjogMTFcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmdyb3VwLmFkZChzeW1ib2wpO1xuICAgICAgICB9XG4gICAgICB9LCB0aGlzKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBheGlzVGlja0xhYmVsOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGF4aXNNb2RlbCA9IHRoaXMuYXhpc01vZGVsO1xuICAgIHZhciBvcHQgPSB0aGlzLm9wdDtcbiAgICB2YXIgdGlja0VscyA9IGJ1aWxkQXhpc1RpY2sodGhpcywgYXhpc01vZGVsLCBvcHQpO1xuICAgIHZhciBsYWJlbEVscyA9IGJ1aWxkQXhpc0xhYmVsKHRoaXMsIGF4aXNNb2RlbCwgb3B0KTtcbiAgICBmaXhNaW5NYXhMYWJlbFNob3coYXhpc01vZGVsLCBsYWJlbEVscywgdGlja0Vscyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBheGlzTmFtZTogZnVuY3Rpb24gKCkge1xuICAgIHZhciBvcHQgPSB0aGlzLm9wdDtcbiAgICB2YXIgYXhpc01vZGVsID0gdGhpcy5heGlzTW9kZWw7XG4gICAgdmFyIG5hbWUgPSByZXRyaWV2ZShvcHQuYXhpc05hbWUsIGF4aXNNb2RlbC5nZXQoJ25hbWUnKSk7XG5cbiAgICBpZiAoIW5hbWUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgbmFtZUxvY2F0aW9uID0gYXhpc01vZGVsLmdldCgnbmFtZUxvY2F0aW9uJyk7XG4gICAgdmFyIG5hbWVEaXJlY3Rpb24gPSBvcHQubmFtZURpcmVjdGlvbjtcbiAgICB2YXIgdGV4dFN0eWxlTW9kZWwgPSBheGlzTW9kZWwuZ2V0TW9kZWwoJ25hbWVUZXh0U3R5bGUnKTtcbiAgICB2YXIgZ2FwID0gYXhpc01vZGVsLmdldCgnbmFtZUdhcCcpIHx8IDA7XG4gICAgdmFyIGV4dGVudCA9IHRoaXMuYXhpc01vZGVsLmF4aXMuZ2V0RXh0ZW50KCk7XG4gICAgdmFyIGdhcFNpZ25hbCA9IGV4dGVudFswXSA+IGV4dGVudFsxXSA/IC0xIDogMTtcbiAgICB2YXIgcG9zID0gW25hbWVMb2NhdGlvbiA9PT0gJ3N0YXJ0JyA/IGV4dGVudFswXSAtIGdhcFNpZ25hbCAqIGdhcCA6IG5hbWVMb2NhdGlvbiA9PT0gJ2VuZCcgPyBleHRlbnRbMV0gKyBnYXBTaWduYWwgKiBnYXAgOiAoZXh0ZW50WzBdICsgZXh0ZW50WzFdKSAvIDIsIC8vICdtaWRkbGUnXG4gICAgLy8gUmV1c2UgbGFiZWxPZmZzZXQuXG4gICAgaXNOYW1lTG9jYXRpb25DZW50ZXIobmFtZUxvY2F0aW9uKSA/IG9wdC5sYWJlbE9mZnNldCArIG5hbWVEaXJlY3Rpb24gKiBnYXAgOiAwXTtcbiAgICB2YXIgbGFiZWxMYXlvdXQ7XG4gICAgdmFyIG5hbWVSb3RhdGlvbiA9IGF4aXNNb2RlbC5nZXQoJ25hbWVSb3RhdGUnKTtcblxuICAgIGlmIChuYW1lUm90YXRpb24gIT0gbnVsbCkge1xuICAgICAgbmFtZVJvdGF0aW9uID0gbmFtZVJvdGF0aW9uICogUEkgLyAxODA7IC8vIFRvIHJhZGlhbi5cbiAgICB9XG5cbiAgICB2YXIgYXhpc05hbWVBdmFpbGFibGVXaWR0aDtcblxuICAgIGlmIChpc05hbWVMb2NhdGlvbkNlbnRlcihuYW1lTG9jYXRpb24pKSB7XG4gICAgICBsYWJlbExheW91dCA9IGlubmVyVGV4dExheW91dChvcHQucm90YXRpb24sIG5hbWVSb3RhdGlvbiAhPSBudWxsID8gbmFtZVJvdGF0aW9uIDogb3B0LnJvdGF0aW9uLCAvLyBBZGFwdCB0byBheGlzLlxuICAgICAgbmFtZURpcmVjdGlvbik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGxhYmVsTGF5b3V0ID0gZW5kVGV4dExheW91dChvcHQsIG5hbWVMb2NhdGlvbiwgbmFtZVJvdGF0aW9uIHx8IDAsIGV4dGVudCk7XG4gICAgICBheGlzTmFtZUF2YWlsYWJsZVdpZHRoID0gb3B0LmF4aXNOYW1lQXZhaWxhYmxlV2lkdGg7XG5cbiAgICAgIGlmIChheGlzTmFtZUF2YWlsYWJsZVdpZHRoICE9IG51bGwpIHtcbiAgICAgICAgYXhpc05hbWVBdmFpbGFibGVXaWR0aCA9IE1hdGguYWJzKGF4aXNOYW1lQXZhaWxhYmxlV2lkdGggLyBNYXRoLnNpbihsYWJlbExheW91dC5yb3RhdGlvbikpO1xuICAgICAgICAhaXNGaW5pdGUoYXhpc05hbWVBdmFpbGFibGVXaWR0aCkgJiYgKGF4aXNOYW1lQXZhaWxhYmxlV2lkdGggPSBudWxsKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgdGV4dEZvbnQgPSB0ZXh0U3R5bGVNb2RlbC5nZXRGb250KCk7XG4gICAgdmFyIHRydW5jYXRlT3B0ID0gYXhpc01vZGVsLmdldCgnbmFtZVRydW5jYXRlJywgdHJ1ZSkgfHwge307XG4gICAgdmFyIGVsbGlwc2lzID0gdHJ1bmNhdGVPcHQuZWxsaXBzaXM7XG4gICAgdmFyIG1heFdpZHRoID0gcmV0cmlldmUob3B0Lm5hbWVUcnVuY2F0ZU1heFdpZHRoLCB0cnVuY2F0ZU9wdC5tYXhXaWR0aCwgYXhpc05hbWVBdmFpbGFibGVXaWR0aCk7IC8vIEZJWE1FXG4gICAgLy8gdHJ1bmNhdGUgcmljaCB0ZXh0PyAoY29uc2lkZXIgcGVyZm9ybWFuY2UpXG5cbiAgICB2YXIgdHJ1bmNhdGVkVGV4dCA9IGVsbGlwc2lzICE9IG51bGwgJiYgbWF4V2lkdGggIT0gbnVsbCA/IGZvcm1hdFV0aWwudHJ1bmNhdGVUZXh0KG5hbWUsIG1heFdpZHRoLCB0ZXh0Rm9udCwgZWxsaXBzaXMsIHtcbiAgICAgIG1pbkNoYXI6IDIsXG4gICAgICBwbGFjZWhvbGRlcjogdHJ1bmNhdGVPcHQucGxhY2Vob2xkZXJcbiAgICB9KSA6IG5hbWU7XG4gICAgdmFyIHRvb2x0aXBPcHQgPSBheGlzTW9kZWwuZ2V0KCd0b29sdGlwJywgdHJ1ZSk7XG4gICAgdmFyIG1haW5UeXBlID0gYXhpc01vZGVsLm1haW5UeXBlO1xuICAgIHZhciBmb3JtYXR0ZXJQYXJhbXMgPSB7XG4gICAgICBjb21wb25lbnRUeXBlOiBtYWluVHlwZSxcbiAgICAgIG5hbWU6IG5hbWUsXG4gICAgICAkdmFyczogWyduYW1lJ11cbiAgICB9O1xuICAgIGZvcm1hdHRlclBhcmFtc1ttYWluVHlwZSArICdJbmRleCddID0gYXhpc01vZGVsLmNvbXBvbmVudEluZGV4O1xuICAgIHZhciB0ZXh0RWwgPSBuZXcgZ3JhcGhpYy5UZXh0KHtcbiAgICAgIC8vIElkIGZvciBhbmltYXRpb25cbiAgICAgIGFuaWQ6ICduYW1lJyxcbiAgICAgIF9fZnVsbFRleHQ6IG5hbWUsXG4gICAgICBfX3RydW5jYXRlZFRleHQ6IHRydW5jYXRlZFRleHQsXG4gICAgICBwb3NpdGlvbjogcG9zLFxuICAgICAgcm90YXRpb246IGxhYmVsTGF5b3V0LnJvdGF0aW9uLFxuICAgICAgc2lsZW50OiBpc1NpbGVudChheGlzTW9kZWwpLFxuICAgICAgejI6IDEsXG4gICAgICB0b29sdGlwOiB0b29sdGlwT3B0ICYmIHRvb2x0aXBPcHQuc2hvdyA/IGV4dGVuZCh7XG4gICAgICAgIGNvbnRlbnQ6IG5hbWUsXG4gICAgICAgIGZvcm1hdHRlcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiBuYW1lO1xuICAgICAgICB9LFxuICAgICAgICBmb3JtYXR0ZXJQYXJhbXM6IGZvcm1hdHRlclBhcmFtc1xuICAgICAgfSwgdG9vbHRpcE9wdCkgOiBudWxsXG4gICAgfSk7XG4gICAgZ3JhcGhpYy5zZXRUZXh0U3R5bGUodGV4dEVsLnN0eWxlLCB0ZXh0U3R5bGVNb2RlbCwge1xuICAgICAgdGV4dDogdHJ1bmNhdGVkVGV4dCxcbiAgICAgIHRleHRGb250OiB0ZXh0Rm9udCxcbiAgICAgIHRleHRGaWxsOiB0ZXh0U3R5bGVNb2RlbC5nZXRUZXh0Q29sb3IoKSB8fCBheGlzTW9kZWwuZ2V0KCdheGlzTGluZS5saW5lU3R5bGUuY29sb3InKSxcbiAgICAgIHRleHRBbGlnbjogbGFiZWxMYXlvdXQudGV4dEFsaWduLFxuICAgICAgdGV4dFZlcnRpY2FsQWxpZ246IGxhYmVsTGF5b3V0LnRleHRWZXJ0aWNhbEFsaWduXG4gICAgfSk7XG5cbiAgICBpZiAoYXhpc01vZGVsLmdldCgndHJpZ2dlckV2ZW50JykpIHtcbiAgICAgIHRleHRFbC5ldmVudERhdGEgPSBtYWtlQXhpc0V2ZW50RGF0YUJhc2UoYXhpc01vZGVsKTtcbiAgICAgIHRleHRFbC5ldmVudERhdGEudGFyZ2V0VHlwZSA9ICdheGlzTmFtZSc7XG4gICAgICB0ZXh0RWwuZXZlbnREYXRhLm5hbWUgPSBuYW1lO1xuICAgIH0gLy8gRklYTUVcblxuXG4gICAgdGhpcy5fZHVtYkdyb3VwLmFkZCh0ZXh0RWwpO1xuXG4gICAgdGV4dEVsLnVwZGF0ZVRyYW5zZm9ybSgpO1xuICAgIHRoaXMuZ3JvdXAuYWRkKHRleHRFbCk7XG4gICAgdGV4dEVsLmRlY29tcG9zZVRyYW5zZm9ybSgpO1xuICB9XG59O1xuLyoqXG4gKiBAcHVibGljXG4gKiBAc3RhdGljXG4gKiBAcGFyYW0ge09iamVjdH0gb3B0XG4gKiBAcGFyYW0ge251bWJlcn0gYXhpc1JvdGF0aW9uIGluIHJhZGlhblxuICogQHBhcmFtIHtudW1iZXJ9IHRleHRSb3RhdGlvbiBpbiByYWRpYW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBkaXJlY3Rpb25cbiAqIEByZXR1cm4ge09iamVjdH0ge1xuICogIHJvdGF0aW9uLCAvLyBhY2NvcmRpbmcgdG8gYXhpc1xuICogIHRleHRBbGlnbixcbiAqICB0ZXh0VmVydGljYWxBbGlnblxuICogfVxuICovXG5cbnZhciBpbm5lclRleHRMYXlvdXQgPSBBeGlzQnVpbGRlci5pbm5lclRleHRMYXlvdXQgPSBmdW5jdGlvbiAoYXhpc1JvdGF0aW9uLCB0ZXh0Um90YXRpb24sIGRpcmVjdGlvbikge1xuICB2YXIgcm90YXRpb25EaWZmID0gcmVtUmFkaWFuKHRleHRSb3RhdGlvbiAtIGF4aXNSb3RhdGlvbik7XG4gIHZhciB0ZXh0QWxpZ247XG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbjtcblxuICBpZiAoaXNSYWRpYW5Bcm91bmRaZXJvKHJvdGF0aW9uRGlmZikpIHtcbiAgICAvLyBMYWJlbCBpcyBwYXJhbGxlbCB3aXRoIGF4aXMgbGluZS5cbiAgICB0ZXh0VmVydGljYWxBbGlnbiA9IGRpcmVjdGlvbiA+IDAgPyAndG9wJyA6ICdib3R0b20nO1xuICAgIHRleHRBbGlnbiA9ICdjZW50ZXInO1xuICB9IGVsc2UgaWYgKGlzUmFkaWFuQXJvdW5kWmVybyhyb3RhdGlvbkRpZmYgLSBQSSkpIHtcbiAgICAvLyBMYWJlbCBpcyBpbnZlcnNlIHBhcmFsbGVsIHdpdGggYXhpcyBsaW5lLlxuICAgIHRleHRWZXJ0aWNhbEFsaWduID0gZGlyZWN0aW9uID4gMCA/ICdib3R0b20nIDogJ3RvcCc7XG4gICAgdGV4dEFsaWduID0gJ2NlbnRlcic7XG4gIH0gZWxzZSB7XG4gICAgdGV4dFZlcnRpY2FsQWxpZ24gPSAnbWlkZGxlJztcblxuICAgIGlmIChyb3RhdGlvbkRpZmYgPiAwICYmIHJvdGF0aW9uRGlmZiA8IFBJKSB7XG4gICAgICB0ZXh0QWxpZ24gPSBkaXJlY3Rpb24gPiAwID8gJ3JpZ2h0JyA6ICdsZWZ0JztcbiAgICB9IGVsc2Uge1xuICAgICAgdGV4dEFsaWduID0gZGlyZWN0aW9uID4gMCA/ICdsZWZ0JyA6ICdyaWdodCc7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICByb3RhdGlvbjogcm90YXRpb25EaWZmLFxuICAgIHRleHRBbGlnbjogdGV4dEFsaWduLFxuICAgIHRleHRWZXJ0aWNhbEFsaWduOiB0ZXh0VmVydGljYWxBbGlnblxuICB9O1xufTtcblxuZnVuY3Rpb24gZW5kVGV4dExheW91dChvcHQsIHRleHRQb3NpdGlvbiwgdGV4dFJvdGF0ZSwgZXh0ZW50KSB7XG4gIHZhciByb3RhdGlvbkRpZmYgPSByZW1SYWRpYW4odGV4dFJvdGF0ZSAtIG9wdC5yb3RhdGlvbik7XG4gIHZhciB0ZXh0QWxpZ247XG4gIHZhciB0ZXh0VmVydGljYWxBbGlnbjtcbiAgdmFyIGludmVyc2UgPSBleHRlbnRbMF0gPiBleHRlbnRbMV07XG4gIHZhciBvbkxlZnQgPSB0ZXh0UG9zaXRpb24gPT09ICdzdGFydCcgJiYgIWludmVyc2UgfHwgdGV4dFBvc2l0aW9uICE9PSAnc3RhcnQnICYmIGludmVyc2U7XG5cbiAgaWYgKGlzUmFkaWFuQXJvdW5kWmVybyhyb3RhdGlvbkRpZmYgLSBQSSAvIDIpKSB7XG4gICAgdGV4dFZlcnRpY2FsQWxpZ24gPSBvbkxlZnQgPyAnYm90dG9tJyA6ICd0b3AnO1xuICAgIHRleHRBbGlnbiA9ICdjZW50ZXInO1xuICB9IGVsc2UgaWYgKGlzUmFkaWFuQXJvdW5kWmVybyhyb3RhdGlvbkRpZmYgLSBQSSAqIDEuNSkpIHtcbiAgICB0ZXh0VmVydGljYWxBbGlnbiA9IG9uTGVmdCA/ICd0b3AnIDogJ2JvdHRvbSc7XG4gICAgdGV4dEFsaWduID0gJ2NlbnRlcic7XG4gIH0gZWxzZSB7XG4gICAgdGV4dFZlcnRpY2FsQWxpZ24gPSAnbWlkZGxlJztcblxuICAgIGlmIChyb3RhdGlvbkRpZmYgPCBQSSAqIDEuNSAmJiByb3RhdGlvbkRpZmYgPiBQSSAvIDIpIHtcbiAgICAgIHRleHRBbGlnbiA9IG9uTGVmdCA/ICdsZWZ0JyA6ICdyaWdodCc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRleHRBbGlnbiA9IG9uTGVmdCA/ICdyaWdodCcgOiAnbGVmdCc7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICByb3RhdGlvbjogcm90YXRpb25EaWZmLFxuICAgIHRleHRBbGlnbjogdGV4dEFsaWduLFxuICAgIHRleHRWZXJ0aWNhbEFsaWduOiB0ZXh0VmVydGljYWxBbGlnblxuICB9O1xufVxuXG5mdW5jdGlvbiBpc1NpbGVudChheGlzTW9kZWwpIHtcbiAgdmFyIHRvb2x0aXBPcHQgPSBheGlzTW9kZWwuZ2V0KCd0b29sdGlwJyk7XG4gIHJldHVybiBheGlzTW9kZWwuZ2V0KCdzaWxlbnQnKSAvLyBDb25zaWRlciBtb3VzZSBjdXJzb3IsIGFkZCB0aGVzZSByZXN0cmljdGlvbnMuXG4gIHx8ICEoYXhpc01vZGVsLmdldCgndHJpZ2dlckV2ZW50JykgfHwgdG9vbHRpcE9wdCAmJiB0b29sdGlwT3B0LnNob3cpO1xufVxuXG5mdW5jdGlvbiBmaXhNaW5NYXhMYWJlbFNob3coYXhpc01vZGVsLCBsYWJlbEVscywgdGlja0Vscykge1xuICBpZiAoc2hvdWxkU2hvd0FsbExhYmVscyhheGlzTW9kZWwuYXhpcykpIHtcbiAgICByZXR1cm47XG4gIH0gLy8gSWYgbWluIG9yIG1heCBhcmUgdXNlciBzZXQsIHdlIG5lZWQgdG8gY2hlY2tcbiAgLy8gSWYgdGhlIHRpY2sgb24gbWluKG1heCkgYXJlIG92ZXJsYXAgb24gdGhlaXIgbmVpZ2hib3VyIHRpY2tcbiAgLy8gSWYgdGhleSBhcmUgb3ZlcmxhcHBlZCwgd2UgbmVlZCB0byBoaWRlIHRoZSBtaW4obWF4KSB0aWNrIGxhYmVsXG5cblxuICB2YXIgc2hvd01pbkxhYmVsID0gYXhpc01vZGVsLmdldCgnYXhpc0xhYmVsLnNob3dNaW5MYWJlbCcpO1xuICB2YXIgc2hvd01heExhYmVsID0gYXhpc01vZGVsLmdldCgnYXhpc0xhYmVsLnNob3dNYXhMYWJlbCcpOyAvLyBGSVhNRVxuICAvLyBIYXZlIG5vdCBjb25zaWRlciBvbkJhbmQgeWV0LCB3aGVyZSB0aWNrIGVscyBpcyBtb3JlIHRoYW4gbGFiZWwgZWxzLlxuXG4gIGxhYmVsRWxzID0gbGFiZWxFbHMgfHwgW107XG4gIHRpY2tFbHMgPSB0aWNrRWxzIHx8IFtdO1xuICB2YXIgZmlyc3RMYWJlbCA9IGxhYmVsRWxzWzBdO1xuICB2YXIgbmV4dExhYmVsID0gbGFiZWxFbHNbMV07XG4gIHZhciBsYXN0TGFiZWwgPSBsYWJlbEVsc1tsYWJlbEVscy5sZW5ndGggLSAxXTtcbiAgdmFyIHByZXZMYWJlbCA9IGxhYmVsRWxzW2xhYmVsRWxzLmxlbmd0aCAtIDJdO1xuICB2YXIgZmlyc3RUaWNrID0gdGlja0Vsc1swXTtcbiAgdmFyIG5leHRUaWNrID0gdGlja0Vsc1sxXTtcbiAgdmFyIGxhc3RUaWNrID0gdGlja0Vsc1t0aWNrRWxzLmxlbmd0aCAtIDFdO1xuICB2YXIgcHJldlRpY2sgPSB0aWNrRWxzW3RpY2tFbHMubGVuZ3RoIC0gMl07XG5cbiAgaWYgKHNob3dNaW5MYWJlbCA9PT0gZmFsc2UpIHtcbiAgICBpZ25vcmVFbChmaXJzdExhYmVsKTtcbiAgICBpZ25vcmVFbChmaXJzdFRpY2spO1xuICB9IGVsc2UgaWYgKGlzVHdvTGFiZWxPdmVybGFwcGVkKGZpcnN0TGFiZWwsIG5leHRMYWJlbCkpIHtcbiAgICBpZiAoc2hvd01pbkxhYmVsKSB7XG4gICAgICBpZ25vcmVFbChuZXh0TGFiZWwpO1xuICAgICAgaWdub3JlRWwobmV4dFRpY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZ25vcmVFbChmaXJzdExhYmVsKTtcbiAgICAgIGlnbm9yZUVsKGZpcnN0VGljayk7XG4gICAgfVxuICB9XG5cbiAgaWYgKHNob3dNYXhMYWJlbCA9PT0gZmFsc2UpIHtcbiAgICBpZ25vcmVFbChsYXN0TGFiZWwpO1xuICAgIGlnbm9yZUVsKGxhc3RUaWNrKTtcbiAgfSBlbHNlIGlmIChpc1R3b0xhYmVsT3ZlcmxhcHBlZChwcmV2TGFiZWwsIGxhc3RMYWJlbCkpIHtcbiAgICBpZiAoc2hvd01heExhYmVsKSB7XG4gICAgICBpZ25vcmVFbChwcmV2TGFiZWwpO1xuICAgICAgaWdub3JlRWwocHJldlRpY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZ25vcmVFbChsYXN0TGFiZWwpO1xuICAgICAgaWdub3JlRWwobGFzdFRpY2spO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBpZ25vcmVFbChlbCkge1xuICBlbCAmJiAoZWwuaWdub3JlID0gdHJ1ZSk7XG59XG5cbmZ1bmN0aW9uIGlzVHdvTGFiZWxPdmVybGFwcGVkKGN1cnJlbnQsIG5leHQsIGxhYmVsTGF5b3V0KSB7XG4gIC8vIGN1cnJlbnQgYW5kIG5leHQgaGFzIHRoZSBzYW1lIHJvdGF0aW9uLlxuICB2YXIgZmlyc3RSZWN0ID0gY3VycmVudCAmJiBjdXJyZW50LmdldEJvdW5kaW5nUmVjdCgpLmNsb25lKCk7XG4gIHZhciBuZXh0UmVjdCA9IG5leHQgJiYgbmV4dC5nZXRCb3VuZGluZ1JlY3QoKS5jbG9uZSgpO1xuXG4gIGlmICghZmlyc3RSZWN0IHx8ICFuZXh0UmVjdCkge1xuICAgIHJldHVybjtcbiAgfSAvLyBXaGVuIGNoZWNraW5nIGludGVyc2VjdCBvZiB0d28gcm90YXRlZCBsYWJlbHMsIHdlIHVzZSBtUm90YXRpb25CYWNrXG4gIC8vIHRvIGF2b2lkIHRoYXQgYm91bmRpbmdSZWN0IGlzIGVubGFyZ2Ugd2hlbiB1c2luZyBgYm91bmRpbmdSZWN0LmFwcGx5VHJhbnNmb3JtYC5cblxuXG4gIHZhciBtUm90YXRpb25CYWNrID0gbWF0cml4VXRpbC5pZGVudGl0eShbXSk7XG4gIG1hdHJpeFV0aWwucm90YXRlKG1Sb3RhdGlvbkJhY2ssIG1Sb3RhdGlvbkJhY2ssIC1jdXJyZW50LnJvdGF0aW9uKTtcbiAgZmlyc3RSZWN0LmFwcGx5VHJhbnNmb3JtKG1hdHJpeFV0aWwubXVsKFtdLCBtUm90YXRpb25CYWNrLCBjdXJyZW50LmdldExvY2FsVHJhbnNmb3JtKCkpKTtcbiAgbmV4dFJlY3QuYXBwbHlUcmFuc2Zvcm0obWF0cml4VXRpbC5tdWwoW10sIG1Sb3RhdGlvbkJhY2ssIG5leHQuZ2V0TG9jYWxUcmFuc2Zvcm0oKSkpO1xuICByZXR1cm4gZmlyc3RSZWN0LmludGVyc2VjdChuZXh0UmVjdCk7XG59XG5cbmZ1bmN0aW9uIGlzTmFtZUxvY2F0aW9uQ2VudGVyKG5hbWVMb2NhdGlvbikge1xuICByZXR1cm4gbmFtZUxvY2F0aW9uID09PSAnbWlkZGxlJyB8fCBuYW1lTG9jYXRpb24gPT09ICdjZW50ZXInO1xufVxuXG5mdW5jdGlvbiBidWlsZEF4aXNUaWNrKGF4aXNCdWlsZGVyLCBheGlzTW9kZWwsIG9wdCkge1xuICB2YXIgYXhpcyA9IGF4aXNNb2RlbC5heGlzO1xuXG4gIGlmICghYXhpc01vZGVsLmdldCgnYXhpc1RpY2suc2hvdycpIHx8IGF4aXMuc2NhbGUuaXNCbGFuaygpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHRpY2tNb2RlbCA9IGF4aXNNb2RlbC5nZXRNb2RlbCgnYXhpc1RpY2snKTtcbiAgdmFyIGxpbmVTdHlsZU1vZGVsID0gdGlja01vZGVsLmdldE1vZGVsKCdsaW5lU3R5bGUnKTtcbiAgdmFyIHRpY2tMZW4gPSB0aWNrTW9kZWwuZ2V0KCdsZW5ndGgnKTtcbiAgdmFyIHRpY2tzQ29vcmRzID0gYXhpcy5nZXRUaWNrc0Nvb3JkcygpO1xuICB2YXIgcHQxID0gW107XG4gIHZhciBwdDIgPSBbXTtcbiAgdmFyIG1hdHJpeCA9IGF4aXNCdWlsZGVyLl90cmFuc2Zvcm07XG4gIHZhciB0aWNrRWxzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aWNrc0Nvb3Jkcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciB0aWNrQ29vcmQgPSB0aWNrc0Nvb3Jkc1tpXS5jb29yZDtcbiAgICBwdDFbMF0gPSB0aWNrQ29vcmQ7XG4gICAgcHQxWzFdID0gMDtcbiAgICBwdDJbMF0gPSB0aWNrQ29vcmQ7XG4gICAgcHQyWzFdID0gb3B0LnRpY2tEaXJlY3Rpb24gKiB0aWNrTGVuO1xuXG4gICAgaWYgKG1hdHJpeCkge1xuICAgICAgdjJBcHBseVRyYW5zZm9ybShwdDEsIHB0MSwgbWF0cml4KTtcbiAgICAgIHYyQXBwbHlUcmFuc2Zvcm0ocHQyLCBwdDIsIG1hdHJpeCk7XG4gICAgfSAvLyBUaWNrIGxpbmUsIE5vdCB1c2UgZ3JvdXAgdHJhbnNmb3JtIHRvIGhhdmUgYmV0dGVyIGxpbmUgZHJhd1xuXG5cbiAgICB2YXIgdGlja0VsID0gbmV3IGdyYXBoaWMuTGluZShncmFwaGljLnN1YlBpeGVsT3B0aW1pemVMaW5lKHtcbiAgICAgIC8vIElkIGZvciBhbmltYXRpb25cbiAgICAgIGFuaWQ6ICd0aWNrXycgKyB0aWNrc0Nvb3Jkc1tpXS50aWNrVmFsdWUsXG4gICAgICBzaGFwZToge1xuICAgICAgICB4MTogcHQxWzBdLFxuICAgICAgICB5MTogcHQxWzFdLFxuICAgICAgICB4MjogcHQyWzBdLFxuICAgICAgICB5MjogcHQyWzFdXG4gICAgICB9LFxuICAgICAgc3R5bGU6IGRlZmF1bHRzKGxpbmVTdHlsZU1vZGVsLmdldExpbmVTdHlsZSgpLCB7XG4gICAgICAgIHN0cm9rZTogYXhpc01vZGVsLmdldCgnYXhpc0xpbmUubGluZVN0eWxlLmNvbG9yJylcbiAgICAgIH0pLFxuICAgICAgejI6IDIsXG4gICAgICBzaWxlbnQ6IHRydWVcbiAgICB9KSk7XG4gICAgYXhpc0J1aWxkZXIuZ3JvdXAuYWRkKHRpY2tFbCk7XG4gICAgdGlja0Vscy5wdXNoKHRpY2tFbCk7XG4gIH1cblxuICByZXR1cm4gdGlja0Vscztcbn1cblxuZnVuY3Rpb24gYnVpbGRBeGlzTGFiZWwoYXhpc0J1aWxkZXIsIGF4aXNNb2RlbCwgb3B0KSB7XG4gIHZhciBheGlzID0gYXhpc01vZGVsLmF4aXM7XG4gIHZhciBzaG93ID0gcmV0cmlldmUob3B0LmF4aXNMYWJlbFNob3csIGF4aXNNb2RlbC5nZXQoJ2F4aXNMYWJlbC5zaG93JykpO1xuXG4gIGlmICghc2hvdyB8fCBheGlzLnNjYWxlLmlzQmxhbmsoKSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBsYWJlbE1vZGVsID0gYXhpc01vZGVsLmdldE1vZGVsKCdheGlzTGFiZWwnKTtcbiAgdmFyIGxhYmVsTWFyZ2luID0gbGFiZWxNb2RlbC5nZXQoJ21hcmdpbicpO1xuICB2YXIgbGFiZWxzID0gYXhpcy5nZXRWaWV3TGFiZWxzKCk7IC8vIFNwZWNpYWwgbGFiZWwgcm90YXRlLlxuXG4gIHZhciBsYWJlbFJvdGF0aW9uID0gKHJldHJpZXZlKG9wdC5sYWJlbFJvdGF0ZSwgbGFiZWxNb2RlbC5nZXQoJ3JvdGF0ZScpKSB8fCAwKSAqIFBJIC8gMTgwO1xuICB2YXIgbGFiZWxMYXlvdXQgPSBpbm5lclRleHRMYXlvdXQob3B0LnJvdGF0aW9uLCBsYWJlbFJvdGF0aW9uLCBvcHQubGFiZWxEaXJlY3Rpb24pO1xuICB2YXIgcmF3Q2F0ZWdvcnlEYXRhID0gYXhpc01vZGVsLmdldENhdGVnb3JpZXModHJ1ZSk7XG4gIHZhciBsYWJlbEVscyA9IFtdO1xuICB2YXIgc2lsZW50ID0gaXNTaWxlbnQoYXhpc01vZGVsKTtcbiAgdmFyIHRyaWdnZXJFdmVudCA9IGF4aXNNb2RlbC5nZXQoJ3RyaWdnZXJFdmVudCcpO1xuICBlYWNoKGxhYmVscywgZnVuY3Rpb24gKGxhYmVsSXRlbSwgaW5kZXgpIHtcbiAgICB2YXIgdGlja1ZhbHVlID0gbGFiZWxJdGVtLnRpY2tWYWx1ZTtcbiAgICB2YXIgZm9ybWF0dGVkTGFiZWwgPSBsYWJlbEl0ZW0uZm9ybWF0dGVkTGFiZWw7XG4gICAgdmFyIHJhd0xhYmVsID0gbGFiZWxJdGVtLnJhd0xhYmVsO1xuICAgIHZhciBpdGVtTGFiZWxNb2RlbCA9IGxhYmVsTW9kZWw7XG5cbiAgICBpZiAocmF3Q2F0ZWdvcnlEYXRhICYmIHJhd0NhdGVnb3J5RGF0YVt0aWNrVmFsdWVdICYmIHJhd0NhdGVnb3J5RGF0YVt0aWNrVmFsdWVdLnRleHRTdHlsZSkge1xuICAgICAgaXRlbUxhYmVsTW9kZWwgPSBuZXcgTW9kZWwocmF3Q2F0ZWdvcnlEYXRhW3RpY2tWYWx1ZV0udGV4dFN0eWxlLCBsYWJlbE1vZGVsLCBheGlzTW9kZWwuZWNNb2RlbCk7XG4gICAgfVxuXG4gICAgdmFyIHRleHRDb2xvciA9IGl0ZW1MYWJlbE1vZGVsLmdldFRleHRDb2xvcigpIHx8IGF4aXNNb2RlbC5nZXQoJ2F4aXNMaW5lLmxpbmVTdHlsZS5jb2xvcicpO1xuICAgIHZhciB0aWNrQ29vcmQgPSBheGlzLmRhdGFUb0Nvb3JkKHRpY2tWYWx1ZSk7XG4gICAgdmFyIHBvcyA9IFt0aWNrQ29vcmQsIG9wdC5sYWJlbE9mZnNldCArIG9wdC5sYWJlbERpcmVjdGlvbiAqIGxhYmVsTWFyZ2luXTtcbiAgICB2YXIgdGV4dEVsID0gbmV3IGdyYXBoaWMuVGV4dCh7XG4gICAgICAvLyBJZCBmb3IgYW5pbWF0aW9uXG4gICAgICBhbmlkOiAnbGFiZWxfJyArIHRpY2tWYWx1ZSxcbiAgICAgIHBvc2l0aW9uOiBwb3MsXG4gICAgICByb3RhdGlvbjogbGFiZWxMYXlvdXQucm90YXRpb24sXG4gICAgICBzaWxlbnQ6IHNpbGVudCxcbiAgICAgIHoyOiAxMFxuICAgIH0pO1xuICAgIGdyYXBoaWMuc2V0VGV4dFN0eWxlKHRleHRFbC5zdHlsZSwgaXRlbUxhYmVsTW9kZWwsIHtcbiAgICAgIHRleHQ6IGZvcm1hdHRlZExhYmVsLFxuICAgICAgdGV4dEFsaWduOiBpdGVtTGFiZWxNb2RlbC5nZXRTaGFsbG93KCdhbGlnbicsIHRydWUpIHx8IGxhYmVsTGF5b3V0LnRleHRBbGlnbixcbiAgICAgIHRleHRWZXJ0aWNhbEFsaWduOiBpdGVtTGFiZWxNb2RlbC5nZXRTaGFsbG93KCd2ZXJ0aWNhbEFsaWduJywgdHJ1ZSkgfHwgaXRlbUxhYmVsTW9kZWwuZ2V0U2hhbGxvdygnYmFzZWxpbmUnLCB0cnVlKSB8fCBsYWJlbExheW91dC50ZXh0VmVydGljYWxBbGlnbixcbiAgICAgIHRleHRGaWxsOiB0eXBlb2YgdGV4dENvbG9yID09PSAnZnVuY3Rpb24nID8gdGV4dENvbG9yKCAvLyAoMSkgSW4gY2F0ZWdvcnkgYXhpcyB3aXRoIGRhdGEgem9vbSwgdGljayBpcyBub3QgdGhlIG9yaWdpbmFsXG4gICAgICAvLyBpbmRleCBvZiBheGlzLmRhdGEuIFNvIHRpY2sgc2hvdWxkIG5vdCBiZSBleHBvc2VkIHRvIHVzZXJcbiAgICAgIC8vIGluIGNhdGVnb3J5IGF4aXMuXG4gICAgICAvLyAoMikgQ29tcGF0aWJsZSB3aXRoIHByZXZpb3VzIHZlcnNpb24sIHdoaWNoIGFsd2F5cyB1c2UgZm9ybWF0dGVkIGxhYmVsIGFzXG4gICAgICAvLyBpbnB1dC4gQnV0IGluIGludGVydmFsIHNjYWxlIHRoZSBmb3JtYXR0ZWQgbGFiZWwgaXMgbGlrZSAnMjIzLDQ0NScsIHdoaWNoXG4gICAgICAvLyBtYWtlZCB1c2VyIHJlcGFsY2UgJywnLiBTbyB3ZSBtb2RpZnkgaXQgdG8gcmV0dXJuIG9yaWdpbmFsIHZhbCBidXQgcmVtYWluXG4gICAgICAvLyBpdCBhcyAnc3RyaW5nJyB0byBhdm9pZCBlcnJvciBpbiByZXBsYWNpbmcuXG4gICAgICBheGlzLnR5cGUgPT09ICdjYXRlZ29yeScgPyByYXdMYWJlbCA6IGF4aXMudHlwZSA9PT0gJ3ZhbHVlJyA/IHRpY2tWYWx1ZSArICcnIDogdGlja1ZhbHVlLCBpbmRleCkgOiB0ZXh0Q29sb3JcbiAgICB9KTsgLy8gUGFjayBkYXRhIGZvciBtb3VzZSBldmVudFxuXG4gICAgaWYgKHRyaWdnZXJFdmVudCkge1xuICAgICAgdGV4dEVsLmV2ZW50RGF0YSA9IG1ha2VBeGlzRXZlbnREYXRhQmFzZShheGlzTW9kZWwpO1xuICAgICAgdGV4dEVsLmV2ZW50RGF0YS50YXJnZXRUeXBlID0gJ2F4aXNMYWJlbCc7XG4gICAgICB0ZXh0RWwuZXZlbnREYXRhLnZhbHVlID0gcmF3TGFiZWw7XG4gICAgfSAvLyBGSVhNRVxuXG5cbiAgICBheGlzQnVpbGRlci5fZHVtYkdyb3VwLmFkZCh0ZXh0RWwpO1xuXG4gICAgdGV4dEVsLnVwZGF0ZVRyYW5zZm9ybSgpO1xuICAgIGxhYmVsRWxzLnB1c2godGV4dEVsKTtcbiAgICBheGlzQnVpbGRlci5ncm91cC5hZGQodGV4dEVsKTtcbiAgICB0ZXh0RWwuZGVjb21wb3NlVHJhbnNmb3JtKCk7XG4gIH0pO1xuICByZXR1cm4gbGFiZWxFbHM7XG59XG5cbnZhciBfZGVmYXVsdCA9IEF4aXNCdWlsZGVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0NBO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBWUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBVEE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbk1BO0FBcU1BOzs7Ozs7Ozs7Ozs7OztBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFDQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/axis/AxisBuilder.js
