__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return customHTML2Content; });
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_toArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/toArray */ "./node_modules/lodash/toArray.js");
/* harmony import */ var lodash_toArray__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_toArray__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! immutable */ "./node_modules/rc-editor-core/node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_2__);




function compose() {
  for (var _len = arguments.length, argument = Array(_len), _key = 0; _key < _len; _key++) {
    argument[_key] = arguments[_key];
  }

  var args = arguments;
  var start = args.length - 1;
  return function () {
    var i = start;
    var result = args[start].apply(this, arguments);

    while (i--) {
      result = args[i].call(this, result);
    }

    return result;
  };
}

;
/*
 * Helpers
 */
// Prepares img meta data object based on img attributes

var getBlockSpecForElement = function getBlockSpecForElement(imgElement) {
  return {
    contentType: 'image',
    src: imgElement.getAttribute('src'),
    width: imgElement.getAttribute('width'),
    height: imgElement.getAttribute('height'),
    align: imgElement.style.cssFloat
  };
}; // Wraps meta data in HTML element which is 'understandable' by Draft, I used <blockquote />.


var wrapBlockSpec = function wrapBlockSpec(blockSpec) {
  if (blockSpec == null) {
    return null;
  }

  var tempEl = document.createElement('blockquote'); // stringify meta data and insert it as text content of temp HTML element. We will later extract
  // and parse it.

  tempEl.innerText = JSON.stringify(blockSpec);
  return tempEl;
}; // Replaces <img> element with our temp element


var replaceElement = function replaceElement(oldEl, newEl) {
  if (!(newEl instanceof HTMLElement)) {
    return;
  }

  var parentNode = oldEl.parentNode;
  return parentNode.replaceChild(newEl, oldEl);
};

var elementToBlockSpecElement = compose(wrapBlockSpec, getBlockSpecForElement);

var imgReplacer = function imgReplacer(imgElement) {
  return replaceElement(imgElement, elementToBlockSpecElement(imgElement));
}; // creates ContentBlock based on provided spec


var createContentBlock = function createContentBlock(blockData, contentState) {
  var key = blockData.key,
      type = blockData.type,
      text = blockData.text,
      data = blockData.data,
      inlineStyles = blockData.inlineStyles,
      entityData = blockData.entityData;
  var blockSpec = {
    type: type != null ? type : 'unstyled',
    text: text != null ? text : '',
    key: key != null ? key : Object(draft_js__WEBPACK_IMPORTED_MODULE_0__["genKey"])(),
    data: null,
    characterList: Object(immutable__WEBPACK_IMPORTED_MODULE_2__["List"])([])
  };

  if (data) {
    blockSpec.data = Object(immutable__WEBPACK_IMPORTED_MODULE_2__["fromJS"])(data);
  }

  if (inlineStyles || entityData) {
    var entityKey = void 0;

    if (entityData) {
      var _type = entityData.type,
          mutability = entityData.mutability,
          _data = entityData.data;
      contentState.createEntity(_type, mutability, _data);
      entityKey = contentState.getLastCreatedEntityKey();
    } else {
      entityKey = null;
    }

    var style = Object(immutable__WEBPACK_IMPORTED_MODULE_2__["OrderedSet"])(inlineStyles || []);
    var charData = draft_js__WEBPACK_IMPORTED_MODULE_0__["CharacterMetadata"].create({
      style: style,
      entityKey: entityKey
    });
    blockSpec.characterList = Object(immutable__WEBPACK_IMPORTED_MODULE_2__["List"])(Object(immutable__WEBPACK_IMPORTED_MODULE_2__["Repeat"])(charData, text.length));
  }

  return new draft_js__WEBPACK_IMPORTED_MODULE_0__["ContentBlock"](blockSpec);
}; // takes HTML string and returns DraftJS ContentState


function customHTML2Content(HTML, contentState) {
  var tempDoc = new DOMParser().parseFromString(HTML, 'text/html'); // replace all <img /> with <blockquote /> elements

  lodash_toArray__WEBPACK_IMPORTED_MODULE_1___default()(tempDoc.querySelectorAll('img')).forEach(imgReplacer); // use DraftJS converter to do initial conversion. I don't provide DOMBuilder and
  // blockRenderMap arguments here since it should fall back to its default ones, which are fine

  var _convertFromHTML = Object(draft_js__WEBPACK_IMPORTED_MODULE_0__["convertFromHTML"])(tempDoc.body.innerHTML),
      contentBlocks = _convertFromHTML.contentBlocks; // now replace <blockquote /> ContentBlocks with 'atomic' ones


  contentBlocks = contentBlocks.reduce(function (contentBlocks, block) {
    if (block.getType() !== 'blockquote') {
      return contentBlocks.concat(block);
    }

    var image = JSON.parse(block.getText());
    contentState.createEntity('IMAGE-ENTITY', 'IMMUTABLE', image);
    var entityKey = contentState.getLastCreatedEntityKey();
    var charData = draft_js__WEBPACK_IMPORTED_MODULE_0__["CharacterMetadata"].create({
      entity: entityKey
    }); // const blockSpec = Object.assign({ type: 'atomic', text: ' ' }, { entityData })
    // const atomicBlock = createContentBlock(blockSpec)
    // const spacerBlock = createContentBlock({});

    var fragmentArray = [new draft_js__WEBPACK_IMPORTED_MODULE_0__["ContentBlock"]({
      key: Object(draft_js__WEBPACK_IMPORTED_MODULE_0__["genKey"])(),
      type: 'image-block',
      text: ' ',
      characterList: Object(immutable__WEBPACK_IMPORTED_MODULE_2__["List"])(Object(immutable__WEBPACK_IMPORTED_MODULE_2__["Repeat"])(charData, charData.count()))
    }), new draft_js__WEBPACK_IMPORTED_MODULE_0__["ContentBlock"]({
      key: Object(draft_js__WEBPACK_IMPORTED_MODULE_0__["genKey"])(),
      type: 'unstyled',
      text: '',
      characterList: Object(immutable__WEBPACK_IMPORTED_MODULE_2__["List"])()
    })];
    return contentBlocks.concat(fragmentArray);
  }, []); // console.log('>> customHTML2Content contentBlocks', contentBlocks);

  tempDoc = null;
  return draft_js__WEBPACK_IMPORTED_MODULE_0__["BlockMapBuilder"].createFromArray(contentBlocks);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLWNvcmUvZXMvRWRpdG9yQ29yZS9jdXN0b21IVE1MMkNvbnRlbnQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItY29yZS9lcy9FZGl0b3JDb3JlL2N1c3RvbUhUTUwyQ29udGVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCbG9ja01hcEJ1aWxkZXIsIGdlbktleSwgQ2hhcmFjdGVyTWV0YWRhdGEsIENvbnRlbnRCbG9jaywgY29udmVydEZyb21IVE1MIH0gZnJvbSAnZHJhZnQtanMnO1xuaW1wb3J0IHRvQXJyYXkgZnJvbSAnbG9kYXNoL3RvQXJyYXknO1xuaW1wb3J0IHsgTGlzdCwgT3JkZXJlZFNldCwgUmVwZWF0LCBmcm9tSlMgfSBmcm9tICdpbW11dGFibGUnO1xuZnVuY3Rpb24gY29tcG9zZSgpIHtcbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJndW1lbnQgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJndW1lbnRbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgdmFyIGFyZ3MgPSBhcmd1bWVudHM7XG4gICAgdmFyIHN0YXJ0ID0gYXJncy5sZW5ndGggLSAxO1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBpID0gc3RhcnQ7XG4gICAgICAgIHZhciByZXN1bHQgPSBhcmdzW3N0YXJ0XS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICB3aGlsZSAoaS0tKSB7XG4gICAgICAgICAgICByZXN1bHQgPSBhcmdzW2ldLmNhbGwodGhpcywgcmVzdWx0KTtcbiAgICAgICAgfXJldHVybiByZXN1bHQ7XG4gICAgfTtcbn1cbjtcbi8qXG4gKiBIZWxwZXJzXG4gKi9cbi8vIFByZXBhcmVzIGltZyBtZXRhIGRhdGEgb2JqZWN0IGJhc2VkIG9uIGltZyBhdHRyaWJ1dGVzXG52YXIgZ2V0QmxvY2tTcGVjRm9yRWxlbWVudCA9IGZ1bmN0aW9uIGdldEJsb2NrU3BlY0ZvckVsZW1lbnQoaW1nRWxlbWVudCkge1xuICAgIHJldHVybiB7XG4gICAgICAgIGNvbnRlbnRUeXBlOiAnaW1hZ2UnLFxuICAgICAgICBzcmM6IGltZ0VsZW1lbnQuZ2V0QXR0cmlidXRlKCdzcmMnKSxcbiAgICAgICAgd2lkdGg6IGltZ0VsZW1lbnQuZ2V0QXR0cmlidXRlKCd3aWR0aCcpLFxuICAgICAgICBoZWlnaHQ6IGltZ0VsZW1lbnQuZ2V0QXR0cmlidXRlKCdoZWlnaHQnKSxcbiAgICAgICAgYWxpZ246IGltZ0VsZW1lbnQuc3R5bGUuY3NzRmxvYXRcbiAgICB9O1xufTtcbi8vIFdyYXBzIG1ldGEgZGF0YSBpbiBIVE1MIGVsZW1lbnQgd2hpY2ggaXMgJ3VuZGVyc3RhbmRhYmxlJyBieSBEcmFmdCwgSSB1c2VkIDxibG9ja3F1b3RlIC8+LlxudmFyIHdyYXBCbG9ja1NwZWMgPSBmdW5jdGlvbiB3cmFwQmxvY2tTcGVjKGJsb2NrU3BlYykge1xuICAgIGlmIChibG9ja1NwZWMgPT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgdmFyIHRlbXBFbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2Jsb2NrcXVvdGUnKTtcbiAgICAvLyBzdHJpbmdpZnkgbWV0YSBkYXRhIGFuZCBpbnNlcnQgaXQgYXMgdGV4dCBjb250ZW50IG9mIHRlbXAgSFRNTCBlbGVtZW50LiBXZSB3aWxsIGxhdGVyIGV4dHJhY3RcbiAgICAvLyBhbmQgcGFyc2UgaXQuXG4gICAgdGVtcEVsLmlubmVyVGV4dCA9IEpTT04uc3RyaW5naWZ5KGJsb2NrU3BlYyk7XG4gICAgcmV0dXJuIHRlbXBFbDtcbn07XG4vLyBSZXBsYWNlcyA8aW1nPiBlbGVtZW50IHdpdGggb3VyIHRlbXAgZWxlbWVudFxudmFyIHJlcGxhY2VFbGVtZW50ID0gZnVuY3Rpb24gcmVwbGFjZUVsZW1lbnQob2xkRWwsIG5ld0VsKSB7XG4gICAgaWYgKCEobmV3RWwgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgcGFyZW50Tm9kZSA9IG9sZEVsLnBhcmVudE5vZGU7XG4gICAgcmV0dXJuIHBhcmVudE5vZGUucmVwbGFjZUNoaWxkKG5ld0VsLCBvbGRFbCk7XG59O1xudmFyIGVsZW1lbnRUb0Jsb2NrU3BlY0VsZW1lbnQgPSBjb21wb3NlKHdyYXBCbG9ja1NwZWMsIGdldEJsb2NrU3BlY0ZvckVsZW1lbnQpO1xudmFyIGltZ1JlcGxhY2VyID0gZnVuY3Rpb24gaW1nUmVwbGFjZXIoaW1nRWxlbWVudCkge1xuICAgIHJldHVybiByZXBsYWNlRWxlbWVudChpbWdFbGVtZW50LCBlbGVtZW50VG9CbG9ja1NwZWNFbGVtZW50KGltZ0VsZW1lbnQpKTtcbn07XG4vLyBjcmVhdGVzIENvbnRlbnRCbG9jayBiYXNlZCBvbiBwcm92aWRlZCBzcGVjXG52YXIgY3JlYXRlQ29udGVudEJsb2NrID0gZnVuY3Rpb24gY3JlYXRlQ29udGVudEJsb2NrKGJsb2NrRGF0YSwgY29udGVudFN0YXRlKSB7XG4gICAgdmFyIGtleSA9IGJsb2NrRGF0YS5rZXksXG4gICAgICAgIHR5cGUgPSBibG9ja0RhdGEudHlwZSxcbiAgICAgICAgdGV4dCA9IGJsb2NrRGF0YS50ZXh0LFxuICAgICAgICBkYXRhID0gYmxvY2tEYXRhLmRhdGEsXG4gICAgICAgIGlubGluZVN0eWxlcyA9IGJsb2NrRGF0YS5pbmxpbmVTdHlsZXMsXG4gICAgICAgIGVudGl0eURhdGEgPSBibG9ja0RhdGEuZW50aXR5RGF0YTtcblxuICAgIHZhciBibG9ja1NwZWMgPSB7XG4gICAgICAgIHR5cGU6IHR5cGUgIT0gbnVsbCA/IHR5cGUgOiAndW5zdHlsZWQnLFxuICAgICAgICB0ZXh0OiB0ZXh0ICE9IG51bGwgPyB0ZXh0IDogJycsXG4gICAgICAgIGtleToga2V5ICE9IG51bGwgPyBrZXkgOiBnZW5LZXkoKSxcbiAgICAgICAgZGF0YTogbnVsbCxcbiAgICAgICAgY2hhcmFjdGVyTGlzdDogTGlzdChbXSlcbiAgICB9O1xuICAgIGlmIChkYXRhKSB7XG4gICAgICAgIGJsb2NrU3BlYy5kYXRhID0gZnJvbUpTKGRhdGEpO1xuICAgIH1cbiAgICBpZiAoaW5saW5lU3R5bGVzIHx8IGVudGl0eURhdGEpIHtcbiAgICAgICAgdmFyIGVudGl0eUtleSA9IHZvaWQgMDtcbiAgICAgICAgaWYgKGVudGl0eURhdGEpIHtcbiAgICAgICAgICAgIHZhciBfdHlwZSA9IGVudGl0eURhdGEudHlwZSxcbiAgICAgICAgICAgICAgICBtdXRhYmlsaXR5ID0gZW50aXR5RGF0YS5tdXRhYmlsaXR5LFxuICAgICAgICAgICAgICAgIF9kYXRhID0gZW50aXR5RGF0YS5kYXRhO1xuXG4gICAgICAgICAgICBjb250ZW50U3RhdGUuY3JlYXRlRW50aXR5KF90eXBlLCBtdXRhYmlsaXR5LCBfZGF0YSk7XG4gICAgICAgICAgICBlbnRpdHlLZXkgPSBjb250ZW50U3RhdGUuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGVudGl0eUtleSA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHN0eWxlID0gT3JkZXJlZFNldChpbmxpbmVTdHlsZXMgfHwgW10pO1xuICAgICAgICB2YXIgY2hhckRhdGEgPSBDaGFyYWN0ZXJNZXRhZGF0YS5jcmVhdGUoeyBzdHlsZTogc3R5bGUsIGVudGl0eUtleTogZW50aXR5S2V5IH0pO1xuICAgICAgICBibG9ja1NwZWMuY2hhcmFjdGVyTGlzdCA9IExpc3QoUmVwZWF0KGNoYXJEYXRhLCB0ZXh0Lmxlbmd0aCkpO1xuICAgIH1cbiAgICByZXR1cm4gbmV3IENvbnRlbnRCbG9jayhibG9ja1NwZWMpO1xufTtcbi8vIHRha2VzIEhUTUwgc3RyaW5nIGFuZCByZXR1cm5zIERyYWZ0SlMgQ29udGVudFN0YXRlXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBjdXN0b21IVE1MMkNvbnRlbnQoSFRNTCwgY29udGVudFN0YXRlKSB7XG4gICAgdmFyIHRlbXBEb2MgPSBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKEhUTUwsICd0ZXh0L2h0bWwnKTtcbiAgICAvLyByZXBsYWNlIGFsbCA8aW1nIC8+IHdpdGggPGJsb2NrcXVvdGUgLz4gZWxlbWVudHNcbiAgICB0b0FycmF5KHRlbXBEb2MucXVlcnlTZWxlY3RvckFsbCgnaW1nJykpLmZvckVhY2goaW1nUmVwbGFjZXIpO1xuICAgIC8vIHVzZSBEcmFmdEpTIGNvbnZlcnRlciB0byBkbyBpbml0aWFsIGNvbnZlcnNpb24uIEkgZG9uJ3QgcHJvdmlkZSBET01CdWlsZGVyIGFuZFxuICAgIC8vIGJsb2NrUmVuZGVyTWFwIGFyZ3VtZW50cyBoZXJlIHNpbmNlIGl0IHNob3VsZCBmYWxsIGJhY2sgdG8gaXRzIGRlZmF1bHQgb25lcywgd2hpY2ggYXJlIGZpbmVcblxuICAgIHZhciBfY29udmVydEZyb21IVE1MID0gY29udmVydEZyb21IVE1MKHRlbXBEb2MuYm9keS5pbm5lckhUTUwpLFxuICAgICAgICBjb250ZW50QmxvY2tzID0gX2NvbnZlcnRGcm9tSFRNTC5jb250ZW50QmxvY2tzO1xuICAgIC8vIG5vdyByZXBsYWNlIDxibG9ja3F1b3RlIC8+IENvbnRlbnRCbG9ja3Mgd2l0aCAnYXRvbWljJyBvbmVzXG5cblxuICAgIGNvbnRlbnRCbG9ja3MgPSBjb250ZW50QmxvY2tzLnJlZHVjZShmdW5jdGlvbiAoY29udGVudEJsb2NrcywgYmxvY2spIHtcbiAgICAgICAgaWYgKGJsb2NrLmdldFR5cGUoKSAhPT0gJ2Jsb2NrcXVvdGUnKSB7XG4gICAgICAgICAgICByZXR1cm4gY29udGVudEJsb2Nrcy5jb25jYXQoYmxvY2spO1xuICAgICAgICB9XG4gICAgICAgIHZhciBpbWFnZSA9IEpTT04ucGFyc2UoYmxvY2suZ2V0VGV4dCgpKTtcbiAgICAgICAgY29udGVudFN0YXRlLmNyZWF0ZUVudGl0eSgnSU1BR0UtRU5USVRZJywgJ0lNTVVUQUJMRScsIGltYWdlKTtcbiAgICAgICAgdmFyIGVudGl0eUtleSA9IGNvbnRlbnRTdGF0ZS5nZXRMYXN0Q3JlYXRlZEVudGl0eUtleSgpO1xuICAgICAgICB2YXIgY2hhckRhdGEgPSBDaGFyYWN0ZXJNZXRhZGF0YS5jcmVhdGUoeyBlbnRpdHk6IGVudGl0eUtleSB9KTtcbiAgICAgICAgLy8gY29uc3QgYmxvY2tTcGVjID0gT2JqZWN0LmFzc2lnbih7IHR5cGU6ICdhdG9taWMnLCB0ZXh0OiAnICcgfSwgeyBlbnRpdHlEYXRhIH0pXG4gICAgICAgIC8vIGNvbnN0IGF0b21pY0Jsb2NrID0gY3JlYXRlQ29udGVudEJsb2NrKGJsb2NrU3BlYylcbiAgICAgICAgLy8gY29uc3Qgc3BhY2VyQmxvY2sgPSBjcmVhdGVDb250ZW50QmxvY2soe30pO1xuICAgICAgICB2YXIgZnJhZ21lbnRBcnJheSA9IFtuZXcgQ29udGVudEJsb2NrKHtcbiAgICAgICAgICAgIGtleTogZ2VuS2V5KCksXG4gICAgICAgICAgICB0eXBlOiAnaW1hZ2UtYmxvY2snLFxuICAgICAgICAgICAgdGV4dDogJyAnLFxuICAgICAgICAgICAgY2hhcmFjdGVyTGlzdDogTGlzdChSZXBlYXQoY2hhckRhdGEsIGNoYXJEYXRhLmNvdW50KCkpKVxuICAgICAgICB9KSwgbmV3IENvbnRlbnRCbG9jayh7XG4gICAgICAgICAgICBrZXk6IGdlbktleSgpLFxuICAgICAgICAgICAgdHlwZTogJ3Vuc3R5bGVkJyxcbiAgICAgICAgICAgIHRleHQ6ICcnLFxuICAgICAgICAgICAgY2hhcmFjdGVyTGlzdDogTGlzdCgpXG4gICAgICAgIH0pXTtcbiAgICAgICAgcmV0dXJuIGNvbnRlbnRCbG9ja3MuY29uY2F0KGZyYWdtZW50QXJyYXkpO1xuICAgIH0sIFtdKTtcbiAgICAvLyBjb25zb2xlLmxvZygnPj4gY3VzdG9tSFRNTDJDb250ZW50IGNvbnRlbnRCbG9ja3MnLCBjb250ZW50QmxvY2tzKTtcbiAgICB0ZW1wRG9jID0gbnVsbDtcbiAgICByZXR1cm4gQmxvY2tNYXBCdWlsZGVyLmNyZWF0ZUZyb21BcnJheShjb250ZW50QmxvY2tzKTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-core/es/EditorCore/customHTML2Content.js
