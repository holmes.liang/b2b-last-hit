__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "list", function() { return list; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash/cloneDeep */ "./node_modules/lodash/cloneDeep.js");
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _quote_entry__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../quote-entry */ "./src/app/desk/quote/quote-entry.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _suspended_step__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../suspended-step */ "./src/app/desk/quote/suspended-step.tsx");
/* harmony import */ var _quote__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./quote */ "./src/app/desk/quote/SAIC/iar/quote.tsx");
/* harmony import */ var _business__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./business */ "./src/app/desk/quote/SAIC/iar/business.tsx");
/* harmony import */ var _location__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./location */ "./src/app/desk/quote/SAIC/iar/location.tsx");
/* harmony import */ var _add_Location__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./add-Location */ "./src/app/desk/quote/SAIC/iar/add-Location.tsx");
/* harmony import */ var _clause__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./clause */ "./src/app/desk/quote/SAIC/iar/clause.tsx");
/* harmony import */ var _attachment__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./attachment */ "./src/app/desk/quote/SAIC/iar/attachment.tsx");
/* harmony import */ var _submit__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./submit */ "./src/app/desk/quote/SAIC/iar/submit.tsx");
/* harmony import */ var _issue__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./issue */ "./src/app/desk/quote/SAIC/iar/issue.tsx");



















var stepComponents = [{
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].QUOTE,
  component: _quote__WEBPACK_IMPORTED_MODULE_11__["Quote"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].BUSINESS,
  component: _business__WEBPACK_IMPORTED_MODULE_12__["Business"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].LOCATION,
  component: _location__WEBPACK_IMPORTED_MODULE_13__["Location"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].ADDLOCATION,
  component: _add_Location__WEBPACK_IMPORTED_MODULE_14__["AddLocation"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].CLAUSE,
  component: _clause__WEBPACK_IMPORTED_MODULE_15__["Clause"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].ATTACHMENTS,
  component: _attachment__WEBPACK_IMPORTED_MODULE_16__["Attachments"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].SUBMIT,
  component: _submit__WEBPACK_IMPORTED_MODULE_17__["Submit"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].ISSUE,
  component: _issue__WEBPACK_IMPORTED_MODULE_18__["Issue"]
}, {
  stepName: _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].SUSPENDED,
  component: _suspended_step__WEBPACK_IMPORTED_MODULE_10__["SuspendedStep"]
}];
var list = ["Basic", "Business", "Locations", "Clauses", "Attachments", "Submit"];

var Life =
/*#__PURE__*/
function (_QuoteEntry) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(Life, _QuoteEntry);

  function Life() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Life);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(Life).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Life, [{
    key: "initPolicy",
    value: function initPolicy() {
      return _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied({
        productCode: "IAR"
      });
    }
  }, {
    key: "getStepComponents",
    value: function getStepComponents() {
      return stepComponents;
    }
  }, {
    key: "mergePolicyToUIModel",
    value: function mergePolicyToUIModel(newPolicy) {
      this.policy = _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied(newPolicy);
      this.forceUpdate();
    }
  }, {
    key: "mergePolicyToServiceModel",
    value: function mergePolicyToServiceModel() {
      var newPolicy = _model__WEBPACK_IMPORTED_MODULE_9__["Modeller"].asProxied(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_5___default()(this.policy));
      return newPolicy;
    }
  }, {
    key: "getCurrentStep",
    value: function getCurrentStep() {
      var params = new URLSearchParams(window.location.search);
      var clonePolicyId = params.get("clonePolicyId");

      if (clonePolicyId) {
        return _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].QUOTE;
      }

      var resultStep = params.get("step");

      if (resultStep === "details") {
        return _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].SUBMIT;
      } else {
        return this.policy["policy.ext._ui.step"] || _all_steps__WEBPACK_IMPORTED_MODULE_7__["AllSteps"].QUOTE;
      }
    }
  }]);

  return Life;
}(_quote_entry__WEBPACK_IMPORTED_MODULE_8__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_6__["Form"].create()(Life));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgY2xvbmVEZWVwIGZyb20gXCJsb2Rhc2gvY2xvbmVEZWVwXCI7XG5pbXBvcnQgeyBGb3JtIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IFF1b3RlRW50cnksIHsgUXVvdGVFbnRyeVByb3BzLCBRdW90ZUVudHJ5U3RhdGUsIFN0ZXBDb21wb25lbnQgfSBmcm9tIFwiLi4vLi4vcXVvdGUtZW50cnlcIjtcbmltcG9ydCB7IFBhZ2VDb21wb25lbnRzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxsZXIgfSBmcm9tIFwiQG1vZGVsXCI7XG5pbXBvcnQgeyBTdXNwZW5kZWRTdGVwIH0gZnJvbSBcIi4uLy4uL3N1c3BlbmRlZC1zdGVwXCI7XG5pbXBvcnQgeyBRdW90ZSB9IGZyb20gXCIuL3F1b3RlXCI7XG5pbXBvcnQgeyBCdXNpbmVzcyB9IGZyb20gXCIuL2J1c2luZXNzXCI7XG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gXCIuL2xvY2F0aW9uXCI7XG5pbXBvcnQgeyBBZGRMb2NhdGlvbiB9IGZyb20gXCIuL2FkZC1Mb2NhdGlvblwiO1xuaW1wb3J0IHsgQ2xhdXNlIH0gZnJvbSBcIi4vY2xhdXNlXCI7XG5pbXBvcnQgeyBBdHRhY2htZW50cyB9IGZyb20gXCIuL2F0dGFjaG1lbnRcIjtcbmltcG9ydCB7IFN1Ym1pdCB9IGZyb20gXCIuL3N1Ym1pdFwiO1xuaW1wb3J0IHsgSXNzdWUgfSBmcm9tIFwiLi9pc3N1ZVwiO1xuXG5jb25zdCBzdGVwQ29tcG9uZW50czogU3RlcENvbXBvbmVudFtdID0gW1xuICB7IHN0ZXBOYW1lOiBBbGxTdGVwcy5RVU9URSwgY29tcG9uZW50OiBRdW90ZSB9LFxuICB7IHN0ZXBOYW1lOiBBbGxTdGVwcy5CVVNJTkVTUywgY29tcG9uZW50OiBCdXNpbmVzcyB9LFxuICB7IHN0ZXBOYW1lOiBBbGxTdGVwcy5MT0NBVElPTiwgY29tcG9uZW50OiBMb2NhdGlvbiB9LFxuICB7IHN0ZXBOYW1lOiBBbGxTdGVwcy5BRERMT0NBVElPTiwgY29tcG9uZW50OiBBZGRMb2NhdGlvbiB9LFxuICB7IHN0ZXBOYW1lOiBBbGxTdGVwcy5DTEFVU0UsIGNvbXBvbmVudDogQ2xhdXNlIH0sXG4gIHsgc3RlcE5hbWU6IEFsbFN0ZXBzLkFUVEFDSE1FTlRTLCBjb21wb25lbnQ6IEF0dGFjaG1lbnRzIH0sXG4gIHsgc3RlcE5hbWU6IEFsbFN0ZXBzLlNVQk1JVCwgY29tcG9uZW50OiBTdWJtaXQgfSxcbiAgeyBzdGVwTmFtZTogQWxsU3RlcHMuSVNTVUUsIGNvbXBvbmVudDogSXNzdWUgfSxcbiAgeyBzdGVwTmFtZTogQWxsU3RlcHMuU1VTUEVOREVELCBjb21wb25lbnQ6IFN1c3BlbmRlZFN0ZXAgfSxcbl07XG5cbmV4cG9ydCB0eXBlIElQcm9wcyA9IHt9ICYgUXVvdGVFbnRyeVByb3BzO1xuZXhwb3J0IHR5cGUgSVN0YXRlID0ge30gJiBRdW90ZUVudHJ5U3RhdGU7XG5leHBvcnQgY29uc3QgbGlzdCA9IFtcbiAgXCJCYXNpY1wiLFxuICBcIkJ1c2luZXNzXCIsXG4gIFwiTG9jYXRpb25zXCIsXG4gIFwiQ2xhdXNlc1wiLFxuICBcIkF0dGFjaG1lbnRzXCIsXG4gIFwiU3VibWl0XCIsXG5dO1xuXG5jbGFzcyBMaWZlPFAgZXh0ZW5kcyBJUHJvcHMsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBRdW90ZUVudHJ5PFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRQb2xpY3koKTogYW55IHtcbiAgICByZXR1cm4gTW9kZWxsZXIuYXNQcm94aWVkKHsgcHJvZHVjdENvZGU6IFwiSUFSXCIgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0U3RlcENvbXBvbmVudHMoKTogU3RlcENvbXBvbmVudFtdIHtcbiAgICByZXR1cm4gc3RlcENvbXBvbmVudHM7XG4gIH1cblxuICBwcm90ZWN0ZWQgbWVyZ2VQb2xpY3lUb1VJTW9kZWwobmV3UG9saWN5OiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLnBvbGljeSA9IE1vZGVsbGVyLmFzUHJveGllZChuZXdQb2xpY3kpO1xuICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBtZXJnZVBvbGljeVRvU2VydmljZU1vZGVsKCk6IGFueSB7XG4gICAgY29uc3QgbmV3UG9saWN5ID0gTW9kZWxsZXIuYXNQcm94aWVkKGNsb25lRGVlcCh0aGlzLnBvbGljeSkpO1xuICAgIHJldHVybiBuZXdQb2xpY3k7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0Q3VycmVudFN0ZXAoKTogc3RyaW5nIHtcbiAgICBsZXQgcGFyYW1zID0gbmV3IFVSTFNlYXJjaFBhcmFtcyh3aW5kb3cubG9jYXRpb24uc2VhcmNoKTtcbiAgICBsZXQgY2xvbmVQb2xpY3lJZCA9IHBhcmFtcy5nZXQoXCJjbG9uZVBvbGljeUlkXCIpO1xuICAgIGlmIChjbG9uZVBvbGljeUlkKSB7XG4gICAgICByZXR1cm4gQWxsU3RlcHMuUVVPVEU7XG4gICAgfVxuICAgIGxldCByZXN1bHRTdGVwID0gcGFyYW1zLmdldChcInN0ZXBcIik7XG4gICAgaWYgKHJlc3VsdFN0ZXAgPT09IFwiZGV0YWlsc1wiKSB7XG4gICAgICByZXR1cm4gQWxsU3RlcHMuU1VCTUlUO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGhpcy5wb2xpY3lbXCJwb2xpY3kuZXh0Ll91aS5zdGVwXCJdIHx8IEFsbFN0ZXBzLlFVT1RFO1xuICAgIH1cbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBGb3JtLmNyZWF0ZSgpKExpZmUpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBUUE7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUEvQkE7QUFDQTtBQWlDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/index.tsx
