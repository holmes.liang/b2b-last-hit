
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

var getElementRect = __webpack_require__(/*! ./getElementRect */ "./node_modules/fbjs/lib/getElementRect.js");
/**
 * Gets an element's position in pixels relative to the viewport. The returned
 * object represents the position of the element's top left corner.
 *
 * @param {DOMElement} element
 * @return {object}
 */


function getElementPosition(element) {
  var rect = getElementRect(element);
  return {
    x: rect.left,
    y: rect.top,
    width: rect.right - rect.left,
    height: rect.bottom - rect.top
  };
}

module.exports = getElementPosition;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0RWxlbWVudFBvc2l0aW9uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0RWxlbWVudFBvc2l0aW9uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqXG4gKiBAdHlwZWNoZWNrc1xuICovXG5cbnZhciBnZXRFbGVtZW50UmVjdCA9IHJlcXVpcmUoJy4vZ2V0RWxlbWVudFJlY3QnKTtcblxuLyoqXG4gKiBHZXRzIGFuIGVsZW1lbnQncyBwb3NpdGlvbiBpbiBwaXhlbHMgcmVsYXRpdmUgdG8gdGhlIHZpZXdwb3J0LiBUaGUgcmV0dXJuZWRcbiAqIG9iamVjdCByZXByZXNlbnRzIHRoZSBwb3NpdGlvbiBvZiB0aGUgZWxlbWVudCdzIHRvcCBsZWZ0IGNvcm5lci5cbiAqXG4gKiBAcGFyYW0ge0RPTUVsZW1lbnR9IGVsZW1lbnRcbiAqIEByZXR1cm4ge29iamVjdH1cbiAqL1xuZnVuY3Rpb24gZ2V0RWxlbWVudFBvc2l0aW9uKGVsZW1lbnQpIHtcbiAgdmFyIHJlY3QgPSBnZXRFbGVtZW50UmVjdChlbGVtZW50KTtcbiAgcmV0dXJuIHtcbiAgICB4OiByZWN0LmxlZnQsXG4gICAgeTogcmVjdC50b3AsXG4gICAgd2lkdGg6IHJlY3QucmlnaHQgLSByZWN0LmxlZnQsXG4gICAgaGVpZ2h0OiByZWN0LmJvdHRvbSAtIHJlY3QudG9wXG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0RWxlbWVudFBvc2l0aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFFQTs7Ozs7Ozs7O0FBU0E7QUFFQTs7Ozs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getElementPosition.js
