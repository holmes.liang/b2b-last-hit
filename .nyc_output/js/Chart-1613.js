/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;

var Group = __webpack_require__(/*! zrender/lib/container/Group */ "./node_modules/zrender/lib/container/Group.js");

var componentUtil = __webpack_require__(/*! ../util/component */ "./node_modules/echarts/lib/util/component.js");

var clazzUtil = __webpack_require__(/*! ../util/clazz */ "./node_modules/echarts/lib/util/clazz.js");

var modelUtil = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var _task = __webpack_require__(/*! ../stream/task */ "./node_modules/echarts/lib/stream/task.js");

var createTask = _task.createTask;

var createRenderPlanner = __webpack_require__(/*! ../chart/helper/createRenderPlanner */ "./node_modules/echarts/lib/chart/helper/createRenderPlanner.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var inner = modelUtil.makeInner();
var renderPlanner = createRenderPlanner();

function Chart() {
  /**
   * @type {module:zrender/container/Group}
   * @readOnly
   */
  this.group = new Group();
  /**
   * @type {string}
   * @readOnly
   */

  this.uid = componentUtil.getUID('viewChart');
  this.renderTask = createTask({
    plan: renderTaskPlan,
    reset: renderTaskReset
  });
  this.renderTask.context = {
    view: this
  };
}

Chart.prototype = {
  type: 'chart',

  /**
   * Init the chart.
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   */
  init: function init(ecModel, api) {},

  /**
   * Render the chart.
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   */
  render: function render(seriesModel, ecModel, api, payload) {},

  /**
   * Highlight series or specified data item.
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   */
  highlight: function highlight(seriesModel, ecModel, api, payload) {
    toggleHighlight(seriesModel.getData(), payload, 'emphasis');
  },

  /**
   * Downplay series or specified data item.
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   */
  downplay: function downplay(seriesModel, ecModel, api, payload) {
    toggleHighlight(seriesModel.getData(), payload, 'normal');
  },

  /**
   * Remove self.
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   */
  remove: function remove(ecModel, api) {
    this.group.removeAll();
  },

  /**
   * Dispose self.
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   */
  dispose: function dispose() {},

  /**
   * Rendering preparation in progressive mode.
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   */
  incrementalPrepareRender: null,

  /**
   * Render in progressive mode.
   * @param  {Object} params See taskParams in `stream/task.js`
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   */
  incrementalRender: null,

  /**
   * Update transform directly.
   * @param  {module:echarts/model/Series} seriesModel
   * @param  {module:echarts/model/Global} ecModel
   * @param  {module:echarts/ExtensionAPI} api
   * @param  {Object} payload
   * @return {Object} {update: true}
   */
  updateTransform: null,

  /**
   * The view contains the given point.
   * @interface
   * @param {Array.<number>} point
   * @return {boolean}
   */
  // containPoint: function () {}

  /**
   * @param {string} eventType
   * @param {Object} query
   * @param {module:zrender/Element} targetEl
   * @param {Object} packedEvent
   * @return {boolen} Pass only when return `true`.
   */
  filterForExposedEvent: null
};
var chartProto = Chart.prototype;

chartProto.updateView = chartProto.updateLayout = chartProto.updateVisual = function (seriesModel, ecModel, api, payload) {
  this.render(seriesModel, ecModel, api, payload);
};
/**
 * Set state of single element
 * @param  {module:zrender/Element} el
 * @param  {string} state
 */


function elSetState(el, state) {
  if (el) {
    el.trigger(state);

    if (el.type === 'group') {
      for (var i = 0; i < el.childCount(); i++) {
        elSetState(el.childAt(i), state);
      }
    }
  }
}
/**
 * @param  {module:echarts/data/List} data
 * @param  {Object} payload
 * @param  {string} state 'normal'|'emphasis'
 */


function toggleHighlight(data, payload, state) {
  var dataIndex = modelUtil.queryDataIndex(data, payload);

  if (dataIndex != null) {
    each(modelUtil.normalizeToArray(dataIndex), function (dataIdx) {
      elSetState(data.getItemGraphicEl(dataIdx), state);
    });
  } else {
    data.eachItemGraphicEl(function (el) {
      elSetState(el, state);
    });
  }
} // Enable Chart.extend.


clazzUtil.enableClassExtend(Chart, ['dispose']); // Add capability of registerClass, getClass, hasClass, registerSubTypeDefaulter and so on.

clazzUtil.enableClassManagement(Chart, {
  registerWhenExtend: true
});

Chart.markUpdateMethod = function (payload, methodName) {
  inner(payload).updateMethod = methodName;
};

function renderTaskPlan(context) {
  return renderPlanner(context.model);
}

function renderTaskReset(context) {
  var seriesModel = context.model;
  var ecModel = context.ecModel;
  var api = context.api;
  var payload = context.payload; // ???! remove updateView updateVisual

  var progressiveRender = seriesModel.pipelineContext.progressiveRender;
  var view = context.view;
  var updateMethod = payload && inner(payload).updateMethod;
  var methodName = progressiveRender ? 'incrementalPrepareRender' : updateMethod && view[updateMethod] ? updateMethod // `appendData` is also supported when data amount
  // is less than progressive threshold.
  : 'render';

  if (methodName !== 'render') {
    view[methodName](seriesModel, ecModel, api, payload);
  }

  return progressMethodMap[methodName];
}

var progressMethodMap = {
  incrementalPrepareRender: {
    progress: function progress(params, context) {
      context.view.incrementalRender(params, context.model, context.ecModel, context.api, context.payload);
    }
  },
  render: {
    // Put view.render in `progress` to support appendData. But in this case
    // view.render should not be called in reset, otherwise it will be called
    // twise. Use `forceFirstProgress` to make sure that view.render is called
    // in any cases.
    forceFirstProgress: true,
    progress: function progress(params, context) {
      context.view.render(context.model, context.ecModel, context.api, context.payload);
    }
  }
};
var _default = Chart;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlldy9DaGFydC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3ZpZXcvQ2hhcnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBlYWNoID0gX3V0aWwuZWFjaDtcblxudmFyIEdyb3VwID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvbnRhaW5lci9Hcm91cFwiKTtcblxudmFyIGNvbXBvbmVudFV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9jb21wb25lbnRcIik7XG5cbnZhciBjbGF6elV0aWwgPSByZXF1aXJlKFwiLi4vdXRpbC9jbGF6elwiKTtcblxudmFyIG1vZGVsVXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL21vZGVsXCIpO1xuXG52YXIgX3Rhc2sgPSByZXF1aXJlKFwiLi4vc3RyZWFtL3Rhc2tcIik7XG5cbnZhciBjcmVhdGVUYXNrID0gX3Rhc2suY3JlYXRlVGFzaztcblxudmFyIGNyZWF0ZVJlbmRlclBsYW5uZXIgPSByZXF1aXJlKFwiLi4vY2hhcnQvaGVscGVyL2NyZWF0ZVJlbmRlclBsYW5uZXJcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBpbm5lciA9IG1vZGVsVXRpbC5tYWtlSW5uZXIoKTtcbnZhciByZW5kZXJQbGFubmVyID0gY3JlYXRlUmVuZGVyUGxhbm5lcigpO1xuXG5mdW5jdGlvbiBDaGFydCgpIHtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9XG4gICAqIEByZWFkT25seVxuICAgKi9cbiAgdGhpcy5ncm91cCA9IG5ldyBHcm91cCgpO1xuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuXG4gIHRoaXMudWlkID0gY29tcG9uZW50VXRpbC5nZXRVSUQoJ3ZpZXdDaGFydCcpO1xuICB0aGlzLnJlbmRlclRhc2sgPSBjcmVhdGVUYXNrKHtcbiAgICBwbGFuOiByZW5kZXJUYXNrUGxhbixcbiAgICByZXNldDogcmVuZGVyVGFza1Jlc2V0XG4gIH0pO1xuICB0aGlzLnJlbmRlclRhc2suY29udGV4dCA9IHtcbiAgICB2aWV3OiB0aGlzXG4gIH07XG59XG5cbkNoYXJ0LnByb3RvdHlwZSA9IHtcbiAgdHlwZTogJ2NoYXJ0JyxcblxuICAvKipcbiAgICogSW5pdCB0aGUgY2hhcnQuXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICAgKi9cbiAgaW5pdDogZnVuY3Rpb24gKGVjTW9kZWwsIGFwaSkge30sXG5cbiAgLyoqXG4gICAqIFJlbmRlciB0aGUgY2hhcnQuXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL1Nlcmllc30gc2VyaWVzTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfSBlY01vZGVsXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gICAqIEBwYXJhbSAge09iamVjdH0gcGF5bG9hZFxuICAgKi9cbiAgcmVuZGVyOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkge30sXG5cbiAgLyoqXG4gICAqIEhpZ2hsaWdodCBzZXJpZXMgb3Igc3BlY2lmaWVkIGRhdGEgaXRlbS5cbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfSBzZXJpZXNNb2RlbFxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvRXh0ZW5zaW9uQVBJfSBhcGlcbiAgICogQHBhcmFtICB7T2JqZWN0fSBwYXlsb2FkXG4gICAqL1xuICBoaWdobGlnaHQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7XG4gICAgdG9nZ2xlSGlnaGxpZ2h0KHNlcmllc01vZGVsLmdldERhdGEoKSwgcGF5bG9hZCwgJ2VtcGhhc2lzJyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIERvd25wbGF5IHNlcmllcyBvciBzcGVjaWZpZWQgZGF0YSBpdGVtLlxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXN9IHNlcmllc01vZGVsXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICAgKiBAcGFyYW0gIHtPYmplY3R9IHBheWxvYWRcbiAgICovXG4gIGRvd25wbGF5OiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCkge1xuICAgIHRvZ2dsZUhpZ2hsaWdodChzZXJpZXNNb2RlbC5nZXREYXRhKCksIHBheWxvYWQsICdub3JtYWwnKTtcbiAgfSxcblxuICAvKipcbiAgICogUmVtb3ZlIHNlbGYuXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9FeHRlbnNpb25BUEl9IGFwaVxuICAgKi9cbiAgcmVtb3ZlOiBmdW5jdGlvbiAoZWNNb2RlbCwgYXBpKSB7XG4gICAgdGhpcy5ncm91cC5yZW1vdmVBbGwoKTtcbiAgfSxcblxuICAvKipcbiAgICogRGlzcG9zZSBzZWxmLlxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvRXh0ZW5zaW9uQVBJfSBhcGlcbiAgICovXG4gIGRpc3Bvc2U6IGZ1bmN0aW9uICgpIHt9LFxuXG4gIC8qKlxuICAgKiBSZW5kZXJpbmcgcHJlcGFyYXRpb24gaW4gcHJvZ3Jlc3NpdmUgbW9kZS5cbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfSBzZXJpZXNNb2RlbFxuICAgKiBAcGFyYW0gIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvRXh0ZW5zaW9uQVBJfSBhcGlcbiAgICogQHBhcmFtICB7T2JqZWN0fSBwYXlsb2FkXG4gICAqL1xuICBpbmNyZW1lbnRhbFByZXBhcmVSZW5kZXI6IG51bGwsXG5cbiAgLyoqXG4gICAqIFJlbmRlciBpbiBwcm9ncmVzc2l2ZSBtb2RlLlxuICAgKiBAcGFyYW0gIHtPYmplY3R9IHBhcmFtcyBTZWUgdGFza1BhcmFtcyBpbiBgc3RyZWFtL3Rhc2suanNgXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL1Nlcmllc30gc2VyaWVzTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfSBlY01vZGVsXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gICAqIEBwYXJhbSAge09iamVjdH0gcGF5bG9hZFxuICAgKi9cbiAgaW5jcmVtZW50YWxSZW5kZXI6IG51bGwsXG5cbiAgLyoqXG4gICAqIFVwZGF0ZSB0cmFuc2Zvcm0gZGlyZWN0bHkuXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL21vZGVsL1Nlcmllc30gc2VyaWVzTW9kZWxcbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfSBlY01vZGVsXG4gICAqIEBwYXJhbSAge21vZHVsZTplY2hhcnRzL0V4dGVuc2lvbkFQSX0gYXBpXG4gICAqIEBwYXJhbSAge09iamVjdH0gcGF5bG9hZFxuICAgKiBAcmV0dXJuIHtPYmplY3R9IHt1cGRhdGU6IHRydWV9XG4gICAqL1xuICB1cGRhdGVUcmFuc2Zvcm06IG51bGwsXG5cbiAgLyoqXG4gICAqIFRoZSB2aWV3IGNvbnRhaW5zIHRoZSBnaXZlbiBwb2ludC5cbiAgICogQGludGVyZmFjZVxuICAgKiBAcGFyYW0ge0FycmF5LjxudW1iZXI+fSBwb2ludFxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgLy8gY29udGFpblBvaW50OiBmdW5jdGlvbiAoKSB7fVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gZXZlbnRUeXBlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBxdWVyeVxuICAgKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IHRhcmdldEVsXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBwYWNrZWRFdmVudFxuICAgKiBAcmV0dXJuIHtib29sZW59IFBhc3Mgb25seSB3aGVuIHJldHVybiBgdHJ1ZWAuXG4gICAqL1xuICBmaWx0ZXJGb3JFeHBvc2VkRXZlbnQ6IG51bGxcbn07XG52YXIgY2hhcnRQcm90byA9IENoYXJ0LnByb3RvdHlwZTtcblxuY2hhcnRQcm90by51cGRhdGVWaWV3ID0gY2hhcnRQcm90by51cGRhdGVMYXlvdXQgPSBjaGFydFByb3RvLnVwZGF0ZVZpc3VhbCA9IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpLCBwYXlsb2FkKSB7XG4gIHRoaXMucmVuZGVyKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpO1xufTtcbi8qKlxuICogU2V0IHN0YXRlIG9mIHNpbmdsZSBlbGVtZW50XG4gKiBAcGFyYW0gIHttb2R1bGU6enJlbmRlci9FbGVtZW50fSBlbFxuICogQHBhcmFtICB7c3RyaW5nfSBzdGF0ZVxuICovXG5cblxuZnVuY3Rpb24gZWxTZXRTdGF0ZShlbCwgc3RhdGUpIHtcbiAgaWYgKGVsKSB7XG4gICAgZWwudHJpZ2dlcihzdGF0ZSk7XG5cbiAgICBpZiAoZWwudHlwZSA9PT0gJ2dyb3VwJykge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBlbC5jaGlsZENvdW50KCk7IGkrKykge1xuICAgICAgICBlbFNldFN0YXRlKGVsLmNoaWxkQXQoaSksIHN0YXRlKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fSBkYXRhXG4gKiBAcGFyYW0gIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSAge3N0cmluZ30gc3RhdGUgJ25vcm1hbCd8J2VtcGhhc2lzJ1xuICovXG5cblxuZnVuY3Rpb24gdG9nZ2xlSGlnaGxpZ2h0KGRhdGEsIHBheWxvYWQsIHN0YXRlKSB7XG4gIHZhciBkYXRhSW5kZXggPSBtb2RlbFV0aWwucXVlcnlEYXRhSW5kZXgoZGF0YSwgcGF5bG9hZCk7XG5cbiAgaWYgKGRhdGFJbmRleCAhPSBudWxsKSB7XG4gICAgZWFjaChtb2RlbFV0aWwubm9ybWFsaXplVG9BcnJheShkYXRhSW5kZXgpLCBmdW5jdGlvbiAoZGF0YUlkeCkge1xuICAgICAgZWxTZXRTdGF0ZShkYXRhLmdldEl0ZW1HcmFwaGljRWwoZGF0YUlkeCksIHN0YXRlKTtcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICBkYXRhLmVhY2hJdGVtR3JhcGhpY0VsKGZ1bmN0aW9uIChlbCkge1xuICAgICAgZWxTZXRTdGF0ZShlbCwgc3RhdGUpO1xuICAgIH0pO1xuICB9XG59IC8vIEVuYWJsZSBDaGFydC5leHRlbmQuXG5cblxuY2xhenpVdGlsLmVuYWJsZUNsYXNzRXh0ZW5kKENoYXJ0LCBbJ2Rpc3Bvc2UnXSk7IC8vIEFkZCBjYXBhYmlsaXR5IG9mIHJlZ2lzdGVyQ2xhc3MsIGdldENsYXNzLCBoYXNDbGFzcywgcmVnaXN0ZXJTdWJUeXBlRGVmYXVsdGVyIGFuZCBzbyBvbi5cblxuY2xhenpVdGlsLmVuYWJsZUNsYXNzTWFuYWdlbWVudChDaGFydCwge1xuICByZWdpc3RlcldoZW5FeHRlbmQ6IHRydWVcbn0pO1xuXG5DaGFydC5tYXJrVXBkYXRlTWV0aG9kID0gZnVuY3Rpb24gKHBheWxvYWQsIG1ldGhvZE5hbWUpIHtcbiAgaW5uZXIocGF5bG9hZCkudXBkYXRlTWV0aG9kID0gbWV0aG9kTmFtZTtcbn07XG5cbmZ1bmN0aW9uIHJlbmRlclRhc2tQbGFuKGNvbnRleHQpIHtcbiAgcmV0dXJuIHJlbmRlclBsYW5uZXIoY29udGV4dC5tb2RlbCk7XG59XG5cbmZ1bmN0aW9uIHJlbmRlclRhc2tSZXNldChjb250ZXh0KSB7XG4gIHZhciBzZXJpZXNNb2RlbCA9IGNvbnRleHQubW9kZWw7XG4gIHZhciBlY01vZGVsID0gY29udGV4dC5lY01vZGVsO1xuICB2YXIgYXBpID0gY29udGV4dC5hcGk7XG4gIHZhciBwYXlsb2FkID0gY29udGV4dC5wYXlsb2FkOyAvLyA/Pz8hIHJlbW92ZSB1cGRhdGVWaWV3IHVwZGF0ZVZpc3VhbFxuXG4gIHZhciBwcm9ncmVzc2l2ZVJlbmRlciA9IHNlcmllc01vZGVsLnBpcGVsaW5lQ29udGV4dC5wcm9ncmVzc2l2ZVJlbmRlcjtcbiAgdmFyIHZpZXcgPSBjb250ZXh0LnZpZXc7XG4gIHZhciB1cGRhdGVNZXRob2QgPSBwYXlsb2FkICYmIGlubmVyKHBheWxvYWQpLnVwZGF0ZU1ldGhvZDtcbiAgdmFyIG1ldGhvZE5hbWUgPSBwcm9ncmVzc2l2ZVJlbmRlciA/ICdpbmNyZW1lbnRhbFByZXBhcmVSZW5kZXInIDogdXBkYXRlTWV0aG9kICYmIHZpZXdbdXBkYXRlTWV0aG9kXSA/IHVwZGF0ZU1ldGhvZCAvLyBgYXBwZW5kRGF0YWAgaXMgYWxzbyBzdXBwb3J0ZWQgd2hlbiBkYXRhIGFtb3VudFxuICAvLyBpcyBsZXNzIHRoYW4gcHJvZ3Jlc3NpdmUgdGhyZXNob2xkLlxuICA6ICdyZW5kZXInO1xuXG4gIGlmIChtZXRob2ROYW1lICE9PSAncmVuZGVyJykge1xuICAgIHZpZXdbbWV0aG9kTmFtZV0oc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSwgcGF5bG9hZCk7XG4gIH1cblxuICByZXR1cm4gcHJvZ3Jlc3NNZXRob2RNYXBbbWV0aG9kTmFtZV07XG59XG5cbnZhciBwcm9ncmVzc01ldGhvZE1hcCA9IHtcbiAgaW5jcmVtZW50YWxQcmVwYXJlUmVuZGVyOiB7XG4gICAgcHJvZ3Jlc3M6IGZ1bmN0aW9uIChwYXJhbXMsIGNvbnRleHQpIHtcbiAgICAgIGNvbnRleHQudmlldy5pbmNyZW1lbnRhbFJlbmRlcihwYXJhbXMsIGNvbnRleHQubW9kZWwsIGNvbnRleHQuZWNNb2RlbCwgY29udGV4dC5hcGksIGNvbnRleHQucGF5bG9hZCk7XG4gICAgfVxuICB9LFxuICByZW5kZXI6IHtcbiAgICAvLyBQdXQgdmlldy5yZW5kZXIgaW4gYHByb2dyZXNzYCB0byBzdXBwb3J0IGFwcGVuZERhdGEuIEJ1dCBpbiB0aGlzIGNhc2VcbiAgICAvLyB2aWV3LnJlbmRlciBzaG91bGQgbm90IGJlIGNhbGxlZCBpbiByZXNldCwgb3RoZXJ3aXNlIGl0IHdpbGwgYmUgY2FsbGVkXG4gICAgLy8gdHdpc2UuIFVzZSBgZm9yY2VGaXJzdFByb2dyZXNzYCB0byBtYWtlIHN1cmUgdGhhdCB2aWV3LnJlbmRlciBpcyBjYWxsZWRcbiAgICAvLyBpbiBhbnkgY2FzZXMuXG4gICAgZm9yY2VGaXJzdFByb2dyZXNzOiB0cnVlLFxuICAgIHByb2dyZXNzOiBmdW5jdGlvbiAocGFyYW1zLCBjb250ZXh0KSB7XG4gICAgICBjb250ZXh0LnZpZXcucmVuZGVyKGNvbnRleHQubW9kZWwsIGNvbnRleHQuZWNNb2RlbCwgY29udGV4dC5hcGksIGNvbnRleHQucGF5bG9hZCk7XG4gICAgfVxuICB9XG59O1xudmFyIF9kZWZhdWx0ID0gQ2hhcnQ7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFyR0E7QUF1R0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQU5BO0FBaUJBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/view/Chart.js
