__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_emitter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/emitter */ "./src/common/emitter.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/avatar.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n                vertical-align: middle;\n                margin-right: 5px;\n                display: inline-flex;\n                -ms-flex-align: center;\n                align-items: center;\n                -ms-flex-pack: center;\n                justify-content: center;\n                background-size: cover;\n                font-size: 14px;\n                margin: 0 10px;\n                color: #fff;\n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_12__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

function getValidatUser(user) {
  if (user) {
    user.profile = user.profile || {};
  }

  return user || {};
}

var Avatar =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Avatar, _ModelWidget);

  function Avatar(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Avatar);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Avatar).call(this, props, context));
    _this.eventEmitter = "";
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Avatar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      // 声明一个自定义事件
      // 在组件装载完成以后
      this.eventEmitter = _common_emitter__WEBPACK_IMPORTED_MODULE_11__["default"].addListener("callMe", function (values) {
        _this2.setState({
          src: values.src
        });
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // 卸载异步操作设置状态
      this.setState = function (state, callback) {
        return;
      };
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Avatar: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      var user = getValidatUser(_common__WEBPACK_IMPORTED_MODULE_10__["Storage"].Account.session().get(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].ACCOUNT_KEY));
      user.profile = user.profile || {};
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Avatar.prototype), "initState", this).call(this), {
        src: _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].getServiceLocation("/users/".concat(user.userId, "/avatar/").concat(user.profile.avatarFid))
      });
    }
  }, {
    key: "renderName",
    value: function renderName(user) {
      if (this.props.name) return this.props.name.substring(0, 2).toUpperCase();
      return !user.userName ? "" : user.userName.substring(0, 2).toUpperCase();
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var styled = this.props.styled;
      var user = getValidatUser(_common__WEBPACK_IMPORTED_MODULE_10__["Storage"].Account.session().get(_common__WEBPACK_IMPORTED_MODULE_10__["Consts"].ACCOUNT_KEY));
      user.profile = user.profile || {};
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.Avatar, {
        style: Object.assign({
          backgroundImage: "".concat(!user.profile.avatarFid ? "none" : "url('".concat(this.state.src, "')")),
          width: "30px",
          height: "30px",
          borderRadius: "15px",
          backgroundColor: !user.profile.avatarFid ? COLOR_PRIMARY : "none"
        }, styled),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          display: !user.profile.avatarFid ? "" : "none"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, this.renderName(user)));
    }
  }]);

  return Avatar;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Avatar);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2F2YXRhci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvYXZhdGFyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBBamF4LCBDb25zdHMsIFN0b3JhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IGVtaXR0ZXIgZnJvbSBcIkBjb21tb24vZW1pdHRlclwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzL2luZGV4XCI7XG5cbmNvbnN0IHsgQ09MT1JfUFJJTUFSWSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcblxuZnVuY3Rpb24gZ2V0VmFsaWRhdFVzZXIodXNlcjogYW55KSB7XG4gIGlmICh1c2VyKSB7XG4gICAgdXNlci5wcm9maWxlID0gdXNlci5wcm9maWxlIHx8IHt9O1xuICB9XG5cbiAgcmV0dXJuIHVzZXIgfHwge307XG59XG5cbmV4cG9ydCB0eXBlIEF2YXRhclByb3BzID0ge1xuICBzdHlsZWQ/OiBhbnk7XG4gIG5hbWU/OiBzdHJpbmc7XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcblxuZXhwb3J0IHR5cGUgQXZhdGFyU3RhdGUgPSB7XG4gIHNyYzogYW55O1xufTtcbmV4cG9ydCB0eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBBdmF0YXJDb21wb25lbnRzID0ge1xuICBBdmF0YXI6IFN0eWxlZERJVjtcbn07XG5cbmNsYXNzIEF2YXRhcjxQIGV4dGVuZHMgQXZhdGFyUHJvcHMsIFMgZXh0ZW5kcyBBdmF0YXJTdGF0ZSwgQyBleHRlbmRzIEF2YXRhckNvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogQXZhdGFyUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBldmVudEVtaXR0ZXI6IGFueSA9IFwiXCI7XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgLy8g5aOw5piO5LiA5Liq6Ieq5a6a5LmJ5LqL5Lu2XG4gICAgLy8g5Zyo57uE5Lu26KOF6L295a6M5oiQ5Lul5ZCOXG4gICAgdGhpcy5ldmVudEVtaXR0ZXIgPSBlbWl0dGVyLmFkZExpc3RlbmVyKFwiY2FsbE1lXCIsIHZhbHVlcyA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc3JjOiB2YWx1ZXMuc3JjLFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAvLyDljbjovb3lvILmraXmk43kvZzorr7nva7nirbmgIFcbiAgICB0aGlzLnNldFN0YXRlID0gKHN0YXRlLCBjYWxsYmFjaykgPT4ge1xuICAgICAgcmV0dXJuO1xuICAgIH07XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEF2YXRhcjogU3R5bGVkLmRpdmBcbiAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgICAgICAgICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAtbXMtZmxleC1wYWNrOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgbWFyZ2luOiAwIDEwcHg7XG4gICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgY29uc3QgdXNlciA9IGdldFZhbGlkYXRVc2VyKFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BQ0NPVU5UX0tFWSkpO1xuICAgIHVzZXIucHJvZmlsZSA9IHVzZXIucHJvZmlsZSB8fCB7fTtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge1xuICAgICAgc3JjOiBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihgL3VzZXJzLyR7dXNlci51c2VySWR9L2F2YXRhci8ke3VzZXIucHJvZmlsZS5hdmF0YXJGaWR9YCksXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHJlbmRlck5hbWUodXNlcjogYW55KSB7XG4gICAgaWYgKHRoaXMucHJvcHMubmFtZSkgcmV0dXJuIHRoaXMucHJvcHMubmFtZS5zdWJzdHJpbmcoMCwgMikudG9VcHBlckNhc2UoKTtcbiAgICByZXR1cm4gIXVzZXIudXNlck5hbWUgPyBcIlwiIDogdXNlci51c2VyTmFtZS5zdWJzdHJpbmcoMCwgMikudG9VcHBlckNhc2UoKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBzdHlsZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgdXNlciA9IGdldFZhbGlkYXRVc2VyKFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BQ0NPVU5UX0tFWSkpO1xuICAgIHVzZXIucHJvZmlsZSA9IHVzZXIucHJvZmlsZSB8fCB7fTtcbiAgICByZXR1cm4gKFxuICAgICAgPEMuQXZhdGFyXG4gICAgICAgIHN0eWxlPXtPYmplY3QuYXNzaWduKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGJhY2tncm91bmRJbWFnZTogYCR7IXVzZXIucHJvZmlsZS5hdmF0YXJGaWQgPyBcIm5vbmVcIiA6IGB1cmwoJyR7dGhpcy5zdGF0ZS5zcmN9JylgfWAsXG4gICAgICAgICAgICB3aWR0aDogXCIzMHB4XCIsXG4gICAgICAgICAgICBoZWlnaHQ6IFwiMzBweFwiLFxuICAgICAgICAgICAgYm9yZGVyUmFkaXVzOiBcIjE1cHhcIixcbiAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogIXVzZXIucHJvZmlsZS5hdmF0YXJGaWQgPyBDT0xPUl9QUklNQVJZIDogXCJub25lXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdHlsZWQsXG4gICAgICAgICl9XG4gICAgICA+XG4gICAgICAgIDxzcGFuIHN0eWxlPXt7IGRpc3BsYXk6ICF1c2VyLnByb2ZpbGUuYXZhdGFyRmlkID8gXCJcIiA6IFwibm9uZVwiIH19Pnt0aGlzLnJlbmRlck5hbWUodXNlcil9PC9zcGFuPlxuICAgICAgPC9DLkF2YXRhcj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF2YXRhcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUVBO0FBQ0E7OztBQUdBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFlQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7O0FBNUVBO0FBQ0E7QUE4RUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/avatar.tsx
