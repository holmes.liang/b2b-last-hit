__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/index */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_endoOutputRender__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @desk-component/endoOutputRender */ "./src/app/desk/component/endoOutputRender.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/NPanelShowOutputs.tsx";








var NPanelShowOutputs = function NPanelShowOutputs(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      isShow = _useState2[0],
      changeShow = _useState2[1];

  var handelAfterFetchData = function handelAfterFetchData(data) {
    if (lodash__WEBPACK_IMPORTED_MODULE_2___default.a.isEmpty(data)) changeShow(false);
  };

  var that = props.that;
  var model = props.model;
  var isEndo = props.isEndo;

  if (isShow) {
    return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_3__["NPanel"], Object.assign({}, props, {
      header: _common__WEBPACK_IMPORTED_MODULE_5__["Language"].en("Outputs").thai("เอาท์พุต").getMessage(),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 20
      },
      __self: this
    }), !isEndo ? _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_desk_component_index__WEBPACK_IMPORTED_MODULE_6__["OutputRender"], {
      isECard: props.isECard,
      dataFixed: props.dataFixed,
      isPolicyDoc: props.isPolicyDoc,
      beforeDownload: props.beforeDownload,
      that: that,
      afterUploadPolicyDoc: props.afterUploadPolicyDoc,
      afterGetData: function afterGetData(data) {
        if (that.props.isConfirm || props.autoShow) {
          handelAfterFetchData(data);
        }
      },
      model: model,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 26
      },
      __self: this
    }) : _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_desk_component_endoOutputRender__WEBPACK_IMPORTED_MODULE_7__["default"], {
      isECard: props.isECard,
      dataFixed: props.dataFixed,
      afterGetData: function afterGetData(data) {
        if (that.props.isConfirm || props.autoShow) {
          handelAfterFetchData(data);
        }
      },
      model: model,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38
      },
      __self: this
    }));
  } else return null;
};

/* harmony default export */ __webpack_exports__["default"] = (NPanelShowOutputs);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L05QYW5lbFNob3dPdXRwdXRzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9OUGFuZWxTaG93T3V0cHV0cy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IE5QYW5lbCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE91dHB1dFJlbmRlciB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvaW5kZXhcIjtcbmltcG9ydCBFbmRvT3V0cHV0UmVuZGVyIGZyb20gXCJAZGVzay1jb21wb25lbnQvZW5kb091dHB1dFJlbmRlclwiO1xuXG5jb25zdCBOUGFuZWxTaG93T3V0cHV0cyA9IChwcm9wczogYW55KSA9PiB7XG4gIGNvbnN0IFtpc1Nob3csIGNoYW5nZVNob3ddID0gdXNlU3RhdGUodHJ1ZSk7XG5cbiAgY29uc3QgaGFuZGVsQWZ0ZXJGZXRjaERhdGEgPSAoZGF0YTogYW55KSA9PiB7XG4gICAgaWYgKF8uaXNFbXB0eShkYXRhKSkgY2hhbmdlU2hvdyhmYWxzZSk7XG4gIH07XG5cbiAgY29uc3QgdGhhdDogYW55ID0gcHJvcHMudGhhdDtcbiAgY29uc3QgbW9kZWw6IGFueSA9IHByb3BzLm1vZGVsO1xuICBjb25zdCBpc0VuZG86IGJvb2xlYW4gPSBwcm9wcy5pc0VuZG87XG4gIGlmIChpc1Nob3cpIHtcbiAgICByZXR1cm4gPE5QYW5lbFxuICAgICAgey4uLnByb3BzfVxuICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIk91dHB1dHNcIilcbiAgICAgICAgLnRoYWkoXCLguYDguK3guLLguJfguYzguJ7guLjguJVcIilcbiAgICAgICAgLmdldE1lc3NhZ2UoKX0+XG5cbiAgICAgIHshaXNFbmRvID8gPE91dHB1dFJlbmRlclxuICAgICAgICAgIGlzRUNhcmQ9e3Byb3BzLmlzRUNhcmR9XG4gICAgICAgICAgZGF0YUZpeGVkPXtwcm9wcy5kYXRhRml4ZWR9XG4gICAgICAgICAgaXNQb2xpY3lEb2M9e3Byb3BzLmlzUG9saWN5RG9jfVxuICAgICAgICAgIGJlZm9yZURvd25sb2FkPXtwcm9wcy5iZWZvcmVEb3dubG9hZH1cbiAgICAgICAgICB0aGF0PXt0aGF0fVxuICAgICAgICAgIGFmdGVyVXBsb2FkUG9saWN5RG9jPXtwcm9wcy5hZnRlclVwbG9hZFBvbGljeURvY31cbiAgICAgICAgICBhZnRlckdldERhdGE9eyhkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGlmICh0aGF0LnByb3BzLmlzQ29uZmlybSB8fCBwcm9wcy5hdXRvU2hvdykge1xuICAgICAgICAgICAgICBoYW5kZWxBZnRlckZldGNoRGF0YShkYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9fSBtb2RlbD17bW9kZWx9Lz4gOlxuICAgICAgICA8RW5kb091dHB1dFJlbmRlclxuICAgICAgICAgIGlzRUNhcmQ9e3Byb3BzLmlzRUNhcmR9XG4gICAgICAgICAgZGF0YUZpeGVkPXtwcm9wcy5kYXRhRml4ZWR9XG4gICAgICAgICAgYWZ0ZXJHZXREYXRhPXsoZGF0YTogYW55KSA9PiB7XG4gICAgICAgICAgICBpZiAodGhhdC5wcm9wcy5pc0NvbmZpcm0gfHwgcHJvcHMuYXV0b1Nob3cpIHtcbiAgICAgICAgICAgICAgaGFuZGVsQWZ0ZXJGZXRjaERhdGEoZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfX0gbW9kZWw9e21vZGVsfS8+XG4gICAgICB9XG5cbiAgICA8L05QYW5lbD47XG4gIH0gZWxzZSByZXR1cm4gbnVsbDtcbn07XG5leHBvcnQgZGVmYXVsdCBOUGFuZWxTaG93T3V0cHV0cztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBWEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFDQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/NPanelShowOutputs.tsx
