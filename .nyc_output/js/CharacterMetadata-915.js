/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule CharacterMetadata
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _require = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js"),
    Map = _require.Map,
    OrderedSet = _require.OrderedSet,
    Record = _require.Record; // Immutable.map is typed such that the value for every key in the map
// must be the same type


var EMPTY_SET = OrderedSet();
var defaultRecord = {
  style: EMPTY_SET,
  entity: null
};
var CharacterMetadataRecord = Record(defaultRecord);

var CharacterMetadata = function (_CharacterMetadataRec) {
  _inherits(CharacterMetadata, _CharacterMetadataRec);

  function CharacterMetadata() {
    _classCallCheck(this, CharacterMetadata);

    return _possibleConstructorReturn(this, _CharacterMetadataRec.apply(this, arguments));
  }

  CharacterMetadata.prototype.getStyle = function getStyle() {
    return this.get('style');
  };

  CharacterMetadata.prototype.getEntity = function getEntity() {
    return this.get('entity');
  };

  CharacterMetadata.prototype.hasStyle = function hasStyle(style) {
    return this.getStyle().includes(style);
  };

  CharacterMetadata.applyStyle = function applyStyle(record, style) {
    var withStyle = record.set('style', record.getStyle().add(style));
    return CharacterMetadata.create(withStyle);
  };

  CharacterMetadata.removeStyle = function removeStyle(record, style) {
    var withoutStyle = record.set('style', record.getStyle().remove(style));
    return CharacterMetadata.create(withoutStyle);
  };

  CharacterMetadata.applyEntity = function applyEntity(record, entityKey) {
    var withEntity = record.getEntity() === entityKey ? record : record.set('entity', entityKey);
    return CharacterMetadata.create(withEntity);
  };
  /**
   * Use this function instead of the `CharacterMetadata` constructor.
   * Since most content generally uses only a very small number of
   * style/entity permutations, we can reuse these objects as often as
   * possible.
   */


  CharacterMetadata.create = function create(config) {
    if (!config) {
      return EMPTY;
    }

    var defaultConfig = {
      style: EMPTY_SET,
      entity: null
    }; // Fill in unspecified properties, if necessary.

    var configMap = Map(defaultConfig).merge(config);
    var existing = pool.get(configMap);

    if (existing) {
      return existing;
    }

    var newCharacter = new CharacterMetadata(configMap);
    pool = pool.set(configMap, newCharacter);
    return newCharacter;
  };

  return CharacterMetadata;
}(CharacterMetadataRecord);

var EMPTY = new CharacterMetadata();
var pool = Map([[Map(defaultRecord), EMPTY]]);
CharacterMetadata.EMPTY = EMPTY;
module.exports = CharacterMetadata;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NoYXJhY3Rlck1ldGFkYXRhLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NoYXJhY3Rlck1ldGFkYXRhLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgQ2hhcmFjdGVyTWV0YWRhdGFcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgX3JlcXVpcmUgPSByZXF1aXJlKCdpbW11dGFibGUnKSxcbiAgICBNYXAgPSBfcmVxdWlyZS5NYXAsXG4gICAgT3JkZXJlZFNldCA9IF9yZXF1aXJlLk9yZGVyZWRTZXQsXG4gICAgUmVjb3JkID0gX3JlcXVpcmUuUmVjb3JkO1xuXG4vLyBJbW11dGFibGUubWFwIGlzIHR5cGVkIHN1Y2ggdGhhdCB0aGUgdmFsdWUgZm9yIGV2ZXJ5IGtleSBpbiB0aGUgbWFwXG4vLyBtdXN0IGJlIHRoZSBzYW1lIHR5cGVcblxuXG52YXIgRU1QVFlfU0VUID0gT3JkZXJlZFNldCgpO1xuXG52YXIgZGVmYXVsdFJlY29yZCA9IHtcbiAgc3R5bGU6IEVNUFRZX1NFVCxcbiAgZW50aXR5OiBudWxsXG59O1xuXG52YXIgQ2hhcmFjdGVyTWV0YWRhdGFSZWNvcmQgPSBSZWNvcmQoZGVmYXVsdFJlY29yZCk7XG5cbnZhciBDaGFyYWN0ZXJNZXRhZGF0YSA9IGZ1bmN0aW9uIChfQ2hhcmFjdGVyTWV0YWRhdGFSZWMpIHtcbiAgX2luaGVyaXRzKENoYXJhY3Rlck1ldGFkYXRhLCBfQ2hhcmFjdGVyTWV0YWRhdGFSZWMpO1xuXG4gIGZ1bmN0aW9uIENoYXJhY3Rlck1ldGFkYXRhKCkge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDaGFyYWN0ZXJNZXRhZGF0YSk7XG5cbiAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NoYXJhY3Rlck1ldGFkYXRhUmVjLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgQ2hhcmFjdGVyTWV0YWRhdGEucHJvdG90eXBlLmdldFN0eWxlID0gZnVuY3Rpb24gZ2V0U3R5bGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdzdHlsZScpO1xuICB9O1xuXG4gIENoYXJhY3Rlck1ldGFkYXRhLnByb3RvdHlwZS5nZXRFbnRpdHkgPSBmdW5jdGlvbiBnZXRFbnRpdHkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdlbnRpdHknKTtcbiAgfTtcblxuICBDaGFyYWN0ZXJNZXRhZGF0YS5wcm90b3R5cGUuaGFzU3R5bGUgPSBmdW5jdGlvbiBoYXNTdHlsZShzdHlsZSkge1xuICAgIHJldHVybiB0aGlzLmdldFN0eWxlKCkuaW5jbHVkZXMoc3R5bGUpO1xuICB9O1xuXG4gIENoYXJhY3Rlck1ldGFkYXRhLmFwcGx5U3R5bGUgPSBmdW5jdGlvbiBhcHBseVN0eWxlKHJlY29yZCwgc3R5bGUpIHtcbiAgICB2YXIgd2l0aFN0eWxlID0gcmVjb3JkLnNldCgnc3R5bGUnLCByZWNvcmQuZ2V0U3R5bGUoKS5hZGQoc3R5bGUpKTtcbiAgICByZXR1cm4gQ2hhcmFjdGVyTWV0YWRhdGEuY3JlYXRlKHdpdGhTdHlsZSk7XG4gIH07XG5cbiAgQ2hhcmFjdGVyTWV0YWRhdGEucmVtb3ZlU3R5bGUgPSBmdW5jdGlvbiByZW1vdmVTdHlsZShyZWNvcmQsIHN0eWxlKSB7XG4gICAgdmFyIHdpdGhvdXRTdHlsZSA9IHJlY29yZC5zZXQoJ3N0eWxlJywgcmVjb3JkLmdldFN0eWxlKCkucmVtb3ZlKHN0eWxlKSk7XG4gICAgcmV0dXJuIENoYXJhY3Rlck1ldGFkYXRhLmNyZWF0ZSh3aXRob3V0U3R5bGUpO1xuICB9O1xuXG4gIENoYXJhY3Rlck1ldGFkYXRhLmFwcGx5RW50aXR5ID0gZnVuY3Rpb24gYXBwbHlFbnRpdHkocmVjb3JkLCBlbnRpdHlLZXkpIHtcbiAgICB2YXIgd2l0aEVudGl0eSA9IHJlY29yZC5nZXRFbnRpdHkoKSA9PT0gZW50aXR5S2V5ID8gcmVjb3JkIDogcmVjb3JkLnNldCgnZW50aXR5JywgZW50aXR5S2V5KTtcbiAgICByZXR1cm4gQ2hhcmFjdGVyTWV0YWRhdGEuY3JlYXRlKHdpdGhFbnRpdHkpO1xuICB9O1xuXG4gIC8qKlxuICAgKiBVc2UgdGhpcyBmdW5jdGlvbiBpbnN0ZWFkIG9mIHRoZSBgQ2hhcmFjdGVyTWV0YWRhdGFgIGNvbnN0cnVjdG9yLlxuICAgKiBTaW5jZSBtb3N0IGNvbnRlbnQgZ2VuZXJhbGx5IHVzZXMgb25seSBhIHZlcnkgc21hbGwgbnVtYmVyIG9mXG4gICAqIHN0eWxlL2VudGl0eSBwZXJtdXRhdGlvbnMsIHdlIGNhbiByZXVzZSB0aGVzZSBvYmplY3RzIGFzIG9mdGVuIGFzXG4gICAqIHBvc3NpYmxlLlxuICAgKi9cblxuXG4gIENoYXJhY3Rlck1ldGFkYXRhLmNyZWF0ZSA9IGZ1bmN0aW9uIGNyZWF0ZShjb25maWcpIHtcbiAgICBpZiAoIWNvbmZpZykge1xuICAgICAgcmV0dXJuIEVNUFRZO1xuICAgIH1cblxuICAgIHZhciBkZWZhdWx0Q29uZmlnID0ge1xuICAgICAgc3R5bGU6IEVNUFRZX1NFVCxcbiAgICAgIGVudGl0eTogbnVsbFxuICAgIH07XG5cbiAgICAvLyBGaWxsIGluIHVuc3BlY2lmaWVkIHByb3BlcnRpZXMsIGlmIG5lY2Vzc2FyeS5cbiAgICB2YXIgY29uZmlnTWFwID0gTWFwKGRlZmF1bHRDb25maWcpLm1lcmdlKGNvbmZpZyk7XG5cbiAgICB2YXIgZXhpc3RpbmcgPSBwb29sLmdldChjb25maWdNYXApO1xuICAgIGlmIChleGlzdGluZykge1xuICAgICAgcmV0dXJuIGV4aXN0aW5nO1xuICAgIH1cblxuICAgIHZhciBuZXdDaGFyYWN0ZXIgPSBuZXcgQ2hhcmFjdGVyTWV0YWRhdGEoY29uZmlnTWFwKTtcbiAgICBwb29sID0gcG9vbC5zZXQoY29uZmlnTWFwLCBuZXdDaGFyYWN0ZXIpO1xuICAgIHJldHVybiBuZXdDaGFyYWN0ZXI7XG4gIH07XG5cbiAgcmV0dXJuIENoYXJhY3Rlck1ldGFkYXRhO1xufShDaGFyYWN0ZXJNZXRhZGF0YVJlY29yZCk7XG5cbnZhciBFTVBUWSA9IG5ldyBDaGFyYWN0ZXJNZXRhZGF0YSgpO1xudmFyIHBvb2wgPSBNYXAoW1tNYXAoZGVmYXVsdFJlY29yZCksIEVNUFRZXV0pO1xuXG5DaGFyYWN0ZXJNZXRhZGF0YS5FTVBUWSA9IEVNUFRZO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENoYXJhY3Rlck1ldGFkYXRhOyJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBS0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/CharacterMetadata.js
