__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _standard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./standard */ "./src/styles/themes/standard.tsx");
/* harmony import */ var _antd_base_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./antd/base.json */ "./src/styles/themes/antd/base.json");
var _antd_base_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./antd/base.json */ "./src/styles/themes/antd/base.json", 1);
/* harmony import */ var _antd_income_orange_json__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./antd/income-orange.json */ "./src/styles/themes/antd/income-orange.json");
var _antd_income_orange_json__WEBPACK_IMPORTED_MODULE_3___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./antd/income-orange.json */ "./src/styles/themes/antd/income-orange.json", 1);





var antdConfig = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _antd_base_json__WEBPACK_IMPORTED_MODULE_2__, {}, _antd_income_orange_json__WEBPACK_IMPORTED_MODULE_3__);

/* harmony default export */ __webpack_exports__["default"] = (Object.assign({}, _standard__WEBPACK_IMPORTED_MODULE_1__["default"], {
  COLOR_PRIMARY: antdConfig["primary-color"],
  BACKGROUND_COLOR: antdConfig["primary-color"],
  BORDER_COLOR: antdConfig["primary-color"],
  HEADER_COLOR: antdConfig["primary-color"],
  HIGHLIGHT_COLOR: antdConfig["primary-color"],
  HREF_A: antdConfig["link-color"],
  COLOR_BACKGROUND_BTN: antdConfig["primary-color"],
  COLOR_A: antdConfig["primary-color"],
  UPLOAD_BOARD_COLOR: antdConfig["primary-color"]
}));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc3R5bGVzL3RoZW1lcy9pbmNvbWUtb3JhbmdlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL3N0eWxlcy90aGVtZXMvaW5jb21lLW9yYW5nZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgeyBTdHlsZWRQcm9wc1RoZW1lIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHBhcmVudCBmcm9tIFwiLi9zdGFuZGFyZFwiO1xuaW1wb3J0IEFudGRCYXNlQ29uZmlnIGZyb20gXCIuL2FudGQvYmFzZS5qc29uXCI7XG5pbXBvcnQgQW50ZFRoZW1lQ29uZmlnIGZyb20gXCIuL2FudGQvaW5jb21lLW9yYW5nZS5qc29uXCI7XG5cbmNvbnN0IGFudGRDb25maWcgPSB7XG4gIC4uLkFudGRCYXNlQ29uZmlnLFxuICAuLi5BbnRkVGhlbWVDb25maWcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBPYmplY3QuYXNzaWduKHt9LCBwYXJlbnQsIHtcbiAgQ09MT1JfUFJJTUFSWTogYW50ZENvbmZpZ1tcInByaW1hcnktY29sb3JcIl0sXG4gIEJBQ0tHUk9VTkRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBCT1JERVJfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBIRUFERVJfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBISUdITElHSFRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBIUkVGX0E6IGFudGRDb25maWdbXCJsaW5rLWNvbG9yXCJdLFxuICBDT0xPUl9CQUNLR1JPVU5EX0JUTjogYW50ZENvbmZpZ1tcInByaW1hcnktY29sb3JcIl0sXG4gIENPTE9SX0E6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxuICBVUExPQURfQk9BUkRfQ09MT1I6IGFudGRDb25maWdbXCJwcmltYXJ5LWNvbG9yXCJdLFxufSkgYXMgU3R5bGVkUHJvcHNUaGVtZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/styles/themes/income-orange.tsx
