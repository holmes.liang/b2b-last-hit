/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandUndo
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

function keyCommandUndo(e, editorState, updateFn) {
  var undoneState = EditorState.undo(editorState); // If the last change to occur was a spellcheck change, allow the undo
  // event to fall through to the browser. This allows the browser to record
  // the unwanted change, which should soon lead it to learn not to suggest
  // the correction again.

  if (editorState.getLastChangeType() === 'spellcheck-change') {
    var nativelyRenderedContent = undoneState.getCurrentContent();
    updateFn(EditorState.set(undoneState, {
      nativelyRenderedContent: nativelyRenderedContent
    }));
    return;
  } // Otheriwse, manage the undo behavior manually.


  e.preventDefault();

  if (!editorState.getNativelyRenderedContent()) {
    updateFn(undoneState);
    return;
  } // Trigger a re-render with the current content state to ensure that the
  // component tree has up-to-date props for comparison.


  updateFn(EditorState.set(editorState, {
    nativelyRenderedContent: null
  })); // Wait to ensure that the re-render has occurred before performing
  // the undo action.

  setTimeout(function () {
    updateFn(undoneState);
  }, 0);
}

module.exports = keyCommandUndo;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRVbmRvLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRVbmRvLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUga2V5Q29tbWFuZFVuZG9cbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG5mdW5jdGlvbiBrZXlDb21tYW5kVW5kbyhlLCBlZGl0b3JTdGF0ZSwgdXBkYXRlRm4pIHtcbiAgdmFyIHVuZG9uZVN0YXRlID0gRWRpdG9yU3RhdGUudW5kbyhlZGl0b3JTdGF0ZSk7XG5cbiAgLy8gSWYgdGhlIGxhc3QgY2hhbmdlIHRvIG9jY3VyIHdhcyBhIHNwZWxsY2hlY2sgY2hhbmdlLCBhbGxvdyB0aGUgdW5kb1xuICAvLyBldmVudCB0byBmYWxsIHRocm91Z2ggdG8gdGhlIGJyb3dzZXIuIFRoaXMgYWxsb3dzIHRoZSBicm93c2VyIHRvIHJlY29yZFxuICAvLyB0aGUgdW53YW50ZWQgY2hhbmdlLCB3aGljaCBzaG91bGQgc29vbiBsZWFkIGl0IHRvIGxlYXJuIG5vdCB0byBzdWdnZXN0XG4gIC8vIHRoZSBjb3JyZWN0aW9uIGFnYWluLlxuICBpZiAoZWRpdG9yU3RhdGUuZ2V0TGFzdENoYW5nZVR5cGUoKSA9PT0gJ3NwZWxsY2hlY2stY2hhbmdlJykge1xuICAgIHZhciBuYXRpdmVseVJlbmRlcmVkQ29udGVudCA9IHVuZG9uZVN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdXBkYXRlRm4oRWRpdG9yU3RhdGUuc2V0KHVuZG9uZVN0YXRlLCB7IG5hdGl2ZWx5UmVuZGVyZWRDb250ZW50OiBuYXRpdmVseVJlbmRlcmVkQ29udGVudCB9KSk7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLy8gT3RoZXJpd3NlLCBtYW5hZ2UgdGhlIHVuZG8gYmVoYXZpb3IgbWFudWFsbHkuXG4gIGUucHJldmVudERlZmF1bHQoKTtcbiAgaWYgKCFlZGl0b3JTdGF0ZS5nZXROYXRpdmVseVJlbmRlcmVkQ29udGVudCgpKSB7XG4gICAgdXBkYXRlRm4odW5kb25lU3RhdGUpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8vIFRyaWdnZXIgYSByZS1yZW5kZXIgd2l0aCB0aGUgY3VycmVudCBjb250ZW50IHN0YXRlIHRvIGVuc3VyZSB0aGF0IHRoZVxuICAvLyBjb21wb25lbnQgdHJlZSBoYXMgdXAtdG8tZGF0ZSBwcm9wcyBmb3IgY29tcGFyaXNvbi5cbiAgdXBkYXRlRm4oRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7IG5hdGl2ZWx5UmVuZGVyZWRDb250ZW50OiBudWxsIH0pKTtcblxuICAvLyBXYWl0IHRvIGVuc3VyZSB0aGF0IHRoZSByZS1yZW5kZXIgaGFzIG9jY3VycmVkIGJlZm9yZSBwZXJmb3JtaW5nXG4gIC8vIHRoZSB1bmRvIGFjdGlvbi5cbiAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgdXBkYXRlRm4odW5kb25lU3RhdGUpO1xuICB9LCAwKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBrZXlDb21tYW5kVW5kbzsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandUndo.js
