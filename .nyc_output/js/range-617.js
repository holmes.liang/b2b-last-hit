

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _util = __webpack_require__(/*! ../util */ "./node_modules/async-validator/es/util.js");

var util = _interopRequireWildcard(_util);

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}
/**
 *  Rule for validating minimum and maximum allowed values.
 *
 *  @param rule The validation rule.
 *  @param value The value of the field on the source object.
 *  @param source The source object being validated.
 *  @param errors An array of errors that this rule may add
 *  validation errors to.
 *  @param options The validation options.
 *  @param options.messages The validation messages.
 */


function range(rule, value, source, errors, options) {
  var len = typeof rule.len === 'number';
  var min = typeof rule.min === 'number';
  var max = typeof rule.max === 'number'; // 正则匹配码点范围从U+010000一直到U+10FFFF的文字（补充平面Supplementary Plane）

  var spRegexp = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
  var val = value;
  var key = null;
  var num = typeof value === 'number';
  var str = typeof value === 'string';
  var arr = Array.isArray(value);

  if (num) {
    key = 'number';
  } else if (str) {
    key = 'string';
  } else if (arr) {
    key = 'array';
  } // if the value is not of a supported type for range validation
  // the validation rule rule should use the
  // type property to also test for a particular type


  if (!key) {
    return false;
  }

  if (arr) {
    val = value.length;
  }

  if (str) {
    // 处理码点大于U+010000的文字length属性不准确的bug，如"𠮷𠮷𠮷".lenght !== 3
    val = value.replace(spRegexp, '_').length;
  }

  if (len) {
    if (val !== rule.len) {
      errors.push(util.format(options.messages[key].len, rule.fullField, rule.len));
    }
  } else if (min && !max && val < rule.min) {
    errors.push(util.format(options.messages[key].min, rule.fullField, rule.min));
  } else if (max && !min && val > rule.max) {
    errors.push(util.format(options.messages[key].max, rule.fullField, rule.max));
  } else if (min && max && (val < rule.min || val > rule.max)) {
    errors.push(util.format(options.messages[key].range, rule.fullField, rule.min, rule.max));
  }
}

exports['default'] = range;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYXN5bmMtdmFsaWRhdG9yL2VzL3J1bGUvcmFuZ2UuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9hc3luYy12YWxpZGF0b3IvZXMvcnVsZS9yYW5nZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoJy4uL3V0aWwnKTtcblxudmFyIHV0aWwgPSBfaW50ZXJvcFJlcXVpcmVXaWxkY2FyZChfdXRpbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZVdpbGRjYXJkKG9iaikgeyBpZiAob2JqICYmIG9iai5fX2VzTW9kdWxlKSB7IHJldHVybiBvYmo7IH0gZWxzZSB7IHZhciBuZXdPYmogPSB7fTsgaWYgKG9iaiAhPSBudWxsKSB7IGZvciAodmFyIGtleSBpbiBvYmopIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGtleSkpIG5ld09ialtrZXldID0gb2JqW2tleV07IH0gfSBuZXdPYmpbJ2RlZmF1bHQnXSA9IG9iajsgcmV0dXJuIG5ld09iajsgfSB9XG5cbi8qKlxuICogIFJ1bGUgZm9yIHZhbGlkYXRpbmcgbWluaW11bSBhbmQgbWF4aW11bSBhbGxvd2VkIHZhbHVlcy5cbiAqXG4gKiAgQHBhcmFtIHJ1bGUgVGhlIHZhbGlkYXRpb24gcnVsZS5cbiAqICBAcGFyYW0gdmFsdWUgVGhlIHZhbHVlIG9mIHRoZSBmaWVsZCBvbiB0aGUgc291cmNlIG9iamVjdC5cbiAqICBAcGFyYW0gc291cmNlIFRoZSBzb3VyY2Ugb2JqZWN0IGJlaW5nIHZhbGlkYXRlZC5cbiAqICBAcGFyYW0gZXJyb3JzIEFuIGFycmF5IG9mIGVycm9ycyB0aGF0IHRoaXMgcnVsZSBtYXkgYWRkXG4gKiAgdmFsaWRhdGlvbiBlcnJvcnMgdG8uXG4gKiAgQHBhcmFtIG9wdGlvbnMgVGhlIHZhbGlkYXRpb24gb3B0aW9ucy5cbiAqICBAcGFyYW0gb3B0aW9ucy5tZXNzYWdlcyBUaGUgdmFsaWRhdGlvbiBtZXNzYWdlcy5cbiAqL1xuZnVuY3Rpb24gcmFuZ2UocnVsZSwgdmFsdWUsIHNvdXJjZSwgZXJyb3JzLCBvcHRpb25zKSB7XG4gIHZhciBsZW4gPSB0eXBlb2YgcnVsZS5sZW4gPT09ICdudW1iZXInO1xuICB2YXIgbWluID0gdHlwZW9mIHJ1bGUubWluID09PSAnbnVtYmVyJztcbiAgdmFyIG1heCA9IHR5cGVvZiBydWxlLm1heCA9PT0gJ251bWJlcic7XG4gIC8vIOato+WImeWMuemFjeeggeeCueiMg+WbtOS7jlUrMDEwMDAw5LiA55u05YiwVSsxMEZGRkbnmoTmloflrZfvvIjooaXlhYXlubPpnaJTdXBwbGVtZW50YXJ5IFBsYW5l77yJXG4gIHZhciBzcFJlZ2V4cCA9IC9bXFx1RDgwMC1cXHVEQkZGXVtcXHVEQzAwLVxcdURGRkZdL2c7XG4gIHZhciB2YWwgPSB2YWx1ZTtcbiAgdmFyIGtleSA9IG51bGw7XG4gIHZhciBudW0gPSB0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInO1xuICB2YXIgc3RyID0gdHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJztcbiAgdmFyIGFyciA9IEFycmF5LmlzQXJyYXkodmFsdWUpO1xuICBpZiAobnVtKSB7XG4gICAga2V5ID0gJ251bWJlcic7XG4gIH0gZWxzZSBpZiAoc3RyKSB7XG4gICAga2V5ID0gJ3N0cmluZyc7XG4gIH0gZWxzZSBpZiAoYXJyKSB7XG4gICAga2V5ID0gJ2FycmF5JztcbiAgfVxuICAvLyBpZiB0aGUgdmFsdWUgaXMgbm90IG9mIGEgc3VwcG9ydGVkIHR5cGUgZm9yIHJhbmdlIHZhbGlkYXRpb25cbiAgLy8gdGhlIHZhbGlkYXRpb24gcnVsZSBydWxlIHNob3VsZCB1c2UgdGhlXG4gIC8vIHR5cGUgcHJvcGVydHkgdG8gYWxzbyB0ZXN0IGZvciBhIHBhcnRpY3VsYXIgdHlwZVxuICBpZiAoIWtleSkge1xuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICBpZiAoYXJyKSB7XG4gICAgdmFsID0gdmFsdWUubGVuZ3RoO1xuICB9XG4gIGlmIChzdHIpIHtcbiAgICAvLyDlpITnkIbnoIHngrnlpKfkuo5VKzAxMDAwMOeahOaWh+Wtl2xlbmd0aOWxnuaAp+S4jeWHhuehrueahGJ1Z++8jOWmglwi8KCut/CgrrfwoK63XCIubGVuZ2h0ICE9PSAzXG4gICAgdmFsID0gdmFsdWUucmVwbGFjZShzcFJlZ2V4cCwgJ18nKS5sZW5ndGg7XG4gIH1cbiAgaWYgKGxlbikge1xuICAgIGlmICh2YWwgIT09IHJ1bGUubGVuKSB7XG4gICAgICBlcnJvcnMucHVzaCh1dGlsLmZvcm1hdChvcHRpb25zLm1lc3NhZ2VzW2tleV0ubGVuLCBydWxlLmZ1bGxGaWVsZCwgcnVsZS5sZW4pKTtcbiAgICB9XG4gIH0gZWxzZSBpZiAobWluICYmICFtYXggJiYgdmFsIDwgcnVsZS5taW4pIHtcbiAgICBlcnJvcnMucHVzaCh1dGlsLmZvcm1hdChvcHRpb25zLm1lc3NhZ2VzW2tleV0ubWluLCBydWxlLmZ1bGxGaWVsZCwgcnVsZS5taW4pKTtcbiAgfSBlbHNlIGlmIChtYXggJiYgIW1pbiAmJiB2YWwgPiBydWxlLm1heCkge1xuICAgIGVycm9ycy5wdXNoKHV0aWwuZm9ybWF0KG9wdGlvbnMubWVzc2FnZXNba2V5XS5tYXgsIHJ1bGUuZnVsbEZpZWxkLCBydWxlLm1heCkpO1xuICB9IGVsc2UgaWYgKG1pbiAmJiBtYXggJiYgKHZhbCA8IHJ1bGUubWluIHx8IHZhbCA+IHJ1bGUubWF4KSkge1xuICAgIGVycm9ycy5wdXNoKHV0aWwuZm9ybWF0KG9wdGlvbnMubWVzc2FnZXNba2V5XS5yYW5nZSwgcnVsZS5mdWxsRmllbGQsIHJ1bGUubWluLCBydWxlLm1heCkpO1xuICB9XG59XG5cbmV4cG9ydHNbJ2RlZmF1bHQnXSA9IHJhbmdlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/async-validator/es/rule/range.js
