var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");
/**
 * 圆环
 * @module zrender/graphic/shape/Ring
 */


var _default = Path.extend({
  type: 'ring',
  shape: {
    cx: 0,
    cy: 0,
    r: 0,
    r0: 0
  },
  buildPath: function buildPath(ctx, shape) {
    var x = shape.cx;
    var y = shape.cy;
    var PI2 = Math.PI * 2;
    ctx.moveTo(x + shape.r, y);
    ctx.arc(x, y, shape.r, 0, PI2, false);
    ctx.moveTo(x + shape.r0, y);
    ctx.arc(x, y, shape.r0, 0, PI2, true);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9SaW5nLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9SaW5nLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL1BhdGhcIik7XG5cbi8qKlxuICog5ZyG546vXG4gKiBAbW9kdWxlIHpyZW5kZXIvZ3JhcGhpYy9zaGFwZS9SaW5nXG4gKi9cbnZhciBfZGVmYXVsdCA9IFBhdGguZXh0ZW5kKHtcbiAgdHlwZTogJ3JpbmcnLFxuICBzaGFwZToge1xuICAgIGN4OiAwLFxuICAgIGN5OiAwLFxuICAgIHI6IDAsXG4gICAgcjA6IDBcbiAgfSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSkge1xuICAgIHZhciB4ID0gc2hhcGUuY3g7XG4gICAgdmFyIHkgPSBzaGFwZS5jeTtcbiAgICB2YXIgUEkyID0gTWF0aC5QSSAqIDI7XG4gICAgY3R4Lm1vdmVUbyh4ICsgc2hhcGUuciwgeSk7XG4gICAgY3R4LmFyYyh4LCB5LCBzaGFwZS5yLCAwLCBQSTIsIGZhbHNlKTtcbiAgICBjdHgubW92ZVRvKHggKyBzaGFwZS5yMCwgeSk7XG4gICAgY3R4LmFyYyh4LCB5LCBzaGFwZS5yMCwgMCwgUEkyLCB0cnVlKTtcbiAgfVxufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBQ0E7QUFrQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Ring.js
