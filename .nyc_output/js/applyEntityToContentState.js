/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule applyEntityToContentState
 * @format
 * 
 */


var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var applyEntityToContentBlock = __webpack_require__(/*! ./applyEntityToContentBlock */ "./node_modules/draft-js/lib/applyEntityToContentBlock.js");

function applyEntityToContentState(contentState, selectionState, entityKey) {
  var blockMap = contentState.getBlockMap();
  var startKey = selectionState.getStartKey();
  var startOffset = selectionState.getStartOffset();
  var endKey = selectionState.getEndKey();
  var endOffset = selectionState.getEndOffset();
  var newBlocks = blockMap.skipUntil(function (_, k) {
    return k === startKey;
  }).takeUntil(function (_, k) {
    return k === endKey;
  }).toOrderedMap().merge(Immutable.OrderedMap([[endKey, blockMap.get(endKey)]])).map(function (block, blockKey) {
    var sliceStart = blockKey === startKey ? startOffset : 0;
    var sliceEnd = blockKey === endKey ? endOffset : block.getLength();
    return applyEntityToContentBlock(block, sliceStart, sliceEnd, entityKey);
  });
  return contentState.merge({
    blockMap: blockMap.merge(newBlocks),
    selectionBefore: selectionState,
    selectionAfter: selectionState
  });
}

module.exports = applyEntityToContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2FwcGx5RW50aXR5VG9Db250ZW50U3RhdGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvYXBwbHlFbnRpdHlUb0NvbnRlbnRTdGF0ZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGFwcGx5RW50aXR5VG9Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEltbXV0YWJsZSA9IHJlcXVpcmUoJ2ltbXV0YWJsZScpO1xuXG52YXIgYXBwbHlFbnRpdHlUb0NvbnRlbnRCbG9jayA9IHJlcXVpcmUoJy4vYXBwbHlFbnRpdHlUb0NvbnRlbnRCbG9jaycpO1xuXG5mdW5jdGlvbiBhcHBseUVudGl0eVRvQ29udGVudFN0YXRlKGNvbnRlbnRTdGF0ZSwgc2VsZWN0aW9uU3RhdGUsIGVudGl0eUtleSkge1xuICB2YXIgYmxvY2tNYXAgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tNYXAoKTtcbiAgdmFyIHN0YXJ0S2V5ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRLZXkoKTtcbiAgdmFyIHN0YXJ0T2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0U3RhcnRPZmZzZXQoKTtcbiAgdmFyIGVuZEtleSA9IHNlbGVjdGlvblN0YXRlLmdldEVuZEtleSgpO1xuICB2YXIgZW5kT2Zmc2V0ID0gc2VsZWN0aW9uU3RhdGUuZ2V0RW5kT2Zmc2V0KCk7XG5cbiAgdmFyIG5ld0Jsb2NrcyA9IGJsb2NrTWFwLnNraXBVbnRpbChmdW5jdGlvbiAoXywgaykge1xuICAgIHJldHVybiBrID09PSBzdGFydEtleTtcbiAgfSkudGFrZVVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgcmV0dXJuIGsgPT09IGVuZEtleTtcbiAgfSkudG9PcmRlcmVkTWFwKCkubWVyZ2UoSW1tdXRhYmxlLk9yZGVyZWRNYXAoW1tlbmRLZXksIGJsb2NrTWFwLmdldChlbmRLZXkpXV0pKS5tYXAoZnVuY3Rpb24gKGJsb2NrLCBibG9ja0tleSkge1xuICAgIHZhciBzbGljZVN0YXJ0ID0gYmxvY2tLZXkgPT09IHN0YXJ0S2V5ID8gc3RhcnRPZmZzZXQgOiAwO1xuICAgIHZhciBzbGljZUVuZCA9IGJsb2NrS2V5ID09PSBlbmRLZXkgPyBlbmRPZmZzZXQgOiBibG9jay5nZXRMZW5ndGgoKTtcbiAgICByZXR1cm4gYXBwbHlFbnRpdHlUb0NvbnRlbnRCbG9jayhibG9jaywgc2xpY2VTdGFydCwgc2xpY2VFbmQsIGVudGl0eUtleSk7XG4gIH0pO1xuXG4gIHJldHVybiBjb250ZW50U3RhdGUubWVyZ2Uoe1xuICAgIGJsb2NrTWFwOiBibG9ja01hcC5tZXJnZShuZXdCbG9ja3MpLFxuICAgIHNlbGVjdGlvbkJlZm9yZTogc2VsZWN0aW9uU3RhdGUsXG4gICAgc2VsZWN0aW9uQWZ0ZXI6IHNlbGVjdGlvblN0YXRlXG4gIH0pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGFwcGx5RW50aXR5VG9Db250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/applyEntityToContentState.js
