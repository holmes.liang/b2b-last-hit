__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicAttachment", function() { return BasicAttachment; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-attachment.tsx";








var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();

var BasicAttachment =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(BasicAttachment, _QuoteStep);

  function BasicAttachment() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicAttachment);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicAttachment)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.attachment = {};
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicAttachment, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicAttachment.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        disabled: false
      };
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }, {
    key: "handleNext",
    value: function handleNext() {}
  }, {
    key: "renderActions",
    value: function renderActions() {}
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this2 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_15__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_12__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_14__["AllSteps"].CLAUSE);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        size: "large",
        type: "primary",
        disabled: this.state.disabled,
        onClick: function onClick() {
          return _this2.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Save and Continue").thai("Save and Continue").my("Save and Continue").getMessage()));
    }
  }, {
    key: "renderAttachment",
    value: function renderAttachment() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null);
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var formProps = {
        form: this.props.form,
        model: this.props.model
      };
      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("ATTACHMENTS").thai("ATTACHMENTS").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NCollapse"], {
        defaultActiveKey: ["1", "2"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_11__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("SELECT AN ATTACHMENT TYPE").thai("SELECT AN ATTACHMENT TYPE").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, this.renderAttachment())), this.renderRightActions())));
    }
  }]);

  return BasicAttachment;
}(_quote_step__WEBPACK_IMPORTED_MODULE_13__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtYXR0YWNobWVudC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9TQUlDL2lhci9iYXNpYy1hdHRhY2htZW50LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgQnV0dG9uLCBDb2wsIFJvdywgU3BpbiB9IGZyb20gXCJhbnRkXCI7XG5cbmltcG9ydCB7IEFqYXgsIExhbmd1YWdlLCBVdGlscywgQXBpcywgQ29uc3RzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTkRhdGUsIE5EYXRlRmlsdGVyLCBOUGFuZWwsIE5SYWRpbywgTlNlbGVjdCwgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IHBhZ2VUbyB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvd2l6YXJkXCI7XG5cbmltcG9ydCBRdW90ZVN0ZXAsIHsgU3RlcENvbXBvbmVudHMsIFN0ZXBQcm9wcyB9IGZyb20gXCIuLi8uLi9xdW90ZS1zdGVwXCI7XG5pbXBvcnQgeyBBbGxTdGVwcyB9IGZyb20gXCIuLi9hbGwtc3RlcHNcIjtcbmltcG9ydCB7IEJ1dHRvbkJhY2sgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2FudGQvYnV0dG9uLWJhY2tcIjtcblxuXG5jb25zdCBmb3JtRGF0ZUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxMiB9LFxuICAgIHNtOiB7IHNwYW46IDEyIH0sXG4gIH0sXG59O1xuY29uc3QgaXNNb2JpbGUgPSBVdGlscy5nZXRJc01vYmlsZSgpO1xuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgbG9hZGluZzogYm9vbGVhbjtcbiAgZGlzYWJsZWQ6IGJvb2xlYW47XG59XG5cbmNsYXNzIEJhc2ljQXR0YWNobWVudDxQIGV4dGVuZHMgU3RlcFByb3BzLCBTIGV4dGVuZHMgSVN0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCkpIGFzIEM7XG4gIH1cblxuICBhdHRhY2htZW50OiBhbnkgPSB7fTtcblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiB7XG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgIGRpc2FibGVkOiBmYWxzZSxcbiAgICB9IGFzIFM7XG4gIH1cblxuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJUaXRsZSgpIHtcbiAgfVxuXG4gIGhhbmRsZU5leHQoKSB7XG5cbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJBY3Rpb25zKCkge1xuICB9XG5cbiAgcmVuZGVyUmlnaHRBY3Rpb25zKCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgYWN0aW9uICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1hY3Rpb24tbGFyZ2VcIiA6IFwiXCJ9YH0+XG4gICAgICAgIDxCdXR0b25CYWNrIGhhbmRlbD17KCkgPT4ge1xuICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5DTEFVU0UpO1xuICAgICAgICB9fS8+XG4gICAgICAgIDxCdXR0b25cbiAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgIHR5cGU9XCJwcmltYXJ5XCJcbiAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5kaXNhYmxlZH1cbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZU5leHQoKX0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiU2F2ZSBhbmQgQ29udGludWVcIikudGhhaShcIlNhdmUgYW5kIENvbnRpbnVlXCIpLm15KFwiU2F2ZSBhbmQgQ29udGludWVcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L0J1dHRvbj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICByZW5kZXJBdHRhY2htZW50KCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIGdldFByb3AocHJvcE5hbWU/OiBzdHJpbmcpIHtcbiAgICByZXR1cm4gXCJcIjtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IGZvcm1Qcm9wcyA9IHtcbiAgICAgIGZvcm06IHRoaXMucHJvcHMuZm9ybSxcbiAgICAgIG1vZGVsOiB0aGlzLnByb3BzLm1vZGVsLFxuICAgIH07XG4gICAgY29uc3QgeyBmb3JtLCBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPFNwaW4gc2l6ZT1cImxhcmdlXCIgc3Bpbm5pbmc9e3RoaXMuc3RhdGUubG9hZGluZ30+XG4gICAgICAgIDxSb3cgdHlwZT1cImZsZXhcIiBqdXN0aWZ5PXtcInNwYWNlLWJldHdlZW5cIn0+XG4gICAgICAgICAge3RoaXMucmVuZGVyU2lkZUluZm8oKX1cbiAgICAgICAgICA8Q29sIHNtPXsxOH0geHM9ezIzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiQVRUQUNITUVOVFNcIikudGhhaShcIkFUVEFDSE1FTlRTXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPE5Db2xsYXBzZSBkZWZhdWx0QWN0aXZlS2V5PXtbXCIxXCIsIFwiMlwiXX0+XG4gICAgICAgICAgICAgIDxOUGFuZWxcbiAgICAgICAgICAgICAgICBrZXk9XCIxXCJcbiAgICAgICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiU0VMRUNUIEFOIEFUVEFDSE1FTlQgVFlQRVwiKVxuICAgICAgICAgICAgICAgICAgLnRoYWkoXCJTRUxFQ1QgQU4gQVRUQUNITUVOVCBUWVBFXCIpXG4gICAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVyQXR0YWNobWVudCgpfVxuICAgICAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgICAgIDwvTkNvbGxhcHNlPlxuICAgICAgICAgICAge3RoaXMucmVuZGVyUmlnaHRBY3Rpb25zKCl9XG4gICAgICAgICAgPC9Db2w+XG4gICAgICAgIDwvUm93PlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IHsgQmFzaWNBdHRhY2htZW50IH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBVUE7QUFDQTtBQU1BOzs7Ozs7Ozs7Ozs7Ozs7OztBQUtBOzs7Ozs7QUFKQTtBQUNBO0FBQ0E7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQTs7O0FBR0E7OztBQUlBOzs7QUFHQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTs7OztBQTFGQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-attachment.tsx
