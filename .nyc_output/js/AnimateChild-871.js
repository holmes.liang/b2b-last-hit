__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genAnimateChild", function() { return genAnimateChild; });
/* harmony import */ var babel_runtime_helpers_toArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/toArray */ "./node_modules/babel-runtime/helpers/toArray.js");
/* harmony import */ var babel_runtime_helpers_toArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_toArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var component_classes__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! component-classes */ "./node_modules/component-classes/index.js");
/* harmony import */ var component_classes__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(component_classes__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/util.js");













var clonePropList = ['appeared', 'show', 'exclusive', 'children', 'animation'];
/**
 * AnimateChild only accept one child node.
 * `transitionSupport` is used for none transition test case.
 * Default we use browser transition event support check.
 */

function genAnimateChild(transitionSupport) {
  var AnimateChild = function (_React$Component) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(AnimateChild, _React$Component);

    function AnimateChild() {
      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, AnimateChild); // [Legacy] Since old code addListener on the element.
      // To avoid break the behaviour that component not handle animation/transition
      // also can handle the animate, let keep the logic.


      var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, (AnimateChild.__proto__ || Object.getPrototypeOf(AnimateChild)).call(this));

      _this.state = {
        child: null,
        eventQueue: [],
        eventActive: false
      };

      _this.onDomUpdated = function () {
        var eventActive = _this.state.eventActive;
        var _this$props = _this.props,
            transitionName = _this$props.transitionName,
            animation = _this$props.animation,
            onChildLeaved = _this$props.onChildLeaved,
            animateKey = _this$props.animateKey;

        var $ele = _this.getDomElement(); // Skip if dom element not ready


        if (!$ele) return; // [Legacy] Add animation/transition event by dom level

        if (transitionSupport && _this.$prevEle !== $ele) {
          _this.cleanDomEvent();

          _this.$prevEle = $ele;

          _this.$prevEle.addEventListener(_util__WEBPACK_IMPORTED_MODULE_12__["animationEndName"], _this.onMotionEnd);

          _this.$prevEle.addEventListener(_util__WEBPACK_IMPORTED_MODULE_12__["transitionEndName"], _this.onMotionEnd);
        }

        var currentEvent = _this.getCurrentEvent();

        if (currentEvent.empty) {
          // Additional process the leave event
          if (currentEvent.lastEventType === 'leave') {
            onChildLeaved(animateKey);
          }

          return;
        }

        var eventType = currentEvent.eventType,
            restQueue = currentEvent.restQueue;
        var nodeClasses = component_classes__WEBPACK_IMPORTED_MODULE_10___default()($ele); // [Legacy] Since origin code use js to set `className`.
        // This caused that any component without support `className` can be forced set.
        // Let's keep the logic.

        function legacyAppendClass() {
          if (!transitionSupport) return;
          var basicClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, '' + eventType);
          if (basicClassName) nodeClasses.add(basicClassName);

          if (eventActive) {
            var activeClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, eventType + '-active');
            if (activeClassName) nodeClasses.add(activeClassName);
          }
        }

        if (_this.currentEvent && _this.currentEvent.type === eventType) {
          legacyAppendClass();
          return;
        } // Clear timeout for legacy check


        clearTimeout(_this.timeout); // Clean up last event environment

        if (_this.currentEvent && _this.currentEvent.animateObj && _this.currentEvent.animateObj.stop) {
          _this.currentEvent.animateObj.stop();
        } // Clean up last transition class


        if (_this.currentEvent) {
          var basicClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, '' + _this.currentEvent.type);
          var activeClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, _this.currentEvent.type + '-active');
          if (basicClassName) nodeClasses.remove(basicClassName);
          if (activeClassName) nodeClasses.remove(activeClassName);
        } // New event come


        _this.currentEvent = {
          type: eventType
        };
        var animationHandler = (animation || {})[eventType]; // =============== Check if has customize animation ===============

        if (animationHandler) {
          _this.currentEvent.animateObj = animationHandler($ele, function () {
            _this.onMotionEnd({
              target: $ele
            });
          }); // Do next step if not animate object provided

          if (!_this.currentEvent || !_this.currentEvent.animateObj) {
            _this.nextEvent(restQueue);
          } // ==================== Use transition instead ====================

        } else if (transitionSupport) {
          legacyAppendClass();

          if (!eventActive) {
            // Trigger `eventActive` in next frame
            raf__WEBPACK_IMPORTED_MODULE_11___default()(function () {
              if (_this.currentEvent && _this.currentEvent.type === eventType && !_this._destroy) {
                _this.setState({
                  eventActive: true
                }, function () {
                  // [Legacy] Handle timeout if browser transition event not handle
                  var transitionDelay = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getStyleValue"])($ele, 'transition-delay') || 0;
                  var transitionDuration = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getStyleValue"])($ele, 'transition-duration') || 0;
                  var animationDelay = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getStyleValue"])($ele, 'animation-delay') || 0;
                  var animationDuration = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getStyleValue"])($ele, 'animation-duration') || 0;
                  var totalTime = Math.max(transitionDuration + transitionDelay, animationDuration + animationDelay);

                  if (totalTime >= 0) {
                    _this.timeout = setTimeout(function () {
                      _this.onMotionEnd({
                        target: $ele
                      });
                    }, totalTime * 1000);
                  }
                });
              }
            });
          } // ======================= Just next action =======================

        } else {
          _this.onMotionEnd({
            target: $ele
          });
        }
      };

      _this.onMotionEnd = function (_ref) {
        var target = _ref.target;
        var _this$props2 = _this.props,
            transitionName = _this$props2.transitionName,
            onChildLeaved = _this$props2.onChildLeaved,
            animateKey = _this$props2.animateKey,
            onAppear = _this$props2.onAppear,
            onEnter = _this$props2.onEnter,
            onLeave = _this$props2.onLeave,
            onEnd = _this$props2.onEnd;

        var currentEvent = _this.getCurrentEvent();

        if (currentEvent.empty) return; // Clear timeout for legacy check

        clearTimeout(_this.timeout);
        var restQueue = currentEvent.restQueue;

        var $ele = _this.getDomElement();

        if (!_this.currentEvent || $ele !== target) return;

        if (_this.currentEvent.animateObj && _this.currentEvent.animateObj.stop) {
          _this.currentEvent.animateObj.stop();
        } // [Legacy] Same as above, we need call js to remove the class


        if (transitionSupport && _this.currentEvent) {
          var basicClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, _this.currentEvent.type);
          var activeClassName = Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, _this.currentEvent.type + '-active');
          var nodeClasses = component_classes__WEBPACK_IMPORTED_MODULE_10___default()($ele);
          if (basicClassName) nodeClasses.remove(basicClassName);
          if (activeClassName) nodeClasses.remove(activeClassName);
        } // Additional process the leave event


        if (_this.currentEvent && _this.currentEvent.type === 'leave') {
          onChildLeaved(animateKey);
        } // [Legacy] Trigger on event when it's last event


        if (_this.currentEvent && !restQueue.length) {
          if (_this.currentEvent.type === 'appear' && onAppear) {
            onAppear(animateKey);
          } else if (_this.currentEvent.type === 'enter' && onEnter) {
            onEnter(animateKey);
          } else if (_this.currentEvent.type === 'leave' && onLeave) {
            onLeave(animateKey);
          }

          if (onEnd) {
            // OnEnd(key, isShow)
            onEnd(animateKey, _this.currentEvent.type !== 'leave');
          }
        }

        _this.currentEvent = null; // Next queue

        _this.nextEvent(restQueue);
      };

      _this.getDomElement = function () {
        if (_this._destroy) return null;
        return react_dom__WEBPACK_IMPORTED_MODULE_6___default.a.findDOMNode(_this);
      };

      _this.getCurrentEvent = function () {
        var _this$state$eventQueu = _this.state.eventQueue,
            eventQueue = _this$state$eventQueu === undefined ? [] : _this$state$eventQueu;
        var _this$props3 = _this.props,
            animation = _this$props3.animation,
            exclusive = _this$props3.exclusive,
            transitionAppear = _this$props3.transitionAppear,
            transitionEnter = _this$props3.transitionEnter,
            transitionLeave = _this$props3.transitionLeave;

        function hasEventHandler(eventType) {
          return eventType === 'appear' && (transitionAppear || animation.appear) || eventType === 'enter' && (transitionEnter || animation.enter) || eventType === 'leave' && (transitionLeave || animation.leave);
        }

        var event = null; // If is exclusive, only check the last event

        if (exclusive) {
          var eventType = eventQueue[eventQueue.length - 1];

          if (hasEventHandler(eventType)) {
            event = {
              eventType: eventType,
              restQueue: []
            };
          }
        } else {
          // Loop check the queue until find match
          var cloneQueue = eventQueue.slice();

          while (cloneQueue.length) {
            var _cloneQueue = cloneQueue,
                _cloneQueue2 = babel_runtime_helpers_toArray__WEBPACK_IMPORTED_MODULE_0___default()(_cloneQueue),
                _eventType = _cloneQueue2[0],
                restQueue = _cloneQueue2.slice(1);

            if (hasEventHandler(_eventType)) {
              event = {
                eventType: _eventType,
                restQueue: restQueue
              };
              break;
            }

            cloneQueue = restQueue;
          }
        }

        if (!event) {
          event = {
            empty: true,
            lastEventType: eventQueue[eventQueue.length - 1]
          };
        }

        return event;
      };

      _this.nextEvent = function (restQueue) {
        // Next queue
        if (!_this._destroy) {
          _this.setState({
            eventQueue: restQueue,
            eventActive: false
          });
        }
      };

      _this.cleanDomEvent = function () {
        if (_this.$prevEle && transitionSupport) {
          _this.$prevEle.removeEventListener(_util__WEBPACK_IMPORTED_MODULE_12__["animationEndName"], _this.onMotionEnd);

          _this.$prevEle.removeEventListener(_util__WEBPACK_IMPORTED_MODULE_12__["transitionEndName"], _this.onMotionEnd);
        }
      };

      _this.$prevEle = null;
      _this.currentEvent = null;
      _this.timeout = null;
      return _this;
    }

    babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(AnimateChild, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.onDomUpdated();
      }
    }, {
      key: 'componentDidUpdate',
      value: function componentDidUpdate() {
        this.onDomUpdated();
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        clearTimeout(this.timeout);
        this._destroy = true;
        this.cleanDomEvent();
      }
    }, {
      key: 'render',
      value: function render() {
        var _state = this.state,
            child = _state.child,
            eventActive = _state.eventActive;
        var _props = this.props,
            showProp = _props.showProp,
            transitionName = _props.transitionName;

        var _ref2 = child.props || {},
            className = _ref2.className;

        var currentEvent = this.getCurrentEvent(); // Class name

        var connectClassName = transitionSupport && this.currentEvent ? classnames__WEBPACK_IMPORTED_MODULE_9___default()(className, Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, this.currentEvent.type), eventActive && Object(_util__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(transitionName, this.currentEvent.type + '-active')) : className;
        var show = true; // Keep show when is in transition or has customize animate

        if (transitionSupport && (!currentEvent.empty || this.currentEvent && this.currentEvent.animateObj)) {
          show = true;
        } else {
          show = child.props[showProp];
        } // Clone child


        var newChildProps = {
          className: connectClassName
        };

        if (showProp) {
          newChildProps[showProp] = show;
        }

        return react__WEBPACK_IMPORTED_MODULE_5___default.a.cloneElement(child, newChildProps);
      }
    }], [{
      key: 'getDerivedStateFromProps',
      value: function getDerivedStateFromProps(nextProps, prevState) {
        var _prevState$prevProps = prevState.prevProps,
            prevProps = _prevState$prevProps === undefined ? {} : _prevState$prevProps;
        var appeared = nextProps.appeared;
        var newState = {
          prevProps: Object(_util__WEBPACK_IMPORTED_MODULE_12__["cloneProps"])(nextProps, clonePropList)
        };

        function processState(propName, updater) {
          if (prevProps[propName] !== nextProps[propName]) {
            if (updater) {
              updater(nextProps[propName]);
            }

            return true;
          }

          return false;
        }

        function pushEvent(eventType) {
          var eventQueue = newState.eventQueue || prevState.eventQueue.slice();
          var matchIndex = eventQueue.indexOf(eventType); // Clean the rest event if eventType match

          if (matchIndex !== -1) {
            eventQueue = eventQueue.slice(0, matchIndex);
          }

          eventQueue.push(eventType);
          newState.eventQueue = eventQueue;
        } // Child update. Only set child.


        processState('children', function (child) {
          newState.child = child;
        });
        processState('appeared', function (isAppeared) {
          if (isAppeared) {
            pushEvent('appear');
          }
        }); // Show update

        processState('show', function (show) {
          if (!appeared) {
            if (show) {
              pushEvent('enter');
            } else {
              pushEvent('leave');
            }
          }
        });
        return newState;
      }
    }]);

    return AnimateChild;
  }(react__WEBPACK_IMPORTED_MODULE_5___default.a.Component);

  AnimateChild.propTypes = {
    transitionName: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object]),
    transitionAppear: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
    transitionEnter: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
    transitionLeave: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
    exclusive: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
    appeared: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
    showProp: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
    animateKey: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.any,
    animation: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object,
    onChildLeaved: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
    onEnd: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
    onAppear: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
    onEnter: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
    onLeave: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func
  };
  Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__["polyfill"])(AnimateChild);
  return AnimateChild;
}
/* harmony default export */ __webpack_exports__["default"] = (genAnimateChild(_util__WEBPACK_IMPORTED_MODULE_12__["supportTransition"]));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvbm9kZV9tb2R1bGVzL3JjLWFuaW1hdGUvZXMvQW5pbWF0ZUNoaWxkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvbm9kZV9tb2R1bGVzL3JjLWFuaW1hdGUvZXMvQW5pbWF0ZUNoaWxkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfdG9BcnJheSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvdG9BcnJheSc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgY2xhc3NlcyBmcm9tICdjb21wb25lbnQtY2xhc3Nlcyc7XG5pbXBvcnQgcmFmIGZyb20gJ3JhZic7XG5cbmltcG9ydCB7IGdldFN0eWxlVmFsdWUsIGNsb25lUHJvcHMsIGdldFRyYW5zaXRpb25OYW1lLCBzdXBwb3J0VHJhbnNpdGlvbiwgYW5pbWF0aW9uRW5kTmFtZSwgdHJhbnNpdGlvbkVuZE5hbWUgfSBmcm9tICcuL3V0aWwnO1xuXG52YXIgY2xvbmVQcm9wTGlzdCA9IFsnYXBwZWFyZWQnLCAnc2hvdycsICdleGNsdXNpdmUnLCAnY2hpbGRyZW4nLCAnYW5pbWF0aW9uJ107XG5cbi8qKlxuICogQW5pbWF0ZUNoaWxkIG9ubHkgYWNjZXB0IG9uZSBjaGlsZCBub2RlLlxuICogYHRyYW5zaXRpb25TdXBwb3J0YCBpcyB1c2VkIGZvciBub25lIHRyYW5zaXRpb24gdGVzdCBjYXNlLlxuICogRGVmYXVsdCB3ZSB1c2UgYnJvd3NlciB0cmFuc2l0aW9uIGV2ZW50IHN1cHBvcnQgY2hlY2suXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZW5BbmltYXRlQ2hpbGQodHJhbnNpdGlvblN1cHBvcnQpIHtcbiAgdmFyIEFuaW1hdGVDaGlsZCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICAgX2luaGVyaXRzKEFuaW1hdGVDaGlsZCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgICBmdW5jdGlvbiBBbmltYXRlQ2hpbGQoKSB7XG4gICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQW5pbWF0ZUNoaWxkKTtcblxuICAgICAgLy8gW0xlZ2FjeV0gU2luY2Ugb2xkIGNvZGUgYWRkTGlzdGVuZXIgb24gdGhlIGVsZW1lbnQuXG4gICAgICAvLyBUbyBhdm9pZCBicmVhayB0aGUgYmVoYXZpb3VyIHRoYXQgY29tcG9uZW50IG5vdCBoYW5kbGUgYW5pbWF0aW9uL3RyYW5zaXRpb25cbiAgICAgIC8vIGFsc28gY2FuIGhhbmRsZSB0aGUgYW5pbWF0ZSwgbGV0IGtlZXAgdGhlIGxvZ2ljLlxuICAgICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKEFuaW1hdGVDaGlsZC5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKEFuaW1hdGVDaGlsZCkpLmNhbGwodGhpcykpO1xuXG4gICAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgY2hpbGQ6IG51bGwsXG5cbiAgICAgICAgZXZlbnRRdWV1ZTogW10sXG4gICAgICAgIGV2ZW50QWN0aXZlOiBmYWxzZVxuICAgICAgfTtcblxuICAgICAgX3RoaXMub25Eb21VcGRhdGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZXZlbnRBY3RpdmUgPSBfdGhpcy5zdGF0ZS5ldmVudEFjdGl2ZTtcbiAgICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICB0cmFuc2l0aW9uTmFtZSA9IF90aGlzJHByb3BzLnRyYW5zaXRpb25OYW1lLFxuICAgICAgICAgICAgYW5pbWF0aW9uID0gX3RoaXMkcHJvcHMuYW5pbWF0aW9uLFxuICAgICAgICAgICAgb25DaGlsZExlYXZlZCA9IF90aGlzJHByb3BzLm9uQ2hpbGRMZWF2ZWQsXG4gICAgICAgICAgICBhbmltYXRlS2V5ID0gX3RoaXMkcHJvcHMuYW5pbWF0ZUtleTtcblxuXG4gICAgICAgIHZhciAkZWxlID0gX3RoaXMuZ2V0RG9tRWxlbWVudCgpO1xuXG4gICAgICAgIC8vIFNraXAgaWYgZG9tIGVsZW1lbnQgbm90IHJlYWR5XG4gICAgICAgIGlmICghJGVsZSkgcmV0dXJuO1xuXG4gICAgICAgIC8vIFtMZWdhY3ldIEFkZCBhbmltYXRpb24vdHJhbnNpdGlvbiBldmVudCBieSBkb20gbGV2ZWxcbiAgICAgICAgaWYgKHRyYW5zaXRpb25TdXBwb3J0ICYmIF90aGlzLiRwcmV2RWxlICE9PSAkZWxlKSB7XG4gICAgICAgICAgX3RoaXMuY2xlYW5Eb21FdmVudCgpO1xuXG4gICAgICAgICAgX3RoaXMuJHByZXZFbGUgPSAkZWxlO1xuICAgICAgICAgIF90aGlzLiRwcmV2RWxlLmFkZEV2ZW50TGlzdGVuZXIoYW5pbWF0aW9uRW5kTmFtZSwgX3RoaXMub25Nb3Rpb25FbmQpO1xuICAgICAgICAgIF90aGlzLiRwcmV2RWxlLmFkZEV2ZW50TGlzdGVuZXIodHJhbnNpdGlvbkVuZE5hbWUsIF90aGlzLm9uTW90aW9uRW5kKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBjdXJyZW50RXZlbnQgPSBfdGhpcy5nZXRDdXJyZW50RXZlbnQoKTtcbiAgICAgICAgaWYgKGN1cnJlbnRFdmVudC5lbXB0eSkge1xuICAgICAgICAgIC8vIEFkZGl0aW9uYWwgcHJvY2VzcyB0aGUgbGVhdmUgZXZlbnRcbiAgICAgICAgICBpZiAoY3VycmVudEV2ZW50Lmxhc3RFdmVudFR5cGUgPT09ICdsZWF2ZScpIHtcbiAgICAgICAgICAgIG9uQ2hpbGRMZWF2ZWQoYW5pbWF0ZUtleSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBldmVudFR5cGUgPSBjdXJyZW50RXZlbnQuZXZlbnRUeXBlLFxuICAgICAgICAgICAgcmVzdFF1ZXVlID0gY3VycmVudEV2ZW50LnJlc3RRdWV1ZTtcblxuICAgICAgICB2YXIgbm9kZUNsYXNzZXMgPSBjbGFzc2VzKCRlbGUpO1xuXG4gICAgICAgIC8vIFtMZWdhY3ldIFNpbmNlIG9yaWdpbiBjb2RlIHVzZSBqcyB0byBzZXQgYGNsYXNzTmFtZWAuXG4gICAgICAgIC8vIFRoaXMgY2F1c2VkIHRoYXQgYW55IGNvbXBvbmVudCB3aXRob3V0IHN1cHBvcnQgYGNsYXNzTmFtZWAgY2FuIGJlIGZvcmNlZCBzZXQuXG4gICAgICAgIC8vIExldCdzIGtlZXAgdGhlIGxvZ2ljLlxuICAgICAgICBmdW5jdGlvbiBsZWdhY3lBcHBlbmRDbGFzcygpIHtcbiAgICAgICAgICBpZiAoIXRyYW5zaXRpb25TdXBwb3J0KSByZXR1cm47XG5cbiAgICAgICAgICB2YXIgYmFzaWNDbGFzc05hbWUgPSBnZXRUcmFuc2l0aW9uTmFtZSh0cmFuc2l0aW9uTmFtZSwgJycgKyBldmVudFR5cGUpO1xuICAgICAgICAgIGlmIChiYXNpY0NsYXNzTmFtZSkgbm9kZUNsYXNzZXMuYWRkKGJhc2ljQ2xhc3NOYW1lKTtcblxuICAgICAgICAgIGlmIChldmVudEFjdGl2ZSkge1xuICAgICAgICAgICAgdmFyIGFjdGl2ZUNsYXNzTmFtZSA9IGdldFRyYW5zaXRpb25OYW1lKHRyYW5zaXRpb25OYW1lLCBldmVudFR5cGUgKyAnLWFjdGl2ZScpO1xuICAgICAgICAgICAgaWYgKGFjdGl2ZUNsYXNzTmFtZSkgbm9kZUNsYXNzZXMuYWRkKGFjdGl2ZUNsYXNzTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKF90aGlzLmN1cnJlbnRFdmVudCAmJiBfdGhpcy5jdXJyZW50RXZlbnQudHlwZSA9PT0gZXZlbnRUeXBlKSB7XG4gICAgICAgICAgbGVnYWN5QXBwZW5kQ2xhc3MoKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDbGVhciB0aW1lb3V0IGZvciBsZWdhY3kgY2hlY2tcbiAgICAgICAgY2xlYXJUaW1lb3V0KF90aGlzLnRpbWVvdXQpO1xuXG4gICAgICAgIC8vIENsZWFuIHVwIGxhc3QgZXZlbnQgZW52aXJvbm1lbnRcbiAgICAgICAgaWYgKF90aGlzLmN1cnJlbnRFdmVudCAmJiBfdGhpcy5jdXJyZW50RXZlbnQuYW5pbWF0ZU9iaiAmJiBfdGhpcy5jdXJyZW50RXZlbnQuYW5pbWF0ZU9iai5zdG9wKSB7XG4gICAgICAgICAgX3RoaXMuY3VycmVudEV2ZW50LmFuaW1hdGVPYmouc3RvcCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQ2xlYW4gdXAgbGFzdCB0cmFuc2l0aW9uIGNsYXNzXG4gICAgICAgIGlmIChfdGhpcy5jdXJyZW50RXZlbnQpIHtcbiAgICAgICAgICB2YXIgYmFzaWNDbGFzc05hbWUgPSBnZXRUcmFuc2l0aW9uTmFtZSh0cmFuc2l0aW9uTmFtZSwgJycgKyBfdGhpcy5jdXJyZW50RXZlbnQudHlwZSk7XG4gICAgICAgICAgdmFyIGFjdGl2ZUNsYXNzTmFtZSA9IGdldFRyYW5zaXRpb25OYW1lKHRyYW5zaXRpb25OYW1lLCBfdGhpcy5jdXJyZW50RXZlbnQudHlwZSArICctYWN0aXZlJyk7XG4gICAgICAgICAgaWYgKGJhc2ljQ2xhc3NOYW1lKSBub2RlQ2xhc3Nlcy5yZW1vdmUoYmFzaWNDbGFzc05hbWUpO1xuICAgICAgICAgIGlmIChhY3RpdmVDbGFzc05hbWUpIG5vZGVDbGFzc2VzLnJlbW92ZShhY3RpdmVDbGFzc05hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gTmV3IGV2ZW50IGNvbWVcbiAgICAgICAgX3RoaXMuY3VycmVudEV2ZW50ID0ge1xuICAgICAgICAgIHR5cGU6IGV2ZW50VHlwZVxuICAgICAgICB9O1xuXG4gICAgICAgIHZhciBhbmltYXRpb25IYW5kbGVyID0gKGFuaW1hdGlvbiB8fCB7fSlbZXZlbnRUeXBlXTtcbiAgICAgICAgLy8gPT09PT09PT09PT09PT09IENoZWNrIGlmIGhhcyBjdXN0b21pemUgYW5pbWF0aW9uID09PT09PT09PT09PT09PVxuICAgICAgICBpZiAoYW5pbWF0aW9uSGFuZGxlcikge1xuICAgICAgICAgIF90aGlzLmN1cnJlbnRFdmVudC5hbmltYXRlT2JqID0gYW5pbWF0aW9uSGFuZGxlcigkZWxlLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy5vbk1vdGlvbkVuZCh7IHRhcmdldDogJGVsZSB9KTtcbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIC8vIERvIG5leHQgc3RlcCBpZiBub3QgYW5pbWF0ZSBvYmplY3QgcHJvdmlkZWRcbiAgICAgICAgICBpZiAoIV90aGlzLmN1cnJlbnRFdmVudCB8fCAhX3RoaXMuY3VycmVudEV2ZW50LmFuaW1hdGVPYmopIHtcbiAgICAgICAgICAgIF90aGlzLm5leHRFdmVudChyZXN0UXVldWUpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vID09PT09PT09PT09PT09PT09PT09IFVzZSB0cmFuc2l0aW9uIGluc3RlYWQgPT09PT09PT09PT09PT09PT09PT1cbiAgICAgICAgfSBlbHNlIGlmICh0cmFuc2l0aW9uU3VwcG9ydCkge1xuICAgICAgICAgIGxlZ2FjeUFwcGVuZENsYXNzKCk7XG4gICAgICAgICAgaWYgKCFldmVudEFjdGl2ZSkge1xuICAgICAgICAgICAgLy8gVHJpZ2dlciBgZXZlbnRBY3RpdmVgIGluIG5leHQgZnJhbWVcbiAgICAgICAgICAgIHJhZihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIGlmIChfdGhpcy5jdXJyZW50RXZlbnQgJiYgX3RoaXMuY3VycmVudEV2ZW50LnR5cGUgPT09IGV2ZW50VHlwZSAmJiAhX3RoaXMuX2Rlc3Ryb3kpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7IGV2ZW50QWN0aXZlOiB0cnVlIH0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIC8vIFtMZWdhY3ldIEhhbmRsZSB0aW1lb3V0IGlmIGJyb3dzZXIgdHJhbnNpdGlvbiBldmVudCBub3QgaGFuZGxlXG4gICAgICAgICAgICAgICAgICB2YXIgdHJhbnNpdGlvbkRlbGF5ID0gZ2V0U3R5bGVWYWx1ZSgkZWxlLCAndHJhbnNpdGlvbi1kZWxheScpIHx8IDA7XG4gICAgICAgICAgICAgICAgICB2YXIgdHJhbnNpdGlvbkR1cmF0aW9uID0gZ2V0U3R5bGVWYWx1ZSgkZWxlLCAndHJhbnNpdGlvbi1kdXJhdGlvbicpIHx8IDA7XG4gICAgICAgICAgICAgICAgICB2YXIgYW5pbWF0aW9uRGVsYXkgPSBnZXRTdHlsZVZhbHVlKCRlbGUsICdhbmltYXRpb24tZGVsYXknKSB8fCAwO1xuICAgICAgICAgICAgICAgICAgdmFyIGFuaW1hdGlvbkR1cmF0aW9uID0gZ2V0U3R5bGVWYWx1ZSgkZWxlLCAnYW5pbWF0aW9uLWR1cmF0aW9uJykgfHwgMDtcbiAgICAgICAgICAgICAgICAgIHZhciB0b3RhbFRpbWUgPSBNYXRoLm1heCh0cmFuc2l0aW9uRHVyYXRpb24gKyB0cmFuc2l0aW9uRGVsYXksIGFuaW1hdGlvbkR1cmF0aW9uICsgYW5pbWF0aW9uRGVsYXkpO1xuXG4gICAgICAgICAgICAgICAgICBpZiAodG90YWxUaW1lID49IDApIHtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMudGltZW91dCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLm9uTW90aW9uRW5kKHsgdGFyZ2V0OiAkZWxlIH0pO1xuICAgICAgICAgICAgICAgICAgICB9LCB0b3RhbFRpbWUgKiAxMDAwKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT0gSnVzdCBuZXh0IGFjdGlvbiA9PT09PT09PT09PT09PT09PT09PT09PVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF90aGlzLm9uTW90aW9uRW5kKHsgdGFyZ2V0OiAkZWxlIH0pO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5vbk1vdGlvbkVuZCA9IGZ1bmN0aW9uIChfcmVmKSB7XG4gICAgICAgIHZhciB0YXJnZXQgPSBfcmVmLnRhcmdldDtcbiAgICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgdHJhbnNpdGlvbk5hbWUgPSBfdGhpcyRwcm9wczIudHJhbnNpdGlvbk5hbWUsXG4gICAgICAgICAgICBvbkNoaWxkTGVhdmVkID0gX3RoaXMkcHJvcHMyLm9uQ2hpbGRMZWF2ZWQsXG4gICAgICAgICAgICBhbmltYXRlS2V5ID0gX3RoaXMkcHJvcHMyLmFuaW1hdGVLZXksXG4gICAgICAgICAgICBvbkFwcGVhciA9IF90aGlzJHByb3BzMi5vbkFwcGVhcixcbiAgICAgICAgICAgIG9uRW50ZXIgPSBfdGhpcyRwcm9wczIub25FbnRlcixcbiAgICAgICAgICAgIG9uTGVhdmUgPSBfdGhpcyRwcm9wczIub25MZWF2ZSxcbiAgICAgICAgICAgIG9uRW5kID0gX3RoaXMkcHJvcHMyLm9uRW5kO1xuXG4gICAgICAgIHZhciBjdXJyZW50RXZlbnQgPSBfdGhpcy5nZXRDdXJyZW50RXZlbnQoKTtcbiAgICAgICAgaWYgKGN1cnJlbnRFdmVudC5lbXB0eSkgcmV0dXJuO1xuXG4gICAgICAgIC8vIENsZWFyIHRpbWVvdXQgZm9yIGxlZ2FjeSBjaGVja1xuICAgICAgICBjbGVhclRpbWVvdXQoX3RoaXMudGltZW91dCk7XG5cbiAgICAgICAgdmFyIHJlc3RRdWV1ZSA9IGN1cnJlbnRFdmVudC5yZXN0UXVldWU7XG5cblxuICAgICAgICB2YXIgJGVsZSA9IF90aGlzLmdldERvbUVsZW1lbnQoKTtcbiAgICAgICAgaWYgKCFfdGhpcy5jdXJyZW50RXZlbnQgfHwgJGVsZSAhPT0gdGFyZ2V0KSByZXR1cm47XG5cbiAgICAgICAgaWYgKF90aGlzLmN1cnJlbnRFdmVudC5hbmltYXRlT2JqICYmIF90aGlzLmN1cnJlbnRFdmVudC5hbmltYXRlT2JqLnN0b3ApIHtcbiAgICAgICAgICBfdGhpcy5jdXJyZW50RXZlbnQuYW5pbWF0ZU9iai5zdG9wKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBbTGVnYWN5XSBTYW1lIGFzIGFib3ZlLCB3ZSBuZWVkIGNhbGwganMgdG8gcmVtb3ZlIHRoZSBjbGFzc1xuICAgICAgICBpZiAodHJhbnNpdGlvblN1cHBvcnQgJiYgX3RoaXMuY3VycmVudEV2ZW50KSB7XG4gICAgICAgICAgdmFyIGJhc2ljQ2xhc3NOYW1lID0gZ2V0VHJhbnNpdGlvbk5hbWUodHJhbnNpdGlvbk5hbWUsIF90aGlzLmN1cnJlbnRFdmVudC50eXBlKTtcbiAgICAgICAgICB2YXIgYWN0aXZlQ2xhc3NOYW1lID0gZ2V0VHJhbnNpdGlvbk5hbWUodHJhbnNpdGlvbk5hbWUsIF90aGlzLmN1cnJlbnRFdmVudC50eXBlICsgJy1hY3RpdmUnKTtcblxuICAgICAgICAgIHZhciBub2RlQ2xhc3NlcyA9IGNsYXNzZXMoJGVsZSk7XG4gICAgICAgICAgaWYgKGJhc2ljQ2xhc3NOYW1lKSBub2RlQ2xhc3Nlcy5yZW1vdmUoYmFzaWNDbGFzc05hbWUpO1xuICAgICAgICAgIGlmIChhY3RpdmVDbGFzc05hbWUpIG5vZGVDbGFzc2VzLnJlbW92ZShhY3RpdmVDbGFzc05hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQWRkaXRpb25hbCBwcm9jZXNzIHRoZSBsZWF2ZSBldmVudFxuICAgICAgICBpZiAoX3RoaXMuY3VycmVudEV2ZW50ICYmIF90aGlzLmN1cnJlbnRFdmVudC50eXBlID09PSAnbGVhdmUnKSB7XG4gICAgICAgICAgb25DaGlsZExlYXZlZChhbmltYXRlS2V5KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFtMZWdhY3ldIFRyaWdnZXIgb24gZXZlbnQgd2hlbiBpdCdzIGxhc3QgZXZlbnRcbiAgICAgICAgaWYgKF90aGlzLmN1cnJlbnRFdmVudCAmJiAhcmVzdFF1ZXVlLmxlbmd0aCkge1xuICAgICAgICAgIGlmIChfdGhpcy5jdXJyZW50RXZlbnQudHlwZSA9PT0gJ2FwcGVhcicgJiYgb25BcHBlYXIpIHtcbiAgICAgICAgICAgIG9uQXBwZWFyKGFuaW1hdGVLZXkpO1xuICAgICAgICAgIH0gZWxzZSBpZiAoX3RoaXMuY3VycmVudEV2ZW50LnR5cGUgPT09ICdlbnRlcicgJiYgb25FbnRlcikge1xuICAgICAgICAgICAgb25FbnRlcihhbmltYXRlS2V5KTtcbiAgICAgICAgICB9IGVsc2UgaWYgKF90aGlzLmN1cnJlbnRFdmVudC50eXBlID09PSAnbGVhdmUnICYmIG9uTGVhdmUpIHtcbiAgICAgICAgICAgIG9uTGVhdmUoYW5pbWF0ZUtleSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKG9uRW5kKSB7XG4gICAgICAgICAgICAvLyBPbkVuZChrZXksIGlzU2hvdylcbiAgICAgICAgICAgIG9uRW5kKGFuaW1hdGVLZXksIF90aGlzLmN1cnJlbnRFdmVudC50eXBlICE9PSAnbGVhdmUnKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBfdGhpcy5jdXJyZW50RXZlbnQgPSBudWxsO1xuXG4gICAgICAgIC8vIE5leHQgcXVldWVcbiAgICAgICAgX3RoaXMubmV4dEV2ZW50KHJlc3RRdWV1ZSk7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5nZXREb21FbGVtZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoX3RoaXMuX2Rlc3Ryb3kpIHJldHVybiBudWxsO1xuICAgICAgICByZXR1cm4gUmVhY3RET00uZmluZERPTU5vZGUoX3RoaXMpO1xuICAgICAgfTtcblxuICAgICAgX3RoaXMuZ2V0Q3VycmVudEV2ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMkc3RhdGUkZXZlbnRRdWV1ID0gX3RoaXMuc3RhdGUuZXZlbnRRdWV1ZSxcbiAgICAgICAgICAgIGV2ZW50UXVldWUgPSBfdGhpcyRzdGF0ZSRldmVudFF1ZXUgPT09IHVuZGVmaW5lZCA/IFtdIDogX3RoaXMkc3RhdGUkZXZlbnRRdWV1O1xuICAgICAgICB2YXIgX3RoaXMkcHJvcHMzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICBhbmltYXRpb24gPSBfdGhpcyRwcm9wczMuYW5pbWF0aW9uLFxuICAgICAgICAgICAgZXhjbHVzaXZlID0gX3RoaXMkcHJvcHMzLmV4Y2x1c2l2ZSxcbiAgICAgICAgICAgIHRyYW5zaXRpb25BcHBlYXIgPSBfdGhpcyRwcm9wczMudHJhbnNpdGlvbkFwcGVhcixcbiAgICAgICAgICAgIHRyYW5zaXRpb25FbnRlciA9IF90aGlzJHByb3BzMy50cmFuc2l0aW9uRW50ZXIsXG4gICAgICAgICAgICB0cmFuc2l0aW9uTGVhdmUgPSBfdGhpcyRwcm9wczMudHJhbnNpdGlvbkxlYXZlO1xuXG5cbiAgICAgICAgZnVuY3Rpb24gaGFzRXZlbnRIYW5kbGVyKGV2ZW50VHlwZSkge1xuICAgICAgICAgIHJldHVybiBldmVudFR5cGUgPT09ICdhcHBlYXInICYmICh0cmFuc2l0aW9uQXBwZWFyIHx8IGFuaW1hdGlvbi5hcHBlYXIpIHx8IGV2ZW50VHlwZSA9PT0gJ2VudGVyJyAmJiAodHJhbnNpdGlvbkVudGVyIHx8IGFuaW1hdGlvbi5lbnRlcikgfHwgZXZlbnRUeXBlID09PSAnbGVhdmUnICYmICh0cmFuc2l0aW9uTGVhdmUgfHwgYW5pbWF0aW9uLmxlYXZlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBldmVudCA9IG51bGw7XG4gICAgICAgIC8vIElmIGlzIGV4Y2x1c2l2ZSwgb25seSBjaGVjayB0aGUgbGFzdCBldmVudFxuICAgICAgICBpZiAoZXhjbHVzaXZlKSB7XG4gICAgICAgICAgdmFyIGV2ZW50VHlwZSA9IGV2ZW50UXVldWVbZXZlbnRRdWV1ZS5sZW5ndGggLSAxXTtcbiAgICAgICAgICBpZiAoaGFzRXZlbnRIYW5kbGVyKGV2ZW50VHlwZSkpIHtcbiAgICAgICAgICAgIGV2ZW50ID0ge1xuICAgICAgICAgICAgICBldmVudFR5cGU6IGV2ZW50VHlwZSxcbiAgICAgICAgICAgICAgcmVzdFF1ZXVlOiBbXVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gTG9vcCBjaGVjayB0aGUgcXVldWUgdW50aWwgZmluZCBtYXRjaFxuICAgICAgICAgIHZhciBjbG9uZVF1ZXVlID0gZXZlbnRRdWV1ZS5zbGljZSgpO1xuICAgICAgICAgIHdoaWxlIChjbG9uZVF1ZXVlLmxlbmd0aCkge1xuICAgICAgICAgICAgdmFyIF9jbG9uZVF1ZXVlID0gY2xvbmVRdWV1ZSxcbiAgICAgICAgICAgICAgICBfY2xvbmVRdWV1ZTIgPSBfdG9BcnJheShfY2xvbmVRdWV1ZSksXG4gICAgICAgICAgICAgICAgX2V2ZW50VHlwZSA9IF9jbG9uZVF1ZXVlMlswXSxcbiAgICAgICAgICAgICAgICByZXN0UXVldWUgPSBfY2xvbmVRdWV1ZTIuc2xpY2UoMSk7XG5cbiAgICAgICAgICAgIGlmIChoYXNFdmVudEhhbmRsZXIoX2V2ZW50VHlwZSkpIHtcbiAgICAgICAgICAgICAgZXZlbnQgPSB7XG4gICAgICAgICAgICAgICAgZXZlbnRUeXBlOiBfZXZlbnRUeXBlLFxuICAgICAgICAgICAgICAgIHJlc3RRdWV1ZTogcmVzdFF1ZXVlXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xvbmVRdWV1ZSA9IHJlc3RRdWV1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIWV2ZW50KSB7XG4gICAgICAgICAgZXZlbnQgPSB7XG4gICAgICAgICAgICBlbXB0eTogdHJ1ZSxcbiAgICAgICAgICAgIGxhc3RFdmVudFR5cGU6IGV2ZW50UXVldWVbZXZlbnRRdWV1ZS5sZW5ndGggLSAxXVxuICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5uZXh0RXZlbnQgPSBmdW5jdGlvbiAocmVzdFF1ZXVlKSB7XG4gICAgICAgIC8vIE5leHQgcXVldWVcbiAgICAgICAgaWYgKCFfdGhpcy5fZGVzdHJveSkge1xuICAgICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGV2ZW50UXVldWU6IHJlc3RRdWV1ZSxcbiAgICAgICAgICAgIGV2ZW50QWN0aXZlOiBmYWxzZVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5jbGVhbkRvbUV2ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoX3RoaXMuJHByZXZFbGUgJiYgdHJhbnNpdGlvblN1cHBvcnQpIHtcbiAgICAgICAgICBfdGhpcy4kcHJldkVsZS5yZW1vdmVFdmVudExpc3RlbmVyKGFuaW1hdGlvbkVuZE5hbWUsIF90aGlzLm9uTW90aW9uRW5kKTtcbiAgICAgICAgICBfdGhpcy4kcHJldkVsZS5yZW1vdmVFdmVudExpc3RlbmVyKHRyYW5zaXRpb25FbmROYW1lLCBfdGhpcy5vbk1vdGlvbkVuZCk7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIF90aGlzLiRwcmV2RWxlID0gbnVsbDtcblxuICAgICAgX3RoaXMuY3VycmVudEV2ZW50ID0gbnVsbDtcbiAgICAgIF90aGlzLnRpbWVvdXQgPSBudWxsO1xuICAgICAgcmV0dXJuIF90aGlzO1xuICAgIH1cblxuICAgIF9jcmVhdGVDbGFzcyhBbmltYXRlQ2hpbGQsIFt7XG4gICAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIHRoaXMub25Eb21VcGRhdGVkKCk7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICAgIHRoaXMub25Eb21VcGRhdGVkKCk7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcbiAgICAgICAgdGhpcy5fZGVzdHJveSA9IHRydWU7XG4gICAgICAgIHRoaXMuY2xlYW5Eb21FdmVudCgpO1xuICAgICAgfVxuICAgIH0sIHtcbiAgICAgIGtleTogJ3JlbmRlcicsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICB2YXIgX3N0YXRlID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICAgIGNoaWxkID0gX3N0YXRlLmNoaWxkLFxuICAgICAgICAgICAgZXZlbnRBY3RpdmUgPSBfc3RhdGUuZXZlbnRBY3RpdmU7XG4gICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgc2hvd1Byb3AgPSBfcHJvcHMuc2hvd1Byb3AsXG4gICAgICAgICAgICB0cmFuc2l0aW9uTmFtZSA9IF9wcm9wcy50cmFuc2l0aW9uTmFtZTtcblxuICAgICAgICB2YXIgX3JlZjIgPSBjaGlsZC5wcm9wcyB8fCB7fSxcbiAgICAgICAgICAgIGNsYXNzTmFtZSA9IF9yZWYyLmNsYXNzTmFtZTtcblxuICAgICAgICB2YXIgY3VycmVudEV2ZW50ID0gdGhpcy5nZXRDdXJyZW50RXZlbnQoKTtcblxuICAgICAgICAvLyBDbGFzcyBuYW1lXG4gICAgICAgIHZhciBjb25uZWN0Q2xhc3NOYW1lID0gdHJhbnNpdGlvblN1cHBvcnQgJiYgdGhpcy5jdXJyZW50RXZlbnQgPyBjbGFzc05hbWVzKGNsYXNzTmFtZSwgZ2V0VHJhbnNpdGlvbk5hbWUodHJhbnNpdGlvbk5hbWUsIHRoaXMuY3VycmVudEV2ZW50LnR5cGUpLCBldmVudEFjdGl2ZSAmJiBnZXRUcmFuc2l0aW9uTmFtZSh0cmFuc2l0aW9uTmFtZSwgdGhpcy5jdXJyZW50RXZlbnQudHlwZSArICctYWN0aXZlJykpIDogY2xhc3NOYW1lO1xuXG4gICAgICAgIHZhciBzaG93ID0gdHJ1ZTtcblxuICAgICAgICAvLyBLZWVwIHNob3cgd2hlbiBpcyBpbiB0cmFuc2l0aW9uIG9yIGhhcyBjdXN0b21pemUgYW5pbWF0ZVxuICAgICAgICBpZiAodHJhbnNpdGlvblN1cHBvcnQgJiYgKCFjdXJyZW50RXZlbnQuZW1wdHkgfHwgdGhpcy5jdXJyZW50RXZlbnQgJiYgdGhpcy5jdXJyZW50RXZlbnQuYW5pbWF0ZU9iaikpIHtcbiAgICAgICAgICBzaG93ID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBzaG93ID0gY2hpbGQucHJvcHNbc2hvd1Byb3BdO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQ2xvbmUgY2hpbGRcbiAgICAgICAgdmFyIG5ld0NoaWxkUHJvcHMgPSB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjb25uZWN0Q2xhc3NOYW1lXG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHNob3dQcm9wKSB7XG4gICAgICAgICAgbmV3Q2hpbGRQcm9wc1tzaG93UHJvcF0gPSBzaG93O1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChjaGlsZCwgbmV3Q2hpbGRQcm9wcyk7XG4gICAgICB9XG4gICAgfV0sIFt7XG4gICAgICBrZXk6ICdnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgICB2YXIgX3ByZXZTdGF0ZSRwcmV2UHJvcHMgPSBwcmV2U3RhdGUucHJldlByb3BzLFxuICAgICAgICAgICAgcHJldlByb3BzID0gX3ByZXZTdGF0ZSRwcmV2UHJvcHMgPT09IHVuZGVmaW5lZCA/IHt9IDogX3ByZXZTdGF0ZSRwcmV2UHJvcHM7XG4gICAgICAgIHZhciBhcHBlYXJlZCA9IG5leHRQcm9wcy5hcHBlYXJlZDtcblxuXG4gICAgICAgIHZhciBuZXdTdGF0ZSA9IHtcbiAgICAgICAgICBwcmV2UHJvcHM6IGNsb25lUHJvcHMobmV4dFByb3BzLCBjbG9uZVByb3BMaXN0KVxuICAgICAgICB9O1xuXG4gICAgICAgIGZ1bmN0aW9uIHByb2Nlc3NTdGF0ZShwcm9wTmFtZSwgdXBkYXRlcikge1xuICAgICAgICAgIGlmIChwcmV2UHJvcHNbcHJvcE5hbWVdICE9PSBuZXh0UHJvcHNbcHJvcE5hbWVdKSB7XG4gICAgICAgICAgICBpZiAodXBkYXRlcikge1xuICAgICAgICAgICAgICB1cGRhdGVyKG5leHRQcm9wc1twcm9wTmFtZV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHB1c2hFdmVudChldmVudFR5cGUpIHtcbiAgICAgICAgICB2YXIgZXZlbnRRdWV1ZSA9IG5ld1N0YXRlLmV2ZW50UXVldWUgfHwgcHJldlN0YXRlLmV2ZW50UXVldWUuc2xpY2UoKTtcbiAgICAgICAgICB2YXIgbWF0Y2hJbmRleCA9IGV2ZW50UXVldWUuaW5kZXhPZihldmVudFR5cGUpO1xuXG4gICAgICAgICAgLy8gQ2xlYW4gdGhlIHJlc3QgZXZlbnQgaWYgZXZlbnRUeXBlIG1hdGNoXG4gICAgICAgICAgaWYgKG1hdGNoSW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICBldmVudFF1ZXVlID0gZXZlbnRRdWV1ZS5zbGljZSgwLCBtYXRjaEluZGV4KTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBldmVudFF1ZXVlLnB1c2goZXZlbnRUeXBlKTtcbiAgICAgICAgICBuZXdTdGF0ZS5ldmVudFF1ZXVlID0gZXZlbnRRdWV1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIENoaWxkIHVwZGF0ZS4gT25seSBzZXQgY2hpbGQuXG4gICAgICAgIHByb2Nlc3NTdGF0ZSgnY2hpbGRyZW4nLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICBuZXdTdGF0ZS5jaGlsZCA9IGNoaWxkO1xuICAgICAgICB9KTtcblxuICAgICAgICBwcm9jZXNzU3RhdGUoJ2FwcGVhcmVkJywgZnVuY3Rpb24gKGlzQXBwZWFyZWQpIHtcbiAgICAgICAgICBpZiAoaXNBcHBlYXJlZCkge1xuICAgICAgICAgICAgcHVzaEV2ZW50KCdhcHBlYXInKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIFNob3cgdXBkYXRlXG4gICAgICAgIHByb2Nlc3NTdGF0ZSgnc2hvdycsIGZ1bmN0aW9uIChzaG93KSB7XG4gICAgICAgICAgaWYgKCFhcHBlYXJlZCkge1xuICAgICAgICAgICAgaWYgKHNob3cpIHtcbiAgICAgICAgICAgICAgcHVzaEV2ZW50KCdlbnRlcicpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcHVzaEV2ZW50KCdsZWF2ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIG5ld1N0YXRlO1xuICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBBbmltYXRlQ2hpbGQ7XG4gIH0oUmVhY3QuQ29tcG9uZW50KTtcblxuICBBbmltYXRlQ2hpbGQucHJvcFR5cGVzID0ge1xuICAgIHRyYW5zaXRpb25OYW1lOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMub2JqZWN0XSksXG4gICAgdHJhbnNpdGlvbkFwcGVhcjogUHJvcFR5cGVzLmJvb2wsXG4gICAgdHJhbnNpdGlvbkVudGVyOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0cmFuc2l0aW9uTGVhdmU6IFByb3BUeXBlcy5ib29sLFxuICAgIGV4Y2x1c2l2ZTogUHJvcFR5cGVzLmJvb2wsXG4gICAgYXBwZWFyZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHNob3dQcm9wOiBQcm9wVHlwZXMuc3RyaW5nLFxuXG4gICAgYW5pbWF0ZUtleTogUHJvcFR5cGVzLmFueSxcbiAgICBhbmltYXRpb246IFByb3BUeXBlcy5vYmplY3QsXG4gICAgb25DaGlsZExlYXZlZDogUHJvcFR5cGVzLmZ1bmMsXG5cbiAgICBvbkVuZDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgb25BcHBlYXI6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uRW50ZXI6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uTGVhdmU6IFByb3BUeXBlcy5mdW5jXG4gIH07XG5cblxuICBwb2x5ZmlsbChBbmltYXRlQ2hpbGQpO1xuXG4gIHJldHVybiBBbmltYXRlQ2hpbGQ7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdlbkFuaW1hdGVDaGlsZChzdXBwb3J0VHJhbnNpdGlvbik7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBOzs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFRQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBckNBO0FBdUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFHQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQTFEQTtBQUNBO0FBNERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQWhCQTtBQW9CQTtBQUVBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/node_modules/rc-trigger/node_modules/rc-animate/es/AnimateChild.js
