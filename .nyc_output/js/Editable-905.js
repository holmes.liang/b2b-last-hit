__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _input_TextArea__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../input/TextArea */ "./node_modules/antd/es/input/TextArea.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}








var Editable =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Editable, _React$Component);

  function Editable() {
    var _this;

    _classCallCheck(this, Editable);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Editable).apply(this, arguments));
    _this.inComposition = false;
    _this.state = {
      current: ''
    };

    _this.onChange = function (_ref) {
      var value = _ref.target.value;

      _this.setState({
        current: value.replace(/[\r\n]/g, '')
      });
    };

    _this.onCompositionStart = function () {
      _this.inComposition = true;
    };

    _this.onCompositionEnd = function () {
      _this.inComposition = false;
    };

    _this.onKeyDown = function (_ref2) {
      var keyCode = _ref2.keyCode; // We don't record keyCode when IME is using

      if (_this.inComposition) return;
      _this.lastKeyCode = keyCode;
    };

    _this.onKeyUp = function (_ref3) {
      var keyCode = _ref3.keyCode,
          ctrlKey = _ref3.ctrlKey,
          altKey = _ref3.altKey,
          metaKey = _ref3.metaKey,
          shiftKey = _ref3.shiftKey;
      var onCancel = _this.props.onCancel; // Check if it's a real key

      if (_this.lastKeyCode === keyCode && !_this.inComposition && !ctrlKey && !altKey && !metaKey && !shiftKey) {
        if (keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_1__["default"].ENTER) {
          _this.confirmChange();
        } else if (keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_1__["default"].ESC) {
          onCancel();
        }
      }
    };

    _this.onBlur = function () {
      _this.confirmChange();
    };

    _this.confirmChange = function () {
      var current = _this.state.current;
      var onSave = _this.props.onSave;
      onSave(current.trim());
    };

    _this.setTextarea = function (textarea) {
      _this.textarea = textarea;
    };

    return _this;
  }

  _createClass(Editable, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.textarea) {
        this.textarea.focus();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var current = this.state.current;
      var _this$props = this.props,
          prefixCls = _this$props.prefixCls,
          ariaLabel = _this$props['aria-label'],
          className = _this$props.className,
          style = _this$props.style;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, "".concat(prefixCls, "-edit-content"), className),
        style: style
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_input_TextArea__WEBPACK_IMPORTED_MODULE_5__["default"], {
        ref: this.setTextarea,
        value: current,
        onChange: this.onChange,
        onKeyDown: this.onKeyDown,
        onKeyUp: this.onKeyUp,
        onCompositionStart: this.onCompositionStart,
        onCompositionEnd: this.onCompositionEnd,
        onBlur: this.onBlur,
        "aria-label": ariaLabel,
        autoSize: true
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        type: "enter",
        className: "".concat(prefixCls, "-edit-content-confirm")
      }));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var prevValue = prevState.prevValue;
      var value = nextProps.value;
      var newState = {
        prevValue: value
      };

      if (prevValue !== value) {
        newState.current = value;
      }

      return newState;
    }
  }]);

  return Editable;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(Editable);
/* harmony default export */ __webpack_exports__["default"] = (Editable);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L0VkaXRhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90eXBvZ3JhcGh5L0VkaXRhYmxlLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgS2V5Q29kZSBmcm9tICdyYy11dGlsL2xpYi9LZXlDb2RlJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCBUZXh0QXJlYSBmcm9tICcuLi9pbnB1dC9UZXh0QXJlYSc7XG5jbGFzcyBFZGl0YWJsZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuaW5Db21wb3NpdGlvbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgICAgICAgY3VycmVudDogJycsXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25DaGFuZ2UgPSAoeyB0YXJnZXQ6IHsgdmFsdWUgfSB9KSA9PiB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgY3VycmVudDogdmFsdWUucmVwbGFjZSgvW1xcclxcbl0vZywgJycpIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uQ29tcG9zaXRpb25TdGFydCA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5Db21wb3NpdGlvbiA9IHRydWU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25Db21wb3NpdGlvbkVuZCA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5Db21wb3NpdGlvbiA9IGZhbHNlO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uS2V5RG93biA9ICh7IGtleUNvZGUgfSkgPT4ge1xuICAgICAgICAgICAgLy8gV2UgZG9uJ3QgcmVjb3JkIGtleUNvZGUgd2hlbiBJTUUgaXMgdXNpbmdcbiAgICAgICAgICAgIGlmICh0aGlzLmluQ29tcG9zaXRpb24pXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgdGhpcy5sYXN0S2V5Q29kZSA9IGtleUNvZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25LZXlVcCA9ICh7IGtleUNvZGUsIGN0cmxLZXksIGFsdEtleSwgbWV0YUtleSwgc2hpZnRLZXksIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25DYW5jZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICAvLyBDaGVjayBpZiBpdCdzIGEgcmVhbCBrZXlcbiAgICAgICAgICAgIGlmICh0aGlzLmxhc3RLZXlDb2RlID09PSBrZXlDb2RlICYmXG4gICAgICAgICAgICAgICAgIXRoaXMuaW5Db21wb3NpdGlvbiAmJlxuICAgICAgICAgICAgICAgICFjdHJsS2V5ICYmXG4gICAgICAgICAgICAgICAgIWFsdEtleSAmJlxuICAgICAgICAgICAgICAgICFtZXRhS2V5ICYmXG4gICAgICAgICAgICAgICAgIXNoaWZ0S2V5KSB7XG4gICAgICAgICAgICAgICAgaWYgKGtleUNvZGUgPT09IEtleUNvZGUuRU5URVIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25maXJtQ2hhbmdlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKGtleUNvZGUgPT09IEtleUNvZGUuRVNDKSB7XG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uQmx1ciA9ICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY29uZmlybUNoYW5nZSgpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmNvbmZpcm1DaGFuZ2UgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGN1cnJlbnQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCB7IG9uU2F2ZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIG9uU2F2ZShjdXJyZW50LnRyaW0oKSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2V0VGV4dGFyZWEgPSAodGV4dGFyZWEpID0+IHtcbiAgICAgICAgICAgIHRoaXMudGV4dGFyZWEgPSB0ZXh0YXJlYTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgICBjb25zdCB7IHByZXZWYWx1ZSB9ID0gcHJldlN0YXRlO1xuICAgICAgICBjb25zdCB7IHZhbHVlIH0gPSBuZXh0UHJvcHM7XG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xuICAgICAgICAgICAgcHJldlZhbHVlOiB2YWx1ZSxcbiAgICAgICAgfTtcbiAgICAgICAgaWYgKHByZXZWYWx1ZSAhPT0gdmFsdWUpIHtcbiAgICAgICAgICAgIG5ld1N0YXRlLmN1cnJlbnQgPSB2YWx1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbmV3U3RhdGU7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICBpZiAodGhpcy50ZXh0YXJlYSkge1xuICAgICAgICAgICAgdGhpcy50ZXh0YXJlYS5mb2N1cygpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgeyBjdXJyZW50IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCB7IHByZWZpeENscywgJ2FyaWEtbGFiZWwnOiBhcmlhTGFiZWwsIGNsYXNzTmFtZSwgc3R5bGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZXMocHJlZml4Q2xzLCBgJHtwcmVmaXhDbHN9LWVkaXQtY29udGVudGAsIGNsYXNzTmFtZSl9IHN0eWxlPXtzdHlsZX0+XG4gICAgICAgIDxUZXh0QXJlYSByZWY9e3RoaXMuc2V0VGV4dGFyZWF9IHZhbHVlPXtjdXJyZW50fSBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX0gb25LZXlEb3duPXt0aGlzLm9uS2V5RG93bn0gb25LZXlVcD17dGhpcy5vbktleVVwfSBvbkNvbXBvc2l0aW9uU3RhcnQ9e3RoaXMub25Db21wb3NpdGlvblN0YXJ0fSBvbkNvbXBvc2l0aW9uRW5kPXt0aGlzLm9uQ29tcG9zaXRpb25FbmR9IG9uQmx1cj17dGhpcy5vbkJsdXJ9IGFyaWEtbGFiZWw9e2FyaWFMYWJlbH0gYXV0b1NpemUvPlxuICAgICAgICA8SWNvbiB0eXBlPVwiZW50ZXJcIiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tZWRpdC1jb250ZW50LWNvbmZpcm1gfS8+XG4gICAgICA8L2Rpdj4pO1xuICAgIH1cbn1cbnBvbHlmaWxsKEVkaXRhYmxlKTtcbmV4cG9ydCBkZWZhdWx0IEVkaXRhYmxlO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUpBO0FBQ0E7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFNQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFmQTtBQUNBO0FBZ0JBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBR0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBQ0E7QUEvQ0E7QUFpREE7QUFDQTs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTs7O0FBdkJBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBN0RBO0FBQ0E7QUEyRUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/Editable.js
