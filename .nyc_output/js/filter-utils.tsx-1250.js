__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _model_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./model-utils */ "./src/data-model/model-utils.tsx");





var Filter =
/*#__PURE__*/
function () {
  function Filter() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Filter);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Filter, null, [{
    key: "isFilterRulePositive",
    value: function isFilterRulePositive(options) {
      var root = options.root,
          model = options.model,
          arrayHolder = options.arrayHolder,
          rule = options.rule,
          caller = options.caller;

      if (rule === "--d") {
        return true;
      } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isString(rule)) {
        var exp = _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].parseFilter(rule);

        if (_model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].isSimpleExpression(exp) && _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].isFromRoot(exp.prop)) {
          return _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].matchFilter(root, {
            prop: _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].getRealId(exp.prop),
            operator: exp.operator,
            value: exp.value
          });
        } else {
          return _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].matchFilter(model, exp);
        }
      } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isFunction(rule)) {
        return rule.call(caller, caller, {
          root: root,
          model: model,
          arrayHolder: arrayHolder,
          rules: [rule],
          caller: caller
        });
      } else if (rule.and) {
        return Filter.shouldApplyFilter({
          root: root,
          model: model,
          arrayHolder: arrayHolder,
          rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(rule.and),
          relation: "and",
          caller: caller
        });
      } else if (rule.or) {
        return Filter.shouldApplyFilter({
          root: root,
          model: model,
          arrayHolder: arrayHolder,
          rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(rule.or),
          relation: "or",
          caller: caller
        });
      } else {
        console.error(options);
        throw new Error("Unsupported rule type.");
      }
    }
  }, {
    key: "shouldApplyFilter",
    value: function shouldApplyFilter(options) {
      var root = options.root,
          model = options.model,
          arrayHolder = options.arrayHolder,
          rules = options.rules,
          caller = options.caller,
          _options$relation = options.relation,
          relation = _options$relation === void 0 ? "and" : _options$relation;

      switch (relation) {
        case "and":
          return rules.find(function (rule) {
            var positive = Filter.isFilterRulePositive({
              root: root,
              model: model,
              arrayHolder: arrayHolder,
              rule: rule,
              caller: caller
            });
            return !positive;
          }) == undefined;

        case "or":
          return rules.find(function (rule) {
            var positive = Filter.isFilterRulePositive({
              root: root,
              model: model,
              arrayHolder: arrayHolder,
              rule: rule,
              caller: caller
            });
            return positive;
          }) != null;

        default:
          console.log(options);
          throw new Error("Unsupported relationship[".concat(relation, "]."));
      }
    }
  }, {
    key: "findMatchedFilter",
    value: function findMatchedFilter(options) {
      var filters = options.filters,
          model = options.model,
          arrayHolder = options.arrayHolder,
          root = options.root,
          caller = options.caller;
      return filters.find(function (filter) {
        var rules = filter.on;

        if (!rules) {
          console.error(options);
          throw new Error("There is no rule defined of filter.");
        } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isString(rules)) {
          return Filter.shouldApplyFilter({
            root: root,
            model: model,
            arrayHolder: arrayHolder,
            rules: [rules],
            caller: caller
          });
        } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isFunction(rules)) {
          return rules.call(caller, caller, {
            root: root,
            model: model,
            arrayHolder: arrayHolder,
            rules: [rules],
            caller: caller
          });
        } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isArray(rules)) {
          return Filter.shouldApplyFilter({
            root: root,
            model: model,
            arrayHolder: arrayHolder,
            rules: rules,
            caller: caller
          });
        } else if (rules.and) {
          var subRules = rules.and;
          return Filter.shouldApplyFilter({
            root: root,
            model: model,
            arrayHolder: arrayHolder,
            rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(subRules),
            caller: caller,
            relation: "and"
          });
        } else if (rules.or) {
          var _subRules = rules.or;
          return Filter.shouldApplyFilter({
            root: root,
            model: model,
            arrayHolder: arrayHolder,
            rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(_subRules),
            caller: caller,
            relation: "or"
          });
        } else {
          console.error(filter);
          throw new Error("Unsupported filter type.");
        }
      });
    }
  }, {
    key: "getConcernedIdsOfFilters",
    value: function getConcernedIdsOfFilters(options) {
      var filters = _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(options.filters);
      return filters.reduce(function (ids, filter) {
        var rules = filter.on;

        if (!rules) {
          throw "There is no rule defined of filter on property[".concat(filter, "]");
        } else {
          var filterIds = Filter.getConcernedIdsOfRules({
            root: options.root,
            model: options.model,
            rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(rules)
          });
          Array.prototype.push.apply(ids, filterIds);
        }

        return ids;
      }, []) || [];
    }
  }, {
    key: "getConcernedIdsOfRules",
    value: function getConcernedIdsOfRules(options) {
      var root = options.root,
          model = options.model,
          rules = options.rules;
      return rules.reduce(function (all, rule) {
        if (rule === "--d") {} else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isString(rule)) {
          var exp = _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].parseFilter(rule);

          if (exp.prop) {
            // FilterExpressionSimple
            var fes = exp;
            all.push({
              propName: _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].getRealId(fes.prop),
              model: _model_utils__WEBPACK_IMPORTED_MODULE_3__["default"].isFromRoot(fes.prop) ? root : model
            });
          }
        } else if (_common__WEBPACK_IMPORTED_MODULE_2__["Utils"].isFunction(rule)) {//do nothing
        } else {
          var sub = Filter.getConcernedIdsOfRules({
            root: root,
            model: model,
            rules: _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].toArray(rule.and || rule.or)
          });
          Array.prototype.push.apply(all, sub);
        }

        return all;
      }, []);
    }
  }]);

  return Filter;
}();

/* harmony default export */ __webpack_exports__["default"] = (Filter);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZGF0YS1tb2RlbC9maWx0ZXItdXRpbHMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvZGF0YS1tb2RlbC9maWx0ZXItdXRpbHMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IENvbXBsZXhGaWx0ZXJSdWxlLCBGaWx0ZXJhYmxlLCBGaWx0ZXJFeHByZXNzaW9uU2ltcGxlLCBGaWx0ZXJSdWxlRnVuY3Rpb24gfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgTW9kZWxsZXIgZnJvbSBcIi4vbW9kZWwtdXRpbHNcIjtcblxudHlwZSBSdWxlT3B0aW9ucyA9IHtcbiAgcnVsZTogXCItLWRcIiB8IHN0cmluZyB8IEZpbHRlclJ1bGVGdW5jdGlvbiB8IENvbXBsZXhGaWx0ZXJSdWxlO1xuICBtb2RlbDogYW55O1xuICByb290OiBhbnk7XG4gIGFycmF5SG9sZGVyPzogYW55O1xuICBjYWxsZXI6IGFueTtcbiAgcmVsYXRpb24/OiBcImFuZFwiIHwgXCJvclwiO1xufTtcbnR5cGUgUnVsZXNPcHRpb25zID0ge1xuICBydWxlczogKHN0cmluZyB8IEZpbHRlclJ1bGVGdW5jdGlvbiB8IENvbXBsZXhGaWx0ZXJSdWxlKVtdO1xuICBtb2RlbDogYW55O1xuICByb290OiBhbnk7XG4gIGFycmF5SG9sZGVyPzogYW55O1xuICBjYWxsZXI6IGFueTtcbiAgcmVsYXRpb24/OiBcImFuZFwiIHwgXCJvclwiO1xufTtcbnR5cGUgRmlsdGVyT3B0aW9ucyA9IHtcbiAgZmlsdGVyczogRmlsdGVyYWJsZVtdO1xuICBtb2RlbDogYW55O1xuICByb290OiBhbnk7XG4gIGFycmF5SG9sZGVyPzogYW55O1xuICBjYWxsZXI6IGFueTtcbn07XG5cbmNsYXNzIEZpbHRlciB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgaXNGaWx0ZXJSdWxlUG9zaXRpdmUob3B0aW9uczogUnVsZU9wdGlvbnMpOiBib29sZWFuIHtcbiAgICBjb25zdCB7IHJvb3QsIG1vZGVsLCBhcnJheUhvbGRlciwgcnVsZSwgY2FsbGVyIH0gPSBvcHRpb25zO1xuXG4gICAgaWYgKHJ1bGUgPT09IFwiLS1kXCIpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNTdHJpbmcocnVsZSkpIHtcbiAgICAgIGNvbnN0IGV4cCA9IE1vZGVsbGVyLnBhcnNlRmlsdGVyKHJ1bGUpO1xuICAgICAgaWYgKE1vZGVsbGVyLmlzU2ltcGxlRXhwcmVzc2lvbihleHApICYmIE1vZGVsbGVyLmlzRnJvbVJvb3QoZXhwLnByb3ApKSB7XG4gICAgICAgIHJldHVybiBNb2RlbGxlci5tYXRjaEZpbHRlcihyb290LCB7XG4gICAgICAgICAgcHJvcDogTW9kZWxsZXIuZ2V0UmVhbElkKGV4cC5wcm9wKSxcbiAgICAgICAgICBvcGVyYXRvcjogZXhwLm9wZXJhdG9yLFxuICAgICAgICAgIHZhbHVlOiBleHAudmFsdWUsXG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIE1vZGVsbGVyLm1hdGNoRmlsdGVyKG1vZGVsLCBleHApO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNGdW5jdGlvbihydWxlKSkge1xuICAgICAgcmV0dXJuIChydWxlIGFzIEZpbHRlclJ1bGVGdW5jdGlvbikuY2FsbChjYWxsZXIsIGNhbGxlciwge1xuICAgICAgICByb290LFxuICAgICAgICBtb2RlbCxcbiAgICAgICAgYXJyYXlIb2xkZXIsXG4gICAgICAgIHJ1bGVzOiBbcnVsZV0sXG4gICAgICAgIGNhbGxlcixcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAocnVsZS5hbmQpIHtcbiAgICAgIHJldHVybiBGaWx0ZXIuc2hvdWxkQXBwbHlGaWx0ZXIoe1xuICAgICAgICByb290LFxuICAgICAgICBtb2RlbCxcbiAgICAgICAgYXJyYXlIb2xkZXIsXG4gICAgICAgIHJ1bGVzOiBVdGlscy50b0FycmF5KHJ1bGUuYW5kKSxcbiAgICAgICAgcmVsYXRpb246IFwiYW5kXCIsXG4gICAgICAgIGNhbGxlcixcbiAgICAgIH0pO1xuICAgIH0gZWxzZSBpZiAocnVsZS5vcikge1xuICAgICAgcmV0dXJuIEZpbHRlci5zaG91bGRBcHBseUZpbHRlcih7XG4gICAgICAgIHJvb3QsXG4gICAgICAgIG1vZGVsLFxuICAgICAgICBhcnJheUhvbGRlcixcbiAgICAgICAgcnVsZXM6IFV0aWxzLnRvQXJyYXkocnVsZS5vciksXG4gICAgICAgIHJlbGF0aW9uOiBcIm9yXCIsXG4gICAgICAgIGNhbGxlcixcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmVycm9yKG9wdGlvbnMpO1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBVbnN1cHBvcnRlZCBydWxlIHR5cGUuYCk7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBzdGF0aWMgc2hvdWxkQXBwbHlGaWx0ZXIob3B0aW9uczogUnVsZXNPcHRpb25zKTogYm9vbGVhbiB7XG4gICAgY29uc3QgeyByb290LCBtb2RlbCwgYXJyYXlIb2xkZXIsIHJ1bGVzLCBjYWxsZXIsIHJlbGF0aW9uID0gXCJhbmRcIiB9ID0gb3B0aW9ucztcblxuICAgIHN3aXRjaCAocmVsYXRpb24pIHtcbiAgICAgIGNhc2UgXCJhbmRcIjpcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICBydWxlcy5maW5kKHJ1bGUgPT4ge1xuICAgICAgICAgICAgY29uc3QgcG9zaXRpdmUgPSBGaWx0ZXIuaXNGaWx0ZXJSdWxlUG9zaXRpdmUoe1xuICAgICAgICAgICAgICByb290LFxuICAgICAgICAgICAgICBtb2RlbCxcbiAgICAgICAgICAgICAgYXJyYXlIb2xkZXIsXG4gICAgICAgICAgICAgIHJ1bGUsXG4gICAgICAgICAgICAgIGNhbGxlcixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuICFwb3NpdGl2ZTtcbiAgICAgICAgICB9KSA9PSB1bmRlZmluZWRcbiAgICAgICAgKTtcbiAgICAgIGNhc2UgXCJvclwiOlxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgIHJ1bGVzLmZpbmQocnVsZSA9PiB7XG4gICAgICAgICAgICBjb25zdCBwb3NpdGl2ZSA9IEZpbHRlci5pc0ZpbHRlclJ1bGVQb3NpdGl2ZSh7XG4gICAgICAgICAgICAgIHJvb3QsXG4gICAgICAgICAgICAgIG1vZGVsLFxuICAgICAgICAgICAgICBhcnJheUhvbGRlcixcbiAgICAgICAgICAgICAgcnVsZSxcbiAgICAgICAgICAgICAgY2FsbGVyLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gcG9zaXRpdmU7XG4gICAgICAgICAgfSkgIT0gbnVsbFxuICAgICAgICApO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgY29uc29sZS5sb2cob3B0aW9ucyk7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihgVW5zdXBwb3J0ZWQgcmVsYXRpb25zaGlwWyR7cmVsYXRpb259XS5gKTtcbiAgICB9XG4gIH1cblxuICBzdGF0aWMgZmluZE1hdGNoZWRGaWx0ZXIob3B0aW9uczogRmlsdGVyT3B0aW9ucyk6IEZpbHRlcmFibGUgfCB1bmRlZmluZWQge1xuICAgIGNvbnN0IHsgZmlsdGVycywgbW9kZWwsIGFycmF5SG9sZGVyLCByb290LCBjYWxsZXIgfSA9IG9wdGlvbnM7XG5cbiAgICByZXR1cm4gZmlsdGVycy5maW5kKChmaWx0ZXI6IEZpbHRlcmFibGUpID0+IHtcbiAgICAgIGNvbnN0IHJ1bGVzID0gZmlsdGVyLm9uO1xuICAgICAgaWYgKCFydWxlcykge1xuICAgICAgICBjb25zb2xlLmVycm9yKG9wdGlvbnMpO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJUaGVyZSBpcyBubyBydWxlIGRlZmluZWQgb2YgZmlsdGVyLlwiKTtcbiAgICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNTdHJpbmcocnVsZXMpKSB7XG4gICAgICAgIHJldHVybiBGaWx0ZXIuc2hvdWxkQXBwbHlGaWx0ZXIoe1xuICAgICAgICAgIHJvb3QsXG4gICAgICAgICAgbW9kZWwsXG4gICAgICAgICAgYXJyYXlIb2xkZXIsXG4gICAgICAgICAgcnVsZXM6IFtydWxlc10sXG4gICAgICAgICAgY2FsbGVyLFxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNGdW5jdGlvbihydWxlcykpIHtcbiAgICAgICAgcmV0dXJuIChydWxlcyBhcyBGaWx0ZXJSdWxlRnVuY3Rpb24pLmNhbGwoY2FsbGVyLCBjYWxsZXIsIHtcbiAgICAgICAgICByb290LFxuICAgICAgICAgIG1vZGVsLFxuICAgICAgICAgIGFycmF5SG9sZGVyLFxuICAgICAgICAgIHJ1bGVzOiBbcnVsZXNdLFxuICAgICAgICAgIGNhbGxlcixcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKFV0aWxzLmlzQXJyYXkocnVsZXMpKSB7XG4gICAgICAgIHJldHVybiBGaWx0ZXIuc2hvdWxkQXBwbHlGaWx0ZXIoe1xuICAgICAgICAgIHJvb3QsXG4gICAgICAgICAgbW9kZWwsXG4gICAgICAgICAgYXJyYXlIb2xkZXIsXG4gICAgICAgICAgcnVsZXMsXG4gICAgICAgICAgY2FsbGVyLFxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAocnVsZXMuYW5kKSB7XG4gICAgICAgIGNvbnN0IHN1YlJ1bGVzID0gcnVsZXMuYW5kO1xuICAgICAgICByZXR1cm4gRmlsdGVyLnNob3VsZEFwcGx5RmlsdGVyKHtcbiAgICAgICAgICByb290LFxuICAgICAgICAgIG1vZGVsLFxuICAgICAgICAgIGFycmF5SG9sZGVyLFxuICAgICAgICAgIHJ1bGVzOiBVdGlscy50b0FycmF5KHN1YlJ1bGVzKSxcbiAgICAgICAgICBjYWxsZXIsXG4gICAgICAgICAgcmVsYXRpb246IFwiYW5kXCIsXG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIGlmIChydWxlcy5vcikge1xuICAgICAgICBjb25zdCBzdWJSdWxlcyA9IHJ1bGVzLm9yO1xuICAgICAgICByZXR1cm4gRmlsdGVyLnNob3VsZEFwcGx5RmlsdGVyKHtcbiAgICAgICAgICByb290LFxuICAgICAgICAgIG1vZGVsLFxuICAgICAgICAgIGFycmF5SG9sZGVyLFxuICAgICAgICAgIHJ1bGVzOiBVdGlscy50b0FycmF5KHN1YlJ1bGVzKSxcbiAgICAgICAgICBjYWxsZXIsXG4gICAgICAgICAgcmVsYXRpb246IFwib3JcIixcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmVycm9yKGZpbHRlcik7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlVuc3VwcG9ydGVkIGZpbHRlciB0eXBlLlwiKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRDb25jZXJuZWRJZHNPZkZpbHRlcnMob3B0aW9uczogRmlsdGVyT3B0aW9ucyk6IHsgcHJvcE5hbWU6IHN0cmluZzsgbW9kZWw6IGFueSB9W10ge1xuICAgIGNvbnN0IGZpbHRlcnMgPSBVdGlscy50b0FycmF5KG9wdGlvbnMuZmlsdGVycyk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgZmlsdGVycy5yZWR1Y2UoKGlkcywgZmlsdGVyKSA9PiB7XG4gICAgICAgIGNvbnN0IHJ1bGVzID0gZmlsdGVyLm9uO1xuICAgICAgICBpZiAoIXJ1bGVzKSB7XG4gICAgICAgICAgdGhyb3cgYFRoZXJlIGlzIG5vIHJ1bGUgZGVmaW5lZCBvZiBmaWx0ZXIgb24gcHJvcGVydHlbJHtmaWx0ZXJ9XWA7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc3QgZmlsdGVySWRzID0gRmlsdGVyLmdldENvbmNlcm5lZElkc09mUnVsZXMoe1xuICAgICAgICAgICAgcm9vdDogb3B0aW9ucy5yb290LFxuICAgICAgICAgICAgbW9kZWw6IG9wdGlvbnMubW9kZWwsXG4gICAgICAgICAgICBydWxlczogVXRpbHMudG9BcnJheShydWxlcyBhcyBhbnkpLFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIEFycmF5LnByb3RvdHlwZS5wdXNoLmFwcGx5KGlkcywgZmlsdGVySWRzKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaWRzO1xuICAgICAgfSwgW10pIHx8IFtdXG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhdGljIGdldENvbmNlcm5lZElkc09mUnVsZXMob3B0aW9uczoge1xuICAgIHJvb3Q6IGFueTtcbiAgICBtb2RlbDogYW55O1xuICAgIHJ1bGVzOiAoc3RyaW5nIHwgRmlsdGVyUnVsZUZ1bmN0aW9uIHwgQ29tcGxleEZpbHRlclJ1bGUpW107XG4gIH0pOiB7IHByb3BOYW1lOiBzdHJpbmc7IG1vZGVsOiBhbnkgfVtdIHtcbiAgICBjb25zdCByb290ID0gb3B0aW9ucy5yb290LFxuICAgICAgbW9kZWwgPSBvcHRpb25zLm1vZGVsLFxuICAgICAgcnVsZXMgPSBvcHRpb25zLnJ1bGVzO1xuXG4gICAgcmV0dXJuIHJ1bGVzLnJlZHVjZShcbiAgICAgIChhbGwsIHJ1bGUpID0+IHtcbiAgICAgICAgaWYgKHJ1bGUgPT09IFwiLS1kXCIpIHtcbiAgICAgICAgfSBlbHNlIGlmIChVdGlscy5pc1N0cmluZyhydWxlKSkge1xuICAgICAgICAgIGNvbnN0IGV4cCA9IE1vZGVsbGVyLnBhcnNlRmlsdGVyKHJ1bGUpO1xuICAgICAgICAgIGlmICgoZXhwIGFzIGFueSkucHJvcCkge1xuICAgICAgICAgICAgLy8gRmlsdGVyRXhwcmVzc2lvblNpbXBsZVxuICAgICAgICAgICAgY29uc3QgZmVzID0gZXhwIGFzIEZpbHRlckV4cHJlc3Npb25TaW1wbGU7XG4gICAgICAgICAgICBhbGwucHVzaCh7XG4gICAgICAgICAgICAgIHByb3BOYW1lOiBNb2RlbGxlci5nZXRSZWFsSWQoZmVzLnByb3ApLFxuICAgICAgICAgICAgICBtb2RlbDogTW9kZWxsZXIuaXNGcm9tUm9vdChmZXMucHJvcCkgPyByb290IDogbW9kZWwsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNGdW5jdGlvbihydWxlKSkge1xuICAgICAgICAgIC8vZG8gbm90aGluZ1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNvbnN0IHN1YiA9IEZpbHRlci5nZXRDb25jZXJuZWRJZHNPZlJ1bGVzKHtcbiAgICAgICAgICAgIHJvb3Q6IHJvb3QsXG4gICAgICAgICAgICBtb2RlbDogbW9kZWwsXG4gICAgICAgICAgICBydWxlczogVXRpbHMudG9BcnJheShydWxlLmFuZCB8fCBydWxlLm9yKSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBBcnJheS5wcm90b3R5cGUucHVzaC5hcHBseShhbGwsIHN1Yik7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFsbDtcbiAgICAgIH0sXG4gICAgICBbXSBhcyB7IHByb3BOYW1lOiBzdHJpbmc7IG1vZGVsOiBhbnkgfVtdLFxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRmlsdGVyO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFFQTtBQUNBO0FBeUJBOzs7Ozs7Ozs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE3QkE7QUErQkE7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7QUFFQTtBQUtBO0FBQUE7QUFBQTtBQUlBO0FBRUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/data-model/filter-utils.tsx
