__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils */ "./node_modules/@ant-design/icons-react/es/utils.js");









var twoToneColorPalette = {
  primaryColor: '#333',
  secondaryColor: '#E6E6E6'
};

var Icon = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_6___default()(Icon, _React$Component);

  function Icon() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Icon);

    return babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default()(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).apply(this, arguments));
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Icon, [{
    key: 'render',
    value: function render() {
      var _extends2;

      var _props = this.props,
          type = _props.type,
          className = _props.className,
          onClick = _props.onClick,
          style = _props.style,
          primaryColor = _props.primaryColor,
          secondaryColor = _props.secondaryColor,
          rest = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default()(_props, ['type', 'className', 'onClick', 'style', 'primaryColor', 'secondaryColor']);

      var target = void 0;
      var colors = twoToneColorPalette;

      if (primaryColor) {
        colors = {
          primaryColor: primaryColor,
          secondaryColor: secondaryColor || Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getSecondaryColor"])(primaryColor)
        };
      }

      if (Object(_utils__WEBPACK_IMPORTED_MODULE_8__["isIconDefinition"])(type)) {
        target = type;
      } else if (typeof type === 'string') {
        target = Icon.get(type, colors);

        if (!target) {
          // log(`Could not find icon: ${type}`);
          return null;
        }
      }

      if (!target) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_8__["log"])('type should be string or icon definiton, but got ' + type);
        return null;
      }

      if (target && typeof target.icon === 'function') {
        target = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, target, {
          icon: target.icon(colors.primaryColor, colors.secondaryColor)
        });
      }

      return Object(_utils__WEBPACK_IMPORTED_MODULE_8__["generate"])(target.icon, 'svg-' + target.name, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()((_extends2 = {
        className: className,
        onClick: onClick,
        style: style
      }, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'data-icon', target.name), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'width', '1em'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'height', '1em'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'fill', 'currentColor'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'aria-hidden', 'true'), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_extends2, 'focusable', 'false'), _extends2), rest));
    }
  }], [{
    key: 'add',
    value: function add() {
      var _this2 = this;

      for (var _len = arguments.length, icons = Array(_len), _key = 0; _key < _len; _key++) {
        icons[_key] = arguments[_key];
      }

      icons.forEach(function (icon) {
        _this2.definitions.set(Object(_utils__WEBPACK_IMPORTED_MODULE_8__["withSuffix"])(icon.name, icon.theme), icon);
      });
    }
  }, {
    key: 'clear',
    value: function clear() {
      this.definitions.clear();
    }
  }, {
    key: 'get',
    value: function get(key) {
      var colors = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : twoToneColorPalette;

      if (key) {
        var target = this.definitions.get(key);

        if (target && typeof target.icon === 'function') {
          target = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, target, {
            icon: target.icon(colors.primaryColor, colors.secondaryColor)
          });
        }

        return target;
      }
    }
  }, {
    key: 'setTwoToneColors',
    value: function setTwoToneColors(_ref) {
      var primaryColor = _ref.primaryColor,
          secondaryColor = _ref.secondaryColor;
      twoToneColorPalette.primaryColor = primaryColor;
      twoToneColorPalette.secondaryColor = secondaryColor || Object(_utils__WEBPACK_IMPORTED_MODULE_8__["getSecondaryColor"])(primaryColor);
    }
  }, {
    key: 'getTwoToneColors',
    value: function getTwoToneColors() {
      return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, twoToneColorPalette);
    }
  }]);

  return Icon;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);

Icon.displayName = 'IconReact';
Icon.definitions = new _utils__WEBPACK_IMPORTED_MODULE_8__["MiniMap"]();
/* harmony default export */ __webpack_exports__["default"] = (Icon);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvQGFudC1kZXNpZ24vaWNvbnMtcmVhY3QvZXMvY29tcG9uZW50cy9JY29uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvQGFudC1kZXNpZ24vaWNvbnMtcmVhY3QvZXMvY29tcG9uZW50cy9JY29uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZGVmaW5lUHJvcGVydHkgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5JztcbmltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX2NyZWF0ZUNsYXNzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcyc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgZ2VuZXJhdGUsIGdldFNlY29uZGFyeUNvbG9yLCBpc0ljb25EZWZpbml0aW9uLCBsb2csIE1pbmlNYXAsIHdpdGhTdWZmaXggfSBmcm9tICcuLi91dGlscyc7XG52YXIgdHdvVG9uZUNvbG9yUGFsZXR0ZSA9IHtcbiAgICBwcmltYXJ5Q29sb3I6ICcjMzMzJyxcbiAgICBzZWNvbmRhcnlDb2xvcjogJyNFNkU2RTYnXG59O1xuXG52YXIgSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICAgX2luaGVyaXRzKEljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gICAgZnVuY3Rpb24gSWNvbigpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEljb24pO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoSWNvbi5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKEljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgICB9XG5cbiAgICBfY3JlYXRlQ2xhc3MoSWNvbiwgW3tcbiAgICAgICAga2V5OiAncmVuZGVyJyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgICAgIHZhciBfZXh0ZW5kczI7XG5cbiAgICAgICAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgICAgICAgIHR5cGUgPSBfcHJvcHMudHlwZSxcbiAgICAgICAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgICAgICAgIG9uQ2xpY2sgPSBfcHJvcHMub25DbGljayxcbiAgICAgICAgICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICAgICAgICBwcmltYXJ5Q29sb3IgPSBfcHJvcHMucHJpbWFyeUNvbG9yLFxuICAgICAgICAgICAgICAgIHNlY29uZGFyeUNvbG9yID0gX3Byb3BzLnNlY29uZGFyeUNvbG9yLFxuICAgICAgICAgICAgICAgIHJlc3QgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3R5cGUnLCAnY2xhc3NOYW1lJywgJ29uQ2xpY2snLCAnc3R5bGUnLCAncHJpbWFyeUNvbG9yJywgJ3NlY29uZGFyeUNvbG9yJ10pO1xuXG4gICAgICAgICAgICB2YXIgdGFyZ2V0ID0gdm9pZCAwO1xuICAgICAgICAgICAgdmFyIGNvbG9ycyA9IHR3b1RvbmVDb2xvclBhbGV0dGU7XG4gICAgICAgICAgICBpZiAocHJpbWFyeUNvbG9yKSB7XG4gICAgICAgICAgICAgICAgY29sb3JzID0ge1xuICAgICAgICAgICAgICAgICAgICBwcmltYXJ5Q29sb3I6IHByaW1hcnlDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgc2Vjb25kYXJ5Q29sb3I6IHNlY29uZGFyeUNvbG9yIHx8IGdldFNlY29uZGFyeUNvbG9yKHByaW1hcnlDb2xvcilcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGlzSWNvbkRlZmluaXRpb24odHlwZSkpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXQgPSB0eXBlO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgdHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICB0YXJnZXQgPSBJY29uLmdldCh0eXBlLCBjb2xvcnMpO1xuICAgICAgICAgICAgICAgIGlmICghdGFyZ2V0KSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyhgQ291bGQgbm90IGZpbmQgaWNvbjogJHt0eXBlfWApO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoIXRhcmdldCkge1xuICAgICAgICAgICAgICAgIGxvZygndHlwZSBzaG91bGQgYmUgc3RyaW5nIG9yIGljb24gZGVmaW5pdG9uLCBidXQgZ290ICcgKyB0eXBlKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0YXJnZXQgJiYgdHlwZW9mIHRhcmdldC5pY29uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0ID0gX2V4dGVuZHMoe30sIHRhcmdldCwge1xuICAgICAgICAgICAgICAgICAgICBpY29uOiB0YXJnZXQuaWNvbihjb2xvcnMucHJpbWFyeUNvbG9yLCBjb2xvcnMuc2Vjb25kYXJ5Q29sb3IpXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZ2VuZXJhdGUodGFyZ2V0Lmljb24sICdzdmctJyArIHRhcmdldC5uYW1lLCBfZXh0ZW5kcygoX2V4dGVuZHMyID0ge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgICAgICAgIG9uQ2xpY2s6IG9uQ2xpY2ssXG4gICAgICAgICAgICAgICAgc3R5bGU6IHN0eWxlXG4gICAgICAgICAgICB9LCBfZGVmaW5lUHJvcGVydHkoX2V4dGVuZHMyLCAnZGF0YS1pY29uJywgdGFyZ2V0Lm5hbWUpLCBfZGVmaW5lUHJvcGVydHkoX2V4dGVuZHMyLCAnd2lkdGgnLCAnMWVtJyksIF9kZWZpbmVQcm9wZXJ0eShfZXh0ZW5kczIsICdoZWlnaHQnLCAnMWVtJyksIF9kZWZpbmVQcm9wZXJ0eShfZXh0ZW5kczIsICdmaWxsJywgJ2N1cnJlbnRDb2xvcicpLCBfZGVmaW5lUHJvcGVydHkoX2V4dGVuZHMyLCAnYXJpYS1oaWRkZW4nLCAndHJ1ZScpLCBfZGVmaW5lUHJvcGVydHkoX2V4dGVuZHMyLCAnZm9jdXNhYmxlJywgJ2ZhbHNlJyksIF9leHRlbmRzMiksIHJlc3QpKTtcbiAgICAgICAgfVxuICAgIH1dLCBbe1xuICAgICAgICBrZXk6ICdhZGQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gYWRkKCkge1xuICAgICAgICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgICAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBpY29ucyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICAgICAgICAgIGljb25zW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpY29ucy5mb3JFYWNoKGZ1bmN0aW9uIChpY29uKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMyLmRlZmluaXRpb25zLnNldCh3aXRoU3VmZml4KGljb24ubmFtZSwgaWNvbi50aGVtZSksIGljb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogJ2NsZWFyJyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNsZWFyKCkge1xuICAgICAgICAgICAgdGhpcy5kZWZpbml0aW9ucy5jbGVhcigpO1xuICAgICAgICB9XG4gICAgfSwge1xuICAgICAgICBrZXk6ICdnZXQnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0KGtleSkge1xuICAgICAgICAgICAgdmFyIGNvbG9ycyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHdvVG9uZUNvbG9yUGFsZXR0ZTtcblxuICAgICAgICAgICAgaWYgKGtleSkge1xuICAgICAgICAgICAgICAgIHZhciB0YXJnZXQgPSB0aGlzLmRlZmluaXRpb25zLmdldChrZXkpO1xuICAgICAgICAgICAgICAgIGlmICh0YXJnZXQgJiYgdHlwZW9mIHRhcmdldC5pY29uID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldCA9IF9leHRlbmRzKHt9LCB0YXJnZXQsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGljb246IHRhcmdldC5pY29uKGNvbG9ycy5wcmltYXJ5Q29sb3IsIGNvbG9ycy5zZWNvbmRhcnlDb2xvcilcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiB0YXJnZXQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogJ3NldFR3b1RvbmVDb2xvcnMnLFxuICAgICAgICB2YWx1ZTogZnVuY3Rpb24gc2V0VHdvVG9uZUNvbG9ycyhfcmVmKSB7XG4gICAgICAgICAgICB2YXIgcHJpbWFyeUNvbG9yID0gX3JlZi5wcmltYXJ5Q29sb3IsXG4gICAgICAgICAgICAgICAgc2Vjb25kYXJ5Q29sb3IgPSBfcmVmLnNlY29uZGFyeUNvbG9yO1xuXG4gICAgICAgICAgICB0d29Ub25lQ29sb3JQYWxldHRlLnByaW1hcnlDb2xvciA9IHByaW1hcnlDb2xvcjtcbiAgICAgICAgICAgIHR3b1RvbmVDb2xvclBhbGV0dGUuc2Vjb25kYXJ5Q29sb3IgPSBzZWNvbmRhcnlDb2xvciB8fCBnZXRTZWNvbmRhcnlDb2xvcihwcmltYXJ5Q29sb3IpO1xuICAgICAgICB9XG4gICAgfSwge1xuICAgICAgICBrZXk6ICdnZXRUd29Ub25lQ29sb3JzJyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGdldFR3b1RvbmVDb2xvcnMoKSB7XG4gICAgICAgICAgICByZXR1cm4gX2V4dGVuZHMoe30sIHR3b1RvbmVDb2xvclBhbGV0dGUpO1xuICAgICAgICB9XG4gICAgfV0pO1xuXG4gICAgcmV0dXJuIEljb247XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbkljb24uZGlzcGxheU5hbWUgPSAnSWNvblJlYWN0Jztcbkljb24uZGVmaW5pdGlvbnMgPSBuZXcgTWluaU1hcCgpO1xuZXhwb3J0IGRlZmF1bHQgSWNvbjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQTdDQTtBQStDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWRBO0FBZ0JBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/@ant-design/icons-react/es/components/Icon.js
