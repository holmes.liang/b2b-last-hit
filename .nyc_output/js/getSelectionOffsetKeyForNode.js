/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getSelectionOffsetKeyForNode
 * @format
 * 
 */

/**
 * Get offset key from a node or it's child nodes. Return the first offset key
 * found on the DOM tree of given node.
 */

function getSelectionOffsetKeyForNode(node) {
  if (node instanceof Element) {
    var offsetKey = node.getAttribute('data-offset-key');

    if (offsetKey) {
      return offsetKey;
    }

    for (var ii = 0; ii < node.childNodes.length; ii++) {
      var childOffsetKey = getSelectionOffsetKeyForNode(node.childNodes[ii]);

      if (childOffsetKey) {
        return childOffsetKey;
      }
    }
  }

  return null;
}

module.exports = getSelectionOffsetKeyForNode;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGdldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBHZXQgb2Zmc2V0IGtleSBmcm9tIGEgbm9kZSBvciBpdCdzIGNoaWxkIG5vZGVzLiBSZXR1cm4gdGhlIGZpcnN0IG9mZnNldCBrZXlcbiAqIGZvdW5kIG9uIHRoZSBET00gdHJlZSBvZiBnaXZlbiBub2RlLlxuICovXG5cbmZ1bmN0aW9uIGdldFNlbGVjdGlvbk9mZnNldEtleUZvck5vZGUobm9kZSkge1xuICBpZiAobm9kZSBpbnN0YW5jZW9mIEVsZW1lbnQpIHtcbiAgICB2YXIgb2Zmc2V0S2V5ID0gbm9kZS5nZXRBdHRyaWJ1dGUoJ2RhdGEtb2Zmc2V0LWtleScpO1xuICAgIGlmIChvZmZzZXRLZXkpIHtcbiAgICAgIHJldHVybiBvZmZzZXRLZXk7XG4gICAgfVxuICAgIGZvciAodmFyIGlpID0gMDsgaWkgPCBub2RlLmNoaWxkTm9kZXMubGVuZ3RoOyBpaSsrKSB7XG4gICAgICB2YXIgY2hpbGRPZmZzZXRLZXkgPSBnZXRTZWxlY3Rpb25PZmZzZXRLZXlGb3JOb2RlKG5vZGUuY2hpbGROb2Rlc1tpaV0pO1xuICAgICAgaWYgKGNoaWxkT2Zmc2V0S2V5KSB7XG4gICAgICAgIHJldHVybiBjaGlsZE9mZnNldEtleTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0U2VsZWN0aW9uT2Zmc2V0S2V5Rm9yTm9kZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUVBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getSelectionOffsetKeyForNode.js
