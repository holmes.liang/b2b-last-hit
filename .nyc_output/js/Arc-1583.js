var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");
/**
 * 圆弧
 * @module zrender/graphic/shape/Arc
 */


var _default = Path.extend({
  type: 'arc',
  shape: {
    cx: 0,
    cy: 0,
    r: 0,
    startAngle: 0,
    endAngle: Math.PI * 2,
    clockwise: true
  },
  style: {
    stroke: '#000',
    fill: null
  },
  buildPath: function buildPath(ctx, shape) {
    var x = shape.cx;
    var y = shape.cy;
    var r = Math.max(shape.r, 0);
    var startAngle = shape.startAngle;
    var endAngle = shape.endAngle;
    var clockwise = shape.clockwise;
    var unitX = Math.cos(startAngle);
    var unitY = Math.sin(startAngle);
    ctx.moveTo(unitX * r + x, unitY * r + y);
    ctx.arc(x, y, r, startAngle, endAngle, !clockwise);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9BcmMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL3NoYXBlL0FyYy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgUGF0aCA9IHJlcXVpcmUoXCIuLi9QYXRoXCIpO1xuXG4vKipcbiAqIOWchuW8p1xuICogQG1vZHVsZSB6cmVuZGVyL2dyYXBoaWMvc2hhcGUvQXJjXG4gKi9cbnZhciBfZGVmYXVsdCA9IFBhdGguZXh0ZW5kKHtcbiAgdHlwZTogJ2FyYycsXG4gIHNoYXBlOiB7XG4gICAgY3g6IDAsXG4gICAgY3k6IDAsXG4gICAgcjogMCxcbiAgICBzdGFydEFuZ2xlOiAwLFxuICAgIGVuZEFuZ2xlOiBNYXRoLlBJICogMixcbiAgICBjbG9ja3dpc2U6IHRydWVcbiAgfSxcbiAgc3R5bGU6IHtcbiAgICBzdHJva2U6ICcjMDAwJyxcbiAgICBmaWxsOiBudWxsXG4gIH0sXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUpIHtcbiAgICB2YXIgeCA9IHNoYXBlLmN4O1xuICAgIHZhciB5ID0gc2hhcGUuY3k7XG4gICAgdmFyIHIgPSBNYXRoLm1heChzaGFwZS5yLCAwKTtcbiAgICB2YXIgc3RhcnRBbmdsZSA9IHNoYXBlLnN0YXJ0QW5nbGU7XG4gICAgdmFyIGVuZEFuZ2xlID0gc2hhcGUuZW5kQW5nbGU7XG4gICAgdmFyIGNsb2Nrd2lzZSA9IHNoYXBlLmNsb2Nrd2lzZTtcbiAgICB2YXIgdW5pdFggPSBNYXRoLmNvcyhzdGFydEFuZ2xlKTtcbiAgICB2YXIgdW5pdFkgPSBNYXRoLnNpbihzdGFydEFuZ2xlKTtcbiAgICBjdHgubW92ZVRvKHVuaXRYICogciArIHgsIHVuaXRZICogciArIHkpO1xuICAgIGN0eC5hcmMoeCwgeSwgciwgc3RhcnRBbmdsZSwgZW5kQW5nbGUsICFjbG9ja3dpc2UpO1xuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF6QkE7QUFDQTtBQTJCQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Arc.js
