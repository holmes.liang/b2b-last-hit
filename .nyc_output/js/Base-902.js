__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var rc_util_es_Dom_findDOMNode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/Dom/findDOMNode */ "./node_modules/rc-util/es/Dom/findDOMNode.js");
/* harmony import */ var copy_to_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! copy-to-clipboard */ "./node_modules/copy-to-clipboard/index.js");
/* harmony import */ var copy_to_clipboard__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(copy_to_clipboard__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rc-resize-observer */ "./node_modules/rc-resize-observer/es/index.js");
/* harmony import */ var rc_resize_observer__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(rc_resize_observer__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _config_provider_context__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../config-provider/context */ "./node_modules/antd/es/config-provider/context.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _util_transButton__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../_util/transButton */ "./node_modules/antd/es/_util/transButton.js");
/* harmony import */ var _util_raf__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../_util/raf */ "./node_modules/antd/es/_util/raf.js");
/* harmony import */ var _util_styleChecker__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../_util/styleChecker */ "./node_modules/antd/es/_util/styleChecker.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _tooltip__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../tooltip */ "./node_modules/antd/es/tooltip/index.js");
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./Typography */ "./node_modules/antd/es/typography/Typography.js");
/* harmony import */ var _Editable__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./Editable */ "./node_modules/antd/es/typography/Editable.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./util */ "./node_modules/antd/es/typography/util.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};





















var isLineClampSupport = Object(_util_styleChecker__WEBPACK_IMPORTED_MODULE_14__["default"])('webkitLineClamp');
var isTextOverflowSupport = Object(_util_styleChecker__WEBPACK_IMPORTED_MODULE_14__["default"])('textOverflow');

function wrapperDecorations(_ref, content) {
  var mark = _ref.mark,
      code = _ref.code,
      underline = _ref.underline,
      del = _ref["delete"],
      strong = _ref.strong;
  var currentContent = content;

  function wrap(needed, tag) {
    if (!needed) return;
    currentContent = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](tag, {}, currentContent);
  }

  wrap(strong, 'strong');
  wrap(underline, 'u');
  wrap(del, 'del');
  wrap(code, 'code');
  wrap(mark, 'mark');
  return currentContent;
}

var ELLIPSIS_STR = '...';

var Base =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Base, _React$Component);

  function Base() {
    var _this;

    _classCallCheck(this, Base);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Base).apply(this, arguments));
    _this.state = {
      edit: false,
      copied: false,
      ellipsisText: '',
      ellipsisContent: null,
      isEllipsis: false,
      expanded: false,
      clientRendered: false
    }; // =============== Expend ===============

    _this.onExpandClick = function () {
      var _this$getEllipsis = _this.getEllipsis(),
          onExpand = _this$getEllipsis.onExpand;

      _this.setState({
        expanded: true
      });

      if (onExpand) {
        onExpand();
      }
    }; // ================ Edit ================


    _this.onEditClick = function () {
      _this.triggerEdit(true);
    };

    _this.onEditChange = function (value) {
      var _this$getEditable = _this.getEditable(),
          onChange = _this$getEditable.onChange;

      if (onChange) {
        onChange(value);
      }

      _this.triggerEdit(false);
    };

    _this.onEditCancel = function () {
      _this.triggerEdit(false);
    }; // ================ Copy ================


    _this.onCopyClick = function () {
      var _this$props = _this.props,
          children = _this$props.children,
          copyable = _this$props.copyable;

      var copyConfig = _extends({}, _typeof(copyable) === 'object' ? copyable : null);

      if (copyConfig.text === undefined) {
        copyConfig.text = String(children);
      }

      copy_to_clipboard__WEBPACK_IMPORTED_MODULE_5___default()(copyConfig.text || '');

      _this.setState({
        copied: true
      }, function () {
        if (copyConfig.onCopy) {
          copyConfig.onCopy();
        }

        _this.copyId = window.setTimeout(function () {
          _this.setState({
            copied: false
          });
        }, 3000);
      });
    };

    _this.setContentRef = function (node) {
      _this.content = node;
    };

    _this.setEditRef = function (node) {
      _this.editIcon = node;
    };

    _this.triggerEdit = function (edit) {
      var _this$getEditable2 = _this.getEditable(),
          onStart = _this$getEditable2.onStart;

      if (edit && onStart) {
        onStart();
      }

      _this.setState({
        edit: edit
      }, function () {
        if (!edit && _this.editIcon) {
          _this.editIcon.focus();
        }
      });
    }; // ============== Ellipsis ==============


    _this.resizeOnNextFrame = function () {
      _util_raf__WEBPACK_IMPORTED_MODULE_13__["default"].cancel(_this.rafId);
      _this.rafId = Object(_util_raf__WEBPACK_IMPORTED_MODULE_13__["default"])(function () {
        // Do not bind `syncEllipsis`. It need for test usage on prototype
        _this.syncEllipsis();
      });
    };

    return _this;
  }

  _createClass(Base, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({
        clientRendered: true
      });
      this.resizeOnNextFrame();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var children = this.props.children;
      var ellipsis = this.getEllipsis();
      var prevEllipsis = this.getEllipsis(prevProps);

      if (children !== prevProps.children || ellipsis.rows !== prevEllipsis.rows) {
        this.resizeOnNextFrame();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.clearTimeout(this.copyId);
      _util_raf__WEBPACK_IMPORTED_MODULE_13__["default"].cancel(this.rafId);
    }
  }, {
    key: "getEditable",
    value: function getEditable(props) {
      var edit = this.state.edit;

      var _ref2 = props || this.props,
          editable = _ref2.editable;

      if (!editable) return {
        editing: edit
      };
      return _extends({
        editing: edit
      }, _typeof(editable) === 'object' ? editable : null);
    }
  }, {
    key: "getEllipsis",
    value: function getEllipsis(props) {
      var _ref3 = props || this.props,
          ellipsis = _ref3.ellipsis;

      if (!ellipsis) return {};
      return _extends({
        rows: 1,
        expandable: false
      }, _typeof(ellipsis) === 'object' ? ellipsis : null);
    }
  }, {
    key: "canUseCSSEllipsis",
    value: function canUseCSSEllipsis() {
      var clientRendered = this.state.clientRendered;
      var _this$props2 = this.props,
          editable = _this$props2.editable,
          copyable = _this$props2.copyable;

      var _this$getEllipsis2 = this.getEllipsis(),
          rows = _this$getEllipsis2.rows,
          expandable = _this$getEllipsis2.expandable; // Can't use css ellipsis since we need to provide the place for button


      if (editable || copyable || expandable || !clientRendered) {
        return false;
      }

      if (rows === 1) {
        return isTextOverflowSupport;
      }

      return isLineClampSupport;
    }
  }, {
    key: "syncEllipsis",
    value: function syncEllipsis() {
      var _this$state = this.state,
          ellipsisText = _this$state.ellipsisText,
          isEllipsis = _this$state.isEllipsis,
          expanded = _this$state.expanded;

      var _this$getEllipsis3 = this.getEllipsis(),
          rows = _this$getEllipsis3.rows;

      var children = this.props.children;
      if (!rows || rows < 0 || !this.content || expanded) return; // Do not measure if css already support ellipsis

      if (this.canUseCSSEllipsis()) return;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_11__["default"])(Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__["default"])(children).every(function (child) {
        return typeof child === 'string';
      }), 'Typography', '`ellipsis` should use string as children only.');

      var _measure = Object(_util__WEBPACK_IMPORTED_MODULE_19__["default"])(Object(rc_util_es_Dom_findDOMNode__WEBPACK_IMPORTED_MODULE_4__["default"])(this.content), rows, children, this.renderOperations(true), ELLIPSIS_STR),
          content = _measure.content,
          text = _measure.text,
          ellipsis = _measure.ellipsis;

      if (ellipsisText !== text || isEllipsis !== ellipsis) {
        this.setState({
          ellipsisText: text,
          ellipsisContent: content,
          isEllipsis: ellipsis
        });
      }
    }
  }, {
    key: "renderExpand",
    value: function renderExpand(forceRender) {
      var _this$getEllipsis4 = this.getEllipsis(),
          expandable = _this$getEllipsis4.expandable;

      var prefixCls = this.props.prefixCls;
      var _this$state2 = this.state,
          expanded = _this$state2.expanded,
          isEllipsis = _this$state2.isEllipsis;
      if (!expandable) return null; // force render expand icon for measure usage or it will cause dead loop

      if (!forceRender && (expanded || !isEllipsis)) return null;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("a", {
        key: "expand",
        className: "".concat(prefixCls, "-expand"),
        onClick: this.onExpandClick,
        "aria-label": this.expandStr
      }, this.expandStr);
    }
  }, {
    key: "renderEdit",
    value: function renderEdit() {
      var _this$props3 = this.props,
          editable = _this$props3.editable,
          prefixCls = _this$props3.prefixCls;
      if (!editable) return;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_tooltip__WEBPACK_IMPORTED_MODULE_16__["default"], {
        key: "edit",
        title: this.editStr
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_transButton__WEBPACK_IMPORTED_MODULE_12__["default"], {
        ref: this.setEditRef,
        className: "".concat(prefixCls, "-edit"),
        onClick: this.onEditClick,
        "aria-label": this.editStr
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_15__["default"], {
        role: "button",
        type: "edit"
      })));
    }
  }, {
    key: "renderCopy",
    value: function renderCopy() {
      var copied = this.state.copied;
      var _this$props4 = this.props,
          copyable = _this$props4.copyable,
          prefixCls = _this$props4.prefixCls;
      if (!copyable) return;
      var title = copied ? this.copiedStr : this.copyStr;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_tooltip__WEBPACK_IMPORTED_MODULE_16__["default"], {
        key: "copy",
        title: title
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_transButton__WEBPACK_IMPORTED_MODULE_12__["default"], {
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-copy"), copied && "".concat(prefixCls, "-copy-success")),
        onClick: this.onCopyClick,
        "aria-label": title
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_15__["default"], {
        role: "button",
        type: copied ? 'check' : 'copy'
      })));
    }
  }, {
    key: "renderEditInput",
    value: function renderEditInput() {
      var _this$props5 = this.props,
          children = _this$props5.children,
          prefixCls = _this$props5.prefixCls,
          className = _this$props5.className,
          style = _this$props5.style;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Editable__WEBPACK_IMPORTED_MODULE_18__["default"], {
        value: typeof children === 'string' ? children : '',
        onSave: this.onEditChange,
        onCancel: this.onEditCancel,
        prefixCls: prefixCls,
        className: className,
        style: style
      });
    }
  }, {
    key: "renderOperations",
    value: function renderOperations(forceRenderExpanded) {
      return [this.renderExpand(forceRenderExpanded), this.renderEdit(), this.renderCopy()].filter(function (node) {
        return node;
      });
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this2 = this;

      var _this$state3 = this.state,
          ellipsisContent = _this$state3.ellipsisContent,
          isEllipsis = _this$state3.isEllipsis,
          expanded = _this$state3.expanded;

      var _a = this.props,
          component = _a.component,
          children = _a.children,
          className = _a.className,
          prefixCls = _a.prefixCls,
          type = _a.type,
          disabled = _a.disabled,
          style = _a.style,
          restProps = __rest(_a, ["component", "children", "className", "prefixCls", "type", "disabled", "style"]);

      var _this$getEllipsis5 = this.getEllipsis(),
          rows = _this$getEllipsis5.rows;

      var textProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_6__["default"])(restProps, ['prefixCls', 'editable', 'copyable', 'ellipsis', 'mark', 'underline', 'mark', 'code', 'delete', 'underline', 'strong'].concat(_toConsumableArray(_config_provider__WEBPACK_IMPORTED_MODULE_8__["configConsumerProps"])));
      var cssEllipsis = this.canUseCSSEllipsis();
      var cssTextOverflow = rows === 1 && cssEllipsis;
      var cssLineClamp = rows && rows > 1 && cssEllipsis;
      var textNode = children;
      var ariaLabel = null; // Only use js ellipsis when css ellipsis not support

      if (rows && isEllipsis && !expanded && !cssEllipsis) {
        ariaLabel = String(children); // We move full content to outer element to avoid repeat read the content by accessibility

        textNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          title: String(children),
          "aria-hidden": "true"
        }, ellipsisContent, ELLIPSIS_STR);
      }

      textNode = wrapperDecorations(this.props, textNode);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_10__["default"], {
        componentName: "Text"
      }, function (_ref4) {
        var _classNames;

        var edit = _ref4.edit,
            copyStr = _ref4.copy,
            copied = _ref4.copied,
            expand = _ref4.expand;
        _this2.editStr = edit;
        _this2.copyStr = copyStr;
        _this2.copiedStr = copied;
        _this2.expandStr = expand;
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_resize_observer__WEBPACK_IMPORTED_MODULE_7___default.a, {
          onResize: _this2.resizeOnNextFrame,
          disabled: !rows
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Typography__WEBPACK_IMPORTED_MODULE_17__["default"], _extends({
          className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(type), type), _defineProperty(_classNames, "".concat(prefixCls, "-disabled"), disabled), _defineProperty(_classNames, "".concat(prefixCls, "-ellipsis"), rows), _defineProperty(_classNames, "".concat(prefixCls, "-ellipsis-single-line"), cssTextOverflow), _defineProperty(_classNames, "".concat(prefixCls, "-ellipsis-multiple-line"), cssLineClamp), _classNames)),
          style: _extends(_extends({}, style), {
            WebkitLineClamp: cssLineClamp ? rows : null
          }),
          component: component,
          ref: _this2.setContentRef,
          "aria-label": ariaLabel
        }, textProps), textNode, _this2.renderOperations()));
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$getEditable3 = this.getEditable(),
          editing = _this$getEditable3.editing;

      if (editing) {
        return this.renderEditInput();
      }

      return this.renderContent();
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      var children = nextProps.children,
          editable = nextProps.editable;
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_11__["default"])(!editable || typeof children === 'string', 'Typography', 'When `editable` is enabled, the `children` should use string.');
      return {};
    }
  }]);

  return Base;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Base.defaultProps = {
  children: ''
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_2__["polyfill"])(Base);
/* harmony default export */ __webpack_exports__["default"] = (Object(_config_provider_context__WEBPACK_IMPORTED_MODULE_9__["withConfigConsumer"])({
  prefixCls: 'typography'
})(Base));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L0Jhc2UuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3R5cG9ncmFwaHkvQmFzZS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCB0b0FycmF5IGZyb20gJ3JjLXV0aWwvbGliL0NoaWxkcmVuL3RvQXJyYXknO1xuaW1wb3J0IGZpbmRET01Ob2RlIGZyb20gJ3JjLXV0aWwvbGliL0RvbS9maW5kRE9NTm9kZSc7XG5pbXBvcnQgY29weSBmcm9tICdjb3B5LXRvLWNsaXBib2FyZCc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBSZXNpemVPYnNlcnZlciBmcm9tICdyYy1yZXNpemUtb2JzZXJ2ZXInO1xuaW1wb3J0IHsgY29uZmlnQ29uc3VtZXJQcm9wcyB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgeyB3aXRoQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXIvY29udGV4dCc7XG5pbXBvcnQgTG9jYWxlUmVjZWl2ZXIgZnJvbSAnLi4vbG9jYWxlLXByb3ZpZGVyL0xvY2FsZVJlY2VpdmVyJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuaW1wb3J0IFRyYW5zQnV0dG9uIGZyb20gJy4uL191dGlsL3RyYW5zQnV0dG9uJztcbmltcG9ydCByYWYgZnJvbSAnLi4vX3V0aWwvcmFmJztcbmltcG9ydCBpc1N0eWxlU3VwcG9ydCBmcm9tICcuLi9fdXRpbC9zdHlsZUNoZWNrZXInO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICcuLi90b29sdGlwJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJy4vVHlwb2dyYXBoeSc7XG5pbXBvcnQgRWRpdGFibGUgZnJvbSAnLi9FZGl0YWJsZSc7XG5pbXBvcnQgbWVhc3VyZSBmcm9tICcuL3V0aWwnO1xuY29uc3QgaXNMaW5lQ2xhbXBTdXBwb3J0ID0gaXNTdHlsZVN1cHBvcnQoJ3dlYmtpdExpbmVDbGFtcCcpO1xuY29uc3QgaXNUZXh0T3ZlcmZsb3dTdXBwb3J0ID0gaXNTdHlsZVN1cHBvcnQoJ3RleHRPdmVyZmxvdycpO1xuZnVuY3Rpb24gd3JhcHBlckRlY29yYXRpb25zKHsgbWFyaywgY29kZSwgdW5kZXJsaW5lLCBkZWxldGU6IGRlbCwgc3Ryb25nIH0sIGNvbnRlbnQpIHtcbiAgICBsZXQgY3VycmVudENvbnRlbnQgPSBjb250ZW50O1xuICAgIGZ1bmN0aW9uIHdyYXAobmVlZGVkLCB0YWcpIHtcbiAgICAgICAgaWYgKCFuZWVkZWQpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIGN1cnJlbnRDb250ZW50ID0gUmVhY3QuY3JlYXRlRWxlbWVudCh0YWcsIHt9LCBjdXJyZW50Q29udGVudCk7XG4gICAgfVxuICAgIHdyYXAoc3Ryb25nLCAnc3Ryb25nJyk7XG4gICAgd3JhcCh1bmRlcmxpbmUsICd1Jyk7XG4gICAgd3JhcChkZWwsICdkZWwnKTtcbiAgICB3cmFwKGNvZGUsICdjb2RlJyk7XG4gICAgd3JhcChtYXJrLCAnbWFyaycpO1xuICAgIHJldHVybiBjdXJyZW50Q29udGVudDtcbn1cbmNvbnN0IEVMTElQU0lTX1NUUiA9ICcuLi4nO1xuY2xhc3MgQmFzZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBlZGl0OiBmYWxzZSxcbiAgICAgICAgICAgIGNvcGllZDogZmFsc2UsXG4gICAgICAgICAgICBlbGxpcHNpc1RleHQ6ICcnLFxuICAgICAgICAgICAgZWxsaXBzaXNDb250ZW50OiBudWxsLFxuICAgICAgICAgICAgaXNFbGxpcHNpczogZmFsc2UsXG4gICAgICAgICAgICBleHBhbmRlZDogZmFsc2UsXG4gICAgICAgICAgICBjbGllbnRSZW5kZXJlZDogZmFsc2UsXG4gICAgICAgIH07XG4gICAgICAgIC8vID09PT09PT09PT09PT09PSBFeHBlbmQgPT09PT09PT09PT09PT09XG4gICAgICAgIHRoaXMub25FeHBhbmRDbGljayA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25FeHBhbmQgfSA9IHRoaXMuZ2V0RWxsaXBzaXMoKTtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBleHBhbmRlZDogdHJ1ZSB9KTtcbiAgICAgICAgICAgIGlmIChvbkV4cGFuZCkge1xuICAgICAgICAgICAgICAgIG9uRXhwYW5kKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIC8vID09PT09PT09PT09PT09PT0gRWRpdCA9PT09PT09PT09PT09PT09XG4gICAgICAgIHRoaXMub25FZGl0Q2xpY2sgPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnRyaWdnZXJFZGl0KHRydWUpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uRWRpdENoYW5nZSA9ICh2YWx1ZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvbkNoYW5nZSB9ID0gdGhpcy5nZXRFZGl0YWJsZSgpO1xuICAgICAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgb25DaGFuZ2UodmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy50cmlnZ2VyRWRpdChmYWxzZSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25FZGl0Q2FuY2VsID0gKCkgPT4ge1xuICAgICAgICAgICAgdGhpcy50cmlnZ2VyRWRpdChmYWxzZSk7XG4gICAgICAgIH07XG4gICAgICAgIC8vID09PT09PT09PT09PT09PT0gQ29weSA9PT09PT09PT09PT09PT09XG4gICAgICAgIHRoaXMub25Db3B5Q2xpY2sgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGNoaWxkcmVuLCBjb3B5YWJsZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGNvcHlDb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCAodHlwZW9mIGNvcHlhYmxlID09PSAnb2JqZWN0JyA/IGNvcHlhYmxlIDogbnVsbCkpO1xuICAgICAgICAgICAgaWYgKGNvcHlDb25maWcudGV4dCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgY29weUNvbmZpZy50ZXh0ID0gU3RyaW5nKGNoaWxkcmVuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvcHkoY29weUNvbmZpZy50ZXh0IHx8ICcnKTtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBjb3BpZWQ6IHRydWUgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChjb3B5Q29uZmlnLm9uQ29weSkge1xuICAgICAgICAgICAgICAgICAgICBjb3B5Q29uZmlnLm9uQ29weSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmNvcHlJZCA9IHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGNvcGllZDogZmFsc2UgfSk7XG4gICAgICAgICAgICAgICAgfSwgMzAwMCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zZXRDb250ZW50UmVmID0gKG5vZGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY29udGVudCA9IG5vZGU7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc2V0RWRpdFJlZiA9IChub2RlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmVkaXRJY29uID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy50cmlnZ2VyRWRpdCA9IChlZGl0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uU3RhcnQgfSA9IHRoaXMuZ2V0RWRpdGFibGUoKTtcbiAgICAgICAgICAgIGlmIChlZGl0ICYmIG9uU3RhcnQpIHtcbiAgICAgICAgICAgICAgICBvblN0YXJ0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgZWRpdCB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFlZGl0ICYmIHRoaXMuZWRpdEljb24pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lZGl0SWNvbi5mb2N1cygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICAvLyA9PT09PT09PT09PT09PSBFbGxpcHNpcyA9PT09PT09PT09PT09PVxuICAgICAgICB0aGlzLnJlc2l6ZU9uTmV4dEZyYW1lID0gKCkgPT4ge1xuICAgICAgICAgICAgcmFmLmNhbmNlbCh0aGlzLnJhZklkKTtcbiAgICAgICAgICAgIHRoaXMucmFmSWQgPSByYWYoKCkgPT4ge1xuICAgICAgICAgICAgICAgIC8vIERvIG5vdCBiaW5kIGBzeW5jRWxsaXBzaXNgLiBJdCBuZWVkIGZvciB0ZXN0IHVzYWdlIG9uIHByb3RvdHlwZVxuICAgICAgICAgICAgICAgIHRoaXMuc3luY0VsbGlwc2lzKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMpIHtcbiAgICAgICAgY29uc3QgeyBjaGlsZHJlbiwgZWRpdGFibGUgfSA9IG5leHRQcm9wcztcbiAgICAgICAgd2FybmluZyghZWRpdGFibGUgfHwgdHlwZW9mIGNoaWxkcmVuID09PSAnc3RyaW5nJywgJ1R5cG9ncmFwaHknLCAnV2hlbiBgZWRpdGFibGVgIGlzIGVuYWJsZWQsIHRoZSBgY2hpbGRyZW5gIHNob3VsZCB1c2Ugc3RyaW5nLicpO1xuICAgICAgICByZXR1cm4ge307XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgY2xpZW50UmVuZGVyZWQ6IHRydWUgfSk7XG4gICAgICAgIHRoaXMucmVzaXplT25OZXh0RnJhbWUoKTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgICBjb25zdCB7IGNoaWxkcmVuIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBlbGxpcHNpcyA9IHRoaXMuZ2V0RWxsaXBzaXMoKTtcbiAgICAgICAgY29uc3QgcHJldkVsbGlwc2lzID0gdGhpcy5nZXRFbGxpcHNpcyhwcmV2UHJvcHMpO1xuICAgICAgICBpZiAoY2hpbGRyZW4gIT09IHByZXZQcm9wcy5jaGlsZHJlbiB8fCBlbGxpcHNpcy5yb3dzICE9PSBwcmV2RWxsaXBzaXMucm93cykge1xuICAgICAgICAgICAgdGhpcy5yZXNpemVPbk5leHRGcmFtZSgpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KHRoaXMuY29weUlkKTtcbiAgICAgICAgcmFmLmNhbmNlbCh0aGlzLnJhZklkKTtcbiAgICB9XG4gICAgZ2V0RWRpdGFibGUocHJvcHMpIHtcbiAgICAgICAgY29uc3QgeyBlZGl0IH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBjb25zdCB7IGVkaXRhYmxlIH0gPSBwcm9wcyB8fCB0aGlzLnByb3BzO1xuICAgICAgICBpZiAoIWVkaXRhYmxlKVxuICAgICAgICAgICAgcmV0dXJuIHsgZWRpdGluZzogZWRpdCB9O1xuICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7IGVkaXRpbmc6IGVkaXQgfSwgKHR5cGVvZiBlZGl0YWJsZSA9PT0gJ29iamVjdCcgPyBlZGl0YWJsZSA6IG51bGwpKTtcbiAgICB9XG4gICAgZ2V0RWxsaXBzaXMocHJvcHMpIHtcbiAgICAgICAgY29uc3QgeyBlbGxpcHNpcyB9ID0gcHJvcHMgfHwgdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKCFlbGxpcHNpcylcbiAgICAgICAgICAgIHJldHVybiB7fTtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oeyByb3dzOiAxLCBleHBhbmRhYmxlOiBmYWxzZSB9LCAodHlwZW9mIGVsbGlwc2lzID09PSAnb2JqZWN0JyA/IGVsbGlwc2lzIDogbnVsbCkpO1xuICAgIH1cbiAgICBjYW5Vc2VDU1NFbGxpcHNpcygpIHtcbiAgICAgICAgY29uc3QgeyBjbGllbnRSZW5kZXJlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgY29uc3QgeyBlZGl0YWJsZSwgY29weWFibGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IHsgcm93cywgZXhwYW5kYWJsZSB9ID0gdGhpcy5nZXRFbGxpcHNpcygpO1xuICAgICAgICAvLyBDYW4ndCB1c2UgY3NzIGVsbGlwc2lzIHNpbmNlIHdlIG5lZWQgdG8gcHJvdmlkZSB0aGUgcGxhY2UgZm9yIGJ1dHRvblxuICAgICAgICBpZiAoZWRpdGFibGUgfHwgY29weWFibGUgfHwgZXhwYW5kYWJsZSB8fCAhY2xpZW50UmVuZGVyZWQpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocm93cyA9PT0gMSkge1xuICAgICAgICAgICAgcmV0dXJuIGlzVGV4dE92ZXJmbG93U3VwcG9ydDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaXNMaW5lQ2xhbXBTdXBwb3J0O1xuICAgIH1cbiAgICBzeW5jRWxsaXBzaXMoKSB7XG4gICAgICAgIGNvbnN0IHsgZWxsaXBzaXNUZXh0LCBpc0VsbGlwc2lzLCBleHBhbmRlZCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgY29uc3QgeyByb3dzIH0gPSB0aGlzLmdldEVsbGlwc2lzKCk7XG4gICAgICAgIGNvbnN0IHsgY2hpbGRyZW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmICghcm93cyB8fCByb3dzIDwgMCB8fCAhdGhpcy5jb250ZW50IHx8IGV4cGFuZGVkKVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAvLyBEbyBub3QgbWVhc3VyZSBpZiBjc3MgYWxyZWFkeSBzdXBwb3J0IGVsbGlwc2lzXG4gICAgICAgIGlmICh0aGlzLmNhblVzZUNTU0VsbGlwc2lzKCkpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIHdhcm5pbmcodG9BcnJheShjaGlsZHJlbikuZXZlcnkoKGNoaWxkKSA9PiB0eXBlb2YgY2hpbGQgPT09ICdzdHJpbmcnKSwgJ1R5cG9ncmFwaHknLCAnYGVsbGlwc2lzYCBzaG91bGQgdXNlIHN0cmluZyBhcyBjaGlsZHJlbiBvbmx5LicpO1xuICAgICAgICBjb25zdCB7IGNvbnRlbnQsIHRleHQsIGVsbGlwc2lzIH0gPSBtZWFzdXJlKGZpbmRET01Ob2RlKHRoaXMuY29udGVudCksIHJvd3MsIGNoaWxkcmVuLCB0aGlzLnJlbmRlck9wZXJhdGlvbnModHJ1ZSksIEVMTElQU0lTX1NUUik7XG4gICAgICAgIGlmIChlbGxpcHNpc1RleHQgIT09IHRleHQgfHwgaXNFbGxpcHNpcyAhPT0gZWxsaXBzaXMpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyBlbGxpcHNpc1RleHQ6IHRleHQsIGVsbGlwc2lzQ29udGVudDogY29udGVudCwgaXNFbGxpcHNpczogZWxsaXBzaXMgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmVuZGVyRXhwYW5kKGZvcmNlUmVuZGVyKSB7XG4gICAgICAgIGNvbnN0IHsgZXhwYW5kYWJsZSB9ID0gdGhpcy5nZXRFbGxpcHNpcygpO1xuICAgICAgICBjb25zdCB7IHByZWZpeENscyB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgeyBleHBhbmRlZCwgaXNFbGxpcHNpcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgaWYgKCFleHBhbmRhYmxlKVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIC8vIGZvcmNlIHJlbmRlciBleHBhbmQgaWNvbiBmb3IgbWVhc3VyZSB1c2FnZSBvciBpdCB3aWxsIGNhdXNlIGRlYWQgbG9vcFxuICAgICAgICBpZiAoIWZvcmNlUmVuZGVyICYmIChleHBhbmRlZCB8fCAhaXNFbGxpcHNpcykpXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgcmV0dXJuICg8YSBrZXk9XCJleHBhbmRcIiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tZXhwYW5kYH0gb25DbGljaz17dGhpcy5vbkV4cGFuZENsaWNrfSBhcmlhLWxhYmVsPXt0aGlzLmV4cGFuZFN0cn0+XG4gICAgICAgIHt0aGlzLmV4cGFuZFN0cn1cbiAgICAgIDwvYT4pO1xuICAgIH1cbiAgICByZW5kZXJFZGl0KCkge1xuICAgICAgICBjb25zdCB7IGVkaXRhYmxlLCBwcmVmaXhDbHMgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmICghZWRpdGFibGUpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIHJldHVybiAoPFRvb2x0aXAga2V5PVwiZWRpdFwiIHRpdGxlPXt0aGlzLmVkaXRTdHJ9PlxuICAgICAgICA8VHJhbnNCdXR0b24gcmVmPXt0aGlzLnNldEVkaXRSZWZ9IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1lZGl0YH0gb25DbGljaz17dGhpcy5vbkVkaXRDbGlja30gYXJpYS1sYWJlbD17dGhpcy5lZGl0U3RyfT5cbiAgICAgICAgICA8SWNvbiByb2xlPVwiYnV0dG9uXCIgdHlwZT1cImVkaXRcIi8+XG4gICAgICAgIDwvVHJhbnNCdXR0b24+XG4gICAgICA8L1Rvb2x0aXA+KTtcbiAgICB9XG4gICAgcmVuZGVyQ29weSgpIHtcbiAgICAgICAgY29uc3QgeyBjb3BpZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IHsgY29weWFibGUsIHByZWZpeENscyB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKCFjb3B5YWJsZSlcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgY29uc3QgdGl0bGUgPSBjb3BpZWQgPyB0aGlzLmNvcGllZFN0ciA6IHRoaXMuY29weVN0cjtcbiAgICAgICAgcmV0dXJuICg8VG9vbHRpcCBrZXk9XCJjb3B5XCIgdGl0bGU9e3RpdGxlfT5cbiAgICAgICAgPFRyYW5zQnV0dG9uIGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWNvcHlgLCBjb3BpZWQgJiYgYCR7cHJlZml4Q2xzfS1jb3B5LXN1Y2Nlc3NgKX0gb25DbGljaz17dGhpcy5vbkNvcHlDbGlja30gYXJpYS1sYWJlbD17dGl0bGV9PlxuICAgICAgICAgIDxJY29uIHJvbGU9XCJidXR0b25cIiB0eXBlPXtjb3BpZWQgPyAnY2hlY2snIDogJ2NvcHknfS8+XG4gICAgICAgIDwvVHJhbnNCdXR0b24+XG4gICAgICA8L1Rvb2x0aXA+KTtcbiAgICB9XG4gICAgcmVuZGVyRWRpdElucHV0KCkge1xuICAgICAgICBjb25zdCB7IGNoaWxkcmVuLCBwcmVmaXhDbHMsIGNsYXNzTmFtZSwgc3R5bGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiAoPEVkaXRhYmxlIHZhbHVlPXt0eXBlb2YgY2hpbGRyZW4gPT09ICdzdHJpbmcnID8gY2hpbGRyZW4gOiAnJ30gb25TYXZlPXt0aGlzLm9uRWRpdENoYW5nZX0gb25DYW5jZWw9e3RoaXMub25FZGl0Q2FuY2VsfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gY2xhc3NOYW1lPXtjbGFzc05hbWV9IHN0eWxlPXtzdHlsZX0vPik7XG4gICAgfVxuICAgIHJlbmRlck9wZXJhdGlvbnMoZm9yY2VSZW5kZXJFeHBhbmRlZCkge1xuICAgICAgICByZXR1cm4gW3RoaXMucmVuZGVyRXhwYW5kKGZvcmNlUmVuZGVyRXhwYW5kZWQpLCB0aGlzLnJlbmRlckVkaXQoKSwgdGhpcy5yZW5kZXJDb3B5KCldLmZpbHRlcihub2RlID0+IG5vZGUpO1xuICAgIH1cbiAgICByZW5kZXJDb250ZW50KCkge1xuICAgICAgICBjb25zdCB7IGVsbGlwc2lzQ29udGVudCwgaXNFbGxpcHNpcywgZXhwYW5kZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBjb21wb25lbnQsIGNoaWxkcmVuLCBjbGFzc05hbWUsIHByZWZpeENscywgdHlwZSwgZGlzYWJsZWQsIHN0eWxlIH0gPSBfYSwgcmVzdFByb3BzID0gX19yZXN0KF9hLCBbXCJjb21wb25lbnRcIiwgXCJjaGlsZHJlblwiLCBcImNsYXNzTmFtZVwiLCBcInByZWZpeENsc1wiLCBcInR5cGVcIiwgXCJkaXNhYmxlZFwiLCBcInN0eWxlXCJdKTtcbiAgICAgICAgY29uc3QgeyByb3dzIH0gPSB0aGlzLmdldEVsbGlwc2lzKCk7XG4gICAgICAgIGNvbnN0IHRleHRQcm9wcyA9IG9taXQocmVzdFByb3BzLCBbXG4gICAgICAgICAgICAncHJlZml4Q2xzJyxcbiAgICAgICAgICAgICdlZGl0YWJsZScsXG4gICAgICAgICAgICAnY29weWFibGUnLFxuICAgICAgICAgICAgJ2VsbGlwc2lzJyxcbiAgICAgICAgICAgICdtYXJrJyxcbiAgICAgICAgICAgICd1bmRlcmxpbmUnLFxuICAgICAgICAgICAgJ21hcmsnLFxuICAgICAgICAgICAgJ2NvZGUnLFxuICAgICAgICAgICAgJ2RlbGV0ZScsXG4gICAgICAgICAgICAndW5kZXJsaW5lJyxcbiAgICAgICAgICAgICdzdHJvbmcnLFxuICAgICAgICAgICAgLi4uY29uZmlnQ29uc3VtZXJQcm9wcyxcbiAgICAgICAgXSk7XG4gICAgICAgIGNvbnN0IGNzc0VsbGlwc2lzID0gdGhpcy5jYW5Vc2VDU1NFbGxpcHNpcygpO1xuICAgICAgICBjb25zdCBjc3NUZXh0T3ZlcmZsb3cgPSByb3dzID09PSAxICYmIGNzc0VsbGlwc2lzO1xuICAgICAgICBjb25zdCBjc3NMaW5lQ2xhbXAgPSByb3dzICYmIHJvd3MgPiAxICYmIGNzc0VsbGlwc2lzO1xuICAgICAgICBsZXQgdGV4dE5vZGUgPSBjaGlsZHJlbjtcbiAgICAgICAgbGV0IGFyaWFMYWJlbCA9IG51bGw7XG4gICAgICAgIC8vIE9ubHkgdXNlIGpzIGVsbGlwc2lzIHdoZW4gY3NzIGVsbGlwc2lzIG5vdCBzdXBwb3J0XG4gICAgICAgIGlmIChyb3dzICYmIGlzRWxsaXBzaXMgJiYgIWV4cGFuZGVkICYmICFjc3NFbGxpcHNpcykge1xuICAgICAgICAgICAgYXJpYUxhYmVsID0gU3RyaW5nKGNoaWxkcmVuKTtcbiAgICAgICAgICAgIC8vIFdlIG1vdmUgZnVsbCBjb250ZW50IHRvIG91dGVyIGVsZW1lbnQgdG8gYXZvaWQgcmVwZWF0IHJlYWQgdGhlIGNvbnRlbnQgYnkgYWNjZXNzaWJpbGl0eVxuICAgICAgICAgICAgdGV4dE5vZGUgPSAoPHNwYW4gdGl0bGU9e1N0cmluZyhjaGlsZHJlbil9IGFyaWEtaGlkZGVuPVwidHJ1ZVwiPlxuICAgICAgICAgIHtlbGxpcHNpc0NvbnRlbnR9XG4gICAgICAgICAge0VMTElQU0lTX1NUUn1cbiAgICAgICAgPC9zcGFuPik7XG4gICAgICAgIH1cbiAgICAgICAgdGV4dE5vZGUgPSB3cmFwcGVyRGVjb3JhdGlvbnModGhpcy5wcm9wcywgdGV4dE5vZGUpO1xuICAgICAgICByZXR1cm4gKDxMb2NhbGVSZWNlaXZlciBjb21wb25lbnROYW1lPVwiVGV4dFwiPlxuICAgICAgICB7KHsgZWRpdCwgY29weTogY29weVN0ciwgY29waWVkLCBleHBhbmQgfSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5lZGl0U3RyID0gZWRpdDtcbiAgICAgICAgICAgIHRoaXMuY29weVN0ciA9IGNvcHlTdHI7XG4gICAgICAgICAgICB0aGlzLmNvcGllZFN0ciA9IGNvcGllZDtcbiAgICAgICAgICAgIHRoaXMuZXhwYW5kU3RyID0gZXhwYW5kO1xuICAgICAgICAgICAgcmV0dXJuICg8UmVzaXplT2JzZXJ2ZXIgb25SZXNpemU9e3RoaXMucmVzaXplT25OZXh0RnJhbWV9IGRpc2FibGVkPXshcm93c30+XG4gICAgICAgICAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhjbGFzc05hbWUsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke3R5cGV9YF06IHR5cGUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZGlzYWJsZWRgXTogZGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZWxsaXBzaXNgXTogcm93cyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1lbGxpcHNpcy1zaW5nbGUtbGluZWBdOiBjc3NUZXh0T3ZlcmZsb3csXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZWxsaXBzaXMtbXVsdGlwbGUtbGluZWBdOiBjc3NMaW5lQ2xhbXAsXG4gICAgICAgICAgICB9KX0gc3R5bGU9e09iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgc3R5bGUpLCB7IFdlYmtpdExpbmVDbGFtcDogY3NzTGluZUNsYW1wID8gcm93cyA6IG51bGwgfSl9IGNvbXBvbmVudD17Y29tcG9uZW50fSByZWY9e3RoaXMuc2V0Q29udGVudFJlZn0gYXJpYS1sYWJlbD17YXJpYUxhYmVsfSB7Li4udGV4dFByb3BzfT5cbiAgICAgICAgICAgICAgICB7dGV4dE5vZGV9XG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVyT3BlcmF0aW9ucygpfVxuICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8L1Jlc2l6ZU9ic2VydmVyPik7XG4gICAgICAgIH19XG4gICAgICA8L0xvY2FsZVJlY2VpdmVyPik7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgeyBlZGl0aW5nIH0gPSB0aGlzLmdldEVkaXRhYmxlKCk7XG4gICAgICAgIGlmIChlZGl0aW5nKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJFZGl0SW5wdXQoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJDb250ZW50KCk7XG4gICAgfVxufVxuQmFzZS5kZWZhdWx0UHJvcHMgPSB7XG4gICAgY2hpbGRyZW46ICcnLFxufTtcbnBvbHlmaWxsKEJhc2UpO1xuZXhwb3J0IGRlZmF1bHQgd2l0aENvbmZpZ0NvbnN1bWVyKHtcbiAgICBwcmVmaXhDbHM6ICd0eXBvZ3JhcGh5Jyxcbn0pKEJhc2UpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUNBO0FBU0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWpCQTtBQUNBO0FBQ0E7QUFrQkE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQS9CQTtBQUNBO0FBQ0E7QUFnQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFKQTtBQVBBO0FBQ0E7QUFlQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBN0RBO0FBQ0E7QUFDQTtBQWtFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQUNBO0FBckVBO0FBMkVBO0FBQ0E7OztBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFWQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBVUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFFQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUhBO0FBQUE7QUFDQTtBQUdBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFOQTtBQVBBO0FBb0JBOzs7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBcEtBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTs7OztBQWpGQTtBQUNBO0FBa1BBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/Base.js
