/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ./graphic */ "./node_modules/echarts/lib/util/graphic.js");

var BoundingRect = __webpack_require__(/*! zrender/lib/core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Symbol factory

/**
 * Triangle shape
 * @inner
 */


var Triangle = graphic.extendShape({
  type: 'triangle',
  shape: {
    cx: 0,
    cy: 0,
    width: 0,
    height: 0
  },
  buildPath: function buildPath(path, shape) {
    var cx = shape.cx;
    var cy = shape.cy;
    var width = shape.width / 2;
    var height = shape.height / 2;
    path.moveTo(cx, cy - height);
    path.lineTo(cx + width, cy + height);
    path.lineTo(cx - width, cy + height);
    path.closePath();
  }
});
/**
 * Diamond shape
 * @inner
 */

var Diamond = graphic.extendShape({
  type: 'diamond',
  shape: {
    cx: 0,
    cy: 0,
    width: 0,
    height: 0
  },
  buildPath: function buildPath(path, shape) {
    var cx = shape.cx;
    var cy = shape.cy;
    var width = shape.width / 2;
    var height = shape.height / 2;
    path.moveTo(cx, cy - height);
    path.lineTo(cx + width, cy);
    path.lineTo(cx, cy + height);
    path.lineTo(cx - width, cy);
    path.closePath();
  }
});
/**
 * Pin shape
 * @inner
 */

var Pin = graphic.extendShape({
  type: 'pin',
  shape: {
    // x, y on the cusp
    x: 0,
    y: 0,
    width: 0,
    height: 0
  },
  buildPath: function buildPath(path, shape) {
    var x = shape.x;
    var y = shape.y;
    var w = shape.width / 5 * 3; // Height must be larger than width

    var h = Math.max(w, shape.height);
    var r = w / 2; // Dist on y with tangent point and circle center

    var dy = r * r / (h - r);
    var cy = y - h + r + dy;
    var angle = Math.asin(dy / r); // Dist on x with tangent point and circle center

    var dx = Math.cos(angle) * r;
    var tanX = Math.sin(angle);
    var tanY = Math.cos(angle);
    var cpLen = r * 0.6;
    var cpLen2 = r * 0.7;
    path.moveTo(x - dx, cy + dy);
    path.arc(x, cy, r, Math.PI - angle, Math.PI * 2 + angle);
    path.bezierCurveTo(x + dx - tanX * cpLen, cy + dy + tanY * cpLen, x, y - cpLen2, x, y);
    path.bezierCurveTo(x, y - cpLen2, x - dx + tanX * cpLen, cy + dy + tanY * cpLen, x - dx, cy + dy);
    path.closePath();
  }
});
/**
 * Arrow shape
 * @inner
 */

var Arrow = graphic.extendShape({
  type: 'arrow',
  shape: {
    x: 0,
    y: 0,
    width: 0,
    height: 0
  },
  buildPath: function buildPath(ctx, shape) {
    var height = shape.height;
    var width = shape.width;
    var x = shape.x;
    var y = shape.y;
    var dx = width / 3 * 2;
    ctx.moveTo(x, y);
    ctx.lineTo(x + dx, y + height);
    ctx.lineTo(x, y + height / 4 * 3);
    ctx.lineTo(x - dx, y + height);
    ctx.lineTo(x, y);
    ctx.closePath();
  }
});
/**
 * Map of path contructors
 * @type {Object.<string, module:zrender/graphic/Path>}
 */

var symbolCtors = {
  line: graphic.Line,
  rect: graphic.Rect,
  roundRect: graphic.Rect,
  square: graphic.Rect,
  circle: graphic.Circle,
  diamond: Diamond,
  pin: Pin,
  arrow: Arrow,
  triangle: Triangle
};
var symbolShapeMakers = {
  line: function line(x, y, w, h, shape) {
    // FIXME
    shape.x1 = x;
    shape.y1 = y + h / 2;
    shape.x2 = x + w;
    shape.y2 = y + h / 2;
  },
  rect: function rect(x, y, w, h, shape) {
    shape.x = x;
    shape.y = y;
    shape.width = w;
    shape.height = h;
  },
  roundRect: function roundRect(x, y, w, h, shape) {
    shape.x = x;
    shape.y = y;
    shape.width = w;
    shape.height = h;
    shape.r = Math.min(w, h) / 4;
  },
  square: function square(x, y, w, h, shape) {
    var size = Math.min(w, h);
    shape.x = x;
    shape.y = y;
    shape.width = size;
    shape.height = size;
  },
  circle: function circle(x, y, w, h, shape) {
    // Put circle in the center of square
    shape.cx = x + w / 2;
    shape.cy = y + h / 2;
    shape.r = Math.min(w, h) / 2;
  },
  diamond: function diamond(x, y, w, h, shape) {
    shape.cx = x + w / 2;
    shape.cy = y + h / 2;
    shape.width = w;
    shape.height = h;
  },
  pin: function pin(x, y, w, h, shape) {
    shape.x = x + w / 2;
    shape.y = y + h / 2;
    shape.width = w;
    shape.height = h;
  },
  arrow: function arrow(x, y, w, h, shape) {
    shape.x = x + w / 2;
    shape.y = y + h / 2;
    shape.width = w;
    shape.height = h;
  },
  triangle: function triangle(x, y, w, h, shape) {
    shape.cx = x + w / 2;
    shape.cy = y + h / 2;
    shape.width = w;
    shape.height = h;
  }
};
var symbolBuildProxies = {};
zrUtil.each(symbolCtors, function (Ctor, name) {
  symbolBuildProxies[name] = new Ctor();
});
var SymbolClz = graphic.extendShape({
  type: 'symbol',
  shape: {
    symbolType: '',
    x: 0,
    y: 0,
    width: 0,
    height: 0
  },
  beforeBrush: function beforeBrush() {
    var style = this.style;
    var shape = this.shape; // FIXME

    if (shape.symbolType === 'pin' && style.textPosition === 'inside') {
      style.textPosition = ['50%', '40%'];
      style.textAlign = 'center';
      style.textVerticalAlign = 'middle';
    }
  },
  buildPath: function buildPath(ctx, shape, inBundle) {
    var symbolType = shape.symbolType;
    var proxySymbol = symbolBuildProxies[symbolType];

    if (shape.symbolType !== 'none') {
      if (!proxySymbol) {
        // Default rect
        symbolType = 'rect';
        proxySymbol = symbolBuildProxies[symbolType];
      }

      symbolShapeMakers[symbolType](shape.x, shape.y, shape.width, shape.height, proxySymbol.shape);
      proxySymbol.buildPath(ctx, proxySymbol.shape, inBundle);
    }
  }
}); // Provide setColor helper method to avoid determine if set the fill or stroke outside

function symbolPathSetColor(color, innerColor) {
  if (this.type !== 'image') {
    var symbolStyle = this.style;
    var symbolShape = this.shape;

    if (symbolShape && symbolShape.symbolType === 'line') {
      symbolStyle.stroke = color;
    } else if (this.__isEmptyBrush) {
      symbolStyle.stroke = color;
      symbolStyle.fill = innerColor || '#fff';
    } else {
      // FIXME 判断图形默认是填充还是描边，使用 onlyStroke ?
      symbolStyle.fill && (symbolStyle.fill = color);
      symbolStyle.stroke && (symbolStyle.stroke = color);
    }

    this.dirty(false);
  }
}
/**
 * Create a symbol element with given symbol configuration: shape, x, y, width, height, color
 * @param {string} symbolType
 * @param {number} x
 * @param {number} y
 * @param {number} w
 * @param {number} h
 * @param {string} color
 * @param {boolean} [keepAspect=false] whether to keep the ratio of w/h,
 *                            for path and image only.
 */


function createSymbol(symbolType, x, y, w, h, color, keepAspect) {
  // TODO Support image object, DynamicImage.
  var isEmpty = symbolType.indexOf('empty') === 0;

  if (isEmpty) {
    symbolType = symbolType.substr(5, 1).toLowerCase() + symbolType.substr(6);
  }

  var symbolPath;

  if (symbolType.indexOf('image://') === 0) {
    symbolPath = graphic.makeImage(symbolType.slice(8), new BoundingRect(x, y, w, h), keepAspect ? 'center' : 'cover');
  } else if (symbolType.indexOf('path://') === 0) {
    symbolPath = graphic.makePath(symbolType.slice(7), {}, new BoundingRect(x, y, w, h), keepAspect ? 'center' : 'cover');
  } else {
    symbolPath = new SymbolClz({
      shape: {
        symbolType: symbolType,
        x: x,
        y: y,
        width: w,
        height: h
      }
    });
  }

  symbolPath.__isEmptyBrush = isEmpty;
  symbolPath.setColor = symbolPathSetColor;
  symbolPath.setColor(color);
  return symbolPath;
}

exports.createSymbol = createSymbol;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9zeW1ib2wuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi91dGlsL3N5bWJvbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4vZ3JhcGhpY1wiKTtcblxudmFyIEJvdW5kaW5nUmVjdCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL0JvdW5kaW5nUmVjdFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gU3ltYm9sIGZhY3RvcnlcblxuLyoqXG4gKiBUcmlhbmdsZSBzaGFwZVxuICogQGlubmVyXG4gKi9cbnZhciBUcmlhbmdsZSA9IGdyYXBoaWMuZXh0ZW5kU2hhcGUoe1xuICB0eXBlOiAndHJpYW5nbGUnLFxuICBzaGFwZToge1xuICAgIGN4OiAwLFxuICAgIGN5OiAwLFxuICAgIHdpZHRoOiAwLFxuICAgIGhlaWdodDogMFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChwYXRoLCBzaGFwZSkge1xuICAgIHZhciBjeCA9IHNoYXBlLmN4O1xuICAgIHZhciBjeSA9IHNoYXBlLmN5O1xuICAgIHZhciB3aWR0aCA9IHNoYXBlLndpZHRoIC8gMjtcbiAgICB2YXIgaGVpZ2h0ID0gc2hhcGUuaGVpZ2h0IC8gMjtcbiAgICBwYXRoLm1vdmVUbyhjeCwgY3kgLSBoZWlnaHQpO1xuICAgIHBhdGgubGluZVRvKGN4ICsgd2lkdGgsIGN5ICsgaGVpZ2h0KTtcbiAgICBwYXRoLmxpbmVUbyhjeCAtIHdpZHRoLCBjeSArIGhlaWdodCk7XG4gICAgcGF0aC5jbG9zZVBhdGgoKTtcbiAgfVxufSk7XG4vKipcbiAqIERpYW1vbmQgc2hhcGVcbiAqIEBpbm5lclxuICovXG5cbnZhciBEaWFtb25kID0gZ3JhcGhpYy5leHRlbmRTaGFwZSh7XG4gIHR5cGU6ICdkaWFtb25kJyxcbiAgc2hhcGU6IHtcbiAgICBjeDogMCxcbiAgICBjeTogMCxcbiAgICB3aWR0aDogMCxcbiAgICBoZWlnaHQ6IDBcbiAgfSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAocGF0aCwgc2hhcGUpIHtcbiAgICB2YXIgY3ggPSBzaGFwZS5jeDtcbiAgICB2YXIgY3kgPSBzaGFwZS5jeTtcbiAgICB2YXIgd2lkdGggPSBzaGFwZS53aWR0aCAvIDI7XG4gICAgdmFyIGhlaWdodCA9IHNoYXBlLmhlaWdodCAvIDI7XG4gICAgcGF0aC5tb3ZlVG8oY3gsIGN5IC0gaGVpZ2h0KTtcbiAgICBwYXRoLmxpbmVUbyhjeCArIHdpZHRoLCBjeSk7XG4gICAgcGF0aC5saW5lVG8oY3gsIGN5ICsgaGVpZ2h0KTtcbiAgICBwYXRoLmxpbmVUbyhjeCAtIHdpZHRoLCBjeSk7XG4gICAgcGF0aC5jbG9zZVBhdGgoKTtcbiAgfVxufSk7XG4vKipcbiAqIFBpbiBzaGFwZVxuICogQGlubmVyXG4gKi9cblxudmFyIFBpbiA9IGdyYXBoaWMuZXh0ZW5kU2hhcGUoe1xuICB0eXBlOiAncGluJyxcbiAgc2hhcGU6IHtcbiAgICAvLyB4LCB5IG9uIHRoZSBjdXNwXG4gICAgeDogMCxcbiAgICB5OiAwLFxuICAgIHdpZHRoOiAwLFxuICAgIGhlaWdodDogMFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChwYXRoLCBzaGFwZSkge1xuICAgIHZhciB4ID0gc2hhcGUueDtcbiAgICB2YXIgeSA9IHNoYXBlLnk7XG4gICAgdmFyIHcgPSBzaGFwZS53aWR0aCAvIDUgKiAzOyAvLyBIZWlnaHQgbXVzdCBiZSBsYXJnZXIgdGhhbiB3aWR0aFxuXG4gICAgdmFyIGggPSBNYXRoLm1heCh3LCBzaGFwZS5oZWlnaHQpO1xuICAgIHZhciByID0gdyAvIDI7IC8vIERpc3Qgb24geSB3aXRoIHRhbmdlbnQgcG9pbnQgYW5kIGNpcmNsZSBjZW50ZXJcblxuICAgIHZhciBkeSA9IHIgKiByIC8gKGggLSByKTtcbiAgICB2YXIgY3kgPSB5IC0gaCArIHIgKyBkeTtcbiAgICB2YXIgYW5nbGUgPSBNYXRoLmFzaW4oZHkgLyByKTsgLy8gRGlzdCBvbiB4IHdpdGggdGFuZ2VudCBwb2ludCBhbmQgY2lyY2xlIGNlbnRlclxuXG4gICAgdmFyIGR4ID0gTWF0aC5jb3MoYW5nbGUpICogcjtcbiAgICB2YXIgdGFuWCA9IE1hdGguc2luKGFuZ2xlKTtcbiAgICB2YXIgdGFuWSA9IE1hdGguY29zKGFuZ2xlKTtcbiAgICB2YXIgY3BMZW4gPSByICogMC42O1xuICAgIHZhciBjcExlbjIgPSByICogMC43O1xuICAgIHBhdGgubW92ZVRvKHggLSBkeCwgY3kgKyBkeSk7XG4gICAgcGF0aC5hcmMoeCwgY3ksIHIsIE1hdGguUEkgLSBhbmdsZSwgTWF0aC5QSSAqIDIgKyBhbmdsZSk7XG4gICAgcGF0aC5iZXppZXJDdXJ2ZVRvKHggKyBkeCAtIHRhblggKiBjcExlbiwgY3kgKyBkeSArIHRhblkgKiBjcExlbiwgeCwgeSAtIGNwTGVuMiwgeCwgeSk7XG4gICAgcGF0aC5iZXppZXJDdXJ2ZVRvKHgsIHkgLSBjcExlbjIsIHggLSBkeCArIHRhblggKiBjcExlbiwgY3kgKyBkeSArIHRhblkgKiBjcExlbiwgeCAtIGR4LCBjeSArIGR5KTtcbiAgICBwYXRoLmNsb3NlUGF0aCgpO1xuICB9XG59KTtcbi8qKlxuICogQXJyb3cgc2hhcGVcbiAqIEBpbm5lclxuICovXG5cbnZhciBBcnJvdyA9IGdyYXBoaWMuZXh0ZW5kU2hhcGUoe1xuICB0eXBlOiAnYXJyb3cnLFxuICBzaGFwZToge1xuICAgIHg6IDAsXG4gICAgeTogMCxcbiAgICB3aWR0aDogMCxcbiAgICBoZWlnaHQ6IDBcbiAgfSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSkge1xuICAgIHZhciBoZWlnaHQgPSBzaGFwZS5oZWlnaHQ7XG4gICAgdmFyIHdpZHRoID0gc2hhcGUud2lkdGg7XG4gICAgdmFyIHggPSBzaGFwZS54O1xuICAgIHZhciB5ID0gc2hhcGUueTtcbiAgICB2YXIgZHggPSB3aWR0aCAvIDMgKiAyO1xuICAgIGN0eC5tb3ZlVG8oeCwgeSk7XG4gICAgY3R4LmxpbmVUbyh4ICsgZHgsIHkgKyBoZWlnaHQpO1xuICAgIGN0eC5saW5lVG8oeCwgeSArIGhlaWdodCAvIDQgKiAzKTtcbiAgICBjdHgubGluZVRvKHggLSBkeCwgeSArIGhlaWdodCk7XG4gICAgY3R4LmxpbmVUbyh4LCB5KTtcbiAgICBjdHguY2xvc2VQYXRoKCk7XG4gIH1cbn0pO1xuLyoqXG4gKiBNYXAgb2YgcGF0aCBjb250cnVjdG9yc1xuICogQHR5cGUge09iamVjdC48c3RyaW5nLCBtb2R1bGU6enJlbmRlci9ncmFwaGljL1BhdGg+fVxuICovXG5cbnZhciBzeW1ib2xDdG9ycyA9IHtcbiAgbGluZTogZ3JhcGhpYy5MaW5lLFxuICByZWN0OiBncmFwaGljLlJlY3QsXG4gIHJvdW5kUmVjdDogZ3JhcGhpYy5SZWN0LFxuICBzcXVhcmU6IGdyYXBoaWMuUmVjdCxcbiAgY2lyY2xlOiBncmFwaGljLkNpcmNsZSxcbiAgZGlhbW9uZDogRGlhbW9uZCxcbiAgcGluOiBQaW4sXG4gIGFycm93OiBBcnJvdyxcbiAgdHJpYW5nbGU6IFRyaWFuZ2xlXG59O1xudmFyIHN5bWJvbFNoYXBlTWFrZXJzID0ge1xuICBsaW5lOiBmdW5jdGlvbiAoeCwgeSwgdywgaCwgc2hhcGUpIHtcbiAgICAvLyBGSVhNRVxuICAgIHNoYXBlLngxID0geDtcbiAgICBzaGFwZS55MSA9IHkgKyBoIC8gMjtcbiAgICBzaGFwZS54MiA9IHggKyB3O1xuICAgIHNoYXBlLnkyID0geSArIGggLyAyO1xuICB9LFxuICByZWN0OiBmdW5jdGlvbiAoeCwgeSwgdywgaCwgc2hhcGUpIHtcbiAgICBzaGFwZS54ID0geDtcbiAgICBzaGFwZS55ID0geTtcbiAgICBzaGFwZS53aWR0aCA9IHc7XG4gICAgc2hhcGUuaGVpZ2h0ID0gaDtcbiAgfSxcbiAgcm91bmRSZWN0OiBmdW5jdGlvbiAoeCwgeSwgdywgaCwgc2hhcGUpIHtcbiAgICBzaGFwZS54ID0geDtcbiAgICBzaGFwZS55ID0geTtcbiAgICBzaGFwZS53aWR0aCA9IHc7XG4gICAgc2hhcGUuaGVpZ2h0ID0gaDtcbiAgICBzaGFwZS5yID0gTWF0aC5taW4odywgaCkgLyA0O1xuICB9LFxuICBzcXVhcmU6IGZ1bmN0aW9uICh4LCB5LCB3LCBoLCBzaGFwZSkge1xuICAgIHZhciBzaXplID0gTWF0aC5taW4odywgaCk7XG4gICAgc2hhcGUueCA9IHg7XG4gICAgc2hhcGUueSA9IHk7XG4gICAgc2hhcGUud2lkdGggPSBzaXplO1xuICAgIHNoYXBlLmhlaWdodCA9IHNpemU7XG4gIH0sXG4gIGNpcmNsZTogZnVuY3Rpb24gKHgsIHksIHcsIGgsIHNoYXBlKSB7XG4gICAgLy8gUHV0IGNpcmNsZSBpbiB0aGUgY2VudGVyIG9mIHNxdWFyZVxuICAgIHNoYXBlLmN4ID0geCArIHcgLyAyO1xuICAgIHNoYXBlLmN5ID0geSArIGggLyAyO1xuICAgIHNoYXBlLnIgPSBNYXRoLm1pbih3LCBoKSAvIDI7XG4gIH0sXG4gIGRpYW1vbmQ6IGZ1bmN0aW9uICh4LCB5LCB3LCBoLCBzaGFwZSkge1xuICAgIHNoYXBlLmN4ID0geCArIHcgLyAyO1xuICAgIHNoYXBlLmN5ID0geSArIGggLyAyO1xuICAgIHNoYXBlLndpZHRoID0gdztcbiAgICBzaGFwZS5oZWlnaHQgPSBoO1xuICB9LFxuICBwaW46IGZ1bmN0aW9uICh4LCB5LCB3LCBoLCBzaGFwZSkge1xuICAgIHNoYXBlLnggPSB4ICsgdyAvIDI7XG4gICAgc2hhcGUueSA9IHkgKyBoIC8gMjtcbiAgICBzaGFwZS53aWR0aCA9IHc7XG4gICAgc2hhcGUuaGVpZ2h0ID0gaDtcbiAgfSxcbiAgYXJyb3c6IGZ1bmN0aW9uICh4LCB5LCB3LCBoLCBzaGFwZSkge1xuICAgIHNoYXBlLnggPSB4ICsgdyAvIDI7XG4gICAgc2hhcGUueSA9IHkgKyBoIC8gMjtcbiAgICBzaGFwZS53aWR0aCA9IHc7XG4gICAgc2hhcGUuaGVpZ2h0ID0gaDtcbiAgfSxcbiAgdHJpYW5nbGU6IGZ1bmN0aW9uICh4LCB5LCB3LCBoLCBzaGFwZSkge1xuICAgIHNoYXBlLmN4ID0geCArIHcgLyAyO1xuICAgIHNoYXBlLmN5ID0geSArIGggLyAyO1xuICAgIHNoYXBlLndpZHRoID0gdztcbiAgICBzaGFwZS5oZWlnaHQgPSBoO1xuICB9XG59O1xudmFyIHN5bWJvbEJ1aWxkUHJveGllcyA9IHt9O1xuenJVdGlsLmVhY2goc3ltYm9sQ3RvcnMsIGZ1bmN0aW9uIChDdG9yLCBuYW1lKSB7XG4gIHN5bWJvbEJ1aWxkUHJveGllc1tuYW1lXSA9IG5ldyBDdG9yKCk7XG59KTtcbnZhciBTeW1ib2xDbHogPSBncmFwaGljLmV4dGVuZFNoYXBlKHtcbiAgdHlwZTogJ3N5bWJvbCcsXG4gIHNoYXBlOiB7XG4gICAgc3ltYm9sVHlwZTogJycsXG4gICAgeDogMCxcbiAgICB5OiAwLFxuICAgIHdpZHRoOiAwLFxuICAgIGhlaWdodDogMFxuICB9LFxuICBiZWZvcmVCcnVzaDogZnVuY3Rpb24gKCkge1xuICAgIHZhciBzdHlsZSA9IHRoaXMuc3R5bGU7XG4gICAgdmFyIHNoYXBlID0gdGhpcy5zaGFwZTsgLy8gRklYTUVcblxuICAgIGlmIChzaGFwZS5zeW1ib2xUeXBlID09PSAncGluJyAmJiBzdHlsZS50ZXh0UG9zaXRpb24gPT09ICdpbnNpZGUnKSB7XG4gICAgICBzdHlsZS50ZXh0UG9zaXRpb24gPSBbJzUwJScsICc0MCUnXTtcbiAgICAgIHN0eWxlLnRleHRBbGlnbiA9ICdjZW50ZXInO1xuICAgICAgc3R5bGUudGV4dFZlcnRpY2FsQWxpZ24gPSAnbWlkZGxlJztcbiAgICB9XG4gIH0sXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUsIGluQnVuZGxlKSB7XG4gICAgdmFyIHN5bWJvbFR5cGUgPSBzaGFwZS5zeW1ib2xUeXBlO1xuICAgIHZhciBwcm94eVN5bWJvbCA9IHN5bWJvbEJ1aWxkUHJveGllc1tzeW1ib2xUeXBlXTtcblxuICAgIGlmIChzaGFwZS5zeW1ib2xUeXBlICE9PSAnbm9uZScpIHtcbiAgICAgIGlmICghcHJveHlTeW1ib2wpIHtcbiAgICAgICAgLy8gRGVmYXVsdCByZWN0XG4gICAgICAgIHN5bWJvbFR5cGUgPSAncmVjdCc7XG4gICAgICAgIHByb3h5U3ltYm9sID0gc3ltYm9sQnVpbGRQcm94aWVzW3N5bWJvbFR5cGVdO1xuICAgICAgfVxuXG4gICAgICBzeW1ib2xTaGFwZU1ha2Vyc1tzeW1ib2xUeXBlXShzaGFwZS54LCBzaGFwZS55LCBzaGFwZS53aWR0aCwgc2hhcGUuaGVpZ2h0LCBwcm94eVN5bWJvbC5zaGFwZSk7XG4gICAgICBwcm94eVN5bWJvbC5idWlsZFBhdGgoY3R4LCBwcm94eVN5bWJvbC5zaGFwZSwgaW5CdW5kbGUpO1xuICAgIH1cbiAgfVxufSk7IC8vIFByb3ZpZGUgc2V0Q29sb3IgaGVscGVyIG1ldGhvZCB0byBhdm9pZCBkZXRlcm1pbmUgaWYgc2V0IHRoZSBmaWxsIG9yIHN0cm9rZSBvdXRzaWRlXG5cbmZ1bmN0aW9uIHN5bWJvbFBhdGhTZXRDb2xvcihjb2xvciwgaW5uZXJDb2xvcikge1xuICBpZiAodGhpcy50eXBlICE9PSAnaW1hZ2UnKSB7XG4gICAgdmFyIHN5bWJvbFN0eWxlID0gdGhpcy5zdHlsZTtcbiAgICB2YXIgc3ltYm9sU2hhcGUgPSB0aGlzLnNoYXBlO1xuXG4gICAgaWYgKHN5bWJvbFNoYXBlICYmIHN5bWJvbFNoYXBlLnN5bWJvbFR5cGUgPT09ICdsaW5lJykge1xuICAgICAgc3ltYm9sU3R5bGUuc3Ryb2tlID0gY29sb3I7XG4gICAgfSBlbHNlIGlmICh0aGlzLl9faXNFbXB0eUJydXNoKSB7XG4gICAgICBzeW1ib2xTdHlsZS5zdHJva2UgPSBjb2xvcjtcbiAgICAgIHN5bWJvbFN0eWxlLmZpbGwgPSBpbm5lckNvbG9yIHx8ICcjZmZmJztcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gRklYTUUg5Yik5pat5Zu+5b2i6buY6K6k5piv5aGr5YWF6L+Y5piv5o+P6L6577yM5L2/55SoIG9ubHlTdHJva2UgP1xuICAgICAgc3ltYm9sU3R5bGUuZmlsbCAmJiAoc3ltYm9sU3R5bGUuZmlsbCA9IGNvbG9yKTtcbiAgICAgIHN5bWJvbFN0eWxlLnN0cm9rZSAmJiAoc3ltYm9sU3R5bGUuc3Ryb2tlID0gY29sb3IpO1xuICAgIH1cblxuICAgIHRoaXMuZGlydHkoZmFsc2UpO1xuICB9XG59XG4vKipcbiAqIENyZWF0ZSBhIHN5bWJvbCBlbGVtZW50IHdpdGggZ2l2ZW4gc3ltYm9sIGNvbmZpZ3VyYXRpb246IHNoYXBlLCB4LCB5LCB3aWR0aCwgaGVpZ2h0LCBjb2xvclxuICogQHBhcmFtIHtzdHJpbmd9IHN5bWJvbFR5cGVcbiAqIEBwYXJhbSB7bnVtYmVyfSB4XG4gKiBAcGFyYW0ge251bWJlcn0geVxuICogQHBhcmFtIHtudW1iZXJ9IHdcbiAqIEBwYXJhbSB7bnVtYmVyfSBoXG4gKiBAcGFyYW0ge3N0cmluZ30gY29sb3JcbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW2tlZXBBc3BlY3Q9ZmFsc2VdIHdoZXRoZXIgdG8ga2VlcCB0aGUgcmF0aW8gb2Ygdy9oLFxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIHBhdGggYW5kIGltYWdlIG9ubHkuXG4gKi9cblxuXG5mdW5jdGlvbiBjcmVhdGVTeW1ib2woc3ltYm9sVHlwZSwgeCwgeSwgdywgaCwgY29sb3IsIGtlZXBBc3BlY3QpIHtcbiAgLy8gVE9ETyBTdXBwb3J0IGltYWdlIG9iamVjdCwgRHluYW1pY0ltYWdlLlxuICB2YXIgaXNFbXB0eSA9IHN5bWJvbFR5cGUuaW5kZXhPZignZW1wdHknKSA9PT0gMDtcblxuICBpZiAoaXNFbXB0eSkge1xuICAgIHN5bWJvbFR5cGUgPSBzeW1ib2xUeXBlLnN1YnN0cig1LCAxKS50b0xvd2VyQ2FzZSgpICsgc3ltYm9sVHlwZS5zdWJzdHIoNik7XG4gIH1cblxuICB2YXIgc3ltYm9sUGF0aDtcblxuICBpZiAoc3ltYm9sVHlwZS5pbmRleE9mKCdpbWFnZTovLycpID09PSAwKSB7XG4gICAgc3ltYm9sUGF0aCA9IGdyYXBoaWMubWFrZUltYWdlKHN5bWJvbFR5cGUuc2xpY2UoOCksIG5ldyBCb3VuZGluZ1JlY3QoeCwgeSwgdywgaCksIGtlZXBBc3BlY3QgPyAnY2VudGVyJyA6ICdjb3ZlcicpO1xuICB9IGVsc2UgaWYgKHN5bWJvbFR5cGUuaW5kZXhPZigncGF0aDovLycpID09PSAwKSB7XG4gICAgc3ltYm9sUGF0aCA9IGdyYXBoaWMubWFrZVBhdGgoc3ltYm9sVHlwZS5zbGljZSg3KSwge30sIG5ldyBCb3VuZGluZ1JlY3QoeCwgeSwgdywgaCksIGtlZXBBc3BlY3QgPyAnY2VudGVyJyA6ICdjb3ZlcicpO1xuICB9IGVsc2Uge1xuICAgIHN5bWJvbFBhdGggPSBuZXcgU3ltYm9sQ2x6KHtcbiAgICAgIHNoYXBlOiB7XG4gICAgICAgIHN5bWJvbFR5cGU6IHN5bWJvbFR5cGUsXG4gICAgICAgIHg6IHgsXG4gICAgICAgIHk6IHksXG4gICAgICAgIHdpZHRoOiB3LFxuICAgICAgICBoZWlnaHQ6IGhcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHN5bWJvbFBhdGguX19pc0VtcHR5QnJ1c2ggPSBpc0VtcHR5O1xuICBzeW1ib2xQYXRoLnNldENvbG9yID0gc3ltYm9sUGF0aFNldENvbG9yO1xuICBzeW1ib2xQYXRoLnNldENvbG9yKGNvbG9yKTtcbiAgcmV0dXJuIHN5bWJvbFBhdGg7XG59XG5cbmV4cG9ydHMuY3JlYXRlU3ltYm9sID0gY3JlYXRlU3ltYm9sOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQW1CQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxCQTtBQW9CQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0JBO0FBaUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXBCQTtBQXNCQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF6REE7QUEyREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpDQTtBQUNBO0FBbUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/symbol.js
