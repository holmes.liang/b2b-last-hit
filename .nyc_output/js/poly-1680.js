/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var Path = __webpack_require__(/*! zrender/lib/graphic/Path */ "./node_modules/zrender/lib/graphic/Path.js");

var vec2 = __webpack_require__(/*! zrender/lib/core/vector */ "./node_modules/zrender/lib/core/vector.js");

var fixClipWithShadow = __webpack_require__(/*! zrender/lib/graphic/helper/fixClipWithShadow */ "./node_modules/zrender/lib/graphic/helper/fixClipWithShadow.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Poly path support NaN point


var vec2Min = vec2.min;
var vec2Max = vec2.max;
var scaleAndAdd = vec2.scaleAndAdd;
var v2Copy = vec2.copy; // Temporary variable

var v = [];
var cp0 = [];
var cp1 = [];

function isPointNull(p) {
  return isNaN(p[0]) || isNaN(p[1]);
}

function drawSegment(ctx, points, start, segLen, allLen, dir, smoothMin, smoothMax, smooth, smoothMonotone, connectNulls) {
  // if (smoothMonotone == null) {
  //     if (isMono(points, 'x')) {
  //         return drawMono(ctx, points, start, segLen, allLen,
  //             dir, smoothMin, smoothMax, smooth, 'x', connectNulls);
  //     }
  //     else if (isMono(points, 'y')) {
  //         return drawMono(ctx, points, start, segLen, allLen,
  //             dir, smoothMin, smoothMax, smooth, 'y', connectNulls);
  //     }
  //     else {
  //         return drawNonMono.apply(this, arguments);
  //     }
  // }
  // else if (smoothMonotone !== 'none' && isMono(points, smoothMonotone)) {
  //     return drawMono.apply(this, arguments);
  // }
  // else {
  //     return drawNonMono.apply(this, arguments);
  // }
  if (smoothMonotone === 'none' || !smoothMonotone) {
    return drawNonMono.apply(this, arguments);
  } else {
    return drawMono.apply(this, arguments);
  }
}
/**
 * Check if points is in monotone.
 *
 * @param {number[][]} points         Array of points which is in [x, y] form
 * @param {string}     smoothMonotone 'x', 'y', or 'none', stating for which
 *                                    dimension that is checking.
 *                                    If is 'none', `drawNonMono` should be
 *                                    called.
 *                                    If is undefined, either being monotone
 *                                    in 'x' or 'y' will call `drawMono`.
 */
// function isMono(points, smoothMonotone) {
//     if (points.length <= 1) {
//         return true;
//     }
//     var dim = smoothMonotone === 'x' ? 0 : 1;
//     var last = points[0][dim];
//     var lastDiff = 0;
//     for (var i = 1; i < points.length; ++i) {
//         var diff = points[i][dim] - last;
//         if (!isNaN(diff) && !isNaN(lastDiff)
//             && diff !== 0 && lastDiff !== 0
//             && ((diff >= 0) !== (lastDiff >= 0))
//         ) {
//             return false;
//         }
//         if (!isNaN(diff) && diff !== 0) {
//             lastDiff = diff;
//             last = points[i][dim];
//         }
//     }
//     return true;
// }

/**
 * Draw smoothed line in monotone, in which only vertical or horizontal bezier
 * control points will be used. This should be used when points are monotone
 * either in x or y dimension.
 */


function drawMono(ctx, points, start, segLen, allLen, dir, smoothMin, smoothMax, smooth, smoothMonotone, connectNulls) {
  var prevIdx = 0;
  var idx = start;

  for (var k = 0; k < segLen; k++) {
    var p = points[idx];

    if (idx >= allLen || idx < 0) {
      break;
    }

    if (isPointNull(p)) {
      if (connectNulls) {
        idx += dir;
        continue;
      }

      break;
    }

    if (idx === start) {
      ctx[dir > 0 ? 'moveTo' : 'lineTo'](p[0], p[1]);
    } else {
      if (smooth > 0) {
        var prevP = points[prevIdx];
        var dim = smoothMonotone === 'y' ? 1 : 0; // Length of control point to p, either in x or y, but not both

        var ctrlLen = (p[dim] - prevP[dim]) * smooth;
        v2Copy(cp0, prevP);
        cp0[dim] = prevP[dim] + ctrlLen;
        v2Copy(cp1, p);
        cp1[dim] = p[dim] - ctrlLen;
        ctx.bezierCurveTo(cp0[0], cp0[1], cp1[0], cp1[1], p[0], p[1]);
      } else {
        ctx.lineTo(p[0], p[1]);
      }
    }

    prevIdx = idx;
    idx += dir;
  }

  return k;
}
/**
 * Draw smoothed line in non-monotone, in may cause undesired curve in extreme
 * situations. This should be used when points are non-monotone neither in x or
 * y dimension.
 */


function drawNonMono(ctx, points, start, segLen, allLen, dir, smoothMin, smoothMax, smooth, smoothMonotone, connectNulls) {
  var prevIdx = 0;
  var idx = start;

  for (var k = 0; k < segLen; k++) {
    var p = points[idx];

    if (idx >= allLen || idx < 0) {
      break;
    }

    if (isPointNull(p)) {
      if (connectNulls) {
        idx += dir;
        continue;
      }

      break;
    }

    if (idx === start) {
      ctx[dir > 0 ? 'moveTo' : 'lineTo'](p[0], p[1]);
      v2Copy(cp0, p);
    } else {
      if (smooth > 0) {
        var nextIdx = idx + dir;
        var nextP = points[nextIdx];

        if (connectNulls) {
          // Find next point not null
          while (nextP && isPointNull(points[nextIdx])) {
            nextIdx += dir;
            nextP = points[nextIdx];
          }
        }

        var ratioNextSeg = 0.5;
        var prevP = points[prevIdx];
        var nextP = points[nextIdx]; // Last point

        if (!nextP || isPointNull(nextP)) {
          v2Copy(cp1, p);
        } else {
          // If next data is null in not connect case
          if (isPointNull(nextP) && !connectNulls) {
            nextP = p;
          }

          vec2.sub(v, nextP, prevP);
          var lenPrevSeg;
          var lenNextSeg;

          if (smoothMonotone === 'x' || smoothMonotone === 'y') {
            var dim = smoothMonotone === 'x' ? 0 : 1;
            lenPrevSeg = Math.abs(p[dim] - prevP[dim]);
            lenNextSeg = Math.abs(p[dim] - nextP[dim]);
          } else {
            lenPrevSeg = vec2.dist(p, prevP);
            lenNextSeg = vec2.dist(p, nextP);
          } // Use ratio of seg length


          ratioNextSeg = lenNextSeg / (lenNextSeg + lenPrevSeg);
          scaleAndAdd(cp1, p, v, -smooth * (1 - ratioNextSeg));
        } // Smooth constraint


        vec2Min(cp0, cp0, smoothMax);
        vec2Max(cp0, cp0, smoothMin);
        vec2Min(cp1, cp1, smoothMax);
        vec2Max(cp1, cp1, smoothMin);
        ctx.bezierCurveTo(cp0[0], cp0[1], cp1[0], cp1[1], p[0], p[1]); // cp0 of next segment

        scaleAndAdd(cp0, p, v, smooth * ratioNextSeg);
      } else {
        ctx.lineTo(p[0], p[1]);
      }
    }

    prevIdx = idx;
    idx += dir;
  }

  return k;
}

function getBoundingBox(points, smoothConstraint) {
  var ptMin = [Infinity, Infinity];
  var ptMax = [-Infinity, -Infinity];

  if (smoothConstraint) {
    for (var i = 0; i < points.length; i++) {
      var pt = points[i];

      if (pt[0] < ptMin[0]) {
        ptMin[0] = pt[0];
      }

      if (pt[1] < ptMin[1]) {
        ptMin[1] = pt[1];
      }

      if (pt[0] > ptMax[0]) {
        ptMax[0] = pt[0];
      }

      if (pt[1] > ptMax[1]) {
        ptMax[1] = pt[1];
      }
    }
  }

  return {
    min: smoothConstraint ? ptMin : ptMax,
    max: smoothConstraint ? ptMax : ptMin
  };
}

var Polyline = Path.extend({
  type: 'ec-polyline',
  shape: {
    points: [],
    smooth: 0,
    smoothConstraint: true,
    smoothMonotone: null,
    connectNulls: false
  },
  style: {
    fill: null,
    stroke: '#000'
  },
  brush: fixClipWithShadow(Path.prototype.brush),
  buildPath: function buildPath(ctx, shape) {
    var points = shape.points;
    var i = 0;
    var len = points.length;
    var result = getBoundingBox(points, shape.smoothConstraint);

    if (shape.connectNulls) {
      // Must remove first and last null values avoid draw error in polygon
      for (; len > 0; len--) {
        if (!isPointNull(points[len - 1])) {
          break;
        }
      }

      for (; i < len; i++) {
        if (!isPointNull(points[i])) {
          break;
        }
      }
    }

    while (i < len) {
      i += drawSegment(ctx, points, i, len, len, 1, result.min, result.max, shape.smooth, shape.smoothMonotone, shape.connectNulls) + 1;
    }
  }
});
var Polygon = Path.extend({
  type: 'ec-polygon',
  shape: {
    points: [],
    // Offset between stacked base points and points
    stackedOnPoints: [],
    smooth: 0,
    stackedOnSmooth: 0,
    smoothConstraint: true,
    smoothMonotone: null,
    connectNulls: false
  },
  brush: fixClipWithShadow(Path.prototype.brush),
  buildPath: function buildPath(ctx, shape) {
    var points = shape.points;
    var stackedOnPoints = shape.stackedOnPoints;
    var i = 0;
    var len = points.length;
    var smoothMonotone = shape.smoothMonotone;
    var bbox = getBoundingBox(points, shape.smoothConstraint);
    var stackedOnBBox = getBoundingBox(stackedOnPoints, shape.smoothConstraint);

    if (shape.connectNulls) {
      // Must remove first and last null values avoid draw error in polygon
      for (; len > 0; len--) {
        if (!isPointNull(points[len - 1])) {
          break;
        }
      }

      for (; i < len; i++) {
        if (!isPointNull(points[i])) {
          break;
        }
      }
    }

    while (i < len) {
      var k = drawSegment(ctx, points, i, len, len, 1, bbox.min, bbox.max, shape.smooth, smoothMonotone, shape.connectNulls);
      drawSegment(ctx, stackedOnPoints, i + k - 1, k, len, -1, stackedOnBBox.min, stackedOnBBox.max, shape.stackedOnSmooth, smoothMonotone, shape.connectNulls);
      i += k + 1;
      ctx.closePath();
    }
  }
});
exports.Polyline = Polyline;
exports.Polygon = Polygon;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9wb2x5LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvbGluZS9wb2x5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgUGF0aCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9ncmFwaGljL1BhdGhcIik7XG5cbnZhciB2ZWMyID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgZml4Q2xpcFdpdGhTaGFkb3cgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvZml4Q2xpcFdpdGhTaGFkb3dcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIFBvbHkgcGF0aCBzdXBwb3J0IE5hTiBwb2ludFxudmFyIHZlYzJNaW4gPSB2ZWMyLm1pbjtcbnZhciB2ZWMyTWF4ID0gdmVjMi5tYXg7XG52YXIgc2NhbGVBbmRBZGQgPSB2ZWMyLnNjYWxlQW5kQWRkO1xudmFyIHYyQ29weSA9IHZlYzIuY29weTsgLy8gVGVtcG9yYXJ5IHZhcmlhYmxlXG5cbnZhciB2ID0gW107XG52YXIgY3AwID0gW107XG52YXIgY3AxID0gW107XG5cbmZ1bmN0aW9uIGlzUG9pbnROdWxsKHApIHtcbiAgcmV0dXJuIGlzTmFOKHBbMF0pIHx8IGlzTmFOKHBbMV0pO1xufVxuXG5mdW5jdGlvbiBkcmF3U2VnbWVudChjdHgsIHBvaW50cywgc3RhcnQsIHNlZ0xlbiwgYWxsTGVuLCBkaXIsIHNtb290aE1pbiwgc21vb3RoTWF4LCBzbW9vdGgsIHNtb290aE1vbm90b25lLCBjb25uZWN0TnVsbHMpIHtcbiAgLy8gaWYgKHNtb290aE1vbm90b25lID09IG51bGwpIHtcbiAgLy8gICAgIGlmIChpc01vbm8ocG9pbnRzLCAneCcpKSB7XG4gIC8vICAgICAgICAgcmV0dXJuIGRyYXdNb25vKGN0eCwgcG9pbnRzLCBzdGFydCwgc2VnTGVuLCBhbGxMZW4sXG4gIC8vICAgICAgICAgICAgIGRpciwgc21vb3RoTWluLCBzbW9vdGhNYXgsIHNtb290aCwgJ3gnLCBjb25uZWN0TnVsbHMpO1xuICAvLyAgICAgfVxuICAvLyAgICAgZWxzZSBpZiAoaXNNb25vKHBvaW50cywgJ3knKSkge1xuICAvLyAgICAgICAgIHJldHVybiBkcmF3TW9ubyhjdHgsIHBvaW50cywgc3RhcnQsIHNlZ0xlbiwgYWxsTGVuLFxuICAvLyAgICAgICAgICAgICBkaXIsIHNtb290aE1pbiwgc21vb3RoTWF4LCBzbW9vdGgsICd5JywgY29ubmVjdE51bGxzKTtcbiAgLy8gICAgIH1cbiAgLy8gICAgIGVsc2Uge1xuICAvLyAgICAgICAgIHJldHVybiBkcmF3Tm9uTW9uby5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAvLyAgICAgfVxuICAvLyB9XG4gIC8vIGVsc2UgaWYgKHNtb290aE1vbm90b25lICE9PSAnbm9uZScgJiYgaXNNb25vKHBvaW50cywgc21vb3RoTW9ub3RvbmUpKSB7XG4gIC8vICAgICByZXR1cm4gZHJhd01vbm8uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgLy8gfVxuICAvLyBlbHNlIHtcbiAgLy8gICAgIHJldHVybiBkcmF3Tm9uTW9uby5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAvLyB9XG4gIGlmIChzbW9vdGhNb25vdG9uZSA9PT0gJ25vbmUnIHx8ICFzbW9vdGhNb25vdG9uZSkge1xuICAgIHJldHVybiBkcmF3Tm9uTW9uby5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBkcmF3TW9uby5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICB9XG59XG4vKipcbiAqIENoZWNrIGlmIHBvaW50cyBpcyBpbiBtb25vdG9uZS5cbiAqXG4gKiBAcGFyYW0ge251bWJlcltdW119IHBvaW50cyAgICAgICAgIEFycmF5IG9mIHBvaW50cyB3aGljaCBpcyBpbiBbeCwgeV0gZm9ybVxuICogQHBhcmFtIHtzdHJpbmd9ICAgICBzbW9vdGhNb25vdG9uZSAneCcsICd5Jywgb3IgJ25vbmUnLCBzdGF0aW5nIGZvciB3aGljaFxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaW1lbnNpb24gdGhhdCBpcyBjaGVja2luZy5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgSWYgaXMgJ25vbmUnLCBgZHJhd05vbk1vbm9gIHNob3VsZCBiZVxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsZWQuXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIElmIGlzIHVuZGVmaW5lZCwgZWl0aGVyIGJlaW5nIG1vbm90b25lXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluICd4JyBvciAneScgd2lsbCBjYWxsIGBkcmF3TW9ub2AuXG4gKi9cbi8vIGZ1bmN0aW9uIGlzTW9ubyhwb2ludHMsIHNtb290aE1vbm90b25lKSB7XG4vLyAgICAgaWYgKHBvaW50cy5sZW5ndGggPD0gMSkge1xuLy8gICAgICAgICByZXR1cm4gdHJ1ZTtcbi8vICAgICB9XG4vLyAgICAgdmFyIGRpbSA9IHNtb290aE1vbm90b25lID09PSAneCcgPyAwIDogMTtcbi8vICAgICB2YXIgbGFzdCA9IHBvaW50c1swXVtkaW1dO1xuLy8gICAgIHZhciBsYXN0RGlmZiA9IDA7XG4vLyAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBwb2ludHMubGVuZ3RoOyArK2kpIHtcbi8vICAgICAgICAgdmFyIGRpZmYgPSBwb2ludHNbaV1bZGltXSAtIGxhc3Q7XG4vLyAgICAgICAgIGlmICghaXNOYU4oZGlmZikgJiYgIWlzTmFOKGxhc3REaWZmKVxuLy8gICAgICAgICAgICAgJiYgZGlmZiAhPT0gMCAmJiBsYXN0RGlmZiAhPT0gMFxuLy8gICAgICAgICAgICAgJiYgKChkaWZmID49IDApICE9PSAobGFzdERpZmYgPj0gMCkpXG4vLyAgICAgICAgICkge1xuLy8gICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIGlmICghaXNOYU4oZGlmZikgJiYgZGlmZiAhPT0gMCkge1xuLy8gICAgICAgICAgICAgbGFzdERpZmYgPSBkaWZmO1xuLy8gICAgICAgICAgICAgbGFzdCA9IHBvaW50c1tpXVtkaW1dO1xuLy8gICAgICAgICB9XG4vLyAgICAgfVxuLy8gICAgIHJldHVybiB0cnVlO1xuLy8gfVxuXG4vKipcbiAqIERyYXcgc21vb3RoZWQgbGluZSBpbiBtb25vdG9uZSwgaW4gd2hpY2ggb25seSB2ZXJ0aWNhbCBvciBob3Jpem9udGFsIGJlemllclxuICogY29udHJvbCBwb2ludHMgd2lsbCBiZSB1c2VkLiBUaGlzIHNob3VsZCBiZSB1c2VkIHdoZW4gcG9pbnRzIGFyZSBtb25vdG9uZVxuICogZWl0aGVyIGluIHggb3IgeSBkaW1lbnNpb24uXG4gKi9cblxuXG5mdW5jdGlvbiBkcmF3TW9ubyhjdHgsIHBvaW50cywgc3RhcnQsIHNlZ0xlbiwgYWxsTGVuLCBkaXIsIHNtb290aE1pbiwgc21vb3RoTWF4LCBzbW9vdGgsIHNtb290aE1vbm90b25lLCBjb25uZWN0TnVsbHMpIHtcbiAgdmFyIHByZXZJZHggPSAwO1xuICB2YXIgaWR4ID0gc3RhcnQ7XG5cbiAgZm9yICh2YXIgayA9IDA7IGsgPCBzZWdMZW47IGsrKykge1xuICAgIHZhciBwID0gcG9pbnRzW2lkeF07XG5cbiAgICBpZiAoaWR4ID49IGFsbExlbiB8fCBpZHggPCAwKSB7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBpZiAoaXNQb2ludE51bGwocCkpIHtcbiAgICAgIGlmIChjb25uZWN0TnVsbHMpIHtcbiAgICAgICAgaWR4ICs9IGRpcjtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIGlmIChpZHggPT09IHN0YXJ0KSB7XG4gICAgICBjdHhbZGlyID4gMCA/ICdtb3ZlVG8nIDogJ2xpbmVUbyddKHBbMF0sIHBbMV0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoc21vb3RoID4gMCkge1xuICAgICAgICB2YXIgcHJldlAgPSBwb2ludHNbcHJldklkeF07XG4gICAgICAgIHZhciBkaW0gPSBzbW9vdGhNb25vdG9uZSA9PT0gJ3knID8gMSA6IDA7IC8vIExlbmd0aCBvZiBjb250cm9sIHBvaW50IHRvIHAsIGVpdGhlciBpbiB4IG9yIHksIGJ1dCBub3QgYm90aFxuXG4gICAgICAgIHZhciBjdHJsTGVuID0gKHBbZGltXSAtIHByZXZQW2RpbV0pICogc21vb3RoO1xuICAgICAgICB2MkNvcHkoY3AwLCBwcmV2UCk7XG4gICAgICAgIGNwMFtkaW1dID0gcHJldlBbZGltXSArIGN0cmxMZW47XG4gICAgICAgIHYyQ29weShjcDEsIHApO1xuICAgICAgICBjcDFbZGltXSA9IHBbZGltXSAtIGN0cmxMZW47XG4gICAgICAgIGN0eC5iZXppZXJDdXJ2ZVRvKGNwMFswXSwgY3AwWzFdLCBjcDFbMF0sIGNwMVsxXSwgcFswXSwgcFsxXSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjdHgubGluZVRvKHBbMF0sIHBbMV0pO1xuICAgICAgfVxuICAgIH1cblxuICAgIHByZXZJZHggPSBpZHg7XG4gICAgaWR4ICs9IGRpcjtcbiAgfVxuXG4gIHJldHVybiBrO1xufVxuLyoqXG4gKiBEcmF3IHNtb290aGVkIGxpbmUgaW4gbm9uLW1vbm90b25lLCBpbiBtYXkgY2F1c2UgdW5kZXNpcmVkIGN1cnZlIGluIGV4dHJlbWVcbiAqIHNpdHVhdGlvbnMuIFRoaXMgc2hvdWxkIGJlIHVzZWQgd2hlbiBwb2ludHMgYXJlIG5vbi1tb25vdG9uZSBuZWl0aGVyIGluIHggb3JcbiAqIHkgZGltZW5zaW9uLlxuICovXG5cblxuZnVuY3Rpb24gZHJhd05vbk1vbm8oY3R4LCBwb2ludHMsIHN0YXJ0LCBzZWdMZW4sIGFsbExlbiwgZGlyLCBzbW9vdGhNaW4sIHNtb290aE1heCwgc21vb3RoLCBzbW9vdGhNb25vdG9uZSwgY29ubmVjdE51bGxzKSB7XG4gIHZhciBwcmV2SWR4ID0gMDtcbiAgdmFyIGlkeCA9IHN0YXJ0O1xuXG4gIGZvciAodmFyIGsgPSAwOyBrIDwgc2VnTGVuOyBrKyspIHtcbiAgICB2YXIgcCA9IHBvaW50c1tpZHhdO1xuXG4gICAgaWYgKGlkeCA+PSBhbGxMZW4gfHwgaWR4IDwgMCkge1xuICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgaWYgKGlzUG9pbnROdWxsKHApKSB7XG4gICAgICBpZiAoY29ubmVjdE51bGxzKSB7XG4gICAgICAgIGlkeCArPSBkaXI7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBpZiAoaWR4ID09PSBzdGFydCkge1xuICAgICAgY3R4W2RpciA+IDAgPyAnbW92ZVRvJyA6ICdsaW5lVG8nXShwWzBdLCBwWzFdKTtcbiAgICAgIHYyQ29weShjcDAsIHApO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoc21vb3RoID4gMCkge1xuICAgICAgICB2YXIgbmV4dElkeCA9IGlkeCArIGRpcjtcbiAgICAgICAgdmFyIG5leHRQID0gcG9pbnRzW25leHRJZHhdO1xuXG4gICAgICAgIGlmIChjb25uZWN0TnVsbHMpIHtcbiAgICAgICAgICAvLyBGaW5kIG5leHQgcG9pbnQgbm90IG51bGxcbiAgICAgICAgICB3aGlsZSAobmV4dFAgJiYgaXNQb2ludE51bGwocG9pbnRzW25leHRJZHhdKSkge1xuICAgICAgICAgICAgbmV4dElkeCArPSBkaXI7XG4gICAgICAgICAgICBuZXh0UCA9IHBvaW50c1tuZXh0SWR4XTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgcmF0aW9OZXh0U2VnID0gMC41O1xuICAgICAgICB2YXIgcHJldlAgPSBwb2ludHNbcHJldklkeF07XG4gICAgICAgIHZhciBuZXh0UCA9IHBvaW50c1tuZXh0SWR4XTsgLy8gTGFzdCBwb2ludFxuXG4gICAgICAgIGlmICghbmV4dFAgfHwgaXNQb2ludE51bGwobmV4dFApKSB7XG4gICAgICAgICAgdjJDb3B5KGNwMSwgcCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gSWYgbmV4dCBkYXRhIGlzIG51bGwgaW4gbm90IGNvbm5lY3QgY2FzZVxuICAgICAgICAgIGlmIChpc1BvaW50TnVsbChuZXh0UCkgJiYgIWNvbm5lY3ROdWxscykge1xuICAgICAgICAgICAgbmV4dFAgPSBwO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHZlYzIuc3ViKHYsIG5leHRQLCBwcmV2UCk7XG4gICAgICAgICAgdmFyIGxlblByZXZTZWc7XG4gICAgICAgICAgdmFyIGxlbk5leHRTZWc7XG5cbiAgICAgICAgICBpZiAoc21vb3RoTW9ub3RvbmUgPT09ICd4JyB8fCBzbW9vdGhNb25vdG9uZSA9PT0gJ3knKSB7XG4gICAgICAgICAgICB2YXIgZGltID0gc21vb3RoTW9ub3RvbmUgPT09ICd4JyA/IDAgOiAxO1xuICAgICAgICAgICAgbGVuUHJldlNlZyA9IE1hdGguYWJzKHBbZGltXSAtIHByZXZQW2RpbV0pO1xuICAgICAgICAgICAgbGVuTmV4dFNlZyA9IE1hdGguYWJzKHBbZGltXSAtIG5leHRQW2RpbV0pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZW5QcmV2U2VnID0gdmVjMi5kaXN0KHAsIHByZXZQKTtcbiAgICAgICAgICAgIGxlbk5leHRTZWcgPSB2ZWMyLmRpc3QocCwgbmV4dFApO1xuICAgICAgICAgIH0gLy8gVXNlIHJhdGlvIG9mIHNlZyBsZW5ndGhcblxuXG4gICAgICAgICAgcmF0aW9OZXh0U2VnID0gbGVuTmV4dFNlZyAvIChsZW5OZXh0U2VnICsgbGVuUHJldlNlZyk7XG4gICAgICAgICAgc2NhbGVBbmRBZGQoY3AxLCBwLCB2LCAtc21vb3RoICogKDEgLSByYXRpb05leHRTZWcpKTtcbiAgICAgICAgfSAvLyBTbW9vdGggY29uc3RyYWludFxuXG5cbiAgICAgICAgdmVjMk1pbihjcDAsIGNwMCwgc21vb3RoTWF4KTtcbiAgICAgICAgdmVjMk1heChjcDAsIGNwMCwgc21vb3RoTWluKTtcbiAgICAgICAgdmVjMk1pbihjcDEsIGNwMSwgc21vb3RoTWF4KTtcbiAgICAgICAgdmVjMk1heChjcDEsIGNwMSwgc21vb3RoTWluKTtcbiAgICAgICAgY3R4LmJlemllckN1cnZlVG8oY3AwWzBdLCBjcDBbMV0sIGNwMVswXSwgY3AxWzFdLCBwWzBdLCBwWzFdKTsgLy8gY3AwIG9mIG5leHQgc2VnbWVudFxuXG4gICAgICAgIHNjYWxlQW5kQWRkKGNwMCwgcCwgdiwgc21vb3RoICogcmF0aW9OZXh0U2VnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGN0eC5saW5lVG8ocFswXSwgcFsxXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcHJldklkeCA9IGlkeDtcbiAgICBpZHggKz0gZGlyO1xuICB9XG5cbiAgcmV0dXJuIGs7XG59XG5cbmZ1bmN0aW9uIGdldEJvdW5kaW5nQm94KHBvaW50cywgc21vb3RoQ29uc3RyYWludCkge1xuICB2YXIgcHRNaW4gPSBbSW5maW5pdHksIEluZmluaXR5XTtcbiAgdmFyIHB0TWF4ID0gWy1JbmZpbml0eSwgLUluZmluaXR5XTtcblxuICBpZiAoc21vb3RoQ29uc3RyYWludCkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcG9pbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgcHQgPSBwb2ludHNbaV07XG5cbiAgICAgIGlmIChwdFswXSA8IHB0TWluWzBdKSB7XG4gICAgICAgIHB0TWluWzBdID0gcHRbMF07XG4gICAgICB9XG5cbiAgICAgIGlmIChwdFsxXSA8IHB0TWluWzFdKSB7XG4gICAgICAgIHB0TWluWzFdID0gcHRbMV07XG4gICAgICB9XG5cbiAgICAgIGlmIChwdFswXSA+IHB0TWF4WzBdKSB7XG4gICAgICAgIHB0TWF4WzBdID0gcHRbMF07XG4gICAgICB9XG5cbiAgICAgIGlmIChwdFsxXSA+IHB0TWF4WzFdKSB7XG4gICAgICAgIHB0TWF4WzFdID0gcHRbMV07XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBtaW46IHNtb290aENvbnN0cmFpbnQgPyBwdE1pbiA6IHB0TWF4LFxuICAgIG1heDogc21vb3RoQ29uc3RyYWludCA/IHB0TWF4IDogcHRNaW5cbiAgfTtcbn1cblxudmFyIFBvbHlsaW5lID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAnZWMtcG9seWxpbmUnLFxuICBzaGFwZToge1xuICAgIHBvaW50czogW10sXG4gICAgc21vb3RoOiAwLFxuICAgIHNtb290aENvbnN0cmFpbnQ6IHRydWUsXG4gICAgc21vb3RoTW9ub3RvbmU6IG51bGwsXG4gICAgY29ubmVjdE51bGxzOiBmYWxzZVxuICB9LFxuICBzdHlsZToge1xuICAgIGZpbGw6IG51bGwsXG4gICAgc3Ryb2tlOiAnIzAwMCdcbiAgfSxcbiAgYnJ1c2g6IGZpeENsaXBXaXRoU2hhZG93KFBhdGgucHJvdG90eXBlLmJydXNoKSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSkge1xuICAgIHZhciBwb2ludHMgPSBzaGFwZS5wb2ludHM7XG4gICAgdmFyIGkgPSAwO1xuICAgIHZhciBsZW4gPSBwb2ludHMubGVuZ3RoO1xuICAgIHZhciByZXN1bHQgPSBnZXRCb3VuZGluZ0JveChwb2ludHMsIHNoYXBlLnNtb290aENvbnN0cmFpbnQpO1xuXG4gICAgaWYgKHNoYXBlLmNvbm5lY3ROdWxscykge1xuICAgICAgLy8gTXVzdCByZW1vdmUgZmlyc3QgYW5kIGxhc3QgbnVsbCB2YWx1ZXMgYXZvaWQgZHJhdyBlcnJvciBpbiBwb2x5Z29uXG4gICAgICBmb3IgKDsgbGVuID4gMDsgbGVuLS0pIHtcbiAgICAgICAgaWYgKCFpc1BvaW50TnVsbChwb2ludHNbbGVuIC0gMV0pKSB7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgZm9yICg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBpZiAoIWlzUG9pbnROdWxsKHBvaW50c1tpXSkpIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHdoaWxlIChpIDwgbGVuKSB7XG4gICAgICBpICs9IGRyYXdTZWdtZW50KGN0eCwgcG9pbnRzLCBpLCBsZW4sIGxlbiwgMSwgcmVzdWx0Lm1pbiwgcmVzdWx0Lm1heCwgc2hhcGUuc21vb3RoLCBzaGFwZS5zbW9vdGhNb25vdG9uZSwgc2hhcGUuY29ubmVjdE51bGxzKSArIDE7XG4gICAgfVxuICB9XG59KTtcbnZhciBQb2x5Z29uID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAnZWMtcG9seWdvbicsXG4gIHNoYXBlOiB7XG4gICAgcG9pbnRzOiBbXSxcbiAgICAvLyBPZmZzZXQgYmV0d2VlbiBzdGFja2VkIGJhc2UgcG9pbnRzIGFuZCBwb2ludHNcbiAgICBzdGFja2VkT25Qb2ludHM6IFtdLFxuICAgIHNtb290aDogMCxcbiAgICBzdGFja2VkT25TbW9vdGg6IDAsXG4gICAgc21vb3RoQ29uc3RyYWludDogdHJ1ZSxcbiAgICBzbW9vdGhNb25vdG9uZTogbnVsbCxcbiAgICBjb25uZWN0TnVsbHM6IGZhbHNlXG4gIH0sXG4gIGJydXNoOiBmaXhDbGlwV2l0aFNoYWRvdyhQYXRoLnByb3RvdHlwZS5icnVzaCksXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUpIHtcbiAgICB2YXIgcG9pbnRzID0gc2hhcGUucG9pbnRzO1xuICAgIHZhciBzdGFja2VkT25Qb2ludHMgPSBzaGFwZS5zdGFja2VkT25Qb2ludHM7XG4gICAgdmFyIGkgPSAwO1xuICAgIHZhciBsZW4gPSBwb2ludHMubGVuZ3RoO1xuICAgIHZhciBzbW9vdGhNb25vdG9uZSA9IHNoYXBlLnNtb290aE1vbm90b25lO1xuICAgIHZhciBiYm94ID0gZ2V0Qm91bmRpbmdCb3gocG9pbnRzLCBzaGFwZS5zbW9vdGhDb25zdHJhaW50KTtcbiAgICB2YXIgc3RhY2tlZE9uQkJveCA9IGdldEJvdW5kaW5nQm94KHN0YWNrZWRPblBvaW50cywgc2hhcGUuc21vb3RoQ29uc3RyYWludCk7XG5cbiAgICBpZiAoc2hhcGUuY29ubmVjdE51bGxzKSB7XG4gICAgICAvLyBNdXN0IHJlbW92ZSBmaXJzdCBhbmQgbGFzdCBudWxsIHZhbHVlcyBhdm9pZCBkcmF3IGVycm9yIGluIHBvbHlnb25cbiAgICAgIGZvciAoOyBsZW4gPiAwOyBsZW4tLSkge1xuICAgICAgICBpZiAoIWlzUG9pbnROdWxsKHBvaW50c1tsZW4gLSAxXSkpIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBmb3IgKDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIGlmICghaXNQb2ludE51bGwocG9pbnRzW2ldKSkge1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgd2hpbGUgKGkgPCBsZW4pIHtcbiAgICAgIHZhciBrID0gZHJhd1NlZ21lbnQoY3R4LCBwb2ludHMsIGksIGxlbiwgbGVuLCAxLCBiYm94Lm1pbiwgYmJveC5tYXgsIHNoYXBlLnNtb290aCwgc21vb3RoTW9ub3RvbmUsIHNoYXBlLmNvbm5lY3ROdWxscyk7XG4gICAgICBkcmF3U2VnbWVudChjdHgsIHN0YWNrZWRPblBvaW50cywgaSArIGsgLSAxLCBrLCBsZW4sIC0xLCBzdGFja2VkT25CQm94Lm1pbiwgc3RhY2tlZE9uQkJveC5tYXgsIHNoYXBlLnN0YWNrZWRPblNtb290aCwgc21vb3RoTW9ub3RvbmUsIHNoYXBlLmNvbm5lY3ROdWxscyk7XG4gICAgICBpICs9IGsgKyAxO1xuICAgICAgY3R4LmNsb3NlUGF0aCgpO1xuICAgIH1cbiAgfVxufSk7XG5leHBvcnRzLlBvbHlsaW5lID0gUG9seWxpbmU7XG5leHBvcnRzLlBvbHlnb24gPSBQb2x5Z29uOyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXRDQTtBQXdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0NBO0FBNkNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/line/poly.js
