/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
function defaultKeyGetter(item) {
  return item;
}
/**
 * @param {Array} oldArr
 * @param {Array} newArr
 * @param {Function} oldKeyGetter
 * @param {Function} newKeyGetter
 * @param {Object} [context] Can be visited by this.context in callback.
 */


function DataDiffer(oldArr, newArr, oldKeyGetter, newKeyGetter, context) {
  this._old = oldArr;
  this._new = newArr;
  this._oldKeyGetter = oldKeyGetter || defaultKeyGetter;
  this._newKeyGetter = newKeyGetter || defaultKeyGetter;
  this.context = context;
}

DataDiffer.prototype = {
  constructor: DataDiffer,

  /**
   * Callback function when add a data
   */
  add: function add(func) {
    this._add = func;
    return this;
  },

  /**
   * Callback function when update a data
   */
  update: function update(func) {
    this._update = func;
    return this;
  },

  /**
   * Callback function when remove a data
   */
  remove: function remove(func) {
    this._remove = func;
    return this;
  },
  execute: function execute() {
    var oldArr = this._old;
    var newArr = this._new;
    var oldDataIndexMap = {};
    var newDataIndexMap = {};
    var oldDataKeyArr = [];
    var newDataKeyArr = [];
    var i;
    initIndexMap(oldArr, oldDataIndexMap, oldDataKeyArr, '_oldKeyGetter', this);
    initIndexMap(newArr, newDataIndexMap, newDataKeyArr, '_newKeyGetter', this); // Travel by inverted order to make sure order consistency
    // when duplicate keys exists (consider newDataIndex.pop() below).
    // For performance consideration, these code below do not look neat.

    for (i = 0; i < oldArr.length; i++) {
      var key = oldDataKeyArr[i];
      var idx = newDataIndexMap[key]; // idx can never be empty array here. see 'set null' logic below.

      if (idx != null) {
        // Consider there is duplicate key (for example, use dataItem.name as key).
        // We should make sure every item in newArr and oldArr can be visited.
        var len = idx.length;

        if (len) {
          len === 1 && (newDataIndexMap[key] = null);
          idx = idx.unshift();
        } else {
          newDataIndexMap[key] = null;
        }

        this._update && this._update(idx, i);
      } else {
        this._remove && this._remove(i);
      }
    }

    for (var i = 0; i < newDataKeyArr.length; i++) {
      var key = newDataKeyArr[i];

      if (newDataIndexMap.hasOwnProperty(key)) {
        var idx = newDataIndexMap[key];

        if (idx == null) {
          continue;
        } // idx can never be empty array here. see 'set null' logic above.


        if (!idx.length) {
          this._add && this._add(idx);
        } else {
          for (var j = 0, len = idx.length; j < len; j++) {
            this._add && this._add(idx[j]);
          }
        }
      }
    }
  }
};

function initIndexMap(arr, map, keyArr, keyGetterName, dataDiffer) {
  for (var i = 0; i < arr.length; i++) {
    // Add prefix to avoid conflict with Object.prototype.
    var key = '_ec_' + dataDiffer[keyGetterName](arr[i], i);
    var existence = map[key];

    if (existence == null) {
      keyArr.push(key);
      map[key] = i;
    } else {
      if (!existence.length) {
        map[key] = existence = [existence];
      }

      existence.push(i);
    }
  }
}

var _default = DataDiffer;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9EYXRhRGlmZmVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9EYXRhRGlmZmVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5mdW5jdGlvbiBkZWZhdWx0S2V5R2V0dGVyKGl0ZW0pIHtcbiAgcmV0dXJuIGl0ZW07XG59XG4vKipcbiAqIEBwYXJhbSB7QXJyYXl9IG9sZEFyclxuICogQHBhcmFtIHtBcnJheX0gbmV3QXJyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBvbGRLZXlHZXR0ZXJcbiAqIEBwYXJhbSB7RnVuY3Rpb259IG5ld0tleUdldHRlclxuICogQHBhcmFtIHtPYmplY3R9IFtjb250ZXh0XSBDYW4gYmUgdmlzaXRlZCBieSB0aGlzLmNvbnRleHQgaW4gY2FsbGJhY2suXG4gKi9cblxuXG5mdW5jdGlvbiBEYXRhRGlmZmVyKG9sZEFyciwgbmV3QXJyLCBvbGRLZXlHZXR0ZXIsIG5ld0tleUdldHRlciwgY29udGV4dCkge1xuICB0aGlzLl9vbGQgPSBvbGRBcnI7XG4gIHRoaXMuX25ldyA9IG5ld0FycjtcbiAgdGhpcy5fb2xkS2V5R2V0dGVyID0gb2xkS2V5R2V0dGVyIHx8IGRlZmF1bHRLZXlHZXR0ZXI7XG4gIHRoaXMuX25ld0tleUdldHRlciA9IG5ld0tleUdldHRlciB8fCBkZWZhdWx0S2V5R2V0dGVyO1xuICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xufVxuXG5EYXRhRGlmZmVyLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IERhdGFEaWZmZXIsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZ1bmN0aW9uIHdoZW4gYWRkIGEgZGF0YVxuICAgKi9cbiAgYWRkOiBmdW5jdGlvbiAoZnVuYykge1xuICAgIHRoaXMuX2FkZCA9IGZ1bmM7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZ1bmN0aW9uIHdoZW4gdXBkYXRlIGEgZGF0YVxuICAgKi9cbiAgdXBkYXRlOiBmdW5jdGlvbiAoZnVuYykge1xuICAgIHRoaXMuX3VwZGF0ZSA9IGZ1bmM7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZ1bmN0aW9uIHdoZW4gcmVtb3ZlIGEgZGF0YVxuICAgKi9cbiAgcmVtb3ZlOiBmdW5jdGlvbiAoZnVuYykge1xuICAgIHRoaXMuX3JlbW92ZSA9IGZ1bmM7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG4gIGV4ZWN1dGU6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgb2xkQXJyID0gdGhpcy5fb2xkO1xuICAgIHZhciBuZXdBcnIgPSB0aGlzLl9uZXc7XG4gICAgdmFyIG9sZERhdGFJbmRleE1hcCA9IHt9O1xuICAgIHZhciBuZXdEYXRhSW5kZXhNYXAgPSB7fTtcbiAgICB2YXIgb2xkRGF0YUtleUFyciA9IFtdO1xuICAgIHZhciBuZXdEYXRhS2V5QXJyID0gW107XG4gICAgdmFyIGk7XG4gICAgaW5pdEluZGV4TWFwKG9sZEFyciwgb2xkRGF0YUluZGV4TWFwLCBvbGREYXRhS2V5QXJyLCAnX29sZEtleUdldHRlcicsIHRoaXMpO1xuICAgIGluaXRJbmRleE1hcChuZXdBcnIsIG5ld0RhdGFJbmRleE1hcCwgbmV3RGF0YUtleUFyciwgJ19uZXdLZXlHZXR0ZXInLCB0aGlzKTsgLy8gVHJhdmVsIGJ5IGludmVydGVkIG9yZGVyIHRvIG1ha2Ugc3VyZSBvcmRlciBjb25zaXN0ZW5jeVxuICAgIC8vIHdoZW4gZHVwbGljYXRlIGtleXMgZXhpc3RzIChjb25zaWRlciBuZXdEYXRhSW5kZXgucG9wKCkgYmVsb3cpLlxuICAgIC8vIEZvciBwZXJmb3JtYW5jZSBjb25zaWRlcmF0aW9uLCB0aGVzZSBjb2RlIGJlbG93IGRvIG5vdCBsb29rIG5lYXQuXG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgb2xkQXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIga2V5ID0gb2xkRGF0YUtleUFycltpXTtcbiAgICAgIHZhciBpZHggPSBuZXdEYXRhSW5kZXhNYXBba2V5XTsgLy8gaWR4IGNhbiBuZXZlciBiZSBlbXB0eSBhcnJheSBoZXJlLiBzZWUgJ3NldCBudWxsJyBsb2dpYyBiZWxvdy5cblxuICAgICAgaWYgKGlkeCAhPSBudWxsKSB7XG4gICAgICAgIC8vIENvbnNpZGVyIHRoZXJlIGlzIGR1cGxpY2F0ZSBrZXkgKGZvciBleGFtcGxlLCB1c2UgZGF0YUl0ZW0ubmFtZSBhcyBrZXkpLlxuICAgICAgICAvLyBXZSBzaG91bGQgbWFrZSBzdXJlIGV2ZXJ5IGl0ZW0gaW4gbmV3QXJyIGFuZCBvbGRBcnIgY2FuIGJlIHZpc2l0ZWQuXG4gICAgICAgIHZhciBsZW4gPSBpZHgubGVuZ3RoO1xuXG4gICAgICAgIGlmIChsZW4pIHtcbiAgICAgICAgICBsZW4gPT09IDEgJiYgKG5ld0RhdGFJbmRleE1hcFtrZXldID0gbnVsbCk7XG4gICAgICAgICAgaWR4ID0gaWR4LnVuc2hpZnQoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBuZXdEYXRhSW5kZXhNYXBba2V5XSA9IG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLl91cGRhdGUgJiYgdGhpcy5fdXBkYXRlKGlkeCwgaSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLl9yZW1vdmUgJiYgdGhpcy5fcmVtb3ZlKGkpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbmV3RGF0YUtleUFyci5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGtleSA9IG5ld0RhdGFLZXlBcnJbaV07XG5cbiAgICAgIGlmIChuZXdEYXRhSW5kZXhNYXAuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICB2YXIgaWR4ID0gbmV3RGF0YUluZGV4TWFwW2tleV07XG5cbiAgICAgICAgaWYgKGlkeCA9PSBudWxsKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH0gLy8gaWR4IGNhbiBuZXZlciBiZSBlbXB0eSBhcnJheSBoZXJlLiBzZWUgJ3NldCBudWxsJyBsb2dpYyBhYm92ZS5cblxuXG4gICAgICAgIGlmICghaWR4Lmxlbmd0aCkge1xuICAgICAgICAgIHRoaXMuX2FkZCAmJiB0aGlzLl9hZGQoaWR4KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBmb3IgKHZhciBqID0gMCwgbGVuID0gaWR4Lmxlbmd0aDsgaiA8IGxlbjsgaisrKSB7XG4gICAgICAgICAgICB0aGlzLl9hZGQgJiYgdGhpcy5fYWRkKGlkeFtqXSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59O1xuXG5mdW5jdGlvbiBpbml0SW5kZXhNYXAoYXJyLCBtYXAsIGtleUFyciwga2V5R2V0dGVyTmFtZSwgZGF0YURpZmZlcikge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IGFyci5sZW5ndGg7IGkrKykge1xuICAgIC8vIEFkZCBwcmVmaXggdG8gYXZvaWQgY29uZmxpY3Qgd2l0aCBPYmplY3QucHJvdG90eXBlLlxuICAgIHZhciBrZXkgPSAnX2VjXycgKyBkYXRhRGlmZmVyW2tleUdldHRlck5hbWVdKGFycltpXSwgaSk7XG4gICAgdmFyIGV4aXN0ZW5jZSA9IG1hcFtrZXldO1xuXG4gICAgaWYgKGV4aXN0ZW5jZSA9PSBudWxsKSB7XG4gICAgICBrZXlBcnIucHVzaChrZXkpO1xuICAgICAgbWFwW2tleV0gPSBpO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoIWV4aXN0ZW5jZS5sZW5ndGgpIHtcbiAgICAgICAgbWFwW2tleV0gPSBleGlzdGVuY2UgPSBbZXhpc3RlbmNlXTtcbiAgICAgIH1cblxuICAgICAgZXhpc3RlbmNlLnB1c2goaSk7XG4gICAgfVxuICB9XG59XG5cbnZhciBfZGVmYXVsdCA9IERhdGFEaWZmZXI7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakZBO0FBQ0E7QUFtRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/DataDiffer.js
