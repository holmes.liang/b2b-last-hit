var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(this, function () {
  function wrap(standard, fallback) {
    return function (el, evtName, listener, useCapture) {
      if (el[standard]) {
        el[standard](evtName, listener, useCapture);
      } else if (el[fallback]) {
        el[fallback]('on' + evtName, listener);
      }
    };
  }

  return {
    add: wrap('addEventListener', 'attachEvent'),
    remove: wrap('removeEventListener', 'detachEvent')
  };
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZXZlbnRsaXN0ZW5lci9ldmVudGxpc3RlbmVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZXZlbnRsaXN0ZW5lci9ldmVudGxpc3RlbmVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbihyb290LGZhY3Rvcnkpe1xuICAgIGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcbiAgICAgICAgZGVmaW5lKGZhY3RvcnkpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKSB7XG4gICAgICAgIG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJvb3QuZXZlbnRMaXN0ZW5lciA9IGZhY3RvcnkoKTtcbiAgfVxufSh0aGlzLCBmdW5jdGlvbiAoKSB7XG5cdGZ1bmN0aW9uIHdyYXAoc3RhbmRhcmQsIGZhbGxiYWNrKSB7XG5cdFx0cmV0dXJuIGZ1bmN0aW9uIChlbCwgZXZ0TmFtZSwgbGlzdGVuZXIsIHVzZUNhcHR1cmUpIHtcblx0XHRcdGlmIChlbFtzdGFuZGFyZF0pIHtcblx0XHRcdFx0ZWxbc3RhbmRhcmRdKGV2dE5hbWUsIGxpc3RlbmVyLCB1c2VDYXB0dXJlKTtcblx0XHRcdH0gZWxzZSBpZiAoZWxbZmFsbGJhY2tdKSB7XG5cdFx0XHRcdGVsW2ZhbGxiYWNrXSgnb24nICsgZXZ0TmFtZSwgbGlzdGVuZXIpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG4gICAgcmV0dXJuIHtcblx0XHRhZGQ6IHdyYXAoJ2FkZEV2ZW50TGlzdGVuZXInLCAnYXR0YWNoRXZlbnQnKSxcblx0XHRyZW1vdmU6IHdyYXAoJ3JlbW92ZUV2ZW50TGlzdGVuZXInLCAnZGV0YWNoRXZlbnQnKVxuXHR9O1xufSkpOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSxXQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/eventlistener/eventlistener.js
