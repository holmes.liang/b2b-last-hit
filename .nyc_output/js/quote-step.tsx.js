__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _quote_step_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./quote-step-style */ "./src/app/desk/quote/quote-step-style.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/quote-step.tsx";




var isMobile = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].getIsMobile();

var QuoteStep =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(QuoteStep, _ModelWidget);

  function QuoteStep() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, QuoteStep);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(QuoteStep).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(QuoteStep, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign({}, _quote_step_style__WEBPACK_IMPORTED_MODULE_7__["default"]);
    }
  }, {
    key: "getPrdtCode",
    value: function getPrdtCode() {
      return this.getValueFromModel("productCode") || this.getValueFromModel("policy.productCode");
    }
  }, {
    key: "getItntCode",
    value: function getItntCode() {
      return this.getValueFromModel("itntCode");
    }
  }, {
    key: "switchPolicy",
    value: function switchPolicy(policy) {
      this.getRootModel().$$switchModel(policy);
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(C.Quote, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        className: "container ".concat(isMobile ? "mobile-step" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      }, this.renderTitle(), this.renderContent(), this.renderActions()));
    }
  }]);

  return QuoteStep;
}(_component__WEBPACK_IMPORTED_MODULE_6__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (QuoteStep);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVvdGUtc3RlcC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9xdW90ZS1zdGVwLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXRQcm9wcywgU3R5bGVkRElWIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgRm9ybUNvbXBvbmVudFByb3BzIH0gZnJvbSBcImFudGQvbGliL2Zvcm1cIjtcbmltcG9ydCBRdW90ZVN0ZXBTdHlsZSBmcm9tIFwiLi9xdW90ZS1zdGVwLXN0eWxlXCI7XG5pbXBvcnQgeyBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmV4cG9ydCB0eXBlIFN0ZXBDb21wb25lbnRzID0ge1xuICBRdW90ZTogU3R5bGVkRElWLFxufVxuXG5leHBvcnQgdHlwZSBTdGVwUHJvcHMgPSB7XG4gIGZvcm06IGFueTtcbiAgbW9kZWw6IG9iamVjdDtcbiAgbWVyZ2VQb2xpY3lUb1VJTW9kZWw6ICh2YWx1ZTogYW55KSA9PiB2b2lkO1xuICBtZXJnZVBvbGljeVRvU2VydmljZU1vZGVsOiAoKSA9PiBhbnk7XG4gIGlkZW50aXR5OiBhbnlcbn0gJiBNb2RlbFdpZGdldFByb3BzICYgRm9ybUNvbXBvbmVudFByb3BzXG5cbmFic3RyYWN0IGNsYXNzIFF1b3RlU3RlcDxQIGV4dGVuZHMgU3RlcFByb3BzLCBTLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIFF1b3RlU3RlcFN0eWxlKSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldFByZHRDb2RlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwcm9kdWN0Q29kZVwiKSB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5LnByb2R1Y3RDb2RlXCIpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEl0bnRDb2RlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJpdG50Q29kZVwiKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzd2l0Y2hQb2xpY3kocG9saWN5OiBhbnkpOiBhbnkge1xuICAgIHRoaXMuZ2V0Um9vdE1vZGVsKCkuJCRzd2l0Y2hNb2RlbChwb2xpY3kpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IHJlbmRlclRpdGxlKCk6IEpTWC5FbGVtZW50IHwgbnVsbCB8IHZvaWQ7XG5cbiAgcHJvdGVjdGVkIGFic3RyYWN0IHJlbmRlckNvbnRlbnQoKTogSlNYLkVsZW1lbnQgfCBudWxsIHwgdm9pZDtcblxuICBwcm90ZWN0ZWQgYWJzdHJhY3QgcmVuZGVyQWN0aW9ucygpOiBKU1guRWxlbWVudCB8IG51bGwgfCB2b2lkO1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEMuUXVvdGU+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgY29udGFpbmVyICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1zdGVwXCIgOiBcIlwifWB9PlxuICAgICAgICAgIHt0aGlzLnJlbmRlclRpdGxlKCl9XG4gICAgICAgICAge3RoaXMucmVuZGVyQ29udGVudCgpfVxuICAgICAgICAgIHt0aGlzLnJlbmRlckFjdGlvbnMoKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L0MuUXVvdGU+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBRdW90ZVN0ZXA7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFZQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBUUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTs7OztBQW5DQTtBQUNBO0FBcUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/quote-step.tsx
