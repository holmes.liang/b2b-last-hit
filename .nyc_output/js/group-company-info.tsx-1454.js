__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _choice_search_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./choice-search-list */ "./src/app/desk/component/group-customer-company/choice-search-list.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/group-customer-company/group-company-info.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var GroupCompanyInfo =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(GroupCompanyInfo, _ModelWidget);

  function GroupCompanyInfo(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, GroupCompanyInfo);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(GroupCompanyInfo).call(this, props, context));
    _this.idNoPage = void 0;
    _this.namePage = void 0;

    _this.initIdNoRef = function (ref) {
      _this.idNoPage = ref;
    };

    _this.initNameRef = function (ref) {
      _this.namePage = ref;
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(GroupCompanyInfo, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var registrationNoProp = this.props.registrationNoProp;
      this.idNoPage && this.idNoPage.setShowLabel(this.getValueFromModel(this.generatePropName(registrationNoProp || "registrationNo")));
      this.namePage && this.namePage.setShowLabel(this.getValueFromModel(this.generatePropName("name")));
    }
  }, {
    key: "setFormValues",
    value: function setFormValues(item) {
      var _this2 = this,
          _valueObj;

      var registrationNoProp = this.props.registrationNoProp;
      ["name", "idNo", "mobile", "mobileNationCode", "email", "officeTel", "officeTelNationCode"].map(function (every) {
        _this2.setValueToModel(every === "name" ? item["name"] : item[every], every === "idNo" ? _this2.generatePropName(registrationNoProp || "registrationNo") : _this2.generatePropName(every));

        _this2.props.form.getFieldDecorator([every === "idNo" ? _this2.generatePropName(registrationNoProp || "registrationNo") : _this2.generatePropName(every)], {
          initialValue: ""
        });

        _this2.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, every === "idNo" ? _this2.generatePropName(registrationNoProp || "registrationNo") : _this2.generatePropName(every), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, every)));
      });
      this.setValueToModel(lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "addressType"), this.generatePropName("addressType"));
      this.props.form.getFieldDecorator([this.generatePropName("addressType")], {
        initialValue: ""
      });
      this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, this.generatePropName("addressType"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "addressType"))); // if (this.props.countryCode && (this.props.countryCode === _.get(item, "address.countryCode"))) {

      var valueObj = (_valueObj = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_valueObj, this.generatePropName("address"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_valueObj, this.generatePropName("address.street"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.street")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_valueObj, this.generatePropName("address.postalCode"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.postalCode")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_valueObj, this.generatePropName("address.unitNo"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.unitNo")), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(_valueObj, this.generatePropName("address.countryCode"), lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(item, "address.countryCode")), _valueObj);
      this.setValuesToModel(valueObj);
      this.props.form.getFieldDecorator([this.generatePropName("address")], {
        initialValue: ""
      });
      this.props.form.getFieldDecorator([this.generatePropName("address.street")], {
        initialValue: ""
      });
      this.props.form.getFieldDecorator([this.generatePropName("address.postalCode")], {
        initialValue: ""
      });
      this.props.form.getFieldDecorator([this.generatePropName("address.unitNo")], {
        initialValue: ""
      });
      this.props.form.setFieldsValue(valueObj); // }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var C = this.getComponents();

      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          isNotSearh = _this$props.isNotSearh,
          registrationNoProp = _this$props.registrationNoProp,
          _this$props$groupType = _this$props.groupType,
          groupType = _this$props$groupType === void 0 ? "ORG" : _this$props$groupType,
          isCedant = _this$props.isCedant,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_this$props, ["model", "form", "isNotSearh", "registrationNoProp", "groupType", "isCedant"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.BoxContent, Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        size: "large",
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
        propName: this.generatePropName("name"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }), !isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_10__["NFormItem"], {
        form: form,
        model: model,
        label: registrationNoProp ? _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Company Name").thai("Company Name").my("Company Name").getMessage() : _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Name").thai("ชื่อ").my("နာမတျောကို").getMessage(),
        propName: this.generatePropName("name"),
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_choice_search_list__WEBPACK_IMPORTED_MODULE_14__["default"], {
        ref: this.initNameRef,
        type: "name",
        isCedant: isCedant,
        isNotSearh: isNotSearh,
        groupType: groupType,
        onSelectList: function onSelectList(itemInfo) {
          var item = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(itemInfo);

          if (item.blur && item.blur === "blur") {
            _this3.setValueToModel(item.name, _this3.generatePropName("name"));

            _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, _this3.generatePropName("name"), item.name));
          } else {
            _this3.idNoPage.setShowLabel(item.idNo);

            _this3.setFormValues(item);

            _this3.props.onSelectChange && _this3.props.onSelectChange(item);
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].Fragment, null, isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        size: "large",
        form: form,
        model: model,
        propName: this.generatePropName("registrationNo"),
        required: true,
        transform: function transform() {
          return (_this3.getValueFromModel(_this3.generatePropName("registrationNo")) || "").toUpperCase();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration No.").thai("Registration No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 148
        },
        __self: this
      }), !isNotSearh && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_10__["NFormItem"], {
        form: form,
        model: model,
        isCedant: isCedant,
        propName: this.generatePropName(registrationNoProp || "registrationNo"),
        required: true,
        transform: function transform() {
          return (_this3.getValueFromModel(_this3.generatePropName(registrationNoProp || "registrationNo")) || "").toUpperCase();
        },
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration No.").thai("Registration No.").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 161
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(_choice_search_list__WEBPACK_IMPORTED_MODULE_14__["default"], {
        type: "idNo",
        isNotSearh: isNotSearh,
        ref: this.initIdNoRef,
        groupType: groupType,
        isValid: function isValid() {
          form.validateFields([_this3.generatePropName(registrationNoProp || "registrationNo")], {
            force: true
          });
        },
        onSelectList: function onSelectList(itemInfo) {
          var item = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.cloneDeep(itemInfo);

          if (item.blur && item.blur === "blur") {
            _this3.setValueToModel(item.idNo, _this3.generatePropName(registrationNoProp || "registrationNo"));

            _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])({}, _this3.generatePropName(registrationNoProp || "registrationNo"), item.idNo));
          } else {
            _this3.namePage.setShowLabel(item.name);

            _this3.setFormValues(item);

            _this3.props.onSelectChange && _this3.props.onSelectChange(item);
          }

          form.validateFields([_this3.generatePropName("idNo")], {
            force: true
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174
        },
        __self: this
      }))));
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(GroupCompanyInfo.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataId) {
      if (!!dataId) return "".concat(dataId, ".").concat(propName);
      return "".concat(this.props.dataId, ".").concat(propName);
    }
  }]);

  return GroupCompanyInfo;
}(_component__WEBPACK_IMPORTED_MODULE_10__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (GroupCompanyInfo);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2dyb3VwLWN1c3RvbWVyLWNvbXBhbnkvZ3JvdXAtY29tcGFueS1pbmZvLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9ncm91cC1jdXN0b21lci1jb21wYW55L2dyb3VwLWNvbXBhbnktaW5mby50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IENob2ljZVNlYXJjaExpc3QgZnJvbSBcIi4vY2hvaWNlLXNlYXJjaC1saXN0XCI7XG5cblxuaW50ZXJmYWNlIElEVHlwZU5hbWVTdGF0ZSB7XG59XG5cbnR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG50eXBlIElEVHlwZU5hbWVQYWdlQ29tcG9uZW50cyA9IHtcbiAgQm94Q29udGVudDogU3R5bGVkRElWO1xufTtcbmV4cG9ydCB0eXBlIElEVHlwZU5hbWVQcm9wcyA9IHtcbiAgbW9kZWw6IGFueTtcbiAgZm9ybTogYW55O1xuICBkYXRhSWQ6IGFueTtcbiAgaXNOb3RTZWFyaD86IGFueTtcbiAgb25TZWxlY3RDaGFuZ2U/OiBhbnk7XG4gIGNvdW50cnlDb2RlPzogYW55O1xuICBpc0NlZGFudD86IGFueTtcbiAgcmVnaXN0cmF0aW9uTm9Qcm9wPzogYW55O1xuICBncm91cFR5cGU/OiBcIklORElcIiB8IFwiT1JHXCI7XG59ICYgTW9kZWxXaWRnZXRQcm9wcztcblxuY2xhc3MgR3JvdXBDb21wYW55SW5mbzxQIGV4dGVuZHMgSURUeXBlTmFtZVByb3BzLFxuICBTIGV4dGVuZHMgSURUeXBlTmFtZVN0YXRlLFxuICBDIGV4dGVuZHMgSURUeXBlTmFtZVBhZ2VDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgcHVibGljIGlkTm9QYWdlOiBhbnk7XG4gIHB1YmxpYyBuYW1lUGFnZTogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBJRFR5cGVOYW1lUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCB7IHJlZ2lzdHJhdGlvbk5vUHJvcCB9ID0gdGhpcy5wcm9wcztcbiAgICB0aGlzLmlkTm9QYWdlICYmIHRoaXMuaWROb1BhZ2Uuc2V0U2hvd0xhYmVsKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKHJlZ2lzdHJhdGlvbk5vUHJvcCB8fCBcInJlZ2lzdHJhdGlvbk5vXCIpKSk7XG4gICAgdGhpcy5uYW1lUGFnZSAmJiB0aGlzLm5hbWVQYWdlLnNldFNob3dMYWJlbCh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcIm5hbWVcIikpKTtcbiAgfVxuXG4gIGluaXRJZE5vUmVmID0gKHJlZjogYW55KSA9PiB7XG4gICAgdGhpcy5pZE5vUGFnZSA9IHJlZjtcbiAgfTtcblxuICBpbml0TmFtZVJlZiA9IChyZWY6IGFueSkgPT4ge1xuICAgIHRoaXMubmFtZVBhZ2UgPSByZWY7XG4gIH07XG5cbiAgc2V0Rm9ybVZhbHVlcyhpdGVtOiBhbnkpIHtcbiAgICBjb25zdCB7IHJlZ2lzdHJhdGlvbk5vUHJvcCB9ID0gdGhpcy5wcm9wcztcbiAgICBbXCJuYW1lXCIsIFwiaWROb1wiLCBcIm1vYmlsZVwiLCBcIm1vYmlsZU5hdGlvbkNvZGVcIiwgXCJlbWFpbFwiLCBcIm9mZmljZVRlbFwiLCBcIm9mZmljZVRlbE5hdGlvbkNvZGVcIl0ubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChldmVyeSA9PT0gXCJuYW1lXCIgP1xuICAgICAgICBpdGVtW1wibmFtZVwiXSA6IGl0ZW1bZXZlcnldLCBldmVyeSA9PT0gXCJpZE5vXCIgP1xuICAgICAgICB0aGlzLmdlbmVyYXRlUHJvcE5hbWUocmVnaXN0cmF0aW9uTm9Qcm9wIHx8IFwicmVnaXN0cmF0aW9uTm9cIikgOiB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoZXZlcnkpKTtcbiAgICAgIHRoaXMucHJvcHMuZm9ybS5nZXRGaWVsZERlY29yYXRvcihbZXZlcnkgPT09IFwiaWROb1wiID9cbiAgICAgICAgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKHJlZ2lzdHJhdGlvbk5vUHJvcCB8fCBcInJlZ2lzdHJhdGlvbk5vXCIpIDogdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGV2ZXJ5KV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHtcbiAgICAgICAgW2V2ZXJ5ID09PSBcImlkTm9cIiA/XG4gICAgICAgICAgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKHJlZ2lzdHJhdGlvbk5vUHJvcCB8fCBcInJlZ2lzdHJhdGlvbk5vXCIpIDogdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGV2ZXJ5KV06IF8uZ2V0KGl0ZW0sIGV2ZXJ5KSxcbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKF8uZ2V0KGl0ZW0sIFwiYWRkcmVzc1R5cGVcIiksIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NUeXBlXCIpKTtcbiAgICB0aGlzLnByb3BzLmZvcm0uZ2V0RmllbGREZWNvcmF0b3IoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3NUeXBlXCIpXSwgeyBpbml0aWFsVmFsdWU6IFwiXCIgfSk7XG4gICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHtcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzVHlwZVwiKV06IF8uZ2V0KGl0ZW0sIFwiYWRkcmVzc1R5cGVcIiksXG4gICAgfSk7XG4gICAgLy8gaWYgKHRoaXMucHJvcHMuY291bnRyeUNvZGUgJiYgKHRoaXMucHJvcHMuY291bnRyeUNvZGUgPT09IF8uZ2V0KGl0ZW0sIFwiYWRkcmVzcy5jb3VudHJ5Q29kZVwiKSkpIHtcbiAgICBjb25zdCB2YWx1ZU9iaiA9IHtcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzXCIpXTogXy5nZXQoaXRlbSwgXCJhZGRyZXNzXCIpLFxuICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImFkZHJlc3Muc3RyZWV0XCIpXTogXy5nZXQoaXRlbSwgXCJhZGRyZXNzLnN0cmVldFwiKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnBvc3RhbENvZGVcIildOiBfLmdldChpdGVtLCBcImFkZHJlc3MucG9zdGFsQ29kZVwiKSxcbiAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnVuaXROb1wiKV06IF8uZ2V0KGl0ZW0sIFwiYWRkcmVzcy51bml0Tm9cIiksXG4gICAgICBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzcy5jb3VudHJ5Q29kZVwiKV06IF8uZ2V0KGl0ZW0sIFwiYWRkcmVzcy5jb3VudHJ5Q29kZVwiKSxcbiAgICB9O1xuICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh2YWx1ZU9iaik7XG4gICAgdGhpcy5wcm9wcy5mb3JtLmdldEZpZWxkRGVjb3JhdG9yKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzXCIpXSwgeyBpbml0aWFsVmFsdWU6IFwiXCIgfSk7XG4gICAgdGhpcy5wcm9wcy5mb3JtLmdldEZpZWxkRGVjb3JhdG9yKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnN0cmVldFwiKV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuICAgIHRoaXMucHJvcHMuZm9ybS5nZXRGaWVsZERlY29yYXRvcihbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiYWRkcmVzcy5wb3N0YWxDb2RlXCIpXSwgeyBpbml0aWFsVmFsdWU6IFwiXCIgfSk7XG4gICAgdGhpcy5wcm9wcy5mb3JtLmdldEZpZWxkRGVjb3JhdG9yKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJhZGRyZXNzLnVuaXROb1wiKV0sIHsgaW5pdGlhbFZhbHVlOiBcIlwiIH0pO1xuXG4gICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHZhbHVlT2JqKTtcblxuICAgIC8vIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSwgaXNOb3RTZWFyaCwgcmVnaXN0cmF0aW9uTm9Qcm9wLCBncm91cFR5cGUgPSBcIk9SR1wiLCBpc0NlZGFudCwgLi4ucmVzdCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94Q29udGVudCB7Li4ucmVzdH0+XG4gICAgICAgIDw+XG4gICAgICAgICAge2lzTm90U2VhcmggJiYgPE5UZXh0XG4gICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiTmFtZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rVwiKVxuICAgICAgICAgICAgICAubXkoXCLhgJThgKzhgJnhgJDhgLvhgLHhgKzhgIDhgK3hgK9cIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgLz59XG4gICAgICAgICAgeyFpc05vdFNlYXJoICYmIDxORm9ybUl0ZW1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBsYWJlbD17cmVnaXN0cmF0aW9uTm9Qcm9wID9cbiAgICAgICAgICAgICAgTGFuZ3VhZ2UuZW4oXCJDb21wYW55IE5hbWVcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIkNvbXBhbnkgTmFtZVwiKVxuICAgICAgICAgICAgICAgIC5teShcIkNvbXBhbnkgTmFtZVwiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKClcbiAgICAgICAgICAgICAgOlxuICAgICAgICAgICAgICBMYW5ndWFnZS5lbihcIk5hbWVcIilcbiAgICAgICAgICAgICAgICAudGhhaShcIuC4iuC4t+C5iOC4rVwiKVxuICAgICAgICAgICAgICAgIC5teShcIuGAlOGArOGAmeGAkOGAu+GAseGArOGAgOGAreGAr1wiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibmFtZVwiKX1cbiAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxDaG9pY2VTZWFyY2hMaXN0XG4gICAgICAgICAgICAgIHJlZj17dGhpcy5pbml0TmFtZVJlZn1cbiAgICAgICAgICAgICAgdHlwZT17XCJuYW1lXCJ9XG4gICAgICAgICAgICAgIGlzQ2VkYW50PXtpc0NlZGFudH1cbiAgICAgICAgICAgICAgaXNOb3RTZWFyaD17aXNOb3RTZWFyaH1cbiAgICAgICAgICAgICAgZ3JvdXBUeXBlPXtncm91cFR5cGV9XG4gICAgICAgICAgICAgIG9uU2VsZWN0TGlzdD17KGl0ZW1JbmZvOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBpdGVtID0gXy5jbG9uZURlZXAoaXRlbUluZm8pO1xuICAgICAgICAgICAgICAgIGlmIChpdGVtLmJsdXIgJiYgaXRlbS5ibHVyID09PSBcImJsdXJcIikge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbS5uYW1lLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpKTtcbiAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7IFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJuYW1lXCIpXTogaXRlbS5uYW1lIH0pO1xuXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuaWROb1BhZ2Uuc2V0U2hvd0xhYmVsKGl0ZW0uaWRObyk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnNldEZvcm1WYWx1ZXMoaXRlbSk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLm9uU2VsZWN0Q2hhbmdlICYmIHRoaXMucHJvcHMub25TZWxlY3RDaGFuZ2UoaXRlbSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L05Gb3JtSXRlbT59XG4gICAgICAgIDwvPlxuXG4gICAgICAgIDw+XG4gICAgICAgICAge2lzTm90U2VhcmggJiYgPE5UZXh0XG4gICAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInJlZ2lzdHJhdGlvbk5vXCIpfVxuICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICB0cmFuc2Zvcm09eygpOiBhbnkgPT4ge1xuICAgICAgICAgICAgICByZXR1cm4gKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicmVnaXN0cmF0aW9uTm9cIikpIHx8IFwiXCIpLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiUmVnaXN0cmF0aW9uIE5vLlwiKVxuICAgICAgICAgICAgICAudGhhaShcIlJlZ2lzdHJhdGlvbiBOby5cIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAvPn1cbiAgICAgICAgICB7IWlzTm90U2VhcmggJiYgPE5Gb3JtSXRlbVxuICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgIGlzQ2VkYW50PXtpc0NlZGFudH1cbiAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUocmVnaXN0cmF0aW9uTm9Qcm9wIHx8IFwicmVnaXN0cmF0aW9uTm9cIil9XG4gICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgIHRyYW5zZm9ybT17KCk6IGFueSA9PiB7XG4gICAgICAgICAgICAgIHJldHVybiAodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUocmVnaXN0cmF0aW9uTm9Qcm9wIHx8IFwicmVnaXN0cmF0aW9uTm9cIikpIHx8IFwiXCIpLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiUmVnaXN0cmF0aW9uIE5vLlwiKVxuICAgICAgICAgICAgICAudGhhaShcIlJlZ2lzdHJhdGlvbiBOby5cIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Q2hvaWNlU2VhcmNoTGlzdFxuICAgICAgICAgICAgICB0eXBlPXtcImlkTm9cIn1cbiAgICAgICAgICAgICAgaXNOb3RTZWFyaD17aXNOb3RTZWFyaH1cbiAgICAgICAgICAgICAgcmVmPXt0aGlzLmluaXRJZE5vUmVmfVxuICAgICAgICAgICAgICBncm91cFR5cGU9e2dyb3VwVHlwZX1cbiAgICAgICAgICAgICAgaXNWYWxpZD17KCkgPT4ge1xuICAgICAgICAgICAgICAgIGZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShyZWdpc3RyYXRpb25Ob1Byb3AgfHwgXCJyZWdpc3RyYXRpb25Ob1wiKV0sIHsgZm9yY2U6IHRydWUgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIG9uU2VsZWN0TGlzdD17KGl0ZW1JbmZvOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBpdGVtID0gXy5jbG9uZURlZXAoaXRlbUluZm8pO1xuICAgICAgICAgICAgICAgIGlmIChpdGVtLmJsdXIgJiYgaXRlbS5ibHVyID09PSBcImJsdXJcIikge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoaXRlbS5pZE5vLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUocmVnaXN0cmF0aW9uTm9Qcm9wIHx8IFwicmVnaXN0cmF0aW9uTm9cIikpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShyZWdpc3RyYXRpb25Ob1Byb3AgfHwgXCJyZWdpc3RyYXRpb25Ob1wiKV06IGl0ZW0uaWRObyB9KTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLm5hbWVQYWdlLnNldFNob3dMYWJlbChpdGVtLm5hbWUpO1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRGb3JtVmFsdWVzKGl0ZW0pO1xuICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vblNlbGVjdENoYW5nZSAmJiB0aGlzLnByb3BzLm9uU2VsZWN0Q2hhbmdlKGl0ZW0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBmb3JtLnZhbGlkYXRlRmllbGRzKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJpZE5vXCIpXSwgeyBmb3JjZTogdHJ1ZSB9KTtcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9ORm9ybUl0ZW0+fVxuICAgICAgICA8Lz5cbiAgICAgIDwvQy5Cb3hDb250ZW50PlxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIEJveENvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHt9KSBhcyBTO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkPzogc3RyaW5nKSB7XG4gICAgaWYgKCEhZGF0YUlkKSByZXR1cm4gYCR7ZGF0YUlkfS4ke3Byb3BOYW1lfWA7XG4gICAgcmV0dXJuIGAke3RoaXMucHJvcHMuZGF0YUlkfS4ke3Byb3BOYW1lfWA7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgR3JvdXBDb21wYW55SW5mbztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXFCQTs7Ozs7QUFNQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQVdBO0FBQ0E7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBakJBO0FBRUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTs7O0FBVUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUdBO0FBT0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFDQTtBQVVBO0FBQ0E7QUFkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQWpCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFwQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBMEJBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBdkxBO0FBQ0E7QUF5TEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/group-customer-company/group-company-info.tsx
