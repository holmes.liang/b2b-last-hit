/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var defaultOption = {
  show: true,
  zlevel: 0,
  z: 0,
  // Inverse the axis.
  inverse: false,
  // Axis name displayed.
  name: '',
  // 'start' | 'middle' | 'end'
  nameLocation: 'end',
  // By degree. By defualt auto rotate by nameLocation.
  nameRotate: null,
  nameTruncate: {
    maxWidth: null,
    ellipsis: '...',
    placeholder: '.'
  },
  // Use global text style by default.
  nameTextStyle: {},
  // The gap between axisName and axisLine.
  nameGap: 15,
  // Default `false` to support tooltip.
  silent: false,
  // Default `false` to avoid legacy user event listener fail.
  triggerEvent: false,
  tooltip: {
    show: false
  },
  axisPointer: {},
  axisLine: {
    show: true,
    onZero: true,
    onZeroAxisIndex: null,
    lineStyle: {
      color: '#333',
      width: 1,
      type: 'solid'
    },
    // The arrow at both ends the the axis.
    symbol: ['none', 'none'],
    symbolSize: [10, 15]
  },
  axisTick: {
    show: true,
    // Whether axisTick is inside the grid or outside the grid.
    inside: false,
    // The length of axisTick.
    length: 5,
    lineStyle: {
      width: 1
    }
  },
  axisLabel: {
    show: true,
    // Whether axisLabel is inside the grid or outside the grid.
    inside: false,
    rotate: 0,
    // true | false | null/undefined (auto)
    showMinLabel: null,
    // true | false | null/undefined (auto)
    showMaxLabel: null,
    margin: 8,
    // formatter: null,
    fontSize: 12
  },
  splitLine: {
    show: true,
    lineStyle: {
      color: ['#ccc'],
      width: 1,
      type: 'solid'
    }
  },
  splitArea: {
    show: false,
    areaStyle: {
      color: ['rgba(250,250,250,0.3)', 'rgba(200,200,200,0.3)']
    }
  }
};
var axisDefault = {};
axisDefault.categoryAxis = zrUtil.merge({
  // The gap at both ends of the axis. For categoryAxis, boolean.
  boundaryGap: true,
  // Set false to faster category collection.
  // Only usefull in the case like: category is
  // ['2012-01-01', '2012-01-02', ...], where the input
  // data has been ensured not duplicate and is large data.
  // null means "auto":
  // if axis.data provided, do not deduplication,
  // else do deduplication.
  deduplication: null,
  // splitArea: {
  // show: false
  // },
  splitLine: {
    show: false
  },
  axisTick: {
    // If tick is align with label when boundaryGap is true
    alignWithLabel: false,
    interval: 'auto'
  },
  axisLabel: {
    interval: 'auto'
  }
}, defaultOption);
axisDefault.valueAxis = zrUtil.merge({
  // The gap at both ends of the axis. For value axis, [GAP, GAP], where
  // `GAP` can be an absolute pixel number (like `35`), or percent (like `'30%'`)
  boundaryGap: [0, 0],
  // TODO
  // min/max: [30, datamin, 60] or [20, datamin] or [datamin, 60]
  // Min value of the axis. can be:
  // + a number
  // + 'dataMin': use the min value in data.
  // + null/undefined: auto decide min value (consider pretty look and boundaryGap).
  // min: null,
  // Max value of the axis. can be:
  // + a number
  // + 'dataMax': use the max value in data.
  // + null/undefined: auto decide max value (consider pretty look and boundaryGap).
  // max: null,
  // Readonly prop, specifies start value of the range when using data zoom.
  // rangeStart: null
  // Readonly prop, specifies end value of the range when using data zoom.
  // rangeEnd: null
  // Optional value can be:
  // + `false`: always include value 0.
  // + `true`: the extent do not consider value 0.
  // scale: false,
  // AxisTick and axisLabel and splitLine are caculated based on splitNumber.
  splitNumber: 5 // Interval specifies the span of the ticks is mandatorily.
  // interval: null
  // Specify min interval when auto calculate tick interval.
  // minInterval: null
  // Specify max interval when auto calculate tick interval.
  // maxInterval: null

}, defaultOption);
axisDefault.timeAxis = zrUtil.defaults({
  scale: true,
  min: 'dataMin',
  max: 'dataMax'
}, axisDefault.valueAxis);
axisDefault.logAxis = zrUtil.defaults({
  scale: true,
  logBase: 10
}, axisDefault.valueAxis);
var _default = axisDefault;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvYXhpc0RlZmF1bHQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jb29yZC9heGlzRGVmYXVsdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBkZWZhdWx0T3B0aW9uID0ge1xuICBzaG93OiB0cnVlLFxuICB6bGV2ZWw6IDAsXG4gIHo6IDAsXG4gIC8vIEludmVyc2UgdGhlIGF4aXMuXG4gIGludmVyc2U6IGZhbHNlLFxuICAvLyBBeGlzIG5hbWUgZGlzcGxheWVkLlxuICBuYW1lOiAnJyxcbiAgLy8gJ3N0YXJ0JyB8ICdtaWRkbGUnIHwgJ2VuZCdcbiAgbmFtZUxvY2F0aW9uOiAnZW5kJyxcbiAgLy8gQnkgZGVncmVlLiBCeSBkZWZ1YWx0IGF1dG8gcm90YXRlIGJ5IG5hbWVMb2NhdGlvbi5cbiAgbmFtZVJvdGF0ZTogbnVsbCxcbiAgbmFtZVRydW5jYXRlOiB7XG4gICAgbWF4V2lkdGg6IG51bGwsXG4gICAgZWxsaXBzaXM6ICcuLi4nLFxuICAgIHBsYWNlaG9sZGVyOiAnLidcbiAgfSxcbiAgLy8gVXNlIGdsb2JhbCB0ZXh0IHN0eWxlIGJ5IGRlZmF1bHQuXG4gIG5hbWVUZXh0U3R5bGU6IHt9LFxuICAvLyBUaGUgZ2FwIGJldHdlZW4gYXhpc05hbWUgYW5kIGF4aXNMaW5lLlxuICBuYW1lR2FwOiAxNSxcbiAgLy8gRGVmYXVsdCBgZmFsc2VgIHRvIHN1cHBvcnQgdG9vbHRpcC5cbiAgc2lsZW50OiBmYWxzZSxcbiAgLy8gRGVmYXVsdCBgZmFsc2VgIHRvIGF2b2lkIGxlZ2FjeSB1c2VyIGV2ZW50IGxpc3RlbmVyIGZhaWwuXG4gIHRyaWdnZXJFdmVudDogZmFsc2UsXG4gIHRvb2x0aXA6IHtcbiAgICBzaG93OiBmYWxzZVxuICB9LFxuICBheGlzUG9pbnRlcjoge30sXG4gIGF4aXNMaW5lOiB7XG4gICAgc2hvdzogdHJ1ZSxcbiAgICBvblplcm86IHRydWUsXG4gICAgb25aZXJvQXhpc0luZGV4OiBudWxsLFxuICAgIGxpbmVTdHlsZToge1xuICAgICAgY29sb3I6ICcjMzMzJyxcbiAgICAgIHdpZHRoOiAxLFxuICAgICAgdHlwZTogJ3NvbGlkJ1xuICAgIH0sXG4gICAgLy8gVGhlIGFycm93IGF0IGJvdGggZW5kcyB0aGUgdGhlIGF4aXMuXG4gICAgc3ltYm9sOiBbJ25vbmUnLCAnbm9uZSddLFxuICAgIHN5bWJvbFNpemU6IFsxMCwgMTVdXG4gIH0sXG4gIGF4aXNUaWNrOiB7XG4gICAgc2hvdzogdHJ1ZSxcbiAgICAvLyBXaGV0aGVyIGF4aXNUaWNrIGlzIGluc2lkZSB0aGUgZ3JpZCBvciBvdXRzaWRlIHRoZSBncmlkLlxuICAgIGluc2lkZTogZmFsc2UsXG4gICAgLy8gVGhlIGxlbmd0aCBvZiBheGlzVGljay5cbiAgICBsZW5ndGg6IDUsXG4gICAgbGluZVN0eWxlOiB7XG4gICAgICB3aWR0aDogMVxuICAgIH1cbiAgfSxcbiAgYXhpc0xhYmVsOiB7XG4gICAgc2hvdzogdHJ1ZSxcbiAgICAvLyBXaGV0aGVyIGF4aXNMYWJlbCBpcyBpbnNpZGUgdGhlIGdyaWQgb3Igb3V0c2lkZSB0aGUgZ3JpZC5cbiAgICBpbnNpZGU6IGZhbHNlLFxuICAgIHJvdGF0ZTogMCxcbiAgICAvLyB0cnVlIHwgZmFsc2UgfCBudWxsL3VuZGVmaW5lZCAoYXV0bylcbiAgICBzaG93TWluTGFiZWw6IG51bGwsXG4gICAgLy8gdHJ1ZSB8IGZhbHNlIHwgbnVsbC91bmRlZmluZWQgKGF1dG8pXG4gICAgc2hvd01heExhYmVsOiBudWxsLFxuICAgIG1hcmdpbjogOCxcbiAgICAvLyBmb3JtYXR0ZXI6IG51bGwsXG4gICAgZm9udFNpemU6IDEyXG4gIH0sXG4gIHNwbGl0TGluZToge1xuICAgIHNob3c6IHRydWUsXG4gICAgbGluZVN0eWxlOiB7XG4gICAgICBjb2xvcjogWycjY2NjJ10sXG4gICAgICB3aWR0aDogMSxcbiAgICAgIHR5cGU6ICdzb2xpZCdcbiAgICB9XG4gIH0sXG4gIHNwbGl0QXJlYToge1xuICAgIHNob3c6IGZhbHNlLFxuICAgIGFyZWFTdHlsZToge1xuICAgICAgY29sb3I6IFsncmdiYSgyNTAsMjUwLDI1MCwwLjMpJywgJ3JnYmEoMjAwLDIwMCwyMDAsMC4zKSddXG4gICAgfVxuICB9XG59O1xudmFyIGF4aXNEZWZhdWx0ID0ge307XG5heGlzRGVmYXVsdC5jYXRlZ29yeUF4aXMgPSB6clV0aWwubWVyZ2Uoe1xuICAvLyBUaGUgZ2FwIGF0IGJvdGggZW5kcyBvZiB0aGUgYXhpcy4gRm9yIGNhdGVnb3J5QXhpcywgYm9vbGVhbi5cbiAgYm91bmRhcnlHYXA6IHRydWUsXG4gIC8vIFNldCBmYWxzZSB0byBmYXN0ZXIgY2F0ZWdvcnkgY29sbGVjdGlvbi5cbiAgLy8gT25seSB1c2VmdWxsIGluIHRoZSBjYXNlIGxpa2U6IGNhdGVnb3J5IGlzXG4gIC8vIFsnMjAxMi0wMS0wMScsICcyMDEyLTAxLTAyJywgLi4uXSwgd2hlcmUgdGhlIGlucHV0XG4gIC8vIGRhdGEgaGFzIGJlZW4gZW5zdXJlZCBub3QgZHVwbGljYXRlIGFuZCBpcyBsYXJnZSBkYXRhLlxuICAvLyBudWxsIG1lYW5zIFwiYXV0b1wiOlxuICAvLyBpZiBheGlzLmRhdGEgcHJvdmlkZWQsIGRvIG5vdCBkZWR1cGxpY2F0aW9uLFxuICAvLyBlbHNlIGRvIGRlZHVwbGljYXRpb24uXG4gIGRlZHVwbGljYXRpb246IG51bGwsXG4gIC8vIHNwbGl0QXJlYToge1xuICAvLyBzaG93OiBmYWxzZVxuICAvLyB9LFxuICBzcGxpdExpbmU6IHtcbiAgICBzaG93OiBmYWxzZVxuICB9LFxuICBheGlzVGljazoge1xuICAgIC8vIElmIHRpY2sgaXMgYWxpZ24gd2l0aCBsYWJlbCB3aGVuIGJvdW5kYXJ5R2FwIGlzIHRydWVcbiAgICBhbGlnbldpdGhMYWJlbDogZmFsc2UsXG4gICAgaW50ZXJ2YWw6ICdhdXRvJ1xuICB9LFxuICBheGlzTGFiZWw6IHtcbiAgICBpbnRlcnZhbDogJ2F1dG8nXG4gIH1cbn0sIGRlZmF1bHRPcHRpb24pO1xuYXhpc0RlZmF1bHQudmFsdWVBeGlzID0genJVdGlsLm1lcmdlKHtcbiAgLy8gVGhlIGdhcCBhdCBib3RoIGVuZHMgb2YgdGhlIGF4aXMuIEZvciB2YWx1ZSBheGlzLCBbR0FQLCBHQVBdLCB3aGVyZVxuICAvLyBgR0FQYCBjYW4gYmUgYW4gYWJzb2x1dGUgcGl4ZWwgbnVtYmVyIChsaWtlIGAzNWApLCBvciBwZXJjZW50IChsaWtlIGAnMzAlJ2ApXG4gIGJvdW5kYXJ5R2FwOiBbMCwgMF0sXG4gIC8vIFRPRE9cbiAgLy8gbWluL21heDogWzMwLCBkYXRhbWluLCA2MF0gb3IgWzIwLCBkYXRhbWluXSBvciBbZGF0YW1pbiwgNjBdXG4gIC8vIE1pbiB2YWx1ZSBvZiB0aGUgYXhpcy4gY2FuIGJlOlxuICAvLyArIGEgbnVtYmVyXG4gIC8vICsgJ2RhdGFNaW4nOiB1c2UgdGhlIG1pbiB2YWx1ZSBpbiBkYXRhLlxuICAvLyArIG51bGwvdW5kZWZpbmVkOiBhdXRvIGRlY2lkZSBtaW4gdmFsdWUgKGNvbnNpZGVyIHByZXR0eSBsb29rIGFuZCBib3VuZGFyeUdhcCkuXG4gIC8vIG1pbjogbnVsbCxcbiAgLy8gTWF4IHZhbHVlIG9mIHRoZSBheGlzLiBjYW4gYmU6XG4gIC8vICsgYSBudW1iZXJcbiAgLy8gKyAnZGF0YU1heCc6IHVzZSB0aGUgbWF4IHZhbHVlIGluIGRhdGEuXG4gIC8vICsgbnVsbC91bmRlZmluZWQ6IGF1dG8gZGVjaWRlIG1heCB2YWx1ZSAoY29uc2lkZXIgcHJldHR5IGxvb2sgYW5kIGJvdW5kYXJ5R2FwKS5cbiAgLy8gbWF4OiBudWxsLFxuICAvLyBSZWFkb25seSBwcm9wLCBzcGVjaWZpZXMgc3RhcnQgdmFsdWUgb2YgdGhlIHJhbmdlIHdoZW4gdXNpbmcgZGF0YSB6b29tLlxuICAvLyByYW5nZVN0YXJ0OiBudWxsXG4gIC8vIFJlYWRvbmx5IHByb3AsIHNwZWNpZmllcyBlbmQgdmFsdWUgb2YgdGhlIHJhbmdlIHdoZW4gdXNpbmcgZGF0YSB6b29tLlxuICAvLyByYW5nZUVuZDogbnVsbFxuICAvLyBPcHRpb25hbCB2YWx1ZSBjYW4gYmU6XG4gIC8vICsgYGZhbHNlYDogYWx3YXlzIGluY2x1ZGUgdmFsdWUgMC5cbiAgLy8gKyBgdHJ1ZWA6IHRoZSBleHRlbnQgZG8gbm90IGNvbnNpZGVyIHZhbHVlIDAuXG4gIC8vIHNjYWxlOiBmYWxzZSxcbiAgLy8gQXhpc1RpY2sgYW5kIGF4aXNMYWJlbCBhbmQgc3BsaXRMaW5lIGFyZSBjYWN1bGF0ZWQgYmFzZWQgb24gc3BsaXROdW1iZXIuXG4gIHNwbGl0TnVtYmVyOiA1IC8vIEludGVydmFsIHNwZWNpZmllcyB0aGUgc3BhbiBvZiB0aGUgdGlja3MgaXMgbWFuZGF0b3JpbHkuXG4gIC8vIGludGVydmFsOiBudWxsXG4gIC8vIFNwZWNpZnkgbWluIGludGVydmFsIHdoZW4gYXV0byBjYWxjdWxhdGUgdGljayBpbnRlcnZhbC5cbiAgLy8gbWluSW50ZXJ2YWw6IG51bGxcbiAgLy8gU3BlY2lmeSBtYXggaW50ZXJ2YWwgd2hlbiBhdXRvIGNhbGN1bGF0ZSB0aWNrIGludGVydmFsLlxuICAvLyBtYXhJbnRlcnZhbDogbnVsbFxuXG59LCBkZWZhdWx0T3B0aW9uKTtcbmF4aXNEZWZhdWx0LnRpbWVBeGlzID0genJVdGlsLmRlZmF1bHRzKHtcbiAgc2NhbGU6IHRydWUsXG4gIG1pbjogJ2RhdGFNaW4nLFxuICBtYXg6ICdkYXRhTWF4J1xufSwgYXhpc0RlZmF1bHQudmFsdWVBeGlzKTtcbmF4aXNEZWZhdWx0LmxvZ0F4aXMgPSB6clV0aWwuZGVmYXVsdHMoe1xuICBzY2FsZTogdHJ1ZSxcbiAgbG9nQmFzZTogMTBcbn0sIGF4aXNEZWZhdWx0LnZhbHVlQXhpcyk7XG52YXIgX2RlZmF1bHQgPSBheGlzRGVmYXVsdDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQU5BO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBekVBO0FBZ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBREE7QUF0QkE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQS9CQTtBQWlDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/axisDefault.js
