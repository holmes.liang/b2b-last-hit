__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Search; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var is_mobile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! is-mobile */ "./node_modules/is-mobile/index.js");
/* harmony import */ var is_mobile__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(is_mobile__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Input */ "./node_modules/antd/es/input/Input.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../button */ "./node_modules/antd/es/button/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};









var Search =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Search, _React$Component);

  function Search() {
    var _this;

    _classCallCheck(this, Search);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Search).apply(this, arguments));

    _this.saveInput = function (node) {
      _this.input = node;
    };

    _this.onChange = function (e) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          onSearch = _this$props.onSearch;

      if (e && e.target && e.type === 'click' && onSearch) {
        onSearch(e.target.value, e);
      }

      if (onChange) {
        onChange(e);
      }
    };

    _this.onSearch = function (e) {
      var _this$props2 = _this.props,
          onSearch = _this$props2.onSearch,
          loading = _this$props2.loading,
          disabled = _this$props2.disabled;

      if (loading || disabled) {
        return;
      }

      if (onSearch) {
        onSearch(_this.input.input.value, e);
      }

      if (!Object(is_mobile__WEBPACK_IMPORTED_MODULE_2__["isMobile"])({
        tablet: true
      })) {
        _this.input.focus();
      }
    };

    _this.renderLoading = function (prefixCls) {
      var _this$props3 = _this.props,
          enterButton = _this$props3.enterButton,
          size = _this$props3.size;

      if (enterButton) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_button__WEBPACK_IMPORTED_MODULE_5__["default"], {
          className: "".concat(prefixCls, "-button"),
          type: "primary",
          size: size,
          key: "enterButton"
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "loading"
        }));
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        className: "".concat(prefixCls, "-icon"),
        type: "loading",
        key: "loadingIcon"
      });
    };

    _this.renderSuffix = function (prefixCls) {
      var _this$props4 = _this.props,
          suffix = _this$props4.suffix,
          enterButton = _this$props4.enterButton,
          loading = _this$props4.loading;

      if (loading && !enterButton) {
        return [suffix, _this.renderLoading(prefixCls)];
      }

      if (enterButton) return suffix;
      var icon = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
        className: "".concat(prefixCls, "-icon"),
        type: "search",
        key: "searchIcon",
        onClick: _this.onSearch
      });

      if (suffix) {
        return [react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](suffix) ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](suffix, {
          key: 'suffix'
        }) : null, icon];
      }

      return icon;
    };

    _this.renderAddonAfter = function (prefixCls) {
      var _this$props5 = _this.props,
          enterButton = _this$props5.enterButton,
          size = _this$props5.size,
          disabled = _this$props5.disabled,
          addonAfter = _this$props5.addonAfter,
          loading = _this$props5.loading;
      var btnClassName = "".concat(prefixCls, "-button");

      if (loading && enterButton) {
        return [_this.renderLoading(prefixCls), addonAfter];
      }

      if (!enterButton) return addonAfter;
      var button;
      var enterButtonAsElement = enterButton;
      var isAntdButton = enterButtonAsElement.type && enterButtonAsElement.type.__ANT_BUTTON === true;

      if (isAntdButton || enterButtonAsElement.type === 'button') {
        button = react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](enterButtonAsElement, _extends({
          onClick: _this.onSearch,
          key: 'enterButton'
        }, isAntdButton ? {
          className: btnClassName,
          size: size
        } : {}));
      } else {
        button = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_button__WEBPACK_IMPORTED_MODULE_5__["default"], {
          className: btnClassName,
          type: "primary",
          size: size,
          disabled: disabled,
          key: "enterButton",
          onClick: _this.onSearch
        }, enterButton === true ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "search"
        }) : enterButton);
      }

      if (addonAfter) {
        return [button, react__WEBPACK_IMPORTED_MODULE_0__["isValidElement"](addonAfter) ? react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](addonAfter, {
          key: 'addonAfter'
        }) : null];
      }

      return button;
    };

    _this.renderSearch = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          customizeInputPrefixCls = _a.inputPrefixCls,
          size = _a.size,
          enterButton = _a.enterButton,
          className = _a.className,
          restProps = __rest(_a, ["prefixCls", "inputPrefixCls", "size", "enterButton", "className"]);

      delete restProps.onSearch;
      delete restProps.loading;
      var prefixCls = getPrefixCls('input-search', customizePrefixCls);
      var inputPrefixCls = getPrefixCls('input', customizeInputPrefixCls);
      var inputClassName;

      if (enterButton) {
        var _classNames;

        inputClassName = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-enter-button"), !!enterButton), _defineProperty(_classNames, "".concat(prefixCls, "-").concat(size), !!size), _classNames));
      } else {
        inputClassName = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className);
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Input__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({
        onPressEnter: _this.onSearch
      }, restProps, {
        size: size,
        prefixCls: inputPrefixCls,
        addonAfter: _this.renderAddonAfter(prefixCls),
        suffix: _this.renderSuffix(prefixCls),
        onChange: _this.onChange,
        ref: _this.saveInput,
        className: inputClassName
      }));
    };

    return _this;
  }

  _createClass(Search, [{
    key: "focus",
    value: function focus() {
      this.input.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.input.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderSearch);
    }
  }]);

  return Search;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Search.defaultProps = {
  enterButton: false
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9TZWFyY2guanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2lucHV0L1NlYXJjaC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBpc01vYmlsZSB9IGZyb20gJ2lzLW1vYmlsZSc7XG5pbXBvcnQgSW5wdXQgZnJvbSAnLi9JbnB1dCc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCBCdXR0b24gZnJvbSAnLi4vYnV0dG9uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFNlYXJjaCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuc2F2ZUlucHV0ID0gKG5vZGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaW5wdXQgPSBub2RlO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uQ2hhbmdlID0gKGUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25DaGFuZ2UsIG9uU2VhcmNoIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGUgJiYgZS50YXJnZXQgJiYgZS50eXBlID09PSAnY2xpY2snICYmIG9uU2VhcmNoKSB7XG4gICAgICAgICAgICAgICAgb25TZWFyY2goZS50YXJnZXQudmFsdWUsIGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgb25DaGFuZ2UoZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25TZWFyY2ggPSAoZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvblNlYXJjaCwgbG9hZGluZywgZGlzYWJsZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAobG9hZGluZyB8fCBkaXNhYmxlZCkge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChvblNlYXJjaCkge1xuICAgICAgICAgICAgICAgIG9uU2VhcmNoKHRoaXMuaW5wdXQuaW5wdXQudmFsdWUsIGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKCFpc01vYmlsZSh7IHRhYmxldDogdHJ1ZSB9KSkge1xuICAgICAgICAgICAgICAgIHRoaXMuaW5wdXQuZm9jdXMoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJMb2FkaW5nID0gKHByZWZpeENscykgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBlbnRlckJ1dHRvbiwgc2l6ZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChlbnRlckJ1dHRvbikge1xuICAgICAgICAgICAgICAgIHJldHVybiAoPEJ1dHRvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tYnV0dG9uYH0gdHlwZT1cInByaW1hcnlcIiBzaXplPXtzaXplfSBrZXk9XCJlbnRlckJ1dHRvblwiPlxuICAgICAgICAgIDxJY29uIHR5cGU9XCJsb2FkaW5nXCIvPlxuICAgICAgICA8L0J1dHRvbj4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIDxJY29uIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pY29uYH0gdHlwZT1cImxvYWRpbmdcIiBrZXk9XCJsb2FkaW5nSWNvblwiLz47XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyU3VmZml4ID0gKHByZWZpeENscykgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBzdWZmaXgsIGVudGVyQnV0dG9uLCBsb2FkaW5nIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKGxvYWRpbmcgJiYgIWVudGVyQnV0dG9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFtzdWZmaXgsIHRoaXMucmVuZGVyTG9hZGluZyhwcmVmaXhDbHMpXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChlbnRlckJ1dHRvbilcbiAgICAgICAgICAgICAgICByZXR1cm4gc3VmZml4O1xuICAgICAgICAgICAgY29uc3QgaWNvbiA9ICg8SWNvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taWNvbmB9IHR5cGU9XCJzZWFyY2hcIiBrZXk9XCJzZWFyY2hJY29uXCIgb25DbGljaz17dGhpcy5vblNlYXJjaH0vPik7XG4gICAgICAgICAgICBpZiAoc3VmZml4KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgUmVhY3QuaXNWYWxpZEVsZW1lbnQoc3VmZml4KVxuICAgICAgICAgICAgICAgICAgICAgICAgPyBSZWFjdC5jbG9uZUVsZW1lbnQoc3VmZml4LCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnc3VmZml4JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICA6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIGljb24sXG4gICAgICAgICAgICAgICAgXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBpY29uO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckFkZG9uQWZ0ZXIgPSAocHJlZml4Q2xzKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGVudGVyQnV0dG9uLCBzaXplLCBkaXNhYmxlZCwgYWRkb25BZnRlciwgbG9hZGluZyB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGJ0bkNsYXNzTmFtZSA9IGAke3ByZWZpeENsc30tYnV0dG9uYDtcbiAgICAgICAgICAgIGlmIChsb2FkaW5nICYmIGVudGVyQnV0dG9uKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFt0aGlzLnJlbmRlckxvYWRpbmcocHJlZml4Q2xzKSwgYWRkb25BZnRlcl07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoIWVudGVyQnV0dG9uKVxuICAgICAgICAgICAgICAgIHJldHVybiBhZGRvbkFmdGVyO1xuICAgICAgICAgICAgbGV0IGJ1dHRvbjtcbiAgICAgICAgICAgIGNvbnN0IGVudGVyQnV0dG9uQXNFbGVtZW50ID0gZW50ZXJCdXR0b247XG4gICAgICAgICAgICBjb25zdCBpc0FudGRCdXR0b24gPSBlbnRlckJ1dHRvbkFzRWxlbWVudC50eXBlICYmXG4gICAgICAgICAgICAgICAgZW50ZXJCdXR0b25Bc0VsZW1lbnQudHlwZS5fX0FOVF9CVVRUT04gPT09IHRydWU7XG4gICAgICAgICAgICBpZiAoaXNBbnRkQnV0dG9uIHx8IGVudGVyQnV0dG9uQXNFbGVtZW50LnR5cGUgPT09ICdidXR0b24nKSB7XG4gICAgICAgICAgICAgICAgYnV0dG9uID0gUmVhY3QuY2xvbmVFbGVtZW50KGVudGVyQnV0dG9uQXNFbGVtZW50LCBPYmplY3QuYXNzaWduKHsgb25DbGljazogdGhpcy5vblNlYXJjaCwga2V5OiAnZW50ZXJCdXR0b24nIH0sIChpc0FudGRCdXR0b25cbiAgICAgICAgICAgICAgICAgICAgPyB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGJ0bkNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemUsXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgOiB7fSkpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGJ1dHRvbiA9ICg8QnV0dG9uIGNsYXNzTmFtZT17YnRuQ2xhc3NOYW1lfSB0eXBlPVwicHJpbWFyeVwiIHNpemU9e3NpemV9IGRpc2FibGVkPXtkaXNhYmxlZH0ga2V5PVwiZW50ZXJCdXR0b25cIiBvbkNsaWNrPXt0aGlzLm9uU2VhcmNofT5cbiAgICAgICAgICB7ZW50ZXJCdXR0b24gPT09IHRydWUgPyA8SWNvbiB0eXBlPVwic2VhcmNoXCIvPiA6IGVudGVyQnV0dG9ufVxuICAgICAgICA8L0J1dHRvbj4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGFkZG9uQWZ0ZXIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgICAgICAgICBidXR0b24sXG4gICAgICAgICAgICAgICAgICAgIFJlYWN0LmlzVmFsaWRFbGVtZW50KGFkZG9uQWZ0ZXIpXG4gICAgICAgICAgICAgICAgICAgICAgICA/IFJlYWN0LmNsb25lRWxlbWVudChhZGRvbkFmdGVyLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnYWRkb25BZnRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgOiBudWxsLFxuICAgICAgICAgICAgICAgIF07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gYnV0dG9uO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlclNlYXJjaCA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGlucHV0UHJlZml4Q2xzOiBjdXN0b21pemVJbnB1dFByZWZpeENscywgc2l6ZSwgZW50ZXJCdXR0b24sIGNsYXNzTmFtZSB9ID0gX2EsIHJlc3RQcm9wcyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwiaW5wdXRQcmVmaXhDbHNcIiwgXCJzaXplXCIsIFwiZW50ZXJCdXR0b25cIiwgXCJjbGFzc05hbWVcIl0pO1xuICAgICAgICAgICAgZGVsZXRlIHJlc3RQcm9wcy5vblNlYXJjaDtcbiAgICAgICAgICAgIGRlbGV0ZSByZXN0UHJvcHMubG9hZGluZztcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnaW5wdXQtc2VhcmNoJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGlucHV0UHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdpbnB1dCcsIGN1c3RvbWl6ZUlucHV0UHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGxldCBpbnB1dENsYXNzTmFtZTtcbiAgICAgICAgICAgIGlmIChlbnRlckJ1dHRvbikge1xuICAgICAgICAgICAgICAgIGlucHV0Q2xhc3NOYW1lID0gY2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSwge1xuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1lbnRlci1idXR0b25gXTogISFlbnRlckJ1dHRvbixcbiAgICAgICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tJHtzaXplfWBdOiAhIXNpemUsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBpbnB1dENsYXNzTmFtZSA9IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuICg8SW5wdXQgb25QcmVzc0VudGVyPXt0aGlzLm9uU2VhcmNofSB7Li4ucmVzdFByb3BzfSBzaXplPXtzaXplfSBwcmVmaXhDbHM9e2lucHV0UHJlZml4Q2xzfSBhZGRvbkFmdGVyPXt0aGlzLnJlbmRlckFkZG9uQWZ0ZXIocHJlZml4Q2xzKX0gc3VmZml4PXt0aGlzLnJlbmRlclN1ZmZpeChwcmVmaXhDbHMpfSBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX0gcmVmPXt0aGlzLnNhdmVJbnB1dH0gY2xhc3NOYW1lPXtpbnB1dENsYXNzTmFtZX0vPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGZvY3VzKCkge1xuICAgICAgICB0aGlzLmlucHV0LmZvY3VzKCk7XG4gICAgfVxuICAgIGJsdXIoKSB7XG4gICAgICAgIHRoaXMuaW5wdXQuYmx1cigpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyU2VhcmNofTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cblNlYXJjaC5kZWZhdWx0UHJvcHMgPSB7XG4gICAgZW50ZXJCdXR0b246IGZhbHNlLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBVkE7QUFDQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBQ0E7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFEQTtBQU1BO0FBQ0E7QUFBQTtBQWxCQTtBQUNBO0FBbUJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBRkE7QUFGQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQURBO0FBS0E7QUFDQTtBQUFBO0FBbkNBO0FBQ0E7QUFvQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaEJBO0FBQ0E7QUE3RkE7QUE4R0E7QUFDQTs7O0FBQUE7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7OztBQXhIQTtBQUNBO0FBREE7QUEwSEE7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/Search.js
