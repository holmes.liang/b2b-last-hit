

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ANT_MARK = void 0;

var React = _interopRequireWildcard(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PropTypes = _interopRequireWildcard(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

var moment = _interopRequireWildcard(__webpack_require__(/*! moment */ "./node_modules/moment/moment.js"));

var _interopDefault = _interopRequireDefault(__webpack_require__(/*! ../_util/interopDefault */ "./node_modules/antd/lib/_util/interopDefault.js"));

var _locale = __webpack_require__(/*! ../modal/locale */ "./node_modules/antd/lib/modal/locale.js");

var _warning = _interopRequireDefault(__webpack_require__(/*! ../_util/warning */ "./node_modules/antd/lib/_util/warning.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var ANT_MARK = 'internalMark';
exports.ANT_MARK = ANT_MARK;

function setMomentLocale(locale) {
  if (locale && locale.locale) {
    (0, _interopDefault["default"])(moment).locale(locale.locale);
  } else {
    (0, _interopDefault["default"])(moment).locale('en');
  }
}

var LocaleProvider =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LocaleProvider, _React$Component);

  function LocaleProvider(props) {
    var _this;

    _classCallCheck(this, LocaleProvider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LocaleProvider).call(this, props));
    setMomentLocale(props.locale);
    (0, _locale.changeConfirmLocale)(props.locale && props.locale.Modal);
    (0, _warning["default"])(props._ANT_MARK__ === ANT_MARK, 'LocaleProvider', '`LocaleProvider` is deprecated. Please use `locale` with `ConfigProvider` instead: http://u.ant.design/locale');
    return _this;
  }

  _createClass(LocaleProvider, [{
    key: "getChildContext",
    value: function getChildContext() {
      return {
        antLocale: _extends(_extends({}, this.props.locale), {
          exist: true
        })
      };
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var locale = this.props.locale;

      if (prevProps.locale !== locale) {
        setMomentLocale(locale);
        (0, _locale.changeConfirmLocale)(locale && locale.Modal);
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      (0, _locale.changeConfirmLocale)();
    }
  }, {
    key: "render",
    value: function render() {
      return this.props.children;
    }
  }]);

  return LocaleProvider;
}(React.Component);

exports["default"] = LocaleProvider;
LocaleProvider.propTypes = {
  locale: PropTypes.object
};
LocaleProvider.defaultProps = {
  locale: {}
};
LocaleProvider.childContextTypes = {
  antLocale: PropTypes.object
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvbG9jYWxlLXByb3ZpZGVyL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9sb2NhbGUtcHJvdmlkZXIvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xuaW1wb3J0IGludGVyb3BEZWZhdWx0IGZyb20gJy4uL191dGlsL2ludGVyb3BEZWZhdWx0JztcbmltcG9ydCB7IGNoYW5nZUNvbmZpcm1Mb2NhbGUgfSBmcm9tICcuLi9tb2RhbC9sb2NhbGUnO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5leHBvcnQgY29uc3QgQU5UX01BUksgPSAnaW50ZXJuYWxNYXJrJztcbmZ1bmN0aW9uIHNldE1vbWVudExvY2FsZShsb2NhbGUpIHtcbiAgICBpZiAobG9jYWxlICYmIGxvY2FsZS5sb2NhbGUpIHtcbiAgICAgICAgaW50ZXJvcERlZmF1bHQobW9tZW50KS5sb2NhbGUobG9jYWxlLmxvY2FsZSk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBpbnRlcm9wRGVmYXVsdChtb21lbnQpLmxvY2FsZSgnZW4nKTtcbiAgICB9XG59XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMb2NhbGVQcm92aWRlciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgICBzZXRNb21lbnRMb2NhbGUocHJvcHMubG9jYWxlKTtcbiAgICAgICAgY2hhbmdlQ29uZmlybUxvY2FsZShwcm9wcy5sb2NhbGUgJiYgcHJvcHMubG9jYWxlLk1vZGFsKTtcbiAgICAgICAgd2FybmluZyhwcm9wcy5fQU5UX01BUktfXyA9PT0gQU5UX01BUkssICdMb2NhbGVQcm92aWRlcicsICdgTG9jYWxlUHJvdmlkZXJgIGlzIGRlcHJlY2F0ZWQuIFBsZWFzZSB1c2UgYGxvY2FsZWAgd2l0aCBgQ29uZmlnUHJvdmlkZXJgIGluc3RlYWQ6IGh0dHA6Ly91LmFudC5kZXNpZ24vbG9jYWxlJyk7XG4gICAgfVxuICAgIGdldENoaWxkQ29udGV4dCgpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGFudExvY2FsZTogT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCB0aGlzLnByb3BzLmxvY2FsZSksIHsgZXhpc3Q6IHRydWUgfSksXG4gICAgICAgIH07XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgICAgY29uc3QgeyBsb2NhbGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChwcmV2UHJvcHMubG9jYWxlICE9PSBsb2NhbGUpIHtcbiAgICAgICAgICAgIHNldE1vbWVudExvY2FsZShsb2NhbGUpO1xuICAgICAgICAgICAgY2hhbmdlQ29uZmlybUxvY2FsZShsb2NhbGUgJiYgbG9jYWxlLk1vZGFsKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgICAgY2hhbmdlQ29uZmlybUxvY2FsZSgpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnByb3BzLmNoaWxkcmVuO1xuICAgIH1cbn1cbkxvY2FsZVByb3ZpZGVyLnByb3BUeXBlcyA9IHtcbiAgICBsb2NhbGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuTG9jYWxlUHJvdmlkZXIuZGVmYXVsdFByb3BzID0ge1xuICAgIGxvY2FsZToge30sXG59O1xuTG9jYWxlUHJvdmlkZXIuY2hpbGRDb250ZXh0VHlwZXMgPSB7XG4gICAgYW50TG9jYWxlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFLQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFHQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUF4QkE7QUFDQTs7QUF5QkE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/lib/locale-provider/index.js
