/**
 * zrender: 生成唯一id
 *
 * @author errorrik (errorrik@gmail.com)
 */
var idStart = 0x0907;

function _default() {
  return idStart++;
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9ndWlkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9ndWlkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogenJlbmRlcjog55Sf5oiQ5ZSv5LiAaWRcbiAqXG4gKiBAYXV0aG9yIGVycm9ycmlrIChlcnJvcnJpa0BnbWFpbC5jb20pXG4gKi9cbnZhciBpZFN0YXJ0ID0gMHgwOTA3O1xuXG5mdW5jdGlvbiBfZGVmYXVsdCgpIHtcbiAgcmV0dXJuIGlkU3RhcnQrKztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/guid.js
