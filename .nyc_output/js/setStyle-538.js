__webpack_require__.r(__webpack_exports__);
/**
 * Easy to set element style, return previou style
 * IE browser compatible(IE browser doesn't merge overflow style, need to set it separately)
 * https://github.com/ant-design/ant-design/issues/19393
 *
 */
function setStyle(style) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var _options$element = options.element,
      element = _options$element === void 0 ? document.body : _options$element;
  var oldStyle = {};
  var styleKeys = Object.keys(style); // IE browser compatible

  styleKeys.forEach(function (key) {
    oldStyle[key] = element.style[key];
  });
  styleKeys.forEach(function (key) {
    element.style[key] = style[key];
  });
  return oldStyle;
}

/* harmony default export */ __webpack_exports__["default"] = (setStyle);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9lcy9zZXRTdHlsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXV0aWwvZXMvc2V0U3R5bGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBFYXN5IHRvIHNldCBlbGVtZW50IHN0eWxlLCByZXR1cm4gcHJldmlvdSBzdHlsZVxuICogSUUgYnJvd3NlciBjb21wYXRpYmxlKElFIGJyb3dzZXIgZG9lc24ndCBtZXJnZSBvdmVyZmxvdyBzdHlsZSwgbmVlZCB0byBzZXQgaXQgc2VwYXJhdGVseSlcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE5MzkzXG4gKlxuICovXG5mdW5jdGlvbiBzZXRTdHlsZShzdHlsZSkge1xuICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG4gIHZhciBfb3B0aW9ucyRlbGVtZW50ID0gb3B0aW9ucy5lbGVtZW50LFxuICAgICAgZWxlbWVudCA9IF9vcHRpb25zJGVsZW1lbnQgPT09IHZvaWQgMCA/IGRvY3VtZW50LmJvZHkgOiBfb3B0aW9ucyRlbGVtZW50O1xuICB2YXIgb2xkU3R5bGUgPSB7fTtcbiAgdmFyIHN0eWxlS2V5cyA9IE9iamVjdC5rZXlzKHN0eWxlKTsgLy8gSUUgYnJvd3NlciBjb21wYXRpYmxlXG5cbiAgc3R5bGVLZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIG9sZFN0eWxlW2tleV0gPSBlbGVtZW50LnN0eWxlW2tleV07XG4gIH0pO1xuICBzdHlsZUtleXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgZWxlbWVudC5zdHlsZVtrZXldID0gc3R5bGVba2V5XTtcbiAgfSk7XG4gIHJldHVybiBvbGRTdHlsZTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgc2V0U3R5bGU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-util/es/setStyle.js
