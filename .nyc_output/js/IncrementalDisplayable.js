var _util = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var inherits = _util.inherits;

var Displayble = __webpack_require__(/*! ./Displayable */ "./node_modules/zrender/lib/graphic/Displayable.js");

var BoundingRect = __webpack_require__(/*! ../core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");
/**
 * Displayable for incremental rendering. It will be rendered in a separate layer
 * IncrementalDisplay have two main methods. `clearDisplayables` and `addDisplayables`
 * addDisplayables will render the added displayables incremetally.
 *
 * It use a not clearFlag to tell the painter don't clear the layer if it's the first element.
 */
// TODO Style override ?


function IncrementalDisplayble(opts) {
  Displayble.call(this, opts);
  this._displayables = [];
  this._temporaryDisplayables = [];
  this._cursor = 0;
  this.notClear = true;
}

IncrementalDisplayble.prototype.incremental = true;

IncrementalDisplayble.prototype.clearDisplaybles = function () {
  this._displayables = [];
  this._temporaryDisplayables = [];
  this._cursor = 0;
  this.dirty();
  this.notClear = false;
};

IncrementalDisplayble.prototype.addDisplayable = function (displayable, notPersistent) {
  if (notPersistent) {
    this._temporaryDisplayables.push(displayable);
  } else {
    this._displayables.push(displayable);
  }

  this.dirty();
};

IncrementalDisplayble.prototype.addDisplayables = function (displayables, notPersistent) {
  notPersistent = notPersistent || false;

  for (var i = 0; i < displayables.length; i++) {
    this.addDisplayable(displayables[i], notPersistent);
  }
};

IncrementalDisplayble.prototype.eachPendingDisplayable = function (cb) {
  for (var i = this._cursor; i < this._displayables.length; i++) {
    cb && cb(this._displayables[i]);
  }

  for (var i = 0; i < this._temporaryDisplayables.length; i++) {
    cb && cb(this._temporaryDisplayables[i]);
  }
};

IncrementalDisplayble.prototype.update = function () {
  this.updateTransform();

  for (var i = this._cursor; i < this._displayables.length; i++) {
    var displayable = this._displayables[i]; // PENDING

    displayable.parent = this;
    displayable.update();
    displayable.parent = null;
  }

  for (var i = 0; i < this._temporaryDisplayables.length; i++) {
    var displayable = this._temporaryDisplayables[i]; // PENDING

    displayable.parent = this;
    displayable.update();
    displayable.parent = null;
  }
};

IncrementalDisplayble.prototype.brush = function (ctx, prevEl) {
  // Render persistant displayables.
  for (var i = this._cursor; i < this._displayables.length; i++) {
    var displayable = this._displayables[i];
    displayable.beforeBrush && displayable.beforeBrush(ctx);
    displayable.brush(ctx, i === this._cursor ? null : this._displayables[i - 1]);
    displayable.afterBrush && displayable.afterBrush(ctx);
  }

  this._cursor = i; // Render temporary displayables.

  for (var i = 0; i < this._temporaryDisplayables.length; i++) {
    var displayable = this._temporaryDisplayables[i];
    displayable.beforeBrush && displayable.beforeBrush(ctx);
    displayable.brush(ctx, i === 0 ? null : this._temporaryDisplayables[i - 1]);
    displayable.afterBrush && displayable.afterBrush(ctx);
  }

  this._temporaryDisplayables = [];
  this.notClear = true;
};

var m = [];

IncrementalDisplayble.prototype.getBoundingRect = function () {
  if (!this._rect) {
    var rect = new BoundingRect(Infinity, Infinity, -Infinity, -Infinity);

    for (var i = 0; i < this._displayables.length; i++) {
      var displayable = this._displayables[i];
      var childRect = displayable.getBoundingRect().clone();

      if (displayable.needLocalTransform()) {
        childRect.applyTransform(displayable.getLocalTransform(m));
      }

      rect.union(childRect);
    }

    this._rect = rect;
  }

  return this._rect;
};

IncrementalDisplayble.prototype.contain = function (x, y) {
  var localPos = this.transformCoordToLocal(x, y);
  var rect = this.getBoundingRect();

  if (rect.contain(localPos[0], localPos[1])) {
    for (var i = 0; i < this._displayables.length; i++) {
      var displayable = this._displayables[i];

      if (displayable.contain(x, y)) {
        return true;
      }
    }
  }

  return false;
};

inherits(IncrementalDisplayble, Displayble);
var _default = IncrementalDisplayble;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9JbmNyZW1lbnRhbERpc3BsYXlhYmxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9JbmNyZW1lbnRhbERpc3BsYXlhYmxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBfdXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBpbmhlcml0cyA9IF91dGlsLmluaGVyaXRzO1xuXG52YXIgRGlzcGxheWJsZSA9IHJlcXVpcmUoXCIuL0Rpc3BsYXlhYmxlXCIpO1xuXG52YXIgQm91bmRpbmdSZWN0ID0gcmVxdWlyZShcIi4uL2NvcmUvQm91bmRpbmdSZWN0XCIpO1xuXG4vKipcbiAqIERpc3BsYXlhYmxlIGZvciBpbmNyZW1lbnRhbCByZW5kZXJpbmcuIEl0IHdpbGwgYmUgcmVuZGVyZWQgaW4gYSBzZXBhcmF0ZSBsYXllclxuICogSW5jcmVtZW50YWxEaXNwbGF5IGhhdmUgdHdvIG1haW4gbWV0aG9kcy4gYGNsZWFyRGlzcGxheWFibGVzYCBhbmQgYGFkZERpc3BsYXlhYmxlc2BcbiAqIGFkZERpc3BsYXlhYmxlcyB3aWxsIHJlbmRlciB0aGUgYWRkZWQgZGlzcGxheWFibGVzIGluY3JlbWV0YWxseS5cbiAqXG4gKiBJdCB1c2UgYSBub3QgY2xlYXJGbGFnIHRvIHRlbGwgdGhlIHBhaW50ZXIgZG9uJ3QgY2xlYXIgdGhlIGxheWVyIGlmIGl0J3MgdGhlIGZpcnN0IGVsZW1lbnQuXG4gKi9cbi8vIFRPRE8gU3R5bGUgb3ZlcnJpZGUgP1xuZnVuY3Rpb24gSW5jcmVtZW50YWxEaXNwbGF5YmxlKG9wdHMpIHtcbiAgRGlzcGxheWJsZS5jYWxsKHRoaXMsIG9wdHMpO1xuICB0aGlzLl9kaXNwbGF5YWJsZXMgPSBbXTtcbiAgdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzID0gW107XG4gIHRoaXMuX2N1cnNvciA9IDA7XG4gIHRoaXMubm90Q2xlYXIgPSB0cnVlO1xufVxuXG5JbmNyZW1lbnRhbERpc3BsYXlibGUucHJvdG90eXBlLmluY3JlbWVudGFsID0gdHJ1ZTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS5jbGVhckRpc3BsYXlibGVzID0gZnVuY3Rpb24gKCkge1xuICB0aGlzLl9kaXNwbGF5YWJsZXMgPSBbXTtcbiAgdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzID0gW107XG4gIHRoaXMuX2N1cnNvciA9IDA7XG4gIHRoaXMuZGlydHkoKTtcbiAgdGhpcy5ub3RDbGVhciA9IGZhbHNlO1xufTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS5hZGREaXNwbGF5YWJsZSA9IGZ1bmN0aW9uIChkaXNwbGF5YWJsZSwgbm90UGVyc2lzdGVudCkge1xuICBpZiAobm90UGVyc2lzdGVudCkge1xuICAgIHRoaXMuX3RlbXBvcmFyeURpc3BsYXlhYmxlcy5wdXNoKGRpc3BsYXlhYmxlKTtcbiAgfSBlbHNlIHtcbiAgICB0aGlzLl9kaXNwbGF5YWJsZXMucHVzaChkaXNwbGF5YWJsZSk7XG4gIH1cblxuICB0aGlzLmRpcnR5KCk7XG59O1xuXG5JbmNyZW1lbnRhbERpc3BsYXlibGUucHJvdG90eXBlLmFkZERpc3BsYXlhYmxlcyA9IGZ1bmN0aW9uIChkaXNwbGF5YWJsZXMsIG5vdFBlcnNpc3RlbnQpIHtcbiAgbm90UGVyc2lzdGVudCA9IG5vdFBlcnNpc3RlbnQgfHwgZmFsc2U7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaXNwbGF5YWJsZXMubGVuZ3RoOyBpKyspIHtcbiAgICB0aGlzLmFkZERpc3BsYXlhYmxlKGRpc3BsYXlhYmxlc1tpXSwgbm90UGVyc2lzdGVudCk7XG4gIH1cbn07XG5cbkluY3JlbWVudGFsRGlzcGxheWJsZS5wcm90b3R5cGUuZWFjaFBlbmRpbmdEaXNwbGF5YWJsZSA9IGZ1bmN0aW9uIChjYikge1xuICBmb3IgKHZhciBpID0gdGhpcy5fY3Vyc29yOyBpIDwgdGhpcy5fZGlzcGxheWFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgY2IgJiYgY2IodGhpcy5fZGlzcGxheWFibGVzW2ldKTtcbiAgfVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgY2IgJiYgY2IodGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzW2ldKTtcbiAgfVxufTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAoKSB7XG4gIHRoaXMudXBkYXRlVHJhbnNmb3JtKCk7XG5cbiAgZm9yICh2YXIgaSA9IHRoaXMuX2N1cnNvcjsgaSA8IHRoaXMuX2Rpc3BsYXlhYmxlcy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBkaXNwbGF5YWJsZSA9IHRoaXMuX2Rpc3BsYXlhYmxlc1tpXTsgLy8gUEVORElOR1xuXG4gICAgZGlzcGxheWFibGUucGFyZW50ID0gdGhpcztcbiAgICBkaXNwbGF5YWJsZS51cGRhdGUoKTtcbiAgICBkaXNwbGF5YWJsZS5wYXJlbnQgPSBudWxsO1xuICB9XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl90ZW1wb3JhcnlEaXNwbGF5YWJsZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgZGlzcGxheWFibGUgPSB0aGlzLl90ZW1wb3JhcnlEaXNwbGF5YWJsZXNbaV07IC8vIFBFTkRJTkdcblxuICAgIGRpc3BsYXlhYmxlLnBhcmVudCA9IHRoaXM7XG4gICAgZGlzcGxheWFibGUudXBkYXRlKCk7XG4gICAgZGlzcGxheWFibGUucGFyZW50ID0gbnVsbDtcbiAgfVxufTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS5icnVzaCA9IGZ1bmN0aW9uIChjdHgsIHByZXZFbCkge1xuICAvLyBSZW5kZXIgcGVyc2lzdGFudCBkaXNwbGF5YWJsZXMuXG4gIGZvciAodmFyIGkgPSB0aGlzLl9jdXJzb3I7IGkgPCB0aGlzLl9kaXNwbGF5YWJsZXMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgZGlzcGxheWFibGUgPSB0aGlzLl9kaXNwbGF5YWJsZXNbaV07XG4gICAgZGlzcGxheWFibGUuYmVmb3JlQnJ1c2ggJiYgZGlzcGxheWFibGUuYmVmb3JlQnJ1c2goY3R4KTtcbiAgICBkaXNwbGF5YWJsZS5icnVzaChjdHgsIGkgPT09IHRoaXMuX2N1cnNvciA/IG51bGwgOiB0aGlzLl9kaXNwbGF5YWJsZXNbaSAtIDFdKTtcbiAgICBkaXNwbGF5YWJsZS5hZnRlckJydXNoICYmIGRpc3BsYXlhYmxlLmFmdGVyQnJ1c2goY3R4KTtcbiAgfVxuXG4gIHRoaXMuX2N1cnNvciA9IGk7IC8vIFJlbmRlciB0ZW1wb3JhcnkgZGlzcGxheWFibGVzLlxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGRpc3BsYXlhYmxlID0gdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzW2ldO1xuICAgIGRpc3BsYXlhYmxlLmJlZm9yZUJydXNoICYmIGRpc3BsYXlhYmxlLmJlZm9yZUJydXNoKGN0eCk7XG4gICAgZGlzcGxheWFibGUuYnJ1c2goY3R4LCBpID09PSAwID8gbnVsbCA6IHRoaXMuX3RlbXBvcmFyeURpc3BsYXlhYmxlc1tpIC0gMV0pO1xuICAgIGRpc3BsYXlhYmxlLmFmdGVyQnJ1c2ggJiYgZGlzcGxheWFibGUuYWZ0ZXJCcnVzaChjdHgpO1xuICB9XG5cbiAgdGhpcy5fdGVtcG9yYXJ5RGlzcGxheWFibGVzID0gW107XG4gIHRoaXMubm90Q2xlYXIgPSB0cnVlO1xufTtcblxudmFyIG0gPSBbXTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS5nZXRCb3VuZGluZ1JlY3QgPSBmdW5jdGlvbiAoKSB7XG4gIGlmICghdGhpcy5fcmVjdCkge1xuICAgIHZhciByZWN0ID0gbmV3IEJvdW5kaW5nUmVjdChJbmZpbml0eSwgSW5maW5pdHksIC1JbmZpbml0eSwgLUluZmluaXR5KTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5fZGlzcGxheWFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGlzcGxheWFibGUgPSB0aGlzLl9kaXNwbGF5YWJsZXNbaV07XG4gICAgICB2YXIgY2hpbGRSZWN0ID0gZGlzcGxheWFibGUuZ2V0Qm91bmRpbmdSZWN0KCkuY2xvbmUoKTtcblxuICAgICAgaWYgKGRpc3BsYXlhYmxlLm5lZWRMb2NhbFRyYW5zZm9ybSgpKSB7XG4gICAgICAgIGNoaWxkUmVjdC5hcHBseVRyYW5zZm9ybShkaXNwbGF5YWJsZS5nZXRMb2NhbFRyYW5zZm9ybShtKSk7XG4gICAgICB9XG5cbiAgICAgIHJlY3QudW5pb24oY2hpbGRSZWN0KTtcbiAgICB9XG5cbiAgICB0aGlzLl9yZWN0ID0gcmVjdDtcbiAgfVxuXG4gIHJldHVybiB0aGlzLl9yZWN0O1xufTtcblxuSW5jcmVtZW50YWxEaXNwbGF5YmxlLnByb3RvdHlwZS5jb250YWluID0gZnVuY3Rpb24gKHgsIHkpIHtcbiAgdmFyIGxvY2FsUG9zID0gdGhpcy50cmFuc2Zvcm1Db29yZFRvTG9jYWwoeCwgeSk7XG4gIHZhciByZWN0ID0gdGhpcy5nZXRCb3VuZGluZ1JlY3QoKTtcblxuICBpZiAocmVjdC5jb250YWluKGxvY2FsUG9zWzBdLCBsb2NhbFBvc1sxXSkpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuX2Rpc3BsYXlhYmxlcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRpc3BsYXlhYmxlID0gdGhpcy5fZGlzcGxheWFibGVzW2ldO1xuXG4gICAgICBpZiAoZGlzcGxheWFibGUuY29udGFpbih4LCB5KSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gZmFsc2U7XG59O1xuXG5pbmhlcml0cyhJbmNyZW1lbnRhbERpc3BsYXlibGUsIERpc3BsYXlibGUpO1xudmFyIF9kZWZhdWx0ID0gSW5jcmVtZW50YWxEaXNwbGF5YmxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/IncrementalDisplayable.js
