__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rc_form_es_createDOMForm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-form/es/createDOMForm */ "./node_modules/rc-form/es/createDOMForm.js");
/* harmony import */ var rc_form_es_createFormField__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-form/es/createFormField */ "./node_modules/rc-form/es/createFormField.js");
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _FormItem__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./FormItem */ "./node_modules/antd/es/form/FormItem.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./constants */ "./node_modules/antd/es/form/constants.js");
/* harmony import */ var _context__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./context */ "./node_modules/antd/es/form/context.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}













var FormLayouts = Object(_util_type__WEBPACK_IMPORTED_MODULE_7__["tuple"])('horizontal', 'inline', 'vertical');

var Form =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Form, _React$Component);

  function Form(props) {
    var _this;

    _classCallCheck(this, Form);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Form).call(this, props));

    _this.renderForm = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          hideRequiredMark = _this$props.hideRequiredMark,
          _this$props$className = _this$props.className,
          className = _this$props$className === void 0 ? '' : _this$props$className,
          layout = _this$props.layout;
      var prefixCls = getPrefixCls('form', customizePrefixCls);
      var formClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-horizontal"), layout === 'horizontal'), _defineProperty(_classNames, "".concat(prefixCls, "-vertical"), layout === 'vertical'), _defineProperty(_classNames, "".concat(prefixCls, "-inline"), layout === 'inline'), _defineProperty(_classNames, "".concat(prefixCls, "-hide-required-mark"), hideRequiredMark), _classNames), className);
      var formProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_5__["default"])(_this.props, ['prefixCls', 'className', 'layout', 'form', 'hideRequiredMark', 'wrapperCol', 'labelAlign', 'labelCol', 'colon']);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("form", _extends({}, formProps, {
        className: formClassName
      }));
    };

    Object(_util_warning__WEBPACK_IMPORTED_MODULE_8__["default"])(!props.form, 'Form', 'It is unnecessary to pass `form` to `Form` after antd@1.7.0.');
    return _this;
  }

  _createClass(Form, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          wrapperCol = _this$props2.wrapperCol,
          labelAlign = _this$props2.labelAlign,
          labelCol = _this$props2.labelCol,
          layout = _this$props2.layout,
          colon = _this$props2.colon;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_context__WEBPACK_IMPORTED_MODULE_11__["default"].Provider, {
        value: {
          wrapperCol: wrapperCol,
          labelAlign: labelAlign,
          labelCol: labelCol,
          vertical: layout === 'vertical',
          colon: colon
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderForm));
    }
  }]);

  return Form;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Form.defaultProps = {
  colon: true,
  layout: 'horizontal',
  hideRequiredMark: false,
  onSubmit: function onSubmit(e) {
    e.preventDefault();
  }
};
Form.propTypes = {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  layout: prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOf"](FormLayouts),
  children: prop_types__WEBPACK_IMPORTED_MODULE_1__["any"],
  onSubmit: prop_types__WEBPACK_IMPORTED_MODULE_1__["func"],
  hideRequiredMark: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"],
  colon: prop_types__WEBPACK_IMPORTED_MODULE_1__["bool"]
};
Form.Item = _FormItem__WEBPACK_IMPORTED_MODULE_9__["default"];
Form.createFormField = rc_form_es_createFormField__WEBPACK_IMPORTED_MODULE_4__["default"];

Form.create = function create() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return Object(rc_form_es_createDOMForm__WEBPACK_IMPORTED_MODULE_3__["default"])(_extends(_extends({
    fieldNameProp: 'id'
  }, options), {
    fieldMetaProp: _constants__WEBPACK_IMPORTED_MODULE_10__["FIELD_META_PROP"],
    fieldDataProp: _constants__WEBPACK_IMPORTED_MODULE_10__["FIELD_DATA_PROP"]
  }));
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9mb3JtL0Zvcm0uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2Zvcm0vRm9ybS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0ICogYXMgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgY3JlYXRlRE9NRm9ybSBmcm9tICdyYy1mb3JtL2xpYi9jcmVhdGVET01Gb3JtJztcbmltcG9ydCBjcmVhdGVGb3JtRmllbGQgZnJvbSAncmMtZm9ybS9saWIvY3JlYXRlRm9ybUZpZWxkJztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHsgdHVwbGUgfSBmcm9tICcuLi9fdXRpbC90eXBlJztcbmltcG9ydCB3YXJuaW5nIGZyb20gJy4uL191dGlsL3dhcm5pbmcnO1xuaW1wb3J0IEZvcm1JdGVtIGZyb20gJy4vRm9ybUl0ZW0nO1xuaW1wb3J0IHsgRklFTERfTUVUQV9QUk9QLCBGSUVMRF9EQVRBX1BST1AgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5pbXBvcnQgRm9ybUNvbnRleHQgZnJvbSAnLi9jb250ZXh0JztcbmNvbnN0IEZvcm1MYXlvdXRzID0gdHVwbGUoJ2hvcml6b250YWwnLCAnaW5saW5lJywgJ3ZlcnRpY2FsJyk7XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBGb3JtIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMucmVuZGVyRm9ybSA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBoaWRlUmVxdWlyZWRNYXJrLCBjbGFzc05hbWUgPSAnJywgbGF5b3V0IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdmb3JtJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGZvcm1DbGFzc05hbWUgPSBjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWhvcml6b250YWxgXTogbGF5b3V0ID09PSAnaG9yaXpvbnRhbCcsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tdmVydGljYWxgXTogbGF5b3V0ID09PSAndmVydGljYWwnLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWlubGluZWBdOiBsYXlvdXQgPT09ICdpbmxpbmUnLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWhpZGUtcmVxdWlyZWQtbWFya2BdOiBoaWRlUmVxdWlyZWRNYXJrLFxuICAgICAgICAgICAgfSwgY2xhc3NOYW1lKTtcbiAgICAgICAgICAgIGNvbnN0IGZvcm1Qcm9wcyA9IG9taXQodGhpcy5wcm9wcywgW1xuICAgICAgICAgICAgICAgICdwcmVmaXhDbHMnLFxuICAgICAgICAgICAgICAgICdjbGFzc05hbWUnLFxuICAgICAgICAgICAgICAgICdsYXlvdXQnLFxuICAgICAgICAgICAgICAgICdmb3JtJyxcbiAgICAgICAgICAgICAgICAnaGlkZVJlcXVpcmVkTWFyaycsXG4gICAgICAgICAgICAgICAgJ3dyYXBwZXJDb2wnLFxuICAgICAgICAgICAgICAgICdsYWJlbEFsaWduJyxcbiAgICAgICAgICAgICAgICAnbGFiZWxDb2wnLFxuICAgICAgICAgICAgICAgICdjb2xvbicsXG4gICAgICAgICAgICBdKTtcbiAgICAgICAgICAgIHJldHVybiA8Zm9ybSB7Li4uZm9ybVByb3BzfSBjbGFzc05hbWU9e2Zvcm1DbGFzc05hbWV9Lz47XG4gICAgICAgIH07XG4gICAgICAgIHdhcm5pbmcoIXByb3BzLmZvcm0sICdGb3JtJywgJ0l0IGlzIHVubmVjZXNzYXJ5IHRvIHBhc3MgYGZvcm1gIHRvIGBGb3JtYCBhZnRlciBhbnRkQDEuNy4wLicpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IHsgd3JhcHBlckNvbCwgbGFiZWxBbGlnbiwgbGFiZWxDb2wsIGxheW91dCwgY29sb24gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiAoPEZvcm1Db250ZXh0LlByb3ZpZGVyIHZhbHVlPXt7IHdyYXBwZXJDb2wsIGxhYmVsQWxpZ24sIGxhYmVsQ29sLCB2ZXJ0aWNhbDogbGF5b3V0ID09PSAndmVydGljYWwnLCBjb2xvbiB9fT5cbiAgICAgICAgPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckZvcm19PC9Db25maWdDb25zdW1lcj5cbiAgICAgIDwvRm9ybUNvbnRleHQuUHJvdmlkZXI+KTtcbiAgICB9XG59XG5Gb3JtLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBjb2xvbjogdHJ1ZSxcbiAgICBsYXlvdXQ6ICdob3Jpem9udGFsJyxcbiAgICBoaWRlUmVxdWlyZWRNYXJrOiBmYWxzZSxcbiAgICBvblN1Ym1pdChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB9LFxufTtcbkZvcm0ucHJvcFR5cGVzID0ge1xuICAgIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBsYXlvdXQ6IFByb3BUeXBlcy5vbmVPZihGb3JtTGF5b3V0cyksXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5hbnksXG4gICAgb25TdWJtaXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhpZGVSZXF1aXJlZE1hcms6IFByb3BUeXBlcy5ib29sLFxuICAgIGNvbG9uOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5Gb3JtLkl0ZW0gPSBGb3JtSXRlbTtcbkZvcm0uY3JlYXRlRm9ybUZpZWxkID0gY3JlYXRlRm9ybUZpZWxkO1xuRm9ybS5jcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUob3B0aW9ucyA9IHt9KSB7XG4gICAgcmV0dXJuIGNyZWF0ZURPTUZvcm0oT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHsgZmllbGROYW1lUHJvcDogJ2lkJyB9LCBvcHRpb25zKSwgeyBmaWVsZE1ldGFQcm9wOiBGSUVMRF9NRVRBX1BST1AsIGZpZWxkRGF0YVByb3A6IEZJRUxEX0RBVEFfUFJPUCB9KSk7XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBTUE7QUFXQTtBQUFBO0FBQUE7QUFwQkE7QUFDQTtBQXFCQTtBQXhCQTtBQXlCQTtBQUNBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7O0FBaENBO0FBQ0E7QUFEQTtBQWtDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/form/Form.js
