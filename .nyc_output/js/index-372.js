__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Avatar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};






var Avatar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Avatar, _React$Component);

  function Avatar() {
    var _this;

    _classCallCheck(this, Avatar);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Avatar).apply(this, arguments));
    _this.state = {
      scale: 1,
      mounted: false,
      isImgExist: true
    };

    _this.setScale = function () {
      if (!_this.avatarChildren || !_this.avatarNode) {
        return;
      }

      var childrenWidth = _this.avatarChildren.offsetWidth; // offsetWidth avoid affecting be transform scale

      var nodeWidth = _this.avatarNode.offsetWidth; // denominator is 0 is no meaning

      if (childrenWidth === 0 || nodeWidth === 0 || _this.lastChildrenWidth === childrenWidth && _this.lastNodeWidth === nodeWidth) {
        return;
      }

      _this.lastChildrenWidth = childrenWidth;
      _this.lastNodeWidth = nodeWidth; // add 4px gap for each side to get better performance

      _this.setState({
        scale: nodeWidth - 8 < childrenWidth ? (nodeWidth - 8) / childrenWidth : 1
      });
    };

    _this.handleImgLoadError = function () {
      var onError = _this.props.onError;
      var errorFlag = onError ? onError() : undefined;

      if (errorFlag !== false) {
        _this.setState({
          isImgExist: false
        });
      }
    };

    _this.renderAvatar = function (_ref) {
      var _classNames, _classNames2;

      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          shape = _a.shape,
          size = _a.size,
          src = _a.src,
          srcSet = _a.srcSet,
          icon = _a.icon,
          className = _a.className,
          alt = _a.alt,
          others = __rest(_a, ["prefixCls", "shape", "size", "src", "srcSet", "icon", "className", "alt"]);

      var _this$state = _this.state,
          isImgExist = _this$state.isImgExist,
          scale = _this$state.scale,
          mounted = _this$state.mounted;
      var prefixCls = getPrefixCls('avatar', customizePrefixCls);
      var sizeCls = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-lg"), size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-sm"), size === 'small'), _classNames));
      var classString = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className, sizeCls, (_classNames2 = {}, _defineProperty(_classNames2, "".concat(prefixCls, "-").concat(shape), shape), _defineProperty(_classNames2, "".concat(prefixCls, "-image"), src && isImgExist), _defineProperty(_classNames2, "".concat(prefixCls, "-icon"), icon), _classNames2));
      var sizeStyle = typeof size === 'number' ? {
        width: size,
        height: size,
        lineHeight: "".concat(size, "px"),
        fontSize: icon ? size / 2 : 18
      } : {};
      var children = _this.props.children;

      if (src && isImgExist) {
        children = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", {
          src: src,
          srcSet: srcSet,
          onError: _this.handleImgLoadError,
          alt: alt
        });
      } else if (icon) {
        if (typeof icon === 'string') {
          children = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_2__["default"], {
            type: icon
          });
        } else {
          children = icon;
        }
      } else {
        var childrenNode = _this.avatarChildren;

        if (childrenNode || scale !== 1) {
          var transformString = "scale(".concat(scale, ") translateX(-50%)");
          var childrenStyle = {
            msTransform: transformString,
            WebkitTransform: transformString,
            transform: transformString
          };
          var sizeChildrenStyle = typeof size === 'number' ? {
            lineHeight: "".concat(size, "px")
          } : {};
          children = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            className: "".concat(prefixCls, "-string"),
            ref: function ref(node) {
              return _this.avatarChildren = node;
            },
            style: _extends(_extends({}, sizeChildrenStyle), childrenStyle)
          }, children);
        } else {
          var _childrenStyle = {};

          if (!mounted) {
            _childrenStyle.opacity = 0;
          }

          children = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            className: "".concat(prefixCls, "-string"),
            style: {
              opacity: 0
            },
            ref: function ref(node) {
              return _this.avatarChildren = node;
            }
          }, children);
        }
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", _extends({}, others, {
        style: _extends(_extends({}, sizeStyle), others.style),
        className: classString,
        ref: function ref(node) {
          return _this.avatarNode = node;
        }
      }), children);
    };

    return _this;
  }

  _createClass(Avatar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setScale();
      this.setState({
        mounted: true
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      this.setScale();

      if (prevProps.src !== this.props.src) {
        this.setState({
          isImgExist: true,
          scale: 1
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"], null, this.renderAvatar);
    }
  }]);

  return Avatar;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Avatar.defaultProps = {
  shape: 'circle',
  size: 'default'
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9hdmF0YXIvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2F2YXRhci9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEF2YXRhciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBzY2FsZTogMSxcbiAgICAgICAgICAgIG1vdW50ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgaXNJbWdFeGlzdDogdHJ1ZSxcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zZXRTY2FsZSA9ICgpID0+IHtcbiAgICAgICAgICAgIGlmICghdGhpcy5hdmF0YXJDaGlsZHJlbiB8fCAhdGhpcy5hdmF0YXJOb2RlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgY2hpbGRyZW5XaWR0aCA9IHRoaXMuYXZhdGFyQ2hpbGRyZW4ub2Zmc2V0V2lkdGg7IC8vIG9mZnNldFdpZHRoIGF2b2lkIGFmZmVjdGluZyBiZSB0cmFuc2Zvcm0gc2NhbGVcbiAgICAgICAgICAgIGNvbnN0IG5vZGVXaWR0aCA9IHRoaXMuYXZhdGFyTm9kZS5vZmZzZXRXaWR0aDtcbiAgICAgICAgICAgIC8vIGRlbm9taW5hdG9yIGlzIDAgaXMgbm8gbWVhbmluZ1xuICAgICAgICAgICAgaWYgKGNoaWxkcmVuV2lkdGggPT09IDAgfHxcbiAgICAgICAgICAgICAgICBub2RlV2lkdGggPT09IDAgfHxcbiAgICAgICAgICAgICAgICAodGhpcy5sYXN0Q2hpbGRyZW5XaWR0aCA9PT0gY2hpbGRyZW5XaWR0aCAmJiB0aGlzLmxhc3ROb2RlV2lkdGggPT09IG5vZGVXaWR0aCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmxhc3RDaGlsZHJlbldpZHRoID0gY2hpbGRyZW5XaWR0aDtcbiAgICAgICAgICAgIHRoaXMubGFzdE5vZGVXaWR0aCA9IG5vZGVXaWR0aDtcbiAgICAgICAgICAgIC8vIGFkZCA0cHggZ2FwIGZvciBlYWNoIHNpZGUgdG8gZ2V0IGJldHRlciBwZXJmb3JtYW5jZVxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgc2NhbGU6IG5vZGVXaWR0aCAtIDggPCBjaGlsZHJlbldpZHRoID8gKG5vZGVXaWR0aCAtIDgpIC8gY2hpbGRyZW5XaWR0aCA6IDEsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVJbWdMb2FkRXJyb3IgPSAoKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uRXJyb3IgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBlcnJvckZsYWcgPSBvbkVycm9yID8gb25FcnJvcigpIDogdW5kZWZpbmVkO1xuICAgICAgICAgICAgaWYgKGVycm9yRmxhZyAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgaXNJbWdFeGlzdDogZmFsc2UgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQXZhdGFyID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IF9hID0gdGhpcy5wcm9wcywgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc2hhcGUsIHNpemUsIHNyYywgc3JjU2V0LCBpY29uLCBjbGFzc05hbWUsIGFsdCB9ID0gX2EsIG90aGVycyA9IF9fcmVzdChfYSwgW1wicHJlZml4Q2xzXCIsIFwic2hhcGVcIiwgXCJzaXplXCIsIFwic3JjXCIsIFwic3JjU2V0XCIsIFwiaWNvblwiLCBcImNsYXNzTmFtZVwiLCBcImFsdFwiXSk7XG4gICAgICAgICAgICBjb25zdCB7IGlzSW1nRXhpc3QsIHNjYWxlLCBtb3VudGVkIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdhdmF0YXInLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgY29uc3Qgc2l6ZUNscyA9IGNsYXNzTmFtZXMoe1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWxnYF06IHNpemUgPT09ICdsYXJnZScsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tc21gXTogc2l6ZSA9PT0gJ3NtYWxsJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc3QgY2xhc3NTdHJpbmcgPSBjbGFzc05hbWVzKHByZWZpeENscywgY2xhc3NOYW1lLCBzaXplQ2xzLCB7XG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tJHtzaGFwZX1gXTogc2hhcGUsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taW1hZ2VgXTogc3JjICYmIGlzSW1nRXhpc3QsXG4gICAgICAgICAgICAgICAgW2Ake3ByZWZpeENsc30taWNvbmBdOiBpY29uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBzaXplU3R5bGUgPSB0eXBlb2Ygc2l6ZSA9PT0gJ251bWJlcidcbiAgICAgICAgICAgICAgICA/IHtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHNpemUsXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogc2l6ZSxcbiAgICAgICAgICAgICAgICAgICAgbGluZUhlaWdodDogYCR7c2l6ZX1weGAsXG4gICAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBpY29uID8gc2l6ZSAvIDIgOiAxOCxcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgOiB7fTtcbiAgICAgICAgICAgIGxldCB7IGNoaWxkcmVuIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgaWYgKHNyYyAmJiBpc0ltZ0V4aXN0KSB7XG4gICAgICAgICAgICAgICAgY2hpbGRyZW4gPSA8aW1nIHNyYz17c3JjfSBzcmNTZXQ9e3NyY1NldH0gb25FcnJvcj17dGhpcy5oYW5kbGVJbWdMb2FkRXJyb3J9IGFsdD17YWx0fS8+O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoaWNvbikge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgaWNvbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRyZW4gPSA8SWNvbiB0eXBlPXtpY29ufS8+O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY2hpbGRyZW4gPSBpY29uO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNoaWxkcmVuTm9kZSA9IHRoaXMuYXZhdGFyQ2hpbGRyZW47XG4gICAgICAgICAgICAgICAgaWYgKGNoaWxkcmVuTm9kZSB8fCBzY2FsZSAhPT0gMSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2Zvcm1TdHJpbmcgPSBgc2NhbGUoJHtzY2FsZX0pIHRyYW5zbGF0ZVgoLTUwJSlgO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBjaGlsZHJlblN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgbXNUcmFuc2Zvcm06IHRyYW5zZm9ybVN0cmluZyxcbiAgICAgICAgICAgICAgICAgICAgICAgIFdlYmtpdFRyYW5zZm9ybTogdHJhbnNmb3JtU3RyaW5nLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm1TdHJpbmcsXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNpemVDaGlsZHJlblN0eWxlID0gdHlwZW9mIHNpemUgPT09ICdudW1iZXInXG4gICAgICAgICAgICAgICAgICAgICAgICA/IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiBgJHtzaXplfXB4YCxcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIDoge307XG4gICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuID0gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zdHJpbmdgfSByZWY9eyhub2RlKSA9PiAodGhpcy5hdmF0YXJDaGlsZHJlbiA9IG5vZGUpfSBzdHlsZT17T2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBzaXplQ2hpbGRyZW5TdHlsZSksIGNoaWxkcmVuU3R5bGUpfT5cbiAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgICA8L3NwYW4+KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNoaWxkcmVuU3R5bGUgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFtb3VudGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGlsZHJlblN0eWxlLm9wYWNpdHkgPSAwO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuID0gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zdHJpbmdgfSBzdHlsZT17eyBvcGFjaXR5OiAwIH19IHJlZj17KG5vZGUpID0+ICh0aGlzLmF2YXRhckNoaWxkcmVuID0gbm9kZSl9PlxuICAgICAgICAgICAge2NoaWxkcmVufVxuICAgICAgICAgIDwvc3Bhbj4pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPHNwYW4gey4uLm90aGVyc30gc3R5bGU9e09iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgc2l6ZVN0eWxlKSwgb3RoZXJzLnN0eWxlKX0gY2xhc3NOYW1lPXtjbGFzc1N0cmluZ30gcmVmPXsobm9kZSkgPT4gKHRoaXMuYXZhdGFyTm9kZSA9IG5vZGUpfT5cbiAgICAgICAge2NoaWxkcmVufVxuICAgICAgPC9zcGFuPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgICB0aGlzLnNldFNjYWxlKCk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb3VudGVkOiB0cnVlIH0pO1xuICAgIH1cbiAgICBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICAgIHRoaXMuc2V0U2NhbGUoKTtcbiAgICAgICAgaWYgKHByZXZQcm9wcy5zcmMgIT09IHRoaXMucHJvcHMuc3JjKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgaXNJbWdFeGlzdDogdHJ1ZSwgc2NhbGU6IDEgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlckF2YXRhcn08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5BdmF0YXIuZGVmYXVsdFByb3BzID0ge1xuICAgIHNoYXBlOiAnY2lyY2xlJyxcbiAgICBzaXplOiAnZGVmYXVsdCcsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFmQTtBQUNBO0FBa0JBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUlBO0FBS0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBZEE7QUFDQTtBQXFCQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUlBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBRUE7QUFEQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWkE7QUFpQkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE3REE7QUFDQTtBQWxDQTtBQWtHQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFoSEE7QUFDQTtBQURBO0FBa0hBO0FBQ0E7QUFDQTtBQUZBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/avatar/index.js
