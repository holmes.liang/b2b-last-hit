__webpack_require__.r(__webpack_exports__);
function loopFiles(item, callback) {
  var dirReader = item.createReader();
  var fileList = [];

  function sequence() {
    dirReader.readEntries(function (entries) {
      var entryList = Array.prototype.slice.apply(entries);
      fileList = fileList.concat(entryList); // Check if all the file has been viewed

      var isFinished = !entryList.length;

      if (isFinished) {
        callback(fileList);
      } else {
        sequence();
      }
    });
  }

  sequence();
}

var traverseFileTree = function traverseFileTree(files, callback, isAccepted) {
  var _traverseFileTree = function _traverseFileTree(item, path) {
    path = path || '';

    if (item.isFile) {
      item.file(function (file) {
        if (isAccepted(file)) {
          // https://github.com/ant-design/ant-design/issues/16426
          if (item.fullPath && !file.webkitRelativePath) {
            Object.defineProperties(file, {
              webkitRelativePath: {
                writable: true
              }
            });
            file.webkitRelativePath = item.fullPath.replace(/^\//, '');
            Object.defineProperties(file, {
              webkitRelativePath: {
                writable: false
              }
            });
          }

          callback([file]);
        }
      });
    } else if (item.isDirectory) {
      loopFiles(item, function (entries) {
        entries.forEach(function (entryItem) {
          _traverseFileTree(entryItem, '' + path + item.name + '/');
        });
      });
    }
  };

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = files[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var file = _step.value;

      _traverseFileTree(file.webkitGetAsEntry());
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator['return']) {
        _iterator['return']();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
};

/* harmony default export */ __webpack_exports__["default"] = (traverseFileTree);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXBsb2FkL2VzL3RyYXZlcnNlRmlsZVRyZWUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11cGxvYWQvZXMvdHJhdmVyc2VGaWxlVHJlZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBsb29wRmlsZXMoaXRlbSwgY2FsbGJhY2spIHtcbiAgdmFyIGRpclJlYWRlciA9IGl0ZW0uY3JlYXRlUmVhZGVyKCk7XG4gIHZhciBmaWxlTGlzdCA9IFtdO1xuXG4gIGZ1bmN0aW9uIHNlcXVlbmNlKCkge1xuICAgIGRpclJlYWRlci5yZWFkRW50cmllcyhmdW5jdGlvbiAoZW50cmllcykge1xuICAgICAgdmFyIGVudHJ5TGlzdCA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5hcHBseShlbnRyaWVzKTtcbiAgICAgIGZpbGVMaXN0ID0gZmlsZUxpc3QuY29uY2F0KGVudHJ5TGlzdCk7XG5cbiAgICAgIC8vIENoZWNrIGlmIGFsbCB0aGUgZmlsZSBoYXMgYmVlbiB2aWV3ZWRcbiAgICAgIHZhciBpc0ZpbmlzaGVkID0gIWVudHJ5TGlzdC5sZW5ndGg7XG5cbiAgICAgIGlmIChpc0ZpbmlzaGVkKSB7XG4gICAgICAgIGNhbGxiYWNrKGZpbGVMaXN0KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlcXVlbmNlKCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBzZXF1ZW5jZSgpO1xufVxuXG52YXIgdHJhdmVyc2VGaWxlVHJlZSA9IGZ1bmN0aW9uIHRyYXZlcnNlRmlsZVRyZWUoZmlsZXMsIGNhbGxiYWNrLCBpc0FjY2VwdGVkKSB7XG4gIHZhciBfdHJhdmVyc2VGaWxlVHJlZSA9IGZ1bmN0aW9uIF90cmF2ZXJzZUZpbGVUcmVlKGl0ZW0sIHBhdGgpIHtcbiAgICBwYXRoID0gcGF0aCB8fCAnJztcbiAgICBpZiAoaXRlbS5pc0ZpbGUpIHtcbiAgICAgIGl0ZW0uZmlsZShmdW5jdGlvbiAoZmlsZSkge1xuICAgICAgICBpZiAoaXNBY2NlcHRlZChmaWxlKSkge1xuICAgICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE2NDI2XG4gICAgICAgICAgaWYgKGl0ZW0uZnVsbFBhdGggJiYgIWZpbGUud2Via2l0UmVsYXRpdmVQYXRoKSB7XG4gICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyhmaWxlLCB7XG4gICAgICAgICAgICAgIHdlYmtpdFJlbGF0aXZlUGF0aDoge1xuICAgICAgICAgICAgICAgIHdyaXRhYmxlOiB0cnVlXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgZmlsZS53ZWJraXRSZWxhdGl2ZVBhdGggPSBpdGVtLmZ1bGxQYXRoLnJlcGxhY2UoL15cXC8vLCAnJyk7XG4gICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyhmaWxlLCB7XG4gICAgICAgICAgICAgIHdlYmtpdFJlbGF0aXZlUGF0aDoge1xuICAgICAgICAgICAgICAgIHdyaXRhYmxlOiBmYWxzZVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgY2FsbGJhY2soW2ZpbGVdKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSBlbHNlIGlmIChpdGVtLmlzRGlyZWN0b3J5KSB7XG4gICAgICBsb29wRmlsZXMoaXRlbSwgZnVuY3Rpb24gKGVudHJpZXMpIHtcbiAgICAgICAgZW50cmllcy5mb3JFYWNoKGZ1bmN0aW9uIChlbnRyeUl0ZW0pIHtcbiAgICAgICAgICBfdHJhdmVyc2VGaWxlVHJlZShlbnRyeUl0ZW0sICcnICsgcGF0aCArIGl0ZW0ubmFtZSArICcvJyk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9O1xuICB2YXIgX2l0ZXJhdG9yTm9ybWFsQ29tcGxldGlvbiA9IHRydWU7XG4gIHZhciBfZGlkSXRlcmF0b3JFcnJvciA9IGZhbHNlO1xuICB2YXIgX2l0ZXJhdG9yRXJyb3IgPSB1bmRlZmluZWQ7XG5cbiAgdHJ5IHtcbiAgICBmb3IgKHZhciBfaXRlcmF0b3IgPSBmaWxlc1tTeW1ib2wuaXRlcmF0b3JdKCksIF9zdGVwOyAhKF9pdGVyYXRvck5vcm1hbENvbXBsZXRpb24gPSAoX3N0ZXAgPSBfaXRlcmF0b3IubmV4dCgpKS5kb25lKTsgX2l0ZXJhdG9yTm9ybWFsQ29tcGxldGlvbiA9IHRydWUpIHtcbiAgICAgIHZhciBmaWxlID0gX3N0ZXAudmFsdWU7XG5cbiAgICAgIF90cmF2ZXJzZUZpbGVUcmVlKGZpbGUud2Via2l0R2V0QXNFbnRyeSgpKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIF9kaWRJdGVyYXRvckVycm9yID0gdHJ1ZTtcbiAgICBfaXRlcmF0b3JFcnJvciA9IGVycjtcbiAgfSBmaW5hbGx5IHtcbiAgICB0cnkge1xuICAgICAgaWYgKCFfaXRlcmF0b3JOb3JtYWxDb21wbGV0aW9uICYmIF9pdGVyYXRvclsncmV0dXJuJ10pIHtcbiAgICAgICAgX2l0ZXJhdG9yWydyZXR1cm4nXSgpO1xuICAgICAgfVxuICAgIH0gZmluYWxseSB7XG4gICAgICBpZiAoX2RpZEl0ZXJhdG9yRXJyb3IpIHtcbiAgICAgICAgdGhyb3cgX2l0ZXJhdG9yRXJyb3I7XG4gICAgICB9XG4gICAgfVxuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cmF2ZXJzZUZpbGVUcmVlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-upload/es/traverseFileTree.js
