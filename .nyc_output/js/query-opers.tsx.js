__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderOpers", function() { return renderOpers; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common_route_common_route__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/route/common-route */ "./src/common/route/common-route.tsx");
/* harmony import */ var _claims_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../claims/common */ "./src/app/desk/claims/common.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/query/components/query-opers.tsx";






function renderOpers(item, router, handleRejectPolicy, _this) {
  var _item$opers = item.opers,
      editable = _item$opers.editable,
      rejectable = _item$opers.rejectable,
      viewable = _item$opers.viewable,
      allowToCancelFromInception = _item$opers.allowToCancelFromInception,
      allowToNFnEndo = _item$opers.allowToNFnEndo,
      allowToMemerMovementEndo = _item$opers.allowToMemerMovementEndo,
      allowToPoiMoveEndo = _item$opers.allowToPoiMoveEndo,
      allowToPoiShorteningOrExtensionEndo = _item$opers.allowToPoiShorteningOrExtensionEndo,
      allowToCoverageChangeEndo = _item$opers.allowToCoverageChangeEndo,
      allowToChangeOfAgentEndo = _item$opers.allowToChangeOfAgentEndo,
      allowToBasicInfoEndo = _item$opers.allowToBasicInfoEndo,
      allowToCessionAdjustmentEndo = _item$opers.allowToCessionAdjustmentEndo,
      allowToViewRIDetails = _item$opers.allowToViewRIDetails,
      allowToReportClaim = _item$opers.allowToReportClaim,
      allowToClone = _item$opers.allowToClone;
  var result = [];
  var moreOper = [];

  if (viewable) {
    result.push(renderViewBtn(item, router));
  }

  if (editable) {
    result.push(renderEditBtn(item, router));
  }

  if (allowToCancelFromInception) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "cancellation",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 31
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        router.pushRedirect(_common__WEBPACK_IMPORTED_MODULE_2__["PATH"].ENDORSEMENT_ENTRY.replace(":policyId", item.refId).replace(":endoType", _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.INCEPTION_CANCELLATION.toLowerCase()).replace(":itntCode", item.itntCode));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Cancel").thai("Cancel").getMessage())));
  }

  if (allowToNFnEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "non-financial",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        router.pushRedirect(_common__WEBPACK_IMPORTED_MODULE_2__["PATH"].ENDORSEMENT_ENTRY.replace(":policyId", item.refId).replace(":endoType", _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.NFN.toLowerCase()).replace(":itntCode", item.itntCode));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Non-financial Endorsement").thai("สลักหลัง").getMessage())));
  }

  if (allowToMemerMovementEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "member-movement",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        var endoTypes = {
          GTL: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_GHS.toLowerCase(),
          GPA: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_GHS.toLowerCase(),
          GHS: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_GHS.toLowerCase(),
          BOSS: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT.toLowerCase(),
          FWB: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_SURETY.toLowerCase(),
          EFC: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_EFC.toLowerCase()
        };
        if (!endoTypes[item.productCode]) return;
        router.pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_5__["endoEntryPath"])({
          itntCode: item.itntCode,
          productCode: item.productCode,
          productVersion: item.productVersion,
          policyId: item.refId,
          endoId: "none",
          endoType: endoTypes[item.productCode],
          isTypesGet: false
        }));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Member Movement Endorsement").thai("Member Movement Endorsement").getMessage())));
  }

  if (allowToBasicInfoEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "basic-info",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 114
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        router.pushRedirect(_common__WEBPACK_IMPORTED_MODULE_2__["PATH"].BASIC_INFO_ENDO.replace(":policyId", item.refId).replace(":itntCode", item.itntCode).replace(":productVersion", item.productVersion).replace(":productCode", item.productCode).replace("/:endoId?", ""));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 115
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Basic Info Endorsement").thai("Basic Info Endorsement").getMessage())));
  } // for endos


  if (allowToPoiMoveEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToPoiMoveEndo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 139
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 140
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("POI Move (WIP)").thai("POI Move (WIP)").getMessage())));
  }

  if (allowToPoiShorteningOrExtensionEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToPoiShorteningOrExtensionEndo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 151
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 152
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("POI Shortening/Extension (WIP)").thai("POI Shortening/Extension (WIP)").getMessage())));
  }

  if (allowToCoverageChangeEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToCoverageChangeEndo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 163
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Changes to Coverage (WIP)").thai("Changes to Coverage (WIP)").getMessage())));
  }

  if (allowToChangeOfAgentEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToChangeOfAgentEndo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 175
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 176
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Change of Agent (WIP)").thai("Change of Agent (WIP)").getMessage())));
  }

  if (rejectable) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "rejectable",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 187
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        return handleRejectPolicy(item);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 188
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Reject").thai("ยกเลิกกรมธรรม์").getMessage())));
  }

  if (allowToCessionAdjustmentEndo) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToCessionAdjustmentEndo",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 199
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        router.pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_5__["endoEntryPath"])({
          itntCode: item.itntCode,
          productCode: item.productCode,
          productVersion: item.productVersion,
          policyId: item.refId,
          endoId: "none",
          endoType: _common__WEBPACK_IMPORTED_MODULE_2__["Consts"].ENDO_TYPES.CESSION_ADJ,
          isTypesGet: false,
          bizType: item.bizType
        }));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 200
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Cession Adjustment").thai("Cession Adjustment").getMessage())));
  }

  if (allowToViewRIDetails) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "viewRIDetails",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 224
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        router.pushRedirect(_common__WEBPACK_IMPORTED_MODULE_2__["PATH"].RI_ENQUIRY_VIEW.replace(":policyId", item.refId));
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 225
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("View RI Details").thai("View RI Details").getMessage())));
  }

  if (allowToReportClaim) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "claimNotification",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 239
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        _claims_common__WEBPACK_IMPORTED_MODULE_6__["default"].getLossItems(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {}, {
          policyId: item.refId
        }), _this);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 240
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Claim Notification").thai("Claim Notification").getMessage())));
  }

  if (allowToClone) {
    moreOper.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"].Item, {
      key: "allowToClone",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 252
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "button",
      onClick: function onClick() {
        var productCode = item.productCode,
            refId = item.refId,
            bizType = item.bizType,
            itntCode = item.itntCode,
            productVersion = item.productVersion;

        var _url = _common__WEBPACK_IMPORTED_MODULE_2__["PATH"].NORMAL_QUOTE.replace(":itntCode", itntCode).replace(":prdtCode", productCode).replace("/:policyId?", "?clonePolicyId=" + refId);

        router.pushRedirect(_url);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 253
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Clone").thai("Clone").getMessage())));
  }

  var menu = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Menu"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 271
    },
    __self: this
  }, moreOper);

  if (moreOper.length > 0) {
    result.push(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Dropdown"], {
      overlay: menu,
      key: "dropdown",
      trigger: ["hover", "click"],
      __source: {
        fileName: _jsxFileName,
        lineNumber: 274
      },
      __self: this
    }, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
      className: "ant-dropdown-link button",
      href: "#",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 275
      },
      __self: this
    }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("More").thai("เรียกดู").getMessage(), " ", _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Icon"], {
      type: "down",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 279
      },
      __self: this
    }))));
  }

  if (!result.length) {
    result.push("-");
  }

  return _common__WEBPACK_IMPORTED_MODULE_2__["Utils"].joinElements(result, _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Divider"], {
    type: "vertical",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 289
    },
    __self: this
  }));
}

function renderViewBtn(item, router) {
  var productCode = item.productCode,
      refId = item.refId,
      bizType = item.bizType,
      itntCode = item.itntCode,
      productVersion = item.productVersion;
  return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
    key: "viewable",
    className: "button",
    onClick: function onClick() {
      router.pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_5__["policyPath"])({
        type: "view",
        productCode: productCode,
        policyId: refId,
        bizType: bizType,
        itntCode: itntCode,
        productVersion: productVersion
      }));
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 301
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("View").thai("ดูรายละเอียด").getMessage());
}

function renderEditBtn(item, router) {
  var productCode = item.productCode,
      productCate = item.productCate,
      bizType = item.bizType,
      itntCode = item.itntCode,
      productVersion = item.productVersion,
      refId = item.refId;
  var code = !lodash__WEBPACK_IMPORTED_MODULE_1___default.a.isEmpty(productCode) ? productCode : productCate;
  return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement("a", {
    key: "editable",
    className: "button",
    onClick: function onClick() {
      //TODO need compatible multi insurer tenant
      router.pushRedirect(Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_5__["policyPath"])({
        productCode: productCode,
        type: "work",
        bizType: bizType,
        itntCode: itntCode,
        productVersion: productVersion,
        policyId: refId
      }));
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 331
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Work On").thai("แก้ไข").getMessage());
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVlcnkvY29tcG9uZW50cy9xdWVyeS1vcGVycy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9xdW90ZS9xdWVyeS9jb21wb25lbnRzL3F1ZXJ5LW9wZXJzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBDb25zdHMsIExhbmd1YWdlLCBQQVRILCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBEaXZpZGVyLCBEcm9wZG93biwgSWNvbiwgTWVudSB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgZW5kb0VudHJ5UGF0aCwgcG9saWN5UGF0aCB9IGZyb20gXCJAY29tbW9uL3JvdXRlL2NvbW1vbi1yb3V0ZVwiO1xuaW1wb3J0IENsYWltc0NvbW1vbiBmcm9tIFwiLi4vLi4vLi4vY2xhaW1zL2NvbW1vblwiO1xuXG5leHBvcnQgZnVuY3Rpb24gcmVuZGVyT3BlcnMoaXRlbTogYW55LCByb3V0ZXI6IGFueSwgaGFuZGxlUmVqZWN0UG9saWN5OiBGdW5jdGlvbiwgX3RoaXM6IGFueSkge1xuICBjb25zdCB7XG4gICAgb3BlcnM6IHtcbiAgICAgIGVkaXRhYmxlLCByZWplY3RhYmxlLCB2aWV3YWJsZSwgYWxsb3dUb0NhbmNlbEZyb21JbmNlcHRpb24sIGFsbG93VG9ORm5FbmRvLCBhbGxvd1RvTWVtZXJNb3ZlbWVudEVuZG8sXG4gICAgICBhbGxvd1RvUG9pTW92ZUVuZG8sIGFsbG93VG9Qb2lTaG9ydGVuaW5nT3JFeHRlbnNpb25FbmRvLCBhbGxvd1RvQ292ZXJhZ2VDaGFuZ2VFbmRvLCBhbGxvd1RvQ2hhbmdlT2ZBZ2VudEVuZG8sXG4gICAgICBhbGxvd1RvQmFzaWNJbmZvRW5kbywgYWxsb3dUb0Nlc3Npb25BZGp1c3RtZW50RW5kbywgYWxsb3dUb1ZpZXdSSURldGFpbHMsIGFsbG93VG9SZXBvcnRDbGFpbSwgYWxsb3dUb0Nsb25lLFxuICAgIH0sXG4gIH0gPSBpdGVtO1xuICBjb25zdCByZXN1bHQgPSBbXTtcbiAgY29uc3QgbW9yZU9wZXIgPSBbXTtcbiAgaWYgKHZpZXdhYmxlKSB7XG4gICAgcmVzdWx0LnB1c2goXG4gICAgICByZW5kZXJWaWV3QnRuKGl0ZW0sIHJvdXRlciksXG4gICAgKTtcbiAgfVxuICBpZiAoZWRpdGFibGUpIHtcbiAgICByZXN1bHQucHVzaChcbiAgICAgIHJlbmRlckVkaXRCdG4oaXRlbSwgcm91dGVyKSxcbiAgICApO1xuICB9XG5cbiAgaWYgKGFsbG93VG9DYW5jZWxGcm9tSW5jZXB0aW9uKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiY2FuY2VsbGF0aW9uXCI+XG4gICAgICAgIDxhXG4gICAgICAgICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICByb3V0ZXJcbiAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChcbiAgICAgICAgICAgICAgICBQQVRILkVORE9SU0VNRU5UX0VOVFJZLnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgaXRlbS5yZWZJZCkucmVwbGFjZShcbiAgICAgICAgICAgICAgICAgIFwiOmVuZG9UeXBlXCIsXG4gICAgICAgICAgICAgICAgICBDb25zdHMuRU5ET19UWVBFUy5JTkNFUFRJT05fQ0FOQ0VMTEFUSU9OLnRvTG93ZXJDYXNlKCksXG4gICAgICAgICAgICAgICAgKS5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0ZW0uaXRudENvZGUpLFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDYW5jZWxcIilcbiAgICAgICAgICAgIC50aGFpKFwiQ2FuY2VsXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuXG4gIGlmIChhbGxvd1RvTkZuRW5kbykge1xuICAgIG1vcmVPcGVyLnB1c2goXG4gICAgICA8TWVudS5JdGVtIGtleT1cIm5vbi1maW5hbmNpYWxcIj5cbiAgICAgICAgPGFcbiAgICAgICAgICBjbGFzc05hbWU9XCJidXR0b25cIlxuICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgIHJvdXRlclxuICAgICAgICAgICAgICAucHVzaFJlZGlyZWN0KFxuICAgICAgICAgICAgICAgIFBBVEguRU5ET1JTRU1FTlRfRU5UUllcbiAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOnBvbGljeUlkXCIsIGl0ZW0ucmVmSWQpXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZShcIjplbmRvVHlwZVwiLCBDb25zdHMuRU5ET19UWVBFUy5ORk4udG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICAgICAgICAgICkucmVwbGFjZShcIjppdG50Q29kZVwiLCBpdGVtLml0bnRDb2RlKSxcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiTm9uLWZpbmFuY2lhbCBFbmRvcnNlbWVudFwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguKrguKXguLHguIHguKvguKXguLHguIdcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvYT5cbiAgICAgIDwvTWVudS5JdGVtPixcbiAgICApO1xuICB9XG5cbiAgaWYgKGFsbG93VG9NZW1lck1vdmVtZW50RW5kbykge1xuICAgIG1vcmVPcGVyLnB1c2goXG4gICAgICA8TWVudS5JdGVtIGtleT1cIm1lbWJlci1tb3ZlbWVudFwiPlxuICAgICAgICA8YVxuICAgICAgICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgbGV0IGVuZG9UeXBlczogYW55ID0ge1xuICAgICAgICAgICAgICBHVEw6IENvbnN0cy5FTkRPX1RZUEVTLk1FTUJFUl9NT1ZFTUVOVF9HSFMudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICAgICAgR1BBOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRfR0hTLnRvTG93ZXJDYXNlKCksXG4gICAgICAgICAgICAgIEdIUzogQ29uc3RzLkVORE9fVFlQRVMuTUVNQkVSX01PVkVNRU5UX0dIUy50b0xvd2VyQ2FzZSgpLFxuICAgICAgICAgICAgICBCT1NTOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlQudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICAgICAgRldCOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRfU1VSRVRZLnRvTG93ZXJDYXNlKCksXG4gICAgICAgICAgICAgIEVGQzogQ29uc3RzLkVORE9fVFlQRVMuTUVNQkVSX01PVkVNRU5UX0VGQy50b0xvd2VyQ2FzZSgpLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmICghZW5kb1R5cGVzW2l0ZW0ucHJvZHVjdENvZGVdKSByZXR1cm47XG4gICAgICAgICAgICByb3V0ZXJcbiAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChcbiAgICAgICAgICAgICAgICBlbmRvRW50cnlQYXRoKHtcbiAgICAgICAgICAgICAgICAgIGl0bnRDb2RlOiBpdGVtLml0bnRDb2RlLFxuICAgICAgICAgICAgICAgICAgcHJvZHVjdENvZGU6IGl0ZW0ucHJvZHVjdENvZGUsXG4gICAgICAgICAgICAgICAgICBwcm9kdWN0VmVyc2lvbjogaXRlbS5wcm9kdWN0VmVyc2lvbixcbiAgICAgICAgICAgICAgICAgIHBvbGljeUlkOiBpdGVtLnJlZklkLFxuICAgICAgICAgICAgICAgICAgZW5kb0lkOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICAgIGVuZG9UeXBlOiBlbmRvVHlwZXNbaXRlbS5wcm9kdWN0Q29kZV0sXG4gICAgICAgICAgICAgICAgICBpc1R5cGVzR2V0OiBmYWxzZSxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiTWVtYmVyIE1vdmVtZW50IEVuZG9yc2VtZW50XCIpXG4gICAgICAgICAgICAudGhhaShcIk1lbWJlciBNb3ZlbWVudCBFbmRvcnNlbWVudFwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9hPlxuICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICk7XG4gIH1cblxuICBpZiAoYWxsb3dUb0Jhc2ljSW5mb0VuZG8pIHtcbiAgICBtb3JlT3Blci5wdXNoKFxuICAgICAgPE1lbnUuSXRlbSBrZXk9XCJiYXNpYy1pbmZvXCI+XG4gICAgICAgIDxhXG4gICAgICAgICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICByb3V0ZXJcbiAgICAgICAgICAgICAgLnB1c2hSZWRpcmVjdChcbiAgICAgICAgICAgICAgICBQQVRILkJBU0lDX0lORk9fRU5ET1xuICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgaXRlbS5yZWZJZClcbiAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0ZW0uaXRudENvZGUpXG4gICAgICAgICAgICAgICAgICAucmVwbGFjZShcIjpwcm9kdWN0VmVyc2lvblwiLCBpdGVtLnByb2R1Y3RWZXJzaW9uKVxuICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdENvZGVcIiwgaXRlbS5wcm9kdWN0Q29kZSlcbiAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiLzplbmRvSWQ/XCIsIFwiXCIpLFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJCYXNpYyBJbmZvIEVuZG9yc2VtZW50XCIpXG4gICAgICAgICAgICAudGhhaShcIkJhc2ljIEluZm8gRW5kb3JzZW1lbnRcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvYT5cbiAgICAgIDwvTWVudS5JdGVtPixcbiAgICApO1xuICB9XG4gIC8vIGZvciBlbmRvc1xuICBpZiAoYWxsb3dUb1BvaU1vdmVFbmRvKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb1BvaU1vdmVFbmRvXCI+XG4gICAgICAgIDxhXG4gICAgICAgICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICAgICAgPlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlBPSSBNb3ZlIChXSVApXCIpLnRoYWkoXCJQT0kgTW92ZSAoV0lQKVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvYT5cbiAgICAgIDwvTWVudS5JdGVtPixcbiAgICApO1xuICB9XG5cbiAgaWYgKGFsbG93VG9Qb2lTaG9ydGVuaW5nT3JFeHRlbnNpb25FbmRvKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb1BvaVNob3J0ZW5pbmdPckV4dGVuc2lvbkVuZG9cIj5cbiAgICAgICAgPGFcbiAgICAgICAgICBjbGFzc05hbWU9XCJidXR0b25cIlxuICAgICAgICA+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiUE9JIFNob3J0ZW5pbmcvRXh0ZW5zaW9uIChXSVApXCIpLnRoYWkoXCJQT0kgU2hvcnRlbmluZy9FeHRlbnNpb24gKFdJUClcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuXG4gIGlmIChhbGxvd1RvQ292ZXJhZ2VDaGFuZ2VFbmRvKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb0NvdmVyYWdlQ2hhbmdlRW5kb1wiPlxuICAgICAgICA8YVxuICAgICAgICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgICAgID5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDaGFuZ2VzIHRvIENvdmVyYWdlIChXSVApXCIpLnRoYWkoXCJDaGFuZ2VzIHRvIENvdmVyYWdlIChXSVApXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9hPlxuICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICk7XG4gIH1cblxuICBpZiAoYWxsb3dUb0NoYW5nZU9mQWdlbnRFbmRvKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb0NoYW5nZU9mQWdlbnRFbmRvXCI+XG4gICAgICAgIDxhXG4gICAgICAgICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICAgICAgPlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIkNoYW5nZSBvZiBBZ2VudCAoV0lQKVwiKS50aGFpKFwiQ2hhbmdlIG9mIEFnZW50IChXSVApXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9hPlxuICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICk7XG4gIH1cblxuICBpZiAocmVqZWN0YWJsZSkge1xuICAgIG1vcmVPcGVyLnB1c2goXG4gICAgICA8TWVudS5JdGVtIGtleT1cInJlamVjdGFibGVcIj5cbiAgICAgICAgPGEgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17KCkgPT4gaGFuZGxlUmVqZWN0UG9saWN5KGl0ZW0pfT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJSZWplY3RcIilcbiAgICAgICAgICAgIC50aGFpKFwi4Lii4LiB4LmA4Lil4Li04LiB4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmMXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuXG4gIGlmIChhbGxvd1RvQ2Vzc2lvbkFkanVzdG1lbnRFbmRvKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb0Nlc3Npb25BZGp1c3RtZW50RW5kb1wiPlxuICAgICAgICA8YSBjbGFzc05hbWU9XCJidXR0b25cIiBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgcm91dGVyXG4gICAgICAgICAgICAucHVzaFJlZGlyZWN0KFxuICAgICAgICAgICAgICBlbmRvRW50cnlQYXRoKHtcbiAgICAgICAgICAgICAgICBpdG50Q29kZTogaXRlbS5pdG50Q29kZSxcbiAgICAgICAgICAgICAgICBwcm9kdWN0Q29kZTogaXRlbS5wcm9kdWN0Q29kZSxcbiAgICAgICAgICAgICAgICBwcm9kdWN0VmVyc2lvbjogaXRlbS5wcm9kdWN0VmVyc2lvbixcbiAgICAgICAgICAgICAgICBwb2xpY3lJZDogaXRlbS5yZWZJZCxcbiAgICAgICAgICAgICAgICBlbmRvSWQ6IFwibm9uZVwiLFxuICAgICAgICAgICAgICAgIGVuZG9UeXBlOiBDb25zdHMuRU5ET19UWVBFUy5DRVNTSU9OX0FESixcbiAgICAgICAgICAgICAgICBpc1R5cGVzR2V0OiBmYWxzZSxcbiAgICAgICAgICAgICAgICBiaXpUeXBlOiBpdGVtLmJpelR5cGUsXG4gICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgfX0+XG4gICAgICAgICAge0xhbmd1YWdlLmVuKFwiQ2Vzc2lvbiBBZGp1c3RtZW50XCIpXG4gICAgICAgICAgICAudGhhaShcIkNlc3Npb24gQWRqdXN0bWVudFwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9hPlxuICAgICAgPC9NZW51Lkl0ZW0+LFxuICAgICk7XG4gIH1cbiAgaWYgKGFsbG93VG9WaWV3UklEZXRhaWxzKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwidmlld1JJRGV0YWlsc1wiPlxuICAgICAgICA8YSBjbGFzc05hbWU9XCJidXR0b25cIiBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgcm91dGVyXG4gICAgICAgICAgICAucHVzaFJlZGlyZWN0KFBBVEguUklfRU5RVUlSWV9WSUVXLnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgaXRlbS5yZWZJZCksXG4gICAgICAgICAgICApO1xuICAgICAgICB9fT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJWaWV3IFJJIERldGFpbHNcIilcbiAgICAgICAgICAgIC50aGFpKFwiVmlldyBSSSBEZXRhaWxzXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuICBpZiAoYWxsb3dUb1JlcG9ydENsYWltKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiY2xhaW1Ob3RpZmljYXRpb25cIj5cbiAgICAgICAgPGEgY2xhc3NOYW1lPVwiYnV0dG9uXCIgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgIENsYWltc0NvbW1vbi5nZXRMb3NzSXRlbXMoeyAuLi5pdGVtLCAuLi57IHBvbGljeUlkOiBpdGVtLnJlZklkIH0gfSwgX3RoaXMpO1xuICAgICAgICB9fT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDbGFpbSBOb3RpZmljYXRpb25cIilcbiAgICAgICAgICAgIC50aGFpKFwiQ2xhaW0gTm90aWZpY2F0aW9uXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuICBpZiAoYWxsb3dUb0Nsb25lKSB7XG4gICAgbW9yZU9wZXIucHVzaChcbiAgICAgIDxNZW51Lkl0ZW0ga2V5PVwiYWxsb3dUb0Nsb25lXCI+XG4gICAgICAgIDxhIGNsYXNzTmFtZT1cImJ1dHRvblwiIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICBjb25zdCB7XG4gICAgICAgICAgICBwcm9kdWN0Q29kZSxcbiAgICAgICAgICAgIHJlZklkLFxuICAgICAgICAgICAgYml6VHlwZSxcbiAgICAgICAgICAgIGl0bnRDb2RlLFxuICAgICAgICAgICAgcHJvZHVjdFZlcnNpb24sXG4gICAgICAgICAgfSA9IGl0ZW07XG4gICAgICAgICAgY29uc3QgX3VybCA9IFBBVEguTk9STUFMX1FVT1RFLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRudENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwcmR0Q29kZVwiLCBwcm9kdWN0Q29kZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiLzpwb2xpY3lJZD9cIiwgXCI/Y2xvbmVQb2xpY3lJZD1cIiArIHJlZklkKTtcbiAgICAgICAgICByb3V0ZXIucHVzaFJlZGlyZWN0KF91cmwpO1xuICAgICAgICB9fT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJDbG9uZVwiKS50aGFpKFwiQ2xvbmVcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICA8L2E+XG4gICAgICA8L01lbnUuSXRlbT4sXG4gICAgKTtcbiAgfVxuICBjb25zdCBtZW51ID0gPE1lbnU+e21vcmVPcGVyfTwvTWVudT47XG4gIGlmIChtb3JlT3Blci5sZW5ndGggPiAwKSB7XG4gICAgcmVzdWx0LnB1c2goXG4gICAgICA8RHJvcGRvd24gb3ZlcmxheT17bWVudX0ga2V5PVwiZHJvcGRvd25cIiB0cmlnZ2VyPXtbXCJob3ZlclwiLCBcImNsaWNrXCJdfT5cbiAgICAgICAgPGEgY2xhc3NOYW1lPXtgYW50LWRyb3Bkb3duLWxpbmsgYnV0dG9uYH0gaHJlZj1cIiNcIj5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJNb3JlXCIpXG4gICAgICAgICAgICAudGhhaShcIuC5gOC4o+C4teC4ouC4geC4lOC4uVwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX17XCIgXCJ9XG4gICAgICAgICAgPEljb24gdHlwZT1cImRvd25cIi8+XG4gICAgICAgIDwvYT5cbiAgICAgIDwvRHJvcGRvd24+LFxuICAgICk7XG4gIH1cblxuICBpZiAoIXJlc3VsdC5sZW5ndGgpIHtcbiAgICByZXN1bHQucHVzaChcIi1cIik7XG4gIH1cblxuICByZXR1cm4gVXRpbHMuam9pbkVsZW1lbnRzKHJlc3VsdCwgPERpdmlkZXIgdHlwZT1cInZlcnRpY2FsXCIvPik7XG59XG5cblxuZnVuY3Rpb24gcmVuZGVyVmlld0J0bihpdGVtOiBhbnksIHJvdXRlcjogYW55KSB7XG4gIGNvbnN0IHtcbiAgICBwcm9kdWN0Q29kZSxcbiAgICByZWZJZCxcbiAgICBiaXpUeXBlLFxuICAgIGl0bnRDb2RlLFxuICAgIHByb2R1Y3RWZXJzaW9uLFxuICB9ID0gaXRlbTtcbiAgcmV0dXJuIDxhXG4gICAga2V5PVwidmlld2FibGVcIlxuICAgIGNsYXNzTmFtZT1cImJ1dHRvblwiXG4gICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgcm91dGVyLnB1c2hSZWRpcmVjdChwb2xpY3lQYXRoKHtcbiAgICAgICAgdHlwZTogXCJ2aWV3XCIsXG4gICAgICAgIHByb2R1Y3RDb2RlLFxuICAgICAgICBwb2xpY3lJZDogcmVmSWQsXG4gICAgICAgIGJpelR5cGUsXG4gICAgICAgIGl0bnRDb2RlLFxuICAgICAgICBwcm9kdWN0VmVyc2lvbixcbiAgICAgIH0pKTtcbiAgICB9fVxuICA+XG4gICAge0xhbmd1YWdlLmVuKFwiVmlld1wiKVxuICAgICAgLnRoYWkoXCLguJTguLnguKPguLLguKLguKXguLDguYDguK3guLXguKLguJRcIilcbiAgICAgIC5nZXRNZXNzYWdlKCl9XG4gIDwvYT47XG59XG5cbmZ1bmN0aW9uIHJlbmRlckVkaXRCdG4oaXRlbTogYW55LCByb3V0ZXI6IGFueSkge1xuICBjb25zdCB7XG4gICAgcHJvZHVjdENvZGUsXG4gICAgcHJvZHVjdENhdGUsXG4gICAgYml6VHlwZSxcbiAgICBpdG50Q29kZSxcbiAgICBwcm9kdWN0VmVyc2lvbixcbiAgICByZWZJZCxcbiAgfSA9IGl0ZW07XG4gIGxldCBjb2RlID0gIV8uaXNFbXB0eShwcm9kdWN0Q29kZSkgPyBwcm9kdWN0Q29kZSA6IHByb2R1Y3RDYXRlO1xuICByZXR1cm4gPGFcbiAgICBrZXk9XCJlZGl0YWJsZVwiXG4gICAgY2xhc3NOYW1lPVwiYnV0dG9uXCJcbiAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAvL1RPRE8gbmVlZCBjb21wYXRpYmxlIG11bHRpIGluc3VyZXIgdGVuYW50XG4gICAgICByb3V0ZXIucHVzaFJlZGlyZWN0KFxuICAgICAgICBwb2xpY3lQYXRoKHtcbiAgICAgICAgICBwcm9kdWN0Q29kZSxcbiAgICAgICAgICB0eXBlOiBcIndvcmtcIixcbiAgICAgICAgICBiaXpUeXBlLFxuICAgICAgICAgIGl0bnRDb2RlLFxuICAgICAgICAgIHByb2R1Y3RWZXJzaW9uLFxuICAgICAgICAgIHBvbGljeUlkOiByZWZJZCxcbiAgICAgICAgfSkpO1xuXG4gICAgfX1cbiAgPlxuICAgIHtMYW5ndWFnZS5lbihcIldvcmsgT25cIilcbiAgICAgIC50aGFpKFwi4LmB4LiB4LmJ4LmE4LiCXCIpXG4gICAgICAuZ2V0TWVzc2FnZSgpfVxuICA8L2E+O1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQU9BO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFPQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVVBO0FBeEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBU0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFvQkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7QUFkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBT0E7QUFDQTtBQUVBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFpQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWtCQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFmQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFxQkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/query/components/query-opers.tsx
