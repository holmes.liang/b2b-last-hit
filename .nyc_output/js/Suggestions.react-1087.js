__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var draft_js_lib_DraftOffsetKey__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! draft-js/lib/DraftOffsetKey */ "./node_modules/draft-js/lib/DraftOffsetKey.js");
/* harmony import */ var draft_js_lib_DraftOffsetKey__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(draft_js_lib_DraftOffsetKey__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var rc_animate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rc-animate */ "./node_modules/rc-animate/es/Animate.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! dom-scroll-into-view */ "./node_modules/dom-scroll-into-view/lib/index.js");
/* harmony import */ var dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _Nav_react__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Nav.react */ "./node_modules/rc-editor-mention/es/component/Nav.react.js");
/* harmony import */ var _SuggestionWrapper_react__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./SuggestionWrapper.react */ "./node_modules/rc-editor-mention/es/component/SuggestionWrapper.react.js");
/* harmony import */ var _utils_insertMention__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../utils/insertMention */ "./node_modules/rc-editor-mention/es/utils/insertMention.js");
/* harmony import */ var _utils_clearMention__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../utils/clearMention */ "./node_modules/rc-editor-mention/es/utils/clearMention.js");
/* harmony import */ var _utils_getOffset__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../utils/getOffset */ "./node_modules/rc-editor-mention/es/utils/getOffset.js");
/* harmony import */ var _utils_getMentions__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../utils/getMentions */ "./node_modules/rc-editor-mention/es/utils/getMentions.js");
/* harmony import */ var _utils_getSearchWord__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../utils/getSearchWord */ "./node_modules/rc-editor-mention/es/utils/getSearchWord.js");



















var isNotFalse = function isNotFalse(i) {
  return i !== false;
};

var Suggestions = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Suggestions, _React$Component);

  function Suggestions() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Suggestions);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _React$Component.call(this));

    _this.onEditorStateChange = function (editorState) {
      var offset = _this.props.store.getOffset();

      if (offset.size === 0) {
        _this.closeDropDown();

        return editorState;
      }

      var selection = editorState.getSelection(); // 修复: 焦点移出再移入时, dropdown 会闪动一下
      // 原因: https://github.com/facebook/draft-js/blob/67c5e69499e3b0c149ce83b004872afdf4180463/src/component/handlers/edit/editOnFocus.js#L33
      // 此处强制 update 了一下,因此 onEditorStateChange 会 call 两次

      if (!_this.props.callbacks.getEditorState().getSelection().getHasFocus() && selection.getHasFocus()) {
        return editorState;
      }

      var _getSearchWord = Object(_utils_getSearchWord__WEBPACK_IMPORTED_MODULE_17__["default"])(editorState, selection),
          word = _getSearchWord.word;

      if (!word) {
        _this.closeDropDown();

        return editorState;
      }

      var selectionInsideMention = offset.map(function (_ref) {
        var offsetKey = _ref.offsetKey;

        var _decode = Object(draft_js_lib_DraftOffsetKey__WEBPACK_IMPORTED_MODULE_7__["decode"])(offsetKey),
            blockKey = _decode.blockKey,
            decoratorKey = _decode.decoratorKey,
            leafKey = _decode.leafKey;

        if (blockKey !== selection.anchorKey) {
          return false;
        }

        var leaf = editorState.getBlockTree(blockKey).getIn([decoratorKey, 'leaves', leafKey]);

        if (!leaf) {
          return false;
        }

        var startKey = leaf.get('start');
        var endKey = leaf.get('end'); // 处理只有一个 `@` 符号时的情况

        if (!word) {
          return false;
        }

        if (startKey === endKey - 1) {
          return selection.anchorOffset >= startKey + 1 && selection.anchorOffset <= endKey ? offsetKey : false;
        }

        return selection.anchorOffset > startKey + 1 && selection.anchorOffset <= endKey ? offsetKey : false;
      });
      var selectionInText = selectionInsideMention.some(isNotFalse);
      _this.activeOffsetKey = selectionInsideMention.find(isNotFalse) || _this.activeOffsetKey;

      var trigger = _this.props.store.getTrigger(_this.activeOffsetKey);

      if (!selectionInText || !selection.getHasFocus()) {
        _this.closeDropDown();

        return editorState;
      }

      var searchValue = word.substring(trigger.length, word.length);

      if (_this.lastSearchValue !== searchValue || _this.lastTrigger !== trigger) {
        _this.lastSearchValue = searchValue;
        _this.lastTrigger = trigger;

        _this.props.onSearchChange(searchValue, trigger);
      }

      if (!_this.state.active) {
        // 特例处理 https://github.com/ant-design/ant-design/issues/7838
        // 暂时没有更优雅的方法
        if (!trigger || word.indexOf(trigger) !== -1) {
          _this.openDropDown();
        }
      }

      return editorState;
    };

    _this.onUpArrow = function (ev) {
      ev.preventDefault();

      if (_this.props.suggestions.length > 0) {
        var newIndex = _this.state.focusedIndex - 1;

        _this.setState({
          focusedIndex: Math.max(newIndex, 0)
        });
      }
    };

    _this.onBlur = function (ev) {
      ev.preventDefault();

      _this.closeDropDown();
    };

    _this.onDownArrow = function (ev) {
      ev.preventDefault();
      var newIndex = _this.state.focusedIndex + 1;

      _this.setState({
        focusedIndex: newIndex >= _this.props.suggestions.length ? 0 : newIndex
      });
    };

    _this.getContainer = function () {
      var popupContainer = document.createElement('div');
      var mountNode = void 0;

      if (_this.props.getSuggestionContainer) {
        mountNode = _this.props.getSuggestionContainer();
        popupContainer.style.position = 'relative';
      } else {
        mountNode = document.body;
      }

      mountNode.appendChild(popupContainer);
      return popupContainer;
    };

    _this.handleKeyBinding = function (command) {
      return command === 'split-block';
    };

    _this.handleReturn = function (ev) {
      ev.preventDefault();
      var selectedSuggestion = _this.props.suggestions[_this.state.focusedIndex];

      if (selectedSuggestion) {
        if (react__WEBPACK_IMPORTED_MODULE_4___default.a.isValidElement(selectedSuggestion)) {
          _this.onMentionSelect(selectedSuggestion.props.value, selectedSuggestion.props.data);
        } else {
          _this.onMentionSelect(selectedSuggestion);
        }

        _this.lastSearchValue = null;
        _this.lastTrigger = null;
        return true;
      }

      return false;
    };

    _this.renderReady = function () {
      var container = _this.dropdownContainer;

      if (!container) {
        return;
      }

      var active = _this.state.active;
      var activeOffsetKey = _this.activeOffsetKey;

      var offset = _this.props.store.getOffset();

      var dropDownPosition = offset.get(activeOffsetKey);

      if (active && dropDownPosition) {
        var placement = _this.props.placement;

        var dropDownStyle = _this.getPositionStyle(true, dropDownPosition.position()); // Check if the above space is crowded


        var isTopCrowded = parseFloat(dropDownStyle.top) - window.scrollY - container.offsetHeight < 0; // Check if the under space is crowded

        var isBottomCrowded = (window.innerHeight || document.documentElement.clientHeight) - (parseFloat(dropDownStyle.top) - window.scrollY) - container.offsetHeight < 0;

        if (placement === 'top' && !isTopCrowded) {
          // The above space isn't crowded
          dropDownStyle.top = (parseFloat(dropDownStyle.top) - container.offsetHeight || 0) + 'px';
        }

        if (placement === 'bottom' && isBottomCrowded && !isTopCrowded) {
          // The above space isn't crowded and the under space is crowded.
          dropDownStyle.top = (parseFloat(dropDownStyle.top) - container.offsetHeight || 0) + 'px';
        }

        Object.keys(dropDownStyle).forEach(function (key) {
          container.style[key] = dropDownStyle[key];
        });
      }

      if (!_this.focusItem) {
        return;
      }

      dom_scroll_into_view__WEBPACK_IMPORTED_MODULE_10___default()(react_dom__WEBPACK_IMPORTED_MODULE_5___default.a.findDOMNode(_this.focusItem), container, {
        onlyScrollIfNeeded: true
      });
    };

    _this.getNavigations = function () {
      var _this$props = _this.props,
          prefixCls = _this$props.prefixCls,
          suggestions = _this$props.suggestions;
      var focusedIndex = _this.state.focusedIndex;
      return suggestions.length ? react__WEBPACK_IMPORTED_MODULE_4___default.a.Children.map(suggestions, function (element, index) {
        var focusItem = index === focusedIndex;
        var ref = focusItem ? function (node) {
          _this.focusItem = node;
        } : null;
        var mentionClass = classnames__WEBPACK_IMPORTED_MODULE_9___default()(prefixCls + '-dropdown-item', {
          focus: focusItem
        });

        if (react__WEBPACK_IMPORTED_MODULE_4___default.a.isValidElement(element)) {
          return react__WEBPACK_IMPORTED_MODULE_4___default.a.cloneElement(element, {
            className: mentionClass,
            onMouseDown: function onMouseDown() {
              return _this.onDropdownMentionSelect(element.props.value, element.props.data);
            },
            ref: ref
          });
        }

        return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_Nav_react__WEBPACK_IMPORTED_MODULE_11__["default"], {
          ref: ref,
          className: mentionClass,
          onMouseDown: function onMouseDown() {
            return _this.onDropdownMentionSelect(element);
          }
        }, element);
      }, _this) : react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
        className: prefixCls + '-dropdown-notfound ' + prefixCls + '-dropdown-item'
      }, _this.props.notFoundContent);
    };

    _this.state = {
      isActive: false,
      focusedIndex: 0,
      container: false
    };
    return _this;
  }

  Suggestions.prototype.componentDidMount = function componentDidMount() {
    this.props.callbacks.onChange = this.onEditorStateChange;
  };

  Suggestions.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    if (nextProps.suggestions.length !== this.props.suggestions.length) {
      this.setState({
        focusedIndex: 0
      });
    }
  };

  Suggestions.prototype.onDropdownMentionSelect = function onDropdownMentionSelect(mention, data) {
    var _this2 = this;

    setTimeout(function () {
      _this2.onMentionSelect(mention, data);
    }, 100);
  };

  Suggestions.prototype.onMentionSelect = function onMentionSelect(mention, data) {
    var editorState = this.props.callbacks.getEditorState();
    var _props = this.props,
        store = _props.store,
        onSelect = _props.onSelect;
    var trigger = store.getTrigger(this.activeOffsetKey);

    if (onSelect) {
      onSelect(mention, data || mention);
    }

    if (this.props.noRedup) {
      var mentions = Object(_utils_getMentions__WEBPACK_IMPORTED_MODULE_16__["default"])(editorState.getCurrentContent(), trigger);

      if (mentions.indexOf('' + trigger + mention) !== -1) {
        // eslint-disable-next-line
        console.warn('you have specified `noRedup` props but have duplicated mentions.');
        this.closeDropDown();
        this.props.callbacks.setEditorState(Object(_utils_clearMention__WEBPACK_IMPORTED_MODULE_14__["default"])(editorState));
        return;
      }
    }

    this.props.callbacks.setEditorState(Object(_utils_insertMention__WEBPACK_IMPORTED_MODULE_13__["default"])(editorState, '' + trigger + mention, data, this.props.mode), true);
    this.closeDropDown();
  };

  Suggestions.prototype.getPositionStyle = function getPositionStyle(isActive, position) {
    if (this.props.getSuggestionStyle) {
      return this.props.getSuggestionStyle(isActive, position);
    }

    var container = this.props.getSuggestionContainer ? this.state.container : document.body;
    var offset = Object(_utils_getOffset__WEBPACK_IMPORTED_MODULE_15__["default"])(container);
    return position ? babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      position: 'absolute',
      left: position.left - offset.left + 'px',
      top: position.top - offset.top + 'px'
    }, this.props.style) : {};
  };

  Suggestions.prototype.openDropDown = function openDropDown() {
    this.props.callbacks.onUpArrow = this.onUpArrow;
    this.props.callbacks.handleReturn = this.handleReturn;
    this.props.callbacks.handleKeyBinding = this.handleKeyBinding;
    this.props.callbacks.onDownArrow = this.onDownArrow;
    this.props.callbacks.onBlur = this.onBlur;
    this.setState({
      active: true,
      container: this.state.container || this.getContainer()
    });
  };

  Suggestions.prototype.closeDropDown = function closeDropDown() {
    this.props.callbacks.onUpArrow = null;
    this.props.callbacks.handleReturn = null;
    this.props.callbacks.handleKeyBinding = null;
    this.props.callbacks.onDownArrow = null;
    this.props.callbacks.onBlur = null;
    this.setState({
      active: false
    });
  };

  Suggestions.prototype.render = function render() {
    var _extends2,
        _this3 = this;

    var _props2 = this.props,
        prefixCls = _props2.prefixCls,
        className = _props2.className,
        placement = _props2.placement;
    var _state = this.state,
        container = _state.container,
        active = _state.active;
    var cls = classnames__WEBPACK_IMPORTED_MODULE_9___default()(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()((_extends2 = {}, _extends2[prefixCls + '-dropdown'] = true, _extends2[prefixCls + '-dropdown-placement-' + placement] = true, _extends2), className));
    var transitionName = placement === 'top' ? 'slide-down' : 'slide-up';
    var navigations = this.getNavigations();
    return container ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_SuggestionWrapper_react__WEBPACK_IMPORTED_MODULE_12__["default"], {
      renderReady: this.renderReady,
      container: container
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_animate__WEBPACK_IMPORTED_MODULE_8__["default"], {
      transitionName: transitionName
    }, active ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: cls,
      ref: function ref(node) {
        _this3.dropdownContainer = node;
      }
    }, navigations) : null)) : null;
  };

  return Suggestions;
}(react__WEBPACK_IMPORTED_MODULE_4___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (Suggestions);
Suggestions.propTypes = {
  callbacks: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
  suggestions: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.array,
  store: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
  onSearchChange: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  mode: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  style: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.object,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  getSuggestionContainer: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  notFoundContent: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.any,
  getSuggestionStyle: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.func,
  className: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string,
  noRedup: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.bool,
  placement: prop_types__WEBPACK_IMPORTED_MODULE_6___default.a.string
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L1N1Z2dlc3Rpb25zLnJlYWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L1N1Z2dlc3Rpb25zLnJlYWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfZXh0ZW5kcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcyc7XG5pbXBvcnQgX2NsYXNzQ2FsbENoZWNrIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjayc7XG5pbXBvcnQgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4gZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nO1xuaW1wb3J0IF9pbmhlcml0cyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnO1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGRlY29kZSB9IGZyb20gJ2RyYWZ0LWpzL2xpYi9EcmFmdE9mZnNldEtleSc7XG5pbXBvcnQgQW5pbWF0ZSBmcm9tICdyYy1hbmltYXRlJztcblxuaW1wb3J0IGN4IGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHNjcm9sbEludG9WaWV3IGZyb20gJ2RvbS1zY3JvbGwtaW50by12aWV3JztcblxuaW1wb3J0IE5hdiBmcm9tICcuL05hdi5yZWFjdCc7XG5pbXBvcnQgU3VnZ2V0aW9uV3JhcHBlciBmcm9tICcuL1N1Z2dlc3Rpb25XcmFwcGVyLnJlYWN0JztcblxuaW1wb3J0IGluc2VydE1lbnRpb24gZnJvbSAnLi4vdXRpbHMvaW5zZXJ0TWVudGlvbic7XG5pbXBvcnQgY2xlYXJNZW50aW9uIGZyb20gJy4uL3V0aWxzL2NsZWFyTWVudGlvbic7XG5pbXBvcnQgZ2V0T2Zmc2V0IGZyb20gJy4uL3V0aWxzL2dldE9mZnNldCc7XG5pbXBvcnQgZ2V0TWVudGlvbnMgZnJvbSAnLi4vdXRpbHMvZ2V0TWVudGlvbnMnO1xuaW1wb3J0IGdldFNlYXJjaFdvcmQgZnJvbSAnLi4vdXRpbHMvZ2V0U2VhcmNoV29yZCc7XG5cbnZhciBpc05vdEZhbHNlID0gZnVuY3Rpb24gaXNOb3RGYWxzZShpKSB7XG4gIHJldHVybiBpICE9PSBmYWxzZTtcbn07XG5cbnZhciBTdWdnZXN0aW9ucyA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhTdWdnZXN0aW9ucywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3VnZ2VzdGlvbnMoKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFN1Z2dlc3Rpb25zKTtcblxuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbCh0aGlzKSk7XG5cbiAgICBfdGhpcy5vbkVkaXRvclN0YXRlQ2hhbmdlID0gZnVuY3Rpb24gKGVkaXRvclN0YXRlKSB7XG4gICAgICB2YXIgb2Zmc2V0ID0gX3RoaXMucHJvcHMuc3RvcmUuZ2V0T2Zmc2V0KCk7XG4gICAgICBpZiAob2Zmc2V0LnNpemUgPT09IDApIHtcbiAgICAgICAgX3RoaXMuY2xvc2VEcm9wRG93bigpO1xuICAgICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgICB9XG4gICAgICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgICAgIC8vIOS/ruWkjTog54Sm54K556e75Ye65YaN56e75YWl5pe2LCBkcm9wZG93biDkvJrpl6rliqjkuIDkuItcbiAgICAgIC8vIOWOn+WboDogaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL2RyYWZ0LWpzL2Jsb2IvNjdjNWU2OTQ5OWUzYjBjMTQ5Y2U4M2IwMDQ4NzJhZmRmNDE4MDQ2My9zcmMvY29tcG9uZW50L2hhbmRsZXJzL2VkaXQvZWRpdE9uRm9jdXMuanMjTDMzXG4gICAgICAvLyDmraTlpITlvLrliLYgdXBkYXRlIOS6huS4gOS4iyzlm6DmraQgb25FZGl0b3JTdGF0ZUNoYW5nZSDkvJogY2FsbCDkuKTmrKFcbiAgICAgIGlmICghX3RoaXMucHJvcHMuY2FsbGJhY2tzLmdldEVkaXRvclN0YXRlKCkuZ2V0U2VsZWN0aW9uKCkuZ2V0SGFzRm9jdXMoKSAmJiBzZWxlY3Rpb24uZ2V0SGFzRm9jdXMoKSkge1xuICAgICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgICB9XG5cbiAgICAgIHZhciBfZ2V0U2VhcmNoV29yZCA9IGdldFNlYXJjaFdvcmQoZWRpdG9yU3RhdGUsIHNlbGVjdGlvbiksXG4gICAgICAgICAgd29yZCA9IF9nZXRTZWFyY2hXb3JkLndvcmQ7XG5cbiAgICAgIGlmICghd29yZCkge1xuICAgICAgICBfdGhpcy5jbG9zZURyb3BEb3duKCk7XG4gICAgICAgIHJldHVybiBlZGl0b3JTdGF0ZTtcbiAgICAgIH1cbiAgICAgIHZhciBzZWxlY3Rpb25JbnNpZGVNZW50aW9uID0gb2Zmc2V0Lm1hcChmdW5jdGlvbiAoX3JlZikge1xuICAgICAgICB2YXIgb2Zmc2V0S2V5ID0gX3JlZi5vZmZzZXRLZXk7XG5cbiAgICAgICAgdmFyIF9kZWNvZGUgPSBkZWNvZGUob2Zmc2V0S2V5KSxcbiAgICAgICAgICAgIGJsb2NrS2V5ID0gX2RlY29kZS5ibG9ja0tleSxcbiAgICAgICAgICAgIGRlY29yYXRvcktleSA9IF9kZWNvZGUuZGVjb3JhdG9yS2V5LFxuICAgICAgICAgICAgbGVhZktleSA9IF9kZWNvZGUubGVhZktleTtcblxuICAgICAgICBpZiAoYmxvY2tLZXkgIT09IHNlbGVjdGlvbi5hbmNob3JLZXkpIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGxlYWYgPSBlZGl0b3JTdGF0ZS5nZXRCbG9ja1RyZWUoYmxvY2tLZXkpLmdldEluKFtkZWNvcmF0b3JLZXksICdsZWF2ZXMnLCBsZWFmS2V5XSk7XG4gICAgICAgIGlmICghbGVhZikge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgc3RhcnRLZXkgPSBsZWFmLmdldCgnc3RhcnQnKTtcbiAgICAgICAgdmFyIGVuZEtleSA9IGxlYWYuZ2V0KCdlbmQnKTtcbiAgICAgICAgLy8g5aSE55CG5Y+q5pyJ5LiA5LiqIGBAYCDnrKblj7fml7bnmoTmg4XlhrVcbiAgICAgICAgaWYgKCF3b3JkKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChzdGFydEtleSA9PT0gZW5kS2V5IC0gMSkge1xuICAgICAgICAgIHJldHVybiBzZWxlY3Rpb24uYW5jaG9yT2Zmc2V0ID49IHN0YXJ0S2V5ICsgMSAmJiBzZWxlY3Rpb24uYW5jaG9yT2Zmc2V0IDw9IGVuZEtleSA/IG9mZnNldEtleSA6IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzZWxlY3Rpb24uYW5jaG9yT2Zmc2V0ID4gc3RhcnRLZXkgKyAxICYmIHNlbGVjdGlvbi5hbmNob3JPZmZzZXQgPD0gZW5kS2V5ID8gb2Zmc2V0S2V5IDogZmFsc2U7XG4gICAgICB9KTtcblxuICAgICAgdmFyIHNlbGVjdGlvbkluVGV4dCA9IHNlbGVjdGlvbkluc2lkZU1lbnRpb24uc29tZShpc05vdEZhbHNlKTtcbiAgICAgIF90aGlzLmFjdGl2ZU9mZnNldEtleSA9IHNlbGVjdGlvbkluc2lkZU1lbnRpb24uZmluZChpc05vdEZhbHNlKSB8fCBfdGhpcy5hY3RpdmVPZmZzZXRLZXk7XG4gICAgICB2YXIgdHJpZ2dlciA9IF90aGlzLnByb3BzLnN0b3JlLmdldFRyaWdnZXIoX3RoaXMuYWN0aXZlT2Zmc2V0S2V5KTtcblxuICAgICAgaWYgKCFzZWxlY3Rpb25JblRleHQgfHwgIXNlbGVjdGlvbi5nZXRIYXNGb2N1cygpKSB7XG4gICAgICAgIF90aGlzLmNsb3NlRHJvcERvd24oKTtcbiAgICAgICAgcmV0dXJuIGVkaXRvclN0YXRlO1xuICAgICAgfVxuICAgICAgdmFyIHNlYXJjaFZhbHVlID0gd29yZC5zdWJzdHJpbmcodHJpZ2dlci5sZW5ndGgsIHdvcmQubGVuZ3RoKTtcbiAgICAgIGlmIChfdGhpcy5sYXN0U2VhcmNoVmFsdWUgIT09IHNlYXJjaFZhbHVlIHx8IF90aGlzLmxhc3RUcmlnZ2VyICE9PSB0cmlnZ2VyKSB7XG4gICAgICAgIF90aGlzLmxhc3RTZWFyY2hWYWx1ZSA9IHNlYXJjaFZhbHVlO1xuICAgICAgICBfdGhpcy5sYXN0VHJpZ2dlciA9IHRyaWdnZXI7XG4gICAgICAgIF90aGlzLnByb3BzLm9uU2VhcmNoQ2hhbmdlKHNlYXJjaFZhbHVlLCB0cmlnZ2VyKTtcbiAgICAgIH1cbiAgICAgIGlmICghX3RoaXMuc3RhdGUuYWN0aXZlKSB7XG4gICAgICAgIC8vIOeJueS+i+WkhOeQhiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy83ODM4XG4gICAgICAgIC8vIOaaguaXtuayoeacieabtOS8mOmbheeahOaWueazlVxuICAgICAgICBpZiAoIXRyaWdnZXIgfHwgd29yZC5pbmRleE9mKHRyaWdnZXIpICE9PSAtMSkge1xuICAgICAgICAgIF90aGlzLm9wZW5Ecm9wRG93bigpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gICAgfTtcblxuICAgIF90aGlzLm9uVXBBcnJvdyA9IGZ1bmN0aW9uIChldikge1xuICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5zdWdnZXN0aW9ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgIHZhciBuZXdJbmRleCA9IF90aGlzLnN0YXRlLmZvY3VzZWRJbmRleCAtIDE7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBmb2N1c2VkSW5kZXg6IE1hdGgubWF4KG5ld0luZGV4LCAwKVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25CbHVyID0gZnVuY3Rpb24gKGV2KSB7XG4gICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgX3RoaXMuY2xvc2VEcm9wRG93bigpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkRvd25BcnJvdyA9IGZ1bmN0aW9uIChldikge1xuICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHZhciBuZXdJbmRleCA9IF90aGlzLnN0YXRlLmZvY3VzZWRJbmRleCArIDE7XG4gICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGZvY3VzZWRJbmRleDogbmV3SW5kZXggPj0gX3RoaXMucHJvcHMuc3VnZ2VzdGlvbnMubGVuZ3RoID8gMCA6IG5ld0luZGV4XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0Q29udGFpbmVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHBvcHVwQ29udGFpbmVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICB2YXIgbW91bnROb2RlID0gdm9pZCAwO1xuICAgICAgaWYgKF90aGlzLnByb3BzLmdldFN1Z2dlc3Rpb25Db250YWluZXIpIHtcbiAgICAgICAgbW91bnROb2RlID0gX3RoaXMucHJvcHMuZ2V0U3VnZ2VzdGlvbkNvbnRhaW5lcigpO1xuICAgICAgICBwb3B1cENvbnRhaW5lci5zdHlsZS5wb3NpdGlvbiA9ICdyZWxhdGl2ZSc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBtb3VudE5vZGUgPSBkb2N1bWVudC5ib2R5O1xuICAgICAgfVxuICAgICAgbW91bnROb2RlLmFwcGVuZENoaWxkKHBvcHVwQ29udGFpbmVyKTtcbiAgICAgIHJldHVybiBwb3B1cENvbnRhaW5lcjtcbiAgICB9O1xuXG4gICAgX3RoaXMuaGFuZGxlS2V5QmluZGluZyA9IGZ1bmN0aW9uIChjb21tYW5kKSB7XG4gICAgICByZXR1cm4gY29tbWFuZCA9PT0gJ3NwbGl0LWJsb2NrJztcbiAgICB9O1xuXG4gICAgX3RoaXMuaGFuZGxlUmV0dXJuID0gZnVuY3Rpb24gKGV2KSB7XG4gICAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgdmFyIHNlbGVjdGVkU3VnZ2VzdGlvbiA9IF90aGlzLnByb3BzLnN1Z2dlc3Rpb25zW190aGlzLnN0YXRlLmZvY3VzZWRJbmRleF07XG4gICAgICBpZiAoc2VsZWN0ZWRTdWdnZXN0aW9uKSB7XG4gICAgICAgIGlmIChSZWFjdC5pc1ZhbGlkRWxlbWVudChzZWxlY3RlZFN1Z2dlc3Rpb24pKSB7XG4gICAgICAgICAgX3RoaXMub25NZW50aW9uU2VsZWN0KHNlbGVjdGVkU3VnZ2VzdGlvbi5wcm9wcy52YWx1ZSwgc2VsZWN0ZWRTdWdnZXN0aW9uLnByb3BzLmRhdGEpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIF90aGlzLm9uTWVudGlvblNlbGVjdChzZWxlY3RlZFN1Z2dlc3Rpb24pO1xuICAgICAgICB9XG4gICAgICAgIF90aGlzLmxhc3RTZWFyY2hWYWx1ZSA9IG51bGw7XG4gICAgICAgIF90aGlzLmxhc3RUcmlnZ2VyID0gbnVsbDtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcblxuICAgIF90aGlzLnJlbmRlclJlYWR5ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGNvbnRhaW5lciA9IF90aGlzLmRyb3Bkb3duQ29udGFpbmVyO1xuICAgICAgaWYgKCFjb250YWluZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdmFyIGFjdGl2ZSA9IF90aGlzLnN0YXRlLmFjdGl2ZTtcbiAgICAgIHZhciBhY3RpdmVPZmZzZXRLZXkgPSBfdGhpcy5hY3RpdmVPZmZzZXRLZXk7XG5cbiAgICAgIHZhciBvZmZzZXQgPSBfdGhpcy5wcm9wcy5zdG9yZS5nZXRPZmZzZXQoKTtcbiAgICAgIHZhciBkcm9wRG93blBvc2l0aW9uID0gb2Zmc2V0LmdldChhY3RpdmVPZmZzZXRLZXkpO1xuXG4gICAgICBpZiAoYWN0aXZlICYmIGRyb3BEb3duUG9zaXRpb24pIHtcbiAgICAgICAgdmFyIHBsYWNlbWVudCA9IF90aGlzLnByb3BzLnBsYWNlbWVudDtcbiAgICAgICAgdmFyIGRyb3BEb3duU3R5bGUgPSBfdGhpcy5nZXRQb3NpdGlvblN0eWxlKHRydWUsIGRyb3BEb3duUG9zaXRpb24ucG9zaXRpb24oKSk7XG5cbiAgICAgICAgLy8gQ2hlY2sgaWYgdGhlIGFib3ZlIHNwYWNlIGlzIGNyb3dkZWRcbiAgICAgICAgdmFyIGlzVG9wQ3Jvd2RlZCA9IHBhcnNlRmxvYXQoZHJvcERvd25TdHlsZS50b3ApIC0gd2luZG93LnNjcm9sbFkgLSBjb250YWluZXIub2Zmc2V0SGVpZ2h0IDwgMDtcbiAgICAgICAgLy8gQ2hlY2sgaWYgdGhlIHVuZGVyIHNwYWNlIGlzIGNyb3dkZWRcbiAgICAgICAgdmFyIGlzQm90dG9tQ3Jvd2RlZCA9ICh3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCkgLSAocGFyc2VGbG9hdChkcm9wRG93blN0eWxlLnRvcCkgLSB3aW5kb3cuc2Nyb2xsWSkgLSBjb250YWluZXIub2Zmc2V0SGVpZ2h0IDwgMDtcblxuICAgICAgICBpZiAocGxhY2VtZW50ID09PSAndG9wJyAmJiAhaXNUb3BDcm93ZGVkKSB7XG4gICAgICAgICAgLy8gVGhlIGFib3ZlIHNwYWNlIGlzbid0IGNyb3dkZWRcbiAgICAgICAgICBkcm9wRG93blN0eWxlLnRvcCA9IChwYXJzZUZsb2F0KGRyb3BEb3duU3R5bGUudG9wKSAtIGNvbnRhaW5lci5vZmZzZXRIZWlnaHQgfHwgMCkgKyAncHgnO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBsYWNlbWVudCA9PT0gJ2JvdHRvbScgJiYgaXNCb3R0b21Dcm93ZGVkICYmICFpc1RvcENyb3dkZWQpIHtcbiAgICAgICAgICAvLyBUaGUgYWJvdmUgc3BhY2UgaXNuJ3QgY3Jvd2RlZCBhbmQgdGhlIHVuZGVyIHNwYWNlIGlzIGNyb3dkZWQuXG4gICAgICAgICAgZHJvcERvd25TdHlsZS50b3AgPSAocGFyc2VGbG9hdChkcm9wRG93blN0eWxlLnRvcCkgLSBjb250YWluZXIub2Zmc2V0SGVpZ2h0IHx8IDApICsgJ3B4JztcbiAgICAgICAgfVxuXG4gICAgICAgIE9iamVjdC5rZXlzKGRyb3BEb3duU3R5bGUpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIGNvbnRhaW5lci5zdHlsZVtrZXldID0gZHJvcERvd25TdHlsZVtrZXldO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgaWYgKCFfdGhpcy5mb2N1c0l0ZW0pIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgc2Nyb2xsSW50b1ZpZXcoUmVhY3RET00uZmluZERPTU5vZGUoX3RoaXMuZm9jdXNJdGVtKSwgY29udGFpbmVyLCB7XG4gICAgICAgIG9ubHlTY3JvbGxJZk5lZWRlZDogdHJ1ZVxuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLmdldE5hdmlnYXRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHMucHJlZml4Q2xzLFxuICAgICAgICAgIHN1Z2dlc3Rpb25zID0gX3RoaXMkcHJvcHMuc3VnZ2VzdGlvbnM7XG4gICAgICB2YXIgZm9jdXNlZEluZGV4ID0gX3RoaXMuc3RhdGUuZm9jdXNlZEluZGV4O1xuXG4gICAgICByZXR1cm4gc3VnZ2VzdGlvbnMubGVuZ3RoID8gUmVhY3QuQ2hpbGRyZW4ubWFwKHN1Z2dlc3Rpb25zLCBmdW5jdGlvbiAoZWxlbWVudCwgaW5kZXgpIHtcbiAgICAgICAgdmFyIGZvY3VzSXRlbSA9IGluZGV4ID09PSBmb2N1c2VkSW5kZXg7XG4gICAgICAgIHZhciByZWYgPSBmb2N1c0l0ZW0gPyBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgIF90aGlzLmZvY3VzSXRlbSA9IG5vZGU7XG4gICAgICAgIH0gOiBudWxsO1xuICAgICAgICB2YXIgbWVudGlvbkNsYXNzID0gY3gocHJlZml4Q2xzICsgJy1kcm9wZG93bi1pdGVtJywge1xuICAgICAgICAgIGZvY3VzOiBmb2N1c0l0ZW1cbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChSZWFjdC5pc1ZhbGlkRWxlbWVudChlbGVtZW50KSkge1xuICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoZWxlbWVudCwge1xuICAgICAgICAgICAgY2xhc3NOYW1lOiBtZW50aW9uQ2xhc3MsXG4gICAgICAgICAgICBvbk1vdXNlRG93bjogZnVuY3Rpb24gb25Nb3VzZURvd24oKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5vbkRyb3Bkb3duTWVudGlvblNlbGVjdChlbGVtZW50LnByb3BzLnZhbHVlLCBlbGVtZW50LnByb3BzLmRhdGEpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJlZjogcmVmXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgTmF2LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHJlZjogcmVmLFxuICAgICAgICAgICAgY2xhc3NOYW1lOiBtZW50aW9uQ2xhc3MsXG4gICAgICAgICAgICBvbk1vdXNlRG93bjogZnVuY3Rpb24gb25Nb3VzZURvd24oKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5vbkRyb3Bkb3duTWVudGlvblNlbGVjdChlbGVtZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIGVsZW1lbnRcbiAgICAgICAgKTtcbiAgICAgIH0sIF90aGlzKSA6IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1kcm9wZG93bi1ub3Rmb3VuZCAnICsgcHJlZml4Q2xzICsgJy1kcm9wZG93bi1pdGVtJyB9LFxuICAgICAgICBfdGhpcy5wcm9wcy5ub3RGb3VuZENvbnRlbnRcbiAgICAgICk7XG4gICAgfTtcblxuICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAgaXNBY3RpdmU6IGZhbHNlLFxuICAgICAgZm9jdXNlZEluZGV4OiAwLFxuICAgICAgY29udGFpbmVyOiBmYWxzZVxuICAgIH07XG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgU3VnZ2VzdGlvbnMucHJvdG90eXBlLmNvbXBvbmVudERpZE1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdGhpcy5wcm9wcy5jYWxsYmFja3Mub25DaGFuZ2UgPSB0aGlzLm9uRWRpdG9yU3RhdGVDaGFuZ2U7XG4gIH07XG5cbiAgU3VnZ2VzdGlvbnMucHJvdG90eXBlLmNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgIGlmIChuZXh0UHJvcHMuc3VnZ2VzdGlvbnMubGVuZ3RoICE9PSB0aGlzLnByb3BzLnN1Z2dlc3Rpb25zLmxlbmd0aCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGZvY3VzZWRJbmRleDogMFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIFN1Z2dlc3Rpb25zLnByb3RvdHlwZS5vbkRyb3Bkb3duTWVudGlvblNlbGVjdCA9IGZ1bmN0aW9uIG9uRHJvcGRvd25NZW50aW9uU2VsZWN0KG1lbnRpb24sIGRhdGEpIHtcbiAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMyLm9uTWVudGlvblNlbGVjdChtZW50aW9uLCBkYXRhKTtcbiAgICB9LCAxMDApO1xuICB9O1xuXG4gIFN1Z2dlc3Rpb25zLnByb3RvdHlwZS5vbk1lbnRpb25TZWxlY3QgPSBmdW5jdGlvbiBvbk1lbnRpb25TZWxlY3QobWVudGlvbiwgZGF0YSkge1xuICAgIHZhciBlZGl0b3JTdGF0ZSA9IHRoaXMucHJvcHMuY2FsbGJhY2tzLmdldEVkaXRvclN0YXRlKCk7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHN0b3JlID0gX3Byb3BzLnN0b3JlLFxuICAgICAgICBvblNlbGVjdCA9IF9wcm9wcy5vblNlbGVjdDtcblxuICAgIHZhciB0cmlnZ2VyID0gc3RvcmUuZ2V0VHJpZ2dlcih0aGlzLmFjdGl2ZU9mZnNldEtleSk7XG4gICAgaWYgKG9uU2VsZWN0KSB7XG4gICAgICBvblNlbGVjdChtZW50aW9uLCBkYXRhIHx8IG1lbnRpb24pO1xuICAgIH1cbiAgICBpZiAodGhpcy5wcm9wcy5ub1JlZHVwKSB7XG4gICAgICB2YXIgbWVudGlvbnMgPSBnZXRNZW50aW9ucyhlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCB0cmlnZ2VyKTtcbiAgICAgIGlmIChtZW50aW9ucy5pbmRleE9mKCcnICsgdHJpZ2dlciArIG1lbnRpb24pICE9PSAtMSkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgY29uc29sZS53YXJuKCd5b3UgaGF2ZSBzcGVjaWZpZWQgYG5vUmVkdXBgIHByb3BzIGJ1dCBoYXZlIGR1cGxpY2F0ZWQgbWVudGlvbnMuJyk7XG4gICAgICAgIHRoaXMuY2xvc2VEcm9wRG93bigpO1xuICAgICAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5zZXRFZGl0b3JTdGF0ZShjbGVhck1lbnRpb24oZWRpdG9yU3RhdGUpKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5zZXRFZGl0b3JTdGF0ZShpbnNlcnRNZW50aW9uKGVkaXRvclN0YXRlLCAnJyArIHRyaWdnZXIgKyBtZW50aW9uLCBkYXRhLCB0aGlzLnByb3BzLm1vZGUpLCB0cnVlKTtcbiAgICB0aGlzLmNsb3NlRHJvcERvd24oKTtcbiAgfTtcblxuICBTdWdnZXN0aW9ucy5wcm90b3R5cGUuZ2V0UG9zaXRpb25TdHlsZSA9IGZ1bmN0aW9uIGdldFBvc2l0aW9uU3R5bGUoaXNBY3RpdmUsIHBvc2l0aW9uKSB7XG4gICAgaWYgKHRoaXMucHJvcHMuZ2V0U3VnZ2VzdGlvblN0eWxlKSB7XG4gICAgICByZXR1cm4gdGhpcy5wcm9wcy5nZXRTdWdnZXN0aW9uU3R5bGUoaXNBY3RpdmUsIHBvc2l0aW9uKTtcbiAgICB9XG4gICAgdmFyIGNvbnRhaW5lciA9IHRoaXMucHJvcHMuZ2V0U3VnZ2VzdGlvbkNvbnRhaW5lciA/IHRoaXMuc3RhdGUuY29udGFpbmVyIDogZG9jdW1lbnQuYm9keTtcbiAgICB2YXIgb2Zmc2V0ID0gZ2V0T2Zmc2V0KGNvbnRhaW5lcik7XG4gICAgcmV0dXJuIHBvc2l0aW9uID8gX2V4dGVuZHMoe1xuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICBsZWZ0OiBwb3NpdGlvbi5sZWZ0IC0gb2Zmc2V0LmxlZnQgKyAncHgnLFxuICAgICAgdG9wOiBwb3NpdGlvbi50b3AgLSBvZmZzZXQudG9wICsgJ3B4J1xuICAgIH0sIHRoaXMucHJvcHMuc3R5bGUpIDoge307XG4gIH07XG5cbiAgU3VnZ2VzdGlvbnMucHJvdG90eXBlLm9wZW5Ecm9wRG93biA9IGZ1bmN0aW9uIG9wZW5Ecm9wRG93bigpIHtcbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5vblVwQXJyb3cgPSB0aGlzLm9uVXBBcnJvdztcbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5oYW5kbGVSZXR1cm4gPSB0aGlzLmhhbmRsZVJldHVybjtcbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5oYW5kbGVLZXlCaW5kaW5nID0gdGhpcy5oYW5kbGVLZXlCaW5kaW5nO1xuICAgIHRoaXMucHJvcHMuY2FsbGJhY2tzLm9uRG93bkFycm93ID0gdGhpcy5vbkRvd25BcnJvdztcbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5vbkJsdXIgPSB0aGlzLm9uQmx1cjtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGFjdGl2ZTogdHJ1ZSxcbiAgICAgIGNvbnRhaW5lcjogdGhpcy5zdGF0ZS5jb250YWluZXIgfHwgdGhpcy5nZXRDb250YWluZXIoKVxuICAgIH0pO1xuICB9O1xuXG4gIFN1Z2dlc3Rpb25zLnByb3RvdHlwZS5jbG9zZURyb3BEb3duID0gZnVuY3Rpb24gY2xvc2VEcm9wRG93bigpIHtcbiAgICB0aGlzLnByb3BzLmNhbGxiYWNrcy5vblVwQXJyb3cgPSBudWxsO1xuICAgIHRoaXMucHJvcHMuY2FsbGJhY2tzLmhhbmRsZVJldHVybiA9IG51bGw7XG4gICAgdGhpcy5wcm9wcy5jYWxsYmFja3MuaGFuZGxlS2V5QmluZGluZyA9IG51bGw7XG4gICAgdGhpcy5wcm9wcy5jYWxsYmFja3Mub25Eb3duQXJyb3cgPSBudWxsO1xuICAgIHRoaXMucHJvcHMuY2FsbGJhY2tzLm9uQmx1ciA9IG51bGw7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBhY3RpdmU6IGZhbHNlXG4gICAgfSk7XG4gIH07XG5cbiAgU3VnZ2VzdGlvbnMucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX2V4dGVuZHMyLFxuICAgICAgICBfdGhpczMgPSB0aGlzO1xuXG4gICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICBwcmVmaXhDbHMgPSBfcHJvcHMyLnByZWZpeENscyxcbiAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzMi5jbGFzc05hbWUsXG4gICAgICAgIHBsYWNlbWVudCA9IF9wcm9wczIucGxhY2VtZW50O1xuICAgIHZhciBfc3RhdGUgPSB0aGlzLnN0YXRlLFxuICAgICAgICBjb250YWluZXIgPSBfc3RhdGUuY29udGFpbmVyLFxuICAgICAgICBhY3RpdmUgPSBfc3RhdGUuYWN0aXZlO1xuXG4gICAgdmFyIGNscyA9IGN4KF9leHRlbmRzKChfZXh0ZW5kczIgPSB7fSwgX2V4dGVuZHMyW3ByZWZpeENscyArICctZHJvcGRvd24nXSA9IHRydWUsIF9leHRlbmRzMltwcmVmaXhDbHMgKyAnLWRyb3Bkb3duLXBsYWNlbWVudC0nICsgcGxhY2VtZW50XSA9IHRydWUsIF9leHRlbmRzMiksIGNsYXNzTmFtZSkpO1xuICAgIHZhciB0cmFuc2l0aW9uTmFtZSA9IHBsYWNlbWVudCA9PT0gJ3RvcCcgPyAnc2xpZGUtZG93bicgOiAnc2xpZGUtdXAnO1xuXG4gICAgdmFyIG5hdmlnYXRpb25zID0gdGhpcy5nZXROYXZpZ2F0aW9ucygpO1xuXG4gICAgcmV0dXJuIGNvbnRhaW5lciA/IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICBTdWdnZXRpb25XcmFwcGVyLFxuICAgICAgeyByZW5kZXJSZWFkeTogdGhpcy5yZW5kZXJSZWFkeSwgY29udGFpbmVyOiBjb250YWluZXIgfSxcbiAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIEFuaW1hdGUsXG4gICAgICAgIHsgdHJhbnNpdGlvbk5hbWU6IHRyYW5zaXRpb25OYW1lIH0sXG4gICAgICAgIGFjdGl2ZSA/IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNscywgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczMuZHJvcGRvd25Db250YWluZXIgPSBub2RlO1xuICAgICAgICAgICAgfSB9LFxuICAgICAgICAgIG5hdmlnYXRpb25zXG4gICAgICAgICkgOiBudWxsXG4gICAgICApXG4gICAgKSA6IG51bGw7XG4gIH07XG5cbiAgcmV0dXJuIFN1Z2dlc3Rpb25zO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5leHBvcnQgZGVmYXVsdCBTdWdnZXN0aW9ucztcblxuXG5TdWdnZXN0aW9ucy5wcm9wVHlwZXMgPSB7XG4gIGNhbGxiYWNrczogUHJvcFR5cGVzLm9iamVjdCxcbiAgc3VnZ2VzdGlvbnM6IFByb3BUeXBlcy5hcnJheSxcbiAgc3RvcmU6IFByb3BUeXBlcy5vYmplY3QsXG4gIG9uU2VhcmNoQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBtb2RlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgb25TZWxlY3Q6IFByb3BUeXBlcy5mdW5jLFxuICBnZXRTdWdnZXN0aW9uQ29udGFpbmVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgbm90Rm91bmRDb250ZW50OiBQcm9wVHlwZXMuYW55LFxuICBnZXRTdWdnZXN0aW9uU3R5bGU6IFByb3BUeXBlcy5mdW5jLFxuICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG5vUmVkdXA6IFByb3BUeXBlcy5ib29sLFxuICBwbGFjZW1lbnQ6IFByb3BUeXBlcy5zdHJpbmdcbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVNBO0FBRUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFFQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/component/Suggestions.react.js
