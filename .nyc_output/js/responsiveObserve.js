__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "responsiveArray", function() { return responsiveArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "responsiveMap", function() { return responsiveMap; });
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
} // matchMedia polyfill for
// https://github.com/WickyNilliams/enquire.js/issues/82


var enquire; // TODO: Will be removed in antd 4.0 because we will no longer support ie9

if (typeof window !== 'undefined') {
  var matchMediaPolyfill = function matchMediaPolyfill(mediaQuery) {
    return {
      media: mediaQuery,
      matches: false,
      addListener: function addListener() {},
      removeListener: function removeListener() {}
    };
  }; // ref: https://github.com/ant-design/ant-design/issues/18774


  if (!window.matchMedia) window.matchMedia = matchMediaPolyfill; // eslint-disable-next-line global-require

  enquire = __webpack_require__(/*! enquire.js */ "./node_modules/enquire.js/src/index.js");
}

var responsiveArray = ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'];
var responsiveMap = {
  xs: '(max-width: 575px)',
  sm: '(min-width: 576px)',
  md: '(min-width: 768px)',
  lg: '(min-width: 992px)',
  xl: '(min-width: 1200px)',
  xxl: '(min-width: 1600px)'
};
var subscribers = [];
var subUid = -1;
var screens = {};
var responsiveObserve = {
  dispatch: function dispatch(pointMap) {
    screens = pointMap;

    if (subscribers.length < 1) {
      return false;
    }

    subscribers.forEach(function (item) {
      item.func(screens);
    });
    return true;
  },
  subscribe: function subscribe(func) {
    if (subscribers.length === 0) {
      this.register();
    }

    var token = (++subUid).toString();
    subscribers.push({
      token: token,
      func: func
    });
    func(screens);
    return token;
  },
  unsubscribe: function unsubscribe(token) {
    subscribers = subscribers.filter(function (item) {
      return item.token !== token;
    });

    if (subscribers.length === 0) {
      this.unregister();
    }
  },
  unregister: function unregister() {
    Object.keys(responsiveMap).map(function (screen) {
      return enquire.unregister(responsiveMap[screen]);
    });
  },
  register: function register() {
    var _this = this;

    Object.keys(responsiveMap).map(function (screen) {
      return enquire.register(responsiveMap[screen], {
        match: function match() {
          var pointMap = _extends(_extends({}, screens), _defineProperty({}, screen, true));

          _this.dispatch(pointMap);
        },
        unmatch: function unmatch() {
          var pointMap = _extends(_extends({}, screens), _defineProperty({}, screen, false));

          _this.dispatch(pointMap);
        },
        // Keep a empty destory to avoid triggering unmatch when unregister
        destroy: function destroy() {}
      });
    });
  }
};
/* harmony default export */ __webpack_exports__["default"] = (responsiveObserve);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9yZXNwb25zaXZlT2JzZXJ2ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvX3V0aWwvcmVzcG9uc2l2ZU9ic2VydmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gbWF0Y2hNZWRpYSBwb2x5ZmlsbCBmb3Jcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9XaWNreU5pbGxpYW1zL2VucXVpcmUuanMvaXNzdWVzLzgyXG5sZXQgZW5xdWlyZTtcbi8vIFRPRE86IFdpbGwgYmUgcmVtb3ZlZCBpbiBhbnRkIDQuMCBiZWNhdXNlIHdlIHdpbGwgbm8gbG9uZ2VyIHN1cHBvcnQgaWU5XG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBjb25zdCBtYXRjaE1lZGlhUG9seWZpbGwgPSAobWVkaWFRdWVyeSkgPT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbWVkaWE6IG1lZGlhUXVlcnksXG4gICAgICAgICAgICBtYXRjaGVzOiBmYWxzZSxcbiAgICAgICAgICAgIGFkZExpc3RlbmVyKCkgeyB9LFxuICAgICAgICAgICAgcmVtb3ZlTGlzdGVuZXIoKSB7IH0sXG4gICAgICAgIH07XG4gICAgfTtcbiAgICAvLyByZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzE4Nzc0XG4gICAgaWYgKCF3aW5kb3cubWF0Y2hNZWRpYSlcbiAgICAgICAgd2luZG93Lm1hdGNoTWVkaWEgPSBtYXRjaE1lZGlhUG9seWZpbGw7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGdsb2JhbC1yZXF1aXJlXG4gICAgZW5xdWlyZSA9IHJlcXVpcmUoJ2VucXVpcmUuanMnKTtcbn1cbmV4cG9ydCBjb25zdCByZXNwb25zaXZlQXJyYXkgPSBbJ3h4bCcsICd4bCcsICdsZycsICdtZCcsICdzbScsICd4cyddO1xuZXhwb3J0IGNvbnN0IHJlc3BvbnNpdmVNYXAgPSB7XG4gICAgeHM6ICcobWF4LXdpZHRoOiA1NzVweCknLFxuICAgIHNtOiAnKG1pbi13aWR0aDogNTc2cHgpJyxcbiAgICBtZDogJyhtaW4td2lkdGg6IDc2OHB4KScsXG4gICAgbGc6ICcobWluLXdpZHRoOiA5OTJweCknLFxuICAgIHhsOiAnKG1pbi13aWR0aDogMTIwMHB4KScsXG4gICAgeHhsOiAnKG1pbi13aWR0aDogMTYwMHB4KScsXG59O1xubGV0IHN1YnNjcmliZXJzID0gW107XG5sZXQgc3ViVWlkID0gLTE7XG5sZXQgc2NyZWVucyA9IHt9O1xuY29uc3QgcmVzcG9uc2l2ZU9ic2VydmUgPSB7XG4gICAgZGlzcGF0Y2gocG9pbnRNYXApIHtcbiAgICAgICAgc2NyZWVucyA9IHBvaW50TWFwO1xuICAgICAgICBpZiAoc3Vic2NyaWJlcnMubGVuZ3RoIDwgMSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHN1YnNjcmliZXJzLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgICAgICBpdGVtLmZ1bmMoc2NyZWVucyk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9LFxuICAgIHN1YnNjcmliZShmdW5jKSB7XG4gICAgICAgIGlmIChzdWJzY3JpYmVycy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMucmVnaXN0ZXIoKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCB0b2tlbiA9ICgrK3N1YlVpZCkudG9TdHJpbmcoKTtcbiAgICAgICAgc3Vic2NyaWJlcnMucHVzaCh7XG4gICAgICAgICAgICB0b2tlbixcbiAgICAgICAgICAgIGZ1bmMsXG4gICAgICAgIH0pO1xuICAgICAgICBmdW5jKHNjcmVlbnMpO1xuICAgICAgICByZXR1cm4gdG9rZW47XG4gICAgfSxcbiAgICB1bnN1YnNjcmliZSh0b2tlbikge1xuICAgICAgICBzdWJzY3JpYmVycyA9IHN1YnNjcmliZXJzLmZpbHRlcihpdGVtID0+IGl0ZW0udG9rZW4gIT09IHRva2VuKTtcbiAgICAgICAgaWYgKHN1YnNjcmliZXJzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy51bnJlZ2lzdGVyKCk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIHVucmVnaXN0ZXIoKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKHJlc3BvbnNpdmVNYXApLm1hcCgoc2NyZWVuKSA9PiBlbnF1aXJlLnVucmVnaXN0ZXIocmVzcG9uc2l2ZU1hcFtzY3JlZW5dKSk7XG4gICAgfSxcbiAgICByZWdpc3RlcigpIHtcbiAgICAgICAgT2JqZWN0LmtleXMocmVzcG9uc2l2ZU1hcCkubWFwKChzY3JlZW4pID0+IGVucXVpcmUucmVnaXN0ZXIocmVzcG9uc2l2ZU1hcFtzY3JlZW5dLCB7XG4gICAgICAgICAgICBtYXRjaDogKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHBvaW50TWFwID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBzY3JlZW5zKSwgeyBbc2NyZWVuXTogdHJ1ZSB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLmRpc3BhdGNoKHBvaW50TWFwKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB1bm1hdGNoOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgcG9pbnRNYXAgPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHNjcmVlbnMpLCB7IFtzY3JlZW5dOiBmYWxzZSB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLmRpc3BhdGNoKHBvaW50TWFwKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAvLyBLZWVwIGEgZW1wdHkgZGVzdG9yeSB0byBhdm9pZCB0cmlnZ2VyaW5nIHVubWF0Y2ggd2hlbiB1bnJlZ2lzdGVyXG4gICAgICAgICAgICBkZXN0cm95KCkgeyB9LFxuICAgICAgICB9KSk7XG4gICAgfSxcbn07XG5leHBvcnQgZGVmYXVsdCByZXNwb25zaXZlT2JzZXJ2ZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFGQTtBQUNBO0FBQ0E7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFyQkE7QUF1QkE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQTNCQTtBQTZCQTtBQUNBO0FBQUE7QUFBQTtBQTlCQTtBQWdDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUFBO0FBUEE7QUFTQTtBQUNBO0FBVkE7QUFBQTtBQVlBO0FBN0NBO0FBK0NBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/responsiveObserve.js
