/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule decodeEntityRanges
 * @format
 * 
 */


var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var substr = UnicodeUtils.substr;
/**
 * Convert to native JavaScript string lengths to determine ranges.
 */

function decodeEntityRanges(text, ranges) {
  var entities = Array(text.length).fill(null);

  if (ranges) {
    ranges.forEach(function (range) {
      // Using Unicode-enabled substrings converted to JavaScript lengths,
      // fill the output array with entity keys.
      var start = substr(text, 0, range.offset).length;
      var end = start + substr(text, range.offset, range.length).length;

      for (var ii = start; ii < end; ii++) {
        entities[ii] = range.key;
      }
    });
  }

  return entities;
}

module.exports = decodeEntityRanges;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2RlY29kZUVudGl0eVJhbmdlcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9kZWNvZGVFbnRpdHlSYW5nZXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBkZWNvZGVFbnRpdHlSYW5nZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgc3Vic3RyID0gVW5pY29kZVV0aWxzLnN1YnN0cjtcblxuLyoqXG4gKiBDb252ZXJ0IHRvIG5hdGl2ZSBKYXZhU2NyaXB0IHN0cmluZyBsZW5ndGhzIHRvIGRldGVybWluZSByYW5nZXMuXG4gKi9cblxuZnVuY3Rpb24gZGVjb2RlRW50aXR5UmFuZ2VzKHRleHQsIHJhbmdlcykge1xuICB2YXIgZW50aXRpZXMgPSBBcnJheSh0ZXh0Lmxlbmd0aCkuZmlsbChudWxsKTtcbiAgaWYgKHJhbmdlcykge1xuICAgIHJhbmdlcy5mb3JFYWNoKGZ1bmN0aW9uIChyYW5nZSkge1xuICAgICAgLy8gVXNpbmcgVW5pY29kZS1lbmFibGVkIHN1YnN0cmluZ3MgY29udmVydGVkIHRvIEphdmFTY3JpcHQgbGVuZ3RocyxcbiAgICAgIC8vIGZpbGwgdGhlIG91dHB1dCBhcnJheSB3aXRoIGVudGl0eSBrZXlzLlxuICAgICAgdmFyIHN0YXJ0ID0gc3Vic3RyKHRleHQsIDAsIHJhbmdlLm9mZnNldCkubGVuZ3RoO1xuICAgICAgdmFyIGVuZCA9IHN0YXJ0ICsgc3Vic3RyKHRleHQsIHJhbmdlLm9mZnNldCwgcmFuZ2UubGVuZ3RoKS5sZW5ndGg7XG4gICAgICBmb3IgKHZhciBpaSA9IHN0YXJ0OyBpaSA8IGVuZDsgaWkrKykge1xuICAgICAgICBlbnRpdGllc1tpaV0gPSByYW5nZS5rZXk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgcmV0dXJuIGVudGl0aWVzO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGRlY29kZUVudGl0eVJhbmdlczsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/decodeEntityRanges.js
