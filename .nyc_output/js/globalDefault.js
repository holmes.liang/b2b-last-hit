/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var platform = ''; // Navigator not exists in node

if (typeof navigator !== 'undefined') {
  platform = navigator.platform || '';
}

var _default = {
  // backgroundColor: 'rgba(0,0,0,0)',
  // https://dribbble.com/shots/1065960-Infographic-Pie-chart-visualization
  // color: ['#5793f3', '#d14a61', '#fd9c35', '#675bba', '#fec42c', '#dd4444', '#d4df5a', '#cd4870'],
  // Light colors:
  // color: ['#bcd3bb', '#e88f70', '#edc1a5', '#9dc5c8', '#e1e8c8', '#7b7c68', '#e5b5b5', '#f0b489', '#928ea8', '#bda29a'],
  // color: ['#cc5664', '#9bd6ec', '#ea946e', '#8acaaa', '#f1ec64', '#ee8686', '#a48dc1', '#5da6bc', '#b9dcae'],
  // Dark colors:
  color: ['#c23531', '#2f4554', '#61a0a8', '#d48265', '#91c7ae', '#749f83', '#ca8622', '#bda29a', '#6e7074', '#546570', '#c4ccd3'],
  gradientColor: ['#f6efa6', '#d88273', '#bf444c'],
  // If xAxis and yAxis declared, grid is created by default.
  // grid: {},
  textStyle: {
    // color: '#000',
    // decoration: 'none',
    // PENDING
    fontFamily: platform.match(/^Win/) ? 'Microsoft YaHei' : 'sans-serif',
    // fontFamily: 'Arial, Verdana, sans-serif',
    fontSize: 12,
    fontStyle: 'normal',
    fontWeight: 'normal'
  },
  // http://blogs.adobe.com/webplatform/2014/02/24/using-blend-modes-in-html-canvas/
  // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
  // Default is source-over
  blendMode: null,
  animation: 'auto',
  animationDuration: 1000,
  animationDurationUpdate: 300,
  animationEasing: 'exponentialOut',
  animationEasingUpdate: 'cubicOut',
  animationThreshold: 2000,
  // Configuration for progressive/incremental rendering
  progressiveThreshold: 3000,
  progressive: 400,
  // Threshold of if use single hover layer to optimize.
  // It is recommended that `hoverLayerThreshold` is equivalent to or less than
  // `progressiveThreshold`, otherwise hover will cause restart of progressive,
  // which is unexpected.
  // see example <echarts/test/heatmap-large.html>.
  hoverLayerThreshold: 3000,
  // See: module:echarts/scale/Time
  useUTC: false
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvZ2xvYmFsRGVmYXVsdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL21vZGVsL2dsb2JhbERlZmF1bHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBwbGF0Zm9ybSA9ICcnOyAvLyBOYXZpZ2F0b3Igbm90IGV4aXN0cyBpbiBub2RlXG5cbmlmICh0eXBlb2YgbmF2aWdhdG9yICE9PSAndW5kZWZpbmVkJykge1xuICBwbGF0Zm9ybSA9IG5hdmlnYXRvci5wbGF0Zm9ybSB8fCAnJztcbn1cblxudmFyIF9kZWZhdWx0ID0ge1xuICAvLyBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsMCwwLDApJyxcbiAgLy8gaHR0cHM6Ly9kcmliYmJsZS5jb20vc2hvdHMvMTA2NTk2MC1JbmZvZ3JhcGhpYy1QaWUtY2hhcnQtdmlzdWFsaXphdGlvblxuICAvLyBjb2xvcjogWycjNTc5M2YzJywgJyNkMTRhNjEnLCAnI2ZkOWMzNScsICcjNjc1YmJhJywgJyNmZWM0MmMnLCAnI2RkNDQ0NCcsICcjZDRkZjVhJywgJyNjZDQ4NzAnXSxcbiAgLy8gTGlnaHQgY29sb3JzOlxuICAvLyBjb2xvcjogWycjYmNkM2JiJywgJyNlODhmNzAnLCAnI2VkYzFhNScsICcjOWRjNWM4JywgJyNlMWU4YzgnLCAnIzdiN2M2OCcsICcjZTViNWI1JywgJyNmMGI0ODknLCAnIzkyOGVhOCcsICcjYmRhMjlhJ10sXG4gIC8vIGNvbG9yOiBbJyNjYzU2NjQnLCAnIzliZDZlYycsICcjZWE5NDZlJywgJyM4YWNhYWEnLCAnI2YxZWM2NCcsICcjZWU4Njg2JywgJyNhNDhkYzEnLCAnIzVkYTZiYycsICcjYjlkY2FlJ10sXG4gIC8vIERhcmsgY29sb3JzOlxuICBjb2xvcjogWycjYzIzNTMxJywgJyMyZjQ1NTQnLCAnIzYxYTBhOCcsICcjZDQ4MjY1JywgJyM5MWM3YWUnLCAnIzc0OWY4MycsICcjY2E4NjIyJywgJyNiZGEyOWEnLCAnIzZlNzA3NCcsICcjNTQ2NTcwJywgJyNjNGNjZDMnXSxcbiAgZ3JhZGllbnRDb2xvcjogWycjZjZlZmE2JywgJyNkODgyNzMnLCAnI2JmNDQ0YyddLFxuICAvLyBJZiB4QXhpcyBhbmQgeUF4aXMgZGVjbGFyZWQsIGdyaWQgaXMgY3JlYXRlZCBieSBkZWZhdWx0LlxuICAvLyBncmlkOiB7fSxcbiAgdGV4dFN0eWxlOiB7XG4gICAgLy8gY29sb3I6ICcjMDAwJyxcbiAgICAvLyBkZWNvcmF0aW9uOiAnbm9uZScsXG4gICAgLy8gUEVORElOR1xuICAgIGZvbnRGYW1pbHk6IHBsYXRmb3JtLm1hdGNoKC9eV2luLykgPyAnTWljcm9zb2Z0IFlhSGVpJyA6ICdzYW5zLXNlcmlmJyxcbiAgICAvLyBmb250RmFtaWx5OiAnQXJpYWwsIFZlcmRhbmEsIHNhbnMtc2VyaWYnLFxuICAgIGZvbnRTaXplOiAxMixcbiAgICBmb250U3R5bGU6ICdub3JtYWwnLFxuICAgIGZvbnRXZWlnaHQ6ICdub3JtYWwnXG4gIH0sXG4gIC8vIGh0dHA6Ly9ibG9ncy5hZG9iZS5jb20vd2VicGxhdGZvcm0vMjAxNC8wMi8yNC91c2luZy1ibGVuZC1tb2Rlcy1pbi1odG1sLWNhbnZhcy9cbiAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvQVBJL0NhbnZhc1JlbmRlcmluZ0NvbnRleHQyRC9nbG9iYWxDb21wb3NpdGVPcGVyYXRpb25cbiAgLy8gRGVmYXVsdCBpcyBzb3VyY2Utb3ZlclxuICBibGVuZE1vZGU6IG51bGwsXG4gIGFuaW1hdGlvbjogJ2F1dG8nLFxuICBhbmltYXRpb25EdXJhdGlvbjogMTAwMCxcbiAgYW5pbWF0aW9uRHVyYXRpb25VcGRhdGU6IDMwMCxcbiAgYW5pbWF0aW9uRWFzaW5nOiAnZXhwb25lbnRpYWxPdXQnLFxuICBhbmltYXRpb25FYXNpbmdVcGRhdGU6ICdjdWJpY091dCcsXG4gIGFuaW1hdGlvblRocmVzaG9sZDogMjAwMCxcbiAgLy8gQ29uZmlndXJhdGlvbiBmb3IgcHJvZ3Jlc3NpdmUvaW5jcmVtZW50YWwgcmVuZGVyaW5nXG4gIHByb2dyZXNzaXZlVGhyZXNob2xkOiAzMDAwLFxuICBwcm9ncmVzc2l2ZTogNDAwLFxuICAvLyBUaHJlc2hvbGQgb2YgaWYgdXNlIHNpbmdsZSBob3ZlciBsYXllciB0byBvcHRpbWl6ZS5cbiAgLy8gSXQgaXMgcmVjb21tZW5kZWQgdGhhdCBgaG92ZXJMYXllclRocmVzaG9sZGAgaXMgZXF1aXZhbGVudCB0byBvciBsZXNzIHRoYW5cbiAgLy8gYHByb2dyZXNzaXZlVGhyZXNob2xkYCwgb3RoZXJ3aXNlIGhvdmVyIHdpbGwgY2F1c2UgcmVzdGFydCBvZiBwcm9ncmVzc2l2ZSxcbiAgLy8gd2hpY2ggaXMgdW5leHBlY3RlZC5cbiAgLy8gc2VlIGV4YW1wbGUgPGVjaGFydHMvdGVzdC9oZWF0bWFwLWxhcmdlLmh0bWw+LlxuICBob3ZlckxheWVyVGhyZXNob2xkOiAzMDAwLFxuICAvLyBTZWU6IG1vZHVsZTplY2hhcnRzL3NjYWxlL1RpbWVcbiAgdXNlVVRDOiBmYWxzZVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMUNBO0FBNENBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/globalDefault.js
