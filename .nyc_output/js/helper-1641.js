/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var numberUtil = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * For testable.
 */


var roundNumber = numberUtil.round;
/**
 * @param {Array.<number>} extent Both extent[0] and extent[1] should be valid number.
 *                                Should be extent[0] < extent[1].
 * @param {number} splitNumber splitNumber should be >= 1.
 * @param {number} [minInterval]
 * @param {number} [maxInterval]
 * @return {Object} {interval, intervalPrecision, niceTickExtent}
 */

function intervalScaleNiceTicks(extent, splitNumber, minInterval, maxInterval) {
  var result = {};
  var span = extent[1] - extent[0];
  var interval = result.interval = numberUtil.nice(span / splitNumber, true);

  if (minInterval != null && interval < minInterval) {
    interval = result.interval = minInterval;
  }

  if (maxInterval != null && interval > maxInterval) {
    interval = result.interval = maxInterval;
  } // Tow more digital for tick.


  var precision = result.intervalPrecision = getIntervalPrecision(interval); // Niced extent inside original extent

  var niceTickExtent = result.niceTickExtent = [roundNumber(Math.ceil(extent[0] / interval) * interval, precision), roundNumber(Math.floor(extent[1] / interval) * interval, precision)];
  fixExtent(niceTickExtent, extent);
  return result;
}
/**
 * @param {number} interval
 * @return {number} interval precision
 */


function getIntervalPrecision(interval) {
  // Tow more digital for tick.
  return numberUtil.getPrecisionSafe(interval) + 2;
}

function clamp(niceTickExtent, idx, extent) {
  niceTickExtent[idx] = Math.max(Math.min(niceTickExtent[idx], extent[1]), extent[0]);
} // In some cases (e.g., splitNumber is 1), niceTickExtent may be out of extent.


function fixExtent(niceTickExtent, extent) {
  !isFinite(niceTickExtent[0]) && (niceTickExtent[0] = extent[0]);
  !isFinite(niceTickExtent[1]) && (niceTickExtent[1] = extent[1]);
  clamp(niceTickExtent, 0, extent);
  clamp(niceTickExtent, 1, extent);

  if (niceTickExtent[0] > niceTickExtent[1]) {
    niceTickExtent[0] = niceTickExtent[1];
  }
}

function intervalScaleGetTicks(interval, extent, niceTickExtent, intervalPrecision) {
  var ticks = []; // If interval is 0, return [];

  if (!interval) {
    return ticks;
  } // Consider this case: using dataZoom toolbox, zoom and zoom.


  var safeLimit = 10000;

  if (extent[0] < niceTickExtent[0]) {
    ticks.push(extent[0]);
  }

  var tick = niceTickExtent[0];

  while (tick <= niceTickExtent[1]) {
    ticks.push(tick); // Avoid rounding error

    tick = roundNumber(tick + interval, intervalPrecision);

    if (tick === ticks[ticks.length - 1]) {
      // Consider out of safe float point, e.g.,
      // -3711126.9907707 + 2e-10 === -3711126.9907707
      break;
    }

    if (ticks.length > safeLimit) {
      return [];
    }
  } // Consider this case: the last item of ticks is smaller
  // than niceTickExtent[1] and niceTickExtent[1] === extent[1].


  if (extent[1] > (ticks.length ? ticks[ticks.length - 1] : niceTickExtent[1])) {
    ticks.push(extent[1]);
  }

  return ticks;
}

exports.intervalScaleNiceTicks = intervalScaleNiceTicks;
exports.getIntervalPrecision = getIntervalPrecision;
exports.fixExtent = fixExtent;
exports.intervalScaleGetTicks = intervalScaleGetTicks;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvaGVscGVyLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc2NhbGUvaGVscGVyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgbnVtYmVyVXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL251bWJlclwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEZvciB0ZXN0YWJsZS5cbiAqL1xudmFyIHJvdW5kTnVtYmVyID0gbnVtYmVyVXRpbC5yb3VuZDtcbi8qKlxuICogQHBhcmFtIHtBcnJheS48bnVtYmVyPn0gZXh0ZW50IEJvdGggZXh0ZW50WzBdIGFuZCBleHRlbnRbMV0gc2hvdWxkIGJlIHZhbGlkIG51bWJlci5cbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBTaG91bGQgYmUgZXh0ZW50WzBdIDwgZXh0ZW50WzFdLlxuICogQHBhcmFtIHtudW1iZXJ9IHNwbGl0TnVtYmVyIHNwbGl0TnVtYmVyIHNob3VsZCBiZSA+PSAxLlxuICogQHBhcmFtIHtudW1iZXJ9IFttaW5JbnRlcnZhbF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbbWF4SW50ZXJ2YWxdXG4gKiBAcmV0dXJuIHtPYmplY3R9IHtpbnRlcnZhbCwgaW50ZXJ2YWxQcmVjaXNpb24sIG5pY2VUaWNrRXh0ZW50fVxuICovXG5cbmZ1bmN0aW9uIGludGVydmFsU2NhbGVOaWNlVGlja3MoZXh0ZW50LCBzcGxpdE51bWJlciwgbWluSW50ZXJ2YWwsIG1heEludGVydmFsKSB7XG4gIHZhciByZXN1bHQgPSB7fTtcbiAgdmFyIHNwYW4gPSBleHRlbnRbMV0gLSBleHRlbnRbMF07XG4gIHZhciBpbnRlcnZhbCA9IHJlc3VsdC5pbnRlcnZhbCA9IG51bWJlclV0aWwubmljZShzcGFuIC8gc3BsaXROdW1iZXIsIHRydWUpO1xuXG4gIGlmIChtaW5JbnRlcnZhbCAhPSBudWxsICYmIGludGVydmFsIDwgbWluSW50ZXJ2YWwpIHtcbiAgICBpbnRlcnZhbCA9IHJlc3VsdC5pbnRlcnZhbCA9IG1pbkludGVydmFsO1xuICB9XG5cbiAgaWYgKG1heEludGVydmFsICE9IG51bGwgJiYgaW50ZXJ2YWwgPiBtYXhJbnRlcnZhbCkge1xuICAgIGludGVydmFsID0gcmVzdWx0LmludGVydmFsID0gbWF4SW50ZXJ2YWw7XG4gIH0gLy8gVG93IG1vcmUgZGlnaXRhbCBmb3IgdGljay5cblxuXG4gIHZhciBwcmVjaXNpb24gPSByZXN1bHQuaW50ZXJ2YWxQcmVjaXNpb24gPSBnZXRJbnRlcnZhbFByZWNpc2lvbihpbnRlcnZhbCk7IC8vIE5pY2VkIGV4dGVudCBpbnNpZGUgb3JpZ2luYWwgZXh0ZW50XG5cbiAgdmFyIG5pY2VUaWNrRXh0ZW50ID0gcmVzdWx0Lm5pY2VUaWNrRXh0ZW50ID0gW3JvdW5kTnVtYmVyKE1hdGguY2VpbChleHRlbnRbMF0gLyBpbnRlcnZhbCkgKiBpbnRlcnZhbCwgcHJlY2lzaW9uKSwgcm91bmROdW1iZXIoTWF0aC5mbG9vcihleHRlbnRbMV0gLyBpbnRlcnZhbCkgKiBpbnRlcnZhbCwgcHJlY2lzaW9uKV07XG4gIGZpeEV4dGVudChuaWNlVGlja0V4dGVudCwgZXh0ZW50KTtcbiAgcmV0dXJuIHJlc3VsdDtcbn1cbi8qKlxuICogQHBhcmFtIHtudW1iZXJ9IGludGVydmFsXG4gKiBAcmV0dXJuIHtudW1iZXJ9IGludGVydmFsIHByZWNpc2lvblxuICovXG5cblxuZnVuY3Rpb24gZ2V0SW50ZXJ2YWxQcmVjaXNpb24oaW50ZXJ2YWwpIHtcbiAgLy8gVG93IG1vcmUgZGlnaXRhbCBmb3IgdGljay5cbiAgcmV0dXJuIG51bWJlclV0aWwuZ2V0UHJlY2lzaW9uU2FmZShpbnRlcnZhbCkgKyAyO1xufVxuXG5mdW5jdGlvbiBjbGFtcChuaWNlVGlja0V4dGVudCwgaWR4LCBleHRlbnQpIHtcbiAgbmljZVRpY2tFeHRlbnRbaWR4XSA9IE1hdGgubWF4KE1hdGgubWluKG5pY2VUaWNrRXh0ZW50W2lkeF0sIGV4dGVudFsxXSksIGV4dGVudFswXSk7XG59IC8vIEluIHNvbWUgY2FzZXMgKGUuZy4sIHNwbGl0TnVtYmVyIGlzIDEpLCBuaWNlVGlja0V4dGVudCBtYXkgYmUgb3V0IG9mIGV4dGVudC5cblxuXG5mdW5jdGlvbiBmaXhFeHRlbnQobmljZVRpY2tFeHRlbnQsIGV4dGVudCkge1xuICAhaXNGaW5pdGUobmljZVRpY2tFeHRlbnRbMF0pICYmIChuaWNlVGlja0V4dGVudFswXSA9IGV4dGVudFswXSk7XG4gICFpc0Zpbml0ZShuaWNlVGlja0V4dGVudFsxXSkgJiYgKG5pY2VUaWNrRXh0ZW50WzFdID0gZXh0ZW50WzFdKTtcbiAgY2xhbXAobmljZVRpY2tFeHRlbnQsIDAsIGV4dGVudCk7XG4gIGNsYW1wKG5pY2VUaWNrRXh0ZW50LCAxLCBleHRlbnQpO1xuXG4gIGlmIChuaWNlVGlja0V4dGVudFswXSA+IG5pY2VUaWNrRXh0ZW50WzFdKSB7XG4gICAgbmljZVRpY2tFeHRlbnRbMF0gPSBuaWNlVGlja0V4dGVudFsxXTtcbiAgfVxufVxuXG5mdW5jdGlvbiBpbnRlcnZhbFNjYWxlR2V0VGlja3MoaW50ZXJ2YWwsIGV4dGVudCwgbmljZVRpY2tFeHRlbnQsIGludGVydmFsUHJlY2lzaW9uKSB7XG4gIHZhciB0aWNrcyA9IFtdOyAvLyBJZiBpbnRlcnZhbCBpcyAwLCByZXR1cm4gW107XG5cbiAgaWYgKCFpbnRlcnZhbCkge1xuICAgIHJldHVybiB0aWNrcztcbiAgfSAvLyBDb25zaWRlciB0aGlzIGNhc2U6IHVzaW5nIGRhdGFab29tIHRvb2xib3gsIHpvb20gYW5kIHpvb20uXG5cblxuICB2YXIgc2FmZUxpbWl0ID0gMTAwMDA7XG5cbiAgaWYgKGV4dGVudFswXSA8IG5pY2VUaWNrRXh0ZW50WzBdKSB7XG4gICAgdGlja3MucHVzaChleHRlbnRbMF0pO1xuICB9XG5cbiAgdmFyIHRpY2sgPSBuaWNlVGlja0V4dGVudFswXTtcblxuICB3aGlsZSAodGljayA8PSBuaWNlVGlja0V4dGVudFsxXSkge1xuICAgIHRpY2tzLnB1c2godGljayk7IC8vIEF2b2lkIHJvdW5kaW5nIGVycm9yXG5cbiAgICB0aWNrID0gcm91bmROdW1iZXIodGljayArIGludGVydmFsLCBpbnRlcnZhbFByZWNpc2lvbik7XG5cbiAgICBpZiAodGljayA9PT0gdGlja3NbdGlja3MubGVuZ3RoIC0gMV0pIHtcbiAgICAgIC8vIENvbnNpZGVyIG91dCBvZiBzYWZlIGZsb2F0IHBvaW50LCBlLmcuLFxuICAgICAgLy8gLTM3MTExMjYuOTkwNzcwNyArIDJlLTEwID09PSAtMzcxMTEyNi45OTA3NzA3XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBpZiAodGlja3MubGVuZ3RoID4gc2FmZUxpbWl0KSB7XG4gICAgICByZXR1cm4gW107XG4gICAgfVxuICB9IC8vIENvbnNpZGVyIHRoaXMgY2FzZTogdGhlIGxhc3QgaXRlbSBvZiB0aWNrcyBpcyBzbWFsbGVyXG4gIC8vIHRoYW4gbmljZVRpY2tFeHRlbnRbMV0gYW5kIG5pY2VUaWNrRXh0ZW50WzFdID09PSBleHRlbnRbMV0uXG5cblxuICBpZiAoZXh0ZW50WzFdID4gKHRpY2tzLmxlbmd0aCA/IHRpY2tzW3RpY2tzLmxlbmd0aCAtIDFdIDogbmljZVRpY2tFeHRlbnRbMV0pKSB7XG4gICAgdGlja3MucHVzaChleHRlbnRbMV0pO1xuICB9XG5cbiAgcmV0dXJuIHRpY2tzO1xufVxuXG5leHBvcnRzLmludGVydmFsU2NhbGVOaWNlVGlja3MgPSBpbnRlcnZhbFNjYWxlTmljZVRpY2tzO1xuZXhwb3J0cy5nZXRJbnRlcnZhbFByZWNpc2lvbiA9IGdldEludGVydmFsUHJlY2lzaW9uO1xuZXhwb3J0cy5maXhFeHRlbnQgPSBmaXhFeHRlbnQ7XG5leHBvcnRzLmludGVydmFsU2NhbGVHZXRUaWNrcyA9IGludGVydmFsU2NhbGVHZXRUaWNrczsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7QUFHQTtBQUNBOzs7Ozs7Ozs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/scale/helper.js
