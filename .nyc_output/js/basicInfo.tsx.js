__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/basicInfo.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}






var PAYMENT_METHOD_RECURRING = _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].PAYMENT_METHOD_RECURRING;
var formViewItemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 19
    },
    sm: {
      span: 13
    }
  }
};

var BasicInfo =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(BasicInfo, _ModelWidget);

  function BasicInfo(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, BasicInfo);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(BasicInfo).call(this, props, context));

    _this.getPremium = function () {
      var currencyCode = _this.getValueFromModel("currencyCode");

      var policy = {
        openEnd: _this.getValueFromModel("openEnd"),
        recurringPayment: _this.getValueFromModel("recurringPayment"),
        policyPremium: _this.getValueFromModel("policyPremium"),
        currencyCode: currencyCode,
        premiumDesc: _this.getValueFromModel("premiumDesc")
      };

      var prems = _this.getPolicyPremium();

      var vat = "VAT ".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Utils"].formatCurrency(prems.gstVat, currencyCode));
      var stampDuty = "Stamp Duty ".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Utils"].formatCurrency(prems.stampDuty, currencyCode));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          marginRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].renderPolicyPremium(policy)), [vat, stampDuty].join(", "));
    };

    _this.getProductAndPlan = function () {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, "".concat(_this.getValueFromModel("productName")).concat(_this.getValueFromModel("planTypeName") ? ", " + _this.getValueFromModel("planTypeName") : "").concat(_this.getValueFromModel("planName") ? ", " + _this.getValueFromModel("planName") : ""));
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(BasicInfo, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        BasicInfo: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(BasicInfo.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "getPolicyPremium",
    value: function getPolicyPremium() {
      var paymentSchedule = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var policy = {
        policyPremium: this.getValueFromModel("policyPremium")
      };
      var lumpsum = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getValueFromJSON(policy, "policyPremium.lumpsum") || {};
      var subsequent = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getValueFromJSON(policy, "policyPremium.subsequent") || {};

      if (this.getValueFromModel("openEnd") || this.getValueFromModel("recurringPayment") || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.BasicInfo, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_11__["default"], {
        layout: formViewItemLayout,
        title: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Policy NO.").thai("หมายเลขอ้างอิง").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "headerMiddle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        },
        __self: this
      }, this.getValueFromModel("policyNo") || this.getValueFromModel("quoteNo"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "boardText",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }, this.getValueFromModel("bizType")), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "headerStatus",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, this.getValueFromModel("statusName")))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_11__["default"], {
        layout: formViewItemLayout,
        title: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Product & Plan").thai("ผลิตภัณฑ์ & แบบประกันภัย").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, this.getProductAndPlan()), this.props.isPremium && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_11__["default"], {
        title: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Premium").thai("เบี้ยประกันภัย").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, this.getPremium()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_11__["default"], {
        layout: formViewItemLayout,
        title: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("agent").thai("ตัวแทน").my("အကျိုးဆောင်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, this.getValueFromModel("scName")));
    }
  }]);

  return BasicInfo;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (BasicInfo);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2Jhc2ljSW5mby50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvYmFzaWNJbmZvLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBDb25zdHMsIExhbmd1YWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgVmlld0l0ZW0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC92aWV3LWl0ZW1cIjtcblxuY29uc3QgeyBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkcgfSA9IENvbnN0cztcbmNvbnN0IGZvcm1WaWV3SXRlbUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTkgfSxcbiAgICBzbTogeyBzcGFuOiAxMyB9LFxuICB9LFxufTtcbmV4cG9ydCB0eXBlIEJhc2ljSW5mb1Byb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBpc1ByZW1pdW0/OiBib29sZWFuO1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIEJhc2ljSW5mb1N0YXRlID0ge307XG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgQmFzaWNJbmZvQ29tcG9uZW50cyA9IHtcbiAgQmFzaWNJbmZvOiBTdHlsZWRESVY7XG59O1xuXG5jbGFzcyBCYXNpY0luZm88UCBleHRlbmRzIEJhc2ljSW5mb1Byb3BzLCBTIGV4dGVuZHMgQmFzaWNJbmZvU3RhdGUsIEMgZXh0ZW5kcyBCYXNpY0luZm9Db21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IEJhc2ljSW5mb1Byb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCYXNpY0luZm86IFN0eWxlZC5kaXZgXG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHt9KSBhcyBTO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRQb2xpY3lQcmVtaXVtKHBheW1lbnRTY2hlZHVsZSA9IG51bGwpIHtcbiAgICBsZXQgcG9saWN5ID0geyBwb2xpY3lQcmVtaXVtOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicG9saWN5UHJlbWl1bVwiKSB9O1xuICAgIGxldCBsdW1wc3VtID0gVXRpbHMuZ2V0VmFsdWVGcm9tSlNPTihwb2xpY3ksIFwicG9saWN5UHJlbWl1bS5sdW1wc3VtXCIpIHx8IHt9O1xuICAgIGxldCBzdWJzZXF1ZW50ID0gVXRpbHMuZ2V0VmFsdWVGcm9tSlNPTihwb2xpY3ksIFwicG9saWN5UHJlbWl1bS5zdWJzZXF1ZW50XCIpIHx8IHt9O1xuICAgIGlmIChcbiAgICAgIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJvcGVuRW5kXCIpIHx8XG4gICAgICB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicmVjdXJyaW5nUGF5bWVudFwiKSB8fFxuICAgICAgcGF5bWVudFNjaGVkdWxlID09PSBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkdcbiAgICApIHtcbiAgICAgIHJldHVybiBzdWJzZXF1ZW50O1xuICAgIH1cbiAgICByZXR1cm4gbHVtcHN1bTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0UHJlbWl1bSA9ICgpID0+IHtcbiAgICBjb25zdCBjdXJyZW5jeUNvZGUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiY3VycmVuY3lDb2RlXCIpO1xuICAgIGxldCBwb2xpY3kgPSB7XG4gICAgICBvcGVuRW5kOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwib3BlbkVuZFwiKSxcbiAgICAgIHJlY3VycmluZ1BheW1lbnQ6IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJyZWN1cnJpbmdQYXltZW50XCIpLFxuICAgICAgcG9saWN5UHJlbWl1bTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInBvbGljeVByZW1pdW1cIiksXG4gICAgICBjdXJyZW5jeUNvZGUsXG4gICAgICBwcmVtaXVtRGVzYzogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChcInByZW1pdW1EZXNjXCIpLFxuICAgIH07XG4gICAgY29uc3QgcHJlbXMgPSB0aGlzLmdldFBvbGljeVByZW1pdW0oKTtcbiAgICBjb25zdCB2YXQgPSBgVkFUICR7VXRpbHMuZm9ybWF0Q3VycmVuY3kocHJlbXMuZ3N0VmF0LCBjdXJyZW5jeUNvZGUpfWA7XG4gICAgY29uc3Qgc3RhbXBEdXR5ID0gYFN0YW1wIER1dHkgJHtVdGlscy5mb3JtYXRDdXJyZW5jeShwcmVtcy5zdGFtcER1dHksIGN1cnJlbmN5Q29kZSl9YDtcbiAgICByZXR1cm4gKFxuICAgICAgPHNwYW4+XG4gICAgICAgIDxzcGFuIHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjVweFwiIH19PntVdGlscy5yZW5kZXJQb2xpY3lQcmVtaXVtKHBvbGljeSl9PC9zcGFuPlxuICAgICAgICB7W3ZhdCwgc3RhbXBEdXR5XS5qb2luKFwiLCBcIil9XG4gICAgICA8L3NwYW4+XG4gICAgKTtcbiAgfTtcblxuICBwcml2YXRlIGdldFByb2R1Y3RBbmRQbGFuID0gKCkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICA8c3Bhbj5cbiAgICAgICAge2Ake3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwcm9kdWN0TmFtZVwiKX0ke1xuICAgICAgICAgIHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwbGFuVHlwZU5hbWVcIikgPyBcIiwgXCIgKyB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicGxhblR5cGVOYW1lXCIpIDogXCJcIlxuICAgICAgICB9JHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicGxhbk5hbWVcIikgPyBcIiwgXCIgKyB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicGxhbk5hbWVcIikgOiBcIlwifWB9XG4gICAgICA8L3NwYW4+XG4gICAgKTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5CYXNpY0luZm8+XG4gICAgICAgIDxWaWV3SXRlbVxuICAgICAgICAgIGxheW91dD17Zm9ybVZpZXdJdGVtTGF5b3V0fVxuICAgICAgICAgIHRpdGxlPXtMYW5ndWFnZS5lbihcIlBvbGljeSBOTy5cIikudGhhaShcIuC4q+C4oeC4suC4ouC5gOC4peC4guC4reC5ieC4suC4h+C4reC4tOC4h1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlck1pZGRsZVwiPlxuICAgICAgICAgICAge3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwoXCJwb2xpY3lOb1wiKSB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwicXVvdGVOb1wiKX1cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImJvYXJkVGV4dFwiPnt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwiYml6VHlwZVwiKX08L3NwYW4+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlclN0YXR1c1wiPnt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwic3RhdHVzTmFtZVwiKX08L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9WaWV3SXRlbT5cbiAgICAgICAgPFZpZXdJdGVtXG4gICAgICAgICAgbGF5b3V0PXtmb3JtVmlld0l0ZW1MYXlvdXR9XG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiUHJvZHVjdCAmIFBsYW5cIikudGhhaShcIuC4nOC4peC4tOC4leC4oOC4seC4k+C4keC5jCAmIOC5geC4muC4muC4m+C4o+C4sOC4geC4seC4meC4oOC4seC4olwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5nZXRQcm9kdWN0QW5kUGxhbigpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICB7dGhpcy5wcm9wcy5pc1ByZW1pdW0gJiYgPFZpZXdJdGVtXG4gICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiUHJlbWl1bVwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguYDguJrguLXguYnguKLguJvguKPguLDguIHguLHguJnguKDguLHguKJcIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgID5cbiAgICAgICAgICB7dGhpcy5nZXRQcmVtaXVtKCl9XG4gICAgICAgIDwvVmlld0l0ZW0+fVxuICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICBsYXlvdXQ9e2Zvcm1WaWV3SXRlbUxheW91dH1cbiAgICAgICAgICB0aXRsZT17TGFuZ3VhZ2UuZW4oXCJhZ2VudFwiKS50aGFpKFwi4LiV4Lix4Lin4LmB4LiX4LiZXCIpLm15KFwi4YCh4YCA4YC74YCt4YCv4YC44YCG4YCx4YCs4YCE4YC6XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPlxuICAgICAgICAgIHt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKFwic2NOYW1lXCIpfVxuICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgPC9DLkJhc2ljSW5mbz5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEJhc2ljSW5mbztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFDQTtBQXFCQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBOEJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFoREE7QUFrREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBMURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFnQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7OztBQS9GQTtBQUNBO0FBaUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/basicInfo.tsx
