/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var contrastColor = '#eee';

var axisCommon = function axisCommon() {
  return {
    axisLine: {
      lineStyle: {
        color: contrastColor
      }
    },
    axisTick: {
      lineStyle: {
        color: contrastColor
      }
    },
    axisLabel: {
      textStyle: {
        color: contrastColor
      }
    },
    splitLine: {
      lineStyle: {
        type: 'dashed',
        color: '#aaa'
      }
    },
    splitArea: {
      areaStyle: {
        color: contrastColor
      }
    }
  };
};

var colorPalette = ['#dd6b66', '#759aa0', '#e69d87', '#8dc1a9', '#ea7e53', '#eedd78', '#73a373', '#73b9bc', '#7289ab', '#91ca8c', '#f49f42'];
var theme = {
  color: colorPalette,
  backgroundColor: '#333',
  tooltip: {
    axisPointer: {
      lineStyle: {
        color: contrastColor
      },
      crossStyle: {
        color: contrastColor
      }
    }
  },
  legend: {
    textStyle: {
      color: contrastColor
    }
  },
  textStyle: {
    color: contrastColor
  },
  title: {
    textStyle: {
      color: contrastColor
    }
  },
  toolbox: {
    iconStyle: {
      normal: {
        borderColor: contrastColor
      }
    }
  },
  dataZoom: {
    textStyle: {
      color: contrastColor
    }
  },
  visualMap: {
    textStyle: {
      color: contrastColor
    }
  },
  timeline: {
    lineStyle: {
      color: contrastColor
    },
    itemStyle: {
      normal: {
        color: colorPalette[1]
      }
    },
    label: {
      normal: {
        textStyle: {
          color: contrastColor
        }
      }
    },
    controlStyle: {
      normal: {
        color: contrastColor,
        borderColor: contrastColor
      }
    }
  },
  timeAxis: axisCommon(),
  logAxis: axisCommon(),
  valueAxis: axisCommon(),
  categoryAxis: axisCommon(),
  line: {
    symbol: 'circle'
  },
  graph: {
    color: colorPalette
  },
  gauge: {
    title: {
      textStyle: {
        color: contrastColor
      }
    }
  },
  candlestick: {
    itemStyle: {
      normal: {
        color: '#FD1050',
        color0: '#0CF49B',
        borderColor: '#FD1050',
        borderColor0: '#0CF49B'
      }
    }
  }
};
theme.categoryAxis.splitLine.show = false;
var _default = theme;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdGhlbWUvZGFyay5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3RoZW1lL2RhcmsuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBjb250cmFzdENvbG9yID0gJyNlZWUnO1xuXG52YXIgYXhpc0NvbW1vbiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHtcbiAgICBheGlzTGluZToge1xuICAgICAgbGluZVN0eWxlOiB7XG4gICAgICAgIGNvbG9yOiBjb250cmFzdENvbG9yXG4gICAgICB9XG4gICAgfSxcbiAgICBheGlzVGljazoge1xuICAgICAgbGluZVN0eWxlOiB7XG4gICAgICAgIGNvbG9yOiBjb250cmFzdENvbG9yXG4gICAgICB9XG4gICAgfSxcbiAgICBheGlzTGFiZWw6IHtcbiAgICAgIHRleHRTdHlsZToge1xuICAgICAgICBjb2xvcjogY29udHJhc3RDb2xvclxuICAgICAgfVxuICAgIH0sXG4gICAgc3BsaXRMaW5lOiB7XG4gICAgICBsaW5lU3R5bGU6IHtcbiAgICAgICAgdHlwZTogJ2Rhc2hlZCcsXG4gICAgICAgIGNvbG9yOiAnI2FhYSdcbiAgICAgIH1cbiAgICB9LFxuICAgIHNwbGl0QXJlYToge1xuICAgICAgYXJlYVN0eWxlOiB7XG4gICAgICAgIGNvbG9yOiBjb250cmFzdENvbG9yXG4gICAgICB9XG4gICAgfVxuICB9O1xufTtcblxudmFyIGNvbG9yUGFsZXR0ZSA9IFsnI2RkNmI2NicsICcjNzU5YWEwJywgJyNlNjlkODcnLCAnIzhkYzFhOScsICcjZWE3ZTUzJywgJyNlZWRkNzgnLCAnIzczYTM3MycsICcjNzNiOWJjJywgJyM3Mjg5YWInLCAnIzkxY2E4YycsICcjZjQ5ZjQyJ107XG52YXIgdGhlbWUgPSB7XG4gIGNvbG9yOiBjb2xvclBhbGV0dGUsXG4gIGJhY2tncm91bmRDb2xvcjogJyMzMzMnLFxuICB0b29sdGlwOiB7XG4gICAgYXhpc1BvaW50ZXI6IHtcbiAgICAgIGxpbmVTdHlsZToge1xuICAgICAgICBjb2xvcjogY29udHJhc3RDb2xvclxuICAgICAgfSxcbiAgICAgIGNyb3NzU3R5bGU6IHtcbiAgICAgICAgY29sb3I6IGNvbnRyYXN0Q29sb3JcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIGxlZ2VuZDoge1xuICAgIHRleHRTdHlsZToge1xuICAgICAgY29sb3I6IGNvbnRyYXN0Q29sb3JcbiAgICB9XG4gIH0sXG4gIHRleHRTdHlsZToge1xuICAgIGNvbG9yOiBjb250cmFzdENvbG9yXG4gIH0sXG4gIHRpdGxlOiB7XG4gICAgdGV4dFN0eWxlOiB7XG4gICAgICBjb2xvcjogY29udHJhc3RDb2xvclxuICAgIH1cbiAgfSxcbiAgdG9vbGJveDoge1xuICAgIGljb25TdHlsZToge1xuICAgICAgbm9ybWFsOiB7XG4gICAgICAgIGJvcmRlckNvbG9yOiBjb250cmFzdENvbG9yXG4gICAgICB9XG4gICAgfVxuICB9LFxuICBkYXRhWm9vbToge1xuICAgIHRleHRTdHlsZToge1xuICAgICAgY29sb3I6IGNvbnRyYXN0Q29sb3JcbiAgICB9XG4gIH0sXG4gIHZpc3VhbE1hcDoge1xuICAgIHRleHRTdHlsZToge1xuICAgICAgY29sb3I6IGNvbnRyYXN0Q29sb3JcbiAgICB9XG4gIH0sXG4gIHRpbWVsaW5lOiB7XG4gICAgbGluZVN0eWxlOiB7XG4gICAgICBjb2xvcjogY29udHJhc3RDb2xvclxuICAgIH0sXG4gICAgaXRlbVN0eWxlOiB7XG4gICAgICBub3JtYWw6IHtcbiAgICAgICAgY29sb3I6IGNvbG9yUGFsZXR0ZVsxXVxuICAgICAgfVxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIG5vcm1hbDoge1xuICAgICAgICB0ZXh0U3R5bGU6IHtcbiAgICAgICAgICBjb2xvcjogY29udHJhc3RDb2xvclxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcbiAgICBjb250cm9sU3R5bGU6IHtcbiAgICAgIG5vcm1hbDoge1xuICAgICAgICBjb2xvcjogY29udHJhc3RDb2xvcixcbiAgICAgICAgYm9yZGVyQ29sb3I6IGNvbnRyYXN0Q29sb3JcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIHRpbWVBeGlzOiBheGlzQ29tbW9uKCksXG4gIGxvZ0F4aXM6IGF4aXNDb21tb24oKSxcbiAgdmFsdWVBeGlzOiBheGlzQ29tbW9uKCksXG4gIGNhdGVnb3J5QXhpczogYXhpc0NvbW1vbigpLFxuICBsaW5lOiB7XG4gICAgc3ltYm9sOiAnY2lyY2xlJ1xuICB9LFxuICBncmFwaDoge1xuICAgIGNvbG9yOiBjb2xvclBhbGV0dGVcbiAgfSxcbiAgZ2F1Z2U6IHtcbiAgICB0aXRsZToge1xuICAgICAgdGV4dFN0eWxlOiB7XG4gICAgICAgIGNvbG9yOiBjb250cmFzdENvbG9yXG4gICAgICB9XG4gICAgfVxuICB9LFxuICBjYW5kbGVzdGljazoge1xuICAgIGl0ZW1TdHlsZToge1xuICAgICAgbm9ybWFsOiB7XG4gICAgICAgIGNvbG9yOiAnI0ZEMTA1MCcsXG4gICAgICAgIGNvbG9yMDogJyMwQ0Y0OUInLFxuICAgICAgICBib3JkZXJDb2xvcjogJyNGRDEwNTAnLFxuICAgICAgICBib3JkZXJDb2xvcjA6ICcjMENGNDlCJ1xuICAgICAgfVxuICAgIH1cbiAgfVxufTtcbnRoZW1lLmNhdGVnb3J5QXhpcy5zcGxpdExpbmUuc2hvdyA9IGZhbHNlO1xudmFyIF9kZWZhdWx0ID0gdGhlbWU7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQURBO0FBREE7QUF0QkE7QUE0QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUpBO0FBREE7QUFVQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFEQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQWhCQTtBQXVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQURBO0FBREE7QUFuRkE7QUE4RkE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/theme/dark.js
