var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var polyHelper = __webpack_require__(/*! ../helper/poly */ "./node_modules/zrender/lib/graphic/helper/poly.js");
/**
 * @module zrender/graphic/shape/Polyline
 */


var _default = Path.extend({
  type: 'polyline',
  shape: {
    points: null,
    smooth: false,
    smoothConstraint: null
  },
  style: {
    stroke: '#000',
    fill: null
  },
  buildPath: function buildPath(ctx, shape) {
    polyHelper.buildPath(ctx, shape, false);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9Qb2x5bGluZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvc2hhcGUvUG9seWxpbmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIFBhdGggPSByZXF1aXJlKFwiLi4vUGF0aFwiKTtcblxudmFyIHBvbHlIZWxwZXIgPSByZXF1aXJlKFwiLi4vaGVscGVyL3BvbHlcIik7XG5cbi8qKlxuICogQG1vZHVsZSB6cmVuZGVyL2dyYXBoaWMvc2hhcGUvUG9seWxpbmVcbiAqL1xudmFyIF9kZWZhdWx0ID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAncG9seWxpbmUnLFxuICBzaGFwZToge1xuICAgIHBvaW50czogbnVsbCxcbiAgICBzbW9vdGg6IGZhbHNlLFxuICAgIHNtb290aENvbnN0cmFpbnQ6IG51bGxcbiAgfSxcbiAgc3R5bGU6IHtcbiAgICBzdHJva2U6ICcjMDAwJyxcbiAgICBmaWxsOiBudWxsXG4gIH0sXG4gIGJ1aWxkUGF0aDogZnVuY3Rpb24gKGN0eCwgc2hhcGUpIHtcbiAgICBwb2x5SGVscGVyLmJ1aWxkUGF0aChjdHgsIHNoYXBlLCBmYWxzZSk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQWJBO0FBQ0E7QUFlQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Polyline.js
