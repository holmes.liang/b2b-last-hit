__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/address.tsx";

function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        cursor: pointer;\n        padding: 0 6px;\n        font-size: 16px;\n        line-height: 42px\n\n      &:hover, &[data-highlight=true] {\n        background: ", ";\n        color: #fff\n      }\n      "]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        max-height: 360px;\n        min-height: 200px;\n        overflow: auto;\n        list-style: none;\n        margin-left: 0;\n        padding-left: 0;\n        font-size: 16px;\n        margin-bottom: 0;\n      "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      &[data-full=false] {\n        border: 1px solid @error-color;\n      }\n      "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      position: absolute;\n      left: 0;\n      top: 43px;\n      width: 100%;\n      border: 1px solid #ebebeb;\n      background: #fff;\n      z-index: 100;\n      margin-bottom: 20px;\n      a {\n        font-size: 16px;\n      }\n      .ant-spin-nested-loading {\n          position: relative;\n          width: calc(100% - 2px);\n          z-index: 100;\n          border: 0;\n      }\n      "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      width: 100%;\n      position: relative;\n        .ant-spin-nested-loading{\n            position: absolute;\n            left: 2px;\n            top: 1px;\n            border: 1px solid #ebebeb;\n            background: #fff;\n            z-index: 100;\n            margin-bottom: 1px;\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_12__["default"].getTheme(),
    HIGHLIGHT_COLOR = _Theme$getTheme.HIGHLIGHT_COLOR;

var Address =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Address, _Widget);

  function Address(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Address);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Address).call(this, props, state));
    _this.initialValues = [];

    _this.clearAddress = function () {
      _this.setState({
        visible: !_this.state.visible
      });

      _this.props.onChange && _this.props.onChange({});
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Address, [{
    key: "initState",
    value: function initState() {
      this.initialValues = this.getInitValues(this.props.selectValue);
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Address.prototype), "initState", this).call(this), {
        visible: false,
        loading: true,
        error: null,
        list: [],
        values: this.initialValues,
        index: this.initialValues.length > 0 ? this.initialValues.length - 1 : 0,
        showLabel: this.getShowLabel(this.props.selectValue)
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject()),
        Dropdown: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject2()),
        Input: Object(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"])(antd__WEBPACK_IMPORTED_MODULE_11__["Input"])(_templateObject3()),
        Ul: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].ul(_templateObject4()),
        Li: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].li(_templateObject5(), HIGHLIGHT_COLOR)
      };
    }
  }, {
    key: "getInitValues",
    value: function getInitValues(modal) {
      var values = [];

      if (modal) {
        // 从k1开始, 最多到k4
        for (var i = 1; i <= 4; i++) {
          if (modal["k".concat(i)]) {
            values.push({
              id: modal["k".concat(i)],
              text: modal["k".concat(i, "name")]
            });
          }
        }
      }

      return values;
    }
  }, {
    key: "getShowLabel",
    value: function getShowLabel(modal) {
      var ltr = this.props.ltr;
      var showLabel = [];

      if (modal) {
        // 从k1开始, 最多到k4
        for (var i = 1; i <= 4; i++) {
          if (modal["k".concat(i, "name")]) {
            showLabel.push(modal["k".concat(i, "name")]);
          }
        }
      }

      if (!ltr) {
        showLabel.reverse();
      }

      return showLabel.join(", ");
    }
  }, {
    key: "setLoading",
    value: function setLoading() {
      this.setState({
        loading: true,
        error: null
      });
    }
  }, {
    key: "setError",
    value: function setError(err) {
      this.setState({
        loading: false,
        error: err,
        list: []
      });
    }
  }, {
    key: "setData",
    value: function setData(list) {
      this.setState({
        loading: false,
        error: null,
        list: list
      });
    }
  }, {
    key: "setDataFinished",
    value: function setDataFinished(modal) {
      var showLabel = this.getShowLabel(modal);
      this.setState({
        loading: false,
        visible: false,
        index: this.state.index - 1,
        showLabel: showLabel
      });
    }
  }, {
    key: "isHighlight",
    value: function isHighlight(id) {
      var _this$state = this.state,
          values = _this$state.values,
          index = _this$state.index;
      var value = values[index];
      return value && value.id === id;
    }
  }, {
    key: "getVmodesData",
    value: function getVmodesData(values) {
      var _this2 = this;

      this.setLoading();
      var level = values.length + 1; // todo: 地址   SGP THA MMR

      var params = {
        itntCode: "PF_THAI",
        countryCode: this.props.countryCode || "THA",
        level: level
      };
      (values || []).forEach(function (item, index) {
        params["k".concat(index + 1)] = item.id;
      });
      var paramsStr = "";
      Object.entries(params).map(function (every, index) {
        paramsStr += "".concat(every[0], "=").concat(every[1]).concat(index === Object.entries(params).length - 1 ? "" : "&");
      });
      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post("/addressesq?".concat(paramsStr), {}, {
        ignoreAuth: false
      }).then(function (response) {
        var respData = response.body.respData;

        if (!respData.leaf) {
          _this2.setData(respData.keys);
        } else {
          _this2.setDataFinished(respData.address);

          _this2.props.onChange && _this2.props.onChange(respData.address);
        }
      }).catch(function (error) {
        _this2.setError(error);
      });
    }
  }, {
    key: "handleSelect",
    value: function handleSelect() {
      this.getVmodesData(this.state.values.slice(0, -1));
      this.setState({
        visible: !this.state.visible
      });
    }
  }, {
    key: "handleValueClick",
    value: function handleValueClick(index) {
      var newValues = this.state.values.slice(0, index + 1);
      var paramsValues = this.state.values.slice(0, index); // 查询倒数第二级

      this.setState({
        values: newValues || [],
        index: index
      });
      this.getVmodesData(paramsValues);
    }
  }, {
    key: "handleListItemClick",
    value: function handleListItemClick(item) {
      var _this$state2 = this.state,
          values = _this$state2.values,
          index = _this$state2.index;

      var newValues = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(values);

      newValues[index] = item;
      this.setState({
        values: newValues,
        index: index + 1
      });
      this.getVmodesData(newValues);
    }
  }, {
    key: "selectedRender",
    value: function selectedRender() {
      var _this3 = this;

      var values = this.state.values;
      var length = values.length;
      var views = [];
      values.forEach(function (value, index) {
        views.push(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", {
          key: value.id,
          onClick: function onClick() {
            return _this3.handleValueClick(index);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 257
          },
          __self: this
        }, value.text));

        if (!(index === length - 1)) {
          views.push(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
            key: "separator-".concat(index),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 262
            },
            __self: this
          }, " / "));
        }
      });
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        style: {
          margin: "0 6px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265
        },
        __self: this
      }, views);
    }
  }, {
    key: "listRender",
    value: function listRender() {
      var _this4 = this;

      var _this$state3 = this.state,
          loading = _this$state3.loading,
          error = _this$state3.error,
          list = _this$state3.list;

      if (error || !list || list.length === 0) {
        var message = !error ? null : error.message; // const { body: { respMessage: message } } = error;

        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 274
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("span", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 275
          },
          __self: this
        }, !!message ? message : "no data"));
      }

      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Spin"], {
        size: "large",
        spinning: loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Ul, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 282
        },
        __self: this
      }, list.map(function (item) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Li, {
          key: item.id,
          "data-highlight": _this4.isHighlight(item.id),
          onClick: function onClick() {
            return _this4.handleListItemClick(item);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 284
          },
          __self: this
        }, item.text);
      })));
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getVmodesData(this.state.values.slice(0, -1));
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      // 卸载异步操作设置状态
      this.setState = function (state, callback) {
        return;
      };
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      var selectValue = nextProps.selectValue;
      var value = this.getInitValues(nextProps["data-__meta"].originalProps.value);
      var values = this.getInitValues(nextProps.value);
      this.setState({
        values: values,
        index: values.length > 0 ? values.length - 1 : 0 // showLabel: this.getShowLabel(value)

      });

      if (selectValue) {
        this.setState({
          showLabel: this.getShowLabel(selectValue)
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state4 = this.state,
          visible = _this$state4.visible,
          showLabel = _this$state4.showLabel;
      var _this$props = this.props,
          disabled = _this$props.disabled,
          style = _this$props.style;
      var C = this.getComponents();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Box, {
        style: style,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 337
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Input, {
        disabled: disabled,
        readOnly: true,
        placeholder: this.props.placeholder,
        value: showLabel,
        size: "large",
        suffix: visible && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Icon"], {
          onClick: this.clearAddress,
          type: "close-circle",
          style: {
            color: "rgba(0,0,0,.25)"
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 345
          },
          __self: this
        }),
        onClick: this.handleSelect.bind(this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 338
        },
        __self: this
      }), visible && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Dropdown, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 349
        },
        __self: this
      }, this.selectedRender(), this.listRender()));
    }
  }]);

  return Address;
}(_component__WEBPACK_IMPORTED_MODULE_9__["Widget"]);

Address.defaultProps = {
  ltr: true,
  disabled: false,
  style: {}
};
/* harmony default export */ __webpack_exports__["default"] = (Address);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3MudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3MudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UsIFN0eWxlZERJViwgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBBamF4IH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IEljb24sIElucHV0LCBTcGluIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuXG5jb25zdCB7IEhJR0hMSUdIVF9DT0xPUiB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcblxuZXhwb3J0IHR5cGUgQWRkcmVzc1Byb3BzID0ge1xuICBwbGFjZWhvbGRlcj86IHN0cmluZztcbiAgbHRyPzogYm9vbGVhbjtcbiAgZGlzYWJsZWQ/OiBib29sZWFuO1xuICBzdHlsZT86IG9iamVjdDtcbiAgc2VsZWN0VmFsdWU/OiBhbnk7XG4gIGNvdW50cnlDb2RlPzogYW55O1xuICBvbkNoYW5nZT86ICh2YWx1ZTogYW55KSA9PiB2b2lkO1xufSAmIFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBBZGRyZXNzU3RhdGUgPSB7XG4gIHZpc2libGU/OiBib29sZWFuO1xuICBsb2FkaW5nPzogYm9vbGVhbjtcbiAgZXJyb3I/OiBhbnk7XG4gIGxpc3Q/OiBhbnlbXTtcbiAgdmFsdWVzOiBhbnlbXTtcbiAgaW5kZXg6IG51bWJlcjtcbiAgc2hvd0xhYmVsPzogc3RyaW5nO1xufTtcblxuZXhwb3J0IHR5cGUgU3R5bGVkVWwgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwidWxcIiwgYW55LCB7fSwgbmV2ZXI+O1xuZXhwb3J0IHR5cGUgU3R5bGVkTGkgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwibGlcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBBZGRyZXNzQ29tcG9uZW50cyA9IHtcbiAgQm94OiBTdHlsZWRESVY7XG4gIERyb3Bkb3duOiBTdHlsZWRESVY7XG4gIFVsOiBTdHlsZWRVbDtcbiAgTGk6IFN0eWxlZExpO1xuICBJbnB1dDogYW55O1xufTtcblxuY2xhc3MgQWRkcmVzczxQIGV4dGVuZHMgQWRkcmVzc1Byb3BzLCBTIGV4dGVuZHMgQWRkcmVzc1N0YXRlLCBDIGV4dGVuZHMgQWRkcmVzc0NvbXBvbmVudHM+IGV4dGVuZHMgV2lkZ2V0PFAsIFMsIEM+IHtcbiAgcHJpdmF0ZSBpbml0aWFsVmFsdWVzOiBhbnlbXSA9IFtdO1xuXG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgbHRyOiB0cnVlLFxuICAgIGRpc2FibGVkOiBmYWxzZSxcbiAgICBzdHlsZToge30sXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgc3RhdGU/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgc3RhdGUpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICB0aGlzLmluaXRpYWxWYWx1ZXMgPSB0aGlzLmdldEluaXRWYWx1ZXModGhpcy5wcm9wcy5zZWxlY3RWYWx1ZSk7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIHZpc2libGU6IGZhbHNlLFxuICAgICAgbG9hZGluZzogdHJ1ZSxcbiAgICAgIGVycm9yOiBudWxsLFxuICAgICAgbGlzdDogW10sXG4gICAgICB2YWx1ZXM6IHRoaXMuaW5pdGlhbFZhbHVlcyxcbiAgICAgIGluZGV4OiB0aGlzLmluaXRpYWxWYWx1ZXMubGVuZ3RoID4gMCA/IHRoaXMuaW5pdGlhbFZhbHVlcy5sZW5ndGggLSAxIDogMCxcbiAgICAgIHNob3dMYWJlbDogdGhpcy5nZXRTaG93TGFiZWwodGhpcy5wcm9wcy5zZWxlY3RWYWx1ZSksXG4gICAgfSkgYXMgUztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YFxuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5hbnQtc3Bpbi1uZXN0ZWQtbG9hZGluZ3tcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIGxlZnQ6IDJweDtcbiAgICAgICAgICAgIHRvcDogMXB4O1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICB6LWluZGV4OiAxMDA7XG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxcHg7XG4gICAgICAgIH1cbiAgICAgIGAsXG4gICAgICBEcm9wZG93bjogU3R5bGVkLmRpdmBcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgICB0b3A6IDQzcHg7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlYmViZWI7XG4gICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgei1pbmRleDogMTAwO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICAgIGEge1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICB9XG4gICAgICAuYW50LXNwaW4tbmVzdGVkLWxvYWRpbmcge1xuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMnB4KTtcbiAgICAgICAgICB6LWluZGV4OiAxMDA7XG4gICAgICAgICAgYm9yZGVyOiAwO1xuICAgICAgfVxuICAgICAgYCxcbiAgICAgIElucHV0OiBTdHlsZWQoSW5wdXQpYFxuICAgICAgJltkYXRhLWZ1bGw9ZmFsc2VdIHtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgQGVycm9yLWNvbG9yO1xuICAgICAgfVxuICAgICAgYCxcbiAgICAgIFVsOiBTdHlsZWQudWxgXG4gICAgICAgIG1heC1oZWlnaHQ6IDM2MHB4O1xuICAgICAgICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgICAgICAgb3ZlcmZsb3c6IGF1dG87XG4gICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgIGAsXG4gICAgICBMaTogU3R5bGVkLmxpYFxuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDAgNnB4O1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiA0MnB4XG5cbiAgICAgICY6aG92ZXIsICZbZGF0YS1oaWdobGlnaHQ9dHJ1ZV0ge1xuICAgICAgICBiYWNrZ3JvdW5kOiAke0hJR0hMSUdIVF9DT0xPUn07XG4gICAgICAgIGNvbG9yOiAjZmZmXG4gICAgICB9XG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRJbml0VmFsdWVzKG1vZGFsOiBhbnkpOiBhbnlbXSB7XG4gICAgY29uc3QgdmFsdWVzID0gW107XG4gICAgaWYgKG1vZGFsKSB7XG4gICAgICAvLyDku45rMeW8gOWniywg5pyA5aSa5YiwazRcbiAgICAgIGZvciAobGV0IGkgPSAxOyBpIDw9IDQ7IGkrKykge1xuICAgICAgICBpZiAobW9kYWxbYGske2l9YF0pIHtcbiAgICAgICAgICB2YWx1ZXMucHVzaCh7XG4gICAgICAgICAgICBpZDogbW9kYWxbYGske2l9YF0sXG4gICAgICAgICAgICB0ZXh0OiBtb2RhbFtgayR7aX1uYW1lYF0sXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlcztcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRTaG93TGFiZWwobW9kYWw6IGFueSk6IHN0cmluZyB7XG4gICAgY29uc3QgeyBsdHIgfSA9IHRoaXMucHJvcHM7XG4gICAgbGV0IHNob3dMYWJlbCA9IFtdO1xuICAgIGlmIChtb2RhbCkge1xuICAgICAgLy8g5LuOazHlvIDlp4ssIOacgOWkmuWIsGs0XG4gICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSA0OyBpKyspIHtcbiAgICAgICAgaWYgKG1vZGFsW2BrJHtpfW5hbWVgXSkge1xuICAgICAgICAgIHNob3dMYWJlbC5wdXNoKG1vZGFsW2BrJHtpfW5hbWVgXSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgaWYgKCFsdHIpIHtcbiAgICAgIHNob3dMYWJlbC5yZXZlcnNlKCk7XG4gICAgfVxuICAgIHJldHVybiBzaG93TGFiZWwuam9pbihcIiwgXCIpO1xuICB9XG5cbiAgcHJvdGVjdGVkIHNldExvYWRpbmcoKTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IHRydWUsIGVycm9yOiBudWxsIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIHNldEVycm9yKGVycjogYW55KTogdm9pZCB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGxvYWRpbmc6IGZhbHNlLCBlcnJvcjogZXJyLCBsaXN0OiBbXSB9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXREYXRhKGxpc3Q6IGFueSk6IHZvaWQge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBsb2FkaW5nOiBmYWxzZSwgZXJyb3I6IG51bGwsIGxpc3Q6IGxpc3QgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgc2V0RGF0YUZpbmlzaGVkKG1vZGFsOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zdCBzaG93TGFiZWwgPSB0aGlzLmdldFNob3dMYWJlbChtb2RhbCk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICAgIHZpc2libGU6IGZhbHNlLFxuICAgICAgaW5kZXg6IHRoaXMuc3RhdGUuaW5kZXggLSAxLFxuICAgICAgc2hvd0xhYmVsLFxuICAgIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGlzSGlnaGxpZ2h0KGlkOiBhbnkpOiBib29sZWFuIHtcbiAgICBjb25zdCB7IHZhbHVlcywgaW5kZXggfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgdmFsdWUgPSB2YWx1ZXNbaW5kZXhdO1xuICAgIHJldHVybiB2YWx1ZSAmJiB2YWx1ZS5pZCA9PT0gaWQ7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0Vm1vZGVzRGF0YSh2YWx1ZXM6IGFueVtdKTogdm9pZCB7XG4gICAgdGhpcy5zZXRMb2FkaW5nKCk7XG4gICAgY29uc3QgbGV2ZWwgPSB2YWx1ZXMubGVuZ3RoICsgMTtcbiAgICAvLyB0b2RvOiDlnLDlnYAgICBTR1AgVEhBIE1NUlxuICAgIGNvbnN0IHBhcmFtcyA9IHsgaXRudENvZGU6IFwiUEZfVEhBSVwiLCBjb3VudHJ5Q29kZTogdGhpcy5wcm9wcy5jb3VudHJ5Q29kZSB8fCBcIlRIQVwiLCBsZXZlbCB9IGFzIGFueTtcbiAgICAodmFsdWVzIHx8IFtdKS5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xuICAgICAgcGFyYW1zW2BrJHtpbmRleCArIDF9YF0gPSBpdGVtLmlkO1xuICAgIH0pO1xuICAgIGxldCBwYXJhbXNTdHIgPSBcIlwiO1xuICAgIE9iamVjdC5lbnRyaWVzKHBhcmFtcykubWFwKChldmVyeTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBwYXJhbXNTdHIgKz0gYCR7ZXZlcnlbMF19PSR7ZXZlcnlbMV19JHtpbmRleCA9PT0gT2JqZWN0LmVudHJpZXMocGFyYW1zKS5sZW5ndGggLSAxID8gXCJcIiA6IFwiJlwifWA7XG4gICAgfSk7XG4gICAgQWpheC5wb3N0KFxuICAgICAgYC9hZGRyZXNzZXNxPyR7cGFyYW1zU3RyfWAsXG4gICAgICB7fSxcbiAgICAgIHtcbiAgICAgICAgaWdub3JlQXV0aDogZmFsc2UsXG4gICAgICB9LFxuICAgIClcbiAgICAgIC50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgcmVzcERhdGEgfSA9IHJlc3BvbnNlLmJvZHk7XG4gICAgICAgIGlmICghcmVzcERhdGEubGVhZikge1xuICAgICAgICAgIHRoaXMuc2V0RGF0YShyZXNwRGF0YS5rZXlzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnNldERhdGFGaW5pc2hlZChyZXNwRGF0YS5hZGRyZXNzKTtcbiAgICAgICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlICYmIHRoaXMucHJvcHMub25DaGFuZ2UocmVzcERhdGEuYWRkcmVzcyk7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICB0aGlzLnNldEVycm9yKGVycm9yKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGhhbmRsZVNlbGVjdCgpOiB2b2lkIHtcbiAgICB0aGlzLmdldFZtb2Rlc0RhdGEodGhpcy5zdGF0ZS52YWx1ZXMuc2xpY2UoMCwgLTEpKTtcbiAgICB0aGlzLnNldFN0YXRlKHsgdmlzaWJsZTogIXRoaXMuc3RhdGUudmlzaWJsZSB9KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBoYW5kbGVWYWx1ZUNsaWNrKGluZGV4OiBudW1iZXIpOiB2b2lkIHtcbiAgICBjb25zdCBuZXdWYWx1ZXMgPSB0aGlzLnN0YXRlLnZhbHVlcy5zbGljZSgwLCBpbmRleCArIDEpO1xuICAgIGNvbnN0IHBhcmFtc1ZhbHVlcyA9IHRoaXMuc3RhdGUudmFsdWVzLnNsaWNlKDAsIGluZGV4KTsgLy8g5p+l6K+i5YCS5pWw56ys5LqM57qnXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2YWx1ZXM6IG5ld1ZhbHVlcyB8fCBbXSxcbiAgICAgIGluZGV4OiBpbmRleCxcbiAgICB9KTtcbiAgICB0aGlzLmdldFZtb2Rlc0RhdGEocGFyYW1zVmFsdWVzKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBoYW5kbGVMaXN0SXRlbUNsaWNrKGl0ZW06IGFueSk6IHZvaWQge1xuICAgIGNvbnN0IHsgdmFsdWVzLCBpbmRleCB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBuZXdWYWx1ZXMgPSBbLi4udmFsdWVzXTtcbiAgICBuZXdWYWx1ZXNbaW5kZXhdID0gaXRlbTtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHZhbHVlczogbmV3VmFsdWVzLFxuICAgICAgaW5kZXg6IGluZGV4ICsgMSxcbiAgICB9KTtcbiAgICB0aGlzLmdldFZtb2Rlc0RhdGEobmV3VmFsdWVzKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBzZWxlY3RlZFJlbmRlcigpOiBhbnkge1xuICAgIGNvbnN0IHsgdmFsdWVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IGxlbmd0aCA9IHZhbHVlcy5sZW5ndGg7XG4gICAgY29uc3Qgdmlld3M6IGFueVtdID0gW107XG4gICAgdmFsdWVzLmZvckVhY2goKHZhbHVlLCBpbmRleCkgPT4ge1xuICAgICAgdmlld3MucHVzaChcbiAgICAgICAgPGEga2V5PXt2YWx1ZS5pZH0gb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVWYWx1ZUNsaWNrKGluZGV4KX0+XG4gICAgICAgICAge3ZhbHVlLnRleHR9XG4gICAgICAgIDwvYT4sXG4gICAgICApO1xuICAgICAgaWYgKCEoaW5kZXggPT09IGxlbmd0aCAtIDEpKSB7XG4gICAgICAgIHZpZXdzLnB1c2goPHNwYW4ga2V5PXtgc2VwYXJhdG9yLSR7aW5kZXh9YH0+IC8gPC9zcGFuPik7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIDxkaXYgc3R5bGU9e3sgbWFyZ2luOiBcIjAgNnB4XCIgfX0+e3ZpZXdzfTwvZGl2PjtcbiAgfVxuXG4gIHByb3RlY3RlZCBsaXN0UmVuZGVyKCk6IGFueSB7XG4gICAgY29uc3QgeyBsb2FkaW5nLCBlcnJvciwgbGlzdCB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAoZXJyb3IgfHwgIWxpc3QgfHwgbGlzdC5sZW5ndGggPT09IDApIHtcbiAgICAgIGNvbnN0IG1lc3NhZ2UgPSAhZXJyb3IgPyBudWxsIDogZXJyb3IubWVzc2FnZTtcbiAgICAgIC8vIGNvbnN0IHsgYm9keTogeyByZXNwTWVzc2FnZTogbWVzc2FnZSB9IH0gPSBlcnJvcjtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPHNwYW4+eyEhbWVzc2FnZSA/IG1lc3NhZ2UgOiBcIm5vIGRhdGFcIn08L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKTtcbiAgICB9XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIHJldHVybiAoXG4gICAgICA8U3BpbiBzaXplPVwibGFyZ2VcIiBzcGlubmluZz17bG9hZGluZ30+XG4gICAgICAgIDxDLlVsPlxuICAgICAgICAgIHtsaXN0Lm1hcChpdGVtID0+IChcbiAgICAgICAgICAgIDxDLkxpXG4gICAgICAgICAgICAgIGtleT17aXRlbS5pZH1cbiAgICAgICAgICAgICAgZGF0YS1oaWdobGlnaHQ9e3RoaXMuaXNIaWdobGlnaHQoaXRlbS5pZCl9XG4gICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlTGlzdEl0ZW1DbGljayhpdGVtKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAge2l0ZW0udGV4dH1cbiAgICAgICAgICAgIDwvQy5MaT5cbiAgICAgICAgICApKX1cbiAgICAgICAgPC9DLlVsPlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmdldFZtb2Rlc0RhdGEodGhpcy5zdGF0ZS52YWx1ZXMuc2xpY2UoMCwgLTEpKTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgIC8vIOWNuOi9veW8guatpeaTjeS9nOiuvue9rueKtuaAgVxuICAgIHRoaXMuc2V0U3RhdGUgPSAoc3RhdGUsIGNhbGxiYWNrKSA9PiB7XG4gICAgICByZXR1cm47XG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzOiBhbnkpIHtcbiAgICBjb25zdCB7IHNlbGVjdFZhbHVlIH0gPSBuZXh0UHJvcHM7XG4gICAgY29uc3QgdmFsdWUgPSB0aGlzLmdldEluaXRWYWx1ZXMobmV4dFByb3BzW1wiZGF0YS1fX21ldGFcIl0ub3JpZ2luYWxQcm9wcy52YWx1ZSk7XG4gICAgY29uc3QgdmFsdWVzID0gdGhpcy5nZXRJbml0VmFsdWVzKG5leHRQcm9wcy52YWx1ZSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2YWx1ZXM6IHZhbHVlcyxcbiAgICAgIGluZGV4OiB2YWx1ZXMubGVuZ3RoID4gMCA/IHZhbHVlcy5sZW5ndGggLSAxIDogMCxcbiAgICAgIC8vIHNob3dMYWJlbDogdGhpcy5nZXRTaG93TGFiZWwodmFsdWUpXG4gICAgfSk7XG4gICAgaWYgKHNlbGVjdFZhbHVlKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2hvd0xhYmVsOiB0aGlzLmdldFNob3dMYWJlbChzZWxlY3RWYWx1ZSksXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBjbGVhckFkZHJlc3MgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB2aXNpYmxlOiAhdGhpcy5zdGF0ZS52aXNpYmxlLFxuICAgIH0pO1xuICAgIHRoaXMucHJvcHMub25DaGFuZ2UgJiYgdGhpcy5wcm9wcy5vbkNoYW5nZSh7fSk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgdmlzaWJsZSwgc2hvd0xhYmVsIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgZGlzYWJsZWQsIHN0eWxlIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3ggc3R5bGU9e3N0eWxlfT5cbiAgICAgICAgPEMuSW5wdXRcbiAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgcmVhZE9ubHk9e3RydWV9XG4gICAgICAgICAgcGxhY2Vob2xkZXI9e3RoaXMucHJvcHMucGxhY2Vob2xkZXJ9XG4gICAgICAgICAgdmFsdWU9e3Nob3dMYWJlbH1cbiAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgIHN1ZmZpeD17dmlzaWJsZSAmJlxuICAgICAgICAgIDxJY29uIG9uQ2xpY2s9e3RoaXMuY2xlYXJBZGRyZXNzfSB0eXBlPVwiY2xvc2UtY2lyY2xlXCIgc3R5bGU9e3sgY29sb3I6IFwicmdiYSgwLDAsMCwuMjUpXCIgfX0vPn1cbiAgICAgICAgICBvbkNsaWNrPXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKHRoaXMpfVxuICAgICAgICAvPlxuICAgICAgICB7dmlzaWJsZSAmJiAoXG4gICAgICAgICAgPEMuRHJvcGRvd24+XG4gICAgICAgICAgICB7dGhpcy5zZWxlY3RlZFJlbmRlcigpfVxuICAgICAgICAgICAge3RoaXMubGlzdFJlbmRlcigpfVxuICAgICAgICAgIDwvQy5Ecm9wZG93bj5cbiAgICAgICAgKX1cbiAgICAgIDwvQy5Cb3g+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBBZGRyZXNzO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWdDQTs7Ozs7QUFTQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFrUkE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUF2UkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7OztBQUVBO0FBQ0E7QUFDQTtBQWFBO0FBbUJBO0FBS0E7QUFVQTtBQWhEQTtBQTREQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFZQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOzs7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BOzs7O0FBMVRBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUhBO0FBMFRBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/address.tsx
