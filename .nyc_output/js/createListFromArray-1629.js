/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var List = __webpack_require__(/*! ../../data/List */ "./node_modules/echarts/lib/data/List.js");

var createDimensions = __webpack_require__(/*! ../../data/helper/createDimensions */ "./node_modules/echarts/lib/data/helper/createDimensions.js");

var _sourceType = __webpack_require__(/*! ../../data/helper/sourceType */ "./node_modules/echarts/lib/data/helper/sourceType.js");

var SOURCE_FORMAT_ORIGINAL = _sourceType.SOURCE_FORMAT_ORIGINAL;

var _dimensionHelper = __webpack_require__(/*! ../../data/helper/dimensionHelper */ "./node_modules/echarts/lib/data/helper/dimensionHelper.js");

var getDimensionTypeByAxis = _dimensionHelper.getDimensionTypeByAxis;

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var getDataItemValue = _model.getDataItemValue;

var CoordinateSystem = __webpack_require__(/*! ../../CoordinateSystem */ "./node_modules/echarts/lib/CoordinateSystem.js");

var _referHelper = __webpack_require__(/*! ../../model/referHelper */ "./node_modules/echarts/lib/model/referHelper.js");

var getCoordSysDefineBySeries = _referHelper.getCoordSysDefineBySeries;

var Source = __webpack_require__(/*! ../../data/Source */ "./node_modules/echarts/lib/data/Source.js");

var _dataStackHelper = __webpack_require__(/*! ../../data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var enableDataStack = _dataStackHelper.enableDataStack;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @param {module:echarts/data/Source|Array} source Or raw data.
 * @param {module:echarts/model/Series} seriesModel
 * @param {Object} [opt]
 * @param {string} [opt.generateCoord]
 */

function createListFromArray(source, seriesModel, opt) {
  opt = opt || {};

  if (!Source.isInstance(source)) {
    source = Source.seriesDataToSource(source);
  }

  var coordSysName = seriesModel.get('coordinateSystem');
  var registeredCoordSys = CoordinateSystem.get(coordSysName);
  var coordSysDefine = getCoordSysDefineBySeries(seriesModel);
  var coordSysDimDefs;

  if (coordSysDefine) {
    coordSysDimDefs = zrUtil.map(coordSysDefine.coordSysDims, function (dim) {
      var dimInfo = {
        name: dim
      };
      var axisModel = coordSysDefine.axisMap.get(dim);

      if (axisModel) {
        var axisType = axisModel.get('type');
        dimInfo.type = getDimensionTypeByAxis(axisType); // dimInfo.stackable = isStackable(axisType);
      }

      return dimInfo;
    });
  }

  if (!coordSysDimDefs) {
    // Get dimensions from registered coordinate system
    coordSysDimDefs = registeredCoordSys && (registeredCoordSys.getDimensionsInfo ? registeredCoordSys.getDimensionsInfo() : registeredCoordSys.dimensions.slice()) || ['x', 'y'];
  }

  var dimInfoList = createDimensions(source, {
    coordDimensions: coordSysDimDefs,
    generateCoord: opt.generateCoord
  });
  var firstCategoryDimIndex;
  var hasNameEncode;
  coordSysDefine && zrUtil.each(dimInfoList, function (dimInfo, dimIndex) {
    var coordDim = dimInfo.coordDim;
    var categoryAxisModel = coordSysDefine.categoryAxisMap.get(coordDim);

    if (categoryAxisModel) {
      if (firstCategoryDimIndex == null) {
        firstCategoryDimIndex = dimIndex;
      }

      dimInfo.ordinalMeta = categoryAxisModel.getOrdinalMeta();
    }

    if (dimInfo.otherDims.itemName != null) {
      hasNameEncode = true;
    }
  });

  if (!hasNameEncode && firstCategoryDimIndex != null) {
    dimInfoList[firstCategoryDimIndex].otherDims.itemName = 0;
  }

  var stackCalculationInfo = enableDataStack(seriesModel, dimInfoList);
  var list = new List(dimInfoList, seriesModel);
  list.setCalculationInfo(stackCalculationInfo);
  var dimValueGetter = firstCategoryDimIndex != null && isNeedCompleteOrdinalData(source) ? function (itemOpt, dimName, dataIndex, dimIndex) {
    // Use dataIndex as ordinal value in categoryAxis
    return dimIndex === firstCategoryDimIndex ? dataIndex : this.defaultDimValueGetter(itemOpt, dimName, dataIndex, dimIndex);
  } : null;
  list.hasItemOption = false;
  list.initData(source, null, dimValueGetter);
  return list;
}

function isNeedCompleteOrdinalData(source) {
  if (source.sourceFormat === SOURCE_FORMAT_ORIGINAL) {
    var sampleItem = firstDataNotNull(source.data || []);
    return sampleItem != null && !zrUtil.isArray(getDataItemValue(sampleItem));
  }
}

function firstDataNotNull(data) {
  var i = 0;

  while (i < data.length && data[i] == null) {
    i++;
  }

  return data[i];
}

var _default = createListFromArray;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvaGVscGVyL2NyZWF0ZUxpc3RGcm9tQXJyYXkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9oZWxwZXIvY3JlYXRlTGlzdEZyb21BcnJheS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBMaXN0ID0gcmVxdWlyZShcIi4uLy4uL2RhdGEvTGlzdFwiKTtcblxudmFyIGNyZWF0ZURpbWVuc2lvbnMgPSByZXF1aXJlKFwiLi4vLi4vZGF0YS9oZWxwZXIvY3JlYXRlRGltZW5zaW9uc1wiKTtcblxudmFyIF9zb3VyY2VUeXBlID0gcmVxdWlyZShcIi4uLy4uL2RhdGEvaGVscGVyL3NvdXJjZVR5cGVcIik7XG5cbnZhciBTT1VSQ0VfRk9STUFUX09SSUdJTkFMID0gX3NvdXJjZVR5cGUuU09VUkNFX0ZPUk1BVF9PUklHSU5BTDtcblxudmFyIF9kaW1lbnNpb25IZWxwZXIgPSByZXF1aXJlKFwiLi4vLi4vZGF0YS9oZWxwZXIvZGltZW5zaW9uSGVscGVyXCIpO1xuXG52YXIgZ2V0RGltZW5zaW9uVHlwZUJ5QXhpcyA9IF9kaW1lbnNpb25IZWxwZXIuZ2V0RGltZW5zaW9uVHlwZUJ5QXhpcztcblxudmFyIF9tb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL21vZGVsXCIpO1xuXG52YXIgZ2V0RGF0YUl0ZW1WYWx1ZSA9IF9tb2RlbC5nZXREYXRhSXRlbVZhbHVlO1xuXG52YXIgQ29vcmRpbmF0ZVN5c3RlbSA9IHJlcXVpcmUoXCIuLi8uLi9Db29yZGluYXRlU3lzdGVtXCIpO1xuXG52YXIgX3JlZmVySGVscGVyID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL3JlZmVySGVscGVyXCIpO1xuXG52YXIgZ2V0Q29vcmRTeXNEZWZpbmVCeVNlcmllcyA9IF9yZWZlckhlbHBlci5nZXRDb29yZFN5c0RlZmluZUJ5U2VyaWVzO1xuXG52YXIgU291cmNlID0gcmVxdWlyZShcIi4uLy4uL2RhdGEvU291cmNlXCIpO1xuXG52YXIgX2RhdGFTdGFja0hlbHBlciA9IHJlcXVpcmUoXCIuLi8uLi9kYXRhL2hlbHBlci9kYXRhU3RhY2tIZWxwZXJcIik7XG5cbnZhciBlbmFibGVEYXRhU3RhY2sgPSBfZGF0YVN0YWNrSGVscGVyLmVuYWJsZURhdGFTdGFjaztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvZGF0YS9Tb3VyY2V8QXJyYXl9IHNvdXJjZSBPciByYXcgZGF0YS5cbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfSBzZXJpZXNNb2RlbFxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdXG4gKiBAcGFyYW0ge3N0cmluZ30gW29wdC5nZW5lcmF0ZUNvb3JkXVxuICovXG5mdW5jdGlvbiBjcmVhdGVMaXN0RnJvbUFycmF5KHNvdXJjZSwgc2VyaWVzTW9kZWwsIG9wdCkge1xuICBvcHQgPSBvcHQgfHwge307XG5cbiAgaWYgKCFTb3VyY2UuaXNJbnN0YW5jZShzb3VyY2UpKSB7XG4gICAgc291cmNlID0gU291cmNlLnNlcmllc0RhdGFUb1NvdXJjZShzb3VyY2UpO1xuICB9XG5cbiAgdmFyIGNvb3JkU3lzTmFtZSA9IHNlcmllc01vZGVsLmdldCgnY29vcmRpbmF0ZVN5c3RlbScpO1xuICB2YXIgcmVnaXN0ZXJlZENvb3JkU3lzID0gQ29vcmRpbmF0ZVN5c3RlbS5nZXQoY29vcmRTeXNOYW1lKTtcbiAgdmFyIGNvb3JkU3lzRGVmaW5lID0gZ2V0Q29vcmRTeXNEZWZpbmVCeVNlcmllcyhzZXJpZXNNb2RlbCk7XG4gIHZhciBjb29yZFN5c0RpbURlZnM7XG5cbiAgaWYgKGNvb3JkU3lzRGVmaW5lKSB7XG4gICAgY29vcmRTeXNEaW1EZWZzID0genJVdGlsLm1hcChjb29yZFN5c0RlZmluZS5jb29yZFN5c0RpbXMsIGZ1bmN0aW9uIChkaW0pIHtcbiAgICAgIHZhciBkaW1JbmZvID0ge1xuICAgICAgICBuYW1lOiBkaW1cbiAgICAgIH07XG4gICAgICB2YXIgYXhpc01vZGVsID0gY29vcmRTeXNEZWZpbmUuYXhpc01hcC5nZXQoZGltKTtcblxuICAgICAgaWYgKGF4aXNNb2RlbCkge1xuICAgICAgICB2YXIgYXhpc1R5cGUgPSBheGlzTW9kZWwuZ2V0KCd0eXBlJyk7XG4gICAgICAgIGRpbUluZm8udHlwZSA9IGdldERpbWVuc2lvblR5cGVCeUF4aXMoYXhpc1R5cGUpOyAvLyBkaW1JbmZvLnN0YWNrYWJsZSA9IGlzU3RhY2thYmxlKGF4aXNUeXBlKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGRpbUluZm87XG4gICAgfSk7XG4gIH1cblxuICBpZiAoIWNvb3JkU3lzRGltRGVmcykge1xuICAgIC8vIEdldCBkaW1lbnNpb25zIGZyb20gcmVnaXN0ZXJlZCBjb29yZGluYXRlIHN5c3RlbVxuICAgIGNvb3JkU3lzRGltRGVmcyA9IHJlZ2lzdGVyZWRDb29yZFN5cyAmJiAocmVnaXN0ZXJlZENvb3JkU3lzLmdldERpbWVuc2lvbnNJbmZvID8gcmVnaXN0ZXJlZENvb3JkU3lzLmdldERpbWVuc2lvbnNJbmZvKCkgOiByZWdpc3RlcmVkQ29vcmRTeXMuZGltZW5zaW9ucy5zbGljZSgpKSB8fCBbJ3gnLCAneSddO1xuICB9XG5cbiAgdmFyIGRpbUluZm9MaXN0ID0gY3JlYXRlRGltZW5zaW9ucyhzb3VyY2UsIHtcbiAgICBjb29yZERpbWVuc2lvbnM6IGNvb3JkU3lzRGltRGVmcyxcbiAgICBnZW5lcmF0ZUNvb3JkOiBvcHQuZ2VuZXJhdGVDb29yZFxuICB9KTtcbiAgdmFyIGZpcnN0Q2F0ZWdvcnlEaW1JbmRleDtcbiAgdmFyIGhhc05hbWVFbmNvZGU7XG4gIGNvb3JkU3lzRGVmaW5lICYmIHpyVXRpbC5lYWNoKGRpbUluZm9MaXN0LCBmdW5jdGlvbiAoZGltSW5mbywgZGltSW5kZXgpIHtcbiAgICB2YXIgY29vcmREaW0gPSBkaW1JbmZvLmNvb3JkRGltO1xuICAgIHZhciBjYXRlZ29yeUF4aXNNb2RlbCA9IGNvb3JkU3lzRGVmaW5lLmNhdGVnb3J5QXhpc01hcC5nZXQoY29vcmREaW0pO1xuXG4gICAgaWYgKGNhdGVnb3J5QXhpc01vZGVsKSB7XG4gICAgICBpZiAoZmlyc3RDYXRlZ29yeURpbUluZGV4ID09IG51bGwpIHtcbiAgICAgICAgZmlyc3RDYXRlZ29yeURpbUluZGV4ID0gZGltSW5kZXg7XG4gICAgICB9XG5cbiAgICAgIGRpbUluZm8ub3JkaW5hbE1ldGEgPSBjYXRlZ29yeUF4aXNNb2RlbC5nZXRPcmRpbmFsTWV0YSgpO1xuICAgIH1cblxuICAgIGlmIChkaW1JbmZvLm90aGVyRGltcy5pdGVtTmFtZSAhPSBudWxsKSB7XG4gICAgICBoYXNOYW1lRW5jb2RlID0gdHJ1ZTtcbiAgICB9XG4gIH0pO1xuXG4gIGlmICghaGFzTmFtZUVuY29kZSAmJiBmaXJzdENhdGVnb3J5RGltSW5kZXggIT0gbnVsbCkge1xuICAgIGRpbUluZm9MaXN0W2ZpcnN0Q2F0ZWdvcnlEaW1JbmRleF0ub3RoZXJEaW1zLml0ZW1OYW1lID0gMDtcbiAgfVxuXG4gIHZhciBzdGFja0NhbGN1bGF0aW9uSW5mbyA9IGVuYWJsZURhdGFTdGFjayhzZXJpZXNNb2RlbCwgZGltSW5mb0xpc3QpO1xuICB2YXIgbGlzdCA9IG5ldyBMaXN0KGRpbUluZm9MaXN0LCBzZXJpZXNNb2RlbCk7XG4gIGxpc3Quc2V0Q2FsY3VsYXRpb25JbmZvKHN0YWNrQ2FsY3VsYXRpb25JbmZvKTtcbiAgdmFyIGRpbVZhbHVlR2V0dGVyID0gZmlyc3RDYXRlZ29yeURpbUluZGV4ICE9IG51bGwgJiYgaXNOZWVkQ29tcGxldGVPcmRpbmFsRGF0YShzb3VyY2UpID8gZnVuY3Rpb24gKGl0ZW1PcHQsIGRpbU5hbWUsIGRhdGFJbmRleCwgZGltSW5kZXgpIHtcbiAgICAvLyBVc2UgZGF0YUluZGV4IGFzIG9yZGluYWwgdmFsdWUgaW4gY2F0ZWdvcnlBeGlzXG4gICAgcmV0dXJuIGRpbUluZGV4ID09PSBmaXJzdENhdGVnb3J5RGltSW5kZXggPyBkYXRhSW5kZXggOiB0aGlzLmRlZmF1bHREaW1WYWx1ZUdldHRlcihpdGVtT3B0LCBkaW1OYW1lLCBkYXRhSW5kZXgsIGRpbUluZGV4KTtcbiAgfSA6IG51bGw7XG4gIGxpc3QuaGFzSXRlbU9wdGlvbiA9IGZhbHNlO1xuICBsaXN0LmluaXREYXRhKHNvdXJjZSwgbnVsbCwgZGltVmFsdWVHZXR0ZXIpO1xuICByZXR1cm4gbGlzdDtcbn1cblxuZnVuY3Rpb24gaXNOZWVkQ29tcGxldGVPcmRpbmFsRGF0YShzb3VyY2UpIHtcbiAgaWYgKHNvdXJjZS5zb3VyY2VGb3JtYXQgPT09IFNPVVJDRV9GT1JNQVRfT1JJR0lOQUwpIHtcbiAgICB2YXIgc2FtcGxlSXRlbSA9IGZpcnN0RGF0YU5vdE51bGwoc291cmNlLmRhdGEgfHwgW10pO1xuICAgIHJldHVybiBzYW1wbGVJdGVtICE9IG51bGwgJiYgIXpyVXRpbC5pc0FycmF5KGdldERhdGFJdGVtVmFsdWUoc2FtcGxlSXRlbSkpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGZpcnN0RGF0YU5vdE51bGwoZGF0YSkge1xuICB2YXIgaSA9IDA7XG5cbiAgd2hpbGUgKGkgPCBkYXRhLmxlbmd0aCAmJiBkYXRhW2ldID09IG51bGwpIHtcbiAgICBpKys7XG4gIH1cblxuICByZXR1cm4gZGF0YVtpXTtcbn1cblxudmFyIF9kZWZhdWx0ID0gY3JlYXRlTGlzdEZyb21BcnJheTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/helper/createListFromArray.js
