__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FormItemNationality; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/useful-form-item/nationality.tsx";





var generatePropsName = function generatePropsName(propName, dataId) {
  if (!!dataId) return "".concat(dataId, ".").concat(propName);
  return propName;
};

var FormItemNationality =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(FormItemNationality, _ModelWidget);

  function FormItemNationality() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, FormItemNationality);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(FormItemNationality).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(FormItemNationality, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          dataId = _this$props.dataId,
          form = _this$props.form,
          model = _this$props.model,
          _this$props$required = _this$props.required,
          required = _this$props$required === void 0 ? true : _this$props$required,
          dataFixed = _this$props.dataFixed;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NSelect"], {
        form: form,
        model: model,
        propName: generatePropsName("nationality", dataId),
        tableName: "nationality",
        required: required,
        size: "large",
        style: {
          width: "100%"
        },
        notFoundContent: "",
        showSearch: true,
        dataFixed: dataFixed,
        filterOption: function filterOption(input, option) {
          return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
        },
        label: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Nationality").thai("สัญชาติ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      });
    }
  }]);

  return FormItemNationality;
}(_component__WEBPACK_IMPORTED_MODULE_6__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vbmF0aW9uYWxpdHkudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vbmF0aW9uYWxpdHkudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQgfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTlNlbGVjdCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuXG5jb25zdCBnZW5lcmF0ZVByb3BzTmFtZSA9IChwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWQ/OiBzdHJpbmcpOiBzdHJpbmcgPT4ge1xuICBpZiAoISFkYXRhSWQpIHJldHVybiBgJHtkYXRhSWR9LiR7cHJvcE5hbWV9YDtcbiAgcmV0dXJuIHByb3BOYW1lO1xufTtcblxudHlwZSBJUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgZGF0YUlkPzogc3RyaW5nO1xuICBkYXRhRml4ZWQ/OiBhbnk7XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbn0gJiBNb2RlbFdpZGdldFByb3BzICYgRm9ybUNvbXBvbmVudFByb3BzO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBGb3JtSXRlbU5hdGlvbmFsaXR5PFAgZXh0ZW5kcyBJUHJvcHMsIFMsIEM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHt9IGFzIEM7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBkYXRhSWQsIGZvcm0sIG1vZGVsLCByZXF1aXJlZCA9IHRydWUsIGRhdGFGaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPE5TZWxlY3RcbiAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICBwcm9wTmFtZT17Z2VuZXJhdGVQcm9wc05hbWUoXCJuYXRpb25hbGl0eVwiLCBkYXRhSWQpfVxuICAgICAgICB0YWJsZU5hbWU9XCJuYXRpb25hbGl0eVwiXG4gICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgbm90Rm91bmRDb250ZW50PVwiXCJcbiAgICAgICAgc2hvd1NlYXJjaFxuICAgICAgICBkYXRhRml4ZWQ9e2RhdGFGaXhlZH1cbiAgICAgICAgZmlsdGVyT3B0aW9uPXsoaW5wdXQsIG9wdGlvbjogYW55KSA9PiBvcHRpb24ucHJvcHMuY2hpbGRyZW4udG9Mb3dlckNhc2UoKS5pbmRleE9mKGlucHV0LnRvTG93ZXJDYXNlKCkpID49IDB9XG4gICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk5hdGlvbmFsaXR5XCIpLnRoYWkoXCLguKrguLHguI3guIrguLLguJXguLRcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgLz5cbiAgICApO1xuICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFTQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTs7OztBQXZCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/useful-form-item/nationality.tsx
