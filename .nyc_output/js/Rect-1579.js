var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var roundRectHelper = __webpack_require__(/*! ../helper/roundRect */ "./node_modules/zrender/lib/graphic/helper/roundRect.js");

var _subPixelOptimize = __webpack_require__(/*! ../helper/subPixelOptimize */ "./node_modules/zrender/lib/graphic/helper/subPixelOptimize.js");

var subPixelOptimizeRect = _subPixelOptimize.subPixelOptimizeRect;
/**
 * 矩形
 * @module zrender/graphic/shape/Rect
 */
// Avoid create repeatly.

var subPixelOptimizeOutputShape = {};

var _default = Path.extend({
  type: 'rect',
  shape: {
    // 左上、右上、右下、左下角的半径依次为r1、r2、r3、r4
    // r缩写为1         相当于 [1, 1, 1, 1]
    // r缩写为[1]       相当于 [1, 1, 1, 1]
    // r缩写为[1, 2]    相当于 [1, 2, 1, 2]
    // r缩写为[1, 2, 3] 相当于 [1, 2, 3, 2]
    r: 0,
    x: 0,
    y: 0,
    width: 0,
    height: 0
  },
  buildPath: function buildPath(ctx, shape) {
    var x;
    var y;
    var width;
    var height;

    if (this.subPixelOptimize) {
      subPixelOptimizeRect(subPixelOptimizeOutputShape, shape, this.style);
      x = subPixelOptimizeOutputShape.x;
      y = subPixelOptimizeOutputShape.y;
      width = subPixelOptimizeOutputShape.width;
      height = subPixelOptimizeOutputShape.height;
      subPixelOptimizeOutputShape.r = shape.r;
      shape = subPixelOptimizeOutputShape;
    } else {
      x = shape.x;
      y = shape.y;
      width = shape.width;
      height = shape.height;
    }

    if (!shape.r) {
      ctx.rect(x, y, width, height);
    } else {
      roundRectHelper.buildPath(ctx, shape);
    }

    ctx.closePath();
    return;
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9SZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9SZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL1BhdGhcIik7XG5cbnZhciByb3VuZFJlY3RIZWxwZXIgPSByZXF1aXJlKFwiLi4vaGVscGVyL3JvdW5kUmVjdFwiKTtcblxudmFyIF9zdWJQaXhlbE9wdGltaXplID0gcmVxdWlyZShcIi4uL2hlbHBlci9zdWJQaXhlbE9wdGltaXplXCIpO1xuXG52YXIgc3ViUGl4ZWxPcHRpbWl6ZVJlY3QgPSBfc3ViUGl4ZWxPcHRpbWl6ZS5zdWJQaXhlbE9wdGltaXplUmVjdDtcblxuLyoqXG4gKiDnn6nlvaJcbiAqIEBtb2R1bGUgenJlbmRlci9ncmFwaGljL3NoYXBlL1JlY3RcbiAqL1xuLy8gQXZvaWQgY3JlYXRlIHJlcGVhdGx5LlxudmFyIHN1YlBpeGVsT3B0aW1pemVPdXRwdXRTaGFwZSA9IHt9O1xuXG52YXIgX2RlZmF1bHQgPSBQYXRoLmV4dGVuZCh7XG4gIHR5cGU6ICdyZWN0JyxcbiAgc2hhcGU6IHtcbiAgICAvLyDlt6bkuIrjgIHlj7PkuIrjgIHlj7PkuIvjgIHlt6bkuIvop5LnmoTljYrlvoTkvp3mrKHkuLpyMeOAgXIy44CBcjPjgIFyNFxuICAgIC8vIHLnvKnlhpnkuLoxICAgICAgICAg55u45b2T5LqOIFsxLCAxLCAxLCAxXVxuICAgIC8vIHLnvKnlhpnkuLpbMV0gICAgICAg55u45b2T5LqOIFsxLCAxLCAxLCAxXVxuICAgIC8vIHLnvKnlhpnkuLpbMSwgMl0gICAg55u45b2T5LqOIFsxLCAyLCAxLCAyXVxuICAgIC8vIHLnvKnlhpnkuLpbMSwgMiwgM10g55u45b2T5LqOIFsxLCAyLCAzLCAyXVxuICAgIHI6IDAsXG4gICAgeDogMCxcbiAgICB5OiAwLFxuICAgIHdpZHRoOiAwLFxuICAgIGhlaWdodDogMFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChjdHgsIHNoYXBlKSB7XG4gICAgdmFyIHg7XG4gICAgdmFyIHk7XG4gICAgdmFyIHdpZHRoO1xuICAgIHZhciBoZWlnaHQ7XG5cbiAgICBpZiAodGhpcy5zdWJQaXhlbE9wdGltaXplKSB7XG4gICAgICBzdWJQaXhlbE9wdGltaXplUmVjdChzdWJQaXhlbE9wdGltaXplT3V0cHV0U2hhcGUsIHNoYXBlLCB0aGlzLnN0eWxlKTtcbiAgICAgIHggPSBzdWJQaXhlbE9wdGltaXplT3V0cHV0U2hhcGUueDtcbiAgICAgIHkgPSBzdWJQaXhlbE9wdGltaXplT3V0cHV0U2hhcGUueTtcbiAgICAgIHdpZHRoID0gc3ViUGl4ZWxPcHRpbWl6ZU91dHB1dFNoYXBlLndpZHRoO1xuICAgICAgaGVpZ2h0ID0gc3ViUGl4ZWxPcHRpbWl6ZU91dHB1dFNoYXBlLmhlaWdodDtcbiAgICAgIHN1YlBpeGVsT3B0aW1pemVPdXRwdXRTaGFwZS5yID0gc2hhcGUucjtcbiAgICAgIHNoYXBlID0gc3ViUGl4ZWxPcHRpbWl6ZU91dHB1dFNoYXBlO1xuICAgIH0gZWxzZSB7XG4gICAgICB4ID0gc2hhcGUueDtcbiAgICAgIHkgPSBzaGFwZS55O1xuICAgICAgd2lkdGggPSBzaGFwZS53aWR0aDtcbiAgICAgIGhlaWdodCA9IHNoYXBlLmhlaWdodDtcbiAgICB9XG5cbiAgICBpZiAoIXNoYXBlLnIpIHtcbiAgICAgIGN0eC5yZWN0KHgsIHksIHdpZHRoLCBoZWlnaHQpO1xuICAgIH0gZWxzZSB7XG4gICAgICByb3VuZFJlY3RIZWxwZXIuYnVpbGRQYXRoKGN0eCwgc2hhcGUpO1xuICAgIH1cblxuICAgIGN0eC5jbG9zZVBhdGgoKTtcbiAgICByZXR1cm47XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7OztBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNDQTtBQUNBO0FBNkNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Rect.js
