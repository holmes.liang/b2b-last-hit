/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var axisDefault = __webpack_require__(/*! ./axisDefault */ "./node_modules/echarts/lib/coord/axisDefault.js");

var ComponentModel = __webpack_require__(/*! ../model/Component */ "./node_modules/echarts/lib/model/Component.js");

var _layout = __webpack_require__(/*! ../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutParams = _layout.getLayoutParams;
var mergeLayoutParam = _layout.mergeLayoutParam;

var OrdinalMeta = __webpack_require__(/*! ../data/OrdinalMeta */ "./node_modules/echarts/lib/data/OrdinalMeta.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// FIXME axisType is fixed ?


var AXIS_TYPES = ['value', 'category', 'time', 'log'];
/**
 * Generate sub axis model class
 * @param {string} axisName 'x' 'y' 'radius' 'angle' 'parallel'
 * @param {module:echarts/model/Component} BaseAxisModelClass
 * @param {Function} axisTypeDefaulter
 * @param {Object} [extraDefaultOption]
 */

function _default(axisName, BaseAxisModelClass, axisTypeDefaulter, extraDefaultOption) {
  zrUtil.each(AXIS_TYPES, function (axisType) {
    BaseAxisModelClass.extend({
      /**
       * @readOnly
       */
      type: axisName + 'Axis.' + axisType,
      mergeDefaultAndTheme: function mergeDefaultAndTheme(option, ecModel) {
        var layoutMode = this.layoutMode;
        var inputPositionParams = layoutMode ? getLayoutParams(option) : {};
        var themeModel = ecModel.getTheme();
        zrUtil.merge(option, themeModel.get(axisType + 'Axis'));
        zrUtil.merge(option, this.getDefaultOption());
        option.type = axisTypeDefaulter(axisName, option);

        if (layoutMode) {
          mergeLayoutParam(option, inputPositionParams, layoutMode);
        }
      },

      /**
       * @override
       */
      optionUpdated: function optionUpdated() {
        var thisOption = this.option;

        if (thisOption.type === 'category') {
          this.__ordinalMeta = OrdinalMeta.createByAxisModel(this);
        }
      },

      /**
       * Should not be called before all of 'getInitailData' finished.
       * Because categories are collected during initializing data.
       */
      getCategories: function getCategories(rawData) {
        var option = this.option; // FIXME
        // warning if called before all of 'getInitailData' finished.

        if (option.type === 'category') {
          if (rawData) {
            return option.data;
          }

          return this.__ordinalMeta.categories;
        }
      },
      getOrdinalMeta: function getOrdinalMeta() {
        return this.__ordinalMeta;
      },
      defaultOption: zrUtil.mergeAll([{}, axisDefault[axisType + 'Axis'], extraDefaultOption], true)
    });
  });
  ComponentModel.registerSubTypeDefaulter(axisName + 'Axis', zrUtil.curry(axisTypeDefaulter, axisName));
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvYXhpc01vZGVsQ3JlYXRvci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2F4aXNNb2RlbENyZWF0b3IuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgYXhpc0RlZmF1bHQgPSByZXF1aXJlKFwiLi9heGlzRGVmYXVsdFwiKTtcblxudmFyIENvbXBvbmVudE1vZGVsID0gcmVxdWlyZShcIi4uL21vZGVsL0NvbXBvbmVudFwiKTtcblxudmFyIF9sYXlvdXQgPSByZXF1aXJlKFwiLi4vdXRpbC9sYXlvdXRcIik7XG5cbnZhciBnZXRMYXlvdXRQYXJhbXMgPSBfbGF5b3V0LmdldExheW91dFBhcmFtcztcbnZhciBtZXJnZUxheW91dFBhcmFtID0gX2xheW91dC5tZXJnZUxheW91dFBhcmFtO1xuXG52YXIgT3JkaW5hbE1ldGEgPSByZXF1aXJlKFwiLi4vZGF0YS9PcmRpbmFsTWV0YVwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gRklYTUUgYXhpc1R5cGUgaXMgZml4ZWQgP1xudmFyIEFYSVNfVFlQRVMgPSBbJ3ZhbHVlJywgJ2NhdGVnb3J5JywgJ3RpbWUnLCAnbG9nJ107XG4vKipcbiAqIEdlbmVyYXRlIHN1YiBheGlzIG1vZGVsIGNsYXNzXG4gKiBAcGFyYW0ge3N0cmluZ30gYXhpc05hbWUgJ3gnICd5JyAncmFkaXVzJyAnYW5nbGUnICdwYXJhbGxlbCdcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvQ29tcG9uZW50fSBCYXNlQXhpc01vZGVsQ2xhc3NcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGF4aXNUeXBlRGVmYXVsdGVyXG4gKiBAcGFyYW0ge09iamVjdH0gW2V4dHJhRGVmYXVsdE9wdGlvbl1cbiAqL1xuXG5mdW5jdGlvbiBfZGVmYXVsdChheGlzTmFtZSwgQmFzZUF4aXNNb2RlbENsYXNzLCBheGlzVHlwZURlZmF1bHRlciwgZXh0cmFEZWZhdWx0T3B0aW9uKSB7XG4gIHpyVXRpbC5lYWNoKEFYSVNfVFlQRVMsIGZ1bmN0aW9uIChheGlzVHlwZSkge1xuICAgIEJhc2VBeGlzTW9kZWxDbGFzcy5leHRlbmQoe1xuICAgICAgLyoqXG4gICAgICAgKiBAcmVhZE9ubHlcbiAgICAgICAqL1xuICAgICAgdHlwZTogYXhpc05hbWUgKyAnQXhpcy4nICsgYXhpc1R5cGUsXG4gICAgICBtZXJnZURlZmF1bHRBbmRUaGVtZTogZnVuY3Rpb24gKG9wdGlvbiwgZWNNb2RlbCkge1xuICAgICAgICB2YXIgbGF5b3V0TW9kZSA9IHRoaXMubGF5b3V0TW9kZTtcbiAgICAgICAgdmFyIGlucHV0UG9zaXRpb25QYXJhbXMgPSBsYXlvdXRNb2RlID8gZ2V0TGF5b3V0UGFyYW1zKG9wdGlvbikgOiB7fTtcbiAgICAgICAgdmFyIHRoZW1lTW9kZWwgPSBlY01vZGVsLmdldFRoZW1lKCk7XG4gICAgICAgIHpyVXRpbC5tZXJnZShvcHRpb24sIHRoZW1lTW9kZWwuZ2V0KGF4aXNUeXBlICsgJ0F4aXMnKSk7XG4gICAgICAgIHpyVXRpbC5tZXJnZShvcHRpb24sIHRoaXMuZ2V0RGVmYXVsdE9wdGlvbigpKTtcbiAgICAgICAgb3B0aW9uLnR5cGUgPSBheGlzVHlwZURlZmF1bHRlcihheGlzTmFtZSwgb3B0aW9uKTtcblxuICAgICAgICBpZiAobGF5b3V0TW9kZSkge1xuICAgICAgICAgIG1lcmdlTGF5b3V0UGFyYW0ob3B0aW9uLCBpbnB1dFBvc2l0aW9uUGFyYW1zLCBsYXlvdXRNb2RlKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBAb3ZlcnJpZGVcbiAgICAgICAqL1xuICAgICAgb3B0aW9uVXBkYXRlZDogZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgdGhpc09wdGlvbiA9IHRoaXMub3B0aW9uO1xuXG4gICAgICAgIGlmICh0aGlzT3B0aW9uLnR5cGUgPT09ICdjYXRlZ29yeScpIHtcbiAgICAgICAgICB0aGlzLl9fb3JkaW5hbE1ldGEgPSBPcmRpbmFsTWV0YS5jcmVhdGVCeUF4aXNNb2RlbCh0aGlzKTtcbiAgICAgICAgfVxuICAgICAgfSxcblxuICAgICAgLyoqXG4gICAgICAgKiBTaG91bGQgbm90IGJlIGNhbGxlZCBiZWZvcmUgYWxsIG9mICdnZXRJbml0YWlsRGF0YScgZmluaXNoZWQuXG4gICAgICAgKiBCZWNhdXNlIGNhdGVnb3JpZXMgYXJlIGNvbGxlY3RlZCBkdXJpbmcgaW5pdGlhbGl6aW5nIGRhdGEuXG4gICAgICAgKi9cbiAgICAgIGdldENhdGVnb3JpZXM6IGZ1bmN0aW9uIChyYXdEYXRhKSB7XG4gICAgICAgIHZhciBvcHRpb24gPSB0aGlzLm9wdGlvbjsgLy8gRklYTUVcbiAgICAgICAgLy8gd2FybmluZyBpZiBjYWxsZWQgYmVmb3JlIGFsbCBvZiAnZ2V0SW5pdGFpbERhdGEnIGZpbmlzaGVkLlxuXG4gICAgICAgIGlmIChvcHRpb24udHlwZSA9PT0gJ2NhdGVnb3J5Jykge1xuICAgICAgICAgIGlmIChyYXdEYXRhKSB7XG4gICAgICAgICAgICByZXR1cm4gb3B0aW9uLmRhdGE7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIHRoaXMuX19vcmRpbmFsTWV0YS5jYXRlZ29yaWVzO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgZ2V0T3JkaW5hbE1ldGE6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX19vcmRpbmFsTWV0YTtcbiAgICAgIH0sXG4gICAgICBkZWZhdWx0T3B0aW9uOiB6clV0aWwubWVyZ2VBbGwoW3t9LCBheGlzRGVmYXVsdFtheGlzVHlwZSArICdBeGlzJ10sIGV4dHJhRGVmYXVsdE9wdGlvbl0sIHRydWUpXG4gICAgfSk7XG4gIH0pO1xuICBDb21wb25lbnRNb2RlbC5yZWdpc3RlclN1YlR5cGVEZWZhdWx0ZXIoYXhpc05hbWUgKyAnQXhpcycsIHpyVXRpbC5jdXJyeShheGlzVHlwZURlZmF1bHRlciwgYXhpc05hbWUpKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFEQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaERBO0FBa0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/axisModelCreator.js
