__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicSubmit", function() { return BasicSubmit; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/SAIC/all-steps.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common/utils */ "./src/common/utils.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-submit.tsx";








var isMobile = _common_utils__WEBPACK_IMPORTED_MODULE_13__["default"].getIsMobile();

var BasicSubmit =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(BasicSubmit, _QuoteStep);

  function BasicSubmit() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, BasicSubmit);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicSubmit)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.handleConfirm = function () {};

    _this.handleSubmit =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicSubmit, [{
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        disabled: true,
        uwLoading: false
      };
    }
  }, {
    key: "renderActions",
    value: function renderActions() {}
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this2 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_14__["ButtonBack"], {
        handel: function handel() {
          Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_10__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_11__["AllSteps"].ATTACHMENTS);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        size: "large",
        type: "primary",
        disabled: this.state.disabled,
        onClick: this.handleConfirm,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Submit").thai("Submit").my("Submit").getMessage()));
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, null);
    }
  }, {
    key: "renderViewComponent",
    value: function renderViewComponent() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].Fragment, null);
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this3 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 67
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Policy Confirmation").thai("สรุปรายละเอียดกรมธรรม์").my("မူဝါဒ CONFIRMATION").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "quote",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, this.renderViewComponent(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "confirm-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Checkbox"], {
        onChange: function onChange(e) {
          _this3.setState({
            disabled: !e.target.checked
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("I confirm all information above are real and correct.").thai("ฉันยืนยันว่าข้อมูลทั้งหมดข้างต้นเป็นจริงและถูกต้อง").my("ငါမူကားအထက်အချက်အလက်အားလုံးကိုအတည်ပြုအစစ်အမှန်နှင့်မှန်ကန်သောဖြစ်ကြသည်။").getMessage())), this.renderRightActions()))));
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }]);

  return BasicSubmit;
}(_quote_step__WEBPACK_IMPORTED_MODULE_8__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtc3VibWl0LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL3F1b3RlL1NBSUMvaWFyL2Jhc2ljLXN1Ym1pdC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCBRdW90ZVN0ZXAsIHsgU3RlcENvbXBvbmVudHMsIFN0ZXBQcm9wcyB9IGZyb20gXCIuLi8uLi9xdW90ZS1zdGVwXCI7XG5pbXBvcnQgeyBCdXR0b24sIENoZWNrYm94LCBNb2RhbCwgSW5wdXQsIG5vdGlmaWNhdGlvbiwgU3BpbiwgUm93LCBDb2wgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgcGFnZVRvIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC93aXphcmRcIjtcbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IHsgQXBpcywgTGFuZ3VhZ2UsIFBBVEggfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHV0aWxzIGZyb20gXCJAY29tbW9uL3V0aWxzXCI7XG5pbXBvcnQgeyBCdXR0b25CYWNrIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9hbnRkL2J1dHRvbi1iYWNrXCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gdXRpbHMuZ2V0SXNNb2JpbGUoKTtcbnR5cGUgQ29uZmlybVN0YXRlID0ge1xuICBkaXNhYmxlZDogYm9vbGVhbjtcbiAgdXdMb2FkaW5nOiBib29sZWFuO1xuICBsb2FkaW5nOiBib29sZWFuO1xufVxuXG5jbGFzcyBCYXNpY1N1Ym1pdDxQIGV4dGVuZHMgU3RlcFByb3BzLCBTIGV4dGVuZHMgQ29uZmlybVN0YXRlLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4ge1xuICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICBkaXNhYmxlZDogdHJ1ZSxcbiAgICAgIHV3TG9hZGluZzogZmFsc2UsXG4gICAgfSBhcyBTO1xuICB9XG5cbiAgaGFuZGxlQ29uZmlybSA9ICgpOiB2b2lkID0+IHtcbiAgfTtcblxuICBwcm90ZWN0ZWQgcmVuZGVyQWN0aW9ucygpIHtcblxuICB9XG5cbiAgcmVuZGVyUmlnaHRBY3Rpb25zKCkge1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtgYWN0aW9uICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1hY3Rpb24tbGFyZ2VcIiA6IFwiXCJ9YH0+XG5cbiAgICAgICAgPEJ1dHRvbkJhY2tcbiAgICAgICAgICBoYW5kZWw9eygpID0+IHtcbiAgICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5BVFRBQ0hNRU5UUyk7XG4gICAgICAgICAgfX1cbiAgICAgICAgLz5cblxuICAgICAgICA8QnV0dG9uIHNpemU9XCJsYXJnZVwiIHR5cGU9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICBkaXNhYmxlZD17dGhpcy5zdGF0ZS5kaXNhYmxlZH0gb25DbGljaz17dGhpcy5oYW5kbGVDb25maXJtfT5cbiAgICAgICAgICB7TGFuZ3VhZ2UuZW4oXCJTdWJtaXRcIikudGhhaShcIlN1Ym1pdFwiKS5teShcIlN1Ym1pdFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgIDwvQnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIGhhbmRsZVN1Ym1pdCA9IGFzeW5jICgpID0+IHtcbiAgfTtcblxuICByZW5kZXJWaWV3Q29tcG9uZW50KCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDb250ZW50KCkge1xuICAgIGNvbnN0IHsgZm9ybSwgbW9kZWwgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxTcGluIHNpemU9XCJsYXJnZVwiIHNwaW5uaW5nPXt0aGlzLnN0YXRlLmxvYWRpbmd9PlxuICAgICAgICA8Um93IHR5cGU9XCJmbGV4XCIganVzdGlmeT17XCJzcGFjZS1iZXR3ZWVuXCJ9PlxuICAgICAgICAgIHt0aGlzLnJlbmRlclNpZGVJbmZvKCl9XG4gICAgICAgICAgPENvbCBzbT17MTh9IHhzPXsyM30+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlBvbGljeSBDb25maXJtYXRpb25cIikudGhhaShcIuC4quC4o+C4uOC4m+C4o+C4suC4ouC4peC4sOC5gOC4reC4teC4ouC4lOC4geC4o+C4oeC4mOC4o+C4o+C4oeC5jFwiKS5teShcIuGAmeGAsOGAneGAq+GAkiBDT05GSVJNQVRJT05cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInF1b3RlXCI+XG4gICAgICAgICAgICAgIHt0aGlzLnJlbmRlclZpZXdDb21wb25lbnQoKX1cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1wiY29uZmlybS1pbmZvXCJ9PlxuICAgICAgICAgICAgICAgIDxDaGVja2JveCBvbkNoYW5nZT17KGU6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiAhZS50YXJnZXQuY2hlY2tlZCxcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH19PntcbiAgICAgICAgICAgICAgICAgIExhbmd1YWdlLmVuKFwiSSBjb25maXJtIGFsbCBpbmZvcm1hdGlvbiBhYm92ZSBhcmUgcmVhbCBhbmQgY29ycmVjdC5cIikudGhhaShcIuC4ieC4seC4meC4ouC4t+C4meC4ouC4seC4meC4p+C5iOC4suC4guC5ieC4reC4oeC4ueC4peC4l+C4seC5ieC4h+C4q+C4oeC4lOC4guC5ieC4suC4h+C4leC5ieC4meC5gOC4m+C5h+C4meC4iOC4o+C4tOC4h+C5geC4peC4sOC4luC4ueC4geC4leC5ieC4reC4h1wiKS5teShcIuGAhOGAq+GAmeGAsOGAgOGArOGAuOGAoeGAkeGAgOGAuuGAoeGAgeGAu+GAgOGAuuGAoeGAnOGAgOGAuuGAoeGArOGAuOGAnOGAr+GAtuGAuOGAgOGAreGAr+GAoeGAkOGAiuGAuuGAleGAvOGAr+GAoeGAheGAheGAuuGAoeGAmeGAvuGAlOGAuuGAlOGAvuGAhOGAuuGAt+GAmeGAvuGAlOGAuuGAgOGAlOGAuuGAnuGAseGArOGAluGAvOGAheGAuuGAgOGAvOGAnuGAiuGAuuGBi1wiKS5nZXRNZXNzYWdlKClcbiAgICAgICAgICAgICAgICB9PC9DaGVja2JveD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIHt0aGlzLnJlbmRlclJpZ2h0QWN0aW9ucygpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9Db2w+XG4gICAgICAgIDwvUm93PlxuICAgICAgPC9TcGluPlxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyVGl0bGUoKSB7XG4gIH1cbn1cblxuZXhwb3J0IHsgQmFzaWNTdWJtaXQgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQU1BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFTQTtBQUNBO0FBNkJBOzs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7O0FBdENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBOzs7QUFLQTs7O0FBSUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTs7O0FBS0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFjQTs7O0FBRUE7Ozs7QUEzRUE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-submit.tsx
