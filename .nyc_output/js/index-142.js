__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Comment; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};





function getAction(actions) {
  if (!actions || !actions.length) {
    return null;
  } // eslint-disable-next-line react/no-array-index-key


  var actionList = actions.map(function (action, index) {
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", {
      key: "action-".concat(index)
    }, action);
  });
  return actionList;
}

var Comment =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Comment, _React$Component);

  function Comment() {
    var _this;

    _classCallCheck(this, Comment);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Comment).apply(this, arguments));

    _this.renderNested = function (prefixCls, children) {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-nested"))
      }, children);
    };

    _this.renderComment = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          actions = _a.actions,
          author = _a.author,
          avatar = _a.avatar,
          children = _a.children,
          className = _a.className,
          content = _a.content,
          customizePrefixCls = _a.prefixCls,
          style = _a.style,
          datetime = _a.datetime,
          otherProps = __rest(_a, ["actions", "author", "avatar", "children", "className", "content", "prefixCls", "style", "datetime"]);

      var prefixCls = getPrefixCls('comment', customizePrefixCls);
      var avatarDom = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-avatar")
      }, typeof avatar === 'string' ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", {
        src: avatar,
        alt: "comment-avatar"
      }) : avatar);
      var actionDom = actions && actions.length ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", {
        className: "".concat(prefixCls, "-actions")
      }, getAction(actions)) : null;
      var authorContent = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-content-author")
      }, author && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-content-author-name")
      }, author), datetime && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-content-author-time")
      }, datetime));
      var contentDom = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-content")
      }, authorContent, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-content-detail")
      }, content), actionDom);
      var comment = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-inner")
      }, avatarDom, contentDom);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, otherProps, {
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className),
        style: style
      }), comment, children ? _this.renderNested(prefixCls, children) : null);
    };

    return _this;
  }

  _createClass(Comment, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, this.renderComment);
    }
  }]);

  return Comment;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9jb21tZW50L2luZGV4LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9jb21tZW50L2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmZ1bmN0aW9uIGdldEFjdGlvbihhY3Rpb25zKSB7XG4gICAgaWYgKCFhY3Rpb25zIHx8ICFhY3Rpb25zLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0L25vLWFycmF5LWluZGV4LWtleVxuICAgIGNvbnN0IGFjdGlvbkxpc3QgPSBhY3Rpb25zLm1hcCgoYWN0aW9uLCBpbmRleCkgPT4gPGxpIGtleT17YGFjdGlvbi0ke2luZGV4fWB9PnthY3Rpb259PC9saT4pO1xuICAgIHJldHVybiBhY3Rpb25MaXN0O1xufVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29tbWVudCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMucmVuZGVyTmVzdGVkID0gKHByZWZpeENscywgY2hpbGRyZW4pID0+IHtcbiAgICAgICAgICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LW5lc3RlZGApfT57Y2hpbGRyZW59PC9kaXY+O1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckNvbW1lbnQgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IGFjdGlvbnMsIGF1dGhvciwgYXZhdGFyLCBjaGlsZHJlbiwgY2xhc3NOYW1lLCBjb250ZW50LCBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgc3R5bGUsIGRhdGV0aW1lIH0gPSBfYSwgb3RoZXJQcm9wcyA9IF9fcmVzdChfYSwgW1wiYWN0aW9uc1wiLCBcImF1dGhvclwiLCBcImF2YXRhclwiLCBcImNoaWxkcmVuXCIsIFwiY2xhc3NOYW1lXCIsIFwiY29udGVudFwiLCBcInByZWZpeENsc1wiLCBcInN0eWxlXCIsIFwiZGF0ZXRpbWVcIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdjb21tZW50JywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGF2YXRhckRvbSA9ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1hdmF0YXJgfT5cbiAgICAgICAge3R5cGVvZiBhdmF0YXIgPT09ICdzdHJpbmcnID8gPGltZyBzcmM9e2F2YXRhcn0gYWx0PVwiY29tbWVudC1hdmF0YXJcIi8+IDogYXZhdGFyfVxuICAgICAgPC9kaXY+KTtcbiAgICAgICAgICAgIGNvbnN0IGFjdGlvbkRvbSA9IGFjdGlvbnMgJiYgYWN0aW9ucy5sZW5ndGggPyAoPHVsIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1hY3Rpb25zYH0+e2dldEFjdGlvbihhY3Rpb25zKX08L3VsPikgOiBudWxsO1xuICAgICAgICAgICAgY29uc3QgYXV0aG9yQ29udGVudCA9ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50LWF1dGhvcmB9PlxuICAgICAgICB7YXV0aG9yICYmIDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50LWF1dGhvci1uYW1lYH0+e2F1dGhvcn08L3NwYW4+fVxuICAgICAgICB7ZGF0ZXRpbWUgJiYgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtYXV0aG9yLXRpbWVgfT57ZGF0ZXRpbWV9PC9zcGFuPn1cbiAgICAgIDwvZGl2Pik7XG4gICAgICAgICAgICBjb25zdCBjb250ZW50RG9tID0gKDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnRgfT5cbiAgICAgICAge2F1dGhvckNvbnRlbnR9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtZGV0YWlsYH0+e2NvbnRlbnR9PC9kaXY+XG4gICAgICAgIHthY3Rpb25Eb219XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgY29uc3QgY29tbWVudCA9ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pbm5lcmB9PlxuICAgICAgICB7YXZhdGFyRG9tfVxuICAgICAgICB7Y29udGVudERvbX1cbiAgICAgIDwvZGl2Pik7XG4gICAgICAgICAgICByZXR1cm4gKDxkaXYgey4uLm90aGVyUHJvcHN9IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSl9IHN0eWxlPXtzdHlsZX0+XG4gICAgICAgIHtjb21tZW50fVxuICAgICAgICB7Y2hpbGRyZW4gPyB0aGlzLnJlbmRlck5lc3RlZChwcmVmaXhDbHMsIGNoaWxkcmVuKSA6IG51bGx9XG4gICAgICA8L2Rpdj4pO1xuICAgICAgICB9O1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQ29tbWVudH08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBcEJBO0FBQ0E7QUFOQTtBQThCQTtBQUNBOzs7QUFBQTtBQUNBO0FBQ0E7Ozs7QUFsQ0E7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/comment/index.js
