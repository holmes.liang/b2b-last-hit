/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Pick color from palette for each data item.
// Applicable for charts that require applying color palette
// in data level (like pie, funnel, chord).

function _default(seriesType) {
  return {
    getTargetSeries: function getTargetSeries(ecModel) {
      // Pie and funnel may use diferrent scope
      var paletteScope = {};
      var seiresModelMap = createHashMap();
      ecModel.eachSeriesByType(seriesType, function (seriesModel) {
        seriesModel.__paletteScope = paletteScope;
        seiresModelMap.set(seriesModel.uid, seriesModel);
      });
      return seiresModelMap;
    },
    reset: function reset(seriesModel, ecModel) {
      var dataAll = seriesModel.getRawData();
      var idxMap = {};
      var data = seriesModel.getData();
      data.each(function (idx) {
        var rawIdx = data.getRawIndex(idx);
        idxMap[rawIdx] = idx;
      });
      dataAll.each(function (rawIdx) {
        var filteredIdx = idxMap[rawIdx]; // If series.itemStyle.normal.color is a function. itemVisual may be encoded

        var singleDataColor = filteredIdx != null && data.getItemVisual(filteredIdx, 'color', true);

        if (!singleDataColor) {
          // FIXME Performance
          var itemModel = dataAll.getItemModel(rawIdx);
          var color = itemModel.get('itemStyle.color') || seriesModel.getColorFromPalette(dataAll.getName(rawIdx) || rawIdx + '', seriesModel.__paletteScope, dataAll.count()); // Legend may use the visual info in data before processed

          dataAll.setItemVisual(rawIdx, 'color', color); // Data is not filtered

          if (filteredIdx != null) {
            data.setItemVisual(filteredIdx, 'color', color);
          }
        } else {
          // Set data all color for legend
          dataAll.setItemVisual(rawIdx, 'color', singleDataColor);
        }
      });
    }
  };
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlzdWFsL2RhdGFDb2xvci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3Zpc3VhbC9kYXRhQ29sb3IuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBjcmVhdGVIYXNoTWFwID0gX3V0aWwuY3JlYXRlSGFzaE1hcDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gUGljayBjb2xvciBmcm9tIHBhbGV0dGUgZm9yIGVhY2ggZGF0YSBpdGVtLlxuLy8gQXBwbGljYWJsZSBmb3IgY2hhcnRzIHRoYXQgcmVxdWlyZSBhcHBseWluZyBjb2xvciBwYWxldHRlXG4vLyBpbiBkYXRhIGxldmVsIChsaWtlIHBpZSwgZnVubmVsLCBjaG9yZCkuXG5mdW5jdGlvbiBfZGVmYXVsdChzZXJpZXNUeXBlKSB7XG4gIHJldHVybiB7XG4gICAgZ2V0VGFyZ2V0U2VyaWVzOiBmdW5jdGlvbiAoZWNNb2RlbCkge1xuICAgICAgLy8gUGllIGFuZCBmdW5uZWwgbWF5IHVzZSBkaWZlcnJlbnQgc2NvcGVcbiAgICAgIHZhciBwYWxldHRlU2NvcGUgPSB7fTtcbiAgICAgIHZhciBzZWlyZXNNb2RlbE1hcCA9IGNyZWF0ZUhhc2hNYXAoKTtcbiAgICAgIGVjTW9kZWwuZWFjaFNlcmllc0J5VHlwZShzZXJpZXNUeXBlLCBmdW5jdGlvbiAoc2VyaWVzTW9kZWwpIHtcbiAgICAgICAgc2VyaWVzTW9kZWwuX19wYWxldHRlU2NvcGUgPSBwYWxldHRlU2NvcGU7XG4gICAgICAgIHNlaXJlc01vZGVsTWFwLnNldChzZXJpZXNNb2RlbC51aWQsIHNlcmllc01vZGVsKTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHNlaXJlc01vZGVsTWFwO1xuICAgIH0sXG4gICAgcmVzZXQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCkge1xuICAgICAgdmFyIGRhdGFBbGwgPSBzZXJpZXNNb2RlbC5nZXRSYXdEYXRhKCk7XG4gICAgICB2YXIgaWR4TWFwID0ge307XG4gICAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICAgIGRhdGEuZWFjaChmdW5jdGlvbiAoaWR4KSB7XG4gICAgICAgIHZhciByYXdJZHggPSBkYXRhLmdldFJhd0luZGV4KGlkeCk7XG4gICAgICAgIGlkeE1hcFtyYXdJZHhdID0gaWR4O1xuICAgICAgfSk7XG4gICAgICBkYXRhQWxsLmVhY2goZnVuY3Rpb24gKHJhd0lkeCkge1xuICAgICAgICB2YXIgZmlsdGVyZWRJZHggPSBpZHhNYXBbcmF3SWR4XTsgLy8gSWYgc2VyaWVzLml0ZW1TdHlsZS5ub3JtYWwuY29sb3IgaXMgYSBmdW5jdGlvbi4gaXRlbVZpc3VhbCBtYXkgYmUgZW5jb2RlZFxuXG4gICAgICAgIHZhciBzaW5nbGVEYXRhQ29sb3IgPSBmaWx0ZXJlZElkeCAhPSBudWxsICYmIGRhdGEuZ2V0SXRlbVZpc3VhbChmaWx0ZXJlZElkeCwgJ2NvbG9yJywgdHJ1ZSk7XG5cbiAgICAgICAgaWYgKCFzaW5nbGVEYXRhQ29sb3IpIHtcbiAgICAgICAgICAvLyBGSVhNRSBQZXJmb3JtYW5jZVxuICAgICAgICAgIHZhciBpdGVtTW9kZWwgPSBkYXRhQWxsLmdldEl0ZW1Nb2RlbChyYXdJZHgpO1xuICAgICAgICAgIHZhciBjb2xvciA9IGl0ZW1Nb2RlbC5nZXQoJ2l0ZW1TdHlsZS5jb2xvcicpIHx8IHNlcmllc01vZGVsLmdldENvbG9yRnJvbVBhbGV0dGUoZGF0YUFsbC5nZXROYW1lKHJhd0lkeCkgfHwgcmF3SWR4ICsgJycsIHNlcmllc01vZGVsLl9fcGFsZXR0ZVNjb3BlLCBkYXRhQWxsLmNvdW50KCkpOyAvLyBMZWdlbmQgbWF5IHVzZSB0aGUgdmlzdWFsIGluZm8gaW4gZGF0YSBiZWZvcmUgcHJvY2Vzc2VkXG5cbiAgICAgICAgICBkYXRhQWxsLnNldEl0ZW1WaXN1YWwocmF3SWR4LCAnY29sb3InLCBjb2xvcik7IC8vIERhdGEgaXMgbm90IGZpbHRlcmVkXG5cbiAgICAgICAgICBpZiAoZmlsdGVyZWRJZHggIT0gbnVsbCkge1xuICAgICAgICAgICAgZGF0YS5zZXRJdGVtVmlzdWFsKGZpbHRlcmVkSWR4LCAnY29sb3InLCBjb2xvcik7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIFNldCBkYXRhIGFsbCBjb2xvciBmb3IgbGVnZW5kXG4gICAgICAgICAgZGF0YUFsbC5zZXRJdGVtVmlzdWFsKHJhd0lkeCwgJ2NvbG9yJywgc2luZ2xlRGF0YUNvbG9yKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXZDQTtBQXlDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/visual/dataColor.js
