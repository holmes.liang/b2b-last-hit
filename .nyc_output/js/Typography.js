__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
/* harmony import */ var _util_ref__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_util/ref */ "./node_modules/antd/es/_util/ref.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};







var Typography = function Typography(_a, ref) {
  var customizePrefixCls = _a.prefixCls,
      _a$component = _a.component,
      component = _a$component === void 0 ? 'article' : _a$component,
      className = _a.className,
      ariaLabel = _a['aria-label'],
      setContentRef = _a.setContentRef,
      children = _a.children,
      restProps = __rest(_a, ["prefixCls", "component", "className", 'aria-label', "setContentRef", "children"]);

  var mergedRef = ref;

  if (setContentRef) {
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_3__["default"])(false, 'Typography', '`setContentRef` is deprecated. Please use `ref` instead.');
    mergedRef = Object(_util_ref__WEBPACK_IMPORTED_MODULE_4__["composeRef"])(ref, setContentRef);
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref) {
    var getPrefixCls = _ref.getPrefixCls;
    var Component = component;
    var prefixCls = getPrefixCls('typography', customizePrefixCls);
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Component, _extends({
      className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, className),
      "aria-label": ariaLabel,
      ref: mergedRef
    }, restProps), children);
  });
};

var RefTypography;

if (react__WEBPACK_IMPORTED_MODULE_0__["forwardRef"]) {
  RefTypography = react__WEBPACK_IMPORTED_MODULE_0__["forwardRef"](Typography);
  RefTypography.displayName = 'Typography';
} else {
  var TypographyWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(TypographyWrapper, _React$Component);

    function TypographyWrapper() {
      var _this;

      _classCallCheck(this, TypographyWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(TypographyWrapper).apply(this, arguments));
      _this.state = {};
      return _this;
    }

    _createClass(TypographyWrapper, [{
      key: "render",
      value: function render() {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Typography, this.props);
      }
    }]);

    return TypographyWrapper;
  }(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

  RefTypography = TypographyWrapper;
} // es default export should use const instead of let


var ExportTypography = RefTypography;
/* harmony default export */ __webpack_exports__["default"] = (ExportTypography);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L1R5cG9ncmFwaHkuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3R5cG9ncmFwaHkvVHlwb2dyYXBoeS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgd2FybmluZyBmcm9tICcuLi9fdXRpbC93YXJuaW5nJztcbmltcG9ydCB7IGNvbXBvc2VSZWYgfSBmcm9tICcuLi9fdXRpbC9yZWYnO1xuY29uc3QgVHlwb2dyYXBoeSA9IChfYSwgcmVmKSA9PiB7XG4gICAgdmFyIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNvbXBvbmVudCA9ICdhcnRpY2xlJywgY2xhc3NOYW1lLCAnYXJpYS1sYWJlbCc6IGFyaWFMYWJlbCwgc2V0Q29udGVudFJlZiwgY2hpbGRyZW4gfSA9IF9hLCByZXN0UHJvcHMgPSBfX3Jlc3QoX2EsIFtcInByZWZpeENsc1wiLCBcImNvbXBvbmVudFwiLCBcImNsYXNzTmFtZVwiLCAnYXJpYS1sYWJlbCcsIFwic2V0Q29udGVudFJlZlwiLCBcImNoaWxkcmVuXCJdKTtcbiAgICBsZXQgbWVyZ2VkUmVmID0gcmVmO1xuICAgIGlmIChzZXRDb250ZW50UmVmKSB7XG4gICAgICAgIHdhcm5pbmcoZmFsc2UsICdUeXBvZ3JhcGh5JywgJ2BzZXRDb250ZW50UmVmYCBpcyBkZXByZWNhdGVkLiBQbGVhc2UgdXNlIGByZWZgIGluc3RlYWQuJyk7XG4gICAgICAgIG1lcmdlZFJlZiA9IGNvbXBvc2VSZWYocmVmLCBzZXRDb250ZW50UmVmKTtcbiAgICB9XG4gICAgcmV0dXJuICg8Q29uZmlnQ29uc3VtZXI+XG4gICAgICB7KHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgY29uc3QgQ29tcG9uZW50ID0gY29tcG9uZW50O1xuICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ3R5cG9ncmFwaHknLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICByZXR1cm4gKDxDb21wb25lbnQgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHByZWZpeENscywgY2xhc3NOYW1lKX0gYXJpYS1sYWJlbD17YXJpYUxhYmVsfSByZWY9e21lcmdlZFJlZn0gey4uLnJlc3RQcm9wc30+XG4gICAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgICAgPC9Db21wb25lbnQ+KTtcbiAgICB9fVxuICAgIDwvQ29uZmlnQ29uc3VtZXI+KTtcbn07XG5sZXQgUmVmVHlwb2dyYXBoeTtcbmlmIChSZWFjdC5mb3J3YXJkUmVmKSB7XG4gICAgUmVmVHlwb2dyYXBoeSA9IFJlYWN0LmZvcndhcmRSZWYoVHlwb2dyYXBoeSk7XG4gICAgUmVmVHlwb2dyYXBoeS5kaXNwbGF5TmFtZSA9ICdUeXBvZ3JhcGh5Jztcbn1cbmVsc2Uge1xuICAgIGNsYXNzIFR5cG9ncmFwaHlXcmFwcGVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICAgICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICAgICAgdGhpcy5zdGF0ZSA9IHt9O1xuICAgICAgICB9XG4gICAgICAgIHJlbmRlcigpIHtcbiAgICAgICAgICAgIHJldHVybiA8VHlwb2dyYXBoeSB7Li4udGhpcy5wcm9wc30vPjtcbiAgICAgICAgfVxuICAgIH1cbiAgICBSZWZUeXBvZ3JhcGh5ID0gVHlwb2dyYXBoeVdyYXBwZXI7XG59XG4vLyBlcyBkZWZhdWx0IGV4cG9ydCBzaG91bGQgdXNlIGNvbnN0IGluc3RlYWQgb2YgbGV0XG5jb25zdCBFeHBvcnRUeXBvZ3JhcGh5ID0gUmVmVHlwb2dyYXBoeTtcbmV4cG9ydCBkZWZhdWx0IEV4cG9ydFR5cG9ncmFwaHk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQVBBO0FBQ0E7QUFnQkE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFHQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQVJBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFTQTtBQUVBO0FBQ0E7QUFDQTtBQURBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/Typography.js
