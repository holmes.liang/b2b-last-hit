

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.responsiveMap = exports.responsiveArray = void 0;

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
} // matchMedia polyfill for
// https://github.com/WickyNilliams/enquire.js/issues/82


var enquire; // TODO: Will be removed in antd 4.0 because we will no longer support ie9

if (typeof window !== 'undefined') {
  var matchMediaPolyfill = function matchMediaPolyfill(mediaQuery) {
    return {
      media: mediaQuery,
      matches: false,
      addListener: function addListener() {},
      removeListener: function removeListener() {}
    };
  }; // ref: https://github.com/ant-design/ant-design/issues/18774


  if (!window.matchMedia) window.matchMedia = matchMediaPolyfill; // eslint-disable-next-line global-require

  enquire = __webpack_require__(/*! enquire.js */ "./node_modules/enquire.js/src/index.js");
}

var responsiveArray = ['xxl', 'xl', 'lg', 'md', 'sm', 'xs'];
exports.responsiveArray = responsiveArray;
var responsiveMap = {
  xs: '(max-width: 575px)',
  sm: '(min-width: 576px)',
  md: '(min-width: 768px)',
  lg: '(min-width: 992px)',
  xl: '(min-width: 1200px)',
  xxl: '(min-width: 1600px)'
};
exports.responsiveMap = responsiveMap;
var subscribers = [];
var subUid = -1;
var screens = {};
var responsiveObserve = {
  dispatch: function dispatch(pointMap) {
    screens = pointMap;

    if (subscribers.length < 1) {
      return false;
    }

    subscribers.forEach(function (item) {
      item.func(screens);
    });
    return true;
  },
  subscribe: function subscribe(func) {
    if (subscribers.length === 0) {
      this.register();
    }

    var token = (++subUid).toString();
    subscribers.push({
      token: token,
      func: func
    });
    func(screens);
    return token;
  },
  unsubscribe: function unsubscribe(token) {
    subscribers = subscribers.filter(function (item) {
      return item.token !== token;
    });

    if (subscribers.length === 0) {
      this.unregister();
    }
  },
  unregister: function unregister() {
    Object.keys(responsiveMap).map(function (screen) {
      return enquire.unregister(responsiveMap[screen]);
    });
  },
  register: function register() {
    var _this = this;

    Object.keys(responsiveMap).map(function (screen) {
      return enquire.register(responsiveMap[screen], {
        match: function match() {
          var pointMap = _extends(_extends({}, screens), _defineProperty({}, screen, true));

          _this.dispatch(pointMap);
        },
        unmatch: function unmatch() {
          var pointMap = _extends(_extends({}, screens), _defineProperty({}, screen, false));

          _this.dispatch(pointMap);
        },
        // Keep a empty destory to avoid triggering unmatch when unregister
        destroy: function destroy() {}
      });
    });
  }
};
var _default = responsiveObserve;
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvX3V0aWwvcmVzcG9uc2l2ZU9ic2VydmUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL191dGlsL3Jlc3BvbnNpdmVPYnNlcnZlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIG1hdGNoTWVkaWEgcG9seWZpbGwgZm9yXG4vLyBodHRwczovL2dpdGh1Yi5jb20vV2lja3lOaWxsaWFtcy9lbnF1aXJlLmpzL2lzc3Vlcy84MlxubGV0IGVucXVpcmU7XG4vLyBUT0RPOiBXaWxsIGJlIHJlbW92ZWQgaW4gYW50ZCA0LjAgYmVjYXVzZSB3ZSB3aWxsIG5vIGxvbmdlciBzdXBwb3J0IGllOVxuaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgY29uc3QgbWF0Y2hNZWRpYVBvbHlmaWxsID0gKG1lZGlhUXVlcnkpID0+IHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIG1lZGlhOiBtZWRpYVF1ZXJ5LFxuICAgICAgICAgICAgbWF0Y2hlczogZmFsc2UsXG4gICAgICAgICAgICBhZGRMaXN0ZW5lcigpIHsgfSxcbiAgICAgICAgICAgIHJlbW92ZUxpc3RlbmVyKCkgeyB9LFxuICAgICAgICB9O1xuICAgIH07XG4gICAgLy8gcmVmOiBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xODc3NFxuICAgIGlmICghd2luZG93Lm1hdGNoTWVkaWEpXG4gICAgICAgIHdpbmRvdy5tYXRjaE1lZGlhID0gbWF0Y2hNZWRpYVBvbHlmaWxsO1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBnbG9iYWwtcmVxdWlyZVxuICAgIGVucXVpcmUgPSByZXF1aXJlKCdlbnF1aXJlLmpzJyk7XG59XG5leHBvcnQgY29uc3QgcmVzcG9uc2l2ZUFycmF5ID0gWyd4eGwnLCAneGwnLCAnbGcnLCAnbWQnLCAnc20nLCAneHMnXTtcbmV4cG9ydCBjb25zdCByZXNwb25zaXZlTWFwID0ge1xuICAgIHhzOiAnKG1heC13aWR0aDogNTc1cHgpJyxcbiAgICBzbTogJyhtaW4td2lkdGg6IDU3NnB4KScsXG4gICAgbWQ6ICcobWluLXdpZHRoOiA3NjhweCknLFxuICAgIGxnOiAnKG1pbi13aWR0aDogOTkycHgpJyxcbiAgICB4bDogJyhtaW4td2lkdGg6IDEyMDBweCknLFxuICAgIHh4bDogJyhtaW4td2lkdGg6IDE2MDBweCknLFxufTtcbmxldCBzdWJzY3JpYmVycyA9IFtdO1xubGV0IHN1YlVpZCA9IC0xO1xubGV0IHNjcmVlbnMgPSB7fTtcbmNvbnN0IHJlc3BvbnNpdmVPYnNlcnZlID0ge1xuICAgIGRpc3BhdGNoKHBvaW50TWFwKSB7XG4gICAgICAgIHNjcmVlbnMgPSBwb2ludE1hcDtcbiAgICAgICAgaWYgKHN1YnNjcmliZXJzLmxlbmd0aCA8IDEpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICBzdWJzY3JpYmVycy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgICAgaXRlbS5mdW5jKHNjcmVlbnMpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSxcbiAgICBzdWJzY3JpYmUoZnVuYykge1xuICAgICAgICBpZiAoc3Vic2NyaWJlcnMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICB0aGlzLnJlZ2lzdGVyKCk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgdG9rZW4gPSAoKytzdWJVaWQpLnRvU3RyaW5nKCk7XG4gICAgICAgIHN1YnNjcmliZXJzLnB1c2goe1xuICAgICAgICAgICAgdG9rZW4sXG4gICAgICAgICAgICBmdW5jLFxuICAgICAgICB9KTtcbiAgICAgICAgZnVuYyhzY3JlZW5zKTtcbiAgICAgICAgcmV0dXJuIHRva2VuO1xuICAgIH0sXG4gICAgdW5zdWJzY3JpYmUodG9rZW4pIHtcbiAgICAgICAgc3Vic2NyaWJlcnMgPSBzdWJzY3JpYmVycy5maWx0ZXIoaXRlbSA9PiBpdGVtLnRva2VuICE9PSB0b2tlbik7XG4gICAgICAgIGlmIChzdWJzY3JpYmVycy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMudW5yZWdpc3RlcigpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICB1bnJlZ2lzdGVyKCkge1xuICAgICAgICBPYmplY3Qua2V5cyhyZXNwb25zaXZlTWFwKS5tYXAoKHNjcmVlbikgPT4gZW5xdWlyZS51bnJlZ2lzdGVyKHJlc3BvbnNpdmVNYXBbc2NyZWVuXSkpO1xuICAgIH0sXG4gICAgcmVnaXN0ZXIoKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKHJlc3BvbnNpdmVNYXApLm1hcCgoc2NyZWVuKSA9PiBlbnF1aXJlLnJlZ2lzdGVyKHJlc3BvbnNpdmVNYXBbc2NyZWVuXSwge1xuICAgICAgICAgICAgbWF0Y2g6ICgpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBwb2ludE1hcCA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgc2NyZWVucyksIHsgW3NjcmVlbl06IHRydWUgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5kaXNwYXRjaChwb2ludE1hcCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdW5tYXRjaDogKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHBvaW50TWFwID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBzY3JlZW5zKSwgeyBbc2NyZWVuXTogZmFsc2UgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5kaXNwYXRjaChwb2ludE1hcCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgLy8gS2VlcCBhIGVtcHR5IGRlc3RvcnkgdG8gYXZvaWQgdHJpZ2dlcmluZyB1bm1hdGNoIHdoZW4gdW5yZWdpc3RlclxuICAgICAgICAgICAgZGVzdHJveSgpIHsgfSxcbiAgICAgICAgfSkpO1xuICAgIH0sXG59O1xuZXhwb3J0IGRlZmF1bHQgcmVzcG9uc2l2ZU9ic2VydmU7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUZBO0FBQ0E7QUFDQTtBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BOztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBckJBO0FBdUJBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUEzQkE7QUE2QkE7QUFDQTtBQUFBO0FBQUE7QUE5QkE7QUFnQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQVBBO0FBU0E7QUFDQTtBQVZBO0FBQUE7QUFZQTtBQTdDQTtBQStDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/lib/_util/responsiveObserve.js
