/**
 * @param {Object} ctx
 * @param {Object} shape
 * @param {number} shape.x
 * @param {number} shape.y
 * @param {number} shape.width
 * @param {number} shape.height
 * @param {number} shape.r
 */
function buildPath(ctx, shape) {
  var x = shape.x;
  var y = shape.y;
  var width = shape.width;
  var height = shape.height;
  var r = shape.r;
  var r1;
  var r2;
  var r3;
  var r4; // Convert width and height to positive for better borderRadius

  if (width < 0) {
    x = x + width;
    width = -width;
  }

  if (height < 0) {
    y = y + height;
    height = -height;
  }

  if (typeof r === 'number') {
    r1 = r2 = r3 = r4 = r;
  } else if (r instanceof Array) {
    if (r.length === 1) {
      r1 = r2 = r3 = r4 = r[0];
    } else if (r.length === 2) {
      r1 = r3 = r[0];
      r2 = r4 = r[1];
    } else if (r.length === 3) {
      r1 = r[0];
      r2 = r4 = r[1];
      r3 = r[2];
    } else {
      r1 = r[0];
      r2 = r[1];
      r3 = r[2];
      r4 = r[3];
    }
  } else {
    r1 = r2 = r3 = r4 = 0;
  }

  var total;

  if (r1 + r2 > width) {
    total = r1 + r2;
    r1 *= width / total;
    r2 *= width / total;
  }

  if (r3 + r4 > width) {
    total = r3 + r4;
    r3 *= width / total;
    r4 *= width / total;
  }

  if (r2 + r3 > height) {
    total = r2 + r3;
    r2 *= height / total;
    r3 *= height / total;
  }

  if (r1 + r4 > height) {
    total = r1 + r4;
    r1 *= height / total;
    r4 *= height / total;
  }

  ctx.moveTo(x + r1, y);
  ctx.lineTo(x + width - r2, y);
  r2 !== 0 && ctx.arc(x + width - r2, y + r2, r2, -Math.PI / 2, 0);
  ctx.lineTo(x + width, y + height - r3);
  r3 !== 0 && ctx.arc(x + width - r3, y + height - r3, r3, 0, Math.PI / 2);
  ctx.lineTo(x + r4, y + height);
  r4 !== 0 && ctx.arc(x + r4, y + height - r4, r4, Math.PI / 2, Math.PI);
  ctx.lineTo(x, y + r1);
  r1 !== 0 && ctx.arc(x + r1, y + r1, r1, Math.PI, Math.PI * 1.5);
}

exports.buildPath = buildPath;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvcm91bmRSZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvcm91bmRSZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQHBhcmFtIHtPYmplY3R9IGN0eFxuICogQHBhcmFtIHtPYmplY3R9IHNoYXBlXG4gKiBAcGFyYW0ge251bWJlcn0gc2hhcGUueFxuICogQHBhcmFtIHtudW1iZXJ9IHNoYXBlLnlcbiAqIEBwYXJhbSB7bnVtYmVyfSBzaGFwZS53aWR0aFxuICogQHBhcmFtIHtudW1iZXJ9IHNoYXBlLmhlaWdodFxuICogQHBhcmFtIHtudW1iZXJ9IHNoYXBlLnJcbiAqL1xuZnVuY3Rpb24gYnVpbGRQYXRoKGN0eCwgc2hhcGUpIHtcbiAgdmFyIHggPSBzaGFwZS54O1xuICB2YXIgeSA9IHNoYXBlLnk7XG4gIHZhciB3aWR0aCA9IHNoYXBlLndpZHRoO1xuICB2YXIgaGVpZ2h0ID0gc2hhcGUuaGVpZ2h0O1xuICB2YXIgciA9IHNoYXBlLnI7XG4gIHZhciByMTtcbiAgdmFyIHIyO1xuICB2YXIgcjM7XG4gIHZhciByNDsgLy8gQ29udmVydCB3aWR0aCBhbmQgaGVpZ2h0IHRvIHBvc2l0aXZlIGZvciBiZXR0ZXIgYm9yZGVyUmFkaXVzXG5cbiAgaWYgKHdpZHRoIDwgMCkge1xuICAgIHggPSB4ICsgd2lkdGg7XG4gICAgd2lkdGggPSAtd2lkdGg7XG4gIH1cblxuICBpZiAoaGVpZ2h0IDwgMCkge1xuICAgIHkgPSB5ICsgaGVpZ2h0O1xuICAgIGhlaWdodCA9IC1oZWlnaHQ7XG4gIH1cblxuICBpZiAodHlwZW9mIHIgPT09ICdudW1iZXInKSB7XG4gICAgcjEgPSByMiA9IHIzID0gcjQgPSByO1xuICB9IGVsc2UgaWYgKHIgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgIGlmIChyLmxlbmd0aCA9PT0gMSkge1xuICAgICAgcjEgPSByMiA9IHIzID0gcjQgPSByWzBdO1xuICAgIH0gZWxzZSBpZiAoci5sZW5ndGggPT09IDIpIHtcbiAgICAgIHIxID0gcjMgPSByWzBdO1xuICAgICAgcjIgPSByNCA9IHJbMV07XG4gICAgfSBlbHNlIGlmIChyLmxlbmd0aCA9PT0gMykge1xuICAgICAgcjEgPSByWzBdO1xuICAgICAgcjIgPSByNCA9IHJbMV07XG4gICAgICByMyA9IHJbMl07XG4gICAgfSBlbHNlIHtcbiAgICAgIHIxID0gclswXTtcbiAgICAgIHIyID0gclsxXTtcbiAgICAgIHIzID0gclsyXTtcbiAgICAgIHI0ID0gclszXTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgcjEgPSByMiA9IHIzID0gcjQgPSAwO1xuICB9XG5cbiAgdmFyIHRvdGFsO1xuXG4gIGlmIChyMSArIHIyID4gd2lkdGgpIHtcbiAgICB0b3RhbCA9IHIxICsgcjI7XG4gICAgcjEgKj0gd2lkdGggLyB0b3RhbDtcbiAgICByMiAqPSB3aWR0aCAvIHRvdGFsO1xuICB9XG5cbiAgaWYgKHIzICsgcjQgPiB3aWR0aCkge1xuICAgIHRvdGFsID0gcjMgKyByNDtcbiAgICByMyAqPSB3aWR0aCAvIHRvdGFsO1xuICAgIHI0ICo9IHdpZHRoIC8gdG90YWw7XG4gIH1cblxuICBpZiAocjIgKyByMyA+IGhlaWdodCkge1xuICAgIHRvdGFsID0gcjIgKyByMztcbiAgICByMiAqPSBoZWlnaHQgLyB0b3RhbDtcbiAgICByMyAqPSBoZWlnaHQgLyB0b3RhbDtcbiAgfVxuXG4gIGlmIChyMSArIHI0ID4gaGVpZ2h0KSB7XG4gICAgdG90YWwgPSByMSArIHI0O1xuICAgIHIxICo9IGhlaWdodCAvIHRvdGFsO1xuICAgIHI0ICo9IGhlaWdodCAvIHRvdGFsO1xuICB9XG5cbiAgY3R4Lm1vdmVUbyh4ICsgcjEsIHkpO1xuICBjdHgubGluZVRvKHggKyB3aWR0aCAtIHIyLCB5KTtcbiAgcjIgIT09IDAgJiYgY3R4LmFyYyh4ICsgd2lkdGggLSByMiwgeSArIHIyLCByMiwgLU1hdGguUEkgLyAyLCAwKTtcbiAgY3R4LmxpbmVUbyh4ICsgd2lkdGgsIHkgKyBoZWlnaHQgLSByMyk7XG4gIHIzICE9PSAwICYmIGN0eC5hcmMoeCArIHdpZHRoIC0gcjMsIHkgKyBoZWlnaHQgLSByMywgcjMsIDAsIE1hdGguUEkgLyAyKTtcbiAgY3R4LmxpbmVUbyh4ICsgcjQsIHkgKyBoZWlnaHQpO1xuICByNCAhPT0gMCAmJiBjdHguYXJjKHggKyByNCwgeSArIGhlaWdodCAtIHI0LCByNCwgTWF0aC5QSSAvIDIsIE1hdGguUEkpO1xuICBjdHgubGluZVRvKHgsIHkgKyByMSk7XG4gIHIxICE9PSAwICYmIGN0eC5hcmMoeCArIHIxLCB5ICsgcjEsIHIxLCBNYXRoLlBJLCBNYXRoLlBJICogMS41KTtcbn1cblxuZXhwb3J0cy5idWlsZFBhdGggPSBidWlsZFBhdGg7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/roundRect.js
