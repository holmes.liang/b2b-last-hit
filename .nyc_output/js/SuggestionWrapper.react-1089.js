__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_5__);






var IS_REACT_16 = !!react_dom__WEBPACK_IMPORTED_MODULE_5__["createPortal"];

var SuggestionWrapper = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_2___default()(SuggestionWrapper, _React$Component);

  function SuggestionWrapper() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, SuggestionWrapper);

    return babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_1___default()(this, _React$Component.apply(this, arguments));
  }

  SuggestionWrapper.prototype.componentDidMount = function componentDidMount() {
    this.renderOrReady();
  };

  SuggestionWrapper.prototype.componentDidUpdate = function componentDidUpdate() {
    this.renderOrReady();
  };

  SuggestionWrapper.prototype.renderOrReady = function renderOrReady() {
    if (IS_REACT_16) {
      this.props.renderReady();
    } else {
      this.renderComponent();
    }
  };

  SuggestionWrapper.prototype.renderComponent = function renderComponent() {
    var _props = this.props,
        children = _props.children,
        container = _props.container,
        renderReady = _props.renderReady;
    Object(react_dom__WEBPACK_IMPORTED_MODULE_5__["unstable_renderSubtreeIntoContainer"])(this, children, container, function callback() {
      if (renderReady) {
        renderReady.call(this);
      }
    });
  };

  SuggestionWrapper.prototype.render = function render() {
    if (IS_REACT_16) {
      var _props2 = this.props,
          children = _props2.children,
          container = _props2.container;
      return Object(react_dom__WEBPACK_IMPORTED_MODULE_5__["createPortal"])(children, container);
    }

    return null;
  };

  return SuggestionWrapper;
}(react__WEBPACK_IMPORTED_MODULE_3___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (SuggestionWrapper);
SuggestionWrapper.propTypes = {
  children: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any,
  renderReady: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.func,
  container: prop_types__WEBPACK_IMPORTED_MODULE_4___default.a.any
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L1N1Z2dlc3Rpb25XcmFwcGVyLnJlYWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L1N1Z2dlc3Rpb25XcmFwcGVyLnJlYWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGNyZWF0ZVBvcnRhbCwgdW5zdGFibGVfcmVuZGVyU3VidHJlZUludG9Db250YWluZXIgfSBmcm9tICdyZWFjdC1kb20nO1xuXG52YXIgSVNfUkVBQ1RfMTYgPSAhIWNyZWF0ZVBvcnRhbDtcblxudmFyIFN1Z2dlc3Rpb25XcmFwcGVyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFN1Z2dlc3Rpb25XcmFwcGVyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTdWdnZXN0aW9uV3JhcHBlcigpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU3VnZ2VzdGlvbldyYXBwZXIpO1xuXG4gICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICBTdWdnZXN0aW9uV3JhcHBlci5wcm90b3R5cGUuY29tcG9uZW50RGlkTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLnJlbmRlck9yUmVhZHkoKTtcbiAgfTtcblxuICBTdWdnZXN0aW9uV3JhcHBlci5wcm90b3R5cGUuY29tcG9uZW50RGlkVXBkYXRlID0gZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgIHRoaXMucmVuZGVyT3JSZWFkeSgpO1xuICB9O1xuXG4gIFN1Z2dlc3Rpb25XcmFwcGVyLnByb3RvdHlwZS5yZW5kZXJPclJlYWR5ID0gZnVuY3Rpb24gcmVuZGVyT3JSZWFkeSgpIHtcbiAgICBpZiAoSVNfUkVBQ1RfMTYpIHtcbiAgICAgIHRoaXMucHJvcHMucmVuZGVyUmVhZHkoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5yZW5kZXJDb21wb25lbnQoKTtcbiAgICB9XG4gIH07XG5cbiAgU3VnZ2VzdGlvbldyYXBwZXIucHJvdG90eXBlLnJlbmRlckNvbXBvbmVudCA9IGZ1bmN0aW9uIHJlbmRlckNvbXBvbmVudCgpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgIGNvbnRhaW5lciA9IF9wcm9wcy5jb250YWluZXIsXG4gICAgICAgIHJlbmRlclJlYWR5ID0gX3Byb3BzLnJlbmRlclJlYWR5O1xuXG4gICAgdW5zdGFibGVfcmVuZGVyU3VidHJlZUludG9Db250YWluZXIodGhpcywgY2hpbGRyZW4sIGNvbnRhaW5lciwgZnVuY3Rpb24gY2FsbGJhY2soKSB7XG4gICAgICBpZiAocmVuZGVyUmVhZHkpIHtcbiAgICAgICAgcmVuZGVyUmVhZHkuY2FsbCh0aGlzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICBTdWdnZXN0aW9uV3JhcHBlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIGlmIChJU19SRUFDVF8xNikge1xuICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzMi5jaGlsZHJlbixcbiAgICAgICAgICBjb250YWluZXIgPSBfcHJvcHMyLmNvbnRhaW5lcjtcblxuICAgICAgcmV0dXJuIGNyZWF0ZVBvcnRhbChjaGlsZHJlbiwgY29udGFpbmVyKTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH07XG5cbiAgcmV0dXJuIFN1Z2dlc3Rpb25XcmFwcGVyO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5leHBvcnQgZGVmYXVsdCBTdWdnZXN0aW9uV3JhcHBlcjtcblxuXG5TdWdnZXN0aW9uV3JhcHBlci5wcm9wVHlwZXMgPSB7XG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMuYW55LFxuICByZW5kZXJSZWFkeTogUHJvcFR5cGVzLmZ1bmMsXG4gIGNvbnRhaW5lcjogUHJvcFR5cGVzLmFueVxufTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFIQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/component/SuggestionWrapper.react.js
