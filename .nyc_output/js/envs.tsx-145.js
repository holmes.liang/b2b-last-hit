__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./consts */ "./src/common/consts.tsx");
/* harmony import */ var _storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./storage */ "./src/common/storage.tsx");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./utils */ "./src/common/utils.tsx");






var Envs =
/*#__PURE__*/
function () {
  function Envs() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Envs);

    this.rootApp = void 0;
    this.os = void 0;
    this.browser = void 0;
    this.standardThemeName = "standard";
    this.os = {};
    this.browser = {};
    this.standardThemeName = "standard";
    this.detect();
  }
  /**
   * set application
   */


  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Envs, [{
    key: "application",
    value: function application(app) {
      if (!_utils__WEBPACK_IMPORTED_MODULE_4__["default"].isUndefined(app)) {
        this.rootApp = app;
      } else {
        return this.rootApp;
      }
    }
    /**
     * get current theme name
     */

  }, {
    key: "getCurrentTheme",
    value: function getCurrentTheme() {
      var queryString = _utils__WEBPACK_IMPORTED_MODULE_4__["default"].fromQueryString();

      if (queryString && queryString.theme) {
        return queryString.theme;
      } else {
        return _storage__WEBPACK_IMPORTED_MODULE_3__["default"].Theme.get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].THEME_KEY);
      }
    }
    /**
     * get relative path, add context prefix
     */

  }, {
    key: "getContextLocation",
    value: function getContextLocation(relativePath) {
      var contextPath = null;
      var path = window.location.pathname;
      var secondIndex = path.indexOf("/", 1);

      if (secondIndex === -1) {
        // no context path
        contextPath = Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}).REACT_APP_AJAX_CLIENT_CONTEXT || "";
      } else {
        contextPath = path.substring(0, secondIndex);
      }

      if (relativePath) {
        return "".concat(contextPath).concat(relativePath);
      } else {
        return contextPath;
      }
    }
    /**
     * detect environment(such as os, browser etc.)
     */

  }, {
    key: "detect",
    value: function detect() {
      var ua = window.navigator.userAgent;
      var platform = window.navigator.platform;
      var os = this.os,
          browser = this.browser,
          // eslint-disable-next-line
      webkit = ua.match(/Web[kK]it[\/]{0,1}([\d.]+)/),
          // eslint-disable-next-line
      android = ua.match(/(Android);?[\s\/]+([\d.]+)?/),
          // mac
      // eslint-disable-next-line
      osx = !!ua.match(/\(Macintosh\; Intel /) || ua.match(/(Mac)/),
          ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
          ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/),
          iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
          // eslint-disable-next-line
      webos = ua.match(/(webOS|hpwOS)[\s\/]([\d.]+)/),
          win = /Win\d{2}|Windows/.test(platform) || ua.match(/(win)/),
          wp = ua.match(/Windows Phone ([\d.]+)/),
          touchpad = webos && ua.match(/TouchPad/),
          kindle = ua.match(/Kindle\/([\d.]+)/),
          silk = ua.match(/Silk\/([\d._]+)/),
          blackberry = ua.match(/(BlackBerry).*Version\/([\d.]+)/),
          bb10 = ua.match(/(BB10).*Version\/([\d.]+)/),
          rimtabletos = ua.match(/(RIM\sTablet\sOS)\s([\d.]+)/),
          playbook = ua.match(/PlayBook/),
          chrome = ua.match(/Chrome\/([\d.]+)/) || ua.match(/CriOS\/([\d.]+)/),
          firefox = ua.match(/Firefox\/([\d.]+)/),
          opera = ua.match(/opera.([\d.]+)/),
          firefoxos = ua.match(/\((?:Mobile|Tablet); rv:([\d.]+)\).*Firefox\/[\d.]+/),
          ie = ua.match(/MSIE\s([\d.]+)/) || // eslint-disable-next-line
      ua.match(/Trident\/[\d](?=[^\?]+).*rv:([0-9.].)/),
          edge = ua.match(/(edge)\/([\w.]+)/),
          appleWebview = !chrome && ua.match(/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/),
          safari = appleWebview || ua.match(/Version\/([\d.]+)([^S](Safari)|[^M]*(Mobile)[^S]*(Safari))/),
          linux = ua.match(/(Linux)/),
          // chrome os
      cros = ua.match(/(CrOS)/),
          androidWebview = ua.match(/(wv\))/);

      if (browser.webkit = !!webkit) {
        browser.version = webkit[1];
      }

      if (android) {
        os.android = true;
        os.version = android[2];
      }

      if (iphone && !ipod) {
        os.ios = os.iphone = true;
        os.version = iphone[2].replace(/_/g, ".");
      }

      if (ipad) {
        os.ios = os.ipad = true;
        os.version = ipad[2].replace(/_/g, ".");
      }

      if (ipod) {
        os.ios = os.ipod = true;
        os.version = ipod[3] ? ipod[3].replace(/_/g, ".") : undefined;
      }

      if (wp) {
        os.wp = true;
        os.version = wp[1];
      }

      if (webos) {
        os.webos = true;
        os.version = webos[2];
      }

      if (touchpad) {
        os.touchpad = true;
      }

      if (blackberry) {
        os.blackberry = true;
        os.version = blackberry[2];
      }

      if (bb10) {
        os.bb10 = true;
        os.version = bb10[2];
      }

      if (rimtabletos) {
        os.rimtabletos = true;
        os.version = rimtabletos[2];
      }

      if (playbook) {
        browser.playbook = true;
      }

      if (kindle) {
        os.kindle = true;
        os.version = kindle[1];
      }

      if (silk) {
        browser.silk = true;
        browser.version = silk[1];
      }

      if (!silk && os.android && ua.match(/Kindle Fire/)) {
        browser.silk = true;
      }

      if (chrome) {
        browser.chrome = true;
        browser.version = chrome[1];
      }

      if (firefox) {
        browser.firefox = true;
        browser.version = firefox[1];
      }

      if (opera) {
        browser.opera = true;
        browser.version = opera[1];
      }

      if (firefoxos) {
        os.firefoxos = true;
        os.version = firefoxos[1];
      }

      if (ie) {
        browser.ie = true;
        browser.version = ie[1];
      }

      if (edge) {
        browser.edge = true;
      }

      if (safari && (osx || os.ios || win)) {
        browser.safari = true;

        if (!os.ios) {
          browser.version = safari[1];
        }
      }

      os.tablet = !!(ipad || playbook || android && !ua.match(/Mobile/) || firefox && ua.match(/Tablet/) || ie && !ua.match(/Phone/) && ua.match(/Touch/));
      os.phone = !!(!os.tablet && !os.ipod && (android || iphone || webos || blackberry || bb10 || chrome && ua.match(/Android/) || chrome && ua.match(/CriOS\/([\d.]+)/) || firefox && ua.match(/Mobile/) || ie && ua.match(/Touch/)));
      os.desk = !!(osx || cros || win || linux) && !os.tablet && !os.phone;
      os.macos = !!osx && !os.tablet && !os.phone;
      os.linux = !!linux && !os.tablet && !os.phone;
      os.windows = !!win && !os.tablet && !os.phone;
      os.unix = platform === "X11" && !os.windows && !os.macos;

      if (appleWebview || androidWebview || os.app) {
        browser.webview = true;
      } // is wechat


      if (/MicroMessenger/i.test(ua)) {
        os.wechat = true;
      } // is wechat applet


      if (window.__wxjs_environment === "miniprogram") {
        os.wechatMini = true;
      }
    }
  }, {
    key: "getAuth",
    value: function getAuth() {
      return _storage__WEBPACK_IMPORTED_MODULE_3__["default"].Auth.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].AUTH_KEY);
    }
    /**
     * get current login user info from session storage
     */

  }, {
    key: "findAccount",
    value: function findAccount() {
      return _storage__WEBPACK_IMPORTED_MODULE_3__["default"].Account.session().get(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].ACCOUNT_KEY) || {};
    }
  }, {
    key: "isAuthorized",
    value: function isAuthorized() {
      return !!this.findAccount().userName;
    }
    /**
     * hold current login user info to session storage
     */

  }, {
    key: "holdAccount",
    value: function holdAccount(account) {
      _storage__WEBPACK_IMPORTED_MODULE_3__["default"].Account.session().set(_consts__WEBPACK_IMPORTED_MODULE_2__["default"].ACCOUNT_KEY, account);
    }
  }]);

  return Envs;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Envs());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2VudnMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvY29tbW9uL2VudnMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFwcGxpY2F0aW9uIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IENvbnN0cyBmcm9tIFwiLi9jb25zdHNcIjtcbmltcG9ydCBTdG9yYWdlIGZyb20gXCIuL3N0b3JhZ2VcIjtcbmltcG9ydCBVdGlscyBmcm9tIFwiLi91dGlsc1wiO1xuXG50eXBlIE9TID0ge1xuICBhbmRyb2lkPzogYm9vbGVhbjtcbiAgaW9zPzogYm9vbGVhbjtcbiAgaXBob25lPzogYm9vbGVhbjtcbiAgaXBhZD86IGJvb2xlYW47XG4gIGlwb2Q/OiBib29sZWFuO1xuICB3cD86IGJvb2xlYW47XG4gIHdlYm9zPzogYm9vbGVhbjtcbiAgdG91Y2hwYWQ/OiBib29sZWFuO1xuICBibGFja2JlcnJ5PzogYm9vbGVhbjtcbiAgYmIxMD86IGJvb2xlYW47XG4gIHJpbXRhYmxldG9zPzogYm9vbGVhbjtcbiAga2luZGxlPzogYm9vbGVhbjtcbiAgZmlyZWZveG9zPzogYm9vbGVhbjtcbiAgdmVyc2lvbj86IHN0cmluZztcbiAgdGFibGV0PzogYm9vbGVhbjtcbiAgcGhvbmU/OiBib29sZWFuO1xuICBkZXNrPzogYm9vbGVhbjtcbiAgYXBwPzogYm9vbGVhbjtcbiAgd2VjaGF0PzogYm9vbGVhbjtcbiAgd2VjaGF0TWluaT86IGJvb2xlYW47XG4gIGxpbnV4PzogYm9vbGVhbjtcbiAgdW5peD86IGJvb2xlYW47XG4gIG1hY29zPzogYm9vbGVhbjtcbiAgd2luZG93cz86IGJvb2xlYW47XG59O1xudHlwZSBCUk9XU0VSID0ge1xuICB3ZWJraXQ/OiBib29sZWFuO1xuICBwbGF5Ym9vaz86IGJvb2xlYW47XG4gIHNpbGs/OiBib29sZWFuO1xuICBjaHJvbWU/OiBib29sZWFuO1xuICBmaXJlZm94PzogYm9vbGVhbjtcbiAgb3BlcmE/OiBib29sZWFuO1xuICBpZT86IGJvb2xlYW47XG4gIGVkZ2U/OiBib29sZWFuO1xuICBzYWZhcmk/OiBib29sZWFuO1xuICB3ZWJ2aWV3PzogYm9vbGVhbjtcbiAgdmVyc2lvbj86IHN0cmluZztcbn07XG5cbmNsYXNzIEVudnMge1xuICBwcml2YXRlIHJvb3RBcHA6IGFueTtcbiAgb3M6IE9TO1xuICBicm93c2VyOiBCUk9XU0VSO1xuICBzdGFuZGFyZFRoZW1lTmFtZTogc3RyaW5nID0gXCJzdGFuZGFyZFwiO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMub3MgPSB7fTtcbiAgICB0aGlzLmJyb3dzZXIgPSB7fTtcbiAgICB0aGlzLnN0YW5kYXJkVGhlbWVOYW1lID0gXCJzdGFuZGFyZFwiO1xuICAgIHRoaXMuZGV0ZWN0KCk7XG4gIH1cblxuICAvKipcbiAgICogc2V0IGFwcGxpY2F0aW9uXG4gICAqL1xuICBhcHBsaWNhdGlvbihhcHA/OiBhbnkpOiBBcHBsaWNhdGlvbiB8IG51bGwgfCB1bmRlZmluZWQge1xuICAgIGlmICghVXRpbHMuaXNVbmRlZmluZWQoYXBwKSkge1xuICAgICAgdGhpcy5yb290QXBwID0gYXBwO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gdGhpcy5yb290QXBwO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBnZXQgY3VycmVudCB0aGVtZSBuYW1lXG4gICAqL1xuICBnZXRDdXJyZW50VGhlbWUoKTogc3RyaW5nIHtcbiAgICBjb25zdCBxdWVyeVN0cmluZyA9IFV0aWxzLmZyb21RdWVyeVN0cmluZygpO1xuICAgIGlmIChxdWVyeVN0cmluZyAmJiBxdWVyeVN0cmluZy50aGVtZSkge1xuICAgICAgcmV0dXJuIHF1ZXJ5U3RyaW5nLnRoZW1lO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gU3RvcmFnZS5UaGVtZS5nZXQoQ29uc3RzLlRIRU1FX0tFWSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIGdldCByZWxhdGl2ZSBwYXRoLCBhZGQgY29udGV4dCBwcmVmaXhcbiAgICovXG4gIGdldENvbnRleHRMb2NhdGlvbihyZWxhdGl2ZVBhdGg6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgbGV0IGNvbnRleHRQYXRoID0gbnVsbDtcbiAgICBsZXQgcGF0aCA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZTtcbiAgICBsZXQgc2Vjb25kSW5kZXggPSBwYXRoLmluZGV4T2YoXCIvXCIsIDEpO1xuICAgIGlmIChzZWNvbmRJbmRleCA9PT0gLTEpIHtcbiAgICAgIC8vIG5vIGNvbnRleHQgcGF0aFxuICAgICAgY29udGV4dFBhdGggPSBwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9DTElFTlRfQ09OVEVYVCB8fCBcIlwiO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb250ZXh0UGF0aCA9IHBhdGguc3Vic3RyaW5nKDAsIHNlY29uZEluZGV4KTtcbiAgICB9XG4gICAgaWYgKHJlbGF0aXZlUGF0aCkge1xuICAgICAgcmV0dXJuIGAke2NvbnRleHRQYXRofSR7cmVsYXRpdmVQYXRofWA7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBjb250ZXh0UGF0aDtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogZGV0ZWN0IGVudmlyb25tZW50KHN1Y2ggYXMgb3MsIGJyb3dzZXIgZXRjLilcbiAgICovXG4gIHByaXZhdGUgZGV0ZWN0KCk6IHZvaWQge1xuICAgIGNvbnN0IHVhID0gd2luZG93Lm5hdmlnYXRvci51c2VyQWdlbnQ7XG4gICAgY29uc3QgcGxhdGZvcm0gPSB3aW5kb3cubmF2aWdhdG9yLnBsYXRmb3JtO1xuXG4gICAgY29uc3Qgb3M6IE9TID0gdGhpcy5vcyxcbiAgICAgIGJyb3dzZXI6IEJST1dTRVIgPSB0aGlzLmJyb3dzZXIsXG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgIHdlYmtpdDogYW55ID0gdWEubWF0Y2goL1dlYltrS11pdFtcXC9dezAsMX0oW1xcZC5dKykvKSxcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgYW5kcm9pZCA9IHVhLm1hdGNoKC8oQW5kcm9pZCk7P1tcXHNcXC9dKyhbXFxkLl0rKT8vKSxcbiAgICAgIC8vIG1hY1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICBvc3ggPSAhIXVhLm1hdGNoKC9cXChNYWNpbnRvc2hcXDsgSW50ZWwgLykgfHwgdWEubWF0Y2goLyhNYWMpLyksXG4gICAgICBpcGFkID0gdWEubWF0Y2goLyhpUGFkKS4qT1NcXHMoW1xcZF9dKykvKSxcbiAgICAgIGlwb2QgPSB1YS5tYXRjaCgvKGlQb2QpKC4qT1NcXHMoW1xcZF9dKykpPy8pLFxuICAgICAgaXBob25lID0gIWlwYWQgJiYgdWEubWF0Y2goLyhpUGhvbmVcXHNPUylcXHMoW1xcZF9dKykvKSxcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgd2Vib3MgPSB1YS5tYXRjaCgvKHdlYk9TfGhwd09TKVtcXHNcXC9dKFtcXGQuXSspLyksXG4gICAgICB3aW4gPSAvV2luXFxkezJ9fFdpbmRvd3MvLnRlc3QocGxhdGZvcm0pIHx8IHVhLm1hdGNoKC8od2luKS8pLFxuICAgICAgd3AgPSB1YS5tYXRjaCgvV2luZG93cyBQaG9uZSAoW1xcZC5dKykvKSxcbiAgICAgIHRvdWNocGFkID0gd2Vib3MgJiYgdWEubWF0Y2goL1RvdWNoUGFkLyksXG4gICAgICBraW5kbGUgPSB1YS5tYXRjaCgvS2luZGxlXFwvKFtcXGQuXSspLyksXG4gICAgICBzaWxrID0gdWEubWF0Y2goL1NpbGtcXC8oW1xcZC5fXSspLyksXG4gICAgICBibGFja2JlcnJ5ID0gdWEubWF0Y2goLyhCbGFja0JlcnJ5KS4qVmVyc2lvblxcLyhbXFxkLl0rKS8pLFxuICAgICAgYmIxMCA9IHVhLm1hdGNoKC8oQkIxMCkuKlZlcnNpb25cXC8oW1xcZC5dKykvKSxcbiAgICAgIHJpbXRhYmxldG9zID0gdWEubWF0Y2goLyhSSU1cXHNUYWJsZXRcXHNPUylcXHMoW1xcZC5dKykvKSxcbiAgICAgIHBsYXlib29rID0gdWEubWF0Y2goL1BsYXlCb29rLyksXG4gICAgICBjaHJvbWUgPSB1YS5tYXRjaCgvQ2hyb21lXFwvKFtcXGQuXSspLykgfHwgdWEubWF0Y2goL0NyaU9TXFwvKFtcXGQuXSspLyksXG4gICAgICBmaXJlZm94ID0gdWEubWF0Y2goL0ZpcmVmb3hcXC8oW1xcZC5dKykvKSxcbiAgICAgIG9wZXJhID0gdWEubWF0Y2goL29wZXJhLihbXFxkLl0rKS8pLFxuICAgICAgZmlyZWZveG9zID0gdWEubWF0Y2goL1xcKCg/Ok1vYmlsZXxUYWJsZXQpOyBydjooW1xcZC5dKylcXCkuKkZpcmVmb3hcXC9bXFxkLl0rLyksXG4gICAgICBpZSA9XG4gICAgICAgIHVhLm1hdGNoKC9NU0lFXFxzKFtcXGQuXSspLykgfHxcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICAgIHVhLm1hdGNoKC9UcmlkZW50XFwvW1xcZF0oPz1bXlxcP10rKS4qcnY6KFswLTkuXS4pLyksXG4gICAgICBlZGdlID0gdWEubWF0Y2goLyhlZGdlKVxcLyhbXFx3Ll0rKS8pLFxuICAgICAgYXBwbGVXZWJ2aWV3ID0gIWNocm9tZSAmJiB1YS5tYXRjaCgvKGlQaG9uZXxpUG9kfGlQYWQpLipBcHBsZVdlYktpdCg/IS4qU2FmYXJpKS8pLFxuICAgICAgc2FmYXJpID0gYXBwbGVXZWJ2aWV3IHx8IHVhLm1hdGNoKC9WZXJzaW9uXFwvKFtcXGQuXSspKFteU10oU2FmYXJpKXxbXk1dKihNb2JpbGUpW15TXSooU2FmYXJpKSkvKSxcbiAgICAgIGxpbnV4ID0gdWEubWF0Y2goLyhMaW51eCkvKSxcbiAgICAgIC8vIGNocm9tZSBvc1xuICAgICAgY3JvcyA9IHVhLm1hdGNoKC8oQ3JPUykvKSxcbiAgICAgIGFuZHJvaWRXZWJ2aWV3ID0gdWEubWF0Y2goLyh3dlxcKSkvKTtcblxuICAgIGlmICgoYnJvd3Nlci53ZWJraXQgPSAhIXdlYmtpdCkpIHtcbiAgICAgIGJyb3dzZXIudmVyc2lvbiA9IHdlYmtpdFsxXTtcbiAgICB9XG5cbiAgICBpZiAoYW5kcm9pZCkge1xuICAgICAgb3MuYW5kcm9pZCA9IHRydWU7XG4gICAgICBvcy52ZXJzaW9uID0gYW5kcm9pZFsyXTtcbiAgICB9XG4gICAgaWYgKGlwaG9uZSAmJiAhaXBvZCkge1xuICAgICAgb3MuaW9zID0gb3MuaXBob25lID0gdHJ1ZTtcbiAgICAgIG9zLnZlcnNpb24gPSBpcGhvbmVbMl0ucmVwbGFjZSgvXy9nLCBcIi5cIik7XG4gICAgfVxuICAgIGlmIChpcGFkKSB7XG4gICAgICBvcy5pb3MgPSBvcy5pcGFkID0gdHJ1ZTtcbiAgICAgIG9zLnZlcnNpb24gPSBpcGFkWzJdLnJlcGxhY2UoL18vZywgXCIuXCIpO1xuICAgIH1cbiAgICBpZiAoaXBvZCkge1xuICAgICAgb3MuaW9zID0gb3MuaXBvZCA9IHRydWU7XG4gICAgICBvcy52ZXJzaW9uID0gaXBvZFszXSA/IGlwb2RbM10ucmVwbGFjZSgvXy9nLCBcIi5cIikgOiB1bmRlZmluZWQ7XG4gICAgfVxuICAgIGlmICh3cCkge1xuICAgICAgb3Mud3AgPSB0cnVlO1xuICAgICAgb3MudmVyc2lvbiA9IHdwWzFdO1xuICAgIH1cbiAgICBpZiAod2Vib3MpIHtcbiAgICAgIG9zLndlYm9zID0gdHJ1ZTtcbiAgICAgIG9zLnZlcnNpb24gPSB3ZWJvc1syXTtcbiAgICB9XG4gICAgaWYgKHRvdWNocGFkKSB7XG4gICAgICBvcy50b3VjaHBhZCA9IHRydWU7XG4gICAgfVxuICAgIGlmIChibGFja2JlcnJ5KSB7XG4gICAgICBvcy5ibGFja2JlcnJ5ID0gdHJ1ZTtcbiAgICAgIG9zLnZlcnNpb24gPSBibGFja2JlcnJ5WzJdO1xuICAgIH1cbiAgICBpZiAoYmIxMCkge1xuICAgICAgb3MuYmIxMCA9IHRydWU7XG4gICAgICBvcy52ZXJzaW9uID0gYmIxMFsyXTtcbiAgICB9XG4gICAgaWYgKHJpbXRhYmxldG9zKSB7XG4gICAgICBvcy5yaW10YWJsZXRvcyA9IHRydWU7XG4gICAgICBvcy52ZXJzaW9uID0gcmltdGFibGV0b3NbMl07XG4gICAgfVxuICAgIGlmIChwbGF5Ym9vaykge1xuICAgICAgYnJvd3Nlci5wbGF5Ym9vayA9IHRydWU7XG4gICAgfVxuICAgIGlmIChraW5kbGUpIHtcbiAgICAgIG9zLmtpbmRsZSA9IHRydWU7XG4gICAgICBvcy52ZXJzaW9uID0ga2luZGxlWzFdO1xuICAgIH1cbiAgICBpZiAoc2lsaykge1xuICAgICAgYnJvd3Nlci5zaWxrID0gdHJ1ZTtcbiAgICAgIGJyb3dzZXIudmVyc2lvbiA9IHNpbGtbMV07XG4gICAgfVxuICAgIGlmICghc2lsayAmJiBvcy5hbmRyb2lkICYmIHVhLm1hdGNoKC9LaW5kbGUgRmlyZS8pKSB7XG4gICAgICBicm93c2VyLnNpbGsgPSB0cnVlO1xuICAgIH1cbiAgICBpZiAoY2hyb21lKSB7XG4gICAgICBicm93c2VyLmNocm9tZSA9IHRydWU7XG4gICAgICBicm93c2VyLnZlcnNpb24gPSBjaHJvbWVbMV07XG4gICAgfVxuICAgIGlmIChmaXJlZm94KSB7XG4gICAgICBicm93c2VyLmZpcmVmb3ggPSB0cnVlO1xuICAgICAgYnJvd3Nlci52ZXJzaW9uID0gZmlyZWZveFsxXTtcbiAgICB9XG4gICAgaWYgKG9wZXJhKSB7XG4gICAgICBicm93c2VyLm9wZXJhID0gdHJ1ZTtcbiAgICAgIGJyb3dzZXIudmVyc2lvbiA9IG9wZXJhWzFdO1xuICAgIH1cbiAgICBpZiAoZmlyZWZveG9zKSB7XG4gICAgICBvcy5maXJlZm94b3MgPSB0cnVlO1xuICAgICAgb3MudmVyc2lvbiA9IGZpcmVmb3hvc1sxXTtcbiAgICB9XG4gICAgaWYgKGllKSB7XG4gICAgICBicm93c2VyLmllID0gdHJ1ZTtcbiAgICAgIGJyb3dzZXIudmVyc2lvbiA9IGllWzFdO1xuICAgIH1cbiAgICBpZiAoZWRnZSkge1xuICAgICAgYnJvd3Nlci5lZGdlID0gdHJ1ZTtcbiAgICB9XG4gICAgaWYgKHNhZmFyaSAmJiAob3N4IHx8IG9zLmlvcyB8fCB3aW4pKSB7XG4gICAgICBicm93c2VyLnNhZmFyaSA9IHRydWU7XG4gICAgICBpZiAoIW9zLmlvcykge1xuICAgICAgICBicm93c2VyLnZlcnNpb24gPSBzYWZhcmlbMV07XG4gICAgICB9XG4gICAgfVxuXG4gICAgb3MudGFibGV0ID0gISEoXG4gICAgICBpcGFkIHx8XG4gICAgICBwbGF5Ym9vayB8fFxuICAgICAgKGFuZHJvaWQgJiYgIXVhLm1hdGNoKC9Nb2JpbGUvKSkgfHxcbiAgICAgIChmaXJlZm94ICYmIHVhLm1hdGNoKC9UYWJsZXQvKSkgfHxcbiAgICAgIChpZSAmJiAhdWEubWF0Y2goL1Bob25lLykgJiYgdWEubWF0Y2goL1RvdWNoLykpXG4gICAgKTtcbiAgICBvcy5waG9uZSA9ICEhKFxuICAgICAgIW9zLnRhYmxldCAmJlxuICAgICAgIW9zLmlwb2QgJiZcbiAgICAgIChhbmRyb2lkIHx8XG4gICAgICAgIGlwaG9uZSB8fFxuICAgICAgICB3ZWJvcyB8fFxuICAgICAgICBibGFja2JlcnJ5IHx8XG4gICAgICAgIGJiMTAgfHxcbiAgICAgICAgKGNocm9tZSAmJiB1YS5tYXRjaCgvQW5kcm9pZC8pKSB8fFxuICAgICAgICAoY2hyb21lICYmIHVhLm1hdGNoKC9DcmlPU1xcLyhbXFxkLl0rKS8pKSB8fFxuICAgICAgICAoZmlyZWZveCAmJiB1YS5tYXRjaCgvTW9iaWxlLykpIHx8XG4gICAgICAgIChpZSAmJiB1YS5tYXRjaCgvVG91Y2gvKSkpXG4gICAgKTtcbiAgICBvcy5kZXNrID0gISEob3N4IHx8IGNyb3MgfHwgd2luIHx8IGxpbnV4KSAmJiAhb3MudGFibGV0ICYmICFvcy5waG9uZTtcbiAgICBvcy5tYWNvcyA9ICEhKG9zeCkgJiYgIW9zLnRhYmxldCAmJiAhb3MucGhvbmU7XG4gICAgb3MubGludXggPSAhIShsaW51eCkgJiYgIW9zLnRhYmxldCAmJiAhb3MucGhvbmU7XG4gICAgb3Mud2luZG93cyA9ICEhKHdpbikgJiYgIW9zLnRhYmxldCAmJiAhb3MucGhvbmU7XG4gICAgb3MudW5peCA9IChwbGF0Zm9ybSA9PT0gXCJYMTFcIikgJiYgIW9zLndpbmRvd3MgJiYgIW9zLm1hY29zO1xuXG4gICAgaWYgKGFwcGxlV2VidmlldyB8fCBhbmRyb2lkV2VidmlldyB8fCBvcy5hcHApIHtcbiAgICAgIGJyb3dzZXIud2VidmlldyA9IHRydWU7XG4gICAgfVxuICAgIC8vIGlzIHdlY2hhdFxuICAgIGlmICgvTWljcm9NZXNzZW5nZXIvaS50ZXN0KHVhKSkge1xuICAgICAgb3Mud2VjaGF0ID0gdHJ1ZTtcbiAgICB9XG4gICAgLy8gaXMgd2VjaGF0IGFwcGxldFxuICAgIGlmICh3aW5kb3cuX193eGpzX2Vudmlyb25tZW50ID09PSBcIm1pbmlwcm9ncmFtXCIpIHtcbiAgICAgIG9zLndlY2hhdE1pbmkgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIGdldEF1dGgoKTogc3RyaW5nIHsgICAgXG4gICAgcmV0dXJuIFN0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSk7XG4gIH1cblxuICAvKipcbiAgICogZ2V0IGN1cnJlbnQgbG9naW4gdXNlciBpbmZvIGZyb20gc2Vzc2lvbiBzdG9yYWdlXG4gICAqL1xuICBmaW5kQWNjb3VudCgpOiBhbnkge1xuICAgIHJldHVybiBTdG9yYWdlLkFjY291bnQuc2Vzc2lvbigpLmdldChDb25zdHMuQUNDT1VOVF9LRVkpIHx8IHt9O1xuICB9XG5cbiAgaXNBdXRob3JpemVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiAhIXRoaXMuZmluZEFjY291bnQoKS51c2VyTmFtZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBob2xkIGN1cnJlbnQgbG9naW4gdXNlciBpbmZvIHRvIHNlc3Npb24gc3RvcmFnZVxuICAgKi9cblxuICBob2xkQWNjb3VudChhY2NvdW50OiBhbnkpOiB2b2lkIHtcbiAgICBTdG9yYWdlLkFjY291bnQuc2Vzc2lvbigpLnNldChDb25zdHMuQUNDT1VOVF9LRVksIGFjY291bnQpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBFbnZzKCk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF5Q0E7OztBQU1BO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFHQTtBQUhBO0FBS0E7QUFMQTtBQU9BO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQThCQTtBQTlCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBb0NBO0FBcENBO0FBQ0E7QUFzQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFPQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7OztBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/common/envs.tsx
