var Displayable = __webpack_require__(/*! ./Displayable */ "./node_modules/zrender/lib/graphic/Displayable.js");

var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var PathProxy = __webpack_require__(/*! ../core/PathProxy */ "./node_modules/zrender/lib/core/PathProxy.js");

var pathContain = __webpack_require__(/*! ../contain/path */ "./node_modules/zrender/lib/contain/path.js");

var Pattern = __webpack_require__(/*! ./Pattern */ "./node_modules/zrender/lib/graphic/Pattern.js");

var getCanvasPattern = Pattern.prototype.getCanvasPattern;
var abs = Math.abs;
var pathProxyForDraw = new PathProxy(true);
/**
 * @alias module:zrender/graphic/Path
 * @extends module:zrender/graphic/Displayable
 * @constructor
 * @param {Object} opts
 */

function Path(opts) {
  Displayable.call(this, opts);
  /**
   * @type {module:zrender/core/PathProxy}
   * @readOnly
   */

  this.path = null;
}

Path.prototype = {
  constructor: Path,
  type: 'path',
  __dirtyPath: true,
  strokeContainThreshold: 5,

  /**
   * See `module:zrender/src/graphic/helper/subPixelOptimize`.
   * @type {boolean}
   */
  subPixelOptimize: false,
  brush: function brush(ctx, prevEl) {
    var style = this.style;
    var path = this.path || pathProxyForDraw;
    var hasStroke = style.hasStroke();
    var hasFill = style.hasFill();
    var fill = style.fill;
    var stroke = style.stroke;
    var hasFillGradient = hasFill && !!fill.colorStops;
    var hasStrokeGradient = hasStroke && !!stroke.colorStops;
    var hasFillPattern = hasFill && !!fill.image;
    var hasStrokePattern = hasStroke && !!stroke.image;
    style.bind(ctx, this, prevEl);
    this.setTransform(ctx);

    if (this.__dirty) {
      var rect; // Update gradient because bounding rect may changed

      if (hasFillGradient) {
        rect = rect || this.getBoundingRect();
        this._fillGradient = style.getGradient(ctx, fill, rect);
      }

      if (hasStrokeGradient) {
        rect = rect || this.getBoundingRect();
        this._strokeGradient = style.getGradient(ctx, stroke, rect);
      }
    } // Use the gradient or pattern


    if (hasFillGradient) {
      // PENDING If may have affect the state
      ctx.fillStyle = this._fillGradient;
    } else if (hasFillPattern) {
      ctx.fillStyle = getCanvasPattern.call(fill, ctx);
    }

    if (hasStrokeGradient) {
      ctx.strokeStyle = this._strokeGradient;
    } else if (hasStrokePattern) {
      ctx.strokeStyle = getCanvasPattern.call(stroke, ctx);
    }

    var lineDash = style.lineDash;
    var lineDashOffset = style.lineDashOffset;
    var ctxLineDash = !!ctx.setLineDash; // Update path sx, sy

    var scale = this.getGlobalScale();
    path.setScale(scale[0], scale[1]); // Proxy context
    // Rebuild path in following 2 cases
    // 1. Path is dirty
    // 2. Path needs javascript implemented lineDash stroking.
    //    In this case, lineDash information will not be saved in PathProxy

    if (this.__dirtyPath || lineDash && !ctxLineDash && hasStroke) {
      path.beginPath(ctx); // Setting line dash before build path

      if (lineDash && !ctxLineDash) {
        path.setLineDash(lineDash);
        path.setLineDashOffset(lineDashOffset);
      }

      this.buildPath(path, this.shape, false); // Clear path dirty flag

      if (this.path) {
        this.__dirtyPath = false;
      }
    } else {
      // Replay path building
      ctx.beginPath();
      this.path.rebuildPath(ctx);
    }

    if (hasFill) {
      if (style.fillOpacity != null) {
        var originalGlobalAlpha = ctx.globalAlpha;
        ctx.globalAlpha = style.fillOpacity * style.opacity;
        path.fill(ctx);
        ctx.globalAlpha = originalGlobalAlpha;
      } else {
        path.fill(ctx);
      }
    }

    if (lineDash && ctxLineDash) {
      ctx.setLineDash(lineDash);
      ctx.lineDashOffset = lineDashOffset;
    }

    if (hasStroke) {
      if (style.strokeOpacity != null) {
        var originalGlobalAlpha = ctx.globalAlpha;
        ctx.globalAlpha = style.strokeOpacity * style.opacity;
        path.stroke(ctx);
        ctx.globalAlpha = originalGlobalAlpha;
      } else {
        path.stroke(ctx);
      }
    }

    if (lineDash && ctxLineDash) {
      // PENDING
      // Remove lineDash
      ctx.setLineDash([]);
    } // Draw rect text


    if (style.text != null) {
      // Only restore transform when needs draw text.
      this.restoreTransform(ctx);
      this.drawRectText(ctx, this.getBoundingRect());
    }
  },
  // When bundling path, some shape may decide if use moveTo to begin a new subpath or closePath
  // Like in circle
  buildPath: function buildPath(ctx, shapeCfg, inBundle) {},
  createPathProxy: function createPathProxy() {
    this.path = new PathProxy();
  },
  getBoundingRect: function getBoundingRect() {
    var rect = this._rect;
    var style = this.style;
    var needsUpdateRect = !rect;

    if (needsUpdateRect) {
      var path = this.path;

      if (!path) {
        // Create path on demand.
        path = this.path = new PathProxy();
      }

      if (this.__dirtyPath) {
        path.beginPath();
        this.buildPath(path, this.shape, false);
      }

      rect = path.getBoundingRect();
    }

    this._rect = rect;

    if (style.hasStroke()) {
      // Needs update rect with stroke lineWidth when
      // 1. Element changes scale or lineWidth
      // 2. Shape is changed
      var rectWithStroke = this._rectWithStroke || (this._rectWithStroke = rect.clone());

      if (this.__dirty || needsUpdateRect) {
        rectWithStroke.copy(rect); // FIXME Must after updateTransform

        var w = style.lineWidth; // PENDING, Min line width is needed when line is horizontal or vertical

        var lineScale = style.strokeNoScale ? this.getLineScale() : 1; // Only add extra hover lineWidth when there are no fill

        if (!style.hasFill()) {
          w = Math.max(w, this.strokeContainThreshold || 4);
        } // Consider line width
        // Line scale can't be 0;


        if (lineScale > 1e-10) {
          rectWithStroke.width += w / lineScale;
          rectWithStroke.height += w / lineScale;
          rectWithStroke.x -= w / lineScale / 2;
          rectWithStroke.y -= w / lineScale / 2;
        }
      } // Return rect with stroke


      return rectWithStroke;
    }

    return rect;
  },
  contain: function contain(x, y) {
    var localPos = this.transformCoordToLocal(x, y);
    var rect = this.getBoundingRect();
    var style = this.style;
    x = localPos[0];
    y = localPos[1];

    if (rect.contain(x, y)) {
      var pathData = this.path.data;

      if (style.hasStroke()) {
        var lineWidth = style.lineWidth;
        var lineScale = style.strokeNoScale ? this.getLineScale() : 1; // Line scale can't be 0;

        if (lineScale > 1e-10) {
          // Only add extra hover lineWidth when there are no fill
          if (!style.hasFill()) {
            lineWidth = Math.max(lineWidth, this.strokeContainThreshold);
          }

          if (pathContain.containStroke(pathData, lineWidth / lineScale, x, y)) {
            return true;
          }
        }
      }

      if (style.hasFill()) {
        return pathContain.contain(pathData, x, y);
      }
    }

    return false;
  },

  /**
   * @param  {boolean} dirtyPath
   */
  dirty: function dirty(dirtyPath) {
    if (dirtyPath == null) {
      dirtyPath = true;
    } // Only mark dirty, not mark clean


    if (dirtyPath) {
      this.__dirtyPath = dirtyPath;
      this._rect = null;
    }

    this.__dirty = this.__dirtyText = true;
    this.__zr && this.__zr.refresh(); // Used as a clipping path

    if (this.__clipTarget) {
      this.__clipTarget.dirty();
    }
  },

  /**
   * Alias for animate('shape')
   * @param {boolean} loop
   */
  animateShape: function animateShape(loop) {
    return this.animate('shape', loop);
  },
  // Overwrite attrKV
  attrKV: function attrKV(key, value) {
    // FIXME
    if (key === 'shape') {
      this.setShape(value);
      this.__dirtyPath = true;
      this._rect = null;
    } else {
      Displayable.prototype.attrKV.call(this, key, value);
    }
  },

  /**
   * @param {Object|string} key
   * @param {*} value
   */
  setShape: function setShape(key, value) {
    var shape = this.shape; // Path from string may not have shape

    if (shape) {
      if (zrUtil.isObject(key)) {
        for (var name in key) {
          if (key.hasOwnProperty(name)) {
            shape[name] = key[name];
          }
        }
      } else {
        shape[key] = value;
      }

      this.dirty(true);
    }

    return this;
  },
  getLineScale: function getLineScale() {
    var m = this.transform; // Get the line scale.
    // Determinant of `m` means how much the area is enlarged by the
    // transformation. So its square root can be used as a scale factor
    // for width.

    return m && abs(m[0] - 1) > 1e-10 && abs(m[3] - 1) > 1e-10 ? Math.sqrt(abs(m[0] * m[3] - m[2] * m[1])) : 1;
  }
};
/**
 * 扩展一个 Path element, 比如星形，圆等。
 * Extend a path element
 * @param {Object} props
 * @param {string} props.type Path type
 * @param {Function} props.init Initialize
 * @param {Function} props.buildPath Overwrite buildPath method
 * @param {Object} [props.style] Extended default style config
 * @param {Object} [props.shape] Extended default shape config
 */

Path.extend = function (defaults) {
  var Sub = function Sub(opts) {
    Path.call(this, opts);

    if (defaults.style) {
      // Extend default style
      this.style.extendFrom(defaults.style, false);
    } // Extend default shape


    var defaultShape = defaults.shape;

    if (defaultShape) {
      this.shape = this.shape || {};
      var thisShape = this.shape;

      for (var name in defaultShape) {
        if (!thisShape.hasOwnProperty(name) && defaultShape.hasOwnProperty(name)) {
          thisShape[name] = defaultShape[name];
        }
      }
    }

    defaults.init && defaults.init.call(this, opts);
  };

  zrUtil.inherits(Sub, Path); // FIXME 不能 extend position, rotation 等引用对象

  for (var name in defaults) {
    // Extending prototype values and methods
    if (name !== 'style' && name !== 'shape') {
      Sub.prototype[name] = defaults[name];
    }
  }

  return Sub;
};

zrUtil.inherits(Path, Displayable);
var _default = Path;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9QYXRoLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9QYXRoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBEaXNwbGF5YWJsZSA9IHJlcXVpcmUoXCIuL0Rpc3BsYXlhYmxlXCIpO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcIi4uL2NvcmUvdXRpbFwiKTtcblxudmFyIFBhdGhQcm94eSA9IHJlcXVpcmUoXCIuLi9jb3JlL1BhdGhQcm94eVwiKTtcblxudmFyIHBhdGhDb250YWluID0gcmVxdWlyZShcIi4uL2NvbnRhaW4vcGF0aFwiKTtcblxudmFyIFBhdHRlcm4gPSByZXF1aXJlKFwiLi9QYXR0ZXJuXCIpO1xuXG52YXIgZ2V0Q2FudmFzUGF0dGVybiA9IFBhdHRlcm4ucHJvdG90eXBlLmdldENhbnZhc1BhdHRlcm47XG52YXIgYWJzID0gTWF0aC5hYnM7XG52YXIgcGF0aFByb3h5Rm9yRHJhdyA9IG5ldyBQYXRoUHJveHkodHJ1ZSk7XG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6enJlbmRlci9ncmFwaGljL1BhdGhcbiAqIEBleHRlbmRzIG1vZHVsZTp6cmVuZGVyL2dyYXBoaWMvRGlzcGxheWFibGVcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IG9wdHNcbiAqL1xuXG5mdW5jdGlvbiBQYXRoKG9wdHMpIHtcbiAgRGlzcGxheWFibGUuY2FsbCh0aGlzLCBvcHRzKTtcbiAgLyoqXG4gICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9jb3JlL1BhdGhQcm94eX1cbiAgICogQHJlYWRPbmx5XG4gICAqL1xuXG4gIHRoaXMucGF0aCA9IG51bGw7XG59XG5cblBhdGgucHJvdG90eXBlID0ge1xuICBjb25zdHJ1Y3RvcjogUGF0aCxcbiAgdHlwZTogJ3BhdGgnLFxuICBfX2RpcnR5UGF0aDogdHJ1ZSxcbiAgc3Ryb2tlQ29udGFpblRocmVzaG9sZDogNSxcblxuICAvKipcbiAgICogU2VlIGBtb2R1bGU6enJlbmRlci9zcmMvZ3JhcGhpYy9oZWxwZXIvc3ViUGl4ZWxPcHRpbWl6ZWAuXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cbiAgc3ViUGl4ZWxPcHRpbWl6ZTogZmFsc2UsXG4gIGJydXNoOiBmdW5jdGlvbiAoY3R4LCBwcmV2RWwpIHtcbiAgICB2YXIgc3R5bGUgPSB0aGlzLnN0eWxlO1xuICAgIHZhciBwYXRoID0gdGhpcy5wYXRoIHx8IHBhdGhQcm94eUZvckRyYXc7XG4gICAgdmFyIGhhc1N0cm9rZSA9IHN0eWxlLmhhc1N0cm9rZSgpO1xuICAgIHZhciBoYXNGaWxsID0gc3R5bGUuaGFzRmlsbCgpO1xuICAgIHZhciBmaWxsID0gc3R5bGUuZmlsbDtcbiAgICB2YXIgc3Ryb2tlID0gc3R5bGUuc3Ryb2tlO1xuICAgIHZhciBoYXNGaWxsR3JhZGllbnQgPSBoYXNGaWxsICYmICEhZmlsbC5jb2xvclN0b3BzO1xuICAgIHZhciBoYXNTdHJva2VHcmFkaWVudCA9IGhhc1N0cm9rZSAmJiAhIXN0cm9rZS5jb2xvclN0b3BzO1xuICAgIHZhciBoYXNGaWxsUGF0dGVybiA9IGhhc0ZpbGwgJiYgISFmaWxsLmltYWdlO1xuICAgIHZhciBoYXNTdHJva2VQYXR0ZXJuID0gaGFzU3Ryb2tlICYmICEhc3Ryb2tlLmltYWdlO1xuICAgIHN0eWxlLmJpbmQoY3R4LCB0aGlzLCBwcmV2RWwpO1xuICAgIHRoaXMuc2V0VHJhbnNmb3JtKGN0eCk7XG5cbiAgICBpZiAodGhpcy5fX2RpcnR5KSB7XG4gICAgICB2YXIgcmVjdDsgLy8gVXBkYXRlIGdyYWRpZW50IGJlY2F1c2UgYm91bmRpbmcgcmVjdCBtYXkgY2hhbmdlZFxuXG4gICAgICBpZiAoaGFzRmlsbEdyYWRpZW50KSB7XG4gICAgICAgIHJlY3QgPSByZWN0IHx8IHRoaXMuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgICAgIHRoaXMuX2ZpbGxHcmFkaWVudCA9IHN0eWxlLmdldEdyYWRpZW50KGN0eCwgZmlsbCwgcmVjdCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChoYXNTdHJva2VHcmFkaWVudCkge1xuICAgICAgICByZWN0ID0gcmVjdCB8fCB0aGlzLmdldEJvdW5kaW5nUmVjdCgpO1xuICAgICAgICB0aGlzLl9zdHJva2VHcmFkaWVudCA9IHN0eWxlLmdldEdyYWRpZW50KGN0eCwgc3Ryb2tlLCByZWN0KTtcbiAgICAgIH1cbiAgICB9IC8vIFVzZSB0aGUgZ3JhZGllbnQgb3IgcGF0dGVyblxuXG5cbiAgICBpZiAoaGFzRmlsbEdyYWRpZW50KSB7XG4gICAgICAvLyBQRU5ESU5HIElmIG1heSBoYXZlIGFmZmVjdCB0aGUgc3RhdGVcbiAgICAgIGN0eC5maWxsU3R5bGUgPSB0aGlzLl9maWxsR3JhZGllbnQ7XG4gICAgfSBlbHNlIGlmIChoYXNGaWxsUGF0dGVybikge1xuICAgICAgY3R4LmZpbGxTdHlsZSA9IGdldENhbnZhc1BhdHRlcm4uY2FsbChmaWxsLCBjdHgpO1xuICAgIH1cblxuICAgIGlmIChoYXNTdHJva2VHcmFkaWVudCkge1xuICAgICAgY3R4LnN0cm9rZVN0eWxlID0gdGhpcy5fc3Ryb2tlR3JhZGllbnQ7XG4gICAgfSBlbHNlIGlmIChoYXNTdHJva2VQYXR0ZXJuKSB7XG4gICAgICBjdHguc3Ryb2tlU3R5bGUgPSBnZXRDYW52YXNQYXR0ZXJuLmNhbGwoc3Ryb2tlLCBjdHgpO1xuICAgIH1cblxuICAgIHZhciBsaW5lRGFzaCA9IHN0eWxlLmxpbmVEYXNoO1xuICAgIHZhciBsaW5lRGFzaE9mZnNldCA9IHN0eWxlLmxpbmVEYXNoT2Zmc2V0O1xuICAgIHZhciBjdHhMaW5lRGFzaCA9ICEhY3R4LnNldExpbmVEYXNoOyAvLyBVcGRhdGUgcGF0aCBzeCwgc3lcblxuICAgIHZhciBzY2FsZSA9IHRoaXMuZ2V0R2xvYmFsU2NhbGUoKTtcbiAgICBwYXRoLnNldFNjYWxlKHNjYWxlWzBdLCBzY2FsZVsxXSk7IC8vIFByb3h5IGNvbnRleHRcbiAgICAvLyBSZWJ1aWxkIHBhdGggaW4gZm9sbG93aW5nIDIgY2FzZXNcbiAgICAvLyAxLiBQYXRoIGlzIGRpcnR5XG4gICAgLy8gMi4gUGF0aCBuZWVkcyBqYXZhc2NyaXB0IGltcGxlbWVudGVkIGxpbmVEYXNoIHN0cm9raW5nLlxuICAgIC8vICAgIEluIHRoaXMgY2FzZSwgbGluZURhc2ggaW5mb3JtYXRpb24gd2lsbCBub3QgYmUgc2F2ZWQgaW4gUGF0aFByb3h5XG5cbiAgICBpZiAodGhpcy5fX2RpcnR5UGF0aCB8fCBsaW5lRGFzaCAmJiAhY3R4TGluZURhc2ggJiYgaGFzU3Ryb2tlKSB7XG4gICAgICBwYXRoLmJlZ2luUGF0aChjdHgpOyAvLyBTZXR0aW5nIGxpbmUgZGFzaCBiZWZvcmUgYnVpbGQgcGF0aFxuXG4gICAgICBpZiAobGluZURhc2ggJiYgIWN0eExpbmVEYXNoKSB7XG4gICAgICAgIHBhdGguc2V0TGluZURhc2gobGluZURhc2gpO1xuICAgICAgICBwYXRoLnNldExpbmVEYXNoT2Zmc2V0KGxpbmVEYXNoT2Zmc2V0KTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5idWlsZFBhdGgocGF0aCwgdGhpcy5zaGFwZSwgZmFsc2UpOyAvLyBDbGVhciBwYXRoIGRpcnR5IGZsYWdcblxuICAgICAgaWYgKHRoaXMucGF0aCkge1xuICAgICAgICB0aGlzLl9fZGlydHlQYXRoID0gZmFsc2U7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIFJlcGxheSBwYXRoIGJ1aWxkaW5nXG4gICAgICBjdHguYmVnaW5QYXRoKCk7XG4gICAgICB0aGlzLnBhdGgucmVidWlsZFBhdGgoY3R4KTtcbiAgICB9XG5cbiAgICBpZiAoaGFzRmlsbCkge1xuICAgICAgaWYgKHN0eWxlLmZpbGxPcGFjaXR5ICE9IG51bGwpIHtcbiAgICAgICAgdmFyIG9yaWdpbmFsR2xvYmFsQWxwaGEgPSBjdHguZ2xvYmFsQWxwaGE7XG4gICAgICAgIGN0eC5nbG9iYWxBbHBoYSA9IHN0eWxlLmZpbGxPcGFjaXR5ICogc3R5bGUub3BhY2l0eTtcbiAgICAgICAgcGF0aC5maWxsKGN0eCk7XG4gICAgICAgIGN0eC5nbG9iYWxBbHBoYSA9IG9yaWdpbmFsR2xvYmFsQWxwaGE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBwYXRoLmZpbGwoY3R4KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAobGluZURhc2ggJiYgY3R4TGluZURhc2gpIHtcbiAgICAgIGN0eC5zZXRMaW5lRGFzaChsaW5lRGFzaCk7XG4gICAgICBjdHgubGluZURhc2hPZmZzZXQgPSBsaW5lRGFzaE9mZnNldDtcbiAgICB9XG5cbiAgICBpZiAoaGFzU3Ryb2tlKSB7XG4gICAgICBpZiAoc3R5bGUuc3Ryb2tlT3BhY2l0eSAhPSBudWxsKSB7XG4gICAgICAgIHZhciBvcmlnaW5hbEdsb2JhbEFscGhhID0gY3R4Lmdsb2JhbEFscGhhO1xuICAgICAgICBjdHguZ2xvYmFsQWxwaGEgPSBzdHlsZS5zdHJva2VPcGFjaXR5ICogc3R5bGUub3BhY2l0eTtcbiAgICAgICAgcGF0aC5zdHJva2UoY3R4KTtcbiAgICAgICAgY3R4Lmdsb2JhbEFscGhhID0gb3JpZ2luYWxHbG9iYWxBbHBoYTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBhdGguc3Ryb2tlKGN0eCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGxpbmVEYXNoICYmIGN0eExpbmVEYXNoKSB7XG4gICAgICAvLyBQRU5ESU5HXG4gICAgICAvLyBSZW1vdmUgbGluZURhc2hcbiAgICAgIGN0eC5zZXRMaW5lRGFzaChbXSk7XG4gICAgfSAvLyBEcmF3IHJlY3QgdGV4dFxuXG5cbiAgICBpZiAoc3R5bGUudGV4dCAhPSBudWxsKSB7XG4gICAgICAvLyBPbmx5IHJlc3RvcmUgdHJhbnNmb3JtIHdoZW4gbmVlZHMgZHJhdyB0ZXh0LlxuICAgICAgdGhpcy5yZXN0b3JlVHJhbnNmb3JtKGN0eCk7XG4gICAgICB0aGlzLmRyYXdSZWN0VGV4dChjdHgsIHRoaXMuZ2V0Qm91bmRpbmdSZWN0KCkpO1xuICAgIH1cbiAgfSxcbiAgLy8gV2hlbiBidW5kbGluZyBwYXRoLCBzb21lIHNoYXBlIG1heSBkZWNpZGUgaWYgdXNlIG1vdmVUbyB0byBiZWdpbiBhIG5ldyBzdWJwYXRoIG9yIGNsb3NlUGF0aFxuICAvLyBMaWtlIGluIGNpcmNsZVxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChjdHgsIHNoYXBlQ2ZnLCBpbkJ1bmRsZSkge30sXG4gIGNyZWF0ZVBhdGhQcm94eTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMucGF0aCA9IG5ldyBQYXRoUHJveHkoKTtcbiAgfSxcbiAgZ2V0Qm91bmRpbmdSZWN0OiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHJlY3QgPSB0aGlzLl9yZWN0O1xuICAgIHZhciBzdHlsZSA9IHRoaXMuc3R5bGU7XG4gICAgdmFyIG5lZWRzVXBkYXRlUmVjdCA9ICFyZWN0O1xuXG4gICAgaWYgKG5lZWRzVXBkYXRlUmVjdCkge1xuICAgICAgdmFyIHBhdGggPSB0aGlzLnBhdGg7XG5cbiAgICAgIGlmICghcGF0aCkge1xuICAgICAgICAvLyBDcmVhdGUgcGF0aCBvbiBkZW1hbmQuXG4gICAgICAgIHBhdGggPSB0aGlzLnBhdGggPSBuZXcgUGF0aFByb3h5KCk7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLl9fZGlydHlQYXRoKSB7XG4gICAgICAgIHBhdGguYmVnaW5QYXRoKCk7XG4gICAgICAgIHRoaXMuYnVpbGRQYXRoKHBhdGgsIHRoaXMuc2hhcGUsIGZhbHNlKTtcbiAgICAgIH1cblxuICAgICAgcmVjdCA9IHBhdGguZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgfVxuXG4gICAgdGhpcy5fcmVjdCA9IHJlY3Q7XG5cbiAgICBpZiAoc3R5bGUuaGFzU3Ryb2tlKCkpIHtcbiAgICAgIC8vIE5lZWRzIHVwZGF0ZSByZWN0IHdpdGggc3Ryb2tlIGxpbmVXaWR0aCB3aGVuXG4gICAgICAvLyAxLiBFbGVtZW50IGNoYW5nZXMgc2NhbGUgb3IgbGluZVdpZHRoXG4gICAgICAvLyAyLiBTaGFwZSBpcyBjaGFuZ2VkXG4gICAgICB2YXIgcmVjdFdpdGhTdHJva2UgPSB0aGlzLl9yZWN0V2l0aFN0cm9rZSB8fCAodGhpcy5fcmVjdFdpdGhTdHJva2UgPSByZWN0LmNsb25lKCkpO1xuXG4gICAgICBpZiAodGhpcy5fX2RpcnR5IHx8IG5lZWRzVXBkYXRlUmVjdCkge1xuICAgICAgICByZWN0V2l0aFN0cm9rZS5jb3B5KHJlY3QpOyAvLyBGSVhNRSBNdXN0IGFmdGVyIHVwZGF0ZVRyYW5zZm9ybVxuXG4gICAgICAgIHZhciB3ID0gc3R5bGUubGluZVdpZHRoOyAvLyBQRU5ESU5HLCBNaW4gbGluZSB3aWR0aCBpcyBuZWVkZWQgd2hlbiBsaW5lIGlzIGhvcml6b250YWwgb3IgdmVydGljYWxcblxuICAgICAgICB2YXIgbGluZVNjYWxlID0gc3R5bGUuc3Ryb2tlTm9TY2FsZSA/IHRoaXMuZ2V0TGluZVNjYWxlKCkgOiAxOyAvLyBPbmx5IGFkZCBleHRyYSBob3ZlciBsaW5lV2lkdGggd2hlbiB0aGVyZSBhcmUgbm8gZmlsbFxuXG4gICAgICAgIGlmICghc3R5bGUuaGFzRmlsbCgpKSB7XG4gICAgICAgICAgdyA9IE1hdGgubWF4KHcsIHRoaXMuc3Ryb2tlQ29udGFpblRocmVzaG9sZCB8fCA0KTtcbiAgICAgICAgfSAvLyBDb25zaWRlciBsaW5lIHdpZHRoXG4gICAgICAgIC8vIExpbmUgc2NhbGUgY2FuJ3QgYmUgMDtcblxuXG4gICAgICAgIGlmIChsaW5lU2NhbGUgPiAxZS0xMCkge1xuICAgICAgICAgIHJlY3RXaXRoU3Ryb2tlLndpZHRoICs9IHcgLyBsaW5lU2NhbGU7XG4gICAgICAgICAgcmVjdFdpdGhTdHJva2UuaGVpZ2h0ICs9IHcgLyBsaW5lU2NhbGU7XG4gICAgICAgICAgcmVjdFdpdGhTdHJva2UueCAtPSB3IC8gbGluZVNjYWxlIC8gMjtcbiAgICAgICAgICByZWN0V2l0aFN0cm9rZS55IC09IHcgLyBsaW5lU2NhbGUgLyAyO1xuICAgICAgICB9XG4gICAgICB9IC8vIFJldHVybiByZWN0IHdpdGggc3Ryb2tlXG5cblxuICAgICAgcmV0dXJuIHJlY3RXaXRoU3Ryb2tlO1xuICAgIH1cblxuICAgIHJldHVybiByZWN0O1xuICB9LFxuICBjb250YWluOiBmdW5jdGlvbiAoeCwgeSkge1xuICAgIHZhciBsb2NhbFBvcyA9IHRoaXMudHJhbnNmb3JtQ29vcmRUb0xvY2FsKHgsIHkpO1xuICAgIHZhciByZWN0ID0gdGhpcy5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgc3R5bGUgPSB0aGlzLnN0eWxlO1xuICAgIHggPSBsb2NhbFBvc1swXTtcbiAgICB5ID0gbG9jYWxQb3NbMV07XG5cbiAgICBpZiAocmVjdC5jb250YWluKHgsIHkpKSB7XG4gICAgICB2YXIgcGF0aERhdGEgPSB0aGlzLnBhdGguZGF0YTtcblxuICAgICAgaWYgKHN0eWxlLmhhc1N0cm9rZSgpKSB7XG4gICAgICAgIHZhciBsaW5lV2lkdGggPSBzdHlsZS5saW5lV2lkdGg7XG4gICAgICAgIHZhciBsaW5lU2NhbGUgPSBzdHlsZS5zdHJva2VOb1NjYWxlID8gdGhpcy5nZXRMaW5lU2NhbGUoKSA6IDE7IC8vIExpbmUgc2NhbGUgY2FuJ3QgYmUgMDtcblxuICAgICAgICBpZiAobGluZVNjYWxlID4gMWUtMTApIHtcbiAgICAgICAgICAvLyBPbmx5IGFkZCBleHRyYSBob3ZlciBsaW5lV2lkdGggd2hlbiB0aGVyZSBhcmUgbm8gZmlsbFxuICAgICAgICAgIGlmICghc3R5bGUuaGFzRmlsbCgpKSB7XG4gICAgICAgICAgICBsaW5lV2lkdGggPSBNYXRoLm1heChsaW5lV2lkdGgsIHRoaXMuc3Ryb2tlQ29udGFpblRocmVzaG9sZCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKHBhdGhDb250YWluLmNvbnRhaW5TdHJva2UocGF0aERhdGEsIGxpbmVXaWR0aCAvIGxpbmVTY2FsZSwgeCwgeSkpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAoc3R5bGUuaGFzRmlsbCgpKSB7XG4gICAgICAgIHJldHVybiBwYXRoQ29udGFpbi5jb250YWluKHBhdGhEYXRhLCB4LCB5KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEBwYXJhbSAge2Jvb2xlYW59IGRpcnR5UGF0aFxuICAgKi9cbiAgZGlydHk6IGZ1bmN0aW9uIChkaXJ0eVBhdGgpIHtcbiAgICBpZiAoZGlydHlQYXRoID09IG51bGwpIHtcbiAgICAgIGRpcnR5UGF0aCA9IHRydWU7XG4gICAgfSAvLyBPbmx5IG1hcmsgZGlydHksIG5vdCBtYXJrIGNsZWFuXG5cblxuICAgIGlmIChkaXJ0eVBhdGgpIHtcbiAgICAgIHRoaXMuX19kaXJ0eVBhdGggPSBkaXJ0eVBhdGg7XG4gICAgICB0aGlzLl9yZWN0ID0gbnVsbDtcbiAgICB9XG5cbiAgICB0aGlzLl9fZGlydHkgPSB0aGlzLl9fZGlydHlUZXh0ID0gdHJ1ZTtcbiAgICB0aGlzLl9fenIgJiYgdGhpcy5fX3pyLnJlZnJlc2goKTsgLy8gVXNlZCBhcyBhIGNsaXBwaW5nIHBhdGhcblxuICAgIGlmICh0aGlzLl9fY2xpcFRhcmdldCkge1xuICAgICAgdGhpcy5fX2NsaXBUYXJnZXQuZGlydHkoKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEFsaWFzIGZvciBhbmltYXRlKCdzaGFwZScpXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gbG9vcFxuICAgKi9cbiAgYW5pbWF0ZVNoYXBlOiBmdW5jdGlvbiAobG9vcCkge1xuICAgIHJldHVybiB0aGlzLmFuaW1hdGUoJ3NoYXBlJywgbG9vcCk7XG4gIH0sXG4gIC8vIE92ZXJ3cml0ZSBhdHRyS1ZcbiAgYXR0cktWOiBmdW5jdGlvbiAoa2V5LCB2YWx1ZSkge1xuICAgIC8vIEZJWE1FXG4gICAgaWYgKGtleSA9PT0gJ3NoYXBlJykge1xuICAgICAgdGhpcy5zZXRTaGFwZSh2YWx1ZSk7XG4gICAgICB0aGlzLl9fZGlydHlQYXRoID0gdHJ1ZTtcbiAgICAgIHRoaXMuX3JlY3QgPSBudWxsO1xuICAgIH0gZWxzZSB7XG4gICAgICBEaXNwbGF5YWJsZS5wcm90b3R5cGUuYXR0cktWLmNhbGwodGhpcywga2V5LCB2YWx1ZSk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge09iamVjdHxzdHJpbmd9IGtleVxuICAgKiBAcGFyYW0geyp9IHZhbHVlXG4gICAqL1xuICBzZXRTaGFwZTogZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgICB2YXIgc2hhcGUgPSB0aGlzLnNoYXBlOyAvLyBQYXRoIGZyb20gc3RyaW5nIG1heSBub3QgaGF2ZSBzaGFwZVxuXG4gICAgaWYgKHNoYXBlKSB7XG4gICAgICBpZiAoenJVdGlsLmlzT2JqZWN0KGtleSkpIHtcbiAgICAgICAgZm9yICh2YXIgbmFtZSBpbiBrZXkpIHtcbiAgICAgICAgICBpZiAoa2V5Lmhhc093blByb3BlcnR5KG5hbWUpKSB7XG4gICAgICAgICAgICBzaGFwZVtuYW1lXSA9IGtleVtuYW1lXTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNoYXBlW2tleV0gPSB2YWx1ZTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5kaXJ0eSh0cnVlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcbiAgZ2V0TGluZVNjYWxlOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIG0gPSB0aGlzLnRyYW5zZm9ybTsgLy8gR2V0IHRoZSBsaW5lIHNjYWxlLlxuICAgIC8vIERldGVybWluYW50IG9mIGBtYCBtZWFucyBob3cgbXVjaCB0aGUgYXJlYSBpcyBlbmxhcmdlZCBieSB0aGVcbiAgICAvLyB0cmFuc2Zvcm1hdGlvbi4gU28gaXRzIHNxdWFyZSByb290IGNhbiBiZSB1c2VkIGFzIGEgc2NhbGUgZmFjdG9yXG4gICAgLy8gZm9yIHdpZHRoLlxuXG4gICAgcmV0dXJuIG0gJiYgYWJzKG1bMF0gLSAxKSA+IDFlLTEwICYmIGFicyhtWzNdIC0gMSkgPiAxZS0xMCA/IE1hdGguc3FydChhYnMobVswXSAqIG1bM10gLSBtWzJdICogbVsxXSkpIDogMTtcbiAgfVxufTtcbi8qKlxuICog5omp5bGV5LiA5LiqIFBhdGggZWxlbWVudCwg5q+U5aaC5pif5b2i77yM5ZyG562J44CCXG4gKiBFeHRlbmQgYSBwYXRoIGVsZW1lbnRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwcm9wc1xuICogQHBhcmFtIHtzdHJpbmd9IHByb3BzLnR5cGUgUGF0aCB0eXBlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcm9wcy5pbml0IEluaXRpYWxpemVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHByb3BzLmJ1aWxkUGF0aCBPdmVyd3JpdGUgYnVpbGRQYXRoIG1ldGhvZFxuICogQHBhcmFtIHtPYmplY3R9IFtwcm9wcy5zdHlsZV0gRXh0ZW5kZWQgZGVmYXVsdCBzdHlsZSBjb25maWdcbiAqIEBwYXJhbSB7T2JqZWN0fSBbcHJvcHMuc2hhcGVdIEV4dGVuZGVkIGRlZmF1bHQgc2hhcGUgY29uZmlnXG4gKi9cblxuUGF0aC5leHRlbmQgPSBmdW5jdGlvbiAoZGVmYXVsdHMpIHtcbiAgdmFyIFN1YiA9IGZ1bmN0aW9uIChvcHRzKSB7XG4gICAgUGF0aC5jYWxsKHRoaXMsIG9wdHMpO1xuXG4gICAgaWYgKGRlZmF1bHRzLnN0eWxlKSB7XG4gICAgICAvLyBFeHRlbmQgZGVmYXVsdCBzdHlsZVxuICAgICAgdGhpcy5zdHlsZS5leHRlbmRGcm9tKGRlZmF1bHRzLnN0eWxlLCBmYWxzZSk7XG4gICAgfSAvLyBFeHRlbmQgZGVmYXVsdCBzaGFwZVxuXG5cbiAgICB2YXIgZGVmYXVsdFNoYXBlID0gZGVmYXVsdHMuc2hhcGU7XG5cbiAgICBpZiAoZGVmYXVsdFNoYXBlKSB7XG4gICAgICB0aGlzLnNoYXBlID0gdGhpcy5zaGFwZSB8fCB7fTtcbiAgICAgIHZhciB0aGlzU2hhcGUgPSB0aGlzLnNoYXBlO1xuXG4gICAgICBmb3IgKHZhciBuYW1lIGluIGRlZmF1bHRTaGFwZSkge1xuICAgICAgICBpZiAoIXRoaXNTaGFwZS5oYXNPd25Qcm9wZXJ0eShuYW1lKSAmJiBkZWZhdWx0U2hhcGUuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgICAgICB0aGlzU2hhcGVbbmFtZV0gPSBkZWZhdWx0U2hhcGVbbmFtZV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBkZWZhdWx0cy5pbml0ICYmIGRlZmF1bHRzLmluaXQuY2FsbCh0aGlzLCBvcHRzKTtcbiAgfTtcblxuICB6clV0aWwuaW5oZXJpdHMoU3ViLCBQYXRoKTsgLy8gRklYTUUg5LiN6IO9IGV4dGVuZCBwb3NpdGlvbiwgcm90YXRpb24g562J5byV55So5a+56LGhXG5cbiAgZm9yICh2YXIgbmFtZSBpbiBkZWZhdWx0cykge1xuICAgIC8vIEV4dGVuZGluZyBwcm90b3R5cGUgdmFsdWVzIGFuZCBtZXRob2RzXG4gICAgaWYgKG5hbWUgIT09ICdzdHlsZScgJiYgbmFtZSAhPT0gJ3NoYXBlJykge1xuICAgICAgU3ViLnByb3RvdHlwZVtuYW1lXSA9IGRlZmF1bHRzW25hbWVdO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBTdWI7XG59O1xuXG56clV0aWwuaW5oZXJpdHMoUGF0aCwgRGlzcGxheWFibGUpO1xudmFyIF9kZWZhdWx0ID0gUGF0aDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsU0E7QUFvU0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Path.js
