__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mixin", function() { return mixin; });
/* harmony import */ var _createBaseForm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createBaseForm */ "./node_modules/rc-form/es/createBaseForm.js");

var mixin = {
  getForm: function getForm() {
    return {
      getFieldsValue: this.fieldsStore.getFieldsValue,
      getFieldValue: this.fieldsStore.getFieldValue,
      getFieldInstance: this.getFieldInstance,
      setFieldsValue: this.setFieldsValue,
      setFields: this.setFields,
      setFieldsInitialValue: this.fieldsStore.setFieldsInitialValue,
      getFieldDecorator: this.getFieldDecorator,
      getFieldProps: this.getFieldProps,
      getFieldsError: this.fieldsStore.getFieldsError,
      getFieldError: this.fieldsStore.getFieldError,
      isFieldValidating: this.fieldsStore.isFieldValidating,
      isFieldsValidating: this.fieldsStore.isFieldsValidating,
      isFieldsTouched: this.fieldsStore.isFieldsTouched,
      isFieldTouched: this.fieldsStore.isFieldTouched,
      isSubmitting: this.isSubmitting,
      submit: this.submit,
      validateFields: this.validateFields,
      resetFields: this.resetFields
    };
  }
};

function createForm(options) {
  return Object(_createBaseForm__WEBPACK_IMPORTED_MODULE_0__["default"])(options, [mixin]);
}

/* harmony default export */ __webpack_exports__["default"] = (createForm);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZm9ybS9lcy9jcmVhdGVGb3JtLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZm9ybS9lcy9jcmVhdGVGb3JtLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjcmVhdGVCYXNlRm9ybSBmcm9tICcuL2NyZWF0ZUJhc2VGb3JtJztcblxuZXhwb3J0IHZhciBtaXhpbiA9IHtcbiAgZ2V0Rm9ybTogZnVuY3Rpb24gZ2V0Rm9ybSgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZ2V0RmllbGRzVmFsdWU6IHRoaXMuZmllbGRzU3RvcmUuZ2V0RmllbGRzVmFsdWUsXG4gICAgICBnZXRGaWVsZFZhbHVlOiB0aGlzLmZpZWxkc1N0b3JlLmdldEZpZWxkVmFsdWUsXG4gICAgICBnZXRGaWVsZEluc3RhbmNlOiB0aGlzLmdldEZpZWxkSW5zdGFuY2UsXG4gICAgICBzZXRGaWVsZHNWYWx1ZTogdGhpcy5zZXRGaWVsZHNWYWx1ZSxcbiAgICAgIHNldEZpZWxkczogdGhpcy5zZXRGaWVsZHMsXG4gICAgICBzZXRGaWVsZHNJbml0aWFsVmFsdWU6IHRoaXMuZmllbGRzU3RvcmUuc2V0RmllbGRzSW5pdGlhbFZhbHVlLFxuICAgICAgZ2V0RmllbGREZWNvcmF0b3I6IHRoaXMuZ2V0RmllbGREZWNvcmF0b3IsXG4gICAgICBnZXRGaWVsZFByb3BzOiB0aGlzLmdldEZpZWxkUHJvcHMsXG4gICAgICBnZXRGaWVsZHNFcnJvcjogdGhpcy5maWVsZHNTdG9yZS5nZXRGaWVsZHNFcnJvcixcbiAgICAgIGdldEZpZWxkRXJyb3I6IHRoaXMuZmllbGRzU3RvcmUuZ2V0RmllbGRFcnJvcixcbiAgICAgIGlzRmllbGRWYWxpZGF0aW5nOiB0aGlzLmZpZWxkc1N0b3JlLmlzRmllbGRWYWxpZGF0aW5nLFxuICAgICAgaXNGaWVsZHNWYWxpZGF0aW5nOiB0aGlzLmZpZWxkc1N0b3JlLmlzRmllbGRzVmFsaWRhdGluZyxcbiAgICAgIGlzRmllbGRzVG91Y2hlZDogdGhpcy5maWVsZHNTdG9yZS5pc0ZpZWxkc1RvdWNoZWQsXG4gICAgICBpc0ZpZWxkVG91Y2hlZDogdGhpcy5maWVsZHNTdG9yZS5pc0ZpZWxkVG91Y2hlZCxcbiAgICAgIGlzU3VibWl0dGluZzogdGhpcy5pc1N1Ym1pdHRpbmcsXG4gICAgICBzdWJtaXQ6IHRoaXMuc3VibWl0LFxuICAgICAgdmFsaWRhdGVGaWVsZHM6IHRoaXMudmFsaWRhdGVGaWVsZHMsXG4gICAgICByZXNldEZpZWxkczogdGhpcy5yZXNldEZpZWxkc1xuICAgIH07XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGNyZWF0ZUZvcm0ob3B0aW9ucykge1xuICByZXR1cm4gY3JlYXRlQmFzZUZvcm0ob3B0aW9ucywgW21peGluXSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUZvcm07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWxCQTtBQW9CQTtBQXRCQTtBQUNBO0FBd0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-form/es/createForm.js
