/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DefaultDraftBlockRenderMap
 * @format
 * 
 */


var _require = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js"),
    Map = _require.Map;

var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var cx = __webpack_require__(/*! fbjs/lib/cx */ "./node_modules/fbjs/lib/cx.js");

var UL_WRAP = React.createElement('ul', {
  className: cx('public/DraftStyleDefault/ul')
});
var OL_WRAP = React.createElement('ol', {
  className: cx('public/DraftStyleDefault/ol')
});
var PRE_WRAP = React.createElement('pre', {
  className: cx('public/DraftStyleDefault/pre')
});
var DefaultDraftBlockRenderMap = Map({
  'header-one': {
    element: 'h1'
  },
  'header-two': {
    element: 'h2'
  },
  'header-three': {
    element: 'h3'
  },
  'header-four': {
    element: 'h4'
  },
  'header-five': {
    element: 'h5'
  },
  'header-six': {
    element: 'h6'
  },
  'unordered-list-item': {
    element: 'li',
    wrapper: UL_WRAP
  },
  'ordered-list-item': {
    element: 'li',
    wrapper: OL_WRAP
  },
  blockquote: {
    element: 'blockquote'
  },
  atomic: {
    element: 'figure'
  },
  'code-block': {
    element: 'pre',
    wrapper: PRE_WRAP
  },
  unstyled: {
    element: 'div',
    aliasedElements: ['p']
  }
});
module.exports = DefaultDraftBlockRenderMap;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RlZmF1bHREcmFmdEJsb2NrUmVuZGVyTWFwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgRGVmYXVsdERyYWZ0QmxvY2tSZW5kZXJNYXBcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9yZXF1aXJlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyksXG4gICAgTWFwID0gX3JlcXVpcmUuTWFwO1xuXG52YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgY3ggPSByZXF1aXJlKCdmYmpzL2xpYi9jeCcpO1xuXG52YXIgVUxfV1JBUCA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoJ3VsJywgeyBjbGFzc05hbWU6IGN4KCdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvdWwnKSB9KTtcbnZhciBPTF9XUkFQID0gUmVhY3QuY3JlYXRlRWxlbWVudCgnb2wnLCB7IGNsYXNzTmFtZTogY3goJ3B1YmxpYy9EcmFmdFN0eWxlRGVmYXVsdC9vbCcpIH0pO1xudmFyIFBSRV9XUkFQID0gUmVhY3QuY3JlYXRlRWxlbWVudCgncHJlJywgeyBjbGFzc05hbWU6IGN4KCdwdWJsaWMvRHJhZnRTdHlsZURlZmF1bHQvcHJlJykgfSk7XG5cbnZhciBEZWZhdWx0RHJhZnRCbG9ja1JlbmRlck1hcCA9IE1hcCh7XG4gICdoZWFkZXItb25lJzoge1xuICAgIGVsZW1lbnQ6ICdoMSdcbiAgfSxcbiAgJ2hlYWRlci10d28nOiB7XG4gICAgZWxlbWVudDogJ2gyJ1xuICB9LFxuICAnaGVhZGVyLXRocmVlJzoge1xuICAgIGVsZW1lbnQ6ICdoMydcbiAgfSxcbiAgJ2hlYWRlci1mb3VyJzoge1xuICAgIGVsZW1lbnQ6ICdoNCdcbiAgfSxcbiAgJ2hlYWRlci1maXZlJzoge1xuICAgIGVsZW1lbnQ6ICdoNSdcbiAgfSxcbiAgJ2hlYWRlci1zaXgnOiB7XG4gICAgZWxlbWVudDogJ2g2J1xuICB9LFxuICAndW5vcmRlcmVkLWxpc3QtaXRlbSc6IHtcbiAgICBlbGVtZW50OiAnbGknLFxuICAgIHdyYXBwZXI6IFVMX1dSQVBcbiAgfSxcbiAgJ29yZGVyZWQtbGlzdC1pdGVtJzoge1xuICAgIGVsZW1lbnQ6ICdsaScsXG4gICAgd3JhcHBlcjogT0xfV1JBUFxuICB9LFxuICBibG9ja3F1b3RlOiB7XG4gICAgZWxlbWVudDogJ2Jsb2NrcXVvdGUnXG4gIH0sXG4gIGF0b21pYzoge1xuICAgIGVsZW1lbnQ6ICdmaWd1cmUnXG4gIH0sXG4gICdjb2RlLWJsb2NrJzoge1xuICAgIGVsZW1lbnQ6ICdwcmUnLFxuICAgIHdyYXBwZXI6IFBSRV9XUkFQXG4gIH0sXG4gIHVuc3R5bGVkOiB7XG4gICAgZWxlbWVudDogJ2RpdicsXG4gICAgYWxpYXNlZEVsZW1lbnRzOiBbJ3AnXVxuICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBEZWZhdWx0RHJhZnRCbG9ja1JlbmRlck1hcDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBckNBO0FBMkNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DefaultDraftBlockRenderMap.js
