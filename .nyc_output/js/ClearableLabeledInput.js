__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hasPrefixSuffix", function() { return hasPrefixSuffix; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _Input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Input */ "./node_modules/antd/es/input/Input.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var ClearableInputType = Object(_util_type__WEBPACK_IMPORTED_MODULE_4__["tuple"])('text', 'input');
function hasPrefixSuffix(props) {
  return !!(props.prefix || props.suffix || props.allowClear);
}

var ClearableLabeledInput =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ClearableLabeledInput, _React$Component);

  function ClearableLabeledInput() {
    _classCallCheck(this, ClearableLabeledInput);

    return _possibleConstructorReturn(this, _getPrototypeOf(ClearableLabeledInput).apply(this, arguments));
  }

  _createClass(ClearableLabeledInput, [{
    key: "renderClearIcon",
    value: function renderClearIcon(prefixCls) {
      var _this$props = this.props,
          allowClear = _this$props.allowClear,
          value = _this$props.value,
          disabled = _this$props.disabled,
          inputType = _this$props.inputType,
          handleReset = _this$props.handleReset;

      if (!allowClear || disabled || value === undefined || value === null || value === '') {
        return null;
      }

      var className = inputType === ClearableInputType[0] ? "".concat(prefixCls, "-textarea-clear-icon") : "".concat(prefixCls, "-clear-icon");
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
        type: "close-circle",
        theme: "filled",
        onClick: handleReset,
        className: className,
        role: "button"
      });
    }
  }, {
    key: "renderSuffix",
    value: function renderSuffix(prefixCls) {
      var _this$props2 = this.props,
          suffix = _this$props2.suffix,
          allowClear = _this$props2.allowClear;

      if (suffix || allowClear) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: "".concat(prefixCls, "-suffix")
        }, this.renderClearIcon(prefixCls), suffix);
      }

      return null;
    }
  }, {
    key: "renderLabeledIcon",
    value: function renderLabeledIcon(prefixCls, element) {
      var _classNames;

      var props = this.props;
      var suffix = this.renderSuffix(prefixCls);

      if (!hasPrefixSuffix(props)) {
        return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](element, {
          value: props.value
        });
      }

      var prefix = props.prefix ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-prefix")
      }, props.prefix) : null;
      var affixWrapperCls = classnames__WEBPACK_IMPORTED_MODULE_2___default()(props.className, "".concat(prefixCls, "-affix-wrapper"), (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-affix-wrapper-sm"), props.size === 'small'), _defineProperty(_classNames, "".concat(prefixCls, "-affix-wrapper-lg"), props.size === 'large'), _defineProperty(_classNames, "".concat(prefixCls, "-affix-wrapper-input-with-clear-btn"), props.suffix && props.allowClear && this.props.value), _classNames));
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: affixWrapperCls,
        style: props.style
      }, prefix, react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](element, {
        style: null,
        value: props.value,
        className: Object(_Input__WEBPACK_IMPORTED_MODULE_5__["getInputClassName"])(prefixCls, props.size, props.disabled)
      }), suffix);
    }
  }, {
    key: "renderInputWithLabel",
    value: function renderInputWithLabel(prefixCls, labeledElement) {
      var _classNames3;

      var _this$props3 = this.props,
          addonBefore = _this$props3.addonBefore,
          addonAfter = _this$props3.addonAfter,
          style = _this$props3.style,
          size = _this$props3.size,
          className = _this$props3.className; // Not wrap when there is not addons

      if (!addonBefore && !addonAfter) {
        return labeledElement;
      }

      var wrapperClassName = "".concat(prefixCls, "-group");
      var addonClassName = "".concat(wrapperClassName, "-addon");
      var addonBeforeNode = addonBefore ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: addonClassName
      }, addonBefore) : null;
      var addonAfterNode = addonAfter ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: addonClassName
      }, addonAfter) : null;
      var mergedWrapperClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-wrapper"), _defineProperty({}, wrapperClassName, addonBefore || addonAfter));
      var mergedGroupClassName = classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, "".concat(prefixCls, "-group-wrapper"), (_classNames3 = {}, _defineProperty(_classNames3, "".concat(prefixCls, "-group-wrapper-sm"), size === 'small'), _defineProperty(_classNames3, "".concat(prefixCls, "-group-wrapper-lg"), size === 'large'), _classNames3)); // Need another wrapper for changing display:table to display:inline-block
      // and put style prop in wrapper

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: mergedGroupClassName,
        style: style
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: mergedWrapperClassName
      }, addonBeforeNode, react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](labeledElement, {
        style: null
      }), addonAfterNode));
    }
  }, {
    key: "renderTextAreaWithClearIcon",
    value: function renderTextAreaWithClearIcon(prefixCls, element) {
      var _this$props4 = this.props,
          value = _this$props4.value,
          allowClear = _this$props4.allowClear,
          className = _this$props4.className,
          style = _this$props4.style;

      if (!allowClear) {
        return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](element, {
          value: value
        });
      }

      var affixWrapperCls = classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, "".concat(prefixCls, "-affix-wrapper"), "".concat(prefixCls, "-affix-wrapper-textarea-with-clear-btn"));
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: affixWrapperCls,
        style: style
      }, react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](element, {
        style: null,
        value: value
      }), this.renderClearIcon(prefixCls));
    }
  }, {
    key: "renderClearableLabeledInput",
    value: function renderClearableLabeledInput() {
      var _this$props5 = this.props,
          prefixCls = _this$props5.prefixCls,
          inputType = _this$props5.inputType,
          element = _this$props5.element;

      if (inputType === ClearableInputType[0]) {
        return this.renderTextAreaWithClearIcon(prefixCls, element);
      }

      return this.renderInputWithLabel(prefixCls, this.renderLabeledIcon(prefixCls, element));
    }
  }, {
    key: "render",
    value: function render() {
      return this.renderClearableLabeledInput();
    }
  }]);

  return ClearableLabeledInput;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__["polyfill"])(ClearableLabeledInput);
/* harmony default export */ __webpack_exports__["default"] = (ClearableLabeledInput);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9DbGVhcmFibGVMYWJlbGVkSW5wdXQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2lucHV0L0NsZWFyYWJsZUxhYmVsZWRJbnB1dC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuaW1wb3J0IHsgdHVwbGUgfSBmcm9tICcuLi9fdXRpbC90eXBlJztcbmltcG9ydCB7IGdldElucHV0Q2xhc3NOYW1lIH0gZnJvbSAnLi9JbnB1dCc7XG5jb25zdCBDbGVhcmFibGVJbnB1dFR5cGUgPSB0dXBsZSgndGV4dCcsICdpbnB1dCcpO1xuZXhwb3J0IGZ1bmN0aW9uIGhhc1ByZWZpeFN1ZmZpeChwcm9wcykge1xuICAgIHJldHVybiAhIShwcm9wcy5wcmVmaXggfHwgcHJvcHMuc3VmZml4IHx8IHByb3BzLmFsbG93Q2xlYXIpO1xufVxuY2xhc3MgQ2xlYXJhYmxlTGFiZWxlZElucHV0IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICByZW5kZXJDbGVhckljb24ocHJlZml4Q2xzKSB7XG4gICAgICAgIGNvbnN0IHsgYWxsb3dDbGVhciwgdmFsdWUsIGRpc2FibGVkLCBpbnB1dFR5cGUsIGhhbmRsZVJlc2V0IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBpZiAoIWFsbG93Q2xlYXIgfHwgZGlzYWJsZWQgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gbnVsbCB8fCB2YWx1ZSA9PT0gJycpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGNsYXNzTmFtZSA9IGlucHV0VHlwZSA9PT0gQ2xlYXJhYmxlSW5wdXRUeXBlWzBdXG4gICAgICAgICAgICA/IGAke3ByZWZpeENsc30tdGV4dGFyZWEtY2xlYXItaWNvbmBcbiAgICAgICAgICAgIDogYCR7cHJlZml4Q2xzfS1jbGVhci1pY29uYDtcbiAgICAgICAgcmV0dXJuICg8SWNvbiB0eXBlPVwiY2xvc2UtY2lyY2xlXCIgdGhlbWU9XCJmaWxsZWRcIiBvbkNsaWNrPXtoYW5kbGVSZXNldH0gY2xhc3NOYW1lPXtjbGFzc05hbWV9IHJvbGU9XCJidXR0b25cIi8+KTtcbiAgICB9XG4gICAgcmVuZGVyU3VmZml4KHByZWZpeENscykge1xuICAgICAgICBjb25zdCB7IHN1ZmZpeCwgYWxsb3dDbGVhciB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKHN1ZmZpeCB8fCBhbGxvd0NsZWFyKSB7XG4gICAgICAgICAgICByZXR1cm4gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zdWZmaXhgfT5cbiAgICAgICAgICB7dGhpcy5yZW5kZXJDbGVhckljb24ocHJlZml4Q2xzKX1cbiAgICAgICAgICB7c3VmZml4fVxuICAgICAgICA8L3NwYW4+KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmVuZGVyTGFiZWxlZEljb24ocHJlZml4Q2xzLCBlbGVtZW50KSB7XG4gICAgICAgIGNvbnN0IHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3Qgc3VmZml4ID0gdGhpcy5yZW5kZXJTdWZmaXgocHJlZml4Q2xzKTtcbiAgICAgICAgaWYgKCFoYXNQcmVmaXhTdWZmaXgocHJvcHMpKSB7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QuY2xvbmVFbGVtZW50KGVsZW1lbnQsIHtcbiAgICAgICAgICAgICAgICB2YWx1ZTogcHJvcHMudmFsdWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBwcmVmaXggPSBwcm9wcy5wcmVmaXggPyAoPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXByZWZpeGB9Pntwcm9wcy5wcmVmaXh9PC9zcGFuPikgOiBudWxsO1xuICAgICAgICBjb25zdCBhZmZpeFdyYXBwZXJDbHMgPSBjbGFzc05hbWVzKHByb3BzLmNsYXNzTmFtZSwgYCR7cHJlZml4Q2xzfS1hZmZpeC13cmFwcGVyYCwge1xuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tYWZmaXgtd3JhcHBlci1zbWBdOiBwcm9wcy5zaXplID09PSAnc21hbGwnLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tYWZmaXgtd3JhcHBlci1sZ2BdOiBwcm9wcy5zaXplID09PSAnbGFyZ2UnLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tYWZmaXgtd3JhcHBlci1pbnB1dC13aXRoLWNsZWFyLWJ0bmBdOiBwcm9wcy5zdWZmaXggJiYgcHJvcHMuYWxsb3dDbGVhciAmJiB0aGlzLnByb3BzLnZhbHVlLFxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuICg8c3BhbiBjbGFzc05hbWU9e2FmZml4V3JhcHBlckNsc30gc3R5bGU9e3Byb3BzLnN0eWxlfT5cbiAgICAgICAge3ByZWZpeH1cbiAgICAgICAge1JlYWN0LmNsb25lRWxlbWVudChlbGVtZW50LCB7XG4gICAgICAgICAgICBzdHlsZTogbnVsbCxcbiAgICAgICAgICAgIHZhbHVlOiBwcm9wcy52YWx1ZSxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogZ2V0SW5wdXRDbGFzc05hbWUocHJlZml4Q2xzLCBwcm9wcy5zaXplLCBwcm9wcy5kaXNhYmxlZCksXG4gICAgICAgIH0pfVxuICAgICAgICB7c3VmZml4fVxuICAgICAgPC9zcGFuPik7XG4gICAgfVxuICAgIHJlbmRlcklucHV0V2l0aExhYmVsKHByZWZpeENscywgbGFiZWxlZEVsZW1lbnQpIHtcbiAgICAgICAgY29uc3QgeyBhZGRvbkJlZm9yZSwgYWRkb25BZnRlciwgc3R5bGUsIHNpemUsIGNsYXNzTmFtZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgLy8gTm90IHdyYXAgd2hlbiB0aGVyZSBpcyBub3QgYWRkb25zXG4gICAgICAgIGlmICghYWRkb25CZWZvcmUgJiYgIWFkZG9uQWZ0ZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBsYWJlbGVkRWxlbWVudDtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCB3cmFwcGVyQ2xhc3NOYW1lID0gYCR7cHJlZml4Q2xzfS1ncm91cGA7XG4gICAgICAgIGNvbnN0IGFkZG9uQ2xhc3NOYW1lID0gYCR7d3JhcHBlckNsYXNzTmFtZX0tYWRkb25gO1xuICAgICAgICBjb25zdCBhZGRvbkJlZm9yZU5vZGUgPSBhZGRvbkJlZm9yZSA/ICg8c3BhbiBjbGFzc05hbWU9e2FkZG9uQ2xhc3NOYW1lfT57YWRkb25CZWZvcmV9PC9zcGFuPikgOiBudWxsO1xuICAgICAgICBjb25zdCBhZGRvbkFmdGVyTm9kZSA9IGFkZG9uQWZ0ZXIgPyA8c3BhbiBjbGFzc05hbWU9e2FkZG9uQ2xhc3NOYW1lfT57YWRkb25BZnRlcn08L3NwYW4+IDogbnVsbDtcbiAgICAgICAgY29uc3QgbWVyZ2VkV3JhcHBlckNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS13cmFwcGVyYCwge1xuICAgICAgICAgICAgW3dyYXBwZXJDbGFzc05hbWVdOiBhZGRvbkJlZm9yZSB8fCBhZGRvbkFmdGVyLFxuICAgICAgICB9KTtcbiAgICAgICAgY29uc3QgbWVyZ2VkR3JvdXBDbGFzc05hbWUgPSBjbGFzc05hbWVzKGNsYXNzTmFtZSwgYCR7cHJlZml4Q2xzfS1ncm91cC13cmFwcGVyYCwge1xuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZ3JvdXAtd3JhcHBlci1zbWBdOiBzaXplID09PSAnc21hbGwnLFxuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tZ3JvdXAtd3JhcHBlci1sZ2BdOiBzaXplID09PSAnbGFyZ2UnLFxuICAgICAgICB9KTtcbiAgICAgICAgLy8gTmVlZCBhbm90aGVyIHdyYXBwZXIgZm9yIGNoYW5naW5nIGRpc3BsYXk6dGFibGUgdG8gZGlzcGxheTppbmxpbmUtYmxvY2tcbiAgICAgICAgLy8gYW5kIHB1dCBzdHlsZSBwcm9wIGluIHdyYXBwZXJcbiAgICAgICAgcmV0dXJuICg8c3BhbiBjbGFzc05hbWU9e21lcmdlZEdyb3VwQ2xhc3NOYW1lfSBzdHlsZT17c3R5bGV9PlxuICAgICAgICA8c3BhbiBjbGFzc05hbWU9e21lcmdlZFdyYXBwZXJDbGFzc05hbWV9PlxuICAgICAgICAgIHthZGRvbkJlZm9yZU5vZGV9XG4gICAgICAgICAge1JlYWN0LmNsb25lRWxlbWVudChsYWJlbGVkRWxlbWVudCwgeyBzdHlsZTogbnVsbCB9KX1cbiAgICAgICAgICB7YWRkb25BZnRlck5vZGV9XG4gICAgICAgIDwvc3Bhbj5cbiAgICAgIDwvc3Bhbj4pO1xuICAgIH1cbiAgICByZW5kZXJUZXh0QXJlYVdpdGhDbGVhckljb24ocHJlZml4Q2xzLCBlbGVtZW50KSB7XG4gICAgICAgIGNvbnN0IHsgdmFsdWUsIGFsbG93Q2xlYXIsIGNsYXNzTmFtZSwgc3R5bGUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmICghYWxsb3dDbGVhcikge1xuICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChlbGVtZW50LCB7XG4gICAgICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBhZmZpeFdyYXBwZXJDbHMgPSBjbGFzc05hbWVzKGNsYXNzTmFtZSwgYCR7cHJlZml4Q2xzfS1hZmZpeC13cmFwcGVyYCwgYCR7cHJlZml4Q2xzfS1hZmZpeC13cmFwcGVyLXRleHRhcmVhLXdpdGgtY2xlYXItYnRuYCk7XG4gICAgICAgIHJldHVybiAoPHNwYW4gY2xhc3NOYW1lPXthZmZpeFdyYXBwZXJDbHN9IHN0eWxlPXtzdHlsZX0+XG4gICAgICAgIHtSZWFjdC5jbG9uZUVsZW1lbnQoZWxlbWVudCwge1xuICAgICAgICAgICAgc3R5bGU6IG51bGwsXG4gICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgfSl9XG4gICAgICAgIHt0aGlzLnJlbmRlckNsZWFySWNvbihwcmVmaXhDbHMpfVxuICAgICAgPC9zcGFuPik7XG4gICAgfVxuICAgIHJlbmRlckNsZWFyYWJsZUxhYmVsZWRJbnB1dCgpIHtcbiAgICAgICAgY29uc3QgeyBwcmVmaXhDbHMsIGlucHV0VHlwZSwgZWxlbWVudCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKGlucHV0VHlwZSA9PT0gQ2xlYXJhYmxlSW5wdXRUeXBlWzBdKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJUZXh0QXJlYVdpdGhDbGVhckljb24ocHJlZml4Q2xzLCBlbGVtZW50KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5yZW5kZXJJbnB1dFdpdGhMYWJlbChwcmVmaXhDbHMsIHRoaXMucmVuZGVyTGFiZWxlZEljb24ocHJlZml4Q2xzLCBlbGVtZW50KSk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVuZGVyQ2xlYXJhYmxlTGFiZWxlZElucHV0KCk7XG4gICAgfVxufVxucG9seWZpbGwoQ2xlYXJhYmxlTGFiZWxlZElucHV0KTtcbmV4cG9ydCBkZWZhdWx0IENsZWFyYWJsZUxhYmVsZWRJbnB1dDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUhBO0FBT0E7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBSUE7OztBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFGQTtBQU1BOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7OztBQWpHQTtBQUNBO0FBa0dBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/ClearableLabeledInput.js
