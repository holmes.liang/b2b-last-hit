__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);




var Rules =
/*#__PURE__*/
function () {
  function Rules() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Rules);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Rules, [{
    key: "isXMultiply",
    value: function isXMultiply(num, multiple) {
      return num > 0 && num % multiple === 0;
    }
  }, {
    key: "isEmail",
    value: function isEmail() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "Email is invalid";
      return {
        pattern: /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/,
        message: message
      };
    }
  }, {
    key: "isMobileNumber",
    value: function isMobileNumber() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "Mobile No. is invalid";
      return {
        pattern: /^\d+$/,
        message: message
      };
    }
  }, {
    key: "passwordValidate",
    value: function passwordValidate() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "Password is invalid";
      var rC = {
        lW: "[a-z]",
        //小写字母
        uW: "[A-Z]",
        //大写字母
        nW: "[0-9]",
        //汉字
        sW: "[!@#$%^&*_-]" //特殊字符

      };
      return {
        pattern: /^((lw*uw*nw) || (lw*uw*sw) || (uw*nw*sw)||(lw*nw*sw)){8,}$/,
        message: message
      };
    }
  }, {
    key: "password",
    value: function password() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "Password must contain at least 8 chars with number, uppercase and lowercase";
      return {
        pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
        message: message
      };
    }
  }, {
    key: "isValidChassisNo",
    value: function isValidChassisNo(value) {
      return new RegExp(/^[a-zA-Z0-9]{6,17}$/).test(value);
    }
  }, {
    key: "isSpgVehicleNo",
    value: function isSpgVehicleNo(value) {
      value = value.toUpperCase();
      return new RegExp(/^[S][\S]{0,9}$/).test(value);
    }
  }, {
    key: "isLetterDigitalAndUnderscore",
    value: function isLetterDigitalAndUnderscore(message) {
      return {
        pattern: /^[\w-]+$/,
        message: message
      };
    }
  }, {
    key: "isValidThaiIdNo",
    value: function isValidThaiIdNo(thaiIdNo) {
      var isValidThaiIdNo = false;

      if (thaiIdNo != null && thaiIdNo != undefined && thaiIdNo != "") {
        if (thaiIdNo.length == 13) {
          var mainDigits = [13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2];
          var sum = 0;

          for (var i = 0; i < 12; i++) {
            var n = Number(thaiIdNo.substring(i, i + 1));
            sum += n * mainDigits[i];
          }

          var finalResult = 11 - sum % 11;

          if (finalResult == 10) {
            finalResult = 0;
          } else if (finalResult == 11) {
            finalResult = 1;
          }

          var lastNo = Number(thaiIdNo.substring(12));

          if (lastNo == finalResult) {
            isValidThaiIdNo = true;
          }
        }
      }

      return isValidThaiIdNo;
    }
  }, {
    key: "isNricValid",
    value: function isNricValid(theNric) {
      var nric = [];
      nric.multiples = [2, 7, 6, 5, 4, 3, 2];

      if (!theNric || theNric == "") {
        return false;
      }

      if (theNric.length != 9) {
        return false;
      }

      var total = 0,
          count = 0,
          numericNric;
      var first = theNric[0],
          last = theNric[theNric.length - 1];

      if (first != "S" && first != "T") {
        return false;
      }

      numericNric = theNric.substr(1, theNric.length - 2);

      if (isNaN(numericNric)) {
        return false;
      }

      while (numericNric != 0) {
        total += numericNric % 10 * nric.multiples[nric.multiples.length - (1 + count++)];
        numericNric /= 10;
        numericNric = Math.floor(numericNric);
      }

      var outputs;

      if (first == "S") {
        outputs = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
      } else {
        outputs = ["G", "F", "E", "D", "C", "B", "A", "J", "Z", "I", "H"];
      }

      return last == outputs[total % 11];
    }
  }, {
    key: "isFinValid",
    value: function isFinValid(fin) {
      var nric = [];
      nric.multiples = [2, 7, 6, 5, 4, 3, 2];

      if (!fin || fin == "") {
        return false;
      }

      if (fin.length != 9) {
        return false;
      }

      var total = 0,
          count = 0,
          numericNric;
      var first = fin[0],
          last = fin[fin.length - 1];

      if (first != "F" && first != "G") {
        return false;
      }

      numericNric = fin.substr(1, fin.length - 2);

      if (isNaN(numericNric)) {
        return false;
      }

      while (numericNric != 0) {
        total += numericNric % 10 * nric.multiples[nric.multiples.length - (1 + count++)];
        numericNric /= 10;
        numericNric = Math.floor(numericNric);
      }

      var outputs;

      if (first == "F") {
        outputs = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];
      } else {
        outputs = ["R", "Q", "P", "N", "M", "L", "K", "X", "W", "U", "T"];
      }

      return last == outputs[total % 11];
    }
  }, {
    key: "isValidMobileNo",
    value: function isValidMobileNo(mobile) {
      var nationCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "+66";
      if (!mobile) return true;
      var isValidMobileNo = false;

      switch (nationCode) {
        case "+66":
          if (mobile.startsWith("0")) {
            isValidMobileNo = mobile.length === 10 && /^[0-9]+$/.test(mobile);
          } else {
            isValidMobileNo = mobile.length === 9 && /^[0-9]+$/.test(mobile);
          }

          break;

        case "+65":
          isValidMobileNo = (mobile.startsWith("8") || mobile.startsWith("9")) && mobile.length === 8 && /^[0-9]+$/.test(mobile);
          break;

        case "+61":
          isValidMobileNo = mobile.length === 9 && /^[0-9]+$/.test(mobile);
          break;

        case "+86":
          isValidMobileNo = mobile.length === 11 && /^[0-9]+$/.test(mobile);
          break;

        case "+95":
          isValidMobileNo = (mobile.length === 9 || mobile.length === 10) && /^09[0-9]+$/.test(mobile);
          break;

        default:
          isValidMobileNo = true;
          break;
      }

      return isValidMobileNo;
    }
  }, {
    key: "isValidTelNo",
    value: function isValidTelNo(mobile) {
      var nationCode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "+66";
      if (!mobile) return true;
      var isValidMobileNo = false;

      switch (nationCode) {
        case "+65":
          isValidMobileNo = (mobile.startsWith("3") || mobile.startsWith("6")) && /^[0-9]+$/.test(mobile);
          break;

        default:
          isValidMobileNo = true;
          break;
      }

      return isValidMobileNo;
    }
  }, {
    key: "isValidaTaxNo",
    value: function isValidaTaxNo(value) {
      return value && !isNaN(value) && value.length == 13;
    }
  }, {
    key: "isValidVehicleNo1",
    value: function isValidVehicleNo1(value) {
      return new RegExp(/^[กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮ0-9\s+]{1,3}$/).test(value);
    }
  }, {
    key: "isPostalCode",
    value: function isPostalCode(value) {
      return /^([1-9][0-9]{5})$/.test(value);
    }
  }, {
    key: "isValidVehicleNo2",
    value: function isValidVehicleNo2(value) {
      return new RegExp(/^[กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮ0-9\s+]{1,4}$/).test(value);
    }
  }, {
    key: "isValidAccountNo",
    value: function isValidAccountNo(value) {
      return new RegExp(/^[\d]{6,16}$/).test(value);
    }
  }, {
    key: "isValidWebsite",
    value: function isValidWebsite(value) {
      return new RegExp(/^[5A-Za-z0-9-\_]+$/).test(value);
    }
  }, {
    key: "isFloatNumber",
    value: function isFloatNumber(value) {
      return new RegExp(/^(0|[1-9][0-9]*)(\.\d+)?$/).test(value);
    }
  }, {
    key: "isNum",
    value: function isNum(value) {
      var name = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.trim(value);

      var numbercode = /^(\-|\+)?\d+(\.\d+)?$/;
      return numbercode.test(name);
    }
  }, {
    key: "isNumFromZero",
    value: function isNumFromZero(value) {
      if (this.isNum(value)) {
        return value > 0;
      }

      return false;
    }
  }, {
    key: "isNumFromZeroAndAllowZero",
    value: function isNumFromZeroAndAllowZero(value) {
      if (this.isNum(value)) {
        return value >= 0;
      }

      return false;
    }
  }, {
    key: "isLen",
    value: function isLen(value, len) {
      return value.length > len;
    }
  }, {
    key: "isNumFrom0To100",
    value: function isNumFrom0To100(value) {
      if (this.isNum(value)) {
        return value > 0 && value <= 100;
      }

      return false;
    }
  }, {
    key: "isNumFrom0To100AlowZero",
    value: function isNumFrom0To100AlowZero(value) {
      if (this.isNum(value)) {
        return value >= 0 && value <= 100;
      }

      return false;
    }
  }, {
    key: "isNumberFromZero",
    value: function isNumberFromZero(value) {
      var Reg = /^[1-9]\d*$/;
      return Reg.test(value);
    }
  }]);

  return Rules;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Rules());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3J1bGVzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2NvbW1vbi9ydWxlcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5jbGFzcyBSdWxlcyB7XG4gIGlzWE11bHRpcGx5KG51bTogbnVtYmVyLCBtdWx0aXBsZTogbnVtYmVyKSB7XG4gICAgcmV0dXJuIG51bSA+IDAgJiYgbnVtICUgbXVsdGlwbGUgPT09IDA7XG4gIH1cblxuICBpc0VtYWlsKG1lc3NhZ2U6IHN0cmluZyA9IFwiRW1haWwgaXMgaW52YWxpZFwiKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHBhdHRlcm46IC9eW2EtejAtOV0rKFsuX1xcXFwtXSpbYS16MC05XSkqQChbYS16MC05XStbLWEtejAtOV0qW2EtejAtOV0rLil7MSw2M31bYS16MC05XSskLyxcbiAgICAgIG1lc3NhZ2UsXG4gICAgfTtcbiAgfVxuXG4gIGlzTW9iaWxlTnVtYmVyKG1lc3NhZ2U6IHN0cmluZyA9IFwiTW9iaWxlIE5vLiBpcyBpbnZhbGlkXCIpIHtcbiAgICByZXR1cm4ge1xuICAgICAgcGF0dGVybjogL15cXGQrJC8sXG4gICAgICBtZXNzYWdlLFxuICAgIH07XG4gIH1cblxuICBwYXNzd29yZFZhbGlkYXRlKG1lc3NhZ2U6IHN0cmluZyA9IFwiUGFzc3dvcmQgaXMgaW52YWxpZFwiKTogYW55IHtcbiAgICBsZXQgckMgPSB7XG4gICAgICBsVzogXCJbYS16XVwiLC8v5bCP5YaZ5a2X5q+NXG4gICAgICB1VzogXCJbQS1aXVwiLC8v5aSn5YaZ5a2X5q+NXG4gICAgICBuVzogXCJbMC05XVwiLC8v5rGJ5a2XXG4gICAgICBzVzogXCJbIUAjJCVeJipfLV1cIiwvL+eJueauiuWtl+esplxuICAgIH07XG4gICAgcmV0dXJuIHtcbiAgICAgIHBhdHRlcm46IC9eKChsdyp1dypudykgfHwgKGx3KnV3KnN3KSB8fCAodXcqbncqc3cpfHwobHcqbncqc3cpKXs4LH0kLyxcbiAgICAgIG1lc3NhZ2UsXG4gICAgfTtcbiAgfVxuXG4gIHBhc3N3b3JkKFxuICAgIG1lc3NhZ2U6IHN0cmluZyA9IFwiUGFzc3dvcmQgbXVzdCBjb250YWluIGF0IGxlYXN0IDggY2hhcnMgd2l0aCBudW1iZXIsIHVwcGVyY2FzZSBhbmQgbG93ZXJjYXNlXCIsXG4gICkge1xuICAgIHJldHVybiB7XG4gICAgICBwYXR0ZXJuOiAvXig/PS4qXFxkKSg/PS4qW2Etel0pKD89LipbQS1aXSlbMC05YS16QS1aXXs4LH0kLyxcbiAgICAgIG1lc3NhZ2UsXG4gICAgfTtcbiAgfVxuXG4gIGlzVmFsaWRDaGFzc2lzTm8odmFsdWU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBuZXcgUmVnRXhwKC9eW2EtekEtWjAtOV17NiwxN30kLykudGVzdCh2YWx1ZSk7XG4gIH1cblxuICBpc1NwZ1ZlaGljbGVObyh2YWx1ZTogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgdmFsdWUgPSB2YWx1ZS50b1VwcGVyQ2FzZSgpO1xuICAgIHJldHVybiBuZXcgUmVnRXhwKC9eW1NdW1xcU117MCw5fSQvKS50ZXN0KHZhbHVlKTtcbiAgfVxuXG4gIGlzTGV0dGVyRGlnaXRhbEFuZFVuZGVyc2NvcmUobWVzc2FnZTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHBhdHRlcm46IC9eW1xcdy1dKyQvLFxuICAgICAgbWVzc2FnZSxcbiAgICB9O1xuICB9XG5cbiAgaXNWYWxpZFRoYWlJZE5vKHRoYWlJZE5vOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICB2YXIgaXNWYWxpZFRoYWlJZE5vID0gZmFsc2U7XG4gICAgaWYgKHRoYWlJZE5vICE9IG51bGwgJiYgdGhhaUlkTm8gIT0gdW5kZWZpbmVkICYmIHRoYWlJZE5vICE9IFwiXCIpIHtcbiAgICAgIGlmICh0aGFpSWROby5sZW5ndGggPT0gMTMpIHtcbiAgICAgICAgdmFyIG1haW5EaWdpdHMgPSBbMTMsIDEyLCAxMSwgMTAsIDksIDgsIDcsIDYsIDUsIDQsIDMsIDJdO1xuICAgICAgICB2YXIgc3VtID0gMDtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCAxMjsgaSsrKSB7XG4gICAgICAgICAgdmFyIG4gPSBOdW1iZXIodGhhaUlkTm8uc3Vic3RyaW5nKGksIGkgKyAxKSk7XG4gICAgICAgICAgc3VtICs9IG4gKiBtYWluRGlnaXRzW2ldO1xuICAgICAgICB9XG4gICAgICAgIHZhciBmaW5hbFJlc3VsdCA9IDExIC0gKHN1bSAlIDExKTtcbiAgICAgICAgaWYgKGZpbmFsUmVzdWx0ID09IDEwKSB7XG4gICAgICAgICAgZmluYWxSZXN1bHQgPSAwO1xuICAgICAgICB9IGVsc2UgaWYgKGZpbmFsUmVzdWx0ID09IDExKSB7XG4gICAgICAgICAgZmluYWxSZXN1bHQgPSAxO1xuICAgICAgICB9XG4gICAgICAgIHZhciBsYXN0Tm8gPSBOdW1iZXIodGhhaUlkTm8uc3Vic3RyaW5nKDEyKSk7XG4gICAgICAgIGlmIChsYXN0Tm8gPT0gZmluYWxSZXN1bHQpIHtcbiAgICAgICAgICBpc1ZhbGlkVGhhaUlkTm8gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBpc1ZhbGlkVGhhaUlkTm87XG4gIH1cblxuICBpc05yaWNWYWxpZCh0aGVOcmljOiBhbnkpIHtcbiAgICBsZXQgbnJpYzogYW55ID0gW107XG4gICAgbnJpYy5tdWx0aXBsZXMgPSBbMiwgNywgNiwgNSwgNCwgMywgMl07XG4gICAgaWYgKCF0aGVOcmljIHx8IHRoZU5yaWMgPT0gXCJcIikge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBpZiAodGhlTnJpYy5sZW5ndGggIT0gOSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBsZXQgdG90YWwgPSAwXG4gICAgICAsIGNvdW50ID0gMFxuICAgICAgLCBudW1lcmljTnJpYztcbiAgICBsZXQgZmlyc3QgPSB0aGVOcmljWzBdXG4gICAgICAsIGxhc3QgPSB0aGVOcmljW3RoZU5yaWMubGVuZ3RoIC0gMV07XG5cbiAgICBpZiAoZmlyc3QgIT0gXCJTXCIgJiYgZmlyc3QgIT0gXCJUXCIpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgbnVtZXJpY05yaWMgPSB0aGVOcmljLnN1YnN0cigxLCB0aGVOcmljLmxlbmd0aCAtIDIpO1xuICAgIGlmIChpc05hTihudW1lcmljTnJpYykpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgd2hpbGUgKG51bWVyaWNOcmljICE9IDApIHtcbiAgICAgIHRvdGFsICs9IChudW1lcmljTnJpYyAlIDEwKSAqIG5yaWMubXVsdGlwbGVzW25yaWMubXVsdGlwbGVzLmxlbmd0aCAtICgxICsgY291bnQrKyldO1xuXG4gICAgICBudW1lcmljTnJpYyAvPSAxMDtcbiAgICAgIG51bWVyaWNOcmljID0gTWF0aC5mbG9vcihudW1lcmljTnJpYyk7XG4gICAgfVxuICAgIGxldCBvdXRwdXRzO1xuICAgIGlmIChmaXJzdCA9PSBcIlNcIikge1xuICAgICAgb3V0cHV0cyA9IFtcIkpcIiwgXCJaXCIsIFwiSVwiLCBcIkhcIiwgXCJHXCIsIFwiRlwiLCBcIkVcIiwgXCJEXCIsIFwiQ1wiLCBcIkJcIiwgXCJBXCJdO1xuICAgIH0gZWxzZSB7XG4gICAgICBvdXRwdXRzID0gW1wiR1wiLCBcIkZcIiwgXCJFXCIsIFwiRFwiLCBcIkNcIiwgXCJCXCIsIFwiQVwiLCBcIkpcIiwgXCJaXCIsIFwiSVwiLCBcIkhcIl07XG4gICAgfVxuICAgIHJldHVybiBsYXN0ID09IG91dHB1dHNbdG90YWwgJSAxMV07XG4gIH1cblxuICBpc0ZpblZhbGlkKGZpbjogYW55KSB7XG4gICAgbGV0IG5yaWM6IGFueSA9IFtdO1xuICAgIG5yaWMubXVsdGlwbGVzID0gWzIsIDcsIDYsIDUsIDQsIDMsIDJdO1xuICAgIGlmICghZmluIHx8IGZpbiA9PSBcIlwiKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIGlmIChmaW4ubGVuZ3RoICE9IDkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgbGV0IHRvdGFsID0gMFxuICAgICAgLCBjb3VudCA9IDBcbiAgICAgICwgbnVtZXJpY05yaWM7XG4gICAgbGV0IGZpcnN0ID0gZmluWzBdXG4gICAgICAsIGxhc3QgPSBmaW5bZmluLmxlbmd0aCAtIDFdO1xuXG4gICAgaWYgKGZpcnN0ICE9IFwiRlwiICYmIGZpcnN0ICE9IFwiR1wiKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIG51bWVyaWNOcmljID0gZmluLnN1YnN0cigxLCBmaW4ubGVuZ3RoIC0gMik7XG4gICAgaWYgKGlzTmFOKG51bWVyaWNOcmljKSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICB3aGlsZSAobnVtZXJpY05yaWMgIT0gMCkge1xuICAgICAgdG90YWwgKz0gKG51bWVyaWNOcmljICUgMTApICogbnJpYy5tdWx0aXBsZXNbbnJpYy5tdWx0aXBsZXMubGVuZ3RoIC0gKDEgKyBjb3VudCsrKV07XG5cbiAgICAgIG51bWVyaWNOcmljIC89IDEwO1xuICAgICAgbnVtZXJpY05yaWMgPSBNYXRoLmZsb29yKG51bWVyaWNOcmljKTtcbiAgICB9XG4gICAgbGV0IG91dHB1dHM7XG4gICAgaWYgKGZpcnN0ID09IFwiRlwiKSB7XG4gICAgICBvdXRwdXRzID0gW1wiWFwiLCBcIldcIiwgXCJVXCIsIFwiVFwiLCBcIlJcIiwgXCJRXCIsIFwiUFwiLCBcIk5cIiwgXCJNXCIsIFwiTFwiLCBcIktcIl07XG4gICAgfSBlbHNlIHtcbiAgICAgIG91dHB1dHMgPSBbXCJSXCIsIFwiUVwiLCBcIlBcIiwgXCJOXCIsIFwiTVwiLCBcIkxcIiwgXCJLXCIsIFwiWFwiLCBcIldcIiwgXCJVXCIsIFwiVFwiXTtcbiAgICB9XG4gICAgcmV0dXJuIGxhc3QgPT0gb3V0cHV0c1t0b3RhbCAlIDExXTtcbiAgfVxuXG4gIGlzVmFsaWRNb2JpbGVObyhtb2JpbGU6IHN0cmluZywgbmF0aW9uQ29kZTogc3RyaW5nID0gXCIrNjZcIikge1xuICAgIGlmICghbW9iaWxlKSByZXR1cm4gdHJ1ZTtcbiAgICB2YXIgaXNWYWxpZE1vYmlsZU5vID0gZmFsc2U7XG4gICAgc3dpdGNoIChuYXRpb25Db2RlKSB7XG4gICAgICBjYXNlIFwiKzY2XCI6XG4gICAgICAgIGlmIChtb2JpbGUuc3RhcnRzV2l0aChcIjBcIikpIHtcbiAgICAgICAgICBpc1ZhbGlkTW9iaWxlTm8gPSBtb2JpbGUubGVuZ3RoID09PSAxMCAmJiAvXlswLTldKyQvLnRlc3QobW9iaWxlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpc1ZhbGlkTW9iaWxlTm8gPSBtb2JpbGUubGVuZ3RoID09PSA5ICYmIC9eWzAtOV0rJC8udGVzdChtb2JpbGUpO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcIis2NVwiOlxuICAgICAgICBpc1ZhbGlkTW9iaWxlTm8gPVxuICAgICAgICAgIChtb2JpbGUuc3RhcnRzV2l0aChcIjhcIikgfHwgbW9iaWxlLnN0YXJ0c1dpdGgoXCI5XCIpKSAmJlxuICAgICAgICAgIG1vYmlsZS5sZW5ndGggPT09IDggJiZcbiAgICAgICAgICAvXlswLTldKyQvLnRlc3QobW9iaWxlKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwiKzYxXCI6XG4gICAgICAgIGlzVmFsaWRNb2JpbGVObyA9IG1vYmlsZS5sZW5ndGggPT09IDkgJiYgL15bMC05XSskLy50ZXN0KG1vYmlsZSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSBcIis4NlwiOlxuICAgICAgICBpc1ZhbGlkTW9iaWxlTm8gPSBtb2JpbGUubGVuZ3RoID09PSAxMSAmJiAvXlswLTldKyQvLnRlc3QobW9iaWxlKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwiKzk1XCI6XG4gICAgICAgIGlzVmFsaWRNb2JpbGVObyA9IChtb2JpbGUubGVuZ3RoID09PSA5IHx8IG1vYmlsZS5sZW5ndGggPT09IDEwKSAmJiAvXjA5WzAtOV0rJC8udGVzdChtb2JpbGUpO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlzVmFsaWRNb2JpbGVObyA9IHRydWU7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gaXNWYWxpZE1vYmlsZU5vO1xuICB9XG5cbiAgaXNWYWxpZFRlbE5vKG1vYmlsZTogc3RyaW5nLCBuYXRpb25Db2RlOiBzdHJpbmcgPSBcIis2NlwiKSB7XG4gICAgaWYgKCFtb2JpbGUpIHJldHVybiB0cnVlO1xuICAgIHZhciBpc1ZhbGlkTW9iaWxlTm8gPSBmYWxzZTtcbiAgICBzd2l0Y2ggKG5hdGlvbkNvZGUpIHtcbiAgICAgIGNhc2UgXCIrNjVcIjpcbiAgICAgICAgaXNWYWxpZE1vYmlsZU5vID1cbiAgICAgICAgICAobW9iaWxlLnN0YXJ0c1dpdGgoXCIzXCIpIHx8IG1vYmlsZS5zdGFydHNXaXRoKFwiNlwiKSkgJiZcbiAgICAgICAgICAvXlswLTldKyQvLnRlc3QobW9iaWxlKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBpc1ZhbGlkTW9iaWxlTm8gPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgcmV0dXJuIGlzVmFsaWRNb2JpbGVObztcbiAgfVxuXG4gIGlzVmFsaWRhVGF4Tm8odmFsdWU6IGFueSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB2YWx1ZSAmJiAhaXNOYU4odmFsdWUpICYmIHZhbHVlLmxlbmd0aCA9PSAxMztcbiAgfVxuXG4gIGlzVmFsaWRWZWhpY2xlTm8xKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcbiAgICByZXR1cm4gbmV3IFJlZ0V4cChcbiAgICAgIC9eW+C4geC4guC4g+C4hOC4heC4huC4h+C4iOC4ieC4iuC4i+C4jOC4jeC4juC4j+C4kOC4keC4kuC4k+C4lOC4leC4luC4l+C4mOC4meC4muC4m+C4nOC4neC4nuC4n+C4oOC4oeC4ouC4o+C4peC4p+C4qOC4qeC4quC4q+C4rOC4reC4rjAtOVxccytdezEsM30kLyxcbiAgICApLnRlc3QodmFsdWUpO1xuICB9XG5cbiAgaXNQb3N0YWxDb2RlKHZhbHVlOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICByZXR1cm4gL14oWzEtOV1bMC05XXs1fSkkLy50ZXN0KHZhbHVlKTtcbiAgfTtcblxuICBpc1ZhbGlkVmVoaWNsZU5vMih2YWx1ZTogYW55KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIG5ldyBSZWdFeHAoXG4gICAgICAvXlvguIHguILguIPguITguIXguIbguIfguIjguInguIrguIvguIzguI3guI7guI/guJDguJHguJLguJPguJTguJXguJbguJfguJjguJnguJrguJvguJzguJ3guJ7guJ/guKDguKHguKLguKPguKXguKfguKjguKnguKrguKvguKzguK3guK4wLTlcXHMrXXsxLDR9JC8sXG4gICAgKS50ZXN0KHZhbHVlKTtcbiAgfVxuXG4gIGlzVmFsaWRBY2NvdW50Tm8odmFsdWU6IGFueSkge1xuICAgIHJldHVybiBuZXcgUmVnRXhwKC9eW1xcZF17NiwxNn0kLykudGVzdCh2YWx1ZSk7XG4gIH1cblxuICBpc1ZhbGlkV2Vic2l0ZSh2YWx1ZTogYW55KSB7XG4gICAgcmV0dXJuIG5ldyBSZWdFeHAoL15bNUEtWmEtejAtOS1cXF9dKyQvKS50ZXN0KHZhbHVlKTtcbiAgfVxuXG4gIGlzRmxvYXROdW1iZXIodmFsdWU6IGFueSk6IGJvb2xlYW4ge1xuICAgIHJldHVybiBuZXcgUmVnRXhwKC9eKDB8WzEtOV1bMC05XSopKFxcLlxcZCspPyQvKS50ZXN0KHZhbHVlKTtcbiAgfVxuXG5cbiAgaXNOdW0odmFsdWU6IGFueSkge1xuICAgIHZhciBuYW1lID0gXy50cmltKHZhbHVlKTtcbiAgICB2YXIgbnVtYmVyY29kZSA9IC9eKFxcLXxcXCspP1xcZCsoXFwuXFxkKyk/JC87XG4gICAgcmV0dXJuIG51bWJlcmNvZGUudGVzdChuYW1lKTtcbiAgfVxuXG4gIGlzTnVtRnJvbVplcm8odmFsdWU6IGFueSkge1xuICAgIGlmICh0aGlzLmlzTnVtKHZhbHVlKSkge1xuICAgICAgcmV0dXJuIHZhbHVlID4gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgaXNOdW1Gcm9tWmVyb0FuZEFsbG93WmVybyh2YWx1ZTogYW55KSB7XG4gICAgaWYgKHRoaXMuaXNOdW0odmFsdWUpKSB7XG4gICAgICByZXR1cm4gdmFsdWUgPj0gMDtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgaXNMZW4odmFsdWU6IGFueSwgbGVuOiBudW1iZXIpIHtcbiAgICByZXR1cm4gdmFsdWUubGVuZ3RoID4gbGVuO1xuICB9XG5cbiAgaXNOdW1Gcm9tMFRvMTAwKHZhbHVlOiBhbnkpIHtcbiAgICBpZiAodGhpcy5pc051bSh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiB2YWx1ZSA+IDAgJiYgdmFsdWUgPD0gMTAwO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBpc051bUZyb20wVG8xMDBBbG93WmVybyh2YWx1ZTogYW55KSB7XG4gICAgaWYgKHRoaXMuaXNOdW0odmFsdWUpKSB7XG4gICAgICByZXR1cm4gdmFsdWUgPj0gMCAmJiB2YWx1ZSA8PSAxMDA7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGlzTnVtYmVyRnJvbVplcm8odmFsdWU6IGFueSkge1xuICAgIGNvbnN0IFJlZyA9IC9eWzEtOV1cXGQqJC87XG4gICAgcmV0dXJuIFJlZy50ZXN0KHZhbHVlKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgUnVsZXMoKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFMQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBSUE7OztBQUlBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQXpCQTtBQUNBO0FBMEJBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFHQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/rules.tsx
