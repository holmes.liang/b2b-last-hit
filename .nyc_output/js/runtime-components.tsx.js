__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerIntoRuntime", function() { return registerIntoRuntime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregisterFromRuntime", function() { return unregisterFromRuntime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAllRuntimeComponents", function() { return getAllRuntimeComponents; });
var runtimeComponentsRegistry = [];
var registerIntoRuntime = function registerIntoRuntime(widget) {
  if (!runtimeComponentsRegistry.includes(widget)) {
    runtimeComponentsRegistry.push(widget);
  }
};
var unregisterFromRuntime = function unregisterFromRuntime(widget) {
  var index = runtimeComponentsRegistry.indexOf(widget);

  if (index !== -1) {
    runtimeComponentsRegistry.splice(index, 1);
  }
};
var getAllRuntimeComponents = function getAllRuntimeComponents() {
  return Array.from(runtimeComponentsRegistry);
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L3J1bnRpbWUtY29tcG9uZW50cy50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvcnVudGltZS1jb21wb25lbnRzLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgV2lkZ2V0IGZyb20gXCIuL3dpZGdldFwiO1xuXG5jb25zdCBydW50aW1lQ29tcG9uZW50c1JlZ2lzdHJ5ID0gW10gYXMgV2lkZ2V0PGFueSwgYW55LCBhbnk+W107XG5cbmV4cG9ydCBjb25zdCByZWdpc3RlckludG9SdW50aW1lID0gKHdpZGdldDogV2lkZ2V0PGFueSwgYW55LCBhbnk+KTogdm9pZCA9PiB7XG4gIGlmICghcnVudGltZUNvbXBvbmVudHNSZWdpc3RyeS5pbmNsdWRlcyh3aWRnZXQpKSB7XG4gICAgcnVudGltZUNvbXBvbmVudHNSZWdpc3RyeS5wdXNoKHdpZGdldCk7XG4gIH1cbn07XG5cbmV4cG9ydCBjb25zdCB1bnJlZ2lzdGVyRnJvbVJ1bnRpbWUgPSAod2lkZ2V0OiBXaWRnZXQ8YW55LCBhbnksIGFueT4pOiB2b2lkID0+IHtcbiAgY29uc3QgaW5kZXggPSBydW50aW1lQ29tcG9uZW50c1JlZ2lzdHJ5LmluZGV4T2Yod2lkZ2V0KTtcbiAgaWYgKGluZGV4ICE9PSAtMSkge1xuICAgIHJ1bnRpbWVDb21wb25lbnRzUmVnaXN0cnkuc3BsaWNlKGluZGV4LCAxKTtcbiAgfVxufTtcblxuZXhwb3J0IGNvbnN0IGdldEFsbFJ1bnRpbWVDb21wb25lbnRzID0gKCk6IFdpZGdldDxhbnksIGFueSwgYW55PltdID0+IHtcbiAgcmV0dXJuIEFycmF5LmZyb20ocnVudGltZUNvbXBvbmVudHNSZWdpc3RyeSk7XG59O1xuXG4iXSwibWFwcGluZ3MiOiJBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/component/runtime-components.tsx
