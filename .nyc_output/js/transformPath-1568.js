var PathProxy = __webpack_require__(/*! ../core/PathProxy */ "./node_modules/zrender/lib/core/PathProxy.js");

var _vector = __webpack_require__(/*! ../core/vector */ "./node_modules/zrender/lib/core/vector.js");

var v2ApplyTransform = _vector.applyTransform;
var CMD = PathProxy.CMD;
var points = [[], [], []];
var mathSqrt = Math.sqrt;
var mathAtan2 = Math.atan2;

function _default(path, m) {
  var data = path.data;
  var cmd;
  var nPoint;
  var i;
  var j;
  var k;
  var p;
  var M = CMD.M;
  var C = CMD.C;
  var L = CMD.L;
  var R = CMD.R;
  var A = CMD.A;
  var Q = CMD.Q;

  for (i = 0, j = 0; i < data.length;) {
    cmd = data[i++];
    j = i;
    nPoint = 0;

    switch (cmd) {
      case M:
        nPoint = 1;
        break;

      case L:
        nPoint = 1;
        break;

      case C:
        nPoint = 3;
        break;

      case Q:
        nPoint = 2;
        break;

      case A:
        var x = m[4];
        var y = m[5];
        var sx = mathSqrt(m[0] * m[0] + m[1] * m[1]);
        var sy = mathSqrt(m[2] * m[2] + m[3] * m[3]);
        var angle = mathAtan2(-m[1] / sy, m[0] / sx); // cx

        data[i] *= sx;
        data[i++] += x; // cy

        data[i] *= sy;
        data[i++] += y; // Scale rx and ry
        // FIXME Assume psi is 0 here

        data[i++] *= sx;
        data[i++] *= sy; // Start angle

        data[i++] += angle; // end angle

        data[i++] += angle; // FIXME psi

        i += 2;
        j = i;
        break;

      case R:
        // x0, y0
        p[0] = data[i++];
        p[1] = data[i++];
        v2ApplyTransform(p, p, m);
        data[j++] = p[0];
        data[j++] = p[1]; // x1, y1

        p[0] += data[i++];
        p[1] += data[i++];
        v2ApplyTransform(p, p, m);
        data[j++] = p[0];
        data[j++] = p[1];
    }

    for (k = 0; k < nPoint; k++) {
      var p = points[k];
      p[0] = data[i++];
      p[1] = data[i++];
      v2ApplyTransform(p, p, m); // Write back

      data[j++] = p[0];
      data[j++] = p[1];
    }
  }
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC90cmFuc2Zvcm1QYXRoLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC90cmFuc2Zvcm1QYXRoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoUHJveHkgPSByZXF1aXJlKFwiLi4vY29yZS9QYXRoUHJveHlcIik7XG5cbnZhciBfdmVjdG9yID0gcmVxdWlyZShcIi4uL2NvcmUvdmVjdG9yXCIpO1xuXG52YXIgdjJBcHBseVRyYW5zZm9ybSA9IF92ZWN0b3IuYXBwbHlUcmFuc2Zvcm07XG52YXIgQ01EID0gUGF0aFByb3h5LkNNRDtcbnZhciBwb2ludHMgPSBbW10sIFtdLCBbXV07XG52YXIgbWF0aFNxcnQgPSBNYXRoLnNxcnQ7XG52YXIgbWF0aEF0YW4yID0gTWF0aC5hdGFuMjtcblxuZnVuY3Rpb24gX2RlZmF1bHQocGF0aCwgbSkge1xuICB2YXIgZGF0YSA9IHBhdGguZGF0YTtcbiAgdmFyIGNtZDtcbiAgdmFyIG5Qb2ludDtcbiAgdmFyIGk7XG4gIHZhciBqO1xuICB2YXIgaztcbiAgdmFyIHA7XG4gIHZhciBNID0gQ01ELk07XG4gIHZhciBDID0gQ01ELkM7XG4gIHZhciBMID0gQ01ELkw7XG4gIHZhciBSID0gQ01ELlI7XG4gIHZhciBBID0gQ01ELkE7XG4gIHZhciBRID0gQ01ELlE7XG5cbiAgZm9yIChpID0gMCwgaiA9IDA7IGkgPCBkYXRhLmxlbmd0aDspIHtcbiAgICBjbWQgPSBkYXRhW2krK107XG4gICAgaiA9IGk7XG4gICAgblBvaW50ID0gMDtcblxuICAgIHN3aXRjaCAoY21kKSB7XG4gICAgICBjYXNlIE06XG4gICAgICAgIG5Qb2ludCA9IDE7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIEw6XG4gICAgICAgIG5Qb2ludCA9IDE7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIEM6XG4gICAgICAgIG5Qb2ludCA9IDM7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIFE6XG4gICAgICAgIG5Qb2ludCA9IDI7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIEE6XG4gICAgICAgIHZhciB4ID0gbVs0XTtcbiAgICAgICAgdmFyIHkgPSBtWzVdO1xuICAgICAgICB2YXIgc3ggPSBtYXRoU3FydChtWzBdICogbVswXSArIG1bMV0gKiBtWzFdKTtcbiAgICAgICAgdmFyIHN5ID0gbWF0aFNxcnQobVsyXSAqIG1bMl0gKyBtWzNdICogbVszXSk7XG4gICAgICAgIHZhciBhbmdsZSA9IG1hdGhBdGFuMigtbVsxXSAvIHN5LCBtWzBdIC8gc3gpOyAvLyBjeFxuXG4gICAgICAgIGRhdGFbaV0gKj0gc3g7XG4gICAgICAgIGRhdGFbaSsrXSArPSB4OyAvLyBjeVxuXG4gICAgICAgIGRhdGFbaV0gKj0gc3k7XG4gICAgICAgIGRhdGFbaSsrXSArPSB5OyAvLyBTY2FsZSByeCBhbmQgcnlcbiAgICAgICAgLy8gRklYTUUgQXNzdW1lIHBzaSBpcyAwIGhlcmVcblxuICAgICAgICBkYXRhW2krK10gKj0gc3g7XG4gICAgICAgIGRhdGFbaSsrXSAqPSBzeTsgLy8gU3RhcnQgYW5nbGVcblxuICAgICAgICBkYXRhW2krK10gKz0gYW5nbGU7IC8vIGVuZCBhbmdsZVxuXG4gICAgICAgIGRhdGFbaSsrXSArPSBhbmdsZTsgLy8gRklYTUUgcHNpXG5cbiAgICAgICAgaSArPSAyO1xuICAgICAgICBqID0gaTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgUjpcbiAgICAgICAgLy8geDAsIHkwXG4gICAgICAgIHBbMF0gPSBkYXRhW2krK107XG4gICAgICAgIHBbMV0gPSBkYXRhW2krK107XG4gICAgICAgIHYyQXBwbHlUcmFuc2Zvcm0ocCwgcCwgbSk7XG4gICAgICAgIGRhdGFbaisrXSA9IHBbMF07XG4gICAgICAgIGRhdGFbaisrXSA9IHBbMV07IC8vIHgxLCB5MVxuXG4gICAgICAgIHBbMF0gKz0gZGF0YVtpKytdO1xuICAgICAgICBwWzFdICs9IGRhdGFbaSsrXTtcbiAgICAgICAgdjJBcHBseVRyYW5zZm9ybShwLCBwLCBtKTtcbiAgICAgICAgZGF0YVtqKytdID0gcFswXTtcbiAgICAgICAgZGF0YVtqKytdID0gcFsxXTtcbiAgICB9XG5cbiAgICBmb3IgKGsgPSAwOyBrIDwgblBvaW50OyBrKyspIHtcbiAgICAgIHZhciBwID0gcG9pbnRzW2tdO1xuICAgICAgcFswXSA9IGRhdGFbaSsrXTtcbiAgICAgIHBbMV0gPSBkYXRhW2krK107XG4gICAgICB2MkFwcGx5VHJhbnNmb3JtKHAsIHAsIG0pOyAvLyBXcml0ZSBiYWNrXG5cbiAgICAgIGRhdGFbaisrXSA9IHBbMF07XG4gICAgICBkYXRhW2orK10gPSBwWzFdO1xuICAgIH1cbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0REE7QUFDQTtBQXdEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/tool/transformPath.js
