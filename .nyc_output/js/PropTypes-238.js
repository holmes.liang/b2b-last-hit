

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var PropTypes = __importStar(__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js"));

function propsValueType() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var props = args[0],
      propName = args[1],
      componentName = args[2],
      rest = args.slice(3);
  var basicType = PropTypes.oneOfType([PropTypes.string, PropTypes.number]);
  var labelInValueShape = PropTypes.shape({
    key: basicType.isRequired,
    label: PropTypes.node
  });

  if (props.labelInValue) {
    var validate = PropTypes.oneOfType([PropTypes.arrayOf(labelInValueShape), labelInValueShape]);
    var error = validate.apply(void 0, [props, propName, componentName].concat(_toConsumableArray(rest)));

    if (error) {
      return new Error("Invalid prop `".concat(propName, "` supplied to `").concat(componentName, "`, ") + "when you set `labelInValue` to `true`, `".concat(propName, "` should in ") + "shape of `{ key: string | number, label?: ReactNode }`.");
    }
  } else if ((props.mode === 'multiple' || props.mode === 'tags' || props.multiple || props.tags) && props[propName] === '') {
    return new Error("Invalid prop `".concat(propName, "` of type `string` supplied to `").concat(componentName, "`, ") + "expected `array` when `multiple` or `tags` is `true`.");
  } else {
    var _validate = PropTypes.oneOfType([PropTypes.arrayOf(basicType), basicType]);

    return _validate.apply(void 0, [props, propName, componentName].concat(_toConsumableArray(rest)));
  }

  return null;
}

var SelectPropTypes = {
  id: PropTypes.string,
  defaultActiveFirstOption: PropTypes.bool,
  multiple: PropTypes.bool,
  filterOption: PropTypes.any,
  children: PropTypes.any,
  showSearch: PropTypes.bool,
  disabled: PropTypes.bool,
  allowClear: PropTypes.bool,
  showArrow: PropTypes.bool,
  tags: PropTypes.bool,
  prefixCls: PropTypes.string,
  className: PropTypes.string,
  transitionName: PropTypes.string,
  optionLabelProp: PropTypes.string,
  optionFilterProp: PropTypes.string,
  animation: PropTypes.string,
  choiceTransitionName: PropTypes.string,
  open: PropTypes.bool,
  defaultOpen: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onSelect: PropTypes.func,
  onSearch: PropTypes.func,
  onPopupScroll: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  onInputKeyDown: PropTypes.func,
  placeholder: PropTypes.any,
  onDeselect: PropTypes.func,
  labelInValue: PropTypes.bool,
  loading: PropTypes.bool,
  value: propsValueType,
  defaultValue: propsValueType,
  dropdownStyle: PropTypes.object,
  maxTagTextLength: PropTypes.number,
  maxTagCount: PropTypes.number,
  maxTagPlaceholder: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  tokenSeparators: PropTypes.arrayOf(PropTypes.string),
  getInputElement: PropTypes.func,
  showAction: PropTypes.arrayOf(PropTypes.string),
  clearIcon: PropTypes.node,
  inputIcon: PropTypes.node,
  removeIcon: PropTypes.node,
  menuItemSelectedIcon: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  dropdownRender: PropTypes.func
};
exports["default"] = SelectPropTypes;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtc2VsZWN0L2VzL1Byb3BUeXBlcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXNlbGVjdC9lcy9Qcm9wVHlwZXMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbmZ1bmN0aW9uIF90b0NvbnN1bWFibGVBcnJheShhcnIpIHsgcmV0dXJuIF9hcnJheVdpdGhvdXRIb2xlcyhhcnIpIHx8IF9pdGVyYWJsZVRvQXJyYXkoYXJyKSB8fCBfbm9uSXRlcmFibGVTcHJlYWQoKTsgfVxuXG5mdW5jdGlvbiBfbm9uSXRlcmFibGVTcHJlYWQoKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gc3ByZWFkIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKTsgfVxuXG5mdW5jdGlvbiBfaXRlcmFibGVUb0FycmF5KGl0ZXIpIHsgaWYgKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoaXRlcikgfHwgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGl0ZXIpID09PSBcIltvYmplY3QgQXJndW1lbnRzXVwiKSByZXR1cm4gQXJyYXkuZnJvbShpdGVyKTsgfVxuXG5mdW5jdGlvbiBfYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB7IGlmIChBcnJheS5pc0FycmF5KGFycikpIHsgZm9yICh2YXIgaSA9IDAsIGFycjIgPSBuZXcgQXJyYXkoYXJyLmxlbmd0aCk7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHsgYXJyMltpXSA9IGFycltpXTsgfSByZXR1cm4gYXJyMjsgfSB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIFByb3BUeXBlcyA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicHJvcC10eXBlc1wiKSk7XG5cbmZ1bmN0aW9uIHByb3BzVmFsdWVUeXBlKCkge1xuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IG5ldyBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICB9XG5cbiAgdmFyIHByb3BzID0gYXJnc1swXSxcbiAgICAgIHByb3BOYW1lID0gYXJnc1sxXSxcbiAgICAgIGNvbXBvbmVudE5hbWUgPSBhcmdzWzJdLFxuICAgICAgcmVzdCA9IGFyZ3Muc2xpY2UoMyk7XG4gIHZhciBiYXNpY1R5cGUgPSBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSk7XG4gIHZhciBsYWJlbEluVmFsdWVTaGFwZSA9IFByb3BUeXBlcy5zaGFwZSh7XG4gICAga2V5OiBiYXNpY1R5cGUuaXNSZXF1aXJlZCxcbiAgICBsYWJlbDogUHJvcFR5cGVzLm5vZGVcbiAgfSk7XG5cbiAgaWYgKHByb3BzLmxhYmVsSW5WYWx1ZSkge1xuICAgIHZhciB2YWxpZGF0ZSA9IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5hcnJheU9mKGxhYmVsSW5WYWx1ZVNoYXBlKSwgbGFiZWxJblZhbHVlU2hhcGVdKTtcbiAgICB2YXIgZXJyb3IgPSB2YWxpZGF0ZS5hcHBseSh2b2lkIDAsIFtwcm9wcywgcHJvcE5hbWUsIGNvbXBvbmVudE5hbWVdLmNvbmNhdChfdG9Db25zdW1hYmxlQXJyYXkocmVzdCkpKTtcblxuICAgIGlmIChlcnJvcikge1xuICAgICAgcmV0dXJuIG5ldyBFcnJvcihcIkludmFsaWQgcHJvcCBgXCIuY29uY2F0KHByb3BOYW1lLCBcImAgc3VwcGxpZWQgdG8gYFwiKS5jb25jYXQoY29tcG9uZW50TmFtZSwgXCJgLCBcIikgKyBcIndoZW4geW91IHNldCBgbGFiZWxJblZhbHVlYCB0byBgdHJ1ZWAsIGBcIi5jb25jYXQocHJvcE5hbWUsIFwiYCBzaG91bGQgaW4gXCIpICsgXCJzaGFwZSBvZiBgeyBrZXk6IHN0cmluZyB8IG51bWJlciwgbGFiZWw/OiBSZWFjdE5vZGUgfWAuXCIpO1xuICAgIH1cbiAgfSBlbHNlIGlmICgocHJvcHMubW9kZSA9PT0gJ211bHRpcGxlJyB8fCBwcm9wcy5tb2RlID09PSAndGFncycgfHwgcHJvcHMubXVsdGlwbGUgfHwgcHJvcHMudGFncykgJiYgcHJvcHNbcHJvcE5hbWVdID09PSAnJykge1xuICAgIHJldHVybiBuZXcgRXJyb3IoXCJJbnZhbGlkIHByb3AgYFwiLmNvbmNhdChwcm9wTmFtZSwgXCJgIG9mIHR5cGUgYHN0cmluZ2Agc3VwcGxpZWQgdG8gYFwiKS5jb25jYXQoY29tcG9uZW50TmFtZSwgXCJgLCBcIikgKyBcImV4cGVjdGVkIGBhcnJheWAgd2hlbiBgbXVsdGlwbGVgIG9yIGB0YWdzYCBpcyBgdHJ1ZWAuXCIpO1xuICB9IGVsc2Uge1xuICAgIHZhciBfdmFsaWRhdGUgPSBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuYXJyYXlPZihiYXNpY1R5cGUpLCBiYXNpY1R5cGVdKTtcblxuICAgIHJldHVybiBfdmFsaWRhdGUuYXBwbHkodm9pZCAwLCBbcHJvcHMsIHByb3BOYW1lLCBjb21wb25lbnROYW1lXS5jb25jYXQoX3RvQ29uc3VtYWJsZUFycmF5KHJlc3QpKSk7XG4gIH1cblxuICByZXR1cm4gbnVsbDtcbn1cblxudmFyIFNlbGVjdFByb3BUeXBlcyA9IHtcbiAgaWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRlZmF1bHRBY3RpdmVGaXJzdE9wdGlvbjogUHJvcFR5cGVzLmJvb2wsXG4gIG11bHRpcGxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgZmlsdGVyT3B0aW9uOiBQcm9wVHlwZXMuYW55LFxuICBjaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcbiAgc2hvd1NlYXJjaDogUHJvcFR5cGVzLmJvb2wsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgYWxsb3dDbGVhcjogUHJvcFR5cGVzLmJvb2wsXG4gIHNob3dBcnJvdzogUHJvcFR5cGVzLmJvb2wsXG4gIHRhZ3M6IFByb3BUeXBlcy5ib29sLFxuICBwcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHJhbnNpdGlvbk5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG9wdGlvbkxhYmVsUHJvcDogUHJvcFR5cGVzLnN0cmluZyxcbiAgb3B0aW9uRmlsdGVyUHJvcDogUHJvcFR5cGVzLnN0cmluZyxcbiAgYW5pbWF0aW9uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBjaG9pY2VUcmFuc2l0aW9uTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgb3BlbjogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRPcGVuOiBQcm9wVHlwZXMuYm9vbCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICBvbkJsdXI6IFByb3BUeXBlcy5mdW5jLFxuICBvbkZvY3VzOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25TZWxlY3Q6IFByb3BUeXBlcy5mdW5jLFxuICBvblNlYXJjaDogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uUG9wdXBTY3JvbGw6IFByb3BUeXBlcy5mdW5jLFxuICBvbk1vdXNlRW50ZXI6IFByb3BUeXBlcy5mdW5jLFxuICBvbk1vdXNlTGVhdmU6IFByb3BUeXBlcy5mdW5jLFxuICBvbklucHV0S2V5RG93bjogUHJvcFR5cGVzLmZ1bmMsXG4gIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuYW55LFxuICBvbkRlc2VsZWN0OiBQcm9wVHlwZXMuZnVuYyxcbiAgbGFiZWxJblZhbHVlOiBQcm9wVHlwZXMuYm9vbCxcbiAgbG9hZGluZzogUHJvcFR5cGVzLmJvb2wsXG4gIHZhbHVlOiBwcm9wc1ZhbHVlVHlwZSxcbiAgZGVmYXVsdFZhbHVlOiBwcm9wc1ZhbHVlVHlwZSxcbiAgZHJvcGRvd25TdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgbWF4VGFnVGV4dExlbmd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4VGFnQ291bnQ6IFByb3BUeXBlcy5udW1iZXIsXG4gIG1heFRhZ1BsYWNlaG9sZGVyOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMubm9kZSwgUHJvcFR5cGVzLmZ1bmNdKSxcbiAgdG9rZW5TZXBhcmF0b3JzOiBQcm9wVHlwZXMuYXJyYXlPZihQcm9wVHlwZXMuc3RyaW5nKSxcbiAgZ2V0SW5wdXRFbGVtZW50OiBQcm9wVHlwZXMuZnVuYyxcbiAgc2hvd0FjdGlvbjogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyksXG4gIGNsZWFySWNvbjogUHJvcFR5cGVzLm5vZGUsXG4gIGlucHV0SWNvbjogUHJvcFR5cGVzLm5vZGUsXG4gIHJlbW92ZUljb246IFByb3BUeXBlcy5ub2RlLFxuICBtZW51SXRlbVNlbGVjdGVkSWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmZ1bmMsIFByb3BUeXBlcy5ub2RlXSksXG4gIGRyb3Bkb3duUmVuZGVyOiBQcm9wVHlwZXMuZnVuY1xufTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gU2VsZWN0UHJvcFR5cGVzOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBOUNBO0FBZ0RBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-select/es/PropTypes.js
