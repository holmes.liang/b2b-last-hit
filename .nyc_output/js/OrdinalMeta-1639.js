/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var isObject = _util.isObject;
var map = _util.map;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @constructor
 * @param {Object} [opt]
 * @param {Object} [opt.categories=[]]
 * @param {Object} [opt.needCollect=false]
 * @param {Object} [opt.deduplication=false]
 */

function OrdinalMeta(opt) {
  /**
   * @readOnly
   * @type {Array.<string>}
   */
  this.categories = opt.categories || [];
  /**
   * @private
   * @type {boolean}
   */

  this._needCollect = opt.needCollect;
  /**
   * @private
   * @type {boolean}
   */

  this._deduplication = opt.deduplication;
  /**
   * @private
   * @type {boolean}
   */

  this._map;
}
/**
 * @param {module:echarts/model/Model} axisModel
 * @return {module:echarts/data/OrdinalMeta}
 */


OrdinalMeta.createByAxisModel = function (axisModel) {
  var option = axisModel.option;
  var data = option.data;
  var categories = data && map(data, getName);
  return new OrdinalMeta({
    categories: categories,
    needCollect: !categories,
    // deduplication is default in axis.
    deduplication: option.dedplication !== false
  });
};

var proto = OrdinalMeta.prototype;
/**
 * @param {string} category
 * @return {number} ordinal
 */

proto.getOrdinal = function (category) {
  return getOrCreateMap(this).get(category);
};
/**
 * @param {*} category
 * @return {number} The ordinal. If not found, return NaN.
 */


proto.parseAndCollect = function (category) {
  var index;
  var needCollect = this._needCollect; // The value of category dim can be the index of the given category set.
  // This feature is only supported when !needCollect, because we should
  // consider a common case: a value is 2017, which is a number but is
  // expected to be tread as a category. This case usually happen in dataset,
  // where it happent to be no need of the index feature.

  if (typeof category !== 'string' && !needCollect) {
    return category;
  } // Optimize for the scenario:
  // category is ['2012-01-01', '2012-01-02', ...], where the input
  // data has been ensured not duplicate and is large data.
  // Notice, if a dataset dimension provide categroies, usually echarts
  // should remove duplication except user tell echarts dont do that
  // (set axis.deduplication = false), because echarts do not know whether
  // the values in the category dimension has duplication (consider the
  // parallel-aqi example)


  if (needCollect && !this._deduplication) {
    index = this.categories.length;
    this.categories[index] = category;
    return index;
  }

  var map = getOrCreateMap(this);
  index = map.get(category);

  if (index == null) {
    if (needCollect) {
      index = this.categories.length;
      this.categories[index] = category;
      map.set(category, index);
    } else {
      index = NaN;
    }
  }

  return index;
}; // Consider big data, do not create map until needed.


function getOrCreateMap(ordinalMeta) {
  return ordinalMeta._map || (ordinalMeta._map = createHashMap(ordinalMeta.categories));
}

function getName(obj) {
  if (isObject(obj) && obj.value != null) {
    return obj.value;
  } else {
    return obj + '';
  }
}

var _default = OrdinalMeta;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9PcmRpbmFsTWV0YS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2RhdGEvT3JkaW5hbE1ldGEuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBjcmVhdGVIYXNoTWFwID0gX3V0aWwuY3JlYXRlSGFzaE1hcDtcbnZhciBpc09iamVjdCA9IF91dGlsLmlzT2JqZWN0O1xudmFyIG1hcCA9IF91dGlsLm1hcDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBjb25zdHJ1Y3RvclxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdC5jYXRlZ29yaWVzPVtdXVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHQubmVlZENvbGxlY3Q9ZmFsc2VdXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdC5kZWR1cGxpY2F0aW9uPWZhbHNlXVxuICovXG5mdW5jdGlvbiBPcmRpbmFsTWV0YShvcHQpIHtcbiAgLyoqXG4gICAqIEByZWFkT25seVxuICAgKiBAdHlwZSB7QXJyYXkuPHN0cmluZz59XG4gICAqL1xuICB0aGlzLmNhdGVnb3JpZXMgPSBvcHQuY2F0ZWdvcmllcyB8fCBbXTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cblxuICB0aGlzLl9uZWVkQ29sbGVjdCA9IG9wdC5uZWVkQ29sbGVjdDtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKi9cblxuICB0aGlzLl9kZWR1cGxpY2F0aW9uID0gb3B0LmRlZHVwbGljYXRpb247XG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG5cbiAgdGhpcy5fbWFwO1xufVxuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL01vZGVsfSBheGlzTW9kZWxcbiAqIEByZXR1cm4ge21vZHVsZTplY2hhcnRzL2RhdGEvT3JkaW5hbE1ldGF9XG4gKi9cblxuXG5PcmRpbmFsTWV0YS5jcmVhdGVCeUF4aXNNb2RlbCA9IGZ1bmN0aW9uIChheGlzTW9kZWwpIHtcbiAgdmFyIG9wdGlvbiA9IGF4aXNNb2RlbC5vcHRpb247XG4gIHZhciBkYXRhID0gb3B0aW9uLmRhdGE7XG4gIHZhciBjYXRlZ29yaWVzID0gZGF0YSAmJiBtYXAoZGF0YSwgZ2V0TmFtZSk7XG4gIHJldHVybiBuZXcgT3JkaW5hbE1ldGEoe1xuICAgIGNhdGVnb3JpZXM6IGNhdGVnb3JpZXMsXG4gICAgbmVlZENvbGxlY3Q6ICFjYXRlZ29yaWVzLFxuICAgIC8vIGRlZHVwbGljYXRpb24gaXMgZGVmYXVsdCBpbiBheGlzLlxuICAgIGRlZHVwbGljYXRpb246IG9wdGlvbi5kZWRwbGljYXRpb24gIT09IGZhbHNlXG4gIH0pO1xufTtcblxudmFyIHByb3RvID0gT3JkaW5hbE1ldGEucHJvdG90eXBlO1xuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gY2F0ZWdvcnlcbiAqIEByZXR1cm4ge251bWJlcn0gb3JkaW5hbFxuICovXG5cbnByb3RvLmdldE9yZGluYWwgPSBmdW5jdGlvbiAoY2F0ZWdvcnkpIHtcbiAgcmV0dXJuIGdldE9yQ3JlYXRlTWFwKHRoaXMpLmdldChjYXRlZ29yeSk7XG59O1xuLyoqXG4gKiBAcGFyYW0geyp9IGNhdGVnb3J5XG4gKiBAcmV0dXJuIHtudW1iZXJ9IFRoZSBvcmRpbmFsLiBJZiBub3QgZm91bmQsIHJldHVybiBOYU4uXG4gKi9cblxuXG5wcm90by5wYXJzZUFuZENvbGxlY3QgPSBmdW5jdGlvbiAoY2F0ZWdvcnkpIHtcbiAgdmFyIGluZGV4O1xuICB2YXIgbmVlZENvbGxlY3QgPSB0aGlzLl9uZWVkQ29sbGVjdDsgLy8gVGhlIHZhbHVlIG9mIGNhdGVnb3J5IGRpbSBjYW4gYmUgdGhlIGluZGV4IG9mIHRoZSBnaXZlbiBjYXRlZ29yeSBzZXQuXG4gIC8vIFRoaXMgZmVhdHVyZSBpcyBvbmx5IHN1cHBvcnRlZCB3aGVuICFuZWVkQ29sbGVjdCwgYmVjYXVzZSB3ZSBzaG91bGRcbiAgLy8gY29uc2lkZXIgYSBjb21tb24gY2FzZTogYSB2YWx1ZSBpcyAyMDE3LCB3aGljaCBpcyBhIG51bWJlciBidXQgaXNcbiAgLy8gZXhwZWN0ZWQgdG8gYmUgdHJlYWQgYXMgYSBjYXRlZ29yeS4gVGhpcyBjYXNlIHVzdWFsbHkgaGFwcGVuIGluIGRhdGFzZXQsXG4gIC8vIHdoZXJlIGl0IGhhcHBlbnQgdG8gYmUgbm8gbmVlZCBvZiB0aGUgaW5kZXggZmVhdHVyZS5cblxuICBpZiAodHlwZW9mIGNhdGVnb3J5ICE9PSAnc3RyaW5nJyAmJiAhbmVlZENvbGxlY3QpIHtcbiAgICByZXR1cm4gY2F0ZWdvcnk7XG4gIH0gLy8gT3B0aW1pemUgZm9yIHRoZSBzY2VuYXJpbzpcbiAgLy8gY2F0ZWdvcnkgaXMgWycyMDEyLTAxLTAxJywgJzIwMTItMDEtMDInLCAuLi5dLCB3aGVyZSB0aGUgaW5wdXRcbiAgLy8gZGF0YSBoYXMgYmVlbiBlbnN1cmVkIG5vdCBkdXBsaWNhdGUgYW5kIGlzIGxhcmdlIGRhdGEuXG4gIC8vIE5vdGljZSwgaWYgYSBkYXRhc2V0IGRpbWVuc2lvbiBwcm92aWRlIGNhdGVncm9pZXMsIHVzdWFsbHkgZWNoYXJ0c1xuICAvLyBzaG91bGQgcmVtb3ZlIGR1cGxpY2F0aW9uIGV4Y2VwdCB1c2VyIHRlbGwgZWNoYXJ0cyBkb250IGRvIHRoYXRcbiAgLy8gKHNldCBheGlzLmRlZHVwbGljYXRpb24gPSBmYWxzZSksIGJlY2F1c2UgZWNoYXJ0cyBkbyBub3Qga25vdyB3aGV0aGVyXG4gIC8vIHRoZSB2YWx1ZXMgaW4gdGhlIGNhdGVnb3J5IGRpbWVuc2lvbiBoYXMgZHVwbGljYXRpb24gKGNvbnNpZGVyIHRoZVxuICAvLyBwYXJhbGxlbC1hcWkgZXhhbXBsZSlcblxuXG4gIGlmIChuZWVkQ29sbGVjdCAmJiAhdGhpcy5fZGVkdXBsaWNhdGlvbikge1xuICAgIGluZGV4ID0gdGhpcy5jYXRlZ29yaWVzLmxlbmd0aDtcbiAgICB0aGlzLmNhdGVnb3JpZXNbaW5kZXhdID0gY2F0ZWdvcnk7XG4gICAgcmV0dXJuIGluZGV4O1xuICB9XG5cbiAgdmFyIG1hcCA9IGdldE9yQ3JlYXRlTWFwKHRoaXMpO1xuICBpbmRleCA9IG1hcC5nZXQoY2F0ZWdvcnkpO1xuXG4gIGlmIChpbmRleCA9PSBudWxsKSB7XG4gICAgaWYgKG5lZWRDb2xsZWN0KSB7XG4gICAgICBpbmRleCA9IHRoaXMuY2F0ZWdvcmllcy5sZW5ndGg7XG4gICAgICB0aGlzLmNhdGVnb3JpZXNbaW5kZXhdID0gY2F0ZWdvcnk7XG4gICAgICBtYXAuc2V0KGNhdGVnb3J5LCBpbmRleCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGluZGV4ID0gTmFOO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBpbmRleDtcbn07IC8vIENvbnNpZGVyIGJpZyBkYXRhLCBkbyBub3QgY3JlYXRlIG1hcCB1bnRpbCBuZWVkZWQuXG5cblxuZnVuY3Rpb24gZ2V0T3JDcmVhdGVNYXAob3JkaW5hbE1ldGEpIHtcbiAgcmV0dXJuIG9yZGluYWxNZXRhLl9tYXAgfHwgKG9yZGluYWxNZXRhLl9tYXAgPSBjcmVhdGVIYXNoTWFwKG9yZGluYWxNZXRhLmNhdGVnb3JpZXMpKTtcbn1cblxuZnVuY3Rpb24gZ2V0TmFtZShvYmopIHtcbiAgaWYgKGlzT2JqZWN0KG9iaikgJiYgb2JqLnZhbHVlICE9IG51bGwpIHtcbiAgICByZXR1cm4gb2JqLnZhbHVlO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBvYmogKyAnJztcbiAgfVxufVxuXG52YXIgX2RlZmF1bHQgPSBPcmRpbmFsTWV0YTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTs7Ozs7Ozs7QUFPQTtBQUNBOzs7O0FBSUE7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/OrdinalMeta.js
