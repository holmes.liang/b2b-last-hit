__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formGroupSelectLayout", function() { return formGroupSelectLayout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formGroupSelectLayout1", function() { return formGroupSelectLayout1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formBtnLayout", function() { return formBtnLayout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Quotes", function() { return Quotes; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_component_wizard__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @desk-component/wizard */ "./src/app/desk/component/wizard.tsx");
/* harmony import */ var _all_steps__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../all-steps */ "./src/app/desk/quote/compare/all-steps.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _desk_component_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component/index */ "./src/app/desk/component/index.tsx");
/* harmony import */ var _desk_component_vehicle_model__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @desk-component/vehicle-model */ "./src/app/desk/component/vehicle-model.tsx");
/* harmony import */ var antd_lib_form_FormItem__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! antd/lib/form/FormItem */ "./node_modules/antd/lib/form/FormItem.js");
/* harmony import */ var antd_lib_form_FormItem__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(antd_lib_form_FormItem__WEBPACK_IMPORTED_MODULE_17__);







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/compare/vmi/quote.tsx";












var formGroupSelectLayout = {
  labelCol: {
    xs: {
      span: 0
    },
    sm: {
      span: 0
    }
  },
  wrapperCol: {
    xs: {
      span: 24
    },
    sm: {
      span: 24
    }
  }
};
var formGroupSelectLayout1 = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var formBtnLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var vehicleCodeOfTonnage = ["310", "320", "340", "420", "520", "540", "803", "804", "805"];
var vehicleCodeOfSeat = ["210", "220", "230"];

var Quotes =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Quotes, _QuoteStep);

  function Quotes() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Quotes);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Quotes)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.dataIdPrefix = "policy.ext";
    _this.usageOptions = [];
    _this.currentYear = new Date().getFullYear();

    _this.loadVehicleUsages = function () {
      var vehicleType = _this.getValueFromModel(_this.generatePropName("vehicleType"));

      if (vehicleType) {
        _common__WEBPACK_IMPORTED_MODULE_12__["Ajax"].get("/compareprice/thai/vmi/mastertable/usage/".concat(vehicleType)).then(function (response) {
          var respData = response.body.respData;
          _this.usageOptions = respData.items || [];
          var usage = (_this.usageOptions[0] || {}).id;

          _this.setValueToModel(usage, _this.generatePropName("usage"));

          _this.setValueToModel(usage + "", _this.generatePropName("vehicleCode"));
        });
      }
    };

    _this.handleModelChange = function () {
      _this.props.form.resetFields(["capacity", "tonnage", "noOfSeat"]);

      var vehicleTypes = _this.getValueFromModel(_this.generatePropName("platformVehicleModel.ext.vehicleTypes")),
          weights = _this.getValueFromModel(_this.generatePropName("platformVehicleModel.ext.weights")),
          seats = _this.getValueFromModel(_this.generatePropName("platformVehicleModel.ext.seats")),
          capacities = _this.getValueFromModel(_this.generatePropName("platformVehicleModel.ext.capacities")),
          marketValue = _this.getValueFromModel(_this.generatePropName("platformVehicleModel.ext.marketValue"));

      if (capacities && capacities.length > 0) {
        _this.setValueToModel(capacities[0], _this.generatePropName("capacity"));
      }

      _this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this.generatePropName("capacity"), _this.getValueFromModel(_this.generatePropName("capacity"))));

      _this.setValueToModel(vehicleTypes[0], _this.generatePropName("vehicleType"));

      _this.setValueToModel(marketValue, _this.generatePropName("marketValue"));

      _this.setValueToModel(weights[0], _this.generatePropName("tonnage"));

      _this.setValueToModel(seats[0], _this.generatePropName("noOfSeat"));

      _this.loadVehicleUsages();
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Quotes, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Quotes.prototype), "initComponents", this).call(this), {});
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      setTimeout(function () {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
      }, 100);
    }
  }, {
    key: "renderActions",
    value: function renderActions() {
      var _this2 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd_lib_form_FormItem__WEBPACK_IMPORTED_MODULE_17___default.a, Object.assign({
        style: {
          padding: "0 16px"
        },
        label: " ",
        colon: false
      }, formBtnLayout, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Button"], {
        style: {
          width: "100%",
          marginBottom: "20px"
        },
        size: "large",
        type: "primary",
        onClick: function onClick() {
          _this2.props.form.validateFields(function (err, fieldsValue) {
            if (err) {
              return;
            }

            Object(_desk_component_wizard__WEBPACK_IMPORTED_MODULE_9__["pageTo"])(_this2, _all_steps__WEBPACK_IMPORTED_MODULE_10__["AllSteps"].PLANS);
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Search").thai("ค้นหา").my("ရှာဖှေ").getMessage())));
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName) {
      if (!propName) return this.dataIdPrefix;
      return "".concat(this.dataIdPrefix, ".").concat(propName);
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this3 = this;

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "quote",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 128
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NCollapse"], {
        defaultActiveKey: ["1", "2", "3"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("About the Vehicle").thai("เกี่ยวกับยานพาหนะ").my("ယာဉ်အကြောင်း").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_14__["NFormItem"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Model").thai("รายละเอียดของรถยนต์").my("မော်တော်ယာဉ်မော်ဒယ်").getMessage(),
        propName: this.generatePropName("platformVehicleModel"),
        required: true,
        onChange: this.handleModelChange,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_vehicle_model__WEBPACK_IMPORTED_MODULE_16__["default"], {
        prdtCode: "VMI",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 138
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Vehicle Type").thai("ประเภทยานพาหนะ").my("ယာဉ်အမျိုးအစား").getMessage(),
        propName: this.generatePropName("vehicleType"),
        required: true,
        onChange: this.loadVehicleUsages,
        size: "large",
        dataFixed: "policy",
        tableName: "vehicletype",
        isCompare: true,
        filter: function filter(options) {
          var vehicleTypes = _this3.getValueFromModel(_this3.generatePropName("platformVehicleModel.ext.vehicleTypes")) || [];
          var vehicleTypeOptions = [];
          options.forEach(function (item) {
            if (vehicleTypes.includes(item.id)) {
              vehicleTypeOptions.push(item);
            }
          });
          /*if vehicle type contains sedan(code: 620) in vehicle model,
           then add taxi(code: 626) in vehicle type list*/

          if (vehicleTypes.includes("620")) {
            vehicleTypeOptions.push({
              id: "626",
              text: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Taxi").thai("แท็กซี่").getMessage()
            });
          }

          return vehicleTypeOptions;
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_index__WEBPACK_IMPORTED_MODULE_15__["FieldGroup"], {
        className: "usage-group",
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        minWidth: "390px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        size: "large",
        form: form,
        options: this.usageOptions,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Usage").thai("ประเภทการใช้งาน").my("အသုံးပြုမှု").getMessage(),
        required: true,
        model: model,
        propName: this.generatePropName("usage"),
        onChange: function onChange(value) {
          _this3.setValueToModel(value + "", _this3.generatePropName("vehicleCode"));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 171
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        form: form,
        model: model,
        propName: this.generatePropName("vehicleCode"),
        disabled: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 180
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Capacity (L)").thai("ขนาดเครื่องยนต์ (L)").my("ထုတ်လွှတ် (L)").getMessage(),
        propName: this.generatePropName("capacity"),
        required: true,
        notFoundContent: "",
        showSearch: true,
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!/^([1-9](\.\d{1,3})?|(0\.\d{1,3}))$/.test(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("The value must be in range of (0, 10) litres, and maximum 3 decimals are allowed").thai("ค่าจะต้องอยู่ในช่วง (0, 10) ลิตรและสูงสุด 3 ทศนิยม").getMessage());
            } else {
              callback();
            }
          }
        }],
        onBlur: function onBlur() {
          _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this3.generatePropName("capacity"), _this3.getValueFromModel(_this3.generatePropName("capacity"))));

          _this3.props.form.validateFields([_this3.generatePropName("capacity")], {
            force: true
          });
        },
        onSearch: function onSearch(value) {
          //TODO
          if (!value) return;

          _this3.setValueToModel(value, _this3.generatePropName("capacity"));
        },
        options: (this.getValueFromModel(this.generatePropName("platformVehicleModel.ext.capacities")) || []).map(function (item, index) {
          return {
            id: item,
            text: item
          };
        }),
        size: "large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }), vehicleCodeOfTonnage.includes(this.getValueFromModel(this.generatePropName("vehicleCode"))) && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Weight(T)").thai("น้ำหนัก (T)").my("အလေးချိန် (T) ကို").getMessage(),
        propName: this.generatePropName("tonnage"),
        required: true,
        notFoundContent: "",
        showSearch: true,
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!/^(\d{2}(\.\d{1,3})?|([1-9](\.\d{1,3})?|(0\.\d{1,3})))$/.test(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("The value must be in range of (0, 100) litres, and maximum 3 decimals are allowed").thai("ค่าต้องอยู่ในช่วง (0, 100) ลิตรและสูงสุด 3 ทศนิยม").getMessage());
            } else {
              callback();
            }
          }
        }],
        onBlur: function onBlur() {
          _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this3.generatePropName("tonnage"), _this3.getValueFromModel(_this3.generatePropName("tonnage"))));

          _this3.props.form.validateFields([_this3.generatePropName("tonnage")], {
            force: true
          });
        },
        onSearch: function onSearch(value) {
          //TODO
          if (!value) return;

          _this3.setValueToModel(value, _this3.generatePropName("tonnage"));
        },
        options: (this.getValueFromModel(this.generatePropName("platformVehicleModel.ext.weights")) || []).map(function (item, index) {
          return {
            id: item,
            text: item
          };
        }),
        size: "large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 219
        },
        __self: this
      }), vehicleCodeOfSeat.includes(this.getValueFromModel(this.generatePropName("vehicleCode"))) && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("No. of Seat").thai("จำนวนที่นั่ง").my("ထိုင်ခုံ၏အမှတ်").getMessage(),
        propName: this.generatePropName("noOfSeat"),
        required: true,
        notFoundContent: "",
        showSearch: true,
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!/^(\d{2}?|[1-9])$/.test(value)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("The value must be in range of (0, 100) litres").thai("ค่าจะต้องอยู่ในช่วง (0, 100) ลิตร").getMessage());
            } else {
              callback();
            }
          }
        }],
        onBlur: function onBlur() {
          _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this3.generatePropName("noOfSeat"), _this3.getValueFromModel(_this3.generatePropName("noOfSeat"))));

          _this3.props.form.validateFields([_this3.generatePropName("noOfSeat")], {
            force: true
          });
        },
        onSearch: function onSearch(value) {
          //TODO
          if (!value) return;

          _this3.setValueToModel(value, _this3.generatePropName("noOfSeat"));
        },
        options: (this.getValueFromModel(this.generatePropName("platformVehicleModel.ext.seats")) || []).map(function (item, index) {
          return {
            id: item,
            text: item
          };
        }),
        size: "large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 260
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_desk_component_index__WEBPACK_IMPORTED_MODULE_15__["FieldGroup"], {
        className: "year-group",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration Year").thai("ปีที่จดทะเบียน").my("REGISTRATION YEAR တွင်").getMessage(),
        selectXsSm: {
          xs: 8,
          sm: 6
        },
        textXsSm: {
          xs: 16,
          sm: 13
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 295
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        propName: this.generatePropName("regYear"),
        layoutCol: formGroupSelectLayout,
        required: true,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration Year").thai("ปีที่จดทะเบียน").my("REGISTRATION YEAR တွင်").getMessage(),
        size: "large",
        disabled: this.getValueFromModel(this.generatePropName("isNewVehicle")) === "Y",
        options: new Array(25).fill(0).map(function (_, index) {
          return {
            id: _this3.currentYear - index,
            text: (_this3.currentYear - index).toString()
          };
        }),
        className: "group-select",
        onChange: function onChange(regYear) {
          var marketValues = _this3.getValueFromModel(_this3.generatePropName("platformVehicleModel.ext.marketValues")) || [];
          var marketValue = (marketValues.find(function (item) {
            return parseInt(item.years) === _this3.currentYear - regYear;
          }) || {}).marketValue;

          _this3.setValueToModel(marketValue, _this3.generatePropName("marketValue"));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 298
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NCheckbox"], {
        form: form,
        model: model,
        propName: this.generatePropName("isNewVehicle"),
        layoutCol: formGroupSelectLayout,
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("New Vehicle").thai("รถใหม่").my("ယာဉ်သစ်").getMessage(),
        checked: this.getValueFromModel(this.generatePropName("isNewVehicle")) === "Y",
        onChange: function onChange(checked) {
          if (checked === "Y") {
            _this3.setValueToModel(_this3.currentYear, _this3.generatePropName("regYear"));

            _this3.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this3.generatePropName("regYear"), _this3.currentYear));
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 318
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSelect"], {
        form: form,
        model: model,
        propName: this.generatePropName("vehicleProvince"),
        tableName: "vehicleprovince",
        isCompare: true,
        dataFixed: "policy",
        required: true,
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Registration Province").thai("จังหวัดที่ลงทะเบียน").my("REGISTRATION ပြည်နယ်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 333
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "2",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Insurance Preferences").thai("การตั้งค่าประกันภัย").my("အာမခံအညွှန်းများ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 344
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPrice"], {
        form: form,
        model: model,
        propName: this.generatePropName("marketValue"),
        size: "large",
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!_common__WEBPACK_IMPORTED_MODULE_12__["Rules"].isXMultiply(value, 10000)) {
              callback(_common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("OD Sum Insured can only be in 10,000s").thai("จำนวนเงินเอาประกันภัย OD สามารถทำได้ใน 10,000 เท่านั้น").getMessage());
            } else {
              callback();
            }
          }
        }],
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("OD Sum Insured").thai("ทุนประกันรถยนต์").my("ပေါင်းလဒ်အာမခံထား").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 347
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NSwitch"], {
        form: form,
        model: model,
        propName: this.generatePropName("cameraInstalled"),
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Camera Installed?").thai("มีกล้องติดรถยนต์หรือไม่?").my("ကင်မရာကိုတပ်ဆင်?").getMessage(),
        defaultChecked: this.getValueFromModel(this.generatePropName("cameraInstalled")) === "Y",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 364
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NPanel"], {
        key: "3",
        header: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Promotion").thai("การส่งเสริม").my("မြှင့်တင်ရေး").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 373
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_13__["NText"], {
        form: form,
        model: model,
        propName: this.generatePropName("promoCode"),
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("Promo Code").thai("รหัสโปรโมชั่น").my("ကြော်ငြာကုဒ်").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 375
        },
        __self: this
      }))));
    }
  }, {
    key: "renderTitle",
    value: function renderTitle() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 389
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_12__["Language"].en("New Quote - Voluntary Motor").thai("ตรวจสอบเบี้ยประกันรถยนต์").my("အသစ်ကကိုးကား - မိမိဆန္ဒအလျောက်မော်တာအာမခံ").getMessage());
    }
  }]);

  return Quotes;
}(_quote_step__WEBPACK_IMPORTED_MODULE_11__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcGFyZS92bWkvcXVvdGUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvY29tcGFyZS92bWkvcXVvdGUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24gfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IHsgcGFnZVRvIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudC93aXphcmRcIjtcbmltcG9ydCB7IEFsbFN0ZXBzIH0gZnJvbSBcIi4uL2FsbC1zdGVwc1wiO1xuaW1wb3J0IFF1b3RlU3RlcCwgeyBTdGVwQ29tcG9uZW50cywgU3RlcFByb3BzIH0gZnJvbSBcIi4uLy4uL3F1b3RlLXN0ZXBcIjtcbmltcG9ydCB7IEFqYXgsIEFwaXMsIExhbmd1YWdlLCBSdWxlcywgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgTkNoZWNrYm94LCBOQ29sbGFwc2UsIE5OdW1iZXIsIE5QYW5lbCwgTlByaWNlLCBOU3dpdGNoIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBORm9ybUl0ZW0gfSBmcm9tIFwiQGNvbXBvbmVudFwiO1xuaW1wb3J0IHsgTlRleHQsIE5TZWxlY3QgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcbmltcG9ydCB7IEZpZWxkR3JvdXAgfSBmcm9tIFwiQGRlc2stY29tcG9uZW50L2luZGV4XCI7XG5pbXBvcnQgVmVoaWNsZU1vZGVsIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmVoaWNsZS1tb2RlbFwiO1xuaW1wb3J0IEZvcm1JdGVtIGZyb20gXCJhbnRkL2xpYi9mb3JtL0Zvcm1JdGVtXCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5cbmV4cG9ydCBjb25zdCBmb3JtR3JvdXBTZWxlY3RMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMCB9LFxuICAgIHNtOiB7IHNwYW46IDAgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDI0IH0sXG4gICAgc206IHsgc3BhbjogMjQgfSxcbiAgfSxcbn07XG5cbmV4cG9ydCBjb25zdCBmb3JtR3JvdXBTZWxlY3RMYXlvdXQxID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDggfSxcbiAgICBzbTogeyBzcGFuOiA2IH0sXG4gIH0sXG4gIHdyYXBwZXJDb2w6IHtcbiAgICB4czogeyBzcGFuOiAxNiB9LFxuICAgIHNtOiB7IHNwYW46IDEzIH0sXG4gIH0sXG59O1xuZXhwb3J0IGNvbnN0IGZvcm1CdG5MYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogOCB9LFxuICAgIHNtOiB7IHNwYW46IDYgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDE2IH0sXG4gICAgc206IHsgc3BhbjogMTMgfSxcbiAgfSxcbn07XG5cbmNvbnN0IHZlaGljbGVDb2RlT2ZUb25uYWdlID0gW1wiMzEwXCIsIFwiMzIwXCIsIFwiMzQwXCIsIFwiNDIwXCIsIFwiNTIwXCIsIFwiNTQwXCIsIFwiODAzXCIsIFwiODA0XCIsIFwiODA1XCJdO1xuY29uc3QgdmVoaWNsZUNvZGVPZlNlYXQgPSBbXCIyMTBcIiwgXCIyMjBcIiwgXCIyMzBcIl07XG5cbmNsYXNzIFF1b3RlczxQIGV4dGVuZHMgU3RlcFByb3BzLCBTLCBDIGV4dGVuZHMgU3RlcENvbXBvbmVudHM+IGV4dGVuZHMgUXVvdGVTdGVwPFAsIFMsIEM+IHtcblxuICBwcml2YXRlIGRhdGFJZFByZWZpeDogc3RyaW5nID0gXCJwb2xpY3kuZXh0XCI7XG4gIHByaXZhdGUgdXNhZ2VPcHRpb25zOiBhbnlbXSA9IFtdO1xuICBwcml2YXRlIGN1cnJlbnRZZWFyOiBudW1iZXIgPSBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCk7XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRDb21wb25lbnRzKCksIHt9KSBhcyBDO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AgPSAwO1xuICAgIH0sIDEwMCk7XG5cblxuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxGb3JtSXRlbSBzdHlsZT17eyBwYWRkaW5nOiBcIjAgMTZweFwiIH19IGxhYmVsPVwiIFwiIGNvbG9uPXtmYWxzZX0gey4uLmZvcm1CdG5MYXlvdXR9PlxuICAgICAgICAgIDxCdXR0b24gc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiLCBtYXJnaW5Cb3R0b206IFwiMjBweFwiIH19XG4gICAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIiB0eXBlPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS52YWxpZGF0ZUZpZWxkcygoZXJyOiBhbnksIGZpZWxkc1ZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIHBhZ2VUbyh0aGlzLCBBbGxTdGVwcy5QTEFOUyk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgfX0+e0xhbmd1YWdlLmVuKFwiU2VhcmNoXCIpLnRoYWkoXCLguITguYnguJnguKvguLJcIikubXkoXCLhgJvhgL7hgKzhgJbhgL7hgLFcIikuZ2V0TWVzc2FnZSgpfTwvQnV0dG9uPlxuICAgICAgICA8L0Zvcm1JdGVtPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgbG9hZFZlaGljbGVVc2FnZXMgPSAoKTogdm9pZCA9PiB7XG4gICAgY29uc3QgdmVoaWNsZVR5cGUgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInZlaGljbGVUeXBlXCIpKTtcbiAgICBpZiAodmVoaWNsZVR5cGUpIHtcbiAgICAgIEFqYXguZ2V0KGAvY29tcGFyZXByaWNlL3RoYWkvdm1pL21hc3RlcnRhYmxlL3VzYWdlLyR7dmVoaWNsZVR5cGV9YCkudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICB0aGlzLnVzYWdlT3B0aW9ucyA9IHJlc3BEYXRhLml0ZW1zIHx8IFtdO1xuICAgICAgICBjb25zdCB1c2FnZSA9ICh0aGlzLnVzYWdlT3B0aW9uc1swXSB8fCB7fSkuaWQ7XG5cbiAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodXNhZ2UsIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInVzYWdlXCIpKTtcbiAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodXNhZ2UgKyBcIlwiLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ2ZWhpY2xlQ29kZVwiKSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgcHJpdmF0ZSBoYW5kbGVNb2RlbENoYW5nZSA9ICgpOiB2b2lkID0+IHtcbiAgICB0aGlzLnByb3BzLmZvcm0ucmVzZXRGaWVsZHMoW1wiY2FwYWNpdHlcIiwgXCJ0b25uYWdlXCIsIFwibm9PZlNlYXRcIl0pO1xuICAgIGNvbnN0IHZlaGljbGVUeXBlcyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGxhdGZvcm1WZWhpY2xlTW9kZWwuZXh0LnZlaGljbGVUeXBlc1wiKSksXG4gICAgICB3ZWlnaHRzID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwbGF0Zm9ybVZlaGljbGVNb2RlbC5leHQud2VpZ2h0c1wiKSksXG4gICAgICBzZWF0cyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGxhdGZvcm1WZWhpY2xlTW9kZWwuZXh0LnNlYXRzXCIpKSxcbiAgICAgIGNhcGFjaXRpZXMgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBsYXRmb3JtVmVoaWNsZU1vZGVsLmV4dC5jYXBhY2l0aWVzXCIpKSxcbiAgICAgIG1hcmtldFZhbHVlID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwbGF0Zm9ybVZlaGljbGVNb2RlbC5leHQubWFya2V0VmFsdWVcIikpO1xuICAgIGlmIChjYXBhY2l0aWVzICYmIGNhcGFjaXRpZXMubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoY2FwYWNpdGllc1swXSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY2FwYWNpdHlcIikpO1xuICAgIH1cbiAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoeyBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY2FwYWNpdHlcIildOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNhcGFjaXR5XCIpKSB9KTtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbCh2ZWhpY2xlVHlwZXNbMF0sIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInZlaGljbGVUeXBlXCIpKTtcbiAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChtYXJrZXRWYWx1ZSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibWFya2V0VmFsdWVcIikpO1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHdlaWdodHNbMF0sIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInRvbm5hZ2VcIikpO1xuICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHNlYXRzWzBdLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJub09mU2VhdFwiKSk7XG4gICAgdGhpcy5sb2FkVmVoaWNsZVVzYWdlcygpO1xuICB9O1xuXG4gIHByaXZhdGUgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZT86IHN0cmluZyk6IHN0cmluZyB7XG4gICAgaWYgKCFwcm9wTmFtZSkgcmV0dXJuIHRoaXMuZGF0YUlkUHJlZml4O1xuXG4gICAgcmV0dXJuIGAke3RoaXMuZGF0YUlkUHJlZml4fS4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQ29udGVudCgpIHtcbiAgICBjb25zdCB7IGZvcm0sIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInF1b3RlXCI+XG4gICAgICAgIDxOQ29sbGFwc2UgZGVmYXVsdEFjdGl2ZUtleT17W1wiMVwiLCBcIjJcIiwgXCIzXCJdfT5cbiAgICAgICAgICA8TlBhbmVsIGtleT1cIjFcIlxuICAgICAgICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkFib3V0IHRoZSBWZWhpY2xlXCIpLnRoYWkoXCLguYDguIHguLXguYjguKLguKfguIHguLHguJrguKLguLLguJnguJ7guLLguKvguJnguLBcIikubXkoXCLhgJrhgKzhgInhgLrhgKHhgIDhgLzhgLHhgKzhgIThgLrhgLhcIikuZ2V0TWVzc2FnZSgpfT5cbiAgICAgICAgICAgIDxORm9ybUl0ZW0gZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJNb2RlbFwiKS50aGFpKFwi4Lij4Liy4Lii4Lil4Liw4LmA4Lit4Li14Lii4LiU4LiC4Lit4LiH4Lij4LiW4Lii4LiZ4LiV4LmMXCIpLm15KFwi4YCZ4YCx4YCs4YC64YCQ4YCx4YCs4YC64YCa4YCs4YCJ4YC64YCZ4YCx4YCs4YC64YCS4YCa4YC6XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBsYXRmb3JtVmVoaWNsZU1vZGVsXCIpfVxuICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlTW9kZWxDaGFuZ2V9PlxuICAgICAgICAgICAgICA8VmVoaWNsZU1vZGVsIHByZHRDb2RlPXtcIlZNSVwifS8+XG4gICAgICAgICAgICA8L05Gb3JtSXRlbT5cblxuICAgICAgICAgICAgPE5TZWxlY3QgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlZlaGljbGUgVHlwZVwiKS50aGFpKFwi4Lib4Lij4Liw4LmA4Lig4LiX4Lii4Liy4LiZ4Lie4Liy4Lir4LiZ4LiwXCIpLm15KFwi4YCa4YCs4YCJ4YC64YCh4YCZ4YC74YCt4YCv4YC44YCh4YCF4YCs4YC4XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ2ZWhpY2xlVHlwZVwiKX1cbiAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMubG9hZFZlaGljbGVVc2FnZXN9XG4gICAgICAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICAgICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgICAgICAgICAgICAgICAgdGFibGVOYW1lPVwidmVoaWNsZXR5cGVcIlxuICAgICAgICAgICAgICAgICAgICAgaXNDb21wYXJlPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgZmlsdGVyPXsob3B0aW9uczogYW55W10pOiBhbnlbXSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHZlaGljbGVUeXBlcyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGxhdGZvcm1WZWhpY2xlTW9kZWwuZXh0LnZlaGljbGVUeXBlc1wiKSkgfHwgW107XG4gICAgICAgICAgICAgICAgICAgICAgIGxldCB2ZWhpY2xlVHlwZU9wdGlvbnMgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmVoaWNsZVR5cGVzLmluY2x1ZGVzKGl0ZW0uaWQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB2ZWhpY2xlVHlwZU9wdGlvbnMucHVzaChpdGVtKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAvKmlmIHZlaGljbGUgdHlwZSBjb250YWlucyBzZWRhbihjb2RlOiA2MjApIGluIHZlaGljbGUgbW9kZWwsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGVuIGFkZCB0YXhpKGNvZGU6IDYyNikgaW4gdmVoaWNsZSB0eXBlIGxpc3QqL1xuICAgICAgICAgICAgICAgICAgICAgICBpZiAodmVoaWNsZVR5cGVzLmluY2x1ZGVzKFwiNjIwXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdmVoaWNsZVR5cGVPcHRpb25zLnB1c2goe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IFwiNjI2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiBMYW5ndWFnZS5lbihcIlRheGlcIikudGhhaShcIuC5geC4l+C5h+C4geC4i+C4teC5iFwiKS5nZXRNZXNzYWdlKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZlaGljbGVUeXBlT3B0aW9ucztcbiAgICAgICAgICAgICAgICAgICAgIH19Lz5cbiAgICAgICAgICAgIDxGaWVsZEdyb3VwIGNsYXNzTmFtZT1cInVzYWdlLWdyb3VwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdFhzU209e3sgeHM6IDgsIHNtOiA2IH19IHRleHRYc1NtPXt7IHhzOiAxNiwgc206IDEzIH19IG1pbldpZHRoPVwiMzkwcHhcIj5cbiAgICAgICAgICAgICAgPE5TZWxlY3Qgc2l6ZT17XCJsYXJnZVwifSBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zPXt0aGlzLnVzYWdlT3B0aW9uc31cbiAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiVXNhZ2VcIikudGhhaShcIuC4m+C4o+C4sOC5gOC4oOC4l+C4geC4suC4o+C5g+C4iuC5ieC4h+C4suC4mVwiKS5teShcIuGAoeGAnuGAr+GAtuGAuOGAleGAvOGAr+GAmeGAvuGAr1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9IHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ1c2FnZVwiKX1cbiAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodmFsdWUgKyBcIlwiLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ2ZWhpY2xlQ29kZVwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgIH19Lz5cblxuICAgICAgICAgICAgICA8TlRleHQgZm9ybT17Zm9ybX0gbW9kZWw9e21vZGVsfSBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidmVoaWNsZUNvZGVcIil9IGRpc2FibGVkLz5cbiAgICAgICAgICAgIDwvRmllbGRHcm91cD5cblxuICAgICAgICAgICAgPE5TZWxlY3QgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIkNhcGFjaXR5IChMKVwiKS50aGFpKFwi4LiC4LiZ4Liy4LiU4LmA4LiE4Lij4Li34LmI4Lit4LiH4Lii4LiZ4LiV4LmMIChMKVwiKS5teShcIuGAkeGAr+GAkOGAuuGAnOGAveGAvuGAkOGAuiAoTClcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNhcGFjaXR5XCIpfVxuICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgICBub3RGb3VuZENvbnRlbnQ9XCJcIlxuICAgICAgICAgICAgICAgICAgICAgc2hvd1NlYXJjaFxuICAgICAgICAgICAgICAgICAgICAgcnVsZXM9e1tcbiAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghL14oWzEtOV0oXFwuXFxkezEsM30pP3woMFxcLlxcZHsxLDN9KSkkLy50ZXN0KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhMYW5ndWFnZS5lbihcIlRoZSB2YWx1ZSBtdXN0IGJlIGluIHJhbmdlIG9mICgwLCAxMCkgbGl0cmVzLCBhbmQgbWF4aW11bSAzIGRlY2ltYWxzIGFyZSBhbGxvd2VkXCIpLnRoYWkoXCLguITguYjguLLguIjguLDguJXguYnguK3guIfguK3guKLguLnguYjguYPguJnguIrguYjguKfguIcgKDAsIDEwKSDguKXguLTguJXguKPguYHguKXguLDguKrguLnguIfguKrguLjguJQgMyDguJfguKjguJnguLTguKLguKFcIikuZ2V0TWVzc2FnZSgpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgIF19XG4gICAgICAgICAgICAgICAgICAgICBvbkJsdXI9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHsgW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImNhcGFjaXR5XCIpXTogdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjYXBhY2l0eVwiKSkgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS52YWxpZGF0ZUZpZWxkcyhbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY2FwYWNpdHlcIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgIG9uU2VhcmNoPXt2YWx1ZSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgIC8vVE9ET1xuICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXZhbHVlKSByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKHZhbHVlLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjYXBhY2l0eVwiKSk7XG4gICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17XG4gICAgICAgICAgICAgICAgICAgICAgICh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBsYXRmb3JtVmVoaWNsZU1vZGVsLmV4dC5jYXBhY2l0aWVzXCIpKSB8fCBbXSkubWFwKChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4geyBpZDogaXRlbSwgdGV4dDogaXRlbSB9O1xuICAgICAgICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiLz5cblxuXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHZlaGljbGVDb2RlT2ZUb25uYWdlLmluY2x1ZGVzKHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidmVoaWNsZUNvZGVcIikpKSAmJiAoXG4gICAgICAgICAgICAgICAgPE5TZWxlY3QgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiV2VpZ2h0KFQpXCIpLnRoYWkoXCLguJnguYnguLPguKvguJnguLHguIEgKFQpXCIpLm15KFwi4YCh4YCc4YCx4YC44YCB4YC74YCt4YCU4YC6IChUKSDhgIDhgK3hgK9cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJ0b25uYWdlXCIpfVxuICAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgIG5vdEZvdW5kQ29udGVudD1cIlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgc2hvd1NlYXJjaFxuICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcihydWxlOiBhbnksIHZhbHVlOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAhL14oXFxkezJ9KFxcLlxcZHsxLDN9KT98KFsxLTldKFxcLlxcZHsxLDN9KT98KDBcXC5cXGR7MSwzfSkpKSQvLnRlc3QoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soTGFuZ3VhZ2UuZW4oXCJUaGUgdmFsdWUgbXVzdCBiZSBpbiByYW5nZSBvZiAoMCwgMTAwKSBsaXRyZXMsIGFuZCBtYXhpbXVtIDMgZGVjaW1hbHMgYXJlIGFsbG93ZWRcIikudGhhaShcIuC4hOC5iOC4suC4leC5ieC4reC4h+C4reC4ouC4ueC5iOC5g+C4meC4iuC5iOC4p+C4hyAoMCwgMTAwKSDguKXguLTguJXguKPguYHguKXguLDguKrguLnguIfguKrguLjguJQgMyDguJfguKjguJnguLTguKLguKFcIikuZ2V0TWVzc2FnZSgpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgIF19XG4gICAgICAgICAgICAgICAgICAgICAgICAgb25CbHVyPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoeyBbdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidG9ubmFnZVwiKV06IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwidG9ubmFnZVwiKSkgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoW3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInRvbm5hZ2VcIildLCB7IGZvcmNlOiB0cnVlIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgb25TZWFyY2g9e3ZhbHVlID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vVE9ET1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCF2YWx1ZSkgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodmFsdWUsIHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInRvbm5hZ2VcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwbGF0Zm9ybVZlaGljbGVNb2RlbC5leHQud2VpZ2h0c1wiKSkgfHwgW10pLm1hcCgoaXRlbTogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7IGlkOiBpdGVtLCB0ZXh0OiBpdGVtIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIi8+XG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICB2ZWhpY2xlQ29kZU9mU2VhdC5pbmNsdWRlcyh0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInZlaGljbGVDb2RlXCIpKSkgJiYgKFxuICAgICAgICAgICAgICAgIDxOU2VsZWN0IGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIk5vLiBvZiBTZWF0XCIpLnRoYWkoXCLguIjguLPguJnguKfguJnguJfguLXguYjguJnguLHguYjguIdcIikubXkoXCLhgJHhgK3hgK/hgIThgLrhgIHhgK/hgLbhgY/hgKHhgJnhgL7hgJDhgLpcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJub09mU2VhdFwiKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBub3RGb3VuZENvbnRlbnQ9XCJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dTZWFyY2hcbiAgICAgICAgICAgICAgICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IocnVsZTogYW55LCB2YWx1ZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCEvXihcXGR7Mn0/fFsxLTldKSQvLnRlc3QodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhMYW5ndWFnZS5lbihcIlRoZSB2YWx1ZSBtdXN0IGJlIGluIHJhbmdlIG9mICgwLCAxMDApIGxpdHJlc1wiKS50aGFpKFwi4LiE4LmI4Liy4LiI4Liw4LiV4LmJ4Lit4LiH4Lit4Lii4Li54LmI4LmD4LiZ4LiK4LmI4Lin4LiHICgwLCAxMDApIOC4peC4tOC4leC4o1wiKS5nZXRNZXNzYWdlKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBvbkJsdXI9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7IFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJub09mU2VhdFwiKV06IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibm9PZlNlYXRcIikpIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnZhbGlkYXRlRmllbGRzKFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJub09mU2VhdFwiKV0sIHsgZm9yY2U6IHRydWUgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBvblNlYXJjaD17dmFsdWUgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9UT0RPXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXZhbHVlKSByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbCh2YWx1ZSwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibm9PZlNlYXRcIikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAodGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwbGF0Zm9ybVZlaGljbGVNb2RlbC5leHQuc2VhdHNcIikpIHx8IFtdKS5tYXAoKGl0ZW06IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4geyBpZDogaXRlbSwgdGV4dDogaXRlbSB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCIvPlxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIDxGaWVsZEdyb3VwIGNsYXNzTmFtZT1cInllYXItZ3JvdXBcIlxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiUmVnaXN0cmF0aW9uIFllYXJcIikudGhhaShcIuC4m+C4teC4l+C4teC5iOC4iOC4lOC4l+C4sOC5gOC4muC4teC4ouC4mVwiKS5teShcIlJFR0lTVFJBVElPTiBZRUFSIOGAkOGAveGAhOGAulwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RYc1NtPXt7IHhzOiA4LCBzbTogNiB9fSB0ZXh0WHNTbT17eyB4czogMTYsIHNtOiAxMyB9fT5cbiAgICAgICAgICAgICAgPE5TZWxlY3QgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicmVnWWVhclwiKX1cbiAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0Q29sPXtmb3JtR3JvdXBTZWxlY3RMYXlvdXR9XG4gICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJSZWdpc3RyYXRpb24gWWVhclwiKS50aGFpKFwi4Lib4Li14LiX4Li14LmI4LiI4LiU4LiX4Liw4LmA4Lia4Li14Lii4LiZXCIpLm15KFwiUkVHSVNUUkFUSU9OIFlFQVIg4YCQ4YC94YCE4YC6XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiaXNOZXdWZWhpY2xlXCIpKSA9PT0gXCJZXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e1xuICAgICAgICAgICAgICAgICAgICAgICAgIG5ldyBBcnJheSgyNSkuZmlsbCgwKS5tYXAoKF8sIGluZGV4KSA9PiAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHRoaXMuY3VycmVudFllYXIgLSBpbmRleCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6ICh0aGlzLmN1cnJlbnRZZWFyIC0gaW5kZXgpLnRvU3RyaW5nKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgfSkpfVxuICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJncm91cC1zZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHJlZ1llYXI6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IG1hcmtldFZhbHVlcyA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicGxhdGZvcm1WZWhpY2xlTW9kZWwuZXh0Lm1hcmtldFZhbHVlc1wiKSkgfHwgW107XG4gICAgICAgICAgICAgICAgICAgICAgICAgbGV0IG1hcmtldFZhbHVlID0gKG1hcmtldFZhbHVlcy5maW5kKChpdGVtOiBhbnkpID0+IHBhcnNlSW50KGl0ZW0ueWVhcnMpID09PSB0aGlzLmN1cnJlbnRZZWFyIC0gcmVnWWVhcikgfHwge30pLm1hcmtldFZhbHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVUb01vZGVsKG1hcmtldFZhbHVlLCB0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJtYXJrZXRWYWx1ZVwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxOQ2hlY2tib3ggZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlzTmV3VmVoaWNsZVwiKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXRDb2w9e2Zvcm1Hcm91cFNlbGVjdExheW91dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJOZXcgVmVoaWNsZVwiKS50aGFpKFwi4Lij4LiW4LmD4Lir4Lih4LmIXCIpLm15KFwi4YCa4YCs4YCJ4YC64YCe4YCF4YC6XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXt0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcImlzTmV3VmVoaWNsZVwiKSkgPT09IFwiWVwifVxuICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoY2hlY2tlZDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2hlY2tlZCA9PT0gXCJZXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodGhpcy5jdXJyZW50WWVhciwgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicmVnWWVhclwiKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvcHMuZm9ybS5zZXRGaWVsZHNWYWx1ZSh7IFt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJyZWdZZWFyXCIpXTogdGhpcy5jdXJyZW50WWVhciB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICAgICAgICA8TlNlbGVjdCBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICAgcHJvcE5hbWU9e3RoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInZlaGljbGVQcm92aW5jZVwiKX1cbiAgICAgICAgICAgICAgICAgICAgIHRhYmxlTmFtZT1cInZlaGljbGVwcm92aW5jZVwiXG4gICAgICAgICAgICAgICAgICAgICBpc0NvbXBhcmU9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgICBkYXRhRml4ZWQ9e1wicG9saWN5XCJ9XG4gICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJSZWdpc3RyYXRpb24gUHJvdmluY2VcIikudGhhaShcIuC4iOC4seC4h+C4q+C4p+C4seC4lOC4l+C4teC5iOC4peC4h+C4l+C4sOC5gOC4muC4teC4ouC4mVwiKS5teShcIlJFR0lTVFJBVElPTiDhgJXhgLzhgIrhgLrhgJThgJrhgLpcIikuZ2V0TWVzc2FnZSgpfS8+XG4gICAgICAgICAgPC9OUGFuZWw+XG5cbiAgICAgICAgICA8TlBhbmVsIGtleT1cIjJcIlxuICAgICAgICAgICAgICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkluc3VyYW5jZSBQcmVmZXJlbmNlc1wiKS50aGFpKFwi4LiB4Liy4Lij4LiV4Lix4LmJ4LiH4LiE4LmI4Liy4Lib4Lij4Liw4LiB4Lix4LiZ4Lig4Lix4LiiXCIpLm15KFwi4YCh4YCs4YCZ4YCB4YC24YCh4YCK4YC94YC+4YCU4YC64YC44YCZ4YC74YCs4YC4XCIpLmdldE1lc3NhZ2UoKX0+XG5cbiAgICAgICAgICAgIDxOUHJpY2UgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwibWFya2V0VmFsdWVcIil9XG4gICAgICAgICAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgICAgICAgICAgIHJ1bGVzPXtbXG4gICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yKHJ1bGU6IGFueSwgdmFsdWU6IGFueSwgY2FsbGJhY2s6IGFueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIVJ1bGVzLmlzWE11bHRpcGx5KHZhbHVlLCAxMDAwMCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhMYW5ndWFnZS5lbihcIk9EIFN1bSBJbnN1cmVkIGNhbiBvbmx5IGJlIGluIDEwLDAwMHNcIikudGhhaShcIuC4iOC4s+C4meC4p+C4meC5gOC4h+C4tOC4meC5gOC4reC4suC4m+C4o+C4sOC4geC4seC4meC4oOC4seC4oiBPRCDguKrguLLguKHguLLguKPguJbguJfguLPguYTguJTguYnguYPguJkgMTAsMDAwIOC5gOC4l+C5iOC4suC4meC4seC5ieC4mVwiKS5nZXRNZXNzYWdlKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiT0QgU3VtIEluc3VyZWRcIikudGhhaShcIuC4l+C4uOC4meC4m+C4o+C4sOC4geC4seC4meC4o+C4luC4ouC4meC4leC5jFwiKS5teShcIuGAleGAseGAq+GAhOGAuuGAuOGAnOGAkuGAuuGAoeGArOGAmeGAgeGAtuGAkeGArOGAuFwiKS5nZXRNZXNzYWdlKCl9Lz5cblxuICAgICAgICAgICAgPE5Td2l0Y2ggZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJjYW1lcmFJbnN0YWxsZWRcIil9XG4gICAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJDYW1lcmEgSW5zdGFsbGVkP1wiKS50aGFpKFwi4Lih4Li14LiB4Lil4LmJ4Lit4LiH4LiV4Li04LiU4Lij4LiW4Lii4LiZ4LiV4LmM4Lir4Lij4Li34Lit4LmE4Lih4LmIP1wiKS5teShcIuGAgOGAhOGAuuGAmeGAm+GArOGAgOGAreGAr+GAkOGAleGAuuGAhuGAhOGAuj9cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdENoZWNrZWQ9e3RoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwiY2FtZXJhSW5zdGFsbGVkXCIpKSA9PT0gXCJZXCJ9XG4gICAgICAgICAgICAvPlxuXG4gICAgICAgICAgPC9OUGFuZWw+XG5cbiAgICAgICAgICA8TlBhbmVsIGtleT1cIjNcIiBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiUHJvbW90aW9uXCIpLnRoYWkoXCLguIHguLLguKPguKrguYjguIfguYDguKrguKPguLTguKFcIikubXkoXCLhgJnhgLzhgL7hgIThgLrhgLfhgJDhgIThgLrhgJvhgLHhgLhcIikuZ2V0TWVzc2FnZSgpfT5cblxuICAgICAgICAgICAgPE5UZXh0IGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcE5hbWUoXCJwcm9tb0NvZGVcIil9XG4gICAgICAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQcm9tbyBDb2RlXCIpLnRoYWkoXCLguKPguKvguLHguKrguYLguJvguKPguYLguKHguIrguLHguYjguJlcIikubXkoXCLhgIDhgLzhgLHhgKzhgLrhgIThgLzhgKzhgIDhgK/hgJLhgLpcIikuZ2V0TWVzc2FnZSgpfS8+XG5cbiAgICAgICAgICA8L05QYW5lbD5cbiAgICAgICAgPC9OQ29sbGFwc2U+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclRpdGxlKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgIHtMYW5ndWFnZS5lbihcIk5ldyBRdW90ZSAtIFZvbHVudGFyeSBNb3RvclwiKS50aGFpKFwi4LiV4Lij4Lin4LiI4Liq4Lit4Lia4LmA4Lia4Li14LmJ4Lii4Lib4Lij4Liw4LiB4Lix4LiZ4Lij4LiW4Lii4LiZ4LiV4LmMXCIpLm15KFwi4YCh4YCe4YCF4YC64YCA4YCA4YCt4YCv4YC44YCA4YCs4YC4IC0g4YCZ4YCt4YCZ4YCt4YCG4YCU4YC54YCS4YCh4YCc4YC74YCx4YCs4YCA4YC64YCZ4YCx4YCs4YC64YCQ4YCs4YCh4YCs4YCZ4YCB4YC2XCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IHsgUXVvdGVzIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBVUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFnQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7QUE5REE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBOzs7QUFrQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUEzQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNEJBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBVUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQS9CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFvQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFjQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBbkNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUEvQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBbUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQWxCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFYQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFVQTtBQWZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBU0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUF2VkE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/quote/compare/vmi/quote.tsx
