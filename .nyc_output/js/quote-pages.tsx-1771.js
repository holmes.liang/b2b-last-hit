__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return QuotePages; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_route_common_route__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/route/common-route */ "./src/common/route/common-route.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/quote-pages.tsx";




var QuotePages =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(QuotePages, _React$Component);

  function QuotePages(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, QuotePages);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(QuotePages).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(QuotePages, [{
    key: "render",
    value: function render() {
      var Result = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].lazy(function () {
        return Promise.all(/*! import() */[__webpack_require__.e("compare-plan1853~._src_app_desk_c"), __webpack_require__.e("compare-plan1855~._src_app_desk_quote_components_success_components_policy-item.tsx~31cd5398"), __webpack_require__.e("compare-plan1857~._src_app_desk_quote_components_success_index.tsx~7527f938")]).then(__webpack_require__.bind(null, /*! ./components/success/index */ "./src/app/desk/quote/components/success/index.tsx"));
      });
      var countryCode = _common__WEBPACK_IMPORTED_MODULE_6__["Utils"].getCountry();
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Switch"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 15
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].QUOTE_NORMAL_VIEW,
        render: function render(_ref) {
          var match = _ref.match;
          return Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_7__["renderPolicyView"])(match);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].NORMAL_QUOTE_CONTINUE,
        render: function render(_ref2) {
          var match = _ref2.match;
          return Object(_common_route_common_route__WEBPACK_IMPORTED_MODULE_7__["renderPolicyContinue"])(match);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: [_common__WEBPACK_IMPORTED_MODULE_6__["PATH"].COMPARE_ENDO_CONTINUE, _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].COMPARE_QUOTE_CONTINUE, _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].COMPARE_QUOTE],
        render: function render(_ref3) {
          var match = _ref3.match;
          var _match$params = match.params,
              policyId = _match$params.policyId,
              paymentStatus = _match$params.paymentStatus,
              bizType = _match$params.bizType,
              endoId = _match$params.endoId;
          var _match$params2 = match.params,
              prdtCode = _match$params2.prdtCode,
              productCode = _match$params2.productCode; // const clientCompanyCode = Storage.Account.session().get(Consts.CLIENT_COMPANY_CODE);

          prdtCode = prdtCode.toLowerCase();
          if (["enel", "hprt"].includes(prdtCode)) prdtCode = "wlsc";
          if (["gpc"].includes(prdtCode)) prdtCode = "vmi"; //if client compant code is empty or 'BF', then routing to base line quote compare page
          // let componentPath = (Utils.isEmpty(clientCompanyCode) || clientCompanyCode === "BF") ?
          //   `./compare/${prdtCode}` : `./compare/${clientCompanyCode.toLowerCase()}/${prdtCode}`;

          var isSuretyArr = ["fwb", "lb", "rb", "sb", "pb"].includes(prdtCode);
          var isEbArr = ["wicun", "wicn"].includes(prdtCode);
          var isLiaArr = ["publ", "pi", "pl", "dol"].includes(prdtCode);
          var componentPath;

          if (countryCode === "SGP" && prdtCode === "vmi") {
            componentPath = "./compare/sgp/".concat(prdtCode);
          } else if (isSuretyArr) {
            componentPath = "./SAIC/surety";
          } else if (isEbArr) {
            componentPath = "./SAIC/eb";
          } else if (isLiaArr) {
            componentPath = "./SAIC/liability";
          } else {
            componentPath = "./compare/".concat(prdtCode);
          }

          var CompareQuotePage = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].lazy(function () {
            return __webpack_require__("./src/app/desk/quote lazy recursive ^.*$")("".concat(componentPath));
          });
          return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(CompareQuotePage, {
            identity: {
              policyId: policyId,
              prdtCode: prdtCode,
              paymentStatus: paymentStatus,
              bizType: bizType,
              endoId: endoId
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 53
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].QUOTE_RESULT,
        render: function render() {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(Result, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 56
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].NORMAL_QUOTE,
        render: function render(_ref4) {
          var match = _ref4.match;
          var _match$params3 = match.params,
              itntCode = _match$params3.itntCode,
              prdtCode = _match$params3.prdtCode,
              bizType = _match$params3.bizType,
              policyId = _match$params3.policyId,
              paymentStatus = _match$params3.paymentStatus;
          var isTccArr = ["SARP", "TCC", "ARPKC", "TWPS"].includes(prdtCode);
          var isSuretyArr = ["FWB", "LB", "RB", "SB", "PB"].includes(prdtCode);
          var isLife = ["FMCA", "ENEL", "HPRT", "WLSC"].includes(prdtCode);
          var isEbArr = ["WICUN", "WICN"].includes(prdtCode);
          var isLiaArr = ["PUBL", "PI", "PL", "DOL"].includes(prdtCode);
          var componentPath;

          if (isTccArr) {
            componentPath = "./".concat(itntCode, "/tcc");
          } else if (isLife) {
            componentPath = "./".concat(itntCode, "/wlsc");
          } else if (isSuretyArr) {
            componentPath = "./".concat(itntCode, "/surety");
          } else if (isEbArr) {
            componentPath = "./".concat(itntCode, "/eb");
          } else if (isLiaArr) {
            componentPath = "./".concat(itntCode, "/liability");
          } else if (prdtCode === "GPC") {
            componentPath = "./".concat(itntCode, "/sgp/").concat(prdtCode.toLowerCase());
          } else {
            componentPath = "./".concat(itntCode, "/").concat(prdtCode.toLowerCase());
          }

          var NormalQuotePage = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].lazy(function () {
            return __webpack_require__("./src/app/desk/quote lazy recursive ^.*$")("".concat(componentPath));
          });
          return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(NormalQuotePage, {
            identity: {
              itntCode: itntCode,
              policyId: policyId,
              prdtCode: prdtCode,
              paymentStatus: paymentStatus,
              bizType: bizType
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 83
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["Route"], {
        path: _common__WEBPACK_IMPORTED_MODULE_6__["PATH"].PLAN_NORMAL,
        render: function render(_ref5) {
          var match = _ref5.match;
          var _match$params4 = match.params,
              itntCode = _match$params4.itntCode,
              prdtCode = _match$params4.prdtCode;
          var componentPath;
          var prdtArr = ["TC", "SURETY", "EB", "LIABILITY"];

          if (prdtArr.includes(prdtCode)) {
            componentPath = "./".concat(itntCode, "/tc/plan");
          } else {
            componentPath = "./".concat(itntCode, "/").concat(prdtCode.toLowerCase(), "/plan");
          }

          var NormalQuotePage = _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].lazy(function () {
            return __webpack_require__("./src/app/desk/quote lazy recursive ^.*$")("".concat(componentPath));
          });
          return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(NormalQuotePage, {
            identity: {
              itntCode: itntCode,
              prdtCode: prdtCode
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 97
            },
            __self: this
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }));
    }
  }]);

  return QuotePages;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].Component);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVvdGUtcGFnZXMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvcXVvdGUtcGFnZXMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBSb3V0ZSwgU3dpdGNoIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBQQVRILCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyByZW5kZXJQb2xpY3lDb250aW51ZSwgcmVuZGVyUG9saWN5VmlldyB9IGZyb20gXCJAY29tbW9uL3JvdXRlL2NvbW1vbi1yb3V0ZVwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBRdW90ZVBhZ2VzPFAsIFMsIEM+IGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBSZXN1bHQgPSBSZWFjdC5sYXp5KCgpID0+IGltcG9ydChcIi4vY29tcG9uZW50cy9zdWNjZXNzL2luZGV4XCIpKTtcblxuICAgIGNvbnN0IGNvdW50cnlDb2RlID0gVXRpbHMuZ2V0Q291bnRyeSgpO1xuICAgIHJldHVybiAoXG4gICAgICA8U3dpdGNoPlxuICAgICAgICA8Um91dGUgcGF0aD17UEFUSC5RVU9URV9OT1JNQUxfVklFV30gcmVuZGVyPXsoeyBtYXRjaCB9KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIHJlbmRlclBvbGljeVZpZXcobWF0Y2gpO1xuICAgICAgICB9fS8+XG4gICAgICAgIDxSb3V0ZSBwYXRoPXtQQVRILk5PUk1BTF9RVU9URV9DT05USU5VRX0gcmVuZGVyPXsoeyBtYXRjaCB9KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIHJlbmRlclBvbGljeUNvbnRpbnVlKG1hdGNoKTtcbiAgICAgICAgfX0vPlxuICAgICAgICA8Um91dGVcbiAgICAgICAgICBwYXRoPXtbUEFUSC5DT01QQVJFX0VORE9fQ09OVElOVUUsIFBBVEguQ09NUEFSRV9RVU9URV9DT05USU5VRSwgUEFUSC5DT01QQVJFX1FVT1RFXX1cbiAgICAgICAgICByZW5kZXI9eyh7IG1hdGNoIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcG9saWN5SWQsIHBheW1lbnRTdGF0dXMsIGJpelR5cGUsIGVuZG9JZCB9ID0gbWF0Y2gucGFyYW1zO1xuICAgICAgICAgICAgbGV0IHsgcHJkdENvZGUsIHByb2R1Y3RDb2RlIH0gPSBtYXRjaC5wYXJhbXM7XG4gICAgICAgICAgICAvLyBjb25zdCBjbGllbnRDb21wYW55Q29kZSA9IFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5DTElFTlRfQ09NUEFOWV9DT0RFKTtcbiAgICAgICAgICAgIHByZHRDb2RlID0gcHJkdENvZGUudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIGlmIChbXCJlbmVsXCIsIFwiaHBydFwiXS5pbmNsdWRlcyhwcmR0Q29kZSkpIHByZHRDb2RlID0gXCJ3bHNjXCI7XG4gICAgICAgICAgICBpZiAoW1wiZ3BjXCJdLmluY2x1ZGVzKHByZHRDb2RlKSkgcHJkdENvZGUgPSBcInZtaVwiO1xuICAgICAgICAgICAgLy9pZiBjbGllbnQgY29tcGFudCBjb2RlIGlzIGVtcHR5IG9yICdCRicsIHRoZW4gcm91dGluZyB0byBiYXNlIGxpbmUgcXVvdGUgY29tcGFyZSBwYWdlXG4gICAgICAgICAgICAvLyBsZXQgY29tcG9uZW50UGF0aCA9IChVdGlscy5pc0VtcHR5KGNsaWVudENvbXBhbnlDb2RlKSB8fCBjbGllbnRDb21wYW55Q29kZSA9PT0gXCJCRlwiKSA/XG4gICAgICAgICAgICAvLyAgIGAuL2NvbXBhcmUvJHtwcmR0Q29kZX1gIDogYC4vY29tcGFyZS8ke2NsaWVudENvbXBhbnlDb2RlLnRvTG93ZXJDYXNlKCl9LyR7cHJkdENvZGV9YDtcbiAgICAgICAgICAgIGNvbnN0IGlzU3VyZXR5QXJyID0gW1wiZndiXCIsIFwibGJcIiwgXCJyYlwiLCBcInNiXCIsIFwicGJcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgICAgICAgICAgY29uc3QgaXNFYkFyciA9IFtcIndpY3VuXCIsIFwid2ljblwiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgICBjb25zdCBpc0xpYUFyciA9IFtcInB1YmxcIiwgXCJwaVwiLCBcInBsXCIsIFwiZG9sXCJdLmluY2x1ZGVzKHByZHRDb2RlKTtcblxuICAgICAgICAgICAgbGV0IGNvbXBvbmVudFBhdGg6IGFueTtcbiAgICAgICAgICAgIGlmIChjb3VudHJ5Q29kZSA9PT0gXCJTR1BcIiAmJiBwcmR0Q29kZSA9PT0gXCJ2bWlcIikge1xuICAgICAgICAgICAgICBjb21wb25lbnRQYXRoID0gYC4vY29tcGFyZS9zZ3AvJHtwcmR0Q29kZX1gO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChpc1N1cmV0eUFycikge1xuICAgICAgICAgICAgICBjb21wb25lbnRQYXRoID0gYC4vU0FJQy9zdXJldHlgO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChpc0ViQXJyKSB7XG4gICAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi9TQUlDL2ViYDtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoaXNMaWFBcnIpIHtcbiAgICAgICAgICAgICAgY29tcG9uZW50UGF0aCA9IGAuL1NBSUMvbGlhYmlsaXR5YDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi9jb21wYXJlLyR7cHJkdENvZGV9YDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IENvbXBhcmVRdW90ZVBhZ2UgPSBSZWFjdC5sYXp5KCgpID0+XG4gICAgICAgICAgICAgIGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiBcImNvbXBhcmUtcXVvdGVcIiAqLyBgJHtjb21wb25lbnRQYXRofWApLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIHJldHVybiA8Q29tcGFyZVF1b3RlUGFnZSBpZGVudGl0eT17eyBwb2xpY3lJZCwgcHJkdENvZGUsIHBheW1lbnRTdGF0dXMsIGJpelR5cGUsIGVuZG9JZCB9fS8+O1xuICAgICAgICAgIH19XG4gICAgICAgIC8+XG4gICAgICAgIDxSb3V0ZSBwYXRoPXtQQVRILlFVT1RFX1JFU1VMVH0gcmVuZGVyPXsoKSA9PiA8UmVzdWx0Lz59Lz5cblxuICAgICAgICA8Um91dGUgcGF0aD17UEFUSC5OT1JNQUxfUVVPVEV9IHJlbmRlcj17KHsgbWF0Y2ggfSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgaXRudENvZGUsIHByZHRDb2RlLCBiaXpUeXBlLCBwb2xpY3lJZCwgcGF5bWVudFN0YXR1cyB9ID0gbWF0Y2gucGFyYW1zO1xuICAgICAgICAgIGNvbnN0IGlzVGNjQXJyID0gW1wiU0FSUFwiLCBcIlRDQ1wiLCBcIkFSUEtDXCIsIFwiVFdQU1wiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgY29uc3QgaXNTdXJldHlBcnIgPSBbXCJGV0JcIiwgXCJMQlwiLCBcIlJCXCIsIFwiU0JcIiwgXCJQQlwiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgY29uc3QgaXNMaWZlID0gW1wiRk1DQVwiLCBcIkVORUxcIiwgXCJIUFJUXCIsIFwiV0xTQ1wiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgY29uc3QgaXNFYkFyciA9IFtcIldJQ1VOXCIsIFwiV0lDTlwiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgY29uc3QgaXNMaWFBcnIgPSBbXCJQVUJMXCIsIFwiUElcIiwgXCJQTFwiLCBcIkRPTFwiXS5pbmNsdWRlcyhwcmR0Q29kZSk7XG4gICAgICAgICAgbGV0IGNvbXBvbmVudFBhdGg6IGFueTtcbiAgICAgICAgICBpZiAoaXNUY2NBcnIpIHtcbiAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi8ke2l0bnRDb2RlfS90Y2NgO1xuICAgICAgICAgIH0gZWxzZSBpZiAoaXNMaWZlKSB7XG4gICAgICAgICAgICBjb21wb25lbnRQYXRoID0gYC4vJHtpdG50Q29kZX0vd2xzY2A7XG4gICAgICAgICAgfSBlbHNlIGlmIChpc1N1cmV0eUFycikge1xuICAgICAgICAgICAgY29tcG9uZW50UGF0aCA9IGAuLyR7aXRudENvZGV9L3N1cmV0eWA7XG4gICAgICAgICAgfSBlbHNlIGlmIChpc0ViQXJyKSB7XG4gICAgICAgICAgICBjb21wb25lbnRQYXRoID0gYC4vJHtpdG50Q29kZX0vZWJgO1xuICAgICAgICAgIH0gZWxzZSBpZiAoaXNMaWFBcnIpIHtcbiAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi8ke2l0bnRDb2RlfS9saWFiaWxpdHlgO1xuICAgICAgICAgIH0gZWxzZSBpZiAocHJkdENvZGUgPT09IFwiR1BDXCIpIHtcbiAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi8ke2l0bnRDb2RlfS9zZ3AvJHtwcmR0Q29kZS50b0xvd2VyQ2FzZSgpfWA7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbXBvbmVudFBhdGggPSBgLi8ke2l0bnRDb2RlfS8ke3ByZHRDb2RlLnRvTG93ZXJDYXNlKCl9YDtcbiAgICAgICAgICB9XG4gICAgICAgICAgY29uc3QgTm9ybWFsUXVvdGVQYWdlID0gUmVhY3QubGF6eSgoKSA9PlxuICAgICAgICAgICAgaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6IFwiY29tcGFyZS1xdW90ZVwiICovIGAke2NvbXBvbmVudFBhdGh9YCkpO1xuICAgICAgICAgIHJldHVybiA8Tm9ybWFsUXVvdGVQYWdlIGlkZW50aXR5PXt7IGl0bnRDb2RlLCBwb2xpY3lJZCwgcHJkdENvZGUsIHBheW1lbnRTdGF0dXMsIGJpelR5cGUgfX0vPjtcbiAgICAgICAgfX0vPlxuXG4gICAgICAgIDxSb3V0ZSBwYXRoPXtQQVRILlBMQU5fTk9STUFMfSByZW5kZXI9eyh7IG1hdGNoIH0pID0+IHtcbiAgICAgICAgICBjb25zdCB7IGl0bnRDb2RlLCBwcmR0Q29kZSB9ID0gbWF0Y2gucGFyYW1zO1xuICAgICAgICAgIGxldCBjb21wb25lbnRQYXRoOiBhbnk7XG4gICAgICAgICAgbGV0IHByZHRBcnIgPSBbXCJUQ1wiLCBcIlNVUkVUWVwiLCBcIkVCXCIsIFwiTElBQklMSVRZXCJdO1xuICAgICAgICAgIGlmIChwcmR0QXJyLmluY2x1ZGVzKHByZHRDb2RlKSkge1xuICAgICAgICAgICAgY29tcG9uZW50UGF0aCA9IGAuLyR7aXRudENvZGV9L3RjL3BsYW5gO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb21wb25lbnRQYXRoID0gYC4vJHtpdG50Q29kZX0vJHtwcmR0Q29kZS50b0xvd2VyQ2FzZSgpfS9wbGFuYDtcbiAgICAgICAgICB9XG4gICAgICAgICAgY29uc3QgTm9ybWFsUXVvdGVQYWdlID0gUmVhY3QubGF6eSgoKSA9PlxuICAgICAgICAgICAgaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6IFwiY29tcGFyZS1wbGFuXCIgKi8gYCR7Y29tcG9uZW50UGF0aH1gKSk7XG4gICAgICAgICAgcmV0dXJuIDxOb3JtYWxRdW90ZVBhZ2UgaWRlbnRpdHk9e3sgaXRudENvZGUsIHByZHRDb2RlIH19Lz47XG4gICAgICAgIH19Lz5cblxuICAgICAgPC9Td2l0Y2g+XG4gICAgKTtcblxuICB9XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQSxtRkFDQTtBQURBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBaENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWtDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBLG1GQUNBO0FBREE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUExQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUEsbUZBQ0E7QUFEQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWlCQTs7OztBQWxHQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/quote-pages.tsx
