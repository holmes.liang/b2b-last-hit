__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatTimeStr", function() { return formatTimeStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatCountdown", function() { return formatCountdown; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/padStart */ "./node_modules/lodash/padStart.js");
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_padStart__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _util_interopDefault__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_util/interopDefault */ "./node_modules/antd/es/_util/interopDefault.js");
function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

function _iterableToArrayLimit(arr, i) {
  if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) {
    return;
  }

  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}



 // Countdown

var timeUnits = [['Y', 1000 * 60 * 60 * 24 * 365], ['M', 1000 * 60 * 60 * 24 * 30], ['D', 1000 * 60 * 60 * 24], ['H', 1000 * 60 * 60], ['m', 1000 * 60], ['s', 1000], ['S', 1]];
function formatTimeStr(duration, format) {
  var leftDuration = duration;
  var escapeRegex = /\[[^\]]*\]/g;
  var keepList = (format.match(escapeRegex) || []).map(function (str) {
    return str.slice(1, -1);
  });
  var templateText = format.replace(escapeRegex, '[]');
  var replacedText = timeUnits.reduce(function (current, _ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        name = _ref2[0],
        unit = _ref2[1];

    if (current.indexOf(name) !== -1) {
      var value = Math.floor(leftDuration / unit);
      leftDuration -= value * unit;
      return current.replace(new RegExp("".concat(name, "+"), 'g'), function (match) {
        var len = match.length;
        return lodash_padStart__WEBPACK_IMPORTED_MODULE_1___default()(value.toString(), len, '0');
      });
    }

    return current;
  }, templateText);
  var index = 0;
  return replacedText.replace(escapeRegex, function () {
    var match = keepList[index];
    index += 1;
    return match;
  });
}
function formatCountdown(value, config) {
  var _config$format = config.format,
      format = _config$format === void 0 ? '' : _config$format;
  var target = Object(_util_interopDefault__WEBPACK_IMPORTED_MODULE_2__["default"])(moment__WEBPACK_IMPORTED_MODULE_0__)(value).valueOf();
  var current = Object(_util_interopDefault__WEBPACK_IMPORTED_MODULE_2__["default"])(moment__WEBPACK_IMPORTED_MODULE_0__)().valueOf();
  var diff = Math.max(target - current, 0);
  return formatTimeStr(diff, format);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zdGF0aXN0aWMvdXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3N0YXRpc3RpYy91dGlscy5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudCc7XG5pbXBvcnQgcGFkU3RhcnQgZnJvbSAnbG9kYXNoL3BhZFN0YXJ0JztcbmltcG9ydCBpbnRlcm9wRGVmYXVsdCBmcm9tICcuLi9fdXRpbC9pbnRlcm9wRGVmYXVsdCc7XG4vLyBDb3VudGRvd25cbmNvbnN0IHRpbWVVbml0cyA9IFtcbiAgICBbJ1knLCAxMDAwICogNjAgKiA2MCAqIDI0ICogMzY1XSxcbiAgICBbJ00nLCAxMDAwICogNjAgKiA2MCAqIDI0ICogMzBdLFxuICAgIFsnRCcsIDEwMDAgKiA2MCAqIDYwICogMjRdLFxuICAgIFsnSCcsIDEwMDAgKiA2MCAqIDYwXSxcbiAgICBbJ20nLCAxMDAwICogNjBdLFxuICAgIFsncycsIDEwMDBdLFxuICAgIFsnUycsIDFdLFxuXTtcbmV4cG9ydCBmdW5jdGlvbiBmb3JtYXRUaW1lU3RyKGR1cmF0aW9uLCBmb3JtYXQpIHtcbiAgICBsZXQgbGVmdER1cmF0aW9uID0gZHVyYXRpb247XG4gICAgY29uc3QgZXNjYXBlUmVnZXggPSAvXFxbW15cXF1dKlxcXS9nO1xuICAgIGNvbnN0IGtlZXBMaXN0ID0gKGZvcm1hdC5tYXRjaChlc2NhcGVSZWdleCkgfHwgW10pLm1hcChzdHIgPT4gc3RyLnNsaWNlKDEsIC0xKSk7XG4gICAgY29uc3QgdGVtcGxhdGVUZXh0ID0gZm9ybWF0LnJlcGxhY2UoZXNjYXBlUmVnZXgsICdbXScpO1xuICAgIGNvbnN0IHJlcGxhY2VkVGV4dCA9IHRpbWVVbml0cy5yZWR1Y2UoKGN1cnJlbnQsIFtuYW1lLCB1bml0XSkgPT4ge1xuICAgICAgICBpZiAoY3VycmVudC5pbmRleE9mKG5hbWUpICE9PSAtMSkge1xuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBNYXRoLmZsb29yKGxlZnREdXJhdGlvbiAvIHVuaXQpO1xuICAgICAgICAgICAgbGVmdER1cmF0aW9uIC09IHZhbHVlICogdW5pdDtcbiAgICAgICAgICAgIHJldHVybiBjdXJyZW50LnJlcGxhY2UobmV3IFJlZ0V4cChgJHtuYW1lfStgLCAnZycpLCAobWF0Y2gpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBsZW4gPSBtYXRjaC5sZW5ndGg7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHBhZFN0YXJ0KHZhbHVlLnRvU3RyaW5nKCksIGxlbiwgJzAnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjdXJyZW50O1xuICAgIH0sIHRlbXBsYXRlVGV4dCk7XG4gICAgbGV0IGluZGV4ID0gMDtcbiAgICByZXR1cm4gcmVwbGFjZWRUZXh0LnJlcGxhY2UoZXNjYXBlUmVnZXgsICgpID0+IHtcbiAgICAgICAgY29uc3QgbWF0Y2ggPSBrZWVwTGlzdFtpbmRleF07XG4gICAgICAgIGluZGV4ICs9IDE7XG4gICAgICAgIHJldHVybiBtYXRjaDtcbiAgICB9KTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBmb3JtYXRDb3VudGRvd24odmFsdWUsIGNvbmZpZykge1xuICAgIGNvbnN0IHsgZm9ybWF0ID0gJycgfSA9IGNvbmZpZztcbiAgICBjb25zdCB0YXJnZXQgPSBpbnRlcm9wRGVmYXVsdChtb21lbnQpKHZhbHVlKS52YWx1ZU9mKCk7XG4gICAgY29uc3QgY3VycmVudCA9IGludGVyb3BEZWZhdWx0KG1vbWVudCkoKS52YWx1ZU9mKCk7XG4gICAgY29uc3QgZGlmZiA9IE1hdGgubWF4KHRhcmdldCAtIGN1cnJlbnQsIDApO1xuICAgIHJldHVybiBmb3JtYXRUaW1lU3RyKGRpZmYsIGZvcm1hdCk7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/statistic/utils.js
