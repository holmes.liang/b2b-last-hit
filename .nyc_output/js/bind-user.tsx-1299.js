__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formItemLayout", function() { return formItemLayout; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/login/components/bind-user.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_2__["default"])(["\n      \n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var formItemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 13
    }
  }
};
var itemLayout = {
  labelCol: {
    xs: {
      span: 8
    },
    sm: {
      span: 6
    }
  },
  wrapperCol: {
    xs: {
      span: 16
    },
    sm: {
      span: 24
    }
  }
};

var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_9__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].getIsMobile();

var BindUser =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(BindUser, _ModelWidget);

  function BindUser(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, BindUser);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(BindUser).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(BindUser, [{
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(BindUser.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxContent: _common_3rd__WEBPACK_IMPORTED_MODULE_13__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {} // render

  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(C.BoxContent, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_15__["NText"], {
        model: model,
        form: form,
        propName: "userName",
        required: true,
        size: "large",
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("user name").thai("user name").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_12__["NFormItem"], {
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Password").thai("Password").getMessage(),
        model: model,
        form: form,
        propName: "pwd",
        required: true,
        rules: [{
          required: true,
          message: "Password is required"
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Input"].Password, {
        placeholder: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Password").thai("รหัสผ่านใหม่").getMessage(),
        autoComplete: "new-password",
        type: "password",
        size: "large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Col"], {
        xs: 8,
        sm: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Col"], {
        xs: 16,
        sm: 13,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_13__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Button"], {
        style: {
          width: "100%"
        },
        onClick: function onClick() {
          _this.bindAccount();
        },
        type: "primary",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, "Bind Now"))));
    } // actions

  }, {
    key: "bindAccount",
    value: function bindAccount() {
      var _this2 = this;

      this.props.form.validateFields(function (err, fieldsValue) {
        if (err) {
          return;
        }

        _this2.loginBind();
      });
    } // methods

  }, {
    key: "loginBind",
    value: function () {
      var _loginBind = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _this3 = this;

        var token, reToken, gmt, timeZone, loginModel;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                token = this.props.token;
                _context.next = 3;
                return _common__WEBPACK_IMPORTED_MODULE_11__["Utils"].tokenRecaptcha("Login");

              case 3:
                reToken = _context.sent;
                gmt = _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].GlobalParams.session().get("timeZone");

                if (gmt) {
                  _context.next = 10;
                  break;
                }

                _context.next = 8;
                return _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].get("/timezone");

              case 8:
                timeZone = _context.sent;
                _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].GlobalParams.session().set("timeZone", timeZone.body.respData);

              case 10:
                loginModel = lodash__WEBPACK_IMPORTED_MODULE_10___default.a.cloneDeep(this.props.model);
                _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].patch(_common__WEBPACK_IMPORTED_MODULE_11__["Apis"].LOGIN_QR_BIND.replace(":token", token), loginModel, {
                  headers: {
                    "recaptcha-token": reToken
                  }
                }).then(function (response) {
                  var respData = response.body.respData;

                  _this3.props.onCancel();

                  _this3.props.onSigninSuccess(respData);
                });

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function loginBind() {
        return _loginBind.apply(this, arguments);
      }

      return loginBind;
    }()
  }]);

  return BindUser;
}(_component__WEBPACK_IMPORTED_MODULE_12__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (antd__WEBPACK_IMPORTED_MODULE_14__["Form"].create()(BindUser));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svbG9naW4vY29tcG9uZW50cy9iaW5kLXVzZXIudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svbG9naW4vY29tcG9uZW50cy9iaW5kLXVzZXIudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlc1wiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgTGFuZ3VhZ2UsIFJ1bGVzLCBTdG9yYWdlLCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UsIE1vZGVsV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBGb3JtQ29tcG9uZW50UHJvcHMgfSBmcm9tIFwiYW50ZC9saWIvZm9ybVwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24sIENvbCwgRm9ybSwgSW5wdXQsIFJvdywgU3BpbiwgVXBsb2FkIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCBQcm9ncmVzc0RpYWxvZyBmcm9tIFwiQGRlc2svcXVvdGUvU0FJQy9lZmMvZGVjbGFyYXRpb24vY29tcG9uZW50cy9wcm9ncmVzcy1kaWFsb2dcIjtcbmltcG9ydCB7IE5UZXh0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5cbmV4cG9ydCBjb25zdCBmb3JtSXRlbUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTYgfSxcbiAgICBzbTogeyBzcGFuOiAxMyB9LFxuICB9LFxufTtcblxuY29uc3QgaXRlbUxheW91dCA9IHtcbiAgbGFiZWxDb2w6IHtcbiAgICB4czogeyBzcGFuOiA4IH0sXG4gICAgc206IHsgc3BhbjogNiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTYgfSxcbiAgICBzbTogeyBzcGFuOiAyNCB9LFxuICB9LFxufTtcbmNvbnN0IHsgQ09MT1JfQSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbnR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG50eXBlIElDb21wb25lbnRzID0ge1xuICBCb3hDb250ZW50OiBTdHlsZWRESVY7XG59O1xuXG50eXBlIEJpbmRTdGF0ZSA9IHt9O1xuXG5leHBvcnQgdHlwZSBCaW5kUHJvcHMgPSB7XG4gIG9uQ2FuY2VsOiBhbnk7XG4gIHRva2VuOiBhbnk7XG4gIG9uU2lnbmluU3VjY2VzczogYW55XG59ICYgTW9kZWxXaWRnZXRQcm9wcyAmIEZvcm1Db21wb25lbnRQcm9wcztcblxuY2xhc3MgQmluZFVzZXI8UCBleHRlbmRzIEJpbmRQcm9wcywgUyBleHRlbmRzIEJpbmRTdGF0ZSwgQyBleHRlbmRzIElDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IEJpbmRQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHt9KSBhcyBTO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgICAgXG4gICAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICB9XG5cbiAgLy8gcmVuZGVyXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3QgeyBtb2RlbCwgZm9ybSB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3hDb250ZW50PlxuICAgICAgICA8TlRleHRcbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICBwcm9wTmFtZT17XCJ1c2VyTmFtZVwifVxuICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwidXNlciBuYW1lXCIpXG4gICAgICAgICAgICAudGhhaShcInVzZXIgbmFtZVwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgLz5cbiAgICAgICAgPE5Gb3JtSXRlbVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlBhc3N3b3JkXCIpXG4gICAgICAgICAgICAudGhhaShcIlBhc3N3b3JkXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIHByb3BOYW1lPVwicHdkXCJcbiAgICAgICAgICByZXF1aXJlZD17dHJ1ZX1cbiAgICAgICAgICBydWxlcz17W3sgcmVxdWlyZWQ6IHRydWUsIG1lc3NhZ2U6IFwiUGFzc3dvcmQgaXMgcmVxdWlyZWRcIiB9XX0+XG4gICAgICAgICAgPElucHV0LlBhc3N3b3JkXG4gICAgICAgICAgICBwbGFjZWhvbGRlcj17TGFuZ3VhZ2UuZW4oXCJQYXNzd29yZFwiKVxuICAgICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4quC4nOC5iOC4suC4meC5g+C4q+C4oeC5iFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJuZXctcGFzc3dvcmRcIlxuICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcbiAgICAgICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgICAgLz5cbiAgICAgICAgPC9ORm9ybUl0ZW0+XG4gICAgICAgIDxSb3c+XG4gICAgICAgICAgPENvbCB4cz17OH0gc209ezZ9Lz5cbiAgICAgICAgICA8Q29sIHhzPXsxNn0gc209ezEzfT5cbiAgICAgICAgICAgIDxCdXR0b24gc3R5bGU9e3tcbiAgICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxuICAgICAgICAgICAgfX0gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmJpbmRBY2NvdW50KCk7XG4gICAgICAgICAgICB9fSB0eXBlPXtcInByaW1hcnlcIn0+XG4gICAgICAgICAgICAgIEJpbmQgTm93XG4gICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG4gICAgICA8L0MuQm94Q29udGVudD5cbiAgICApO1xuICB9XG5cbiAgLy8gYWN0aW9uc1xuICBiaW5kQWNjb3VudCgpIHtcbiAgICB0aGlzLnByb3BzLmZvcm0udmFsaWRhdGVGaWVsZHMoKGVycjogYW55LCBmaWVsZHNWYWx1ZTogYW55KSA9PiB7XG4gICAgICBpZiAoZXJyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMubG9naW5CaW5kKCk7XG4gICAgfSk7XG4gIH1cblxuICAvLyBtZXRob2RzXG4gIGFzeW5jIGxvZ2luQmluZCgpIHtcbiAgICBjb25zdCB7IHRva2VuIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHJlVG9rZW4gPSBhd2FpdCBVdGlscy50b2tlblJlY2FwdGNoYShcIkxvZ2luXCIpO1xuICAgIGNvbnN0IGdtdCA9IFN0b3JhZ2UuR2xvYmFsUGFyYW1zLnNlc3Npb24oKS5nZXQoXCJ0aW1lWm9uZVwiKTtcbiAgICBpZiAoIWdtdCkge1xuICAgICAgY29uc3QgdGltZVpvbmUgPSBhd2FpdCBBamF4LmdldChcIi90aW1lem9uZVwiKTtcbiAgICAgIFN0b3JhZ2UuR2xvYmFsUGFyYW1zLnNlc3Npb24oKS5zZXQoXCJ0aW1lWm9uZVwiLCB0aW1lWm9uZS5ib2R5LnJlc3BEYXRhKTtcbiAgICB9XG4gICAgY29uc3QgbG9naW5Nb2RlbCA9IF8uY2xvbmVEZWVwKHRoaXMucHJvcHMubW9kZWwpO1xuICAgIEFqYXgucGF0Y2goQXBpcy5MT0dJTl9RUl9CSU5ELnJlcGxhY2UoXCI6dG9rZW5cIiwgdG9rZW4pLFxuICAgICAgbG9naW5Nb2RlbCxcbiAgICAgIHtcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIFwicmVjYXB0Y2hhLXRva2VuXCI6IHJlVG9rZW4sXG4gICAgICAgIH0sXG4gICAgICB9KS50aGVuKChyZXNwb25zZTogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgdGhpcy5wcm9wcy5vbkNhbmNlbCgpO1xuICAgICAgdGhpcy5wcm9wcy5vblNpZ25pblN1Y2Nlc3MocmVzcERhdGEpO1xuICAgIH0pO1xuXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRm9ybS5jcmVhdGU8QmluZFByb3BzPigpKEJpbmRVc2VyKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQVdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFMQTtBQUNBO0FBU0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQWFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUtBOzs7QUFFQTtBQUNBOzs7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVUE7QUFFQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQUdBOztBQUNBO0FBQ0E7O0FBREE7QUFDQTtBQUNBO0FBQUE7Ozs7OztBQUNBO0FBQ0E7O0FBREE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBREE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFyR0E7QUFDQTtBQXdHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/login/components/bind-user.tsx
