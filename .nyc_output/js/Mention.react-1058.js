__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var rc_editor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rc-editor-core */ "./node_modules/rc-editor-core/es/index.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _utils_createMention__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/createMention */ "./node_modules/rc-editor-mention/es/utils/createMention.js");
/* harmony import */ var _utils_exportContent__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../utils/exportContent */ "./node_modules/rc-editor-mention/es/utils/exportContent.js");












var Mention = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(Mention, _React$Component);

  function Mention(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Mention);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _React$Component.call(this, props));

    _this.onEditorChange = function (editorState) {
      var selection = editorState.getSelection();
      _this._decorator = editorState.getDecorator();
      var content = editorState.getCurrentContent();

      if (_this.props.onChange) {
        _this.setState({
          selection: selection
        }, function () {
          _this.props.onChange(content, Object(_utils_exportContent__WEBPACK_IMPORTED_MODULE_10__["default"])(content));
        });
      } else {
        _this.setState({
          editorState: editorState,
          selection: selection
        });
      }
    };

    _this.onFocus = function (e) {
      if (_this.props.onFocus) {
        _this.props.onFocus(e);
      }
    };

    _this.onBlur = function (e) {
      if (_this.props.onBlur) {
        _this.props.onBlur(e);
      }
    };

    _this.onKeyDown = function (e) {
      if (_this.props.onKeyDown) {
        _this.props.onKeyDown(e);
      }
    };

    _this.reset = function () {
      /*eslint-disable*/
      _this._editor.Reset();
      /*eslint-enable*/

    };

    _this.mention = Object(_utils_createMention__WEBPACK_IMPORTED_MODULE_9__["default"])({
      prefix: _this.getPrefix(props),
      tag: props.tag,
      mode: props.mode,
      mentionStyle: props.mentionStyle
    });
    _this.Suggestions = _this.mention.Suggestions;
    _this.plugins = [_this.mention];
    _this.state = {
      suggestions: props.suggestions,
      value: props.value && draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].createWithContent(props.value, new draft_js__WEBPACK_IMPORTED_MODULE_8__["CompositeDecorator"](_this.mention.decorators)),
      selection: draft_js__WEBPACK_IMPORTED_MODULE_8__["SelectionState"].createEmpty()
    };

    if (typeof props.defaultValue === 'string') {
      // eslint-disable-next-line
      console.warn('The property `defaultValue` now allow `EditorState` only, see http://react-component.github.io/editor-mention/examples/defaultValue.html ');
    }

    if (props.value !== undefined) {
      _this.controlledMode = true;
    }

    return _this;
  }

  Mention.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps) {
    var suggestions = nextProps.suggestions;
    var selection = this.state.selection;
    var value = nextProps.value;

    if (value && selection) {
      value = draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].acceptSelection(draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].createWithContent(value, this._decorator), selection);
    }

    this.setState({
      suggestions: suggestions,
      value: value
    });
  };

  Mention.prototype.getPrefix = function getPrefix() {
    var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.props;
    return Array.isArray(props.prefix) ? props.prefix : [props.prefix];
  };

  Mention.prototype.render = function render() {
    var _classnames,
        _this2 = this;

    var _props = this.props,
        prefixCls = _props.prefixCls,
        style = _props.style,
        tag = _props.tag,
        multiLines = _props.multiLines,
        editorKey = _props.editorKey,
        suggestionStyle = _props.suggestionStyle,
        placeholder = _props.placeholder,
        defaultValue = _props.defaultValue,
        className = _props.className,
        notFoundContent = _props.notFoundContent,
        getSuggestionContainer = _props.getSuggestionContainer,
        readOnly = _props.readOnly,
        disabled = _props.disabled,
        placement = _props.placement,
        mode = _props.mode;
    var suggestions = this.state.suggestions;
    var Suggestions = this.Suggestions;
    var editorClass = classnames__WEBPACK_IMPORTED_MODULE_6___default()(className, (_classnames = {}, _classnames[prefixCls + '-wrapper'] = true, _classnames.readonly = readOnly, _classnames.disabled = disabled, _classnames.multilines = multiLines, _classnames));
    var editorProps = this.controlledMode ? {
      value: this.state.value
    } : {};
    var defaultValueState = defaultValue && draft_js__WEBPACK_IMPORTED_MODULE_8__["EditorState"].createWithContent(typeof defaultValue === 'string' ? draft_js__WEBPACK_IMPORTED_MODULE_8__["ContentState"].createFromText(defaultValue) : defaultValue, this._decorator);
    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: editorClass,
      style: style,
      ref: function ref(wrapper) {
        return _this2._wrapper = wrapper;
      }
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(rc_editor_core__WEBPACK_IMPORTED_MODULE_7__["EditorCore"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      ref: function ref(editor) {
        return _this2._editor = editor;
      },
      prefixCls: prefixCls,
      style: style,
      multiLines: multiLines,
      editorKey: editorKey,
      plugins: this.plugins,
      defaultValue: defaultValueState,
      placeholder: placeholder,
      onFocus: this.onFocus,
      onBlur: this.onBlur,
      onKeyDown: this.onKeyDown,
      onChange: this.onEditorChange
    }, editorProps, {
      readOnly: readOnly || disabled
    }), react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(Suggestions, {
      mode: tag ? 'immutable' : mode,
      prefix: this.getPrefix(),
      prefixCls: prefixCls,
      style: suggestionStyle,
      placement: placement,
      notFoundContent: notFoundContent,
      suggestions: suggestions,
      getSuggestionContainer: getSuggestionContainer ? function () {
        return getSuggestionContainer(_this2._wrapper);
      } : null,
      onSearchChange: this.props.onSearchChange,
      onSelect: this.props.onSelect,
      noRedup: this.props.noRedup
    })));
  };

  return Mention;
}(react__WEBPACK_IMPORTED_MODULE_4___default.a.Component);

Mention.propTypes = {
  value: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  suggestions: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.array,
  prefix: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string)]),
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  tag: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.element, prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func]),
  style: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  className: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  onSearchChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  mode: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  multiLines: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  suggestionStyle: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  placeholder: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  notFoundContent: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  position: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  onFocus: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onBlur: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  getSuggestionContainer: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  noRedup: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  mentionStyle: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  placement: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  editorKey: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string
};
Mention.controlledMode = false;
Mention.defaultProps = {
  prefixCls: 'rc-editor-mention',
  prefix: '@',
  mode: 'mutable',
  suggestions: [],
  multiLines: false,
  className: '',
  suggestionStyle: {},
  notFoundContent: '无法找到',
  position: 'absolute',
  placement: 'bottom',
  // top, bottom
  mentionStyle: {}
};
/* harmony default export */ __webpack_exports__["default"] = (Mention);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvY29tcG9uZW50L01lbnRpb24ucmVhY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1lZGl0b3ItbWVudGlvbi9lcy9jb21wb25lbnQvTWVudGlvbi5yZWFjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBFZGl0b3JDb3JlIH0gZnJvbSAncmMtZWRpdG9yLWNvcmUnO1xuaW1wb3J0IHsgRWRpdG9yU3RhdGUsIFNlbGVjdGlvblN0YXRlLCBDb250ZW50U3RhdGUsIENvbXBvc2l0ZURlY29yYXRvciB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IGNyZWF0ZU1lbnRpb24gZnJvbSAnLi4vdXRpbHMvY3JlYXRlTWVudGlvbic7XG5pbXBvcnQgZXhwb3J0Q29udGVudCBmcm9tICcuLi91dGlscy9leHBvcnRDb250ZW50JztcblxudmFyIE1lbnRpb24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoTWVudGlvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTWVudGlvbihwcm9wcykge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBNZW50aW9uKTtcblxuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbCh0aGlzLCBwcm9wcykpO1xuXG4gICAgX3RoaXMub25FZGl0b3JDaGFuZ2UgPSBmdW5jdGlvbiAoZWRpdG9yU3RhdGUpIHtcbiAgICAgIHZhciBzZWxlY3Rpb24gPSBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKTtcbiAgICAgIF90aGlzLl9kZWNvcmF0b3IgPSBlZGl0b3JTdGF0ZS5nZXREZWNvcmF0b3IoKTtcbiAgICAgIHZhciBjb250ZW50ID0gZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2hhbmdlKSB7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBzZWxlY3Rpb246IHNlbGVjdGlvblxuICAgICAgICB9LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgX3RoaXMucHJvcHMub25DaGFuZ2UoY29udGVudCwgZXhwb3J0Q29udGVudChjb250ZW50KSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGVkaXRvclN0YXRlOiBlZGl0b3JTdGF0ZSxcbiAgICAgICAgICBzZWxlY3Rpb246IHNlbGVjdGlvblxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMub25Gb2N1cyA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25Gb2N1cykge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkZvY3VzKGUpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbkJsdXIgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQmx1cikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkJsdXIoZSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uS2V5RG93biA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25LZXlEb3duKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uS2V5RG93bihlKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMucmVzZXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvKmVzbGludC1kaXNhYmxlKi9cbiAgICAgIF90aGlzLl9lZGl0b3IuUmVzZXQoKTtcbiAgICAgIC8qZXNsaW50LWVuYWJsZSovXG4gICAgfTtcblxuICAgIF90aGlzLm1lbnRpb24gPSBjcmVhdGVNZW50aW9uKHtcbiAgICAgIHByZWZpeDogX3RoaXMuZ2V0UHJlZml4KHByb3BzKSxcbiAgICAgIHRhZzogcHJvcHMudGFnLFxuICAgICAgbW9kZTogcHJvcHMubW9kZSxcbiAgICAgIG1lbnRpb25TdHlsZTogcHJvcHMubWVudGlvblN0eWxlXG4gICAgfSk7XG5cbiAgICBfdGhpcy5TdWdnZXN0aW9ucyA9IF90aGlzLm1lbnRpb24uU3VnZ2VzdGlvbnM7XG4gICAgX3RoaXMucGx1Z2lucyA9IFtfdGhpcy5tZW50aW9uXTtcblxuICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAgc3VnZ2VzdGlvbnM6IHByb3BzLnN1Z2dlc3Rpb25zLFxuICAgICAgdmFsdWU6IHByb3BzLnZhbHVlICYmIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50KHByb3BzLnZhbHVlLCBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKF90aGlzLm1lbnRpb24uZGVjb3JhdG9ycykpLFxuICAgICAgc2VsZWN0aW9uOiBTZWxlY3Rpb25TdGF0ZS5jcmVhdGVFbXB0eSgpXG4gICAgfTtcblxuICAgIGlmICh0eXBlb2YgcHJvcHMuZGVmYXVsdFZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXG4gICAgICBjb25zb2xlLndhcm4oJ1RoZSBwcm9wZXJ0eSBgZGVmYXVsdFZhbHVlYCBub3cgYWxsb3cgYEVkaXRvclN0YXRlYCBvbmx5LCBzZWUgaHR0cDovL3JlYWN0LWNvbXBvbmVudC5naXRodWIuaW8vZWRpdG9yLW1lbnRpb24vZXhhbXBsZXMvZGVmYXVsdFZhbHVlLmh0bWwgJyk7XG4gICAgfVxuICAgIGlmIChwcm9wcy52YWx1ZSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBfdGhpcy5jb250cm9sbGVkTW9kZSA9IHRydWU7XG4gICAgfVxuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIE1lbnRpb24ucHJvdG90eXBlLmNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgIHZhciBzdWdnZXN0aW9ucyA9IG5leHRQcm9wcy5zdWdnZXN0aW9ucztcbiAgICB2YXIgc2VsZWN0aW9uID0gdGhpcy5zdGF0ZS5zZWxlY3Rpb247XG5cbiAgICB2YXIgdmFsdWUgPSBuZXh0UHJvcHMudmFsdWU7XG4gICAgaWYgKHZhbHVlICYmIHNlbGVjdGlvbikge1xuICAgICAgdmFsdWUgPSBFZGl0b3JTdGF0ZS5hY2NlcHRTZWxlY3Rpb24oRWRpdG9yU3RhdGUuY3JlYXRlV2l0aENvbnRlbnQodmFsdWUsIHRoaXMuX2RlY29yYXRvciksIHNlbGVjdGlvbik7XG4gICAgfVxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc3VnZ2VzdGlvbnM6IHN1Z2dlc3Rpb25zLFxuICAgICAgdmFsdWU6IHZhbHVlXG4gICAgfSk7XG4gIH07XG5cbiAgTWVudGlvbi5wcm90b3R5cGUuZ2V0UHJlZml4ID0gZnVuY3Rpb24gZ2V0UHJlZml4KCkge1xuICAgIHZhciBwcm9wcyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogdGhpcy5wcm9wcztcblxuICAgIHJldHVybiBBcnJheS5pc0FycmF5KHByb3BzLnByZWZpeCkgPyBwcm9wcy5wcmVmaXggOiBbcHJvcHMucHJlZml4XTtcbiAgfTtcblxuICBNZW50aW9uLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9jbGFzc25hbWVzLFxuICAgICAgICBfdGhpczIgPSB0aGlzO1xuXG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHByZWZpeENscyA9IF9wcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgIHN0eWxlID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICB0YWcgPSBfcHJvcHMudGFnLFxuICAgICAgICBtdWx0aUxpbmVzID0gX3Byb3BzLm11bHRpTGluZXMsXG4gICAgICAgIGVkaXRvcktleSA9IF9wcm9wcy5lZGl0b3JLZXksXG4gICAgICAgIHN1Z2dlc3Rpb25TdHlsZSA9IF9wcm9wcy5zdWdnZXN0aW9uU3R5bGUsXG4gICAgICAgIHBsYWNlaG9sZGVyID0gX3Byb3BzLnBsYWNlaG9sZGVyLFxuICAgICAgICBkZWZhdWx0VmFsdWUgPSBfcHJvcHMuZGVmYXVsdFZhbHVlLFxuICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICBub3RGb3VuZENvbnRlbnQgPSBfcHJvcHMubm90Rm91bmRDb250ZW50LFxuICAgICAgICBnZXRTdWdnZXN0aW9uQ29udGFpbmVyID0gX3Byb3BzLmdldFN1Z2dlc3Rpb25Db250YWluZXIsXG4gICAgICAgIHJlYWRPbmx5ID0gX3Byb3BzLnJlYWRPbmx5LFxuICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgcGxhY2VtZW50ID0gX3Byb3BzLnBsYWNlbWVudCxcbiAgICAgICAgbW9kZSA9IF9wcm9wcy5tb2RlO1xuICAgIHZhciBzdWdnZXN0aW9ucyA9IHRoaXMuc3RhdGUuc3VnZ2VzdGlvbnM7XG4gICAgdmFyIFN1Z2dlc3Rpb25zID0gdGhpcy5TdWdnZXN0aW9ucztcblxuICAgIHZhciBlZGl0b3JDbGFzcyA9IGNsYXNzbmFtZXMoY2xhc3NOYW1lLCAoX2NsYXNzbmFtZXMgPSB7fSwgX2NsYXNzbmFtZXNbcHJlZml4Q2xzICsgJy13cmFwcGVyJ10gPSB0cnVlLCBfY2xhc3NuYW1lcy5yZWFkb25seSA9IHJlYWRPbmx5LCBfY2xhc3NuYW1lcy5kaXNhYmxlZCA9IGRpc2FibGVkLCBfY2xhc3NuYW1lcy5tdWx0aWxpbmVzID0gbXVsdGlMaW5lcywgX2NsYXNzbmFtZXMpKTtcbiAgICB2YXIgZWRpdG9yUHJvcHMgPSB0aGlzLmNvbnRyb2xsZWRNb2RlID8geyB2YWx1ZTogdGhpcy5zdGF0ZS52YWx1ZSB9IDoge307XG4gICAgdmFyIGRlZmF1bHRWYWx1ZVN0YXRlID0gZGVmYXVsdFZhbHVlICYmIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50KHR5cGVvZiBkZWZhdWx0VmFsdWUgPT09ICdzdHJpbmcnID8gQ29udGVudFN0YXRlLmNyZWF0ZUZyb21UZXh0KGRlZmF1bHRWYWx1ZSkgOiBkZWZhdWx0VmFsdWUsIHRoaXMuX2RlY29yYXRvcik7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAnZGl2JyxcbiAgICAgIHsgY2xhc3NOYW1lOiBlZGl0b3JDbGFzcywgc3R5bGU6IHN0eWxlLCByZWY6IGZ1bmN0aW9uIHJlZih3cmFwcGVyKSB7XG4gICAgICAgICAgcmV0dXJuIF90aGlzMi5fd3JhcHBlciA9IHdyYXBwZXI7XG4gICAgICAgIH0gfSxcbiAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIEVkaXRvckNvcmUsXG4gICAgICAgIF9leHRlbmRzKHtcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihlZGl0b3IpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpczIuX2VkaXRvciA9IGVkaXRvcjtcbiAgICAgICAgICB9LFxuICAgICAgICAgIHByZWZpeENsczogcHJlZml4Q2xzLFxuICAgICAgICAgIHN0eWxlOiBzdHlsZSxcbiAgICAgICAgICBtdWx0aUxpbmVzOiBtdWx0aUxpbmVzLFxuICAgICAgICAgIGVkaXRvcktleTogZWRpdG9yS2V5LFxuICAgICAgICAgIHBsdWdpbnM6IHRoaXMucGx1Z2lucyxcbiAgICAgICAgICBkZWZhdWx0VmFsdWU6IGRlZmF1bHRWYWx1ZVN0YXRlLFxuICAgICAgICAgIHBsYWNlaG9sZGVyOiBwbGFjZWhvbGRlcixcbiAgICAgICAgICBvbkZvY3VzOiB0aGlzLm9uRm9jdXMsXG4gICAgICAgICAgb25CbHVyOiB0aGlzLm9uQmx1cixcbiAgICAgICAgICBvbktleURvd246IHRoaXMub25LZXlEb3duLFxuICAgICAgICAgIG9uQ2hhbmdlOiB0aGlzLm9uRWRpdG9yQ2hhbmdlXG4gICAgICAgIH0sIGVkaXRvclByb3BzLCB7XG4gICAgICAgICAgcmVhZE9ubHk6IHJlYWRPbmx5IHx8IGRpc2FibGVkXG4gICAgICAgIH0pLFxuICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFN1Z2dlc3Rpb25zLCB7XG4gICAgICAgICAgbW9kZTogdGFnID8gJ2ltbXV0YWJsZScgOiBtb2RlLFxuICAgICAgICAgIHByZWZpeDogdGhpcy5nZXRQcmVmaXgoKSxcbiAgICAgICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgICAgICBzdHlsZTogc3VnZ2VzdGlvblN0eWxlLFxuICAgICAgICAgIHBsYWNlbWVudDogcGxhY2VtZW50LFxuICAgICAgICAgIG5vdEZvdW5kQ29udGVudDogbm90Rm91bmRDb250ZW50LFxuICAgICAgICAgIHN1Z2dlc3Rpb25zOiBzdWdnZXN0aW9ucyxcbiAgICAgICAgICBnZXRTdWdnZXN0aW9uQ29udGFpbmVyOiBnZXRTdWdnZXN0aW9uQ29udGFpbmVyID8gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIGdldFN1Z2dlc3Rpb25Db250YWluZXIoX3RoaXMyLl93cmFwcGVyKTtcbiAgICAgICAgICB9IDogbnVsbCxcbiAgICAgICAgICBvblNlYXJjaENoYW5nZTogdGhpcy5wcm9wcy5vblNlYXJjaENoYW5nZSxcbiAgICAgICAgICBvblNlbGVjdDogdGhpcy5wcm9wcy5vblNlbGVjdCxcbiAgICAgICAgICBub1JlZHVwOiB0aGlzLnByb3BzLm5vUmVkdXBcbiAgICAgICAgfSlcbiAgICAgIClcbiAgICApO1xuICB9O1xuXG4gIHJldHVybiBNZW50aW9uO1xufShSZWFjdC5Db21wb25lbnQpO1xuXG5NZW50aW9uLnByb3BUeXBlcyA9IHtcbiAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG4gIHN1Z2dlc3Rpb25zOiBQcm9wVHlwZXMuYXJyYXksXG4gIHByZWZpeDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyldKSxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0YWc6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5lbGVtZW50LCBQcm9wVHlwZXMuZnVuY10pLFxuICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBvblNlYXJjaENoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgbW9kZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgbXVsdGlMaW5lczogUHJvcFR5cGVzLmJvb2wsXG4gIHN1Z2dlc3Rpb25TdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRlZmF1bHRWYWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgbm90Rm91bmRDb250ZW50OiBQcm9wVHlwZXMuYW55LFxuICBwb3NpdGlvbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgb25Gb2N1czogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uQmx1cjogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uU2VsZWN0OiBQcm9wVHlwZXMuZnVuYyxcbiAgb25LZXlEb3duOiBQcm9wVHlwZXMuZnVuYyxcbiAgZ2V0U3VnZ2VzdGlvbkNvbnRhaW5lcjogUHJvcFR5cGVzLmZ1bmMsXG4gIG5vUmVkdXA6IFByb3BUeXBlcy5ib29sLFxuICBtZW50aW9uU3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gIHBsYWNlbWVudDogUHJvcFR5cGVzLnN0cmluZyxcbiAgZWRpdG9yS2V5OiBQcm9wVHlwZXMuc3RyaW5nXG59O1xuTWVudGlvbi5jb250cm9sbGVkTW9kZSA9IGZhbHNlO1xuXG5cbk1lbnRpb24uZGVmYXVsdFByb3BzID0ge1xuICBwcmVmaXhDbHM6ICdyYy1lZGl0b3ItbWVudGlvbicsXG4gIHByZWZpeDogJ0AnLFxuICBtb2RlOiAnbXV0YWJsZScsXG4gIHN1Z2dlc3Rpb25zOiBbXSxcbiAgbXVsdGlMaW5lczogZmFsc2UsXG4gIGNsYXNzTmFtZTogJycsXG4gIHN1Z2dlc3Rpb25TdHlsZToge30sXG4gIG5vdEZvdW5kQ29udGVudDogJ+aXoOazleaJvuWIsCcsXG4gIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICBwbGFjZW1lbnQ6ICdib3R0b20nLCAvLyB0b3AsIGJvdHRvbVxuICBtZW50aW9uU3R5bGU6IHt9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBNZW50aW9uOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEE7QUFnQkE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBYkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF6QkE7QUEyQkE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQVhBO0FBY0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/component/Mention.react.js
