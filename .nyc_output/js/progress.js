__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Progress; });
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
/* harmony import */ var _Line__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Line */ "./node_modules/antd/es/progress/Line.js");
/* harmony import */ var _Circle__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Circle */ "./node_modules/antd/es/progress/Circle.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./utils */ "./node_modules/antd/es/progress/utils.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};











var ProgressTypes = Object(_util_type__WEBPACK_IMPORTED_MODULE_6__["tuple"])('line', 'circle', 'dashboard');
var ProgressStatuses = Object(_util_type__WEBPACK_IMPORTED_MODULE_6__["tuple"])('normal', 'exception', 'active', 'success');

var Progress =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Progress, _React$Component);

  function Progress() {
    var _this;

    _classCallCheck(this, Progress);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Progress).apply(this, arguments));

    _this.renderProgress = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props;

      var customizePrefixCls = props.prefixCls,
          className = props.className,
          size = props.size,
          type = props.type,
          showInfo = props.showInfo,
          restProps = __rest(props, ["prefixCls", "className", "size", "type", "showInfo"]);

      var prefixCls = getPrefixCls('progress', customizePrefixCls);

      var progressStatus = _this.getProgressStatus();

      var progressInfo = _this.renderProcessInfo(prefixCls, progressStatus);

      var progress; // Render progress shape

      if (type === 'line') {
        progress = react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_Line__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({}, _this.props, {
          prefixCls: prefixCls
        }), progressInfo);
      } else if (type === 'circle' || type === 'dashboard') {
        progress = react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_Circle__WEBPACK_IMPORTED_MODULE_8__["default"], _extends({}, _this.props, {
          prefixCls: prefixCls,
          progressStatus: progressStatus
        }), progressInfo);
      }

      var classString = classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(type === 'dashboard' && 'circle' || type), true), _defineProperty(_classNames, "".concat(prefixCls, "-status-").concat(progressStatus), true), _defineProperty(_classNames, "".concat(prefixCls, "-show-info"), showInfo), _defineProperty(_classNames, "".concat(prefixCls, "-").concat(size), size), _classNames), className);
      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("div", _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_3__["default"])(restProps, ['status', 'format', 'trailColor', 'successPercent', 'strokeWidth', 'width', 'gapDegree', 'gapPosition', 'strokeColor', 'strokeLinecap', 'percent']), {
        className: classString
      }), progress);
    };

    return _this;
  }

  _createClass(Progress, [{
    key: "getPercentNumber",
    value: function getPercentNumber() {
      var _this$props = this.props,
          successPercent = _this$props.successPercent,
          _this$props$percent = _this$props.percent,
          percent = _this$props$percent === void 0 ? 0 : _this$props$percent;
      return parseInt(successPercent !== undefined ? successPercent.toString() : percent.toString(), 10);
    }
  }, {
    key: "getProgressStatus",
    value: function getProgressStatus() {
      var status = this.props.status;

      if (ProgressStatuses.indexOf(status) < 0 && this.getPercentNumber() >= 100) {
        return 'success';
      }

      return status || 'normal';
    }
  }, {
    key: "renderProcessInfo",
    value: function renderProcessInfo(prefixCls, progressStatus) {
      var _this$props2 = this.props,
          showInfo = _this$props2.showInfo,
          format = _this$props2.format,
          type = _this$props2.type,
          percent = _this$props2.percent,
          successPercent = _this$props2.successPercent;
      if (!showInfo) return null;
      var text;

      var textFormatter = format || function (percentNumber) {
        return "".concat(percentNumber, "%");
      };

      var iconType = type === 'circle' || type === 'dashboard' ? '' : '-circle';

      if (format || progressStatus !== 'exception' && progressStatus !== 'success') {
        text = textFormatter(Object(_utils__WEBPACK_IMPORTED_MODULE_9__["validProgress"])(percent), Object(_utils__WEBPACK_IMPORTED_MODULE_9__["validProgress"])(successPercent));
      } else if (progressStatus === 'exception') {
        text = react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "close".concat(iconType),
          theme: type === 'line' ? 'filled' : 'outlined'
        });
      } else if (progressStatus === 'success') {
        text = react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_4__["default"], {
          type: "check".concat(iconType),
          theme: type === 'line' ? 'filled' : 'outlined'
        });
      }

      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"]("span", {
        className: "".concat(prefixCls, "-text"),
        title: typeof text === 'string' ? text : undefined
      }, text);
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_5__["ConfigConsumer"], null, this.renderProgress);
    }
  }]);

  return Progress;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);


Progress.defaultProps = {
  type: 'line',
  percent: 0,
  showInfo: true,
  trailColor: '#f3f3f3',
  size: 'default',
  gapDegree: 0,
  strokeLinecap: 'round'
};
Progress.propTypes = {
  status: prop_types__WEBPACK_IMPORTED_MODULE_0__["oneOf"](ProgressStatuses),
  type: prop_types__WEBPACK_IMPORTED_MODULE_0__["oneOf"](ProgressTypes),
  showInfo: prop_types__WEBPACK_IMPORTED_MODULE_0__["bool"],
  percent: prop_types__WEBPACK_IMPORTED_MODULE_0__["number"],
  width: prop_types__WEBPACK_IMPORTED_MODULE_0__["number"],
  strokeWidth: prop_types__WEBPACK_IMPORTED_MODULE_0__["number"],
  strokeLinecap: prop_types__WEBPACK_IMPORTED_MODULE_0__["oneOf"](['round', 'square']),
  strokeColor: prop_types__WEBPACK_IMPORTED_MODULE_0__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_0__["string"], prop_types__WEBPACK_IMPORTED_MODULE_0__["object"]]),
  trailColor: prop_types__WEBPACK_IMPORTED_MODULE_0__["string"],
  format: prop_types__WEBPACK_IMPORTED_MODULE_0__["func"],
  gapDegree: prop_types__WEBPACK_IMPORTED_MODULE_0__["number"]
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9wcm9ncmVzcy9wcm9ncmVzcy5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvcHJvZ3Jlc3MvcHJvZ3Jlc3MuanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IG9taXQgZnJvbSAnb21pdC5qcyc7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCB7IHR1cGxlIH0gZnJvbSAnLi4vX3V0aWwvdHlwZSc7XG5pbXBvcnQgTGluZSBmcm9tICcuL0xpbmUnO1xuaW1wb3J0IENpcmNsZSBmcm9tICcuL0NpcmNsZSc7XG5pbXBvcnQgeyB2YWxpZFByb2dyZXNzIH0gZnJvbSAnLi91dGlscyc7XG5jb25zdCBQcm9ncmVzc1R5cGVzID0gdHVwbGUoJ2xpbmUnLCAnY2lyY2xlJywgJ2Rhc2hib2FyZCcpO1xuY29uc3QgUHJvZ3Jlc3NTdGF0dXNlcyA9IHR1cGxlKCdub3JtYWwnLCAnZXhjZXB0aW9uJywgJ2FjdGl2ZScsICdzdWNjZXNzJyk7XG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQcm9ncmVzcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKC4uLmFyZ3VtZW50cyk7XG4gICAgICAgIHRoaXMucmVuZGVyUHJvZ3Jlc3MgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcm9wcyB9ID0gdGhpcztcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNsYXNzTmFtZSwgc2l6ZSwgdHlwZSwgc2hvd0luZm8gfSA9IHByb3BzLCByZXN0UHJvcHMgPSBfX3Jlc3QocHJvcHMsIFtcInByZWZpeENsc1wiLCBcImNsYXNzTmFtZVwiLCBcInNpemVcIiwgXCJ0eXBlXCIsIFwic2hvd0luZm9cIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdwcm9ncmVzcycsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBwcm9ncmVzc1N0YXR1cyA9IHRoaXMuZ2V0UHJvZ3Jlc3NTdGF0dXMoKTtcbiAgICAgICAgICAgIGNvbnN0IHByb2dyZXNzSW5mbyA9IHRoaXMucmVuZGVyUHJvY2Vzc0luZm8ocHJlZml4Q2xzLCBwcm9ncmVzc1N0YXR1cyk7XG4gICAgICAgICAgICBsZXQgcHJvZ3Jlc3M7XG4gICAgICAgICAgICAvLyBSZW5kZXIgcHJvZ3Jlc3Mgc2hhcGVcbiAgICAgICAgICAgIGlmICh0eXBlID09PSAnbGluZScpIHtcbiAgICAgICAgICAgICAgICBwcm9ncmVzcyA9ICg8TGluZSB7Li4udGhpcy5wcm9wc30gcHJlZml4Q2xzPXtwcmVmaXhDbHN9PlxuICAgICAgICAgIHtwcm9ncmVzc0luZm99XG4gICAgICAgIDwvTGluZT4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAodHlwZSA9PT0gJ2NpcmNsZScgfHwgdHlwZSA9PT0gJ2Rhc2hib2FyZCcpIHtcbiAgICAgICAgICAgICAgICBwcm9ncmVzcyA9ICg8Q2lyY2xlIHsuLi50aGlzLnByb3BzfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gcHJvZ3Jlc3NTdGF0dXM9e3Byb2dyZXNzU3RhdHVzfT5cbiAgICAgICAgICB7cHJvZ3Jlc3NJbmZvfVxuICAgICAgICA8L0NpcmNsZT4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgY2xhc3NTdHJpbmcgPSBjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LSR7KHR5cGUgPT09ICdkYXNoYm9hcmQnICYmICdjaXJjbGUnKSB8fCB0eXBlfWBdOiB0cnVlLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXN0YXR1cy0ke3Byb2dyZXNzU3RhdHVzfWBdOiB0cnVlLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXNob3ctaW5mb2BdOiBzaG93SW5mbyxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke3NpemV9YF06IHNpemUsXG4gICAgICAgICAgICB9LCBjbGFzc05hbWUpO1xuICAgICAgICAgICAgcmV0dXJuICg8ZGl2IHsuLi5vbWl0KHJlc3RQcm9wcywgW1xuICAgICAgICAgICAgICAgICdzdGF0dXMnLFxuICAgICAgICAgICAgICAgICdmb3JtYXQnLFxuICAgICAgICAgICAgICAgICd0cmFpbENvbG9yJyxcbiAgICAgICAgICAgICAgICAnc3VjY2Vzc1BlcmNlbnQnLFxuICAgICAgICAgICAgICAgICdzdHJva2VXaWR0aCcsXG4gICAgICAgICAgICAgICAgJ3dpZHRoJyxcbiAgICAgICAgICAgICAgICAnZ2FwRGVncmVlJyxcbiAgICAgICAgICAgICAgICAnZ2FwUG9zaXRpb24nLFxuICAgICAgICAgICAgICAgICdzdHJva2VDb2xvcicsXG4gICAgICAgICAgICAgICAgJ3N0cm9rZUxpbmVjYXAnLFxuICAgICAgICAgICAgICAgICdwZXJjZW50JyxcbiAgICAgICAgICAgIF0pfSBjbGFzc05hbWU9e2NsYXNzU3RyaW5nfT5cbiAgICAgICAge3Byb2dyZXNzfVxuICAgICAgPC9kaXY+KTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgZ2V0UGVyY2VudE51bWJlcigpIHtcbiAgICAgICAgY29uc3QgeyBzdWNjZXNzUGVyY2VudCwgcGVyY2VudCA9IDAgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiBwYXJzZUludChzdWNjZXNzUGVyY2VudCAhPT0gdW5kZWZpbmVkID8gc3VjY2Vzc1BlcmNlbnQudG9TdHJpbmcoKSA6IHBlcmNlbnQudG9TdHJpbmcoKSwgMTApO1xuICAgIH1cbiAgICBnZXRQcm9ncmVzc1N0YXR1cygpIHtcbiAgICAgICAgY29uc3QgeyBzdGF0dXMgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGlmIChQcm9ncmVzc1N0YXR1c2VzLmluZGV4T2Yoc3RhdHVzKSA8IDAgJiYgdGhpcy5nZXRQZXJjZW50TnVtYmVyKCkgPj0gMTAwKSB7XG4gICAgICAgICAgICByZXR1cm4gJ3N1Y2Nlc3MnO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzdGF0dXMgfHwgJ25vcm1hbCc7XG4gICAgfVxuICAgIHJlbmRlclByb2Nlc3NJbmZvKHByZWZpeENscywgcHJvZ3Jlc3NTdGF0dXMpIHtcbiAgICAgICAgY29uc3QgeyBzaG93SW5mbywgZm9ybWF0LCB0eXBlLCBwZXJjZW50LCBzdWNjZXNzUGVyY2VudCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKCFzaG93SW5mbylcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICBsZXQgdGV4dDtcbiAgICAgICAgY29uc3QgdGV4dEZvcm1hdHRlciA9IGZvcm1hdCB8fCAocGVyY2VudE51bWJlciA9PiBgJHtwZXJjZW50TnVtYmVyfSVgKTtcbiAgICAgICAgY29uc3QgaWNvblR5cGUgPSB0eXBlID09PSAnY2lyY2xlJyB8fCB0eXBlID09PSAnZGFzaGJvYXJkJyA/ICcnIDogJy1jaXJjbGUnO1xuICAgICAgICBpZiAoZm9ybWF0IHx8IChwcm9ncmVzc1N0YXR1cyAhPT0gJ2V4Y2VwdGlvbicgJiYgcHJvZ3Jlc3NTdGF0dXMgIT09ICdzdWNjZXNzJykpIHtcbiAgICAgICAgICAgIHRleHQgPSB0ZXh0Rm9ybWF0dGVyKHZhbGlkUHJvZ3Jlc3MocGVyY2VudCksIHZhbGlkUHJvZ3Jlc3Moc3VjY2Vzc1BlcmNlbnQpKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChwcm9ncmVzc1N0YXR1cyA9PT0gJ2V4Y2VwdGlvbicpIHtcbiAgICAgICAgICAgIHRleHQgPSA8SWNvbiB0eXBlPXtgY2xvc2Uke2ljb25UeXBlfWB9IHRoZW1lPXt0eXBlID09PSAnbGluZScgPyAnZmlsbGVkJyA6ICdvdXRsaW5lZCd9Lz47XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocHJvZ3Jlc3NTdGF0dXMgPT09ICdzdWNjZXNzJykge1xuICAgICAgICAgICAgdGV4dCA9IDxJY29uIHR5cGU9e2BjaGVjayR7aWNvblR5cGV9YH0gdGhlbWU9e3R5cGUgPT09ICdsaW5lJyA/ICdmaWxsZWQnIDogJ291dGxpbmVkJ30vPjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gKDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS10ZXh0YH0gdGl0bGU9e3R5cGVvZiB0ZXh0ID09PSAnc3RyaW5nJyA/IHRleHQgOiB1bmRlZmluZWR9PlxuICAgICAgICB7dGV4dH1cbiAgICAgIDwvc3Bhbj4pO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyUHJvZ3Jlc3N9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuUHJvZ3Jlc3MuZGVmYXVsdFByb3BzID0ge1xuICAgIHR5cGU6ICdsaW5lJyxcbiAgICBwZXJjZW50OiAwLFxuICAgIHNob3dJbmZvOiB0cnVlLFxuICAgIHRyYWlsQ29sb3I6ICcjZjNmM2YzJyxcbiAgICBzaXplOiAnZGVmYXVsdCcsXG4gICAgZ2FwRGVncmVlOiAwLFxuICAgIHN0cm9rZUxpbmVjYXA6ICdyb3VuZCcsXG59O1xuUHJvZ3Jlc3MucHJvcFR5cGVzID0ge1xuICAgIHN0YXR1czogUHJvcFR5cGVzLm9uZU9mKFByb2dyZXNzU3RhdHVzZXMpLFxuICAgIHR5cGU6IFByb3BUeXBlcy5vbmVPZihQcm9ncmVzc1R5cGVzKSxcbiAgICBzaG93SW5mbzogUHJvcFR5cGVzLmJvb2wsXG4gICAgcGVyY2VudDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB3aWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBzdHJva2VXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBzdHJva2VMaW5lY2FwOiBQcm9wVHlwZXMub25lT2YoWydyb3VuZCcsICdzcXVhcmUnXSksXG4gICAgc3Ryb2tlQ29sb3I6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5vYmplY3RdKSxcbiAgICB0cmFpbENvbG9yOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGZvcm1hdDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgZ2FwRGVncmVlOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBTUE7QUFZQTtBQVpBO0FBeEJBO0FBQ0E7QUFIQTtBQTBDQTtBQUNBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBOzs7QUFDQTtBQUNBO0FBQ0E7Ozs7QUE3RUE7QUFDQTtBQURBO0FBK0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/progress/progress.js
