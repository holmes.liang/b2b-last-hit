__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _address__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../address */ "./src/app/desk/component/address.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/address/thai-address.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n              \n            "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var ThaiAddress =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ThaiAddress, _ModelWidget);

  function ThaiAddress(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, ThaiAddress);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ThaiAddress).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(ThaiAddress, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        ThaiAddress: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ThaiAddress.prototype), "initState", this).call(this), {});
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var C = this.getComponents();
      var _this$props = this.props,
          model = _this$props.model,
          form = _this$props.form,
          prefix = _this$props.prefix,
          selectValue = _this$props.selectValue,
          extAddressType = _this$props.extAddressType,
          notAddressType = _this$props.notAddressType,
          layoutCol = _this$props.layoutCol,
          _this$props$required = _this$props.required,
          required = _this$props$required === void 0 ? true : _this$props$required;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.ThaiAddress, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], {
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Address").thai("ที่อยู่").my("လိပ်စာ").getMessage(),
        model: model,
        required: required,
        layoutCol: layoutCol,
        propName: "".concat(prefix, "-").concat(this.addressFix),
        onChange: function onChange(address) {
          var _this$setValuesToMode;

          var postalCode = address.postalCode;

          _this.setValuesToModel((_this$setValuesToMode = {}, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix), address), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix, "-street"), form.getFieldValue("".concat(prefix, "-address-street"))), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$setValuesToMode, "".concat(prefix, "-").concat(_this.addressFix, "-postalCode"), postalCode), _this$setValuesToMode));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_address__WEBPACK_IMPORTED_MODULE_13__["default"], {
        selectValue: selectValue || this.getValueFromModel("".concat(prefix, "-").concat(this.addressFix)),
        ltr: false,
        countryCode: this.props.countryCode,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      })), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NText"], {
        form: form,
        model: model,
        propName: "".concat(prefix, "-").concat(this.addressFix, "-street"),
        required: required,
        layoutCol: layoutCol,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Street").thai("ที่อยู่ถนน").my("လမ်းပေါ်မှ").getMessage(),
        placeholder: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("organization, building, village, street no. and street name etc.").thai("สังกัดหน่วยงาน, เลขที่, อาคาร, หมู่บ้าน, ถนน, อื่น ๆ").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_12__["NText"], {
        form: form,
        label: _common__WEBPACK_IMPORTED_MODULE_11__["Language"].en("Postal Code").thai("รหัสไปรษณีย์").my("စာတိုက်သင်္ကေတ").getMessage(),
        required: required,
        layoutCol: layoutCol,
        model: model,
        propName: "".concat(prefix, "-").concat(this.addressFix, "-postalCode"),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }));
    }
  }, {
    key: "addressFix",
    get: function get() {
      return this.props.addressFix || "address";
    }
  }]);

  return ThaiAddress;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ThaiAddress);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3MvdGhhaS1hZGRyZXNzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9hZGRyZXNzL3RoYWktYWRkcmVzcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgTW9kZWxXaWRnZXQsIE5Gb3JtSXRlbSB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBNb2RlbFdpZGdldFByb3BzLCBTdHlsZWRESVYgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgKiBhcyBTdHlsZWRGdW5jdGlvbnMgZnJvbSBcInN0eWxlZC1jb21wb25lbnRzXCI7XG5pbXBvcnQgeyBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBOUmFkaW8sIE5UZXh0IH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgQWRkcmVzcyBmcm9tIFwiLi4vYWRkcmVzc1wiO1xuXG5leHBvcnQgdHlwZSBUaGFpQWRkcmVzc1Byb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBmb3JtOiBhbnk7XG4gIHByZWZpeD86IGFueTtcbiAgYWRkcmVzc0ZpeD86IHN0cmluZztcbiAgZXh0QWRkcmVzc1R5cGU/OiBhbnk7XG4gIHNlbGVjdFZhbHVlPzogYW55O1xuICBub3RBZGRyZXNzVHlwZT86IGJvb2xlYW47XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbiAgY291bnRyeUNvZGU/OiBzdHJpbmc7XG4gIGxheW91dENvbD86IGFueTtcbn0gJiBNb2RlbFdpZGdldFByb3BzO1xuXG5leHBvcnQgdHlwZSBUaGFpQWRkcmVzc1N0YXRlID0ge1xuICBzcmM6IGFueTtcbn07XG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgVGhhaUFkZHJlc3NDb21wb25lbnRzID0ge1xuICBUaGFpQWRkcmVzczogU3R5bGVkRElWO1xufTtcblxuY2xhc3MgVGhhaUFkZHJlc3M8UCBleHRlbmRzIFRoYWlBZGRyZXNzUHJvcHMsXG4gIFMgZXh0ZW5kcyBUaGFpQWRkcmVzc1N0YXRlLFxuICBDIGV4dGVuZHMgVGhhaUFkZHJlc3NDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IFRoYWlBZGRyZXNzUHJvcHMsIGNvbnRleHQ/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgY29udGV4dCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIFRoYWlBZGRyZXNzOiBTdHlsZWQuZGl2YFxuICAgICAgICAgICAgICBcbiAgICAgICAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0U3RhdGUoKSwge30pIGFzIFM7XG4gIH1cblxuICBnZXQgYWRkcmVzc0ZpeCgpIHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy5hZGRyZXNzRml4IHx8IFwiYWRkcmVzc1wiO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IG1vZGVsLCBmb3JtLCBwcmVmaXgsIHNlbGVjdFZhbHVlLCBleHRBZGRyZXNzVHlwZSwgbm90QWRkcmVzc1R5cGUsIGxheW91dENvbCwgcmVxdWlyZWQgPSB0cnVlIH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5UaGFpQWRkcmVzcz5cbiAgICAgICAgPE5Gb3JtSXRlbVxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiQWRkcmVzc1wiKVxuICAgICAgICAgICAgLnRoYWkoXCLguJfguLXguYjguK3guKLguLnguYhcIilcbiAgICAgICAgICAgIC5teShcIuGAnOGAreGAleGAuuGAheGArFwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmVkfVxuICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgIHByb3BOYW1lPXtgJHtwcmVmaXh9LSR7dGhpcy5hZGRyZXNzRml4fWB9XG4gICAgICAgICAgb25DaGFuZ2U9eyhhZGRyZXNzOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcG9zdGFsQ29kZSB9ID0gYWRkcmVzcztcbiAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgICAgICAgIFtgJHtwcmVmaXh9LSR7dGhpcy5hZGRyZXNzRml4fWBdOiBhZGRyZXNzLFxuICAgICAgICAgICAgICBbYCR7cHJlZml4fS0ke3RoaXMuYWRkcmVzc0ZpeH0tc3RyZWV0YF06IGZvcm0uZ2V0RmllbGRWYWx1ZShgJHtwcmVmaXh9LWFkZHJlc3Mtc3RyZWV0YCksXG4gICAgICAgICAgICAgIFtgJHtwcmVmaXh9LSR7dGhpcy5hZGRyZXNzRml4fS1wb3N0YWxDb2RlYF06IHBvc3RhbENvZGUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPEFkZHJlc3NcbiAgICAgICAgICAgIHNlbGVjdFZhbHVlPXtzZWxlY3RWYWx1ZSB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9YCl9XG4gICAgICAgICAgICBsdHI9e2ZhbHNlfVxuICAgICAgICAgICAgY291bnRyeUNvZGU9e3RoaXMucHJvcHMuY291bnRyeUNvZGUhfVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvTkZvcm1JdGVtPlxuXG4gICAgICAgIDxOVGV4dFxuICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgIHByb3BOYW1lPXtgJHtwcmVmaXh9LSR7dGhpcy5hZGRyZXNzRml4fS1zdHJlZXRgfVxuICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJTdHJlZXRcIilcbiAgICAgICAgICAgIC50aGFpKFwi4LiX4Li14LmI4Lit4Lii4Li54LmI4LiW4LiZ4LiZXCIpXG4gICAgICAgICAgICAubXkoXCLhgJzhgJnhgLrhgLjhgJXhgLHhgKvhgLrhgJnhgL5cIilcbiAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgcGxhY2Vob2xkZXI9e0xhbmd1YWdlLmVuKFwib3JnYW5pemF0aW9uLCBidWlsZGluZywgdmlsbGFnZSwgc3RyZWV0IG5vLiBhbmQgc3RyZWV0IG5hbWUgZXRjLlwiKVxuICAgICAgICAgICAgLnRoYWkoXCLguKrguLHguIfguIHguLHguJTguKvguJnguYjguKfguKLguIfguLLguJksIOC5gOC4peC4guC4l+C4teC5iCwg4Lit4Liy4LiE4Liy4LijLCDguKvguKHguLnguYjguJrguYnguLLguJksIOC4luC4meC4mSwg4Lit4Li34LmI4LiZIOC5hlwiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgLz5cblxuICAgICAgICA8TlRleHRcbiAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlBvc3RhbCBDb2RlXCIpXG4gICAgICAgICAgICAudGhhaShcIuC4o+C4q+C4seC4quC5hOC4m+C4o+C4qeC4k+C4teC4ouC5jFwiKVxuICAgICAgICAgICAgLm15KFwi4YCF4YCs4YCQ4YCt4YCv4YCA4YC64YCe4YCE4YC64YC54YCA4YCx4YCQXCIpXG4gICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgcHJvcE5hbWU9e2Ake3ByZWZpeH0tJHt0aGlzLmFkZHJlc3NGaXh9LXBvc3RhbENvZGVgfVxuICAgICAgICAvPlxuICAgICAgPC9DLlRoYWlBZGRyZXNzPlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVGhhaUFkZHJlc3M7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUF1QkE7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUtBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQU1BO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUtBO0FBakJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQW9CQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTs7O0FBL0RBO0FBQ0E7QUFDQTs7OztBQW5CQTtBQUNBO0FBa0ZBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/address/thai-address.tsx
