/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnBlur
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var containsNode = __webpack_require__(/*! fbjs/lib/containsNode */ "./node_modules/fbjs/lib/containsNode.js");

var getActiveElement = __webpack_require__(/*! fbjs/lib/getActiveElement */ "./node_modules/fbjs/lib/getActiveElement.js");

function editOnBlur(editor, e) {
  // In a contentEditable element, when you select a range and then click
  // another active element, this does trigger a `blur` event but will not
  // remove the DOM selection from the contenteditable.
  // This is consistent across all browsers, but we prefer that the editor
  // behave like a textarea, where a `blur` event clears the DOM selection.
  // We therefore force the issue to be certain, checking whether the active
  // element is `body` to force it when blurring occurs within the window (as
  // opposed to clicking to another tab or window).
  if (getActiveElement() === document.body) {
    var _selection = global.getSelection();

    var editorNode = editor.editor;

    if (_selection.rangeCount === 1 && containsNode(editorNode, _selection.anchorNode) && containsNode(editorNode, _selection.focusNode)) {
      _selection.removeAllRanges();
    }
  }

  var editorState = editor._latestEditorState;
  var currentSelection = editorState.getSelection();

  if (!currentSelection.getHasFocus()) {
    return;
  }

  var selection = currentSelection.set('hasFocus', false);
  editor.props.onBlur && editor.props.onBlur(e);
  editor.update(EditorState.acceptSelection(editorState, selection));
}

module.exports = editOnBlur;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPbkJsdXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvZWRpdE9uQmx1ci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIGVkaXRPbkJsdXJcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xuXG52YXIgY29udGFpbnNOb2RlID0gcmVxdWlyZSgnZmJqcy9saWIvY29udGFpbnNOb2RlJyk7XG52YXIgZ2V0QWN0aXZlRWxlbWVudCA9IHJlcXVpcmUoJ2ZianMvbGliL2dldEFjdGl2ZUVsZW1lbnQnKTtcblxuZnVuY3Rpb24gZWRpdE9uQmx1cihlZGl0b3IsIGUpIHtcbiAgLy8gSW4gYSBjb250ZW50RWRpdGFibGUgZWxlbWVudCwgd2hlbiB5b3Ugc2VsZWN0IGEgcmFuZ2UgYW5kIHRoZW4gY2xpY2tcbiAgLy8gYW5vdGhlciBhY3RpdmUgZWxlbWVudCwgdGhpcyBkb2VzIHRyaWdnZXIgYSBgYmx1cmAgZXZlbnQgYnV0IHdpbGwgbm90XG4gIC8vIHJlbW92ZSB0aGUgRE9NIHNlbGVjdGlvbiBmcm9tIHRoZSBjb250ZW50ZWRpdGFibGUuXG4gIC8vIFRoaXMgaXMgY29uc2lzdGVudCBhY3Jvc3MgYWxsIGJyb3dzZXJzLCBidXQgd2UgcHJlZmVyIHRoYXQgdGhlIGVkaXRvclxuICAvLyBiZWhhdmUgbGlrZSBhIHRleHRhcmVhLCB3aGVyZSBhIGBibHVyYCBldmVudCBjbGVhcnMgdGhlIERPTSBzZWxlY3Rpb24uXG4gIC8vIFdlIHRoZXJlZm9yZSBmb3JjZSB0aGUgaXNzdWUgdG8gYmUgY2VydGFpbiwgY2hlY2tpbmcgd2hldGhlciB0aGUgYWN0aXZlXG4gIC8vIGVsZW1lbnQgaXMgYGJvZHlgIHRvIGZvcmNlIGl0IHdoZW4gYmx1cnJpbmcgb2NjdXJzIHdpdGhpbiB0aGUgd2luZG93IChhc1xuICAvLyBvcHBvc2VkIHRvIGNsaWNraW5nIHRvIGFub3RoZXIgdGFiIG9yIHdpbmRvdykuXG4gIGlmIChnZXRBY3RpdmVFbGVtZW50KCkgPT09IGRvY3VtZW50LmJvZHkpIHtcbiAgICB2YXIgX3NlbGVjdGlvbiA9IGdsb2JhbC5nZXRTZWxlY3Rpb24oKTtcbiAgICB2YXIgZWRpdG9yTm9kZSA9IGVkaXRvci5lZGl0b3I7XG4gICAgaWYgKF9zZWxlY3Rpb24ucmFuZ2VDb3VudCA9PT0gMSAmJiBjb250YWluc05vZGUoZWRpdG9yTm9kZSwgX3NlbGVjdGlvbi5hbmNob3JOb2RlKSAmJiBjb250YWluc05vZGUoZWRpdG9yTm9kZSwgX3NlbGVjdGlvbi5mb2N1c05vZGUpKSB7XG4gICAgICBfc2VsZWN0aW9uLnJlbW92ZUFsbFJhbmdlcygpO1xuICAgIH1cbiAgfVxuXG4gIHZhciBlZGl0b3JTdGF0ZSA9IGVkaXRvci5fbGF0ZXN0RWRpdG9yU3RhdGU7XG4gIHZhciBjdXJyZW50U2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG4gIGlmICghY3VycmVudFNlbGVjdGlvbi5nZXRIYXNGb2N1cygpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgdmFyIHNlbGVjdGlvbiA9IGN1cnJlbnRTZWxlY3Rpb24uc2V0KCdoYXNGb2N1cycsIGZhbHNlKTtcbiAgZWRpdG9yLnByb3BzLm9uQmx1ciAmJiBlZGl0b3IucHJvcHMub25CbHVyKGUpO1xuICBlZGl0b3IudXBkYXRlKEVkaXRvclN0YXRlLmFjY2VwdFNlbGVjdGlvbihlZGl0b3JTdGF0ZSwgc2VsZWN0aW9uKSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZWRpdE9uQmx1cjsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnBlur.js
