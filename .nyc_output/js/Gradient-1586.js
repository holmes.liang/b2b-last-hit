/**
 * @param {Array.<Object>} colorStops
 */
var Gradient = function Gradient(colorStops) {
  this.colorStops = colorStops || [];
};

Gradient.prototype = {
  constructor: Gradient,
  addColorStop: function addColorStop(offset, color) {
    this.colorStops.push({
      offset: offset,
      color: color
    });
  }
};
var _default = Gradient;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9HcmFkaWVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvR3JhZGllbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAcGFyYW0ge0FycmF5LjxPYmplY3Q+fSBjb2xvclN0b3BzXG4gKi9cbnZhciBHcmFkaWVudCA9IGZ1bmN0aW9uIChjb2xvclN0b3BzKSB7XG4gIHRoaXMuY29sb3JTdG9wcyA9IGNvbG9yU3RvcHMgfHwgW107XG59O1xuXG5HcmFkaWVudC5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBHcmFkaWVudCxcbiAgYWRkQ29sb3JTdG9wOiBmdW5jdGlvbiAob2Zmc2V0LCBjb2xvcikge1xuICAgIHRoaXMuY29sb3JTdG9wcy5wdXNoKHtcbiAgICAgIG9mZnNldDogb2Zmc2V0LFxuICAgICAgY29sb3I6IGNvbG9yXG4gICAgfSk7XG4gIH1cbn07XG52YXIgX2RlZmF1bHQgPSBHcmFkaWVudDtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBUEE7QUFTQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Gradient.js
