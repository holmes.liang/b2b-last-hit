/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule moveBlockInContentState
 * @format
 * 
 */


var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var getNextDelimiterBlockKey = __webpack_require__(/*! ./getNextDelimiterBlockKey */ "./node_modules/draft-js/lib/getNextDelimiterBlockKey.js");

var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var OrderedMap = Immutable.OrderedMap,
    List = Immutable.List;

var transformBlock = function transformBlock(key, blockMap, func) {
  if (!key) {
    return;
  }

  var block = blockMap.get(key);

  if (!block) {
    return;
  }

  blockMap.set(key, func(block));
};

var updateBlockMapLinks = function updateBlockMapLinks(blockMap, originalBlockToBeMoved, originalTargetBlock, insertionMode, isExperimentalTreeBlock) {
  if (!isExperimentalTreeBlock) {
    return blockMap;
  } // possible values of 'insertionMode' are: 'after', 'before'


  var isInsertedAfterTarget = insertionMode === 'after';
  var originalBlockKey = originalBlockToBeMoved.getKey();
  var originalTargetKey = originalTargetBlock.getKey();
  var originalParentKey = originalBlockToBeMoved.getParentKey();
  var originalNextSiblingKey = originalBlockToBeMoved.getNextSiblingKey();
  var originalPrevSiblingKey = originalBlockToBeMoved.getPrevSiblingKey();
  var newParentKey = originalTargetBlock.getParentKey();
  var newNextSiblingKey = isInsertedAfterTarget ? originalTargetBlock.getNextSiblingKey() : originalTargetKey;
  var newPrevSiblingKey = isInsertedAfterTarget ? originalTargetKey : originalTargetBlock.getPrevSiblingKey();
  return blockMap.withMutations(function (blocks) {
    // update old parent
    transformBlock(originalParentKey, blocks, function (block) {
      var parentChildrenList = block.getChildKeys();
      return block.merge({
        children: parentChildrenList['delete'](parentChildrenList.indexOf(originalBlockKey))
      });
    }); // update old prev

    transformBlock(originalPrevSiblingKey, blocks, function (block) {
      return block.merge({
        nextSibling: originalNextSiblingKey
      });
    }); // update old next

    transformBlock(originalNextSiblingKey, blocks, function (block) {
      return block.merge({
        prevSibling: originalPrevSiblingKey
      });
    }); // update new next

    transformBlock(newNextSiblingKey, blocks, function (block) {
      return block.merge({
        prevSibling: originalBlockKey
      });
    }); // update new prev

    transformBlock(newPrevSiblingKey, blocks, function (block) {
      return block.merge({
        nextSibling: originalBlockKey
      });
    }); // update new parent

    transformBlock(newParentKey, blocks, function (block) {
      var newParentChildrenList = block.getChildKeys();
      var targetBlockIndex = newParentChildrenList.indexOf(originalTargetKey);
      var insertionIndex = isInsertedAfterTarget ? targetBlockIndex + 1 : targetBlockIndex !== 0 ? targetBlockIndex - 1 : 0;
      var newChildrenArray = newParentChildrenList.toArray();
      newChildrenArray.splice(insertionIndex, 0, originalBlockKey);
      return block.merge({
        children: List(newChildrenArray)
      });
    }); // update block

    transformBlock(originalBlockKey, blocks, function (block) {
      return block.merge({
        nextSibling: newNextSiblingKey,
        prevSibling: newPrevSiblingKey,
        parent: newParentKey
      });
    });
  });
};

var moveBlockInContentState = function moveBlockInContentState(contentState, blockToBeMoved, targetBlock, insertionMode) {
  !(insertionMode !== 'replace') ?  true ? invariant(false, 'Replacing blocks is not supported.') : undefined : void 0;
  var targetKey = targetBlock.getKey();
  var blockKey = blockToBeMoved.getKey();
  !(blockKey !== targetKey) ?  true ? invariant(false, 'Block cannot be moved next to itself.') : undefined : void 0;
  var blockMap = contentState.getBlockMap();
  var isExperimentalTreeBlock = blockToBeMoved instanceof ContentBlockNode;
  var blocksToBeMoved = [blockToBeMoved];
  var blockMapWithoutBlocksToBeMoved = blockMap['delete'](blockKey);

  if (isExperimentalTreeBlock) {
    blocksToBeMoved = [];
    blockMapWithoutBlocksToBeMoved = blockMap.withMutations(function (blocks) {
      var nextSiblingKey = blockToBeMoved.getNextSiblingKey();
      var nextDelimiterBlockKey = getNextDelimiterBlockKey(blockToBeMoved, blocks);
      blocks.toSeq().skipUntil(function (block) {
        return block.getKey() === blockKey;
      }).takeWhile(function (block) {
        var key = block.getKey();
        var isBlockToBeMoved = key === blockKey;
        var hasNextSiblingAndIsNotNextSibling = nextSiblingKey && key !== nextSiblingKey;
        var doesNotHaveNextSiblingAndIsNotDelimiter = !nextSiblingKey && block.getParentKey() && (!nextDelimiterBlockKey || key !== nextDelimiterBlockKey);
        return !!(isBlockToBeMoved || hasNextSiblingAndIsNotNextSibling || doesNotHaveNextSiblingAndIsNotDelimiter);
      }).forEach(function (block) {
        blocksToBeMoved.push(block);
        blocks['delete'](block.getKey());
      });
    });
  }

  var blocksBefore = blockMapWithoutBlocksToBeMoved.toSeq().takeUntil(function (v) {
    return v === targetBlock;
  });
  var blocksAfter = blockMapWithoutBlocksToBeMoved.toSeq().skipUntil(function (v) {
    return v === targetBlock;
  }).skip(1);
  var slicedBlocks = blocksToBeMoved.map(function (block) {
    return [block.getKey(), block];
  });
  var newBlocks = OrderedMap();

  if (insertionMode === 'before') {
    var blockBefore = contentState.getBlockBefore(targetKey);
    !(!blockBefore || blockBefore.getKey() !== blockToBeMoved.getKey()) ?  true ? invariant(false, 'Block cannot be moved next to itself.') : undefined : void 0;
    newBlocks = blocksBefore.concat([].concat(slicedBlocks, [[targetKey, targetBlock]]), blocksAfter).toOrderedMap();
  } else if (insertionMode === 'after') {
    var blockAfter = contentState.getBlockAfter(targetKey);
    !(!blockAfter || blockAfter.getKey() !== blockKey) ?  true ? invariant(false, 'Block cannot be moved next to itself.') : undefined : void 0;
    newBlocks = blocksBefore.concat([[targetKey, targetBlock]].concat(slicedBlocks), blocksAfter).toOrderedMap();
  }

  return contentState.merge({
    blockMap: updateBlockMapLinks(newBlocks, blockToBeMoved, targetBlock, insertionMode, isExperimentalTreeBlock),
    selectionBefore: contentState.getSelectionAfter(),
    selectionAfter: contentState.getSelectionAfter().merge({
      anchorKey: blockKey,
      focusKey: blockKey
    })
  });
};

module.exports = moveBlockInContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vdmVCbG9ja0luQ29udGVudFN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL21vdmVCbG9ja0luQ29udGVudFN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgbW92ZUJsb2NrSW5Db250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcblxudmFyIGdldE5leHREZWxpbWl0ZXJCbG9ja0tleSA9IHJlcXVpcmUoJy4vZ2V0TmV4dERlbGltaXRlckJsb2NrS2V5Jyk7XG52YXIgaW52YXJpYW50ID0gcmVxdWlyZSgnZmJqcy9saWIvaW52YXJpYW50Jyk7XG5cbnZhciBPcmRlcmVkTWFwID0gSW1tdXRhYmxlLk9yZGVyZWRNYXAsXG4gICAgTGlzdCA9IEltbXV0YWJsZS5MaXN0O1xuXG5cbnZhciB0cmFuc2Zvcm1CbG9jayA9IGZ1bmN0aW9uIHRyYW5zZm9ybUJsb2NrKGtleSwgYmxvY2tNYXAsIGZ1bmMpIHtcbiAgaWYgKCFrZXkpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgYmxvY2sgPSBibG9ja01hcC5nZXQoa2V5KTtcblxuICBpZiAoIWJsb2NrKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgYmxvY2tNYXAuc2V0KGtleSwgZnVuYyhibG9jaykpO1xufTtcblxudmFyIHVwZGF0ZUJsb2NrTWFwTGlua3MgPSBmdW5jdGlvbiB1cGRhdGVCbG9ja01hcExpbmtzKGJsb2NrTWFwLCBvcmlnaW5hbEJsb2NrVG9CZU1vdmVkLCBvcmlnaW5hbFRhcmdldEJsb2NrLCBpbnNlcnRpb25Nb2RlLCBpc0V4cGVyaW1lbnRhbFRyZWVCbG9jaykge1xuICBpZiAoIWlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgcmV0dXJuIGJsb2NrTWFwO1xuICB9XG4gIC8vIHBvc3NpYmxlIHZhbHVlcyBvZiAnaW5zZXJ0aW9uTW9kZScgYXJlOiAnYWZ0ZXInLCAnYmVmb3JlJ1xuICB2YXIgaXNJbnNlcnRlZEFmdGVyVGFyZ2V0ID0gaW5zZXJ0aW9uTW9kZSA9PT0gJ2FmdGVyJztcblxuICB2YXIgb3JpZ2luYWxCbG9ja0tleSA9IG9yaWdpbmFsQmxvY2tUb0JlTW92ZWQuZ2V0S2V5KCk7XG4gIHZhciBvcmlnaW5hbFRhcmdldEtleSA9IG9yaWdpbmFsVGFyZ2V0QmxvY2suZ2V0S2V5KCk7XG4gIHZhciBvcmlnaW5hbFBhcmVudEtleSA9IG9yaWdpbmFsQmxvY2tUb0JlTW92ZWQuZ2V0UGFyZW50S2V5KCk7XG4gIHZhciBvcmlnaW5hbE5leHRTaWJsaW5nS2V5ID0gb3JpZ2luYWxCbG9ja1RvQmVNb3ZlZC5nZXROZXh0U2libGluZ0tleSgpO1xuICB2YXIgb3JpZ2luYWxQcmV2U2libGluZ0tleSA9IG9yaWdpbmFsQmxvY2tUb0JlTW92ZWQuZ2V0UHJldlNpYmxpbmdLZXkoKTtcbiAgdmFyIG5ld1BhcmVudEtleSA9IG9yaWdpbmFsVGFyZ2V0QmxvY2suZ2V0UGFyZW50S2V5KCk7XG4gIHZhciBuZXdOZXh0U2libGluZ0tleSA9IGlzSW5zZXJ0ZWRBZnRlclRhcmdldCA/IG9yaWdpbmFsVGFyZ2V0QmxvY2suZ2V0TmV4dFNpYmxpbmdLZXkoKSA6IG9yaWdpbmFsVGFyZ2V0S2V5O1xuICB2YXIgbmV3UHJldlNpYmxpbmdLZXkgPSBpc0luc2VydGVkQWZ0ZXJUYXJnZXQgPyBvcmlnaW5hbFRhcmdldEtleSA6IG9yaWdpbmFsVGFyZ2V0QmxvY2suZ2V0UHJldlNpYmxpbmdLZXkoKTtcblxuICByZXR1cm4gYmxvY2tNYXAud2l0aE11dGF0aW9ucyhmdW5jdGlvbiAoYmxvY2tzKSB7XG4gICAgLy8gdXBkYXRlIG9sZCBwYXJlbnRcbiAgICB0cmFuc2Zvcm1CbG9jayhvcmlnaW5hbFBhcmVudEtleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHZhciBwYXJlbnRDaGlsZHJlbkxpc3QgPSBibG9jay5nZXRDaGlsZEtleXMoKTtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIGNoaWxkcmVuOiBwYXJlbnRDaGlsZHJlbkxpc3RbJ2RlbGV0ZSddKHBhcmVudENoaWxkcmVuTGlzdC5pbmRleE9mKG9yaWdpbmFsQmxvY2tLZXkpKVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgb2xkIHByZXZcbiAgICB0cmFuc2Zvcm1CbG9jayhvcmlnaW5hbFByZXZTaWJsaW5nS2V5LCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgbmV4dFNpYmxpbmc6IG9yaWdpbmFsTmV4dFNpYmxpbmdLZXlcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIG9sZCBuZXh0XG4gICAgdHJhbnNmb3JtQmxvY2sob3JpZ2luYWxOZXh0U2libGluZ0tleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIHByZXZTaWJsaW5nOiBvcmlnaW5hbFByZXZTaWJsaW5nS2V5XG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIC8vIHVwZGF0ZSBuZXcgbmV4dFxuICAgIHRyYW5zZm9ybUJsb2NrKG5ld05leHRTaWJsaW5nS2V5LCBibG9ja3MsIGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgcHJldlNpYmxpbmc6IG9yaWdpbmFsQmxvY2tLZXlcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIG5ldyBwcmV2XG4gICAgdHJhbnNmb3JtQmxvY2sobmV3UHJldlNpYmxpbmdLZXksIGJsb2NrcywgZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgICByZXR1cm4gYmxvY2subWVyZ2Uoe1xuICAgICAgICBuZXh0U2libGluZzogb3JpZ2luYWxCbG9ja0tleVxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAvLyB1cGRhdGUgbmV3IHBhcmVudFxuICAgIHRyYW5zZm9ybUJsb2NrKG5ld1BhcmVudEtleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHZhciBuZXdQYXJlbnRDaGlsZHJlbkxpc3QgPSBibG9jay5nZXRDaGlsZEtleXMoKTtcbiAgICAgIHZhciB0YXJnZXRCbG9ja0luZGV4ID0gbmV3UGFyZW50Q2hpbGRyZW5MaXN0LmluZGV4T2Yob3JpZ2luYWxUYXJnZXRLZXkpO1xuXG4gICAgICB2YXIgaW5zZXJ0aW9uSW5kZXggPSBpc0luc2VydGVkQWZ0ZXJUYXJnZXQgPyB0YXJnZXRCbG9ja0luZGV4ICsgMSA6IHRhcmdldEJsb2NrSW5kZXggIT09IDAgPyB0YXJnZXRCbG9ja0luZGV4IC0gMSA6IDA7XG5cbiAgICAgIHZhciBuZXdDaGlsZHJlbkFycmF5ID0gbmV3UGFyZW50Q2hpbGRyZW5MaXN0LnRvQXJyYXkoKTtcbiAgICAgIG5ld0NoaWxkcmVuQXJyYXkuc3BsaWNlKGluc2VydGlvbkluZGV4LCAwLCBvcmlnaW5hbEJsb2NrS2V5KTtcblxuICAgICAgcmV0dXJuIGJsb2NrLm1lcmdlKHtcbiAgICAgICAgY2hpbGRyZW46IExpc3QobmV3Q2hpbGRyZW5BcnJheSlcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gdXBkYXRlIGJsb2NrXG4gICAgdHJhbnNmb3JtQmxvY2sob3JpZ2luYWxCbG9ja0tleSwgYmxvY2tzLCBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIHJldHVybiBibG9jay5tZXJnZSh7XG4gICAgICAgIG5leHRTaWJsaW5nOiBuZXdOZXh0U2libGluZ0tleSxcbiAgICAgICAgcHJldlNpYmxpbmc6IG5ld1ByZXZTaWJsaW5nS2V5LFxuICAgICAgICBwYXJlbnQ6IG5ld1BhcmVudEtleVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH0pO1xufTtcblxudmFyIG1vdmVCbG9ja0luQ29udGVudFN0YXRlID0gZnVuY3Rpb24gbW92ZUJsb2NrSW5Db250ZW50U3RhdGUoY29udGVudFN0YXRlLCBibG9ja1RvQmVNb3ZlZCwgdGFyZ2V0QmxvY2ssIGluc2VydGlvbk1vZGUpIHtcbiAgIShpbnNlcnRpb25Nb2RlICE9PSAncmVwbGFjZScpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ1JlcGxhY2luZyBibG9ja3MgaXMgbm90IHN1cHBvcnRlZC4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgdmFyIHRhcmdldEtleSA9IHRhcmdldEJsb2NrLmdldEtleSgpO1xuICB2YXIgYmxvY2tLZXkgPSBibG9ja1RvQmVNb3ZlZC5nZXRLZXkoKTtcblxuICAhKGJsb2NrS2V5ICE9PSB0YXJnZXRLZXkpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0Jsb2NrIGNhbm5vdCBiZSBtb3ZlZCBuZXh0IHRvIGl0c2VsZi4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgdmFyIGJsb2NrTWFwID0gY29udGVudFN0YXRlLmdldEJsb2NrTWFwKCk7XG4gIHZhciBpc0V4cGVyaW1lbnRhbFRyZWVCbG9jayA9IGJsb2NrVG9CZU1vdmVkIGluc3RhbmNlb2YgQ29udGVudEJsb2NrTm9kZTtcblxuICB2YXIgYmxvY2tzVG9CZU1vdmVkID0gW2Jsb2NrVG9CZU1vdmVkXTtcbiAgdmFyIGJsb2NrTWFwV2l0aG91dEJsb2Nrc1RvQmVNb3ZlZCA9IGJsb2NrTWFwWydkZWxldGUnXShibG9ja0tleSk7XG5cbiAgaWYgKGlzRXhwZXJpbWVudGFsVHJlZUJsb2NrKSB7XG4gICAgYmxvY2tzVG9CZU1vdmVkID0gW107XG4gICAgYmxvY2tNYXBXaXRob3V0QmxvY2tzVG9CZU1vdmVkID0gYmxvY2tNYXAud2l0aE11dGF0aW9ucyhmdW5jdGlvbiAoYmxvY2tzKSB7XG4gICAgICB2YXIgbmV4dFNpYmxpbmdLZXkgPSBibG9ja1RvQmVNb3ZlZC5nZXROZXh0U2libGluZ0tleSgpO1xuICAgICAgdmFyIG5leHREZWxpbWl0ZXJCbG9ja0tleSA9IGdldE5leHREZWxpbWl0ZXJCbG9ja0tleShibG9ja1RvQmVNb3ZlZCwgYmxvY2tzKTtcblxuICAgICAgYmxvY2tzLnRvU2VxKCkuc2tpcFVudGlsKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgICByZXR1cm4gYmxvY2suZ2V0S2V5KCkgPT09IGJsb2NrS2V5O1xuICAgICAgfSkudGFrZVdoaWxlKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgICB2YXIga2V5ID0gYmxvY2suZ2V0S2V5KCk7XG4gICAgICAgIHZhciBpc0Jsb2NrVG9CZU1vdmVkID0ga2V5ID09PSBibG9ja0tleTtcbiAgICAgICAgdmFyIGhhc05leHRTaWJsaW5nQW5kSXNOb3ROZXh0U2libGluZyA9IG5leHRTaWJsaW5nS2V5ICYmIGtleSAhPT0gbmV4dFNpYmxpbmdLZXk7XG4gICAgICAgIHZhciBkb2VzTm90SGF2ZU5leHRTaWJsaW5nQW5kSXNOb3REZWxpbWl0ZXIgPSAhbmV4dFNpYmxpbmdLZXkgJiYgYmxvY2suZ2V0UGFyZW50S2V5KCkgJiYgKCFuZXh0RGVsaW1pdGVyQmxvY2tLZXkgfHwga2V5ICE9PSBuZXh0RGVsaW1pdGVyQmxvY2tLZXkpO1xuXG4gICAgICAgIHJldHVybiAhIShpc0Jsb2NrVG9CZU1vdmVkIHx8IGhhc05leHRTaWJsaW5nQW5kSXNOb3ROZXh0U2libGluZyB8fCBkb2VzTm90SGF2ZU5leHRTaWJsaW5nQW5kSXNOb3REZWxpbWl0ZXIpO1xuICAgICAgfSkuZm9yRWFjaChmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgICAgYmxvY2tzVG9CZU1vdmVkLnB1c2goYmxvY2spO1xuICAgICAgICBibG9ja3NbJ2RlbGV0ZSddKGJsb2NrLmdldEtleSgpKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgdmFyIGJsb2Nrc0JlZm9yZSA9IGJsb2NrTWFwV2l0aG91dEJsb2Nrc1RvQmVNb3ZlZC50b1NlcSgpLnRha2VVbnRpbChmdW5jdGlvbiAodikge1xuICAgIHJldHVybiB2ID09PSB0YXJnZXRCbG9jaztcbiAgfSk7XG5cbiAgdmFyIGJsb2Nrc0FmdGVyID0gYmxvY2tNYXBXaXRob3V0QmxvY2tzVG9CZU1vdmVkLnRvU2VxKCkuc2tpcFVudGlsKGZ1bmN0aW9uICh2KSB7XG4gICAgcmV0dXJuIHYgPT09IHRhcmdldEJsb2NrO1xuICB9KS5za2lwKDEpO1xuXG4gIHZhciBzbGljZWRCbG9ja3MgPSBibG9ja3NUb0JlTW92ZWQubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgIHJldHVybiBbYmxvY2suZ2V0S2V5KCksIGJsb2NrXTtcbiAgfSk7XG5cbiAgdmFyIG5ld0Jsb2NrcyA9IE9yZGVyZWRNYXAoKTtcblxuICBpZiAoaW5zZXJ0aW9uTW9kZSA9PT0gJ2JlZm9yZScpIHtcbiAgICB2YXIgYmxvY2tCZWZvcmUgPSBjb250ZW50U3RhdGUuZ2V0QmxvY2tCZWZvcmUodGFyZ2V0S2V5KTtcblxuICAgICEoIWJsb2NrQmVmb3JlIHx8IGJsb2NrQmVmb3JlLmdldEtleSgpICE9PSBibG9ja1RvQmVNb3ZlZC5nZXRLZXkoKSkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnQmxvY2sgY2Fubm90IGJlIG1vdmVkIG5leHQgdG8gaXRzZWxmLicpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICAgIG5ld0Jsb2NrcyA9IGJsb2Nrc0JlZm9yZS5jb25jYXQoW10uY29uY2F0KHNsaWNlZEJsb2NrcywgW1t0YXJnZXRLZXksIHRhcmdldEJsb2NrXV0pLCBibG9ja3NBZnRlcikudG9PcmRlcmVkTWFwKCk7XG4gIH0gZWxzZSBpZiAoaW5zZXJ0aW9uTW9kZSA9PT0gJ2FmdGVyJykge1xuICAgIHZhciBibG9ja0FmdGVyID0gY29udGVudFN0YXRlLmdldEJsb2NrQWZ0ZXIodGFyZ2V0S2V5KTtcblxuICAgICEoIWJsb2NrQWZ0ZXIgfHwgYmxvY2tBZnRlci5nZXRLZXkoKSAhPT0gYmxvY2tLZXkpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ0Jsb2NrIGNhbm5vdCBiZSBtb3ZlZCBuZXh0IHRvIGl0c2VsZi4nKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG5cbiAgICBuZXdCbG9ja3MgPSBibG9ja3NCZWZvcmUuY29uY2F0KFtbdGFyZ2V0S2V5LCB0YXJnZXRCbG9ja11dLmNvbmNhdChzbGljZWRCbG9ja3MpLCBibG9ja3NBZnRlcikudG9PcmRlcmVkTWFwKCk7XG4gIH1cblxuICByZXR1cm4gY29udGVudFN0YXRlLm1lcmdlKHtcbiAgICBibG9ja01hcDogdXBkYXRlQmxvY2tNYXBMaW5rcyhuZXdCbG9ja3MsIGJsb2NrVG9CZU1vdmVkLCB0YXJnZXRCbG9jaywgaW5zZXJ0aW9uTW9kZSwgaXNFeHBlcmltZW50YWxUcmVlQmxvY2spLFxuICAgIHNlbGVjdGlvbkJlZm9yZTogY29udGVudFN0YXRlLmdldFNlbGVjdGlvbkFmdGVyKCksXG4gICAgc2VsZWN0aW9uQWZ0ZXI6IGNvbnRlbnRTdGF0ZS5nZXRTZWxlY3Rpb25BZnRlcigpLm1lcmdlKHtcbiAgICAgIGFuY2hvcktleTogYmxvY2tLZXksXG4gICAgICBmb2N1c0tleTogYmxvY2tLZXlcbiAgICB9KVxuICB9KTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gbW92ZUJsb2NrSW5Db250ZW50U3RhdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFIQTtBQVFBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/moveBlockInContentState.js
