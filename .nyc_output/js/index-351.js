

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var generate_1 = __importDefault(__webpack_require__(/*! ./generate */ "./node_modules/@ant-design/colors/lib/generate.js"));

exports.generate = generate_1.default;
var presetPrimaryColors = {
  red: '#F5222D',
  volcano: '#FA541C',
  orange: '#FA8C16',
  gold: '#FAAD14',
  yellow: '#FADB14',
  lime: '#A0D911',
  green: '#52C41A',
  cyan: '#13C2C2',
  blue: '#1890FF',
  geekblue: '#2F54EB',
  purple: '#722ED1',
  magenta: '#EB2F96',
  grey: '#666666'
};
exports.presetPrimaryColors = presetPrimaryColors;
var presetPalettes = {};
exports.presetPalettes = presetPalettes;
Object.keys(presetPrimaryColors).forEach(function (key) {
  presetPalettes[key] = generate_1.default(presetPrimaryColors[key]);
  presetPalettes[key].primary = presetPalettes[key][5];
});
var red = presetPalettes.red;
exports.red = red;
var volcano = presetPalettes.volcano;
exports.volcano = volcano;
var gold = presetPalettes.gold;
exports.gold = gold;
var orange = presetPalettes.orange;
exports.orange = orange;
var yellow = presetPalettes.yellow;
exports.yellow = yellow;
var lime = presetPalettes.lime;
exports.lime = lime;
var green = presetPalettes.green;
exports.green = green;
var cyan = presetPalettes.cyan;
exports.cyan = cyan;
var blue = presetPalettes.blue;
exports.blue = blue;
var geekblue = presetPalettes.geekblue;
exports.geekblue = geekblue;
var purple = presetPalettes.purple;
exports.purple = purple;
var magenta = presetPalettes.magenta;
exports.magenta = magenta;
var grey = presetPalettes.grey;
exports.grey = grey;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvQGFudC1kZXNpZ24vY29sb3JzL2xpYi9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL0BhbnQtZGVzaWduL2NvbG9ycy9saWIvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19pbXBvcnREZWZhdWx0ID0gKHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQpIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IFwiZGVmYXVsdFwiOiBtb2QgfTtcbn07XG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XG52YXIgZ2VuZXJhdGVfMSA9IF9faW1wb3J0RGVmYXVsdChyZXF1aXJlKFwiLi9nZW5lcmF0ZVwiKSk7XG5leHBvcnRzLmdlbmVyYXRlID0gZ2VuZXJhdGVfMS5kZWZhdWx0O1xudmFyIHByZXNldFByaW1hcnlDb2xvcnMgPSB7XG4gICAgcmVkOiAnI0Y1MjIyRCcsXG4gICAgdm9sY2FubzogJyNGQTU0MUMnLFxuICAgIG9yYW5nZTogJyNGQThDMTYnLFxuICAgIGdvbGQ6ICcjRkFBRDE0JyxcbiAgICB5ZWxsb3c6ICcjRkFEQjE0JyxcbiAgICBsaW1lOiAnI0EwRDkxMScsXG4gICAgZ3JlZW46ICcjNTJDNDFBJyxcbiAgICBjeWFuOiAnIzEzQzJDMicsXG4gICAgYmx1ZTogJyMxODkwRkYnLFxuICAgIGdlZWtibHVlOiAnIzJGNTRFQicsXG4gICAgcHVycGxlOiAnIzcyMkVEMScsXG4gICAgbWFnZW50YTogJyNFQjJGOTYnLFxuICAgIGdyZXk6ICcjNjY2NjY2Jyxcbn07XG5leHBvcnRzLnByZXNldFByaW1hcnlDb2xvcnMgPSBwcmVzZXRQcmltYXJ5Q29sb3JzO1xudmFyIHByZXNldFBhbGV0dGVzID0ge307XG5leHBvcnRzLnByZXNldFBhbGV0dGVzID0gcHJlc2V0UGFsZXR0ZXM7XG5PYmplY3Qua2V5cyhwcmVzZXRQcmltYXJ5Q29sb3JzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICBwcmVzZXRQYWxldHRlc1trZXldID0gZ2VuZXJhdGVfMS5kZWZhdWx0KHByZXNldFByaW1hcnlDb2xvcnNba2V5XSk7XG4gICAgcHJlc2V0UGFsZXR0ZXNba2V5XS5wcmltYXJ5ID0gcHJlc2V0UGFsZXR0ZXNba2V5XVs1XTtcbn0pO1xudmFyIHJlZCA9IHByZXNldFBhbGV0dGVzLnJlZDtcbmV4cG9ydHMucmVkID0gcmVkO1xudmFyIHZvbGNhbm8gPSBwcmVzZXRQYWxldHRlcy52b2xjYW5vO1xuZXhwb3J0cy52b2xjYW5vID0gdm9sY2FubztcbnZhciBnb2xkID0gcHJlc2V0UGFsZXR0ZXMuZ29sZDtcbmV4cG9ydHMuZ29sZCA9IGdvbGQ7XG52YXIgb3JhbmdlID0gcHJlc2V0UGFsZXR0ZXMub3JhbmdlO1xuZXhwb3J0cy5vcmFuZ2UgPSBvcmFuZ2U7XG52YXIgeWVsbG93ID0gcHJlc2V0UGFsZXR0ZXMueWVsbG93O1xuZXhwb3J0cy55ZWxsb3cgPSB5ZWxsb3c7XG52YXIgbGltZSA9IHByZXNldFBhbGV0dGVzLmxpbWU7XG5leHBvcnRzLmxpbWUgPSBsaW1lO1xudmFyIGdyZWVuID0gcHJlc2V0UGFsZXR0ZXMuZ3JlZW47XG5leHBvcnRzLmdyZWVuID0gZ3JlZW47XG52YXIgY3lhbiA9IHByZXNldFBhbGV0dGVzLmN5YW47XG5leHBvcnRzLmN5YW4gPSBjeWFuO1xudmFyIGJsdWUgPSBwcmVzZXRQYWxldHRlcy5ibHVlO1xuZXhwb3J0cy5ibHVlID0gYmx1ZTtcbnZhciBnZWVrYmx1ZSA9IHByZXNldFBhbGV0dGVzLmdlZWtibHVlO1xuZXhwb3J0cy5nZWVrYmx1ZSA9IGdlZWtibHVlO1xudmFyIHB1cnBsZSA9IHByZXNldFBhbGV0dGVzLnB1cnBsZTtcbmV4cG9ydHMucHVycGxlID0gcHVycGxlO1xudmFyIG1hZ2VudGEgPSBwcmVzZXRQYWxldHRlcy5tYWdlbnRhO1xuZXhwb3J0cy5tYWdlbnRhID0gbWFnZW50YTtcbnZhciBncmV5ID0gcHJlc2V0UGFsZXR0ZXMuZ3JleTtcbmV4cG9ydHMuZ3JleSA9IGdyZXk7XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/@ant-design/colors/lib/index.js
