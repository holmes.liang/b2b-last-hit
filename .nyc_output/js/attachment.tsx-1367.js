__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-images */ "./node_modules/react-images/lib/Lightbox.js");
/* harmony import */ var react_images__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_images__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _my_types__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @my-types */ "./src/types/index.tsx");
/* harmony import */ var _desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @desk-component/attachment/doc-types */ "./src/app/desk/component/attachment/doc-types.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @assets/attachment.svg */ "./src/assets/attachment.svg");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(_assets_attachment_svg__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @desk-component/icons/pdf.png */ "./src/app/desk/component/icons/pdf.png");
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(_desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_21__);











var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/attachment/attachment.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .attachment-uploader-wrap {\n          .ant-upload.ant-upload-drag\xA0{\n              display: table;\n              width: 104px;\n              height: 104px;\n              margin-right: 8px;\n              margin-bottom: 8px;\n              .ant-upload.ant-upload-btn {\n                display: table-cell;\n                width: 100%;\n                height: 100%;\n                padding: 8px;\n                text-align: center;\n                vertical-align: middle;\n                .anticon.anticon-plus {\n                  font-size: 14px;\n                  color: rgba(0, 0, 0, 0.65);\n                }\n              }\n           }\n        }\n        .ant-upload-text {\n          text-transform: uppercase;\n          font-size: 12px;\n          width: 86px;\n          overflow: hidden;\n          text-overflow: ellipsis;\n        }\n        .operate {\n          position: absolute;\n          z-index: 22;\n          width: 100%; \n          height: 23px; \n          bottom: 2px;\n          margin-left: -5px;\n          display: none;\n          .bg {\n            background: #fff;\n            height: 23px; \n            zoom: 1; \n          }\n          .con {\n            position: relative;\n          }\n          a {\n            display: inline-block !important;\n            height: 22px !important;\n            line-height: 22px !important;\n            .anticon {\n              font-size: 20px;\n            }\n            &.a-pl {\n              padding-left: 48px;\n            }\n          }\n        }\n        .attachment-preview-list {\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n        }\n        .attachment-preview-item {\n            position: relative;\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-direction: column;\n            flex-direction: column;\n            margin: 0 15px 15px 0;\n            padding: 5px;\n            width: 135px;\n            height: 135px;\n            text-align: center;\n            border: 1px solid #ebebeb;\n            border-radius: 2px;\n            cursor: pointer;\n            img {\n                flex: 1 1 0%;\n                max-height: 100px;\n            }\n            a {\n                display: block;\n                height: 23px;\n                line-height: 23px;\n                white-space: nowrap;\n                overflow: hidden;\n                text-overflow: ellipsis;\n                color: #878787;\n                &.img-a {\n                  height: auto;\n                }\n            }\n          \n            .attachment-preview-item__oper {\n                position: absolute;\n                top: -9px;\n                right: -10px;\n                display: none;\n                z-index: 22;\n                img {\n                  width: 23px;\n                }\n                i{\n                    position: relative;\n                    color: #5fbfec;\n                    z-index: 25;\n                }\n            }\n            .file-name {\n              display: block;\n            }\n            &:hover {\n                .attachment-preview-item__oper, .operate {\n                    display: block;\n                }\n                .file-name {\n                  display: none;\n                }\n            }\n         }\n        .attachment-uploader-list {\n            display: -ms-flexbox;\n            display: flex;\n            -ms-flex-wrap: wrap;\n            flex-wrap: wrap;\n            overflow-x: auto;\n            overflow-y: hidden;\n            .attachment-uploader-item {\n                margin-right: 30px;\n                text-align: center;\n                a,img {\n                    display: block;\n                    height: 65px;\n                    width: 80px;\n                    margin: 0 auto;\n                    opacity: .6;\n                    background-size: contain;\n                    background-repeat: no-repeat;\n                    background-position: center bottom;\n                }\n                 p {\n                    font-size: 13px;\n                    margin-top: 5px;\n                    color: #9e9e9e;\n                    text-transform: uppercase;\n                }\n            }\n        } \n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}













var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_19__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

var UploadFileState = {
  PROGRESS: 0,
  COMPLETED: 1
};
var uploadFileMgr = {
  startUpload: function startUpload(list, docType, callback) {
    var _this = this;

    var result = list.map(function (file) {
      var name = file.name,
          uid = file.uid,
          type = file.type;
      var isImage = type.indexOf("image/") === 0;

      if (isImage) {
        _this.getBase64(file, function (url) {
          return callback(file, url);
        });
      }

      return {
        uid: uid,
        state: UploadFileState.PROGRESS,
        fileName: name,
        type: docType,
        isImage: isImage,
        isPdf: type.includes("pdf")
      };
    });
    return result;
  },
  getBase64: function getBase64(file, callback) {
    var reader = new FileReader();
    reader.addEventListener("load", function () {
      return callback(reader.result);
    });
    reader.readAsDataURL(file);
  },
  getFileRenderUrl: function getFileRenderUrl(item, policyId, downUrl) {
    var id = item.id,
        url = item.url,
        isImage = item.isImage,
        isPdf = item.isPdf,
        attachId = item.attachId;
    var authKey = _common__WEBPACK_IMPORTED_MODULE_18__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_18__["Consts"].AUTH_KEY);

    if (isPdf) {
      return _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_21___default.a;
    }

    if (!isImage) {
      return _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_20___default.a;
    }

    if (id) {
      // todo 验证
      return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(downUrl ? "".concat(downUrl, "/").concat(id, "/file?access_token=").concat(authKey, "&resize=sm") : "/policies/".concat(policyId, "/attaches/").concat(id, "/file?access_token=").concat(authKey, "&resize=sm"));
    }

    if (attachId) {
      return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(downUrl ? "".concat(downUrl, "/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=sm") : "/policies/".concat(policyId, "/attaches/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=sm"));
    }

    if (url) {
      return url;
    }

    return null;
  },
  getFileRenderUrlMd: function getFileRenderUrlMd(item, policyId, downUrl) {
    var id = item.id,
        url = item.url,
        isImage = item.isImage,
        isPdf = item.isPdf,
        attachId = item.attachId;
    var authKey = _common__WEBPACK_IMPORTED_MODULE_18__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_18__["Consts"].AUTH_KEY);

    if (isPdf) {
      return _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_21___default.a;
    }

    if (!isImage) {
      return _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_20___default.a;
    }

    if (id) {
      return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(downUrl ? "".concat(downUrl, "/").concat(id, "/file?access_token=").concat(authKey, "&resize=md") : "/policies/".concat(policyId, "/attaches/").concat(id, "/file?access_token=").concat(authKey, "&resize=md"));
    }

    if (attachId) {
      return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(downUrl ? "".concat(downUrl, "/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=md") : "/policies/".concat(policyId, "/attaches/").concat(attachId, "/file?access_token=").concat(authKey, "&resize=md"));
    }

    if (url) {
      return url;
    }

    return null;
  },
  getFileDownloadUrl: function getFileDownloadUrl(item, policyId, downUrl) {
    var id = item.id;
    var authKey = _common__WEBPACK_IMPORTED_MODULE_18__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_18__["Consts"].AUTH_KEY);

    if (id) {
      return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(downUrl ? "".concat(downUrl, "/").concat(id, "/file?access_token=").concat(authKey, "&resize=orig") : "/policies/".concat(policyId, "/attaches/").concat(id, "/file?access_token=").concat(authKey, "&resize=orig"));
    }

    return "";
  },
  completeUpload: function completeUpload(file, fid) {
    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, file, {
      state: UploadFileState.COMPLETED,
      id: fid
    });
  }
};

var Attachment =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_9__["default"])(Attachment, _ModelWidget);

  function Attachment(props, context) {
    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Attachment);

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Attachment).call(this, props, context));

    _this2.handleFileRemove = function (uid) {
      var fileList = _this2.state.fileList;

      var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(fileList);

      var policyId = _this2.getValueFromModel(_this2.generatePropName("policyId", _this2.props.dataIdPrefix));

      var index = nextList.findIndex(function (item) {
        return item.uid === uid;
      });

      if (index === -1) {
        return;
      }

      var file = nextList.splice(index, 1)[0];

      _this2.setState({
        fileList: nextList,
        imageList: _this2.getImgList(nextList)
      }, function () {
        _this2.storeFiles();
      });

      if (file.id) {
        _this2.deletePolicyAttach(policyId, file.id, file.type);
      }
    };

    _this2.setLightbox = function (nextLightbox) {
      _this2.setState(function (_ref) {
        var lightbox = _ref.lightbox;
        return {
          lightbox: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, lightbox, {}, nextLightbox)
        };
      });
    };

    _this2.handleFileChange = function (info) {
      var fileList = _this2.state.fileList;
      var uploadedFileList = info.fileList;
      uploadedFileList.forEach(function (file) {
        if (file.response) {
          var fid = file.attachId = file.response.respData;
          console.log(info, "info");
          var uid = file.uid;

          if (fid && fileList.findIndex(function (item) {
            return item.id === fid;
          }) !== -1) {
            return;
          }

          _this2.setState(function (_ref2) {
            var fileList = _ref2.fileList;

            var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(fileList);

            var index = fileList.findIndex(function (item) {
              return item.uid === uid;
            });

            if (index !== -1) {
              nextList[index] = uploadFileMgr.completeUpload(nextList[index], fid);
            }

            return {
              fileList: nextList,
              imageList: _this2.getImgList(nextList)
            };
          }, function () {
            _this2.storeFiles();
          });
        }
      });
    };

    _this2.handleFileBeforeUpload = function (file, files, docType) {
      var willUploadedFiles = uploadFileMgr.startUpload(files, docType, function (file, url) {
        var fileList = _this2.state.fileList;
        var index = fileList.findIndex(function (item) {
          return item.uid === file.uid;
        });

        if (index !== -1) {
          var nextList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(fileList);

          nextList[index] = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_10__["default"])({}, nextList[index], {
            url: url
          });

          _this2.setState({
            fileList: nextList
          });
        }
      });

      _this2.setState({
        fileList: [].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(_this2.state.fileList), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(willUploadedFiles)),
        imageList: _this2.getImgList([].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(_this2.state.fileList), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_3__["default"])(willUploadedFiles))),
        disableBtn: true
      });

      return true;
    };

    _this2.gotoPrevLightboxImage = function () {
      var index = _this2.state.lightbox.index;

      if (index === 0) {
        return;
      }

      _this2.setLightbox({
        index: index - 1
      });
    };

    _this2.gotoNextLightboxImage = function () {
      var _this2$state = _this2.state,
          index = _this2$state.lightbox.index,
          fileList = _this2$state.fileList;

      if (index === fileList.length - 1) {
        return;
      }

      _this2.setLightbox({
        index: index + 1
      });
    };

    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_5__["default"])(Attachment, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (!this.props.docTypes) {
        this.getUploadList();
      }

      this.getAttaches();
    }
  }, {
    key: "getUploadList",
    value: function () {
      var _getUploadList = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        var _this$props, model, getUploadList, res, respData;

        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this$props = this.props, model = _this$props.model, getUploadList = _this$props.getUploadList;
                _context.prev = 1;

                if (!getUploadList) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return getUploadList();

              case 5:
                res = _context.sent;
                _context.next = 11;
                break;

              case 8:
                _context.next = 10;
                return this.getHelpers().getAjax().post("/policies/eligiblesupportingdocs", model, {});

              case 10:
                res = _context.sent;

              case 11:
                respData = lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(res, "body.respData", []);
                this.setState({
                  uploadList: respData
                });
                _context.next = 17;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](1);

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 15]]);
      }));

      function getUploadList() {
        return _getUploadList.apply(this, arguments);
      }

      return getUploadList;
    }()
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();
      var _this$props$isView = this.props.isView,
          isView = _this$props$isView === void 0 ? false : _this$props$isView;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(C.Attachment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 194
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "clearfix",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 195
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "attachmentForm",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 196
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 197
        },
        __self: this
      }, this.renderPreview()), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "attachment-uploader-wrap",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 198
        },
        __self: this
      }, !isView && this.renderUploalders()))), this.renderImageLightbox());
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Attachment: _common_3rd__WEBPACK_IMPORTED_MODULE_11__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Attachment.prototype), "initState", this).call(this), {
        uploadList: [],
        fileList: [],
        lightbox: {
          index: -1,
          visible: false
        },
        imageList: [],
        disableBtn: false
      });
    }
  }, {
    key: "generateExtPropName",
    value: function generateExtPropName(propName) {
      var endoFixed = this.props.endoFixed;
      if (endoFixed && propName) return "ext.".concat(endoFixed, ".").concat(propName);
      if (endoFixed) return "ext.".concat(endoFixed);
      return "ext.".concat(propName);
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (!dataIdPrefix) return "".concat(propName);
      return "".concat(dataIdPrefix, ".").concat(propName);
    }
  }, {
    key: "docTypeChange",
    value: function docTypeChange(docType) {
      var docTypeArr = docType.toLowerCase().split("_");
      docTypeArr.map(function (every, index) {
        if (index > 0) {
          docTypeArr[index] = every.replace(/\b(\w)/g, function (m) {
            return m.toUpperCase();
          });
        }
      });
      return docTypeArr;
    }
  }, {
    key: "getImgList",
    value: function getImgList(fileList) {
      var _this3 = this;

      var imgList = [];
      if (!fileList) return [];
      fileList.map(function (every) {
        if (_this3.isPicture(every)) {
          imgList.push(every);
        }
      });
      return imgList;
    }
  }, {
    key: "deletePolicyAttach",
    value: function deletePolicyAttach(policyId, attachId, type) {
      var _this4 = this;

      var deleteUrl = this.props.deleteUrl;
      var dataFixed = this.props.dataFixed;
      this.getHelpers().getAjax().delete(deleteUrl ? "".concat(deleteUrl, "/").concat(attachId) : "/policies/".concat(policyId, "/attaches/").concat(attachId), {}, {}).then(function (response) {
        var docTypeArr = _this4.docTypeChange(type);

        var field = docTypeArr.join("");

        var suppDocs = _this4.getValueFromModel(_this4.generatePropName(dataFixed || _this4.generateExtPropName("suppDocs"), _this4.props.dataIdPrefix));

        for (var item in suppDocs) {
          if (item === field && suppDocs[item].includes(attachId)) {
            suppDocs[item].splice(suppDocs[item].indexOf(attachId), 1);
          }
        }

        _this4.setValueToModel(suppDocs, _this4.generatePropName(dataFixed || _this4.generateExtPropName("suppDocs"), _this4.props.dataIdPrefix));

        antd__WEBPACK_IMPORTED_MODULE_14__["notification"].success({
          message: "success",
          description: "Your file is deleted successfully"
        });
      }).catch(function (error) {
        antd__WEBPACK_IMPORTED_MODULE_14__["notification"].error({
          message: "error",
          description: error.message
        });
      });
    }
  }, {
    key: "renderPreview",
    value: function renderPreview() {
      var _this5 = this;

      var _this$state = this.state,
          fileList = _this$state.fileList,
          uploadList = _this$state.uploadList;
      var _this$props$isView2 = this.props.isView,
          isView = _this$props$isView2 === void 0 ? false : _this$props$isView2;
      var filterDocTypes = !this.props.docTypes ? uploadList : this.props.docTypes;
      var filterDocTypesType = [];
      filterDocTypes.map(function (every) {
        filterDocTypesType.push(every.docType);
      });
      var imageList = [];
      fileList.map(function (item) {
        if (filterDocTypesType.includes(item.type)) {
          imageList.push(item);
        }
      });
      var policyId = this.getValueFromModel(this.generatePropName("policyId", this.props.dataIdPrefix));
      var getDownUrl = this.props.getDownUrl;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("ul", {
        style: {
          paddingLeft: 0
        },
        className: "attachment-preview-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 491
        },
        __self: this
      }, imageList.map(function (item, index) {
        var type = item.type,
            fileName = item.fileName,
            state = item.state,
            uid = item.uid;
        var url = uploadFileMgr.getFileRenderUrl(item, policyId, getDownUrl);
        var downloadUrl = uploadFileMgr.getFileDownloadUrl(item, policyId, getDownUrl);
        var findImgIndex;

        _this5.getImgList(imageList).forEach(function (imageItem, imageIndex) {
          if (imageItem.uid === uid) {
            findImgIndex = imageIndex;
          }
        });

        return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Spin"], {
          size: "large",
          key: uid,
          spinning: state === UploadFileState.PROGRESS,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 503
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 504
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Tooltip"], {
          overlayClassName: "file-name-tip",
          placement: "bottomRight",
          title: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 505
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("li", {
          className: "attachment-preview-item",
          "data-type": type,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 506
          },
          __self: this
        }, url && _this5.isPicture(item) && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("img", {
          alt: uid,
          src: url,
          onClick: function onClick() {
            _this5.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 508
          },
          __self: this
        }), url && !_this5.isPicture(item) && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
          className: "img-a",
          href: downloadUrl,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 517
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("img", {
          alt: uid,
          src: url,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 518
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
          className: "file-name",
          href: downloadUrl,
          title: fileName,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 523
          },
          __self: this
        }, fileName), !isView && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "attachment-preview-item__oper",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 526
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("img", {
          src: __webpack_require__(/*! @assets/close.svg */ "./src/assets/close.svg"),
          onClick: function onClick() {
            return _this5.handleFileRemove(uid);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 527
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "operate",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 530
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "bg",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 531
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "con",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 532
          },
          __self: this
        }, _this5.isPicture(item) && _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
          onClick: function onClick() {
            return _this5.setLightbox({
              index: findImgIndex,
              visible: true
            });
          },
          href: "javascript:void(0)",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 533
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Icon"], {
          type: "eye",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 536
          },
          __self: this
        })), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("a", {
          className: _this5.isPicture(item) ? "a-pl" : "",
          style: {
            color: COLOR_PRIMARY
          },
          href: downloadUrl,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 538
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Icon"], {
          type: "download",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 540
          },
          __self: this
        }))))))), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          style: {
            marginTop: -8,
            marginRight: 15,
            textAlign: "center",
            hyphens: "auto",
            width: 135
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 546
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 553
          },
          __self: this
        }, Object(_desk_component_attachment_doc_types__WEBPACK_IMPORTED_MODULE_17__["transTypeToName"])(lodash__WEBPACK_IMPORTED_MODULE_12___default.a.get(item, "type"))))));
      }));
    }
  }, {
    key: "getAttaches",
    value: function getAttaches() {
      var _this6 = this;

      var policyId = this.getValueFromModel(this.generatePropName("policyId", this.props.dataIdPrefix));
      var getUrl = this.props.getUrl;
      this.getHelpers().getAjax().get(getUrl || "/policies/".concat(policyId, "/attaches"), {}, {}).then(function (response) {
        var respData = response.body.respData;

        _this6.setState({
          fileList: respData.map(function (item) {
            var attachDetails = item.attachDetails;
            return {
              id: item.attachId,
              uid: item.actualFid,
              state: UploadFileState.COMPLETED,
              fileName: attachDetails.originalFileName,
              type: attachDetails.docType,
              isImage: attachDetails.mimeType.indexOf("image/") === 0,
              isPdf: attachDetails.mimeType.includes("pdf")
            };
          })
        });

        _this6.storeFiles();
      }).catch(function (error) {
        antd__WEBPACK_IMPORTED_MODULE_14__["notification"].success({
          message: "error",
          description: error.message
        });
      });
    }
  }, {
    key: "storeFiles",
    value: function storeFiles() {// const {dataFixed} = this.props;
      // const suppDocs = {
      //     ...this.getValueFromModel(this.generatePropName(dataFixed
      //         || this.generateExtPropName("suppDocs"), this.props.dataIdPrefix)), ...this.getFormData()
      // };
      // this.setValueToModel(suppDocs, this.generatePropName(dataFixed
      //     || this.generateExtPropName("suppDocs"), this.props.dataIdPrefix));
    }
  }, {
    key: "getFormData",
    value: function getFormData() {
      var _this7 = this;

      var DocTypes = {};
      var _this$state2 = this.state,
          fileList = _this$state2.fileList,
          uploadList = _this$state2.uploadList;
      var result = {};
      var filterDocTypes = !this.props.docTypes ? uploadList : this.props.docTypes;
      var filterDocTypesType = [];
      filterDocTypes.map(function (every) {
        filterDocTypesType.push(every.docType);
      });
      filterDocTypes.map(function (every) {
        if (every.docType) {
          var docTypeArr = _this7.docTypeChange(every.docType);

          var field = docTypeArr.join("");
          DocTypes[every.docType] = {
            field: field
          };
          result[field] = [];
        }
      });
      fileList.forEach(function (item) {
        var id = item.id,
            type = item.type;

        if (!id) {
          return;
        }

        if (filterDocTypesType.includes(type)) {
          result[DocTypes[type].field].push(id);
        }
      });
      return result;
    }
  }, {
    key: "isPicture",
    value: function isPicture(item) {
      var fileName = item.fileName,
          isImage = item.isImage;
      if (!fileName) return;
      var type = fileName.split(".")[fileName.split(".").length - 1];
      return /gif|jpg|jpeg|png|gif|jpg|png|svg$/.test(type) || isImage;
    }
  }, {
    key: "renderImageLightbox",
    value: function renderImageLightbox() {
      var _this8 = this;

      var policyId = this.getValueFromModel(this.generatePropName("policyId", this.props.dataIdPrefix));
      var _this$state3 = this.state,
          fileList = _this$state3.fileList,
          uploadList = _this$state3.uploadList,
          _this$state3$lightbox = _this$state3.lightbox,
          visible = _this$state3$lightbox.visible,
          uid = _this$state3$lightbox.uid,
          index = _this$state3$lightbox.index;
      var filterDocTypes = !this.props.docTypes ? uploadList : this.props.docTypes;
      var filterDocTypesType = [];
      filterDocTypes.map(function (every) {
        filterDocTypesType.push(every.docType);
      });
      var imageList = [];
      var getDownUrl = this.props.getDownUrl;
      fileList.map(function (item) {
        var fileName = item.fileName;

        if (_this8.isPicture(item)) {
          imageList.push({
            caption: fileName,
            src: uploadFileMgr.getFileRenderUrlMd(item, policyId, getDownUrl),
            uid: item.uid
          });
        }
      });
      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(react_images__WEBPACK_IMPORTED_MODULE_13___default.a, {
        images: imageList,
        currentImage: index,
        backdropClosesModal: true,
        isOpen: visible && imageList.length > 0,
        onClickPrev: this.gotoPrevLightboxImage,
        onClickNext: this.gotoNextLightboxImage,
        onClose: function onClose() {
          return _this8.setLightbox({
            visible: false
          });
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 746
        },
        __self: this
      });
    }
  }, {
    key: "renderUploalders",
    value: function renderUploalders() {
      var _this9 = this;

      var policyId = this.getValueFromModel(this.generatePropName("policyId", this.props.dataIdPrefix));
      var authKey = _common__WEBPACK_IMPORTED_MODULE_18__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_18__["Consts"].AUTH_KEY);
      var postUrl = this.props.postUrl;

      var buildProps = function buildProps(docType) {
        return {
          action: function action(file) {
            return _common__WEBPACK_IMPORTED_MODULE_18__["Ajax"].getServiceLocation(postUrl ? "".concat(postUrl, "?docType=").concat(docType, "&docName=").concat(file.name) : "/policies/".concat(policyId, "/attaches?docType=").concat(docType, "&docName=").concat(file.name));
          },
          headers: {
            Authorization: authKey
          },
          beforeUpload: function beforeUpload(file, fileList) {
            return _this9.handleFileBeforeUpload(file, fileList, docType);
          },
          showUploadList: false,
          onChange: function onChange(info) {
            var _ref3 = info || {
              file: {}
            },
                file = _ref3.file;

            _this9.handleFileChange(info);

            var count = 0;

            _this9.state.fileList.forEach(function (every) {
              if (every.state === 1) {
                count++;
              }
            });

            _this9.setState({
              disableBtn: !(_this9.state.fileList.length > 0 && count === _this9.state.fileList.length - 1)
            }, function () {
              _this9.props.onChange && _this9.props.onChange(_this9.state.disableBtn);
            });
          },
          // onRemove: this.handleFileRemove,
          multiple: true
        };
      };

      return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
        className: "attachment-uploader-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 795
        },
        __self: this
      }, (this.props.docTypes || this.state.uploadList || []).map(function (type) {
        var docType = type.docType,
            docTypeDesc = type.docTypeDesc;
        var props = buildProps(docType);
        return _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Upload"].Dragger, Object.assign({}, props, {
          key: docType,
          listType: "picture-card",
          className: "attachment-uploader-item-wrap",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 800
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 806
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_14__["Icon"], {
          type: "plus",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 807
          },
          __self: this
        }), _common_3rd__WEBPACK_IMPORTED_MODULE_11__["React"].createElement("div", {
          className: "ant-upload-text",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 808
          },
          __self: this
        }, docTypeDesc)));
      }));
    }
  }]);

  return Attachment;
}(_component__WEBPACK_IMPORTED_MODULE_15__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Attachment);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2F0dGFjaG1lbnQvYXR0YWNobWVudC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvYXR0YWNobWVudC9hdHRhY2htZW50LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgTGlnaHRib3ggZnJvbSBcInJlYWN0LWltYWdlc1wiO1xuaW1wb3J0IHsgUmNGaWxlIH0gZnJvbSBcImFudGQvbGliL3VwbG9hZFwiO1xuaW1wb3J0IHsgSWNvbiwgbm90aWZpY2F0aW9uLCBTcGluLCBUb29sdGlwLCBVcGxvYWQgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldCB9IGZyb20gXCJAY29tcG9uZW50XCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UsIE1vZGVsV2lkZ2V0UHJvcHMsIFN0eWxlZERJViB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCB7IHRyYW5zVHlwZVRvTmFtZSB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYXR0YWNobWVudC9kb2MtdHlwZXNcIjtcbmltcG9ydCB7IEFqYXgsIENvbnN0cywgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCBhdHRhY2htZW50SW1hZ2VVcmwgZnJvbSBcIkBhc3NldHMvYXR0YWNobWVudC5zdmdcIjtcbmltcG9ydCBhdHRhY2htZW50UGRmIGZyb20gXCJAZGVzay1jb21wb25lbnQvaWNvbnMvcGRmLnBuZ1wiO1xuXG5cbmNvbnN0IHsgQ09MT1JfUFJJTUFSWSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcblxuY29uc3QgVXBsb2FkRmlsZVN0YXRlID0ge1xuICAgIFBST0dSRVNTOiAwLFxuICAgIENPTVBMRVRFRDogMSxcbn07XG5jb25zdCB1cGxvYWRGaWxlTWdyID0ge1xuICAgIHN0YXJ0VXBsb2FkKGxpc3Q6IGFueSwgZG9jVHlwZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGxpc3QubWFwKChmaWxlOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHtuYW1lLCB1aWQsIHR5cGV9ID0gZmlsZTtcbiAgICAgICAgICAgIGNvbnN0IGlzSW1hZ2UgPSB0eXBlLmluZGV4T2YoXCJpbWFnZS9cIikgPT09IDA7XG5cbiAgICAgICAgICAgIGlmIChpc0ltYWdlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRCYXNlNjQoZmlsZSwgKHVybDogc3RyaW5nKSA9PiBjYWxsYmFjayhmaWxlLCB1cmwpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICB1aWQsXG4gICAgICAgICAgICAgICAgc3RhdGU6IFVwbG9hZEZpbGVTdGF0ZS5QUk9HUkVTUyxcbiAgICAgICAgICAgICAgICBmaWxlTmFtZTogbmFtZSxcbiAgICAgICAgICAgICAgICB0eXBlOiBkb2NUeXBlLFxuICAgICAgICAgICAgICAgIGlzSW1hZ2UsXG4gICAgICAgICAgICAgICAgaXNQZGY6IHR5cGUuaW5jbHVkZXMoXCJwZGZcIiksXG4gICAgICAgICAgICB9O1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH0sXG5cbiAgICBnZXRCYXNlNjQoZmlsZTogYW55LCBjYWxsYmFjazogYW55KSB7XG4gICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG4gICAgICAgIHJlYWRlci5hZGRFdmVudExpc3RlbmVyKFwibG9hZFwiLCAoKSA9PiBjYWxsYmFjayhyZWFkZXIucmVzdWx0KSk7XG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgIH0sXG5cbiAgICBnZXRGaWxlUmVuZGVyVXJsKGl0ZW06IGFueSwgcG9saWN5SWQ6IHN0cmluZywgZG93blVybD86IHN0cmluZykge1xuICAgICAgICBjb25zdCB7aWQsIHVybCwgaXNJbWFnZSwgaXNQZGYsIGF0dGFjaElkfSA9IGl0ZW07XG4gICAgICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgICAgICBpZiAoaXNQZGYpIHtcbiAgICAgICAgICAgIHJldHVybiBhdHRhY2htZW50UGRmO1xuICAgICAgICB9XG4gICAgICAgIGlmICghaXNJbWFnZSkge1xuICAgICAgICAgICAgcmV0dXJuIGF0dGFjaG1lbnRJbWFnZVVybDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaWQpIHtcbiAgICAgICAgICAgIC8vIHRvZG8g6aqM6K+BXG4gICAgICAgICAgICByZXR1cm4gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oXG4gICAgICAgICAgICAgICAgZG93blVybCA/XG4gICAgICAgICAgICAgICAgICAgIGAke2Rvd25Vcmx9LyR7aWR9L2ZpbGU/YWNjZXNzX3Rva2VuPSR7YXV0aEtleX0mcmVzaXplPXNtYCA6XG4gICAgICAgICAgICAgICAgICAgIGAvcG9saWNpZXMvJHtwb2xpY3lJZH0vYXR0YWNoZXMvJHtpZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9c21gKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoYXR0YWNoSWQpIHtcbiAgICAgICAgICAgIHJldHVybiBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihcbiAgICAgICAgICAgICAgICBkb3duVXJsID9cbiAgICAgICAgICAgICAgICAgICAgYCR7ZG93blVybH0vJHthdHRhY2hJZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9c21gIDpcbiAgICAgICAgICAgICAgICAgICAgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9hdHRhY2hlcy8ke2F0dGFjaElkfS9maWxlP2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9JnJlc2l6ZT1zbWApO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHVybCkge1xuICAgICAgICAgICAgcmV0dXJuIHVybDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9LFxuXG4gICAgZ2V0RmlsZVJlbmRlclVybE1kKGl0ZW06IGFueSwgcG9saWN5SWQ6IHN0cmluZywgZG93blVybD86IHN0cmluZykge1xuICAgICAgICBjb25zdCB7aWQsIHVybCwgaXNJbWFnZSwgaXNQZGYsIGF0dGFjaElkfSA9IGl0ZW07XG4gICAgICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgICAgICBpZiAoaXNQZGYpIHtcbiAgICAgICAgICAgIHJldHVybiBhdHRhY2htZW50UGRmO1xuICAgICAgICB9XG4gICAgICAgIGlmICghaXNJbWFnZSkge1xuICAgICAgICAgICAgcmV0dXJuIGF0dGFjaG1lbnRJbWFnZVVybDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaWQpIHtcbiAgICAgICAgICAgIHJldHVybiBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihcbiAgICAgICAgICAgICAgICBkb3duVXJsID9cbiAgICAgICAgICAgICAgICAgICAgYCR7ZG93blVybH0vJHtpZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9bWRgIDpcbiAgICAgICAgICAgICAgICAgICAgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9hdHRhY2hlcy8ke2lkfS9maWxlP2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9JnJlc2l6ZT1tZGApO1xuICAgICAgICB9XG4gICAgICAgIGlmIChhdHRhY2hJZCkge1xuICAgICAgICAgICAgcmV0dXJuIEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKFxuICAgICAgICAgICAgICAgIGRvd25VcmwgP1xuICAgICAgICAgICAgICAgICAgICBgJHtkb3duVXJsfS8ke2F0dGFjaElkfS9maWxlP2FjY2Vzc190b2tlbj0ke2F1dGhLZXl9JnJlc2l6ZT1tZGAgOlxuICAgICAgICAgICAgICAgICAgICBgL3BvbGljaWVzLyR7cG9saWN5SWR9L2F0dGFjaGVzLyR7YXR0YWNoSWR9L2ZpbGU/YWNjZXNzX3Rva2VuPSR7YXV0aEtleX0mcmVzaXplPW1kYCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodXJsKSB7XG4gICAgICAgICAgICByZXR1cm4gdXJsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH0sXG5cbiAgICBnZXRGaWxlRG93bmxvYWRVcmwoaXRlbTogYW55LCBwb2xpY3lJZDogc3RyaW5nLCBkb3duVXJsPzogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHtpZH0gPSBpdGVtO1xuICAgICAgICBjb25zdCBhdXRoS2V5ID0gU3RvcmFnZS5BdXRoLnNlc3Npb24oKS5nZXQoQ29uc3RzLkFVVEhfS0VZKTtcbiAgICAgICAgaWYgKGlkKSB7XG4gICAgICAgICAgICByZXR1cm4gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oZG93blVybCA/XG4gICAgICAgICAgICAgICAgYCR7ZG93blVybH0vJHtpZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9b3JpZ2AgOlxuICAgICAgICAgICAgICAgIGAvcG9saWNpZXMvJHtwb2xpY3lJZH0vYXR0YWNoZXMvJHtpZH0vZmlsZT9hY2Nlc3NfdG9rZW49JHthdXRoS2V5fSZyZXNpemU9b3JpZ2ApO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfSxcblxuICAgIGNvbXBsZXRlVXBsb2FkKGZpbGU6IGFueSwgZmlkOiBzdHJpbmcpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIC4uLmZpbGUsXG4gICAgICAgICAgICBzdGF0ZTogVXBsb2FkRmlsZVN0YXRlLkNPTVBMRVRFRCxcbiAgICAgICAgICAgIGlkOiBmaWQsXG4gICAgICAgIH07XG4gICAgfSxcbn07XG5cbmV4cG9ydCB0eXBlIEF0dGFjaG1lbnRQcm9wcyA9IHtcbiAgICBvbkNoYW5nZT86ICh2YWx1ZTogYW55KSA9PiB2b2lkO1xuICAgIG1vZGVsOiBhbnk7XG4gICAgZm9ybT86IGFueTtcbiAgICBkb2NUeXBlcz86IGFueTtcbiAgICBnZXRVcGxvYWRMaXN0PzogRnVuY3Rpb247XG4gICAgcG9zdFVybD86IHN0cmluZztcbiAgICBkZWxldGVVcmw/OiBzdHJpbmc7XG4gICAgZ2V0RG93blVybD86IHN0cmluZztcbiAgICBnZXRVcmw/OiBzdHJpbmc7XG4gICAgZGF0YUlkUHJlZml4Pzogc3RyaW5nO1xuICAgIGRhdGFGaXhlZD86IGFueVxuICAgIGVuZG9GaXhlZD86IGFueTtcbiAgICBpc1ZpZXc/OiBib29sZWFuO1xufSAmIE1vZGVsV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIEF0dGFjaG1lbnRTdGF0ZSA9IHtcbiAgICBmaWxlTGlzdDogYW55O1xuICAgIHVwbG9hZExpc3Q6IGFueTtcbiAgICBsaWdodGJveDogYW55O1xuICAgIGltYWdlTGlzdDogYW55O1xuICAgIGRpc2FibGVCdG46IGJvb2xlYW47XG59XG5leHBvcnQgdHlwZSBTdHlsZWRESVYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwge30sIG5ldmVyPjtcblxuZXhwb3J0IHR5cGUgQXR0YWNobWVudENvbXBvbmVudHMgPSB7XG4gICAgQXR0YWNobWVudDogU3R5bGVkRElWLFxufVxuXG5jbGFzcyBBdHRhY2htZW50PFAgZXh0ZW5kcyBBdHRhY2htZW50UHJvcHMsIFMgZXh0ZW5kcyBBdHRhY2htZW50U3RhdGUsIEMgZXh0ZW5kcyBBdHRhY2htZW50Q29tcG9uZW50cz5cbiAgICBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wczogQXR0YWNobWVudFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgICB9XG5cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLnByb3BzLmRvY1R5cGVzKSB7XG4gICAgICAgICAgICB0aGlzLmdldFVwbG9hZExpc3QoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmdldEF0dGFjaGVzKCk7XG4gICAgfVxuXG4gICAgYXN5bmMgZ2V0VXBsb2FkTGlzdCgpIHtcbiAgICAgICAgY29uc3Qge21vZGVsLCBnZXRVcGxvYWRMaXN0fSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBsZXQgcmVzO1xuICAgICAgICAgICAgaWYgKGdldFVwbG9hZExpc3QpIHJlcyA9IGF3YWl0IGdldFVwbG9hZExpc3QoKTtcbiAgICAgICAgICAgIGVsc2UgcmVzID0gYXdhaXQgdGhpcy5nZXRIZWxwZXJzKClcbiAgICAgICAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgICAgICAgLnBvc3QoYC9wb2xpY2llcy9lbGlnaWJsZXN1cHBvcnRpbmdkb2NzYCwgbW9kZWwsIHt9KTtcblxuICAgICAgICAgICAgY29uc3QgcmVzcERhdGEgPSBfLmdldChyZXMsIFwiYm9keS5yZXNwRGF0YVwiLCBbXSk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICB1cGxvYWRMaXN0OiByZXNwRGF0YSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgICAgICBjb25zdCB7aXNWaWV3ID0gZmFsc2V9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxDLkF0dGFjaG1lbnQ+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjbGVhcmZpeFwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImF0dGFjaG1lbnRGb3JtXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2Pnt0aGlzLnJlbmRlclByZXZpZXcoKX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXR0YWNobWVudC11cGxvYWRlci13cmFwXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyFpc1ZpZXcgJiYgdGhpcy5yZW5kZXJVcGxvYWxkZXJzKCl9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAge3RoaXMucmVuZGVySW1hZ2VMaWdodGJveCgpfVxuICAgICAgICAgICAgPC9DLkF0dGFjaG1lbnQ+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgQXR0YWNobWVudDogU3R5bGVkLmRpdmBcbiAgICAgICAgLmF0dGFjaG1lbnQtdXBsb2FkZXItd3JhcCB7XG4gICAgICAgICAgLmFudC11cGxvYWQuYW50LXVwbG9hZC1kcmFnwqB7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgICAgICAgICAgICB3aWR0aDogMTA0cHg7XG4gICAgICAgICAgICAgIGhlaWdodDogMTA0cHg7XG4gICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gICAgICAgICAgICAgIC5hbnQtdXBsb2FkLmFudC11cGxvYWQtYnRuIHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiA4cHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgICAgICAgICAgICAgLmFudGljb24uYW50aWNvbi1wbHVzIHtcbiAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNjUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmFudC11cGxvYWQtdGV4dCB7XG4gICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgd2lkdGg6IDg2cHg7XG4gICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICAgICAgfVxuICAgICAgICAub3BlcmF0ZSB7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIHotaW5kZXg6IDIyO1xuICAgICAgICAgIHdpZHRoOiAxMDAlOyBcbiAgICAgICAgICBoZWlnaHQ6IDIzcHg7IFxuICAgICAgICAgIGJvdHRvbTogMnB4O1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgLmJnIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICBoZWlnaHQ6IDIzcHg7IFxuICAgICAgICAgICAgem9vbTogMTsgXG4gICAgICAgICAgfVxuICAgICAgICAgIC5jb24ge1xuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBhIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMnB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjJweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgLmFudGljb24ge1xuICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAmLmEtcGwge1xuICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDQ4cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5hdHRhY2htZW50LXByZXZpZXctbGlzdCB7XG4gICAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAtbXMtZmxleC13cmFwOiB3cmFwO1xuICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgICB9XG4gICAgICAgIC5hdHRhY2htZW50LXByZXZpZXctaXRlbSB7XG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAtbXMtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICBtYXJnaW46IDAgMTVweCAxNXB4IDA7XG4gICAgICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgICAgICB3aWR0aDogMTM1cHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDEzNXB4O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgIGltZyB7XG4gICAgICAgICAgICAgICAgZmxleDogMSAxIDAlO1xuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDEwMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYSB7XG4gICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyM3B4O1xuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyM3B4O1xuICAgICAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICAgICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcbiAgICAgICAgICAgICAgICAmLmltZy1hIHtcbiAgICAgICAgICAgICAgICAgIGhlaWdodDogYXV0bztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgXG4gICAgICAgICAgICAuYXR0YWNobWVudC1wcmV2aWV3LWl0ZW1fX29wZXIge1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICB0b3A6IC05cHg7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC0xMHB4O1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgICAgICAgei1pbmRleDogMjI7XG4gICAgICAgICAgICAgICAgaW1nIHtcbiAgICAgICAgICAgICAgICAgIHdpZHRoOiAyM3B4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpe1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNWZiZmVjO1xuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiAyNTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuZmlsZS1uYW1lIHtcbiAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgICAgICAuYXR0YWNobWVudC1wcmV2aWV3LWl0ZW1fX29wZXIsIC5vcGVyYXRlIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC5maWxlLW5hbWUge1xuICAgICAgICAgICAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICB9XG4gICAgICAgIC5hdHRhY2htZW50LXVwbG9hZGVyLWxpc3Qge1xuICAgICAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgICAgIG92ZXJmbG93LXg6IGF1dG87XG4gICAgICAgICAgICBvdmVyZmxvdy15OiBoaWRkZW47XG4gICAgICAgICAgICAuYXR0YWNobWVudC11cGxvYWRlci1pdGVtIHtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIGEsaW1nIHtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNjVweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDgwcHg7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAuNjtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgYm90dG9tO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgcCB7XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzllOWU5ZTtcbiAgICAgICAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gXG4gICAgICBgLFxuICAgICAgICB9IGFzIEM7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGluaXRTdGF0ZSgpOiBTIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgICAgICAgIHVwbG9hZExpc3Q6IFtdLFxuICAgICAgICAgICAgZmlsZUxpc3Q6IFtdLFxuICAgICAgICAgICAgbGlnaHRib3g6IHtpbmRleDogLTEsIHZpc2libGU6IGZhbHNlfSxcbiAgICAgICAgICAgIGltYWdlTGlzdDogW10sXG4gICAgICAgICAgICBkaXNhYmxlQnRuOiBmYWxzZSxcbiAgICAgICAgfSkgYXMgUztcbiAgICB9XG5cbiAgICBwcml2YXRlIGdlbmVyYXRlRXh0UHJvcE5hbWUocHJvcE5hbWU/OiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBjb25zdCB7ZW5kb0ZpeGVkfSA9IHRoaXMucHJvcHNcbiAgICAgICAgaWYgKGVuZG9GaXhlZCAmJiBwcm9wTmFtZSkgcmV0dXJuIGBleHQuJHtlbmRvRml4ZWR9LiR7cHJvcE5hbWV9YDtcbiAgICAgICAgaWYgKGVuZG9GaXhlZCkgcmV0dXJuIGBleHQuJHtlbmRvRml4ZWR9YDtcbiAgICAgICAgcmV0dXJuIGBleHQuJHtwcm9wTmFtZX1gO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2VuZXJhdGVQcm9wTmFtZShwcm9wTmFtZTogc3RyaW5nLCBkYXRhSWRQcmVmaXg/OiBhbnkpOiBzdHJpbmcge1xuICAgICAgICBpZiAoIWRhdGFJZFByZWZpeCkgcmV0dXJuIGAke3Byb3BOYW1lfWA7XG5cbiAgICAgICAgcmV0dXJuIGAke2RhdGFJZFByZWZpeH0uJHtwcm9wTmFtZX1gO1xuICAgIH1cblxuICAgIHByaXZhdGUgZG9jVHlwZUNoYW5nZShkb2NUeXBlOiBhbnkpOiBhbnlbXSB7XG4gICAgICAgIGxldCBkb2NUeXBlQXJyID0gZG9jVHlwZS50b0xvd2VyQ2FzZSgpLnNwbGl0KFwiX1wiKTtcbiAgICAgICAgZG9jVHlwZUFyci5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgIGlmIChpbmRleCA+IDApIHtcbiAgICAgICAgICAgICAgICBkb2NUeXBlQXJyW2luZGV4XSA9IGV2ZXJ5LnJlcGxhY2UoL1xcYihcXHcpL2csIGZ1bmN0aW9uIChtOiBzdHJpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG0udG9VcHBlckNhc2UoKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBkb2NUeXBlQXJyO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0SW1nTGlzdChmaWxlTGlzdDogYW55KTogYW55W10ge1xuICAgICAgICBsZXQgaW1nTGlzdDogYW55ID0gW107XG4gICAgICAgIGlmICghZmlsZUxpc3QpIHJldHVybiBbXTtcbiAgICAgICAgZmlsZUxpc3QubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICAgICAgICBpZiAodGhpcy5pc1BpY3R1cmUoZXZlcnkpKSB7XG4gICAgICAgICAgICAgICAgaW1nTGlzdC5wdXNoKGV2ZXJ5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBpbWdMaXN0O1xuICAgIH1cblxuICAgIHByaXZhdGUgaGFuZGxlRmlsZVJlbW92ZSA9ICh1aWQ6IHN0cmluZykgPT4ge1xuICAgICAgICBjb25zdCB7ZmlsZUxpc3R9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgY29uc3QgbmV4dExpc3QgPSBbLi4uZmlsZUxpc3RdO1xuICAgICAgICBjb25zdCBwb2xpY3lJZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicG9saWN5SWRcIiwgdGhpcy5wcm9wcy5kYXRhSWRQcmVmaXgpKTtcbiAgICAgICAgY29uc3QgaW5kZXggPSBuZXh0TGlzdC5maW5kSW5kZXgoaXRlbSA9PiBpdGVtLnVpZCA9PT0gdWlkKTtcbiAgICAgICAgaWYgKGluZGV4ID09PSAtMSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZmlsZSA9IG5leHRMaXN0LnNwbGljZShpbmRleCwgMSlbMF07XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgZmlsZUxpc3Q6IG5leHRMaXN0LFxuICAgICAgICAgICAgaW1hZ2VMaXN0OiB0aGlzLmdldEltZ0xpc3QobmV4dExpc3QpLFxuICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnN0b3JlRmlsZXMoKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKGZpbGUuaWQpIHtcbiAgICAgICAgICAgIHRoaXMuZGVsZXRlUG9saWN5QXR0YWNoKHBvbGljeUlkLCBmaWxlLmlkLCBmaWxlLnR5cGUpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIHByaXZhdGUgZGVsZXRlUG9saWN5QXR0YWNoKHBvbGljeUlkOiBzdHJpbmcsIGF0dGFjaElkOiBzdHJpbmcsIHR5cGU6IHN0cmluZykge1xuICAgICAgICBsZXQgZGVsZXRlVXJsOiBhbnkgPSB0aGlzLnByb3BzLmRlbGV0ZVVybDtcbiAgICAgICAgY29uc3Qge2RhdGFGaXhlZH0gPSB0aGlzLnByb3BzO1xuICAgICAgICB0aGlzLmdldEhlbHBlcnMoKVxuICAgICAgICAgICAgLmdldEFqYXgoKVxuICAgICAgICAgICAgLmRlbGV0ZShkZWxldGVVcmwgP1xuICAgICAgICAgICAgICAgIGAke2RlbGV0ZVVybH0vJHthdHRhY2hJZH1gIDpcbiAgICAgICAgICAgICAgICBgL3BvbGljaWVzLyR7cG9saWN5SWR9L2F0dGFjaGVzLyR7YXR0YWNoSWR9YCwge30sIHt9KVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBsZXQgZG9jVHlwZUFyciA9IHRoaXMuZG9jVHlwZUNoYW5nZSh0eXBlKTtcbiAgICAgICAgICAgICAgICBsZXQgZmllbGQgPSBkb2NUeXBlQXJyLmpvaW4oXCJcIik7XG4gICAgICAgICAgICAgICAgbGV0IHN1cHBEb2NzID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoZGF0YUZpeGVkXG4gICAgICAgICAgICAgICAgICAgIHx8IHRoaXMuZ2VuZXJhdGVFeHRQcm9wTmFtZShcInN1cHBEb2NzXCIpLCB0aGlzLnByb3BzLmRhdGFJZFByZWZpeCkpO1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGl0ZW0gaW4gc3VwcERvY3MpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0gPT09IGZpZWxkICYmIHN1cHBEb2NzW2l0ZW1dLmluY2x1ZGVzKGF0dGFjaElkKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3VwcERvY3NbaXRlbV0uc3BsaWNlKHN1cHBEb2NzW2l0ZW1dLmluZGV4T2YoYXR0YWNoSWQpLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLnNldFZhbHVlVG9Nb2RlbChzdXBwRG9jcywgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGRhdGFGaXhlZFxuICAgICAgICAgICAgICAgICAgICB8fCB0aGlzLmdlbmVyYXRlRXh0UHJvcE5hbWUoXCJzdXBwRG9jc1wiKSwgdGhpcy5wcm9wcy5kYXRhSWRQcmVmaXgpKTtcbiAgICAgICAgICAgICAgICBub3RpZmljYXRpb24uc3VjY2Vzcyh7XG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwic3VjY2Vzc1wiLFxuICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJZb3VyIGZpbGUgaXMgZGVsZXRlZCBzdWNjZXNzZnVsbHlcIixcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pLmNhdGNoKChlcnJvcjogYW55KSA9PiB7XG4gICAgICAgICAgICBub3RpZmljYXRpb24uZXJyb3Ioe1xuICAgICAgICAgICAgICAgIG1lc3NhZ2U6IFwiZXJyb3JcIixcbiAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogZXJyb3IubWVzc2FnZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNldExpZ2h0Ym94ID0gKG5leHRMaWdodGJveDogYW55KSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoKHtsaWdodGJveH0pID0+IHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgbGlnaHRib3g6IHtcbiAgICAgICAgICAgICAgICAgICAgLi4ubGlnaHRib3gsXG4gICAgICAgICAgICAgICAgICAgIC4uLm5leHRMaWdodGJveCxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHByaXZhdGUgcmVuZGVyUHJldmlldygpIHtcbiAgICAgICAgY29uc3Qge2ZpbGVMaXN0LCB1cGxvYWRMaXN0fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IHtpc1ZpZXcgPSBmYWxzZX0gPSB0aGlzLnByb3BzO1xuICAgICAgICBsZXQgZmlsdGVyRG9jVHlwZXM6IGFueVtdID0gIXRoaXMucHJvcHMuZG9jVHlwZXMgPyB1cGxvYWRMaXN0IDogdGhpcy5wcm9wcy5kb2NUeXBlcztcbiAgICAgICAgbGV0IGZpbHRlckRvY1R5cGVzVHlwZTogYW55ID0gW107XG4gICAgICAgIGZpbHRlckRvY1R5cGVzLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgZmlsdGVyRG9jVHlwZXNUeXBlLnB1c2goZXZlcnkuZG9jVHlwZSk7XG4gICAgICAgIH0pO1xuICAgICAgICBsZXQgaW1hZ2VMaXN0OiBhbnkgPSBbXTtcbiAgICAgICAgZmlsZUxpc3QubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGlmIChmaWx0ZXJEb2NUeXBlc1R5cGUuaW5jbHVkZXMoaXRlbS50eXBlKSkge1xuICAgICAgICAgICAgICAgIGltYWdlTGlzdC5wdXNoKGl0ZW0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgY29uc3QgcG9saWN5SWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBvbGljeUlkXCIsIHRoaXMucHJvcHMuZGF0YUlkUHJlZml4KSk7XG4gICAgICAgIGxldCBnZXREb3duVXJsOiBhbnkgPSB0aGlzLnByb3BzLmdldERvd25Vcmw7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8dWwgc3R5bGU9e3twYWRkaW5nTGVmdDogMH19IGNsYXNzTmFtZT1cImF0dGFjaG1lbnQtcHJldmlldy1saXN0XCI+XG4gICAgICAgICAgICAgICAge2ltYWdlTGlzdC5tYXAoKGl0ZW06IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB7dHlwZSwgZmlsZU5hbWUsIHN0YXRlLCB1aWR9ID0gaXRlbTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdXJsID0gdXBsb2FkRmlsZU1nci5nZXRGaWxlUmVuZGVyVXJsKGl0ZW0sIHBvbGljeUlkLCBnZXREb3duVXJsKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZG93bmxvYWRVcmwgPSB1cGxvYWRGaWxlTWdyLmdldEZpbGVEb3dubG9hZFVybChpdGVtLCBwb2xpY3lJZCwgZ2V0RG93blVybCk7XG4gICAgICAgICAgICAgICAgICAgIGxldCBmaW5kSW1nSW5kZXg6IG51bWJlcjtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRJbWdMaXN0KGltYWdlTGlzdCkuZm9yRWFjaCgoaW1hZ2VJdGVtOiBhbnksIGltYWdlSW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGltYWdlSXRlbS51aWQgPT09IHVpZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbmRJbWdJbmRleCA9IGltYWdlSW5kZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgPFNwaW4gc2l6ZT1cImxhcmdlXCIga2V5PXt1aWR9IHNwaW5uaW5nPXtzdGF0ZSA9PT0gVXBsb2FkRmlsZVN0YXRlLlBST0dSRVNTfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VG9vbHRpcCBvdmVybGF5Q2xhc3NOYW1lPVwiZmlsZS1uYW1lLXRpcFwiIHBsYWNlbWVudD1cImJvdHRvbVJpZ2h0XCIgdGl0bGU9e2ZpbGVOYW1lfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzc05hbWU9XCJhdHRhY2htZW50LXByZXZpZXctaXRlbVwiIGRhdGEtdHlwZT17dHlwZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3VybCAmJiB0aGlzLmlzUGljdHVyZShpdGVtKSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsdD17dWlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPXt1cmx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRMaWdodGJveCh7aW5kZXg6IGZpbmRJbWdJbmRleCwgdmlzaWJsZTogdHJ1ZX0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt1cmwgJiYgIXRoaXMuaXNQaWN0dXJlKGl0ZW0pICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaW1nLWFcIiBocmVmPXtkb3dubG9hZFVybH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWx0PXt1aWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPXt1cmx9Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiZmlsZS1uYW1lXCIgaHJlZj17ZG93bmxvYWRVcmx9IHRpdGxlPXtmaWxlTmFtZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtmaWxlTmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyFpc1ZpZXcgJiYgPGRpdiBjbGFzc05hbWU9XCJhdHRhY2htZW50LXByZXZpZXctaXRlbV9fb3BlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz17cmVxdWlyZShcIkBhc3NldHMvY2xvc2Uuc3ZnXCIpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlRmlsZVJlbW92ZSh1aWQpfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwib3BlcmF0ZVwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImJnXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0aGlzLmlzUGljdHVyZShpdGVtKSAmJiAoPGEgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRMaWdodGJveCh7aW5kZXg6IGZpbmRJbWdJbmRleCwgdmlzaWJsZTogdHJ1ZX0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9IGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cImV5ZVwiLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2E+KX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9e3RoaXMuaXNQaWN0dXJlKGl0ZW0pID8gXCJhLXBsXCIgOiBcIlwifVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7Y29sb3I6IENPTE9SX1BSSU1BUll9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhyZWY9e2Rvd25sb2FkVXJsfT48SWNvbiB0eXBlPVwiZG93bmxvYWRcIi8+PC9hPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW5Ub3A6IC04LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luUmlnaHQ6IDE1LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHlwaGVuczogXCJhdXRvXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTM1LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwPnt0cmFuc1R5cGVUb05hbWUoXy5nZXQoaXRlbSwgXCJ0eXBlXCIpKX08L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TcGluPlxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEF0dGFjaGVzKCkge1xuICAgICAgICBjb25zdCBwb2xpY3lJZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicG9saWN5SWRcIiwgdGhpcy5wcm9wcy5kYXRhSWRQcmVmaXgpKTtcbiAgICAgICAgbGV0IGdldFVybDogYW55ID0gdGhpcy5wcm9wcy5nZXRVcmw7XG4gICAgICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAgICAgICAuZ2V0QWpheCgpXG4gICAgICAgICAgICAuZ2V0KChnZXRVcmwgfHwgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9hdHRhY2hlc2ApLCB7fSwge30pXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHtyZXNwRGF0YX0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBmaWxlTGlzdDogcmVzcERhdGEubWFwKGZ1bmN0aW9uIChpdGVtOiBhbnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGF0dGFjaERldGFpbHMgPSBpdGVtLmF0dGFjaERldGFpbHM7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBpdGVtLmF0dGFjaElkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVpZDogaXRlbS5hY3R1YWxGaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGU6IFVwbG9hZEZpbGVTdGF0ZS5DT01QTEVURUQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZU5hbWU6IGF0dGFjaERldGFpbHMub3JpZ2luYWxGaWxlTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBhdHRhY2hEZXRhaWxzLmRvY1R5cGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNJbWFnZTogYXR0YWNoRGV0YWlscy5taW1lVHlwZS5pbmRleE9mKFwiaW1hZ2UvXCIpID09PSAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzUGRmOiBhdHRhY2hEZXRhaWxzLm1pbWVUeXBlLmluY2x1ZGVzKFwicGRmXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zdG9yZUZpbGVzKCk7XG4gICAgICAgICAgICB9KS5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICBub3RpZmljYXRpb24uc3VjY2Vzcyh7XG4gICAgICAgICAgICAgICAgbWVzc2FnZTogXCJlcnJvclwiLFxuICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBlcnJvci5tZXNzYWdlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgaGFuZGxlRmlsZUNoYW5nZSA9IChpbmZvOiBhbnkpID0+IHtcbiAgICAgICAgY29uc3Qge2ZpbGVMaXN0fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IHtmaWxlTGlzdDogdXBsb2FkZWRGaWxlTGlzdH0gPSBpbmZvO1xuICAgICAgICB1cGxvYWRlZEZpbGVMaXN0LmZvckVhY2goKGZpbGU6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKGZpbGUucmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBmaWQgPSAoZmlsZS5hdHRhY2hJZCA9IGZpbGUucmVzcG9uc2UucmVzcERhdGEpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGluZm8sIFwiaW5mb1wiKTtcbiAgICAgICAgICAgICAgICBjb25zdCB7dWlkfSA9IGZpbGU7XG4gICAgICAgICAgICAgICAgaWYgKGZpZCAmJiBmaWxlTGlzdC5maW5kSW5kZXgoKGl0ZW06IGFueSkgPT4gaXRlbS5pZCA9PT0gZmlkKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKFxuICAgICAgICAgICAgICAgICAgICAoe2ZpbGVMaXN0fSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbmV4dExpc3QgPSBbLi4uZmlsZUxpc3RdO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaW5kZXggPSBmaWxlTGlzdC5maW5kSW5kZXgoKGl0ZW06IGFueSkgPT4gaXRlbS51aWQgPT09IHVpZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dExpc3RbaW5kZXhdID0gdXBsb2FkRmlsZU1nci5jb21wbGV0ZVVwbG9hZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dExpc3RbaW5kZXhdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlTGlzdDogbmV4dExpc3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2VMaXN0OiB0aGlzLmdldEltZ0xpc3QobmV4dExpc3QpLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JlRmlsZXMoKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBzdG9yZUZpbGVzKCkge1xuICAgICAgICAvLyBjb25zdCB7ZGF0YUZpeGVkfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIC8vIGNvbnN0IHN1cHBEb2NzID0ge1xuICAgICAgICAvLyAgICAgLi4udGhpcy5nZXRWYWx1ZUZyb21Nb2RlbCh0aGlzLmdlbmVyYXRlUHJvcE5hbWUoZGF0YUZpeGVkXG4gICAgICAgIC8vICAgICAgICAgfHwgdGhpcy5nZW5lcmF0ZUV4dFByb3BOYW1lKFwic3VwcERvY3NcIiksIHRoaXMucHJvcHMuZGF0YUlkUHJlZml4KSksIC4uLnRoaXMuZ2V0Rm9ybURhdGEoKVxuICAgICAgICAvLyB9O1xuICAgICAgICAvLyB0aGlzLnNldFZhbHVlVG9Nb2RlbChzdXBwRG9jcywgdGhpcy5nZW5lcmF0ZVByb3BOYW1lKGRhdGFGaXhlZFxuICAgICAgICAvLyAgICAgfHwgdGhpcy5nZW5lcmF0ZUV4dFByb3BOYW1lKFwic3VwcERvY3NcIiksIHRoaXMucHJvcHMuZGF0YUlkUHJlZml4KSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRGb3JtRGF0YSgpIHtcbiAgICAgICAgbGV0IERvY1R5cGVzOiBhbnkgPSB7fTtcbiAgICAgICAgY29uc3Qge2ZpbGVMaXN0LCB1cGxvYWRMaXN0fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGxldCByZXN1bHQ6IGFueSA9IHt9O1xuICAgICAgICBsZXQgZmlsdGVyRG9jVHlwZXM6IGFueVtdID0gIXRoaXMucHJvcHMuZG9jVHlwZXMgPyB1cGxvYWRMaXN0IDogdGhpcy5wcm9wcy5kb2NUeXBlcztcbiAgICAgICAgbGV0IGZpbHRlckRvY1R5cGVzVHlwZTogYW55ID0gW107XG4gICAgICAgIGZpbHRlckRvY1R5cGVzLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgZmlsdGVyRG9jVHlwZXNUeXBlLnB1c2goZXZlcnkuZG9jVHlwZSk7XG4gICAgICAgIH0pO1xuICAgICAgICAoZmlsdGVyRG9jVHlwZXMpLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKGV2ZXJ5LmRvY1R5cGUpIHtcbiAgICAgICAgICAgICAgICBsZXQgZG9jVHlwZUFyciA9IHRoaXMuZG9jVHlwZUNoYW5nZShldmVyeS5kb2NUeXBlKTtcbiAgICAgICAgICAgICAgICBsZXQgZmllbGQgPSBkb2NUeXBlQXJyLmpvaW4oXCJcIik7XG4gICAgICAgICAgICAgICAgRG9jVHlwZXNbZXZlcnkuZG9jVHlwZV0gPSB7XG4gICAgICAgICAgICAgICAgICAgIGZpZWxkOiBmaWVsZCxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHJlc3VsdFtmaWVsZF0gPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGZpbGVMaXN0LmZvckVhY2goKGl0ZW06IGFueSkgPT4ge1xuICAgICAgICAgICAgY29uc3Qge2lkLCB0eXBlfSA9IGl0ZW07XG4gICAgICAgICAgICBpZiAoIWlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGZpbHRlckRvY1R5cGVzVHlwZS5pbmNsdWRlcyh0eXBlKSkge1xuICAgICAgICAgICAgICAgIHJlc3VsdFtEb2NUeXBlc1t0eXBlXS5maWVsZF0ucHVzaChpZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cblxuICAgIHByaXZhdGUgaGFuZGxlRmlsZUJlZm9yZVVwbG9hZCA9IChmaWxlOiBSY0ZpbGUsIGZpbGVzOiBSY0ZpbGVbXSwgZG9jVHlwZTogc3RyaW5nKSA9PiB7XG4gICAgICAgIGNvbnN0IHdpbGxVcGxvYWRlZEZpbGVzID0gdXBsb2FkRmlsZU1nci5zdGFydFVwbG9hZChcbiAgICAgICAgICAgIGZpbGVzLFxuICAgICAgICAgICAgZG9jVHlwZSxcbiAgICAgICAgICAgIChmaWxlOiBhbnksIHVybDogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3Qge2ZpbGVMaXN0fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICAgICAgY29uc3QgaW5kZXggPSBmaWxlTGlzdC5maW5kSW5kZXgoKGl0ZW06IGFueSkgPT4gaXRlbS51aWQgPT09IGZpbGUudWlkKTtcbiAgICAgICAgICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG5leHRMaXN0ID0gWy4uLmZpbGVMaXN0XTtcbiAgICAgICAgICAgICAgICAgICAgbmV4dExpc3RbaW5kZXhdID0gey4uLm5leHRMaXN0W2luZGV4XSwgdXJsfTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxlTGlzdDogbmV4dExpc3QsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBmaWxlTGlzdDogWy4uLnRoaXMuc3RhdGUuZmlsZUxpc3QsIC4uLndpbGxVcGxvYWRlZEZpbGVzXSxcbiAgICAgICAgICAgIGltYWdlTGlzdDogdGhpcy5nZXRJbWdMaXN0KFsuLi50aGlzLnN0YXRlLmZpbGVMaXN0LCAuLi53aWxsVXBsb2FkZWRGaWxlc10pLFxuICAgICAgICAgICAgZGlzYWJsZUJ0bjogdHJ1ZSxcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG5cbiAgICBwcml2YXRlIGdvdG9QcmV2TGlnaHRib3hJbWFnZSA9ICgpID0+IHtcbiAgICAgICAgY29uc3Qge2xpZ2h0Ym94OiB7aW5kZXh9fSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zZXRMaWdodGJveCh7XG4gICAgICAgICAgICBpbmRleDogaW5kZXggLSAxLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBnb3RvTmV4dExpZ2h0Ym94SW1hZ2UgPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IHtsaWdodGJveDoge2luZGV4fSwgZmlsZUxpc3R9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgaWYgKGluZGV4ID09PSBmaWxlTGlzdC5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNldExpZ2h0Ym94KHtcbiAgICAgICAgICAgIGluZGV4OiBpbmRleCArIDEsXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBwcml2YXRlIGlzUGljdHVyZShpdGVtOiBhbnkpIHtcbiAgICAgICAgY29uc3Qge2ZpbGVOYW1lLCBpc0ltYWdlfSA9IGl0ZW07XG4gICAgICAgIGlmICghZmlsZU5hbWUpIHJldHVybjtcbiAgICAgICAgY29uc3QgdHlwZTogc3RyaW5nID0gZmlsZU5hbWUuc3BsaXQoXCIuXCIpW2ZpbGVOYW1lLnNwbGl0KFwiLlwiKS5sZW5ndGggLSAxXTtcbiAgICAgICAgcmV0dXJuICgvZ2lmfGpwZ3xqcGVnfHBuZ3xnaWZ8anBnfHBuZ3xzdmckLykudGVzdCh0eXBlKSB8fCBpc0ltYWdlO1xuICAgIH1cblxuICAgIHByaXZhdGUgcmVuZGVySW1hZ2VMaWdodGJveCgpIHtcbiAgICAgICAgY29uc3QgcG9saWN5SWQgPSB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKHRoaXMuZ2VuZXJhdGVQcm9wTmFtZShcInBvbGljeUlkXCIsIHRoaXMucHJvcHMuZGF0YUlkUHJlZml4KSk7XG4gICAgICAgIGNvbnN0IHtmaWxlTGlzdCwgdXBsb2FkTGlzdCwgbGlnaHRib3g6IHt2aXNpYmxlLCB1aWQsIGluZGV4fX0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBsZXQgZmlsdGVyRG9jVHlwZXM6IGFueVtdID0gIXRoaXMucHJvcHMuZG9jVHlwZXMgPyB1cGxvYWRMaXN0IDogdGhpcy5wcm9wcy5kb2NUeXBlcztcbiAgICAgICAgbGV0IGZpbHRlckRvY1R5cGVzVHlwZTogYW55ID0gW107XG4gICAgICAgIGZpbHRlckRvY1R5cGVzLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgZmlsdGVyRG9jVHlwZXNUeXBlLnB1c2goZXZlcnkuZG9jVHlwZSk7XG4gICAgICAgIH0pO1xuICAgICAgICBsZXQgaW1hZ2VMaXN0OiBhbnkgPSBbXTtcbiAgICAgICAgbGV0IGdldERvd25Vcmw6IGFueSA9IHRoaXMucHJvcHMuZ2V0RG93blVybDtcbiAgICAgICAgZmlsZUxpc3QubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHtmaWxlTmFtZX0gPSBpdGVtO1xuICAgICAgICAgICAgaWYgKHRoaXMuaXNQaWN0dXJlKGl0ZW0pKSB7XG4gICAgICAgICAgICAgICAgaW1hZ2VMaXN0LnB1c2goe1xuICAgICAgICAgICAgICAgICAgICBjYXB0aW9uOiBmaWxlTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgc3JjOiB1cGxvYWRGaWxlTWdyLmdldEZpbGVSZW5kZXJVcmxNZChpdGVtLCBwb2xpY3lJZCwgZ2V0RG93blVybCksXG4gICAgICAgICAgICAgICAgICAgIHVpZDogaXRlbS51aWQsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPExpZ2h0Ym94XG4gICAgICAgICAgICAgICAgaW1hZ2VzPXtpbWFnZUxpc3R9XG4gICAgICAgICAgICAgICAgY3VycmVudEltYWdlPXtpbmRleH1cbiAgICAgICAgICAgICAgICBiYWNrZHJvcENsb3Nlc01vZGFsXG4gICAgICAgICAgICAgICAgaXNPcGVuPXt2aXNpYmxlICYmIGltYWdlTGlzdC5sZW5ndGggPiAwfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2tQcmV2PXt0aGlzLmdvdG9QcmV2TGlnaHRib3hJbWFnZX1cbiAgICAgICAgICAgICAgICBvbkNsaWNrTmV4dD17dGhpcy5nb3RvTmV4dExpZ2h0Ym94SW1hZ2V9XG4gICAgICAgICAgICAgICAgb25DbG9zZT17KCkgPT4gdGhpcy5zZXRMaWdodGJveCh7dmlzaWJsZTogZmFsc2V9KX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZW5kZXJVcGxvYWxkZXJzKCkge1xuICAgICAgICBjb25zdCBwb2xpY3lJZCA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwodGhpcy5nZW5lcmF0ZVByb3BOYW1lKFwicG9saWN5SWRcIiwgdGhpcy5wcm9wcy5kYXRhSWRQcmVmaXgpKTtcbiAgICAgICAgY29uc3QgYXV0aEtleSA9IFN0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSk7XG4gICAgICAgIGxldCBwb3N0VXJsOiBhbnkgPSB0aGlzLnByb3BzLnBvc3RVcmw7XG4gICAgICAgIGNvbnN0IGJ1aWxkUHJvcHMgPSAoZG9jVHlwZTogc3RyaW5nKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbjogKGZpbGU6IFJjRmlsZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gQWpheC5nZXRTZXJ2aWNlTG9jYXRpb24oXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3N0VXJsID8gYCR7cG9zdFVybH0/ZG9jVHlwZT0ke2RvY1R5cGV9JmRvY05hbWU9JHtmaWxlLm5hbWV9YCA6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYC9wb2xpY2llcy8ke3BvbGljeUlkfS9hdHRhY2hlcz9kb2NUeXBlPSR7ZG9jVHlwZX0mZG9jTmFtZT0ke2ZpbGUubmFtZX1gKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogYXV0aEtleSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGJlZm9yZVVwbG9hZDogKGZpbGU6IFJjRmlsZSwgZmlsZUxpc3Q6IFJjRmlsZVtdKSA9PlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhbmRsZUZpbGVCZWZvcmVVcGxvYWQoZmlsZSwgZmlsZUxpc3QsIGRvY1R5cGUpLFxuICAgICAgICAgICAgICAgIHNob3dVcGxvYWRMaXN0OiBmYWxzZSxcbiAgICAgICAgICAgICAgICBvbkNoYW5nZTogKGluZm86IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB7ZmlsZX0gPSBpbmZvIHx8IHtmaWxlOiB7fX07XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGFuZGxlRmlsZUNoYW5nZShpbmZvKTtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNvdW50ID0gMDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5maWxlTGlzdC5mb3JFYWNoKChldmVyeTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZXZlcnkuc3RhdGUgPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudCsrO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlQnRuOiAhKHRoaXMuc3RhdGUuZmlsZUxpc3QubGVuZ3RoID4gMCAmJiBjb3VudCA9PT0gdGhpcy5zdGF0ZS5maWxlTGlzdC5sZW5ndGggLSAxKSxcbiAgICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZSAmJiB0aGlzLnByb3BzLm9uQ2hhbmdlKHRoaXMuc3RhdGUuZGlzYWJsZUJ0bik7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgLy8gb25SZW1vdmU6IHRoaXMuaGFuZGxlRmlsZVJlbW92ZSxcbiAgICAgICAgICAgICAgICBtdWx0aXBsZTogdHJ1ZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImF0dGFjaG1lbnQtdXBsb2FkZXItbGlzdFwiPlxuICAgICAgICAgICAgICAgIHsodGhpcy5wcm9wcy5kb2NUeXBlcyB8fCB0aGlzLnN0YXRlLnVwbG9hZExpc3QgfHwgW10pLm1hcCgodHlwZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHtkb2NUeXBlLCBkb2NUeXBlRGVzY30gPSB0eXBlO1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBwcm9wczogYW55ID0gYnVpbGRQcm9wcyhkb2NUeXBlKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxVcGxvYWQuRHJhZ2dlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5wcm9wc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2RvY1R5cGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGlzdFR5cGU9XCJwaWN0dXJlLWNhcmRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImF0dGFjaG1lbnQtdXBsb2FkZXItaXRlbS13cmFwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SWNvbiB0eXBlPXtcInBsdXNcIn0vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImFudC11cGxvYWQtdGV4dFwiPntkb2NUeXBlRGVzY308L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVXBsb2FkLkRyYWdnZXI+XG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0dGFjaG1lbnQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUhBO0FBS0E7QUF4R0E7QUFDQTtBQXVJQTs7Ozs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBd1BBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBN1FBO0FBK1NBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUF4VEE7QUFpYkE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQXBkQTtBQStmQTtBQUdBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQXRoQkE7QUF1aEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFqaUJBO0FBa2lCQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQTVpQkE7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7Ozs7Ozs7Ozs7OztBQUdBOzs7QUFHQTs7Ozs7O0FBQUE7QUFDQTs7QUFEQTs7Ozs7O0FBQ0E7QUFDQTs7QUFEQTtBQUNBOztBQUdBO0FBQ0E7QUFDQTtBQURBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBREE7QUFBQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBd0pBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBd0JBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFHQTtBQU1BO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7QUFhQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFFQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFaQTtBQUNBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7O0FBc0NBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQWlEQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBR0E7Ozs7QUEvb0JBO0FBQ0E7QUFpcEJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/attachment/attachment.tsx
