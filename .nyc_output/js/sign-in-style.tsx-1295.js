__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");


function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      .or-line-div {\n        text-align: center;\n        position: relative;\n        color: #ffffff;\n        margin: 25px 0 10px;\n        &::before, &::after{\n          content: '';\n          display: block;\n          position: absolute;\n          height: 1px;\n          width: 40%;\n          top: 50%;\n          background-color: #ddd;\n        }\n        &::after {\n          right: 0;\n        }\n      }\n      .or-line-icon {\n        display: flex;\n        justify-content: space-between;\n        margin-bottom: 10px;\n        padding: 0 50px\n        > i {\n        font-size: 36px;\n        cursor: pointer;\n            &.fa-line {\n              color: rgb(26,181,30) !important;\n            }\n            &.fa-linkedin {\n              color: rgb(17,116,176) !important;\n            }\n            &.fa-twitter-square {\n              color: rgb(42,163,240) !important;\n            }\n            &.fa-facebook-square {\n              color: rgb(59,87,154) !important;\n            }\n        }\n      }\n    "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    .or-code-content {\n        display: flex;\n        width: 200px;\n        height: 200px;\n        margin: 0 auto;\n        padding: 20px;\n        position: relative;\n        z-index: 1;\n        background: #fff;\n        justify-content: center;\n          &[data-signed=true] {\n            > img, > i:nth-child(2), > i:nth-child(3) {\n                display: none;\n            }\n            > i:nth-child(4) {\n              opacity: 1;\n            }\n          }\n          > img {\n            height: 158px;\n            width: 158px;\n            background: #fff;\n            border-radius: 2px;\n          }\n          > i:nth-child(2), > i:nth-child(3) {\n            position: absolute;\n            top: 0;\n            right: -60px;\n            font-size: 20px;\n            opacity: 1;\n            cursor: pointer;\n            width: 30px;\n            height: 20px;\n            line-height: 20px;\n            text-align: center;\n          }\n          > i:nth-child(3) {\n            right: -35px;\n            font-size: 15px;\n          }\n          > i:nth-child(4) {\n            display: block;\n            font-size: 24px;\n            border: 4px solid ", ";\n            border-radius: 100%;\n            position: absolute;\n            left: 50%;\n            top: 30%;\n            transform: translateX(-50%) translateY(-50%);\n            width: 40px;\n            height: 40px;\n            line-height: 34px;\n            text-align: center;\n            \n            opacity: 0;\n            transition: opacity 300ms ease-in-out;\n            &:after {\n              content: 'Signed in successfully. Screen will be refreshed automatically.';\n              font-size: 12px;\n              position: absolute;\n              top: 140%;\n              width: 200px;\n              left: 50%;\n              transform: translateX(-50%);\n              text-align: center;\n              line-height: 14px;\n            }\n          }\n      }\n    "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        text-align: left;\n        color: #fff;\n        "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n       font-size: 1.53846rem;\n       font-weight: 300;\n       display: block;\n       margin-bottom: 1.5rem;\n      \n       font-feature-settings: \"tnum\";\n       font-size: 20px;\n       "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_2__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY,
    BORDER_COLOR = _Theme$getTheme.BORDER_COLOR,
    COLOR_BACKGROUND_BTN = _Theme$getTheme.COLOR_BACKGROUND_BTN;

/* harmony default export */ __webpack_exports__["default"] = ({
  H3: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].h3(_templateObject()),
  FormCheckbox: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject2()),
  CodeContent: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject3(), COLOR_PRIMARY),
  LineContent: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject4())
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svbG9naW4vc2lnbi1pbi1zdHlsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9sb2dpbi9zaWduLWluLXN0eWxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlc1wiO1xuXG5jb25zdCB7IENPTE9SX1BSSU1BUlksIEJPUkRFUl9DT0xPUiwgQ09MT1JfQkFDS0dST1VORF9CVE4gfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgSDM6IFN0eWxlZC5oM2BcbiAgICAgICBmb250LXNpemU6IDEuNTM4NDZyZW07XG4gICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XG4gICAgICBcbiAgICAgICBmb250LWZlYXR1cmUtc2V0dGluZ3M6IFwidG51bVwiO1xuICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICBgLFxuICBGb3JtQ2hlY2tib3g6IFN0eWxlZC5kaXZgXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBgLFxuXG4gIENvZGVDb250ZW50OiBTdHlsZWQuZGl2YFxuICAgIC5vci1jb2RlLWNvbnRlbnQge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICB3aWR0aDogMjAwcHg7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICBwYWRkaW5nOiAyMHB4O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgICZbZGF0YS1zaWduZWQ9dHJ1ZV0ge1xuICAgICAgICAgICAgPiBpbWcsID4gaTpudGgtY2hpbGQoMiksID4gaTpudGgtY2hpbGQoMykge1xuICAgICAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICA+IGk6bnRoLWNoaWxkKDQpIHtcbiAgICAgICAgICAgICAgb3BhY2l0eTogMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgPiBpbWcge1xuICAgICAgICAgICAgaGVpZ2h0OiAxNThweDtcbiAgICAgICAgICAgIHdpZHRoOiAxNThweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgICAgfVxuICAgICAgICAgID4gaTpudGgtY2hpbGQoMiksID4gaTpudGgtY2hpbGQoMykge1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgcmlnaHQ6IC02MHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgb3BhY2l0eTogMTtcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgIHdpZHRoOiAzMHB4O1xuICAgICAgICAgICAgaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgfVxuICAgICAgICAgID4gaTpudGgtY2hpbGQoMykge1xuICAgICAgICAgICAgcmlnaHQ6IC0zNXB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICA+IGk6bnRoLWNoaWxkKDQpIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgICAgICAgICAgYm9yZGVyOiA0cHggc29saWQgJHtDT0xPUl9QUklNQVJZfTtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICBsZWZ0OiA1MCU7XG4gICAgICAgICAgICB0b3A6IDMwJTtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSB0cmFuc2xhdGVZKC01MCUpO1xuICAgICAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMzRweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgb3BhY2l0eTogMDtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IG9wYWNpdHkgMzAwbXMgZWFzZS1pbi1vdXQ7XG4gICAgICAgICAgICAmOmFmdGVyIHtcbiAgICAgICAgICAgICAgY29udGVudDogJ1NpZ25lZCBpbiBzdWNjZXNzZnVsbHkuIFNjcmVlbiB3aWxsIGJlIHJlZnJlc2hlZCBhdXRvbWF0aWNhbGx5Lic7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICB0b3A6IDE0MCU7XG4gICAgICAgICAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgICAgICAgICAgbGVmdDogNTAlO1xuICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgIGAsXG5cbiAgTGluZUNvbnRlbnQ6IFN0eWxlZC5kaXZgXG4gICAgICAub3ItbGluZS1kaXYge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIG1hcmdpbjogMjVweCAwIDEwcHg7XG4gICAgICAgICY6OmJlZm9yZSwgJjo6YWZ0ZXJ7XG4gICAgICAgICAgY29udGVudDogJyc7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgIGhlaWdodDogMXB4O1xuICAgICAgICAgIHdpZHRoOiA0MCU7XG4gICAgICAgICAgdG9wOiA1MCU7XG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2RkZDtcbiAgICAgICAgfVxuICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgcmlnaHQ6IDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC5vci1saW5lLWljb24ge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgNTBweFxuICAgICAgICA+IGkge1xuICAgICAgICBmb250LXNpemU6IDM2cHg7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgICYuZmEtbGluZSB7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2IoMjYsMTgxLDMwKSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJi5mYS1saW5rZWRpbiB7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2IoMTcsMTE2LDE3NikgIWltcG9ydGFudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICYuZmEtdHdpdHRlci1zcXVhcmUge1xuICAgICAgICAgICAgICBjb2xvcjogcmdiKDQyLDE2MywyNDApICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAmLmZhLWZhY2Vib29rLXNxdWFyZSB7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2IoNTksODcsMTU0KSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgYCxcbn07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQVNBO0FBS0E7QUF3RUE7QUF2RkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/login/sign-in-style.tsx
