__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return wrapperRaf; });
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_0__);

var id = 0;
var ids = {}; // Support call raf with delay specified frame

function wrapperRaf(callback) {
  var delayFrames = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
  var myId = id++;
  var restFrames = delayFrames;

  function internalCallback() {
    restFrames -= 1;

    if (restFrames <= 0) {
      callback();
      delete ids[myId];
    } else {
      ids[myId] = raf__WEBPACK_IMPORTED_MODULE_0___default()(internalCallback);
    }
  }

  ids[myId] = raf__WEBPACK_IMPORTED_MODULE_0___default()(internalCallback);
  return myId;
}

wrapperRaf.cancel = function cancel(pid) {
  if (pid === undefined) return;
  raf__WEBPACK_IMPORTED_MODULE_0___default.a.cancel(ids[pid]);
  delete ids[pid];
};

wrapperRaf.ids = ids; // export this for test usage//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9fdXRpbC9yYWYuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL191dGlsL3JhZi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcmFmIGZyb20gJ3JhZic7XG5sZXQgaWQgPSAwO1xuY29uc3QgaWRzID0ge307XG4vLyBTdXBwb3J0IGNhbGwgcmFmIHdpdGggZGVsYXkgc3BlY2lmaWVkIGZyYW1lXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiB3cmFwcGVyUmFmKGNhbGxiYWNrLCBkZWxheUZyYW1lcyA9IDEpIHtcbiAgICBjb25zdCBteUlkID0gaWQrKztcbiAgICBsZXQgcmVzdEZyYW1lcyA9IGRlbGF5RnJhbWVzO1xuICAgIGZ1bmN0aW9uIGludGVybmFsQ2FsbGJhY2soKSB7XG4gICAgICAgIHJlc3RGcmFtZXMgLT0gMTtcbiAgICAgICAgaWYgKHJlc3RGcmFtZXMgPD0gMCkge1xuICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgIGRlbGV0ZSBpZHNbbXlJZF07XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBpZHNbbXlJZF0gPSByYWYoaW50ZXJuYWxDYWxsYmFjayk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaWRzW215SWRdID0gcmFmKGludGVybmFsQ2FsbGJhY2spO1xuICAgIHJldHVybiBteUlkO1xufVxud3JhcHBlclJhZi5jYW5jZWwgPSBmdW5jdGlvbiBjYW5jZWwocGlkKSB7XG4gICAgaWYgKHBpZCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm47XG4gICAgcmFmLmNhbmNlbChpZHNbcGlkXSk7XG4gICAgZGVsZXRlIGlkc1twaWRdO1xufTtcbndyYXBwZXJSYWYuaWRzID0gaWRzOyAvLyBleHBvcnQgdGhpcyBmb3IgdGVzdCB1c2FnZVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBSkE7QUFDQTtBQUtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/_util/raf.js
