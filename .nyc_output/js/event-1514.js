var Eventful = __webpack_require__(/*! ../mixin/Eventful */ "./node_modules/zrender/lib/mixin/Eventful.js");

exports.Dispatcher = Eventful;

var env = __webpack_require__(/*! ./env */ "./node_modules/zrender/lib/core/env.js");
/**
 * 事件辅助类
 * @module zrender/core/event
 * @author Kener (@Kener-林峰, kener.linfeng@gmail.com)
 */


var isDomLevel2 = typeof window !== 'undefined' && !!window.addEventListener;
var MOUSE_EVENT_REG = /^(?:mouse|pointer|contextmenu|drag|drop)|click/;

function getBoundingClientRect(el) {
  // BlackBerry 5, iOS 3 (original iPhone) don't have getBoundingRect
  return el.getBoundingClientRect ? el.getBoundingClientRect() : {
    left: 0,
    top: 0
  };
} // `calculate` is optional, default false


function clientToLocal(el, e, out, calculate) {
  out = out || {}; // According to the W3C Working Draft, offsetX and offsetY should be relative
  // to the padding edge of the target element. The only browser using this convention
  // is IE. Webkit uses the border edge, Opera uses the content edge, and FireFox does
  // not support the properties.
  // (see http://www.jacklmoore.com/notes/mouse-position/)
  // In zr painter.dom, padding edge equals to border edge.
  // FIXME
  // When mousemove event triggered on ec tooltip, target is not zr painter.dom, and
  // offsetX/Y is relative to e.target, where the calculation of zrX/Y via offsetX/Y
  // is too complex. So css-transfrom dont support in this case temporarily.

  if (calculate || !env.canvasSupported) {
    defaultGetZrXY(el, e, out);
  } // Caution: In FireFox, layerX/layerY Mouse position relative to the closest positioned
  // ancestor element, so we should make sure el is positioned (e.g., not position:static).
  // BTW1, Webkit don't return the same results as FF in non-simple cases (like add
  // zoom-factor, overflow / opacity layers, transforms ...)
  // BTW2, (ev.offsetY || ev.pageY - $(ev.target).offset().top) is not correct in preserve-3d.
  // <https://bugs.jquery.com/ticket/8523#comment:14>
  // BTW3, In ff, offsetX/offsetY is always 0.
  else if (env.browser.firefox && e.layerX != null && e.layerX !== e.offsetX) {
      out.zrX = e.layerX;
      out.zrY = e.layerY;
    } // For IE6+, chrome, safari, opera. (When will ff support offsetX?)
    else if (e.offsetX != null) {
        out.zrX = e.offsetX;
        out.zrY = e.offsetY;
      } // For some other device, e.g., IOS safari.
      else {
          defaultGetZrXY(el, e, out);
        }

  return out;
}

function defaultGetZrXY(el, e, out) {
  // This well-known method below does not support css transform.
  var box = getBoundingClientRect(el);
  out.zrX = e.clientX - box.left;
  out.zrY = e.clientY - box.top;
}
/**
 * 如果存在第三方嵌入的一些dom触发的事件，或touch事件，需要转换一下事件坐标.
 * `calculate` is optional, default false.
 */


function normalizeEvent(el, e, calculate) {
  e = e || window.event;

  if (e.zrX != null) {
    return e;
  }

  var eventType = e.type;
  var isTouch = eventType && eventType.indexOf('touch') >= 0;

  if (!isTouch) {
    clientToLocal(el, e, e, calculate);
    e.zrDelta = e.wheelDelta ? e.wheelDelta / 120 : -(e.detail || 0) / 3;
  } else {
    var touch = eventType !== 'touchend' ? e.targetTouches[0] : e.changedTouches[0];
    touch && clientToLocal(el, touch, e, calculate);
  } // Add which for click: 1 === left; 2 === middle; 3 === right; otherwise: 0;
  // See jQuery: https://github.com/jquery/jquery/blob/master/src/event.js
  // If e.which has been defined, if may be readonly,
  // see: https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/which


  var button = e.button;

  if (e.which == null && button !== undefined && MOUSE_EVENT_REG.test(e.type)) {
    e.which = button & 1 ? 1 : button & 2 ? 3 : button & 4 ? 2 : 0;
  } // [Caution]: `e.which` from browser is not always reliable. For example,
  // when press left button and `mousemove (pointermove)` in Edge, the `e.which`
  // is 65536 and the `e.button` is -1. But the `mouseup (pointerup)` and
  // `mousedown (pointerdown)` is the same as Chrome does.


  return e;
}
/**
 * @param {HTMLElement} el
 * @param {string} name
 * @param {Function} handler
 */


function addEventListener(el, name, handler) {
  if (isDomLevel2) {
    // Reproduct the console warning:
    // [Violation] Added non-passive event listener to a scroll-blocking <some> event.
    // Consider marking event handler as 'passive' to make the page more responsive.
    // Just set console log level: verbose in chrome dev tool.
    // then the warning log will be printed when addEventListener called.
    // See https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md
    // We have not yet found a neat way to using passive. Because in zrender the dom event
    // listener delegate all of the upper events of element. Some of those events need
    // to prevent default. For example, the feature `preventDefaultMouseMove` of echarts.
    // Before passive can be adopted, these issues should be considered:
    // (1) Whether and how a zrender user specifies an event listener passive. And by default,
    // passive or not.
    // (2) How to tread that some zrender event listener is passive, and some is not. If
    // we use other way but not preventDefault of mousewheel and touchmove, browser
    // compatibility should be handled.
    // var opts = (env.passiveSupported && name === 'mousewheel')
    //     ? {passive: true}
    //     // By default, the third param of el.addEventListener is `capture: false`.
    //     : void 0;
    // el.addEventListener(name, handler /* , opts */);
    el.addEventListener(name, handler);
  } else {
    el.attachEvent('on' + name, handler);
  }
}

function removeEventListener(el, name, handler) {
  if (isDomLevel2) {
    el.removeEventListener(name, handler);
  } else {
    el.detachEvent('on' + name, handler);
  }
}
/**
 * preventDefault and stopPropagation.
 * Notice: do not do that in zrender. Upper application
 * do that if necessary.
 *
 * @memberOf module:zrender/core/event
 * @method
 * @param {Event} e : event对象
 */


var stop = isDomLevel2 ? function (e) {
  e.preventDefault();
  e.stopPropagation();
  e.cancelBubble = true;
} : function (e) {
  e.returnValue = false;
  e.cancelBubble = true;
};
/**
 * This method only works for mouseup and mousedown. The functionality is restricted
 * for fault tolerance, See the `e.which` compatibility above.
 *
 * @param {MouseEvent} e
 * @return {boolean}
 */

function isMiddleOrRightButtonOnMouseUpDown(e) {
  return e.which === 2 || e.which === 3;
}
/**
 * To be removed.
 * @deprecated
 */


function notLeftMouse(e) {
  // If e.which is undefined, considered as left mouse event.
  return e.which > 1;
} // 做向上兼容


exports.clientToLocal = clientToLocal;
exports.normalizeEvent = normalizeEvent;
exports.addEventListener = addEventListener;
exports.removeEventListener = removeEventListener;
exports.stop = stop;
exports.isMiddleOrRightButtonOnMouseUpDown = isMiddleOrRightButtonOnMouseUpDown;
exports.notLeftMouse = notLeftMouse;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29yZS9ldmVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2NvcmUvZXZlbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIEV2ZW50ZnVsID0gcmVxdWlyZShcIi4uL21peGluL0V2ZW50ZnVsXCIpO1xuXG5leHBvcnRzLkRpc3BhdGNoZXIgPSBFdmVudGZ1bDtcblxudmFyIGVudiA9IHJlcXVpcmUoXCIuL2VudlwiKTtcblxuLyoqXG4gKiDkuovku7bovoXliqnnsbtcbiAqIEBtb2R1bGUgenJlbmRlci9jb3JlL2V2ZW50XG4gKiBAYXV0aG9yIEtlbmVyIChAS2VuZXIt5p6X5bOwLCBrZW5lci5saW5mZW5nQGdtYWlsLmNvbSlcbiAqL1xudmFyIGlzRG9tTGV2ZWwyID0gdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgISF3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcjtcbnZhciBNT1VTRV9FVkVOVF9SRUcgPSAvXig/Om1vdXNlfHBvaW50ZXJ8Y29udGV4dG1lbnV8ZHJhZ3xkcm9wKXxjbGljay87XG5cbmZ1bmN0aW9uIGdldEJvdW5kaW5nQ2xpZW50UmVjdChlbCkge1xuICAvLyBCbGFja0JlcnJ5IDUsIGlPUyAzIChvcmlnaW5hbCBpUGhvbmUpIGRvbid0IGhhdmUgZ2V0Qm91bmRpbmdSZWN0XG4gIHJldHVybiBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QgPyBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSA6IHtcbiAgICBsZWZ0OiAwLFxuICAgIHRvcDogMFxuICB9O1xufSAvLyBgY2FsY3VsYXRlYCBpcyBvcHRpb25hbCwgZGVmYXVsdCBmYWxzZVxuXG5cbmZ1bmN0aW9uIGNsaWVudFRvTG9jYWwoZWwsIGUsIG91dCwgY2FsY3VsYXRlKSB7XG4gIG91dCA9IG91dCB8fCB7fTsgLy8gQWNjb3JkaW5nIHRvIHRoZSBXM0MgV29ya2luZyBEcmFmdCwgb2Zmc2V0WCBhbmQgb2Zmc2V0WSBzaG91bGQgYmUgcmVsYXRpdmVcbiAgLy8gdG8gdGhlIHBhZGRpbmcgZWRnZSBvZiB0aGUgdGFyZ2V0IGVsZW1lbnQuIFRoZSBvbmx5IGJyb3dzZXIgdXNpbmcgdGhpcyBjb252ZW50aW9uXG4gIC8vIGlzIElFLiBXZWJraXQgdXNlcyB0aGUgYm9yZGVyIGVkZ2UsIE9wZXJhIHVzZXMgdGhlIGNvbnRlbnQgZWRnZSwgYW5kIEZpcmVGb3ggZG9lc1xuICAvLyBub3Qgc3VwcG9ydCB0aGUgcHJvcGVydGllcy5cbiAgLy8gKHNlZSBodHRwOi8vd3d3LmphY2tsbW9vcmUuY29tL25vdGVzL21vdXNlLXBvc2l0aW9uLylcbiAgLy8gSW4genIgcGFpbnRlci5kb20sIHBhZGRpbmcgZWRnZSBlcXVhbHMgdG8gYm9yZGVyIGVkZ2UuXG4gIC8vIEZJWE1FXG4gIC8vIFdoZW4gbW91c2Vtb3ZlIGV2ZW50IHRyaWdnZXJlZCBvbiBlYyB0b29sdGlwLCB0YXJnZXQgaXMgbm90IHpyIHBhaW50ZXIuZG9tLCBhbmRcbiAgLy8gb2Zmc2V0WC9ZIGlzIHJlbGF0aXZlIHRvIGUudGFyZ2V0LCB3aGVyZSB0aGUgY2FsY3VsYXRpb24gb2YgenJYL1kgdmlhIG9mZnNldFgvWVxuICAvLyBpcyB0b28gY29tcGxleC4gU28gY3NzLXRyYW5zZnJvbSBkb250IHN1cHBvcnQgaW4gdGhpcyBjYXNlIHRlbXBvcmFyaWx5LlxuXG4gIGlmIChjYWxjdWxhdGUgfHwgIWVudi5jYW52YXNTdXBwb3J0ZWQpIHtcbiAgICBkZWZhdWx0R2V0WnJYWShlbCwgZSwgb3V0KTtcbiAgfSAvLyBDYXV0aW9uOiBJbiBGaXJlRm94LCBsYXllclgvbGF5ZXJZIE1vdXNlIHBvc2l0aW9uIHJlbGF0aXZlIHRvIHRoZSBjbG9zZXN0IHBvc2l0aW9uZWRcbiAgLy8gYW5jZXN0b3IgZWxlbWVudCwgc28gd2Ugc2hvdWxkIG1ha2Ugc3VyZSBlbCBpcyBwb3NpdGlvbmVkIChlLmcuLCBub3QgcG9zaXRpb246c3RhdGljKS5cbiAgLy8gQlRXMSwgV2Via2l0IGRvbid0IHJldHVybiB0aGUgc2FtZSByZXN1bHRzIGFzIEZGIGluIG5vbi1zaW1wbGUgY2FzZXMgKGxpa2UgYWRkXG4gIC8vIHpvb20tZmFjdG9yLCBvdmVyZmxvdyAvIG9wYWNpdHkgbGF5ZXJzLCB0cmFuc2Zvcm1zIC4uLilcbiAgLy8gQlRXMiwgKGV2Lm9mZnNldFkgfHwgZXYucGFnZVkgLSAkKGV2LnRhcmdldCkub2Zmc2V0KCkudG9wKSBpcyBub3QgY29ycmVjdCBpbiBwcmVzZXJ2ZS0zZC5cbiAgLy8gPGh0dHBzOi8vYnVncy5qcXVlcnkuY29tL3RpY2tldC84NTIzI2NvbW1lbnQ6MTQ+XG4gIC8vIEJUVzMsIEluIGZmLCBvZmZzZXRYL29mZnNldFkgaXMgYWx3YXlzIDAuXG4gIGVsc2UgaWYgKGVudi5icm93c2VyLmZpcmVmb3ggJiYgZS5sYXllclggIT0gbnVsbCAmJiBlLmxheWVyWCAhPT0gZS5vZmZzZXRYKSB7XG4gICAgICBvdXQuenJYID0gZS5sYXllclg7XG4gICAgICBvdXQuenJZID0gZS5sYXllclk7XG4gICAgfSAvLyBGb3IgSUU2KywgY2hyb21lLCBzYWZhcmksIG9wZXJhLiAoV2hlbiB3aWxsIGZmIHN1cHBvcnQgb2Zmc2V0WD8pXG4gICAgZWxzZSBpZiAoZS5vZmZzZXRYICE9IG51bGwpIHtcbiAgICAgICAgb3V0LnpyWCA9IGUub2Zmc2V0WDtcbiAgICAgICAgb3V0LnpyWSA9IGUub2Zmc2V0WTtcbiAgICAgIH0gLy8gRm9yIHNvbWUgb3RoZXIgZGV2aWNlLCBlLmcuLCBJT1Mgc2FmYXJpLlxuICAgICAgZWxzZSB7XG4gICAgICAgICAgZGVmYXVsdEdldFpyWFkoZWwsIGUsIG91dCk7XG4gICAgICAgIH1cblxuICByZXR1cm4gb3V0O1xufVxuXG5mdW5jdGlvbiBkZWZhdWx0R2V0WnJYWShlbCwgZSwgb3V0KSB7XG4gIC8vIFRoaXMgd2VsbC1rbm93biBtZXRob2QgYmVsb3cgZG9lcyBub3Qgc3VwcG9ydCBjc3MgdHJhbnNmb3JtLlxuICB2YXIgYm94ID0gZ2V0Qm91bmRpbmdDbGllbnRSZWN0KGVsKTtcbiAgb3V0LnpyWCA9IGUuY2xpZW50WCAtIGJveC5sZWZ0O1xuICBvdXQuenJZID0gZS5jbGllbnRZIC0gYm94LnRvcDtcbn1cbi8qKlxuICog5aaC5p6c5a2Y5Zyo56ys5LiJ5pa55bWM5YWl55qE5LiA5LqbZG9t6Kem5Y+R55qE5LqL5Lu277yM5oiWdG91Y2jkuovku7bvvIzpnIDopoHovazmjaLkuIDkuIvkuovku7blnZDmoIcuXG4gKiBgY2FsY3VsYXRlYCBpcyBvcHRpb25hbCwgZGVmYXVsdCBmYWxzZS5cbiAqL1xuXG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUV2ZW50KGVsLCBlLCBjYWxjdWxhdGUpIHtcbiAgZSA9IGUgfHwgd2luZG93LmV2ZW50O1xuXG4gIGlmIChlLnpyWCAhPSBudWxsKSB7XG4gICAgcmV0dXJuIGU7XG4gIH1cblxuICB2YXIgZXZlbnRUeXBlID0gZS50eXBlO1xuICB2YXIgaXNUb3VjaCA9IGV2ZW50VHlwZSAmJiBldmVudFR5cGUuaW5kZXhPZigndG91Y2gnKSA+PSAwO1xuXG4gIGlmICghaXNUb3VjaCkge1xuICAgIGNsaWVudFRvTG9jYWwoZWwsIGUsIGUsIGNhbGN1bGF0ZSk7XG4gICAgZS56ckRlbHRhID0gZS53aGVlbERlbHRhID8gZS53aGVlbERlbHRhIC8gMTIwIDogLShlLmRldGFpbCB8fCAwKSAvIDM7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHRvdWNoID0gZXZlbnRUeXBlICE9PSAndG91Y2hlbmQnID8gZS50YXJnZXRUb3VjaGVzWzBdIDogZS5jaGFuZ2VkVG91Y2hlc1swXTtcbiAgICB0b3VjaCAmJiBjbGllbnRUb0xvY2FsKGVsLCB0b3VjaCwgZSwgY2FsY3VsYXRlKTtcbiAgfSAvLyBBZGQgd2hpY2ggZm9yIGNsaWNrOiAxID09PSBsZWZ0OyAyID09PSBtaWRkbGU7IDMgPT09IHJpZ2h0OyBvdGhlcndpc2U6IDA7XG4gIC8vIFNlZSBqUXVlcnk6IGh0dHBzOi8vZ2l0aHViLmNvbS9qcXVlcnkvanF1ZXJ5L2Jsb2IvbWFzdGVyL3NyYy9ldmVudC5qc1xuICAvLyBJZiBlLndoaWNoIGhhcyBiZWVuIGRlZmluZWQsIGlmIG1heSBiZSByZWFkb25seSxcbiAgLy8gc2VlOiBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BUEkvTW91c2VFdmVudC93aGljaFxuXG5cbiAgdmFyIGJ1dHRvbiA9IGUuYnV0dG9uO1xuXG4gIGlmIChlLndoaWNoID09IG51bGwgJiYgYnV0dG9uICE9PSB1bmRlZmluZWQgJiYgTU9VU0VfRVZFTlRfUkVHLnRlc3QoZS50eXBlKSkge1xuICAgIGUud2hpY2ggPSBidXR0b24gJiAxID8gMSA6IGJ1dHRvbiAmIDIgPyAzIDogYnV0dG9uICYgNCA/IDIgOiAwO1xuICB9IC8vIFtDYXV0aW9uXTogYGUud2hpY2hgIGZyb20gYnJvd3NlciBpcyBub3QgYWx3YXlzIHJlbGlhYmxlLiBGb3IgZXhhbXBsZSxcbiAgLy8gd2hlbiBwcmVzcyBsZWZ0IGJ1dHRvbiBhbmQgYG1vdXNlbW92ZSAocG9pbnRlcm1vdmUpYCBpbiBFZGdlLCB0aGUgYGUud2hpY2hgXG4gIC8vIGlzIDY1NTM2IGFuZCB0aGUgYGUuYnV0dG9uYCBpcyAtMS4gQnV0IHRoZSBgbW91c2V1cCAocG9pbnRlcnVwKWAgYW5kXG4gIC8vIGBtb3VzZWRvd24gKHBvaW50ZXJkb3duKWAgaXMgdGhlIHNhbWUgYXMgQ2hyb21lIGRvZXMuXG5cblxuICByZXR1cm4gZTtcbn1cbi8qKlxuICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxcbiAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBoYW5kbGVyXG4gKi9cblxuXG5mdW5jdGlvbiBhZGRFdmVudExpc3RlbmVyKGVsLCBuYW1lLCBoYW5kbGVyKSB7XG4gIGlmIChpc0RvbUxldmVsMikge1xuICAgIC8vIFJlcHJvZHVjdCB0aGUgY29uc29sZSB3YXJuaW5nOlxuICAgIC8vIFtWaW9sYXRpb25dIEFkZGVkIG5vbi1wYXNzaXZlIGV2ZW50IGxpc3RlbmVyIHRvIGEgc2Nyb2xsLWJsb2NraW5nIDxzb21lPiBldmVudC5cbiAgICAvLyBDb25zaWRlciBtYXJraW5nIGV2ZW50IGhhbmRsZXIgYXMgJ3Bhc3NpdmUnIHRvIG1ha2UgdGhlIHBhZ2UgbW9yZSByZXNwb25zaXZlLlxuICAgIC8vIEp1c3Qgc2V0IGNvbnNvbGUgbG9nIGxldmVsOiB2ZXJib3NlIGluIGNocm9tZSBkZXYgdG9vbC5cbiAgICAvLyB0aGVuIHRoZSB3YXJuaW5nIGxvZyB3aWxsIGJlIHByaW50ZWQgd2hlbiBhZGRFdmVudExpc3RlbmVyIGNhbGxlZC5cbiAgICAvLyBTZWUgaHR0cHM6Ly9naXRodWIuY29tL1dJQ0cvRXZlbnRMaXN0ZW5lck9wdGlvbnMvYmxvYi9naC1wYWdlcy9leHBsYWluZXIubWRcbiAgICAvLyBXZSBoYXZlIG5vdCB5ZXQgZm91bmQgYSBuZWF0IHdheSB0byB1c2luZyBwYXNzaXZlLiBCZWNhdXNlIGluIHpyZW5kZXIgdGhlIGRvbSBldmVudFxuICAgIC8vIGxpc3RlbmVyIGRlbGVnYXRlIGFsbCBvZiB0aGUgdXBwZXIgZXZlbnRzIG9mIGVsZW1lbnQuIFNvbWUgb2YgdGhvc2UgZXZlbnRzIG5lZWRcbiAgICAvLyB0byBwcmV2ZW50IGRlZmF1bHQuIEZvciBleGFtcGxlLCB0aGUgZmVhdHVyZSBgcHJldmVudERlZmF1bHRNb3VzZU1vdmVgIG9mIGVjaGFydHMuXG4gICAgLy8gQmVmb3JlIHBhc3NpdmUgY2FuIGJlIGFkb3B0ZWQsIHRoZXNlIGlzc3VlcyBzaG91bGQgYmUgY29uc2lkZXJlZDpcbiAgICAvLyAoMSkgV2hldGhlciBhbmQgaG93IGEgenJlbmRlciB1c2VyIHNwZWNpZmllcyBhbiBldmVudCBsaXN0ZW5lciBwYXNzaXZlLiBBbmQgYnkgZGVmYXVsdCxcbiAgICAvLyBwYXNzaXZlIG9yIG5vdC5cbiAgICAvLyAoMikgSG93IHRvIHRyZWFkIHRoYXQgc29tZSB6cmVuZGVyIGV2ZW50IGxpc3RlbmVyIGlzIHBhc3NpdmUsIGFuZCBzb21lIGlzIG5vdC4gSWZcbiAgICAvLyB3ZSB1c2Ugb3RoZXIgd2F5IGJ1dCBub3QgcHJldmVudERlZmF1bHQgb2YgbW91c2V3aGVlbCBhbmQgdG91Y2htb3ZlLCBicm93c2VyXG4gICAgLy8gY29tcGF0aWJpbGl0eSBzaG91bGQgYmUgaGFuZGxlZC5cbiAgICAvLyB2YXIgb3B0cyA9IChlbnYucGFzc2l2ZVN1cHBvcnRlZCAmJiBuYW1lID09PSAnbW91c2V3aGVlbCcpXG4gICAgLy8gICAgID8ge3Bhc3NpdmU6IHRydWV9XG4gICAgLy8gICAgIC8vIEJ5IGRlZmF1bHQsIHRoZSB0aGlyZCBwYXJhbSBvZiBlbC5hZGRFdmVudExpc3RlbmVyIGlzIGBjYXB0dXJlOiBmYWxzZWAuXG4gICAgLy8gICAgIDogdm9pZCAwO1xuICAgIC8vIGVsLmFkZEV2ZW50TGlzdGVuZXIobmFtZSwgaGFuZGxlciAvKiAsIG9wdHMgKi8pO1xuICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIobmFtZSwgaGFuZGxlcik7XG4gIH0gZWxzZSB7XG4gICAgZWwuYXR0YWNoRXZlbnQoJ29uJyArIG5hbWUsIGhhbmRsZXIpO1xuICB9XG59XG5cbmZ1bmN0aW9uIHJlbW92ZUV2ZW50TGlzdGVuZXIoZWwsIG5hbWUsIGhhbmRsZXIpIHtcbiAgaWYgKGlzRG9tTGV2ZWwyKSB7XG4gICAgZWwucmVtb3ZlRXZlbnRMaXN0ZW5lcihuYW1lLCBoYW5kbGVyKTtcbiAgfSBlbHNlIHtcbiAgICBlbC5kZXRhY2hFdmVudCgnb24nICsgbmFtZSwgaGFuZGxlcik7XG4gIH1cbn1cbi8qKlxuICogcHJldmVudERlZmF1bHQgYW5kIHN0b3BQcm9wYWdhdGlvbi5cbiAqIE5vdGljZTogZG8gbm90IGRvIHRoYXQgaW4genJlbmRlci4gVXBwZXIgYXBwbGljYXRpb25cbiAqIGRvIHRoYXQgaWYgbmVjZXNzYXJ5LlxuICpcbiAqIEBtZW1iZXJPZiBtb2R1bGU6enJlbmRlci9jb3JlL2V2ZW50XG4gKiBAbWV0aG9kXG4gKiBAcGFyYW0ge0V2ZW50fSBlIDogZXZlbnTlr7nosaFcbiAqL1xuXG5cbnZhciBzdG9wID0gaXNEb21MZXZlbDIgPyBmdW5jdGlvbiAoZSkge1xuICBlLnByZXZlbnREZWZhdWx0KCk7XG4gIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gIGUuY2FuY2VsQnViYmxlID0gdHJ1ZTtcbn0gOiBmdW5jdGlvbiAoZSkge1xuICBlLnJldHVyblZhbHVlID0gZmFsc2U7XG4gIGUuY2FuY2VsQnViYmxlID0gdHJ1ZTtcbn07XG4vKipcbiAqIFRoaXMgbWV0aG9kIG9ubHkgd29ya3MgZm9yIG1vdXNldXAgYW5kIG1vdXNlZG93bi4gVGhlIGZ1bmN0aW9uYWxpdHkgaXMgcmVzdHJpY3RlZFxuICogZm9yIGZhdWx0IHRvbGVyYW5jZSwgU2VlIHRoZSBgZS53aGljaGAgY29tcGF0aWJpbGl0eSBhYm92ZS5cbiAqXG4gKiBAcGFyYW0ge01vdXNlRXZlbnR9IGVcbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cblxuZnVuY3Rpb24gaXNNaWRkbGVPclJpZ2h0QnV0dG9uT25Nb3VzZVVwRG93bihlKSB7XG4gIHJldHVybiBlLndoaWNoID09PSAyIHx8IGUud2hpY2ggPT09IDM7XG59XG4vKipcbiAqIFRvIGJlIHJlbW92ZWQuXG4gKiBAZGVwcmVjYXRlZFxuICovXG5cblxuZnVuY3Rpb24gbm90TGVmdE1vdXNlKGUpIHtcbiAgLy8gSWYgZS53aGljaCBpcyB1bmRlZmluZWQsIGNvbnNpZGVyZWQgYXMgbGVmdCBtb3VzZSBldmVudC5cbiAgcmV0dXJuIGUud2hpY2ggPiAxO1xufSAvLyDlgZrlkJHkuIrlhbzlrrlcblxuXG5leHBvcnRzLmNsaWVudFRvTG9jYWwgPSBjbGllbnRUb0xvY2FsO1xuZXhwb3J0cy5ub3JtYWxpemVFdmVudCA9IG5vcm1hbGl6ZUV2ZW50O1xuZXhwb3J0cy5hZGRFdmVudExpc3RlbmVyID0gYWRkRXZlbnRMaXN0ZW5lcjtcbmV4cG9ydHMucmVtb3ZlRXZlbnRMaXN0ZW5lciA9IHJlbW92ZUV2ZW50TGlzdGVuZXI7XG5leHBvcnRzLnN0b3AgPSBzdG9wO1xuZXhwb3J0cy5pc01pZGRsZU9yUmlnaHRCdXR0b25Pbk1vdXNlVXBEb3duID0gaXNNaWRkbGVPclJpZ2h0QnV0dG9uT25Nb3VzZVVwRG93bjtcbmV4cG9ydHMubm90TGVmdE1vdXNlID0gbm90TGVmdE1vdXNlOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/core/event.js
