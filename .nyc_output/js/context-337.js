

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _createReactContext = _interopRequireDefault(__webpack_require__(/*! @ant-design/create-react-context */ "./node_modules/@ant-design/create-react-context/lib/index.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var FormContext = (0, _createReactContext["default"])({
  labelAlign: 'right',
  vertical: false
});
var _default = FormContext;
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9saWIvZm9ybS9jb250ZXh0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9mb3JtL2NvbnRleHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGNyZWF0ZVJlYWN0Q29udGV4dCBmcm9tICdAYW50LWRlc2lnbi9jcmVhdGUtcmVhY3QtY29udGV4dCc7XG5jb25zdCBGb3JtQ29udGV4dCA9IGNyZWF0ZVJlYWN0Q29udGV4dCh7XG4gICAgbGFiZWxBbGlnbjogJ3JpZ2h0JyxcbiAgICB2ZXJ0aWNhbDogZmFsc2UsXG59KTtcbmV4cG9ydCBkZWZhdWx0IEZvcm1Db250ZXh0O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/lib/form/context.js
