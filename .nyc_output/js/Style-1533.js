var fixShadow = __webpack_require__(/*! ./helper/fixShadow */ "./node_modules/zrender/lib/graphic/helper/fixShadow.js");

var _constant = __webpack_require__(/*! ./constant */ "./node_modules/zrender/lib/graphic/constant.js");

var ContextCachedBy = _constant.ContextCachedBy;
var STYLE_COMMON_PROPS = [['shadowBlur', 0], ['shadowOffsetX', 0], ['shadowOffsetY', 0], ['shadowColor', '#000'], ['lineCap', 'butt'], ['lineJoin', 'miter'], ['miterLimit', 10]]; // var SHADOW_PROPS = STYLE_COMMON_PROPS.slice(0, 4);
// var LINE_PROPS = STYLE_COMMON_PROPS.slice(4);

var Style = function Style(opts) {
  this.extendFrom(opts, false);
};

function createLinearGradient(ctx, obj, rect) {
  var x = obj.x == null ? 0 : obj.x;
  var x2 = obj.x2 == null ? 1 : obj.x2;
  var y = obj.y == null ? 0 : obj.y;
  var y2 = obj.y2 == null ? 0 : obj.y2;

  if (!obj.global) {
    x = x * rect.width + rect.x;
    x2 = x2 * rect.width + rect.x;
    y = y * rect.height + rect.y;
    y2 = y2 * rect.height + rect.y;
  } // Fix NaN when rect is Infinity


  x = isNaN(x) ? 0 : x;
  x2 = isNaN(x2) ? 1 : x2;
  y = isNaN(y) ? 0 : y;
  y2 = isNaN(y2) ? 0 : y2;
  var canvasGradient = ctx.createLinearGradient(x, y, x2, y2);
  return canvasGradient;
}

function createRadialGradient(ctx, obj, rect) {
  var width = rect.width;
  var height = rect.height;
  var min = Math.min(width, height);
  var x = obj.x == null ? 0.5 : obj.x;
  var y = obj.y == null ? 0.5 : obj.y;
  var r = obj.r == null ? 0.5 : obj.r;

  if (!obj.global) {
    x = x * width + rect.x;
    y = y * height + rect.y;
    r = r * min;
  }

  var canvasGradient = ctx.createRadialGradient(x, y, 0, x, y, r);
  return canvasGradient;
}

Style.prototype = {
  constructor: Style,

  /**
   * @type {string}
   */
  fill: '#000',

  /**
   * @type {string}
   */
  stroke: null,

  /**
   * @type {number}
   */
  opacity: 1,

  /**
   * @type {number}
   */
  fillOpacity: null,

  /**
   * @type {number}
   */
  strokeOpacity: null,

  /**
   * @type {Array.<number>}
   */
  lineDash: null,

  /**
   * @type {number}
   */
  lineDashOffset: 0,

  /**
   * @type {number}
   */
  shadowBlur: 0,

  /**
   * @type {number}
   */
  shadowOffsetX: 0,

  /**
   * @type {number}
   */
  shadowOffsetY: 0,

  /**
   * @type {number}
   */
  lineWidth: 1,

  /**
   * If stroke ignore scale
   * @type {Boolean}
   */
  strokeNoScale: false,
  // Bounding rect text configuration
  // Not affected by element transform

  /**
   * @type {string}
   */
  text: null,

  /**
   * If `fontSize` or `fontFamily` exists, `font` will be reset by
   * `fontSize`, `fontStyle`, `fontWeight`, `fontFamily`.
   * So do not visit it directly in upper application (like echarts),
   * but use `contain/text#makeFont` instead.
   * @type {string}
   */
  font: null,

  /**
   * The same as font. Use font please.
   * @deprecated
   * @type {string}
   */
  textFont: null,

  /**
   * It helps merging respectively, rather than parsing an entire font string.
   * @type {string}
   */
  fontStyle: null,

  /**
   * It helps merging respectively, rather than parsing an entire font string.
   * @type {string}
   */
  fontWeight: null,

  /**
   * It helps merging respectively, rather than parsing an entire font string.
   * Should be 12 but not '12px'.
   * @type {number}
   */
  fontSize: null,

  /**
   * It helps merging respectively, rather than parsing an entire font string.
   * @type {string}
   */
  fontFamily: null,

  /**
   * Reserved for special functinality, like 'hr'.
   * @type {string}
   */
  textTag: null,

  /**
   * @type {string}
   */
  textFill: '#000',

  /**
   * @type {string}
   */
  textStroke: null,

  /**
   * @type {number}
   */
  textWidth: null,

  /**
   * Only for textBackground.
   * @type {number}
   */
  textHeight: null,

  /**
   * textStroke may be set as some color as a default
   * value in upper applicaion, where the default value
   * of textStrokeWidth should be 0 to make sure that
   * user can choose to do not use text stroke.
   * @type {number}
   */
  textStrokeWidth: 0,

  /**
   * @type {number}
   */
  textLineHeight: null,

  /**
   * 'inside', 'left', 'right', 'top', 'bottom'
   * [x, y]
   * Based on x, y of rect.
   * @type {string|Array.<number>}
   * @default 'inside'
   */
  textPosition: 'inside',

  /**
   * If not specified, use the boundingRect of a `displayable`.
   * @type {Object}
   */
  textRect: null,

  /**
   * [x, y]
   * @type {Array.<number>}
   */
  textOffset: null,

  /**
   * @type {string}
   */
  textAlign: null,

  /**
   * @type {string}
   */
  textVerticalAlign: null,

  /**
   * @type {number}
   */
  textDistance: 5,

  /**
   * @type {string}
   */
  textShadowColor: 'transparent',

  /**
   * @type {number}
   */
  textShadowBlur: 0,

  /**
   * @type {number}
   */
  textShadowOffsetX: 0,

  /**
   * @type {number}
   */
  textShadowOffsetY: 0,

  /**
   * @type {string}
   */
  textBoxShadowColor: 'transparent',

  /**
   * @type {number}
   */
  textBoxShadowBlur: 0,

  /**
   * @type {number}
   */
  textBoxShadowOffsetX: 0,

  /**
   * @type {number}
   */
  textBoxShadowOffsetY: 0,

  /**
   * Whether transform text.
   * Only useful in Path and Image element
   * @type {boolean}
   */
  transformText: false,

  /**
   * Text rotate around position of Path or Image
   * Only useful in Path and Image element and transformText is false.
   */
  textRotation: 0,

  /**
   * Text origin of text rotation, like [10, 40].
   * Based on x, y of rect.
   * Useful in label rotation of circular symbol.
   * By default, this origin is textPosition.
   * Can be 'center'.
   * @type {string|Array.<number>}
   */
  textOrigin: null,

  /**
   * @type {string}
   */
  textBackgroundColor: null,

  /**
   * @type {string}
   */
  textBorderColor: null,

  /**
   * @type {number}
   */
  textBorderWidth: 0,

  /**
   * @type {number}
   */
  textBorderRadius: 0,

  /**
   * Can be `2` or `[2, 4]` or `[2, 3, 4, 5]`
   * @type {number|Array.<number>}
   */
  textPadding: null,

  /**
   * Text styles for rich text.
   * @type {Object}
   */
  rich: null,

  /**
   * {outerWidth, outerHeight, ellipsis, placeholder}
   * @type {Object}
   */
  truncate: null,

  /**
   * https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/globalCompositeOperation
   * @type {string}
   */
  blend: null,

  /**
   * @param {CanvasRenderingContext2D} ctx
   */
  bind: function bind(ctx, el, prevEl) {
    var style = this;
    var prevStyle = prevEl && prevEl.style; // If no prevStyle, it means first draw.
    // Only apply cache if the last time cachced by this function.

    var notCheckCache = !prevStyle || ctx.__attrCachedBy !== ContextCachedBy.STYLE_BIND;
    ctx.__attrCachedBy = ContextCachedBy.STYLE_BIND;

    for (var i = 0; i < STYLE_COMMON_PROPS.length; i++) {
      var prop = STYLE_COMMON_PROPS[i];
      var styleName = prop[0];

      if (notCheckCache || style[styleName] !== prevStyle[styleName]) {
        // FIXME Invalid property value will cause style leak from previous element.
        ctx[styleName] = fixShadow(ctx, styleName, style[styleName] || prop[1]);
      }
    }

    if (notCheckCache || style.fill !== prevStyle.fill) {
      ctx.fillStyle = style.fill;
    }

    if (notCheckCache || style.stroke !== prevStyle.stroke) {
      ctx.strokeStyle = style.stroke;
    }

    if (notCheckCache || style.opacity !== prevStyle.opacity) {
      ctx.globalAlpha = style.opacity == null ? 1 : style.opacity;
    }

    if (notCheckCache || style.blend !== prevStyle.blend) {
      ctx.globalCompositeOperation = style.blend || 'source-over';
    }

    if (this.hasStroke()) {
      var lineWidth = style.lineWidth;
      ctx.lineWidth = lineWidth / (this.strokeNoScale && el && el.getLineScale ? el.getLineScale() : 1);
    }
  },
  hasFill: function hasFill() {
    var fill = this.fill;
    return fill != null && fill !== 'none';
  },
  hasStroke: function hasStroke() {
    var stroke = this.stroke;
    return stroke != null && stroke !== 'none' && this.lineWidth > 0;
  },

  /**
   * Extend from other style
   * @param {zrender/graphic/Style} otherStyle
   * @param {boolean} overwrite true: overwrirte any way.
   *                            false: overwrite only when !target.hasOwnProperty
   *                            others: overwrite when property is not null/undefined.
   */
  extendFrom: function extendFrom(otherStyle, overwrite) {
    if (otherStyle) {
      for (var name in otherStyle) {
        if (otherStyle.hasOwnProperty(name) && (overwrite === true || (overwrite === false ? !this.hasOwnProperty(name) : otherStyle[name] != null))) {
          this[name] = otherStyle[name];
        }
      }
    }
  },

  /**
   * Batch setting style with a given object
   * @param {Object|string} obj
   * @param {*} [obj]
   */
  set: function set(obj, value) {
    if (typeof obj === 'string') {
      this[obj] = value;
    } else {
      this.extendFrom(obj, true);
    }
  },

  /**
   * Clone
   * @return {zrender/graphic/Style} [description]
   */
  clone: function clone() {
    var newStyle = new this.constructor();
    newStyle.extendFrom(this, true);
    return newStyle;
  },
  getGradient: function getGradient(ctx, obj, rect) {
    var method = obj.type === 'radial' ? createRadialGradient : createLinearGradient;
    var canvasGradient = method(ctx, obj, rect);
    var colorStops = obj.colorStops;

    for (var i = 0; i < colorStops.length; i++) {
      canvasGradient.addColorStop(colorStops[i].offset, colorStops[i].color);
    }

    return canvasGradient;
  }
};
var styleProto = Style.prototype;

for (var i = 0; i < STYLE_COMMON_PROPS.length; i++) {
  var prop = STYLE_COMMON_PROPS[i];

  if (!(prop[0] in styleProto)) {
    styleProto[prop[0]] = prop[1];
  }
} // Provide for others


Style.getGradient = styleProto.getGradient;
var _default = Style;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9TdHlsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvU3R5bGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGZpeFNoYWRvdyA9IHJlcXVpcmUoXCIuL2hlbHBlci9maXhTaGFkb3dcIik7XG5cbnZhciBfY29uc3RhbnQgPSByZXF1aXJlKFwiLi9jb25zdGFudFwiKTtcblxudmFyIENvbnRleHRDYWNoZWRCeSA9IF9jb25zdGFudC5Db250ZXh0Q2FjaGVkQnk7XG52YXIgU1RZTEVfQ09NTU9OX1BST1BTID0gW1snc2hhZG93Qmx1cicsIDBdLCBbJ3NoYWRvd09mZnNldFgnLCAwXSwgWydzaGFkb3dPZmZzZXRZJywgMF0sIFsnc2hhZG93Q29sb3InLCAnIzAwMCddLCBbJ2xpbmVDYXAnLCAnYnV0dCddLCBbJ2xpbmVKb2luJywgJ21pdGVyJ10sIFsnbWl0ZXJMaW1pdCcsIDEwXV07IC8vIHZhciBTSEFET1dfUFJPUFMgPSBTVFlMRV9DT01NT05fUFJPUFMuc2xpY2UoMCwgNCk7XG4vLyB2YXIgTElORV9QUk9QUyA9IFNUWUxFX0NPTU1PTl9QUk9QUy5zbGljZSg0KTtcblxudmFyIFN0eWxlID0gZnVuY3Rpb24gKG9wdHMpIHtcbiAgdGhpcy5leHRlbmRGcm9tKG9wdHMsIGZhbHNlKTtcbn07XG5cbmZ1bmN0aW9uIGNyZWF0ZUxpbmVhckdyYWRpZW50KGN0eCwgb2JqLCByZWN0KSB7XG4gIHZhciB4ID0gb2JqLnggPT0gbnVsbCA/IDAgOiBvYmoueDtcbiAgdmFyIHgyID0gb2JqLngyID09IG51bGwgPyAxIDogb2JqLngyO1xuICB2YXIgeSA9IG9iai55ID09IG51bGwgPyAwIDogb2JqLnk7XG4gIHZhciB5MiA9IG9iai55MiA9PSBudWxsID8gMCA6IG9iai55MjtcblxuICBpZiAoIW9iai5nbG9iYWwpIHtcbiAgICB4ID0geCAqIHJlY3Qud2lkdGggKyByZWN0Lng7XG4gICAgeDIgPSB4MiAqIHJlY3Qud2lkdGggKyByZWN0Lng7XG4gICAgeSA9IHkgKiByZWN0LmhlaWdodCArIHJlY3QueTtcbiAgICB5MiA9IHkyICogcmVjdC5oZWlnaHQgKyByZWN0Lnk7XG4gIH0gLy8gRml4IE5hTiB3aGVuIHJlY3QgaXMgSW5maW5pdHlcblxuXG4gIHggPSBpc05hTih4KSA/IDAgOiB4O1xuICB4MiA9IGlzTmFOKHgyKSA/IDEgOiB4MjtcbiAgeSA9IGlzTmFOKHkpID8gMCA6IHk7XG4gIHkyID0gaXNOYU4oeTIpID8gMCA6IHkyO1xuICB2YXIgY2FudmFzR3JhZGllbnQgPSBjdHguY3JlYXRlTGluZWFyR3JhZGllbnQoeCwgeSwgeDIsIHkyKTtcbiAgcmV0dXJuIGNhbnZhc0dyYWRpZW50O1xufVxuXG5mdW5jdGlvbiBjcmVhdGVSYWRpYWxHcmFkaWVudChjdHgsIG9iaiwgcmVjdCkge1xuICB2YXIgd2lkdGggPSByZWN0LndpZHRoO1xuICB2YXIgaGVpZ2h0ID0gcmVjdC5oZWlnaHQ7XG4gIHZhciBtaW4gPSBNYXRoLm1pbih3aWR0aCwgaGVpZ2h0KTtcbiAgdmFyIHggPSBvYmoueCA9PSBudWxsID8gMC41IDogb2JqLng7XG4gIHZhciB5ID0gb2JqLnkgPT0gbnVsbCA/IDAuNSA6IG9iai55O1xuICB2YXIgciA9IG9iai5yID09IG51bGwgPyAwLjUgOiBvYmoucjtcblxuICBpZiAoIW9iai5nbG9iYWwpIHtcbiAgICB4ID0geCAqIHdpZHRoICsgcmVjdC54O1xuICAgIHkgPSB5ICogaGVpZ2h0ICsgcmVjdC55O1xuICAgIHIgPSByICogbWluO1xuICB9XG5cbiAgdmFyIGNhbnZhc0dyYWRpZW50ID0gY3R4LmNyZWF0ZVJhZGlhbEdyYWRpZW50KHgsIHksIDAsIHgsIHksIHIpO1xuICByZXR1cm4gY2FudmFzR3JhZGllbnQ7XG59XG5cblN0eWxlLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IFN0eWxlLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgZmlsbDogJyMwMDAnLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgc3Ryb2tlOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgb3BhY2l0eTogMSxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIGZpbGxPcGFjaXR5OiBudWxsLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgc3Ryb2tlT3BhY2l0eTogbnVsbCxcblxuICAvKipcbiAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgKi9cbiAgbGluZURhc2g6IG51bGwsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBsaW5lRGFzaE9mZnNldDogMCxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIHNoYWRvd0JsdXI6IDAsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICBzaGFkb3dPZmZzZXRYOiAwLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgc2hhZG93T2Zmc2V0WTogMCxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIGxpbmVXaWR0aDogMSxcblxuICAvKipcbiAgICogSWYgc3Ryb2tlIGlnbm9yZSBzY2FsZVxuICAgKiBAdHlwZSB7Qm9vbGVhbn1cbiAgICovXG4gIHN0cm9rZU5vU2NhbGU6IGZhbHNlLFxuICAvLyBCb3VuZGluZyByZWN0IHRleHQgY29uZmlndXJhdGlvblxuICAvLyBOb3QgYWZmZWN0ZWQgYnkgZWxlbWVudCB0cmFuc2Zvcm1cblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIElmIGBmb250U2l6ZWAgb3IgYGZvbnRGYW1pbHlgIGV4aXN0cywgYGZvbnRgIHdpbGwgYmUgcmVzZXQgYnlcbiAgICogYGZvbnRTaXplYCwgYGZvbnRTdHlsZWAsIGBmb250V2VpZ2h0YCwgYGZvbnRGYW1pbHlgLlxuICAgKiBTbyBkbyBub3QgdmlzaXQgaXQgZGlyZWN0bHkgaW4gdXBwZXIgYXBwbGljYXRpb24gKGxpa2UgZWNoYXJ0cyksXG4gICAqIGJ1dCB1c2UgYGNvbnRhaW4vdGV4dCNtYWtlRm9udGAgaW5zdGVhZC5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIGZvbnQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIFRoZSBzYW1lIGFzIGZvbnQuIFVzZSBmb250IHBsZWFzZS5cbiAgICogQGRlcHJlY2F0ZWRcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHRGb250OiBudWxsLFxuXG4gIC8qKlxuICAgKiBJdCBoZWxwcyBtZXJnaW5nIHJlc3BlY3RpdmVseSwgcmF0aGVyIHRoYW4gcGFyc2luZyBhbiBlbnRpcmUgZm9udCBzdHJpbmcuXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBmb250U3R5bGU6IG51bGwsXG5cbiAgLyoqXG4gICAqIEl0IGhlbHBzIG1lcmdpbmcgcmVzcGVjdGl2ZWx5LCByYXRoZXIgdGhhbiBwYXJzaW5nIGFuIGVudGlyZSBmb250IHN0cmluZy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIGZvbnRXZWlnaHQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIEl0IGhlbHBzIG1lcmdpbmcgcmVzcGVjdGl2ZWx5LCByYXRoZXIgdGhhbiBwYXJzaW5nIGFuIGVudGlyZSBmb250IHN0cmluZy5cbiAgICogU2hvdWxkIGJlIDEyIGJ1dCBub3QgJzEycHgnLlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgZm9udFNpemU6IG51bGwsXG5cbiAgLyoqXG4gICAqIEl0IGhlbHBzIG1lcmdpbmcgcmVzcGVjdGl2ZWx5LCByYXRoZXIgdGhhbiBwYXJzaW5nIGFuIGVudGlyZSBmb250IHN0cmluZy5cbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIGZvbnRGYW1pbHk6IG51bGwsXG5cbiAgLyoqXG4gICAqIFJlc2VydmVkIGZvciBzcGVjaWFsIGZ1bmN0aW5hbGl0eSwgbGlrZSAnaHInLlxuICAgKiBAdHlwZSB7c3RyaW5nfVxuICAgKi9cbiAgdGV4dFRhZzogbnVsbCxcblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHRGaWxsOiAnIzAwMCcsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICB0ZXh0U3Ryb2tlOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgdGV4dFdpZHRoOiBudWxsLFxuXG4gIC8qKlxuICAgKiBPbmx5IGZvciB0ZXh0QmFja2dyb3VuZC5cbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIHRleHRIZWlnaHQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIHRleHRTdHJva2UgbWF5IGJlIHNldCBhcyBzb21lIGNvbG9yIGFzIGEgZGVmYXVsdFxuICAgKiB2YWx1ZSBpbiB1cHBlciBhcHBsaWNhaW9uLCB3aGVyZSB0aGUgZGVmYXVsdCB2YWx1ZVxuICAgKiBvZiB0ZXh0U3Ryb2tlV2lkdGggc2hvdWxkIGJlIDAgdG8gbWFrZSBzdXJlIHRoYXRcbiAgICogdXNlciBjYW4gY2hvb3NlIHRvIGRvIG5vdCB1c2UgdGV4dCBzdHJva2UuXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0U3Ryb2tlV2lkdGg6IDAsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0TGluZUhlaWdodDogbnVsbCxcblxuICAvKipcbiAgICogJ2luc2lkZScsICdsZWZ0JywgJ3JpZ2h0JywgJ3RvcCcsICdib3R0b20nXG4gICAqIFt4LCB5XVxuICAgKiBCYXNlZCBvbiB4LCB5IG9mIHJlY3QuXG4gICAqIEB0eXBlIHtzdHJpbmd8QXJyYXkuPG51bWJlcj59XG4gICAqIEBkZWZhdWx0ICdpbnNpZGUnXG4gICAqL1xuICB0ZXh0UG9zaXRpb246ICdpbnNpZGUnLFxuXG4gIC8qKlxuICAgKiBJZiBub3Qgc3BlY2lmaWVkLCB1c2UgdGhlIGJvdW5kaW5nUmVjdCBvZiBhIGBkaXNwbGF5YWJsZWAuXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICB0ZXh0UmVjdDogbnVsbCxcblxuICAvKipcbiAgICogW3gsIHldXG4gICAqIEB0eXBlIHtBcnJheS48bnVtYmVyPn1cbiAgICovXG4gIHRleHRPZmZzZXQ6IG51bGwsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICB0ZXh0QWxpZ246IG51bGwsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICB0ZXh0VmVydGljYWxBbGlnbjogbnVsbCxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIHRleHREaXN0YW5jZTogNSxcblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHRTaGFkb3dDb2xvcjogJ3RyYW5zcGFyZW50JyxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIHRleHRTaGFkb3dCbHVyOiAwLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgdGV4dFNoYWRvd09mZnNldFg6IDAsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0U2hhZG93T2Zmc2V0WTogMCxcblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHRCb3hTaGFkb3dDb2xvcjogJ3RyYW5zcGFyZW50JyxcblxuICAvKipcbiAgICogQHR5cGUge251bWJlcn1cbiAgICovXG4gIHRleHRCb3hTaGFkb3dCbHVyOiAwLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgKi9cbiAgdGV4dEJveFNoYWRvd09mZnNldFg6IDAsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0Qm94U2hhZG93T2Zmc2V0WTogMCxcblxuICAvKipcbiAgICogV2hldGhlciB0cmFuc2Zvcm0gdGV4dC5cbiAgICogT25seSB1c2VmdWwgaW4gUGF0aCBhbmQgSW1hZ2UgZWxlbWVudFxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICovXG4gIHRyYW5zZm9ybVRleHQ6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiBUZXh0IHJvdGF0ZSBhcm91bmQgcG9zaXRpb24gb2YgUGF0aCBvciBJbWFnZVxuICAgKiBPbmx5IHVzZWZ1bCBpbiBQYXRoIGFuZCBJbWFnZSBlbGVtZW50IGFuZCB0cmFuc2Zvcm1UZXh0IGlzIGZhbHNlLlxuICAgKi9cbiAgdGV4dFJvdGF0aW9uOiAwLFxuXG4gIC8qKlxuICAgKiBUZXh0IG9yaWdpbiBvZiB0ZXh0IHJvdGF0aW9uLCBsaWtlIFsxMCwgNDBdLlxuICAgKiBCYXNlZCBvbiB4LCB5IG9mIHJlY3QuXG4gICAqIFVzZWZ1bCBpbiBsYWJlbCByb3RhdGlvbiBvZiBjaXJjdWxhciBzeW1ib2wuXG4gICAqIEJ5IGRlZmF1bHQsIHRoaXMgb3JpZ2luIGlzIHRleHRQb3NpdGlvbi5cbiAgICogQ2FuIGJlICdjZW50ZXInLlxuICAgKiBAdHlwZSB7c3RyaW5nfEFycmF5LjxudW1iZXI+fVxuICAgKi9cbiAgdGV4dE9yaWdpbjogbnVsbCxcblxuICAvKipcbiAgICogQHR5cGUge3N0cmluZ31cbiAgICovXG4gIHRleHRCYWNrZ3JvdW5kQ29sb3I6IG51bGwsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICB0ZXh0Qm9yZGVyQ29sb3I6IG51bGwsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0Qm9yZGVyV2lkdGg6IDAsXG5cbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuICB0ZXh0Qm9yZGVyUmFkaXVzOiAwLFxuXG4gIC8qKlxuICAgKiBDYW4gYmUgYDJgIG9yIGBbMiwgNF1gIG9yIGBbMiwgMywgNCwgNV1gXG4gICAqIEB0eXBlIHtudW1iZXJ8QXJyYXkuPG51bWJlcj59XG4gICAqL1xuICB0ZXh0UGFkZGluZzogbnVsbCxcblxuICAvKipcbiAgICogVGV4dCBzdHlsZXMgZm9yIHJpY2ggdGV4dC5cbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHJpY2g6IG51bGwsXG5cbiAgLyoqXG4gICAqIHtvdXRlcldpZHRoLCBvdXRlckhlaWdodCwgZWxsaXBzaXMsIHBsYWNlaG9sZGVyfVxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKi9cbiAgdHJ1bmNhdGU6IG51bGwsXG5cbiAgLyoqXG4gICAqIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0FQSS9DYW52YXNSZW5kZXJpbmdDb250ZXh0MkQvZ2xvYmFsQ29tcG9zaXRlT3BlcmF0aW9uXG4gICAqIEB0eXBlIHtzdHJpbmd9XG4gICAqL1xuICBibGVuZDogbnVsbCxcblxuICAvKipcbiAgICogQHBhcmFtIHtDYW52YXNSZW5kZXJpbmdDb250ZXh0MkR9IGN0eFxuICAgKi9cbiAgYmluZDogZnVuY3Rpb24gKGN0eCwgZWwsIHByZXZFbCkge1xuICAgIHZhciBzdHlsZSA9IHRoaXM7XG4gICAgdmFyIHByZXZTdHlsZSA9IHByZXZFbCAmJiBwcmV2RWwuc3R5bGU7IC8vIElmIG5vIHByZXZTdHlsZSwgaXQgbWVhbnMgZmlyc3QgZHJhdy5cbiAgICAvLyBPbmx5IGFwcGx5IGNhY2hlIGlmIHRoZSBsYXN0IHRpbWUgY2FjaGNlZCBieSB0aGlzIGZ1bmN0aW9uLlxuXG4gICAgdmFyIG5vdENoZWNrQ2FjaGUgPSAhcHJldlN0eWxlIHx8IGN0eC5fX2F0dHJDYWNoZWRCeSAhPT0gQ29udGV4dENhY2hlZEJ5LlNUWUxFX0JJTkQ7XG4gICAgY3R4Ll9fYXR0ckNhY2hlZEJ5ID0gQ29udGV4dENhY2hlZEJ5LlNUWUxFX0JJTkQ7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IFNUWUxFX0NPTU1PTl9QUk9QUy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHByb3AgPSBTVFlMRV9DT01NT05fUFJPUFNbaV07XG4gICAgICB2YXIgc3R5bGVOYW1lID0gcHJvcFswXTtcblxuICAgICAgaWYgKG5vdENoZWNrQ2FjaGUgfHwgc3R5bGVbc3R5bGVOYW1lXSAhPT0gcHJldlN0eWxlW3N0eWxlTmFtZV0pIHtcbiAgICAgICAgLy8gRklYTUUgSW52YWxpZCBwcm9wZXJ0eSB2YWx1ZSB3aWxsIGNhdXNlIHN0eWxlIGxlYWsgZnJvbSBwcmV2aW91cyBlbGVtZW50LlxuICAgICAgICBjdHhbc3R5bGVOYW1lXSA9IGZpeFNoYWRvdyhjdHgsIHN0eWxlTmFtZSwgc3R5bGVbc3R5bGVOYW1lXSB8fCBwcm9wWzFdKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAobm90Q2hlY2tDYWNoZSB8fCBzdHlsZS5maWxsICE9PSBwcmV2U3R5bGUuZmlsbCkge1xuICAgICAgY3R4LmZpbGxTdHlsZSA9IHN0eWxlLmZpbGw7XG4gICAgfVxuXG4gICAgaWYgKG5vdENoZWNrQ2FjaGUgfHwgc3R5bGUuc3Ryb2tlICE9PSBwcmV2U3R5bGUuc3Ryb2tlKSB7XG4gICAgICBjdHguc3Ryb2tlU3R5bGUgPSBzdHlsZS5zdHJva2U7XG4gICAgfVxuXG4gICAgaWYgKG5vdENoZWNrQ2FjaGUgfHwgc3R5bGUub3BhY2l0eSAhPT0gcHJldlN0eWxlLm9wYWNpdHkpIHtcbiAgICAgIGN0eC5nbG9iYWxBbHBoYSA9IHN0eWxlLm9wYWNpdHkgPT0gbnVsbCA/IDEgOiBzdHlsZS5vcGFjaXR5O1xuICAgIH1cblxuICAgIGlmIChub3RDaGVja0NhY2hlIHx8IHN0eWxlLmJsZW5kICE9PSBwcmV2U3R5bGUuYmxlbmQpIHtcbiAgICAgIGN0eC5nbG9iYWxDb21wb3NpdGVPcGVyYXRpb24gPSBzdHlsZS5ibGVuZCB8fCAnc291cmNlLW92ZXInO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmhhc1N0cm9rZSgpKSB7XG4gICAgICB2YXIgbGluZVdpZHRoID0gc3R5bGUubGluZVdpZHRoO1xuICAgICAgY3R4LmxpbmVXaWR0aCA9IGxpbmVXaWR0aCAvICh0aGlzLnN0cm9rZU5vU2NhbGUgJiYgZWwgJiYgZWwuZ2V0TGluZVNjYWxlID8gZWwuZ2V0TGluZVNjYWxlKCkgOiAxKTtcbiAgICB9XG4gIH0sXG4gIGhhc0ZpbGw6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZmlsbCA9IHRoaXMuZmlsbDtcbiAgICByZXR1cm4gZmlsbCAhPSBudWxsICYmIGZpbGwgIT09ICdub25lJztcbiAgfSxcbiAgaGFzU3Ryb2tlOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHN0cm9rZSA9IHRoaXMuc3Ryb2tlO1xuICAgIHJldHVybiBzdHJva2UgIT0gbnVsbCAmJiBzdHJva2UgIT09ICdub25lJyAmJiB0aGlzLmxpbmVXaWR0aCA+IDA7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEV4dGVuZCBmcm9tIG90aGVyIHN0eWxlXG4gICAqIEBwYXJhbSB7enJlbmRlci9ncmFwaGljL1N0eWxlfSBvdGhlclN0eWxlXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gb3ZlcndyaXRlIHRydWU6IG92ZXJ3cmlydGUgYW55IHdheS5cbiAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFsc2U6IG92ZXJ3cml0ZSBvbmx5IHdoZW4gIXRhcmdldC5oYXNPd25Qcm9wZXJ0eVxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvdGhlcnM6IG92ZXJ3cml0ZSB3aGVuIHByb3BlcnR5IGlzIG5vdCBudWxsL3VuZGVmaW5lZC5cbiAgICovXG4gIGV4dGVuZEZyb206IGZ1bmN0aW9uIChvdGhlclN0eWxlLCBvdmVyd3JpdGUpIHtcbiAgICBpZiAob3RoZXJTdHlsZSkge1xuICAgICAgZm9yICh2YXIgbmFtZSBpbiBvdGhlclN0eWxlKSB7XG4gICAgICAgIGlmIChvdGhlclN0eWxlLmhhc093blByb3BlcnR5KG5hbWUpICYmIChvdmVyd3JpdGUgPT09IHRydWUgfHwgKG92ZXJ3cml0ZSA9PT0gZmFsc2UgPyAhdGhpcy5oYXNPd25Qcm9wZXJ0eShuYW1lKSA6IG90aGVyU3R5bGVbbmFtZV0gIT0gbnVsbCkpKSB7XG4gICAgICAgICAgdGhpc1tuYW1lXSA9IG90aGVyU3R5bGVbbmFtZV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIEJhdGNoIHNldHRpbmcgc3R5bGUgd2l0aCBhIGdpdmVuIG9iamVjdFxuICAgKiBAcGFyYW0ge09iamVjdHxzdHJpbmd9IG9ialxuICAgKiBAcGFyYW0geyp9IFtvYmpdXG4gICAqL1xuICBzZXQ6IGZ1bmN0aW9uIChvYmosIHZhbHVlKSB7XG4gICAgaWYgKHR5cGVvZiBvYmogPT09ICdzdHJpbmcnKSB7XG4gICAgICB0aGlzW29ial0gPSB2YWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5leHRlbmRGcm9tKG9iaiwgdHJ1ZSk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBDbG9uZVxuICAgKiBAcmV0dXJuIHt6cmVuZGVyL2dyYXBoaWMvU3R5bGV9IFtkZXNjcmlwdGlvbl1cbiAgICovXG4gIGNsb25lOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIG5ld1N0eWxlID0gbmV3IHRoaXMuY29uc3RydWN0b3IoKTtcbiAgICBuZXdTdHlsZS5leHRlbmRGcm9tKHRoaXMsIHRydWUpO1xuICAgIHJldHVybiBuZXdTdHlsZTtcbiAgfSxcbiAgZ2V0R3JhZGllbnQ6IGZ1bmN0aW9uIChjdHgsIG9iaiwgcmVjdCkge1xuICAgIHZhciBtZXRob2QgPSBvYmoudHlwZSA9PT0gJ3JhZGlhbCcgPyBjcmVhdGVSYWRpYWxHcmFkaWVudCA6IGNyZWF0ZUxpbmVhckdyYWRpZW50O1xuICAgIHZhciBjYW52YXNHcmFkaWVudCA9IG1ldGhvZChjdHgsIG9iaiwgcmVjdCk7XG4gICAgdmFyIGNvbG9yU3RvcHMgPSBvYmouY29sb3JTdG9wcztcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY29sb3JTdG9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgY2FudmFzR3JhZGllbnQuYWRkQ29sb3JTdG9wKGNvbG9yU3RvcHNbaV0ub2Zmc2V0LCBjb2xvclN0b3BzW2ldLmNvbG9yKTtcbiAgICB9XG5cbiAgICByZXR1cm4gY2FudmFzR3JhZGllbnQ7XG4gIH1cbn07XG52YXIgc3R5bGVQcm90byA9IFN0eWxlLnByb3RvdHlwZTtcblxuZm9yICh2YXIgaSA9IDA7IGkgPCBTVFlMRV9DT01NT05fUFJPUFMubGVuZ3RoOyBpKyspIHtcbiAgdmFyIHByb3AgPSBTVFlMRV9DT01NT05fUFJPUFNbaV07XG5cbiAgaWYgKCEocHJvcFswXSBpbiBzdHlsZVByb3RvKSkge1xuICAgIHN0eWxlUHJvdG9bcHJvcFswXV0gPSBwcm9wWzFdO1xuICB9XG59IC8vIFByb3ZpZGUgZm9yIG90aGVyc1xuXG5cblN0eWxlLmdldEdyYWRpZW50ID0gc3R5bGVQcm90by5nZXRHcmFkaWVudDtcbnZhciBfZGVmYXVsdCA9IFN0eWxlO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1WUE7QUE4WUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/Style.js
