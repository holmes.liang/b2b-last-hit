var zrUtil = __webpack_require__(/*! ../core/util */ "./node_modules/zrender/lib/core/util.js");

var Gradient = __webpack_require__(/*! ./Gradient */ "./node_modules/zrender/lib/graphic/Gradient.js");
/**
 * x, y, r are all percent from 0 to 1
 * @param {number} [x=0.5]
 * @param {number} [y=0.5]
 * @param {number} [r=0.5]
 * @param {Array.<Object>} [colorStops]
 * @param {boolean} [globalCoord=false]
 */


var RadialGradient = function RadialGradient(x, y, r, colorStops, globalCoord) {
  // Should do nothing more in this constructor. Because gradient can be
  // declard by `color: {type: 'radial', colorStops: ...}`, where
  // this constructor will not be called.
  this.x = x == null ? 0.5 : x;
  this.y = y == null ? 0.5 : y;
  this.r = r == null ? 0.5 : r; // Can be cloned

  this.type = 'radial'; // If use global coord

  this.global = globalCoord || false;
  Gradient.call(this, colorStops);
};

RadialGradient.prototype = {
  constructor: RadialGradient
};
zrUtil.inherits(RadialGradient, Gradient);
var _default = RadialGradient;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9SYWRpYWxHcmFkaWVudC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvUmFkaWFsR3JhZGllbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHpyVXRpbCA9IHJlcXVpcmUoXCIuLi9jb3JlL3V0aWxcIik7XG5cbnZhciBHcmFkaWVudCA9IHJlcXVpcmUoXCIuL0dyYWRpZW50XCIpO1xuXG4vKipcbiAqIHgsIHksIHIgYXJlIGFsbCBwZXJjZW50IGZyb20gMCB0byAxXG4gKiBAcGFyYW0ge251bWJlcn0gW3g9MC41XVxuICogQHBhcmFtIHtudW1iZXJ9IFt5PTAuNV1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbcj0wLjVdXG4gKiBAcGFyYW0ge0FycmF5LjxPYmplY3Q+fSBbY29sb3JTdG9wc11cbiAqIEBwYXJhbSB7Ym9vbGVhbn0gW2dsb2JhbENvb3JkPWZhbHNlXVxuICovXG52YXIgUmFkaWFsR3JhZGllbnQgPSBmdW5jdGlvbiAoeCwgeSwgciwgY29sb3JTdG9wcywgZ2xvYmFsQ29vcmQpIHtcbiAgLy8gU2hvdWxkIGRvIG5vdGhpbmcgbW9yZSBpbiB0aGlzIGNvbnN0cnVjdG9yLiBCZWNhdXNlIGdyYWRpZW50IGNhbiBiZVxuICAvLyBkZWNsYXJkIGJ5IGBjb2xvcjoge3R5cGU6ICdyYWRpYWwnLCBjb2xvclN0b3BzOiAuLi59YCwgd2hlcmVcbiAgLy8gdGhpcyBjb25zdHJ1Y3RvciB3aWxsIG5vdCBiZSBjYWxsZWQuXG4gIHRoaXMueCA9IHggPT0gbnVsbCA/IDAuNSA6IHg7XG4gIHRoaXMueSA9IHkgPT0gbnVsbCA/IDAuNSA6IHk7XG4gIHRoaXMuciA9IHIgPT0gbnVsbCA/IDAuNSA6IHI7IC8vIENhbiBiZSBjbG9uZWRcblxuICB0aGlzLnR5cGUgPSAncmFkaWFsJzsgLy8gSWYgdXNlIGdsb2JhbCBjb29yZFxuXG4gIHRoaXMuZ2xvYmFsID0gZ2xvYmFsQ29vcmQgfHwgZmFsc2U7XG4gIEdyYWRpZW50LmNhbGwodGhpcywgY29sb3JTdG9wcyk7XG59O1xuXG5SYWRpYWxHcmFkaWVudC5wcm90b3R5cGUgPSB7XG4gIGNvbnN0cnVjdG9yOiBSYWRpYWxHcmFkaWVudFxufTtcbnpyVXRpbC5pbmhlcml0cyhSYWRpYWxHcmFkaWVudCwgR3JhZGllbnQpO1xudmFyIF9kZWZhdWx0ID0gUmFkaWFsR3JhZGllbnQ7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/RadialGradient.js
