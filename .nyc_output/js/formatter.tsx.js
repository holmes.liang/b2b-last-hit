__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatPercent", function() { return formatPercent; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);


function formatInThousandSeparator(str) {
  if (str === null || str === undefined) return "";
  if (["$", "S", "฿"].includes(str)) return "";
  return "".concat(str).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function parseCurrency(str, currencySymbol) {
  if (["$", "S", "฿"].includes(str)) return "";
  return str.replace(eval("/\\" + currencySymbol + "\\s?|(,*)/g"), "");
} // ByPrefix


var formatterParser = {
  formatter: function formatter(value) {
    if (value == "S") return "";
    if (value == "$") return "";
    return "S$ ".concat(value).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  parser: function parser(value) {
    if (value == "S") return "";
    if (value == "$") return "";
    return value.replace(/\S\$\s?|(,*)/g, "");
  }
};
var formatterPercent = {
  formatter: function formatter(value) {
    return "".concat(value, "%");
  },
  parser: function parser(value) {
    return value && value.replace("%", "");
  }
};
var formatPercent = function formatPercent(value) {
  var mul = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 100;
  if (!value) return;

  var percent = lodash__WEBPACK_IMPORTED_MODULE_0___default.a.toNumber(value * mul).toFixed(2);

  return "".concat(percent, "%");
};
/* harmony default export */ __webpack_exports__["default"] = ({
  formatInThousandSeparator: formatInThousandSeparator,
  parseCurrency: parseCurrency,
  formatterParser: formatterParser,
  formatterPercent: formatterPercent
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL2Zvcm1hdHRlci50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21tb24vZm9ybWF0dGVyLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBOTnVtYmVyIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgUnVsZXMgfSBmcm9tIFwiQGNvbW1vbi9pbmRleFwiO1xuXG5mdW5jdGlvbiBmb3JtYXRJblRob3VzYW5kU2VwYXJhdG9yKHN0cjogc3RyaW5nKSB7XG4gIGlmIChzdHIgPT09IG51bGwgfHwgc3RyID09PSB1bmRlZmluZWQpIHJldHVybiBcIlwiO1xuICBpZiAoW1wiJFwiLCBcIlNcIiwgXCLguL9cIl0uaW5jbHVkZXMoc3RyKSkgcmV0dXJuIFwiXCI7XG4gIHJldHVybiBgJHtzdHJ9YC5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCBcIixcIik7XG59XG5cbmZ1bmN0aW9uIHBhcnNlQ3VycmVuY3koc3RyOiBzdHJpbmcsIGN1cnJlbmN5U3ltYm9sOiBzdHJpbmcpIHtcbiAgaWYgKFtcIiRcIiwgXCJTXCIsIFwi4Li/XCJdLmluY2x1ZGVzKHN0cikpIHJldHVybiBcIlwiO1xuICByZXR1cm4gc3RyLnJlcGxhY2UoZXZhbChcIi9cXFxcXCIgKyBjdXJyZW5jeVN5bWJvbCArIFwiXFxcXHM/fCgsKikvZ1wiKSwgXCJcIik7XG59XG5cbi8vIEJ5UHJlZml4XG5jb25zdCBmb3JtYXR0ZXJQYXJzZXIgPSB7XG4gIGZvcm1hdHRlcjogKHZhbHVlOiBhbnkpID0+IHtcbiAgICBpZiAodmFsdWUgPT0gXCJTXCIpIHJldHVybiBcIlwiO1xuICAgIGlmICh2YWx1ZSA9PSBcIiRcIikgcmV0dXJuIFwiXCI7XG4gICAgcmV0dXJuIGBTJCAke3ZhbHVlfWAucmVwbGFjZSgvXFxCKD89KFxcZHszfSkrKD8hXFxkKSkvZywgXCIsXCIpO1xuICB9LFxuICBwYXJzZXI6ICh2YWx1ZTogYW55KSA9PiB7XG4gICAgaWYgKHZhbHVlID09IFwiU1wiKSByZXR1cm4gXCJcIjtcbiAgICBpZiAodmFsdWUgPT0gXCIkXCIpIHJldHVybiBcIlwiO1xuICAgIHJldHVybiB2YWx1ZS5yZXBsYWNlKC9cXFNcXCRcXHM/fCgsKikvZywgXCJcIik7XG4gIH0sXG59O1xuXG5jb25zdCBmb3JtYXR0ZXJQZXJjZW50ID0ge1xuICBmb3JtYXR0ZXI6ICh2YWx1ZTogYW55KSA9PiB7XG4gICAgcmV0dXJuIGAke3ZhbHVlfSVgO1xuICB9LFxuICBwYXJzZXI6ICh2YWx1ZTogYW55KSA9PiB7XG4gICAgcmV0dXJuIHZhbHVlICYmIHZhbHVlLnJlcGxhY2UoXCIlXCIsIFwiXCIpO1xuICB9LFxufTtcblxuZXhwb3J0IGNvbnN0IGZvcm1hdFBlcmNlbnQgPSAodmFsdWU6IGFueSwgbXVsOiBudW1iZXIgPSAxMDApID0+IHtcbiAgaWYgKCF2YWx1ZSkgcmV0dXJuO1xuICBjb25zdCBwZXJjZW50ID0gXy50b051bWJlcih2YWx1ZSAqIG11bCkudG9GaXhlZCgyKTtcbiAgcmV0dXJuIGAke3BlcmNlbnR9JWA7XG59O1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgZm9ybWF0SW5UaG91c2FuZFNlcGFyYXRvcixcbiAgcGFyc2VDdXJyZW5jeSxcbiAgZm9ybWF0dGVyUGFyc2VyLFxuICBmb3JtYXR0ZXJQZXJjZW50LFxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/formatter.tsx
