/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule SelectionState
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var Record = Immutable.Record;
var defaultRecord = {
  anchorKey: '',
  anchorOffset: 0,
  focusKey: '',
  focusOffset: 0,
  isBackward: false,
  hasFocus: false
};
var SelectionStateRecord = Record(defaultRecord);

var SelectionState = function (_SelectionStateRecord) {
  _inherits(SelectionState, _SelectionStateRecord);

  function SelectionState() {
    _classCallCheck(this, SelectionState);

    return _possibleConstructorReturn(this, _SelectionStateRecord.apply(this, arguments));
  }

  SelectionState.prototype.serialize = function serialize() {
    return 'Anchor: ' + this.getAnchorKey() + ':' + this.getAnchorOffset() + ', ' + 'Focus: ' + this.getFocusKey() + ':' + this.getFocusOffset() + ', ' + 'Is Backward: ' + String(this.getIsBackward()) + ', ' + 'Has Focus: ' + String(this.getHasFocus());
  };

  SelectionState.prototype.getAnchorKey = function getAnchorKey() {
    return this.get('anchorKey');
  };

  SelectionState.prototype.getAnchorOffset = function getAnchorOffset() {
    return this.get('anchorOffset');
  };

  SelectionState.prototype.getFocusKey = function getFocusKey() {
    return this.get('focusKey');
  };

  SelectionState.prototype.getFocusOffset = function getFocusOffset() {
    return this.get('focusOffset');
  };

  SelectionState.prototype.getIsBackward = function getIsBackward() {
    return this.get('isBackward');
  };

  SelectionState.prototype.getHasFocus = function getHasFocus() {
    return this.get('hasFocus');
  };
  /**
   * Return whether the specified range overlaps with an edge of the
   * SelectionState.
   */


  SelectionState.prototype.hasEdgeWithin = function hasEdgeWithin(blockKey, start, end) {
    var anchorKey = this.getAnchorKey();
    var focusKey = this.getFocusKey();

    if (anchorKey === focusKey && anchorKey === blockKey) {
      var selectionStart = this.getStartOffset();
      var selectionEnd = this.getEndOffset();
      return start <= selectionEnd && selectionStart <= end;
    }

    if (blockKey !== anchorKey && blockKey !== focusKey) {
      return false;
    }

    var offsetToCheck = blockKey === anchorKey ? this.getAnchorOffset() : this.getFocusOffset();
    return start <= offsetToCheck && end >= offsetToCheck;
  };

  SelectionState.prototype.isCollapsed = function isCollapsed() {
    return this.getAnchorKey() === this.getFocusKey() && this.getAnchorOffset() === this.getFocusOffset();
  };

  SelectionState.prototype.getStartKey = function getStartKey() {
    return this.getIsBackward() ? this.getFocusKey() : this.getAnchorKey();
  };

  SelectionState.prototype.getStartOffset = function getStartOffset() {
    return this.getIsBackward() ? this.getFocusOffset() : this.getAnchorOffset();
  };

  SelectionState.prototype.getEndKey = function getEndKey() {
    return this.getIsBackward() ? this.getAnchorKey() : this.getFocusKey();
  };

  SelectionState.prototype.getEndOffset = function getEndOffset() {
    return this.getIsBackward() ? this.getAnchorOffset() : this.getFocusOffset();
  };

  SelectionState.createEmpty = function createEmpty(key) {
    return new SelectionState({
      anchorKey: key,
      anchorOffset: 0,
      focusKey: key,
      focusOffset: 0,
      isBackward: false,
      hasFocus: false
    });
  };

  return SelectionState;
}(SelectionStateRecord);

module.exports = SelectionState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL1NlbGVjdGlvblN0YXRlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL1NlbGVjdGlvblN0YXRlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgU2VsZWN0aW9uU3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgSW1tdXRhYmxlID0gcmVxdWlyZSgnaW1tdXRhYmxlJyk7XG5cbnZhciBSZWNvcmQgPSBJbW11dGFibGUuUmVjb3JkO1xuXG5cbnZhciBkZWZhdWx0UmVjb3JkID0ge1xuICBhbmNob3JLZXk6ICcnLFxuICBhbmNob3JPZmZzZXQ6IDAsXG4gIGZvY3VzS2V5OiAnJyxcbiAgZm9jdXNPZmZzZXQ6IDAsXG4gIGlzQmFja3dhcmQ6IGZhbHNlLFxuICBoYXNGb2N1czogZmFsc2Vcbn07XG5cbnZhciBTZWxlY3Rpb25TdGF0ZVJlY29yZCA9IFJlY29yZChkZWZhdWx0UmVjb3JkKTtcblxudmFyIFNlbGVjdGlvblN0YXRlID0gZnVuY3Rpb24gKF9TZWxlY3Rpb25TdGF0ZVJlY29yZCkge1xuICBfaW5oZXJpdHMoU2VsZWN0aW9uU3RhdGUsIF9TZWxlY3Rpb25TdGF0ZVJlY29yZCk7XG5cbiAgZnVuY3Rpb24gU2VsZWN0aW9uU3RhdGUoKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNlbGVjdGlvblN0YXRlKTtcblxuICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfU2VsZWN0aW9uU3RhdGVSZWNvcmQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICBTZWxlY3Rpb25TdGF0ZS5wcm90b3R5cGUuc2VyaWFsaXplID0gZnVuY3Rpb24gc2VyaWFsaXplKCkge1xuICAgIHJldHVybiAnQW5jaG9yOiAnICsgdGhpcy5nZXRBbmNob3JLZXkoKSArICc6JyArIHRoaXMuZ2V0QW5jaG9yT2Zmc2V0KCkgKyAnLCAnICsgJ0ZvY3VzOiAnICsgdGhpcy5nZXRGb2N1c0tleSgpICsgJzonICsgdGhpcy5nZXRGb2N1c09mZnNldCgpICsgJywgJyArICdJcyBCYWNrd2FyZDogJyArIFN0cmluZyh0aGlzLmdldElzQmFja3dhcmQoKSkgKyAnLCAnICsgJ0hhcyBGb2N1czogJyArIFN0cmluZyh0aGlzLmdldEhhc0ZvY3VzKCkpO1xuICB9O1xuXG4gIFNlbGVjdGlvblN0YXRlLnByb3RvdHlwZS5nZXRBbmNob3JLZXkgPSBmdW5jdGlvbiBnZXRBbmNob3JLZXkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdhbmNob3JLZXknKTtcbiAgfTtcblxuICBTZWxlY3Rpb25TdGF0ZS5wcm90b3R5cGUuZ2V0QW5jaG9yT2Zmc2V0ID0gZnVuY3Rpb24gZ2V0QW5jaG9yT2Zmc2V0KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnYW5jaG9yT2Zmc2V0Jyk7XG4gIH07XG5cbiAgU2VsZWN0aW9uU3RhdGUucHJvdG90eXBlLmdldEZvY3VzS2V5ID0gZnVuY3Rpb24gZ2V0Rm9jdXNLZXkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdmb2N1c0tleScpO1xuICB9O1xuXG4gIFNlbGVjdGlvblN0YXRlLnByb3RvdHlwZS5nZXRGb2N1c09mZnNldCA9IGZ1bmN0aW9uIGdldEZvY3VzT2Zmc2V0KCkge1xuICAgIHJldHVybiB0aGlzLmdldCgnZm9jdXNPZmZzZXQnKTtcbiAgfTtcblxuICBTZWxlY3Rpb25TdGF0ZS5wcm90b3R5cGUuZ2V0SXNCYWNrd2FyZCA9IGZ1bmN0aW9uIGdldElzQmFja3dhcmQoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdpc0JhY2t3YXJkJyk7XG4gIH07XG5cbiAgU2VsZWN0aW9uU3RhdGUucHJvdG90eXBlLmdldEhhc0ZvY3VzID0gZnVuY3Rpb24gZ2V0SGFzRm9jdXMoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdoYXNGb2N1cycpO1xuICB9O1xuXG4gIC8qKlxuICAgKiBSZXR1cm4gd2hldGhlciB0aGUgc3BlY2lmaWVkIHJhbmdlIG92ZXJsYXBzIHdpdGggYW4gZWRnZSBvZiB0aGVcbiAgICogU2VsZWN0aW9uU3RhdGUuXG4gICAqL1xuXG5cbiAgU2VsZWN0aW9uU3RhdGUucHJvdG90eXBlLmhhc0VkZ2VXaXRoaW4gPSBmdW5jdGlvbiBoYXNFZGdlV2l0aGluKGJsb2NrS2V5LCBzdGFydCwgZW5kKSB7XG4gICAgdmFyIGFuY2hvcktleSA9IHRoaXMuZ2V0QW5jaG9yS2V5KCk7XG4gICAgdmFyIGZvY3VzS2V5ID0gdGhpcy5nZXRGb2N1c0tleSgpO1xuXG4gICAgaWYgKGFuY2hvcktleSA9PT0gZm9jdXNLZXkgJiYgYW5jaG9yS2V5ID09PSBibG9ja0tleSkge1xuICAgICAgdmFyIHNlbGVjdGlvblN0YXJ0ID0gdGhpcy5nZXRTdGFydE9mZnNldCgpO1xuICAgICAgdmFyIHNlbGVjdGlvbkVuZCA9IHRoaXMuZ2V0RW5kT2Zmc2V0KCk7XG4gICAgICByZXR1cm4gc3RhcnQgPD0gc2VsZWN0aW9uRW5kICYmIHNlbGVjdGlvblN0YXJ0IDw9IGVuZDtcbiAgICB9XG5cbiAgICBpZiAoYmxvY2tLZXkgIT09IGFuY2hvcktleSAmJiBibG9ja0tleSAhPT0gZm9jdXNLZXkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgb2Zmc2V0VG9DaGVjayA9IGJsb2NrS2V5ID09PSBhbmNob3JLZXkgPyB0aGlzLmdldEFuY2hvck9mZnNldCgpIDogdGhpcy5nZXRGb2N1c09mZnNldCgpO1xuXG4gICAgcmV0dXJuIHN0YXJ0IDw9IG9mZnNldFRvQ2hlY2sgJiYgZW5kID49IG9mZnNldFRvQ2hlY2s7XG4gIH07XG5cbiAgU2VsZWN0aW9uU3RhdGUucHJvdG90eXBlLmlzQ29sbGFwc2VkID0gZnVuY3Rpb24gaXNDb2xsYXBzZWQoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QW5jaG9yS2V5KCkgPT09IHRoaXMuZ2V0Rm9jdXNLZXkoKSAmJiB0aGlzLmdldEFuY2hvck9mZnNldCgpID09PSB0aGlzLmdldEZvY3VzT2Zmc2V0KCk7XG4gIH07XG5cbiAgU2VsZWN0aW9uU3RhdGUucHJvdG90eXBlLmdldFN0YXJ0S2V5ID0gZnVuY3Rpb24gZ2V0U3RhcnRLZXkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0SXNCYWNrd2FyZCgpID8gdGhpcy5nZXRGb2N1c0tleSgpIDogdGhpcy5nZXRBbmNob3JLZXkoKTtcbiAgfTtcblxuICBTZWxlY3Rpb25TdGF0ZS5wcm90b3R5cGUuZ2V0U3RhcnRPZmZzZXQgPSBmdW5jdGlvbiBnZXRTdGFydE9mZnNldCgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXRJc0JhY2t3YXJkKCkgPyB0aGlzLmdldEZvY3VzT2Zmc2V0KCkgOiB0aGlzLmdldEFuY2hvck9mZnNldCgpO1xuICB9O1xuXG4gIFNlbGVjdGlvblN0YXRlLnByb3RvdHlwZS5nZXRFbmRLZXkgPSBmdW5jdGlvbiBnZXRFbmRLZXkoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0SXNCYWNrd2FyZCgpID8gdGhpcy5nZXRBbmNob3JLZXkoKSA6IHRoaXMuZ2V0Rm9jdXNLZXkoKTtcbiAgfTtcblxuICBTZWxlY3Rpb25TdGF0ZS5wcm90b3R5cGUuZ2V0RW5kT2Zmc2V0ID0gZnVuY3Rpb24gZ2V0RW5kT2Zmc2V0KCkge1xuICAgIHJldHVybiB0aGlzLmdldElzQmFja3dhcmQoKSA/IHRoaXMuZ2V0QW5jaG9yT2Zmc2V0KCkgOiB0aGlzLmdldEZvY3VzT2Zmc2V0KCk7XG4gIH07XG5cbiAgU2VsZWN0aW9uU3RhdGUuY3JlYXRlRW1wdHkgPSBmdW5jdGlvbiBjcmVhdGVFbXB0eShrZXkpIHtcbiAgICByZXR1cm4gbmV3IFNlbGVjdGlvblN0YXRlKHtcbiAgICAgIGFuY2hvcktleToga2V5LFxuICAgICAgYW5jaG9yT2Zmc2V0OiAwLFxuICAgICAgZm9jdXNLZXk6IGtleSxcbiAgICAgIGZvY3VzT2Zmc2V0OiAwLFxuICAgICAgaXNCYWNrd2FyZDogZmFsc2UsXG4gICAgICBoYXNGb2N1czogZmFsc2VcbiAgICB9KTtcbiAgfTtcblxuICByZXR1cm4gU2VsZWN0aW9uU3RhdGU7XG59KFNlbGVjdGlvblN0YXRlUmVjb3JkKTtcblxubW9kdWxlLmV4cG9ydHMgPSBTZWxlY3Rpb25TdGF0ZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/SelectionState.js
