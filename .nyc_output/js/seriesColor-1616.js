/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var Gradient = __webpack_require__(/*! zrender/lib/graphic/Gradient */ "./node_modules/zrender/lib/graphic/Gradient.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var _default = {
  createOnAllSeries: true,
  performRawSeries: true,
  reset: function reset(seriesModel, ecModel) {
    var data = seriesModel.getData();
    var colorAccessPath = (seriesModel.visualColorAccessPath || 'itemStyle.color').split('.');
    var color = seriesModel.get(colorAccessPath) // Set in itemStyle
    || seriesModel.getColorFromPalette( // TODO series count changed.
    seriesModel.name, null, ecModel.getSeriesCount()); // Default color
    // FIXME Set color function or use the platte color

    data.setVisual('color', color); // Only visible series has each data be visual encoded

    if (!ecModel.isSeriesFiltered(seriesModel)) {
      if (typeof color === 'function' && !(color instanceof Gradient)) {
        data.each(function (idx) {
          data.setItemVisual(idx, 'color', color(seriesModel.getDataParams(idx)));
        });
      } // itemStyle in each data item


      var dataEach = function dataEach(data, idx) {
        var itemModel = data.getItemModel(idx);
        var color = itemModel.get(colorAccessPath, true);

        if (color != null) {
          data.setItemVisual(idx, 'color', color);
        }
      };

      return {
        dataEach: data.hasItemOption ? dataEach : null
      };
    }
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlzdWFsL3Nlcmllc0NvbG9yLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdmlzdWFsL3Nlcmllc0NvbG9yLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgR3JhZGllbnQgPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9HcmFkaWVudFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIF9kZWZhdWx0ID0ge1xuICBjcmVhdGVPbkFsbFNlcmllczogdHJ1ZSxcbiAgcGVyZm9ybVJhd1NlcmllczogdHJ1ZSxcbiAgcmVzZXQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCkge1xuICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgIHZhciBjb2xvckFjY2Vzc1BhdGggPSAoc2VyaWVzTW9kZWwudmlzdWFsQ29sb3JBY2Nlc3NQYXRoIHx8ICdpdGVtU3R5bGUuY29sb3InKS5zcGxpdCgnLicpO1xuICAgIHZhciBjb2xvciA9IHNlcmllc01vZGVsLmdldChjb2xvckFjY2Vzc1BhdGgpIC8vIFNldCBpbiBpdGVtU3R5bGVcbiAgICB8fCBzZXJpZXNNb2RlbC5nZXRDb2xvckZyb21QYWxldHRlKCAvLyBUT0RPIHNlcmllcyBjb3VudCBjaGFuZ2VkLlxuICAgIHNlcmllc01vZGVsLm5hbWUsIG51bGwsIGVjTW9kZWwuZ2V0U2VyaWVzQ291bnQoKSk7IC8vIERlZmF1bHQgY29sb3JcbiAgICAvLyBGSVhNRSBTZXQgY29sb3IgZnVuY3Rpb24gb3IgdXNlIHRoZSBwbGF0dGUgY29sb3JcblxuICAgIGRhdGEuc2V0VmlzdWFsKCdjb2xvcicsIGNvbG9yKTsgLy8gT25seSB2aXNpYmxlIHNlcmllcyBoYXMgZWFjaCBkYXRhIGJlIHZpc3VhbCBlbmNvZGVkXG5cbiAgICBpZiAoIWVjTW9kZWwuaXNTZXJpZXNGaWx0ZXJlZChzZXJpZXNNb2RlbCkpIHtcbiAgICAgIGlmICh0eXBlb2YgY29sb3IgPT09ICdmdW5jdGlvbicgJiYgIShjb2xvciBpbnN0YW5jZW9mIEdyYWRpZW50KSkge1xuICAgICAgICBkYXRhLmVhY2goZnVuY3Rpb24gKGlkeCkge1xuICAgICAgICAgIGRhdGEuc2V0SXRlbVZpc3VhbChpZHgsICdjb2xvcicsIGNvbG9yKHNlcmllc01vZGVsLmdldERhdGFQYXJhbXMoaWR4KSkpO1xuICAgICAgICB9KTtcbiAgICAgIH0gLy8gaXRlbVN0eWxlIGluIGVhY2ggZGF0YSBpdGVtXG5cblxuICAgICAgdmFyIGRhdGFFYWNoID0gZnVuY3Rpb24gKGRhdGEsIGlkeCkge1xuICAgICAgICB2YXIgaXRlbU1vZGVsID0gZGF0YS5nZXRJdGVtTW9kZWwoaWR4KTtcbiAgICAgICAgdmFyIGNvbG9yID0gaXRlbU1vZGVsLmdldChjb2xvckFjY2Vzc1BhdGgsIHRydWUpO1xuXG4gICAgICAgIGlmIChjb2xvciAhPSBudWxsKSB7XG4gICAgICAgICAgZGF0YS5zZXRJdGVtVmlzdWFsKGlkeCwgJ2NvbG9yJywgY29sb3IpO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBkYXRhRWFjaDogZGF0YS5oYXNJdGVtT3B0aW9uID8gZGF0YUVhY2ggOiBudWxsXG4gICAgICB9O1xuICAgIH1cbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFsQ0E7QUFvQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/visual/seriesColor.js
