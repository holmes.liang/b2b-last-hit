

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

var defaultProps = {
  accessibility: true,
  adaptiveHeight: false,
  afterChange: null,
  appendDots: function appendDots(dots) {
    return _react["default"].createElement("ul", {
      style: {
        display: "block"
      }
    }, dots);
  },
  arrows: true,
  autoplay: false,
  autoplaySpeed: 3000,
  beforeChange: null,
  centerMode: false,
  centerPadding: "50px",
  className: "",
  cssEase: "ease",
  customPaging: function customPaging(i) {
    return _react["default"].createElement("button", null, i + 1);
  },
  dots: false,
  dotsClass: "slick-dots",
  draggable: true,
  easing: "linear",
  edgeFriction: 0.35,
  fade: false,
  focusOnSelect: false,
  infinite: true,
  initialSlide: 0,
  lazyLoad: null,
  nextArrow: null,
  onEdge: null,
  onInit: null,
  onLazyLoadError: null,
  onReInit: null,
  pauseOnDotsHover: false,
  pauseOnFocus: false,
  pauseOnHover: true,
  prevArrow: null,
  responsive: null,
  rows: 1,
  rtl: false,
  slide: "div",
  slidesPerRow: 1,
  slidesToScroll: 1,
  slidesToShow: 1,
  speed: 500,
  swipe: true,
  swipeEvent: null,
  swipeToSlide: false,
  touchMove: true,
  touchThreshold: 5,
  useCSS: true,
  useTransform: true,
  variableWidth: false,
  vertical: false,
  waitForAnimate: true
};
var _default = defaultProps;
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2xpY2svbGliL2RlZmF1bHQtcHJvcHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1zbGljay9saWIvZGVmYXVsdC1wcm9wcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gdm9pZCAwO1xuXG52YXIgX3JlYWN0ID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChyZXF1aXJlKFwicmVhY3RcIikpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBcImRlZmF1bHRcIjogb2JqIH07IH1cblxudmFyIGRlZmF1bHRQcm9wcyA9IHtcbiAgYWNjZXNzaWJpbGl0eTogdHJ1ZSxcbiAgYWRhcHRpdmVIZWlnaHQ6IGZhbHNlLFxuICBhZnRlckNoYW5nZTogbnVsbCxcbiAgYXBwZW5kRG90czogZnVuY3Rpb24gYXBwZW5kRG90cyhkb3RzKSB7XG4gICAgcmV0dXJuIF9yZWFjdFtcImRlZmF1bHRcIl0uY3JlYXRlRWxlbWVudChcInVsXCIsIHtcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIGRpc3BsYXk6IFwiYmxvY2tcIlxuICAgICAgfVxuICAgIH0sIGRvdHMpO1xuICB9LFxuICBhcnJvd3M6IHRydWUsXG4gIGF1dG9wbGF5OiBmYWxzZSxcbiAgYXV0b3BsYXlTcGVlZDogMzAwMCxcbiAgYmVmb3JlQ2hhbmdlOiBudWxsLFxuICBjZW50ZXJNb2RlOiBmYWxzZSxcbiAgY2VudGVyUGFkZGluZzogXCI1MHB4XCIsXG4gIGNsYXNzTmFtZTogXCJcIixcbiAgY3NzRWFzZTogXCJlYXNlXCIsXG4gIGN1c3RvbVBhZ2luZzogZnVuY3Rpb24gY3VzdG9tUGFnaW5nKGkpIHtcbiAgICByZXR1cm4gX3JlYWN0W1wiZGVmYXVsdFwiXS5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIsIG51bGwsIGkgKyAxKTtcbiAgfSxcbiAgZG90czogZmFsc2UsXG4gIGRvdHNDbGFzczogXCJzbGljay1kb3RzXCIsXG4gIGRyYWdnYWJsZTogdHJ1ZSxcbiAgZWFzaW5nOiBcImxpbmVhclwiLFxuICBlZGdlRnJpY3Rpb246IDAuMzUsXG4gIGZhZGU6IGZhbHNlLFxuICBmb2N1c09uU2VsZWN0OiBmYWxzZSxcbiAgaW5maW5pdGU6IHRydWUsXG4gIGluaXRpYWxTbGlkZTogMCxcbiAgbGF6eUxvYWQ6IG51bGwsXG4gIG5leHRBcnJvdzogbnVsbCxcbiAgb25FZGdlOiBudWxsLFxuICBvbkluaXQ6IG51bGwsXG4gIG9uTGF6eUxvYWRFcnJvcjogbnVsbCxcbiAgb25SZUluaXQ6IG51bGwsXG4gIHBhdXNlT25Eb3RzSG92ZXI6IGZhbHNlLFxuICBwYXVzZU9uRm9jdXM6IGZhbHNlLFxuICBwYXVzZU9uSG92ZXI6IHRydWUsXG4gIHByZXZBcnJvdzogbnVsbCxcbiAgcmVzcG9uc2l2ZTogbnVsbCxcbiAgcm93czogMSxcbiAgcnRsOiBmYWxzZSxcbiAgc2xpZGU6IFwiZGl2XCIsXG4gIHNsaWRlc1BlclJvdzogMSxcbiAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gIHNsaWRlc1RvU2hvdzogMSxcbiAgc3BlZWQ6IDUwMCxcbiAgc3dpcGU6IHRydWUsXG4gIHN3aXBlRXZlbnQ6IG51bGwsXG4gIHN3aXBlVG9TbGlkZTogZmFsc2UsXG4gIHRvdWNoTW92ZTogdHJ1ZSxcbiAgdG91Y2hUaHJlc2hvbGQ6IDUsXG4gIHVzZUNTUzogdHJ1ZSxcbiAgdXNlVHJhbnNmb3JtOiB0cnVlLFxuICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgdmVydGljYWw6IGZhbHNlLFxuICB3YWl0Rm9yQW5pbWF0ZTogdHJ1ZVxufTtcbnZhciBfZGVmYXVsdCA9IGRlZmF1bHRQcm9wcztcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBMURBO0FBNERBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/react-slick/lib/default-props.js
