/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _dataProvider = __webpack_require__(/*! ../../data/helper/dataProvider */ "./node_modules/echarts/lib/data/helper/dataProvider.js");

var retrieveRawValue = _dataProvider.retrieveRawValue;

var _format = __webpack_require__(/*! ../../util/format */ "./node_modules/echarts/lib/util/format.js");

var getTooltipMarker = _format.getTooltipMarker;
var formatTpl = _format.formatTpl;

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var getTooltipRenderMode = _model.getTooltipRenderMode;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var DIMENSION_LABEL_REG = /\{@(.+?)\}/g; // PENDING A little ugly

var _default = {
  /**
   * Get params for formatter
   * @param {number} dataIndex
   * @param {string} [dataType]
   * @return {Object}
   */
  getDataParams: function getDataParams(dataIndex, dataType) {
    var data = this.getData(dataType);
    var rawValue = this.getRawValue(dataIndex, dataType);
    var rawDataIndex = data.getRawIndex(dataIndex);
    var name = data.getName(dataIndex);
    var itemOpt = data.getRawDataItem(dataIndex);
    var color = data.getItemVisual(dataIndex, 'color');
    var tooltipModel = this.ecModel.getComponent('tooltip');
    var renderModeOption = tooltipModel && tooltipModel.get('renderMode');
    var renderMode = getTooltipRenderMode(renderModeOption);
    var mainType = this.mainType;
    var isSeries = mainType === 'series';
    return {
      componentType: mainType,
      componentSubType: this.subType,
      componentIndex: this.componentIndex,
      seriesType: isSeries ? this.subType : null,
      seriesIndex: this.seriesIndex,
      seriesId: isSeries ? this.id : null,
      seriesName: isSeries ? this.name : null,
      name: name,
      dataIndex: rawDataIndex,
      data: itemOpt,
      dataType: dataType,
      value: rawValue,
      color: color,
      marker: getTooltipMarker({
        color: color,
        renderMode: renderMode
      }),
      // Param name list for mapping `a`, `b`, `c`, `d`, `e`
      $vars: ['seriesName', 'name', 'value']
    };
  },

  /**
   * Format label
   * @param {number} dataIndex
   * @param {string} [status='normal'] 'normal' or 'emphasis'
   * @param {string} [dataType]
   * @param {number} [dimIndex]
   * @param {string} [labelProp='label']
   * @return {string} If not formatter, return null/undefined
   */
  getFormattedLabel: function getFormattedLabel(dataIndex, status, dataType, dimIndex, labelProp) {
    status = status || 'normal';
    var data = this.getData(dataType);
    var itemModel = data.getItemModel(dataIndex);
    var params = this.getDataParams(dataIndex, dataType);

    if (dimIndex != null && params.value instanceof Array) {
      params.value = params.value[dimIndex];
    }

    var formatter = itemModel.get(status === 'normal' ? [labelProp || 'label', 'formatter'] : [status, labelProp || 'label', 'formatter']);

    if (typeof formatter === 'function') {
      params.status = status;
      return formatter(params);
    } else if (typeof formatter === 'string') {
      var str = formatTpl(formatter, params); // Support 'aaa{@[3]}bbb{@product}ccc'.
      // Do not support '}' in dim name util have to.

      return str.replace(DIMENSION_LABEL_REG, function (origin, dim) {
        var len = dim.length;

        if (dim.charAt(0) === '[' && dim.charAt(len - 1) === ']') {
          dim = +dim.slice(1, len - 1); // Also: '[]' => 0
        }

        return retrieveRawValue(data, dataIndex, dim);
      });
    }
  },

  /**
   * Get raw value in option
   * @param {number} idx
   * @param {string} [dataType]
   * @return {Array|number|string}
   */
  getRawValue: function getRawValue(idx, dataType) {
    return retrieveRawValue(this.getData(dataType), idx);
  },

  /**
   * Should be implemented.
   * @param {number} dataIndex
   * @param {boolean} [multipleSeries=false]
   * @param {number} [dataType]
   * @return {string} tooltip string
   */
  formatTooltip: function formatTooltip() {// Empty function
  }
};
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvbWl4aW4vZGF0YUZvcm1hdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL21vZGVsL21peGluL2RhdGFGb3JtYXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfZGF0YVByb3ZpZGVyID0gcmVxdWlyZShcIi4uLy4uL2RhdGEvaGVscGVyL2RhdGFQcm92aWRlclwiKTtcblxudmFyIHJldHJpZXZlUmF3VmFsdWUgPSBfZGF0YVByb3ZpZGVyLnJldHJpZXZlUmF3VmFsdWU7XG5cbnZhciBfZm9ybWF0ID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZm9ybWF0XCIpO1xuXG52YXIgZ2V0VG9vbHRpcE1hcmtlciA9IF9mb3JtYXQuZ2V0VG9vbHRpcE1hcmtlcjtcbnZhciBmb3JtYXRUcGwgPSBfZm9ybWF0LmZvcm1hdFRwbDtcblxudmFyIF9tb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL21vZGVsXCIpO1xuXG52YXIgZ2V0VG9vbHRpcFJlbmRlck1vZGUgPSBfbW9kZWwuZ2V0VG9vbHRpcFJlbmRlck1vZGU7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbnZhciBESU1FTlNJT05fTEFCRUxfUkVHID0gL1xce0AoLis/KVxcfS9nOyAvLyBQRU5ESU5HIEEgbGl0dGxlIHVnbHlcblxudmFyIF9kZWZhdWx0ID0ge1xuICAvKipcbiAgICogR2V0IHBhcmFtcyBmb3IgZm9ybWF0dGVyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBkYXRhSW5kZXhcbiAgICogQHBhcmFtIHtzdHJpbmd9IFtkYXRhVHlwZV1cbiAgICogQHJldHVybiB7T2JqZWN0fVxuICAgKi9cbiAgZ2V0RGF0YVBhcmFtczogZnVuY3Rpb24gKGRhdGFJbmRleCwgZGF0YVR5cGUpIHtcbiAgICB2YXIgZGF0YSA9IHRoaXMuZ2V0RGF0YShkYXRhVHlwZSk7XG4gICAgdmFyIHJhd1ZhbHVlID0gdGhpcy5nZXRSYXdWYWx1ZShkYXRhSW5kZXgsIGRhdGFUeXBlKTtcbiAgICB2YXIgcmF3RGF0YUluZGV4ID0gZGF0YS5nZXRSYXdJbmRleChkYXRhSW5kZXgpO1xuICAgIHZhciBuYW1lID0gZGF0YS5nZXROYW1lKGRhdGFJbmRleCk7XG4gICAgdmFyIGl0ZW1PcHQgPSBkYXRhLmdldFJhd0RhdGFJdGVtKGRhdGFJbmRleCk7XG4gICAgdmFyIGNvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGRhdGFJbmRleCwgJ2NvbG9yJyk7XG4gICAgdmFyIHRvb2x0aXBNb2RlbCA9IHRoaXMuZWNNb2RlbC5nZXRDb21wb25lbnQoJ3Rvb2x0aXAnKTtcbiAgICB2YXIgcmVuZGVyTW9kZU9wdGlvbiA9IHRvb2x0aXBNb2RlbCAmJiB0b29sdGlwTW9kZWwuZ2V0KCdyZW5kZXJNb2RlJyk7XG4gICAgdmFyIHJlbmRlck1vZGUgPSBnZXRUb29sdGlwUmVuZGVyTW9kZShyZW5kZXJNb2RlT3B0aW9uKTtcbiAgICB2YXIgbWFpblR5cGUgPSB0aGlzLm1haW5UeXBlO1xuICAgIHZhciBpc1NlcmllcyA9IG1haW5UeXBlID09PSAnc2VyaWVzJztcbiAgICByZXR1cm4ge1xuICAgICAgY29tcG9uZW50VHlwZTogbWFpblR5cGUsXG4gICAgICBjb21wb25lbnRTdWJUeXBlOiB0aGlzLnN1YlR5cGUsXG4gICAgICBjb21wb25lbnRJbmRleDogdGhpcy5jb21wb25lbnRJbmRleCxcbiAgICAgIHNlcmllc1R5cGU6IGlzU2VyaWVzID8gdGhpcy5zdWJUeXBlIDogbnVsbCxcbiAgICAgIHNlcmllc0luZGV4OiB0aGlzLnNlcmllc0luZGV4LFxuICAgICAgc2VyaWVzSWQ6IGlzU2VyaWVzID8gdGhpcy5pZCA6IG51bGwsXG4gICAgICBzZXJpZXNOYW1lOiBpc1NlcmllcyA/IHRoaXMubmFtZSA6IG51bGwsXG4gICAgICBuYW1lOiBuYW1lLFxuICAgICAgZGF0YUluZGV4OiByYXdEYXRhSW5kZXgsXG4gICAgICBkYXRhOiBpdGVtT3B0LFxuICAgICAgZGF0YVR5cGU6IGRhdGFUeXBlLFxuICAgICAgdmFsdWU6IHJhd1ZhbHVlLFxuICAgICAgY29sb3I6IGNvbG9yLFxuICAgICAgbWFya2VyOiBnZXRUb29sdGlwTWFya2VyKHtcbiAgICAgICAgY29sb3I6IGNvbG9yLFxuICAgICAgICByZW5kZXJNb2RlOiByZW5kZXJNb2RlXG4gICAgICB9KSxcbiAgICAgIC8vIFBhcmFtIG5hbWUgbGlzdCBmb3IgbWFwcGluZyBgYWAsIGBiYCwgYGNgLCBgZGAsIGBlYFxuICAgICAgJHZhcnM6IFsnc2VyaWVzTmFtZScsICduYW1lJywgJ3ZhbHVlJ11cbiAgICB9O1xuICB9LFxuXG4gIC8qKlxuICAgKiBGb3JtYXQgbGFiZWxcbiAgICogQHBhcmFtIHtudW1iZXJ9IGRhdGFJbmRleFxuICAgKiBAcGFyYW0ge3N0cmluZ30gW3N0YXR1cz0nbm9ybWFsJ10gJ25vcm1hbCcgb3IgJ2VtcGhhc2lzJ1xuICAgKiBAcGFyYW0ge3N0cmluZ30gW2RhdGFUeXBlXVxuICAgKiBAcGFyYW0ge251bWJlcn0gW2RpbUluZGV4XVxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2xhYmVsUHJvcD0nbGFiZWwnXVxuICAgKiBAcmV0dXJuIHtzdHJpbmd9IElmIG5vdCBmb3JtYXR0ZXIsIHJldHVybiBudWxsL3VuZGVmaW5lZFxuICAgKi9cbiAgZ2V0Rm9ybWF0dGVkTGFiZWw6IGZ1bmN0aW9uIChkYXRhSW5kZXgsIHN0YXR1cywgZGF0YVR5cGUsIGRpbUluZGV4LCBsYWJlbFByb3ApIHtcbiAgICBzdGF0dXMgPSBzdGF0dXMgfHwgJ25vcm1hbCc7XG4gICAgdmFyIGRhdGEgPSB0aGlzLmdldERhdGEoZGF0YVR5cGUpO1xuICAgIHZhciBpdGVtTW9kZWwgPSBkYXRhLmdldEl0ZW1Nb2RlbChkYXRhSW5kZXgpO1xuICAgIHZhciBwYXJhbXMgPSB0aGlzLmdldERhdGFQYXJhbXMoZGF0YUluZGV4LCBkYXRhVHlwZSk7XG5cbiAgICBpZiAoZGltSW5kZXggIT0gbnVsbCAmJiBwYXJhbXMudmFsdWUgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgcGFyYW1zLnZhbHVlID0gcGFyYW1zLnZhbHVlW2RpbUluZGV4XTtcbiAgICB9XG5cbiAgICB2YXIgZm9ybWF0dGVyID0gaXRlbU1vZGVsLmdldChzdGF0dXMgPT09ICdub3JtYWwnID8gW2xhYmVsUHJvcCB8fCAnbGFiZWwnLCAnZm9ybWF0dGVyJ10gOiBbc3RhdHVzLCBsYWJlbFByb3AgfHwgJ2xhYmVsJywgJ2Zvcm1hdHRlciddKTtcblxuICAgIGlmICh0eXBlb2YgZm9ybWF0dGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBwYXJhbXMuc3RhdHVzID0gc3RhdHVzO1xuICAgICAgcmV0dXJuIGZvcm1hdHRlcihwYXJhbXMpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGZvcm1hdHRlciA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHZhciBzdHIgPSBmb3JtYXRUcGwoZm9ybWF0dGVyLCBwYXJhbXMpOyAvLyBTdXBwb3J0ICdhYWF7QFszXX1iYmJ7QHByb2R1Y3R9Y2NjJy5cbiAgICAgIC8vIERvIG5vdCBzdXBwb3J0ICd9JyBpbiBkaW0gbmFtZSB1dGlsIGhhdmUgdG8uXG5cbiAgICAgIHJldHVybiBzdHIucmVwbGFjZShESU1FTlNJT05fTEFCRUxfUkVHLCBmdW5jdGlvbiAob3JpZ2luLCBkaW0pIHtcbiAgICAgICAgdmFyIGxlbiA9IGRpbS5sZW5ndGg7XG5cbiAgICAgICAgaWYgKGRpbS5jaGFyQXQoMCkgPT09ICdbJyAmJiBkaW0uY2hhckF0KGxlbiAtIDEpID09PSAnXScpIHtcbiAgICAgICAgICBkaW0gPSArZGltLnNsaWNlKDEsIGxlbiAtIDEpOyAvLyBBbHNvOiAnW10nID0+IDBcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXRyaWV2ZVJhd1ZhbHVlKGRhdGEsIGRhdGFJbmRleCwgZGltKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogR2V0IHJhdyB2YWx1ZSBpbiBvcHRpb25cbiAgICogQHBhcmFtIHtudW1iZXJ9IGlkeFxuICAgKiBAcGFyYW0ge3N0cmluZ30gW2RhdGFUeXBlXVxuICAgKiBAcmV0dXJuIHtBcnJheXxudW1iZXJ8c3RyaW5nfVxuICAgKi9cbiAgZ2V0UmF3VmFsdWU6IGZ1bmN0aW9uIChpZHgsIGRhdGFUeXBlKSB7XG4gICAgcmV0dXJuIHJldHJpZXZlUmF3VmFsdWUodGhpcy5nZXREYXRhKGRhdGFUeXBlKSwgaWR4KTtcbiAgfSxcblxuICAvKipcbiAgICogU2hvdWxkIGJlIGltcGxlbWVudGVkLlxuICAgKiBAcGFyYW0ge251bWJlcn0gZGF0YUluZGV4XG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW211bHRpcGxlU2VyaWVzPWZhbHNlXVxuICAgKiBAcGFyYW0ge251bWJlcn0gW2RhdGFUeXBlXVxuICAgKiBAcmV0dXJuIHtzdHJpbmd9IHRvb2x0aXAgc3RyaW5nXG4gICAqL1xuICBmb3JtYXRUb29sdGlwOiBmdW5jdGlvbiAoKSB7Ly8gRW1wdHkgZnVuY3Rpb25cbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBbkJBO0FBcUJBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFwR0E7QUFzR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/mixin/dataFormat.js
