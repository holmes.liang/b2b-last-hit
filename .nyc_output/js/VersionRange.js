/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */


var invariant = __webpack_require__(/*! ./invariant */ "./node_modules/fbjs/lib/invariant.js");

var componentRegex = /\./;
var orRegex = /\|\|/;
var rangeRegex = /\s+\-\s+/;
var modifierRegex = /^(<=|<|=|>=|~>|~|>|)?\s*(.+)/;
var numericRegex = /^(\d*)(.*)/;
/**
 * Splits input `range` on "||" and returns true if any subrange matches
 * `version`.
 *
 * @param {string} range
 * @param {string} version
 * @returns {boolean}
 */

function checkOrExpression(range, version) {
  var expressions = range.split(orRegex);

  if (expressions.length > 1) {
    return expressions.some(function (range) {
      return VersionRange.contains(range, version);
    });
  } else {
    range = expressions[0].trim();
    return checkRangeExpression(range, version);
  }
}
/**
 * Splits input `range` on " - " (the surrounding whitespace is required) and
 * returns true if version falls between the two operands.
 *
 * @param {string} range
 * @param {string} version
 * @returns {boolean}
 */


function checkRangeExpression(range, version) {
  var expressions = range.split(rangeRegex);
  !(expressions.length > 0 && expressions.length <= 2) ?  true ? invariant(false, 'the "-" operator expects exactly 2 operands') : undefined : void 0;

  if (expressions.length === 1) {
    return checkSimpleExpression(expressions[0], version);
  } else {
    var startVersion = expressions[0],
        endVersion = expressions[1];
    !(isSimpleVersion(startVersion) && isSimpleVersion(endVersion)) ?  true ? invariant(false, 'operands to the "-" operator must be simple (no modifiers)') : undefined : void 0;
    return checkSimpleExpression('>=' + startVersion, version) && checkSimpleExpression('<=' + endVersion, version);
  }
}
/**
 * Checks if `range` matches `version`. `range` should be a "simple" range (ie.
 * not a compound range using the " - " or "||" operators).
 *
 * @param {string} range
 * @param {string} version
 * @returns {boolean}
 */


function checkSimpleExpression(range, version) {
  range = range.trim();

  if (range === '') {
    return true;
  }

  var versionComponents = version.split(componentRegex);

  var _getModifierAndCompon = getModifierAndComponents(range),
      modifier = _getModifierAndCompon.modifier,
      rangeComponents = _getModifierAndCompon.rangeComponents;

  switch (modifier) {
    case '<':
      return checkLessThan(versionComponents, rangeComponents);

    case '<=':
      return checkLessThanOrEqual(versionComponents, rangeComponents);

    case '>=':
      return checkGreaterThanOrEqual(versionComponents, rangeComponents);

    case '>':
      return checkGreaterThan(versionComponents, rangeComponents);

    case '~':
    case '~>':
      return checkApproximateVersion(versionComponents, rangeComponents);

    default:
      return checkEqual(versionComponents, rangeComponents);
  }
}
/**
 * Checks whether `a` is less than `b`.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkLessThan(a, b) {
  return compareComponents(a, b) === -1;
}
/**
 * Checks whether `a` is less than or equal to `b`.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkLessThanOrEqual(a, b) {
  var result = compareComponents(a, b);
  return result === -1 || result === 0;
}
/**
 * Checks whether `a` is equal to `b`.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkEqual(a, b) {
  return compareComponents(a, b) === 0;
}
/**
 * Checks whether `a` is greater than or equal to `b`.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkGreaterThanOrEqual(a, b) {
  var result = compareComponents(a, b);
  return result === 1 || result === 0;
}
/**
 * Checks whether `a` is greater than `b`.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkGreaterThan(a, b) {
  return compareComponents(a, b) === 1;
}
/**
 * Checks whether `a` is "reasonably close" to `b` (as described in
 * https://www.npmjs.org/doc/misc/semver.html). For example, if `b` is "1.3.1"
 * then "reasonably close" is defined as ">= 1.3.1 and < 1.4".
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {boolean}
 */


function checkApproximateVersion(a, b) {
  var lowerBound = b.slice();
  var upperBound = b.slice();

  if (upperBound.length > 1) {
    upperBound.pop();
  }

  var lastIndex = upperBound.length - 1;
  var numeric = parseInt(upperBound[lastIndex], 10);

  if (isNumber(numeric)) {
    upperBound[lastIndex] = numeric + 1 + '';
  }

  return checkGreaterThanOrEqual(a, lowerBound) && checkLessThan(a, upperBound);
}
/**
 * Extracts the optional modifier (<, <=, =, >=, >, ~, ~>) and version
 * components from `range`.
 *
 * For example, given `range` ">= 1.2.3" returns an object with a `modifier` of
 * `">="` and `components` of `[1, 2, 3]`.
 *
 * @param {string} range
 * @returns {object}
 */


function getModifierAndComponents(range) {
  var rangeComponents = range.split(componentRegex);
  var matches = rangeComponents[0].match(modifierRegex);
  !matches ?  true ? invariant(false, 'expected regex to match but it did not') : undefined : void 0;
  return {
    modifier: matches[1],
    rangeComponents: [matches[2]].concat(rangeComponents.slice(1))
  };
}
/**
 * Determines if `number` is a number.
 *
 * @param {mixed} number
 * @returns {boolean}
 */


function isNumber(number) {
  return !isNaN(number) && isFinite(number);
}
/**
 * Tests whether `range` is a "simple" version number without any modifiers
 * (">", "~" etc).
 *
 * @param {string} range
 * @returns {boolean}
 */


function isSimpleVersion(range) {
  return !getModifierAndComponents(range).modifier;
}
/**
 * Zero-pads array `array` until it is at least `length` long.
 *
 * @param {array} array
 * @param {number} length
 */


function zeroPad(array, length) {
  for (var i = array.length; i < length; i++) {
    array[i] = '0';
  }
}
/**
 * Normalizes `a` and `b` in preparation for comparison by doing the following:
 *
 * - zero-pads `a` and `b`
 * - marks any "x", "X" or "*" component in `b` as equivalent by zero-ing it out
 *   in both `a` and `b`
 * - marks any final "*" component in `b` as a greedy wildcard by zero-ing it
 *   and all of its successors in `a`
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {array<array<string>>}
 */


function normalizeVersions(a, b) {
  a = a.slice();
  b = b.slice();
  zeroPad(a, b.length); // mark "x" and "*" components as equal

  for (var i = 0; i < b.length; i++) {
    var matches = b[i].match(/^[x*]$/i);

    if (matches) {
      b[i] = a[i] = '0'; // final "*" greedily zeros all remaining components

      if (matches[0] === '*' && i === b.length - 1) {
        for (var j = i; j < a.length; j++) {
          a[j] = '0';
        }
      }
    }
  }

  zeroPad(b, a.length);
  return [a, b];
}
/**
 * Returns the numerical -- not the lexicographical -- ordering of `a` and `b`.
 *
 * For example, `10-alpha` is greater than `2-beta`.
 *
 * @param {string} a
 * @param {string} b
 * @returns {number} -1, 0 or 1 to indicate whether `a` is less than, equal to,
 * or greater than `b`, respectively
 */


function compareNumeric(a, b) {
  var aPrefix = a.match(numericRegex)[1];
  var bPrefix = b.match(numericRegex)[1];
  var aNumeric = parseInt(aPrefix, 10);
  var bNumeric = parseInt(bPrefix, 10);

  if (isNumber(aNumeric) && isNumber(bNumeric) && aNumeric !== bNumeric) {
    return compare(aNumeric, bNumeric);
  } else {
    return compare(a, b);
  }
}
/**
 * Returns the ordering of `a` and `b`.
 *
 * @param {string|number} a
 * @param {string|number} b
 * @returns {number} -1, 0 or 1 to indicate whether `a` is less than, equal to,
 * or greater than `b`, respectively
 */


function compare(a, b) {
  !(typeof a === typeof b) ?  true ? invariant(false, '"a" and "b" must be of the same type') : undefined : void 0;

  if (a > b) {
    return 1;
  } else if (a < b) {
    return -1;
  } else {
    return 0;
  }
}
/**
 * Compares arrays of version components.
 *
 * @param {array<string>} a
 * @param {array<string>} b
 * @returns {number} -1, 0 or 1 to indicate whether `a` is less than, equal to,
 * or greater than `b`, respectively
 */


function compareComponents(a, b) {
  var _normalizeVersions = normalizeVersions(a, b),
      aNormalized = _normalizeVersions[0],
      bNormalized = _normalizeVersions[1];

  for (var i = 0; i < bNormalized.length; i++) {
    var result = compareNumeric(aNormalized[i], bNormalized[i]);

    if (result) {
      return result;
    }
  }

  return 0;
}

var VersionRange = {
  /**
   * Checks whether `version` satisfies the `range` specification.
   *
   * We support a subset of the expressions defined in
   * https://www.npmjs.org/doc/misc/semver.html:
   *
   *    version   Must match version exactly
   *    =version  Same as just version
   *    >version  Must be greater than version
   *    >=version Must be greater than or equal to version
   *    <version  Must be less than version
   *    <=version Must be less than or equal to version
   *    ~version  Must be at least version, but less than the next significant
   *              revision above version:
   *              "~1.2.3" is equivalent to ">= 1.2.3 and < 1.3"
   *    ~>version Equivalent to ~version
   *    1.2.x     Must match "1.2.x", where "x" is a wildcard that matches
   *              anything
   *    1.2.*     Similar to "1.2.x", but "*" in the trailing position is a
   *              "greedy" wildcard, so will match any number of additional
   *              components:
   *              "1.2.*" will match "1.2.1", "1.2.1.1", "1.2.1.1.1" etc
   *    *         Any version
   *    ""        (Empty string) Same as *
   *    v1 - v2   Equivalent to ">= v1 and <= v2"
   *    r1 || r2  Passes if either r1 or r2 are satisfied
   *
   * @param {string} range
   * @param {string} version
   * @returns {boolean}
   */
  contains: function contains(range, version) {
    return checkOrExpression(range.trim(), version.trim());
  }
};
module.exports = VersionRange;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVmVyc2lvblJhbmdlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVmVyc2lvblJhbmdlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJy4vaW52YXJpYW50Jyk7XG5cbnZhciBjb21wb25lbnRSZWdleCA9IC9cXC4vO1xudmFyIG9yUmVnZXggPSAvXFx8XFx8LztcbnZhciByYW5nZVJlZ2V4ID0gL1xccytcXC1cXHMrLztcbnZhciBtb2RpZmllclJlZ2V4ID0gL14oPD18PHw9fD49fH4+fH58PnwpP1xccyooLispLztcbnZhciBudW1lcmljUmVnZXggPSAvXihcXGQqKSguKikvO1xuXG4vKipcbiAqIFNwbGl0cyBpbnB1dCBgcmFuZ2VgIG9uIFwifHxcIiBhbmQgcmV0dXJucyB0cnVlIGlmIGFueSBzdWJyYW5nZSBtYXRjaGVzXG4gKiBgdmVyc2lvbmAuXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHJhbmdlXG4gKiBAcGFyYW0ge3N0cmluZ30gdmVyc2lvblxuICogQHJldHVybnMge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIGNoZWNrT3JFeHByZXNzaW9uKHJhbmdlLCB2ZXJzaW9uKSB7XG4gIHZhciBleHByZXNzaW9ucyA9IHJhbmdlLnNwbGl0KG9yUmVnZXgpO1xuXG4gIGlmIChleHByZXNzaW9ucy5sZW5ndGggPiAxKSB7XG4gICAgcmV0dXJuIGV4cHJlc3Npb25zLnNvbWUoZnVuY3Rpb24gKHJhbmdlKSB7XG4gICAgICByZXR1cm4gVmVyc2lvblJhbmdlLmNvbnRhaW5zKHJhbmdlLCB2ZXJzaW9uKTtcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICByYW5nZSA9IGV4cHJlc3Npb25zWzBdLnRyaW0oKTtcbiAgICByZXR1cm4gY2hlY2tSYW5nZUV4cHJlc3Npb24ocmFuZ2UsIHZlcnNpb24pO1xuICB9XG59XG5cbi8qKlxuICogU3BsaXRzIGlucHV0IGByYW5nZWAgb24gXCIgLSBcIiAodGhlIHN1cnJvdW5kaW5nIHdoaXRlc3BhY2UgaXMgcmVxdWlyZWQpIGFuZFxuICogcmV0dXJucyB0cnVlIGlmIHZlcnNpb24gZmFsbHMgYmV0d2VlbiB0aGUgdHdvIG9wZXJhbmRzLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSByYW5nZVxuICogQHBhcmFtIHtzdHJpbmd9IHZlcnNpb25cbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBjaGVja1JhbmdlRXhwcmVzc2lvbihyYW5nZSwgdmVyc2lvbikge1xuICB2YXIgZXhwcmVzc2lvbnMgPSByYW5nZS5zcGxpdChyYW5nZVJlZ2V4KTtcblxuICAhKGV4cHJlc3Npb25zLmxlbmd0aCA+IDAgJiYgZXhwcmVzc2lvbnMubGVuZ3RoIDw9IDIpID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ3RoZSBcIi1cIiBvcGVyYXRvciBleHBlY3RzIGV4YWN0bHkgMiBvcGVyYW5kcycpIDogaW52YXJpYW50KGZhbHNlKSA6IHZvaWQgMDtcblxuICBpZiAoZXhwcmVzc2lvbnMubGVuZ3RoID09PSAxKSB7XG4gICAgcmV0dXJuIGNoZWNrU2ltcGxlRXhwcmVzc2lvbihleHByZXNzaW9uc1swXSwgdmVyc2lvbik7XG4gIH0gZWxzZSB7XG4gICAgdmFyIHN0YXJ0VmVyc2lvbiA9IGV4cHJlc3Npb25zWzBdLFxuICAgICAgICBlbmRWZXJzaW9uID0gZXhwcmVzc2lvbnNbMV07XG5cbiAgICAhKGlzU2ltcGxlVmVyc2lvbihzdGFydFZlcnNpb24pICYmIGlzU2ltcGxlVmVyc2lvbihlbmRWZXJzaW9uKSkgPyBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nID8gaW52YXJpYW50KGZhbHNlLCAnb3BlcmFuZHMgdG8gdGhlIFwiLVwiIG9wZXJhdG9yIG11c3QgYmUgc2ltcGxlIChubyBtb2RpZmllcnMpJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gICAgcmV0dXJuIGNoZWNrU2ltcGxlRXhwcmVzc2lvbignPj0nICsgc3RhcnRWZXJzaW9uLCB2ZXJzaW9uKSAmJiBjaGVja1NpbXBsZUV4cHJlc3Npb24oJzw9JyArIGVuZFZlcnNpb24sIHZlcnNpb24pO1xuICB9XG59XG5cbi8qKlxuICogQ2hlY2tzIGlmIGByYW5nZWAgbWF0Y2hlcyBgdmVyc2lvbmAuIGByYW5nZWAgc2hvdWxkIGJlIGEgXCJzaW1wbGVcIiByYW5nZSAoaWUuXG4gKiBub3QgYSBjb21wb3VuZCByYW5nZSB1c2luZyB0aGUgXCIgLSBcIiBvciBcInx8XCIgb3BlcmF0b3JzKS5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gcmFuZ2VcbiAqIEBwYXJhbSB7c3RyaW5nfSB2ZXJzaW9uXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gY2hlY2tTaW1wbGVFeHByZXNzaW9uKHJhbmdlLCB2ZXJzaW9uKSB7XG4gIHJhbmdlID0gcmFuZ2UudHJpbSgpO1xuICBpZiAocmFuZ2UgPT09ICcnKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICB2YXIgdmVyc2lvbkNvbXBvbmVudHMgPSB2ZXJzaW9uLnNwbGl0KGNvbXBvbmVudFJlZ2V4KTtcblxuICB2YXIgX2dldE1vZGlmaWVyQW5kQ29tcG9uID0gZ2V0TW9kaWZpZXJBbmRDb21wb25lbnRzKHJhbmdlKSxcbiAgICAgIG1vZGlmaWVyID0gX2dldE1vZGlmaWVyQW5kQ29tcG9uLm1vZGlmaWVyLFxuICAgICAgcmFuZ2VDb21wb25lbnRzID0gX2dldE1vZGlmaWVyQW5kQ29tcG9uLnJhbmdlQ29tcG9uZW50cztcblxuICBzd2l0Y2ggKG1vZGlmaWVyKSB7XG4gICAgY2FzZSAnPCc6XG4gICAgICByZXR1cm4gY2hlY2tMZXNzVGhhbih2ZXJzaW9uQ29tcG9uZW50cywgcmFuZ2VDb21wb25lbnRzKTtcbiAgICBjYXNlICc8PSc6XG4gICAgICByZXR1cm4gY2hlY2tMZXNzVGhhbk9yRXF1YWwodmVyc2lvbkNvbXBvbmVudHMsIHJhbmdlQ29tcG9uZW50cyk7XG4gICAgY2FzZSAnPj0nOlxuICAgICAgcmV0dXJuIGNoZWNrR3JlYXRlclRoYW5PckVxdWFsKHZlcnNpb25Db21wb25lbnRzLCByYW5nZUNvbXBvbmVudHMpO1xuICAgIGNhc2UgJz4nOlxuICAgICAgcmV0dXJuIGNoZWNrR3JlYXRlclRoYW4odmVyc2lvbkNvbXBvbmVudHMsIHJhbmdlQ29tcG9uZW50cyk7XG4gICAgY2FzZSAnfic6XG4gICAgY2FzZSAnfj4nOlxuICAgICAgcmV0dXJuIGNoZWNrQXBwcm94aW1hdGVWZXJzaW9uKHZlcnNpb25Db21wb25lbnRzLCByYW5nZUNvbXBvbmVudHMpO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gY2hlY2tFcXVhbCh2ZXJzaW9uQ29tcG9uZW50cywgcmFuZ2VDb21wb25lbnRzKTtcbiAgfVxufVxuXG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGBhYCBpcyBsZXNzIHRoYW4gYGJgLlxuICpcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYVxuICogQHBhcmFtIHthcnJheTxzdHJpbmc+fSBiXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gY2hlY2tMZXNzVGhhbihhLCBiKSB7XG4gIHJldHVybiBjb21wYXJlQ29tcG9uZW50cyhhLCBiKSA9PT0gLTE7XG59XG5cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgYGFgIGlzIGxlc3MgdGhhbiBvciBlcXVhbCB0byBgYmAuXG4gKlxuICogQHBhcmFtIHthcnJheTxzdHJpbmc+fSBhXG4gKiBAcGFyYW0ge2FycmF5PHN0cmluZz59IGJcbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBjaGVja0xlc3NUaGFuT3JFcXVhbChhLCBiKSB7XG4gIHZhciByZXN1bHQgPSBjb21wYXJlQ29tcG9uZW50cyhhLCBiKTtcbiAgcmV0dXJuIHJlc3VsdCA9PT0gLTEgfHwgcmVzdWx0ID09PSAwO1xufVxuXG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGBhYCBpcyBlcXVhbCB0byBgYmAuXG4gKlxuICogQHBhcmFtIHthcnJheTxzdHJpbmc+fSBhXG4gKiBAcGFyYW0ge2FycmF5PHN0cmluZz59IGJcbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG5mdW5jdGlvbiBjaGVja0VxdWFsKGEsIGIpIHtcbiAgcmV0dXJuIGNvbXBhcmVDb21wb25lbnRzKGEsIGIpID09PSAwO1xufVxuXG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGBhYCBpcyBncmVhdGVyIHRoYW4gb3IgZXF1YWwgdG8gYGJgLlxuICpcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYVxuICogQHBhcmFtIHthcnJheTxzdHJpbmc+fSBiXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gY2hlY2tHcmVhdGVyVGhhbk9yRXF1YWwoYSwgYikge1xuICB2YXIgcmVzdWx0ID0gY29tcGFyZUNvbXBvbmVudHMoYSwgYik7XG4gIHJldHVybiByZXN1bHQgPT09IDEgfHwgcmVzdWx0ID09PSAwO1xufVxuXG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGBhYCBpcyBncmVhdGVyIHRoYW4gYGJgLlxuICpcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYVxuICogQHBhcmFtIHthcnJheTxzdHJpbmc+fSBiXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZnVuY3Rpb24gY2hlY2tHcmVhdGVyVGhhbihhLCBiKSB7XG4gIHJldHVybiBjb21wYXJlQ29tcG9uZW50cyhhLCBiKSA9PT0gMTtcbn1cblxuLyoqXG4gKiBDaGVja3Mgd2hldGhlciBgYWAgaXMgXCJyZWFzb25hYmx5IGNsb3NlXCIgdG8gYGJgIChhcyBkZXNjcmliZWQgaW5cbiAqIGh0dHBzOi8vd3d3Lm5wbWpzLm9yZy9kb2MvbWlzYy9zZW12ZXIuaHRtbCkuIEZvciBleGFtcGxlLCBpZiBgYmAgaXMgXCIxLjMuMVwiXG4gKiB0aGVuIFwicmVhc29uYWJseSBjbG9zZVwiIGlzIGRlZmluZWQgYXMgXCI+PSAxLjMuMSBhbmQgPCAxLjRcIi5cbiAqXG4gKiBAcGFyYW0ge2FycmF5PHN0cmluZz59IGFcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYlxuICogQHJldHVybnMge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIGNoZWNrQXBwcm94aW1hdGVWZXJzaW9uKGEsIGIpIHtcbiAgdmFyIGxvd2VyQm91bmQgPSBiLnNsaWNlKCk7XG4gIHZhciB1cHBlckJvdW5kID0gYi5zbGljZSgpO1xuXG4gIGlmICh1cHBlckJvdW5kLmxlbmd0aCA+IDEpIHtcbiAgICB1cHBlckJvdW5kLnBvcCgpO1xuICB9XG4gIHZhciBsYXN0SW5kZXggPSB1cHBlckJvdW5kLmxlbmd0aCAtIDE7XG4gIHZhciBudW1lcmljID0gcGFyc2VJbnQodXBwZXJCb3VuZFtsYXN0SW5kZXhdLCAxMCk7XG4gIGlmIChpc051bWJlcihudW1lcmljKSkge1xuICAgIHVwcGVyQm91bmRbbGFzdEluZGV4XSA9IG51bWVyaWMgKyAxICsgJyc7XG4gIH1cblxuICByZXR1cm4gY2hlY2tHcmVhdGVyVGhhbk9yRXF1YWwoYSwgbG93ZXJCb3VuZCkgJiYgY2hlY2tMZXNzVGhhbihhLCB1cHBlckJvdW5kKTtcbn1cblxuLyoqXG4gKiBFeHRyYWN0cyB0aGUgb3B0aW9uYWwgbW9kaWZpZXIgKDwsIDw9LCA9LCA+PSwgPiwgfiwgfj4pIGFuZCB2ZXJzaW9uXG4gKiBjb21wb25lbnRzIGZyb20gYHJhbmdlYC5cbiAqXG4gKiBGb3IgZXhhbXBsZSwgZ2l2ZW4gYHJhbmdlYCBcIj49IDEuMi4zXCIgcmV0dXJucyBhbiBvYmplY3Qgd2l0aCBhIGBtb2RpZmllcmAgb2ZcbiAqIGBcIj49XCJgIGFuZCBgY29tcG9uZW50c2Agb2YgYFsxLCAyLCAzXWAuXG4gKlxuICogQHBhcmFtIHtzdHJpbmd9IHJhbmdlXG4gKiBAcmV0dXJucyB7b2JqZWN0fVxuICovXG5mdW5jdGlvbiBnZXRNb2RpZmllckFuZENvbXBvbmVudHMocmFuZ2UpIHtcbiAgdmFyIHJhbmdlQ29tcG9uZW50cyA9IHJhbmdlLnNwbGl0KGNvbXBvbmVudFJlZ2V4KTtcbiAgdmFyIG1hdGNoZXMgPSByYW5nZUNvbXBvbmVudHNbMF0ubWF0Y2gobW9kaWZpZXJSZWdleCk7XG4gICFtYXRjaGVzID8gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyA/IGludmFyaWFudChmYWxzZSwgJ2V4cGVjdGVkIHJlZ2V4IHRvIG1hdGNoIGJ1dCBpdCBkaWQgbm90JykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIHJldHVybiB7XG4gICAgbW9kaWZpZXI6IG1hdGNoZXNbMV0sXG4gICAgcmFuZ2VDb21wb25lbnRzOiBbbWF0Y2hlc1syXV0uY29uY2F0KHJhbmdlQ29tcG9uZW50cy5zbGljZSgxKSlcbiAgfTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmVzIGlmIGBudW1iZXJgIGlzIGEgbnVtYmVyLlxuICpcbiAqIEBwYXJhbSB7bWl4ZWR9IG51bWJlclxuICogQHJldHVybnMge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIGlzTnVtYmVyKG51bWJlcikge1xuICByZXR1cm4gIWlzTmFOKG51bWJlcikgJiYgaXNGaW5pdGUobnVtYmVyKTtcbn1cblxuLyoqXG4gKiBUZXN0cyB3aGV0aGVyIGByYW5nZWAgaXMgYSBcInNpbXBsZVwiIHZlcnNpb24gbnVtYmVyIHdpdGhvdXQgYW55IG1vZGlmaWVyc1xuICogKFwiPlwiLCBcIn5cIiBldGMpLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSByYW5nZVxuICogQHJldHVybnMge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIGlzU2ltcGxlVmVyc2lvbihyYW5nZSkge1xuICByZXR1cm4gIWdldE1vZGlmaWVyQW5kQ29tcG9uZW50cyhyYW5nZSkubW9kaWZpZXI7XG59XG5cbi8qKlxuICogWmVyby1wYWRzIGFycmF5IGBhcnJheWAgdW50aWwgaXQgaXMgYXQgbGVhc3QgYGxlbmd0aGAgbG9uZy5cbiAqXG4gKiBAcGFyYW0ge2FycmF5fSBhcnJheVxuICogQHBhcmFtIHtudW1iZXJ9IGxlbmd0aFxuICovXG5mdW5jdGlvbiB6ZXJvUGFkKGFycmF5LCBsZW5ndGgpIHtcbiAgZm9yICh2YXIgaSA9IGFycmF5Lmxlbmd0aDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgYXJyYXlbaV0gPSAnMCc7XG4gIH1cbn1cblxuLyoqXG4gKiBOb3JtYWxpemVzIGBhYCBhbmQgYGJgIGluIHByZXBhcmF0aW9uIGZvciBjb21wYXJpc29uIGJ5IGRvaW5nIHRoZSBmb2xsb3dpbmc6XG4gKlxuICogLSB6ZXJvLXBhZHMgYGFgIGFuZCBgYmBcbiAqIC0gbWFya3MgYW55IFwieFwiLCBcIlhcIiBvciBcIipcIiBjb21wb25lbnQgaW4gYGJgIGFzIGVxdWl2YWxlbnQgYnkgemVyby1pbmcgaXQgb3V0XG4gKiAgIGluIGJvdGggYGFgIGFuZCBgYmBcbiAqIC0gbWFya3MgYW55IGZpbmFsIFwiKlwiIGNvbXBvbmVudCBpbiBgYmAgYXMgYSBncmVlZHkgd2lsZGNhcmQgYnkgemVyby1pbmcgaXRcbiAqICAgYW5kIGFsbCBvZiBpdHMgc3VjY2Vzc29ycyBpbiBgYWBcbiAqXG4gKiBAcGFyYW0ge2FycmF5PHN0cmluZz59IGFcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYlxuICogQHJldHVybnMge2FycmF5PGFycmF5PHN0cmluZz4+fVxuICovXG5mdW5jdGlvbiBub3JtYWxpemVWZXJzaW9ucyhhLCBiKSB7XG4gIGEgPSBhLnNsaWNlKCk7XG4gIGIgPSBiLnNsaWNlKCk7XG5cbiAgemVyb1BhZChhLCBiLmxlbmd0aCk7XG5cbiAgLy8gbWFyayBcInhcIiBhbmQgXCIqXCIgY29tcG9uZW50cyBhcyBlcXVhbFxuICBmb3IgKHZhciBpID0gMDsgaSA8IGIubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgbWF0Y2hlcyA9IGJbaV0ubWF0Y2goL15beCpdJC9pKTtcbiAgICBpZiAobWF0Y2hlcykge1xuICAgICAgYltpXSA9IGFbaV0gPSAnMCc7XG5cbiAgICAgIC8vIGZpbmFsIFwiKlwiIGdyZWVkaWx5IHplcm9zIGFsbCByZW1haW5pbmcgY29tcG9uZW50c1xuICAgICAgaWYgKG1hdGNoZXNbMF0gPT09ICcqJyAmJiBpID09PSBiLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgZm9yICh2YXIgaiA9IGk7IGogPCBhLmxlbmd0aDsgaisrKSB7XG4gICAgICAgICAgYVtqXSA9ICcwJztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHplcm9QYWQoYiwgYS5sZW5ndGgpO1xuXG4gIHJldHVybiBbYSwgYl07XG59XG5cbi8qKlxuICogUmV0dXJucyB0aGUgbnVtZXJpY2FsIC0tIG5vdCB0aGUgbGV4aWNvZ3JhcGhpY2FsIC0tIG9yZGVyaW5nIG9mIGBhYCBhbmQgYGJgLlxuICpcbiAqIEZvciBleGFtcGxlLCBgMTAtYWxwaGFgIGlzIGdyZWF0ZXIgdGhhbiBgMi1iZXRhYC5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gYVxuICogQHBhcmFtIHtzdHJpbmd9IGJcbiAqIEByZXR1cm5zIHtudW1iZXJ9IC0xLCAwIG9yIDEgdG8gaW5kaWNhdGUgd2hldGhlciBgYWAgaXMgbGVzcyB0aGFuLCBlcXVhbCB0byxcbiAqIG9yIGdyZWF0ZXIgdGhhbiBgYmAsIHJlc3BlY3RpdmVseVxuICovXG5mdW5jdGlvbiBjb21wYXJlTnVtZXJpYyhhLCBiKSB7XG4gIHZhciBhUHJlZml4ID0gYS5tYXRjaChudW1lcmljUmVnZXgpWzFdO1xuICB2YXIgYlByZWZpeCA9IGIubWF0Y2gobnVtZXJpY1JlZ2V4KVsxXTtcbiAgdmFyIGFOdW1lcmljID0gcGFyc2VJbnQoYVByZWZpeCwgMTApO1xuICB2YXIgYk51bWVyaWMgPSBwYXJzZUludChiUHJlZml4LCAxMCk7XG5cbiAgaWYgKGlzTnVtYmVyKGFOdW1lcmljKSAmJiBpc051bWJlcihiTnVtZXJpYykgJiYgYU51bWVyaWMgIT09IGJOdW1lcmljKSB7XG4gICAgcmV0dXJuIGNvbXBhcmUoYU51bWVyaWMsIGJOdW1lcmljKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gY29tcGFyZShhLCBiKTtcbiAgfVxufVxuXG4vKipcbiAqIFJldHVybnMgdGhlIG9yZGVyaW5nIG9mIGBhYCBhbmQgYGJgLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfG51bWJlcn0gYVxuICogQHBhcmFtIHtzdHJpbmd8bnVtYmVyfSBiXG4gKiBAcmV0dXJucyB7bnVtYmVyfSAtMSwgMCBvciAxIHRvIGluZGljYXRlIHdoZXRoZXIgYGFgIGlzIGxlc3MgdGhhbiwgZXF1YWwgdG8sXG4gKiBvciBncmVhdGVyIHRoYW4gYGJgLCByZXNwZWN0aXZlbHlcbiAqL1xuZnVuY3Rpb24gY29tcGFyZShhLCBiKSB7XG4gICEodHlwZW9mIGEgPT09IHR5cGVvZiBiKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdcImFcIiBhbmQgXCJiXCIgbXVzdCBiZSBvZiB0aGUgc2FtZSB0eXBlJykgOiBpbnZhcmlhbnQoZmFsc2UpIDogdm9pZCAwO1xuXG4gIGlmIChhID4gYikge1xuICAgIHJldHVybiAxO1xuICB9IGVsc2UgaWYgKGEgPCBiKSB7XG4gICAgcmV0dXJuIC0xO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiAwO1xuICB9XG59XG5cbi8qKlxuICogQ29tcGFyZXMgYXJyYXlzIG9mIHZlcnNpb24gY29tcG9uZW50cy5cbiAqXG4gKiBAcGFyYW0ge2FycmF5PHN0cmluZz59IGFcbiAqIEBwYXJhbSB7YXJyYXk8c3RyaW5nPn0gYlxuICogQHJldHVybnMge251bWJlcn0gLTEsIDAgb3IgMSB0byBpbmRpY2F0ZSB3aGV0aGVyIGBhYCBpcyBsZXNzIHRoYW4sIGVxdWFsIHRvLFxuICogb3IgZ3JlYXRlciB0aGFuIGBiYCwgcmVzcGVjdGl2ZWx5XG4gKi9cbmZ1bmN0aW9uIGNvbXBhcmVDb21wb25lbnRzKGEsIGIpIHtcbiAgdmFyIF9ub3JtYWxpemVWZXJzaW9ucyA9IG5vcm1hbGl6ZVZlcnNpb25zKGEsIGIpLFxuICAgICAgYU5vcm1hbGl6ZWQgPSBfbm9ybWFsaXplVmVyc2lvbnNbMF0sXG4gICAgICBiTm9ybWFsaXplZCA9IF9ub3JtYWxpemVWZXJzaW9uc1sxXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGJOb3JtYWxpemVkLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIHJlc3VsdCA9IGNvbXBhcmVOdW1lcmljKGFOb3JtYWxpemVkW2ldLCBiTm9ybWFsaXplZFtpXSk7XG4gICAgaWYgKHJlc3VsdCkge1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gMDtcbn1cblxudmFyIFZlcnNpb25SYW5nZSA9IHtcbiAgLyoqXG4gICAqIENoZWNrcyB3aGV0aGVyIGB2ZXJzaW9uYCBzYXRpc2ZpZXMgdGhlIGByYW5nZWAgc3BlY2lmaWNhdGlvbi5cbiAgICpcbiAgICogV2Ugc3VwcG9ydCBhIHN1YnNldCBvZiB0aGUgZXhwcmVzc2lvbnMgZGVmaW5lZCBpblxuICAgKiBodHRwczovL3d3dy5ucG1qcy5vcmcvZG9jL21pc2Mvc2VtdmVyLmh0bWw6XG4gICAqXG4gICAqICAgIHZlcnNpb24gICBNdXN0IG1hdGNoIHZlcnNpb24gZXhhY3RseVxuICAgKiAgICA9dmVyc2lvbiAgU2FtZSBhcyBqdXN0IHZlcnNpb25cbiAgICogICAgPnZlcnNpb24gIE11c3QgYmUgZ3JlYXRlciB0aGFuIHZlcnNpb25cbiAgICogICAgPj12ZXJzaW9uIE11c3QgYmUgZ3JlYXRlciB0aGFuIG9yIGVxdWFsIHRvIHZlcnNpb25cbiAgICogICAgPHZlcnNpb24gIE11c3QgYmUgbGVzcyB0aGFuIHZlcnNpb25cbiAgICogICAgPD12ZXJzaW9uIE11c3QgYmUgbGVzcyB0aGFuIG9yIGVxdWFsIHRvIHZlcnNpb25cbiAgICogICAgfnZlcnNpb24gIE11c3QgYmUgYXQgbGVhc3QgdmVyc2lvbiwgYnV0IGxlc3MgdGhhbiB0aGUgbmV4dCBzaWduaWZpY2FudFxuICAgKiAgICAgICAgICAgICAgcmV2aXNpb24gYWJvdmUgdmVyc2lvbjpcbiAgICogICAgICAgICAgICAgIFwifjEuMi4zXCIgaXMgZXF1aXZhbGVudCB0byBcIj49IDEuMi4zIGFuZCA8IDEuM1wiXG4gICAqICAgIH4+dmVyc2lvbiBFcXVpdmFsZW50IHRvIH52ZXJzaW9uXG4gICAqICAgIDEuMi54ICAgICBNdXN0IG1hdGNoIFwiMS4yLnhcIiwgd2hlcmUgXCJ4XCIgaXMgYSB3aWxkY2FyZCB0aGF0IG1hdGNoZXNcbiAgICogICAgICAgICAgICAgIGFueXRoaW5nXG4gICAqICAgIDEuMi4qICAgICBTaW1pbGFyIHRvIFwiMS4yLnhcIiwgYnV0IFwiKlwiIGluIHRoZSB0cmFpbGluZyBwb3NpdGlvbiBpcyBhXG4gICAqICAgICAgICAgICAgICBcImdyZWVkeVwiIHdpbGRjYXJkLCBzbyB3aWxsIG1hdGNoIGFueSBudW1iZXIgb2YgYWRkaXRpb25hbFxuICAgKiAgICAgICAgICAgICAgY29tcG9uZW50czpcbiAgICogICAgICAgICAgICAgIFwiMS4yLipcIiB3aWxsIG1hdGNoIFwiMS4yLjFcIiwgXCIxLjIuMS4xXCIsIFwiMS4yLjEuMS4xXCIgZXRjXG4gICAqICAgICogICAgICAgICBBbnkgdmVyc2lvblxuICAgKiAgICBcIlwiICAgICAgICAoRW1wdHkgc3RyaW5nKSBTYW1lIGFzICpcbiAgICogICAgdjEgLSB2MiAgIEVxdWl2YWxlbnQgdG8gXCI+PSB2MSBhbmQgPD0gdjJcIlxuICAgKiAgICByMSB8fCByMiAgUGFzc2VzIGlmIGVpdGhlciByMSBvciByMiBhcmUgc2F0aXNmaWVkXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSByYW5nZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gdmVyc2lvblxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGNvbnRhaW5zOiBmdW5jdGlvbiBjb250YWlucyhyYW5nZSwgdmVyc2lvbikge1xuICAgIHJldHVybiBjaGVja09yRXhwcmVzc2lvbihyYW5nZS50cmltKCksIHZlcnNpb24udHJpbSgpKTtcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBWZXJzaW9uUmFuZ2U7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7OztBQVFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBYkE7QUFlQTtBQUVBOzs7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7Ozs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7O0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7QUFRQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQkE7QUFDQTtBQUNBO0FBbENBO0FBcUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/VersionRange.js
