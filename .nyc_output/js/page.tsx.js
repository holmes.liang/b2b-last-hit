__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _widget__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../widget */ "./src/component/widget.tsx");
/* harmony import */ var _page_body__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./page-body */ "./src/component/page/page-body.tsx");
/* harmony import */ var _page_footer__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./page-footer */ "./src/component/page/page-footer.tsx");
/* harmony import */ var _page_header__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./page-header */ "./src/component/page/page-header.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");









var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/component/page/page.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_2__["default"])(["\n        table{\n          thead.ant-table-thead{\n            th{\n              text-transform: uppercase;\n              color: #9e9e9e;\n            }\n          }\n        }\n        textarea.ant-input {\n            max-width: 100%;\n            height: auto;\n            min-height: 32px;\n            line-height: 1.5;\n            vertical-align: bottom;\n            transition: all 0.3s, height 0s;\n            // \u6539\u53D8\u9ED8\u8BA4\u5B57\u4F53\n            font-size: 16px;\n            font-weight: 300;\n            color: #333 !important;\n        }\n        .mobile-form-item {\n          padding: 0 20px;\n          .ant-col {\n            width: 100%;\n            &.ant-form-item-label {\n              margin-bottom: 10px;\n            }\n          }\n          &.mobile-label {\n            .ant-col {\n              &.ant-form-item-label {\n                line-height: 20px;\n                label {\n                  line-height: 20px;\n                }\n              }\n            }\n          }\n          &.mobile-bottom {\n            .ant-col {\n              &.ant-form-item-label {\n                margin-bottom: 0;\n              }\n            }\n          }\n        }\n        .mobile-action-large {\n          .ant-btn {\n              float: none !important;\n              width: calc(100% - 40px);\n              display: block;\n              margin: 0 auto !important;\n              margin-bottom: 25px !important;\n              line-height: 40px;\n              height: 40px;\n          }\n        }   \n        .mobile-action-large-32 {\n          .ant-btn {\n              float: none !important;\n              width: calc(100% - 32px);\n              display: block;\n              margin: 0 auto !important;\n              margin-bottom: 25px !important;\n          }\n        }   \n      .mobile-card {\n        .card-item__head {\n          height: auto !important;\n        }\n        .card-item__body {\n          .card-item__cell {\n            padding: 0 30px 0 95px;\n            &:first-child {\n              padding: 0 30px;\n            }\n          }\n        }\n      }\n      .mobile-card-header {\n        .card-item__head {\n          line-height: 20px;\n          margin: 10px 0;\n        }\n      }\n      .mobile-card-list {\n        .card-item__cell:first-child {\n          padding: 0 30px;\n        }\n        // .card-item__cell {\n        //   width: 33% !important;\n        // }\n      }\n        overflow: hidden;\n        display: flex;\n        flex-direction: column;\n        -webkit-overflow-scrolling: touch; \n        // overflow-y: scroll;\n        .page-body > * {\n          //min-height: ", "px;\n          // margin-bottom: ", ";\n          padding-top: 40px;\n          padding-bottom: 20px;\n          min-height: ", ";//todo\n        }\n        .ant-btn-primary,.ant-btn-background-ghost.ant-btn-primary{\n            background: ", ";\n            border-color: ", ";\n        }\n        .ant-btn-background-ghost.ant-btn-primary {\n          color: ", ";\n        }\n        &> div:nth-child(2){\n          position: relative;\n        }\n        .page-title--main {\n          font-size: 20px;\n          padding: 14px 24px 24px 24px;\n          text-align: center;\n          text-transform: uppercase;\n        }\n        .page-title {\n          font-weight: 500;\n          color: rgba(0,0,0,.85);\n          white-space: normal;\n        }\n        .action {\n          padding-bottom: 20px;\n          padding-top: 20px;\n          button {\n              margin-left: .5em;\n              min-width: 7.5em;\n              overflow: hidden;\n              text-overflow: ellipsis;\n              white-space: nowrap;\n              float:right;\n              &:last-child {\n                  margin-right: .5em;\n              }\n              &:first-child {\n                  float: left;\n              }\n          }  \n        }\n        \n         .ant-progress-circle .ant-progress-text {\n            .price--black {\n              color: rgba(0, 0, 0, 0.85);\n            }\n            .price--lg {\n               font-size: 16px;\n               font-weight: 400;\n            }\n         }\n         .ant-btn:hover, .ant-btn:focus {\n            color: ", ";\n            border-color: ", ";\n         }\n         .ant-btn-primary:hover, .ant-btn-primary:focus {\n            color: #fff;\n         }\n         .primary-color{\n            color: ", ";\n         }\n         .price {\n              color: ", ";\n              font-weight: 600;\n              font-size: 14px;\n              &::before {\n                  font-size: 80%;\n                  font-weight: 400;\n                  margin-right: .2em;\n                  content: attr(data-prefix);\n                  vertical-align: 0;\n              }\n              &::after {\n                  font-size: 80%;\n                  font-weight: 400;\n                  display: inline-block;\n                  margin-left: .3em;\n                  content: attr(data-postfix);\n                  vertical-align: 1px;\n                  color: #878787;\n              }\n          }\n         .price--lg {\n            font-size: 16px;\n         }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var isMobile = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getIsMobile();

var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_15__["default"].getTheme(),
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

/**
 * Page component which contains header, body and footer
 */
var Page =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(Page, _Widget);

  function Page(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Page);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Page).call(this, props, context));
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Page, [{
    key: "initComponents",
    value: function initComponents() {
      var mHeight = _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].getHeight() - (isMobile ? 169 : 196);
      var clientHeight = document.body.clientHeight;
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_10__["Styled"].div.attrs({
          "data-widget": "page"
        })(_templateObject(), mHeight, isMobile ? "0" : "60px", "".concat(clientHeight - 195, "px"), COLOR_PRIMARY, COLOR_PRIMARY, COLOR_PRIMARY, COLOR_PRIMARY, COLOR_PRIMARY, COLOR_PRIMARY, COLOR_PRIMARY)
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_7__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Page.prototype), "initState", this).call(this), {
        isGetLogo: false
      });
    }
  }, {
    key: "isStateLogo",
    value: function isStateLogo() {
      return this.state.isGetLogo;
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getTitleLogo();
      this.initTheme();
      _common__WEBPACK_IMPORTED_MODULE_9__["Utils"].bodyScale();
    }
  }, {
    key: "getTitleLogo",
    value: function getTitleLogo() {
      var _this2 = this;

      if (!_common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].LOGO_KEY)) {
        this.getLogo().then(function (res) {
          _common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().set(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].LOGO_KEY, (res.body || {}).respData);

          _this2.setState({
            isGetLogo: true
          });
        });
      } else {
        this.setState({
          isGetLogo: true
        });
      }

      if (!_common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].TITLE_KEY)) {
        this.getHtmlTitle().then(function (res) {
          _common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().set(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].TITLE_KEY, (res.body || {}).respData);
          document.title = (res.body || {}).respData || "Bytesforce";
        });
      } else {
        document.title = _common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].TITLE_KEY) || "Bytesforce";
      }

      if (!_common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].FAVICON_KEY)) {
        this.getFavicon().then(function (res) {
          _common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().set(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].FAVICON_KEY, (res.body || {}).respData);
          document.querySelectorAll("[name='favicon']")[0].setAttribute("href", (res.body || {}).respData || _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].getServiceLocation(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].LOGO));
        });
      } else {
        document.querySelectorAll("[name='favicon']")[0].setAttribute("href", _common__WEBPACK_IMPORTED_MODULE_9__["Storage"].TitleLogo.session().get(_common__WEBPACK_IMPORTED_MODULE_9__["Consts"].FAVICON_KEY) || _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].getServiceLocation(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].LOGO));
      }
    }
  }, {
    key: "renderPageHeader",
    value: function renderPageHeader() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_page_header__WEBPACK_IMPORTED_MODULE_14__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 273
        },
        __self: this
      });
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_page_body__WEBPACK_IMPORTED_MODULE_12__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 277
        },
        __self: this
      });
    }
  }, {
    key: "renderPageFooter",
    value: function renderPageFooter() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(_page_footer__WEBPACK_IMPORTED_MODULE_13__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      });
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.C;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_10__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287
        },
        __self: this
      }, this.renderPageHeader(), this.renderPageBody(), this.renderPageFooter());
    }
    /**
     * current page title
     * @return {string} default return null
     */

  }, {
    key: "getHtmlTitle",
    value: function () {
      var _getHtmlTitle = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].COMPANYNAME, null, {
                  ignoreAuth: true
                });

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getHtmlTitle() {
        return _getHtmlTitle.apply(this, arguments);
      }

      return getHtmlTitle;
    }()
  }, {
    key: "getFavicon",
    value: function () {
      var _getFavicon = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].FAVICON, null, {
                  ignoreAuth: true
                });

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getFavicon() {
        return _getFavicon.apply(this, arguments);
      }

      return getFavicon;
    }()
  }, {
    key: "getLogo",
    value: function () {
      var _getLogo = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].LOGO, null, {
                  ignoreAuth: true
                });

              case 2:
                return _context3.abrupt("return", _context3.sent);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getLogo() {
        return _getLogo.apply(this, arguments);
      }

      return getLogo;
    }()
    /**
     * init theme
     */

  }, {
    key: "initTheme",
    value: function initTheme() {
      var retrieve = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      var theme = _common__WEBPACK_IMPORTED_MODULE_9__["Envs"].getCurrentTheme();

      if (theme) {
        _common__WEBPACK_IMPORTED_MODULE_9__["Envs"].application().changeTheme(theme);
      }

      return Promise.resolve();
    }
  }]);

  return Page;
}(_widget__WEBPACK_IMPORTED_MODULE_11__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (Page);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L3BhZ2UvcGFnZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvcGFnZS9wYWdlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBamF4LCBBcGlzLCBDb25zdHMsIEVudnMsIFN0b3JhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IFBhZ2VDb21wb25lbnRzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IFdpZGdldCBmcm9tIFwiLi4vd2lkZ2V0XCI7XG5pbXBvcnQgUGFnZUJvZHkgZnJvbSBcIi4vcGFnZS1ib2R5XCI7XG5pbXBvcnQgUGFnZUZvb3RlciBmcm9tIFwiLi9wYWdlLWZvb3RlclwiO1xuaW1wb3J0IFBhZ2VIZWFkZXIgZnJvbSBcIi4vcGFnZS1oZWFkZXJcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuXG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5jb25zdCB7IENPTE9SX1BSSU1BUlkgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5cbmludGVyZmFjZSBJU3RhdGUge1xuICBbcHJvcE5hbWU6IHN0cmluZ106IGFueTtcbn1cblxuLyoqXG4gKiBQYWdlIGNvbXBvbmVudCB3aGljaCBjb250YWlucyBoZWFkZXIsIGJvZHkgYW5kIGZvb3RlclxuICovXG5jbGFzcyBQYWdlPFAsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBQYWdlQ29tcG9uZW50cz4gZXh0ZW5kcyBXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogYW55LCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICAgIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCA9IDA7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgY29uc3QgbUhlaWdodCA9IFV0aWxzLmdldEhlaWdodCgpIC0gKGlzTW9iaWxlID8gMTY5IDogMTk2KTtcbiAgICBjb25zdCBjbGllbnRIZWlnaHQgPSBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodDtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2LmF0dHJzKHtcbiAgICAgICAgXCJkYXRhLXdpZGdldFwiOiBcInBhZ2VcIixcbiAgICAgIH0pYFxuICAgICAgICB0YWJsZXtcbiAgICAgICAgICB0aGVhZC5hbnQtdGFibGUtdGhlYWR7XG4gICAgICAgICAgICB0aHtcbiAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgICAgICAgY29sb3I6ICM5ZTllOWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRleHRhcmVhLmFudC1pbnB1dCB7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgICAgICBtaW4taGVpZ2h0OiAzMnB4O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBib3R0b207XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcywgaGVpZ2h0IDBzO1xuICAgICAgICAgICAgLy8g5pS55Y+Y6buY6K6k5a2X5L2TXG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAgICAgY29sb3I6ICMzMzMgIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAubW9iaWxlLWZvcm0taXRlbSB7XG4gICAgICAgICAgcGFkZGluZzogMCAyMHB4O1xuICAgICAgICAgIC5hbnQtY29sIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgJi5hbnQtZm9ybS1pdGVtLWxhYmVsIHtcbiAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgJi5tb2JpbGUtbGFiZWwge1xuICAgICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgICAmLmFudC1mb3JtLWl0ZW0tbGFiZWwge1xuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgIGxhYmVsIHtcbiAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICAmLm1vYmlsZS1ib3R0b20ge1xuICAgICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgICAmLmFudC1mb3JtLWl0ZW0tbGFiZWwge1xuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLm1vYmlsZS1hY3Rpb24tbGFyZ2Uge1xuICAgICAgICAgIC5hbnQtYnRuIHtcbiAgICAgICAgICAgICAgZmxvYXQ6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDQwcHgpO1xuICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgfSAgIFxuICAgICAgICAubW9iaWxlLWFjdGlvbi1sYXJnZS0zMiB7XG4gICAgICAgICAgLmFudC1idG4ge1xuICAgICAgICAgICAgICBmbG9hdDogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMzJweCk7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9ICAgXG4gICAgICAubW9iaWxlLWNhcmQge1xuICAgICAgICAuY2FyZC1pdGVtX19oZWFkIHtcbiAgICAgICAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAuY2FyZC1pdGVtX19ib2R5IHtcbiAgICAgICAgICAuY2FyZC1pdGVtX19jZWxsIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDAgMzBweCAwIDk1cHg7XG4gICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgcGFkZGluZzogMCAzMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLm1vYmlsZS1jYXJkLWhlYWRlciB7XG4gICAgICAgIC5jYXJkLWl0ZW1fX2hlYWQge1xuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgICAgICAgIG1hcmdpbjogMTBweCAwO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICAubW9iaWxlLWNhcmQtbGlzdCB7XG4gICAgICAgIC5jYXJkLWl0ZW1fX2NlbGw6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgIHBhZGRpbmc6IDAgMzBweDtcbiAgICAgICAgfVxuICAgICAgICAvLyAuY2FyZC1pdGVtX19jZWxsIHtcbiAgICAgICAgLy8gICB3aWR0aDogMzMlICFpbXBvcnRhbnQ7XG4gICAgICAgIC8vIH1cbiAgICAgIH1cbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAgLXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6IHRvdWNoOyBcbiAgICAgICAgLy8gb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgICAgICAucGFnZS1ib2R5ID4gKiB7XG4gICAgICAgICAgLy9taW4taGVpZ2h0OiAke21IZWlnaHR9cHg7XG4gICAgICAgICAgLy8gbWFyZ2luLWJvdHRvbTogJHtpc01vYmlsZSA/IFwiMFwiIDogXCI2MHB4XCJ9O1xuICAgICAgICAgIHBhZGRpbmctdG9wOiA0MHB4O1xuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICAgICAgICAgIG1pbi1oZWlnaHQ6ICR7YCR7Y2xpZW50SGVpZ2h0IC0gMTk1fXB4YH07Ly90b2RvXG4gICAgICAgIH1cbiAgICAgICAgLmFudC1idG4tcHJpbWFyeSwuYW50LWJ0bi1iYWNrZ3JvdW5kLWdob3N0LmFudC1idG4tcHJpbWFyeXtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICR7Q09MT1JfUFJJTUFSWX07XG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICR7Q09MT1JfUFJJTUFSWX07XG4gICAgICAgIH1cbiAgICAgICAgLmFudC1idG4tYmFja2dyb3VuZC1naG9zdC5hbnQtYnRuLXByaW1hcnkge1xuICAgICAgICAgIGNvbG9yOiAke0NPTE9SX1BSSU1BUll9O1xuICAgICAgICB9XG4gICAgICAgICY+IGRpdjpudGgtY2hpbGQoMil7XG4gICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICB9XG4gICAgICAgIC5wYWdlLXRpdGxlLS1tYWluIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogMTRweCAyNHB4IDI0cHggMjRweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgfVxuICAgICAgICAucGFnZS10aXRsZSB7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICBjb2xvcjogcmdiYSgwLDAsMCwuODUpO1xuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgICAgIH1cbiAgICAgICAgLmFjdGlvbiB7XG4gICAgICAgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gICAgICAgICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgICAgICAgYnV0dG9uIHtcbiAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC41ZW07XG4gICAgICAgICAgICAgIG1pbi13aWR0aDogNy41ZW07XG4gICAgICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICAgICAgICBmbG9hdDpyaWdodDtcbiAgICAgICAgICAgICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogLjVlbTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAmOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSAgXG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgICAuYW50LXByb2dyZXNzLWNpcmNsZSAuYW50LXByb2dyZXNzLXRleHQge1xuICAgICAgICAgICAgLnByaWNlLS1ibGFjayB7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnByaWNlLS1sZyB7XG4gICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgfVxuICAgICAgICAgLmFudC1idG46aG92ZXIsIC5hbnQtYnRuOmZvY3VzIHtcbiAgICAgICAgICAgIGNvbG9yOiAke0NPTE9SX1BSSU1BUll9O1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAke0NPTE9SX1BSSU1BUll9O1xuICAgICAgICAgfVxuICAgICAgICAgLmFudC1idG4tcHJpbWFyeTpob3ZlciwgLmFudC1idG4tcHJpbWFyeTpmb2N1cyB7XG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgIH1cbiAgICAgICAgIC5wcmltYXJ5LWNvbG9ye1xuICAgICAgICAgICAgY29sb3I6ICR7Q09MT1JfUFJJTUFSWX07XG4gICAgICAgICB9XG4gICAgICAgICAucHJpY2Uge1xuICAgICAgICAgICAgICBjb2xvcjogJHtDT0xPUl9QUklNQVJZfTtcbiAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAmOjpiZWZvcmUge1xuICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA4MCU7XG4gICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAuMmVtO1xuICAgICAgICAgICAgICAgICAgY29udGVudDogYXR0cihkYXRhLXByZWZpeCk7XG4gICAgICAgICAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogMDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAmOjphZnRlciB7XG4gICAgICAgICAgICAgICAgICBmb250LXNpemU6IDgwJTtcbiAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLjNlbTtcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IGF0dHIoZGF0YS1wb3N0Zml4KTtcbiAgICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiAxcHg7XG4gICAgICAgICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgIC5wcmljZS0tbGcge1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgfVxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBpc0dldExvZ286IGZhbHNlLFxuICAgIH0pIGFzIFM7XG4gIH1cblxuICBpc1N0YXRlTG9nbygpIHtcbiAgICByZXR1cm4gdGhpcy5zdGF0ZS5pc0dldExvZ287XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmdldFRpdGxlTG9nbygpO1xuICAgIHRoaXMuaW5pdFRoZW1lKCk7XG4gICAgVXRpbHMuYm9keVNjYWxlKCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0VGl0bGVMb2dvKCkge1xuICAgIGlmICghU3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuTE9HT19LRVkpKSB7XG4gICAgICB0aGlzLmdldExvZ28oKS50aGVuKHJlcyA9PiB7XG4gICAgICAgIFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5zZXQoQ29uc3RzLkxPR09fS0VZLCAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhKTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgaXNHZXRMb2dvOiB0cnVlLFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgaXNHZXRMb2dvOiB0cnVlLFxuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICghU3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuVElUTEVfS0VZKSkge1xuICAgICAgdGhpcy5nZXRIdG1sVGl0bGUoKS50aGVuKHJlcyA9PiB7XG4gICAgICAgIFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5zZXQoQ29uc3RzLlRJVExFX0tFWSwgKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSk7XG4gICAgICAgIGRvY3VtZW50LnRpdGxlID0gKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCAocHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0RFRkFVTFRfVElUTEUgYXMgc3RyaW5nKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBkb2N1bWVudC50aXRsZSA9IFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5nZXQoQ29uc3RzLlRJVExFX0tFWSkgfHwgKHByb2Nlc3MuZW52LlJFQUNUX0FQUF9ERUZBVUxUX1RJVExFIGFzIHN0cmluZyk7XG4gICAgfVxuICAgIGlmICghU3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuRkFWSUNPTl9LRVkpKSB7XG4gICAgICB0aGlzLmdldEZhdmljb24oKS50aGVuKHJlcyA9PiB7XG4gICAgICAgIFN0b3JhZ2UuVGl0bGVMb2dvLnNlc3Npb24oKS5zZXQoQ29uc3RzLkZBVklDT05fS0VZLCAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhKTtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltuYW1lPSdmYXZpY29uJ11cIilbMF0uc2V0QXR0cmlidXRlKFwiaHJlZlwiLCAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhIHx8IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKEFwaXMuTE9HTykpO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbbmFtZT0nZmF2aWNvbiddXCIpWzBdLnNldEF0dHJpYnV0ZShcImhyZWZcIiwgU3RvcmFnZS5UaXRsZUxvZ28uc2Vzc2lvbigpLmdldChDb25zdHMuRkFWSUNPTl9LRVkpIHx8IEFqYXguZ2V0U2VydmljZUxvY2F0aW9uKEFwaXMuTE9HTykpO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJQYWdlSGVhZGVyKCk6IEpTWC5FbGVtZW50IHwgbnVsbCB8IHZvaWQge1xuICAgIHJldHVybiA8UGFnZUhlYWRlci8+O1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclBhZ2VCb2R5KCk6IGFueSB7XG4gICAgcmV0dXJuIDxQYWdlQm9keS8+O1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclBhZ2VGb290ZXIoKTogYW55IHtcbiAgICByZXR1cm4gPFBhZ2VGb290ZXIvPjtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5DO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3g+XG4gICAgICAgIHt0aGlzLnJlbmRlclBhZ2VIZWFkZXIoKX1cbiAgICAgICAge3RoaXMucmVuZGVyUGFnZUJvZHkoKX1cbiAgICAgICAge3RoaXMucmVuZGVyUGFnZUZvb3RlcigpfVxuICAgICAgPC9DLkJveD5cbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIGN1cnJlbnQgcGFnZSB0aXRsZVxuICAgKiBAcmV0dXJuIHtzdHJpbmd9IGRlZmF1bHQgcmV0dXJuIG51bGxcbiAgICovXG4gIGFzeW5jIGdldEh0bWxUaXRsZSgpIHtcbiAgICByZXR1cm4gYXdhaXQgQWpheC5nZXQoQXBpcy5DT01QQU5ZTkFNRSwgbnVsbCwge1xuICAgICAgaWdub3JlQXV0aDogdHJ1ZSxcbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIGdldEZhdmljb24oKSB7XG4gICAgcmV0dXJuIGF3YWl0IEFqYXguZ2V0KEFwaXMuRkFWSUNPTiwgbnVsbCwge1xuICAgICAgaWdub3JlQXV0aDogdHJ1ZSxcbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIGdldExvZ28oKSB7XG4gICAgcmV0dXJuIGF3YWl0IEFqYXguZ2V0KEFwaXMuTE9HTywgbnVsbCwge1xuICAgICAgaWdub3JlQXV0aDogdHJ1ZSxcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBpbml0IHRoZW1lXG4gICAqL1xuICBwcm90ZWN0ZWQgaW5pdFRoZW1lKHJldHJpZXZlOiBib29sZWFuID0gdHJ1ZSk6IFByb21pc2U8dm9pZD4ge1xuICAgIGNvbnN0IHRoZW1lID0gRW52cy5nZXRDdXJyZW50VGhlbWUoKTtcbiAgICBpZiAodGhlbWUpIHtcbiAgICAgIEVudnMuYXBwbGljYXRpb24oKSEuY2hhbmdlVGhlbWUodGhlbWUpO1xuICAgIH1cbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgUGFnZTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBS0E7OztBQUdBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBRkE7QUFHQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBa01BOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQTtBQUNBO0FBREE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQTtBQUNBO0FBREE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQTtBQUNBO0FBREE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUE7Ozs7OztBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7O0FBbFRBO0FBQ0E7QUFvVEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/component/page/page.tsx
