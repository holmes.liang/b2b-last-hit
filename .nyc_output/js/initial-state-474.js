

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var initialState = {
  animating: false,
  autoplaying: null,
  currentDirection: 0,
  currentLeft: null,
  currentSlide: 0,
  direction: 1,
  dragging: false,
  edgeDragged: false,
  initialized: false,
  lazyLoadedList: [],
  listHeight: null,
  listWidth: null,
  scrolling: false,
  slideCount: null,
  slideHeight: null,
  slideWidth: null,
  swipeLeft: null,
  swiped: false,
  // used by swipeEvent. differentites between touch and swipe.
  swiping: false,
  touchObject: {
    startX: 0,
    startY: 0,
    curX: 0,
    curY: 0
  },
  trackStyle: {},
  trackWidth: 0
};
var _default = initialState;
exports["default"] = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2xpY2svbGliL2luaXRpYWwtc3RhdGUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1zbGljay9saWIvaW5pdGlhbC1zdGF0ZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gdm9pZCAwO1xudmFyIGluaXRpYWxTdGF0ZSA9IHtcbiAgYW5pbWF0aW5nOiBmYWxzZSxcbiAgYXV0b3BsYXlpbmc6IG51bGwsXG4gIGN1cnJlbnREaXJlY3Rpb246IDAsXG4gIGN1cnJlbnRMZWZ0OiBudWxsLFxuICBjdXJyZW50U2xpZGU6IDAsXG4gIGRpcmVjdGlvbjogMSxcbiAgZHJhZ2dpbmc6IGZhbHNlLFxuICBlZGdlRHJhZ2dlZDogZmFsc2UsXG4gIGluaXRpYWxpemVkOiBmYWxzZSxcbiAgbGF6eUxvYWRlZExpc3Q6IFtdLFxuICBsaXN0SGVpZ2h0OiBudWxsLFxuICBsaXN0V2lkdGg6IG51bGwsXG4gIHNjcm9sbGluZzogZmFsc2UsXG4gIHNsaWRlQ291bnQ6IG51bGwsXG4gIHNsaWRlSGVpZ2h0OiBudWxsLFxuICBzbGlkZVdpZHRoOiBudWxsLFxuICBzd2lwZUxlZnQ6IG51bGwsXG4gIHN3aXBlZDogZmFsc2UsXG4gIC8vIHVzZWQgYnkgc3dpcGVFdmVudC4gZGlmZmVyZW50aXRlcyBiZXR3ZWVuIHRvdWNoIGFuZCBzd2lwZS5cbiAgc3dpcGluZzogZmFsc2UsXG4gIHRvdWNoT2JqZWN0OiB7XG4gICAgc3RhcnRYOiAwLFxuICAgIHN0YXJ0WTogMCxcbiAgICBjdXJYOiAwLFxuICAgIGN1clk6IDBcbiAgfSxcbiAgdHJhY2tTdHlsZToge30sXG4gIHRyYWNrV2lkdGg6IDBcbn07XG52YXIgX2RlZmF1bHQgPSBpbml0aWFsU3RhdGU7XG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQTVCQTtBQThCQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-slick/lib/initial-state.js
