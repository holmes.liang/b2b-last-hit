

Object.defineProperty(exports, "__esModule", {
  value: true
});

var style = function style(element, prop) {
  return typeof getComputedStyle !== 'undefined' ? getComputedStyle(element, null).getPropertyValue(prop) : element.style[prop];
};

var overflow = function overflow(element) {
  return style(element, 'overflow') + style(element, 'overflow-y') + style(element, 'overflow-x');
};

var scrollParent = function scrollParent(element) {
  if (!(element instanceof HTMLElement)) {
    return window;
  }

  var parent = element;

  while (parent) {
    if (parent === document.body || parent === document.documentElement) {
      break;
    }

    if (!parent.parentNode) {
      break;
    }

    if (/(scroll|auto)/.test(overflow(parent))) {
      return parent;
    }

    parent = parent.parentNode;
  }

  return window;
};

exports.default = scrollParent;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtbGF6eS1sb2FkL2xpYi91dGlscy9wYXJlbnRTY3JvbGwuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1sYXp5LWxvYWQvbGliL3V0aWxzL3BhcmVudFNjcm9sbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG52YXIgc3R5bGUgPSBmdW5jdGlvbiBzdHlsZShlbGVtZW50LCBwcm9wKSB7XG4gIHJldHVybiB0eXBlb2YgZ2V0Q29tcHV0ZWRTdHlsZSAhPT0gJ3VuZGVmaW5lZCcgPyBnZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQsIG51bGwpLmdldFByb3BlcnR5VmFsdWUocHJvcCkgOiBlbGVtZW50LnN0eWxlW3Byb3BdO1xufTtcblxudmFyIG92ZXJmbG93ID0gZnVuY3Rpb24gb3ZlcmZsb3coZWxlbWVudCkge1xuICByZXR1cm4gc3R5bGUoZWxlbWVudCwgJ292ZXJmbG93JykgKyBzdHlsZShlbGVtZW50LCAnb3ZlcmZsb3cteScpICsgc3R5bGUoZWxlbWVudCwgJ292ZXJmbG93LXgnKTtcbn07XG5cbnZhciBzY3JvbGxQYXJlbnQgPSBmdW5jdGlvbiBzY3JvbGxQYXJlbnQoZWxlbWVudCkge1xuICBpZiAoIShlbGVtZW50IGluc3RhbmNlb2YgSFRNTEVsZW1lbnQpKSB7XG4gICAgcmV0dXJuIHdpbmRvdztcbiAgfVxuXG4gIHZhciBwYXJlbnQgPSBlbGVtZW50O1xuXG4gIHdoaWxlIChwYXJlbnQpIHtcbiAgICBpZiAocGFyZW50ID09PSBkb2N1bWVudC5ib2R5IHx8IHBhcmVudCA9PT0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50KSB7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBpZiAoIXBhcmVudC5wYXJlbnROb2RlKSB7XG4gICAgICBicmVhaztcbiAgICB9XG5cbiAgICBpZiAoLyhzY3JvbGx8YXV0bykvLnRlc3Qob3ZlcmZsb3cocGFyZW50KSkpIHtcbiAgICAgIHJldHVybiBwYXJlbnQ7XG4gICAgfVxuXG4gICAgcGFyZW50ID0gcGFyZW50LnBhcmVudE5vZGU7XG4gIH1cblxuICByZXR1cm4gd2luZG93O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2Nyb2xsUGFyZW50OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/react-lazy-load/lib/utils/parentScroll.js
