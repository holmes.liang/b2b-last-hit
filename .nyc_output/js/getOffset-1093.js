__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getOffset; });
function getOffset(element, container) {
  var rect = element.getBoundingClientRect();

  if (rect.width || rect.height) {
    var elementContainer = container || element.parentElement;
    return {
      top: rect.top - elementContainer.clientTop,
      left: rect.left - elementContainer.clientLeft
    };
  }

  return rect;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0T2Zmc2V0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0T2Zmc2V0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGdldE9mZnNldChlbGVtZW50LCBjb250YWluZXIpIHtcbiAgdmFyIHJlY3QgPSBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICBpZiAocmVjdC53aWR0aCB8fCByZWN0LmhlaWdodCkge1xuICAgIHZhciBlbGVtZW50Q29udGFpbmVyID0gY29udGFpbmVyIHx8IGVsZW1lbnQucGFyZW50RWxlbWVudDtcbiAgICByZXR1cm4ge1xuICAgICAgdG9wOiByZWN0LnRvcCAtIGVsZW1lbnRDb250YWluZXIuY2xpZW50VG9wLFxuICAgICAgbGVmdDogcmVjdC5sZWZ0IC0gZWxlbWVudENvbnRhaW5lci5jbGllbnRMZWZ0XG4gICAgfTtcbiAgfVxuICByZXR1cm4gcmVjdDtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/getOffset.js
