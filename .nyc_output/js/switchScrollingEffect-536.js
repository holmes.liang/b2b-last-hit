__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _getScrollBarSize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./getScrollBarSize */ "./node_modules/rc-util/es/getScrollBarSize.js");
/* harmony import */ var _setStyle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./setStyle */ "./node_modules/rc-util/es/setStyle.js");



function isBodyOverflowing() {
  return document.body.scrollHeight > (window.innerHeight || document.documentElement.clientHeight) && window.innerWidth > document.body.offsetWidth;
}

var cacheStyle = {};
/* harmony default export */ __webpack_exports__["default"] = (function (close) {
  if (!isBodyOverflowing() && !close) {
    return;
  } // https://github.com/ant-design/ant-design/issues/19729


  var scrollingEffectClassName = 'ant-scrolling-effect';
  var scrollingEffectClassNameReg = new RegExp("".concat(scrollingEffectClassName), 'g');
  var bodyClassName = document.body.className;

  if (close) {
    if (!scrollingEffectClassNameReg.test(bodyClassName)) return;
    Object(_setStyle__WEBPACK_IMPORTED_MODULE_1__["default"])(cacheStyle);
    cacheStyle = {};
    document.body.className = bodyClassName.replace(scrollingEffectClassNameReg, '').trim();
    return;
  }

  var scrollBarSize = Object(_getScrollBarSize__WEBPACK_IMPORTED_MODULE_0__["default"])();

  if (scrollBarSize) {
    cacheStyle = Object(_setStyle__WEBPACK_IMPORTED_MODULE_1__["default"])({
      position: 'relative',
      width: "calc(100% - ".concat(scrollBarSize, "px)")
    });

    if (!scrollingEffectClassNameReg.test(bodyClassName)) {
      var addClassName = "".concat(bodyClassName, " ").concat(scrollingEffectClassName);
      document.body.className = addClassName.trim();
    }
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdXRpbC9lcy9zd2l0Y2hTY3JvbGxpbmdFZmZlY3QuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy11dGlsL2VzL3N3aXRjaFNjcm9sbGluZ0VmZmVjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0U2Nyb2xsQmFyU2l6ZSBmcm9tICcuL2dldFNjcm9sbEJhclNpemUnO1xuaW1wb3J0IHNldFN0eWxlIGZyb20gJy4vc2V0U3R5bGUnO1xuXG5mdW5jdGlvbiBpc0JvZHlPdmVyZmxvd2luZygpIHtcbiAgcmV0dXJuIGRvY3VtZW50LmJvZHkuc2Nyb2xsSGVpZ2h0ID4gKHdpbmRvdy5pbm5lckhlaWdodCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0KSAmJiB3aW5kb3cuaW5uZXJXaWR0aCA+IGRvY3VtZW50LmJvZHkub2Zmc2V0V2lkdGg7XG59XG5cbnZhciBjYWNoZVN0eWxlID0ge307XG5leHBvcnQgZGVmYXVsdCAoZnVuY3Rpb24gKGNsb3NlKSB7XG4gIGlmICghaXNCb2R5T3ZlcmZsb3dpbmcoKSAmJiAhY2xvc2UpIHtcbiAgICByZXR1cm47XG4gIH0gLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTk3MjlcblxuXG4gIHZhciBzY3JvbGxpbmdFZmZlY3RDbGFzc05hbWUgPSAnYW50LXNjcm9sbGluZy1lZmZlY3QnO1xuICB2YXIgc2Nyb2xsaW5nRWZmZWN0Q2xhc3NOYW1lUmVnID0gbmV3IFJlZ0V4cChcIlwiLmNvbmNhdChzY3JvbGxpbmdFZmZlY3RDbGFzc05hbWUpLCAnZycpO1xuICB2YXIgYm9keUNsYXNzTmFtZSA9IGRvY3VtZW50LmJvZHkuY2xhc3NOYW1lO1xuXG4gIGlmIChjbG9zZSkge1xuICAgIGlmICghc2Nyb2xsaW5nRWZmZWN0Q2xhc3NOYW1lUmVnLnRlc3QoYm9keUNsYXNzTmFtZSkpIHJldHVybjtcbiAgICBzZXRTdHlsZShjYWNoZVN0eWxlKTtcbiAgICBjYWNoZVN0eWxlID0ge307XG4gICAgZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBib2R5Q2xhc3NOYW1lLnJlcGxhY2Uoc2Nyb2xsaW5nRWZmZWN0Q2xhc3NOYW1lUmVnLCAnJykudHJpbSgpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBzY3JvbGxCYXJTaXplID0gZ2V0U2Nyb2xsQmFyU2l6ZSgpO1xuXG4gIGlmIChzY3JvbGxCYXJTaXplKSB7XG4gICAgY2FjaGVTdHlsZSA9IHNldFN0eWxlKHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgd2lkdGg6IFwiY2FsYygxMDAlIC0gXCIuY29uY2F0KHNjcm9sbEJhclNpemUsIFwicHgpXCIpXG4gICAgfSk7XG5cbiAgICBpZiAoIXNjcm9sbGluZ0VmZmVjdENsYXNzTmFtZVJlZy50ZXN0KGJvZHlDbGFzc05hbWUpKSB7XG4gICAgICB2YXIgYWRkQ2xhc3NOYW1lID0gXCJcIi5jb25jYXQoYm9keUNsYXNzTmFtZSwgXCIgXCIpLmNvbmNhdChzY3JvbGxpbmdFZmZlY3RDbGFzc05hbWUpO1xuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc05hbWUgPSBhZGRDbGFzc05hbWUudHJpbSgpO1xuICAgIH1cbiAgfVxufSk7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-util/es/switchScrollingEffect.js
