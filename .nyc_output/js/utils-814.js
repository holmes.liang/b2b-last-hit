

Object.defineProperty(exports, "__esModule", {
  value: true
});
var scrollbarVerticalSize;
var scrollbarHorizontalSize; // Measure scrollbar width for padding body during modal show/hide

var scrollbarMeasure = {
  position: 'absolute',
  top: '-9999px',
  width: '50px',
  height: '50px'
}; // This const is used for colgroup.col internal props. And should not provides to user.

exports.INTERNAL_COL_DEFINE = 'RC_TABLE_INTERNAL_COL_DEFINE';

function measureScrollbar(_ref) {
  var _ref$direction = _ref.direction,
      direction = _ref$direction === void 0 ? 'vertical' : _ref$direction,
      prefixCls = _ref.prefixCls;

  if (typeof document === 'undefined' || typeof window === 'undefined') {
    return 0;
  }

  var isVertical = direction === 'vertical';

  if (isVertical && scrollbarVerticalSize) {
    return scrollbarVerticalSize;
  }

  if (!isVertical && scrollbarHorizontalSize) {
    return scrollbarHorizontalSize;
  }

  var scrollDiv = document.createElement('div');
  Object.keys(scrollbarMeasure).forEach(function (scrollProp) {
    scrollDiv.style[scrollProp] = scrollbarMeasure[scrollProp];
  }); // apply hide scrollbar className ahead

  scrollDiv.className = "".concat(prefixCls, "-hide-scrollbar scroll-div-append-to-body"); // Append related overflow style

  if (isVertical) {
    scrollDiv.style.overflowY = 'scroll';
  } else {
    scrollDiv.style.overflowX = 'scroll';
  }

  document.body.appendChild(scrollDiv);
  var size = 0;

  if (isVertical) {
    size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    scrollbarVerticalSize = size;
  } else {
    size = scrollDiv.offsetHeight - scrollDiv.clientHeight;
    scrollbarHorizontalSize = size;
  }

  document.body.removeChild(scrollDiv);
  return size;
}

exports.measureScrollbar = measureScrollbar;

function debounce(func, wait, immediate) {
  var timeout;

  function debounceFunc() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var context = this; // https://fb.me/react-event-pooling

    if (args[0] && args[0].persist) {
      args[0].persist();
    }

    var later = function later() {
      timeout = null;

      if (!immediate) {
        func.apply(context, args);
      }
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  }

  debounceFunc.cancel = function cancel() {
    if (timeout) {
      clearTimeout(timeout);
      timeout = null;
    }
  };

  return debounceFunc;
}

exports.debounce = debounce;

function remove(array, item) {
  var index = array.indexOf(item);
  var front = array.slice(0, index);
  var last = array.slice(index + 1, array.length);
  return front.concat(last);
}

exports.remove = remove;
/**
 * Returns only data- and aria- key/value pairs
 * @param {object} props
 */

function getDataAndAriaProps(props) {
  return Object.keys(props).reduce(function (memo, key) {
    if (key.substr(0, 5) === 'data-' || key.substr(0, 5) === 'aria-') {
      memo[key] = props[key];
    }

    return memo;
  }, {});
}

exports.getDataAndAriaProps = getDataAndAriaProps;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvdXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10YWJsZS9lcy91dGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbnZhciBzY3JvbGxiYXJWZXJ0aWNhbFNpemU7XG52YXIgc2Nyb2xsYmFySG9yaXpvbnRhbFNpemU7IC8vIE1lYXN1cmUgc2Nyb2xsYmFyIHdpZHRoIGZvciBwYWRkaW5nIGJvZHkgZHVyaW5nIG1vZGFsIHNob3cvaGlkZVxuXG52YXIgc2Nyb2xsYmFyTWVhc3VyZSA9IHtcbiAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gIHRvcDogJy05OTk5cHgnLFxuICB3aWR0aDogJzUwcHgnLFxuICBoZWlnaHQ6ICc1MHB4J1xufTsgLy8gVGhpcyBjb25zdCBpcyB1c2VkIGZvciBjb2xncm91cC5jb2wgaW50ZXJuYWwgcHJvcHMuIEFuZCBzaG91bGQgbm90IHByb3ZpZGVzIHRvIHVzZXIuXG5cbmV4cG9ydHMuSU5URVJOQUxfQ09MX0RFRklORSA9ICdSQ19UQUJMRV9JTlRFUk5BTF9DT0xfREVGSU5FJztcblxuZnVuY3Rpb24gbWVhc3VyZVNjcm9sbGJhcihfcmVmKSB7XG4gIHZhciBfcmVmJGRpcmVjdGlvbiA9IF9yZWYuZGlyZWN0aW9uLFxuICAgICAgZGlyZWN0aW9uID0gX3JlZiRkaXJlY3Rpb24gPT09IHZvaWQgMCA/ICd2ZXJ0aWNhbCcgOiBfcmVmJGRpcmVjdGlvbixcbiAgICAgIHByZWZpeENscyA9IF9yZWYucHJlZml4Q2xzO1xuXG4gIGlmICh0eXBlb2YgZG9jdW1lbnQgPT09ICd1bmRlZmluZWQnIHx8IHR5cGVvZiB3aW5kb3cgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB2YXIgaXNWZXJ0aWNhbCA9IGRpcmVjdGlvbiA9PT0gJ3ZlcnRpY2FsJztcblxuICBpZiAoaXNWZXJ0aWNhbCAmJiBzY3JvbGxiYXJWZXJ0aWNhbFNpemUpIHtcbiAgICByZXR1cm4gc2Nyb2xsYmFyVmVydGljYWxTaXplO1xuICB9XG5cbiAgaWYgKCFpc1ZlcnRpY2FsICYmIHNjcm9sbGJhckhvcml6b250YWxTaXplKSB7XG4gICAgcmV0dXJuIHNjcm9sbGJhckhvcml6b250YWxTaXplO1xuICB9XG5cbiAgdmFyIHNjcm9sbERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICBPYmplY3Qua2V5cyhzY3JvbGxiYXJNZWFzdXJlKS5mb3JFYWNoKGZ1bmN0aW9uIChzY3JvbGxQcm9wKSB7XG4gICAgc2Nyb2xsRGl2LnN0eWxlW3Njcm9sbFByb3BdID0gc2Nyb2xsYmFyTWVhc3VyZVtzY3JvbGxQcm9wXTtcbiAgfSk7IC8vIGFwcGx5IGhpZGUgc2Nyb2xsYmFyIGNsYXNzTmFtZSBhaGVhZFxuXG4gIHNjcm9sbERpdi5jbGFzc05hbWUgPSBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWhpZGUtc2Nyb2xsYmFyIHNjcm9sbC1kaXYtYXBwZW5kLXRvLWJvZHlcIik7IC8vIEFwcGVuZCByZWxhdGVkIG92ZXJmbG93IHN0eWxlXG5cbiAgaWYgKGlzVmVydGljYWwpIHtcbiAgICBzY3JvbGxEaXYuc3R5bGUub3ZlcmZsb3dZID0gJ3Njcm9sbCc7XG4gIH0gZWxzZSB7XG4gICAgc2Nyb2xsRGl2LnN0eWxlLm92ZXJmbG93WCA9ICdzY3JvbGwnO1xuICB9XG5cbiAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JvbGxEaXYpO1xuICB2YXIgc2l6ZSA9IDA7XG5cbiAgaWYgKGlzVmVydGljYWwpIHtcbiAgICBzaXplID0gc2Nyb2xsRGl2Lm9mZnNldFdpZHRoIC0gc2Nyb2xsRGl2LmNsaWVudFdpZHRoO1xuICAgIHNjcm9sbGJhclZlcnRpY2FsU2l6ZSA9IHNpemU7XG4gIH0gZWxzZSB7XG4gICAgc2l6ZSA9IHNjcm9sbERpdi5vZmZzZXRIZWlnaHQgLSBzY3JvbGxEaXYuY2xpZW50SGVpZ2h0O1xuICAgIHNjcm9sbGJhckhvcml6b250YWxTaXplID0gc2l6ZTtcbiAgfVxuXG4gIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoc2Nyb2xsRGl2KTtcbiAgcmV0dXJuIHNpemU7XG59XG5cbmV4cG9ydHMubWVhc3VyZVNjcm9sbGJhciA9IG1lYXN1cmVTY3JvbGxiYXI7XG5cbmZ1bmN0aW9uIGRlYm91bmNlKGZ1bmMsIHdhaXQsIGltbWVkaWF0ZSkge1xuICB2YXIgdGltZW91dDtcblxuICBmdW5jdGlvbiBkZWJvdW5jZUZ1bmMoKSB7XG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHZhciBjb250ZXh0ID0gdGhpczsgLy8gaHR0cHM6Ly9mYi5tZS9yZWFjdC1ldmVudC1wb29saW5nXG5cbiAgICBpZiAoYXJnc1swXSAmJiBhcmdzWzBdLnBlcnNpc3QpIHtcbiAgICAgIGFyZ3NbMF0ucGVyc2lzdCgpO1xuICAgIH1cblxuICAgIHZhciBsYXRlciA9IGZ1bmN0aW9uIGxhdGVyKCkge1xuICAgICAgdGltZW91dCA9IG51bGw7XG5cbiAgICAgIGlmICghaW1tZWRpYXRlKSB7XG4gICAgICAgIGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIHZhciBjYWxsTm93ID0gaW1tZWRpYXRlICYmICF0aW1lb3V0O1xuICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcbiAgICB0aW1lb3V0ID0gc2V0VGltZW91dChsYXRlciwgd2FpdCk7XG5cbiAgICBpZiAoY2FsbE5vdykge1xuICAgICAgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKTtcbiAgICB9XG4gIH1cblxuICBkZWJvdW5jZUZ1bmMuY2FuY2VsID0gZnVuY3Rpb24gY2FuY2VsKCkge1xuICAgIGlmICh0aW1lb3V0KSB7XG4gICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICB0aW1lb3V0ID0gbnVsbDtcbiAgICB9XG4gIH07XG5cbiAgcmV0dXJuIGRlYm91bmNlRnVuYztcbn1cblxuZXhwb3J0cy5kZWJvdW5jZSA9IGRlYm91bmNlO1xuXG5mdW5jdGlvbiByZW1vdmUoYXJyYXksIGl0ZW0pIHtcbiAgdmFyIGluZGV4ID0gYXJyYXkuaW5kZXhPZihpdGVtKTtcbiAgdmFyIGZyb250ID0gYXJyYXkuc2xpY2UoMCwgaW5kZXgpO1xuICB2YXIgbGFzdCA9IGFycmF5LnNsaWNlKGluZGV4ICsgMSwgYXJyYXkubGVuZ3RoKTtcbiAgcmV0dXJuIGZyb250LmNvbmNhdChsYXN0KTtcbn1cblxuZXhwb3J0cy5yZW1vdmUgPSByZW1vdmU7XG4vKipcbiAqIFJldHVybnMgb25seSBkYXRhLSBhbmQgYXJpYS0ga2V5L3ZhbHVlIHBhaXJzXG4gKiBAcGFyYW0ge29iamVjdH0gcHJvcHNcbiAqL1xuXG5mdW5jdGlvbiBnZXREYXRhQW5kQXJpYVByb3BzKHByb3BzKSB7XG4gIHJldHVybiBPYmplY3Qua2V5cyhwcm9wcykucmVkdWNlKGZ1bmN0aW9uIChtZW1vLCBrZXkpIHtcbiAgICBpZiAoa2V5LnN1YnN0cigwLCA1KSA9PT0gJ2RhdGEtJyB8fCBrZXkuc3Vic3RyKDAsIDUpID09PSAnYXJpYS0nKSB7XG4gICAgICBtZW1vW2tleV0gPSBwcm9wc1trZXldO1xuICAgIH1cblxuICAgIHJldHVybiBtZW1vO1xuICB9LCB7fSk7XG59XG5cbmV4cG9ydHMuZ2V0RGF0YUFuZEFyaWFQcm9wcyA9IGdldERhdGFBbmRBcmlhUHJvcHM7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/utils.js
