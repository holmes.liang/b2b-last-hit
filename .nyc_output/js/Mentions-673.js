__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _KeywordTrigger__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./KeywordTrigger */ "./node_modules/rc-mentions/es/KeywordTrigger.js");
/* harmony import */ var _MentionsContext__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./MentionsContext */ "./node_modules/rc-mentions/es/MentionsContext.js");
/* harmony import */ var _Option__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Option */ "./node_modules/rc-mentions/es/Option.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./util */ "./node_modules/rc-mentions/es/util.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}











var Mentions =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Mentions, _React$Component);

  function Mentions(props) {
    var _this;

    _classCallCheck(this, Mentions);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Mentions).call(this, props));
    _this.focusId = undefined;

    _this.triggerChange = function (value) {
      var onChange = _this.props.onChange;

      if (!('value' in _this.props)) {
        _this.setState({
          value: value
        });
      }

      if (onChange) {
        onChange(value);
      }
    };

    _this.onChange = function (_ref) {
      var value = _ref.target.value;

      _this.triggerChange(value);
    }; // Check if hit the measure keyword


    _this.onKeyDown = function (event) {
      var which = event.which;
      var _this$state = _this.state,
          activeIndex = _this$state.activeIndex,
          measuring = _this$state.measuring; // Skip if not measuring

      if (!measuring) {
        return;
      }

      if (which === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP || which === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].DOWN) {
        // Control arrow function
        var optionLen = _this.getOptions().length;

        var offset = which === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP ? -1 : 1;
        var newActiveIndex = (activeIndex + offset + optionLen) % optionLen;

        _this.setState({
          activeIndex: newActiveIndex
        });

        event.preventDefault();
      } else if (which === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].ESC) {
        _this.stopMeasure();
      } else if (which === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].ENTER) {
        // Measure hit
        var option = _this.getOptions()[activeIndex];

        _this.selectOption(option);

        event.preventDefault();
      }
    };
    /**
     * When to start measure:
     * 1. When user press `prefix`
     * 2. When measureText !== prevMeasureText
     *  - If measure hit
     *  - If measuring
     *
     * When to stop measure:
     * 1. Selection is out of range
     * 2. Contains `space`
     * 3. ESC or select one
     */


    _this.onKeyUp = function (event) {
      var key = event.key,
          which = event.which;
      var _this$state2 = _this.state,
          prevMeasureText = _this$state2.measureText,
          measuring = _this$state2.measuring;
      var _this$props = _this.props,
          _this$props$prefix = _this$props.prefix,
          prefix = _this$props$prefix === void 0 ? '' : _this$props$prefix,
          onSearch = _this$props.onSearch,
          validateSearch = _this$props.validateSearch;
      var target = event.target;
      var selectionStartText = Object(_util__WEBPACK_IMPORTED_MODULE_8__["getBeforeSelectionText"])(target);

      var _getLastMeasureIndex = Object(_util__WEBPACK_IMPORTED_MODULE_8__["getLastMeasureIndex"])(selectionStartText, prefix),
          measureIndex = _getLastMeasureIndex.location,
          measurePrefix = _getLastMeasureIndex.prefix; // Skip if match the white key list


      if ([rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].ESC, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].DOWN, rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].ENTER].indexOf(which) !== -1) {
        return;
      }

      if (measureIndex !== -1) {
        var measureText = selectionStartText.slice(measureIndex + measurePrefix.length);
        var validateMeasure = validateSearch(measureText, _this.props);
        var matchOption = !!_this.getOptions(measureText).length;

        if (validateMeasure) {
          if (key === measurePrefix || measuring || measureText !== prevMeasureText && matchOption) {
            _this.startMeasure(measureText, measurePrefix, measureIndex);
          }
        } else if (measuring) {
          // Stop if measureText is invalidate
          _this.stopMeasure();
        }
        /**
         * We will trigger `onSearch` to developer since they may use for async update.
         * If met `space` means user finished searching.
         */


        if (onSearch && validateMeasure) {
          onSearch(measureText, measurePrefix);
        }
      } else if (measuring) {
        _this.stopMeasure();
      }
    };

    _this.onInputFocus = function (event) {
      _this.onFocus(event);
    };

    _this.onInputBlur = function (event) {
      _this.onBlur(event);
    };

    _this.onDropdownFocus = function () {
      _this.onFocus();
    };

    _this.onDropdownBlur = function () {
      _this.onBlur();
    };

    _this.onFocus = function (event) {
      window.clearTimeout(_this.focusId);
      var isFocus = _this.state.isFocus;
      var onFocus = _this.props.onFocus;

      if (!isFocus && event && onFocus) {
        onFocus(event);
      }

      _this.setState({
        isFocus: true
      });
    };

    _this.onBlur = function (event) {
      _this.focusId = window.setTimeout(function () {
        var onBlur = _this.props.onBlur;

        _this.setState({
          isFocus: false
        });

        _this.stopMeasure();

        if (onBlur) {
          onBlur(event);
        }
      }, 0);
    };

    _this.selectOption = function (option) {
      var _this$state3 = _this.state,
          value = _this$state3.value,
          measureLocation = _this$state3.measureLocation,
          measurePrefix = _this$state3.measurePrefix;
      var _this$props2 = _this.props,
          split = _this$props2.split,
          onSelect = _this$props2.onSelect;
      var _option$value = option.value,
          mentionValue = _option$value === void 0 ? '' : _option$value;

      var _replaceWithMeasure = Object(_util__WEBPACK_IMPORTED_MODULE_8__["replaceWithMeasure"])(value, {
        measureLocation: measureLocation,
        targetText: mentionValue,
        prefix: measurePrefix,
        selectionStart: _this.textarea.selectionStart,
        split: split
      }),
          text = _replaceWithMeasure.text,
          selectionLocation = _replaceWithMeasure.selectionLocation;

      _this.triggerChange(text);

      _this.stopMeasure(function () {
        // We need restore the selection position
        Object(_util__WEBPACK_IMPORTED_MODULE_8__["setInputSelection"])(_this.textarea, selectionLocation);
      });

      if (onSelect) {
        onSelect(option, measurePrefix);
      }
    };

    _this.setActiveIndex = function (activeIndex) {
      _this.setState({
        activeIndex: activeIndex
      });
    };

    _this.setTextAreaRef = function (element) {
      _this.textarea = element;
    };

    _this.setMeasureRef = function (element) {
      _this.measure = element;
    };

    _this.getOptions = function (measureText) {
      var targetMeasureText = measureText || _this.state.measureText || '';
      var _this$props3 = _this.props,
          children = _this$props3.children,
          filterOption = _this$props3.filterOption;
      var list = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_1__["default"])(children).map(function (_ref2) {
        var props = _ref2.props;
        return props;
      }).filter(function (option) {
        /** Return all result if `filterOption` is false. */
        if (filterOption === false) {
          return true;
        }

        return filterOption(targetMeasureText, option);
      });
      return list;
    };

    _this.state = {
      value: props.defaultValue || props.value || '',
      measuring: false,
      measureLocation: 0,
      measureText: null,
      measurePrefix: '',
      activeIndex: 0,
      isFocus: false
    };
    return _this;
  }

  _createClass(Mentions, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var measuring = this.state.measuring; // Sync measure div top with textarea for rc-trigger usage

      if (measuring) {
        this.measure.scrollTop = this.textarea.scrollTop;
      }
    }
  }, {
    key: "startMeasure",
    value: function startMeasure(measureText, measurePrefix, measureLocation) {
      this.setState({
        measuring: true,
        measureText: measureText,
        measurePrefix: measurePrefix,
        measureLocation: measureLocation,
        activeIndex: 0
      });
    }
  }, {
    key: "stopMeasure",
    value: function stopMeasure(callback) {
      this.setState({
        measuring: false,
        measureLocation: 0,
        measureText: null
      }, callback);
    }
  }, {
    key: "focus",
    value: function focus() {
      this.textarea.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.textarea.blur();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state4 = this.state,
          value = _this$state4.value,
          measureLocation = _this$state4.measureLocation,
          measurePrefix = _this$state4.measurePrefix,
          measuring = _this$state4.measuring,
          activeIndex = _this$state4.activeIndex;

      var _this$props4 = this.props,
          prefixCls = _this$props4.prefixCls,
          placement = _this$props4.placement,
          transitionName = _this$props4.transitionName,
          className = _this$props4.className,
          style = _this$props4.style,
          autoFocus = _this$props4.autoFocus,
          notFoundContent = _this$props4.notFoundContent,
          getPopupContainer = _this$props4.getPopupContainer,
          restProps = _objectWithoutProperties(_this$props4, ["prefixCls", "placement", "transitionName", "className", "style", "autoFocus", "notFoundContent", "getPopupContainer"]);

      var inputProps = Object(_util__WEBPACK_IMPORTED_MODULE_8__["omit"])(restProps, 'value', 'defaultValue', 'prefix', 'split', 'children', 'validateSearch', 'filterOption', 'onSelect', 'onSearch');
      var options = measuring ? this.getOptions() : [];
      return react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", {
        className: classnames__WEBPACK_IMPORTED_MODULE_0___default()(prefixCls, className),
        style: style
      }, react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("textarea", Object.assign({
        autoFocus: autoFocus,
        ref: this.setTextAreaRef,
        value: value
      }, inputProps, {
        onChange: this.onChange,
        onKeyDown: this.onKeyDown,
        onKeyUp: this.onKeyUp,
        onFocus: this.onInputFocus,
        onBlur: this.onInputBlur
      })), measuring && react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", {
        ref: this.setMeasureRef,
        className: "".concat(prefixCls, "-measure")
      }, value.slice(0, measureLocation), react__WEBPACK_IMPORTED_MODULE_3__["createElement"](_MentionsContext__WEBPACK_IMPORTED_MODULE_6__["MentionsContextProvider"], {
        value: {
          notFoundContent: notFoundContent,
          activeIndex: activeIndex,
          setActiveIndex: this.setActiveIndex,
          selectOption: this.selectOption,
          onFocus: this.onDropdownFocus,
          onBlur: this.onDropdownBlur
        }
      }, react__WEBPACK_IMPORTED_MODULE_3__["createElement"](_KeywordTrigger__WEBPACK_IMPORTED_MODULE_5__["default"], {
        prefixCls: prefixCls,
        transitionName: transitionName,
        placement: placement,
        options: options,
        visible: true,
        getPopupContainer: getPopupContainer
      }, react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("span", null, measurePrefix))), value.slice(measureLocation + measurePrefix.length)));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, prevState) {
      var newState = {};

      if ('value' in props && props.value !== prevState.value) {
        newState.value = props.value || '';
      }

      return newState;
    }
  }]);

  return Mentions;
}(react__WEBPACK_IMPORTED_MODULE_3__["Component"]);

Mentions.Option = _Option__WEBPACK_IMPORTED_MODULE_7__["default"];
Mentions.defaultProps = {
  prefixCls: 'rc-mentions',
  prefix: '@',
  split: ' ',
  validateSearch: _util__WEBPACK_IMPORTED_MODULE_8__["validateSearch"],
  filterOption: _util__WEBPACK_IMPORTED_MODULE_8__["filterOption"],
  notFoundContent: 'Not Found',
  rows: 1
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__["polyfill"])(Mentions);
/* harmony default export */ __webpack_exports__["default"] = (Mentions);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudGlvbnMvZXMvTWVudGlvbnMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1tZW50aW9ucy9lcy9NZW50aW9ucy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9OyB9IGVsc2UgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07IH0gcmV0dXJuIF90eXBlb2Yob2JqKTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoc291cmNlLCBleGNsdWRlZCkgeyBpZiAoc291cmNlID09IG51bGwpIHJldHVybiB7fTsgdmFyIHRhcmdldCA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllc0xvb3NlKHNvdXJjZSwgZXhjbHVkZWQpOyB2YXIga2V5LCBpOyBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykgeyB2YXIgc291cmNlU3ltYm9sS2V5cyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMoc291cmNlKTsgZm9yIChpID0gMDsgaSA8IHNvdXJjZVN5bWJvbEtleXMubGVuZ3RoOyBpKyspIHsga2V5ID0gc291cmNlU3ltYm9sS2V5c1tpXTsgaWYgKGV4Y2x1ZGVkLmluZGV4T2Yoa2V5KSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwoc291cmNlLCBrZXkpKSBjb250aW51ZTsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2Uoc291cmNlLCBleGNsdWRlZCkgeyBpZiAoc291cmNlID09IG51bGwpIHJldHVybiB7fTsgdmFyIHRhcmdldCA9IHt9OyB2YXIgc291cmNlS2V5cyA9IE9iamVjdC5rZXlzKHNvdXJjZSk7IHZhciBrZXksIGk7IGZvciAoaSA9IDA7IGkgPCBzb3VyY2VLZXlzLmxlbmd0aDsgaSsrKSB7IGtleSA9IHNvdXJjZUtleXNbaV07IGlmIChleGNsdWRlZC5pbmRleE9mKGtleSkgPj0gMCkgY29udGludWU7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKGNhbGwgJiYgKF90eXBlb2YoY2FsbCkgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikpIHsgcmV0dXJuIGNhbGw7IH0gcmV0dXJuIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZik7IH1cblxuZnVuY3Rpb24gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKSB7IGlmIChzZWxmID09PSB2b2lkIDApIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IF9nZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5nZXRQcm90b3R5cGVPZiA6IGZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7IH07IHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uXCIpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIF9zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcyk7IH1cblxuZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgX3NldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8IGZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IG8uX19wcm90b19fID0gcDsgcmV0dXJuIG87IH07IHJldHVybiBfc2V0UHJvdG90eXBlT2YobywgcCk7IH1cblxuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgdG9BcnJheSBmcm9tIFwicmMtdXRpbC9lcy9DaGlsZHJlbi90b0FycmF5XCI7XG5pbXBvcnQgS2V5Q29kZSBmcm9tIFwicmMtdXRpbC9lcy9LZXlDb2RlXCI7XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCBLZXl3b3JkVHJpZ2dlciBmcm9tICcuL0tleXdvcmRUcmlnZ2VyJztcbmltcG9ydCB7IE1lbnRpb25zQ29udGV4dFByb3ZpZGVyIH0gZnJvbSAnLi9NZW50aW9uc0NvbnRleHQnO1xuaW1wb3J0IE9wdGlvbiBmcm9tICcuL09wdGlvbic7XG5pbXBvcnQgeyBmaWx0ZXJPcHRpb24gYXMgZGVmYXVsdEZpbHRlck9wdGlvbiwgZ2V0QmVmb3JlU2VsZWN0aW9uVGV4dCwgZ2V0TGFzdE1lYXN1cmVJbmRleCwgb21pdCwgcmVwbGFjZVdpdGhNZWFzdXJlLCBzZXRJbnB1dFNlbGVjdGlvbiwgdmFsaWRhdGVTZWFyY2ggYXMgZGVmYXVsdFZhbGlkYXRlU2VhcmNoIH0gZnJvbSAnLi91dGlsJztcblxudmFyIE1lbnRpb25zID1cbi8qI19fUFVSRV9fKi9cbmZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhNZW50aW9ucywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTWVudGlvbnMocHJvcHMpIHtcbiAgICB2YXIgX3RoaXM7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTWVudGlvbnMpO1xuXG4gICAgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfZ2V0UHJvdG90eXBlT2YoTWVudGlvbnMpLmNhbGwodGhpcywgcHJvcHMpKTtcbiAgICBfdGhpcy5mb2N1c0lkID0gdW5kZWZpbmVkO1xuXG4gICAgX3RoaXMudHJpZ2dlckNoYW5nZSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgdmFyIG9uQ2hhbmdlID0gX3RoaXMucHJvcHMub25DaGFuZ2U7XG5cbiAgICAgIGlmICghKCd2YWx1ZScgaW4gX3RoaXMucHJvcHMpKSB7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICB2YWx1ZTogdmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGlmIChvbkNoYW5nZSkge1xuICAgICAgICBvbkNoYW5nZSh2YWx1ZSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uQ2hhbmdlID0gZnVuY3Rpb24gKF9yZWYpIHtcbiAgICAgIHZhciB2YWx1ZSA9IF9yZWYudGFyZ2V0LnZhbHVlO1xuXG4gICAgICBfdGhpcy50cmlnZ2VyQ2hhbmdlKHZhbHVlKTtcbiAgICB9OyAvLyBDaGVjayBpZiBoaXQgdGhlIG1lYXN1cmUga2V5d29yZFxuXG5cbiAgICBfdGhpcy5vbktleURvd24gPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciB3aGljaCA9IGV2ZW50LndoaWNoO1xuICAgICAgdmFyIF90aGlzJHN0YXRlID0gX3RoaXMuc3RhdGUsXG4gICAgICAgICAgYWN0aXZlSW5kZXggPSBfdGhpcyRzdGF0ZS5hY3RpdmVJbmRleCxcbiAgICAgICAgICBtZWFzdXJpbmcgPSBfdGhpcyRzdGF0ZS5tZWFzdXJpbmc7IC8vIFNraXAgaWYgbm90IG1lYXN1cmluZ1xuXG4gICAgICBpZiAoIW1lYXN1cmluZykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmICh3aGljaCA9PT0gS2V5Q29kZS5VUCB8fCB3aGljaCA9PT0gS2V5Q29kZS5ET1dOKSB7XG4gICAgICAgIC8vIENvbnRyb2wgYXJyb3cgZnVuY3Rpb25cbiAgICAgICAgdmFyIG9wdGlvbkxlbiA9IF90aGlzLmdldE9wdGlvbnMoKS5sZW5ndGg7XG5cbiAgICAgICAgdmFyIG9mZnNldCA9IHdoaWNoID09PSBLZXlDb2RlLlVQID8gLTEgOiAxO1xuICAgICAgICB2YXIgbmV3QWN0aXZlSW5kZXggPSAoYWN0aXZlSW5kZXggKyBvZmZzZXQgKyBvcHRpb25MZW4pICUgb3B0aW9uTGVuO1xuXG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBhY3RpdmVJbmRleDogbmV3QWN0aXZlSW5kZXhcbiAgICAgICAgfSk7XG5cbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIH0gZWxzZSBpZiAod2hpY2ggPT09IEtleUNvZGUuRVNDKSB7XG4gICAgICAgIF90aGlzLnN0b3BNZWFzdXJlKCk7XG4gICAgICB9IGVsc2UgaWYgKHdoaWNoID09PSBLZXlDb2RlLkVOVEVSKSB7XG4gICAgICAgIC8vIE1lYXN1cmUgaGl0XG4gICAgICAgIHZhciBvcHRpb24gPSBfdGhpcy5nZXRPcHRpb25zKClbYWN0aXZlSW5kZXhdO1xuXG4gICAgICAgIF90aGlzLnNlbGVjdE9wdGlvbihvcHRpb24pO1xuXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBXaGVuIHRvIHN0YXJ0IG1lYXN1cmU6XG4gICAgICogMS4gV2hlbiB1c2VyIHByZXNzIGBwcmVmaXhgXG4gICAgICogMi4gV2hlbiBtZWFzdXJlVGV4dCAhPT0gcHJldk1lYXN1cmVUZXh0XG4gICAgICogIC0gSWYgbWVhc3VyZSBoaXRcbiAgICAgKiAgLSBJZiBtZWFzdXJpbmdcbiAgICAgKlxuICAgICAqIFdoZW4gdG8gc3RvcCBtZWFzdXJlOlxuICAgICAqIDEuIFNlbGVjdGlvbiBpcyBvdXQgb2YgcmFuZ2VcbiAgICAgKiAyLiBDb250YWlucyBgc3BhY2VgXG4gICAgICogMy4gRVNDIG9yIHNlbGVjdCBvbmVcbiAgICAgKi9cblxuXG4gICAgX3RoaXMub25LZXlVcCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIGtleSA9IGV2ZW50LmtleSxcbiAgICAgICAgICB3aGljaCA9IGV2ZW50LndoaWNoO1xuICAgICAgdmFyIF90aGlzJHN0YXRlMiA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHByZXZNZWFzdXJlVGV4dCA9IF90aGlzJHN0YXRlMi5tZWFzdXJlVGV4dCxcbiAgICAgICAgICBtZWFzdXJpbmcgPSBfdGhpcyRzdGF0ZTIubWVhc3VyaW5nO1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgX3RoaXMkcHJvcHMkcHJlZml4ID0gX3RoaXMkcHJvcHMucHJlZml4LFxuICAgICAgICAgIHByZWZpeCA9IF90aGlzJHByb3BzJHByZWZpeCA9PT0gdm9pZCAwID8gJycgOiBfdGhpcyRwcm9wcyRwcmVmaXgsXG4gICAgICAgICAgb25TZWFyY2ggPSBfdGhpcyRwcm9wcy5vblNlYXJjaCxcbiAgICAgICAgICB2YWxpZGF0ZVNlYXJjaCA9IF90aGlzJHByb3BzLnZhbGlkYXRlU2VhcmNoO1xuICAgICAgdmFyIHRhcmdldCA9IGV2ZW50LnRhcmdldDtcbiAgICAgIHZhciBzZWxlY3Rpb25TdGFydFRleHQgPSBnZXRCZWZvcmVTZWxlY3Rpb25UZXh0KHRhcmdldCk7XG5cbiAgICAgIHZhciBfZ2V0TGFzdE1lYXN1cmVJbmRleCA9IGdldExhc3RNZWFzdXJlSW5kZXgoc2VsZWN0aW9uU3RhcnRUZXh0LCBwcmVmaXgpLFxuICAgICAgICAgIG1lYXN1cmVJbmRleCA9IF9nZXRMYXN0TWVhc3VyZUluZGV4LmxvY2F0aW9uLFxuICAgICAgICAgIG1lYXN1cmVQcmVmaXggPSBfZ2V0TGFzdE1lYXN1cmVJbmRleC5wcmVmaXg7IC8vIFNraXAgaWYgbWF0Y2ggdGhlIHdoaXRlIGtleSBsaXN0XG5cblxuICAgICAgaWYgKFtLZXlDb2RlLkVTQywgS2V5Q29kZS5VUCwgS2V5Q29kZS5ET1dOLCBLZXlDb2RlLkVOVEVSXS5pbmRleE9mKHdoaWNoKSAhPT0gLTEpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAobWVhc3VyZUluZGV4ICE9PSAtMSkge1xuICAgICAgICB2YXIgbWVhc3VyZVRleHQgPSBzZWxlY3Rpb25TdGFydFRleHQuc2xpY2UobWVhc3VyZUluZGV4ICsgbWVhc3VyZVByZWZpeC5sZW5ndGgpO1xuICAgICAgICB2YXIgdmFsaWRhdGVNZWFzdXJlID0gdmFsaWRhdGVTZWFyY2gobWVhc3VyZVRleHQsIF90aGlzLnByb3BzKTtcbiAgICAgICAgdmFyIG1hdGNoT3B0aW9uID0gISFfdGhpcy5nZXRPcHRpb25zKG1lYXN1cmVUZXh0KS5sZW5ndGg7XG5cbiAgICAgICAgaWYgKHZhbGlkYXRlTWVhc3VyZSkge1xuICAgICAgICAgIGlmIChrZXkgPT09IG1lYXN1cmVQcmVmaXggfHwgbWVhc3VyaW5nIHx8IG1lYXN1cmVUZXh0ICE9PSBwcmV2TWVhc3VyZVRleHQgJiYgbWF0Y2hPcHRpb24pIHtcbiAgICAgICAgICAgIF90aGlzLnN0YXJ0TWVhc3VyZShtZWFzdXJlVGV4dCwgbWVhc3VyZVByZWZpeCwgbWVhc3VyZUluZGV4KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAobWVhc3VyaW5nKSB7XG4gICAgICAgICAgLy8gU3RvcCBpZiBtZWFzdXJlVGV4dCBpcyBpbnZhbGlkYXRlXG4gICAgICAgICAgX3RoaXMuc3RvcE1lYXN1cmUoKTtcbiAgICAgICAgfVxuICAgICAgICAvKipcbiAgICAgICAgICogV2Ugd2lsbCB0cmlnZ2VyIGBvblNlYXJjaGAgdG8gZGV2ZWxvcGVyIHNpbmNlIHRoZXkgbWF5IHVzZSBmb3IgYXN5bmMgdXBkYXRlLlxuICAgICAgICAgKiBJZiBtZXQgYHNwYWNlYCBtZWFucyB1c2VyIGZpbmlzaGVkIHNlYXJjaGluZy5cbiAgICAgICAgICovXG5cblxuICAgICAgICBpZiAob25TZWFyY2ggJiYgdmFsaWRhdGVNZWFzdXJlKSB7XG4gICAgICAgICAgb25TZWFyY2gobWVhc3VyZVRleHQsIG1lYXN1cmVQcmVmaXgpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKG1lYXN1cmluZykge1xuICAgICAgICBfdGhpcy5zdG9wTWVhc3VyZSgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbklucHV0Rm9jdXMgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIF90aGlzLm9uRm9jdXMoZXZlbnQpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbklucHV0Qmx1ciA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgX3RoaXMub25CbHVyKGV2ZW50KTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25Ecm9wZG93bkZvY3VzID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMub25Gb2N1cygpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkRyb3Bkb3duQmx1ciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLm9uQmx1cigpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkZvY3VzID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KF90aGlzLmZvY3VzSWQpO1xuICAgICAgdmFyIGlzRm9jdXMgPSBfdGhpcy5zdGF0ZS5pc0ZvY3VzO1xuICAgICAgdmFyIG9uRm9jdXMgPSBfdGhpcy5wcm9wcy5vbkZvY3VzO1xuXG4gICAgICBpZiAoIWlzRm9jdXMgJiYgZXZlbnQgJiYgb25Gb2N1cykge1xuICAgICAgICBvbkZvY3VzKGV2ZW50KTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBpc0ZvY3VzOiB0cnVlXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25CbHVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBfdGhpcy5mb2N1c0lkID0gd2luZG93LnNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgb25CbHVyID0gX3RoaXMucHJvcHMub25CbHVyO1xuXG4gICAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBpc0ZvY3VzOiBmYWxzZVxuICAgICAgICB9KTtcblxuICAgICAgICBfdGhpcy5zdG9wTWVhc3VyZSgpO1xuXG4gICAgICAgIGlmIChvbkJsdXIpIHtcbiAgICAgICAgICBvbkJsdXIoZXZlbnQpO1xuICAgICAgICB9XG4gICAgICB9LCAwKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc2VsZWN0T3B0aW9uID0gZnVuY3Rpb24gKG9wdGlvbikge1xuICAgICAgdmFyIF90aGlzJHN0YXRlMyA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgIHZhbHVlID0gX3RoaXMkc3RhdGUzLnZhbHVlLFxuICAgICAgICAgIG1lYXN1cmVMb2NhdGlvbiA9IF90aGlzJHN0YXRlMy5tZWFzdXJlTG9jYXRpb24sXG4gICAgICAgICAgbWVhc3VyZVByZWZpeCA9IF90aGlzJHN0YXRlMy5tZWFzdXJlUHJlZml4O1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHNwbGl0ID0gX3RoaXMkcHJvcHMyLnNwbGl0LFxuICAgICAgICAgIG9uU2VsZWN0ID0gX3RoaXMkcHJvcHMyLm9uU2VsZWN0O1xuICAgICAgdmFyIF9vcHRpb24kdmFsdWUgPSBvcHRpb24udmFsdWUsXG4gICAgICAgICAgbWVudGlvblZhbHVlID0gX29wdGlvbiR2YWx1ZSA9PT0gdm9pZCAwID8gJycgOiBfb3B0aW9uJHZhbHVlO1xuXG4gICAgICB2YXIgX3JlcGxhY2VXaXRoTWVhc3VyZSA9IHJlcGxhY2VXaXRoTWVhc3VyZSh2YWx1ZSwge1xuICAgICAgICBtZWFzdXJlTG9jYXRpb246IG1lYXN1cmVMb2NhdGlvbixcbiAgICAgICAgdGFyZ2V0VGV4dDogbWVudGlvblZhbHVlLFxuICAgICAgICBwcmVmaXg6IG1lYXN1cmVQcmVmaXgsXG4gICAgICAgIHNlbGVjdGlvblN0YXJ0OiBfdGhpcy50ZXh0YXJlYS5zZWxlY3Rpb25TdGFydCxcbiAgICAgICAgc3BsaXQ6IHNwbGl0XG4gICAgICB9KSxcbiAgICAgICAgICB0ZXh0ID0gX3JlcGxhY2VXaXRoTWVhc3VyZS50ZXh0LFxuICAgICAgICAgIHNlbGVjdGlvbkxvY2F0aW9uID0gX3JlcGxhY2VXaXRoTWVhc3VyZS5zZWxlY3Rpb25Mb2NhdGlvbjtcblxuICAgICAgX3RoaXMudHJpZ2dlckNoYW5nZSh0ZXh0KTtcblxuICAgICAgX3RoaXMuc3RvcE1lYXN1cmUoZnVuY3Rpb24gKCkge1xuICAgICAgICAvLyBXZSBuZWVkIHJlc3RvcmUgdGhlIHNlbGVjdGlvbiBwb3NpdGlvblxuICAgICAgICBzZXRJbnB1dFNlbGVjdGlvbihfdGhpcy50ZXh0YXJlYSwgc2VsZWN0aW9uTG9jYXRpb24pO1xuICAgICAgfSk7XG5cbiAgICAgIGlmIChvblNlbGVjdCkge1xuICAgICAgICBvblNlbGVjdChvcHRpb24sIG1lYXN1cmVQcmVmaXgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5zZXRBY3RpdmVJbmRleCA9IGZ1bmN0aW9uIChhY3RpdmVJbmRleCkge1xuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhY3RpdmVJbmRleDogYWN0aXZlSW5kZXhcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5zZXRUZXh0QXJlYVJlZiA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XG4gICAgICBfdGhpcy50ZXh0YXJlYSA9IGVsZW1lbnQ7XG4gICAgfTtcblxuICAgIF90aGlzLnNldE1lYXN1cmVSZWYgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgX3RoaXMubWVhc3VyZSA9IGVsZW1lbnQ7XG4gICAgfTtcblxuICAgIF90aGlzLmdldE9wdGlvbnMgPSBmdW5jdGlvbiAobWVhc3VyZVRleHQpIHtcbiAgICAgIHZhciB0YXJnZXRNZWFzdXJlVGV4dCA9IG1lYXN1cmVUZXh0IHx8IF90aGlzLnN0YXRlLm1lYXN1cmVUZXh0IHx8ICcnO1xuICAgICAgdmFyIF90aGlzJHByb3BzMyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3RoaXMkcHJvcHMzLmNoaWxkcmVuLFxuICAgICAgICAgIGZpbHRlck9wdGlvbiA9IF90aGlzJHByb3BzMy5maWx0ZXJPcHRpb247XG4gICAgICB2YXIgbGlzdCA9IHRvQXJyYXkoY2hpbGRyZW4pLm1hcChmdW5jdGlvbiAoX3JlZjIpIHtcbiAgICAgICAgdmFyIHByb3BzID0gX3JlZjIucHJvcHM7XG4gICAgICAgIHJldHVybiBwcm9wcztcbiAgICAgIH0pLmZpbHRlcihmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIC8qKiBSZXR1cm4gYWxsIHJlc3VsdCBpZiBgZmlsdGVyT3B0aW9uYCBpcyBmYWxzZS4gKi9cbiAgICAgICAgaWYgKGZpbHRlck9wdGlvbiA9PT0gZmFsc2UpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmaWx0ZXJPcHRpb24odGFyZ2V0TWVhc3VyZVRleHQsIG9wdGlvbik7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBsaXN0O1xuICAgIH07XG5cbiAgICBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHZhbHVlOiBwcm9wcy5kZWZhdWx0VmFsdWUgfHwgcHJvcHMudmFsdWUgfHwgJycsXG4gICAgICBtZWFzdXJpbmc6IGZhbHNlLFxuICAgICAgbWVhc3VyZUxvY2F0aW9uOiAwLFxuICAgICAgbWVhc3VyZVRleHQ6IG51bGwsXG4gICAgICBtZWFzdXJlUHJlZml4OiAnJyxcbiAgICAgIGFjdGl2ZUluZGV4OiAwLFxuICAgICAgaXNGb2N1czogZmFsc2VcbiAgICB9O1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhNZW50aW9ucywgW3tcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkVXBkYXRlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIHZhciBtZWFzdXJpbmcgPSB0aGlzLnN0YXRlLm1lYXN1cmluZzsgLy8gU3luYyBtZWFzdXJlIGRpdiB0b3Agd2l0aCB0ZXh0YXJlYSBmb3IgcmMtdHJpZ2dlciB1c2FnZVxuXG4gICAgICBpZiAobWVhc3VyaW5nKSB7XG4gICAgICAgIHRoaXMubWVhc3VyZS5zY3JvbGxUb3AgPSB0aGlzLnRleHRhcmVhLnNjcm9sbFRvcDtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwic3RhcnRNZWFzdXJlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHN0YXJ0TWVhc3VyZShtZWFzdXJlVGV4dCwgbWVhc3VyZVByZWZpeCwgbWVhc3VyZUxvY2F0aW9uKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbWVhc3VyaW5nOiB0cnVlLFxuICAgICAgICBtZWFzdXJlVGV4dDogbWVhc3VyZVRleHQsXG4gICAgICAgIG1lYXN1cmVQcmVmaXg6IG1lYXN1cmVQcmVmaXgsXG4gICAgICAgIG1lYXN1cmVMb2NhdGlvbjogbWVhc3VyZUxvY2F0aW9uLFxuICAgICAgICBhY3RpdmVJbmRleDogMFxuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInN0b3BNZWFzdXJlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHN0b3BNZWFzdXJlKGNhbGxiYWNrKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbWVhc3VyaW5nOiBmYWxzZSxcbiAgICAgICAgbWVhc3VyZUxvY2F0aW9uOiAwLFxuICAgICAgICBtZWFzdXJlVGV4dDogbnVsbFxuICAgICAgfSwgY2FsbGJhY2spO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJmb2N1c1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIHRoaXMudGV4dGFyZWEuZm9jdXMoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiYmx1clwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBibHVyKCkge1xuICAgICAgdGhpcy50ZXh0YXJlYS5ibHVyKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlclwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3RoaXMkc3RhdGU0ID0gdGhpcy5zdGF0ZSxcbiAgICAgICAgICB2YWx1ZSA9IF90aGlzJHN0YXRlNC52YWx1ZSxcbiAgICAgICAgICBtZWFzdXJlTG9jYXRpb24gPSBfdGhpcyRzdGF0ZTQubWVhc3VyZUxvY2F0aW9uLFxuICAgICAgICAgIG1lYXN1cmVQcmVmaXggPSBfdGhpcyRzdGF0ZTQubWVhc3VyZVByZWZpeCxcbiAgICAgICAgICBtZWFzdXJpbmcgPSBfdGhpcyRzdGF0ZTQubWVhc3VyaW5nLFxuICAgICAgICAgIGFjdGl2ZUluZGV4ID0gX3RoaXMkc3RhdGU0LmFjdGl2ZUluZGV4O1xuXG4gICAgICB2YXIgX3RoaXMkcHJvcHM0ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBwcmVmaXhDbHMgPSBfdGhpcyRwcm9wczQucHJlZml4Q2xzLFxuICAgICAgICAgIHBsYWNlbWVudCA9IF90aGlzJHByb3BzNC5wbGFjZW1lbnQsXG4gICAgICAgICAgdHJhbnNpdGlvbk5hbWUgPSBfdGhpcyRwcm9wczQudHJhbnNpdGlvbk5hbWUsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3RoaXMkcHJvcHM0LmNsYXNzTmFtZSxcbiAgICAgICAgICBzdHlsZSA9IF90aGlzJHByb3BzNC5zdHlsZSxcbiAgICAgICAgICBhdXRvRm9jdXMgPSBfdGhpcyRwcm9wczQuYXV0b0ZvY3VzLFxuICAgICAgICAgIG5vdEZvdW5kQ29udGVudCA9IF90aGlzJHByb3BzNC5ub3RGb3VuZENvbnRlbnQsXG4gICAgICAgICAgZ2V0UG9wdXBDb250YWluZXIgPSBfdGhpcyRwcm9wczQuZ2V0UG9wdXBDb250YWluZXIsXG4gICAgICAgICAgcmVzdFByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF90aGlzJHByb3BzNCwgW1wicHJlZml4Q2xzXCIsIFwicGxhY2VtZW50XCIsIFwidHJhbnNpdGlvbk5hbWVcIiwgXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiLCBcImF1dG9Gb2N1c1wiLCBcIm5vdEZvdW5kQ29udGVudFwiLCBcImdldFBvcHVwQ29udGFpbmVyXCJdKTtcblxuICAgICAgdmFyIGlucHV0UHJvcHMgPSBvbWl0KHJlc3RQcm9wcywgJ3ZhbHVlJywgJ2RlZmF1bHRWYWx1ZScsICdwcmVmaXgnLCAnc3BsaXQnLCAnY2hpbGRyZW4nLCAndmFsaWRhdGVTZWFyY2gnLCAnZmlsdGVyT3B0aW9uJywgJ29uU2VsZWN0JywgJ29uU2VhcmNoJyk7XG4gICAgICB2YXIgb3B0aW9ucyA9IG1lYXN1cmluZyA/IHRoaXMuZ2V0T3B0aW9ucygpIDogW107XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhwcmVmaXhDbHMsIGNsYXNzTmFtZSksXG4gICAgICAgIHN0eWxlOiBzdHlsZVxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcInRleHRhcmVhXCIsIE9iamVjdC5hc3NpZ24oe1xuICAgICAgICBhdXRvRm9jdXM6IGF1dG9Gb2N1cyxcbiAgICAgICAgcmVmOiB0aGlzLnNldFRleHRBcmVhUmVmLFxuICAgICAgICB2YWx1ZTogdmFsdWVcbiAgICAgIH0sIGlucHV0UHJvcHMsIHtcbiAgICAgICAgb25DaGFuZ2U6IHRoaXMub25DaGFuZ2UsXG4gICAgICAgIG9uS2V5RG93bjogdGhpcy5vbktleURvd24sXG4gICAgICAgIG9uS2V5VXA6IHRoaXMub25LZXlVcCxcbiAgICAgICAgb25Gb2N1czogdGhpcy5vbklucHV0Rm9jdXMsXG4gICAgICAgIG9uQmx1cjogdGhpcy5vbklucHV0Qmx1clxuICAgICAgfSkpLCBtZWFzdXJpbmcgJiYgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIHJlZjogdGhpcy5zZXRNZWFzdXJlUmVmLFxuICAgICAgICBjbGFzc05hbWU6IFwiXCIuY29uY2F0KHByZWZpeENscywgXCItbWVhc3VyZVwiKVxuICAgICAgfSwgdmFsdWUuc2xpY2UoMCwgbWVhc3VyZUxvY2F0aW9uKSwgUmVhY3QuY3JlYXRlRWxlbWVudChNZW50aW9uc0NvbnRleHRQcm92aWRlciwge1xuICAgICAgICB2YWx1ZToge1xuICAgICAgICAgIG5vdEZvdW5kQ29udGVudDogbm90Rm91bmRDb250ZW50LFxuICAgICAgICAgIGFjdGl2ZUluZGV4OiBhY3RpdmVJbmRleCxcbiAgICAgICAgICBzZXRBY3RpdmVJbmRleDogdGhpcy5zZXRBY3RpdmVJbmRleCxcbiAgICAgICAgICBzZWxlY3RPcHRpb246IHRoaXMuc2VsZWN0T3B0aW9uLFxuICAgICAgICAgIG9uRm9jdXM6IHRoaXMub25Ecm9wZG93bkZvY3VzLFxuICAgICAgICAgIG9uQmx1cjogdGhpcy5vbkRyb3Bkb3duQmx1clxuICAgICAgICB9XG4gICAgICB9LCBSZWFjdC5jcmVhdGVFbGVtZW50KEtleXdvcmRUcmlnZ2VyLCB7XG4gICAgICAgIHByZWZpeENsczogcHJlZml4Q2xzLFxuICAgICAgICB0cmFuc2l0aW9uTmFtZTogdHJhbnNpdGlvbk5hbWUsXG4gICAgICAgIHBsYWNlbWVudDogcGxhY2VtZW50LFxuICAgICAgICBvcHRpb25zOiBvcHRpb25zLFxuICAgICAgICB2aXNpYmxlOiB0cnVlLFxuICAgICAgICBnZXRQb3B1cENvbnRhaW5lcjogZ2V0UG9wdXBDb250YWluZXJcbiAgICAgIH0sIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIG51bGwsIG1lYXN1cmVQcmVmaXgpKSksIHZhbHVlLnNsaWNlKG1lYXN1cmVMb2NhdGlvbiArIG1lYXN1cmVQcmVmaXgubGVuZ3RoKSkpO1xuICAgIH1cbiAgfV0sIFt7XG4gICAga2V5OiBcImdldERlcml2ZWRTdGF0ZUZyb21Qcm9wc1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMocHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgdmFyIG5ld1N0YXRlID0ge307XG5cbiAgICAgIGlmICgndmFsdWUnIGluIHByb3BzICYmIHByb3BzLnZhbHVlICE9PSBwcmV2U3RhdGUudmFsdWUpIHtcbiAgICAgICAgbmV3U3RhdGUudmFsdWUgPSBwcm9wcy52YWx1ZSB8fCAnJztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ld1N0YXRlO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBNZW50aW9ucztcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuTWVudGlvbnMuT3B0aW9uID0gT3B0aW9uO1xuTWVudGlvbnMuZGVmYXVsdFByb3BzID0ge1xuICBwcmVmaXhDbHM6ICdyYy1tZW50aW9ucycsXG4gIHByZWZpeDogJ0AnLFxuICBzcGxpdDogJyAnLFxuICB2YWxpZGF0ZVNlYXJjaDogZGVmYXVsdFZhbGlkYXRlU2VhcmNoLFxuICBmaWx0ZXJPcHRpb246IGRlZmF1bHRGaWx0ZXJPcHRpb24sXG4gIG5vdEZvdW5kQ29udGVudDogJ05vdCBGb3VuZCcsXG4gIHJvd3M6IDFcbn07XG5wb2x5ZmlsbChNZW50aW9ucyk7XG5leHBvcnQgZGVmYXVsdCBNZW50aW9uczsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBY0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQURBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQXhEQTtBQTBEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-mentions/es/Mentions.js
