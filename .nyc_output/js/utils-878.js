__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAlignFromPlacement", function() { return getAlignFromPlacement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getAlignPopupClassName", function() { return getAlignPopupClassName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveRef", function() { return saveRef; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);


function isPointsEq(a1, a2, isAlignPoint) {
  if (isAlignPoint) {
    return a1[0] === a2[0];
  }

  return a1[0] === a2[0] && a1[1] === a2[1];
}

function getAlignFromPlacement(builtinPlacements, placementStr, align) {
  var baseAlign = builtinPlacements[placementStr] || {};
  return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, baseAlign, align);
}
function getAlignPopupClassName(builtinPlacements, prefixCls, align, isAlignPoint) {
  var points = align.points;

  for (var placement in builtinPlacements) {
    if (builtinPlacements.hasOwnProperty(placement)) {
      if (isPointsEq(builtinPlacements[placement].points, points, isAlignPoint)) {
        return prefixCls + '-placement-' + placement;
      }
    }
  }

  return '';
}
function saveRef(name, component) {
  this[name] = component;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3Qvbm9kZV9tb2R1bGVzL3JjLXRyaWdnZXIvZXMvdXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10cmVlLXNlbGVjdC9ub2RlX21vZHVsZXMvcmMtdHJpZ2dlci9lcy91dGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuZnVuY3Rpb24gaXNQb2ludHNFcShhMSwgYTIsIGlzQWxpZ25Qb2ludCkge1xuICBpZiAoaXNBbGlnblBvaW50KSB7XG4gICAgcmV0dXJuIGExWzBdID09PSBhMlswXTtcbiAgfVxuICByZXR1cm4gYTFbMF0gPT09IGEyWzBdICYmIGExWzFdID09PSBhMlsxXTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEFsaWduRnJvbVBsYWNlbWVudChidWlsdGluUGxhY2VtZW50cywgcGxhY2VtZW50U3RyLCBhbGlnbikge1xuICB2YXIgYmFzZUFsaWduID0gYnVpbHRpblBsYWNlbWVudHNbcGxhY2VtZW50U3RyXSB8fCB7fTtcbiAgcmV0dXJuIF9leHRlbmRzKHt9LCBiYXNlQWxpZ24sIGFsaWduKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEFsaWduUG9wdXBDbGFzc05hbWUoYnVpbHRpblBsYWNlbWVudHMsIHByZWZpeENscywgYWxpZ24sIGlzQWxpZ25Qb2ludCkge1xuICB2YXIgcG9pbnRzID0gYWxpZ24ucG9pbnRzO1xuICBmb3IgKHZhciBwbGFjZW1lbnQgaW4gYnVpbHRpblBsYWNlbWVudHMpIHtcbiAgICBpZiAoYnVpbHRpblBsYWNlbWVudHMuaGFzT3duUHJvcGVydHkocGxhY2VtZW50KSkge1xuICAgICAgaWYgKGlzUG9pbnRzRXEoYnVpbHRpblBsYWNlbWVudHNbcGxhY2VtZW50XS5wb2ludHMsIHBvaW50cywgaXNBbGlnblBvaW50KSkge1xuICAgICAgICByZXR1cm4gcHJlZml4Q2xzICsgJy1wbGFjZW1lbnQtJyArIHBsYWNlbWVudDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcmV0dXJuICcnO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc2F2ZVJlZihuYW1lLCBjb21wb25lbnQpIHtcbiAgdGhpc1tuYW1lXSA9IGNvbXBvbmVudDtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/node_modules/rc-trigger/es/utils.js
