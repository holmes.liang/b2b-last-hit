__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCheckBoxValidItem", function() { return getCheckBoxValidItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSelectTenantItem", function() { return getSelectTenantItem; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! numeral */ "./node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _common_apis__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/apis */ "./src/common/apis.tsx");
/* harmony import */ var _common_authority__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/authority */ "./src/common/authority.tsx");
/* harmony import */ var _common_options__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/options */ "./src/common/options.tsx");
/* harmony import */ var _component_filter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../component/filter */ "./src/app/desk/component/filter/index.tsx");








var exceptedTenants = ["D2C", "PLATFORM"];

var selectTenantCallback = function selectTenantCallback(respData) {
  return (respData.items || []).filter(function (item) {
    return exceptedTenants.indexOf(item.tenantType) === -1;
  }).map(function (item) {
    return {
      label: item.name,
      id: item.tenantId,
      value: item.tenantCode,
      tenantType: item.tenantType
    };
  });
};

var selectInsurerCallback = function selectInsurerCallback(respData) {
  return (respData || []).map(function (item) {
    return {
      label: item.text,
      value: item.code
    };
  });
};

var selectTaskTypeCallBack = function selectTaskTypeCallBack(respData) {
  return (respData || []).map(function (item) {
    return {
      label: item.name,
      value: item.code
    };
  });
};

var selectJobCallback = function selectJobCallback(respData) {
  return (respData || []).map(function (item) {
    return {
      label: item.jobName,
      value: item.jobCode
    };
  });
};

var selectChannelCallback = function selectChannelCallback(respData) {
  return (respData.items || []).map(function (item) {
    return {
      label: item.name,
      value: item.tenantCode
    };
  });
};

var selectOperationCallback = function selectOperationCallback(respData) {
  return (respData || []).map(function (item) {
    return {
      label: item,
      value: item
    };
  });
};

var CheckboxOperationCallback = function CheckboxOperationCallback(respData) {
  return (respData || []).map(function (item) {
    return {
      label: item.text,
      value: item.id
    };
  });
};

var handleDefaultValue = function handleDefaultValue(value) {
  return value === "" ? undefined : value;
};

var getCheckBoxValidItem = function getCheckBoxValidItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
    props: {
      label: "Status",
      field: "status",
      options: _common_options__WEBPACK_IMPORTED_MODULE_5__["validateStateOptions"],
      value: handleDefaultValue(defaultValue)
    }
  };
};
var CheckBoxTenantTypeItem = {
  type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
  props: {
    label: "Type of Tenant",
    field: "tenantTypes",
    multiple: true,
    options: [{
      label: "Agent",
      value: "AGENT"
    }, {
      label: "Broker",
      value: "BROKER"
    }, {
      label: "Insurer",
      value: "INSURER"
    }]
  }
};
var CheckBoxEnableItem = {
  type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
  props: {
    label: "Disabled",
    field: "disabled",
    options: _common_options__WEBPACK_IMPORTED_MODULE_5__["EnabledOptions"]
  }
};
var CheckBoxLockedItem = {
  type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
  props: {
    label: "Locked",
    field: "locked",
    options: _common_options__WEBPACK_IMPORTED_MODULE_5__["EnabledOptions"]
  }
};
var CheckBoxActivatedItem = {
  type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
  props: {
    label: "Activated",
    field: "activated",
    options: _common_options__WEBPACK_IMPORTED_MODULE_5__["EnabledOptions"]
  }
};

var getCheckBoxResultExeItem = function getCheckBoxResultExeItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
    props: {
      label: "Status",
      field: "status",
      value: handleDefaultValue(defaultValue),
      options: _common_options__WEBPACK_IMPORTED_MODULE_5__["resultOfExecutionOptions"]
    }
  };
};

var creditLimitRange = [1000, 10000, 50000, 100000, 500000].map(function (item) {
  return {
    label: "0 ~ ".concat(numeral__WEBPACK_IMPORTED_MODULE_2___default()(item).format("0,0")),
    value: item
  };
});
var CheckBoxCreditLimitRangeItem = {
  type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].NUMRANGE,
  props: {
    label: "Credit Limit",
    field: "creditLimitRange",
    options: creditLimitRange
  }
};

var getCheckBoxDateRangeItem = function getCheckBoxDateRangeItem(_ref) {
  var _ref$label = _ref.label,
      label = _ref$label === void 0 ? "Date of Sign-in" : _ref$label,
      _ref$field = _ref.field,
      field = _ref$field === void 0 ? "signedInDateRange" : _ref$field,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? "" : _ref$value,
      rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__["default"])(_ref, ["label", "field", "value"]);

  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].DATERANGE,
    props: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({
      label: label,
      field: field,
      value: {
        type: value
      },
      diffDays: 7
    }, rest)
  };
};

var getSelectTaskTypeItem = function getSelectTaskTypeItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Type of Task",
      field: "taskType",
      placeholder: "Select a task",
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].TASK_TYPES,
      get: true,
      layout: true,
      callback: selectTaskTypeCallBack,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getSelectJobItem = function getSelectJobItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Job",
      field: "jobCode",
      placeholder: "select a job",
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].JOB_TYPE,
      get: true,
      layout: true,
      callback: selectJobCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getSelectTenantItem = function getSelectTenantItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Tenant",
      field: "tenantCode",
      placeholder: "Select a tenant",
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].TENANT_QUERY,
      layout: true,
      callback: selectTenantCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getSelectTenantEntityItem = function getSelectTenantEntityItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, getSelectTenantItem(defaultValue).props, {}, {
      hasEntity: true,
      entityProps: {
        label: "Entity",
        field: "structId"
      }
    })
  };
};

var getSelectTenantQueryEntityItem = function getSelectTenantQueryEntityItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, getSelectTenantItem(defaultValue).props, {}, {
      hasEntity: true,
      entityProps: {
        label: "Entity",
        field: "ctntStructIds"
      }
    })
  };
};

var getSelectInsurerItem = function getSelectInsurerItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Insurer",
      field: "itntCode",
      placeholder: "Select an insurer",
      get: true,
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].TANANT_INSURER_QUERY,
      layout: true,
      callback: selectInsurerCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getSelectInsurerEntityItem = function getSelectInsurerEntityItem(defaultValue) {
  var hideTopSelect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, getSelectInsurerItem(defaultValue).props, {}, {
      hasEntity: true,
      hideTopSelect: hideTopSelect,
      entityProps: {
        label: "Insurer Entity",
        field: "itntStructId"
      }
    })
  };
};

var getSelectChannelItem = function getSelectChannelItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Channel",
      field: "ctntCode",
      placeholder: "Select a channel",
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].CHANNEL_QUERY,
      layout: true,
      callback: selectChannelCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getSelectChannelEntityItem = function getSelectChannelEntityItem(defaultValue) {
  var hideTopSelect = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, getSelectChannelItem(defaultValue).props, {}, {
      hideTopSelect: hideTopSelect,
      hasEntity: true,
      entityProps: {
        label: "Channel Entity",
        field: "ctntStructId"
      }
    })
  };
};

var getCheckboxOperationItem = function getCheckboxOperationItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].SELECT,
    props: {
      label: "Type of Operation",
      field: "operType",
      placeholder: "Select",
      get: true,
      mode: "multiple",
      layout: true,
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].AUDIT_OPS,
      callback: selectOperationCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getCheckboxStatusItem = function getCheckboxStatusItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
    props: {
      label: "Status",
      field: "statusName",
      multiple: false,
      get: true,
      url: _common_apis__WEBPACK_IMPORTED_MODULE_3__["default"].MESSAGE_STATUS,
      callback: CheckboxOperationCallback,
      value: handleDefaultValue(defaultValue)
    }
  };
};

var getTenantFilters = function getTenantFilters(_ref2) {
  var _ref2$status = _ref2.status,
      status = _ref2$status === void 0 ? "" : _ref2$status;
  return {
    items: [getCheckBoxValidItem(status), CheckBoxTenantTypeItem]
  };
};

var getHierarchyFilters = function getHierarchyFilters(_ref3) {
  var _ref3$status = _ref3.status,
      status = _ref3$status === void 0 ? "" : _ref3$status,
      _ref3$tenantCode = _ref3.tenantCode,
      tenantCode = _ref3$tenantCode === void 0 ? "" : _ref3$tenantCode;
  return {
    items: [_common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isPlatform() && getSelectTenantItem(tenantCode), getCheckBoxValidItem(status)]
  };
};

var getUserAccountsFilters = function getUserAccountsFilters(_ref4) {
  var _ref4$tenantCode = _ref4.tenantCode,
      tenantCode = _ref4$tenantCode === void 0 ? "" : _ref4$tenantCode;
  return {
    items: [// Authority.isPlatform() && getSelectTenantEntityItem(tenantCode),
    getSelectTenantEntityItem(tenantCode), CheckBoxEnableItem, CheckBoxLockedItem, CheckBoxActivatedItem]
  };
};

var getSalesChannelsFilters = function getSalesChannelsFilters(_ref5) {
  var _ref5$ctntCode = _ref5.ctntCode,
      ctntCode = _ref5$ctntCode === void 0 ? "" : _ref5$ctntCode,
      _ref5$itntCode = _ref5.itntCode,
      itntCode = _ref5$itntCode === void 0 ? "" : _ref5$itntCode,
      _ref5$status = _ref5.status,
      status = _ref5$status === void 0 ? "" : _ref5$status;
  var hideTopInsurerSelect = _common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isInsurer();
  var hideTopChannelSelect = _common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isChannelTenantWithD2C();
  return {
    items: [getSelectInsurerEntityItem(itntCode, hideTopInsurerSelect), getSelectChannelEntityItem(ctntCode, hideTopChannelSelect), getCheckBoxValidItem(status), CheckBoxCreditLimitRangeItem]
  };
};

var getAgreementsFilters = function getAgreementsFilters(_ref6) {
  var _ref6$status = _ref6.status,
      status = _ref6$status === void 0 ? "" : _ref6$status,
      _ref6$ctntCode = _ref6.ctntCode,
      ctntCode = _ref6$ctntCode === void 0 ? "" : _ref6$ctntCode,
      _ref6$itntCode = _ref6.itntCode,
      itntCode = _ref6$itntCode === void 0 ? "" : _ref6$itntCode;
  var hideTopInsurerSelect = _common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isInsurer();
  var hideTopChannelSelect = _common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isChannelTenantWithD2C();
  return {
    items: [getSelectInsurerEntityItem(itntCode, hideTopInsurerSelect), getSelectChannelEntityItem(ctntCode, hideTopChannelSelect), getCheckBoxValidItem(status)]
  };
};

var getUserSessionFilters = function getUserSessionFilters(_ref7) {
  var _ref7$status = _ref7.status,
      status = _ref7$status === void 0 ? "" : _ref7$status,
      _ref7$signedInDateRan = _ref7.signedInDateRange,
      signedInDateRange = _ref7$signedInDateRan === void 0 ? "" : _ref7$signedInDateRan;
  return {
    items: [!_common_authority__WEBPACK_IMPORTED_MODULE_4__["default"].isInsurer() && getSelectTenantItem(""), getCheckBoxResultExeItem(status), getCheckBoxDateRangeItem({
      label: "Date of Sign-in",
      field: "signedInDateRange",
      value: signedInDateRange
    })]
  };
};

var getTraceTaskFilters = function getTraceTaskFilters(_ref8) {
  var _ref8$dueAtRange = _ref8.dueAtRange,
      dueAtRange = _ref8$dueAtRange === void 0 ? "" : _ref8$dueAtRange,
      _ref8$createdAtRange = _ref8.createdAtRange,
      createdAtRange = _ref8$createdAtRange === void 0 ? "" : _ref8$createdAtRange;
  return {
    items: [getSelectTaskTypeItem(""), {
      type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
      props: {
        label: "Result of Execution",
        field: "status",
        options: [{
          label: "Successful",
          value: "SUCC"
        }, {
          label: "Fail",
          value: "FAIL"
        }, {
          label: "Pending",
          value: "PENDING"
        }, {
          label: "Executing",
          value: "IN_PGRES"
        }]
      }
    }, getCheckBoxDateRangeItem({
      label: "Due Date of Task",
      field: "dueAtRange",
      value: dueAtRange
    }), getCheckBoxDateRangeItem({
      label: "Creation Data of Task",
      field: "createdAtRange",
      value: createdAtRange
    })]
  };
};

var getJobQueryFilters = function getJobQueryFilters(_ref9) {
  var _ref9$startAtRange = _ref9.startAtRange,
      startAtRange = _ref9$startAtRange === void 0 ? "" : _ref9$startAtRange;
  return {
    items: [getSelectJobItem(""), {
      type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
      props: {
        label: "Result of Execution",
        field: "jobStatus",
        options: [{
          label: "In Progress",
          value: "IN_PGRES"
        }, {
          label: "Successful",
          value: "SUCC"
        }, {
          label: "Fail",
          value: "FAIL"
        }, {
          label: "killed",
          value: "KILLED"
        }]
      }
    }, getCheckBoxDateRangeItem({
      label: "Date of Execution",
      field: "startAtRange",
      value: startAtRange
    })]
  };
};

var getMessagesFilters = function getMessagesFilters(_ref10) {
  var _ref10$startAtRange = _ref10.startAtRange,
      startAtRange = _ref10$startAtRange === void 0 ? "" : _ref10$startAtRange;
  return {
    items: [getCheckboxStatusItem(""), getCheckBoxDateRangeItem({
      label: "Created on",
      field: "creationDateRange",
      value: startAtRange
    }), getCheckBoxDateRangeItem({
      label: "Published on",
      field: "publishDateRange",
      value: startAtRange
    })]
  };
};

var getEmailTypeItem = function getEmailTypeItem(defaultValue) {
  return {
    type: _component_filter__WEBPACK_IMPORTED_MODULE_6__["FilterItemTypes"].CHECKBOX,
    props: {
      label: "Filter",
      field: "readStatus",
      value: handleDefaultValue(defaultValue),
      options: [{
        label: "All",
        value: "all"
      }, {
        label: "Unread",
        value: "unread"
      }, {
        label: "Read",
        value: "read"
      }]
    }
  };
};

var getEmailFilters = function getEmailFilters(_ref11) {
  var _ref11$defaultValue = _ref11.defaultValue,
      defaultValue = _ref11$defaultValue === void 0 ? "" : _ref11$defaultValue;
  return {
    items: [getEmailTypeItem(defaultValue)]
  };
};

var getQueryAuditLogFilters = function getQueryAuditLogFilters(_ref12) {
  var _ref12$operType = _ref12.operType,
      operType = _ref12$operType === void 0 ? "" : _ref12$operType,
      _ref12$accessedAtRang = _ref12.accessedAtRange,
      accessedAtRange = _ref12$accessedAtRang === void 0 ? "" : _ref12$accessedAtRang;
  return {
    items: [getCheckboxOperationItem(operType), getCheckBoxDateRangeItem({
      label: "Date of Operation",
      field: "accessedAtRange",
      value: accessedAtRange,
      required: true
    })]
  };
};

/* harmony default export */ __webpack_exports__["default"] = ({
  getTenantFilters: getTenantFilters,
  getHierarchyFilters: getHierarchyFilters,
  getUserAccountsFilters: getUserAccountsFilters,
  getSalesChannelsFilters: getSalesChannelsFilters,
  getAgreementsFilters: getAgreementsFilters,
  getUserSessionFilters: getUserSessionFilters,
  getTraceTaskFilters: getTraceTaskFilters,
  getQueryAuditLogFilters: getQueryAuditLogFilters,
  getJobQueryFilters: getJobQueryFilters,
  getSelectTenantItem: getSelectTenantItem,
  getCheckBoxValidItem: getCheckBoxValidItem,
  getSelectTenantQueryEntityItem: getSelectTenantQueryEntityItem,
  getMessagesFilters: getMessagesFilters,
  getEmailFilters: getEmailFilters
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svYWRtaW4vY29tbW9uL2ZpbHRlcnMudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svYWRtaW4vY29tbW9uL2ZpbHRlcnMudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBudW1lcmFsIGZyb20gXCJudW1lcmFsXCI7XG5pbXBvcnQgQVBJIGZyb20gXCJAY29tbW9uL2FwaXNcIjtcbmltcG9ydCBBdXRob3JpdHkgZnJvbSBcIkBjb21tb24vYXV0aG9yaXR5XCI7XG5pbXBvcnQgYXV0aG9yaXR5IGZyb20gXCJAY29tbW9uL2F1dGhvcml0eVwiO1xuaW1wb3J0IHsgRW5hYmxlZE9wdGlvbnMsIHJlc3VsdE9mRXhlY3V0aW9uT3B0aW9ucywgdmFsaWRhdGVTdGF0ZU9wdGlvbnMgfSBmcm9tIFwiQGNvbW1vbi9vcHRpb25zXCI7XG5pbXBvcnQgeyBGaWx0ZXJJdGVtVHlwZXMgfSBmcm9tIFwiLi4vLi4vY29tcG9uZW50L2ZpbHRlclwiO1xuXG5jb25zdCBleGNlcHRlZFRlbmFudHMgPSBbXCJEMkNcIiwgXCJQTEFURk9STVwiXTtcbmNvbnN0IHNlbGVjdFRlbmFudENhbGxiYWNrID0gKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgcmV0dXJuIChyZXNwRGF0YS5pdGVtcyB8fCBbXSlcbiAgICAuZmlsdGVyKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiBleGNlcHRlZFRlbmFudHMuaW5kZXhPZihpdGVtLnRlbmFudFR5cGUpID09PSAtMTtcbiAgICB9KVxuICAgIC5tYXAoKGl0ZW06IGFueSkgPT4gKHtcbiAgICAgIGxhYmVsOiBpdGVtLm5hbWUsXG4gICAgICBpZDogaXRlbS50ZW5hbnRJZCxcbiAgICAgIHZhbHVlOiBpdGVtLnRlbmFudENvZGUsXG4gICAgICB0ZW5hbnRUeXBlOiBpdGVtLnRlbmFudFR5cGUsXG4gICAgfSkpO1xufTtcblxuY29uc3Qgc2VsZWN0SW5zdXJlckNhbGxiYWNrID0gKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgcmV0dXJuIChyZXNwRGF0YSB8fCBbXSlcbiAgICAubWFwKChpdGVtOiBhbnkpID0+ICh7XG4gICAgICBsYWJlbDogaXRlbS50ZXh0LFxuICAgICAgdmFsdWU6IGl0ZW0uY29kZSxcbiAgICB9KSk7XG59O1xuXG5jb25zdCBzZWxlY3RUYXNrVHlwZUNhbGxCYWNrID0gKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgcmV0dXJuIChyZXNwRGF0YSB8fCBbXSlcbiAgICAubWFwKChpdGVtOiBhbnkpID0+ICh7XG4gICAgICBsYWJlbDogaXRlbS5uYW1lLFxuICAgICAgdmFsdWU6IGl0ZW0uY29kZSxcbiAgICB9KSk7XG59O1xuXG5jb25zdCBzZWxlY3RKb2JDYWxsYmFjayA9IChyZXNwRGF0YTogYW55KSA9PiB7XG4gIHJldHVybiAocmVzcERhdGEgfHwgW10pXG4gICAgLm1hcCgoaXRlbTogYW55KSA9PiAoe1xuICAgICAgbGFiZWw6IGl0ZW0uam9iTmFtZSxcbiAgICAgIHZhbHVlOiBpdGVtLmpvYkNvZGUsXG4gICAgfSkpO1xufTtcblxuY29uc3Qgc2VsZWN0Q2hhbm5lbENhbGxiYWNrID0gKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgcmV0dXJuIChyZXNwRGF0YS5pdGVtcyB8fCBbXSlcbiAgICAubWFwKChpdGVtOiBhbnkpID0+ICh7XG4gICAgICBsYWJlbDogaXRlbS5uYW1lLFxuICAgICAgdmFsdWU6IGl0ZW0udGVuYW50Q29kZSxcbiAgICB9KSk7XG59O1xuXG5jb25zdCBzZWxlY3RPcGVyYXRpb25DYWxsYmFjayA9IChyZXNwRGF0YTogYW55KSA9PiB7XG4gIHJldHVybiAocmVzcERhdGEgfHwgW10pXG4gICAgLm1hcCgoaXRlbTogYW55KSA9PiAoe1xuICAgICAgbGFiZWw6IGl0ZW0sXG4gICAgICB2YWx1ZTogaXRlbSxcbiAgICB9KSk7XG59O1xuXG5jb25zdCBDaGVja2JveE9wZXJhdGlvbkNhbGxiYWNrID0gKHJlc3BEYXRhOiBhbnkpID0+IHtcbiAgcmV0dXJuIChyZXNwRGF0YSB8fCBbXSlcbiAgICAubWFwKChpdGVtOiBhbnkpID0+ICh7XG4gICAgICBsYWJlbDogaXRlbS50ZXh0LFxuICAgICAgdmFsdWU6IGl0ZW0uaWQsXG4gICAgfSkpO1xufTtcblxuY29uc3QgaGFuZGxlRGVmYXVsdFZhbHVlID0gKHZhbHVlOiBhbnkpID0+IHtcbiAgcmV0dXJuIHZhbHVlID09PSBcIlwiID8gdW5kZWZpbmVkIDogdmFsdWU7XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0Q2hlY2tCb3hWYWxpZEl0ZW0gPSAoZGVmYXVsdFZhbHVlOiBzdHJpbmcpID0+IHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuQ0hFQ0tCT1gsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIlN0YXR1c1wiLFxuICAgICAgZmllbGQ6IFwic3RhdHVzXCIsXG4gICAgICBvcHRpb25zOiB2YWxpZGF0ZVN0YXRlT3B0aW9ucyxcbiAgICAgIHZhbHVlOiBoYW5kbGVEZWZhdWx0VmFsdWUoZGVmYXVsdFZhbHVlKSxcbiAgICB9LFxuICB9O1xufTtcblxuY29uc3QgQ2hlY2tCb3hUZW5hbnRUeXBlSXRlbSA9IHtcbiAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkNIRUNLQk9YLFxuICBwcm9wczoge1xuICAgIGxhYmVsOiBcIlR5cGUgb2YgVGVuYW50XCIsXG4gICAgZmllbGQ6IFwidGVuYW50VHlwZXNcIixcbiAgICBtdWx0aXBsZTogdHJ1ZSxcbiAgICBvcHRpb25zOiBbXG4gICAgICB7IGxhYmVsOiBcIkFnZW50XCIsIHZhbHVlOiBcIkFHRU5UXCIgfSxcbiAgICAgIHsgbGFiZWw6IFwiQnJva2VyXCIsIHZhbHVlOiBcIkJST0tFUlwiIH0sXG4gICAgICB7IGxhYmVsOiBcIkluc3VyZXJcIiwgdmFsdWU6IFwiSU5TVVJFUlwiIH0sXG4gICAgXSxcbiAgfSxcbn07XG5cbmNvbnN0IENoZWNrQm94RW5hYmxlSXRlbSA9IHtcbiAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkNIRUNLQk9YLFxuICBwcm9wczoge1xuICAgIGxhYmVsOiBcIkRpc2FibGVkXCIsXG4gICAgZmllbGQ6IFwiZGlzYWJsZWRcIixcbiAgICBvcHRpb25zOiBFbmFibGVkT3B0aW9ucyxcbiAgfSxcbn07XG5cbmNvbnN0IENoZWNrQm94TG9ja2VkSXRlbSA9IHtcbiAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkNIRUNLQk9YLFxuICBwcm9wczoge1xuICAgIGxhYmVsOiBcIkxvY2tlZFwiLFxuICAgIGZpZWxkOiBcImxvY2tlZFwiLFxuICAgIG9wdGlvbnM6IEVuYWJsZWRPcHRpb25zLFxuICB9LFxufTtcblxuY29uc3QgQ2hlY2tCb3hBY3RpdmF0ZWRJdGVtID0ge1xuICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuQ0hFQ0tCT1gsXG4gIHByb3BzOiB7XG4gICAgbGFiZWw6IFwiQWN0aXZhdGVkXCIsXG4gICAgZmllbGQ6IFwiYWN0aXZhdGVkXCIsXG4gICAgb3B0aW9uczogRW5hYmxlZE9wdGlvbnMsXG4gIH0sXG59O1xuXG5jb25zdCBnZXRDaGVja0JveFJlc3VsdEV4ZUl0ZW0gPSAoZGVmYXVsdFZhbHVlOiBzdHJpbmcpID0+IHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuQ0hFQ0tCT1gsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIlN0YXR1c1wiLFxuICAgICAgZmllbGQ6IFwic3RhdHVzXCIsXG4gICAgICB2YWx1ZTogaGFuZGxlRGVmYXVsdFZhbHVlKGRlZmF1bHRWYWx1ZSksXG4gICAgICBvcHRpb25zOiByZXN1bHRPZkV4ZWN1dGlvbk9wdGlvbnMsXG4gICAgfSxcbiAgfTtcbn07XG5cbmNvbnN0IGNyZWRpdExpbWl0UmFuZ2UgPSBbMTAwMCwgMTAwMDAsIDUwMDAwLCAxMDAwMDAsIDUwMDAwMF0ubWFwKGl0ZW0gPT4gKHtcbiAgbGFiZWw6IGAwIH4gJHtudW1lcmFsKGl0ZW0pLmZvcm1hdChcIjAsMFwiKX1gLFxuICB2YWx1ZTogaXRlbSxcbn0pKTtcbmNvbnN0IENoZWNrQm94Q3JlZGl0TGltaXRSYW5nZUl0ZW0gPSB7XG4gIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5OVU1SQU5HRSxcbiAgcHJvcHM6IHtcbiAgICBsYWJlbDogXCJDcmVkaXQgTGltaXRcIixcbiAgICBmaWVsZDogXCJjcmVkaXRMaW1pdFJhbmdlXCIsXG4gICAgb3B0aW9uczogY3JlZGl0TGltaXRSYW5nZSxcbiAgfSxcbn07XG5cbmNvbnN0IGdldENoZWNrQm94RGF0ZVJhbmdlSXRlbSA9ICh7IGxhYmVsID0gXCJEYXRlIG9mIFNpZ24taW5cIiwgZmllbGQgPSBcInNpZ25lZEluRGF0ZVJhbmdlXCIsIHZhbHVlID0gXCJcIiwgLi4ucmVzdCB9KSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkRBVEVSQU5HRSxcbiAgICBwcm9wczoge1xuICAgICAgbGFiZWwsXG4gICAgICBmaWVsZCxcbiAgICAgIHZhbHVlOiB7IHR5cGU6IHZhbHVlIH0sXG4gICAgICBkaWZmRGF5czogNyxcbiAgICAgIC4uLnJlc3QsXG4gICAgfSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldFNlbGVjdFRhc2tUeXBlSXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZykgPT4ge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5TRUxFQ1QsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIlR5cGUgb2YgVGFza1wiLFxuICAgICAgZmllbGQ6IFwidGFza1R5cGVcIixcbiAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCBhIHRhc2tcIixcbiAgICAgIHVybDogQVBJLlRBU0tfVFlQRVMsXG4gICAgICBnZXQ6IHRydWUsXG4gICAgICBsYXlvdXQ6IHRydWUsXG4gICAgICBjYWxsYmFjazogc2VsZWN0VGFza1R5cGVDYWxsQmFjayxcbiAgICAgIHZhbHVlOiBoYW5kbGVEZWZhdWx0VmFsdWUoZGVmYXVsdFZhbHVlKSxcbiAgICB9LFxuICB9O1xufTtcblxuY29uc3QgZ2V0U2VsZWN0Sm9iSXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZykgPT4ge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5TRUxFQ1QsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIkpvYlwiLFxuICAgICAgZmllbGQ6IFwiam9iQ29kZVwiLFxuICAgICAgcGxhY2Vob2xkZXI6IFwic2VsZWN0IGEgam9iXCIsXG4gICAgICB1cmw6IEFQSS5KT0JfVFlQRSxcbiAgICAgIGdldDogdHJ1ZSxcbiAgICAgIGxheW91dDogdHJ1ZSxcbiAgICAgIGNhbGxiYWNrOiBzZWxlY3RKb2JDYWxsYmFjayxcbiAgICAgIHZhbHVlOiBoYW5kbGVEZWZhdWx0VmFsdWUoZGVmYXVsdFZhbHVlKSxcbiAgICB9LFxuICB9O1xufTtcblxuZXhwb3J0IGNvbnN0IGdldFNlbGVjdFRlbmFudEl0ZW0gPSAoZGVmYXVsdFZhbHVlOiBzdHJpbmcpID0+IHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiBGaWx0ZXJJdGVtVHlwZXMuU0VMRUNULFxuICAgIHByb3BzOiB7XG4gICAgICBsYWJlbDogXCJUZW5hbnRcIixcbiAgICAgIGZpZWxkOiBcInRlbmFudENvZGVcIixcbiAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCBhIHRlbmFudFwiLFxuICAgICAgdXJsOiBBUEkuVEVOQU5UX1FVRVJZLFxuICAgICAgbGF5b3V0OiB0cnVlLFxuICAgICAgY2FsbGJhY2s6IHNlbGVjdFRlbmFudENhbGxiYWNrLFxuICAgICAgdmFsdWU6IGhhbmRsZURlZmF1bHRWYWx1ZShkZWZhdWx0VmFsdWUpLFxuICAgIH0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRTZWxlY3RUZW5hbnRFbnRpdHlJdGVtID0gKGRlZmF1bHRWYWx1ZTogc3RyaW5nKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLlNFTEVDVCxcbiAgICBwcm9wczoge1xuICAgICAgLi4uZ2V0U2VsZWN0VGVuYW50SXRlbShkZWZhdWx0VmFsdWUpLnByb3BzLCAuLi57XG4gICAgICAgIGhhc0VudGl0eTogdHJ1ZSxcbiAgICAgICAgZW50aXR5UHJvcHM6IHtcbiAgICAgICAgICBsYWJlbDogXCJFbnRpdHlcIixcbiAgICAgICAgICBmaWVsZDogXCJzdHJ1Y3RJZFwiLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICB9O1xufTtcblxuY29uc3QgZ2V0U2VsZWN0VGVuYW50UXVlcnlFbnRpdHlJdGVtID0gKGRlZmF1bHRWYWx1ZTogc3RyaW5nKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLlNFTEVDVCxcbiAgICBwcm9wczoge1xuICAgICAgLi4uZ2V0U2VsZWN0VGVuYW50SXRlbShkZWZhdWx0VmFsdWUpLnByb3BzLCAuLi57XG4gICAgICAgIGhhc0VudGl0eTogdHJ1ZSxcbiAgICAgICAgZW50aXR5UHJvcHM6IHtcbiAgICAgICAgICBsYWJlbDogXCJFbnRpdHlcIixcbiAgICAgICAgICBmaWVsZDogXCJjdG50U3RydWN0SWRzXCIsXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgIH0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRTZWxlY3RJbnN1cmVySXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZykgPT4ge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5TRUxFQ1QsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIkluc3VyZXJcIixcbiAgICAgIGZpZWxkOiBcIml0bnRDb2RlXCIsXG4gICAgICBwbGFjZWhvbGRlcjogXCJTZWxlY3QgYW4gaW5zdXJlclwiLFxuICAgICAgZ2V0OiB0cnVlLFxuICAgICAgdXJsOiBBUEkuVEFOQU5UX0lOU1VSRVJfUVVFUlksXG4gICAgICBsYXlvdXQ6IHRydWUsXG4gICAgICBjYWxsYmFjazogc2VsZWN0SW5zdXJlckNhbGxiYWNrLFxuICAgICAgdmFsdWU6IGhhbmRsZURlZmF1bHRWYWx1ZShkZWZhdWx0VmFsdWUpLFxuICAgIH0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRTZWxlY3RJbnN1cmVyRW50aXR5SXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZywgaGlkZVRvcFNlbGVjdCA9IGZhbHNlKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLlNFTEVDVCxcbiAgICBwcm9wczoge1xuICAgICAgLi4uZ2V0U2VsZWN0SW5zdXJlckl0ZW0oZGVmYXVsdFZhbHVlKS5wcm9wcywgLi4ue1xuICAgICAgICBoYXNFbnRpdHk6IHRydWUsXG4gICAgICAgIGhpZGVUb3BTZWxlY3QsXG4gICAgICAgIGVudGl0eVByb3BzOiB7XG4gICAgICAgICAgbGFiZWw6IFwiSW5zdXJlciBFbnRpdHlcIixcbiAgICAgICAgICBmaWVsZDogXCJpdG50U3RydWN0SWRcIixcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldFNlbGVjdENoYW5uZWxJdGVtID0gKGRlZmF1bHRWYWx1ZTogc3RyaW5nKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLlNFTEVDVCxcbiAgICBwcm9wczoge1xuICAgICAgbGFiZWw6IFwiQ2hhbm5lbFwiLFxuICAgICAgZmllbGQ6IFwiY3RudENvZGVcIixcbiAgICAgIHBsYWNlaG9sZGVyOiBcIlNlbGVjdCBhIGNoYW5uZWxcIixcbiAgICAgIHVybDogQVBJLkNIQU5ORUxfUVVFUlksXG4gICAgICBsYXlvdXQ6IHRydWUsXG4gICAgICBjYWxsYmFjazogc2VsZWN0Q2hhbm5lbENhbGxiYWNrLFxuICAgICAgdmFsdWU6IGhhbmRsZURlZmF1bHRWYWx1ZShkZWZhdWx0VmFsdWUpLFxuICAgIH0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRTZWxlY3RDaGFubmVsRW50aXR5SXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZywgaGlkZVRvcFNlbGVjdCA9IGZhbHNlKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLlNFTEVDVCxcbiAgICBwcm9wczoge1xuICAgICAgLi4uZ2V0U2VsZWN0Q2hhbm5lbEl0ZW0oZGVmYXVsdFZhbHVlKS5wcm9wcywgLi4ue1xuICAgICAgICBoaWRlVG9wU2VsZWN0LFxuICAgICAgICBoYXNFbnRpdHk6IHRydWUsXG4gICAgICAgIGVudGl0eVByb3BzOiB7XG4gICAgICAgICAgbGFiZWw6IFwiQ2hhbm5lbCBFbnRpdHlcIixcbiAgICAgICAgICBmaWVsZDogXCJjdG50U3RydWN0SWRcIixcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldENoZWNrYm94T3BlcmF0aW9uSXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZykgPT4ge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5TRUxFQ1QsXG4gICAgcHJvcHM6IHtcbiAgICAgIGxhYmVsOiBcIlR5cGUgb2YgT3BlcmF0aW9uXCIsXG4gICAgICBmaWVsZDogXCJvcGVyVHlwZVwiLFxuICAgICAgcGxhY2Vob2xkZXI6IFwiU2VsZWN0XCIsXG4gICAgICBnZXQ6IHRydWUsXG4gICAgICBtb2RlOiBcIm11bHRpcGxlXCIsXG4gICAgICBsYXlvdXQ6IHRydWUsXG4gICAgICB1cmw6IEFQSS5BVURJVF9PUFMsXG4gICAgICBjYWxsYmFjazogc2VsZWN0T3BlcmF0aW9uQ2FsbGJhY2ssXG4gICAgICB2YWx1ZTogaGFuZGxlRGVmYXVsdFZhbHVlKGRlZmF1bHRWYWx1ZSksXG4gICAgfSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldENoZWNrYm94U3RhdHVzSXRlbSA9IChkZWZhdWx0VmFsdWU6IHN0cmluZykgPT4ge1xuICByZXR1cm4ge1xuICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5DSEVDS0JPWCxcbiAgICBwcm9wczoge1xuICAgICAgbGFiZWw6IFwiU3RhdHVzXCIsXG4gICAgICBmaWVsZDogXCJzdGF0dXNOYW1lXCIsXG4gICAgICBtdWx0aXBsZTogZmFsc2UsXG4gICAgICBnZXQ6IHRydWUsXG4gICAgICB1cmw6IEFQSS5NRVNTQUdFX1NUQVRVUyxcbiAgICAgIGNhbGxiYWNrOiBDaGVja2JveE9wZXJhdGlvbkNhbGxiYWNrLFxuICAgICAgdmFsdWU6IGhhbmRsZURlZmF1bHRWYWx1ZShkZWZhdWx0VmFsdWUpLFxuICAgIH0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRUZW5hbnRGaWx0ZXJzID0gKHsgc3RhdHVzID0gXCJcIiB9KSA9PiB7XG4gIHJldHVybiB7XG4gICAgaXRlbXM6IFtcbiAgICAgIGdldENoZWNrQm94VmFsaWRJdGVtKHN0YXR1cyksXG4gICAgICBDaGVja0JveFRlbmFudFR5cGVJdGVtLFxuICAgIF0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRIaWVyYXJjaHlGaWx0ZXJzID0gKHsgc3RhdHVzID0gXCJcIiwgdGVuYW50Q29kZSA9IFwiXCIgfSkgPT4ge1xuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBBdXRob3JpdHkuaXNQbGF0Zm9ybSgpICYmIGdldFNlbGVjdFRlbmFudEl0ZW0odGVuYW50Q29kZSksXG4gICAgICBnZXRDaGVja0JveFZhbGlkSXRlbShzdGF0dXMpLFxuICAgIF0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRVc2VyQWNjb3VudHNGaWx0ZXJzID0gKHsgdGVuYW50Q29kZSA9IFwiXCIgfSkgPT4ge1xuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICAvLyBBdXRob3JpdHkuaXNQbGF0Zm9ybSgpICYmIGdldFNlbGVjdFRlbmFudEVudGl0eUl0ZW0odGVuYW50Q29kZSksXG4gICAgICBnZXRTZWxlY3RUZW5hbnRFbnRpdHlJdGVtKHRlbmFudENvZGUpLFxuICAgICAgQ2hlY2tCb3hFbmFibGVJdGVtLFxuICAgICAgQ2hlY2tCb3hMb2NrZWRJdGVtLFxuICAgICAgQ2hlY2tCb3hBY3RpdmF0ZWRJdGVtLFxuICAgIF0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRTYWxlc0NoYW5uZWxzRmlsdGVycyA9ICh7IGN0bnRDb2RlID0gXCJcIiwgaXRudENvZGUgPSBcIlwiLCBzdGF0dXMgPSBcIlwiIH0pID0+IHtcbiAgY29uc3QgaGlkZVRvcEluc3VyZXJTZWxlY3QgPSBhdXRob3JpdHkuaXNJbnN1cmVyKCk7XG4gIGNvbnN0IGhpZGVUb3BDaGFubmVsU2VsZWN0ID0gYXV0aG9yaXR5LmlzQ2hhbm5lbFRlbmFudFdpdGhEMkMoKTtcblxuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBnZXRTZWxlY3RJbnN1cmVyRW50aXR5SXRlbShpdG50Q29kZSwgaGlkZVRvcEluc3VyZXJTZWxlY3QpLFxuICAgICAgZ2V0U2VsZWN0Q2hhbm5lbEVudGl0eUl0ZW0oY3RudENvZGUsIGhpZGVUb3BDaGFubmVsU2VsZWN0KSxcbiAgICAgIGdldENoZWNrQm94VmFsaWRJdGVtKHN0YXR1cyksXG4gICAgICBDaGVja0JveENyZWRpdExpbWl0UmFuZ2VJdGVtLFxuICAgIF0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRBZ3JlZW1lbnRzRmlsdGVycyA9ICh7IHN0YXR1cyA9IFwiXCIsIGN0bnRDb2RlID0gXCJcIiwgaXRudENvZGUgPSBcIlwiIH0pID0+IHtcbiAgY29uc3QgaGlkZVRvcEluc3VyZXJTZWxlY3QgPSBhdXRob3JpdHkuaXNJbnN1cmVyKCk7XG4gIGNvbnN0IGhpZGVUb3BDaGFubmVsU2VsZWN0ID0gYXV0aG9yaXR5LmlzQ2hhbm5lbFRlbmFudFdpdGhEMkMoKTtcblxuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBnZXRTZWxlY3RJbnN1cmVyRW50aXR5SXRlbShpdG50Q29kZSwgaGlkZVRvcEluc3VyZXJTZWxlY3QpLFxuICAgICAgZ2V0U2VsZWN0Q2hhbm5lbEVudGl0eUl0ZW0oY3RudENvZGUsIGhpZGVUb3BDaGFubmVsU2VsZWN0KSxcbiAgICAgIGdldENoZWNrQm94VmFsaWRJdGVtKHN0YXR1cyksXG4gICAgXSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldFVzZXJTZXNzaW9uRmlsdGVycyA9ICh7IHN0YXR1cyA9IFwiXCIsIHNpZ25lZEluRGF0ZVJhbmdlID0gXCJcIiB9KSA9PiB7XG4gIHJldHVybiB7XG4gICAgaXRlbXM6IFtcbiAgICAgICFBdXRob3JpdHkuaXNJbnN1cmVyKCkgJiYgZ2V0U2VsZWN0VGVuYW50SXRlbShcIlwiKSxcbiAgICAgIGdldENoZWNrQm94UmVzdWx0RXhlSXRlbShzdGF0dXMpLFxuICAgICAgZ2V0Q2hlY2tCb3hEYXRlUmFuZ2VJdGVtKHsgbGFiZWw6IFwiRGF0ZSBvZiBTaWduLWluXCIsIGZpZWxkOiBcInNpZ25lZEluRGF0ZVJhbmdlXCIsIHZhbHVlOiBzaWduZWRJbkRhdGVSYW5nZSB9KSxcbiAgICBdLFxuICB9O1xufTtcblxuY29uc3QgZ2V0VHJhY2VUYXNrRmlsdGVycyA9ICh7IGR1ZUF0UmFuZ2UgPSBcIlwiLCBjcmVhdGVkQXRSYW5nZSA9IFwiXCIgfSkgPT4ge1xuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBnZXRTZWxlY3RUYXNrVHlwZUl0ZW0oXCJcIiksXG4gICAgICB7XG4gICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5DSEVDS0JPWCxcbiAgICAgICAgcHJvcHM6IHtcbiAgICAgICAgICBsYWJlbDogXCJSZXN1bHQgb2YgRXhlY3V0aW9uXCIsXG4gICAgICAgICAgZmllbGQ6IFwic3RhdHVzXCIsXG4gICAgICAgICAgb3B0aW9uczogW1xuICAgICAgICAgICAgeyBsYWJlbDogXCJTdWNjZXNzZnVsXCIsIHZhbHVlOiBcIlNVQ0NcIiB9LFxuICAgICAgICAgICAgeyBsYWJlbDogXCJGYWlsXCIsIHZhbHVlOiBcIkZBSUxcIiB9LFxuICAgICAgICAgICAgeyBsYWJlbDogXCJQZW5kaW5nXCIsIHZhbHVlOiBcIlBFTkRJTkdcIiB9LFxuICAgICAgICAgICAgeyBsYWJlbDogXCJFeGVjdXRpbmdcIiwgdmFsdWU6IFwiSU5fUEdSRVNcIiB9LFxuICAgICAgICAgIF0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgZ2V0Q2hlY2tCb3hEYXRlUmFuZ2VJdGVtKHsgbGFiZWw6IFwiRHVlIERhdGUgb2YgVGFza1wiLCBmaWVsZDogXCJkdWVBdFJhbmdlXCIsIHZhbHVlOiBkdWVBdFJhbmdlIH0pLFxuICAgICAgZ2V0Q2hlY2tCb3hEYXRlUmFuZ2VJdGVtKHsgbGFiZWw6IFwiQ3JlYXRpb24gRGF0YSBvZiBUYXNrXCIsIGZpZWxkOiBcImNyZWF0ZWRBdFJhbmdlXCIsIHZhbHVlOiBjcmVhdGVkQXRSYW5nZSB9KSxcbiAgICBdLFxuICB9O1xufTtcblxuY29uc3QgZ2V0Sm9iUXVlcnlGaWx0ZXJzID0gKHsgc3RhcnRBdFJhbmdlID0gXCJcIiB9KSA9PiB7XG4gIHJldHVybiB7XG4gICAgaXRlbXM6IFtcbiAgICAgIGdldFNlbGVjdEpvYkl0ZW0oXCJcIiksXG4gICAgICB7XG4gICAgICAgIHR5cGU6IEZpbHRlckl0ZW1UeXBlcy5DSEVDS0JPWCxcbiAgICAgICAgcHJvcHM6IHtcbiAgICAgICAgICBsYWJlbDogXCJSZXN1bHQgb2YgRXhlY3V0aW9uXCIsXG4gICAgICAgICAgZmllbGQ6IFwiam9iU3RhdHVzXCIsXG4gICAgICAgICAgb3B0aW9uczogW1xuICAgICAgICAgICAgeyBsYWJlbDogXCJJbiBQcm9ncmVzc1wiLCB2YWx1ZTogXCJJTl9QR1JFU1wiIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiBcIlN1Y2Nlc3NmdWxcIiwgdmFsdWU6IFwiU1VDQ1wiIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiBcIkZhaWxcIiwgdmFsdWU6IFwiRkFJTFwiIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiBcImtpbGxlZFwiLCB2YWx1ZTogXCJLSUxMRURcIiB9LFxuICAgICAgICAgIF0sXG4gICAgICAgIH0sXG4gICAgICB9LFxuICAgICAgZ2V0Q2hlY2tCb3hEYXRlUmFuZ2VJdGVtKHsgbGFiZWw6IFwiRGF0ZSBvZiBFeGVjdXRpb25cIiwgZmllbGQ6IFwic3RhcnRBdFJhbmdlXCIsIHZhbHVlOiBzdGFydEF0UmFuZ2UgfSksXG4gICAgXSxcbiAgfTtcbn07XG5cbmNvbnN0IGdldE1lc3NhZ2VzRmlsdGVycyA9ICh7IHN0YXJ0QXRSYW5nZSA9IFwiXCIgfSkgPT4ge1xuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBnZXRDaGVja2JveFN0YXR1c0l0ZW0oXCJcIiksXG4gICAgICBnZXRDaGVja0JveERhdGVSYW5nZUl0ZW0oeyBsYWJlbDogXCJDcmVhdGVkIG9uXCIsIGZpZWxkOiBcImNyZWF0aW9uRGF0ZVJhbmdlXCIsIHZhbHVlOiBzdGFydEF0UmFuZ2UgfSksXG4gICAgICBnZXRDaGVja0JveERhdGVSYW5nZUl0ZW0oeyBsYWJlbDogXCJQdWJsaXNoZWQgb25cIiwgZmllbGQ6IFwicHVibGlzaERhdGVSYW5nZVwiLCB2YWx1ZTogc3RhcnRBdFJhbmdlIH0pLFxuICAgIF0sXG4gIH07XG59O1xuXG5jb25zdCBnZXRFbWFpbFR5cGVJdGVtID0gKGRlZmF1bHRWYWx1ZTogc3RyaW5nKSA9PiB7XG4gIHJldHVybiB7XG4gICAgdHlwZTogRmlsdGVySXRlbVR5cGVzLkNIRUNLQk9YLFxuICAgIHByb3BzOiB7XG4gICAgICBsYWJlbDogXCJGaWx0ZXJcIixcbiAgICAgIGZpZWxkOiBcInJlYWRTdGF0dXNcIixcbiAgICAgIHZhbHVlOiBoYW5kbGVEZWZhdWx0VmFsdWUoZGVmYXVsdFZhbHVlKSxcbiAgICAgIG9wdGlvbnM6IFtcbiAgICAgICAgeyBsYWJlbDogXCJBbGxcIiwgdmFsdWU6IFwiYWxsXCIgfSxcbiAgICAgICAgeyBsYWJlbDogXCJVbnJlYWRcIiwgdmFsdWU6IFwidW5yZWFkXCIgfSxcbiAgICAgICAgeyBsYWJlbDogXCJSZWFkXCIsIHZhbHVlOiBcInJlYWRcIiB9LFxuICAgICAgXSxcbiAgICB9LFxuICB9O1xufTtcblxuY29uc3QgZ2V0RW1haWxGaWx0ZXJzID0gKHsgZGVmYXVsdFZhbHVlID0gXCJcIiB9KSA9PiB7XG4gIHJldHVybiB7XG4gICAgaXRlbXM6IFtcbiAgICAgIGdldEVtYWlsVHlwZUl0ZW0oZGVmYXVsdFZhbHVlKSxcbiAgICBdLFxuICB9O1xufTtcblxuY29uc3QgZ2V0UXVlcnlBdWRpdExvZ0ZpbHRlcnMgPSAoeyBvcGVyVHlwZSA9IFwiXCIsIGFjY2Vzc2VkQXRSYW5nZSA9IFwiXCIgfSkgPT4ge1xuICByZXR1cm4ge1xuICAgIGl0ZW1zOiBbXG4gICAgICBnZXRDaGVja2JveE9wZXJhdGlvbkl0ZW0ob3BlclR5cGUpLFxuICAgICAgZ2V0Q2hlY2tCb3hEYXRlUmFuZ2VJdGVtKHtcbiAgICAgICAgbGFiZWw6IFwiRGF0ZSBvZiBPcGVyYXRpb25cIixcbiAgICAgICAgZmllbGQ6IFwiYWNjZXNzZWRBdFJhbmdlXCIsXG4gICAgICAgIHZhbHVlOiBhY2Nlc3NlZEF0UmFuZ2UsXG4gICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgfSksXG4gICAgXSxcbiAgfTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgZ2V0VGVuYW50RmlsdGVycyxcbiAgZ2V0SGllcmFyY2h5RmlsdGVycyxcbiAgZ2V0VXNlckFjY291bnRzRmlsdGVycyxcbiAgZ2V0U2FsZXNDaGFubmVsc0ZpbHRlcnMsXG4gIGdldEFncmVlbWVudHNGaWx0ZXJzLFxuICBnZXRVc2VyU2Vzc2lvbkZpbHRlcnMsXG4gIGdldFRyYWNlVGFza0ZpbHRlcnMsXG4gIGdldFF1ZXJ5QXVkaXRMb2dGaWx0ZXJzLFxuICBnZXRKb2JRdWVyeUZpbHRlcnMsXG4gIGdldFNlbGVjdFRlbmFudEl0ZW0sXG4gIGdldENoZWNrQm94VmFsaWRJdGVtLFxuICBnZXRTZWxlY3RUZW5hbnRRdWVyeUVudGl0eUl0ZW0sXG4gIGdldE1lc3NhZ2VzRmlsdGVycyxcbiAgZ2V0RW1haWxGaWx0ZXJzLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBRkE7QUFTQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBUEE7QUFGQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBRkE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUZBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFGQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBRkE7QUFTQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFGQTtBQUNBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBSkE7QUFGQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUZBO0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBRkE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBRkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBRkE7QUFIQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQUhBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBRkE7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUhBO0FBSEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBRkE7QUFZQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUhBO0FBSEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUZBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUZBO0FBWUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUhBO0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQVBBO0FBRkE7QUFhQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBakJBO0FBb0JBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQVBBO0FBRkE7QUFhQTtBQUFBO0FBQUE7QUFBQTtBQWhCQTtBQW1CQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBUEE7QUFGQTtBQWFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBSEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBZEEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/admin/common/filters.tsx
