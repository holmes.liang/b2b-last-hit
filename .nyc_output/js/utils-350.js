__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "log", function() { return log; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isIconDefinition", function() { return isIconDefinition; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizeAttrs", function() { return normalizeAttrs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiniMap", function() { return MiniMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generate", function() { return generate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSecondaryColor", function() { return getSecondaryColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withSuffix", function() { return withSuffix; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ant_design_colors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ant-design/colors */ "./node_modules/@ant-design/colors/lib/index.js");
/* harmony import */ var _ant_design_colors__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ant_design_colors__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);





function log(message) {
  if (!(process && Object({"NODE_ENV":"development","PUBLIC_URL":"","REACT_APP_DEFAULT_TITLE":"Bytesforce","REACT_APP_HEAD_LOGO_URL":"https://upload.cc/i1/2019/01/01/IsUmrC.png","REACT_APP_AJAX_SERVER_PORT":"4000","REACT_APP_ENV_NAME":"Local","REACT_APP_THEME":"IncomeOrange","REACT_APP_AJAX_SERVER_HOST":"http://localhost"}) && "development" === 'production')) {
    console.error('[@ant-design/icons-react]: ' + message + '.');
  }
}
function isIconDefinition(target) {
  return typeof target === 'object' && typeof target.name === 'string' && typeof target.theme === 'string' && (typeof target.icon === 'object' || typeof target.icon === 'function');
}
function normalizeAttrs() {
  var attrs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  return Object.keys(attrs).reduce(function (acc, key) {
    var val = attrs[key];

    switch (key) {
      case 'class':
        acc.className = val;
        delete acc['class'];
        break;

      default:
        acc[key] = val;
    }

    return acc;
  }, {});
}
var MiniMap = function () {
  function MiniMap() {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, MiniMap);

    this.collection = {};
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(MiniMap, [{
    key: 'clear',
    value: function clear() {
      this.collection = {};
    }
  }, {
    key: 'delete',
    value: function _delete(key) {
      return delete this.collection[key];
    }
  }, {
    key: 'get',
    value: function get(key) {
      return this.collection[key];
    }
  }, {
    key: 'has',
    value: function has(key) {
      return Boolean(this.collection[key]);
    }
  }, {
    key: 'set',
    value: function set(key, value) {
      this.collection[key] = value;
      return this;
    }
  }, {
    key: 'size',
    get: function get() {
      return Object.keys(this.collection).length;
    }
  }]);

  return MiniMap;
}();
function generate(node, key, rootProps) {
  if (!rootProps) {
    return react__WEBPACK_IMPORTED_MODULE_4__["createElement"](node.tag, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      key: key
    }, normalizeAttrs(node.attrs)), (node.children || []).map(function (child, index) {
      return generate(child, key + '-' + node.tag + '-' + index);
    }));
  }

  return react__WEBPACK_IMPORTED_MODULE_4__["createElement"](node.tag, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
    key: key
  }, normalizeAttrs(node.attrs), rootProps), (node.children || []).map(function (child, index) {
    return generate(child, key + '-' + node.tag + '-' + index);
  }));
}
function getSecondaryColor(primaryColor) {
  // choose the second color
  return Object(_ant_design_colors__WEBPACK_IMPORTED_MODULE_3__["generate"])(primaryColor)[0];
}
function withSuffix(name, theme) {
  switch (theme) {
    case 'fill':
      return name + '-fill';

    case 'outline':
      return name + '-o';

    case 'twotone':
      return name + '-twotone';

    default:
      throw new TypeError('Unknown theme type: ' + theme + ', name: ' + name);
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../process/browser.js */ "./node_modules/process/browser.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvQGFudC1kZXNpZ24vaWNvbnMtcmVhY3QvZXMvdXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9AYW50LWRlc2lnbi9pY29ucy1yZWFjdC9lcy91dGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9jcmVhdGVDbGFzcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnO1xuaW1wb3J0IHsgZ2VuZXJhdGUgYXMgZ2VuZXJhdGVDb2xvciB9IGZyb20gJ0BhbnQtZGVzaWduL2NvbG9ycyc7XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5leHBvcnQgZnVuY3Rpb24gbG9nKG1lc3NhZ2UpIHtcbiAgICBpZiAoIShwcm9jZXNzICYmIHByb2Nlc3MuZW52ICYmIHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicpKSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1tAYW50LWRlc2lnbi9pY29ucy1yZWFjdF06ICcgKyBtZXNzYWdlICsgJy4nKTtcbiAgICB9XG59XG5leHBvcnQgZnVuY3Rpb24gaXNJY29uRGVmaW5pdGlvbih0YXJnZXQpIHtcbiAgICByZXR1cm4gdHlwZW9mIHRhcmdldCA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIHRhcmdldC5uYW1lID09PSAnc3RyaW5nJyAmJiB0eXBlb2YgdGFyZ2V0LnRoZW1lID09PSAnc3RyaW5nJyAmJiAodHlwZW9mIHRhcmdldC5pY29uID09PSAnb2JqZWN0JyB8fCB0eXBlb2YgdGFyZ2V0Lmljb24gPT09ICdmdW5jdGlvbicpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIG5vcm1hbGl6ZUF0dHJzKCkge1xuICAgIHZhciBhdHRycyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG5cbiAgICByZXR1cm4gT2JqZWN0LmtleXMoYXR0cnMpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBrZXkpIHtcbiAgICAgICAgdmFyIHZhbCA9IGF0dHJzW2tleV07XG4gICAgICAgIHN3aXRjaCAoa2V5KSB7XG4gICAgICAgICAgICBjYXNlICdjbGFzcyc6XG4gICAgICAgICAgICAgICAgYWNjLmNsYXNzTmFtZSA9IHZhbDtcbiAgICAgICAgICAgICAgICBkZWxldGUgYWNjWydjbGFzcyddO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBhY2Nba2V5XSA9IHZhbDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYWNjO1xuICAgIH0sIHt9KTtcbn1cbmV4cG9ydCB2YXIgTWluaU1hcCA9IGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBNaW5pTWFwKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTWluaU1hcCk7XG5cbiAgICAgICAgdGhpcy5jb2xsZWN0aW9uID0ge307XG4gICAgfVxuXG4gICAgX2NyZWF0ZUNsYXNzKE1pbmlNYXAsIFt7XG4gICAgICAgIGtleTogJ2NsZWFyJyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNsZWFyKCkge1xuICAgICAgICAgICAgdGhpcy5jb2xsZWN0aW9uID0ge307XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogJ2RlbGV0ZScsXG4gICAgICAgIHZhbHVlOiBmdW5jdGlvbiBfZGVsZXRlKGtleSkge1xuICAgICAgICAgICAgcmV0dXJuIGRlbGV0ZSB0aGlzLmNvbGxlY3Rpb25ba2V5XTtcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiAnZ2V0JyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGdldChrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbGxlY3Rpb25ba2V5XTtcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiAnaGFzJyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIGhhcyhrZXkpIHtcbiAgICAgICAgICAgIHJldHVybiBCb29sZWFuKHRoaXMuY29sbGVjdGlvbltrZXldKTtcbiAgICAgICAgfVxuICAgIH0sIHtcbiAgICAgICAga2V5OiAnc2V0JyxcbiAgICAgICAgdmFsdWU6IGZ1bmN0aW9uIHNldChrZXksIHZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLmNvbGxlY3Rpb25ba2V5XSA9IHZhbHVlO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgIH1cbiAgICB9LCB7XG4gICAgICAgIGtleTogJ3NpemUnLFxuICAgICAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmNvbGxlY3Rpb24pLmxlbmd0aDtcbiAgICAgICAgfVxuICAgIH1dKTtcblxuICAgIHJldHVybiBNaW5pTWFwO1xufSgpO1xuZXhwb3J0IGZ1bmN0aW9uIGdlbmVyYXRlKG5vZGUsIGtleSwgcm9vdFByb3BzKSB7XG4gICAgaWYgKCFyb290UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQobm9kZS50YWcsIF9leHRlbmRzKHsga2V5OiBrZXkgfSwgbm9ybWFsaXplQXR0cnMobm9kZS5hdHRycykpLCAobm9kZS5jaGlsZHJlbiB8fCBbXSkubWFwKGZ1bmN0aW9uIChjaGlsZCwgaW5kZXgpIHtcbiAgICAgICAgICAgIHJldHVybiBnZW5lcmF0ZShjaGlsZCwga2V5ICsgJy0nICsgbm9kZS50YWcgKyAnLScgKyBpbmRleCk7XG4gICAgICAgIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQobm9kZS50YWcsIF9leHRlbmRzKHtcbiAgICAgICAga2V5OiBrZXlcbiAgICB9LCBub3JtYWxpemVBdHRycyhub2RlLmF0dHJzKSwgcm9vdFByb3BzKSwgKG5vZGUuY2hpbGRyZW4gfHwgW10pLm1hcChmdW5jdGlvbiAoY2hpbGQsIGluZGV4KSB7XG4gICAgICAgIHJldHVybiBnZW5lcmF0ZShjaGlsZCwga2V5ICsgJy0nICsgbm9kZS50YWcgKyAnLScgKyBpbmRleCk7XG4gICAgfSkpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldFNlY29uZGFyeUNvbG9yKHByaW1hcnlDb2xvcikge1xuICAgIC8vIGNob29zZSB0aGUgc2Vjb25kIGNvbG9yXG4gICAgcmV0dXJuIGdlbmVyYXRlQ29sb3IocHJpbWFyeUNvbG9yKVswXTtcbn1cbmV4cG9ydCBmdW5jdGlvbiB3aXRoU3VmZml4KG5hbWUsIHRoZW1lKSB7XG4gICAgc3dpdGNoICh0aGVtZSkge1xuICAgICAgICBjYXNlICdmaWxsJzpcbiAgICAgICAgICAgIHJldHVybiBuYW1lICsgJy1maWxsJztcbiAgICAgICAgY2FzZSAnb3V0bGluZSc6XG4gICAgICAgICAgICByZXR1cm4gbmFtZSArICctbyc7XG4gICAgICAgIGNhc2UgJ3R3b3RvbmUnOlxuICAgICAgICAgICAgcmV0dXJuIG5hbWUgKyAnLXR3b3RvbmUnO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignVW5rbm93biB0aGVtZSB0eXBlOiAnICsgdGhlbWUgKyAnLCBuYW1lOiAnICsgbmFtZSk7XG4gICAgfVxufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQU5BO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBUkE7QUFVQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/@ant-design/icons-react/es/utils.js
