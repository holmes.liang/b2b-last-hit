__webpack_require__.r(__webpack_exports__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var _range_calendar_CalendarPart__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./range-calendar/CalendarPart */ "./node_modules/rc-calendar/es/range-calendar/CalendarPart.js");
/* harmony import */ var _calendar_TodayButton__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./calendar/TodayButton */ "./node_modules/rc-calendar/es/calendar/TodayButton.js");
/* harmony import */ var _calendar_OkButton__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./calendar/OkButton */ "./node_modules/rc-calendar/es/calendar/OkButton.js");
/* harmony import */ var _calendar_TimePickerButton__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./calendar/TimePickerButton */ "./node_modules/rc-calendar/es/calendar/TimePickerButton.js");
/* harmony import */ var _mixin_CommonMixin__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./mixin/CommonMixin */ "./node_modules/rc-calendar/es/mixin/CommonMixin.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./util */ "./node_modules/rc-calendar/es/util/index.js");
/* harmony import */ var _util_toTime__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./util/toTime */ "./node_modules/rc-calendar/es/util/toTime.js");


















function noop() {}

function isEmptyArray(arr) {
  return Array.isArray(arr) && (arr.length === 0 || arr.every(function (i) {
    return !i;
  }));
}

function isArraysEqual(a, b) {
  if (a === b) return true;

  if (a === null || typeof a === 'undefined' || b === null || typeof b === 'undefined') {
    return false;
  }

  if (a.length !== b.length) return false;

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }

  return true;
}

function getValueFromSelectedValue(selectedValue) {
  var start = selectedValue[0],
      end = selectedValue[1];

  if (end && (start === undefined || start === null)) {
    start = end.clone().subtract(1, 'month');
  }

  if (start && (end === undefined || end === null)) {
    end = start.clone().add(1, 'month');
  }

  return [start, end];
}

function normalizeAnchor(props, init) {
  var selectedValue = props.selectedValue || init && props.defaultSelectedValue;
  var value = props.value || init && props.defaultValue;
  var normalizedValue = value ? getValueFromSelectedValue(value) : getValueFromSelectedValue(selectedValue);
  return !isEmptyArray(normalizedValue) ? normalizedValue : init && [moment__WEBPACK_IMPORTED_MODULE_6___default()(), moment__WEBPACK_IMPORTED_MODULE_6___default()().add(1, 'months')];
}

function generateOptions(length, extraOptionGen) {
  var arr = extraOptionGen ? extraOptionGen().concat() : [];

  for (var value = 0; value < length; value++) {
    if (arr.indexOf(value) === -1) {
      arr.push(value);
    }
  }

  return arr;
}

function onInputSelect(direction, value, cause) {
  if (!value) {
    return;
  }

  var originalValue = this.state.selectedValue;
  var selectedValue = originalValue.concat();
  var index = direction === 'left' ? 0 : 1;
  selectedValue[index] = value;

  if (selectedValue[0] && this.compare(selectedValue[0], selectedValue[1]) > 0) {
    selectedValue[1 - index] = this.state.showTimePicker ? selectedValue[index] : undefined;
  }

  this.props.onInputSelect(selectedValue);
  this.fireSelectValueChange(selectedValue, null, cause || {
    source: 'dateInput'
  });
}

var RangeCalendar = function (_React$Component) {
  babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(RangeCalendar, _React$Component);

  function RangeCalendar(props) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, RangeCalendar);

    var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2___default()(this, _React$Component.call(this, props));

    _initialiseProps.call(_this);

    var selectedValue = props.selectedValue || props.defaultSelectedValue;
    var value = normalizeAnchor(props, 1);
    _this.state = {
      selectedValue: selectedValue,
      prevSelectedValue: selectedValue,
      firstSelectedValue: null,
      hoverValue: props.hoverValue || [],
      value: value,
      showTimePicker: false,
      mode: props.mode || ['date', 'date'],
      panelTriggerSource: '' // Trigger by which picker panel: 'start' & 'end'

    };
    return _this;
  }

  RangeCalendar.getDerivedStateFromProps = function getDerivedStateFromProps(nextProps, state) {
    var newState = {};

    if ('value' in nextProps) {
      newState.value = normalizeAnchor(nextProps, 0);
    }

    if ('hoverValue' in nextProps && !isArraysEqual(state.hoverValue, nextProps.hoverValue)) {
      newState.hoverValue = nextProps.hoverValue;
    }

    if ('selectedValue' in nextProps) {
      newState.selectedValue = nextProps.selectedValue;
      newState.prevSelectedValue = nextProps.selectedValue;
    }

    if ('mode' in nextProps && !isArraysEqual(state.mode, nextProps.mode)) {
      newState.mode = nextProps.mode;
    }

    return newState;
  }; // get disabled hours for second picker


  RangeCalendar.prototype.render = function render() {
    var _className, _classnames;

    var props = this.props,
        state = this.state;
    var prefixCls = props.prefixCls,
        dateInputPlaceholder = props.dateInputPlaceholder,
        seperator = props.seperator,
        timePicker = props.timePicker,
        showOk = props.showOk,
        locale = props.locale,
        showClear = props.showClear,
        showToday = props.showToday,
        type = props.type,
        clearIcon = props.clearIcon;
    var hoverValue = state.hoverValue,
        selectedValue = state.selectedValue,
        mode = state.mode,
        showTimePicker = state.showTimePicker;
    var className = (_className = {}, _className[props.className] = !!props.className, _className[prefixCls] = 1, _className[prefixCls + '-hidden'] = !props.visible, _className[prefixCls + '-range'] = 1, _className[prefixCls + '-show-time-picker'] = showTimePicker, _className[prefixCls + '-week-number'] = props.showWeekNumber, _className);
    var classes = classnames__WEBPACK_IMPORTED_MODULE_7___default()(className);
    var newProps = {
      selectedValue: state.selectedValue,
      onSelect: this.onSelect,
      onDayHover: type === 'start' && selectedValue[1] || type === 'end' && selectedValue[0] || !!hoverValue.length ? this.onDayHover : undefined
    };
    var placeholder1 = void 0;
    var placeholder2 = void 0;

    if (dateInputPlaceholder) {
      if (Array.isArray(dateInputPlaceholder)) {
        placeholder1 = dateInputPlaceholder[0];
        placeholder2 = dateInputPlaceholder[1];
      } else {
        placeholder1 = placeholder2 = dateInputPlaceholder;
      }
    }

    var showOkButton = showOk === true || showOk !== false && !!timePicker;
    var cls = classnames__WEBPACK_IMPORTED_MODULE_7___default()((_classnames = {}, _classnames[prefixCls + '-footer'] = true, _classnames[prefixCls + '-range-bottom'] = true, _classnames[prefixCls + '-footer-show-ok'] = showOkButton, _classnames));
    var startValue = this.getStartValue();
    var endValue = this.getEndValue();
    var todayTime = Object(_util__WEBPACK_IMPORTED_MODULE_15__["getTodayTime"])(startValue);
    var thisMonth = todayTime.month();
    var thisYear = todayTime.year();
    var isTodayInView = startValue.year() === thisYear && startValue.month() === thisMonth || endValue.year() === thisYear && endValue.month() === thisMonth;
    var nextMonthOfStart = startValue.clone().add(1, 'months');
    var isClosestMonths = nextMonthOfStart.year() === endValue.year() && nextMonthOfStart.month() === endValue.month();
    var extraFooter = props.renderFooter();
    return react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      ref: this.saveRoot,
      className: classes,
      style: props.style,
      tabIndex: '0',
      onKeyDown: this.onKeyDown
    }, props.renderSidebar(), react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: prefixCls + '-panel'
    }, showClear && selectedValue[0] && selectedValue[1] ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('a', {
      role: 'button',
      title: locale.clear,
      onClick: this.clear
    }, clearIcon || react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('span', {
      className: prefixCls + '-clear-btn'
    })) : null, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: prefixCls + '-date-panel',
      onMouseLeave: type !== 'both' ? this.onDatePanelLeave : undefined,
      onMouseEnter: type !== 'both' ? this.onDatePanelEnter : undefined
    }, react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_range_calendar_CalendarPart__WEBPACK_IMPORTED_MODULE_10__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, newProps, {
      hoverValue: hoverValue,
      direction: 'left',
      disabledTime: this.disabledStartTime,
      disabledMonth: this.disabledStartMonth,
      format: this.getFormat(),
      value: startValue,
      mode: mode[0],
      placeholder: placeholder1,
      onInputChange: this.onStartInputChange,
      onInputSelect: this.onStartInputSelect,
      onValueChange: this.onStartValueChange,
      onPanelChange: this.onStartPanelChange,
      showDateInput: this.props.showDateInput,
      timePicker: timePicker,
      showTimePicker: showTimePicker || mode[0] === 'time',
      enablePrev: true,
      enableNext: !isClosestMonths || this.isMonthYearPanelShow(mode[1]),
      clearIcon: clearIcon
    })), react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('span', {
      className: prefixCls + '-range-middle'
    }, seperator), react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_range_calendar_CalendarPart__WEBPACK_IMPORTED_MODULE_10__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, newProps, {
      hoverValue: hoverValue,
      direction: 'right',
      format: this.getFormat(),
      timePickerDisabledTime: this.getEndDisableTime(),
      placeholder: placeholder2,
      value: endValue,
      mode: mode[1],
      onInputChange: this.onEndInputChange,
      onInputSelect: this.onEndInputSelect,
      onValueChange: this.onEndValueChange,
      onPanelChange: this.onEndPanelChange,
      showDateInput: this.props.showDateInput,
      timePicker: timePicker,
      showTimePicker: showTimePicker || mode[1] === 'time',
      disabledTime: this.disabledEndTime,
      disabledMonth: this.disabledEndMonth,
      enablePrev: !isClosestMonths || this.isMonthYearPanelShow(mode[0]),
      enableNext: true,
      clearIcon: clearIcon
    }))), react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: cls
    }, showToday || props.timePicker || showOkButton || extraFooter ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement('div', {
      className: prefixCls + '-footer-btn'
    }, extraFooter, showToday ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_calendar_TodayButton__WEBPACK_IMPORTED_MODULE_11__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, {
      disabled: isTodayInView,
      value: state.value[0],
      onToday: this.onToday,
      text: locale.backToToday
    })) : null, props.timePicker ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_calendar_TimePickerButton__WEBPACK_IMPORTED_MODULE_13__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, {
      showTimePicker: showTimePicker || mode[0] === 'time' && mode[1] === 'time',
      onOpenTimePicker: this.onOpenTimePicker,
      onCloseTimePicker: this.onCloseTimePicker,
      timePickerDisabled: !this.hasSelectedValue() || hoverValue.length
    })) : null, showOkButton ? react__WEBPACK_IMPORTED_MODULE_4___default.a.createElement(_calendar_OkButton__WEBPACK_IMPORTED_MODULE_12__["default"], babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, props, {
      onOk: this.onOk,
      okDisabled: !this.isAllowedDateAndTime(selectedValue) || !this.hasSelectedValue() || hoverValue.length
    })) : null) : null)));
  };

  return RangeCalendar;
}(react__WEBPACK_IMPORTED_MODULE_4___default.a.Component);

RangeCalendar.propTypes = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _mixin_CommonMixin__WEBPACK_IMPORTED_MODULE_14__["propType"], {
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  dateInputPlaceholder: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  seperator: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string,
  defaultValue: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  value: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  hoverValue: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  mode: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.oneOf(['time', 'date', 'month', 'year', 'decade'])),
  showDateInput: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  timePicker: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  showOk: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  showToday: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  defaultSelectedValue: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.array,
  selectedValue: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.array,
  onOk: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  showClear: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.bool,
  locale: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.object,
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onValueChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onHoverChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  onPanelChange: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  format: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.arrayOf(prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.string)]),
  onClear: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  type: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.any,
  disabledDate: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  disabledTime: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func,
  clearIcon: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.node,
  onKeyDown: prop_types__WEBPACK_IMPORTED_MODULE_5___default.a.func
});
RangeCalendar.defaultProps = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, _mixin_CommonMixin__WEBPACK_IMPORTED_MODULE_14__["defaultProp"], {
  type: 'both',
  seperator: '~',
  defaultSelectedValue: [],
  onValueChange: noop,
  onHoverChange: noop,
  onPanelChange: noop,
  disabledTime: noop,
  onInputSelect: noop,
  showToday: true,
  showDateInput: true
});

var _initialiseProps = function _initialiseProps() {
  var _this2 = this;

  this.onDatePanelEnter = function () {
    if (_this2.hasSelectedValue()) {
      _this2.fireHoverValueChange(_this2.state.selectedValue.concat());
    }
  };

  this.onDatePanelLeave = function () {
    if (_this2.hasSelectedValue()) {
      _this2.fireHoverValueChange([]);
    }
  };

  this.onSelect = function (value) {
    var type = _this2.props.type;
    var _state = _this2.state,
        selectedValue = _state.selectedValue,
        prevSelectedValue = _state.prevSelectedValue,
        firstSelectedValue = _state.firstSelectedValue;
    var nextSelectedValue = void 0;

    if (type === 'both') {
      if (!firstSelectedValue) {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[0], value);
        nextSelectedValue = [value];
      } else if (_this2.compare(firstSelectedValue, value) < 0) {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[1], value);
        nextSelectedValue = [firstSelectedValue, value];
      } else {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[0], value);
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[1], firstSelectedValue);
        nextSelectedValue = [value, firstSelectedValue];
      }
    } else if (type === 'start') {
      Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[0], value);
      var endValue = selectedValue[1];
      nextSelectedValue = endValue && _this2.compare(endValue, value) > 0 ? [value, endValue] : [value];
    } else {
      // type === 'end'
      var startValue = selectedValue[0];

      if (startValue && _this2.compare(startValue, value) <= 0) {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[1], value);
        nextSelectedValue = [startValue, value];
      } else {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(prevSelectedValue[0], value);
        nextSelectedValue = [value];
      }
    }

    _this2.fireSelectValueChange(nextSelectedValue);
  };

  this.onKeyDown = function (event) {
    if (event.target.nodeName.toLowerCase() === 'input') {
      return;
    }

    var keyCode = event.keyCode;
    var ctrlKey = event.ctrlKey || event.metaKey;
    var _state2 = _this2.state,
        selectedValue = _state2.selectedValue,
        hoverValue = _state2.hoverValue,
        firstSelectedValue = _state2.firstSelectedValue,
        value = _state2.value;
    var _props = _this2.props,
        onKeyDown = _props.onKeyDown,
        disabledDate = _props.disabledDate; // Update last time of the picker

    var updateHoverPoint = function updateHoverPoint(func) {
      // Change hover to make focus in UI
      var currentHoverTime = void 0;
      var nextHoverTime = void 0;
      var nextHoverValue = void 0;

      if (!firstSelectedValue) {
        currentHoverTime = hoverValue[0] || selectedValue[0] || value[0] || moment__WEBPACK_IMPORTED_MODULE_6___default()();
        nextHoverTime = func(currentHoverTime);
        nextHoverValue = [nextHoverTime];

        _this2.fireHoverValueChange(nextHoverValue);
      } else {
        if (hoverValue.length === 1) {
          currentHoverTime = hoverValue[0].clone();
          nextHoverTime = func(currentHoverTime);
          nextHoverValue = _this2.onDayHover(nextHoverTime);
        } else {
          currentHoverTime = hoverValue[0].isSame(firstSelectedValue, 'day') ? hoverValue[1] : hoverValue[0];
          nextHoverTime = func(currentHoverTime);
          nextHoverValue = _this2.onDayHover(nextHoverTime);
        }
      } // Find origin hover time on value index


      if (nextHoverValue.length >= 2) {
        var miss = nextHoverValue.some(function (ht) {
          return !Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["includesTime"])(value, ht, 'month');
        });

        if (miss) {
          var newValue = nextHoverValue.slice().sort(function (t1, t2) {
            return t1.valueOf() - t2.valueOf();
          });

          if (newValue[0].isSame(newValue[1], 'month')) {
            newValue[1] = newValue[0].clone().add(1, 'month');
          }

          _this2.fireValueChange(newValue);
        }
      } else if (nextHoverValue.length === 1) {
        // If only one value, let's keep the origin panel
        var oriValueIndex = value.findIndex(function (time) {
          return time.isSame(currentHoverTime, 'month');
        });
        if (oriValueIndex === -1) oriValueIndex = 0;

        if (value.every(function (time) {
          return !time.isSame(nextHoverTime, 'month');
        })) {
          var _newValue = value.slice();

          _newValue[oriValueIndex] = nextHoverTime.clone();

          _this2.fireValueChange(_newValue);
        }
      }

      event.preventDefault();
      return nextHoverTime;
    };

    switch (keyCode) {
      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].DOWN:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, 1, 'weeks');
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].UP:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, -1, 'weeks');
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].LEFT:
        if (ctrlKey) {
          updateHoverPoint(function (time) {
            return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, -1, 'years');
          });
        } else {
          updateHoverPoint(function (time) {
            return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, -1, 'days');
          });
        }

        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].RIGHT:
        if (ctrlKey) {
          updateHoverPoint(function (time) {
            return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, 1, 'years');
          });
        } else {
          updateHoverPoint(function (time) {
            return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, 1, 'days');
          });
        }

        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].HOME:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goStartMonth"])(time);
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].END:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goEndMonth"])(time);
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].PAGE_DOWN:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, 1, 'month');
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].PAGE_UP:
        updateHoverPoint(function (time) {
          return Object(_util_toTime__WEBPACK_IMPORTED_MODULE_16__["goTime"])(time, -1, 'month');
        });
        return;

      case rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_9__["default"].ENTER:
        {
          var lastValue = void 0;

          if (hoverValue.length === 0) {
            lastValue = updateHoverPoint(function (time) {
              return time;
            });
          } else if (hoverValue.length === 1) {
            lastValue = hoverValue[0];
          } else {
            lastValue = hoverValue[0].isSame(firstSelectedValue, 'day') ? hoverValue[1] : hoverValue[0];
          }

          if (lastValue && (!disabledDate || !disabledDate(lastValue))) {
            _this2.onSelect(lastValue);
          }

          event.preventDefault();
          return;
        }

      default:
        if (onKeyDown) {
          onKeyDown(event);
        }

    }
  };

  this.onDayHover = function (value) {
    var hoverValue = [];
    var _state3 = _this2.state,
        selectedValue = _state3.selectedValue,
        firstSelectedValue = _state3.firstSelectedValue;
    var type = _this2.props.type;

    if (type === 'start' && selectedValue[1]) {
      hoverValue = _this2.compare(value, selectedValue[1]) < 0 ? [value, selectedValue[1]] : [value];
    } else if (type === 'end' && selectedValue[0]) {
      hoverValue = _this2.compare(value, selectedValue[0]) > 0 ? [selectedValue[0], value] : [];
    } else {
      if (!firstSelectedValue) {
        if (_this2.state.hoverValue.length) {
          _this2.setState({
            hoverValue: []
          });
        }

        return hoverValue;
      }

      hoverValue = _this2.compare(value, firstSelectedValue) < 0 ? [value, firstSelectedValue] : [firstSelectedValue, value];
    }

    _this2.fireHoverValueChange(hoverValue);

    return hoverValue;
  };

  this.onToday = function () {
    var startValue = Object(_util__WEBPACK_IMPORTED_MODULE_15__["getTodayTime"])(_this2.state.value[0]);
    var endValue = startValue.clone().add(1, 'months');

    _this2.setState({
      value: [startValue, endValue]
    });
  };

  this.onOpenTimePicker = function () {
    _this2.setState({
      showTimePicker: true
    });
  };

  this.onCloseTimePicker = function () {
    _this2.setState({
      showTimePicker: false
    });
  };

  this.onOk = function () {
    var selectedValue = _this2.state.selectedValue;

    if (_this2.isAllowedDateAndTime(selectedValue)) {
      _this2.props.onOk(_this2.state.selectedValue);
    }
  };

  this.onStartInputChange = function () {
    for (var _len = arguments.length, oargs = Array(_len), _key = 0; _key < _len; _key++) {
      oargs[_key] = arguments[_key];
    }

    var args = ['left'].concat(oargs);
    return onInputSelect.apply(_this2, args);
  };

  this.onEndInputChange = function () {
    for (var _len2 = arguments.length, oargs = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      oargs[_key2] = arguments[_key2];
    }

    var args = ['right'].concat(oargs);
    return onInputSelect.apply(_this2, args);
  };

  this.onStartInputSelect = function (value) {
    var args = ['left', value, {
      source: 'dateInputSelect'
    }];
    return onInputSelect.apply(_this2, args);
  };

  this.onEndInputSelect = function (value) {
    var args = ['right', value, {
      source: 'dateInputSelect'
    }];
    return onInputSelect.apply(_this2, args);
  };

  this.onStartValueChange = function (leftValue) {
    var value = [].concat(_this2.state.value);
    value[0] = leftValue;
    return _this2.fireValueChange(value);
  };

  this.onEndValueChange = function (rightValue) {
    var value = [].concat(_this2.state.value);
    value[1] = rightValue;
    return _this2.fireValueChange(value);
  };

  this.onStartPanelChange = function (value, mode) {
    var props = _this2.props,
        state = _this2.state;
    var newMode = [mode, state.mode[1]];
    var newState = {
      panelTriggerSource: 'start'
    };

    if (!('mode' in props)) {
      newState.mode = newMode;
    }

    _this2.setState(newState);

    var newValue = [value || state.value[0], state.value[1]];
    props.onPanelChange(newValue, newMode);
  };

  this.onEndPanelChange = function (value, mode) {
    var props = _this2.props,
        state = _this2.state;
    var newMode = [state.mode[0], mode];
    var newState = {
      panelTriggerSource: 'end'
    };

    if (!('mode' in props)) {
      newState.mode = newMode;
    }

    _this2.setState(newState);

    var newValue = [state.value[0], value || state.value[1]];
    props.onPanelChange(newValue, newMode);
  };

  this.getStartValue = function () {
    var _state4 = _this2.state,
        selectedValue = _state4.selectedValue,
        showTimePicker = _state4.showTimePicker,
        value = _state4.value,
        mode = _state4.mode,
        panelTriggerSource = _state4.panelTriggerSource;
    var startValue = value[0]; // keep selectedTime when select date

    if (selectedValue[0] && _this2.props.timePicker) {
      startValue = startValue.clone();
      Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(selectedValue[0], startValue);
    }

    if (showTimePicker && selectedValue[0]) {
      startValue = selectedValue[0];
    } // Adjust month if date not align


    if (panelTriggerSource === 'end' && mode[0] === 'date' && mode[1] === 'date' && startValue.isSame(value[1], 'month')) {
      startValue = startValue.clone().subtract(1, 'month');
    }

    return startValue;
  };

  this.getEndValue = function () {
    var _state5 = _this2.state,
        value = _state5.value,
        selectedValue = _state5.selectedValue,
        showTimePicker = _state5.showTimePicker,
        mode = _state5.mode,
        panelTriggerSource = _state5.panelTriggerSource;
    var endValue = value[1] ? value[1].clone() : value[0].clone().add(1, 'month'); // keep selectedTime when select date

    if (selectedValue[1] && _this2.props.timePicker) {
      Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(selectedValue[1], endValue);
    }

    if (showTimePicker) {
      endValue = selectedValue[1] ? selectedValue[1] : _this2.getStartValue();
    } // Adjust month if date not align


    if (!showTimePicker && panelTriggerSource !== 'end' && mode[0] === 'date' && mode[1] === 'date' && endValue.isSame(value[0], 'month')) {
      endValue = endValue.clone().add(1, 'month');
    }

    return endValue;
  };

  this.getEndDisableTime = function () {
    var _state6 = _this2.state,
        selectedValue = _state6.selectedValue,
        value = _state6.value;
    var disabledTime = _this2.props.disabledTime;
    var userSettingDisabledTime = disabledTime(selectedValue, 'end') || {};
    var startValue = selectedValue && selectedValue[0] || value[0].clone(); // if startTime and endTime is same day..
    // the second time picker will not able to pick time before first time picker

    if (!selectedValue[1] || startValue.isSame(selectedValue[1], 'day')) {
      var hours = startValue.hour();
      var minutes = startValue.minute();
      var second = startValue.second();
      var _disabledHours = userSettingDisabledTime.disabledHours,
          _disabledMinutes = userSettingDisabledTime.disabledMinutes,
          _disabledSeconds = userSettingDisabledTime.disabledSeconds;
      var oldDisabledMinutes = _disabledMinutes ? _disabledMinutes() : [];
      var olddisabledSeconds = _disabledSeconds ? _disabledSeconds() : [];
      _disabledHours = generateOptions(hours, _disabledHours);
      _disabledMinutes = generateOptions(minutes, _disabledMinutes);
      _disabledSeconds = generateOptions(second, _disabledSeconds);
      return {
        disabledHours: function disabledHours() {
          return _disabledHours;
        },
        disabledMinutes: function disabledMinutes(hour) {
          if (hour === hours) {
            return _disabledMinutes;
          }

          return oldDisabledMinutes;
        },
        disabledSeconds: function disabledSeconds(hour, minute) {
          if (hour === hours && minute === minutes) {
            return _disabledSeconds;
          }

          return olddisabledSeconds;
        }
      };
    }

    return userSettingDisabledTime;
  };

  this.isAllowedDateAndTime = function (selectedValue) {
    return Object(_util__WEBPACK_IMPORTED_MODULE_15__["isAllowedDate"])(selectedValue[0], _this2.props.disabledDate, _this2.disabledStartTime) && Object(_util__WEBPACK_IMPORTED_MODULE_15__["isAllowedDate"])(selectedValue[1], _this2.props.disabledDate, _this2.disabledEndTime);
  };

  this.isMonthYearPanelShow = function (mode) {
    return ['month', 'year', 'decade'].indexOf(mode) > -1;
  };

  this.hasSelectedValue = function () {
    var selectedValue = _this2.state.selectedValue;
    return !!selectedValue[1] && !!selectedValue[0];
  };

  this.compare = function (v1, v2) {
    if (_this2.props.timePicker) {
      return v1.diff(v2);
    }

    return v1.diff(v2, 'days');
  };

  this.fireSelectValueChange = function (selectedValue, direct, cause) {
    var timePicker = _this2.props.timePicker;
    var prevSelectedValue = _this2.state.prevSelectedValue;

    if (timePicker && timePicker.props.defaultValue) {
      var timePickerDefaultValue = timePicker.props.defaultValue;

      if (!prevSelectedValue[0] && selectedValue[0]) {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(timePickerDefaultValue[0], selectedValue[0]);
      }

      if (!prevSelectedValue[1] && selectedValue[1]) {
        Object(_util__WEBPACK_IMPORTED_MODULE_15__["syncTime"])(timePickerDefaultValue[1], selectedValue[1]);
      }
    }

    if (!('selectedValue' in _this2.props)) {
      _this2.setState({
        selectedValue: selectedValue
      });
    } // 尚未选择过时间，直接输入的话


    if (!_this2.state.selectedValue[0] || !_this2.state.selectedValue[1]) {
      var startValue = selectedValue[0] || moment__WEBPACK_IMPORTED_MODULE_6___default()();
      var endValue = selectedValue[1] || startValue.clone().add(1, 'months');

      _this2.setState({
        selectedValue: selectedValue,
        value: getValueFromSelectedValue([startValue, endValue])
      });
    }

    if (selectedValue[0] && !selectedValue[1]) {
      _this2.setState({
        firstSelectedValue: selectedValue[0]
      });

      _this2.fireHoverValueChange(selectedValue.concat());
    }

    _this2.props.onChange(selectedValue);

    if (direct || selectedValue[0] && selectedValue[1]) {
      _this2.setState({
        prevSelectedValue: selectedValue,
        firstSelectedValue: null
      });

      _this2.fireHoverValueChange([]);

      _this2.props.onSelect(selectedValue, cause);
    }
  };

  this.fireValueChange = function (value) {
    var props = _this2.props;

    if (!('value' in props)) {
      _this2.setState({
        value: value
      });
    }

    props.onValueChange(value);
  };

  this.fireHoverValueChange = function (hoverValue) {
    var props = _this2.props;

    if (!('hoverValue' in props)) {
      _this2.setState({
        hoverValue: hoverValue
      });
    }

    props.onHoverChange(hoverValue);
  };

  this.clear = function () {
    _this2.fireSelectValueChange([], true);

    _this2.props.onClear();
  };

  this.disabledStartTime = function (time) {
    return _this2.props.disabledTime(time, 'start');
  };

  this.disabledEndTime = function (time) {
    return _this2.props.disabledTime(time, 'end');
  };

  this.disabledStartMonth = function (month) {
    var value = _this2.state.value;
    return month.isAfter(value[1], 'month');
  };

  this.disabledEndMonth = function (month) {
    var value = _this2.state.value;
    return month.isBefore(value[0], 'month');
  };
};

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__["polyfill"])(RangeCalendar);
/* harmony default export */ __webpack_exports__["default"] = (Object(_mixin_CommonMixin__WEBPACK_IMPORTED_MODULE_14__["commonMixinWrapper"])(RangeCalendar));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvUmFuZ2VDYWxlbmRhci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWNhbGVuZGFyL2VzL1JhbmdlQ2FsZW5kYXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybiBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybic7XG5pbXBvcnQgX2luaGVyaXRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cyc7XG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgS2V5Q29kZSBmcm9tICdyYy11dGlsL2VzL0tleUNvZGUnO1xuaW1wb3J0IENhbGVuZGFyUGFydCBmcm9tICcuL3JhbmdlLWNhbGVuZGFyL0NhbGVuZGFyUGFydCc7XG5pbXBvcnQgVG9kYXlCdXR0b24gZnJvbSAnLi9jYWxlbmRhci9Ub2RheUJ1dHRvbic7XG5pbXBvcnQgT2tCdXR0b24gZnJvbSAnLi9jYWxlbmRhci9Pa0J1dHRvbic7XG5pbXBvcnQgVGltZVBpY2tlckJ1dHRvbiBmcm9tICcuL2NhbGVuZGFyL1RpbWVQaWNrZXJCdXR0b24nO1xuaW1wb3J0IHsgY29tbW9uTWl4aW5XcmFwcGVyLCBwcm9wVHlwZSwgZGVmYXVsdFByb3AgfSBmcm9tICcuL21peGluL0NvbW1vbk1peGluJztcbmltcG9ydCB7IHN5bmNUaW1lLCBnZXRUb2RheVRpbWUsIGlzQWxsb3dlZERhdGUgfSBmcm9tICcuL3V0aWwnO1xuaW1wb3J0IHsgZ29UaW1lLCBnb1N0YXJ0TW9udGgsIGdvRW5kTW9udGgsIGluY2x1ZGVzVGltZSB9IGZyb20gJy4vdXRpbC90b1RpbWUnO1xuXG5mdW5jdGlvbiBub29wKCkge31cblxuZnVuY3Rpb24gaXNFbXB0eUFycmF5KGFycikge1xuICByZXR1cm4gQXJyYXkuaXNBcnJheShhcnIpICYmIChhcnIubGVuZ3RoID09PSAwIHx8IGFyci5ldmVyeShmdW5jdGlvbiAoaSkge1xuICAgIHJldHVybiAhaTtcbiAgfSkpO1xufVxuXG5mdW5jdGlvbiBpc0FycmF5c0VxdWFsKGEsIGIpIHtcbiAgaWYgKGEgPT09IGIpIHJldHVybiB0cnVlO1xuICBpZiAoYSA9PT0gbnVsbCB8fCB0eXBlb2YgYSA9PT0gJ3VuZGVmaW5lZCcgfHwgYiA9PT0gbnVsbCB8fCB0eXBlb2YgYiA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgaWYgKGEubGVuZ3RoICE9PSBiLmxlbmd0aCkgcmV0dXJuIGZhbHNlO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgYS5sZW5ndGg7ICsraSkge1xuICAgIGlmIChhW2ldICE9PSBiW2ldKSByZXR1cm4gZmFsc2U7XG4gIH1cbiAgcmV0dXJuIHRydWU7XG59XG5cbmZ1bmN0aW9uIGdldFZhbHVlRnJvbVNlbGVjdGVkVmFsdWUoc2VsZWN0ZWRWYWx1ZSkge1xuICB2YXIgc3RhcnQgPSBzZWxlY3RlZFZhbHVlWzBdLFxuICAgICAgZW5kID0gc2VsZWN0ZWRWYWx1ZVsxXTtcblxuICBpZiAoZW5kICYmIChzdGFydCA9PT0gdW5kZWZpbmVkIHx8IHN0YXJ0ID09PSBudWxsKSkge1xuICAgIHN0YXJ0ID0gZW5kLmNsb25lKCkuc3VidHJhY3QoMSwgJ21vbnRoJyk7XG4gIH1cblxuICBpZiAoc3RhcnQgJiYgKGVuZCA9PT0gdW5kZWZpbmVkIHx8IGVuZCA9PT0gbnVsbCkpIHtcbiAgICBlbmQgPSBzdGFydC5jbG9uZSgpLmFkZCgxLCAnbW9udGgnKTtcbiAgfVxuICByZXR1cm4gW3N0YXJ0LCBlbmRdO1xufVxuXG5mdW5jdGlvbiBub3JtYWxpemVBbmNob3IocHJvcHMsIGluaXQpIHtcbiAgdmFyIHNlbGVjdGVkVmFsdWUgPSBwcm9wcy5zZWxlY3RlZFZhbHVlIHx8IGluaXQgJiYgcHJvcHMuZGVmYXVsdFNlbGVjdGVkVmFsdWU7XG4gIHZhciB2YWx1ZSA9IHByb3BzLnZhbHVlIHx8IGluaXQgJiYgcHJvcHMuZGVmYXVsdFZhbHVlO1xuICB2YXIgbm9ybWFsaXplZFZhbHVlID0gdmFsdWUgPyBnZXRWYWx1ZUZyb21TZWxlY3RlZFZhbHVlKHZhbHVlKSA6IGdldFZhbHVlRnJvbVNlbGVjdGVkVmFsdWUoc2VsZWN0ZWRWYWx1ZSk7XG4gIHJldHVybiAhaXNFbXB0eUFycmF5KG5vcm1hbGl6ZWRWYWx1ZSkgPyBub3JtYWxpemVkVmFsdWUgOiBpbml0ICYmIFttb21lbnQoKSwgbW9tZW50KCkuYWRkKDEsICdtb250aHMnKV07XG59XG5cbmZ1bmN0aW9uIGdlbmVyYXRlT3B0aW9ucyhsZW5ndGgsIGV4dHJhT3B0aW9uR2VuKSB7XG4gIHZhciBhcnIgPSBleHRyYU9wdGlvbkdlbiA/IGV4dHJhT3B0aW9uR2VuKCkuY29uY2F0KCkgOiBbXTtcbiAgZm9yICh2YXIgdmFsdWUgPSAwOyB2YWx1ZSA8IGxlbmd0aDsgdmFsdWUrKykge1xuICAgIGlmIChhcnIuaW5kZXhPZih2YWx1ZSkgPT09IC0xKSB7XG4gICAgICBhcnIucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG4gIHJldHVybiBhcnI7XG59XG5cbmZ1bmN0aW9uIG9uSW5wdXRTZWxlY3QoZGlyZWN0aW9uLCB2YWx1ZSwgY2F1c2UpIHtcbiAgaWYgKCF2YWx1ZSkge1xuICAgIHJldHVybjtcbiAgfVxuICB2YXIgb3JpZ2luYWxWYWx1ZSA9IHRoaXMuc3RhdGUuc2VsZWN0ZWRWYWx1ZTtcbiAgdmFyIHNlbGVjdGVkVmFsdWUgPSBvcmlnaW5hbFZhbHVlLmNvbmNhdCgpO1xuICB2YXIgaW5kZXggPSBkaXJlY3Rpb24gPT09ICdsZWZ0JyA/IDAgOiAxO1xuICBzZWxlY3RlZFZhbHVlW2luZGV4XSA9IHZhbHVlO1xuICBpZiAoc2VsZWN0ZWRWYWx1ZVswXSAmJiB0aGlzLmNvbXBhcmUoc2VsZWN0ZWRWYWx1ZVswXSwgc2VsZWN0ZWRWYWx1ZVsxXSkgPiAwKSB7XG4gICAgc2VsZWN0ZWRWYWx1ZVsxIC0gaW5kZXhdID0gdGhpcy5zdGF0ZS5zaG93VGltZVBpY2tlciA/IHNlbGVjdGVkVmFsdWVbaW5kZXhdIDogdW5kZWZpbmVkO1xuICB9XG4gIHRoaXMucHJvcHMub25JbnB1dFNlbGVjdChzZWxlY3RlZFZhbHVlKTtcbiAgdGhpcy5maXJlU2VsZWN0VmFsdWVDaGFuZ2Uoc2VsZWN0ZWRWYWx1ZSwgbnVsbCwgY2F1c2UgfHwgeyBzb3VyY2U6ICdkYXRlSW5wdXQnIH0pO1xufVxuXG52YXIgUmFuZ2VDYWxlbmRhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhSYW5nZUNhbGVuZGFyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBSYW5nZUNhbGVuZGFyKHByb3BzKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFJhbmdlQ2FsZW5kYXIpO1xuXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICBfaW5pdGlhbGlzZVByb3BzLmNhbGwoX3RoaXMpO1xuXG4gICAgdmFyIHNlbGVjdGVkVmFsdWUgPSBwcm9wcy5zZWxlY3RlZFZhbHVlIHx8IHByb3BzLmRlZmF1bHRTZWxlY3RlZFZhbHVlO1xuICAgIHZhciB2YWx1ZSA9IG5vcm1hbGl6ZUFuY2hvcihwcm9wcywgMSk7XG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBzZWxlY3RlZFZhbHVlOiBzZWxlY3RlZFZhbHVlLFxuICAgICAgcHJldlNlbGVjdGVkVmFsdWU6IHNlbGVjdGVkVmFsdWUsXG4gICAgICBmaXJzdFNlbGVjdGVkVmFsdWU6IG51bGwsXG4gICAgICBob3ZlclZhbHVlOiBwcm9wcy5ob3ZlclZhbHVlIHx8IFtdLFxuICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgc2hvd1RpbWVQaWNrZXI6IGZhbHNlLFxuICAgICAgbW9kZTogcHJvcHMubW9kZSB8fCBbJ2RhdGUnLCAnZGF0ZSddLFxuICAgICAgcGFuZWxUcmlnZ2VyU291cmNlOiAnJyAvLyBUcmlnZ2VyIGJ5IHdoaWNoIHBpY2tlciBwYW5lbDogJ3N0YXJ0JyAmICdlbmQnXG4gICAgfTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBSYW5nZUNhbGVuZGFyLmdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyA9IGZ1bmN0aW9uIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIHN0YXRlKSB7XG4gICAgdmFyIG5ld1N0YXRlID0ge307XG4gICAgaWYgKCd2YWx1ZScgaW4gbmV4dFByb3BzKSB7XG4gICAgICBuZXdTdGF0ZS52YWx1ZSA9IG5vcm1hbGl6ZUFuY2hvcihuZXh0UHJvcHMsIDApO1xuICAgIH1cbiAgICBpZiAoJ2hvdmVyVmFsdWUnIGluIG5leHRQcm9wcyAmJiAhaXNBcnJheXNFcXVhbChzdGF0ZS5ob3ZlclZhbHVlLCBuZXh0UHJvcHMuaG92ZXJWYWx1ZSkpIHtcbiAgICAgIG5ld1N0YXRlLmhvdmVyVmFsdWUgPSBuZXh0UHJvcHMuaG92ZXJWYWx1ZTtcbiAgICB9XG4gICAgaWYgKCdzZWxlY3RlZFZhbHVlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgIG5ld1N0YXRlLnNlbGVjdGVkVmFsdWUgPSBuZXh0UHJvcHMuc2VsZWN0ZWRWYWx1ZTtcbiAgICAgIG5ld1N0YXRlLnByZXZTZWxlY3RlZFZhbHVlID0gbmV4dFByb3BzLnNlbGVjdGVkVmFsdWU7XG4gICAgfVxuICAgIGlmICgnbW9kZScgaW4gbmV4dFByb3BzICYmICFpc0FycmF5c0VxdWFsKHN0YXRlLm1vZGUsIG5leHRQcm9wcy5tb2RlKSkge1xuICAgICAgbmV3U3RhdGUubW9kZSA9IG5leHRQcm9wcy5tb2RlO1xuICAgIH1cbiAgICByZXR1cm4gbmV3U3RhdGU7XG4gIH07XG5cbiAgLy8gZ2V0IGRpc2FibGVkIGhvdXJzIGZvciBzZWNvbmQgcGlja2VyXG5cblxuICBSYW5nZUNhbGVuZGFyLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9jbGFzc05hbWUsIF9jbGFzc25hbWVzO1xuXG4gICAgdmFyIHByb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgc3RhdGUgPSB0aGlzLnN0YXRlO1xuICAgIHZhciBwcmVmaXhDbHMgPSBwcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgIGRhdGVJbnB1dFBsYWNlaG9sZGVyID0gcHJvcHMuZGF0ZUlucHV0UGxhY2Vob2xkZXIsXG4gICAgICAgIHNlcGVyYXRvciA9IHByb3BzLnNlcGVyYXRvcixcbiAgICAgICAgdGltZVBpY2tlciA9IHByb3BzLnRpbWVQaWNrZXIsXG4gICAgICAgIHNob3dPayA9IHByb3BzLnNob3dPayxcbiAgICAgICAgbG9jYWxlID0gcHJvcHMubG9jYWxlLFxuICAgICAgICBzaG93Q2xlYXIgPSBwcm9wcy5zaG93Q2xlYXIsXG4gICAgICAgIHNob3dUb2RheSA9IHByb3BzLnNob3dUb2RheSxcbiAgICAgICAgdHlwZSA9IHByb3BzLnR5cGUsXG4gICAgICAgIGNsZWFySWNvbiA9IHByb3BzLmNsZWFySWNvbjtcbiAgICB2YXIgaG92ZXJWYWx1ZSA9IHN0YXRlLmhvdmVyVmFsdWUsXG4gICAgICAgIHNlbGVjdGVkVmFsdWUgPSBzdGF0ZS5zZWxlY3RlZFZhbHVlLFxuICAgICAgICBtb2RlID0gc3RhdGUubW9kZSxcbiAgICAgICAgc2hvd1RpbWVQaWNrZXIgPSBzdGF0ZS5zaG93VGltZVBpY2tlcjtcblxuICAgIHZhciBjbGFzc05hbWUgPSAoX2NsYXNzTmFtZSA9IHt9LCBfY2xhc3NOYW1lW3Byb3BzLmNsYXNzTmFtZV0gPSAhIXByb3BzLmNsYXNzTmFtZSwgX2NsYXNzTmFtZVtwcmVmaXhDbHNdID0gMSwgX2NsYXNzTmFtZVtwcmVmaXhDbHMgKyAnLWhpZGRlbiddID0gIXByb3BzLnZpc2libGUsIF9jbGFzc05hbWVbcHJlZml4Q2xzICsgJy1yYW5nZSddID0gMSwgX2NsYXNzTmFtZVtwcmVmaXhDbHMgKyAnLXNob3ctdGltZS1waWNrZXInXSA9IHNob3dUaW1lUGlja2VyLCBfY2xhc3NOYW1lW3ByZWZpeENscyArICctd2Vlay1udW1iZXInXSA9IHByb3BzLnNob3dXZWVrTnVtYmVyLCBfY2xhc3NOYW1lKTtcbiAgICB2YXIgY2xhc3NlcyA9IGNsYXNzbmFtZXMoY2xhc3NOYW1lKTtcbiAgICB2YXIgbmV3UHJvcHMgPSB7XG4gICAgICBzZWxlY3RlZFZhbHVlOiBzdGF0ZS5zZWxlY3RlZFZhbHVlLFxuICAgICAgb25TZWxlY3Q6IHRoaXMub25TZWxlY3QsXG4gICAgICBvbkRheUhvdmVyOiB0eXBlID09PSAnc3RhcnQnICYmIHNlbGVjdGVkVmFsdWVbMV0gfHwgdHlwZSA9PT0gJ2VuZCcgJiYgc2VsZWN0ZWRWYWx1ZVswXSB8fCAhIWhvdmVyVmFsdWUubGVuZ3RoID8gdGhpcy5vbkRheUhvdmVyIDogdW5kZWZpbmVkXG4gICAgfTtcblxuICAgIHZhciBwbGFjZWhvbGRlcjEgPSB2b2lkIDA7XG4gICAgdmFyIHBsYWNlaG9sZGVyMiA9IHZvaWQgMDtcblxuICAgIGlmIChkYXRlSW5wdXRQbGFjZWhvbGRlcikge1xuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZGF0ZUlucHV0UGxhY2Vob2xkZXIpKSB7XG4gICAgICAgIHBsYWNlaG9sZGVyMSA9IGRhdGVJbnB1dFBsYWNlaG9sZGVyWzBdO1xuICAgICAgICBwbGFjZWhvbGRlcjIgPSBkYXRlSW5wdXRQbGFjZWhvbGRlclsxXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBsYWNlaG9sZGVyMSA9IHBsYWNlaG9sZGVyMiA9IGRhdGVJbnB1dFBsYWNlaG9sZGVyO1xuICAgICAgfVxuICAgIH1cbiAgICB2YXIgc2hvd09rQnV0dG9uID0gc2hvd09rID09PSB0cnVlIHx8IHNob3dPayAhPT0gZmFsc2UgJiYgISF0aW1lUGlja2VyO1xuICAgIHZhciBjbHMgPSBjbGFzc25hbWVzKChfY2xhc3NuYW1lcyA9IHt9LCBfY2xhc3NuYW1lc1twcmVmaXhDbHMgKyAnLWZvb3RlciddID0gdHJ1ZSwgX2NsYXNzbmFtZXNbcHJlZml4Q2xzICsgJy1yYW5nZS1ib3R0b20nXSA9IHRydWUsIF9jbGFzc25hbWVzW3ByZWZpeENscyArICctZm9vdGVyLXNob3ctb2snXSA9IHNob3dPa0J1dHRvbiwgX2NsYXNzbmFtZXMpKTtcblxuICAgIHZhciBzdGFydFZhbHVlID0gdGhpcy5nZXRTdGFydFZhbHVlKCk7XG4gICAgdmFyIGVuZFZhbHVlID0gdGhpcy5nZXRFbmRWYWx1ZSgpO1xuICAgIHZhciB0b2RheVRpbWUgPSBnZXRUb2RheVRpbWUoc3RhcnRWYWx1ZSk7XG4gICAgdmFyIHRoaXNNb250aCA9IHRvZGF5VGltZS5tb250aCgpO1xuICAgIHZhciB0aGlzWWVhciA9IHRvZGF5VGltZS55ZWFyKCk7XG4gICAgdmFyIGlzVG9kYXlJblZpZXcgPSBzdGFydFZhbHVlLnllYXIoKSA9PT0gdGhpc1llYXIgJiYgc3RhcnRWYWx1ZS5tb250aCgpID09PSB0aGlzTW9udGggfHwgZW5kVmFsdWUueWVhcigpID09PSB0aGlzWWVhciAmJiBlbmRWYWx1ZS5tb250aCgpID09PSB0aGlzTW9udGg7XG4gICAgdmFyIG5leHRNb250aE9mU3RhcnQgPSBzdGFydFZhbHVlLmNsb25lKCkuYWRkKDEsICdtb250aHMnKTtcbiAgICB2YXIgaXNDbG9zZXN0TW9udGhzID0gbmV4dE1vbnRoT2ZTdGFydC55ZWFyKCkgPT09IGVuZFZhbHVlLnllYXIoKSAmJiBuZXh0TW9udGhPZlN0YXJ0Lm1vbnRoKCkgPT09IGVuZFZhbHVlLm1vbnRoKCk7XG5cbiAgICB2YXIgZXh0cmFGb290ZXIgPSBwcm9wcy5yZW5kZXJGb290ZXIoKTtcblxuICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgJ2RpdicsXG4gICAgICB7XG4gICAgICAgIHJlZjogdGhpcy5zYXZlUm9vdCxcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLFxuICAgICAgICBzdHlsZTogcHJvcHMuc3R5bGUsXG4gICAgICAgIHRhYkluZGV4OiAnMCcsXG4gICAgICAgIG9uS2V5RG93bjogdGhpcy5vbktleURvd25cbiAgICAgIH0sXG4gICAgICBwcm9wcy5yZW5kZXJTaWRlYmFyKCksXG4gICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgeyBjbGFzc05hbWU6IHByZWZpeENscyArICctcGFuZWwnIH0sXG4gICAgICAgIHNob3dDbGVhciAmJiBzZWxlY3RlZFZhbHVlWzBdICYmIHNlbGVjdGVkVmFsdWVbMV0gPyBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdhJyxcbiAgICAgICAgICB7XG4gICAgICAgICAgICByb2xlOiAnYnV0dG9uJyxcbiAgICAgICAgICAgIHRpdGxlOiBsb2NhbGUuY2xlYXIsXG4gICAgICAgICAgICBvbkNsaWNrOiB0aGlzLmNsZWFyXG4gICAgICAgICAgfSxcbiAgICAgICAgICBjbGVhckljb24gfHwgUmVhY3QuY3JlYXRlRWxlbWVudCgnc3BhbicsIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWNsZWFyLWJ0bicgfSlcbiAgICAgICAgKSA6IG51bGwsXG4gICAgICAgIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAge1xuICAgICAgICAgICAgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLWRhdGUtcGFuZWwnLFxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlOiB0eXBlICE9PSAnYm90aCcgPyB0aGlzLm9uRGF0ZVBhbmVsTGVhdmUgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI6IHR5cGUgIT09ICdib3RoJyA/IHRoaXMub25EYXRlUGFuZWxFbnRlciA6IHVuZGVmaW5lZFxuICAgICAgICAgIH0sXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChDYWxlbmRhclBhcnQsIF9leHRlbmRzKHt9LCBwcm9wcywgbmV3UHJvcHMsIHtcbiAgICAgICAgICAgIGhvdmVyVmFsdWU6IGhvdmVyVmFsdWUsXG4gICAgICAgICAgICBkaXJlY3Rpb246ICdsZWZ0JyxcbiAgICAgICAgICAgIGRpc2FibGVkVGltZTogdGhpcy5kaXNhYmxlZFN0YXJ0VGltZSxcbiAgICAgICAgICAgIGRpc2FibGVkTW9udGg6IHRoaXMuZGlzYWJsZWRTdGFydE1vbnRoLFxuICAgICAgICAgICAgZm9ybWF0OiB0aGlzLmdldEZvcm1hdCgpLFxuICAgICAgICAgICAgdmFsdWU6IHN0YXJ0VmFsdWUsXG4gICAgICAgICAgICBtb2RlOiBtb2RlWzBdLFxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6IHBsYWNlaG9sZGVyMSxcbiAgICAgICAgICAgIG9uSW5wdXRDaGFuZ2U6IHRoaXMub25TdGFydElucHV0Q2hhbmdlLFxuICAgICAgICAgICAgb25JbnB1dFNlbGVjdDogdGhpcy5vblN0YXJ0SW5wdXRTZWxlY3QsXG4gICAgICAgICAgICBvblZhbHVlQ2hhbmdlOiB0aGlzLm9uU3RhcnRWYWx1ZUNoYW5nZSxcbiAgICAgICAgICAgIG9uUGFuZWxDaGFuZ2U6IHRoaXMub25TdGFydFBhbmVsQ2hhbmdlLFxuICAgICAgICAgICAgc2hvd0RhdGVJbnB1dDogdGhpcy5wcm9wcy5zaG93RGF0ZUlucHV0LFxuICAgICAgICAgICAgdGltZVBpY2tlcjogdGltZVBpY2tlcixcbiAgICAgICAgICAgIHNob3dUaW1lUGlja2VyOiBzaG93VGltZVBpY2tlciB8fCBtb2RlWzBdID09PSAndGltZScsXG4gICAgICAgICAgICBlbmFibGVQcmV2OiB0cnVlLFxuICAgICAgICAgICAgZW5hYmxlTmV4dDogIWlzQ2xvc2VzdE1vbnRocyB8fCB0aGlzLmlzTW9udGhZZWFyUGFuZWxTaG93KG1vZGVbMV0pLFxuICAgICAgICAgICAgY2xlYXJJY29uOiBjbGVhckljb25cbiAgICAgICAgICB9KSksXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBwcmVmaXhDbHMgKyAnLXJhbmdlLW1pZGRsZScgfSxcbiAgICAgICAgICAgIHNlcGVyYXRvclxuICAgICAgICAgICksXG4gICAgICAgICAgUmVhY3QuY3JlYXRlRWxlbWVudChDYWxlbmRhclBhcnQsIF9leHRlbmRzKHt9LCBwcm9wcywgbmV3UHJvcHMsIHtcbiAgICAgICAgICAgIGhvdmVyVmFsdWU6IGhvdmVyVmFsdWUsXG4gICAgICAgICAgICBkaXJlY3Rpb246ICdyaWdodCcsXG4gICAgICAgICAgICBmb3JtYXQ6IHRoaXMuZ2V0Rm9ybWF0KCksXG4gICAgICAgICAgICB0aW1lUGlja2VyRGlzYWJsZWRUaW1lOiB0aGlzLmdldEVuZERpc2FibGVUaW1lKCksXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogcGxhY2Vob2xkZXIyLFxuICAgICAgICAgICAgdmFsdWU6IGVuZFZhbHVlLFxuICAgICAgICAgICAgbW9kZTogbW9kZVsxXSxcbiAgICAgICAgICAgIG9uSW5wdXRDaGFuZ2U6IHRoaXMub25FbmRJbnB1dENoYW5nZSxcbiAgICAgICAgICAgIG9uSW5wdXRTZWxlY3Q6IHRoaXMub25FbmRJbnB1dFNlbGVjdCxcbiAgICAgICAgICAgIG9uVmFsdWVDaGFuZ2U6IHRoaXMub25FbmRWYWx1ZUNoYW5nZSxcbiAgICAgICAgICAgIG9uUGFuZWxDaGFuZ2U6IHRoaXMub25FbmRQYW5lbENoYW5nZSxcbiAgICAgICAgICAgIHNob3dEYXRlSW5wdXQ6IHRoaXMucHJvcHMuc2hvd0RhdGVJbnB1dCxcbiAgICAgICAgICAgIHRpbWVQaWNrZXI6IHRpbWVQaWNrZXIsXG4gICAgICAgICAgICBzaG93VGltZVBpY2tlcjogc2hvd1RpbWVQaWNrZXIgfHwgbW9kZVsxXSA9PT0gJ3RpbWUnLFxuICAgICAgICAgICAgZGlzYWJsZWRUaW1lOiB0aGlzLmRpc2FibGVkRW5kVGltZSxcbiAgICAgICAgICAgIGRpc2FibGVkTW9udGg6IHRoaXMuZGlzYWJsZWRFbmRNb250aCxcbiAgICAgICAgICAgIGVuYWJsZVByZXY6ICFpc0Nsb3Nlc3RNb250aHMgfHwgdGhpcy5pc01vbnRoWWVhclBhbmVsU2hvdyhtb2RlWzBdKSxcbiAgICAgICAgICAgIGVuYWJsZU5leHQ6IHRydWUsXG4gICAgICAgICAgICBjbGVhckljb246IGNsZWFySWNvblxuICAgICAgICAgIH0pKVxuICAgICAgICApLFxuICAgICAgICBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbHMgfSxcbiAgICAgICAgICBzaG93VG9kYXkgfHwgcHJvcHMudGltZVBpY2tlciB8fCBzaG93T2tCdXR0b24gfHwgZXh0cmFGb290ZXIgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgICB7IGNsYXNzTmFtZTogcHJlZml4Q2xzICsgJy1mb290ZXItYnRuJyB9LFxuICAgICAgICAgICAgZXh0cmFGb290ZXIsXG4gICAgICAgICAgICBzaG93VG9kYXkgPyBSZWFjdC5jcmVhdGVFbGVtZW50KFRvZGF5QnV0dG9uLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHtcbiAgICAgICAgICAgICAgZGlzYWJsZWQ6IGlzVG9kYXlJblZpZXcsXG4gICAgICAgICAgICAgIHZhbHVlOiBzdGF0ZS52YWx1ZVswXSxcbiAgICAgICAgICAgICAgb25Ub2RheTogdGhpcy5vblRvZGF5LFxuICAgICAgICAgICAgICB0ZXh0OiBsb2NhbGUuYmFja1RvVG9kYXlcbiAgICAgICAgICAgIH0pKSA6IG51bGwsXG4gICAgICAgICAgICBwcm9wcy50aW1lUGlja2VyID8gUmVhY3QuY3JlYXRlRWxlbWVudChUaW1lUGlja2VyQnV0dG9uLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHtcbiAgICAgICAgICAgICAgc2hvd1RpbWVQaWNrZXI6IHNob3dUaW1lUGlja2VyIHx8IG1vZGVbMF0gPT09ICd0aW1lJyAmJiBtb2RlWzFdID09PSAndGltZScsXG4gICAgICAgICAgICAgIG9uT3BlblRpbWVQaWNrZXI6IHRoaXMub25PcGVuVGltZVBpY2tlcixcbiAgICAgICAgICAgICAgb25DbG9zZVRpbWVQaWNrZXI6IHRoaXMub25DbG9zZVRpbWVQaWNrZXIsXG4gICAgICAgICAgICAgIHRpbWVQaWNrZXJEaXNhYmxlZDogIXRoaXMuaGFzU2VsZWN0ZWRWYWx1ZSgpIHx8IGhvdmVyVmFsdWUubGVuZ3RoXG4gICAgICAgICAgICB9KSkgOiBudWxsLFxuICAgICAgICAgICAgc2hvd09rQnV0dG9uID8gUmVhY3QuY3JlYXRlRWxlbWVudChPa0J1dHRvbiwgX2V4dGVuZHMoe30sIHByb3BzLCB7XG4gICAgICAgICAgICAgIG9uT2s6IHRoaXMub25PayxcbiAgICAgICAgICAgICAgb2tEaXNhYmxlZDogIXRoaXMuaXNBbGxvd2VkRGF0ZUFuZFRpbWUoc2VsZWN0ZWRWYWx1ZSkgfHwgIXRoaXMuaGFzU2VsZWN0ZWRWYWx1ZSgpIHx8IGhvdmVyVmFsdWUubGVuZ3RoXG4gICAgICAgICAgICB9KSkgOiBudWxsXG4gICAgICAgICAgKSA6IG51bGxcbiAgICAgICAgKVxuICAgICAgKVxuICAgICk7XG4gIH07XG5cbiAgcmV0dXJuIFJhbmdlQ2FsZW5kYXI7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cblJhbmdlQ2FsZW5kYXIucHJvcFR5cGVzID0gX2V4dGVuZHMoe30sIHByb3BUeXBlLCB7XG4gIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGF0ZUlucHV0UGxhY2Vob2xkZXI6IFByb3BUeXBlcy5hbnksXG4gIHNlcGVyYXRvcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGVmYXVsdFZhbHVlOiBQcm9wVHlwZXMuYW55LFxuICB2YWx1ZTogUHJvcFR5cGVzLmFueSxcbiAgaG92ZXJWYWx1ZTogUHJvcFR5cGVzLmFueSxcbiAgbW9kZTogUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLm9uZU9mKFsndGltZScsICdkYXRlJywgJ21vbnRoJywgJ3llYXInLCAnZGVjYWRlJ10pKSxcbiAgc2hvd0RhdGVJbnB1dDogUHJvcFR5cGVzLmJvb2wsXG4gIHRpbWVQaWNrZXI6IFByb3BUeXBlcy5hbnksXG4gIHNob3dPazogUHJvcFR5cGVzLmJvb2wsXG4gIHNob3dUb2RheTogUHJvcFR5cGVzLmJvb2wsXG4gIGRlZmF1bHRTZWxlY3RlZFZhbHVlOiBQcm9wVHlwZXMuYXJyYXksXG4gIHNlbGVjdGVkVmFsdWU6IFByb3BUeXBlcy5hcnJheSxcbiAgb25PazogUHJvcFR5cGVzLmZ1bmMsXG4gIHNob3dDbGVhcjogUHJvcFR5cGVzLmJvb2wsXG4gIGxvY2FsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICBvblNlbGVjdDogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICBvbkhvdmVyQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25QYW5lbENoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gIGZvcm1hdDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmFycmF5T2YoUHJvcFR5cGVzLnN0cmluZyldKSxcbiAgb25DbGVhcjogUHJvcFR5cGVzLmZ1bmMsXG4gIHR5cGU6IFByb3BUeXBlcy5hbnksXG4gIGRpc2FibGVkRGF0ZTogUHJvcFR5cGVzLmZ1bmMsXG4gIGRpc2FibGVkVGltZTogUHJvcFR5cGVzLmZ1bmMsXG4gIGNsZWFySWNvbjogUHJvcFR5cGVzLm5vZGUsXG4gIG9uS2V5RG93bjogUHJvcFR5cGVzLmZ1bmNcbn0pO1xuUmFuZ2VDYWxlbmRhci5kZWZhdWx0UHJvcHMgPSBfZXh0ZW5kcyh7fSwgZGVmYXVsdFByb3AsIHtcbiAgdHlwZTogJ2JvdGgnLFxuICBzZXBlcmF0b3I6ICd+JyxcbiAgZGVmYXVsdFNlbGVjdGVkVmFsdWU6IFtdLFxuICBvblZhbHVlQ2hhbmdlOiBub29wLFxuICBvbkhvdmVyQ2hhbmdlOiBub29wLFxuICBvblBhbmVsQ2hhbmdlOiBub29wLFxuICBkaXNhYmxlZFRpbWU6IG5vb3AsXG4gIG9uSW5wdXRTZWxlY3Q6IG5vb3AsXG4gIHNob3dUb2RheTogdHJ1ZSxcbiAgc2hvd0RhdGVJbnB1dDogdHJ1ZVxufSk7XG5cbnZhciBfaW5pdGlhbGlzZVByb3BzID0gZnVuY3Rpb24gX2luaXRpYWxpc2VQcm9wcygpIHtcbiAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgdGhpcy5vbkRhdGVQYW5lbEVudGVyID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChfdGhpczIuaGFzU2VsZWN0ZWRWYWx1ZSgpKSB7XG4gICAgICBfdGhpczIuZmlyZUhvdmVyVmFsdWVDaGFuZ2UoX3RoaXMyLnN0YXRlLnNlbGVjdGVkVmFsdWUuY29uY2F0KCkpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLm9uRGF0ZVBhbmVsTGVhdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKF90aGlzMi5oYXNTZWxlY3RlZFZhbHVlKCkpIHtcbiAgICAgIF90aGlzMi5maXJlSG92ZXJWYWx1ZUNoYW5nZShbXSk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMub25TZWxlY3QgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICB2YXIgdHlwZSA9IF90aGlzMi5wcm9wcy50eXBlO1xuICAgIHZhciBfc3RhdGUgPSBfdGhpczIuc3RhdGUsXG4gICAgICAgIHNlbGVjdGVkVmFsdWUgPSBfc3RhdGUuc2VsZWN0ZWRWYWx1ZSxcbiAgICAgICAgcHJldlNlbGVjdGVkVmFsdWUgPSBfc3RhdGUucHJldlNlbGVjdGVkVmFsdWUsXG4gICAgICAgIGZpcnN0U2VsZWN0ZWRWYWx1ZSA9IF9zdGF0ZS5maXJzdFNlbGVjdGVkVmFsdWU7XG5cbiAgICB2YXIgbmV4dFNlbGVjdGVkVmFsdWUgPSB2b2lkIDA7XG4gICAgaWYgKHR5cGUgPT09ICdib3RoJykge1xuICAgICAgaWYgKCFmaXJzdFNlbGVjdGVkVmFsdWUpIHtcbiAgICAgICAgc3luY1RpbWUocHJldlNlbGVjdGVkVmFsdWVbMF0sIHZhbHVlKTtcbiAgICAgICAgbmV4dFNlbGVjdGVkVmFsdWUgPSBbdmFsdWVdO1xuICAgICAgfSBlbHNlIGlmIChfdGhpczIuY29tcGFyZShmaXJzdFNlbGVjdGVkVmFsdWUsIHZhbHVlKSA8IDApIHtcbiAgICAgICAgc3luY1RpbWUocHJldlNlbGVjdGVkVmFsdWVbMV0sIHZhbHVlKTtcbiAgICAgICAgbmV4dFNlbGVjdGVkVmFsdWUgPSBbZmlyc3RTZWxlY3RlZFZhbHVlLCB2YWx1ZV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzeW5jVGltZShwcmV2U2VsZWN0ZWRWYWx1ZVswXSwgdmFsdWUpO1xuICAgICAgICBzeW5jVGltZShwcmV2U2VsZWN0ZWRWYWx1ZVsxXSwgZmlyc3RTZWxlY3RlZFZhbHVlKTtcbiAgICAgICAgbmV4dFNlbGVjdGVkVmFsdWUgPSBbdmFsdWUsIGZpcnN0U2VsZWN0ZWRWYWx1ZV07XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh0eXBlID09PSAnc3RhcnQnKSB7XG4gICAgICBzeW5jVGltZShwcmV2U2VsZWN0ZWRWYWx1ZVswXSwgdmFsdWUpO1xuICAgICAgdmFyIGVuZFZhbHVlID0gc2VsZWN0ZWRWYWx1ZVsxXTtcbiAgICAgIG5leHRTZWxlY3RlZFZhbHVlID0gZW5kVmFsdWUgJiYgX3RoaXMyLmNvbXBhcmUoZW5kVmFsdWUsIHZhbHVlKSA+IDAgPyBbdmFsdWUsIGVuZFZhbHVlXSA6IFt2YWx1ZV07XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIHR5cGUgPT09ICdlbmQnXG4gICAgICB2YXIgc3RhcnRWYWx1ZSA9IHNlbGVjdGVkVmFsdWVbMF07XG4gICAgICBpZiAoc3RhcnRWYWx1ZSAmJiBfdGhpczIuY29tcGFyZShzdGFydFZhbHVlLCB2YWx1ZSkgPD0gMCkge1xuICAgICAgICBzeW5jVGltZShwcmV2U2VsZWN0ZWRWYWx1ZVsxXSwgdmFsdWUpO1xuICAgICAgICBuZXh0U2VsZWN0ZWRWYWx1ZSA9IFtzdGFydFZhbHVlLCB2YWx1ZV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzeW5jVGltZShwcmV2U2VsZWN0ZWRWYWx1ZVswXSwgdmFsdWUpO1xuICAgICAgICBuZXh0U2VsZWN0ZWRWYWx1ZSA9IFt2YWx1ZV07XG4gICAgICB9XG4gICAgfVxuXG4gICAgX3RoaXMyLmZpcmVTZWxlY3RWYWx1ZUNoYW5nZShuZXh0U2VsZWN0ZWRWYWx1ZSk7XG4gIH07XG5cbiAgdGhpcy5vbktleURvd24gPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBpZiAoZXZlbnQudGFyZ2V0Lm5vZGVOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdpbnB1dCcpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIga2V5Q29kZSA9IGV2ZW50LmtleUNvZGU7XG5cbiAgICB2YXIgY3RybEtleSA9IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQubWV0YUtleTtcblxuICAgIHZhciBfc3RhdGUyID0gX3RoaXMyLnN0YXRlLFxuICAgICAgICBzZWxlY3RlZFZhbHVlID0gX3N0YXRlMi5zZWxlY3RlZFZhbHVlLFxuICAgICAgICBob3ZlclZhbHVlID0gX3N0YXRlMi5ob3ZlclZhbHVlLFxuICAgICAgICBmaXJzdFNlbGVjdGVkVmFsdWUgPSBfc3RhdGUyLmZpcnN0U2VsZWN0ZWRWYWx1ZSxcbiAgICAgICAgdmFsdWUgPSBfc3RhdGUyLnZhbHVlO1xuICAgIHZhciBfcHJvcHMgPSBfdGhpczIucHJvcHMsXG4gICAgICAgIG9uS2V5RG93biA9IF9wcm9wcy5vbktleURvd24sXG4gICAgICAgIGRpc2FibGVkRGF0ZSA9IF9wcm9wcy5kaXNhYmxlZERhdGU7XG5cbiAgICAvLyBVcGRhdGUgbGFzdCB0aW1lIG9mIHRoZSBwaWNrZXJcblxuICAgIHZhciB1cGRhdGVIb3ZlclBvaW50ID0gZnVuY3Rpb24gdXBkYXRlSG92ZXJQb2ludChmdW5jKSB7XG4gICAgICAvLyBDaGFuZ2UgaG92ZXIgdG8gbWFrZSBmb2N1cyBpbiBVSVxuICAgICAgdmFyIGN1cnJlbnRIb3ZlclRpbWUgPSB2b2lkIDA7XG4gICAgICB2YXIgbmV4dEhvdmVyVGltZSA9IHZvaWQgMDtcbiAgICAgIHZhciBuZXh0SG92ZXJWYWx1ZSA9IHZvaWQgMDtcblxuICAgICAgaWYgKCFmaXJzdFNlbGVjdGVkVmFsdWUpIHtcbiAgICAgICAgY3VycmVudEhvdmVyVGltZSA9IGhvdmVyVmFsdWVbMF0gfHwgc2VsZWN0ZWRWYWx1ZVswXSB8fCB2YWx1ZVswXSB8fCBtb21lbnQoKTtcbiAgICAgICAgbmV4dEhvdmVyVGltZSA9IGZ1bmMoY3VycmVudEhvdmVyVGltZSk7XG4gICAgICAgIG5leHRIb3ZlclZhbHVlID0gW25leHRIb3ZlclRpbWVdO1xuICAgICAgICBfdGhpczIuZmlyZUhvdmVyVmFsdWVDaGFuZ2UobmV4dEhvdmVyVmFsdWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGhvdmVyVmFsdWUubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgY3VycmVudEhvdmVyVGltZSA9IGhvdmVyVmFsdWVbMF0uY2xvbmUoKTtcbiAgICAgICAgICBuZXh0SG92ZXJUaW1lID0gZnVuYyhjdXJyZW50SG92ZXJUaW1lKTtcbiAgICAgICAgICBuZXh0SG92ZXJWYWx1ZSA9IF90aGlzMi5vbkRheUhvdmVyKG5leHRIb3ZlclRpbWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGN1cnJlbnRIb3ZlclRpbWUgPSBob3ZlclZhbHVlWzBdLmlzU2FtZShmaXJzdFNlbGVjdGVkVmFsdWUsICdkYXknKSA/IGhvdmVyVmFsdWVbMV0gOiBob3ZlclZhbHVlWzBdO1xuICAgICAgICAgIG5leHRIb3ZlclRpbWUgPSBmdW5jKGN1cnJlbnRIb3ZlclRpbWUpO1xuICAgICAgICAgIG5leHRIb3ZlclZhbHVlID0gX3RoaXMyLm9uRGF5SG92ZXIobmV4dEhvdmVyVGltZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gRmluZCBvcmlnaW4gaG92ZXIgdGltZSBvbiB2YWx1ZSBpbmRleFxuICAgICAgaWYgKG5leHRIb3ZlclZhbHVlLmxlbmd0aCA+PSAyKSB7XG4gICAgICAgIHZhciBtaXNzID0gbmV4dEhvdmVyVmFsdWUuc29tZShmdW5jdGlvbiAoaHQpIHtcbiAgICAgICAgICByZXR1cm4gIWluY2x1ZGVzVGltZSh2YWx1ZSwgaHQsICdtb250aCcpO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKG1pc3MpIHtcbiAgICAgICAgICB2YXIgbmV3VmFsdWUgPSBuZXh0SG92ZXJWYWx1ZS5zbGljZSgpLnNvcnQoZnVuY3Rpb24gKHQxLCB0Mikge1xuICAgICAgICAgICAgcmV0dXJuIHQxLnZhbHVlT2YoKSAtIHQyLnZhbHVlT2YoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBpZiAobmV3VmFsdWVbMF0uaXNTYW1lKG5ld1ZhbHVlWzFdLCAnbW9udGgnKSkge1xuICAgICAgICAgICAgbmV3VmFsdWVbMV0gPSBuZXdWYWx1ZVswXS5jbG9uZSgpLmFkZCgxLCAnbW9udGgnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgX3RoaXMyLmZpcmVWYWx1ZUNoYW5nZShuZXdWYWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAobmV4dEhvdmVyVmFsdWUubGVuZ3RoID09PSAxKSB7XG4gICAgICAgIC8vIElmIG9ubHkgb25lIHZhbHVlLCBsZXQncyBrZWVwIHRoZSBvcmlnaW4gcGFuZWxcbiAgICAgICAgdmFyIG9yaVZhbHVlSW5kZXggPSB2YWx1ZS5maW5kSW5kZXgoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICByZXR1cm4gdGltZS5pc1NhbWUoY3VycmVudEhvdmVyVGltZSwgJ21vbnRoJyk7XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAob3JpVmFsdWVJbmRleCA9PT0gLTEpIG9yaVZhbHVlSW5kZXggPSAwO1xuXG4gICAgICAgIGlmICh2YWx1ZS5ldmVyeShmdW5jdGlvbiAodGltZSkge1xuICAgICAgICAgIHJldHVybiAhdGltZS5pc1NhbWUobmV4dEhvdmVyVGltZSwgJ21vbnRoJyk7XG4gICAgICAgIH0pKSB7XG4gICAgICAgICAgdmFyIF9uZXdWYWx1ZSA9IHZhbHVlLnNsaWNlKCk7XG4gICAgICAgICAgX25ld1ZhbHVlW29yaVZhbHVlSW5kZXhdID0gbmV4dEhvdmVyVGltZS5jbG9uZSgpO1xuICAgICAgICAgIF90aGlzMi5maXJlVmFsdWVDaGFuZ2UoX25ld1ZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICByZXR1cm4gbmV4dEhvdmVyVGltZTtcbiAgICB9O1xuXG4gICAgc3dpdGNoIChrZXlDb2RlKSB7XG4gICAgICBjYXNlIEtleUNvZGUuRE9XTjpcbiAgICAgICAgdXBkYXRlSG92ZXJQb2ludChmdW5jdGlvbiAodGltZSkge1xuICAgICAgICAgIHJldHVybiBnb1RpbWUodGltZSwgMSwgJ3dlZWtzJyk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICBjYXNlIEtleUNvZGUuVVA6XG4gICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICByZXR1cm4gZ29UaW1lKHRpbWUsIC0xLCAnd2Vla3MnKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIGNhc2UgS2V5Q29kZS5MRUZUOlxuICAgICAgICBpZiAoY3RybEtleSkge1xuICAgICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICAgIHJldHVybiBnb1RpbWUodGltZSwgLTEsICd5ZWFycycpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICAgIHJldHVybiBnb1RpbWUodGltZSwgLTEsICdkYXlzJyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuO1xuICAgICAgY2FzZSBLZXlDb2RlLlJJR0hUOlxuICAgICAgICBpZiAoY3RybEtleSkge1xuICAgICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICAgIHJldHVybiBnb1RpbWUodGltZSwgMSwgJ3llYXJzJyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdXBkYXRlSG92ZXJQb2ludChmdW5jdGlvbiAodGltZSkge1xuICAgICAgICAgICAgcmV0dXJuIGdvVGltZSh0aW1lLCAxLCAnZGF5cycpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybjtcbiAgICAgIGNhc2UgS2V5Q29kZS5IT01FOlxuICAgICAgICB1cGRhdGVIb3ZlclBvaW50KGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgICAgICAgcmV0dXJuIGdvU3RhcnRNb250aCh0aW1lKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIGNhc2UgS2V5Q29kZS5FTkQ6XG4gICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICByZXR1cm4gZ29FbmRNb250aCh0aW1lKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIGNhc2UgS2V5Q29kZS5QQUdFX0RPV046XG4gICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICByZXR1cm4gZ29UaW1lKHRpbWUsIDEsICdtb250aCcpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgY2FzZSBLZXlDb2RlLlBBR0VfVVA6XG4gICAgICAgIHVwZGF0ZUhvdmVyUG9pbnQoZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgICAgICByZXR1cm4gZ29UaW1lKHRpbWUsIC0xLCAnbW9udGgnKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIGNhc2UgS2V5Q29kZS5FTlRFUjpcbiAgICAgICAge1xuICAgICAgICAgIHZhciBsYXN0VmFsdWUgPSB2b2lkIDA7XG4gICAgICAgICAgaWYgKGhvdmVyVmFsdWUubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBsYXN0VmFsdWUgPSB1cGRhdGVIb3ZlclBvaW50KGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgICAgICAgICAgIHJldHVybiB0aW1lO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSBlbHNlIGlmIChob3ZlclZhbHVlLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgICAgbGFzdFZhbHVlID0gaG92ZXJWYWx1ZVswXTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGFzdFZhbHVlID0gaG92ZXJWYWx1ZVswXS5pc1NhbWUoZmlyc3RTZWxlY3RlZFZhbHVlLCAnZGF5JykgPyBob3ZlclZhbHVlWzFdIDogaG92ZXJWYWx1ZVswXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGxhc3RWYWx1ZSAmJiAoIWRpc2FibGVkRGF0ZSB8fCAhZGlzYWJsZWREYXRlKGxhc3RWYWx1ZSkpKSB7XG4gICAgICAgICAgICBfdGhpczIub25TZWxlY3QobGFzdFZhbHVlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlmIChvbktleURvd24pIHtcbiAgICAgICAgICBvbktleURvd24oZXZlbnQpO1xuICAgICAgICB9XG4gICAgfVxuICB9O1xuXG4gIHRoaXMub25EYXlIb3ZlciA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIHZhciBob3ZlclZhbHVlID0gW107XG4gICAgdmFyIF9zdGF0ZTMgPSBfdGhpczIuc3RhdGUsXG4gICAgICAgIHNlbGVjdGVkVmFsdWUgPSBfc3RhdGUzLnNlbGVjdGVkVmFsdWUsXG4gICAgICAgIGZpcnN0U2VsZWN0ZWRWYWx1ZSA9IF9zdGF0ZTMuZmlyc3RTZWxlY3RlZFZhbHVlO1xuICAgIHZhciB0eXBlID0gX3RoaXMyLnByb3BzLnR5cGU7XG5cbiAgICBpZiAodHlwZSA9PT0gJ3N0YXJ0JyAmJiBzZWxlY3RlZFZhbHVlWzFdKSB7XG4gICAgICBob3ZlclZhbHVlID0gX3RoaXMyLmNvbXBhcmUodmFsdWUsIHNlbGVjdGVkVmFsdWVbMV0pIDwgMCA/IFt2YWx1ZSwgc2VsZWN0ZWRWYWx1ZVsxXV0gOiBbdmFsdWVdO1xuICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ2VuZCcgJiYgc2VsZWN0ZWRWYWx1ZVswXSkge1xuICAgICAgaG92ZXJWYWx1ZSA9IF90aGlzMi5jb21wYXJlKHZhbHVlLCBzZWxlY3RlZFZhbHVlWzBdKSA+IDAgPyBbc2VsZWN0ZWRWYWx1ZVswXSwgdmFsdWVdIDogW107XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICghZmlyc3RTZWxlY3RlZFZhbHVlKSB7XG4gICAgICAgIGlmIChfdGhpczIuc3RhdGUuaG92ZXJWYWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgICBfdGhpczIuc2V0U3RhdGUoeyBob3ZlclZhbHVlOiBbXSB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaG92ZXJWYWx1ZTtcbiAgICAgIH1cbiAgICAgIGhvdmVyVmFsdWUgPSBfdGhpczIuY29tcGFyZSh2YWx1ZSwgZmlyc3RTZWxlY3RlZFZhbHVlKSA8IDAgPyBbdmFsdWUsIGZpcnN0U2VsZWN0ZWRWYWx1ZV0gOiBbZmlyc3RTZWxlY3RlZFZhbHVlLCB2YWx1ZV07XG4gICAgfVxuICAgIF90aGlzMi5maXJlSG92ZXJWYWx1ZUNoYW5nZShob3ZlclZhbHVlKTtcblxuICAgIHJldHVybiBob3ZlclZhbHVlO1xuICB9O1xuXG4gIHRoaXMub25Ub2RheSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgc3RhcnRWYWx1ZSA9IGdldFRvZGF5VGltZShfdGhpczIuc3RhdGUudmFsdWVbMF0pO1xuICAgIHZhciBlbmRWYWx1ZSA9IHN0YXJ0VmFsdWUuY2xvbmUoKS5hZGQoMSwgJ21vbnRocycpO1xuICAgIF90aGlzMi5zZXRTdGF0ZSh7IHZhbHVlOiBbc3RhcnRWYWx1ZSwgZW5kVmFsdWVdIH0pO1xuICB9O1xuXG4gIHRoaXMub25PcGVuVGltZVBpY2tlciA9IGZ1bmN0aW9uICgpIHtcbiAgICBfdGhpczIuc2V0U3RhdGUoe1xuICAgICAgc2hvd1RpbWVQaWNrZXI6IHRydWVcbiAgICB9KTtcbiAgfTtcblxuICB0aGlzLm9uQ2xvc2VUaW1lUGlja2VyID0gZnVuY3Rpb24gKCkge1xuICAgIF90aGlzMi5zZXRTdGF0ZSh7XG4gICAgICBzaG93VGltZVBpY2tlcjogZmFsc2VcbiAgICB9KTtcbiAgfTtcblxuICB0aGlzLm9uT2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNlbGVjdGVkVmFsdWUgPSBfdGhpczIuc3RhdGUuc2VsZWN0ZWRWYWx1ZTtcblxuICAgIGlmIChfdGhpczIuaXNBbGxvd2VkRGF0ZUFuZFRpbWUoc2VsZWN0ZWRWYWx1ZSkpIHtcbiAgICAgIF90aGlzMi5wcm9wcy5vbk9rKF90aGlzMi5zdGF0ZS5zZWxlY3RlZFZhbHVlKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5vblN0YXJ0SW5wdXRDaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIG9hcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBvYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICB2YXIgYXJncyA9IFsnbGVmdCddLmNvbmNhdChvYXJncyk7XG4gICAgcmV0dXJuIG9uSW5wdXRTZWxlY3QuYXBwbHkoX3RoaXMyLCBhcmdzKTtcbiAgfTtcblxuICB0aGlzLm9uRW5kSW5wdXRDaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgZm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBvYXJncyA9IEFycmF5KF9sZW4yKSwgX2tleTIgPSAwOyBfa2V5MiA8IF9sZW4yOyBfa2V5MisrKSB7XG4gICAgICBvYXJnc1tfa2V5Ml0gPSBhcmd1bWVudHNbX2tleTJdO1xuICAgIH1cblxuICAgIHZhciBhcmdzID0gWydyaWdodCddLmNvbmNhdChvYXJncyk7XG4gICAgcmV0dXJuIG9uSW5wdXRTZWxlY3QuYXBwbHkoX3RoaXMyLCBhcmdzKTtcbiAgfTtcblxuICB0aGlzLm9uU3RhcnRJbnB1dFNlbGVjdCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIHZhciBhcmdzID0gWydsZWZ0JywgdmFsdWUsIHsgc291cmNlOiAnZGF0ZUlucHV0U2VsZWN0JyB9XTtcbiAgICByZXR1cm4gb25JbnB1dFNlbGVjdC5hcHBseShfdGhpczIsIGFyZ3MpO1xuICB9O1xuXG4gIHRoaXMub25FbmRJbnB1dFNlbGVjdCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIHZhciBhcmdzID0gWydyaWdodCcsIHZhbHVlLCB7IHNvdXJjZTogJ2RhdGVJbnB1dFNlbGVjdCcgfV07XG4gICAgcmV0dXJuIG9uSW5wdXRTZWxlY3QuYXBwbHkoX3RoaXMyLCBhcmdzKTtcbiAgfTtcblxuICB0aGlzLm9uU3RhcnRWYWx1ZUNoYW5nZSA9IGZ1bmN0aW9uIChsZWZ0VmFsdWUpIHtcbiAgICB2YXIgdmFsdWUgPSBbXS5jb25jYXQoX3RoaXMyLnN0YXRlLnZhbHVlKTtcbiAgICB2YWx1ZVswXSA9IGxlZnRWYWx1ZTtcbiAgICByZXR1cm4gX3RoaXMyLmZpcmVWYWx1ZUNoYW5nZSh2YWx1ZSk7XG4gIH07XG5cbiAgdGhpcy5vbkVuZFZhbHVlQ2hhbmdlID0gZnVuY3Rpb24gKHJpZ2h0VmFsdWUpIHtcbiAgICB2YXIgdmFsdWUgPSBbXS5jb25jYXQoX3RoaXMyLnN0YXRlLnZhbHVlKTtcbiAgICB2YWx1ZVsxXSA9IHJpZ2h0VmFsdWU7XG4gICAgcmV0dXJuIF90aGlzMi5maXJlVmFsdWVDaGFuZ2UodmFsdWUpO1xuICB9O1xuXG4gIHRoaXMub25TdGFydFBhbmVsQ2hhbmdlID0gZnVuY3Rpb24gKHZhbHVlLCBtb2RlKSB7XG4gICAgdmFyIHByb3BzID0gX3RoaXMyLnByb3BzLFxuICAgICAgICBzdGF0ZSA9IF90aGlzMi5zdGF0ZTtcblxuICAgIHZhciBuZXdNb2RlID0gW21vZGUsIHN0YXRlLm1vZGVbMV1dO1xuICAgIHZhciBuZXdTdGF0ZSA9IHtcbiAgICAgIHBhbmVsVHJpZ2dlclNvdXJjZTogJ3N0YXJ0J1xuICAgIH07XG4gICAgaWYgKCEoJ21vZGUnIGluIHByb3BzKSkge1xuICAgICAgbmV3U3RhdGUubW9kZSA9IG5ld01vZGU7XG4gICAgfVxuICAgIF90aGlzMi5zZXRTdGF0ZShuZXdTdGF0ZSk7XG4gICAgdmFyIG5ld1ZhbHVlID0gW3ZhbHVlIHx8IHN0YXRlLnZhbHVlWzBdLCBzdGF0ZS52YWx1ZVsxXV07XG4gICAgcHJvcHMub25QYW5lbENoYW5nZShuZXdWYWx1ZSwgbmV3TW9kZSk7XG4gIH07XG5cbiAgdGhpcy5vbkVuZFBhbmVsQ2hhbmdlID0gZnVuY3Rpb24gKHZhbHVlLCBtb2RlKSB7XG4gICAgdmFyIHByb3BzID0gX3RoaXMyLnByb3BzLFxuICAgICAgICBzdGF0ZSA9IF90aGlzMi5zdGF0ZTtcblxuICAgIHZhciBuZXdNb2RlID0gW3N0YXRlLm1vZGVbMF0sIG1vZGVdO1xuICAgIHZhciBuZXdTdGF0ZSA9IHtcbiAgICAgIHBhbmVsVHJpZ2dlclNvdXJjZTogJ2VuZCdcbiAgICB9O1xuICAgIGlmICghKCdtb2RlJyBpbiBwcm9wcykpIHtcbiAgICAgIG5ld1N0YXRlLm1vZGUgPSBuZXdNb2RlO1xuICAgIH1cbiAgICBfdGhpczIuc2V0U3RhdGUobmV3U3RhdGUpO1xuICAgIHZhciBuZXdWYWx1ZSA9IFtzdGF0ZS52YWx1ZVswXSwgdmFsdWUgfHwgc3RhdGUudmFsdWVbMV1dO1xuICAgIHByb3BzLm9uUGFuZWxDaGFuZ2UobmV3VmFsdWUsIG5ld01vZGUpO1xuICB9O1xuXG4gIHRoaXMuZ2V0U3RhcnRWYWx1ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgX3N0YXRlNCA9IF90aGlzMi5zdGF0ZSxcbiAgICAgICAgc2VsZWN0ZWRWYWx1ZSA9IF9zdGF0ZTQuc2VsZWN0ZWRWYWx1ZSxcbiAgICAgICAgc2hvd1RpbWVQaWNrZXIgPSBfc3RhdGU0LnNob3dUaW1lUGlja2VyLFxuICAgICAgICB2YWx1ZSA9IF9zdGF0ZTQudmFsdWUsXG4gICAgICAgIG1vZGUgPSBfc3RhdGU0Lm1vZGUsXG4gICAgICAgIHBhbmVsVHJpZ2dlclNvdXJjZSA9IF9zdGF0ZTQucGFuZWxUcmlnZ2VyU291cmNlO1xuXG4gICAgdmFyIHN0YXJ0VmFsdWUgPSB2YWx1ZVswXTtcbiAgICAvLyBrZWVwIHNlbGVjdGVkVGltZSB3aGVuIHNlbGVjdCBkYXRlXG4gICAgaWYgKHNlbGVjdGVkVmFsdWVbMF0gJiYgX3RoaXMyLnByb3BzLnRpbWVQaWNrZXIpIHtcbiAgICAgIHN0YXJ0VmFsdWUgPSBzdGFydFZhbHVlLmNsb25lKCk7XG4gICAgICBzeW5jVGltZShzZWxlY3RlZFZhbHVlWzBdLCBzdGFydFZhbHVlKTtcbiAgICB9XG4gICAgaWYgKHNob3dUaW1lUGlja2VyICYmIHNlbGVjdGVkVmFsdWVbMF0pIHtcbiAgICAgIHN0YXJ0VmFsdWUgPSBzZWxlY3RlZFZhbHVlWzBdO1xuICAgIH1cblxuICAgIC8vIEFkanVzdCBtb250aCBpZiBkYXRlIG5vdCBhbGlnblxuICAgIGlmIChwYW5lbFRyaWdnZXJTb3VyY2UgPT09ICdlbmQnICYmIG1vZGVbMF0gPT09ICdkYXRlJyAmJiBtb2RlWzFdID09PSAnZGF0ZScgJiYgc3RhcnRWYWx1ZS5pc1NhbWUodmFsdWVbMV0sICdtb250aCcpKSB7XG4gICAgICBzdGFydFZhbHVlID0gc3RhcnRWYWx1ZS5jbG9uZSgpLnN1YnRyYWN0KDEsICdtb250aCcpO1xuICAgIH1cblxuICAgIHJldHVybiBzdGFydFZhbHVlO1xuICB9O1xuXG4gIHRoaXMuZ2V0RW5kVmFsdWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF9zdGF0ZTUgPSBfdGhpczIuc3RhdGUsXG4gICAgICAgIHZhbHVlID0gX3N0YXRlNS52YWx1ZSxcbiAgICAgICAgc2VsZWN0ZWRWYWx1ZSA9IF9zdGF0ZTUuc2VsZWN0ZWRWYWx1ZSxcbiAgICAgICAgc2hvd1RpbWVQaWNrZXIgPSBfc3RhdGU1LnNob3dUaW1lUGlja2VyLFxuICAgICAgICBtb2RlID0gX3N0YXRlNS5tb2RlLFxuICAgICAgICBwYW5lbFRyaWdnZXJTb3VyY2UgPSBfc3RhdGU1LnBhbmVsVHJpZ2dlclNvdXJjZTtcblxuICAgIHZhciBlbmRWYWx1ZSA9IHZhbHVlWzFdID8gdmFsdWVbMV0uY2xvbmUoKSA6IHZhbHVlWzBdLmNsb25lKCkuYWRkKDEsICdtb250aCcpO1xuICAgIC8vIGtlZXAgc2VsZWN0ZWRUaW1lIHdoZW4gc2VsZWN0IGRhdGVcbiAgICBpZiAoc2VsZWN0ZWRWYWx1ZVsxXSAmJiBfdGhpczIucHJvcHMudGltZVBpY2tlcikge1xuICAgICAgc3luY1RpbWUoc2VsZWN0ZWRWYWx1ZVsxXSwgZW5kVmFsdWUpO1xuICAgIH1cbiAgICBpZiAoc2hvd1RpbWVQaWNrZXIpIHtcbiAgICAgIGVuZFZhbHVlID0gc2VsZWN0ZWRWYWx1ZVsxXSA/IHNlbGVjdGVkVmFsdWVbMV0gOiBfdGhpczIuZ2V0U3RhcnRWYWx1ZSgpO1xuICAgIH1cblxuICAgIC8vIEFkanVzdCBtb250aCBpZiBkYXRlIG5vdCBhbGlnblxuICAgIGlmICghc2hvd1RpbWVQaWNrZXIgJiYgcGFuZWxUcmlnZ2VyU291cmNlICE9PSAnZW5kJyAmJiBtb2RlWzBdID09PSAnZGF0ZScgJiYgbW9kZVsxXSA9PT0gJ2RhdGUnICYmIGVuZFZhbHVlLmlzU2FtZSh2YWx1ZVswXSwgJ21vbnRoJykpIHtcbiAgICAgIGVuZFZhbHVlID0gZW5kVmFsdWUuY2xvbmUoKS5hZGQoMSwgJ21vbnRoJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGVuZFZhbHVlO1xuICB9O1xuXG4gIHRoaXMuZ2V0RW5kRGlzYWJsZVRpbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIF9zdGF0ZTYgPSBfdGhpczIuc3RhdGUsXG4gICAgICAgIHNlbGVjdGVkVmFsdWUgPSBfc3RhdGU2LnNlbGVjdGVkVmFsdWUsXG4gICAgICAgIHZhbHVlID0gX3N0YXRlNi52YWx1ZTtcbiAgICB2YXIgZGlzYWJsZWRUaW1lID0gX3RoaXMyLnByb3BzLmRpc2FibGVkVGltZTtcblxuICAgIHZhciB1c2VyU2V0dGluZ0Rpc2FibGVkVGltZSA9IGRpc2FibGVkVGltZShzZWxlY3RlZFZhbHVlLCAnZW5kJykgfHwge307XG4gICAgdmFyIHN0YXJ0VmFsdWUgPSBzZWxlY3RlZFZhbHVlICYmIHNlbGVjdGVkVmFsdWVbMF0gfHwgdmFsdWVbMF0uY2xvbmUoKTtcbiAgICAvLyBpZiBzdGFydFRpbWUgYW5kIGVuZFRpbWUgaXMgc2FtZSBkYXkuLlxuICAgIC8vIHRoZSBzZWNvbmQgdGltZSBwaWNrZXIgd2lsbCBub3QgYWJsZSB0byBwaWNrIHRpbWUgYmVmb3JlIGZpcnN0IHRpbWUgcGlja2VyXG4gICAgaWYgKCFzZWxlY3RlZFZhbHVlWzFdIHx8IHN0YXJ0VmFsdWUuaXNTYW1lKHNlbGVjdGVkVmFsdWVbMV0sICdkYXknKSkge1xuICAgICAgdmFyIGhvdXJzID0gc3RhcnRWYWx1ZS5ob3VyKCk7XG4gICAgICB2YXIgbWludXRlcyA9IHN0YXJ0VmFsdWUubWludXRlKCk7XG4gICAgICB2YXIgc2Vjb25kID0gc3RhcnRWYWx1ZS5zZWNvbmQoKTtcbiAgICAgIHZhciBfZGlzYWJsZWRIb3VycyA9IHVzZXJTZXR0aW5nRGlzYWJsZWRUaW1lLmRpc2FibGVkSG91cnMsXG4gICAgICAgICAgX2Rpc2FibGVkTWludXRlcyA9IHVzZXJTZXR0aW5nRGlzYWJsZWRUaW1lLmRpc2FibGVkTWludXRlcyxcbiAgICAgICAgICBfZGlzYWJsZWRTZWNvbmRzID0gdXNlclNldHRpbmdEaXNhYmxlZFRpbWUuZGlzYWJsZWRTZWNvbmRzO1xuXG4gICAgICB2YXIgb2xkRGlzYWJsZWRNaW51dGVzID0gX2Rpc2FibGVkTWludXRlcyA/IF9kaXNhYmxlZE1pbnV0ZXMoKSA6IFtdO1xuICAgICAgdmFyIG9sZGRpc2FibGVkU2Vjb25kcyA9IF9kaXNhYmxlZFNlY29uZHMgPyBfZGlzYWJsZWRTZWNvbmRzKCkgOiBbXTtcbiAgICAgIF9kaXNhYmxlZEhvdXJzID0gZ2VuZXJhdGVPcHRpb25zKGhvdXJzLCBfZGlzYWJsZWRIb3Vycyk7XG4gICAgICBfZGlzYWJsZWRNaW51dGVzID0gZ2VuZXJhdGVPcHRpb25zKG1pbnV0ZXMsIF9kaXNhYmxlZE1pbnV0ZXMpO1xuICAgICAgX2Rpc2FibGVkU2Vjb25kcyA9IGdlbmVyYXRlT3B0aW9ucyhzZWNvbmQsIF9kaXNhYmxlZFNlY29uZHMpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZGlzYWJsZWRIb3VyczogZnVuY3Rpb24gZGlzYWJsZWRIb3VycygpIHtcbiAgICAgICAgICByZXR1cm4gX2Rpc2FibGVkSG91cnM7XG4gICAgICAgIH0sXG4gICAgICAgIGRpc2FibGVkTWludXRlczogZnVuY3Rpb24gZGlzYWJsZWRNaW51dGVzKGhvdXIpIHtcbiAgICAgICAgICBpZiAoaG91ciA9PT0gaG91cnMpIHtcbiAgICAgICAgICAgIHJldHVybiBfZGlzYWJsZWRNaW51dGVzO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gb2xkRGlzYWJsZWRNaW51dGVzO1xuICAgICAgICB9LFxuICAgICAgICBkaXNhYmxlZFNlY29uZHM6IGZ1bmN0aW9uIGRpc2FibGVkU2Vjb25kcyhob3VyLCBtaW51dGUpIHtcbiAgICAgICAgICBpZiAoaG91ciA9PT0gaG91cnMgJiYgbWludXRlID09PSBtaW51dGVzKSB7XG4gICAgICAgICAgICByZXR1cm4gX2Rpc2FibGVkU2Vjb25kcztcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIG9sZGRpc2FibGVkU2Vjb25kcztcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9XG4gICAgcmV0dXJuIHVzZXJTZXR0aW5nRGlzYWJsZWRUaW1lO1xuICB9O1xuXG4gIHRoaXMuaXNBbGxvd2VkRGF0ZUFuZFRpbWUgPSBmdW5jdGlvbiAoc2VsZWN0ZWRWYWx1ZSkge1xuICAgIHJldHVybiBpc0FsbG93ZWREYXRlKHNlbGVjdGVkVmFsdWVbMF0sIF90aGlzMi5wcm9wcy5kaXNhYmxlZERhdGUsIF90aGlzMi5kaXNhYmxlZFN0YXJ0VGltZSkgJiYgaXNBbGxvd2VkRGF0ZShzZWxlY3RlZFZhbHVlWzFdLCBfdGhpczIucHJvcHMuZGlzYWJsZWREYXRlLCBfdGhpczIuZGlzYWJsZWRFbmRUaW1lKTtcbiAgfTtcblxuICB0aGlzLmlzTW9udGhZZWFyUGFuZWxTaG93ID0gZnVuY3Rpb24gKG1vZGUpIHtcbiAgICByZXR1cm4gWydtb250aCcsICd5ZWFyJywgJ2RlY2FkZSddLmluZGV4T2YobW9kZSkgPiAtMTtcbiAgfTtcblxuICB0aGlzLmhhc1NlbGVjdGVkVmFsdWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHNlbGVjdGVkVmFsdWUgPSBfdGhpczIuc3RhdGUuc2VsZWN0ZWRWYWx1ZTtcblxuICAgIHJldHVybiAhIXNlbGVjdGVkVmFsdWVbMV0gJiYgISFzZWxlY3RlZFZhbHVlWzBdO1xuICB9O1xuXG4gIHRoaXMuY29tcGFyZSA9IGZ1bmN0aW9uICh2MSwgdjIpIHtcbiAgICBpZiAoX3RoaXMyLnByb3BzLnRpbWVQaWNrZXIpIHtcbiAgICAgIHJldHVybiB2MS5kaWZmKHYyKTtcbiAgICB9XG4gICAgcmV0dXJuIHYxLmRpZmYodjIsICdkYXlzJyk7XG4gIH07XG5cbiAgdGhpcy5maXJlU2VsZWN0VmFsdWVDaGFuZ2UgPSBmdW5jdGlvbiAoc2VsZWN0ZWRWYWx1ZSwgZGlyZWN0LCBjYXVzZSkge1xuICAgIHZhciB0aW1lUGlja2VyID0gX3RoaXMyLnByb3BzLnRpbWVQaWNrZXI7XG4gICAgdmFyIHByZXZTZWxlY3RlZFZhbHVlID0gX3RoaXMyLnN0YXRlLnByZXZTZWxlY3RlZFZhbHVlO1xuXG4gICAgaWYgKHRpbWVQaWNrZXIgJiYgdGltZVBpY2tlci5wcm9wcy5kZWZhdWx0VmFsdWUpIHtcbiAgICAgIHZhciB0aW1lUGlja2VyRGVmYXVsdFZhbHVlID0gdGltZVBpY2tlci5wcm9wcy5kZWZhdWx0VmFsdWU7XG4gICAgICBpZiAoIXByZXZTZWxlY3RlZFZhbHVlWzBdICYmIHNlbGVjdGVkVmFsdWVbMF0pIHtcbiAgICAgICAgc3luY1RpbWUodGltZVBpY2tlckRlZmF1bHRWYWx1ZVswXSwgc2VsZWN0ZWRWYWx1ZVswXSk7XG4gICAgICB9XG4gICAgICBpZiAoIXByZXZTZWxlY3RlZFZhbHVlWzFdICYmIHNlbGVjdGVkVmFsdWVbMV0pIHtcbiAgICAgICAgc3luY1RpbWUodGltZVBpY2tlckRlZmF1bHRWYWx1ZVsxXSwgc2VsZWN0ZWRWYWx1ZVsxXSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCEoJ3NlbGVjdGVkVmFsdWUnIGluIF90aGlzMi5wcm9wcykpIHtcbiAgICAgIF90aGlzMi5zZXRTdGF0ZSh7XG4gICAgICAgIHNlbGVjdGVkVmFsdWU6IHNlbGVjdGVkVmFsdWVcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8vIOWwmuacqumAieaLqei/h+aXtumXtO+8jOebtOaOpei+k+WFpeeahOivnVxuICAgIGlmICghX3RoaXMyLnN0YXRlLnNlbGVjdGVkVmFsdWVbMF0gfHwgIV90aGlzMi5zdGF0ZS5zZWxlY3RlZFZhbHVlWzFdKSB7XG4gICAgICB2YXIgc3RhcnRWYWx1ZSA9IHNlbGVjdGVkVmFsdWVbMF0gfHwgbW9tZW50KCk7XG4gICAgICB2YXIgZW5kVmFsdWUgPSBzZWxlY3RlZFZhbHVlWzFdIHx8IHN0YXJ0VmFsdWUuY2xvbmUoKS5hZGQoMSwgJ21vbnRocycpO1xuICAgICAgX3RoaXMyLnNldFN0YXRlKHtcbiAgICAgICAgc2VsZWN0ZWRWYWx1ZTogc2VsZWN0ZWRWYWx1ZSxcbiAgICAgICAgdmFsdWU6IGdldFZhbHVlRnJvbVNlbGVjdGVkVmFsdWUoW3N0YXJ0VmFsdWUsIGVuZFZhbHVlXSlcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChzZWxlY3RlZFZhbHVlWzBdICYmICFzZWxlY3RlZFZhbHVlWzFdKSB7XG4gICAgICBfdGhpczIuc2V0U3RhdGUoeyBmaXJzdFNlbGVjdGVkVmFsdWU6IHNlbGVjdGVkVmFsdWVbMF0gfSk7XG4gICAgICBfdGhpczIuZmlyZUhvdmVyVmFsdWVDaGFuZ2Uoc2VsZWN0ZWRWYWx1ZS5jb25jYXQoKSk7XG4gICAgfVxuICAgIF90aGlzMi5wcm9wcy5vbkNoYW5nZShzZWxlY3RlZFZhbHVlKTtcbiAgICBpZiAoZGlyZWN0IHx8IHNlbGVjdGVkVmFsdWVbMF0gJiYgc2VsZWN0ZWRWYWx1ZVsxXSkge1xuICAgICAgX3RoaXMyLnNldFN0YXRlKHtcbiAgICAgICAgcHJldlNlbGVjdGVkVmFsdWU6IHNlbGVjdGVkVmFsdWUsXG4gICAgICAgIGZpcnN0U2VsZWN0ZWRWYWx1ZTogbnVsbFxuICAgICAgfSk7XG4gICAgICBfdGhpczIuZmlyZUhvdmVyVmFsdWVDaGFuZ2UoW10pO1xuICAgICAgX3RoaXMyLnByb3BzLm9uU2VsZWN0KHNlbGVjdGVkVmFsdWUsIGNhdXNlKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5maXJlVmFsdWVDaGFuZ2UgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICB2YXIgcHJvcHMgPSBfdGhpczIucHJvcHM7XG4gICAgaWYgKCEoJ3ZhbHVlJyBpbiBwcm9wcykpIHtcbiAgICAgIF90aGlzMi5zZXRTdGF0ZSh7XG4gICAgICAgIHZhbHVlOiB2YWx1ZVxuICAgICAgfSk7XG4gICAgfVxuICAgIHByb3BzLm9uVmFsdWVDaGFuZ2UodmFsdWUpO1xuICB9O1xuXG4gIHRoaXMuZmlyZUhvdmVyVmFsdWVDaGFuZ2UgPSBmdW5jdGlvbiAoaG92ZXJWYWx1ZSkge1xuICAgIHZhciBwcm9wcyA9IF90aGlzMi5wcm9wcztcbiAgICBpZiAoISgnaG92ZXJWYWx1ZScgaW4gcHJvcHMpKSB7XG4gICAgICBfdGhpczIuc2V0U3RhdGUoeyBob3ZlclZhbHVlOiBob3ZlclZhbHVlIH0pO1xuICAgIH1cbiAgICBwcm9wcy5vbkhvdmVyQ2hhbmdlKGhvdmVyVmFsdWUpO1xuICB9O1xuXG4gIHRoaXMuY2xlYXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgX3RoaXMyLmZpcmVTZWxlY3RWYWx1ZUNoYW5nZShbXSwgdHJ1ZSk7XG4gICAgX3RoaXMyLnByb3BzLm9uQ2xlYXIoKTtcbiAgfTtcblxuICB0aGlzLmRpc2FibGVkU3RhcnRUaW1lID0gZnVuY3Rpb24gKHRpbWUpIHtcbiAgICByZXR1cm4gX3RoaXMyLnByb3BzLmRpc2FibGVkVGltZSh0aW1lLCAnc3RhcnQnKTtcbiAgfTtcblxuICB0aGlzLmRpc2FibGVkRW5kVGltZSA9IGZ1bmN0aW9uICh0aW1lKSB7XG4gICAgcmV0dXJuIF90aGlzMi5wcm9wcy5kaXNhYmxlZFRpbWUodGltZSwgJ2VuZCcpO1xuICB9O1xuXG4gIHRoaXMuZGlzYWJsZWRTdGFydE1vbnRoID0gZnVuY3Rpb24gKG1vbnRoKSB7XG4gICAgdmFyIHZhbHVlID0gX3RoaXMyLnN0YXRlLnZhbHVlO1xuXG4gICAgcmV0dXJuIG1vbnRoLmlzQWZ0ZXIodmFsdWVbMV0sICdtb250aCcpO1xuICB9O1xuXG4gIHRoaXMuZGlzYWJsZWRFbmRNb250aCA9IGZ1bmN0aW9uIChtb250aCkge1xuICAgIHZhciB2YWx1ZSA9IF90aGlzMi5zdGF0ZS52YWx1ZTtcblxuICAgIHJldHVybiBtb250aC5pc0JlZm9yZSh2YWx1ZVswXSwgJ21vbnRoJyk7XG4gIH07XG59O1xuXG5wb2x5ZmlsbChSYW5nZUNhbGVuZGFyKTtcblxuZXhwb3J0IGRlZmF1bHQgY29tbW9uTWl4aW5XcmFwcGVyKFJhbmdlQ2FsZW5kYXIpOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBVUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbEJBO0FBc0JBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5CQTtBQXdCQTtBQUFBO0FBR0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUZBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1QkE7QUE4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVZBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBM0VBO0FBNEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/RangeCalendar.js
