__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _desk_page_body__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./desk-page-body */ "./src/app/desk/component/page/desk-page-body.tsx");
/* harmony import */ var _desk_page_header__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./desk-page-header */ "./src/app/desk/component/page/desk-page-header.tsx");
/* harmony import */ var _desk_page_footer__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./desk-page-footer */ "./src/app/desk/component/page/desk-page-footer.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/page/desk-page.tsx";






var DeskPage =
/*#__PURE__*/
function (_Page) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(DeskPage, _Page);

  function DeskPage() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, DeskPage);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(DeskPage).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(DeskPage, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(DeskPage.prototype), "initComponents", this).call(this), {});
    }
  }, {
    key: "renderPageHeader",
    value: function renderPageHeader() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_page_header__WEBPACK_IMPORTED_MODULE_9__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 14
        },
        __self: this
      });
    }
  }, {
    key: "renderPageBody",
    value: function renderPageBody() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_page_body__WEBPACK_IMPORTED_MODULE_8__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        },
        __self: this
      });
    }
  }, {
    key: "renderPageFooter",
    value: function renderPageFooter() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_page_footer__WEBPACK_IMPORTED_MODULE_10__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      });
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.C;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        },
        __self: this
      }, this.renderPageHeader(), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "page-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31
        },
        __self: this
      }, this.renderPageBody()), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        },
        __self: this
      }, this.renderPageFooter()));
    }
  }]);

  return DeskPage;
}(_component__WEBPACK_IMPORTED_MODULE_7__["Page"]);

/* harmony default export */ __webpack_exports__["default"] = (DeskPage);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvZGVzay1wYWdlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9wYWdlL2Rlc2stcGFnZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGFnZUNvbXBvbmVudHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCIuLi8uLi8uLi8uLi9jb21tb24vM3JkXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCBEZXNrUGFnZUJvZHkgZnJvbSBcIi4vZGVzay1wYWdlLWJvZHlcIjtcbmltcG9ydCBEZXNrUGFnZUhlYWRlciBmcm9tIFwiLi9kZXNrLXBhZ2UtaGVhZGVyXCI7XG5pbXBvcnQgRGVza1BhZ2VGb290ZXIgZnJvbSBcIi4vZGVzay1wYWdlLWZvb3RlclwiO1xuXG5jbGFzcyBEZXNrUGFnZTxQLCBTLCBDIGV4dGVuZHMgUGFnZUNvbXBvbmVudHM+IGV4dGVuZHMgUGFnZTxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihzdXBlci5pbml0Q29tcG9uZW50cygpLCB7fSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyUGFnZUhlYWRlcigpIHtcbiAgICByZXR1cm4gPERlc2tQYWdlSGVhZGVyLz47XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyUGFnZUJvZHkoKTogYW55IHtcbiAgICByZXR1cm4gPERlc2tQYWdlQm9keS8+O1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlclBhZ2VGb290ZXIoKSB7XG4gICAgcmV0dXJuIDxEZXNrUGFnZUZvb3Rlci8+O1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLkM7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEMuQm94PlxuICAgICAgICB7dGhpcy5yZW5kZXJQYWdlSGVhZGVyKCl9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGFnZS1ib2R5XCI+e3RoaXMucmVuZGVyUGFnZUJvZHkoKX08L2Rpdj5cbiAgICAgICAgPGRpdj57dGhpcy5yZW5kZXJQYWdlRm9vdGVyKCl9PC9kaXY+XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgRGVza1BhZ2U7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTs7OztBQTNCQTtBQUNBO0FBNkJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/page/desk-page.tsx
