/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */


var UserAgentData = __webpack_require__(/*! ./UserAgentData */ "./node_modules/fbjs/lib/UserAgentData.js");

var VersionRange = __webpack_require__(/*! ./VersionRange */ "./node_modules/fbjs/lib/VersionRange.js");

var mapObject = __webpack_require__(/*! ./mapObject */ "./node_modules/fbjs/lib/mapObject.js");

var memoizeStringOnly = __webpack_require__(/*! ./memoizeStringOnly */ "./node_modules/fbjs/lib/memoizeStringOnly.js");
/**
 * Checks to see whether `name` and `version` satisfy `query`.
 *
 * @param {string} name Name of the browser, device, engine or platform
 * @param {?string} version Version of the browser, engine or platform
 * @param {string} query Query of form "Name [range expression]"
 * @param {?function} normalizer Optional pre-processor for range expression
 * @return {boolean}
 */


function compare(name, version, query, normalizer) {
  // check for exact match with no version
  if (name === query) {
    return true;
  } // check for non-matching names


  if (!query.startsWith(name)) {
    return false;
  } // full comparison with version


  var range = query.slice(name.length);

  if (version) {
    range = normalizer ? normalizer(range) : range;
    return VersionRange.contains(range, version);
  }

  return false;
}
/**
 * Normalizes `version` by stripping any "NT" prefix, but only on the Windows
 * platform.
 *
 * Mimics the stripping performed by the `UserAgentWindowsPlatform` PHP class.
 *
 * @param {string} version
 * @return {string}
 */


function normalizePlatformVersion(version) {
  if (UserAgentData.platformName === 'Windows') {
    return version.replace(/^\s*NT/, '');
  }

  return version;
}
/**
 * Provides client-side access to the authoritative PHP-generated User Agent
 * information supplied by the server.
 */


var UserAgent = {
  /**
   * Check if the User Agent browser matches `query`.
   *
   * `query` should be a string like "Chrome" or "Chrome > 33".
   *
   * Valid browser names include:
   *
   * - ACCESS NetFront
   * - AOL
   * - Amazon Silk
   * - Android
   * - BlackBerry
   * - BlackBerry PlayBook
   * - Chrome
   * - Chrome for iOS
   * - Chrome frame
   * - Facebook PHP SDK
   * - Facebook for iOS
   * - Firefox
   * - IE
   * - IE Mobile
   * - Mobile Safari
   * - Motorola Internet Browser
   * - Nokia
   * - Openwave Mobile Browser
   * - Opera
   * - Opera Mini
   * - Opera Mobile
   * - Safari
   * - UIWebView
   * - Unknown
   * - webOS
   * - etc...
   *
   * An authoritative list can be found in the PHP `BrowserDetector` class and
   * related classes in the same file (see calls to `new UserAgentBrowser` here:
   * https://fburl.com/50728104).
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "Name [range expression]"
   * @return {boolean}
   */
  isBrowser: function isBrowser(query) {
    return compare(UserAgentData.browserName, UserAgentData.browserFullVersion, query);
  },

  /**
   * Check if the User Agent browser uses a 32 or 64 bit architecture.
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "32" or "64".
   * @return {boolean}
   */
  isBrowserArchitecture: function isBrowserArchitecture(query) {
    return compare(UserAgentData.browserArchitecture, null, query);
  },

  /**
   * Check if the User Agent device matches `query`.
   *
   * `query` should be a string like "iPhone" or "iPad".
   *
   * Valid device names include:
   *
   * - Kindle
   * - Kindle Fire
   * - Unknown
   * - iPad
   * - iPhone
   * - iPod
   * - etc...
   *
   * An authoritative list can be found in the PHP `DeviceDetector` class and
   * related classes in the same file (see calls to `new UserAgentDevice` here:
   * https://fburl.com/50728332).
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "Name"
   * @return {boolean}
   */
  isDevice: function isDevice(query) {
    return compare(UserAgentData.deviceName, null, query);
  },

  /**
   * Check if the User Agent rendering engine matches `query`.
   *
   * `query` should be a string like "WebKit" or "WebKit >= 537".
   *
   * Valid engine names include:
   *
   * - Gecko
   * - Presto
   * - Trident
   * - WebKit
   * - etc...
   *
   * An authoritative list can be found in the PHP `RenderingEngineDetector`
   * class related classes in the same file (see calls to `new
   * UserAgentRenderingEngine` here: https://fburl.com/50728617).
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "Name [range expression]"
   * @return {boolean}
   */
  isEngine: function isEngine(query) {
    return compare(UserAgentData.engineName, UserAgentData.engineVersion, query);
  },

  /**
   * Check if the User Agent platform matches `query`.
   *
   * `query` should be a string like "Windows" or "iOS 5 - 6".
   *
   * Valid platform names include:
   *
   * - Android
   * - BlackBerry OS
   * - Java ME
   * - Linux
   * - Mac OS X
   * - Mac OS X Calendar
   * - Mac OS X Internet Account
   * - Symbian
   * - SymbianOS
   * - Windows
   * - Windows Mobile
   * - Windows Phone
   * - iOS
   * - iOS Facebook Integration Account
   * - iOS Facebook Social Sharing UI
   * - webOS
   * - Chrome OS
   * - etc...
   *
   * An authoritative list can be found in the PHP `PlatformDetector` class and
   * related classes in the same file (see calls to `new UserAgentPlatform`
   * here: https://fburl.com/50729226).
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "Name [range expression]"
   * @return {boolean}
   */
  isPlatform: function isPlatform(query) {
    return compare(UserAgentData.platformName, UserAgentData.platformFullVersion, query, normalizePlatformVersion);
  },

  /**
   * Check if the User Agent platform is a 32 or 64 bit architecture.
   *
   * @note Function results are memoized
   *
   * @param {string} query Query of the form "32" or "64".
   * @return {boolean}
   */
  isPlatformArchitecture: function isPlatformArchitecture(query) {
    return compare(UserAgentData.platformArchitecture, null, query);
  }
};
module.exports = mapObject(UserAgent, memoizeStringOnly);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVXNlckFnZW50LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZmJqcy9saWIvVXNlckFnZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFVzZXJBZ2VudERhdGEgPSByZXF1aXJlKCcuL1VzZXJBZ2VudERhdGEnKTtcbnZhciBWZXJzaW9uUmFuZ2UgPSByZXF1aXJlKCcuL1ZlcnNpb25SYW5nZScpO1xuXG52YXIgbWFwT2JqZWN0ID0gcmVxdWlyZSgnLi9tYXBPYmplY3QnKTtcbnZhciBtZW1vaXplU3RyaW5nT25seSA9IHJlcXVpcmUoJy4vbWVtb2l6ZVN0cmluZ09ubHknKTtcblxuLyoqXG4gKiBDaGVja3MgdG8gc2VlIHdoZXRoZXIgYG5hbWVgIGFuZCBgdmVyc2lvbmAgc2F0aXNmeSBgcXVlcnlgLlxuICpcbiAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIE5hbWUgb2YgdGhlIGJyb3dzZXIsIGRldmljZSwgZW5naW5lIG9yIHBsYXRmb3JtXG4gKiBAcGFyYW0gez9zdHJpbmd9IHZlcnNpb24gVmVyc2lvbiBvZiB0aGUgYnJvd3NlciwgZW5naW5lIG9yIHBsYXRmb3JtXG4gKiBAcGFyYW0ge3N0cmluZ30gcXVlcnkgUXVlcnkgb2YgZm9ybSBcIk5hbWUgW3JhbmdlIGV4cHJlc3Npb25dXCJcbiAqIEBwYXJhbSB7P2Z1bmN0aW9ufSBub3JtYWxpemVyIE9wdGlvbmFsIHByZS1wcm9jZXNzb3IgZm9yIHJhbmdlIGV4cHJlc3Npb25cbiAqIEByZXR1cm4ge2Jvb2xlYW59XG4gKi9cbmZ1bmN0aW9uIGNvbXBhcmUobmFtZSwgdmVyc2lvbiwgcXVlcnksIG5vcm1hbGl6ZXIpIHtcbiAgLy8gY2hlY2sgZm9yIGV4YWN0IG1hdGNoIHdpdGggbm8gdmVyc2lvblxuICBpZiAobmFtZSA9PT0gcXVlcnkpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIC8vIGNoZWNrIGZvciBub24tbWF0Y2hpbmcgbmFtZXNcbiAgaWYgKCFxdWVyeS5zdGFydHNXaXRoKG5hbWUpKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLy8gZnVsbCBjb21wYXJpc29uIHdpdGggdmVyc2lvblxuICB2YXIgcmFuZ2UgPSBxdWVyeS5zbGljZShuYW1lLmxlbmd0aCk7XG4gIGlmICh2ZXJzaW9uKSB7XG4gICAgcmFuZ2UgPSBub3JtYWxpemVyID8gbm9ybWFsaXplcihyYW5nZSkgOiByYW5nZTtcbiAgICByZXR1cm4gVmVyc2lvblJhbmdlLmNvbnRhaW5zKHJhbmdlLCB2ZXJzaW9uKTtcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxuLyoqXG4gKiBOb3JtYWxpemVzIGB2ZXJzaW9uYCBieSBzdHJpcHBpbmcgYW55IFwiTlRcIiBwcmVmaXgsIGJ1dCBvbmx5IG9uIHRoZSBXaW5kb3dzXG4gKiBwbGF0Zm9ybS5cbiAqXG4gKiBNaW1pY3MgdGhlIHN0cmlwcGluZyBwZXJmb3JtZWQgYnkgdGhlIGBVc2VyQWdlbnRXaW5kb3dzUGxhdGZvcm1gIFBIUCBjbGFzcy5cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ30gdmVyc2lvblxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBub3JtYWxpemVQbGF0Zm9ybVZlcnNpb24odmVyc2lvbikge1xuICBpZiAoVXNlckFnZW50RGF0YS5wbGF0Zm9ybU5hbWUgPT09ICdXaW5kb3dzJykge1xuICAgIHJldHVybiB2ZXJzaW9uLnJlcGxhY2UoL15cXHMqTlQvLCAnJyk7XG4gIH1cblxuICByZXR1cm4gdmVyc2lvbjtcbn1cblxuLyoqXG4gKiBQcm92aWRlcyBjbGllbnQtc2lkZSBhY2Nlc3MgdG8gdGhlIGF1dGhvcml0YXRpdmUgUEhQLWdlbmVyYXRlZCBVc2VyIEFnZW50XG4gKiBpbmZvcm1hdGlvbiBzdXBwbGllZCBieSB0aGUgc2VydmVyLlxuICovXG52YXIgVXNlckFnZW50ID0ge1xuICAvKipcbiAgICogQ2hlY2sgaWYgdGhlIFVzZXIgQWdlbnQgYnJvd3NlciBtYXRjaGVzIGBxdWVyeWAuXG4gICAqXG4gICAqIGBxdWVyeWAgc2hvdWxkIGJlIGEgc3RyaW5nIGxpa2UgXCJDaHJvbWVcIiBvciBcIkNocm9tZSA+IDMzXCIuXG4gICAqXG4gICAqIFZhbGlkIGJyb3dzZXIgbmFtZXMgaW5jbHVkZTpcbiAgICpcbiAgICogLSBBQ0NFU1MgTmV0RnJvbnRcbiAgICogLSBBT0xcbiAgICogLSBBbWF6b24gU2lsa1xuICAgKiAtIEFuZHJvaWRcbiAgICogLSBCbGFja0JlcnJ5XG4gICAqIC0gQmxhY2tCZXJyeSBQbGF5Qm9va1xuICAgKiAtIENocm9tZVxuICAgKiAtIENocm9tZSBmb3IgaU9TXG4gICAqIC0gQ2hyb21lIGZyYW1lXG4gICAqIC0gRmFjZWJvb2sgUEhQIFNES1xuICAgKiAtIEZhY2Vib29rIGZvciBpT1NcbiAgICogLSBGaXJlZm94XG4gICAqIC0gSUVcbiAgICogLSBJRSBNb2JpbGVcbiAgICogLSBNb2JpbGUgU2FmYXJpXG4gICAqIC0gTW90b3JvbGEgSW50ZXJuZXQgQnJvd3NlclxuICAgKiAtIE5va2lhXG4gICAqIC0gT3BlbndhdmUgTW9iaWxlIEJyb3dzZXJcbiAgICogLSBPcGVyYVxuICAgKiAtIE9wZXJhIE1pbmlcbiAgICogLSBPcGVyYSBNb2JpbGVcbiAgICogLSBTYWZhcmlcbiAgICogLSBVSVdlYlZpZXdcbiAgICogLSBVbmtub3duXG4gICAqIC0gd2ViT1NcbiAgICogLSBldGMuLi5cbiAgICpcbiAgICogQW4gYXV0aG9yaXRhdGl2ZSBsaXN0IGNhbiBiZSBmb3VuZCBpbiB0aGUgUEhQIGBCcm93c2VyRGV0ZWN0b3JgIGNsYXNzIGFuZFxuICAgKiByZWxhdGVkIGNsYXNzZXMgaW4gdGhlIHNhbWUgZmlsZSAoc2VlIGNhbGxzIHRvIGBuZXcgVXNlckFnZW50QnJvd3NlcmAgaGVyZTpcbiAgICogaHR0cHM6Ly9mYnVybC5jb20vNTA3MjgxMDQpLlxuICAgKlxuICAgKiBAbm90ZSBGdW5jdGlvbiByZXN1bHRzIGFyZSBtZW1vaXplZFxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcXVlcnkgUXVlcnkgb2YgdGhlIGZvcm0gXCJOYW1lIFtyYW5nZSBleHByZXNzaW9uXVwiXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpc0Jyb3dzZXI6IGZ1bmN0aW9uIGlzQnJvd3NlcihxdWVyeSkge1xuICAgIHJldHVybiBjb21wYXJlKFVzZXJBZ2VudERhdGEuYnJvd3Nlck5hbWUsIFVzZXJBZ2VudERhdGEuYnJvd3NlckZ1bGxWZXJzaW9uLCBxdWVyeSk7XG4gIH0sXG5cblxuICAvKipcbiAgICogQ2hlY2sgaWYgdGhlIFVzZXIgQWdlbnQgYnJvd3NlciB1c2VzIGEgMzIgb3IgNjQgYml0IGFyY2hpdGVjdHVyZS5cbiAgICpcbiAgICogQG5vdGUgRnVuY3Rpb24gcmVzdWx0cyBhcmUgbWVtb2l6ZWRcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHF1ZXJ5IFF1ZXJ5IG9mIHRoZSBmb3JtIFwiMzJcIiBvciBcIjY0XCIuXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpc0Jyb3dzZXJBcmNoaXRlY3R1cmU6IGZ1bmN0aW9uIGlzQnJvd3NlckFyY2hpdGVjdHVyZShxdWVyeSkge1xuICAgIHJldHVybiBjb21wYXJlKFVzZXJBZ2VudERhdGEuYnJvd3NlckFyY2hpdGVjdHVyZSwgbnVsbCwgcXVlcnkpO1xuICB9LFxuXG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHRoZSBVc2VyIEFnZW50IGRldmljZSBtYXRjaGVzIGBxdWVyeWAuXG4gICAqXG4gICAqIGBxdWVyeWAgc2hvdWxkIGJlIGEgc3RyaW5nIGxpa2UgXCJpUGhvbmVcIiBvciBcImlQYWRcIi5cbiAgICpcbiAgICogVmFsaWQgZGV2aWNlIG5hbWVzIGluY2x1ZGU6XG4gICAqXG4gICAqIC0gS2luZGxlXG4gICAqIC0gS2luZGxlIEZpcmVcbiAgICogLSBVbmtub3duXG4gICAqIC0gaVBhZFxuICAgKiAtIGlQaG9uZVxuICAgKiAtIGlQb2RcbiAgICogLSBldGMuLi5cbiAgICpcbiAgICogQW4gYXV0aG9yaXRhdGl2ZSBsaXN0IGNhbiBiZSBmb3VuZCBpbiB0aGUgUEhQIGBEZXZpY2VEZXRlY3RvcmAgY2xhc3MgYW5kXG4gICAqIHJlbGF0ZWQgY2xhc3NlcyBpbiB0aGUgc2FtZSBmaWxlIChzZWUgY2FsbHMgdG8gYG5ldyBVc2VyQWdlbnREZXZpY2VgIGhlcmU6XG4gICAqIGh0dHBzOi8vZmJ1cmwuY29tLzUwNzI4MzMyKS5cbiAgICpcbiAgICogQG5vdGUgRnVuY3Rpb24gcmVzdWx0cyBhcmUgbWVtb2l6ZWRcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHF1ZXJ5IFF1ZXJ5IG9mIHRoZSBmb3JtIFwiTmFtZVwiXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpc0RldmljZTogZnVuY3Rpb24gaXNEZXZpY2UocXVlcnkpIHtcbiAgICByZXR1cm4gY29tcGFyZShVc2VyQWdlbnREYXRhLmRldmljZU5hbWUsIG51bGwsIHF1ZXJ5KTtcbiAgfSxcblxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiB0aGUgVXNlciBBZ2VudCByZW5kZXJpbmcgZW5naW5lIG1hdGNoZXMgYHF1ZXJ5YC5cbiAgICpcbiAgICogYHF1ZXJ5YCBzaG91bGQgYmUgYSBzdHJpbmcgbGlrZSBcIldlYktpdFwiIG9yIFwiV2ViS2l0ID49IDUzN1wiLlxuICAgKlxuICAgKiBWYWxpZCBlbmdpbmUgbmFtZXMgaW5jbHVkZTpcbiAgICpcbiAgICogLSBHZWNrb1xuICAgKiAtIFByZXN0b1xuICAgKiAtIFRyaWRlbnRcbiAgICogLSBXZWJLaXRcbiAgICogLSBldGMuLi5cbiAgICpcbiAgICogQW4gYXV0aG9yaXRhdGl2ZSBsaXN0IGNhbiBiZSBmb3VuZCBpbiB0aGUgUEhQIGBSZW5kZXJpbmdFbmdpbmVEZXRlY3RvcmBcbiAgICogY2xhc3MgcmVsYXRlZCBjbGFzc2VzIGluIHRoZSBzYW1lIGZpbGUgKHNlZSBjYWxscyB0byBgbmV3XG4gICAqIFVzZXJBZ2VudFJlbmRlcmluZ0VuZ2luZWAgaGVyZTogaHR0cHM6Ly9mYnVybC5jb20vNTA3Mjg2MTcpLlxuICAgKlxuICAgKiBAbm90ZSBGdW5jdGlvbiByZXN1bHRzIGFyZSBtZW1vaXplZFxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcXVlcnkgUXVlcnkgb2YgdGhlIGZvcm0gXCJOYW1lIFtyYW5nZSBleHByZXNzaW9uXVwiXG4gICAqIEByZXR1cm4ge2Jvb2xlYW59XG4gICAqL1xuICBpc0VuZ2luZTogZnVuY3Rpb24gaXNFbmdpbmUocXVlcnkpIHtcbiAgICByZXR1cm4gY29tcGFyZShVc2VyQWdlbnREYXRhLmVuZ2luZU5hbWUsIFVzZXJBZ2VudERhdGEuZW5naW5lVmVyc2lvbiwgcXVlcnkpO1xuICB9LFxuXG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHRoZSBVc2VyIEFnZW50IHBsYXRmb3JtIG1hdGNoZXMgYHF1ZXJ5YC5cbiAgICpcbiAgICogYHF1ZXJ5YCBzaG91bGQgYmUgYSBzdHJpbmcgbGlrZSBcIldpbmRvd3NcIiBvciBcImlPUyA1IC0gNlwiLlxuICAgKlxuICAgKiBWYWxpZCBwbGF0Zm9ybSBuYW1lcyBpbmNsdWRlOlxuICAgKlxuICAgKiAtIEFuZHJvaWRcbiAgICogLSBCbGFja0JlcnJ5IE9TXG4gICAqIC0gSmF2YSBNRVxuICAgKiAtIExpbnV4XG4gICAqIC0gTWFjIE9TIFhcbiAgICogLSBNYWMgT1MgWCBDYWxlbmRhclxuICAgKiAtIE1hYyBPUyBYIEludGVybmV0IEFjY291bnRcbiAgICogLSBTeW1iaWFuXG4gICAqIC0gU3ltYmlhbk9TXG4gICAqIC0gV2luZG93c1xuICAgKiAtIFdpbmRvd3MgTW9iaWxlXG4gICAqIC0gV2luZG93cyBQaG9uZVxuICAgKiAtIGlPU1xuICAgKiAtIGlPUyBGYWNlYm9vayBJbnRlZ3JhdGlvbiBBY2NvdW50XG4gICAqIC0gaU9TIEZhY2Vib29rIFNvY2lhbCBTaGFyaW5nIFVJXG4gICAqIC0gd2ViT1NcbiAgICogLSBDaHJvbWUgT1NcbiAgICogLSBldGMuLi5cbiAgICpcbiAgICogQW4gYXV0aG9yaXRhdGl2ZSBsaXN0IGNhbiBiZSBmb3VuZCBpbiB0aGUgUEhQIGBQbGF0Zm9ybURldGVjdG9yYCBjbGFzcyBhbmRcbiAgICogcmVsYXRlZCBjbGFzc2VzIGluIHRoZSBzYW1lIGZpbGUgKHNlZSBjYWxscyB0byBgbmV3IFVzZXJBZ2VudFBsYXRmb3JtYFxuICAgKiBoZXJlOiBodHRwczovL2ZidXJsLmNvbS81MDcyOTIyNikuXG4gICAqXG4gICAqIEBub3RlIEZ1bmN0aW9uIHJlc3VsdHMgYXJlIG1lbW9pemVkXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBxdWVyeSBRdWVyeSBvZiB0aGUgZm9ybSBcIk5hbWUgW3JhbmdlIGV4cHJlc3Npb25dXCJcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIGlzUGxhdGZvcm06IGZ1bmN0aW9uIGlzUGxhdGZvcm0ocXVlcnkpIHtcbiAgICByZXR1cm4gY29tcGFyZShVc2VyQWdlbnREYXRhLnBsYXRmb3JtTmFtZSwgVXNlckFnZW50RGF0YS5wbGF0Zm9ybUZ1bGxWZXJzaW9uLCBxdWVyeSwgbm9ybWFsaXplUGxhdGZvcm1WZXJzaW9uKTtcbiAgfSxcblxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiB0aGUgVXNlciBBZ2VudCBwbGF0Zm9ybSBpcyBhIDMyIG9yIDY0IGJpdCBhcmNoaXRlY3R1cmUuXG4gICAqXG4gICAqIEBub3RlIEZ1bmN0aW9uIHJlc3VsdHMgYXJlIG1lbW9pemVkXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBxdWVyeSBRdWVyeSBvZiB0aGUgZm9ybSBcIjMyXCIgb3IgXCI2NFwiLlxuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgaXNQbGF0Zm9ybUFyY2hpdGVjdHVyZTogZnVuY3Rpb24gaXNQbGF0Zm9ybUFyY2hpdGVjdHVyZShxdWVyeSkge1xuICAgIHJldHVybiBjb21wYXJlKFVzZXJBZ2VudERhdGEucGxhdGZvcm1BcmNoaXRlY3R1cmUsIG51bGwsIHF1ZXJ5KTtcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBtYXBPYmplY3QoVXNlckFnZW50LCBtZW1vaXplU3RyaW5nT25seSk7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEyQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQXhLQTtBQTJLQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/UserAgent.js
