__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortGradient", function() { return sortGradient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleGradient", function() { return handleGradient; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils */ "./node_modules/antd/es/progress/utils.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

function _iterableToArrayLimit(arr, i) {
  if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) {
    return;
  }

  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};



/**
 * {
 *   '0%': '#afc163',
 *   '75%': '#009900',
 *   '50%': 'green',     ====>     '#afc163 0%, #66FF00 25%, #00CC00 50%, #009900 75%, #ffffff 100%'
 *   '25%': '#66FF00',
 *   '100%': '#ffffff'
 * }
 */

var sortGradient = function sortGradient(gradients) {
  var tempArr = []; // eslint-disable-next-line no-restricted-syntax

  for (var _i = 0, _Object$entries = Object.entries(gradients); _i < _Object$entries.length; _i++) {
    var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        key = _Object$entries$_i[0],
        value = _Object$entries$_i[1];

    var formatKey = parseFloat(key.replace(/%/g, ''));

    if (isNaN(formatKey)) {
      return {};
    }

    tempArr.push({
      key: formatKey,
      value: value
    });
  }

  tempArr = tempArr.sort(function (a, b) {
    return a.key - b.key;
  });
  return tempArr.map(function (_ref) {
    var key = _ref.key,
        value = _ref.value;
    return "".concat(value, " ").concat(key, "%");
  }).join(', ');
};
/**
 * {
 *   '0%': '#afc163',
 *   '25%': '#66FF00',
 *   '50%': '#00CC00',     ====>  linear-gradient(to right, #afc163 0%, #66FF00 25%,
 *   '75%': '#009900',              #00CC00 50%, #009900 75%, #ffffff 100%)
 *   '100%': '#ffffff'
 * }
 *
 * Then this man came to realize the truth:
 * Besides six pence, there is the moon.
 * Besides bread and butter, there is the bug.
 * And...
 * Besides women, there is the code.
 */

var handleGradient = function handleGradient(strokeColor) {
  var _strokeColor$from = strokeColor.from,
      from = _strokeColor$from === void 0 ? '#1890ff' : _strokeColor$from,
      _strokeColor$to = strokeColor.to,
      to = _strokeColor$to === void 0 ? '#1890ff' : _strokeColor$to,
      _strokeColor$directio = strokeColor.direction,
      direction = _strokeColor$directio === void 0 ? 'to right' : _strokeColor$directio,
      rest = __rest(strokeColor, ["from", "to", "direction"]);

  if (Object.keys(rest).length !== 0) {
    var sortedGradients = sortGradient(rest);
    return {
      backgroundImage: "linear-gradient(".concat(direction, ", ").concat(sortedGradients, ")")
    };
  }

  return {
    backgroundImage: "linear-gradient(".concat(direction, ", ").concat(from, ", ").concat(to, ")")
  };
};

var Line = function Line(props) {
  var prefixCls = props.prefixCls,
      percent = props.percent,
      successPercent = props.successPercent,
      strokeWidth = props.strokeWidth,
      size = props.size,
      strokeColor = props.strokeColor,
      strokeLinecap = props.strokeLinecap,
      children = props.children;
  var backgroundProps;

  if (strokeColor && typeof strokeColor !== 'string') {
    backgroundProps = handleGradient(strokeColor);
  } else {
    backgroundProps = {
      background: strokeColor
    };
  }

  var percentStyle = _extends({
    width: "".concat(Object(_utils__WEBPACK_IMPORTED_MODULE_1__["validProgress"])(percent), "%"),
    height: strokeWidth || (size === 'small' ? 6 : 8),
    borderRadius: strokeLinecap === 'square' ? 0 : ''
  }, backgroundProps);

  var successPercentStyle = {
    width: "".concat(Object(_utils__WEBPACK_IMPORTED_MODULE_1__["validProgress"])(successPercent), "%"),
    height: strokeWidth || (size === 'small' ? 6 : 8),
    borderRadius: strokeLinecap === 'square' ? 0 : ''
  };
  var successSegment = successPercent !== undefined ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-success-bg"),
    style: successPercentStyle
  }) : null;
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-outer")
  }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-inner")
  }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-bg"),
    style: percentStyle
  }), successSegment)), children);
};

/* harmony default export */ __webpack_exports__["default"] = (Line);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9wcm9ncmVzcy9MaW5lLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9wcm9ncmVzcy9MaW5lLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyB2YWxpZFByb2dyZXNzIH0gZnJvbSAnLi91dGlscyc7XG4vKipcbiAqIHtcbiAqICAgJzAlJzogJyNhZmMxNjMnLFxuICogICAnNzUlJzogJyMwMDk5MDAnLFxuICogICAnNTAlJzogJ2dyZWVuJywgICAgID09PT0+ICAgICAnI2FmYzE2MyAwJSwgIzY2RkYwMCAyNSUsICMwMENDMDAgNTAlLCAjMDA5OTAwIDc1JSwgI2ZmZmZmZiAxMDAlJ1xuICogICAnMjUlJzogJyM2NkZGMDAnLFxuICogICAnMTAwJSc6ICcjZmZmZmZmJ1xuICogfVxuICovXG5leHBvcnQgY29uc3Qgc29ydEdyYWRpZW50ID0gKGdyYWRpZW50cykgPT4ge1xuICAgIGxldCB0ZW1wQXJyID0gW107XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXJlc3RyaWN0ZWQtc3ludGF4XG4gICAgZm9yIChjb25zdCBba2V5LCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMoZ3JhZGllbnRzKSkge1xuICAgICAgICBjb25zdCBmb3JtYXRLZXkgPSBwYXJzZUZsb2F0KGtleS5yZXBsYWNlKC8lL2csICcnKSk7XG4gICAgICAgIGlmIChpc05hTihmb3JtYXRLZXkpKSB7XG4gICAgICAgICAgICByZXR1cm4ge307XG4gICAgICAgIH1cbiAgICAgICAgdGVtcEFyci5wdXNoKHtcbiAgICAgICAgICAgIGtleTogZm9ybWF0S2V5LFxuICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICB0ZW1wQXJyID0gdGVtcEFyci5zb3J0KChhLCBiKSA9PiBhLmtleSAtIGIua2V5KTtcbiAgICByZXR1cm4gdGVtcEFyci5tYXAoKHsga2V5LCB2YWx1ZSB9KSA9PiBgJHt2YWx1ZX0gJHtrZXl9JWApLmpvaW4oJywgJyk7XG59O1xuLyoqXG4gKiB7XG4gKiAgICcwJSc6ICcjYWZjMTYzJyxcbiAqICAgJzI1JSc6ICcjNjZGRjAwJyxcbiAqICAgJzUwJSc6ICcjMDBDQzAwJywgICAgID09PT0+ICBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICNhZmMxNjMgMCUsICM2NkZGMDAgMjUlLFxuICogICAnNzUlJzogJyMwMDk5MDAnLCAgICAgICAgICAgICAgIzAwQ0MwMCA1MCUsICMwMDk5MDAgNzUlLCAjZmZmZmZmIDEwMCUpXG4gKiAgICcxMDAlJzogJyNmZmZmZmYnXG4gKiB9XG4gKlxuICogVGhlbiB0aGlzIG1hbiBjYW1lIHRvIHJlYWxpemUgdGhlIHRydXRoOlxuICogQmVzaWRlcyBzaXggcGVuY2UsIHRoZXJlIGlzIHRoZSBtb29uLlxuICogQmVzaWRlcyBicmVhZCBhbmQgYnV0dGVyLCB0aGVyZSBpcyB0aGUgYnVnLlxuICogQW5kLi4uXG4gKiBCZXNpZGVzIHdvbWVuLCB0aGVyZSBpcyB0aGUgY29kZS5cbiAqL1xuZXhwb3J0IGNvbnN0IGhhbmRsZUdyYWRpZW50ID0gKHN0cm9rZUNvbG9yKSA9PiB7XG4gICAgY29uc3QgeyBmcm9tID0gJyMxODkwZmYnLCB0byA9ICcjMTg5MGZmJywgZGlyZWN0aW9uID0gJ3RvIHJpZ2h0JyB9ID0gc3Ryb2tlQ29sb3IsIHJlc3QgPSBfX3Jlc3Qoc3Ryb2tlQ29sb3IsIFtcImZyb21cIiwgXCJ0b1wiLCBcImRpcmVjdGlvblwiXSk7XG4gICAgaWYgKE9iamVjdC5rZXlzKHJlc3QpLmxlbmd0aCAhPT0gMCkge1xuICAgICAgICBjb25zdCBzb3J0ZWRHcmFkaWVudHMgPSBzb3J0R3JhZGllbnQocmVzdCk7XG4gICAgICAgIHJldHVybiB7IGJhY2tncm91bmRJbWFnZTogYGxpbmVhci1ncmFkaWVudCgke2RpcmVjdGlvbn0sICR7c29ydGVkR3JhZGllbnRzfSlgIH07XG4gICAgfVxuICAgIHJldHVybiB7IGJhY2tncm91bmRJbWFnZTogYGxpbmVhci1ncmFkaWVudCgke2RpcmVjdGlvbn0sICR7ZnJvbX0sICR7dG99KWAgfTtcbn07XG5jb25zdCBMaW5lID0gcHJvcHMgPT4ge1xuICAgIGNvbnN0IHsgcHJlZml4Q2xzLCBwZXJjZW50LCBzdWNjZXNzUGVyY2VudCwgc3Ryb2tlV2lkdGgsIHNpemUsIHN0cm9rZUNvbG9yLCBzdHJva2VMaW5lY2FwLCBjaGlsZHJlbiwgfSA9IHByb3BzO1xuICAgIGxldCBiYWNrZ3JvdW5kUHJvcHM7XG4gICAgaWYgKHN0cm9rZUNvbG9yICYmIHR5cGVvZiBzdHJva2VDb2xvciAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgYmFja2dyb3VuZFByb3BzID0gaGFuZGxlR3JhZGllbnQoc3Ryb2tlQ29sb3IpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgYmFja2dyb3VuZFByb3BzID0ge1xuICAgICAgICAgICAgYmFja2dyb3VuZDogc3Ryb2tlQ29sb3IsXG4gICAgICAgIH07XG4gICAgfVxuICAgIGNvbnN0IHBlcmNlbnRTdHlsZSA9IE9iamVjdC5hc3NpZ24oeyB3aWR0aDogYCR7dmFsaWRQcm9ncmVzcyhwZXJjZW50KX0lYCwgaGVpZ2h0OiBzdHJva2VXaWR0aCB8fCAoc2l6ZSA9PT0gJ3NtYWxsJyA/IDYgOiA4KSwgYm9yZGVyUmFkaXVzOiBzdHJva2VMaW5lY2FwID09PSAnc3F1YXJlJyA/IDAgOiAnJyB9LCBiYWNrZ3JvdW5kUHJvcHMpO1xuICAgIGNvbnN0IHN1Y2Nlc3NQZXJjZW50U3R5bGUgPSB7XG4gICAgICAgIHdpZHRoOiBgJHt2YWxpZFByb2dyZXNzKHN1Y2Nlc3NQZXJjZW50KX0lYCxcbiAgICAgICAgaGVpZ2h0OiBzdHJva2VXaWR0aCB8fCAoc2l6ZSA9PT0gJ3NtYWxsJyA/IDYgOiA4KSxcbiAgICAgICAgYm9yZGVyUmFkaXVzOiBzdHJva2VMaW5lY2FwID09PSAnc3F1YXJlJyA/IDAgOiAnJyxcbiAgICB9O1xuICAgIGNvbnN0IHN1Y2Nlc3NTZWdtZW50ID0gc3VjY2Vzc1BlcmNlbnQgIT09IHVuZGVmaW5lZCA/ICg8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1zdWNjZXNzLWJnYH0gc3R5bGU9e3N1Y2Nlc3NQZXJjZW50U3R5bGV9Lz4pIDogbnVsbDtcbiAgICByZXR1cm4gKDxkaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1vdXRlcmB9PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1pbm5lcmB9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWJnYH0gc3R5bGU9e3BlcmNlbnRTdHlsZX0vPlxuICAgICAgICAgIHtzdWNjZXNzU2VnbWVudH1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIHtjaGlsZHJlbn1cbiAgICA8L2Rpdj4pO1xufTtcbmV4cG9ydCBkZWZhdWx0IExpbmU7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBOzs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFkQTtBQWdCQTs7Ozs7Ozs7Ozs7Ozs7OztBQWVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBTkE7QUFDQTtBQU9BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQXJCQTtBQUNBO0FBMkJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/progress/Line.js
