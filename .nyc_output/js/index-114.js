__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTodayTime", function() { return getTodayTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTitleString", function() { return getTitleString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTodayTimeStr", function() { return getTodayTimeStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMonthName", function() { return getMonthName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "syncTime", function() { return syncTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTimeConfig", function() { return getTimeConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTimeValidByConfig", function() { return isTimeValidByConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isTimeValid", function() { return isTimeValid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAllowedDate", function() { return isAllowedDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatDate", function() { return formatDate; });
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


var defaultDisabledTime = {
  disabledHours: function disabledHours() {
    return [];
  },
  disabledMinutes: function disabledMinutes() {
    return [];
  },
  disabledSeconds: function disabledSeconds() {
    return [];
  }
};
function getTodayTime(value) {
  var today = moment__WEBPACK_IMPORTED_MODULE_1___default()();
  today.locale(value.locale()).utcOffset(value.utcOffset());
  return today;
}
function getTitleString(value) {
  return value.format('LL');
}
function getTodayTimeStr(value) {
  var today = getTodayTime(value);
  return getTitleString(today);
}
function getMonthName(month) {
  var locale = month.locale();
  var localeData = month.localeData();
  return localeData[locale === 'zh-cn' ? 'months' : 'monthsShort'](month);
}
function syncTime(from, to) {
  if (!moment__WEBPACK_IMPORTED_MODULE_1___default.a.isMoment(from) || !moment__WEBPACK_IMPORTED_MODULE_1___default.a.isMoment(to)) return;
  to.hour(from.hour());
  to.minute(from.minute());
  to.second(from.second());
  to.millisecond(from.millisecond());
}
function getTimeConfig(value, disabledTime) {
  var disabledTimeConfig = disabledTime ? disabledTime(value) : {};
  disabledTimeConfig = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, defaultDisabledTime, disabledTimeConfig);
  return disabledTimeConfig;
}
function isTimeValidByConfig(value, disabledTimeConfig) {
  var invalidTime = false;

  if (value) {
    var hour = value.hour();
    var minutes = value.minute();
    var seconds = value.second();
    var disabledHours = disabledTimeConfig.disabledHours();

    if (disabledHours.indexOf(hour) === -1) {
      var disabledMinutes = disabledTimeConfig.disabledMinutes(hour);

      if (disabledMinutes.indexOf(minutes) === -1) {
        var disabledSeconds = disabledTimeConfig.disabledSeconds(hour, minutes);
        invalidTime = disabledSeconds.indexOf(seconds) !== -1;
      } else {
        invalidTime = true;
      }
    } else {
      invalidTime = true;
    }
  }

  return !invalidTime;
}
function isTimeValid(value, disabledTime) {
  var disabledTimeConfig = getTimeConfig(value, disabledTime);
  return isTimeValidByConfig(value, disabledTimeConfig);
}
function isAllowedDate(value, disabledDate, disabledTime) {
  if (disabledDate) {
    if (disabledDate(value)) {
      return false;
    }
  }

  if (disabledTime) {
    if (!isTimeValid(value, disabledTime)) {
      return false;
    }
  }

  return true;
}
function formatDate(value, format) {
  if (!value) {
    return '';
  }

  if (Array.isArray(format)) {
    format = format[0];
  }

  return value.format(format);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtY2FsZW5kYXIvZXMvdXRpbC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWNhbGVuZGFyL2VzL3V0aWwvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcblxudmFyIGRlZmF1bHREaXNhYmxlZFRpbWUgPSB7XG4gIGRpc2FibGVkSG91cnM6IGZ1bmN0aW9uIGRpc2FibGVkSG91cnMoKSB7XG4gICAgcmV0dXJuIFtdO1xuICB9LFxuICBkaXNhYmxlZE1pbnV0ZXM6IGZ1bmN0aW9uIGRpc2FibGVkTWludXRlcygpIHtcbiAgICByZXR1cm4gW107XG4gIH0sXG4gIGRpc2FibGVkU2Vjb25kczogZnVuY3Rpb24gZGlzYWJsZWRTZWNvbmRzKCkge1xuICAgIHJldHVybiBbXTtcbiAgfVxufTtcblxuZXhwb3J0IGZ1bmN0aW9uIGdldFRvZGF5VGltZSh2YWx1ZSkge1xuICB2YXIgdG9kYXkgPSBtb21lbnQoKTtcbiAgdG9kYXkubG9jYWxlKHZhbHVlLmxvY2FsZSgpKS51dGNPZmZzZXQodmFsdWUudXRjT2Zmc2V0KCkpO1xuICByZXR1cm4gdG9kYXk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRUaXRsZVN0cmluZyh2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUuZm9ybWF0KCdMTCcpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VG9kYXlUaW1lU3RyKHZhbHVlKSB7XG4gIHZhciB0b2RheSA9IGdldFRvZGF5VGltZSh2YWx1ZSk7XG4gIHJldHVybiBnZXRUaXRsZVN0cmluZyh0b2RheSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRNb250aE5hbWUobW9udGgpIHtcbiAgdmFyIGxvY2FsZSA9IG1vbnRoLmxvY2FsZSgpO1xuICB2YXIgbG9jYWxlRGF0YSA9IG1vbnRoLmxvY2FsZURhdGEoKTtcbiAgcmV0dXJuIGxvY2FsZURhdGFbbG9jYWxlID09PSAnemgtY24nID8gJ21vbnRocycgOiAnbW9udGhzU2hvcnQnXShtb250aCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzeW5jVGltZShmcm9tLCB0bykge1xuICBpZiAoIW1vbWVudC5pc01vbWVudChmcm9tKSB8fCAhbW9tZW50LmlzTW9tZW50KHRvKSkgcmV0dXJuO1xuICB0by5ob3VyKGZyb20uaG91cigpKTtcbiAgdG8ubWludXRlKGZyb20ubWludXRlKCkpO1xuICB0by5zZWNvbmQoZnJvbS5zZWNvbmQoKSk7XG4gIHRvLm1pbGxpc2Vjb25kKGZyb20ubWlsbGlzZWNvbmQoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRUaW1lQ29uZmlnKHZhbHVlLCBkaXNhYmxlZFRpbWUpIHtcbiAgdmFyIGRpc2FibGVkVGltZUNvbmZpZyA9IGRpc2FibGVkVGltZSA/IGRpc2FibGVkVGltZSh2YWx1ZSkgOiB7fTtcbiAgZGlzYWJsZWRUaW1lQ29uZmlnID0gX2V4dGVuZHMoe30sIGRlZmF1bHREaXNhYmxlZFRpbWUsIGRpc2FibGVkVGltZUNvbmZpZyk7XG4gIHJldHVybiBkaXNhYmxlZFRpbWVDb25maWc7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpc1RpbWVWYWxpZEJ5Q29uZmlnKHZhbHVlLCBkaXNhYmxlZFRpbWVDb25maWcpIHtcbiAgdmFyIGludmFsaWRUaW1lID0gZmFsc2U7XG4gIGlmICh2YWx1ZSkge1xuICAgIHZhciBob3VyID0gdmFsdWUuaG91cigpO1xuICAgIHZhciBtaW51dGVzID0gdmFsdWUubWludXRlKCk7XG4gICAgdmFyIHNlY29uZHMgPSB2YWx1ZS5zZWNvbmQoKTtcbiAgICB2YXIgZGlzYWJsZWRIb3VycyA9IGRpc2FibGVkVGltZUNvbmZpZy5kaXNhYmxlZEhvdXJzKCk7XG4gICAgaWYgKGRpc2FibGVkSG91cnMuaW5kZXhPZihob3VyKSA9PT0gLTEpIHtcbiAgICAgIHZhciBkaXNhYmxlZE1pbnV0ZXMgPSBkaXNhYmxlZFRpbWVDb25maWcuZGlzYWJsZWRNaW51dGVzKGhvdXIpO1xuICAgICAgaWYgKGRpc2FibGVkTWludXRlcy5pbmRleE9mKG1pbnV0ZXMpID09PSAtMSkge1xuICAgICAgICB2YXIgZGlzYWJsZWRTZWNvbmRzID0gZGlzYWJsZWRUaW1lQ29uZmlnLmRpc2FibGVkU2Vjb25kcyhob3VyLCBtaW51dGVzKTtcbiAgICAgICAgaW52YWxpZFRpbWUgPSBkaXNhYmxlZFNlY29uZHMuaW5kZXhPZihzZWNvbmRzKSAhPT0gLTE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbnZhbGlkVGltZSA9IHRydWU7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGludmFsaWRUaW1lID0gdHJ1ZTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuICFpbnZhbGlkVGltZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGlzVGltZVZhbGlkKHZhbHVlLCBkaXNhYmxlZFRpbWUpIHtcbiAgdmFyIGRpc2FibGVkVGltZUNvbmZpZyA9IGdldFRpbWVDb25maWcodmFsdWUsIGRpc2FibGVkVGltZSk7XG4gIHJldHVybiBpc1RpbWVWYWxpZEJ5Q29uZmlnKHZhbHVlLCBkaXNhYmxlZFRpbWVDb25maWcpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaXNBbGxvd2VkRGF0ZSh2YWx1ZSwgZGlzYWJsZWREYXRlLCBkaXNhYmxlZFRpbWUpIHtcbiAgaWYgKGRpc2FibGVkRGF0ZSkge1xuICAgIGlmIChkaXNhYmxlZERhdGUodmFsdWUpKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG4gIGlmIChkaXNhYmxlZFRpbWUpIHtcbiAgICBpZiAoIWlzVGltZVZhbGlkKHZhbHVlLCBkaXNhYmxlZFRpbWUpKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9XG4gIHJldHVybiB0cnVlO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZm9ybWF0RGF0ZSh2YWx1ZSwgZm9ybWF0KSB7XG4gIGlmICghdmFsdWUpIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICBpZiAoQXJyYXkuaXNBcnJheShmb3JtYXQpKSB7XG4gICAgZm9ybWF0ID0gZm9ybWF0WzBdO1xuICB9XG5cbiAgcmV0dXJuIHZhbHVlLmZvcm1hdChmb3JtYXQpO1xufSJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-calendar/es/util/index.js
