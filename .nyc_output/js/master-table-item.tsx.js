__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMasterTableItemText", function() { return getMasterTableItemText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formIdToValue", function() { return formIdToValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initMasterTables", function() { return initMasterTables; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useMasterTables", function() { return useMasterTables; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdToValueWidget", function() { return IdToValueWidget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdToValueWithTableWidget", function() { return IdToValueWithTableWidget; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);


var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/useful-form-item/utils/master-table-item.tsx";
// 通过id获取value



function getMasterTableItemText(codeTables, tableName, itemId) {
  var table = codeTables[tableName];
  return formIdToValue(table, itemId);
}
function formIdToValue(table, itemId) {
  var item = (table || []).find(function (option) {
    return itemId === option.id;
  }) || {};
  return item.text || "";
}
function initMasterTables(props) {
  var codeTables = {};

  if (props.initCodeTables) {
    codeTables = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, props.initCodeTables, {}, codeTables);
  }

  var ajaxTasks = props.tables.map(function (item) {
    return _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].get("/mastertable", {
      tableName: item,
      productCode: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "productCode"),
      itntCode: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "itntCode"),
      productVersion: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "productVersion", "")
    }).catch(function (error) {
      console.error(error);
    });
  });
  return Promise.all(ajaxTasks).then(function (tasks) {
    props.tables.forEach(function (item, index) {
      codeTables[item] = lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(tasks, "".concat(index, ".body.respData.items"), []);
    });
    return codeTables;
  });
}
function useMasterTables(props) {
  var _React$useState = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useState({}),
      _React$useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_React$useState, 2),
      masterTables = _React$useState2[0],
      changeMasterTables = _React$useState2[1];

  _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useEffect(function () {
    initMasterTables(props).then(function (data) {
      return changeMasterTables(data);
    });
  }, []);

  var transMasterTableItem = function transMasterTableItem(tableName, itemId) {
    return getMasterTableItemText(masterTables, tableName, itemId);
  };

  return [masterTables, transMasterTableItem];
}
var IdToValueWidget = function IdToValueWidget(props) {
  var _React$useState3 = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useState(props.options || []),
      _React$useState4 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_React$useState3, 2),
      options = _React$useState4[0],
      changeOptions = _React$useState4[1];

  _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useEffect(function () {
    if (!props.fetch) return;
    props.fetch && props.fetch().then(function (res) {
      return changeOptions(res);
    });
  }, []);
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, formIdToValue(options, lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props, props.idKey || "id")) || lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props, props.idKey || "id"));
};
var IdToValueWithTableWidget = function IdToValueWithTableWidget(props) {
  var _React$useState5 = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useState([]),
      _React$useState6 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_React$useState5, 2),
      options = _React$useState6[0],
      changeOptions = _React$useState6[1];

  _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].useEffect(function () {
    if (!props.tableName) return;
    _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].get("/mastertable", {
      tableName: props.tableName,
      productCode: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "productCode"),
      itntCode: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "itntCode"),
      productVersion: lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(props.model, "productVersion", "")
    }).then(function (data) {
      var op = lodash__WEBPACK_IMPORTED_MODULE_4___default.a.get(data, "body.respData.items", []);

      changeOptions(op);
    });
  }, []);
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 100
    },
    __self: this
  }, formIdToValue(options, props.id) || props.id);
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vdXRpbHMvbWFzdGVyLXRhYmxlLWl0ZW0udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3VzZWZ1bC1mb3JtLWl0ZW0vdXRpbHMvbWFzdGVyLXRhYmxlLWl0ZW0udHN4Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIOmAmui/h2lk6I635Y+WdmFsdWVcbmltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBBamF4IH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcblxuZXhwb3J0IGZ1bmN0aW9uIGdldE1hc3RlclRhYmxlSXRlbVRleHQoY29kZVRhYmxlczogYW55LCB0YWJsZU5hbWU6IHN0cmluZywgaXRlbUlkOiBhbnkpIHtcbiAgY29uc3QgdGFibGUgPSBjb2RlVGFibGVzW3RhYmxlTmFtZV07XG4gIHJldHVybiBmb3JtSWRUb1ZhbHVlKHRhYmxlLCBpdGVtSWQpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZm9ybUlkVG9WYWx1ZSh0YWJsZTogYW55LCBpdGVtSWQ6IGFueSkge1xuICBjb25zdCBpdGVtID1cbiAgICAodGFibGUgfHwgW10pLmZpbmQoKG9wdGlvbjogYW55KSA9PiB7XG4gICAgICByZXR1cm4gaXRlbUlkID09PSBvcHRpb24uaWQ7XG4gICAgfSkgfHwge307XG4gIHJldHVybiBpdGVtLnRleHQgfHwgXCJcIjtcbn1cblxuaW50ZXJmYWNlIElQcm9wc01hc3RlclRhYmxlcyB7XG4gIG1vZGVsOiBhbnk7XG4gIHRhYmxlczogYW55O1xuICBpbml0Q29kZVRhYmxlcz86IGFueTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGluaXRNYXN0ZXJUYWJsZXMocHJvcHM6IElQcm9wc01hc3RlclRhYmxlcykge1xuICBsZXQgY29kZVRhYmxlczogYW55ID0ge307XG4gIGlmIChwcm9wcy5pbml0Q29kZVRhYmxlcykge1xuICAgIGNvZGVUYWJsZXMgPSB7XG4gICAgICAuLi5wcm9wcy5pbml0Q29kZVRhYmxlcyxcbiAgICAgIC4uLmNvZGVUYWJsZXMsXG4gICAgfTtcbiAgfVxuICBjb25zdCBhamF4VGFza3MgPSBwcm9wcy50YWJsZXMubWFwKChpdGVtOiBzdHJpbmcpID0+XG4gICAgQWpheC5nZXQoXCIvbWFzdGVydGFibGVcIiwge1xuICAgICAgdGFibGVOYW1lOiBpdGVtLFxuICAgICAgcHJvZHVjdENvZGU6IF8uZ2V0KHByb3BzLm1vZGVsLCBcInByb2R1Y3RDb2RlXCIpLFxuICAgICAgaXRudENvZGU6IF8uZ2V0KHByb3BzLm1vZGVsLCBcIml0bnRDb2RlXCIpLFxuICAgICAgcHJvZHVjdFZlcnNpb246IF8uZ2V0KHByb3BzLm1vZGVsLCBcInByb2R1Y3RWZXJzaW9uXCIsIFwiXCIpLFxuICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH0pLFxuICApO1xuICByZXR1cm4gUHJvbWlzZS5hbGwoYWpheFRhc2tzKS50aGVuKHRhc2tzID0+IHtcbiAgICBwcm9wcy50YWJsZXMuZm9yRWFjaCgoaXRlbTogc3RyaW5nLCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICBjb2RlVGFibGVzW2l0ZW1dID0gXy5nZXQodGFza3MsIGAke2luZGV4fS5ib2R5LnJlc3BEYXRhLml0ZW1zYCwgW10pO1xuICAgIH0pO1xuICAgIHJldHVybiBjb2RlVGFibGVzO1xuICB9KTtcbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gdXNlTWFzdGVyVGFibGVzKHByb3BzOiBJUHJvcHNNYXN0ZXJUYWJsZXMpIHtcbiAgY29uc3QgW21hc3RlclRhYmxlcywgY2hhbmdlTWFzdGVyVGFibGVzXSA9IFJlYWN0LnVzZVN0YXRlKHt9KTtcbiAgUmVhY3QudXNlRWZmZWN0KCgpID0+IHtcbiAgICBpbml0TWFzdGVyVGFibGVzKHByb3BzKVxuICAgICAgLnRoZW4oZGF0YSA9PiBjaGFuZ2VNYXN0ZXJUYWJsZXMoZGF0YSkpO1xuICB9LCBbXSk7XG4gIGNvbnN0IHRyYW5zTWFzdGVyVGFibGVJdGVtOiBhbnkgPSAodGFibGVOYW1lOiBzdHJpbmcsIGl0ZW1JZDogYW55KSA9PiBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KG1hc3RlclRhYmxlcywgdGFibGVOYW1lLCBpdGVtSWQpO1xuICByZXR1cm4gW21hc3RlclRhYmxlcywgdHJhbnNNYXN0ZXJUYWJsZUl0ZW1dO1xufVxuXG5pbnRlcmZhY2UgSVByb3BzSWRUb1ZhbHVlIHtcbiAgb3B0aW9ucz86IGFueTtcbiAgZmV0Y2g/OiBhbnk7XG4gIGlkOiBhbnk7XG4gIGlkS2V5PzogYW55O1xufVxuXG5leHBvcnQgY29uc3QgSWRUb1ZhbHVlV2lkZ2V0ID0gKHByb3BzOiBJUHJvcHNJZFRvVmFsdWUpID0+IHtcbiAgY29uc3QgW29wdGlvbnMsIGNoYW5nZU9wdGlvbnNdID0gUmVhY3QudXNlU3RhdGUocHJvcHMub3B0aW9ucyB8fCBbXSk7XG4gIFJlYWN0LnVzZUVmZmVjdCgoKSA9PiB7XG4gICAgaWYgKCFwcm9wcy5mZXRjaCkgcmV0dXJuO1xuICAgIHByb3BzLmZldGNoICYmIHByb3BzLmZldGNoKCkudGhlbihcbiAgICAgIChyZXM6IGFueSkgPT4gY2hhbmdlT3B0aW9ucyhyZXMpLFxuICAgICk7XG4gIH0sIFtdKTtcbiAgcmV0dXJuIDxzcGFuPntmb3JtSWRUb1ZhbHVlKG9wdGlvbnMsIF8uZ2V0KHByb3BzLCBwcm9wcy5pZEtleSB8fCBcImlkXCIpKSB8fCBfLmdldChwcm9wcywgcHJvcHMuaWRLZXkgfHwgXCJpZFwiKX08L3NwYW4+O1xufTtcblxuaW50ZXJmYWNlIElQcm9wc0lkVG9WYWx1ZVdpdGhUYWJsZSB7XG4gIHRhYmxlTmFtZTogYW55O1xuICBpZDogYW55O1xuICBtb2RlbDogYW55O1xufVxuXG5leHBvcnQgY29uc3QgSWRUb1ZhbHVlV2l0aFRhYmxlV2lkZ2V0OiBSZWFjdC5GQzxJUHJvcHNJZFRvVmFsdWVXaXRoVGFibGU+ID0gKHByb3BzKSA9PiB7XG4gIGNvbnN0IFtvcHRpb25zLCBjaGFuZ2VPcHRpb25zXSA9IFJlYWN0LnVzZVN0YXRlKFtdKTtcbiAgUmVhY3QudXNlRWZmZWN0KCgpID0+IHtcbiAgICBpZiAoIXByb3BzLnRhYmxlTmFtZSkgcmV0dXJuO1xuICAgIEFqYXguZ2V0KFwiL21hc3RlcnRhYmxlXCIsIHtcbiAgICAgIHRhYmxlTmFtZTogcHJvcHMudGFibGVOYW1lLFxuICAgICAgcHJvZHVjdENvZGU6IF8uZ2V0KHByb3BzLm1vZGVsLCBcInByb2R1Y3RDb2RlXCIpLFxuICAgICAgaXRudENvZGU6IF8uZ2V0KHByb3BzLm1vZGVsLCBcIml0bnRDb2RlXCIpLFxuICAgICAgcHJvZHVjdFZlcnNpb246IF8uZ2V0KHByb3BzLm1vZGVsLCBcInByb2R1Y3RWZXJzaW9uXCIsIFwiXCIpLFxuICAgIH0pLnRoZW4oKGRhdGEpID0+IHtcbiAgICAgIGNvbnN0IG9wID0gXy5nZXQoZGF0YSwgYGJvZHkucmVzcERhdGEuaXRlbXNgLCBbXSk7XG4gICAgICBjaGFuZ2VPcHRpb25zKG9wKTtcbiAgICB9KTtcbiAgfSwgW10pO1xuICByZXR1cm4gPHNwYW4+e2Zvcm1JZFRvVmFsdWUob3B0aW9ucywgcHJvcHMuaWQpIHx8IHByb3BzLmlkfTwvc3Bhbj47XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/useful-form-item/utils/master-table-item.tsx
