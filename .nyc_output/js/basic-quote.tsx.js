__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicQuote", function() { return BasicQuote; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _quote_step__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../quote-step */ "./src/app/desk/quote/quote-step.tsx");
/* harmony import */ var _desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk-component/antd/button-back */ "./src/app/desk/component/antd/button-back.tsx");






var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/basic-quote.tsx";






var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].getIsMobile();

var BasicQuote =
/*#__PURE__*/
function (_QuoteStep) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(BasicQuote, _QuoteStep);

  function BasicQuote() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, BasicQuote);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicQuote).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(BasicQuote, [{
    key: "initComponents",
    value: function initComponents() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_4__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(BasicQuote.prototype), "initComponents", this).call(this));
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "renderTitle",
    value: function renderTitle() {}
  }, {
    key: "handleNext",
    value: function handleNext() {}
  }, {
    key: "renderActions",
    value: function renderActions() {}
  }, {
    key: "renderRightActions",
    value: function renderRightActions() {
      var _this = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "action ".concat(isMobile ? "mobile-action-large" : ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_desk_component_antd_button_back__WEBPACK_IMPORTED_MODULE_11__["ButtonBack"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Button"], {
        size: "large",
        type: "primary",
        onClick: function onClick() {
          return _this.handleNext();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 55
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("Save and Continue").thai("Save and Continue").my("Save and Continue").getMessage()));
    }
  }, {
    key: "renderBasicInfo",
    value: function renderBasicInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Fragment, null);
    }
  }, {
    key: "renderSideInfo",
    value: function renderSideInfo() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Fragment, null);
    }
  }, {
    key: "renderEndoEffDate",
    value: function renderEndoEffDate() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].Fragment, null);
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      return "";
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      return "";
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Spin"], {
        size: "large",
        spinning: this.state.loading,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Row"], {
        type: "flex",
        justify: "space-between",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, this.renderSideInfo(), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_7__["Col"], {
        sm: 18,
        xs: 23,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement("div", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("BASIC INFO - <INDUSTRY ALL RISKS>").thai("BASIC INFO - <INDUSTRY ALL RISKS>").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NCollapse"], {
        defaultActiveKey: ["1", "2"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        },
        __self: this
      }, this.renderEndoEffDate(), _common_3rd__WEBPACK_IMPORTED_MODULE_6__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_9__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_8__["Language"].en("BASIC INFO").thai("BASIC INFO").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, this.renderBasicInfo())), this.renderRightActions())));
    }
  }]);

  return BasicQuote;
}(_quote_step__WEBPACK_IMPORTED_MODULE_10__["default"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtcXVvdGUudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvYmFzaWMtcXVvdGUudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24sIENvbCwgUm93LCBTcGluIH0gZnJvbSBcImFudGRcIjtcblxuaW1wb3J0IHsgQWpheCwgTGFuZ3VhZ2UsIFV0aWxzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTlBhbmVsIH0gZnJvbSBcIkBhcHAtY29tcG9uZW50XCI7XG5pbXBvcnQgUXVvdGVTdGVwLCB7IFN0ZXBDb21wb25lbnRzLCBTdGVwUHJvcHMgfSBmcm9tIFwiLi4vLi4vcXVvdGUtc3RlcFwiO1xuaW1wb3J0IHsgQnV0dG9uQmFjayB9IGZyb20gXCJAZGVzay1jb21wb25lbnQvYW50ZC9idXR0b24tYmFja1wiO1xuXG5cbmNvbnN0IGZvcm1EYXRlTGF5b3V0ID0ge1xuICBsYWJlbENvbDoge1xuICAgIHhzOiB7IHNwYW46IDEyIH0sXG4gICAgc206IHsgc3BhbjogMTIgfSxcbiAgfSxcbiAgd3JhcHBlckNvbDoge1xuICAgIHhzOiB7IHNwYW46IDEyIH0sXG4gICAgc206IHsgc3BhbjogMTIgfSxcbiAgfSxcbn07XG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5cbmludGVyZmFjZSBJU3RhdGUge1xuICBsb2FkaW5nOiBib29sZWFuO1xufVxuXG5jbGFzcyBCYXNpY1F1b3RlPFAgZXh0ZW5kcyBTdGVwUHJvcHMsIFMgZXh0ZW5kcyBJU3RhdGUsIEMgZXh0ZW5kcyBTdGVwQ29tcG9uZW50cz4gZXh0ZW5kcyBRdW90ZVN0ZXA8UCwgUywgQz4ge1xuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdENvbXBvbmVudHMoKSkgYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgIH0gYXMgUztcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuXG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyVGl0bGUoKSB7XG4gIH1cblxuICBoYW5kbGVOZXh0KCkge1xuICB9XG5cbiAgcHJvdGVjdGVkIHJlbmRlckFjdGlvbnMoKSB7XG4gIH1cblxuICByZW5kZXJSaWdodEFjdGlvbnMoKSB7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2BhY3Rpb24gJHtpc01vYmlsZSA/IFwibW9iaWxlLWFjdGlvbi1sYXJnZVwiIDogXCJcIn1gfT5cbiAgICAgICAgPEJ1dHRvbkJhY2svPlxuICAgICAgICA8QnV0dG9uIHNpemU9XCJsYXJnZVwiIHR5cGU9XCJwcmltYXJ5XCIgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVOZXh0KCl9PlxuICAgICAgICAgIHtMYW5ndWFnZS5lbihcIlNhdmUgYW5kIENvbnRpbnVlXCIpLnRoYWkoXCJTYXZlIGFuZCBDb250aW51ZVwiKS5teShcIlNhdmUgYW5kIENvbnRpbnVlXCIpLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgPC9CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG5cbiAgcmVuZGVyQmFzaWNJbmZvKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlclNpZGVJbmZvKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIHJlbmRlckVuZG9FZmZEYXRlKCkge1xuICAgIHJldHVybiA8PjwvPjtcbiAgfVxuXG4gIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKSB7XG4gICAgcmV0dXJuIFwiXCI7XG4gIH1cblxuICBnZXRQcm9wKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgcmV0dXJuIFwiXCI7XG4gIH1cblxuICBwcm90ZWN0ZWQgcmVuZGVyQ29udGVudCgpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFNwaW4gc2l6ZT1cImxhcmdlXCIgc3Bpbm5pbmc9e3RoaXMuc3RhdGUubG9hZGluZ30+XG4gICAgICAgIDxSb3cgdHlwZT1cImZsZXhcIiBqdXN0aWZ5PXtcInNwYWNlLWJldHdlZW5cIn0+XG4gICAgICAgICAge3RoaXMucmVuZGVyU2lkZUluZm8oKX1cbiAgICAgICAgICA8Q29sIHNtPXsxOH0geHM9ezIzfT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidGl0bGVcIj5cbiAgICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwiQkFTSUMgSU5GTyAtIDxJTkRVU1RSWSBBTEwgUklTS1M+XCIpLnRoYWkoXCJCQVNJQyBJTkZPIC0gPElORFVTVFJZIEFMTCBSSVNLUz5cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8TkNvbGxhcHNlIGRlZmF1bHRBY3RpdmVLZXk9e1tcIjFcIiwgXCIyXCJdfT5cbiAgICAgICAgICAgICAge3RoaXMucmVuZGVyRW5kb0VmZkRhdGUoKX1cbiAgICAgICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAgICAgIGtleT1cIjFcIlxuICAgICAgICAgICAgICAgIGhlYWRlcj17TGFuZ3VhZ2UuZW4oXCJCQVNJQyBJTkZPXCIpXG4gICAgICAgICAgICAgICAgICAudGhhaShcIkJBU0lDIElORk9cIilcbiAgICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGhpcy5yZW5kZXJCYXNpY0luZm8oKX1cbiAgICAgICAgICAgICAgPC9OUGFuZWw+XG4gICAgICAgICAgICA8L05Db2xsYXBzZT5cbiAgICAgICAgICAgIHt0aGlzLnJlbmRlclJpZ2h0QWN0aW9ucygpfVxuICAgICAgICAgIDwvQ29sPlxuICAgICAgICA8L1Jvdz5cbiAgICAgIDwvU3Bpbj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCB7IEJhc2ljUXVvdGUgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFGQTtBQUxBO0FBVUE7QUFDQTtBQUtBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTs7O0FBSUE7OztBQUdBOzs7QUFHQTs7O0FBR0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBOzs7O0FBakZBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/basic-quote.tsx
