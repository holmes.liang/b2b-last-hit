__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Base */ "./node_modules/antd/es/typography/Base.js");
/* harmony import */ var _util_type__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/type */ "./node_modules/antd/es/_util/type.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};





var TITLE_ELE_LIST = Object(_util_type__WEBPACK_IMPORTED_MODULE_3__["tupleNum"])(1, 2, 3, 4);

var Title = function Title(props) {
  var _props$level = props.level,
      level = _props$level === void 0 ? 1 : _props$level,
      restProps = __rest(props, ["level"]);

  var component;

  if (TITLE_ELE_LIST.indexOf(level) !== -1) {
    component = "h".concat(level);
  } else {
    warning__WEBPACK_IMPORTED_MODULE_1___default()(false, 'Title only accept `1 | 2 | 3 | 4` as `level` value.');
    component = 'h1';
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Base__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({}, restProps, {
    component: component
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Title);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90eXBvZ3JhcGh5L1RpdGxlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi90eXBvZ3JhcGh5L1RpdGxlLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2FybmluZyBmcm9tICd3YXJuaW5nJztcbmltcG9ydCBCYXNlIGZyb20gJy4vQmFzZSc7XG5pbXBvcnQgeyB0dXBsZU51bSB9IGZyb20gJy4uL191dGlsL3R5cGUnO1xuY29uc3QgVElUTEVfRUxFX0xJU1QgPSB0dXBsZU51bSgxLCAyLCAzLCA0KTtcbmNvbnN0IFRpdGxlID0gcHJvcHMgPT4ge1xuICAgIGNvbnN0IHsgbGV2ZWwgPSAxIH0gPSBwcm9wcywgcmVzdFByb3BzID0gX19yZXN0KHByb3BzLCBbXCJsZXZlbFwiXSk7XG4gICAgbGV0IGNvbXBvbmVudDtcbiAgICBpZiAoVElUTEVfRUxFX0xJU1QuaW5kZXhPZihsZXZlbCkgIT09IC0xKSB7XG4gICAgICAgIGNvbXBvbmVudCA9IGBoJHtsZXZlbH1gO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgICAgd2FybmluZyhmYWxzZSwgJ1RpdGxlIG9ubHkgYWNjZXB0IGAxIHwgMiB8IDMgfCA0YCBhcyBgbGV2ZWxgIHZhbHVlLicpO1xuICAgICAgICBjb21wb25lbnQgPSAnaDEnO1xuICAgIH1cbiAgICByZXR1cm4gPEJhc2Ugey4uLnJlc3RQcm9wc30gY29tcG9uZW50PXtjb21wb25lbnR9Lz47XG59O1xuZXhwb3J0IGRlZmF1bHQgVGl0bGU7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFWQTtBQUNBO0FBV0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/typography/Title.js
