
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @typechecks
 */

var camelize = __webpack_require__(/*! ./camelize */ "./node_modules/fbjs/lib/camelize.js");

var hyphenate = __webpack_require__(/*! ./hyphenate */ "./node_modules/fbjs/lib/hyphenate.js");

function asString(value)
/*?string*/
{
  return value == null ? value : String(value);
}

function getStyleProperty(
/*DOMNode*/
node,
/*string*/
name)
/*?string*/
{
  var computedStyle = void 0; // W3C Standard

  if (window.getComputedStyle) {
    // In certain cases such as within an iframe in FF3, this returns null.
    computedStyle = window.getComputedStyle(node, null);

    if (computedStyle) {
      return asString(computedStyle.getPropertyValue(hyphenate(name)));
    }
  } // Safari


  if (document.defaultView && document.defaultView.getComputedStyle) {
    computedStyle = document.defaultView.getComputedStyle(node, null); // A Safari bug causes this to return null for `display: none` elements.

    if (computedStyle) {
      return asString(computedStyle.getPropertyValue(hyphenate(name)));
    }

    if (name === 'display') {
      return 'none';
    }
  } // Internet Explorer


  if (node.currentStyle) {
    if (name === 'float') {
      return asString(node.currentStyle.cssFloat || node.currentStyle.styleFloat);
    }

    return asString(node.currentStyle[camelize(name)]);
  }

  return asString(node.style && node.style[camelize(name)]);
}

module.exports = getStyleProperty;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZmJqcy9saWIvZ2V0U3R5bGVQcm9wZXJ0eS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2ZianMvbGliL2dldFN0eWxlUHJvcGVydHkuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLlxuICpcbiAqIEB0eXBlY2hlY2tzXG4gKi9cblxudmFyIGNhbWVsaXplID0gcmVxdWlyZSgnLi9jYW1lbGl6ZScpO1xudmFyIGh5cGhlbmF0ZSA9IHJlcXVpcmUoJy4vaHlwaGVuYXRlJyk7XG5cbmZ1bmN0aW9uIGFzU3RyaW5nKHZhbHVlKSAvKj9zdHJpbmcqL3tcbiAgcmV0dXJuIHZhbHVlID09IG51bGwgPyB2YWx1ZSA6IFN0cmluZyh2YWx1ZSk7XG59XG5cbmZ1bmN0aW9uIGdldFN0eWxlUHJvcGVydHkoIC8qRE9NTm9kZSovbm9kZSwgLypzdHJpbmcqL25hbWUpIC8qP3N0cmluZyove1xuICB2YXIgY29tcHV0ZWRTdHlsZSA9IHZvaWQgMDtcblxuICAvLyBXM0MgU3RhbmRhcmRcbiAgaWYgKHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKSB7XG4gICAgLy8gSW4gY2VydGFpbiBjYXNlcyBzdWNoIGFzIHdpdGhpbiBhbiBpZnJhbWUgaW4gRkYzLCB0aGlzIHJldHVybnMgbnVsbC5cbiAgICBjb21wdXRlZFN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUobm9kZSwgbnVsbCk7XG4gICAgaWYgKGNvbXB1dGVkU3R5bGUpIHtcbiAgICAgIHJldHVybiBhc1N0cmluZyhjb21wdXRlZFN0eWxlLmdldFByb3BlcnR5VmFsdWUoaHlwaGVuYXRlKG5hbWUpKSk7XG4gICAgfVxuICB9XG4gIC8vIFNhZmFyaVxuICBpZiAoZG9jdW1lbnQuZGVmYXVsdFZpZXcgJiYgZG9jdW1lbnQuZGVmYXVsdFZpZXcuZ2V0Q29tcHV0ZWRTdHlsZSkge1xuICAgIGNvbXB1dGVkU3R5bGUgPSBkb2N1bWVudC5kZWZhdWx0Vmlldy5nZXRDb21wdXRlZFN0eWxlKG5vZGUsIG51bGwpO1xuICAgIC8vIEEgU2FmYXJpIGJ1ZyBjYXVzZXMgdGhpcyB0byByZXR1cm4gbnVsbCBmb3IgYGRpc3BsYXk6IG5vbmVgIGVsZW1lbnRzLlxuICAgIGlmIChjb21wdXRlZFN0eWxlKSB7XG4gICAgICByZXR1cm4gYXNTdHJpbmcoY29tcHV0ZWRTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKGh5cGhlbmF0ZShuYW1lKSkpO1xuICAgIH1cbiAgICBpZiAobmFtZSA9PT0gJ2Rpc3BsYXknKSB7XG4gICAgICByZXR1cm4gJ25vbmUnO1xuICAgIH1cbiAgfVxuICAvLyBJbnRlcm5ldCBFeHBsb3JlclxuICBpZiAobm9kZS5jdXJyZW50U3R5bGUpIHtcbiAgICBpZiAobmFtZSA9PT0gJ2Zsb2F0Jykge1xuICAgICAgcmV0dXJuIGFzU3RyaW5nKG5vZGUuY3VycmVudFN0eWxlLmNzc0Zsb2F0IHx8IG5vZGUuY3VycmVudFN0eWxlLnN0eWxlRmxvYXQpO1xuICAgIH1cbiAgICByZXR1cm4gYXNTdHJpbmcobm9kZS5jdXJyZW50U3R5bGVbY2FtZWxpemUobmFtZSldKTtcbiAgfVxuICByZXR1cm4gYXNTdHJpbmcobm9kZS5zdHlsZSAmJiBub2RlLnN0eWxlW2NhbWVsaXplKG5hbWUpXSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZ2V0U3R5bGVQcm9wZXJ0eTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBRUE7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/fbjs/lib/getStyleProperty.js
