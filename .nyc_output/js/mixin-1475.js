

var Quill = __webpack_require__(/*! quill */ "./node_modules/quill/dist/quill.js");

var QuillMixin = {
  /**
  Creates an editor on the given element. The editor will
  be passed the configuration, have its events bound,
  */
  createEditor: function createEditor($el, config) {
    var editor = new Quill($el, config);

    if (config.tabIndex !== undefined) {
      this.setEditorTabIndex(editor, config.tabIndex);
    }

    this.hookEditor(editor);
    return editor;
  },
  hookEditor: function hookEditor(editor) {
    // Expose the editor on change events via a weaker,
    // unprivileged proxy object that does not allow
    // accidentally modifying editor state.
    var unprivilegedEditor = this.makeUnprivilegedEditor(editor);

    this.handleTextChange = function (delta, oldDelta, source) {
      if (this.onEditorChangeText) {
        this.onEditorChangeText(editor.root.innerHTML, delta, source, unprivilegedEditor);
        this.onEditorChangeSelection(editor.getSelection(), source, unprivilegedEditor);
      }
    }.bind(this);

    this.handleSelectionChange = function (range, oldRange, source) {
      if (this.onEditorChangeSelection) {
        this.onEditorChangeSelection(range, source, unprivilegedEditor);
      }
    }.bind(this);

    editor.on('editor-change', function (eventType, rangeOrDelta, oldRangeOrOldDelta, source) {
      if (eventType === Quill.events.SELECTION_CHANGE) {
        this.handleSelectionChange(rangeOrDelta, oldRangeOrOldDelta, source);
      }

      if (eventType === Quill.events.TEXT_CHANGE) {
        this.handleTextChange(rangeOrDelta, oldRangeOrOldDelta, source);
      }
    }.bind(this));
  },
  unhookEditor: function unhookEditor(editor) {
    editor.off('selection-change');
    editor.off('text-change');
  },
  setEditorReadOnly: function setEditorReadOnly(editor, value) {
    value ? editor.disable() : editor.enable();
  },

  /*
  Replace the contents of the editor, but keep
  the previous selection hanging around so that
  the cursor won't move.
  */
  setEditorContents: function setEditorContents(editor, value) {
    var sel = editor.getSelection();

    if (typeof value === 'string') {
      editor.setContents(editor.clipboard.convert(value));
    } else {
      editor.setContents(value);
    }

    if (sel && editor.hasFocus()) this.setEditorSelection(editor, sel);
  },
  setEditorSelection: function setEditorSelection(editor, range) {
    if (range) {
      // Validate bounds before applying.
      var length = editor.getLength();
      range.index = Math.max(0, Math.min(range.index, length - 1));
      range.length = Math.max(0, Math.min(range.length, length - 1 - range.index));
    }

    editor.setSelection(range);
  },
  setEditorTabIndex: function setEditorTabIndex(editor, tabIndex) {
    if (editor.editor && editor.editor.scroll && editor.editor.scroll.domNode) {
      editor.editor.scroll.domNode.tabIndex = tabIndex;
    }
  },

  /*
  Returns an weaker, unprivileged proxy object that only
  exposes read-only accessors found on the editor instance,
  without any state-modificating methods.
  */
  makeUnprivilegedEditor: function makeUnprivilegedEditor(editor) {
    var e = editor;
    return {
      getLength: function getLength() {
        return e.getLength.apply(e, arguments);
      },
      getText: function getText() {
        return e.getText.apply(e, arguments);
      },
      getHTML: function getHTML() {
        return e.root.innerHTML;
      },
      getContents: function getContents() {
        return e.getContents.apply(e, arguments);
      },
      getSelection: function getSelection() {
        return e.getSelection.apply(e, arguments);
      },
      getBounds: function getBounds() {
        return e.getBounds.apply(e, arguments);
      }
    };
  }
};
module.exports = QuillMixin;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtcXVpbGwvbGliL21peGluLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmVhY3QtcXVpbGwvbGliL21peGluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxudmFyIFF1aWxsID0gcmVxdWlyZSgncXVpbGwnKTtcblxudmFyIFF1aWxsTWl4aW4gPSB7XG5cblx0LyoqXG5cdENyZWF0ZXMgYW4gZWRpdG9yIG9uIHRoZSBnaXZlbiBlbGVtZW50LiBUaGUgZWRpdG9yIHdpbGxcblx0YmUgcGFzc2VkIHRoZSBjb25maWd1cmF0aW9uLCBoYXZlIGl0cyBldmVudHMgYm91bmQsXG5cdCovXG5cdGNyZWF0ZUVkaXRvcjogZnVuY3Rpb24oJGVsLCBjb25maWcpIHtcblx0XHR2YXIgZWRpdG9yID0gbmV3IFF1aWxsKCRlbCwgY29uZmlnKTtcblx0XHRpZiAoY29uZmlnLnRhYkluZGV4ICE9PSB1bmRlZmluZWQpIHtcblx0XHRcdHRoaXMuc2V0RWRpdG9yVGFiSW5kZXgoZWRpdG9yLCBjb25maWcudGFiSW5kZXgpO1xuXHRcdH1cblx0XHR0aGlzLmhvb2tFZGl0b3IoZWRpdG9yKTtcblx0XHRyZXR1cm4gZWRpdG9yO1xuXHR9LFxuXG5cdGhvb2tFZGl0b3I6IGZ1bmN0aW9uKGVkaXRvcikge1xuXHRcdC8vIEV4cG9zZSB0aGUgZWRpdG9yIG9uIGNoYW5nZSBldmVudHMgdmlhIGEgd2Vha2VyLFxuXHRcdC8vIHVucHJpdmlsZWdlZCBwcm94eSBvYmplY3QgdGhhdCBkb2VzIG5vdCBhbGxvd1xuXHRcdC8vIGFjY2lkZW50YWxseSBtb2RpZnlpbmcgZWRpdG9yIHN0YXRlLlxuXHRcdHZhciB1bnByaXZpbGVnZWRFZGl0b3IgPSB0aGlzLm1ha2VVbnByaXZpbGVnZWRFZGl0b3IoZWRpdG9yKTtcblxuXHRcdHRoaXMuaGFuZGxlVGV4dENoYW5nZSA9IGZ1bmN0aW9uKGRlbHRhLCBvbGREZWx0YSwgc291cmNlKSB7XG5cdFx0XHRpZiAodGhpcy5vbkVkaXRvckNoYW5nZVRleHQpIHtcblx0XHRcdFx0dGhpcy5vbkVkaXRvckNoYW5nZVRleHQoXG5cdFx0XHRcdFx0ZWRpdG9yLnJvb3QuaW5uZXJIVE1MLCBkZWx0YSwgc291cmNlLFxuXHRcdFx0XHRcdHVucHJpdmlsZWdlZEVkaXRvclxuXHRcdFx0XHQpO1xuXHRcdFx0XHR0aGlzLm9uRWRpdG9yQ2hhbmdlU2VsZWN0aW9uKFxuXHRcdFx0XHRcdGVkaXRvci5nZXRTZWxlY3Rpb24oKSwgc291cmNlLFxuXHRcdFx0XHRcdHVucHJpdmlsZWdlZEVkaXRvclxuXHRcdFx0XHQpO1xuXHRcdFx0fVxuXHRcdH0uYmluZCh0aGlzKTtcblxuXHRcdHRoaXMuaGFuZGxlU2VsZWN0aW9uQ2hhbmdlID0gZnVuY3Rpb24ocmFuZ2UsIG9sZFJhbmdlLCBzb3VyY2UpIHtcblx0XHRcdGlmICh0aGlzLm9uRWRpdG9yQ2hhbmdlU2VsZWN0aW9uKSB7XG5cdFx0XHRcdHRoaXMub25FZGl0b3JDaGFuZ2VTZWxlY3Rpb24oXG5cdFx0XHRcdFx0cmFuZ2UsIHNvdXJjZSxcblx0XHRcdFx0XHR1bnByaXZpbGVnZWRFZGl0b3Jcblx0XHRcdFx0KTtcblx0XHRcdH1cblx0XHR9LmJpbmQodGhpcyk7XG5cblx0XHRlZGl0b3Iub24oJ2VkaXRvci1jaGFuZ2UnLCBmdW5jdGlvbihldmVudFR5cGUsIHJhbmdlT3JEZWx0YSwgb2xkUmFuZ2VPck9sZERlbHRhLCBzb3VyY2UpIHtcblx0XHRcdGlmIChldmVudFR5cGUgPT09IFF1aWxsLmV2ZW50cy5TRUxFQ1RJT05fQ0hBTkdFKSB7XG5cdFx0XHRcdHRoaXMuaGFuZGxlU2VsZWN0aW9uQ2hhbmdlKHJhbmdlT3JEZWx0YSwgb2xkUmFuZ2VPck9sZERlbHRhLCBzb3VyY2UpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRpZiAoZXZlbnRUeXBlID09PSBRdWlsbC5ldmVudHMuVEVYVF9DSEFOR0UpIHtcblx0XHRcdFx0dGhpcy5oYW5kbGVUZXh0Q2hhbmdlKHJhbmdlT3JEZWx0YSwgb2xkUmFuZ2VPck9sZERlbHRhLCBzb3VyY2UpO1xuXHRcdFx0fVxuXHRcdH0uYmluZCh0aGlzKSk7XG5cdH0sXG5cblx0dW5ob29rRWRpdG9yOiBmdW5jdGlvbihlZGl0b3IpIHtcblx0XHRlZGl0b3Iub2ZmKCdzZWxlY3Rpb24tY2hhbmdlJyk7XG5cdFx0ZWRpdG9yLm9mZigndGV4dC1jaGFuZ2UnKTtcblx0fSxcblxuXHRzZXRFZGl0b3JSZWFkT25seTogZnVuY3Rpb24oZWRpdG9yLCB2YWx1ZSkge1xuXHRcdHZhbHVlPyBlZGl0b3IuZGlzYWJsZSgpXG5cdFx0ICAgICA6IGVkaXRvci5lbmFibGUoKTtcblx0fSxcblxuXHQvKlxuXHRSZXBsYWNlIHRoZSBjb250ZW50cyBvZiB0aGUgZWRpdG9yLCBidXQga2VlcFxuXHR0aGUgcHJldmlvdXMgc2VsZWN0aW9uIGhhbmdpbmcgYXJvdW5kIHNvIHRoYXRcblx0dGhlIGN1cnNvciB3b24ndCBtb3ZlLlxuXHQqL1xuXHRzZXRFZGl0b3JDb250ZW50czogZnVuY3Rpb24oZWRpdG9yLCB2YWx1ZSkge1xuXHRcdHZhciBzZWwgPSBlZGl0b3IuZ2V0U2VsZWN0aW9uKCk7XG5cblx0XHRpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuXHRcdFx0ZWRpdG9yLnNldENvbnRlbnRzKGVkaXRvci5jbGlwYm9hcmQuY29udmVydCh2YWx1ZSkpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRlZGl0b3Iuc2V0Q29udGVudHModmFsdWUpO1xuXHRcdH1cblxuXHRcdGlmIChzZWwgJiYgZWRpdG9yLmhhc0ZvY3VzKCkpIHRoaXMuc2V0RWRpdG9yU2VsZWN0aW9uKGVkaXRvciwgc2VsKTtcblx0fSxcblxuXHRzZXRFZGl0b3JTZWxlY3Rpb246IGZ1bmN0aW9uKGVkaXRvciwgcmFuZ2UpIHtcblx0XHRpZiAocmFuZ2UpIHtcblx0XHRcdC8vIFZhbGlkYXRlIGJvdW5kcyBiZWZvcmUgYXBwbHlpbmcuXG5cdFx0XHR2YXIgbGVuZ3RoID0gZWRpdG9yLmdldExlbmd0aCgpO1xuXHRcdFx0cmFuZ2UuaW5kZXggPSBNYXRoLm1heCgwLCBNYXRoLm1pbihyYW5nZS5pbmRleCwgbGVuZ3RoLTEpKTtcblx0XHRcdHJhbmdlLmxlbmd0aCA9IE1hdGgubWF4KDAsIE1hdGgubWluKHJhbmdlLmxlbmd0aCwgKGxlbmd0aC0xKSAtIHJhbmdlLmluZGV4KSk7XG5cdFx0fVxuXHRcdGVkaXRvci5zZXRTZWxlY3Rpb24ocmFuZ2UpO1xuXHR9LFxuXG5cdHNldEVkaXRvclRhYkluZGV4OiBmdW5jdGlvbihlZGl0b3IsIHRhYkluZGV4KSB7XG5cdFx0aWYgKGVkaXRvci5lZGl0b3IgJiYgZWRpdG9yLmVkaXRvci5zY3JvbGwgJiYgZWRpdG9yLmVkaXRvci5zY3JvbGwuZG9tTm9kZSkge1xuXHRcdFx0ZWRpdG9yLmVkaXRvci5zY3JvbGwuZG9tTm9kZS50YWJJbmRleCA9IHRhYkluZGV4O1xuXHRcdH1cblx0fSxcblxuXHQvKlxuXHRSZXR1cm5zIGFuIHdlYWtlciwgdW5wcml2aWxlZ2VkIHByb3h5IG9iamVjdCB0aGF0IG9ubHlcblx0ZXhwb3NlcyByZWFkLW9ubHkgYWNjZXNzb3JzIGZvdW5kIG9uIHRoZSBlZGl0b3IgaW5zdGFuY2UsXG5cdHdpdGhvdXQgYW55IHN0YXRlLW1vZGlmaWNhdGluZyBtZXRob2RzLlxuXHQqL1xuXHRtYWtlVW5wcml2aWxlZ2VkRWRpdG9yOiBmdW5jdGlvbihlZGl0b3IpIHtcblx0XHR2YXIgZSA9IGVkaXRvcjtcblx0XHRyZXR1cm4ge1xuXHRcdFx0Z2V0TGVuZ3RoOiAgICBmdW5jdGlvbigpeyByZXR1cm4gZS5nZXRMZW5ndGguYXBwbHkoZSwgYXJndW1lbnRzKTsgfSxcblx0XHRcdGdldFRleHQ6ICAgICAgZnVuY3Rpb24oKXsgcmV0dXJuIGUuZ2V0VGV4dC5hcHBseShlLCBhcmd1bWVudHMpOyB9LFxuXHRcdFx0Z2V0SFRNTDogICAgICBmdW5jdGlvbigpeyByZXR1cm4gZS5yb290LmlubmVySFRNTCB9LFxuXHRcdFx0Z2V0Q29udGVudHM6ICBmdW5jdGlvbigpeyByZXR1cm4gZS5nZXRDb250ZW50cy5hcHBseShlLCBhcmd1bWVudHMpOyB9LFxuXHRcdFx0Z2V0U2VsZWN0aW9uOiBmdW5jdGlvbigpeyByZXR1cm4gZS5nZXRTZWxlY3Rpb24uYXBwbHkoZSwgYXJndW1lbnRzKTsgfSxcblx0XHRcdGdldEJvdW5kczogICAgZnVuY3Rpb24oKXsgcmV0dXJuIGUuZ2V0Qm91bmRzLmFwcGx5KGUsIGFyZ3VtZW50cyk7IH0sXG5cdFx0fTtcblx0fVxuXG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IFF1aWxsTWl4aW47XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFOQTtBQVFBO0FBaEhBO0FBb0hBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-quill/lib/mixin.js
