__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ClearableLabeledInput */ "./node_modules/antd/es/input/ClearableLabeledInput.js");
/* harmony import */ var _ResizableTextArea__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ResizableTextArea */ "./node_modules/antd/es/input/ResizableTextArea.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _Input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Input */ "./node_modules/antd/es/input/Input.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}








var TextArea =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TextArea, _React$Component);

  function TextArea(props) {
    var _this;

    _classCallCheck(this, TextArea);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TextArea).call(this, props));

    _this.saveTextArea = function (resizableTextArea) {
      _this.resizableTextArea = resizableTextArea;
    };

    _this.saveClearableInput = function (clearableInput) {
      _this.clearableInput = clearableInput;
    };

    _this.handleChange = function (e) {
      _this.setValue(e.target.value, function () {
        _this.resizableTextArea.resizeTextarea();
      });

      Object(_Input__WEBPACK_IMPORTED_MODULE_5__["resolveOnChange"])(_this.resizableTextArea.textArea, e, _this.props.onChange);
    };

    _this.handleKeyDown = function (e) {
      var _this$props = _this.props,
          onPressEnter = _this$props.onPressEnter,
          onKeyDown = _this$props.onKeyDown;

      if (e.keyCode === 13 && onPressEnter) {
        onPressEnter(e);
      }

      if (onKeyDown) {
        onKeyDown(e);
      }
    };

    _this.handleReset = function (e) {
      _this.setValue('', function () {
        _this.resizableTextArea.renderTextArea();

        _this.focus();
      });

      Object(_Input__WEBPACK_IMPORTED_MODULE_5__["resolveOnChange"])(_this.resizableTextArea.textArea, e, _this.props.onChange);
    };

    _this.renderTextArea = function (prefixCls) {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ResizableTextArea__WEBPACK_IMPORTED_MODULE_3__["default"], _extends({}, _this.props, {
        prefixCls: prefixCls,
        onKeyDown: _this.handleKeyDown,
        onChange: _this.handleChange,
        ref: _this.saveTextArea
      }));
    };

    _this.renderComponent = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var value = _this.state.value;
      var customizePrefixCls = _this.props.prefixCls;
      var prefixCls = getPrefixCls('input', customizePrefixCls);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_ClearableLabeledInput__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({}, _this.props, {
        prefixCls: prefixCls,
        inputType: "text",
        value: Object(_Input__WEBPACK_IMPORTED_MODULE_5__["fixControlledValue"])(value),
        element: _this.renderTextArea(prefixCls),
        handleReset: _this.handleReset,
        ref: _this.saveClearableInput
      }));
    };

    var value = typeof props.value === 'undefined' ? props.defaultValue : props.value;
    _this.state = {
      value: value
    };
    return _this;
  }

  _createClass(TextArea, [{
    key: "setValue",
    value: function setValue(value, callback) {
      if (!('value' in this.props)) {
        this.setState({
          value: value
        }, callback);
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      this.resizableTextArea.textArea.focus();
    }
  }, {
    key: "blur",
    value: function blur() {
      this.resizableTextArea.textArea.blur();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_4__["ConfigConsumer"], null, this.renderComponent);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      if ('value' in nextProps) {
        return {
          value: nextProps.value
        };
      }

      return null;
    }
  }]);

  return TextArea;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__["polyfill"])(TextArea);
/* harmony default export */ __webpack_exports__["default"] = (TextArea);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9pbnB1dC9UZXh0QXJlYS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvaW5wdXQvVGV4dEFyZWEuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IENsZWFyYWJsZUxhYmVsZWRJbnB1dCBmcm9tICcuL0NsZWFyYWJsZUxhYmVsZWRJbnB1dCc7XG5pbXBvcnQgUmVzaXphYmxlVGV4dEFyZWEgZnJvbSAnLi9SZXNpemFibGVUZXh0QXJlYSc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgeyBmaXhDb250cm9sbGVkVmFsdWUsIHJlc29sdmVPbkNoYW5nZSB9IGZyb20gJy4vSW5wdXQnO1xuY2xhc3MgVGV4dEFyZWEgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgICAgIHN1cGVyKHByb3BzKTtcbiAgICAgICAgdGhpcy5zYXZlVGV4dEFyZWEgPSAocmVzaXphYmxlVGV4dEFyZWEpID0+IHtcbiAgICAgICAgICAgIHRoaXMucmVzaXphYmxlVGV4dEFyZWEgPSByZXNpemFibGVUZXh0QXJlYTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zYXZlQ2xlYXJhYmxlSW5wdXQgPSAoY2xlYXJhYmxlSW5wdXQpID0+IHtcbiAgICAgICAgICAgIHRoaXMuY2xlYXJhYmxlSW5wdXQgPSBjbGVhcmFibGVJbnB1dDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVDaGFuZ2UgPSAoZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZShlLnRhcmdldC52YWx1ZSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzaXphYmxlVGV4dEFyZWEucmVzaXplVGV4dGFyZWEoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmVzb2x2ZU9uQ2hhbmdlKHRoaXMucmVzaXphYmxlVGV4dEFyZWEudGV4dEFyZWEsIGUsIHRoaXMucHJvcHMub25DaGFuZ2UpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZUtleURvd24gPSAoZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvblByZXNzRW50ZXIsIG9uS2V5RG93biB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChlLmtleUNvZGUgPT09IDEzICYmIG9uUHJlc3NFbnRlcikge1xuICAgICAgICAgICAgICAgIG9uUHJlc3NFbnRlcihlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChvbktleURvd24pIHtcbiAgICAgICAgICAgICAgICBvbktleURvd24oZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlUmVzZXQgPSAoZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZSgnJywgKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzaXphYmxlVGV4dEFyZWEucmVuZGVyVGV4dEFyZWEoKTtcbiAgICAgICAgICAgICAgICB0aGlzLmZvY3VzKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJlc29sdmVPbkNoYW5nZSh0aGlzLnJlc2l6YWJsZVRleHRBcmVhLnRleHRBcmVhLCBlLCB0aGlzLnByb3BzLm9uQ2hhbmdlKTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5yZW5kZXJUZXh0QXJlYSA9IChwcmVmaXhDbHMpID0+IHtcbiAgICAgICAgICAgIHJldHVybiAoPFJlc2l6YWJsZVRleHRBcmVhIHsuLi50aGlzLnByb3BzfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gb25LZXlEb3duPXt0aGlzLmhhbmRsZUtleURvd259IG9uQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZX0gcmVmPXt0aGlzLnNhdmVUZXh0QXJlYX0vPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQ29tcG9uZW50ID0gKHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgdmFsdWUgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdpbnB1dCcsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICByZXR1cm4gKDxDbGVhcmFibGVMYWJlbGVkSW5wdXQgey4uLnRoaXMucHJvcHN9IHByZWZpeENscz17cHJlZml4Q2xzfSBpbnB1dFR5cGU9XCJ0ZXh0XCIgdmFsdWU9e2ZpeENvbnRyb2xsZWRWYWx1ZSh2YWx1ZSl9IGVsZW1lbnQ9e3RoaXMucmVuZGVyVGV4dEFyZWEocHJlZml4Q2xzKX0gaGFuZGxlUmVzZXQ9e3RoaXMuaGFuZGxlUmVzZXR9IHJlZj17dGhpcy5zYXZlQ2xlYXJhYmxlSW5wdXR9Lz4pO1xuICAgICAgICB9O1xuICAgICAgICBjb25zdCB2YWx1ZSA9IHR5cGVvZiBwcm9wcy52YWx1ZSA9PT0gJ3VuZGVmaW5lZCcgPyBwcm9wcy5kZWZhdWx0VmFsdWUgOiBwcm9wcy52YWx1ZTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICB9O1xuICAgIH1cbiAgICBzdGF0aWMgZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKG5leHRQcm9wcykge1xuICAgICAgICBpZiAoJ3ZhbHVlJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdmFsdWU6IG5leHRQcm9wcy52YWx1ZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIHNldFZhbHVlKHZhbHVlLCBjYWxsYmFjaykge1xuICAgICAgICBpZiAoISgndmFsdWUnIGluIHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgdmFsdWUgfSwgY2FsbGJhY2spO1xuICAgICAgICB9XG4gICAgfVxuICAgIGZvY3VzKCkge1xuICAgICAgICB0aGlzLnJlc2l6YWJsZVRleHRBcmVhLnRleHRBcmVhLmZvY3VzKCk7XG4gICAgfVxuICAgIGJsdXIoKSB7XG4gICAgICAgIHRoaXMucmVzaXphYmxlVGV4dEFyZWEudGV4dEFyZWEuYmx1cigpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQ29tcG9uZW50fTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cbnBvbHlmaWxsKFRleHRBcmVhKTtcbmV4cG9ydCBkZWZhdWx0IFRleHRBcmVhO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFKQTtBQUNBO0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBUEE7QUFDQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFGQTtBQUNBO0FBR0E7QUFMQTtBQUNBO0FBTUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBREE7QUF4Q0E7QUEyQ0E7QUFDQTs7O0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQXJCQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7Ozs7QUFwREE7QUFDQTtBQW1FQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/input/TextArea.js
