/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var _symbol = __webpack_require__(/*! ../../util/symbol */ "./node_modules/echarts/lib/util/symbol.js");

var createSymbol = _symbol.createSymbol;

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var _listComponent = __webpack_require__(/*! ../helper/listComponent */ "./node_modules/echarts/lib/component/helper/listComponent.js");

var makeBackground = _listComponent.makeBackground;

var layoutUtil = __webpack_require__(/*! ../../util/layout */ "./node_modules/echarts/lib/util/layout.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var curry = zrUtil.curry;
var each = zrUtil.each;
var Group = graphic.Group;

var _default = echarts.extendComponentView({
  type: 'legend.plain',
  newlineDisabled: false,

  /**
   * @override
   */
  init: function init() {
    /**
     * @private
     * @type {module:zrender/container/Group}
     */
    this.group.add(this._contentGroup = new Group());
    /**
     * @private
     * @type {module:zrender/Element}
     */

    this._backgroundEl;
    /**
     * If first rendering, `contentGroup.position` is [0, 0], which
     * does not make sense and may cause unexepcted animation if adopted.
     * @private
     * @type {boolean}
     */

    this._isFirstRender = true;
  },

  /**
   * @protected
   */
  getContentGroup: function getContentGroup() {
    return this._contentGroup;
  },

  /**
   * @override
   */
  render: function render(legendModel, ecModel, api) {
    var isFirstRender = this._isFirstRender;
    this._isFirstRender = false;
    this.resetInner();

    if (!legendModel.get('show', true)) {
      return;
    }

    var itemAlign = legendModel.get('align');

    if (!itemAlign || itemAlign === 'auto') {
      itemAlign = legendModel.get('left') === 'right' && legendModel.get('orient') === 'vertical' ? 'right' : 'left';
    }

    this.renderInner(itemAlign, legendModel, ecModel, api); // Perform layout.

    var positionInfo = legendModel.getBoxLayoutParams();
    var viewportSize = {
      width: api.getWidth(),
      height: api.getHeight()
    };
    var padding = legendModel.get('padding');
    var maxSize = layoutUtil.getLayoutRect(positionInfo, viewportSize, padding);
    var mainRect = this.layoutInner(legendModel, itemAlign, maxSize, isFirstRender); // Place mainGroup, based on the calculated `mainRect`.

    var layoutRect = layoutUtil.getLayoutRect(zrUtil.defaults({
      width: mainRect.width,
      height: mainRect.height
    }, positionInfo), viewportSize, padding);
    this.group.attr('position', [layoutRect.x - mainRect.x, layoutRect.y - mainRect.y]); // Render background after group is layout.

    this.group.add(this._backgroundEl = makeBackground(mainRect, legendModel));
  },

  /**
   * @protected
   */
  resetInner: function resetInner() {
    this.getContentGroup().removeAll();
    this._backgroundEl && this.group.remove(this._backgroundEl);
  },

  /**
   * @protected
   */
  renderInner: function renderInner(itemAlign, legendModel, ecModel, api) {
    var contentGroup = this.getContentGroup();
    var legendDrawnMap = zrUtil.createHashMap();
    var selectMode = legendModel.get('selectedMode');
    var excludeSeriesId = [];
    ecModel.eachRawSeries(function (seriesModel) {
      !seriesModel.get('legendHoverLink') && excludeSeriesId.push(seriesModel.id);
    });
    each(legendModel.getData(), function (itemModel, dataIndex) {
      var name = itemModel.get('name'); // Use empty string or \n as a newline string

      if (!this.newlineDisabled && (name === '' || name === '\n')) {
        contentGroup.add(new Group({
          newline: true
        }));
        return;
      } // Representitive series.


      var seriesModel = ecModel.getSeriesByName(name)[0];

      if (legendDrawnMap.get(name)) {
        // Have been drawed
        return;
      } // Series legend


      if (seriesModel) {
        var data = seriesModel.getData();
        var color = data.getVisual('color'); // If color is a callback function

        if (typeof color === 'function') {
          // Use the first data
          color = color(seriesModel.getDataParams(0));
        } // Using rect symbol defaultly


        var legendSymbolType = data.getVisual('legendSymbol') || 'roundRect';
        var symbolType = data.getVisual('symbol');

        var itemGroup = this._createItem(name, dataIndex, itemModel, legendModel, legendSymbolType, symbolType, itemAlign, color, selectMode);

        itemGroup.on('click', curry(dispatchSelectAction, name, api)).on('mouseover', curry(dispatchHighlightAction, seriesModel.name, null, api, excludeSeriesId)).on('mouseout', curry(dispatchDownplayAction, seriesModel.name, null, api, excludeSeriesId));
        legendDrawnMap.set(name, true);
      } else {
        // Data legend of pie, funnel
        ecModel.eachRawSeries(function (seriesModel) {
          // In case multiple series has same data name
          if (legendDrawnMap.get(name)) {
            return;
          }

          if (seriesModel.legendDataProvider) {
            var data = seriesModel.legendDataProvider();
            var idx = data.indexOfName(name);

            if (idx < 0) {
              return;
            }

            var color = data.getItemVisual(idx, 'color');
            var legendSymbolType = 'roundRect';

            var itemGroup = this._createItem(name, dataIndex, itemModel, legendModel, legendSymbolType, null, itemAlign, color, selectMode); // FIXME: consider different series has items with the same name.


            itemGroup.on('click', curry(dispatchSelectAction, name, api)) // Should not specify the series name, consider legend controls
            // more than one pie series.
            .on('mouseover', curry(dispatchHighlightAction, null, name, api, excludeSeriesId)).on('mouseout', curry(dispatchDownplayAction, null, name, api, excludeSeriesId));
            legendDrawnMap.set(name, true);
          }
        }, this);
      }
    }, this);
  },
  _createItem: function _createItem(name, dataIndex, itemModel, legendModel, legendSymbolType, symbolType, itemAlign, color, selectMode) {
    var itemWidth = legendModel.get('itemWidth');
    var itemHeight = legendModel.get('itemHeight');
    var inactiveColor = legendModel.get('inactiveColor');
    var symbolKeepAspect = legendModel.get('symbolKeepAspect');
    var isSelected = legendModel.isSelected(name);
    var itemGroup = new Group();
    var textStyleModel = itemModel.getModel('textStyle');
    var itemIcon = itemModel.get('icon');
    var tooltipModel = itemModel.getModel('tooltip');
    var legendGlobalTooltipModel = tooltipModel.parentModel; // Use user given icon first

    legendSymbolType = itemIcon || legendSymbolType;
    itemGroup.add(createSymbol(legendSymbolType, 0, 0, itemWidth, itemHeight, isSelected ? color : inactiveColor, // symbolKeepAspect default true for legend
    symbolKeepAspect == null ? true : symbolKeepAspect)); // Compose symbols
    // PENDING

    if (!itemIcon && symbolType // At least show one symbol, can't be all none
    && (symbolType !== legendSymbolType || symbolType === 'none')) {
      var size = itemHeight * 0.8;

      if (symbolType === 'none') {
        symbolType = 'circle';
      } // Put symbol in the center


      itemGroup.add(createSymbol(symbolType, (itemWidth - size) / 2, (itemHeight - size) / 2, size, size, isSelected ? color : inactiveColor, // symbolKeepAspect default true for legend
      symbolKeepAspect == null ? true : symbolKeepAspect));
    }

    var textX = itemAlign === 'left' ? itemWidth + 5 : -5;
    var textAlign = itemAlign;
    var formatter = legendModel.get('formatter');
    var content = name;

    if (typeof formatter === 'string' && formatter) {
      content = formatter.replace('{name}', name != null ? name : '');
    } else if (typeof formatter === 'function') {
      content = formatter(name);
    }

    itemGroup.add(new graphic.Text({
      style: graphic.setTextStyle({}, textStyleModel, {
        text: content,
        x: textX,
        y: itemHeight / 2,
        textFill: isSelected ? textStyleModel.getTextColor() : inactiveColor,
        textAlign: textAlign,
        textVerticalAlign: 'middle'
      })
    })); // Add a invisible rect to increase the area of mouse hover

    var hitRect = new graphic.Rect({
      shape: itemGroup.getBoundingRect(),
      invisible: true,
      tooltip: tooltipModel.get('show') ? zrUtil.extend({
        content: name,
        // Defaul formatter
        formatter: legendGlobalTooltipModel.get('formatter', true) || function () {
          return name;
        },
        formatterParams: {
          componentType: 'legend',
          legendIndex: legendModel.componentIndex,
          name: name,
          $vars: ['name']
        }
      }, tooltipModel.option) : null
    });
    itemGroup.add(hitRect);
    itemGroup.eachChild(function (child) {
      child.silent = true;
    });
    hitRect.silent = !selectMode;
    this.getContentGroup().add(itemGroup);
    graphic.setHoverStyle(itemGroup);
    itemGroup.__legendDataIndex = dataIndex;
    return itemGroup;
  },

  /**
   * @protected
   */
  layoutInner: function layoutInner(legendModel, itemAlign, maxSize) {
    var contentGroup = this.getContentGroup(); // Place items in contentGroup.

    layoutUtil.box(legendModel.get('orient'), contentGroup, legendModel.get('itemGap'), maxSize.width, maxSize.height);
    var contentRect = contentGroup.getBoundingRect();
    contentGroup.attr('position', [-contentRect.x, -contentRect.y]);
    return this.group.getBoundingRect();
  },

  /**
   * @protected
   */
  remove: function remove() {
    this.getContentGroup().removeAll();
    this._isFirstRender = true;
  }
});

function dispatchSelectAction(name, api) {
  api.dispatchAction({
    type: 'legendToggleSelect',
    name: name
  });
}

function dispatchHighlightAction(seriesName, dataName, api, excludeSeriesId) {
  // If element hover will move to a hoverLayer.
  var el = api.getZr().storage.getDisplayList()[0];

  if (!(el && el.useHoverLayer)) {
    api.dispatchAction({
      type: 'highlight',
      seriesName: seriesName,
      name: dataName,
      excludeSeriesId: excludeSeriesId
    });
  }
}

function dispatchDownplayAction(seriesName, dataName, api, excludeSeriesId) {
  // If element hover will move to a hoverLayer.
  var el = api.getZr().storage.getDisplayList()[0];

  if (!(el && el.useHoverLayer)) {
    api.dispatchAction({
      type: 'downplay',
      seriesName: seriesName,
      name: dataName,
      excludeSeriesId: excludeSeriesId
    });
  }
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZC9MZWdlbmRWaWV3LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29tcG9uZW50L2xlZ2VuZC9MZWdlbmRWaWV3LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi8uLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgZWNoYXJ0cyA9IHJlcXVpcmUoXCIuLi8uLi9lY2hhcnRzXCIpO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIF9zeW1ib2wgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9zeW1ib2xcIik7XG5cbnZhciBjcmVhdGVTeW1ib2wgPSBfc3ltYm9sLmNyZWF0ZVN5bWJvbDtcblxudmFyIGdyYXBoaWMgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9ncmFwaGljXCIpO1xuXG52YXIgX2xpc3RDb21wb25lbnQgPSByZXF1aXJlKFwiLi4vaGVscGVyL2xpc3RDb21wb25lbnRcIik7XG5cbnZhciBtYWtlQmFja2dyb3VuZCA9IF9saXN0Q29tcG9uZW50Lm1ha2VCYWNrZ3JvdW5kO1xuXG52YXIgbGF5b3V0VXRpbCA9IHJlcXVpcmUoXCIuLi8uLi91dGlsL2xheW91dFwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xudmFyIGN1cnJ5ID0genJVdGlsLmN1cnJ5O1xudmFyIGVhY2ggPSB6clV0aWwuZWFjaDtcbnZhciBHcm91cCA9IGdyYXBoaWMuR3JvdXA7XG5cbnZhciBfZGVmYXVsdCA9IGVjaGFydHMuZXh0ZW5kQ29tcG9uZW50Vmlldyh7XG4gIHR5cGU6ICdsZWdlbmQucGxhaW4nLFxuICBuZXdsaW5lRGlzYWJsZWQ6IGZhbHNlLFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEB0eXBlIHttb2R1bGU6enJlbmRlci9jb250YWluZXIvR3JvdXB9XG4gICAgICovXG4gICAgdGhpcy5ncm91cC5hZGQodGhpcy5fY29udGVudEdyb3VwID0gbmV3IEdyb3VwKCkpO1xuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9XG4gICAgICovXG5cbiAgICB0aGlzLl9iYWNrZ3JvdW5kRWw7XG4gICAgLyoqXG4gICAgICogSWYgZmlyc3QgcmVuZGVyaW5nLCBgY29udGVudEdyb3VwLnBvc2l0aW9uYCBpcyBbMCwgMF0sIHdoaWNoXG4gICAgICogZG9lcyBub3QgbWFrZSBzZW5zZSBhbmQgbWF5IGNhdXNlIHVuZXhlcGN0ZWQgYW5pbWF0aW9uIGlmIGFkb3B0ZWQuXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICAgIHRoaXMuX2lzRmlyc3RSZW5kZXIgPSB0cnVlO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBnZXRDb250ZW50R3JvdXA6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fY29udGVudEdyb3VwO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAb3ZlcnJpZGVcbiAgICovXG4gIHJlbmRlcjogZnVuY3Rpb24gKGxlZ2VuZE1vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICB2YXIgaXNGaXJzdFJlbmRlciA9IHRoaXMuX2lzRmlyc3RSZW5kZXI7XG4gICAgdGhpcy5faXNGaXJzdFJlbmRlciA9IGZhbHNlO1xuICAgIHRoaXMucmVzZXRJbm5lcigpO1xuXG4gICAgaWYgKCFsZWdlbmRNb2RlbC5nZXQoJ3Nob3cnLCB0cnVlKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBpdGVtQWxpZ24gPSBsZWdlbmRNb2RlbC5nZXQoJ2FsaWduJyk7XG5cbiAgICBpZiAoIWl0ZW1BbGlnbiB8fCBpdGVtQWxpZ24gPT09ICdhdXRvJykge1xuICAgICAgaXRlbUFsaWduID0gbGVnZW5kTW9kZWwuZ2V0KCdsZWZ0JykgPT09ICdyaWdodCcgJiYgbGVnZW5kTW9kZWwuZ2V0KCdvcmllbnQnKSA9PT0gJ3ZlcnRpY2FsJyA/ICdyaWdodCcgOiAnbGVmdCc7XG4gICAgfVxuXG4gICAgdGhpcy5yZW5kZXJJbm5lcihpdGVtQWxpZ24sIGxlZ2VuZE1vZGVsLCBlY01vZGVsLCBhcGkpOyAvLyBQZXJmb3JtIGxheW91dC5cblxuICAgIHZhciBwb3NpdGlvbkluZm8gPSBsZWdlbmRNb2RlbC5nZXRCb3hMYXlvdXRQYXJhbXMoKTtcbiAgICB2YXIgdmlld3BvcnRTaXplID0ge1xuICAgICAgd2lkdGg6IGFwaS5nZXRXaWR0aCgpLFxuICAgICAgaGVpZ2h0OiBhcGkuZ2V0SGVpZ2h0KClcbiAgICB9O1xuICAgIHZhciBwYWRkaW5nID0gbGVnZW5kTW9kZWwuZ2V0KCdwYWRkaW5nJyk7XG4gICAgdmFyIG1heFNpemUgPSBsYXlvdXRVdGlsLmdldExheW91dFJlY3QocG9zaXRpb25JbmZvLCB2aWV3cG9ydFNpemUsIHBhZGRpbmcpO1xuICAgIHZhciBtYWluUmVjdCA9IHRoaXMubGF5b3V0SW5uZXIobGVnZW5kTW9kZWwsIGl0ZW1BbGlnbiwgbWF4U2l6ZSwgaXNGaXJzdFJlbmRlcik7IC8vIFBsYWNlIG1haW5Hcm91cCwgYmFzZWQgb24gdGhlIGNhbGN1bGF0ZWQgYG1haW5SZWN0YC5cblxuICAgIHZhciBsYXlvdXRSZWN0ID0gbGF5b3V0VXRpbC5nZXRMYXlvdXRSZWN0KHpyVXRpbC5kZWZhdWx0cyh7XG4gICAgICB3aWR0aDogbWFpblJlY3Qud2lkdGgsXG4gICAgICBoZWlnaHQ6IG1haW5SZWN0LmhlaWdodFxuICAgIH0sIHBvc2l0aW9uSW5mbyksIHZpZXdwb3J0U2l6ZSwgcGFkZGluZyk7XG4gICAgdGhpcy5ncm91cC5hdHRyKCdwb3NpdGlvbicsIFtsYXlvdXRSZWN0LnggLSBtYWluUmVjdC54LCBsYXlvdXRSZWN0LnkgLSBtYWluUmVjdC55XSk7IC8vIFJlbmRlciBiYWNrZ3JvdW5kIGFmdGVyIGdyb3VwIGlzIGxheW91dC5cblxuICAgIHRoaXMuZ3JvdXAuYWRkKHRoaXMuX2JhY2tncm91bmRFbCA9IG1ha2VCYWNrZ3JvdW5kKG1haW5SZWN0LCBsZWdlbmRNb2RlbCkpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICByZXNldElubmVyOiBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5nZXRDb250ZW50R3JvdXAoKS5yZW1vdmVBbGwoKTtcbiAgICB0aGlzLl9iYWNrZ3JvdW5kRWwgJiYgdGhpcy5ncm91cC5yZW1vdmUodGhpcy5fYmFja2dyb3VuZEVsKTtcbiAgfSxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgcmVuZGVySW5uZXI6IGZ1bmN0aW9uIChpdGVtQWxpZ24sIGxlZ2VuZE1vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICB2YXIgY29udGVudEdyb3VwID0gdGhpcy5nZXRDb250ZW50R3JvdXAoKTtcbiAgICB2YXIgbGVnZW5kRHJhd25NYXAgPSB6clV0aWwuY3JlYXRlSGFzaE1hcCgpO1xuICAgIHZhciBzZWxlY3RNb2RlID0gbGVnZW5kTW9kZWwuZ2V0KCdzZWxlY3RlZE1vZGUnKTtcbiAgICB2YXIgZXhjbHVkZVNlcmllc0lkID0gW107XG4gICAgZWNNb2RlbC5lYWNoUmF3U2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgICAgIXNlcmllc01vZGVsLmdldCgnbGVnZW5kSG92ZXJMaW5rJykgJiYgZXhjbHVkZVNlcmllc0lkLnB1c2goc2VyaWVzTW9kZWwuaWQpO1xuICAgIH0pO1xuICAgIGVhY2gobGVnZW5kTW9kZWwuZ2V0RGF0YSgpLCBmdW5jdGlvbiAoaXRlbU1vZGVsLCBkYXRhSW5kZXgpIHtcbiAgICAgIHZhciBuYW1lID0gaXRlbU1vZGVsLmdldCgnbmFtZScpOyAvLyBVc2UgZW1wdHkgc3RyaW5nIG9yIFxcbiBhcyBhIG5ld2xpbmUgc3RyaW5nXG5cbiAgICAgIGlmICghdGhpcy5uZXdsaW5lRGlzYWJsZWQgJiYgKG5hbWUgPT09ICcnIHx8IG5hbWUgPT09ICdcXG4nKSkge1xuICAgICAgICBjb250ZW50R3JvdXAuYWRkKG5ldyBHcm91cCh7XG4gICAgICAgICAgbmV3bGluZTogdHJ1ZVxuICAgICAgICB9KSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH0gLy8gUmVwcmVzZW50aXRpdmUgc2VyaWVzLlxuXG5cbiAgICAgIHZhciBzZXJpZXNNb2RlbCA9IGVjTW9kZWwuZ2V0U2VyaWVzQnlOYW1lKG5hbWUpWzBdO1xuXG4gICAgICBpZiAobGVnZW5kRHJhd25NYXAuZ2V0KG5hbWUpKSB7XG4gICAgICAgIC8vIEhhdmUgYmVlbiBkcmF3ZWRcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfSAvLyBTZXJpZXMgbGVnZW5kXG5cblxuICAgICAgaWYgKHNlcmllc01vZGVsKSB7XG4gICAgICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgICAgICB2YXIgY29sb3IgPSBkYXRhLmdldFZpc3VhbCgnY29sb3InKTsgLy8gSWYgY29sb3IgaXMgYSBjYWxsYmFjayBmdW5jdGlvblxuXG4gICAgICAgIGlmICh0eXBlb2YgY29sb3IgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAvLyBVc2UgdGhlIGZpcnN0IGRhdGFcbiAgICAgICAgICBjb2xvciA9IGNvbG9yKHNlcmllc01vZGVsLmdldERhdGFQYXJhbXMoMCkpO1xuICAgICAgICB9IC8vIFVzaW5nIHJlY3Qgc3ltYm9sIGRlZmF1bHRseVxuXG5cbiAgICAgICAgdmFyIGxlZ2VuZFN5bWJvbFR5cGUgPSBkYXRhLmdldFZpc3VhbCgnbGVnZW5kU3ltYm9sJykgfHwgJ3JvdW5kUmVjdCc7XG4gICAgICAgIHZhciBzeW1ib2xUeXBlID0gZGF0YS5nZXRWaXN1YWwoJ3N5bWJvbCcpO1xuXG4gICAgICAgIHZhciBpdGVtR3JvdXAgPSB0aGlzLl9jcmVhdGVJdGVtKG5hbWUsIGRhdGFJbmRleCwgaXRlbU1vZGVsLCBsZWdlbmRNb2RlbCwgbGVnZW5kU3ltYm9sVHlwZSwgc3ltYm9sVHlwZSwgaXRlbUFsaWduLCBjb2xvciwgc2VsZWN0TW9kZSk7XG5cbiAgICAgICAgaXRlbUdyb3VwLm9uKCdjbGljaycsIGN1cnJ5KGRpc3BhdGNoU2VsZWN0QWN0aW9uLCBuYW1lLCBhcGkpKS5vbignbW91c2VvdmVyJywgY3VycnkoZGlzcGF0Y2hIaWdobGlnaHRBY3Rpb24sIHNlcmllc01vZGVsLm5hbWUsIG51bGwsIGFwaSwgZXhjbHVkZVNlcmllc0lkKSkub24oJ21vdXNlb3V0JywgY3VycnkoZGlzcGF0Y2hEb3ducGxheUFjdGlvbiwgc2VyaWVzTW9kZWwubmFtZSwgbnVsbCwgYXBpLCBleGNsdWRlU2VyaWVzSWQpKTtcbiAgICAgICAgbGVnZW5kRHJhd25NYXAuc2V0KG5hbWUsIHRydWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gRGF0YSBsZWdlbmQgb2YgcGllLCBmdW5uZWxcbiAgICAgICAgZWNNb2RlbC5lYWNoUmF3U2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgICAgICAgIC8vIEluIGNhc2UgbXVsdGlwbGUgc2VyaWVzIGhhcyBzYW1lIGRhdGEgbmFtZVxuICAgICAgICAgIGlmIChsZWdlbmREcmF3bk1hcC5nZXQobmFtZSkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoc2VyaWVzTW9kZWwubGVnZW5kRGF0YVByb3ZpZGVyKSB7XG4gICAgICAgICAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmxlZ2VuZERhdGFQcm92aWRlcigpO1xuICAgICAgICAgICAgdmFyIGlkeCA9IGRhdGEuaW5kZXhPZk5hbWUobmFtZSk7XG5cbiAgICAgICAgICAgIGlmIChpZHggPCAwKSB7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdmFyIGNvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ2NvbG9yJyk7XG4gICAgICAgICAgICB2YXIgbGVnZW5kU3ltYm9sVHlwZSA9ICdyb3VuZFJlY3QnO1xuXG4gICAgICAgICAgICB2YXIgaXRlbUdyb3VwID0gdGhpcy5fY3JlYXRlSXRlbShuYW1lLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCwgbGVnZW5kTW9kZWwsIGxlZ2VuZFN5bWJvbFR5cGUsIG51bGwsIGl0ZW1BbGlnbiwgY29sb3IsIHNlbGVjdE1vZGUpOyAvLyBGSVhNRTogY29uc2lkZXIgZGlmZmVyZW50IHNlcmllcyBoYXMgaXRlbXMgd2l0aCB0aGUgc2FtZSBuYW1lLlxuXG5cbiAgICAgICAgICAgIGl0ZW1Hcm91cC5vbignY2xpY2snLCBjdXJyeShkaXNwYXRjaFNlbGVjdEFjdGlvbiwgbmFtZSwgYXBpKSkgLy8gU2hvdWxkIG5vdCBzcGVjaWZ5IHRoZSBzZXJpZXMgbmFtZSwgY29uc2lkZXIgbGVnZW5kIGNvbnRyb2xzXG4gICAgICAgICAgICAvLyBtb3JlIHRoYW4gb25lIHBpZSBzZXJpZXMuXG4gICAgICAgICAgICAub24oJ21vdXNlb3ZlcicsIGN1cnJ5KGRpc3BhdGNoSGlnaGxpZ2h0QWN0aW9uLCBudWxsLCBuYW1lLCBhcGksIGV4Y2x1ZGVTZXJpZXNJZCkpLm9uKCdtb3VzZW91dCcsIGN1cnJ5KGRpc3BhdGNoRG93bnBsYXlBY3Rpb24sIG51bGwsIG5hbWUsIGFwaSwgZXhjbHVkZVNlcmllc0lkKSk7XG4gICAgICAgICAgICBsZWdlbmREcmF3bk1hcC5zZXQobmFtZSwgdHJ1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LCB0aGlzKTtcbiAgICAgIH1cbiAgICB9LCB0aGlzKTtcbiAgfSxcbiAgX2NyZWF0ZUl0ZW06IGZ1bmN0aW9uIChuYW1lLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCwgbGVnZW5kTW9kZWwsIGxlZ2VuZFN5bWJvbFR5cGUsIHN5bWJvbFR5cGUsIGl0ZW1BbGlnbiwgY29sb3IsIHNlbGVjdE1vZGUpIHtcbiAgICB2YXIgaXRlbVdpZHRoID0gbGVnZW5kTW9kZWwuZ2V0KCdpdGVtV2lkdGgnKTtcbiAgICB2YXIgaXRlbUhlaWdodCA9IGxlZ2VuZE1vZGVsLmdldCgnaXRlbUhlaWdodCcpO1xuICAgIHZhciBpbmFjdGl2ZUNvbG9yID0gbGVnZW5kTW9kZWwuZ2V0KCdpbmFjdGl2ZUNvbG9yJyk7XG4gICAgdmFyIHN5bWJvbEtlZXBBc3BlY3QgPSBsZWdlbmRNb2RlbC5nZXQoJ3N5bWJvbEtlZXBBc3BlY3QnKTtcbiAgICB2YXIgaXNTZWxlY3RlZCA9IGxlZ2VuZE1vZGVsLmlzU2VsZWN0ZWQobmFtZSk7XG4gICAgdmFyIGl0ZW1Hcm91cCA9IG5ldyBHcm91cCgpO1xuICAgIHZhciB0ZXh0U3R5bGVNb2RlbCA9IGl0ZW1Nb2RlbC5nZXRNb2RlbCgndGV4dFN0eWxlJyk7XG4gICAgdmFyIGl0ZW1JY29uID0gaXRlbU1vZGVsLmdldCgnaWNvbicpO1xuICAgIHZhciB0b29sdGlwTW9kZWwgPSBpdGVtTW9kZWwuZ2V0TW9kZWwoJ3Rvb2x0aXAnKTtcbiAgICB2YXIgbGVnZW5kR2xvYmFsVG9vbHRpcE1vZGVsID0gdG9vbHRpcE1vZGVsLnBhcmVudE1vZGVsOyAvLyBVc2UgdXNlciBnaXZlbiBpY29uIGZpcnN0XG5cbiAgICBsZWdlbmRTeW1ib2xUeXBlID0gaXRlbUljb24gfHwgbGVnZW5kU3ltYm9sVHlwZTtcbiAgICBpdGVtR3JvdXAuYWRkKGNyZWF0ZVN5bWJvbChsZWdlbmRTeW1ib2xUeXBlLCAwLCAwLCBpdGVtV2lkdGgsIGl0ZW1IZWlnaHQsIGlzU2VsZWN0ZWQgPyBjb2xvciA6IGluYWN0aXZlQ29sb3IsIC8vIHN5bWJvbEtlZXBBc3BlY3QgZGVmYXVsdCB0cnVlIGZvciBsZWdlbmRcbiAgICBzeW1ib2xLZWVwQXNwZWN0ID09IG51bGwgPyB0cnVlIDogc3ltYm9sS2VlcEFzcGVjdCkpOyAvLyBDb21wb3NlIHN5bWJvbHNcbiAgICAvLyBQRU5ESU5HXG5cbiAgICBpZiAoIWl0ZW1JY29uICYmIHN5bWJvbFR5cGUgLy8gQXQgbGVhc3Qgc2hvdyBvbmUgc3ltYm9sLCBjYW4ndCBiZSBhbGwgbm9uZVxuICAgICYmIChzeW1ib2xUeXBlICE9PSBsZWdlbmRTeW1ib2xUeXBlIHx8IHN5bWJvbFR5cGUgPT09ICdub25lJykpIHtcbiAgICAgIHZhciBzaXplID0gaXRlbUhlaWdodCAqIDAuODtcblxuICAgICAgaWYgKHN5bWJvbFR5cGUgPT09ICdub25lJykge1xuICAgICAgICBzeW1ib2xUeXBlID0gJ2NpcmNsZSc7XG4gICAgICB9IC8vIFB1dCBzeW1ib2wgaW4gdGhlIGNlbnRlclxuXG5cbiAgICAgIGl0ZW1Hcm91cC5hZGQoY3JlYXRlU3ltYm9sKHN5bWJvbFR5cGUsIChpdGVtV2lkdGggLSBzaXplKSAvIDIsIChpdGVtSGVpZ2h0IC0gc2l6ZSkgLyAyLCBzaXplLCBzaXplLCBpc1NlbGVjdGVkID8gY29sb3IgOiBpbmFjdGl2ZUNvbG9yLCAvLyBzeW1ib2xLZWVwQXNwZWN0IGRlZmF1bHQgdHJ1ZSBmb3IgbGVnZW5kXG4gICAgICBzeW1ib2xLZWVwQXNwZWN0ID09IG51bGwgPyB0cnVlIDogc3ltYm9sS2VlcEFzcGVjdCkpO1xuICAgIH1cblxuICAgIHZhciB0ZXh0WCA9IGl0ZW1BbGlnbiA9PT0gJ2xlZnQnID8gaXRlbVdpZHRoICsgNSA6IC01O1xuICAgIHZhciB0ZXh0QWxpZ24gPSBpdGVtQWxpZ247XG4gICAgdmFyIGZvcm1hdHRlciA9IGxlZ2VuZE1vZGVsLmdldCgnZm9ybWF0dGVyJyk7XG4gICAgdmFyIGNvbnRlbnQgPSBuYW1lO1xuXG4gICAgaWYgKHR5cGVvZiBmb3JtYXR0ZXIgPT09ICdzdHJpbmcnICYmIGZvcm1hdHRlcikge1xuICAgICAgY29udGVudCA9IGZvcm1hdHRlci5yZXBsYWNlKCd7bmFtZX0nLCBuYW1lICE9IG51bGwgPyBuYW1lIDogJycpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGZvcm1hdHRlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgY29udGVudCA9IGZvcm1hdHRlcihuYW1lKTtcbiAgICB9XG5cbiAgICBpdGVtR3JvdXAuYWRkKG5ldyBncmFwaGljLlRleHQoe1xuICAgICAgc3R5bGU6IGdyYXBoaWMuc2V0VGV4dFN0eWxlKHt9LCB0ZXh0U3R5bGVNb2RlbCwge1xuICAgICAgICB0ZXh0OiBjb250ZW50LFxuICAgICAgICB4OiB0ZXh0WCxcbiAgICAgICAgeTogaXRlbUhlaWdodCAvIDIsXG4gICAgICAgIHRleHRGaWxsOiBpc1NlbGVjdGVkID8gdGV4dFN0eWxlTW9kZWwuZ2V0VGV4dENvbG9yKCkgOiBpbmFjdGl2ZUNvbG9yLFxuICAgICAgICB0ZXh0QWxpZ246IHRleHRBbGlnbixcbiAgICAgICAgdGV4dFZlcnRpY2FsQWxpZ246ICdtaWRkbGUnXG4gICAgICB9KVxuICAgIH0pKTsgLy8gQWRkIGEgaW52aXNpYmxlIHJlY3QgdG8gaW5jcmVhc2UgdGhlIGFyZWEgb2YgbW91c2UgaG92ZXJcblxuICAgIHZhciBoaXRSZWN0ID0gbmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgICBzaGFwZTogaXRlbUdyb3VwLmdldEJvdW5kaW5nUmVjdCgpLFxuICAgICAgaW52aXNpYmxlOiB0cnVlLFxuICAgICAgdG9vbHRpcDogdG9vbHRpcE1vZGVsLmdldCgnc2hvdycpID8genJVdGlsLmV4dGVuZCh7XG4gICAgICAgIGNvbnRlbnQ6IG5hbWUsXG4gICAgICAgIC8vIERlZmF1bCBmb3JtYXR0ZXJcbiAgICAgICAgZm9ybWF0dGVyOiBsZWdlbmRHbG9iYWxUb29sdGlwTW9kZWwuZ2V0KCdmb3JtYXR0ZXInLCB0cnVlKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIG5hbWU7XG4gICAgICAgIH0sXG4gICAgICAgIGZvcm1hdHRlclBhcmFtczoge1xuICAgICAgICAgIGNvbXBvbmVudFR5cGU6ICdsZWdlbmQnLFxuICAgICAgICAgIGxlZ2VuZEluZGV4OiBsZWdlbmRNb2RlbC5jb21wb25lbnRJbmRleCxcbiAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgICR2YXJzOiBbJ25hbWUnXVxuICAgICAgICB9XG4gICAgICB9LCB0b29sdGlwTW9kZWwub3B0aW9uKSA6IG51bGxcbiAgICB9KTtcbiAgICBpdGVtR3JvdXAuYWRkKGhpdFJlY3QpO1xuICAgIGl0ZW1Hcm91cC5lYWNoQ2hpbGQoZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICBjaGlsZC5zaWxlbnQgPSB0cnVlO1xuICAgIH0pO1xuICAgIGhpdFJlY3Quc2lsZW50ID0gIXNlbGVjdE1vZGU7XG4gICAgdGhpcy5nZXRDb250ZW50R3JvdXAoKS5hZGQoaXRlbUdyb3VwKTtcbiAgICBncmFwaGljLnNldEhvdmVyU3R5bGUoaXRlbUdyb3VwKTtcbiAgICBpdGVtR3JvdXAuX19sZWdlbmREYXRhSW5kZXggPSBkYXRhSW5kZXg7XG4gICAgcmV0dXJuIGl0ZW1Hcm91cDtcbiAgfSxcblxuICAvKipcbiAgICogQHByb3RlY3RlZFxuICAgKi9cbiAgbGF5b3V0SW5uZXI6IGZ1bmN0aW9uIChsZWdlbmRNb2RlbCwgaXRlbUFsaWduLCBtYXhTaXplKSB7XG4gICAgdmFyIGNvbnRlbnRHcm91cCA9IHRoaXMuZ2V0Q29udGVudEdyb3VwKCk7IC8vIFBsYWNlIGl0ZW1zIGluIGNvbnRlbnRHcm91cC5cblxuICAgIGxheW91dFV0aWwuYm94KGxlZ2VuZE1vZGVsLmdldCgnb3JpZW50JyksIGNvbnRlbnRHcm91cCwgbGVnZW5kTW9kZWwuZ2V0KCdpdGVtR2FwJyksIG1heFNpemUud2lkdGgsIG1heFNpemUuaGVpZ2h0KTtcbiAgICB2YXIgY29udGVudFJlY3QgPSBjb250ZW50R3JvdXAuZ2V0Qm91bmRpbmdSZWN0KCk7XG4gICAgY29udGVudEdyb3VwLmF0dHIoJ3Bvc2l0aW9uJywgWy1jb250ZW50UmVjdC54LCAtY29udGVudFJlY3QueV0pO1xuICAgIHJldHVybiB0aGlzLmdyb3VwLmdldEJvdW5kaW5nUmVjdCgpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICByZW1vdmU6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmdldENvbnRlbnRHcm91cCgpLnJlbW92ZUFsbCgpO1xuICAgIHRoaXMuX2lzRmlyc3RSZW5kZXIgPSB0cnVlO1xuICB9XG59KTtcblxuZnVuY3Rpb24gZGlzcGF0Y2hTZWxlY3RBY3Rpb24obmFtZSwgYXBpKSB7XG4gIGFwaS5kaXNwYXRjaEFjdGlvbih7XG4gICAgdHlwZTogJ2xlZ2VuZFRvZ2dsZVNlbGVjdCcsXG4gICAgbmFtZTogbmFtZVxuICB9KTtcbn1cblxuZnVuY3Rpb24gZGlzcGF0Y2hIaWdobGlnaHRBY3Rpb24oc2VyaWVzTmFtZSwgZGF0YU5hbWUsIGFwaSwgZXhjbHVkZVNlcmllc0lkKSB7XG4gIC8vIElmIGVsZW1lbnQgaG92ZXIgd2lsbCBtb3ZlIHRvIGEgaG92ZXJMYXllci5cbiAgdmFyIGVsID0gYXBpLmdldFpyKCkuc3RvcmFnZS5nZXREaXNwbGF5TGlzdCgpWzBdO1xuXG4gIGlmICghKGVsICYmIGVsLnVzZUhvdmVyTGF5ZXIpKSB7XG4gICAgYXBpLmRpc3BhdGNoQWN0aW9uKHtcbiAgICAgIHR5cGU6ICdoaWdobGlnaHQnLFxuICAgICAgc2VyaWVzTmFtZTogc2VyaWVzTmFtZSxcbiAgICAgIG5hbWU6IGRhdGFOYW1lLFxuICAgICAgZXhjbHVkZVNlcmllc0lkOiBleGNsdWRlU2VyaWVzSWRcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBkaXNwYXRjaERvd25wbGF5QWN0aW9uKHNlcmllc05hbWUsIGRhdGFOYW1lLCBhcGksIGV4Y2x1ZGVTZXJpZXNJZCkge1xuICAvLyBJZiBlbGVtZW50IGhvdmVyIHdpbGwgbW92ZSB0byBhIGhvdmVyTGF5ZXIuXG4gIHZhciBlbCA9IGFwaS5nZXRacigpLnN0b3JhZ2UuZ2V0RGlzcGxheUxpc3QoKVswXTtcblxuICBpZiAoIShlbCAmJiBlbC51c2VIb3ZlckxheWVyKSkge1xuICAgIGFwaS5kaXNwYXRjaEFjdGlvbih7XG4gICAgICB0eXBlOiAnZG93bnBsYXknLFxuICAgICAgc2VyaWVzTmFtZTogc2VyaWVzTmFtZSxcbiAgICAgIG5hbWU6IGRhdGFOYW1lLFxuICAgICAgZXhjbHVkZVNlcmllc0lkOiBleGNsdWRlU2VyaWVzSWRcbiAgICB9KTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFEQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTkE7QUFIQTtBQWlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQWxRQTtBQUNBO0FBb1FBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/component/legend/LegendView.js
