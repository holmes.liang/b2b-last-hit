__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _util_interopDefault__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_util/interopDefault */ "./node_modules/antd/es/_util/interopDefault.js");
/* harmony import */ var _Statistic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Statistic */ "./node_modules/antd/es/statistic/Statistic.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./node_modules/antd/es/statistic/utils.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var REFRESH_INTERVAL = 1000 / 30;

function getTime(value) {
  return Object(_util_interopDefault__WEBPACK_IMPORTED_MODULE_3__["default"])(moment__WEBPACK_IMPORTED_MODULE_2__)(value).valueOf();
}

var Countdown =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Countdown, _React$Component);

  function Countdown() {
    var _this;

    _classCallCheck(this, Countdown);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Countdown).apply(this, arguments));

    _this.syncTimer = function () {
      var value = _this.props.value;
      var timestamp = getTime(value);

      if (timestamp >= Date.now()) {
        _this.startTimer();
      } else {
        _this.stopTimer();
      }
    };

    _this.startTimer = function () {
      if (_this.countdownId) return;
      _this.countdownId = window.setInterval(function () {
        _this.forceUpdate();
      }, REFRESH_INTERVAL);
    };

    _this.stopTimer = function () {
      var _this$props = _this.props,
          onFinish = _this$props.onFinish,
          value = _this$props.value;

      if (_this.countdownId) {
        clearInterval(_this.countdownId);
        _this.countdownId = undefined;
        var timestamp = getTime(value);

        if (onFinish && timestamp < Date.now()) {
          onFinish();
        }
      }
    };

    _this.formatCountdown = function (value, config) {
      var format = _this.props.format;
      return Object(_utils__WEBPACK_IMPORTED_MODULE_5__["formatCountdown"])(value, _extends(_extends({}, config), {
        format: format
      }));
    }; // Countdown do not need display the timestamp


    _this.valueRender = function (node) {
      return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](node, {
        title: undefined
      });
    };

    return _this;
  }

  _createClass(Countdown, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.syncTimer();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.syncTimer();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.stopTimer();
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Statistic__WEBPACK_IMPORTED_MODULE_4__["default"], _extends({
        valueRender: this.valueRender
      }, this.props, {
        formatter: this.formatCountdown
      }));
    }
  }]);

  return Countdown;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Countdown.defaultProps = {
  format: 'HH:mm:ss'
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_1__["polyfill"])(Countdown);
/* harmony default export */ __webpack_exports__["default"] = (Countdown);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9zdGF0aXN0aWMvQ291bnRkb3duLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zdGF0aXN0aWMvQ291bnRkb3duLmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xuaW1wb3J0IGludGVyb3BEZWZhdWx0IGZyb20gJy4uL191dGlsL2ludGVyb3BEZWZhdWx0JztcbmltcG9ydCBTdGF0aXN0aWMgZnJvbSAnLi9TdGF0aXN0aWMnO1xuaW1wb3J0IHsgZm9ybWF0Q291bnRkb3duIH0gZnJvbSAnLi91dGlscyc7XG5jb25zdCBSRUZSRVNIX0lOVEVSVkFMID0gMTAwMCAvIDMwO1xuZnVuY3Rpb24gZ2V0VGltZSh2YWx1ZSkge1xuICAgIHJldHVybiBpbnRlcm9wRGVmYXVsdChtb21lbnQpKHZhbHVlKS52YWx1ZU9mKCk7XG59XG5jbGFzcyBDb3VudGRvd24gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlciguLi5hcmd1bWVudHMpO1xuICAgICAgICB0aGlzLnN5bmNUaW1lciA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgdmFsdWUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCB0aW1lc3RhbXAgPSBnZXRUaW1lKHZhbHVlKTtcbiAgICAgICAgICAgIGlmICh0aW1lc3RhbXAgPj0gRGF0ZS5ub3coKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3RhcnRUaW1lcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdG9wVGltZXIoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zdGFydFRpbWVyID0gKCkgPT4ge1xuICAgICAgICAgICAgaWYgKHRoaXMuY291bnRkb3duSWQpXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgdGhpcy5jb3VudGRvd25JZCA9IHdpbmRvdy5zZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JjZVVwZGF0ZSgpO1xuICAgICAgICAgICAgfSwgUkVGUkVTSF9JTlRFUlZBTCk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RvcFRpbWVyID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvbkZpbmlzaCwgdmFsdWUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAodGhpcy5jb3VudGRvd25JZCkge1xuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5jb3VudGRvd25JZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5jb3VudGRvd25JZCA9IHVuZGVmaW5lZDtcbiAgICAgICAgICAgICAgICBjb25zdCB0aW1lc3RhbXAgPSBnZXRUaW1lKHZhbHVlKTtcbiAgICAgICAgICAgICAgICBpZiAob25GaW5pc2ggJiYgdGltZXN0YW1wIDwgRGF0ZS5ub3coKSkge1xuICAgICAgICAgICAgICAgICAgICBvbkZpbmlzaCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5mb3JtYXRDb3VudGRvd24gPSAodmFsdWUsIGNvbmZpZykgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBmb3JtYXQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICByZXR1cm4gZm9ybWF0Q291bnRkb3duKHZhbHVlLCBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIGNvbmZpZyksIHsgZm9ybWF0IH0pKTtcbiAgICAgICAgfTtcbiAgICAgICAgLy8gQ291bnRkb3duIGRvIG5vdCBuZWVkIGRpc3BsYXkgdGhlIHRpbWVzdGFtcFxuICAgICAgICB0aGlzLnZhbHVlUmVuZGVyID0gKG5vZGUpID0+IFJlYWN0LmNsb25lRWxlbWVudChub2RlLCB7XG4gICAgICAgICAgICB0aXRsZTogdW5kZWZpbmVkLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAgIHRoaXMuc3luY1RpbWVyKCk7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgICAgdGhpcy5zeW5jVGltZXIoKTtcbiAgICB9XG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICAgIHRoaXMuc3RvcFRpbWVyKCk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuICg8U3RhdGlzdGljIHZhbHVlUmVuZGVyPXt0aGlzLnZhbHVlUmVuZGVyfSB7Li4udGhpcy5wcm9wc30gZm9ybWF0dGVyPXt0aGlzLmZvcm1hdENvdW50ZG93bn0vPik7XG4gICAgfVxufVxuQ291bnRkb3duLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBmb3JtYXQ6ICdISDptbTpzcycsXG59O1xucG9seWZpbGwoQ291bnRkb3duKTtcbmV4cG9ydCBkZWZhdWx0IENvdW50ZG93bjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTs7Ozs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQVJBO0FBQ0E7QUFTQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBSEE7QUFDQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQWhDQTtBQUNBO0FBQ0E7QUFpQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBcENBO0FBc0NBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7O0FBbkRBO0FBQ0E7QUFvREE7QUFDQTtBQURBO0FBR0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/statistic/Countdown.js
