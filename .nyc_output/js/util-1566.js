var PI2 = Math.PI * 2;

function normalizeRadian(angle) {
  angle %= PI2;

  if (angle < 0) {
    angle += PI2;
  }

  return angle;
}

exports.normalizeRadian = normalizeRadian;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi91dGlsLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvY29udGFpbi91dGlsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQSTIgPSBNYXRoLlBJICogMjtcblxuZnVuY3Rpb24gbm9ybWFsaXplUmFkaWFuKGFuZ2xlKSB7XG4gIGFuZ2xlICU9IFBJMjtcblxuICBpZiAoYW5nbGUgPCAwKSB7XG4gICAgYW5nbGUgKz0gUEkyO1xuICB9XG5cbiAgcmV0dXJuIGFuZ2xlO1xufVxuXG5leHBvcnRzLm5vcm1hbGl6ZVJhZGlhbiA9IG5vcm1hbGl6ZVJhZGlhbjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/contain/util.js
