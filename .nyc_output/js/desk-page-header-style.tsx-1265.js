__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");


function _templateObject9() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        .top {\n            background-color: ", "\n            padding: 0 20px;\n            min-height: 64px;\n            line-height: 64px;\n            display: flex;\n            justify-content: space-between;\n            align-items: center;\n            color: #fff;\n            i {\n                font-size: 23px;\n                vertical-align: middle;\n                color: #fff;\n            }\n        }\n        .ant-menu-inline .ant-menu-item {\n           margin-top: 0;\n           padding-top: 0;\n        }\n        .ant-menu-sub.ant-menu-inline > .ant-menu-item {\n            padding-left: 28px !important;\n        }\n        .top-menu-icon {\n            line-height: 22px;\n            color: rgba(0, 0, 0, 0.85);\n            cursor: pointer;\n            position: relative;\n            -webkit-transition: all .3s;\n            transition: all .3s;\n            padding: 8px 0 !important;\n            text-align: center;\n            i {\n                font-size: 24px;\n                color: #fff;\n            }\n            &.back {\n                background-color: #3a3a3a;\n            }\n            &.blue {\n                background: #29a9e6;\n            }\n        }\n        .ant-menu-dark {\n            background: #3a3a3a;\n            color: #878787;\n            .ant-menu-sub {\n              background: #3f3f3f;\n            }\n            .ant-menu-item-selected {\n                color: ", ";\n                background-color: inherit;\n            }\n            .ant-menu-inline.ant-menu-sub {\n                box-shadow: none;\n            }\n        }\n        .ant-menu-dark .ant-menu-item-selected, .ant-menu-dark .ant-menu-sub .ant-menu-item-selected {\n            background-color: transparent;\n        }\n        .top-menu-icon.blue {\n            background: #373636;\n        }\n    "]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        position: relative;\n        height: 70px;\n        background-color: ", ";\n        font-size: 15px;\n        font-weight: 600;\n        .ant-menu-dark {\n            line-height: 70px;\n            background-color: ", ";\n            padding-left: 96px;\n        }\n        .ant-menu.ant-menu-dark .ant-menu-item-selected, .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected{\n            background-color: ", "\n        }\n        .sub-menu {\n            .ant-menu-item {\n                padding: 0 10px;\n                text-align: center;\n                .anticon {\n                    min-width: 0;\n                }\n            }\n        }\n    "]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        display: inline-block;\n        flex: auto;\n        margin-left: 35px;\n        .ant-menu-dark {\n            margin-top: 3px;\n        }\n        .ant-menu-dark,.ant-menu-dark.ant-menu-horizontal > .ant-menu-submenu {   \n            background: #323232;\n        }\n        .ant-menu-item {\n            line-height: 56px;\n            height: 56px;\n            padding: 0 15px;\n            border-color: #73d13d;\n            border-bottom: 0;\n            top: 0;\n            margin-top: 0;\n            font-size: 16px;\n            font-weight: 600;\n            color: #fff;\n            .anticon {\n                font-size: 20px\n            }\n        }\n        i {\n          font-size: 20px;\n          margin-right: 10px;\n        }\n    "]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        vertical-align: middle;\n        width: 30px;\n        height: 30px;\n        border-radius: 15px;\n        margin-right: 5px;\n        display: inline-flex;\n        -ms-flex-align: center;\n        align-items: center;\n        -ms-flex-pack: center;\n        justify-content: center;\n        background-size: cover;\n        font-size: 14px;\n        margin: 0 10px;\n    "]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        cursor: pointer;\n        display: inline-block;\n        transition: all .3s;\n        height: 100%;\n        i {\n          font-size: 23px;\n          vertical-align: middle;\n          color: #707070;\n    \n          &:hover {\n            color: #fff !important;\n          }\n        }\n        .avatar {\n          margin: 20px 8px 20px 0;\n          color: #1890ff;\n          vertical-align: middle;\n        }\n    \n        i {\n          font-size: 14px !important;\n          margin-left: .3em;\n          vertical-align: -3px;\n          color: fade(#fff, 85%);\n          transition: transform ease-in-out .2s;\n    \n          &[data-open=true] {\n            transform: rotate(-180deg);\n          }\n        }\n    "]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        width: 80px;\n        height: 64px;\n        display: inline-block;\n        cursor: pointer;\n        vertical-align: bottom;\n        img {\n            height: 60px;\n            vertical-align: middle;\n        }\n    "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        font-size: 16px;\n        float: right;\n        height: 100%;\n        color: #fff;\n        display: flex;\n        justify-content: center;\n        align-items: center;\n        .ant-dropdown-trigger {\n             display: flex;\n            justify-content: center;\n            align-items: center;\n        }\n        .ant-menu {\n            &.avatar-dropdown-menu {\n                background: #323232;\n            \n                .ant-dropdown-menu-item {\n                  &:hover {\n                    background-color: @primary-color;\n                    color: white;\n                  }\n                }\n            \n                .ant-dropdown-menu-item-divider {\n                  opacity: 0;\n                }\n            }\n        }\n        .ant-badge {\n            margin: 0 12px;\n            .anticon-bell {\n                font-size: 23px;\n                vertical-align: middle;\n                color: #707070;\n                cursor: pointer;\n                &:hover {\n                    color: #fff;\n                }\n            }\n        }\n        \n        \n    "]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        margin: 0 auto;\n         .ant-menu-horizontal {\n          white-space: inherit;\n        }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      height: 64px;\n      padding-top: 5px;\n      line-height: 54px;\n      background: #323232;\n      // box-shadow: 0 1px 4px rgba(0, 21, 41, .08);\n      position: relative;\n      .ant-menu.ant-menu-dark .ant-menu-item-selected, .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected{\n         background-color: ", ";\n      }\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_2__["default"].getTheme(),
    HEADER_COLOR = _Theme$getTheme.HEADER_COLOR,
    COLOR_PRIMARY = _Theme$getTheme.COLOR_PRIMARY;

/* harmony default export */ __webpack_exports__["default"] = ({
  Header: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject(), HEADER_COLOR),
  Container: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject2()),
  Right: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject3()),
  Logo: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].a(_templateObject4()),
  Action: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].span(_templateObject5()),
  Avatar: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject6()),
  Slider: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject7()),
  SubMenu: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject8(), HEADER_COLOR, HEADER_COLOR, HEADER_COLOR),
  //mobile
  MobileHeader: _common_3rd__WEBPACK_IMPORTED_MODULE_1__["Styled"].div(_templateObject9(), HEADER_COLOR, COLOR_PRIMARY)
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3BhZ2UvZGVzay1wYWdlLWhlYWRlci1zdHlsZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jb21wb25lbnQvcGFnZS9kZXNrLXBhZ2UtaGVhZGVyLXN0eWxlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCBUaGVtZSBmcm9tIFwiQHN0eWxlcy9pbmRleFwiO1xuXG5jb25zdCB7IEhFQURFUl9DT0xPUiwgQ09MT1JfUFJJTUFSWSB9ID0gVGhlbWUuZ2V0VGhlbWUoKTtcbmV4cG9ydCBkZWZhdWx0IHtcbiAgSGVhZGVyOiBTdHlsZWQuZGl2YFxuICAgICAgaGVpZ2h0OiA2NHB4O1xuICAgICAgcGFkZGluZy10b3A6IDVweDtcbiAgICAgIGxpbmUtaGVpZ2h0OiA1NHB4O1xuICAgICAgYmFja2dyb3VuZDogIzMyMzIzMjtcbiAgICAgIC8vIGJveC1zaGFkb3c6IDAgMXB4IDRweCByZ2JhKDAsIDIxLCA0MSwgLjA4KTtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIC5hbnQtbWVudS5hbnQtbWVudS1kYXJrIC5hbnQtbWVudS1pdGVtLXNlbGVjdGVkLCAuYW50LW1lbnUtc3VibWVudS1wb3B1cC5hbnQtbWVudS1kYXJrIC5hbnQtbWVudS1pdGVtLXNlbGVjdGVke1xuICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHtIRUFERVJfQ09MT1J9O1xuICAgICAgfVxuICAgIGAsXG4gIENvbnRhaW5lcjogU3R5bGVkLmRpdmBcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgICAuYW50LW1lbnUtaG9yaXpvbnRhbCB7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IGluaGVyaXQ7XG4gICAgICAgIH1cbiAgICBgLFxuICBSaWdodDogU3R5bGVkLmRpdmBcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICAuYW50LWRyb3Bkb3duLXRyaWdnZXIge1xuICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICAgICAgLmFudC1tZW51IHtcbiAgICAgICAgICAgICYuYXZhdGFyLWRyb3Bkb3duLW1lbnUge1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzMjMyMzI7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAuYW50LWRyb3Bkb3duLW1lbnUtaXRlbSB7XG4gICAgICAgICAgICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogQHByaW1hcnktY29sb3I7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAuYW50LWRyb3Bkb3duLW1lbnUtaXRlbS1kaXZpZGVyIHtcbiAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDA7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5hbnQtYmFkZ2Uge1xuICAgICAgICAgICAgbWFyZ2luOiAwIDEycHg7XG4gICAgICAgICAgICAuYW50aWNvbi1iZWxsIHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIzcHg7XG4gICAgICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICAgICAgJjpob3ZlciB7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgXG4gICAgYCxcbiAgTG9nbzogU3R5bGVkLmFgXG4gICAgICAgIHdpZHRoOiA4MHB4O1xuICAgICAgICBoZWlnaHQ6IDY0cHg7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO1xuICAgICAgICBpbWcge1xuICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgfVxuICAgIGAsXG4gIEFjdGlvbjogU3R5bGVkLnNwYW5gXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjNzO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIGkge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgIFxuICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmF2YXRhciB7XG4gICAgICAgICAgbWFyZ2luOiAyMHB4IDhweCAyMHB4IDA7XG4gICAgICAgICAgY29sb3I6ICMxODkwZmY7XG4gICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgfVxuICAgIFxuICAgICAgICBpIHtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogLjNlbTtcbiAgICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogLTNweDtcbiAgICAgICAgICBjb2xvcjogZmFkZSgjZmZmLCA4NSUpO1xuICAgICAgICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSBlYXNlLWluLW91dCAuMnM7XG4gICAgXG4gICAgICAgICAgJltkYXRhLW9wZW49dHJ1ZV0ge1xuICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLTE4MGRlZyk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgYCxcbiAgQXZhdGFyOiBTdHlsZWQuZGl2YFxuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgICAgIC1tcy1mbGV4LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgbWFyZ2luOiAwIDEwcHg7XG4gICAgYCxcbiAgU2xpZGVyOiBTdHlsZWQuZGl2YFxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIGZsZXg6IGF1dG87XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzNXB4O1xuICAgICAgICAuYW50LW1lbnUtZGFyayB7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAzcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmFudC1tZW51LWRhcmssLmFudC1tZW51LWRhcmsuYW50LW1lbnUtaG9yaXpvbnRhbCA+IC5hbnQtbWVudS1zdWJtZW51IHsgICBcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzMjMyMzI7XG4gICAgICAgIH1cbiAgICAgICAgLmFudC1tZW51LWl0ZW0ge1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDU2cHg7XG4gICAgICAgICAgICBoZWlnaHQ6IDU2cHg7XG4gICAgICAgICAgICBwYWRkaW5nOiAwIDE1cHg7XG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICM3M2QxM2Q7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAwO1xuICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICAgIC5hbnRpY29uIHtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICB9XG4gICAgYCxcbiAgU3ViTWVudTogU3R5bGVkLmRpdmBcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgICBoZWlnaHQ6IDcwcHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICR7SEVBREVSX0NPTE9SfTtcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAuYW50LW1lbnUtZGFyayB7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogNzBweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICR7SEVBREVSX0NPTE9SfTtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogOTZweDtcbiAgICAgICAgfVxuICAgICAgICAuYW50LW1lbnUuYW50LW1lbnUtZGFyayAuYW50LW1lbnUtaXRlbS1zZWxlY3RlZCwgLmFudC1tZW51LXN1Ym1lbnUtcG9wdXAuYW50LW1lbnUtZGFyayAuYW50LW1lbnUtaXRlbS1zZWxlY3RlZHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICR7SEVBREVSX0NPTE9SfVxuICAgICAgICB9XG4gICAgICAgIC5zdWItbWVudSB7XG4gICAgICAgICAgICAuYW50LW1lbnUtaXRlbSB7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMCAxMHB4O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAuYW50aWNvbiB7XG4gICAgICAgICAgICAgICAgICAgIG1pbi13aWR0aDogMDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICBgLFxuICAvL21vYmlsZVxuICBNb2JpbGVIZWFkZXI6IFN0eWxlZC5kaXZgXG4gICAgICAgIC50b3Age1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHtIRUFERVJfQ09MT1J9XG4gICAgICAgICAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgICAgICAgICBtaW4taGVpZ2h0OiA2NHB4O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDY0cHg7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgaSB7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyM3B4O1xuICAgICAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLmFudC1tZW51LWlubGluZSAuYW50LW1lbnUtaXRlbSB7XG4gICAgICAgICAgIG1hcmdpbi10b3A6IDA7XG4gICAgICAgICAgIHBhZGRpbmctdG9wOiAwO1xuICAgICAgICB9XG4gICAgICAgIC5hbnQtbWVudS1zdWIuYW50LW1lbnUtaW5saW5lID4gLmFudC1tZW51LWl0ZW0ge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiAyOHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICAgICAgLnRvcC1tZW51LWljb24ge1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgICAgICAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg1KTtcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4zcztcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XG4gICAgICAgICAgICBwYWRkaW5nOiA4cHggMCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgaSB7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJi5iYWNrIHtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2EzYTNhO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgJi5ibHVlIHtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMjlhOWU2O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5hbnQtbWVudS1kYXJrIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzYTNhM2E7XG4gICAgICAgICAgICBjb2xvcjogIzg3ODc4NztcbiAgICAgICAgICAgIC5hbnQtbWVudS1zdWIge1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjM2YzZjNmO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmFudC1tZW51LWl0ZW0tc2VsZWN0ZWQge1xuICAgICAgICAgICAgICAgIGNvbG9yOiAke0NPTE9SX1BSSU1BUll9O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IGluaGVyaXQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuYW50LW1lbnUtaW5saW5lLmFudC1tZW51LXN1YiB7XG4gICAgICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuYW50LW1lbnUtZGFyayAuYW50LW1lbnUtaXRlbS1zZWxlY3RlZCwgLmFudC1tZW51LWRhcmsgLmFudC1tZW51LXN1YiAuYW50LW1lbnUtaXRlbS1zZWxlY3RlZCB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgICAgfVxuICAgICAgICAudG9wLW1lbnUtaWNvbi5ibHVlIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICMzNzM2MzY7XG4gICAgICAgIH1cbiAgICBgLFxufTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBV0E7QUFNQTtBQTRDQTtBQVdBO0FBZ0NBO0FBZUE7QUE4QkE7QUF3QkE7QUFDQTtBQS9LQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/page/desk-page-header-style.tsx
