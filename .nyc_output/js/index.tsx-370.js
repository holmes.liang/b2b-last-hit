__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _desk_component_address_spgAddress__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk/component/address/spgAddress */ "./src/app/desk/component/address/spgAddress.tsx");
/* harmony import */ var _desk_component_address_thai_address__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @desk/component/address/thai-address */ "./src/app/desk/component/address/thai-address.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/address/index.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}








var generatePropsName = function generatePropsName(propName, addressFix, dataId) {
  if (!!dataId) return "".concat(dataId, ".").concat(addressFix, ".").concat(propName);
  return "".concat(addressFix, ".").concat(propName);
};

var Address =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(Address, _ModelWidget);

  function Address(props, state) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Address);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Address).call(this, props, state));
    _this.idNoPage = void 0;

    _this.initIdNoRef = function (ref) {
      _this.idNoPage = ref;
    };

    _this.changeCountry = function (countryCode) {
      _this.setState({
        countryCode: countryCode
      });
    };

    _this.generatePropsName = function (propName, dataId) {
      var _this$props = _this.props,
          addressFix = _this$props.addressFix,
          isNotAddressPresix = _this$props.isNotAddressPresix;
      var initAddressFix = addressFix || "address";
      if (!!dataId) return isNotAddressPresix || propName === initAddressFix ? "".concat(dataId, ".").concat(propName) : "".concat(dataId, ".").concat(initAddressFix, ".").concat(propName);
      return isNotAddressPresix || propName === initAddressFix ? propName : "".concat(initAddressFix, ".").concat(propName);
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Address, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "initState",
    value: function initState() {
      var _this$props2 = this.props,
          dataIdOrPrefix = _this$props2.dataIdOrPrefix,
          addressFix = _this$props2.addressFix;
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Address.prototype), "initState", this).call(this), {
        countryCode: this.props.defaultCountryCode || this.getValueFromModel(generatePropsName("countryCode", addressFix || "address", dataIdOrPrefix))
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var C = this.getComponents();

      var _this$props3 = this.props,
          model = _this$props3.model,
          form = _this$props3.form,
          defaultCountryCode = _this$props3.defaultCountryCode,
          dataIdOrPrefix = _this$props3.dataIdOrPrefix,
          layoutCol = _this$props3.layoutCol,
          notAddressType = _this$props3.notAddressType,
          _this$props3$require = _this$props3.require,
          require = _this$props3$require === void 0 ? true : _this$props3$require,
          isNotAddressPresix = _this$props3.isNotAddressPresix,
          addressFix = _this$props3.addressFix;

      var initAddressFix = addressFix || "address";
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, !defaultCountryCode && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NSelect"], {
        label: "Country",
        size: "large",
        form: form,
        required: require,
        tableName: "nationality",
        dataFixed: "policy",
        model: model,
        style: {
          width: "100%"
        },
        layoutCol: layoutCol,
        propName: generatePropsName("countryCode", initAddressFix, dataIdOrPrefix),
        onChange: function onChange(value) {
          _this2.setState({
            countryCode: value
          });

          _this2.setValuesToModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this2.generatePropsName(initAddressFix, dataIdOrPrefix), {
            countryCode: value
          }));

          _this2.props.form.resetFields([_this2.generatePropsName(initAddressFix, dataIdOrPrefix)]);

          _this2.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this2.generatePropsName(initAddressFix, dataIdOrPrefix), {
            countryCode: value
          }));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }), this.state.countryCode === "SGP" && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_address_spgAddress__WEBPACK_IMPORTED_MODULE_11__["default"], {
        model: model,
        form: form,
        ref: this.initIdNoRef,
        dataId: dataIdOrPrefix,
        addressFix: initAddressFix,
        selectXsSm: layoutCol && layoutCol.labelCol,
        textXsSm: layoutCol && layoutCol.wrapperCol,
        required: require,
        isNotAddressPresix: isNotAddressPresix,
        propLabel: this.props.label,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }), this.state.countryCode === "THA" && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_address_thai_address__WEBPACK_IMPORTED_MODULE_12__["default"], {
        model: model,
        form: form,
        addressFix: initAddressFix,
        prefix: dataIdOrPrefix,
        countryCode: "THA",
        notAddressType: notAddressType,
        layoutCol: layoutCol,
        required: require,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }), this.state.countryCode !== "SGP" && this.state.countryCode !== "THA" && !!this.state.countryCode && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], {
        form: form,
        model: model,
        layoutCol: layoutCol,
        label: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Address").thai("Address").getMessage(),
        propName: this.generatePropsName("street", dataIdOrPrefix),
        size: "large",
        style: {
          width: "100%"
        },
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_10__["NText"], {
        form: form,
        model: model,
        layoutCol: layoutCol,
        label: _common__WEBPACK_IMPORTED_MODULE_13__["Language"].en("Postal Code").thai("Postal Code").getMessage(),
        propName: this.generatePropsName("postalCode", dataIdOrPrefix),
        size: "large",
        style: {
          width: "100%"
        },
        required: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      })));
    }
  }]);

  return Address;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (Address);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3MvaW5kZXgudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2FkZHJlc3MvaW5kZXgudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFN0eWxlZERJViwgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBOU2VsZWN0LCBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IFNwZ0FkZHJlc3MgZnJvbSBcIkBkZXNrL2NvbXBvbmVudC9hZGRyZXNzL3NwZ0FkZHJlc3NcIjtcbmltcG9ydCBUaGFpQWRkcmVzcyBmcm9tIFwiQGRlc2svY29tcG9uZW50L2FkZHJlc3MvdGhhaS1hZGRyZXNzXCI7XG5pbXBvcnQgeyBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5cbnR5cGUgQWRkcmVzc1Byb3BzID0ge1xuICBtb2RlbDogYW55O1xuICBmb3JtOiBhbnk7XG4gIGRlZmF1bHRDb3VudHJ5Q29kZT86IHN0cmluZztcbiAgZGF0YUlkT3JQcmVmaXg/OiBzdHJpbmc7Ly/pnIDopoHkvKBhZGRyZXNz55qE5YmN57yA77yM5q+U5aaCcHJvZmlsZS5hZGRyZXNz5bCx5LygcHJvZmlsZVxuICBsYXlvdXRDb2w/OiBhbnk7XG4gIG5vdEFkZHJlc3NUeXBlPzogYm9vbGVhbjtcbiAgcmVxdWlyZT86IGJvb2xlYW47XG4gIGlzTm90QWRkcmVzc1ByZXNpeD86IGJvb2xlYW47XG4gIGxhYmVsPzogYW55XG4gIGFkZHJlc3NGaXg/OiBhbnk7IC8vYWRkcmVzc+WvueixoWtleSwg6buY6K6k5Li6YWRkcmVzc1xufSAmIFdpZGdldFByb3BzO1xuXG50eXBlIEFkZHJlc3NTdGF0ZSA9IHtcbiAgY291bnRyeUNvZGU6IHN0cmluZztcbn07XG5cbnR5cGUgQWRkcmVzc0NvbXBvbmVudHMgPSB7XG4gIEJveDogU3R5bGVkRElWO1xufTtcbmNvbnN0IGdlbmVyYXRlUHJvcHNOYW1lID0gKHByb3BOYW1lOiBzdHJpbmcsIGFkZHJlc3NGaXg6IHN0cmluZywgZGF0YUlkPzogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgaWYgKCEhZGF0YUlkKSByZXR1cm4gYCR7ZGF0YUlkfS4ke2FkZHJlc3NGaXh9LiR7cHJvcE5hbWV9YDtcbiAgcmV0dXJuIGAke2FkZHJlc3NGaXh9LiR7cHJvcE5hbWV9YDtcbn07XG5cbmNsYXNzIEFkZHJlc3M8UCBleHRlbmRzIEFkZHJlc3NQcm9wcywgUyBleHRlbmRzIEFkZHJlc3NTdGF0ZSwgQyBleHRlbmRzIEFkZHJlc3NDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgcHVibGljIGlkTm9QYWdlOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgc3RhdGU/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgc3RhdGUpO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gIH1cblxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YFxuICAgICAgYCxcbiAgICB9IGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIGNvbnN0IHsgZGF0YUlkT3JQcmVmaXgsIGFkZHJlc3NGaXggfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oc3VwZXIuaW5pdFN0YXRlKCksIHtcbiAgICAgIGNvdW50cnlDb2RlOiB0aGlzLnByb3BzLmRlZmF1bHRDb3VudHJ5Q29kZVxuICAgICAgICB8fCB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGdlbmVyYXRlUHJvcHNOYW1lKFwiY291bnRyeUNvZGVcIiwgYWRkcmVzc0ZpeCB8fCBcImFkZHJlc3NcIiwgZGF0YUlkT3JQcmVmaXgpKSxcbiAgICB9KSBhcyBTO1xuICB9XG5cbiAgaW5pdElkTm9SZWYgPSAocmVmOiBhbnkpID0+IHtcbiAgICB0aGlzLmlkTm9QYWdlID0gcmVmO1xuICB9O1xuXG4gIGNoYW5nZUNvdW50cnkgPSAoY291bnRyeUNvZGU6IGFueSkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgY291bnRyeUNvZGUsXG4gICAgfSk7XG4gIH07XG5cbiAgZ2VuZXJhdGVQcm9wc05hbWUgPSAocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkPzogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgICBjb25zdCB7IGFkZHJlc3NGaXgsIGlzTm90QWRkcmVzc1ByZXNpeCB9ID0gdGhpcy5wcm9wcztcbiAgICBsZXQgaW5pdEFkZHJlc3NGaXggPSBhZGRyZXNzRml4IHx8IFwiYWRkcmVzc1wiO1xuICAgIGlmICghIWRhdGFJZCkgcmV0dXJuIChpc05vdEFkZHJlc3NQcmVzaXggfHwgcHJvcE5hbWUgPT09IGluaXRBZGRyZXNzRml4KSA/IGAke2RhdGFJZH0uJHtwcm9wTmFtZX1gIDogYCR7ZGF0YUlkfS4ke2luaXRBZGRyZXNzRml4fS4ke3Byb3BOYW1lfWA7XG4gICAgcmV0dXJuIChpc05vdEFkZHJlc3NQcmVzaXggfHwgcHJvcE5hbWUgPT09IGluaXRBZGRyZXNzRml4KSA/IHByb3BOYW1lIDogYCR7aW5pdEFkZHJlc3NGaXh9LiR7cHJvcE5hbWV9YDtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuICAgIGNvbnN0IHsgbW9kZWwsIGZvcm0sIGRlZmF1bHRDb3VudHJ5Q29kZSwgZGF0YUlkT3JQcmVmaXgsIGxheW91dENvbCwgbm90QWRkcmVzc1R5cGUsIHJlcXVpcmUgPSB0cnVlLCBpc05vdEFkZHJlc3NQcmVzaXgsIGFkZHJlc3NGaXggfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgaW5pdEFkZHJlc3NGaXggPSBhZGRyZXNzRml4IHx8IFwiYWRkcmVzc1wiO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3g+XG4gICAgICAgIHshZGVmYXVsdENvdW50cnlDb2RlICYmIDxOU2VsZWN0XG4gICAgICAgICAgbGFiZWw9XCJDb3VudHJ5XCJcbiAgICAgICAgICBzaXplPXtcImxhcmdlXCJ9XG4gICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICByZXF1aXJlZD17cmVxdWlyZX1cbiAgICAgICAgICB0YWJsZU5hbWU9XCJuYXRpb25hbGl0eVwiXG4gICAgICAgICAgZGF0YUZpeGVkPXtcInBvbGljeVwifVxuICAgICAgICAgIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxuICAgICAgICAgIH19XG4gICAgICAgICAgbGF5b3V0Q29sPXtsYXlvdXRDb2x9XG4gICAgICAgICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcHNOYW1lKFwiY291bnRyeUNvZGVcIiwgaW5pdEFkZHJlc3NGaXgsIGRhdGFJZE9yUHJlZml4KX1cbiAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBjb3VudHJ5Q29kZTogdmFsdWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHRoaXMuc2V0VmFsdWVzVG9Nb2RlbCh7XG4gICAgICAgICAgICAgIFt0aGlzLmdlbmVyYXRlUHJvcHNOYW1lKGluaXRBZGRyZXNzRml4LCBkYXRhSWRPclByZWZpeCkgYXMgYW55XToge1xuICAgICAgICAgICAgICAgIGNvdW50cnlDb2RlOiB2YWx1ZSxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnJlc2V0RmllbGRzKFt0aGlzLmdlbmVyYXRlUHJvcHNOYW1lKGluaXRBZGRyZXNzRml4LCBkYXRhSWRPclByZWZpeCldKTtcblxuICAgICAgICAgICAgdGhpcy5wcm9wcy5mb3JtLnNldEZpZWxkc1ZhbHVlKHtcbiAgICAgICAgICAgICAgW3RoaXMuZ2VuZXJhdGVQcm9wc05hbWUoaW5pdEFkZHJlc3NGaXgsIGRhdGFJZE9yUHJlZml4KSBhcyBhbnldOiB7XG4gICAgICAgICAgICAgICAgY291bnRyeUNvZGU6IHZhbHVlLFxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfX1cbiAgICAgICAgLz59XG4gICAgICAgIHt0aGlzLnN0YXRlLmNvdW50cnlDb2RlID09PSBcIlNHUFwiICYmXG4gICAgICAgIDxTcGdBZGRyZXNzIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgICAgICAgcmVmPXt0aGlzLmluaXRJZE5vUmVmfVxuICAgICAgICAgICAgICAgICAgICBkYXRhSWQ9e2RhdGFJZE9yUHJlZml4fVxuICAgICAgICAgICAgICAgICAgICBhZGRyZXNzRml4PXtpbml0QWRkcmVzc0ZpeH1cbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0WHNTbT17bGF5b3V0Q29sICYmIGxheW91dENvbC5sYWJlbENvbH1cbiAgICAgICAgICAgICAgICAgICAgdGV4dFhzU209e2xheW91dENvbCAmJiBsYXlvdXRDb2wud3JhcHBlckNvbH1cbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmV9XG4gICAgICAgICAgICAgICAgICAgIGlzTm90QWRkcmVzc1ByZXNpeD17aXNOb3RBZGRyZXNzUHJlc2l4fVxuICAgICAgICAgICAgICAgICAgICBwcm9wTGFiZWw9e3RoaXMucHJvcHMubGFiZWx9XG5cbiAgICAgICAgLz59XG5cbiAgICAgICAge3RoaXMuc3RhdGUuY291bnRyeUNvZGUgPT09IFwiVEhBXCIgJiZcbiAgICAgICAgPFRoYWlBZGRyZXNzIG1vZGVsPXttb2RlbH1cbiAgICAgICAgICAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICAgICAgICAgICBhZGRyZXNzRml4PXtpbml0QWRkcmVzc0ZpeH1cbiAgICAgICAgICAgICAgICAgICAgIHByZWZpeD17ZGF0YUlkT3JQcmVmaXh9XG4gICAgICAgICAgICAgICAgICAgICBjb3VudHJ5Q29kZT17XCJUSEFcIn1cbiAgICAgICAgICAgICAgICAgICAgIG5vdEFkZHJlc3NUeXBlPXtub3RBZGRyZXNzVHlwZX1cbiAgICAgICAgICAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ9e3JlcXVpcmV9XG4gICAgICAgIC8+fVxuICAgICAgICB7XG4gICAgICAgICAgdGhpcy5zdGF0ZS5jb3VudHJ5Q29kZSAhPT0gXCJTR1BcIiAmJiB0aGlzLnN0YXRlLmNvdW50cnlDb2RlICE9PSBcIlRIQVwiICYmICEhdGhpcy5zdGF0ZS5jb3VudHJ5Q29kZSAmJlxuICAgICAgICAgIDw+XG4gICAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgICAgZm9ybT17Zm9ybX1cbiAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICBsYXlvdXRDb2w9e2xheW91dENvbH1cbiAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiQWRkcmVzc1wiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwiQWRkcmVzc1wiKVxuICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgIHByb3BOYW1lPXt0aGlzLmdlbmVyYXRlUHJvcHNOYW1lKFwic3RyZWV0XCIsIGRhdGFJZE9yUHJlZml4KX1cbiAgICAgICAgICAgICAgc2l6ZT1cImxhcmdlXCJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiIH19XG4gICAgICAgICAgICAgIHJlcXVpcmVkPXt0cnVlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxOVGV4dFxuICAgICAgICAgICAgICBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgIGxheW91dENvbD17bGF5b3V0Q29sfVxuICAgICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQb3N0YWwgQ29kZVwiKVxuICAgICAgICAgICAgICAgIC50aGFpKFwiUG9zdGFsIENvZGVcIilcbiAgICAgICAgICAgICAgICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICBwcm9wTmFtZT17dGhpcy5nZW5lcmF0ZVByb3BzTmFtZShcInBvc3RhbENvZGVcIiwgZGF0YUlkT3JQcmVmaXgpfVxuICAgICAgICAgICAgICBzaXplPVwibGFyZ2VcIlxuICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIgfX1cbiAgICAgICAgICAgICAgcmVxdWlyZWQ9e3RydWV9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvPlxuICAgICAgICB9XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQWRkcmVzcztcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBR0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBREE7QUFDQTtBQURBO0FBd0JBO0FBQ0E7QUFDQTtBQTFCQTtBQTRCQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBaENBO0FBaUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF2Q0E7QUFFQTtBQUNBOzs7QUFDQTs7O0FBSUE7QUFDQTtBQUNBO0FBREE7QUFJQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBREE7QUFJQTs7O0FBbUJBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBRUE7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFEQTtBQUlBO0FBN0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQVZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBOzs7O0FBdklBO0FBQ0E7QUF5SUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/address/index.tsx
