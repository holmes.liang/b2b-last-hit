__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createContainer", function() { return createContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useContainer", function() { return useContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


function createContainer(useHook) {
  var Context = react__WEBPACK_IMPORTED_MODULE_0__.createContext(null);

  function Provider(props) {
    var value = useHook(props.initialState);
    return react__WEBPACK_IMPORTED_MODULE_0__.createElement(Context.Provider, {
      value: value
    }, props.children);
  }

  function useContainer() {
    var value = react__WEBPACK_IMPORTED_MODULE_0__.useContext(Context);

    if (value === null) {
      throw new Error("Component must be wrapped with <Container.Provider>");
    }

    return value;
  }

  return {
    Provider: Provider,
    useContainer: useContainer
  };
}

function useContainer(container) {
  return container.useContainer();
}

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvdW5zdGF0ZWQtbmV4dC9kaXN0L3Vuc3RhdGVkLW5leHQubWpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL3NyYy91bnN0YXRlZC1uZXh0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCJcblxuZXhwb3J0IGludGVyZmFjZSBDb250YWluZXJQcm92aWRlclByb3BzPFN0YXRlID0gdm9pZD4ge1xuXHRpbml0aWFsU3RhdGU/OiBTdGF0ZVxuXHRjaGlsZHJlbjogUmVhY3QuUmVhY3ROb2RlXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgQ29udGFpbmVyPFZhbHVlLCBTdGF0ZSA9IHZvaWQ+IHtcblx0UHJvdmlkZXI6IFJlYWN0LkNvbXBvbmVudFR5cGU8Q29udGFpbmVyUHJvdmlkZXJQcm9wczxTdGF0ZT4+XG5cdHVzZUNvbnRhaW5lcjogKCkgPT4gVmFsdWVcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUNvbnRhaW5lcjxWYWx1ZSwgU3RhdGUgPSB2b2lkPihcblx0dXNlSG9vazogKGluaXRpYWxTdGF0ZT86IFN0YXRlKSA9PiBWYWx1ZSxcbik6IENvbnRhaW5lcjxWYWx1ZSwgU3RhdGU+IHtcblx0bGV0IENvbnRleHQgPSBSZWFjdC5jcmVhdGVDb250ZXh0PFZhbHVlIHwgbnVsbD4obnVsbClcblxuXHRmdW5jdGlvbiBQcm92aWRlcihwcm9wczogQ29udGFpbmVyUHJvdmlkZXJQcm9wczxTdGF0ZT4pIHtcblx0XHRsZXQgdmFsdWUgPSB1c2VIb29rKHByb3BzLmluaXRpYWxTdGF0ZSlcblx0XHRyZXR1cm4gPENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3ZhbHVlfT57cHJvcHMuY2hpbGRyZW59PC9Db250ZXh0LlByb3ZpZGVyPlxuXHR9XG5cblx0ZnVuY3Rpb24gdXNlQ29udGFpbmVyKCk6IFZhbHVlIHtcblx0XHRsZXQgdmFsdWUgPSBSZWFjdC51c2VDb250ZXh0KENvbnRleHQpXG5cdFx0aWYgKHZhbHVlID09PSBudWxsKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoXCJDb21wb25lbnQgbXVzdCBiZSB3cmFwcGVkIHdpdGggPENvbnRhaW5lci5Qcm92aWRlcj5cIilcblx0XHR9XG5cdFx0cmV0dXJuIHZhbHVlXG5cdH1cblxuXHRyZXR1cm4geyBQcm92aWRlciwgdXNlQ29udGFpbmVyIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHVzZUNvbnRhaW5lcjxWYWx1ZSwgU3RhdGUgPSB2b2lkPihcblx0Y29udGFpbmVyOiBDb250YWluZXI8VmFsdWUsIFN0YXRlPixcbik6IFZhbHVlIHtcblx0cmV0dXJuIGNvbnRhaW5lci51c2VDb250YWluZXIoKVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFZQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBOzs7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTs7O0FBR0E7QUFBQTtBQUFBO0FBQUE7OztBQUdBO0FBR0E7OzsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/unstated-next/dist/unstated-next.mjs
