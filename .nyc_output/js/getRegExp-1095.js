__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getRegExp; });
function getRegExp(prefix) {
  var prefixArray = Array.isArray(prefix) ? prefix : [prefix];
  var prefixToken = prefixArray.join('').replace(/(\$|\^)/g, '\\$1');

  if (prefixArray.length > 1) {
    prefixToken = '[' + prefixToken + ']';
  }

  return new RegExp('(\\s|^)(' + prefixToken + ')[^\\s]*', 'g');
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0UmVnRXhwLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvdXRpbHMvZ2V0UmVnRXhwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gZ2V0UmVnRXhwKHByZWZpeCkge1xuICB2YXIgcHJlZml4QXJyYXkgPSBBcnJheS5pc0FycmF5KHByZWZpeCkgPyBwcmVmaXggOiBbcHJlZml4XTtcbiAgdmFyIHByZWZpeFRva2VuID0gcHJlZml4QXJyYXkuam9pbignJykucmVwbGFjZSgvKFxcJHxcXF4pL2csICdcXFxcJDEnKTtcblxuICBpZiAocHJlZml4QXJyYXkubGVuZ3RoID4gMSkge1xuICAgIHByZWZpeFRva2VuID0gJ1snICsgcHJlZml4VG9rZW4gKyAnXSc7XG4gIH1cblxuICByZXR1cm4gbmV3IFJlZ0V4cCgnKFxcXFxzfF4pKCcgKyBwcmVmaXhUb2tlbiArICcpW15cXFxcc10qJywgJ2cnKTtcbn0iXSwibWFwcGluZ3MiOiJBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/utils/getRegExp.js
