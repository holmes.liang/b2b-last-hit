__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./consts */ "./src/common/consts.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/index */ "./src/common/index.tsx");




var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/common/utils.tsx";





var DATE_FORMAT = _consts__WEBPACK_IMPORTED_MODULE_5__["default"].DATE_FORMAT,
    CURRENCY_SYMBOL_TABLE = _consts__WEBPACK_IMPORTED_MODULE_5__["default"].CURRENCY_SYMBOL_TABLE,
    PAYMENT_METHOD_RECURRING = _consts__WEBPACK_IMPORTED_MODULE_5__["default"].PAYMENT_METHOD_RECURRING;

/**
 * copy from https://github.com/AceMetrix/jquery-deparam
 */
var deparam = function deparam(params) {
  var coerce = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var obj = {};
  var coerce_types = {
    true: !0,
    false: !1,
    null: null
  }; // If params is an empty string or otherwise falsy, return obj.

  if (!params) {
    return obj;
  } // Iterate over all name=value pairs.


  params.replace(/\+/g, " ").split("&").forEach(function (v) {
    var param = v.split("=");
    var key = decodeURIComponent(param[0]);
    var val;
    var cur = obj;
    var i = 0; // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
    // into its component parts.

    var keys = key.split("][");
    var keys_last = keys.length - 1; // If the first keys part contains [ and the last ends with ], then []
    // are correctly balanced.

    if (/\[/.test(keys[0]) && /\]$/.test(keys[keys_last])) {
      // Remove the trailing ] from the last keys part.
      keys[keys_last] = keys[keys_last].replace(/\]$/, ""); // Split first keys part into two parts on the [ and add them back onto
      // the beginning of the keys array.

      keys = keys.shift().split("[").concat(keys);
      keys_last = keys.length - 1;
    } else {
      // Basic 'foo' style key.
      keys_last = 0;
    } // Are we dealing with a name=value pair, or just a name?


    if (param.length === 2) {
      val = decodeURIComponent(param[1]); // Coerce values.

      if (coerce) {
        val = val && !isNaN(val) && +val + "" === val ? +val // number
        : val === "undefined" ? undefined // undefined
        : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
        : val; // string
      }

      if (keys_last) {
        // Complex key, build deep object structure based on a few rules:
        // * The 'cur' pointer starts at the object top-level.
        // * [] = array push (n is set to array length), [n] = array if n is
        //   numeric, otherwise object.
        // * If at the last keys part, set the value.
        // * For each keys part, if the current level is undefined create an
        //   object or array based on the type of the next keys part.
        // * Move the 'cur' pointer to the next level.
        // * Rinse & repeat.
        for (; i <= keys_last; i++) {
          key = keys[i] === "" ? cur.length : keys[i];
          cur = cur[key] = i < keys_last ? cur[key] || (keys[i + 1] && isNaN(keys[i + 1]) ? {} : []) : val;
        }
      } else {
        // Simple key, even simpler rules, since only scalars and shallow
        // arrays are allowed.
        if (Object.prototype.toString.call(obj[key]) === "[object Array]") {
          // val is already an array, so push on the next value.
          obj[key].push(val);
        } else if ({}.hasOwnProperty.call(obj, key)) {
          // val isn't an array, but since a second value has been specified,
          // convert val into an array.
          obj[key] = [obj[key], val];
        } else {
          // val is a scalar.
          obj[key] = val;
        }
      }
    } else if (key) {
      // No value was defined, so set something meaningful.
      obj[key] = coerce ? undefined : "";
    }
  });
  return obj;
};
/**
 * copy from https://github.com/knowledgecode/jquery-param
 */


var param = function param(a) {
  var s = [];

  var add = function add(k, v) {
    v = typeof v === "function" ? v() : v;
    v = v === null ? "" : v === undefined ? "" : v;
    s[s.length] = encodeURIComponent(k) + "=" + encodeURIComponent(v);
  };

  var buildParams = function buildParams(prefix, obj) {
    var i, len, key;

    if (prefix) {
      if (Array.isArray(obj)) {
        for (i = 0, len = obj.length; i < len; i++) {
          buildParams(prefix + "[" + (typeof obj[i] === "object" && obj[i] ? i : "") + "]", obj[i]);
        }
      } else if (String(obj) === "[object Object]") {
        for (key in obj) {
          buildParams(prefix + "[" + key + "]", obj[key]);
        }
      } else {
        add(prefix, obj);
      }
    } else if (Array.isArray(obj)) {
      for (i = 0, len = obj.length; i < len; i++) {
        add(obj[i].name, obj[i].value);
      }
    } else {
      for (key in obj) {
        buildParams(key, obj[key]);
      }
    }

    return s;
  };

  return buildParams("", a).join("&");
};

var Utils =
/*#__PURE__*/
function () {
  function Utils() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Utils);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Utils, [{
    key: "toArray",
    value: function toArray(any) {
      return Array.isArray(any) ? any : any == null ? [] : [any];
    }
  }, {
    key: "times",
    value: function times(element, _times) {
      var ret = new Array(_times || 1);
      ret.fill(element, 0, ret.length);
      return ret;
    }
  }, {
    key: "truncateAsDate",
    value: function truncateAsDate(date) {
      return date ? date.substring(0, 11) : date;
    }
  }, {
    key: "formatAmount",
    value: function formatAmount(currency) {
      if (currency != null) {
        return (currency + "").toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      } else {
        return "";
      }
    }
  }, {
    key: "isEmpty",
    value: function isEmpty(o) {
      return !this.isNumber(o) && (this.isNull(o) || this.isUndefined(o) || o.length == 0);
    }
  }, {
    key: "isString",
    value: function isString(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "String";
    }
  }, {
    key: "isNumber",
    value: function isNumber(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Number";
    }
  }, {
    key: "isBoolean",
    value: function isBoolean(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Boolean";
    }
  }, {
    key: "isFunction",
    value: function isFunction(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Function";
    }
  }, {
    key: "isNull",
    value: function isNull(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Null";
    }
  }, {
    key: "isUndefined",
    value: function isUndefined(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Undefined";
    }
  }, {
    key: "isObject",
    value: function isObject(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Object";
    }
  }, {
    key: "isArray",
    value: function isArray(o) {
      return Array.isArray(o);
    }
  }, {
    key: "isDate",
    value: function isDate(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Date";
    }
  }, {
    key: "isRegExp",
    value: function isRegExp(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "RegExp";
    }
  }, {
    key: "isError",
    value: function isError(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Error";
    }
  }, {
    key: "isSymbol",
    value: function isSymbol(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Symbol";
    }
  }, {
    key: "isPromise",
    value: function isPromise(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Promise";
    }
  }, {
    key: "isSet",
    value: function isSet(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "Set";
    }
  }, {
    key: "isFormData",
    value: function isFormData(o) {
      return Object.prototype.toString.call(o).slice(8, -1) === "FormData";
    }
  }, {
    key: "isFalse",
    value: function isFalse(o) {
      return !o || o === "null" || o === "undefined" || o === "false" || o === "NaN";
    }
  }, {
    key: "isFalseNot0",
    value: function isFalseNot0(o) {
      return !o || o === "null" || o === "undefined" || o === "false" || o === "NaN" || o === 0;
    }
  }, {
    key: "isTrue",
    value: function isTrue(o) {
      return !this.isFalse(o);
    }
  }, {
    key: "fromQueryString",
    value: function fromQueryString(qs) {
      if (!this.isNull(qs) && !this.isUndefined(qs)) {
        return deparam(qs);
      } else {
        var search = window.location.search;

        if (search && search !== "?") {
          return deparam(search.substring(1));
        } else {
          return {};
        }
      }
    }
  }, {
    key: "toQueryString",
    value: function toQueryString(obj) {
      return param(obj);
    }
  }, {
    key: "encodeCurrentURI",
    value: function encodeCurrentURI(qs) {
      if (this.isNull(qs) || this.isUndefined(qs)) {
        return encodeURIComponent(window.location.origin + window.location.pathname);
      } else {
        return encodeURIComponent("".concat(window.location.origin).concat(window.location.pathname, "?").concat(qs));
      }
    }
  }, {
    key: "copyTextToClipboard",
    value: function copyTextToClipboard(s) {
      return new Promise(function (resolve, reject) {
        var textarea = document.createElement("textarea"); // textarea.style.display = 'none';

        textarea.style.position = "fixed";
        textarea.style.height = "0";
        textarea.style.top = "-9999px";
        textarea.value = s;
        document.body.appendChild(textarea);
        textarea.select();
        textarea.setSelectionRange(0, s.length);
        var ret = document.execCommand("copy");
        document.body.removeChild(textarea);

        if (ret) {
          resolve();
        } else {
          reject();
        }
      });
    }
  }, {
    key: "convertToMoment",
    value: function convertToMoment(value) {
      if (moment__WEBPACK_IMPORTED_MODULE_4___default.a.isMoment(value)) {
        return value;
      }

      if (value == null) {
        return moment__WEBPACK_IMPORTED_MODULE_4___default.a.invalid();
      }

      return moment__WEBPACK_IMPORTED_MODULE_4___default()(value, DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
    }
  }, {
    key: "dateFromNow",
    value: function dateFromNow(value) {
      return this.convertToMoment(value).fromNow();
    }
  }, {
    key: "parseDate",
    value: function parseDate(value) {
      return this.convertToMoment(value).toDate();
    }
  }, {
    key: "dateTimeFromNow",
    value: function dateTimeFromNow(value) {
      if (value) {
        return this.convertToMoment(value).fromNow();
      } else {
        return "";
      }
    }
  }, {
    key: "formatDate",
    value: function formatDate(value) {
      var formatStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DATE_FORMAT.DATE_FORMAT;
      return moment__WEBPACK_IMPORTED_MODULE_4___default()(value, DATE_FORMAT.DATE_FORMAT).format(formatStr);
    }
  }, {
    key: "formatDateTime",
    value: function formatDateTime(value) {
      var formatStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DATE_FORMAT.DATE_TIME_FORMAT;
      return this.convertToMoment(value).format(formatStr);
    }
  }, {
    key: "stringifyDate",
    value: function stringifyDate(value) {
      if (value) {
        return this.convertToMoment(value).format(DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
      }

      return "";
    }
  }, {
    key: "toThousands",
    value: function toThousands(num) {
      num = num + "";

      if (!num.includes(".")) {
        num += ".";
      }

      return num.replace(/(\d)(?=(\d{3})+\.)/g, function ($0, $1) {
        return $1 + ",";
      }).replace(/\.$/, "");
    }
  }, {
    key: "toFloat",
    value: function toFloat(num) {
      if (!num) return "";
      return String(num).replace(/,/gi, "");
    }
  }, {
    key: "isThousands",
    value: function isThousands(num) {
      if (!num) return false;
      if (String(num).indexOf(",") > 0) return true;
    }
  }, {
    key: "setThousandsValue",
    value: function setThousandsValue(value) {
      var isThousands = this.isThousands(value);
      var valueThousands = isThousands ? this.toThousands(parseFloat(this.toFloat(value)).toFixed(2)) : this.toThousands(parseFloat(value).toFixed(2));
      return valueThousands;
    }
  }, {
    key: "setFloatValue",
    value: function setFloatValue(value) {
      var isThousands = this.isThousands(value);
      var valueFloat = isThousands ? parseFloat(this.toFloat(value)) : parseFloat(value);
      return valueFloat;
    }
  }, {
    key: "getCountry",
    value: function getCountry() {
      return lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(_common_index__WEBPACK_IMPORTED_MODULE_8__["Storage"].Account.session().get("Account"), "struct.country", "");
    }
  }, {
    key: "formatCurrencyInfo",
    value: function formatCurrencyInfo(amount, currencyCode) {
      var digits = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;
      var currencyText = !currencyCode ? "" : CURRENCY_SYMBOL_TABLE[currencyCode];
      var format = "0,0";

      for (var index = 0; index < digits; index++) {
        if (index === 0) {
          format = format + ".0";
        } else {
          format = format + "0";
        }
      }

      if (!amount) {
        return {
          amount: "0.00",
          currencyText: currencyText
        };
      }

      return {
        amount: this.toThousands(amount.toFixed(2)),
        currencyText: currencyText
      };
    }
  }, {
    key: "formatCurrency",
    value: function formatCurrency(amount, currencyCode) {
      var digits = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;
      var currencyText = !currencyCode ? "" : CURRENCY_SYMBOL_TABLE[currencyCode];
      var negativeSign = amount && amount < 0 ? "-" : "";
      var format = "0,0";

      for (var index = 0; index < digits; index++) {
        if (index === 0) {
          format = format + ".0";
        } else {
          format = format + "0";
        }
      }

      return "".concat(negativeSign).concat(currencyText).concat(this.toThousands(Math.abs(amount).toFixed(2)));
    }
  }, {
    key: "formatCurrencyHtml",
    value: function formatCurrencyHtml(amount, currencyCode, style) {
      var digits = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2;
      var currencyText = !currencyCode ? "" : CURRENCY_SYMBOL_TABLE[currencyCode];
      var negativeSign = amount && amount < 0 ? "-" : "";
      var format = "0,0";

      for (var index = 0; index < digits; index++) {
        if (index === 0) {
          format = format + ".0";
        } else {
          format = format + "0";
        }
      }

      var styleObj = !style ? {
        color: "rgba(0,0,0,0.85)"
      } : style;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("label", {
        style: styleObj,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 442
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          fontSize: "80%",
          fontWeight: 400
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 443
        },
        __self: this
      }, "".concat(negativeSign).concat(currencyText)), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          fontSize: "16px",
          paddingLeft: "5px",
          fontWeight: 500
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 449
        },
        __self: this
      }, "".concat(this.toThousands(Math.abs(amount).toFixed(2)))));
    }
  }, {
    key: "formatCurrencyHtmlNumber",
    value: function formatCurrencyHtmlNumber(amount, currencyCode, style) {
      var digits = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2;
      var currencyText = !currencyCode ? "" : CURRENCY_SYMBOL_TABLE[currencyCode];
      var negativeSign = amount && amount < 0 ? "-" : "";
      var format = "0,0";

      for (var index = 0; index < digits; index++) {
        if (index === 0) {
          format = format + ".0";
        } else {
          format = format + "0";
        }
      }

      var styleObj = !style ? {
        color: "rgba(0,0,0,0.85)"
      } : style;
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("label", {
        style: styleObj,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 473
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          fontSize: "80%",
          fontWeight: 400
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, "".concat(negativeSign).concat(currencyText)), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        style: {
          fontSize: "16px",
          paddingLeft: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 480
        },
        __self: this
      }, "".concat(this.toThousands(Math.abs(lodash__WEBPACK_IMPORTED_MODULE_7___default.a.toNumber(amount))))));
    }
  }, {
    key: "getValueFromJSON",
    value: function getValueFromJSON() {
      var jsonObject = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var id = arguments.length > 1 ? arguments[1] : undefined;
      if (id.indexOf(".") === -1) return jsonObject[id];
      var ids = id.split(".");
      var parent = jsonObject;
      var value = null;
      var values = ids.map(function (id) {
        if (parent == null) {
          return null;
        } else {
          value = parent[id];
          parent = value;
          return value;
        }
      });
      return values[values.length - 1];
    }
  }, {
    key: "checkPlanPremiumIsZero",
    value: function checkPlanPremiumIsZero(plan) {
      var lumpsumAr = this.getValueFromJSON(plan, "policyPremium.lumpsum.ar") || 0; // 分期付款，不止是用于月付

      var subsequentAr = this.getValueFromJSON(plan, "policyPremium.subsequent.ar") || 0;
      return lumpsumAr === 0 && subsequentAr === 0;
    }
  }, {
    key: "getPolicyPremium",
    value: function getPolicyPremium() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var recurringPayment = policy.recurringPayment;
      var lumpsum = this.getValueFromJSON(policy, "policyPremium.lumpsum") || {};
      var subsequent = this.getValueFromJSON(policy, "policyPremium.subsequent") || {};

      if (recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    }
  }, {
    key: "getPolicyPremiumVmi",
    value: function getPolicyPremiumVmi() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var cartPremium = arguments.length > 2 ? arguments[2] : undefined;
      var openEnd = policy.openEnd,
          recurringPayment = policy.recurringPayment;
      var policyCopy = JSON.parse(JSON.stringify(policy || {}));
      policyCopy.policyPremium = cartPremium;
      var lumpsum = this.getValueFromJSON(policyCopy, "policyPremium.lumpsum") || {};
      var subsequent = this.getValueFromJSON(policyCopy, "policyPremium.subsequent") || {};

      if (openEnd || recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    } //todo

  }, {
    key: "getPolicyPremium1",
    value: function getPolicyPremium1() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var openEnd = policy.openEnd,
          recurringPayment = policy.recurringPayment;
      var lumpsum = this.getValueFromJSON(policy, "lumpsum") || {};
      var subsequent = this.getValueFromJSON(policy, "subsequent") || {};

      if (openEnd || recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    }
  }, {
    key: "renderPolicyPremium1",
    value: function renderPolicyPremium1() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var openEnd = policy.openEnd,
          recurringPayment = policy.recurringPayment;
      var policyPremium = this.getPolicyPremium1(policy);

      var _this$formatCurrencyI = this.formatCurrencyInfo(policyPremium.ar, policy.currencyCode),
          amount = _this$formatCurrencyI.amount,
          currencyText = _this$formatCurrencyI.currencyText;

      var isRecurringPayment = openEnd || recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING; // 分期付款，不止是用于月付

      var premiumDesc = policy.premiumDesc || "month";
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        className: "price price--lg",
        "data-postfix": isRecurringPayment ? "/ ".concat(premiumDesc ? premiumDesc : "month") : "",
        "data-prefix": "S$",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 554
        },
        __self: this
      }, amount);
    }
  }, {
    key: "renderPolicyPremium",
    value: function renderPolicyPremium() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var paymentSchedule = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var recurringPayment = policy.recurringPayment;
      var policyPremium = this.getPolicyPremium(policy, paymentSchedule);

      var _this$formatCurrencyI2 = this.formatCurrencyInfo(policyPremium.ar, policy.premCurrencyCode || policy.currencyCode),
          amount = _this$formatCurrencyI2.amount,
          currencyText = _this$formatCurrencyI2.currencyText;

      var isRecurringPayment = recurringPayment || paymentSchedule === PAYMENT_METHOD_RECURRING; // 分期付款，不止是用于月付

      var premiumDesc = policy.premiumDesc || "month";
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        className: "price price--lg",
        "data-postfix": isRecurringPayment ? "/ ".concat(premiumDesc ? premiumDesc : "month") : "",
        "data-prefix": currencyText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 572
        },
        __self: this
      }, amount);
    }
  }, {
    key: "renderDownpaymentPremium",
    value: function renderDownpaymentPremium() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var ar = this.getValueFromJSON(policy, "policyPremium.downpayment.ar");

      var _this$formatCurrencyI3 = this.formatCurrencyInfo(ar, policy.currencyCode),
          amount = _this$formatCurrencyI3.amount,
          currencyText = _this$formatCurrencyI3.currencyText;

      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        className: "price price--lg",
        "data-prefix": currencyText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 586
        },
        __self: this
      }, amount);
    }
  }, {
    key: "getPolicyPremiumBeforeAfter",
    value: function getPolicyPremiumBeforeAfter() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var policyPremiumDataFixed = arguments.length > 1 ? arguments[1] : undefined;
      var paymentSchedule = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var recurringPayment = policy.recurringPayment,
          premiumDesc = policy.premiumDesc;
      var lumpsum = this.getValueFromJSON(policy, "".concat(policyPremiumDataFixed, ".lumpsum")) || {};
      var subsequent = this.getValueFromJSON(policy, "".concat(policyPremiumDataFixed, ".subsequent")) || {};

      if (recurringPayment || premiumDesc || paymentSchedule === PAYMENT_METHOD_RECURRING) {
        return subsequent;
      }

      return lumpsum;
    }
  }, {
    key: "renderPolicyPremiumBeforeAfter",
    value: function renderPolicyPremiumBeforeAfter() {
      var policy = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var policyPremiumDataFixed = arguments.length > 1 ? arguments[1] : undefined;
      var paymentSchedule = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var recurringPayment = policy.recurringPayment;
      var policyPremium = this.getPolicyPremiumBeforeAfter(policy, policyPremiumDataFixed, paymentSchedule);

      var _this$formatCurrencyI4 = this.formatCurrencyInfo(policyPremium.ar, policy.currencyCode),
          amount = _this$formatCurrencyI4.amount,
          currencyText = _this$formatCurrencyI4.currencyText;

      var isRecurringPayment = recurringPayment || policy.premiumDesc || paymentSchedule === PAYMENT_METHOD_RECURRING; // 分期付款，不止是用于月付

      var premiumDesc = policy.premiumDesc || "month";
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        className: "price price--lg",
        "data-postfix": isRecurringPayment ? "/ ".concat(premiumDesc ? premiumDesc : "month") : "",
        "data-prefix": currencyText,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 610
        },
        __self: this
      }, amount);
    }
  }, {
    key: "redirectToPayPage",
    value: function redirectToPayPage(html) {
      var wrap = document.createElement("div");
      wrap.innerHTML = html;
      document.body.appendChild(wrap);
      wrap.querySelector("form").submit();
    }
  }, {
    key: "dateTimeInit",
    value: function dateTimeInit(value) {
      if (value) {
        var mValue = moment__WEBPACK_IMPORTED_MODULE_4___default()(value, _consts__WEBPACK_IMPORTED_MODULE_5__["default"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
        return mValue.toDate();
      } else {
        return new Date();
      }
    }
  }, {
    key: "toDateString",
    value: function toDateString(value) {
      if (value) {
        var mValue = moment__WEBPACK_IMPORTED_MODULE_4___default()(value, _consts__WEBPACK_IMPORTED_MODULE_5__["default"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
        return mValue.format(_consts__WEBPACK_IMPORTED_MODULE_5__["default"].DATE_FORMAT.DATE_FORMAT);
      } else {
        return "";
      }
    }
  }, {
    key: "toTimeString",
    value: function toTimeString(value) {
      if (value) {
        var mValue = moment__WEBPACK_IMPORTED_MODULE_4___default()(value, _consts__WEBPACK_IMPORTED_MODULE_5__["default"].DATE_FORMAT.DATE_TIME_WITH_TIME_ZONE);
        return mValue.format("HH:mm:ss");
      } else {
        return "";
      }
    }
  }, {
    key: "toDateTimeString",
    value: function toDateTimeString(value) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "DD/MM/YYYYTHH:mm:ssZ";

      if (value) {
        var date = typeof value === "string" ? this.dateTimeInit(value) : value;
        var mValue = moment__WEBPACK_IMPORTED_MODULE_4___default()(date);
        return mValue.format(format);
      } else {
        return "";
      }
    }
  }, {
    key: "joinElements",
    value: function joinElements(list, separator) {
      if (!list || list.length <= 1) {
        return list;
      }

      return list.slice(0, -1).reduce(function (acc, cur, index) {
        return acc.concat([cur, react__WEBPACK_IMPORTED_MODULE_6___default.a.cloneElement(separator, {
          key: index
        })]);
      }, []).concat(list.slice(-1));
    }
  }, {
    key: "getCurrencyText",
    value: function getCurrencyText(currencyCode) {
      var currencyText = !currencyCode ? "" : _consts__WEBPACK_IMPORTED_MODULE_5__["default"].CURRENCY_SYMBOL_TABLE[currencyCode];
      return currencyText;
    }
  }, {
    key: "isTccArr",
    value: function isTccArr(productCodeUp) {
      return ["SARP", "TCC", "ARPKC", "TWPS"].includes(productCodeUp);
    }
  }, {
    key: "isSuretyArr",
    value: function isSuretyArr(productCodeUp) {
      return ["FWB", "LB", "RB", "SB", "PB"].includes(productCodeUp);
    }
  }, {
    key: "isLiaArr",
    value: function isLiaArr(productCodeUp) {
      return ["PUBL", "PI", "PL", "DOL", "EL"].includes(productCodeUp);
    }
  }, {
    key: "isEb",
    value: function isEb(productCodeUp) {
      return ["WICUN", "WICN"].includes(productCodeUp);
    }
  }, {
    key: "isLife",
    value: function isLife(productCodeUp) {
      return ["FMCA", "ENEL", "HPRT", "WLSC"].includes(productCodeUp);
    }
  }, {
    key: "isTccSuretyLia",
    value: function isTccSuretyLia(productCodeUp) {
      return this.isTccArr(productCodeUp) || this.isSuretyArr(productCodeUp) || this.isLiaArr(productCodeUp) || this.isEb(productCodeUp);
    }
  }, {
    key: "getParamsFromUrl",
    value: function getParamsFromUrl() {
      var search = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window.location.href;
      var paramsStr = search.split("?")[1];

      if (paramsStr) {
        var paramsArr = paramsStr.split("&");
        var obj = {};
        paramsArr.forEach(function (p) {
          var keyValue = p.split("=");
          obj[keyValue[0]] = keyValue[1];
        });
        return obj;
      }

      return null;
    }
  }, {
    key: "getIsMobile",
    value: function getIsMobile() {
      if (this.getIsInApp()) return true;

      if (/Android|webOS|iPhone|iPad|iPod|PlayBook|BlackBerry/i.test(navigator.userAgent)) {
        if (document.body.scrollWidth >= 768) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    }
  }, {
    key: "getIsInApp",
    value: function getIsInApp() {
      return window.device == "app";
    }
  }, {
    key: "getPureMobile",
    value: function getPureMobile() {
      return /Android|webOS|iPhone|iPad|iPod|PlayBook|BlackBerry/i.test(navigator.userAgent);
    }
  }, {
    key: "getWhith",
    value: function getWhith() {
      if (window.innerWidth) {
        return window.innerWidth;
      } else {
        if (document.compatMode == "CSS1Compat") {
          return document.documentElement.clientWidth;
        } else {
          return document.body.clientWidth;
        }
      }
    }
  }, {
    key: "getHeight",
    value: function getHeight() {
      if (window.innerHeight) {
        return window.innerHeight;
      } else {
        if (document.compatMode == "CSS1Compat") {
          return document.documentElement.clientHeight;
        } else {
          return document.body.clientHeight;
        }
      }
    }
  }, {
    key: "bodyScale",
    value: function bodyScale() {
      var scale = this.getWhith() > 1200 || this.getWhith() < 768 ? 1 : this.getWhith() / 1200;
      var container = document.querySelector("#root>div[data-widget='page'] .page-body");

      if (container) {
        try {
          container.childNodes[0].style.minHeight = this.getHeight() * (1 / scale) - 196 * scale + "px";
          container.childNodes[0].style.backgroundColor = "#fff";
        } catch (e) {}
      }
    }
  }, {
    key: "bodyScaleContainer",
    value: function bodyScaleContainer() {
      var scale = this.getWhith() > 1200 || this.getWhith() < 768 ? 1 : this.getWhith() / 1200;
      var container = document.querySelector(".page-body .container");

      if (container) {
        try {
          container.style.minHeight = this.getHeight() * (1 / scale) - 196 * scale + "px";
        } catch (e) {}
      }
    }
  }, {
    key: "getMonthEn",
    value: function getMonthEn(month) {
      var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

      if (!month && month !== 0) {
        return months[new Date().getMonth()];
      }

      return months[month - 1] || months[new Date().getMonth()];
    }
  }, {
    key: "returnNotNullValue",
    value: function returnNotNullValue(value) {
      return this.isFalse(value) ? "" : value;
    }
  }, {
    key: "showAmount",
    value: function showAmount(text, symbol) {
      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 808
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement("label", {
        style: {
          fontSize: "12px",
          paddingRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 809
        },
        __self: this
      }, symbol), this.toThousands(parseFloat(text || 0).toFixed(2)));
    } //判断浏览器类型和版本

  }, {
    key: "getBrowserVersion",
    value: function getBrowserVersion(userAgent) {
      // = navigator.userAgent
      var version = "";

      if (userAgent.indexOf("Firefox") > -1) {
        version = userAgent.match(/firefox\/[\d.]+/gi)[0].match(/[\d]+/)[0];
        return "Firefox " + version;
      } else if (userAgent.indexOf("Edge") > -1) {
        version = userAgent.match(/edge\/[\d.]+/gi)[0].match(/[\d]+/)[0];
        return "Edge " + version;
      } else if (userAgent.indexOf("Opera") > -1 || userAgent.indexOf("OPR") > -1) {
        if (userAgent.indexOf("Opera") > -1) {
          version = userAgent.match(/opera\/[\d.]+/gi)[0].match(/[\d]+/)[0];
          return "Opera " + version;
        }

        if (userAgent.indexOf("OPR") > -1) {
          version = userAgent.match(/opr\/[\d.]+/gi)[0].match(/[\d]+/)[0];
          return "Opera " + version;
        }
      } else if (userAgent.indexOf("Chrome") > -1) {
        version = userAgent.match(/chrome\/[\d.]+/gi)[0].match(/[\d]+/)[0];
        return "Chrome " + version;
      } else if (userAgent.indexOf("Safari") > -1) {
        version = userAgent.match(/safari\/[\d.]+/gi)[0].match(/[\d]+/)[0];
        return "Safari " + version;
      } else if (userAgent.indexOf("MSIE") > -1 || userAgent.indexOf("Trident") > -1) {
        if (userAgent.indexOf("MSIE") > -1) {
          version = userAgent.match(/msie [\d.]+/gi)[0].match(/[\d]+/)[0];
          return "IE " + version;
        }

        if (userAgent.indexOf("Trident") > -1) {
          var versionTrident = userAgent.match(/trident\/[\d.]+/gi)[0].match(/[\d]+/)[0];
          version = parseInt(versionTrident) + 4;
          return "IE " + version;
        }
      }

      return userAgent;
    }
  }, {
    key: "changeColorType",
    value: function changeColorType(sColor) {
      sColor = sColor.toLowerCase(); //十六进制颜色值的正则表达式

      var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/; // 如果是16进制颜色

      if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
          var sColorNew = "#";

          for (var i = 1; i < 4; i += 1) {
            sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
          }

          sColor = sColorNew;
        } //处理六位的颜色值


        var sColorChange = [];

        for (var i = 1; i < 7; i += 2) {
          sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
        }

        return sColorChange.join(",");
      }

      return sColor;
    }
  }, {
    key: "tokenRecaptcha",
    value: function () {
      var _tokenRecaptcha = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(action) {
        var envUrl;
        return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                envUrl = "http://localhost";

                if (true) {
                  envUrl = "".concat(envUrl, ":").concat("4000");
                }

                return _context2.abrupt("return", _common_index__WEBPACK_IMPORTED_MODULE_8__["Ajax"].get("".concat(envUrl, "/recaptchakey")).then(
                /*#__PURE__*/
                function () {
                  var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
                  /*#__PURE__*/
                  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(res) {
                    var tokenRe, key, token;
                    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            tokenRe = "0123456789";
                            key = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(res, "body.respData", "");

                            if (true) {
                              _context.next = 15;
                              break;
                            }

                            _context.next = 5;
                            return grecaptchaReady();

                          case 5:
                            _context.prev = 5;
                            _context.next = 8;
                            return window.grecaptcha.execute(key, {
                              action: action
                            });

                          case 8:
                            token = _context.sent;
                            tokenRe = token;
                            _context.next = 14;
                            break;

                          case 12:
                            _context.prev = 12;
                            _context.t0 = _context["catch"](5);

                          case 14:
                            return _context.abrupt("return", tokenRe);

                          case 15:
                            return _context.abrupt("return", tokenRe);

                          case 16:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee, null, [[5, 12]]);
                  }));

                  return function (_x2) {
                    return _ref.apply(this, arguments);
                  };
                }()).catch(function () {
                  return "0123456789";
                }));

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function tokenRecaptcha(_x) {
        return _tokenRecaptcha.apply(this, arguments);
      }

      return tokenRecaptcha;
    }()
  }]);

  return Utils;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Utils());

var _grecaptchaReady = function _grecaptchaReady(resolve) {
  window.grecaptcha.ready(function () {
    resolve(true);
  });
};

var grecaptchaReady = function grecaptchaReady() {
  return new Promise(function (resolve, reject) {
    _grecaptchaReady(resolve);
  });
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3V0aWxzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2NvbW1vbi91dGlscy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XG5pbXBvcnQgQ29uc3RzIGZyb20gXCIuL2NvbnN0c1wiO1xuaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IHsgQWpheCwgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uL2luZGV4XCI7XG5cbmNvbnN0IHsgREFURV9GT1JNQVQsIENVUlJFTkNZX1NZTUJPTF9UQUJMRSwgUEFZTUVOVF9NRVRIT0RfUkVDVVJSSU5HIH0gPSBDb25zdHM7XG5cbnR5cGUgUVMgPSB7XG4gIHRpY2tldD86IHN0cmluZyB8IG51bGw7XG4gIHRva2VuPzogc3RyaW5nIHwgbnVsbDtcbiAgZGVsZWdhdGVkPzogc3RyaW5nIHwgbnVsbDtcbiAgdGhlbWU/OiBzdHJpbmcgfCBudWxsO1xuICBhdXRoRmFpbFRvPzogc3RyaW5nIHwgbnVsbDtcbiAgcm9vdFVybEZyb20/OiBzdHJpbmcgfCBudWxsO1xuICB1cmxGcm9tPzogc3RyaW5nIHwgbnVsbDtcbiAgcGFyY2VsPzogc3RyaW5nIHwgbnVsbDtcbn07XG5cbi8qKlxuICogY29weSBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9BY2VNZXRyaXgvanF1ZXJ5LWRlcGFyYW1cbiAqL1xuY29uc3QgZGVwYXJhbSA9IChwYXJhbXM/OiBzdHJpbmcgfCBudWxsLCBjb2VyY2U6IGJvb2xlYW4gPSBmYWxzZSkgPT4ge1xuICB2YXIgb2JqOiBhbnkgPSB7fTtcbiAgdmFyIGNvZXJjZV90eXBlczogYW55ID0geyB0cnVlOiAhMCwgZmFsc2U6ICExLCBudWxsOiBudWxsIH07XG5cbiAgLy8gSWYgcGFyYW1zIGlzIGFuIGVtcHR5IHN0cmluZyBvciBvdGhlcndpc2UgZmFsc3ksIHJldHVybiBvYmouXG4gIGlmICghcGFyYW1zKSB7XG4gICAgcmV0dXJuIG9iajtcbiAgfVxuXG4gIC8vIEl0ZXJhdGUgb3ZlciBhbGwgbmFtZT12YWx1ZSBwYWlycy5cbiAgcGFyYW1zXG4gICAgLnJlcGxhY2UoL1xcKy9nLCBcIiBcIilcbiAgICAuc3BsaXQoXCImXCIpXG4gICAgLmZvckVhY2goZnVuY3Rpb24odikge1xuICAgICAgdmFyIHBhcmFtOiBzdHJpbmdbXSA9IHYuc3BsaXQoXCI9XCIpO1xuICAgICAgdmFyIGtleTogc3RyaW5nID0gZGVjb2RlVVJJQ29tcG9uZW50KHBhcmFtWzBdKTtcbiAgICAgIHZhciB2YWw6IGFueTtcbiAgICAgIHZhciBjdXI6IGFueSA9IG9iajtcbiAgICAgIHZhciBpOiBudW1iZXIgPSAwO1xuICAgICAgLy8gSWYga2V5IGlzIG1vcmUgY29tcGxleCB0aGFuICdmb28nLCBsaWtlICdhW10nIG9yICdhW2JdW2NdJywgc3BsaXQgaXRcbiAgICAgIC8vIGludG8gaXRzIGNvbXBvbmVudCBwYXJ0cy5cbiAgICAgIHZhciBrZXlzOiBhbnlbXSA9IGtleS5zcGxpdChcIl1bXCIpO1xuICAgICAgdmFyIGtleXNfbGFzdDogbnVtYmVyID0ga2V5cy5sZW5ndGggLSAxO1xuXG4gICAgICAvLyBJZiB0aGUgZmlyc3Qga2V5cyBwYXJ0IGNvbnRhaW5zIFsgYW5kIHRoZSBsYXN0IGVuZHMgd2l0aCBdLCB0aGVuIFtdXG4gICAgICAvLyBhcmUgY29ycmVjdGx5IGJhbGFuY2VkLlxuICAgICAgaWYgKC9cXFsvLnRlc3Qoa2V5c1swXSkgJiYgL1xcXSQvLnRlc3Qoa2V5c1trZXlzX2xhc3RdKSkge1xuICAgICAgICAvLyBSZW1vdmUgdGhlIHRyYWlsaW5nIF0gZnJvbSB0aGUgbGFzdCBrZXlzIHBhcnQuXG4gICAgICAgIGtleXNba2V5c19sYXN0XSA9IGtleXNba2V5c19sYXN0XS5yZXBsYWNlKC9cXF0kLywgXCJcIik7XG5cbiAgICAgICAgLy8gU3BsaXQgZmlyc3Qga2V5cyBwYXJ0IGludG8gdHdvIHBhcnRzIG9uIHRoZSBbIGFuZCBhZGQgdGhlbSBiYWNrIG9udG9cbiAgICAgICAgLy8gdGhlIGJlZ2lubmluZyBvZiB0aGUga2V5cyBhcnJheS5cbiAgICAgICAga2V5cyA9IGtleXNcbiAgICAgICAgICAuc2hpZnQoKVxuICAgICAgICAgIC5zcGxpdChcIltcIilcbiAgICAgICAgICAuY29uY2F0KGtleXMpO1xuXG4gICAgICAgIGtleXNfbGFzdCA9IGtleXMubGVuZ3RoIC0gMTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEJhc2ljICdmb28nIHN0eWxlIGtleS5cbiAgICAgICAga2V5c19sYXN0ID0gMDtcbiAgICAgIH1cblxuICAgICAgLy8gQXJlIHdlIGRlYWxpbmcgd2l0aCBhIG5hbWU9dmFsdWUgcGFpciwgb3IganVzdCBhIG5hbWU/XG4gICAgICBpZiAocGFyYW0ubGVuZ3RoID09PSAyKSB7XG4gICAgICAgIHZhbCA9IGRlY29kZVVSSUNvbXBvbmVudChwYXJhbVsxXSk7XG5cbiAgICAgICAgLy8gQ29lcmNlIHZhbHVlcy5cbiAgICAgICAgaWYgKGNvZXJjZSkge1xuICAgICAgICAgIHZhbCA9XG4gICAgICAgICAgICB2YWwgJiYgIWlzTmFOKHZhbCkgJiYgK3ZhbCArIFwiXCIgPT09IHZhbFxuICAgICAgICAgICAgICA/ICt2YWwgLy8gbnVtYmVyXG4gICAgICAgICAgICAgIDogdmFsID09PSBcInVuZGVmaW5lZFwiXG4gICAgICAgICAgICAgID8gdW5kZWZpbmVkIC8vIHVuZGVmaW5lZFxuICAgICAgICAgICAgICA6IGNvZXJjZV90eXBlc1t2YWxdICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgICAgICA/IGNvZXJjZV90eXBlc1t2YWxdIC8vIHRydWUsIGZhbHNlLCBudWxsXG4gICAgICAgICAgICAgICAgOiB2YWw7IC8vIHN0cmluZ1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGtleXNfbGFzdCkge1xuICAgICAgICAgIC8vIENvbXBsZXgga2V5LCBidWlsZCBkZWVwIG9iamVjdCBzdHJ1Y3R1cmUgYmFzZWQgb24gYSBmZXcgcnVsZXM6XG4gICAgICAgICAgLy8gKiBUaGUgJ2N1cicgcG9pbnRlciBzdGFydHMgYXQgdGhlIG9iamVjdCB0b3AtbGV2ZWwuXG4gICAgICAgICAgLy8gKiBbXSA9IGFycmF5IHB1c2ggKG4gaXMgc2V0IHRvIGFycmF5IGxlbmd0aCksIFtuXSA9IGFycmF5IGlmIG4gaXNcbiAgICAgICAgICAvLyAgIG51bWVyaWMsIG90aGVyd2lzZSBvYmplY3QuXG4gICAgICAgICAgLy8gKiBJZiBhdCB0aGUgbGFzdCBrZXlzIHBhcnQsIHNldCB0aGUgdmFsdWUuXG4gICAgICAgICAgLy8gKiBGb3IgZWFjaCBrZXlzIHBhcnQsIGlmIHRoZSBjdXJyZW50IGxldmVsIGlzIHVuZGVmaW5lZCBjcmVhdGUgYW5cbiAgICAgICAgICAvLyAgIG9iamVjdCBvciBhcnJheSBiYXNlZCBvbiB0aGUgdHlwZSBvZiB0aGUgbmV4dCBrZXlzIHBhcnQuXG4gICAgICAgICAgLy8gKiBNb3ZlIHRoZSAnY3VyJyBwb2ludGVyIHRvIHRoZSBuZXh0IGxldmVsLlxuICAgICAgICAgIC8vICogUmluc2UgJiByZXBlYXQuXG4gICAgICAgICAgZm9yICg7IGkgPD0ga2V5c19sYXN0OyBpKyspIHtcbiAgICAgICAgICAgIGtleSA9IGtleXNbaV0gPT09IFwiXCIgPyBjdXIubGVuZ3RoIDoga2V5c1tpXTtcbiAgICAgICAgICAgIGN1ciA9IGN1cltrZXldID0gaSA8IGtleXNfbGFzdCA/IGN1cltrZXldIHx8IChrZXlzW2kgKyAxXSAmJiBpc05hTihrZXlzW2kgKyAxXSkgPyB7fSA6IFtdKSA6IHZhbDtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gU2ltcGxlIGtleSwgZXZlbiBzaW1wbGVyIHJ1bGVzLCBzaW5jZSBvbmx5IHNjYWxhcnMgYW5kIHNoYWxsb3dcbiAgICAgICAgICAvLyBhcnJheXMgYXJlIGFsbG93ZWQuXG5cbiAgICAgICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG9ialtrZXldKSA9PT0gXCJbb2JqZWN0IEFycmF5XVwiKSB7XG4gICAgICAgICAgICAvLyB2YWwgaXMgYWxyZWFkeSBhbiBhcnJheSwgc28gcHVzaCBvbiB0aGUgbmV4dCB2YWx1ZS5cbiAgICAgICAgICAgIG9ialtrZXldLnB1c2godmFsKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHt9Lmhhc093blByb3BlcnR5LmNhbGwob2JqLCBrZXkpKSB7XG4gICAgICAgICAgICAvLyB2YWwgaXNuJ3QgYW4gYXJyYXksIGJ1dCBzaW5jZSBhIHNlY29uZCB2YWx1ZSBoYXMgYmVlbiBzcGVjaWZpZWQsXG4gICAgICAgICAgICAvLyBjb252ZXJ0IHZhbCBpbnRvIGFuIGFycmF5LlxuICAgICAgICAgICAgb2JqW2tleV0gPSBbb2JqW2tleV0sIHZhbF07XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIC8vIHZhbCBpcyBhIHNjYWxhci5cbiAgICAgICAgICAgIG9ialtrZXldID0gdmFsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChrZXkpIHtcbiAgICAgICAgLy8gTm8gdmFsdWUgd2FzIGRlZmluZWQsIHNvIHNldCBzb21ldGhpbmcgbWVhbmluZ2Z1bC5cbiAgICAgICAgb2JqW2tleV0gPSBjb2VyY2UgPyB1bmRlZmluZWQgOiBcIlwiO1xuICAgICAgfVxuICAgIH0pO1xuXG4gIHJldHVybiBvYmo7XG59O1xuXG4vKipcbiAqIGNvcHkgZnJvbSBodHRwczovL2dpdGh1Yi5jb20va25vd2xlZGdlY29kZS9qcXVlcnktcGFyYW1cbiAqL1xuY29uc3QgcGFyYW0gPSAoYTogYW55KTogc3RyaW5nID0+IHtcbiAgdmFyIHM6IHN0cmluZ1tdID0gW107XG4gIHZhciBhZGQgPSBmdW5jdGlvbihrOiBzdHJpbmcsIHY6IGFueSkge1xuICAgIHYgPSB0eXBlb2YgdiA9PT0gXCJmdW5jdGlvblwiID8gdigpIDogdjtcbiAgICB2ID0gdiA9PT0gbnVsbCA/IFwiXCIgOiB2ID09PSB1bmRlZmluZWQgPyBcIlwiIDogdjtcbiAgICBzW3MubGVuZ3RoXSA9IGVuY29kZVVSSUNvbXBvbmVudChrKSArIFwiPVwiICsgZW5jb2RlVVJJQ29tcG9uZW50KHYpO1xuICB9O1xuICB2YXIgYnVpbGRQYXJhbXMgPSBmdW5jdGlvbihwcmVmaXg6IHN0cmluZywgb2JqOiBhbnkpIHtcbiAgICB2YXIgaSwgbGVuLCBrZXk7XG5cbiAgICBpZiAocHJlZml4KSB7XG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShvYmopKSB7XG4gICAgICAgIGZvciAoaSA9IDAsIGxlbiA9IG9iai5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgIGJ1aWxkUGFyYW1zKHByZWZpeCArIFwiW1wiICsgKHR5cGVvZiBvYmpbaV0gPT09IFwib2JqZWN0XCIgJiYgb2JqW2ldID8gaSA6IFwiXCIpICsgXCJdXCIsIG9ialtpXSk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoU3RyaW5nKG9iaikgPT09IFwiW29iamVjdCBPYmplY3RdXCIpIHtcbiAgICAgICAgZm9yIChrZXkgaW4gb2JqKSB7XG4gICAgICAgICAgYnVpbGRQYXJhbXMocHJlZml4ICsgXCJbXCIgKyBrZXkgKyBcIl1cIiwgb2JqW2tleV0pO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBhZGQocHJlZml4LCBvYmopO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheShvYmopKSB7XG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSBvYmoubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgYWRkKG9ialtpXS5uYW1lLCBvYmpbaV0udmFsdWUpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBmb3IgKGtleSBpbiBvYmopIHtcbiAgICAgICAgYnVpbGRQYXJhbXMoa2V5LCBvYmpba2V5XSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBzO1xuICB9O1xuXG4gIHJldHVybiBidWlsZFBhcmFtcyhcIlwiLCBhKS5qb2luKFwiJlwiKTtcbn07XG5cbmNsYXNzIFV0aWxzIHtcbiAgdG9BcnJheTxUPihhbnk6IG51bGwgfCB1bmRlZmluZWQgfCBUIHwgVFtdKTogVFtdIHtcbiAgICByZXR1cm4gQXJyYXkuaXNBcnJheShhbnkpID8gYW55IDogYW55ID09IG51bGwgPyBbXSA6IFthbnldO1xuICB9XG5cbiAgdGltZXM8VD4oZWxlbWVudDogVCwgdGltZXM/OiBudW1iZXIpOiBUW10ge1xuICAgIGNvbnN0IHJldCA9IG5ldyBBcnJheTxUPih0aW1lcyB8fCAxKTtcbiAgICByZXQuZmlsbChlbGVtZW50LCAwLCByZXQubGVuZ3RoKTtcbiAgICByZXR1cm4gcmV0O1xuICB9XG5cbiAgdHJ1bmNhdGVBc0RhdGUoZGF0ZTogc3RyaW5nKTogc3RyaW5nIHtcbiAgICByZXR1cm4gZGF0ZSA/IGRhdGUuc3Vic3RyaW5nKDAsIDExKSA6IGRhdGU7XG4gIH1cblxuICBmb3JtYXRBbW91bnQoY3VycmVuY3k/OiBudW1iZXIgfCBzdHJpbmcpIHtcbiAgICBpZiAoY3VycmVuY3kgIT0gbnVsbCkge1xuICAgICAgcmV0dXJuIChjdXJyZW5jeSArIFwiXCIpLnRvU3RyaW5nKCkucmVwbGFjZSgvKFxcZCkoPz0oXFxkXFxkXFxkKSsoPyFcXGQpKS9nLCBcIiQxLFwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICB9XG5cbiAgaXNFbXB0eShvPzogc3RyaW5nIHwgbnVtYmVyIHwgbnVsbCkge1xuICAgIHJldHVybiAhdGhpcy5pc051bWJlcihvKSAmJiAodGhpcy5pc051bGwobykgfHwgdGhpcy5pc1VuZGVmaW5lZChvKSB8fCBvLmxlbmd0aCA9PSAwKTtcbiAgfVxuXG4gIGlzU3RyaW5nKG86IGFueSk6IG8gaXMgc3RyaW5nIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJTdHJpbmdcIjtcbiAgfVxuXG4gIGlzTnVtYmVyKG86IGFueSk6IG8gaXMgbnVtYmVyIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJOdW1iZXJcIjtcbiAgfVxuXG4gIGlzQm9vbGVhbihvOiBhbnkpOiBvIGlzIGJvb2xlYW4ge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpID09PSBcIkJvb2xlYW5cIjtcbiAgfVxuXG4gIGlzRnVuY3Rpb24obzogYW55KTogbyBpcyBGdW5jdGlvbiB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvKS5zbGljZSg4LCAtMSkgPT09IFwiRnVuY3Rpb25cIjtcbiAgfVxuXG4gIGlzTnVsbChvOiBhbnkpOiBvIGlzIG51bGwge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpID09PSBcIk51bGxcIjtcbiAgfVxuXG4gIGlzVW5kZWZpbmVkKG86IGFueSk6IG8gaXMgdW5kZWZpbmVkIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJVbmRlZmluZWRcIjtcbiAgfVxuXG4gIGlzT2JqZWN0KG86IGFueSk6IG8gaXMgb2JqZWN0IHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJPYmplY3RcIjtcbiAgfVxuXG4gIGlzQXJyYXkobzogYW55KTogbyBpcyBhbnlbXSB7XG4gICAgcmV0dXJuIEFycmF5LmlzQXJyYXkobyk7XG4gIH1cblxuICBpc0RhdGUobzogYW55KTogbyBpcyBEYXRlIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJEYXRlXCI7XG4gIH1cblxuICBpc1JlZ0V4cChvOiBhbnkpOiBvIGlzIFJlZ0V4cCB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvKS5zbGljZSg4LCAtMSkgPT09IFwiUmVnRXhwXCI7XG4gIH1cblxuICBpc0Vycm9yKG86IGFueSk6IG8gaXMgRXJyb3Ige1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpID09PSBcIkVycm9yXCI7XG4gIH1cblxuICBpc1N5bWJvbChvOiBhbnkpOiBvIGlzIFN5bWJvbCB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvKS5zbGljZSg4LCAtMSkgPT09IFwiU3ltYm9sXCI7XG4gIH1cblxuICBpc1Byb21pc2UobzogYW55KTogbyBpcyBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpID09PSBcIlByb21pc2VcIjtcbiAgfVxuXG4gIGlzU2V0KG86IGFueSk6IG8gaXMgU2V0PGFueT4ge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpID09PSBcIlNldFwiO1xuICB9XG5cbiAgaXNGb3JtRGF0YShvOiBhbnkpOiBvIGlzIEZvcm1EYXRhIHtcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKSA9PT0gXCJGb3JtRGF0YVwiO1xuICB9XG5cbiAgaXNGYWxzZShvOiBhbnkpOiBvIGlzIGZhbHNlIHtcbiAgICByZXR1cm4gIW8gfHwgbyA9PT0gXCJudWxsXCIgfHwgbyA9PT0gXCJ1bmRlZmluZWRcIiB8fCBvID09PSBcImZhbHNlXCIgfHwgbyA9PT0gXCJOYU5cIjtcbiAgfVxuXG4gIGlzRmFsc2VOb3QwKG86IGFueSk6IG8gaXMgZmFsc2Uge1xuICAgIHJldHVybiAhbyB8fCBvID09PSBcIm51bGxcIiB8fCBvID09PSBcInVuZGVmaW5lZFwiIHx8IG8gPT09IFwiZmFsc2VcIiB8fCBvID09PSBcIk5hTlwiIHx8IG8gPT09IDA7XG4gIH1cblxuICBpc1RydWUobzogYW55KTogbyBpcyB0cnVlIHtcbiAgICByZXR1cm4gIXRoaXMuaXNGYWxzZShvKTtcbiAgfVxuXG4gIGZyb21RdWVyeVN0cmluZyhxcz86IHN0cmluZyB8IG51bGwpOiBRUyB8IGFueSB7XG4gICAgaWYgKCF0aGlzLmlzTnVsbChxcykgJiYgIXRoaXMuaXNVbmRlZmluZWQocXMpKSB7XG4gICAgICByZXR1cm4gZGVwYXJhbShxcyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnN0IHNlYXJjaCA9IHdpbmRvdy5sb2NhdGlvbi5zZWFyY2g7XG4gICAgICBpZiAoc2VhcmNoICYmIHNlYXJjaCAhPT0gXCI/XCIpIHtcbiAgICAgICAgcmV0dXJuIGRlcGFyYW0oc2VhcmNoLnN1YnN0cmluZygxKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4ge307XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdG9RdWVyeVN0cmluZyhvYmo6IGFueSk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHBhcmFtKG9iaik7XG4gIH1cblxuICBlbmNvZGVDdXJyZW50VVJJKHFzPzogc3RyaW5nIHwgbnVsbCk6IHN0cmluZyB7XG4gICAgaWYgKHRoaXMuaXNOdWxsKHFzKSB8fCB0aGlzLmlzVW5kZWZpbmVkKHFzKSkge1xuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudCh3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChgJHt3aW5kb3cubG9jYXRpb24ub3JpZ2lufSR7d2luZG93LmxvY2F0aW9uLnBhdGhuYW1lfT8ke3FzfWApO1xuICAgIH1cbiAgfVxuXG4gIGNvcHlUZXh0VG9DbGlwYm9hcmQoczogc3RyaW5nKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IHRleHRhcmVhID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInRleHRhcmVhXCIpO1xuICAgICAgLy8gdGV4dGFyZWEuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICAgIHRleHRhcmVhLnN0eWxlLnBvc2l0aW9uID0gXCJmaXhlZFwiO1xuICAgICAgdGV4dGFyZWEuc3R5bGUuaGVpZ2h0ID0gXCIwXCI7XG4gICAgICB0ZXh0YXJlYS5zdHlsZS50b3AgPSBcIi05OTk5cHhcIjtcbiAgICAgIHRleHRhcmVhLnZhbHVlID0gcztcbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGV4dGFyZWEpO1xuICAgICAgdGV4dGFyZWEuc2VsZWN0KCk7XG4gICAgICB0ZXh0YXJlYS5zZXRTZWxlY3Rpb25SYW5nZSgwLCBzLmxlbmd0aCk7XG4gICAgICBjb25zdCByZXQgPSBkb2N1bWVudC5leGVjQ29tbWFuZChcImNvcHlcIik7XG4gICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRleHRhcmVhKTtcbiAgICAgIGlmIChyZXQpIHtcbiAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmVqZWN0KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBjb252ZXJ0VG9Nb21lbnQodmFsdWU6IGFueSk6IG1vbWVudC5Nb21lbnQge1xuICAgIGlmIChtb21lbnQuaXNNb21lbnQodmFsdWUpKSB7XG4gICAgICByZXR1cm4gdmFsdWU7XG4gICAgfVxuXG4gICAgaWYgKHZhbHVlID09IG51bGwpIHtcbiAgICAgIHJldHVybiBtb21lbnQuaW52YWxpZCgpO1xuICAgIH1cblxuICAgIHJldHVybiBtb21lbnQodmFsdWUsIERBVEVfRk9STUFULkRBVEVfVElNRV9XSVRIX1RJTUVfWk9ORSk7XG4gIH1cblxuICBkYXRlRnJvbU5vdyh2YWx1ZTogYW55KTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5jb252ZXJ0VG9Nb21lbnQodmFsdWUpLmZyb21Ob3coKTtcbiAgfVxuXG4gIHBhcnNlRGF0ZSh2YWx1ZTogYW55KTogRGF0ZSB7XG4gICAgcmV0dXJuIHRoaXMuY29udmVydFRvTW9tZW50KHZhbHVlKS50b0RhdGUoKTtcbiAgfVxuXG4gIGRhdGVUaW1lRnJvbU5vdyh2YWx1ZTogYW55KTogc3RyaW5nIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbnZlcnRUb01vbWVudCh2YWx1ZSkuZnJvbU5vdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gIH1cblxuICBmb3JtYXREYXRlKHZhbHVlOiBhbnksIGZvcm1hdFN0ciA9IERBVEVfRk9STUFULkRBVEVfRk9STUFUKTogc3RyaW5nIHtcbiAgICByZXR1cm4gbW9tZW50KHZhbHVlLCBEQVRFX0ZPUk1BVC5EQVRFX0ZPUk1BVCkuZm9ybWF0KGZvcm1hdFN0cik7XG4gIH1cblxuICBmb3JtYXREYXRlVGltZSh2YWx1ZTogYW55LCBmb3JtYXRTdHIgPSBEQVRFX0ZPUk1BVC5EQVRFX1RJTUVfRk9STUFUKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5jb252ZXJ0VG9Nb21lbnQodmFsdWUpLmZvcm1hdChmb3JtYXRTdHIpO1xuICB9XG5cbiAgc3RyaW5naWZ5RGF0ZSh2YWx1ZTogYW55KTogc3RyaW5nIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbnZlcnRUb01vbWVudCh2YWx1ZSkuZm9ybWF0KERBVEVfRk9STUFULkRBVEVfVElNRV9XSVRIX1RJTUVfWk9ORSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIFwiXCI7XG4gIH1cblxuICB0b1Rob3VzYW5kcyhudW06IGFueSkge1xuICAgIG51bSA9IG51bSArIFwiXCI7XG4gICAgaWYgKCFudW0uaW5jbHVkZXMoXCIuXCIpKSB7XG4gICAgICBudW0gKz0gXCIuXCI7XG4gICAgfVxuICAgIHJldHVybiBudW1cbiAgICAgIC5yZXBsYWNlKC8oXFxkKSg/PShcXGR7M30pK1xcLikvZywgZnVuY3Rpb24oJDA6IGFueSwgJDE6IGFueSkge1xuICAgICAgICByZXR1cm4gJDEgKyBcIixcIjtcbiAgICAgIH0pXG4gICAgICAucmVwbGFjZSgvXFwuJC8sIFwiXCIpO1xuICB9XG5cbiAgdG9GbG9hdChudW06IGFueSkge1xuICAgIGlmICghbnVtKSByZXR1cm4gXCJcIjtcbiAgICByZXR1cm4gU3RyaW5nKG51bSkucmVwbGFjZSgvLC9naSwgXCJcIik7XG4gIH1cblxuICBpc1Rob3VzYW5kcyhudW06IGFueSkge1xuICAgIGlmICghbnVtKSByZXR1cm4gZmFsc2U7XG4gICAgaWYgKFN0cmluZyhudW0pLmluZGV4T2YoXCIsXCIpID4gMCkgcmV0dXJuIHRydWU7XG4gIH1cblxuICBzZXRUaG91c2FuZHNWYWx1ZSh2YWx1ZTogYW55KSB7XG4gICAgbGV0IGlzVGhvdXNhbmRzOiBib29sZWFuIHwgdW5kZWZpbmVkID0gdGhpcy5pc1Rob3VzYW5kcyh2YWx1ZSk7XG4gICAgbGV0IHZhbHVlVGhvdXNhbmRzOiBhbnkgPSBpc1Rob3VzYW5kc1xuICAgICAgPyB0aGlzLnRvVGhvdXNhbmRzKHBhcnNlRmxvYXQodGhpcy50b0Zsb2F0KHZhbHVlKSkudG9GaXhlZCgyKSlcbiAgICAgIDogdGhpcy50b1Rob3VzYW5kcyhwYXJzZUZsb2F0KHZhbHVlKS50b0ZpeGVkKDIpKTtcbiAgICByZXR1cm4gdmFsdWVUaG91c2FuZHM7XG4gIH1cblxuICBzZXRGbG9hdFZhbHVlKHZhbHVlOiBhbnkpIHtcbiAgICBsZXQgaXNUaG91c2FuZHM6IGJvb2xlYW4gfCB1bmRlZmluZWQgPSB0aGlzLmlzVGhvdXNhbmRzKHZhbHVlKTtcbiAgICBsZXQgdmFsdWVGbG9hdDogYW55ID0gaXNUaG91c2FuZHNcbiAgICAgID8gcGFyc2VGbG9hdCh0aGlzLnRvRmxvYXQodmFsdWUpKVxuICAgICAgOiBwYXJzZUZsb2F0KHZhbHVlKTtcbiAgICByZXR1cm4gdmFsdWVGbG9hdDtcbiAgfVxuXG4gIGdldENvdW50cnkoKSB7XG4gICAgcmV0dXJuIF8uZ2V0KFN0b3JhZ2UuQWNjb3VudC5zZXNzaW9uKCkuZ2V0KFwiQWNjb3VudFwiKSwgXCJzdHJ1Y3QuY291bnRyeVwiLCBcIlwiKTtcbiAgfVxuXG4gIGZvcm1hdEN1cnJlbmN5SW5mbyhhbW91bnQ6IGFueSwgY3VycmVuY3lDb2RlOiBhbnksIGRpZ2l0czogbnVtYmVyID0gMik6IHsgYW1vdW50OiBhbnk7IGN1cnJlbmN5VGV4dDogYW55IH0ge1xuICAgIGNvbnN0IGN1cnJlbmN5VGV4dCA9ICFjdXJyZW5jeUNvZGUgPyBcIlwiIDogQ1VSUkVOQ1lfU1lNQk9MX1RBQkxFW2N1cnJlbmN5Q29kZV07XG4gICAgbGV0IGZvcm1hdCA9IFwiMCwwXCI7XG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IGRpZ2l0czsgaW5kZXgrKykge1xuICAgICAgaWYgKGluZGV4ID09PSAwKSB7XG4gICAgICAgIGZvcm1hdCA9IGZvcm1hdCArIFwiLjBcIjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZvcm1hdCA9IGZvcm1hdCArIFwiMFwiO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAoIWFtb3VudCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgYW1vdW50OiBcIjAuMDBcIixcbiAgICAgICAgY3VycmVuY3lUZXh0LFxuICAgICAgfTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIGFtb3VudDogdGhpcy50b1Rob3VzYW5kcyhhbW91bnQudG9GaXhlZCgyKSksXG4gICAgICBjdXJyZW5jeVRleHQsXG4gICAgfTtcbiAgfVxuXG4gIGZvcm1hdEN1cnJlbmN5KGFtb3VudDogYW55LCBjdXJyZW5jeUNvZGU/OiBhbnksIGRpZ2l0cyA9IDIpOiBzdHJpbmcge1xuICAgIGNvbnN0IGN1cnJlbmN5VGV4dCA9ICFjdXJyZW5jeUNvZGUgPyBcIlwiIDogQ1VSUkVOQ1lfU1lNQk9MX1RBQkxFW2N1cnJlbmN5Q29kZV07XG4gICAgY29uc3QgbmVnYXRpdmVTaWduID0gYW1vdW50ICYmIGFtb3VudCA8IDAgPyBcIi1cIiA6IFwiXCI7XG4gICAgbGV0IGZvcm1hdCA9IFwiMCwwXCI7XG4gICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IGRpZ2l0czsgaW5kZXgrKykge1xuICAgICAgaWYgKGluZGV4ID09PSAwKSB7XG4gICAgICAgIGZvcm1hdCA9IGZvcm1hdCArIFwiLjBcIjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZvcm1hdCA9IGZvcm1hdCArIFwiMFwiO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gYCR7bmVnYXRpdmVTaWdufSR7Y3VycmVuY3lUZXh0fSR7dGhpcy50b1Rob3VzYW5kcyhNYXRoLmFicyhhbW91bnQpLnRvRml4ZWQoMikpfWA7XG4gIH1cblxuICBmb3JtYXRDdXJyZW5jeUh0bWwoYW1vdW50OiBhbnksIGN1cnJlbmN5Q29kZT86IGFueSwgc3R5bGU/OiBhbnksIGRpZ2l0cyA9IDIpIHtcbiAgICBjb25zdCBjdXJyZW5jeVRleHQgPSAhY3VycmVuY3lDb2RlID8gXCJcIiA6IENVUlJFTkNZX1NZTUJPTF9UQUJMRVtjdXJyZW5jeUNvZGVdO1xuICAgIGNvbnN0IG5lZ2F0aXZlU2lnbiA9IGFtb3VudCAmJiBhbW91bnQgPCAwID8gXCItXCIgOiBcIlwiO1xuICAgIGxldCBmb3JtYXQgPSBcIjAsMFwiO1xuICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBkaWdpdHM7IGluZGV4KyspIHtcbiAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyBcIi4wXCI7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyBcIjBcIjtcbiAgICAgIH1cbiAgICB9XG4gICAgY29uc3Qgc3R5bGVPYmogPSAhc3R5bGUgPyB7XG4gICAgICBjb2xvcjogXCJyZ2JhKDAsMCwwLDAuODUpXCIsXG4gICAgfSA6IHN0eWxlO1xuICAgIHJldHVybiA8bGFiZWwgc3R5bGU9e3N0eWxlT2JqfT5cbiAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgIGZvbnRTaXplOiBcIjgwJVwiLFxuICAgICAgICBmb250V2VpZ2h0OiA0MDAsXG4gICAgICB9fT5cbiAgICAgIHtgJHtuZWdhdGl2ZVNpZ259JHtjdXJyZW5jeVRleHR9YH1cbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgIGZvbnRTaXplOiBcIjE2cHhcIixcbiAgICAgICAgcGFkZGluZ0xlZnQ6IFwiNXB4XCIsXG4gICAgICAgIGZvbnRXZWlnaHQ6IDUwMCxcbiAgICAgIH19PlxuICAgICAgICB7YCR7dGhpcy50b1Rob3VzYW5kcyhNYXRoLmFicyhhbW91bnQpLnRvRml4ZWQoMikpfWB9XG4gICAgICA8L3NwYW4+XG4gICAgPC9sYWJlbD47XG4gIH1cblxuICBmb3JtYXRDdXJyZW5jeUh0bWxOdW1iZXIoYW1vdW50OiBhbnksIGN1cnJlbmN5Q29kZT86IGFueSwgc3R5bGU/OiBhbnksIGRpZ2l0cyA9IDIpIHtcbiAgICBjb25zdCBjdXJyZW5jeVRleHQgPSAhY3VycmVuY3lDb2RlID8gXCJcIiA6IENVUlJFTkNZX1NZTUJPTF9UQUJMRVtjdXJyZW5jeUNvZGVdO1xuICAgIGNvbnN0IG5lZ2F0aXZlU2lnbiA9IGFtb3VudCAmJiBhbW91bnQgPCAwID8gXCItXCIgOiBcIlwiO1xuICAgIGxldCBmb3JtYXQgPSBcIjAsMFwiO1xuICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBkaWdpdHM7IGluZGV4KyspIHtcbiAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyBcIi4wXCI7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyBcIjBcIjtcbiAgICAgIH1cbiAgICB9XG4gICAgY29uc3Qgc3R5bGVPYmogPSAhc3R5bGUgPyB7XG4gICAgICBjb2xvcjogXCJyZ2JhKDAsMCwwLDAuODUpXCIsXG4gICAgfSA6IHN0eWxlO1xuICAgIHJldHVybiA8bGFiZWwgc3R5bGU9e3N0eWxlT2JqfT5cbiAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgIGZvbnRTaXplOiBcIjgwJVwiLFxuICAgICAgICBmb250V2VpZ2h0OiA0MDAsXG4gICAgICB9fT5cbiAgICAgIHtgJHtuZWdhdGl2ZVNpZ259JHtjdXJyZW5jeVRleHR9YH1cbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxzcGFuIHN0eWxlPXt7XG4gICAgICAgIGZvbnRTaXplOiBcIjE2cHhcIixcbiAgICAgICAgcGFkZGluZ0xlZnQ6IFwiNXB4XCIsXG4gICAgICB9fT5cbiAgICAgICAge2Ake3RoaXMudG9UaG91c2FuZHMoTWF0aC5hYnMoXy50b051bWJlcihhbW91bnQpKSl9YH1cbiAgICAgIDwvc3Bhbj5cbiAgICA8L2xhYmVsPjtcbiAgfVxuXG4gIGdldFZhbHVlRnJvbUpTT04oanNvbk9iamVjdDogYW55ID0ge30sIGlkOiBzdHJpbmcpIHtcbiAgICBpZiAoaWQuaW5kZXhPZihcIi5cIikgPT09IC0xKSByZXR1cm4ganNvbk9iamVjdFtpZF07XG4gICAgdmFyIGlkcyA9IGlkLnNwbGl0KFwiLlwiKTtcbiAgICB2YXIgcGFyZW50ID0ganNvbk9iamVjdDtcbiAgICB2YXIgdmFsdWUgPSBudWxsO1xuICAgIHZhciB2YWx1ZXMgPSBpZHMubWFwKGZ1bmN0aW9uKGlkKSB7XG4gICAgICBpZiAocGFyZW50ID09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YWx1ZSA9IHBhcmVudFtpZF07XG4gICAgICAgIHBhcmVudCA9IHZhbHVlO1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIHZhbHVlc1t2YWx1ZXMubGVuZ3RoIC0gMV07XG4gIH1cblxuICBjaGVja1BsYW5QcmVtaXVtSXNaZXJvKHBsYW46IGFueSkge1xuICAgIGxldCBsdW1wc3VtQXIgPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocGxhbiwgXCJwb2xpY3lQcmVtaXVtLmx1bXBzdW0uYXJcIikgfHwgMDtcbiAgICAvLyDliIbmnJ/ku5jmrL7vvIzkuI3mraLmmK/nlKjkuo7mnIjku5hcbiAgICBsZXQgc3Vic2VxdWVudEFyID0gdGhpcy5nZXRWYWx1ZUZyb21KU09OKHBsYW4sIFwicG9saWN5UHJlbWl1bS5zdWJzZXF1ZW50LmFyXCIpIHx8IDA7XG4gICAgcmV0dXJuIGx1bXBzdW1BciA9PT0gMCAmJiBzdWJzZXF1ZW50QXIgPT09IDA7XG4gIH1cblxuICBnZXRQb2xpY3lQcmVtaXVtKHBvbGljeTogYW55ID0ge30sIHBheW1lbnRTY2hlZHVsZTogYW55ID0gbnVsbCkge1xuICAgIGNvbnN0IHsgcmVjdXJyaW5nUGF5bWVudCB9ID0gcG9saWN5O1xuICAgIGxldCBsdW1wc3VtID0gdGhpcy5nZXRWYWx1ZUZyb21KU09OKHBvbGljeSwgXCJwb2xpY3lQcmVtaXVtLmx1bXBzdW1cIikgfHwge307XG4gICAgbGV0IHN1YnNlcXVlbnQgPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocG9saWN5LCBcInBvbGljeVByZW1pdW0uc3Vic2VxdWVudFwiKSB8fCB7fTtcbiAgICBpZiAocmVjdXJyaW5nUGF5bWVudCB8fCBwYXltZW50U2NoZWR1bGUgPT09IFBBWU1FTlRfTUVUSE9EX1JFQ1VSUklORykge1xuICAgICAgcmV0dXJuIHN1YnNlcXVlbnQ7XG4gICAgfVxuICAgIHJldHVybiBsdW1wc3VtO1xuICB9XG5cbiAgZ2V0UG9saWN5UHJlbWl1bVZtaShwb2xpY3k6IGFueSA9IHt9LCBwYXltZW50U2NoZWR1bGU6IGFueSA9IG51bGwsIGNhcnRQcmVtaXVtOiBhbnkpIHtcbiAgICBjb25zdCB7IG9wZW5FbmQsIHJlY3VycmluZ1BheW1lbnQgfSA9IHBvbGljeTtcbiAgICBsZXQgcG9saWN5Q29weSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkocG9saWN5IHx8IHt9KSk7XG4gICAgcG9saWN5Q29weS5wb2xpY3lQcmVtaXVtID0gY2FydFByZW1pdW07XG4gICAgbGV0IGx1bXBzdW0gPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocG9saWN5Q29weSwgXCJwb2xpY3lQcmVtaXVtLmx1bXBzdW1cIikgfHwge307XG4gICAgbGV0IHN1YnNlcXVlbnQgPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocG9saWN5Q29weSwgXCJwb2xpY3lQcmVtaXVtLnN1YnNlcXVlbnRcIikgfHwge307XG4gICAgaWYgKG9wZW5FbmQgfHwgcmVjdXJyaW5nUGF5bWVudCB8fCBwYXltZW50U2NoZWR1bGUgPT09IFBBWU1FTlRfTUVUSE9EX1JFQ1VSUklORykge1xuICAgICAgcmV0dXJuIHN1YnNlcXVlbnQ7XG4gICAgfVxuICAgIHJldHVybiBsdW1wc3VtO1xuICB9XG5cbiAgLy90b2RvXG4gIGdldFBvbGljeVByZW1pdW0xKHBvbGljeTogYW55ID0ge30sIHBheW1lbnRTY2hlZHVsZTogYW55ID0gbnVsbCkge1xuICAgIGNvbnN0IHsgb3BlbkVuZCwgcmVjdXJyaW5nUGF5bWVudCB9ID0gcG9saWN5O1xuICAgIGxldCBsdW1wc3VtID0gdGhpcy5nZXRWYWx1ZUZyb21KU09OKHBvbGljeSwgXCJsdW1wc3VtXCIpIHx8IHt9O1xuICAgIGxldCBzdWJzZXF1ZW50ID0gdGhpcy5nZXRWYWx1ZUZyb21KU09OKHBvbGljeSwgXCJzdWJzZXF1ZW50XCIpIHx8IHt9O1xuICAgIGlmIChvcGVuRW5kIHx8IHJlY3VycmluZ1BheW1lbnQgfHwgcGF5bWVudFNjaGVkdWxlID09PSBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkcpIHtcbiAgICAgIHJldHVybiBzdWJzZXF1ZW50O1xuICAgIH1cbiAgICByZXR1cm4gbHVtcHN1bTtcbiAgfVxuXG4gIHJlbmRlclBvbGljeVByZW1pdW0xKHBvbGljeTogYW55ID0ge30sIHBheW1lbnRTY2hlZHVsZTogYW55ID0gbnVsbCkge1xuICAgIGNvbnN0IHsgb3BlbkVuZCwgcmVjdXJyaW5nUGF5bWVudCB9ID0gcG9saWN5O1xuICAgIGNvbnN0IHBvbGljeVByZW1pdW0gPSB0aGlzLmdldFBvbGljeVByZW1pdW0xKHBvbGljeSk7XG4gICAgY29uc3QgeyBhbW91bnQsIGN1cnJlbmN5VGV4dCB9ID0gdGhpcy5mb3JtYXRDdXJyZW5jeUluZm8ocG9saWN5UHJlbWl1bS5hciwgcG9saWN5LmN1cnJlbmN5Q29kZSk7XG4gICAgY29uc3QgaXNSZWN1cnJpbmdQYXltZW50ID0gb3BlbkVuZCB8fCByZWN1cnJpbmdQYXltZW50IHx8IHBheW1lbnRTY2hlZHVsZSA9PT0gUEFZTUVOVF9NRVRIT0RfUkVDVVJSSU5HO1xuICAgIC8vIOWIhuacn+S7mOasvu+8jOS4jeatouaYr+eUqOS6juaciOS7mFxuICAgIGNvbnN0IHByZW1pdW1EZXNjID0gcG9saWN5LnByZW1pdW1EZXNjIHx8IFwibW9udGhcIjtcbiAgICByZXR1cm4gKFxuICAgICAgPHNwYW5cbiAgICAgICAgY2xhc3NOYW1lPVwicHJpY2UgcHJpY2UtLWxnXCJcbiAgICAgICAgZGF0YS1wb3N0Zml4PXtpc1JlY3VycmluZ1BheW1lbnQgPyBgLyAke3ByZW1pdW1EZXNjID8gcHJlbWl1bURlc2MgOiBcIm1vbnRoXCJ9YCA6IFwiXCJ9XG4gICAgICAgIGRhdGEtcHJlZml4PXtcIlMkXCJ9XG4gICAgICA+XG4gICAgICAgIHthbW91bnR9XG4gICAgICA8L3NwYW4+XG4gICAgKTtcbiAgfVxuXG4gIHJlbmRlclBvbGljeVByZW1pdW0ocG9saWN5OiBhbnkgPSB7fSwgcGF5bWVudFNjaGVkdWxlOiBhbnkgPSBudWxsKSB7XG4gICAgY29uc3QgeyByZWN1cnJpbmdQYXltZW50IH0gPSBwb2xpY3k7XG4gICAgY29uc3QgcG9saWN5UHJlbWl1bSA9IHRoaXMuZ2V0UG9saWN5UHJlbWl1bShwb2xpY3ksIHBheW1lbnRTY2hlZHVsZSk7XG4gICAgY29uc3QgeyBhbW91bnQsIGN1cnJlbmN5VGV4dCB9ID0gdGhpcy5mb3JtYXRDdXJyZW5jeUluZm8ocG9saWN5UHJlbWl1bS5hciwgcG9saWN5LnByZW1DdXJyZW5jeUNvZGUgfHwgcG9saWN5LmN1cnJlbmN5Q29kZSk7XG4gICAgY29uc3QgaXNSZWN1cnJpbmdQYXltZW50ID0gcmVjdXJyaW5nUGF5bWVudCB8fCBwYXltZW50U2NoZWR1bGUgPT09IFBBWU1FTlRfTUVUSE9EX1JFQ1VSUklORztcbiAgICAvLyDliIbmnJ/ku5jmrL7vvIzkuI3mraLmmK/nlKjkuo7mnIjku5hcbiAgICBjb25zdCBwcmVtaXVtRGVzYyA9IHBvbGljeS5wcmVtaXVtRGVzYyB8fCBcIm1vbnRoXCI7XG4gICAgcmV0dXJuIChcbiAgICAgIDxzcGFuXG4gICAgICAgIGNsYXNzTmFtZT1cInByaWNlIHByaWNlLS1sZ1wiXG4gICAgICAgIGRhdGEtcG9zdGZpeD17aXNSZWN1cnJpbmdQYXltZW50ID8gYC8gJHtwcmVtaXVtRGVzYyA/IHByZW1pdW1EZXNjIDogXCJtb250aFwifWAgOiBcIlwifVxuICAgICAgICBkYXRhLXByZWZpeD17Y3VycmVuY3lUZXh0fVxuICAgICAgPlxuICAgICAgICB7YW1vdW50fVxuICAgICAgPC9zcGFuPlxuICAgICk7XG4gIH1cblxuICByZW5kZXJEb3ducGF5bWVudFByZW1pdW0ocG9saWN5OiBhbnkgPSB7fSkge1xuICAgIGxldCBhciA9IHRoaXMuZ2V0VmFsdWVGcm9tSlNPTihwb2xpY3ksIFwicG9saWN5UHJlbWl1bS5kb3ducGF5bWVudC5hclwiKTtcbiAgICBjb25zdCB7IGFtb3VudCwgY3VycmVuY3lUZXh0IH0gPSB0aGlzLmZvcm1hdEN1cnJlbmN5SW5mbyhhciwgcG9saWN5LmN1cnJlbmN5Q29kZSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInByaWNlIHByaWNlLS1sZ1wiIGRhdGEtcHJlZml4PXtjdXJyZW5jeVRleHR9PlxuICAgICAgICB7YW1vdW50fVxuICAgICAgPC9zcGFuPlxuICAgICk7XG4gIH1cblxuICBnZXRQb2xpY3lQcmVtaXVtQmVmb3JlQWZ0ZXIocG9saWN5OiBhbnkgPSB7fSwgcG9saWN5UHJlbWl1bURhdGFGaXhlZDogc3RyaW5nLCBwYXltZW50U2NoZWR1bGU6IGFueSA9IG51bGwpIHtcbiAgICBjb25zdCB7IHJlY3VycmluZ1BheW1lbnQsIHByZW1pdW1EZXNjIH0gPSBwb2xpY3k7XG4gICAgbGV0IGx1bXBzdW0gPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocG9saWN5LCBgJHtwb2xpY3lQcmVtaXVtRGF0YUZpeGVkfS5sdW1wc3VtYCkgfHwge307XG4gICAgbGV0IHN1YnNlcXVlbnQgPSB0aGlzLmdldFZhbHVlRnJvbUpTT04ocG9saWN5LCBgJHtwb2xpY3lQcmVtaXVtRGF0YUZpeGVkfS5zdWJzZXF1ZW50YCkgfHwge307XG4gICAgaWYgKHJlY3VycmluZ1BheW1lbnQgfHwgcHJlbWl1bURlc2MgfHwgcGF5bWVudFNjaGVkdWxlID09PSBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkcpIHtcbiAgICAgIHJldHVybiBzdWJzZXF1ZW50O1xuICAgIH1cbiAgICByZXR1cm4gbHVtcHN1bTtcbiAgfVxuXG4gIHJlbmRlclBvbGljeVByZW1pdW1CZWZvcmVBZnRlcihwb2xpY3k6IGFueSA9IHt9LCBwb2xpY3lQcmVtaXVtRGF0YUZpeGVkOiBzdHJpbmcsIHBheW1lbnRTY2hlZHVsZTogYW55ID0gbnVsbCkge1xuICAgIGNvbnN0IHsgcmVjdXJyaW5nUGF5bWVudCB9ID0gcG9saWN5O1xuICAgIGNvbnN0IHBvbGljeVByZW1pdW0gPSB0aGlzLmdldFBvbGljeVByZW1pdW1CZWZvcmVBZnRlcihwb2xpY3ksIHBvbGljeVByZW1pdW1EYXRhRml4ZWQsIHBheW1lbnRTY2hlZHVsZSk7XG4gICAgY29uc3QgeyBhbW91bnQsIGN1cnJlbmN5VGV4dCB9ID0gdGhpcy5mb3JtYXRDdXJyZW5jeUluZm8ocG9saWN5UHJlbWl1bS5hciwgcG9saWN5LmN1cnJlbmN5Q29kZSk7XG4gICAgY29uc3QgaXNSZWN1cnJpbmdQYXltZW50ID0gcmVjdXJyaW5nUGF5bWVudCB8fCBwb2xpY3kucHJlbWl1bURlc2MgfHwgcGF5bWVudFNjaGVkdWxlID09PSBQQVlNRU5UX01FVEhPRF9SRUNVUlJJTkc7XG4gICAgLy8g5YiG5pyf5LuY5qy+77yM5LiN5q2i5piv55So5LqO5pyI5LuYXG4gICAgY29uc3QgcHJlbWl1bURlc2MgPSBwb2xpY3kucHJlbWl1bURlc2MgfHwgXCJtb250aFwiO1xuICAgIHJldHVybiAoXG4gICAgICA8c3BhblxuICAgICAgICBjbGFzc05hbWU9XCJwcmljZSBwcmljZS0tbGdcIlxuICAgICAgICBkYXRhLXBvc3RmaXg9e2lzUmVjdXJyaW5nUGF5bWVudCA/IGAvICR7cHJlbWl1bURlc2MgPyBwcmVtaXVtRGVzYyA6IFwibW9udGhcIn1gIDogXCJcIn1cbiAgICAgICAgZGF0YS1wcmVmaXg9e2N1cnJlbmN5VGV4dH1cbiAgICAgID5cbiAgICAgICAge2Ftb3VudH1cbiAgICAgIDwvc3Bhbj5cbiAgICApO1xuICB9XG5cbiAgcmVkaXJlY3RUb1BheVBhZ2UoaHRtbDogYW55KTogdm9pZCB7XG4gICAgY29uc3Qgd3JhcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgd3JhcC5pbm5lckhUTUwgPSBodG1sO1xuICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQod3JhcCk7XG4gICAgd3JhcC5xdWVyeVNlbGVjdG9yKFwiZm9ybVwiKSEuc3VibWl0KCk7XG4gIH1cblxuICBkYXRlVGltZUluaXQodmFsdWU6IGFueSkge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgY29uc3QgbVZhbHVlID0gbW9tZW50KHZhbHVlLCBDb25zdHMuREFURV9GT1JNQVQuREFURV9USU1FX1dJVEhfVElNRV9aT05FKTtcbiAgICAgIHJldHVybiBtVmFsdWUudG9EYXRlKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBuZXcgRGF0ZSgpO1xuICAgIH1cbiAgfVxuXG4gIHRvRGF0ZVN0cmluZyh2YWx1ZTogYW55KSB7XG4gICAgaWYgKHZhbHVlKSB7XG4gICAgICBjb25zdCBtVmFsdWUgPSBtb21lbnQodmFsdWUsIENvbnN0cy5EQVRFX0ZPUk1BVC5EQVRFX1RJTUVfV0lUSF9USU1FX1pPTkUpO1xuICAgICAgcmV0dXJuIG1WYWx1ZS5mb3JtYXQoQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICB9XG5cbiAgdG9UaW1lU3RyaW5nKHZhbHVlOiBhbnkpIHtcbiAgICBpZiAodmFsdWUpIHtcbiAgICAgIGNvbnN0IG1WYWx1ZSA9IG1vbWVudCh2YWx1ZSwgQ29uc3RzLkRBVEVfRk9STUFULkRBVEVfVElNRV9XSVRIX1RJTUVfWk9ORSk7XG4gICAgICByZXR1cm4gbVZhbHVlLmZvcm1hdChcIkhIOm1tOnNzXCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gIH1cblxuICB0b0RhdGVUaW1lU3RyaW5nKHZhbHVlOiBhbnksIGZvcm1hdCA9IFwiREQvTU0vWVlZWVRISDptbTpzc1pcIikge1xuICAgIGlmICh2YWx1ZSkge1xuICAgICAgY29uc3QgZGF0ZSA9IHR5cGVvZiB2YWx1ZSA9PT0gXCJzdHJpbmdcIiA/IHRoaXMuZGF0ZVRpbWVJbml0KHZhbHVlKSA6IHZhbHVlO1xuXG4gICAgICBjb25zdCBtVmFsdWUgPSBtb21lbnQoZGF0ZSk7XG4gICAgICByZXR1cm4gbVZhbHVlLmZvcm1hdChmb3JtYXQpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gXCJcIjtcbiAgICB9XG4gIH1cblxuICBqb2luRWxlbWVudHMobGlzdDogYW55LCBzZXBhcmF0b3I6IGFueSkge1xuICAgIGlmICghbGlzdCB8fCBsaXN0Lmxlbmd0aCA8PSAxKSB7XG4gICAgICByZXR1cm4gbGlzdDtcbiAgICB9XG5cbiAgICByZXR1cm4gbGlzdFxuICAgICAgLnNsaWNlKDAsIC0xKVxuICAgICAgLnJlZHVjZShmdW5jdGlvbihhY2M6IGFueSwgY3VyOiBhbnksIGluZGV4OiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIGFjYy5jb25jYXQoW2N1ciwgUmVhY3QuY2xvbmVFbGVtZW50KHNlcGFyYXRvciwgeyBrZXk6IGluZGV4IH0pXSk7XG4gICAgICB9LCBbXSlcbiAgICAgIC5jb25jYXQobGlzdC5zbGljZSgtMSkpO1xuICB9XG5cbiAgZ2V0Q3VycmVuY3lUZXh0KGN1cnJlbmN5Q29kZTogYW55KSB7XG4gICAgY29uc3QgY3VycmVuY3lUZXh0ID0gIWN1cnJlbmN5Q29kZSA/IFwiXCIgOiBDb25zdHMuQ1VSUkVOQ1lfU1lNQk9MX1RBQkxFW2N1cnJlbmN5Q29kZV07XG4gICAgcmV0dXJuIGN1cnJlbmN5VGV4dDtcbiAgfVxuXG4gIGlzVGNjQXJyKHByb2R1Y3RDb2RlVXA6IHN0cmluZykge1xuICAgIHJldHVybiBbXCJTQVJQXCIsIFwiVENDXCIsIFwiQVJQS0NcIiwgXCJUV1BTXCJdLmluY2x1ZGVzKHByb2R1Y3RDb2RlVXApO1xuICB9XG5cbiAgaXNTdXJldHlBcnIocHJvZHVjdENvZGVVcDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIFtcIkZXQlwiLCBcIkxCXCIsIFwiUkJcIiwgXCJTQlwiLCBcIlBCXCJdLmluY2x1ZGVzKHByb2R1Y3RDb2RlVXApO1xuICB9XG5cbiAgaXNMaWFBcnIocHJvZHVjdENvZGVVcDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIFtcIlBVQkxcIiwgXCJQSVwiLCBcIlBMXCIsIFwiRE9MXCIsIFwiRUxcIl0uaW5jbHVkZXMocHJvZHVjdENvZGVVcCk7XG4gIH1cblxuICBpc0ViKHByb2R1Y3RDb2RlVXA6IHN0cmluZykge1xuICAgIHJldHVybiBbXCJXSUNVTlwiLCBcIldJQ05cIl0uaW5jbHVkZXMocHJvZHVjdENvZGVVcCk7XG4gIH1cblxuICBpc0xpZmUocHJvZHVjdENvZGVVcDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIFtcIkZNQ0FcIiwgXCJFTkVMXCIsIFwiSFBSVFwiLCBcIldMU0NcIl0uaW5jbHVkZXMocHJvZHVjdENvZGVVcCk7XG4gIH1cblxuXG4gIGlzVGNjU3VyZXR5TGlhKHByb2R1Y3RDb2RlVXA6IHN0cmluZykge1xuICAgIHJldHVybiB0aGlzLmlzVGNjQXJyKHByb2R1Y3RDb2RlVXApIHx8IHRoaXMuaXNTdXJldHlBcnIocHJvZHVjdENvZGVVcCkgfHwgdGhpcy5pc0xpYUFycihwcm9kdWN0Q29kZVVwKSB8fCB0aGlzLmlzRWIocHJvZHVjdENvZGVVcCk7XG4gIH1cblxuICBnZXRQYXJhbXNGcm9tVXJsKHNlYXJjaCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmKSB7XG4gICAgY29uc3QgcGFyYW1zU3RyID0gc2VhcmNoLnNwbGl0KFwiP1wiKVsxXTtcbiAgICBpZiAocGFyYW1zU3RyKSB7XG4gICAgICBjb25zdCBwYXJhbXNBcnIgPSBwYXJhbXNTdHIuc3BsaXQoXCImXCIpO1xuICAgICAgY29uc3Qgb2JqOiBhbnkgPSB7fTtcblxuICAgICAgcGFyYW1zQXJyLmZvckVhY2gocCA9PiB7XG4gICAgICAgIGNvbnN0IGtleVZhbHVlID0gcC5zcGxpdChcIj1cIik7XG5cbiAgICAgICAgb2JqW2tleVZhbHVlWzBdXSA9IGtleVZhbHVlWzFdO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gb2JqO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGdldElzTW9iaWxlKCkge1xuICAgIGlmICh0aGlzLmdldElzSW5BcHAoKSkgcmV0dXJuIHRydWU7XG4gICAgaWYgKC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8UGxheUJvb2t8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCkpIHtcbiAgICAgIGlmIChkb2N1bWVudC5ib2R5LnNjcm9sbFdpZHRoID49IDc2OCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIGdldElzSW5BcHAoKSB7XG4gICAgcmV0dXJuICh3aW5kb3cgYXMgYW55KS5kZXZpY2UgPT0gXCJhcHBcIjtcbiAgfVxuXG4gIGdldFB1cmVNb2JpbGUoKSB7XG4gICAgcmV0dXJuIC9BbmRyb2lkfHdlYk9TfGlQaG9uZXxpUGFkfGlQb2R8UGxheUJvb2t8QmxhY2tCZXJyeS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XG4gIH1cblxuICBnZXRXaGl0aCgpIHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGgpIHtcbiAgICAgIHJldHVybiB3aW5kb3cuaW5uZXJXaWR0aDtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGRvY3VtZW50LmNvbXBhdE1vZGUgPT0gXCJDU1MxQ29tcGF0XCIpIHtcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRXaWR0aDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBkb2N1bWVudC5ib2R5LmNsaWVudFdpZHRoO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGdldEhlaWdodCgpIHtcbiAgICBpZiAod2luZG93LmlubmVySGVpZ2h0KSB7XG4gICAgICByZXR1cm4gd2luZG93LmlubmVySGVpZ2h0O1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAoZG9jdW1lbnQuY29tcGF0TW9kZSA9PSBcIkNTUzFDb21wYXRcIikge1xuICAgICAgICByZXR1cm4gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBkb2N1bWVudC5ib2R5LmNsaWVudEhlaWdodDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBib2R5U2NhbGUoKSB7XG4gICAgY29uc3Qgc2NhbGUgPSB0aGlzLmdldFdoaXRoKCkgPiAxMjAwIHx8IHRoaXMuZ2V0V2hpdGgoKSA8IDc2OCA/IDEgOiB0aGlzLmdldFdoaXRoKCkgLyAxMjAwO1xuICAgIGxldCBjb250YWluZXI6IGFueSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcm9vdD5kaXZbZGF0YS13aWRnZXQ9J3BhZ2UnXSAucGFnZS1ib2R5XCIpO1xuICAgIGlmIChjb250YWluZXIpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIGNvbnRhaW5lci5jaGlsZE5vZGVzWzBdLnN0eWxlLm1pbkhlaWdodCA9ICh0aGlzLmdldEhlaWdodCgpICogKDEgLyBzY2FsZSkgLSAxOTYgKiBzY2FsZSkgKyBcInB4XCI7XG4gICAgICAgIGNvbnRhaW5lci5jaGlsZE5vZGVzWzBdLnN0eWxlLmJhY2tncm91bmRDb2xvciA9IFwiI2ZmZlwiO1xuICAgICAgfSBjYXRjaCAoZSkge1xuXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgYm9keVNjYWxlQ29udGFpbmVyKCkge1xuICAgIGNvbnN0IHNjYWxlID0gdGhpcy5nZXRXaGl0aCgpID4gMTIwMCB8fCB0aGlzLmdldFdoaXRoKCkgPCA3NjggPyAxIDogdGhpcy5nZXRXaGl0aCgpIC8gMTIwMDtcbiAgICBsZXQgY29udGFpbmVyOiBhbnkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnBhZ2UtYm9keSAuY29udGFpbmVyXCIpO1xuICAgIGlmIChjb250YWluZXIpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIGNvbnRhaW5lci5zdHlsZS5taW5IZWlnaHQgPSAodGhpcy5nZXRIZWlnaHQoKSAqICgxIC8gc2NhbGUpIC0gMTk2ICogc2NhbGUpICsgXCJweFwiO1xuICAgICAgfSBjYXRjaCAoZSkge1xuXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZ2V0TW9udGhFbihtb250aDogbnVtYmVyKSB7XG4gICAgbGV0IG1vbnRoczogYW55W10gPSBbXCJKYW5cIiwgXCJGZWJcIiwgXCJNYXJcIiwgXCJBcHJcIiwgXCJNYXlcIiwgXCJKdW5cIiwgXCJKdWxcIiwgXCJBdWdcIiwgXCJTZXBcIiwgXCJPY3RcIiwgXCJOb3ZcIiwgXCJEZWNcIl07XG4gICAgaWYgKCFtb250aCAmJiBtb250aCAhPT0gMCkge1xuICAgICAgcmV0dXJuIG1vbnRoc1tuZXcgRGF0ZSgpLmdldE1vbnRoKCldO1xuICAgIH1cbiAgICByZXR1cm4gbW9udGhzW21vbnRoIC0gMV0gfHwgbW9udGhzW25ldyBEYXRlKCkuZ2V0TW9udGgoKV07XG4gIH1cblxuICByZXR1cm5Ob3ROdWxsVmFsdWUodmFsdWU6IGFueSkge1xuICAgIHJldHVybiB0aGlzLmlzRmFsc2UodmFsdWUpID8gXCJcIiA6IHZhbHVlO1xuICB9XG5cbiAgc2hvd0Ftb3VudCh0ZXh0OiBhbnksIHN5bWJvbDogYW55KSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxzcGFuPlxuICAgICAgICA8bGFiZWwgc3R5bGU9e3tcbiAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgcGFkZGluZ1JpZ2h0OiBcIjVweFwiLFxuICAgICAgICB9fT57c3ltYm9sfTwvbGFiZWw+XG4gICAgICAgIHt0aGlzLnRvVGhvdXNhbmRzKHBhcnNlRmxvYXQodGV4dCB8fCAwKS50b0ZpeGVkKDIpKX1cbiAgICAgIDwvc3Bhbj5cbiAgICApO1xuICB9XG5cbiAgLy/liKTmlq3mtY/op4jlmajnsbvlnovlkozniYjmnKxcbiAgZ2V0QnJvd3NlclZlcnNpb24odXNlckFnZW50OiBhbnkpIHtcbiAgICAvLyA9IG5hdmlnYXRvci51c2VyQWdlbnRcbiAgICBsZXQgdmVyc2lvbjogYW55ID0gXCJcIjtcbiAgICBpZiAodXNlckFnZW50LmluZGV4T2YoXCJGaXJlZm94XCIpID4gLTEpIHtcbiAgICAgIHZlcnNpb24gPSB1c2VyQWdlbnQubWF0Y2goL2ZpcmVmb3hcXC9bXFxkLl0rL2dpKVswXS5tYXRjaCgvW1xcZF0rLylbMF07XG4gICAgICByZXR1cm4gXCJGaXJlZm94IFwiICsgdmVyc2lvbjtcbiAgICB9IGVsc2UgaWYgKHVzZXJBZ2VudC5pbmRleE9mKFwiRWRnZVwiKSA+IC0xKSB7XG4gICAgICB2ZXJzaW9uID0gdXNlckFnZW50Lm1hdGNoKC9lZGdlXFwvW1xcZC5dKy9naSlbMF0ubWF0Y2goL1tcXGRdKy8pWzBdO1xuICAgICAgcmV0dXJuIFwiRWRnZSBcIiArIHZlcnNpb247XG4gICAgfSBlbHNlIGlmICh1c2VyQWdlbnQuaW5kZXhPZihcIk9wZXJhXCIpID4gLTEgfHwgdXNlckFnZW50LmluZGV4T2YoXCJPUFJcIikgPiAtMSkge1xuICAgICAgaWYgKHVzZXJBZ2VudC5pbmRleE9mKFwiT3BlcmFcIikgPiAtMSkge1xuICAgICAgICB2ZXJzaW9uID0gdXNlckFnZW50Lm1hdGNoKC9vcGVyYVxcL1tcXGQuXSsvZ2kpWzBdLm1hdGNoKC9bXFxkXSsvKVswXTtcbiAgICAgICAgcmV0dXJuIFwiT3BlcmEgXCIgKyB2ZXJzaW9uO1xuICAgICAgfVxuICAgICAgaWYgKHVzZXJBZ2VudC5pbmRleE9mKFwiT1BSXCIpID4gLTEpIHtcbiAgICAgICAgdmVyc2lvbiA9IHVzZXJBZ2VudC5tYXRjaCgvb3ByXFwvW1xcZC5dKy9naSlbMF0ubWF0Y2goL1tcXGRdKy8pWzBdO1xuICAgICAgICByZXR1cm4gXCJPcGVyYSBcIiArIHZlcnNpb247XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh1c2VyQWdlbnQuaW5kZXhPZihcIkNocm9tZVwiKSA+IC0xKSB7XG4gICAgICB2ZXJzaW9uID0gdXNlckFnZW50Lm1hdGNoKC9jaHJvbWVcXC9bXFxkLl0rL2dpKVswXS5tYXRjaCgvW1xcZF0rLylbMF07XG4gICAgICByZXR1cm4gXCJDaHJvbWUgXCIgKyB2ZXJzaW9uO1xuICAgIH0gZWxzZSBpZiAodXNlckFnZW50LmluZGV4T2YoXCJTYWZhcmlcIikgPiAtMSkge1xuICAgICAgdmVyc2lvbiA9IHVzZXJBZ2VudC5tYXRjaCgvc2FmYXJpXFwvW1xcZC5dKy9naSlbMF0ubWF0Y2goL1tcXGRdKy8pWzBdO1xuICAgICAgcmV0dXJuIFwiU2FmYXJpIFwiICsgdmVyc2lvbjtcbiAgICB9IGVsc2UgaWYgKHVzZXJBZ2VudC5pbmRleE9mKFwiTVNJRVwiKSA+IC0xIHx8IHVzZXJBZ2VudC5pbmRleE9mKFwiVHJpZGVudFwiKSA+IC0xKSB7XG4gICAgICBpZiAodXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpID4gLTEpIHtcbiAgICAgICAgdmVyc2lvbiA9IHVzZXJBZ2VudC5tYXRjaCgvbXNpZSBbXFxkLl0rL2dpKVswXS5tYXRjaCgvW1xcZF0rLylbMF07XG4gICAgICAgIHJldHVybiBcIklFIFwiICsgdmVyc2lvbjtcbiAgICAgIH1cbiAgICAgIGlmICh1c2VyQWdlbnQuaW5kZXhPZihcIlRyaWRlbnRcIikgPiAtMSkge1xuICAgICAgICBsZXQgdmVyc2lvblRyaWRlbnQgPSB1c2VyQWdlbnQubWF0Y2goL3RyaWRlbnRcXC9bXFxkLl0rL2dpKVswXS5tYXRjaCgvW1xcZF0rLylbMF07XG4gICAgICAgIHZlcnNpb24gPSBwYXJzZUludCh2ZXJzaW9uVHJpZGVudCkgKyA0O1xuICAgICAgICByZXR1cm4gXCJJRSBcIiArIHZlcnNpb247XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiB1c2VyQWdlbnQ7XG4gIH1cblxuICBjaGFuZ2VDb2xvclR5cGUoc0NvbG9yOiBhbnkpIHtcbiAgICBzQ29sb3IgPSBzQ29sb3IudG9Mb3dlckNhc2UoKTtcbiAgICAvL+WNgeWFrei/m+WItuminOiJsuWAvOeahOato+WImeihqOi+vuW8j1xuICAgIHZhciByZWcgPSAvXiMoWzAtOWEtZkEtZl17M318WzAtOWEtZkEtZl17Nn0pJC87XG4gICAgLy8g5aaC5p6c5pivMTbov5vliLbpopzoibJcbiAgICBpZiAoc0NvbG9yICYmIHJlZy50ZXN0KHNDb2xvcikpIHtcbiAgICAgIGlmIChzQ29sb3IubGVuZ3RoID09PSA0KSB7XG4gICAgICAgIHZhciBzQ29sb3JOZXcgPSBcIiNcIjtcbiAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCA0OyBpICs9IDEpIHtcbiAgICAgICAgICBzQ29sb3JOZXcgKz0gc0NvbG9yLnNsaWNlKGksIGkgKyAxKS5jb25jYXQoc0NvbG9yLnNsaWNlKGksIGkgKyAxKSk7XG4gICAgICAgIH1cbiAgICAgICAgc0NvbG9yID0gc0NvbG9yTmV3O1xuICAgICAgfVxuICAgICAgLy/lpITnkIblha3kvY3nmoTpopzoibLlgLxcbiAgICAgIHZhciBzQ29sb3JDaGFuZ2UgPSBbXTtcbiAgICAgIGZvciAodmFyIGkgPSAxOyBpIDwgNzsgaSArPSAyKSB7XG4gICAgICAgIHNDb2xvckNoYW5nZS5wdXNoKHBhcnNlSW50KFwiMHhcIiArIHNDb2xvci5zbGljZShpLCBpICsgMikpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzQ29sb3JDaGFuZ2Uuam9pbihcIixcIik7XG4gICAgfVxuICAgIHJldHVybiBzQ29sb3I7XG4gIH1cblxuICBhc3luYyB0b2tlblJlY2FwdGNoYShhY3Rpb246IGFueSkge1xuICAgIGxldCBlbnZVcmwgPSBwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfSE9TVDtcbiAgICBpZiAocHJvY2Vzcy5lbnYuUkVBQ1RfQVBQX0VOVl9OQU1FID09PSBcIkxvY2FsXCIpIHtcbiAgICAgIGVudlVybCA9IGAke2VudlVybH06JHtwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfUE9SVH1gO1xuICAgIH1cbiAgICByZXR1cm4gQWpheC5nZXQoYCR7ZW52VXJsfS9yZWNhcHRjaGFrZXlgKS50aGVuKGFzeW5jIChyZXM6IGFueSxcbiAgICApID0+IHtcbiAgICAgIGxldCB0b2tlblJlID0gXCIwMTIzNDU2Nzg5XCI7XG4gICAgICBjb25zdCBrZXkgPSBfLmdldChyZXMsIFwiYm9keS5yZXNwRGF0YVwiLCBcIlwiKTtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5SRUFDVF9BUFBfRU5WX05BTUUgPT09IFwiRGVtb1wiKSB7XG4gICAgICAgIGF3YWl0IGdyZWNhcHRjaGFSZWFkeSgpO1xuICAgICAgICB0cnkge1xuICAgICAgICAgIGNvbnN0IHRva2VuID0gYXdhaXQgKHdpbmRvdyBhcyBhbnkpLmdyZWNhcHRjaGEuZXhlY3V0ZShrZXksIHsgYWN0aW9uIH0pO1xuICAgICAgICAgIHRva2VuUmUgPSB0b2tlbjtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0b2tlblJlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRva2VuUmU7XG4gICAgfSkuY2F0Y2goKCkgPT4ge1xuICAgICAgcmV0dXJuIFwiMDEyMzQ1Njc4OVwiO1xuICAgIH0pO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBVdGlscygpO1xuXG5cbmNvbnN0IF9ncmVjYXB0Y2hhUmVhZHkgPSAocmVzb2x2ZTogYW55KSA9PiB7XG4gICh3aW5kb3cgYXMgYW55KS5ncmVjYXB0Y2hhLnJlYWR5KCgpID0+IHtcbiAgICByZXNvbHZlKHRydWUpO1xuICB9KTtcbn07XG5cbmNvbnN0IGdyZWNhcHRjaGFSZWFkeSA9ICgpID0+IHtcbiAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlOiBhbnksIHJlamVjdDogYW55KSA9PiB7XG4gICAgX2dyZWNhcHRjaGFSZWFkeShyZXNvbHZlKTtcbiAgfSk7XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBWUE7OztBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7Ozs7O0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7OztBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBOzs7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7Ozs7OztBQUVBOzs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFBQTtBQUNBO0FBUkE7QUFPQTtBQUNBO0FBUkE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/common/utils.tsx
