__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







function getNumberArray(num) {
  return num ? num.toString().split('').reverse().map(function (i) {
    var current = Number(i);
    return isNaN(current) ? i : current;
  }) : [];
}

function renderNumberList(position) {
  var childrenToReturn = [];

  for (var i = 0; i < 30; i++) {
    var currentClassName = position === i ? 'current' : '';
    childrenToReturn.push(react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", {
      key: i.toString(),
      className: currentClassName
    }, i % 10));
  }

  return childrenToReturn;
}

var ScrollNumber =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ScrollNumber, _React$Component);

  function ScrollNumber(props) {
    var _this;

    _classCallCheck(this, ScrollNumber);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ScrollNumber).call(this, props));

    _this.onAnimated = function () {
      var onAnimated = _this.props.onAnimated;

      if (onAnimated) {
        onAnimated();
      }
    };

    _this.renderScrollNumber = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          className = _this$props.className,
          style = _this$props.style,
          title = _this$props.title,
          _this$props$component = _this$props.component,
          component = _this$props$component === void 0 ? 'sup' : _this$props$component,
          displayComponent = _this$props.displayComponent; // fix https://fb.me/react-unknown-prop

      var restProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_1__["default"])(_this.props, ['count', 'onAnimated', 'component', 'prefixCls', 'displayComponent']);
      var prefixCls = getPrefixCls('scroll-number', customizePrefixCls);

      var newProps = _extends(_extends({}, restProps), {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, className),
        title: title
      }); // allow specify the border
      // mock border-color by box-shadow for compatible with old usage:
      // <Badge count={4} style={{ backgroundColor: '#fff', color: '#999', borderColor: '#d9d9d9' }} />


      if (style && style.borderColor) {
        newProps.style = _extends(_extends({}, style), {
          boxShadow: "0 0 0 1px ".concat(style.borderColor, " inset")
        });
      }

      if (displayComponent) {
        return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](displayComponent, {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-custom-component"), displayComponent.props && displayComponent.props.className)
        });
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](component, newProps, _this.renderNumberElement(prefixCls));
    };

    _this.state = {
      animateStarted: true,
      count: props.count
    };
    return _this;
  }

  _createClass(ScrollNumber, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(_, prevState) {
      this.lastCount = prevState.count;
      var animateStarted = this.state.animateStarted;

      if (animateStarted) {
        // eslint-disable-next-line react/no-did-update-set-state
        this.setState(function (__, props) {
          return {
            animateStarted: false,
            count: props.count
          };
        }, this.onAnimated);
      }
    }
  }, {
    key: "getPositionByNum",
    value: function getPositionByNum(num, i) {
      var count = this.state.count;
      var currentCount = Math.abs(Number(count));
      var lastCount = Math.abs(Number(this.lastCount));
      var currentDigit = Math.abs(getNumberArray(this.state.count)[i]);
      var lastDigit = Math.abs(getNumberArray(this.lastCount)[i]);

      if (this.state.animateStarted) {
        return 10 + num;
      } // 同方向则在同一侧切换数字


      if (currentCount > lastCount) {
        if (currentDigit >= lastDigit) {
          return 10 + num;
        }

        return 20 + num;
      }

      if (currentDigit <= lastDigit) {
        return 10 + num;
      }

      return num;
    }
  }, {
    key: "renderCurrentNumber",
    value: function renderCurrentNumber(prefixCls, num, i) {
      if (typeof num === 'number') {
        var position = this.getPositionByNum(num, i);
        var removeTransition = this.state.animateStarted || getNumberArray(this.lastCount)[i] === undefined;
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]('span', {
          className: "".concat(prefixCls, "-only"),
          style: {
            transition: removeTransition ? 'none' : undefined,
            msTransform: "translateY(".concat(-position * 100, "%)"),
            WebkitTransform: "translateY(".concat(-position * 100, "%)"),
            transform: "translateY(".concat(-position * 100, "%)")
          },
          key: i
        }, renderNumberList(position));
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        key: "symbol",
        className: "".concat(prefixCls, "-symbol")
      }, num);
    }
  }, {
    key: "renderNumberElement",
    value: function renderNumberElement(prefixCls) {
      var _this2 = this;

      var count = this.state.count;

      if (count && Number(count) % 1 === 0) {
        return getNumberArray(count).map(function (num, i) {
          return _this2.renderCurrentNumber(prefixCls, num, i);
        }).reverse();
      }

      return count;
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_4__["ConfigConsumer"], null, this.renderScrollNumber);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, nextState) {
      if ('count' in nextProps) {
        if (nextState.count === nextProps.count) {
          return null;
        }

        return {
          animateStarted: true
        };
      }

      return null;
    }
  }]);

  return ScrollNumber;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

ScrollNumber.defaultProps = {
  count: null,
  onAnimated: function onAnimated() {}
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_3__["polyfill"])(ScrollNumber);
/* harmony default export */ __webpack_exports__["default"] = (ScrollNumber);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9iYWRnZS9TY3JvbGxOdW1iZXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL2JhZGdlL1Njcm9sbE51bWJlci5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IG9taXQgZnJvbSAnb21pdC5qcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuZnVuY3Rpb24gZ2V0TnVtYmVyQXJyYXkobnVtKSB7XG4gICAgcmV0dXJuIG51bVxuICAgICAgICA/IG51bVxuICAgICAgICAgICAgLnRvU3RyaW5nKClcbiAgICAgICAgICAgIC5zcGxpdCgnJylcbiAgICAgICAgICAgIC5yZXZlcnNlKClcbiAgICAgICAgICAgIC5tYXAoaSA9PiB7XG4gICAgICAgICAgICBjb25zdCBjdXJyZW50ID0gTnVtYmVyKGkpO1xuICAgICAgICAgICAgcmV0dXJuIGlzTmFOKGN1cnJlbnQpID8gaSA6IGN1cnJlbnQ7XG4gICAgICAgIH0pXG4gICAgICAgIDogW107XG59XG5mdW5jdGlvbiByZW5kZXJOdW1iZXJMaXN0KHBvc2l0aW9uKSB7XG4gICAgY29uc3QgY2hpbGRyZW5Ub1JldHVybiA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgMzA7IGkrKykge1xuICAgICAgICBjb25zdCBjdXJyZW50Q2xhc3NOYW1lID0gcG9zaXRpb24gPT09IGkgPyAnY3VycmVudCcgOiAnJztcbiAgICAgICAgY2hpbGRyZW5Ub1JldHVybi5wdXNoKDxwIGtleT17aS50b1N0cmluZygpfSBjbGFzc05hbWU9e2N1cnJlbnRDbGFzc05hbWV9PlxuICAgICAgICB7aSAlIDEwfVxuICAgICAgPC9wPik7XG4gICAgfVxuICAgIHJldHVybiBjaGlsZHJlblRvUmV0dXJuO1xufVxuY2xhc3MgU2Nyb2xsTnVtYmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMub25BbmltYXRlZCA9ICgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgb25BbmltYXRlZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkFuaW1hdGVkKSB7XG4gICAgICAgICAgICAgICAgb25BbmltYXRlZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlclNjcm9sbE51bWJlciA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWUsIHN0eWxlLCB0aXRsZSwgY29tcG9uZW50ID0gJ3N1cCcsIGRpc3BsYXlDb21wb25lbnQsIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgLy8gZml4IGh0dHBzOi8vZmIubWUvcmVhY3QtdW5rbm93bi1wcm9wXG4gICAgICAgICAgICBjb25zdCByZXN0UHJvcHMgPSBvbWl0KHRoaXMucHJvcHMsIFtcbiAgICAgICAgICAgICAgICAnY291bnQnLFxuICAgICAgICAgICAgICAgICdvbkFuaW1hdGVkJyxcbiAgICAgICAgICAgICAgICAnY29tcG9uZW50JyxcbiAgICAgICAgICAgICAgICAncHJlZml4Q2xzJyxcbiAgICAgICAgICAgICAgICAnZGlzcGxheUNvbXBvbmVudCcsXG4gICAgICAgICAgICBdKTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygnc2Nyb2xsLW51bWJlcicsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBuZXdQcm9wcyA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgcmVzdFByb3BzKSwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMocHJlZml4Q2xzLCBjbGFzc05hbWUpLCB0aXRsZTogdGl0bGUgfSk7XG4gICAgICAgICAgICAvLyBhbGxvdyBzcGVjaWZ5IHRoZSBib3JkZXJcbiAgICAgICAgICAgIC8vIG1vY2sgYm9yZGVyLWNvbG9yIGJ5IGJveC1zaGFkb3cgZm9yIGNvbXBhdGlibGUgd2l0aCBvbGQgdXNhZ2U6XG4gICAgICAgICAgICAvLyA8QmFkZ2UgY291bnQ9ezR9IHN0eWxlPXt7IGJhY2tncm91bmRDb2xvcjogJyNmZmYnLCBjb2xvcjogJyM5OTknLCBib3JkZXJDb2xvcjogJyNkOWQ5ZDknIH19IC8+XG4gICAgICAgICAgICBpZiAoc3R5bGUgJiYgc3R5bGUuYm9yZGVyQ29sb3IpIHtcbiAgICAgICAgICAgICAgICBuZXdQcm9wcy5zdHlsZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgc3R5bGUpLCB7IGJveFNoYWRvdzogYDAgMCAwIDFweCAke3N0eWxlLmJvcmRlckNvbG9yfSBpbnNldGAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZGlzcGxheUNvbXBvbmVudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoZGlzcGxheUNvbXBvbmVudCwge1xuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1jdXN0b20tY29tcG9uZW50YCwgZGlzcGxheUNvbXBvbmVudC5wcm9wcyAmJiBkaXNwbGF5Q29tcG9uZW50LnByb3BzLmNsYXNzTmFtZSksXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChjb21wb25lbnQsIG5ld1Byb3BzLCB0aGlzLnJlbmRlck51bWJlckVsZW1lbnQocHJlZml4Q2xzKSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBhbmltYXRlU3RhcnRlZDogdHJ1ZSxcbiAgICAgICAgICAgIGNvdW50OiBwcm9wcy5jb3VudCxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgc3RhdGljIGdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyhuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAgICAgICBpZiAoJ2NvdW50JyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgICAgICAgIGlmIChuZXh0U3RhdGUuY291bnQgPT09IG5leHRQcm9wcy5jb3VudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICBhbmltYXRlU3RhcnRlZDogdHJ1ZSxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZShfLCBwcmV2U3RhdGUpIHtcbiAgICAgICAgdGhpcy5sYXN0Q291bnQgPSBwcmV2U3RhdGUuY291bnQ7XG4gICAgICAgIGNvbnN0IHsgYW5pbWF0ZVN0YXJ0ZWQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGlmIChhbmltYXRlU3RhcnRlZCkge1xuICAgICAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0L25vLWRpZC11cGRhdGUtc2V0LXN0YXRlXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKChfXywgcHJvcHMpID0+ICh7XG4gICAgICAgICAgICAgICAgYW5pbWF0ZVN0YXJ0ZWQ6IGZhbHNlLFxuICAgICAgICAgICAgICAgIGNvdW50OiBwcm9wcy5jb3VudCxcbiAgICAgICAgICAgIH0pLCB0aGlzLm9uQW5pbWF0ZWQpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGdldFBvc2l0aW9uQnlOdW0obnVtLCBpKSB7XG4gICAgICAgIGNvbnN0IHsgY291bnQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRDb3VudCA9IE1hdGguYWJzKE51bWJlcihjb3VudCkpO1xuICAgICAgICBjb25zdCBsYXN0Q291bnQgPSBNYXRoLmFicyhOdW1iZXIodGhpcy5sYXN0Q291bnQpKTtcbiAgICAgICAgY29uc3QgY3VycmVudERpZ2l0ID0gTWF0aC5hYnMoZ2V0TnVtYmVyQXJyYXkodGhpcy5zdGF0ZS5jb3VudClbaV0pO1xuICAgICAgICBjb25zdCBsYXN0RGlnaXQgPSBNYXRoLmFicyhnZXROdW1iZXJBcnJheSh0aGlzLmxhc3RDb3VudClbaV0pO1xuICAgICAgICBpZiAodGhpcy5zdGF0ZS5hbmltYXRlU3RhcnRlZCkge1xuICAgICAgICAgICAgcmV0dXJuIDEwICsgbnVtO1xuICAgICAgICB9XG4gICAgICAgIC8vIOWQjOaWueWQkeWImeWcqOWQjOS4gOS+p+WIh+aNouaVsOWtl1xuICAgICAgICBpZiAoY3VycmVudENvdW50ID4gbGFzdENvdW50KSB7XG4gICAgICAgICAgICBpZiAoY3VycmVudERpZ2l0ID49IGxhc3REaWdpdCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAxMCArIG51bTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAyMCArIG51bTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoY3VycmVudERpZ2l0IDw9IGxhc3REaWdpdCkge1xuICAgICAgICAgICAgcmV0dXJuIDEwICsgbnVtO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudW07XG4gICAgfVxuICAgIHJlbmRlckN1cnJlbnROdW1iZXIocHJlZml4Q2xzLCBudW0sIGkpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBudW0gPT09ICdudW1iZXInKSB7XG4gICAgICAgICAgICBjb25zdCBwb3NpdGlvbiA9IHRoaXMuZ2V0UG9zaXRpb25CeU51bShudW0sIGkpO1xuICAgICAgICAgICAgY29uc3QgcmVtb3ZlVHJhbnNpdGlvbiA9IHRoaXMuc3RhdGUuYW5pbWF0ZVN0YXJ0ZWQgfHwgZ2V0TnVtYmVyQXJyYXkodGhpcy5sYXN0Q291bnQpW2ldID09PSB1bmRlZmluZWQ7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudCgnc3BhbicsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGAke3ByZWZpeENsc30tb25seWAsXG4gICAgICAgICAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogcmVtb3ZlVHJhbnNpdGlvbiA/ICdub25lJyA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgbXNUcmFuc2Zvcm06IGB0cmFuc2xhdGVZKCR7LXBvc2l0aW9uICogMTAwfSUpYCxcbiAgICAgICAgICAgICAgICAgICAgV2Via2l0VHJhbnNmb3JtOiBgdHJhbnNsYXRlWSgkey1wb3NpdGlvbiAqIDEwMH0lKWAsXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogYHRyYW5zbGF0ZVkoJHstcG9zaXRpb24gKiAxMDB9JSlgLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAga2V5OiBpLFxuICAgICAgICAgICAgfSwgcmVuZGVyTnVtYmVyTGlzdChwb3NpdGlvbikpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoPHNwYW4ga2V5PVwic3ltYm9sXCIgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LXN5bWJvbGB9PlxuICAgICAgICB7bnVtfVxuICAgICAgPC9zcGFuPik7XG4gICAgfVxuICAgIHJlbmRlck51bWJlckVsZW1lbnQocHJlZml4Q2xzKSB7XG4gICAgICAgIGNvbnN0IHsgY291bnQgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGlmIChjb3VudCAmJiBOdW1iZXIoY291bnQpICUgMSA9PT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIGdldE51bWJlckFycmF5KGNvdW50KVxuICAgICAgICAgICAgICAgIC5tYXAoKG51bSwgaSkgPT4gdGhpcy5yZW5kZXJDdXJyZW50TnVtYmVyKHByZWZpeENscywgbnVtLCBpKSlcbiAgICAgICAgICAgICAgICAucmV2ZXJzZSgpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb3VudDtcbiAgICB9XG4gICAgcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gPENvbmZpZ0NvbnN1bWVyPnt0aGlzLnJlbmRlclNjcm9sbE51bWJlcn08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5TY3JvbGxOdW1iZXIuZGVmYXVsdFByb3BzID0ge1xuICAgIGNvdW50OiBudWxsLFxuICAgIG9uQW5pbWF0ZWQoKSB7IH0sXG59O1xucG9seWZpbGwoU2Nyb2xsTnVtYmVyKTtcbmV4cG9ydCBkZWZhdWx0IFNjcm9sbE51bWJlcjtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFNQTtBQUNBO0FBUEE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUF2QkE7QUFDQTtBQXdCQTtBQUNBO0FBQ0E7QUFGQTtBQWpDQTtBQXFDQTtBQUNBOzs7QUFXQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFJQTtBQUNBOzs7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFQQTtBQUNBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQVJBO0FBVUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7OztBQXpFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTs7OztBQWpEQTtBQUNBO0FBaUhBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/badge/ScrollNumber.js
