__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationView", function() { return LocationView; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _desk_component_view_item__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/view-item */ "./src/app/desk/component/view-item/index.tsx");
/* harmony import */ var _coverage_view__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./coverage-view */ "./src/app/desk/quote/SAIC/iar/component/coverage-view.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");





var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/location-view.tsx";









var formDateLayout = {
  labelCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  },
  wrapperCol: {
    xs: {
      span: 12
    },
    sm: {
      span: 12
    }
  }
};
var isMobile = _common__WEBPACK_IMPORTED_MODULE_7__["Utils"].getIsMobile();
var dataId = "member.ext";

var LocationView =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_4__["default"])(LocationView, _ModelWidget);

  function LocationView() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, LocationView);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_2__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_3__["default"])(LocationView)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.renderTitle = function () {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("VIEW LOCATION").thai("VIEW LOCATION").getMessage());
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(LocationView, [{
    key: "initComponents",
    value: function initComponents() {
      return {};
    }
  }, {
    key: "initState",
    value: function initState() {
      return {
        loading: false,
        countryCode: "SGP",
        selectedCover: []
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var coverages = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.model, "member.ext.coverages", []);

      var selectedCover = coverages.filter(function (item) {
        return item.selected === "Y";
      });
      this.setState({
        selectedCover: selectedCover
      });
    }
  }, {
    key: "generatePropName",
    value: function generatePropName(propName, dataIdPrefix) {
      if (dataIdPrefix) return "".concat(dataIdPrefix, ".").concat(propName);
      return "".concat(propName);
    }
  }, {
    key: "getProp",
    value: function getProp(propName) {
      if (lodash__WEBPACK_IMPORTED_MODULE_9___default.a.isEmpty(propName)) {
        return "policy";
      }

      return "policy.".concat(propName);
    }
  }, {
    key: "getValue",
    value: function getValue(propName) {
      return this.getValueFromModel("".concat(dataId, ".").concat(propName));
    }
  }, {
    key: "render",
    value: function render() {
      var formProps = {
        form: this.props.form,
        model: this.props.model
      };
      var _this$props = this.props,
          model = _this$props.model,
          labelCol = _this$props.labelCol,
          getMasterTableItemText = _this$props.getMasterTableItemText,
          endoFixed = _this$props.endoFixed;
      var selectedCover = this.state.selectedCover;

      var coverages = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(model, "member.ext.coverages", []);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
        width: isMobile ? "100%" : "70%",
        centered: true,
        closable: true,
        className: "new-damage-form",
        visible: true,
        title: this.renderTitle(),
        onCancel: this.props.onCancel,
        footer: null,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NCollapse"], {
        defaultActiveKey: ["0", "1"],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NPanel"], {
        key: "0",
        header: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("LOCATION ADDRESS").thai("LOCATION ADDRESS").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_10__["default"], {
        layout: labelCol,
        title: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Country").thai("Country").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 119
        },
        __self: this
      }, getMasterTableItemText("nationality", this.getValue("location.countryCode"))), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component_view_item__WEBPACK_IMPORTED_MODULE_10__["default"], {
        layout: labelCol,
        title: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("Address").thai("Address").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_13__["ShowAddress"], {
        model: model,
        addressFixed: "member.ext.location",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NPanel"], {
        key: "1",
        header: _common__WEBPACK_IMPORTED_MODULE_7__["Language"].en("coverage").thai("coverage").getMessage(),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }, selectedCover.map(function (item, i) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_5__["React"].createElement(_coverage_view__WEBPACK_IMPORTED_MODULE_11__["CoverageView"], {
          model: model,
          endoFixed: endoFixed,
          key: i,
          coverage: item,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 136
          },
          __self: this
        });
      }))));
    }
  }]);

  return LocationView;
}(_component__WEBPACK_IMPORTED_MODULE_12__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2xvY2F0aW9uLXZpZXcudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2xvY2F0aW9uLXZpZXcudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24sIENvbCwgTW9kYWwsIFJvdywgU3BpbiB9IGZyb20gXCJhbnRkXCI7XG5cbmltcG9ydCB7IEFqYXgsIExhbmd1YWdlLCBVdGlscywgQXBpcywgQ29uc3RzIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCB7IE5Db2xsYXBzZSwgTkRhdGUsIE5EYXRlRmlsdGVyLCBOUGFuZWwsIE5SYWRpbywgTlNlbGVjdCwgTlRleHQgfSBmcm9tIFwiQGFwcC1jb21wb25lbnRcIjtcblxuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IFZpZXdJdGVtIGZyb20gXCJAZGVzay1jb21wb25lbnQvdmlldy1pdGVtXCI7XG5pbXBvcnQgeyBDb3ZlcmFnZVZpZXcgfSBmcm9tIFwiLi9jb3ZlcmFnZS12aWV3XCI7XG5cbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBTaG93QWRkcmVzcyB9IGZyb20gXCJAZGVzay1jb21wb25lbnRcIjtcblxuY29uc3QgZm9ybURhdGVMYXlvdXQgPSB7XG4gIGxhYmVsQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxuICB3cmFwcGVyQ29sOiB7XG4gICAgeHM6IHsgc3BhbjogMTIgfSxcbiAgICBzbTogeyBzcGFuOiAxMiB9LFxuICB9LFxufTtcbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmNvbnN0IGRhdGFJZCA9IFwibWVtYmVyLmV4dFwiO1xuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgbG9hZGluZzogYm9vbGVhbjtcbiAgY291bnRyeUNvZGU6IGFueTtcbiAgc2VsZWN0ZWRDb3ZlcjogYW55XG59XG5cbmludGVyZmFjZSBJUHJvcHMgZXh0ZW5kcyBNb2RlbFdpZGdldFByb3BzIHtcbiAgb25DYW5jZWw6IGFueVxuICBtb2RlbDogYW55XG4gIGxhYmVsQ29sPzogYW55O1xuICBlbmRvRml4ZWQ/OiBhbnk7XG4gIGdldE1hc3RlclRhYmxlSXRlbVRleHQ6IGFueTtcblxufVxuXG5jbGFzcyBMb2NhdGlvblZpZXc8UCBleHRlbmRzIElQcm9wcywgUyBleHRlbmRzIElTdGF0ZSwgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge30gYXMgQztcbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0U3RhdGUoKTogUyB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgY291bnRyeUNvZGU6IFwiU0dQXCIsXG4gICAgICBzZWxlY3RlZENvdmVyOiBbXSxcbiAgICB9IGFzIFM7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCBjb3ZlcmFnZXM6IGFueSA9IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwibWVtYmVyLmV4dC5jb3ZlcmFnZXNcIiwgW10pO1xuICAgIGNvbnN0IHNlbGVjdGVkQ292ZXIgPSBjb3ZlcmFnZXMuZmlsdGVyKChpdGVtOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiBpdGVtLnNlbGVjdGVkID09PSBcIllcIjtcbiAgICB9KTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgc2VsZWN0ZWRDb3ZlcixcbiAgICB9KTtcbiAgfVxuXG5cbiAgcmVuZGVyVGl0bGUgPSAoKSA9PiB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIHtMYW5ndWFnZS5lbihcIlZJRVcgTE9DQVRJT05cIikudGhhaShcIlZJRVcgTE9DQVRJT05cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfTtcblxuICBwcml2YXRlIGdlbmVyYXRlUHJvcE5hbWUocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkUHJlZml4Pzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICBpZiAoZGF0YUlkUHJlZml4KSByZXR1cm4gYCR7ZGF0YUlkUHJlZml4fS4ke3Byb3BOYW1lfWA7XG4gICAgcmV0dXJuIGAke3Byb3BOYW1lfWA7XG4gIH1cblxuICBnZXRQcm9wKHByb3BOYW1lPzogc3RyaW5nKSB7XG4gICAgaWYgKF8uaXNFbXB0eShwcm9wTmFtZSkpIHtcbiAgICAgIHJldHVybiBcInBvbGljeVwiO1xuICAgIH1cbiAgICByZXR1cm4gYHBvbGljeS4ke3Byb3BOYW1lfWA7XG4gIH1cblxuICBwcml2YXRlIGdldFZhbHVlKHByb3BOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKGAke2RhdGFJZH0uJHtwcm9wTmFtZX1gKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBmb3JtUHJvcHMgPSB7XG4gICAgICBmb3JtOiB0aGlzLnByb3BzLmZvcm0sXG4gICAgICBtb2RlbDogdGhpcy5wcm9wcy5tb2RlbCxcbiAgICB9O1xuICAgIGNvbnN0IHsgbW9kZWwsIGxhYmVsQ29sLCBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0LCBlbmRvRml4ZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBzZWxlY3RlZENvdmVyIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IGNvdmVyYWdlcyA9IF8uZ2V0KG1vZGVsLCBcIm1lbWJlci5leHQuY292ZXJhZ2VzXCIsIFtdKTtcbiAgICByZXR1cm4gKFxuXG4gICAgICA8TW9kYWxcbiAgICAgICAgd2lkdGg9e2lzTW9iaWxlID8gXCIxMDAlXCIgOiBcIjcwJVwifVxuICAgICAgICBjZW50ZXJlZFxuICAgICAgICBjbG9zYWJsZT17dHJ1ZX1cbiAgICAgICAgY2xhc3NOYW1lPVwibmV3LWRhbWFnZS1mb3JtXCJcbiAgICAgICAgdmlzaWJsZT17dHJ1ZX1cbiAgICAgICAgdGl0bGU9e3RoaXMucmVuZGVyVGl0bGUoKX1cbiAgICAgICAgb25DYW5jZWw9e3RoaXMucHJvcHMub25DYW5jZWx9XG4gICAgICAgIGZvb3Rlcj17bnVsbH1cbiAgICAgID5cbiAgICAgICAgPE5Db2xsYXBzZSBkZWZhdWx0QWN0aXZlS2V5PXtbXCIwXCIsIFwiMVwiXX0+XG4gICAgICAgICAgPE5QYW5lbFxuICAgICAgICAgICAga2V5PVwiMFwiXG4gICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiTE9DQVRJT04gQUREUkVTU1wiKVxuICAgICAgICAgICAgICAudGhhaShcIkxPQ0FUSU9OIEFERFJFU1NcIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgbGF5b3V0PXtsYWJlbENvbH1cbiAgICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQ291bnRyeVwiKS50aGFpKFwiQ291bnRyeVwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHtnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0KFwibmF0aW9uYWxpdHlcIiwgdGhpcy5nZXRWYWx1ZShcImxvY2F0aW9uLmNvdW50cnlDb2RlXCIpKX1cbiAgICAgICAgICAgIDwvVmlld0l0ZW0+XG4gICAgICAgICAgICA8Vmlld0l0ZW1cbiAgICAgICAgICAgICAgbGF5b3V0PXtsYWJlbENvbH1cbiAgICAgICAgICAgICAgdGl0bGU9e0xhbmd1YWdlLmVuKFwiQWRkcmVzc1wiKS50aGFpKFwiQWRkcmVzc1wiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxTaG93QWRkcmVzcyBtb2RlbD17bW9kZWx9IGFkZHJlc3NGaXhlZD17YG1lbWJlci5leHQubG9jYXRpb25gfS8+XG4gICAgICAgICAgICA8L1ZpZXdJdGVtPlxuICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICAgIDxOUGFuZWwga2V5PVwiMVwiXG4gICAgICAgICAgICAgICAgICBoZWFkZXI9e0xhbmd1YWdlLmVuKFwiY292ZXJhZ2VcIikudGhhaShcImNvdmVyYWdlXCIpLmdldE1lc3NhZ2UoKX0+XG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHNlbGVjdGVkQ292ZXIubWFwKChpdGVtOiBhbnksIGk6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiA8Q292ZXJhZ2VWaWV3XG4gICAgICAgICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICBlbmRvRml4ZWQ9e2VuZG9GaXhlZH1cbiAgICAgICAgICAgICAgICAgIGtleT17aX1cbiAgICAgICAgICAgICAgICAgIGNvdmVyYWdlPXtpdGVtfVxuICAgICAgICAgICAgICAgIC8+O1xuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICAgIDwvTlBhbmVsPlxuICAgICAgICA8L05Db2xsYXBzZT5cbiAgICAgIDwvTW9kYWw+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBMb2NhdGlvblZpZXcgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBTEE7QUFVQTtBQUNBO0FBQ0E7QUFnQkE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTs7Ozs7O0FBL0JBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUdBOzs7QUFXQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBTUE7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9BO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7QUFNQTs7OztBQXpHQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/location-view.tsx
