__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var rc_util_es_getScrollBarSize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rc-util/es/getScrollBarSize */ "./node_modules/rc-util/es/getScrollBarSize.js");
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-drawer/es/utils.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var currentDrawer = {};

var DrawerChild =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DrawerChild, _React$Component);

  function DrawerChild(props) {
    var _this;

    _classCallCheck(this, DrawerChild);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DrawerChild).call(this, props));

    _this.domFocus = function () {
      if (_this.dom) {
        _this.dom.focus();
      }
    };

    _this.removeStartHandler = function (e) {
      if (e.touches.length > 1) {
        return;
      }

      _this.startPos = {
        x: e.touches[0].clientX,
        y: e.touches[0].clientY
      };
    };

    _this.removeMoveHandler = function (e) {
      if (e.changedTouches.length > 1) {
        return;
      }

      var currentTarget = e.currentTarget;
      var differX = e.changedTouches[0].clientX - _this.startPos.x;
      var differY = e.changedTouches[0].clientY - _this.startPos.y;

      if (currentTarget === _this.maskDom || currentTarget === _this.handlerDom || currentTarget === _this.contentDom && Object(_utils__WEBPACK_IMPORTED_MODULE_5__["getTouchParentScroll"])(currentTarget, e.target, differX, differY)) {
        e.preventDefault();
      }
    };

    _this.transitionEnd = function (e) {
      var dom = e.target;
      Object(_utils__WEBPACK_IMPORTED_MODULE_5__["removeEventListener"])(dom, _utils__WEBPACK_IMPORTED_MODULE_5__["transitionEnd"], _this.transitionEnd);
      dom.style.transition = '';
    };

    _this.onKeyDown = function (e) {
      if (e.keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].ESC) {
        var onClose = _this.props.onClose;
        e.stopPropagation();

        if (onClose) {
          onClose(e);
        }
      }
    };

    _this.onWrapperTransitionEnd = function (e) {
      var _this$props = _this.props,
          open = _this$props.open,
          afterVisibleChange = _this$props.afterVisibleChange;

      if (e.target === _this.contentWrapper && e.propertyName.match(/transform$/)) {
        _this.dom.style.transition = '';

        if (!open && _this.getCurrentDrawerSome()) {
          document.body.style.overflowX = '';

          if (_this.maskDom) {
            _this.maskDom.style.left = '';
            _this.maskDom.style.width = '';
          }
        }

        if (afterVisibleChange) {
          afterVisibleChange(!!open);
        }
      }
    };

    _this.openLevelTransition = function () {
      var _this$props2 = _this.props,
          open = _this$props2.open,
          width = _this$props2.width,
          height = _this$props2.height;

      var _this$getHorizontalBo = _this.getHorizontalBoolAndPlacementName(),
          isHorizontal = _this$getHorizontalBo.isHorizontal,
          placementName = _this$getHorizontalBo.placementName;

      var contentValue = _this.contentDom ? _this.contentDom.getBoundingClientRect()[isHorizontal ? 'width' : 'height'] : 0;
      var value = (isHorizontal ? width : height) || contentValue;

      _this.setLevelAndScrolling(open, placementName, value);
    };

    _this.setLevelTransform = function (open, placementName, value, right) {
      var _this$props3 = _this.props,
          placement = _this$props3.placement,
          levelMove = _this$props3.levelMove,
          duration = _this$props3.duration,
          ease = _this$props3.ease,
          showMask = _this$props3.showMask; // router 切换时可能会导至页面失去滚动条，所以需要时时获取。

      _this.levelDom.forEach(function (dom) {
        dom.style.transition = "transform ".concat(duration, " ").concat(ease);
        Object(_utils__WEBPACK_IMPORTED_MODULE_5__["addEventListener"])(dom, _utils__WEBPACK_IMPORTED_MODULE_5__["transitionEnd"], _this.transitionEnd);
        var levelValue = open ? value : 0;

        if (levelMove) {
          var $levelMove = Object(_utils__WEBPACK_IMPORTED_MODULE_5__["transformArguments"])(levelMove, {
            target: dom,
            open: open
          });
          levelValue = open ? $levelMove[0] : $levelMove[1] || 0;
        }

        var $value = typeof levelValue === 'number' ? "".concat(levelValue, "px") : levelValue;
        var placementPos = placement === 'left' || placement === 'top' ? $value : "-".concat($value);
        placementPos = showMask && placement === 'right' && right ? "calc(".concat(placementPos, " + ").concat(right, "px)") : placementPos;
        dom.style.transform = levelValue ? "".concat(placementName, "(").concat(placementPos, ")") : '';
      });
    };

    _this.setLevelAndScrolling = function (open, placementName, value) {
      var onChange = _this.props.onChange;

      if (!_utils__WEBPACK_IMPORTED_MODULE_5__["windowIsUndefined"]) {
        var right = document.body.scrollHeight > (window.innerHeight || document.documentElement.clientHeight) && window.innerWidth > document.body.offsetWidth ? Object(rc_util_es_getScrollBarSize__WEBPACK_IMPORTED_MODULE_1__["default"])(true) : 0;

        _this.setLevelTransform(open, placementName, value, right);

        _this.toggleScrollingToDrawerAndBody(right);
      }

      if (onChange) {
        onChange(open);
      }
    };

    _this.toggleScrollingToDrawerAndBody = function (right) {
      var _this$props4 = _this.props,
          getOpenCount = _this$props4.getOpenCount,
          getContainer = _this$props4.getContainer,
          showMask = _this$props4.showMask,
          open = _this$props4.open;
      var container = getContainer && getContainer();
      var openCount = getOpenCount && getOpenCount(); // 处理 body 滚动

      if (container && container.parentNode === document.body && showMask) {
        var eventArray = ['touchstart'];
        var domArray = [document.body, _this.maskDom, _this.handlerDom, _this.contentDom];

        if (open && document.body.style.overflow !== 'hidden') {
          if (right) {
            _this.addScrollingEffect(right);
          }

          if (openCount === 1) {
            document.body.style.overflow = 'hidden';
          }

          document.body.style.touchAction = 'none'; // 手机禁滚

          domArray.forEach(function (item, i) {
            if (!item) {
              return;
            }

            Object(_utils__WEBPACK_IMPORTED_MODULE_5__["addEventListener"])(item, eventArray[i] || 'touchmove', i ? _this.removeMoveHandler : _this.removeStartHandler, _this.passive);
          });
        } else if (_this.getCurrentDrawerSome()) {
          // 没有弹框的状态下清除 overflow;
          if (!openCount) {
            document.body.style.overflow = '';
          }

          document.body.style.touchAction = '';

          if (right) {
            _this.remScrollingEffect(right);
          } // 恢复事件


          domArray.forEach(function (item, i) {
            if (!item) {
              return;
            }

            Object(_utils__WEBPACK_IMPORTED_MODULE_5__["removeEventListener"])(item, eventArray[i] || 'touchmove', i ? _this.removeMoveHandler : _this.removeStartHandler, _this.passive);
          });
        }
      }
    };

    _this.addScrollingEffect = function (right) {
      var _this$props5 = _this.props,
          placement = _this$props5.placement,
          duration = _this$props5.duration,
          ease = _this$props5.ease,
          getOpenCount = _this$props5.getOpenCount,
          switchScrollingEffect = _this$props5.switchScrollingEffect;
      var openCount = getOpenCount && getOpenCount();

      if (openCount === 1) {
        switchScrollingEffect();
      }

      var widthTransition = "width ".concat(duration, " ").concat(ease);
      var transformTransition = "transform ".concat(duration, " ").concat(ease);
      _this.dom.style.transition = 'none';

      switch (placement) {
        case 'right':
          _this.dom.style.transform = "translateX(-".concat(right, "px)");
          break;

        case 'top':
        case 'bottom':
          _this.dom.style.width = "calc(100% - ".concat(right, "px)");
          _this.dom.style.transform = 'translateZ(0)';
          break;

        default:
          break;
      }

      clearTimeout(_this.timeout);
      _this.timeout = setTimeout(function () {
        if (_this.dom) {
          _this.dom.style.transition = "".concat(transformTransition, ",").concat(widthTransition);
          _this.dom.style.width = '';
          _this.dom.style.transform = '';
        }
      });
    };

    _this.remScrollingEffect = function (right) {
      var _this$props6 = _this.props,
          placement = _this$props6.placement,
          duration = _this$props6.duration,
          ease = _this$props6.ease,
          getOpenCount = _this$props6.getOpenCount,
          switchScrollingEffect = _this$props6.switchScrollingEffect;
      var openCount = getOpenCount && getOpenCount();

      if (!openCount) {
        switchScrollingEffect(true);
      }

      if (_utils__WEBPACK_IMPORTED_MODULE_5__["transitionStr"]) {
        document.body.style.overflowX = 'hidden';
      }

      _this.dom.style.transition = 'none';
      var heightTransition;
      var widthTransition = "width ".concat(duration, " ").concat(ease);
      var transformTransition = "transform ".concat(duration, " ").concat(ease);

      switch (placement) {
        case 'left':
          {
            _this.dom.style.width = '100%';
            widthTransition = "width 0s ".concat(ease, " ").concat(duration);
            break;
          }

        case 'right':
          {
            _this.dom.style.transform = "translateX(".concat(right, "px)");
            _this.dom.style.width = '100%';
            widthTransition = "width 0s ".concat(ease, " ").concat(duration);

            if (_this.maskDom) {
              _this.maskDom.style.left = "-".concat(right, "px");
              _this.maskDom.style.width = "calc(100% + ".concat(right, "px)");
            }

            break;
          }

        case 'top':
        case 'bottom':
          {
            _this.dom.style.width = "calc(100% + ".concat(right, "px)");
            _this.dom.style.height = '100%';
            _this.dom.style.transform = 'translateZ(0)';
            heightTransition = "height 0s ".concat(ease, " ").concat(duration);
            break;
          }

        default:
          break;
      }

      clearTimeout(_this.timeout);
      _this.timeout = setTimeout(function () {
        if (_this.dom) {
          _this.dom.style.transition = "".concat(transformTransition, ",").concat(heightTransition ? "".concat(heightTransition, ",") : '').concat(widthTransition);
          _this.dom.style.transform = '';
          _this.dom.style.width = '';
          _this.dom.style.height = '';
        }
      });
    };

    _this.getCurrentDrawerSome = function () {
      return !Object.keys(currentDrawer).some(function (key) {
        return currentDrawer[key];
      });
    };

    _this.getLevelDom = function (_ref) {
      var level = _ref.level,
          getContainer = _ref.getContainer;

      if (_utils__WEBPACK_IMPORTED_MODULE_5__["windowIsUndefined"]) {
        return;
      }

      var container = getContainer && getContainer();
      var parent = container ? container.parentNode : null;
      _this.levelDom = [];

      if (level === 'all') {
        var children = parent ? Array.prototype.slice.call(parent.children) : [];
        children.forEach(function (child) {
          if (child.nodeName !== 'SCRIPT' && child.nodeName !== 'STYLE' && child.nodeName !== 'LINK' && child !== container) {
            _this.levelDom.push(child);
          }
        });
      } else if (level) {
        Object(_utils__WEBPACK_IMPORTED_MODULE_5__["dataToArray"])(level).forEach(function (key) {
          document.querySelectorAll(key).forEach(function (item) {
            _this.levelDom.push(item);
          });
        });
      }
    };

    _this.getHorizontalBoolAndPlacementName = function () {
      var placement = _this.props.placement;
      var isHorizontal = placement === 'left' || placement === 'right';
      var placementName = "translate".concat(isHorizontal ? 'X' : 'Y');
      return {
        isHorizontal: isHorizontal,
        placementName: placementName
      };
    };

    _this.state = {
      _self: _assertThisInitialized(_this)
    };
    return _this;
  }

  _createClass(DrawerChild, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (!_utils__WEBPACK_IMPORTED_MODULE_5__["windowIsUndefined"]) {
        var passiveSupported = false;
        window.addEventListener('test', function () {}, Object.defineProperty({}, 'passive', {
          get: function get() {
            passiveSupported = true;
            return null;
          }
        }));
        this.passive = passiveSupported ? {
          passive: false
        } : false;
      }

      var open = this.props.open;
      this.drawerId = "drawer_id_".concat(Number((Date.now() + Math.random()).toString().replace('.', Math.round(Math.random() * 9).toString())).toString(16));
      this.getLevelDom(this.props);

      if (open) {
        currentDrawer[this.drawerId] = open; // 默认打开状态时推出 level;

        this.openLevelTransition();
        this.forceUpdate(function () {
          _this2.domFocus();
        });
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var open = this.props.open;

      if (open !== prevProps.open) {
        if (open) {
          this.domFocus();
        }

        currentDrawer[this.drawerId] = !!open;
        this.openLevelTransition();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this$props7 = this.props,
          getOpenCount = _this$props7.getOpenCount,
          open = _this$props7.open,
          switchScrollingEffect = _this$props7.switchScrollingEffect;
      var openCount = typeof getOpenCount === 'function' && getOpenCount();
      delete currentDrawer[this.drawerId];

      if (open) {
        this.setLevelTransform(false);
        document.body.style.touchAction = '';
      }

      if (!openCount) {
        document.body.style.overflow = '';
        switchScrollingEffect(true);
      }
    } // tslint:disable-next-line:member-ordering

  }, {
    key: "render",
    value: function render() {
      var _classnames,
          _this3 = this;

      var _this$props8 = this.props,
          className = _this$props8.className,
          children = _this$props8.children,
          style = _this$props8.style,
          width = _this$props8.width,
          height = _this$props8.height,
          defaultOpen = _this$props8.defaultOpen,
          $open = _this$props8.open,
          prefixCls = _this$props8.prefixCls,
          placement = _this$props8.placement,
          level = _this$props8.level,
          levelMove = _this$props8.levelMove,
          ease = _this$props8.ease,
          duration = _this$props8.duration,
          getContainer = _this$props8.getContainer,
          handler = _this$props8.handler,
          onChange = _this$props8.onChange,
          afterVisibleChange = _this$props8.afterVisibleChange,
          showMask = _this$props8.showMask,
          maskClosable = _this$props8.maskClosable,
          maskStyle = _this$props8.maskStyle,
          onClose = _this$props8.onClose,
          onHandleClick = _this$props8.onHandleClick,
          keyboard = _this$props8.keyboard,
          getOpenCount = _this$props8.getOpenCount,
          switchScrollingEffect = _this$props8.switchScrollingEffect,
          props = _objectWithoutProperties(_this$props8, ["className", "children", "style", "width", "height", "defaultOpen", "open", "prefixCls", "placement", "level", "levelMove", "ease", "duration", "getContainer", "handler", "onChange", "afterVisibleChange", "showMask", "maskClosable", "maskStyle", "onClose", "onHandleClick", "keyboard", "getOpenCount", "switchScrollingEffect"]); // 首次渲染都将是关闭状态。


      var open = this.dom ? $open : false;
      var wrapperClassName = classnames__WEBPACK_IMPORTED_MODULE_0___default()(prefixCls, (_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls, "-").concat(placement), true), _defineProperty(_classnames, "".concat(prefixCls, "-open"), open), _defineProperty(_classnames, className || '', !!className), _defineProperty(_classnames, 'no-mask', !showMask), _classnames));

      var _this$getHorizontalBo2 = this.getHorizontalBoolAndPlacementName(),
          placementName = _this$getHorizontalBo2.placementName; // 百分比与像素动画不同步，第一次打用后全用像素动画。
      // const defaultValue = !this.contentDom || !level ? '100%' : `${value}px`;


      var placementPos = placement === 'left' || placement === 'top' ? '-100%' : '100%';
      var transform = open ? '' : "".concat(placementName, "(").concat(placementPos, ")");
      var handlerChildren = handler && react__WEBPACK_IMPORTED_MODULE_3__["cloneElement"](handler, {
        onClick: function onClick(e) {
          if (handler.props.onClick) {
            handler.props.onClick();
          }

          if (onHandleClick) {
            onHandleClick(e);
          }
        },
        ref: function ref(c) {
          _this3.handlerDom = c;
        }
      });
      return react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", Object.assign({}, props, {
        tabIndex: -1,
        className: wrapperClassName,
        style: style,
        ref: function ref(c) {
          _this3.dom = c;
        },
        onKeyDown: open && keyboard ? this.onKeyDown : undefined,
        onTransitionEnd: this.onWrapperTransitionEnd
      }), showMask && react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", {
        className: "".concat(prefixCls, "-mask"),
        onClick: maskClosable ? onClose : undefined,
        style: maskStyle,
        ref: function ref(c) {
          _this3.maskDom = c;
        }
      }), react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", {
        className: "".concat(prefixCls, "-content-wrapper"),
        style: {
          transform: transform,
          msTransform: transform,
          width: Object(_utils__WEBPACK_IMPORTED_MODULE_5__["isNumeric"])(width) ? "".concat(width, "px") : width,
          height: Object(_utils__WEBPACK_IMPORTED_MODULE_5__["isNumeric"])(height) ? "".concat(height, "px") : height
        },
        ref: function ref(c) {
          _this3.contentWrapper = c;
        }
      }, react__WEBPACK_IMPORTED_MODULE_3__["createElement"]("div", {
        className: "".concat(prefixCls, "-content"),
        ref: function ref(c) {
          _this3.contentDom = c;
        },
        onTouchStart: open && showMask ? this.removeStartHandler : undefined,
        onTouchMove: open && showMask ? this.removeMoveHandler : undefined
      }, children), handlerChildren));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, _ref2) {
      var prevProps = _ref2.prevProps,
          _self = _ref2._self;
      var nextState = {
        prevProps: props
      };

      if (prevProps !== undefined) {
        var placement = props.placement,
            level = props.level;

        if (placement !== prevProps.placement) {
          // test 的 bug, 有动画过场，删除 dom
          _self.contentDom = null;
        }

        if (level !== prevProps.level) {
          _self.getLevelDom(props);
        }
      }

      return nextState;
    }
  }]);

  return DrawerChild;
}(react__WEBPACK_IMPORTED_MODULE_3__["Component"]);

DrawerChild.defaultProps = {
  switchScrollingEffect: function switchScrollingEffect() {}
};
/* harmony default export */ __webpack_exports__["default"] = (Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_4__["polyfill"])(DrawerChild));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZHJhd2VyL2VzL0RyYXdlckNoaWxkLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZHJhd2VyL2VzL0RyYXdlckNoaWxkLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IGlmICh0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIikgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH07IH0gZWxzZSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTsgfSByZXR1cm4gX3R5cGVvZihvYmopOyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhzb3VyY2UsIGV4Y2x1ZGVkKSB7IGlmIChzb3VyY2UgPT0gbnVsbCkgcmV0dXJuIHt9OyB2YXIgdGFyZ2V0ID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2Uoc291cmNlLCBleGNsdWRlZCk7IHZhciBrZXksIGk7IGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7IHZhciBzb3VyY2VTeW1ib2xLZXlzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzb3VyY2UpOyBmb3IgKGkgPSAwOyBpIDwgc291cmNlU3ltYm9sS2V5cy5sZW5ndGg7IGkrKykgeyBrZXkgPSBzb3VyY2VTeW1ib2xLZXlzW2ldOyBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzb3VyY2UsIGtleSkpIGNvbnRpbnVlOyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKSB7IGlmIChzb3VyY2UgPT0gbnVsbCkgcmV0dXJuIHt9OyB2YXIgdGFyZ2V0ID0ge307IHZhciBzb3VyY2VLZXlzID0gT2JqZWN0LmtleXMoc291cmNlKTsgdmFyIGtleSwgaTsgZm9yIChpID0gMDsgaSA8IHNvdXJjZUtleXMubGVuZ3RoOyBpKyspIHsga2V5ID0gc291cmNlS2V5c1tpXTsgaWYgKGV4Y2x1ZGVkLmluZGV4T2Yoa2V5KSA+PSAwKSBjb250aW51ZTsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH1cblxuZnVuY3Rpb24gX2NyZWF0ZUNsYXNzKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoY2FsbCAmJiAoX3R5cGVvZihjYWxsKSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSkgeyByZXR1cm4gY2FsbDsgfSByZXR1cm4gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKTsgfVxuXG5mdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2YobykgeyByZXR1cm4gby5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKG8pOyB9OyByZXR1cm4gX2dldFByb3RvdHlwZU9mKG8pOyB9XG5cbmZ1bmN0aW9uIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZikgeyBpZiAoc2VsZiA9PT0gdm9pZCAwKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb25cIik7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKTsgfVxuXG5mdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBfc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgby5fX3Byb3RvX18gPSBwOyByZXR1cm4gbzsgfTsgcmV0dXJuIF9zZXRQcm90b3R5cGVPZihvLCBwKTsgfVxuXG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBnZXRTY3JvbGxCYXJTaXplIGZyb20gXCJyYy11dGlsL2VzL2dldFNjcm9sbEJhclNpemVcIjtcbmltcG9ydCBLZXlDb2RlIGZyb20gXCJyYy11dGlsL2VzL0tleUNvZGVcIjtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHBvbHlmaWxsIH0gZnJvbSAncmVhY3QtbGlmZWN5Y2xlcy1jb21wYXQnO1xuaW1wb3J0IHsgYWRkRXZlbnRMaXN0ZW5lciwgZGF0YVRvQXJyYXksIGdldFRvdWNoUGFyZW50U2Nyb2xsLCBpc051bWVyaWMsIHJlbW92ZUV2ZW50TGlzdGVuZXIsIHRyYW5zZm9ybUFyZ3VtZW50cywgdHJhbnNpdGlvbkVuZCwgdHJhbnNpdGlvblN0ciwgd2luZG93SXNVbmRlZmluZWQgfSBmcm9tICcuL3V0aWxzJztcbnZhciBjdXJyZW50RHJhd2VyID0ge307XG5cbnZhciBEcmF3ZXJDaGlsZCA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoRHJhd2VyQ2hpbGQsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERyYXdlckNoaWxkKHByb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIERyYXdlckNoaWxkKTtcblxuICAgIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX2dldFByb3RvdHlwZU9mKERyYXdlckNoaWxkKS5jYWxsKHRoaXMsIHByb3BzKSk7XG5cbiAgICBfdGhpcy5kb21Gb2N1cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5kb20pIHtcbiAgICAgICAgX3RoaXMuZG9tLmZvY3VzKCk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLnJlbW92ZVN0YXJ0SGFuZGxlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoZS50b3VjaGVzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBfdGhpcy5zdGFydFBvcyA9IHtcbiAgICAgICAgeDogZS50b3VjaGVzWzBdLmNsaWVudFgsXG4gICAgICAgIHk6IGUudG91Y2hlc1swXS5jbGllbnRZXG4gICAgICB9O1xuICAgIH07XG5cbiAgICBfdGhpcy5yZW1vdmVNb3ZlSGFuZGxlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoZS5jaGFuZ2VkVG91Y2hlcy5sZW5ndGggPiAxKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGN1cnJlbnRUYXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG4gICAgICB2YXIgZGlmZmVyWCA9IGUuY2hhbmdlZFRvdWNoZXNbMF0uY2xpZW50WCAtIF90aGlzLnN0YXJ0UG9zLng7XG4gICAgICB2YXIgZGlmZmVyWSA9IGUuY2hhbmdlZFRvdWNoZXNbMF0uY2xpZW50WSAtIF90aGlzLnN0YXJ0UG9zLnk7XG5cbiAgICAgIGlmIChjdXJyZW50VGFyZ2V0ID09PSBfdGhpcy5tYXNrRG9tIHx8IGN1cnJlbnRUYXJnZXQgPT09IF90aGlzLmhhbmRsZXJEb20gfHwgY3VycmVudFRhcmdldCA9PT0gX3RoaXMuY29udGVudERvbSAmJiBnZXRUb3VjaFBhcmVudFNjcm9sbChjdXJyZW50VGFyZ2V0LCBlLnRhcmdldCwgZGlmZmVyWCwgZGlmZmVyWSkpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy50cmFuc2l0aW9uRW5kID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBkb20gPSBlLnRhcmdldDtcbiAgICAgIHJlbW92ZUV2ZW50TGlzdGVuZXIoZG9tLCB0cmFuc2l0aW9uRW5kLCBfdGhpcy50cmFuc2l0aW9uRW5kKTtcbiAgICAgIGRvbS5zdHlsZS50cmFuc2l0aW9uID0gJyc7XG4gICAgfTtcblxuICAgIF90aGlzLm9uS2V5RG93biA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoZS5rZXlDb2RlID09PSBLZXlDb2RlLkVTQykge1xuICAgICAgICB2YXIgb25DbG9zZSA9IF90aGlzLnByb3BzLm9uQ2xvc2U7XG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgICAgaWYgKG9uQ2xvc2UpIHtcbiAgICAgICAgICBvbkNsb3NlKGUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uV3JhcHBlclRyYW5zaXRpb25FbmQgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb3BlbiA9IF90aGlzJHByb3BzLm9wZW4sXG4gICAgICAgICAgYWZ0ZXJWaXNpYmxlQ2hhbmdlID0gX3RoaXMkcHJvcHMuYWZ0ZXJWaXNpYmxlQ2hhbmdlO1xuXG4gICAgICBpZiAoZS50YXJnZXQgPT09IF90aGlzLmNvbnRlbnRXcmFwcGVyICYmIGUucHJvcGVydHlOYW1lLm1hdGNoKC90cmFuc2Zvcm0kLykpIHtcbiAgICAgICAgX3RoaXMuZG9tLnN0eWxlLnRyYW5zaXRpb24gPSAnJztcblxuICAgICAgICBpZiAoIW9wZW4gJiYgX3RoaXMuZ2V0Q3VycmVudERyYXdlclNvbWUoKSkge1xuICAgICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3dYID0gJyc7XG5cbiAgICAgICAgICBpZiAoX3RoaXMubWFza0RvbSkge1xuICAgICAgICAgICAgX3RoaXMubWFza0RvbS5zdHlsZS5sZWZ0ID0gJyc7XG4gICAgICAgICAgICBfdGhpcy5tYXNrRG9tLnN0eWxlLndpZHRoID0gJyc7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGFmdGVyVmlzaWJsZUNoYW5nZSkge1xuICAgICAgICAgIGFmdGVyVmlzaWJsZUNoYW5nZSghIW9wZW4pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9wZW5MZXZlbFRyYW5zaXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb3BlbiA9IF90aGlzJHByb3BzMi5vcGVuLFxuICAgICAgICAgIHdpZHRoID0gX3RoaXMkcHJvcHMyLndpZHRoLFxuICAgICAgICAgIGhlaWdodCA9IF90aGlzJHByb3BzMi5oZWlnaHQ7XG5cbiAgICAgIHZhciBfdGhpcyRnZXRIb3Jpem9udGFsQm8gPSBfdGhpcy5nZXRIb3Jpem9udGFsQm9vbEFuZFBsYWNlbWVudE5hbWUoKSxcbiAgICAgICAgICBpc0hvcml6b250YWwgPSBfdGhpcyRnZXRIb3Jpem9udGFsQm8uaXNIb3Jpem9udGFsLFxuICAgICAgICAgIHBsYWNlbWVudE5hbWUgPSBfdGhpcyRnZXRIb3Jpem9udGFsQm8ucGxhY2VtZW50TmFtZTtcblxuICAgICAgdmFyIGNvbnRlbnRWYWx1ZSA9IF90aGlzLmNvbnRlbnREb20gPyBfdGhpcy5jb250ZW50RG9tLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpW2lzSG9yaXpvbnRhbCA/ICd3aWR0aCcgOiAnaGVpZ2h0J10gOiAwO1xuICAgICAgdmFyIHZhbHVlID0gKGlzSG9yaXpvbnRhbCA/IHdpZHRoIDogaGVpZ2h0KSB8fCBjb250ZW50VmFsdWU7XG5cbiAgICAgIF90aGlzLnNldExldmVsQW5kU2Nyb2xsaW5nKG9wZW4sIHBsYWNlbWVudE5hbWUsIHZhbHVlKTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc2V0TGV2ZWxUcmFuc2Zvcm0gPSBmdW5jdGlvbiAob3BlbiwgcGxhY2VtZW50TmFtZSwgdmFsdWUsIHJpZ2h0KSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgcGxhY2VtZW50ID0gX3RoaXMkcHJvcHMzLnBsYWNlbWVudCxcbiAgICAgICAgICBsZXZlbE1vdmUgPSBfdGhpcyRwcm9wczMubGV2ZWxNb3ZlLFxuICAgICAgICAgIGR1cmF0aW9uID0gX3RoaXMkcHJvcHMzLmR1cmF0aW9uLFxuICAgICAgICAgIGVhc2UgPSBfdGhpcyRwcm9wczMuZWFzZSxcbiAgICAgICAgICBzaG93TWFzayA9IF90aGlzJHByb3BzMy5zaG93TWFzazsgLy8gcm91dGVyIOWIh+aNouaXtuWPr+iDveS8muWvvOiHs+mhtemdouWkseWOu+a7muWKqOadoe+8jOaJgOS7pemcgOimgeaXtuaXtuiOt+WPluOAglxuXG4gICAgICBfdGhpcy5sZXZlbERvbS5mb3JFYWNoKGZ1bmN0aW9uIChkb20pIHtcbiAgICAgICAgZG9tLnN0eWxlLnRyYW5zaXRpb24gPSBcInRyYW5zZm9ybSBcIi5jb25jYXQoZHVyYXRpb24sIFwiIFwiKS5jb25jYXQoZWFzZSk7XG4gICAgICAgIGFkZEV2ZW50TGlzdGVuZXIoZG9tLCB0cmFuc2l0aW9uRW5kLCBfdGhpcy50cmFuc2l0aW9uRW5kKTtcbiAgICAgICAgdmFyIGxldmVsVmFsdWUgPSBvcGVuID8gdmFsdWUgOiAwO1xuXG4gICAgICAgIGlmIChsZXZlbE1vdmUpIHtcbiAgICAgICAgICB2YXIgJGxldmVsTW92ZSA9IHRyYW5zZm9ybUFyZ3VtZW50cyhsZXZlbE1vdmUsIHtcbiAgICAgICAgICAgIHRhcmdldDogZG9tLFxuICAgICAgICAgICAgb3Blbjogb3BlblxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGxldmVsVmFsdWUgPSBvcGVuID8gJGxldmVsTW92ZVswXSA6ICRsZXZlbE1vdmVbMV0gfHwgMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciAkdmFsdWUgPSB0eXBlb2YgbGV2ZWxWYWx1ZSA9PT0gJ251bWJlcicgPyBcIlwiLmNvbmNhdChsZXZlbFZhbHVlLCBcInB4XCIpIDogbGV2ZWxWYWx1ZTtcbiAgICAgICAgdmFyIHBsYWNlbWVudFBvcyA9IHBsYWNlbWVudCA9PT0gJ2xlZnQnIHx8IHBsYWNlbWVudCA9PT0gJ3RvcCcgPyAkdmFsdWUgOiBcIi1cIi5jb25jYXQoJHZhbHVlKTtcbiAgICAgICAgcGxhY2VtZW50UG9zID0gc2hvd01hc2sgJiYgcGxhY2VtZW50ID09PSAncmlnaHQnICYmIHJpZ2h0ID8gXCJjYWxjKFwiLmNvbmNhdChwbGFjZW1lbnRQb3MsIFwiICsgXCIpLmNvbmNhdChyaWdodCwgXCJweClcIikgOiBwbGFjZW1lbnRQb3M7XG4gICAgICAgIGRvbS5zdHlsZS50cmFuc2Zvcm0gPSBsZXZlbFZhbHVlID8gXCJcIi5jb25jYXQocGxhY2VtZW50TmFtZSwgXCIoXCIpLmNvbmNhdChwbGFjZW1lbnRQb3MsIFwiKVwiKSA6ICcnO1xuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLnNldExldmVsQW5kU2Nyb2xsaW5nID0gZnVuY3Rpb24gKG9wZW4sIHBsYWNlbWVudE5hbWUsIHZhbHVlKSB7XG4gICAgICB2YXIgb25DaGFuZ2UgPSBfdGhpcy5wcm9wcy5vbkNoYW5nZTtcblxuICAgICAgaWYgKCF3aW5kb3dJc1VuZGVmaW5lZCkge1xuICAgICAgICB2YXIgcmlnaHQgPSBkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCA+ICh3aW5kb3cuaW5uZXJIZWlnaHQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodCkgJiYgd2luZG93LmlubmVyV2lkdGggPiBkb2N1bWVudC5ib2R5Lm9mZnNldFdpZHRoID8gZ2V0U2Nyb2xsQmFyU2l6ZSh0cnVlKSA6IDA7XG5cbiAgICAgICAgX3RoaXMuc2V0TGV2ZWxUcmFuc2Zvcm0ob3BlbiwgcGxhY2VtZW50TmFtZSwgdmFsdWUsIHJpZ2h0KTtcblxuICAgICAgICBfdGhpcy50b2dnbGVTY3JvbGxpbmdUb0RyYXdlckFuZEJvZHkocmlnaHQpO1xuICAgICAgfVxuXG4gICAgICBpZiAob25DaGFuZ2UpIHtcbiAgICAgICAgb25DaGFuZ2Uob3Blbik7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLnRvZ2dsZVNjcm9sbGluZ1RvRHJhd2VyQW5kQm9keSA9IGZ1bmN0aW9uIChyaWdodCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNCA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGdldE9wZW5Db3VudCA9IF90aGlzJHByb3BzNC5nZXRPcGVuQ291bnQsXG4gICAgICAgICAgZ2V0Q29udGFpbmVyID0gX3RoaXMkcHJvcHM0LmdldENvbnRhaW5lcixcbiAgICAgICAgICBzaG93TWFzayA9IF90aGlzJHByb3BzNC5zaG93TWFzayxcbiAgICAgICAgICBvcGVuID0gX3RoaXMkcHJvcHM0Lm9wZW47XG4gICAgICB2YXIgY29udGFpbmVyID0gZ2V0Q29udGFpbmVyICYmIGdldENvbnRhaW5lcigpO1xuICAgICAgdmFyIG9wZW5Db3VudCA9IGdldE9wZW5Db3VudCAmJiBnZXRPcGVuQ291bnQoKTsgLy8g5aSE55CGIGJvZHkg5rua5YqoXG5cbiAgICAgIGlmIChjb250YWluZXIgJiYgY29udGFpbmVyLnBhcmVudE5vZGUgPT09IGRvY3VtZW50LmJvZHkgJiYgc2hvd01hc2spIHtcbiAgICAgICAgdmFyIGV2ZW50QXJyYXkgPSBbJ3RvdWNoc3RhcnQnXTtcbiAgICAgICAgdmFyIGRvbUFycmF5ID0gW2RvY3VtZW50LmJvZHksIF90aGlzLm1hc2tEb20sIF90aGlzLmhhbmRsZXJEb20sIF90aGlzLmNvbnRlbnREb21dO1xuXG4gICAgICAgIGlmIChvcGVuICYmIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgIT09ICdoaWRkZW4nKSB7XG4gICAgICAgICAgaWYgKHJpZ2h0KSB7XG4gICAgICAgICAgICBfdGhpcy5hZGRTY3JvbGxpbmdFZmZlY3QocmlnaHQpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChvcGVuQ291bnQgPT09IDEpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLnRvdWNoQWN0aW9uID0gJ25vbmUnOyAvLyDmiYvmnLrnpoHmu5pcblxuICAgICAgICAgIGRvbUFycmF5LmZvckVhY2goZnVuY3Rpb24gKGl0ZW0sIGkpIHtcbiAgICAgICAgICAgIGlmICghaXRlbSkge1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGFkZEV2ZW50TGlzdGVuZXIoaXRlbSwgZXZlbnRBcnJheVtpXSB8fCAndG91Y2htb3ZlJywgaSA/IF90aGlzLnJlbW92ZU1vdmVIYW5kbGVyIDogX3RoaXMucmVtb3ZlU3RhcnRIYW5kbGVyLCBfdGhpcy5wYXNzaXZlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChfdGhpcy5nZXRDdXJyZW50RHJhd2VyU29tZSgpKSB7XG4gICAgICAgICAgLy8g5rKh5pyJ5by55qGG55qE54q25oCB5LiL5riF6ZmkIG92ZXJmbG93O1xuICAgICAgICAgIGlmICghb3BlbkNvdW50KSB7XG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gJyc7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS50b3VjaEFjdGlvbiA9ICcnO1xuXG4gICAgICAgICAgaWYgKHJpZ2h0KSB7XG4gICAgICAgICAgICBfdGhpcy5yZW1TY3JvbGxpbmdFZmZlY3QocmlnaHQpO1xuICAgICAgICAgIH0gLy8g5oGi5aSN5LqL5Lu2XG5cblxuICAgICAgICAgIGRvbUFycmF5LmZvckVhY2goZnVuY3Rpb24gKGl0ZW0sIGkpIHtcbiAgICAgICAgICAgIGlmICghaXRlbSkge1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJlbW92ZUV2ZW50TGlzdGVuZXIoaXRlbSwgZXZlbnRBcnJheVtpXSB8fCAndG91Y2htb3ZlJywgaSA/IF90aGlzLnJlbW92ZU1vdmVIYW5kbGVyIDogX3RoaXMucmVtb3ZlU3RhcnRIYW5kbGVyLCBfdGhpcy5wYXNzaXZlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5hZGRTY3JvbGxpbmdFZmZlY3QgPSBmdW5jdGlvbiAocmlnaHQpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczUgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBwbGFjZW1lbnQgPSBfdGhpcyRwcm9wczUucGxhY2VtZW50LFxuICAgICAgICAgIGR1cmF0aW9uID0gX3RoaXMkcHJvcHM1LmR1cmF0aW9uLFxuICAgICAgICAgIGVhc2UgPSBfdGhpcyRwcm9wczUuZWFzZSxcbiAgICAgICAgICBnZXRPcGVuQ291bnQgPSBfdGhpcyRwcm9wczUuZ2V0T3BlbkNvdW50LFxuICAgICAgICAgIHN3aXRjaFNjcm9sbGluZ0VmZmVjdCA9IF90aGlzJHByb3BzNS5zd2l0Y2hTY3JvbGxpbmdFZmZlY3Q7XG4gICAgICB2YXIgb3BlbkNvdW50ID0gZ2V0T3BlbkNvdW50ICYmIGdldE9wZW5Db3VudCgpO1xuXG4gICAgICBpZiAob3BlbkNvdW50ID09PSAxKSB7XG4gICAgICAgIHN3aXRjaFNjcm9sbGluZ0VmZmVjdCgpO1xuICAgICAgfVxuXG4gICAgICB2YXIgd2lkdGhUcmFuc2l0aW9uID0gXCJ3aWR0aCBcIi5jb25jYXQoZHVyYXRpb24sIFwiIFwiKS5jb25jYXQoZWFzZSk7XG4gICAgICB2YXIgdHJhbnNmb3JtVHJhbnNpdGlvbiA9IFwidHJhbnNmb3JtIFwiLmNvbmNhdChkdXJhdGlvbiwgXCIgXCIpLmNvbmNhdChlYXNlKTtcbiAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2l0aW9uID0gJ25vbmUnO1xuXG4gICAgICBzd2l0Y2ggKHBsYWNlbWVudCkge1xuICAgICAgICBjYXNlICdyaWdodCc6XG4gICAgICAgICAgX3RoaXMuZG9tLnN0eWxlLnRyYW5zZm9ybSA9IFwidHJhbnNsYXRlWCgtXCIuY29uY2F0KHJpZ2h0LCBcInB4KVwiKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICd0b3AnOlxuICAgICAgICBjYXNlICdib3R0b20nOlxuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS53aWR0aCA9IFwiY2FsYygxMDAlIC0gXCIuY29uY2F0KHJpZ2h0LCBcInB4KVwiKTtcbiAgICAgICAgICBfdGhpcy5kb20uc3R5bGUudHJhbnNmb3JtID0gJ3RyYW5zbGF0ZVooMCknO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGNsZWFyVGltZW91dChfdGhpcy50aW1lb3V0KTtcbiAgICAgIF90aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKF90aGlzLmRvbSkge1xuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2l0aW9uID0gXCJcIi5jb25jYXQodHJhbnNmb3JtVHJhbnNpdGlvbiwgXCIsXCIpLmNvbmNhdCh3aWR0aFRyYW5zaXRpb24pO1xuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS53aWR0aCA9ICcnO1xuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2Zvcm0gPSAnJztcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfTtcblxuICAgIF90aGlzLnJlbVNjcm9sbGluZ0VmZmVjdCA9IGZ1bmN0aW9uIChyaWdodCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHBsYWNlbWVudCA9IF90aGlzJHByb3BzNi5wbGFjZW1lbnQsXG4gICAgICAgICAgZHVyYXRpb24gPSBfdGhpcyRwcm9wczYuZHVyYXRpb24sXG4gICAgICAgICAgZWFzZSA9IF90aGlzJHByb3BzNi5lYXNlLFxuICAgICAgICAgIGdldE9wZW5Db3VudCA9IF90aGlzJHByb3BzNi5nZXRPcGVuQ291bnQsXG4gICAgICAgICAgc3dpdGNoU2Nyb2xsaW5nRWZmZWN0ID0gX3RoaXMkcHJvcHM2LnN3aXRjaFNjcm9sbGluZ0VmZmVjdDtcbiAgICAgIHZhciBvcGVuQ291bnQgPSBnZXRPcGVuQ291bnQgJiYgZ2V0T3BlbkNvdW50KCk7XG5cbiAgICAgIGlmICghb3BlbkNvdW50KSB7XG4gICAgICAgIHN3aXRjaFNjcm9sbGluZ0VmZmVjdCh0cnVlKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRyYW5zaXRpb25TdHIpIHtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvd1ggPSAnaGlkZGVuJztcbiAgICAgIH1cblxuICAgICAgX3RoaXMuZG9tLnN0eWxlLnRyYW5zaXRpb24gPSAnbm9uZSc7XG4gICAgICB2YXIgaGVpZ2h0VHJhbnNpdGlvbjtcbiAgICAgIHZhciB3aWR0aFRyYW5zaXRpb24gPSBcIndpZHRoIFwiLmNvbmNhdChkdXJhdGlvbiwgXCIgXCIpLmNvbmNhdChlYXNlKTtcbiAgICAgIHZhciB0cmFuc2Zvcm1UcmFuc2l0aW9uID0gXCJ0cmFuc2Zvcm0gXCIuY29uY2F0KGR1cmF0aW9uLCBcIiBcIikuY29uY2F0KGVhc2UpO1xuXG4gICAgICBzd2l0Y2ggKHBsYWNlbWVudCkge1xuICAgICAgICBjYXNlICdsZWZ0JzpcbiAgICAgICAgICB7XG4gICAgICAgICAgICBfdGhpcy5kb20uc3R5bGUud2lkdGggPSAnMTAwJSc7XG4gICAgICAgICAgICB3aWR0aFRyYW5zaXRpb24gPSBcIndpZHRoIDBzIFwiLmNvbmNhdChlYXNlLCBcIiBcIikuY29uY2F0KGR1cmF0aW9uKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cblxuICAgICAgICBjYXNlICdyaWdodCc6XG4gICAgICAgICAge1xuICAgICAgICAgICAgX3RoaXMuZG9tLnN0eWxlLnRyYW5zZm9ybSA9IFwidHJhbnNsYXRlWChcIi5jb25jYXQocmlnaHQsIFwicHgpXCIpO1xuICAgICAgICAgICAgX3RoaXMuZG9tLnN0eWxlLndpZHRoID0gJzEwMCUnO1xuICAgICAgICAgICAgd2lkdGhUcmFuc2l0aW9uID0gXCJ3aWR0aCAwcyBcIi5jb25jYXQoZWFzZSwgXCIgXCIpLmNvbmNhdChkdXJhdGlvbik7XG5cbiAgICAgICAgICAgIGlmIChfdGhpcy5tYXNrRG9tKSB7XG4gICAgICAgICAgICAgIF90aGlzLm1hc2tEb20uc3R5bGUubGVmdCA9IFwiLVwiLmNvbmNhdChyaWdodCwgXCJweFwiKTtcbiAgICAgICAgICAgICAgX3RoaXMubWFza0RvbS5zdHlsZS53aWR0aCA9IFwiY2FsYygxMDAlICsgXCIuY29uY2F0KHJpZ2h0LCBcInB4KVwiKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuXG4gICAgICAgIGNhc2UgJ3RvcCc6XG4gICAgICAgIGNhc2UgJ2JvdHRvbSc6XG4gICAgICAgICAge1xuICAgICAgICAgICAgX3RoaXMuZG9tLnN0eWxlLndpZHRoID0gXCJjYWxjKDEwMCUgKyBcIi5jb25jYXQocmlnaHQsIFwicHgpXCIpO1xuICAgICAgICAgICAgX3RoaXMuZG9tLnN0eWxlLmhlaWdodCA9ICcxMDAlJztcbiAgICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlWigwKSc7XG4gICAgICAgICAgICBoZWlnaHRUcmFuc2l0aW9uID0gXCJoZWlnaHQgMHMgXCIuY29uY2F0KGVhc2UsIFwiIFwiKS5jb25jYXQoZHVyYXRpb24pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuXG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIGNsZWFyVGltZW91dChfdGhpcy50aW1lb3V0KTtcbiAgICAgIF90aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKF90aGlzLmRvbSkge1xuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2l0aW9uID0gXCJcIi5jb25jYXQodHJhbnNmb3JtVHJhbnNpdGlvbiwgXCIsXCIpLmNvbmNhdChoZWlnaHRUcmFuc2l0aW9uID8gXCJcIi5jb25jYXQoaGVpZ2h0VHJhbnNpdGlvbiwgXCIsXCIpIDogJycpLmNvbmNhdCh3aWR0aFRyYW5zaXRpb24pO1xuICAgICAgICAgIF90aGlzLmRvbS5zdHlsZS50cmFuc2Zvcm0gPSAnJztcbiAgICAgICAgICBfdGhpcy5kb20uc3R5bGUud2lkdGggPSAnJztcbiAgICAgICAgICBfdGhpcy5kb20uc3R5bGUuaGVpZ2h0ID0gJyc7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICBfdGhpcy5nZXRDdXJyZW50RHJhd2VyU29tZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiAhT2JqZWN0LmtleXMoY3VycmVudERyYXdlcikuc29tZShmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIHJldHVybiBjdXJyZW50RHJhd2VyW2tleV07XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0TGV2ZWxEb20gPSBmdW5jdGlvbiAoX3JlZikge1xuICAgICAgdmFyIGxldmVsID0gX3JlZi5sZXZlbCxcbiAgICAgICAgICBnZXRDb250YWluZXIgPSBfcmVmLmdldENvbnRhaW5lcjtcblxuICAgICAgaWYgKHdpbmRvd0lzVW5kZWZpbmVkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGNvbnRhaW5lciA9IGdldENvbnRhaW5lciAmJiBnZXRDb250YWluZXIoKTtcbiAgICAgIHZhciBwYXJlbnQgPSBjb250YWluZXIgPyBjb250YWluZXIucGFyZW50Tm9kZSA6IG51bGw7XG4gICAgICBfdGhpcy5sZXZlbERvbSA9IFtdO1xuXG4gICAgICBpZiAobGV2ZWwgPT09ICdhbGwnKSB7XG4gICAgICAgIHZhciBjaGlsZHJlbiA9IHBhcmVudCA/IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHBhcmVudC5jaGlsZHJlbikgOiBbXTtcbiAgICAgICAgY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICBpZiAoY2hpbGQubm9kZU5hbWUgIT09ICdTQ1JJUFQnICYmIGNoaWxkLm5vZGVOYW1lICE9PSAnU1RZTEUnICYmIGNoaWxkLm5vZGVOYW1lICE9PSAnTElOSycgJiYgY2hpbGQgIT09IGNvbnRhaW5lcikge1xuICAgICAgICAgICAgX3RoaXMubGV2ZWxEb20ucHVzaChjaGlsZCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAobGV2ZWwpIHtcbiAgICAgICAgZGF0YVRvQXJyYXkobGV2ZWwpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoa2V5KS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICBfdGhpcy5sZXZlbERvbS5wdXNoKGl0ZW0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0SG9yaXpvbnRhbEJvb2xBbmRQbGFjZW1lbnROYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHBsYWNlbWVudCA9IF90aGlzLnByb3BzLnBsYWNlbWVudDtcbiAgICAgIHZhciBpc0hvcml6b250YWwgPSBwbGFjZW1lbnQgPT09ICdsZWZ0JyB8fCBwbGFjZW1lbnQgPT09ICdyaWdodCc7XG4gICAgICB2YXIgcGxhY2VtZW50TmFtZSA9IFwidHJhbnNsYXRlXCIuY29uY2F0KGlzSG9yaXpvbnRhbCA/ICdYJyA6ICdZJyk7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBpc0hvcml6b250YWw6IGlzSG9yaXpvbnRhbCxcbiAgICAgICAgcGxhY2VtZW50TmFtZTogcGxhY2VtZW50TmFtZVxuICAgICAgfTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBfc2VsZjogX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcylcbiAgICB9O1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhEcmF3ZXJDaGlsZCwgW3tcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkTW91bnRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgaWYgKCF3aW5kb3dJc1VuZGVmaW5lZCkge1xuICAgICAgICB2YXIgcGFzc2l2ZVN1cHBvcnRlZCA9IGZhbHNlO1xuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigndGVzdCcsIGZ1bmN0aW9uICgpIHt9LCBPYmplY3QuZGVmaW5lUHJvcGVydHkoe30sICdwYXNzaXZlJywge1xuICAgICAgICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgICAgICAgcGFzc2l2ZVN1cHBvcnRlZCA9IHRydWU7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pKTtcbiAgICAgICAgdGhpcy5wYXNzaXZlID0gcGFzc2l2ZVN1cHBvcnRlZCA/IHtcbiAgICAgICAgICBwYXNzaXZlOiBmYWxzZVxuICAgICAgICB9IDogZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHZhciBvcGVuID0gdGhpcy5wcm9wcy5vcGVuO1xuICAgICAgdGhpcy5kcmF3ZXJJZCA9IFwiZHJhd2VyX2lkX1wiLmNvbmNhdChOdW1iZXIoKERhdGUubm93KCkgKyBNYXRoLnJhbmRvbSgpKS50b1N0cmluZygpLnJlcGxhY2UoJy4nLCBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA5KS50b1N0cmluZygpKSkudG9TdHJpbmcoMTYpKTtcbiAgICAgIHRoaXMuZ2V0TGV2ZWxEb20odGhpcy5wcm9wcyk7XG5cbiAgICAgIGlmIChvcGVuKSB7XG4gICAgICAgIGN1cnJlbnREcmF3ZXJbdGhpcy5kcmF3ZXJJZF0gPSBvcGVuOyAvLyDpu5jorqTmiZPlvIDnirbmgIHml7bmjqjlh7ogbGV2ZWw7XG5cbiAgICAgICAgdGhpcy5vcGVuTGV2ZWxUcmFuc2l0aW9uKCk7XG4gICAgICAgIHRoaXMuZm9yY2VVcGRhdGUoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIF90aGlzMi5kb21Gb2N1cygpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkVXBkYXRlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgIHZhciBvcGVuID0gdGhpcy5wcm9wcy5vcGVuO1xuXG4gICAgICBpZiAob3BlbiAhPT0gcHJldlByb3BzLm9wZW4pIHtcbiAgICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgICB0aGlzLmRvbUZvY3VzKCk7XG4gICAgICAgIH1cblxuICAgICAgICBjdXJyZW50RHJhd2VyW3RoaXMuZHJhd2VySWRdID0gISFvcGVuO1xuICAgICAgICB0aGlzLm9wZW5MZXZlbFRyYW5zaXRpb24oKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiY29tcG9uZW50V2lsbFVubW91bnRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM3ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBnZXRPcGVuQ291bnQgPSBfdGhpcyRwcm9wczcuZ2V0T3BlbkNvdW50LFxuICAgICAgICAgIG9wZW4gPSBfdGhpcyRwcm9wczcub3BlbixcbiAgICAgICAgICBzd2l0Y2hTY3JvbGxpbmdFZmZlY3QgPSBfdGhpcyRwcm9wczcuc3dpdGNoU2Nyb2xsaW5nRWZmZWN0O1xuICAgICAgdmFyIG9wZW5Db3VudCA9IHR5cGVvZiBnZXRPcGVuQ291bnQgPT09ICdmdW5jdGlvbicgJiYgZ2V0T3BlbkNvdW50KCk7XG4gICAgICBkZWxldGUgY3VycmVudERyYXdlclt0aGlzLmRyYXdlcklkXTtcblxuICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgdGhpcy5zZXRMZXZlbFRyYW5zZm9ybShmYWxzZSk7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUudG91Y2hBY3Rpb24gPSAnJztcbiAgICAgIH1cblxuICAgICAgaWYgKCFvcGVuQ291bnQpIHtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICcnO1xuICAgICAgICBzd2l0Y2hTY3JvbGxpbmdFZmZlY3QodHJ1ZSk7XG4gICAgICB9XG4gICAgfSAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWVtYmVyLW9yZGVyaW5nXG5cbiAgfSwge1xuICAgIGtleTogXCJyZW5kZXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc25hbWVzLFxuICAgICAgICAgIF90aGlzMyA9IHRoaXM7XG5cbiAgICAgIHZhciBfdGhpcyRwcm9wczggPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF90aGlzJHByb3BzOC5jbGFzc05hbWUsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfdGhpcyRwcm9wczguY2hpbGRyZW4sXG4gICAgICAgICAgc3R5bGUgPSBfdGhpcyRwcm9wczguc3R5bGUsXG4gICAgICAgICAgd2lkdGggPSBfdGhpcyRwcm9wczgud2lkdGgsXG4gICAgICAgICAgaGVpZ2h0ID0gX3RoaXMkcHJvcHM4LmhlaWdodCxcbiAgICAgICAgICBkZWZhdWx0T3BlbiA9IF90aGlzJHByb3BzOC5kZWZhdWx0T3BlbixcbiAgICAgICAgICAkb3BlbiA9IF90aGlzJHByb3BzOC5vcGVuLFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzOC5wcmVmaXhDbHMsXG4gICAgICAgICAgcGxhY2VtZW50ID0gX3RoaXMkcHJvcHM4LnBsYWNlbWVudCxcbiAgICAgICAgICBsZXZlbCA9IF90aGlzJHByb3BzOC5sZXZlbCxcbiAgICAgICAgICBsZXZlbE1vdmUgPSBfdGhpcyRwcm9wczgubGV2ZWxNb3ZlLFxuICAgICAgICAgIGVhc2UgPSBfdGhpcyRwcm9wczguZWFzZSxcbiAgICAgICAgICBkdXJhdGlvbiA9IF90aGlzJHByb3BzOC5kdXJhdGlvbixcbiAgICAgICAgICBnZXRDb250YWluZXIgPSBfdGhpcyRwcm9wczguZ2V0Q29udGFpbmVyLFxuICAgICAgICAgIGhhbmRsZXIgPSBfdGhpcyRwcm9wczguaGFuZGxlcixcbiAgICAgICAgICBvbkNoYW5nZSA9IF90aGlzJHByb3BzOC5vbkNoYW5nZSxcbiAgICAgICAgICBhZnRlclZpc2libGVDaGFuZ2UgPSBfdGhpcyRwcm9wczguYWZ0ZXJWaXNpYmxlQ2hhbmdlLFxuICAgICAgICAgIHNob3dNYXNrID0gX3RoaXMkcHJvcHM4LnNob3dNYXNrLFxuICAgICAgICAgIG1hc2tDbG9zYWJsZSA9IF90aGlzJHByb3BzOC5tYXNrQ2xvc2FibGUsXG4gICAgICAgICAgbWFza1N0eWxlID0gX3RoaXMkcHJvcHM4Lm1hc2tTdHlsZSxcbiAgICAgICAgICBvbkNsb3NlID0gX3RoaXMkcHJvcHM4Lm9uQ2xvc2UsXG4gICAgICAgICAgb25IYW5kbGVDbGljayA9IF90aGlzJHByb3BzOC5vbkhhbmRsZUNsaWNrLFxuICAgICAgICAgIGtleWJvYXJkID0gX3RoaXMkcHJvcHM4LmtleWJvYXJkLFxuICAgICAgICAgIGdldE9wZW5Db3VudCA9IF90aGlzJHByb3BzOC5nZXRPcGVuQ291bnQsXG4gICAgICAgICAgc3dpdGNoU2Nyb2xsaW5nRWZmZWN0ID0gX3RoaXMkcHJvcHM4LnN3aXRjaFNjcm9sbGluZ0VmZmVjdCxcbiAgICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfdGhpcyRwcm9wczgsIFtcImNsYXNzTmFtZVwiLCBcImNoaWxkcmVuXCIsIFwic3R5bGVcIiwgXCJ3aWR0aFwiLCBcImhlaWdodFwiLCBcImRlZmF1bHRPcGVuXCIsIFwib3BlblwiLCBcInByZWZpeENsc1wiLCBcInBsYWNlbWVudFwiLCBcImxldmVsXCIsIFwibGV2ZWxNb3ZlXCIsIFwiZWFzZVwiLCBcImR1cmF0aW9uXCIsIFwiZ2V0Q29udGFpbmVyXCIsIFwiaGFuZGxlclwiLCBcIm9uQ2hhbmdlXCIsIFwiYWZ0ZXJWaXNpYmxlQ2hhbmdlXCIsIFwic2hvd01hc2tcIiwgXCJtYXNrQ2xvc2FibGVcIiwgXCJtYXNrU3R5bGVcIiwgXCJvbkNsb3NlXCIsIFwib25IYW5kbGVDbGlja1wiLCBcImtleWJvYXJkXCIsIFwiZ2V0T3BlbkNvdW50XCIsIFwic3dpdGNoU2Nyb2xsaW5nRWZmZWN0XCJdKTsgLy8g6aaW5qyh5riy5p+T6YO95bCG5piv5YWz6Zet54q25oCB44CCXG5cblxuICAgICAgdmFyIG9wZW4gPSB0aGlzLmRvbSA/ICRvcGVuIDogZmFsc2U7XG4gICAgICB2YXIgd3JhcHBlckNsYXNzTmFtZSA9IGNsYXNzbmFtZXMocHJlZml4Q2xzLCAoX2NsYXNzbmFtZXMgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzLCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLVwiKS5jb25jYXQocGxhY2VtZW50KSwgdHJ1ZSksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NuYW1lcywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1vcGVuXCIpLCBvcGVuKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzLCBjbGFzc05hbWUgfHwgJycsICEhY2xhc3NOYW1lKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzLCAnbm8tbWFzaycsICFzaG93TWFzayksIF9jbGFzc25hbWVzKSk7XG5cbiAgICAgIHZhciBfdGhpcyRnZXRIb3Jpem9udGFsQm8yID0gdGhpcy5nZXRIb3Jpem9udGFsQm9vbEFuZFBsYWNlbWVudE5hbWUoKSxcbiAgICAgICAgICBwbGFjZW1lbnROYW1lID0gX3RoaXMkZ2V0SG9yaXpvbnRhbEJvMi5wbGFjZW1lbnROYW1lOyAvLyDnmb7liIbmr5TkuI7lg4/ntKDliqjnlLvkuI3lkIzmraXvvIznrKzkuIDmrKHmiZPnlKjlkI7lhajnlKjlg4/ntKDliqjnlLvjgIJcbiAgICAgIC8vIGNvbnN0IGRlZmF1bHRWYWx1ZSA9ICF0aGlzLmNvbnRlbnREb20gfHwgIWxldmVsID8gJzEwMCUnIDogYCR7dmFsdWV9cHhgO1xuXG5cbiAgICAgIHZhciBwbGFjZW1lbnRQb3MgPSBwbGFjZW1lbnQgPT09ICdsZWZ0JyB8fCBwbGFjZW1lbnQgPT09ICd0b3AnID8gJy0xMDAlJyA6ICcxMDAlJztcbiAgICAgIHZhciB0cmFuc2Zvcm0gPSBvcGVuID8gJycgOiBcIlwiLmNvbmNhdChwbGFjZW1lbnROYW1lLCBcIihcIikuY29uY2F0KHBsYWNlbWVudFBvcywgXCIpXCIpO1xuICAgICAgdmFyIGhhbmRsZXJDaGlsZHJlbiA9IGhhbmRsZXIgJiYgUmVhY3QuY2xvbmVFbGVtZW50KGhhbmRsZXIsIHtcbiAgICAgICAgb25DbGljazogZnVuY3Rpb24gb25DbGljayhlKSB7XG4gICAgICAgICAgaWYgKGhhbmRsZXIucHJvcHMub25DbGljaykge1xuICAgICAgICAgICAgaGFuZGxlci5wcm9wcy5vbkNsaWNrKCk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKG9uSGFuZGxlQ2xpY2spIHtcbiAgICAgICAgICAgIG9uSGFuZGxlQ2xpY2soZSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihjKSB7XG4gICAgICAgICAgX3RoaXMzLmhhbmRsZXJEb20gPSBjO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwiZGl2XCIsIE9iamVjdC5hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgIHRhYkluZGV4OiAtMSxcbiAgICAgICAgY2xhc3NOYW1lOiB3cmFwcGVyQ2xhc3NOYW1lLFxuICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKGMpIHtcbiAgICAgICAgICBfdGhpczMuZG9tID0gYztcbiAgICAgICAgfSxcbiAgICAgICAgb25LZXlEb3duOiBvcGVuICYmIGtleWJvYXJkID8gdGhpcy5vbktleURvd24gOiB1bmRlZmluZWQsXG4gICAgICAgIG9uVHJhbnNpdGlvbkVuZDogdGhpcy5vbldyYXBwZXJUcmFuc2l0aW9uRW5kXG4gICAgICB9KSwgc2hvd01hc2sgJiYgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1tYXNrXCIpLFxuICAgICAgICBvbkNsaWNrOiBtYXNrQ2xvc2FibGUgPyBvbkNsb3NlIDogdW5kZWZpbmVkLFxuICAgICAgICBzdHlsZTogbWFza1N0eWxlLFxuICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihjKSB7XG4gICAgICAgICAgX3RoaXMzLm1hc2tEb20gPSBjO1xuICAgICAgICB9XG4gICAgICB9KSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jb250ZW50LXdyYXBwZXJcIiksXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm0sXG4gICAgICAgICAgbXNUcmFuc2Zvcm06IHRyYW5zZm9ybSxcbiAgICAgICAgICB3aWR0aDogaXNOdW1lcmljKHdpZHRoKSA/IFwiXCIuY29uY2F0KHdpZHRoLCBcInB4XCIpIDogd2lkdGgsXG4gICAgICAgICAgaGVpZ2h0OiBpc051bWVyaWMoaGVpZ2h0KSA/IFwiXCIuY29uY2F0KGhlaWdodCwgXCJweFwiKSA6IGhlaWdodFxuICAgICAgICB9LFxuICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihjKSB7XG4gICAgICAgICAgX3RoaXMzLmNvbnRlbnRXcmFwcGVyID0gYztcbiAgICAgICAgfVxuICAgICAgfSwgUmVhY3QuY3JlYXRlRWxlbWVudChcImRpdlwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jb250ZW50XCIpLFxuICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihjKSB7XG4gICAgICAgICAgX3RoaXMzLmNvbnRlbnREb20gPSBjO1xuICAgICAgICB9LFxuICAgICAgICBvblRvdWNoU3RhcnQ6IG9wZW4gJiYgc2hvd01hc2sgPyB0aGlzLnJlbW92ZVN0YXJ0SGFuZGxlciA6IHVuZGVmaW5lZCxcbiAgICAgICAgb25Ub3VjaE1vdmU6IG9wZW4gJiYgc2hvd01hc2sgPyB0aGlzLnJlbW92ZU1vdmVIYW5kbGVyIDogdW5kZWZpbmVkXG4gICAgICB9LCBjaGlsZHJlbiksIGhhbmRsZXJDaGlsZHJlbikpO1xuICAgIH1cbiAgfV0sIFt7XG4gICAga2V5OiBcImdldERlcml2ZWRTdGF0ZUZyb21Qcm9wc1wiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMocHJvcHMsIF9yZWYyKSB7XG4gICAgICB2YXIgcHJldlByb3BzID0gX3JlZjIucHJldlByb3BzLFxuICAgICAgICAgIF9zZWxmID0gX3JlZjIuX3NlbGY7XG4gICAgICB2YXIgbmV4dFN0YXRlID0ge1xuICAgICAgICBwcmV2UHJvcHM6IHByb3BzXG4gICAgICB9O1xuXG4gICAgICBpZiAocHJldlByb3BzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdmFyIHBsYWNlbWVudCA9IHByb3BzLnBsYWNlbWVudCxcbiAgICAgICAgICAgIGxldmVsID0gcHJvcHMubGV2ZWw7XG5cbiAgICAgICAgaWYgKHBsYWNlbWVudCAhPT0gcHJldlByb3BzLnBsYWNlbWVudCkge1xuICAgICAgICAgIC8vIHRlc3Qg55qEIGJ1Zywg5pyJ5Yqo55S76L+H5Zy677yM5Yig6ZmkIGRvbVxuICAgICAgICAgIF9zZWxmLmNvbnRlbnREb20gPSBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGxldmVsICE9PSBwcmV2UHJvcHMubGV2ZWwpIHtcbiAgICAgICAgICBfc2VsZi5nZXRMZXZlbERvbShwcm9wcyk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5leHRTdGF0ZTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gRHJhd2VyQ2hpbGQ7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5cbkRyYXdlckNoaWxkLmRlZmF1bHRQcm9wcyA9IHtcbiAgc3dpdGNoU2Nyb2xsaW5nRWZmZWN0OiBmdW5jdGlvbiBzd2l0Y2hTY3JvbGxpbmdFZmZlY3QoKSB7fVxufTtcbmV4cG9ydCBkZWZhdWx0IHBvbHlmaWxsKERyYXdlckNoaWxkKTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWkE7QUFDQTtBQWNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWpDQTtBQUNBO0FBbUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE5QkE7QUFnQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFwQkE7QUFzQkE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUEyQkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBOUZBO0FBZ0dBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhCQTtBQUNBO0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-drawer/es/DrawerChild.js
