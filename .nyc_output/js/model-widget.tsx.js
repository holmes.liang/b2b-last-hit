__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelWidget", function() { return ModelWidget; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelContainerWidget", function() { return ModelContainerWidget; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @model */ "./src/data-model/index.tsx");
/* harmony import */ var _widget__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./widget */ "./src/component/widget.tsx");












/**
 * model widget
 */

var ModelWidget =
/*#__PURE__*/
function (_Widget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ModelWidget, _Widget);

  function ModelWidget() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ModelWidget);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ModelWidget)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.dataChanged = function (event) {
      _this.setValueToDOM(event.newValue);

      _this.triggerForceListenEvent(event.oldValue);

      return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this);
    };

    _this.forceUpdateForVisible = function () {
      _this.forceUpdate();
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ModelWidget, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ModelWidget.prototype), "componentDidMount", this).call(this);

      this.installModelListeners();
      _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].bodyScaleContainer();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState, prevContext) {
      Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ModelWidget.prototype), "componentDidUpdate", this).call(this, prevProps, prevState, prevContext);

      this.installModelListeners();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ModelWidget.prototype), "componentWillUnmount", this).call(this);

      this.uninstallModelListeners();
    }
  }, {
    key: "isModelWatchable",
    value: function isModelWatchable(model, propName) {
      return _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].isProxied(model) && !!(propName ? propName : this.getPropName());
    }
  }, {
    key: "installModelListeners",
    value: function installModelListeners() {
      if (this.isModelWatchable(this.getModel())) {
        this.getModel().$$on(this.getPropName(), this.dataChanged);
      }

      this.installVisibleListeners();
      return this;
    }
  }, {
    key: "uninstallModelListeners",
    value: function uninstallModelListeners() {
      if (this.isModelWatchable(this.getModel())) {
        this.getModel().$$off(this.getPropName(), this.dataChanged);
      }

      this.uninstallVisibleListeners();
      return this;
    }
  }, {
    key: "getPropName",
    value: function getPropName() {
      return this.props.propName ? this.props.propName : this.props.id ? this.props.id : "";
    }
  }, {
    key: "getModel",
    value: function getModel() {
      return this.props.model || {};
    }
  }, {
    key: "getForm",
    value: function getForm() {
      return this.props.form;
    }
  }, {
    key: "getRootModel",
    value: function getRootModel() {
      return this.props.root || this.getModel();
    }
  }, {
    key: "getArrayHolder",
    value: function getArrayHolder() {
      return this.props.arrayHolder;
    }
  }, {
    key: "getValueFromModel",
    value: function getValueFromModel(propName) {
      var prop = (propName || this.getPropName()).replace(/-/g, ".");
      return _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].getValue({
        model: this.getModel(),
        root: this.getRootModel()
      }, prop);
    }
  }, {
    key: "setValueToDOM",
    value: function setValueToDOM(value, propName) {
      this.forceUpdate();
      return this;
    }
  }, {
    key: "setValuesToModel",
    value: function setValuesToModel(obj) {
      var _this2 = this;

      new Map(Object.entries(obj)).forEach(function (value, key) {
        _this2.setValueToModel(value, key);
      });
      return this;
    }
  }, {
    key: "setValueToModel",
    value: function setValueToModel(value, propName) {
      //set value to antd form
      var form = this.getForm();

      if (form) {
        var formPropName = propName || this.getPropName();
        var formFields = new Map(Object.entries(form.getFieldsValue()));

        if (formFields.has(formPropName)) {
          form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, propName || this.getPropName(), value));
        }
      }

      var prop = (propName || this.getPropName()).replace(/-/g, ".");
      var oldValue = this.getValueFromModel(prop);

      if (oldValue === value) {
        if (_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isArray(oldValue) || _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isArray(value)) {
          return this.setValueToDOM(value, prop);
        }

        return this;
      }

      _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].setValue({
        model: this.getModel(),
        root: this.getRootModel()
      }, prop, value);
      return this.setValueToDOM(value, prop);
    }
  }, {
    key: "isVisible",
    value: function isVisible() {
      var visible = this.getVisibleTrigger();

      if (_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isBoolean(visible)) {
        return visible;
      } else if (visible.length === 0) {
        return true;
      } else {
        var matchedFilter = this.matchFilter(visible);

        if (matchedFilter == null) {
          return false;
        } else {
          return matchedFilter.do || _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isUndefined(matchedFilter.do);
        }
      }
    }
  }, {
    key: "getVisibleTrigger",
    value: function getVisibleTrigger() {
      var visible = this.props.visible;

      if (_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isBoolean(visible)) {
        return visible;
      } else {
        return _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].toArray(visible).map(function (filter) {
          if (_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isString(filter)) {
            return {
              on: filter,
              do: true
            };
          } else {
            return filter;
          }
        });
      }
    }
  }, {
    key: "installVisibleListeners",
    value: function installVisibleListeners() {
      var _this3 = this;

      if (this.getModel().$$proxied) {
        return this.careForVisibleListeners(function (item) {
          return _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].asProxied(item.model).$$on(item.propName, _this3.forceUpdateForVisible);
        });
      } else {
        return this;
      }
    }
  }, {
    key: "uninstallVisibleListeners",
    value: function uninstallVisibleListeners() {
      var _this4 = this;

      if (this.getModel().$$proxied) {
        return this.careForVisibleListeners(function (item) {
          return _model__WEBPACK_IMPORTED_MODULE_10__["Modeller"].asProxied(item.model).$$off(item.propName, _this4.forceUpdateForVisible);
        });
      } else {
        return this;
      }
    }
  }, {
    key: "careForVisibleListeners",
    value: function careForVisibleListeners(handler) {
      var visible = this.getVisibleTrigger();

      if (!_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isBoolean(visible)) {
        _model__WEBPACK_IMPORTED_MODULE_10__["Filter"].getConcernedIdsOfFilters({
          filters: visible,
          root: this.getRootModel(),
          model: this.getModel(),
          caller: this
        }).forEach(handler);
      }

      return this;
    }
  }, {
    key: "matchFilter",
    value: function matchFilter(filters) {
      if (filters == null) {
        return undefined;
      }

      return _model__WEBPACK_IMPORTED_MODULE_10__["Filter"].findMatchedFilter({
        filters: _common__WEBPACK_IMPORTED_MODULE_8__["Utils"].toArray(filters),
        root: this.getRootModel(),
        model: this.getModel(),
        arrayHolder: this.getArrayHolder(),
        caller: this
      });
    }
  }, {
    key: "getForceListener",
    value: function getForceListener() {
      return this.props.listen;
    }
  }, {
    key: "triggerForceListenEvent",
    value: function triggerForceListenEvent(oldValue) {
      var listener = this.getForceListener();

      if (listener) {
        listener.call(this, {
          invoker: this,
          model: this.getModel(),
          root: this.getRootModel(),
          arrayHolder: this.getArrayHolder(),
          value: this.getValueFromModel(),
          oldValue: oldValue
        });
      }
    }
  }]);

  return ModelWidget;
}(_widget__WEBPACK_IMPORTED_MODULE_11__["default"]);
/**
 * model container widget
 */


var ModelContainerWidget =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(ModelContainerWidget, _ModelWidget);

  function ModelContainerWidget() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ModelContainerWidget);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(ModelContainerWidget).apply(this, arguments));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ModelContainerWidget, [{
    key: "renderChild",
    value: function renderChild(child, additionalProps) {
      if (child == null) {
        return null;
      }

      if (child.type && !_common__WEBPACK_IMPORTED_MODULE_8__["Utils"].isString(child.type)) {
        var childProps = child.props;
        var overwrite = childProps.overwriteModel === true;
        return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].cloneElement(child, Object.assign({
          model: overwrite ? childProps.model || this.getModel() : this.getModel(),
          root: overwrite ? childProps.root || this.getRootModel() : this.getRootModel(),
          arrayHolder: overwrite ? childProps.arrayHolder || this.getArrayHolder() : this.getArrayHolder(),
          parentContainer: this
        }, additionalProps), childProps.children);
      } else {
        return child;
      }
    }
  }, {
    key: "renderChildren",
    value: function renderChildren() {
      var _this5 = this;

      return _common_3rd__WEBPACK_IMPORTED_MODULE_9__["React"].Children.map(this.props.children, function (child) {
        return _this5.renderChild(child);
      });
    }
  }]);

  return ModelContainerWidget;
}(ModelWidget);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tcG9uZW50L21vZGVsLXdpZGdldC50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21wb25lbnQvbW9kZWwtd2lkZ2V0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBSZWFjdCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgRmlsdGVyLCBNb2RlbGxlciB9IGZyb20gXCJAbW9kZWxcIjtcbmltcG9ydCB7XG4gIEZpbHRlcmFibGUsXG4gIEZvcmNlTGlzdGVuZXIsXG4gIE1vZGVsRGF0YUNoYW5nZWRFdmVudCxcbiAgTW9kZWxXaWRnZXRJbnRlcmZhY2UsXG4gIE1vZGVsV2lkZ2V0UHJvcHMsXG4gIFZpc2libGVGaWx0ZXJhYmxlLFxufSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgV2lkZ2V0IGZyb20gXCIuL3dpZGdldFwiO1xuXG4vKipcbiAqIG1vZGVsIHdpZGdldFxuICovXG5hYnN0cmFjdCBjbGFzcyBNb2RlbFdpZGdldDxQIGV4dGVuZHMgTW9kZWxXaWRnZXRQcm9wcywgUywgQz4gZXh0ZW5kcyBXaWRnZXQ8UCwgUywgQz4gaW1wbGVtZW50cyBNb2RlbFdpZGdldEludGVyZmFjZSB7XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHN1cGVyLmNvbXBvbmVudERpZE1vdW50KCk7XG4gICAgdGhpcy5pbnN0YWxsTW9kZWxMaXN0ZW5lcnMoKTtcbiAgICBVdGlscy5ib2R5U2NhbGVDb250YWluZXIoKTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHM6IGFueSwgcHJldlN0YXRlPzogYW55LCBwcmV2Q29udGV4dD86IGFueSkge1xuICAgIHN1cGVyLmNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSwgcHJldkNvbnRleHQpO1xuICAgIHRoaXMuaW5zdGFsbE1vZGVsTGlzdGVuZXJzKCk7XG4gIH1cblxuICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICBzdXBlci5jb21wb25lbnRXaWxsVW5tb3VudCgpO1xuICAgIHRoaXMudW5pbnN0YWxsTW9kZWxMaXN0ZW5lcnMoKTtcbiAgfVxuXG4gIHByb3RlY3RlZCBpc01vZGVsV2F0Y2hhYmxlKG1vZGVsOiBhbnksIHByb3BOYW1lPzogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIE1vZGVsbGVyLmlzUHJveGllZChtb2RlbCkgJiYgISEocHJvcE5hbWUgPyBwcm9wTmFtZSA6IHRoaXMuZ2V0UHJvcE5hbWUoKSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5zdGFsbE1vZGVsTGlzdGVuZXJzKCk6IHRoaXMge1xuICAgIGlmICh0aGlzLmlzTW9kZWxXYXRjaGFibGUodGhpcy5nZXRNb2RlbCgpKSkge1xuICAgICAgdGhpcy5nZXRNb2RlbCgpLiQkb24odGhpcy5nZXRQcm9wTmFtZSgpLCB0aGlzLmRhdGFDaGFuZ2VkKTtcbiAgICB9XG4gICAgdGhpcy5pbnN0YWxsVmlzaWJsZUxpc3RlbmVycygpO1xuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBwcm90ZWN0ZWQgdW5pbnN0YWxsTW9kZWxMaXN0ZW5lcnMoKTogdGhpcyB7XG4gICAgaWYgKHRoaXMuaXNNb2RlbFdhdGNoYWJsZSh0aGlzLmdldE1vZGVsKCkpKSB7XG4gICAgICB0aGlzLmdldE1vZGVsKCkuJCRvZmYodGhpcy5nZXRQcm9wTmFtZSgpLCB0aGlzLmRhdGFDaGFuZ2VkKTtcbiAgICB9XG4gICAgdGhpcy51bmluc3RhbGxWaXNpYmxlTGlzdGVuZXJzKCk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBnZXRQcm9wTmFtZSgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLnByb3BzLnByb3BOYW1lID8gKHRoaXMucHJvcHMucHJvcE5hbWUgYXMgc3RyaW5nKSA6IHRoaXMucHJvcHMuaWQgPyAodGhpcy5wcm9wcy5pZCBhcyBzdHJpbmcpIDogXCJcIjtcbiAgfVxuXG4gIGdldE1vZGVsKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMubW9kZWwgfHwge307XG4gIH1cblxuICBnZXRGb3JtKCk6IGFueSB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMuZm9ybTtcbiAgfVxuXG4gIGdldFJvb3RNb2RlbCgpOiBhbnkge1xuICAgIHJldHVybiB0aGlzLnByb3BzLnJvb3QgfHwgdGhpcy5nZXRNb2RlbCgpO1xuICB9XG5cbiAgZ2V0QXJyYXlIb2xkZXIoKTogYW55IHtcbiAgICByZXR1cm4gdGhpcy5wcm9wcy5hcnJheUhvbGRlcjtcbiAgfVxuXG4gIHByb3RlY3RlZCBnZXRWYWx1ZUZyb21Nb2RlbChwcm9wTmFtZT86IHN0cmluZyk6IGFueSB7XG4gICAgY29uc3QgcHJvcCA9IChwcm9wTmFtZSB8fCB0aGlzLmdldFByb3BOYW1lKCkpLnJlcGxhY2UoLy0vZywgXCIuXCIpO1xuICAgIHJldHVybiBNb2RlbGxlci5nZXRWYWx1ZSh7IG1vZGVsOiB0aGlzLmdldE1vZGVsKCksIHJvb3Q6IHRoaXMuZ2V0Um9vdE1vZGVsKCkgfSwgcHJvcCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgc2V0VmFsdWVUb0RPTSh2YWx1ZTogYW55LCBwcm9wTmFtZT86IHN0cmluZyk6IHRoaXMge1xuICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXRWYWx1ZXNUb01vZGVsKG9iajogT2JqZWN0KTogdGhpcyB7XG4gICAgbmV3IE1hcChPYmplY3QuZW50cmllcyhvYmopKS5mb3JFYWNoKCh2YWx1ZTogYW55LCBrZXk6IHN0cmluZykgPT4ge1xuICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwodmFsdWUsIGtleSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfVxuXG4gIHByb3RlY3RlZCBzZXRWYWx1ZVRvTW9kZWwodmFsdWU6IGFueSwgcHJvcE5hbWU/OiBzdHJpbmcpOiB0aGlzIHtcbiAgICAvL3NldCB2YWx1ZSB0byBhbnRkIGZvcm1cbiAgICBjb25zdCBmb3JtID0gdGhpcy5nZXRGb3JtKCk7XG4gICAgaWYgKGZvcm0pIHtcbiAgICAgIGNvbnN0IGZvcm1Qcm9wTmFtZSA9IHByb3BOYW1lIHx8IHRoaXMuZ2V0UHJvcE5hbWUoKTtcbiAgICAgIGNvbnN0IGZvcm1GaWVsZHMgPSBuZXcgTWFwKE9iamVjdC5lbnRyaWVzKGZvcm0uZ2V0RmllbGRzVmFsdWUoKSkpO1xuICAgICAgaWYgKGZvcm1GaWVsZHMuaGFzKGZvcm1Qcm9wTmFtZSkpIHtcbiAgICAgICAgZm9ybS5zZXRGaWVsZHNWYWx1ZSh7IFtwcm9wTmFtZSB8fCB0aGlzLmdldFByb3BOYW1lKCldOiB2YWx1ZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgY29uc3QgcHJvcCA9IChwcm9wTmFtZSB8fCB0aGlzLmdldFByb3BOYW1lKCkpLnJlcGxhY2UoLy0vZywgXCIuXCIpO1xuICAgIGxldCBvbGRWYWx1ZSA9IHRoaXMuZ2V0VmFsdWVGcm9tTW9kZWwocHJvcCk7XG4gICAgaWYgKG9sZFZhbHVlID09PSB2YWx1ZSkge1xuICAgICAgaWYgKFV0aWxzLmlzQXJyYXkob2xkVmFsdWUpIHx8IFV0aWxzLmlzQXJyYXkodmFsdWUpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNldFZhbHVlVG9ET00odmFsdWUsIHByb3ApO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIE1vZGVsbGVyLnNldFZhbHVlKHsgbW9kZWw6IHRoaXMuZ2V0TW9kZWwoKSwgcm9vdDogdGhpcy5nZXRSb290TW9kZWwoKSB9LCBwcm9wLCB2YWx1ZSk7XG4gICAgcmV0dXJuIHRoaXMuc2V0VmFsdWVUb0RPTSh2YWx1ZSwgcHJvcCk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZGF0YUNoYW5nZWQgPSAoZXZlbnQ6IE1vZGVsRGF0YUNoYW5nZWRFdmVudCk6IHRoaXMgPT4ge1xuICAgIHRoaXMuc2V0VmFsdWVUb0RPTShldmVudC5uZXdWYWx1ZSk7XG4gICAgdGhpcy50cmlnZ2VyRm9yY2VMaXN0ZW5FdmVudChldmVudC5vbGRWYWx1ZSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH07XG5cbiAgcHJvdGVjdGVkIGlzVmlzaWJsZSgpOiBib29sZWFuIHtcbiAgICBjb25zdCB2aXNpYmxlID0gdGhpcy5nZXRWaXNpYmxlVHJpZ2dlcigpO1xuICAgIGlmIChVdGlscy5pc0Jvb2xlYW4odmlzaWJsZSkpIHtcbiAgICAgIHJldHVybiB2aXNpYmxlO1xuICAgIH0gZWxzZSBpZiAodmlzaWJsZS5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCBtYXRjaGVkRmlsdGVyID0gdGhpcy5tYXRjaEZpbHRlcih2aXNpYmxlKSBhcyBWaXNpYmxlRmlsdGVyYWJsZTtcbiAgICAgIGlmIChtYXRjaGVkRmlsdGVyID09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIG1hdGNoZWRGaWx0ZXIuZG8gfHwgVXRpbHMuaXNVbmRlZmluZWQobWF0Y2hlZEZpbHRlci5kbyk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGdldFZpc2libGVUcmlnZ2VyKCk6IFZpc2libGVGaWx0ZXJhYmxlW10gfCBib29sZWFuIHtcbiAgICBjb25zdCB2aXNpYmxlID0gdGhpcy5wcm9wcy52aXNpYmxlO1xuICAgIGlmIChVdGlscy5pc0Jvb2xlYW4odmlzaWJsZSkpIHtcbiAgICAgIHJldHVybiB2aXNpYmxlO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gVXRpbHMudG9BcnJheSh2aXNpYmxlIGFzIGFueSkubWFwKChmaWx0ZXI6IHN0cmluZyB8IFZpc2libGVGaWx0ZXJhYmxlKSA9PiB7XG4gICAgICAgIGlmIChVdGlscy5pc1N0cmluZyhmaWx0ZXIpKSB7XG4gICAgICAgICAgcmV0dXJuIHsgb246IGZpbHRlciwgZG86IHRydWUgfSBhcyBWaXNpYmxlRmlsdGVyYWJsZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gZmlsdGVyO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBwcm90ZWN0ZWQgZm9yY2VVcGRhdGVGb3JWaXNpYmxlID0gKCkgPT4ge1xuICAgIHRoaXMuZm9yY2VVcGRhdGUoKTtcbiAgfTtcblxuICBwcm90ZWN0ZWQgaW5zdGFsbFZpc2libGVMaXN0ZW5lcnMoKTogdGhpcyB7XG4gICAgaWYgKHRoaXMuZ2V0TW9kZWwoKS4kJHByb3hpZWQpIHtcbiAgICAgIHJldHVybiB0aGlzLmNhcmVGb3JWaXNpYmxlTGlzdGVuZXJzKGl0ZW0gPT5cbiAgICAgICAgTW9kZWxsZXIuYXNQcm94aWVkKGl0ZW0ubW9kZWwpLiQkb24oaXRlbS5wcm9wTmFtZSwgdGhpcy5mb3JjZVVwZGF0ZUZvclZpc2libGUpLFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIHVuaW5zdGFsbFZpc2libGVMaXN0ZW5lcnMoKTogdGhpcyB7XG4gICAgaWYgKHRoaXMuZ2V0TW9kZWwoKS4kJHByb3hpZWQpIHtcbiAgICAgIHJldHVybiB0aGlzLmNhcmVGb3JWaXNpYmxlTGlzdGVuZXJzKGl0ZW0gPT5cbiAgICAgICAgTW9kZWxsZXIuYXNQcm94aWVkKGl0ZW0ubW9kZWwpLiQkb2ZmKGl0ZW0ucHJvcE5hbWUsIHRoaXMuZm9yY2VVcGRhdGVGb3JWaXNpYmxlKSxcbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgY2FyZUZvclZpc2libGVMaXN0ZW5lcnMoaGFuZGxlcjogKGl0ZW06IHsgcHJvcE5hbWU6IHN0cmluZzsgbW9kZWw6IGFueSB9KSA9PiB2b2lkKTogdGhpcyB7XG4gICAgY29uc3QgdmlzaWJsZSA9IHRoaXMuZ2V0VmlzaWJsZVRyaWdnZXIoKTtcbiAgICBpZiAoIVV0aWxzLmlzQm9vbGVhbih2aXNpYmxlKSkge1xuICAgICAgRmlsdGVyLmdldENvbmNlcm5lZElkc09mRmlsdGVycyh7XG4gICAgICAgIGZpbHRlcnM6IHZpc2libGUsXG4gICAgICAgIHJvb3Q6IHRoaXMuZ2V0Um9vdE1vZGVsKCksXG4gICAgICAgIG1vZGVsOiB0aGlzLmdldE1vZGVsKCksXG4gICAgICAgIGNhbGxlcjogdGhpcyxcbiAgICAgIH0pLmZvckVhY2goaGFuZGxlcik7XG4gICAgfVxuICAgIHJldHVybiB0aGlzO1xuICB9XG5cbiAgcHJvdGVjdGVkIG1hdGNoRmlsdGVyKGZpbHRlcnM/OiBGaWx0ZXJhYmxlW10gfCBGaWx0ZXJhYmxlIHwgbnVsbCk6IEZpbHRlcmFibGUgfCB1bmRlZmluZWQge1xuICAgIGlmIChmaWx0ZXJzID09IG51bGwpIHtcbiAgICAgIHJldHVybiB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIEZpbHRlci5maW5kTWF0Y2hlZEZpbHRlcih7XG4gICAgICBmaWx0ZXJzOiBVdGlscy50b0FycmF5KGZpbHRlcnMpLFxuICAgICAgcm9vdDogdGhpcy5nZXRSb290TW9kZWwoKSxcbiAgICAgIG1vZGVsOiB0aGlzLmdldE1vZGVsKCksXG4gICAgICBhcnJheUhvbGRlcjogdGhpcy5nZXRBcnJheUhvbGRlcigpLFxuICAgICAgY2FsbGVyOiB0aGlzLFxuICAgIH0pO1xuICB9XG5cbiAgcHJvdGVjdGVkIGdldEZvcmNlTGlzdGVuZXIoKTogRm9yY2VMaXN0ZW5lciB8IHVuZGVmaW5lZCB7XG4gICAgcmV0dXJuIHRoaXMucHJvcHMubGlzdGVuO1xuICB9XG5cbiAgcHJvdGVjdGVkIHRyaWdnZXJGb3JjZUxpc3RlbkV2ZW50KG9sZFZhbHVlOiBhbnkpOiB2b2lkIHtcbiAgICBjb25zdCBsaXN0ZW5lciA9IHRoaXMuZ2V0Rm9yY2VMaXN0ZW5lcigpO1xuICAgIGlmIChsaXN0ZW5lcikge1xuICAgICAgbGlzdGVuZXIuY2FsbCh0aGlzLCB7XG4gICAgICAgIGludm9rZXI6IHRoaXMsXG4gICAgICAgIG1vZGVsOiB0aGlzLmdldE1vZGVsKCksXG4gICAgICAgIHJvb3Q6IHRoaXMuZ2V0Um9vdE1vZGVsKCksXG4gICAgICAgIGFycmF5SG9sZGVyOiB0aGlzLmdldEFycmF5SG9sZGVyKCksXG4gICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRnJvbU1vZGVsKCksXG4gICAgICAgIG9sZFZhbHVlLFxuICAgICAgfSk7XG4gICAgfVxuICB9XG59XG5cbi8qKlxuICogbW9kZWwgY29udGFpbmVyIHdpZGdldFxuICovXG5hYnN0cmFjdCBjbGFzcyBNb2RlbENvbnRhaW5lcldpZGdldDxQIGV4dGVuZHMgTW9kZWxXaWRnZXRQcm9wcywgUywgQz4gZXh0ZW5kcyBNb2RlbFdpZGdldDxQLCBTLCBDPiB7XG4gIHByb3RlY3RlZCByZW5kZXJDaGlsZChjaGlsZDogYW55LCBhZGRpdGlvbmFsUHJvcHM/OiBhbnkpOiBhbnkge1xuICAgIGlmIChjaGlsZCA9PSBudWxsKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgaWYgKGNoaWxkLnR5cGUgJiYgIVV0aWxzLmlzU3RyaW5nKGNoaWxkLnR5cGUpKSB7XG4gICAgICBjb25zdCBjaGlsZFByb3BzID0gY2hpbGQucHJvcHM7XG4gICAgICBjb25zdCBvdmVyd3JpdGUgPSBjaGlsZFByb3BzLm92ZXJ3cml0ZU1vZGVsID09PSB0cnVlO1xuICAgICAgcmV0dXJuIFJlYWN0LmNsb25lRWxlbWVudChcbiAgICAgICAgY2hpbGQsXG4gICAgICAgIE9iamVjdC5hc3NpZ24oXG4gICAgICAgICAge1xuICAgICAgICAgICAgbW9kZWw6IG92ZXJ3cml0ZSA/IGNoaWxkUHJvcHMubW9kZWwgfHwgdGhpcy5nZXRNb2RlbCgpIDogdGhpcy5nZXRNb2RlbCgpLFxuICAgICAgICAgICAgcm9vdDogb3ZlcndyaXRlID8gY2hpbGRQcm9wcy5yb290IHx8IHRoaXMuZ2V0Um9vdE1vZGVsKCkgOiB0aGlzLmdldFJvb3RNb2RlbCgpLFxuICAgICAgICAgICAgYXJyYXlIb2xkZXI6IG92ZXJ3cml0ZVxuICAgICAgICAgICAgICA/IGNoaWxkUHJvcHMuYXJyYXlIb2xkZXIgfHwgdGhpcy5nZXRBcnJheUhvbGRlcigpXG4gICAgICAgICAgICAgIDogdGhpcy5nZXRBcnJheUhvbGRlcigpLFxuICAgICAgICAgICAgcGFyZW50Q29udGFpbmVyOiB0aGlzLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BzLFxuICAgICAgICApLFxuICAgICAgICBjaGlsZFByb3BzLmNoaWxkcmVuLFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIGNoaWxkO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCByZW5kZXJDaGlsZHJlbigpOiBhbnkge1xuICAgIHJldHVybiBSZWFjdC5DaGlsZHJlbi5tYXAodGhpcy5wcm9wcy5jaGlsZHJlbiwgKGNoaWxkOiBhbnkpID0+IHtcbiAgICAgIHJldHVybiB0aGlzLnJlbmRlckNoaWxkKGNoaWxkKTtcbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgeyBNb2RlbFdpZGdldCwgTW9kZWxDb250YWluZXJXaWRnZXQgfTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFTQTtBQUVBOzs7O0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBZ0NBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUF6SUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7O0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFNQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBOzs7O0FBMU1BO0FBNk1BOzs7OztBQUdBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBR0E7QUFOQTtBQVlBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7OztBQWhDQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/component/model-widget.tsx
