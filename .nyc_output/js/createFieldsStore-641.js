__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return createFieldsStore; });
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var lodash_set__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash/set */ "./node_modules/lodash/set.js");
/* harmony import */ var lodash_set__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash_set__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _createFormField__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./createFormField */ "./node_modules/rc-form/es/createFormField.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./utils */ "./node_modules/rc-form/es/utils.js");








function partOf(a, b) {
  return b.indexOf(a) === 0 && ['.', '['].indexOf(b[a.length]) !== -1;
}

function internalFlattenFields(fields) {
  return Object(_utils__WEBPACK_IMPORTED_MODULE_6__["flattenFields"])(fields, function (_, node) {
    return Object(_createFormField__WEBPACK_IMPORTED_MODULE_5__["isFormField"])(node);
  }, 'You must wrap field data with `createFormField`.');
}

var FieldsStore = function () {
  function FieldsStore(fields) {
    babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, FieldsStore);

    _initialiseProps.call(this);

    this.fields = internalFlattenFields(fields);
    this.fieldsMeta = {};
  }

  babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(FieldsStore, [{
    key: 'updateFields',
    value: function updateFields(fields) {
      this.fields = internalFlattenFields(fields);
    }
  }, {
    key: 'flattenRegisteredFields',
    value: function flattenRegisteredFields(fields) {
      var validFieldsName = this.getAllFieldsName();
      return Object(_utils__WEBPACK_IMPORTED_MODULE_6__["flattenFields"])(fields, function (path) {
        return validFieldsName.indexOf(path) >= 0;
      }, 'You cannot set a form field before rendering a field associated with the value.');
    }
  }, {
    key: 'setFields',
    value: function setFields(fields) {
      var _this = this;

      var fieldsMeta = this.fieldsMeta;

      var nowFields = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, this.fields, fields);

      var nowValues = {};
      Object.keys(fieldsMeta).forEach(function (f) {
        nowValues[f] = _this.getValueFromFields(f, nowFields);
      });
      Object.keys(nowValues).forEach(function (f) {
        var value = nowValues[f];

        var fieldMeta = _this.getFieldMeta(f);

        if (fieldMeta && fieldMeta.normalize) {
          var nowValue = fieldMeta.normalize(value, _this.getValueFromFields(f, _this.fields), nowValues);

          if (nowValue !== value) {
            nowFields[f] = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, nowFields[f], {
              value: nowValue
            });
          }
        }
      });
      this.fields = nowFields;
    }
  }, {
    key: 'resetFields',
    value: function resetFields(ns) {
      var fields = this.fields;
      var names = ns ? this.getValidFieldsFullName(ns) : this.getAllFieldsName();
      return names.reduce(function (acc, name) {
        var field = fields[name];

        if (field && 'value' in field) {
          acc[name] = {};
        }

        return acc;
      }, {});
    }
  }, {
    key: 'setFieldMeta',
    value: function setFieldMeta(name, meta) {
      this.fieldsMeta[name] = meta;
    }
  }, {
    key: 'setFieldsAsDirty',
    value: function setFieldsAsDirty() {
      var _this2 = this;

      Object.keys(this.fields).forEach(function (name) {
        var field = _this2.fields[name];
        var fieldMeta = _this2.fieldsMeta[name];

        if (field && fieldMeta && Object(_utils__WEBPACK_IMPORTED_MODULE_6__["hasRules"])(fieldMeta.validate)) {
          _this2.fields[name] = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, field, {
            dirty: true
          });
        }
      });
    }
  }, {
    key: 'getFieldMeta',
    value: function getFieldMeta(name) {
      this.fieldsMeta[name] = this.fieldsMeta[name] || {};
      return this.fieldsMeta[name];
    }
  }, {
    key: 'getValueFromFields',
    value: function getValueFromFields(name, fields) {
      var field = fields[name];

      if (field && 'value' in field) {
        return field.value;
      }

      var fieldMeta = this.getFieldMeta(name);
      return fieldMeta && fieldMeta.initialValue;
    }
  }, {
    key: 'getValidFieldsName',
    value: function getValidFieldsName() {
      var _this3 = this;

      var fieldsMeta = this.fieldsMeta;
      return fieldsMeta ? Object.keys(fieldsMeta).filter(function (name) {
        return !_this3.getFieldMeta(name).hidden;
      }) : [];
    }
  }, {
    key: 'getAllFieldsName',
    value: function getAllFieldsName() {
      var fieldsMeta = this.fieldsMeta;
      return fieldsMeta ? Object.keys(fieldsMeta) : [];
    }
  }, {
    key: 'getValidFieldsFullName',
    value: function getValidFieldsFullName(maybePartialName) {
      var maybePartialNames = Array.isArray(maybePartialName) ? maybePartialName : [maybePartialName];
      return this.getValidFieldsName().filter(function (fullName) {
        return maybePartialNames.some(function (partialName) {
          return fullName === partialName || Object(_utils__WEBPACK_IMPORTED_MODULE_6__["startsWith"])(fullName, partialName) && ['.', '['].indexOf(fullName[partialName.length]) >= 0;
        });
      });
    }
  }, {
    key: 'getFieldValuePropValue',
    value: function getFieldValuePropValue(fieldMeta) {
      var name = fieldMeta.name,
          getValueProps = fieldMeta.getValueProps,
          valuePropName = fieldMeta.valuePropName;
      var field = this.getField(name);
      var fieldValue = 'value' in field ? field.value : fieldMeta.initialValue;

      if (getValueProps) {
        return getValueProps(fieldValue);
      }

      return babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()({}, valuePropName, fieldValue);
    }
  }, {
    key: 'getField',
    value: function getField(name) {
      return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, this.fields[name], {
        name: name
      });
    }
  }, {
    key: 'getNotCollectedFields',
    value: function getNotCollectedFields() {
      var _this4 = this;

      var fieldsName = this.getValidFieldsName();
      return fieldsName.filter(function (name) {
        return !_this4.fields[name];
      }).map(function (name) {
        return {
          name: name,
          dirty: false,
          value: _this4.getFieldMeta(name).initialValue
        };
      }).reduce(function (acc, field) {
        return lodash_set__WEBPACK_IMPORTED_MODULE_4___default()(acc, field.name, Object(_createFormField__WEBPACK_IMPORTED_MODULE_5__["default"])(field));
      }, {});
    }
  }, {
    key: 'getNestedAllFields',
    value: function getNestedAllFields() {
      var _this5 = this;

      return Object.keys(this.fields).reduce(function (acc, name) {
        return lodash_set__WEBPACK_IMPORTED_MODULE_4___default()(acc, name, Object(_createFormField__WEBPACK_IMPORTED_MODULE_5__["default"])(_this5.fields[name]));
      }, this.getNotCollectedFields());
    }
  }, {
    key: 'getFieldMember',
    value: function getFieldMember(name, member) {
      return this.getField(name)[member];
    }
  }, {
    key: 'getNestedFields',
    value: function getNestedFields(names, getter) {
      var fields = names || this.getValidFieldsName();
      return fields.reduce(function (acc, f) {
        return lodash_set__WEBPACK_IMPORTED_MODULE_4___default()(acc, f, getter(f));
      }, {});
    }
  }, {
    key: 'getNestedField',
    value: function getNestedField(name, getter) {
      var fullNames = this.getValidFieldsFullName(name);

      if (fullNames.length === 0 || // Not registered
      fullNames.length === 1 && fullNames[0] === name // Name already is full name.
      ) {
          return getter(name);
        }

      var isArrayValue = fullNames[0][name.length] === '[';
      var suffixNameStartIndex = isArrayValue ? name.length : name.length + 1;
      return fullNames.reduce(function (acc, fullName) {
        return lodash_set__WEBPACK_IMPORTED_MODULE_4___default()(acc, fullName.slice(suffixNameStartIndex), getter(fullName));
      }, isArrayValue ? [] : {});
    }
  }, {
    key: 'isValidNestedFieldName',
    // @private
    // BG: `a` and `a.b` cannot be use in the same form
    value: function isValidNestedFieldName(name) {
      var names = this.getAllFieldsName();
      return names.every(function (n) {
        return !partOf(n, name) && !partOf(name, n);
      });
    }
  }, {
    key: 'clearField',
    value: function clearField(name) {
      delete this.fields[name];
      delete this.fieldsMeta[name];
    }
  }]);

  return FieldsStore;
}();

var _initialiseProps = function _initialiseProps() {
  var _this6 = this;

  this.setFieldsInitialValue = function (initialValues) {
    var flattenedInitialValues = _this6.flattenRegisteredFields(initialValues);

    var fieldsMeta = _this6.fieldsMeta;
    Object.keys(flattenedInitialValues).forEach(function (name) {
      if (fieldsMeta[name]) {
        _this6.setFieldMeta(name, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, _this6.getFieldMeta(name), {
          initialValue: flattenedInitialValues[name]
        }));
      }
    });
  };

  this.getAllValues = function () {
    var fieldsMeta = _this6.fieldsMeta,
        fields = _this6.fields;
    return Object.keys(fieldsMeta).reduce(function (acc, name) {
      return lodash_set__WEBPACK_IMPORTED_MODULE_4___default()(acc, name, _this6.getValueFromFields(name, fields));
    }, {});
  };

  this.getFieldsValue = function (names) {
    return _this6.getNestedFields(names, _this6.getFieldValue);
  };

  this.getFieldValue = function (name) {
    var fields = _this6.fields;
    return _this6.getNestedField(name, function (fullName) {
      return _this6.getValueFromFields(fullName, fields);
    });
  };

  this.getFieldsError = function (names) {
    return _this6.getNestedFields(names, _this6.getFieldError);
  };

  this.getFieldError = function (name) {
    return _this6.getNestedField(name, function (fullName) {
      return Object(_utils__WEBPACK_IMPORTED_MODULE_6__["getErrorStrs"])(_this6.getFieldMember(fullName, 'errors'));
    });
  };

  this.isFieldValidating = function (name) {
    return _this6.getFieldMember(name, 'validating');
  };

  this.isFieldsValidating = function (ns) {
    var names = ns || _this6.getValidFieldsName();

    return names.some(function (n) {
      return _this6.isFieldValidating(n);
    });
  };

  this.isFieldTouched = function (name) {
    return _this6.getFieldMember(name, 'touched');
  };

  this.isFieldsTouched = function (ns) {
    var names = ns || _this6.getValidFieldsName();

    return names.some(function (n) {
      return _this6.isFieldTouched(n);
    });
  };
};

function createFieldsStore(fields) {
  return new FieldsStore(fields);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZm9ybS9lcy9jcmVhdGVGaWVsZHNTdG9yZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLWZvcm0vZXMvY3JlYXRlRmllbGRzU3RvcmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IF9kZWZpbmVQcm9wZXJ0eSBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknO1xuaW1wb3J0IF9leHRlbmRzIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJztcbmltcG9ydCBfY2xhc3NDYWxsQ2hlY2sgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJztcbmltcG9ydCBfY3JlYXRlQ2xhc3MgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJztcbmltcG9ydCBzZXQgZnJvbSAnbG9kYXNoL3NldCc7XG5pbXBvcnQgY3JlYXRlRm9ybUZpZWxkLCB7IGlzRm9ybUZpZWxkIH0gZnJvbSAnLi9jcmVhdGVGb3JtRmllbGQnO1xuaW1wb3J0IHsgaGFzUnVsZXMsIGZsYXR0ZW5GaWVsZHMsIGdldEVycm9yU3Rycywgc3RhcnRzV2l0aCB9IGZyb20gJy4vdXRpbHMnO1xuXG5mdW5jdGlvbiBwYXJ0T2YoYSwgYikge1xuICByZXR1cm4gYi5pbmRleE9mKGEpID09PSAwICYmIFsnLicsICdbJ10uaW5kZXhPZihiW2EubGVuZ3RoXSkgIT09IC0xO1xufVxuXG5mdW5jdGlvbiBpbnRlcm5hbEZsYXR0ZW5GaWVsZHMoZmllbGRzKSB7XG4gIHJldHVybiBmbGF0dGVuRmllbGRzKGZpZWxkcywgZnVuY3Rpb24gKF8sIG5vZGUpIHtcbiAgICByZXR1cm4gaXNGb3JtRmllbGQobm9kZSk7XG4gIH0sICdZb3UgbXVzdCB3cmFwIGZpZWxkIGRhdGEgd2l0aCBgY3JlYXRlRm9ybUZpZWxkYC4nKTtcbn1cblxudmFyIEZpZWxkc1N0b3JlID0gZnVuY3Rpb24gKCkge1xuICBmdW5jdGlvbiBGaWVsZHNTdG9yZShmaWVsZHMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgRmllbGRzU3RvcmUpO1xuXG4gICAgX2luaXRpYWxpc2VQcm9wcy5jYWxsKHRoaXMpO1xuXG4gICAgdGhpcy5maWVsZHMgPSBpbnRlcm5hbEZsYXR0ZW5GaWVsZHMoZmllbGRzKTtcbiAgICB0aGlzLmZpZWxkc01ldGEgPSB7fTtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhGaWVsZHNTdG9yZSwgW3tcbiAgICBrZXk6ICd1cGRhdGVGaWVsZHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1cGRhdGVGaWVsZHMoZmllbGRzKSB7XG4gICAgICB0aGlzLmZpZWxkcyA9IGludGVybmFsRmxhdHRlbkZpZWxkcyhmaWVsZHMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZsYXR0ZW5SZWdpc3RlcmVkRmllbGRzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZmxhdHRlblJlZ2lzdGVyZWRGaWVsZHMoZmllbGRzKSB7XG4gICAgICB2YXIgdmFsaWRGaWVsZHNOYW1lID0gdGhpcy5nZXRBbGxGaWVsZHNOYW1lKCk7XG4gICAgICByZXR1cm4gZmxhdHRlbkZpZWxkcyhmaWVsZHMsIGZ1bmN0aW9uIChwYXRoKSB7XG4gICAgICAgIHJldHVybiB2YWxpZEZpZWxkc05hbWUuaW5kZXhPZihwYXRoKSA+PSAwO1xuICAgICAgfSwgJ1lvdSBjYW5ub3Qgc2V0IGEgZm9ybSBmaWVsZCBiZWZvcmUgcmVuZGVyaW5nIGEgZmllbGQgYXNzb2NpYXRlZCB3aXRoIHRoZSB2YWx1ZS4nKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzZXRGaWVsZHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRGaWVsZHMoZmllbGRzKSB7XG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgICB2YXIgZmllbGRzTWV0YSA9IHRoaXMuZmllbGRzTWV0YTtcbiAgICAgIHZhciBub3dGaWVsZHMgPSBfZXh0ZW5kcyh7fSwgdGhpcy5maWVsZHMsIGZpZWxkcyk7XG4gICAgICB2YXIgbm93VmFsdWVzID0ge307XG4gICAgICBPYmplY3Qua2V5cyhmaWVsZHNNZXRhKS5mb3JFYWNoKGZ1bmN0aW9uIChmKSB7XG4gICAgICAgIG5vd1ZhbHVlc1tmXSA9IF90aGlzLmdldFZhbHVlRnJvbUZpZWxkcyhmLCBub3dGaWVsZHMpO1xuICAgICAgfSk7XG4gICAgICBPYmplY3Qua2V5cyhub3dWYWx1ZXMpLmZvckVhY2goZnVuY3Rpb24gKGYpIHtcbiAgICAgICAgdmFyIHZhbHVlID0gbm93VmFsdWVzW2ZdO1xuICAgICAgICB2YXIgZmllbGRNZXRhID0gX3RoaXMuZ2V0RmllbGRNZXRhKGYpO1xuICAgICAgICBpZiAoZmllbGRNZXRhICYmIGZpZWxkTWV0YS5ub3JtYWxpemUpIHtcbiAgICAgICAgICB2YXIgbm93VmFsdWUgPSBmaWVsZE1ldGEubm9ybWFsaXplKHZhbHVlLCBfdGhpcy5nZXRWYWx1ZUZyb21GaWVsZHMoZiwgX3RoaXMuZmllbGRzKSwgbm93VmFsdWVzKTtcbiAgICAgICAgICBpZiAobm93VmFsdWUgIT09IHZhbHVlKSB7XG4gICAgICAgICAgICBub3dGaWVsZHNbZl0gPSBfZXh0ZW5kcyh7fSwgbm93RmllbGRzW2ZdLCB7XG4gICAgICAgICAgICAgIHZhbHVlOiBub3dWYWx1ZVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHRoaXMuZmllbGRzID0gbm93RmllbGRzO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Jlc2V0RmllbGRzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzZXRGaWVsZHMobnMpIHtcbiAgICAgIHZhciBmaWVsZHMgPSB0aGlzLmZpZWxkcztcblxuICAgICAgdmFyIG5hbWVzID0gbnMgPyB0aGlzLmdldFZhbGlkRmllbGRzRnVsbE5hbWUobnMpIDogdGhpcy5nZXRBbGxGaWVsZHNOYW1lKCk7XG4gICAgICByZXR1cm4gbmFtZXMucmVkdWNlKGZ1bmN0aW9uIChhY2MsIG5hbWUpIHtcbiAgICAgICAgdmFyIGZpZWxkID0gZmllbGRzW25hbWVdO1xuICAgICAgICBpZiAoZmllbGQgJiYgJ3ZhbHVlJyBpbiBmaWVsZCkge1xuICAgICAgICAgIGFjY1tuYW1lXSA9IHt9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBhY2M7XG4gICAgICB9LCB7fSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2V0RmllbGRNZXRhJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0RmllbGRNZXRhKG5hbWUsIG1ldGEpIHtcbiAgICAgIHRoaXMuZmllbGRzTWV0YVtuYW1lXSA9IG1ldGE7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2V0RmllbGRzQXNEaXJ0eScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldEZpZWxkc0FzRGlydHkoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgT2JqZWN0LmtleXModGhpcy5maWVsZHMpLmZvckVhY2goZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgdmFyIGZpZWxkID0gX3RoaXMyLmZpZWxkc1tuYW1lXTtcbiAgICAgICAgdmFyIGZpZWxkTWV0YSA9IF90aGlzMi5maWVsZHNNZXRhW25hbWVdO1xuICAgICAgICBpZiAoZmllbGQgJiYgZmllbGRNZXRhICYmIGhhc1J1bGVzKGZpZWxkTWV0YS52YWxpZGF0ZSkpIHtcbiAgICAgICAgICBfdGhpczIuZmllbGRzW25hbWVdID0gX2V4dGVuZHMoe30sIGZpZWxkLCB7XG4gICAgICAgICAgICBkaXJ0eTogdHJ1ZVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGaWVsZE1ldGEnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGaWVsZE1ldGEobmFtZSkge1xuICAgICAgdGhpcy5maWVsZHNNZXRhW25hbWVdID0gdGhpcy5maWVsZHNNZXRhW25hbWVdIHx8IHt9O1xuICAgICAgcmV0dXJuIHRoaXMuZmllbGRzTWV0YVtuYW1lXTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRWYWx1ZUZyb21GaWVsZHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRWYWx1ZUZyb21GaWVsZHMobmFtZSwgZmllbGRzKSB7XG4gICAgICB2YXIgZmllbGQgPSBmaWVsZHNbbmFtZV07XG4gICAgICBpZiAoZmllbGQgJiYgJ3ZhbHVlJyBpbiBmaWVsZCkge1xuICAgICAgICByZXR1cm4gZmllbGQudmFsdWU7XG4gICAgICB9XG4gICAgICB2YXIgZmllbGRNZXRhID0gdGhpcy5nZXRGaWVsZE1ldGEobmFtZSk7XG4gICAgICByZXR1cm4gZmllbGRNZXRhICYmIGZpZWxkTWV0YS5pbml0aWFsVmFsdWU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0VmFsaWRGaWVsZHNOYW1lJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0VmFsaWRGaWVsZHNOYW1lKCkge1xuICAgICAgdmFyIF90aGlzMyA9IHRoaXM7XG5cbiAgICAgIHZhciBmaWVsZHNNZXRhID0gdGhpcy5maWVsZHNNZXRhO1xuXG4gICAgICByZXR1cm4gZmllbGRzTWV0YSA/IE9iamVjdC5rZXlzKGZpZWxkc01ldGEpLmZpbHRlcihmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICByZXR1cm4gIV90aGlzMy5nZXRGaWVsZE1ldGEobmFtZSkuaGlkZGVuO1xuICAgICAgfSkgOiBbXTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRBbGxGaWVsZHNOYW1lJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0QWxsRmllbGRzTmFtZSgpIHtcbiAgICAgIHZhciBmaWVsZHNNZXRhID0gdGhpcy5maWVsZHNNZXRhO1xuXG4gICAgICByZXR1cm4gZmllbGRzTWV0YSA/IE9iamVjdC5rZXlzKGZpZWxkc01ldGEpIDogW107XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0VmFsaWRGaWVsZHNGdWxsTmFtZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFZhbGlkRmllbGRzRnVsbE5hbWUobWF5YmVQYXJ0aWFsTmFtZSkge1xuICAgICAgdmFyIG1heWJlUGFydGlhbE5hbWVzID0gQXJyYXkuaXNBcnJheShtYXliZVBhcnRpYWxOYW1lKSA/IG1heWJlUGFydGlhbE5hbWUgOiBbbWF5YmVQYXJ0aWFsTmFtZV07XG4gICAgICByZXR1cm4gdGhpcy5nZXRWYWxpZEZpZWxkc05hbWUoKS5maWx0ZXIoZnVuY3Rpb24gKGZ1bGxOYW1lKSB7XG4gICAgICAgIHJldHVybiBtYXliZVBhcnRpYWxOYW1lcy5zb21lKGZ1bmN0aW9uIChwYXJ0aWFsTmFtZSkge1xuICAgICAgICAgIHJldHVybiBmdWxsTmFtZSA9PT0gcGFydGlhbE5hbWUgfHwgc3RhcnRzV2l0aChmdWxsTmFtZSwgcGFydGlhbE5hbWUpICYmIFsnLicsICdbJ10uaW5kZXhPZihmdWxsTmFtZVtwYXJ0aWFsTmFtZS5sZW5ndGhdKSA+PSAwO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldEZpZWxkVmFsdWVQcm9wVmFsdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGaWVsZFZhbHVlUHJvcFZhbHVlKGZpZWxkTWV0YSkge1xuICAgICAgdmFyIG5hbWUgPSBmaWVsZE1ldGEubmFtZSxcbiAgICAgICAgICBnZXRWYWx1ZVByb3BzID0gZmllbGRNZXRhLmdldFZhbHVlUHJvcHMsXG4gICAgICAgICAgdmFsdWVQcm9wTmFtZSA9IGZpZWxkTWV0YS52YWx1ZVByb3BOYW1lO1xuXG4gICAgICB2YXIgZmllbGQgPSB0aGlzLmdldEZpZWxkKG5hbWUpO1xuICAgICAgdmFyIGZpZWxkVmFsdWUgPSAndmFsdWUnIGluIGZpZWxkID8gZmllbGQudmFsdWUgOiBmaWVsZE1ldGEuaW5pdGlhbFZhbHVlO1xuICAgICAgaWYgKGdldFZhbHVlUHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIGdldFZhbHVlUHJvcHMoZmllbGRWYWx1ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX2RlZmluZVByb3BlcnR5KHt9LCB2YWx1ZVByb3BOYW1lLCBmaWVsZFZhbHVlKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGaWVsZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEZpZWxkKG5hbWUpIHtcbiAgICAgIHJldHVybiBfZXh0ZW5kcyh7fSwgdGhpcy5maWVsZHNbbmFtZV0sIHtcbiAgICAgICAgbmFtZTogbmFtZVxuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0Tm90Q29sbGVjdGVkRmllbGRzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Tm90Q29sbGVjdGVkRmllbGRzKCkge1xuICAgICAgdmFyIF90aGlzNCA9IHRoaXM7XG5cbiAgICAgIHZhciBmaWVsZHNOYW1lID0gdGhpcy5nZXRWYWxpZEZpZWxkc05hbWUoKTtcbiAgICAgIHJldHVybiBmaWVsZHNOYW1lLmZpbHRlcihmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICByZXR1cm4gIV90aGlzNC5maWVsZHNbbmFtZV07XG4gICAgICB9KS5tYXAoZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgIGRpcnR5OiBmYWxzZSxcbiAgICAgICAgICB2YWx1ZTogX3RoaXM0LmdldEZpZWxkTWV0YShuYW1lKS5pbml0aWFsVmFsdWVcbiAgICAgICAgfTtcbiAgICAgIH0pLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBmaWVsZCkge1xuICAgICAgICByZXR1cm4gc2V0KGFjYywgZmllbGQubmFtZSwgY3JlYXRlRm9ybUZpZWxkKGZpZWxkKSk7XG4gICAgICB9LCB7fSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0TmVzdGVkQWxsRmllbGRzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TmVzdGVkQWxsRmllbGRzKCkge1xuICAgICAgdmFyIF90aGlzNSA9IHRoaXM7XG5cbiAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLmZpZWxkcykucmVkdWNlKGZ1bmN0aW9uIChhY2MsIG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIHNldChhY2MsIG5hbWUsIGNyZWF0ZUZvcm1GaWVsZChfdGhpczUuZmllbGRzW25hbWVdKSk7XG4gICAgICB9LCB0aGlzLmdldE5vdENvbGxlY3RlZEZpZWxkcygpKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGaWVsZE1lbWJlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEZpZWxkTWVtYmVyKG5hbWUsIG1lbWJlcikge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0RmllbGQobmFtZSlbbWVtYmVyXTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXROZXN0ZWRGaWVsZHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXROZXN0ZWRGaWVsZHMobmFtZXMsIGdldHRlcikge1xuICAgICAgdmFyIGZpZWxkcyA9IG5hbWVzIHx8IHRoaXMuZ2V0VmFsaWRGaWVsZHNOYW1lKCk7XG4gICAgICByZXR1cm4gZmllbGRzLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBmKSB7XG4gICAgICAgIHJldHVybiBzZXQoYWNjLCBmLCBnZXR0ZXIoZikpO1xuICAgICAgfSwge30pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldE5lc3RlZEZpZWxkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TmVzdGVkRmllbGQobmFtZSwgZ2V0dGVyKSB7XG4gICAgICB2YXIgZnVsbE5hbWVzID0gdGhpcy5nZXRWYWxpZEZpZWxkc0Z1bGxOYW1lKG5hbWUpO1xuICAgICAgaWYgKGZ1bGxOYW1lcy5sZW5ndGggPT09IDAgfHwgLy8gTm90IHJlZ2lzdGVyZWRcbiAgICAgIGZ1bGxOYW1lcy5sZW5ndGggPT09IDEgJiYgZnVsbE5hbWVzWzBdID09PSBuYW1lIC8vIE5hbWUgYWxyZWFkeSBpcyBmdWxsIG5hbWUuXG4gICAgICApIHtcbiAgICAgICAgICByZXR1cm4gZ2V0dGVyKG5hbWUpO1xuICAgICAgICB9XG4gICAgICB2YXIgaXNBcnJheVZhbHVlID0gZnVsbE5hbWVzWzBdW25hbWUubGVuZ3RoXSA9PT0gJ1snO1xuICAgICAgdmFyIHN1ZmZpeE5hbWVTdGFydEluZGV4ID0gaXNBcnJheVZhbHVlID8gbmFtZS5sZW5ndGggOiBuYW1lLmxlbmd0aCArIDE7XG4gICAgICByZXR1cm4gZnVsbE5hbWVzLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBmdWxsTmFtZSkge1xuICAgICAgICByZXR1cm4gc2V0KGFjYywgZnVsbE5hbWUuc2xpY2Uoc3VmZml4TmFtZVN0YXJ0SW5kZXgpLCBnZXR0ZXIoZnVsbE5hbWUpKTtcbiAgICAgIH0sIGlzQXJyYXlWYWx1ZSA/IFtdIDoge30pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2lzVmFsaWROZXN0ZWRGaWVsZE5hbWUnLFxuXG5cbiAgICAvLyBAcHJpdmF0ZVxuICAgIC8vIEJHOiBgYWAgYW5kIGBhLmJgIGNhbm5vdCBiZSB1c2UgaW4gdGhlIHNhbWUgZm9ybVxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc1ZhbGlkTmVzdGVkRmllbGROYW1lKG5hbWUpIHtcbiAgICAgIHZhciBuYW1lcyA9IHRoaXMuZ2V0QWxsRmllbGRzTmFtZSgpO1xuICAgICAgcmV0dXJuIG5hbWVzLmV2ZXJ5KGZ1bmN0aW9uIChuKSB7XG4gICAgICAgIHJldHVybiAhcGFydE9mKG4sIG5hbWUpICYmICFwYXJ0T2YobmFtZSwgbik7XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjbGVhckZpZWxkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY2xlYXJGaWVsZChuYW1lKSB7XG4gICAgICBkZWxldGUgdGhpcy5maWVsZHNbbmFtZV07XG4gICAgICBkZWxldGUgdGhpcy5maWVsZHNNZXRhW25hbWVdO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBGaWVsZHNTdG9yZTtcbn0oKTtcblxudmFyIF9pbml0aWFsaXNlUHJvcHMgPSBmdW5jdGlvbiBfaW5pdGlhbGlzZVByb3BzKCkge1xuICB2YXIgX3RoaXM2ID0gdGhpcztcblxuICB0aGlzLnNldEZpZWxkc0luaXRpYWxWYWx1ZSA9IGZ1bmN0aW9uIChpbml0aWFsVmFsdWVzKSB7XG4gICAgdmFyIGZsYXR0ZW5lZEluaXRpYWxWYWx1ZXMgPSBfdGhpczYuZmxhdHRlblJlZ2lzdGVyZWRGaWVsZHMoaW5pdGlhbFZhbHVlcyk7XG4gICAgdmFyIGZpZWxkc01ldGEgPSBfdGhpczYuZmllbGRzTWV0YTtcbiAgICBPYmplY3Qua2V5cyhmbGF0dGVuZWRJbml0aWFsVmFsdWVzKS5mb3JFYWNoKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICBpZiAoZmllbGRzTWV0YVtuYW1lXSkge1xuICAgICAgICBfdGhpczYuc2V0RmllbGRNZXRhKG5hbWUsIF9leHRlbmRzKHt9LCBfdGhpczYuZ2V0RmllbGRNZXRhKG5hbWUpLCB7XG4gICAgICAgICAgaW5pdGlhbFZhbHVlOiBmbGF0dGVuZWRJbml0aWFsVmFsdWVzW25hbWVdXG4gICAgICAgIH0pKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICB0aGlzLmdldEFsbFZhbHVlcyA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZmllbGRzTWV0YSA9IF90aGlzNi5maWVsZHNNZXRhLFxuICAgICAgICBmaWVsZHMgPSBfdGhpczYuZmllbGRzO1xuXG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKGZpZWxkc01ldGEpLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBuYW1lKSB7XG4gICAgICByZXR1cm4gc2V0KGFjYywgbmFtZSwgX3RoaXM2LmdldFZhbHVlRnJvbUZpZWxkcyhuYW1lLCBmaWVsZHMpKTtcbiAgICB9LCB7fSk7XG4gIH07XG5cbiAgdGhpcy5nZXRGaWVsZHNWYWx1ZSA9IGZ1bmN0aW9uIChuYW1lcykge1xuICAgIHJldHVybiBfdGhpczYuZ2V0TmVzdGVkRmllbGRzKG5hbWVzLCBfdGhpczYuZ2V0RmllbGRWYWx1ZSk7XG4gIH07XG5cbiAgdGhpcy5nZXRGaWVsZFZhbHVlID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICB2YXIgZmllbGRzID0gX3RoaXM2LmZpZWxkcztcblxuICAgIHJldHVybiBfdGhpczYuZ2V0TmVzdGVkRmllbGQobmFtZSwgZnVuY3Rpb24gKGZ1bGxOYW1lKSB7XG4gICAgICByZXR1cm4gX3RoaXM2LmdldFZhbHVlRnJvbUZpZWxkcyhmdWxsTmFtZSwgZmllbGRzKTtcbiAgICB9KTtcbiAgfTtcblxuICB0aGlzLmdldEZpZWxkc0Vycm9yID0gZnVuY3Rpb24gKG5hbWVzKSB7XG4gICAgcmV0dXJuIF90aGlzNi5nZXROZXN0ZWRGaWVsZHMobmFtZXMsIF90aGlzNi5nZXRGaWVsZEVycm9yKTtcbiAgfTtcblxuICB0aGlzLmdldEZpZWxkRXJyb3IgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHJldHVybiBfdGhpczYuZ2V0TmVzdGVkRmllbGQobmFtZSwgZnVuY3Rpb24gKGZ1bGxOYW1lKSB7XG4gICAgICByZXR1cm4gZ2V0RXJyb3JTdHJzKF90aGlzNi5nZXRGaWVsZE1lbWJlcihmdWxsTmFtZSwgJ2Vycm9ycycpKTtcbiAgICB9KTtcbiAgfTtcblxuICB0aGlzLmlzRmllbGRWYWxpZGF0aW5nID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICByZXR1cm4gX3RoaXM2LmdldEZpZWxkTWVtYmVyKG5hbWUsICd2YWxpZGF0aW5nJyk7XG4gIH07XG5cbiAgdGhpcy5pc0ZpZWxkc1ZhbGlkYXRpbmcgPSBmdW5jdGlvbiAobnMpIHtcbiAgICB2YXIgbmFtZXMgPSBucyB8fCBfdGhpczYuZ2V0VmFsaWRGaWVsZHNOYW1lKCk7XG4gICAgcmV0dXJuIG5hbWVzLnNvbWUoZnVuY3Rpb24gKG4pIHtcbiAgICAgIHJldHVybiBfdGhpczYuaXNGaWVsZFZhbGlkYXRpbmcobik7XG4gICAgfSk7XG4gIH07XG5cbiAgdGhpcy5pc0ZpZWxkVG91Y2hlZCA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgcmV0dXJuIF90aGlzNi5nZXRGaWVsZE1lbWJlcihuYW1lLCAndG91Y2hlZCcpO1xuICB9O1xuXG4gIHRoaXMuaXNGaWVsZHNUb3VjaGVkID0gZnVuY3Rpb24gKG5zKSB7XG4gICAgdmFyIG5hbWVzID0gbnMgfHwgX3RoaXM2LmdldFZhbGlkRmllbGRzTmFtZSgpO1xuICAgIHJldHVybiBuYW1lcy5zb21lKGZ1bmN0aW9uIChuKSB7XG4gICAgICByZXR1cm4gX3RoaXM2LmlzRmllbGRUb3VjaGVkKG4pO1xuICAgIH0pO1xuICB9O1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gY3JlYXRlRmllbGRzU3RvcmUoZmllbGRzKSB7XG4gIHJldHVybiBuZXcgRmllbGRzU3RvcmUoZmllbGRzKTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBeEJBO0FBMEJBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFiQTtBQWVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBVEE7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQWJBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-form/es/createFieldsStore.js
