/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule ContentState
 * @format
 * 
 */


function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var BlockMapBuilder = __webpack_require__(/*! ./BlockMapBuilder */ "./node_modules/draft-js/lib/BlockMapBuilder.js");

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var DraftEntity = __webpack_require__(/*! ./DraftEntity */ "./node_modules/draft-js/lib/DraftEntity.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var SelectionState = __webpack_require__(/*! ./SelectionState */ "./node_modules/draft-js/lib/SelectionState.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var sanitizeDraftText = __webpack_require__(/*! ./sanitizeDraftText */ "./node_modules/draft-js/lib/sanitizeDraftText.js");

var List = Immutable.List,
    Record = Immutable.Record,
    Repeat = Immutable.Repeat;
var experimentalTreeDataSupport = DraftFeatureFlags.draft_tree_data_support;
var defaultRecord = {
  entityMap: null,
  blockMap: null,
  selectionBefore: null,
  selectionAfter: null
};
var ContentBlockNodeRecord = experimentalTreeDataSupport ? ContentBlockNode : ContentBlock;
var ContentStateRecord = Record(defaultRecord);

var ContentState = function (_ContentStateRecord) {
  _inherits(ContentState, _ContentStateRecord);

  function ContentState() {
    _classCallCheck(this, ContentState);

    return _possibleConstructorReturn(this, _ContentStateRecord.apply(this, arguments));
  }

  ContentState.prototype.getEntityMap = function getEntityMap() {
    // TODO: update this when we fully remove DraftEntity
    return DraftEntity;
  };

  ContentState.prototype.getBlockMap = function getBlockMap() {
    return this.get('blockMap');
  };

  ContentState.prototype.getSelectionBefore = function getSelectionBefore() {
    return this.get('selectionBefore');
  };

  ContentState.prototype.getSelectionAfter = function getSelectionAfter() {
    return this.get('selectionAfter');
  };

  ContentState.prototype.getBlockForKey = function getBlockForKey(key) {
    var block = this.getBlockMap().get(key);
    return block;
  };

  ContentState.prototype.getKeyBefore = function getKeyBefore(key) {
    return this.getBlockMap().reverse().keySeq().skipUntil(function (v) {
      return v === key;
    }).skip(1).first();
  };

  ContentState.prototype.getKeyAfter = function getKeyAfter(key) {
    return this.getBlockMap().keySeq().skipUntil(function (v) {
      return v === key;
    }).skip(1).first();
  };

  ContentState.prototype.getBlockAfter = function getBlockAfter(key) {
    return this.getBlockMap().skipUntil(function (_, k) {
      return k === key;
    }).skip(1).first();
  };

  ContentState.prototype.getBlockBefore = function getBlockBefore(key) {
    return this.getBlockMap().reverse().skipUntil(function (_, k) {
      return k === key;
    }).skip(1).first();
  };

  ContentState.prototype.getBlocksAsArray = function getBlocksAsArray() {
    return this.getBlockMap().toArray();
  };

  ContentState.prototype.getFirstBlock = function getFirstBlock() {
    return this.getBlockMap().first();
  };

  ContentState.prototype.getLastBlock = function getLastBlock() {
    return this.getBlockMap().last();
  };

  ContentState.prototype.getPlainText = function getPlainText(delimiter) {
    return this.getBlockMap().map(function (block) {
      return block ? block.getText() : '';
    }).join(delimiter || '\n');
  };

  ContentState.prototype.getLastCreatedEntityKey = function getLastCreatedEntityKey() {
    // TODO: update this when we fully remove DraftEntity
    return DraftEntity.__getLastCreatedEntityKey();
  };

  ContentState.prototype.hasText = function hasText() {
    var blockMap = this.getBlockMap();
    return blockMap.size > 1 || blockMap.first().getLength() > 0;
  };

  ContentState.prototype.createEntity = function createEntity(type, mutability, data) {
    // TODO: update this when we fully remove DraftEntity
    DraftEntity.__create(type, mutability, data);

    return this;
  };

  ContentState.prototype.mergeEntityData = function mergeEntityData(key, toMerge) {
    // TODO: update this when we fully remove DraftEntity
    DraftEntity.__mergeData(key, toMerge);

    return this;
  };

  ContentState.prototype.replaceEntityData = function replaceEntityData(key, newData) {
    // TODO: update this when we fully remove DraftEntity
    DraftEntity.__replaceData(key, newData);

    return this;
  };

  ContentState.prototype.addEntity = function addEntity(instance) {
    // TODO: update this when we fully remove DraftEntity
    DraftEntity.__add(instance);

    return this;
  };

  ContentState.prototype.getEntity = function getEntity(key) {
    // TODO: update this when we fully remove DraftEntity
    return DraftEntity.__get(key);
  };

  ContentState.createFromBlockArray = function createFromBlockArray( // TODO: update flow type when we completely deprecate the old entity API
  blocks, entityMap) {
    // TODO: remove this when we completely deprecate the old entity API
    var theBlocks = Array.isArray(blocks) ? blocks : blocks.contentBlocks;
    var blockMap = BlockMapBuilder.createFromArray(theBlocks);
    var selectionState = blockMap.isEmpty() ? new SelectionState() : SelectionState.createEmpty(blockMap.first().getKey());
    return new ContentState({
      blockMap: blockMap,
      entityMap: entityMap || DraftEntity,
      selectionBefore: selectionState,
      selectionAfter: selectionState
    });
  };

  ContentState.createFromText = function createFromText(text) {
    var delimiter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : /\r\n?|\n/g;
    var strings = text.split(delimiter);
    var blocks = strings.map(function (block) {
      block = sanitizeDraftText(block);
      return new ContentBlockNodeRecord({
        key: generateRandomKey(),
        text: block,
        type: 'unstyled',
        characterList: List(Repeat(CharacterMetadata.EMPTY, block.length))
      });
    });
    return ContentState.createFromBlockArray(blocks);
  };

  return ContentState;
}(ContentStateRecord);

module.exports = ContentState;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0NvbnRlbnRTdGF0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9Db250ZW50U3RhdGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBDb250ZW50U3RhdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgQmxvY2tNYXBCdWlsZGVyID0gcmVxdWlyZSgnLi9CbG9ja01hcEJ1aWxkZXInKTtcbnZhciBDaGFyYWN0ZXJNZXRhZGF0YSA9IHJlcXVpcmUoJy4vQ2hhcmFjdGVyTWV0YWRhdGEnKTtcbnZhciBDb250ZW50QmxvY2sgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9jaycpO1xudmFyIENvbnRlbnRCbG9ja05vZGUgPSByZXF1aXJlKCcuL0NvbnRlbnRCbG9ja05vZGUnKTtcbnZhciBEcmFmdEVudGl0eSA9IHJlcXVpcmUoJy4vRHJhZnRFbnRpdHknKTtcbnZhciBEcmFmdEZlYXR1cmVGbGFncyA9IHJlcXVpcmUoJy4vRHJhZnRGZWF0dXJlRmxhZ3MnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcbnZhciBTZWxlY3Rpb25TdGF0ZSA9IHJlcXVpcmUoJy4vU2VsZWN0aW9uU3RhdGUnKTtcblxudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xudmFyIHNhbml0aXplRHJhZnRUZXh0ID0gcmVxdWlyZSgnLi9zYW5pdGl6ZURyYWZ0VGV4dCcpO1xuXG52YXIgTGlzdCA9IEltbXV0YWJsZS5MaXN0LFxuICAgIFJlY29yZCA9IEltbXV0YWJsZS5SZWNvcmQsXG4gICAgUmVwZWF0ID0gSW1tdXRhYmxlLlJlcGVhdDtcblxuXG52YXIgZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ID0gRHJhZnRGZWF0dXJlRmxhZ3MuZHJhZnRfdHJlZV9kYXRhX3N1cHBvcnQ7XG5cbnZhciBkZWZhdWx0UmVjb3JkID0ge1xuICBlbnRpdHlNYXA6IG51bGwsXG4gIGJsb2NrTWFwOiBudWxsLFxuICBzZWxlY3Rpb25CZWZvcmU6IG51bGwsXG4gIHNlbGVjdGlvbkFmdGVyOiBudWxsXG59O1xuXG52YXIgQ29udGVudEJsb2NrTm9kZVJlY29yZCA9IGV4cGVyaW1lbnRhbFRyZWVEYXRhU3VwcG9ydCA/IENvbnRlbnRCbG9ja05vZGUgOiBDb250ZW50QmxvY2s7XG5cbnZhciBDb250ZW50U3RhdGVSZWNvcmQgPSBSZWNvcmQoZGVmYXVsdFJlY29yZCk7XG5cbnZhciBDb250ZW50U3RhdGUgPSBmdW5jdGlvbiAoX0NvbnRlbnRTdGF0ZVJlY29yZCkge1xuICBfaW5oZXJpdHMoQ29udGVudFN0YXRlLCBfQ29udGVudFN0YXRlUmVjb3JkKTtcblxuICBmdW5jdGlvbiBDb250ZW50U3RhdGUoKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIENvbnRlbnRTdGF0ZSk7XG5cbiAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbnRlbnRTdGF0ZVJlY29yZC5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0RW50aXR5TWFwID0gZnVuY3Rpb24gZ2V0RW50aXR5TWFwKCkge1xuICAgIC8vIFRPRE86IHVwZGF0ZSB0aGlzIHdoZW4gd2UgZnVsbHkgcmVtb3ZlIERyYWZ0RW50aXR5XG4gICAgcmV0dXJuIERyYWZ0RW50aXR5O1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0QmxvY2tNYXAgPSBmdW5jdGlvbiBnZXRCbG9ja01hcCgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXQoJ2Jsb2NrTWFwJyk7XG4gIH07XG5cbiAgQ29udGVudFN0YXRlLnByb3RvdHlwZS5nZXRTZWxlY3Rpb25CZWZvcmUgPSBmdW5jdGlvbiBnZXRTZWxlY3Rpb25CZWZvcmUoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdzZWxlY3Rpb25CZWZvcmUnKTtcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLmdldFNlbGVjdGlvbkFmdGVyID0gZnVuY3Rpb24gZ2V0U2VsZWN0aW9uQWZ0ZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdzZWxlY3Rpb25BZnRlcicpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0QmxvY2tGb3JLZXkgPSBmdW5jdGlvbiBnZXRCbG9ja0ZvcktleShrZXkpIHtcbiAgICB2YXIgYmxvY2sgPSB0aGlzLmdldEJsb2NrTWFwKCkuZ2V0KGtleSk7XG4gICAgcmV0dXJuIGJsb2NrO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0S2V5QmVmb3JlID0gZnVuY3Rpb24gZ2V0S2V5QmVmb3JlKGtleSkge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkucmV2ZXJzZSgpLmtleVNlcSgpLnNraXBVbnRpbChmdW5jdGlvbiAodikge1xuICAgICAgcmV0dXJuIHYgPT09IGtleTtcbiAgICB9KS5za2lwKDEpLmZpcnN0KCk7XG4gIH07XG5cbiAgQ29udGVudFN0YXRlLnByb3RvdHlwZS5nZXRLZXlBZnRlciA9IGZ1bmN0aW9uIGdldEtleUFmdGVyKGtleSkge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkua2V5U2VxKCkuc2tpcFVudGlsKGZ1bmN0aW9uICh2KSB7XG4gICAgICByZXR1cm4gdiA9PT0ga2V5O1xuICAgIH0pLnNraXAoMSkuZmlyc3QoKTtcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLmdldEJsb2NrQWZ0ZXIgPSBmdW5jdGlvbiBnZXRCbG9ja0FmdGVyKGtleSkge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkuc2tpcFVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgICByZXR1cm4gayA9PT0ga2V5O1xuICAgIH0pLnNraXAoMSkuZmlyc3QoKTtcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLmdldEJsb2NrQmVmb3JlID0gZnVuY3Rpb24gZ2V0QmxvY2tCZWZvcmUoa2V5KSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QmxvY2tNYXAoKS5yZXZlcnNlKCkuc2tpcFVudGlsKGZ1bmN0aW9uIChfLCBrKSB7XG4gICAgICByZXR1cm4gayA9PT0ga2V5O1xuICAgIH0pLnNraXAoMSkuZmlyc3QoKTtcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLmdldEJsb2Nrc0FzQXJyYXkgPSBmdW5jdGlvbiBnZXRCbG9ja3NBc0FycmF5KCkge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkudG9BcnJheSgpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0Rmlyc3RCbG9jayA9IGZ1bmN0aW9uIGdldEZpcnN0QmxvY2soKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0QmxvY2tNYXAoKS5maXJzdCgpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0TGFzdEJsb2NrID0gZnVuY3Rpb24gZ2V0TGFzdEJsb2NrKCkge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkubGFzdCgpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0UGxhaW5UZXh0ID0gZnVuY3Rpb24gZ2V0UGxhaW5UZXh0KGRlbGltaXRlcikge1xuICAgIHJldHVybiB0aGlzLmdldEJsb2NrTWFwKCkubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgcmV0dXJuIGJsb2NrID8gYmxvY2suZ2V0VGV4dCgpIDogJyc7XG4gICAgfSkuam9pbihkZWxpbWl0ZXIgfHwgJ1xcbicpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkgPSBmdW5jdGlvbiBnZXRMYXN0Q3JlYXRlZEVudGl0eUtleSgpIHtcbiAgICAvLyBUT0RPOiB1cGRhdGUgdGhpcyB3aGVuIHdlIGZ1bGx5IHJlbW92ZSBEcmFmdEVudGl0eVxuICAgIHJldHVybiBEcmFmdEVudGl0eS5fX2dldExhc3RDcmVhdGVkRW50aXR5S2V5KCk7XG4gIH07XG5cbiAgQ29udGVudFN0YXRlLnByb3RvdHlwZS5oYXNUZXh0ID0gZnVuY3Rpb24gaGFzVGV4dCgpIHtcbiAgICB2YXIgYmxvY2tNYXAgPSB0aGlzLmdldEJsb2NrTWFwKCk7XG4gICAgcmV0dXJuIGJsb2NrTWFwLnNpemUgPiAxIHx8IGJsb2NrTWFwLmZpcnN0KCkuZ2V0TGVuZ3RoKCkgPiAwO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUuY3JlYXRlRW50aXR5ID0gZnVuY3Rpb24gY3JlYXRlRW50aXR5KHR5cGUsIG11dGFiaWxpdHksIGRhdGEpIHtcbiAgICAvLyBUT0RPOiB1cGRhdGUgdGhpcyB3aGVuIHdlIGZ1bGx5IHJlbW92ZSBEcmFmdEVudGl0eVxuICAgIERyYWZ0RW50aXR5Ll9fY3JlYXRlKHR5cGUsIG11dGFiaWxpdHksIGRhdGEpO1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5wcm90b3R5cGUubWVyZ2VFbnRpdHlEYXRhID0gZnVuY3Rpb24gbWVyZ2VFbnRpdHlEYXRhKGtleSwgdG9NZXJnZSkge1xuICAgIC8vIFRPRE86IHVwZGF0ZSB0aGlzIHdoZW4gd2UgZnVsbHkgcmVtb3ZlIERyYWZ0RW50aXR5XG4gICAgRHJhZnRFbnRpdHkuX19tZXJnZURhdGEoa2V5LCB0b01lcmdlKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLnJlcGxhY2VFbnRpdHlEYXRhID0gZnVuY3Rpb24gcmVwbGFjZUVudGl0eURhdGEoa2V5LCBuZXdEYXRhKSB7XG4gICAgLy8gVE9ETzogdXBkYXRlIHRoaXMgd2hlbiB3ZSBmdWxseSByZW1vdmUgRHJhZnRFbnRpdHlcbiAgICBEcmFmdEVudGl0eS5fX3JlcGxhY2VEYXRhKGtleSwgbmV3RGF0YSk7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH07XG5cbiAgQ29udGVudFN0YXRlLnByb3RvdHlwZS5hZGRFbnRpdHkgPSBmdW5jdGlvbiBhZGRFbnRpdHkoaW5zdGFuY2UpIHtcbiAgICAvLyBUT0RPOiB1cGRhdGUgdGhpcyB3aGVuIHdlIGZ1bGx5IHJlbW92ZSBEcmFmdEVudGl0eVxuICAgIERyYWZ0RW50aXR5Ll9fYWRkKGluc3RhbmNlKTtcbiAgICByZXR1cm4gdGhpcztcbiAgfTtcblxuICBDb250ZW50U3RhdGUucHJvdG90eXBlLmdldEVudGl0eSA9IGZ1bmN0aW9uIGdldEVudGl0eShrZXkpIHtcbiAgICAvLyBUT0RPOiB1cGRhdGUgdGhpcyB3aGVuIHdlIGZ1bGx5IHJlbW92ZSBEcmFmdEVudGl0eVxuICAgIHJldHVybiBEcmFmdEVudGl0eS5fX2dldChrZXkpO1xuICB9O1xuXG4gIENvbnRlbnRTdGF0ZS5jcmVhdGVGcm9tQmxvY2tBcnJheSA9IGZ1bmN0aW9uIGNyZWF0ZUZyb21CbG9ja0FycmF5KFxuICAvLyBUT0RPOiB1cGRhdGUgZmxvdyB0eXBlIHdoZW4gd2UgY29tcGxldGVseSBkZXByZWNhdGUgdGhlIG9sZCBlbnRpdHkgQVBJXG4gIGJsb2NrcywgZW50aXR5TWFwKSB7XG4gICAgLy8gVE9ETzogcmVtb3ZlIHRoaXMgd2hlbiB3ZSBjb21wbGV0ZWx5IGRlcHJlY2F0ZSB0aGUgb2xkIGVudGl0eSBBUElcbiAgICB2YXIgdGhlQmxvY2tzID0gQXJyYXkuaXNBcnJheShibG9ja3MpID8gYmxvY2tzIDogYmxvY2tzLmNvbnRlbnRCbG9ja3M7XG4gICAgdmFyIGJsb2NrTWFwID0gQmxvY2tNYXBCdWlsZGVyLmNyZWF0ZUZyb21BcnJheSh0aGVCbG9ja3MpO1xuICAgIHZhciBzZWxlY3Rpb25TdGF0ZSA9IGJsb2NrTWFwLmlzRW1wdHkoKSA/IG5ldyBTZWxlY3Rpb25TdGF0ZSgpIDogU2VsZWN0aW9uU3RhdGUuY3JlYXRlRW1wdHkoYmxvY2tNYXAuZmlyc3QoKS5nZXRLZXkoKSk7XG4gICAgcmV0dXJuIG5ldyBDb250ZW50U3RhdGUoe1xuICAgICAgYmxvY2tNYXA6IGJsb2NrTWFwLFxuICAgICAgZW50aXR5TWFwOiBlbnRpdHlNYXAgfHwgRHJhZnRFbnRpdHksXG4gICAgICBzZWxlY3Rpb25CZWZvcmU6IHNlbGVjdGlvblN0YXRlLFxuICAgICAgc2VsZWN0aW9uQWZ0ZXI6IHNlbGVjdGlvblN0YXRlXG4gICAgfSk7XG4gIH07XG5cbiAgQ29udGVudFN0YXRlLmNyZWF0ZUZyb21UZXh0ID0gZnVuY3Rpb24gY3JlYXRlRnJvbVRleHQodGV4dCkge1xuICAgIHZhciBkZWxpbWl0ZXIgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IC9cXHJcXG4/fFxcbi9nO1xuXG4gICAgdmFyIHN0cmluZ3MgPSB0ZXh0LnNwbGl0KGRlbGltaXRlcik7XG4gICAgdmFyIGJsb2NrcyA9IHN0cmluZ3MubWFwKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgYmxvY2sgPSBzYW5pdGl6ZURyYWZ0VGV4dChibG9jayk7XG4gICAgICByZXR1cm4gbmV3IENvbnRlbnRCbG9ja05vZGVSZWNvcmQoe1xuICAgICAgICBrZXk6IGdlbmVyYXRlUmFuZG9tS2V5KCksXG4gICAgICAgIHRleHQ6IGJsb2NrLFxuICAgICAgICB0eXBlOiAndW5zdHlsZWQnLFxuICAgICAgICBjaGFyYWN0ZXJMaXN0OiBMaXN0KFJlcGVhdChDaGFyYWN0ZXJNZXRhZGF0YS5FTVBUWSwgYmxvY2subGVuZ3RoKSlcbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHJldHVybiBDb250ZW50U3RhdGUuY3JlYXRlRnJvbUJsb2NrQXJyYXkoYmxvY2tzKTtcbiAgfTtcblxuICByZXR1cm4gQ29udGVudFN0YXRlO1xufShDb250ZW50U3RhdGVSZWNvcmQpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENvbnRlbnRTdGF0ZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUtBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/ContentState.js
