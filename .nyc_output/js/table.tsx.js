__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableStyleForDefaultSize", function() { return TableStyleForDefaultSize; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");


function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nfont-size: 14px !important;\n.ant-table-content tbody tr td{\n  padding: 6px 4px !important;\n}\n\n.ant-select-selection-selected-value{\n  color: rgba(0, 0, 0, 0.65) !important;\n  font-size: 14px !important;\n}\n.ant-input-number-input {\n  color: rgba(0, 0, 0, 0.65) !important;\n  font-size: 14px !important;\n}\n\n.ant-calendar-picker-input {\n  font-size: 14px !important;\n}\n\n.ant-form-item-control .ant-input-number-input {\n    text-align: right !important;\n}\n\n.ant-select-selection__rendered {\n   height: 30px !important;\n   line-height: 30px !important;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


var TableStyleForDefaultSize = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div(_templateObject());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY2xhaW1zL2hhbmRpbmcvZGV0YWlscy9TQUlDL2NvbXBvbmVudHMvc2V0dGxlbWVudC9jb21wb25lbnRzL3N0eWxlL3RhYmxlLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NsYWltcy9oYW5kaW5nL2RldGFpbHMvU0FJQy9jb21wb25lbnRzL3NldHRsZW1lbnQvY29tcG9uZW50cy9zdHlsZS90YWJsZS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlZCBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcblxuZXhwb3J0IGNvbnN0IFRhYmxlU3R5bGVGb3JEZWZhdWx0U2l6ZSA9IHN0eWxlZC5kaXZgXG5mb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbi5hbnQtdGFibGUtY29udGVudCB0Ym9keSB0ciB0ZHtcbiAgcGFkZGluZzogNnB4IDRweCAhaW1wb3J0YW50O1xufVxuXG4uYW50LXNlbGVjdC1zZWxlY3Rpb24tc2VsZWN0ZWQtdmFsdWV7XG4gIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNjUpICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xufVxuLmFudC1pbnB1dC1udW1iZXItaW5wdXQge1xuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjY1KSAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbn1cblxuLmFudC1jYWxlbmRhci1waWNrZXItaW5wdXQge1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbn1cblxuLmFudC1mb3JtLWl0ZW0tY29udHJvbCAuYW50LWlucHV0LW51bWJlci1pbnB1dCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcbn1cblxuLmFudC1zZWxlY3Qtc2VsZWN0aW9uX19yZW5kZXJlZCB7XG4gICBoZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbiAgIGxpbmUtaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5gO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFFQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/claims/handing/details/SAIC/components/settlement/components/style/table.tsx
