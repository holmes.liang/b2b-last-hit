__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconMap", function() { return IconMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExceptionMap", function() { return ExceptionMap; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _noFound__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./noFound */ "./node_modules/antd/es/result/noFound.js");
/* harmony import */ var _serverError__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./serverError */ "./node_modules/antd/es/result/serverError.js");
/* harmony import */ var _unauthorized__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./unauthorized */ "./node_modules/antd/es/result/unauthorized.js");







var IconMap = {
  success: 'check-circle',
  error: 'close-circle',
  info: 'exclamation-circle',
  warning: 'warning'
};
var ExceptionMap = {
  '404': _noFound__WEBPACK_IMPORTED_MODULE_4__["default"],
  '500': _serverError__WEBPACK_IMPORTED_MODULE_5__["default"],
  '403': _unauthorized__WEBPACK_IMPORTED_MODULE_6__["default"]
}; // ExceptionImageMap keys

var ExceptionStatus = Object.keys(ExceptionMap);
/**
 * render icon
 * if ExceptionStatus includes ,render svg image
 * else render iconNode
 * @param prefixCls
 * @param {status, icon}
 */

var renderIcon = function renderIcon(prefixCls, _ref) {
  var status = _ref.status,
      icon = _ref.icon;
  var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-icon"));

  if (ExceptionStatus.includes(status)) {
    var SVGComponent = ExceptionMap[status];
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(className, " ").concat(prefixCls, "-image")
    }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](SVGComponent, null));
  }

  var iconString = IconMap[status];
  var iconNode = icon || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
    type: iconString,
    theme: "filled"
  });
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: className
  }, iconNode);
};

var renderExtra = function renderExtra(prefixCls, _ref2) {
  var extra = _ref2.extra;
  return extra && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
    className: "".concat(prefixCls, "-extra")
  }, extra);
};

var Result = function Result(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref3) {
    var getPrefixCls = _ref3.getPrefixCls;
    var customizePrefixCls = props.prefixCls,
        customizeClassName = props.className,
        subTitle = props.subTitle,
        title = props.title,
        style = props.style,
        children = props.children,
        status = props.status;
    var prefixCls = getPrefixCls('result', customizePrefixCls);
    var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, "".concat(prefixCls, "-").concat(status), customizeClassName);
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: className,
      style: style
    }, renderIcon(prefixCls, props), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-title")
    }, title), subTitle && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-subtitle")
    }, subTitle), children && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-content")
    }, children), renderExtra(prefixCls, props));
  });
};

Result.defaultProps = {
  status: 'info'
};
Result.PRESENTED_IMAGE_403 = ExceptionMap[403];
Result.PRESENTED_IMAGE_404 = ExceptionMap[404];
Result.PRESENTED_IMAGE_500 = ExceptionMap[500];
/* harmony default export */ __webpack_exports__["default"] = (Result);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9yZXN1bHQvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3Jlc3VsdC9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgeyBDb25maWdDb25zdW1lciB9IGZyb20gJy4uL2NvbmZpZy1wcm92aWRlcic7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCBub0ZvdW5kIGZyb20gJy4vbm9Gb3VuZCc7XG5pbXBvcnQgc2VydmVyRXJyb3IgZnJvbSAnLi9zZXJ2ZXJFcnJvcic7XG5pbXBvcnQgdW5hdXRob3JpemVkIGZyb20gJy4vdW5hdXRob3JpemVkJztcbmV4cG9ydCBjb25zdCBJY29uTWFwID0ge1xuICAgIHN1Y2Nlc3M6ICdjaGVjay1jaXJjbGUnLFxuICAgIGVycm9yOiAnY2xvc2UtY2lyY2xlJyxcbiAgICBpbmZvOiAnZXhjbGFtYXRpb24tY2lyY2xlJyxcbiAgICB3YXJuaW5nOiAnd2FybmluZycsXG59O1xuZXhwb3J0IGNvbnN0IEV4Y2VwdGlvbk1hcCA9IHtcbiAgICAnNDA0Jzogbm9Gb3VuZCxcbiAgICAnNTAwJzogc2VydmVyRXJyb3IsXG4gICAgJzQwMyc6IHVuYXV0aG9yaXplZCxcbn07XG4vLyBFeGNlcHRpb25JbWFnZU1hcCBrZXlzXG5jb25zdCBFeGNlcHRpb25TdGF0dXMgPSBPYmplY3Qua2V5cyhFeGNlcHRpb25NYXApO1xuLyoqXG4gKiByZW5kZXIgaWNvblxuICogaWYgRXhjZXB0aW9uU3RhdHVzIGluY2x1ZGVzICxyZW5kZXIgc3ZnIGltYWdlXG4gKiBlbHNlIHJlbmRlciBpY29uTm9kZVxuICogQHBhcmFtIHByZWZpeENsc1xuICogQHBhcmFtIHtzdGF0dXMsIGljb259XG4gKi9cbmNvbnN0IHJlbmRlckljb24gPSAocHJlZml4Q2xzLCB7IHN0YXR1cywgaWNvbiB9KSA9PiB7XG4gICAgY29uc3QgY2xhc3NOYW1lID0gY2xhc3NuYW1lcyhgJHtwcmVmaXhDbHN9LWljb25gKTtcbiAgICBpZiAoRXhjZXB0aW9uU3RhdHVzLmluY2x1ZGVzKHN0YXR1cykpIHtcbiAgICAgICAgY29uc3QgU1ZHQ29tcG9uZW50ID0gRXhjZXB0aW9uTWFwW3N0YXR1c107XG4gICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2Ake2NsYXNzTmFtZX0gJHtwcmVmaXhDbHN9LWltYWdlYH0+XG4gICAgICAgIDxTVkdDb21wb25lbnQgLz5cbiAgICAgIDwvZGl2Pik7XG4gICAgfVxuICAgIGNvbnN0IGljb25TdHJpbmcgPSBJY29uTWFwW3N0YXR1c107XG4gICAgY29uc3QgaWNvbk5vZGUgPSBpY29uIHx8IDxJY29uIHR5cGU9e2ljb25TdHJpbmd9IHRoZW1lPVwiZmlsbGVkXCIvPjtcbiAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZX0+e2ljb25Ob2RlfTwvZGl2Pjtcbn07XG5jb25zdCByZW5kZXJFeHRyYSA9IChwcmVmaXhDbHMsIHsgZXh0cmEgfSkgPT4gZXh0cmEgJiYgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tZXh0cmFgfT57ZXh0cmF9PC9kaXY+O1xuY29uc3QgUmVzdWx0ID0gcHJvcHMgPT4gKDxDb25maWdDb25zdW1lcj5cbiAgICB7KHsgZ2V0UHJlZml4Q2xzIH0pID0+IHtcbiAgICBjb25zdCB7IHByZWZpeENsczogY3VzdG9taXplUHJlZml4Q2xzLCBjbGFzc05hbWU6IGN1c3RvbWl6ZUNsYXNzTmFtZSwgc3ViVGl0bGUsIHRpdGxlLCBzdHlsZSwgY2hpbGRyZW4sIHN0YXR1cywgfSA9IHByb3BzO1xuICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygncmVzdWx0JywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICBjb25zdCBjbGFzc05hbWUgPSBjbGFzc25hbWVzKHByZWZpeENscywgYCR7cHJlZml4Q2xzfS0ke3N0YXR1c31gLCBjdXN0b21pemVDbGFzc05hbWUpO1xuICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZX0gc3R5bGU9e3N0eWxlfT5cbiAgICAgICAgICB7cmVuZGVySWNvbihwcmVmaXhDbHMsIHByb3BzKX1cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS10aXRsZWB9Pnt0aXRsZX08L2Rpdj5cbiAgICAgICAgICB7c3ViVGl0bGUgJiYgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tc3VidGl0bGVgfT57c3ViVGl0bGV9PC9kaXY+fVxuICAgICAgICAgIHtjaGlsZHJlbiAmJiA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50YH0+e2NoaWxkcmVufTwvZGl2Pn1cbiAgICAgICAgICB7cmVuZGVyRXh0cmEocHJlZml4Q2xzLCBwcm9wcyl9XG4gICAgICAgIDwvZGl2Pik7XG59fVxuICA8L0NvbmZpZ0NvbnN1bWVyPik7XG5SZXN1bHQuZGVmYXVsdFByb3BzID0ge1xuICAgIHN0YXR1czogJ2luZm8nLFxufTtcblJlc3VsdC5QUkVTRU5URURfSU1BR0VfNDAzID0gRXhjZXB0aW9uTWFwWzQwM107XG5SZXN1bHQuUFJFU0VOVEVEX0lNQUdFXzQwNCA9IEV4Y2VwdGlvbk1hcFs0MDRdO1xuUmVzdWx0LlBSRVNFTlRFRF9JTUFHRV81MDAgPSBFeGNlcHRpb25NYXBbNTAwXTtcbmV4cG9ydCBkZWZhdWx0IFJlc3VsdDtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBOzs7Ozs7OztBQU9BO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFWQTtBQUNBO0FBV0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQVRBO0FBQUE7QUFDQTtBQWFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/result/index.js
