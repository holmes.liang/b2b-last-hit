__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "omit", function() { return omit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBeforeSelectionText", function() { return getBeforeSelectionText; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLastMeasureIndex", function() { return getLastMeasureIndex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "replaceWithMeasure", function() { return replaceWithMeasure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setInputSelection", function() { return setInputSelection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateSearch", function() { return validateSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "filterOption", function() { return filterOption; });
function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var omit = function omit(obj) {
  var clone = _objectSpread({}, obj);

  for (var _len = arguments.length, keys = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    keys[_key - 1] = arguments[_key];
  }

  keys.forEach(function (key) {
    delete clone[key];
  });
  return clone;
};
/**
 * Cut input selection into 2 part and return text before selection start
 */

function getBeforeSelectionText(input) {
  var selectionStart = input.selectionStart;
  return input.value.slice(0, selectionStart);
}
/**
 * Find the last match prefix index
 */

function getLastMeasureIndex(text) {
  var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var prefixList = Array.isArray(prefix) ? prefix : [prefix];
  return prefixList.reduce(function (lastMatch, prefixStr) {
    var lastIndex = text.lastIndexOf(prefixStr);

    if (lastIndex > lastMatch.location) {
      return {
        location: lastIndex,
        prefix: prefixStr
      };
    }

    return lastMatch;
  }, {
    location: -1,
    prefix: ''
  });
}

function lower(char) {
  return (char || '').toLowerCase();
}

function reduceText(text, targetText, split) {
  var firstChar = text[0];

  if (!firstChar || firstChar === split) {
    return text;
  } // Reuse rest text as it can


  var restText = text;
  var targetTextLen = targetText.length;

  for (var i = 0; i < targetTextLen; i += 1) {
    if (lower(restText[i]) !== lower(targetText[i])) {
      restText = restText.slice(i);
      break;
    } else if (i === targetTextLen - 1) {
      restText = restText.slice(targetTextLen);
    }
  }

  return restText;
}
/**
 * Paint targetText into current text:
 *  text: little@litest
 *  targetText: light
 *  => little @light test
 */


function replaceWithMeasure(text, measureConfig) {
  var measureLocation = measureConfig.measureLocation,
      prefix = measureConfig.prefix,
      targetText = measureConfig.targetText,
      selectionStart = measureConfig.selectionStart,
      split = measureConfig.split; // Before text will append one space if have other text

  var beforeMeasureText = text.slice(0, measureLocation);

  if (beforeMeasureText[beforeMeasureText.length - split.length] === split) {
    beforeMeasureText = beforeMeasureText.slice(0, beforeMeasureText.length - split.length);
  }

  if (beforeMeasureText) {
    beforeMeasureText = "".concat(beforeMeasureText).concat(split);
  } // Cut duplicate string with current targetText


  var restText = reduceText(text.slice(selectionStart), targetText.slice(selectionStart - measureLocation - prefix.length), split);

  if (restText.slice(0, split.length) === split) {
    restText = restText.slice(split.length);
  }

  var connectedStartText = "".concat(beforeMeasureText).concat(prefix).concat(targetText).concat(split);
  return {
    text: "".concat(connectedStartText).concat(restText),
    selectionLocation: connectedStartText.length
  };
}
function setInputSelection(input, location) {
  input.setSelectionRange(location, location);
  /**
   * Reset caret into view.
   * Since this function always called by user control, it's safe to focus element.
   */

  input.blur();
  input.focus();
}
function validateSearch(text, props) {
  var split = props.split;
  return !split || text.indexOf(split) === -1;
}
function filterOption(input, _ref) {
  var _ref$value = _ref.value,
      value = _ref$value === void 0 ? '' : _ref$value;
  var lowerCase = input.toLowerCase();
  return value.toLowerCase().indexOf(lowerCase) !== -1;
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudGlvbnMvZXMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLW1lbnRpb25zL2VzL3V0aWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKHNvdXJjZSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKHNvdXJjZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZXhwb3J0IHZhciBvbWl0ID0gZnVuY3Rpb24gb21pdChvYmopIHtcbiAgdmFyIGNsb25lID0gX29iamVjdFNwcmVhZCh7fSwgb2JqKTtcblxuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwga2V5cyA9IG5ldyBBcnJheShfbGVuID4gMSA/IF9sZW4gLSAxIDogMCksIF9rZXkgPSAxOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAga2V5c1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gIH1cblxuICBrZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIGRlbGV0ZSBjbG9uZVtrZXldO1xuICB9KTtcbiAgcmV0dXJuIGNsb25lO1xufTtcbi8qKlxuICogQ3V0IGlucHV0IHNlbGVjdGlvbiBpbnRvIDIgcGFydCBhbmQgcmV0dXJuIHRleHQgYmVmb3JlIHNlbGVjdGlvbiBzdGFydFxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRCZWZvcmVTZWxlY3Rpb25UZXh0KGlucHV0KSB7XG4gIHZhciBzZWxlY3Rpb25TdGFydCA9IGlucHV0LnNlbGVjdGlvblN0YXJ0O1xuICByZXR1cm4gaW5wdXQudmFsdWUuc2xpY2UoMCwgc2VsZWN0aW9uU3RhcnQpO1xufVxuLyoqXG4gKiBGaW5kIHRoZSBsYXN0IG1hdGNoIHByZWZpeCBpbmRleFxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRMYXN0TWVhc3VyZUluZGV4KHRleHQpIHtcbiAgdmFyIHByZWZpeCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogJyc7XG4gIHZhciBwcmVmaXhMaXN0ID0gQXJyYXkuaXNBcnJheShwcmVmaXgpID8gcHJlZml4IDogW3ByZWZpeF07XG4gIHJldHVybiBwcmVmaXhMaXN0LnJlZHVjZShmdW5jdGlvbiAobGFzdE1hdGNoLCBwcmVmaXhTdHIpIHtcbiAgICB2YXIgbGFzdEluZGV4ID0gdGV4dC5sYXN0SW5kZXhPZihwcmVmaXhTdHIpO1xuXG4gICAgaWYgKGxhc3RJbmRleCA+IGxhc3RNYXRjaC5sb2NhdGlvbikge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbG9jYXRpb246IGxhc3RJbmRleCxcbiAgICAgICAgcHJlZml4OiBwcmVmaXhTdHJcbiAgICAgIH07XG4gICAgfVxuXG4gICAgcmV0dXJuIGxhc3RNYXRjaDtcbiAgfSwge1xuICAgIGxvY2F0aW9uOiAtMSxcbiAgICBwcmVmaXg6ICcnXG4gIH0pO1xufVxuXG5mdW5jdGlvbiBsb3dlcihjaGFyKSB7XG4gIHJldHVybiAoY2hhciB8fCAnJykudG9Mb3dlckNhc2UoKTtcbn1cblxuZnVuY3Rpb24gcmVkdWNlVGV4dCh0ZXh0LCB0YXJnZXRUZXh0LCBzcGxpdCkge1xuICB2YXIgZmlyc3RDaGFyID0gdGV4dFswXTtcblxuICBpZiAoIWZpcnN0Q2hhciB8fCBmaXJzdENoYXIgPT09IHNwbGl0KSB7XG4gICAgcmV0dXJuIHRleHQ7XG4gIH0gLy8gUmV1c2UgcmVzdCB0ZXh0IGFzIGl0IGNhblxuXG5cbiAgdmFyIHJlc3RUZXh0ID0gdGV4dDtcbiAgdmFyIHRhcmdldFRleHRMZW4gPSB0YXJnZXRUZXh0Lmxlbmd0aDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRhcmdldFRleHRMZW47IGkgKz0gMSkge1xuICAgIGlmIChsb3dlcihyZXN0VGV4dFtpXSkgIT09IGxvd2VyKHRhcmdldFRleHRbaV0pKSB7XG4gICAgICByZXN0VGV4dCA9IHJlc3RUZXh0LnNsaWNlKGkpO1xuICAgICAgYnJlYWs7XG4gICAgfSBlbHNlIGlmIChpID09PSB0YXJnZXRUZXh0TGVuIC0gMSkge1xuICAgICAgcmVzdFRleHQgPSByZXN0VGV4dC5zbGljZSh0YXJnZXRUZXh0TGVuKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmVzdFRleHQ7XG59XG4vKipcbiAqIFBhaW50IHRhcmdldFRleHQgaW50byBjdXJyZW50IHRleHQ6XG4gKiAgdGV4dDogbGl0dGxlQGxpdGVzdFxuICogIHRhcmdldFRleHQ6IGxpZ2h0XG4gKiAgPT4gbGl0dGxlIEBsaWdodCB0ZXN0XG4gKi9cblxuXG5leHBvcnQgZnVuY3Rpb24gcmVwbGFjZVdpdGhNZWFzdXJlKHRleHQsIG1lYXN1cmVDb25maWcpIHtcbiAgdmFyIG1lYXN1cmVMb2NhdGlvbiA9IG1lYXN1cmVDb25maWcubWVhc3VyZUxvY2F0aW9uLFxuICAgICAgcHJlZml4ID0gbWVhc3VyZUNvbmZpZy5wcmVmaXgsXG4gICAgICB0YXJnZXRUZXh0ID0gbWVhc3VyZUNvbmZpZy50YXJnZXRUZXh0LFxuICAgICAgc2VsZWN0aW9uU3RhcnQgPSBtZWFzdXJlQ29uZmlnLnNlbGVjdGlvblN0YXJ0LFxuICAgICAgc3BsaXQgPSBtZWFzdXJlQ29uZmlnLnNwbGl0OyAvLyBCZWZvcmUgdGV4dCB3aWxsIGFwcGVuZCBvbmUgc3BhY2UgaWYgaGF2ZSBvdGhlciB0ZXh0XG5cbiAgdmFyIGJlZm9yZU1lYXN1cmVUZXh0ID0gdGV4dC5zbGljZSgwLCBtZWFzdXJlTG9jYXRpb24pO1xuXG4gIGlmIChiZWZvcmVNZWFzdXJlVGV4dFtiZWZvcmVNZWFzdXJlVGV4dC5sZW5ndGggLSBzcGxpdC5sZW5ndGhdID09PSBzcGxpdCkge1xuICAgIGJlZm9yZU1lYXN1cmVUZXh0ID0gYmVmb3JlTWVhc3VyZVRleHQuc2xpY2UoMCwgYmVmb3JlTWVhc3VyZVRleHQubGVuZ3RoIC0gc3BsaXQubGVuZ3RoKTtcbiAgfVxuXG4gIGlmIChiZWZvcmVNZWFzdXJlVGV4dCkge1xuICAgIGJlZm9yZU1lYXN1cmVUZXh0ID0gXCJcIi5jb25jYXQoYmVmb3JlTWVhc3VyZVRleHQpLmNvbmNhdChzcGxpdCk7XG4gIH0gLy8gQ3V0IGR1cGxpY2F0ZSBzdHJpbmcgd2l0aCBjdXJyZW50IHRhcmdldFRleHRcblxuXG4gIHZhciByZXN0VGV4dCA9IHJlZHVjZVRleHQodGV4dC5zbGljZShzZWxlY3Rpb25TdGFydCksIHRhcmdldFRleHQuc2xpY2Uoc2VsZWN0aW9uU3RhcnQgLSBtZWFzdXJlTG9jYXRpb24gLSBwcmVmaXgubGVuZ3RoKSwgc3BsaXQpO1xuXG4gIGlmIChyZXN0VGV4dC5zbGljZSgwLCBzcGxpdC5sZW5ndGgpID09PSBzcGxpdCkge1xuICAgIHJlc3RUZXh0ID0gcmVzdFRleHQuc2xpY2Uoc3BsaXQubGVuZ3RoKTtcbiAgfVxuXG4gIHZhciBjb25uZWN0ZWRTdGFydFRleHQgPSBcIlwiLmNvbmNhdChiZWZvcmVNZWFzdXJlVGV4dCkuY29uY2F0KHByZWZpeCkuY29uY2F0KHRhcmdldFRleHQpLmNvbmNhdChzcGxpdCk7XG4gIHJldHVybiB7XG4gICAgdGV4dDogXCJcIi5jb25jYXQoY29ubmVjdGVkU3RhcnRUZXh0KS5jb25jYXQocmVzdFRleHQpLFxuICAgIHNlbGVjdGlvbkxvY2F0aW9uOiBjb25uZWN0ZWRTdGFydFRleHQubGVuZ3RoXG4gIH07XG59XG5leHBvcnQgZnVuY3Rpb24gc2V0SW5wdXRTZWxlY3Rpb24oaW5wdXQsIGxvY2F0aW9uKSB7XG4gIGlucHV0LnNldFNlbGVjdGlvblJhbmdlKGxvY2F0aW9uLCBsb2NhdGlvbik7XG4gIC8qKlxuICAgKiBSZXNldCBjYXJldCBpbnRvIHZpZXcuXG4gICAqIFNpbmNlIHRoaXMgZnVuY3Rpb24gYWx3YXlzIGNhbGxlZCBieSB1c2VyIGNvbnRyb2wsIGl0J3Mgc2FmZSB0byBmb2N1cyBlbGVtZW50LlxuICAgKi9cblxuICBpbnB1dC5ibHVyKCk7XG4gIGlucHV0LmZvY3VzKCk7XG59XG5leHBvcnQgZnVuY3Rpb24gdmFsaWRhdGVTZWFyY2godGV4dCwgcHJvcHMpIHtcbiAgdmFyIHNwbGl0ID0gcHJvcHMuc3BsaXQ7XG4gIHJldHVybiAhc3BsaXQgfHwgdGV4dC5pbmRleE9mKHNwbGl0KSA9PT0gLTE7XG59XG5leHBvcnQgZnVuY3Rpb24gZmlsdGVyT3B0aW9uKGlucHV0LCBfcmVmKSB7XG4gIHZhciBfcmVmJHZhbHVlID0gX3JlZi52YWx1ZSxcbiAgICAgIHZhbHVlID0gX3JlZiR2YWx1ZSA9PT0gdm9pZCAwID8gJycgOiBfcmVmJHZhbHVlO1xuICB2YXIgbG93ZXJDYXNlID0gaW5wdXQudG9Mb3dlckNhc2UoKTtcbiAgcmV0dXJuIHZhbHVlLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihsb3dlckNhc2UpICE9PSAtMTtcbn0iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7O0FBUUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/rc-mentions/es/util.js
