/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule keyCommandPlainDelete
 * @format
 * 
 */


var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var UnicodeUtils = __webpack_require__(/*! fbjs/lib/UnicodeUtils */ "./node_modules/fbjs/lib/UnicodeUtils.js");

var moveSelectionForward = __webpack_require__(/*! ./moveSelectionForward */ "./node_modules/draft-js/lib/moveSelectionForward.js");

var removeTextWithStrategy = __webpack_require__(/*! ./removeTextWithStrategy */ "./node_modules/draft-js/lib/removeTextWithStrategy.js");
/**
 * Remove the selected range. If the cursor is collapsed, remove the following
 * character. This operation is Unicode-aware, so removing a single character
 * will remove a surrogate pair properly as well.
 */


function keyCommandPlainDelete(editorState) {
  var afterRemoval = removeTextWithStrategy(editorState, function (strategyState) {
    var selection = strategyState.getSelection();
    var content = strategyState.getCurrentContent();
    var key = selection.getAnchorKey();
    var offset = selection.getAnchorOffset();
    var charAhead = content.getBlockForKey(key).getText()[offset];
    return moveSelectionForward(strategyState, charAhead ? UnicodeUtils.getUTF16Length(charAhead, 0) : 1);
  }, 'forward');

  if (afterRemoval === editorState.getCurrentContent()) {
    return editorState;
  }

  var selection = editorState.getSelection();
  return EditorState.push(editorState, afterRemoval.set('selectionBefore', selection), selection.isCollapsed() ? 'delete-character' : 'remove-range');
}

module.exports = keyCommandPlainDelete;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2tleUNvbW1hbmRQbGFpbkRlbGV0ZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2RyYWZ0LWpzL2xpYi9rZXlDb21tYW5kUGxhaW5EZWxldGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTMtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgQlNELXN0eWxlIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuIEFuIGFkZGl0aW9uYWwgZ3JhbnRcbiAqIG9mIHBhdGVudCByaWdodHMgY2FuIGJlIGZvdW5kIGluIHRoZSBQQVRFTlRTIGZpbGUgaW4gdGhlIHNhbWUgZGlyZWN0b3J5LlxuICpcbiAqIEBwcm92aWRlc01vZHVsZSBrZXlDb21tYW5kUGxhaW5EZWxldGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEVkaXRvclN0YXRlID0gcmVxdWlyZSgnLi9FZGl0b3JTdGF0ZScpO1xudmFyIFVuaWNvZGVVdGlscyA9IHJlcXVpcmUoJ2ZianMvbGliL1VuaWNvZGVVdGlscycpO1xuXG52YXIgbW92ZVNlbGVjdGlvbkZvcndhcmQgPSByZXF1aXJlKCcuL21vdmVTZWxlY3Rpb25Gb3J3YXJkJyk7XG52YXIgcmVtb3ZlVGV4dFdpdGhTdHJhdGVneSA9IHJlcXVpcmUoJy4vcmVtb3ZlVGV4dFdpdGhTdHJhdGVneScpO1xuXG4vKipcbiAqIFJlbW92ZSB0aGUgc2VsZWN0ZWQgcmFuZ2UuIElmIHRoZSBjdXJzb3IgaXMgY29sbGFwc2VkLCByZW1vdmUgdGhlIGZvbGxvd2luZ1xuICogY2hhcmFjdGVyLiBUaGlzIG9wZXJhdGlvbiBpcyBVbmljb2RlLWF3YXJlLCBzbyByZW1vdmluZyBhIHNpbmdsZSBjaGFyYWN0ZXJcbiAqIHdpbGwgcmVtb3ZlIGEgc3Vycm9nYXRlIHBhaXIgcHJvcGVybHkgYXMgd2VsbC5cbiAqL1xuZnVuY3Rpb24ga2V5Q29tbWFuZFBsYWluRGVsZXRlKGVkaXRvclN0YXRlKSB7XG4gIHZhciBhZnRlclJlbW92YWwgPSByZW1vdmVUZXh0V2l0aFN0cmF0ZWd5KGVkaXRvclN0YXRlLCBmdW5jdGlvbiAoc3RyYXRlZ3lTdGF0ZSkge1xuICAgIHZhciBzZWxlY3Rpb24gPSBzdHJhdGVneVN0YXRlLmdldFNlbGVjdGlvbigpO1xuICAgIHZhciBjb250ZW50ID0gc3RyYXRlZ3lTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgIHZhciBrZXkgPSBzZWxlY3Rpb24uZ2V0QW5jaG9yS2V5KCk7XG4gICAgdmFyIG9mZnNldCA9IHNlbGVjdGlvbi5nZXRBbmNob3JPZmZzZXQoKTtcbiAgICB2YXIgY2hhckFoZWFkID0gY29udGVudC5nZXRCbG9ja0ZvcktleShrZXkpLmdldFRleHQoKVtvZmZzZXRdO1xuICAgIHJldHVybiBtb3ZlU2VsZWN0aW9uRm9yd2FyZChzdHJhdGVneVN0YXRlLCBjaGFyQWhlYWQgPyBVbmljb2RlVXRpbHMuZ2V0VVRGMTZMZW5ndGgoY2hhckFoZWFkLCAwKSA6IDEpO1xuICB9LCAnZm9yd2FyZCcpO1xuXG4gIGlmIChhZnRlclJlbW92YWwgPT09IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCkpIHtcbiAgICByZXR1cm4gZWRpdG9yU3RhdGU7XG4gIH1cblxuICB2YXIgc2VsZWN0aW9uID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIGFmdGVyUmVtb3ZhbC5zZXQoJ3NlbGVjdGlvbkJlZm9yZScsIHNlbGVjdGlvbiksIHNlbGVjdGlvbi5pc0NvbGxhcHNlZCgpID8gJ2RlbGV0ZS1jaGFyYWN0ZXInIDogJ3JlbW92ZS1yYW5nZScpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGtleUNvbW1hbmRQbGFpbkRlbGV0ZTsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/keyCommandPlainDelete.js
