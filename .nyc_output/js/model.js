/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var each = zrUtil.each;
var isObject = zrUtil.isObject;
var isArray = zrUtil.isArray;
/**
 * Make the name displayable. But we should
 * make sure it is not duplicated with user
 * specified name, so use '\0';
 */

var DUMMY_COMPONENT_NAME_PREFIX = 'series\0';
/**
 * If value is not array, then translate it to array.
 * @param  {*} value
 * @return {Array} [value] or value
 */

function normalizeToArray(value) {
  return value instanceof Array ? value : value == null ? [] : [value];
}
/**
 * Sync default option between normal and emphasis like `position` and `show`
 * In case some one will write code like
 *     label: {
 *          show: false,
 *          position: 'outside',
 *          fontSize: 18
 *     },
 *     emphasis: {
 *          label: { show: true }
 *     }
 * @param {Object} opt
 * @param {string} key
 * @param {Array.<string>} subOpts
 */


function defaultEmphasis(opt, key, subOpts) {
  // Caution: performance sensitive.
  if (opt) {
    opt[key] = opt[key] || {};
    opt.emphasis = opt.emphasis || {};
    opt.emphasis[key] = opt.emphasis[key] || {}; // Default emphasis option from normal

    for (var i = 0, len = subOpts.length; i < len; i++) {
      var subOptName = subOpts[i];

      if (!opt.emphasis[key].hasOwnProperty(subOptName) && opt[key].hasOwnProperty(subOptName)) {
        opt.emphasis[key][subOptName] = opt[key][subOptName];
      }
    }
  }
}

var TEXT_STYLE_OPTIONS = ['fontStyle', 'fontWeight', 'fontSize', 'fontFamily', 'rich', 'tag', 'color', 'textBorderColor', 'textBorderWidth', 'width', 'height', 'lineHeight', 'align', 'verticalAlign', 'baseline', 'shadowColor', 'shadowBlur', 'shadowOffsetX', 'shadowOffsetY', 'textShadowColor', 'textShadowBlur', 'textShadowOffsetX', 'textShadowOffsetY', 'backgroundColor', 'borderColor', 'borderWidth', 'borderRadius', 'padding']; // modelUtil.LABEL_OPTIONS = modelUtil.TEXT_STYLE_OPTIONS.concat([
//     'position', 'offset', 'rotate', 'origin', 'show', 'distance', 'formatter',
//     'fontStyle', 'fontWeight', 'fontSize', 'fontFamily',
//     // FIXME: deprecated, check and remove it.
//     'textStyle'
// ]);

/**
 * The method do not ensure performance.
 * data could be [12, 2323, {value: 223}, [1221, 23], {value: [2, 23]}]
 * This helper method retieves value from data.
 * @param {string|number|Date|Array|Object} dataItem
 * @return {number|string|Date|Array.<number|string|Date>}
 */

function getDataItemValue(dataItem) {
  return isObject(dataItem) && !isArray(dataItem) && !(dataItem instanceof Date) ? dataItem.value : dataItem;
}
/**
 * data could be [12, 2323, {value: 223}, [1221, 23], {value: [2, 23]}]
 * This helper method determine if dataItem has extra option besides value
 * @param {string|number|Date|Array|Object} dataItem
 */


function isDataItemOption(dataItem) {
  return isObject(dataItem) && !(dataItem instanceof Array); // // markLine data can be array
  // && !(dataItem[0] && isObject(dataItem[0]) && !(dataItem[0] instanceof Array));
}
/**
 * Mapping to exists for merge.
 *
 * @public
 * @param {Array.<Object>|Array.<module:echarts/model/Component>} exists
 * @param {Object|Array.<Object>} newCptOptions
 * @return {Array.<Object>} Result, like [{exist: ..., option: ...}, {}],
 *                          index of which is the same as exists.
 */


function mappingToExists(exists, newCptOptions) {
  // Mapping by the order by original option (but not order of
  // new option) in merge mode. Because we should ensure
  // some specified index (like xAxisIndex) is consistent with
  // original option, which is easy to understand, espatially in
  // media query. And in most case, merge option is used to
  // update partial option but not be expected to change order.
  newCptOptions = (newCptOptions || []).slice();
  var result = zrUtil.map(exists || [], function (obj, index) {
    return {
      exist: obj
    };
  }); // Mapping by id or name if specified.

  each(newCptOptions, function (cptOption, index) {
    if (!isObject(cptOption)) {
      return;
    } // id has highest priority.


    for (var i = 0; i < result.length; i++) {
      if (!result[i].option // Consider name: two map to one.
      && cptOption.id != null && result[i].exist.id === cptOption.id + '') {
        result[i].option = cptOption;
        newCptOptions[index] = null;
        return;
      }
    }

    for (var i = 0; i < result.length; i++) {
      var exist = result[i].exist;

      if (!result[i].option // Consider name: two map to one.
      // Can not match when both ids exist but different.
      && (exist.id == null || cptOption.id == null) && cptOption.name != null && !isIdInner(cptOption) && !isIdInner(exist) && exist.name === cptOption.name + '') {
        result[i].option = cptOption;
        newCptOptions[index] = null;
        return;
      }
    }
  }); // Otherwise mapping by index.

  each(newCptOptions, function (cptOption, index) {
    if (!isObject(cptOption)) {
      return;
    }

    var i = 0;

    for (; i < result.length; i++) {
      var exist = result[i].exist;

      if (!result[i].option // Existing model that already has id should be able to
      // mapped to (because after mapping performed model may
      // be assigned with a id, whish should not affect next
      // mapping), except those has inner id.
      && !isIdInner(exist) // Caution:
      // Do not overwrite id. But name can be overwritten,
      // because axis use name as 'show label text'.
      // 'exist' always has id and name and we dont
      // need to check it.
      && cptOption.id == null) {
        result[i].option = cptOption;
        break;
      }
    }

    if (i >= result.length) {
      result.push({
        option: cptOption
      });
    }
  });
  return result;
}
/**
 * Make id and name for mapping result (result of mappingToExists)
 * into `keyInfo` field.
 *
 * @public
 * @param {Array.<Object>} Result, like [{exist: ..., option: ...}, {}],
 *                          which order is the same as exists.
 * @return {Array.<Object>} The input.
 */


function makeIdAndName(mapResult) {
  // We use this id to hash component models and view instances
  // in echarts. id can be specified by user, or auto generated.
  // The id generation rule ensures new view instance are able
  // to mapped to old instance when setOption are called in
  // no-merge mode. So we generate model id by name and plus
  // type in view id.
  // name can be duplicated among components, which is convenient
  // to specify multi components (like series) by one name.
  // Ensure that each id is distinct.
  var idMap = zrUtil.createHashMap();
  each(mapResult, function (item, index) {
    var existCpt = item.exist;
    existCpt && idMap.set(existCpt.id, item);
  });
  each(mapResult, function (item, index) {
    var opt = item.option;
    zrUtil.assert(!opt || opt.id == null || !idMap.get(opt.id) || idMap.get(opt.id) === item, 'id duplicates: ' + (opt && opt.id));
    opt && opt.id != null && idMap.set(opt.id, item);
    !item.keyInfo && (item.keyInfo = {});
  }); // Make name and id.

  each(mapResult, function (item, index) {
    var existCpt = item.exist;
    var opt = item.option;
    var keyInfo = item.keyInfo;

    if (!isObject(opt)) {
      return;
    } // name can be overwitten. Consider case: axis.name = '20km'.
    // But id generated by name will not be changed, which affect
    // only in that case: setOption with 'not merge mode' and view
    // instance will be recreated, which can be accepted.


    keyInfo.name = opt.name != null ? opt.name + '' : existCpt ? existCpt.name // Avoid diffferent series has the same name,
    // because name may be used like in color pallet.
    : DUMMY_COMPONENT_NAME_PREFIX + index;

    if (existCpt) {
      keyInfo.id = existCpt.id;
    } else if (opt.id != null) {
      keyInfo.id = opt.id + '';
    } else {
      // Consider this situatoin:
      //  optionA: [{name: 'a'}, {name: 'a'}, {..}]
      //  optionB [{..}, {name: 'a'}, {name: 'a'}]
      // Series with the same name between optionA and optionB
      // should be mapped.
      var idNum = 0;

      do {
        keyInfo.id = '\0' + keyInfo.name + '\0' + idNum++;
      } while (idMap.get(keyInfo.id));
    }

    idMap.set(keyInfo.id, item);
  });
}

function isNameSpecified(componentModel) {
  var name = componentModel.name; // Is specified when `indexOf` get -1 or > 0.

  return !!(name && name.indexOf(DUMMY_COMPONENT_NAME_PREFIX));
}
/**
 * @public
 * @param {Object} cptOption
 * @return {boolean}
 */


function isIdInner(cptOption) {
  return isObject(cptOption) && cptOption.id && (cptOption.id + '').indexOf('\0_ec_\0') === 0;
}
/**
 * A helper for removing duplicate items between batchA and batchB,
 * and in themselves, and categorize by series.
 *
 * @param {Array.<Object>} batchA Like: [{seriesId: 2, dataIndex: [32, 4, 5]}, ...]
 * @param {Array.<Object>} batchB Like: [{seriesId: 2, dataIndex: [32, 4, 5]}, ...]
 * @return {Array.<Array.<Object>, Array.<Object>>} result: [resultBatchA, resultBatchB]
 */


function compressBatches(batchA, batchB) {
  var mapA = {};
  var mapB = {};
  makeMap(batchA || [], mapA);
  makeMap(batchB || [], mapB, mapA);
  return [mapToArray(mapA), mapToArray(mapB)];

  function makeMap(sourceBatch, map, otherMap) {
    for (var i = 0, len = sourceBatch.length; i < len; i++) {
      var seriesId = sourceBatch[i].seriesId;
      var dataIndices = normalizeToArray(sourceBatch[i].dataIndex);
      var otherDataIndices = otherMap && otherMap[seriesId];

      for (var j = 0, lenj = dataIndices.length; j < lenj; j++) {
        var dataIndex = dataIndices[j];

        if (otherDataIndices && otherDataIndices[dataIndex]) {
          otherDataIndices[dataIndex] = null;
        } else {
          (map[seriesId] || (map[seriesId] = {}))[dataIndex] = 1;
        }
      }
    }
  }

  function mapToArray(map, isData) {
    var result = [];

    for (var i in map) {
      if (map.hasOwnProperty(i) && map[i] != null) {
        if (isData) {
          result.push(+i);
        } else {
          var dataIndices = mapToArray(map[i], true);
          dataIndices.length && result.push({
            seriesId: i,
            dataIndex: dataIndices
          });
        }
      }
    }

    return result;
  }
}
/**
 * @param {module:echarts/data/List} data
 * @param {Object} payload Contains dataIndex (means rawIndex) / dataIndexInside / name
 *                         each of which can be Array or primary type.
 * @return {number|Array.<number>} dataIndex If not found, return undefined/null.
 */


function queryDataIndex(data, payload) {
  if (payload.dataIndexInside != null) {
    return payload.dataIndexInside;
  } else if (payload.dataIndex != null) {
    return zrUtil.isArray(payload.dataIndex) ? zrUtil.map(payload.dataIndex, function (value) {
      return data.indexOfRawIndex(value);
    }) : data.indexOfRawIndex(payload.dataIndex);
  } else if (payload.name != null) {
    return zrUtil.isArray(payload.name) ? zrUtil.map(payload.name, function (value) {
      return data.indexOfName(value);
    }) : data.indexOfName(payload.name);
  }
}
/**
 * Enable property storage to any host object.
 * Notice: Serialization is not supported.
 *
 * For example:
 * var inner = zrUitl.makeInner();
 *
 * function some1(hostObj) {
 *      inner(hostObj).someProperty = 1212;
 *      ...
 * }
 * function some2() {
 *      var fields = inner(this);
 *      fields.someProperty1 = 1212;
 *      fields.someProperty2 = 'xx';
 *      ...
 * }
 *
 * @return {Function}
 */


function makeInner() {
  // Consider different scope by es module import.
  var key = '__\0ec_inner_' + innerUniqueIndex++ + '_' + Math.random().toFixed(5);
  return function (hostObj) {
    return hostObj[key] || (hostObj[key] = {});
  };
}

var innerUniqueIndex = 0;
/**
 * @param {module:echarts/model/Global} ecModel
 * @param {string|Object} finder
 *        If string, e.g., 'geo', means {geoIndex: 0}.
 *        If Object, could contain some of these properties below:
 *        {
 *            seriesIndex, seriesId, seriesName,
 *            geoIndex, geoId, geoName,
 *            bmapIndex, bmapId, bmapName,
 *            xAxisIndex, xAxisId, xAxisName,
 *            yAxisIndex, yAxisId, yAxisName,
 *            gridIndex, gridId, gridName,
 *            ... (can be extended)
 *        }
 *        Each properties can be number|string|Array.<number>|Array.<string>
 *        For example, a finder could be
 *        {
 *            seriesIndex: 3,
 *            geoId: ['aa', 'cc'],
 *            gridName: ['xx', 'rr']
 *        }
 *        xxxIndex can be set as 'all' (means all xxx) or 'none' (means not specify)
 *        If nothing or null/undefined specified, return nothing.
 * @param {Object} [opt]
 * @param {string} [opt.defaultMainType]
 * @param {Array.<string>} [opt.includeMainTypes]
 * @return {Object} result like:
 *        {
 *            seriesModels: [seriesModel1, seriesModel2],
 *            seriesModel: seriesModel1, // The first model
 *            geoModels: [geoModel1, geoModel2],
 *            geoModel: geoModel1, // The first model
 *            ...
 *        }
 */

function parseFinder(ecModel, finder, opt) {
  if (zrUtil.isString(finder)) {
    var obj = {};
    obj[finder + 'Index'] = 0;
    finder = obj;
  }

  var defaultMainType = opt && opt.defaultMainType;

  if (defaultMainType && !has(finder, defaultMainType + 'Index') && !has(finder, defaultMainType + 'Id') && !has(finder, defaultMainType + 'Name')) {
    finder[defaultMainType + 'Index'] = 0;
  }

  var result = {};
  each(finder, function (value, key) {
    var value = finder[key]; // Exclude 'dataIndex' and other illgal keys.

    if (key === 'dataIndex' || key === 'dataIndexInside') {
      result[key] = value;
      return;
    }

    var parsedKey = key.match(/^(\w+)(Index|Id|Name)$/) || [];
    var mainType = parsedKey[1];
    var queryType = (parsedKey[2] || '').toLowerCase();

    if (!mainType || !queryType || value == null || queryType === 'index' && value === 'none' || opt && opt.includeMainTypes && zrUtil.indexOf(opt.includeMainTypes, mainType) < 0) {
      return;
    }

    var queryParam = {
      mainType: mainType
    };

    if (queryType !== 'index' || value !== 'all') {
      queryParam[queryType] = value;
    }

    var models = ecModel.queryComponents(queryParam);
    result[mainType + 'Models'] = models;
    result[mainType + 'Model'] = models[0];
  });
  return result;
}

function has(obj, prop) {
  return obj && obj.hasOwnProperty(prop);
}

function setAttribute(dom, key, value) {
  dom.setAttribute ? dom.setAttribute(key, value) : dom[key] = value;
}

function getAttribute(dom, key) {
  return dom.getAttribute ? dom.getAttribute(key) : dom[key];
}

function getTooltipRenderMode(renderModeOption) {
  if (renderModeOption === 'auto') {
    // Using html when `document` exists, use richText otherwise
    return env.domSupported ? 'html' : 'richText';
  } else {
    return renderModeOption || 'html';
  }
}
/**
 * Group a list by key.
 *
 * @param {Array} array
 * @param {Function} getKey
 *        param {*} Array item
 *        return {string} key
 * @return {Object} Result
 *        {Array}: keys,
 *        {module:zrender/core/util/HashMap} buckets: {key -> Array}
 */


function groupData(array, getKey) {
  var buckets = zrUtil.createHashMap();
  var keys = [];
  zrUtil.each(array, function (item) {
    var key = getKey(item);
    (buckets.get(key) || (keys.push(key), buckets.set(key, []))).push(item);
  });
  return {
    keys: keys,
    buckets: buckets
  };
}

exports.normalizeToArray = normalizeToArray;
exports.defaultEmphasis = defaultEmphasis;
exports.TEXT_STYLE_OPTIONS = TEXT_STYLE_OPTIONS;
exports.getDataItemValue = getDataItemValue;
exports.isDataItemOption = isDataItemOption;
exports.mappingToExists = mappingToExists;
exports.makeIdAndName = makeIdAndName;
exports.isNameSpecified = isNameSpecified;
exports.isIdInner = isIdInner;
exports.compressBatches = compressBatches;
exports.queryDataIndex = queryDataIndex;
exports.makeInner = makeInner;
exports.parseFinder = parseFinder;
exports.setAttribute = setAttribute;
exports.getAttribute = getAttribute;
exports.getTooltipRenderMode = getTooltipRenderMode;
exports.groupData = groupData;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvdXRpbC9tb2RlbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3V0aWwvbW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZW52ID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvZW52XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgZWFjaCA9IHpyVXRpbC5lYWNoO1xudmFyIGlzT2JqZWN0ID0genJVdGlsLmlzT2JqZWN0O1xudmFyIGlzQXJyYXkgPSB6clV0aWwuaXNBcnJheTtcbi8qKlxuICogTWFrZSB0aGUgbmFtZSBkaXNwbGF5YWJsZS4gQnV0IHdlIHNob3VsZFxuICogbWFrZSBzdXJlIGl0IGlzIG5vdCBkdXBsaWNhdGVkIHdpdGggdXNlclxuICogc3BlY2lmaWVkIG5hbWUsIHNvIHVzZSAnXFwwJztcbiAqL1xuXG52YXIgRFVNTVlfQ09NUE9ORU5UX05BTUVfUFJFRklYID0gJ3Nlcmllc1xcMCc7XG4vKipcbiAqIElmIHZhbHVlIGlzIG5vdCBhcnJheSwgdGhlbiB0cmFuc2xhdGUgaXQgdG8gYXJyYXkuXG4gKiBAcGFyYW0gIHsqfSB2YWx1ZVxuICogQHJldHVybiB7QXJyYXl9IFt2YWx1ZV0gb3IgdmFsdWVcbiAqL1xuXG5mdW5jdGlvbiBub3JtYWxpemVUb0FycmF5KHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIEFycmF5ID8gdmFsdWUgOiB2YWx1ZSA9PSBudWxsID8gW10gOiBbdmFsdWVdO1xufVxuLyoqXG4gKiBTeW5jIGRlZmF1bHQgb3B0aW9uIGJldHdlZW4gbm9ybWFsIGFuZCBlbXBoYXNpcyBsaWtlIGBwb3NpdGlvbmAgYW5kIGBzaG93YFxuICogSW4gY2FzZSBzb21lIG9uZSB3aWxsIHdyaXRlIGNvZGUgbGlrZVxuICogICAgIGxhYmVsOiB7XG4gKiAgICAgICAgICBzaG93OiBmYWxzZSxcbiAqICAgICAgICAgIHBvc2l0aW9uOiAnb3V0c2lkZScsXG4gKiAgICAgICAgICBmb250U2l6ZTogMThcbiAqICAgICB9LFxuICogICAgIGVtcGhhc2lzOiB7XG4gKiAgICAgICAgICBsYWJlbDogeyBzaG93OiB0cnVlIH1cbiAqICAgICB9XG4gKiBAcGFyYW0ge09iamVjdH0gb3B0XG4gKiBAcGFyYW0ge3N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge0FycmF5LjxzdHJpbmc+fSBzdWJPcHRzXG4gKi9cblxuXG5mdW5jdGlvbiBkZWZhdWx0RW1waGFzaXMob3B0LCBrZXksIHN1Yk9wdHMpIHtcbiAgLy8gQ2F1dGlvbjogcGVyZm9ybWFuY2Ugc2Vuc2l0aXZlLlxuICBpZiAob3B0KSB7XG4gICAgb3B0W2tleV0gPSBvcHRba2V5XSB8fCB7fTtcbiAgICBvcHQuZW1waGFzaXMgPSBvcHQuZW1waGFzaXMgfHwge307XG4gICAgb3B0LmVtcGhhc2lzW2tleV0gPSBvcHQuZW1waGFzaXNba2V5XSB8fCB7fTsgLy8gRGVmYXVsdCBlbXBoYXNpcyBvcHRpb24gZnJvbSBub3JtYWxcblxuICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBzdWJPcHRzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICB2YXIgc3ViT3B0TmFtZSA9IHN1Yk9wdHNbaV07XG5cbiAgICAgIGlmICghb3B0LmVtcGhhc2lzW2tleV0uaGFzT3duUHJvcGVydHkoc3ViT3B0TmFtZSkgJiYgb3B0W2tleV0uaGFzT3duUHJvcGVydHkoc3ViT3B0TmFtZSkpIHtcbiAgICAgICAgb3B0LmVtcGhhc2lzW2tleV1bc3ViT3B0TmFtZV0gPSBvcHRba2V5XVtzdWJPcHROYW1lXTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxudmFyIFRFWFRfU1RZTEVfT1BUSU9OUyA9IFsnZm9udFN0eWxlJywgJ2ZvbnRXZWlnaHQnLCAnZm9udFNpemUnLCAnZm9udEZhbWlseScsICdyaWNoJywgJ3RhZycsICdjb2xvcicsICd0ZXh0Qm9yZGVyQ29sb3InLCAndGV4dEJvcmRlcldpZHRoJywgJ3dpZHRoJywgJ2hlaWdodCcsICdsaW5lSGVpZ2h0JywgJ2FsaWduJywgJ3ZlcnRpY2FsQWxpZ24nLCAnYmFzZWxpbmUnLCAnc2hhZG93Q29sb3InLCAnc2hhZG93Qmx1cicsICdzaGFkb3dPZmZzZXRYJywgJ3NoYWRvd09mZnNldFknLCAndGV4dFNoYWRvd0NvbG9yJywgJ3RleHRTaGFkb3dCbHVyJywgJ3RleHRTaGFkb3dPZmZzZXRYJywgJ3RleHRTaGFkb3dPZmZzZXRZJywgJ2JhY2tncm91bmRDb2xvcicsICdib3JkZXJDb2xvcicsICdib3JkZXJXaWR0aCcsICdib3JkZXJSYWRpdXMnLCAncGFkZGluZyddOyAvLyBtb2RlbFV0aWwuTEFCRUxfT1BUSU9OUyA9IG1vZGVsVXRpbC5URVhUX1NUWUxFX09QVElPTlMuY29uY2F0KFtcbi8vICAgICAncG9zaXRpb24nLCAnb2Zmc2V0JywgJ3JvdGF0ZScsICdvcmlnaW4nLCAnc2hvdycsICdkaXN0YW5jZScsICdmb3JtYXR0ZXInLFxuLy8gICAgICdmb250U3R5bGUnLCAnZm9udFdlaWdodCcsICdmb250U2l6ZScsICdmb250RmFtaWx5Jyxcbi8vICAgICAvLyBGSVhNRTogZGVwcmVjYXRlZCwgY2hlY2sgYW5kIHJlbW92ZSBpdC5cbi8vICAgICAndGV4dFN0eWxlJ1xuLy8gXSk7XG5cbi8qKlxuICogVGhlIG1ldGhvZCBkbyBub3QgZW5zdXJlIHBlcmZvcm1hbmNlLlxuICogZGF0YSBjb3VsZCBiZSBbMTIsIDIzMjMsIHt2YWx1ZTogMjIzfSwgWzEyMjEsIDIzXSwge3ZhbHVlOiBbMiwgMjNdfV1cbiAqIFRoaXMgaGVscGVyIG1ldGhvZCByZXRpZXZlcyB2YWx1ZSBmcm9tIGRhdGEuXG4gKiBAcGFyYW0ge3N0cmluZ3xudW1iZXJ8RGF0ZXxBcnJheXxPYmplY3R9IGRhdGFJdGVtXG4gKiBAcmV0dXJuIHtudW1iZXJ8c3RyaW5nfERhdGV8QXJyYXkuPG51bWJlcnxzdHJpbmd8RGF0ZT59XG4gKi9cblxuZnVuY3Rpb24gZ2V0RGF0YUl0ZW1WYWx1ZShkYXRhSXRlbSkge1xuICByZXR1cm4gaXNPYmplY3QoZGF0YUl0ZW0pICYmICFpc0FycmF5KGRhdGFJdGVtKSAmJiAhKGRhdGFJdGVtIGluc3RhbmNlb2YgRGF0ZSkgPyBkYXRhSXRlbS52YWx1ZSA6IGRhdGFJdGVtO1xufVxuLyoqXG4gKiBkYXRhIGNvdWxkIGJlIFsxMiwgMjMyMywge3ZhbHVlOiAyMjN9LCBbMTIyMSwgMjNdLCB7dmFsdWU6IFsyLCAyM119XVxuICogVGhpcyBoZWxwZXIgbWV0aG9kIGRldGVybWluZSBpZiBkYXRhSXRlbSBoYXMgZXh0cmEgb3B0aW9uIGJlc2lkZXMgdmFsdWVcbiAqIEBwYXJhbSB7c3RyaW5nfG51bWJlcnxEYXRlfEFycmF5fE9iamVjdH0gZGF0YUl0ZW1cbiAqL1xuXG5cbmZ1bmN0aW9uIGlzRGF0YUl0ZW1PcHRpb24oZGF0YUl0ZW0pIHtcbiAgcmV0dXJuIGlzT2JqZWN0KGRhdGFJdGVtKSAmJiAhKGRhdGFJdGVtIGluc3RhbmNlb2YgQXJyYXkpOyAvLyAvLyBtYXJrTGluZSBkYXRhIGNhbiBiZSBhcnJheVxuICAvLyAmJiAhKGRhdGFJdGVtWzBdICYmIGlzT2JqZWN0KGRhdGFJdGVtWzBdKSAmJiAhKGRhdGFJdGVtWzBdIGluc3RhbmNlb2YgQXJyYXkpKTtcbn1cbi8qKlxuICogTWFwcGluZyB0byBleGlzdHMgZm9yIG1lcmdlLlxuICpcbiAqIEBwdWJsaWNcbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdD58QXJyYXkuPG1vZHVsZTplY2hhcnRzL21vZGVsL0NvbXBvbmVudD59IGV4aXN0c1xuICogQHBhcmFtIHtPYmplY3R8QXJyYXkuPE9iamVjdD59IG5ld0NwdE9wdGlvbnNcbiAqIEByZXR1cm4ge0FycmF5LjxPYmplY3Q+fSBSZXN1bHQsIGxpa2UgW3tleGlzdDogLi4uLCBvcHRpb246IC4uLn0sIHt9XSxcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICBpbmRleCBvZiB3aGljaCBpcyB0aGUgc2FtZSBhcyBleGlzdHMuXG4gKi9cblxuXG5mdW5jdGlvbiBtYXBwaW5nVG9FeGlzdHMoZXhpc3RzLCBuZXdDcHRPcHRpb25zKSB7XG4gIC8vIE1hcHBpbmcgYnkgdGhlIG9yZGVyIGJ5IG9yaWdpbmFsIG9wdGlvbiAoYnV0IG5vdCBvcmRlciBvZlxuICAvLyBuZXcgb3B0aW9uKSBpbiBtZXJnZSBtb2RlLiBCZWNhdXNlIHdlIHNob3VsZCBlbnN1cmVcbiAgLy8gc29tZSBzcGVjaWZpZWQgaW5kZXggKGxpa2UgeEF4aXNJbmRleCkgaXMgY29uc2lzdGVudCB3aXRoXG4gIC8vIG9yaWdpbmFsIG9wdGlvbiwgd2hpY2ggaXMgZWFzeSB0byB1bmRlcnN0YW5kLCBlc3BhdGlhbGx5IGluXG4gIC8vIG1lZGlhIHF1ZXJ5LiBBbmQgaW4gbW9zdCBjYXNlLCBtZXJnZSBvcHRpb24gaXMgdXNlZCB0b1xuICAvLyB1cGRhdGUgcGFydGlhbCBvcHRpb24gYnV0IG5vdCBiZSBleHBlY3RlZCB0byBjaGFuZ2Ugb3JkZXIuXG4gIG5ld0NwdE9wdGlvbnMgPSAobmV3Q3B0T3B0aW9ucyB8fCBbXSkuc2xpY2UoKTtcbiAgdmFyIHJlc3VsdCA9IHpyVXRpbC5tYXAoZXhpc3RzIHx8IFtdLCBmdW5jdGlvbiAob2JqLCBpbmRleCkge1xuICAgIHJldHVybiB7XG4gICAgICBleGlzdDogb2JqXG4gICAgfTtcbiAgfSk7IC8vIE1hcHBpbmcgYnkgaWQgb3IgbmFtZSBpZiBzcGVjaWZpZWQuXG5cbiAgZWFjaChuZXdDcHRPcHRpb25zLCBmdW5jdGlvbiAoY3B0T3B0aW9uLCBpbmRleCkge1xuICAgIGlmICghaXNPYmplY3QoY3B0T3B0aW9uKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH0gLy8gaWQgaGFzIGhpZ2hlc3QgcHJpb3JpdHkuXG5cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmVzdWx0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAoIXJlc3VsdFtpXS5vcHRpb24gLy8gQ29uc2lkZXIgbmFtZTogdHdvIG1hcCB0byBvbmUuXG4gICAgICAmJiBjcHRPcHRpb24uaWQgIT0gbnVsbCAmJiByZXN1bHRbaV0uZXhpc3QuaWQgPT09IGNwdE9wdGlvbi5pZCArICcnKSB7XG4gICAgICAgIHJlc3VsdFtpXS5vcHRpb24gPSBjcHRPcHRpb247XG4gICAgICAgIG5ld0NwdE9wdGlvbnNbaW5kZXhdID0gbnVsbDtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcmVzdWx0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZXhpc3QgPSByZXN1bHRbaV0uZXhpc3Q7XG5cbiAgICAgIGlmICghcmVzdWx0W2ldLm9wdGlvbiAvLyBDb25zaWRlciBuYW1lOiB0d28gbWFwIHRvIG9uZS5cbiAgICAgIC8vIENhbiBub3QgbWF0Y2ggd2hlbiBib3RoIGlkcyBleGlzdCBidXQgZGlmZmVyZW50LlxuICAgICAgJiYgKGV4aXN0LmlkID09IG51bGwgfHwgY3B0T3B0aW9uLmlkID09IG51bGwpICYmIGNwdE9wdGlvbi5uYW1lICE9IG51bGwgJiYgIWlzSWRJbm5lcihjcHRPcHRpb24pICYmICFpc0lkSW5uZXIoZXhpc3QpICYmIGV4aXN0Lm5hbWUgPT09IGNwdE9wdGlvbi5uYW1lICsgJycpIHtcbiAgICAgICAgcmVzdWx0W2ldLm9wdGlvbiA9IGNwdE9wdGlvbjtcbiAgICAgICAgbmV3Q3B0T3B0aW9uc1tpbmRleF0gPSBudWxsO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgfVxuICB9KTsgLy8gT3RoZXJ3aXNlIG1hcHBpbmcgYnkgaW5kZXguXG5cbiAgZWFjaChuZXdDcHRPcHRpb25zLCBmdW5jdGlvbiAoY3B0T3B0aW9uLCBpbmRleCkge1xuICAgIGlmICghaXNPYmplY3QoY3B0T3B0aW9uKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBpID0gMDtcblxuICAgIGZvciAoOyBpIDwgcmVzdWx0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZXhpc3QgPSByZXN1bHRbaV0uZXhpc3Q7XG5cbiAgICAgIGlmICghcmVzdWx0W2ldLm9wdGlvbiAvLyBFeGlzdGluZyBtb2RlbCB0aGF0IGFscmVhZHkgaGFzIGlkIHNob3VsZCBiZSBhYmxlIHRvXG4gICAgICAvLyBtYXBwZWQgdG8gKGJlY2F1c2UgYWZ0ZXIgbWFwcGluZyBwZXJmb3JtZWQgbW9kZWwgbWF5XG4gICAgICAvLyBiZSBhc3NpZ25lZCB3aXRoIGEgaWQsIHdoaXNoIHNob3VsZCBub3QgYWZmZWN0IG5leHRcbiAgICAgIC8vIG1hcHBpbmcpLCBleGNlcHQgdGhvc2UgaGFzIGlubmVyIGlkLlxuICAgICAgJiYgIWlzSWRJbm5lcihleGlzdCkgLy8gQ2F1dGlvbjpcbiAgICAgIC8vIERvIG5vdCBvdmVyd3JpdGUgaWQuIEJ1dCBuYW1lIGNhbiBiZSBvdmVyd3JpdHRlbixcbiAgICAgIC8vIGJlY2F1c2UgYXhpcyB1c2UgbmFtZSBhcyAnc2hvdyBsYWJlbCB0ZXh0Jy5cbiAgICAgIC8vICdleGlzdCcgYWx3YXlzIGhhcyBpZCBhbmQgbmFtZSBhbmQgd2UgZG9udFxuICAgICAgLy8gbmVlZCB0byBjaGVjayBpdC5cbiAgICAgICYmIGNwdE9wdGlvbi5pZCA9PSBudWxsKSB7XG4gICAgICAgIHJlc3VsdFtpXS5vcHRpb24gPSBjcHRPcHRpb247XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChpID49IHJlc3VsdC5sZW5ndGgpIHtcbiAgICAgIHJlc3VsdC5wdXNoKHtcbiAgICAgICAgb3B0aW9uOiBjcHRPcHRpb25cbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiByZXN1bHQ7XG59XG4vKipcbiAqIE1ha2UgaWQgYW5kIG5hbWUgZm9yIG1hcHBpbmcgcmVzdWx0IChyZXN1bHQgb2YgbWFwcGluZ1RvRXhpc3RzKVxuICogaW50byBga2V5SW5mb2AgZmllbGQuXG4gKlxuICogQHB1YmxpY1xuICogQHBhcmFtIHtBcnJheS48T2JqZWN0Pn0gUmVzdWx0LCBsaWtlIFt7ZXhpc3Q6IC4uLiwgb3B0aW9uOiAuLi59LCB7fV0sXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgd2hpY2ggb3JkZXIgaXMgdGhlIHNhbWUgYXMgZXhpc3RzLlxuICogQHJldHVybiB7QXJyYXkuPE9iamVjdD59IFRoZSBpbnB1dC5cbiAqL1xuXG5cbmZ1bmN0aW9uIG1ha2VJZEFuZE5hbWUobWFwUmVzdWx0KSB7XG4gIC8vIFdlIHVzZSB0aGlzIGlkIHRvIGhhc2ggY29tcG9uZW50IG1vZGVscyBhbmQgdmlldyBpbnN0YW5jZXNcbiAgLy8gaW4gZWNoYXJ0cy4gaWQgY2FuIGJlIHNwZWNpZmllZCBieSB1c2VyLCBvciBhdXRvIGdlbmVyYXRlZC5cbiAgLy8gVGhlIGlkIGdlbmVyYXRpb24gcnVsZSBlbnN1cmVzIG5ldyB2aWV3IGluc3RhbmNlIGFyZSBhYmxlXG4gIC8vIHRvIG1hcHBlZCB0byBvbGQgaW5zdGFuY2Ugd2hlbiBzZXRPcHRpb24gYXJlIGNhbGxlZCBpblxuICAvLyBuby1tZXJnZSBtb2RlLiBTbyB3ZSBnZW5lcmF0ZSBtb2RlbCBpZCBieSBuYW1lIGFuZCBwbHVzXG4gIC8vIHR5cGUgaW4gdmlldyBpZC5cbiAgLy8gbmFtZSBjYW4gYmUgZHVwbGljYXRlZCBhbW9uZyBjb21wb25lbnRzLCB3aGljaCBpcyBjb252ZW5pZW50XG4gIC8vIHRvIHNwZWNpZnkgbXVsdGkgY29tcG9uZW50cyAobGlrZSBzZXJpZXMpIGJ5IG9uZSBuYW1lLlxuICAvLyBFbnN1cmUgdGhhdCBlYWNoIGlkIGlzIGRpc3RpbmN0LlxuICB2YXIgaWRNYXAgPSB6clV0aWwuY3JlYXRlSGFzaE1hcCgpO1xuICBlYWNoKG1hcFJlc3VsdCwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XG4gICAgdmFyIGV4aXN0Q3B0ID0gaXRlbS5leGlzdDtcbiAgICBleGlzdENwdCAmJiBpZE1hcC5zZXQoZXhpc3RDcHQuaWQsIGl0ZW0pO1xuICB9KTtcbiAgZWFjaChtYXBSZXN1bHQsIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xuICAgIHZhciBvcHQgPSBpdGVtLm9wdGlvbjtcbiAgICB6clV0aWwuYXNzZXJ0KCFvcHQgfHwgb3B0LmlkID09IG51bGwgfHwgIWlkTWFwLmdldChvcHQuaWQpIHx8IGlkTWFwLmdldChvcHQuaWQpID09PSBpdGVtLCAnaWQgZHVwbGljYXRlczogJyArIChvcHQgJiYgb3B0LmlkKSk7XG4gICAgb3B0ICYmIG9wdC5pZCAhPSBudWxsICYmIGlkTWFwLnNldChvcHQuaWQsIGl0ZW0pO1xuICAgICFpdGVtLmtleUluZm8gJiYgKGl0ZW0ua2V5SW5mbyA9IHt9KTtcbiAgfSk7IC8vIE1ha2UgbmFtZSBhbmQgaWQuXG5cbiAgZWFjaChtYXBSZXN1bHQsIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xuICAgIHZhciBleGlzdENwdCA9IGl0ZW0uZXhpc3Q7XG4gICAgdmFyIG9wdCA9IGl0ZW0ub3B0aW9uO1xuICAgIHZhciBrZXlJbmZvID0gaXRlbS5rZXlJbmZvO1xuXG4gICAgaWYgKCFpc09iamVjdChvcHQpKSB7XG4gICAgICByZXR1cm47XG4gICAgfSAvLyBuYW1lIGNhbiBiZSBvdmVyd2l0dGVuLiBDb25zaWRlciBjYXNlOiBheGlzLm5hbWUgPSAnMjBrbScuXG4gICAgLy8gQnV0IGlkIGdlbmVyYXRlZCBieSBuYW1lIHdpbGwgbm90IGJlIGNoYW5nZWQsIHdoaWNoIGFmZmVjdFxuICAgIC8vIG9ubHkgaW4gdGhhdCBjYXNlOiBzZXRPcHRpb24gd2l0aCAnbm90IG1lcmdlIG1vZGUnIGFuZCB2aWV3XG4gICAgLy8gaW5zdGFuY2Ugd2lsbCBiZSByZWNyZWF0ZWQsIHdoaWNoIGNhbiBiZSBhY2NlcHRlZC5cblxuXG4gICAga2V5SW5mby5uYW1lID0gb3B0Lm5hbWUgIT0gbnVsbCA/IG9wdC5uYW1lICsgJycgOiBleGlzdENwdCA/IGV4aXN0Q3B0Lm5hbWUgLy8gQXZvaWQgZGlmZmZlcmVudCBzZXJpZXMgaGFzIHRoZSBzYW1lIG5hbWUsXG4gICAgLy8gYmVjYXVzZSBuYW1lIG1heSBiZSB1c2VkIGxpa2UgaW4gY29sb3IgcGFsbGV0LlxuICAgIDogRFVNTVlfQ09NUE9ORU5UX05BTUVfUFJFRklYICsgaW5kZXg7XG5cbiAgICBpZiAoZXhpc3RDcHQpIHtcbiAgICAgIGtleUluZm8uaWQgPSBleGlzdENwdC5pZDtcbiAgICB9IGVsc2UgaWYgKG9wdC5pZCAhPSBudWxsKSB7XG4gICAgICBrZXlJbmZvLmlkID0gb3B0LmlkICsgJyc7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIENvbnNpZGVyIHRoaXMgc2l0dWF0b2luOlxuICAgICAgLy8gIG9wdGlvbkE6IFt7bmFtZTogJ2EnfSwge25hbWU6ICdhJ30sIHsuLn1dXG4gICAgICAvLyAgb3B0aW9uQiBbey4ufSwge25hbWU6ICdhJ30sIHtuYW1lOiAnYSd9XVxuICAgICAgLy8gU2VyaWVzIHdpdGggdGhlIHNhbWUgbmFtZSBiZXR3ZWVuIG9wdGlvbkEgYW5kIG9wdGlvbkJcbiAgICAgIC8vIHNob3VsZCBiZSBtYXBwZWQuXG4gICAgICB2YXIgaWROdW0gPSAwO1xuXG4gICAgICBkbyB7XG4gICAgICAgIGtleUluZm8uaWQgPSAnXFwwJyArIGtleUluZm8ubmFtZSArICdcXDAnICsgaWROdW0rKztcbiAgICAgIH0gd2hpbGUgKGlkTWFwLmdldChrZXlJbmZvLmlkKSk7XG4gICAgfVxuXG4gICAgaWRNYXAuc2V0KGtleUluZm8uaWQsIGl0ZW0pO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gaXNOYW1lU3BlY2lmaWVkKGNvbXBvbmVudE1vZGVsKSB7XG4gIHZhciBuYW1lID0gY29tcG9uZW50TW9kZWwubmFtZTsgLy8gSXMgc3BlY2lmaWVkIHdoZW4gYGluZGV4T2ZgIGdldCAtMSBvciA+IDAuXG5cbiAgcmV0dXJuICEhKG5hbWUgJiYgbmFtZS5pbmRleE9mKERVTU1ZX0NPTVBPTkVOVF9OQU1FX1BSRUZJWCkpO1xufVxuLyoqXG4gKiBAcHVibGljXG4gKiBAcGFyYW0ge09iamVjdH0gY3B0T3B0aW9uXG4gKiBAcmV0dXJuIHtib29sZWFufVxuICovXG5cblxuZnVuY3Rpb24gaXNJZElubmVyKGNwdE9wdGlvbikge1xuICByZXR1cm4gaXNPYmplY3QoY3B0T3B0aW9uKSAmJiBjcHRPcHRpb24uaWQgJiYgKGNwdE9wdGlvbi5pZCArICcnKS5pbmRleE9mKCdcXDBfZWNfXFwwJykgPT09IDA7XG59XG4vKipcbiAqIEEgaGVscGVyIGZvciByZW1vdmluZyBkdXBsaWNhdGUgaXRlbXMgYmV0d2VlbiBiYXRjaEEgYW5kIGJhdGNoQixcbiAqIGFuZCBpbiB0aGVtc2VsdmVzLCBhbmQgY2F0ZWdvcml6ZSBieSBzZXJpZXMuXG4gKlxuICogQHBhcmFtIHtBcnJheS48T2JqZWN0Pn0gYmF0Y2hBIExpa2U6IFt7c2VyaWVzSWQ6IDIsIGRhdGFJbmRleDogWzMyLCA0LCA1XX0sIC4uLl1cbiAqIEBwYXJhbSB7QXJyYXkuPE9iamVjdD59IGJhdGNoQiBMaWtlOiBbe3Nlcmllc0lkOiAyLCBkYXRhSW5kZXg6IFszMiwgNCwgNV19LCAuLi5dXG4gKiBAcmV0dXJuIHtBcnJheS48QXJyYXkuPE9iamVjdD4sIEFycmF5LjxPYmplY3Q+Pn0gcmVzdWx0OiBbcmVzdWx0QmF0Y2hBLCByZXN1bHRCYXRjaEJdXG4gKi9cblxuXG5mdW5jdGlvbiBjb21wcmVzc0JhdGNoZXMoYmF0Y2hBLCBiYXRjaEIpIHtcbiAgdmFyIG1hcEEgPSB7fTtcbiAgdmFyIG1hcEIgPSB7fTtcbiAgbWFrZU1hcChiYXRjaEEgfHwgW10sIG1hcEEpO1xuICBtYWtlTWFwKGJhdGNoQiB8fCBbXSwgbWFwQiwgbWFwQSk7XG4gIHJldHVybiBbbWFwVG9BcnJheShtYXBBKSwgbWFwVG9BcnJheShtYXBCKV07XG5cbiAgZnVuY3Rpb24gbWFrZU1hcChzb3VyY2VCYXRjaCwgbWFwLCBvdGhlck1hcCkge1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBzb3VyY2VCYXRjaC5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgdmFyIHNlcmllc0lkID0gc291cmNlQmF0Y2hbaV0uc2VyaWVzSWQ7XG4gICAgICB2YXIgZGF0YUluZGljZXMgPSBub3JtYWxpemVUb0FycmF5KHNvdXJjZUJhdGNoW2ldLmRhdGFJbmRleCk7XG4gICAgICB2YXIgb3RoZXJEYXRhSW5kaWNlcyA9IG90aGVyTWFwICYmIG90aGVyTWFwW3Nlcmllc0lkXTtcblxuICAgICAgZm9yICh2YXIgaiA9IDAsIGxlbmogPSBkYXRhSW5kaWNlcy5sZW5ndGg7IGogPCBsZW5qOyBqKyspIHtcbiAgICAgICAgdmFyIGRhdGFJbmRleCA9IGRhdGFJbmRpY2VzW2pdO1xuXG4gICAgICAgIGlmIChvdGhlckRhdGFJbmRpY2VzICYmIG90aGVyRGF0YUluZGljZXNbZGF0YUluZGV4XSkge1xuICAgICAgICAgIG90aGVyRGF0YUluZGljZXNbZGF0YUluZGV4XSA9IG51bGw7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgKG1hcFtzZXJpZXNJZF0gfHwgKG1hcFtzZXJpZXNJZF0gPSB7fSkpW2RhdGFJbmRleF0gPSAxO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gbWFwVG9BcnJheShtYXAsIGlzRGF0YSkge1xuICAgIHZhciByZXN1bHQgPSBbXTtcblxuICAgIGZvciAodmFyIGkgaW4gbWFwKSB7XG4gICAgICBpZiAobWFwLmhhc093blByb3BlcnR5KGkpICYmIG1hcFtpXSAhPSBudWxsKSB7XG4gICAgICAgIGlmIChpc0RhdGEpIHtcbiAgICAgICAgICByZXN1bHQucHVzaCgraSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFyIGRhdGFJbmRpY2VzID0gbWFwVG9BcnJheShtYXBbaV0sIHRydWUpO1xuICAgICAgICAgIGRhdGFJbmRpY2VzLmxlbmd0aCAmJiByZXN1bHQucHVzaCh7XG4gICAgICAgICAgICBzZXJpZXNJZDogaSxcbiAgICAgICAgICAgIGRhdGFJbmRleDogZGF0YUluZGljZXNcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbn1cbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9IGRhdGFcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIENvbnRhaW5zIGRhdGFJbmRleCAobWVhbnMgcmF3SW5kZXgpIC8gZGF0YUluZGV4SW5zaWRlIC8gbmFtZVxuICogICAgICAgICAgICAgICAgICAgICAgICAgZWFjaCBvZiB3aGljaCBjYW4gYmUgQXJyYXkgb3IgcHJpbWFyeSB0eXBlLlxuICogQHJldHVybiB7bnVtYmVyfEFycmF5LjxudW1iZXI+fSBkYXRhSW5kZXggSWYgbm90IGZvdW5kLCByZXR1cm4gdW5kZWZpbmVkL251bGwuXG4gKi9cblxuXG5mdW5jdGlvbiBxdWVyeURhdGFJbmRleChkYXRhLCBwYXlsb2FkKSB7XG4gIGlmIChwYXlsb2FkLmRhdGFJbmRleEluc2lkZSAhPSBudWxsKSB7XG4gICAgcmV0dXJuIHBheWxvYWQuZGF0YUluZGV4SW5zaWRlO1xuICB9IGVsc2UgaWYgKHBheWxvYWQuZGF0YUluZGV4ICE9IG51bGwpIHtcbiAgICByZXR1cm4genJVdGlsLmlzQXJyYXkocGF5bG9hZC5kYXRhSW5kZXgpID8genJVdGlsLm1hcChwYXlsb2FkLmRhdGFJbmRleCwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICByZXR1cm4gZGF0YS5pbmRleE9mUmF3SW5kZXgodmFsdWUpO1xuICAgIH0pIDogZGF0YS5pbmRleE9mUmF3SW5kZXgocGF5bG9hZC5kYXRhSW5kZXgpO1xuICB9IGVsc2UgaWYgKHBheWxvYWQubmFtZSAhPSBudWxsKSB7XG4gICAgcmV0dXJuIHpyVXRpbC5pc0FycmF5KHBheWxvYWQubmFtZSkgPyB6clV0aWwubWFwKHBheWxvYWQubmFtZSwgZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICByZXR1cm4gZGF0YS5pbmRleE9mTmFtZSh2YWx1ZSk7XG4gICAgfSkgOiBkYXRhLmluZGV4T2ZOYW1lKHBheWxvYWQubmFtZSk7XG4gIH1cbn1cbi8qKlxuICogRW5hYmxlIHByb3BlcnR5IHN0b3JhZ2UgdG8gYW55IGhvc3Qgb2JqZWN0LlxuICogTm90aWNlOiBTZXJpYWxpemF0aW9uIGlzIG5vdCBzdXBwb3J0ZWQuXG4gKlxuICogRm9yIGV4YW1wbGU6XG4gKiB2YXIgaW5uZXIgPSB6clVpdGwubWFrZUlubmVyKCk7XG4gKlxuICogZnVuY3Rpb24gc29tZTEoaG9zdE9iaikge1xuICogICAgICBpbm5lcihob3N0T2JqKS5zb21lUHJvcGVydHkgPSAxMjEyO1xuICogICAgICAuLi5cbiAqIH1cbiAqIGZ1bmN0aW9uIHNvbWUyKCkge1xuICogICAgICB2YXIgZmllbGRzID0gaW5uZXIodGhpcyk7XG4gKiAgICAgIGZpZWxkcy5zb21lUHJvcGVydHkxID0gMTIxMjtcbiAqICAgICAgZmllbGRzLnNvbWVQcm9wZXJ0eTIgPSAneHgnO1xuICogICAgICAuLi5cbiAqIH1cbiAqXG4gKiBAcmV0dXJuIHtGdW5jdGlvbn1cbiAqL1xuXG5cbmZ1bmN0aW9uIG1ha2VJbm5lcigpIHtcbiAgLy8gQ29uc2lkZXIgZGlmZmVyZW50IHNjb3BlIGJ5IGVzIG1vZHVsZSBpbXBvcnQuXG4gIHZhciBrZXkgPSAnX19cXDBlY19pbm5lcl8nICsgaW5uZXJVbmlxdWVJbmRleCsrICsgJ18nICsgTWF0aC5yYW5kb20oKS50b0ZpeGVkKDUpO1xuICByZXR1cm4gZnVuY3Rpb24gKGhvc3RPYmopIHtcbiAgICByZXR1cm4gaG9zdE9ialtrZXldIHx8IChob3N0T2JqW2tleV0gPSB7fSk7XG4gIH07XG59XG5cbnZhciBpbm5lclVuaXF1ZUluZGV4ID0gMDtcbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAqIEBwYXJhbSB7c3RyaW5nfE9iamVjdH0gZmluZGVyXG4gKiAgICAgICAgSWYgc3RyaW5nLCBlLmcuLCAnZ2VvJywgbWVhbnMge2dlb0luZGV4OiAwfS5cbiAqICAgICAgICBJZiBPYmplY3QsIGNvdWxkIGNvbnRhaW4gc29tZSBvZiB0aGVzZSBwcm9wZXJ0aWVzIGJlbG93OlxuICogICAgICAgIHtcbiAqICAgICAgICAgICAgc2VyaWVzSW5kZXgsIHNlcmllc0lkLCBzZXJpZXNOYW1lLFxuICogICAgICAgICAgICBnZW9JbmRleCwgZ2VvSWQsIGdlb05hbWUsXG4gKiAgICAgICAgICAgIGJtYXBJbmRleCwgYm1hcElkLCBibWFwTmFtZSxcbiAqICAgICAgICAgICAgeEF4aXNJbmRleCwgeEF4aXNJZCwgeEF4aXNOYW1lLFxuICogICAgICAgICAgICB5QXhpc0luZGV4LCB5QXhpc0lkLCB5QXhpc05hbWUsXG4gKiAgICAgICAgICAgIGdyaWRJbmRleCwgZ3JpZElkLCBncmlkTmFtZSxcbiAqICAgICAgICAgICAgLi4uIChjYW4gYmUgZXh0ZW5kZWQpXG4gKiAgICAgICAgfVxuICogICAgICAgIEVhY2ggcHJvcGVydGllcyBjYW4gYmUgbnVtYmVyfHN0cmluZ3xBcnJheS48bnVtYmVyPnxBcnJheS48c3RyaW5nPlxuICogICAgICAgIEZvciBleGFtcGxlLCBhIGZpbmRlciBjb3VsZCBiZVxuICogICAgICAgIHtcbiAqICAgICAgICAgICAgc2VyaWVzSW5kZXg6IDMsXG4gKiAgICAgICAgICAgIGdlb0lkOiBbJ2FhJywgJ2NjJ10sXG4gKiAgICAgICAgICAgIGdyaWROYW1lOiBbJ3h4JywgJ3JyJ11cbiAqICAgICAgICB9XG4gKiAgICAgICAgeHh4SW5kZXggY2FuIGJlIHNldCBhcyAnYWxsJyAobWVhbnMgYWxsIHh4eCkgb3IgJ25vbmUnIChtZWFucyBub3Qgc3BlY2lmeSlcbiAqICAgICAgICBJZiBub3RoaW5nIG9yIG51bGwvdW5kZWZpbmVkIHNwZWNpZmllZCwgcmV0dXJuIG5vdGhpbmcuXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdF1cbiAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0LmRlZmF1bHRNYWluVHlwZV1cbiAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IFtvcHQuaW5jbHVkZU1haW5UeXBlc11cbiAqIEByZXR1cm4ge09iamVjdH0gcmVzdWx0IGxpa2U6XG4gKiAgICAgICAge1xuICogICAgICAgICAgICBzZXJpZXNNb2RlbHM6IFtzZXJpZXNNb2RlbDEsIHNlcmllc01vZGVsMl0sXG4gKiAgICAgICAgICAgIHNlcmllc01vZGVsOiBzZXJpZXNNb2RlbDEsIC8vIFRoZSBmaXJzdCBtb2RlbFxuICogICAgICAgICAgICBnZW9Nb2RlbHM6IFtnZW9Nb2RlbDEsIGdlb01vZGVsMl0sXG4gKiAgICAgICAgICAgIGdlb01vZGVsOiBnZW9Nb2RlbDEsIC8vIFRoZSBmaXJzdCBtb2RlbFxuICogICAgICAgICAgICAuLi5cbiAqICAgICAgICB9XG4gKi9cblxuZnVuY3Rpb24gcGFyc2VGaW5kZXIoZWNNb2RlbCwgZmluZGVyLCBvcHQpIHtcbiAgaWYgKHpyVXRpbC5pc1N0cmluZyhmaW5kZXIpKSB7XG4gICAgdmFyIG9iaiA9IHt9O1xuICAgIG9ialtmaW5kZXIgKyAnSW5kZXgnXSA9IDA7XG4gICAgZmluZGVyID0gb2JqO1xuICB9XG5cbiAgdmFyIGRlZmF1bHRNYWluVHlwZSA9IG9wdCAmJiBvcHQuZGVmYXVsdE1haW5UeXBlO1xuXG4gIGlmIChkZWZhdWx0TWFpblR5cGUgJiYgIWhhcyhmaW5kZXIsIGRlZmF1bHRNYWluVHlwZSArICdJbmRleCcpICYmICFoYXMoZmluZGVyLCBkZWZhdWx0TWFpblR5cGUgKyAnSWQnKSAmJiAhaGFzKGZpbmRlciwgZGVmYXVsdE1haW5UeXBlICsgJ05hbWUnKSkge1xuICAgIGZpbmRlcltkZWZhdWx0TWFpblR5cGUgKyAnSW5kZXgnXSA9IDA7XG4gIH1cblxuICB2YXIgcmVzdWx0ID0ge307XG4gIGVhY2goZmluZGVyLCBmdW5jdGlvbiAodmFsdWUsIGtleSkge1xuICAgIHZhciB2YWx1ZSA9IGZpbmRlcltrZXldOyAvLyBFeGNsdWRlICdkYXRhSW5kZXgnIGFuZCBvdGhlciBpbGxnYWwga2V5cy5cblxuICAgIGlmIChrZXkgPT09ICdkYXRhSW5kZXgnIHx8IGtleSA9PT0gJ2RhdGFJbmRleEluc2lkZScpIHtcbiAgICAgIHJlc3VsdFtrZXldID0gdmFsdWU7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHBhcnNlZEtleSA9IGtleS5tYXRjaCgvXihcXHcrKShJbmRleHxJZHxOYW1lKSQvKSB8fCBbXTtcbiAgICB2YXIgbWFpblR5cGUgPSBwYXJzZWRLZXlbMV07XG4gICAgdmFyIHF1ZXJ5VHlwZSA9IChwYXJzZWRLZXlbMl0gfHwgJycpLnRvTG93ZXJDYXNlKCk7XG5cbiAgICBpZiAoIW1haW5UeXBlIHx8ICFxdWVyeVR5cGUgfHwgdmFsdWUgPT0gbnVsbCB8fCBxdWVyeVR5cGUgPT09ICdpbmRleCcgJiYgdmFsdWUgPT09ICdub25lJyB8fCBvcHQgJiYgb3B0LmluY2x1ZGVNYWluVHlwZXMgJiYgenJVdGlsLmluZGV4T2Yob3B0LmluY2x1ZGVNYWluVHlwZXMsIG1haW5UeXBlKSA8IDApIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcXVlcnlQYXJhbSA9IHtcbiAgICAgIG1haW5UeXBlOiBtYWluVHlwZVxuICAgIH07XG5cbiAgICBpZiAocXVlcnlUeXBlICE9PSAnaW5kZXgnIHx8IHZhbHVlICE9PSAnYWxsJykge1xuICAgICAgcXVlcnlQYXJhbVtxdWVyeVR5cGVdID0gdmFsdWU7XG4gICAgfVxuXG4gICAgdmFyIG1vZGVscyA9IGVjTW9kZWwucXVlcnlDb21wb25lbnRzKHF1ZXJ5UGFyYW0pO1xuICAgIHJlc3VsdFttYWluVHlwZSArICdNb2RlbHMnXSA9IG1vZGVscztcbiAgICByZXN1bHRbbWFpblR5cGUgKyAnTW9kZWwnXSA9IG1vZGVsc1swXTtcbiAgfSk7XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmZ1bmN0aW9uIGhhcyhvYmosIHByb3ApIHtcbiAgcmV0dXJuIG9iaiAmJiBvYmouaGFzT3duUHJvcGVydHkocHJvcCk7XG59XG5cbmZ1bmN0aW9uIHNldEF0dHJpYnV0ZShkb20sIGtleSwgdmFsdWUpIHtcbiAgZG9tLnNldEF0dHJpYnV0ZSA/IGRvbS5zZXRBdHRyaWJ1dGUoa2V5LCB2YWx1ZSkgOiBkb21ba2V5XSA9IHZhbHVlO1xufVxuXG5mdW5jdGlvbiBnZXRBdHRyaWJ1dGUoZG9tLCBrZXkpIHtcbiAgcmV0dXJuIGRvbS5nZXRBdHRyaWJ1dGUgPyBkb20uZ2V0QXR0cmlidXRlKGtleSkgOiBkb21ba2V5XTtcbn1cblxuZnVuY3Rpb24gZ2V0VG9vbHRpcFJlbmRlck1vZGUocmVuZGVyTW9kZU9wdGlvbikge1xuICBpZiAocmVuZGVyTW9kZU9wdGlvbiA9PT0gJ2F1dG8nKSB7XG4gICAgLy8gVXNpbmcgaHRtbCB3aGVuIGBkb2N1bWVudGAgZXhpc3RzLCB1c2UgcmljaFRleHQgb3RoZXJ3aXNlXG4gICAgcmV0dXJuIGVudi5kb21TdXBwb3J0ZWQgPyAnaHRtbCcgOiAncmljaFRleHQnO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiByZW5kZXJNb2RlT3B0aW9uIHx8ICdodG1sJztcbiAgfVxufVxuLyoqXG4gKiBHcm91cCBhIGxpc3QgYnkga2V5LlxuICpcbiAqIEBwYXJhbSB7QXJyYXl9IGFycmF5XG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBnZXRLZXlcbiAqICAgICAgICBwYXJhbSB7Kn0gQXJyYXkgaXRlbVxuICogICAgICAgIHJldHVybiB7c3RyaW5nfSBrZXlcbiAqIEByZXR1cm4ge09iamVjdH0gUmVzdWx0XG4gKiAgICAgICAge0FycmF5fToga2V5cyxcbiAqICAgICAgICB7bW9kdWxlOnpyZW5kZXIvY29yZS91dGlsL0hhc2hNYXB9IGJ1Y2tldHM6IHtrZXkgLT4gQXJyYXl9XG4gKi9cblxuXG5mdW5jdGlvbiBncm91cERhdGEoYXJyYXksIGdldEtleSkge1xuICB2YXIgYnVja2V0cyA9IHpyVXRpbC5jcmVhdGVIYXNoTWFwKCk7XG4gIHZhciBrZXlzID0gW107XG4gIHpyVXRpbC5lYWNoKGFycmF5LCBmdW5jdGlvbiAoaXRlbSkge1xuICAgIHZhciBrZXkgPSBnZXRLZXkoaXRlbSk7XG4gICAgKGJ1Y2tldHMuZ2V0KGtleSkgfHwgKGtleXMucHVzaChrZXkpLCBidWNrZXRzLnNldChrZXksIFtdKSkpLnB1c2goaXRlbSk7XG4gIH0pO1xuICByZXR1cm4ge1xuICAgIGtleXM6IGtleXMsXG4gICAgYnVja2V0czogYnVja2V0c1xuICB9O1xufVxuXG5leHBvcnRzLm5vcm1hbGl6ZVRvQXJyYXkgPSBub3JtYWxpemVUb0FycmF5O1xuZXhwb3J0cy5kZWZhdWx0RW1waGFzaXMgPSBkZWZhdWx0RW1waGFzaXM7XG5leHBvcnRzLlRFWFRfU1RZTEVfT1BUSU9OUyA9IFRFWFRfU1RZTEVfT1BUSU9OUztcbmV4cG9ydHMuZ2V0RGF0YUl0ZW1WYWx1ZSA9IGdldERhdGFJdGVtVmFsdWU7XG5leHBvcnRzLmlzRGF0YUl0ZW1PcHRpb24gPSBpc0RhdGFJdGVtT3B0aW9uO1xuZXhwb3J0cy5tYXBwaW5nVG9FeGlzdHMgPSBtYXBwaW5nVG9FeGlzdHM7XG5leHBvcnRzLm1ha2VJZEFuZE5hbWUgPSBtYWtlSWRBbmROYW1lO1xuZXhwb3J0cy5pc05hbWVTcGVjaWZpZWQgPSBpc05hbWVTcGVjaWZpZWQ7XG5leHBvcnRzLmlzSWRJbm5lciA9IGlzSWRJbm5lcjtcbmV4cG9ydHMuY29tcHJlc3NCYXRjaGVzID0gY29tcHJlc3NCYXRjaGVzO1xuZXhwb3J0cy5xdWVyeURhdGFJbmRleCA9IHF1ZXJ5RGF0YUluZGV4O1xuZXhwb3J0cy5tYWtlSW5uZXIgPSBtYWtlSW5uZXI7XG5leHBvcnRzLnBhcnNlRmluZGVyID0gcGFyc2VGaW5kZXI7XG5leHBvcnRzLnNldEF0dHJpYnV0ZSA9IHNldEF0dHJpYnV0ZTtcbmV4cG9ydHMuZ2V0QXR0cmlidXRlID0gZ2V0QXR0cmlidXRlO1xuZXhwb3J0cy5nZXRUb29sdGlwUmVuZGVyTW9kZSA9IGdldFRvb2x0aXBSZW5kZXJNb2RlO1xuZXhwb3J0cy5ncm91cERhdGEgPSBncm91cERhdGE7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/util/model.js
