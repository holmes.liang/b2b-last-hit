__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NSwitch", function() { return NSwitch; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/component/NSwitch.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n      .ant-form-item-control-wrapper {\n        width: auto;\n      }\n    "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}





var NSwitch =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(NSwitch, _ModelWidget);

  function NSwitch(props, context) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, NSwitch);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(NSwitch).call(this, props, context));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(NSwitch, [{
    key: "initComponents",
    value: function initComponents() {
      return {
        BoxDiv: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["Styled"].div(_templateObject())
      };
    }
  }, {
    key: "render",
    value: function render() {
      var C = this.getComponents();

      var _this$props = this.props,
          form = _this$props.form,
          model = _this$props.model,
          label = _this$props.label,
          propName = _this$props.propName,
          onChange = _this$props.onChange,
          rules = _this$props.rules,
          required = _this$props.required,
          layoutCol = _this$props.layoutCol,
          rest = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_this$props, ["form", "model", "label", "propName", "onChange", "rules", "required", "layoutCol"]);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.BoxDiv, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_component__WEBPACK_IMPORTED_MODULE_9__["NFormItem"], Object.assign({
        form: form,
        model: model,
        label: label,
        propName: propName,
        onChange: onChange,
        rules: rules,
        required: required,
        layoutCol: layoutCol
      }, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_8__["Switch"], Object.assign({}, rest, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }))));
    }
  }]);

  return NSwitch;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2NvbXBvbmVudC9OU3dpdGNoLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9jb21wb25lbnQvTlN3aXRjaC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QsIFN0eWxlZCB9IGZyb20gXCJAY29tbW9uLzNyZFwiO1xuaW1wb3J0IHsgU3dpdGNoIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IFN3aXRjaFByb3BzIH0gZnJvbSBcImFudGQvbGliL3N3aXRjaFwiO1xuaW1wb3J0ICogYXMgU3R5bGVkRnVuY3Rpb25zIGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiO1xuXG5pbXBvcnQgeyBNb2RlbFdpZGdldCwgTkZvcm1JdGVtIH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFBhZ2VDb21wb25lbnRzIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IHsgTkZvcm1JdGVtUHJvcHMgfSBmcm9tIFwiQGNvbXBvbmVudC9ORm9ybUl0ZW1cIjtcblxuZXhwb3J0IHR5cGUgTlN3aXRjaFByb3BzID0ge30gJiBORm9ybUl0ZW1Qcm9wcyAmIFN3aXRjaFByb3BzO1xuXG5leHBvcnQgdHlwZSBTdHlsZWREaXYgPSBTdHlsZWRGdW5jdGlvbnMuU3R5bGVkQ29tcG9uZW50PFwiZGl2XCIsIGFueSwgeyByZWFkT25seT86IGJvb2xlYW4gfSwgbmV2ZXI+O1xuXG5leHBvcnQgdHlwZSBOU3dpdGNoQ29tcG9uZW50cyA9IHtcbiAgQm94RGl2OiBTdHlsZWREaXZcbn0gJiBQYWdlQ29tcG9uZW50cztcblxuY2xhc3MgTlN3aXRjaDxQIGV4dGVuZHMgTlN3aXRjaFByb3BzLCBTLCBDIGV4dGVuZHMgTlN3aXRjaENvbXBvbmVudHM+IGV4dGVuZHMgTW9kZWxXaWRnZXQ8UCwgUywgQz4ge1xuICBjb25zdHJ1Y3Rvcihwcm9wczogTlN3aXRjaFByb3BzLCBjb250ZXh0PzogYW55KSB7XG4gICAgc3VwZXIocHJvcHMsIGNvbnRleHQpO1xuICB9XG5cbiAgcHJvdGVjdGVkIGluaXRDb21wb25lbnRzKCk6IEMge1xuICAgIHJldHVybiB7XG4gICAgICBCb3hEaXY6IFN0eWxlZC5kaXZgXG4gICAgICAuYW50LWZvcm0taXRlbS1jb250cm9sLXdyYXBwZXIge1xuICAgICAgICB3aWR0aDogYXV0bztcbiAgICAgIH1cbiAgICBgLFxuICAgIH0gYXMgQztcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBDID0gdGhpcy5nZXRDb21wb25lbnRzKCk7XG4gICAgY29uc3Qge1xuICAgICAgZm9ybSwgbW9kZWwsIGxhYmVsLCBwcm9wTmFtZSwgb25DaGFuZ2UsIHJ1bGVzLCByZXF1aXJlZCwgbGF5b3V0Q29sLCAuLi5yZXN0XG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLkJveERpdj5cbiAgICAgICAgPE5Gb3JtSXRlbSB7Li4ueyBmb3JtLCBtb2RlbCwgbGFiZWwsIHByb3BOYW1lLCBvbkNoYW5nZSwgcnVsZXMsIHJlcXVpcmVkLCBsYXlvdXRDb2wgfX0+XG4gICAgICAgICAgPFN3aXRjaCB7Li4ucmVzdH0vPlxuICAgICAgICA8L05Gb3JtSXRlbT5cbiAgICAgIDwvQy5Cb3hEaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgeyBOU3dpdGNoIH07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQVdBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFPQTs7O0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7Ozs7QUEzQkE7QUFDQTsiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/component/NSwitch.tsx
