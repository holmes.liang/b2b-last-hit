var camel2hyphen = __webpack_require__(/*! string-convert/camel2hyphen */ "./node_modules/string-convert/camel2hyphen.js");

var isDimension = function isDimension(feature) {
  var re = /[height|width]$/;
  return re.test(feature);
};

var obj2mq = function obj2mq(obj) {
  var mq = '';
  var features = Object.keys(obj);
  features.forEach(function (feature, index) {
    var value = obj[feature];
    feature = camel2hyphen(feature); // Add px to dimension features

    if (isDimension(feature) && typeof value === 'number') {
      value = value + 'px';
    }

    if (value === true) {
      mq += feature;
    } else if (value === false) {
      mq += 'not ' + feature;
    } else {
      mq += '(' + feature + ': ' + value + ')';
    }

    if (index < features.length - 1) {
      mq += ' and ';
    }
  });
  return mq;
};

var json2mq = function json2mq(query) {
  var mq = '';

  if (typeof query === 'string') {
    return query;
  } // Handling array of media queries


  if (query instanceof Array) {
    query.forEach(function (q, index) {
      mq += obj2mq(q);

      if (index < query.length - 1) {
        mq += ', ';
      }
    });
    return mq;
  } // Handling single media query


  return obj2mq(query);
};

module.exports = json2mq;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvanNvbjJtcS9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2pzb24ybXEvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIGNhbWVsMmh5cGhlbiA9IHJlcXVpcmUoJ3N0cmluZy1jb252ZXJ0L2NhbWVsMmh5cGhlbicpO1xuXG52YXIgaXNEaW1lbnNpb24gPSBmdW5jdGlvbiAoZmVhdHVyZSkge1xuICB2YXIgcmUgPSAvW2hlaWdodHx3aWR0aF0kLztcbiAgcmV0dXJuIHJlLnRlc3QoZmVhdHVyZSk7XG59O1xuXG52YXIgb2JqMm1xID0gZnVuY3Rpb24gKG9iaikge1xuICB2YXIgbXEgPSAnJztcbiAgdmFyIGZlYXR1cmVzID0gT2JqZWN0LmtleXMob2JqKTtcbiAgZmVhdHVyZXMuZm9yRWFjaChmdW5jdGlvbiAoZmVhdHVyZSwgaW5kZXgpIHtcbiAgICB2YXIgdmFsdWUgPSBvYmpbZmVhdHVyZV07XG4gICAgZmVhdHVyZSA9IGNhbWVsMmh5cGhlbihmZWF0dXJlKTtcbiAgICAvLyBBZGQgcHggdG8gZGltZW5zaW9uIGZlYXR1cmVzXG4gICAgaWYgKGlzRGltZW5zaW9uKGZlYXR1cmUpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcbiAgICAgIHZhbHVlID0gdmFsdWUgKyAncHgnO1xuICAgIH1cbiAgICBpZiAodmFsdWUgPT09IHRydWUpIHtcbiAgICAgIG1xICs9IGZlYXR1cmU7XG4gICAgfSBlbHNlIGlmICh2YWx1ZSA9PT0gZmFsc2UpIHtcbiAgICAgIG1xICs9ICdub3QgJyArIGZlYXR1cmU7XG4gICAgfSBlbHNlIHtcbiAgICAgIG1xICs9ICcoJyArIGZlYXR1cmUgKyAnOiAnICsgdmFsdWUgKyAnKSc7XG4gICAgfVxuICAgIGlmIChpbmRleCA8IGZlYXR1cmVzLmxlbmd0aC0xKSB7XG4gICAgICBtcSArPSAnIGFuZCAnXG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIG1xO1xufTtcblxudmFyIGpzb24ybXEgPSBmdW5jdGlvbiAocXVlcnkpIHtcbiAgdmFyIG1xID0gJyc7XG4gIGlmICh0eXBlb2YgcXVlcnkgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHF1ZXJ5O1xuICB9XG4gIC8vIEhhbmRsaW5nIGFycmF5IG9mIG1lZGlhIHF1ZXJpZXNcbiAgaWYgKHF1ZXJ5IGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICBxdWVyeS5mb3JFYWNoKGZ1bmN0aW9uIChxLCBpbmRleCkge1xuICAgICAgbXEgKz0gb2JqMm1xKHEpO1xuICAgICAgaWYgKGluZGV4IDwgcXVlcnkubGVuZ3RoLTEpIHtcbiAgICAgICAgbXEgKz0gJywgJ1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBtcTtcbiAgfVxuICAvLyBIYW5kbGluZyBzaW5nbGUgbWVkaWEgcXVlcnlcbiAgcmV0dXJuIG9iajJtcShxdWVyeSk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGpzb24ybXE7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/json2mq/index.js
