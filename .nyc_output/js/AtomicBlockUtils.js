/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule AtomicBlockUtils
 * @format
 * 
 */


var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var _extends = _assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var BlockMapBuilder = __webpack_require__(/*! ./BlockMapBuilder */ "./node_modules/draft-js/lib/BlockMapBuilder.js");

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var ContentBlock = __webpack_require__(/*! ./ContentBlock */ "./node_modules/draft-js/lib/ContentBlock.js");

var ContentBlockNode = __webpack_require__(/*! ./ContentBlockNode */ "./node_modules/draft-js/lib/ContentBlockNode.js");

var DraftFeatureFlags = __webpack_require__(/*! ./DraftFeatureFlags */ "./node_modules/draft-js/lib/DraftFeatureFlags.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var Immutable = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");

var SelectionState = __webpack_require__(/*! ./SelectionState */ "./node_modules/draft-js/lib/SelectionState.js");

var generateRandomKey = __webpack_require__(/*! ./generateRandomKey */ "./node_modules/draft-js/lib/generateRandomKey.js");

var moveBlockInContentState = __webpack_require__(/*! ./moveBlockInContentState */ "./node_modules/draft-js/lib/moveBlockInContentState.js");

var experimentalTreeDataSupport = DraftFeatureFlags.draft_tree_data_support;
var ContentBlockRecord = experimentalTreeDataSupport ? ContentBlockNode : ContentBlock;
var List = Immutable.List,
    Repeat = Immutable.Repeat;
var AtomicBlockUtils = {
  insertAtomicBlock: function insertAtomicBlock(editorState, entityKey, character) {
    var contentState = editorState.getCurrentContent();
    var selectionState = editorState.getSelection();
    var afterRemoval = DraftModifier.removeRange(contentState, selectionState, 'backward');
    var targetSelection = afterRemoval.getSelectionAfter();
    var afterSplit = DraftModifier.splitBlock(afterRemoval, targetSelection);
    var insertionTarget = afterSplit.getSelectionAfter();
    var asAtomicBlock = DraftModifier.setBlockType(afterSplit, insertionTarget, 'atomic');
    var charData = CharacterMetadata.create({
      entity: entityKey
    });
    var atomicBlockConfig = {
      key: generateRandomKey(),
      type: 'atomic',
      text: character,
      characterList: List(Repeat(charData, character.length))
    };
    var atomicDividerBlockConfig = {
      key: generateRandomKey(),
      type: 'unstyled'
    };

    if (experimentalTreeDataSupport) {
      atomicBlockConfig = _extends({}, atomicBlockConfig, {
        nextSibling: atomicDividerBlockConfig.key
      });
      atomicDividerBlockConfig = _extends({}, atomicDividerBlockConfig, {
        prevSibling: atomicBlockConfig.key
      });
    }

    var fragmentArray = [new ContentBlockRecord(atomicBlockConfig), new ContentBlockRecord(atomicDividerBlockConfig)];
    var fragment = BlockMapBuilder.createFromArray(fragmentArray);
    var withAtomicBlock = DraftModifier.replaceWithFragment(asAtomicBlock, insertionTarget, fragment);
    var newContent = withAtomicBlock.merge({
      selectionBefore: selectionState,
      selectionAfter: withAtomicBlock.getSelectionAfter().set('hasFocus', true)
    });
    return EditorState.push(editorState, newContent, 'insert-fragment');
  },
  moveAtomicBlock: function moveAtomicBlock(editorState, atomicBlock, targetRange, insertionMode) {
    var contentState = editorState.getCurrentContent();
    var selectionState = editorState.getSelection();
    var withMovedAtomicBlock = void 0;

    if (insertionMode === 'before' || insertionMode === 'after') {
      var targetBlock = contentState.getBlockForKey(insertionMode === 'before' ? targetRange.getStartKey() : targetRange.getEndKey());
      withMovedAtomicBlock = moveBlockInContentState(contentState, atomicBlock, targetBlock, insertionMode);
    } else {
      var afterRemoval = DraftModifier.removeRange(contentState, targetRange, 'backward');
      var selectionAfterRemoval = afterRemoval.getSelectionAfter();

      var _targetBlock = afterRemoval.getBlockForKey(selectionAfterRemoval.getFocusKey());

      if (selectionAfterRemoval.getStartOffset() === 0) {
        withMovedAtomicBlock = moveBlockInContentState(afterRemoval, atomicBlock, _targetBlock, 'before');
      } else if (selectionAfterRemoval.getEndOffset() === _targetBlock.getLength()) {
        withMovedAtomicBlock = moveBlockInContentState(afterRemoval, atomicBlock, _targetBlock, 'after');
      } else {
        var afterSplit = DraftModifier.splitBlock(afterRemoval, selectionAfterRemoval);
        var selectionAfterSplit = afterSplit.getSelectionAfter();

        var _targetBlock2 = afterSplit.getBlockForKey(selectionAfterSplit.getFocusKey());

        withMovedAtomicBlock = moveBlockInContentState(afterSplit, atomicBlock, _targetBlock2, 'before');
      }
    }

    var newContent = withMovedAtomicBlock.merge({
      selectionBefore: selectionState,
      selectionAfter: withMovedAtomicBlock.getSelectionAfter().set('hasFocus', true)
    });
    return EditorState.push(editorState, newContent, 'move-block');
  }
};
module.exports = AtomicBlockUtils;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0F0b21pY0Jsb2NrVXRpbHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvQXRvbWljQmxvY2tVdGlscy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIEF0b21pY0Jsb2NrVXRpbHNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIF9hc3NpZ24gPSByZXF1aXJlKCdvYmplY3QtYXNzaWduJyk7XG5cbnZhciBfZXh0ZW5kcyA9IF9hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBCbG9ja01hcEJ1aWxkZXIgPSByZXF1aXJlKCcuL0Jsb2NrTWFwQnVpbGRlcicpO1xudmFyIENoYXJhY3Rlck1ldGFkYXRhID0gcmVxdWlyZSgnLi9DaGFyYWN0ZXJNZXRhZGF0YScpO1xudmFyIENvbnRlbnRCbG9jayA9IHJlcXVpcmUoJy4vQ29udGVudEJsb2NrJyk7XG52YXIgQ29udGVudEJsb2NrTm9kZSA9IHJlcXVpcmUoJy4vQ29udGVudEJsb2NrTm9kZScpO1xudmFyIERyYWZ0RmVhdHVyZUZsYWdzID0gcmVxdWlyZSgnLi9EcmFmdEZlYXR1cmVGbGFncycpO1xudmFyIERyYWZ0TW9kaWZpZXIgPSByZXF1aXJlKCcuL0RyYWZ0TW9kaWZpZXInKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBJbW11dGFibGUgPSByZXF1aXJlKCdpbW11dGFibGUnKTtcbnZhciBTZWxlY3Rpb25TdGF0ZSA9IHJlcXVpcmUoJy4vU2VsZWN0aW9uU3RhdGUnKTtcblxudmFyIGdlbmVyYXRlUmFuZG9tS2V5ID0gcmVxdWlyZSgnLi9nZW5lcmF0ZVJhbmRvbUtleScpO1xudmFyIG1vdmVCbG9ja0luQ29udGVudFN0YXRlID0gcmVxdWlyZSgnLi9tb3ZlQmxvY2tJbkNvbnRlbnRTdGF0ZScpO1xuXG52YXIgZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ID0gRHJhZnRGZWF0dXJlRmxhZ3MuZHJhZnRfdHJlZV9kYXRhX3N1cHBvcnQ7XG52YXIgQ29udGVudEJsb2NrUmVjb3JkID0gZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0ID8gQ29udGVudEJsb2NrTm9kZSA6IENvbnRlbnRCbG9jaztcblxudmFyIExpc3QgPSBJbW11dGFibGUuTGlzdCxcbiAgICBSZXBlYXQgPSBJbW11dGFibGUuUmVwZWF0O1xuXG5cbnZhciBBdG9taWNCbG9ja1V0aWxzID0ge1xuICBpbnNlcnRBdG9taWNCbG9jazogZnVuY3Rpb24gaW5zZXJ0QXRvbWljQmxvY2soZWRpdG9yU3RhdGUsIGVudGl0eUtleSwgY2hhcmFjdGVyKSB7XG4gICAgdmFyIGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIHNlbGVjdGlvblN0YXRlID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgICB2YXIgYWZ0ZXJSZW1vdmFsID0gRHJhZnRNb2RpZmllci5yZW1vdmVSYW5nZShjb250ZW50U3RhdGUsIHNlbGVjdGlvblN0YXRlLCAnYmFja3dhcmQnKTtcblxuICAgIHZhciB0YXJnZXRTZWxlY3Rpb24gPSBhZnRlclJlbW92YWwuZ2V0U2VsZWN0aW9uQWZ0ZXIoKTtcbiAgICB2YXIgYWZ0ZXJTcGxpdCA9IERyYWZ0TW9kaWZpZXIuc3BsaXRCbG9jayhhZnRlclJlbW92YWwsIHRhcmdldFNlbGVjdGlvbik7XG4gICAgdmFyIGluc2VydGlvblRhcmdldCA9IGFmdGVyU3BsaXQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKTtcblxuICAgIHZhciBhc0F0b21pY0Jsb2NrID0gRHJhZnRNb2RpZmllci5zZXRCbG9ja1R5cGUoYWZ0ZXJTcGxpdCwgaW5zZXJ0aW9uVGFyZ2V0LCAnYXRvbWljJyk7XG5cbiAgICB2YXIgY2hhckRhdGEgPSBDaGFyYWN0ZXJNZXRhZGF0YS5jcmVhdGUoeyBlbnRpdHk6IGVudGl0eUtleSB9KTtcblxuICAgIHZhciBhdG9taWNCbG9ja0NvbmZpZyA9IHtcbiAgICAgIGtleTogZ2VuZXJhdGVSYW5kb21LZXkoKSxcbiAgICAgIHR5cGU6ICdhdG9taWMnLFxuICAgICAgdGV4dDogY2hhcmFjdGVyLFxuICAgICAgY2hhcmFjdGVyTGlzdDogTGlzdChSZXBlYXQoY2hhckRhdGEsIGNoYXJhY3Rlci5sZW5ndGgpKVxuICAgIH07XG5cbiAgICB2YXIgYXRvbWljRGl2aWRlckJsb2NrQ29uZmlnID0ge1xuICAgICAga2V5OiBnZW5lcmF0ZVJhbmRvbUtleSgpLFxuICAgICAgdHlwZTogJ3Vuc3R5bGVkJ1xuICAgIH07XG5cbiAgICBpZiAoZXhwZXJpbWVudGFsVHJlZURhdGFTdXBwb3J0KSB7XG4gICAgICBhdG9taWNCbG9ja0NvbmZpZyA9IF9leHRlbmRzKHt9LCBhdG9taWNCbG9ja0NvbmZpZywge1xuICAgICAgICBuZXh0U2libGluZzogYXRvbWljRGl2aWRlckJsb2NrQ29uZmlnLmtleVxuICAgICAgfSk7XG4gICAgICBhdG9taWNEaXZpZGVyQmxvY2tDb25maWcgPSBfZXh0ZW5kcyh7fSwgYXRvbWljRGl2aWRlckJsb2NrQ29uZmlnLCB7XG4gICAgICAgIHByZXZTaWJsaW5nOiBhdG9taWNCbG9ja0NvbmZpZy5rZXlcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHZhciBmcmFnbWVudEFycmF5ID0gW25ldyBDb250ZW50QmxvY2tSZWNvcmQoYXRvbWljQmxvY2tDb25maWcpLCBuZXcgQ29udGVudEJsb2NrUmVjb3JkKGF0b21pY0RpdmlkZXJCbG9ja0NvbmZpZyldO1xuXG4gICAgdmFyIGZyYWdtZW50ID0gQmxvY2tNYXBCdWlsZGVyLmNyZWF0ZUZyb21BcnJheShmcmFnbWVudEFycmF5KTtcblxuICAgIHZhciB3aXRoQXRvbWljQmxvY2sgPSBEcmFmdE1vZGlmaWVyLnJlcGxhY2VXaXRoRnJhZ21lbnQoYXNBdG9taWNCbG9jaywgaW5zZXJ0aW9uVGFyZ2V0LCBmcmFnbWVudCk7XG5cbiAgICB2YXIgbmV3Q29udGVudCA9IHdpdGhBdG9taWNCbG9jay5tZXJnZSh7XG4gICAgICBzZWxlY3Rpb25CZWZvcmU6IHNlbGVjdGlvblN0YXRlLFxuICAgICAgc2VsZWN0aW9uQWZ0ZXI6IHdpdGhBdG9taWNCbG9jay5nZXRTZWxlY3Rpb25BZnRlcigpLnNldCgnaGFzRm9jdXMnLCB0cnVlKVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIG5ld0NvbnRlbnQsICdpbnNlcnQtZnJhZ21lbnQnKTtcbiAgfSxcblxuICBtb3ZlQXRvbWljQmxvY2s6IGZ1bmN0aW9uIG1vdmVBdG9taWNCbG9jayhlZGl0b3JTdGF0ZSwgYXRvbWljQmxvY2ssIHRhcmdldFJhbmdlLCBpbnNlcnRpb25Nb2RlKSB7XG4gICAgdmFyIGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgdmFyIHNlbGVjdGlvblN0YXRlID0gZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCk7XG5cbiAgICB2YXIgd2l0aE1vdmVkQXRvbWljQmxvY2sgPSB2b2lkIDA7XG5cbiAgICBpZiAoaW5zZXJ0aW9uTW9kZSA9PT0gJ2JlZm9yZScgfHwgaW5zZXJ0aW9uTW9kZSA9PT0gJ2FmdGVyJykge1xuICAgICAgdmFyIHRhcmdldEJsb2NrID0gY29udGVudFN0YXRlLmdldEJsb2NrRm9yS2V5KGluc2VydGlvbk1vZGUgPT09ICdiZWZvcmUnID8gdGFyZ2V0UmFuZ2UuZ2V0U3RhcnRLZXkoKSA6IHRhcmdldFJhbmdlLmdldEVuZEtleSgpKTtcblxuICAgICAgd2l0aE1vdmVkQXRvbWljQmxvY2sgPSBtb3ZlQmxvY2tJbkNvbnRlbnRTdGF0ZShjb250ZW50U3RhdGUsIGF0b21pY0Jsb2NrLCB0YXJnZXRCbG9jaywgaW5zZXJ0aW9uTW9kZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBhZnRlclJlbW92YWwgPSBEcmFmdE1vZGlmaWVyLnJlbW92ZVJhbmdlKGNvbnRlbnRTdGF0ZSwgdGFyZ2V0UmFuZ2UsICdiYWNrd2FyZCcpO1xuXG4gICAgICB2YXIgc2VsZWN0aW9uQWZ0ZXJSZW1vdmFsID0gYWZ0ZXJSZW1vdmFsLmdldFNlbGVjdGlvbkFmdGVyKCk7XG4gICAgICB2YXIgX3RhcmdldEJsb2NrID0gYWZ0ZXJSZW1vdmFsLmdldEJsb2NrRm9yS2V5KHNlbGVjdGlvbkFmdGVyUmVtb3ZhbC5nZXRGb2N1c0tleSgpKTtcblxuICAgICAgaWYgKHNlbGVjdGlvbkFmdGVyUmVtb3ZhbC5nZXRTdGFydE9mZnNldCgpID09PSAwKSB7XG4gICAgICAgIHdpdGhNb3ZlZEF0b21pY0Jsb2NrID0gbW92ZUJsb2NrSW5Db250ZW50U3RhdGUoYWZ0ZXJSZW1vdmFsLCBhdG9taWNCbG9jaywgX3RhcmdldEJsb2NrLCAnYmVmb3JlJyk7XG4gICAgICB9IGVsc2UgaWYgKHNlbGVjdGlvbkFmdGVyUmVtb3ZhbC5nZXRFbmRPZmZzZXQoKSA9PT0gX3RhcmdldEJsb2NrLmdldExlbmd0aCgpKSB7XG4gICAgICAgIHdpdGhNb3ZlZEF0b21pY0Jsb2NrID0gbW92ZUJsb2NrSW5Db250ZW50U3RhdGUoYWZ0ZXJSZW1vdmFsLCBhdG9taWNCbG9jaywgX3RhcmdldEJsb2NrLCAnYWZ0ZXInKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBhZnRlclNwbGl0ID0gRHJhZnRNb2RpZmllci5zcGxpdEJsb2NrKGFmdGVyUmVtb3ZhbCwgc2VsZWN0aW9uQWZ0ZXJSZW1vdmFsKTtcblxuICAgICAgICB2YXIgc2VsZWN0aW9uQWZ0ZXJTcGxpdCA9IGFmdGVyU3BsaXQuZ2V0U2VsZWN0aW9uQWZ0ZXIoKTtcbiAgICAgICAgdmFyIF90YXJnZXRCbG9jazIgPSBhZnRlclNwbGl0LmdldEJsb2NrRm9yS2V5KHNlbGVjdGlvbkFmdGVyU3BsaXQuZ2V0Rm9jdXNLZXkoKSk7XG5cbiAgICAgICAgd2l0aE1vdmVkQXRvbWljQmxvY2sgPSBtb3ZlQmxvY2tJbkNvbnRlbnRTdGF0ZShhZnRlclNwbGl0LCBhdG9taWNCbG9jaywgX3RhcmdldEJsb2NrMiwgJ2JlZm9yZScpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBuZXdDb250ZW50ID0gd2l0aE1vdmVkQXRvbWljQmxvY2subWVyZ2Uoe1xuICAgICAgc2VsZWN0aW9uQmVmb3JlOiBzZWxlY3Rpb25TdGF0ZSxcbiAgICAgIHNlbGVjdGlvbkFmdGVyOiB3aXRoTW92ZWRBdG9taWNCbG9jay5nZXRTZWxlY3Rpb25BZnRlcigpLnNldCgnaGFzRm9jdXMnLCB0cnVlKVxuICAgIH0pO1xuXG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLnB1c2goZWRpdG9yU3RhdGUsIG5ld0NvbnRlbnQsICdtb3ZlLWJsb2NrJyk7XG4gIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQXRvbWljQmxvY2tVdGlsczsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBdEZBO0FBeUZBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/AtomicBlockUtils.js
