(function (factory) {
   true ? module['exports'] = factory() : undefined;
})(function () {
  'use strict';

  return function (insertRule) {
    var delimiter = '/*|*/';
    var needle = delimiter + '}';

    function toSheet(block) {
      if (block) try {
        insertRule(block + '}');
      } catch (e) {}
    }

    return function ruleSheet(context, content, selectors, parents, line, column, length, ns, depth, at) {
      switch (context) {
        // property
        case 1:
          // @import
          if (depth === 0 && content.charCodeAt(0) === 64) return insertRule(content + ';'), '';
          break;
        // selector

        case 2:
          if (ns === 0) return content + delimiter;
          break;
        // at-rule

        case 3:
          switch (ns) {
            // @font-face, @page
            case 102:
            case 112:
              return insertRule(selectors[0] + content), '';

            default:
              return content + (at === 0 ? delimiter : '');
          }

        case -2:
          content.split(needle).forEach(toSheet);
      }
    };
  };
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvc3R5bGlzLXJ1bGUtc2hlZXQvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9zdHlsaXMtcnVsZS1zaGVldC9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKGZhY3RvcnkpIHtcblx0dHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnID8gKG1vZHVsZVsnZXhwb3J0cyddID0gZmFjdG9yeSgpKSA6XG5cdFx0dHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmVbJ2FtZCddID8gZGVmaW5lKGZhY3RvcnkoKSkgOlxuXHRcdFx0KHdpbmRvd1snc3R5bGlzUnVsZVNoZWV0J10gPSBmYWN0b3J5KCkpXG59KGZ1bmN0aW9uICgpIHtcblxuXHQndXNlIHN0cmljdCdcblxuXHRyZXR1cm4gZnVuY3Rpb24gKGluc2VydFJ1bGUpIHtcblx0XHR2YXIgZGVsaW1pdGVyID0gJy8qfCovJ1xuXHRcdHZhciBuZWVkbGUgPSBkZWxpbWl0ZXIrJ30nXG5cblx0XHRmdW5jdGlvbiB0b1NoZWV0IChibG9jaykge1xuXHRcdFx0aWYgKGJsb2NrKVxuXHRcdFx0XHR0cnkge1xuXHRcdFx0XHRcdGluc2VydFJ1bGUoYmxvY2sgKyAnfScpXG5cdFx0XHRcdH0gY2F0Y2ggKGUpIHt9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uIHJ1bGVTaGVldCAoY29udGV4dCwgY29udGVudCwgc2VsZWN0b3JzLCBwYXJlbnRzLCBsaW5lLCBjb2x1bW4sIGxlbmd0aCwgbnMsIGRlcHRoLCBhdCkge1xuXHRcdFx0c3dpdGNoIChjb250ZXh0KSB7XG5cdFx0XHRcdC8vIHByb3BlcnR5XG5cdFx0XHRcdGNhc2UgMTpcblx0XHRcdFx0XHQvLyBAaW1wb3J0XG5cdFx0XHRcdFx0aWYgKGRlcHRoID09PSAwICYmIGNvbnRlbnQuY2hhckNvZGVBdCgwKSA9PT0gNjQpXG5cdFx0XHRcdFx0XHRyZXR1cm4gaW5zZXJ0UnVsZShjb250ZW50Kyc7JyksICcnXG5cdFx0XHRcdFx0YnJlYWtcblx0XHRcdFx0Ly8gc2VsZWN0b3Jcblx0XHRcdFx0Y2FzZSAyOlxuXHRcdFx0XHRcdGlmIChucyA9PT0gMClcblx0XHRcdFx0XHRcdHJldHVybiBjb250ZW50ICsgZGVsaW1pdGVyXG5cdFx0XHRcdFx0YnJlYWtcblx0XHRcdFx0Ly8gYXQtcnVsZVxuXHRcdFx0XHRjYXNlIDM6XG5cdFx0XHRcdFx0c3dpdGNoIChucykge1xuXHRcdFx0XHRcdFx0Ly8gQGZvbnQtZmFjZSwgQHBhZ2Vcblx0XHRcdFx0XHRcdGNhc2UgMTAyOlxuXHRcdFx0XHRcdFx0Y2FzZSAxMTI6XG5cdFx0XHRcdFx0XHRcdHJldHVybiBpbnNlcnRSdWxlKHNlbGVjdG9yc1swXStjb250ZW50KSwgJydcblx0XHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRcdHJldHVybiBjb250ZW50ICsgKGF0ID09PSAwID8gZGVsaW1pdGVyIDogJycpXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRjYXNlIC0yOlxuXHRcdFx0XHRcdGNvbnRlbnQuc3BsaXQobmVlZGxlKS5mb3JFYWNoKHRvU2hlZXQpXG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59KSlcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQU5BO0FBQ0E7QUFPQTtBQUNBO0FBdkJBO0FBeUJBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/stylis-rule-sheet/index.js
