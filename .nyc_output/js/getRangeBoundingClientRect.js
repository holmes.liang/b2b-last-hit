/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getRangeBoundingClientRect
 * @format
 * 
 */


var getRangeClientRects = __webpack_require__(/*! ./getRangeClientRects */ "./node_modules/draft-js/lib/getRangeClientRects.js");
/**
 * Like range.getBoundingClientRect() but normalizes for browser bugs.
 */


function getRangeBoundingClientRect(range) {
  // "Return a DOMRect object describing the smallest rectangle that includes
  // the first rectangle in list and all of the remaining rectangles of which
  // the height or width is not zero."
  // http://www.w3.org/TR/cssom-view/#dom-range-getboundingclientrect
  var rects = getRangeClientRects(range);
  var top = 0;
  var right = 0;
  var bottom = 0;
  var left = 0;

  if (rects.length) {
    // If the first rectangle has 0 width, we use the second, this is needed
    // because Chrome renders a 0 width rectangle when the selection contains
    // a line break.
    if (rects.length > 1 && rects[0].width === 0) {
      var _rects$ = rects[1];
      top = _rects$.top;
      right = _rects$.right;
      bottom = _rects$.bottom;
      left = _rects$.left;
    } else {
      var _rects$2 = rects[0];
      top = _rects$2.top;
      right = _rects$2.right;
      bottom = _rects$2.bottom;
      left = _rects$2.left;
    }

    for (var ii = 1; ii < rects.length; ii++) {
      var rect = rects[ii];

      if (rect.height !== 0 && rect.width !== 0) {
        top = Math.min(top, rect.top);
        right = Math.max(right, rect.right);
        bottom = Math.max(bottom, rect.bottom);
        left = Math.min(left, rect.left);
      }
    }
  }

  return {
    top: top,
    right: right,
    bottom: bottom,
    left: left,
    width: right - left,
    height: bottom - top
  };
}

module.exports = getRangeBoundingClientRect;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFJhbmdlQm91bmRpbmdDbGllbnRSZWN0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFJhbmdlQm91bmRpbmdDbGllbnRSZWN0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0UmFuZ2VCb3VuZGluZ0NsaWVudFJlY3RcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGdldFJhbmdlQ2xpZW50UmVjdHMgPSByZXF1aXJlKCcuL2dldFJhbmdlQ2xpZW50UmVjdHMnKTtcblxuLyoqXG4gKiBMaWtlIHJhbmdlLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpIGJ1dCBub3JtYWxpemVzIGZvciBicm93c2VyIGJ1Z3MuXG4gKi9cbmZ1bmN0aW9uIGdldFJhbmdlQm91bmRpbmdDbGllbnRSZWN0KHJhbmdlKSB7XG4gIC8vIFwiUmV0dXJuIGEgRE9NUmVjdCBvYmplY3QgZGVzY3JpYmluZyB0aGUgc21hbGxlc3QgcmVjdGFuZ2xlIHRoYXQgaW5jbHVkZXNcbiAgLy8gdGhlIGZpcnN0IHJlY3RhbmdsZSBpbiBsaXN0IGFuZCBhbGwgb2YgdGhlIHJlbWFpbmluZyByZWN0YW5nbGVzIG9mIHdoaWNoXG4gIC8vIHRoZSBoZWlnaHQgb3Igd2lkdGggaXMgbm90IHplcm8uXCJcbiAgLy8gaHR0cDovL3d3dy53My5vcmcvVFIvY3Nzb20tdmlldy8jZG9tLXJhbmdlLWdldGJvdW5kaW5nY2xpZW50cmVjdFxuICB2YXIgcmVjdHMgPSBnZXRSYW5nZUNsaWVudFJlY3RzKHJhbmdlKTtcbiAgdmFyIHRvcCA9IDA7XG4gIHZhciByaWdodCA9IDA7XG4gIHZhciBib3R0b20gPSAwO1xuICB2YXIgbGVmdCA9IDA7XG5cbiAgaWYgKHJlY3RzLmxlbmd0aCkge1xuICAgIC8vIElmIHRoZSBmaXJzdCByZWN0YW5nbGUgaGFzIDAgd2lkdGgsIHdlIHVzZSB0aGUgc2Vjb25kLCB0aGlzIGlzIG5lZWRlZFxuICAgIC8vIGJlY2F1c2UgQ2hyb21lIHJlbmRlcnMgYSAwIHdpZHRoIHJlY3RhbmdsZSB3aGVuIHRoZSBzZWxlY3Rpb24gY29udGFpbnNcbiAgICAvLyBhIGxpbmUgYnJlYWsuXG4gICAgaWYgKHJlY3RzLmxlbmd0aCA+IDEgJiYgcmVjdHNbMF0ud2lkdGggPT09IDApIHtcbiAgICAgIHZhciBfcmVjdHMkID0gcmVjdHNbMV07XG4gICAgICB0b3AgPSBfcmVjdHMkLnRvcDtcbiAgICAgIHJpZ2h0ID0gX3JlY3RzJC5yaWdodDtcbiAgICAgIGJvdHRvbSA9IF9yZWN0cyQuYm90dG9tO1xuICAgICAgbGVmdCA9IF9yZWN0cyQubGVmdDtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIF9yZWN0cyQyID0gcmVjdHNbMF07XG4gICAgICB0b3AgPSBfcmVjdHMkMi50b3A7XG4gICAgICByaWdodCA9IF9yZWN0cyQyLnJpZ2h0O1xuICAgICAgYm90dG9tID0gX3JlY3RzJDIuYm90dG9tO1xuICAgICAgbGVmdCA9IF9yZWN0cyQyLmxlZnQ7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgaWkgPSAxOyBpaSA8IHJlY3RzLmxlbmd0aDsgaWkrKykge1xuICAgICAgdmFyIHJlY3QgPSByZWN0c1tpaV07XG4gICAgICBpZiAocmVjdC5oZWlnaHQgIT09IDAgJiYgcmVjdC53aWR0aCAhPT0gMCkge1xuICAgICAgICB0b3AgPSBNYXRoLm1pbih0b3AsIHJlY3QudG9wKTtcbiAgICAgICAgcmlnaHQgPSBNYXRoLm1heChyaWdodCwgcmVjdC5yaWdodCk7XG4gICAgICAgIGJvdHRvbSA9IE1hdGgubWF4KGJvdHRvbSwgcmVjdC5ib3R0b20pO1xuICAgICAgICBsZWZ0ID0gTWF0aC5taW4obGVmdCwgcmVjdC5sZWZ0KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4ge1xuICAgIHRvcDogdG9wLFxuICAgIHJpZ2h0OiByaWdodCxcbiAgICBib3R0b206IGJvdHRvbSxcbiAgICBsZWZ0OiBsZWZ0LFxuICAgIHdpZHRoOiByaWdodCAtIGxlZnQsXG4gICAgaGVpZ2h0OiBib3R0b20gLSB0b3BcbiAgfTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRSYW5nZUJvdW5kaW5nQ2xpZW50UmVjdDsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getRangeBoundingClientRect.js
