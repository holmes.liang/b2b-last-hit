__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var rc_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-table */ "./node_modules/rc-table/es/index.js");
/* harmony import */ var rc_table__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rc_table__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _filterDropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./filterDropdown */ "./node_modules/antd/es/table/filterDropdown.js");
/* harmony import */ var _createStore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./createStore */ "./node_modules/antd/es/table/createStore.js");
/* harmony import */ var _SelectionBox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./SelectionBox */ "./node_modules/antd/es/table/SelectionBox.js");
/* harmony import */ var _SelectionCheckboxAll__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./SelectionCheckboxAll */ "./node_modules/antd/es/table/SelectionCheckboxAll.js");
/* harmony import */ var _Column__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Column */ "./node_modules/antd/es/table/Column.js");
/* harmony import */ var _ColumnGroup__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ColumnGroup */ "./node_modules/antd/es/table/ColumnGroup.js");
/* harmony import */ var _createBodyRow__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./createBodyRow */ "./node_modules/antd/es/table/createBodyRow.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./util */ "./node_modules/antd/es/table/util.js");
/* harmony import */ var _util_scrollTo__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../_util/scrollTo */ "./node_modules/antd/es/_util/scrollTo.js");
/* harmony import */ var _pagination__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../pagination */ "./node_modules/antd/es/pagination/index.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
/* harmony import */ var _spin__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../spin */ "./node_modules/antd/es/spin/index.js");
/* harmony import */ var _util_transButton__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../_util/transButton */ "./node_modules/antd/es/_util/transButton.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
/* harmony import */ var _locale_default__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../locale/default */ "./node_modules/antd/es/locale/default.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _util_warning__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../_util/warning */ "./node_modules/antd/es/_util/warning.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};
/* eslint-disable prefer-spread */



























function noop() {}

function stopPropagation(e) {
  e.stopPropagation();
}

function getRowSelection(props) {
  return props.rowSelection || {};
}

function getColumnKey(column, index) {
  return column.key || column.dataIndex || index;
}

function isSameColumn(a, b) {
  if (a && b && a.key && a.key === b.key) {
    return true;
  }

  return a === b || shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(a, b, function (value, other) {
    // https://github.com/ant-design/ant-design/issues/12737
    if (typeof value === 'function' && typeof other === 'function') {
      return value === other || value.toString() === other.toString();
    } // https://github.com/ant-design/ant-design/issues/19398


    if (Array.isArray(value) && Array.isArray(other)) {
      return value === other || shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(value, other);
    }
  });
}

var defaultPagination = {
  onChange: noop,
  onShowSizeChange: noop
};
/**
 * Avoid creating new object, so that parent component's shouldComponentUpdate
 * can works appropriately。
 */

var emptyObject = {};

var createComponents = function createComponents() {
  var components = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var bodyRow = components && components.body && components.body.row;
  return _extends(_extends({}, components), {
    body: _extends(_extends({}, components.body), {
      row: Object(_createBodyRow__WEBPACK_IMPORTED_MODULE_13__["default"])(bodyRow)
    })
  });
};

function isTheSameComponents() {
  var components1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var components2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return components1 === components2 || ['table', 'header', 'body'].every(function (key) {
    return shallowequal__WEBPACK_IMPORTED_MODULE_5___default()(components1[key], components2[key]);
  });
}

function getFilteredValueColumns(state, columns) {
  return Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatFilter"])(columns || (state || {}).columns || [], function (column) {
    return typeof column.filteredValue !== 'undefined';
  });
}

function getFiltersFromColumns() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var columns = arguments.length > 1 ? arguments[1] : undefined;
  var filters = {};
  getFilteredValueColumns(state, columns).forEach(function (col) {
    var colKey = getColumnKey(col);
    filters[colKey] = col.filteredValue;
  });
  return filters;
}

function isFiltersChanged(state, filters) {
  if (Object.keys(filters).length !== Object.keys(state.filters).length) {
    return true;
  }

  return Object.keys(filters).some(function (columnKey) {
    return filters[columnKey] !== state.filters[columnKey];
  });
}

var Table =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Table, _React$Component);

  function Table(props) {
    var _this;

    _classCallCheck(this, Table);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Table).call(this, props));

    _this.setTableRef = function (table) {
      _this.rcTable = table;
    };

    _this.getCheckboxPropsByItem = function (item, index) {
      var rowSelection = getRowSelection(_this.props);

      if (!rowSelection.getCheckboxProps) {
        return {};
      }

      var key = _this.getRecordKey(item, index); // Cache checkboxProps


      if (!_this.props.checkboxPropsCache[key]) {
        _this.props.checkboxPropsCache[key] = rowSelection.getCheckboxProps(item) || {};
        var checkboxProps = _this.props.checkboxPropsCache[key];
        Object(_util_warning__WEBPACK_IMPORTED_MODULE_23__["default"])(!('checked' in checkboxProps) && !('defaultChecked' in checkboxProps), 'Table', 'Do not set `checked` or `defaultChecked` in `getCheckboxProps`. Please use `selectedRowKeys` instead.');
      }

      return _this.props.checkboxPropsCache[key];
    };

    _this.getRecordKey = function (record, index) {
      var rowKey = _this.props.rowKey;
      var recordKey = typeof rowKey === 'function' ? rowKey(record, index) : record[rowKey];
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_23__["default"])(recordKey !== undefined, 'Table', 'Each record in dataSource of table should have a unique `key` prop, ' + 'or set `rowKey` of Table to an unique primary key, ' + 'see https://u.ant.design/table-row-key');
      return recordKey === undefined ? index : recordKey;
    };

    _this.onRow = function (prefixCls, record, index) {
      var onRow = _this.props.onRow;
      var custom = onRow ? onRow(record, index) : {};
      return _extends(_extends({}, custom), {
        prefixCls: prefixCls,
        store: _this.props.store,
        rowKey: _this.getRecordKey(record, index)
      });
    };

    _this.generatePopupContainerFunc = function (getPopupContainer) {
      var scroll = _this.props.scroll;
      var table = _this.rcTable;

      if (getPopupContainer) {
        return getPopupContainer;
      } // Use undefined to let rc component use default logic.


      return scroll && table ? function () {
        return table.tableNode;
      } : undefined;
    };

    _this.scrollToFirstRow = function () {
      var scroll = _this.props.scroll;

      if (scroll && scroll.scrollToFirstRowOnChange !== false) {
        Object(_util_scrollTo__WEBPACK_IMPORTED_MODULE_15__["default"])(0, {
          getContainer: function getContainer() {
            return _this.rcTable.bodyTable;
          }
        });
      }
    };

    _this.handleFilter = function (column, nextFilters) {
      var props = _this.props;

      var pagination = _extends({}, _this.state.pagination);

      var filters = _extends(_extends({}, _this.state.filters), _defineProperty({}, getColumnKey(column), nextFilters)); // Remove filters not in current columns


      var currentColumnKeys = [];
      Object(_util__WEBPACK_IMPORTED_MODULE_14__["treeMap"])(_this.state.columns, function (c) {
        if (!c.children) {
          currentColumnKeys.push(getColumnKey(c));
        }
      });
      Object.keys(filters).forEach(function (columnKey) {
        if (currentColumnKeys.indexOf(columnKey) < 0) {
          delete filters[columnKey];
        }
      });

      if (props.pagination) {
        // Reset current prop
        pagination.current = 1;
        pagination.onChange(pagination.current);
      }

      var newState = {
        pagination: pagination,
        filters: {}
      };

      var filtersToSetState = _extends({}, filters); // Remove filters which is controlled


      getFilteredValueColumns(_this.state).forEach(function (col) {
        var columnKey = getColumnKey(col);

        if (columnKey) {
          delete filtersToSetState[columnKey];
        }
      });

      if (Object.keys(filtersToSetState).length > 0) {
        newState.filters = filtersToSetState;
      } // Controlled current prop will not respond user interaction


      if (_typeof(props.pagination) === 'object' && 'current' in props.pagination) {
        newState.pagination = _extends(_extends({}, pagination), {
          current: _this.state.pagination.current
        });
      }

      _this.setState(newState, function () {
        _this.scrollToFirstRow();

        _this.props.store.setState({
          selectionDirty: false
        });

        var onChange = _this.props.onChange;

        if (onChange) {
          onChange.apply(null, _this.prepareParamsArguments(_extends(_extends({}, _this.state), {
            selectionDirty: false,
            filters: filters,
            pagination: pagination
          })));
        }
      });
    };

    _this.handleSelect = function (record, rowIndex, e) {
      var checked = e.target.checked;
      var nativeEvent = e.nativeEvent;
      var defaultSelection = _this.props.store.getState().selectionDirty ? [] : _this.getDefaultSelection();

      var selectedRowKeys = _this.props.store.getState().selectedRowKeys.concat(defaultSelection);

      var key = _this.getRecordKey(record, rowIndex);

      var pivot = _this.state.pivot;

      var rows = _this.getFlatCurrentPageData();

      var realIndex = rowIndex;

      if (_this.props.expandedRowRender) {
        realIndex = rows.findIndex(function (row) {
          return _this.getRecordKey(row, rowIndex) === key;
        });
      }

      if (nativeEvent.shiftKey && pivot !== undefined && realIndex !== pivot) {
        var changeRowKeys = [];
        var direction = Math.sign(pivot - realIndex);
        var dist = Math.abs(pivot - realIndex);
        var step = 0;

        var _loop = function _loop() {
          var i = realIndex + step * direction;
          step += 1;
          var row = rows[i];

          var rowKey = _this.getRecordKey(row, i);

          var checkboxProps = _this.getCheckboxPropsByItem(row, i);

          if (!checkboxProps.disabled) {
            if (selectedRowKeys.includes(rowKey)) {
              if (!checked) {
                selectedRowKeys = selectedRowKeys.filter(function (j) {
                  return rowKey !== j;
                });
                changeRowKeys.push(rowKey);
              }
            } else if (checked) {
              selectedRowKeys.push(rowKey);
              changeRowKeys.push(rowKey);
            }
          }
        };

        while (step <= dist) {
          _loop();
        }

        _this.setState({
          pivot: realIndex
        });

        _this.props.store.setState({
          selectionDirty: true
        });

        _this.setSelectedRowKeys(selectedRowKeys, {
          selectWay: 'onSelectMultiple',
          record: record,
          checked: checked,
          changeRowKeys: changeRowKeys,
          nativeEvent: nativeEvent
        });
      } else {
        if (checked) {
          selectedRowKeys.push(_this.getRecordKey(record, realIndex));
        } else {
          selectedRowKeys = selectedRowKeys.filter(function (i) {
            return key !== i;
          });
        }

        _this.setState({
          pivot: realIndex
        });

        _this.props.store.setState({
          selectionDirty: true
        });

        _this.setSelectedRowKeys(selectedRowKeys, {
          selectWay: 'onSelect',
          record: record,
          checked: checked,
          changeRowKeys: undefined,
          nativeEvent: nativeEvent
        });
      }
    };

    _this.handleRadioSelect = function (record, rowIndex, e) {
      var checked = e.target.checked;
      var nativeEvent = e.nativeEvent;

      var key = _this.getRecordKey(record, rowIndex);

      var selectedRowKeys = [key];

      _this.props.store.setState({
        selectionDirty: true
      });

      _this.setSelectedRowKeys(selectedRowKeys, {
        selectWay: 'onSelect',
        record: record,
        checked: checked,
        changeRowKeys: undefined,
        nativeEvent: nativeEvent
      });
    };

    _this.handleSelectRow = function (selectionKey, index, onSelectFunc) {
      var data = _this.getFlatCurrentPageData();

      var defaultSelection = _this.props.store.getState().selectionDirty ? [] : _this.getDefaultSelection();

      var selectedRowKeys = _this.props.store.getState().selectedRowKeys.concat(defaultSelection);

      var changeableRowKeys = data.filter(function (item, i) {
        return !_this.getCheckboxPropsByItem(item, i).disabled;
      }).map(function (item, i) {
        return _this.getRecordKey(item, i);
      });
      var changeRowKeys = [];
      var selectWay = 'onSelectAll';
      var checked; // handle default selection

      switch (selectionKey) {
        case 'all':
          changeableRowKeys.forEach(function (key) {
            if (selectedRowKeys.indexOf(key) < 0) {
              selectedRowKeys.push(key);
              changeRowKeys.push(key);
            }
          });
          selectWay = 'onSelectAll';
          checked = true;
          break;

        case 'removeAll':
          changeableRowKeys.forEach(function (key) {
            if (selectedRowKeys.indexOf(key) >= 0) {
              selectedRowKeys.splice(selectedRowKeys.indexOf(key), 1);
              changeRowKeys.push(key);
            }
          });
          selectWay = 'onSelectAll';
          checked = false;
          break;

        case 'invert':
          changeableRowKeys.forEach(function (key) {
            if (selectedRowKeys.indexOf(key) < 0) {
              selectedRowKeys.push(key);
            } else {
              selectedRowKeys.splice(selectedRowKeys.indexOf(key), 1);
            }

            changeRowKeys.push(key);
            selectWay = 'onSelectInvert';
          });
          break;

        default:
          break;
      }

      _this.props.store.setState({
        selectionDirty: true
      }); // when select custom selection, callback selections[n].onSelect


      var rowSelection = _this.props.rowSelection;
      var customSelectionStartIndex = 2;

      if (rowSelection && rowSelection.hideDefaultSelections) {
        customSelectionStartIndex = 0;
      }

      if (index >= customSelectionStartIndex && typeof onSelectFunc === 'function') {
        return onSelectFunc(changeableRowKeys);
      }

      _this.setSelectedRowKeys(selectedRowKeys, {
        selectWay: selectWay,
        checked: checked,
        changeRowKeys: changeRowKeys
      });
    };

    _this.handlePageChange = function (current) {
      var props = _this.props;

      var pagination = _extends({}, _this.state.pagination);

      if (current) {
        pagination.current = current;
      } else {
        pagination.current = pagination.current || 1;
      }

      for (var _len = arguments.length, otherArguments = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        otherArguments[_key - 1] = arguments[_key];
      }

      pagination.onChange.apply(pagination, [pagination.current].concat(otherArguments));
      var newState = {
        pagination: pagination
      }; // Controlled current prop will not respond user interaction

      if (props.pagination && _typeof(props.pagination) === 'object' && 'current' in props.pagination) {
        newState.pagination = _extends(_extends({}, pagination), {
          current: _this.state.pagination.current
        });
      }

      _this.setState(newState, _this.scrollToFirstRow);

      _this.props.store.setState({
        selectionDirty: false
      });

      var onChange = _this.props.onChange;

      if (onChange) {
        onChange.apply(null, _this.prepareParamsArguments(_extends(_extends({}, _this.state), {
          selectionDirty: false,
          pagination: pagination
        })));
      }
    };

    _this.handleShowSizeChange = function (current, pageSize) {
      var pagination = _this.state.pagination;
      pagination.onShowSizeChange(current, pageSize);

      var nextPagination = _extends(_extends({}, pagination), {
        pageSize: pageSize,
        current: current
      });

      _this.setState({
        pagination: nextPagination
      }, _this.scrollToFirstRow);

      var onChange = _this.props.onChange;

      if (onChange) {
        onChange.apply(null, _this.prepareParamsArguments(_extends(_extends({}, _this.state), {
          pagination: nextPagination
        })));
      }
    };

    _this.renderExpandIcon = function (prefixCls) {
      return function (_ref) {
        var expandable = _ref.expandable,
            expanded = _ref.expanded,
            needIndentSpaced = _ref.needIndentSpaced,
            record = _ref.record,
            onExpand = _ref.onExpand;

        if (expandable) {
          return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_20__["default"], {
            componentName: "Table",
            defaultLocale: _locale_default__WEBPACK_IMPORTED_MODULE_21__["default"].Table
          }, function (locale) {
            var _classNames;

            return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_util_transButton__WEBPACK_IMPORTED_MODULE_19__["default"], {
              className: classnames__WEBPACK_IMPORTED_MODULE_4___default()("".concat(prefixCls, "-row-expand-icon"), (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-row-collapsed"), !expanded), _defineProperty(_classNames, "".concat(prefixCls, "-row-expanded"), expanded), _classNames)),
              onClick: function onClick(event) {
                onExpand(record, event);
              },
              "aria-label": expanded ? locale.collapse : locale.expand,
              noStyle: true
            });
          });
        }

        if (needIndentSpaced) {
          return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            className: "".concat(prefixCls, "-row-expand-icon ").concat(prefixCls, "-row-spaced")
          });
        }

        return null;
      };
    };

    _this.renderSelectionBox = function (type) {
      return function (_, record, index) {
        var rowKey = _this.getRecordKey(record, index);

        var props = _this.getCheckboxPropsByItem(record, index);

        var handleChange = function handleChange(e) {
          return type === 'radio' ? _this.handleRadioSelect(record, index, e) : _this.handleSelect(record, index, e);
        };

        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          onClick: stopPropagation
        }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_SelectionBox__WEBPACK_IMPORTED_MODULE_9__["default"], _extends({
          type: type,
          store: _this.props.store,
          rowIndex: rowKey,
          onChange: handleChange,
          defaultSelection: _this.getDefaultSelection()
        }, props)));
      };
    };

    _this.renderTable = function (_ref2) {
      var _classNames2;

      var prefixCls = _ref2.prefixCls,
          renderEmpty = _ref2.renderEmpty,
          dropdownPrefixCls = _ref2.dropdownPrefixCls,
          contextLocale = _ref2.contextLocale,
          contextGetPopupContainer = _ref2.getPopupContainer;

      var _a = _this.props,
          showHeader = _a.showHeader,
          locale = _a.locale,
          getPopupContainer = _a.getPopupContainer,
          restTableProps = __rest(_a, ["showHeader", "locale", "getPopupContainer"]); // do not pass prop.style to rc-table, since already apply it to container div


      var restProps = Object(omit_js__WEBPACK_IMPORTED_MODULE_1__["default"])(restTableProps, ['style']);

      var data = _this.getCurrentPageData();

      var expandIconAsCell = _this.props.expandedRowRender && _this.props.expandIconAsCell !== false; // use props.getPopupContainer first

      var realGetPopupContainer = getPopupContainer || contextGetPopupContainer; // Merge too locales

      var mergedLocale = _extends(_extends({}, contextLocale), locale);

      if (!locale || !locale.emptyText) {
        mergedLocale.emptyText = renderEmpty('Table');
      }

      var classString = classnames__WEBPACK_IMPORTED_MODULE_4___default()("".concat(prefixCls, "-").concat(_this.props.size), (_classNames2 = {}, _defineProperty(_classNames2, "".concat(prefixCls, "-bordered"), _this.props.bordered), _defineProperty(_classNames2, "".concat(prefixCls, "-empty"), !data.length), _defineProperty(_classNames2, "".concat(prefixCls, "-without-column-header"), !showHeader), _classNames2));

      var columnsWithRowSelection = _this.renderRowSelection({
        prefixCls: prefixCls,
        locale: mergedLocale,
        getPopupContainer: realGetPopupContainer
      });

      var columns = _this.renderColumnsDropdown({
        columns: columnsWithRowSelection,
        prefixCls: prefixCls,
        dropdownPrefixCls: dropdownPrefixCls,
        locale: mergedLocale,
        getPopupContainer: realGetPopupContainer
      }).map(function (column, i) {
        var newColumn = _extends({}, column);

        newColumn.key = getColumnKey(newColumn, i);
        return newColumn;
      });

      var expandIconColumnIndex = columns[0] && columns[0].key === 'selection-column' ? 1 : 0;

      if ('expandIconColumnIndex' in restProps) {
        expandIconColumnIndex = restProps.expandIconColumnIndex;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_table__WEBPACK_IMPORTED_MODULE_2___default.a, _extends({
        ref: _this.setTableRef,
        key: "table",
        expandIcon: _this.renderExpandIcon(prefixCls)
      }, restProps, {
        onRow: function onRow(record, index) {
          return _this.onRow(prefixCls, record, index);
        },
        components: _this.state.components,
        prefixCls: prefixCls,
        data: data,
        columns: columns,
        showHeader: showHeader,
        className: classString,
        expandIconColumnIndex: expandIconColumnIndex,
        expandIconAsCell: expandIconAsCell,
        emptyText: mergedLocale.emptyText
      }));
    };

    _this.renderComponent = function (_ref3) {
      var getPrefixCls = _ref3.getPrefixCls,
          renderEmpty = _ref3.renderEmpty,
          getPopupContainer = _ref3.getPopupContainer;
      var _this$props = _this.props,
          customizePrefixCls = _this$props.prefixCls,
          customizeDropdownPrefixCls = _this$props.dropdownPrefixCls,
          style = _this$props.style,
          className = _this$props.className;

      var data = _this.getCurrentPageData();

      var loading = _this.props.loading;

      if (typeof loading === 'boolean') {
        loading = {
          spinning: loading
        };
      }

      var prefixCls = getPrefixCls('table', customizePrefixCls);
      var dropdownPrefixCls = getPrefixCls('dropdown', customizeDropdownPrefixCls);
      var table = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_20__["default"], {
        componentName: "Table",
        defaultLocale: _locale_default__WEBPACK_IMPORTED_MODULE_21__["default"].Table
      }, function (locale) {
        return _this.renderTable({
          prefixCls: prefixCls,
          renderEmpty: renderEmpty,
          dropdownPrefixCls: dropdownPrefixCls,
          contextLocale: locale,
          getPopupContainer: getPopupContainer
        });
      }); // if there is no pagination or no data,
      // the height of spin should decrease by half of pagination

      var paginationPatchClass = _this.hasPagination() && data && data.length !== 0 ? "".concat(prefixCls, "-with-pagination") : "".concat(prefixCls, "-without-pagination");
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: classnames__WEBPACK_IMPORTED_MODULE_4___default()("".concat(prefixCls, "-wrapper"), className),
        style: style
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_spin__WEBPACK_IMPORTED_MODULE_18__["default"], _extends({}, loading, {
        className: loading.spinning ? "".concat(paginationPatchClass, " ").concat(prefixCls, "-spin-holder") : ''
      }), _this.renderPagination(prefixCls, 'top'), table, _this.renderPagination(prefixCls, 'bottom')));
    };

    var expandedRowRender = props.expandedRowRender,
        columnsProp = props.columns;
    Object(_util_warning__WEBPACK_IMPORTED_MODULE_23__["default"])(!('columnsPageRange' in props || 'columnsPageSize' in props), 'Table', '`columnsPageRange` and `columnsPageSize` are removed, please use ' + 'fixed columns instead, see: https://u.ant.design/fixed-columns.');

    if (expandedRowRender && (columnsProp || []).some(function (_ref4) {
      var fixed = _ref4.fixed;
      return !!fixed;
    })) {
      Object(_util_warning__WEBPACK_IMPORTED_MODULE_23__["default"])(false, 'Table', '`expandedRowRender` and `Column.fixed` are not compatible. Please use one of them at one time.');
    }

    var columns = columnsProp || Object(_util__WEBPACK_IMPORTED_MODULE_14__["normalizeColumns"])(props.children);
    _this.state = _extends(_extends({}, _this.getDefaultSortOrder(columns || [])), {
      // 减少状态
      filters: _this.getDefaultFilters(columns),
      pagination: _this.getDefaultPagination(props),
      pivot: undefined,
      prevProps: props,
      components: createComponents(props.components),
      columns: columns
    });
    return _this;
  }

  _createClass(Table, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var _this$state = this.state,
          columns = _this$state.columns,
          sortColumn = _this$state.sortColumn,
          sortOrder = _this$state.sortOrder;

      if (this.getSortOrderColumns(columns).length > 0) {
        var sortState = this.getSortStateFromColumns(columns);

        if (!isSameColumn(sortState.sortColumn, sortColumn) || sortState.sortOrder !== sortOrder) {
          this.setState(sortState);
        }
      }
    }
  }, {
    key: "getDefaultSelection",
    value: function getDefaultSelection() {
      var _this2 = this;

      var rowSelection = getRowSelection(this.props);

      if (!rowSelection.getCheckboxProps) {
        return [];
      }

      return this.getFlatData().filter(function (item, rowIndex) {
        return _this2.getCheckboxPropsByItem(item, rowIndex).defaultChecked;
      }).map(function (record, rowIndex) {
        return _this2.getRecordKey(record, rowIndex);
      });
    }
  }, {
    key: "getDefaultPagination",
    value: function getDefaultPagination(props) {
      var pagination = _typeof(props.pagination) === 'object' ? props.pagination : {};
      var current;

      if ('current' in pagination) {
        current = pagination.current;
      } else if ('defaultCurrent' in pagination) {
        current = pagination.defaultCurrent;
      }

      var pageSize;

      if ('pageSize' in pagination) {
        pageSize = pagination.pageSize;
      } else if ('defaultPageSize' in pagination) {
        pageSize = pagination.defaultPageSize;
      }

      return this.hasPagination(props) ? _extends(_extends(_extends({}, defaultPagination), pagination), {
        current: current || 1,
        pageSize: pageSize || 10
      }) : {};
    }
  }, {
    key: "getSortOrderColumns",
    value: function getSortOrderColumns(columns) {
      return Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatFilter"])(columns || (this.state || {}).columns || [], function (column) {
        return 'sortOrder' in column;
      });
    }
  }, {
    key: "getDefaultFilters",
    value: function getDefaultFilters(columns) {
      var definedFilters = getFiltersFromColumns(this.state, columns);
      var defaultFilteredValueColumns = Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatFilter"])(columns || [], function (column) {
        return typeof column.defaultFilteredValue !== 'undefined';
      });
      var defaultFilters = defaultFilteredValueColumns.reduce(function (soFar, col) {
        var colKey = getColumnKey(col);
        soFar[colKey] = col.defaultFilteredValue;
        return soFar;
      }, {});
      return _extends(_extends({}, defaultFilters), definedFilters);
    }
  }, {
    key: "getDefaultSortOrder",
    value: function getDefaultSortOrder(columns) {
      var definedSortState = this.getSortStateFromColumns(columns);
      var defaultSortedColumn = Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatFilter"])(columns || [], function (column) {
        return column.defaultSortOrder != null;
      })[0];

      if (defaultSortedColumn && !definedSortState.sortColumn) {
        return {
          sortColumn: defaultSortedColumn,
          sortOrder: defaultSortedColumn.defaultSortOrder
        };
      }

      return definedSortState;
    }
  }, {
    key: "getSortStateFromColumns",
    value: function getSortStateFromColumns(columns) {
      // return first column which sortOrder is not falsy
      var sortedColumn = this.getSortOrderColumns(columns).filter(function (col) {
        return col.sortOrder;
      })[0];

      if (sortedColumn) {
        return {
          sortColumn: sortedColumn,
          sortOrder: sortedColumn.sortOrder
        };
      }

      return {
        sortColumn: null,
        sortOrder: null
      };
    }
  }, {
    key: "getMaxCurrent",
    value: function getMaxCurrent(total) {
      var _this$state$paginatio = this.state.pagination,
          current = _this$state$paginatio.current,
          pageSize = _this$state$paginatio.pageSize;

      if ((current - 1) * pageSize >= total) {
        return Math.floor((total - 1) / pageSize) + 1;
      }

      return current;
    }
  }, {
    key: "getSorterFn",
    value: function getSorterFn(state) {
      var _ref5 = state || this.state,
          sortOrder = _ref5.sortOrder,
          sortColumn = _ref5.sortColumn;

      if (!sortOrder || !sortColumn || typeof sortColumn.sorter !== 'function') {
        return;
      }

      return function (a, b) {
        var result = sortColumn.sorter(a, b, sortOrder);

        if (result !== 0) {
          return sortOrder === 'descend' ? -result : result;
        }

        return 0;
      };
    }
  }, {
    key: "getCurrentPageData",
    value: function getCurrentPageData() {
      var data = this.getLocalData();
      var current;
      var pageSize;
      var state = this.state; // 如果没有分页的话，默认全部展示

      if (!this.hasPagination()) {
        pageSize = Number.MAX_VALUE;
        current = 1;
      } else {
        pageSize = state.pagination.pageSize;
        current = this.getMaxCurrent(state.pagination.total || data.length);
      } // 分页
      // ---
      // 当数据量少于等于每页数量时，直接设置数据
      // 否则进行读取分页数据


      if (data.length > pageSize || pageSize === Number.MAX_VALUE) {
        data = data.slice((current - 1) * pageSize, current * pageSize);
      }

      return data;
    }
  }, {
    key: "getFlatData",
    value: function getFlatData() {
      var childrenColumnName = this.props.childrenColumnName;
      return Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatArray"])(this.getLocalData(null, false), childrenColumnName);
    }
  }, {
    key: "getFlatCurrentPageData",
    value: function getFlatCurrentPageData() {
      var childrenColumnName = this.props.childrenColumnName;
      return Object(_util__WEBPACK_IMPORTED_MODULE_14__["flatArray"])(this.getCurrentPageData(), childrenColumnName);
    }
  }, {
    key: "getLocalData",
    value: function getLocalData(state) {
      var _this3 = this;

      var filter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      var currentState = state || this.state;
      var dataSource = this.props.dataSource;
      var data = dataSource || []; // 优化本地排序

      data = data.slice(0);
      var sorterFn = this.getSorterFn(currentState);

      if (sorterFn) {
        data = this.recursiveSort(data, sorterFn);
      } // 筛选


      if (filter && currentState.filters) {
        Object.keys(currentState.filters).forEach(function (columnKey) {
          var col = _this3.findColumn(columnKey);

          if (!col) {
            return;
          }

          var values = currentState.filters[columnKey] || [];

          if (values.length === 0) {
            return;
          }

          var onFilter = col.onFilter;
          data = onFilter ? data.filter(function (record) {
            return values.some(function (v) {
              return onFilter(v, record);
            });
          }) : data;
        });
      }

      return data;
    }
  }, {
    key: "setSelectedRowKeys",
    value: function setSelectedRowKeys(selectedRowKeys, selectionInfo) {
      var _this4 = this;

      var selectWay = selectionInfo.selectWay,
          record = selectionInfo.record,
          checked = selectionInfo.checked,
          changeRowKeys = selectionInfo.changeRowKeys,
          nativeEvent = selectionInfo.nativeEvent;
      var rowSelection = getRowSelection(this.props);

      if (rowSelection && !('selectedRowKeys' in rowSelection)) {
        this.props.store.setState({
          selectedRowKeys: selectedRowKeys
        });
      }

      var data = this.getFlatData();

      if (!rowSelection.onChange && !rowSelection[selectWay]) {
        return;
      }

      var selectedRows = data.filter(function (row, i) {
        return selectedRowKeys.indexOf(_this4.getRecordKey(row, i)) >= 0;
      });

      if (rowSelection.onChange) {
        rowSelection.onChange(selectedRowKeys, selectedRows);
      }

      if (selectWay === 'onSelect' && rowSelection.onSelect) {
        rowSelection.onSelect(record, checked, selectedRows, nativeEvent);
      } else if (selectWay === 'onSelectMultiple' && rowSelection.onSelectMultiple) {
        var changeRows = data.filter(function (row, i) {
          return changeRowKeys.indexOf(_this4.getRecordKey(row, i)) >= 0;
        });
        rowSelection.onSelectMultiple(checked, selectedRows, changeRows);
      } else if (selectWay === 'onSelectAll' && rowSelection.onSelectAll) {
        var _changeRows = data.filter(function (row, i) {
          return changeRowKeys.indexOf(_this4.getRecordKey(row, i)) >= 0;
        });

        rowSelection.onSelectAll(checked, selectedRows, _changeRows);
      } else if (selectWay === 'onSelectInvert' && rowSelection.onSelectInvert) {
        rowSelection.onSelectInvert(selectedRowKeys);
      }
    }
  }, {
    key: "toggleSortOrder",
    value: function toggleSortOrder(column) {
      var sortDirections = column.sortDirections || this.props.sortDirections;
      var _this$state2 = this.state,
          sortOrder = _this$state2.sortOrder,
          sortColumn = _this$state2.sortColumn; // 只同时允许一列进行排序，否则会导致排序顺序的逻辑问题

      var newSortOrder; // 切换另一列时，丢弃 sortOrder 的状态

      if (isSameColumn(sortColumn, column) && sortOrder !== undefined) {
        // 按照sortDirections的内容依次切换排序状态
        var methodIndex = sortDirections.indexOf(sortOrder) + 1;
        newSortOrder = methodIndex === sortDirections.length ? undefined : sortDirections[methodIndex];
      } else {
        newSortOrder = sortDirections[0];
      }

      var newState = {
        sortOrder: newSortOrder,
        sortColumn: newSortOrder ? column : null
      }; // Controlled

      if (this.getSortOrderColumns().length === 0) {
        this.setState(newState, this.scrollToFirstRow);
      }

      var onChange = this.props.onChange;

      if (onChange) {
        onChange.apply(null, this.prepareParamsArguments(_extends(_extends({}, this.state), newState), column));
      }
    }
  }, {
    key: "hasPagination",
    value: function hasPagination(props) {
      return (props || this.props).pagination !== false;
    }
  }, {
    key: "isSortColumn",
    value: function isSortColumn(column) {
      var sortColumn = this.state.sortColumn;

      if (!column || !sortColumn) {
        return false;
      }

      return getColumnKey(sortColumn) === getColumnKey(column);
    } // Get pagination, filters, sorter

  }, {
    key: "prepareParamsArguments",
    value: function prepareParamsArguments(state, column) {
      var pagination = _extends({}, state.pagination); // remove useless handle function in Table.onChange


      delete pagination.onChange;
      delete pagination.onShowSizeChange;
      var filters = state.filters;
      var sorter = {};
      var currentColumn = column;

      if (state.sortColumn && state.sortOrder) {
        currentColumn = state.sortColumn;
        sorter.column = state.sortColumn;
        sorter.order = state.sortOrder;
      }

      if (currentColumn) {
        sorter.field = currentColumn.dataIndex;
        sorter.columnKey = getColumnKey(currentColumn);
      }

      var extra = {
        currentDataSource: this.getLocalData(state)
      };
      return [pagination, filters, sorter, extra];
    }
  }, {
    key: "findColumn",
    value: function findColumn(myKey) {
      var column;
      Object(_util__WEBPACK_IMPORTED_MODULE_14__["treeMap"])(this.state.columns, function (c) {
        if (getColumnKey(c) === myKey) {
          column = c;
        }
      });
      return column;
    }
  }, {
    key: "recursiveSort",
    value: function recursiveSort(data, sorterFn) {
      var _this5 = this;

      var _this$props$childrenC = this.props.childrenColumnName,
          childrenColumnName = _this$props$childrenC === void 0 ? 'children' : _this$props$childrenC;
      return data.sort(sorterFn).map(function (item) {
        return item[childrenColumnName] ? _extends(_extends({}, item), _defineProperty({}, childrenColumnName, _this5.recursiveSort(item[childrenColumnName], sorterFn))) : item;
      });
    }
  }, {
    key: "renderPagination",
    value: function renderPagination(prefixCls, paginationPosition) {
      // 强制不需要分页
      if (!this.hasPagination()) {
        return null;
      }

      var size = 'default';
      var pagination = this.state.pagination;

      if (pagination.size) {
        size = pagination.size;
      } else if (this.props.size === 'middle' || this.props.size === 'small') {
        size = 'small';
      }

      var position = pagination.position || 'bottom';
      var total = pagination.total || this.getLocalData().length;
      return total > 0 && (position === paginationPosition || position === 'both') ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_pagination__WEBPACK_IMPORTED_MODULE_16__["default"], _extends({
        key: "pagination-".concat(paginationPosition)
      }, pagination, {
        className: classnames__WEBPACK_IMPORTED_MODULE_4___default()(pagination.className, "".concat(prefixCls, "-pagination")),
        onChange: this.handlePageChange,
        total: total,
        size: size,
        current: this.getMaxCurrent(total),
        onShowSizeChange: this.handleShowSizeChange
      })) : null;
    }
  }, {
    key: "renderRowSelection",
    value: function renderRowSelection(_ref6) {
      var _this6 = this;

      var prefixCls = _ref6.prefixCls,
          locale = _ref6.locale,
          getPopupContainer = _ref6.getPopupContainer;
      var rowSelection = this.props.rowSelection;
      var columns = this.state.columns.concat();

      if (rowSelection) {
        var data = this.getFlatCurrentPageData().filter(function (item, index) {
          if (rowSelection.getCheckboxProps) {
            return !_this6.getCheckboxPropsByItem(item, index).disabled;
          }

          return true;
        });
        var selectionColumnClass = classnames__WEBPACK_IMPORTED_MODULE_4___default()("".concat(prefixCls, "-selection-column"), _defineProperty({}, "".concat(prefixCls, "-selection-column-custom"), rowSelection.selections));

        var selectionColumn = _defineProperty({
          key: 'selection-column',
          render: this.renderSelectionBox(rowSelection.type),
          className: selectionColumnClass,
          fixed: rowSelection.fixed,
          width: rowSelection.columnWidth,
          title: rowSelection.columnTitle
        }, rc_table__WEBPACK_IMPORTED_MODULE_2__["INTERNAL_COL_DEFINE"], {
          className: "".concat(prefixCls, "-selection-col")
        });

        if (rowSelection.type !== 'radio') {
          var checkboxAllDisabled = data.every(function (item, index) {
            return _this6.getCheckboxPropsByItem(item, index).disabled;
          });
          selectionColumn.title = selectionColumn.title || react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_SelectionCheckboxAll__WEBPACK_IMPORTED_MODULE_10__["default"], {
            store: this.props.store,
            locale: locale,
            data: data,
            getCheckboxPropsByItem: this.getCheckboxPropsByItem,
            getRecordKey: this.getRecordKey,
            disabled: checkboxAllDisabled,
            prefixCls: prefixCls,
            onSelect: this.handleSelectRow,
            selections: rowSelection.selections,
            hideDefaultSelections: rowSelection.hideDefaultSelections,
            getPopupContainer: this.generatePopupContainerFunc(getPopupContainer)
          });
        }

        if ('fixed' in rowSelection) {
          selectionColumn.fixed = rowSelection.fixed;
        } else if (columns.some(function (column) {
          return column.fixed === 'left' || column.fixed === true;
        })) {
          selectionColumn.fixed = 'left';
        }

        if (columns[0] && columns[0].key === 'selection-column') {
          columns[0] = selectionColumn;
        } else {
          columns.unshift(selectionColumn);
        }
      }

      return columns;
    }
  }, {
    key: "renderColumnsDropdown",
    value: function renderColumnsDropdown(_ref7) {
      var _this7 = this;

      var prefixCls = _ref7.prefixCls,
          dropdownPrefixCls = _ref7.dropdownPrefixCls,
          columns = _ref7.columns,
          locale = _ref7.locale,
          getPopupContainer = _ref7.getPopupContainer;
      var _this$state3 = this.state,
          sortOrder = _this$state3.sortOrder,
          filters = _this$state3.filters;
      return Object(_util__WEBPACK_IMPORTED_MODULE_14__["treeMap"])(columns, function (column, i) {
        var _classNames4;

        var key = getColumnKey(column, i);
        var filterDropdown;
        var sortButton;
        var onHeaderCell = column.onHeaderCell;

        var isSortColumn = _this7.isSortColumn(column);

        if (column.filters && column.filters.length > 0 || column.filterDropdown) {
          var colFilters = key in filters ? filters[key] : [];
          filterDropdown = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_filterDropdown__WEBPACK_IMPORTED_MODULE_7__["default"], {
            locale: locale,
            column: column,
            selectedKeys: colFilters,
            confirmFilter: _this7.handleFilter,
            prefixCls: "".concat(prefixCls, "-filter"),
            dropdownPrefixCls: dropdownPrefixCls || 'ant-dropdown',
            getPopupContainer: _this7.generatePopupContainerFunc(getPopupContainer),
            key: "filter-dropdown"
          });
        }

        if (column.sorter) {
          var sortDirections = column.sortDirections || _this7.props.sortDirections;
          var isAscend = isSortColumn && sortOrder === 'ascend';
          var isDescend = isSortColumn && sortOrder === 'descend';
          var ascend = sortDirections.indexOf('ascend') !== -1 && react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_17__["default"], {
            className: "".concat(prefixCls, "-column-sorter-up ").concat(isAscend ? 'on' : 'off'),
            type: "caret-up",
            theme: "filled"
          });
          var descend = sortDirections.indexOf('descend') !== -1 && react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_17__["default"], {
            className: "".concat(prefixCls, "-column-sorter-down ").concat(isDescend ? 'on' : 'off'),
            type: "caret-down",
            theme: "filled"
          });
          sortButton = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
            title: locale.sortTitle,
            className: classnames__WEBPACK_IMPORTED_MODULE_4___default()("".concat(prefixCls, "-column-sorter-inner"), ascend && descend && "".concat(prefixCls, "-column-sorter-inner-full")),
            key: "sorter"
          }, ascend, descend);

          onHeaderCell = function onHeaderCell(col) {
            var colProps = {}; // Get original first

            if (column.onHeaderCell) {
              colProps = _extends({}, column.onHeaderCell(col));
            } // Add sorter logic


            var onHeaderCellClick = colProps.onClick;

            colProps.onClick = function () {
              _this7.toggleSortOrder(column);

              if (onHeaderCellClick) {
                onHeaderCellClick.apply(void 0, arguments);
              }
            };

            return colProps;
          };
        }

        return _extends(_extends({}, column), {
          className: classnames__WEBPACK_IMPORTED_MODULE_4___default()(column.className, (_classNames4 = {}, _defineProperty(_classNames4, "".concat(prefixCls, "-column-has-actions"), sortButton || filterDropdown), _defineProperty(_classNames4, "".concat(prefixCls, "-column-has-filters"), filterDropdown), _defineProperty(_classNames4, "".concat(prefixCls, "-column-has-sorters"), sortButton), _defineProperty(_classNames4, "".concat(prefixCls, "-column-sort"), isSortColumn && sortOrder), _classNames4)),
          title: [react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            key: "title",
            className: "".concat(prefixCls, "-header-column")
          }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
            className: sortButton ? "".concat(prefixCls, "-column-sorters") : undefined
          }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            className: "".concat(prefixCls, "-column-title")
          }, _this7.renderColumnTitle(column.title)), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
            className: "".concat(prefixCls, "-column-sorter")
          }, sortButton))), filterDropdown],
          onHeaderCell: onHeaderCell
        });
      });
    }
  }, {
    key: "renderColumnTitle",
    value: function renderColumnTitle(title) {
      var _this$state4 = this.state,
          filters = _this$state4.filters,
          sortOrder = _this$state4.sortOrder,
          sortColumn = _this$state4.sortColumn; // https://github.com/ant-design/ant-design/issues/11246#issuecomment-405009167

      if (title instanceof Function) {
        return title({
          filters: filters,
          sortOrder: sortOrder,
          sortColumn: sortColumn
        });
      }

      return title;
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_22__["ConfigConsumer"], null, this.renderComponent);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var prevProps = prevState.prevProps;
      var columns = nextProps.columns || Object(_util__WEBPACK_IMPORTED_MODULE_14__["normalizeColumns"])(nextProps.children);

      var nextState = _extends(_extends({}, prevState), {
        prevProps: nextProps,
        columns: columns
      });

      if ('pagination' in nextProps || 'pagination' in prevProps) {
        var newPagination = _extends(_extends(_extends({}, defaultPagination), prevState.pagination), nextProps.pagination);

        newPagination.current = newPagination.current || 1;
        newPagination.pageSize = newPagination.pageSize || 10;
        nextState = _extends(_extends({}, nextState), {
          pagination: nextProps.pagination !== false ? newPagination : emptyObject
        });
      }

      if (nextProps.rowSelection && 'selectedRowKeys' in nextProps.rowSelection) {
        nextProps.store.setState({
          selectedRowKeys: nextProps.rowSelection.selectedRowKeys || []
        });
      } else if (prevProps.rowSelection && !nextProps.rowSelection) {
        nextProps.store.setState({
          selectedRowKeys: []
        });
      }

      if ('dataSource' in nextProps && nextProps.dataSource !== prevProps.dataSource) {
        nextProps.store.setState({
          selectionDirty: false
        });
      } // https://github.com/ant-design/ant-design/issues/10133


      nextProps.setCheckboxPropsCache({}); // Update filters

      var filteredValueColumns = getFilteredValueColumns(nextState, nextState.columns);

      if (filteredValueColumns.length > 0) {
        var filtersFromColumns = getFiltersFromColumns(nextState, nextState.columns);

        var newFilters = _extends({}, nextState.filters);

        Object.keys(filtersFromColumns).forEach(function (key) {
          newFilters[key] = filtersFromColumns[key];
        });

        if (isFiltersChanged(nextState, newFilters)) {
          nextState = _extends(_extends({}, nextState), {
            filters: newFilters
          });
        }
      }

      if (!isTheSameComponents(nextProps.components, prevProps.components)) {
        var components = createComponents(nextProps.components);
        nextState = _extends(_extends({}, nextState), {
          components: components
        });
      }

      return nextState;
    }
  }]);

  return Table;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Table.propTypes = {
  dataSource: prop_types__WEBPACK_IMPORTED_MODULE_3__["array"],
  columns: prop_types__WEBPACK_IMPORTED_MODULE_3__["array"],
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_3__["string"],
  useFixedHeader: prop_types__WEBPACK_IMPORTED_MODULE_3__["bool"],
  rowSelection: prop_types__WEBPACK_IMPORTED_MODULE_3__["object"],
  className: prop_types__WEBPACK_IMPORTED_MODULE_3__["string"],
  size: prop_types__WEBPACK_IMPORTED_MODULE_3__["string"],
  loading: prop_types__WEBPACK_IMPORTED_MODULE_3__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_3__["bool"], prop_types__WEBPACK_IMPORTED_MODULE_3__["object"]]),
  bordered: prop_types__WEBPACK_IMPORTED_MODULE_3__["bool"],
  onChange: prop_types__WEBPACK_IMPORTED_MODULE_3__["func"],
  locale: prop_types__WEBPACK_IMPORTED_MODULE_3__["object"],
  dropdownPrefixCls: prop_types__WEBPACK_IMPORTED_MODULE_3__["string"],
  sortDirections: prop_types__WEBPACK_IMPORTED_MODULE_3__["array"],
  getPopupContainer: prop_types__WEBPACK_IMPORTED_MODULE_3__["func"]
};
Table.defaultProps = {
  dataSource: [],
  useFixedHeader: false,
  className: '',
  size: 'default',
  loading: false,
  bordered: false,
  indentSize: 20,
  locale: {},
  rowKey: 'key',
  showHeader: true,
  sortDirections: ['ascend', 'descend'],
  childrenColumnName: 'children'
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_6__["polyfill"])(Table);

var StoreTable =
/*#__PURE__*/
function (_React$Component2) {
  _inherits(StoreTable, _React$Component2);

  function StoreTable(props) {
    var _this8;

    _classCallCheck(this, StoreTable);

    _this8 = _possibleConstructorReturn(this, _getPrototypeOf(StoreTable).call(this, props));

    _this8.setCheckboxPropsCache = function (cache) {
      return _this8.CheckboxPropsCache = cache;
    };

    _this8.CheckboxPropsCache = {};
    _this8.store = Object(_createStore__WEBPACK_IMPORTED_MODULE_8__["default"])({
      selectedRowKeys: getRowSelection(props).selectedRowKeys || [],
      selectionDirty: false
    });
    return _this8;
  }

  _createClass(StoreTable, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](Table, _extends({}, this.props, {
        store: this.store,
        checkboxPropsCache: this.CheckboxPropsCache,
        setCheckboxPropsCache: this.setCheckboxPropsCache
      }));
    }
  }]);

  return StoreTable;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

StoreTable.displayName = 'withStore(Table)';
StoreTable.Column = _Column__WEBPACK_IMPORTED_MODULE_11__["default"];
StoreTable.ColumnGroup = _ColumnGroup__WEBPACK_IMPORTED_MODULE_12__["default"];
/* harmony default export */ __webpack_exports__["default"] = (StoreTable);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90YWJsZS9UYWJsZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvdGFibGUvVGFibGUuanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbi8qIGVzbGludC1kaXNhYmxlIHByZWZlci1zcHJlYWQgKi9cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBvbWl0IGZyb20gJ29taXQuanMnO1xuaW1wb3J0IFJjVGFibGUsIHsgSU5URVJOQUxfQ09MX0RFRklORSB9IGZyb20gJ3JjLXRhYmxlJztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHNoYWxsb3dFcXVhbCBmcm9tICdzaGFsbG93ZXF1YWwnO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgRmlsdGVyRHJvcGRvd24gZnJvbSAnLi9maWx0ZXJEcm9wZG93bic7XG5pbXBvcnQgY3JlYXRlU3RvcmUgZnJvbSAnLi9jcmVhdGVTdG9yZSc7XG5pbXBvcnQgU2VsZWN0aW9uQm94IGZyb20gJy4vU2VsZWN0aW9uQm94JztcbmltcG9ydCBTZWxlY3Rpb25DaGVja2JveEFsbCBmcm9tICcuL1NlbGVjdGlvbkNoZWNrYm94QWxsJztcbmltcG9ydCBDb2x1bW4gZnJvbSAnLi9Db2x1bW4nO1xuaW1wb3J0IENvbHVtbkdyb3VwIGZyb20gJy4vQ29sdW1uR3JvdXAnO1xuaW1wb3J0IGNyZWF0ZUJvZHlSb3cgZnJvbSAnLi9jcmVhdGVCb2R5Um93JztcbmltcG9ydCB7IGZsYXRBcnJheSwgdHJlZU1hcCwgZmxhdEZpbHRlciwgbm9ybWFsaXplQ29sdW1ucyB9IGZyb20gJy4vdXRpbCc7XG5pbXBvcnQgc2Nyb2xsVG8gZnJvbSAnLi4vX3V0aWwvc2Nyb2xsVG8nO1xuaW1wb3J0IFBhZ2luYXRpb24gZnJvbSAnLi4vcGFnaW5hdGlvbic7XG5pbXBvcnQgSWNvbiBmcm9tICcuLi9pY29uJztcbmltcG9ydCBTcGluIGZyb20gJy4uL3NwaW4nO1xuaW1wb3J0IFRyYW5zQnV0dG9uIGZyb20gJy4uL191dGlsL3RyYW5zQnV0dG9uJztcbmltcG9ydCBMb2NhbGVSZWNlaXZlciBmcm9tICcuLi9sb2NhbGUtcHJvdmlkZXIvTG9jYWxlUmVjZWl2ZXInO1xuaW1wb3J0IGRlZmF1bHRMb2NhbGUgZnJvbSAnLi4vbG9jYWxlL2RlZmF1bHQnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IHdhcm5pbmcgZnJvbSAnLi4vX3V0aWwvd2FybmluZyc7XG5mdW5jdGlvbiBub29wKCkgeyB9XG5mdW5jdGlvbiBzdG9wUHJvcGFnYXRpb24oZSkge1xuICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG59XG5mdW5jdGlvbiBnZXRSb3dTZWxlY3Rpb24ocHJvcHMpIHtcbiAgICByZXR1cm4gcHJvcHMucm93U2VsZWN0aW9uIHx8IHt9O1xufVxuZnVuY3Rpb24gZ2V0Q29sdW1uS2V5KGNvbHVtbiwgaW5kZXgpIHtcbiAgICByZXR1cm4gY29sdW1uLmtleSB8fCBjb2x1bW4uZGF0YUluZGV4IHx8IGluZGV4O1xufVxuZnVuY3Rpb24gaXNTYW1lQ29sdW1uKGEsIGIpIHtcbiAgICBpZiAoYSAmJiBiICYmIGEua2V5ICYmIGEua2V5ID09PSBiLmtleSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIChhID09PSBiIHx8XG4gICAgICAgIHNoYWxsb3dFcXVhbChhLCBiLCAodmFsdWUsIG90aGVyKSA9PiB7XG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xMjczN1xuICAgICAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2Ygb3RoZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWUgPT09IG90aGVyIHx8IHZhbHVlLnRvU3RyaW5nKCkgPT09IG90aGVyLnRvU3RyaW5nKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8xOTM5OFxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpICYmIEFycmF5LmlzQXJyYXkob3RoZXIpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlID09PSBvdGhlciB8fCBzaGFsbG93RXF1YWwodmFsdWUsIG90aGVyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSkpO1xufVxuY29uc3QgZGVmYXVsdFBhZ2luYXRpb24gPSB7XG4gICAgb25DaGFuZ2U6IG5vb3AsXG4gICAgb25TaG93U2l6ZUNoYW5nZTogbm9vcCxcbn07XG4vKipcbiAqIEF2b2lkIGNyZWF0aW5nIG5ldyBvYmplY3QsIHNvIHRoYXQgcGFyZW50IGNvbXBvbmVudCdzIHNob3VsZENvbXBvbmVudFVwZGF0ZVxuICogY2FuIHdvcmtzIGFwcHJvcHJpYXRlbHnjgIJcbiAqL1xuY29uc3QgZW1wdHlPYmplY3QgPSB7fTtcbmNvbnN0IGNyZWF0ZUNvbXBvbmVudHMgPSAoY29tcG9uZW50cyA9IHt9KSA9PiB7XG4gICAgY29uc3QgYm9keVJvdyA9IGNvbXBvbmVudHMgJiYgY29tcG9uZW50cy5ib2R5ICYmIGNvbXBvbmVudHMuYm9keS5yb3c7XG4gICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgY29tcG9uZW50cyksIHsgYm9keTogT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBjb21wb25lbnRzLmJvZHkpLCB7IHJvdzogY3JlYXRlQm9keVJvdyhib2R5Um93KSB9KSB9KTtcbn07XG5mdW5jdGlvbiBpc1RoZVNhbWVDb21wb25lbnRzKGNvbXBvbmVudHMxID0ge30sIGNvbXBvbmVudHMyID0ge30pIHtcbiAgICByZXR1cm4gKGNvbXBvbmVudHMxID09PSBjb21wb25lbnRzMiB8fFxuICAgICAgICBbJ3RhYmxlJywgJ2hlYWRlcicsICdib2R5J10uZXZlcnkoKGtleSkgPT4gc2hhbGxvd0VxdWFsKGNvbXBvbmVudHMxW2tleV0sIGNvbXBvbmVudHMyW2tleV0pKSk7XG59XG5mdW5jdGlvbiBnZXRGaWx0ZXJlZFZhbHVlQ29sdW1ucyhzdGF0ZSwgY29sdW1ucykge1xuICAgIHJldHVybiBmbGF0RmlsdGVyKGNvbHVtbnMgfHwgKHN0YXRlIHx8IHt9KS5jb2x1bW5zIHx8IFtdLCAoY29sdW1uKSA9PiB0eXBlb2YgY29sdW1uLmZpbHRlcmVkVmFsdWUgIT09ICd1bmRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGdldEZpbHRlcnNGcm9tQ29sdW1ucyhzdGF0ZSA9IHt9LCBjb2x1bW5zKSB7XG4gICAgY29uc3QgZmlsdGVycyA9IHt9O1xuICAgIGdldEZpbHRlcmVkVmFsdWVDb2x1bW5zKHN0YXRlLCBjb2x1bW5zKS5mb3JFYWNoKChjb2wpID0+IHtcbiAgICAgICAgY29uc3QgY29sS2V5ID0gZ2V0Q29sdW1uS2V5KGNvbCk7XG4gICAgICAgIGZpbHRlcnNbY29sS2V5XSA9IGNvbC5maWx0ZXJlZFZhbHVlO1xuICAgIH0pO1xuICAgIHJldHVybiBmaWx0ZXJzO1xufVxuZnVuY3Rpb24gaXNGaWx0ZXJzQ2hhbmdlZChzdGF0ZSwgZmlsdGVycykge1xuICAgIGlmIChPYmplY3Qua2V5cyhmaWx0ZXJzKS5sZW5ndGggIT09IE9iamVjdC5rZXlzKHN0YXRlLmZpbHRlcnMpLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKGZpbHRlcnMpLnNvbWUoY29sdW1uS2V5ID0+IGZpbHRlcnNbY29sdW1uS2V5XSAhPT0gc3RhdGUuZmlsdGVyc1tjb2x1bW5LZXldKTtcbn1cbmNsYXNzIFRhYmxlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMuc2V0VGFibGVSZWYgPSAodGFibGUpID0+IHtcbiAgICAgICAgICAgIHRoaXMucmNUYWJsZSA9IHRhYmxlO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmdldENoZWNrYm94UHJvcHNCeUl0ZW0gPSAoaXRlbSwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHJvd1NlbGVjdGlvbiA9IGdldFJvd1NlbGVjdGlvbih0aGlzLnByb3BzKTtcbiAgICAgICAgICAgIGlmICghcm93U2VsZWN0aW9uLmdldENoZWNrYm94UHJvcHMpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge307XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmdldFJlY29yZEtleShpdGVtLCBpbmRleCk7XG4gICAgICAgICAgICAvLyBDYWNoZSBjaGVja2JveFByb3BzXG4gICAgICAgICAgICBpZiAoIXRoaXMucHJvcHMuY2hlY2tib3hQcm9wc0NhY2hlW2tleV0pIHtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLmNoZWNrYm94UHJvcHNDYWNoZVtrZXldID0gcm93U2VsZWN0aW9uLmdldENoZWNrYm94UHJvcHMoaXRlbSkgfHwge307XG4gICAgICAgICAgICAgICAgY29uc3QgY2hlY2tib3hQcm9wcyA9IHRoaXMucHJvcHMuY2hlY2tib3hQcm9wc0NhY2hlW2tleV07XG4gICAgICAgICAgICAgICAgd2FybmluZyghKCdjaGVja2VkJyBpbiBjaGVja2JveFByb3BzKSAmJiAhKCdkZWZhdWx0Q2hlY2tlZCcgaW4gY2hlY2tib3hQcm9wcyksICdUYWJsZScsICdEbyBub3Qgc2V0IGBjaGVja2VkYCBvciBgZGVmYXVsdENoZWNrZWRgIGluIGBnZXRDaGVja2JveFByb3BzYC4gUGxlYXNlIHVzZSBgc2VsZWN0ZWRSb3dLZXlzYCBpbnN0ZWFkLicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucHJvcHMuY2hlY2tib3hQcm9wc0NhY2hlW2tleV07XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuZ2V0UmVjb3JkS2V5ID0gKHJlY29yZCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHsgcm93S2V5IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgcmVjb3JkS2V5ID0gdHlwZW9mIHJvd0tleSA9PT0gJ2Z1bmN0aW9uJyA/IHJvd0tleShyZWNvcmQsIGluZGV4KSA6IHJlY29yZFtyb3dLZXldO1xuICAgICAgICAgICAgd2FybmluZyhyZWNvcmRLZXkgIT09IHVuZGVmaW5lZCwgJ1RhYmxlJywgJ0VhY2ggcmVjb3JkIGluIGRhdGFTb3VyY2Ugb2YgdGFibGUgc2hvdWxkIGhhdmUgYSB1bmlxdWUgYGtleWAgcHJvcCwgJyArXG4gICAgICAgICAgICAgICAgJ29yIHNldCBgcm93S2V5YCBvZiBUYWJsZSB0byBhbiB1bmlxdWUgcHJpbWFyeSBrZXksICcgK1xuICAgICAgICAgICAgICAgICdzZWUgaHR0cHM6Ly91LmFudC5kZXNpZ24vdGFibGUtcm93LWtleScpO1xuICAgICAgICAgICAgcmV0dXJuIHJlY29yZEtleSA9PT0gdW5kZWZpbmVkID8gaW5kZXggOiByZWNvcmRLZXk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25Sb3cgPSAocHJlZml4Q2xzLCByZWNvcmQsIGluZGV4KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uUm93IH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgY29uc3QgY3VzdG9tID0gb25Sb3cgPyBvblJvdyhyZWNvcmQsIGluZGV4KSA6IHt9O1xuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgY3VzdG9tKSwgeyBwcmVmaXhDbHMsIHN0b3JlOiB0aGlzLnByb3BzLnN0b3JlLCByb3dLZXk6IHRoaXMuZ2V0UmVjb3JkS2V5KHJlY29yZCwgaW5kZXgpIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmdlbmVyYXRlUG9wdXBDb250YWluZXJGdW5jID0gKGdldFBvcHVwQ29udGFpbmVyKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHNjcm9sbCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHRhYmxlID0gdGhpcy5yY1RhYmxlO1xuICAgICAgICAgICAgaWYgKGdldFBvcHVwQ29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGdldFBvcHVwQ29udGFpbmVyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gVXNlIHVuZGVmaW5lZCB0byBsZXQgcmMgY29tcG9uZW50IHVzZSBkZWZhdWx0IGxvZ2ljLlxuICAgICAgICAgICAgcmV0dXJuIHNjcm9sbCAmJiB0YWJsZSA/ICgpID0+IHRhYmxlLnRhYmxlTm9kZSA6IHVuZGVmaW5lZDtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5zY3JvbGxUb0ZpcnN0Um93ID0gKCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBzY3JvbGwgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAoc2Nyb2xsICYmIHNjcm9sbC5zY3JvbGxUb0ZpcnN0Um93T25DaGFuZ2UgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG8oMCwge1xuICAgICAgICAgICAgICAgICAgICBnZXRDb250YWluZXI6ICgpID0+IHRoaXMucmNUYWJsZS5ib2R5VGFibGUsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlRmlsdGVyID0gKGNvbHVtbiwgbmV4dEZpbHRlcnMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHBhZ2luYXRpb24gPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlLnBhZ2luYXRpb24pO1xuICAgICAgICAgICAgY29uc3QgZmlsdGVycyA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5zdGF0ZS5maWx0ZXJzKSwgeyBbZ2V0Q29sdW1uS2V5KGNvbHVtbildOiBuZXh0RmlsdGVycyB9KTtcbiAgICAgICAgICAgIC8vIFJlbW92ZSBmaWx0ZXJzIG5vdCBpbiBjdXJyZW50IGNvbHVtbnNcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRDb2x1bW5LZXlzID0gW107XG4gICAgICAgICAgICB0cmVlTWFwKHRoaXMuc3RhdGUuY29sdW1ucywgYyA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKCFjLmNoaWxkcmVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRDb2x1bW5LZXlzLnB1c2goZ2V0Q29sdW1uS2V5KGMpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKGZpbHRlcnMpLmZvckVhY2goY29sdW1uS2V5ID0+IHtcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudENvbHVtbktleXMuaW5kZXhPZihjb2x1bW5LZXkpIDwgMCkge1xuICAgICAgICAgICAgICAgICAgICBkZWxldGUgZmlsdGVyc1tjb2x1bW5LZXldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKHByb3BzLnBhZ2luYXRpb24pIHtcbiAgICAgICAgICAgICAgICAvLyBSZXNldCBjdXJyZW50IHByb3BcbiAgICAgICAgICAgICAgICBwYWdpbmF0aW9uLmN1cnJlbnQgPSAxO1xuICAgICAgICAgICAgICAgIHBhZ2luYXRpb24ub25DaGFuZ2UocGFnaW5hdGlvbi5jdXJyZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xuICAgICAgICAgICAgICAgIHBhZ2luYXRpb24sXG4gICAgICAgICAgICAgICAgZmlsdGVyczoge30sXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgY29uc3QgZmlsdGVyc1RvU2V0U3RhdGUgPSBPYmplY3QuYXNzaWduKHt9LCBmaWx0ZXJzKTtcbiAgICAgICAgICAgIC8vIFJlbW92ZSBmaWx0ZXJzIHdoaWNoIGlzIGNvbnRyb2xsZWRcbiAgICAgICAgICAgIGdldEZpbHRlcmVkVmFsdWVDb2x1bW5zKHRoaXMuc3RhdGUpLmZvckVhY2goKGNvbCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNvbHVtbktleSA9IGdldENvbHVtbktleShjb2wpO1xuICAgICAgICAgICAgICAgIGlmIChjb2x1bW5LZXkpIHtcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIGZpbHRlcnNUb1NldFN0YXRlW2NvbHVtbktleV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAoT2JqZWN0LmtleXMoZmlsdGVyc1RvU2V0U3RhdGUpLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5maWx0ZXJzID0gZmlsdGVyc1RvU2V0U3RhdGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBDb250cm9sbGVkIGN1cnJlbnQgcHJvcCB3aWxsIG5vdCByZXNwb25kIHVzZXIgaW50ZXJhY3Rpb25cbiAgICAgICAgICAgIGlmICh0eXBlb2YgcHJvcHMucGFnaW5hdGlvbiA9PT0gJ29iamVjdCcgJiYgJ2N1cnJlbnQnIGluIHByb3BzLnBhZ2luYXRpb24pIHtcbiAgICAgICAgICAgICAgICBuZXdTdGF0ZS5wYWdpbmF0aW9uID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBwYWdpbmF0aW9uKSwgeyBjdXJyZW50OiB0aGlzLnN0YXRlLnBhZ2luYXRpb24uY3VycmVudCB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUsICgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbFRvRmlyc3RSb3coKTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uRGlydHk6IGZhbHNlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGNvbnN0IHsgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlLmFwcGx5KG51bGwsIHRoaXMucHJlcGFyZVBhcmFtc0FyZ3VtZW50cyhPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHRoaXMuc3RhdGUpLCB7IHNlbGVjdGlvbkRpcnR5OiBmYWxzZSwgZmlsdGVycyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2luYXRpb24gfSkpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5oYW5kbGVTZWxlY3QgPSAocmVjb3JkLCByb3dJbmRleCwgZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgY2hlY2tlZCA9IGUudGFyZ2V0LmNoZWNrZWQ7XG4gICAgICAgICAgICBjb25zdCBuYXRpdmVFdmVudCA9IGUubmF0aXZlRXZlbnQ7XG4gICAgICAgICAgICBjb25zdCBkZWZhdWx0U2VsZWN0aW9uID0gdGhpcy5wcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLnNlbGVjdGlvbkRpcnR5XG4gICAgICAgICAgICAgICAgPyBbXVxuICAgICAgICAgICAgICAgIDogdGhpcy5nZXREZWZhdWx0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICBsZXQgc2VsZWN0ZWRSb3dLZXlzID0gdGhpcy5wcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLnNlbGVjdGVkUm93S2V5cy5jb25jYXQoZGVmYXVsdFNlbGVjdGlvbik7XG4gICAgICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmdldFJlY29yZEtleShyZWNvcmQsIHJvd0luZGV4KTtcbiAgICAgICAgICAgIGNvbnN0IHsgcGl2b3QgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBjb25zdCByb3dzID0gdGhpcy5nZXRGbGF0Q3VycmVudFBhZ2VEYXRhKCk7XG4gICAgICAgICAgICBsZXQgcmVhbEluZGV4ID0gcm93SW5kZXg7XG4gICAgICAgICAgICBpZiAodGhpcy5wcm9wcy5leHBhbmRlZFJvd1JlbmRlcikge1xuICAgICAgICAgICAgICAgIHJlYWxJbmRleCA9IHJvd3MuZmluZEluZGV4KHJvdyA9PiB0aGlzLmdldFJlY29yZEtleShyb3csIHJvd0luZGV4KSA9PT0ga2V5KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChuYXRpdmVFdmVudC5zaGlmdEtleSAmJiBwaXZvdCAhPT0gdW5kZWZpbmVkICYmIHJlYWxJbmRleCAhPT0gcGl2b3QpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjaGFuZ2VSb3dLZXlzID0gW107XG4gICAgICAgICAgICAgICAgY29uc3QgZGlyZWN0aW9uID0gTWF0aC5zaWduKHBpdm90IC0gcmVhbEluZGV4KTtcbiAgICAgICAgICAgICAgICBjb25zdCBkaXN0ID0gTWF0aC5hYnMocGl2b3QgLSByZWFsSW5kZXgpO1xuICAgICAgICAgICAgICAgIGxldCBzdGVwID0gMDtcbiAgICAgICAgICAgICAgICB3aGlsZSAoc3RlcCA8PSBkaXN0KSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGkgPSByZWFsSW5kZXggKyBzdGVwICogZGlyZWN0aW9uO1xuICAgICAgICAgICAgICAgICAgICBzdGVwICs9IDE7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvdyA9IHJvd3NbaV07XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvd0tleSA9IHRoaXMuZ2V0UmVjb3JkS2V5KHJvdywgaSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNoZWNrYm94UHJvcHMgPSB0aGlzLmdldENoZWNrYm94UHJvcHNCeUl0ZW0ocm93LCBpKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjaGVja2JveFByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb3dLZXlzLmluY2x1ZGVzKHJvd0tleSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRSb3dLZXlzID0gc2VsZWN0ZWRSb3dLZXlzLmZpbHRlcigoaikgPT4gcm93S2V5ICE9PSBqKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlUm93S2V5cy5wdXNoKHJvd0tleSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoY2hlY2tlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkUm93S2V5cy5wdXNoKHJvd0tleSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlUm93S2V5cy5wdXNoKHJvd0tleSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHBpdm90OiByZWFsSW5kZXggfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wcy5zdG9yZS5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiB0cnVlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U2VsZWN0ZWRSb3dLZXlzKHNlbGVjdGVkUm93S2V5cywge1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3RXYXk6ICdvblNlbGVjdE11bHRpcGxlJyxcbiAgICAgICAgICAgICAgICAgICAgcmVjb3JkLFxuICAgICAgICAgICAgICAgICAgICBjaGVja2VkLFxuICAgICAgICAgICAgICAgICAgICBjaGFuZ2VSb3dLZXlzLFxuICAgICAgICAgICAgICAgICAgICBuYXRpdmVFdmVudCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmIChjaGVja2VkKSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkUm93S2V5cy5wdXNoKHRoaXMuZ2V0UmVjb3JkS2V5KHJlY29yZCwgcmVhbEluZGV4KSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFJvd0tleXMgPSBzZWxlY3RlZFJvd0tleXMuZmlsdGVyKChpKSA9PiBrZXkgIT09IGkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcGl2b3Q6IHJlYWxJbmRleCB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0aW9uRGlydHk6IHRydWUsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTZWxlY3RlZFJvd0tleXMoc2VsZWN0ZWRSb3dLZXlzLCB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdFdheTogJ29uU2VsZWN0JyxcbiAgICAgICAgICAgICAgICAgICAgcmVjb3JkLFxuICAgICAgICAgICAgICAgICAgICBjaGVja2VkLFxuICAgICAgICAgICAgICAgICAgICBjaGFuZ2VSb3dLZXlzOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgIG5hdGl2ZUV2ZW50LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZVJhZGlvU2VsZWN0ID0gKHJlY29yZCwgcm93SW5kZXgsIGUpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGNoZWNrZWQgPSBlLnRhcmdldC5jaGVja2VkO1xuICAgICAgICAgICAgY29uc3QgbmF0aXZlRXZlbnQgPSBlLm5hdGl2ZUV2ZW50O1xuICAgICAgICAgICAgY29uc3Qga2V5ID0gdGhpcy5nZXRSZWNvcmRLZXkocmVjb3JkLCByb3dJbmRleCk7XG4gICAgICAgICAgICBjb25zdCBzZWxlY3RlZFJvd0tleXMgPSBba2V5XTtcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aGlzLnNldFNlbGVjdGVkUm93S2V5cyhzZWxlY3RlZFJvd0tleXMsIHtcbiAgICAgICAgICAgICAgICBzZWxlY3RXYXk6ICdvblNlbGVjdCcsXG4gICAgICAgICAgICAgICAgcmVjb3JkLFxuICAgICAgICAgICAgICAgIGNoZWNrZWQsXG4gICAgICAgICAgICAgICAgY2hhbmdlUm93S2V5czogdW5kZWZpbmVkLFxuICAgICAgICAgICAgICAgIG5hdGl2ZUV2ZW50LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuaGFuZGxlU2VsZWN0Um93ID0gKHNlbGVjdGlvbktleSwgaW5kZXgsIG9uU2VsZWN0RnVuYykgPT4ge1xuICAgICAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZ2V0RmxhdEN1cnJlbnRQYWdlRGF0YSgpO1xuICAgICAgICAgICAgY29uc3QgZGVmYXVsdFNlbGVjdGlvbiA9IHRoaXMucHJvcHMuc3RvcmUuZ2V0U3RhdGUoKS5zZWxlY3Rpb25EaXJ0eVxuICAgICAgICAgICAgICAgID8gW11cbiAgICAgICAgICAgICAgICA6IHRoaXMuZ2V0RGVmYXVsdFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb3dLZXlzID0gdGhpcy5wcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLnNlbGVjdGVkUm93S2V5cy5jb25jYXQoZGVmYXVsdFNlbGVjdGlvbik7XG4gICAgICAgICAgICBjb25zdCBjaGFuZ2VhYmxlUm93S2V5cyA9IGRhdGFcbiAgICAgICAgICAgICAgICAuZmlsdGVyKChpdGVtLCBpKSA9PiAhdGhpcy5nZXRDaGVja2JveFByb3BzQnlJdGVtKGl0ZW0sIGkpLmRpc2FibGVkKVxuICAgICAgICAgICAgICAgIC5tYXAoKGl0ZW0sIGkpID0+IHRoaXMuZ2V0UmVjb3JkS2V5KGl0ZW0sIGkpKTtcbiAgICAgICAgICAgIGNvbnN0IGNoYW5nZVJvd0tleXMgPSBbXTtcbiAgICAgICAgICAgIGxldCBzZWxlY3RXYXkgPSAnb25TZWxlY3RBbGwnO1xuICAgICAgICAgICAgbGV0IGNoZWNrZWQ7XG4gICAgICAgICAgICAvLyBoYW5kbGUgZGVmYXVsdCBzZWxlY3Rpb25cbiAgICAgICAgICAgIHN3aXRjaCAoc2VsZWN0aW9uS2V5KSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnYWxsJzpcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlYWJsZVJvd0tleXMuZm9yRWFjaChrZXkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93S2V5cy5pbmRleE9mKGtleSkgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRSb3dLZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2VSb3dLZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdFdheSA9ICdvblNlbGVjdEFsbCc7XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdyZW1vdmVBbGwnOlxuICAgICAgICAgICAgICAgICAgICBjaGFuZ2VhYmxlUm93S2V5cy5mb3JFYWNoKGtleSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb3dLZXlzLmluZGV4T2Yoa2V5KSA+PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRSb3dLZXlzLnNwbGljZShzZWxlY3RlZFJvd0tleXMuaW5kZXhPZihrZXkpLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2VSb3dLZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdFdheSA9ICdvblNlbGVjdEFsbCc7XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnaW52ZXJ0JzpcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlYWJsZVJvd0tleXMuZm9yRWFjaChrZXkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93S2V5cy5pbmRleE9mKGtleSkgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRSb3dLZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkUm93S2V5cy5zcGxpY2Uoc2VsZWN0ZWRSb3dLZXlzLmluZGV4T2Yoa2V5KSwgMSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2VSb3dLZXlzLnB1c2goa2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdFdheSA9ICdvblNlbGVjdEludmVydCc7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAvLyB3aGVuIHNlbGVjdCBjdXN0b20gc2VsZWN0aW9uLCBjYWxsYmFjayBzZWxlY3Rpb25zW25dLm9uU2VsZWN0XG4gICAgICAgICAgICBjb25zdCB7IHJvd1NlbGVjdGlvbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGxldCBjdXN0b21TZWxlY3Rpb25TdGFydEluZGV4ID0gMjtcbiAgICAgICAgICAgIGlmIChyb3dTZWxlY3Rpb24gJiYgcm93U2VsZWN0aW9uLmhpZGVEZWZhdWx0U2VsZWN0aW9ucykge1xuICAgICAgICAgICAgICAgIGN1c3RvbVNlbGVjdGlvblN0YXJ0SW5kZXggPSAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGluZGV4ID49IGN1c3RvbVNlbGVjdGlvblN0YXJ0SW5kZXggJiYgdHlwZW9mIG9uU2VsZWN0RnVuYyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIHJldHVybiBvblNlbGVjdEZ1bmMoY2hhbmdlYWJsZVJvd0tleXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5zZXRTZWxlY3RlZFJvd0tleXMoc2VsZWN0ZWRSb3dLZXlzLCB7XG4gICAgICAgICAgICAgICAgc2VsZWN0V2F5LFxuICAgICAgICAgICAgICAgIGNoZWNrZWQsXG4gICAgICAgICAgICAgICAgY2hhbmdlUm93S2V5cyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZVBhZ2VDaGFuZ2UgPSAoY3VycmVudCwgLi4ub3RoZXJBcmd1bWVudHMpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHByb3BzID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IHBhZ2luYXRpb24gPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlLnBhZ2luYXRpb24pO1xuICAgICAgICAgICAgaWYgKGN1cnJlbnQpIHtcbiAgICAgICAgICAgICAgICBwYWdpbmF0aW9uLmN1cnJlbnQgPSBjdXJyZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgcGFnaW5hdGlvbi5jdXJyZW50ID0gcGFnaW5hdGlvbi5jdXJyZW50IHx8IDE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBwYWdpbmF0aW9uLm9uQ2hhbmdlKHBhZ2luYXRpb24uY3VycmVudCwgLi4ub3RoZXJBcmd1bWVudHMpO1xuICAgICAgICAgICAgY29uc3QgbmV3U3RhdGUgPSB7XG4gICAgICAgICAgICAgICAgcGFnaW5hdGlvbixcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAvLyBDb250cm9sbGVkIGN1cnJlbnQgcHJvcCB3aWxsIG5vdCByZXNwb25kIHVzZXIgaW50ZXJhY3Rpb25cbiAgICAgICAgICAgIGlmIChwcm9wcy5wYWdpbmF0aW9uICYmIHR5cGVvZiBwcm9wcy5wYWdpbmF0aW9uID09PSAnb2JqZWN0JyAmJiAnY3VycmVudCcgaW4gcHJvcHMucGFnaW5hdGlvbikge1xuICAgICAgICAgICAgICAgIG5ld1N0YXRlLnBhZ2luYXRpb24gPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHBhZ2luYXRpb24pLCB7IGN1cnJlbnQ6IHRoaXMuc3RhdGUucGFnaW5hdGlvbi5jdXJyZW50IH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSwgdGhpcy5zY3JvbGxUb0ZpcnN0Um93KTtcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiBmYWxzZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc3QgeyBvbkNoYW5nZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGlmIChvbkNoYW5nZSkge1xuICAgICAgICAgICAgICAgIG9uQ2hhbmdlLmFwcGx5KG51bGwsIHRoaXMucHJlcGFyZVBhcmFtc0FyZ3VtZW50cyhPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHRoaXMuc3RhdGUpLCB7IHNlbGVjdGlvbkRpcnR5OiBmYWxzZSwgcGFnaW5hdGlvbiB9KSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLmhhbmRsZVNob3dTaXplQ2hhbmdlID0gKGN1cnJlbnQsIHBhZ2VTaXplKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IHBhZ2luYXRpb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgICAgICBwYWdpbmF0aW9uLm9uU2hvd1NpemVDaGFuZ2UoY3VycmVudCwgcGFnZVNpemUpO1xuICAgICAgICAgICAgY29uc3QgbmV4dFBhZ2luYXRpb24gPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIHBhZ2luYXRpb24pLCB7IHBhZ2VTaXplLFxuICAgICAgICAgICAgICAgIGN1cnJlbnQgfSk7XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcGFnaW5hdGlvbjogbmV4dFBhZ2luYXRpb24gfSwgdGhpcy5zY3JvbGxUb0ZpcnN0Um93KTtcbiAgICAgICAgICAgIGNvbnN0IHsgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBpZiAob25DaGFuZ2UpIHtcbiAgICAgICAgICAgICAgICBvbkNoYW5nZS5hcHBseShudWxsLCB0aGlzLnByZXBhcmVQYXJhbXNBcmd1bWVudHMoT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlKSwgeyBwYWdpbmF0aW9uOiBuZXh0UGFnaW5hdGlvbiB9KSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlckV4cGFuZEljb24gPSAocHJlZml4Q2xzKSA9PiAoeyBleHBhbmRhYmxlLCBleHBhbmRlZCwgbmVlZEluZGVudFNwYWNlZCwgcmVjb3JkLCBvbkV4cGFuZCwgfSkgPT4ge1xuICAgICAgICAgICAgaWYgKGV4cGFuZGFibGUpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKDxMb2NhbGVSZWNlaXZlciBjb21wb25lbnROYW1lPVwiVGFibGVcIiBkZWZhdWx0TG9jYWxlPXtkZWZhdWx0TG9jYWxlLlRhYmxlfT5cbiAgICAgICAgICB7KGxvY2FsZSkgPT4gKDxUcmFuc0J1dHRvbiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1yb3ctZXhwYW5kLWljb25gLCB7XG4gICAgICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXJvdy1jb2xsYXBzZWRgXTogIWV4cGFuZGVkLFxuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1yb3ctZXhwYW5kZWRgXTogZXhwYW5kZWQsXG4gICAgICAgICAgICAgICAgfSl9IG9uQ2xpY2s9e2V2ZW50ID0+IHtcbiAgICAgICAgICAgICAgICAgICAgb25FeHBhbmQocmVjb3JkLCBldmVudCk7XG4gICAgICAgICAgICAgICAgfX0gYXJpYS1sYWJlbD17ZXhwYW5kZWQgPyBsb2NhbGUuY29sbGFwc2UgOiBsb2NhbGUuZXhwYW5kfSBub1N0eWxlLz4pfVxuICAgICAgICA8L0xvY2FsZVJlY2VpdmVyPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAobmVlZEluZGVudFNwYWNlZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiA8c3BhbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tcm93LWV4cGFuZC1pY29uICR7cHJlZml4Q2xzfS1yb3ctc3BhY2VkYH0vPjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnJlbmRlclNlbGVjdGlvbkJveCA9ICh0eXBlKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gKF8sIHJlY29yZCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCByb3dLZXkgPSB0aGlzLmdldFJlY29yZEtleShyZWNvcmQsIGluZGV4KTtcbiAgICAgICAgICAgICAgICBjb25zdCBwcm9wcyA9IHRoaXMuZ2V0Q2hlY2tib3hQcm9wc0J5SXRlbShyZWNvcmQsIGluZGV4KTtcbiAgICAgICAgICAgICAgICBjb25zdCBoYW5kbGVDaGFuZ2UgPSAoZSkgPT4gdHlwZSA9PT0gJ3JhZGlvJ1xuICAgICAgICAgICAgICAgICAgICA/IHRoaXMuaGFuZGxlUmFkaW9TZWxlY3QocmVjb3JkLCBpbmRleCwgZSlcbiAgICAgICAgICAgICAgICAgICAgOiB0aGlzLmhhbmRsZVNlbGVjdChyZWNvcmQsIGluZGV4LCBlKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gKDxzcGFuIG9uQ2xpY2s9e3N0b3BQcm9wYWdhdGlvbn0+XG4gICAgICAgICAgPFNlbGVjdGlvbkJveCB0eXBlPXt0eXBlfSBzdG9yZT17dGhpcy5wcm9wcy5zdG9yZX0gcm93SW5kZXg9e3Jvd0tleX0gb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX0gZGVmYXVsdFNlbGVjdGlvbj17dGhpcy5nZXREZWZhdWx0U2VsZWN0aW9uKCl9IHsuLi5wcm9wc30vPlxuICAgICAgICA8L3NwYW4+KTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyVGFibGUgPSAoeyBwcmVmaXhDbHMsIHJlbmRlckVtcHR5LCBkcm9wZG93blByZWZpeENscywgY29udGV4dExvY2FsZSwgZ2V0UG9wdXBDb250YWluZXI6IGNvbnRleHRHZXRQb3B1cENvbnRhaW5lciwgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgX2EgPSB0aGlzLnByb3BzLCB7IHNob3dIZWFkZXIsIGxvY2FsZSwgZ2V0UG9wdXBDb250YWluZXIgfSA9IF9hLCByZXN0VGFibGVQcm9wcyA9IF9fcmVzdChfYSwgW1wic2hvd0hlYWRlclwiLCBcImxvY2FsZVwiLCBcImdldFBvcHVwQ29udGFpbmVyXCJdKTtcbiAgICAgICAgICAgIC8vIGRvIG5vdCBwYXNzIHByb3Auc3R5bGUgdG8gcmMtdGFibGUsIHNpbmNlIGFscmVhZHkgYXBwbHkgaXQgdG8gY29udGFpbmVyIGRpdlxuICAgICAgICAgICAgY29uc3QgcmVzdFByb3BzID0gb21pdChyZXN0VGFibGVQcm9wcywgWydzdHlsZSddKTtcbiAgICAgICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmdldEN1cnJlbnRQYWdlRGF0YSgpO1xuICAgICAgICAgICAgY29uc3QgZXhwYW5kSWNvbkFzQ2VsbCA9IHRoaXMucHJvcHMuZXhwYW5kZWRSb3dSZW5kZXIgJiYgdGhpcy5wcm9wcy5leHBhbmRJY29uQXNDZWxsICE9PSBmYWxzZTtcbiAgICAgICAgICAgIC8vIHVzZSBwcm9wcy5nZXRQb3B1cENvbnRhaW5lciBmaXJzdFxuICAgICAgICAgICAgY29uc3QgcmVhbEdldFBvcHVwQ29udGFpbmVyID0gZ2V0UG9wdXBDb250YWluZXIgfHwgY29udGV4dEdldFBvcHVwQ29udGFpbmVyO1xuICAgICAgICAgICAgLy8gTWVyZ2UgdG9vIGxvY2FsZXNcbiAgICAgICAgICAgIGNvbnN0IG1lcmdlZExvY2FsZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgY29udGV4dExvY2FsZSksIGxvY2FsZSk7XG4gICAgICAgICAgICBpZiAoIWxvY2FsZSB8fCAhbG9jYWxlLmVtcHR5VGV4dCkge1xuICAgICAgICAgICAgICAgIG1lcmdlZExvY2FsZS5lbXB0eVRleHQgPSByZW5kZXJFbXB0eSgnVGFibGUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IGNsYXNzU3RyaW5nID0gY2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LSR7dGhpcy5wcm9wcy5zaXplfWAsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1ib3JkZXJlZGBdOiB0aGlzLnByb3BzLmJvcmRlcmVkLFxuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWVtcHR5YF06ICFkYXRhLmxlbmd0aCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS13aXRob3V0LWNvbHVtbi1oZWFkZXJgXTogIXNob3dIZWFkZXIsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGNvbnN0IGNvbHVtbnNXaXRoUm93U2VsZWN0aW9uID0gdGhpcy5yZW5kZXJSb3dTZWxlY3Rpb24oe1xuICAgICAgICAgICAgICAgIHByZWZpeENscyxcbiAgICAgICAgICAgICAgICBsb2NhbGU6IG1lcmdlZExvY2FsZSxcbiAgICAgICAgICAgICAgICBnZXRQb3B1cENvbnRhaW5lcjogcmVhbEdldFBvcHVwQ29udGFpbmVyLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBjb2x1bW5zID0gdGhpcy5yZW5kZXJDb2x1bW5zRHJvcGRvd24oe1xuICAgICAgICAgICAgICAgIGNvbHVtbnM6IGNvbHVtbnNXaXRoUm93U2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIHByZWZpeENscyxcbiAgICAgICAgICAgICAgICBkcm9wZG93blByZWZpeENscyxcbiAgICAgICAgICAgICAgICBsb2NhbGU6IG1lcmdlZExvY2FsZSxcbiAgICAgICAgICAgICAgICBnZXRQb3B1cENvbnRhaW5lcjogcmVhbEdldFBvcHVwQ29udGFpbmVyLFxuICAgICAgICAgICAgfSkubWFwKChjb2x1bW4sIGkpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdDb2x1bW4gPSBPYmplY3QuYXNzaWduKHt9LCBjb2x1bW4pO1xuICAgICAgICAgICAgICAgIG5ld0NvbHVtbi5rZXkgPSBnZXRDb2x1bW5LZXkobmV3Q29sdW1uLCBpKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3Q29sdW1uO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBsZXQgZXhwYW5kSWNvbkNvbHVtbkluZGV4ID0gY29sdW1uc1swXSAmJiBjb2x1bW5zWzBdLmtleSA9PT0gJ3NlbGVjdGlvbi1jb2x1bW4nID8gMSA6IDA7XG4gICAgICAgICAgICBpZiAoJ2V4cGFuZEljb25Db2x1bW5JbmRleCcgaW4gcmVzdFByb3BzKSB7XG4gICAgICAgICAgICAgICAgZXhwYW5kSWNvbkNvbHVtbkluZGV4ID0gcmVzdFByb3BzLmV4cGFuZEljb25Db2x1bW5JbmRleDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiAoPFJjVGFibGUgcmVmPXt0aGlzLnNldFRhYmxlUmVmfSBrZXk9XCJ0YWJsZVwiIGV4cGFuZEljb249e3RoaXMucmVuZGVyRXhwYW5kSWNvbihwcmVmaXhDbHMpfSB7Li4ucmVzdFByb3BzfSBvblJvdz17KHJlY29yZCwgaW5kZXgpID0+IHRoaXMub25Sb3cocHJlZml4Q2xzLCByZWNvcmQsIGluZGV4KX0gY29tcG9uZW50cz17dGhpcy5zdGF0ZS5jb21wb25lbnRzfSBwcmVmaXhDbHM9e3ByZWZpeENsc30gZGF0YT17ZGF0YX0gY29sdW1ucz17Y29sdW1uc30gc2hvd0hlYWRlcj17c2hvd0hlYWRlcn0gY2xhc3NOYW1lPXtjbGFzc1N0cmluZ30gZXhwYW5kSWNvbkNvbHVtbkluZGV4PXtleHBhbmRJY29uQ29sdW1uSW5kZXh9IGV4cGFuZEljb25Bc0NlbGw9e2V4cGFuZEljb25Bc0NlbGx9IGVtcHR5VGV4dD17bWVyZ2VkTG9jYWxlLmVtcHR5VGV4dH0vPik7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyQ29tcG9uZW50ID0gKHsgZ2V0UHJlZml4Q2xzLCByZW5kZXJFbXB0eSwgZ2V0UG9wdXBDb250YWluZXIgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgZHJvcGRvd25QcmVmaXhDbHM6IGN1c3RvbWl6ZURyb3Bkb3duUHJlZml4Q2xzLCBzdHlsZSwgY2xhc3NOYW1lLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmdldEN1cnJlbnRQYWdlRGF0YSgpO1xuICAgICAgICAgICAgbGV0IGxvYWRpbmcgPSB0aGlzLnByb3BzLmxvYWRpbmc7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGxvYWRpbmcgPT09ICdib29sZWFuJykge1xuICAgICAgICAgICAgICAgIGxvYWRpbmcgPSB7XG4gICAgICAgICAgICAgICAgICAgIHNwaW5uaW5nOiBsb2FkaW5nLFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBwcmVmaXhDbHMgPSBnZXRQcmVmaXhDbHMoJ3RhYmxlJywgY3VzdG9taXplUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IGRyb3Bkb3duUHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdkcm9wZG93bicsIGN1c3RvbWl6ZURyb3Bkb3duUHJlZml4Q2xzKTtcbiAgICAgICAgICAgIGNvbnN0IHRhYmxlID0gKDxMb2NhbGVSZWNlaXZlciBjb21wb25lbnROYW1lPVwiVGFibGVcIiBkZWZhdWx0TG9jYWxlPXtkZWZhdWx0TG9jYWxlLlRhYmxlfT5cbiAgICAgICAge2xvY2FsZSA9PiB0aGlzLnJlbmRlclRhYmxlKHtcbiAgICAgICAgICAgICAgICBwcmVmaXhDbHMsXG4gICAgICAgICAgICAgICAgcmVuZGVyRW1wdHksXG4gICAgICAgICAgICAgICAgZHJvcGRvd25QcmVmaXhDbHMsXG4gICAgICAgICAgICAgICAgY29udGV4dExvY2FsZTogbG9jYWxlLFxuICAgICAgICAgICAgICAgIGdldFBvcHVwQ29udGFpbmVyLFxuICAgICAgICAgICAgfSl9XG4gICAgICA8L0xvY2FsZVJlY2VpdmVyPik7XG4gICAgICAgICAgICAvLyBpZiB0aGVyZSBpcyBubyBwYWdpbmF0aW9uIG9yIG5vIGRhdGEsXG4gICAgICAgICAgICAvLyB0aGUgaGVpZ2h0IG9mIHNwaW4gc2hvdWxkIGRlY3JlYXNlIGJ5IGhhbGYgb2YgcGFnaW5hdGlvblxuICAgICAgICAgICAgY29uc3QgcGFnaW5hdGlvblBhdGNoQ2xhc3MgPSB0aGlzLmhhc1BhZ2luYXRpb24oKSAmJiBkYXRhICYmIGRhdGEubGVuZ3RoICE9PSAwXG4gICAgICAgICAgICAgICAgPyBgJHtwcmVmaXhDbHN9LXdpdGgtcGFnaW5hdGlvbmBcbiAgICAgICAgICAgICAgICA6IGAke3ByZWZpeENsc30td2l0aG91dC1wYWdpbmF0aW9uYDtcbiAgICAgICAgICAgIHJldHVybiAoPGRpdiBjbGFzc05hbWU9e2NsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS13cmFwcGVyYCwgY2xhc3NOYW1lKX0gc3R5bGU9e3N0eWxlfT5cbiAgICAgICAgPFNwaW4gey4uLmxvYWRpbmd9IGNsYXNzTmFtZT17bG9hZGluZy5zcGlubmluZyA/IGAke3BhZ2luYXRpb25QYXRjaENsYXNzfSAke3ByZWZpeENsc30tc3Bpbi1ob2xkZXJgIDogJyd9PlxuICAgICAgICAgIHt0aGlzLnJlbmRlclBhZ2luYXRpb24ocHJlZml4Q2xzLCAndG9wJyl9XG4gICAgICAgICAge3RhYmxlfVxuICAgICAgICAgIHt0aGlzLnJlbmRlclBhZ2luYXRpb24ocHJlZml4Q2xzLCAnYm90dG9tJyl9XG4gICAgICAgIDwvU3Bpbj5cbiAgICAgIDwvZGl2Pik7XG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHsgZXhwYW5kZWRSb3dSZW5kZXIsIGNvbHVtbnM6IGNvbHVtbnNQcm9wIH0gPSBwcm9wcztcbiAgICAgICAgd2FybmluZyghKCdjb2x1bW5zUGFnZVJhbmdlJyBpbiBwcm9wcyB8fCAnY29sdW1uc1BhZ2VTaXplJyBpbiBwcm9wcyksICdUYWJsZScsICdgY29sdW1uc1BhZ2VSYW5nZWAgYW5kIGBjb2x1bW5zUGFnZVNpemVgIGFyZSByZW1vdmVkLCBwbGVhc2UgdXNlICcgK1xuICAgICAgICAgICAgJ2ZpeGVkIGNvbHVtbnMgaW5zdGVhZCwgc2VlOiBodHRwczovL3UuYW50LmRlc2lnbi9maXhlZC1jb2x1bW5zLicpO1xuICAgICAgICBpZiAoZXhwYW5kZWRSb3dSZW5kZXIgJiYgKGNvbHVtbnNQcm9wIHx8IFtdKS5zb21lKCh7IGZpeGVkIH0pID0+ICEhZml4ZWQpKSB7XG4gICAgICAgICAgICB3YXJuaW5nKGZhbHNlLCAnVGFibGUnLCAnYGV4cGFuZGVkUm93UmVuZGVyYCBhbmQgYENvbHVtbi5maXhlZGAgYXJlIG5vdCBjb21wYXRpYmxlLiBQbGVhc2UgdXNlIG9uZSBvZiB0aGVtIGF0IG9uZSB0aW1lLicpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGNvbHVtbnMgPSBjb2x1bW5zUHJvcCB8fCBub3JtYWxpemVDb2x1bW5zKHByb3BzLmNoaWxkcmVuKTtcbiAgICAgICAgdGhpcy5zdGF0ZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5nZXREZWZhdWx0U29ydE9yZGVyKGNvbHVtbnMgfHwgW10pKSwgeyBcbiAgICAgICAgICAgIC8vIOWHj+WwkeeKtuaAgVxuICAgICAgICAgICAgZmlsdGVyczogdGhpcy5nZXREZWZhdWx0RmlsdGVycyhjb2x1bW5zKSwgcGFnaW5hdGlvbjogdGhpcy5nZXREZWZhdWx0UGFnaW5hdGlvbihwcm9wcyksIHBpdm90OiB1bmRlZmluZWQsIHByZXZQcm9wczogcHJvcHMsIGNvbXBvbmVudHM6IGNyZWF0ZUNvbXBvbmVudHMocHJvcHMuY29tcG9uZW50cyksIGNvbHVtbnMgfSk7XG4gICAgfVxuICAgIHN0YXRpYyBnZXREZXJpdmVkU3RhdGVGcm9tUHJvcHMobmV4dFByb3BzLCBwcmV2U3RhdGUpIHtcbiAgICAgICAgY29uc3QgeyBwcmV2UHJvcHMgfSA9IHByZXZTdGF0ZTtcbiAgICAgICAgY29uc3QgY29sdW1ucyA9IG5leHRQcm9wcy5jb2x1bW5zIHx8IG5vcm1hbGl6ZUNvbHVtbnMobmV4dFByb3BzLmNoaWxkcmVuKTtcbiAgICAgICAgbGV0IG5leHRTdGF0ZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgcHJldlN0YXRlKSwgeyBwcmV2UHJvcHM6IG5leHRQcm9wcywgY29sdW1ucyB9KTtcbiAgICAgICAgaWYgKCdwYWdpbmF0aW9uJyBpbiBuZXh0UHJvcHMgfHwgJ3BhZ2luYXRpb24nIGluIHByZXZQcm9wcykge1xuICAgICAgICAgICAgY29uc3QgbmV3UGFnaW5hdGlvbiA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0UGFnaW5hdGlvbiksIHByZXZTdGF0ZS5wYWdpbmF0aW9uKSwgbmV4dFByb3BzLnBhZ2luYXRpb24pO1xuICAgICAgICAgICAgbmV3UGFnaW5hdGlvbi5jdXJyZW50ID0gbmV3UGFnaW5hdGlvbi5jdXJyZW50IHx8IDE7XG4gICAgICAgICAgICBuZXdQYWdpbmF0aW9uLnBhZ2VTaXplID0gbmV3UGFnaW5hdGlvbi5wYWdlU2l6ZSB8fCAxMDtcbiAgICAgICAgICAgIG5leHRTdGF0ZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgbmV4dFN0YXRlKSwgeyBwYWdpbmF0aW9uOiBuZXh0UHJvcHMucGFnaW5hdGlvbiAhPT0gZmFsc2UgPyBuZXdQYWdpbmF0aW9uIDogZW1wdHlPYmplY3QgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKG5leHRQcm9wcy5yb3dTZWxlY3Rpb24gJiYgJ3NlbGVjdGVkUm93S2V5cycgaW4gbmV4dFByb3BzLnJvd1NlbGVjdGlvbikge1xuICAgICAgICAgICAgbmV4dFByb3BzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBzZWxlY3RlZFJvd0tleXM6IG5leHRQcm9wcy5yb3dTZWxlY3Rpb24uc2VsZWN0ZWRSb3dLZXlzIHx8IFtdLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAocHJldlByb3BzLnJvd1NlbGVjdGlvbiAmJiAhbmV4dFByb3BzLnJvd1NlbGVjdGlvbikge1xuICAgICAgICAgICAgbmV4dFByb3BzLnN0b3JlLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBzZWxlY3RlZFJvd0tleXM6IFtdLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCdkYXRhU291cmNlJyBpbiBuZXh0UHJvcHMgJiYgbmV4dFByb3BzLmRhdGFTb3VyY2UgIT09IHByZXZQcm9wcy5kYXRhU291cmNlKSB7XG4gICAgICAgICAgICBuZXh0UHJvcHMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiBmYWxzZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzEwMTMzXG4gICAgICAgIG5leHRQcm9wcy5zZXRDaGVja2JveFByb3BzQ2FjaGUoe30pO1xuICAgICAgICAvLyBVcGRhdGUgZmlsdGVyc1xuICAgICAgICBjb25zdCBmaWx0ZXJlZFZhbHVlQ29sdW1ucyA9IGdldEZpbHRlcmVkVmFsdWVDb2x1bW5zKG5leHRTdGF0ZSwgbmV4dFN0YXRlLmNvbHVtbnMpO1xuICAgICAgICBpZiAoZmlsdGVyZWRWYWx1ZUNvbHVtbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgY29uc3QgZmlsdGVyc0Zyb21Db2x1bW5zID0gZ2V0RmlsdGVyc0Zyb21Db2x1bW5zKG5leHRTdGF0ZSwgbmV4dFN0YXRlLmNvbHVtbnMpO1xuICAgICAgICAgICAgY29uc3QgbmV3RmlsdGVycyA9IE9iamVjdC5hc3NpZ24oe30sIG5leHRTdGF0ZS5maWx0ZXJzKTtcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKGZpbHRlcnNGcm9tQ29sdW1ucykuZm9yRWFjaChrZXkgPT4ge1xuICAgICAgICAgICAgICAgIG5ld0ZpbHRlcnNba2V5XSA9IGZpbHRlcnNGcm9tQ29sdW1uc1trZXldO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZiAoaXNGaWx0ZXJzQ2hhbmdlZChuZXh0U3RhdGUsIG5ld0ZpbHRlcnMpKSB7XG4gICAgICAgICAgICAgICAgbmV4dFN0YXRlID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBuZXh0U3RhdGUpLCB7IGZpbHRlcnM6IG5ld0ZpbHRlcnMgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCFpc1RoZVNhbWVDb21wb25lbnRzKG5leHRQcm9wcy5jb21wb25lbnRzLCBwcmV2UHJvcHMuY29tcG9uZW50cykpIHtcbiAgICAgICAgICAgIGNvbnN0IGNvbXBvbmVudHMgPSBjcmVhdGVDb21wb25lbnRzKG5leHRQcm9wcy5jb21wb25lbnRzKTtcbiAgICAgICAgICAgIG5leHRTdGF0ZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgbmV4dFN0YXRlKSwgeyBjb21wb25lbnRzIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBuZXh0U3RhdGU7XG4gICAgfVxuICAgIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgICAgY29uc3QgeyBjb2x1bW5zLCBzb3J0Q29sdW1uLCBzb3J0T3JkZXIgfSA9IHRoaXMuc3RhdGU7XG4gICAgICAgIGlmICh0aGlzLmdldFNvcnRPcmRlckNvbHVtbnMoY29sdW1ucykubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgY29uc3Qgc29ydFN0YXRlID0gdGhpcy5nZXRTb3J0U3RhdGVGcm9tQ29sdW1ucyhjb2x1bW5zKTtcbiAgICAgICAgICAgIGlmICghaXNTYW1lQ29sdW1uKHNvcnRTdGF0ZS5zb3J0Q29sdW1uLCBzb3J0Q29sdW1uKSB8fCBzb3J0U3RhdGUuc29ydE9yZGVyICE9PSBzb3J0T3JkZXIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHNvcnRTdGF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgZ2V0RGVmYXVsdFNlbGVjdGlvbigpIHtcbiAgICAgICAgY29uc3Qgcm93U2VsZWN0aW9uID0gZ2V0Um93U2VsZWN0aW9uKHRoaXMucHJvcHMpO1xuICAgICAgICBpZiAoIXJvd1NlbGVjdGlvbi5nZXRDaGVja2JveFByb3BzKSB7XG4gICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RmxhdERhdGEoKVxuICAgICAgICAgICAgLmZpbHRlcigoaXRlbSwgcm93SW5kZXgpID0+IHRoaXMuZ2V0Q2hlY2tib3hQcm9wc0J5SXRlbShpdGVtLCByb3dJbmRleCkuZGVmYXVsdENoZWNrZWQpXG4gICAgICAgICAgICAubWFwKChyZWNvcmQsIHJvd0luZGV4KSA9PiB0aGlzLmdldFJlY29yZEtleShyZWNvcmQsIHJvd0luZGV4KSk7XG4gICAgfVxuICAgIGdldERlZmF1bHRQYWdpbmF0aW9uKHByb3BzKSB7XG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb24gPSB0eXBlb2YgcHJvcHMucGFnaW5hdGlvbiA9PT0gJ29iamVjdCcgPyBwcm9wcy5wYWdpbmF0aW9uIDoge307XG4gICAgICAgIGxldCBjdXJyZW50O1xuICAgICAgICBpZiAoJ2N1cnJlbnQnIGluIHBhZ2luYXRpb24pIHtcbiAgICAgICAgICAgIGN1cnJlbnQgPSBwYWdpbmF0aW9uLmN1cnJlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoJ2RlZmF1bHRDdXJyZW50JyBpbiBwYWdpbmF0aW9uKSB7XG4gICAgICAgICAgICBjdXJyZW50ID0gcGFnaW5hdGlvbi5kZWZhdWx0Q3VycmVudDtcbiAgICAgICAgfVxuICAgICAgICBsZXQgcGFnZVNpemU7XG4gICAgICAgIGlmICgncGFnZVNpemUnIGluIHBhZ2luYXRpb24pIHtcbiAgICAgICAgICAgIHBhZ2VTaXplID0gcGFnaW5hdGlvbi5wYWdlU2l6ZTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICgnZGVmYXVsdFBhZ2VTaXplJyBpbiBwYWdpbmF0aW9uKSB7XG4gICAgICAgICAgICBwYWdlU2l6ZSA9IHBhZ2luYXRpb24uZGVmYXVsdFBhZ2VTaXplO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLmhhc1BhZ2luYXRpb24ocHJvcHMpXG4gICAgICAgICAgICA/IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0UGFnaW5hdGlvbiksIHBhZ2luYXRpb24pLCB7IGN1cnJlbnQ6IGN1cnJlbnQgfHwgMSwgcGFnZVNpemU6IHBhZ2VTaXplIHx8IDEwIH0pIDoge307XG4gICAgfVxuICAgIGdldFNvcnRPcmRlckNvbHVtbnMoY29sdW1ucykge1xuICAgICAgICByZXR1cm4gZmxhdEZpbHRlcihjb2x1bW5zIHx8ICh0aGlzLnN0YXRlIHx8IHt9KS5jb2x1bW5zIHx8IFtdLCAoY29sdW1uKSA9PiAnc29ydE9yZGVyJyBpbiBjb2x1bW4pO1xuICAgIH1cbiAgICBnZXREZWZhdWx0RmlsdGVycyhjb2x1bW5zKSB7XG4gICAgICAgIGNvbnN0IGRlZmluZWRGaWx0ZXJzID0gZ2V0RmlsdGVyc0Zyb21Db2x1bW5zKHRoaXMuc3RhdGUsIGNvbHVtbnMpO1xuICAgICAgICBjb25zdCBkZWZhdWx0RmlsdGVyZWRWYWx1ZUNvbHVtbnMgPSBmbGF0RmlsdGVyKGNvbHVtbnMgfHwgW10sIChjb2x1bW4pID0+IHR5cGVvZiBjb2x1bW4uZGVmYXVsdEZpbHRlcmVkVmFsdWUgIT09ICd1bmRlZmluZWQnKTtcbiAgICAgICAgY29uc3QgZGVmYXVsdEZpbHRlcnMgPSBkZWZhdWx0RmlsdGVyZWRWYWx1ZUNvbHVtbnMucmVkdWNlKChzb0ZhciwgY29sKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBjb2xLZXkgPSBnZXRDb2x1bW5LZXkoY29sKTtcbiAgICAgICAgICAgIHNvRmFyW2NvbEtleV0gPSBjb2wuZGVmYXVsdEZpbHRlcmVkVmFsdWU7XG4gICAgICAgICAgICByZXR1cm4gc29GYXI7XG4gICAgICAgIH0sIHt9KTtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgZGVmYXVsdEZpbHRlcnMpLCBkZWZpbmVkRmlsdGVycyk7XG4gICAgfVxuICAgIGdldERlZmF1bHRTb3J0T3JkZXIoY29sdW1ucykge1xuICAgICAgICBjb25zdCBkZWZpbmVkU29ydFN0YXRlID0gdGhpcy5nZXRTb3J0U3RhdGVGcm9tQ29sdW1ucyhjb2x1bW5zKTtcbiAgICAgICAgY29uc3QgZGVmYXVsdFNvcnRlZENvbHVtbiA9IGZsYXRGaWx0ZXIoY29sdW1ucyB8fCBbXSwgKGNvbHVtbikgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIGNvbHVtbi5kZWZhdWx0U29ydE9yZGVyICE9IG51bGw7XG4gICAgICAgIH0pWzBdO1xuICAgICAgICBpZiAoZGVmYXVsdFNvcnRlZENvbHVtbiAmJiAhZGVmaW5lZFNvcnRTdGF0ZS5zb3J0Q29sdW1uKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHNvcnRDb2x1bW46IGRlZmF1bHRTb3J0ZWRDb2x1bW4sXG4gICAgICAgICAgICAgICAgc29ydE9yZGVyOiBkZWZhdWx0U29ydGVkQ29sdW1uLmRlZmF1bHRTb3J0T3JkZXIsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBkZWZpbmVkU29ydFN0YXRlO1xuICAgIH1cbiAgICBnZXRTb3J0U3RhdGVGcm9tQ29sdW1ucyhjb2x1bW5zKSB7XG4gICAgICAgIC8vIHJldHVybiBmaXJzdCBjb2x1bW4gd2hpY2ggc29ydE9yZGVyIGlzIG5vdCBmYWxzeVxuICAgICAgICBjb25zdCBzb3J0ZWRDb2x1bW4gPSB0aGlzLmdldFNvcnRPcmRlckNvbHVtbnMoY29sdW1ucykuZmlsdGVyKChjb2wpID0+IGNvbC5zb3J0T3JkZXIpWzBdO1xuICAgICAgICBpZiAoc29ydGVkQ29sdW1uKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgIHNvcnRDb2x1bW46IHNvcnRlZENvbHVtbixcbiAgICAgICAgICAgICAgICBzb3J0T3JkZXI6IHNvcnRlZENvbHVtbi5zb3J0T3JkZXIsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBzb3J0Q29sdW1uOiBudWxsLFxuICAgICAgICAgICAgc29ydE9yZGVyOiBudWxsLFxuICAgICAgICB9O1xuICAgIH1cbiAgICBnZXRNYXhDdXJyZW50KHRvdGFsKSB7XG4gICAgICAgIGNvbnN0IHsgcGFnaW5hdGlvbjogeyBjdXJyZW50LCBwYWdlU2l6ZSB9LCB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgaWYgKChjdXJyZW50IC0gMSkgKiBwYWdlU2l6ZSA+PSB0b3RhbCkge1xuICAgICAgICAgICAgcmV0dXJuIE1hdGguZmxvb3IoKHRvdGFsIC0gMSkgLyBwYWdlU2l6ZSkgKyAxO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjdXJyZW50O1xuICAgIH1cbiAgICBnZXRTb3J0ZXJGbihzdGF0ZSkge1xuICAgICAgICBjb25zdCB7IHNvcnRPcmRlciwgc29ydENvbHVtbiB9ID0gc3RhdGUgfHwgdGhpcy5zdGF0ZTtcbiAgICAgICAgaWYgKCFzb3J0T3JkZXIgfHwgIXNvcnRDb2x1bW4gfHwgdHlwZW9mIHNvcnRDb2x1bW4uc29ydGVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIChhLCBiKSA9PiB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSBzb3J0Q29sdW1uLnNvcnRlcihhLCBiLCBzb3J0T3JkZXIpO1xuICAgICAgICAgICAgaWYgKHJlc3VsdCAhPT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBzb3J0T3JkZXIgPT09ICdkZXNjZW5kJyA/IC1yZXN1bHQgOiByZXN1bHQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgZ2V0Q3VycmVudFBhZ2VEYXRhKCkge1xuICAgICAgICBsZXQgZGF0YSA9IHRoaXMuZ2V0TG9jYWxEYXRhKCk7XG4gICAgICAgIGxldCBjdXJyZW50O1xuICAgICAgICBsZXQgcGFnZVNpemU7XG4gICAgICAgIGNvbnN0IHN0YXRlID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgLy8g5aaC5p6c5rKh5pyJ5YiG6aG155qE6K+d77yM6buY6K6k5YWo6YOo5bGV56S6XG4gICAgICAgIGlmICghdGhpcy5oYXNQYWdpbmF0aW9uKCkpIHtcbiAgICAgICAgICAgIHBhZ2VTaXplID0gTnVtYmVyLk1BWF9WQUxVRTtcbiAgICAgICAgICAgIGN1cnJlbnQgPSAxO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcGFnZVNpemUgPSBzdGF0ZS5wYWdpbmF0aW9uLnBhZ2VTaXplO1xuICAgICAgICAgICAgY3VycmVudCA9IHRoaXMuZ2V0TWF4Q3VycmVudChzdGF0ZS5wYWdpbmF0aW9uLnRvdGFsIHx8IGRhdGEubGVuZ3RoKTtcbiAgICAgICAgfVxuICAgICAgICAvLyDliIbpobVcbiAgICAgICAgLy8gLS0tXG4gICAgICAgIC8vIOW9k+aVsOaNrumHj+WwkeS6juetieS6juavj+mhteaVsOmHj+aXtu+8jOebtOaOpeiuvue9ruaVsOaNrlxuICAgICAgICAvLyDlkKbliJnov5vooYzor7vlj5bliIbpobXmlbDmja5cbiAgICAgICAgaWYgKGRhdGEubGVuZ3RoID4gcGFnZVNpemUgfHwgcGFnZVNpemUgPT09IE51bWJlci5NQVhfVkFMVUUpIHtcbiAgICAgICAgICAgIGRhdGEgPSBkYXRhLnNsaWNlKChjdXJyZW50IC0gMSkgKiBwYWdlU2l6ZSwgY3VycmVudCAqIHBhZ2VTaXplKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG4gICAgZ2V0RmxhdERhdGEoKSB7XG4gICAgICAgIGNvbnN0IHsgY2hpbGRyZW5Db2x1bW5OYW1lIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4gZmxhdEFycmF5KHRoaXMuZ2V0TG9jYWxEYXRhKG51bGwsIGZhbHNlKSwgY2hpbGRyZW5Db2x1bW5OYW1lKTtcbiAgICB9XG4gICAgZ2V0RmxhdEN1cnJlbnRQYWdlRGF0YSgpIHtcbiAgICAgICAgY29uc3QgeyBjaGlsZHJlbkNvbHVtbk5hbWUgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiBmbGF0QXJyYXkodGhpcy5nZXRDdXJyZW50UGFnZURhdGEoKSwgY2hpbGRyZW5Db2x1bW5OYW1lKTtcbiAgICB9XG4gICAgZ2V0TG9jYWxEYXRhKHN0YXRlLCBmaWx0ZXIgPSB0cnVlKSB7XG4gICAgICAgIGNvbnN0IGN1cnJlbnRTdGF0ZSA9IHN0YXRlIHx8IHRoaXMuc3RhdGU7XG4gICAgICAgIGNvbnN0IHsgZGF0YVNvdXJjZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgbGV0IGRhdGEgPSBkYXRhU291cmNlIHx8IFtdO1xuICAgICAgICAvLyDkvJjljJbmnKzlnLDmjpLluo9cbiAgICAgICAgZGF0YSA9IGRhdGEuc2xpY2UoMCk7XG4gICAgICAgIGNvbnN0IHNvcnRlckZuID0gdGhpcy5nZXRTb3J0ZXJGbihjdXJyZW50U3RhdGUpO1xuICAgICAgICBpZiAoc29ydGVyRm4pIHtcbiAgICAgICAgICAgIGRhdGEgPSB0aGlzLnJlY3Vyc2l2ZVNvcnQoZGF0YSwgc29ydGVyRm4pO1xuICAgICAgICB9XG4gICAgICAgIC8vIOetm+mAiVxuICAgICAgICBpZiAoZmlsdGVyICYmIGN1cnJlbnRTdGF0ZS5maWx0ZXJzKSB7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhjdXJyZW50U3RhdGUuZmlsdGVycykuZm9yRWFjaChjb2x1bW5LZXkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNvbCA9IHRoaXMuZmluZENvbHVtbihjb2x1bW5LZXkpO1xuICAgICAgICAgICAgICAgIGlmICghY29sKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29uc3QgdmFsdWVzID0gY3VycmVudFN0YXRlLmZpbHRlcnNbY29sdW1uS2V5XSB8fCBbXTtcbiAgICAgICAgICAgICAgICBpZiAodmFsdWVzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnN0IG9uRmlsdGVyID0gY29sLm9uRmlsdGVyO1xuICAgICAgICAgICAgICAgIGRhdGEgPSBvbkZpbHRlclxuICAgICAgICAgICAgICAgICAgICA/IGRhdGEuZmlsdGVyKHJlY29yZCA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWVzLnNvbWUodiA9PiBvbkZpbHRlcih2LCByZWNvcmQpKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgOiBkYXRhO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuICAgIHNldFNlbGVjdGVkUm93S2V5cyhzZWxlY3RlZFJvd0tleXMsIHNlbGVjdGlvbkluZm8pIHtcbiAgICAgICAgY29uc3QgeyBzZWxlY3RXYXksIHJlY29yZCwgY2hlY2tlZCwgY2hhbmdlUm93S2V5cywgbmF0aXZlRXZlbnQgfSA9IHNlbGVjdGlvbkluZm87XG4gICAgICAgIGNvbnN0IHJvd1NlbGVjdGlvbiA9IGdldFJvd1NlbGVjdGlvbih0aGlzLnByb3BzKTtcbiAgICAgICAgaWYgKHJvd1NlbGVjdGlvbiAmJiAhKCdzZWxlY3RlZFJvd0tleXMnIGluIHJvd1NlbGVjdGlvbikpIHtcbiAgICAgICAgICAgIHRoaXMucHJvcHMuc3RvcmUuc2V0U3RhdGUoeyBzZWxlY3RlZFJvd0tleXMgfSk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZ2V0RmxhdERhdGEoKTtcbiAgICAgICAgaWYgKCFyb3dTZWxlY3Rpb24ub25DaGFuZ2UgJiYgIXJvd1NlbGVjdGlvbltzZWxlY3RXYXldKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRSb3dzID0gZGF0YS5maWx0ZXIoKHJvdywgaSkgPT4gc2VsZWN0ZWRSb3dLZXlzLmluZGV4T2YodGhpcy5nZXRSZWNvcmRLZXkocm93LCBpKSkgPj0gMCk7XG4gICAgICAgIGlmIChyb3dTZWxlY3Rpb24ub25DaGFuZ2UpIHtcbiAgICAgICAgICAgIHJvd1NlbGVjdGlvbi5vbkNoYW5nZShzZWxlY3RlZFJvd0tleXMsIHNlbGVjdGVkUm93cyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHNlbGVjdFdheSA9PT0gJ29uU2VsZWN0JyAmJiByb3dTZWxlY3Rpb24ub25TZWxlY3QpIHtcbiAgICAgICAgICAgIHJvd1NlbGVjdGlvbi5vblNlbGVjdChyZWNvcmQsIGNoZWNrZWQsIHNlbGVjdGVkUm93cywgbmF0aXZlRXZlbnQpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHNlbGVjdFdheSA9PT0gJ29uU2VsZWN0TXVsdGlwbGUnICYmIHJvd1NlbGVjdGlvbi5vblNlbGVjdE11bHRpcGxlKSB7XG4gICAgICAgICAgICBjb25zdCBjaGFuZ2VSb3dzID0gZGF0YS5maWx0ZXIoKHJvdywgaSkgPT4gY2hhbmdlUm93S2V5cy5pbmRleE9mKHRoaXMuZ2V0UmVjb3JkS2V5KHJvdywgaSkpID49IDApO1xuICAgICAgICAgICAgcm93U2VsZWN0aW9uLm9uU2VsZWN0TXVsdGlwbGUoY2hlY2tlZCwgc2VsZWN0ZWRSb3dzLCBjaGFuZ2VSb3dzKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChzZWxlY3RXYXkgPT09ICdvblNlbGVjdEFsbCcgJiYgcm93U2VsZWN0aW9uLm9uU2VsZWN0QWxsKSB7XG4gICAgICAgICAgICBjb25zdCBjaGFuZ2VSb3dzID0gZGF0YS5maWx0ZXIoKHJvdywgaSkgPT4gY2hhbmdlUm93S2V5cy5pbmRleE9mKHRoaXMuZ2V0UmVjb3JkS2V5KHJvdywgaSkpID49IDApO1xuICAgICAgICAgICAgcm93U2VsZWN0aW9uLm9uU2VsZWN0QWxsKGNoZWNrZWQsIHNlbGVjdGVkUm93cywgY2hhbmdlUm93cyk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZWN0V2F5ID09PSAnb25TZWxlY3RJbnZlcnQnICYmIHJvd1NlbGVjdGlvbi5vblNlbGVjdEludmVydCkge1xuICAgICAgICAgICAgcm93U2VsZWN0aW9uLm9uU2VsZWN0SW52ZXJ0KHNlbGVjdGVkUm93S2V5cyk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgdG9nZ2xlU29ydE9yZGVyKGNvbHVtbikge1xuICAgICAgICBjb25zdCBzb3J0RGlyZWN0aW9ucyA9IGNvbHVtbi5zb3J0RGlyZWN0aW9ucyB8fCB0aGlzLnByb3BzLnNvcnREaXJlY3Rpb25zO1xuICAgICAgICBjb25zdCB7IHNvcnRPcmRlciwgc29ydENvbHVtbiB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgLy8g5Y+q5ZCM5pe25YWB6K645LiA5YiX6L+b6KGM5o6S5bqP77yM5ZCm5YiZ5Lya5a+86Ie05o6S5bqP6aG65bqP55qE6YC76L6R6Zeu6aKYXG4gICAgICAgIGxldCBuZXdTb3J0T3JkZXI7XG4gICAgICAgIC8vIOWIh+aNouWPpuS4gOWIl+aXtu+8jOS4ouW8gyBzb3J0T3JkZXIg55qE54q25oCBXG4gICAgICAgIGlmIChpc1NhbWVDb2x1bW4oc29ydENvbHVtbiwgY29sdW1uKSAmJiBzb3J0T3JkZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgLy8g5oyJ54Wnc29ydERpcmVjdGlvbnPnmoTlhoXlrrnkvp3mrKHliIfmjaLmjpLluo/nirbmgIFcbiAgICAgICAgICAgIGNvbnN0IG1ldGhvZEluZGV4ID0gc29ydERpcmVjdGlvbnMuaW5kZXhPZihzb3J0T3JkZXIpICsgMTtcbiAgICAgICAgICAgIG5ld1NvcnRPcmRlciA9XG4gICAgICAgICAgICAgICAgbWV0aG9kSW5kZXggPT09IHNvcnREaXJlY3Rpb25zLmxlbmd0aCA/IHVuZGVmaW5lZCA6IHNvcnREaXJlY3Rpb25zW21ldGhvZEluZGV4XTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIG5ld1NvcnRPcmRlciA9IHNvcnREaXJlY3Rpb25zWzBdO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xuICAgICAgICAgICAgc29ydE9yZGVyOiBuZXdTb3J0T3JkZXIsXG4gICAgICAgICAgICBzb3J0Q29sdW1uOiBuZXdTb3J0T3JkZXIgPyBjb2x1bW4gOiBudWxsLFxuICAgICAgICB9O1xuICAgICAgICAvLyBDb250cm9sbGVkXG4gICAgICAgIGlmICh0aGlzLmdldFNvcnRPcmRlckNvbHVtbnMoKS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUobmV3U3RhdGUsIHRoaXMuc2Nyb2xsVG9GaXJzdFJvdyk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgeyBvbkNoYW5nZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgICAgICBvbkNoYW5nZS5hcHBseShudWxsLCB0aGlzLnByZXBhcmVQYXJhbXNBcmd1bWVudHMoT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlKSwgbmV3U3RhdGUpLCBjb2x1bW4pKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBoYXNQYWdpbmF0aW9uKHByb3BzKSB7XG4gICAgICAgIHJldHVybiAocHJvcHMgfHwgdGhpcy5wcm9wcykucGFnaW5hdGlvbiAhPT0gZmFsc2U7XG4gICAgfVxuICAgIGlzU29ydENvbHVtbihjb2x1bW4pIHtcbiAgICAgICAgY29uc3QgeyBzb3J0Q29sdW1uIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBpZiAoIWNvbHVtbiB8fCAhc29ydENvbHVtbikge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBnZXRDb2x1bW5LZXkoc29ydENvbHVtbikgPT09IGdldENvbHVtbktleShjb2x1bW4pO1xuICAgIH1cbiAgICAvLyBHZXQgcGFnaW5hdGlvbiwgZmlsdGVycywgc29ydGVyXG4gICAgcHJlcGFyZVBhcmFtc0FyZ3VtZW50cyhzdGF0ZSwgY29sdW1uKSB7XG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb24gPSBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZS5wYWdpbmF0aW9uKTtcbiAgICAgICAgLy8gcmVtb3ZlIHVzZWxlc3MgaGFuZGxlIGZ1bmN0aW9uIGluIFRhYmxlLm9uQ2hhbmdlXG4gICAgICAgIGRlbGV0ZSBwYWdpbmF0aW9uLm9uQ2hhbmdlO1xuICAgICAgICBkZWxldGUgcGFnaW5hdGlvbi5vblNob3dTaXplQ2hhbmdlO1xuICAgICAgICBjb25zdCBmaWx0ZXJzID0gc3RhdGUuZmlsdGVycztcbiAgICAgICAgY29uc3Qgc29ydGVyID0ge307XG4gICAgICAgIGxldCBjdXJyZW50Q29sdW1uID0gY29sdW1uO1xuICAgICAgICBpZiAoc3RhdGUuc29ydENvbHVtbiAmJiBzdGF0ZS5zb3J0T3JkZXIpIHtcbiAgICAgICAgICAgIGN1cnJlbnRDb2x1bW4gPSBzdGF0ZS5zb3J0Q29sdW1uO1xuICAgICAgICAgICAgc29ydGVyLmNvbHVtbiA9IHN0YXRlLnNvcnRDb2x1bW47XG4gICAgICAgICAgICBzb3J0ZXIub3JkZXIgPSBzdGF0ZS5zb3J0T3JkZXI7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGN1cnJlbnRDb2x1bW4pIHtcbiAgICAgICAgICAgIHNvcnRlci5maWVsZCA9IGN1cnJlbnRDb2x1bW4uZGF0YUluZGV4O1xuICAgICAgICAgICAgc29ydGVyLmNvbHVtbktleSA9IGdldENvbHVtbktleShjdXJyZW50Q29sdW1uKTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBleHRyYSA9IHtcbiAgICAgICAgICAgIGN1cnJlbnREYXRhU291cmNlOiB0aGlzLmdldExvY2FsRGF0YShzdGF0ZSksXG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiBbcGFnaW5hdGlvbiwgZmlsdGVycywgc29ydGVyLCBleHRyYV07XG4gICAgfVxuICAgIGZpbmRDb2x1bW4obXlLZXkpIHtcbiAgICAgICAgbGV0IGNvbHVtbjtcbiAgICAgICAgdHJlZU1hcCh0aGlzLnN0YXRlLmNvbHVtbnMsIGMgPT4ge1xuICAgICAgICAgICAgaWYgKGdldENvbHVtbktleShjKSA9PT0gbXlLZXkpIHtcbiAgICAgICAgICAgICAgICBjb2x1bW4gPSBjO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGNvbHVtbjtcbiAgICB9XG4gICAgcmVjdXJzaXZlU29ydChkYXRhLCBzb3J0ZXJGbikge1xuICAgICAgICBjb25zdCB7IGNoaWxkcmVuQ29sdW1uTmFtZSA9ICdjaGlsZHJlbicgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIHJldHVybiBkYXRhLnNvcnQoc29ydGVyRm4pLm1hcCgoaXRlbSkgPT4gaXRlbVtjaGlsZHJlbkNvbHVtbk5hbWVdXG4gICAgICAgICAgICA/IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbih7fSwgaXRlbSksIHsgW2NoaWxkcmVuQ29sdW1uTmFtZV06IHRoaXMucmVjdXJzaXZlU29ydChpdGVtW2NoaWxkcmVuQ29sdW1uTmFtZV0sIHNvcnRlckZuKSB9KSA6IGl0ZW0pO1xuICAgIH1cbiAgICByZW5kZXJQYWdpbmF0aW9uKHByZWZpeENscywgcGFnaW5hdGlvblBvc2l0aW9uKSB7XG4gICAgICAgIC8vIOW8uuWItuS4jemcgOimgeWIhumhtVxuICAgICAgICBpZiAoIXRoaXMuaGFzUGFnaW5hdGlvbigpKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBsZXQgc2l6ZSA9ICdkZWZhdWx0JztcbiAgICAgICAgY29uc3QgeyBwYWdpbmF0aW9uIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICBpZiAocGFnaW5hdGlvbi5zaXplKSB7XG4gICAgICAgICAgICBzaXplID0gcGFnaW5hdGlvbi5zaXplO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMucHJvcHMuc2l6ZSA9PT0gJ21pZGRsZScgfHwgdGhpcy5wcm9wcy5zaXplID09PSAnc21hbGwnKSB7XG4gICAgICAgICAgICBzaXplID0gJ3NtYWxsJztcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBwb3NpdGlvbiA9IHBhZ2luYXRpb24ucG9zaXRpb24gfHwgJ2JvdHRvbSc7XG4gICAgICAgIGNvbnN0IHRvdGFsID0gcGFnaW5hdGlvbi50b3RhbCB8fCB0aGlzLmdldExvY2FsRGF0YSgpLmxlbmd0aDtcbiAgICAgICAgcmV0dXJuIHRvdGFsID4gMCAmJiAocG9zaXRpb24gPT09IHBhZ2luYXRpb25Qb3NpdGlvbiB8fCBwb3NpdGlvbiA9PT0gJ2JvdGgnKSA/ICg8UGFnaW5hdGlvbiBrZXk9e2BwYWdpbmF0aW9uLSR7cGFnaW5hdGlvblBvc2l0aW9ufWB9IHsuLi5wYWdpbmF0aW9ufSBjbGFzc05hbWU9e2NsYXNzTmFtZXMocGFnaW5hdGlvbi5jbGFzc05hbWUsIGAke3ByZWZpeENsc30tcGFnaW5hdGlvbmApfSBvbkNoYW5nZT17dGhpcy5oYW5kbGVQYWdlQ2hhbmdlfSB0b3RhbD17dG90YWx9IHNpemU9e3NpemV9IGN1cnJlbnQ9e3RoaXMuZ2V0TWF4Q3VycmVudCh0b3RhbCl9IG9uU2hvd1NpemVDaGFuZ2U9e3RoaXMuaGFuZGxlU2hvd1NpemVDaGFuZ2V9Lz4pIDogbnVsbDtcbiAgICB9XG4gICAgcmVuZGVyUm93U2VsZWN0aW9uKHsgcHJlZml4Q2xzLCBsb2NhbGUsIGdldFBvcHVwQ29udGFpbmVyLCB9KSB7XG4gICAgICAgIGNvbnN0IHsgcm93U2VsZWN0aW9uIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICBjb25zdCBjb2x1bW5zID0gdGhpcy5zdGF0ZS5jb2x1bW5zLmNvbmNhdCgpO1xuICAgICAgICBpZiAocm93U2VsZWN0aW9uKSB7XG4gICAgICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5nZXRGbGF0Q3VycmVudFBhZ2VEYXRhKCkuZmlsdGVyKChpdGVtLCBpbmRleCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChyb3dTZWxlY3Rpb24uZ2V0Q2hlY2tib3hQcm9wcykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gIXRoaXMuZ2V0Q2hlY2tib3hQcm9wc0J5SXRlbShpdGVtLCBpbmRleCkuZGlzYWJsZWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBzZWxlY3Rpb25Db2x1bW5DbGFzcyA9IGNsYXNzTmFtZXMoYCR7cHJlZml4Q2xzfS1zZWxlY3Rpb24tY29sdW1uYCwge1xuICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LXNlbGVjdGlvbi1jb2x1bW4tY3VzdG9tYF06IHJvd1NlbGVjdGlvbi5zZWxlY3Rpb25zLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBzZWxlY3Rpb25Db2x1bW4gPSB7XG4gICAgICAgICAgICAgICAga2V5OiAnc2VsZWN0aW9uLWNvbHVtbicsXG4gICAgICAgICAgICAgICAgcmVuZGVyOiB0aGlzLnJlbmRlclNlbGVjdGlvbkJveChyb3dTZWxlY3Rpb24udHlwZSksXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBzZWxlY3Rpb25Db2x1bW5DbGFzcyxcbiAgICAgICAgICAgICAgICBmaXhlZDogcm93U2VsZWN0aW9uLmZpeGVkLFxuICAgICAgICAgICAgICAgIHdpZHRoOiByb3dTZWxlY3Rpb24uY29sdW1uV2lkdGgsXG4gICAgICAgICAgICAgICAgdGl0bGU6IHJvd1NlbGVjdGlvbi5jb2x1bW5UaXRsZSxcbiAgICAgICAgICAgICAgICBbSU5URVJOQUxfQ09MX0RFRklORV06IHtcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBgJHtwcmVmaXhDbHN9LXNlbGVjdGlvbi1jb2xgLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaWYgKHJvd1NlbGVjdGlvbi50eXBlICE9PSAncmFkaW8nKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgY2hlY2tib3hBbGxEaXNhYmxlZCA9IGRhdGEuZXZlcnkoKGl0ZW0sIGluZGV4KSA9PiB0aGlzLmdldENoZWNrYm94UHJvcHNCeUl0ZW0oaXRlbSwgaW5kZXgpLmRpc2FibGVkKTtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25Db2x1bW4udGl0bGUgPSBzZWxlY3Rpb25Db2x1bW4udGl0bGUgfHwgKDxTZWxlY3Rpb25DaGVja2JveEFsbCBzdG9yZT17dGhpcy5wcm9wcy5zdG9yZX0gbG9jYWxlPXtsb2NhbGV9IGRhdGE9e2RhdGF9IGdldENoZWNrYm94UHJvcHNCeUl0ZW09e3RoaXMuZ2V0Q2hlY2tib3hQcm9wc0J5SXRlbX0gZ2V0UmVjb3JkS2V5PXt0aGlzLmdldFJlY29yZEtleX0gZGlzYWJsZWQ9e2NoZWNrYm94QWxsRGlzYWJsZWR9IHByZWZpeENscz17cHJlZml4Q2xzfSBvblNlbGVjdD17dGhpcy5oYW5kbGVTZWxlY3RSb3d9IHNlbGVjdGlvbnM9e3Jvd1NlbGVjdGlvbi5zZWxlY3Rpb25zfSBoaWRlRGVmYXVsdFNlbGVjdGlvbnM9e3Jvd1NlbGVjdGlvbi5oaWRlRGVmYXVsdFNlbGVjdGlvbnN9IGdldFBvcHVwQ29udGFpbmVyPXt0aGlzLmdlbmVyYXRlUG9wdXBDb250YWluZXJGdW5jKGdldFBvcHVwQ29udGFpbmVyKX0vPik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoJ2ZpeGVkJyBpbiByb3dTZWxlY3Rpb24pIHtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25Db2x1bW4uZml4ZWQgPSByb3dTZWxlY3Rpb24uZml4ZWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmIChjb2x1bW5zLnNvbWUoY29sdW1uID0+IGNvbHVtbi5maXhlZCA9PT0gJ2xlZnQnIHx8IGNvbHVtbi5maXhlZCA9PT0gdHJ1ZSkpIHtcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb25Db2x1bW4uZml4ZWQgPSAnbGVmdCc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoY29sdW1uc1swXSAmJiBjb2x1bW5zWzBdLmtleSA9PT0gJ3NlbGVjdGlvbi1jb2x1bW4nKSB7XG4gICAgICAgICAgICAgICAgY29sdW1uc1swXSA9IHNlbGVjdGlvbkNvbHVtbjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGNvbHVtbnMudW5zaGlmdChzZWxlY3Rpb25Db2x1bW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBjb2x1bW5zO1xuICAgIH1cbiAgICByZW5kZXJDb2x1bW5zRHJvcGRvd24oeyBwcmVmaXhDbHMsIGRyb3Bkb3duUHJlZml4Q2xzLCBjb2x1bW5zLCBsb2NhbGUsIGdldFBvcHVwQ29udGFpbmVyLCB9KSB7XG4gICAgICAgIGNvbnN0IHsgc29ydE9yZGVyLCBmaWx0ZXJzIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICByZXR1cm4gdHJlZU1hcChjb2x1bW5zLCAoY29sdW1uLCBpKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBrZXkgPSBnZXRDb2x1bW5LZXkoY29sdW1uLCBpKTtcbiAgICAgICAgICAgIGxldCBmaWx0ZXJEcm9wZG93bjtcbiAgICAgICAgICAgIGxldCBzb3J0QnV0dG9uO1xuICAgICAgICAgICAgbGV0IG9uSGVhZGVyQ2VsbCA9IGNvbHVtbi5vbkhlYWRlckNlbGw7XG4gICAgICAgICAgICBjb25zdCBpc1NvcnRDb2x1bW4gPSB0aGlzLmlzU29ydENvbHVtbihjb2x1bW4pO1xuICAgICAgICAgICAgaWYgKChjb2x1bW4uZmlsdGVycyAmJiBjb2x1bW4uZmlsdGVycy5sZW5ndGggPiAwKSB8fCBjb2x1bW4uZmlsdGVyRHJvcGRvd24pIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjb2xGaWx0ZXJzID0ga2V5IGluIGZpbHRlcnMgPyBmaWx0ZXJzW2tleV0gOiBbXTtcbiAgICAgICAgICAgICAgICBmaWx0ZXJEcm9wZG93biA9ICg8RmlsdGVyRHJvcGRvd24gbG9jYWxlPXtsb2NhbGV9IGNvbHVtbj17Y29sdW1ufSBzZWxlY3RlZEtleXM9e2NvbEZpbHRlcnN9IGNvbmZpcm1GaWx0ZXI9e3RoaXMuaGFuZGxlRmlsdGVyfSBwcmVmaXhDbHM9e2Ake3ByZWZpeENsc30tZmlsdGVyYH0gZHJvcGRvd25QcmVmaXhDbHM9e2Ryb3Bkb3duUHJlZml4Q2xzIHx8ICdhbnQtZHJvcGRvd24nfSBnZXRQb3B1cENvbnRhaW5lcj17dGhpcy5nZW5lcmF0ZVBvcHVwQ29udGFpbmVyRnVuYyhnZXRQb3B1cENvbnRhaW5lcil9IGtleT1cImZpbHRlci1kcm9wZG93blwiLz4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbHVtbi5zb3J0ZXIpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBzb3J0RGlyZWN0aW9ucyA9IGNvbHVtbi5zb3J0RGlyZWN0aW9ucyB8fCB0aGlzLnByb3BzLnNvcnREaXJlY3Rpb25zO1xuICAgICAgICAgICAgICAgIGNvbnN0IGlzQXNjZW5kID0gaXNTb3J0Q29sdW1uICYmIHNvcnRPcmRlciA9PT0gJ2FzY2VuZCc7XG4gICAgICAgICAgICAgICAgY29uc3QgaXNEZXNjZW5kID0gaXNTb3J0Q29sdW1uICYmIHNvcnRPcmRlciA9PT0gJ2Rlc2NlbmQnO1xuICAgICAgICAgICAgICAgIGNvbnN0IGFzY2VuZCA9IHNvcnREaXJlY3Rpb25zLmluZGV4T2YoJ2FzY2VuZCcpICE9PSAtMSAmJiAoPEljb24gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbHVtbi1zb3J0ZXItdXAgJHtpc0FzY2VuZCA/ICdvbicgOiAnb2ZmJ31gfSB0eXBlPVwiY2FyZXQtdXBcIiB0aGVtZT1cImZpbGxlZFwiLz4pO1xuICAgICAgICAgICAgICAgIGNvbnN0IGRlc2NlbmQgPSBzb3J0RGlyZWN0aW9ucy5pbmRleE9mKCdkZXNjZW5kJykgIT09IC0xICYmICg8SWNvbiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30tY29sdW1uLXNvcnRlci1kb3duICR7aXNEZXNjZW5kID8gJ29uJyA6ICdvZmYnfWB9IHR5cGU9XCJjYXJldC1kb3duXCIgdGhlbWU9XCJmaWxsZWRcIi8+KTtcbiAgICAgICAgICAgICAgICBzb3J0QnV0dG9uID0gKDxkaXYgdGl0bGU9e2xvY2FsZS5zb3J0VGl0bGV9IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWNvbHVtbi1zb3J0ZXItaW5uZXJgLCBhc2NlbmQgJiYgZGVzY2VuZCAmJiBgJHtwcmVmaXhDbHN9LWNvbHVtbi1zb3J0ZXItaW5uZXItZnVsbGApfSBrZXk9XCJzb3J0ZXJcIj5cbiAgICAgICAgICAgIHthc2NlbmR9XG4gICAgICAgICAgICB7ZGVzY2VuZH1cbiAgICAgICAgICA8L2Rpdj4pO1xuICAgICAgICAgICAgICAgIG9uSGVhZGVyQ2VsbCA9IChjb2wpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNvbFByb3BzID0ge307XG4gICAgICAgICAgICAgICAgICAgIC8vIEdldCBvcmlnaW5hbCBmaXJzdFxuICAgICAgICAgICAgICAgICAgICBpZiAoY29sdW1uLm9uSGVhZGVyQ2VsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sUHJvcHMgPSBPYmplY3QuYXNzaWduKHt9LCBjb2x1bW4ub25IZWFkZXJDZWxsKGNvbCkpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIEFkZCBzb3J0ZXIgbG9naWNcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb25IZWFkZXJDZWxsQ2xpY2sgPSBjb2xQcm9wcy5vbkNsaWNrO1xuICAgICAgICAgICAgICAgICAgICBjb2xQcm9wcy5vbkNsaWNrID0gKC4uLmFyZ3MpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlU29ydE9yZGVyKGNvbHVtbik7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob25IZWFkZXJDZWxsQ2xpY2spIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkhlYWRlckNlbGxDbGljayguLi5hcmdzKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbFByb3BzO1xuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBjb2x1bW4pLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhjb2x1bW4uY2xhc3NOYW1lLCB7XG4gICAgICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWNvbHVtbi1oYXMtYWN0aW9uc2BdOiBzb3J0QnV0dG9uIHx8IGZpbHRlckRyb3Bkb3duLFxuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb2x1bW4taGFzLWZpbHRlcnNgXTogZmlsdGVyRHJvcGRvd24sXG4gICAgICAgICAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWNvbHVtbi1oYXMtc29ydGVyc2BdOiBzb3J0QnV0dG9uLFxuICAgICAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb2x1bW4tc29ydGBdOiBpc1NvcnRDb2x1bW4gJiYgc29ydE9yZGVyLFxuICAgICAgICAgICAgICAgIH0pLCB0aXRsZTogW1xuICAgICAgICAgICAgICAgICAgICA8c3BhbiBrZXk9XCJ0aXRsZVwiIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1oZWFkZXItY29sdW1uYH0+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c29ydEJ1dHRvbiA/IGAke3ByZWZpeENsc30tY29sdW1uLXNvcnRlcnNgIDogdW5kZWZpbmVkfT5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbHVtbi10aXRsZWB9PlxuICAgICAgICAgICAgICAgIHt0aGlzLnJlbmRlckNvbHVtblRpdGxlKGNvbHVtbi50aXRsZSl9XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWNvbHVtbi1zb3J0ZXJgfT57c29ydEJ1dHRvbn08L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L3NwYW4+LFxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJEcm9wZG93bixcbiAgICAgICAgICAgICAgICBdLCBvbkhlYWRlckNlbGwgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICByZW5kZXJDb2x1bW5UaXRsZSh0aXRsZSkge1xuICAgICAgICBjb25zdCB7IGZpbHRlcnMsIHNvcnRPcmRlciwgc29ydENvbHVtbiB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTEyNDYjaXNzdWVjb21tZW50LTQwNTAwOTE2N1xuICAgICAgICBpZiAodGl0bGUgaW5zdGFuY2VvZiBGdW5jdGlvbikge1xuICAgICAgICAgICAgcmV0dXJuIHRpdGxlKHtcbiAgICAgICAgICAgICAgICBmaWx0ZXJzLFxuICAgICAgICAgICAgICAgIHNvcnRPcmRlcixcbiAgICAgICAgICAgICAgICBzb3J0Q29sdW1uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRpdGxlO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyQ29tcG9uZW50fTwvQ29uZmlnQ29uc3VtZXI+O1xuICAgIH1cbn1cblRhYmxlLnByb3BUeXBlcyA9IHtcbiAgICBkYXRhU291cmNlOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgY29sdW1uczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHByZWZpeENsczogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB1c2VGaXhlZEhlYWRlcjogUHJvcFR5cGVzLmJvb2wsXG4gICAgcm93U2VsZWN0aW9uOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzaXplOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGxvYWRpbmc6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ib29sLCBQcm9wVHlwZXMub2JqZWN0XSksXG4gICAgYm9yZGVyZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBsb2NhbGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgZHJvcGRvd25QcmVmaXhDbHM6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc29ydERpcmVjdGlvbnM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBnZXRQb3B1cENvbnRhaW5lcjogUHJvcFR5cGVzLmZ1bmMsXG59O1xuVGFibGUuZGVmYXVsdFByb3BzID0ge1xuICAgIGRhdGFTb3VyY2U6IFtdLFxuICAgIHVzZUZpeGVkSGVhZGVyOiBmYWxzZSxcbiAgICBjbGFzc05hbWU6ICcnLFxuICAgIHNpemU6ICdkZWZhdWx0JyxcbiAgICBsb2FkaW5nOiBmYWxzZSxcbiAgICBib3JkZXJlZDogZmFsc2UsXG4gICAgaW5kZW50U2l6ZTogMjAsXG4gICAgbG9jYWxlOiB7fSxcbiAgICByb3dLZXk6ICdrZXknLFxuICAgIHNob3dIZWFkZXI6IHRydWUsXG4gICAgc29ydERpcmVjdGlvbnM6IFsnYXNjZW5kJywgJ2Rlc2NlbmQnXSxcbiAgICBjaGlsZHJlbkNvbHVtbk5hbWU6ICdjaGlsZHJlbicsXG59O1xucG9seWZpbGwoVGFibGUpO1xuY2xhc3MgU3RvcmVUYWJsZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICAgICAgc3VwZXIocHJvcHMpO1xuICAgICAgICB0aGlzLnNldENoZWNrYm94UHJvcHNDYWNoZSA9IChjYWNoZSkgPT4gKHRoaXMuQ2hlY2tib3hQcm9wc0NhY2hlID0gY2FjaGUpO1xuICAgICAgICB0aGlzLkNoZWNrYm94UHJvcHNDYWNoZSA9IHt9O1xuICAgICAgICB0aGlzLnN0b3JlID0gY3JlYXRlU3RvcmUoe1xuICAgICAgICAgICAgc2VsZWN0ZWRSb3dLZXlzOiBnZXRSb3dTZWxlY3Rpb24ocHJvcHMpLnNlbGVjdGVkUm93S2V5cyB8fCBbXSxcbiAgICAgICAgICAgIHNlbGVjdGlvbkRpcnR5OiBmYWxzZSxcbiAgICAgICAgfSk7XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuICg8VGFibGUgey4uLnRoaXMucHJvcHN9IHN0b3JlPXt0aGlzLnN0b3JlfSBjaGVja2JveFByb3BzQ2FjaGU9e3RoaXMuQ2hlY2tib3hQcm9wc0NhY2hlfSBzZXRDaGVja2JveFByb3BzQ2FjaGU9e3RoaXMuc2V0Q2hlY2tib3hQcm9wc0NhY2hlfS8+KTtcbiAgICB9XG59XG5TdG9yZVRhYmxlLmRpc3BsYXlOYW1lID0gJ3dpdGhTdG9yZShUYWJsZSknO1xuU3RvcmVUYWJsZS5Db2x1bW4gPSBDb2x1bW47XG5TdG9yZVRhYmxlLkNvbHVtbkdyb3VwID0gQ29sdW1uR3JvdXA7XG5leHBvcnQgZGVmYXVsdCBTdG9yZVRhYmxlO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7Ozs7O0FBSUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGQTtBQUNBO0FBR0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBWkE7QUFDQTtBQWFBO0FBQUE7QUFFQTtBQUNBO0FBR0E7QUFOQTtBQUNBO0FBT0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUhBO0FBQ0E7QUFJQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFKQTtBQUNBO0FBQ0E7QUFLQTtBQUFBO0FBQUE7QUFQQTtBQUNBO0FBUUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBR0E7QUFOQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQWxDQTtBQUNBO0FBQ0E7QUFtQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFUQTtBQXhDQTtBQUNBO0FBbURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQVJBO0FBQ0E7QUFRQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBdEJBO0FBQ0E7QUFJQTtBQUFBO0FBa0JBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUE1QkE7QUFxQ0E7QUFDQTtBQURBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBcEVBO0FBQ0E7QUFxRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFSQTtBQUNBO0FBZUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUZBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBbENBO0FBQ0E7QUFtQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQW5EQTtBQXNEQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUE3REE7QUFDQTtBQWtFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBVEE7QUFBQTtBQUFBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBbkJBO0FBQ0E7QUFxQkE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBeEJBO0FBQ0E7QUF5QkE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFOQTtBQUNBO0FBTUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUFBO0FBR0E7QUFDQTtBQUpBO0FBS0E7QUFBQTtBQUxBO0FBREE7QUFRQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFkQTtBQUFBO0FBQ0E7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVBBO0FBREE7QUFDQTtBQVdBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXRDQTtBQUNBO0FBdUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVpBO0FBcUJBO0FBQ0E7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQTFCQTtBQUNBO0FBeFdBO0FBQUE7QUF5WUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUEvWUE7QUFrWkE7QUFDQTs7O0FBNkNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBRkE7QUFHQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFJQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUxBO0FBT0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQVpBO0FBZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUVBO0FBQ0E7OztBQUNBO0FBQUE7QUFFQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQVJBO0FBQ0E7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBRkE7QUFWQTtBQWdCQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFBQTtBQUFBO0FBQ0E7QUFGQTtBQUtBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFGQTtBQUtBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUF2QkE7QUFDQTtBQXVCQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBRUE7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBTEE7QUFDQTtBQU1BO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUpBO0FBTUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFkQTtBQWdCQTtBQUNBO0FBQUE7QUFBQTtBQUtBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFHQTtBQUFBO0FBSUE7QUFmQTtBQXJDQTtBQXNEQTs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7O0FBdmJBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQXJCQTtBQUNBO0FBQ0E7QUF3QkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7OztBQWhjQTtBQUNBO0FBNDBCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFkQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBY0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSkE7QUFRQTtBQUNBOzs7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7OztBQVpBO0FBQ0E7QUFhQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/table/Table.js
