/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var each = _util.each;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// (1) [Caution]: the logic is correct based on the premises:
//     data processing stage is blocked in stream.
//     See <module:echarts/stream/Scheduler#performDataProcessorTasks>
// (2) Only register once when import repeatly.
//     Should be executed before after series filtered and before stack calculation.

function _default(ecModel) {
  var stackInfoMap = createHashMap();
  ecModel.eachSeries(function (seriesModel) {
    var stack = seriesModel.get('stack'); // Compatibal: when `stack` is set as '', do not stack.

    if (stack) {
      var stackInfoList = stackInfoMap.get(stack) || stackInfoMap.set(stack, []);
      var data = seriesModel.getData();
      var stackInfo = {
        // Used for calculate axis extent automatically.
        stackResultDimension: data.getCalculationInfo('stackResultDimension'),
        stackedOverDimension: data.getCalculationInfo('stackedOverDimension'),
        stackedDimension: data.getCalculationInfo('stackedDimension'),
        stackedByDimension: data.getCalculationInfo('stackedByDimension'),
        isStackedByIndex: data.getCalculationInfo('isStackedByIndex'),
        data: data,
        seriesModel: seriesModel
      }; // If stacked on axis that do not support data stack.

      if (!stackInfo.stackedDimension || !(stackInfo.isStackedByIndex || stackInfo.stackedByDimension)) {
        return;
      }

      stackInfoList.length && data.setCalculationInfo('stackedOnSeries', stackInfoList[stackInfoList.length - 1].seriesModel);
      stackInfoList.push(stackInfo);
    }
  });
  stackInfoMap.each(calculateStack);
}

function calculateStack(stackInfoList) {
  each(stackInfoList, function (targetStackInfo, idxInStack) {
    var resultVal = [];
    var resultNaN = [NaN, NaN];
    var dims = [targetStackInfo.stackResultDimension, targetStackInfo.stackedOverDimension];
    var targetData = targetStackInfo.data;
    var isStackedByIndex = targetStackInfo.isStackedByIndex; // Should not write on raw data, because stack series model list changes
    // depending on legend selection.

    var newData = targetData.map(dims, function (v0, v1, dataIndex) {
      var sum = targetData.get(targetStackInfo.stackedDimension, dataIndex); // Consider `connectNulls` of line area, if value is NaN, stackedOver
      // should also be NaN, to draw a appropriate belt area.

      if (isNaN(sum)) {
        return resultNaN;
      }

      var byValue;
      var stackedDataRawIndex;

      if (isStackedByIndex) {
        stackedDataRawIndex = targetData.getRawIndex(dataIndex);
      } else {
        byValue = targetData.get(targetStackInfo.stackedByDimension, dataIndex);
      } // If stackOver is NaN, chart view will render point on value start.


      var stackedOver = NaN;

      for (var j = idxInStack - 1; j >= 0; j--) {
        var stackInfo = stackInfoList[j]; // Has been optimized by inverted indices on `stackedByDimension`.

        if (!isStackedByIndex) {
          stackedDataRawIndex = stackInfo.data.rawIndexOf(stackInfo.stackedByDimension, byValue);
        }

        if (stackedDataRawIndex >= 0) {
          var val = stackInfo.data.getByRawIndex(stackInfo.stackResultDimension, stackedDataRawIndex); // Considering positive stack, negative stack and empty data

          if (sum >= 0 && val > 0 || // Positive stack
          sum <= 0 && val < 0 // Negative stack
          ) {
              sum += val;
              stackedOver = val;
              break;
            }
        }
      }

      resultVal[0] = sum;
      resultVal[1] = stackedOver;
      return resultVal;
    });
    targetData.hostModel.setData(newData); // Update for consequent calculation

    targetStackInfo.data = newData;
  });
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvcHJvY2Vzc29yL2RhdGFTdGFjay5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3Byb2Nlc3Nvci9kYXRhU3RhY2suanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBjcmVhdGVIYXNoTWFwID0gX3V0aWwuY3JlYXRlSGFzaE1hcDtcbnZhciBlYWNoID0gX3V0aWwuZWFjaDtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuLy8gKDEpIFtDYXV0aW9uXTogdGhlIGxvZ2ljIGlzIGNvcnJlY3QgYmFzZWQgb24gdGhlIHByZW1pc2VzOlxuLy8gICAgIGRhdGEgcHJvY2Vzc2luZyBzdGFnZSBpcyBibG9ja2VkIGluIHN0cmVhbS5cbi8vICAgICBTZWUgPG1vZHVsZTplY2hhcnRzL3N0cmVhbS9TY2hlZHVsZXIjcGVyZm9ybURhdGFQcm9jZXNzb3JUYXNrcz5cbi8vICgyKSBPbmx5IHJlZ2lzdGVyIG9uY2Ugd2hlbiBpbXBvcnQgcmVwZWF0bHkuXG4vLyAgICAgU2hvdWxkIGJlIGV4ZWN1dGVkIGJlZm9yZSBhZnRlciBzZXJpZXMgZmlsdGVyZWQgYW5kIGJlZm9yZSBzdGFjayBjYWxjdWxhdGlvbi5cbmZ1bmN0aW9uIF9kZWZhdWx0KGVjTW9kZWwpIHtcbiAgdmFyIHN0YWNrSW5mb01hcCA9IGNyZWF0ZUhhc2hNYXAoKTtcbiAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIHZhciBzdGFjayA9IHNlcmllc01vZGVsLmdldCgnc3RhY2snKTsgLy8gQ29tcGF0aWJhbDogd2hlbiBgc3RhY2tgIGlzIHNldCBhcyAnJywgZG8gbm90IHN0YWNrLlxuXG4gICAgaWYgKHN0YWNrKSB7XG4gICAgICB2YXIgc3RhY2tJbmZvTGlzdCA9IHN0YWNrSW5mb01hcC5nZXQoc3RhY2spIHx8IHN0YWNrSW5mb01hcC5zZXQoc3RhY2ssIFtdKTtcbiAgICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgICAgdmFyIHN0YWNrSW5mbyA9IHtcbiAgICAgICAgLy8gVXNlZCBmb3IgY2FsY3VsYXRlIGF4aXMgZXh0ZW50IGF1dG9tYXRpY2FsbHkuXG4gICAgICAgIHN0YWNrUmVzdWx0RGltZW5zaW9uOiBkYXRhLmdldENhbGN1bGF0aW9uSW5mbygnc3RhY2tSZXN1bHREaW1lbnNpb24nKSxcbiAgICAgICAgc3RhY2tlZE92ZXJEaW1lbnNpb246IGRhdGEuZ2V0Q2FsY3VsYXRpb25JbmZvKCdzdGFja2VkT3ZlckRpbWVuc2lvbicpLFxuICAgICAgICBzdGFja2VkRGltZW5zaW9uOiBkYXRhLmdldENhbGN1bGF0aW9uSW5mbygnc3RhY2tlZERpbWVuc2lvbicpLFxuICAgICAgICBzdGFja2VkQnlEaW1lbnNpb246IGRhdGEuZ2V0Q2FsY3VsYXRpb25JbmZvKCdzdGFja2VkQnlEaW1lbnNpb24nKSxcbiAgICAgICAgaXNTdGFja2VkQnlJbmRleDogZGF0YS5nZXRDYWxjdWxhdGlvbkluZm8oJ2lzU3RhY2tlZEJ5SW5kZXgnKSxcbiAgICAgICAgZGF0YTogZGF0YSxcbiAgICAgICAgc2VyaWVzTW9kZWw6IHNlcmllc01vZGVsXG4gICAgICB9OyAvLyBJZiBzdGFja2VkIG9uIGF4aXMgdGhhdCBkbyBub3Qgc3VwcG9ydCBkYXRhIHN0YWNrLlxuXG4gICAgICBpZiAoIXN0YWNrSW5mby5zdGFja2VkRGltZW5zaW9uIHx8ICEoc3RhY2tJbmZvLmlzU3RhY2tlZEJ5SW5kZXggfHwgc3RhY2tJbmZvLnN0YWNrZWRCeURpbWVuc2lvbikpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBzdGFja0luZm9MaXN0Lmxlbmd0aCAmJiBkYXRhLnNldENhbGN1bGF0aW9uSW5mbygnc3RhY2tlZE9uU2VyaWVzJywgc3RhY2tJbmZvTGlzdFtzdGFja0luZm9MaXN0Lmxlbmd0aCAtIDFdLnNlcmllc01vZGVsKTtcbiAgICAgIHN0YWNrSW5mb0xpc3QucHVzaChzdGFja0luZm8pO1xuICAgIH1cbiAgfSk7XG4gIHN0YWNrSW5mb01hcC5lYWNoKGNhbGN1bGF0ZVN0YWNrKTtcbn1cblxuZnVuY3Rpb24gY2FsY3VsYXRlU3RhY2soc3RhY2tJbmZvTGlzdCkge1xuICBlYWNoKHN0YWNrSW5mb0xpc3QsIGZ1bmN0aW9uICh0YXJnZXRTdGFja0luZm8sIGlkeEluU3RhY2spIHtcbiAgICB2YXIgcmVzdWx0VmFsID0gW107XG4gICAgdmFyIHJlc3VsdE5hTiA9IFtOYU4sIE5hTl07XG4gICAgdmFyIGRpbXMgPSBbdGFyZ2V0U3RhY2tJbmZvLnN0YWNrUmVzdWx0RGltZW5zaW9uLCB0YXJnZXRTdGFja0luZm8uc3RhY2tlZE92ZXJEaW1lbnNpb25dO1xuICAgIHZhciB0YXJnZXREYXRhID0gdGFyZ2V0U3RhY2tJbmZvLmRhdGE7XG4gICAgdmFyIGlzU3RhY2tlZEJ5SW5kZXggPSB0YXJnZXRTdGFja0luZm8uaXNTdGFja2VkQnlJbmRleDsgLy8gU2hvdWxkIG5vdCB3cml0ZSBvbiByYXcgZGF0YSwgYmVjYXVzZSBzdGFjayBzZXJpZXMgbW9kZWwgbGlzdCBjaGFuZ2VzXG4gICAgLy8gZGVwZW5kaW5nIG9uIGxlZ2VuZCBzZWxlY3Rpb24uXG5cbiAgICB2YXIgbmV3RGF0YSA9IHRhcmdldERhdGEubWFwKGRpbXMsIGZ1bmN0aW9uICh2MCwgdjEsIGRhdGFJbmRleCkge1xuICAgICAgdmFyIHN1bSA9IHRhcmdldERhdGEuZ2V0KHRhcmdldFN0YWNrSW5mby5zdGFja2VkRGltZW5zaW9uLCBkYXRhSW5kZXgpOyAvLyBDb25zaWRlciBgY29ubmVjdE51bGxzYCBvZiBsaW5lIGFyZWEsIGlmIHZhbHVlIGlzIE5hTiwgc3RhY2tlZE92ZXJcbiAgICAgIC8vIHNob3VsZCBhbHNvIGJlIE5hTiwgdG8gZHJhdyBhIGFwcHJvcHJpYXRlIGJlbHQgYXJlYS5cblxuICAgICAgaWYgKGlzTmFOKHN1bSkpIHtcbiAgICAgICAgcmV0dXJuIHJlc3VsdE5hTjtcbiAgICAgIH1cblxuICAgICAgdmFyIGJ5VmFsdWU7XG4gICAgICB2YXIgc3RhY2tlZERhdGFSYXdJbmRleDtcblxuICAgICAgaWYgKGlzU3RhY2tlZEJ5SW5kZXgpIHtcbiAgICAgICAgc3RhY2tlZERhdGFSYXdJbmRleCA9IHRhcmdldERhdGEuZ2V0UmF3SW5kZXgoZGF0YUluZGV4KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJ5VmFsdWUgPSB0YXJnZXREYXRhLmdldCh0YXJnZXRTdGFja0luZm8uc3RhY2tlZEJ5RGltZW5zaW9uLCBkYXRhSW5kZXgpO1xuICAgICAgfSAvLyBJZiBzdGFja092ZXIgaXMgTmFOLCBjaGFydCB2aWV3IHdpbGwgcmVuZGVyIHBvaW50IG9uIHZhbHVlIHN0YXJ0LlxuXG5cbiAgICAgIHZhciBzdGFja2VkT3ZlciA9IE5hTjtcblxuICAgICAgZm9yICh2YXIgaiA9IGlkeEluU3RhY2sgLSAxOyBqID49IDA7IGotLSkge1xuICAgICAgICB2YXIgc3RhY2tJbmZvID0gc3RhY2tJbmZvTGlzdFtqXTsgLy8gSGFzIGJlZW4gb3B0aW1pemVkIGJ5IGludmVydGVkIGluZGljZXMgb24gYHN0YWNrZWRCeURpbWVuc2lvbmAuXG5cbiAgICAgICAgaWYgKCFpc1N0YWNrZWRCeUluZGV4KSB7XG4gICAgICAgICAgc3RhY2tlZERhdGFSYXdJbmRleCA9IHN0YWNrSW5mby5kYXRhLnJhd0luZGV4T2Yoc3RhY2tJbmZvLnN0YWNrZWRCeURpbWVuc2lvbiwgYnlWYWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoc3RhY2tlZERhdGFSYXdJbmRleCA+PSAwKSB7XG4gICAgICAgICAgdmFyIHZhbCA9IHN0YWNrSW5mby5kYXRhLmdldEJ5UmF3SW5kZXgoc3RhY2tJbmZvLnN0YWNrUmVzdWx0RGltZW5zaW9uLCBzdGFja2VkRGF0YVJhd0luZGV4KTsgLy8gQ29uc2lkZXJpbmcgcG9zaXRpdmUgc3RhY2ssIG5lZ2F0aXZlIHN0YWNrIGFuZCBlbXB0eSBkYXRhXG5cbiAgICAgICAgICBpZiAoc3VtID49IDAgJiYgdmFsID4gMCB8fCAvLyBQb3NpdGl2ZSBzdGFja1xuICAgICAgICAgIHN1bSA8PSAwICYmIHZhbCA8IDAgLy8gTmVnYXRpdmUgc3RhY2tcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgc3VtICs9IHZhbDtcbiAgICAgICAgICAgICAgc3RhY2tlZE92ZXIgPSB2YWw7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJlc3VsdFZhbFswXSA9IHN1bTtcbiAgICAgIHJlc3VsdFZhbFsxXSA9IHN0YWNrZWRPdmVyO1xuICAgICAgcmV0dXJuIHJlc3VsdFZhbDtcbiAgICB9KTtcbiAgICB0YXJnZXREYXRhLmhvc3RNb2RlbC5zZXREYXRhKG5ld0RhdGEpOyAvLyBVcGRhdGUgZm9yIGNvbnNlcXVlbnQgY2FsY3VsYXRpb25cblxuICAgIHRhcmdldFN0YWNrSW5mby5kYXRhID0gbmV3RGF0YTtcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/processor/dataStack.js
