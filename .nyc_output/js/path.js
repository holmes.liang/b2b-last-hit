var Path = __webpack_require__(/*! ../graphic/Path */ "./node_modules/zrender/lib/graphic/Path.js");

var PathProxy = __webpack_require__(/*! ../core/PathProxy */ "./node_modules/zrender/lib/core/PathProxy.js");

var transformPath = __webpack_require__(/*! ./transformPath */ "./node_modules/zrender/lib/tool/transformPath.js"); // command chars
// var cc = [
//     'm', 'M', 'l', 'L', 'v', 'V', 'h', 'H', 'z', 'Z',
//     'c', 'C', 'q', 'Q', 't', 'T', 's', 'S', 'a', 'A'
// ];


var mathSqrt = Math.sqrt;
var mathSin = Math.sin;
var mathCos = Math.cos;
var PI = Math.PI;

var vMag = function vMag(v) {
  return Math.sqrt(v[0] * v[0] + v[1] * v[1]);
};

var vRatio = function vRatio(u, v) {
  return (u[0] * v[0] + u[1] * v[1]) / (vMag(u) * vMag(v));
};

var vAngle = function vAngle(u, v) {
  return (u[0] * v[1] < u[1] * v[0] ? -1 : 1) * Math.acos(vRatio(u, v));
};

function processArc(x1, y1, x2, y2, fa, fs, rx, ry, psiDeg, cmd, path) {
  var psi = psiDeg * (PI / 180.0);
  var xp = mathCos(psi) * (x1 - x2) / 2.0 + mathSin(psi) * (y1 - y2) / 2.0;
  var yp = -1 * mathSin(psi) * (x1 - x2) / 2.0 + mathCos(psi) * (y1 - y2) / 2.0;
  var lambda = xp * xp / (rx * rx) + yp * yp / (ry * ry);

  if (lambda > 1) {
    rx *= mathSqrt(lambda);
    ry *= mathSqrt(lambda);
  }

  var f = (fa === fs ? -1 : 1) * mathSqrt((rx * rx * (ry * ry) - rx * rx * (yp * yp) - ry * ry * (xp * xp)) / (rx * rx * (yp * yp) + ry * ry * (xp * xp))) || 0;
  var cxp = f * rx * yp / ry;
  var cyp = f * -ry * xp / rx;
  var cx = (x1 + x2) / 2.0 + mathCos(psi) * cxp - mathSin(psi) * cyp;
  var cy = (y1 + y2) / 2.0 + mathSin(psi) * cxp + mathCos(psi) * cyp;
  var theta = vAngle([1, 0], [(xp - cxp) / rx, (yp - cyp) / ry]);
  var u = [(xp - cxp) / rx, (yp - cyp) / ry];
  var v = [(-1 * xp - cxp) / rx, (-1 * yp - cyp) / ry];
  var dTheta = vAngle(u, v);

  if (vRatio(u, v) <= -1) {
    dTheta = PI;
  }

  if (vRatio(u, v) >= 1) {
    dTheta = 0;
  }

  if (fs === 0 && dTheta > 0) {
    dTheta = dTheta - 2 * PI;
  }

  if (fs === 1 && dTheta < 0) {
    dTheta = dTheta + 2 * PI;
  }

  path.addData(cmd, cx, cy, rx, ry, theta, dTheta, psi, fs);
}

var commandReg = /([mlvhzcqtsa])([^mlvhzcqtsa]*)/ig; // Consider case:
// (1) delimiter can be comma or space, where continuous commas
// or spaces should be seen as one comma.
// (2) value can be like:
// '2e-4', 'l.5.9' (ignore 0), 'M-10-10', 'l-2.43e-1,34.9983',
// 'l-.5E1,54', '121-23-44-11' (no delimiter)

var numberReg = /-?([0-9]*\.)?[0-9]+([eE]-?[0-9]+)?/g; // var valueSplitReg = /[\s,]+/;

function createPathProxyFromString(data) {
  if (!data) {
    return new PathProxy();
  } // var data = data.replace(/-/g, ' -')
  //     .replace(/  /g, ' ')
  //     .replace(/ /g, ',')
  //     .replace(/,,/g, ',');
  // var n;
  // create pipes so that we can split the data
  // for (n = 0; n < cc.length; n++) {
  //     cs = cs.replace(new RegExp(cc[n], 'g'), '|' + cc[n]);
  // }
  // data = data.replace(/-/g, ',-');
  // create array
  // var arr = cs.split('|');
  // init context point


  var cpx = 0;
  var cpy = 0;
  var subpathX = cpx;
  var subpathY = cpy;
  var prevCmd;
  var path = new PathProxy();
  var CMD = PathProxy.CMD; // commandReg.lastIndex = 0;
  // var cmdResult;
  // while ((cmdResult = commandReg.exec(data)) != null) {
  //     var cmdStr = cmdResult[1];
  //     var cmdContent = cmdResult[2];

  var cmdList = data.match(commandReg);

  for (var l = 0; l < cmdList.length; l++) {
    var cmdText = cmdList[l];
    var cmdStr = cmdText.charAt(0);
    var cmd; // String#split is faster a little bit than String#replace or RegExp#exec.
    // var p = cmdContent.split(valueSplitReg);
    // var pLen = 0;
    // for (var i = 0; i < p.length; i++) {
    //     // '' and other invalid str => NaN
    //     var val = parseFloat(p[i]);
    //     !isNaN(val) && (p[pLen++] = val);
    // }

    var p = cmdText.match(numberReg) || [];
    var pLen = p.length;

    for (var i = 0; i < pLen; i++) {
      p[i] = parseFloat(p[i]);
    }

    var off = 0;

    while (off < pLen) {
      var ctlPtx;
      var ctlPty;
      var rx;
      var ry;
      var psi;
      var fa;
      var fs;
      var x1 = cpx;
      var y1 = cpy; // convert l, H, h, V, and v to L

      switch (cmdStr) {
        case 'l':
          cpx += p[off++];
          cpy += p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'L':
          cpx = p[off++];
          cpy = p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'm':
          cpx += p[off++];
          cpy += p[off++];
          cmd = CMD.M;
          path.addData(cmd, cpx, cpy);
          subpathX = cpx;
          subpathY = cpy;
          cmdStr = 'l';
          break;

        case 'M':
          cpx = p[off++];
          cpy = p[off++];
          cmd = CMD.M;
          path.addData(cmd, cpx, cpy);
          subpathX = cpx;
          subpathY = cpy;
          cmdStr = 'L';
          break;

        case 'h':
          cpx += p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'H':
          cpx = p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'v':
          cpy += p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'V':
          cpy = p[off++];
          cmd = CMD.L;
          path.addData(cmd, cpx, cpy);
          break;

        case 'C':
          cmd = CMD.C;
          path.addData(cmd, p[off++], p[off++], p[off++], p[off++], p[off++], p[off++]);
          cpx = p[off - 2];
          cpy = p[off - 1];
          break;

        case 'c':
          cmd = CMD.C;
          path.addData(cmd, p[off++] + cpx, p[off++] + cpy, p[off++] + cpx, p[off++] + cpy, p[off++] + cpx, p[off++] + cpy);
          cpx += p[off - 2];
          cpy += p[off - 1];
          break;

        case 'S':
          ctlPtx = cpx;
          ctlPty = cpy;
          var len = path.len();
          var pathData = path.data;

          if (prevCmd === CMD.C) {
            ctlPtx += cpx - pathData[len - 4];
            ctlPty += cpy - pathData[len - 3];
          }

          cmd = CMD.C;
          x1 = p[off++];
          y1 = p[off++];
          cpx = p[off++];
          cpy = p[off++];
          path.addData(cmd, ctlPtx, ctlPty, x1, y1, cpx, cpy);
          break;

        case 's':
          ctlPtx = cpx;
          ctlPty = cpy;
          var len = path.len();
          var pathData = path.data;

          if (prevCmd === CMD.C) {
            ctlPtx += cpx - pathData[len - 4];
            ctlPty += cpy - pathData[len - 3];
          }

          cmd = CMD.C;
          x1 = cpx + p[off++];
          y1 = cpy + p[off++];
          cpx += p[off++];
          cpy += p[off++];
          path.addData(cmd, ctlPtx, ctlPty, x1, y1, cpx, cpy);
          break;

        case 'Q':
          x1 = p[off++];
          y1 = p[off++];
          cpx = p[off++];
          cpy = p[off++];
          cmd = CMD.Q;
          path.addData(cmd, x1, y1, cpx, cpy);
          break;

        case 'q':
          x1 = p[off++] + cpx;
          y1 = p[off++] + cpy;
          cpx += p[off++];
          cpy += p[off++];
          cmd = CMD.Q;
          path.addData(cmd, x1, y1, cpx, cpy);
          break;

        case 'T':
          ctlPtx = cpx;
          ctlPty = cpy;
          var len = path.len();
          var pathData = path.data;

          if (prevCmd === CMD.Q) {
            ctlPtx += cpx - pathData[len - 4];
            ctlPty += cpy - pathData[len - 3];
          }

          cpx = p[off++];
          cpy = p[off++];
          cmd = CMD.Q;
          path.addData(cmd, ctlPtx, ctlPty, cpx, cpy);
          break;

        case 't':
          ctlPtx = cpx;
          ctlPty = cpy;
          var len = path.len();
          var pathData = path.data;

          if (prevCmd === CMD.Q) {
            ctlPtx += cpx - pathData[len - 4];
            ctlPty += cpy - pathData[len - 3];
          }

          cpx += p[off++];
          cpy += p[off++];
          cmd = CMD.Q;
          path.addData(cmd, ctlPtx, ctlPty, cpx, cpy);
          break;

        case 'A':
          rx = p[off++];
          ry = p[off++];
          psi = p[off++];
          fa = p[off++];
          fs = p[off++];
          x1 = cpx, y1 = cpy;
          cpx = p[off++];
          cpy = p[off++];
          cmd = CMD.A;
          processArc(x1, y1, cpx, cpy, fa, fs, rx, ry, psi, cmd, path);
          break;

        case 'a':
          rx = p[off++];
          ry = p[off++];
          psi = p[off++];
          fa = p[off++];
          fs = p[off++];
          x1 = cpx, y1 = cpy;
          cpx += p[off++];
          cpy += p[off++];
          cmd = CMD.A;
          processArc(x1, y1, cpx, cpy, fa, fs, rx, ry, psi, cmd, path);
          break;
      }
    }

    if (cmdStr === 'z' || cmdStr === 'Z') {
      cmd = CMD.Z;
      path.addData(cmd); // z may be in the middle of the path.

      cpx = subpathX;
      cpy = subpathY;
    }

    prevCmd = cmd;
  }

  path.toStatic();
  return path;
} // TODO Optimize double memory cost problem


function createPathOptions(str, opts) {
  var pathProxy = createPathProxyFromString(str);
  opts = opts || {};

  opts.buildPath = function (path) {
    if (path.setData) {
      path.setData(pathProxy.data); // Svg and vml renderer don't have context

      var ctx = path.getContext();

      if (ctx) {
        path.rebuildPath(ctx);
      }
    } else {
      var ctx = path;
      pathProxy.rebuildPath(ctx);
    }
  };

  opts.applyTransform = function (m) {
    transformPath(pathProxy, m);
    this.dirty(true);
  };

  return opts;
}
/**
 * Create a Path object from path string data
 * http://www.w3.org/TR/SVG/paths.html#PathData
 * @param  {Object} opts Other options
 */


function createFromString(str, opts) {
  return new Path(createPathOptions(str, opts));
}
/**
 * Create a Path class from path string data
 * @param  {string} str
 * @param  {Object} opts Other options
 */


function extendFromString(str, opts) {
  return Path.extend(createPathOptions(str, opts));
}
/**
 * Merge multiple paths
 */
// TODO Apply transform
// TODO stroke dash
// TODO Optimize double memory cost problem


function mergePath(pathEls, opts) {
  var pathList = [];
  var len = pathEls.length;

  for (var i = 0; i < len; i++) {
    var pathEl = pathEls[i];

    if (!pathEl.path) {
      pathEl.createPathProxy();
    }

    if (pathEl.__dirtyPath) {
      pathEl.buildPath(pathEl.path, pathEl.shape, true);
    }

    pathList.push(pathEl.path);
  }

  var pathBundle = new Path(opts); // Need path proxy.

  pathBundle.createPathProxy();

  pathBundle.buildPath = function (path) {
    path.appendPath(pathList); // Svg and vml renderer don't have context

    var ctx = path.getContext();

    if (ctx) {
      path.rebuildPath(ctx);
    }
  };

  return pathBundle;
}

exports.createFromString = createFromString;
exports.extendFromString = extendFromString;
exports.mergePath = mergePath;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC9wYXRoLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvdG9vbC9wYXRoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL2dyYXBoaWMvUGF0aFwiKTtcblxudmFyIFBhdGhQcm94eSA9IHJlcXVpcmUoXCIuLi9jb3JlL1BhdGhQcm94eVwiKTtcblxudmFyIHRyYW5zZm9ybVBhdGggPSByZXF1aXJlKFwiLi90cmFuc2Zvcm1QYXRoXCIpO1xuXG4vLyBjb21tYW5kIGNoYXJzXG4vLyB2YXIgY2MgPSBbXG4vLyAgICAgJ20nLCAnTScsICdsJywgJ0wnLCAndicsICdWJywgJ2gnLCAnSCcsICd6JywgJ1onLFxuLy8gICAgICdjJywgJ0MnLCAncScsICdRJywgJ3QnLCAnVCcsICdzJywgJ1MnLCAnYScsICdBJ1xuLy8gXTtcbnZhciBtYXRoU3FydCA9IE1hdGguc3FydDtcbnZhciBtYXRoU2luID0gTWF0aC5zaW47XG52YXIgbWF0aENvcyA9IE1hdGguY29zO1xudmFyIFBJID0gTWF0aC5QSTtcblxudmFyIHZNYWcgPSBmdW5jdGlvbiAodikge1xuICByZXR1cm4gTWF0aC5zcXJ0KHZbMF0gKiB2WzBdICsgdlsxXSAqIHZbMV0pO1xufTtcblxudmFyIHZSYXRpbyA9IGZ1bmN0aW9uICh1LCB2KSB7XG4gIHJldHVybiAodVswXSAqIHZbMF0gKyB1WzFdICogdlsxXSkgLyAodk1hZyh1KSAqIHZNYWcodikpO1xufTtcblxudmFyIHZBbmdsZSA9IGZ1bmN0aW9uICh1LCB2KSB7XG4gIHJldHVybiAodVswXSAqIHZbMV0gPCB1WzFdICogdlswXSA/IC0xIDogMSkgKiBNYXRoLmFjb3ModlJhdGlvKHUsIHYpKTtcbn07XG5cbmZ1bmN0aW9uIHByb2Nlc3NBcmMoeDEsIHkxLCB4MiwgeTIsIGZhLCBmcywgcngsIHJ5LCBwc2lEZWcsIGNtZCwgcGF0aCkge1xuICB2YXIgcHNpID0gcHNpRGVnICogKFBJIC8gMTgwLjApO1xuICB2YXIgeHAgPSBtYXRoQ29zKHBzaSkgKiAoeDEgLSB4MikgLyAyLjAgKyBtYXRoU2luKHBzaSkgKiAoeTEgLSB5MikgLyAyLjA7XG4gIHZhciB5cCA9IC0xICogbWF0aFNpbihwc2kpICogKHgxIC0geDIpIC8gMi4wICsgbWF0aENvcyhwc2kpICogKHkxIC0geTIpIC8gMi4wO1xuICB2YXIgbGFtYmRhID0geHAgKiB4cCAvIChyeCAqIHJ4KSArIHlwICogeXAgLyAocnkgKiByeSk7XG5cbiAgaWYgKGxhbWJkYSA+IDEpIHtcbiAgICByeCAqPSBtYXRoU3FydChsYW1iZGEpO1xuICAgIHJ5ICo9IG1hdGhTcXJ0KGxhbWJkYSk7XG4gIH1cblxuICB2YXIgZiA9IChmYSA9PT0gZnMgPyAtMSA6IDEpICogbWF0aFNxcnQoKHJ4ICogcnggKiAocnkgKiByeSkgLSByeCAqIHJ4ICogKHlwICogeXApIC0gcnkgKiByeSAqICh4cCAqIHhwKSkgLyAocnggKiByeCAqICh5cCAqIHlwKSArIHJ5ICogcnkgKiAoeHAgKiB4cCkpKSB8fCAwO1xuICB2YXIgY3hwID0gZiAqIHJ4ICogeXAgLyByeTtcbiAgdmFyIGN5cCA9IGYgKiAtcnkgKiB4cCAvIHJ4O1xuICB2YXIgY3ggPSAoeDEgKyB4MikgLyAyLjAgKyBtYXRoQ29zKHBzaSkgKiBjeHAgLSBtYXRoU2luKHBzaSkgKiBjeXA7XG4gIHZhciBjeSA9ICh5MSArIHkyKSAvIDIuMCArIG1hdGhTaW4ocHNpKSAqIGN4cCArIG1hdGhDb3MocHNpKSAqIGN5cDtcbiAgdmFyIHRoZXRhID0gdkFuZ2xlKFsxLCAwXSwgWyh4cCAtIGN4cCkgLyByeCwgKHlwIC0gY3lwKSAvIHJ5XSk7XG4gIHZhciB1ID0gWyh4cCAtIGN4cCkgLyByeCwgKHlwIC0gY3lwKSAvIHJ5XTtcbiAgdmFyIHYgPSBbKC0xICogeHAgLSBjeHApIC8gcngsICgtMSAqIHlwIC0gY3lwKSAvIHJ5XTtcbiAgdmFyIGRUaGV0YSA9IHZBbmdsZSh1LCB2KTtcblxuICBpZiAodlJhdGlvKHUsIHYpIDw9IC0xKSB7XG4gICAgZFRoZXRhID0gUEk7XG4gIH1cblxuICBpZiAodlJhdGlvKHUsIHYpID49IDEpIHtcbiAgICBkVGhldGEgPSAwO1xuICB9XG5cbiAgaWYgKGZzID09PSAwICYmIGRUaGV0YSA+IDApIHtcbiAgICBkVGhldGEgPSBkVGhldGEgLSAyICogUEk7XG4gIH1cblxuICBpZiAoZnMgPT09IDEgJiYgZFRoZXRhIDwgMCkge1xuICAgIGRUaGV0YSA9IGRUaGV0YSArIDIgKiBQSTtcbiAgfVxuXG4gIHBhdGguYWRkRGF0YShjbWQsIGN4LCBjeSwgcngsIHJ5LCB0aGV0YSwgZFRoZXRhLCBwc2ksIGZzKTtcbn1cblxudmFyIGNvbW1hbmRSZWcgPSAvKFttbHZoemNxdHNhXSkoW15tbHZoemNxdHNhXSopL2lnOyAvLyBDb25zaWRlciBjYXNlOlxuLy8gKDEpIGRlbGltaXRlciBjYW4gYmUgY29tbWEgb3Igc3BhY2UsIHdoZXJlIGNvbnRpbnVvdXMgY29tbWFzXG4vLyBvciBzcGFjZXMgc2hvdWxkIGJlIHNlZW4gYXMgb25lIGNvbW1hLlxuLy8gKDIpIHZhbHVlIGNhbiBiZSBsaWtlOlxuLy8gJzJlLTQnLCAnbC41LjknIChpZ25vcmUgMCksICdNLTEwLTEwJywgJ2wtMi40M2UtMSwzNC45OTgzJyxcbi8vICdsLS41RTEsNTQnLCAnMTIxLTIzLTQ0LTExJyAobm8gZGVsaW1pdGVyKVxuXG52YXIgbnVtYmVyUmVnID0gLy0/KFswLTldKlxcLik/WzAtOV0rKFtlRV0tP1swLTldKyk/L2c7IC8vIHZhciB2YWx1ZVNwbGl0UmVnID0gL1tcXHMsXSsvO1xuXG5mdW5jdGlvbiBjcmVhdGVQYXRoUHJveHlGcm9tU3RyaW5nKGRhdGEpIHtcbiAgaWYgKCFkYXRhKSB7XG4gICAgcmV0dXJuIG5ldyBQYXRoUHJveHkoKTtcbiAgfSAvLyB2YXIgZGF0YSA9IGRhdGEucmVwbGFjZSgvLS9nLCAnIC0nKVxuICAvLyAgICAgLnJlcGxhY2UoLyAgL2csICcgJylcbiAgLy8gICAgIC5yZXBsYWNlKC8gL2csICcsJylcbiAgLy8gICAgIC5yZXBsYWNlKC8sLC9nLCAnLCcpO1xuICAvLyB2YXIgbjtcbiAgLy8gY3JlYXRlIHBpcGVzIHNvIHRoYXQgd2UgY2FuIHNwbGl0IHRoZSBkYXRhXG4gIC8vIGZvciAobiA9IDA7IG4gPCBjYy5sZW5ndGg7IG4rKykge1xuICAvLyAgICAgY3MgPSBjcy5yZXBsYWNlKG5ldyBSZWdFeHAoY2Nbbl0sICdnJyksICd8JyArIGNjW25dKTtcbiAgLy8gfVxuICAvLyBkYXRhID0gZGF0YS5yZXBsYWNlKC8tL2csICcsLScpO1xuICAvLyBjcmVhdGUgYXJyYXlcbiAgLy8gdmFyIGFyciA9IGNzLnNwbGl0KCd8Jyk7XG4gIC8vIGluaXQgY29udGV4dCBwb2ludFxuXG5cbiAgdmFyIGNweCA9IDA7XG4gIHZhciBjcHkgPSAwO1xuICB2YXIgc3VicGF0aFggPSBjcHg7XG4gIHZhciBzdWJwYXRoWSA9IGNweTtcbiAgdmFyIHByZXZDbWQ7XG4gIHZhciBwYXRoID0gbmV3IFBhdGhQcm94eSgpO1xuICB2YXIgQ01EID0gUGF0aFByb3h5LkNNRDsgLy8gY29tbWFuZFJlZy5sYXN0SW5kZXggPSAwO1xuICAvLyB2YXIgY21kUmVzdWx0O1xuICAvLyB3aGlsZSAoKGNtZFJlc3VsdCA9IGNvbW1hbmRSZWcuZXhlYyhkYXRhKSkgIT0gbnVsbCkge1xuICAvLyAgICAgdmFyIGNtZFN0ciA9IGNtZFJlc3VsdFsxXTtcbiAgLy8gICAgIHZhciBjbWRDb250ZW50ID0gY21kUmVzdWx0WzJdO1xuXG4gIHZhciBjbWRMaXN0ID0gZGF0YS5tYXRjaChjb21tYW5kUmVnKTtcblxuICBmb3IgKHZhciBsID0gMDsgbCA8IGNtZExpc3QubGVuZ3RoOyBsKyspIHtcbiAgICB2YXIgY21kVGV4dCA9IGNtZExpc3RbbF07XG4gICAgdmFyIGNtZFN0ciA9IGNtZFRleHQuY2hhckF0KDApO1xuICAgIHZhciBjbWQ7IC8vIFN0cmluZyNzcGxpdCBpcyBmYXN0ZXIgYSBsaXR0bGUgYml0IHRoYW4gU3RyaW5nI3JlcGxhY2Ugb3IgUmVnRXhwI2V4ZWMuXG4gICAgLy8gdmFyIHAgPSBjbWRDb250ZW50LnNwbGl0KHZhbHVlU3BsaXRSZWcpO1xuICAgIC8vIHZhciBwTGVuID0gMDtcbiAgICAvLyBmb3IgKHZhciBpID0gMDsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAvLyAgICAgLy8gJycgYW5kIG90aGVyIGludmFsaWQgc3RyID0+IE5hTlxuICAgIC8vICAgICB2YXIgdmFsID0gcGFyc2VGbG9hdChwW2ldKTtcbiAgICAvLyAgICAgIWlzTmFOKHZhbCkgJiYgKHBbcExlbisrXSA9IHZhbCk7XG4gICAgLy8gfVxuXG4gICAgdmFyIHAgPSBjbWRUZXh0Lm1hdGNoKG51bWJlclJlZykgfHwgW107XG4gICAgdmFyIHBMZW4gPSBwLmxlbmd0aDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcExlbjsgaSsrKSB7XG4gICAgICBwW2ldID0gcGFyc2VGbG9hdChwW2ldKTtcbiAgICB9XG5cbiAgICB2YXIgb2ZmID0gMDtcblxuICAgIHdoaWxlIChvZmYgPCBwTGVuKSB7XG4gICAgICB2YXIgY3RsUHR4O1xuICAgICAgdmFyIGN0bFB0eTtcbiAgICAgIHZhciByeDtcbiAgICAgIHZhciByeTtcbiAgICAgIHZhciBwc2k7XG4gICAgICB2YXIgZmE7XG4gICAgICB2YXIgZnM7XG4gICAgICB2YXIgeDEgPSBjcHg7XG4gICAgICB2YXIgeTEgPSBjcHk7IC8vIGNvbnZlcnQgbCwgSCwgaCwgViwgYW5kIHYgdG8gTFxuXG4gICAgICBzd2l0Y2ggKGNtZFN0cikge1xuICAgICAgICBjYXNlICdsJzpcbiAgICAgICAgICBjcHggKz0gcFtvZmYrK107XG4gICAgICAgICAgY3B5ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5MO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGNweCwgY3B5KTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdMJzpcbiAgICAgICAgICBjcHggPSBwW29mZisrXTtcbiAgICAgICAgICBjcHkgPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuTDtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjcHgsIGNweSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAnbSc6XG4gICAgICAgICAgY3B4ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweSArPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuTTtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjcHgsIGNweSk7XG4gICAgICAgICAgc3VicGF0aFggPSBjcHg7XG4gICAgICAgICAgc3VicGF0aFkgPSBjcHk7XG4gICAgICAgICAgY21kU3RyID0gJ2wnO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ00nOlxuICAgICAgICAgIGNweCA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweSA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5NO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGNweCwgY3B5KTtcbiAgICAgICAgICBzdWJwYXRoWCA9IGNweDtcbiAgICAgICAgICBzdWJwYXRoWSA9IGNweTtcbiAgICAgICAgICBjbWRTdHIgPSAnTCc7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAnaCc6XG4gICAgICAgICAgY3B4ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5MO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGNweCwgY3B5KTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdIJzpcbiAgICAgICAgICBjcHggPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuTDtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjcHgsIGNweSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAndic6XG4gICAgICAgICAgY3B5ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5MO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGNweCwgY3B5KTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdWJzpcbiAgICAgICAgICBjcHkgPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuTDtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjcHgsIGNweSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAnQyc6XG4gICAgICAgICAgY21kID0gQ01ELkM7XG4gICAgICAgICAgcGF0aC5hZGREYXRhKGNtZCwgcFtvZmYrK10sIHBbb2ZmKytdLCBwW29mZisrXSwgcFtvZmYrK10sIHBbb2ZmKytdLCBwW29mZisrXSk7XG4gICAgICAgICAgY3B4ID0gcFtvZmYgLSAyXTtcbiAgICAgICAgICBjcHkgPSBwW29mZiAtIDFdO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ2MnOlxuICAgICAgICAgIGNtZCA9IENNRC5DO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIHBbb2ZmKytdICsgY3B4LCBwW29mZisrXSArIGNweSwgcFtvZmYrK10gKyBjcHgsIHBbb2ZmKytdICsgY3B5LCBwW29mZisrXSArIGNweCwgcFtvZmYrK10gKyBjcHkpO1xuICAgICAgICAgIGNweCArPSBwW29mZiAtIDJdO1xuICAgICAgICAgIGNweSArPSBwW29mZiAtIDFdO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ1MnOlxuICAgICAgICAgIGN0bFB0eCA9IGNweDtcbiAgICAgICAgICBjdGxQdHkgPSBjcHk7XG4gICAgICAgICAgdmFyIGxlbiA9IHBhdGgubGVuKCk7XG4gICAgICAgICAgdmFyIHBhdGhEYXRhID0gcGF0aC5kYXRhO1xuXG4gICAgICAgICAgaWYgKHByZXZDbWQgPT09IENNRC5DKSB7XG4gICAgICAgICAgICBjdGxQdHggKz0gY3B4IC0gcGF0aERhdGFbbGVuIC0gNF07XG4gICAgICAgICAgICBjdGxQdHkgKz0gY3B5IC0gcGF0aERhdGFbbGVuIC0gM107XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY21kID0gQ01ELkM7XG4gICAgICAgICAgeDEgPSBwW29mZisrXTtcbiAgICAgICAgICB5MSA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweCA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweSA9IHBbb2ZmKytdO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGN0bFB0eCwgY3RsUHR5LCB4MSwgeTEsIGNweCwgY3B5KTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdzJzpcbiAgICAgICAgICBjdGxQdHggPSBjcHg7XG4gICAgICAgICAgY3RsUHR5ID0gY3B5O1xuICAgICAgICAgIHZhciBsZW4gPSBwYXRoLmxlbigpO1xuICAgICAgICAgIHZhciBwYXRoRGF0YSA9IHBhdGguZGF0YTtcblxuICAgICAgICAgIGlmIChwcmV2Q21kID09PSBDTUQuQykge1xuICAgICAgICAgICAgY3RsUHR4ICs9IGNweCAtIHBhdGhEYXRhW2xlbiAtIDRdO1xuICAgICAgICAgICAgY3RsUHR5ICs9IGNweSAtIHBhdGhEYXRhW2xlbiAtIDNdO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNtZCA9IENNRC5DO1xuICAgICAgICAgIHgxID0gY3B4ICsgcFtvZmYrK107XG4gICAgICAgICAgeTEgPSBjcHkgKyBwW29mZisrXTtcbiAgICAgICAgICBjcHggKz0gcFtvZmYrK107XG4gICAgICAgICAgY3B5ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIHBhdGguYWRkRGF0YShjbWQsIGN0bFB0eCwgY3RsUHR5LCB4MSwgeTEsIGNweCwgY3B5KTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdRJzpcbiAgICAgICAgICB4MSA9IHBbb2ZmKytdO1xuICAgICAgICAgIHkxID0gcFtvZmYrK107XG4gICAgICAgICAgY3B4ID0gcFtvZmYrK107XG4gICAgICAgICAgY3B5ID0gcFtvZmYrK107XG4gICAgICAgICAgY21kID0gQ01ELlE7XG4gICAgICAgICAgcGF0aC5hZGREYXRhKGNtZCwgeDEsIHkxLCBjcHgsIGNweSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAncSc6XG4gICAgICAgICAgeDEgPSBwW29mZisrXSArIGNweDtcbiAgICAgICAgICB5MSA9IHBbb2ZmKytdICsgY3B5O1xuICAgICAgICAgIGNweCArPSBwW29mZisrXTtcbiAgICAgICAgICBjcHkgKz0gcFtvZmYrK107XG4gICAgICAgICAgY21kID0gQ01ELlE7XG4gICAgICAgICAgcGF0aC5hZGREYXRhKGNtZCwgeDEsIHkxLCBjcHgsIGNweSk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAnVCc6XG4gICAgICAgICAgY3RsUHR4ID0gY3B4O1xuICAgICAgICAgIGN0bFB0eSA9IGNweTtcbiAgICAgICAgICB2YXIgbGVuID0gcGF0aC5sZW4oKTtcbiAgICAgICAgICB2YXIgcGF0aERhdGEgPSBwYXRoLmRhdGE7XG5cbiAgICAgICAgICBpZiAocHJldkNtZCA9PT0gQ01ELlEpIHtcbiAgICAgICAgICAgIGN0bFB0eCArPSBjcHggLSBwYXRoRGF0YVtsZW4gLSA0XTtcbiAgICAgICAgICAgIGN0bFB0eSArPSBjcHkgLSBwYXRoRGF0YVtsZW4gLSAzXTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjcHggPSBwW29mZisrXTtcbiAgICAgICAgICBjcHkgPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuUTtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjdGxQdHgsIGN0bFB0eSwgY3B4LCBjcHkpO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ3QnOlxuICAgICAgICAgIGN0bFB0eCA9IGNweDtcbiAgICAgICAgICBjdGxQdHkgPSBjcHk7XG4gICAgICAgICAgdmFyIGxlbiA9IHBhdGgubGVuKCk7XG4gICAgICAgICAgdmFyIHBhdGhEYXRhID0gcGF0aC5kYXRhO1xuXG4gICAgICAgICAgaWYgKHByZXZDbWQgPT09IENNRC5RKSB7XG4gICAgICAgICAgICBjdGxQdHggKz0gY3B4IC0gcGF0aERhdGFbbGVuIC0gNF07XG4gICAgICAgICAgICBjdGxQdHkgKz0gY3B5IC0gcGF0aERhdGFbbGVuIC0gM107XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY3B4ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweSArPSBwW29mZisrXTtcbiAgICAgICAgICBjbWQgPSBDTUQuUTtcbiAgICAgICAgICBwYXRoLmFkZERhdGEoY21kLCBjdGxQdHgsIGN0bFB0eSwgY3B4LCBjcHkpO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIGNhc2UgJ0EnOlxuICAgICAgICAgIHJ4ID0gcFtvZmYrK107XG4gICAgICAgICAgcnkgPSBwW29mZisrXTtcbiAgICAgICAgICBwc2kgPSBwW29mZisrXTtcbiAgICAgICAgICBmYSA9IHBbb2ZmKytdO1xuICAgICAgICAgIGZzID0gcFtvZmYrK107XG4gICAgICAgICAgeDEgPSBjcHgsIHkxID0gY3B5O1xuICAgICAgICAgIGNweCA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNweSA9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5BO1xuICAgICAgICAgIHByb2Nlc3NBcmMoeDEsIHkxLCBjcHgsIGNweSwgZmEsIGZzLCByeCwgcnksIHBzaSwgY21kLCBwYXRoKTtcbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdhJzpcbiAgICAgICAgICByeCA9IHBbb2ZmKytdO1xuICAgICAgICAgIHJ5ID0gcFtvZmYrK107XG4gICAgICAgICAgcHNpID0gcFtvZmYrK107XG4gICAgICAgICAgZmEgPSBwW29mZisrXTtcbiAgICAgICAgICBmcyA9IHBbb2ZmKytdO1xuICAgICAgICAgIHgxID0gY3B4LCB5MSA9IGNweTtcbiAgICAgICAgICBjcHggKz0gcFtvZmYrK107XG4gICAgICAgICAgY3B5ICs9IHBbb2ZmKytdO1xuICAgICAgICAgIGNtZCA9IENNRC5BO1xuICAgICAgICAgIHByb2Nlc3NBcmMoeDEsIHkxLCBjcHgsIGNweSwgZmEsIGZzLCByeCwgcnksIHBzaSwgY21kLCBwYXRoKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY21kU3RyID09PSAneicgfHwgY21kU3RyID09PSAnWicpIHtcbiAgICAgIGNtZCA9IENNRC5aO1xuICAgICAgcGF0aC5hZGREYXRhKGNtZCk7IC8vIHogbWF5IGJlIGluIHRoZSBtaWRkbGUgb2YgdGhlIHBhdGguXG5cbiAgICAgIGNweCA9IHN1YnBhdGhYO1xuICAgICAgY3B5ID0gc3VicGF0aFk7XG4gICAgfVxuXG4gICAgcHJldkNtZCA9IGNtZDtcbiAgfVxuXG4gIHBhdGgudG9TdGF0aWMoKTtcbiAgcmV0dXJuIHBhdGg7XG59IC8vIFRPRE8gT3B0aW1pemUgZG91YmxlIG1lbW9yeSBjb3N0IHByb2JsZW1cblxuXG5mdW5jdGlvbiBjcmVhdGVQYXRoT3B0aW9ucyhzdHIsIG9wdHMpIHtcbiAgdmFyIHBhdGhQcm94eSA9IGNyZWF0ZVBhdGhQcm94eUZyb21TdHJpbmcoc3RyKTtcbiAgb3B0cyA9IG9wdHMgfHwge307XG5cbiAgb3B0cy5idWlsZFBhdGggPSBmdW5jdGlvbiAocGF0aCkge1xuICAgIGlmIChwYXRoLnNldERhdGEpIHtcbiAgICAgIHBhdGguc2V0RGF0YShwYXRoUHJveHkuZGF0YSk7IC8vIFN2ZyBhbmQgdm1sIHJlbmRlcmVyIGRvbid0IGhhdmUgY29udGV4dFxuXG4gICAgICB2YXIgY3R4ID0gcGF0aC5nZXRDb250ZXh0KCk7XG5cbiAgICAgIGlmIChjdHgpIHtcbiAgICAgICAgcGF0aC5yZWJ1aWxkUGF0aChjdHgpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgY3R4ID0gcGF0aDtcbiAgICAgIHBhdGhQcm94eS5yZWJ1aWxkUGF0aChjdHgpO1xuICAgIH1cbiAgfTtcblxuICBvcHRzLmFwcGx5VHJhbnNmb3JtID0gZnVuY3Rpb24gKG0pIHtcbiAgICB0cmFuc2Zvcm1QYXRoKHBhdGhQcm94eSwgbSk7XG4gICAgdGhpcy5kaXJ0eSh0cnVlKTtcbiAgfTtcblxuICByZXR1cm4gb3B0cztcbn1cbi8qKlxuICogQ3JlYXRlIGEgUGF0aCBvYmplY3QgZnJvbSBwYXRoIHN0cmluZyBkYXRhXG4gKiBodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcvcGF0aHMuaHRtbCNQYXRoRGF0YVxuICogQHBhcmFtICB7T2JqZWN0fSBvcHRzIE90aGVyIG9wdGlvbnNcbiAqL1xuXG5cbmZ1bmN0aW9uIGNyZWF0ZUZyb21TdHJpbmcoc3RyLCBvcHRzKSB7XG4gIHJldHVybiBuZXcgUGF0aChjcmVhdGVQYXRoT3B0aW9ucyhzdHIsIG9wdHMpKTtcbn1cbi8qKlxuICogQ3JlYXRlIGEgUGF0aCBjbGFzcyBmcm9tIHBhdGggc3RyaW5nIGRhdGFcbiAqIEBwYXJhbSAge3N0cmluZ30gc3RyXG4gKiBAcGFyYW0gIHtPYmplY3R9IG9wdHMgT3RoZXIgb3B0aW9uc1xuICovXG5cblxuZnVuY3Rpb24gZXh0ZW5kRnJvbVN0cmluZyhzdHIsIG9wdHMpIHtcbiAgcmV0dXJuIFBhdGguZXh0ZW5kKGNyZWF0ZVBhdGhPcHRpb25zKHN0ciwgb3B0cykpO1xufVxuLyoqXG4gKiBNZXJnZSBtdWx0aXBsZSBwYXRoc1xuICovXG4vLyBUT0RPIEFwcGx5IHRyYW5zZm9ybVxuLy8gVE9ETyBzdHJva2UgZGFzaFxuLy8gVE9ETyBPcHRpbWl6ZSBkb3VibGUgbWVtb3J5IGNvc3QgcHJvYmxlbVxuXG5cbmZ1bmN0aW9uIG1lcmdlUGF0aChwYXRoRWxzLCBvcHRzKSB7XG4gIHZhciBwYXRoTGlzdCA9IFtdO1xuICB2YXIgbGVuID0gcGF0aEVscy5sZW5ndGg7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuICAgIHZhciBwYXRoRWwgPSBwYXRoRWxzW2ldO1xuXG4gICAgaWYgKCFwYXRoRWwucGF0aCkge1xuICAgICAgcGF0aEVsLmNyZWF0ZVBhdGhQcm94eSgpO1xuICAgIH1cblxuICAgIGlmIChwYXRoRWwuX19kaXJ0eVBhdGgpIHtcbiAgICAgIHBhdGhFbC5idWlsZFBhdGgocGF0aEVsLnBhdGgsIHBhdGhFbC5zaGFwZSwgdHJ1ZSk7XG4gICAgfVxuXG4gICAgcGF0aExpc3QucHVzaChwYXRoRWwucGF0aCk7XG4gIH1cblxuICB2YXIgcGF0aEJ1bmRsZSA9IG5ldyBQYXRoKG9wdHMpOyAvLyBOZWVkIHBhdGggcHJveHkuXG5cbiAgcGF0aEJ1bmRsZS5jcmVhdGVQYXRoUHJveHkoKTtcblxuICBwYXRoQnVuZGxlLmJ1aWxkUGF0aCA9IGZ1bmN0aW9uIChwYXRoKSB7XG4gICAgcGF0aC5hcHBlbmRQYXRoKHBhdGhMaXN0KTsgLy8gU3ZnIGFuZCB2bWwgcmVuZGVyZXIgZG9uJ3QgaGF2ZSBjb250ZXh0XG5cbiAgICB2YXIgY3R4ID0gcGF0aC5nZXRDb250ZXh0KCk7XG5cbiAgICBpZiAoY3R4KSB7XG4gICAgICBwYXRoLnJlYnVpbGRQYXRoKGN0eCk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiBwYXRoQnVuZGxlO1xufVxuXG5leHBvcnRzLmNyZWF0ZUZyb21TdHJpbmcgPSBjcmVhdGVGcm9tU3RyaW5nO1xuZXhwb3J0cy5leHRlbmRGcm9tU3RyaW5nID0gZXh0ZW5kRnJvbVN0cmluZztcbmV4cG9ydHMubWVyZ2VQYXRoID0gbWVyZ2VQYXRoOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTNMQTtBQTZMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/tool/path.js
