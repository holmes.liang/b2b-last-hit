/* WEBPACK VAR INJECTION */(function(global) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule getTextContentFromFiles
 * @format
 * 
 */


var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

var TEXT_CLIPPING_REGEX = /\.textClipping$/;
var TEXT_TYPES = {
  'text/plain': true,
  'text/html': true,
  'text/rtf': true
}; // Somewhat arbitrary upper bound on text size. Let's not lock up the browser.

var TEXT_SIZE_UPPER_BOUND = 5000;
/**
 * Extract the text content from a file list.
 */

function getTextContentFromFiles(files, callback) {
  var readCount = 0;
  var results = [];
  files.forEach(function (
  /*blob*/
  file) {
    readFile(file, function (
    /*string*/
    text) {
      readCount++;
      text && results.push(text.slice(0, TEXT_SIZE_UPPER_BOUND));

      if (readCount == files.length) {
        callback(results.join('\r'));
      }
    });
  });
}
/**
 * todo isaac: Do work to turn html/rtf into a content fragment.
 */


function readFile(file, callback) {
  if (!global.FileReader || file.type && !(file.type in TEXT_TYPES)) {
    callback('');
    return;
  }

  if (file.type === '') {
    var contents = ''; // Special-case text clippings, which have an empty type but include
    // `.textClipping` in the file name. `readAsText` results in an empty
    // string for text clippings, so we force the file name to serve
    // as the text value for the file.

    if (TEXT_CLIPPING_REGEX.test(file.name)) {
      contents = file.name.replace(TEXT_CLIPPING_REGEX, '');
    }

    callback(contents);
    return;
  }

  var reader = new FileReader();

  reader.onload = function () {
    var result = reader.result;
    !(typeof result === 'string') ?  true ? invariant(false, 'We should be calling "FileReader.readAsText" which returns a string') : undefined : void 0;
    callback(result);
  };

  reader.onerror = function () {
    callback('');
  };

  reader.readAsText(file);
}

module.exports = getTextContentFromFiles;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFRleHRDb250ZW50RnJvbUZpbGVzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2dldFRleHRDb250ZW50RnJvbUZpbGVzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZ2V0VGV4dENvbnRlbnRGcm9tRmlsZXNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIGludmFyaWFudCA9IHJlcXVpcmUoJ2ZianMvbGliL2ludmFyaWFudCcpO1xuXG52YXIgVEVYVF9DTElQUElOR19SRUdFWCA9IC9cXC50ZXh0Q2xpcHBpbmckLztcblxudmFyIFRFWFRfVFlQRVMgPSB7XG4gICd0ZXh0L3BsYWluJzogdHJ1ZSxcbiAgJ3RleHQvaHRtbCc6IHRydWUsXG4gICd0ZXh0L3J0Zic6IHRydWVcbn07XG5cbi8vIFNvbWV3aGF0IGFyYml0cmFyeSB1cHBlciBib3VuZCBvbiB0ZXh0IHNpemUuIExldCdzIG5vdCBsb2NrIHVwIHRoZSBicm93c2VyLlxudmFyIFRFWFRfU0laRV9VUFBFUl9CT1VORCA9IDUwMDA7XG5cbi8qKlxuICogRXh0cmFjdCB0aGUgdGV4dCBjb250ZW50IGZyb20gYSBmaWxlIGxpc3QuXG4gKi9cbmZ1bmN0aW9uIGdldFRleHRDb250ZW50RnJvbUZpbGVzKGZpbGVzLCBjYWxsYmFjaykge1xuICB2YXIgcmVhZENvdW50ID0gMDtcbiAgdmFyIHJlc3VsdHMgPSBbXTtcbiAgZmlsZXMuZm9yRWFjaChmdW5jdGlvbiAoIC8qYmxvYiovZmlsZSkge1xuICAgIHJlYWRGaWxlKGZpbGUsIGZ1bmN0aW9uICggLypzdHJpbmcqL3RleHQpIHtcbiAgICAgIHJlYWRDb3VudCsrO1xuICAgICAgdGV4dCAmJiByZXN1bHRzLnB1c2godGV4dC5zbGljZSgwLCBURVhUX1NJWkVfVVBQRVJfQk9VTkQpKTtcbiAgICAgIGlmIChyZWFkQ291bnQgPT0gZmlsZXMubGVuZ3RoKSB7XG4gICAgICAgIGNhbGxiYWNrKHJlc3VsdHMuam9pbignXFxyJykpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn1cblxuLyoqXG4gKiB0b2RvIGlzYWFjOiBEbyB3b3JrIHRvIHR1cm4gaHRtbC9ydGYgaW50byBhIGNvbnRlbnQgZnJhZ21lbnQuXG4gKi9cbmZ1bmN0aW9uIHJlYWRGaWxlKGZpbGUsIGNhbGxiYWNrKSB7XG4gIGlmICghZ2xvYmFsLkZpbGVSZWFkZXIgfHwgZmlsZS50eXBlICYmICEoZmlsZS50eXBlIGluIFRFWFRfVFlQRVMpKSB7XG4gICAgY2FsbGJhY2soJycpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGlmIChmaWxlLnR5cGUgPT09ICcnKSB7XG4gICAgdmFyIGNvbnRlbnRzID0gJyc7XG4gICAgLy8gU3BlY2lhbC1jYXNlIHRleHQgY2xpcHBpbmdzLCB3aGljaCBoYXZlIGFuIGVtcHR5IHR5cGUgYnV0IGluY2x1ZGVcbiAgICAvLyBgLnRleHRDbGlwcGluZ2AgaW4gdGhlIGZpbGUgbmFtZS4gYHJlYWRBc1RleHRgIHJlc3VsdHMgaW4gYW4gZW1wdHlcbiAgICAvLyBzdHJpbmcgZm9yIHRleHQgY2xpcHBpbmdzLCBzbyB3ZSBmb3JjZSB0aGUgZmlsZSBuYW1lIHRvIHNlcnZlXG4gICAgLy8gYXMgdGhlIHRleHQgdmFsdWUgZm9yIHRoZSBmaWxlLlxuICAgIGlmIChURVhUX0NMSVBQSU5HX1JFR0VYLnRlc3QoZmlsZS5uYW1lKSkge1xuICAgICAgY29udGVudHMgPSBmaWxlLm5hbWUucmVwbGFjZShURVhUX0NMSVBQSU5HX1JFR0VYLCAnJyk7XG4gICAgfVxuICAgIGNhbGxiYWNrKGNvbnRlbnRzKTtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcbiAgcmVhZGVyLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcmVzdWx0ID0gcmVhZGVyLnJlc3VsdDtcbiAgICAhKHR5cGVvZiByZXN1bHQgPT09ICdzdHJpbmcnKSA/IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgPyBpbnZhcmlhbnQoZmFsc2UsICdXZSBzaG91bGQgYmUgY2FsbGluZyBcIkZpbGVSZWFkZXIucmVhZEFzVGV4dFwiIHdoaWNoIHJldHVybnMgYSBzdHJpbmcnKSA6IGludmFyaWFudChmYWxzZSkgOiB2b2lkIDA7XG4gICAgY2FsbGJhY2socmVzdWx0KTtcbiAgfTtcbiAgcmVhZGVyLm9uZXJyb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgY2FsbGJhY2soJycpO1xuICB9O1xuICByZWFkZXIucmVhZEFzVGV4dChmaWxlKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRUZXh0Q29udGVudEZyb21GaWxlczsiXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBTUE7QUFFQTs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/getTextContentFromFiles.js
