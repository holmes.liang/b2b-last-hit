__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../locale-provider/LocaleReceiver */ "./node_modules/antd/es/locale-provider/LocaleReceiver.js");
/* harmony import */ var _empty__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./empty */ "./node_modules/antd/es/empty/empty.js");
/* harmony import */ var _simple__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./simple */ "./node_modules/antd/es/empty/simple.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};







var defaultEmptyImg = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_empty__WEBPACK_IMPORTED_MODULE_4__["default"], null);
var simpleEmptyImg = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_simple__WEBPACK_IMPORTED_MODULE_5__["default"], null);

var Empty = function Empty(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_2__["ConfigConsumer"], null, function (_ref) {
    var getPrefixCls = _ref.getPrefixCls;

    var className = props.className,
        customizePrefixCls = props.prefixCls,
        _props$image = props.image,
        image = _props$image === void 0 ? defaultEmptyImg : _props$image,
        description = props.description,
        children = props.children,
        imageStyle = props.imageStyle,
        restProps = __rest(props, ["className", "prefixCls", "image", "description", "children", "imageStyle"]);

    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_locale_provider_LocaleReceiver__WEBPACK_IMPORTED_MODULE_3__["default"], {
      componentName: "Empty"
    }, function (locale) {
      var prefixCls = getPrefixCls('empty', customizePrefixCls);
      var des = typeof description !== 'undefined' ? description : locale.description;
      var alt = typeof des === 'string' ? des : 'empty';
      var imageNode = null;

      if (typeof image === 'string') {
        imageNode = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", {
          alt: alt,
          src: image
        });
      } else {
        imageNode = image;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({
        className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(prefixCls, _defineProperty({}, "".concat(prefixCls, "-normal"), image === simpleEmptyImg), className)
      }, restProps), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-image"),
        style: imageStyle
      }, imageNode), des && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", {
        className: "".concat(prefixCls, "-description")
      }, des), children && react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
        className: "".concat(prefixCls, "-footer")
      }, children));
    });
  });
};

Empty.PRESENTED_IMAGE_DEFAULT = defaultEmptyImg;
Empty.PRESENTED_IMAGE_SIMPLE = simpleEmptyImg;
/* harmony default export */ __webpack_exports__["default"] = (Empty);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9lbXB0eS9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvZW1wdHkvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgQ29uZmlnQ29uc3VtZXIgfSBmcm9tICcuLi9jb25maWctcHJvdmlkZXInO1xuaW1wb3J0IExvY2FsZVJlY2VpdmVyIGZyb20gJy4uL2xvY2FsZS1wcm92aWRlci9Mb2NhbGVSZWNlaXZlcic7XG5pbXBvcnQgRGVmYXVsdEVtcHR5SW1nIGZyb20gJy4vZW1wdHknO1xuaW1wb3J0IFNpbXBsZUVtcHR5SW1nIGZyb20gJy4vc2ltcGxlJztcbmNvbnN0IGRlZmF1bHRFbXB0eUltZyA9IDxEZWZhdWx0RW1wdHlJbWcgLz47XG5jb25zdCBzaW1wbGVFbXB0eUltZyA9IDxTaW1wbGVFbXB0eUltZyAvPjtcbmNvbnN0IEVtcHR5ID0gKHByb3BzKSA9PiAoPENvbmZpZ0NvbnN1bWVyPlxuICAgIHsoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgIGNvbnN0IHsgY2xhc3NOYW1lLCBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgaW1hZ2UgPSBkZWZhdWx0RW1wdHlJbWcsIGRlc2NyaXB0aW9uLCBjaGlsZHJlbiwgaW1hZ2VTdHlsZSB9ID0gcHJvcHMsIHJlc3RQcm9wcyA9IF9fcmVzdChwcm9wcywgW1wiY2xhc3NOYW1lXCIsIFwicHJlZml4Q2xzXCIsIFwiaW1hZ2VcIiwgXCJkZXNjcmlwdGlvblwiLCBcImNoaWxkcmVuXCIsIFwiaW1hZ2VTdHlsZVwiXSk7XG4gICAgcmV0dXJuICg8TG9jYWxlUmVjZWl2ZXIgY29tcG9uZW50TmFtZT1cIkVtcHR5XCI+XG4gICAgICAgICAgeyhsb2NhbGUpID0+IHtcbiAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdlbXB0eScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgIGNvbnN0IGRlcyA9IHR5cGVvZiBkZXNjcmlwdGlvbiAhPT0gJ3VuZGVmaW5lZCcgPyBkZXNjcmlwdGlvbiA6IGxvY2FsZS5kZXNjcmlwdGlvbjtcbiAgICAgICAgY29uc3QgYWx0ID0gdHlwZW9mIGRlcyA9PT0gJ3N0cmluZycgPyBkZXMgOiAnZW1wdHknO1xuICAgICAgICBsZXQgaW1hZ2VOb2RlID0gbnVsbDtcbiAgICAgICAgaWYgKHR5cGVvZiBpbWFnZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIGltYWdlTm9kZSA9IDxpbWcgYWx0PXthbHR9IHNyYz17aW1hZ2V9Lz47XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBpbWFnZU5vZGUgPSBpbWFnZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gKDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHByZWZpeENscywge1xuICAgICAgICAgICAgW2Ake3ByZWZpeENsc30tbm9ybWFsYF06IGltYWdlID09PSBzaW1wbGVFbXB0eUltZyxcbiAgICAgICAgfSwgY2xhc3NOYW1lKX0gey4uLnJlc3RQcm9wc30+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Ake3ByZWZpeENsc30taW1hZ2VgfSBzdHlsZT17aW1hZ2VTdHlsZX0+XG4gICAgICAgICAgICAgICAgICB7aW1hZ2VOb2RlfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIHtkZXMgJiYgPHAgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWRlc2NyaXB0aW9uYH0+e2Rlc308L3A+fVxuICAgICAgICAgICAgICAgIHtjaGlsZHJlbiAmJiA8ZGl2IGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1mb290ZXJgfT57Y2hpbGRyZW59PC9kaXY+fVxuICAgICAgICAgICAgICA8L2Rpdj4pO1xuICAgIH19XG4gICAgICAgIDwvTG9jYWxlUmVjZWl2ZXI+KTtcbn19XG4gIDwvQ29uZmlnQ29uc3VtZXI+KTtcbkVtcHR5LlBSRVNFTlRFRF9JTUFHRV9ERUZBVUxUID0gZGVmYXVsdEVtcHR5SW1nO1xuRW1wdHkuUFJFU0VOVEVEX0lNQUdFX1NJTVBMRSA9IHNpbXBsZUVtcHR5SW1nO1xuZXhwb3J0IGRlZmF1bHQgRW1wdHk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUNBO0FBQUE7QUFuQkE7QUFIQTtBQUFBO0FBQ0E7QUEyQkE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/empty/index.js
