/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var createHashMap = _util.createHashMap;
var each = _util.each;
var isString = _util.isString;
var defaults = _util.defaults;
var extend = _util.extend;
var isObject = _util.isObject;
var clone = _util.clone;

var _model = __webpack_require__(/*! ../../util/model */ "./node_modules/echarts/lib/util/model.js");

var normalizeToArray = _model.normalizeToArray;

var _sourceHelper = __webpack_require__(/*! ./sourceHelper */ "./node_modules/echarts/lib/data/helper/sourceHelper.js");

var guessOrdinal = _sourceHelper.guessOrdinal;

var Source = __webpack_require__(/*! ../Source */ "./node_modules/echarts/lib/data/Source.js");

var _dimensionHelper = __webpack_require__(/*! ./dimensionHelper */ "./node_modules/echarts/lib/data/helper/dimensionHelper.js");

var OTHER_DIMENSIONS = _dimensionHelper.OTHER_DIMENSIONS;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @deprecated
 * Use `echarts/data/helper/createDimensions` instead.
 */

/**
 * @see {module:echarts/test/ut/spec/data/completeDimensions}
 *
 * Complete the dimensions array, by user defined `dimension` and `encode`,
 * and guessing from the data structure.
 * If no 'value' dimension specified, the first no-named dimension will be
 * named as 'value'.
 *
 * @param {Array.<string>} sysDims Necessary dimensions, like ['x', 'y'], which
 *      provides not only dim template, but also default order.
 *      properties: 'name', 'type', 'displayName'.
 *      `name` of each item provides default coord name.
 *      [{dimsDef: [string|Object, ...]}, ...] dimsDef of sysDim item provides default dim name, and
 *                                    provide dims count that the sysDim required.
 *      [{ordinalMeta}] can be specified.
 * @param {module:echarts/data/Source|Array|Object} source or data (for compatibal with pervious)
 * @param {Object} [opt]
 * @param {Array.<Object|string>} [opt.dimsDef] option.series.dimensions User defined dimensions
 *      For example: ['asdf', {name, type}, ...].
 * @param {Object|HashMap} [opt.encodeDef] option.series.encode {x: 2, y: [3, 1], tooltip: [1, 2], label: 3}
 * @param {string} [opt.generateCoord] Generate coord dim with the given name.
 *                 If not specified, extra dim names will be:
 *                 'value', 'value0', 'value1', ...
 * @param {number} [opt.generateCoordCount] By default, the generated dim name is `generateCoord`.
 *                 If `generateCoordCount` specified, the generated dim names will be:
 *                 `generateCoord` + 0, `generateCoord` + 1, ...
 *                 can be Infinity, indicate that use all of the remain columns.
 * @param {number} [opt.dimCount] If not specified, guess by the first data item.
 * @param {number} [opt.encodeDefaulter] If not specified, auto find the next available data dim.
 * @return {Array.<Object>} [{
 *      name: string mandatory,
 *      displayName: string, the origin name in dimsDef, see source helper.
 *                 If displayName given, the tooltip will displayed vertically.
 *      coordDim: string mandatory,
 *      coordDimIndex: number mandatory,
 *      type: string optional,
 *      otherDims: { never null/undefined
 *          tooltip: number optional,
 *          label: number optional,
 *          itemName: number optional,
 *          seriesName: number optional,
 *      },
 *      isExtraCoord: boolean true if coord is generated
 *          (not specified in encode and not series specified)
 *      other props ...
 * }]
 */

function completeDimensions(sysDims, source, opt) {
  if (!Source.isInstance(source)) {
    source = Source.seriesDataToSource(source);
  }

  opt = opt || {};
  sysDims = (sysDims || []).slice();
  var dimsDef = (opt.dimsDef || []).slice();
  var encodeDef = createHashMap(opt.encodeDef);
  var dataDimNameMap = createHashMap();
  var coordDimNameMap = createHashMap(); // var valueCandidate;

  var result = [];
  var dimCount = getDimCount(source, sysDims, dimsDef, opt.dimCount); // Apply user defined dims (`name` and `type`) and init result.

  for (var i = 0; i < dimCount; i++) {
    var dimDefItem = dimsDef[i] = extend({}, isObject(dimsDef[i]) ? dimsDef[i] : {
      name: dimsDef[i]
    });
    var userDimName = dimDefItem.name;
    var resultItem = result[i] = {
      otherDims: {}
    }; // Name will be applied later for avoiding duplication.

    if (userDimName != null && dataDimNameMap.get(userDimName) == null) {
      // Only if `series.dimensions` is defined in option
      // displayName, will be set, and dimension will be diplayed vertically in
      // tooltip by default.
      resultItem.name = resultItem.displayName = userDimName;
      dataDimNameMap.set(userDimName, i);
    }

    dimDefItem.type != null && (resultItem.type = dimDefItem.type);
    dimDefItem.displayName != null && (resultItem.displayName = dimDefItem.displayName);
  } // Set `coordDim` and `coordDimIndex` by `encodeDef` and normalize `encodeDef`.


  encodeDef.each(function (dataDims, coordDim) {
    dataDims = normalizeToArray(dataDims).slice(); // Note: It is allowed that `dataDims.length` is `0`, e.g., options is
    // `{encode: {x: -1, y: 1}}`. Should not filter anything in
    // this case.

    if (dataDims.length === 1 && dataDims[0] < 0) {
      encodeDef.set(coordDim, false);
      return;
    }

    var validDataDims = encodeDef.set(coordDim, []);
    each(dataDims, function (resultDimIdx, idx) {
      // The input resultDimIdx can be dim name or index.
      isString(resultDimIdx) && (resultDimIdx = dataDimNameMap.get(resultDimIdx));

      if (resultDimIdx != null && resultDimIdx < dimCount) {
        validDataDims[idx] = resultDimIdx;
        applyDim(result[resultDimIdx], coordDim, idx);
      }
    });
  }); // Apply templetes and default order from `sysDims`.

  var availDimIdx = 0;
  each(sysDims, function (sysDimItem, sysDimIndex) {
    var coordDim;
    var sysDimItem;
    var sysDimItemDimsDef;
    var sysDimItemOtherDims;

    if (isString(sysDimItem)) {
      coordDim = sysDimItem;
      sysDimItem = {};
    } else {
      coordDim = sysDimItem.name;
      var ordinalMeta = sysDimItem.ordinalMeta;
      sysDimItem.ordinalMeta = null;
      sysDimItem = clone(sysDimItem);
      sysDimItem.ordinalMeta = ordinalMeta; // `coordDimIndex` should not be set directly.

      sysDimItemDimsDef = sysDimItem.dimsDef;
      sysDimItemOtherDims = sysDimItem.otherDims;
      sysDimItem.name = sysDimItem.coordDim = sysDimItem.coordDimIndex = sysDimItem.dimsDef = sysDimItem.otherDims = null;
    }

    var dataDims = encodeDef.get(coordDim); // negative resultDimIdx means no need to mapping.

    if (dataDims === false) {
      return;
    }

    var dataDims = normalizeToArray(dataDims); // dimensions provides default dim sequences.

    if (!dataDims.length) {
      for (var i = 0; i < (sysDimItemDimsDef && sysDimItemDimsDef.length || 1); i++) {
        while (availDimIdx < result.length && result[availDimIdx].coordDim != null) {
          availDimIdx++;
        }

        availDimIdx < result.length && dataDims.push(availDimIdx++);
      }
    } // Apply templates.


    each(dataDims, function (resultDimIdx, coordDimIndex) {
      var resultItem = result[resultDimIdx];
      applyDim(defaults(resultItem, sysDimItem), coordDim, coordDimIndex);

      if (resultItem.name == null && sysDimItemDimsDef) {
        var sysDimItemDimsDefItem = sysDimItemDimsDef[coordDimIndex];
        !isObject(sysDimItemDimsDefItem) && (sysDimItemDimsDefItem = {
          name: sysDimItemDimsDefItem
        });
        resultItem.name = resultItem.displayName = sysDimItemDimsDefItem.name;
        resultItem.defaultTooltip = sysDimItemDimsDefItem.defaultTooltip;
      } // FIXME refactor, currently only used in case: {otherDims: {tooltip: false}}


      sysDimItemOtherDims && defaults(resultItem.otherDims, sysDimItemOtherDims);
    });
  });

  function applyDim(resultItem, coordDim, coordDimIndex) {
    if (OTHER_DIMENSIONS.get(coordDim) != null) {
      resultItem.otherDims[coordDim] = coordDimIndex;
    } else {
      resultItem.coordDim = coordDim;
      resultItem.coordDimIndex = coordDimIndex;
      coordDimNameMap.set(coordDim, true);
    }
  } // Make sure the first extra dim is 'value'.


  var generateCoord = opt.generateCoord;
  var generateCoordCount = opt.generateCoordCount;
  var fromZero = generateCoordCount != null;
  generateCoordCount = generateCoord ? generateCoordCount || 1 : 0;
  var extra = generateCoord || 'value'; // Set dim `name` and other `coordDim` and other props.

  for (var resultDimIdx = 0; resultDimIdx < dimCount; resultDimIdx++) {
    var resultItem = result[resultDimIdx] = result[resultDimIdx] || {};
    var coordDim = resultItem.coordDim;

    if (coordDim == null) {
      resultItem.coordDim = genName(extra, coordDimNameMap, fromZero);
      resultItem.coordDimIndex = 0;

      if (!generateCoord || generateCoordCount <= 0) {
        resultItem.isExtraCoord = true;
      }

      generateCoordCount--;
    }

    resultItem.name == null && (resultItem.name = genName(resultItem.coordDim, dataDimNameMap));

    if (resultItem.type == null && guessOrdinal(source, resultDimIdx, resultItem.name)) {
      resultItem.type = 'ordinal';
    }
  }

  return result;
} // ??? TODO
// Originally detect dimCount by data[0]. Should we
// optimize it to only by sysDims and dimensions and encode.
// So only necessary dims will be initialized.
// But
// (1) custom series should be considered. where other dims
// may be visited.
// (2) sometimes user need to calcualte bubble size or use visualMap
// on other dimensions besides coordSys needed.
// So, dims that is not used by system, should be shared in storage?


function getDimCount(source, sysDims, dimsDef, optDimCount) {
  // Note that the result dimCount should not small than columns count
  // of data, otherwise `dataDimNameMap` checking will be incorrect.
  var dimCount = Math.max(source.dimensionsDetectCount || 1, sysDims.length, dimsDef.length, optDimCount || 0);
  each(sysDims, function (sysDimItem) {
    var sysDimItemDimsDef = sysDimItem.dimsDef;
    sysDimItemDimsDef && (dimCount = Math.max(dimCount, sysDimItemDimsDef.length));
  });
  return dimCount;
}

function genName(name, map, fromZero) {
  if (fromZero || map.get(name) != null) {
    var i = 0;

    while (map.get(name + i) != null) {
      i++;
    }

    name += i;
  }

  map.set(name, true);
  return name;
}

var _default = completeDimensions;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvY29tcGxldGVEaW1lbnNpb25zLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvZGF0YS9oZWxwZXIvY29tcGxldGVEaW1lbnNpb25zLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgY3JlYXRlSGFzaE1hcCA9IF91dGlsLmNyZWF0ZUhhc2hNYXA7XG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgaXNTdHJpbmcgPSBfdXRpbC5pc1N0cmluZztcbnZhciBkZWZhdWx0cyA9IF91dGlsLmRlZmF1bHRzO1xudmFyIGV4dGVuZCA9IF91dGlsLmV4dGVuZDtcbnZhciBpc09iamVjdCA9IF91dGlsLmlzT2JqZWN0O1xudmFyIGNsb25lID0gX3V0aWwuY2xvbmU7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIG5vcm1hbGl6ZVRvQXJyYXkgPSBfbW9kZWwubm9ybWFsaXplVG9BcnJheTtcblxudmFyIF9zb3VyY2VIZWxwZXIgPSByZXF1aXJlKFwiLi9zb3VyY2VIZWxwZXJcIik7XG5cbnZhciBndWVzc09yZGluYWwgPSBfc291cmNlSGVscGVyLmd1ZXNzT3JkaW5hbDtcblxudmFyIFNvdXJjZSA9IHJlcXVpcmUoXCIuLi9Tb3VyY2VcIik7XG5cbnZhciBfZGltZW5zaW9uSGVscGVyID0gcmVxdWlyZShcIi4vZGltZW5zaW9uSGVscGVyXCIpO1xuXG52YXIgT1RIRVJfRElNRU5TSU9OUyA9IF9kaW1lbnNpb25IZWxwZXIuT1RIRVJfRElNRU5TSU9OUztcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBkZXByZWNhdGVkXG4gKiBVc2UgYGVjaGFydHMvZGF0YS9oZWxwZXIvY3JlYXRlRGltZW5zaW9uc2AgaW5zdGVhZC5cbiAqL1xuXG4vKipcbiAqIEBzZWUge21vZHVsZTplY2hhcnRzL3Rlc3QvdXQvc3BlYy9kYXRhL2NvbXBsZXRlRGltZW5zaW9uc31cbiAqXG4gKiBDb21wbGV0ZSB0aGUgZGltZW5zaW9ucyBhcnJheSwgYnkgdXNlciBkZWZpbmVkIGBkaW1lbnNpb25gIGFuZCBgZW5jb2RlYCxcbiAqIGFuZCBndWVzc2luZyBmcm9tIHRoZSBkYXRhIHN0cnVjdHVyZS5cbiAqIElmIG5vICd2YWx1ZScgZGltZW5zaW9uIHNwZWNpZmllZCwgdGhlIGZpcnN0IG5vLW5hbWVkIGRpbWVuc2lvbiB3aWxsIGJlXG4gKiBuYW1lZCBhcyAndmFsdWUnLlxuICpcbiAqIEBwYXJhbSB7QXJyYXkuPHN0cmluZz59IHN5c0RpbXMgTmVjZXNzYXJ5IGRpbWVuc2lvbnMsIGxpa2UgWyd4JywgJ3knXSwgd2hpY2hcbiAqICAgICAgcHJvdmlkZXMgbm90IG9ubHkgZGltIHRlbXBsYXRlLCBidXQgYWxzbyBkZWZhdWx0IG9yZGVyLlxuICogICAgICBwcm9wZXJ0aWVzOiAnbmFtZScsICd0eXBlJywgJ2Rpc3BsYXlOYW1lJy5cbiAqICAgICAgYG5hbWVgIG9mIGVhY2ggaXRlbSBwcm92aWRlcyBkZWZhdWx0IGNvb3JkIG5hbWUuXG4gKiAgICAgIFt7ZGltc0RlZjogW3N0cmluZ3xPYmplY3QsIC4uLl19LCAuLi5dIGRpbXNEZWYgb2Ygc3lzRGltIGl0ZW0gcHJvdmlkZXMgZGVmYXVsdCBkaW0gbmFtZSwgYW5kXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3ZpZGUgZGltcyBjb3VudCB0aGF0IHRoZSBzeXNEaW0gcmVxdWlyZWQuXG4gKiAgICAgIFt7b3JkaW5hbE1ldGF9XSBjYW4gYmUgc3BlY2lmaWVkLlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL1NvdXJjZXxBcnJheXxPYmplY3R9IHNvdXJjZSBvciBkYXRhIChmb3IgY29tcGF0aWJhbCB3aXRoIHBlcnZpb3VzKVxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRdXG4gKiBAcGFyYW0ge0FycmF5LjxPYmplY3R8c3RyaW5nPn0gW29wdC5kaW1zRGVmXSBvcHRpb24uc2VyaWVzLmRpbWVuc2lvbnMgVXNlciBkZWZpbmVkIGRpbWVuc2lvbnNcbiAqICAgICAgRm9yIGV4YW1wbGU6IFsnYXNkZicsIHtuYW1lLCB0eXBlfSwgLi4uXS5cbiAqIEBwYXJhbSB7T2JqZWN0fEhhc2hNYXB9IFtvcHQuZW5jb2RlRGVmXSBvcHRpb24uc2VyaWVzLmVuY29kZSB7eDogMiwgeTogWzMsIDFdLCB0b29sdGlwOiBbMSwgMl0sIGxhYmVsOiAzfVxuICogQHBhcmFtIHtzdHJpbmd9IFtvcHQuZ2VuZXJhdGVDb29yZF0gR2VuZXJhdGUgY29vcmQgZGltIHdpdGggdGhlIGdpdmVuIG5hbWUuXG4gKiAgICAgICAgICAgICAgICAgSWYgbm90IHNwZWNpZmllZCwgZXh0cmEgZGltIG5hbWVzIHdpbGwgYmU6XG4gKiAgICAgICAgICAgICAgICAgJ3ZhbHVlJywgJ3ZhbHVlMCcsICd2YWx1ZTEnLCAuLi5cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LmdlbmVyYXRlQ29vcmRDb3VudF0gQnkgZGVmYXVsdCwgdGhlIGdlbmVyYXRlZCBkaW0gbmFtZSBpcyBgZ2VuZXJhdGVDb29yZGAuXG4gKiAgICAgICAgICAgICAgICAgSWYgYGdlbmVyYXRlQ29vcmRDb3VudGAgc3BlY2lmaWVkLCB0aGUgZ2VuZXJhdGVkIGRpbSBuYW1lcyB3aWxsIGJlOlxuICogICAgICAgICAgICAgICAgIGBnZW5lcmF0ZUNvb3JkYCArIDAsIGBnZW5lcmF0ZUNvb3JkYCArIDEsIC4uLlxuICogICAgICAgICAgICAgICAgIGNhbiBiZSBJbmZpbml0eSwgaW5kaWNhdGUgdGhhdCB1c2UgYWxsIG9mIHRoZSByZW1haW4gY29sdW1ucy5cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LmRpbUNvdW50XSBJZiBub3Qgc3BlY2lmaWVkLCBndWVzcyBieSB0aGUgZmlyc3QgZGF0YSBpdGVtLlxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQuZW5jb2RlRGVmYXVsdGVyXSBJZiBub3Qgc3BlY2lmaWVkLCBhdXRvIGZpbmQgdGhlIG5leHQgYXZhaWxhYmxlIGRhdGEgZGltLlxuICogQHJldHVybiB7QXJyYXkuPE9iamVjdD59IFt7XG4gKiAgICAgIG5hbWU6IHN0cmluZyBtYW5kYXRvcnksXG4gKiAgICAgIGRpc3BsYXlOYW1lOiBzdHJpbmcsIHRoZSBvcmlnaW4gbmFtZSBpbiBkaW1zRGVmLCBzZWUgc291cmNlIGhlbHBlci5cbiAqICAgICAgICAgICAgICAgICBJZiBkaXNwbGF5TmFtZSBnaXZlbiwgdGhlIHRvb2x0aXAgd2lsbCBkaXNwbGF5ZWQgdmVydGljYWxseS5cbiAqICAgICAgY29vcmREaW06IHN0cmluZyBtYW5kYXRvcnksXG4gKiAgICAgIGNvb3JkRGltSW5kZXg6IG51bWJlciBtYW5kYXRvcnksXG4gKiAgICAgIHR5cGU6IHN0cmluZyBvcHRpb25hbCxcbiAqICAgICAgb3RoZXJEaW1zOiB7IG5ldmVyIG51bGwvdW5kZWZpbmVkXG4gKiAgICAgICAgICB0b29sdGlwOiBudW1iZXIgb3B0aW9uYWwsXG4gKiAgICAgICAgICBsYWJlbDogbnVtYmVyIG9wdGlvbmFsLFxuICogICAgICAgICAgaXRlbU5hbWU6IG51bWJlciBvcHRpb25hbCxcbiAqICAgICAgICAgIHNlcmllc05hbWU6IG51bWJlciBvcHRpb25hbCxcbiAqICAgICAgfSxcbiAqICAgICAgaXNFeHRyYUNvb3JkOiBib29sZWFuIHRydWUgaWYgY29vcmQgaXMgZ2VuZXJhdGVkXG4gKiAgICAgICAgICAobm90IHNwZWNpZmllZCBpbiBlbmNvZGUgYW5kIG5vdCBzZXJpZXMgc3BlY2lmaWVkKVxuICogICAgICBvdGhlciBwcm9wcyAuLi5cbiAqIH1dXG4gKi9cbmZ1bmN0aW9uIGNvbXBsZXRlRGltZW5zaW9ucyhzeXNEaW1zLCBzb3VyY2UsIG9wdCkge1xuICBpZiAoIVNvdXJjZS5pc0luc3RhbmNlKHNvdXJjZSkpIHtcbiAgICBzb3VyY2UgPSBTb3VyY2Uuc2VyaWVzRGF0YVRvU291cmNlKHNvdXJjZSk7XG4gIH1cblxuICBvcHQgPSBvcHQgfHwge307XG4gIHN5c0RpbXMgPSAoc3lzRGltcyB8fCBbXSkuc2xpY2UoKTtcbiAgdmFyIGRpbXNEZWYgPSAob3B0LmRpbXNEZWYgfHwgW10pLnNsaWNlKCk7XG4gIHZhciBlbmNvZGVEZWYgPSBjcmVhdGVIYXNoTWFwKG9wdC5lbmNvZGVEZWYpO1xuICB2YXIgZGF0YURpbU5hbWVNYXAgPSBjcmVhdGVIYXNoTWFwKCk7XG4gIHZhciBjb29yZERpbU5hbWVNYXAgPSBjcmVhdGVIYXNoTWFwKCk7IC8vIHZhciB2YWx1ZUNhbmRpZGF0ZTtcblxuICB2YXIgcmVzdWx0ID0gW107XG4gIHZhciBkaW1Db3VudCA9IGdldERpbUNvdW50KHNvdXJjZSwgc3lzRGltcywgZGltc0RlZiwgb3B0LmRpbUNvdW50KTsgLy8gQXBwbHkgdXNlciBkZWZpbmVkIGRpbXMgKGBuYW1lYCBhbmQgYHR5cGVgKSBhbmQgaW5pdCByZXN1bHQuXG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBkaW1Db3VudDsgaSsrKSB7XG4gICAgdmFyIGRpbURlZkl0ZW0gPSBkaW1zRGVmW2ldID0gZXh0ZW5kKHt9LCBpc09iamVjdChkaW1zRGVmW2ldKSA/IGRpbXNEZWZbaV0gOiB7XG4gICAgICBuYW1lOiBkaW1zRGVmW2ldXG4gICAgfSk7XG4gICAgdmFyIHVzZXJEaW1OYW1lID0gZGltRGVmSXRlbS5uYW1lO1xuICAgIHZhciByZXN1bHRJdGVtID0gcmVzdWx0W2ldID0ge1xuICAgICAgb3RoZXJEaW1zOiB7fVxuICAgIH07IC8vIE5hbWUgd2lsbCBiZSBhcHBsaWVkIGxhdGVyIGZvciBhdm9pZGluZyBkdXBsaWNhdGlvbi5cblxuICAgIGlmICh1c2VyRGltTmFtZSAhPSBudWxsICYmIGRhdGFEaW1OYW1lTWFwLmdldCh1c2VyRGltTmFtZSkgPT0gbnVsbCkge1xuICAgICAgLy8gT25seSBpZiBgc2VyaWVzLmRpbWVuc2lvbnNgIGlzIGRlZmluZWQgaW4gb3B0aW9uXG4gICAgICAvLyBkaXNwbGF5TmFtZSwgd2lsbCBiZSBzZXQsIGFuZCBkaW1lbnNpb24gd2lsbCBiZSBkaXBsYXllZCB2ZXJ0aWNhbGx5IGluXG4gICAgICAvLyB0b29sdGlwIGJ5IGRlZmF1bHQuXG4gICAgICByZXN1bHRJdGVtLm5hbWUgPSByZXN1bHRJdGVtLmRpc3BsYXlOYW1lID0gdXNlckRpbU5hbWU7XG4gICAgICBkYXRhRGltTmFtZU1hcC5zZXQodXNlckRpbU5hbWUsIGkpO1xuICAgIH1cblxuICAgIGRpbURlZkl0ZW0udHlwZSAhPSBudWxsICYmIChyZXN1bHRJdGVtLnR5cGUgPSBkaW1EZWZJdGVtLnR5cGUpO1xuICAgIGRpbURlZkl0ZW0uZGlzcGxheU5hbWUgIT0gbnVsbCAmJiAocmVzdWx0SXRlbS5kaXNwbGF5TmFtZSA9IGRpbURlZkl0ZW0uZGlzcGxheU5hbWUpO1xuICB9IC8vIFNldCBgY29vcmREaW1gIGFuZCBgY29vcmREaW1JbmRleGAgYnkgYGVuY29kZURlZmAgYW5kIG5vcm1hbGl6ZSBgZW5jb2RlRGVmYC5cblxuXG4gIGVuY29kZURlZi5lYWNoKGZ1bmN0aW9uIChkYXRhRGltcywgY29vcmREaW0pIHtcbiAgICBkYXRhRGltcyA9IG5vcm1hbGl6ZVRvQXJyYXkoZGF0YURpbXMpLnNsaWNlKCk7IC8vIE5vdGU6IEl0IGlzIGFsbG93ZWQgdGhhdCBgZGF0YURpbXMubGVuZ3RoYCBpcyBgMGAsIGUuZy4sIG9wdGlvbnMgaXNcbiAgICAvLyBge2VuY29kZToge3g6IC0xLCB5OiAxfX1gLiBTaG91bGQgbm90IGZpbHRlciBhbnl0aGluZyBpblxuICAgIC8vIHRoaXMgY2FzZS5cblxuICAgIGlmIChkYXRhRGltcy5sZW5ndGggPT09IDEgJiYgZGF0YURpbXNbMF0gPCAwKSB7XG4gICAgICBlbmNvZGVEZWYuc2V0KGNvb3JkRGltLCBmYWxzZSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIHZhbGlkRGF0YURpbXMgPSBlbmNvZGVEZWYuc2V0KGNvb3JkRGltLCBbXSk7XG4gICAgZWFjaChkYXRhRGltcywgZnVuY3Rpb24gKHJlc3VsdERpbUlkeCwgaWR4KSB7XG4gICAgICAvLyBUaGUgaW5wdXQgcmVzdWx0RGltSWR4IGNhbiBiZSBkaW0gbmFtZSBvciBpbmRleC5cbiAgICAgIGlzU3RyaW5nKHJlc3VsdERpbUlkeCkgJiYgKHJlc3VsdERpbUlkeCA9IGRhdGFEaW1OYW1lTWFwLmdldChyZXN1bHREaW1JZHgpKTtcblxuICAgICAgaWYgKHJlc3VsdERpbUlkeCAhPSBudWxsICYmIHJlc3VsdERpbUlkeCA8IGRpbUNvdW50KSB7XG4gICAgICAgIHZhbGlkRGF0YURpbXNbaWR4XSA9IHJlc3VsdERpbUlkeDtcbiAgICAgICAgYXBwbHlEaW0ocmVzdWx0W3Jlc3VsdERpbUlkeF0sIGNvb3JkRGltLCBpZHgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTsgLy8gQXBwbHkgdGVtcGxldGVzIGFuZCBkZWZhdWx0IG9yZGVyIGZyb20gYHN5c0RpbXNgLlxuXG4gIHZhciBhdmFpbERpbUlkeCA9IDA7XG4gIGVhY2goc3lzRGltcywgZnVuY3Rpb24gKHN5c0RpbUl0ZW0sIHN5c0RpbUluZGV4KSB7XG4gICAgdmFyIGNvb3JkRGltO1xuICAgIHZhciBzeXNEaW1JdGVtO1xuICAgIHZhciBzeXNEaW1JdGVtRGltc0RlZjtcbiAgICB2YXIgc3lzRGltSXRlbU90aGVyRGltcztcblxuICAgIGlmIChpc1N0cmluZyhzeXNEaW1JdGVtKSkge1xuICAgICAgY29vcmREaW0gPSBzeXNEaW1JdGVtO1xuICAgICAgc3lzRGltSXRlbSA9IHt9O1xuICAgIH0gZWxzZSB7XG4gICAgICBjb29yZERpbSA9IHN5c0RpbUl0ZW0ubmFtZTtcbiAgICAgIHZhciBvcmRpbmFsTWV0YSA9IHN5c0RpbUl0ZW0ub3JkaW5hbE1ldGE7XG4gICAgICBzeXNEaW1JdGVtLm9yZGluYWxNZXRhID0gbnVsbDtcbiAgICAgIHN5c0RpbUl0ZW0gPSBjbG9uZShzeXNEaW1JdGVtKTtcbiAgICAgIHN5c0RpbUl0ZW0ub3JkaW5hbE1ldGEgPSBvcmRpbmFsTWV0YTsgLy8gYGNvb3JkRGltSW5kZXhgIHNob3VsZCBub3QgYmUgc2V0IGRpcmVjdGx5LlxuXG4gICAgICBzeXNEaW1JdGVtRGltc0RlZiA9IHN5c0RpbUl0ZW0uZGltc0RlZjtcbiAgICAgIHN5c0RpbUl0ZW1PdGhlckRpbXMgPSBzeXNEaW1JdGVtLm90aGVyRGltcztcbiAgICAgIHN5c0RpbUl0ZW0ubmFtZSA9IHN5c0RpbUl0ZW0uY29vcmREaW0gPSBzeXNEaW1JdGVtLmNvb3JkRGltSW5kZXggPSBzeXNEaW1JdGVtLmRpbXNEZWYgPSBzeXNEaW1JdGVtLm90aGVyRGltcyA9IG51bGw7XG4gICAgfVxuXG4gICAgdmFyIGRhdGFEaW1zID0gZW5jb2RlRGVmLmdldChjb29yZERpbSk7IC8vIG5lZ2F0aXZlIHJlc3VsdERpbUlkeCBtZWFucyBubyBuZWVkIHRvIG1hcHBpbmcuXG5cbiAgICBpZiAoZGF0YURpbXMgPT09IGZhbHNlKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGRhdGFEaW1zID0gbm9ybWFsaXplVG9BcnJheShkYXRhRGltcyk7IC8vIGRpbWVuc2lvbnMgcHJvdmlkZXMgZGVmYXVsdCBkaW0gc2VxdWVuY2VzLlxuXG4gICAgaWYgKCFkYXRhRGltcy5sZW5ndGgpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgKHN5c0RpbUl0ZW1EaW1zRGVmICYmIHN5c0RpbUl0ZW1EaW1zRGVmLmxlbmd0aCB8fCAxKTsgaSsrKSB7XG4gICAgICAgIHdoaWxlIChhdmFpbERpbUlkeCA8IHJlc3VsdC5sZW5ndGggJiYgcmVzdWx0W2F2YWlsRGltSWR4XS5jb29yZERpbSAhPSBudWxsKSB7XG4gICAgICAgICAgYXZhaWxEaW1JZHgrKztcbiAgICAgICAgfVxuXG4gICAgICAgIGF2YWlsRGltSWR4IDwgcmVzdWx0Lmxlbmd0aCAmJiBkYXRhRGltcy5wdXNoKGF2YWlsRGltSWR4KyspO1xuICAgICAgfVxuICAgIH0gLy8gQXBwbHkgdGVtcGxhdGVzLlxuXG5cbiAgICBlYWNoKGRhdGFEaW1zLCBmdW5jdGlvbiAocmVzdWx0RGltSWR4LCBjb29yZERpbUluZGV4KSB7XG4gICAgICB2YXIgcmVzdWx0SXRlbSA9IHJlc3VsdFtyZXN1bHREaW1JZHhdO1xuICAgICAgYXBwbHlEaW0oZGVmYXVsdHMocmVzdWx0SXRlbSwgc3lzRGltSXRlbSksIGNvb3JkRGltLCBjb29yZERpbUluZGV4KTtcblxuICAgICAgaWYgKHJlc3VsdEl0ZW0ubmFtZSA9PSBudWxsICYmIHN5c0RpbUl0ZW1EaW1zRGVmKSB7XG4gICAgICAgIHZhciBzeXNEaW1JdGVtRGltc0RlZkl0ZW0gPSBzeXNEaW1JdGVtRGltc0RlZltjb29yZERpbUluZGV4XTtcbiAgICAgICAgIWlzT2JqZWN0KHN5c0RpbUl0ZW1EaW1zRGVmSXRlbSkgJiYgKHN5c0RpbUl0ZW1EaW1zRGVmSXRlbSA9IHtcbiAgICAgICAgICBuYW1lOiBzeXNEaW1JdGVtRGltc0RlZkl0ZW1cbiAgICAgICAgfSk7XG4gICAgICAgIHJlc3VsdEl0ZW0ubmFtZSA9IHJlc3VsdEl0ZW0uZGlzcGxheU5hbWUgPSBzeXNEaW1JdGVtRGltc0RlZkl0ZW0ubmFtZTtcbiAgICAgICAgcmVzdWx0SXRlbS5kZWZhdWx0VG9vbHRpcCA9IHN5c0RpbUl0ZW1EaW1zRGVmSXRlbS5kZWZhdWx0VG9vbHRpcDtcbiAgICAgIH0gLy8gRklYTUUgcmVmYWN0b3IsIGN1cnJlbnRseSBvbmx5IHVzZWQgaW4gY2FzZToge290aGVyRGltczoge3Rvb2x0aXA6IGZhbHNlfX1cblxuXG4gICAgICBzeXNEaW1JdGVtT3RoZXJEaW1zICYmIGRlZmF1bHRzKHJlc3VsdEl0ZW0ub3RoZXJEaW1zLCBzeXNEaW1JdGVtT3RoZXJEaW1zKTtcbiAgICB9KTtcbiAgfSk7XG5cbiAgZnVuY3Rpb24gYXBwbHlEaW0ocmVzdWx0SXRlbSwgY29vcmREaW0sIGNvb3JkRGltSW5kZXgpIHtcbiAgICBpZiAoT1RIRVJfRElNRU5TSU9OUy5nZXQoY29vcmREaW0pICE9IG51bGwpIHtcbiAgICAgIHJlc3VsdEl0ZW0ub3RoZXJEaW1zW2Nvb3JkRGltXSA9IGNvb3JkRGltSW5kZXg7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc3VsdEl0ZW0uY29vcmREaW0gPSBjb29yZERpbTtcbiAgICAgIHJlc3VsdEl0ZW0uY29vcmREaW1JbmRleCA9IGNvb3JkRGltSW5kZXg7XG4gICAgICBjb29yZERpbU5hbWVNYXAuc2V0KGNvb3JkRGltLCB0cnVlKTtcbiAgICB9XG4gIH0gLy8gTWFrZSBzdXJlIHRoZSBmaXJzdCBleHRyYSBkaW0gaXMgJ3ZhbHVlJy5cblxuXG4gIHZhciBnZW5lcmF0ZUNvb3JkID0gb3B0LmdlbmVyYXRlQ29vcmQ7XG4gIHZhciBnZW5lcmF0ZUNvb3JkQ291bnQgPSBvcHQuZ2VuZXJhdGVDb29yZENvdW50O1xuICB2YXIgZnJvbVplcm8gPSBnZW5lcmF0ZUNvb3JkQ291bnQgIT0gbnVsbDtcbiAgZ2VuZXJhdGVDb29yZENvdW50ID0gZ2VuZXJhdGVDb29yZCA/IGdlbmVyYXRlQ29vcmRDb3VudCB8fCAxIDogMDtcbiAgdmFyIGV4dHJhID0gZ2VuZXJhdGVDb29yZCB8fCAndmFsdWUnOyAvLyBTZXQgZGltIGBuYW1lYCBhbmQgb3RoZXIgYGNvb3JkRGltYCBhbmQgb3RoZXIgcHJvcHMuXG5cbiAgZm9yICh2YXIgcmVzdWx0RGltSWR4ID0gMDsgcmVzdWx0RGltSWR4IDwgZGltQ291bnQ7IHJlc3VsdERpbUlkeCsrKSB7XG4gICAgdmFyIHJlc3VsdEl0ZW0gPSByZXN1bHRbcmVzdWx0RGltSWR4XSA9IHJlc3VsdFtyZXN1bHREaW1JZHhdIHx8IHt9O1xuICAgIHZhciBjb29yZERpbSA9IHJlc3VsdEl0ZW0uY29vcmREaW07XG5cbiAgICBpZiAoY29vcmREaW0gPT0gbnVsbCkge1xuICAgICAgcmVzdWx0SXRlbS5jb29yZERpbSA9IGdlbk5hbWUoZXh0cmEsIGNvb3JkRGltTmFtZU1hcCwgZnJvbVplcm8pO1xuICAgICAgcmVzdWx0SXRlbS5jb29yZERpbUluZGV4ID0gMDtcblxuICAgICAgaWYgKCFnZW5lcmF0ZUNvb3JkIHx8IGdlbmVyYXRlQ29vcmRDb3VudCA8PSAwKSB7XG4gICAgICAgIHJlc3VsdEl0ZW0uaXNFeHRyYUNvb3JkID0gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgZ2VuZXJhdGVDb29yZENvdW50LS07XG4gICAgfVxuXG4gICAgcmVzdWx0SXRlbS5uYW1lID09IG51bGwgJiYgKHJlc3VsdEl0ZW0ubmFtZSA9IGdlbk5hbWUocmVzdWx0SXRlbS5jb29yZERpbSwgZGF0YURpbU5hbWVNYXApKTtcblxuICAgIGlmIChyZXN1bHRJdGVtLnR5cGUgPT0gbnVsbCAmJiBndWVzc09yZGluYWwoc291cmNlLCByZXN1bHREaW1JZHgsIHJlc3VsdEl0ZW0ubmFtZSkpIHtcbiAgICAgIHJlc3VsdEl0ZW0udHlwZSA9ICdvcmRpbmFsJztcbiAgICB9XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufSAvLyA/Pz8gVE9ET1xuLy8gT3JpZ2luYWxseSBkZXRlY3QgZGltQ291bnQgYnkgZGF0YVswXS4gU2hvdWxkIHdlXG4vLyBvcHRpbWl6ZSBpdCB0byBvbmx5IGJ5IHN5c0RpbXMgYW5kIGRpbWVuc2lvbnMgYW5kIGVuY29kZS5cbi8vIFNvIG9ubHkgbmVjZXNzYXJ5IGRpbXMgd2lsbCBiZSBpbml0aWFsaXplZC5cbi8vIEJ1dFxuLy8gKDEpIGN1c3RvbSBzZXJpZXMgc2hvdWxkIGJlIGNvbnNpZGVyZWQuIHdoZXJlIG90aGVyIGRpbXNcbi8vIG1heSBiZSB2aXNpdGVkLlxuLy8gKDIpIHNvbWV0aW1lcyB1c2VyIG5lZWQgdG8gY2FsY3VhbHRlIGJ1YmJsZSBzaXplIG9yIHVzZSB2aXN1YWxNYXBcbi8vIG9uIG90aGVyIGRpbWVuc2lvbnMgYmVzaWRlcyBjb29yZFN5cyBuZWVkZWQuXG4vLyBTbywgZGltcyB0aGF0IGlzIG5vdCB1c2VkIGJ5IHN5c3RlbSwgc2hvdWxkIGJlIHNoYXJlZCBpbiBzdG9yYWdlP1xuXG5cbmZ1bmN0aW9uIGdldERpbUNvdW50KHNvdXJjZSwgc3lzRGltcywgZGltc0RlZiwgb3B0RGltQ291bnQpIHtcbiAgLy8gTm90ZSB0aGF0IHRoZSByZXN1bHQgZGltQ291bnQgc2hvdWxkIG5vdCBzbWFsbCB0aGFuIGNvbHVtbnMgY291bnRcbiAgLy8gb2YgZGF0YSwgb3RoZXJ3aXNlIGBkYXRhRGltTmFtZU1hcGAgY2hlY2tpbmcgd2lsbCBiZSBpbmNvcnJlY3QuXG4gIHZhciBkaW1Db3VudCA9IE1hdGgubWF4KHNvdXJjZS5kaW1lbnNpb25zRGV0ZWN0Q291bnQgfHwgMSwgc3lzRGltcy5sZW5ndGgsIGRpbXNEZWYubGVuZ3RoLCBvcHREaW1Db3VudCB8fCAwKTtcbiAgZWFjaChzeXNEaW1zLCBmdW5jdGlvbiAoc3lzRGltSXRlbSkge1xuICAgIHZhciBzeXNEaW1JdGVtRGltc0RlZiA9IHN5c0RpbUl0ZW0uZGltc0RlZjtcbiAgICBzeXNEaW1JdGVtRGltc0RlZiAmJiAoZGltQ291bnQgPSBNYXRoLm1heChkaW1Db3VudCwgc3lzRGltSXRlbURpbXNEZWYubGVuZ3RoKSk7XG4gIH0pO1xuICByZXR1cm4gZGltQ291bnQ7XG59XG5cbmZ1bmN0aW9uIGdlbk5hbWUobmFtZSwgbWFwLCBmcm9tWmVybykge1xuICBpZiAoZnJvbVplcm8gfHwgbWFwLmdldChuYW1lKSAhPSBudWxsKSB7XG4gICAgdmFyIGkgPSAwO1xuXG4gICAgd2hpbGUgKG1hcC5nZXQobmFtZSArIGkpICE9IG51bGwpIHtcbiAgICAgIGkrKztcbiAgICB9XG5cbiAgICBuYW1lICs9IGk7XG4gIH1cblxuICBtYXAuc2V0KG5hbWUsIHRydWUpO1xuICByZXR1cm4gbmFtZTtcbn1cblxudmFyIF9kZWZhdWx0ID0gY29tcGxldGVEaW1lbnNpb25zO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7OztBQUtBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/data/helper/completeDimensions.js
