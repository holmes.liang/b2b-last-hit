/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var echarts = __webpack_require__(/*! ../../echarts */ "./node_modules/echarts/lib/echarts.js");

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var _helper = __webpack_require__(/*! ./helper */ "./node_modules/echarts/lib/chart/bar/helper.js");

var setLabel = _helper.setLabel;

var Model = __webpack_require__(/*! ../../model/Model */ "./node_modules/echarts/lib/model/Model.js");

var barItemStyle = __webpack_require__(/*! ./barItemStyle */ "./node_modules/echarts/lib/chart/bar/barItemStyle.js");

var Path = __webpack_require__(/*! zrender/lib/graphic/Path */ "./node_modules/zrender/lib/graphic/Path.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


var BAR_BORDER_WIDTH_QUERY = ['itemStyle', 'barBorderWidth']; // FIXME
// Just for compatible with ec2.

zrUtil.extend(Model.prototype, barItemStyle);

var _default = echarts.extendChartView({
  type: 'bar',
  render: function render(seriesModel, ecModel, api) {
    this._updateDrawMode(seriesModel);

    var coordinateSystemType = seriesModel.get('coordinateSystem');

    if (coordinateSystemType === 'cartesian2d' || coordinateSystemType === 'polar') {
      this._isLargeDraw ? this._renderLarge(seriesModel, ecModel, api) : this._renderNormal(seriesModel, ecModel, api);
    } else {}

    return this.group;
  },
  incrementalPrepareRender: function incrementalPrepareRender(seriesModel, ecModel, api) {
    this._clear();

    this._updateDrawMode(seriesModel);
  },
  incrementalRender: function incrementalRender(params, seriesModel, ecModel, api) {
    // Do not support progressive in normal mode.
    this._incrementalRenderLarge(params, seriesModel);
  },
  _updateDrawMode: function _updateDrawMode(seriesModel) {
    var isLargeDraw = seriesModel.pipelineContext.large;

    if (this._isLargeDraw == null || isLargeDraw ^ this._isLargeDraw) {
      this._isLargeDraw = isLargeDraw;

      this._clear();
    }
  },
  _renderNormal: function _renderNormal(seriesModel, ecModel, api) {
    var group = this.group;
    var data = seriesModel.getData();
    var oldData = this._data;
    var coord = seriesModel.coordinateSystem;
    var baseAxis = coord.getBaseAxis();
    var isHorizontalOrRadial;

    if (coord.type === 'cartesian2d') {
      isHorizontalOrRadial = baseAxis.isHorizontal();
    } else if (coord.type === 'polar') {
      isHorizontalOrRadial = baseAxis.dim === 'angle';
    }

    var animationModel = seriesModel.isAnimationEnabled() ? seriesModel : null;
    data.diff(oldData).add(function (dataIndex) {
      if (!data.hasValue(dataIndex)) {
        return;
      }

      var itemModel = data.getItemModel(dataIndex);
      var layout = getLayout[coord.type](data, dataIndex, itemModel);
      var el = elementCreator[coord.type](data, dataIndex, itemModel, layout, isHorizontalOrRadial, animationModel);
      data.setItemGraphicEl(dataIndex, el);
      group.add(el);
      updateStyle(el, data, dataIndex, itemModel, layout, seriesModel, isHorizontalOrRadial, coord.type === 'polar');
    }).update(function (newIndex, oldIndex) {
      var el = oldData.getItemGraphicEl(oldIndex);

      if (!data.hasValue(newIndex)) {
        group.remove(el);
        return;
      }

      var itemModel = data.getItemModel(newIndex);
      var layout = getLayout[coord.type](data, newIndex, itemModel);

      if (el) {
        graphic.updateProps(el, {
          shape: layout
        }, animationModel, newIndex);
      } else {
        el = elementCreator[coord.type](data, newIndex, itemModel, layout, isHorizontalOrRadial, animationModel, true);
      }

      data.setItemGraphicEl(newIndex, el); // Add back

      group.add(el);
      updateStyle(el, data, newIndex, itemModel, layout, seriesModel, isHorizontalOrRadial, coord.type === 'polar');
    }).remove(function (dataIndex) {
      var el = oldData.getItemGraphicEl(dataIndex);

      if (coord.type === 'cartesian2d') {
        el && removeRect(dataIndex, animationModel, el);
      } else {
        el && removeSector(dataIndex, animationModel, el);
      }
    }).execute();
    this._data = data;
  },
  _renderLarge: function _renderLarge(seriesModel, ecModel, api) {
    this._clear();

    createLarge(seriesModel, this.group);
  },
  _incrementalRenderLarge: function _incrementalRenderLarge(params, seriesModel) {
    createLarge(seriesModel, this.group, true);
  },
  dispose: zrUtil.noop,
  remove: function remove(ecModel) {
    this._clear(ecModel);
  },
  _clear: function _clear(ecModel) {
    var group = this.group;
    var data = this._data;

    if (ecModel && ecModel.get('animation') && data && !this._isLargeDraw) {
      data.eachItemGraphicEl(function (el) {
        if (el.type === 'sector') {
          removeSector(el.dataIndex, ecModel, el);
        } else {
          removeRect(el.dataIndex, ecModel, el);
        }
      });
    } else {
      group.removeAll();
    }

    this._data = null;
  }
});

var elementCreator = {
  cartesian2d: function cartesian2d(data, dataIndex, itemModel, layout, isHorizontal, animationModel, isUpdate) {
    var rect = new graphic.Rect({
      shape: zrUtil.extend({}, layout)
    }); // Animation

    if (animationModel) {
      var rectShape = rect.shape;
      var animateProperty = isHorizontal ? 'height' : 'width';
      var animateTarget = {};
      rectShape[animateProperty] = 0;
      animateTarget[animateProperty] = layout[animateProperty];
      graphic[isUpdate ? 'updateProps' : 'initProps'](rect, {
        shape: animateTarget
      }, animationModel, dataIndex);
    }

    return rect;
  },
  polar: function polar(data, dataIndex, itemModel, layout, isRadial, animationModel, isUpdate) {
    // Keep the same logic with bar in catesion: use end value to control
    // direction. Notice that if clockwise is true (by default), the sector
    // will always draw clockwisely, no matter whether endAngle is greater
    // or less than startAngle.
    var clockwise = layout.startAngle < layout.endAngle;
    var sector = new graphic.Sector({
      shape: zrUtil.defaults({
        clockwise: clockwise
      }, layout)
    }); // Animation

    if (animationModel) {
      var sectorShape = sector.shape;
      var animateProperty = isRadial ? 'r' : 'endAngle';
      var animateTarget = {};
      sectorShape[animateProperty] = isRadial ? 0 : layout.startAngle;
      animateTarget[animateProperty] = layout[animateProperty];
      graphic[isUpdate ? 'updateProps' : 'initProps'](sector, {
        shape: animateTarget
      }, animationModel, dataIndex);
    }

    return sector;
  }
};

function removeRect(dataIndex, animationModel, el) {
  // Not show text when animating
  el.style.text = null;
  graphic.updateProps(el, {
    shape: {
      width: 0
    }
  }, animationModel, dataIndex, function () {
    el.parent && el.parent.remove(el);
  });
}

function removeSector(dataIndex, animationModel, el) {
  // Not show text when animating
  el.style.text = null;
  graphic.updateProps(el, {
    shape: {
      r: el.shape.r0
    }
  }, animationModel, dataIndex, function () {
    el.parent && el.parent.remove(el);
  });
}

var getLayout = {
  cartesian2d: function cartesian2d(data, dataIndex, itemModel) {
    var layout = data.getItemLayout(dataIndex);
    var fixedLineWidth = getLineWidth(itemModel, layout); // fix layout with lineWidth

    var signX = layout.width > 0 ? 1 : -1;
    var signY = layout.height > 0 ? 1 : -1;
    return {
      x: layout.x + signX * fixedLineWidth / 2,
      y: layout.y + signY * fixedLineWidth / 2,
      width: layout.width - signX * fixedLineWidth,
      height: layout.height - signY * fixedLineWidth
    };
  },
  polar: function polar(data, dataIndex, itemModel) {
    var layout = data.getItemLayout(dataIndex);
    return {
      cx: layout.cx,
      cy: layout.cy,
      r0: layout.r0,
      r: layout.r,
      startAngle: layout.startAngle,
      endAngle: layout.endAngle
    };
  }
};

function updateStyle(el, data, dataIndex, itemModel, layout, seriesModel, isHorizontal, isPolar) {
  var color = data.getItemVisual(dataIndex, 'color');
  var opacity = data.getItemVisual(dataIndex, 'opacity');
  var itemStyleModel = itemModel.getModel('itemStyle');
  var hoverStyle = itemModel.getModel('emphasis.itemStyle').getBarItemStyle();

  if (!isPolar) {
    el.setShape('r', itemStyleModel.get('barBorderRadius') || 0);
  }

  el.useStyle(zrUtil.defaults({
    fill: color,
    opacity: opacity
  }, itemStyleModel.getBarItemStyle()));
  var cursorStyle = itemModel.getShallow('cursor');
  cursorStyle && el.attr('cursor', cursorStyle);
  var labelPositionOutside = isHorizontal ? layout.height > 0 ? 'bottom' : 'top' : layout.width > 0 ? 'left' : 'right';

  if (!isPolar) {
    setLabel(el.style, hoverStyle, itemModel, color, seriesModel, dataIndex, labelPositionOutside);
  }

  graphic.setHoverStyle(el, hoverStyle);
} // In case width or height are too small.


function getLineWidth(itemModel, rawLayout) {
  var lineWidth = itemModel.get(BAR_BORDER_WIDTH_QUERY) || 0;
  return Math.min(lineWidth, Math.abs(rawLayout.width), Math.abs(rawLayout.height));
}

var LargePath = Path.extend({
  type: 'largeBar',
  shape: {
    points: []
  },
  buildPath: function buildPath(ctx, shape) {
    // Drawing lines is more efficient than drawing
    // a whole line or drawing rects.
    var points = shape.points;
    var startPoint = this.__startPoint;
    var valueIdx = this.__valueIdx;

    for (var i = 0; i < points.length; i += 2) {
      startPoint[this.__valueIdx] = points[i + valueIdx];
      ctx.moveTo(startPoint[0], startPoint[1]);
      ctx.lineTo(points[i], points[i + 1]);
    }
  }
});

function createLarge(seriesModel, group, incremental) {
  // TODO support polar
  var data = seriesModel.getData();
  var startPoint = [];
  var valueIdx = data.getLayout('valueAxisHorizontal') ? 1 : 0;
  startPoint[1 - valueIdx] = data.getLayout('valueAxisStart');
  var el = new LargePath({
    shape: {
      points: data.getLayout('largePoints')
    },
    incremental: !!incremental,
    __startPoint: startPoint,
    __valueIdx: valueIdx
  });
  group.add(el);
  setLargeStyle(el, seriesModel, data);
}

function setLargeStyle(el, seriesModel, data) {
  var borderColor = data.getVisual('borderColor') || data.getVisual('color');
  var itemStyle = seriesModel.getModel('itemStyle').getItemStyle(['color', 'borderColor']);
  el.useStyle(itemStyle);
  el.style.fill = null;
  el.style.stroke = borderColor;
  el.style.lineWidth = data.getLayout('barWidth');
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvYmFyL0JhclZpZXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9iYXIvQmFyVmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi4vLi4vY29uZmlnXCIpO1xuXG52YXIgX19ERVZfXyA9IF9jb25maWcuX19ERVZfXztcblxudmFyIGVjaGFydHMgPSByZXF1aXJlKFwiLi4vLi4vZWNoYXJ0c1wiKTtcblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIF9oZWxwZXIgPSByZXF1aXJlKFwiLi9oZWxwZXJcIik7XG5cbnZhciBzZXRMYWJlbCA9IF9oZWxwZXIuc2V0TGFiZWw7XG5cbnZhciBNb2RlbCA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9Nb2RlbFwiKTtcblxudmFyIGJhckl0ZW1TdHlsZSA9IHJlcXVpcmUoXCIuL2Jhckl0ZW1TdHlsZVwiKTtcblxudmFyIFBhdGggPSByZXF1aXJlKFwienJlbmRlci9saWIvZ3JhcGhpYy9QYXRoXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgQkFSX0JPUkRFUl9XSURUSF9RVUVSWSA9IFsnaXRlbVN0eWxlJywgJ2JhckJvcmRlcldpZHRoJ107IC8vIEZJWE1FXG4vLyBKdXN0IGZvciBjb21wYXRpYmxlIHdpdGggZWMyLlxuXG56clV0aWwuZXh0ZW5kKE1vZGVsLnByb3RvdHlwZSwgYmFySXRlbVN0eWxlKTtcblxudmFyIF9kZWZhdWx0ID0gZWNoYXJ0cy5leHRlbmRDaGFydFZpZXcoe1xuICB0eXBlOiAnYmFyJyxcbiAgcmVuZGVyOiBmdW5jdGlvbiAoc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSkge1xuICAgIHRoaXMuX3VwZGF0ZURyYXdNb2RlKHNlcmllc01vZGVsKTtcblxuICAgIHZhciBjb29yZGluYXRlU3lzdGVtVHlwZSA9IHNlcmllc01vZGVsLmdldCgnY29vcmRpbmF0ZVN5c3RlbScpO1xuXG4gICAgaWYgKGNvb3JkaW5hdGVTeXN0ZW1UeXBlID09PSAnY2FydGVzaWFuMmQnIHx8IGNvb3JkaW5hdGVTeXN0ZW1UeXBlID09PSAncG9sYXInKSB7XG4gICAgICB0aGlzLl9pc0xhcmdlRHJhdyA/IHRoaXMuX3JlbmRlckxhcmdlKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGkpIDogdGhpcy5fcmVuZGVyTm9ybWFsKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGkpO1xuICAgIH0gZWxzZSB7fVxuXG4gICAgcmV0dXJuIHRoaXMuZ3JvdXA7XG4gIH0sXG4gIGluY3JlbWVudGFsUHJlcGFyZVJlbmRlcjogZnVuY3Rpb24gKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICB0aGlzLl9jbGVhcigpO1xuXG4gICAgdGhpcy5fdXBkYXRlRHJhd01vZGUoc2VyaWVzTW9kZWwpO1xuICB9LFxuICBpbmNyZW1lbnRhbFJlbmRlcjogZnVuY3Rpb24gKHBhcmFtcywgc2VyaWVzTW9kZWwsIGVjTW9kZWwsIGFwaSkge1xuICAgIC8vIERvIG5vdCBzdXBwb3J0IHByb2dyZXNzaXZlIGluIG5vcm1hbCBtb2RlLlxuICAgIHRoaXMuX2luY3JlbWVudGFsUmVuZGVyTGFyZ2UocGFyYW1zLCBzZXJpZXNNb2RlbCk7XG4gIH0sXG4gIF91cGRhdGVEcmF3TW9kZTogZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgdmFyIGlzTGFyZ2VEcmF3ID0gc2VyaWVzTW9kZWwucGlwZWxpbmVDb250ZXh0LmxhcmdlO1xuXG4gICAgaWYgKHRoaXMuX2lzTGFyZ2VEcmF3ID09IG51bGwgfHwgaXNMYXJnZURyYXcgXiB0aGlzLl9pc0xhcmdlRHJhdykge1xuICAgICAgdGhpcy5faXNMYXJnZURyYXcgPSBpc0xhcmdlRHJhdztcblxuICAgICAgdGhpcy5fY2xlYXIoKTtcbiAgICB9XG4gIH0sXG4gIF9yZW5kZXJOb3JtYWw6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgZWNNb2RlbCwgYXBpKSB7XG4gICAgdmFyIGdyb3VwID0gdGhpcy5ncm91cDtcbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgb2xkRGF0YSA9IHRoaXMuX2RhdGE7XG4gICAgdmFyIGNvb3JkID0gc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbTtcbiAgICB2YXIgYmFzZUF4aXMgPSBjb29yZC5nZXRCYXNlQXhpcygpO1xuICAgIHZhciBpc0hvcml6b250YWxPclJhZGlhbDtcblxuICAgIGlmIChjb29yZC50eXBlID09PSAnY2FydGVzaWFuMmQnKSB7XG4gICAgICBpc0hvcml6b250YWxPclJhZGlhbCA9IGJhc2VBeGlzLmlzSG9yaXpvbnRhbCgpO1xuICAgIH0gZWxzZSBpZiAoY29vcmQudHlwZSA9PT0gJ3BvbGFyJykge1xuICAgICAgaXNIb3Jpem9udGFsT3JSYWRpYWwgPSBiYXNlQXhpcy5kaW0gPT09ICdhbmdsZSc7XG4gICAgfVxuXG4gICAgdmFyIGFuaW1hdGlvbk1vZGVsID0gc2VyaWVzTW9kZWwuaXNBbmltYXRpb25FbmFibGVkKCkgPyBzZXJpZXNNb2RlbCA6IG51bGw7XG4gICAgZGF0YS5kaWZmKG9sZERhdGEpLmFkZChmdW5jdGlvbiAoZGF0YUluZGV4KSB7XG4gICAgICBpZiAoIWRhdGEuaGFzVmFsdWUoZGF0YUluZGV4KSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBpdGVtTW9kZWwgPSBkYXRhLmdldEl0ZW1Nb2RlbChkYXRhSW5kZXgpO1xuICAgICAgdmFyIGxheW91dCA9IGdldExheW91dFtjb29yZC50eXBlXShkYXRhLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCk7XG4gICAgICB2YXIgZWwgPSBlbGVtZW50Q3JlYXRvcltjb29yZC50eXBlXShkYXRhLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCwgbGF5b3V0LCBpc0hvcml6b250YWxPclJhZGlhbCwgYW5pbWF0aW9uTW9kZWwpO1xuICAgICAgZGF0YS5zZXRJdGVtR3JhcGhpY0VsKGRhdGFJbmRleCwgZWwpO1xuICAgICAgZ3JvdXAuYWRkKGVsKTtcbiAgICAgIHVwZGF0ZVN0eWxlKGVsLCBkYXRhLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCwgbGF5b3V0LCBzZXJpZXNNb2RlbCwgaXNIb3Jpem9udGFsT3JSYWRpYWwsIGNvb3JkLnR5cGUgPT09ICdwb2xhcicpO1xuICAgIH0pLnVwZGF0ZShmdW5jdGlvbiAobmV3SW5kZXgsIG9sZEluZGV4KSB7XG4gICAgICB2YXIgZWwgPSBvbGREYXRhLmdldEl0ZW1HcmFwaGljRWwob2xkSW5kZXgpO1xuXG4gICAgICBpZiAoIWRhdGEuaGFzVmFsdWUobmV3SW5kZXgpKSB7XG4gICAgICAgIGdyb3VwLnJlbW92ZShlbCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGl0ZW1Nb2RlbCA9IGRhdGEuZ2V0SXRlbU1vZGVsKG5ld0luZGV4KTtcbiAgICAgIHZhciBsYXlvdXQgPSBnZXRMYXlvdXRbY29vcmQudHlwZV0oZGF0YSwgbmV3SW5kZXgsIGl0ZW1Nb2RlbCk7XG5cbiAgICAgIGlmIChlbCkge1xuICAgICAgICBncmFwaGljLnVwZGF0ZVByb3BzKGVsLCB7XG4gICAgICAgICAgc2hhcGU6IGxheW91dFxuICAgICAgICB9LCBhbmltYXRpb25Nb2RlbCwgbmV3SW5kZXgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWwgPSBlbGVtZW50Q3JlYXRvcltjb29yZC50eXBlXShkYXRhLCBuZXdJbmRleCwgaXRlbU1vZGVsLCBsYXlvdXQsIGlzSG9yaXpvbnRhbE9yUmFkaWFsLCBhbmltYXRpb25Nb2RlbCwgdHJ1ZSk7XG4gICAgICB9XG5cbiAgICAgIGRhdGEuc2V0SXRlbUdyYXBoaWNFbChuZXdJbmRleCwgZWwpOyAvLyBBZGQgYmFja1xuXG4gICAgICBncm91cC5hZGQoZWwpO1xuICAgICAgdXBkYXRlU3R5bGUoZWwsIGRhdGEsIG5ld0luZGV4LCBpdGVtTW9kZWwsIGxheW91dCwgc2VyaWVzTW9kZWwsIGlzSG9yaXpvbnRhbE9yUmFkaWFsLCBjb29yZC50eXBlID09PSAncG9sYXInKTtcbiAgICB9KS5yZW1vdmUoZnVuY3Rpb24gKGRhdGFJbmRleCkge1xuICAgICAgdmFyIGVsID0gb2xkRGF0YS5nZXRJdGVtR3JhcGhpY0VsKGRhdGFJbmRleCk7XG5cbiAgICAgIGlmIChjb29yZC50eXBlID09PSAnY2FydGVzaWFuMmQnKSB7XG4gICAgICAgIGVsICYmIHJlbW92ZVJlY3QoZGF0YUluZGV4LCBhbmltYXRpb25Nb2RlbCwgZWwpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZWwgJiYgcmVtb3ZlU2VjdG9yKGRhdGFJbmRleCwgYW5pbWF0aW9uTW9kZWwsIGVsKTtcbiAgICAgIH1cbiAgICB9KS5leGVjdXRlKCk7XG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gIH0sXG4gIF9yZW5kZXJMYXJnZTogZnVuY3Rpb24gKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgICB0aGlzLl9jbGVhcigpO1xuXG4gICAgY3JlYXRlTGFyZ2Uoc2VyaWVzTW9kZWwsIHRoaXMuZ3JvdXApO1xuICB9LFxuICBfaW5jcmVtZW50YWxSZW5kZXJMYXJnZTogZnVuY3Rpb24gKHBhcmFtcywgc2VyaWVzTW9kZWwpIHtcbiAgICBjcmVhdGVMYXJnZShzZXJpZXNNb2RlbCwgdGhpcy5ncm91cCwgdHJ1ZSk7XG4gIH0sXG4gIGRpc3Bvc2U6IHpyVXRpbC5ub29wLFxuICByZW1vdmU6IGZ1bmN0aW9uIChlY01vZGVsKSB7XG4gICAgdGhpcy5fY2xlYXIoZWNNb2RlbCk7XG4gIH0sXG4gIF9jbGVhcjogZnVuY3Rpb24gKGVjTW9kZWwpIHtcbiAgICB2YXIgZ3JvdXAgPSB0aGlzLmdyb3VwO1xuICAgIHZhciBkYXRhID0gdGhpcy5fZGF0YTtcblxuICAgIGlmIChlY01vZGVsICYmIGVjTW9kZWwuZ2V0KCdhbmltYXRpb24nKSAmJiBkYXRhICYmICF0aGlzLl9pc0xhcmdlRHJhdykge1xuICAgICAgZGF0YS5lYWNoSXRlbUdyYXBoaWNFbChmdW5jdGlvbiAoZWwpIHtcbiAgICAgICAgaWYgKGVsLnR5cGUgPT09ICdzZWN0b3InKSB7XG4gICAgICAgICAgcmVtb3ZlU2VjdG9yKGVsLmRhdGFJbmRleCwgZWNNb2RlbCwgZWwpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJlbW92ZVJlY3QoZWwuZGF0YUluZGV4LCBlY01vZGVsLCBlbCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBncm91cC5yZW1vdmVBbGwoKTtcbiAgICB9XG5cbiAgICB0aGlzLl9kYXRhID0gbnVsbDtcbiAgfVxufSk7XG5cbnZhciBlbGVtZW50Q3JlYXRvciA9IHtcbiAgY2FydGVzaWFuMmQ6IGZ1bmN0aW9uIChkYXRhLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCwgbGF5b3V0LCBpc0hvcml6b250YWwsIGFuaW1hdGlvbk1vZGVsLCBpc1VwZGF0ZSkge1xuICAgIHZhciByZWN0ID0gbmV3IGdyYXBoaWMuUmVjdCh7XG4gICAgICBzaGFwZTogenJVdGlsLmV4dGVuZCh7fSwgbGF5b3V0KVxuICAgIH0pOyAvLyBBbmltYXRpb25cblxuICAgIGlmIChhbmltYXRpb25Nb2RlbCkge1xuICAgICAgdmFyIHJlY3RTaGFwZSA9IHJlY3Quc2hhcGU7XG4gICAgICB2YXIgYW5pbWF0ZVByb3BlcnR5ID0gaXNIb3Jpem9udGFsID8gJ2hlaWdodCcgOiAnd2lkdGgnO1xuICAgICAgdmFyIGFuaW1hdGVUYXJnZXQgPSB7fTtcbiAgICAgIHJlY3RTaGFwZVthbmltYXRlUHJvcGVydHldID0gMDtcbiAgICAgIGFuaW1hdGVUYXJnZXRbYW5pbWF0ZVByb3BlcnR5XSA9IGxheW91dFthbmltYXRlUHJvcGVydHldO1xuICAgICAgZ3JhcGhpY1tpc1VwZGF0ZSA/ICd1cGRhdGVQcm9wcycgOiAnaW5pdFByb3BzJ10ocmVjdCwge1xuICAgICAgICBzaGFwZTogYW5pbWF0ZVRhcmdldFxuICAgICAgfSwgYW5pbWF0aW9uTW9kZWwsIGRhdGFJbmRleCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlY3Q7XG4gIH0sXG4gIHBvbGFyOiBmdW5jdGlvbiAoZGF0YSwgZGF0YUluZGV4LCBpdGVtTW9kZWwsIGxheW91dCwgaXNSYWRpYWwsIGFuaW1hdGlvbk1vZGVsLCBpc1VwZGF0ZSkge1xuICAgIC8vIEtlZXAgdGhlIHNhbWUgbG9naWMgd2l0aCBiYXIgaW4gY2F0ZXNpb246IHVzZSBlbmQgdmFsdWUgdG8gY29udHJvbFxuICAgIC8vIGRpcmVjdGlvbi4gTm90aWNlIHRoYXQgaWYgY2xvY2t3aXNlIGlzIHRydWUgKGJ5IGRlZmF1bHQpLCB0aGUgc2VjdG9yXG4gICAgLy8gd2lsbCBhbHdheXMgZHJhdyBjbG9ja3dpc2VseSwgbm8gbWF0dGVyIHdoZXRoZXIgZW5kQW5nbGUgaXMgZ3JlYXRlclxuICAgIC8vIG9yIGxlc3MgdGhhbiBzdGFydEFuZ2xlLlxuICAgIHZhciBjbG9ja3dpc2UgPSBsYXlvdXQuc3RhcnRBbmdsZSA8IGxheW91dC5lbmRBbmdsZTtcbiAgICB2YXIgc2VjdG9yID0gbmV3IGdyYXBoaWMuU2VjdG9yKHtcbiAgICAgIHNoYXBlOiB6clV0aWwuZGVmYXVsdHMoe1xuICAgICAgICBjbG9ja3dpc2U6IGNsb2Nrd2lzZVxuICAgICAgfSwgbGF5b3V0KVxuICAgIH0pOyAvLyBBbmltYXRpb25cblxuICAgIGlmIChhbmltYXRpb25Nb2RlbCkge1xuICAgICAgdmFyIHNlY3RvclNoYXBlID0gc2VjdG9yLnNoYXBlO1xuICAgICAgdmFyIGFuaW1hdGVQcm9wZXJ0eSA9IGlzUmFkaWFsID8gJ3InIDogJ2VuZEFuZ2xlJztcbiAgICAgIHZhciBhbmltYXRlVGFyZ2V0ID0ge307XG4gICAgICBzZWN0b3JTaGFwZVthbmltYXRlUHJvcGVydHldID0gaXNSYWRpYWwgPyAwIDogbGF5b3V0LnN0YXJ0QW5nbGU7XG4gICAgICBhbmltYXRlVGFyZ2V0W2FuaW1hdGVQcm9wZXJ0eV0gPSBsYXlvdXRbYW5pbWF0ZVByb3BlcnR5XTtcbiAgICAgIGdyYXBoaWNbaXNVcGRhdGUgPyAndXBkYXRlUHJvcHMnIDogJ2luaXRQcm9wcyddKHNlY3Rvciwge1xuICAgICAgICBzaGFwZTogYW5pbWF0ZVRhcmdldFxuICAgICAgfSwgYW5pbWF0aW9uTW9kZWwsIGRhdGFJbmRleCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHNlY3RvcjtcbiAgfVxufTtcblxuZnVuY3Rpb24gcmVtb3ZlUmVjdChkYXRhSW5kZXgsIGFuaW1hdGlvbk1vZGVsLCBlbCkge1xuICAvLyBOb3Qgc2hvdyB0ZXh0IHdoZW4gYW5pbWF0aW5nXG4gIGVsLnN0eWxlLnRleHQgPSBudWxsO1xuICBncmFwaGljLnVwZGF0ZVByb3BzKGVsLCB7XG4gICAgc2hhcGU6IHtcbiAgICAgIHdpZHRoOiAwXG4gICAgfVxuICB9LCBhbmltYXRpb25Nb2RlbCwgZGF0YUluZGV4LCBmdW5jdGlvbiAoKSB7XG4gICAgZWwucGFyZW50ICYmIGVsLnBhcmVudC5yZW1vdmUoZWwpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gcmVtb3ZlU2VjdG9yKGRhdGFJbmRleCwgYW5pbWF0aW9uTW9kZWwsIGVsKSB7XG4gIC8vIE5vdCBzaG93IHRleHQgd2hlbiBhbmltYXRpbmdcbiAgZWwuc3R5bGUudGV4dCA9IG51bGw7XG4gIGdyYXBoaWMudXBkYXRlUHJvcHMoZWwsIHtcbiAgICBzaGFwZToge1xuICAgICAgcjogZWwuc2hhcGUucjBcbiAgICB9XG4gIH0sIGFuaW1hdGlvbk1vZGVsLCBkYXRhSW5kZXgsIGZ1bmN0aW9uICgpIHtcbiAgICBlbC5wYXJlbnQgJiYgZWwucGFyZW50LnJlbW92ZShlbCk7XG4gIH0pO1xufVxuXG52YXIgZ2V0TGF5b3V0ID0ge1xuICBjYXJ0ZXNpYW4yZDogZnVuY3Rpb24gKGRhdGEsIGRhdGFJbmRleCwgaXRlbU1vZGVsKSB7XG4gICAgdmFyIGxheW91dCA9IGRhdGEuZ2V0SXRlbUxheW91dChkYXRhSW5kZXgpO1xuICAgIHZhciBmaXhlZExpbmVXaWR0aCA9IGdldExpbmVXaWR0aChpdGVtTW9kZWwsIGxheW91dCk7IC8vIGZpeCBsYXlvdXQgd2l0aCBsaW5lV2lkdGhcblxuICAgIHZhciBzaWduWCA9IGxheW91dC53aWR0aCA+IDAgPyAxIDogLTE7XG4gICAgdmFyIHNpZ25ZID0gbGF5b3V0LmhlaWdodCA+IDAgPyAxIDogLTE7XG4gICAgcmV0dXJuIHtcbiAgICAgIHg6IGxheW91dC54ICsgc2lnblggKiBmaXhlZExpbmVXaWR0aCAvIDIsXG4gICAgICB5OiBsYXlvdXQueSArIHNpZ25ZICogZml4ZWRMaW5lV2lkdGggLyAyLFxuICAgICAgd2lkdGg6IGxheW91dC53aWR0aCAtIHNpZ25YICogZml4ZWRMaW5lV2lkdGgsXG4gICAgICBoZWlnaHQ6IGxheW91dC5oZWlnaHQgLSBzaWduWSAqIGZpeGVkTGluZVdpZHRoXG4gICAgfTtcbiAgfSxcbiAgcG9sYXI6IGZ1bmN0aW9uIChkYXRhLCBkYXRhSW5kZXgsIGl0ZW1Nb2RlbCkge1xuICAgIHZhciBsYXlvdXQgPSBkYXRhLmdldEl0ZW1MYXlvdXQoZGF0YUluZGV4KTtcbiAgICByZXR1cm4ge1xuICAgICAgY3g6IGxheW91dC5jeCxcbiAgICAgIGN5OiBsYXlvdXQuY3ksXG4gICAgICByMDogbGF5b3V0LnIwLFxuICAgICAgcjogbGF5b3V0LnIsXG4gICAgICBzdGFydEFuZ2xlOiBsYXlvdXQuc3RhcnRBbmdsZSxcbiAgICAgIGVuZEFuZ2xlOiBsYXlvdXQuZW5kQW5nbGVcbiAgICB9O1xuICB9XG59O1xuXG5mdW5jdGlvbiB1cGRhdGVTdHlsZShlbCwgZGF0YSwgZGF0YUluZGV4LCBpdGVtTW9kZWwsIGxheW91dCwgc2VyaWVzTW9kZWwsIGlzSG9yaXpvbnRhbCwgaXNQb2xhcikge1xuICB2YXIgY29sb3IgPSBkYXRhLmdldEl0ZW1WaXN1YWwoZGF0YUluZGV4LCAnY29sb3InKTtcbiAgdmFyIG9wYWNpdHkgPSBkYXRhLmdldEl0ZW1WaXN1YWwoZGF0YUluZGV4LCAnb3BhY2l0eScpO1xuICB2YXIgaXRlbVN0eWxlTW9kZWwgPSBpdGVtTW9kZWwuZ2V0TW9kZWwoJ2l0ZW1TdHlsZScpO1xuICB2YXIgaG92ZXJTdHlsZSA9IGl0ZW1Nb2RlbC5nZXRNb2RlbCgnZW1waGFzaXMuaXRlbVN0eWxlJykuZ2V0QmFySXRlbVN0eWxlKCk7XG5cbiAgaWYgKCFpc1BvbGFyKSB7XG4gICAgZWwuc2V0U2hhcGUoJ3InLCBpdGVtU3R5bGVNb2RlbC5nZXQoJ2JhckJvcmRlclJhZGl1cycpIHx8IDApO1xuICB9XG5cbiAgZWwudXNlU3R5bGUoenJVdGlsLmRlZmF1bHRzKHtcbiAgICBmaWxsOiBjb2xvcixcbiAgICBvcGFjaXR5OiBvcGFjaXR5XG4gIH0sIGl0ZW1TdHlsZU1vZGVsLmdldEJhckl0ZW1TdHlsZSgpKSk7XG4gIHZhciBjdXJzb3JTdHlsZSA9IGl0ZW1Nb2RlbC5nZXRTaGFsbG93KCdjdXJzb3InKTtcbiAgY3Vyc29yU3R5bGUgJiYgZWwuYXR0cignY3Vyc29yJywgY3Vyc29yU3R5bGUpO1xuICB2YXIgbGFiZWxQb3NpdGlvbk91dHNpZGUgPSBpc0hvcml6b250YWwgPyBsYXlvdXQuaGVpZ2h0ID4gMCA/ICdib3R0b20nIDogJ3RvcCcgOiBsYXlvdXQud2lkdGggPiAwID8gJ2xlZnQnIDogJ3JpZ2h0JztcblxuICBpZiAoIWlzUG9sYXIpIHtcbiAgICBzZXRMYWJlbChlbC5zdHlsZSwgaG92ZXJTdHlsZSwgaXRlbU1vZGVsLCBjb2xvciwgc2VyaWVzTW9kZWwsIGRhdGFJbmRleCwgbGFiZWxQb3NpdGlvbk91dHNpZGUpO1xuICB9XG5cbiAgZ3JhcGhpYy5zZXRIb3ZlclN0eWxlKGVsLCBob3ZlclN0eWxlKTtcbn0gLy8gSW4gY2FzZSB3aWR0aCBvciBoZWlnaHQgYXJlIHRvbyBzbWFsbC5cblxuXG5mdW5jdGlvbiBnZXRMaW5lV2lkdGgoaXRlbU1vZGVsLCByYXdMYXlvdXQpIHtcbiAgdmFyIGxpbmVXaWR0aCA9IGl0ZW1Nb2RlbC5nZXQoQkFSX0JPUkRFUl9XSURUSF9RVUVSWSkgfHwgMDtcbiAgcmV0dXJuIE1hdGgubWluKGxpbmVXaWR0aCwgTWF0aC5hYnMocmF3TGF5b3V0LndpZHRoKSwgTWF0aC5hYnMocmF3TGF5b3V0LmhlaWdodCkpO1xufVxuXG52YXIgTGFyZ2VQYXRoID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAnbGFyZ2VCYXInLFxuICBzaGFwZToge1xuICAgIHBvaW50czogW11cbiAgfSxcbiAgYnVpbGRQYXRoOiBmdW5jdGlvbiAoY3R4LCBzaGFwZSkge1xuICAgIC8vIERyYXdpbmcgbGluZXMgaXMgbW9yZSBlZmZpY2llbnQgdGhhbiBkcmF3aW5nXG4gICAgLy8gYSB3aG9sZSBsaW5lIG9yIGRyYXdpbmcgcmVjdHMuXG4gICAgdmFyIHBvaW50cyA9IHNoYXBlLnBvaW50cztcbiAgICB2YXIgc3RhcnRQb2ludCA9IHRoaXMuX19zdGFydFBvaW50O1xuICAgIHZhciB2YWx1ZUlkeCA9IHRoaXMuX192YWx1ZUlkeDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcG9pbnRzLmxlbmd0aDsgaSArPSAyKSB7XG4gICAgICBzdGFydFBvaW50W3RoaXMuX192YWx1ZUlkeF0gPSBwb2ludHNbaSArIHZhbHVlSWR4XTtcbiAgICAgIGN0eC5tb3ZlVG8oc3RhcnRQb2ludFswXSwgc3RhcnRQb2ludFsxXSk7XG4gICAgICBjdHgubGluZVRvKHBvaW50c1tpXSwgcG9pbnRzW2kgKyAxXSk7XG4gICAgfVxuICB9XG59KTtcblxuZnVuY3Rpb24gY3JlYXRlTGFyZ2Uoc2VyaWVzTW9kZWwsIGdyb3VwLCBpbmNyZW1lbnRhbCkge1xuICAvLyBUT0RPIHN1cHBvcnQgcG9sYXJcbiAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gIHZhciBzdGFydFBvaW50ID0gW107XG4gIHZhciB2YWx1ZUlkeCA9IGRhdGEuZ2V0TGF5b3V0KCd2YWx1ZUF4aXNIb3Jpem9udGFsJykgPyAxIDogMDtcbiAgc3RhcnRQb2ludFsxIC0gdmFsdWVJZHhdID0gZGF0YS5nZXRMYXlvdXQoJ3ZhbHVlQXhpc1N0YXJ0Jyk7XG4gIHZhciBlbCA9IG5ldyBMYXJnZVBhdGgoe1xuICAgIHNoYXBlOiB7XG4gICAgICBwb2ludHM6IGRhdGEuZ2V0TGF5b3V0KCdsYXJnZVBvaW50cycpXG4gICAgfSxcbiAgICBpbmNyZW1lbnRhbDogISFpbmNyZW1lbnRhbCxcbiAgICBfX3N0YXJ0UG9pbnQ6IHN0YXJ0UG9pbnQsXG4gICAgX192YWx1ZUlkeDogdmFsdWVJZHhcbiAgfSk7XG4gIGdyb3VwLmFkZChlbCk7XG4gIHNldExhcmdlU3R5bGUoZWwsIHNlcmllc01vZGVsLCBkYXRhKTtcbn1cblxuZnVuY3Rpb24gc2V0TGFyZ2VTdHlsZShlbCwgc2VyaWVzTW9kZWwsIGRhdGEpIHtcbiAgdmFyIGJvcmRlckNvbG9yID0gZGF0YS5nZXRWaXN1YWwoJ2JvcmRlckNvbG9yJykgfHwgZGF0YS5nZXRWaXN1YWwoJ2NvbG9yJyk7XG4gIHZhciBpdGVtU3R5bGUgPSBzZXJpZXNNb2RlbC5nZXRNb2RlbCgnaXRlbVN0eWxlJykuZ2V0SXRlbVN0eWxlKFsnY29sb3InLCAnYm9yZGVyQ29sb3InXSk7XG4gIGVsLnVzZVN0eWxlKGl0ZW1TdHlsZSk7XG4gIGVsLnN0eWxlLmZpbGwgPSBudWxsO1xuICBlbC5zdHlsZS5zdHJva2UgPSBib3JkZXJDb2xvcjtcbiAgZWwuc3R5bGUubGluZVdpZHRoID0gZGF0YS5nZXRMYXlvdXQoJ2JhcldpZHRoJyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXhIQTtBQUNBO0FBMEhBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUEzQ0E7QUFDQTtBQTZDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQXhCQTtBQUNBO0FBMEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJBO0FBQ0E7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/bar/BarView.js
