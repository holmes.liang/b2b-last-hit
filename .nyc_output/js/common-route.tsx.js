__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "policyPath", function() { return policyPath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderPolicyView", function() { return renderPolicyView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderPolicyContinue", function() { return renderPolicyContinue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endoPath", function() { return endoPath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endoEntryPath", function() { return endoEntryPath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endoViewPath", function() { return endoViewPath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderEndoComponent", function() { return renderEndoComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endoViewComponent", function() { return endoViewComponent; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");


var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/common/route/common-route.tsx";



var policyPath = function policyPath(props) {
  var itntCode = props.itntCode,
      productCode = props.productCode,
      policyId = props.policyId,
      _props$productVersion = props.productVersion,
      productVersion = _props$productVersion === void 0 ? "_" : _props$productVersion,
      _props$bizType = props.bizType,
      bizType = _props$bizType === void 0 ? "_" : _props$bizType,
      _props$type = props.type,
      type = _props$type === void 0 ? "view" : _props$type,
      _props$compare = props.compare,
      compare = _props$compare === void 0 ? false : _props$compare;

  if (type === "view") {
    return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].QUOTE_NORMAL_VIEW.replace(":itntCode", itntCode).replace(":prdtCode", productCode).replace(":policyId", policyId).replace(":productVersion", productVersion).replace(":bizType", bizType);
  } else {
    return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].NORMAL_QUOTE_CONTINUE.replace(":itntCode", itntCode).replace(":prdtCode", productCode).replace(":policyId", policyId).replace(":productVersion", productVersion).replace(":bizType", bizType);
  }
};
var renderPolicyView = function renderPolicyView(match) {
  var _match$params = match.params,
      itntCode = _match$params.itntCode,
      policyId = _match$params.policyId,
      productVersion = _match$params.productVersion,
      bizType = _match$params.bizType;

  var prdtCode = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(match, "params.prdtCode");

  prdtCode = prdtCode.toLowerCase();
  var isSuretyArr = ["fwb", "lb", "rb", "sb", "pb"].includes(prdtCode);
  var isTccArr = ["sarp", "tcc", "arpkc", "twps"].includes(prdtCode);
  var isEb = ["wicun", "wicn"].includes(prdtCode);
  var isLife = ["fmca", "enel", "hprt", "wlsc"].includes(prdtCode);
  var isLiaArr = ["publ", "pi", "pl", "dol"].includes(prdtCode);
  var ViewComponent;

  if (prdtCode === "boss") {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/boss\\/view$")("./".concat(itntCode, "/boss/view"));
    });
  } else if (isTccArr) {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/tcc\\/view$")("./".concat(itntCode, "/tcc/view"));
    });
  } else if (isSuretyArr) {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/surety\\/view$")("./".concat(itntCode, "/surety/view"));
    });
  } else if (isEb) {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/eb\\/view$")("./".concat(itntCode, "/eb/view"));
    });
  } else if (isLiaArr) {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/liability\\/view$")("./".concat(itntCode, "/liability/view"));
    });
  } else if (isLife) {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/wlsc\\/view$")("./".concat(itntCode, "/wlsc/view"));
    });
  } else if (prdtCode === "gch") {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__.e(/*! import() */ 281).then(__webpack_require__.bind(null, /*! @desk/master-policy/group-cancer/view */ "./src/app/desk/master-policy/group-cancer/view.tsx"));
    });
  } else if (prdtCode === "gpc") {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/sgp\\/view$")("./".concat(itntCode, "/sgp/view"));
    });
  } else {
    ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/view$")("./".concat(itntCode, "/").concat(prdtCode, "/view"));
    });
  }

  if (["ghm", "ph"].includes(prdtCode) || isLife) {
    return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(ViewComponent, {
      policyId: policyId,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75
      },
      __self: this
    });
  }

  return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(ViewComponent, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  });
};
var renderPolicyContinue = function renderPolicyContinue(match) {
  var _match$params2 = match.params,
      itntCode = _match$params2.itntCode,
      policyId = _match$params2.policyId,
      productVersion = _match$params2.productVersion,
      bizType = _match$params2.bizType;

  var prdtCode = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(match, "params.prdtCode");

  prdtCode = prdtCode.toLowerCase();
  var isSuretyArr = ["fwb", "lb", "rb", "sb", "pb"].includes(prdtCode);
  var isTccArr = ["sarp", "tcc", "arpkc", "twps"].includes(prdtCode);
  var isEb = ["wicun", "wicn"].includes(prdtCode);
  var isLife = ["fmca", "enel", "hprt", "wlsc"].includes(prdtCode);
  var isLiaArr = ["publ", "pi", "pl", "dol"].includes(prdtCode);
  var PageComponent;

  if (prdtCode === "boss") {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/boss$")("./".concat(itntCode, "/boss"));
    });
  } else if (isTccArr) {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/tcc$")("./".concat(itntCode, "/tcc"));
    });
  } else if (isSuretyArr) {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/surety$")("./".concat(itntCode, "/surety"));
    });
  } else if (isEb) {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/eb$")("./".concat(itntCode, "/eb"));
    });
  } else if (isLiaArr) {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/liability$")("./".concat(itntCode, "/liability"));
    });
  } else if (isLife) {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/wlsc$")("./".concat(itntCode, "/wlsc"));
    });
  } else if (prdtCode === "gch") {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return Promise.all(/*! import() */[__webpack_require__.e(304), __webpack_require__.e(306), __webpack_require__.e("group-cancer~._src_app_desk_master-policy_group-cancer_index-")]).then(__webpack_require__.bind(null, /*! @desk/master-policy/group-cancer */ "./src/app/desk/master-policy/group-cancer/index.tsx"));
    });
  } else if (prdtCode === "gpc") {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*\\/sgp\\/gpc$")("./".concat(itntCode, "/sgp/gpc"));
    });
  } else {
    PageComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/quote lazy recursive ^\\.\\/.*$")("./".concat(itntCode, "/").concat(prdtCode));
    });
  }

  return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(PageComponent, {
    identity: {
      itntCode: itntCode,
      policyId: policyId,
      prdtCode: prdtCode,
      productVersion: productVersion,
      bizType: bizType
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 109
    },
    __self: this
  });
};
var endoPath =
/*#__PURE__*/
function () {
  var _ref = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
  /*#__PURE__*/
  _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(props) {
    var itntCode, productCode, productVersion, policyId, endoType, endoId, callback, bizType;
    return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            itntCode = props.itntCode, productCode = props.productCode, productVersion = props.productVersion, policyId = props.policyId, endoType = props.endoType, endoId = props.endoId, callback = props.callback, bizType = props.bizType;

            if (!(endoType === "BASIC_INFO_ENDO")) {
              _context.next = 5;
              break;
            }

            return _context.abrupt("return", _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].BASIC_INFO_ENDO.replace(":policyId", policyId).replace(":itntCode", itntCode).replace(":productVersion", productVersion).replace(":productCode", productCode).replace(":endoId?", endoId));

          case 5:
            if (![_common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.NFN, _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.INCEPTION_CANCELLATION, _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.CESSION_ADJ].includes(endoType)) {
              _context.next = 9;
              break;
            }

            return _context.abrupt("return", _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].ENDORSEMENT_ENTRY_ENDO.replace(":endoType", endoType).replace(":policyId", policyId).replace(":endoId", endoId).replace(":itntCode", itntCode).replace(":productVersion", productVersion).replace(":productCode", productCode).replace(":bizType?", bizType || ""));

          case 9:
            return _context.abrupt("return", _common__WEBPACK_IMPORTED_MODULE_3__["Ajax"].get("".concat(_common__WEBPACK_IMPORTED_MODULE_3__["Apis"].ENDO_ID.replace(":endoId", endoId)), {}).then(function (response) {
              var _ref2 = response.body || {
                respData: {}
              },
                  respData = _ref2.respData;

              var step = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(respData, "ext._ui.step");

              if (step === "payment-endo") {
                callback && callback(_common__WEBPACK_IMPORTED_MODULE_3__["PATH"].QUOTE_PAYMENT_FOR_CART.replace(":transId", lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(respData, "transId")));
                return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].QUOTE_PAYMENT_FOR_CART.replace(":transId", lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(respData, "transId"));
              } else {
                var endoTypes = {
                  BOSS: "MEMBER_MOVEMENT",
                  GHS: "MEMBER_MOVEMENT_GHS",
                  FWB: "MEMBER_MOVEMENT_SURETY",
                  EFC: "MEMBER_MOVEMENT_EFC"
                };

                var endoTypeNormal = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(endoTypes, productCode);

                if (!endoType) return;
                return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].ENDORSEMENT_ENTRY_ENDO.replace(":policyId", policyId).replace(":endoId", endoId).replace(":endoType", endoTypeNormal).replace(":itntCode", itntCode).replace(":productVersion", productVersion).replace(":productCode", productCode).replace(":bizType?", bizType || "");
              }
            }).catch(function () {
              return "";
            }));

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function endoPath(_x) {
    return _ref.apply(this, arguments);
  };
}();
var endoEntryPath = function endoEntryPath(props) {
  var itntCode = props.itntCode,
      productCode = props.productCode,
      productVersion = props.productVersion,
      policyId = props.policyId,
      endoType = props.endoType,
      endoId = props.endoId,
      isTypesGet = props.isTypesGet,
      bizType = props.bizType;

  if (isTypesGet) {
    var endoTypes = {
      BOSS: _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT,
      GHS: _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_GHS,
      FWB: _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_SURETY,
      EFC: _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT_EFC
    };

    var endoTypeMovement = lodash__WEBPACK_IMPORTED_MODULE_2___default.a.get(endoTypes, productCode);

    return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].ENDORSEMENT_ENTRY_ENDO.replace(":itntCode", itntCode).replace(":productCode", productCode).replace(":productVersion", productVersion).replace(":policyId", policyId).replace(":endoId", endoId).replace(":endoType", endoType === _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.MEMBER_MOVEMENT ? endoTypeMovement : endoType).replace(":bizType?", bizType || "");
  } else {
    return _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].ENDORSEMENT_ENTRY_ENDO.replace(":itntCode", itntCode).replace(":productCode", productCode).replace(":productVersion", productVersion).replace(":policyId", policyId).replace(":endoId", endoId).replace(":endoType", endoType).replace(":bizType?", bizType || "");
  }
};
var endoViewPath = function endoViewPath(props) {
  var itntCode = props.itntCode,
      productCode = props.productCode,
      productVersion = props.productVersion,
      endoId = props.endoId,
      policyId = props.policyId,
      endoType = props.endoType;
  var path;

  if (endoType === _common__WEBPACK_IMPORTED_MODULE_3__["Consts"].ENDO_TYPES.BASIC_INFO_ENDO) {
    path = _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].BASIC_INFO_ENDO_VIEW.replace(":endoId", endoId).replace(":itntCode", itntCode).replace(":productVersion", productVersion).replace(":productCode", productCode);
  } else {
    path = _common__WEBPACK_IMPORTED_MODULE_3__["PATH"].ENDORSEMENT_VIEW.replace(":endoId", endoId).replace(":itntCode", itntCode).replace(":productVersion", productVersion).replace(":productCode", productCode);
  }

  if (policyId) {
    path = path.replace(":policyId?", policyId);
  } else {
    path = path.replace("/:policyId?", "");
  }

  return path;
};
var renderEndoComponent = function renderEndoComponent(props) {
  if (props.itntCode === "SAIC") {
    if (props.productCode === "BOSS") {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
        return Promise.all(/*! import() | endorsement-entry */[__webpack_require__.e("endorsement-entry124~._src_app_desk_endorsement_components_attachment-blob.tsx~867218f3"), __webpack_require__.e("endorsement-entry122~._src_app_desk_endorsement_b"), __webpack_require__.e("default~claims-ghs~claims-phs~claims-sme~endorsement-entry~endorsement-entry174~endorsement-entry178~4b266ddd"), __webpack_require__.e("compare-quote1455~._src_app_desk_c"), __webpack_require__.e("endorsement-entry126~._src_app_desk_e"), __webpack_require__.e("endorsement-entry128~._src_app_desk_endorsement_i"), __webpack_require__.e("endorsement-entry~._src_app_desk_endorsement_non-financial_boss.tsx~a3cabfe4")]).then(__webpack_require__.bind(null, /*! @desk/endorsement/non-financial/boss */ "./src/app/desk/endorsement/non-financial/boss.tsx"));
      });
    } else if (_common__WEBPACK_IMPORTED_MODULE_3__["Utils"].isTccSuretyLia(props.productCode)) {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
        return __webpack_require__("./src/app/desk/endorsement lazy recursive ^\\.\\/.*\\/non\\-financial\\/tcc$")("./".concat(props.itntCode, "/non-financial/tcc"));
      });
    } else {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
        return __webpack_require__("./src/app/desk/endorsement lazy recursive ^\\.\\/.*$")("./".concat(props.itntCode, "/non-financial/").concat(props.productCode.toLowerCase()));
      });
    }
  } else if (props.itntCode === "SAIC_THAI") {
    if (props.productCode === "CMI") {
      return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
        return __webpack_require__("./src/app/desk/endorsement lazy recursive ^\\.\\/.*\\/non\\-financial\\/vmi$")("./".concat(props.itntCode, "/non-financial/vmi"));
      });
    }

    return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
      return __webpack_require__("./src/app/desk/endorsement lazy recursive ^\\.\\/.*$")("./".concat(props.itntCode, "/non-financial/").concat(props.productCode.toLowerCase()));
    });
  } else {
    return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].Fragment, null);
  }
};
var endoViewComponent = function endoViewComponent(match) {
  var endoId = match.params.endoId;
  var ViewComponent;
  ViewComponent = _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].lazy(function () {
    return __webpack_require__.e(/*! import() | endorsement-view */ "endorsement-view~._src_app_desk_endorsement_query-view.tsx~695698ea").then(__webpack_require__.bind(null, /*! @desk/endorsement/query-view */ "./src/app/desk/endorsement/query-view.tsx"));
  });
  return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(ViewComponent, {
    endoId: endoId,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 293
    },
    __self: this
  });
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29tbW9uL3JvdXRlL2NvbW1vbi1yb3V0ZS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9jb21tb24vcm91dGUvY29tbW9uLXJvdXRlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQge0FqYXgsIEFwaXMsIENvbnN0cywgUEFUSCwgVXRpbHN9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQge1JlYWN0fSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7QWpheFJlc3BvbnNlfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5cbmludGVyZmFjZSBJUHJvcHMge1xuICAgIGl0bnRDb2RlOiBzdHJpbmcsXG4gICAgcHJvZHVjdENvZGU6IHN0cmluZyxcbiAgICBwcm9kdWN0VmVyc2lvbjogYW55LFxuICAgIHBvbGljeUlkOiBzdHJpbmcsXG4gICAgYml6VHlwZTogYW55LFxuICAgIGNvbXBhcmU/OiBib29sZWFuLFxuICAgIHR5cGU6IFwidmlld1wiIHwgXCJ3b3JrXCIsXG59XG5cbmludGVyZmFjZSBFbmRvUHJvcHMge1xuICAgIGl0bnRDb2RlOiBzdHJpbmcsXG4gICAgcHJvZHVjdENvZGU6IHN0cmluZyxcbiAgICBwcm9kdWN0VmVyc2lvbjogYW55LFxuICAgIHBvbGljeUlkOiBzdHJpbmcsXG4gICAgZW5kb1R5cGU6IGFueSxcbiAgICBlbmRvSWQ6IGFueSxcbiAgICBjYWxsYmFjaz86IGFueTtcbiAgICBiaXpUeXBlPzogYW55LFxufVxuXG5leHBvcnQgY29uc3QgcG9saWN5UGF0aCA9IChwcm9wczogSVByb3BzKSA9PiB7XG4gICAgY29uc3Qge2l0bnRDb2RlLCBwcm9kdWN0Q29kZSwgcG9saWN5SWQsIHByb2R1Y3RWZXJzaW9uID0gXCJfXCIsIGJpelR5cGUgPSBcIl9cIiwgdHlwZSA9IFwidmlld1wiLCBjb21wYXJlID0gZmFsc2V9ID0gcHJvcHM7XG4gICAgaWYgKHR5cGUgPT09IFwidmlld1wiKSB7XG4gICAgICAgIHJldHVybiBQQVRILlFVT1RFX05PUk1BTF9WSUVXLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRudENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwcmR0Q29kZVwiLCBwcm9kdWN0Q29kZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnBvbGljeUlkXCIsIHBvbGljeUlkKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdFZlcnNpb25cIiwgcHJvZHVjdFZlcnNpb24pXG4gICAgICAgICAgICAucmVwbGFjZShcIjpiaXpUeXBlXCIsIGJpelR5cGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBQQVRILk5PUk1BTF9RVU9URV9DT05USU5VRS5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0bnRDb2RlKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJkdENvZGVcIiwgcHJvZHVjdENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByb2R1Y3RWZXJzaW9uXCIsIHByb2R1Y3RWZXJzaW9uKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6Yml6VHlwZVwiLCBiaXpUeXBlKTtcbiAgICB9XG5cbn07XG5cbmV4cG9ydCBjb25zdCByZW5kZXJQb2xpY3lWaWV3ID0gKG1hdGNoOiBhbnkpID0+IHtcbiAgICBjb25zdCB7aXRudENvZGUsIHBvbGljeUlkLCBwcm9kdWN0VmVyc2lvbiwgYml6VHlwZX0gPSBtYXRjaC5wYXJhbXM7XG4gICAgbGV0IHByZHRDb2RlID0gXy5nZXQobWF0Y2gsIFwicGFyYW1zLnByZHRDb2RlXCIpO1xuICAgIHByZHRDb2RlID0gcHJkdENvZGUudG9Mb3dlckNhc2UoKTtcbiAgICBjb25zdCBpc1N1cmV0eUFyciA9IFtcImZ3YlwiLCBcImxiXCIsIFwicmJcIiwgXCJzYlwiLCBcInBiXCJdLmluY2x1ZGVzKHByZHRDb2RlKTtcbiAgICBjb25zdCBpc1RjY0FyciA9IFtcInNhcnBcIiwgXCJ0Y2NcIiwgXCJhcnBrY1wiLCBcInR3cHNcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzRWIgPSBbXCJ3aWN1blwiLCBcIndpY25cIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzTGlmZSA9IFtcImZtY2FcIiwgXCJlbmVsXCIsIFwiaHBydFwiLCBcIndsc2NcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzTGlhQXJyID0gW1wicHVibFwiLCBcInBpXCIsIFwicGxcIiwgXCJkb2xcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGxldCBWaWV3Q29tcG9uZW50OiBhbnk7XG4gICAgaWYgKHByZHRDb2RlID09PSBcImJvc3NcIikge1xuICAgICAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L2Jvc3Mvdmlld2ApKTtcbiAgICB9IGVsc2UgaWYgKGlzVGNjQXJyKSB7XG4gICAgICAgIFZpZXdDb21wb25lbnQgPSBSZWFjdC5sYXp5KCgpID0+IGltcG9ydChgQGRlc2svcXVvdGUvJHtpdG50Q29kZX0vdGNjL3ZpZXdgKSk7XG4gICAgfSBlbHNlIGlmIChpc1N1cmV0eUFycikge1xuICAgICAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L3N1cmV0eS92aWV3YCkpO1xuICAgIH0gZWxzZSBpZiAoaXNFYikge1xuICAgICAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L2ViL3ZpZXdgKSk7XG4gICAgfSBlbHNlIGlmIChpc0xpYUFycikge1xuICAgICAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L2xpYWJpbGl0eS92aWV3YCkpO1xuICAgIH0gZWxzZSBpZiAoaXNMaWZlKSB7XG4gICAgICAgIFZpZXdDb21wb25lbnQgPSBSZWFjdC5sYXp5KCgpID0+IGltcG9ydChgQGRlc2svcXVvdGUvJHtpdG50Q29kZX0vd2xzYy92aWV3YCkpO1xuICAgIH0gZWxzZSBpZiAocHJkdENvZGUgPT09IFwiZ2NoXCIpIHtcbiAgICAgICAgVmlld0NvbXBvbmVudCA9IFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KGBAZGVzay9tYXN0ZXItcG9saWN5L2dyb3VwLWNhbmNlci92aWV3YCkpO1xuICAgIH0gZWxzZSBpZiAocHJkdENvZGUgPT09IFwiZ3BjXCIpIHtcbiAgICAgICAgVmlld0NvbXBvbmVudCA9IFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KGBAZGVzay9xdW90ZS8ke2l0bnRDb2RlfS9zZ3Avdmlld2ApKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9LyR7cHJkdENvZGV9L3ZpZXdgKSk7XG4gICAgfVxuICAgIGlmIChbXCJnaG1cIiwgXCJwaFwiXS5pbmNsdWRlcyhwcmR0Q29kZSkgfHwgaXNMaWZlKSB7XG4gICAgICAgIHJldHVybiA8Vmlld0NvbXBvbmVudCBwb2xpY3lJZD17cG9saWN5SWR9Lz47XG4gICAgfVxuICAgIHJldHVybiA8Vmlld0NvbXBvbmVudC8+O1xufTtcblxuZXhwb3J0IGNvbnN0IHJlbmRlclBvbGljeUNvbnRpbnVlID0gKG1hdGNoOiBhbnkpID0+IHtcbiAgICBjb25zdCB7aXRudENvZGUsIHBvbGljeUlkLCBwcm9kdWN0VmVyc2lvbiwgYml6VHlwZX0gPSBtYXRjaC5wYXJhbXM7XG4gICAgbGV0IHByZHRDb2RlID0gXy5nZXQobWF0Y2gsIFwicGFyYW1zLnByZHRDb2RlXCIpO1xuICAgIHByZHRDb2RlID0gcHJkdENvZGUudG9Mb3dlckNhc2UoKTtcbiAgICBjb25zdCBpc1N1cmV0eUFyciA9IFtcImZ3YlwiLCBcImxiXCIsIFwicmJcIiwgXCJzYlwiLCBcInBiXCJdLmluY2x1ZGVzKHByZHRDb2RlKTtcbiAgICBjb25zdCBpc1RjY0FyciA9IFtcInNhcnBcIiwgXCJ0Y2NcIiwgXCJhcnBrY1wiLCBcInR3cHNcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzRWIgPSBbXCJ3aWN1blwiLCBcIndpY25cIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzTGlmZSA9IFtcImZtY2FcIiwgXCJlbmVsXCIsIFwiaHBydFwiLCBcIndsc2NcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGNvbnN0IGlzTGlhQXJyID0gW1wicHVibFwiLCBcInBpXCIsIFwicGxcIiwgXCJkb2xcIl0uaW5jbHVkZXMocHJkdENvZGUpO1xuICAgIGxldCBQYWdlQ29tcG9uZW50OiBhbnk7XG4gICAgaWYgKHByZHRDb2RlID09PSBcImJvc3NcIikge1xuICAgICAgICBQYWdlQ29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L2Jvc3NgKSk7XG4gICAgfSBlbHNlIGlmIChpc1RjY0Fycikge1xuICAgICAgICBQYWdlQ29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L3RjY2ApKTtcbiAgICB9IGVsc2UgaWYgKGlzU3VyZXR5QXJyKSB7XG4gICAgICAgIFBhZ2VDb21wb25lbnQgPSBSZWFjdC5sYXp5KCgpID0+IGltcG9ydChgQGRlc2svcXVvdGUvJHtpdG50Q29kZX0vc3VyZXR5YCkpO1xuICAgIH0gZWxzZSBpZiAoaXNFYikge1xuICAgICAgICBQYWdlQ29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9L2ViYCkpO1xuICAgIH0gZWxzZSBpZiAoaXNMaWFBcnIpIHtcbiAgICAgICAgUGFnZUNvbXBvbmVudCA9IFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KGBAZGVzay9xdW90ZS8ke2l0bnRDb2RlfS9saWFiaWxpdHlgKSk7XG4gICAgfSBlbHNlIGlmIChpc0xpZmUpIHtcbiAgICAgICAgUGFnZUNvbXBvbmVudCA9IFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KGBAZGVzay9xdW90ZS8ke2l0bnRDb2RlfS93bHNjYCkpO1xuICAgIH0gZWxzZSBpZiAocHJkdENvZGUgPT09IFwiZ2NoXCIpIHtcbiAgICAgICAgUGFnZUNvbXBvbmVudCA9IFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KGBAZGVzay9tYXN0ZXItcG9saWN5L2dyb3VwLWNhbmNlcmApKTtcbiAgICB9IGVsc2UgaWYgKHByZHRDb2RlID09PSBcImdwY1wiKSB7XG4gICAgICAgIFBhZ2VDb21wb25lbnQgPSBSZWFjdC5sYXp5KCgpID0+IGltcG9ydChgQGRlc2svcXVvdGUvJHtpdG50Q29kZX0vc2dwL2dwY2ApKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBQYWdlQ29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoYEBkZXNrL3F1b3RlLyR7aXRudENvZGV9LyR7cHJkdENvZGV9YCkpO1xuICAgIH1cbiAgICByZXR1cm4gPFBhZ2VDb21wb25lbnQgaWRlbnRpdHk9e3tpdG50Q29kZSwgcG9saWN5SWQsIHByZHRDb2RlLCBwcm9kdWN0VmVyc2lvbiwgYml6VHlwZX19Lz47XG59O1xuXG5leHBvcnQgY29uc3QgZW5kb1BhdGggPSBhc3luYyAocHJvcHM6IEVuZG9Qcm9wcykgPT4ge1xuICAgIGNvbnN0IHtcbiAgICAgICAgaXRudENvZGUsXG4gICAgICAgIHByb2R1Y3RDb2RlLFxuICAgICAgICBwcm9kdWN0VmVyc2lvbixcbiAgICAgICAgcG9saWN5SWQsXG4gICAgICAgIGVuZG9UeXBlLFxuICAgICAgICBlbmRvSWQsXG4gICAgICAgIGNhbGxiYWNrLFxuICAgICAgICBiaXpUeXBlLFxuICAgIH0gPSBwcm9wcztcbiAgICBpZiAoZW5kb1R5cGUgPT09IFwiQkFTSUNfSU5GT19FTkRPXCIpIHtcbiAgICAgICAgcmV0dXJuIFBBVEguQkFTSUNfSU5GT19FTkRPLnJlcGxhY2UoXCI6cG9saWN5SWRcIiwgcG9saWN5SWQpXG4gICAgICAgICAgICAucmVwbGFjZShcIjppdG50Q29kZVwiLCBpdG50Q29kZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByb2R1Y3RWZXJzaW9uXCIsIHByb2R1Y3RWZXJzaW9uKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdENvZGVcIiwgcHJvZHVjdENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjplbmRvSWQ/XCIsIGVuZG9JZCk7XG4gICAgfSBlbHNlIGlmIChbQ29uc3RzLkVORE9fVFlQRVMuTkZOLFxuICAgICAgICBDb25zdHMuRU5ET19UWVBFUy5JTkNFUFRJT05fQ0FOQ0VMTEFUSU9OLFxuICAgICAgICBDb25zdHMuRU5ET19UWVBFUy5DRVNTSU9OX0FESixcbiAgICBdLmluY2x1ZGVzKGVuZG9UeXBlKSkge1xuICAgICAgICByZXR1cm4gUEFUSC5FTkRPUlNFTUVOVF9FTlRSWV9FTkRPLnJlcGxhY2UoXCI6ZW5kb1R5cGVcIiwgZW5kb1R5cGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9JZFwiLCBlbmRvSWQpXG4gICAgICAgICAgICAucmVwbGFjZShcIjppdG50Q29kZVwiLCBpdG50Q29kZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByb2R1Y3RWZXJzaW9uXCIsIHByb2R1Y3RWZXJzaW9uKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdENvZGVcIiwgcHJvZHVjdENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpiaXpUeXBlP1wiLCBiaXpUeXBlIHx8IFwiXCIpXG4gICAgICAgICAgICA7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIEFqYXguZ2V0KGAke0FwaXMuRU5ET19JRC5yZXBsYWNlKFwiOmVuZG9JZFwiLCBlbmRvSWQpfWAsIHt9KVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCB7cmVzcERhdGF9ID0gcmVzcG9uc2UuYm9keSB8fCB7cmVzcERhdGE6IHt9fTtcbiAgICAgICAgICAgICAgICBjb25zdCBzdGVwID0gXy5nZXQocmVzcERhdGEsIFwiZXh0Ll91aS5zdGVwXCIpO1xuICAgICAgICAgICAgICAgIGlmIChzdGVwID09PSBcInBheW1lbnQtZW5kb1wiKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrICYmIGNhbGxiYWNrKFBBVEguUVVPVEVfUEFZTUVOVF9GT1JfQ0FSVC5yZXBsYWNlKFwiOnRyYW5zSWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIF8uZ2V0KHJlc3BEYXRhLCBcInRyYW5zSWRcIiksXG4gICAgICAgICAgICAgICAgICAgICkpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUEFUSC5RVU9URV9QQVlNRU5UX0ZPUl9DQVJULnJlcGxhY2UoXCI6dHJhbnNJZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgXy5nZXQocmVzcERhdGEsIFwidHJhbnNJZFwiKSxcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBsZXQgZW5kb1R5cGVzID1cbiAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBCT1NTOiBcIk1FTUJFUl9NT1ZFTUVOVFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIEdIUzogXCJNRU1CRVJfTU9WRU1FTlRfR0hTXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgRldCOiBcIk1FTUJFUl9NT1ZFTUVOVF9TVVJFVFlcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBFRkM6IFwiTUVNQkVSX01PVkVNRU5UX0VGQ1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZW5kb1R5cGVOb3JtYWwgPSBfLmdldChlbmRvVHlwZXMsIHByb2R1Y3RDb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFlbmRvVHlwZSkgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gUEFUSC5FTkRPUlNFTUVOVF9FTlRSWV9FTkRPXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9JZFwiLCBlbmRvSWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcIjplbmRvVHlwZVwiLCBlbmRvVHlwZU5vcm1hbClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0bnRDb2RlKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdFZlcnNpb25cIiwgcHJvZHVjdFZlcnNpb24pXG4gICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZShcIjpwcm9kdWN0Q29kZVwiLCBwcm9kdWN0Q29kZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKFwiOmJpelR5cGU/XCIsIGJpelR5cGUgfHwgXCJcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KS5jYXRjaCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFwiXCI7XG4gICAgICAgICAgICB9KTtcbiAgICB9XG59O1xuXG5pbnRlcmZhY2UgRW5kb1BhdGhQcm9wcyB7XG4gICAgaXRudENvZGU6IHN0cmluZyxcbiAgICBwcm9kdWN0Q29kZTogc3RyaW5nLFxuICAgIHByb2R1Y3RWZXJzaW9uOiBzdHJpbmcsXG4gICAgcG9saWN5SWQ6IHN0cmluZyxcbiAgICBlbmRvVHlwZTogc3RyaW5nLFxuICAgIGVuZG9JZDogc3RyaW5nLFxuICAgIGlzVHlwZXNHZXQ6IGJvb2xlYW4sXG4gICAgYml6VHlwZT86IHN0cmluZyxcbn1cblxuZXhwb3J0IGNvbnN0IGVuZG9FbnRyeVBhdGggPSAocHJvcHM6IEVuZG9QYXRoUHJvcHMpID0+IHtcbiAgICBjb25zdCB7XG4gICAgICAgIGl0bnRDb2RlLFxuICAgICAgICBwcm9kdWN0Q29kZSxcbiAgICAgICAgcHJvZHVjdFZlcnNpb24sXG4gICAgICAgIHBvbGljeUlkLFxuICAgICAgICBlbmRvVHlwZSxcbiAgICAgICAgZW5kb0lkLFxuICAgICAgICBpc1R5cGVzR2V0LFxuICAgICAgICBiaXpUeXBlLFxuICAgIH0gPSBwcm9wcztcbiAgICBpZiAoaXNUeXBlc0dldCkge1xuICAgICAgICBsZXQgZW5kb1R5cGVzID0ge1xuICAgICAgICAgICAgQk9TUzogQ29uc3RzLkVORE9fVFlQRVMuTUVNQkVSX01PVkVNRU5ULFxuICAgICAgICAgICAgR0hTOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRfR0hTLFxuICAgICAgICAgICAgRldCOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRfU1VSRVRZLFxuICAgICAgICAgICAgRUZDOiBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRfRUZDLFxuICAgICAgICB9O1xuICAgICAgICBjb25zdCBlbmRvVHlwZU1vdmVtZW50ID0gXy5nZXQoZW5kb1R5cGVzLCBwcm9kdWN0Q29kZSk7XG4gICAgICAgIHJldHVybiBQQVRILkVORE9SU0VNRU5UX0VOVFJZX0VORE9cbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0bnRDb2RlKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdENvZGVcIiwgcHJvZHVjdENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwcm9kdWN0VmVyc2lvblwiLCBwcm9kdWN0VmVyc2lvbilcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnBvbGljeUlkXCIsIHBvbGljeUlkKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6ZW5kb0lkXCIsIGVuZG9JZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9UeXBlXCIsIGVuZG9UeXBlID09PSBDb25zdHMuRU5ET19UWVBFUy5NRU1CRVJfTU9WRU1FTlRcbiAgICAgICAgICAgICAgICA/IGVuZG9UeXBlTW92ZW1lbnQgOiBlbmRvVHlwZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOmJpelR5cGU/XCIsIGJpelR5cGUgfHwgXCJcIilcbiAgICAgICAgICAgIDtcbiAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gUEFUSC5FTkRPUlNFTUVOVF9FTlRSWV9FTkRPXG4gICAgICAgICAgICAucmVwbGFjZShcIjppdG50Q29kZVwiLCBpdG50Q29kZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByb2R1Y3RDb2RlXCIsIHByb2R1Y3RDb2RlKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdFZlcnNpb25cIiwgcHJvZHVjdFZlcnNpb24pXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwb2xpY3lJZFwiLCBwb2xpY3lJZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOmVuZG9JZFwiLCBlbmRvSWQpXG4gICAgICAgICAgICAucmVwbGFjZShcIjplbmRvVHlwZVwiLCBlbmRvVHlwZSlcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOmJpelR5cGU/XCIsIGJpelR5cGUgfHwgXCJcIilcbiAgICAgICAgICAgIDtcbiAgICB9XG59O1xuXG5pbnRlcmZhY2UgRW5kb1ZpZXcge1xuICAgIGl0bnRDb2RlOiBzdHJpbmcsXG4gICAgcHJvZHVjdENvZGU6IHN0cmluZyxcbiAgICBwcm9kdWN0VmVyc2lvbjogc3RyaW5nLFxuICAgIGVuZG9JZDogc3RyaW5nLFxuICAgIHBvbGljeUlkPzogc3RyaW5nLFxuICAgIGVuZG9UeXBlPzogc3RyaW5nLFxufVxuXG5leHBvcnQgY29uc3QgZW5kb1ZpZXdQYXRoID0gKHByb3BzOiBFbmRvVmlldykgPT4ge1xuICAgIGNvbnN0IHtcbiAgICAgICAgaXRudENvZGUsXG4gICAgICAgIHByb2R1Y3RDb2RlLFxuICAgICAgICBwcm9kdWN0VmVyc2lvbixcbiAgICAgICAgZW5kb0lkLFxuICAgICAgICBwb2xpY3lJZCxcbiAgICAgICAgZW5kb1R5cGUsXG4gICAgfSA9IHByb3BzO1xuICAgIGxldCBwYXRoOiBhbnk7XG4gICAgaWYgKGVuZG9UeXBlID09PSBDb25zdHMuRU5ET19UWVBFUy5CQVNJQ19JTkZPX0VORE8pIHtcbiAgICAgICAgcGF0aCA9IFBBVEguQkFTSUNfSU5GT19FTkRPX1ZJRVcucmVwbGFjZShcIjplbmRvSWRcIiwgZW5kb0lkKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6aXRudENvZGVcIiwgaXRudENvZGUpXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwcm9kdWN0VmVyc2lvblwiLCBwcm9kdWN0VmVyc2lvbilcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOnByb2R1Y3RDb2RlXCIsIHByb2R1Y3RDb2RlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBwYXRoID0gUEFUSC5FTkRPUlNFTUVOVF9WSUVXLnJlcGxhY2UoXCI6ZW5kb0lkXCIsIGVuZG9JZClcbiAgICAgICAgICAgIC5yZXBsYWNlKFwiOml0bnRDb2RlXCIsIGl0bnRDb2RlKVxuICAgICAgICAgICAgLnJlcGxhY2UoXCI6cHJvZHVjdFZlcnNpb25cIiwgcHJvZHVjdFZlcnNpb24pXG4gICAgICAgICAgICAucmVwbGFjZShcIjpwcm9kdWN0Q29kZVwiLCBwcm9kdWN0Q29kZSk7XG4gICAgfVxuICAgIGlmIChwb2xpY3lJZCkge1xuICAgICAgICBwYXRoID0gcGF0aC5yZXBsYWNlKFwiOnBvbGljeUlkP1wiLCBwb2xpY3lJZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcGF0aCA9IHBhdGgucmVwbGFjZShcIi86cG9saWN5SWQ/XCIsIFwiXCIpO1xuICAgIH1cbiAgICByZXR1cm4gcGF0aDtcbn07XG5cbmV4cG9ydCBjb25zdCByZW5kZXJFbmRvQ29tcG9uZW50ID0gKHByb3BzOiB7IGl0bnRDb2RlOiBzdHJpbmcsIHByb2R1Y3RDb2RlOiBzdHJpbmcgfSkgPT4ge1xuICAgIGlmIChwcm9wcy5pdG50Q29kZSA9PT0gXCJTQUlDXCIpIHtcbiAgICAgICAgaWYgKHByb3BzLnByb2R1Y3RDb2RlID09PSBcIkJPU1NcIikge1xuICAgICAgICAgICAgcmV0dXJuIFJlYWN0LmxhenkoKCkgPT4gaW1wb3J0KC8qIHdlYnBhY2tDaHVua05hbWU6IFwiZW5kb3JzZW1lbnQtZW50cnlcIiAqLyBcIkBkZXNrL2VuZG9yc2VtZW50L25vbi1maW5hbmNpYWwvYm9zc1wiKSk7XG4gICAgICAgIH0gZWxzZSBpZiAoVXRpbHMuaXNUY2NTdXJldHlMaWEocHJvcHMucHJvZHVjdENvZGUpKSB7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJlbmRvcnNlbWVudC1lbnRyeVwiICovIGBAZGVzay9lbmRvcnNlbWVudC8ke3Byb3BzLml0bnRDb2RlfS9ub24tZmluYW5jaWFsL3RjY2ApKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBSZWFjdC5sYXp5KCgpID0+IGltcG9ydCgvKiB3ZWJwYWNrQ2h1bmtOYW1lOiBcImVuZG9yc2VtZW50LWVudHJ5XCIgKi8gYEBkZXNrL2VuZG9yc2VtZW50LyR7cHJvcHMuaXRudENvZGV9L25vbi1maW5hbmNpYWwvJHtwcm9wcy5wcm9kdWN0Q29kZS50b0xvd2VyQ2FzZSgpfWApKTtcbiAgICAgICAgfVxuICAgIH0gZWxzZSBpZiAocHJvcHMuaXRudENvZGUgPT09IFwiU0FJQ19USEFJXCIpIHtcbiAgICAgICAgaWYgKHByb3BzLnByb2R1Y3RDb2RlID09PSBcIkNNSVwiKSB7XG4gICAgICAgICAgICByZXR1cm4gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJlbmRvcnNlbWVudC1lbnRyeVwiICovIGBAZGVzay9lbmRvcnNlbWVudC8ke3Byb3BzLml0bnRDb2RlfS9ub24tZmluYW5jaWFsL3ZtaWApKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJlbmRvcnNlbWVudC1lbnRyeVwiICovIGBAZGVzay9lbmRvcnNlbWVudC8ke3Byb3BzLml0bnRDb2RlfS9ub24tZmluYW5jaWFsLyR7cHJvcHMucHJvZHVjdENvZGUudG9Mb3dlckNhc2UoKX1gKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIDw+PC8+O1xuICAgIH1cbn07XG5cbmV4cG9ydCBjb25zdCBlbmRvVmlld0NvbXBvbmVudCA9IChtYXRjaDogYW55KSA9PiB7XG4gICAgY29uc3Qge2VuZG9JZH0gPSBtYXRjaC5wYXJhbXM7XG4gICAgbGV0IFZpZXdDb21wb25lbnQ6IGFueTtcbiAgICBWaWV3Q29tcG9uZW50ID0gUmVhY3QubGF6eSgoKSA9PiBpbXBvcnQoLyogd2VicGFja0NodW5rTmFtZTogXCJlbmRvcnNlbWVudC12aWV3XCIgKi8gXCJAZGVzay9lbmRvcnNlbWVudC9xdWVyeS12aWV3XCIpKTtcbiAgICByZXR1cm4gPFZpZXdDb21wb25lbnQgZW5kb0lkPXtlbmRvSWR9Lz47XG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBd0JBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFLQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFIQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBK0JBO0FBQ0E7QUFBQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUdBO0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFBQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUE4RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUFBO0FBVUE7QUFDQTtBQVNBO0FBQ0E7QUFXQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBSUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/common/route/common-route.tsx
