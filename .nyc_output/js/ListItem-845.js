__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_util_es_PureRenderMixin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/PureRenderMixin */ "./node_modules/rc-util/es/PureRenderMixin.js");
/* harmony import */ var rc_util_es_PureRenderMixin__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rc_util_es_PureRenderMixin__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_lazy_load__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-lazy-load */ "./node_modules/react-lazy-load/lib/LazyLoad.js");
/* harmony import */ var react_lazy_load__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_lazy_load__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _checkbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../checkbox */ "./node_modules/antd/es/checkbox/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}







var ListItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ListItem, _React$Component);

  function ListItem() {
    _classCallCheck(this, ListItem);

    return _possibleConstructorReturn(this, _getPrototypeOf(ListItem).apply(this, arguments));
  }

  _createClass(ListItem, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return rc_util_es_PureRenderMixin__WEBPACK_IMPORTED_MODULE_2___default.a.shouldComponentUpdate.apply(this, args);
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames;

      var _this$props = this.props,
          renderedText = _this$props.renderedText,
          renderedEl = _this$props.renderedEl,
          item = _this$props.item,
          lazy = _this$props.lazy,
          checked = _this$props.checked,
          disabled = _this$props.disabled,
          prefixCls = _this$props.prefixCls,
          onClick = _this$props.onClick;
      var className = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-content-item"), true), _defineProperty(_classNames, "".concat(prefixCls, "-content-item-disabled"), disabled || item.disabled), _classNames));
      var title;

      if (typeof renderedText === 'string' || typeof renderedText === 'number') {
        title = String(renderedText);
      }

      var listItem = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", {
        className: className,
        title: title,
        onClick: disabled || item.disabled ? undefined : function () {
          return onClick(item);
        }
      }, react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_checkbox__WEBPACK_IMPORTED_MODULE_4__["default"], {
        checked: checked,
        disabled: disabled || item.disabled
      }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-content-item-text")
      }, renderedEl));
      var children = null;

      if (lazy) {
        var lazyProps = _extends({
          height: 32,
          offset: 500,
          throttle: 0,
          debounce: false
        }, lazy);

        children = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](react_lazy_load__WEBPACK_IMPORTED_MODULE_3___default.a, lazyProps, listItem);
      } else {
        children = listItem;
      }

      return children;
    }
  }]);

  return ListItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90cmFuc2Zlci9MaXN0SXRlbS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvdHJhbnNmZXIvTGlzdEl0ZW0uanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IFB1cmVSZW5kZXJNaXhpbiBmcm9tICdyYy11dGlsL2xpYi9QdXJlUmVuZGVyTWl4aW4nO1xuaW1wb3J0IExhenlsb2FkIGZyb20gJ3JlYWN0LWxhenktbG9hZCc7XG5pbXBvcnQgQ2hlY2tib3ggZnJvbSAnLi4vY2hlY2tib3gnO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTGlzdEl0ZW0gZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICAgIHNob3VsZENvbXBvbmVudFVwZGF0ZSguLi5hcmdzKSB7XG4gICAgICAgIHJldHVybiBQdXJlUmVuZGVyTWl4aW4uc2hvdWxkQ29tcG9uZW50VXBkYXRlLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIGNvbnN0IHsgcmVuZGVyZWRUZXh0LCByZW5kZXJlZEVsLCBpdGVtLCBsYXp5LCBjaGVja2VkLCBkaXNhYmxlZCwgcHJlZml4Q2xzLCBvbkNsaWNrLCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgY2xhc3NOYW1lID0gY2xhc3NOYW1lcyh7XG4gICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1jb250ZW50LWl0ZW1gXTogdHJ1ZSxcbiAgICAgICAgICAgIFtgJHtwcmVmaXhDbHN9LWNvbnRlbnQtaXRlbS1kaXNhYmxlZGBdOiBkaXNhYmxlZCB8fCBpdGVtLmRpc2FibGVkLFxuICAgICAgICB9KTtcbiAgICAgICAgbGV0IHRpdGxlO1xuICAgICAgICBpZiAodHlwZW9mIHJlbmRlcmVkVGV4dCA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIHJlbmRlcmVkVGV4dCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIHRpdGxlID0gU3RyaW5nKHJlbmRlcmVkVGV4dCk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgbGlzdEl0ZW0gPSAoPGxpIGNsYXNzTmFtZT17Y2xhc3NOYW1lfSB0aXRsZT17dGl0bGV9IG9uQ2xpY2s9e2Rpc2FibGVkIHx8IGl0ZW0uZGlzYWJsZWQgPyB1bmRlZmluZWQgOiAoKSA9PiBvbkNsaWNrKGl0ZW0pfT5cbiAgICAgICAgPENoZWNrYm94IGNoZWNrZWQ9e2NoZWNrZWR9IGRpc2FibGVkPXtkaXNhYmxlZCB8fCBpdGVtLmRpc2FibGVkfS8+XG4gICAgICAgIDxzcGFuIGNsYXNzTmFtZT17YCR7cHJlZml4Q2xzfS1jb250ZW50LWl0ZW0tdGV4dGB9PntyZW5kZXJlZEVsfTwvc3Bhbj5cbiAgICAgIDwvbGk+KTtcbiAgICAgICAgbGV0IGNoaWxkcmVuID0gbnVsbDtcbiAgICAgICAgaWYgKGxhenkpIHtcbiAgICAgICAgICAgIGNvbnN0IGxhenlQcm9wcyA9IE9iamVjdC5hc3NpZ24oeyBoZWlnaHQ6IDMyLCBvZmZzZXQ6IDUwMCwgdGhyb3R0bGU6IDAsIGRlYm91bmNlOiBmYWxzZSB9LCBsYXp5KTtcbiAgICAgICAgICAgIGNoaWxkcmVuID0gPExhenlsb2FkIHsuLi5sYXp5UHJvcHN9PntsaXN0SXRlbX08L0xhenlsb2FkPjtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNoaWxkcmVuID0gbGlzdEl0ZW07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGNoaWxkcmVuO1xuICAgIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7OztBQTNCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/transfer/ListItem.js
