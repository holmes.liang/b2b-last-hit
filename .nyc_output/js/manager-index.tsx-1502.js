__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! echarts/lib/chart/bar */ "./node_modules/echarts/lib/chart/bar.js");
/* harmony import */ var echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_bar__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! echarts/lib/chart/line */ "./node_modules/echarts/lib/chart/line.js");
/* harmony import */ var echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_line__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! echarts/lib/chart/pie */ "./node_modules/echarts/lib/chart/pie.js");
/* harmony import */ var echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_chart_pie__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! echarts/lib/component/legend */ "./node_modules/echarts/lib/component/legend.js");
/* harmony import */ var echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legend__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! echarts/lib/component/legendScroll */ "./node_modules/echarts/lib/component/legendScroll.js");
/* harmony import */ var echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_legendScroll__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! echarts/lib/component/title */ "./node_modules/echarts/lib/component/title.js");
/* harmony import */ var echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_title__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! echarts/lib/component/tooltip */ "./node_modules/echarts/lib/component/tooltip.js");
/* harmony import */ var echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_component_tooltip__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! echarts/lib/echarts */ "./node_modules/echarts/lib/echarts.js");
/* harmony import */ var echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var _manager_index_style__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./manager-index-style */ "./src/app/desk/home/manager-index-style.tsx");
/* harmony import */ var _index_common_style__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./index-common-style */ "./src/app/desk/home/index-common-style.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/home/manager-index.tsx";















var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();
var TabPane = antd__WEBPACK_IMPORTED_MODULE_9__["Tabs"].TabPane;
var RangePicker = antd__WEBPACK_IMPORTED_MODULE_9__["DatePicker"].RangePicker;

var tabListNoTitle = function tabListNoTitle() {
  return [{
    key: "premium",
    tab: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("PREMIUM").thai("เบี้ยประกัน").my("အာမခံပရီမီယံ").getMessage()
  }, {
    key: "policies",
    tab: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("POLICIES").thai("จำนวนกรมธรรม์").getMessage()
  }];
};

var concatReportId = {
  "premium-month": "9721",
  "premium-year": "9723",
  "policies-month": "9722",
  "policies-year": "9724",
  "policies-time": "9722",
  "premium-time": "9721"
};
var concatTrendReportId = {
  "premium-trend-month": "9794",
  "premium-trend-year": "9796",
  "policies-trend-month": "9795",
  "policies-trend-year": "9797",
  "policies-trend-time": "9799",
  "premium-trend-time": "9798"
};

var tabsA = function tabsA() {
  return [{
    key: "month",
    tab: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("This Month").thai("ดือนนี้").getMessage(),
    time: {
      from: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].nowWithTimeAtStartOfMonth(),
      to: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].nowWithTimeAtEndOfMonth()
    }
  }, {
    key: "year",
    tab: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("This Year").thai("ปีนี้").getMessage(),
    time: {
      from: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].nowWithTimeAtStartOfYear(),
      to: _common__WEBPACK_IMPORTED_MODULE_10__["DateUtils"].nowWithTimeAtEndOfYear()
    }
  }, {
    key: "time",
    tab: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Real-time").thai("Real-time").getMessage(),
    time: null
  }];
};

var gridTop = {
  left: "1px",
  bottom: "2px",
  top: "10px",
  right: "1px",
  containLabel: false
};
var gridBottom = {
  left: "10px",
  bottom: "10px",
  top: "10px",
  right: "45px",
  containLabel: true
};

var ManagerIndex =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(ManagerIndex, _ModelWidget);

  function ManagerIndex(props, context) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ManagerIndex);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ManagerIndex).call(this, props, context));
    _this.time = void 0;
    _this.salesTime = void 0;
    _this.annualisedTime = void 0;
    _this.policiesTime = void 0;
    _this.activeTime = void 0;
    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ManagerIndex, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var isInsurer = _common__WEBPACK_IMPORTED_MODULE_10__["Authority"].isInsurer(); // if (isInsurer) {

      ["9701", "9702", "9703", "9704", "9705", "9706", "9707", "9711", "9712", "9721", "9801", "9708", "9709", "9710", "9794"].forEach(function (every) {
        _this2.getSalesHistory(every);

        if (false) {}
      }); // }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this3 = this;

      ["time", "salesTime", "annualisedTime", "policiesTime", "activeTime"].forEach(function (every) {
        if (!!_this3[every]) {
          clearInterval(_this3[every]);
        }
      });
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return _manager_index_style__WEBPACK_IMPORTED_MODULE_20__["default"];
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(ManagerIndex.prototype), "initState", this).call(this), {
        activeKey: "month",
        annualisedReportData: false,
        annualisedPremium: {},
        policies: {},
        policiesReportData: false,
        newBusinessConversion: {},
        renewalRatio: {},
        activeUsers: {},
        sepActiveUsers: {},
        InActiveUsers: {},
        agentData: false,
        gwpData: false,
        tabKey: "premium",
        policyData: false,
        salesChampion: {},
        activeTab: tabsA()[0],
        renewalPremium: {},
        kpiOption: {},
        avgActive: {}
      });
    }
  }, {
    key: "getSalesHistory",
    value: function getSalesHistory(reportId, isLoading) {
      var _this4 = this;

      this.getHelpers().getAjax().get(encodeURI("".concat(_common__WEBPACK_IMPORTED_MODULE_10__["Apis"].REPORT.replace(":reportId", reportId), "?reportParams={}")), {}, {
        loading: !isLoading
      }).then(function (response) {
        var _ref = response.body || {
          respData: {}
        },
            respData = _ref.respData;

        var _ref2 = respData || {
          option: {}
        },
            option = _ref2.option;

        switch (reportId) {
          case "9701":
            _this4.setState({
              annualisedPremium: option
            });

            break;

          case "9702":
            _this4.getAnnualisedReportData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, {
              grid: gridTop
            }));

            break;

          case "9703":
            _this4.setState({
              renewalPremium: option
            });

            break;

          case "9704":
            _this4.setState({
              policies: option
            });

            break;

          case "9705":
            _this4.getPoliciesReportData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, {
              grid: gridTop
            }));

            break;

          case "9706":
            _this4.setState({
              newBusinessConversion: option
            });

            break;

          case "9707":
            _this4.setState({
              renewalRatio: option
            });

            break;

          case "9708":
            _this4.getAgentData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, {
              grid: gridTop
            }, {}, _this4.setLine(option, ["series"])));

            break;

          case "9709":
            _this4.setState({
              sepActiveUsers: option
            });

            break;

          case "9710":
            _this4.setState({
              InActiveUsers: option
            });

            break;

          case "9711":
            _this4.setState({
              kpiOption: option
            });

            break;

          case "9712":
            _this4.setState({
              avgActive: option
            });

            break;

          case "9801":
            _this4.setState({
              activeUsers: option
            });

          case "9794":
          case "9796":
          case "9798":
            _this4.getGWPData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, _this4.setLine(option, ["yAxis", "series"]), {}, {
              grid: gridBottom
            }));

            break;

          case "9795":
          case "9797":
          case "9799":
            _this4.getPolicyData(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, option, {}, _this4.setLine(option, ["yAxis", "series"]), {}, {
              grid: gridBottom
            }));

            break;

          case "9721":
          case "9722":
          case "9723":
          case "9724":
            _this4.setState({
              salesChampion: option
            });

            break;
        }
      }).catch(function () {
        _this4.getCatch(reportId);
      });
    } //9701

  }, {
    key: "getAnnualisedReportData",
    value: function getAnnualisedReportData(option) {
      this.setState({
        annualisedReportData: true
      });
      var el = document.getElementById("annualisedReportData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        ((option || {}).tooltip || {}).confine = true;
        myChart.setOption(option);
      }
    } //9705

  }, {
    key: "getPoliciesReportData",
    value: function getPoliciesReportData(option) {
      this.setState({
        policiesReportData: true
      });
      var el = document.getElementById("policiesReportData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        ((option || {}).tooltip || {}).confine = true;
        myChart.setOption(option);
      }
    } //9708

  }, {
    key: "getAgentData",
    value: function getAgentData(option) {
      this.setState({
        agentData: true
      });
      var el = document.getElementById("agentData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        myChart.setOption(option);
      }
    } //9798

  }, {
    key: "getGWPData",
    value: function getGWPData(option) {
      this.setState({
        gwpData: true
      });
      var el = document.getElementById("gwpData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        myChart.setOption(option);
      }
    } //9799

  }, {
    key: "getPolicyData",
    value: function getPolicyData(option) {
      this.setState({
        policyData: true
      });
      var el = document.getElementById("policyData");

      if (!!el) {
        var myChart = echarts_lib_echarts__WEBPACK_IMPORTED_MODULE_19___default.a.init(el);
        myChart.setOption(option);
      }
    }
  }, {
    key: "getCatch",
    value: function getCatch(reportId) {
      if (reportId === "9701") {
        this.setState({
          annualisedReportData: false
        });
      }

      if (reportId === "9705") {
        this.setState({
          policiesReportData: false
        });
      }

      if (reportId === "9708") {
        this.setState({
          agentData: false
        });
      }

      if (["9794", "9796", "9798"].includes(reportId)) {
        this.setState({
          gwpData: false
        });
      }

      if (["9795", "9797", "9799"].includes(reportId)) {
        this.setState({
          policyData: false
        });
      }
    }
  }, {
    key: "annualisedRenderTitle",
    value: function annualisedRenderTitle() {
      var _this$state = this.state,
          annualisedReportData = _this$state.annualisedReportData,
          annualisedPremium = _this$state.annualisedPremium;

      var premium = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "yoy", 0);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 371
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 372
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 373
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("PREMIUM").thai("ค่าใช้จ่ายเพิ่มเติม").my("အပိုဆောင်းကုန်ကျစရိတ်").getMessage(), " ", new Date().getFullYear()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: "Total premium since 1st of Jan this year",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 374
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "info-circle-o",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 377
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 380
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 381
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          fontSize: "16px",
          paddingRight: "5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 382
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "currencySymbol")), _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(annualisedPremium, "annualizedPremium", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 386
        },
        __self: this
      }, premium >= 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-up",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 387
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-down",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 387
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "percent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 388
        },
        __self: this
      }, Math.abs(parseInt(premium * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 389
        },
        __self: this
      }, "YOY"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          height: 60,
          marginTop: "20px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 392
        },
        __self: this
      }, annualisedReportData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "annualisedReportData",
        style: {
          width: "100%",
          height: 60
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 394
        },
        __self: this
      })));
    }
  }, {
    key: "policiesRenderTitle",
    value: function policiesRenderTitle() {
      var _this$state2 = this.state,
          policies = _this$state2.policies,
          policiesReportData = _this$state2.policiesReportData;

      var premium = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(policies, "yoy", 0);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 404
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 405
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 406
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("POLICIES").thai("จำนวนกรมธรรม์").getMessage(), " ", new Date().getFullYear()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: "Total No. of policies since 1st of Jan this year",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 407
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "info-circle-o",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 410
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 413
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 414
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(policies, "policies", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 415
        },
        __self: this
      }, premium >= 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-up",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 416
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-down",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 416
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "percent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 417
        },
        __self: this
      }, Math.abs(parseInt(premium * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 418
        },
        __self: this
      }, "YOY"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          height: 80,
          marginTop: "0px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 421
        },
        __self: this
      }, policiesReportData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "policiesReportData",
        style: {
          width: "100%",
          height: 80
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 423
        },
        __self: this
      })));
    }
  }, {
    key: "setLine",
    value: function setLine(option, filter) {
      if (!option) return;
      filter = filter || ["series", "xAxis", "yAxis"];
      var series = option.series,
          xAxis = option.xAxis,
          yAxis = option.yAxis;
      option.title = null;

      if (filter.includes("series")) {
        (series || []).map(function (every) {
          return Object.assign(every, {
            // color: [COLOR_PRIMARY], //折线颜色
            symbol: "none"
          });
        });
      }

      if (filter.includes("xAxis")) {
        return Object.assign(xAxis, {
          splitLine: {
            show: false
          }
        });
      }

      if (filter.includes("yAxis")) {
        return Object.assign(yAxis, {
          splitLine: {
            show: false
          }
        });
      }

      return option;
    }
  }, {
    key: "agentRenderTitle",
    value: function agentRenderTitle() {
      var _this$state3 = this.state,
          agentData = _this$state3.agentData,
          activeUsers = _this$state3.activeUsers,
          avgActive = _this$state3.avgActive;

      var average = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(avgActive, "avg", 0);

      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 463
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 464
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 465
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("ACTIVE AGENTS TODAY").thai("ตัวแทนที่ใช้งานอยู่ในปัจจุบัน").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: "No. of agents that have logged in system today\xA0",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 466
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "info-circle-o",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 469
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "money",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 472
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 473
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(activeUsers, "activeUsers", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "money-right",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, average >= 0 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-up",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 475
        },
        __self: this
      }) : _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "caret-down",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 475
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "percent",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 476
        },
        __self: this
      }, Math.abs(parseInt(average * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 477
        },
        __self: this
      }, "AVG"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        style: {
          height: 60,
          marginTop: "20px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 480
        },
        __self: this
      }, agentData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "agentData",
        style: {
          width: "100%",
          height: 60
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 482
        },
        __self: this
      })));
    }
  }, {
    key: "renderGWPData",
    value: function renderGWPData() {
      var gwpData = this.state.gwpData;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 492
        },
        __self: this
      }, gwpData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "gwpData",
        style: {
          width: "calc(100% - 10px)",
          height: isMobile ? 250 : 350,
          margin: "0 auto"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 494
        },
        __self: this
      }), !gwpData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "p-center",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("no data in statistics").thai("ไม่มีข้อมูลในสถิติ").getMessage()));
    }
  }, {
    key: "renderPolicyData",
    value: function renderPolicyData() {
      var policyData = this.state.policyData;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 507
        },
        __self: this
      }, policyData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        id: "policyData",
        style: {
          width: "calc(100% - 10px)",
          height: 350,
          margin: "0 auto"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 509
        },
        __self: this
      }), !policyData && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "p-center",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 511
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("no data in statistics").thai("ไม่มีข้อมูลในสถิติ").getMessage()));
    }
  }, {
    key: "changeTab",
    value: function changeTab() {
      var reportIdKey = "".concat(this.state.tabKey, "-").concat(this.state.activeKey);
      var reportIdTrendKey = "".concat(this.state.tabKey, "-trend-").concat(this.state.activeKey);

      if (Object.keys(concatReportId).includes(reportIdKey)) {
        this.getSalesHistory(concatReportId[reportIdKey]);
      }

      if (Object.keys(concatTrendReportId).includes(reportIdTrendKey)) {
        this.getSalesHistory(concatTrendReportId[reportIdTrendKey]);
      }
    }
  }, {
    key: "filterProgress",
    value: function filterProgress(realValue, filterValue) {
      if (realValue > 100) {
        return realValue + "%";
      } else {
        return filterValue + "%";
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      var C = this.getComponents();
      var _this$state4 = this.state,
          activeKey = _this$state4.activeKey,
          newBusinessConversion = _this$state4.newBusinessConversion,
          renewalRatio = _this$state4.renewalRatio,
          renewalPremium = _this$state4.renewalPremium,
          sepActiveUsers = _this$state4.sepActiveUsers,
          InActiveUsers = _this$state4.InActiveUsers,
          tabKey = _this$state4.tabKey,
          salesChampion = _this$state4.salesChampion,
          activeTab = _this$state4.activeTab,
          kpiOption = _this$state4.kpiOption;

      var conversionRatio = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(newBusinessConversion, "conversionRatio", 0);

      var renewal = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(renewalRatio, "renewalRatio", 0);

      var customerGrowth = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "customerGrowth", 0) * 100));
      var sales = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "sales", 0) * 100));
      var agentRecruiting = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "agentRecruiting", 0) * 100));
      var lossRatio = Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(kpiOption, "lossRatio", 0) * 100));
      return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(C.ManagerIndex, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 549
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: isMobile ? "mobile-agent-index" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 550
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        gutter: 16,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 551
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 552
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.annualisedRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 553
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 556
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 557
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Renewal Ratio").thai("อัตราส่วนการต่ออายุ").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 558
        },
        __self: this
      }, Math.abs(parseInt(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(renewalPremium, "renewalRatio", 0) * 100)), "%"), " ")))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 562
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.policiesRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 563
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 566
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 567
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Conversion").thai("การแปลง").my("ပြောင်းလဲမှု").getMessage(), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 568
        },
        __self: this
      }, "NB", _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 570
        },
        __self: this
      }, Math.abs(parseInt(conversionRatio * 100)), "%")), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
        style: {
          marginLeft: "10px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 572
        },
        __self: this
      }, "RN", _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 574
        },
        __self: this
      }, Math.abs(parseInt(renewal * 100)), "%"))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        className: "remark",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 577
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tooltip"], {
        title: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 580
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 581
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("New Biz Conversion").thai("การแปลงธุรกิจใหม่").my("နယူးစီးပွားရေးဆိုင်ရာကူးပြောင်းခြင်း").getMessage(), " ", Math.abs(parseInt(conversionRatio * 100)), "%"), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 582
          },
          __self: this
        }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Renewal Ratio").thai("อัตราส่วนการต่ออายุ").my("သက်တမ်းတိုးအချို").getMessage(), " ", Math.abs(parseInt(renewal * 100)), "%")),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 578
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Icon"], {
        type: "question-circle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 586
        },
        __self: this
      })))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 592
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        title: this.agentRenderTitle(),
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 593
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 596
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 597
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(sepActiveUsers, "activeUsers", 0), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          padding: "0 5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 599
        },
        __self: this
      }, "in"), _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getMonthEn(lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(sepActiveUsers, "currentMonth", 0))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 602
        },
        __self: this
      }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(InActiveUsers, "activeUsers", 0), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        style: {
          padding: "0 5px"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 604
        },
        __self: this
      }, "in"), lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(InActiveUsers, "currentYear", new Date().getFullYear()))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        className: "card-last",
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 610
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Card"], {
        bordered: false,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 611
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "card-title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 612
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("p", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 613
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 614
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("KPI ACHIEVEMENTS").thai("อัตราการเติบโตตัวแทนและลูกค้า").my("KPI အောင်မြင်မှုများ").getMessage(), " ", new Date().getFullYear())), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(_index_common_style__WEBPACK_IMPORTED_MODULE_21__["default"].KPI, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 616
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "kpi",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 617
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("ul", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 618
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 619
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 620
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Sales").thai("ขาย").my("အရောင်း").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 621
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        format: function format(pp) {
          return _this5.filterProgress(sales, pp);
        },
        strokeColor: "#65a172",
        status: "active",
        percent: sales,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 622
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 627
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 628
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Agent Recruiting").thai("Agent Recruiting").my("Agent Recruiting").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 629
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        format: function format(pp) {
          return _this5.filterProgress(agentRecruiting, pp);
        },
        strokeColor: "#65a172",
        status: "active",
        percent: agentRecruiting,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 630
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 635
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 636
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Customer Growth").thai("การเติบโตของลูกค้า").my("ဖောက်သည်ကြီးထွား").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 637
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        strokeColor: "#65a172",
        format: function format(pp) {
          return _this5.filterProgress(customerGrowth, pp);
        },
        status: "active",
        percent: customerGrowth,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 638
        },
        __self: this
      }))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 643
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 644
        },
        __self: this
      }, _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Loss Ratio").thai("อัตราการสูญเสีย").my("အရှုံးအချိုး").getMessage()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 645
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        strokeColor: "#ca7a68",
        status: "active",
        format: function format(pp) {
          return _this5.filterProgress(lossRatio, pp);
        },
        percent: lossRatio,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 646
        },
        __self: this
      })))))))))), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
        className: "gwp-issued ".concat(isMobile ? "mobile-gwp" : ""),
        style: {
          padding: "20px",
          marginTop: isMobile ? "0px" : "20px",
          backgroundColor: "#fff"
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 658
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Tabs"], {
        defaultActiveKey: "premium",
        onChange: function onChange(key) {
          _this5.setState({
            tabKey: key,
            activeKey: "month"
          }, function () {
            clearInterval(_this5.time);
            clearInterval(_this5.salesTime);

            _this5.changeTab();
          });
        },
        tabBarExtraContent: _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "tabs-right",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 673
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "tabs-a",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 674
          },
          __self: this
        }, tabsA().map(function (every, index) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("a", {
            onClick: function onClick() {
              _this5.setState({
                activeKey: every.key,
                activeTab: tabsA()[index]
              }, function () {
                if (every.key === "time") {
                  _this5.getSalesHistory(tabKey === "premium" ? "9798" : "9799");

                  _this5.getSalesHistory(tabKey === "premium" ? "9721" : "9722");

                  _this5.time = setInterval(function () {
                    _this5.getSalesHistory(tabKey === "premium" ? "9798" : "9799", true);
                  }, 3000);
                  _this5.salesTime = setInterval(function () {
                    _this5.getSalesHistory(tabKey === "premium" ? "9721" : "9722", true);
                  }, 3000);
                } else {
                  clearInterval(_this5.time);
                  clearInterval(_this5.salesTime);

                  _this5.changeTab();
                }
              });
            },
            className: activeKey === every.key ? "active" : "",
            key: every.key,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 677
            },
            __self: this
          }, every.tab);
        })), !isMobile && _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(RangePicker, {
          onChange: function onChange(value) {
            console.log(value);
          },
          disabled: true,
          value: [(activeTab.time || {}).from, (activeTab.time || {}).to],
          defaultValue: [(activeTab.time || {}).from, (activeTab.time || {}).to],
          format: _common__WEBPACK_IMPORTED_MODULE_10__["Consts"].DATE_FORMAT.DATE_FORMAT,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 701
          },
          __self: this
        })),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 660
        },
        __self: this
      }, tabListNoTitle().map(function (every) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement(TabPane, {
          tab: every.tab,
          key: every.key,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 715
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "tab ".concat(isMobile ? "mobile-tab" : ""),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 716
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "tab-left ".concat(isMobile ? "mobile-left" : ""),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 717
          },
          __self: this
        }, every.key === "premium" ? _this5.renderGWPData() : _this5.renderPolicyData()), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("div", {
          className: "tab-right ".concat(isMobile ? "mobile-right" : ""),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 720
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("h4", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 721
          },
          __self: this
        }, lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(salesChampion, "title")), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("ul", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 722
          },
          __self: this
        }, (lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(salesChampion, "topSales") || []).map(function (every, index) {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("li", {
            key: index,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 726
            },
            __self: this
          }, _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
            className: "i-span",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 727
            },
            __self: this
          }, index < 3 ? _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("img", {
            src: __webpack_require__("./src/assets/reports sync recursive ^\\.\\/.*\\.svg$")("./".concat(["one", "two", "three"][index], ".svg")),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 729
            },
            __self: this
          }) : index + 1), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("label", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 733
            },
            __self: this
          }, every.salesName), _common_3rd__WEBPACK_IMPORTED_MODULE_7__["React"].createElement("span", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 736
            },
            __self: this
          }, every.premium || every.policyIssued));
        })))));
      })))));
    }
  }]);

  return ManagerIndex;
}(_component__WEBPACK_IMPORTED_MODULE_8__["ModelWidget"]);

/* harmony default export */ __webpack_exports__["default"] = (ManagerIndex);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svaG9tZS9tYW5hZ2VyLWluZGV4LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2hvbWUvbWFuYWdlci1pbmRleC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IEFqYXhSZXNwb25zZSwgTW9kZWxXaWRnZXRQcm9wcyB9IGZyb20gXCJAbXktdHlwZXNcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcbmltcG9ydCB7IENhcmQsIENvbCwgRGF0ZVBpY2tlciwgSWNvbiwgUHJvZ3Jlc3MsIFJvdywgVGFicywgVG9vbHRpcCB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBBcGlzLCBBdXRob3JpdHksIENvbnN0cywgRGF0ZVV0aWxzLCBMYW5ndWFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY2hhcnQvYmFyXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jaGFydC9saW5lXCI7XG5pbXBvcnQgXCJlY2hhcnRzL2xpYi9jaGFydC9waWVcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmRcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC9sZWdlbmRTY3JvbGxcIjtcbmltcG9ydCBcImVjaGFydHMvbGliL2NvbXBvbmVudC90aXRsZVwiO1xuaW1wb3J0IFwiZWNoYXJ0cy9saWIvY29tcG9uZW50L3Rvb2x0aXBcIjtcbmltcG9ydCBFQ2hhcnRzIGZyb20gXCJlY2hhcnRzL2xpYi9lY2hhcnRzXCI7XG5pbXBvcnQgTWFuYWdlckluZGV4U3R5bGUgZnJvbSBcIi4vbWFuYWdlci1pbmRleC1zdHlsZVwiO1xuaW1wb3J0IEluZGV4Q29tbW9uU3R5bGUgZnJvbSBcIi4vaW5kZXgtY29tbW9uLXN0eWxlXCI7XG5cbmNvbnN0IGlzTW9iaWxlID0gVXRpbHMuZ2V0SXNNb2JpbGUoKTtcbmNvbnN0IHsgVGFiUGFuZSB9ID0gVGFicztcbmNvbnN0IHsgUmFuZ2VQaWNrZXIgfSA9IERhdGVQaWNrZXI7XG5jb25zdCB0YWJMaXN0Tm9UaXRsZTogYW55ID0gKCkgPT4ge1xuICByZXR1cm4gW1xuICAgIHtcbiAgICAgIGtleTogXCJwcmVtaXVtXCIsXG4gICAgICB0YWI6IExhbmd1YWdlLmVuKFwiUFJFTUlVTVwiKS50aGFpKFwi4LmA4Lia4Li14LmJ4Lii4Lib4Lij4Liw4LiB4Lix4LiZXCIpLm15KFwi4YCh4YCs4YCZ4YCB4YC24YCV4YCb4YCu4YCZ4YCu4YCa4YC2XCIpLmdldE1lc3NhZ2UoKSxcbiAgICB9LFxuICAgIHtcbiAgICAgIGtleTogXCJwb2xpY2llc1wiLFxuICAgICAgdGFiOiBMYW5ndWFnZS5lbihcIlBPTElDSUVTXCIpLnRoYWkoXCLguIjguLPguJnguKfguJnguIHguKPguKHguJjguKPguKPguKHguYxcIikuZ2V0TWVzc2FnZSgpLFxuICAgIH0sXG4gIF07XG59O1xuY29uc3QgY29uY2F0UmVwb3J0SWQ6IGFueSA9IHtcbiAgXCJwcmVtaXVtLW1vbnRoXCI6IFwiOTcyMVwiLFxuICBcInByZW1pdW0teWVhclwiOiBcIjk3MjNcIixcbiAgXCJwb2xpY2llcy1tb250aFwiOiBcIjk3MjJcIixcbiAgXCJwb2xpY2llcy15ZWFyXCI6IFwiOTcyNFwiLFxuICBcInBvbGljaWVzLXRpbWVcIjogXCI5NzIyXCIsXG4gIFwicHJlbWl1bS10aW1lXCI6IFwiOTcyMVwiLFxufTtcbmNvbnN0IGNvbmNhdFRyZW5kUmVwb3J0SWQ6IGFueSA9IHtcbiAgXCJwcmVtaXVtLXRyZW5kLW1vbnRoXCI6IFwiOTc5NFwiLFxuICBcInByZW1pdW0tdHJlbmQteWVhclwiOiBcIjk3OTZcIixcbiAgXCJwb2xpY2llcy10cmVuZC1tb250aFwiOiBcIjk3OTVcIixcbiAgXCJwb2xpY2llcy10cmVuZC15ZWFyXCI6IFwiOTc5N1wiLFxuICBcInBvbGljaWVzLXRyZW5kLXRpbWVcIjogXCI5Nzk5XCIsXG4gIFwicHJlbWl1bS10cmVuZC10aW1lXCI6IFwiOTc5OFwiLFxufTtcbmNvbnN0IHRhYnNBOiBhbnkgPSAoKSA9PiB7XG4gIHJldHVybiBbXG4gICAge1xuICAgICAga2V5OiBcIm1vbnRoXCIsIHRhYjogTGFuZ3VhZ2UuZW4oXCJUaGlzIE1vbnRoXCIpLnRoYWkoXCLguJTguLfguK3guJnguJnguLXguYlcIikuZ2V0TWVzc2FnZSgpLCB0aW1lOiB7XG4gICAgICAgIGZyb206IERhdGVVdGlscy5ub3dXaXRoVGltZUF0U3RhcnRPZk1vbnRoKCksXG4gICAgICAgIHRvOiBEYXRlVXRpbHMubm93V2l0aFRpbWVBdEVuZE9mTW9udGgoKSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICB7XG4gICAgICBrZXk6IFwieWVhclwiLCB0YWI6IExhbmd1YWdlLmVuKFwiVGhpcyBZZWFyXCIpLnRoYWkoXCLguJvguLXguJnguLXguYlcIikuZ2V0TWVzc2FnZSgpLCB0aW1lOiB7XG4gICAgICAgIGZyb206IERhdGVVdGlscy5ub3dXaXRoVGltZUF0U3RhcnRPZlllYXIoKSxcbiAgICAgICAgdG86IERhdGVVdGlscy5ub3dXaXRoVGltZUF0RW5kT2ZZZWFyKCksXG4gICAgICB9LFxuICAgIH0sXG4gICAgeyBrZXk6IFwidGltZVwiLCB0YWI6IExhbmd1YWdlLmVuKFwiUmVhbC10aW1lXCIpLnRoYWkoXCJSZWFsLXRpbWVcIikuZ2V0TWVzc2FnZSgpLCB0aW1lOiBudWxsIH0sXG4gIF07XG59O1xuY29uc3QgZ3JpZFRvcCA9IHtcbiAgbGVmdDogXCIxcHhcIixcbiAgYm90dG9tOiBcIjJweFwiLFxuICB0b3A6IFwiMTBweFwiLFxuICByaWdodDogXCIxcHhcIixcbiAgY29udGFpbkxhYmVsOiBmYWxzZSxcbn07XG5jb25zdCBncmlkQm90dG9tID0ge1xuICBsZWZ0OiBcIjEwcHhcIixcbiAgYm90dG9tOiBcIjEwcHhcIixcbiAgdG9wOiBcIjEwcHhcIixcbiAgcmlnaHQ6IFwiNDVweFwiLFxuICBjb250YWluTGFiZWw6IHRydWUsXG59O1xudHlwZSBNYW5hZ2VySW5kZXhQcm9wcyA9IHt9ICYgTW9kZWxXaWRnZXRQcm9wcztcblxudHlwZSBNYW5hZ2VySW5kZXhTdGF0ZSA9IHtcbiAgYWN0aXZlS2V5OiBzdHJpbmc7XG4gIGFubnVhbGlzZWRSZXBvcnREYXRhOiBib29sZWFuO1xuICBhbm51YWxpc2VkUHJlbWl1bTogYW55O1xuICBwb2xpY2llczogYW55O1xuICBwb2xpY2llc1JlcG9ydERhdGE6IGJvb2xlYW47XG4gIG5ld0J1c2luZXNzQ29udmVyc2lvbjogYW55O1xuICByZW5ld2FsUmF0aW86IGFueTtcbiAgYWN0aXZlVXNlcnM6IGFueTtcbiAgc2VwQWN0aXZlVXNlcnM6IGFueTtcbiAgSW5BY3RpdmVVc2VyczogYW55O1xuICBhZ2VudERhdGE6IGJvb2xlYW47XG4gIGd3cERhdGE6IGJvb2xlYW47XG4gIHRhYktleTogc3RyaW5nO1xuICBwb2xpY3lEYXRhOiBib29sZWFuO1xuICBzYWxlc0NoYW1waW9uOiBhbnk7XG4gIGFjdGl2ZVRhYjogYW55O1xuICByZW5ld2FsUHJlbWl1bTogYW55O1xuICBrcGlPcHRpb246IGFueTtcbiAgYXZnQWN0aXZlOiBhbnk7XG59XG50eXBlIFN0eWxlZERJViA9IFN0eWxlZEZ1bmN0aW9ucy5TdHlsZWRDb21wb25lbnQ8XCJkaXZcIiwgYW55LCB7fSwgbmV2ZXI+O1xuXG50eXBlIE1hbmFnZXJJbmRleENvbXBvbmVudHMgPSB7XG4gIE1hbmFnZXJJbmRleDogU3R5bGVkRElWLFxufVxuXG5jbGFzcyBNYW5hZ2VySW5kZXg8UCBleHRlbmRzIE1hbmFnZXJJbmRleFByb3BzLCBTIGV4dGVuZHMgTWFuYWdlckluZGV4U3RhdGUsIEMgZXh0ZW5kcyBNYW5hZ2VySW5kZXhDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgcHJvdGVjdGVkIHRpbWU6IGFueTtcbiAgcHJvdGVjdGVkIHNhbGVzVGltZTogYW55O1xuICBwcm90ZWN0ZWQgYW5udWFsaXNlZFRpbWU6IGFueTtcbiAgcHJvdGVjdGVkIHBvbGljaWVzVGltZTogYW55O1xuICBwcm90ZWN0ZWQgYWN0aXZlVGltZTogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzOiBNYW5hZ2VySW5kZXhQcm9wcywgY29udGV4dD86IGFueSkge1xuICAgIHN1cGVyKHByb3BzLCBjb250ZXh0KTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IGlzSW5zdXJlciA9IEF1dGhvcml0eS5pc0luc3VyZXIoKTtcbiAgICAvLyBpZiAoaXNJbnN1cmVyKSB7XG4gICAgW1wiOTcwMVwiLCBcIjk3MDJcIiwgXCI5NzAzXCIsIFwiOTcwNFwiLCBcIjk3MDVcIiwgXCI5NzA2XCIsIFwiOTcwN1wiLCBcIjk3MTFcIiwgXCI5NzEyXCIsIFwiOTcyMVwiLCBcIjk4MDFcIiwgXCI5NzA4XCIsIFwiOTcwOVwiLCBcIjk3MTBcIiwgXCI5Nzk0XCJdLmZvckVhY2goKGV2ZXJ5OiBhbnkpID0+IHtcbiAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KGV2ZXJ5KTtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5SRUFDVF9BUFBfQUpBWF9TRVJWRVJfSE9TVCA9PT0gXCJodHRwczovL2FwaS5kZW1vLmJ5dGVzZm9yY2UuY29tXCIpIHtcbiAgICAgICAgc3dpdGNoIChldmVyeSkge1xuICAgICAgICAgIGNhc2UgXCI5NzAyXCI6XG4gICAgICAgICAgICB0aGlzLmFubnVhbGlzZWRUaW1lID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmdldFNhbGVzSGlzdG9yeShcIjk3MDJcIiwgdHJ1ZSk7XG4gICAgICAgICAgICB9LCAzMDAwKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzA1XCI6XG4gICAgICAgICAgICB0aGlzLnBvbGljaWVzVGltZSA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5nZXRTYWxlc0hpc3RvcnkoXCI5NzA1XCIsIHRydWUpO1xuICAgICAgICAgICAgfSwgMzAwMCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTcwOFwiOlxuICAgICAgICAgICAgdGhpcy5hY3RpdmVUaW1lID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLmdldFNhbGVzSGlzdG9yeShcIjk3MDhcIiwgdHJ1ZSk7XG4gICAgICAgICAgICB9LCAzMDAwKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gICAgLy8gfVxuICB9XG5cbiAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgW1widGltZVwiLCBcInNhbGVzVGltZVwiLCBcImFubnVhbGlzZWRUaW1lXCIsIFwicG9saWNpZXNUaW1lXCIsIFwiYWN0aXZlVGltZVwiXS5mb3JFYWNoKChldmVyeTogYW55KSA9PiB7XG4gICAgICBpZiAoISEoKHRoaXMgYXMgYW55KVtldmVyeV0gYXMgYW55KSkge1xuICAgICAgICBjbGVhckludGVydmFsKCh0aGlzIGFzIGFueSlbZXZlcnldIGFzIGFueSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdENvbXBvbmVudHMoKTogQyB7XG4gICAgcmV0dXJuIE1hbmFnZXJJbmRleFN0eWxlIGFzIEM7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBhY3RpdmVLZXk6IFwibW9udGhcIixcbiAgICAgIGFubnVhbGlzZWRSZXBvcnREYXRhOiBmYWxzZSxcbiAgICAgIGFubnVhbGlzZWRQcmVtaXVtOiB7fSxcbiAgICAgIHBvbGljaWVzOiB7fSxcbiAgICAgIHBvbGljaWVzUmVwb3J0RGF0YTogZmFsc2UsXG4gICAgICBuZXdCdXNpbmVzc0NvbnZlcnNpb246IHt9LFxuICAgICAgcmVuZXdhbFJhdGlvOiB7fSxcbiAgICAgIGFjdGl2ZVVzZXJzOiB7fSxcbiAgICAgIHNlcEFjdGl2ZVVzZXJzOiB7fSxcbiAgICAgIEluQWN0aXZlVXNlcnM6IHt9LFxuICAgICAgYWdlbnREYXRhOiBmYWxzZSxcbiAgICAgIGd3cERhdGE6IGZhbHNlLFxuICAgICAgdGFiS2V5OiBcInByZW1pdW1cIixcbiAgICAgIHBvbGljeURhdGE6IGZhbHNlLFxuICAgICAgc2FsZXNDaGFtcGlvbjoge30sXG4gICAgICBhY3RpdmVUYWI6IHRhYnNBKClbMF0sXG4gICAgICByZW5ld2FsUHJlbWl1bToge30sXG4gICAgICBrcGlPcHRpb246IHt9LFxuICAgICAgYXZnQWN0aXZlOiB7fSxcbiAgICB9KSBhcyBTO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRTYWxlc0hpc3RvcnkocmVwb3J0SWQ6IHN0cmluZywgaXNMb2FkaW5nPzogYm9vbGVhbikge1xuICAgIHRoaXMuZ2V0SGVscGVycygpXG4gICAgICAuZ2V0QWpheCgpLmdldChlbmNvZGVVUkkoYCR7QXBpcy5SRVBPUlQucmVwbGFjZShcIjpyZXBvcnRJZFwiLCByZXBvcnRJZCl9P3JlcG9ydFBhcmFtcz17fWApLCB7fSwge1xuICAgICAgbG9hZGluZzogIWlzTG9hZGluZyxcbiAgICB9KVxuICAgICAgLnRoZW4oKHJlc3BvbnNlOiBBamF4UmVzcG9uc2UpID0+IHtcbiAgICAgICAgY29uc3QgeyByZXNwRGF0YSB9ID0gcmVzcG9uc2UuYm9keSB8fCB7IHJlc3BEYXRhOiB7fSB9O1xuICAgICAgICBjb25zdCB7IG9wdGlvbiB9ID0gcmVzcERhdGEgfHwgeyBvcHRpb246IHt9IH07XG4gICAgICAgIHN3aXRjaCAocmVwb3J0SWQpIHtcbiAgICAgICAgICBjYXNlIFwiOTcwMVwiOlxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgIGFubnVhbGlzZWRQcmVtaXVtOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzAyXCI6XG4gICAgICAgICAgICB0aGlzLmdldEFubnVhbGlzZWRSZXBvcnREYXRhKHsgLi4ub3B0aW9uLCAuLi57IGdyaWQ6IGdyaWRUb3AgfSB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzAzXCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgcmVuZXdhbFByZW1pdW06IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3MDRcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBwb2xpY2llczogb3B0aW9uLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTcwNVwiOlxuICAgICAgICAgICAgdGhpcy5nZXRQb2xpY2llc1JlcG9ydERhdGEoeyAuLi5vcHRpb24sIC4uLnsgZ3JpZDogZ3JpZFRvcCB9IH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3MDZcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBuZXdCdXNpbmVzc0NvbnZlcnNpb246IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3MDdcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICByZW5ld2FsUmF0aW86IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3MDhcIjpcbiAgICAgICAgICAgIHRoaXMuZ2V0QWdlbnREYXRhKHsgLi4ub3B0aW9uLCAuLi57IGdyaWQ6IGdyaWRUb3AgfSwgLi4udGhpcy5zZXRMaW5lKG9wdGlvbiwgW1wic2VyaWVzXCJdKSB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzA5XCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgc2VwQWN0aXZlVXNlcnM6IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3MTBcIjpcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICBJbkFjdGl2ZVVzZXJzOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzExXCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAga3BpT3B0aW9uOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5NzEyXCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgYXZnQWN0aXZlOiBvcHRpb24sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgXCI5ODAxXCI6XG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgYWN0aXZlVXNlcnM6IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIGNhc2UgXCI5Nzk0XCI6XG4gICAgICAgICAgY2FzZSBcIjk3OTZcIjpcbiAgICAgICAgICBjYXNlIFwiOTc5OFwiOlxuICAgICAgICAgICAgdGhpcy5nZXRHV1BEYXRhKHsgLi4ub3B0aW9uLCAuLi50aGlzLnNldExpbmUob3B0aW9uLCBbXCJ5QXhpc1wiLCBcInNlcmllc1wiXSksIC4uLnsgZ3JpZDogZ3JpZEJvdHRvbSB9IH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcIjk3OTVcIjpcbiAgICAgICAgICBjYXNlIFwiOTc5N1wiOlxuICAgICAgICAgIGNhc2UgXCI5Nzk5XCI6XG4gICAgICAgICAgICB0aGlzLmdldFBvbGljeURhdGEoeyAuLi5vcHRpb24sIC4uLnRoaXMuc2V0TGluZShvcHRpb24sIFtcInlBeGlzXCIsIFwic2VyaWVzXCJdKSwgLi4ueyBncmlkOiBncmlkQm90dG9tIH0gfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBjYXNlIFwiOTcyMVwiOlxuICAgICAgICAgIGNhc2UgXCI5NzIyXCI6XG4gICAgICAgICAgY2FzZSBcIjk3MjNcIjpcbiAgICAgICAgICBjYXNlIFwiOTcyNFwiOlxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgIHNhbGVzQ2hhbXBpb246IG9wdGlvbixcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH0pLmNhdGNoKCgpID0+IHtcbiAgICAgIHRoaXMuZ2V0Q2F0Y2gocmVwb3J0SWQpO1xuICAgIH0pO1xuICB9XG5cbiAgLy85NzAxXG4gIHByaXZhdGUgZ2V0QW5udWFsaXNlZFJlcG9ydERhdGEob3B0aW9uOiBhbnkpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGFubnVhbGlzZWRSZXBvcnREYXRhOiB0cnVlLFxuICAgIH0pO1xuICAgIGxldCBlbDogYW55ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhbm51YWxpc2VkUmVwb3J0RGF0YVwiKTtcbiAgICBpZiAoISFlbCkge1xuICAgICAgY29uc3QgbXlDaGFydCA9IEVDaGFydHMuaW5pdChlbCk7XG4gICAgICAoKG9wdGlvbiB8fCB7fSkudG9vbHRpcCB8fCB7fSkuY29uZmluZSA9IHRydWU7XG4gICAgICBteUNoYXJ0LnNldE9wdGlvbihvcHRpb24pO1xuICAgIH1cbiAgfVxuXG4gIC8vOTcwNVxuICBnZXRQb2xpY2llc1JlcG9ydERhdGEob3B0aW9uOiBhbnkpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIHBvbGljaWVzUmVwb3J0RGF0YTogdHJ1ZSxcbiAgICB9KTtcbiAgICBsZXQgZWw6IGFueSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicG9saWNpZXNSZXBvcnREYXRhXCIpO1xuICAgIGlmICghIWVsKSB7XG4gICAgICBjb25zdCBteUNoYXJ0ID0gRUNoYXJ0cy5pbml0KGVsKTtcbiAgICAgICgob3B0aW9uIHx8IHt9KS50b29sdGlwIHx8IHt9KS5jb25maW5lID0gdHJ1ZTtcbiAgICAgIG15Q2hhcnQuc2V0T3B0aW9uKG9wdGlvbik7XG4gICAgfVxuICB9XG5cbiAgLy85NzA4XG4gIGdldEFnZW50RGF0YShvcHRpb246IGFueSkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgYWdlbnREYXRhOiB0cnVlLFxuICAgIH0pO1xuICAgIGxldCBlbDogYW55ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhZ2VudERhdGFcIik7XG4gICAgaWYgKCEhZWwpIHtcbiAgICAgIGNvbnN0IG15Q2hhcnQgPSBFQ2hhcnRzLmluaXQoZWwpO1xuICAgICAgbXlDaGFydC5zZXRPcHRpb24ob3B0aW9uKTtcbiAgICB9XG4gIH1cblxuICAvLzk3OThcbiAgZ2V0R1dQRGF0YShvcHRpb246IGFueSkge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgZ3dwRGF0YTogdHJ1ZSxcbiAgICB9KTtcbiAgICBsZXQgZWw6IGFueSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZ3dwRGF0YVwiKTtcbiAgICBpZiAoISFlbCkge1xuICAgICAgY29uc3QgbXlDaGFydCA9IEVDaGFydHMuaW5pdChlbCk7XG4gICAgICBteUNoYXJ0LnNldE9wdGlvbihvcHRpb24pO1xuICAgIH1cbiAgfVxuXG4gIC8vOTc5OVxuICBnZXRQb2xpY3lEYXRhKG9wdGlvbjogYW55KSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBwb2xpY3lEYXRhOiB0cnVlLFxuICAgIH0pO1xuICAgIGxldCBlbDogYW55ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwb2xpY3lEYXRhXCIpO1xuICAgIGlmICghIWVsKSB7XG4gICAgICBjb25zdCBteUNoYXJ0ID0gRUNoYXJ0cy5pbml0KGVsKTtcbiAgICAgIG15Q2hhcnQuc2V0T3B0aW9uKG9wdGlvbik7XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBnZXRDYXRjaChyZXBvcnRJZDogc3RyaW5nKSB7XG4gICAgaWYgKHJlcG9ydElkID09PSBcIjk3MDFcIikge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGFubnVhbGlzZWRSZXBvcnREYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAocmVwb3J0SWQgPT09IFwiOTcwNVwiKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgcG9saWNpZXNSZXBvcnREYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAocmVwb3J0SWQgPT09IFwiOTcwOFwiKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgYWdlbnREYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChbXCI5Nzk0XCIsIFwiOTc5NlwiLCBcIjk3OThcIl0uaW5jbHVkZXMocmVwb3J0SWQpKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZ3dwRGF0YTogZmFsc2UsXG4gICAgICB9KTtcbiAgICB9XG4gICAgaWYgKFtcIjk3OTVcIiwgXCI5Nzk3XCIsIFwiOTc5OVwiXS5pbmNsdWRlcyhyZXBvcnRJZCkpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBwb2xpY3lEYXRhOiBmYWxzZSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYW5udWFsaXNlZFJlbmRlclRpdGxlKCkge1xuICAgIGNvbnN0IHsgYW5udWFsaXNlZFJlcG9ydERhdGEsIGFubnVhbGlzZWRQcmVtaXVtIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHByZW1pdW0gPSBfLmdldChhbm51YWxpc2VkUHJlbWl1bSwgXCJ5b3lcIiwgMCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC10aXRsZVwiPlxuICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIlBSRU1JVU1cIikudGhhaShcIuC4hOC5iOC4suC5g+C4iuC5ieC4iOC5iOC4suC4ouC5gOC4nuC4tOC5iOC4oeC5gOC4leC4tOC4oVwiKS5teShcIuGAoeGAleGAreGAr+GAhuGAseGArOGAhOGAuuGAuOGAgOGAr+GAlOGAuuGAgOGAu+GAheGAm+GAreGAkOGAulwiKS5nZXRNZXNzYWdlKCl9IHtuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCl9PC9zcGFuPlxuICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICB0aXRsZT1cIlRvdGFsIHByZW1pdW0gc2luY2UgMXN0IG9mIEphbiB0aGlzIHllYXJcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxJY29uIHR5cGU9XCJpbmZvLWNpcmNsZS1vXCIvPlxuICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgPC9wPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1vbmV5XCI+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibW9uZXktbGVmdFwiPlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjE2cHhcIiwgcGFkZGluZ1JpZ2h0OiBcIjVweFwiIH19PntfLmdldChhbm51YWxpc2VkUHJlbWl1bSwgXCJjdXJyZW5jeVN5bWJvbFwiKX08L3NwYW4+XG4gICAgICAgICAgICB7VXRpbHMudG9UaG91c2FuZHMoXy5nZXQoYW5udWFsaXNlZFByZW1pdW0sIFwiYW5udWFsaXplZFByZW1pdW1cIiwgMCkpfVxuICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJtb25leS1yaWdodFwiPlxuICAgICAgICAgICAge3ByZW1pdW0gPj0gMCA/IDxJY29uIHR5cGU9XCJjYXJldC11cFwiLz4gOiA8SWNvbiB0eXBlPVwiY2FyZXQtZG93blwiLz59XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwZXJjZW50XCI+e01hdGguYWJzKHBhcnNlSW50KChwcmVtaXVtICogMTAwKSBhcyBhbnkpKX0lPC9zcGFuPlxuICAgICAgICAgICAgPGxhYmVsPllPWTwvbGFiZWw+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDYwLCBtYXJnaW5Ub3A6IFwiMjBweFwiIH19PlxuICAgICAgICAgIHthbm51YWxpc2VkUmVwb3J0RGF0YSAmJlxuICAgICAgICAgIDxkaXYgaWQ9XCJhbm51YWxpc2VkUmVwb3J0RGF0YVwiIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiwgaGVpZ2h0OiA2MCB9fS8+fVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHBvbGljaWVzUmVuZGVyVGl0bGUoKSB7XG4gICAgY29uc3QgeyBwb2xpY2llcywgcG9saWNpZXNSZXBvcnREYXRhIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHByZW1pdW0gPSBfLmdldChwb2xpY2llcywgXCJ5b3lcIiwgMCk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC10aXRsZVwiPlxuICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIlBPTElDSUVTXCIpLnRoYWkoXCLguIjguLPguJnguKfguJnguIHguKPguKHguJjguKPguKPguKHguYxcIikuZ2V0TWVzc2FnZSgpfSB7bmV3IERhdGUoKS5nZXRGdWxsWWVhcigpfTwvc3Bhbj5cbiAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgdGl0bGU9XCJUb3RhbCBOby4gb2YgcG9saWNpZXMgc2luY2UgMXN0IG9mIEphbiB0aGlzIHllYXJcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxJY29uIHR5cGU9XCJpbmZvLWNpcmNsZS1vXCIvPlxuICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgPC9wPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1vbmV5XCI+XG4gICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwibW9uZXktbGVmdFwiPntVdGlscy50b1Rob3VzYW5kcyhfLmdldChwb2xpY2llcywgXCJwb2xpY2llc1wiLCAwKSl9PC9zcGFuPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm1vbmV5LXJpZ2h0XCI+XG4gICAgICAgICAgICB7cHJlbWl1bSA+PSAwID8gPEljb24gdHlwZT1cImNhcmV0LXVwXCIvPiA6IDxJY29uIHR5cGU9XCJjYXJldC1kb3duXCIvPn1cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInBlcmNlbnRcIj57TWF0aC5hYnMocGFyc2VJbnQoKHByZW1pdW0gKiAxMDApIGFzIGFueSkpfSU8L3NwYW4+XG4gICAgICAgICAgICA8bGFiZWw+WU9ZPC9sYWJlbD5cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXt7IGhlaWdodDogODAsIG1hcmdpblRvcDogXCIwcHhcIiB9fT5cbiAgICAgICAgICB7cG9saWNpZXNSZXBvcnREYXRhICYmXG4gICAgICAgICAgPGRpdiBpZD1cInBvbGljaWVzUmVwb3J0RGF0YVwiIHN0eWxlPXt7IHdpZHRoOiBcIjEwMCVcIiwgaGVpZ2h0OiA4MCB9fS8+fVxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHNldExpbmUob3B0aW9uOiBhbnksIGZpbHRlcj86IGFueVtdKSB7XG4gICAgaWYgKCFvcHRpb24pIHJldHVybjtcbiAgICBmaWx0ZXIgPSBmaWx0ZXIgfHwgW1wic2VyaWVzXCIsIFwieEF4aXNcIiwgXCJ5QXhpc1wiXTtcbiAgICBsZXQgeyBzZXJpZXMsIHhBeGlzLCB5QXhpcyB9ID0gb3B0aW9uO1xuICAgIG9wdGlvbi50aXRsZSA9IG51bGw7XG4gICAgaWYgKGZpbHRlci5pbmNsdWRlcyhcInNlcmllc1wiKSkge1xuICAgICAgKHNlcmllcyB8fCBbXSkubWFwKChldmVyeTogYW55KSA9PiB7XG4gICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKGV2ZXJ5LCB7XG4gICAgICAgICAgLy8gY29sb3I6IFtDT0xPUl9QUklNQVJZXSwgLy/mipjnur/popzoibJcbiAgICAgICAgICBzeW1ib2w6IFwibm9uZVwiLFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoZmlsdGVyLmluY2x1ZGVzKFwieEF4aXNcIikpIHtcbiAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHhBeGlzLCB7XG4gICAgICAgIHNwbGl0TGluZToge1xuICAgICAgICAgIHNob3c6IGZhbHNlLFxuICAgICAgICB9LFxuICAgICAgfSk7XG4gICAgfVxuICAgIGlmIChmaWx0ZXIuaW5jbHVkZXMoXCJ5QXhpc1wiKSkge1xuICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oeUF4aXMsIHtcbiAgICAgICAgc3BsaXRMaW5lOiB7XG4gICAgICAgICAgc2hvdzogZmFsc2UsXG4gICAgICAgIH0sXG4gICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIG9wdGlvbjtcbiAgfVxuXG4gIHByaXZhdGUgYWdlbnRSZW5kZXJUaXRsZSgpIHtcbiAgICBjb25zdCB7IGFnZW50RGF0YSwgYWN0aXZlVXNlcnMsIGF2Z0FjdGl2ZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBhdmVyYWdlID0gXy5nZXQoYXZnQWN0aXZlLCBcImF2Z1wiLCAwKTtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLXRpdGxlXCI+XG4gICAgICAgIDxwIGNsYXNzTmFtZT1cInRpdGxlXCI+XG4gICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiQUNUSVZFIEFHRU5UUyBUT0RBWVwiKS50aGFpKFwi4LiV4Lix4Lin4LmB4LiX4LiZ4LiX4Li14LmI4LmD4LiK4LmJ4LiH4Liy4LiZ4Lit4Lii4Li54LmI4LmD4LiZ4Lib4Lix4LiI4LiI4Li44Lia4Lix4LiZXCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgIHRpdGxlPVwiTm8uIG9mIGFnZW50cyB0aGF0IGhhdmUgbG9nZ2VkIGluIHN5c3RlbSB0b2RhecKgXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8SWNvbiB0eXBlPVwiaW5mby1jaXJjbGUtb1wiLz5cbiAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgIDwvcD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb25leVwiPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm1vbmV5LWxlZnRcIj57VXRpbHMudG9UaG91c2FuZHMoXy5nZXQoYWN0aXZlVXNlcnMsIFwiYWN0aXZlVXNlcnNcIiwgMCkpfTwvc3Bhbj5cbiAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJtb25leS1yaWdodFwiPlxuICAgICAgICAgICAge2F2ZXJhZ2UgPj0gMCA/IDxJY29uIHR5cGU9XCJjYXJldC11cFwiLz4gOiA8SWNvbiB0eXBlPVwiY2FyZXQtZG93blwiLz59XG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJwZXJjZW50XCI+e01hdGguYWJzKHBhcnNlSW50KChhdmVyYWdlICogMTAwKSBhcyBhbnkpKX0lPC9zcGFuPlxuICAgICAgICAgICAgPGxhYmVsPkFWRzwvbGFiZWw+XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDYwLCBtYXJnaW5Ub3A6IFwiMjBweFwiIH19PlxuICAgICAgICAgIHthZ2VudERhdGEgJiZcbiAgICAgICAgICA8ZGl2IGlkPVwiYWdlbnREYXRhXCIgc3R5bGU9e3sgd2lkdGg6IFwiMTAwJVwiLCBoZWlnaHQ6IDYwIH19Lz59XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVuZGVyR1dQRGF0YSgpIHtcbiAgICBjb25zdCB7IGd3cERhdGEgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAge2d3cERhdGEgJiZcbiAgICAgICAgPGRpdiBpZD1cImd3cERhdGFcIiBzdHlsZT17eyB3aWR0aDogXCJjYWxjKDEwMCUgLSAxMHB4KVwiLCBoZWlnaHQ6IGlzTW9iaWxlID8gMjUwIDogMzUwLCBtYXJnaW46IFwiMCBhdXRvXCIgfX0vPn1cbiAgICAgICAgeyFnd3BEYXRhICYmIChcbiAgICAgICAgICA8cCBjbGFzc05hbWU9XCJwLWNlbnRlclwiPlxuICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwibm8gZGF0YSBpbiBzdGF0aXN0aWNzXCIpLnRoYWkoXCLguYTguKHguYjguKHguLXguILguYnguK3guKHguLnguKXguYPguJnguKrguJbguLTguJXguLRcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIHJlbmRlclBvbGljeURhdGEoKSB7XG4gICAgY29uc3QgeyBwb2xpY3lEYXRhIH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICB7cG9saWN5RGF0YSAmJlxuICAgICAgICA8ZGl2IGlkPVwicG9saWN5RGF0YVwiIHN0eWxlPXt7IHdpZHRoOiBcImNhbGMoMTAwJSAtIDEwcHgpXCIsIGhlaWdodDogMzUwLCBtYXJnaW46IFwiMCBhdXRvXCIgfX0vPn1cbiAgICAgICAgeyFwb2xpY3lEYXRhICYmIChcbiAgICAgICAgICA8cCBjbGFzc05hbWU9XCJwLWNlbnRlclwiPlxuICAgICAgICAgICAge0xhbmd1YWdlLmVuKFwibm8gZGF0YSBpbiBzdGF0aXN0aWNzXCIpLnRoYWkoXCLguYTguKHguYjguKHguLXguILguYnguK3guKHguLnguKXguYPguJnguKrguJbguLTguJXguLRcIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgIDwvcD5cbiAgICAgICAgKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cblxuICBwcml2YXRlIGNoYW5nZVRhYigpIHtcbiAgICBsZXQgcmVwb3J0SWRLZXk6IGFueSA9IGAke3RoaXMuc3RhdGUudGFiS2V5fS0ke3RoaXMuc3RhdGUuYWN0aXZlS2V5fWA7XG4gICAgbGV0IHJlcG9ydElkVHJlbmRLZXk6IGFueSA9IGAke3RoaXMuc3RhdGUudGFiS2V5fS10cmVuZC0ke3RoaXMuc3RhdGUuYWN0aXZlS2V5fWA7XG4gICAgaWYgKE9iamVjdC5rZXlzKGNvbmNhdFJlcG9ydElkKS5pbmNsdWRlcyhyZXBvcnRJZEtleSkpIHtcbiAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KGNvbmNhdFJlcG9ydElkW3JlcG9ydElkS2V5XSk7XG4gICAgfVxuICAgIGlmIChPYmplY3Qua2V5cyhjb25jYXRUcmVuZFJlcG9ydElkKS5pbmNsdWRlcyhyZXBvcnRJZFRyZW5kS2V5KSkge1xuICAgICAgdGhpcy5nZXRTYWxlc0hpc3RvcnkoY29uY2F0VHJlbmRSZXBvcnRJZFtyZXBvcnRJZFRyZW5kS2V5XSk7XG4gICAgfVxuICB9XG5cbiAgcHJvdGVjdGVkIGZpbHRlclByb2dyZXNzKHJlYWxWYWx1ZTogbnVtYmVyLCBmaWx0ZXJWYWx1ZTogYW55KSB7XG4gICAgaWYgKHJlYWxWYWx1ZSA+IDEwMCkge1xuICAgICAgcmV0dXJuIHJlYWxWYWx1ZSArIFwiJVwiO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZmlsdGVyVmFsdWUgKyBcIiVcIjtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgQyA9IHRoaXMuZ2V0Q29tcG9uZW50cygpO1xuXG4gICAgY29uc3QgeyBhY3RpdmVLZXksIG5ld0J1c2luZXNzQ29udmVyc2lvbiwgcmVuZXdhbFJhdGlvLCByZW5ld2FsUHJlbWl1bSwgc2VwQWN0aXZlVXNlcnMsIEluQWN0aXZlVXNlcnMsIHRhYktleSwgc2FsZXNDaGFtcGlvbiwgYWN0aXZlVGFiLCBrcGlPcHRpb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgbGV0IGNvbnZlcnNpb25SYXRpbzogYW55ID0gXy5nZXQobmV3QnVzaW5lc3NDb252ZXJzaW9uLCBcImNvbnZlcnNpb25SYXRpb1wiLCAwKTtcbiAgICBsZXQgcmVuZXdhbDogYW55ID0gXy5nZXQocmVuZXdhbFJhdGlvLCBcInJlbmV3YWxSYXRpb1wiLCAwKTtcbiAgICBsZXQgY3VzdG9tZXJHcm93dGggPSBNYXRoLmFicyhwYXJzZUludCgoXy5nZXQoa3BpT3B0aW9uLCBcImN1c3RvbWVyR3Jvd3RoXCIsIDApICogMTAwKSBhcyBhbnkpKTtcbiAgICBsZXQgc2FsZXMgPSBNYXRoLmFicyhwYXJzZUludCgoXy5nZXQoa3BpT3B0aW9uLCBcInNhbGVzXCIsIDApICogMTAwKSBhcyBhbnkpKTtcbiAgICBsZXQgYWdlbnRSZWNydWl0aW5nID0gTWF0aC5hYnMocGFyc2VJbnQoKF8uZ2V0KGtwaU9wdGlvbiwgXCJhZ2VudFJlY3J1aXRpbmdcIiwgMCkgKiAxMDApIGFzIGFueSkpO1xuICAgIGxldCBsb3NzUmF0aW8gPSBNYXRoLmFicyhwYXJzZUludCgoXy5nZXQoa3BpT3B0aW9uLCBcImxvc3NSYXRpb1wiLCAwKSAqIDEwMCkgYXMgYW55KSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxDLk1hbmFnZXJJbmRleD5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2lzTW9iaWxlID8gXCJtb2JpbGUtYWdlbnQtaW5kZXhcIiA6IFwiXCJ9PlxuICAgICAgICAgIDxSb3cgZ3V0dGVyPXsxNn0+XG4gICAgICAgICAgICA8Q29sIHNwYW49ezZ9PlxuICAgICAgICAgICAgICA8Q2FyZCB0aXRsZT17XG4gICAgICAgICAgICAgICAgdGhpcy5hbm51YWxpc2VkUmVuZGVyVGl0bGUoKVxuICAgICAgICAgICAgICB9IGJvcmRlcmVkPXtmYWxzZX0+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWJvZHlcIj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIlJlbmV3YWwgUmF0aW9cIikudGhhaShcIuC4reC4seC4leC4o+C4suC4quC5iOC4p+C4meC4geC4suC4o+C4leC5iOC4reC4reC4suC4ouC4uFwiKS5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbD57TWF0aC5hYnMocGFyc2VJbnQoKF8uZ2V0KHJlbmV3YWxQcmVtaXVtLCBcInJlbmV3YWxSYXRpb1wiLCAwKSAqIDEwMCkgYXMgYW55KSl9JTwvbGFiZWw+IDwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8Q29sIHNwYW49ezZ9PlxuICAgICAgICAgICAgICA8Q2FyZCB0aXRsZT17XG4gICAgICAgICAgICAgICAgdGhpcy5wb2xpY2llc1JlbmRlclRpdGxlKClcbiAgICAgICAgICAgICAgfSBib3JkZXJlZD17ZmFsc2V9PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2FyZC1ib2R5XCI+XG4gICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJDb252ZXJzaW9uXCIpLnRoYWkoXCLguIHguLLguKPguYHguJvguKXguIdcIikubXkoXCLhgJXhgLzhgLHhgKzhgIThgLrhgLjhgJzhgLLhgJnhgL7hgK9cIikuZ2V0TWVzc2FnZSgpfVxuICAgICAgICAgICAgICAgICAgICA8bGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgTkJcbiAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TWF0aC5hYnMocGFyc2VJbnQoKGNvbnZlcnNpb25SYXRpbyAqIDEwMCkgYXMgYW55KSl9JTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIHN0eWxlPXt7IG1hcmdpbkxlZnQ6IFwiMTBweFwiIH19PlxuICAgICAgICAgICAgICAgICAgICAgIFJOXG4gICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e01hdGguYWJzKHBhcnNlSW50KChyZW5ld2FsICogMTAwKSBhcyBhbnkpKX0lPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwicmVtYXJrXCI+XG4gICAgICAgICAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+e0xhbmd1YWdlLmVuKFwiTmV3IEJpeiBDb252ZXJzaW9uXCIpLnRoYWkoXCLguIHguLLguKPguYHguJvguKXguIfguJjguLjguKPguIHguLTguIjguYPguKvguKHguYhcIikubXkoXCLhgJThgJrhgLDhgLjhgIXhgK7hgLjhgJXhgL3hgKzhgLjhgJvhgLHhgLjhgIbhgK3hgK/hgIThgLrhgJvhgKzhgIDhgLDhgLjhgJXhgLzhgLHhgKzhgIThgLrhgLjhgIHhgLzhgIThgLrhgLhcIikuZ2V0TWVzc2FnZSgpfSB7TWF0aC5hYnMocGFyc2VJbnQoKGNvbnZlcnNpb25SYXRpbyAqIDEwMCkgYXMgYW55KSl9JTwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHA+e0xhbmd1YWdlLmVuKFwiUmVuZXdhbCBSYXRpb1wiKS50aGFpKFwi4Lit4Lix4LiV4Lij4Liy4Liq4LmI4Lin4LiZ4LiB4Liy4Lij4LiV4LmI4Lit4Lit4Liy4Lii4Li4XCIpLm15KFwi4YCe4YCA4YC64YCQ4YCZ4YC64YC44YCQ4YCt4YCv4YC44YCh4YCB4YC74YCt4YCvXCIpLmdldE1lc3NhZ2UoKX0ge01hdGguYWJzKHBhcnNlSW50KChyZW5ld2FsICogMTAwKSBhcyBhbnkpKX0lPC9wPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgPEljb24gdHlwZT1cInF1ZXN0aW9uLWNpcmNsZVwiLz5cbiAgICAgICAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0+XG4gICAgICAgICAgICAgIDxDYXJkIHRpdGxlPXtcbiAgICAgICAgICAgICAgICB0aGlzLmFnZW50UmVuZGVyVGl0bGUoKVxuICAgICAgICAgICAgICB9IGJvcmRlcmVkPXtmYWxzZX0+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWJvZHlcIj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgICB7Xy5nZXQoc2VwQWN0aXZlVXNlcnMsIFwiYWN0aXZlVXNlcnNcIiwgMCl9XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7IHBhZGRpbmc6IFwiMCA1cHhcIiB9fT5pbjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAge1V0aWxzLmdldE1vbnRoRW4oXy5nZXQoc2VwQWN0aXZlVXNlcnMsIFwiY3VycmVudE1vbnRoXCIsIDApKX1cbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgICAgICB7Xy5nZXQoSW5BY3RpdmVVc2VycywgXCJhY3RpdmVVc2Vyc1wiLCAwKX1cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgcGFkZGluZzogXCIwIDVweFwiIH19PmluPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICB7Xy5nZXQoSW5BY3RpdmVVc2VycywgXCJjdXJyZW50WWVhclwiLCBuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCkpfVxuICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgY2xhc3NOYW1lPVwiY2FyZC1sYXN0XCIgc3Bhbj17Nn0+XG4gICAgICAgICAgICAgIDxDYXJkIGJvcmRlcmVkPXtmYWxzZX0+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLXRpdGxlXCI+XG4gICAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0aXRsZVwiPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJLUEkgQUNISUVWRU1FTlRTXCIpLnRoYWkoXCLguK3guLHguJXguKPguLLguIHguLLguKPguYDguJXguLTguJrguYLguJXguJXguLHguKfguYHguJfguJnguYHguKXguLDguKXguLnguIHguITguYnguLJcIikubXkoXCJLUEkg4YCh4YCx4YCs4YCE4YC64YCZ4YC84YCE4YC64YCZ4YC+4YCv4YCZ4YC74YCs4YC4XCIpLmdldE1lc3NhZ2UoKX0ge25ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgICA8SW5kZXhDb21tb25TdHlsZS5LUEk+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwia3BpXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57TGFuZ3VhZ2UuZW4oXCJTYWxlc1wiKS50aGFpKFwi4LiC4Liy4LiiXCIpLm15KFwi4YCh4YCb4YCx4YCs4YCE4YC64YC4XCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2dyZXNzIGZvcm1hdD17KHBwOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZpbHRlclByb2dyZXNzKHNhbGVzLCBwcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gc3Ryb2tlQ29sb3I9JyM2NWExNzInIHN0YXR1cz1cImFjdGl2ZVwiIHBlcmNlbnQ9e3NhbGVzfS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiQWdlbnQgUmVjcnVpdGluZ1wiKS50aGFpKFwiQWdlbnQgUmVjcnVpdGluZ1wiKS5teShcIkFnZW50IFJlY3J1aXRpbmdcIikuZ2V0TWVzc2FnZSgpfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8UHJvZ3Jlc3MgZm9ybWF0PXsocHA6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmlsdGVyUHJvZ3Jlc3MoYWdlbnRSZWNydWl0aW5nLCBwcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0gc3Ryb2tlQ29sb3I9JyM2NWExNzInIHN0YXR1cz1cImFjdGl2ZVwiIHBlcmNlbnQ9e2FnZW50UmVjcnVpdGluZ30vPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntMYW5ndWFnZS5lbihcIkN1c3RvbWVyIEdyb3d0aFwiKS50aGFpKFwi4LiB4Liy4Lij4LmA4LiV4Li04Lia4LmC4LiV4LiC4Lit4LiH4Lil4Li54LiB4LiE4LmJ4LiyXCIpLm15KFwi4YCW4YCx4YCs4YCA4YC64YCe4YCK4YC64YCA4YC84YCu4YC44YCR4YC94YCs4YC4XCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2dyZXNzIHN0cm9rZUNvbG9yPScjNjVhMTcyJyBmb3JtYXQ9eyhwcDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maWx0ZXJQcm9ncmVzcyhjdXN0b21lckdyb3d0aCwgcHApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19IHN0YXR1cz1cImFjdGl2ZVwiIHBlcmNlbnQ9e2N1c3RvbWVyR3Jvd3RofS8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e0xhbmd1YWdlLmVuKFwiTG9zcyBSYXRpb1wiKS50aGFpKFwi4Lit4Lix4LiV4Lij4Liy4LiB4Liy4Lij4Liq4Li54LiN4LmA4Liq4Li14LiiXCIpLm15KFwi4YCh4YCb4YC+4YCv4YC24YC44YCh4YCB4YC74YCt4YCv4YC4XCIpLmdldE1lc3NhZ2UoKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFByb2dyZXNzIHN0cm9rZUNvbG9yPScjY2E3YTY4JyBzdGF0dXM9XCJhY3RpdmVcIiBmb3JtYXQ9eyhwcDogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5maWx0ZXJQcm9ncmVzcyhsb3NzUmF0aW8sIHBwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fSBwZXJjZW50PXtsb3NzUmF0aW99Lz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9JbmRleENvbW1vblN0eWxlLktQST5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2Bnd3AtaXNzdWVkICR7aXNNb2JpbGUgPyBcIm1vYmlsZS1nd3BcIiA6IFwiXCJ9YH1cbiAgICAgICAgICAgICAgIHN0eWxlPXt7IHBhZGRpbmc6IFwiMjBweFwiLCBtYXJnaW5Ub3A6IGlzTW9iaWxlID8gXCIwcHhcIiA6IFwiMjBweFwiLCBiYWNrZ3JvdW5kQ29sb3I6IFwiI2ZmZlwiIH19PlxuICAgICAgICAgICAgPFRhYnNcbiAgICAgICAgICAgICAgZGVmYXVsdEFjdGl2ZUtleT1cInByZW1pdW1cIlxuICAgICAgICAgICAgICBvbkNoYW5nZT17KGtleSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgdGFiS2V5OiBrZXksXG4gICAgICAgICAgICAgICAgICBhY3RpdmVLZXk6IFwibW9udGhcIixcbiAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMudGltZSk7XG4gICAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRoaXMuc2FsZXNUaW1lKTtcbiAgICAgICAgICAgICAgICAgIHRoaXMuY2hhbmdlVGFiKCk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIHRhYkJhckV4dHJhQ29udGVudD17XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0YWJzLXJpZ2h0XCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRhYnMtYVwiPlxuICAgICAgICAgICAgICAgICAgICB7dGFic0EoKS5tYXAoKGV2ZXJ5OiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgPGEgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVLZXk6IGV2ZXJ5LmtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVUYWI6IHRhYnNBKClbaW5kZXhdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZXJ5LmtleSA9PT0gXCJ0aW1lXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KHRhYktleSA9PT0gXCJwcmVtaXVtXCIgPyBcIjk3OThcIiA6IFwiOTc5OVwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0U2FsZXNIaXN0b3J5KHRhYktleSA9PT0gXCJwcmVtaXVtXCIgPyBcIjk3MjFcIiA6IFwiOTcyMlwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudGltZSA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRTYWxlc0hpc3RvcnkodGFiS2V5ID09PSBcInByZW1pdW1cIiA/IFwiOTc5OFwiIDogXCI5Nzk5XCIsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMzAwMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNhbGVzVGltZSA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRTYWxlc0hpc3RvcnkodGFiS2V5ID09PSBcInByZW1pdW1cIiA/IFwiOTcyMVwiIDogXCI5NzIyXCIsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSwgMzAwMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy50aW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5zYWxlc1RpbWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGFuZ2VUYWIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfX0gY2xhc3NOYW1lPXthY3RpdmVLZXkgPT09IGV2ZXJ5LmtleSA/IFwiYWN0aXZlXCIgOiBcIlwifSBrZXk9e2V2ZXJ5LmtleX0+e2V2ZXJ5LnRhYn08L2E+XG4gICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIHshaXNNb2JpbGUgJiYgPFJhbmdlUGlja2VyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17WyhhY3RpdmVUYWIudGltZSB8fCB7fSkuZnJvbSwgKGFjdGl2ZVRhYi50aW1lIHx8IHt9KS50b119XG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17WyhhY3RpdmVUYWIudGltZSB8fCB7fSkuZnJvbSwgKGFjdGl2ZVRhYi50aW1lIHx8IHt9KS50b119XG4gICAgICAgICAgICAgICAgICAgIGZvcm1hdD17Q29uc3RzLkRBVEVfRk9STUFULkRBVEVfRk9STUFUfVxuICAgICAgICAgICAgICAgICAgLz59XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIH0+XG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICB0YWJMaXN0Tm9UaXRsZSgpLm1hcCgoZXZlcnk6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgPFRhYlBhbmUgdGFiPXtldmVyeS50YWJ9IGtleT17ZXZlcnkua2V5fT5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YHRhYiAke2lzTW9iaWxlID8gXCJtb2JpbGUtdGFiXCIgOiBcIlwifWB9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2B0YWItbGVmdCAke2lzTW9iaWxlID8gXCJtb2JpbGUtbGVmdFwiIDogXCJcIn1gfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAge2V2ZXJ5LmtleSA9PT0gXCJwcmVtaXVtXCIgPyB0aGlzLnJlbmRlckdXUERhdGEoKSA6IHRoaXMucmVuZGVyUG9saWN5RGF0YSgpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17YHRhYi1yaWdodCAke2lzTW9iaWxlID8gXCJtb2JpbGUtcmlnaHRcIiA6IFwiXCJ9YH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxoND57Xy5nZXQoc2FsZXNDaGFtcGlvbiwgXCJ0aXRsZVwiKX08L2g0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKF8uZ2V0KHNhbGVzQ2hhbXBpb24sIFwidG9wU2FsZXNcIikgfHwgW10pLm1hcCgoZXZlcnk6IGFueSwgaW5kZXg6IG51bWJlcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsaSBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImktc3BhblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXggPCAzID8gPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjPXtyZXF1aXJlKGAuLi8uLi8uLi9hc3NldHMvcmVwb3J0cy8ke1tcIm9uZVwiLCBcInR3b1wiLCBcInRocmVlXCJdW2luZGV4XX0uc3ZnYCl9Lz4gOiBpbmRleCArIDFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXZlcnkuc2FsZXNOYW1lfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntldmVyeS5wcmVtaXVtIHx8IGV2ZXJ5LnBvbGljeUlzc3VlZH08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvVGFiUGFuZT5cbiAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgPC9UYWJzPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvQy5NYW5hZ2VySW5kZXg+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBNYW5hZ2VySW5kZXg7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQU9BO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQU9BO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBTUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUNBO0FBbUNBOzs7OztBQU9BO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQSxxQkFrQkE7QUFDQTtBQUVBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbkJBO0FBcUJBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUVBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFEQTtBQUNBO0FBREE7QUFFQTtBQUFBO0FBRkE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQTVFQTtBQThFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTs7O0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7OztBQUVBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQXBCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzQkE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXpDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFzREE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBR0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQU9BO0FBT0E7Ozs7QUFub0JBO0FBQ0E7QUFxb0JBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/home/manager-index.tsx
