__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};






var TimelineItem = function TimelineItem(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_3__["ConfigConsumer"], null, function (_ref) {
    var _classNames, _classNames2;

    var getPrefixCls = _ref.getPrefixCls;

    var customizePrefixCls = props.prefixCls,
        className = props.className,
        _props$color = props.color,
        color = _props$color === void 0 ? '' : _props$color,
        children = props.children,
        pending = props.pending,
        dot = props.dot,
        restProps = __rest(props, ["prefixCls", "className", "color", "children", "pending", "dot"]);

    var prefixCls = getPrefixCls('timeline', customizePrefixCls);
    var itemClassName = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-item"), true), _defineProperty(_classNames, "".concat(prefixCls, "-item-pending"), pending), _classNames), className);
    var dotClassName = classnames__WEBPACK_IMPORTED_MODULE_1___default()((_classNames2 = {}, _defineProperty(_classNames2, "".concat(prefixCls, "-item-head"), true), _defineProperty(_classNames2, "".concat(prefixCls, "-item-head-custom"), dot), _defineProperty(_classNames2, "".concat(prefixCls, "-item-head-").concat(color), true), _classNames2));
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", _extends({}, Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(restProps, ['position']), {
      className: itemClassName
    }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-item-tail")
    }), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: dotClassName,
      style: {
        borderColor: /blue|red|green|gray/.test(color) ? undefined : color
      }
    }, dot), react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", {
      className: "".concat(prefixCls, "-item-content")
    }, children));
  });
};

TimelineItem.defaultProps = {
  color: 'blue',
  pending: false,
  position: ''
};
/* harmony default export */ __webpack_exports__["default"] = (TimelineItem);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90aW1lbGluZS9UaW1lbGluZUl0ZW0uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3RpbWVsaW5lL1RpbWVsaW5lSXRlbS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmNvbnN0IFRpbWVsaW5lSXRlbSA9IHByb3BzID0+ICg8Q29uZmlnQ29uc3VtZXI+XG4gICAgeyh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgY29uc3QgeyBwcmVmaXhDbHM6IGN1c3RvbWl6ZVByZWZpeENscywgY2xhc3NOYW1lLCBjb2xvciA9ICcnLCBjaGlsZHJlbiwgcGVuZGluZywgZG90IH0gPSBwcm9wcywgcmVzdFByb3BzID0gX19yZXN0KHByb3BzLCBbXCJwcmVmaXhDbHNcIiwgXCJjbGFzc05hbWVcIiwgXCJjb2xvclwiLCBcImNoaWxkcmVuXCIsIFwicGVuZGluZ1wiLCBcImRvdFwiXSk7XG4gICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCd0aW1lbGluZScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgY29uc3QgaXRlbUNsYXNzTmFtZSA9IGNsYXNzTmFtZXMoe1xuICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtYF06IHRydWUsXG4gICAgICAgIFtgJHtwcmVmaXhDbHN9LWl0ZW0tcGVuZGluZ2BdOiBwZW5kaW5nLFxuICAgIH0sIGNsYXNzTmFtZSk7XG4gICAgY29uc3QgZG90Q2xhc3NOYW1lID0gY2xhc3NOYW1lcyh7XG4gICAgICAgIFtgJHtwcmVmaXhDbHN9LWl0ZW0taGVhZGBdOiB0cnVlLFxuICAgICAgICBbYCR7cHJlZml4Q2xzfS1pdGVtLWhlYWQtY3VzdG9tYF06IGRvdCxcbiAgICAgICAgW2Ake3ByZWZpeENsc30taXRlbS1oZWFkLSR7Y29sb3J9YF06IHRydWUsXG4gICAgfSk7XG4gICAgcmV0dXJuICg8bGkgey4uLm9taXQocmVzdFByb3BzLCBbJ3Bvc2l0aW9uJ10pfSBjbGFzc05hbWU9e2l0ZW1DbGFzc05hbWV9PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tdGFpbGB9Lz5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17ZG90Q2xhc3NOYW1lfSBzdHlsZT17eyBib3JkZXJDb2xvcjogL2JsdWV8cmVkfGdyZWVufGdyYXkvLnRlc3QoY29sb3IpID8gdW5kZWZpbmVkIDogY29sb3IgfX0+XG4gICAgICAgICAgICB7ZG90fVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtgJHtwcmVmaXhDbHN9LWl0ZW0tY29udGVudGB9PntjaGlsZHJlbn08L2Rpdj5cbiAgICAgICAgPC9saT4pO1xufX1cbiAgPC9Db25maWdDb25zdW1lcj4pO1xuVGltZWxpbmVJdGVtLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBjb2xvcjogJ2JsdWUnLFxuICAgIHBlbmRpbmc6IGZhbHNlLFxuICAgIHBvc2l0aW9uOiAnJyxcbn07XG5leHBvcnQgZGVmYXVsdCBUaW1lbGluZUl0ZW07XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFLQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFsQkE7QUFBQTtBQUNBO0FBcUJBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/timeline/TimelineItem.js
