__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Col; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _RowContext__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RowContext */ "./node_modules/antd/es/grid/RowContext.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};






var objectOrNumber = prop_types__WEBPACK_IMPORTED_MODULE_1__["oneOfType"]([prop_types__WEBPACK_IMPORTED_MODULE_1__["object"], prop_types__WEBPACK_IMPORTED_MODULE_1__["number"]]);

var Col =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Col, _React$Component);

  function Col() {
    var _this;

    _classCallCheck(this, Col);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Col).apply(this, arguments));

    _this.renderCol = function (_ref) {
      var _classNames;

      var getPrefixCls = _ref.getPrefixCls;

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props;

      var customizePrefixCls = props.prefixCls,
          span = props.span,
          order = props.order,
          offset = props.offset,
          push = props.push,
          pull = props.pull,
          className = props.className,
          children = props.children,
          others = __rest(props, ["prefixCls", "span", "order", "offset", "push", "pull", "className", "children"]);

      var prefixCls = getPrefixCls('col', customizePrefixCls);
      var sizeClassObj = {};
      ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'].forEach(function (size) {
        var _extends2;

        var sizeProps = {};
        var propSize = props[size];

        if (typeof propSize === 'number') {
          sizeProps.span = propSize;
        } else if (_typeof(propSize) === 'object') {
          sizeProps = propSize || {};
        }

        delete others[size];
        sizeClassObj = _extends(_extends({}, sizeClassObj), (_extends2 = {}, _defineProperty(_extends2, "".concat(prefixCls, "-").concat(size, "-").concat(sizeProps.span), sizeProps.span !== undefined), _defineProperty(_extends2, "".concat(prefixCls, "-").concat(size, "-order-").concat(sizeProps.order), sizeProps.order || sizeProps.order === 0), _defineProperty(_extends2, "".concat(prefixCls, "-").concat(size, "-offset-").concat(sizeProps.offset), sizeProps.offset || sizeProps.offset === 0), _defineProperty(_extends2, "".concat(prefixCls, "-").concat(size, "-push-").concat(sizeProps.push), sizeProps.push || sizeProps.push === 0), _defineProperty(_extends2, "".concat(prefixCls, "-").concat(size, "-pull-").concat(sizeProps.pull), sizeProps.pull || sizeProps.pull === 0), _extends2));
      });
      var classes = classnames__WEBPACK_IMPORTED_MODULE_2___default()(prefixCls, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-").concat(span), span !== undefined), _defineProperty(_classNames, "".concat(prefixCls, "-order-").concat(order), order), _defineProperty(_classNames, "".concat(prefixCls, "-offset-").concat(offset), offset), _defineProperty(_classNames, "".concat(prefixCls, "-push-").concat(push), push), _defineProperty(_classNames, "".concat(prefixCls, "-pull-").concat(pull), pull), _classNames), className, sizeClassObj);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_RowContext__WEBPACK_IMPORTED_MODULE_3__["default"].Consumer, null, function (_ref2) {
        var gutter = _ref2.gutter;
        var style = others.style;

        if (gutter) {
          style = _extends(_extends(_extends({}, gutter[0] > 0 ? {
            paddingLeft: gutter[0] / 2,
            paddingRight: gutter[0] / 2
          } : {}), gutter[1] > 0 ? {
            paddingTop: gutter[1] / 2,
            paddingBottom: gutter[1] / 2
          } : {}), style);
        }

        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", _extends({}, others, {
          style: style,
          className: classes
        }), children);
      });
    };

    return _this;
  }

  _createClass(Col, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_4__["ConfigConsumer"], null, this.renderCol);
    }
  }]);

  return Col;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);


Col.propTypes = {
  span: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  order: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  offset: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  push: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  pull: prop_types__WEBPACK_IMPORTED_MODULE_1__["number"],
  className: prop_types__WEBPACK_IMPORTED_MODULE_1__["string"],
  children: prop_types__WEBPACK_IMPORTED_MODULE_1__["node"],
  xs: objectOrNumber,
  sm: objectOrNumber,
  md: objectOrNumber,
  lg: objectOrNumber,
  xl: objectOrNumber,
  xxl: objectOrNumber
};//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9ncmlkL2NvbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvZ3JpZC9jb2wuanN4Il0sInNvdXJjZXNDb250ZW50IjpbInZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCAqIGFzIFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IFJvd0NvbnRleHQgZnJvbSAnLi9Sb3dDb250ZXh0JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmNvbnN0IG9iamVjdE9yTnVtYmVyID0gUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm9iamVjdCwgUHJvcFR5cGVzLm51bWJlcl0pO1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29sIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoLi4uYXJndW1lbnRzKTtcbiAgICAgICAgdGhpcy5yZW5kZXJDb2wgPSAoeyBnZXRQcmVmaXhDbHMgfSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBwcm9wcyB9ID0gdGhpcztcbiAgICAgICAgICAgIGNvbnN0IHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIHNwYW4sIG9yZGVyLCBvZmZzZXQsIHB1c2gsIHB1bGwsIGNsYXNzTmFtZSwgY2hpbGRyZW4gfSA9IHByb3BzLCBvdGhlcnMgPSBfX3Jlc3QocHJvcHMsIFtcInByZWZpeENsc1wiLCBcInNwYW5cIiwgXCJvcmRlclwiLCBcIm9mZnNldFwiLCBcInB1c2hcIiwgXCJwdWxsXCIsIFwiY2xhc3NOYW1lXCIsIFwiY2hpbGRyZW5cIl0pO1xuICAgICAgICAgICAgY29uc3QgcHJlZml4Q2xzID0gZ2V0UHJlZml4Q2xzKCdjb2wnLCBjdXN0b21pemVQcmVmaXhDbHMpO1xuICAgICAgICAgICAgbGV0IHNpemVDbGFzc09iaiA9IHt9O1xuICAgICAgICAgICAgWyd4cycsICdzbScsICdtZCcsICdsZycsICd4bCcsICd4eGwnXS5mb3JFYWNoKHNpemUgPT4ge1xuICAgICAgICAgICAgICAgIGxldCBzaXplUHJvcHMgPSB7fTtcbiAgICAgICAgICAgICAgICBjb25zdCBwcm9wU2l6ZSA9IHByb3BzW3NpemVdO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcHJvcFNpemUgPT09ICdudW1iZXInKSB7XG4gICAgICAgICAgICAgICAgICAgIHNpemVQcm9wcy5zcGFuID0gcHJvcFNpemU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBwcm9wU2l6ZSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICAgICAgc2l6ZVByb3BzID0gcHJvcFNpemUgfHwge307XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRlbGV0ZSBvdGhlcnNbc2l6ZV07XG4gICAgICAgICAgICAgICAgc2l6ZUNsYXNzT2JqID0gT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCBzaXplQ2xhc3NPYmopLCB7IFtgJHtwcmVmaXhDbHN9LSR7c2l6ZX0tJHtzaXplUHJvcHMuc3Bhbn1gXTogc2l6ZVByb3BzLnNwYW4gIT09IHVuZGVmaW5lZCwgW2Ake3ByZWZpeENsc30tJHtzaXplfS1vcmRlci0ke3NpemVQcm9wcy5vcmRlcn1gXTogc2l6ZVByb3BzLm9yZGVyIHx8IHNpemVQcm9wcy5vcmRlciA9PT0gMCwgW2Ake3ByZWZpeENsc30tJHtzaXplfS1vZmZzZXQtJHtzaXplUHJvcHMub2Zmc2V0fWBdOiBzaXplUHJvcHMub2Zmc2V0IHx8IHNpemVQcm9wcy5vZmZzZXQgPT09IDAsIFtgJHtwcmVmaXhDbHN9LSR7c2l6ZX0tcHVzaC0ke3NpemVQcm9wcy5wdXNofWBdOiBzaXplUHJvcHMucHVzaCB8fCBzaXplUHJvcHMucHVzaCA9PT0gMCwgW2Ake3ByZWZpeENsc30tJHtzaXplfS1wdWxsLSR7c2l6ZVByb3BzLnB1bGx9YF06IHNpemVQcm9wcy5wdWxsIHx8IHNpemVQcm9wcy5wdWxsID09PSAwIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjb25zdCBjbGFzc2VzID0gY2xhc3NOYW1lcyhwcmVmaXhDbHMsIHtcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS0ke3NwYW59YF06IHNwYW4gIT09IHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1vcmRlci0ke29yZGVyfWBdOiBvcmRlcixcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1vZmZzZXQtJHtvZmZzZXR9YF06IG9mZnNldCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1wdXNoLSR7cHVzaH1gXTogcHVzaCxcbiAgICAgICAgICAgICAgICBbYCR7cHJlZml4Q2xzfS1wdWxsLSR7cHVsbH1gXTogcHVsbCxcbiAgICAgICAgICAgIH0sIGNsYXNzTmFtZSwgc2l6ZUNsYXNzT2JqKTtcbiAgICAgICAgICAgIHJldHVybiAoPFJvd0NvbnRleHQuQ29uc3VtZXI+XG4gICAgICAgIHsoeyBndXR0ZXIgfSkgPT4ge1xuICAgICAgICAgICAgICAgIGxldCB7IHN0eWxlIH0gPSBvdGhlcnM7XG4gICAgICAgICAgICAgICAgaWYgKGd1dHRlcikge1xuICAgICAgICAgICAgICAgICAgICBzdHlsZSA9IE9iamVjdC5hc3NpZ24oT2JqZWN0LmFzc2lnbihPYmplY3QuYXNzaWduKHt9LCAoZ3V0dGVyWzBdID4gMFxuICAgICAgICAgICAgICAgICAgICAgICAgPyB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZ0xlZnQ6IGd1dHRlclswXSAvIDIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZ1JpZ2h0OiBndXR0ZXJbMF0gLyAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgOiB7fSkpLCAoZ3V0dGVyWzFdID4gMFxuICAgICAgICAgICAgICAgICAgICAgICAgPyB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZ1RvcDogZ3V0dGVyWzFdIC8gMixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nQm90dG9tOiBndXR0ZXJbMV0gLyAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgOiB7fSkpLCBzdHlsZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiAoPGRpdiB7Li4ub3RoZXJzfSBzdHlsZT17c3R5bGV9IGNsYXNzTmFtZT17Y2xhc3Nlc30+XG4gICAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgICAgIDwvZGl2Pik7XG4gICAgICAgICAgICB9fVxuICAgICAgPC9Sb3dDb250ZXh0LkNvbnN1bWVyPik7XG4gICAgICAgIH07XG4gICAgfVxuICAgIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIDxDb25maWdDb25zdW1lcj57dGhpcy5yZW5kZXJDb2x9PC9Db25maWdDb25zdW1lcj47XG4gICAgfVxufVxuQ29sLnByb3BUeXBlcyA9IHtcbiAgICBzcGFuOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIG9yZGVyOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIG9mZnNldDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBwdXNoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHB1bGw6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZSxcbiAgICB4czogb2JqZWN0T3JOdW1iZXIsXG4gICAgc206IG9iamVjdE9yTnVtYmVyLFxuICAgIG1kOiBvYmplY3RPck51bWJlcixcbiAgICBsZzogb2JqZWN0T3JOdW1iZXIsXG4gICAgeGw6IG9iamVjdE9yTnVtYmVyLFxuICAgIHh4bDogb2JqZWN0T3JOdW1iZXIsXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBVEE7QUFDQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQVZBO0FBWUE7QUFPQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFoQkE7QUF4QkE7QUFDQTtBQUhBO0FBZ0RBO0FBQ0E7OztBQUFBO0FBQ0E7QUFDQTs7OztBQXBEQTtBQUNBO0FBREE7QUFzREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWJBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/antd/es/grid/col.js
