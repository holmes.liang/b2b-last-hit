__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rc_pagination_es_locale_en_US__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rc-pagination/es/locale/en_US */ "./node_modules/rc-pagination/es/locale/en_US.js");
/* harmony import */ var _date_picker_locale_en_US__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../date-picker/locale/en_US */ "./node_modules/antd/es/date-picker/locale/en_US.js");
/* harmony import */ var _time_picker_locale_en_US__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../time-picker/locale/en_US */ "./node_modules/antd/es/time-picker/locale/en_US.js");
/* harmony import */ var _calendar_locale_en_US__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../calendar/locale/en_US */ "./node_modules/antd/es/calendar/locale/en_US.js");




/* harmony default export */ __webpack_exports__["default"] = ({
  locale: 'en',
  Pagination: rc_pagination_es_locale_en_US__WEBPACK_IMPORTED_MODULE_0__["default"],
  DatePicker: _date_picker_locale_en_US__WEBPACK_IMPORTED_MODULE_1__["default"],
  TimePicker: _time_picker_locale_en_US__WEBPACK_IMPORTED_MODULE_2__["default"],
  Calendar: _calendar_locale_en_US__WEBPACK_IMPORTED_MODULE_3__["default"],
  global: {
    placeholder: 'Please select'
  },
  Table: {
    filterTitle: 'Filter menu',
    filterConfirm: 'OK',
    filterReset: 'Reset',
    selectAll: 'Select current page',
    selectInvert: 'Invert current page',
    sortTitle: 'Sort',
    expand: 'Expand row',
    collapse: 'Collapse row'
  },
  Modal: {
    okText: 'OK',
    cancelText: 'Cancel',
    justOkText: 'OK'
  },
  Popconfirm: {
    okText: 'OK',
    cancelText: 'Cancel'
  },
  Transfer: {
    titles: ['', ''],
    searchPlaceholder: 'Search here',
    itemUnit: 'item',
    itemsUnit: 'items'
  },
  Upload: {
    uploading: 'Uploading...',
    removeFile: 'Remove file',
    uploadError: 'Upload error',
    previewFile: 'Preview file',
    downloadFile: 'Download file'
  },
  Empty: {
    description: 'No Data'
  },
  Icon: {
    icon: 'icon'
  },
  Text: {
    edit: 'Edit',
    copy: 'Copy',
    copied: 'Copied',
    expand: 'Expand'
  },
  PageHeader: {
    back: 'Back'
  }
});//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9sb2NhbGUvZGVmYXVsdC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbG9jYWxlL2RlZmF1bHQuanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQYWdpbmF0aW9uIGZyb20gJ3JjLXBhZ2luYXRpb24vbGliL2xvY2FsZS9lbl9VUyc7XG5pbXBvcnQgRGF0ZVBpY2tlciBmcm9tICcuLi9kYXRlLXBpY2tlci9sb2NhbGUvZW5fVVMnO1xuaW1wb3J0IFRpbWVQaWNrZXIgZnJvbSAnLi4vdGltZS1waWNrZXIvbG9jYWxlL2VuX1VTJztcbmltcG9ydCBDYWxlbmRhciBmcm9tICcuLi9jYWxlbmRhci9sb2NhbGUvZW5fVVMnO1xuZXhwb3J0IGRlZmF1bHQge1xuICAgIGxvY2FsZTogJ2VuJyxcbiAgICBQYWdpbmF0aW9uLFxuICAgIERhdGVQaWNrZXIsXG4gICAgVGltZVBpY2tlcixcbiAgICBDYWxlbmRhcixcbiAgICBnbG9iYWw6IHtcbiAgICAgICAgcGxhY2Vob2xkZXI6ICdQbGVhc2Ugc2VsZWN0JyxcbiAgICB9LFxuICAgIFRhYmxlOiB7XG4gICAgICAgIGZpbHRlclRpdGxlOiAnRmlsdGVyIG1lbnUnLFxuICAgICAgICBmaWx0ZXJDb25maXJtOiAnT0snLFxuICAgICAgICBmaWx0ZXJSZXNldDogJ1Jlc2V0JyxcbiAgICAgICAgc2VsZWN0QWxsOiAnU2VsZWN0IGN1cnJlbnQgcGFnZScsXG4gICAgICAgIHNlbGVjdEludmVydDogJ0ludmVydCBjdXJyZW50IHBhZ2UnLFxuICAgICAgICBzb3J0VGl0bGU6ICdTb3J0JyxcbiAgICAgICAgZXhwYW5kOiAnRXhwYW5kIHJvdycsXG4gICAgICAgIGNvbGxhcHNlOiAnQ29sbGFwc2Ugcm93JyxcbiAgICB9LFxuICAgIE1vZGFsOiB7XG4gICAgICAgIG9rVGV4dDogJ09LJyxcbiAgICAgICAgY2FuY2VsVGV4dDogJ0NhbmNlbCcsXG4gICAgICAgIGp1c3RPa1RleHQ6ICdPSycsXG4gICAgfSxcbiAgICBQb3Bjb25maXJtOiB7XG4gICAgICAgIG9rVGV4dDogJ09LJyxcbiAgICAgICAgY2FuY2VsVGV4dDogJ0NhbmNlbCcsXG4gICAgfSxcbiAgICBUcmFuc2Zlcjoge1xuICAgICAgICB0aXRsZXM6IFsnJywgJyddLFxuICAgICAgICBzZWFyY2hQbGFjZWhvbGRlcjogJ1NlYXJjaCBoZXJlJyxcbiAgICAgICAgaXRlbVVuaXQ6ICdpdGVtJyxcbiAgICAgICAgaXRlbXNVbml0OiAnaXRlbXMnLFxuICAgIH0sXG4gICAgVXBsb2FkOiB7XG4gICAgICAgIHVwbG9hZGluZzogJ1VwbG9hZGluZy4uLicsXG4gICAgICAgIHJlbW92ZUZpbGU6ICdSZW1vdmUgZmlsZScsXG4gICAgICAgIHVwbG9hZEVycm9yOiAnVXBsb2FkIGVycm9yJyxcbiAgICAgICAgcHJldmlld0ZpbGU6ICdQcmV2aWV3IGZpbGUnLFxuICAgICAgICBkb3dubG9hZEZpbGU6ICdEb3dubG9hZCBmaWxlJyxcbiAgICB9LFxuICAgIEVtcHR5OiB7XG4gICAgICAgIGRlc2NyaXB0aW9uOiAnTm8gRGF0YScsXG4gICAgfSxcbiAgICBJY29uOiB7XG4gICAgICAgIGljb246ICdpY29uJyxcbiAgICB9LFxuICAgIFRleHQ6IHtcbiAgICAgICAgZWRpdDogJ0VkaXQnLFxuICAgICAgICBjb3B5OiAnQ29weScsXG4gICAgICAgIGNvcGllZDogJ0NvcGllZCcsXG4gICAgICAgIGV4cGFuZDogJ0V4cGFuZCcsXG4gICAgfSxcbiAgICBQYWdlSGVhZGVyOiB7XG4gICAgICAgIGJhY2s6ICdCYWNrJyxcbiAgICB9LFxufTtcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQXJEQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/locale/default.js
