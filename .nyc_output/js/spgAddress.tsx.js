__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/get.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @component */ "./src/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @styles/index */ "./src/styles/index.tsx");
/* harmony import */ var _desk_component_field_group__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-component/field-group */ "./src/app/desk/component/field-group.tsx");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_15__);








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/spgAddress.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_1__["default"])(["\n        .usage-group{\n          .ant-col {\n            >label {\n              text-transform: uppercase\n            }\n          }\n        }\n        .group .ant-row {\n          &:first-child {\n            width: 100%;\n            .ant-col {\n              .ant-input {\n                border-left: 1px solid #ebebeb;\n                border-top-left-radius: 4px;\n                border-bottom-left-radius: 4px;\n                border-top-right-radius: 0;\n                border-bottom-right-radius: 0;\n                &:focus, &:hover {\n                  border-color: ", ";\n                }\n              }\n              .has-error .ant-input {\n                border-color: #f5222d;\n              }\n            }\n          }\n        }\n        .address-view-row{\n          margin-top: -10px;\n          margin-bottom: 20px;\n          .mobile-with {\n            width: 100%;\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}










var _Theme$getTheme = _styles_index__WEBPACK_IMPORTED_MODULE_12__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var isMobile = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].getIsMobile();
var defaultLayout = {
  selectXsSm: {
    xs: 8,
    sm: 6
  },
  textXsSm: {
    xs: 16,
    sm: 13
  }
};

var generatePropsName = function generatePropsName(propName, dataId) {
  if (!!dataId) return "".concat(dataId, ".").concat(propName);
  return propName;
};

var SpgAddress =
/*#__PURE__*/
function (_ModelWidget) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(SpgAddress, _ModelWidget);

  function SpgAddress(props, state) {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, SpgAddress);

    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(SpgAddress).call(this, props, state));
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(SpgAddress, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getAddress = lodash__WEBPACK_IMPORTED_MODULE_15___default.a.debounce(this.getAddress, 300);
      this.getAddressContent = lodash__WEBPACK_IMPORTED_MODULE_15___default.a.debounce(this.getAddressContent, 300);

      if (this.getAddressInfoData() && Object.values(this.getAddressInfoData()).length > 0) {
        this.getAddressContent();
      }
    }
  }, {
    key: "initComponents",
    value: function initComponents() {
      return {
        Box: _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject(), COLOR_A)
      };
    }
  }, {
    key: "getAddressInfoData",
    value: function getAddressInfoData() {
      var dataId = this.props.dataId;
      var addressInfo = this.getValueFromModel(dataId);
      return addressInfo;
    }
  }, {
    key: "getAddressContent",
    value: function getAddressContent() {
      var _this = this;

      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].post("/addresses/full/1_line", this.getAddressInfoData()).then(function (res) {
        var respData = (res.body || {}).respData || "";

        _this.setState({
          addressInfoContent: respData === "null" ? "" : respData
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var C = this.getComponents();
      var _this$props = this.props,
          dataId = _this$props.dataId,
          form = _this$props.form,
          model = _this$props.model,
          required = _this$props.required;
      var addressInfoContent = this.state.addressInfoContent;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(C.Box, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component_field_group__WEBPACK_IMPORTED_MODULE_13__["default"], {
        className: "usage-group",
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en(this.props.propLabel || "Address").thai("Address").getMessage(),
        selectXsSm: this.props.selectXsSm || defaultLayout.selectXsSm,
        textXsSm: this.props.textXsSm || defaultLayout.textXsSm,
        minWidth: "140px",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NText"], {
        required: required,
        form: form,
        model: model,
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Postal Code").thai("Postal Code").getMessage(),
        placeholder: "Postal Code",
        propName: generatePropsName("postalCode", dataId),
        onChange: function onChange(value) {
          if (/^([1-9][0-9]{5})$/.test(value)) {
            _this2.getAddress(value);
          }
        },
        rules: [{
          validator: function validator(rule, value, callback) {
            if (!required && !value) {
              callback();
            } else {
              if (!/^([1-9][0-9]{5})$/.test(value)) {
                callback(_common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Postal Code is valid").thai("Postal Code is invalid").getMessage());
              } else {
                callback();
              }
            }
          }
        }],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_14__["NText"], {
        required: false,
        form: form,
        model: model,
        placeholder: "Unit No.",
        label: _common__WEBPACK_IMPORTED_MODULE_10__["Language"].en("Unit No.").thai("Unit No.").getMessage(),
        onChange: function onChange() {
          _this2.getAddressContent();
        },
        propName: generatePropsName("unitNo", dataId),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166
        },
        __self: this
      })), addressInfoContent && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Fragment, null, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Row"], {
        className: "address-view-row",
        gutter: 16,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 182
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 183
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_11__["Col"], {
        span: 13,
        className: isMobile ? "mobile-with" : "",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 184
        },
        __self: this
      }, addressInfoContent))));
    }
  }, {
    key: "getAddress",
    value: function getAddress(value) {
      var _this3 = this;

      var dataId = this.props.dataId; // 542211

      _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/addresses/postalcode", {
        countryCode: "SGP",
        postalCode: value
      }).then(function (res) {
        _this3.setState({
          addressInfo: (res.body || {}).respData || {}
        }, function () {
          if (!!dataId) {
            _this3.setValueToModel(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, _this3.state.addressInfo, {}, {
              unitNo: lodash__WEBPACK_IMPORTED_MODULE_15___default.a.get(_this3.props.model, generatePropsName("unitNo", dataId))
            }), dataId);
          }

          _this3.getAddressContent();
        });
      });
    }
  }, {
    key: "initState",
    value: function initState() {
      return Object.assign(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_get__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(SpgAddress.prototype), "initState", this).call(this), {
        addressInfo: {},
        addressInfoContent: ""
      });
    }
  }]);

  return SpgAddress;
}(_component__WEBPACK_IMPORTED_MODULE_9__["ModelWidget"]);

SpgAddress.defaultProps = {
  required: true
};
/* harmony default export */ __webpack_exports__["default"] = (SpgAddress);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3NwZ0FkZHJlc3MudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3NwZ0FkZHJlc3MudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0LCBTdHlsZWQgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IE1vZGVsV2lkZ2V0IH0gZnJvbSBcIkBjb21wb25lbnRcIjtcbmltcG9ydCB7IFN0eWxlZERJViwgV2lkZ2V0UHJvcHMgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBBamF4LCBMYW5ndWFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tIFwiYW50ZFwiO1xuaW1wb3J0IFRoZW1lIGZyb20gXCJAc3R5bGVzL2luZGV4XCI7XG5pbXBvcnQgRmllbGRHcm91cCBmcm9tIFwiQGRlc2stY29tcG9uZW50L2ZpZWxkLWdyb3VwXCI7XG5pbXBvcnQgeyBOVGV4dCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5jb25zdCB7IENPTE9SX0EgfSA9IFRoZW1lLmdldFRoZW1lKCk7XG5jb25zdCBpc01vYmlsZSA9IFV0aWxzLmdldElzTW9iaWxlKCk7XG5jb25zdCBkZWZhdWx0TGF5b3V0ID0ge1xuICBzZWxlY3RYc1NtOiB7XG4gICAgeHM6IDgsXG4gICAgc206IDYsXG4gIH0sXG4gIHRleHRYc1NtOiB7XG4gICAgeHM6IDE2LFxuICAgIHNtOiAxMyxcbiAgfSxcbn07XG5leHBvcnQgdHlwZSBTcGdBZGRyZXNzUHJvcHMgPSB7XG4gIG1vZGVsOiBhbnk7XG4gIGZvcm06IGFueTtcbiAgZGF0YUlkOiBzdHJpbmc7XG4gIGNvdW50cnlDb2RlPzogc3RyaW5nO1xuICBzZWxlY3RYc1NtPzogYW55O1xuICB0ZXh0WHNTbT86IGFueTtcbiAgcmVxdWlyZWQ/OiBib29sZWFuO1xuICBwcm9wTGFiZWw/OiBhbnk7XG59ICYgV2lkZ2V0UHJvcHM7XG5cbmV4cG9ydCB0eXBlIFNwZ0FkZHJlc3NTdGF0ZSA9IHtcbiAgYWRkcmVzc0luZm86IGFueTtcbiAgYWRkcmVzc0luZm9Db250ZW50OiBzdHJpbmc7XG59O1xuXG5leHBvcnQgdHlwZSBTcGdBZGRyZXNzQ29tcG9uZW50cyA9IHtcbiAgQm94OiBTdHlsZWRESVY7XG59O1xuY29uc3QgZ2VuZXJhdGVQcm9wc05hbWUgPSAocHJvcE5hbWU6IHN0cmluZywgZGF0YUlkPzogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgaWYgKCEhZGF0YUlkKSByZXR1cm4gYCR7ZGF0YUlkfS4ke3Byb3BOYW1lfWA7XG4gIHJldHVybiBwcm9wTmFtZTtcbn07XG5cbmNsYXNzIFNwZ0FkZHJlc3M8UCBleHRlbmRzIFNwZ0FkZHJlc3NQcm9wcywgUyBleHRlbmRzIFNwZ0FkZHJlc3NTdGF0ZSwgQyBleHRlbmRzIFNwZ0FkZHJlc3NDb21wb25lbnRzPiBleHRlbmRzIE1vZGVsV2lkZ2V0PFAsIFMsIEM+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSwgc3RhdGU/OiBhbnkpIHtcbiAgICBzdXBlcihwcm9wcywgc3RhdGUpO1xuICB9XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICByZXF1aXJlZDogdHJ1ZSxcbiAgfTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0aGlzLmdldEFkZHJlc3MgPSBfLmRlYm91bmNlKHRoaXMuZ2V0QWRkcmVzcywgMzAwKTtcbiAgICB0aGlzLmdldEFkZHJlc3NDb250ZW50ID0gXy5kZWJvdW5jZSh0aGlzLmdldEFkZHJlc3NDb250ZW50LCAzMDApO1xuICAgIGlmICh0aGlzLmdldEFkZHJlc3NJbmZvRGF0YSgpICYmIE9iamVjdC52YWx1ZXModGhpcy5nZXRBZGRyZXNzSW5mb0RhdGEoKSkubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5nZXRBZGRyZXNzQ29udGVudCgpO1xuICAgIH1cbiAgfVxuXG4gIHByb3RlY3RlZCBpbml0Q29tcG9uZW50cygpOiBDIHtcbiAgICByZXR1cm4ge1xuICAgICAgQm94OiBTdHlsZWQuZGl2YFxuICAgICAgICAudXNhZ2UtZ3JvdXB7XG4gICAgICAgICAgLmFudC1jb2wge1xuICAgICAgICAgICAgPmxhYmVsIHtcbiAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuZ3JvdXAgLmFudC1yb3cge1xuICAgICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAuYW50LWNvbCB7XG4gICAgICAgICAgICAgIC5hbnQtaW5wdXQge1xuICAgICAgICAgICAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2ViZWJlYjtcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA0cHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICAgICAgICAgICAgICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1xuICAgICAgICAgICAgICAgICY6Zm9jdXMsICY6aG92ZXIge1xuICAgICAgICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAke0NPTE9SX0F9O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuaGFzLWVycm9yIC5hbnQtaW5wdXQge1xuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2Y1MjIyZDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAuYWRkcmVzcy12aWV3LXJvd3tcbiAgICAgICAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICAgIC5tb2JpbGUtd2l0aCB7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIGAsXG4gICAgfSBhcyBDO1xuICB9XG5cbiAgZ2V0QWRkcmVzc0luZm9EYXRhKCkge1xuICAgIGNvbnN0IHsgZGF0YUlkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGFkZHJlc3NJbmZvID0gdGhpcy5nZXRWYWx1ZUZyb21Nb2RlbChkYXRhSWQpO1xuICAgIHJldHVybiBhZGRyZXNzSW5mbztcbiAgfVxuXG4gIGdldEFkZHJlc3NDb250ZW50KCkge1xuICAgIEFqYXgucG9zdChgL2FkZHJlc3Nlcy9mdWxsLzFfbGluZWAsIHRoaXMuZ2V0QWRkcmVzc0luZm9EYXRhKCkpLnRoZW4oKHJlczogYW55KSA9PiB7XG4gICAgICBjb25zdCByZXNwRGF0YSA9IChyZXMuYm9keSB8fCB7fSkucmVzcERhdGEgfHwgXCJcIjtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhZGRyZXNzSW5mb0NvbnRlbnQ6IHJlc3BEYXRhID09PSBcIm51bGxcIiA/IFwiXCIgOiByZXNwRGF0YSxcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IEMgPSB0aGlzLmdldENvbXBvbmVudHMoKTtcbiAgICBjb25zdCB7IGRhdGFJZCwgZm9ybSwgbW9kZWwsIHJlcXVpcmVkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgYWRkcmVzc0luZm9Db250ZW50IH0gPSB0aGlzLnN0YXRlO1xuICAgIHJldHVybiAoXG4gICAgICA8Qy5Cb3g+XG4gICAgICAgIDxGaWVsZEdyb3VwXG4gICAgICAgICAgY2xhc3NOYW1lPVwidXNhZ2UtZ3JvdXBcIlxuICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbih0aGlzLnByb3BzLnByb3BMYWJlbCB8fCBcIkFkZHJlc3NcIilcbiAgICAgICAgICAgIC50aGFpKFwiQWRkcmVzc1wiKVxuICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICBzZWxlY3RYc1NtPXt0aGlzLnByb3BzLnNlbGVjdFhzU20gfHwgZGVmYXVsdExheW91dC5zZWxlY3RYc1NtfVxuICAgICAgICAgIHRleHRYc1NtPXt0aGlzLnByb3BzLnRleHRYc1NtIHx8IGRlZmF1bHRMYXlvdXQudGV4dFhzU219XG4gICAgICAgICAgbWluV2lkdGg9XCIxNDBweFwiXG4gICAgICAgID5cbiAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgIHJlcXVpcmVkPXtyZXF1aXJlZH1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBsYWJlbD17TGFuZ3VhZ2UuZW4oXCJQb3N0YWwgQ29kZVwiKVxuICAgICAgICAgICAgICAudGhhaShcIlBvc3RhbCBDb2RlXCIpXG4gICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBvc3RhbCBDb2RlXCJcbiAgICAgICAgICAgIHByb3BOYW1lPXtnZW5lcmF0ZVByb3BzTmFtZShcInBvc3RhbENvZGVcIiwgZGF0YUlkKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsodmFsdWU6IGFueSkgPT4ge1xuICAgICAgICAgICAgICBpZiAoL14oWzEtOV1bMC05XXs1fSkkLy50ZXN0KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0QWRkcmVzcyh2YWx1ZSBhcyBhbnkpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9fVxuXG4gICAgICAgICAgICBydWxlcz17W1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFsaWRhdG9yKHJ1bGU6IGFueSwgdmFsdWU6IGFueSwgY2FsbGJhY2s6IGFueSkge1xuICAgICAgICAgICAgICAgICAgaWYgKCFyZXF1aXJlZCAmJiAhdmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghL14oWzEtOV1bMC05XXs1fSkkLy50ZXN0KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKExhbmd1YWdlLmVuKFwiUG9zdGFsIENvZGUgaXMgdmFsaWRcIikudGhhaShcIlBvc3RhbCBDb2RlIGlzIGludmFsaWRcIikuZ2V0TWVzc2FnZSgpKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF19XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8TlRleHRcbiAgICAgICAgICAgIHJlcXVpcmVkPXtmYWxzZX1cbiAgICAgICAgICAgIGZvcm09e2Zvcm19XG4gICAgICAgICAgICBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlVuaXQgTm8uXCJcbiAgICAgICAgICAgIGxhYmVsPXtMYW5ndWFnZS5lbihcIlVuaXQgTm8uXCIpXG4gICAgICAgICAgICAgIC50aGFpKFwiVW5pdCBOby5cIilcbiAgICAgICAgICAgICAgLmdldE1lc3NhZ2UoKX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXsoKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZ2V0QWRkcmVzc0NvbnRlbnQoKTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBwcm9wTmFtZT17Z2VuZXJhdGVQcm9wc05hbWUoXCJ1bml0Tm9cIiwgZGF0YUlkKX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ZpZWxkR3JvdXA+XG4gICAgICAgIHthZGRyZXNzSW5mb0NvbnRlbnQgJiZcbiAgICAgICAgPD5cbiAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cImFkZHJlc3Mtdmlldy1yb3dcIiBndXR0ZXI9ezE2fT5cbiAgICAgICAgICAgIDxDb2wgc3Bhbj17Nn0vPlxuICAgICAgICAgICAgPENvbCBzcGFuPXsxM30gY2xhc3NOYW1lPXtpc01vYmlsZSA/IFwibW9iaWxlLXdpdGhcIiA6IFwiXCJ9PlxuICAgICAgICAgICAgICB7YWRkcmVzc0luZm9Db250ZW50fVxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgPC9Sb3c+XG4gICAgICAgIDwvPlxuICAgICAgICB9XG4gICAgICA8L0MuQm94PlxuICAgICk7XG4gIH1cblxuICBwcm90ZWN0ZWQgZ2V0QWRkcmVzcyh2YWx1ZTogYW55KSB7XG4gICAgY29uc3QgeyBkYXRhSWQgfSA9IHRoaXMucHJvcHM7XG4gICAgLy8gNTQyMjExXG4gICAgQWpheC5nZXQoXCIvYWRkcmVzc2VzL3Bvc3RhbGNvZGVcIiwge1xuICAgICAgY291bnRyeUNvZGU6IFwiU0dQXCIsXG4gICAgICBwb3N0YWxDb2RlOiB2YWx1ZSxcbiAgICB9KS50aGVuKChyZXMpID0+IHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBhZGRyZXNzSW5mbzogKHJlcy5ib2R5IHx8IHt9KS5yZXNwRGF0YSB8fCB7fSxcbiAgICAgIH0sICgpID0+IHtcbiAgICAgICAgaWYgKCEhZGF0YUlkKSB7XG4gICAgICAgICAgdGhpcy5zZXRWYWx1ZVRvTW9kZWwoe1xuICAgICAgICAgICAgLi4udGhpcy5zdGF0ZS5hZGRyZXNzSW5mbyxcbiAgICAgICAgICAgIC4uLnsgdW5pdE5vOiBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBnZW5lcmF0ZVByb3BzTmFtZShcInVuaXROb1wiLCBkYXRhSWQpKSB9LFxuICAgICAgICAgIH0sIGRhdGFJZCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5nZXRBZGRyZXNzQ29udGVudCgpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBwcm90ZWN0ZWQgaW5pdFN0YXRlKCk6IFMge1xuICAgIHJldHVybiBPYmplY3QuYXNzaWduKHN1cGVyLmluaXRTdGF0ZSgpLCB7XG4gICAgICBhZGRyZXNzSW5mbzoge30sXG4gICAgICBhZGRyZXNzSW5mb0NvbnRlbnQ6IFwiXCIsXG4gICAgfSkgYXMgUztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBTcGdBZGRyZXNzO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBTEE7QUFDQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOzs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQXNDQTs7O0FBRUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFoQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWdCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUUE7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7Ozs7QUE3S0E7QUFDQTtBQURBO0FBTUE7QUFEQTtBQTJLQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/spgAddress.tsx
