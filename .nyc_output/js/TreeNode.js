__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternalTreeNode", function() { return TreeNode; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rc_animate_es_CSSMotion__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-animate/es/CSSMotion */ "./node_modules/rc-animate/es/CSSMotion.js");
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _contextTypes__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contextTypes */ "./node_modules/rc-tree/es/contextTypes.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./util */ "./node_modules/rc-tree/es/util.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}



 // @ts-ignore






var ICON_OPEN = 'open';
var ICON_CLOSE = 'close';
var defaultTitle = '---';

var TreeNode =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TreeNode, _React$Component);

  function TreeNode() {
    var _this;

    _classCallCheck(this, TreeNode);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TreeNode).apply(this, arguments));
    _this.state = {
      dragNodeHighlight: false
    };

    _this.onSelectorClick = function (e) {
      // Click trigger before select/check operation
      var onNodeClick = _this.props.context.onNodeClick;
      onNodeClick(e, _assertThisInitialized(_this));

      if (_this.isSelectable()) {
        _this.onSelect(e);
      } else {
        _this.onCheck(e);
      }
    };

    _this.onSelectorDoubleClick = function (e) {
      var onNodeDoubleClick = _this.props.context.onNodeDoubleClick;
      onNodeDoubleClick(e, _assertThisInitialized(_this));
    };

    _this.onSelect = function (e) {
      if (_this.isDisabled()) return;
      var onNodeSelect = _this.props.context.onNodeSelect;
      e.preventDefault();
      onNodeSelect(e, _assertThisInitialized(_this));
    };

    _this.onCheck = function (e) {
      if (_this.isDisabled()) return;
      var _this$props = _this.props,
          disableCheckbox = _this$props.disableCheckbox,
          checked = _this$props.checked;
      var onNodeCheck = _this.props.context.onNodeCheck;
      if (!_this.isCheckable() || disableCheckbox) return;
      e.preventDefault();
      var targetChecked = !checked;
      onNodeCheck(e, _assertThisInitialized(_this), targetChecked);
    };

    _this.onMouseEnter = function (e) {
      var onNodeMouseEnter = _this.props.context.onNodeMouseEnter;
      onNodeMouseEnter(e, _assertThisInitialized(_this));
    };

    _this.onMouseLeave = function (e) {
      var onNodeMouseLeave = _this.props.context.onNodeMouseLeave;
      onNodeMouseLeave(e, _assertThisInitialized(_this));
    };

    _this.onContextMenu = function (e) {
      var onNodeContextMenu = _this.props.context.onNodeContextMenu;
      onNodeContextMenu(e, _assertThisInitialized(_this));
    };

    _this.onDragStart = function (e) {
      var onNodeDragStart = _this.props.context.onNodeDragStart;
      e.stopPropagation();

      _this.setState({
        dragNodeHighlight: true
      });

      onNodeDragStart(e, _assertThisInitialized(_this));

      try {
        // ie throw error
        // firefox-need-it
        e.dataTransfer.setData('text/plain', '');
      } catch (error) {// empty
      }
    };

    _this.onDragEnter = function (e) {
      var onNodeDragEnter = _this.props.context.onNodeDragEnter;
      e.preventDefault();
      e.stopPropagation();
      onNodeDragEnter(e, _assertThisInitialized(_this));
    };

    _this.onDragOver = function (e) {
      var onNodeDragOver = _this.props.context.onNodeDragOver;
      e.preventDefault();
      e.stopPropagation();
      onNodeDragOver(e, _assertThisInitialized(_this));
    };

    _this.onDragLeave = function (e) {
      var onNodeDragLeave = _this.props.context.onNodeDragLeave;
      e.stopPropagation();
      onNodeDragLeave(e, _assertThisInitialized(_this));
    };

    _this.onDragEnd = function (e) {
      var onNodeDragEnd = _this.props.context.onNodeDragEnd;
      e.stopPropagation();

      _this.setState({
        dragNodeHighlight: false
      });

      onNodeDragEnd(e, _assertThisInitialized(_this));
    };

    _this.onDrop = function (e) {
      var onNodeDrop = _this.props.context.onNodeDrop;
      e.preventDefault();
      e.stopPropagation();

      _this.setState({
        dragNodeHighlight: false
      });

      onNodeDrop(e, _assertThisInitialized(_this));
    }; // Disabled item still can be switch


    _this.onExpand = function (e) {
      var onNodeExpand = _this.props.context.onNodeExpand;
      onNodeExpand(e, _assertThisInitialized(_this));
    }; // Drag usage


    _this.setSelectHandle = function (node) {
      _this.selectHandle = node;
    };

    _this.getNodeChildren = function () {
      var children = _this.props.children;
      var originList = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_4__["default"])(children).filter(function (node) {
        return node;
      });
      var targetList = Object(_util__WEBPACK_IMPORTED_MODULE_7__["getNodeChildren"])(originList);

      if (originList.length !== targetList.length) {
        Object(_util__WEBPACK_IMPORTED_MODULE_7__["warnOnlyTreeNode"])();
      }

      return targetList;
    };

    _this.getNodeState = function () {
      var expanded = _this.props.expanded;

      if (_this.isLeaf()) {
        return null;
      }

      return expanded ? ICON_OPEN : ICON_CLOSE;
    };

    _this.isLeaf = function () {
      var _this$props2 = _this.props,
          isLeaf = _this$props2.isLeaf,
          loaded = _this$props2.loaded;
      var loadData = _this.props.context.loadData;
      var hasChildren = _this.getNodeChildren().length !== 0;

      if (isLeaf === false) {
        return false;
      }

      return isLeaf || !loadData && !hasChildren || loadData && loaded && !hasChildren;
    };

    _this.isDisabled = function () {
      var disabled = _this.props.disabled;
      var treeDisabled = _this.props.context.disabled; // Follow the logic of Selectable

      if (disabled === false) {
        return false;
      }

      return !!(treeDisabled || disabled);
    };

    _this.isCheckable = function () {
      var checkable = _this.props.checkable;
      var treeCheckable = _this.props.context.checkable; // Return false if tree or treeNode is not checkable

      if (!treeCheckable || checkable === false) return false;
      return treeCheckable;
    }; // Load data to avoid default expanded tree without data


    _this.syncLoadData = function (props) {
      var expanded = props.expanded,
          loading = props.loading,
          loaded = props.loaded;
      var _this$props$context = _this.props.context,
          loadData = _this$props$context.loadData,
          onNodeLoad = _this$props$context.onNodeLoad;
      if (loading) return; // read from state to avoid loadData at same time

      if (loadData && expanded && !_this.isLeaf()) {
        // We needn't reload data when has children in sync logic
        // It's only needed in node expanded
        var hasChildren = _this.getNodeChildren().length !== 0;

        if (!hasChildren && !loaded) {
          onNodeLoad(_assertThisInitialized(_this));
        }
      }
    }; // Switcher


    _this.renderSwitcher = function () {
      var _this$props3 = _this.props,
          expanded = _this$props3.expanded,
          switcherIconFromProps = _this$props3.switcherIcon;
      var _this$props$context2 = _this.props.context,
          prefixCls = _this$props$context2.prefixCls,
          switcherIconFromCtx = _this$props$context2.switcherIcon;
      var switcherIcon = switcherIconFromProps || switcherIconFromCtx;

      if (_this.isLeaf()) {
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-switcher"), "".concat(prefixCls, "-switcher-noop"))
        }, typeof switcherIcon === 'function' ? switcherIcon(_objectSpread({}, _this.props, {
          isLeaf: true
        })) : switcherIcon);
      }

      var switcherCls = classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-switcher"), "".concat(prefixCls, "-switcher_").concat(expanded ? ICON_OPEN : ICON_CLOSE));
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        onClick: _this.onExpand,
        className: switcherCls
      }, typeof switcherIcon === 'function' ? switcherIcon(_objectSpread({}, _this.props, {
        isLeaf: false
      })) : switcherIcon);
    }; // Checkbox


    _this.renderCheckbox = function () {
      var _this$props4 = _this.props,
          checked = _this$props4.checked,
          halfChecked = _this$props4.halfChecked,
          disableCheckbox = _this$props4.disableCheckbox;
      var prefixCls = _this.props.context.prefixCls;

      var disabled = _this.isDisabled();

      var checkable = _this.isCheckable();

      if (!checkable) return null; // [Legacy] Custom element should be separate with `checkable` in future

      var $custom = typeof checkable !== 'boolean' ? checkable : null;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-checkbox"), checked && "".concat(prefixCls, "-checkbox-checked"), !checked && halfChecked && "".concat(prefixCls, "-checkbox-indeterminate"), (disabled || disableCheckbox) && "".concat(prefixCls, "-checkbox-disabled")),
        onClick: _this.onCheck
      }, $custom);
    };

    _this.renderIcon = function () {
      var loading = _this.props.loading;
      var prefixCls = _this.props.context.prefixCls;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-iconEle"), "".concat(prefixCls, "-icon__").concat(_this.getNodeState() || 'docu'), loading && "".concat(prefixCls, "-icon_loading"))
      });
    }; // Icon + Title


    _this.renderSelector = function () {
      var dragNodeHighlight = _this.state.dragNodeHighlight;
      var _this$props5 = _this.props,
          title = _this$props5.title,
          selected = _this$props5.selected,
          icon = _this$props5.icon,
          loading = _this$props5.loading;
      var _this$props$context3 = _this.props.context,
          prefixCls = _this$props$context3.prefixCls,
          showIcon = _this$props$context3.showIcon,
          treeIcon = _this$props$context3.icon,
          draggable = _this$props$context3.draggable,
          loadData = _this$props$context3.loadData;

      var disabled = _this.isDisabled();

      var wrapClass = "".concat(prefixCls, "-node-content-wrapper"); // Icon - Still show loading icon when loading without showIcon

      var $icon;

      if (showIcon) {
        var currentIcon = icon || treeIcon;
        $icon = currentIcon ? react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(prefixCls, "-iconEle"), "".concat(prefixCls, "-icon__customize"))
        }, typeof currentIcon === 'function' ? currentIcon(_this.props) : currentIcon) : _this.renderIcon();
      } else if (loadData && loading) {
        $icon = _this.renderIcon();
      } // Title


      var $title = react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        className: "".concat(prefixCls, "-title")
      }, title);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", {
        ref: _this.setSelectHandle,
        title: typeof title === 'string' ? title : '',
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()("".concat(wrapClass), "".concat(wrapClass, "-").concat(_this.getNodeState() || 'normal'), !disabled && (selected || dragNodeHighlight) && "".concat(prefixCls, "-node-selected"), !disabled && draggable && 'draggable'),
        draggable: !disabled && draggable || undefined,
        "aria-grabbed": !disabled && draggable || undefined,
        onMouseEnter: _this.onMouseEnter,
        onMouseLeave: _this.onMouseLeave,
        onContextMenu: _this.onContextMenu,
        onClick: _this.onSelectorClick,
        onDoubleClick: _this.onSelectorDoubleClick,
        onDragStart: draggable ? _this.onDragStart : undefined
      }, $icon, $title);
    }; // Children list wrapped with `Animation`


    _this.renderChildren = function () {
      var _this$props6 = _this.props,
          expanded = _this$props6.expanded,
          pos = _this$props6.pos;
      var _this$props$context4 = _this.props.context,
          prefixCls = _this$props$context4.prefixCls,
          motion = _this$props$context4.motion,
          renderTreeNode = _this$props$context4.renderTreeNode; // Children TreeNode

      var nodeList = _this.getNodeChildren();

      if (nodeList.length === 0) {
        return null;
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](rc_animate_es_CSSMotion__WEBPACK_IMPORTED_MODULE_3__["default"], Object.assign({
        visible: expanded
      }, motion), function (_ref) {
        var style = _ref.style,
            className = _ref.className;
        return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", {
          className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, "".concat(prefixCls, "-child-tree"), expanded && "".concat(prefixCls, "-child-tree-open")),
          style: style,
          "data-expanded": expanded,
          role: "group"
        }, Object(_util__WEBPACK_IMPORTED_MODULE_7__["mapChildren"])(nodeList, function (node, index) {
          return renderTreeNode(node, index, pos);
        }));
      });
    };

    return _this;
  } // Isomorphic needn't load data in server side


  _createClass(TreeNode, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props7 = this.props,
          eventKey = _this$props7.eventKey,
          registerTreeNode = _this$props7.context.registerTreeNode;
      this.syncLoadData(this.props);
      registerTreeNode(eventKey, this);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.syncLoadData(this.props);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this$props8 = this.props,
          eventKey = _this$props8.eventKey,
          registerTreeNode = _this$props8.context.registerTreeNode;
      registerTreeNode(eventKey, null);
    }
  }, {
    key: "isSelectable",
    value: function isSelectable() {
      var selectable = this.props.selectable;
      var treeSelectable = this.props.context.selectable; // Ignore when selectable is undefined or null

      if (typeof selectable === 'boolean') {
        return selectable;
      }

      return treeSelectable;
    }
  }, {
    key: "render",
    value: function render() {
      var _classNames;

      var loading = this.props.loading;

      var _this$props9 = this.props,
          className = _this$props9.className,
          style = _this$props9.style,
          dragOver = _this$props9.dragOver,
          dragOverGapTop = _this$props9.dragOverGapTop,
          dragOverGapBottom = _this$props9.dragOverGapBottom,
          isLeaf = _this$props9.isLeaf,
          expanded = _this$props9.expanded,
          selected = _this$props9.selected,
          checked = _this$props9.checked,
          halfChecked = _this$props9.halfChecked,
          otherProps = _objectWithoutProperties(_this$props9, ["className", "style", "dragOver", "dragOverGapTop", "dragOverGapBottom", "isLeaf", "expanded", "selected", "checked", "halfChecked"]);

      var _this$props$context5 = this.props.context,
          prefixCls = _this$props$context5.prefixCls,
          filterTreeNode = _this$props$context5.filterTreeNode,
          draggable = _this$props$context5.draggable;
      var disabled = this.isDisabled();
      var dataOrAriaAttributeProps = Object(_util__WEBPACK_IMPORTED_MODULE_7__["getDataAndAria"])(otherProps);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", Object.assign({
        className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(className, (_classNames = {}, _defineProperty(_classNames, "".concat(prefixCls, "-treenode-disabled"), disabled), _defineProperty(_classNames, "".concat(prefixCls, "-treenode-switcher-").concat(expanded ? 'open' : 'close'), !isLeaf), _defineProperty(_classNames, "".concat(prefixCls, "-treenode-checkbox-checked"), checked), _defineProperty(_classNames, "".concat(prefixCls, "-treenode-checkbox-indeterminate"), halfChecked), _defineProperty(_classNames, "".concat(prefixCls, "-treenode-selected"), selected), _defineProperty(_classNames, "".concat(prefixCls, "-treenode-loading"), loading), _defineProperty(_classNames, 'drag-over', !disabled && dragOver), _defineProperty(_classNames, 'drag-over-gap-top', !disabled && dragOverGapTop), _defineProperty(_classNames, 'drag-over-gap-bottom', !disabled && dragOverGapBottom), _defineProperty(_classNames, 'filter-node', filterTreeNode && filterTreeNode(this)), _classNames)),
        style: style,
        role: "treeitem",
        onDragEnter: draggable ? this.onDragEnter : undefined,
        onDragOver: draggable ? this.onDragOver : undefined,
        onDragLeave: draggable ? this.onDragLeave : undefined,
        onDrop: draggable ? this.onDrop : undefined,
        onDragEnd: draggable ? this.onDragEnd : undefined
      }, dataOrAriaAttributeProps), this.renderSwitcher(), this.renderCheckbox(), this.renderSelector(), this.renderChildren());
    }
  }]);

  return TreeNode;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

TreeNode.propTypes = {
  eventKey: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  prefixCls: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  className: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  style: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object,
  onSelect: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func,
  // By parent
  expanded: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  selected: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  checked: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  loaded: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  loading: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  halfChecked: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  children: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  title: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node,
  pos: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  dragOver: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  dragOverGapTop: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  dragOverGapBottom: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  // By user
  isLeaf: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  checkable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  selectable: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  disabled: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  disableCheckbox: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.bool,
  icon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func]),
  switcherIcon: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.node, prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.func])
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__["polyfill"])(TreeNode);

var ContextTreeNode = function ContextTreeNode(props) {
  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_contextTypes__WEBPACK_IMPORTED_MODULE_6__["TreeContext"].Consumer, null, function (context) {
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](TreeNode, Object.assign({}, props, {
      context: context
    }));
  });
};

ContextTreeNode.defaultProps = {
  title: defaultTitle
};
ContextTreeNode.isTreeNode = 1;

/* harmony default export */ __webpack_exports__["default"] = (ContextTreeNode);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS9lcy9UcmVlTm9kZS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRyZWUvZXMvVHJlZU5vZGUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiKSB7IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfTsgfSBlbHNlIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9OyB9IHJldHVybiBfdHlwZW9mKG9iaik7IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKTsgdmFyIGtleSwgaTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHNvdXJjZVN5bWJvbEtleXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHNvdXJjZSk7IGZvciAoaSA9IDA7IGkgPCBzb3VyY2VTeW1ib2xLZXlzLmxlbmd0aDsgaSsrKSB7IGtleSA9IHNvdXJjZVN5bWJvbEtleXNbaV07IGlmIChleGNsdWRlZC5pbmRleE9mKGtleSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHNvdXJjZSwga2V5KSkgY29udGludWU7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllc0xvb3NlKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSB7fTsgdmFyIHNvdXJjZUtleXMgPSBPYmplY3Qua2V5cyhzb3VyY2UpOyB2YXIga2V5LCBpOyBmb3IgKGkgPSAwOyBpIDwgc291cmNlS2V5cy5sZW5ndGg7IGkrKykgeyBrZXkgPSBzb3VyY2VLZXlzW2ldOyBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlOyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKHNvdXJjZSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKHNvdXJjZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfVxuXG5mdW5jdGlvbiBfY3JlYXRlQ2xhc3MoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBfZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7IHJldHVybiBjYWxsOyB9IHJldHVybiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpOyB9XG5cbmZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IF9nZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5nZXRQcm90b3R5cGVPZiA6IGZ1bmN0aW9uIF9nZXRQcm90b3R5cGVPZihvKSB7IHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7IH07IHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7IH1cblxuZnVuY3Rpb24gX2Fzc2VydFRoaXNJbml0aWFsaXplZChzZWxmKSB7IGlmIChzZWxmID09PSB2b2lkIDApIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBfc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpOyB9XG5cbmZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IF9zZXRQcm90b3R5cGVPZiA9IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCBmdW5jdGlvbiBfc2V0UHJvdG90eXBlT2YobywgcCkgeyBvLl9fcHJvdG9fXyA9IHA7IHJldHVybiBvOyB9OyByZXR1cm4gX3NldFByb3RvdHlwZU9mKG8sIHApOyB9XG5cbmltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJzsgLy8gQHRzLWlnbm9yZVxuXG5pbXBvcnQgQ1NTTW90aW9uIGZyb20gXCJyYy1hbmltYXRlL2VzL0NTU01vdGlvblwiO1xuaW1wb3J0IHRvQXJyYXkgZnJvbSBcInJjLXV0aWwvZXMvQ2hpbGRyZW4vdG9BcnJheVwiO1xuaW1wb3J0IHsgcG9seWZpbGwgfSBmcm9tICdyZWFjdC1saWZlY3ljbGVzLWNvbXBhdCc7XG5pbXBvcnQgeyBUcmVlQ29udGV4dCB9IGZyb20gJy4vY29udGV4dFR5cGVzJztcbmltcG9ydCB7IGdldE5vZGVDaGlsZHJlbiwgZ2V0RGF0YUFuZEFyaWEsIG1hcENoaWxkcmVuLCB3YXJuT25seVRyZWVOb2RlIH0gZnJvbSAnLi91dGlsJztcbnZhciBJQ09OX09QRU4gPSAnb3Blbic7XG52YXIgSUNPTl9DTE9TRSA9ICdjbG9zZSc7XG52YXIgZGVmYXVsdFRpdGxlID0gJy0tLSc7XG5cbnZhciBUcmVlTm9kZSA9XG4vKiNfX1BVUkVfXyovXG5mdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoVHJlZU5vZGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRyZWVOb2RlKCkge1xuICAgIHZhciBfdGhpcztcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBUcmVlTm9kZSk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihUcmVlTm9kZSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBkcmFnTm9kZUhpZ2hsaWdodDogZmFsc2VcbiAgICB9O1xuXG4gICAgX3RoaXMub25TZWxlY3RvckNsaWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIC8vIENsaWNrIHRyaWdnZXIgYmVmb3JlIHNlbGVjdC9jaGVjayBvcGVyYXRpb25cbiAgICAgIHZhciBvbk5vZGVDbGljayA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlQ2xpY2s7XG4gICAgICBvbk5vZGVDbGljayhlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG5cbiAgICAgIGlmIChfdGhpcy5pc1NlbGVjdGFibGUoKSkge1xuICAgICAgICBfdGhpcy5vblNlbGVjdChlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIF90aGlzLm9uQ2hlY2soZSk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIF90aGlzLm9uU2VsZWN0b3JEb3VibGVDbGljayA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgb25Ob2RlRG91YmxlQ2xpY2sgPSBfdGhpcy5wcm9wcy5jb250ZXh0Lm9uTm9kZURvdWJsZUNsaWNrO1xuICAgICAgb25Ob2RlRG91YmxlQ2xpY2soZSwgX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcykpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vblNlbGVjdCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBpZiAoX3RoaXMuaXNEaXNhYmxlZCgpKSByZXR1cm47XG4gICAgICB2YXIgb25Ob2RlU2VsZWN0ID0gX3RoaXMucHJvcHMuY29udGV4dC5vbk5vZGVTZWxlY3Q7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBvbk5vZGVTZWxlY3QoZSwgX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcykpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkNoZWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIGlmIChfdGhpcy5pc0Rpc2FibGVkKCkpIHJldHVybjtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGRpc2FibGVDaGVja2JveCA9IF90aGlzJHByb3BzLmRpc2FibGVDaGVja2JveCxcbiAgICAgICAgICBjaGVja2VkID0gX3RoaXMkcHJvcHMuY2hlY2tlZDtcbiAgICAgIHZhciBvbk5vZGVDaGVjayA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlQ2hlY2s7XG4gICAgICBpZiAoIV90aGlzLmlzQ2hlY2thYmxlKCkgfHwgZGlzYWJsZUNoZWNrYm94KSByZXR1cm47XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB2YXIgdGFyZ2V0Q2hlY2tlZCA9ICFjaGVja2VkO1xuICAgICAgb25Ob2RlQ2hlY2soZSwgX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksIHRhcmdldENoZWNrZWQpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbk1vdXNlRW50ZXIgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIG9uTm9kZU1vdXNlRW50ZXIgPSBfdGhpcy5wcm9wcy5jb250ZXh0Lm9uTm9kZU1vdXNlRW50ZXI7XG4gICAgICBvbk5vZGVNb3VzZUVudGVyKGUsIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25Nb3VzZUxlYXZlID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBvbk5vZGVNb3VzZUxlYXZlID0gX3RoaXMucHJvcHMuY29udGV4dC5vbk5vZGVNb3VzZUxlYXZlO1xuICAgICAgb25Ob2RlTW91c2VMZWF2ZShlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uQ29udGV4dE1lbnUgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIG9uTm9kZUNvbnRleHRNZW51ID0gX3RoaXMucHJvcHMuY29udGV4dC5vbk5vZGVDb250ZXh0TWVudTtcbiAgICAgIG9uTm9kZUNvbnRleHRNZW51KGUsIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25EcmFnU3RhcnQgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIG9uTm9kZURyYWdTdGFydCA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlRHJhZ1N0YXJ0O1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBkcmFnTm9kZUhpZ2hsaWdodDogdHJ1ZVxuICAgICAgfSk7XG5cbiAgICAgIG9uTm9kZURyYWdTdGFydChlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG5cbiAgICAgIHRyeSB7XG4gICAgICAgIC8vIGllIHRocm93IGVycm9yXG4gICAgICAgIC8vIGZpcmVmb3gtbmVlZC1pdFxuICAgICAgICBlLmRhdGFUcmFuc2Zlci5zZXREYXRhKCd0ZXh0L3BsYWluJywgJycpO1xuICAgICAgfSBjYXRjaCAoZXJyb3IpIHsvLyBlbXB0eVxuICAgICAgfVxuICAgIH07XG5cbiAgICBfdGhpcy5vbkRyYWdFbnRlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgb25Ob2RlRHJhZ0VudGVyID0gX3RoaXMucHJvcHMuY29udGV4dC5vbk5vZGVEcmFnRW50ZXI7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgb25Ob2RlRHJhZ0VudGVyKGUsIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25EcmFnT3ZlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgb25Ob2RlRHJhZ092ZXIgPSBfdGhpcy5wcm9wcy5jb250ZXh0Lm9uTm9kZURyYWdPdmVyO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIG9uTm9kZURyYWdPdmVyKGUsIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICB9O1xuXG4gICAgX3RoaXMub25EcmFnTGVhdmUgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIG9uTm9kZURyYWdMZWF2ZSA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlRHJhZ0xlYXZlO1xuICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIG9uTm9kZURyYWdMZWF2ZShlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uRHJhZ0VuZCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgb25Ob2RlRHJhZ0VuZCA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlRHJhZ0VuZDtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZHJhZ05vZGVIaWdobGlnaHQ6IGZhbHNlXG4gICAgICB9KTtcblxuICAgICAgb25Ob2RlRHJhZ0VuZChlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uRHJvcCA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICB2YXIgb25Ob2RlRHJvcCA9IF90aGlzLnByb3BzLmNvbnRleHQub25Ob2RlRHJvcDtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgZHJhZ05vZGVIaWdobGlnaHQ6IGZhbHNlXG4gICAgICB9KTtcblxuICAgICAgb25Ob2RlRHJvcChlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG4gICAgfTsgLy8gRGlzYWJsZWQgaXRlbSBzdGlsbCBjYW4gYmUgc3dpdGNoXG5cblxuICAgIF90aGlzLm9uRXhwYW5kID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBvbk5vZGVFeHBhbmQgPSBfdGhpcy5wcm9wcy5jb250ZXh0Lm9uTm9kZUV4cGFuZDtcbiAgICAgIG9uTm9kZUV4cGFuZChlLCBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKF90aGlzKSk7XG4gICAgfTsgLy8gRHJhZyB1c2FnZVxuXG5cbiAgICBfdGhpcy5zZXRTZWxlY3RIYW5kbGUgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgX3RoaXMuc2VsZWN0SGFuZGxlID0gbm9kZTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0Tm9kZUNoaWxkcmVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGNoaWxkcmVuID0gX3RoaXMucHJvcHMuY2hpbGRyZW47XG4gICAgICB2YXIgb3JpZ2luTGlzdCA9IHRvQXJyYXkoY2hpbGRyZW4pLmZpbHRlcihmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgIH0pO1xuICAgICAgdmFyIHRhcmdldExpc3QgPSBnZXROb2RlQ2hpbGRyZW4ob3JpZ2luTGlzdCk7XG5cbiAgICAgIGlmIChvcmlnaW5MaXN0Lmxlbmd0aCAhPT0gdGFyZ2V0TGlzdC5sZW5ndGgpIHtcbiAgICAgICAgd2Fybk9ubHlUcmVlTm9kZSgpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGFyZ2V0TGlzdDtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0Tm9kZVN0YXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGV4cGFuZGVkID0gX3RoaXMucHJvcHMuZXhwYW5kZWQ7XG5cbiAgICAgIGlmIChfdGhpcy5pc0xlYWYoKSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGV4cGFuZGVkID8gSUNPTl9PUEVOIDogSUNPTl9DTE9TRTtcbiAgICB9O1xuXG4gICAgX3RoaXMuaXNMZWFmID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGlzTGVhZiA9IF90aGlzJHByb3BzMi5pc0xlYWYsXG4gICAgICAgICAgbG9hZGVkID0gX3RoaXMkcHJvcHMyLmxvYWRlZDtcbiAgICAgIHZhciBsb2FkRGF0YSA9IF90aGlzLnByb3BzLmNvbnRleHQubG9hZERhdGE7XG4gICAgICB2YXIgaGFzQ2hpbGRyZW4gPSBfdGhpcy5nZXROb2RlQ2hpbGRyZW4oKS5sZW5ndGggIT09IDA7XG5cbiAgICAgIGlmIChpc0xlYWYgPT09IGZhbHNlKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGlzTGVhZiB8fCAhbG9hZERhdGEgJiYgIWhhc0NoaWxkcmVuIHx8IGxvYWREYXRhICYmIGxvYWRlZCAmJiAhaGFzQ2hpbGRyZW47XG4gICAgfTtcblxuICAgIF90aGlzLmlzRGlzYWJsZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgZGlzYWJsZWQgPSBfdGhpcy5wcm9wcy5kaXNhYmxlZDtcbiAgICAgIHZhciB0cmVlRGlzYWJsZWQgPSBfdGhpcy5wcm9wcy5jb250ZXh0LmRpc2FibGVkOyAvLyBGb2xsb3cgdGhlIGxvZ2ljIG9mIFNlbGVjdGFibGVcblxuICAgICAgaWYgKGRpc2FibGVkID09PSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAhISh0cmVlRGlzYWJsZWQgfHwgZGlzYWJsZWQpO1xuICAgIH07XG5cbiAgICBfdGhpcy5pc0NoZWNrYWJsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBjaGVja2FibGUgPSBfdGhpcy5wcm9wcy5jaGVja2FibGU7XG4gICAgICB2YXIgdHJlZUNoZWNrYWJsZSA9IF90aGlzLnByb3BzLmNvbnRleHQuY2hlY2thYmxlOyAvLyBSZXR1cm4gZmFsc2UgaWYgdHJlZSBvciB0cmVlTm9kZSBpcyBub3QgY2hlY2thYmxlXG5cbiAgICAgIGlmICghdHJlZUNoZWNrYWJsZSB8fCBjaGVja2FibGUgPT09IGZhbHNlKSByZXR1cm4gZmFsc2U7XG4gICAgICByZXR1cm4gdHJlZUNoZWNrYWJsZTtcbiAgICB9OyAvLyBMb2FkIGRhdGEgdG8gYXZvaWQgZGVmYXVsdCBleHBhbmRlZCB0cmVlIHdpdGhvdXQgZGF0YVxuXG5cbiAgICBfdGhpcy5zeW5jTG9hZERhdGEgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICAgIHZhciBleHBhbmRlZCA9IHByb3BzLmV4cGFuZGVkLFxuICAgICAgICAgIGxvYWRpbmcgPSBwcm9wcy5sb2FkaW5nLFxuICAgICAgICAgIGxvYWRlZCA9IHByb3BzLmxvYWRlZDtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyRjb250ZXh0ID0gX3RoaXMucHJvcHMuY29udGV4dCxcbiAgICAgICAgICBsb2FkRGF0YSA9IF90aGlzJHByb3BzJGNvbnRleHQubG9hZERhdGEsXG4gICAgICAgICAgb25Ob2RlTG9hZCA9IF90aGlzJHByb3BzJGNvbnRleHQub25Ob2RlTG9hZDtcbiAgICAgIGlmIChsb2FkaW5nKSByZXR1cm47IC8vIHJlYWQgZnJvbSBzdGF0ZSB0byBhdm9pZCBsb2FkRGF0YSBhdCBzYW1lIHRpbWVcblxuICAgICAgaWYgKGxvYWREYXRhICYmIGV4cGFuZGVkICYmICFfdGhpcy5pc0xlYWYoKSkge1xuICAgICAgICAvLyBXZSBuZWVkbid0IHJlbG9hZCBkYXRhIHdoZW4gaGFzIGNoaWxkcmVuIGluIHN5bmMgbG9naWNcbiAgICAgICAgLy8gSXQncyBvbmx5IG5lZWRlZCBpbiBub2RlIGV4cGFuZGVkXG4gICAgICAgIHZhciBoYXNDaGlsZHJlbiA9IF90aGlzLmdldE5vZGVDaGlsZHJlbigpLmxlbmd0aCAhPT0gMDtcblxuICAgICAgICBpZiAoIWhhc0NoaWxkcmVuICYmICFsb2FkZWQpIHtcbiAgICAgICAgICBvbk5vZGVMb2FkKF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoX3RoaXMpKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07IC8vIFN3aXRjaGVyXG5cblxuICAgIF90aGlzLnJlbmRlclN3aXRjaGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGV4cGFuZGVkID0gX3RoaXMkcHJvcHMzLmV4cGFuZGVkLFxuICAgICAgICAgIHN3aXRjaGVySWNvbkZyb21Qcm9wcyA9IF90aGlzJHByb3BzMy5zd2l0Y2hlckljb247XG4gICAgICB2YXIgX3RoaXMkcHJvcHMkY29udGV4dDIgPSBfdGhpcy5wcm9wcy5jb250ZXh0LFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzJGNvbnRleHQyLnByZWZpeENscyxcbiAgICAgICAgICBzd2l0Y2hlckljb25Gcm9tQ3R4ID0gX3RoaXMkcHJvcHMkY29udGV4dDIuc3dpdGNoZXJJY29uO1xuICAgICAgdmFyIHN3aXRjaGVySWNvbiA9IHN3aXRjaGVySWNvbkZyb21Qcm9wcyB8fCBzd2l0Y2hlckljb25Gcm9tQ3R4O1xuXG4gICAgICBpZiAoX3RoaXMuaXNMZWFmKCkpIHtcbiAgICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZXMoXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zd2l0Y2hlclwiKSwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zd2l0Y2hlci1ub29wXCIpKVxuICAgICAgICB9LCB0eXBlb2Ygc3dpdGNoZXJJY29uID09PSAnZnVuY3Rpb24nID8gc3dpdGNoZXJJY29uKF9vYmplY3RTcHJlYWQoe30sIF90aGlzLnByb3BzLCB7XG4gICAgICAgICAgaXNMZWFmOiB0cnVlXG4gICAgICAgIH0pKSA6IHN3aXRjaGVySWNvbik7XG4gICAgICB9XG5cbiAgICAgIHZhciBzd2l0Y2hlckNscyA9IGNsYXNzTmFtZXMoXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zd2l0Y2hlclwiKSwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1zd2l0Y2hlcl9cIikuY29uY2F0KGV4cGFuZGVkID8gSUNPTl9PUEVOIDogSUNPTl9DTE9TRSkpO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgb25DbGljazogX3RoaXMub25FeHBhbmQsXG4gICAgICAgIGNsYXNzTmFtZTogc3dpdGNoZXJDbHNcbiAgICAgIH0sIHR5cGVvZiBzd2l0Y2hlckljb24gPT09ICdmdW5jdGlvbicgPyBzd2l0Y2hlckljb24oX29iamVjdFNwcmVhZCh7fSwgX3RoaXMucHJvcHMsIHtcbiAgICAgICAgaXNMZWFmOiBmYWxzZVxuICAgICAgfSkpIDogc3dpdGNoZXJJY29uKTtcbiAgICB9OyAvLyBDaGVja2JveFxuXG5cbiAgICBfdGhpcy5yZW5kZXJDaGVja2JveCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczQgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGVja2VkID0gX3RoaXMkcHJvcHM0LmNoZWNrZWQsXG4gICAgICAgICAgaGFsZkNoZWNrZWQgPSBfdGhpcyRwcm9wczQuaGFsZkNoZWNrZWQsXG4gICAgICAgICAgZGlzYWJsZUNoZWNrYm94ID0gX3RoaXMkcHJvcHM0LmRpc2FibGVDaGVja2JveDtcbiAgICAgIHZhciBwcmVmaXhDbHMgPSBfdGhpcy5wcm9wcy5jb250ZXh0LnByZWZpeENscztcblxuICAgICAgdmFyIGRpc2FibGVkID0gX3RoaXMuaXNEaXNhYmxlZCgpO1xuXG4gICAgICB2YXIgY2hlY2thYmxlID0gX3RoaXMuaXNDaGVja2FibGUoKTtcblxuICAgICAgaWYgKCFjaGVja2FibGUpIHJldHVybiBudWxsOyAvLyBbTGVnYWN5XSBDdXN0b20gZWxlbWVudCBzaG91bGQgYmUgc2VwYXJhdGUgd2l0aCBgY2hlY2thYmxlYCBpbiBmdXR1cmVcblxuICAgICAgdmFyICRjdXN0b20gPSB0eXBlb2YgY2hlY2thYmxlICE9PSAnYm9vbGVhbicgPyBjaGVja2FibGUgOiBudWxsO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKFwiXCIuY29uY2F0KHByZWZpeENscywgXCItY2hlY2tib3hcIiksIGNoZWNrZWQgJiYgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jaGVja2JveC1jaGVja2VkXCIpLCAhY2hlY2tlZCAmJiBoYWxmQ2hlY2tlZCAmJiBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWNoZWNrYm94LWluZGV0ZXJtaW5hdGVcIiksIChkaXNhYmxlZCB8fCBkaXNhYmxlQ2hlY2tib3gpICYmIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItY2hlY2tib3gtZGlzYWJsZWRcIikpLFxuICAgICAgICBvbkNsaWNrOiBfdGhpcy5vbkNoZWNrXG4gICAgICB9LCAkY3VzdG9tKTtcbiAgICB9O1xuXG4gICAgX3RoaXMucmVuZGVySWNvbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBsb2FkaW5nID0gX3RoaXMucHJvcHMubG9hZGluZztcbiAgICAgIHZhciBwcmVmaXhDbHMgPSBfdGhpcy5wcm9wcy5jb250ZXh0LnByZWZpeENscztcbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWljb25FbGVcIiksIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItaWNvbl9fXCIpLmNvbmNhdChfdGhpcy5nZXROb2RlU3RhdGUoKSB8fCAnZG9jdScpLCBsb2FkaW5nICYmIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItaWNvbl9sb2FkaW5nXCIpKVxuICAgICAgfSk7XG4gICAgfTsgLy8gSWNvbiArIFRpdGxlXG5cblxuICAgIF90aGlzLnJlbmRlclNlbGVjdG9yID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGRyYWdOb2RlSGlnaGxpZ2h0ID0gX3RoaXMuc3RhdGUuZHJhZ05vZGVIaWdobGlnaHQ7XG4gICAgICB2YXIgX3RoaXMkcHJvcHM1ID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdGl0bGUgPSBfdGhpcyRwcm9wczUudGl0bGUsXG4gICAgICAgICAgc2VsZWN0ZWQgPSBfdGhpcyRwcm9wczUuc2VsZWN0ZWQsXG4gICAgICAgICAgaWNvbiA9IF90aGlzJHByb3BzNS5pY29uLFxuICAgICAgICAgIGxvYWRpbmcgPSBfdGhpcyRwcm9wczUubG9hZGluZztcbiAgICAgIHZhciBfdGhpcyRwcm9wcyRjb250ZXh0MyA9IF90aGlzLnByb3BzLmNvbnRleHQsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHMkY29udGV4dDMucHJlZml4Q2xzLFxuICAgICAgICAgIHNob3dJY29uID0gX3RoaXMkcHJvcHMkY29udGV4dDMuc2hvd0ljb24sXG4gICAgICAgICAgdHJlZUljb24gPSBfdGhpcyRwcm9wcyRjb250ZXh0My5pY29uLFxuICAgICAgICAgIGRyYWdnYWJsZSA9IF90aGlzJHByb3BzJGNvbnRleHQzLmRyYWdnYWJsZSxcbiAgICAgICAgICBsb2FkRGF0YSA9IF90aGlzJHByb3BzJGNvbnRleHQzLmxvYWREYXRhO1xuXG4gICAgICB2YXIgZGlzYWJsZWQgPSBfdGhpcy5pc0Rpc2FibGVkKCk7XG5cbiAgICAgIHZhciB3cmFwQ2xhc3MgPSBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLW5vZGUtY29udGVudC13cmFwcGVyXCIpOyAvLyBJY29uIC0gU3RpbGwgc2hvdyBsb2FkaW5nIGljb24gd2hlbiBsb2FkaW5nIHdpdGhvdXQgc2hvd0ljb25cblxuICAgICAgdmFyICRpY29uO1xuXG4gICAgICBpZiAoc2hvd0ljb24pIHtcbiAgICAgICAgdmFyIGN1cnJlbnRJY29uID0gaWNvbiB8fCB0cmVlSWNvbjtcbiAgICAgICAgJGljb24gPSBjdXJyZW50SWNvbiA/IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZXMoXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1pY29uRWxlXCIpLCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLWljb25fX2N1c3RvbWl6ZVwiKSlcbiAgICAgICAgfSwgdHlwZW9mIGN1cnJlbnRJY29uID09PSAnZnVuY3Rpb24nID8gY3VycmVudEljb24oX3RoaXMucHJvcHMpIDogY3VycmVudEljb24pIDogX3RoaXMucmVuZGVySWNvbigpO1xuICAgICAgfSBlbHNlIGlmIChsb2FkRGF0YSAmJiBsb2FkaW5nKSB7XG4gICAgICAgICRpY29uID0gX3RoaXMucmVuZGVySWNvbigpO1xuICAgICAgfSAvLyBUaXRsZVxuXG5cbiAgICAgIHZhciAkdGl0bGUgPSBSZWFjdC5jcmVhdGVFbGVtZW50KFwic3BhblwiLCB7XG4gICAgICAgIGNsYXNzTmFtZTogXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10aXRsZVwiKVxuICAgICAgfSwgdGl0bGUpO1xuICAgICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIsIHtcbiAgICAgICAgcmVmOiBfdGhpcy5zZXRTZWxlY3RIYW5kbGUsXG4gICAgICAgIHRpdGxlOiB0eXBlb2YgdGl0bGUgPT09ICdzdHJpbmcnID8gdGl0bGUgOiAnJyxcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKFwiXCIuY29uY2F0KHdyYXBDbGFzcyksIFwiXCIuY29uY2F0KHdyYXBDbGFzcywgXCItXCIpLmNvbmNhdChfdGhpcy5nZXROb2RlU3RhdGUoKSB8fCAnbm9ybWFsJyksICFkaXNhYmxlZCAmJiAoc2VsZWN0ZWQgfHwgZHJhZ05vZGVIaWdobGlnaHQpICYmIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItbm9kZS1zZWxlY3RlZFwiKSwgIWRpc2FibGVkICYmIGRyYWdnYWJsZSAmJiAnZHJhZ2dhYmxlJyksXG4gICAgICAgIGRyYWdnYWJsZTogIWRpc2FibGVkICYmIGRyYWdnYWJsZSB8fCB1bmRlZmluZWQsXG4gICAgICAgIFwiYXJpYS1ncmFiYmVkXCI6ICFkaXNhYmxlZCAmJiBkcmFnZ2FibGUgfHwgdW5kZWZpbmVkLFxuICAgICAgICBvbk1vdXNlRW50ZXI6IF90aGlzLm9uTW91c2VFbnRlcixcbiAgICAgICAgb25Nb3VzZUxlYXZlOiBfdGhpcy5vbk1vdXNlTGVhdmUsXG4gICAgICAgIG9uQ29udGV4dE1lbnU6IF90aGlzLm9uQ29udGV4dE1lbnUsXG4gICAgICAgIG9uQ2xpY2s6IF90aGlzLm9uU2VsZWN0b3JDbGljayxcbiAgICAgICAgb25Eb3VibGVDbGljazogX3RoaXMub25TZWxlY3RvckRvdWJsZUNsaWNrLFxuICAgICAgICBvbkRyYWdTdGFydDogZHJhZ2dhYmxlID8gX3RoaXMub25EcmFnU3RhcnQgOiB1bmRlZmluZWRcbiAgICAgIH0sICRpY29uLCAkdGl0bGUpO1xuICAgIH07IC8vIENoaWxkcmVuIGxpc3Qgd3JhcHBlZCB3aXRoIGBBbmltYXRpb25gXG5cblxuICAgIF90aGlzLnJlbmRlckNoaWxkcmVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzNiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGV4cGFuZGVkID0gX3RoaXMkcHJvcHM2LmV4cGFuZGVkLFxuICAgICAgICAgIHBvcyA9IF90aGlzJHByb3BzNi5wb3M7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMkY29udGV4dDQgPSBfdGhpcy5wcm9wcy5jb250ZXh0LFxuICAgICAgICAgIHByZWZpeENscyA9IF90aGlzJHByb3BzJGNvbnRleHQ0LnByZWZpeENscyxcbiAgICAgICAgICBtb3Rpb24gPSBfdGhpcyRwcm9wcyRjb250ZXh0NC5tb3Rpb24sXG4gICAgICAgICAgcmVuZGVyVHJlZU5vZGUgPSBfdGhpcyRwcm9wcyRjb250ZXh0NC5yZW5kZXJUcmVlTm9kZTsgLy8gQ2hpbGRyZW4gVHJlZU5vZGVcblxuICAgICAgdmFyIG5vZGVMaXN0ID0gX3RoaXMuZ2V0Tm9kZUNoaWxkcmVuKCk7XG5cbiAgICAgIGlmIChub2RlTGlzdC5sZW5ndGggPT09IDApIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KENTU01vdGlvbiwgT2JqZWN0LmFzc2lnbih7XG4gICAgICAgIHZpc2libGU6IGV4cGFuZGVkXG4gICAgICB9LCBtb3Rpb24pLCBmdW5jdGlvbiAoX3JlZikge1xuICAgICAgICB2YXIgc3R5bGUgPSBfcmVmLnN0eWxlLFxuICAgICAgICAgICAgY2xhc3NOYW1lID0gX3JlZi5jbGFzc05hbWU7XG4gICAgICAgIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KFwidWxcIiwge1xuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lcyhjbGFzc05hbWUsIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItY2hpbGQtdHJlZVwiKSwgZXhwYW5kZWQgJiYgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1jaGlsZC10cmVlLW9wZW5cIikpLFxuICAgICAgICAgIHN0eWxlOiBzdHlsZSxcbiAgICAgICAgICBcImRhdGEtZXhwYW5kZWRcIjogZXhwYW5kZWQsXG4gICAgICAgICAgcm9sZTogXCJncm91cFwiXG4gICAgICAgIH0sIG1hcENoaWxkcmVuKG5vZGVMaXN0LCBmdW5jdGlvbiAobm9kZSwgaW5kZXgpIHtcbiAgICAgICAgICByZXR1cm4gcmVuZGVyVHJlZU5vZGUobm9kZSwgaW5kZXgsIHBvcyk7XG4gICAgICAgIH0pKTtcbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICByZXR1cm4gX3RoaXM7XG4gIH0gLy8gSXNvbW9ycGhpYyBuZWVkbid0IGxvYWQgZGF0YSBpbiBzZXJ2ZXIgc2lkZVxuXG5cbiAgX2NyZWF0ZUNsYXNzKFRyZWVOb2RlLCBbe1xuICAgIGtleTogXCJjb21wb25lbnREaWRNb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczcgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGV2ZW50S2V5ID0gX3RoaXMkcHJvcHM3LmV2ZW50S2V5LFxuICAgICAgICAgIHJlZ2lzdGVyVHJlZU5vZGUgPSBfdGhpcyRwcm9wczcuY29udGV4dC5yZWdpc3RlclRyZWVOb2RlO1xuICAgICAgdGhpcy5zeW5jTG9hZERhdGEodGhpcy5wcm9wcyk7XG4gICAgICByZWdpc3RlclRyZWVOb2RlKGV2ZW50S2V5LCB0aGlzKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkVXBkYXRlXCIsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIHRoaXMuc3luY0xvYWREYXRhKHRoaXMucHJvcHMpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJjb21wb25lbnRXaWxsVW5tb3VudFwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczggPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGV2ZW50S2V5ID0gX3RoaXMkcHJvcHM4LmV2ZW50S2V5LFxuICAgICAgICAgIHJlZ2lzdGVyVHJlZU5vZGUgPSBfdGhpcyRwcm9wczguY29udGV4dC5yZWdpc3RlclRyZWVOb2RlO1xuICAgICAgcmVnaXN0ZXJUcmVlTm9kZShldmVudEtleSwgbnVsbCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImlzU2VsZWN0YWJsZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc1NlbGVjdGFibGUoKSB7XG4gICAgICB2YXIgc2VsZWN0YWJsZSA9IHRoaXMucHJvcHMuc2VsZWN0YWJsZTtcbiAgICAgIHZhciB0cmVlU2VsZWN0YWJsZSA9IHRoaXMucHJvcHMuY29udGV4dC5zZWxlY3RhYmxlOyAvLyBJZ25vcmUgd2hlbiBzZWxlY3RhYmxlIGlzIHVuZGVmaW5lZCBvciBudWxsXG5cbiAgICAgIGlmICh0eXBlb2Ygc2VsZWN0YWJsZSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgIHJldHVybiBzZWxlY3RhYmxlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJlZVNlbGVjdGFibGU7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcInJlbmRlclwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBsb2FkaW5nID0gdGhpcy5wcm9wcy5sb2FkaW5nO1xuXG4gICAgICB2YXIgX3RoaXMkcHJvcHM5ID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfdGhpcyRwcm9wczkuY2xhc3NOYW1lLFxuICAgICAgICAgIHN0eWxlID0gX3RoaXMkcHJvcHM5LnN0eWxlLFxuICAgICAgICAgIGRyYWdPdmVyID0gX3RoaXMkcHJvcHM5LmRyYWdPdmVyLFxuICAgICAgICAgIGRyYWdPdmVyR2FwVG9wID0gX3RoaXMkcHJvcHM5LmRyYWdPdmVyR2FwVG9wLFxuICAgICAgICAgIGRyYWdPdmVyR2FwQm90dG9tID0gX3RoaXMkcHJvcHM5LmRyYWdPdmVyR2FwQm90dG9tLFxuICAgICAgICAgIGlzTGVhZiA9IF90aGlzJHByb3BzOS5pc0xlYWYsXG4gICAgICAgICAgZXhwYW5kZWQgPSBfdGhpcyRwcm9wczkuZXhwYW5kZWQsXG4gICAgICAgICAgc2VsZWN0ZWQgPSBfdGhpcyRwcm9wczkuc2VsZWN0ZWQsXG4gICAgICAgICAgY2hlY2tlZCA9IF90aGlzJHByb3BzOS5jaGVja2VkLFxuICAgICAgICAgIGhhbGZDaGVja2VkID0gX3RoaXMkcHJvcHM5LmhhbGZDaGVja2VkLFxuICAgICAgICAgIG90aGVyUHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3RoaXMkcHJvcHM5LCBbXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiLCBcImRyYWdPdmVyXCIsIFwiZHJhZ092ZXJHYXBUb3BcIiwgXCJkcmFnT3ZlckdhcEJvdHRvbVwiLCBcImlzTGVhZlwiLCBcImV4cGFuZGVkXCIsIFwic2VsZWN0ZWRcIiwgXCJjaGVja2VkXCIsIFwiaGFsZkNoZWNrZWRcIl0pO1xuXG4gICAgICB2YXIgX3RoaXMkcHJvcHMkY29udGV4dDUgPSB0aGlzLnByb3BzLmNvbnRleHQsXG4gICAgICAgICAgcHJlZml4Q2xzID0gX3RoaXMkcHJvcHMkY29udGV4dDUucHJlZml4Q2xzLFxuICAgICAgICAgIGZpbHRlclRyZWVOb2RlID0gX3RoaXMkcHJvcHMkY29udGV4dDUuZmlsdGVyVHJlZU5vZGUsXG4gICAgICAgICAgZHJhZ2dhYmxlID0gX3RoaXMkcHJvcHMkY29udGV4dDUuZHJhZ2dhYmxlO1xuICAgICAgdmFyIGRpc2FibGVkID0gdGhpcy5pc0Rpc2FibGVkKCk7XG4gICAgICB2YXIgZGF0YU9yQXJpYUF0dHJpYnV0ZVByb3BzID0gZ2V0RGF0YUFuZEFyaWEob3RoZXJQcm9wcyk7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChcImxpXCIsIE9iamVjdC5hc3NpZ24oe1xuICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZXMoY2xhc3NOYW1lLCAoX2NsYXNzTmFtZXMgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXRyZWVub2RlLWRpc2FibGVkXCIpLCBkaXNhYmxlZCksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10cmVlbm9kZS1zd2l0Y2hlci1cIikuY29uY2F0KGV4cGFuZGVkID8gJ29wZW4nIDogJ2Nsb3NlJyksICFpc0xlYWYpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItdHJlZW5vZGUtY2hlY2tib3gtY2hlY2tlZFwiKSwgY2hlY2tlZCksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10cmVlbm9kZS1jaGVja2JveC1pbmRldGVybWluYXRlXCIpLCBoYWxmQ2hlY2tlZCksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi10cmVlbm9kZS1zZWxlY3RlZFwiKSwgc2VsZWN0ZWQpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIFwiXCIuY29uY2F0KHByZWZpeENscywgXCItdHJlZW5vZGUtbG9hZGluZ1wiKSwgbG9hZGluZyksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgJ2RyYWctb3ZlcicsICFkaXNhYmxlZCAmJiBkcmFnT3ZlciksIF9kZWZpbmVQcm9wZXJ0eShfY2xhc3NOYW1lcywgJ2RyYWctb3Zlci1nYXAtdG9wJywgIWRpc2FibGVkICYmIGRyYWdPdmVyR2FwVG9wKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCAnZHJhZy1vdmVyLWdhcC1ib3R0b20nLCAhZGlzYWJsZWQgJiYgZHJhZ092ZXJHYXBCb3R0b20pLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsICdmaWx0ZXItbm9kZScsIGZpbHRlclRyZWVOb2RlICYmIGZpbHRlclRyZWVOb2RlKHRoaXMpKSwgX2NsYXNzTmFtZXMpKSxcbiAgICAgICAgc3R5bGU6IHN0eWxlLFxuICAgICAgICByb2xlOiBcInRyZWVpdGVtXCIsXG4gICAgICAgIG9uRHJhZ0VudGVyOiBkcmFnZ2FibGUgPyB0aGlzLm9uRHJhZ0VudGVyIDogdW5kZWZpbmVkLFxuICAgICAgICBvbkRyYWdPdmVyOiBkcmFnZ2FibGUgPyB0aGlzLm9uRHJhZ092ZXIgOiB1bmRlZmluZWQsXG4gICAgICAgIG9uRHJhZ0xlYXZlOiBkcmFnZ2FibGUgPyB0aGlzLm9uRHJhZ0xlYXZlIDogdW5kZWZpbmVkLFxuICAgICAgICBvbkRyb3A6IGRyYWdnYWJsZSA/IHRoaXMub25Ecm9wIDogdW5kZWZpbmVkLFxuICAgICAgICBvbkRyYWdFbmQ6IGRyYWdnYWJsZSA/IHRoaXMub25EcmFnRW5kIDogdW5kZWZpbmVkXG4gICAgICB9LCBkYXRhT3JBcmlhQXR0cmlidXRlUHJvcHMpLCB0aGlzLnJlbmRlclN3aXRjaGVyKCksIHRoaXMucmVuZGVyQ2hlY2tib3goKSwgdGhpcy5yZW5kZXJTZWxlY3RvcigpLCB0aGlzLnJlbmRlckNoaWxkcmVuKCkpO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBUcmVlTm9kZTtcbn0oUmVhY3QuQ29tcG9uZW50KTtcblxuVHJlZU5vZGUucHJvcFR5cGVzID0ge1xuICBldmVudEtleTogUHJvcFR5cGVzLnN0cmluZyxcbiAgcHJlZml4Q2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICBvblNlbGVjdDogUHJvcFR5cGVzLmZ1bmMsXG4gIC8vIEJ5IHBhcmVudFxuICBleHBhbmRlZDogUHJvcFR5cGVzLmJvb2wsXG4gIHNlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgY2hlY2tlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGxvYWRlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGxvYWRpbmc6IFByb3BUeXBlcy5ib29sLFxuICBoYWxmQ2hlY2tlZDogUHJvcFR5cGVzLmJvb2wsXG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMubm9kZSxcbiAgdGl0bGU6IFByb3BUeXBlcy5ub2RlLFxuICBwb3M6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRyYWdPdmVyOiBQcm9wVHlwZXMuYm9vbCxcbiAgZHJhZ092ZXJHYXBUb3A6IFByb3BUeXBlcy5ib29sLFxuICBkcmFnT3ZlckdhcEJvdHRvbTogUHJvcFR5cGVzLmJvb2wsXG4gIC8vIEJ5IHVzZXJcbiAgaXNMZWFmOiBQcm9wVHlwZXMuYm9vbCxcbiAgY2hlY2thYmxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgc2VsZWN0YWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgZGlzYWJsZUNoZWNrYm94OiBQcm9wVHlwZXMuYm9vbCxcbiAgaWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm5vZGUsIFByb3BUeXBlcy5mdW5jXSksXG4gIHN3aXRjaGVySWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm5vZGUsIFByb3BUeXBlcy5mdW5jXSlcbn07XG5wb2x5ZmlsbChUcmVlTm9kZSk7XG5cbnZhciBDb250ZXh0VHJlZU5vZGUgPSBmdW5jdGlvbiBDb250ZXh0VHJlZU5vZGUocHJvcHMpIHtcbiAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoVHJlZUNvbnRleHQuQ29uc3VtZXIsIG51bGwsIGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoVHJlZU5vZGUsIE9iamVjdC5hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICBjb250ZXh0OiBjb250ZXh0XG4gICAgfSkpO1xuICB9KTtcbn07XG5cbkNvbnRleHRUcmVlTm9kZS5kZWZhdWx0UHJvcHMgPSB7XG4gIHRpdGxlOiBkZWZhdWx0VGl0bGVcbn07XG5Db250ZXh0VHJlZU5vZGUuaXNUcmVlTm9kZSA9IDE7XG5leHBvcnQgeyBUcmVlTm9kZSBhcyBJbnRlcm5hbFRyZWVOb2RlIH07XG5leHBvcnQgZGVmYXVsdCBDb250ZXh0VHJlZU5vZGU7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBWEE7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBWUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQXBDQTtBQUNBO0FBc0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTFCQTtBQTRCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree/es/TreeNode.js
