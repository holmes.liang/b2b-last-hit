/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var isArray = _util.isArray;
var isObject = _util.isObject;

var compatStyle = __webpack_require__(/*! ./helper/compatStyle */ "./node_modules/echarts/lib/preprocessor/helper/compatStyle.js");

var _model = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var normalizeToArray = _model.normalizeToArray;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
// Compatitable with 2.0

function get(opt, path) {
  path = path.split(',');
  var obj = opt;

  for (var i = 0; i < path.length; i++) {
    obj = obj && obj[path[i]];

    if (obj == null) {
      break;
    }
  }

  return obj;
}

function set(opt, path, val, overwrite) {
  path = path.split(',');
  var obj = opt;
  var key;

  for (var i = 0; i < path.length - 1; i++) {
    key = path[i];

    if (obj[key] == null) {
      obj[key] = {};
    }

    obj = obj[key];
  }

  if (overwrite || obj[path[i]] == null) {
    obj[path[i]] = val;
  }
}

function compatLayoutProperties(option) {
  each(LAYOUT_PROPERTIES, function (prop) {
    if (prop[0] in option && !(prop[1] in option)) {
      option[prop[1]] = option[prop[0]];
    }
  });
}

var LAYOUT_PROPERTIES = [['x', 'left'], ['y', 'top'], ['x2', 'right'], ['y2', 'bottom']];
var COMPATITABLE_COMPONENTS = ['grid', 'geo', 'parallel', 'legend', 'toolbox', 'title', 'visualMap', 'dataZoom', 'timeline'];

function _default(option, isTheme) {
  compatStyle(option, isTheme); // Make sure series array for model initialization.

  option.series = normalizeToArray(option.series);
  each(option.series, function (seriesOpt) {
    if (!isObject(seriesOpt)) {
      return;
    }

    var seriesType = seriesOpt.type;

    if (seriesType === 'pie' || seriesType === 'gauge') {
      if (seriesOpt.clockWise != null) {
        seriesOpt.clockwise = seriesOpt.clockWise;
      }
    }

    if (seriesType === 'gauge') {
      var pointerColor = get(seriesOpt, 'pointer.color');
      pointerColor != null && set(seriesOpt, 'itemStyle.normal.color', pointerColor);
    }

    compatLayoutProperties(seriesOpt);
  }); // dataRange has changed to visualMap

  if (option.dataRange) {
    option.visualMap = option.dataRange;
  }

  each(COMPATITABLE_COMPONENTS, function (componentName) {
    var options = option[componentName];

    if (options) {
      if (!isArray(options)) {
        options = [options];
      }

      each(options, function (option) {
        compatLayoutProperties(option);
      });
    }
  });
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvcHJlcHJvY2Vzc29yL2JhY2t3YXJkQ29tcGF0LmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvcHJlcHJvY2Vzc29yL2JhY2t3YXJkQ29tcGF0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX3V0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgZWFjaCA9IF91dGlsLmVhY2g7XG52YXIgaXNBcnJheSA9IF91dGlsLmlzQXJyYXk7XG52YXIgaXNPYmplY3QgPSBfdXRpbC5pc09iamVjdDtcblxudmFyIGNvbXBhdFN0eWxlID0gcmVxdWlyZShcIi4vaGVscGVyL2NvbXBhdFN0eWxlXCIpO1xuXG52YXIgX21vZGVsID0gcmVxdWlyZShcIi4uL3V0aWwvbW9kZWxcIik7XG5cbnZhciBub3JtYWxpemVUb0FycmF5ID0gX21vZGVsLm5vcm1hbGl6ZVRvQXJyYXk7XG5cbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cbi8vIENvbXBhdGl0YWJsZSB3aXRoIDIuMFxuZnVuY3Rpb24gZ2V0KG9wdCwgcGF0aCkge1xuICBwYXRoID0gcGF0aC5zcGxpdCgnLCcpO1xuICB2YXIgb2JqID0gb3B0O1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aC5sZW5ndGg7IGkrKykge1xuICAgIG9iaiA9IG9iaiAmJiBvYmpbcGF0aFtpXV07XG5cbiAgICBpZiAob2JqID09IG51bGwpIHtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBvYmo7XG59XG5cbmZ1bmN0aW9uIHNldChvcHQsIHBhdGgsIHZhbCwgb3ZlcndyaXRlKSB7XG4gIHBhdGggPSBwYXRoLnNwbGl0KCcsJyk7XG4gIHZhciBvYmogPSBvcHQ7XG4gIHZhciBrZXk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoLmxlbmd0aCAtIDE7IGkrKykge1xuICAgIGtleSA9IHBhdGhbaV07XG5cbiAgICBpZiAob2JqW2tleV0gPT0gbnVsbCkge1xuICAgICAgb2JqW2tleV0gPSB7fTtcbiAgICB9XG5cbiAgICBvYmogPSBvYmpba2V5XTtcbiAgfVxuXG4gIGlmIChvdmVyd3JpdGUgfHwgb2JqW3BhdGhbaV1dID09IG51bGwpIHtcbiAgICBvYmpbcGF0aFtpXV0gPSB2YWw7XG4gIH1cbn1cblxuZnVuY3Rpb24gY29tcGF0TGF5b3V0UHJvcGVydGllcyhvcHRpb24pIHtcbiAgZWFjaChMQVlPVVRfUFJPUEVSVElFUywgZnVuY3Rpb24gKHByb3ApIHtcbiAgICBpZiAocHJvcFswXSBpbiBvcHRpb24gJiYgIShwcm9wWzFdIGluIG9wdGlvbikpIHtcbiAgICAgIG9wdGlvbltwcm9wWzFdXSA9IG9wdGlvbltwcm9wWzBdXTtcbiAgICB9XG4gIH0pO1xufVxuXG52YXIgTEFZT1VUX1BST1BFUlRJRVMgPSBbWyd4JywgJ2xlZnQnXSwgWyd5JywgJ3RvcCddLCBbJ3gyJywgJ3JpZ2h0J10sIFsneTInLCAnYm90dG9tJ11dO1xudmFyIENPTVBBVElUQUJMRV9DT01QT05FTlRTID0gWydncmlkJywgJ2dlbycsICdwYXJhbGxlbCcsICdsZWdlbmQnLCAndG9vbGJveCcsICd0aXRsZScsICd2aXN1YWxNYXAnLCAnZGF0YVpvb20nLCAndGltZWxpbmUnXTtcblxuZnVuY3Rpb24gX2RlZmF1bHQob3B0aW9uLCBpc1RoZW1lKSB7XG4gIGNvbXBhdFN0eWxlKG9wdGlvbiwgaXNUaGVtZSk7IC8vIE1ha2Ugc3VyZSBzZXJpZXMgYXJyYXkgZm9yIG1vZGVsIGluaXRpYWxpemF0aW9uLlxuXG4gIG9wdGlvbi5zZXJpZXMgPSBub3JtYWxpemVUb0FycmF5KG9wdGlvbi5zZXJpZXMpO1xuICBlYWNoKG9wdGlvbi5zZXJpZXMsIGZ1bmN0aW9uIChzZXJpZXNPcHQpIHtcbiAgICBpZiAoIWlzT2JqZWN0KHNlcmllc09wdCkpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgc2VyaWVzVHlwZSA9IHNlcmllc09wdC50eXBlO1xuXG4gICAgaWYgKHNlcmllc1R5cGUgPT09ICdwaWUnIHx8IHNlcmllc1R5cGUgPT09ICdnYXVnZScpIHtcbiAgICAgIGlmIChzZXJpZXNPcHQuY2xvY2tXaXNlICE9IG51bGwpIHtcbiAgICAgICAgc2VyaWVzT3B0LmNsb2Nrd2lzZSA9IHNlcmllc09wdC5jbG9ja1dpc2U7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHNlcmllc1R5cGUgPT09ICdnYXVnZScpIHtcbiAgICAgIHZhciBwb2ludGVyQ29sb3IgPSBnZXQoc2VyaWVzT3B0LCAncG9pbnRlci5jb2xvcicpO1xuICAgICAgcG9pbnRlckNvbG9yICE9IG51bGwgJiYgc2V0KHNlcmllc09wdCwgJ2l0ZW1TdHlsZS5ub3JtYWwuY29sb3InLCBwb2ludGVyQ29sb3IpO1xuICAgIH1cblxuICAgIGNvbXBhdExheW91dFByb3BlcnRpZXMoc2VyaWVzT3B0KTtcbiAgfSk7IC8vIGRhdGFSYW5nZSBoYXMgY2hhbmdlZCB0byB2aXN1YWxNYXBcblxuICBpZiAob3B0aW9uLmRhdGFSYW5nZSkge1xuICAgIG9wdGlvbi52aXN1YWxNYXAgPSBvcHRpb24uZGF0YVJhbmdlO1xuICB9XG5cbiAgZWFjaChDT01QQVRJVEFCTEVfQ09NUE9ORU5UUywgZnVuY3Rpb24gKGNvbXBvbmVudE5hbWUpIHtcbiAgICB2YXIgb3B0aW9ucyA9IG9wdGlvbltjb21wb25lbnROYW1lXTtcblxuICAgIGlmIChvcHRpb25zKSB7XG4gICAgICBpZiAoIWlzQXJyYXkob3B0aW9ucykpIHtcbiAgICAgICAgb3B0aW9ucyA9IFtvcHRpb25zXTtcbiAgICAgIH1cblxuICAgICAgZWFjaChvcHRpb25zLCBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIGNvbXBhdExheW91dFByb3BlcnRpZXMob3B0aW9uKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2RlZmF1bHQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/preprocessor/backwardCompat.js
