__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotionPropTypes", function() { return MotionPropTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genCSSMotion", function() { return genCSSMotion; });
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");
/* harmony import */ var babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");
/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");
/* harmony import */ var babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");
/* harmony import */ var babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");
/* harmony import */ var babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var rc_util_es_Dom_findDOMNode__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rc-util/es/Dom/findDOMNode */ "./node_modules/rc-util/es/Dom/findDOMNode.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! raf */ "./node_modules/raf/index.js");
/* harmony import */ var raf__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(raf__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _util_motion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./util/motion */ "./node_modules/rc-animate/es/util/motion.js");






/* eslint-disable react/default-props-match-prop-types, react/no-multi-comp */








var STATUS_NONE = 'none';
var STATUS_APPEAR = 'appear';
var STATUS_ENTER = 'enter';
var STATUS_LEAVE = 'leave';
var MotionPropTypes = {
  eventProps: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object,
  // Internal usage. Only pass by CSSMotionList
  visible: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  children: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  motionName: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string, prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object]),
  motionAppear: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  motionEnter: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  motionLeave: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  motionLeaveImmediately: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  // Trigger leave motion immediately
  removeOnLeave: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.bool,
  leavedClassName: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.string,
  onAppearStart: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onAppearActive: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onAppearEnd: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onEnterStart: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onEnterActive: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onEnterEnd: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onLeaveStart: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onLeaveActive: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func,
  onLeaveEnd: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func
};
/**
 * `transitionSupport` is used for none transition test case.
 * Default we use browser transition event support check.
 */

function genCSSMotion(config) {
  var transitionSupport = config;
  var forwardRef = !!react__WEBPACK_IMPORTED_MODULE_6___default.a.forwardRef;

  if (typeof config === 'object') {
    transitionSupport = config.transitionSupport;
    forwardRef = 'forwardRef' in config ? config.forwardRef : forwardRef;
  }

  function isSupportTransition(props) {
    return !!(props.motionName && transitionSupport);
  }

  var CSSMotion = function (_React$Component) {
    babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(CSSMotion, _React$Component);

    function CSSMotion() {
      babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, CSSMotion);

      var _this = babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, (CSSMotion.__proto__ || Object.getPrototypeOf(CSSMotion)).call(this));

      _this.onDomUpdate = function () {
        var _this$state = _this.state,
            status = _this$state.status,
            newStatus = _this$state.newStatus;
        var _this$props = _this.props,
            onAppearStart = _this$props.onAppearStart,
            onEnterStart = _this$props.onEnterStart,
            onLeaveStart = _this$props.onLeaveStart,
            onAppearActive = _this$props.onAppearActive,
            onEnterActive = _this$props.onEnterActive,
            onLeaveActive = _this$props.onLeaveActive,
            motionAppear = _this$props.motionAppear,
            motionEnter = _this$props.motionEnter,
            motionLeave = _this$props.motionLeave;

        if (!isSupportTransition(_this.props)) {
          return;
        } // Event injection


        var $ele = _this.getElement();

        if (_this.$cacheEle !== $ele) {
          _this.removeEventListener(_this.$cacheEle);

          _this.addEventListener($ele);

          _this.$cacheEle = $ele;
        } // Init status


        if (newStatus && status === STATUS_APPEAR && motionAppear) {
          _this.updateStatus(onAppearStart, null, null, function () {
            _this.updateActiveStatus(onAppearActive, STATUS_APPEAR);
          });
        } else if (newStatus && status === STATUS_ENTER && motionEnter) {
          _this.updateStatus(onEnterStart, null, null, function () {
            _this.updateActiveStatus(onEnterActive, STATUS_ENTER);
          });
        } else if (newStatus && status === STATUS_LEAVE && motionLeave) {
          _this.updateStatus(onLeaveStart, null, null, function () {
            _this.updateActiveStatus(onLeaveActive, STATUS_LEAVE);
          });
        }
      };

      _this.onMotionEnd = function (event) {
        var _this$state2 = _this.state,
            status = _this$state2.status,
            statusActive = _this$state2.statusActive;
        var _this$props2 = _this.props,
            onAppearEnd = _this$props2.onAppearEnd,
            onEnterEnd = _this$props2.onEnterEnd,
            onLeaveEnd = _this$props2.onLeaveEnd;

        if (status === STATUS_APPEAR && statusActive) {
          _this.updateStatus(onAppearEnd, {
            status: STATUS_NONE
          }, event);
        } else if (status === STATUS_ENTER && statusActive) {
          _this.updateStatus(onEnterEnd, {
            status: STATUS_NONE
          }, event);
        } else if (status === STATUS_LEAVE && statusActive) {
          _this.updateStatus(onLeaveEnd, {
            status: STATUS_NONE
          }, event);
        }
      };

      _this.setNodeRef = function (node) {
        var internalRef = _this.props.internalRef;
        _this.node = node;

        if (typeof internalRef === 'function') {
          internalRef(node);
        } else if (internalRef && 'current' in internalRef) {
          internalRef.current = node;
        }
      };

      _this.getElement = function () {
        return Object(rc_util_es_Dom_findDOMNode__WEBPACK_IMPORTED_MODULE_9__["default"])(_this.node || _this);
      };

      _this.addEventListener = function ($ele) {
        if (!$ele) return;
        $ele.addEventListener(_util_motion__WEBPACK_IMPORTED_MODULE_12__["transitionEndName"], _this.onMotionEnd);
        $ele.addEventListener(_util_motion__WEBPACK_IMPORTED_MODULE_12__["animationEndName"], _this.onMotionEnd);
      };

      _this.removeEventListener = function ($ele) {
        if (!$ele) return;
        $ele.removeEventListener(_util_motion__WEBPACK_IMPORTED_MODULE_12__["transitionEndName"], _this.onMotionEnd);
        $ele.removeEventListener(_util_motion__WEBPACK_IMPORTED_MODULE_12__["animationEndName"], _this.onMotionEnd);
      };

      _this.updateStatus = function (styleFunc, additionalState, event, callback) {
        var statusStyle = styleFunc ? styleFunc(_this.getElement(), event) : null;
        if (statusStyle === false || _this._destroyed) return;
        var nextStep = void 0;

        if (callback) {
          nextStep = function nextStep() {
            _this.nextFrame(callback);
          };
        }

        _this.setState(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
          statusStyle: typeof statusStyle === 'object' ? statusStyle : null,
          newStatus: false
        }, additionalState), nextStep); // Trigger before next frame & after `componentDidMount`

      };

      _this.updateActiveStatus = function (styleFunc, currentStatus) {
        // `setState` use `postMessage` to trigger at the end of frame.
        // Let's use requestAnimationFrame to update new state in next frame.
        _this.nextFrame(function () {
          var status = _this.state.status;
          if (status !== currentStatus) return;

          _this.updateStatus(styleFunc, {
            statusActive: true
          });
        });
      };

      _this.nextFrame = function (func) {
        _this.cancelNextFrame();

        _this.raf = raf__WEBPACK_IMPORTED_MODULE_11___default()(func);
      };

      _this.cancelNextFrame = function () {
        if (_this.raf) {
          raf__WEBPACK_IMPORTED_MODULE_11___default.a.cancel(_this.raf);
          _this.raf = null;
        }
      };

      _this.state = {
        status: STATUS_NONE,
        statusActive: false,
        newStatus: false,
        statusStyle: null
      };
      _this.$cacheEle = null;
      _this.node = null;
      _this.raf = null;
      return _this;
    }

    babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(CSSMotion, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.onDomUpdate();
      }
    }, {
      key: 'componentDidUpdate',
      value: function componentDidUpdate() {
        this.onDomUpdate();
      }
    }, {
      key: 'componentWillUnmount',
      value: function componentWillUnmount() {
        this._destroyed = true;
        this.removeEventListener(this.$cacheEle);
        this.cancelNextFrame();
      }
    }, {
      key: 'render',
      value: function render() {
        var _classNames;

        var _state = this.state,
            status = _state.status,
            statusActive = _state.statusActive,
            statusStyle = _state.statusStyle;
        var _props = this.props,
            children = _props.children,
            motionName = _props.motionName,
            visible = _props.visible,
            removeOnLeave = _props.removeOnLeave,
            leavedClassName = _props.leavedClassName,
            eventProps = _props.eventProps;
        if (!children) return null;

        if (status === STATUS_NONE || !isSupportTransition(this.props)) {
          if (visible) {
            return children(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, eventProps), this.setNodeRef);
          } else if (!removeOnLeave) {
            return children(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, eventProps, {
              className: leavedClassName
            }), this.setNodeRef);
          }

          return null;
        }

        return children(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, eventProps, {
          className: classnames__WEBPACK_IMPORTED_MODULE_10___default()((_classNames = {}, babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, Object(_util_motion__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(motionName, status), status !== STATUS_NONE), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, Object(_util_motion__WEBPACK_IMPORTED_MODULE_12__["getTransitionName"])(motionName, status + '-active'), status !== STATUS_NONE && statusActive), babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(_classNames, motionName, typeof motionName === 'string'), _classNames)),
          style: statusStyle
        }), this.setNodeRef);
      }
    }], [{
      key: 'getDerivedStateFromProps',
      value: function getDerivedStateFromProps(props, _ref) {
        var prevProps = _ref.prevProps,
            prevStatus = _ref.status;
        if (!isSupportTransition(props)) return {};
        var visible = props.visible,
            motionAppear = props.motionAppear,
            motionEnter = props.motionEnter,
            motionLeave = props.motionLeave,
            motionLeaveImmediately = props.motionLeaveImmediately;
        var newState = {
          prevProps: props
        }; // Clean up status if prop set to false

        if (prevStatus === STATUS_APPEAR && !motionAppear || prevStatus === STATUS_ENTER && !motionEnter || prevStatus === STATUS_LEAVE && !motionLeave) {
          newState.status = STATUS_NONE;
          newState.statusActive = false;
          newState.newStatus = false;
        } // Appear


        if (!prevProps && visible && motionAppear) {
          newState.status = STATUS_APPEAR;
          newState.statusActive = false;
          newState.newStatus = true;
        } // Enter


        if (prevProps && !prevProps.visible && visible && motionEnter) {
          newState.status = STATUS_ENTER;
          newState.statusActive = false;
          newState.newStatus = true;
        } // Leave


        if (prevProps && prevProps.visible && !visible && motionLeave || !prevProps && motionLeaveImmediately && !visible && motionLeave) {
          newState.status = STATUS_LEAVE;
          newState.statusActive = false;
          newState.newStatus = true;
        }

        return newState;
      }
    }]);

    return CSSMotion;
  }(react__WEBPACK_IMPORTED_MODULE_6___default.a.Component);

  CSSMotion.propTypes = babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({}, MotionPropTypes, {
    internalRef: prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.oneOfType([prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.object, prop_types__WEBPACK_IMPORTED_MODULE_7___default.a.func])
  });
  CSSMotion.defaultProps = {
    visible: true,
    motionEnter: true,
    motionAppear: true,
    motionLeave: true,
    removeOnLeave: true
  };
  Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_8__["polyfill"])(CSSMotion);

  if (!forwardRef) {
    return CSSMotion;
  }

  return react__WEBPACK_IMPORTED_MODULE_6___default.a.forwardRef(function (props, ref) {
    return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(CSSMotion, babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_1___default()({
      internalRef: ref
    }, props));
  });
}
/* harmony default export */ __webpack_exports__["default"] = (genCSSMotion(_util_motion__WEBPACK_IMPORTED_MODULE_12__["supportTransition"]));//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtYW5pbWF0ZS9lcy9DU1NNb3Rpb24uanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1hbmltYXRlL2VzL0NTU01vdGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgX2RlZmluZVByb3BlcnR5IGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eSc7XG5pbXBvcnQgX2V4dGVuZHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnO1xuaW1wb3J0IF9jbGFzc0NhbGxDaGVjayBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snO1xuaW1wb3J0IF9jcmVhdGVDbGFzcyBmcm9tICdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnO1xuaW1wb3J0IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuIGZyb20gJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJztcbmltcG9ydCBfaW5oZXJpdHMgZnJvbSAnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJztcbi8qIGVzbGludC1kaXNhYmxlIHJlYWN0L2RlZmF1bHQtcHJvcHMtbWF0Y2gtcHJvcC10eXBlcywgcmVhY3Qvbm8tbXVsdGktY29tcCAqL1xuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCBmaW5kRE9NTm9kZSBmcm9tICdyYy11dGlsL2VzL0RvbS9maW5kRE9NTm9kZSc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCByYWYgZnJvbSAncmFmJztcbmltcG9ydCB7IGdldFRyYW5zaXRpb25OYW1lLCBhbmltYXRpb25FbmROYW1lLCB0cmFuc2l0aW9uRW5kTmFtZSwgc3VwcG9ydFRyYW5zaXRpb24gfSBmcm9tICcuL3V0aWwvbW90aW9uJztcblxudmFyIFNUQVRVU19OT05FID0gJ25vbmUnO1xudmFyIFNUQVRVU19BUFBFQVIgPSAnYXBwZWFyJztcbnZhciBTVEFUVVNfRU5URVIgPSAnZW50ZXInO1xudmFyIFNUQVRVU19MRUFWRSA9ICdsZWF2ZSc7XG5cbmV4cG9ydCB2YXIgTW90aW9uUHJvcFR5cGVzID0ge1xuICBldmVudFByb3BzOiBQcm9wVHlwZXMub2JqZWN0LCAvLyBJbnRlcm5hbCB1c2FnZS4gT25seSBwYXNzIGJ5IENTU01vdGlvbkxpc3RcbiAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gIGNoaWxkcmVuOiBQcm9wVHlwZXMuZnVuYyxcbiAgbW90aW9uTmFtZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm9iamVjdF0pLFxuICBtb3Rpb25BcHBlYXI6IFByb3BUeXBlcy5ib29sLFxuICBtb3Rpb25FbnRlcjogUHJvcFR5cGVzLmJvb2wsXG4gIG1vdGlvbkxlYXZlOiBQcm9wVHlwZXMuYm9vbCxcbiAgbW90aW9uTGVhdmVJbW1lZGlhdGVseTogUHJvcFR5cGVzLmJvb2wsIC8vIFRyaWdnZXIgbGVhdmUgbW90aW9uIGltbWVkaWF0ZWx5XG4gIHJlbW92ZU9uTGVhdmU6IFByb3BUeXBlcy5ib29sLFxuICBsZWF2ZWRDbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIG9uQXBwZWFyU3RhcnQ6IFByb3BUeXBlcy5mdW5jLFxuICBvbkFwcGVhckFjdGl2ZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uQXBwZWFyRW5kOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25FbnRlclN0YXJ0OiBQcm9wVHlwZXMuZnVuYyxcbiAgb25FbnRlckFjdGl2ZTogUHJvcFR5cGVzLmZ1bmMsXG4gIG9uRW50ZXJFbmQ6IFByb3BUeXBlcy5mdW5jLFxuICBvbkxlYXZlU3RhcnQ6IFByb3BUeXBlcy5mdW5jLFxuICBvbkxlYXZlQWN0aXZlOiBQcm9wVHlwZXMuZnVuYyxcbiAgb25MZWF2ZUVuZDogUHJvcFR5cGVzLmZ1bmNcbn07XG5cbi8qKlxuICogYHRyYW5zaXRpb25TdXBwb3J0YCBpcyB1c2VkIGZvciBub25lIHRyYW5zaXRpb24gdGVzdCBjYXNlLlxuICogRGVmYXVsdCB3ZSB1c2UgYnJvd3NlciB0cmFuc2l0aW9uIGV2ZW50IHN1cHBvcnQgY2hlY2suXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZW5DU1NNb3Rpb24oY29uZmlnKSB7XG4gIHZhciB0cmFuc2l0aW9uU3VwcG9ydCA9IGNvbmZpZztcbiAgdmFyIGZvcndhcmRSZWYgPSAhIVJlYWN0LmZvcndhcmRSZWY7XG5cbiAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdvYmplY3QnKSB7XG4gICAgdHJhbnNpdGlvblN1cHBvcnQgPSBjb25maWcudHJhbnNpdGlvblN1cHBvcnQ7XG4gICAgZm9yd2FyZFJlZiA9ICdmb3J3YXJkUmVmJyBpbiBjb25maWcgPyBjb25maWcuZm9yd2FyZFJlZiA6IGZvcndhcmRSZWY7XG4gIH1cblxuICBmdW5jdGlvbiBpc1N1cHBvcnRUcmFuc2l0aW9uKHByb3BzKSB7XG4gICAgcmV0dXJuICEhKHByb3BzLm1vdGlvbk5hbWUgJiYgdHJhbnNpdGlvblN1cHBvcnQpO1xuICB9XG5cbiAgdmFyIENTU01vdGlvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICAgX2luaGVyaXRzKENTU01vdGlvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgICBmdW5jdGlvbiBDU1NNb3Rpb24oKSB7XG4gICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ1NTTW90aW9uKTtcblxuICAgICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKENTU01vdGlvbi5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKENTU01vdGlvbikpLmNhbGwodGhpcykpO1xuXG4gICAgICBfdGhpcy5vbkRvbVVwZGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzJHN0YXRlID0gX3RoaXMuc3RhdGUsXG4gICAgICAgICAgICBzdGF0dXMgPSBfdGhpcyRzdGF0ZS5zdGF0dXMsXG4gICAgICAgICAgICBuZXdTdGF0dXMgPSBfdGhpcyRzdGF0ZS5uZXdTdGF0dXM7XG4gICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgb25BcHBlYXJTdGFydCA9IF90aGlzJHByb3BzLm9uQXBwZWFyU3RhcnQsXG4gICAgICAgICAgICBvbkVudGVyU3RhcnQgPSBfdGhpcyRwcm9wcy5vbkVudGVyU3RhcnQsXG4gICAgICAgICAgICBvbkxlYXZlU3RhcnQgPSBfdGhpcyRwcm9wcy5vbkxlYXZlU3RhcnQsXG4gICAgICAgICAgICBvbkFwcGVhckFjdGl2ZSA9IF90aGlzJHByb3BzLm9uQXBwZWFyQWN0aXZlLFxuICAgICAgICAgICAgb25FbnRlckFjdGl2ZSA9IF90aGlzJHByb3BzLm9uRW50ZXJBY3RpdmUsXG4gICAgICAgICAgICBvbkxlYXZlQWN0aXZlID0gX3RoaXMkcHJvcHMub25MZWF2ZUFjdGl2ZSxcbiAgICAgICAgICAgIG1vdGlvbkFwcGVhciA9IF90aGlzJHByb3BzLm1vdGlvbkFwcGVhcixcbiAgICAgICAgICAgIG1vdGlvbkVudGVyID0gX3RoaXMkcHJvcHMubW90aW9uRW50ZXIsXG4gICAgICAgICAgICBtb3Rpb25MZWF2ZSA9IF90aGlzJHByb3BzLm1vdGlvbkxlYXZlO1xuXG5cbiAgICAgICAgaWYgKCFpc1N1cHBvcnRUcmFuc2l0aW9uKF90aGlzLnByb3BzKSkge1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEV2ZW50IGluamVjdGlvblxuICAgICAgICB2YXIgJGVsZSA9IF90aGlzLmdldEVsZW1lbnQoKTtcbiAgICAgICAgaWYgKF90aGlzLiRjYWNoZUVsZSAhPT0gJGVsZSkge1xuICAgICAgICAgIF90aGlzLnJlbW92ZUV2ZW50TGlzdGVuZXIoX3RoaXMuJGNhY2hlRWxlKTtcbiAgICAgICAgICBfdGhpcy5hZGRFdmVudExpc3RlbmVyKCRlbGUpO1xuICAgICAgICAgIF90aGlzLiRjYWNoZUVsZSA9ICRlbGU7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBJbml0IHN0YXR1c1xuICAgICAgICBpZiAobmV3U3RhdHVzICYmIHN0YXR1cyA9PT0gU1RBVFVTX0FQUEVBUiAmJiBtb3Rpb25BcHBlYXIpIHtcbiAgICAgICAgICBfdGhpcy51cGRhdGVTdGF0dXMob25BcHBlYXJTdGFydCwgbnVsbCwgbnVsbCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgX3RoaXMudXBkYXRlQWN0aXZlU3RhdHVzKG9uQXBwZWFyQWN0aXZlLCBTVEFUVVNfQVBQRUFSKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChuZXdTdGF0dXMgJiYgc3RhdHVzID09PSBTVEFUVVNfRU5URVIgJiYgbW90aW9uRW50ZXIpIHtcbiAgICAgICAgICBfdGhpcy51cGRhdGVTdGF0dXMob25FbnRlclN0YXJ0LCBudWxsLCBudWxsLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy51cGRhdGVBY3RpdmVTdGF0dXMob25FbnRlckFjdGl2ZSwgU1RBVFVTX0VOVEVSKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChuZXdTdGF0dXMgJiYgc3RhdHVzID09PSBTVEFUVVNfTEVBVkUgJiYgbW90aW9uTGVhdmUpIHtcbiAgICAgICAgICBfdGhpcy51cGRhdGVTdGF0dXMob25MZWF2ZVN0YXJ0LCBudWxsLCBudWxsLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy51cGRhdGVBY3RpdmVTdGF0dXMob25MZWF2ZUFjdGl2ZSwgU1RBVFVTX0xFQVZFKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfTtcblxuICAgICAgX3RoaXMub25Nb3Rpb25FbmQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdmFyIF90aGlzJHN0YXRlMiA9IF90aGlzLnN0YXRlLFxuICAgICAgICAgICAgc3RhdHVzID0gX3RoaXMkc3RhdGUyLnN0YXR1cyxcbiAgICAgICAgICAgIHN0YXR1c0FjdGl2ZSA9IF90aGlzJHN0YXRlMi5zdGF0dXNBY3RpdmU7XG4gICAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgIG9uQXBwZWFyRW5kID0gX3RoaXMkcHJvcHMyLm9uQXBwZWFyRW5kLFxuICAgICAgICAgICAgb25FbnRlckVuZCA9IF90aGlzJHByb3BzMi5vbkVudGVyRW5kLFxuICAgICAgICAgICAgb25MZWF2ZUVuZCA9IF90aGlzJHByb3BzMi5vbkxlYXZlRW5kO1xuXG4gICAgICAgIGlmIChzdGF0dXMgPT09IFNUQVRVU19BUFBFQVIgJiYgc3RhdHVzQWN0aXZlKSB7XG4gICAgICAgICAgX3RoaXMudXBkYXRlU3RhdHVzKG9uQXBwZWFyRW5kLCB7IHN0YXR1czogU1RBVFVTX05PTkUgfSwgZXZlbnQpO1xuICAgICAgICB9IGVsc2UgaWYgKHN0YXR1cyA9PT0gU1RBVFVTX0VOVEVSICYmIHN0YXR1c0FjdGl2ZSkge1xuICAgICAgICAgIF90aGlzLnVwZGF0ZVN0YXR1cyhvbkVudGVyRW5kLCB7IHN0YXR1czogU1RBVFVTX05PTkUgfSwgZXZlbnQpO1xuICAgICAgICB9IGVsc2UgaWYgKHN0YXR1cyA9PT0gU1RBVFVTX0xFQVZFICYmIHN0YXR1c0FjdGl2ZSkge1xuICAgICAgICAgIF90aGlzLnVwZGF0ZVN0YXR1cyhvbkxlYXZlRW5kLCB7IHN0YXR1czogU1RBVFVTX05PTkUgfSwgZXZlbnQpO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5zZXROb2RlUmVmID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgdmFyIGludGVybmFsUmVmID0gX3RoaXMucHJvcHMuaW50ZXJuYWxSZWY7XG5cbiAgICAgICAgX3RoaXMubm9kZSA9IG5vZGU7XG5cbiAgICAgICAgaWYgKHR5cGVvZiBpbnRlcm5hbFJlZiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgIGludGVybmFsUmVmKG5vZGUpO1xuICAgICAgICB9IGVsc2UgaWYgKGludGVybmFsUmVmICYmICdjdXJyZW50JyBpbiBpbnRlcm5hbFJlZikge1xuICAgICAgICAgIGludGVybmFsUmVmLmN1cnJlbnQgPSBub2RlO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5nZXRFbGVtZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gZmluZERPTU5vZGUoX3RoaXMubm9kZSB8fCBfdGhpcyk7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5hZGRFdmVudExpc3RlbmVyID0gZnVuY3Rpb24gKCRlbGUpIHtcbiAgICAgICAgaWYgKCEkZWxlKSByZXR1cm47XG5cbiAgICAgICAgJGVsZS5hZGRFdmVudExpc3RlbmVyKHRyYW5zaXRpb25FbmROYW1lLCBfdGhpcy5vbk1vdGlvbkVuZCk7XG4gICAgICAgICRlbGUuYWRkRXZlbnRMaXN0ZW5lcihhbmltYXRpb25FbmROYW1lLCBfdGhpcy5vbk1vdGlvbkVuZCk7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5yZW1vdmVFdmVudExpc3RlbmVyID0gZnVuY3Rpb24gKCRlbGUpIHtcbiAgICAgICAgaWYgKCEkZWxlKSByZXR1cm47XG5cbiAgICAgICAgJGVsZS5yZW1vdmVFdmVudExpc3RlbmVyKHRyYW5zaXRpb25FbmROYW1lLCBfdGhpcy5vbk1vdGlvbkVuZCk7XG4gICAgICAgICRlbGUucmVtb3ZlRXZlbnRMaXN0ZW5lcihhbmltYXRpb25FbmROYW1lLCBfdGhpcy5vbk1vdGlvbkVuZCk7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy51cGRhdGVTdGF0dXMgPSBmdW5jdGlvbiAoc3R5bGVGdW5jLCBhZGRpdGlvbmFsU3RhdGUsIGV2ZW50LCBjYWxsYmFjaykge1xuICAgICAgICB2YXIgc3RhdHVzU3R5bGUgPSBzdHlsZUZ1bmMgPyBzdHlsZUZ1bmMoX3RoaXMuZ2V0RWxlbWVudCgpLCBldmVudCkgOiBudWxsO1xuXG4gICAgICAgIGlmIChzdGF0dXNTdHlsZSA9PT0gZmFsc2UgfHwgX3RoaXMuX2Rlc3Ryb3llZCkgcmV0dXJuO1xuXG4gICAgICAgIHZhciBuZXh0U3RlcCA9IHZvaWQgMDtcbiAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICAgICAgbmV4dFN0ZXAgPSBmdW5jdGlvbiBuZXh0U3RlcCgpIHtcbiAgICAgICAgICAgIF90aGlzLm5leHRGcmFtZShjYWxsYmFjayk7XG4gICAgICAgICAgfTtcbiAgICAgICAgfVxuXG4gICAgICAgIF90aGlzLnNldFN0YXRlKF9leHRlbmRzKHtcbiAgICAgICAgICBzdGF0dXNTdHlsZTogdHlwZW9mIHN0YXR1c1N0eWxlID09PSAnb2JqZWN0JyA/IHN0YXR1c1N0eWxlIDogbnVsbCxcbiAgICAgICAgICBuZXdTdGF0dXM6IGZhbHNlXG4gICAgICAgIH0sIGFkZGl0aW9uYWxTdGF0ZSksIG5leHRTdGVwKTsgLy8gVHJpZ2dlciBiZWZvcmUgbmV4dCBmcmFtZSAmIGFmdGVyIGBjb21wb25lbnREaWRNb3VudGBcbiAgICAgIH07XG5cbiAgICAgIF90aGlzLnVwZGF0ZUFjdGl2ZVN0YXR1cyA9IGZ1bmN0aW9uIChzdHlsZUZ1bmMsIGN1cnJlbnRTdGF0dXMpIHtcbiAgICAgICAgLy8gYHNldFN0YXRlYCB1c2UgYHBvc3RNZXNzYWdlYCB0byB0cmlnZ2VyIGF0IHRoZSBlbmQgb2YgZnJhbWUuXG4gICAgICAgIC8vIExldCdzIHVzZSByZXF1ZXN0QW5pbWF0aW9uRnJhbWUgdG8gdXBkYXRlIG5ldyBzdGF0ZSBpbiBuZXh0IGZyYW1lLlxuICAgICAgICBfdGhpcy5uZXh0RnJhbWUoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBzdGF0dXMgPSBfdGhpcy5zdGF0ZS5zdGF0dXM7XG5cbiAgICAgICAgICBpZiAoc3RhdHVzICE9PSBjdXJyZW50U3RhdHVzKSByZXR1cm47XG5cbiAgICAgICAgICBfdGhpcy51cGRhdGVTdGF0dXMoc3R5bGVGdW5jLCB7IHN0YXR1c0FjdGl2ZTogdHJ1ZSB9KTtcbiAgICAgICAgfSk7XG4gICAgICB9O1xuXG4gICAgICBfdGhpcy5uZXh0RnJhbWUgPSBmdW5jdGlvbiAoZnVuYykge1xuICAgICAgICBfdGhpcy5jYW5jZWxOZXh0RnJhbWUoKTtcbiAgICAgICAgX3RoaXMucmFmID0gcmFmKGZ1bmMpO1xuICAgICAgfTtcblxuICAgICAgX3RoaXMuY2FuY2VsTmV4dEZyYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoX3RoaXMucmFmKSB7XG4gICAgICAgICAgcmFmLmNhbmNlbChfdGhpcy5yYWYpO1xuICAgICAgICAgIF90aGlzLnJhZiA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIF90aGlzLnN0YXRlID0ge1xuICAgICAgICBzdGF0dXM6IFNUQVRVU19OT05FLFxuICAgICAgICBzdGF0dXNBY3RpdmU6IGZhbHNlLFxuICAgICAgICBuZXdTdGF0dXM6IGZhbHNlLFxuICAgICAgICBzdGF0dXNTdHlsZTogbnVsbFxuICAgICAgfTtcbiAgICAgIF90aGlzLiRjYWNoZUVsZSA9IG51bGw7XG4gICAgICBfdGhpcy5ub2RlID0gbnVsbDtcbiAgICAgIF90aGlzLnJhZiA9IG51bGw7XG4gICAgICByZXR1cm4gX3RoaXM7XG4gICAgfVxuXG4gICAgX2NyZWF0ZUNsYXNzKENTU01vdGlvbiwgW3tcbiAgICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgICAgdGhpcy5vbkRvbVVwZGF0ZSgpO1xuICAgICAgfVxuICAgIH0sIHtcbiAgICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgICAgICB0aGlzLm9uRG9tVXBkYXRlKCk7XG4gICAgICB9XG4gICAgfSwge1xuICAgICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgICB0aGlzLl9kZXN0cm95ZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLnJlbW92ZUV2ZW50TGlzdGVuZXIodGhpcy4kY2FjaGVFbGUpO1xuICAgICAgICB0aGlzLmNhbmNlbE5leHRGcmFtZSgpO1xuICAgICAgfVxuICAgIH0sIHtcbiAgICAgIGtleTogJ3JlbmRlcicsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgICAgdmFyIF9zdGF0ZSA9IHRoaXMuc3RhdGUsXG4gICAgICAgICAgICBzdGF0dXMgPSBfc3RhdGUuc3RhdHVzLFxuICAgICAgICAgICAgc3RhdHVzQWN0aXZlID0gX3N0YXRlLnN0YXR1c0FjdGl2ZSxcbiAgICAgICAgICAgIHN0YXR1c1N0eWxlID0gX3N0YXRlLnN0YXR1c1N0eWxlO1xuICAgICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgICAgbW90aW9uTmFtZSA9IF9wcm9wcy5tb3Rpb25OYW1lLFxuICAgICAgICAgICAgdmlzaWJsZSA9IF9wcm9wcy52aXNpYmxlLFxuICAgICAgICAgICAgcmVtb3ZlT25MZWF2ZSA9IF9wcm9wcy5yZW1vdmVPbkxlYXZlLFxuICAgICAgICAgICAgbGVhdmVkQ2xhc3NOYW1lID0gX3Byb3BzLmxlYXZlZENsYXNzTmFtZSxcbiAgICAgICAgICAgIGV2ZW50UHJvcHMgPSBfcHJvcHMuZXZlbnRQcm9wcztcblxuXG4gICAgICAgIGlmICghY2hpbGRyZW4pIHJldHVybiBudWxsO1xuXG4gICAgICAgIGlmIChzdGF0dXMgPT09IFNUQVRVU19OT05FIHx8ICFpc1N1cHBvcnRUcmFuc2l0aW9uKHRoaXMucHJvcHMpKSB7XG4gICAgICAgICAgaWYgKHZpc2libGUpIHtcbiAgICAgICAgICAgIHJldHVybiBjaGlsZHJlbihfZXh0ZW5kcyh7fSwgZXZlbnRQcm9wcyksIHRoaXMuc2V0Tm9kZVJlZik7XG4gICAgICAgICAgfSBlbHNlIGlmICghcmVtb3ZlT25MZWF2ZSkge1xuICAgICAgICAgICAgcmV0dXJuIGNoaWxkcmVuKF9leHRlbmRzKHt9LCBldmVudFByb3BzLCB7IGNsYXNzTmFtZTogbGVhdmVkQ2xhc3NOYW1lIH0pLCB0aGlzLnNldE5vZGVSZWYpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGNoaWxkcmVuKF9leHRlbmRzKHt9LCBldmVudFByb3BzLCB7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWVzKChfY2xhc3NOYW1lcyA9IHt9LCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIGdldFRyYW5zaXRpb25OYW1lKG1vdGlvbk5hbWUsIHN0YXR1cyksIHN0YXR1cyAhPT0gU1RBVFVTX05PTkUpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzTmFtZXMsIGdldFRyYW5zaXRpb25OYW1lKG1vdGlvbk5hbWUsIHN0YXR1cyArICctYWN0aXZlJyksIHN0YXR1cyAhPT0gU1RBVFVTX05PTkUgJiYgc3RhdHVzQWN0aXZlKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc05hbWVzLCBtb3Rpb25OYW1lLCB0eXBlb2YgbW90aW9uTmFtZSA9PT0gJ3N0cmluZycpLCBfY2xhc3NOYW1lcykpLFxuICAgICAgICAgIHN0eWxlOiBzdGF0dXNTdHlsZVxuICAgICAgICB9KSwgdGhpcy5zZXROb2RlUmVmKTtcbiAgICAgIH1cbiAgICB9XSwgW3tcbiAgICAgIGtleTogJ2dldERlcml2ZWRTdGF0ZUZyb21Qcm9wcycsXG4gICAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKHByb3BzLCBfcmVmKSB7XG4gICAgICAgIHZhciBwcmV2UHJvcHMgPSBfcmVmLnByZXZQcm9wcyxcbiAgICAgICAgICAgIHByZXZTdGF0dXMgPSBfcmVmLnN0YXR1cztcblxuICAgICAgICBpZiAoIWlzU3VwcG9ydFRyYW5zaXRpb24ocHJvcHMpKSByZXR1cm4ge307XG5cbiAgICAgICAgdmFyIHZpc2libGUgPSBwcm9wcy52aXNpYmxlLFxuICAgICAgICAgICAgbW90aW9uQXBwZWFyID0gcHJvcHMubW90aW9uQXBwZWFyLFxuICAgICAgICAgICAgbW90aW9uRW50ZXIgPSBwcm9wcy5tb3Rpb25FbnRlcixcbiAgICAgICAgICAgIG1vdGlvbkxlYXZlID0gcHJvcHMubW90aW9uTGVhdmUsXG4gICAgICAgICAgICBtb3Rpb25MZWF2ZUltbWVkaWF0ZWx5ID0gcHJvcHMubW90aW9uTGVhdmVJbW1lZGlhdGVseTtcblxuICAgICAgICB2YXIgbmV3U3RhdGUgPSB7XG4gICAgICAgICAgcHJldlByb3BzOiBwcm9wc1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vIENsZWFuIHVwIHN0YXR1cyBpZiBwcm9wIHNldCB0byBmYWxzZVxuICAgICAgICBpZiAocHJldlN0YXR1cyA9PT0gU1RBVFVTX0FQUEVBUiAmJiAhbW90aW9uQXBwZWFyIHx8IHByZXZTdGF0dXMgPT09IFNUQVRVU19FTlRFUiAmJiAhbW90aW9uRW50ZXIgfHwgcHJldlN0YXR1cyA9PT0gU1RBVFVTX0xFQVZFICYmICFtb3Rpb25MZWF2ZSkge1xuICAgICAgICAgIG5ld1N0YXRlLnN0YXR1cyA9IFNUQVRVU19OT05FO1xuICAgICAgICAgIG5ld1N0YXRlLnN0YXR1c0FjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgIG5ld1N0YXRlLm5ld1N0YXR1cyA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQXBwZWFyXG4gICAgICAgIGlmICghcHJldlByb3BzICYmIHZpc2libGUgJiYgbW90aW9uQXBwZWFyKSB7XG4gICAgICAgICAgbmV3U3RhdGUuc3RhdHVzID0gU1RBVFVTX0FQUEVBUjtcbiAgICAgICAgICBuZXdTdGF0ZS5zdGF0dXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICBuZXdTdGF0ZS5uZXdTdGF0dXMgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gRW50ZXJcbiAgICAgICAgaWYgKHByZXZQcm9wcyAmJiAhcHJldlByb3BzLnZpc2libGUgJiYgdmlzaWJsZSAmJiBtb3Rpb25FbnRlcikge1xuICAgICAgICAgIG5ld1N0YXRlLnN0YXR1cyA9IFNUQVRVU19FTlRFUjtcbiAgICAgICAgICBuZXdTdGF0ZS5zdGF0dXNBY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICBuZXdTdGF0ZS5uZXdTdGF0dXMgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gTGVhdmVcbiAgICAgICAgaWYgKHByZXZQcm9wcyAmJiBwcmV2UHJvcHMudmlzaWJsZSAmJiAhdmlzaWJsZSAmJiBtb3Rpb25MZWF2ZSB8fCAhcHJldlByb3BzICYmIG1vdGlvbkxlYXZlSW1tZWRpYXRlbHkgJiYgIXZpc2libGUgJiYgbW90aW9uTGVhdmUpIHtcbiAgICAgICAgICBuZXdTdGF0ZS5zdGF0dXMgPSBTVEFUVVNfTEVBVkU7XG4gICAgICAgICAgbmV3U3RhdGUuc3RhdHVzQWN0aXZlID0gZmFsc2U7XG4gICAgICAgICAgbmV3U3RhdGUubmV3U3RhdHVzID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBuZXdTdGF0ZTtcbiAgICAgIH1cbiAgICB9XSk7XG5cbiAgICByZXR1cm4gQ1NTTW90aW9uO1xuICB9KFJlYWN0LkNvbXBvbmVudCk7XG5cbiAgQ1NTTW90aW9uLnByb3BUeXBlcyA9IF9leHRlbmRzKHt9LCBNb3Rpb25Qcm9wVHlwZXMsIHtcblxuICAgIGludGVybmFsUmVmOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMub2JqZWN0LCBQcm9wVHlwZXMuZnVuY10pXG4gIH0pO1xuICBDU1NNb3Rpb24uZGVmYXVsdFByb3BzID0ge1xuICAgIHZpc2libGU6IHRydWUsXG4gICAgbW90aW9uRW50ZXI6IHRydWUsXG4gICAgbW90aW9uQXBwZWFyOiB0cnVlLFxuICAgIG1vdGlvbkxlYXZlOiB0cnVlLFxuICAgIHJlbW92ZU9uTGVhdmU6IHRydWVcbiAgfTtcblxuXG4gIHBvbHlmaWxsKENTU01vdGlvbik7XG5cbiAgaWYgKCFmb3J3YXJkUmVmKSB7XG4gICAgcmV0dXJuIENTU01vdGlvbjtcbiAgfVxuXG4gIHJldHVybiBSZWFjdC5mb3J3YXJkUmVmKGZ1bmN0aW9uIChwcm9wcywgcmVmKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoQ1NTTW90aW9uLCBfZXh0ZW5kcyh7IGludGVybmFsUmVmOiByZWYgfSwgcHJvcHMpKTtcbiAgfSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdlbkNTU01vdGlvbihzdXBwb3J0VHJhbnNpdGlvbik7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQW5CQTtBQXNCQTs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQWxDQTtBQW9DQTtBQUNBO0FBQ0E7QUFBQTtBQUdBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFEQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQS9DQTtBQUNBO0FBaURBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-animate/es/CSSMotion.js
