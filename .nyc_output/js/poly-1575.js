var smoothSpline = __webpack_require__(/*! ./smoothSpline */ "./node_modules/zrender/lib/graphic/helper/smoothSpline.js");

var smoothBezier = __webpack_require__(/*! ./smoothBezier */ "./node_modules/zrender/lib/graphic/helper/smoothBezier.js");

function buildPath(ctx, shape, closePath) {
  var points = shape.points;
  var smooth = shape.smooth;

  if (points && points.length >= 2) {
    if (smooth && smooth !== 'spline') {
      var controlPoints = smoothBezier(points, smooth, closePath, shape.smoothConstraint);
      ctx.moveTo(points[0][0], points[0][1]);
      var len = points.length;

      for (var i = 0; i < (closePath ? len : len - 1); i++) {
        var cp1 = controlPoints[i * 2];
        var cp2 = controlPoints[i * 2 + 1];
        var p = points[(i + 1) % len];
        ctx.bezierCurveTo(cp1[0], cp1[1], cp2[0], cp2[1], p[0], p[1]);
      }
    } else {
      if (smooth === 'spline') {
        points = smoothSpline(points, closePath);
      }

      ctx.moveTo(points[0][0], points[0][1]);

      for (var i = 1, l = points.length; i < l; i++) {
        ctx.lineTo(points[i][0], points[i][1]);
      }
    }

    closePath && ctx.closePath();
  }
}

exports.buildPath = buildPath;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvcG9seS5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL2dyYXBoaWMvaGVscGVyL3BvbHkuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIHNtb290aFNwbGluZSA9IHJlcXVpcmUoXCIuL3Ntb290aFNwbGluZVwiKTtcblxudmFyIHNtb290aEJlemllciA9IHJlcXVpcmUoXCIuL3Ntb290aEJlemllclwiKTtcblxuZnVuY3Rpb24gYnVpbGRQYXRoKGN0eCwgc2hhcGUsIGNsb3NlUGF0aCkge1xuICB2YXIgcG9pbnRzID0gc2hhcGUucG9pbnRzO1xuICB2YXIgc21vb3RoID0gc2hhcGUuc21vb3RoO1xuXG4gIGlmIChwb2ludHMgJiYgcG9pbnRzLmxlbmd0aCA+PSAyKSB7XG4gICAgaWYgKHNtb290aCAmJiBzbW9vdGggIT09ICdzcGxpbmUnKSB7XG4gICAgICB2YXIgY29udHJvbFBvaW50cyA9IHNtb290aEJlemllcihwb2ludHMsIHNtb290aCwgY2xvc2VQYXRoLCBzaGFwZS5zbW9vdGhDb25zdHJhaW50KTtcbiAgICAgIGN0eC5tb3ZlVG8ocG9pbnRzWzBdWzBdLCBwb2ludHNbMF1bMV0pO1xuICAgICAgdmFyIGxlbiA9IHBvaW50cy5sZW5ndGg7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgKGNsb3NlUGF0aCA/IGxlbiA6IGxlbiAtIDEpOyBpKyspIHtcbiAgICAgICAgdmFyIGNwMSA9IGNvbnRyb2xQb2ludHNbaSAqIDJdO1xuICAgICAgICB2YXIgY3AyID0gY29udHJvbFBvaW50c1tpICogMiArIDFdO1xuICAgICAgICB2YXIgcCA9IHBvaW50c1soaSArIDEpICUgbGVuXTtcbiAgICAgICAgY3R4LmJlemllckN1cnZlVG8oY3AxWzBdLCBjcDFbMV0sIGNwMlswXSwgY3AyWzFdLCBwWzBdLCBwWzFdKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHNtb290aCA9PT0gJ3NwbGluZScpIHtcbiAgICAgICAgcG9pbnRzID0gc21vb3RoU3BsaW5lKHBvaW50cywgY2xvc2VQYXRoKTtcbiAgICAgIH1cblxuICAgICAgY3R4Lm1vdmVUbyhwb2ludHNbMF1bMF0sIHBvaW50c1swXVsxXSk7XG5cbiAgICAgIGZvciAodmFyIGkgPSAxLCBsID0gcG9pbnRzLmxlbmd0aDsgaSA8IGw7IGkrKykge1xuICAgICAgICBjdHgubGluZVRvKHBvaW50c1tpXVswXSwgcG9pbnRzW2ldWzFdKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjbG9zZVBhdGggJiYgY3R4LmNsb3NlUGF0aCgpO1xuICB9XG59XG5cbmV4cG9ydHMuYnVpbGRQYXRoID0gYnVpbGRQYXRoOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/poly.js
