__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_styles_font__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @desk-styles/font */ "./src/app/desk/styles/font.tsx");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @assets/attachment.svg */ "./src/assets/attachment.svg");
/* harmony import */ var _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_assets_attachment_svg__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @desk-component/icons/pdf.png */ "./src/app/desk/component/icons/pdf.png");
/* harmony import */ var _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_7__);

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/history-timeline/history-timeline-item.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n      padding: 1em 0;\n\n      > img {\n          width: 104px;\n          margin: 0 .5em 0 0;\n        }\n      }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}









var timeLineItem = function timeLineItem(item, index) {
  var remark = item.remark,
      operDate = item.operDate;
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Timeline"].Item, {
    key: index,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    type: "flex",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    xs: _common__WEBPACK_IMPORTED_MODULE_4__["Utils"].getIsMobile() ? 7 : 5,
    style: {
      color: _desk_styles_font__WEBPACK_IMPORTED_MODULE_5__["FontColors"].black65
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, _common__WEBPACK_IMPORTED_MODULE_4__["DateUtils"].formatDateTime(_common__WEBPACK_IMPORTED_MODULE_4__["DateUtils"].toDate(operDate)), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    __self: this
  }, "by ", item.operUserName)), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    xs: 12,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("div", {
    style: {
      fontSize: 14,
      color: _desk_styles_font__WEBPACK_IMPORTED_MODULE_5__["FontColors"].valueAfterLabel
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, remark), _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(TimeLineItemImageList, {
    item: item,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }))));
};

var TimeLineItemImageList = function TimeLineItemImageList(_ref) {
  var item = _ref.item;
  var attaches = item.attaches;

  if (!attaches || !attaches.length) {
    return null;
  }

  var StyleImages = _common_3rd__WEBPACK_IMPORTED_MODULE_2__["Styled"].div(_templateObject());
  return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement(StyleImages, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: this
  }, attaches.map(function (attach, index) {
    var url;

    var isImage = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(attach, "attachDetails.mimeType").includes("image/");

    var isPdf = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(attach, "attachDetails.mimeType").includes("pdf");

    var id = lodash__WEBPACK_IMPORTED_MODULE_1___default.a.get(attach, "attachFid");

    if (isPdf) {
      url = _desk_component_icons_pdf_png__WEBPACK_IMPORTED_MODULE_7___default.a;
    } else if (isImage) {
      url = _common__WEBPACK_IMPORTED_MODULE_4__["Ajax"].getServiceLocation("/storage/".concat(id, "?access_token=").concat(_common__WEBPACK_IMPORTED_MODULE_4__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_4__["Consts"].AUTH_KEY)));
    } else {
      url = _assets_attachment_svg__WEBPACK_IMPORTED_MODULE_6___default.a;
    }

    return _common_3rd__WEBPACK_IMPORTED_MODULE_2__["React"].createElement("img", {
      key: id,
      src: url,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    });
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (timeLineItem);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2hpc3RvcnktdGltZWxpbmUvaGlzdG9yeS10aW1lbGluZS1pdGVtLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9oaXN0b3J5LXRpbWVsaW5lL2hpc3RvcnktdGltZWxpbmUtaXRlbS50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vbWVudCBmcm9tIFwibW9tZW50XCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5cbmltcG9ydCB7IENvbCwgUm93LCBUaW1lbGluZSB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgeyBBamF4LCBDb25zdHMsIERhdGVVdGlscywgU3RvcmFnZSwgVXRpbHMgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IHsgRm9udENvbG9ycyB9IGZyb20gXCJAZGVzay1zdHlsZXMvZm9udFwiO1xuaW1wb3J0IGF0dGFjaG1lbnRJbWFnZVVybCBmcm9tIFwiQGFzc2V0cy9hdHRhY2htZW50LnN2Z1wiO1xuaW1wb3J0IGF0dGFjaG1lbnRQZGYgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9pY29ucy9wZGYucG5nXCI7XG5cbmNvbnN0IHRpbWVMaW5lSXRlbSA9IChpdGVtOiBhbnksIGluZGV4OiBudW1iZXIpID0+IHtcbiAgY29uc3QgeyByZW1hcmssIG9wZXJEYXRlIH0gPSBpdGVtO1xuICByZXR1cm4gPFRpbWVsaW5lLkl0ZW0ga2V5PXtpbmRleH0+XG4gICAgPFJvdyB0eXBlPVwiZmxleFwiPlxuICAgICAgPENvbCB4cz17VXRpbHMuZ2V0SXNNb2JpbGUoKSA/IDcgOiA1fSBzdHlsZT17eyBjb2xvcjogRm9udENvbG9ycy5ibGFjazY1IH19PlxuICAgICAgICB7XG4gICAgICAgICAgRGF0ZVV0aWxzLmZvcm1hdERhdGVUaW1lKERhdGVVdGlscy50b0RhdGUob3BlckRhdGUpKVxuICAgICAgICB9XG4gICAgICAgIDxwPmJ5IHtpdGVtLm9wZXJVc2VyTmFtZX08L3A+XG4gICAgICA8L0NvbD5cbiAgICAgIDxDb2wgeHM9ezEyfT5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogMTQsIGNvbG9yOiBGb250Q29sb3JzLnZhbHVlQWZ0ZXJMYWJlbCB9fT57cmVtYXJrfTwvZGl2PlxuICAgICAgICA8VGltZUxpbmVJdGVtSW1hZ2VMaXN0IGl0ZW09e2l0ZW19Lz5cbiAgICAgIDwvQ29sPlxuICAgIDwvUm93PlxuICA8L1RpbWVsaW5lLkl0ZW0+O1xufTtcblxuY29uc3QgVGltZUxpbmVJdGVtSW1hZ2VMaXN0ID0gKHsgaXRlbSB9OiBhbnkpID0+IHtcbiAgY29uc3QgeyBhdHRhY2hlcyB9ID0gaXRlbTtcbiAgaWYgKCFhdHRhY2hlcyB8fCAhYXR0YWNoZXMubGVuZ3RoKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBjb25zdCBTdHlsZUltYWdlcyA9IFN0eWxlZC5kaXZgXG4gICAgICBwYWRkaW5nOiAxZW0gMDtcblxuICAgICAgPiBpbWcge1xuICAgICAgICAgIHdpZHRoOiAxMDRweDtcbiAgICAgICAgICBtYXJnaW46IDAgLjVlbSAwIDA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgYDtcblxuICByZXR1cm4gKFxuICAgIDxTdHlsZUltYWdlcz5cbiAgICAgIHthdHRhY2hlcy5tYXAoKGF0dGFjaDogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgIGxldCB1cmw7XG4gICAgICAgIGNvbnN0IGlzSW1hZ2UgPSBfLmdldChhdHRhY2gsIFwiYXR0YWNoRGV0YWlscy5taW1lVHlwZVwiKS5pbmNsdWRlcyhcImltYWdlL1wiKTtcbiAgICAgICAgY29uc3QgaXNQZGYgPSBfLmdldChhdHRhY2gsIFwiYXR0YWNoRGV0YWlscy5taW1lVHlwZVwiKS5pbmNsdWRlcyhcInBkZlwiKTtcbiAgICAgICAgY29uc3QgaWQgPSBfLmdldChhdHRhY2gsIFwiYXR0YWNoRmlkXCIpO1xuICAgICAgICBpZiAoaXNQZGYpIHtcbiAgICAgICAgICB1cmwgPSBhdHRhY2htZW50UGRmO1xuICAgICAgICB9IGVsc2UgaWYgKGlzSW1hZ2UpIHtcbiAgICAgICAgICB1cmwgPSBBamF4LmdldFNlcnZpY2VMb2NhdGlvbihcbiAgICAgICAgICAgIGAvc3RvcmFnZS8ke2lkfT9hY2Nlc3NfdG9rZW49JHtTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpfWAsXG4gICAgICAgICAgKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB1cmwgPSBhdHRhY2htZW50SW1hZ2VVcmw7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gPGltZyBrZXk9e2lkfSBzcmM9e3VybH0vPjtcbiAgICAgIH0pfVxuICAgIDwvU3R5bGVJbWFnZXM+XG4gICk7XG59O1xuXG5cbmV4cG9ydCBkZWZhdWx0IHRpbWVMaW5lSXRlbTtcblxuXG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUlBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFVQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBRUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/history-timeline/history-timeline-item.tsx
