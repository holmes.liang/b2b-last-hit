__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PAYMENT_SLIP", function() { return PAYMENT_SLIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IC", function() { return IC; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INSPECTION", function() { return INSPECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BUSINESS_REGISTRATION", function() { return BUSINESS_REGISTRATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OVERDUE_DEBTS", function() { return OVERDUE_DEBTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOMERS_CREDIT_LIMIT", function() { return CUSTOMERS_CREDIT_LIMIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REGISTRATION_BOOK", function() { return REGISTRATION_BOOK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OTHERS", function() { return OTHERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DIRECT_DEBIT_AUTH_FORM", function() { return DIRECT_DEBIT_AUTH_FORM; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BANK_BOOK", function() { return BANK_BOOK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INSURED_ID", function() { return INSURED_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PAYER_ID", function() { return PAYER_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MEDICAL_REPORT", function() { return MEDICAL_REPORT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPLICATION_FORM", function() { return APPLICATION_FORM; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SURVEY_REPORTS", function() { return SURVEY_REPORTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VOUCHER_TYPE_IMAGE", function() { return VOUCHER_TYPE_IMAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FACUTATIVE_AGREEMENT", function() { return FACUTATIVE_AGREEMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "docTypesAll", function() { return docTypesAll; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TREATY_SCANCOPY", function() { return TREATY_SCANCOPY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SUPPORTING", function() { return SUPPORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForAddQuoteShare", function() { return getDocTypeForAddQuoteShare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeSupporting", function() { return getDocTypeSupporting; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transTypeToName", function() { return transTypeToName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transOtherTypeToName", function() { return transOtherTypeToName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForVMI", function() { return getDocTypeForVMI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeAssignment", function() { return getDocTypeAssignment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForVMIClass1", function() { return getDocTypeForVMIClass1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForWlsc", function() { return getDocTypeForWlsc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForGTA", function() { return getDocTypeForGTA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForCard", function() { return getDocTypeForCard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForGHM", function() { return getDocTypeForGHM; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForBOSS", function() { return getDocTypeForBOSS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForTcc", function() { return getDocTypeForTcc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForFacIn", function() { return getDocTypeForFacIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForIar", function() { return getDocTypeForIar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForSgpVMI", function() { return getDocTypeForSgpVMI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForSgpVMIClass1", function() { return getDocTypeForSgpVMIClass1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForPaymentNormal", function() { return getDocTypeForPaymentNormal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForPaymentDebit", function() { return getDocTypeForPaymentDebit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDocTypeForPaymentBOSS", function() { return getDocTypeForPaymentBOSS; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _common_attachment_type__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @common/attachment-type */ "./src/common/attachment-type.tsx");




// for 出单
var PAYMENT_SLIP = {
  docType: "PAYMENT_SLIP",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Payment Slip").thai("หลักฐานการชำระเงิน").my("လစာစလစ်").getMessage()
};
var IC = {
  docType: "IC",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("ID Card").thai("เอกสารยืนยันตัวตน").my("အိုင်ဒီကဒ်").getMessage()
};
var INSPECTION = {
  docType: "INSPECTION",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Inspection Photos").thai("ภาพถ่ายรถยนต์").my("စစ်ဆေးရေးဓါတ်ပုံမျာ").getMessage()
};
var BUSINESS_REGISTRATION = {
  docType: "BUSINESS_REGISTRATION",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Business Registration").thai("Business Registration").my("Business Registration").getMessage()
};
var OVERDUE_DEBTS = {
  docType: "OVERDUE_DEBTS",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("List of overdue debts").thai("List of overdue debts").getMessage()
};
var CUSTOMERS_CREDIT_LIMIT = {
  docType: "CUSTOMERS_CREDIT_LIMIT",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Credit limit of all customers").thai("Credit limit of all customers").getMessage()
};
var REGISTRATION_BOOK = {
  docType: "REGISTRATION_BOOK",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Registration Book").thai("คู่มือรถ").my("မှတ်ပုံတင်စာအုပ်").getMessage()
};
var OTHERS = {
  docType: "OTHERS",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Others").thai("อื่น ๆ").my("တခွားသူ").getMessage()
};
var DIRECT_DEBIT_AUTH_FORM = {
  docType: "DIRECT_DEBIT_AUTH_FORM",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Debit Authorization").thai("เอกสารการหักผ่านธนาคาร").my("debit ခွင့်ပြုချက်").getMessage()
};
var BANK_BOOK = {
  docType: "BANK_BOOK",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Bank Book").thai("สมุดบัญชีธนาคาร").my("ဘဏ်စာအုပ်").getMessage()
};
var INSURED_ID = {
  docType: "INSURED_ID",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Insured ID").thai("การระบุผู้ประกันตน").getMessage()
};
var PAYER_ID = {
  docType: "PAYER_ID",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Payer ID").thai("บัตรประจำตัวผู้ชำระเงิน").getMessage()
};
var MEDICAL_REPORT = {
  docType: "MEDICAL_REPORT",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Medical Report").thai("รายงานทางการแพทย์").getMessage()
};
var APPLICATION_FORM = {
  docType: "APPLICATION_FORM",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Application Form").thai("Application Form").getMessage()
};
var SURVEY_REPORTS = {
  docType: "SURVEY_REPORTS",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Survey Reports").thai("Survey Reports").getMessage()
};
var VOUCHER_TYPE_IMAGE = {
  docType: "VOUCHER_TYPE_IMAGE",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Image").thai("Image").getMessage()
};
var FACUTATIVE_AGREEMENT = {
  docType: "FACUTATIVE_AGREEMENT",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Facutative Agreement").thai("Facutative Agreement").getMessage()
};
var docTypesAll = [VOUCHER_TYPE_IMAGE, FACUTATIVE_AGREEMENT, APPLICATION_FORM, SURVEY_REPORTS, PAYMENT_SLIP, IC, REGISTRATION_BOOK, OTHERS, DIRECT_DEBIT_AUTH_FORM, PAYER_ID, MEDICAL_REPORT, BUSINESS_REGISTRATION, INSPECTION].concat(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesFG), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesFire), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesPa_1), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesPa_2), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesPala), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesWic_1), Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_1__["default"])(_common_attachment_type__WEBPACK_IMPORTED_MODULE_3__["default"].PaymentDocTypesWic_2));
var TREATY_SCANCOPY = {
  docType: "TREATY_SCANCOPY",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Treaty ScanCopy").thai("Treaty ScanCopy").getMessage()
};
var SUPPORTING = {
  docType: "SUPPORTING",
  docTypeDesc: _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Supporting Documents").thai("Supporting Documents").getMessage()
}; // for

function getDocTypeForAddQuoteShare() {
  return mergeRequired([TREATY_SCANCOPY], [false]);
}
function getDocTypeSupporting() {
  return mergeRequired([SUPPORTING], [false]);
}
function transTypeToName(docType) {
  var findeOne = docTypesAll.find(function (item) {
    return item.docType === docType;
  });
  if (!findeOne) return "";
  return findeOne.docTypeDesc;
}
function transOtherTypeToName(docType) {
  var findeOne = docTypesAll.find(function (item) {
    return item.docType === docType;
  });
  if (!findeOne) return _common__WEBPACK_IMPORTED_MODULE_2__["Language"].en("Policy").thai("กรมธรรม์ประกันภัย").getMessage();
  return findeOne.docTypeDesc;
}

function mergeRequired(docTypeList, requiredList) {
  return docTypeList.map(function (item, index) {
    return Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
      required: requiredList[index]
    });
  });
}

function getDocTypeForVMI() {
  return mergeRequired([IC, REGISTRATION_BOOK, PAYMENT_SLIP, OTHERS], [true, false, false, false]);
}
function getDocTypeAssignment() {
  return mergeRequired([MEDICAL_REPORT, OTHERS], [false, false]);
}
function getDocTypeForVMIClass1() {
  return mergeRequired([IC, INSPECTION, REGISTRATION_BOOK, PAYMENT_SLIP, OTHERS], [true, true, false, false, false]);
}
function getDocTypeForWlsc() {
  return mergeRequired([IC, PAYER_ID, MEDICAL_REPORT, OTHERS], [true, false, false, false]);
}
function getDocTypeForGTA() {
  return mergeRequired([IC, PAYMENT_SLIP, OTHERS], [true, false, false]);
}
function getDocTypeForCard() {
  return mergeRequired([IC], [true]);
}
function getDocTypeForGHM() {
  return mergeRequired([IC, PAYMENT_SLIP, OTHERS], [true, false, false]);
}
function getDocTypeForBOSS() {
  return mergeRequired([BUSINESS_REGISTRATION, OTHERS], [true, false]);
}
function getDocTypeForTcc() {
  return mergeRequired([BUSINESS_REGISTRATION, OVERDUE_DEBTS, CUSTOMERS_CREDIT_LIMIT], [true, false, false]);
}
function getDocTypeForFacIn() {
  return mergeRequired([FACUTATIVE_AGREEMENT, SURVEY_REPORTS, OTHERS], [true, false, false]);
}
function getDocTypeForIar() {
  return mergeRequired([BUSINESS_REGISTRATION, APPLICATION_FORM, SURVEY_REPORTS, OTHERS], [true, false, false, false]);
}
function getDocTypeForSgpVMI() {
  return mergeRequired([IC, OTHERS], [true, false]);
}
function getDocTypeForSgpVMIClass1() {
  return mergeRequired([IC, INSPECTION, OTHERS], [true, true, false]);
}
function getDocTypeForPaymentNormal() {
  return mergeRequired([PAYMENT_SLIP], [false]);
}
function getDocTypeForPaymentDebit() {
  return mergeRequired([DIRECT_DEBIT_AUTH_FORM, PAYMENT_SLIP, BANK_BOOK], [false, false, false]);
}
function getDocTypeForPaymentBOSS() {
  return mergeRequired([DIRECT_DEBIT_AUTH_FORM, PAYMENT_SLIP], [false, false]);
}//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L2F0dGFjaG1lbnQvZG9jLXR5cGVzLnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9hdHRhY2htZW50L2RvYy10eXBlcy50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTGFuZ3VhZ2UgfSBmcm9tIFwiQGNvbW1vblwiO1xuaW1wb3J0IFR5cGVzIGZyb20gXCJAY29tbW9uL2F0dGFjaG1lbnQtdHlwZVwiO1xuXG5pbnRlcmZhY2UgSURvY1R5cGUge1xuICBkb2NUeXBlOiBzdHJpbmc7XG4gIGRvY1R5cGVEZXNjOiBzdHJpbmc7XG4gIHJlcXVpcmVkPzogYm9vbGVhbjtcbn1cblxuLy8gZm9yIOWHuuWNlVxuXG5leHBvcnQgY29uc3QgUEFZTUVOVF9TTElQOiBJRG9jVHlwZSA9IHtcbiAgZG9jVHlwZTogXCJQQVlNRU5UX1NMSVBcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiUGF5bWVudCBTbGlwXCIpLnRoYWkoXCLguKvguKXguLHguIHguJDguLLguJnguIHguLLguKPguIrguLPguKPguLDguYDguIfguLTguJlcIikubXkoXCLhgJzhgIXhgKzhgIXhgJzhgIXhgLpcIikuZ2V0TWVzc2FnZSgpLFxufTtcblxuZXhwb3J0IGNvbnN0IElDID0ge1xuICBkb2NUeXBlOiBcIklDXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIklEIENhcmRcIikudGhhaShcIuC5gOC4reC4geC4quC4suC4o+C4ouC4t+C4meC4ouC4seC4meC4leC4seC4p+C4leC4mVwiKS5teShcIuGAoeGAreGAr+GAhOGAuuGAkuGAruGAgOGAkuGAulwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgSU5TUEVDVElPTiA9IHtcbiAgZG9jVHlwZTogXCJJTlNQRUNUSU9OXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIkluc3BlY3Rpb24gUGhvdG9zXCIpLnRoYWkoXCLguKDguLLguJ7guJbguYjguLLguKLguKPguJbguKLguJnguJXguYxcIikubXkoXCLhgIXhgIXhgLrhgIbhgLHhgLjhgJvhgLHhgLjhgJPhgKvhgJDhgLrhgJXhgK/hgLbhgJnhgLvhgKxcIikuZ2V0TWVzc2FnZSgpLFxufTtcblxuZXhwb3J0IGNvbnN0IEJVU0lORVNTX1JFR0lTVFJBVElPTiA9IHtcbiAgZG9jVHlwZTogXCJCVVNJTkVTU19SRUdJU1RSQVRJT05cIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiQnVzaW5lc3MgUmVnaXN0cmF0aW9uXCIpLnRoYWkoXCJCdXNpbmVzcyBSZWdpc3RyYXRpb25cIikubXkoXCJCdXNpbmVzcyBSZWdpc3RyYXRpb25cIikuZ2V0TWVzc2FnZSgpLFxufTtcblxuZXhwb3J0IGNvbnN0IE9WRVJEVUVfREVCVFMgPSB7XG4gIGRvY1R5cGU6IFwiT1ZFUkRVRV9ERUJUU1wiLFxuICBkb2NUeXBlRGVzYzogTGFuZ3VhZ2UuZW4oXCJMaXN0IG9mIG92ZXJkdWUgZGVidHNcIikudGhhaShcIkxpc3Qgb2Ygb3ZlcmR1ZSBkZWJ0c1wiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgQ1VTVE9NRVJTX0NSRURJVF9MSU1JVCA9IHtcbiAgZG9jVHlwZTogXCJDVVNUT01FUlNfQ1JFRElUX0xJTUlUXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIkNyZWRpdCBsaW1pdCBvZiBhbGwgY3VzdG9tZXJzXCIpLnRoYWkoXCJDcmVkaXQgbGltaXQgb2YgYWxsIGN1c3RvbWVyc1wiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgUkVHSVNUUkFUSU9OX0JPT0sgPSB7XG4gIGRvY1R5cGU6IFwiUkVHSVNUUkFUSU9OX0JPT0tcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiUmVnaXN0cmF0aW9uIEJvb2tcIikudGhhaShcIuC4hOC4ueC5iOC4oeC4t+C4reC4o+C4llwiKS5teShcIuGAmeGAvuGAkOGAuuGAleGAr+GAtuGAkOGAhOGAuuGAheGArOGAoeGAr+GAleGAulwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgT1RIRVJTID0ge1xuICBkb2NUeXBlOiBcIk9USEVSU1wiLFxuICBkb2NUeXBlRGVzYzogTGFuZ3VhZ2UuZW4oXCJPdGhlcnNcIikudGhhaShcIuC4reC4t+C5iOC4mSDguYZcIikubXkoXCLhgJDhgIHhgL3hgKzhgLjhgJ7hgLBcIikuZ2V0TWVzc2FnZSgpLFxufTtcblxuXG5leHBvcnQgY29uc3QgRElSRUNUX0RFQklUX0FVVEhfRk9STSA9IHtcbiAgZG9jVHlwZTogXCJESVJFQ1RfREVCSVRfQVVUSF9GT1JNXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIkRlYml0IEF1dGhvcml6YXRpb25cIilcbiAgICAudGhhaShcIuC5gOC4reC4geC4quC4suC4o+C4geC4suC4o+C4q+C4seC4geC4nOC5iOC4suC4meC4mOC4meC4suC4hOC4suC4o1wiKVxuICAgIC5teShcImRlYml0IOGAgeGAveGAhOGAuuGAt+GAleGAvOGAr+GAgeGAu+GAgOGAulwiKVxuICAgIC5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgQkFOS19CT09LID0ge1xuICBkb2NUeXBlOiBcIkJBTktfQk9PS1wiLFxuICBkb2NUeXBlRGVzYzogTGFuZ3VhZ2UuZW4oXCJCYW5rIEJvb2tcIilcbiAgICAudGhhaShcIuC4quC4oeC4uOC4lOC4muC4seC4jeC4iuC4teC4mOC4meC4suC4hOC4suC4o1wiKVxuICAgIC5teShcIuGAmOGAj+GAuuGAheGArOGAoeGAr+GAleGAulwiKVxuICAgIC5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgSU5TVVJFRF9JRCA9IHtcbiAgZG9jVHlwZTogXCJJTlNVUkVEX0lEXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIkluc3VyZWQgSURcIikudGhhaShcIuC4geC4suC4o+C4o+C4sOC4muC4uOC4nOC4ueC5ieC4m+C4o+C4sOC4geC4seC4meC4leC4mVwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgUEFZRVJfSUQgPSB7XG4gIGRvY1R5cGU6IFwiUEFZRVJfSURcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiUGF5ZXIgSURcIikudGhhaShcIuC4muC4seC4leC4o+C4m+C4o+C4sOC4iOC4s+C4leC4seC4p+C4nOC4ueC5ieC4iuC4s+C4o+C4sOC5gOC4h+C4tOC4mVwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5cbmV4cG9ydCBjb25zdCBNRURJQ0FMX1JFUE9SVCA9IHtcbiAgZG9jVHlwZTogXCJNRURJQ0FMX1JFUE9SVFwiLFxuICBkb2NUeXBlRGVzYzogTGFuZ3VhZ2UuZW4oXCJNZWRpY2FsIFJlcG9ydFwiKS50aGFpKFwi4Lij4Liy4Lii4LiH4Liy4LiZ4LiX4Liy4LiH4LiB4Liy4Lij4LmB4Lie4LiX4Lii4LmMXCIpLmdldE1lc3NhZ2UoKSxcbn07XG5cbmV4cG9ydCBjb25zdCBBUFBMSUNBVElPTl9GT1JNID0ge1xuICBkb2NUeXBlOiBcIkFQUExJQ0FUSU9OX0ZPUk1cIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiQXBwbGljYXRpb24gRm9ybVwiKS50aGFpKFwiQXBwbGljYXRpb24gRm9ybVwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgU1VSVkVZX1JFUE9SVFMgPSB7XG4gIGRvY1R5cGU6IFwiU1VSVkVZX1JFUE9SVFNcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiU3VydmV5IFJlcG9ydHNcIikudGhhaShcIlN1cnZleSBSZXBvcnRzXCIpLmdldE1lc3NhZ2UoKSxcbn07XG5cbmV4cG9ydCBjb25zdCBWT1VDSEVSX1RZUEVfSU1BR0UgPSB7XG4gIGRvY1R5cGU6IFwiVk9VQ0hFUl9UWVBFX0lNQUdFXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIkltYWdlXCIpLnRoYWkoXCJJbWFnZVwiKS5nZXRNZXNzYWdlKCksXG59O1xuXG5leHBvcnQgY29uc3QgRkFDVVRBVElWRV9BR1JFRU1FTlQgPSB7XG4gIGRvY1R5cGU6IFwiRkFDVVRBVElWRV9BR1JFRU1FTlRcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiRmFjdXRhdGl2ZSBBZ3JlZW1lbnRcIikudGhhaShcIkZhY3V0YXRpdmUgQWdyZWVtZW50XCIpLmdldE1lc3NhZ2UoKSxcbn07XG5cbmV4cG9ydCBjb25zdCBkb2NUeXBlc0FsbCA9IFtcbiAgVk9VQ0hFUl9UWVBFX0lNQUdFLFxuICBGQUNVVEFUSVZFX0FHUkVFTUVOVCxcbiAgQVBQTElDQVRJT05fRk9STSwgU1VSVkVZX1JFUE9SVFMsXG4gIFBBWU1FTlRfU0xJUCwgSUMsIFJFR0lTVFJBVElPTl9CT09LLCBPVEhFUlMsIERJUkVDVF9ERUJJVF9BVVRIX0ZPUk0sXG4gIFBBWUVSX0lELCBNRURJQ0FMX1JFUE9SVCwgQlVTSU5FU1NfUkVHSVNUUkFUSU9OLCBJTlNQRUNUSU9OLCAuLi5UeXBlcy5QYXltZW50RG9jVHlwZXNGRyxcbiAgLi4uVHlwZXMuUGF5bWVudERvY1R5cGVzRmlyZSwgLi4uVHlwZXMuUGF5bWVudERvY1R5cGVzUGFfMSwgLi4uVHlwZXMuUGF5bWVudERvY1R5cGVzUGFfMixcbiAgLi4uVHlwZXMuUGF5bWVudERvY1R5cGVzUGFsYSwgLi4uVHlwZXMuUGF5bWVudERvY1R5cGVzV2ljXzEsIC4uLlR5cGVzLlBheW1lbnREb2NUeXBlc1dpY18yLFxuXTtcbmV4cG9ydCBjb25zdCBUUkVBVFlfU0NBTkNPUFkgPSB7XG4gIGRvY1R5cGU6IFwiVFJFQVRZX1NDQU5DT1BZXCIsXG4gIGRvY1R5cGVEZXNjOiBMYW5ndWFnZS5lbihcIlRyZWF0eSBTY2FuQ29weVwiKS50aGFpKFwiVHJlYXR5IFNjYW5Db3B5XCIpLmdldE1lc3NhZ2UoKSxcbn07XG5cbmV4cG9ydCBjb25zdCBTVVBQT1JUSU5HID0ge1xuICBkb2NUeXBlOiBcIlNVUFBPUlRJTkdcIixcbiAgZG9jVHlwZURlc2M6IExhbmd1YWdlLmVuKFwiU3VwcG9ydGluZyBEb2N1bWVudHNcIikudGhhaShcIlN1cHBvcnRpbmcgRG9jdW1lbnRzXCIpLmdldE1lc3NhZ2UoKSxcbn07XG5cbi8vIGZvclxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JBZGRRdW90ZVNoYXJlKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbVFJFQVRZX1NDQU5DT1BZXSxcbiAgICBbZmFsc2VdLFxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RG9jVHlwZVN1cHBvcnRpbmcoKSB7XG4gIHJldHVybiBtZXJnZVJlcXVpcmVkKFxuICAgIFtTVVBQT1JUSU5HXSxcbiAgICBbZmFsc2VdLFxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdHJhbnNUeXBlVG9OYW1lKGRvY1R5cGU6IHN0cmluZykge1xuICBjb25zdCBmaW5kZU9uZSA9IGRvY1R5cGVzQWxsLmZpbmQoKGl0ZW06IElEb2NUeXBlKSA9PiBpdGVtLmRvY1R5cGUgPT09IGRvY1R5cGUpO1xuICBpZiAoIWZpbmRlT25lKSByZXR1cm4gXCJcIjtcbiAgcmV0dXJuIGZpbmRlT25lLmRvY1R5cGVEZXNjO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gdHJhbnNPdGhlclR5cGVUb05hbWUoZG9jVHlwZTogc3RyaW5nKSB7XG4gIGNvbnN0IGZpbmRlT25lID0gZG9jVHlwZXNBbGwuZmluZCgoaXRlbTogSURvY1R5cGUpID0+IGl0ZW0uZG9jVHlwZSA9PT0gZG9jVHlwZSk7XG4gIGlmICghZmluZGVPbmUpIHJldHVybiBMYW5ndWFnZS5lbihcIlBvbGljeVwiKS50aGFpKFwi4LiB4Lij4Lih4LiY4Lij4Lij4Lih4LmM4Lib4Lij4Liw4LiB4Lix4LiZ4Lig4Lix4LiiXCIpLmdldE1lc3NhZ2UoKTtcbiAgcmV0dXJuIGZpbmRlT25lLmRvY1R5cGVEZXNjO1xufVxuXG5mdW5jdGlvbiBtZXJnZVJlcXVpcmVkKGRvY1R5cGVMaXN0OiBJRG9jVHlwZVtdLCByZXF1aXJlZExpc3Q6IGJvb2xlYW5bXSk6IElEb2NUeXBlW10ge1xuICByZXR1cm4gZG9jVHlwZUxpc3QubWFwKChpdGVtOiBJRG9jVHlwZSwgaW5kZXg6IG51bWJlcikgPT4gKHsgLi4uaXRlbSwgcmVxdWlyZWQ6IHJlcXVpcmVkTGlzdFtpbmRleF0gfSkpO1xufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yVk1JKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbSUMsIFJFR0lTVFJBVElPTl9CT09LLCBQQVlNRU5UX1NMSVAsIE9USEVSU10sXG4gICAgW3RydWUsIGZhbHNlLCBmYWxzZSwgZmFsc2VdLFxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RG9jVHlwZUFzc2lnbm1lbnQoKSB7XG4gIHJldHVybiBtZXJnZVJlcXVpcmVkKFxuICAgIFtNRURJQ0FMX1JFUE9SVCwgT1RIRVJTXSxcbiAgICBbZmFsc2UsIGZhbHNlXSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JWTUlDbGFzczEoKSB7XG4gIHJldHVybiBtZXJnZVJlcXVpcmVkKFxuICAgIFtJQywgSU5TUEVDVElPTiwgUkVHSVNUUkFUSU9OX0JPT0ssIFBBWU1FTlRfU0xJUCwgT1RIRVJTXSxcbiAgICBbdHJ1ZSwgdHJ1ZSwgZmFsc2UsIGZhbHNlLCBmYWxzZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yV2xzYygpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0lDLCBQQVlFUl9JRCwgTUVESUNBTF9SRVBPUlQsIE9USEVSU10sXG4gICAgW3RydWUsIGZhbHNlLCBmYWxzZSwgZmFsc2VdLFxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RG9jVHlwZUZvckdUQSgpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0lDLCBQQVlNRU5UX1NMSVAsIE9USEVSU10sXG4gICAgW3RydWUsIGZhbHNlLCBmYWxzZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yQ2FyZCgpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0lDXSxcbiAgICBbdHJ1ZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yR0hNKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbSUMsIFBBWU1FTlRfU0xJUCwgT1RIRVJTXSxcbiAgICBbdHJ1ZSwgZmFsc2UsIGZhbHNlXSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JCT1NTKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbQlVTSU5FU1NfUkVHSVNUUkFUSU9OLCBPVEhFUlNdLFxuICAgIFt0cnVlLCBmYWxzZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yVGNjKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbQlVTSU5FU1NfUkVHSVNUUkFUSU9OLCBPVkVSRFVFX0RFQlRTLCBDVVNUT01FUlNfQ1JFRElUX0xJTUlUXSxcbiAgICBbdHJ1ZSwgZmFsc2UsIGZhbHNlXSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JGYWNJbigpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0ZBQ1VUQVRJVkVfQUdSRUVNRU5ULCBTVVJWRVlfUkVQT1JUUywgT1RIRVJTXSxcbiAgICBbdHJ1ZSwgZmFsc2UsIGZhbHNlXSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JJYXIoKSB7XG4gIHJldHVybiBtZXJnZVJlcXVpcmVkKFxuICAgIFtCVVNJTkVTU19SRUdJU1RSQVRJT04sIEFQUExJQ0FUSU9OX0ZPUk0sIFNVUlZFWV9SRVBPUlRTLCBPVEhFUlNdLFxuICAgIFt0cnVlLCBmYWxzZSwgZmFsc2UsIGZhbHNlXSxcbiAgKTtcbn1cblxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RG9jVHlwZUZvclNncFZNSSgpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0lDLCBPVEhFUlNdLFxuICAgIFt0cnVlLCBmYWxzZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yU2dwVk1JQ2xhc3MxKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbSUMsIElOU1BFQ1RJT04sIE9USEVSU10sXG4gICAgW3RydWUsIHRydWUsIGZhbHNlXSxcbiAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldERvY1R5cGVGb3JQYXltZW50Tm9ybWFsKCkge1xuICByZXR1cm4gbWVyZ2VSZXF1aXJlZChcbiAgICBbUEFZTUVOVF9TTElQXSxcbiAgICBbZmFsc2VdLFxuICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0RG9jVHlwZUZvclBheW1lbnREZWJpdCgpIHtcbiAgcmV0dXJuIG1lcmdlUmVxdWlyZWQoXG4gICAgW0RJUkVDVF9ERUJJVF9BVVRIX0ZPUk0sIFBBWU1FTlRfU0xJUCwgQkFOS19CT09LXSxcbiAgICBbZmFsc2UsIGZhbHNlLCBmYWxzZV0sXG4gICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXREb2NUeXBlRm9yUGF5bWVudEJPU1MoKSB7XG4gIHJldHVybiBtZXJnZVJlcXVpcmVkKFxuICAgIFtESVJFQ1RfREVCSVRfQVVUSF9GT1JNLCBQQVlNRU5UX1NMSVBdLFxuICAgIFtmYWxzZSwgZmFsc2VdLFxuICApO1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFRQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFGQTtBQVFBO0FBQ0E7QUFDQTtBQUZBO0FBUUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQU1BO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQVNBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUtBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFHQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUE7QUFFQTtBQUNBO0FBSUEiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./src/app/desk/component/attachment/doc-types.tsx
