__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getActiveKey", function() { return getActiveKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveRef", function() { return saveRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubPopupMenu", function() { return SubPopupMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mini_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");
/* harmony import */ var mini_store__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(mini_store__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-util/es/KeyCode */ "./node_modules/rc-util/es/KeyCode.js");
/* harmony import */ var rc_util_es_createChainedFunction__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-util/es/createChainedFunction */ "./node_modules/rc-util/es/createChainedFunction.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! shallowequal */ "./node_modules/shallowequal/index.js");
/* harmony import */ var shallowequal__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(shallowequal__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./util */ "./node_modules/rc-menu/es/util.js");
/* harmony import */ var _DOMWrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./DOMWrap */ "./node_modules/rc-menu/es/DOMWrap.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}










function allDisabled(arr) {
  if (!arr.length) {
    return true;
  }

  return arr.every(function (c) {
    return !!c.props.disabled;
  });
}

function updateActiveKey(store, menuId, activeKey) {
  var state = store.getState();
  store.setState({
    activeKey: _objectSpread({}, state.activeKey, _defineProperty({}, menuId, activeKey))
  });
}

function getEventKey(props) {
  // when eventKey not available ,it's menu and return menu id '0-menu-'
  return props.eventKey || '0-menu-';
}

function getActiveKey(props, originalActiveKey) {
  var activeKey = originalActiveKey;
  var children = props.children,
      eventKey = props.eventKey;

  if (activeKey) {
    var found;
    Object(_util__WEBPACK_IMPORTED_MODULE_6__["loopMenuItem"])(children, function (c, i) {
      if (c && c.props && !c.props.disabled && activeKey === Object(_util__WEBPACK_IMPORTED_MODULE_6__["getKeyFromChildrenIndex"])(c, eventKey, i)) {
        found = true;
      }
    });

    if (found) {
      return activeKey;
    }
  }

  activeKey = null;

  if (props.defaultActiveFirst) {
    Object(_util__WEBPACK_IMPORTED_MODULE_6__["loopMenuItem"])(children, function (c, i) {
      if (!activeKey && c && !c.props.disabled) {
        activeKey = Object(_util__WEBPACK_IMPORTED_MODULE_6__["getKeyFromChildrenIndex"])(c, eventKey, i);
      }
    });
    return activeKey;
  }

  return activeKey;
}
function saveRef(c) {
  if (c) {
    var index = this.instanceArray.indexOf(c);

    if (index !== -1) {
      // update component if it's already inside instanceArray
      this.instanceArray[index] = c;
    } else {
      // add component if it's not in instanceArray yet;
      this.instanceArray.push(c);
    }
  }
}
var SubPopupMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SubPopupMenu, _React$Component);

  function SubPopupMenu(props) {
    var _this;

    _classCallCheck(this, SubPopupMenu);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SubPopupMenu).call(this, props));
    /**
     * all keyboard events callbacks run from here at first
     *
     * note:
     *  This legacy code that `onKeyDown` is called by parent instead of dom self.
     *  which need return code to check if this event is handled
     */

    _this.onKeyDown = function (e, callback) {
      var keyCode = e.keyCode;
      var handled;

      _this.getFlatInstanceArray().forEach(function (obj) {
        if (obj && obj.props.active && obj.onKeyDown) {
          handled = obj.onKeyDown(e);
        }
      });

      if (handled) {
        return 1;
      }

      var activeItem = null;

      if (keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP || keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].DOWN) {
        activeItem = _this.step(keyCode === rc_util_es_KeyCode__WEBPACK_IMPORTED_MODULE_2__["default"].UP ? -1 : 1);
      }

      if (activeItem) {
        e.preventDefault();
        updateActiveKey(_this.props.store, getEventKey(_this.props), activeItem.props.eventKey);

        if (typeof callback === 'function') {
          callback(activeItem);
        }

        return 1;
      }

      return undefined;
    };

    _this.onItemHover = function (e) {
      var key = e.key,
          hover = e.hover;
      updateActiveKey(_this.props.store, getEventKey(_this.props), hover ? key : null);
    };

    _this.onDeselect = function (selectInfo) {
      _this.props.onDeselect(selectInfo);
    };

    _this.onSelect = function (selectInfo) {
      _this.props.onSelect(selectInfo);
    };

    _this.onClick = function (e) {
      _this.props.onClick(e);
    };

    _this.onOpenChange = function (e) {
      _this.props.onOpenChange(e);
    };

    _this.onDestroy = function (key) {
      /* istanbul ignore next */
      _this.props.onDestroy(key);
    };

    _this.getFlatInstanceArray = function () {
      return _this.instanceArray;
    };

    _this.step = function (direction) {
      var children = _this.getFlatInstanceArray();

      var activeKey = _this.props.store.getState().activeKey[getEventKey(_this.props)];

      var len = children.length;

      if (!len) {
        return null;
      }

      if (direction < 0) {
        children = children.concat().reverse();
      } // find current activeIndex


      var activeIndex = -1;
      children.every(function (c, ci) {
        if (c && c.props.eventKey === activeKey) {
          activeIndex = ci;
          return false;
        }

        return true;
      });

      if (!_this.props.defaultActiveFirst && activeIndex !== -1 && allDisabled(children.slice(activeIndex, len - 1))) {
        return undefined;
      }

      var start = (activeIndex + 1) % len;
      var i = start;

      do {
        var child = children[i];

        if (!child || child.props.disabled) {
          i = (i + 1) % len;
        } else {
          return child;
        }
      } while (i !== start);

      return null;
    };

    _this.renderCommonMenuItem = function (child, i, extraProps) {
      var state = _this.props.store.getState();

      var _assertThisInitialize = _assertThisInitialized(_this),
          props = _assertThisInitialize.props;

      var key = Object(_util__WEBPACK_IMPORTED_MODULE_6__["getKeyFromChildrenIndex"])(child, props.eventKey, i);
      var childProps = child.props; // https://github.com/ant-design/ant-design/issues/11517#issuecomment-477403055

      if (!childProps || typeof child.type === 'string') {
        return child;
      }

      var isActive = key === state.activeKey;

      var newChildProps = _objectSpread({
        mode: childProps.mode || props.mode,
        level: props.level,
        inlineIndent: props.inlineIndent,
        renderMenuItem: _this.renderMenuItem,
        rootPrefixCls: props.prefixCls,
        index: i,
        parentMenu: props.parentMenu,
        // customized ref function, need to be invoked manually in child's componentDidMount
        manualRef: childProps.disabled ? undefined : Object(rc_util_es_createChainedFunction__WEBPACK_IMPORTED_MODULE_3__["default"])(child.ref, saveRef.bind(_assertThisInitialized(_this))),
        eventKey: key,
        active: !childProps.disabled && isActive,
        multiple: props.multiple,
        onClick: function onClick(e) {
          (childProps.onClick || _util__WEBPACK_IMPORTED_MODULE_6__["noop"])(e);

          _this.onClick(e);
        },
        onItemHover: _this.onItemHover,
        motion: props.motion,
        subMenuOpenDelay: props.subMenuOpenDelay,
        subMenuCloseDelay: props.subMenuCloseDelay,
        forceSubMenuRender: props.forceSubMenuRender,
        onOpenChange: _this.onOpenChange,
        onDeselect: _this.onDeselect,
        onSelect: _this.onSelect,
        builtinPlacements: props.builtinPlacements,
        itemIcon: childProps.itemIcon || _this.props.itemIcon,
        expandIcon: childProps.expandIcon || _this.props.expandIcon
      }, extraProps); // ref: https://github.com/ant-design/ant-design/issues/13943


      if (props.mode === 'inline' || Object(_util__WEBPACK_IMPORTED_MODULE_6__["isMobileDevice"])()) {
        newChildProps.triggerSubMenuAction = 'click';
      }

      return react__WEBPACK_IMPORTED_MODULE_0__["cloneElement"](child, newChildProps);
    };

    _this.renderMenuItem = function (c, i, subMenuKey) {
      /* istanbul ignore if */
      if (!c) {
        return null;
      }

      var state = _this.props.store.getState();

      var extraProps = {
        openKeys: state.openKeys,
        selectedKeys: state.selectedKeys,
        triggerSubMenuAction: _this.props.triggerSubMenuAction,
        subMenuKey: subMenuKey
      };
      return _this.renderCommonMenuItem(c, i, extraProps);
    };

    props.store.setState({
      activeKey: _objectSpread({}, props.store.getState().activeKey, _defineProperty({}, props.eventKey, getActiveKey(props, props.activeKey)))
    });
    _this.instanceArray = [];
    return _this;
  }

  _createClass(SubPopupMenu, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // invoke customized ref to expose component to mixin
      if (this.props.manualRef) {
        this.props.manualRef(this);
      }
    }
  }, {
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return this.props.visible || nextProps.visible || this.props.className !== nextProps.className || !shallowequal__WEBPACK_IMPORTED_MODULE_4___default()(this.props.style, nextProps.style);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var props = this.props;
      var originalActiveKey = 'activeKey' in props ? props.activeKey : props.store.getState().activeKey[getEventKey(props)];
      var activeKey = getActiveKey(props, originalActiveKey);

      if (activeKey !== originalActiveKey) {
        updateActiveKey(props.store, getEventKey(props), activeKey);
      } else if ('activeKey' in prevProps) {
        // If prev activeKey is not same as current activeKey,
        // we should set it.
        var prevActiveKey = getActiveKey(prevProps, prevProps.activeKey);

        if (activeKey !== prevActiveKey) {
          updateActiveKey(props.store, getEventKey(props), activeKey);
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var props = _extends({}, this.props);

      this.instanceArray = [];
      var className = classnames__WEBPACK_IMPORTED_MODULE_5___default()(props.prefixCls, props.className, "".concat(props.prefixCls, "-").concat(props.mode));
      var domProps = {
        className: className,
        // role could be 'select' and by default set to menu
        role: props.role || 'menu'
      };

      if (props.id) {
        domProps.id = props.id;
      }

      if (props.focusable) {
        domProps.tabIndex = 0;
        domProps.onKeyDown = this.onKeyDown;
      }

      var prefixCls = props.prefixCls,
          eventKey = props.eventKey,
          visible = props.visible,
          level = props.level,
          mode = props.mode,
          overflowedIndicator = props.overflowedIndicator,
          theme = props.theme;
      _util__WEBPACK_IMPORTED_MODULE_6__["menuAllProps"].forEach(function (key) {
        return delete props[key];
      }); // Otherwise, the propagated click event will trigger another onClick

      delete props.onClick;
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_DOMWrap__WEBPACK_IMPORTED_MODULE_7__["default"], Object.assign({}, props, {
        prefixCls: prefixCls,
        mode: mode,
        tag: "ul",
        level: level,
        theme: theme,
        visible: visible,
        overflowedIndicator: overflowedIndicator
      }, domProps), react__WEBPACK_IMPORTED_MODULE_0__["Children"].map(props.children, function (c, i) {
        return _this2.renderMenuItem(c, i, eventKey || '0-menu-');
      }));
    }
  }]);

  return SubPopupMenu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);
SubPopupMenu.defaultProps = {
  prefixCls: 'rc-menu',
  className: '',
  mode: 'vertical',
  level: 1,
  inlineIndent: 24,
  visible: true,
  focusable: true,
  style: {},
  manualRef: _util__WEBPACK_IMPORTED_MODULE_6__["noop"]
};
var connected = Object(mini_store__WEBPACK_IMPORTED_MODULE_1__["connect"])()(SubPopupMenu);
/* harmony default export */ __webpack_exports__["default"] = (connected);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtbWVudS9lcy9TdWJQb3B1cE1lbnUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy1tZW51L2VzL1N1YlBvcHVwTWVudS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJmdW5jdGlvbiBfdHlwZW9mKG9iaikgeyBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHsgX3R5cGVvZiA9IGZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9OyB9IGVsc2UgeyBfdHlwZW9mID0gZnVuY3Rpb24gX3R5cGVvZihvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07IH0gcmV0dXJuIF90eXBlb2Yob2JqKTsgfVxuXG5mdW5jdGlvbiBfZXh0ZW5kcygpIHsgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9OyByZXR1cm4gX2V4dGVuZHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKGNhbGwgJiYgKF90eXBlb2YoY2FsbCkgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikpIHsgcmV0dXJuIGNhbGw7IH0gcmV0dXJuIF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQoc2VsZik7IH1cblxuZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgX2dldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LmdldFByb3RvdHlwZU9mIDogZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHsgcmV0dXJuIG8uX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihvKTsgfTsgcmV0dXJuIF9nZXRQcm90b3R5cGVPZihvKTsgfVxuXG5mdW5jdGlvbiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpIHsgaWYgKHNlbGYgPT09IHZvaWQgMCkgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uXCIpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIF9zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcyk7IH1cblxuZnVuY3Rpb24gX3NldFByb3RvdHlwZU9mKG8sIHApIHsgX3NldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8IGZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7IG8uX19wcm90b19fID0gcDsgcmV0dXJuIG87IH07IHJldHVybiBfc2V0UHJvdG90eXBlT2YobywgcCk7IH1cblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKHNvdXJjZSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IF9kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgc291cmNlW2tleV0pOyB9KTsgfSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9ycykgeyBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpOyB9IGVsc2UgeyBvd25LZXlzKHNvdXJjZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihzb3VyY2UsIGtleSkpOyB9KTsgfSB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkgeyBpZiAoa2V5IGluIG9iaikgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHsgdmFsdWU6IHZhbHVlLCBlbnVtZXJhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUsIHdyaXRhYmxlOiB0cnVlIH0pOyB9IGVsc2UgeyBvYmpba2V5XSA9IHZhbHVlOyB9IHJldHVybiBvYmo7IH1cblxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ21pbmktc3RvcmUnO1xuaW1wb3J0IEtleUNvZGUgZnJvbSBcInJjLXV0aWwvZXMvS2V5Q29kZVwiO1xuaW1wb3J0IGNyZWF0ZUNoYWluZWRGdW5jdGlvbiBmcm9tIFwicmMtdXRpbC9lcy9jcmVhdGVDaGFpbmVkRnVuY3Rpb25cIjtcbmltcG9ydCBzaGFsbG93RXF1YWwgZnJvbSAnc2hhbGxvd2VxdWFsJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHsgZ2V0S2V5RnJvbUNoaWxkcmVuSW5kZXgsIGxvb3BNZW51SXRlbSwgbm9vcCwgbWVudUFsbFByb3BzLCBpc01vYmlsZURldmljZSB9IGZyb20gJy4vdXRpbCc7XG5pbXBvcnQgRE9NV3JhcCBmcm9tICcuL0RPTVdyYXAnO1xuXG5mdW5jdGlvbiBhbGxEaXNhYmxlZChhcnIpIHtcbiAgaWYgKCFhcnIubGVuZ3RoKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICByZXR1cm4gYXJyLmV2ZXJ5KGZ1bmN0aW9uIChjKSB7XG4gICAgcmV0dXJuICEhYy5wcm9wcy5kaXNhYmxlZDtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHVwZGF0ZUFjdGl2ZUtleShzdG9yZSwgbWVudUlkLCBhY3RpdmVLZXkpIHtcbiAgdmFyIHN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKTtcbiAgc3RvcmUuc2V0U3RhdGUoe1xuICAgIGFjdGl2ZUtleTogX29iamVjdFNwcmVhZCh7fSwgc3RhdGUuYWN0aXZlS2V5LCBfZGVmaW5lUHJvcGVydHkoe30sIG1lbnVJZCwgYWN0aXZlS2V5KSlcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGdldEV2ZW50S2V5KHByb3BzKSB7XG4gIC8vIHdoZW4gZXZlbnRLZXkgbm90IGF2YWlsYWJsZSAsaXQncyBtZW51IGFuZCByZXR1cm4gbWVudSBpZCAnMC1tZW51LSdcbiAgcmV0dXJuIHByb3BzLmV2ZW50S2V5IHx8ICcwLW1lbnUtJztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEFjdGl2ZUtleShwcm9wcywgb3JpZ2luYWxBY3RpdmVLZXkpIHtcbiAgdmFyIGFjdGl2ZUtleSA9IG9yaWdpbmFsQWN0aXZlS2V5O1xuICB2YXIgY2hpbGRyZW4gPSBwcm9wcy5jaGlsZHJlbixcbiAgICAgIGV2ZW50S2V5ID0gcHJvcHMuZXZlbnRLZXk7XG5cbiAgaWYgKGFjdGl2ZUtleSkge1xuICAgIHZhciBmb3VuZDtcbiAgICBsb29wTWVudUl0ZW0oY2hpbGRyZW4sIGZ1bmN0aW9uIChjLCBpKSB7XG4gICAgICBpZiAoYyAmJiBjLnByb3BzICYmICFjLnByb3BzLmRpc2FibGVkICYmIGFjdGl2ZUtleSA9PT0gZ2V0S2V5RnJvbUNoaWxkcmVuSW5kZXgoYywgZXZlbnRLZXksIGkpKSB7XG4gICAgICAgIGZvdW5kID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGlmIChmb3VuZCkge1xuICAgICAgcmV0dXJuIGFjdGl2ZUtleTtcbiAgICB9XG4gIH1cblxuICBhY3RpdmVLZXkgPSBudWxsO1xuXG4gIGlmIChwcm9wcy5kZWZhdWx0QWN0aXZlRmlyc3QpIHtcbiAgICBsb29wTWVudUl0ZW0oY2hpbGRyZW4sIGZ1bmN0aW9uIChjLCBpKSB7XG4gICAgICBpZiAoIWFjdGl2ZUtleSAmJiBjICYmICFjLnByb3BzLmRpc2FibGVkKSB7XG4gICAgICAgIGFjdGl2ZUtleSA9IGdldEtleUZyb21DaGlsZHJlbkluZGV4KGMsIGV2ZW50S2V5LCBpKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gYWN0aXZlS2V5O1xuICB9XG5cbiAgcmV0dXJuIGFjdGl2ZUtleTtcbn1cbmV4cG9ydCBmdW5jdGlvbiBzYXZlUmVmKGMpIHtcbiAgaWYgKGMpIHtcbiAgICB2YXIgaW5kZXggPSB0aGlzLmluc3RhbmNlQXJyYXkuaW5kZXhPZihjKTtcblxuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIC8vIHVwZGF0ZSBjb21wb25lbnQgaWYgaXQncyBhbHJlYWR5IGluc2lkZSBpbnN0YW5jZUFycmF5XG4gICAgICB0aGlzLmluc3RhbmNlQXJyYXlbaW5kZXhdID0gYztcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gYWRkIGNvbXBvbmVudCBpZiBpdCdzIG5vdCBpbiBpbnN0YW5jZUFycmF5IHlldDtcbiAgICAgIHRoaXMuaW5zdGFuY2VBcnJheS5wdXNoKGMpO1xuICAgIH1cbiAgfVxufVxuZXhwb3J0IHZhciBTdWJQb3B1cE1lbnUgPVxuLyojX19QVVJFX18qL1xuZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFN1YlBvcHVwTWVudSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ViUG9wdXBNZW51KHByb3BzKSB7XG4gICAgdmFyIF90aGlzO1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFN1YlBvcHVwTWVudSk7XG5cbiAgICBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9nZXRQcm90b3R5cGVPZihTdWJQb3B1cE1lbnUpLmNhbGwodGhpcywgcHJvcHMpKTtcbiAgICAvKipcbiAgICAgKiBhbGwga2V5Ym9hcmQgZXZlbnRzIGNhbGxiYWNrcyBydW4gZnJvbSBoZXJlIGF0IGZpcnN0XG4gICAgICpcbiAgICAgKiBub3RlOlxuICAgICAqICBUaGlzIGxlZ2FjeSBjb2RlIHRoYXQgYG9uS2V5RG93bmAgaXMgY2FsbGVkIGJ5IHBhcmVudCBpbnN0ZWFkIG9mIGRvbSBzZWxmLlxuICAgICAqICB3aGljaCBuZWVkIHJldHVybiBjb2RlIHRvIGNoZWNrIGlmIHRoaXMgZXZlbnQgaXMgaGFuZGxlZFxuICAgICAqL1xuXG4gICAgX3RoaXMub25LZXlEb3duID0gZnVuY3Rpb24gKGUsIGNhbGxiYWNrKSB7XG4gICAgICB2YXIga2V5Q29kZSA9IGUua2V5Q29kZTtcbiAgICAgIHZhciBoYW5kbGVkO1xuXG4gICAgICBfdGhpcy5nZXRGbGF0SW5zdGFuY2VBcnJheSgpLmZvckVhY2goZnVuY3Rpb24gKG9iaikge1xuICAgICAgICBpZiAob2JqICYmIG9iai5wcm9wcy5hY3RpdmUgJiYgb2JqLm9uS2V5RG93bikge1xuICAgICAgICAgIGhhbmRsZWQgPSBvYmoub25LZXlEb3duKGUpO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgaWYgKGhhbmRsZWQpIHtcbiAgICAgICAgcmV0dXJuIDE7XG4gICAgICB9XG5cbiAgICAgIHZhciBhY3RpdmVJdGVtID0gbnVsbDtcblxuICAgICAgaWYgKGtleUNvZGUgPT09IEtleUNvZGUuVVAgfHwga2V5Q29kZSA9PT0gS2V5Q29kZS5ET1dOKSB7XG4gICAgICAgIGFjdGl2ZUl0ZW0gPSBfdGhpcy5zdGVwKGtleUNvZGUgPT09IEtleUNvZGUuVVAgPyAtMSA6IDEpO1xuICAgICAgfVxuXG4gICAgICBpZiAoYWN0aXZlSXRlbSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHVwZGF0ZUFjdGl2ZUtleShfdGhpcy5wcm9wcy5zdG9yZSwgZ2V0RXZlbnRLZXkoX3RoaXMucHJvcHMpLCBhY3RpdmVJdGVtLnByb3BzLmV2ZW50S2V5KTtcblxuICAgICAgICBpZiAodHlwZW9mIGNhbGxiYWNrID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgY2FsbGJhY2soYWN0aXZlSXRlbSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gMTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9O1xuXG4gICAgX3RoaXMub25JdGVtSG92ZXIgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIGtleSA9IGUua2V5LFxuICAgICAgICAgIGhvdmVyID0gZS5ob3ZlcjtcbiAgICAgIHVwZGF0ZUFjdGl2ZUtleShfdGhpcy5wcm9wcy5zdG9yZSwgZ2V0RXZlbnRLZXkoX3RoaXMucHJvcHMpLCBob3ZlciA/IGtleSA6IG51bGwpO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkRlc2VsZWN0ID0gZnVuY3Rpb24gKHNlbGVjdEluZm8pIHtcbiAgICAgIF90aGlzLnByb3BzLm9uRGVzZWxlY3Qoc2VsZWN0SW5mbyk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uU2VsZWN0ID0gZnVuY3Rpb24gKHNlbGVjdEluZm8pIHtcbiAgICAgIF90aGlzLnByb3BzLm9uU2VsZWN0KHNlbGVjdEluZm8pO1xuICAgIH07XG5cbiAgICBfdGhpcy5vbkNsaWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIF90aGlzLnByb3BzLm9uQ2xpY2soZSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uT3BlbkNoYW5nZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICBfdGhpcy5wcm9wcy5vbk9wZW5DaGFuZ2UoZSk7XG4gICAgfTtcblxuICAgIF90aGlzLm9uRGVzdHJveSA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXG4gICAgICBfdGhpcy5wcm9wcy5vbkRlc3Ryb3koa2V5KTtcbiAgICB9O1xuXG4gICAgX3RoaXMuZ2V0RmxhdEluc3RhbmNlQXJyYXkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gX3RoaXMuaW5zdGFuY2VBcnJheTtcbiAgICB9O1xuXG4gICAgX3RoaXMuc3RlcCA9IGZ1bmN0aW9uIChkaXJlY3Rpb24pIHtcbiAgICAgIHZhciBjaGlsZHJlbiA9IF90aGlzLmdldEZsYXRJbnN0YW5jZUFycmF5KCk7XG5cbiAgICAgIHZhciBhY3RpdmVLZXkgPSBfdGhpcy5wcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLmFjdGl2ZUtleVtnZXRFdmVudEtleShfdGhpcy5wcm9wcyldO1xuXG4gICAgICB2YXIgbGVuID0gY2hpbGRyZW4ubGVuZ3RoO1xuXG4gICAgICBpZiAoIWxlbikge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKGRpcmVjdGlvbiA8IDApIHtcbiAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlbi5jb25jYXQoKS5yZXZlcnNlKCk7XG4gICAgICB9IC8vIGZpbmQgY3VycmVudCBhY3RpdmVJbmRleFxuXG5cbiAgICAgIHZhciBhY3RpdmVJbmRleCA9IC0xO1xuICAgICAgY2hpbGRyZW4uZXZlcnkoZnVuY3Rpb24gKGMsIGNpKSB7XG4gICAgICAgIGlmIChjICYmIGMucHJvcHMuZXZlbnRLZXkgPT09IGFjdGl2ZUtleSkge1xuICAgICAgICAgIGFjdGl2ZUluZGV4ID0gY2k7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9KTtcblxuICAgICAgaWYgKCFfdGhpcy5wcm9wcy5kZWZhdWx0QWN0aXZlRmlyc3QgJiYgYWN0aXZlSW5kZXggIT09IC0xICYmIGFsbERpc2FibGVkKGNoaWxkcmVuLnNsaWNlKGFjdGl2ZUluZGV4LCBsZW4gLSAxKSkpIHtcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICAgIH1cblxuICAgICAgdmFyIHN0YXJ0ID0gKGFjdGl2ZUluZGV4ICsgMSkgJSBsZW47XG4gICAgICB2YXIgaSA9IHN0YXJ0O1xuXG4gICAgICBkbyB7XG4gICAgICAgIHZhciBjaGlsZCA9IGNoaWxkcmVuW2ldO1xuXG4gICAgICAgIGlmICghY2hpbGQgfHwgY2hpbGQucHJvcHMuZGlzYWJsZWQpIHtcbiAgICAgICAgICBpID0gKGkgKyAxKSAlIGxlbjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgIH1cbiAgICAgIH0gd2hpbGUgKGkgIT09IHN0YXJ0KTtcblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfTtcblxuICAgIF90aGlzLnJlbmRlckNvbW1vbk1lbnVJdGVtID0gZnVuY3Rpb24gKGNoaWxkLCBpLCBleHRyYVByb3BzKSB7XG4gICAgICB2YXIgc3RhdGUgPSBfdGhpcy5wcm9wcy5zdG9yZS5nZXRTdGF0ZSgpO1xuXG4gICAgICB2YXIgX2Fzc2VydFRoaXNJbml0aWFsaXplID0gX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcyksXG4gICAgICAgICAgcHJvcHMgPSBfYXNzZXJ0VGhpc0luaXRpYWxpemUucHJvcHM7XG5cbiAgICAgIHZhciBrZXkgPSBnZXRLZXlGcm9tQ2hpbGRyZW5JbmRleChjaGlsZCwgcHJvcHMuZXZlbnRLZXksIGkpO1xuICAgICAgdmFyIGNoaWxkUHJvcHMgPSBjaGlsZC5wcm9wczsgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTE1MTcjaXNzdWVjb21tZW50LTQ3NzQwMzA1NVxuXG4gICAgICBpZiAoIWNoaWxkUHJvcHMgfHwgdHlwZW9mIGNoaWxkLnR5cGUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiBjaGlsZDtcbiAgICAgIH1cblxuICAgICAgdmFyIGlzQWN0aXZlID0ga2V5ID09PSBzdGF0ZS5hY3RpdmVLZXk7XG5cbiAgICAgIHZhciBuZXdDaGlsZFByb3BzID0gX29iamVjdFNwcmVhZCh7XG4gICAgICAgIG1vZGU6IGNoaWxkUHJvcHMubW9kZSB8fCBwcm9wcy5tb2RlLFxuICAgICAgICBsZXZlbDogcHJvcHMubGV2ZWwsXG4gICAgICAgIGlubGluZUluZGVudDogcHJvcHMuaW5saW5lSW5kZW50LFxuICAgICAgICByZW5kZXJNZW51SXRlbTogX3RoaXMucmVuZGVyTWVudUl0ZW0sXG4gICAgICAgIHJvb3RQcmVmaXhDbHM6IHByb3BzLnByZWZpeENscyxcbiAgICAgICAgaW5kZXg6IGksXG4gICAgICAgIHBhcmVudE1lbnU6IHByb3BzLnBhcmVudE1lbnUsXG4gICAgICAgIC8vIGN1c3RvbWl6ZWQgcmVmIGZ1bmN0aW9uLCBuZWVkIHRvIGJlIGludm9rZWQgbWFudWFsbHkgaW4gY2hpbGQncyBjb21wb25lbnREaWRNb3VudFxuICAgICAgICBtYW51YWxSZWY6IGNoaWxkUHJvcHMuZGlzYWJsZWQgPyB1bmRlZmluZWQgOiBjcmVhdGVDaGFpbmVkRnVuY3Rpb24oY2hpbGQucmVmLCBzYXZlUmVmLmJpbmQoX2Fzc2VydFRoaXNJbml0aWFsaXplZChfdGhpcykpKSxcbiAgICAgICAgZXZlbnRLZXk6IGtleSxcbiAgICAgICAgYWN0aXZlOiAhY2hpbGRQcm9wcy5kaXNhYmxlZCAmJiBpc0FjdGl2ZSxcbiAgICAgICAgbXVsdGlwbGU6IHByb3BzLm11bHRpcGxlLFxuICAgICAgICBvbkNsaWNrOiBmdW5jdGlvbiBvbkNsaWNrKGUpIHtcbiAgICAgICAgICAoY2hpbGRQcm9wcy5vbkNsaWNrIHx8IG5vb3ApKGUpO1xuXG4gICAgICAgICAgX3RoaXMub25DbGljayhlKTtcbiAgICAgICAgfSxcbiAgICAgICAgb25JdGVtSG92ZXI6IF90aGlzLm9uSXRlbUhvdmVyLFxuICAgICAgICBtb3Rpb246IHByb3BzLm1vdGlvbixcbiAgICAgICAgc3ViTWVudU9wZW5EZWxheTogcHJvcHMuc3ViTWVudU9wZW5EZWxheSxcbiAgICAgICAgc3ViTWVudUNsb3NlRGVsYXk6IHByb3BzLnN1Yk1lbnVDbG9zZURlbGF5LFxuICAgICAgICBmb3JjZVN1Yk1lbnVSZW5kZXI6IHByb3BzLmZvcmNlU3ViTWVudVJlbmRlcixcbiAgICAgICAgb25PcGVuQ2hhbmdlOiBfdGhpcy5vbk9wZW5DaGFuZ2UsXG4gICAgICAgIG9uRGVzZWxlY3Q6IF90aGlzLm9uRGVzZWxlY3QsXG4gICAgICAgIG9uU2VsZWN0OiBfdGhpcy5vblNlbGVjdCxcbiAgICAgICAgYnVpbHRpblBsYWNlbWVudHM6IHByb3BzLmJ1aWx0aW5QbGFjZW1lbnRzLFxuICAgICAgICBpdGVtSWNvbjogY2hpbGRQcm9wcy5pdGVtSWNvbiB8fCBfdGhpcy5wcm9wcy5pdGVtSWNvbixcbiAgICAgICAgZXhwYW5kSWNvbjogY2hpbGRQcm9wcy5leHBhbmRJY29uIHx8IF90aGlzLnByb3BzLmV4cGFuZEljb25cbiAgICAgIH0sIGV4dHJhUHJvcHMpOyAvLyByZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzEzOTQzXG5cblxuICAgICAgaWYgKHByb3BzLm1vZGUgPT09ICdpbmxpbmUnIHx8IGlzTW9iaWxlRGV2aWNlKCkpIHtcbiAgICAgICAgbmV3Q2hpbGRQcm9wcy50cmlnZ2VyU3ViTWVudUFjdGlvbiA9ICdjbGljayc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBSZWFjdC5jbG9uZUVsZW1lbnQoY2hpbGQsIG5ld0NoaWxkUHJvcHMpO1xuICAgIH07XG5cbiAgICBfdGhpcy5yZW5kZXJNZW51SXRlbSA9IGZ1bmN0aW9uIChjLCBpLCBzdWJNZW51S2V5KSB7XG4gICAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgaWYgKi9cbiAgICAgIGlmICghYykge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgdmFyIHN0YXRlID0gX3RoaXMucHJvcHMuc3RvcmUuZ2V0U3RhdGUoKTtcblxuICAgICAgdmFyIGV4dHJhUHJvcHMgPSB7XG4gICAgICAgIG9wZW5LZXlzOiBzdGF0ZS5vcGVuS2V5cyxcbiAgICAgICAgc2VsZWN0ZWRLZXlzOiBzdGF0ZS5zZWxlY3RlZEtleXMsXG4gICAgICAgIHRyaWdnZXJTdWJNZW51QWN0aW9uOiBfdGhpcy5wcm9wcy50cmlnZ2VyU3ViTWVudUFjdGlvbixcbiAgICAgICAgc3ViTWVudUtleTogc3ViTWVudUtleVxuICAgICAgfTtcbiAgICAgIHJldHVybiBfdGhpcy5yZW5kZXJDb21tb25NZW51SXRlbShjLCBpLCBleHRyYVByb3BzKTtcbiAgICB9O1xuXG4gICAgcHJvcHMuc3RvcmUuc2V0U3RhdGUoe1xuICAgICAgYWN0aXZlS2V5OiBfb2JqZWN0U3ByZWFkKHt9LCBwcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLmFjdGl2ZUtleSwgX2RlZmluZVByb3BlcnR5KHt9LCBwcm9wcy5ldmVudEtleSwgZ2V0QWN0aXZlS2V5KHByb3BzLCBwcm9wcy5hY3RpdmVLZXkpKSlcbiAgICB9KTtcbiAgICBfdGhpcy5pbnN0YW5jZUFycmF5ID0gW107XG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKFN1YlBvcHVwTWVudSwgW3tcbiAgICBrZXk6IFwiY29tcG9uZW50RGlkTW91bnRcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAvLyBpbnZva2UgY3VzdG9taXplZCByZWYgdG8gZXhwb3NlIGNvbXBvbmVudCB0byBtaXhpblxuICAgICAgaWYgKHRoaXMucHJvcHMubWFudWFsUmVmKSB7XG4gICAgICAgIHRoaXMucHJvcHMubWFudWFsUmVmKHRoaXMpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJzaG91bGRDb21wb25lbnRVcGRhdGVcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgcmV0dXJuIHRoaXMucHJvcHMudmlzaWJsZSB8fCBuZXh0UHJvcHMudmlzaWJsZSB8fCB0aGlzLnByb3BzLmNsYXNzTmFtZSAhPT0gbmV4dFByb3BzLmNsYXNzTmFtZSB8fCAhc2hhbGxvd0VxdWFsKHRoaXMucHJvcHMuc3R5bGUsIG5leHRQcm9wcy5zdHlsZSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiBcImNvbXBvbmVudERpZFVwZGF0ZVwiLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuICAgICAgdmFyIG9yaWdpbmFsQWN0aXZlS2V5ID0gJ2FjdGl2ZUtleScgaW4gcHJvcHMgPyBwcm9wcy5hY3RpdmVLZXkgOiBwcm9wcy5zdG9yZS5nZXRTdGF0ZSgpLmFjdGl2ZUtleVtnZXRFdmVudEtleShwcm9wcyldO1xuICAgICAgdmFyIGFjdGl2ZUtleSA9IGdldEFjdGl2ZUtleShwcm9wcywgb3JpZ2luYWxBY3RpdmVLZXkpO1xuXG4gICAgICBpZiAoYWN0aXZlS2V5ICE9PSBvcmlnaW5hbEFjdGl2ZUtleSkge1xuICAgICAgICB1cGRhdGVBY3RpdmVLZXkocHJvcHMuc3RvcmUsIGdldEV2ZW50S2V5KHByb3BzKSwgYWN0aXZlS2V5KTtcbiAgICAgIH0gZWxzZSBpZiAoJ2FjdGl2ZUtleScgaW4gcHJldlByb3BzKSB7XG4gICAgICAgIC8vIElmIHByZXYgYWN0aXZlS2V5IGlzIG5vdCBzYW1lIGFzIGN1cnJlbnQgYWN0aXZlS2V5LFxuICAgICAgICAvLyB3ZSBzaG91bGQgc2V0IGl0LlxuICAgICAgICB2YXIgcHJldkFjdGl2ZUtleSA9IGdldEFjdGl2ZUtleShwcmV2UHJvcHMsIHByZXZQcm9wcy5hY3RpdmVLZXkpO1xuXG4gICAgICAgIGlmIChhY3RpdmVLZXkgIT09IHByZXZBY3RpdmVLZXkpIHtcbiAgICAgICAgICB1cGRhdGVBY3RpdmVLZXkocHJvcHMuc3RvcmUsIGdldEV2ZW50S2V5KHByb3BzKSwgYWN0aXZlS2V5KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogXCJyZW5kZXJcIixcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBwcm9wcyA9IF9leHRlbmRzKHt9LCB0aGlzLnByb3BzKTtcblxuICAgICAgdGhpcy5pbnN0YW5jZUFycmF5ID0gW107XG4gICAgICB2YXIgY2xhc3NOYW1lID0gY2xhc3NOYW1lcyhwcm9wcy5wcmVmaXhDbHMsIHByb3BzLmNsYXNzTmFtZSwgXCJcIi5jb25jYXQocHJvcHMucHJlZml4Q2xzLCBcIi1cIikuY29uY2F0KHByb3BzLm1vZGUpKTtcbiAgICAgIHZhciBkb21Qcm9wcyA9IHtcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgIC8vIHJvbGUgY291bGQgYmUgJ3NlbGVjdCcgYW5kIGJ5IGRlZmF1bHQgc2V0IHRvIG1lbnVcbiAgICAgICAgcm9sZTogcHJvcHMucm9sZSB8fCAnbWVudSdcbiAgICAgIH07XG5cbiAgICAgIGlmIChwcm9wcy5pZCkge1xuICAgICAgICBkb21Qcm9wcy5pZCA9IHByb3BzLmlkO1xuICAgICAgfVxuXG4gICAgICBpZiAocHJvcHMuZm9jdXNhYmxlKSB7XG4gICAgICAgIGRvbVByb3BzLnRhYkluZGV4ID0gMDtcbiAgICAgICAgZG9tUHJvcHMub25LZXlEb3duID0gdGhpcy5vbktleURvd247XG4gICAgICB9XG5cbiAgICAgIHZhciBwcmVmaXhDbHMgPSBwcm9wcy5wcmVmaXhDbHMsXG4gICAgICAgICAgZXZlbnRLZXkgPSBwcm9wcy5ldmVudEtleSxcbiAgICAgICAgICB2aXNpYmxlID0gcHJvcHMudmlzaWJsZSxcbiAgICAgICAgICBsZXZlbCA9IHByb3BzLmxldmVsLFxuICAgICAgICAgIG1vZGUgPSBwcm9wcy5tb2RlLFxuICAgICAgICAgIG92ZXJmbG93ZWRJbmRpY2F0b3IgPSBwcm9wcy5vdmVyZmxvd2VkSW5kaWNhdG9yLFxuICAgICAgICAgIHRoZW1lID0gcHJvcHMudGhlbWU7XG4gICAgICBtZW51QWxsUHJvcHMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIHJldHVybiBkZWxldGUgcHJvcHNba2V5XTtcbiAgICAgIH0pOyAvLyBPdGhlcndpc2UsIHRoZSBwcm9wYWdhdGVkIGNsaWNrIGV2ZW50IHdpbGwgdHJpZ2dlciBhbm90aGVyIG9uQ2xpY2tcblxuICAgICAgZGVsZXRlIHByb3BzLm9uQ2xpY2s7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChET01XcmFwLCBPYmplY3QuYXNzaWduKHt9LCBwcm9wcywge1xuICAgICAgICBwcmVmaXhDbHM6IHByZWZpeENscyxcbiAgICAgICAgbW9kZTogbW9kZSxcbiAgICAgICAgdGFnOiBcInVsXCIsXG4gICAgICAgIGxldmVsOiBsZXZlbCxcbiAgICAgICAgdGhlbWU6IHRoZW1lLFxuICAgICAgICB2aXNpYmxlOiB2aXNpYmxlLFxuICAgICAgICBvdmVyZmxvd2VkSW5kaWNhdG9yOiBvdmVyZmxvd2VkSW5kaWNhdG9yXG4gICAgICB9LCBkb21Qcm9wcyksIFJlYWN0LkNoaWxkcmVuLm1hcChwcm9wcy5jaGlsZHJlbiwgZnVuY3Rpb24gKGMsIGkpIHtcbiAgICAgICAgcmV0dXJuIF90aGlzMi5yZW5kZXJNZW51SXRlbShjLCBpLCBldmVudEtleSB8fCAnMC1tZW51LScpO1xuICAgICAgfSkpO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBTdWJQb3B1cE1lbnU7XG59KFJlYWN0LkNvbXBvbmVudCk7XG5TdWJQb3B1cE1lbnUuZGVmYXVsdFByb3BzID0ge1xuICBwcmVmaXhDbHM6ICdyYy1tZW51JyxcbiAgY2xhc3NOYW1lOiAnJyxcbiAgbW9kZTogJ3ZlcnRpY2FsJyxcbiAgbGV2ZWw6IDEsXG4gIGlubGluZUluZGVudDogMjQsXG4gIHZpc2libGU6IHRydWUsXG4gIGZvY3VzYWJsZTogdHJ1ZSxcbiAgc3R5bGU6IHt9LFxuICBtYW51YWxSZWY6IG5vb3Bcbn07XG52YXIgY29ubmVjdGVkID0gY29ubmVjdCgpKFN1YlBvcHVwTWVudSk7XG5leHBvcnQgZGVmYXVsdCBjb25uZWN0ZWQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUJBO0FBQ0E7QUFDQTtBQThCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQS9DQTtBQUNBO0FBaURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRBO0FBV0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-menu/es/SubPopupMenu.js
