__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var omit_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! omit.js */ "./node_modules/omit.js/es/index.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rc_tree_es_util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-tree/es/util */ "./node_modules/rc-tree/es/util.js");
/* harmony import */ var react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-lifecycles-compat */ "./node_modules/react-lifecycles-compat/react-lifecycles-compat.es.js");
/* harmony import */ var _config_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../config-provider */ "./node_modules/antd/es/config-provider/index.js");
/* harmony import */ var _Tree__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Tree */ "./node_modules/antd/es/tree/Tree.js");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./util */ "./node_modules/antd/es/tree/util.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var __rest = undefined && undefined.__rest || function (s, e) {
  var t = {};

  for (var p in s) {
    if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
  }

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
};












function getIcon(props) {
  var isLeaf = props.isLeaf,
      expanded = props.expanded;

  if (isLeaf) {
    return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_9__["default"], {
      type: "file"
    });
  }

  return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_9__["default"], {
    type: expanded ? 'folder-open' : 'folder'
  });
}

var DirectoryTree =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DirectoryTree, _React$Component);

  function DirectoryTree(props) {
    var _this;

    _classCallCheck(this, DirectoryTree);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DirectoryTree).call(this, props));

    _this.onExpand = function (expandedKeys, info) {
      var onExpand = _this.props.onExpand;

      _this.setUncontrolledState({
        expandedKeys: expandedKeys
      }); // Call origin function


      if (onExpand) {
        return onExpand(expandedKeys, info);
      }

      return undefined;
    };

    _this.onClick = function (event, node) {
      var _this$props = _this.props,
          onClick = _this$props.onClick,
          expandAction = _this$props.expandAction; // Expand the tree

      if (expandAction === 'click') {
        _this.onDebounceExpand(event, node);
      }

      if (onClick) {
        onClick(event, node);
      }
    };

    _this.onDoubleClick = function (event, node) {
      var _this$props2 = _this.props,
          onDoubleClick = _this$props2.onDoubleClick,
          expandAction = _this$props2.expandAction; // Expand the tree

      if (expandAction === 'doubleClick') {
        _this.onDebounceExpand(event, node);
      }

      if (onDoubleClick) {
        onDoubleClick(event, node);
      }
    };

    _this.onSelect = function (keys, event) {
      var _this$props3 = _this.props,
          onSelect = _this$props3.onSelect,
          multiple = _this$props3.multiple,
          children = _this$props3.children;
      var _this$state$expandedK = _this.state.expandedKeys,
          expandedKeys = _this$state$expandedK === void 0 ? [] : _this$state$expandedK;
      var node = event.node,
          nativeEvent = event.nativeEvent;
      var _node$props$eventKey = node.props.eventKey,
          eventKey = _node$props$eventKey === void 0 ? '' : _node$props$eventKey;
      var newState = {}; // We need wrap this event since some value is not same

      var newEvent = _extends(_extends({}, event), {
        selected: true
      }); // Windows / Mac single pick


      var ctrlPick = nativeEvent.ctrlKey || nativeEvent.metaKey;
      var shiftPick = nativeEvent.shiftKey; // Generate new selected keys

      var newSelectedKeys;

      if (multiple && ctrlPick) {
        // Control click
        newSelectedKeys = keys;
        _this.lastSelectedKey = eventKey;
        _this.cachedSelectedKeys = newSelectedKeys;
        newEvent.selectedNodes = Object(_util__WEBPACK_IMPORTED_MODULE_8__["convertDirectoryKeysToNodes"])(children, newSelectedKeys);
      } else if (multiple && shiftPick) {
        // Shift click
        newSelectedKeys = Array.from(new Set([].concat(_toConsumableArray(_this.cachedSelectedKeys || []), _toConsumableArray(Object(_util__WEBPACK_IMPORTED_MODULE_8__["calcRangeKeys"])(children, expandedKeys, eventKey, _this.lastSelectedKey)))));
        newEvent.selectedNodes = Object(_util__WEBPACK_IMPORTED_MODULE_8__["convertDirectoryKeysToNodes"])(children, newSelectedKeys);
      } else {
        // Single click
        newSelectedKeys = [eventKey];
        _this.lastSelectedKey = eventKey;
        _this.cachedSelectedKeys = newSelectedKeys;
        newEvent.selectedNodes = [event.node];
      }

      newState.selectedKeys = newSelectedKeys;

      if (onSelect) {
        onSelect(newSelectedKeys, newEvent);
      }

      _this.setUncontrolledState(newState);
    };

    _this.setTreeRef = function (node) {
      _this.tree = node;
    };

    _this.expandFolderNode = function (event, node) {
      var isLeaf = node.props.isLeaf;

      if (isLeaf || event.shiftKey || event.metaKey || event.ctrlKey) {
        return;
      } // Get internal rc-tree


      var internalTree = _this.tree.tree; // Call internal rc-tree expand function
      // https://github.com/ant-design/ant-design/issues/12567

      internalTree.onNodeExpand(event, node);
    };

    _this.setUncontrolledState = function (state) {
      var newState = Object(omit_js__WEBPACK_IMPORTED_MODULE_2__["default"])(state, Object.keys(_this.props));

      if (Object.keys(newState).length) {
        _this.setState(newState);
      }
    };

    _this.renderDirectoryTree = function (_ref) {
      var getPrefixCls = _ref.getPrefixCls;

      var _a = _this.props,
          customizePrefixCls = _a.prefixCls,
          className = _a.className,
          props = __rest(_a, ["prefixCls", "className"]);

      var _this$state = _this.state,
          expandedKeys = _this$state.expandedKeys,
          selectedKeys = _this$state.selectedKeys;
      var prefixCls = getPrefixCls('tree', customizePrefixCls);
      var connectClassName = classnames__WEBPACK_IMPORTED_MODULE_1___default()("".concat(prefixCls, "-directory"), className);
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_Tree__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({
        icon: getIcon,
        ref: _this.setTreeRef
      }, props, {
        prefixCls: prefixCls,
        className: connectClassName,
        expandedKeys: expandedKeys,
        selectedKeys: selectedKeys,
        onSelect: _this.onSelect,
        onClick: _this.onClick,
        onDoubleClick: _this.onDoubleClick,
        onExpand: _this.onExpand
      }));
    };

    var defaultExpandAll = props.defaultExpandAll,
        defaultExpandParent = props.defaultExpandParent,
        expandedKeys = props.expandedKeys,
        defaultExpandedKeys = props.defaultExpandedKeys,
        children = props.children;

    var _convertTreeToEntitie = Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_4__["convertTreeToEntities"])(children),
        keyEntities = _convertTreeToEntitie.keyEntities; // Selected keys


    _this.state = {
      selectedKeys: props.selectedKeys || props.defaultSelectedKeys || []
    }; // Expanded keys

    if (defaultExpandAll) {
      if (props.treeData) {
        _this.state.expandedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_8__["getFullKeyListByTreeData"])(props.treeData);
      } else {
        _this.state.expandedKeys = Object(_util__WEBPACK_IMPORTED_MODULE_8__["getFullKeyList"])(props.children);
      }
    } else if (defaultExpandParent) {
      _this.state.expandedKeys = Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_4__["conductExpandParent"])(expandedKeys || defaultExpandedKeys, keyEntities);
    } else {
      _this.state.expandedKeys = expandedKeys || defaultExpandedKeys;
    }

    _this.onDebounceExpand = lodash_debounce__WEBPACK_IMPORTED_MODULE_3___default()(_this.expandFolderNode, 200, {
      leading: true
    });
    return _this;
  }

  _createClass(DirectoryTree, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_config_provider__WEBPACK_IMPORTED_MODULE_6__["ConfigConsumer"], null, this.renderDirectoryTree);
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps) {
      var newState = {};

      if ('expandedKeys' in nextProps) {
        newState.expandedKeys = nextProps.expandedKeys;
      }

      if ('selectedKeys' in nextProps) {
        newState.selectedKeys = nextProps.selectedKeys;
      }

      return newState;
    }
  }]);

  return DirectoryTree;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

DirectoryTree.defaultProps = {
  showIcon: true,
  expandAction: 'click'
};
Object(react_lifecycles_compat__WEBPACK_IMPORTED_MODULE_5__["polyfill"])(DirectoryTree);
/* harmony default export */ __webpack_exports__["default"] = (DirectoryTree);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy90cmVlL0RpcmVjdG9yeVRyZWUuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3RyZWUvRGlyZWN0b3J5VHJlZS5qc3giXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgb21pdCBmcm9tICdvbWl0LmpzJztcbmltcG9ydCBkZWJvdW5jZSBmcm9tICdsb2Rhc2gvZGVib3VuY2UnO1xuaW1wb3J0IHsgY29uZHVjdEV4cGFuZFBhcmVudCwgY29udmVydFRyZWVUb0VudGl0aWVzIH0gZnJvbSAncmMtdHJlZS9saWIvdXRpbCc7XG5pbXBvcnQgeyBwb2x5ZmlsbCB9IGZyb20gJ3JlYWN0LWxpZmVjeWNsZXMtY29tcGF0JztcbmltcG9ydCB7IENvbmZpZ0NvbnN1bWVyIH0gZnJvbSAnLi4vY29uZmlnLXByb3ZpZGVyJztcbmltcG9ydCBUcmVlIGZyb20gJy4vVHJlZSc7XG5pbXBvcnQgeyBjYWxjUmFuZ2VLZXlzLCBnZXRGdWxsS2V5TGlzdCwgY29udmVydERpcmVjdG9yeUtleXNUb05vZGVzLCBnZXRGdWxsS2V5TGlzdEJ5VHJlZURhdGEsIH0gZnJvbSAnLi91dGlsJztcbmltcG9ydCBJY29uIGZyb20gJy4uL2ljb24nO1xuZnVuY3Rpb24gZ2V0SWNvbihwcm9wcykge1xuICAgIGNvbnN0IHsgaXNMZWFmLCBleHBhbmRlZCB9ID0gcHJvcHM7XG4gICAgaWYgKGlzTGVhZikge1xuICAgICAgICByZXR1cm4gPEljb24gdHlwZT1cImZpbGVcIi8+O1xuICAgIH1cbiAgICByZXR1cm4gPEljb24gdHlwZT17ZXhwYW5kZWQgPyAnZm9sZGVyLW9wZW4nIDogJ2ZvbGRlcid9Lz47XG59XG5jbGFzcyBEaXJlY3RvcnlUcmVlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgICAgICBzdXBlcihwcm9wcyk7XG4gICAgICAgIHRoaXMub25FeHBhbmQgPSAoZXhwYW5kZWRLZXlzLCBpbmZvKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uRXhwYW5kIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICAgICAgdGhpcy5zZXRVbmNvbnRyb2xsZWRTdGF0ZSh7IGV4cGFuZGVkS2V5cyB9KTtcbiAgICAgICAgICAgIC8vIENhbGwgb3JpZ2luIGZ1bmN0aW9uXG4gICAgICAgICAgICBpZiAob25FeHBhbmQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gb25FeHBhbmQoZXhwYW5kZWRLZXlzLCBpbmZvKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB1bmRlZmluZWQ7XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMub25DbGljayA9IChldmVudCwgbm9kZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyBvbkNsaWNrLCBleHBhbmRBY3Rpb24gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICAvLyBFeHBhbmQgdGhlIHRyZWVcbiAgICAgICAgICAgIGlmIChleHBhbmRBY3Rpb24gPT09ICdjbGljaycpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uRGVib3VuY2VFeHBhbmQoZXZlbnQsIG5vZGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKG9uQ2xpY2spIHtcbiAgICAgICAgICAgICAgICBvbkNsaWNrKGV2ZW50LCBub2RlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5vbkRvdWJsZUNsaWNrID0gKGV2ZW50LCBub2RlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uRG91YmxlQ2xpY2ssIGV4cGFuZEFjdGlvbiB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgICAgIC8vIEV4cGFuZCB0aGUgdHJlZVxuICAgICAgICAgICAgaWYgKGV4cGFuZEFjdGlvbiA9PT0gJ2RvdWJsZUNsaWNrJykge1xuICAgICAgICAgICAgICAgIHRoaXMub25EZWJvdW5jZUV4cGFuZChldmVudCwgbm9kZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAob25Eb3VibGVDbGljaykge1xuICAgICAgICAgICAgICAgIG9uRG91YmxlQ2xpY2soZXZlbnQsIG5vZGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICB0aGlzLm9uU2VsZWN0ID0gKGtleXMsIGV2ZW50KSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IG9uU2VsZWN0LCBtdWx0aXBsZSwgY2hpbGRyZW4gfSA9IHRoaXMucHJvcHM7XG4gICAgICAgICAgICBjb25zdCB7IGV4cGFuZGVkS2V5cyA9IFtdIH0gPSB0aGlzLnN0YXRlO1xuICAgICAgICAgICAgY29uc3QgeyBub2RlLCBuYXRpdmVFdmVudCB9ID0gZXZlbnQ7XG4gICAgICAgICAgICBjb25zdCB7IGV2ZW50S2V5ID0gJycgfSA9IG5vZGUucHJvcHM7XG4gICAgICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHt9O1xuICAgICAgICAgICAgLy8gV2UgbmVlZCB3cmFwIHRoaXMgZXZlbnQgc2luY2Ugc29tZSB2YWx1ZSBpcyBub3Qgc2FtZVxuICAgICAgICAgICAgY29uc3QgbmV3RXZlbnQgPSBPYmplY3QuYXNzaWduKE9iamVjdC5hc3NpZ24oe30sIGV2ZW50KSwgeyBzZWxlY3RlZDogdHJ1ZSB9KTtcbiAgICAgICAgICAgIC8vIFdpbmRvd3MgLyBNYWMgc2luZ2xlIHBpY2tcbiAgICAgICAgICAgIGNvbnN0IGN0cmxQaWNrID0gbmF0aXZlRXZlbnQuY3RybEtleSB8fCBuYXRpdmVFdmVudC5tZXRhS2V5O1xuICAgICAgICAgICAgY29uc3Qgc2hpZnRQaWNrID0gbmF0aXZlRXZlbnQuc2hpZnRLZXk7XG4gICAgICAgICAgICAvLyBHZW5lcmF0ZSBuZXcgc2VsZWN0ZWQga2V5c1xuICAgICAgICAgICAgbGV0IG5ld1NlbGVjdGVkS2V5cztcbiAgICAgICAgICAgIGlmIChtdWx0aXBsZSAmJiBjdHJsUGljaykge1xuICAgICAgICAgICAgICAgIC8vIENvbnRyb2wgY2xpY2tcbiAgICAgICAgICAgICAgICBuZXdTZWxlY3RlZEtleXMgPSBrZXlzO1xuICAgICAgICAgICAgICAgIHRoaXMubGFzdFNlbGVjdGVkS2V5ID0gZXZlbnRLZXk7XG4gICAgICAgICAgICAgICAgdGhpcy5jYWNoZWRTZWxlY3RlZEtleXMgPSBuZXdTZWxlY3RlZEtleXM7XG4gICAgICAgICAgICAgICAgbmV3RXZlbnQuc2VsZWN0ZWROb2RlcyA9IGNvbnZlcnREaXJlY3RvcnlLZXlzVG9Ob2RlcyhjaGlsZHJlbiwgbmV3U2VsZWN0ZWRLZXlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKG11bHRpcGxlICYmIHNoaWZ0UGljaykge1xuICAgICAgICAgICAgICAgIC8vIFNoaWZ0IGNsaWNrXG4gICAgICAgICAgICAgICAgbmV3U2VsZWN0ZWRLZXlzID0gQXJyYXkuZnJvbShuZXcgU2V0KFtcbiAgICAgICAgICAgICAgICAgICAgLi4uKHRoaXMuY2FjaGVkU2VsZWN0ZWRLZXlzIHx8IFtdKSxcbiAgICAgICAgICAgICAgICAgICAgLi4uY2FsY1JhbmdlS2V5cyhjaGlsZHJlbiwgZXhwYW5kZWRLZXlzLCBldmVudEtleSwgdGhpcy5sYXN0U2VsZWN0ZWRLZXkpLFxuICAgICAgICAgICAgICAgIF0pKTtcbiAgICAgICAgICAgICAgICBuZXdFdmVudC5zZWxlY3RlZE5vZGVzID0gY29udmVydERpcmVjdG9yeUtleXNUb05vZGVzKGNoaWxkcmVuLCBuZXdTZWxlY3RlZEtleXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgLy8gU2luZ2xlIGNsaWNrXG4gICAgICAgICAgICAgICAgbmV3U2VsZWN0ZWRLZXlzID0gW2V2ZW50S2V5XTtcbiAgICAgICAgICAgICAgICB0aGlzLmxhc3RTZWxlY3RlZEtleSA9IGV2ZW50S2V5O1xuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVkU2VsZWN0ZWRLZXlzID0gbmV3U2VsZWN0ZWRLZXlzO1xuICAgICAgICAgICAgICAgIG5ld0V2ZW50LnNlbGVjdGVkTm9kZXMgPSBbZXZlbnQubm9kZV07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBuZXdTdGF0ZS5zZWxlY3RlZEtleXMgPSBuZXdTZWxlY3RlZEtleXM7XG4gICAgICAgICAgICBpZiAob25TZWxlY3QpIHtcbiAgICAgICAgICAgICAgICBvblNlbGVjdChuZXdTZWxlY3RlZEtleXMsIG5ld0V2ZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc2V0VW5jb250cm9sbGVkU3RhdGUobmV3U3RhdGUpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNldFRyZWVSZWYgPSAobm9kZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy50cmVlID0gbm9kZTtcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5leHBhbmRGb2xkZXJOb2RlID0gKGV2ZW50LCBub2RlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB7IGlzTGVhZiB9ID0gbm9kZS5wcm9wcztcbiAgICAgICAgICAgIGlmIChpc0xlYWYgfHwgZXZlbnQuc2hpZnRLZXkgfHwgZXZlbnQubWV0YUtleSB8fCBldmVudC5jdHJsS2V5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gR2V0IGludGVybmFsIHJjLXRyZWVcbiAgICAgICAgICAgIGNvbnN0IGludGVybmFsVHJlZSA9IHRoaXMudHJlZS50cmVlO1xuICAgICAgICAgICAgLy8gQ2FsbCBpbnRlcm5hbCByYy10cmVlIGV4cGFuZCBmdW5jdGlvblxuICAgICAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMTI1NjdcbiAgICAgICAgICAgIGludGVybmFsVHJlZS5vbk5vZGVFeHBhbmQoZXZlbnQsIG5vZGUpO1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLnNldFVuY29udHJvbGxlZFN0YXRlID0gKHN0YXRlKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IG9taXQoc3RhdGUsIE9iamVjdC5rZXlzKHRoaXMucHJvcHMpKTtcbiAgICAgICAgICAgIGlmIChPYmplY3Qua2V5cyhuZXdTdGF0ZSkubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZShuZXdTdGF0ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMucmVuZGVyRGlyZWN0b3J5VHJlZSA9ICh7IGdldFByZWZpeENscyB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBfYSA9IHRoaXMucHJvcHMsIHsgcHJlZml4Q2xzOiBjdXN0b21pemVQcmVmaXhDbHMsIGNsYXNzTmFtZSB9ID0gX2EsIHByb3BzID0gX19yZXN0KF9hLCBbXCJwcmVmaXhDbHNcIiwgXCJjbGFzc05hbWVcIl0pO1xuICAgICAgICAgICAgY29uc3QgeyBleHBhbmRlZEtleXMsIHNlbGVjdGVkS2V5cyB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgICAgICAgIGNvbnN0IHByZWZpeENscyA9IGdldFByZWZpeENscygndHJlZScsIGN1c3RvbWl6ZVByZWZpeENscyk7XG4gICAgICAgICAgICBjb25zdCBjb25uZWN0Q2xhc3NOYW1lID0gY2xhc3NOYW1lcyhgJHtwcmVmaXhDbHN9LWRpcmVjdG9yeWAsIGNsYXNzTmFtZSk7XG4gICAgICAgICAgICByZXR1cm4gKDxUcmVlIGljb249e2dldEljb259IHJlZj17dGhpcy5zZXRUcmVlUmVmfSB7Li4ucHJvcHN9IHByZWZpeENscz17cHJlZml4Q2xzfSBjbGFzc05hbWU9e2Nvbm5lY3RDbGFzc05hbWV9IGV4cGFuZGVkS2V5cz17ZXhwYW5kZWRLZXlzfSBzZWxlY3RlZEtleXM9e3NlbGVjdGVkS2V5c30gb25TZWxlY3Q9e3RoaXMub25TZWxlY3R9IG9uQ2xpY2s9e3RoaXMub25DbGlja30gb25Eb3VibGVDbGljaz17dGhpcy5vbkRvdWJsZUNsaWNrfSBvbkV4cGFuZD17dGhpcy5vbkV4cGFuZH0vPik7XG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHsgZGVmYXVsdEV4cGFuZEFsbCwgZGVmYXVsdEV4cGFuZFBhcmVudCwgZXhwYW5kZWRLZXlzLCBkZWZhdWx0RXhwYW5kZWRLZXlzLCBjaGlsZHJlbiwgfSA9IHByb3BzO1xuICAgICAgICBjb25zdCB7IGtleUVudGl0aWVzIH0gPSBjb252ZXJ0VHJlZVRvRW50aXRpZXMoY2hpbGRyZW4pO1xuICAgICAgICAvLyBTZWxlY3RlZCBrZXlzXG4gICAgICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICAgICAgICBzZWxlY3RlZEtleXM6IHByb3BzLnNlbGVjdGVkS2V5cyB8fCBwcm9wcy5kZWZhdWx0U2VsZWN0ZWRLZXlzIHx8IFtdLFxuICAgICAgICB9O1xuICAgICAgICAvLyBFeHBhbmRlZCBrZXlzXG4gICAgICAgIGlmIChkZWZhdWx0RXhwYW5kQWxsKSB7XG4gICAgICAgICAgICBpZiAocHJvcHMudHJlZURhdGEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmV4cGFuZGVkS2V5cyA9IGdldEZ1bGxLZXlMaXN0QnlUcmVlRGF0YShwcm9wcy50cmVlRGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmV4cGFuZGVkS2V5cyA9IGdldEZ1bGxLZXlMaXN0KHByb3BzLmNoaWxkcmVuKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChkZWZhdWx0RXhwYW5kUGFyZW50KSB7XG4gICAgICAgICAgICB0aGlzLnN0YXRlLmV4cGFuZGVkS2V5cyA9IGNvbmR1Y3RFeHBhbmRQYXJlbnQoZXhwYW5kZWRLZXlzIHx8IGRlZmF1bHRFeHBhbmRlZEtleXMsIGtleUVudGl0aWVzKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuZXhwYW5kZWRLZXlzID0gZXhwYW5kZWRLZXlzIHx8IGRlZmF1bHRFeHBhbmRlZEtleXM7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5vbkRlYm91bmNlRXhwYW5kID0gZGVib3VuY2UodGhpcy5leHBhbmRGb2xkZXJOb2RlLCAyMDAsIHtcbiAgICAgICAgICAgIGxlYWRpbmc6IHRydWUsXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBzdGF0aWMgZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzKG5leHRQcm9wcykge1xuICAgICAgICBjb25zdCBuZXdTdGF0ZSA9IHt9O1xuICAgICAgICBpZiAoJ2V4cGFuZGVkS2V5cycgaW4gbmV4dFByb3BzKSB7XG4gICAgICAgICAgICBuZXdTdGF0ZS5leHBhbmRlZEtleXMgPSBuZXh0UHJvcHMuZXhwYW5kZWRLZXlzO1xuICAgICAgICB9XG4gICAgICAgIGlmICgnc2VsZWN0ZWRLZXlzJyBpbiBuZXh0UHJvcHMpIHtcbiAgICAgICAgICAgIG5ld1N0YXRlLnNlbGVjdGVkS2V5cyA9IG5leHRQcm9wcy5zZWxlY3RlZEtleXM7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ld1N0YXRlO1xuICAgIH1cbiAgICByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiA8Q29uZmlnQ29uc3VtZXI+e3RoaXMucmVuZGVyRGlyZWN0b3J5VHJlZX08L0NvbmZpZ0NvbnN1bWVyPjtcbiAgICB9XG59XG5EaXJlY3RvcnlUcmVlLmRlZmF1bHRQcm9wcyA9IHtcbiAgICBzaG93SWNvbjogdHJ1ZSxcbiAgICBleHBhbmRBY3Rpb246ICdjbGljaycsXG59O1xucG9seWZpbGwoRGlyZWN0b3J5VHJlZSk7XG5leHBvcnQgZGVmYXVsdCBEaXJlY3RvcnlUcmVlO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFUQTtBQUNBO0FBVUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBOzs7OztBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFQQTtBQUNBO0FBUUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBUkE7QUFDQTtBQVNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQVJBO0FBQ0E7QUFTQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFRQTtBQUNBO0FBSUE7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUF2Q0E7QUFDQTtBQXdDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBQ0E7QUFJQTtBQUVBO0FBQ0E7QUFBQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQUNBO0FBS0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUZBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxBO0FBQ0E7QUE3RkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFvR0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFOQTtBQVNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUF4SEE7QUEySEE7QUFDQTs7O0FBVUE7QUFDQTtBQUNBOzs7QUFaQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7OztBQXRJQTtBQUNBO0FBMElBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/antd/es/tree/DirectoryTree.js
