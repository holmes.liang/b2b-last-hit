/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule editOnPaste
 * @format
 * 
 */


var BlockMapBuilder = __webpack_require__(/*! ./BlockMapBuilder */ "./node_modules/draft-js/lib/BlockMapBuilder.js");

var CharacterMetadata = __webpack_require__(/*! ./CharacterMetadata */ "./node_modules/draft-js/lib/CharacterMetadata.js");

var DataTransfer = __webpack_require__(/*! fbjs/lib/DataTransfer */ "./node_modules/fbjs/lib/DataTransfer.js");

var DraftModifier = __webpack_require__(/*! ./DraftModifier */ "./node_modules/draft-js/lib/DraftModifier.js");

var DraftPasteProcessor = __webpack_require__(/*! ./DraftPasteProcessor */ "./node_modules/draft-js/lib/DraftPasteProcessor.js");

var EditorState = __webpack_require__(/*! ./EditorState */ "./node_modules/draft-js/lib/EditorState.js");

var RichTextEditorUtil = __webpack_require__(/*! ./RichTextEditorUtil */ "./node_modules/draft-js/lib/RichTextEditorUtil.js");

var getEntityKeyForSelection = __webpack_require__(/*! ./getEntityKeyForSelection */ "./node_modules/draft-js/lib/getEntityKeyForSelection.js");

var getTextContentFromFiles = __webpack_require__(/*! ./getTextContentFromFiles */ "./node_modules/draft-js/lib/getTextContentFromFiles.js");

var isEventHandled = __webpack_require__(/*! ./isEventHandled */ "./node_modules/draft-js/lib/isEventHandled.js");

var splitTextIntoTextBlocks = __webpack_require__(/*! ./splitTextIntoTextBlocks */ "./node_modules/draft-js/lib/splitTextIntoTextBlocks.js");
/**
 * Paste content.
 */


function editOnPaste(editor, e) {
  e.preventDefault();
  var data = new DataTransfer(e.clipboardData); // Get files, unless this is likely to be a string the user wants inline.

  if (!data.isRichText()) {
    var files = data.getFiles();
    var defaultFileText = data.getText();

    if (files.length > 0) {
      // Allow customized paste handling for images, etc. Otherwise, fall
      // through to insert text contents into the editor.
      if (editor.props.handlePastedFiles && isEventHandled(editor.props.handlePastedFiles(files))) {
        return;
      }

      getTextContentFromFiles(files, function (
      /*string*/
      fileText) {
        fileText = fileText || defaultFileText;

        if (!fileText) {
          return;
        }

        var editorState = editor._latestEditorState;
        var blocks = splitTextIntoTextBlocks(fileText);
        var character = CharacterMetadata.create({
          style: editorState.getCurrentInlineStyle(),
          entity: getEntityKeyForSelection(editorState.getCurrentContent(), editorState.getSelection())
        });
        var currentBlockType = RichTextEditorUtil.getCurrentBlockType(editorState);
        var text = DraftPasteProcessor.processText(blocks, character, currentBlockType);
        var fragment = BlockMapBuilder.createFromArray(text);
        var withInsertedText = DraftModifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), fragment);
        editor.update(EditorState.push(editorState, withInsertedText, 'insert-fragment'));
      });
      return;
    }
  }

  var textBlocks = [];
  var text = data.getText();
  var html = data.getHTML();
  var editorState = editor._latestEditorState;

  if (editor.props.handlePastedText && isEventHandled(editor.props.handlePastedText(text, html, editorState))) {
    return;
  }

  if (text) {
    textBlocks = splitTextIntoTextBlocks(text);
  }

  if (!editor.props.stripPastedStyles) {
    // If the text from the paste event is rich content that matches what we
    // already have on the internal clipboard, assume that we should just use
    // the clipboard fragment for the paste. This will allow us to preserve
    // styling and entities, if any are present. Note that newlines are
    // stripped during comparison -- this is because copy/paste within the
    // editor in Firefox and IE will not include empty lines. The resulting
    // paste will preserve the newlines correctly.
    var internalClipboard = editor.getClipboard();

    if (data.isRichText() && internalClipboard) {
      if ( // If the editorKey is present in the pasted HTML, it should be safe to
      // assume this is an internal paste.
      html.indexOf(editor.getEditorKey()) !== -1 || // The copy may have been made within a single block, in which case the
      // editor key won't be part of the paste. In this case, just check
      // whether the pasted text matches the internal clipboard.
      textBlocks.length === 1 && internalClipboard.size === 1 && internalClipboard.first().getText() === text) {
        editor.update(insertFragment(editor._latestEditorState, internalClipboard));
        return;
      }
    } else if (internalClipboard && data.types.includes('com.apple.webarchive') && !data.types.includes('text/html') && areTextBlocksAndClipboardEqual(textBlocks, internalClipboard)) {
      // Safari does not properly store text/html in some cases.
      // Use the internalClipboard if present and equal to what is on
      // the clipboard. See https://bugs.webkit.org/show_bug.cgi?id=19893.
      editor.update(insertFragment(editor._latestEditorState, internalClipboard));
      return;
    } // If there is html paste data, try to parse that.


    if (html) {
      var htmlFragment = DraftPasteProcessor.processHTML(html, editor.props.blockRenderMap);

      if (htmlFragment) {
        var contentBlocks = htmlFragment.contentBlocks,
            entityMap = htmlFragment.entityMap;

        if (contentBlocks) {
          var htmlMap = BlockMapBuilder.createFromArray(contentBlocks);
          editor.update(insertFragment(editor._latestEditorState, htmlMap, entityMap));
          return;
        }
      }
    } // Otherwise, create a new fragment from our pasted text. Also
    // empty the internal clipboard, since it's no longer valid.


    editor.setClipboard(null);
  }

  if (textBlocks.length) {
    var character = CharacterMetadata.create({
      style: editorState.getCurrentInlineStyle(),
      entity: getEntityKeyForSelection(editorState.getCurrentContent(), editorState.getSelection())
    });
    var currentBlockType = RichTextEditorUtil.getCurrentBlockType(editorState);
    var textFragment = DraftPasteProcessor.processText(textBlocks, character, currentBlockType);
    var textMap = BlockMapBuilder.createFromArray(textFragment);
    editor.update(insertFragment(editor._latestEditorState, textMap));
  }
}

function insertFragment(editorState, fragment, entityMap) {
  var newContent = DraftModifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), fragment); // TODO: merge the entity map once we stop using DraftEntity
  // like this:
  // const mergedEntityMap = newContent.getEntityMap().merge(entityMap);

  return EditorState.push(editorState, newContent.set('entityMap', entityMap), 'insert-fragment');
}

function areTextBlocksAndClipboardEqual(textBlocks, blockMap) {
  return textBlocks.length === blockMap.size && blockMap.valueSeq().every(function (block, ii) {
    return block.getText() === textBlocks[ii];
  });
}

module.exports = editOnPaste;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPblBhc3RlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL2VkaXRPblBhc3RlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDEzLXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKiBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICpcbiAqIFRoaXMgc291cmNlIGNvZGUgaXMgbGljZW5zZWQgdW5kZXIgdGhlIEJTRC1zdHlsZSBsaWNlbnNlIGZvdW5kIGluIHRoZVxuICogTElDRU5TRSBmaWxlIGluIHRoZSByb290IGRpcmVjdG9yeSBvZiB0aGlzIHNvdXJjZSB0cmVlLiBBbiBhZGRpdGlvbmFsIGdyYW50XG4gKiBvZiBwYXRlbnQgcmlnaHRzIGNhbiBiZSBmb3VuZCBpbiB0aGUgUEFURU5UUyBmaWxlIGluIHRoZSBzYW1lIGRpcmVjdG9yeS5cbiAqXG4gKiBAcHJvdmlkZXNNb2R1bGUgZWRpdE9uUGFzdGVcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIEJsb2NrTWFwQnVpbGRlciA9IHJlcXVpcmUoJy4vQmxvY2tNYXBCdWlsZGVyJyk7XG52YXIgQ2hhcmFjdGVyTWV0YWRhdGEgPSByZXF1aXJlKCcuL0NoYXJhY3Rlck1ldGFkYXRhJyk7XG52YXIgRGF0YVRyYW5zZmVyID0gcmVxdWlyZSgnZmJqcy9saWIvRGF0YVRyYW5zZmVyJyk7XG52YXIgRHJhZnRNb2RpZmllciA9IHJlcXVpcmUoJy4vRHJhZnRNb2RpZmllcicpO1xudmFyIERyYWZ0UGFzdGVQcm9jZXNzb3IgPSByZXF1aXJlKCcuL0RyYWZ0UGFzdGVQcm9jZXNzb3InKTtcbnZhciBFZGl0b3JTdGF0ZSA9IHJlcXVpcmUoJy4vRWRpdG9yU3RhdGUnKTtcbnZhciBSaWNoVGV4dEVkaXRvclV0aWwgPSByZXF1aXJlKCcuL1JpY2hUZXh0RWRpdG9yVXRpbCcpO1xuXG52YXIgZ2V0RW50aXR5S2V5Rm9yU2VsZWN0aW9uID0gcmVxdWlyZSgnLi9nZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24nKTtcbnZhciBnZXRUZXh0Q29udGVudEZyb21GaWxlcyA9IHJlcXVpcmUoJy4vZ2V0VGV4dENvbnRlbnRGcm9tRmlsZXMnKTtcbnZhciBpc0V2ZW50SGFuZGxlZCA9IHJlcXVpcmUoJy4vaXNFdmVudEhhbmRsZWQnKTtcbnZhciBzcGxpdFRleHRJbnRvVGV4dEJsb2NrcyA9IHJlcXVpcmUoJy4vc3BsaXRUZXh0SW50b1RleHRCbG9ja3MnKTtcblxuLyoqXG4gKiBQYXN0ZSBjb250ZW50LlxuICovXG5mdW5jdGlvbiBlZGl0T25QYXN0ZShlZGl0b3IsIGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB2YXIgZGF0YSA9IG5ldyBEYXRhVHJhbnNmZXIoZS5jbGlwYm9hcmREYXRhKTtcblxuICAvLyBHZXQgZmlsZXMsIHVubGVzcyB0aGlzIGlzIGxpa2VseSB0byBiZSBhIHN0cmluZyB0aGUgdXNlciB3YW50cyBpbmxpbmUuXG4gIGlmICghZGF0YS5pc1JpY2hUZXh0KCkpIHtcbiAgICB2YXIgZmlsZXMgPSBkYXRhLmdldEZpbGVzKCk7XG4gICAgdmFyIGRlZmF1bHRGaWxlVGV4dCA9IGRhdGEuZ2V0VGV4dCgpO1xuICAgIGlmIChmaWxlcy5sZW5ndGggPiAwKSB7XG4gICAgICAvLyBBbGxvdyBjdXN0b21pemVkIHBhc3RlIGhhbmRsaW5nIGZvciBpbWFnZXMsIGV0Yy4gT3RoZXJ3aXNlLCBmYWxsXG4gICAgICAvLyB0aHJvdWdoIHRvIGluc2VydCB0ZXh0IGNvbnRlbnRzIGludG8gdGhlIGVkaXRvci5cbiAgICAgIGlmIChlZGl0b3IucHJvcHMuaGFuZGxlUGFzdGVkRmlsZXMgJiYgaXNFdmVudEhhbmRsZWQoZWRpdG9yLnByb3BzLmhhbmRsZVBhc3RlZEZpbGVzKGZpbGVzKSkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBnZXRUZXh0Q29udGVudEZyb21GaWxlcyhmaWxlcywgZnVuY3Rpb24gKCAvKnN0cmluZyovZmlsZVRleHQpIHtcbiAgICAgICAgZmlsZVRleHQgPSBmaWxlVGV4dCB8fCBkZWZhdWx0RmlsZVRleHQ7XG4gICAgICAgIGlmICghZmlsZVRleHQpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlO1xuICAgICAgICB2YXIgYmxvY2tzID0gc3BsaXRUZXh0SW50b1RleHRCbG9ja3MoZmlsZVRleHQpO1xuICAgICAgICB2YXIgY2hhcmFjdGVyID0gQ2hhcmFjdGVyTWV0YWRhdGEuY3JlYXRlKHtcbiAgICAgICAgICBzdHlsZTogZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCksXG4gICAgICAgICAgZW50aXR5OiBnZXRFbnRpdHlLZXlGb3JTZWxlY3Rpb24oZWRpdG9yU3RhdGUuZ2V0Q3VycmVudENvbnRlbnQoKSwgZWRpdG9yU3RhdGUuZ2V0U2VsZWN0aW9uKCkpXG4gICAgICAgIH0pO1xuICAgICAgICB2YXIgY3VycmVudEJsb2NrVHlwZSA9IFJpY2hUZXh0RWRpdG9yVXRpbC5nZXRDdXJyZW50QmxvY2tUeXBlKGVkaXRvclN0YXRlKTtcblxuICAgICAgICB2YXIgdGV4dCA9IERyYWZ0UGFzdGVQcm9jZXNzb3IucHJvY2Vzc1RleHQoYmxvY2tzLCBjaGFyYWN0ZXIsIGN1cnJlbnRCbG9ja1R5cGUpO1xuICAgICAgICB2YXIgZnJhZ21lbnQgPSBCbG9ja01hcEJ1aWxkZXIuY3JlYXRlRnJvbUFycmF5KHRleHQpO1xuXG4gICAgICAgIHZhciB3aXRoSW5zZXJ0ZWRUZXh0ID0gRHJhZnRNb2RpZmllci5yZXBsYWNlV2l0aEZyYWdtZW50KGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCksIGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLCBmcmFnbWVudCk7XG5cbiAgICAgICAgZWRpdG9yLnVwZGF0ZShFZGl0b3JTdGF0ZS5wdXNoKGVkaXRvclN0YXRlLCB3aXRoSW5zZXJ0ZWRUZXh0LCAnaW5zZXJ0LWZyYWdtZW50JykpO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG4gIH1cblxuICB2YXIgdGV4dEJsb2NrcyA9IFtdO1xuICB2YXIgdGV4dCA9IGRhdGEuZ2V0VGV4dCgpO1xuICB2YXIgaHRtbCA9IGRhdGEuZ2V0SFRNTCgpO1xuICB2YXIgZWRpdG9yU3RhdGUgPSBlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlO1xuXG4gIGlmIChlZGl0b3IucHJvcHMuaGFuZGxlUGFzdGVkVGV4dCAmJiBpc0V2ZW50SGFuZGxlZChlZGl0b3IucHJvcHMuaGFuZGxlUGFzdGVkVGV4dCh0ZXh0LCBodG1sLCBlZGl0b3JTdGF0ZSkpKSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKHRleHQpIHtcbiAgICB0ZXh0QmxvY2tzID0gc3BsaXRUZXh0SW50b1RleHRCbG9ja3ModGV4dCk7XG4gIH1cblxuICBpZiAoIWVkaXRvci5wcm9wcy5zdHJpcFBhc3RlZFN0eWxlcykge1xuICAgIC8vIElmIHRoZSB0ZXh0IGZyb20gdGhlIHBhc3RlIGV2ZW50IGlzIHJpY2ggY29udGVudCB0aGF0IG1hdGNoZXMgd2hhdCB3ZVxuICAgIC8vIGFscmVhZHkgaGF2ZSBvbiB0aGUgaW50ZXJuYWwgY2xpcGJvYXJkLCBhc3N1bWUgdGhhdCB3ZSBzaG91bGQganVzdCB1c2VcbiAgICAvLyB0aGUgY2xpcGJvYXJkIGZyYWdtZW50IGZvciB0aGUgcGFzdGUuIFRoaXMgd2lsbCBhbGxvdyB1cyB0byBwcmVzZXJ2ZVxuICAgIC8vIHN0eWxpbmcgYW5kIGVudGl0aWVzLCBpZiBhbnkgYXJlIHByZXNlbnQuIE5vdGUgdGhhdCBuZXdsaW5lcyBhcmVcbiAgICAvLyBzdHJpcHBlZCBkdXJpbmcgY29tcGFyaXNvbiAtLSB0aGlzIGlzIGJlY2F1c2UgY29weS9wYXN0ZSB3aXRoaW4gdGhlXG4gICAgLy8gZWRpdG9yIGluIEZpcmVmb3ggYW5kIElFIHdpbGwgbm90IGluY2x1ZGUgZW1wdHkgbGluZXMuIFRoZSByZXN1bHRpbmdcbiAgICAvLyBwYXN0ZSB3aWxsIHByZXNlcnZlIHRoZSBuZXdsaW5lcyBjb3JyZWN0bHkuXG4gICAgdmFyIGludGVybmFsQ2xpcGJvYXJkID0gZWRpdG9yLmdldENsaXBib2FyZCgpO1xuICAgIGlmIChkYXRhLmlzUmljaFRleHQoKSAmJiBpbnRlcm5hbENsaXBib2FyZCkge1xuICAgICAgaWYgKFxuICAgICAgLy8gSWYgdGhlIGVkaXRvcktleSBpcyBwcmVzZW50IGluIHRoZSBwYXN0ZWQgSFRNTCwgaXQgc2hvdWxkIGJlIHNhZmUgdG9cbiAgICAgIC8vIGFzc3VtZSB0aGlzIGlzIGFuIGludGVybmFsIHBhc3RlLlxuICAgICAgaHRtbC5pbmRleE9mKGVkaXRvci5nZXRFZGl0b3JLZXkoKSkgIT09IC0xIHx8XG4gICAgICAvLyBUaGUgY29weSBtYXkgaGF2ZSBiZWVuIG1hZGUgd2l0aGluIGEgc2luZ2xlIGJsb2NrLCBpbiB3aGljaCBjYXNlIHRoZVxuICAgICAgLy8gZWRpdG9yIGtleSB3b24ndCBiZSBwYXJ0IG9mIHRoZSBwYXN0ZS4gSW4gdGhpcyBjYXNlLCBqdXN0IGNoZWNrXG4gICAgICAvLyB3aGV0aGVyIHRoZSBwYXN0ZWQgdGV4dCBtYXRjaGVzIHRoZSBpbnRlcm5hbCBjbGlwYm9hcmQuXG4gICAgICB0ZXh0QmxvY2tzLmxlbmd0aCA9PT0gMSAmJiBpbnRlcm5hbENsaXBib2FyZC5zaXplID09PSAxICYmIGludGVybmFsQ2xpcGJvYXJkLmZpcnN0KCkuZ2V0VGV4dCgpID09PSB0ZXh0KSB7XG4gICAgICAgIGVkaXRvci51cGRhdGUoaW5zZXJ0RnJhZ21lbnQoZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZSwgaW50ZXJuYWxDbGlwYm9hcmQpKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoaW50ZXJuYWxDbGlwYm9hcmQgJiYgZGF0YS50eXBlcy5pbmNsdWRlcygnY29tLmFwcGxlLndlYmFyY2hpdmUnKSAmJiAhZGF0YS50eXBlcy5pbmNsdWRlcygndGV4dC9odG1sJykgJiYgYXJlVGV4dEJsb2Nrc0FuZENsaXBib2FyZEVxdWFsKHRleHRCbG9ja3MsIGludGVybmFsQ2xpcGJvYXJkKSkge1xuICAgICAgLy8gU2FmYXJpIGRvZXMgbm90IHByb3Blcmx5IHN0b3JlIHRleHQvaHRtbCBpbiBzb21lIGNhc2VzLlxuICAgICAgLy8gVXNlIHRoZSBpbnRlcm5hbENsaXBib2FyZCBpZiBwcmVzZW50IGFuZCBlcXVhbCB0byB3aGF0IGlzIG9uXG4gICAgICAvLyB0aGUgY2xpcGJvYXJkLiBTZWUgaHR0cHM6Ly9idWdzLndlYmtpdC5vcmcvc2hvd19idWcuY2dpP2lkPTE5ODkzLlxuICAgICAgZWRpdG9yLnVwZGF0ZShpbnNlcnRGcmFnbWVudChlZGl0b3IuX2xhdGVzdEVkaXRvclN0YXRlLCBpbnRlcm5hbENsaXBib2FyZCkpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIElmIHRoZXJlIGlzIGh0bWwgcGFzdGUgZGF0YSwgdHJ5IHRvIHBhcnNlIHRoYXQuXG4gICAgaWYgKGh0bWwpIHtcbiAgICAgIHZhciBodG1sRnJhZ21lbnQgPSBEcmFmdFBhc3RlUHJvY2Vzc29yLnByb2Nlc3NIVE1MKGh0bWwsIGVkaXRvci5wcm9wcy5ibG9ja1JlbmRlck1hcCk7XG4gICAgICBpZiAoaHRtbEZyYWdtZW50KSB7XG4gICAgICAgIHZhciBjb250ZW50QmxvY2tzID0gaHRtbEZyYWdtZW50LmNvbnRlbnRCbG9ja3MsXG4gICAgICAgICAgICBlbnRpdHlNYXAgPSBodG1sRnJhZ21lbnQuZW50aXR5TWFwO1xuXG4gICAgICAgIGlmIChjb250ZW50QmxvY2tzKSB7XG4gICAgICAgICAgdmFyIGh0bWxNYXAgPSBCbG9ja01hcEJ1aWxkZXIuY3JlYXRlRnJvbUFycmF5KGNvbnRlbnRCbG9ja3MpO1xuICAgICAgICAgIGVkaXRvci51cGRhdGUoaW5zZXJ0RnJhZ21lbnQoZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZSwgaHRtbE1hcCwgZW50aXR5TWFwKSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gT3RoZXJ3aXNlLCBjcmVhdGUgYSBuZXcgZnJhZ21lbnQgZnJvbSBvdXIgcGFzdGVkIHRleHQuIEFsc29cbiAgICAvLyBlbXB0eSB0aGUgaW50ZXJuYWwgY2xpcGJvYXJkLCBzaW5jZSBpdCdzIG5vIGxvbmdlciB2YWxpZC5cbiAgICBlZGl0b3Iuc2V0Q2xpcGJvYXJkKG51bGwpO1xuICB9XG5cbiAgaWYgKHRleHRCbG9ja3MubGVuZ3RoKSB7XG4gICAgdmFyIGNoYXJhY3RlciA9IENoYXJhY3Rlck1ldGFkYXRhLmNyZWF0ZSh7XG4gICAgICBzdHlsZTogZWRpdG9yU3RhdGUuZ2V0Q3VycmVudElubGluZVN0eWxlKCksXG4gICAgICBlbnRpdHk6IGdldEVudGl0eUtleUZvclNlbGVjdGlvbihlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpLCBlZGl0b3JTdGF0ZS5nZXRTZWxlY3Rpb24oKSlcbiAgICB9KTtcblxuICAgIHZhciBjdXJyZW50QmxvY2tUeXBlID0gUmljaFRleHRFZGl0b3JVdGlsLmdldEN1cnJlbnRCbG9ja1R5cGUoZWRpdG9yU3RhdGUpO1xuXG4gICAgdmFyIHRleHRGcmFnbWVudCA9IERyYWZ0UGFzdGVQcm9jZXNzb3IucHJvY2Vzc1RleHQodGV4dEJsb2NrcywgY2hhcmFjdGVyLCBjdXJyZW50QmxvY2tUeXBlKTtcblxuICAgIHZhciB0ZXh0TWFwID0gQmxvY2tNYXBCdWlsZGVyLmNyZWF0ZUZyb21BcnJheSh0ZXh0RnJhZ21lbnQpO1xuICAgIGVkaXRvci51cGRhdGUoaW5zZXJ0RnJhZ21lbnQoZWRpdG9yLl9sYXRlc3RFZGl0b3JTdGF0ZSwgdGV4dE1hcCkpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGluc2VydEZyYWdtZW50KGVkaXRvclN0YXRlLCBmcmFnbWVudCwgZW50aXR5TWFwKSB7XG4gIHZhciBuZXdDb250ZW50ID0gRHJhZnRNb2RpZmllci5yZXBsYWNlV2l0aEZyYWdtZW50KGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCksIGVkaXRvclN0YXRlLmdldFNlbGVjdGlvbigpLCBmcmFnbWVudCk7XG4gIC8vIFRPRE86IG1lcmdlIHRoZSBlbnRpdHkgbWFwIG9uY2Ugd2Ugc3RvcCB1c2luZyBEcmFmdEVudGl0eVxuICAvLyBsaWtlIHRoaXM6XG4gIC8vIGNvbnN0IG1lcmdlZEVudGl0eU1hcCA9IG5ld0NvbnRlbnQuZ2V0RW50aXR5TWFwKCkubWVyZ2UoZW50aXR5TWFwKTtcblxuICByZXR1cm4gRWRpdG9yU3RhdGUucHVzaChlZGl0b3JTdGF0ZSwgbmV3Q29udGVudC5zZXQoJ2VudGl0eU1hcCcsIGVudGl0eU1hcCksICdpbnNlcnQtZnJhZ21lbnQnKTtcbn1cblxuZnVuY3Rpb24gYXJlVGV4dEJsb2Nrc0FuZENsaXBib2FyZEVxdWFsKHRleHRCbG9ja3MsIGJsb2NrTWFwKSB7XG4gIHJldHVybiB0ZXh0QmxvY2tzLmxlbmd0aCA9PT0gYmxvY2tNYXAuc2l6ZSAmJiBibG9ja01hcC52YWx1ZVNlcSgpLmV2ZXJ5KGZ1bmN0aW9uIChibG9jaywgaWkpIHtcbiAgICByZXR1cm4gYmxvY2suZ2V0VGV4dCgpID09PSB0ZXh0QmxvY2tzW2lpXTtcbiAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZWRpdE9uUGFzdGU7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/editOnPaste.js
