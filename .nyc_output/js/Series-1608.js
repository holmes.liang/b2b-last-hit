/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _config = __webpack_require__(/*! ../config */ "./node_modules/echarts/lib/config.js");

var __DEV__ = _config.__DEV__;

var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var env = __webpack_require__(/*! zrender/lib/core/env */ "./node_modules/zrender/lib/core/env.js");

var _format = __webpack_require__(/*! ../util/format */ "./node_modules/echarts/lib/util/format.js");

var formatTime = _format.formatTime;
var encodeHTML = _format.encodeHTML;
var addCommas = _format.addCommas;
var getTooltipMarker = _format.getTooltipMarker;

var modelUtil = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var ComponentModel = __webpack_require__(/*! ./Component */ "./node_modules/echarts/lib/model/Component.js");

var colorPaletteMixin = __webpack_require__(/*! ./mixin/colorPalette */ "./node_modules/echarts/lib/model/mixin/colorPalette.js");

var dataFormatMixin = __webpack_require__(/*! ../model/mixin/dataFormat */ "./node_modules/echarts/lib/model/mixin/dataFormat.js");

var _layout = __webpack_require__(/*! ../util/layout */ "./node_modules/echarts/lib/util/layout.js");

var getLayoutParams = _layout.getLayoutParams;
var mergeLayoutParam = _layout.mergeLayoutParam;

var _task = __webpack_require__(/*! ../stream/task */ "./node_modules/echarts/lib/stream/task.js");

var createTask = _task.createTask;

var _sourceHelper = __webpack_require__(/*! ../data/helper/sourceHelper */ "./node_modules/echarts/lib/data/helper/sourceHelper.js");

var prepareSource = _sourceHelper.prepareSource;
var _getSource = _sourceHelper.getSource;

var _dataProvider = __webpack_require__(/*! ../data/helper/dataProvider */ "./node_modules/echarts/lib/data/helper/dataProvider.js");

var retrieveRawValue = _dataProvider.retrieveRawValue;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

var inner = modelUtil.makeInner();
var SeriesModel = ComponentModel.extend({
  type: 'series.__base__',

  /**
   * @readOnly
   */
  seriesIndex: 0,
  // coodinateSystem will be injected in the echarts/CoordinateSystem
  coordinateSystem: null,

  /**
   * @type {Object}
   * @protected
   */
  defaultOption: null,

  /**
   * Data provided for legend
   * @type {Function}
   */
  // PENDING
  legendDataProvider: null,

  /**
   * Access path of color for visual
   */
  visualColorAccessPath: 'itemStyle.color',

  /**
   * Support merge layout params.
   * Only support 'box' now (left/right/top/bottom/width/height).
   * @type {string|Object} Object can be {ignoreSize: true}
   * @readOnly
   */
  layoutMode: null,
  init: function init(option, parentModel, ecModel, extraOpt) {
    /**
     * @type {number}
     * @readOnly
     */
    this.seriesIndex = this.componentIndex;
    this.dataTask = createTask({
      count: dataTaskCount,
      reset: dataTaskReset
    });
    this.dataTask.context = {
      model: this
    };
    this.mergeDefaultAndTheme(option, ecModel);
    prepareSource(this);
    var data = this.getInitialData(option, ecModel);
    wrapData(data, this);
    this.dataTask.context.data = data;
    /**
     * @type {module:echarts/data/List|module:echarts/data/Tree|module:echarts/data/Graph}
     * @private
     */

    inner(this).dataBeforeProcessed = data; // If we reverse the order (make data firstly, and then make
    // dataBeforeProcessed by cloneShallow), cloneShallow will
    // cause data.graph.data !== data when using
    // module:echarts/data/Graph or module:echarts/data/Tree.
    // See module:echarts/data/helper/linkList
    // Theoretically, it is unreasonable to call `seriesModel.getData()` in the model
    // init or merge stage, because the data can be restored. So we do not `restoreData`
    // and `setData` here, which forbids calling `seriesModel.getData()` in this stage.
    // Call `seriesModel.getRawData()` instead.
    // this.restoreData();

    autoSeriesName(this);
  },

  /**
   * Util for merge default and theme to option
   * @param  {Object} option
   * @param  {module:echarts/model/Global} ecModel
   */
  mergeDefaultAndTheme: function mergeDefaultAndTheme(option, ecModel) {
    var layoutMode = this.layoutMode;
    var inputPositionParams = layoutMode ? getLayoutParams(option) : {}; // Backward compat: using subType on theme.
    // But if name duplicate between series subType
    // (for example: parallel) add component mainType,
    // add suffix 'Series'.

    var themeSubType = this.subType;

    if (ComponentModel.hasClass(themeSubType)) {
      themeSubType += 'Series';
    }

    zrUtil.merge(option, ecModel.getTheme().get(this.subType));
    zrUtil.merge(option, this.getDefaultOption()); // Default label emphasis `show`

    modelUtil.defaultEmphasis(option, 'label', ['show']);
    this.fillDataTextStyle(option.data);

    if (layoutMode) {
      mergeLayoutParam(option, inputPositionParams, layoutMode);
    }
  },
  mergeOption: function mergeOption(newSeriesOption, ecModel) {
    // this.settingTask.dirty();
    newSeriesOption = zrUtil.merge(this.option, newSeriesOption, true);
    this.fillDataTextStyle(newSeriesOption.data);
    var layoutMode = this.layoutMode;

    if (layoutMode) {
      mergeLayoutParam(this.option, newSeriesOption, layoutMode);
    }

    prepareSource(this);
    var data = this.getInitialData(newSeriesOption, ecModel);
    wrapData(data, this);
    this.dataTask.dirty();
    this.dataTask.context.data = data;
    inner(this).dataBeforeProcessed = data;
    autoSeriesName(this);
  },
  fillDataTextStyle: function fillDataTextStyle(data) {
    // Default data label emphasis `show`
    // FIXME Tree structure data ?
    // FIXME Performance ?
    if (data && !zrUtil.isTypedArray(data)) {
      var props = ['show'];

      for (var i = 0; i < data.length; i++) {
        if (data[i] && data[i].label) {
          modelUtil.defaultEmphasis(data[i], 'label', props);
        }
      }
    }
  },

  /**
   * Init a data structure from data related option in series
   * Must be overwritten
   */
  getInitialData: function getInitialData() {},

  /**
   * Append data to list
   * @param {Object} params
   * @param {Array|TypedArray} params.data
   */
  appendData: function appendData(params) {
    // FIXME ???
    // (1) If data from dataset, forbidden append.
    // (2) support append data of dataset.
    var data = this.getRawData();
    data.appendData(params.data);
  },

  /**
   * Consider some method like `filter`, `map` need make new data,
   * We should make sure that `seriesModel.getData()` get correct
   * data in the stream procedure. So we fetch data from upstream
   * each time `task.perform` called.
   * @param {string} [dataType]
   * @return {module:echarts/data/List}
   */
  getData: function getData(dataType) {
    var task = getCurrentTask(this);

    if (task) {
      var data = task.context.data;
      return dataType == null ? data : data.getLinkedData(dataType);
    } else {
      // When series is not alive (that may happen when click toolbox
      // restore or setOption with not merge mode), series data may
      // be still need to judge animation or something when graphic
      // elements want to know whether fade out.
      return inner(this).data;
    }
  },

  /**
   * @param {module:echarts/data/List} data
   */
  setData: function setData(data) {
    var task = getCurrentTask(this);

    if (task) {
      var context = task.context; // Consider case: filter, data sample.

      if (context.data !== data && task.modifyOutputEnd) {
        task.setOutputEnd(data.count());
      }

      context.outputData = data; // Caution: setData should update context.data,
      // Because getData may be called multiply in a
      // single stage and expect to get the data just
      // set. (For example, AxisProxy, x y both call
      // getData and setDate sequentially).
      // So the context.data should be fetched from
      // upstream each time when a stage starts to be
      // performed.

      if (task !== this.dataTask) {
        context.data = data;
      }
    }

    inner(this).data = data;
  },

  /**
   * @see {module:echarts/data/helper/sourceHelper#getSource}
   * @return {module:echarts/data/Source} source
   */
  getSource: function getSource() {
    return _getSource(this);
  },

  /**
   * Get data before processed
   * @return {module:echarts/data/List}
   */
  getRawData: function getRawData() {
    return inner(this).dataBeforeProcessed;
  },

  /**
   * Get base axis if has coordinate system and has axis.
   * By default use coordSys.getBaseAxis();
   * Can be overrided for some chart.
   * @return {type} description
   */
  getBaseAxis: function getBaseAxis() {
    var coordSys = this.coordinateSystem;
    return coordSys && coordSys.getBaseAxis && coordSys.getBaseAxis();
  },
  // FIXME

  /**
   * Default tooltip formatter
   *
   * @param {number} dataIndex
   * @param {boolean} [multipleSeries=false]
   * @param {number} [dataType]
   * @param {string} [renderMode='html'] valid values: 'html' and 'richText'.
   *                                     'html' is used for rendering tooltip in extra DOM form, and the result
   *                                     string is used as DOM HTML content.
   *                                     'richText' is used for rendering tooltip in rich text form, for those where
   *                                     DOM operation is not supported.
   * @return {Object} formatted tooltip with `html` and `markers`
   */
  formatTooltip: function formatTooltip(dataIndex, multipleSeries, dataType, renderMode) {
    var series = this;
    renderMode = renderMode || 'html';
    var newLine = renderMode === 'html' ? '<br/>' : '\n';
    var isRichText = renderMode === 'richText';
    var markers = {};
    var markerId = 0;

    function formatArrayValue(value) {
      // ??? TODO refactor these logic.
      // check: category-no-encode-has-axis-data in dataset.html
      var vertially = zrUtil.reduce(value, function (vertially, val, idx) {
        var dimItem = data.getDimensionInfo(idx);
        return vertially |= dimItem && dimItem.tooltip !== false && dimItem.displayName != null;
      }, 0);
      var result = [];
      tooltipDims.length ? zrUtil.each(tooltipDims, function (dim) {
        setEachItem(retrieveRawValue(data, dataIndex, dim), dim);
      }) // By default, all dims is used on tooltip.
      : zrUtil.each(value, setEachItem);

      function setEachItem(val, dim) {
        var dimInfo = data.getDimensionInfo(dim); // If `dimInfo.tooltip` is not set, show tooltip.

        if (!dimInfo || dimInfo.otherDims.tooltip === false) {
          return;
        }

        var dimType = dimInfo.type;
        var markName = 'sub' + series.seriesIndex + 'at' + markerId;
        var dimHead = getTooltipMarker({
          color: color,
          type: 'subItem',
          renderMode: renderMode,
          markerId: markName
        });
        var dimHeadStr = typeof dimHead === 'string' ? dimHead : dimHead.content;
        var valStr = (vertially ? dimHeadStr + encodeHTML(dimInfo.displayName || '-') + ': ' : '') + // FIXME should not format time for raw data?
        encodeHTML(dimType === 'ordinal' ? val + '' : dimType === 'time' ? multipleSeries ? '' : formatTime('yyyy/MM/dd hh:mm:ss', val) : addCommas(val));
        valStr && result.push(valStr);

        if (isRichText) {
          markers[markName] = color;
          ++markerId;
        }
      }

      var newLine = vertially ? isRichText ? '\n' : '<br/>' : '';
      var content = newLine + result.join(newLine || ', ');
      return {
        renderMode: renderMode,
        content: content,
        style: markers
      };
    }

    function formatSingleValue(val) {
      // return encodeHTML(addCommas(val));
      return {
        renderMode: renderMode,
        content: encodeHTML(addCommas(val)),
        style: markers
      };
    }

    var data = this.getData();
    var tooltipDims = data.mapDimension('defaultedTooltip', true);
    var tooltipDimLen = tooltipDims.length;
    var value = this.getRawValue(dataIndex);
    var isValueArr = zrUtil.isArray(value);
    var color = data.getItemVisual(dataIndex, 'color');

    if (zrUtil.isObject(color) && color.colorStops) {
      color = (color.colorStops[0] || {}).color;
    }

    color = color || 'transparent'; // Complicated rule for pretty tooltip.

    var formattedValue = tooltipDimLen > 1 || isValueArr && !tooltipDimLen ? formatArrayValue(value) : tooltipDimLen ? formatSingleValue(retrieveRawValue(data, dataIndex, tooltipDims[0])) : formatSingleValue(isValueArr ? value[0] : value);
    var content = formattedValue.content;
    var markName = series.seriesIndex + 'at' + markerId;
    var colorEl = getTooltipMarker({
      color: color,
      type: 'item',
      renderMode: renderMode,
      markerId: markName
    });
    markers[markName] = color;
    ++markerId;
    var name = data.getName(dataIndex);
    var seriesName = this.name;

    if (!modelUtil.isNameSpecified(this)) {
      seriesName = '';
    }

    seriesName = seriesName ? encodeHTML(seriesName) + (!multipleSeries ? newLine : ': ') : '';
    var colorStr = typeof colorEl === 'string' ? colorEl : colorEl.content;
    var html = !multipleSeries ? seriesName + colorStr + (name ? encodeHTML(name) + ': ' + content : content) : colorStr + seriesName + content;
    return {
      html: html,
      markers: markers
    };
  },

  /**
   * @return {boolean}
   */
  isAnimationEnabled: function isAnimationEnabled() {
    if (env.node) {
      return false;
    }

    var animationEnabled = this.getShallow('animation');

    if (animationEnabled) {
      if (this.getData().count() > this.getShallow('animationThreshold')) {
        animationEnabled = false;
      }
    }

    return animationEnabled;
  },
  restoreData: function restoreData() {
    this.dataTask.dirty();
  },
  getColorFromPalette: function getColorFromPalette(name, scope, requestColorNum) {
    var ecModel = this.ecModel; // PENDING

    var color = colorPaletteMixin.getColorFromPalette.call(this, name, scope, requestColorNum);

    if (!color) {
      color = ecModel.getColorFromPalette(name, scope, requestColorNum);
    }

    return color;
  },

  /**
   * Use `data.mapDimension(coordDim, true)` instead.
   * @deprecated
   */
  coordDimToDataDim: function coordDimToDataDim(coordDim) {
    return this.getRawData().mapDimension(coordDim, true);
  },

  /**
   * Get progressive rendering count each step
   * @return {number}
   */
  getProgressive: function getProgressive() {
    return this.get('progressive');
  },

  /**
   * Get progressive rendering count each step
   * @return {number}
   */
  getProgressiveThreshold: function getProgressiveThreshold() {
    return this.get('progressiveThreshold');
  },

  /**
   * Get data indices for show tooltip content. See tooltip.
   * @abstract
   * @param {Array.<string>|string} dim
   * @param {Array.<number>} value
   * @param {module:echarts/coord/single/SingleAxis} baseAxis
   * @return {Object} {dataIndices, nestestValue}.
   */
  getAxisTooltipData: null,

  /**
   * See tooltip.
   * @abstract
   * @param {number} dataIndex
   * @return {Array.<number>} Point of tooltip. null/undefined can be returned.
   */
  getTooltipPosition: null,

  /**
   * @see {module:echarts/stream/Scheduler}
   */
  pipeTask: null,

  /**
   * Convinient for override in extended class.
   * @protected
   * @type {Function}
   */
  preventIncremental: null,

  /**
   * @public
   * @readOnly
   * @type {Object}
   */
  pipelineContext: null
});
zrUtil.mixin(SeriesModel, dataFormatMixin);
zrUtil.mixin(SeriesModel, colorPaletteMixin);
/**
 * MUST be called after `prepareSource` called
 * Here we need to make auto series, especially for auto legend. But we
 * do not modify series.name in option to avoid side effects.
 */

function autoSeriesName(seriesModel) {
  // User specified name has higher priority, otherwise it may cause
  // series can not be queried unexpectedly.
  var name = seriesModel.name;

  if (!modelUtil.isNameSpecified(seriesModel)) {
    seriesModel.name = getSeriesAutoName(seriesModel) || name;
  }
}

function getSeriesAutoName(seriesModel) {
  var data = seriesModel.getRawData();
  var dataDims = data.mapDimension('seriesName', true);
  var nameArr = [];
  zrUtil.each(dataDims, function (dataDim) {
    var dimInfo = data.getDimensionInfo(dataDim);
    dimInfo.displayName && nameArr.push(dimInfo.displayName);
  });
  return nameArr.join(' ');
}

function dataTaskCount(context) {
  return context.model.getRawData().count();
}

function dataTaskReset(context) {
  var seriesModel = context.model;
  seriesModel.setData(seriesModel.getRawData().cloneShallow());
  return dataTaskProgress;
}

function dataTaskProgress(param, context) {
  // Avoid repead cloneShallow when data just created in reset.
  if (param.end > context.outputData.count()) {
    context.model.getRawData().cloneShallow(context.outputData);
  }
} // TODO refactor


function wrapData(data, seriesModel) {
  zrUtil.each(data.CHANGABLE_METHODS, function (methodName) {
    data.wrapMethod(methodName, zrUtil.curry(onDataSelfChange, seriesModel));
  });
}

function onDataSelfChange(seriesModel) {
  var task = getCurrentTask(seriesModel);

  if (task) {
    // Consider case: filter, selectRange
    task.setOutputEnd(this.count());
  }
}

function getCurrentTask(seriesModel) {
  var scheduler = (seriesModel.ecModel || {}).scheduler;
  var pipeline = scheduler && scheduler.getPipeline(seriesModel.uid);

  if (pipeline) {
    // When pipline finished, the currrentTask keep the last
    // task (renderTask).
    var task = pipeline.currentTask;

    if (task) {
      var agentStubMap = task.agentStubMap;

      if (agentStubMap) {
        task = agentStubMap.get(seriesModel.uid);
      }
    }

    return task;
  }
}

var _default = SeriesModel;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvU2VyaWVzLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbW9kZWwvU2VyaWVzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIlxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG52YXIgX2NvbmZpZyA9IHJlcXVpcmUoXCIuLi9jb25maWdcIik7XG5cbnZhciBfX0RFVl9fID0gX2NvbmZpZy5fX0RFVl9fO1xuXG52YXIgenJVdGlsID0gcmVxdWlyZShcInpyZW5kZXIvbGliL2NvcmUvdXRpbFwiKTtcblxudmFyIGVudiA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL2VudlwiKTtcblxudmFyIF9mb3JtYXQgPSByZXF1aXJlKFwiLi4vdXRpbC9mb3JtYXRcIik7XG5cbnZhciBmb3JtYXRUaW1lID0gX2Zvcm1hdC5mb3JtYXRUaW1lO1xudmFyIGVuY29kZUhUTUwgPSBfZm9ybWF0LmVuY29kZUhUTUw7XG52YXIgYWRkQ29tbWFzID0gX2Zvcm1hdC5hZGRDb21tYXM7XG52YXIgZ2V0VG9vbHRpcE1hcmtlciA9IF9mb3JtYXQuZ2V0VG9vbHRpcE1hcmtlcjtcblxudmFyIG1vZGVsVXRpbCA9IHJlcXVpcmUoXCIuLi91dGlsL21vZGVsXCIpO1xuXG52YXIgQ29tcG9uZW50TW9kZWwgPSByZXF1aXJlKFwiLi9Db21wb25lbnRcIik7XG5cbnZhciBjb2xvclBhbGV0dGVNaXhpbiA9IHJlcXVpcmUoXCIuL21peGluL2NvbG9yUGFsZXR0ZVwiKTtcblxudmFyIGRhdGFGb3JtYXRNaXhpbiA9IHJlcXVpcmUoXCIuLi9tb2RlbC9taXhpbi9kYXRhRm9ybWF0XCIpO1xuXG52YXIgX2xheW91dCA9IHJlcXVpcmUoXCIuLi91dGlsL2xheW91dFwiKTtcblxudmFyIGdldExheW91dFBhcmFtcyA9IF9sYXlvdXQuZ2V0TGF5b3V0UGFyYW1zO1xudmFyIG1lcmdlTGF5b3V0UGFyYW0gPSBfbGF5b3V0Lm1lcmdlTGF5b3V0UGFyYW07XG5cbnZhciBfdGFzayA9IHJlcXVpcmUoXCIuLi9zdHJlYW0vdGFza1wiKTtcblxudmFyIGNyZWF0ZVRhc2sgPSBfdGFzay5jcmVhdGVUYXNrO1xuXG52YXIgX3NvdXJjZUhlbHBlciA9IHJlcXVpcmUoXCIuLi9kYXRhL2hlbHBlci9zb3VyY2VIZWxwZXJcIik7XG5cbnZhciBwcmVwYXJlU291cmNlID0gX3NvdXJjZUhlbHBlci5wcmVwYXJlU291cmNlO1xudmFyIGdldFNvdXJjZSA9IF9zb3VyY2VIZWxwZXIuZ2V0U291cmNlO1xuXG52YXIgX2RhdGFQcm92aWRlciA9IHJlcXVpcmUoXCIuLi9kYXRhL2hlbHBlci9kYXRhUHJvdmlkZXJcIik7XG5cbnZhciByZXRyaWV2ZVJhd1ZhbHVlID0gX2RhdGFQcm92aWRlci5yZXRyaWV2ZVJhd1ZhbHVlO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG52YXIgaW5uZXIgPSBtb2RlbFV0aWwubWFrZUlubmVyKCk7XG52YXIgU2VyaWVzTW9kZWwgPSBDb21wb25lbnRNb2RlbC5leHRlbmQoe1xuICB0eXBlOiAnc2VyaWVzLl9fYmFzZV9fJyxcblxuICAvKipcbiAgICogQHJlYWRPbmx5XG4gICAqL1xuICBzZXJpZXNJbmRleDogMCxcbiAgLy8gY29vZGluYXRlU3lzdGVtIHdpbGwgYmUgaW5qZWN0ZWQgaW4gdGhlIGVjaGFydHMvQ29vcmRpbmF0ZVN5c3RlbVxuICBjb29yZGluYXRlU3lzdGVtOiBudWxsLFxuXG4gIC8qKlxuICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgKiBAcHJvdGVjdGVkXG4gICAqL1xuICBkZWZhdWx0T3B0aW9uOiBudWxsLFxuXG4gIC8qKlxuICAgKiBEYXRhIHByb3ZpZGVkIGZvciBsZWdlbmRcbiAgICogQHR5cGUge0Z1bmN0aW9ufVxuICAgKi9cbiAgLy8gUEVORElOR1xuICBsZWdlbmREYXRhUHJvdmlkZXI6IG51bGwsXG5cbiAgLyoqXG4gICAqIEFjY2VzcyBwYXRoIG9mIGNvbG9yIGZvciB2aXN1YWxcbiAgICovXG4gIHZpc3VhbENvbG9yQWNjZXNzUGF0aDogJ2l0ZW1TdHlsZS5jb2xvcicsXG5cbiAgLyoqXG4gICAqIFN1cHBvcnQgbWVyZ2UgbGF5b3V0IHBhcmFtcy5cbiAgICogT25seSBzdXBwb3J0ICdib3gnIG5vdyAobGVmdC9yaWdodC90b3AvYm90dG9tL3dpZHRoL2hlaWdodCkuXG4gICAqIEB0eXBlIHtzdHJpbmd8T2JqZWN0fSBPYmplY3QgY2FuIGJlIHtpZ25vcmVTaXplOiB0cnVlfVxuICAgKiBAcmVhZE9ubHlcbiAgICovXG4gIGxheW91dE1vZGU6IG51bGwsXG4gIGluaXQ6IGZ1bmN0aW9uIChvcHRpb24sIHBhcmVudE1vZGVsLCBlY01vZGVsLCBleHRyYU9wdCkge1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICogQHJlYWRPbmx5XG4gICAgICovXG4gICAgdGhpcy5zZXJpZXNJbmRleCA9IHRoaXMuY29tcG9uZW50SW5kZXg7XG4gICAgdGhpcy5kYXRhVGFzayA9IGNyZWF0ZVRhc2soe1xuICAgICAgY291bnQ6IGRhdGFUYXNrQ291bnQsXG4gICAgICByZXNldDogZGF0YVRhc2tSZXNldFxuICAgIH0pO1xuICAgIHRoaXMuZGF0YVRhc2suY29udGV4dCA9IHtcbiAgICAgIG1vZGVsOiB0aGlzXG4gICAgfTtcbiAgICB0aGlzLm1lcmdlRGVmYXVsdEFuZFRoZW1lKG9wdGlvbiwgZWNNb2RlbCk7XG4gICAgcHJlcGFyZVNvdXJjZSh0aGlzKTtcbiAgICB2YXIgZGF0YSA9IHRoaXMuZ2V0SW5pdGlhbERhdGEob3B0aW9uLCBlY01vZGVsKTtcbiAgICB3cmFwRGF0YShkYXRhLCB0aGlzKTtcbiAgICB0aGlzLmRhdGFUYXNrLmNvbnRleHQuZGF0YSA9IGRhdGE7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fG1vZHVsZTplY2hhcnRzL2RhdGEvVHJlZXxtb2R1bGU6ZWNoYXJ0cy9kYXRhL0dyYXBofVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgaW5uZXIodGhpcykuZGF0YUJlZm9yZVByb2Nlc3NlZCA9IGRhdGE7IC8vIElmIHdlIHJldmVyc2UgdGhlIG9yZGVyIChtYWtlIGRhdGEgZmlyc3RseSwgYW5kIHRoZW4gbWFrZVxuICAgIC8vIGRhdGFCZWZvcmVQcm9jZXNzZWQgYnkgY2xvbmVTaGFsbG93KSwgY2xvbmVTaGFsbG93IHdpbGxcbiAgICAvLyBjYXVzZSBkYXRhLmdyYXBoLmRhdGEgIT09IGRhdGEgd2hlbiB1c2luZ1xuICAgIC8vIG1vZHVsZTplY2hhcnRzL2RhdGEvR3JhcGggb3IgbW9kdWxlOmVjaGFydHMvZGF0YS9UcmVlLlxuICAgIC8vIFNlZSBtb2R1bGU6ZWNoYXJ0cy9kYXRhL2hlbHBlci9saW5rTGlzdFxuICAgIC8vIFRoZW9yZXRpY2FsbHksIGl0IGlzIHVucmVhc29uYWJsZSB0byBjYWxsIGBzZXJpZXNNb2RlbC5nZXREYXRhKClgIGluIHRoZSBtb2RlbFxuICAgIC8vIGluaXQgb3IgbWVyZ2Ugc3RhZ2UsIGJlY2F1c2UgdGhlIGRhdGEgY2FuIGJlIHJlc3RvcmVkLiBTbyB3ZSBkbyBub3QgYHJlc3RvcmVEYXRhYFxuICAgIC8vIGFuZCBgc2V0RGF0YWAgaGVyZSwgd2hpY2ggZm9yYmlkcyBjYWxsaW5nIGBzZXJpZXNNb2RlbC5nZXREYXRhKClgIGluIHRoaXMgc3RhZ2UuXG4gICAgLy8gQ2FsbCBgc2VyaWVzTW9kZWwuZ2V0UmF3RGF0YSgpYCBpbnN0ZWFkLlxuICAgIC8vIHRoaXMucmVzdG9yZURhdGEoKTtcblxuICAgIGF1dG9TZXJpZXNOYW1lKHRoaXMpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBVdGlsIGZvciBtZXJnZSBkZWZhdWx0IGFuZCB0aGVtZSB0byBvcHRpb25cbiAgICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25cbiAgICogQHBhcmFtICB7bW9kdWxlOmVjaGFydHMvbW9kZWwvR2xvYmFsfSBlY01vZGVsXG4gICAqL1xuICBtZXJnZURlZmF1bHRBbmRUaGVtZTogZnVuY3Rpb24gKG9wdGlvbiwgZWNNb2RlbCkge1xuICAgIHZhciBsYXlvdXRNb2RlID0gdGhpcy5sYXlvdXRNb2RlO1xuICAgIHZhciBpbnB1dFBvc2l0aW9uUGFyYW1zID0gbGF5b3V0TW9kZSA/IGdldExheW91dFBhcmFtcyhvcHRpb24pIDoge307IC8vIEJhY2t3YXJkIGNvbXBhdDogdXNpbmcgc3ViVHlwZSBvbiB0aGVtZS5cbiAgICAvLyBCdXQgaWYgbmFtZSBkdXBsaWNhdGUgYmV0d2VlbiBzZXJpZXMgc3ViVHlwZVxuICAgIC8vIChmb3IgZXhhbXBsZTogcGFyYWxsZWwpIGFkZCBjb21wb25lbnQgbWFpblR5cGUsXG4gICAgLy8gYWRkIHN1ZmZpeCAnU2VyaWVzJy5cblxuICAgIHZhciB0aGVtZVN1YlR5cGUgPSB0aGlzLnN1YlR5cGU7XG5cbiAgICBpZiAoQ29tcG9uZW50TW9kZWwuaGFzQ2xhc3ModGhlbWVTdWJUeXBlKSkge1xuICAgICAgdGhlbWVTdWJUeXBlICs9ICdTZXJpZXMnO1xuICAgIH1cblxuICAgIHpyVXRpbC5tZXJnZShvcHRpb24sIGVjTW9kZWwuZ2V0VGhlbWUoKS5nZXQodGhpcy5zdWJUeXBlKSk7XG4gICAgenJVdGlsLm1lcmdlKG9wdGlvbiwgdGhpcy5nZXREZWZhdWx0T3B0aW9uKCkpOyAvLyBEZWZhdWx0IGxhYmVsIGVtcGhhc2lzIGBzaG93YFxuXG4gICAgbW9kZWxVdGlsLmRlZmF1bHRFbXBoYXNpcyhvcHRpb24sICdsYWJlbCcsIFsnc2hvdyddKTtcbiAgICB0aGlzLmZpbGxEYXRhVGV4dFN0eWxlKG9wdGlvbi5kYXRhKTtcblxuICAgIGlmIChsYXlvdXRNb2RlKSB7XG4gICAgICBtZXJnZUxheW91dFBhcmFtKG9wdGlvbiwgaW5wdXRQb3NpdGlvblBhcmFtcywgbGF5b3V0TW9kZSk7XG4gICAgfVxuICB9LFxuICBtZXJnZU9wdGlvbjogZnVuY3Rpb24gKG5ld1Nlcmllc09wdGlvbiwgZWNNb2RlbCkge1xuICAgIC8vIHRoaXMuc2V0dGluZ1Rhc2suZGlydHkoKTtcbiAgICBuZXdTZXJpZXNPcHRpb24gPSB6clV0aWwubWVyZ2UodGhpcy5vcHRpb24sIG5ld1Nlcmllc09wdGlvbiwgdHJ1ZSk7XG4gICAgdGhpcy5maWxsRGF0YVRleHRTdHlsZShuZXdTZXJpZXNPcHRpb24uZGF0YSk7XG4gICAgdmFyIGxheW91dE1vZGUgPSB0aGlzLmxheW91dE1vZGU7XG5cbiAgICBpZiAobGF5b3V0TW9kZSkge1xuICAgICAgbWVyZ2VMYXlvdXRQYXJhbSh0aGlzLm9wdGlvbiwgbmV3U2VyaWVzT3B0aW9uLCBsYXlvdXRNb2RlKTtcbiAgICB9XG5cbiAgICBwcmVwYXJlU291cmNlKHRoaXMpO1xuICAgIHZhciBkYXRhID0gdGhpcy5nZXRJbml0aWFsRGF0YShuZXdTZXJpZXNPcHRpb24sIGVjTW9kZWwpO1xuICAgIHdyYXBEYXRhKGRhdGEsIHRoaXMpO1xuICAgIHRoaXMuZGF0YVRhc2suZGlydHkoKTtcbiAgICB0aGlzLmRhdGFUYXNrLmNvbnRleHQuZGF0YSA9IGRhdGE7XG4gICAgaW5uZXIodGhpcykuZGF0YUJlZm9yZVByb2Nlc3NlZCA9IGRhdGE7XG4gICAgYXV0b1Nlcmllc05hbWUodGhpcyk7XG4gIH0sXG4gIGZpbGxEYXRhVGV4dFN0eWxlOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgIC8vIERlZmF1bHQgZGF0YSBsYWJlbCBlbXBoYXNpcyBgc2hvd2BcbiAgICAvLyBGSVhNRSBUcmVlIHN0cnVjdHVyZSBkYXRhID9cbiAgICAvLyBGSVhNRSBQZXJmb3JtYW5jZSA/XG4gICAgaWYgKGRhdGEgJiYgIXpyVXRpbC5pc1R5cGVkQXJyYXkoZGF0YSkpIHtcbiAgICAgIHZhciBwcm9wcyA9IFsnc2hvdyddO1xuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGRhdGFbaV0gJiYgZGF0YVtpXS5sYWJlbCkge1xuICAgICAgICAgIG1vZGVsVXRpbC5kZWZhdWx0RW1waGFzaXMoZGF0YVtpXSwgJ2xhYmVsJywgcHJvcHMpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiBJbml0IGEgZGF0YSBzdHJ1Y3R1cmUgZnJvbSBkYXRhIHJlbGF0ZWQgb3B0aW9uIGluIHNlcmllc1xuICAgKiBNdXN0IGJlIG92ZXJ3cml0dGVuXG4gICAqL1xuICBnZXRJbml0aWFsRGF0YTogZnVuY3Rpb24gKCkge30sXG5cbiAgLyoqXG4gICAqIEFwcGVuZCBkYXRhIHRvIGxpc3RcbiAgICogQHBhcmFtIHtPYmplY3R9IHBhcmFtc1xuICAgKiBAcGFyYW0ge0FycmF5fFR5cGVkQXJyYXl9IHBhcmFtcy5kYXRhXG4gICAqL1xuICBhcHBlbmREYXRhOiBmdW5jdGlvbiAocGFyYW1zKSB7XG4gICAgLy8gRklYTUUgPz8/XG4gICAgLy8gKDEpIElmIGRhdGEgZnJvbSBkYXRhc2V0LCBmb3JiaWRkZW4gYXBwZW5kLlxuICAgIC8vICgyKSBzdXBwb3J0IGFwcGVuZCBkYXRhIG9mIGRhdGFzZXQuXG4gICAgdmFyIGRhdGEgPSB0aGlzLmdldFJhd0RhdGEoKTtcbiAgICBkYXRhLmFwcGVuZERhdGEocGFyYW1zLmRhdGEpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBDb25zaWRlciBzb21lIG1ldGhvZCBsaWtlIGBmaWx0ZXJgLCBgbWFwYCBuZWVkIG1ha2UgbmV3IGRhdGEsXG4gICAqIFdlIHNob3VsZCBtYWtlIHN1cmUgdGhhdCBgc2VyaWVzTW9kZWwuZ2V0RGF0YSgpYCBnZXQgY29ycmVjdFxuICAgKiBkYXRhIGluIHRoZSBzdHJlYW0gcHJvY2VkdXJlLiBTbyB3ZSBmZXRjaCBkYXRhIGZyb20gdXBzdHJlYW1cbiAgICogZWFjaCB0aW1lIGB0YXNrLnBlcmZvcm1gIGNhbGxlZC5cbiAgICogQHBhcmFtIHtzdHJpbmd9IFtkYXRhVHlwZV1cbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvZGF0YS9MaXN0fVxuICAgKi9cbiAgZ2V0RGF0YTogZnVuY3Rpb24gKGRhdGFUeXBlKSB7XG4gICAgdmFyIHRhc2sgPSBnZXRDdXJyZW50VGFzayh0aGlzKTtcblxuICAgIGlmICh0YXNrKSB7XG4gICAgICB2YXIgZGF0YSA9IHRhc2suY29udGV4dC5kYXRhO1xuICAgICAgcmV0dXJuIGRhdGFUeXBlID09IG51bGwgPyBkYXRhIDogZGF0YS5nZXRMaW5rZWREYXRhKGRhdGFUeXBlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gV2hlbiBzZXJpZXMgaXMgbm90IGFsaXZlICh0aGF0IG1heSBoYXBwZW4gd2hlbiBjbGljayB0b29sYm94XG4gICAgICAvLyByZXN0b3JlIG9yIHNldE9wdGlvbiB3aXRoIG5vdCBtZXJnZSBtb2RlKSwgc2VyaWVzIGRhdGEgbWF5XG4gICAgICAvLyBiZSBzdGlsbCBuZWVkIHRvIGp1ZGdlIGFuaW1hdGlvbiBvciBzb21ldGhpbmcgd2hlbiBncmFwaGljXG4gICAgICAvLyBlbGVtZW50cyB3YW50IHRvIGtub3cgd2hldGhlciBmYWRlIG91dC5cbiAgICAgIHJldHVybiBpbm5lcih0aGlzKS5kYXRhO1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9IGRhdGFcbiAgICovXG4gIHNldERhdGE6IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgdmFyIHRhc2sgPSBnZXRDdXJyZW50VGFzayh0aGlzKTtcblxuICAgIGlmICh0YXNrKSB7XG4gICAgICB2YXIgY29udGV4dCA9IHRhc2suY29udGV4dDsgLy8gQ29uc2lkZXIgY2FzZTogZmlsdGVyLCBkYXRhIHNhbXBsZS5cblxuICAgICAgaWYgKGNvbnRleHQuZGF0YSAhPT0gZGF0YSAmJiB0YXNrLm1vZGlmeU91dHB1dEVuZCkge1xuICAgICAgICB0YXNrLnNldE91dHB1dEVuZChkYXRhLmNvdW50KCkpO1xuICAgICAgfVxuXG4gICAgICBjb250ZXh0Lm91dHB1dERhdGEgPSBkYXRhOyAvLyBDYXV0aW9uOiBzZXREYXRhIHNob3VsZCB1cGRhdGUgY29udGV4dC5kYXRhLFxuICAgICAgLy8gQmVjYXVzZSBnZXREYXRhIG1heSBiZSBjYWxsZWQgbXVsdGlwbHkgaW4gYVxuICAgICAgLy8gc2luZ2xlIHN0YWdlIGFuZCBleHBlY3QgdG8gZ2V0IHRoZSBkYXRhIGp1c3RcbiAgICAgIC8vIHNldC4gKEZvciBleGFtcGxlLCBBeGlzUHJveHksIHggeSBib3RoIGNhbGxcbiAgICAgIC8vIGdldERhdGEgYW5kIHNldERhdGUgc2VxdWVudGlhbGx5KS5cbiAgICAgIC8vIFNvIHRoZSBjb250ZXh0LmRhdGEgc2hvdWxkIGJlIGZldGNoZWQgZnJvbVxuICAgICAgLy8gdXBzdHJlYW0gZWFjaCB0aW1lIHdoZW4gYSBzdGFnZSBzdGFydHMgdG8gYmVcbiAgICAgIC8vIHBlcmZvcm1lZC5cblxuICAgICAgaWYgKHRhc2sgIT09IHRoaXMuZGF0YVRhc2spIHtcbiAgICAgICAgY29udGV4dC5kYXRhID0gZGF0YTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpbm5lcih0aGlzKS5kYXRhID0gZGF0YTtcbiAgfSxcblxuICAvKipcbiAgICogQHNlZSB7bW9kdWxlOmVjaGFydHMvZGF0YS9oZWxwZXIvc291cmNlSGVscGVyI2dldFNvdXJjZX1cbiAgICogQHJldHVybiB7bW9kdWxlOmVjaGFydHMvZGF0YS9Tb3VyY2V9IHNvdXJjZVxuICAgKi9cbiAgZ2V0U291cmNlOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGdldFNvdXJjZSh0aGlzKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IGRhdGEgYmVmb3JlIHByb2Nlc3NlZFxuICAgKiBAcmV0dXJuIHttb2R1bGU6ZWNoYXJ0cy9kYXRhL0xpc3R9XG4gICAqL1xuICBnZXRSYXdEYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGlubmVyKHRoaXMpLmRhdGFCZWZvcmVQcm9jZXNzZWQ7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBiYXNlIGF4aXMgaWYgaGFzIGNvb3JkaW5hdGUgc3lzdGVtIGFuZCBoYXMgYXhpcy5cbiAgICogQnkgZGVmYXVsdCB1c2UgY29vcmRTeXMuZ2V0QmFzZUF4aXMoKTtcbiAgICogQ2FuIGJlIG92ZXJyaWRlZCBmb3Igc29tZSBjaGFydC5cbiAgICogQHJldHVybiB7dHlwZX0gZGVzY3JpcHRpb25cbiAgICovXG4gIGdldEJhc2VBeGlzOiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNvb3JkU3lzID0gdGhpcy5jb29yZGluYXRlU3lzdGVtO1xuICAgIHJldHVybiBjb29yZFN5cyAmJiBjb29yZFN5cy5nZXRCYXNlQXhpcyAmJiBjb29yZFN5cy5nZXRCYXNlQXhpcygpO1xuICB9LFxuICAvLyBGSVhNRVxuXG4gIC8qKlxuICAgKiBEZWZhdWx0IHRvb2x0aXAgZm9ybWF0dGVyXG4gICAqXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBkYXRhSW5kZXhcbiAgICogQHBhcmFtIHtib29sZWFufSBbbXVsdGlwbGVTZXJpZXM9ZmFsc2VdXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbZGF0YVR5cGVdXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbcmVuZGVyTW9kZT0naHRtbCddIHZhbGlkIHZhbHVlczogJ2h0bWwnIGFuZCAncmljaFRleHQnLlxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaHRtbCcgaXMgdXNlZCBmb3IgcmVuZGVyaW5nIHRvb2x0aXAgaW4gZXh0cmEgRE9NIGZvcm0sIGFuZCB0aGUgcmVzdWx0XG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0cmluZyBpcyB1c2VkIGFzIERPTSBIVE1MIGNvbnRlbnQuXG4gICAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdyaWNoVGV4dCcgaXMgdXNlZCBmb3IgcmVuZGVyaW5nIHRvb2x0aXAgaW4gcmljaCB0ZXh0IGZvcm0sIGZvciB0aG9zZSB3aGVyZVxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBET00gb3BlcmF0aW9uIGlzIG5vdCBzdXBwb3J0ZWQuXG4gICAqIEByZXR1cm4ge09iamVjdH0gZm9ybWF0dGVkIHRvb2x0aXAgd2l0aCBgaHRtbGAgYW5kIGBtYXJrZXJzYFxuICAgKi9cbiAgZm9ybWF0VG9vbHRpcDogZnVuY3Rpb24gKGRhdGFJbmRleCwgbXVsdGlwbGVTZXJpZXMsIGRhdGFUeXBlLCByZW5kZXJNb2RlKSB7XG4gICAgdmFyIHNlcmllcyA9IHRoaXM7XG4gICAgcmVuZGVyTW9kZSA9IHJlbmRlck1vZGUgfHwgJ2h0bWwnO1xuICAgIHZhciBuZXdMaW5lID0gcmVuZGVyTW9kZSA9PT0gJ2h0bWwnID8gJzxici8+JyA6ICdcXG4nO1xuICAgIHZhciBpc1JpY2hUZXh0ID0gcmVuZGVyTW9kZSA9PT0gJ3JpY2hUZXh0JztcbiAgICB2YXIgbWFya2VycyA9IHt9O1xuICAgIHZhciBtYXJrZXJJZCA9IDA7XG5cbiAgICBmdW5jdGlvbiBmb3JtYXRBcnJheVZhbHVlKHZhbHVlKSB7XG4gICAgICAvLyA/Pz8gVE9ETyByZWZhY3RvciB0aGVzZSBsb2dpYy5cbiAgICAgIC8vIGNoZWNrOiBjYXRlZ29yeS1uby1lbmNvZGUtaGFzLWF4aXMtZGF0YSBpbiBkYXRhc2V0Lmh0bWxcbiAgICAgIHZhciB2ZXJ0aWFsbHkgPSB6clV0aWwucmVkdWNlKHZhbHVlLCBmdW5jdGlvbiAodmVydGlhbGx5LCB2YWwsIGlkeCkge1xuICAgICAgICB2YXIgZGltSXRlbSA9IGRhdGEuZ2V0RGltZW5zaW9uSW5mbyhpZHgpO1xuICAgICAgICByZXR1cm4gdmVydGlhbGx5IHw9IGRpbUl0ZW0gJiYgZGltSXRlbS50b29sdGlwICE9PSBmYWxzZSAmJiBkaW1JdGVtLmRpc3BsYXlOYW1lICE9IG51bGw7XG4gICAgICB9LCAwKTtcbiAgICAgIHZhciByZXN1bHQgPSBbXTtcbiAgICAgIHRvb2x0aXBEaW1zLmxlbmd0aCA/IHpyVXRpbC5lYWNoKHRvb2x0aXBEaW1zLCBmdW5jdGlvbiAoZGltKSB7XG4gICAgICAgIHNldEVhY2hJdGVtKHJldHJpZXZlUmF3VmFsdWUoZGF0YSwgZGF0YUluZGV4LCBkaW0pLCBkaW0pO1xuICAgICAgfSkgLy8gQnkgZGVmYXVsdCwgYWxsIGRpbXMgaXMgdXNlZCBvbiB0b29sdGlwLlxuICAgICAgOiB6clV0aWwuZWFjaCh2YWx1ZSwgc2V0RWFjaEl0ZW0pO1xuXG4gICAgICBmdW5jdGlvbiBzZXRFYWNoSXRlbSh2YWwsIGRpbSkge1xuICAgICAgICB2YXIgZGltSW5mbyA9IGRhdGEuZ2V0RGltZW5zaW9uSW5mbyhkaW0pOyAvLyBJZiBgZGltSW5mby50b29sdGlwYCBpcyBub3Qgc2V0LCBzaG93IHRvb2x0aXAuXG5cbiAgICAgICAgaWYgKCFkaW1JbmZvIHx8IGRpbUluZm8ub3RoZXJEaW1zLnRvb2x0aXAgPT09IGZhbHNlKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGRpbVR5cGUgPSBkaW1JbmZvLnR5cGU7XG4gICAgICAgIHZhciBtYXJrTmFtZSA9ICdzdWInICsgc2VyaWVzLnNlcmllc0luZGV4ICsgJ2F0JyArIG1hcmtlcklkO1xuICAgICAgICB2YXIgZGltSGVhZCA9IGdldFRvb2x0aXBNYXJrZXIoe1xuICAgICAgICAgIGNvbG9yOiBjb2xvcixcbiAgICAgICAgICB0eXBlOiAnc3ViSXRlbScsXG4gICAgICAgICAgcmVuZGVyTW9kZTogcmVuZGVyTW9kZSxcbiAgICAgICAgICBtYXJrZXJJZDogbWFya05hbWVcbiAgICAgICAgfSk7XG4gICAgICAgIHZhciBkaW1IZWFkU3RyID0gdHlwZW9mIGRpbUhlYWQgPT09ICdzdHJpbmcnID8gZGltSGVhZCA6IGRpbUhlYWQuY29udGVudDtcbiAgICAgICAgdmFyIHZhbFN0ciA9ICh2ZXJ0aWFsbHkgPyBkaW1IZWFkU3RyICsgZW5jb2RlSFRNTChkaW1JbmZvLmRpc3BsYXlOYW1lIHx8ICctJykgKyAnOiAnIDogJycpICsgLy8gRklYTUUgc2hvdWxkIG5vdCBmb3JtYXQgdGltZSBmb3IgcmF3IGRhdGE/XG4gICAgICAgIGVuY29kZUhUTUwoZGltVHlwZSA9PT0gJ29yZGluYWwnID8gdmFsICsgJycgOiBkaW1UeXBlID09PSAndGltZScgPyBtdWx0aXBsZVNlcmllcyA/ICcnIDogZm9ybWF0VGltZSgneXl5eS9NTS9kZCBoaDptbTpzcycsIHZhbCkgOiBhZGRDb21tYXModmFsKSk7XG4gICAgICAgIHZhbFN0ciAmJiByZXN1bHQucHVzaCh2YWxTdHIpO1xuXG4gICAgICAgIGlmIChpc1JpY2hUZXh0KSB7XG4gICAgICAgICAgbWFya2Vyc1ttYXJrTmFtZV0gPSBjb2xvcjtcbiAgICAgICAgICArK21hcmtlcklkO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHZhciBuZXdMaW5lID0gdmVydGlhbGx5ID8gaXNSaWNoVGV4dCA/ICdcXG4nIDogJzxici8+JyA6ICcnO1xuICAgICAgdmFyIGNvbnRlbnQgPSBuZXdMaW5lICsgcmVzdWx0LmpvaW4obmV3TGluZSB8fCAnLCAnKTtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHJlbmRlck1vZGU6IHJlbmRlck1vZGUsXG4gICAgICAgIGNvbnRlbnQ6IGNvbnRlbnQsXG4gICAgICAgIHN0eWxlOiBtYXJrZXJzXG4gICAgICB9O1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGZvcm1hdFNpbmdsZVZhbHVlKHZhbCkge1xuICAgICAgLy8gcmV0dXJuIGVuY29kZUhUTUwoYWRkQ29tbWFzKHZhbCkpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgcmVuZGVyTW9kZTogcmVuZGVyTW9kZSxcbiAgICAgICAgY29udGVudDogZW5jb2RlSFRNTChhZGRDb21tYXModmFsKSksXG4gICAgICAgIHN0eWxlOiBtYXJrZXJzXG4gICAgICB9O1xuICAgIH1cblxuICAgIHZhciBkYXRhID0gdGhpcy5nZXREYXRhKCk7XG4gICAgdmFyIHRvb2x0aXBEaW1zID0gZGF0YS5tYXBEaW1lbnNpb24oJ2RlZmF1bHRlZFRvb2x0aXAnLCB0cnVlKTtcbiAgICB2YXIgdG9vbHRpcERpbUxlbiA9IHRvb2x0aXBEaW1zLmxlbmd0aDtcbiAgICB2YXIgdmFsdWUgPSB0aGlzLmdldFJhd1ZhbHVlKGRhdGFJbmRleCk7XG4gICAgdmFyIGlzVmFsdWVBcnIgPSB6clV0aWwuaXNBcnJheSh2YWx1ZSk7XG4gICAgdmFyIGNvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGRhdGFJbmRleCwgJ2NvbG9yJyk7XG5cbiAgICBpZiAoenJVdGlsLmlzT2JqZWN0KGNvbG9yKSAmJiBjb2xvci5jb2xvclN0b3BzKSB7XG4gICAgICBjb2xvciA9IChjb2xvci5jb2xvclN0b3BzWzBdIHx8IHt9KS5jb2xvcjtcbiAgICB9XG5cbiAgICBjb2xvciA9IGNvbG9yIHx8ICd0cmFuc3BhcmVudCc7IC8vIENvbXBsaWNhdGVkIHJ1bGUgZm9yIHByZXR0eSB0b29sdGlwLlxuXG4gICAgdmFyIGZvcm1hdHRlZFZhbHVlID0gdG9vbHRpcERpbUxlbiA+IDEgfHwgaXNWYWx1ZUFyciAmJiAhdG9vbHRpcERpbUxlbiA/IGZvcm1hdEFycmF5VmFsdWUodmFsdWUpIDogdG9vbHRpcERpbUxlbiA/IGZvcm1hdFNpbmdsZVZhbHVlKHJldHJpZXZlUmF3VmFsdWUoZGF0YSwgZGF0YUluZGV4LCB0b29sdGlwRGltc1swXSkpIDogZm9ybWF0U2luZ2xlVmFsdWUoaXNWYWx1ZUFyciA/IHZhbHVlWzBdIDogdmFsdWUpO1xuICAgIHZhciBjb250ZW50ID0gZm9ybWF0dGVkVmFsdWUuY29udGVudDtcbiAgICB2YXIgbWFya05hbWUgPSBzZXJpZXMuc2VyaWVzSW5kZXggKyAnYXQnICsgbWFya2VySWQ7XG4gICAgdmFyIGNvbG9yRWwgPSBnZXRUb29sdGlwTWFya2VyKHtcbiAgICAgIGNvbG9yOiBjb2xvcixcbiAgICAgIHR5cGU6ICdpdGVtJyxcbiAgICAgIHJlbmRlck1vZGU6IHJlbmRlck1vZGUsXG4gICAgICBtYXJrZXJJZDogbWFya05hbWVcbiAgICB9KTtcbiAgICBtYXJrZXJzW21hcmtOYW1lXSA9IGNvbG9yO1xuICAgICsrbWFya2VySWQ7XG4gICAgdmFyIG5hbWUgPSBkYXRhLmdldE5hbWUoZGF0YUluZGV4KTtcbiAgICB2YXIgc2VyaWVzTmFtZSA9IHRoaXMubmFtZTtcblxuICAgIGlmICghbW9kZWxVdGlsLmlzTmFtZVNwZWNpZmllZCh0aGlzKSkge1xuICAgICAgc2VyaWVzTmFtZSA9ICcnO1xuICAgIH1cblxuICAgIHNlcmllc05hbWUgPSBzZXJpZXNOYW1lID8gZW5jb2RlSFRNTChzZXJpZXNOYW1lKSArICghbXVsdGlwbGVTZXJpZXMgPyBuZXdMaW5lIDogJzogJykgOiAnJztcbiAgICB2YXIgY29sb3JTdHIgPSB0eXBlb2YgY29sb3JFbCA9PT0gJ3N0cmluZycgPyBjb2xvckVsIDogY29sb3JFbC5jb250ZW50O1xuICAgIHZhciBodG1sID0gIW11bHRpcGxlU2VyaWVzID8gc2VyaWVzTmFtZSArIGNvbG9yU3RyICsgKG5hbWUgPyBlbmNvZGVIVE1MKG5hbWUpICsgJzogJyArIGNvbnRlbnQgOiBjb250ZW50KSA6IGNvbG9yU3RyICsgc2VyaWVzTmFtZSArIGNvbnRlbnQ7XG4gICAgcmV0dXJuIHtcbiAgICAgIGh0bWw6IGh0bWwsXG4gICAgICBtYXJrZXJzOiBtYXJrZXJzXG4gICAgfTtcbiAgfSxcblxuICAvKipcbiAgICogQHJldHVybiB7Ym9vbGVhbn1cbiAgICovXG4gIGlzQW5pbWF0aW9uRW5hYmxlZDogZnVuY3Rpb24gKCkge1xuICAgIGlmIChlbnYubm9kZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHZhciBhbmltYXRpb25FbmFibGVkID0gdGhpcy5nZXRTaGFsbG93KCdhbmltYXRpb24nKTtcblxuICAgIGlmIChhbmltYXRpb25FbmFibGVkKSB7XG4gICAgICBpZiAodGhpcy5nZXREYXRhKCkuY291bnQoKSA+IHRoaXMuZ2V0U2hhbGxvdygnYW5pbWF0aW9uVGhyZXNob2xkJykpIHtcbiAgICAgICAgYW5pbWF0aW9uRW5hYmxlZCA9IGZhbHNlO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBhbmltYXRpb25FbmFibGVkO1xuICB9LFxuICByZXN0b3JlRGF0YTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuZGF0YVRhc2suZGlydHkoKTtcbiAgfSxcbiAgZ2V0Q29sb3JGcm9tUGFsZXR0ZTogZnVuY3Rpb24gKG5hbWUsIHNjb3BlLCByZXF1ZXN0Q29sb3JOdW0pIHtcbiAgICB2YXIgZWNNb2RlbCA9IHRoaXMuZWNNb2RlbDsgLy8gUEVORElOR1xuXG4gICAgdmFyIGNvbG9yID0gY29sb3JQYWxldHRlTWl4aW4uZ2V0Q29sb3JGcm9tUGFsZXR0ZS5jYWxsKHRoaXMsIG5hbWUsIHNjb3BlLCByZXF1ZXN0Q29sb3JOdW0pO1xuXG4gICAgaWYgKCFjb2xvcikge1xuICAgICAgY29sb3IgPSBlY01vZGVsLmdldENvbG9yRnJvbVBhbGV0dGUobmFtZSwgc2NvcGUsIHJlcXVlc3RDb2xvck51bSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGNvbG9yO1xuICB9LFxuXG4gIC8qKlxuICAgKiBVc2UgYGRhdGEubWFwRGltZW5zaW9uKGNvb3JkRGltLCB0cnVlKWAgaW5zdGVhZC5cbiAgICogQGRlcHJlY2F0ZWRcbiAgICovXG4gIGNvb3JkRGltVG9EYXRhRGltOiBmdW5jdGlvbiAoY29vcmREaW0pIHtcbiAgICByZXR1cm4gdGhpcy5nZXRSYXdEYXRhKCkubWFwRGltZW5zaW9uKGNvb3JkRGltLCB0cnVlKTtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IHByb2dyZXNzaXZlIHJlbmRlcmluZyBjb3VudCBlYWNoIHN0ZXBcbiAgICogQHJldHVybiB7bnVtYmVyfVxuICAgKi9cbiAgZ2V0UHJvZ3Jlc3NpdmU6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5nZXQoJ3Byb2dyZXNzaXZlJyk7XG4gIH0sXG5cbiAgLyoqXG4gICAqIEdldCBwcm9ncmVzc2l2ZSByZW5kZXJpbmcgY291bnQgZWFjaCBzdGVwXG4gICAqIEByZXR1cm4ge251bWJlcn1cbiAgICovXG4gIGdldFByb2dyZXNzaXZlVGhyZXNob2xkOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0KCdwcm9ncmVzc2l2ZVRocmVzaG9sZCcpO1xuICB9LFxuXG4gIC8qKlxuICAgKiBHZXQgZGF0YSBpbmRpY2VzIGZvciBzaG93IHRvb2x0aXAgY29udGVudC4gU2VlIHRvb2x0aXAuXG4gICAqIEBhYnN0cmFjdFxuICAgKiBAcGFyYW0ge0FycmF5LjxzdHJpbmc+fHN0cmluZ30gZGltXG4gICAqIEBwYXJhbSB7QXJyYXkuPG51bWJlcj59IHZhbHVlXG4gICAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvY29vcmQvc2luZ2xlL1NpbmdsZUF4aXN9IGJhc2VBeGlzXG4gICAqIEByZXR1cm4ge09iamVjdH0ge2RhdGFJbmRpY2VzLCBuZXN0ZXN0VmFsdWV9LlxuICAgKi9cbiAgZ2V0QXhpc1Rvb2x0aXBEYXRhOiBudWxsLFxuXG4gIC8qKlxuICAgKiBTZWUgdG9vbHRpcC5cbiAgICogQGFic3RyYWN0XG4gICAqIEBwYXJhbSB7bnVtYmVyfSBkYXRhSW5kZXhcbiAgICogQHJldHVybiB7QXJyYXkuPG51bWJlcj59IFBvaW50IG9mIHRvb2x0aXAuIG51bGwvdW5kZWZpbmVkIGNhbiBiZSByZXR1cm5lZC5cbiAgICovXG4gIGdldFRvb2x0aXBQb3NpdGlvbjogbnVsbCxcblxuICAvKipcbiAgICogQHNlZSB7bW9kdWxlOmVjaGFydHMvc3RyZWFtL1NjaGVkdWxlcn1cbiAgICovXG4gIHBpcGVUYXNrOiBudWxsLFxuXG4gIC8qKlxuICAgKiBDb252aW5pZW50IGZvciBvdmVycmlkZSBpbiBleHRlbmRlZCBjbGFzcy5cbiAgICogQHByb3RlY3RlZFxuICAgKiBAdHlwZSB7RnVuY3Rpb259XG4gICAqL1xuICBwcmV2ZW50SW5jcmVtZW50YWw6IG51bGwsXG5cbiAgLyoqXG4gICAqIEBwdWJsaWNcbiAgICogQHJlYWRPbmx5XG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICBwaXBlbGluZUNvbnRleHQ6IG51bGxcbn0pO1xuenJVdGlsLm1peGluKFNlcmllc01vZGVsLCBkYXRhRm9ybWF0TWl4aW4pO1xuenJVdGlsLm1peGluKFNlcmllc01vZGVsLCBjb2xvclBhbGV0dGVNaXhpbik7XG4vKipcbiAqIE1VU1QgYmUgY2FsbGVkIGFmdGVyIGBwcmVwYXJlU291cmNlYCBjYWxsZWRcbiAqIEhlcmUgd2UgbmVlZCB0byBtYWtlIGF1dG8gc2VyaWVzLCBlc3BlY2lhbGx5IGZvciBhdXRvIGxlZ2VuZC4gQnV0IHdlXG4gKiBkbyBub3QgbW9kaWZ5IHNlcmllcy5uYW1lIGluIG9wdGlvbiB0byBhdm9pZCBzaWRlIGVmZmVjdHMuXG4gKi9cblxuZnVuY3Rpb24gYXV0b1Nlcmllc05hbWUoc2VyaWVzTW9kZWwpIHtcbiAgLy8gVXNlciBzcGVjaWZpZWQgbmFtZSBoYXMgaGlnaGVyIHByaW9yaXR5LCBvdGhlcndpc2UgaXQgbWF5IGNhdXNlXG4gIC8vIHNlcmllcyBjYW4gbm90IGJlIHF1ZXJpZWQgdW5leHBlY3RlZGx5LlxuICB2YXIgbmFtZSA9IHNlcmllc01vZGVsLm5hbWU7XG5cbiAgaWYgKCFtb2RlbFV0aWwuaXNOYW1lU3BlY2lmaWVkKHNlcmllc01vZGVsKSkge1xuICAgIHNlcmllc01vZGVsLm5hbWUgPSBnZXRTZXJpZXNBdXRvTmFtZShzZXJpZXNNb2RlbCkgfHwgbmFtZTtcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRTZXJpZXNBdXRvTmFtZShzZXJpZXNNb2RlbCkge1xuICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldFJhd0RhdGEoKTtcbiAgdmFyIGRhdGFEaW1zID0gZGF0YS5tYXBEaW1lbnNpb24oJ3Nlcmllc05hbWUnLCB0cnVlKTtcbiAgdmFyIG5hbWVBcnIgPSBbXTtcbiAgenJVdGlsLmVhY2goZGF0YURpbXMsIGZ1bmN0aW9uIChkYXRhRGltKSB7XG4gICAgdmFyIGRpbUluZm8gPSBkYXRhLmdldERpbWVuc2lvbkluZm8oZGF0YURpbSk7XG4gICAgZGltSW5mby5kaXNwbGF5TmFtZSAmJiBuYW1lQXJyLnB1c2goZGltSW5mby5kaXNwbGF5TmFtZSk7XG4gIH0pO1xuICByZXR1cm4gbmFtZUFyci5qb2luKCcgJyk7XG59XG5cbmZ1bmN0aW9uIGRhdGFUYXNrQ291bnQoY29udGV4dCkge1xuICByZXR1cm4gY29udGV4dC5tb2RlbC5nZXRSYXdEYXRhKCkuY291bnQoKTtcbn1cblxuZnVuY3Rpb24gZGF0YVRhc2tSZXNldChjb250ZXh0KSB7XG4gIHZhciBzZXJpZXNNb2RlbCA9IGNvbnRleHQubW9kZWw7XG4gIHNlcmllc01vZGVsLnNldERhdGEoc2VyaWVzTW9kZWwuZ2V0UmF3RGF0YSgpLmNsb25lU2hhbGxvdygpKTtcbiAgcmV0dXJuIGRhdGFUYXNrUHJvZ3Jlc3M7XG59XG5cbmZ1bmN0aW9uIGRhdGFUYXNrUHJvZ3Jlc3MocGFyYW0sIGNvbnRleHQpIHtcbiAgLy8gQXZvaWQgcmVwZWFkIGNsb25lU2hhbGxvdyB3aGVuIGRhdGEganVzdCBjcmVhdGVkIGluIHJlc2V0LlxuICBpZiAocGFyYW0uZW5kID4gY29udGV4dC5vdXRwdXREYXRhLmNvdW50KCkpIHtcbiAgICBjb250ZXh0Lm1vZGVsLmdldFJhd0RhdGEoKS5jbG9uZVNoYWxsb3coY29udGV4dC5vdXRwdXREYXRhKTtcbiAgfVxufSAvLyBUT0RPIHJlZmFjdG9yXG5cblxuZnVuY3Rpb24gd3JhcERhdGEoZGF0YSwgc2VyaWVzTW9kZWwpIHtcbiAgenJVdGlsLmVhY2goZGF0YS5DSEFOR0FCTEVfTUVUSE9EUywgZnVuY3Rpb24gKG1ldGhvZE5hbWUpIHtcbiAgICBkYXRhLndyYXBNZXRob2QobWV0aG9kTmFtZSwgenJVdGlsLmN1cnJ5KG9uRGF0YVNlbGZDaGFuZ2UsIHNlcmllc01vZGVsKSk7XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBvbkRhdGFTZWxmQ2hhbmdlKHNlcmllc01vZGVsKSB7XG4gIHZhciB0YXNrID0gZ2V0Q3VycmVudFRhc2soc2VyaWVzTW9kZWwpO1xuXG4gIGlmICh0YXNrKSB7XG4gICAgLy8gQ29uc2lkZXIgY2FzZTogZmlsdGVyLCBzZWxlY3RSYW5nZVxuICAgIHRhc2suc2V0T3V0cHV0RW5kKHRoaXMuY291bnQoKSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gZ2V0Q3VycmVudFRhc2soc2VyaWVzTW9kZWwpIHtcbiAgdmFyIHNjaGVkdWxlciA9IChzZXJpZXNNb2RlbC5lY01vZGVsIHx8IHt9KS5zY2hlZHVsZXI7XG4gIHZhciBwaXBlbGluZSA9IHNjaGVkdWxlciAmJiBzY2hlZHVsZXIuZ2V0UGlwZWxpbmUoc2VyaWVzTW9kZWwudWlkKTtcblxuICBpZiAocGlwZWxpbmUpIHtcbiAgICAvLyBXaGVuIHBpcGxpbmUgZmluaXNoZWQsIHRoZSBjdXJycmVudFRhc2sga2VlcCB0aGUgbGFzdFxuICAgIC8vIHRhc2sgKHJlbmRlclRhc2spLlxuICAgIHZhciB0YXNrID0gcGlwZWxpbmUuY3VycmVudFRhc2s7XG5cbiAgICBpZiAodGFzaykge1xuICAgICAgdmFyIGFnZW50U3R1Yk1hcCA9IHRhc2suYWdlbnRTdHViTWFwO1xuXG4gICAgICBpZiAoYWdlbnRTdHViTWFwKSB7XG4gICAgICAgIHRhc2sgPSBhZ2VudFN0dWJNYXAuZ2V0KHNlcmllc01vZGVsLnVpZCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRhc2s7XG4gIH1cbn1cblxudmFyIF9kZWZhdWx0ID0gU2VyaWVzTW9kZWw7XG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7OztBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQTNiQTtBQTZiQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/model/Series.js
