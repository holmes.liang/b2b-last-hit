/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var Region = __webpack_require__(/*! ./Region */ "./node_modules/echarts/lib/coord/geo/Region.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * Parse and decode geo json
 * @module echarts/coord/geo/parseGeoJson
 */


function decode(json) {
  if (!json.UTF8Encoding) {
    return json;
  }

  var encodeScale = json.UTF8Scale;

  if (encodeScale == null) {
    encodeScale = 1024;
  }

  var features = json.features;

  for (var f = 0; f < features.length; f++) {
    var feature = features[f];
    var geometry = feature.geometry;
    var coordinates = geometry.coordinates;
    var encodeOffsets = geometry.encodeOffsets;

    for (var c = 0; c < coordinates.length; c++) {
      var coordinate = coordinates[c];

      if (geometry.type === 'Polygon') {
        coordinates[c] = decodePolygon(coordinate, encodeOffsets[c], encodeScale);
      } else if (geometry.type === 'MultiPolygon') {
        for (var c2 = 0; c2 < coordinate.length; c2++) {
          var polygon = coordinate[c2];
          coordinate[c2] = decodePolygon(polygon, encodeOffsets[c][c2], encodeScale);
        }
      }
    }
  } // Has been decoded


  json.UTF8Encoding = false;
  return json;
}

function decodePolygon(coordinate, encodeOffsets, encodeScale) {
  var result = [];
  var prevX = encodeOffsets[0];
  var prevY = encodeOffsets[1];

  for (var i = 0; i < coordinate.length; i += 2) {
    var x = coordinate.charCodeAt(i) - 64;
    var y = coordinate.charCodeAt(i + 1) - 64; // ZigZag decoding

    x = x >> 1 ^ -(x & 1);
    y = y >> 1 ^ -(y & 1); // Delta deocding

    x += prevX;
    y += prevY;
    prevX = x;
    prevY = y; // Dequantize

    result.push([x / encodeScale, y / encodeScale]);
  }

  return result;
}
/**
 * @alias module:echarts/coord/geo/parseGeoJson
 * @param {Object} geoJson
 * @return {module:zrender/container/Group}
 */


function _default(geoJson) {
  decode(geoJson);
  return zrUtil.map(zrUtil.filter(geoJson.features, function (featureObj) {
    // Output of mapshaper may have geometry null
    return featureObj.geometry && featureObj.properties && featureObj.geometry.coordinates.length > 0;
  }), function (featureObj) {
    var properties = featureObj.properties;
    var geo = featureObj.geometry;
    var coordinates = geo.coordinates;
    var geometries = [];

    if (geo.type === 'Polygon') {
      geometries.push({
        type: 'polygon',
        // According to the GeoJSON specification.
        // First must be exterior, and the rest are all interior(holes).
        exterior: coordinates[0],
        interiors: coordinates.slice(1)
      });
    }

    if (geo.type === 'MultiPolygon') {
      zrUtil.each(coordinates, function (item) {
        if (item[0]) {
          geometries.push({
            type: 'polygon',
            exterior: item[0],
            interiors: item.slice(1)
          });
        }
      });
    }

    var region = new Region(properties.name, geometries, properties.cp);
    region.properties = properties;
    return region;
  });
}

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY29vcmQvZ2VvL3BhcnNlR2VvSnNvbi5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL2Nvb3JkL2dlby9wYXJzZUdlb0pzb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciB6clV0aWwgPSByZXF1aXJlKFwienJlbmRlci9saWIvY29yZS91dGlsXCIpO1xuXG52YXIgUmVnaW9uID0gcmVxdWlyZShcIi4vUmVnaW9uXCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogUGFyc2UgYW5kIGRlY29kZSBnZW8ganNvblxuICogQG1vZHVsZSBlY2hhcnRzL2Nvb3JkL2dlby9wYXJzZUdlb0pzb25cbiAqL1xuZnVuY3Rpb24gZGVjb2RlKGpzb24pIHtcbiAgaWYgKCFqc29uLlVURjhFbmNvZGluZykge1xuICAgIHJldHVybiBqc29uO1xuICB9XG5cbiAgdmFyIGVuY29kZVNjYWxlID0ganNvbi5VVEY4U2NhbGU7XG5cbiAgaWYgKGVuY29kZVNjYWxlID09IG51bGwpIHtcbiAgICBlbmNvZGVTY2FsZSA9IDEwMjQ7XG4gIH1cblxuICB2YXIgZmVhdHVyZXMgPSBqc29uLmZlYXR1cmVzO1xuXG4gIGZvciAodmFyIGYgPSAwOyBmIDwgZmVhdHVyZXMubGVuZ3RoOyBmKyspIHtcbiAgICB2YXIgZmVhdHVyZSA9IGZlYXR1cmVzW2ZdO1xuICAgIHZhciBnZW9tZXRyeSA9IGZlYXR1cmUuZ2VvbWV0cnk7XG4gICAgdmFyIGNvb3JkaW5hdGVzID0gZ2VvbWV0cnkuY29vcmRpbmF0ZXM7XG4gICAgdmFyIGVuY29kZU9mZnNldHMgPSBnZW9tZXRyeS5lbmNvZGVPZmZzZXRzO1xuXG4gICAgZm9yICh2YXIgYyA9IDA7IGMgPCBjb29yZGluYXRlcy5sZW5ndGg7IGMrKykge1xuICAgICAgdmFyIGNvb3JkaW5hdGUgPSBjb29yZGluYXRlc1tjXTtcblxuICAgICAgaWYgKGdlb21ldHJ5LnR5cGUgPT09ICdQb2x5Z29uJykge1xuICAgICAgICBjb29yZGluYXRlc1tjXSA9IGRlY29kZVBvbHlnb24oY29vcmRpbmF0ZSwgZW5jb2RlT2Zmc2V0c1tjXSwgZW5jb2RlU2NhbGUpO1xuICAgICAgfSBlbHNlIGlmIChnZW9tZXRyeS50eXBlID09PSAnTXVsdGlQb2x5Z29uJykge1xuICAgICAgICBmb3IgKHZhciBjMiA9IDA7IGMyIDwgY29vcmRpbmF0ZS5sZW5ndGg7IGMyKyspIHtcbiAgICAgICAgICB2YXIgcG9seWdvbiA9IGNvb3JkaW5hdGVbYzJdO1xuICAgICAgICAgIGNvb3JkaW5hdGVbYzJdID0gZGVjb2RlUG9seWdvbihwb2x5Z29uLCBlbmNvZGVPZmZzZXRzW2NdW2MyXSwgZW5jb2RlU2NhbGUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9IC8vIEhhcyBiZWVuIGRlY29kZWRcblxuXG4gIGpzb24uVVRGOEVuY29kaW5nID0gZmFsc2U7XG4gIHJldHVybiBqc29uO1xufVxuXG5mdW5jdGlvbiBkZWNvZGVQb2x5Z29uKGNvb3JkaW5hdGUsIGVuY29kZU9mZnNldHMsIGVuY29kZVNjYWxlKSB7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgdmFyIHByZXZYID0gZW5jb2RlT2Zmc2V0c1swXTtcbiAgdmFyIHByZXZZID0gZW5jb2RlT2Zmc2V0c1sxXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGNvb3JkaW5hdGUubGVuZ3RoOyBpICs9IDIpIHtcbiAgICB2YXIgeCA9IGNvb3JkaW5hdGUuY2hhckNvZGVBdChpKSAtIDY0O1xuICAgIHZhciB5ID0gY29vcmRpbmF0ZS5jaGFyQ29kZUF0KGkgKyAxKSAtIDY0OyAvLyBaaWdaYWcgZGVjb2RpbmdcblxuICAgIHggPSB4ID4+IDEgXiAtKHggJiAxKTtcbiAgICB5ID0geSA+PiAxIF4gLSh5ICYgMSk7IC8vIERlbHRhIGRlb2NkaW5nXG5cbiAgICB4ICs9IHByZXZYO1xuICAgIHkgKz0gcHJldlk7XG4gICAgcHJldlggPSB4O1xuICAgIHByZXZZID0geTsgLy8gRGVxdWFudGl6ZVxuXG4gICAgcmVzdWx0LnB1c2goW3ggLyBlbmNvZGVTY2FsZSwgeSAvIGVuY29kZVNjYWxlXSk7XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufVxuLyoqXG4gKiBAYWxpYXMgbW9kdWxlOmVjaGFydHMvY29vcmQvZ2VvL3BhcnNlR2VvSnNvblxuICogQHBhcmFtIHtPYmplY3R9IGdlb0pzb25cbiAqIEByZXR1cm4ge21vZHVsZTp6cmVuZGVyL2NvbnRhaW5lci9Hcm91cH1cbiAqL1xuXG5cbmZ1bmN0aW9uIF9kZWZhdWx0KGdlb0pzb24pIHtcbiAgZGVjb2RlKGdlb0pzb24pO1xuICByZXR1cm4genJVdGlsLm1hcCh6clV0aWwuZmlsdGVyKGdlb0pzb24uZmVhdHVyZXMsIGZ1bmN0aW9uIChmZWF0dXJlT2JqKSB7XG4gICAgLy8gT3V0cHV0IG9mIG1hcHNoYXBlciBtYXkgaGF2ZSBnZW9tZXRyeSBudWxsXG4gICAgcmV0dXJuIGZlYXR1cmVPYmouZ2VvbWV0cnkgJiYgZmVhdHVyZU9iai5wcm9wZXJ0aWVzICYmIGZlYXR1cmVPYmouZ2VvbWV0cnkuY29vcmRpbmF0ZXMubGVuZ3RoID4gMDtcbiAgfSksIGZ1bmN0aW9uIChmZWF0dXJlT2JqKSB7XG4gICAgdmFyIHByb3BlcnRpZXMgPSBmZWF0dXJlT2JqLnByb3BlcnRpZXM7XG4gICAgdmFyIGdlbyA9IGZlYXR1cmVPYmouZ2VvbWV0cnk7XG4gICAgdmFyIGNvb3JkaW5hdGVzID0gZ2VvLmNvb3JkaW5hdGVzO1xuICAgIHZhciBnZW9tZXRyaWVzID0gW107XG5cbiAgICBpZiAoZ2VvLnR5cGUgPT09ICdQb2x5Z29uJykge1xuICAgICAgZ2VvbWV0cmllcy5wdXNoKHtcbiAgICAgICAgdHlwZTogJ3BvbHlnb24nLFxuICAgICAgICAvLyBBY2NvcmRpbmcgdG8gdGhlIEdlb0pTT04gc3BlY2lmaWNhdGlvbi5cbiAgICAgICAgLy8gRmlyc3QgbXVzdCBiZSBleHRlcmlvciwgYW5kIHRoZSByZXN0IGFyZSBhbGwgaW50ZXJpb3IoaG9sZXMpLlxuICAgICAgICBleHRlcmlvcjogY29vcmRpbmF0ZXNbMF0sXG4gICAgICAgIGludGVyaW9yczogY29vcmRpbmF0ZXMuc2xpY2UoMSlcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChnZW8udHlwZSA9PT0gJ011bHRpUG9seWdvbicpIHtcbiAgICAgIHpyVXRpbC5lYWNoKGNvb3JkaW5hdGVzLCBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICBpZiAoaXRlbVswXSkge1xuICAgICAgICAgIGdlb21ldHJpZXMucHVzaCh7XG4gICAgICAgICAgICB0eXBlOiAncG9seWdvbicsXG4gICAgICAgICAgICBleHRlcmlvcjogaXRlbVswXSxcbiAgICAgICAgICAgIGludGVyaW9yczogaXRlbS5zbGljZSgxKVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICB2YXIgcmVnaW9uID0gbmV3IFJlZ2lvbihwcm9wZXJ0aWVzLm5hbWUsIGdlb21ldHJpZXMsIHByb3BlcnRpZXMuY3ApO1xuICAgIHJlZ2lvbi5wcm9wZXJ0aWVzID0gcHJvcGVydGllcztcbiAgICByZXR1cm4gcmVnaW9uO1xuICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/coord/geo/parseGeoJson.js
