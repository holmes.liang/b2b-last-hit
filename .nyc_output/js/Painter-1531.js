var _config = __webpack_require__(/*! ./config */ "./node_modules/zrender/lib/config.js");

var devicePixelRatio = _config.devicePixelRatio;

var util = __webpack_require__(/*! ./core/util */ "./node_modules/zrender/lib/core/util.js");

var log = __webpack_require__(/*! ./core/log */ "./node_modules/zrender/lib/core/log.js");

var BoundingRect = __webpack_require__(/*! ./core/BoundingRect */ "./node_modules/zrender/lib/core/BoundingRect.js");

var timsort = __webpack_require__(/*! ./core/timsort */ "./node_modules/zrender/lib/core/timsort.js");

var Layer = __webpack_require__(/*! ./Layer */ "./node_modules/zrender/lib/Layer.js");

var requestAnimationFrame = __webpack_require__(/*! ./animation/requestAnimationFrame */ "./node_modules/zrender/lib/animation/requestAnimationFrame.js");

var Image = __webpack_require__(/*! ./graphic/Image */ "./node_modules/zrender/lib/graphic/Image.js");

var env = __webpack_require__(/*! ./core/env */ "./node_modules/zrender/lib/core/env.js");

var HOVER_LAYER_ZLEVEL = 1e5;
var CANVAS_ZLEVEL = 314159;
var EL_AFTER_INCREMENTAL_INC = 0.01;
var INCREMENTAL_INC = 0.001;

function parseInt10(val) {
  return parseInt(val, 10);
}

function isLayerValid(layer) {
  if (!layer) {
    return false;
  }

  if (layer.__builtin__) {
    return true;
  }

  if (typeof layer.resize !== 'function' || typeof layer.refresh !== 'function') {
    return false;
  }

  return true;
}

var tmpRect = new BoundingRect(0, 0, 0, 0);
var viewRect = new BoundingRect(0, 0, 0, 0);

function isDisplayableCulled(el, width, height) {
  tmpRect.copy(el.getBoundingRect());

  if (el.transform) {
    tmpRect.applyTransform(el.transform);
  }

  viewRect.width = width;
  viewRect.height = height;
  return !tmpRect.intersect(viewRect);
}

function isClipPathChanged(clipPaths, prevClipPaths) {
  if (clipPaths === prevClipPaths) {
    // Can both be null or undefined
    return false;
  }

  if (!clipPaths || !prevClipPaths || clipPaths.length !== prevClipPaths.length) {
    return true;
  }

  for (var i = 0; i < clipPaths.length; i++) {
    if (clipPaths[i] !== prevClipPaths[i]) {
      return true;
    }
  }
}

function doClip(clipPaths, ctx) {
  for (var i = 0; i < clipPaths.length; i++) {
    var clipPath = clipPaths[i];
    clipPath.setTransform(ctx);
    ctx.beginPath();
    clipPath.buildPath(ctx, clipPath.shape);
    ctx.clip(); // Transform back

    clipPath.restoreTransform(ctx);
  }
}

function createRoot(width, height) {
  var domRoot = document.createElement('div'); // domRoot.onselectstart = returnFalse; // 避免页面选中的尴尬

  domRoot.style.cssText = ['position:relative', 'overflow:hidden', 'width:' + width + 'px', 'height:' + height + 'px', 'padding:0', 'margin:0', 'border-width:0'].join(';') + ';';
  return domRoot;
}
/**
 * @alias module:zrender/Painter
 * @constructor
 * @param {HTMLElement} root 绘图容器
 * @param {module:zrender/Storage} storage
 * @param {Object} opts
 */


var Painter = function Painter(root, storage, opts) {
  this.type = 'canvas'; // In node environment using node-canvas

  var singleCanvas = !root.nodeName // In node ?
  || root.nodeName.toUpperCase() === 'CANVAS';
  this._opts = opts = util.extend({}, opts || {});
  /**
   * @type {number}
   */

  this.dpr = opts.devicePixelRatio || devicePixelRatio;
  /**
   * @type {boolean}
   * @private
   */

  this._singleCanvas = singleCanvas;
  /**
   * 绘图容器
   * @type {HTMLElement}
   */

  this.root = root;
  var rootStyle = root.style;

  if (rootStyle) {
    rootStyle['-webkit-tap-highlight-color'] = 'transparent';
    rootStyle['-webkit-user-select'] = rootStyle['user-select'] = rootStyle['-webkit-touch-callout'] = 'none';
    root.innerHTML = '';
  }
  /**
   * @type {module:zrender/Storage}
   */


  this.storage = storage;
  /**
   * @type {Array.<number>}
   * @private
   */

  var zlevelList = this._zlevelList = [];
  /**
   * @type {Object.<string, module:zrender/Layer>}
   * @private
   */

  var layers = this._layers = {};
  /**
   * @type {Object.<string, Object>}
   * @private
   */

  this._layerConfig = {};
  /**
   * zrender will do compositing when root is a canvas and have multiple zlevels.
   */

  this._needsManuallyCompositing = false;

  if (!singleCanvas) {
    this._width = this._getSize(0);
    this._height = this._getSize(1);
    var domRoot = this._domRoot = createRoot(this._width, this._height);
    root.appendChild(domRoot);
  } else {
    var width = root.width;
    var height = root.height;

    if (opts.width != null) {
      width = opts.width;
    }

    if (opts.height != null) {
      height = opts.height;
    }

    this.dpr = opts.devicePixelRatio || 1; // Use canvas width and height directly

    root.width = width * this.dpr;
    root.height = height * this.dpr;
    this._width = width;
    this._height = height; // Create layer if only one given canvas
    // Device can be specified to create a high dpi image.

    var mainLayer = new Layer(root, this, this.dpr);
    mainLayer.__builtin__ = true;
    mainLayer.initContext(); // FIXME Use canvas width and height
    // mainLayer.resize(width, height);

    layers[CANVAS_ZLEVEL] = mainLayer;
    mainLayer.zlevel = CANVAS_ZLEVEL; // Not use common zlevel.

    zlevelList.push(CANVAS_ZLEVEL);
    this._domRoot = root;
  }
  /**
   * @type {module:zrender/Layer}
   * @private
   */


  this._hoverlayer = null;
  this._hoverElements = [];
};

Painter.prototype = {
  constructor: Painter,
  getType: function getType() {
    return 'canvas';
  },

  /**
   * If painter use a single canvas
   * @return {boolean}
   */
  isSingleCanvas: function isSingleCanvas() {
    return this._singleCanvas;
  },

  /**
   * @return {HTMLDivElement}
   */
  getViewportRoot: function getViewportRoot() {
    return this._domRoot;
  },
  getViewportRootOffset: function getViewportRootOffset() {
    var viewportRoot = this.getViewportRoot();

    if (viewportRoot) {
      return {
        offsetLeft: viewportRoot.offsetLeft || 0,
        offsetTop: viewportRoot.offsetTop || 0
      };
    }
  },

  /**
   * 刷新
   * @param {boolean} [paintAll=false] 强制绘制所有displayable
   */
  refresh: function refresh(paintAll) {
    var list = this.storage.getDisplayList(true);
    var zlevelList = this._zlevelList;
    this._redrawId = Math.random();

    this._paintList(list, paintAll, this._redrawId); // Paint custum layers


    for (var i = 0; i < zlevelList.length; i++) {
      var z = zlevelList[i];
      var layer = this._layers[z];

      if (!layer.__builtin__ && layer.refresh) {
        var clearColor = i === 0 ? this._backgroundColor : null;
        layer.refresh(clearColor);
      }
    }

    this.refreshHover();
    return this;
  },
  addHover: function addHover(el, hoverStyle) {
    if (el.__hoverMir) {
      return;
    }

    var elMirror = new el.constructor({
      style: el.style,
      shape: el.shape,
      z: el.z,
      z2: el.z2,
      silent: el.silent
    });
    elMirror.__from = el;
    el.__hoverMir = elMirror;
    hoverStyle && elMirror.setStyle(hoverStyle);

    this._hoverElements.push(elMirror);

    return elMirror;
  },
  removeHover: function removeHover(el) {
    var elMirror = el.__hoverMir;
    var hoverElements = this._hoverElements;
    var idx = util.indexOf(hoverElements, elMirror);

    if (idx >= 0) {
      hoverElements.splice(idx, 1);
    }

    el.__hoverMir = null;
  },
  clearHover: function clearHover(el) {
    var hoverElements = this._hoverElements;

    for (var i = 0; i < hoverElements.length; i++) {
      var from = hoverElements[i].__from;

      if (from) {
        from.__hoverMir = null;
      }
    }

    hoverElements.length = 0;
  },
  refreshHover: function refreshHover() {
    var hoverElements = this._hoverElements;
    var len = hoverElements.length;
    var hoverLayer = this._hoverlayer;
    hoverLayer && hoverLayer.clear();

    if (!len) {
      return;
    }

    timsort(hoverElements, this.storage.displayableSortFunc); // Use a extream large zlevel
    // FIXME?

    if (!hoverLayer) {
      hoverLayer = this._hoverlayer = this.getLayer(HOVER_LAYER_ZLEVEL);
    }

    var scope = {};
    hoverLayer.ctx.save();

    for (var i = 0; i < len;) {
      var el = hoverElements[i];
      var originalEl = el.__from; // Original el is removed
      // PENDING

      if (!(originalEl && originalEl.__zr)) {
        hoverElements.splice(i, 1);
        originalEl.__hoverMir = null;
        len--;
        continue;
      }

      i++; // Use transform
      // FIXME style and shape ?

      if (!originalEl.invisible) {
        el.transform = originalEl.transform;
        el.invTransform = originalEl.invTransform;
        el.__clipPaths = originalEl.__clipPaths; // el.

        this._doPaintEl(el, hoverLayer, true, scope);
      }
    }

    hoverLayer.ctx.restore();
  },
  getHoverLayer: function getHoverLayer() {
    return this.getLayer(HOVER_LAYER_ZLEVEL);
  },
  _paintList: function _paintList(list, paintAll, redrawId) {
    if (this._redrawId !== redrawId) {
      return;
    }

    paintAll = paintAll || false;

    this._updateLayerStatus(list);

    var finished = this._doPaintList(list, paintAll);

    if (this._needsManuallyCompositing) {
      this._compositeManually();
    }

    if (!finished) {
      var self = this;
      requestAnimationFrame(function () {
        self._paintList(list, paintAll, redrawId);
      });
    }
  },
  _compositeManually: function _compositeManually() {
    var ctx = this.getLayer(CANVAS_ZLEVEL).ctx;
    var width = this._domRoot.width;
    var height = this._domRoot.height;
    ctx.clearRect(0, 0, width, height); // PENDING, If only builtin layer?

    this.eachBuiltinLayer(function (layer) {
      if (layer.virtual) {
        ctx.drawImage(layer.dom, 0, 0, width, height);
      }
    });
  },
  _doPaintList: function _doPaintList(list, paintAll) {
    var layerList = [];

    for (var zi = 0; zi < this._zlevelList.length; zi++) {
      var zlevel = this._zlevelList[zi];
      var layer = this._layers[zlevel];

      if (layer.__builtin__ && layer !== this._hoverlayer && (layer.__dirty || paintAll)) {
        layerList.push(layer);
      }
    }

    var finished = true;

    for (var k = 0; k < layerList.length; k++) {
      var layer = layerList[k];
      var ctx = layer.ctx;
      var scope = {};
      ctx.save();
      var start = paintAll ? layer.__startIndex : layer.__drawIndex;
      var useTimer = !paintAll && layer.incremental && Date.now;
      var startTime = useTimer && Date.now();
      var clearColor = layer.zlevel === this._zlevelList[0] ? this._backgroundColor : null; // All elements in this layer are cleared.

      if (layer.__startIndex === layer.__endIndex) {
        layer.clear(false, clearColor);
      } else if (start === layer.__startIndex) {
        var firstEl = list[start];

        if (!firstEl.incremental || !firstEl.notClear || paintAll) {
          layer.clear(false, clearColor);
        }
      }

      if (start === -1) {
        console.error('For some unknown reason. drawIndex is -1');
        start = layer.__startIndex;
      }

      for (var i = start; i < layer.__endIndex; i++) {
        var el = list[i];

        this._doPaintEl(el, layer, paintAll, scope);

        el.__dirty = el.__dirtyText = false;

        if (useTimer) {
          // Date.now can be executed in 13,025,305 ops/second.
          var dTime = Date.now() - startTime; // Give 15 millisecond to draw.
          // The rest elements will be drawn in the next frame.

          if (dTime > 15) {
            break;
          }
        }
      }

      layer.__drawIndex = i;

      if (layer.__drawIndex < layer.__endIndex) {
        finished = false;
      }

      if (scope.prevElClipPaths) {
        // Needs restore the state. If last drawn element is in the clipping area.
        ctx.restore();
      }

      ctx.restore();
    }

    if (env.wxa) {
      // Flush for weixin application
      util.each(this._layers, function (layer) {
        if (layer && layer.ctx && layer.ctx.draw) {
          layer.ctx.draw();
        }
      });
    }

    return finished;
  },
  _doPaintEl: function _doPaintEl(el, currentLayer, forcePaint, scope) {
    var ctx = currentLayer.ctx;
    var m = el.transform;

    if ((currentLayer.__dirty || forcePaint) && // Ignore invisible element
    !el.invisible // Ignore transparent element
    && el.style.opacity !== 0 // Ignore scale 0 element, in some environment like node-canvas
    // Draw a scale 0 element can cause all following draw wrong
    // And setTransform with scale 0 will cause set back transform failed.
    && !(m && !m[0] && !m[3]) // Ignore culled element
    && !(el.culling && isDisplayableCulled(el, this._width, this._height))) {
      var clipPaths = el.__clipPaths; // Optimize when clipping on group with several elements

      if (!scope.prevElClipPaths || isClipPathChanged(clipPaths, scope.prevElClipPaths)) {
        // If has previous clipping state, restore from it
        if (scope.prevElClipPaths) {
          currentLayer.ctx.restore();
          scope.prevElClipPaths = null; // Reset prevEl since context has been restored

          scope.prevEl = null;
        } // New clipping state


        if (clipPaths) {
          ctx.save();
          doClip(clipPaths, ctx);
          scope.prevElClipPaths = clipPaths;
        }
      }

      el.beforeBrush && el.beforeBrush(ctx);
      el.brush(ctx, scope.prevEl || null);
      scope.prevEl = el;
      el.afterBrush && el.afterBrush(ctx);
    }
  },

  /**
   * 获取 zlevel 所在层，如果不存在则会创建一个新的层
   * @param {number} zlevel
   * @param {boolean} virtual Virtual layer will not be inserted into dom.
   * @return {module:zrender/Layer}
   */
  getLayer: function getLayer(zlevel, virtual) {
    if (this._singleCanvas && !this._needsManuallyCompositing) {
      zlevel = CANVAS_ZLEVEL;
    }

    var layer = this._layers[zlevel];

    if (!layer) {
      // Create a new layer
      layer = new Layer('zr_' + zlevel, this, this.dpr);
      layer.zlevel = zlevel;
      layer.__builtin__ = true;

      if (this._layerConfig[zlevel]) {
        util.merge(layer, this._layerConfig[zlevel], true);
      }

      if (virtual) {
        layer.virtual = virtual;
      }

      this.insertLayer(zlevel, layer); // Context is created after dom inserted to document
      // Or excanvas will get 0px clientWidth and clientHeight

      layer.initContext();
    }

    return layer;
  },
  insertLayer: function insertLayer(zlevel, layer) {
    var layersMap = this._layers;
    var zlevelList = this._zlevelList;
    var len = zlevelList.length;
    var prevLayer = null;
    var i = -1;
    var domRoot = this._domRoot;

    if (layersMap[zlevel]) {
      log('ZLevel ' + zlevel + ' has been used already');
      return;
    } // Check if is a valid layer


    if (!isLayerValid(layer)) {
      log('Layer of zlevel ' + zlevel + ' is not valid');
      return;
    }

    if (len > 0 && zlevel > zlevelList[0]) {
      for (i = 0; i < len - 1; i++) {
        if (zlevelList[i] < zlevel && zlevelList[i + 1] > zlevel) {
          break;
        }
      }

      prevLayer = layersMap[zlevelList[i]];
    }

    zlevelList.splice(i + 1, 0, zlevel);
    layersMap[zlevel] = layer; // Vitual layer will not directly show on the screen.
    // (It can be a WebGL layer and assigned to a ZImage element)
    // But it still under management of zrender.

    if (!layer.virtual) {
      if (prevLayer) {
        var prevDom = prevLayer.dom;

        if (prevDom.nextSibling) {
          domRoot.insertBefore(layer.dom, prevDom.nextSibling);
        } else {
          domRoot.appendChild(layer.dom);
        }
      } else {
        if (domRoot.firstChild) {
          domRoot.insertBefore(layer.dom, domRoot.firstChild);
        } else {
          domRoot.appendChild(layer.dom);
        }
      }
    }
  },
  // Iterate each layer
  eachLayer: function eachLayer(cb, context) {
    var zlevelList = this._zlevelList;
    var z;
    var i;

    for (i = 0; i < zlevelList.length; i++) {
      z = zlevelList[i];
      cb.call(context, this._layers[z], z);
    }
  },
  // Iterate each buildin layer
  eachBuiltinLayer: function eachBuiltinLayer(cb, context) {
    var zlevelList = this._zlevelList;
    var layer;
    var z;
    var i;

    for (i = 0; i < zlevelList.length; i++) {
      z = zlevelList[i];
      layer = this._layers[z];

      if (layer.__builtin__) {
        cb.call(context, layer, z);
      }
    }
  },
  // Iterate each other layer except buildin layer
  eachOtherLayer: function eachOtherLayer(cb, context) {
    var zlevelList = this._zlevelList;
    var layer;
    var z;
    var i;

    for (i = 0; i < zlevelList.length; i++) {
      z = zlevelList[i];
      layer = this._layers[z];

      if (!layer.__builtin__) {
        cb.call(context, layer, z);
      }
    }
  },

  /**
   * 获取所有已创建的层
   * @param {Array.<module:zrender/Layer>} [prevLayer]
   */
  getLayers: function getLayers() {
    return this._layers;
  },
  _updateLayerStatus: function _updateLayerStatus(list) {
    this.eachBuiltinLayer(function (layer, z) {
      layer.__dirty = layer.__used = false;
    });

    function updatePrevLayer(idx) {
      if (prevLayer) {
        if (prevLayer.__endIndex !== idx) {
          prevLayer.__dirty = true;
        }

        prevLayer.__endIndex = idx;
      }
    }

    if (this._singleCanvas) {
      for (var i = 1; i < list.length; i++) {
        var el = list[i];

        if (el.zlevel !== list[i - 1].zlevel || el.incremental) {
          this._needsManuallyCompositing = true;
          break;
        }
      }
    }

    var prevLayer = null;
    var incrementalLayerCount = 0;

    for (var i = 0; i < list.length; i++) {
      var el = list[i];
      var zlevel = el.zlevel;
      var layer; // PENDING If change one incremental element style ?
      // TODO Where there are non-incremental elements between incremental elements.

      if (el.incremental) {
        layer = this.getLayer(zlevel + INCREMENTAL_INC, this._needsManuallyCompositing);
        layer.incremental = true;
        incrementalLayerCount = 1;
      } else {
        layer = this.getLayer(zlevel + (incrementalLayerCount > 0 ? EL_AFTER_INCREMENTAL_INC : 0), this._needsManuallyCompositing);
      }

      if (!layer.__builtin__) {
        log('ZLevel ' + zlevel + ' has been used by unkown layer ' + layer.id);
      }

      if (layer !== prevLayer) {
        layer.__used = true;

        if (layer.__startIndex !== i) {
          layer.__dirty = true;
        }

        layer.__startIndex = i;

        if (!layer.incremental) {
          layer.__drawIndex = i;
        } else {
          // Mark layer draw index needs to update.
          layer.__drawIndex = -1;
        }

        updatePrevLayer(i);
        prevLayer = layer;
      }

      if (el.__dirty) {
        layer.__dirty = true;

        if (layer.incremental && layer.__drawIndex < 0) {
          // Start draw from the first dirty element.
          layer.__drawIndex = i;
        }
      }
    }

    updatePrevLayer(i);
    this.eachBuiltinLayer(function (layer, z) {
      // Used in last frame but not in this frame. Needs clear
      if (!layer.__used && layer.getElementCount() > 0) {
        layer.__dirty = true;
        layer.__startIndex = layer.__endIndex = layer.__drawIndex = 0;
      } // For incremental layer. In case start index changed and no elements are dirty.


      if (layer.__dirty && layer.__drawIndex < 0) {
        layer.__drawIndex = layer.__startIndex;
      }
    });
  },

  /**
   * 清除hover层外所有内容
   */
  clear: function clear() {
    this.eachBuiltinLayer(this._clearLayer);
    return this;
  },
  _clearLayer: function _clearLayer(layer) {
    layer.clear();
  },
  setBackgroundColor: function setBackgroundColor(backgroundColor) {
    this._backgroundColor = backgroundColor;
  },

  /**
   * 修改指定zlevel的绘制参数
   *
   * @param {string} zlevel
   * @param {Object} config 配置对象
   * @param {string} [config.clearColor=0] 每次清空画布的颜色
   * @param {string} [config.motionBlur=false] 是否开启动态模糊
   * @param {number} [config.lastFrameAlpha=0.7]
   *                 在开启动态模糊的时候使用，与上一帧混合的alpha值，值越大尾迹越明显
   */
  configLayer: function configLayer(zlevel, config) {
    if (config) {
      var layerConfig = this._layerConfig;

      if (!layerConfig[zlevel]) {
        layerConfig[zlevel] = config;
      } else {
        util.merge(layerConfig[zlevel], config, true);
      }

      for (var i = 0; i < this._zlevelList.length; i++) {
        var _zlevel = this._zlevelList[i];

        if (_zlevel === zlevel || _zlevel === zlevel + EL_AFTER_INCREMENTAL_INC) {
          var layer = this._layers[_zlevel];
          util.merge(layer, layerConfig[zlevel], true);
        }
      }
    }
  },

  /**
   * 删除指定层
   * @param {number} zlevel 层所在的zlevel
   */
  delLayer: function delLayer(zlevel) {
    var layers = this._layers;
    var zlevelList = this._zlevelList;
    var layer = layers[zlevel];

    if (!layer) {
      return;
    }

    layer.dom.parentNode.removeChild(layer.dom);
    delete layers[zlevel];
    zlevelList.splice(util.indexOf(zlevelList, zlevel), 1);
  },

  /**
   * 区域大小变化后重绘
   */
  resize: function resize(width, height) {
    if (!this._domRoot.style) {
      // Maybe in node or worker
      if (width == null || height == null) {
        return;
      }

      this._width = width;
      this._height = height;
      this.getLayer(CANVAS_ZLEVEL).resize(width, height);
    } else {
      var domRoot = this._domRoot; // FIXME Why ?

      domRoot.style.display = 'none'; // Save input w/h

      var opts = this._opts;
      width != null && (opts.width = width);
      height != null && (opts.height = height);
      width = this._getSize(0);
      height = this._getSize(1);
      domRoot.style.display = ''; // 优化没有实际改变的resize

      if (this._width !== width || height !== this._height) {
        domRoot.style.width = width + 'px';
        domRoot.style.height = height + 'px';

        for (var id in this._layers) {
          if (this._layers.hasOwnProperty(id)) {
            this._layers[id].resize(width, height);
          }
        }

        util.each(this._progressiveLayers, function (layer) {
          layer.resize(width, height);
        });
        this.refresh(true);
      }

      this._width = width;
      this._height = height;
    }

    return this;
  },

  /**
   * 清除单独的一个层
   * @param {number} zlevel
   */
  clearLayer: function clearLayer(zlevel) {
    var layer = this._layers[zlevel];

    if (layer) {
      layer.clear();
    }
  },

  /**
   * 释放
   */
  dispose: function dispose() {
    this.root.innerHTML = '';
    this.root = this.storage = this._domRoot = this._layers = null;
  },

  /**
   * Get canvas which has all thing rendered
   * @param {Object} opts
   * @param {string} [opts.backgroundColor]
   * @param {number} [opts.pixelRatio]
   */
  getRenderedCanvas: function getRenderedCanvas(opts) {
    opts = opts || {};

    if (this._singleCanvas && !this._compositeManually) {
      return this._layers[CANVAS_ZLEVEL].dom;
    }

    var imageLayer = new Layer('image', this, opts.pixelRatio || this.dpr);
    imageLayer.initContext();
    imageLayer.clear(false, opts.backgroundColor || this._backgroundColor);

    if (opts.pixelRatio <= this.dpr) {
      this.refresh();
      var width = imageLayer.dom.width;
      var height = imageLayer.dom.height;
      var ctx = imageLayer.ctx;
      this.eachLayer(function (layer) {
        if (layer.__builtin__) {
          ctx.drawImage(layer.dom, 0, 0, width, height);
        } else if (layer.renderToCanvas) {
          imageLayer.ctx.save();
          layer.renderToCanvas(imageLayer.ctx);
          imageLayer.ctx.restore();
        }
      });
    } else {
      // PENDING, echarts-gl and incremental rendering.
      var scope = {};
      var displayList = this.storage.getDisplayList(true);

      for (var i = 0; i < displayList.length; i++) {
        var el = displayList[i];

        this._doPaintEl(el, imageLayer, true, scope);
      }
    }

    return imageLayer.dom;
  },

  /**
   * 获取绘图区域宽度
   */
  getWidth: function getWidth() {
    return this._width;
  },

  /**
   * 获取绘图区域高度
   */
  getHeight: function getHeight() {
    return this._height;
  },
  _getSize: function _getSize(whIdx) {
    var opts = this._opts;
    var wh = ['width', 'height'][whIdx];
    var cwh = ['clientWidth', 'clientHeight'][whIdx];
    var plt = ['paddingLeft', 'paddingTop'][whIdx];
    var prb = ['paddingRight', 'paddingBottom'][whIdx];

    if (opts[wh] != null && opts[wh] !== 'auto') {
      return parseFloat(opts[wh]);
    }

    var root = this.root; // IE8 does not support getComputedStyle, but it use VML.

    var stl = document.defaultView.getComputedStyle(root);
    return (root[cwh] || parseInt10(stl[wh]) || parseInt10(root.style[wh])) - (parseInt10(stl[plt]) || 0) - (parseInt10(stl[prb]) || 0) | 0;
  },
  pathToImage: function pathToImage(path, dpr) {
    dpr = dpr || this.dpr;
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var rect = path.getBoundingRect();
    var style = path.style;
    var shadowBlurSize = style.shadowBlur * dpr;
    var shadowOffsetX = style.shadowOffsetX * dpr;
    var shadowOffsetY = style.shadowOffsetY * dpr;
    var lineWidth = style.hasStroke() ? style.lineWidth : 0;
    var leftMargin = Math.max(lineWidth / 2, -shadowOffsetX + shadowBlurSize);
    var rightMargin = Math.max(lineWidth / 2, shadowOffsetX + shadowBlurSize);
    var topMargin = Math.max(lineWidth / 2, -shadowOffsetY + shadowBlurSize);
    var bottomMargin = Math.max(lineWidth / 2, shadowOffsetY + shadowBlurSize);
    var width = rect.width + leftMargin + rightMargin;
    var height = rect.height + topMargin + bottomMargin;
    canvas.width = width * dpr;
    canvas.height = height * dpr;
    ctx.scale(dpr, dpr);
    ctx.clearRect(0, 0, width, height);
    ctx.dpr = dpr;
    var pathTransform = {
      position: path.position,
      rotation: path.rotation,
      scale: path.scale
    };
    path.position = [leftMargin - rect.x, topMargin - rect.y];
    path.rotation = 0;
    path.scale = [1, 1];
    path.updateTransform();

    if (path) {
      path.brush(ctx);
    }

    var ImageShape = Image;
    var imgShape = new ImageShape({
      style: {
        x: 0,
        y: 0,
        image: canvas
      }
    });

    if (pathTransform.position != null) {
      imgShape.position = path.position = pathTransform.position;
    }

    if (pathTransform.rotation != null) {
      imgShape.rotation = path.rotation = pathTransform.rotation;
    }

    if (pathTransform.scale != null) {
      imgShape.scale = path.scale = pathTransform.scale;
    }

    return imgShape;
  }
};
var _default = Painter;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvUGFpbnRlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3pyZW5kZXIvbGliL1BhaW50ZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIF9jb25maWcgPSByZXF1aXJlKFwiLi9jb25maWdcIik7XG5cbnZhciBkZXZpY2VQaXhlbFJhdGlvID0gX2NvbmZpZy5kZXZpY2VQaXhlbFJhdGlvO1xuXG52YXIgdXRpbCA9IHJlcXVpcmUoXCIuL2NvcmUvdXRpbFwiKTtcblxudmFyIGxvZyA9IHJlcXVpcmUoXCIuL2NvcmUvbG9nXCIpO1xuXG52YXIgQm91bmRpbmdSZWN0ID0gcmVxdWlyZShcIi4vY29yZS9Cb3VuZGluZ1JlY3RcIik7XG5cbnZhciB0aW1zb3J0ID0gcmVxdWlyZShcIi4vY29yZS90aW1zb3J0XCIpO1xuXG52YXIgTGF5ZXIgPSByZXF1aXJlKFwiLi9MYXllclwiKTtcblxudmFyIHJlcXVlc3RBbmltYXRpb25GcmFtZSA9IHJlcXVpcmUoXCIuL2FuaW1hdGlvbi9yZXF1ZXN0QW5pbWF0aW9uRnJhbWVcIik7XG5cbnZhciBJbWFnZSA9IHJlcXVpcmUoXCIuL2dyYXBoaWMvSW1hZ2VcIik7XG5cbnZhciBlbnYgPSByZXF1aXJlKFwiLi9jb3JlL2VudlwiKTtcblxudmFyIEhPVkVSX0xBWUVSX1pMRVZFTCA9IDFlNTtcbnZhciBDQU5WQVNfWkxFVkVMID0gMzE0MTU5O1xudmFyIEVMX0FGVEVSX0lOQ1JFTUVOVEFMX0lOQyA9IDAuMDE7XG52YXIgSU5DUkVNRU5UQUxfSU5DID0gMC4wMDE7XG5cbmZ1bmN0aW9uIHBhcnNlSW50MTAodmFsKSB7XG4gIHJldHVybiBwYXJzZUludCh2YWwsIDEwKTtcbn1cblxuZnVuY3Rpb24gaXNMYXllclZhbGlkKGxheWVyKSB7XG4gIGlmICghbGF5ZXIpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBpZiAobGF5ZXIuX19idWlsdGluX18pIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGlmICh0eXBlb2YgbGF5ZXIucmVzaXplICE9PSAnZnVuY3Rpb24nIHx8IHR5cGVvZiBsYXllci5yZWZyZXNoICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgcmV0dXJuIHRydWU7XG59XG5cbnZhciB0bXBSZWN0ID0gbmV3IEJvdW5kaW5nUmVjdCgwLCAwLCAwLCAwKTtcbnZhciB2aWV3UmVjdCA9IG5ldyBCb3VuZGluZ1JlY3QoMCwgMCwgMCwgMCk7XG5cbmZ1bmN0aW9uIGlzRGlzcGxheWFibGVDdWxsZWQoZWwsIHdpZHRoLCBoZWlnaHQpIHtcbiAgdG1wUmVjdC5jb3B5KGVsLmdldEJvdW5kaW5nUmVjdCgpKTtcblxuICBpZiAoZWwudHJhbnNmb3JtKSB7XG4gICAgdG1wUmVjdC5hcHBseVRyYW5zZm9ybShlbC50cmFuc2Zvcm0pO1xuICB9XG5cbiAgdmlld1JlY3Qud2lkdGggPSB3aWR0aDtcbiAgdmlld1JlY3QuaGVpZ2h0ID0gaGVpZ2h0O1xuICByZXR1cm4gIXRtcFJlY3QuaW50ZXJzZWN0KHZpZXdSZWN0KTtcbn1cblxuZnVuY3Rpb24gaXNDbGlwUGF0aENoYW5nZWQoY2xpcFBhdGhzLCBwcmV2Q2xpcFBhdGhzKSB7XG4gIGlmIChjbGlwUGF0aHMgPT09IHByZXZDbGlwUGF0aHMpIHtcbiAgICAvLyBDYW4gYm90aCBiZSBudWxsIG9yIHVuZGVmaW5lZFxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIGlmICghY2xpcFBhdGhzIHx8ICFwcmV2Q2xpcFBhdGhzIHx8IGNsaXBQYXRocy5sZW5ndGggIT09IHByZXZDbGlwUGF0aHMubGVuZ3RoKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGNsaXBQYXRocy5sZW5ndGg7IGkrKykge1xuICAgIGlmIChjbGlwUGF0aHNbaV0gIT09IHByZXZDbGlwUGF0aHNbaV0pIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBkb0NsaXAoY2xpcFBhdGhzLCBjdHgpIHtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBjbGlwUGF0aHMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgY2xpcFBhdGggPSBjbGlwUGF0aHNbaV07XG4gICAgY2xpcFBhdGguc2V0VHJhbnNmb3JtKGN0eCk7XG4gICAgY3R4LmJlZ2luUGF0aCgpO1xuICAgIGNsaXBQYXRoLmJ1aWxkUGF0aChjdHgsIGNsaXBQYXRoLnNoYXBlKTtcbiAgICBjdHguY2xpcCgpOyAvLyBUcmFuc2Zvcm0gYmFja1xuXG4gICAgY2xpcFBhdGgucmVzdG9yZVRyYW5zZm9ybShjdHgpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVJvb3Qod2lkdGgsIGhlaWdodCkge1xuICB2YXIgZG9tUm9vdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpOyAvLyBkb21Sb290Lm9uc2VsZWN0c3RhcnQgPSByZXR1cm5GYWxzZTsgLy8g6YG/5YWN6aG16Z2i6YCJ5Lit55qE5bC05bCsXG5cbiAgZG9tUm9vdC5zdHlsZS5jc3NUZXh0ID0gWydwb3NpdGlvbjpyZWxhdGl2ZScsICdvdmVyZmxvdzpoaWRkZW4nLCAnd2lkdGg6JyArIHdpZHRoICsgJ3B4JywgJ2hlaWdodDonICsgaGVpZ2h0ICsgJ3B4JywgJ3BhZGRpbmc6MCcsICdtYXJnaW46MCcsICdib3JkZXItd2lkdGg6MCddLmpvaW4oJzsnKSArICc7JztcbiAgcmV0dXJuIGRvbVJvb3Q7XG59XG4vKipcbiAqIEBhbGlhcyBtb2R1bGU6enJlbmRlci9QYWludGVyXG4gKiBAY29uc3RydWN0b3JcbiAqIEBwYXJhbSB7SFRNTEVsZW1lbnR9IHJvb3Qg57uY5Zu+5a655ZmoXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL1N0b3JhZ2V9IHN0b3JhZ2VcbiAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXG4gKi9cblxuXG52YXIgUGFpbnRlciA9IGZ1bmN0aW9uIChyb290LCBzdG9yYWdlLCBvcHRzKSB7XG4gIHRoaXMudHlwZSA9ICdjYW52YXMnOyAvLyBJbiBub2RlIGVudmlyb25tZW50IHVzaW5nIG5vZGUtY2FudmFzXG5cbiAgdmFyIHNpbmdsZUNhbnZhcyA9ICFyb290Lm5vZGVOYW1lIC8vIEluIG5vZGUgP1xuICB8fCByb290Lm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkgPT09ICdDQU5WQVMnO1xuICB0aGlzLl9vcHRzID0gb3B0cyA9IHV0aWwuZXh0ZW5kKHt9LCBvcHRzIHx8IHt9KTtcbiAgLyoqXG4gICAqIEB0eXBlIHtudW1iZXJ9XG4gICAqL1xuXG4gIHRoaXMuZHByID0gb3B0cy5kZXZpY2VQaXhlbFJhdGlvIHx8IGRldmljZVBpeGVsUmF0aW87XG4gIC8qKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQHByaXZhdGVcbiAgICovXG5cbiAgdGhpcy5fc2luZ2xlQ2FudmFzID0gc2luZ2xlQ2FudmFzO1xuICAvKipcbiAgICog57uY5Zu+5a655ZmoXG4gICAqIEB0eXBlIHtIVE1MRWxlbWVudH1cbiAgICovXG5cbiAgdGhpcy5yb290ID0gcm9vdDtcbiAgdmFyIHJvb3RTdHlsZSA9IHJvb3Quc3R5bGU7XG5cbiAgaWYgKHJvb3RTdHlsZSkge1xuICAgIHJvb3RTdHlsZVsnLXdlYmtpdC10YXAtaGlnaGxpZ2h0LWNvbG9yJ10gPSAndHJhbnNwYXJlbnQnO1xuICAgIHJvb3RTdHlsZVsnLXdlYmtpdC11c2VyLXNlbGVjdCddID0gcm9vdFN0eWxlWyd1c2VyLXNlbGVjdCddID0gcm9vdFN0eWxlWyctd2Via2l0LXRvdWNoLWNhbGxvdXQnXSA9ICdub25lJztcbiAgICByb290LmlubmVySFRNTCA9ICcnO1xuICB9XG4gIC8qKlxuICAgKiBAdHlwZSB7bW9kdWxlOnpyZW5kZXIvU3RvcmFnZX1cbiAgICovXG5cblxuICB0aGlzLnN0b3JhZ2UgPSBzdG9yYWdlO1xuICAvKipcbiAgICogQHR5cGUge0FycmF5LjxudW1iZXI+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB2YXIgemxldmVsTGlzdCA9IHRoaXMuX3psZXZlbExpc3QgPSBbXTtcbiAgLyoqXG4gICAqIEB0eXBlIHtPYmplY3QuPHN0cmluZywgbW9kdWxlOnpyZW5kZXIvTGF5ZXI+fVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuICB2YXIgbGF5ZXJzID0gdGhpcy5fbGF5ZXJzID0ge307XG4gIC8qKlxuICAgKiBAdHlwZSB7T2JqZWN0LjxzdHJpbmcsIE9iamVjdD59XG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG4gIHRoaXMuX2xheWVyQ29uZmlnID0ge307XG4gIC8qKlxuICAgKiB6cmVuZGVyIHdpbGwgZG8gY29tcG9zaXRpbmcgd2hlbiByb290IGlzIGEgY2FudmFzIGFuZCBoYXZlIG11bHRpcGxlIHpsZXZlbHMuXG4gICAqL1xuXG4gIHRoaXMuX25lZWRzTWFudWFsbHlDb21wb3NpdGluZyA9IGZhbHNlO1xuXG4gIGlmICghc2luZ2xlQ2FudmFzKSB7XG4gICAgdGhpcy5fd2lkdGggPSB0aGlzLl9nZXRTaXplKDApO1xuICAgIHRoaXMuX2hlaWdodCA9IHRoaXMuX2dldFNpemUoMSk7XG4gICAgdmFyIGRvbVJvb3QgPSB0aGlzLl9kb21Sb290ID0gY3JlYXRlUm9vdCh0aGlzLl93aWR0aCwgdGhpcy5faGVpZ2h0KTtcbiAgICByb290LmFwcGVuZENoaWxkKGRvbVJvb3QpO1xuICB9IGVsc2Uge1xuICAgIHZhciB3aWR0aCA9IHJvb3Qud2lkdGg7XG4gICAgdmFyIGhlaWdodCA9IHJvb3QuaGVpZ2h0O1xuXG4gICAgaWYgKG9wdHMud2lkdGggIT0gbnVsbCkge1xuICAgICAgd2lkdGggPSBvcHRzLndpZHRoO1xuICAgIH1cblxuICAgIGlmIChvcHRzLmhlaWdodCAhPSBudWxsKSB7XG4gICAgICBoZWlnaHQgPSBvcHRzLmhlaWdodDtcbiAgICB9XG5cbiAgICB0aGlzLmRwciA9IG9wdHMuZGV2aWNlUGl4ZWxSYXRpbyB8fCAxOyAvLyBVc2UgY2FudmFzIHdpZHRoIGFuZCBoZWlnaHQgZGlyZWN0bHlcblxuICAgIHJvb3Qud2lkdGggPSB3aWR0aCAqIHRoaXMuZHByO1xuICAgIHJvb3QuaGVpZ2h0ID0gaGVpZ2h0ICogdGhpcy5kcHI7XG4gICAgdGhpcy5fd2lkdGggPSB3aWR0aDtcbiAgICB0aGlzLl9oZWlnaHQgPSBoZWlnaHQ7IC8vIENyZWF0ZSBsYXllciBpZiBvbmx5IG9uZSBnaXZlbiBjYW52YXNcbiAgICAvLyBEZXZpY2UgY2FuIGJlIHNwZWNpZmllZCB0byBjcmVhdGUgYSBoaWdoIGRwaSBpbWFnZS5cblxuICAgIHZhciBtYWluTGF5ZXIgPSBuZXcgTGF5ZXIocm9vdCwgdGhpcywgdGhpcy5kcHIpO1xuICAgIG1haW5MYXllci5fX2J1aWx0aW5fXyA9IHRydWU7XG4gICAgbWFpbkxheWVyLmluaXRDb250ZXh0KCk7IC8vIEZJWE1FIFVzZSBjYW52YXMgd2lkdGggYW5kIGhlaWdodFxuICAgIC8vIG1haW5MYXllci5yZXNpemUod2lkdGgsIGhlaWdodCk7XG5cbiAgICBsYXllcnNbQ0FOVkFTX1pMRVZFTF0gPSBtYWluTGF5ZXI7XG4gICAgbWFpbkxheWVyLnpsZXZlbCA9IENBTlZBU19aTEVWRUw7IC8vIE5vdCB1c2UgY29tbW9uIHpsZXZlbC5cblxuICAgIHpsZXZlbExpc3QucHVzaChDQU5WQVNfWkxFVkVMKTtcbiAgICB0aGlzLl9kb21Sb290ID0gcm9vdDtcbiAgfVxuICAvKipcbiAgICogQHR5cGUge21vZHVsZTp6cmVuZGVyL0xheWVyfVxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIHRoaXMuX2hvdmVybGF5ZXIgPSBudWxsO1xuICB0aGlzLl9ob3ZlckVsZW1lbnRzID0gW107XG59O1xuXG5QYWludGVyLnByb3RvdHlwZSA9IHtcbiAgY29uc3RydWN0b3I6IFBhaW50ZXIsXG4gIGdldFR5cGU6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gJ2NhbnZhcyc7XG4gIH0sXG5cbiAgLyoqXG4gICAqIElmIHBhaW50ZXIgdXNlIGEgc2luZ2xlIGNhbnZhc1xuICAgKiBAcmV0dXJuIHtib29sZWFufVxuICAgKi9cbiAgaXNTaW5nbGVDYW52YXM6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5fc2luZ2xlQ2FudmFzO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAcmV0dXJuIHtIVE1MRGl2RWxlbWVudH1cbiAgICovXG4gIGdldFZpZXdwb3J0Um9vdDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9kb21Sb290O1xuICB9LFxuICBnZXRWaWV3cG9ydFJvb3RPZmZzZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmlld3BvcnRSb290ID0gdGhpcy5nZXRWaWV3cG9ydFJvb3QoKTtcblxuICAgIGlmICh2aWV3cG9ydFJvb3QpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG9mZnNldExlZnQ6IHZpZXdwb3J0Um9vdC5vZmZzZXRMZWZ0IHx8IDAsXG4gICAgICAgIG9mZnNldFRvcDogdmlld3BvcnRSb290Lm9mZnNldFRvcCB8fCAwXG4gICAgICB9O1xuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICog5Yi35pawXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW3BhaW50QWxsPWZhbHNlXSDlvLrliLbnu5jliLbmiYDmnIlkaXNwbGF5YWJsZVxuICAgKi9cbiAgcmVmcmVzaDogZnVuY3Rpb24gKHBhaW50QWxsKSB7XG4gICAgdmFyIGxpc3QgPSB0aGlzLnN0b3JhZ2UuZ2V0RGlzcGxheUxpc3QodHJ1ZSk7XG4gICAgdmFyIHpsZXZlbExpc3QgPSB0aGlzLl96bGV2ZWxMaXN0O1xuICAgIHRoaXMuX3JlZHJhd0lkID0gTWF0aC5yYW5kb20oKTtcblxuICAgIHRoaXMuX3BhaW50TGlzdChsaXN0LCBwYWludEFsbCwgdGhpcy5fcmVkcmF3SWQpOyAvLyBQYWludCBjdXN0dW0gbGF5ZXJzXG5cblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgemxldmVsTGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIHogPSB6bGV2ZWxMaXN0W2ldO1xuICAgICAgdmFyIGxheWVyID0gdGhpcy5fbGF5ZXJzW3pdO1xuXG4gICAgICBpZiAoIWxheWVyLl9fYnVpbHRpbl9fICYmIGxheWVyLnJlZnJlc2gpIHtcbiAgICAgICAgdmFyIGNsZWFyQ29sb3IgPSBpID09PSAwID8gdGhpcy5fYmFja2dyb3VuZENvbG9yIDogbnVsbDtcbiAgICAgICAgbGF5ZXIucmVmcmVzaChjbGVhckNvbG9yKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnJlZnJlc2hIb3ZlcigpO1xuICAgIHJldHVybiB0aGlzO1xuICB9LFxuICBhZGRIb3ZlcjogZnVuY3Rpb24gKGVsLCBob3ZlclN0eWxlKSB7XG4gICAgaWYgKGVsLl9faG92ZXJNaXIpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgZWxNaXJyb3IgPSBuZXcgZWwuY29uc3RydWN0b3Ioe1xuICAgICAgc3R5bGU6IGVsLnN0eWxlLFxuICAgICAgc2hhcGU6IGVsLnNoYXBlLFxuICAgICAgejogZWwueixcbiAgICAgIHoyOiBlbC56MixcbiAgICAgIHNpbGVudDogZWwuc2lsZW50XG4gICAgfSk7XG4gICAgZWxNaXJyb3IuX19mcm9tID0gZWw7XG4gICAgZWwuX19ob3Zlck1pciA9IGVsTWlycm9yO1xuICAgIGhvdmVyU3R5bGUgJiYgZWxNaXJyb3Iuc2V0U3R5bGUoaG92ZXJTdHlsZSk7XG5cbiAgICB0aGlzLl9ob3ZlckVsZW1lbnRzLnB1c2goZWxNaXJyb3IpO1xuXG4gICAgcmV0dXJuIGVsTWlycm9yO1xuICB9LFxuICByZW1vdmVIb3ZlcjogZnVuY3Rpb24gKGVsKSB7XG4gICAgdmFyIGVsTWlycm9yID0gZWwuX19ob3Zlck1pcjtcbiAgICB2YXIgaG92ZXJFbGVtZW50cyA9IHRoaXMuX2hvdmVyRWxlbWVudHM7XG4gICAgdmFyIGlkeCA9IHV0aWwuaW5kZXhPZihob3ZlckVsZW1lbnRzLCBlbE1pcnJvcik7XG5cbiAgICBpZiAoaWR4ID49IDApIHtcbiAgICAgIGhvdmVyRWxlbWVudHMuc3BsaWNlKGlkeCwgMSk7XG4gICAgfVxuXG4gICAgZWwuX19ob3Zlck1pciA9IG51bGw7XG4gIH0sXG4gIGNsZWFySG92ZXI6IGZ1bmN0aW9uIChlbCkge1xuICAgIHZhciBob3ZlckVsZW1lbnRzID0gdGhpcy5faG92ZXJFbGVtZW50cztcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaG92ZXJFbGVtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGZyb20gPSBob3ZlckVsZW1lbnRzW2ldLl9fZnJvbTtcblxuICAgICAgaWYgKGZyb20pIHtcbiAgICAgICAgZnJvbS5fX2hvdmVyTWlyID0gbnVsbDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBob3ZlckVsZW1lbnRzLmxlbmd0aCA9IDA7XG4gIH0sXG4gIHJlZnJlc2hIb3ZlcjogZnVuY3Rpb24gKCkge1xuICAgIHZhciBob3ZlckVsZW1lbnRzID0gdGhpcy5faG92ZXJFbGVtZW50cztcbiAgICB2YXIgbGVuID0gaG92ZXJFbGVtZW50cy5sZW5ndGg7XG4gICAgdmFyIGhvdmVyTGF5ZXIgPSB0aGlzLl9ob3ZlcmxheWVyO1xuICAgIGhvdmVyTGF5ZXIgJiYgaG92ZXJMYXllci5jbGVhcigpO1xuXG4gICAgaWYgKCFsZW4pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aW1zb3J0KGhvdmVyRWxlbWVudHMsIHRoaXMuc3RvcmFnZS5kaXNwbGF5YWJsZVNvcnRGdW5jKTsgLy8gVXNlIGEgZXh0cmVhbSBsYXJnZSB6bGV2ZWxcbiAgICAvLyBGSVhNRT9cblxuICAgIGlmICghaG92ZXJMYXllcikge1xuICAgICAgaG92ZXJMYXllciA9IHRoaXMuX2hvdmVybGF5ZXIgPSB0aGlzLmdldExheWVyKEhPVkVSX0xBWUVSX1pMRVZFTCk7XG4gICAgfVxuXG4gICAgdmFyIHNjb3BlID0ge307XG4gICAgaG92ZXJMYXllci5jdHguc2F2ZSgpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47KSB7XG4gICAgICB2YXIgZWwgPSBob3ZlckVsZW1lbnRzW2ldO1xuICAgICAgdmFyIG9yaWdpbmFsRWwgPSBlbC5fX2Zyb207IC8vIE9yaWdpbmFsIGVsIGlzIHJlbW92ZWRcbiAgICAgIC8vIFBFTkRJTkdcblxuICAgICAgaWYgKCEob3JpZ2luYWxFbCAmJiBvcmlnaW5hbEVsLl9fenIpKSB7XG4gICAgICAgIGhvdmVyRWxlbWVudHMuc3BsaWNlKGksIDEpO1xuICAgICAgICBvcmlnaW5hbEVsLl9faG92ZXJNaXIgPSBudWxsO1xuICAgICAgICBsZW4tLTtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGkrKzsgLy8gVXNlIHRyYW5zZm9ybVxuICAgICAgLy8gRklYTUUgc3R5bGUgYW5kIHNoYXBlID9cblxuICAgICAgaWYgKCFvcmlnaW5hbEVsLmludmlzaWJsZSkge1xuICAgICAgICBlbC50cmFuc2Zvcm0gPSBvcmlnaW5hbEVsLnRyYW5zZm9ybTtcbiAgICAgICAgZWwuaW52VHJhbnNmb3JtID0gb3JpZ2luYWxFbC5pbnZUcmFuc2Zvcm07XG4gICAgICAgIGVsLl9fY2xpcFBhdGhzID0gb3JpZ2luYWxFbC5fX2NsaXBQYXRoczsgLy8gZWwuXG5cbiAgICAgICAgdGhpcy5fZG9QYWludEVsKGVsLCBob3ZlckxheWVyLCB0cnVlLCBzY29wZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaG92ZXJMYXllci5jdHgucmVzdG9yZSgpO1xuICB9LFxuICBnZXRIb3ZlckxheWVyOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ2V0TGF5ZXIoSE9WRVJfTEFZRVJfWkxFVkVMKTtcbiAgfSxcbiAgX3BhaW50TGlzdDogZnVuY3Rpb24gKGxpc3QsIHBhaW50QWxsLCByZWRyYXdJZCkge1xuICAgIGlmICh0aGlzLl9yZWRyYXdJZCAhPT0gcmVkcmF3SWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBwYWludEFsbCA9IHBhaW50QWxsIHx8IGZhbHNlO1xuXG4gICAgdGhpcy5fdXBkYXRlTGF5ZXJTdGF0dXMobGlzdCk7XG5cbiAgICB2YXIgZmluaXNoZWQgPSB0aGlzLl9kb1BhaW50TGlzdChsaXN0LCBwYWludEFsbCk7XG5cbiAgICBpZiAodGhpcy5fbmVlZHNNYW51YWxseUNvbXBvc2l0aW5nKSB7XG4gICAgICB0aGlzLl9jb21wb3NpdGVNYW51YWxseSgpO1xuICAgIH1cblxuICAgIGlmICghZmluaXNoZWQpIHtcbiAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNlbGYuX3BhaW50TGlzdChsaXN0LCBwYWludEFsbCwgcmVkcmF3SWQpO1xuICAgICAgfSk7XG4gICAgfVxuICB9LFxuICBfY29tcG9zaXRlTWFudWFsbHk6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgY3R4ID0gdGhpcy5nZXRMYXllcihDQU5WQVNfWkxFVkVMKS5jdHg7XG4gICAgdmFyIHdpZHRoID0gdGhpcy5fZG9tUm9vdC53aWR0aDtcbiAgICB2YXIgaGVpZ2h0ID0gdGhpcy5fZG9tUm9vdC5oZWlnaHQ7XG4gICAgY3R4LmNsZWFyUmVjdCgwLCAwLCB3aWR0aCwgaGVpZ2h0KTsgLy8gUEVORElORywgSWYgb25seSBidWlsdGluIGxheWVyP1xuXG4gICAgdGhpcy5lYWNoQnVpbHRpbkxheWVyKGZ1bmN0aW9uIChsYXllcikge1xuICAgICAgaWYgKGxheWVyLnZpcnR1YWwpIHtcbiAgICAgICAgY3R4LmRyYXdJbWFnZShsYXllci5kb20sIDAsIDAsIHdpZHRoLCBoZWlnaHQpO1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxuICBfZG9QYWludExpc3Q6IGZ1bmN0aW9uIChsaXN0LCBwYWludEFsbCkge1xuICAgIHZhciBsYXllckxpc3QgPSBbXTtcblxuICAgIGZvciAodmFyIHppID0gMDsgemkgPCB0aGlzLl96bGV2ZWxMaXN0Lmxlbmd0aDsgemkrKykge1xuICAgICAgdmFyIHpsZXZlbCA9IHRoaXMuX3psZXZlbExpc3RbemldO1xuICAgICAgdmFyIGxheWVyID0gdGhpcy5fbGF5ZXJzW3psZXZlbF07XG5cbiAgICAgIGlmIChsYXllci5fX2J1aWx0aW5fXyAmJiBsYXllciAhPT0gdGhpcy5faG92ZXJsYXllciAmJiAobGF5ZXIuX19kaXJ0eSB8fCBwYWludEFsbCkpIHtcbiAgICAgICAgbGF5ZXJMaXN0LnB1c2gobGF5ZXIpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciBmaW5pc2hlZCA9IHRydWU7XG5cbiAgICBmb3IgKHZhciBrID0gMDsgayA8IGxheWVyTGlzdC5sZW5ndGg7IGsrKykge1xuICAgICAgdmFyIGxheWVyID0gbGF5ZXJMaXN0W2tdO1xuICAgICAgdmFyIGN0eCA9IGxheWVyLmN0eDtcbiAgICAgIHZhciBzY29wZSA9IHt9O1xuICAgICAgY3R4LnNhdmUoKTtcbiAgICAgIHZhciBzdGFydCA9IHBhaW50QWxsID8gbGF5ZXIuX19zdGFydEluZGV4IDogbGF5ZXIuX19kcmF3SW5kZXg7XG4gICAgICB2YXIgdXNlVGltZXIgPSAhcGFpbnRBbGwgJiYgbGF5ZXIuaW5jcmVtZW50YWwgJiYgRGF0ZS5ub3c7XG4gICAgICB2YXIgc3RhcnRUaW1lID0gdXNlVGltZXIgJiYgRGF0ZS5ub3coKTtcbiAgICAgIHZhciBjbGVhckNvbG9yID0gbGF5ZXIuemxldmVsID09PSB0aGlzLl96bGV2ZWxMaXN0WzBdID8gdGhpcy5fYmFja2dyb3VuZENvbG9yIDogbnVsbDsgLy8gQWxsIGVsZW1lbnRzIGluIHRoaXMgbGF5ZXIgYXJlIGNsZWFyZWQuXG5cbiAgICAgIGlmIChsYXllci5fX3N0YXJ0SW5kZXggPT09IGxheWVyLl9fZW5kSW5kZXgpIHtcbiAgICAgICAgbGF5ZXIuY2xlYXIoZmFsc2UsIGNsZWFyQ29sb3IpO1xuICAgICAgfSBlbHNlIGlmIChzdGFydCA9PT0gbGF5ZXIuX19zdGFydEluZGV4KSB7XG4gICAgICAgIHZhciBmaXJzdEVsID0gbGlzdFtzdGFydF07XG5cbiAgICAgICAgaWYgKCFmaXJzdEVsLmluY3JlbWVudGFsIHx8ICFmaXJzdEVsLm5vdENsZWFyIHx8IHBhaW50QWxsKSB7XG4gICAgICAgICAgbGF5ZXIuY2xlYXIoZmFsc2UsIGNsZWFyQ29sb3IpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChzdGFydCA9PT0gLTEpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcignRm9yIHNvbWUgdW5rbm93biByZWFzb24uIGRyYXdJbmRleCBpcyAtMScpO1xuICAgICAgICBzdGFydCA9IGxheWVyLl9fc3RhcnRJbmRleDtcbiAgICAgIH1cblxuICAgICAgZm9yICh2YXIgaSA9IHN0YXJ0OyBpIDwgbGF5ZXIuX19lbmRJbmRleDsgaSsrKSB7XG4gICAgICAgIHZhciBlbCA9IGxpc3RbaV07XG5cbiAgICAgICAgdGhpcy5fZG9QYWludEVsKGVsLCBsYXllciwgcGFpbnRBbGwsIHNjb3BlKTtcblxuICAgICAgICBlbC5fX2RpcnR5ID0gZWwuX19kaXJ0eVRleHQgPSBmYWxzZTtcblxuICAgICAgICBpZiAodXNlVGltZXIpIHtcbiAgICAgICAgICAvLyBEYXRlLm5vdyBjYW4gYmUgZXhlY3V0ZWQgaW4gMTMsMDI1LDMwNSBvcHMvc2Vjb25kLlxuICAgICAgICAgIHZhciBkVGltZSA9IERhdGUubm93KCkgLSBzdGFydFRpbWU7IC8vIEdpdmUgMTUgbWlsbGlzZWNvbmQgdG8gZHJhdy5cbiAgICAgICAgICAvLyBUaGUgcmVzdCBlbGVtZW50cyB3aWxsIGJlIGRyYXduIGluIHRoZSBuZXh0IGZyYW1lLlxuXG4gICAgICAgICAgaWYgKGRUaW1lID4gMTUpIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBsYXllci5fX2RyYXdJbmRleCA9IGk7XG5cbiAgICAgIGlmIChsYXllci5fX2RyYXdJbmRleCA8IGxheWVyLl9fZW5kSW5kZXgpIHtcbiAgICAgICAgZmluaXNoZWQgPSBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgaWYgKHNjb3BlLnByZXZFbENsaXBQYXRocykge1xuICAgICAgICAvLyBOZWVkcyByZXN0b3JlIHRoZSBzdGF0ZS4gSWYgbGFzdCBkcmF3biBlbGVtZW50IGlzIGluIHRoZSBjbGlwcGluZyBhcmVhLlxuICAgICAgICBjdHgucmVzdG9yZSgpO1xuICAgICAgfVxuXG4gICAgICBjdHgucmVzdG9yZSgpO1xuICAgIH1cblxuICAgIGlmIChlbnYud3hhKSB7XG4gICAgICAvLyBGbHVzaCBmb3Igd2VpeGluIGFwcGxpY2F0aW9uXG4gICAgICB1dGlsLmVhY2godGhpcy5fbGF5ZXJzLCBmdW5jdGlvbiAobGF5ZXIpIHtcbiAgICAgICAgaWYgKGxheWVyICYmIGxheWVyLmN0eCAmJiBsYXllci5jdHguZHJhdykge1xuICAgICAgICAgIGxheWVyLmN0eC5kcmF3KCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHJldHVybiBmaW5pc2hlZDtcbiAgfSxcbiAgX2RvUGFpbnRFbDogZnVuY3Rpb24gKGVsLCBjdXJyZW50TGF5ZXIsIGZvcmNlUGFpbnQsIHNjb3BlKSB7XG4gICAgdmFyIGN0eCA9IGN1cnJlbnRMYXllci5jdHg7XG4gICAgdmFyIG0gPSBlbC50cmFuc2Zvcm07XG5cbiAgICBpZiAoKGN1cnJlbnRMYXllci5fX2RpcnR5IHx8IGZvcmNlUGFpbnQpICYmIC8vIElnbm9yZSBpbnZpc2libGUgZWxlbWVudFxuICAgICFlbC5pbnZpc2libGUgLy8gSWdub3JlIHRyYW5zcGFyZW50IGVsZW1lbnRcbiAgICAmJiBlbC5zdHlsZS5vcGFjaXR5ICE9PSAwIC8vIElnbm9yZSBzY2FsZSAwIGVsZW1lbnQsIGluIHNvbWUgZW52aXJvbm1lbnQgbGlrZSBub2RlLWNhbnZhc1xuICAgIC8vIERyYXcgYSBzY2FsZSAwIGVsZW1lbnQgY2FuIGNhdXNlIGFsbCBmb2xsb3dpbmcgZHJhdyB3cm9uZ1xuICAgIC8vIEFuZCBzZXRUcmFuc2Zvcm0gd2l0aCBzY2FsZSAwIHdpbGwgY2F1c2Ugc2V0IGJhY2sgdHJhbnNmb3JtIGZhaWxlZC5cbiAgICAmJiAhKG0gJiYgIW1bMF0gJiYgIW1bM10pIC8vIElnbm9yZSBjdWxsZWQgZWxlbWVudFxuICAgICYmICEoZWwuY3VsbGluZyAmJiBpc0Rpc3BsYXlhYmxlQ3VsbGVkKGVsLCB0aGlzLl93aWR0aCwgdGhpcy5faGVpZ2h0KSkpIHtcbiAgICAgIHZhciBjbGlwUGF0aHMgPSBlbC5fX2NsaXBQYXRoczsgLy8gT3B0aW1pemUgd2hlbiBjbGlwcGluZyBvbiBncm91cCB3aXRoIHNldmVyYWwgZWxlbWVudHNcblxuICAgICAgaWYgKCFzY29wZS5wcmV2RWxDbGlwUGF0aHMgfHwgaXNDbGlwUGF0aENoYW5nZWQoY2xpcFBhdGhzLCBzY29wZS5wcmV2RWxDbGlwUGF0aHMpKSB7XG4gICAgICAgIC8vIElmIGhhcyBwcmV2aW91cyBjbGlwcGluZyBzdGF0ZSwgcmVzdG9yZSBmcm9tIGl0XG4gICAgICAgIGlmIChzY29wZS5wcmV2RWxDbGlwUGF0aHMpIHtcbiAgICAgICAgICBjdXJyZW50TGF5ZXIuY3R4LnJlc3RvcmUoKTtcbiAgICAgICAgICBzY29wZS5wcmV2RWxDbGlwUGF0aHMgPSBudWxsOyAvLyBSZXNldCBwcmV2RWwgc2luY2UgY29udGV4dCBoYXMgYmVlbiByZXN0b3JlZFxuXG4gICAgICAgICAgc2NvcGUucHJldkVsID0gbnVsbDtcbiAgICAgICAgfSAvLyBOZXcgY2xpcHBpbmcgc3RhdGVcblxuXG4gICAgICAgIGlmIChjbGlwUGF0aHMpIHtcbiAgICAgICAgICBjdHguc2F2ZSgpO1xuICAgICAgICAgIGRvQ2xpcChjbGlwUGF0aHMsIGN0eCk7XG4gICAgICAgICAgc2NvcGUucHJldkVsQ2xpcFBhdGhzID0gY2xpcFBhdGhzO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGVsLmJlZm9yZUJydXNoICYmIGVsLmJlZm9yZUJydXNoKGN0eCk7XG4gICAgICBlbC5icnVzaChjdHgsIHNjb3BlLnByZXZFbCB8fCBudWxsKTtcbiAgICAgIHNjb3BlLnByZXZFbCA9IGVsO1xuICAgICAgZWwuYWZ0ZXJCcnVzaCAmJiBlbC5hZnRlckJydXNoKGN0eCk7XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiDojrflj5YgemxldmVsIOaJgOWcqOWxgu+8jOWmguaenOS4jeWtmOWcqOWImeS8muWIm+W7uuS4gOS4quaWsOeahOWxglxuICAgKiBAcGFyYW0ge251bWJlcn0gemxldmVsXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gdmlydHVhbCBWaXJ0dWFsIGxheWVyIHdpbGwgbm90IGJlIGluc2VydGVkIGludG8gZG9tLlxuICAgKiBAcmV0dXJuIHttb2R1bGU6enJlbmRlci9MYXllcn1cbiAgICovXG4gIGdldExheWVyOiBmdW5jdGlvbiAoemxldmVsLCB2aXJ0dWFsKSB7XG4gICAgaWYgKHRoaXMuX3NpbmdsZUNhbnZhcyAmJiAhdGhpcy5fbmVlZHNNYW51YWxseUNvbXBvc2l0aW5nKSB7XG4gICAgICB6bGV2ZWwgPSBDQU5WQVNfWkxFVkVMO1xuICAgIH1cblxuICAgIHZhciBsYXllciA9IHRoaXMuX2xheWVyc1t6bGV2ZWxdO1xuXG4gICAgaWYgKCFsYXllcikge1xuICAgICAgLy8gQ3JlYXRlIGEgbmV3IGxheWVyXG4gICAgICBsYXllciA9IG5ldyBMYXllcignenJfJyArIHpsZXZlbCwgdGhpcywgdGhpcy5kcHIpO1xuICAgICAgbGF5ZXIuemxldmVsID0gemxldmVsO1xuICAgICAgbGF5ZXIuX19idWlsdGluX18gPSB0cnVlO1xuXG4gICAgICBpZiAodGhpcy5fbGF5ZXJDb25maWdbemxldmVsXSkge1xuICAgICAgICB1dGlsLm1lcmdlKGxheWVyLCB0aGlzLl9sYXllckNvbmZpZ1t6bGV2ZWxdLCB0cnVlKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHZpcnR1YWwpIHtcbiAgICAgICAgbGF5ZXIudmlydHVhbCA9IHZpcnR1YWw7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuaW5zZXJ0TGF5ZXIoemxldmVsLCBsYXllcik7IC8vIENvbnRleHQgaXMgY3JlYXRlZCBhZnRlciBkb20gaW5zZXJ0ZWQgdG8gZG9jdW1lbnRcbiAgICAgIC8vIE9yIGV4Y2FudmFzIHdpbGwgZ2V0IDBweCBjbGllbnRXaWR0aCBhbmQgY2xpZW50SGVpZ2h0XG5cbiAgICAgIGxheWVyLmluaXRDb250ZXh0KCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGxheWVyO1xuICB9LFxuICBpbnNlcnRMYXllcjogZnVuY3Rpb24gKHpsZXZlbCwgbGF5ZXIpIHtcbiAgICB2YXIgbGF5ZXJzTWFwID0gdGhpcy5fbGF5ZXJzO1xuICAgIHZhciB6bGV2ZWxMaXN0ID0gdGhpcy5femxldmVsTGlzdDtcbiAgICB2YXIgbGVuID0gemxldmVsTGlzdC5sZW5ndGg7XG4gICAgdmFyIHByZXZMYXllciA9IG51bGw7XG4gICAgdmFyIGkgPSAtMTtcbiAgICB2YXIgZG9tUm9vdCA9IHRoaXMuX2RvbVJvb3Q7XG5cbiAgICBpZiAobGF5ZXJzTWFwW3psZXZlbF0pIHtcbiAgICAgIGxvZygnWkxldmVsICcgKyB6bGV2ZWwgKyAnIGhhcyBiZWVuIHVzZWQgYWxyZWFkeScpO1xuICAgICAgcmV0dXJuO1xuICAgIH0gLy8gQ2hlY2sgaWYgaXMgYSB2YWxpZCBsYXllclxuXG5cbiAgICBpZiAoIWlzTGF5ZXJWYWxpZChsYXllcikpIHtcbiAgICAgIGxvZygnTGF5ZXIgb2YgemxldmVsICcgKyB6bGV2ZWwgKyAnIGlzIG5vdCB2YWxpZCcpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChsZW4gPiAwICYmIHpsZXZlbCA+IHpsZXZlbExpc3RbMF0pIHtcbiAgICAgIGZvciAoaSA9IDA7IGkgPCBsZW4gLSAxOyBpKyspIHtcbiAgICAgICAgaWYgKHpsZXZlbExpc3RbaV0gPCB6bGV2ZWwgJiYgemxldmVsTGlzdFtpICsgMV0gPiB6bGV2ZWwpIHtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBwcmV2TGF5ZXIgPSBsYXllcnNNYXBbemxldmVsTGlzdFtpXV07XG4gICAgfVxuXG4gICAgemxldmVsTGlzdC5zcGxpY2UoaSArIDEsIDAsIHpsZXZlbCk7XG4gICAgbGF5ZXJzTWFwW3psZXZlbF0gPSBsYXllcjsgLy8gVml0dWFsIGxheWVyIHdpbGwgbm90IGRpcmVjdGx5IHNob3cgb24gdGhlIHNjcmVlbi5cbiAgICAvLyAoSXQgY2FuIGJlIGEgV2ViR0wgbGF5ZXIgYW5kIGFzc2lnbmVkIHRvIGEgWkltYWdlIGVsZW1lbnQpXG4gICAgLy8gQnV0IGl0IHN0aWxsIHVuZGVyIG1hbmFnZW1lbnQgb2YgenJlbmRlci5cblxuICAgIGlmICghbGF5ZXIudmlydHVhbCkge1xuICAgICAgaWYgKHByZXZMYXllcikge1xuICAgICAgICB2YXIgcHJldkRvbSA9IHByZXZMYXllci5kb207XG5cbiAgICAgICAgaWYgKHByZXZEb20ubmV4dFNpYmxpbmcpIHtcbiAgICAgICAgICBkb21Sb290Lmluc2VydEJlZm9yZShsYXllci5kb20sIHByZXZEb20ubmV4dFNpYmxpbmcpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRvbVJvb3QuYXBwZW5kQ2hpbGQobGF5ZXIuZG9tKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGRvbVJvb3QuZmlyc3RDaGlsZCkge1xuICAgICAgICAgIGRvbVJvb3QuaW5zZXJ0QmVmb3JlKGxheWVyLmRvbSwgZG9tUm9vdC5maXJzdENoaWxkKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBkb21Sb290LmFwcGVuZENoaWxkKGxheWVyLmRvbSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIC8vIEl0ZXJhdGUgZWFjaCBsYXllclxuICBlYWNoTGF5ZXI6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIHZhciB6bGV2ZWxMaXN0ID0gdGhpcy5femxldmVsTGlzdDtcbiAgICB2YXIgejtcbiAgICB2YXIgaTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCB6bGV2ZWxMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICB6ID0gemxldmVsTGlzdFtpXTtcbiAgICAgIGNiLmNhbGwoY29udGV4dCwgdGhpcy5fbGF5ZXJzW3pdLCB6KTtcbiAgICB9XG4gIH0sXG4gIC8vIEl0ZXJhdGUgZWFjaCBidWlsZGluIGxheWVyXG4gIGVhY2hCdWlsdGluTGF5ZXI6IGZ1bmN0aW9uIChjYiwgY29udGV4dCkge1xuICAgIHZhciB6bGV2ZWxMaXN0ID0gdGhpcy5femxldmVsTGlzdDtcbiAgICB2YXIgbGF5ZXI7XG4gICAgdmFyIHo7XG4gICAgdmFyIGk7XG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgemxldmVsTGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgeiA9IHpsZXZlbExpc3RbaV07XG4gICAgICBsYXllciA9IHRoaXMuX2xheWVyc1t6XTtcblxuICAgICAgaWYgKGxheWVyLl9fYnVpbHRpbl9fKSB7XG4gICAgICAgIGNiLmNhbGwoY29udGV4dCwgbGF5ZXIsIHopO1xuICAgICAgfVxuICAgIH1cbiAgfSxcbiAgLy8gSXRlcmF0ZSBlYWNoIG90aGVyIGxheWVyIGV4Y2VwdCBidWlsZGluIGxheWVyXG4gIGVhY2hPdGhlckxheWVyOiBmdW5jdGlvbiAoY2IsIGNvbnRleHQpIHtcbiAgICB2YXIgemxldmVsTGlzdCA9IHRoaXMuX3psZXZlbExpc3Q7XG4gICAgdmFyIGxheWVyO1xuICAgIHZhciB6O1xuICAgIHZhciBpO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IHpsZXZlbExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIHogPSB6bGV2ZWxMaXN0W2ldO1xuICAgICAgbGF5ZXIgPSB0aGlzLl9sYXllcnNbel07XG5cbiAgICAgIGlmICghbGF5ZXIuX19idWlsdGluX18pIHtcbiAgICAgICAgY2IuY2FsbChjb250ZXh0LCBsYXllciwgeik7XG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIC8qKlxuICAgKiDojrflj5bmiYDmnInlt7LliJvlu7rnmoTlsYJcbiAgICogQHBhcmFtIHtBcnJheS48bW9kdWxlOnpyZW5kZXIvTGF5ZXI+fSBbcHJldkxheWVyXVxuICAgKi9cbiAgZ2V0TGF5ZXJzOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2xheWVycztcbiAgfSxcbiAgX3VwZGF0ZUxheWVyU3RhdHVzOiBmdW5jdGlvbiAobGlzdCkge1xuICAgIHRoaXMuZWFjaEJ1aWx0aW5MYXllcihmdW5jdGlvbiAobGF5ZXIsIHopIHtcbiAgICAgIGxheWVyLl9fZGlydHkgPSBsYXllci5fX3VzZWQgPSBmYWxzZTtcbiAgICB9KTtcblxuICAgIGZ1bmN0aW9uIHVwZGF0ZVByZXZMYXllcihpZHgpIHtcbiAgICAgIGlmIChwcmV2TGF5ZXIpIHtcbiAgICAgICAgaWYgKHByZXZMYXllci5fX2VuZEluZGV4ICE9PSBpZHgpIHtcbiAgICAgICAgICBwcmV2TGF5ZXIuX19kaXJ0eSA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBwcmV2TGF5ZXIuX19lbmRJbmRleCA9IGlkeDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodGhpcy5fc2luZ2xlQ2FudmFzKSB7XG4gICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGVsID0gbGlzdFtpXTtcblxuICAgICAgICBpZiAoZWwuemxldmVsICE9PSBsaXN0W2kgLSAxXS56bGV2ZWwgfHwgZWwuaW5jcmVtZW50YWwpIHtcbiAgICAgICAgICB0aGlzLl9uZWVkc01hbnVhbGx5Q29tcG9zaXRpbmcgPSB0cnVlO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIHByZXZMYXllciA9IG51bGw7XG4gICAgdmFyIGluY3JlbWVudGFsTGF5ZXJDb3VudCA9IDA7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBlbCA9IGxpc3RbaV07XG4gICAgICB2YXIgemxldmVsID0gZWwuemxldmVsO1xuICAgICAgdmFyIGxheWVyOyAvLyBQRU5ESU5HIElmIGNoYW5nZSBvbmUgaW5jcmVtZW50YWwgZWxlbWVudCBzdHlsZSA/XG4gICAgICAvLyBUT0RPIFdoZXJlIHRoZXJlIGFyZSBub24taW5jcmVtZW50YWwgZWxlbWVudHMgYmV0d2VlbiBpbmNyZW1lbnRhbCBlbGVtZW50cy5cblxuICAgICAgaWYgKGVsLmluY3JlbWVudGFsKSB7XG4gICAgICAgIGxheWVyID0gdGhpcy5nZXRMYXllcih6bGV2ZWwgKyBJTkNSRU1FTlRBTF9JTkMsIHRoaXMuX25lZWRzTWFudWFsbHlDb21wb3NpdGluZyk7XG4gICAgICAgIGxheWVyLmluY3JlbWVudGFsID0gdHJ1ZTtcbiAgICAgICAgaW5jcmVtZW50YWxMYXllckNvdW50ID0gMTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGxheWVyID0gdGhpcy5nZXRMYXllcih6bGV2ZWwgKyAoaW5jcmVtZW50YWxMYXllckNvdW50ID4gMCA/IEVMX0FGVEVSX0lOQ1JFTUVOVEFMX0lOQyA6IDApLCB0aGlzLl9uZWVkc01hbnVhbGx5Q29tcG9zaXRpbmcpO1xuICAgICAgfVxuXG4gICAgICBpZiAoIWxheWVyLl9fYnVpbHRpbl9fKSB7XG4gICAgICAgIGxvZygnWkxldmVsICcgKyB6bGV2ZWwgKyAnIGhhcyBiZWVuIHVzZWQgYnkgdW5rb3duIGxheWVyICcgKyBsYXllci5pZCk7XG4gICAgICB9XG5cbiAgICAgIGlmIChsYXllciAhPT0gcHJldkxheWVyKSB7XG4gICAgICAgIGxheWVyLl9fdXNlZCA9IHRydWU7XG5cbiAgICAgICAgaWYgKGxheWVyLl9fc3RhcnRJbmRleCAhPT0gaSkge1xuICAgICAgICAgIGxheWVyLl9fZGlydHkgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgbGF5ZXIuX19zdGFydEluZGV4ID0gaTtcblxuICAgICAgICBpZiAoIWxheWVyLmluY3JlbWVudGFsKSB7XG4gICAgICAgICAgbGF5ZXIuX19kcmF3SW5kZXggPSBpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIE1hcmsgbGF5ZXIgZHJhdyBpbmRleCBuZWVkcyB0byB1cGRhdGUuXG4gICAgICAgICAgbGF5ZXIuX19kcmF3SW5kZXggPSAtMTtcbiAgICAgICAgfVxuXG4gICAgICAgIHVwZGF0ZVByZXZMYXllcihpKTtcbiAgICAgICAgcHJldkxheWVyID0gbGF5ZXI7XG4gICAgICB9XG5cbiAgICAgIGlmIChlbC5fX2RpcnR5KSB7XG4gICAgICAgIGxheWVyLl9fZGlydHkgPSB0cnVlO1xuXG4gICAgICAgIGlmIChsYXllci5pbmNyZW1lbnRhbCAmJiBsYXllci5fX2RyYXdJbmRleCA8IDApIHtcbiAgICAgICAgICAvLyBTdGFydCBkcmF3IGZyb20gdGhlIGZpcnN0IGRpcnR5IGVsZW1lbnQuXG4gICAgICAgICAgbGF5ZXIuX19kcmF3SW5kZXggPSBpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgdXBkYXRlUHJldkxheWVyKGkpO1xuICAgIHRoaXMuZWFjaEJ1aWx0aW5MYXllcihmdW5jdGlvbiAobGF5ZXIsIHopIHtcbiAgICAgIC8vIFVzZWQgaW4gbGFzdCBmcmFtZSBidXQgbm90IGluIHRoaXMgZnJhbWUuIE5lZWRzIGNsZWFyXG4gICAgICBpZiAoIWxheWVyLl9fdXNlZCAmJiBsYXllci5nZXRFbGVtZW50Q291bnQoKSA+IDApIHtcbiAgICAgICAgbGF5ZXIuX19kaXJ0eSA9IHRydWU7XG4gICAgICAgIGxheWVyLl9fc3RhcnRJbmRleCA9IGxheWVyLl9fZW5kSW5kZXggPSBsYXllci5fX2RyYXdJbmRleCA9IDA7XG4gICAgICB9IC8vIEZvciBpbmNyZW1lbnRhbCBsYXllci4gSW4gY2FzZSBzdGFydCBpbmRleCBjaGFuZ2VkIGFuZCBubyBlbGVtZW50cyBhcmUgZGlydHkuXG5cblxuICAgICAgaWYgKGxheWVyLl9fZGlydHkgJiYgbGF5ZXIuX19kcmF3SW5kZXggPCAwKSB7XG4gICAgICAgIGxheWVyLl9fZHJhd0luZGV4ID0gbGF5ZXIuX19zdGFydEluZGV4O1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxuXG4gIC8qKlxuICAgKiDmuIXpmaRob3ZlcuWxguWkluaJgOacieWGheWuuVxuICAgKi9cbiAgY2xlYXI6IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmVhY2hCdWlsdGluTGF5ZXIodGhpcy5fY2xlYXJMYXllcik7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0sXG4gIF9jbGVhckxheWVyOiBmdW5jdGlvbiAobGF5ZXIpIHtcbiAgICBsYXllci5jbGVhcigpO1xuICB9LFxuICBzZXRCYWNrZ3JvdW5kQ29sb3I6IGZ1bmN0aW9uIChiYWNrZ3JvdW5kQ29sb3IpIHtcbiAgICB0aGlzLl9iYWNrZ3JvdW5kQ29sb3IgPSBiYWNrZ3JvdW5kQ29sb3I7XG4gIH0sXG5cbiAgLyoqXG4gICAqIOS/ruaUueaMh+WumnpsZXZlbOeahOe7mOWItuWPguaVsFxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gemxldmVsXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBjb25maWcg6YWN572u5a+56LGhXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbY29uZmlnLmNsZWFyQ29sb3I9MF0g5q+P5qyh5riF56m655S75biD55qE6aKc6ImyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbY29uZmlnLm1vdGlvbkJsdXI9ZmFsc2VdIOaYr+WQpuW8gOWQr+WKqOaAgeaooeezilxuICAgKiBAcGFyYW0ge251bWJlcn0gW2NvbmZpZy5sYXN0RnJhbWVBbHBoYT0wLjddXG4gICAqICAgICAgICAgICAgICAgICDlnKjlvIDlkK/liqjmgIHmqKHns4rnmoTml7blgJnkvb/nlKjvvIzkuI7kuIrkuIDluKfmt7flkIjnmoRhbHBoYeWAvO+8jOWAvOi2iuWkp+Wwvui/uei2iuaYjuaYvlxuICAgKi9cbiAgY29uZmlnTGF5ZXI6IGZ1bmN0aW9uICh6bGV2ZWwsIGNvbmZpZykge1xuICAgIGlmIChjb25maWcpIHtcbiAgICAgIHZhciBsYXllckNvbmZpZyA9IHRoaXMuX2xheWVyQ29uZmlnO1xuXG4gICAgICBpZiAoIWxheWVyQ29uZmlnW3psZXZlbF0pIHtcbiAgICAgICAgbGF5ZXJDb25maWdbemxldmVsXSA9IGNvbmZpZztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHV0aWwubWVyZ2UobGF5ZXJDb25maWdbemxldmVsXSwgY29uZmlnLCB0cnVlKTtcbiAgICAgIH1cblxuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl96bGV2ZWxMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBfemxldmVsID0gdGhpcy5femxldmVsTGlzdFtpXTtcblxuICAgICAgICBpZiAoX3psZXZlbCA9PT0gemxldmVsIHx8IF96bGV2ZWwgPT09IHpsZXZlbCArIEVMX0FGVEVSX0lOQ1JFTUVOVEFMX0lOQykge1xuICAgICAgICAgIHZhciBsYXllciA9IHRoaXMuX2xheWVyc1tfemxldmVsXTtcbiAgICAgICAgICB1dGlsLm1lcmdlKGxheWVyLCBsYXllckNvbmZpZ1t6bGV2ZWxdLCB0cnVlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICAvKipcbiAgICog5Yig6Zmk5oyH5a6a5bGCXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB6bGV2ZWwg5bGC5omA5Zyo55qEemxldmVsXG4gICAqL1xuICBkZWxMYXllcjogZnVuY3Rpb24gKHpsZXZlbCkge1xuICAgIHZhciBsYXllcnMgPSB0aGlzLl9sYXllcnM7XG4gICAgdmFyIHpsZXZlbExpc3QgPSB0aGlzLl96bGV2ZWxMaXN0O1xuICAgIHZhciBsYXllciA9IGxheWVyc1t6bGV2ZWxdO1xuXG4gICAgaWYgKCFsYXllcikge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGxheWVyLmRvbS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGxheWVyLmRvbSk7XG4gICAgZGVsZXRlIGxheWVyc1t6bGV2ZWxdO1xuICAgIHpsZXZlbExpc3Quc3BsaWNlKHV0aWwuaW5kZXhPZih6bGV2ZWxMaXN0LCB6bGV2ZWwpLCAxKTtcbiAgfSxcblxuICAvKipcbiAgICog5Yy65Z+f5aSn5bCP5Y+Y5YyW5ZCO6YeN57uYXG4gICAqL1xuICByZXNpemU6IGZ1bmN0aW9uICh3aWR0aCwgaGVpZ2h0KSB7XG4gICAgaWYgKCF0aGlzLl9kb21Sb290LnN0eWxlKSB7XG4gICAgICAvLyBNYXliZSBpbiBub2RlIG9yIHdvcmtlclxuICAgICAgaWYgKHdpZHRoID09IG51bGwgfHwgaGVpZ2h0ID09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLl93aWR0aCA9IHdpZHRoO1xuICAgICAgdGhpcy5faGVpZ2h0ID0gaGVpZ2h0O1xuICAgICAgdGhpcy5nZXRMYXllcihDQU5WQVNfWkxFVkVMKS5yZXNpemUod2lkdGgsIGhlaWdodCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBkb21Sb290ID0gdGhpcy5fZG9tUm9vdDsgLy8gRklYTUUgV2h5ID9cblxuICAgICAgZG9tUm9vdC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnOyAvLyBTYXZlIGlucHV0IHcvaFxuXG4gICAgICB2YXIgb3B0cyA9IHRoaXMuX29wdHM7XG4gICAgICB3aWR0aCAhPSBudWxsICYmIChvcHRzLndpZHRoID0gd2lkdGgpO1xuICAgICAgaGVpZ2h0ICE9IG51bGwgJiYgKG9wdHMuaGVpZ2h0ID0gaGVpZ2h0KTtcbiAgICAgIHdpZHRoID0gdGhpcy5fZ2V0U2l6ZSgwKTtcbiAgICAgIGhlaWdodCA9IHRoaXMuX2dldFNpemUoMSk7XG4gICAgICBkb21Sb290LnN0eWxlLmRpc3BsYXkgPSAnJzsgLy8g5LyY5YyW5rKh5pyJ5a6e6ZmF5pS55Y+Y55qEcmVzaXplXG5cbiAgICAgIGlmICh0aGlzLl93aWR0aCAhPT0gd2lkdGggfHwgaGVpZ2h0ICE9PSB0aGlzLl9oZWlnaHQpIHtcbiAgICAgICAgZG9tUm9vdC5zdHlsZS53aWR0aCA9IHdpZHRoICsgJ3B4JztcbiAgICAgICAgZG9tUm9vdC5zdHlsZS5oZWlnaHQgPSBoZWlnaHQgKyAncHgnO1xuXG4gICAgICAgIGZvciAodmFyIGlkIGluIHRoaXMuX2xheWVycykge1xuICAgICAgICAgIGlmICh0aGlzLl9sYXllcnMuaGFzT3duUHJvcGVydHkoaWQpKSB7XG4gICAgICAgICAgICB0aGlzLl9sYXllcnNbaWRdLnJlc2l6ZSh3aWR0aCwgaGVpZ2h0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB1dGlsLmVhY2godGhpcy5fcHJvZ3Jlc3NpdmVMYXllcnMsIGZ1bmN0aW9uIChsYXllcikge1xuICAgICAgICAgIGxheWVyLnJlc2l6ZSh3aWR0aCwgaGVpZ2h0KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMucmVmcmVzaCh0cnVlKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5fd2lkdGggPSB3aWR0aDtcbiAgICAgIHRoaXMuX2hlaWdodCA9IGhlaWdodDtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcztcbiAgfSxcblxuICAvKipcbiAgICog5riF6Zmk5Y2V54us55qE5LiA5Liq5bGCXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB6bGV2ZWxcbiAgICovXG4gIGNsZWFyTGF5ZXI6IGZ1bmN0aW9uICh6bGV2ZWwpIHtcbiAgICB2YXIgbGF5ZXIgPSB0aGlzLl9sYXllcnNbemxldmVsXTtcblxuICAgIGlmIChsYXllcikge1xuICAgICAgbGF5ZXIuY2xlYXIoKTtcbiAgICB9XG4gIH0sXG5cbiAgLyoqXG4gICAqIOmHiuaUvlxuICAgKi9cbiAgZGlzcG9zZTogZnVuY3Rpb24gKCkge1xuICAgIHRoaXMucm9vdC5pbm5lckhUTUwgPSAnJztcbiAgICB0aGlzLnJvb3QgPSB0aGlzLnN0b3JhZ2UgPSB0aGlzLl9kb21Sb290ID0gdGhpcy5fbGF5ZXJzID0gbnVsbDtcbiAgfSxcblxuICAvKipcbiAgICogR2V0IGNhbnZhcyB3aGljaCBoYXMgYWxsIHRoaW5nIHJlbmRlcmVkXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRzXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbb3B0cy5iYWNrZ3JvdW5kQ29sb3JdXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0cy5waXhlbFJhdGlvXVxuICAgKi9cbiAgZ2V0UmVuZGVyZWRDYW52YXM6IGZ1bmN0aW9uIChvcHRzKSB7XG4gICAgb3B0cyA9IG9wdHMgfHwge307XG5cbiAgICBpZiAodGhpcy5fc2luZ2xlQ2FudmFzICYmICF0aGlzLl9jb21wb3NpdGVNYW51YWxseSkge1xuICAgICAgcmV0dXJuIHRoaXMuX2xheWVyc1tDQU5WQVNfWkxFVkVMXS5kb207XG4gICAgfVxuXG4gICAgdmFyIGltYWdlTGF5ZXIgPSBuZXcgTGF5ZXIoJ2ltYWdlJywgdGhpcywgb3B0cy5waXhlbFJhdGlvIHx8IHRoaXMuZHByKTtcbiAgICBpbWFnZUxheWVyLmluaXRDb250ZXh0KCk7XG4gICAgaW1hZ2VMYXllci5jbGVhcihmYWxzZSwgb3B0cy5iYWNrZ3JvdW5kQ29sb3IgfHwgdGhpcy5fYmFja2dyb3VuZENvbG9yKTtcblxuICAgIGlmIChvcHRzLnBpeGVsUmF0aW8gPD0gdGhpcy5kcHIpIHtcbiAgICAgIHRoaXMucmVmcmVzaCgpO1xuICAgICAgdmFyIHdpZHRoID0gaW1hZ2VMYXllci5kb20ud2lkdGg7XG4gICAgICB2YXIgaGVpZ2h0ID0gaW1hZ2VMYXllci5kb20uaGVpZ2h0O1xuICAgICAgdmFyIGN0eCA9IGltYWdlTGF5ZXIuY3R4O1xuICAgICAgdGhpcy5lYWNoTGF5ZXIoZnVuY3Rpb24gKGxheWVyKSB7XG4gICAgICAgIGlmIChsYXllci5fX2J1aWx0aW5fXykge1xuICAgICAgICAgIGN0eC5kcmF3SW1hZ2UobGF5ZXIuZG9tLCAwLCAwLCB3aWR0aCwgaGVpZ2h0KTtcbiAgICAgICAgfSBlbHNlIGlmIChsYXllci5yZW5kZXJUb0NhbnZhcykge1xuICAgICAgICAgIGltYWdlTGF5ZXIuY3R4LnNhdmUoKTtcbiAgICAgICAgICBsYXllci5yZW5kZXJUb0NhbnZhcyhpbWFnZUxheWVyLmN0eCk7XG4gICAgICAgICAgaW1hZ2VMYXllci5jdHgucmVzdG9yZSgpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gUEVORElORywgZWNoYXJ0cy1nbCBhbmQgaW5jcmVtZW50YWwgcmVuZGVyaW5nLlxuICAgICAgdmFyIHNjb3BlID0ge307XG4gICAgICB2YXIgZGlzcGxheUxpc3QgPSB0aGlzLnN0b3JhZ2UuZ2V0RGlzcGxheUxpc3QodHJ1ZSk7XG5cbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZGlzcGxheUxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGVsID0gZGlzcGxheUxpc3RbaV07XG5cbiAgICAgICAgdGhpcy5fZG9QYWludEVsKGVsLCBpbWFnZUxheWVyLCB0cnVlLCBzY29wZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGltYWdlTGF5ZXIuZG9tO1xuICB9LFxuXG4gIC8qKlxuICAgKiDojrflj5bnu5jlm77ljLrln5/lrr3luqZcbiAgICovXG4gIGdldFdpZHRoOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX3dpZHRoO1xuICB9LFxuXG4gIC8qKlxuICAgKiDojrflj5bnu5jlm77ljLrln5/pq5jluqZcbiAgICovXG4gIGdldEhlaWdodDogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLl9oZWlnaHQ7XG4gIH0sXG4gIF9nZXRTaXplOiBmdW5jdGlvbiAod2hJZHgpIHtcbiAgICB2YXIgb3B0cyA9IHRoaXMuX29wdHM7XG4gICAgdmFyIHdoID0gWyd3aWR0aCcsICdoZWlnaHQnXVt3aElkeF07XG4gICAgdmFyIGN3aCA9IFsnY2xpZW50V2lkdGgnLCAnY2xpZW50SGVpZ2h0J11bd2hJZHhdO1xuICAgIHZhciBwbHQgPSBbJ3BhZGRpbmdMZWZ0JywgJ3BhZGRpbmdUb3AnXVt3aElkeF07XG4gICAgdmFyIHByYiA9IFsncGFkZGluZ1JpZ2h0JywgJ3BhZGRpbmdCb3R0b20nXVt3aElkeF07XG5cbiAgICBpZiAob3B0c1t3aF0gIT0gbnVsbCAmJiBvcHRzW3doXSAhPT0gJ2F1dG8nKSB7XG4gICAgICByZXR1cm4gcGFyc2VGbG9hdChvcHRzW3doXSk7XG4gICAgfVxuXG4gICAgdmFyIHJvb3QgPSB0aGlzLnJvb3Q7IC8vIElFOCBkb2VzIG5vdCBzdXBwb3J0IGdldENvbXB1dGVkU3R5bGUsIGJ1dCBpdCB1c2UgVk1MLlxuXG4gICAgdmFyIHN0bCA9IGRvY3VtZW50LmRlZmF1bHRWaWV3LmdldENvbXB1dGVkU3R5bGUocm9vdCk7XG4gICAgcmV0dXJuIChyb290W2N3aF0gfHwgcGFyc2VJbnQxMChzdGxbd2hdKSB8fCBwYXJzZUludDEwKHJvb3Quc3R5bGVbd2hdKSkgLSAocGFyc2VJbnQxMChzdGxbcGx0XSkgfHwgMCkgLSAocGFyc2VJbnQxMChzdGxbcHJiXSkgfHwgMCkgfCAwO1xuICB9LFxuICBwYXRoVG9JbWFnZTogZnVuY3Rpb24gKHBhdGgsIGRwcikge1xuICAgIGRwciA9IGRwciB8fCB0aGlzLmRwcjtcbiAgICB2YXIgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG4gICAgdmFyIGN0eCA9IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpO1xuICAgIHZhciByZWN0ID0gcGF0aC5nZXRCb3VuZGluZ1JlY3QoKTtcbiAgICB2YXIgc3R5bGUgPSBwYXRoLnN0eWxlO1xuICAgIHZhciBzaGFkb3dCbHVyU2l6ZSA9IHN0eWxlLnNoYWRvd0JsdXIgKiBkcHI7XG4gICAgdmFyIHNoYWRvd09mZnNldFggPSBzdHlsZS5zaGFkb3dPZmZzZXRYICogZHByO1xuICAgIHZhciBzaGFkb3dPZmZzZXRZID0gc3R5bGUuc2hhZG93T2Zmc2V0WSAqIGRwcjtcbiAgICB2YXIgbGluZVdpZHRoID0gc3R5bGUuaGFzU3Ryb2tlKCkgPyBzdHlsZS5saW5lV2lkdGggOiAwO1xuICAgIHZhciBsZWZ0TWFyZ2luID0gTWF0aC5tYXgobGluZVdpZHRoIC8gMiwgLXNoYWRvd09mZnNldFggKyBzaGFkb3dCbHVyU2l6ZSk7XG4gICAgdmFyIHJpZ2h0TWFyZ2luID0gTWF0aC5tYXgobGluZVdpZHRoIC8gMiwgc2hhZG93T2Zmc2V0WCArIHNoYWRvd0JsdXJTaXplKTtcbiAgICB2YXIgdG9wTWFyZ2luID0gTWF0aC5tYXgobGluZVdpZHRoIC8gMiwgLXNoYWRvd09mZnNldFkgKyBzaGFkb3dCbHVyU2l6ZSk7XG4gICAgdmFyIGJvdHRvbU1hcmdpbiA9IE1hdGgubWF4KGxpbmVXaWR0aCAvIDIsIHNoYWRvd09mZnNldFkgKyBzaGFkb3dCbHVyU2l6ZSk7XG4gICAgdmFyIHdpZHRoID0gcmVjdC53aWR0aCArIGxlZnRNYXJnaW4gKyByaWdodE1hcmdpbjtcbiAgICB2YXIgaGVpZ2h0ID0gcmVjdC5oZWlnaHQgKyB0b3BNYXJnaW4gKyBib3R0b21NYXJnaW47XG4gICAgY2FudmFzLndpZHRoID0gd2lkdGggKiBkcHI7XG4gICAgY2FudmFzLmhlaWdodCA9IGhlaWdodCAqIGRwcjtcbiAgICBjdHguc2NhbGUoZHByLCBkcHIpO1xuICAgIGN0eC5jbGVhclJlY3QoMCwgMCwgd2lkdGgsIGhlaWdodCk7XG4gICAgY3R4LmRwciA9IGRwcjtcbiAgICB2YXIgcGF0aFRyYW5zZm9ybSA9IHtcbiAgICAgIHBvc2l0aW9uOiBwYXRoLnBvc2l0aW9uLFxuICAgICAgcm90YXRpb246IHBhdGgucm90YXRpb24sXG4gICAgICBzY2FsZTogcGF0aC5zY2FsZVxuICAgIH07XG4gICAgcGF0aC5wb3NpdGlvbiA9IFtsZWZ0TWFyZ2luIC0gcmVjdC54LCB0b3BNYXJnaW4gLSByZWN0LnldO1xuICAgIHBhdGgucm90YXRpb24gPSAwO1xuICAgIHBhdGguc2NhbGUgPSBbMSwgMV07XG4gICAgcGF0aC51cGRhdGVUcmFuc2Zvcm0oKTtcblxuICAgIGlmIChwYXRoKSB7XG4gICAgICBwYXRoLmJydXNoKGN0eCk7XG4gICAgfVxuXG4gICAgdmFyIEltYWdlU2hhcGUgPSBJbWFnZTtcbiAgICB2YXIgaW1nU2hhcGUgPSBuZXcgSW1hZ2VTaGFwZSh7XG4gICAgICBzdHlsZToge1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBpbWFnZTogY2FudmFzXG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBpZiAocGF0aFRyYW5zZm9ybS5wb3NpdGlvbiAhPSBudWxsKSB7XG4gICAgICBpbWdTaGFwZS5wb3NpdGlvbiA9IHBhdGgucG9zaXRpb24gPSBwYXRoVHJhbnNmb3JtLnBvc2l0aW9uO1xuICAgIH1cblxuICAgIGlmIChwYXRoVHJhbnNmb3JtLnJvdGF0aW9uICE9IG51bGwpIHtcbiAgICAgIGltZ1NoYXBlLnJvdGF0aW9uID0gcGF0aC5yb3RhdGlvbiA9IHBhdGhUcmFuc2Zvcm0ucm90YXRpb247XG4gICAgfVxuXG4gICAgaWYgKHBhdGhUcmFuc2Zvcm0uc2NhbGUgIT0gbnVsbCkge1xuICAgICAgaW1nU2hhcGUuc2NhbGUgPSBwYXRoLnNjYWxlID0gcGF0aFRyYW5zZm9ybS5zY2FsZTtcbiAgICB9XG5cbiAgICByZXR1cm4gaW1nU2hhcGU7XG4gIH1cbn07XG52YXIgX2RlZmF1bHQgPSBQYWludGVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7Ozs7QUFJQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7O0FBS0E7QUFDQTs7Ozs7QUFLQTtBQUNBOzs7OztBQUtBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBSkE7QUFBQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTN4QkE7QUE2eEJBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/Painter.js
