

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  }
  result["default"] = mod;
  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var mini_store_1 = __webpack_require__(/*! mini-store */ "./node_modules/mini-store/lib/index.js");

var classnames_1 = __importDefault(__webpack_require__(/*! classnames */ "./node_modules/classnames/index.js"));

function TableHeaderRow(_ref) {
  var row = _ref.row,
      index = _ref.index,
      height = _ref.height,
      components = _ref.components,
      onHeaderRow = _ref.onHeaderRow,
      prefixCls = _ref.prefixCls;
  var HeaderRow = components.header.row;
  var HeaderCell = components.header.cell;
  var rowProps = onHeaderRow(row.map(function (cell) {
    return cell.column;
  }), index);
  var customStyle = rowProps ? rowProps.style : {};

  var style = _objectSpread({
    // https://github.com/ant-design/ant-design/issues/20126
    // https://github.com/ant-design/ant-design/issues/20269
    // https://github.com/ant-design/ant-design/issues/20495
    height: row.length > 1 && index === 0 && height && height !== 'auto' ? parseInt(height.toString(), 10) : height
  }, customStyle);

  return React.createElement(HeaderRow, Object.assign({}, rowProps, {
    style: style
  }), row.map(function (cell, i) {
    var _classnames_1$default;

    var column = cell.column,
        isLast = cell.isLast,
        cellProps = _objectWithoutProperties(cell, ["column", "isLast"]);

    var customProps = column.onHeaderCell ? column.onHeaderCell(column) : {};

    if (column.align) {
      customProps.style = _objectSpread({}, customProps.style, {
        textAlign: column.align
      });
    }

    customProps.className = classnames_1.default(customProps.className, column.className, (_classnames_1$default = {}, _defineProperty(_classnames_1$default, "".concat(prefixCls, "-align-").concat(column.align), !!column.align), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-row-cell-ellipsis"), !!column.ellipsis), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-row-cell-break-word"), !!column.width), _defineProperty(_classnames_1$default, "".concat(prefixCls, "-row-cell-last"), isLast), _classnames_1$default));
    return React.createElement(HeaderCell, Object.assign({}, cellProps, customProps, {
      key: column.key || column.dataIndex || i
    }));
  }));
}

function getRowHeight(state, props) {
  var fixedColumnsHeadRowsHeight = state.fixedColumnsHeadRowsHeight;
  var columns = props.columns,
      rows = props.rows,
      fixed = props.fixed;
  var headerHeight = fixedColumnsHeadRowsHeight[0];

  if (!fixed) {
    return null;
  }

  if (headerHeight && columns) {
    if (headerHeight === 'auto') {
      return 'auto';
    }

    return headerHeight / rows.length;
  }

  return null;
}

exports.default = mini_store_1.connect(function (state, props) {
  return {
    height: getRowHeight(state, props)
  };
})(TableHeaderRow);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdGFibGUvZXMvVGFibGVIZWFkZXJSb3cuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yYy10YWJsZS9lcy9UYWJsZUhlYWRlclJvdy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKTsgdmFyIGtleSwgaTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHNvdXJjZVN5bWJvbEtleXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHNvdXJjZSk7IGZvciAoaSA9IDA7IGkgPCBzb3VyY2VTeW1ib2xLZXlzLmxlbmd0aDsgaSsrKSB7IGtleSA9IHNvdXJjZVN5bWJvbEtleXNbaV07IGlmIChleGNsdWRlZC5pbmRleE9mKGtleSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHNvdXJjZSwga2V5KSkgY29udGludWU7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllc0xvb3NlKHNvdXJjZSwgZXhjbHVkZWQpIHsgaWYgKHNvdXJjZSA9PSBudWxsKSByZXR1cm4ge307IHZhciB0YXJnZXQgPSB7fTsgdmFyIHNvdXJjZUtleXMgPSBPYmplY3Qua2V5cyhzb3VyY2UpOyB2YXIga2V5LCBpOyBmb3IgKGkgPSAwOyBpIDwgc291cmNlS2V5cy5sZW5ndGg7IGkrKykgeyBrZXkgPSBzb3VyY2VLZXlzW2ldOyBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlOyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKE9iamVjdChzb3VyY2UpLCB0cnVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgX2RlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBzb3VyY2Vba2V5XSk7IH0pOyB9IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMoc291cmNlKSk7IH0gZWxzZSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSkpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBrZXkpKTsgfSk7IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbnZhciBfX2ltcG9ydFN0YXIgPSB0aGlzICYmIHRoaXMuX19pbXBvcnRTdGFyIHx8IGZ1bmN0aW9uIChtb2QpIHtcbiAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcbiAgdmFyIHJlc3VsdCA9IHt9O1xuICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSB7XG4gICAgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcbiAgfVxuICByZXN1bHRbXCJkZWZhdWx0XCJdID0gbW9kO1xuICByZXR1cm4gcmVzdWx0O1xufTtcblxudmFyIF9faW1wb3J0RGVmYXVsdCA9IHRoaXMgJiYgdGhpcy5fX2ltcG9ydERlZmF1bHQgfHwgZnVuY3Rpb24gKG1vZCkge1xuICByZXR1cm4gbW9kICYmIG1vZC5fX2VzTW9kdWxlID8gbW9kIDoge1xuICAgIFwiZGVmYXVsdFwiOiBtb2RcbiAgfTtcbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBSZWFjdCA9IF9faW1wb3J0U3RhcihyZXF1aXJlKFwicmVhY3RcIikpO1xuXG52YXIgbWluaV9zdG9yZV8xID0gcmVxdWlyZShcIm1pbmktc3RvcmVcIik7XG5cbnZhciBjbGFzc25hbWVzXzEgPSBfX2ltcG9ydERlZmF1bHQocmVxdWlyZShcImNsYXNzbmFtZXNcIikpO1xuXG5mdW5jdGlvbiBUYWJsZUhlYWRlclJvdyhfcmVmKSB7XG4gIHZhciByb3cgPSBfcmVmLnJvdyxcbiAgICAgIGluZGV4ID0gX3JlZi5pbmRleCxcbiAgICAgIGhlaWdodCA9IF9yZWYuaGVpZ2h0LFxuICAgICAgY29tcG9uZW50cyA9IF9yZWYuY29tcG9uZW50cyxcbiAgICAgIG9uSGVhZGVyUm93ID0gX3JlZi5vbkhlYWRlclJvdyxcbiAgICAgIHByZWZpeENscyA9IF9yZWYucHJlZml4Q2xzO1xuICB2YXIgSGVhZGVyUm93ID0gY29tcG9uZW50cy5oZWFkZXIucm93O1xuICB2YXIgSGVhZGVyQ2VsbCA9IGNvbXBvbmVudHMuaGVhZGVyLmNlbGw7XG4gIHZhciByb3dQcm9wcyA9IG9uSGVhZGVyUm93KHJvdy5tYXAoZnVuY3Rpb24gKGNlbGwpIHtcbiAgICByZXR1cm4gY2VsbC5jb2x1bW47XG4gIH0pLCBpbmRleCk7XG4gIHZhciBjdXN0b21TdHlsZSA9IHJvd1Byb3BzID8gcm93UHJvcHMuc3R5bGUgOiB7fTtcblxuICB2YXIgc3R5bGUgPSBfb2JqZWN0U3ByZWFkKHtcbiAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vYW50LWRlc2lnbi9hbnQtZGVzaWduL2lzc3Vlcy8yMDEyNlxuICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzIwMjY5XG4gICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2FudC1kZXNpZ24vYW50LWRlc2lnbi9pc3N1ZXMvMjA0OTVcbiAgICBoZWlnaHQ6IHJvdy5sZW5ndGggPiAxICYmIGluZGV4ID09PSAwICYmIGhlaWdodCAmJiBoZWlnaHQgIT09ICdhdXRvJyA/IHBhcnNlSW50KGhlaWdodC50b1N0cmluZygpLCAxMCkgOiBoZWlnaHRcbiAgfSwgY3VzdG9tU3R5bGUpO1xuXG4gIHJldHVybiBSZWFjdC5jcmVhdGVFbGVtZW50KEhlYWRlclJvdywgT2JqZWN0LmFzc2lnbih7fSwgcm93UHJvcHMsIHtcbiAgICBzdHlsZTogc3R5bGVcbiAgfSksIHJvdy5tYXAoZnVuY3Rpb24gKGNlbGwsIGkpIHtcbiAgICB2YXIgX2NsYXNzbmFtZXNfMSRkZWZhdWx0O1xuXG4gICAgdmFyIGNvbHVtbiA9IGNlbGwuY29sdW1uLFxuICAgICAgICBpc0xhc3QgPSBjZWxsLmlzTGFzdCxcbiAgICAgICAgY2VsbFByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKGNlbGwsIFtcImNvbHVtblwiLCBcImlzTGFzdFwiXSk7XG5cbiAgICB2YXIgY3VzdG9tUHJvcHMgPSBjb2x1bW4ub25IZWFkZXJDZWxsID8gY29sdW1uLm9uSGVhZGVyQ2VsbChjb2x1bW4pIDoge307XG5cbiAgICBpZiAoY29sdW1uLmFsaWduKSB7XG4gICAgICBjdXN0b21Qcm9wcy5zdHlsZSA9IF9vYmplY3RTcHJlYWQoe30sIGN1c3RvbVByb3BzLnN0eWxlLCB7XG4gICAgICAgIHRleHRBbGlnbjogY29sdW1uLmFsaWduXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBjdXN0b21Qcm9wcy5jbGFzc05hbWUgPSBjbGFzc25hbWVzXzEuZGVmYXVsdChjdXN0b21Qcm9wcy5jbGFzc05hbWUsIGNvbHVtbi5jbGFzc05hbWUsIChfY2xhc3NuYW1lc18xJGRlZmF1bHQgPSB7fSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzXzEkZGVmYXVsdCwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1hbGlnbi1cIikuY29uY2F0KGNvbHVtbi5hbGlnbiksICEhY29sdW1uLmFsaWduKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzXzEkZGVmYXVsdCwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1yb3ctY2VsbC1lbGxpcHNpc1wiKSwgISFjb2x1bW4uZWxsaXBzaXMpLCBfZGVmaW5lUHJvcGVydHkoX2NsYXNzbmFtZXNfMSRkZWZhdWx0LCBcIlwiLmNvbmNhdChwcmVmaXhDbHMsIFwiLXJvdy1jZWxsLWJyZWFrLXdvcmRcIiksICEhY29sdW1uLndpZHRoKSwgX2RlZmluZVByb3BlcnR5KF9jbGFzc25hbWVzXzEkZGVmYXVsdCwgXCJcIi5jb25jYXQocHJlZml4Q2xzLCBcIi1yb3ctY2VsbC1sYXN0XCIpLCBpc0xhc3QpLCBfY2xhc3NuYW1lc18xJGRlZmF1bHQpKTtcbiAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChIZWFkZXJDZWxsLCBPYmplY3QuYXNzaWduKHt9LCBjZWxsUHJvcHMsIGN1c3RvbVByb3BzLCB7XG4gICAgICBrZXk6IGNvbHVtbi5rZXkgfHwgY29sdW1uLmRhdGFJbmRleCB8fCBpXG4gICAgfSkpO1xuICB9KSk7XG59XG5cbmZ1bmN0aW9uIGdldFJvd0hlaWdodChzdGF0ZSwgcHJvcHMpIHtcbiAgdmFyIGZpeGVkQ29sdW1uc0hlYWRSb3dzSGVpZ2h0ID0gc3RhdGUuZml4ZWRDb2x1bW5zSGVhZFJvd3NIZWlnaHQ7XG4gIHZhciBjb2x1bW5zID0gcHJvcHMuY29sdW1ucyxcbiAgICAgIHJvd3MgPSBwcm9wcy5yb3dzLFxuICAgICAgZml4ZWQgPSBwcm9wcy5maXhlZDtcbiAgdmFyIGhlYWRlckhlaWdodCA9IGZpeGVkQ29sdW1uc0hlYWRSb3dzSGVpZ2h0WzBdO1xuXG4gIGlmICghZml4ZWQpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGlmIChoZWFkZXJIZWlnaHQgJiYgY29sdW1ucykge1xuICAgIGlmIChoZWFkZXJIZWlnaHQgPT09ICdhdXRvJykge1xuICAgICAgcmV0dXJuICdhdXRvJztcbiAgICB9XG5cbiAgICByZXR1cm4gaGVhZGVySGVpZ2h0IC8gcm93cy5sZW5ndGg7XG4gIH1cblxuICByZXR1cm4gbnVsbDtcbn1cblxuZXhwb3J0cy5kZWZhdWx0ID0gbWluaV9zdG9yZV8xLmNvbm5lY3QoZnVuY3Rpb24gKHN0YXRlLCBwcm9wcykge1xuICByZXR1cm4ge1xuICAgIGhlaWdodDogZ2V0Um93SGVpZ2h0KHN0YXRlLCBwcm9wcylcbiAgfTtcbn0pKFRhYmxlSGVhZGVyUm93KTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQ0E7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-table/es/TableHeaderRow.js
