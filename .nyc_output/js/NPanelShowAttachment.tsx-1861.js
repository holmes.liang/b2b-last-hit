__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_attachment_attachment_view__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/attachment/attachment-view */ "./src/app/desk/component/attachment/attachment-view.tsx");

var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/NPanelShowAttachment.tsx";







var NPanelShowAttachment = function NPanelShowAttachment(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(true),
      _useState2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_useState, 2),
      isShow = _useState2[0],
      changeShow = _useState2[1];

  var handelAfterFetchData = function handelAfterFetchData(data) {
    if (lodash__WEBPACK_IMPORTED_MODULE_2___default.a.isEmpty(data)) changeShow(false);
  };

  var that = props.that;

  var getProps = function getProps(propName) {
    var dataFixed = props.dataFixed;

    if (dataFixed) {
      return "".concat(dataFixed, ".").concat(propName);
    } else {
      return propName;
    }
  };

  if (isShow) {
    return _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_app_component__WEBPACK_IMPORTED_MODULE_3__["NPanel"], Object.assign({}, props, {
      header: _common__WEBPACK_IMPORTED_MODULE_5__["Language"].en("Attachments").thai("สิ่งที่แนบมา").my("ချိတ်တွဲမှုများ").getMessage(),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27
      },
      __self: this
    }), _common_3rd__WEBPACK_IMPORTED_MODULE_4__["React"].createElement(_desk_component_attachment_attachment_view__WEBPACK_IMPORTED_MODULE_6__["default"], {
      afterGetData: function afterGetData(data) {
        handelAfterFetchData(data);
      },
      policyId: that.getValueFromModel(getProps("policyId")),
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30
      },
      __self: this
    }));
  } else return null;
};

/* harmony default export */ __webpack_exports__["default"] = (NPanelShowAttachment);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L05QYW5lbFNob3dBdHRhY2htZW50LnRzeC5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvc3JjL2FwcC9kZXNrL2NvbXBvbmVudC9OUGFuZWxTaG93QXR0YWNobWVudC50c3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBfIGZyb20gXCJsb2Rhc2hcIjtcbmltcG9ydCB7IE5QYW5lbCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgUmVhY3QgfSBmcm9tIFwiQGNvbW1vbi8zcmRcIjtcbmltcG9ydCB7IExhbmd1YWdlIH0gZnJvbSBcIkBjb21tb25cIjtcbmltcG9ydCBBdHRhY2htZW50VmlldyBmcm9tIFwiQGRlc2stY29tcG9uZW50L2F0dGFjaG1lbnQvYXR0YWNobWVudC12aWV3XCI7XG5cbmNvbnN0IE5QYW5lbFNob3dBdHRhY2htZW50ID0gKHByb3BzOiBhbnkpID0+IHtcbiAgY29uc3QgW2lzU2hvdywgY2hhbmdlU2hvd10gPSB1c2VTdGF0ZSh0cnVlKTtcblxuICBjb25zdCBoYW5kZWxBZnRlckZldGNoRGF0YSA9IChkYXRhOiBhbnkpID0+IHtcbiAgICBpZiAoXy5pc0VtcHR5KGRhdGEpKSBjaGFuZ2VTaG93KGZhbHNlKTtcbiAgfTtcblxuICBjb25zdCB0aGF0OiBhbnkgPSBwcm9wcy50aGF0O1xuXG4gIGNvbnN0IGdldFByb3BzID0gKHByb3BOYW1lOiBhbnkpID0+IHtcbiAgICBjb25zdCB7IGRhdGFGaXhlZCB9ID0gcHJvcHM7XG4gICAgaWYgKGRhdGFGaXhlZCkge1xuICAgICAgcmV0dXJuIGAke2RhdGFGaXhlZH0uJHtwcm9wTmFtZX1gO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcHJvcE5hbWU7XG4gICAgfVxuXG4gIH07XG4gIGlmIChpc1Nob3cpIHtcbiAgICByZXR1cm4gPE5QYW5lbFxuICAgICAgey4uLnByb3BzfVxuICAgICAgaGVhZGVyPXtMYW5ndWFnZS5lbihcIkF0dGFjaG1lbnRzXCIpLnRoYWkoXCLguKrguLTguYjguIfguJfguLXguYjguYHguJnguJrguKHguLJcIikubXkoXCLhgIHhgLvhgK3hgJDhgLrhgJDhgL3hgLLhgJnhgL7hgK/hgJnhgLvhgKzhgLhcIikuZ2V0TWVzc2FnZSgpfT5cbiAgICAgIDxBdHRhY2htZW50Vmlld1xuICAgICAgICBhZnRlckdldERhdGE9e1xuICAgICAgICAgIChkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGhhbmRlbEFmdGVyRmV0Y2hEYXRhKGRhdGEpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBwb2xpY3lJZD17dGhhdC5nZXRWYWx1ZUZyb21Nb2RlbChnZXRQcm9wcyhcInBvbGljeUlkXCIpKX0vPlxuXG4gICAgPC9OUGFuZWw+O1xuICB9IGVsc2UgcmV0dXJuIG51bGw7XG59O1xuZXhwb3J0IGRlZmF1bHQgTlBhbmVsU2hvd0F0dGFjaG1lbnQ7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQUVBO0FBQ0E7QUFFQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNBO0FBQ0E7QUFDQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/component/NPanelShowAttachment.tsx
