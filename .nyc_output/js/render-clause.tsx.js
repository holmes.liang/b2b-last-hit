__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return RenderClause; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @app-component */ "./src/app/component/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_react_quill__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @desk-component/react-quill */ "./src/app/desk/component/react-quill/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_11__);







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/render-clause.tsx";






var RenderClause =
/*#__PURE__*/
function (_Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(RenderClause, _Component);

  function RenderClause(props) {
    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, RenderClause);

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(RenderClause).call(this, props));

    _this.getTemplate = function (tplId) {
      var _this$props = _this.props,
          generatePropName = _this$props.generatePropName,
          that = _this$props.that;
      _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].get(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].TPL_CONTENT.replace(":tplId", tplId), {}, {
        loading: true
      }).then(function (res) {
        var content = lodash__WEBPACK_IMPORTED_MODULE_11___default.a.get(res, "body.respData") || "";
        that.setValueToModel(content, generatePropName("specialClauses"));

        _this.props.form.setFieldsValue(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__["default"])({}, generatePropName("specialClauses"), content));
      });
    };

    _this.requestFullScreen = _this.requestFullScreen.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this)); //进入全屏

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(RenderClause, [{
    key: "requestFullScreen",
    value: function requestFullScreen() {
      var dom = document.getElementsByClassName("quill")[0] || {}; //绑定想要全屏的组件

      if (dom.requestFullscreen) {
        dom.requestFullscreen();
      } else if (dom.mozRequestFullScreen) {
        dom.mozRequestFullScreen();
      } else if (dom.webkitRequestFullScreen) {
        dom.webkitRequestFullScreen();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          form = _this$props2.form,
          model = _this$props2.model,
          getProp = _this$props2.getProp,
          generatePropName = _this$props2.generatePropName,
          that = _this$props2.that;
      var policyId = that.getValueFromModel(getProp("policyId"));
      return react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_7___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_app_component__WEBPACK_IMPORTED_MODULE_8__["NSelect"], {
        form: form,
        size: "large",
        model: model,
        propName: generatePropName("specialClausesTemplate"),
        label: _common__WEBPACK_IMPORTED_MODULE_9__["Language"].en("SELECT TEMPLATE").thai("SELECT TEMPLATE").getMessage(),
        fetchOptions: _common__WEBPACK_IMPORTED_MODULE_9__["Ajax"].post(_common__WEBPACK_IMPORTED_MODULE_9__["Apis"].TPL_LIST, {
          tplType: _common__WEBPACK_IMPORTED_MODULE_9__["Consts"].TPL_TYPE.EXTENDED_CLAUSE,
          productCodes: ["IAR"]
        }),
        onChange: function onChange(value) {
          _this2.getTemplate(value);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement(_desk_component_react_quill__WEBPACK_IMPORTED_MODULE_10__["default"] // label={Language.en("CLAUSE TEXTS")
      //   .thai("CLAUSE TEXTS")
      //   .getMessage()}
      , {
        showLabel: false,
        textValue: that.getValueFromModel(generatePropName("specialClauses")) || "",
        onChangeContext: function onChangeContext(value) {
          that.setValueToModel(value, generatePropName("specialClauses"));
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }));
    }
  }]);

  return RenderClause;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1jbGF1c2UudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L3JlbmRlci1jbGF1c2UudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IE5QYW5lbCwgTlNlbGVjdCB9IGZyb20gXCJAYXBwLWNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWpheCwgQXBpcywgQ29uc3RzLCBMYW5ndWFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgUmVhY3RRdWlsbENvbnRlbnQgZnJvbSBcIkBkZXNrLWNvbXBvbmVudC9yZWFjdC1xdWlsbFwiO1xuaW1wb3J0IHsgQWpheFJlc3BvbnNlIH0gZnJvbSBcIkBteS10eXBlc1wiO1xuaW1wb3J0IF8gZnJvbSBcImxvZGFzaFwiO1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgZm9ybTogYW55LFxuICBtb2RlbDogYW55LFxuICBnZXRQcm9wOiBhbnksXG4gIGdlbmVyYXRlUHJvcE5hbWU6IGFueSxcbiAgdGhhdDogYW55LFxufVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZW5kZXJDbGF1c2UgZXh0ZW5kcyBDb21wb25lbnQ8SVByb3BzLCBhbnk+IHtcbiAgY29uc3RydWN0b3IocHJvcHM6IGFueSkge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnJlcXVlc3RGdWxsU2NyZWVuID0gdGhpcy5yZXF1ZXN0RnVsbFNjcmVlbi5iaW5kKHRoaXMpOy8v6L+b5YWl5YWo5bGPXG4gIH1cblxuICBnZXRUZW1wbGF0ZSA9ICh0cGxJZDogYW55KSA9PiB7XG4gICAgY29uc3Qge1xuICAgICAgZ2VuZXJhdGVQcm9wTmFtZSxcbiAgICAgIHRoYXQsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgQWpheC5nZXQoQXBpcy5UUExfQ09OVEVOVC5yZXBsYWNlKFwiOnRwbElkXCIsIHRwbElkKSwge30sIHsgbG9hZGluZzogdHJ1ZSB9KVxuICAgICAgLnRoZW4oKHJlczogQWpheFJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGNvbnN0IGNvbnRlbnQgPSBfLmdldChyZXMsIFwiYm9keS5yZXNwRGF0YVwiKSB8fCBcIlwiO1xuICAgICAgICB0aGF0LnNldFZhbHVlVG9Nb2RlbChjb250ZW50LCBnZW5lcmF0ZVByb3BOYW1lKFwic3BlY2lhbENsYXVzZXNcIikpO1xuICAgICAgICB0aGlzLnByb3BzLmZvcm0uc2V0RmllbGRzVmFsdWUoe1xuICAgICAgICAgIFtnZW5lcmF0ZVByb3BOYW1lKFwic3BlY2lhbENsYXVzZXNcIildOiBjb250ZW50LFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9O1xuXG4gIHJlcXVlc3RGdWxsU2NyZWVuKCkge1xuICAgIHZhciBkb206IGFueSA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJxdWlsbFwiKVswXSB8fCB7fTsvL+e7keWumuaDs+imgeWFqOWxj+eahOe7hOS7tlxuICAgIGlmIChkb20ucmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgIGRvbS5yZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAoZG9tLm1velJlcXVlc3RGdWxsU2NyZWVuKSB7XG4gICAgICBkb20ubW96UmVxdWVzdEZ1bGxTY3JlZW4oKTtcbiAgICB9IGVsc2UgaWYgKGRvbS53ZWJraXRSZXF1ZXN0RnVsbFNjcmVlbikge1xuICAgICAgZG9tLndlYmtpdFJlcXVlc3RGdWxsU2NyZWVuKCk7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICBmb3JtLFxuICAgICAgbW9kZWwsXG4gICAgICBnZXRQcm9wLFxuICAgICAgZ2VuZXJhdGVQcm9wTmFtZSxcbiAgICAgIHRoYXQsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgcG9saWN5SWQgPSB0aGF0LmdldFZhbHVlRnJvbU1vZGVsKGdldFByb3AoXCJwb2xpY3lJZFwiKSk7XG4gICAgcmV0dXJuIDw+XG4gICAgICA8TlNlbGVjdCBmb3JtPXtmb3JtfVxuICAgICAgICAgICAgICAgc2l6ZT17XCJsYXJnZVwifVxuICAgICAgICAgICAgICAgbW9kZWw9e21vZGVsfVxuICAgICAgICAgICAgICAgcHJvcE5hbWU9e2dlbmVyYXRlUHJvcE5hbWUoXCJzcGVjaWFsQ2xhdXNlc1RlbXBsYXRlXCIpfVxuICAgICAgICAgICAgICAgbGFiZWw9e0xhbmd1YWdlLmVuKFwiU0VMRUNUIFRFTVBMQVRFXCIpXG4gICAgICAgICAgICAgICAgIC50aGFpKFwiU0VMRUNUIFRFTVBMQVRFXCIpXG4gICAgICAgICAgICAgICAgIC5nZXRNZXNzYWdlKCl9XG4gICAgICAgICAgICAgICBmZXRjaE9wdGlvbnM9e0FqYXgucG9zdChBcGlzLlRQTF9MSVNULCB7XG4gICAgICAgICAgICAgICAgIHRwbFR5cGU6IENvbnN0cy5UUExfVFlQRS5FWFRFTkRFRF9DTEFVU0UsXG4gICAgICAgICAgICAgICAgIHByb2R1Y3RDb2RlczogW1wiSUFSXCJdLFxuICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICBvbkNoYW5nZT17KHZhbHVlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgdGhpcy5nZXRUZW1wbGF0ZSh2YWx1ZSk7XG4gICAgICAgICAgICAgICB9fS8+XG4gICAgICA8UmVhY3RRdWlsbENvbnRlbnRcbiAgICAgICAgLy8gbGFiZWw9e0xhbmd1YWdlLmVuKFwiQ0xBVVNFIFRFWFRTXCIpXG4gICAgICAgIC8vICAgLnRoYWkoXCJDTEFVU0UgVEVYVFNcIilcbiAgICAgICAgLy8gICAuZ2V0TWVzc2FnZSgpfVxuICAgICAgICBzaG93TGFiZWw9e2ZhbHNlfVxuICAgICAgICB0ZXh0VmFsdWU9e3RoYXQuZ2V0VmFsdWVGcm9tTW9kZWwoZ2VuZXJhdGVQcm9wTmFtZShcInNwZWNpYWxDbGF1c2VzXCIpKSB8fCBcIlwifVxuICAgICAgICBvbkNoYW5nZUNvbnRleHQ9eyh2YWx1ZTogYW55KSA9PiB7XG4gICAgICAgICAgdGhhdC5zZXRWYWx1ZVRvTW9kZWwodmFsdWUsIGdlbmVyYXRlUHJvcE5hbWUoXCJzcGVjaWFsQ2xhdXNlc1wiKSk7XG4gICAgICAgIH19Lz5cbiAgICA8Lz47XG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBU0E7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUtBO0FBQUE7QUFBQTtBQUtBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBakJBO0FBQ0E7QUFIQTtBQUdBO0FBQ0E7OztBQWdCQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZ0JBO0FBQ0E7QUFIQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTs7OztBQWxFQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/render-clause.tsx
