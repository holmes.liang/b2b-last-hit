__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");


var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/component/spin.tsx";




var SpinLoading =
/*#__PURE__*/
function () {
  function SpinLoading() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, SpinLoading);

    this.spinElementId = "spinElement";
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(SpinLoading, [{
    key: "spinShow",
    value: function spinShow(wrapClassName, opacityNum) {
      var el = document.getElementById(this.spinElementId);
      var opacity = opacityNum ? opacityNum / 10 : 0;

      if (!el) {
        var div = document.createElement("div");
        div.style.width = "100%";
        div.style.height = "100%";
        div.style.top = "0px";
        div.style.left = "0px";
        div.style.position = "fixed";
        div.style.display = "block"; // div.style.zIndex = "10";

        div.style.zIndex = "10000";
        div.style.backgroundColor = "rgba(0,0,0,".concat(opacity, ")");
        div.id = this.spinElementId; // let appendDiv = document.querySelectorAll("[data-widget='page-header']");
        // if (
        //   appendDiv.length > 0 &&
        //   appendDiv[0].parentNode &&
        //   appendDiv[0].parentNode.children &&
        //   appendDiv[0].parentNode.children.length > 1
        // ) {
        //   // appendDiv[0].parentNode.children[1].append(div);
        //   document.body.append(div);
        // } else {
        //   document.body.append(div);
        // }

        if (wrapClassName) {
          var divParent = document.getElementsByClassName(wrapClassName);

          if (divParent && divParent.length > 0) {
            divParent[0].append(div);
          }
        } else {
          document.body.append(div);
        }
      } else {
        el.style.display = "block";
      }

      react_dom__WEBPACK_IMPORTED_MODULE_3___default.a.render(react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(antd__WEBPACK_IMPORTED_MODULE_4__["Spin"], {
        size: "large",
        style: {
          height: "calc(100%)",
          width: "calc(100%)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          zIndex: 10001,
          position: "relative",
          top: 0
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }), document.getElementById(this.spinElementId));
    }
  }, {
    key: "spinClose",
    value: function spinClose() {
      var el = document.getElementById(this.spinElementId);

      if (el) {
        setTimeout(function () {
          el.style.display = "none";
          react_dom__WEBPACK_IMPORTED_MODULE_3___default.a.unmountComponentAtNode(el);
        }, 200);
      }
    }
  }]);

  return SpinLoading;
}();

/* harmony default export */ __webpack_exports__["default"] = (new SpinLoading());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3NwaW4udHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svY29tcG9uZW50L3NwaW4udHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBSZWFjdERPTSBmcm9tIFwicmVhY3QtZG9tXCI7XG5pbXBvcnQgeyBTcGluIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCAqIGFzIFN0eWxlZEZ1bmN0aW9ucyBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIjtcblxuZXhwb3J0IHR5cGUgU3R5bGVkRElWID0gU3R5bGVkRnVuY3Rpb25zLlN0eWxlZENvbXBvbmVudDxcImRpdlwiLCBhbnksIHt9LCBuZXZlcj47XG5cbmNsYXNzIFNwaW5Mb2FkaW5nIHtcbiAgc3BpbkVsZW1lbnRJZCA9IFwic3BpbkVsZW1lbnRcIjtcblxuICBzcGluU2hvdyh3cmFwQ2xhc3NOYW1lPzogc3RyaW5nLCBvcGFjaXR5TnVtPzogbnVtYmVyKSB7XG4gICAgY29uc3QgZWwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0aGlzLnNwaW5FbGVtZW50SWQpO1xuICAgIGNvbnN0IG9wYWNpdHkgPSBvcGFjaXR5TnVtID8gb3BhY2l0eU51bSAvIDEwIDogMDtcbiAgICBpZiAoIWVsKSB7XG4gICAgICBjb25zdCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgICAgZGl2LnN0eWxlLndpZHRoID0gXCIxMDAlXCI7XG4gICAgICBkaXYuc3R5bGUuaGVpZ2h0ID0gXCIxMDAlXCI7XG4gICAgICBkaXYuc3R5bGUudG9wID0gXCIwcHhcIjtcbiAgICAgIGRpdi5zdHlsZS5sZWZ0ID0gXCIwcHhcIjtcbiAgICAgIGRpdi5zdHlsZS5wb3NpdGlvbiA9IFwiZml4ZWRcIjtcbiAgICAgIGRpdi5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuICAgICAgLy8gZGl2LnN0eWxlLnpJbmRleCA9IFwiMTBcIjtcbiAgICAgIGRpdi5zdHlsZS56SW5kZXggPSBcIjEwMDAwXCI7XG4gICAgICBkaXYuc3R5bGUuYmFja2dyb3VuZENvbG9yID0gYHJnYmEoMCwwLDAsJHtvcGFjaXR5fSlgO1xuICAgICAgZGl2LmlkID0gdGhpcy5zcGluRWxlbWVudElkO1xuICAgICAgLy8gbGV0IGFwcGVuZERpdiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS13aWRnZXQ9J3BhZ2UtaGVhZGVyJ11cIik7XG4gICAgICAvLyBpZiAoXG4gICAgICAvLyAgIGFwcGVuZERpdi5sZW5ndGggPiAwICYmXG4gICAgICAvLyAgIGFwcGVuZERpdlswXS5wYXJlbnROb2RlICYmXG4gICAgICAvLyAgIGFwcGVuZERpdlswXS5wYXJlbnROb2RlLmNoaWxkcmVuICYmXG4gICAgICAvLyAgIGFwcGVuZERpdlswXS5wYXJlbnROb2RlLmNoaWxkcmVuLmxlbmd0aCA+IDFcbiAgICAgIC8vICkge1xuICAgICAgLy8gICAvLyBhcHBlbmREaXZbMF0ucGFyZW50Tm9kZS5jaGlsZHJlblsxXS5hcHBlbmQoZGl2KTtcbiAgICAgIC8vICAgZG9jdW1lbnQuYm9keS5hcHBlbmQoZGl2KTtcbiAgICAgIC8vIH0gZWxzZSB7XG4gICAgICAvLyAgIGRvY3VtZW50LmJvZHkuYXBwZW5kKGRpdik7XG4gICAgICAvLyB9XG4gICAgICBpZiAod3JhcENsYXNzTmFtZSkge1xuICAgICAgICBjb25zdCBkaXZQYXJlbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKHdyYXBDbGFzc05hbWUpO1xuICAgICAgICBpZiAoZGl2UGFyZW50ICYmIGRpdlBhcmVudC5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgZGl2UGFyZW50WzBdLmFwcGVuZChkaXYpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZChkaXYpO1xuICAgICAgfVxuXG4gICAgfSBlbHNlIHtcbiAgICAgIGVsLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XG4gICAgfVxuICAgIFJlYWN0RE9NLnJlbmRlcihcbiAgICAgIDxTcGluXG4gICAgICAgIHNpemU9XCJsYXJnZVwiXG4gICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgaGVpZ2h0OiBcImNhbGMoMTAwJSlcIixcbiAgICAgICAgICB3aWR0aDogXCJjYWxjKDEwMCUpXCIsXG4gICAgICAgICAgZGlzcGxheTogXCJmbGV4XCIsXG4gICAgICAgICAganVzdGlmeUNvbnRlbnQ6IFwiY2VudGVyXCIsXG4gICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcbiAgICAgICAgICB6SW5kZXg6IDEwMDAxLFxuICAgICAgICAgIHBvc2l0aW9uOiBcInJlbGF0aXZlXCIsXG4gICAgICAgICAgdG9wOiAwLFxuICAgICAgICB9fVxuICAgICAgLz4sXG4gICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0aGlzLnNwaW5FbGVtZW50SWQpLFxuICAgICk7XG4gIH1cblxuICBzcGluQ2xvc2UoKSB7XG4gICAgY29uc3QgZWwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0aGlzLnNwaW5FbGVtZW50SWQpO1xuICAgIGlmIChlbCkge1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGVsLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcbiAgICAgICAgUmVhY3RET00udW5tb3VudENvbXBvbmVudEF0Tm9kZShlbCk7XG4gICAgICB9LCAyMDApO1xuICAgIH1cbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBuZXcgU3BpbkxvYWRpbmcoKTtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUlBOzs7Ozs7QUFDQTs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUFHQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/component/spin.tsx
