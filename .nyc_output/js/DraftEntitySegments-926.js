/**
 * Copyright (c) 2013-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @providesModule DraftEntitySegments
 * @format
 * 
 */

/**
 * Identify the range to delete from a segmented entity.
 *
 * Rules:
 *
 *  Example: 'John F. Kennedy'
 *
 *   - Deletion from within any non-whitespace (i.e. ['John', 'F.', 'Kennedy'])
 *     will return the range of that text.
 *
 *       'John F. Kennedy' -> 'John F.'
 *                  ^
 *
 *   - Forward deletion of whitespace will remove the following section:
 *
 *       'John F. Kennedy' -> 'John Kennedy'
 *            ^
 *
 *   - Backward deletion of whitespace will remove the previous section:
 *
 *       'John F. Kennedy' -> 'F. Kennedy'
 *            ^
 */

var DraftEntitySegments = {
  getRemovalRange: function getRemovalRange(selectionStart, selectionEnd, text, entityStart, direction) {
    var segments = text.split(' ');
    segments = segments.map(function (
    /*string*/
    segment,
    /*number*/
    ii) {
      if (direction === 'forward') {
        if (ii > 0) {
          return ' ' + segment;
        }
      } else if (ii < segments.length - 1) {
        return segment + ' ';
      }

      return segment;
    });
    var segmentStart = entityStart;
    var segmentEnd;
    var segment;
    var removalStart = null;
    var removalEnd = null;

    for (var jj = 0; jj < segments.length; jj++) {
      segment = segments[jj];
      segmentEnd = segmentStart + segment.length; // Our selection overlaps this segment.

      if (selectionStart < segmentEnd && segmentStart < selectionEnd) {
        if (removalStart !== null) {
          removalEnd = segmentEnd;
        } else {
          removalStart = segmentStart;
          removalEnd = segmentEnd;
        }
      } else if (removalStart !== null) {
        break;
      }

      segmentStart = segmentEnd;
    }

    var entityEnd = entityStart + text.length;
    var atStart = removalStart === entityStart;
    var atEnd = removalEnd === entityEnd;

    if (!atStart && atEnd || atStart && !atEnd) {
      if (direction === 'forward') {
        if (removalEnd !== entityEnd) {
          removalEnd++;
        }
      } else if (removalStart !== entityStart) {
        removalStart--;
      }
    }

    return {
      start: removalStart,
      end: removalEnd
    };
  }
};
module.exports = DraftEntitySegments;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZHJhZnQtanMvbGliL0RyYWZ0RW50aXR5U2VnbWVudHMuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9kcmFmdC1qcy9saWIvRHJhZnRFbnRpdHlTZWdtZW50cy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCAoYykgMjAxMy1wcmVzZW50LCBGYWNlYm9vaywgSW5jLlxuICogQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBCU0Qtc3R5bGUgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS4gQW4gYWRkaXRpb25hbCBncmFudFxuICogb2YgcGF0ZW50IHJpZ2h0cyBjYW4gYmUgZm91bmQgaW4gdGhlIFBBVEVOVFMgZmlsZSBpbiB0aGUgc2FtZSBkaXJlY3RvcnkuXG4gKlxuICogQHByb3ZpZGVzTW9kdWxlIERyYWZ0RW50aXR5U2VnbWVudHNcbiAqIEBmb3JtYXRcbiAqIFxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBJZGVudGlmeSB0aGUgcmFuZ2UgdG8gZGVsZXRlIGZyb20gYSBzZWdtZW50ZWQgZW50aXR5LlxuICpcbiAqIFJ1bGVzOlxuICpcbiAqICBFeGFtcGxlOiAnSm9obiBGLiBLZW5uZWR5J1xuICpcbiAqICAgLSBEZWxldGlvbiBmcm9tIHdpdGhpbiBhbnkgbm9uLXdoaXRlc3BhY2UgKGkuZS4gWydKb2huJywgJ0YuJywgJ0tlbm5lZHknXSlcbiAqICAgICB3aWxsIHJldHVybiB0aGUgcmFuZ2Ugb2YgdGhhdCB0ZXh0LlxuICpcbiAqICAgICAgICdKb2huIEYuIEtlbm5lZHknIC0+ICdKb2huIEYuJ1xuICogICAgICAgICAgICAgICAgICBeXG4gKlxuICogICAtIEZvcndhcmQgZGVsZXRpb24gb2Ygd2hpdGVzcGFjZSB3aWxsIHJlbW92ZSB0aGUgZm9sbG93aW5nIHNlY3Rpb246XG4gKlxuICogICAgICAgJ0pvaG4gRi4gS2VubmVkeScgLT4gJ0pvaG4gS2VubmVkeSdcbiAqICAgICAgICAgICAgXlxuICpcbiAqICAgLSBCYWNrd2FyZCBkZWxldGlvbiBvZiB3aGl0ZXNwYWNlIHdpbGwgcmVtb3ZlIHRoZSBwcmV2aW91cyBzZWN0aW9uOlxuICpcbiAqICAgICAgICdKb2huIEYuIEtlbm5lZHknIC0+ICdGLiBLZW5uZWR5J1xuICogICAgICAgICAgICBeXG4gKi9cbnZhciBEcmFmdEVudGl0eVNlZ21lbnRzID0ge1xuICBnZXRSZW1vdmFsUmFuZ2U6IGZ1bmN0aW9uIGdldFJlbW92YWxSYW5nZShzZWxlY3Rpb25TdGFydCwgc2VsZWN0aW9uRW5kLCB0ZXh0LCBlbnRpdHlTdGFydCwgZGlyZWN0aW9uKSB7XG4gICAgdmFyIHNlZ21lbnRzID0gdGV4dC5zcGxpdCgnICcpO1xuICAgIHNlZ21lbnRzID0gc2VnbWVudHMubWFwKGZ1bmN0aW9uICggLypzdHJpbmcqL3NlZ21lbnQsIC8qbnVtYmVyKi9paSkge1xuICAgICAgaWYgKGRpcmVjdGlvbiA9PT0gJ2ZvcndhcmQnKSB7XG4gICAgICAgIGlmIChpaSA+IDApIHtcbiAgICAgICAgICByZXR1cm4gJyAnICsgc2VnbWVudDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChpaSA8IHNlZ21lbnRzLmxlbmd0aCAtIDEpIHtcbiAgICAgICAgcmV0dXJuIHNlZ21lbnQgKyAnICc7XG4gICAgICB9XG4gICAgICByZXR1cm4gc2VnbWVudDtcbiAgICB9KTtcblxuICAgIHZhciBzZWdtZW50U3RhcnQgPSBlbnRpdHlTdGFydDtcbiAgICB2YXIgc2VnbWVudEVuZDtcbiAgICB2YXIgc2VnbWVudDtcbiAgICB2YXIgcmVtb3ZhbFN0YXJ0ID0gbnVsbDtcbiAgICB2YXIgcmVtb3ZhbEVuZCA9IG51bGw7XG5cbiAgICBmb3IgKHZhciBqaiA9IDA7IGpqIDwgc2VnbWVudHMubGVuZ3RoOyBqaisrKSB7XG4gICAgICBzZWdtZW50ID0gc2VnbWVudHNbampdO1xuICAgICAgc2VnbWVudEVuZCA9IHNlZ21lbnRTdGFydCArIHNlZ21lbnQubGVuZ3RoO1xuXG4gICAgICAvLyBPdXIgc2VsZWN0aW9uIG92ZXJsYXBzIHRoaXMgc2VnbWVudC5cbiAgICAgIGlmIChzZWxlY3Rpb25TdGFydCA8IHNlZ21lbnRFbmQgJiYgc2VnbWVudFN0YXJ0IDwgc2VsZWN0aW9uRW5kKSB7XG4gICAgICAgIGlmIChyZW1vdmFsU3RhcnQgIT09IG51bGwpIHtcbiAgICAgICAgICByZW1vdmFsRW5kID0gc2VnbWVudEVuZDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZW1vdmFsU3RhcnQgPSBzZWdtZW50U3RhcnQ7XG4gICAgICAgICAgcmVtb3ZhbEVuZCA9IHNlZ21lbnRFbmQ7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAocmVtb3ZhbFN0YXJ0ICE9PSBudWxsKSB7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuXG4gICAgICBzZWdtZW50U3RhcnQgPSBzZWdtZW50RW5kO1xuICAgIH1cblxuICAgIHZhciBlbnRpdHlFbmQgPSBlbnRpdHlTdGFydCArIHRleHQubGVuZ3RoO1xuICAgIHZhciBhdFN0YXJ0ID0gcmVtb3ZhbFN0YXJ0ID09PSBlbnRpdHlTdGFydDtcbiAgICB2YXIgYXRFbmQgPSByZW1vdmFsRW5kID09PSBlbnRpdHlFbmQ7XG5cbiAgICBpZiAoIWF0U3RhcnQgJiYgYXRFbmQgfHwgYXRTdGFydCAmJiAhYXRFbmQpIHtcbiAgICAgIGlmIChkaXJlY3Rpb24gPT09ICdmb3J3YXJkJykge1xuICAgICAgICBpZiAocmVtb3ZhbEVuZCAhPT0gZW50aXR5RW5kKSB7XG4gICAgICAgICAgcmVtb3ZhbEVuZCsrO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHJlbW92YWxTdGFydCAhPT0gZW50aXR5U3RhcnQpIHtcbiAgICAgICAgcmVtb3ZhbFN0YXJ0LS07XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXJ0OiByZW1vdmFsU3RhcnQsXG4gICAgICBlbmQ6IHJlbW92YWxFbmRcbiAgICB9O1xuICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IERyYWZ0RW50aXR5U2VnbWVudHM7Il0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7O0FBYUE7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBekRBO0FBNERBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/draft-js/lib/DraftEntitySegments.js
