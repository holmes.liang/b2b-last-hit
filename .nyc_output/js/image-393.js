var LRU = __webpack_require__(/*! ../../core/LRU */ "./node_modules/zrender/lib/core/LRU.js");

var globalImageCache = new LRU(50);
/**
 * @param {string|HTMLImageElement|HTMLCanvasElement|Canvas} newImageOrSrc
 * @return {HTMLImageElement|HTMLCanvasElement|Canvas} image
 */

function findExistImage(newImageOrSrc) {
  if (typeof newImageOrSrc === 'string') {
    var cachedImgObj = globalImageCache.get(newImageOrSrc);
    return cachedImgObj && cachedImgObj.image;
  } else {
    return newImageOrSrc;
  }
}
/**
 * Caution: User should cache loaded images, but not just count on LRU.
 * Consider if required images more than LRU size, will dead loop occur?
 *
 * @param {string|HTMLImageElement|HTMLCanvasElement|Canvas} newImageOrSrc
 * @param {HTMLImageElement|HTMLCanvasElement|Canvas} image Existent image.
 * @param {module:zrender/Element} [hostEl] For calling `dirty`.
 * @param {Function} [cb] params: (image, cbPayload)
 * @param {Object} [cbPayload] Payload on cb calling.
 * @return {HTMLImageElement|HTMLCanvasElement|Canvas} image
 */


function createOrUpdateImage(newImageOrSrc, image, hostEl, cb, cbPayload) {
  if (!newImageOrSrc) {
    return image;
  } else if (typeof newImageOrSrc === 'string') {
    // Image should not be loaded repeatly.
    if (image && image.__zrImageSrc === newImageOrSrc || !hostEl) {
      return image;
    } // Only when there is no existent image or existent image src
    // is different, this method is responsible for load.


    var cachedImgObj = globalImageCache.get(newImageOrSrc);
    var pendingWrap = {
      hostEl: hostEl,
      cb: cb,
      cbPayload: cbPayload
    };

    if (cachedImgObj) {
      image = cachedImgObj.image;
      !isImageReady(image) && cachedImgObj.pending.push(pendingWrap);
    } else {
      image = new Image();
      image.onload = image.onerror = imageOnLoad;
      globalImageCache.put(newImageOrSrc, image.__cachedImgObj = {
        image: image,
        pending: [pendingWrap]
      });
      image.src = image.__zrImageSrc = newImageOrSrc;
    }

    return image;
  } // newImageOrSrc is an HTMLImageElement or HTMLCanvasElement or Canvas
  else {
      return newImageOrSrc;
    }
}

function imageOnLoad() {
  var cachedImgObj = this.__cachedImgObj;
  this.onload = this.onerror = this.__cachedImgObj = null;

  for (var i = 0; i < cachedImgObj.pending.length; i++) {
    var pendingWrap = cachedImgObj.pending[i];
    var cb = pendingWrap.cb;
    cb && cb(this, pendingWrap.cbPayload);
    pendingWrap.hostEl.dirty();
  }

  cachedImgObj.pending.length = 0;
}

function isImageReady(image) {
  return image && image.width && image.height;
}

exports.findExistImage = findExistImage;
exports.createOrUpdateImage = createOrUpdateImage;
exports.isImageReady = isImageReady;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9oZWxwZXIvaW1hZ2UuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy96cmVuZGVyL2xpYi9ncmFwaGljL2hlbHBlci9pbWFnZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgTFJVID0gcmVxdWlyZShcIi4uLy4uL2NvcmUvTFJVXCIpO1xuXG52YXIgZ2xvYmFsSW1hZ2VDYWNoZSA9IG5ldyBMUlUoNTApO1xuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ3xIVE1MSW1hZ2VFbGVtZW50fEhUTUxDYW52YXNFbGVtZW50fENhbnZhc30gbmV3SW1hZ2VPclNyY1xuICogQHJldHVybiB7SFRNTEltYWdlRWxlbWVudHxIVE1MQ2FudmFzRWxlbWVudHxDYW52YXN9IGltYWdlXG4gKi9cblxuZnVuY3Rpb24gZmluZEV4aXN0SW1hZ2UobmV3SW1hZ2VPclNyYykge1xuICBpZiAodHlwZW9mIG5ld0ltYWdlT3JTcmMgPT09ICdzdHJpbmcnKSB7XG4gICAgdmFyIGNhY2hlZEltZ09iaiA9IGdsb2JhbEltYWdlQ2FjaGUuZ2V0KG5ld0ltYWdlT3JTcmMpO1xuICAgIHJldHVybiBjYWNoZWRJbWdPYmogJiYgY2FjaGVkSW1nT2JqLmltYWdlO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBuZXdJbWFnZU9yU3JjO1xuICB9XG59XG4vKipcbiAqIENhdXRpb246IFVzZXIgc2hvdWxkIGNhY2hlIGxvYWRlZCBpbWFnZXMsIGJ1dCBub3QganVzdCBjb3VudCBvbiBMUlUuXG4gKiBDb25zaWRlciBpZiByZXF1aXJlZCBpbWFnZXMgbW9yZSB0aGFuIExSVSBzaXplLCB3aWxsIGRlYWQgbG9vcCBvY2N1cj9cbiAqXG4gKiBAcGFyYW0ge3N0cmluZ3xIVE1MSW1hZ2VFbGVtZW50fEhUTUxDYW52YXNFbGVtZW50fENhbnZhc30gbmV3SW1hZ2VPclNyY1xuICogQHBhcmFtIHtIVE1MSW1hZ2VFbGVtZW50fEhUTUxDYW52YXNFbGVtZW50fENhbnZhc30gaW1hZ2UgRXhpc3RlbnQgaW1hZ2UuXG4gKiBAcGFyYW0ge21vZHVsZTp6cmVuZGVyL0VsZW1lbnR9IFtob3N0RWxdIEZvciBjYWxsaW5nIGBkaXJ0eWAuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbY2JdIHBhcmFtczogKGltYWdlLCBjYlBheWxvYWQpXG4gKiBAcGFyYW0ge09iamVjdH0gW2NiUGF5bG9hZF0gUGF5bG9hZCBvbiBjYiBjYWxsaW5nLlxuICogQHJldHVybiB7SFRNTEltYWdlRWxlbWVudHxIVE1MQ2FudmFzRWxlbWVudHxDYW52YXN9IGltYWdlXG4gKi9cblxuXG5mdW5jdGlvbiBjcmVhdGVPclVwZGF0ZUltYWdlKG5ld0ltYWdlT3JTcmMsIGltYWdlLCBob3N0RWwsIGNiLCBjYlBheWxvYWQpIHtcbiAgaWYgKCFuZXdJbWFnZU9yU3JjKSB7XG4gICAgcmV0dXJuIGltYWdlO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBuZXdJbWFnZU9yU3JjID09PSAnc3RyaW5nJykge1xuICAgIC8vIEltYWdlIHNob3VsZCBub3QgYmUgbG9hZGVkIHJlcGVhdGx5LlxuICAgIGlmIChpbWFnZSAmJiBpbWFnZS5fX3pySW1hZ2VTcmMgPT09IG5ld0ltYWdlT3JTcmMgfHwgIWhvc3RFbCkge1xuICAgICAgcmV0dXJuIGltYWdlO1xuICAgIH0gLy8gT25seSB3aGVuIHRoZXJlIGlzIG5vIGV4aXN0ZW50IGltYWdlIG9yIGV4aXN0ZW50IGltYWdlIHNyY1xuICAgIC8vIGlzIGRpZmZlcmVudCwgdGhpcyBtZXRob2QgaXMgcmVzcG9uc2libGUgZm9yIGxvYWQuXG5cblxuICAgIHZhciBjYWNoZWRJbWdPYmogPSBnbG9iYWxJbWFnZUNhY2hlLmdldChuZXdJbWFnZU9yU3JjKTtcbiAgICB2YXIgcGVuZGluZ1dyYXAgPSB7XG4gICAgICBob3N0RWw6IGhvc3RFbCxcbiAgICAgIGNiOiBjYixcbiAgICAgIGNiUGF5bG9hZDogY2JQYXlsb2FkXG4gICAgfTtcblxuICAgIGlmIChjYWNoZWRJbWdPYmopIHtcbiAgICAgIGltYWdlID0gY2FjaGVkSW1nT2JqLmltYWdlO1xuICAgICAgIWlzSW1hZ2VSZWFkeShpbWFnZSkgJiYgY2FjaGVkSW1nT2JqLnBlbmRpbmcucHVzaChwZW5kaW5nV3JhcCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGltYWdlID0gbmV3IEltYWdlKCk7XG4gICAgICBpbWFnZS5vbmxvYWQgPSBpbWFnZS5vbmVycm9yID0gaW1hZ2VPbkxvYWQ7XG4gICAgICBnbG9iYWxJbWFnZUNhY2hlLnB1dChuZXdJbWFnZU9yU3JjLCBpbWFnZS5fX2NhY2hlZEltZ09iaiA9IHtcbiAgICAgICAgaW1hZ2U6IGltYWdlLFxuICAgICAgICBwZW5kaW5nOiBbcGVuZGluZ1dyYXBdXG4gICAgICB9KTtcbiAgICAgIGltYWdlLnNyYyA9IGltYWdlLl9fenJJbWFnZVNyYyA9IG5ld0ltYWdlT3JTcmM7XG4gICAgfVxuXG4gICAgcmV0dXJuIGltYWdlO1xuICB9IC8vIG5ld0ltYWdlT3JTcmMgaXMgYW4gSFRNTEltYWdlRWxlbWVudCBvciBIVE1MQ2FudmFzRWxlbWVudCBvciBDYW52YXNcbiAgZWxzZSB7XG4gICAgICByZXR1cm4gbmV3SW1hZ2VPclNyYztcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGltYWdlT25Mb2FkKCkge1xuICB2YXIgY2FjaGVkSW1nT2JqID0gdGhpcy5fX2NhY2hlZEltZ09iajtcbiAgdGhpcy5vbmxvYWQgPSB0aGlzLm9uZXJyb3IgPSB0aGlzLl9fY2FjaGVkSW1nT2JqID0gbnVsbDtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGNhY2hlZEltZ09iai5wZW5kaW5nLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIHBlbmRpbmdXcmFwID0gY2FjaGVkSW1nT2JqLnBlbmRpbmdbaV07XG4gICAgdmFyIGNiID0gcGVuZGluZ1dyYXAuY2I7XG4gICAgY2IgJiYgY2IodGhpcywgcGVuZGluZ1dyYXAuY2JQYXlsb2FkKTtcbiAgICBwZW5kaW5nV3JhcC5ob3N0RWwuZGlydHkoKTtcbiAgfVxuXG4gIGNhY2hlZEltZ09iai5wZW5kaW5nLmxlbmd0aCA9IDA7XG59XG5cbmZ1bmN0aW9uIGlzSW1hZ2VSZWFkeShpbWFnZSkge1xuICByZXR1cm4gaW1hZ2UgJiYgaW1hZ2Uud2lkdGggJiYgaW1hZ2UuaGVpZ2h0O1xufVxuXG5leHBvcnRzLmZpbmRFeGlzdEltYWdlID0gZmluZEV4aXN0SW1hZ2U7XG5leHBvcnRzLmNyZWF0ZU9yVXBkYXRlSW1hZ2UgPSBjcmVhdGVPclVwZGF0ZUltYWdlO1xuZXhwb3J0cy5pc0ltYWdlUmVhZHkgPSBpc0ltYWdlUmVhZHk7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUFhQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE3QkE7QUErQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/helper/image.js
