__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Modal */ "./node_modules/antd/es/modal/Modal.js");
/* harmony import */ var _confirm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirm */ "./node_modules/antd/es/modal/confirm.js");
/* harmony import */ var _icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../icon */ "./node_modules/antd/es/icon/index.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}






function modalWarn(props) {
  var config = _extends({
    type: 'warning',
    icon: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "exclamation-circle"
    }),
    okCancel: false
  }, props);

  return Object(_confirm__WEBPACK_IMPORTED_MODULE_2__["default"])(config);
}

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].info = function infoFn(props) {
  var config = _extends({
    type: 'info',
    icon: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "info-circle"
    }),
    okCancel: false
  }, props);

  return Object(_confirm__WEBPACK_IMPORTED_MODULE_2__["default"])(config);
};

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].success = function successFn(props) {
  var config = _extends({
    type: 'success',
    icon: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "check-circle"
    }),
    okCancel: false
  }, props);

  return Object(_confirm__WEBPACK_IMPORTED_MODULE_2__["default"])(config);
};

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].error = function errorFn(props) {
  var config = _extends({
    type: 'error',
    icon: react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_icon__WEBPACK_IMPORTED_MODULE_3__["default"], {
      type: "close-circle"
    }),
    okCancel: false
  }, props);

  return Object(_confirm__WEBPACK_IMPORTED_MODULE_2__["default"])(config);
};

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].warning = modalWarn;
_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].warn = modalWarn;

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].confirm = function confirmFn(props) {
  var config = _extends({
    type: 'confirm',
    okCancel: true
  }, props);

  return Object(_confirm__WEBPACK_IMPORTED_MODULE_2__["default"])(config);
};

_Modal__WEBPACK_IMPORTED_MODULE_1__["default"].destroyAll = function destroyAllFn() {
  while (_Modal__WEBPACK_IMPORTED_MODULE_1__["destroyFns"].length) {
    var close = _Modal__WEBPACK_IMPORTED_MODULE_1__["destroyFns"].pop();

    if (close) {
      close();
    }
  }
};

/* harmony default export */ __webpack_exports__["default"] = (_Modal__WEBPACK_IMPORTED_MODULE_1__["default"]);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvYW50ZC9lcy9tb2RhbC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbW9kYWwvaW5kZXguanN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBNb2RhbCwgeyBkZXN0cm95Rm5zIH0gZnJvbSAnLi9Nb2RhbCc7XG5pbXBvcnQgY29uZmlybSBmcm9tICcuL2NvbmZpcm0nO1xuaW1wb3J0IEljb24gZnJvbSAnLi4vaWNvbic7XG5mdW5jdGlvbiBtb2RhbFdhcm4ocHJvcHMpIHtcbiAgICBjb25zdCBjb25maWcgPSBPYmplY3QuYXNzaWduKHsgdHlwZTogJ3dhcm5pbmcnLCBpY29uOiA8SWNvbiB0eXBlPVwiZXhjbGFtYXRpb24tY2lyY2xlXCIvPiwgb2tDYW5jZWw6IGZhbHNlIH0sIHByb3BzKTtcbiAgICByZXR1cm4gY29uZmlybShjb25maWcpO1xufVxuTW9kYWwuaW5mbyA9IGZ1bmN0aW9uIGluZm9Gbihwcm9wcykge1xuICAgIGNvbnN0IGNvbmZpZyA9IE9iamVjdC5hc3NpZ24oeyB0eXBlOiAnaW5mbycsIGljb246IDxJY29uIHR5cGU9XCJpbmZvLWNpcmNsZVwiLz4sIG9rQ2FuY2VsOiBmYWxzZSB9LCBwcm9wcyk7XG4gICAgcmV0dXJuIGNvbmZpcm0oY29uZmlnKTtcbn07XG5Nb2RhbC5zdWNjZXNzID0gZnVuY3Rpb24gc3VjY2Vzc0ZuKHByb3BzKSB7XG4gICAgY29uc3QgY29uZmlnID0gT2JqZWN0LmFzc2lnbih7IHR5cGU6ICdzdWNjZXNzJywgaWNvbjogPEljb24gdHlwZT1cImNoZWNrLWNpcmNsZVwiLz4sIG9rQ2FuY2VsOiBmYWxzZSB9LCBwcm9wcyk7XG4gICAgcmV0dXJuIGNvbmZpcm0oY29uZmlnKTtcbn07XG5Nb2RhbC5lcnJvciA9IGZ1bmN0aW9uIGVycm9yRm4ocHJvcHMpIHtcbiAgICBjb25zdCBjb25maWcgPSBPYmplY3QuYXNzaWduKHsgdHlwZTogJ2Vycm9yJywgaWNvbjogPEljb24gdHlwZT1cImNsb3NlLWNpcmNsZVwiLz4sIG9rQ2FuY2VsOiBmYWxzZSB9LCBwcm9wcyk7XG4gICAgcmV0dXJuIGNvbmZpcm0oY29uZmlnKTtcbn07XG5Nb2RhbC53YXJuaW5nID0gbW9kYWxXYXJuO1xuTW9kYWwud2FybiA9IG1vZGFsV2Fybjtcbk1vZGFsLmNvbmZpcm0gPSBmdW5jdGlvbiBjb25maXJtRm4ocHJvcHMpIHtcbiAgICBjb25zdCBjb25maWcgPSBPYmplY3QuYXNzaWduKHsgdHlwZTogJ2NvbmZpcm0nLCBva0NhbmNlbDogdHJ1ZSB9LCBwcm9wcyk7XG4gICAgcmV0dXJuIGNvbmZpcm0oY29uZmlnKTtcbn07XG5Nb2RhbC5kZXN0cm95QWxsID0gZnVuY3Rpb24gZGVzdHJveUFsbEZuKCkge1xuICAgIHdoaWxlIChkZXN0cm95Rm5zLmxlbmd0aCkge1xuICAgICAgICBjb25zdCBjbG9zZSA9IGRlc3Ryb3lGbnMucG9wKCk7XG4gICAgICAgIGlmIChjbG9zZSkge1xuICAgICAgICAgICAgY2xvc2UoKTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5leHBvcnQgZGVmYXVsdCBNb2RhbDtcbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQUNBO0FBT0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/antd/es/modal/index.js
