__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LocationTableView; });
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _desk_styles_global__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @desk-styles/global */ "./src/app/desk/styles/global.tsx");
/* harmony import */ var _location_view__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./location-view */ "./src/app/desk/quote/SAIC/iar/component/location-view.tsx");
/* harmony import */ var _desk_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @desk-component */ "./src/app/desk/component/index.tsx");








var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/quote/SAIC/iar/component/location-table-view.tsx";









var LocationTableView =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(LocationTableView, _React$Component);

  function LocationTableView() {
    var _getPrototypeOf2;

    var _this;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, LocationTableView);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(LocationTableView)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this.state = {
      codeTables: {}
    };
    _this.queryPage = {};
    _this.columns = [{
      title: "Postal Code",
      dataIndex: "location.postalCode",
      key: "location.postalCode"
    }, {
      title: "Address",
      dataIndex: "location",
      key: "location",
      render: function render(col, record, index) {
        var _this$props = _this.props,
            model = _this$props.model,
            endoFixed = _this$props.endoFixed;
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_component__WEBPACK_IMPORTED_MODULE_15__["ShowAddress"], {
          model: model,
          addressFixed: endoFixed ? "ext.changes.locations.".concat(index, ".location") : "ext.locations.".concat(index, ".location"),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 38
          },
          __self: this
        });
      }
    }, {
      title: function title() {
        var endoFixed = _this.props.endoFixed;
        var dataCurrency = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].formatCurrencyInfo("", lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, endoFixed ? "".concat(endoFixed, ".siCurrency") : "siCurrencyCode"));
        return "Sum Insured (".concat(dataCurrency.currencyText, ")");
      },
      dataIndex: "coverages",
      key: "coverages",
      className: "amount-right",
      render: function render(col, record) {
        var coverages = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(record, "coverages", []);

        var sum = 0;
        (coverages || []).forEach(function (item) {
          if (item.selected === "Y") {
            sum += parseFloat(item.aggregatedSi) || 0;
          }
        }); // const data = Utils.formatCurrencyInfo(sum, _.get(this.props.model, "siCurrencyCode"));

        sum = sum.toFixed(0);
        return _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].toThousands(sum); // return `${data.currencyText} ${data.amount}`;
      }
    }, {
      title: function title() {
        var endoFixed = _this.props.endoFixed;
        var dataCurr = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].formatCurrencyInfo("", lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, endoFixed ? "".concat(endoFixed, ".premiumCurrency") : "premCurrencyCode"));
        return "Premium (".concat(dataCurr.currencyText, ")");
      },
      dataIndex: "premium.lumpsum.gwp",
      key: "premium",
      className: "amount-right",
      render: function render(col, record) {
        var endoFixed = _this.props.endoFixed;

        var code = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(record, "location.postalCode", "");

        var breakDown = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, endoFixed ? "deltaPolicyPremium.insuredPremiums" : "policyPremium.insuredPremiums") || [];
        var locationPremium = breakDown.find(function (item) {
          return item.insuredKey === code;
        });
        var premium = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(locationPremium, "gwp", 0) || 0;
        var data = _common__WEBPACK_IMPORTED_MODULE_10__["Utils"].formatCurrencyInfo(premium, lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, endoFixed ? "".concat(endoFixed, ".premiumCurrency") : "premCurrencyCode"));
        return "".concat(data.amount);
      }
    }, {
      title: "Actions",
      align: "center",
      dataIndex: "action",
      render: function render(col, record, index) {
        return _this.renderOperColumn(record, index);
      }
    }];

    _this.handleMemberView = function (record) {
      // this.setValueToModel(record, "member.ext");
      lodash__WEBPACK_IMPORTED_MODULE_9___default.a.set(_this.props.model, "member.ext", record);

      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_11__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_location_view__WEBPACK_IMPORTED_MODULE_14__["LocationView"], {
            onCancel: onCancel,
            model: _this.props.model,
            endoFixed: _this.props.endoFixed,
            getMasterTableItemText: _this.getMasterTableItemText.bind(Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_6__["default"])(_this)),
            __source: {
              fileName: _jsxFileName,
              lineNumber: 109
            },
            __self: this
          });
        }
      });
    };

    _this.loadCodeTables =
    /*#__PURE__*/
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
    /*#__PURE__*/
    _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var codeTables, arr;
      return _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              codeTables = _this.state.codeTables;
              arr = [];
              ["idtype", "title", "nationality"].forEach(function (item) {
                arr.push(new Promise(function (resolve, reject) {
                  _common__WEBPACK_IMPORTED_MODULE_10__["Ajax"].get("/mastertable", {
                    itntCode: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, "itntCode"),
                    productCode: lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(_this.props.model, "productCode"),
                    tableName: item
                  }, {}).then(function (response) {
                    var respData = response.body.respData;
                    codeTables[item] = respData.items || [];
                    resolve();
                  }).catch(function (error) {});
                }));
              });
              Promise.all(arr).then(function (values) {
                _this.setState({
                  codeTables: codeTables
                });
              }, function (reason) {
                console.log(reason);
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    _this.getMasterTableItemText = function (tableName, itemId) {
      var codeTables = _this.state.codeTables;

      if (itemId) {
        var item = (codeTables[tableName] || []).find(function (option) {
          return itemId.toString() === option.id.toString();
        }) || {};
        return item.text || "";
      } else {
        return "";
      }
    };

    return _this;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(LocationTableView, [{
    key: "renderOperColumn",
    value: function renderOperColumn(record, index) {
      var _this2 = this;

      var _record$opers = record.opers,
          opers = _record$opers === void 0 ? {} : _record$opers;
      var list = [{
        text: "View",
        onClick: function onClick() {
          return _this2.handleMemberView(record);
        },
        enabled: true
      }];
      return list.filter(function (item) {
        return item.enabled;
      }).map(function (item) {
        return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 101
          },
          __self: this
        }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("a", Object.assign({
          key: item.text
        }, item, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 101
          },
          __self: this
        }), item.text));
      });
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadCodeTables();
    }
  }, {
    key: "getValue",
    value: function getValue(propName) {
      return lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.model, "member.ext.".concat(propName));
    }
  }, {
    key: "getPlan",
    value: function getPlan(ext) {
      var plan = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(ext, "planCode", "");

      return plan;
    }
  }, {
    key: "render",
    value: function render() {
      var endoFixed = this.props.endoFixed;
      var locations = lodash__WEBPACK_IMPORTED_MODULE_9___default.a.get(this.props.model, endoFixed ? "".concat(endoFixed, ".locations") : "ext.locations") || [];
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(_desk_styles_global__WEBPACK_IMPORTED_MODULE_13__["TableHeader"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_12__["Table"], {
        pagination: false,
        size: "default",
        bordered: true,
        columns: this.columns,
        dataSource: locations,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 178
        },
        __self: this
      }));
    }
  }]);

  return LocationTableView;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Component);

//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2xvY2F0aW9uLXRhYmxlLXZpZXcudHN4LmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9zcmMvYXBwL2Rlc2svcXVvdGUvU0FJQy9pYXIvY29tcG9uZW50L2xvY2F0aW9uLXRhYmxlLXZpZXcudHN4Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlYWN0IH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBBamF4LCBVdGlscyB9IGZyb20gXCJAY29tbW9uXCI7XG5pbXBvcnQgeyBBamF4UmVzcG9uc2UgfSBmcm9tIFwiQG15LXR5cGVzXCI7XG5pbXBvcnQgeyBPcGVyTGluayB9IGZyb20gXCIuLi8uLi8uLi8uLi9hZG1pbi9jb21tb24vT3BlckxpbmtzXCI7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCB7IFRhYmxlIH0gZnJvbSBcImFudGRcIjtcbmltcG9ydCB7IFRhYmxlSGVhZGVyIH0gZnJvbSBcIkBkZXNrLXN0eWxlcy9nbG9iYWxcIjtcbmltcG9ydCB7IExvY2F0aW9uVmlldyB9IGZyb20gXCIuL2xvY2F0aW9uLXZpZXdcIjtcbmltcG9ydCB7IFNob3dBZGRyZXNzIH0gZnJvbSBcIkBkZXNrLWNvbXBvbmVudFwiO1xuXG5pbnRlcmZhY2UgSVByb3BzIHtcbiAgbW9kZWw6IGFueTtcbiAgZW5kb0ZpeGVkPzogYW55O1xufVxuXG5pbnRlcmZhY2UgSVN0YXRlIHtcbiAgY29kZVRhYmxlczogYW55O1xufVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMb2NhdGlvblRhYmxlVmlldyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxJUHJvcHMsIElTdGF0ZT4ge1xuICBzdGF0ZTogSVN0YXRlID0ge1xuICAgIGNvZGVUYWJsZXM6IHt9LFxuICB9O1xuICBxdWVyeVBhZ2UgPSB7fSBhcyBhbnk7XG4gIGNvbHVtbnM6IGFueSA9IFtcbiAgICB7XG4gICAgICB0aXRsZTogXCJQb3N0YWwgQ29kZVwiLFxuICAgICAgZGF0YUluZGV4OiBcImxvY2F0aW9uLnBvc3RhbENvZGVcIixcbiAgICAgIGtleTogXCJsb2NhdGlvbi5wb3N0YWxDb2RlXCIsXG4gICAgfSxcbiAgICB7XG4gICAgICB0aXRsZTogXCJBZGRyZXNzXCIsXG4gICAgICBkYXRhSW5kZXg6IFwibG9jYXRpb25cIixcbiAgICAgIGtleTogXCJsb2NhdGlvblwiLFxuICAgICAgcmVuZGVyOiAoY29sOiBhbnksIHJlY29yZDogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgbW9kZWwsIGVuZG9GaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgcmV0dXJuIDxTaG93QWRkcmVzcyBtb2RlbD17bW9kZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkcmVzc0ZpeGVkPXtlbmRvRml4ZWQgPyBgZXh0LmNoYW5nZXMubG9jYXRpb25zLiR7aW5kZXh9LmxvY2F0aW9uYCA6IGBleHQubG9jYXRpb25zLiR7aW5kZXh9LmxvY2F0aW9uYH0vPjtcbiAgICAgIH0sXG4gICAgfSxcbiAgICB7XG4gICAgICB0aXRsZTogKCkgPT4ge1xuICAgICAgICBjb25zdCB7IGVuZG9GaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgICAgY29uc3QgZGF0YUN1cnJlbmN5ID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKFwiXCIsIF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGVuZG9GaXhlZCA/IGAke2VuZG9GaXhlZH0uc2lDdXJyZW5jeWAgOiBcInNpQ3VycmVuY3lDb2RlXCIpKTtcbiAgICAgICAgcmV0dXJuIGBTdW0gSW5zdXJlZCAoJHtkYXRhQ3VycmVuY3kuY3VycmVuY3lUZXh0fSlgO1xuICAgICAgfSxcbiAgICAgIGRhdGFJbmRleDogXCJjb3ZlcmFnZXNcIixcbiAgICAgIGtleTogXCJjb3ZlcmFnZXNcIixcbiAgICAgIGNsYXNzTmFtZTogXCJhbW91bnQtcmlnaHRcIixcbiAgICAgIHJlbmRlcjogKGNvbDogYW55LCByZWNvcmQ6IGFueSkgPT4ge1xuICAgICAgICBjb25zdCBjb3ZlcmFnZXMgPSBfLmdldChyZWNvcmQsIFwiY292ZXJhZ2VzXCIsIFtdKTtcbiAgICAgICAgbGV0IHN1bTogYW55ID0gMDtcbiAgICAgICAgKGNvdmVyYWdlcyB8fCBbXSkuZm9yRWFjaCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgaWYgKGl0ZW0uc2VsZWN0ZWQgPT09IFwiWVwiKSB7XG4gICAgICAgICAgICBzdW0gKz0gcGFyc2VGbG9hdChpdGVtLmFnZ3JlZ2F0ZWRTaSkgfHwgMDtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICAvLyBjb25zdCBkYXRhID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKHN1bSwgXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJzaUN1cnJlbmN5Q29kZVwiKSk7XG4gICAgICAgIHN1bSA9IHN1bS50b0ZpeGVkKDApO1xuICAgICAgICByZXR1cm4gVXRpbHMudG9UaG91c2FuZHMoc3VtKTtcbiAgICAgICAgLy8gcmV0dXJuIGAke2RhdGEuY3VycmVuY3lUZXh0fSAke2RhdGEuYW1vdW50fWA7XG4gICAgICB9LFxuICAgIH0sXG4gICAge1xuICAgICAgdGl0bGU6ICgpID0+IHtcbiAgICAgICAgY29uc3QgeyBlbmRvRml4ZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGRhdGFDdXJyID0gVXRpbHMuZm9ybWF0Q3VycmVuY3lJbmZvKFwiXCIsIF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGVuZG9GaXhlZCA/IGAke2VuZG9GaXhlZH0ucHJlbWl1bUN1cnJlbmN5YCA6IFwicHJlbUN1cnJlbmN5Q29kZVwiKSk7XG4gICAgICAgIHJldHVybiBgUHJlbWl1bSAoJHtkYXRhQ3Vyci5jdXJyZW5jeVRleHR9KWA7XG4gICAgICB9LFxuICAgICAgZGF0YUluZGV4OiBcInByZW1pdW0ubHVtcHN1bS5nd3BcIixcbiAgICAgIGtleTogXCJwcmVtaXVtXCIsXG4gICAgICBjbGFzc05hbWU6IFwiYW1vdW50LXJpZ2h0XCIsXG4gICAgICByZW5kZXI6IChjb2w6IGFueSwgcmVjb3JkOiBhbnkpID0+IHtcbiAgICAgICAgY29uc3QgeyBlbmRvRml4ZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICAgIGNvbnN0IGNvZGUgPSBfLmdldChyZWNvcmQsIFwibG9jYXRpb24ucG9zdGFsQ29kZVwiLCBcIlwiKTtcbiAgICAgICAgY29uc3QgYnJlYWtEb3duID0gXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgZW5kb0ZpeGVkID8gXCJkZWx0YVBvbGljeVByZW1pdW0uaW5zdXJlZFByZW1pdW1zXCIgOiBcInBvbGljeVByZW1pdW0uaW5zdXJlZFByZW1pdW1zXCIpIHx8IFtdO1xuICAgICAgICBjb25zdCBsb2NhdGlvblByZW1pdW0gPSBicmVha0Rvd24uZmluZCgoaXRlbTogYW55KSA9PiB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW0uaW5zdXJlZEtleSA9PT0gY29kZTtcbiAgICAgICAgfSk7XG4gICAgICAgIGNvbnN0IHByZW1pdW0gPSBfLmdldChsb2NhdGlvblByZW1pdW0sIFwiZ3dwXCIsIDApIHx8IDA7XG4gICAgICAgIGNvbnN0IGRhdGEgPSBVdGlscy5mb3JtYXRDdXJyZW5jeUluZm8ocHJlbWl1bSwgXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgZW5kb0ZpeGVkID8gYCR7ZW5kb0ZpeGVkfS5wcmVtaXVtQ3VycmVuY3lgIDogXCJwcmVtQ3VycmVuY3lDb2RlXCIpKTtcbiAgICAgICAgcmV0dXJuIGAke2RhdGEuYW1vdW50fWA7XG4gICAgICB9LFxuICAgIH0sIHtcbiAgICAgIHRpdGxlOiBcIkFjdGlvbnNcIixcbiAgICAgIGFsaWduOiBcImNlbnRlclwiLFxuICAgICAgZGF0YUluZGV4OiBcImFjdGlvblwiLFxuICAgICAgcmVuZGVyOiAoY29sOiBhbnksIHJlY29yZDogYW55LCBpbmRleDogbnVtYmVyKSA9PiB0aGlzLnJlbmRlck9wZXJDb2x1bW4ocmVjb3JkLCBpbmRleCksXG4gICAgfV07XG5cbiAgcmVuZGVyT3BlckNvbHVtbihyZWNvcmQ6IGFueSwgaW5kZXg6IGFueSkge1xuICAgIGNvbnN0IHsgb3BlcnMgPSB7fSB9ID0gcmVjb3JkO1xuICAgIGNvbnN0IGxpc3Q6IE9wZXJMaW5rW10gPSBbXG4gICAgICB7IHRleHQ6IFwiVmlld1wiLCBvbkNsaWNrOiAoKSA9PiB0aGlzLmhhbmRsZU1lbWJlclZpZXcocmVjb3JkKSwgZW5hYmxlZDogdHJ1ZSB9LFxuICAgIF07XG5cbiAgICByZXR1cm4gbGlzdFxuICAgICAgLmZpbHRlcigoaXRlbTogYW55KSA9PiBpdGVtLmVuYWJsZWQpXG4gICAgICAubWFwKChpdGVtOiBhbnkpID0+IHtcbiAgICAgICAgcmV0dXJuIDxkaXY+PGEga2V5PXtpdGVtLnRleHR9IHsuLi5pdGVtfT57aXRlbS50ZXh0fTwvYT48L2Rpdj47XG4gICAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZU1lbWJlclZpZXcgPSAocmVjb3JkOiBhbnkpID0+IHtcbiAgICAvLyB0aGlzLnNldFZhbHVlVG9Nb2RlbChyZWNvcmQsIFwibWVtYmVyLmV4dFwiKTtcbiAgICBfLnNldCh0aGlzLnByb3BzLm1vZGVsLCBcIm1lbWJlci5leHRcIiwgcmVjb3JkKTtcbiAgICBNYXNrLmNyZWF0ZSh7XG4gICAgICBDb21wb25lbnQ6ICh7IG9uQ2FuY2VsLCBvbk9rIH06IGFueSkgPT4gPExvY2F0aW9uVmlld1xuICAgICAgICBvbkNhbmNlbD17b25DYW5jZWx9XG4gICAgICAgIG1vZGVsPXt0aGlzLnByb3BzLm1vZGVsfVxuICAgICAgICBlbmRvRml4ZWQ9e3RoaXMucHJvcHMuZW5kb0ZpeGVkfVxuICAgICAgICBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0PXt0aGlzLmdldE1hc3RlclRhYmxlSXRlbVRleHQuYmluZCh0aGlzKX1cbiAgICAgIC8+LFxuICAgIH0pO1xuICB9O1xuXG5cbiAgcHJpdmF0ZSBsb2FkQ29kZVRhYmxlcyA9IGFzeW5jICgpID0+IHtcbiAgICBsZXQgeyBjb2RlVGFibGVzIH0gPSB0aGlzLnN0YXRlO1xuICAgIGxldCBhcnI6IGFueSA9IFtdO1xuICAgIFtcbiAgICAgIFwiaWR0eXBlXCIsXG4gICAgICBcInRpdGxlXCIsXG4gICAgICBcIm5hdGlvbmFsaXR5XCIsXG4gICAgXS5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgYXJyLnB1c2goXG4gICAgICAgIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICBBamF4LmdldChcbiAgICAgICAgICAgIGAvbWFzdGVydGFibGVgLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBpdG50Q29kZTogXy5nZXQodGhpcy5wcm9wcy5tb2RlbCwgXCJpdG50Q29kZVwiKSxcbiAgICAgICAgICAgICAgcHJvZHVjdENvZGU6IF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIFwicHJvZHVjdENvZGVcIiksXG4gICAgICAgICAgICAgIHRhYmxlTmFtZTogaXRlbSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7fSxcbiAgICAgICAgICApXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2U6IEFqYXhSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICBjb25zdCB7IHJlc3BEYXRhIH0gPSByZXNwb25zZS5ib2R5O1xuICAgICAgICAgICAgICBjb2RlVGFibGVzW2l0ZW1dID0gcmVzcERhdGEuaXRlbXMgfHwgW107XG4gICAgICAgICAgICAgIHJlc29sdmUoKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pLFxuICAgICAgKTtcbiAgICB9KTtcbiAgICBQcm9taXNlLmFsbChhcnIpLnRoZW4oXG4gICAgICB2YWx1ZXMgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgY29kZVRhYmxlczogY29kZVRhYmxlcyB9KTtcbiAgICAgIH0sXG4gICAgICByZWFzb24gPT4ge1xuICAgICAgICBjb25zb2xlLmxvZyhyZWFzb24pO1xuICAgICAgfSxcbiAgICApO1xuICB9O1xuXG4gIGNvbXBvbmVudERpZE1vdW50KCk6IHZvaWQge1xuICAgIHRoaXMubG9hZENvZGVUYWJsZXMoKTtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0VmFsdWUocHJvcE5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgcmV0dXJuIF8uZ2V0KHRoaXMucHJvcHMubW9kZWwsIGBtZW1iZXIuZXh0LiR7cHJvcE5hbWV9YCk7XG4gIH1cblxuICBnZXRQbGFuKGV4dDogYW55KSB7XG4gICAgY29uc3QgcGxhbiA9IF8uZ2V0KGV4dCwgYHBsYW5Db2RlYCwgXCJcIik7XG5cbiAgICByZXR1cm4gcGxhbjtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGVuZG9GaXhlZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCBsb2NhdGlvbnMgPSBfLmdldCh0aGlzLnByb3BzLm1vZGVsLCBlbmRvRml4ZWQgPyBgJHtlbmRvRml4ZWR9LmxvY2F0aW9uc2AgOiBcImV4dC5sb2NhdGlvbnNcIikgfHwgW107XG5cblxuICAgIHJldHVybiA8VGFibGVIZWFkZXI+XG4gICAgICA8VGFibGVcbiAgICAgICAgcGFnaW5hdGlvbj17ZmFsc2V9XG4gICAgICAgIHNpemU9XCJkZWZhdWx0XCJcbiAgICAgICAgYm9yZGVyZWRcbiAgICAgICAgY29sdW1ucz17dGhpcy5jb2x1bW5zfVxuICAgICAgICBkYXRhU291cmNlPXtsb2NhdGlvbnN9XG4gICAgICAvPlxuICAgIDwvVGFibGVIZWFkZXI+O1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRNYXN0ZXJUYWJsZUl0ZW1UZXh0ID0gKHRhYmxlTmFtZTogc3RyaW5nLCBpdGVtSWQ6IHN0cmluZykgPT4ge1xuICAgIGNvbnN0IHsgY29kZVRhYmxlcyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBpZiAoaXRlbUlkKSB7XG4gICAgICBjb25zdCBpdGVtID1cbiAgICAgICAgKGNvZGVUYWJsZXNbdGFibGVOYW1lXSB8fCBbXSkuZmluZCgob3B0aW9uOiBhbnkpID0+IHtcbiAgICAgICAgICByZXR1cm4gaXRlbUlkLnRvU3RyaW5nKCkgPT09IG9wdGlvbi5pZC50b1N0cmluZygpO1xuICAgICAgICB9KSB8fCB7fTtcbiAgICAgIHJldHVybiBpdGVtLnRleHQgfHwgXCJcIjtcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIFwiXCI7XG4gICAgfVxuICB9O1xufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVVBOzs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFSQTtBQVdBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQXJCQTtBQXdCQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFuQkE7QUFxQkE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBSkE7QUFDQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFRQTtBQUNBO0FBRUE7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUVBO0FBR0E7QUFDQTtBQUNBO0FBSEE7QUFPQTtBQUVBO0FBQ0E7QUFDQTtBQUdBO0FBRUE7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBcENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFvRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQTVHQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFHQTtBQUNBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBOzs7QUF1REE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFFQTtBQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVFBOzs7O0FBcktBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/quote/SAIC/iar/component/location-table-view.tsx
