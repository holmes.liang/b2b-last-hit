__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @desk-component/create-dialog */ "./src/app/desk/component/create-dialog.tsx");
/* harmony import */ var _desk_claims_fnol_create_claim_sme_components_typeModal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @desk/claims/fnol/create-claim/sme/components/typeModal */ "./src/app/desk/claims/fnol/create-claim/sme/components/typeModal.tsx");



var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/claims/common.tsx";






var ClaimsCommon =
/*#__PURE__*/
function () {
  function ClaimsCommon() {
    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, ClaimsCommon);
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(ClaimsCommon, [{
    key: "checkTime",
    value: function checkTime(item, _this) {
      var _this2 = this;

      _desk_component_create_dialog__WEBPACK_IMPORTED_MODULE_6__["default"].create({
        Component: function Component(_ref) {
          var onCancel = _ref.onCancel,
              onOk = _ref.onOk;
          return react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement(_desk_claims_fnol_create_claim_sme_components_typeModal__WEBPACK_IMPORTED_MODULE_7__["default"], {
            onOk: onOk,
            onCancel: onCancel,
            onContinue: function onContinue(item, type, insured, cancel) {
              _this2.onContinue(_this, item, type, insured, cancel);
            },
            item: item,
            that: _this,
            __source: {
              fileName: _jsxFileName,
              lineNumber: 11
            },
            __self: this
          });
        }
      });
    }
  }, {
    key: "getLossItems",
    value: function getLossItems(item, _this) {
      if (!item) return;
      this.checkTime(item, _this);
    }
  }, {
    key: "onContinue",
    value: function onContinue(_this, item, type, insured, cancel) {
      cancel();
      var claimInsured = insured ? JSON.parse(insured) : '';

      if (lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode') === 'EFC') {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL_SME_TYPE.replace(':type', 'PA'),
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: 'PA',
            dateTime: _this.state.dateTime
          }
        });
      } else if ("EL" === lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode')) {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL_SME_TYPE.replace(':type', 'EL'),
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: 'EL',
            dateTime: _this.state.dateTime
          }
        });
      } else if ("PUBL" === lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode')) {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL_SME_TYPE.replace(':type', 'PUBL'),
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: 'PUBL',
            dateTime: _this.state.dateTime
          }
        });
      } else if (["PHS", "GHS"].includes(lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode'))) {
        //todo phs
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL_PHS_TYPE.replace(':type', type),
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: type,
            dateTime: _this.state.dateTime
          }
        });

        return;
      } else if (lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode') === 'BOSS') {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL_SME_TYPE.replace(':type', type),
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: type,
            dateTime: _this.state.dateTime
          }
        });
      } else if (lodash__WEBPACK_IMPORTED_MODULE_5___default.a.get(item, 'productCode') === 'IAR') {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FIRE,
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            type: 'AR',
            dateTime: _this.state.dateTime
          }
        });
      } else {
        _this.props.history.push({
          pathname: _common__WEBPACK_IMPORTED_MODULE_4__["PATH"].CLAIMS_FONL,
          state: {
            policy: Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, item, {
              policyId: item.policyId,
              dateOfLoss: _this.state.dateOfLoss,
              ext: {
                claimInsured: claimInsured
              }
            }),
            contact: _this.state.dummyCustomer,
            dateTime: _this.state.dateTime
          }
        });
      }
    }
  }]);

  return ClaimsCommon;
}();

/* harmony default export */ __webpack_exports__["default"] = (new ClaimsCommon());//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svY2xhaW1zL2NvbW1vbi50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9jbGFpbXMvY29tbW9uLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHtQQVRIfSBmcm9tICdAY29tbW9uJztcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgTWFzayBmcm9tIFwiQGRlc2stY29tcG9uZW50L2NyZWF0ZS1kaWFsb2dcIjtcbmltcG9ydCBUeXBlTW9kYWwgZnJvbSBcIkBkZXNrL2NsYWltcy9mbm9sL2NyZWF0ZS1jbGFpbS9zbWUvY29tcG9uZW50cy90eXBlTW9kYWxcIjtcblxuY2xhc3MgQ2xhaW1zQ29tbW9uIHtcbiAgICBjaGVja1RpbWUoaXRlbTogYW55LCBfdGhpczogYW55KSB7XG4gICAgICAgIE1hc2suY3JlYXRlKHtcbiAgICAgICAgICAgIENvbXBvbmVudDogKHtvbkNhbmNlbCwgb25Pa306IGFueSkgPT4gKFxuICAgICAgICAgICAgICAgIDxUeXBlTW9kYWwgb25Paz17b25Pa31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXtvbkNhbmNlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ29udGludWU9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChpdGVtOiBhbnksIHR5cGU6IGFueSwgaW5zdXJlZDogYW55LCBjYW5jZWw6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uQ29udGludWUoX3RoaXMsIGl0ZW0sIHR5cGUsIGluc3VyZWQsIGNhbmNlbClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtPXtpdGVtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdD17X3RoaXN9Lz5cbiAgICAgICAgICAgICksXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBnZXRMb3NzSXRlbXMoaXRlbTogYW55LCBfdGhpczogYW55KSB7XG4gICAgICAgIGlmICghaXRlbSkgcmV0dXJuO1xuICAgICAgICB0aGlzLmNoZWNrVGltZShpdGVtLCBfdGhpcyk7XG4gICAgfVxuXG4gICAgb25Db250aW51ZShfdGhpczogYW55LCBpdGVtOiBhbnksIHR5cGU6IGFueSwgaW5zdXJlZDogYW55LCBjYW5jZWw6IGFueSkge1xuICAgICAgICBjYW5jZWwoKTtcbiAgICAgICAgY29uc3QgY2xhaW1JbnN1cmVkID0gaW5zdXJlZCA/IEpTT04ucGFyc2UoaW5zdXJlZCkgOiAnJztcbiAgICAgICAgaWYgKF8uZ2V0KGl0ZW0sICdwcm9kdWN0Q29kZScpID09PSAnRUZDJykge1xuICAgICAgICAgICAgX3RoaXMucHJvcHMuaGlzdG9yeS5wdXNoKHtcbiAgICAgICAgICAgICAgICBwYXRobmFtZTogUEFUSC5DTEFJTVNfRk9OTF9TTUVfVFlQRS5yZXBsYWNlKCc6dHlwZScsICdQQScpLFxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XG4gICAgICAgICAgICAgICAgICAgIHBvbGljeToge1xuICAgICAgICAgICAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVPZkxvc3M6IF90aGlzLnN0YXRlLmRhdGVPZkxvc3MsXG4gICAgICAgICAgICAgICAgICAgICAgICBleHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFpbUluc3VyZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjb250YWN0OiBfdGhpcy5zdGF0ZS5kdW1teUN1c3RvbWVyLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnUEEnLFxuICAgICAgICAgICAgICAgICAgICBkYXRlVGltZTogX3RoaXMuc3RhdGUuZGF0ZVRpbWUsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2UgaWYgKFwiRUxcIiA9PT0gXy5nZXQoaXRlbSwgJ3Byb2R1Y3RDb2RlJykpIHtcbiAgICAgICAgICAgIF90aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XG4gICAgICAgICAgICAgICAgcGF0aG5hbWU6IFBBVEguQ0xBSU1TX0ZPTkxfU01FX1RZUEUucmVwbGFjZSgnOnR5cGUnLCAnRUwnKSxcbiAgICAgICAgICAgICAgICBzdGF0ZToge1xuICAgICAgICAgICAgICAgICAgICBwb2xpY3k6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLml0ZW0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlT2ZMb3NzOiBfdGhpcy5zdGF0ZS5kYXRlT2ZMb3NzLFxuICAgICAgICAgICAgICAgICAgICAgICAgZXh0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhaW1JbnN1cmVkLFxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY29udGFjdDogX3RoaXMuc3RhdGUuZHVtbXlDdXN0b21lcixcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0VMJyxcbiAgICAgICAgICAgICAgICAgICAgZGF0ZVRpbWU6IF90aGlzLnN0YXRlLmRhdGVUaW1lLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmKCBcIlBVQkxcIiA9PT0gXy5nZXQoaXRlbSwgJ3Byb2R1Y3RDb2RlJykpIHtcbiAgICAgICAgICAgIF90aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XG4gICAgICAgICAgICAgICAgcGF0aG5hbWU6IFBBVEguQ0xBSU1TX0ZPTkxfU01FX1RZUEUucmVwbGFjZSgnOnR5cGUnLCAnUFVCTCcpLFxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XG4gICAgICAgICAgICAgICAgICAgIHBvbGljeToge1xuICAgICAgICAgICAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVPZkxvc3M6IF90aGlzLnN0YXRlLmRhdGVPZkxvc3MsXG4gICAgICAgICAgICAgICAgICAgICAgICBleHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFpbUluc3VyZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjb250YWN0OiBfdGhpcy5zdGF0ZS5kdW1teUN1c3RvbWVyLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnUFVCTCcsXG4gICAgICAgICAgICAgICAgICAgIGRhdGVUaW1lOiBfdGhpcy5zdGF0ZS5kYXRlVGltZSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSBpZiAoW1wiUEhTXCIsIFwiR0hTXCJdLmluY2x1ZGVzKF8uZ2V0KGl0ZW0sICdwcm9kdWN0Q29kZScpKSkge1xuICAgICAgICAgICAgLy90b2RvIHBoc1xuICAgICAgICAgICAgX3RoaXMucHJvcHMuaGlzdG9yeS5wdXNoKHtcbiAgICAgICAgICAgICAgICBwYXRobmFtZTogUEFUSC5DTEFJTVNfRk9OTF9QSFNfVFlQRS5yZXBsYWNlKCc6dHlwZScsIHR5cGUpLFxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XG4gICAgICAgICAgICAgICAgICAgIHBvbGljeToge1xuICAgICAgICAgICAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVPZkxvc3M6IF90aGlzLnN0YXRlLmRhdGVPZkxvc3MsXG4gICAgICAgICAgICAgICAgICAgICAgICBleHQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFpbUluc3VyZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjb250YWN0OiBfdGhpcy5zdGF0ZS5kdW1teUN1c3RvbWVyLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiB0eXBlLFxuICAgICAgICAgICAgICAgICAgICBkYXRlVGltZTogX3RoaXMuc3RhdGUuZGF0ZVRpbWUsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9IGVsc2UgaWYgKF8uZ2V0KGl0ZW0sICdwcm9kdWN0Q29kZScpID09PSAnQk9TUycpIHtcbiAgICAgICAgICAgIF90aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XG4gICAgICAgICAgICAgICAgcGF0aG5hbWU6IFBBVEguQ0xBSU1TX0ZPTkxfU01FX1RZUEUucmVwbGFjZSgnOnR5cGUnLCB0eXBlKSxcbiAgICAgICAgICAgICAgICBzdGF0ZToge1xuICAgICAgICAgICAgICAgICAgICBwb2xpY3k6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLml0ZW0sXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlT2ZMb3NzOiBfdGhpcy5zdGF0ZS5kYXRlT2ZMb3NzLFxuICAgICAgICAgICAgICAgICAgICAgICAgZXh0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhaW1JbnN1cmVkLFxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY29udGFjdDogX3RoaXMuc3RhdGUuZHVtbXlDdXN0b21lcixcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogdHlwZSxcbiAgICAgICAgICAgICAgICAgICAgZGF0ZVRpbWU6IF90aGlzLnN0YXRlLmRhdGVUaW1lLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIGlmIChfLmdldChpdGVtLCAncHJvZHVjdENvZGUnKSA9PT0gJ0lBUicpIHtcbiAgICAgICAgICAgIF90aGlzLnByb3BzLmhpc3RvcnkucHVzaCh7XG4gICAgICAgICAgICAgICAgcGF0aG5hbWU6IFBBVEguQ0xBSU1TX0ZJUkUsXG4gICAgICAgICAgICAgICAgc3RhdGU6IHtcbiAgICAgICAgICAgICAgICAgICAgcG9saWN5OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAuLi5pdGVtLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZU9mTG9zczogX3RoaXMuc3RhdGUuZGF0ZU9mTG9zcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4dDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYWltSW5zdXJlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNvbnRhY3Q6IF90aGlzLnN0YXRlLmR1bW15Q3VzdG9tZXIsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdBUicsXG4gICAgICAgICAgICAgICAgICAgIGRhdGVUaW1lOiBfdGhpcy5zdGF0ZS5kYXRlVGltZSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBfdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goe1xuICAgICAgICAgICAgICAgIHBhdGhuYW1lOiBQQVRILkNMQUlNU19GT05MLFxuICAgICAgICAgICAgICAgIHN0YXRlOiB7XG4gICAgICAgICAgICAgICAgICAgIHBvbGljeToge1xuICAgICAgICAgICAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvbGljeUlkOiBpdGVtLnBvbGljeUlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZU9mTG9zczogX3RoaXMuc3RhdGUuZGF0ZU9mTG9zcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGV4dDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYWltSW5zdXJlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNvbnRhY3Q6IF90aGlzLnN0YXRlLmR1bW15Q3VzdG9tZXIsXG4gICAgICAgICAgICAgICAgICAgIGRhdGVUaW1lOiBfdGhpcy5zdGF0ZS5kYXRlVGltZSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgIH07XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgbmV3IENsYWltc0NvbW1vbigpO1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBUkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFEQTtBQWFBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUhBO0FBT0E7QUFDQTtBQUNBO0FBVkE7QUFGQTtBQUNBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUpBO0FBUUE7QUFDQTtBQVZBO0FBRkE7QUFlQTtBQUVBOzs7Ozs7QUFJQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./src/app/desk/claims/common.tsx
