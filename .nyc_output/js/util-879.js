__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findPopupContainer", function() { return findPopupContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toTitle", function() { return toTitle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toArray", function() { return toArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createRef", function() { return createRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UNSELECTABLE_STYLE", function() { return UNSELECTABLE_STYLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UNSELECTABLE_ATTRIBUTE", function() { return UNSELECTABLE_ATTRIBUTE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flatToHierarchy", function() { return flatToHierarchy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resetAriaId", function() { return resetAriaId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generateAriaId", function() { return generateAriaId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isLabelInValue", function() { return isLabelInValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseSimpleTreeData", function() { return parseSimpleTreeData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPosRelated", function() { return isPosRelated; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cleanEntity", function() { return cleanEntity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFilterTree", function() { return getFilterTree; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatInternalValue", function() { return formatInternalValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLabel", function() { return getLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "formatSelectorValue", function() { return formatSelectorValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertDataToTree", function() { return convertDataToTree; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "convertTreeToEntities", function() { return convertTreeToEntities; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHalfCheckedKeys", function() { return getHalfCheckedKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "conductCheck", function() { return conductCheck; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! warning */ "./node_modules/warning/warning.js");
/* harmony import */ var warning__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(warning__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rc_tree_es_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rc-tree/es/util */ "./node_modules/rc-tree/es/util.js");
/* harmony import */ var rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rc-util/es/Children/toArray */ "./node_modules/rc-util/es/Children/toArray.js");
/* harmony import */ var rc_util_es_Dom_class__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rc-util/es/Dom/class */ "./node_modules/rc-util/es/Dom/class.js");
/* harmony import */ var _strategies__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./strategies */ "./node_modules/rc-tree-select/es/strategies.js");
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly) symbols = symbols.filter(function (sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}







var warnDeprecatedLabel = false; // =================== DOM =====================

function findPopupContainer(node, prefixClass) {
  var current = node;

  while (current) {
    if (Object(rc_util_es_Dom_class__WEBPACK_IMPORTED_MODULE_4__["hasClass"])(current, prefixClass)) {
      return current;
    }

    current = current.parentNode;
  }

  return null;
} // =================== MISC ====================

function toTitle(title) {
  if (typeof title === 'string') {
    return title;
  }

  return null;
}
function toArray(data) {
  if (data === undefined || data === null) return [];
  return Array.isArray(data) ? data : [data];
} // Shallow copy of React 16.3 createRef api

function createRef() {
  var func = function setRef(node) {
    func.current = node;
  };

  return func;
} // =============== Legacy ===============

var UNSELECTABLE_STYLE = {
  userSelect: 'none',
  WebkitUserSelect: 'none'
};
var UNSELECTABLE_ATTRIBUTE = {
  unselectable: 'unselectable'
};
/**
 * Convert position list to hierarchy structure.
 * This is little hack since use '-' to split the position.
 */

function flatToHierarchy(positionList) {
  if (!positionList.length) {
    return [];
  }

  var entrances = {}; // Prepare the position map

  var posMap = {};
  var parsedList = positionList.slice().map(function (entity) {
    var clone = _objectSpread({}, entity, {
      fields: entity.pos.split('-')
    });

    delete clone.children;
    return clone;
  });
  parsedList.forEach(function (entity) {
    posMap[entity.pos] = entity;
  });
  parsedList.sort(function (a, b) {
    return a.fields.length - b.fields.length;
  }); // Create the hierarchy

  parsedList.forEach(function (entity) {
    var parentPos = entity.fields.slice(0, -1).join('-');
    var parentEntity = posMap[parentPos];

    if (!parentEntity) {
      entrances[entity.pos] = entity;
    } else {
      parentEntity.children = parentEntity.children || [];
      parentEntity.children.push(entity);
    } // Some time position list provide `key`, we don't need it


    delete entity.key;
    delete entity.fields;
  });
  return Object.keys(entrances).map(function (key) {
    return entrances[key];
  });
} // =============== Accessibility ===============

var ariaId = 0;
function resetAriaId() {
  ariaId = 0;
}
function generateAriaId(prefix) {
  ariaId += 1;
  return "".concat(prefix, "_").concat(ariaId);
}
function isLabelInValue(props) {
  var treeCheckable = props.treeCheckable,
      treeCheckStrictly = props.treeCheckStrictly,
      labelInValue = props.labelInValue;

  if (treeCheckable && treeCheckStrictly) {
    return true;
  }

  return labelInValue || false;
} // =================== Tree ====================

function parseSimpleTreeData(treeData, _ref) {
  var id = _ref.id,
      pId = _ref.pId,
      rootPId = _ref.rootPId;
  var keyNodes = {};
  var rootNodeList = []; // Fill in the map

  var nodeList = treeData.map(function (node) {
    var clone = _objectSpread({}, node);

    var key = clone[id];
    keyNodes[key] = clone;
    clone.key = clone.key || key;
    return clone;
  }); // Connect tree

  nodeList.forEach(function (node) {
    var parentKey = node[pId];
    var parent = keyNodes[parentKey]; // Fill parent

    if (parent) {
      parent.children = parent.children || [];
      parent.children.push(node);
    } // Fill root tree node


    if (parentKey === rootPId || !parent && rootPId === null) {
      rootNodeList.push(node);
    }
  });
  return rootNodeList;
}
/**
 * Detect if position has relation.
 * e.g. 1-2 related with 1-2-3
 * e.g. 1-3-2 related with 1
 * e.g. 1-2 not related with 1-21
 */

function isPosRelated(pos1, pos2) {
  var fields1 = pos1.split('-');
  var fields2 = pos2.split('-');
  var minLen = Math.min(fields1.length, fields2.length);

  for (var i = 0; i < minLen; i += 1) {
    if (fields1[i] !== fields2[i]) {
      return false;
    }
  }

  return true;
}
/**
 * This function is only used on treeNode check (none treeCheckStrictly but has searchInput).
 * We convert entity to { node, pos, children } format.
 * This is legacy bug but we still need to do with it.
 * @param entity
 */

function cleanEntity(_ref2) {
  var node = _ref2.node,
      pos = _ref2.pos,
      children = _ref2.children;
  var instance = {
    node: node,
    pos: pos
  };

  if (children) {
    instance.children = children.map(cleanEntity);
  }

  return instance;
}
/**
 * Get a filtered TreeNode list by provided treeNodes.
 * [Legacy] Since `Tree` use `key` as map but `key` will changed by React,
 * we have to convert `treeNodes > data > treeNodes` to keep the key.
 * Such performance hungry!
 *
 * We pass `Component` as argument is to fix eslint issue.
 */

function getFilterTree(treeNodes, searchValue, filterFunc, valueEntities, Component) {
  if (!searchValue) {
    return null;
  }

  function mapFilteredNodeToData(node) {
    if (!node) return null;
    var match = false;

    if (filterFunc(searchValue, node)) {
      match = true;
    }

    var children = Object(rc_util_es_Children_toArray__WEBPACK_IMPORTED_MODULE_3__["default"])(node.props.children).map(mapFilteredNodeToData).filter(function (n) {
      return n;
    });

    if (children.length || match) {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, _extends({}, node.props, {
        key: valueEntities[node.props.value].key
      }), children);
    }

    return null;
  }

  return treeNodes.map(mapFilteredNodeToData).filter(function (node) {
    return node;
  });
} // =================== Value ===================

/**
 * Convert value to array format to make logic simplify.
 */

function formatInternalValue(value, props) {
  var valueList = toArray(value); // Parse label in value

  if (isLabelInValue(props)) {
    return valueList.map(function (val) {
      if (typeof val !== 'object' || !val) {
        return {
          value: '',
          label: ''
        };
      }

      return val;
    });
  }

  return valueList.map(function (val) {
    return {
      value: val
    };
  });
}
function getLabel(wrappedValue, entity, treeNodeLabelProp) {
  if (wrappedValue.label) {
    return wrappedValue.label;
  }

  if (entity && entity.node.props) {
    return entity.node.props[treeNodeLabelProp];
  } // Since value without entity will be in missValueList.
  // This code will never reached, but we still need this in case.


  return wrappedValue.value;
}
/**
 * Convert internal state `valueList` to user needed value list.
 * This will return an array list. You need check if is not multiple when return.
 *
 * `allCheckedNodes` is used for `treeCheckStrictly`
 */

function formatSelectorValue(valueList, props, valueEntities) {
  var treeNodeLabelProp = props.treeNodeLabelProp,
      treeCheckable = props.treeCheckable,
      treeCheckStrictly = props.treeCheckStrictly,
      showCheckedStrategy = props.showCheckedStrategy; // Will hide some value if `showCheckedStrategy` is set

  if (treeCheckable && !treeCheckStrictly) {
    var values = {};
    valueList.forEach(function (wrappedValue) {
      values[wrappedValue.value] = wrappedValue;
    });
    var hierarchyList = flatToHierarchy(valueList.map(function (_ref3) {
      var value = _ref3.value;
      return valueEntities[value];
    }));

    if (showCheckedStrategy === _strategies__WEBPACK_IMPORTED_MODULE_5__["SHOW_PARENT"]) {
      // Only get the parent checked value
      return hierarchyList.map(function (_ref4) {
        var value = _ref4.node.props.value;
        return {
          label: getLabel(values[value], valueEntities[value], treeNodeLabelProp),
          value: value
        };
      });
    }

    if (showCheckedStrategy === _strategies__WEBPACK_IMPORTED_MODULE_5__["SHOW_CHILD"]) {
      // Only get the children checked value
      var targetValueList = []; // Find the leaf children

      var traverse = function traverse(_ref5) {
        var value = _ref5.node.props.value,
            children = _ref5.children;

        if (!children || children.length === 0) {
          targetValueList.push({
            label: getLabel(values[value], valueEntities[value], treeNodeLabelProp),
            value: value
          });
          return;
        }

        children.forEach(function (entity) {
          traverse(entity);
        });
      };

      hierarchyList.forEach(function (entity) {
        traverse(entity);
      });
      return targetValueList;
    }
  }

  return valueList.map(function (wrappedValue) {
    return {
      label: getLabel(wrappedValue, valueEntities[wrappedValue.value], treeNodeLabelProp),
      value: wrappedValue.value
    };
  });
}
/**
 * Use `rc-tree` convertDataToTree to convert treeData to TreeNodes.
 * This will change the label to title value
 */

function processProps(props) {
  var title = props.title,
      label = props.label,
      key = props.key,
      value = props.value;

  var cloneProps = _objectSpread({}, props); // Warning user not to use deprecated label prop.


  if (label && !title) {
    if (!warnDeprecatedLabel) {
      warning__WEBPACK_IMPORTED_MODULE_1___default()(false, "'label' in treeData is deprecated. Please use 'title' instead.");
      warnDeprecatedLabel = true;
    }

    cloneProps.title = label;
  }

  if (!key) {
    cloneProps.key = value;
  }

  return cloneProps;
}

function convertDataToTree(treeData) {
  return Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_2__["convertDataToTree"])(treeData, {
    processProps: processProps
  });
}
/**
 * Use `rc-tree` convertTreeToEntities for entities calculation.
 * We have additional entities of `valueEntities`
 */

function initWrapper(wrapper) {
  return _objectSpread({}, wrapper, {
    valueEntities: {}
  });
}

function processEntity(entity, wrapper) {
  var value = entity.node.props.value;
  entity.value = value; // This should be empty, or will get error message.

  var currentEntity = wrapper.valueEntities[value];

  if (currentEntity) {
    warning__WEBPACK_IMPORTED_MODULE_1___default()(false, "Conflict! value of node '".concat(entity.key, "' (").concat(value, ") has already used by node '").concat(currentEntity.key, "'."));
  }

  wrapper.valueEntities[value] = entity;
}

function convertTreeToEntities(treeNodes) {
  return Object(rc_tree_es_util__WEBPACK_IMPORTED_MODULE_2__["convertTreeToEntities"])(treeNodes, {
    initWrapper: initWrapper,
    processEntity: processEntity
  });
}
/**
 * https://github.com/ant-design/ant-design/issues/13328
 * We need calculate the half check key when searchValue is set.
 */
// TODO: This logic may better move to rc-tree

function getHalfCheckedKeys(valueList, valueEntities) {
  var values = {}; // Fill checked keys

  valueList.forEach(function (_ref6) {
    var value = _ref6.value;
    values[value] = false;
  }); // Fill half checked keys

  valueList.forEach(function (_ref7) {
    var value = _ref7.value;
    var current = valueEntities[value];

    while (current && current.parent) {
      var parentValue = current.parent.value;
      if (parentValue in values) break;
      values[parentValue] = true;
      current = current.parent;
    }
  }); // Get half keys

  return Object.keys(values).filter(function (value) {
    return values[value];
  }).map(function (value) {
    return valueEntities[value].key;
  });
}
var conductCheck = rc_tree_es_util__WEBPACK_IMPORTED_MODULE_2__["conductCheck"];//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtdHJlZS1zZWxlY3QvZXMvdXRpbC5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL3JjLXRyZWUtc2VsZWN0L2VzL3V0aWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gX2V4dGVuZHMoKSB7IF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTsgcmV0dXJuIF9leHRlbmRzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH1cblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7IHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTsgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHsgdmFyIHN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iamVjdCk7IGlmIChlbnVtZXJhYmxlT25seSkgc3ltYm9scyA9IHN5bWJvbHMuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHsgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBzeW0pLmVudW1lcmFibGU7IH0pOyBrZXlzLnB1c2guYXBwbHkoa2V5cywgc3ltYm9scyk7IH0gcmV0dXJuIGtleXM7IH1cblxuZnVuY3Rpb24gX29iamVjdFNwcmVhZCh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXSAhPSBudWxsID8gYXJndW1lbnRzW2ldIDoge307IGlmIChpICUgMikgeyBvd25LZXlzKE9iamVjdChzb3VyY2UpLCB0cnVlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHsgX2RlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBzb3VyY2Vba2V5XSk7IH0pOyB9IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcnMoc291cmNlKSk7IH0gZWxzZSB7IG93bktleXMoT2JqZWN0KHNvdXJjZSkpLmZvckVhY2goZnVuY3Rpb24gKGtleSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc291cmNlLCBrZXkpKTsgfSk7IH0gfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwgdmFsdWUpIHsgaWYgKGtleSBpbiBvYmopIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7IHZhbHVlOiB2YWx1ZSwgZW51bWVyYWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlLCB3cml0YWJsZTogdHJ1ZSB9KTsgfSBlbHNlIHsgb2JqW2tleV0gPSB2YWx1ZTsgfSByZXR1cm4gb2JqOyB9XG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgd2FybmluZyBmcm9tICd3YXJuaW5nJztcbmltcG9ydCB7IGNvbnZlcnREYXRhVG9UcmVlIGFzIHJjQ29udmVydERhdGFUb1RyZWUsIGNvbnZlcnRUcmVlVG9FbnRpdGllcyBhcyByY0NvbnZlcnRUcmVlVG9FbnRpdGllcywgY29uZHVjdENoZWNrIGFzIHJjQ29uZHVjdENoZWNrIH0gZnJvbSBcInJjLXRyZWUvZXMvdXRpbFwiO1xuaW1wb3J0IHRvTm9kZUFycmF5IGZyb20gXCJyYy11dGlsL2VzL0NoaWxkcmVuL3RvQXJyYXlcIjtcbmltcG9ydCB7IGhhc0NsYXNzIH0gZnJvbSBcInJjLXV0aWwvZXMvRG9tL2NsYXNzXCI7XG5pbXBvcnQgeyBTSE9XX0NISUxELCBTSE9XX1BBUkVOVCB9IGZyb20gJy4vc3RyYXRlZ2llcyc7XG52YXIgd2FybkRlcHJlY2F0ZWRMYWJlbCA9IGZhbHNlOyAvLyA9PT09PT09PT09PT09PT09PT09IERPTSA9PT09PT09PT09PT09PT09PT09PT1cblxuZXhwb3J0IGZ1bmN0aW9uIGZpbmRQb3B1cENvbnRhaW5lcihub2RlLCBwcmVmaXhDbGFzcykge1xuICB2YXIgY3VycmVudCA9IG5vZGU7XG5cbiAgd2hpbGUgKGN1cnJlbnQpIHtcbiAgICBpZiAoaGFzQ2xhc3MoY3VycmVudCwgcHJlZml4Q2xhc3MpKSB7XG4gICAgICByZXR1cm4gY3VycmVudDtcbiAgICB9XG5cbiAgICBjdXJyZW50ID0gY3VycmVudC5wYXJlbnROb2RlO1xuICB9XG5cbiAgcmV0dXJuIG51bGw7XG59IC8vID09PT09PT09PT09PT09PT09PT0gTUlTQyA9PT09PT09PT09PT09PT09PT09PVxuXG5leHBvcnQgZnVuY3Rpb24gdG9UaXRsZSh0aXRsZSkge1xuICBpZiAodHlwZW9mIHRpdGxlID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiB0aXRsZTtcbiAgfVxuXG4gIHJldHVybiBudWxsO1xufVxuZXhwb3J0IGZ1bmN0aW9uIHRvQXJyYXkoZGF0YSkge1xuICBpZiAoZGF0YSA9PT0gdW5kZWZpbmVkIHx8IGRhdGEgPT09IG51bGwpIHJldHVybiBbXTtcbiAgcmV0dXJuIEFycmF5LmlzQXJyYXkoZGF0YSkgPyBkYXRhIDogW2RhdGFdO1xufSAvLyBTaGFsbG93IGNvcHkgb2YgUmVhY3QgMTYuMyBjcmVhdGVSZWYgYXBpXG5cbmV4cG9ydCBmdW5jdGlvbiBjcmVhdGVSZWYoKSB7XG4gIHZhciBmdW5jID0gZnVuY3Rpb24gc2V0UmVmKG5vZGUpIHtcbiAgICBmdW5jLmN1cnJlbnQgPSBub2RlO1xuICB9O1xuXG4gIHJldHVybiBmdW5jO1xufSAvLyA9PT09PT09PT09PT09PT0gTGVnYWN5ID09PT09PT09PT09PT09PVxuXG5leHBvcnQgdmFyIFVOU0VMRUNUQUJMRV9TVFlMRSA9IHtcbiAgdXNlclNlbGVjdDogJ25vbmUnLFxuICBXZWJraXRVc2VyU2VsZWN0OiAnbm9uZSdcbn07XG5leHBvcnQgdmFyIFVOU0VMRUNUQUJMRV9BVFRSSUJVVEUgPSB7XG4gIHVuc2VsZWN0YWJsZTogJ3Vuc2VsZWN0YWJsZSdcbn07XG4vKipcbiAqIENvbnZlcnQgcG9zaXRpb24gbGlzdCB0byBoaWVyYXJjaHkgc3RydWN0dXJlLlxuICogVGhpcyBpcyBsaXR0bGUgaGFjayBzaW5jZSB1c2UgJy0nIHRvIHNwbGl0IHRoZSBwb3NpdGlvbi5cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gZmxhdFRvSGllcmFyY2h5KHBvc2l0aW9uTGlzdCkge1xuICBpZiAoIXBvc2l0aW9uTGlzdC5sZW5ndGgpIHtcbiAgICByZXR1cm4gW107XG4gIH1cblxuICB2YXIgZW50cmFuY2VzID0ge307IC8vIFByZXBhcmUgdGhlIHBvc2l0aW9uIG1hcFxuXG4gIHZhciBwb3NNYXAgPSB7fTtcbiAgdmFyIHBhcnNlZExpc3QgPSBwb3NpdGlvbkxpc3Quc2xpY2UoKS5tYXAoZnVuY3Rpb24gKGVudGl0eSkge1xuICAgIHZhciBjbG9uZSA9IF9vYmplY3RTcHJlYWQoe30sIGVudGl0eSwge1xuICAgICAgZmllbGRzOiBlbnRpdHkucG9zLnNwbGl0KCctJylcbiAgICB9KTtcblxuICAgIGRlbGV0ZSBjbG9uZS5jaGlsZHJlbjtcbiAgICByZXR1cm4gY2xvbmU7XG4gIH0pO1xuICBwYXJzZWRMaXN0LmZvckVhY2goZnVuY3Rpb24gKGVudGl0eSkge1xuICAgIHBvc01hcFtlbnRpdHkucG9zXSA9IGVudGl0eTtcbiAgfSk7XG4gIHBhcnNlZExpc3Quc29ydChmdW5jdGlvbiAoYSwgYikge1xuICAgIHJldHVybiBhLmZpZWxkcy5sZW5ndGggLSBiLmZpZWxkcy5sZW5ndGg7XG4gIH0pOyAvLyBDcmVhdGUgdGhlIGhpZXJhcmNoeVxuXG4gIHBhcnNlZExpc3QuZm9yRWFjaChmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgdmFyIHBhcmVudFBvcyA9IGVudGl0eS5maWVsZHMuc2xpY2UoMCwgLTEpLmpvaW4oJy0nKTtcbiAgICB2YXIgcGFyZW50RW50aXR5ID0gcG9zTWFwW3BhcmVudFBvc107XG5cbiAgICBpZiAoIXBhcmVudEVudGl0eSkge1xuICAgICAgZW50cmFuY2VzW2VudGl0eS5wb3NdID0gZW50aXR5O1xuICAgIH0gZWxzZSB7XG4gICAgICBwYXJlbnRFbnRpdHkuY2hpbGRyZW4gPSBwYXJlbnRFbnRpdHkuY2hpbGRyZW4gfHwgW107XG4gICAgICBwYXJlbnRFbnRpdHkuY2hpbGRyZW4ucHVzaChlbnRpdHkpO1xuICAgIH0gLy8gU29tZSB0aW1lIHBvc2l0aW9uIGxpc3QgcHJvdmlkZSBga2V5YCwgd2UgZG9uJ3QgbmVlZCBpdFxuXG5cbiAgICBkZWxldGUgZW50aXR5LmtleTtcbiAgICBkZWxldGUgZW50aXR5LmZpZWxkcztcbiAgfSk7XG4gIHJldHVybiBPYmplY3Qua2V5cyhlbnRyYW5jZXMpLm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgcmV0dXJuIGVudHJhbmNlc1trZXldO1xuICB9KTtcbn0gLy8gPT09PT09PT09PT09PT09IEFjY2Vzc2liaWxpdHkgPT09PT09PT09PT09PT09XG5cbnZhciBhcmlhSWQgPSAwO1xuZXhwb3J0IGZ1bmN0aW9uIHJlc2V0QXJpYUlkKCkge1xuICBhcmlhSWQgPSAwO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdlbmVyYXRlQXJpYUlkKHByZWZpeCkge1xuICBhcmlhSWQgKz0gMTtcbiAgcmV0dXJuIFwiXCIuY29uY2F0KHByZWZpeCwgXCJfXCIpLmNvbmNhdChhcmlhSWQpO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGlzTGFiZWxJblZhbHVlKHByb3BzKSB7XG4gIHZhciB0cmVlQ2hlY2thYmxlID0gcHJvcHMudHJlZUNoZWNrYWJsZSxcbiAgICAgIHRyZWVDaGVja1N0cmljdGx5ID0gcHJvcHMudHJlZUNoZWNrU3RyaWN0bHksXG4gICAgICBsYWJlbEluVmFsdWUgPSBwcm9wcy5sYWJlbEluVmFsdWU7XG5cbiAgaWYgKHRyZWVDaGVja2FibGUgJiYgdHJlZUNoZWNrU3RyaWN0bHkpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHJldHVybiBsYWJlbEluVmFsdWUgfHwgZmFsc2U7XG59IC8vID09PT09PT09PT09PT09PT09PT0gVHJlZSA9PT09PT09PT09PT09PT09PT09PVxuXG5leHBvcnQgZnVuY3Rpb24gcGFyc2VTaW1wbGVUcmVlRGF0YSh0cmVlRGF0YSwgX3JlZikge1xuICB2YXIgaWQgPSBfcmVmLmlkLFxuICAgICAgcElkID0gX3JlZi5wSWQsXG4gICAgICByb290UElkID0gX3JlZi5yb290UElkO1xuICB2YXIga2V5Tm9kZXMgPSB7fTtcbiAgdmFyIHJvb3ROb2RlTGlzdCA9IFtdOyAvLyBGaWxsIGluIHRoZSBtYXBcblxuICB2YXIgbm9kZUxpc3QgPSB0cmVlRGF0YS5tYXAoZnVuY3Rpb24gKG5vZGUpIHtcbiAgICB2YXIgY2xvbmUgPSBfb2JqZWN0U3ByZWFkKHt9LCBub2RlKTtcblxuICAgIHZhciBrZXkgPSBjbG9uZVtpZF07XG4gICAga2V5Tm9kZXNba2V5XSA9IGNsb25lO1xuICAgIGNsb25lLmtleSA9IGNsb25lLmtleSB8fCBrZXk7XG4gICAgcmV0dXJuIGNsb25lO1xuICB9KTsgLy8gQ29ubmVjdCB0cmVlXG5cbiAgbm9kZUxpc3QuZm9yRWFjaChmdW5jdGlvbiAobm9kZSkge1xuICAgIHZhciBwYXJlbnRLZXkgPSBub2RlW3BJZF07XG4gICAgdmFyIHBhcmVudCA9IGtleU5vZGVzW3BhcmVudEtleV07IC8vIEZpbGwgcGFyZW50XG5cbiAgICBpZiAocGFyZW50KSB7XG4gICAgICBwYXJlbnQuY2hpbGRyZW4gPSBwYXJlbnQuY2hpbGRyZW4gfHwgW107XG4gICAgICBwYXJlbnQuY2hpbGRyZW4ucHVzaChub2RlKTtcbiAgICB9IC8vIEZpbGwgcm9vdCB0cmVlIG5vZGVcblxuXG4gICAgaWYgKHBhcmVudEtleSA9PT0gcm9vdFBJZCB8fCAhcGFyZW50ICYmIHJvb3RQSWQgPT09IG51bGwpIHtcbiAgICAgIHJvb3ROb2RlTGlzdC5wdXNoKG5vZGUpO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiByb290Tm9kZUxpc3Q7XG59XG4vKipcbiAqIERldGVjdCBpZiBwb3NpdGlvbiBoYXMgcmVsYXRpb24uXG4gKiBlLmcuIDEtMiByZWxhdGVkIHdpdGggMS0yLTNcbiAqIGUuZy4gMS0zLTIgcmVsYXRlZCB3aXRoIDFcbiAqIGUuZy4gMS0yIG5vdCByZWxhdGVkIHdpdGggMS0yMVxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBpc1Bvc1JlbGF0ZWQocG9zMSwgcG9zMikge1xuICB2YXIgZmllbGRzMSA9IHBvczEuc3BsaXQoJy0nKTtcbiAgdmFyIGZpZWxkczIgPSBwb3MyLnNwbGl0KCctJyk7XG4gIHZhciBtaW5MZW4gPSBNYXRoLm1pbihmaWVsZHMxLmxlbmd0aCwgZmllbGRzMi5sZW5ndGgpO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbWluTGVuOyBpICs9IDEpIHtcbiAgICBpZiAoZmllbGRzMVtpXSAhPT0gZmllbGRzMltpXSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufVxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIGlzIG9ubHkgdXNlZCBvbiB0cmVlTm9kZSBjaGVjayAobm9uZSB0cmVlQ2hlY2tTdHJpY3RseSBidXQgaGFzIHNlYXJjaElucHV0KS5cbiAqIFdlIGNvbnZlcnQgZW50aXR5IHRvIHsgbm9kZSwgcG9zLCBjaGlsZHJlbiB9IGZvcm1hdC5cbiAqIFRoaXMgaXMgbGVnYWN5IGJ1ZyBidXQgd2Ugc3RpbGwgbmVlZCB0byBkbyB3aXRoIGl0LlxuICogQHBhcmFtIGVudGl0eVxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBjbGVhbkVudGl0eShfcmVmMikge1xuICB2YXIgbm9kZSA9IF9yZWYyLm5vZGUsXG4gICAgICBwb3MgPSBfcmVmMi5wb3MsXG4gICAgICBjaGlsZHJlbiA9IF9yZWYyLmNoaWxkcmVuO1xuICB2YXIgaW5zdGFuY2UgPSB7XG4gICAgbm9kZTogbm9kZSxcbiAgICBwb3M6IHBvc1xuICB9O1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIGluc3RhbmNlLmNoaWxkcmVuID0gY2hpbGRyZW4ubWFwKGNsZWFuRW50aXR5KTtcbiAgfVxuXG4gIHJldHVybiBpbnN0YW5jZTtcbn1cbi8qKlxuICogR2V0IGEgZmlsdGVyZWQgVHJlZU5vZGUgbGlzdCBieSBwcm92aWRlZCB0cmVlTm9kZXMuXG4gKiBbTGVnYWN5XSBTaW5jZSBgVHJlZWAgdXNlIGBrZXlgIGFzIG1hcCBidXQgYGtleWAgd2lsbCBjaGFuZ2VkIGJ5IFJlYWN0LFxuICogd2UgaGF2ZSB0byBjb252ZXJ0IGB0cmVlTm9kZXMgPiBkYXRhID4gdHJlZU5vZGVzYCB0byBrZWVwIHRoZSBrZXkuXG4gKiBTdWNoIHBlcmZvcm1hbmNlIGh1bmdyeSFcbiAqXG4gKiBXZSBwYXNzIGBDb21wb25lbnRgIGFzIGFyZ3VtZW50IGlzIHRvIGZpeCBlc2xpbnQgaXNzdWUuXG4gKi9cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEZpbHRlclRyZWUodHJlZU5vZGVzLCBzZWFyY2hWYWx1ZSwgZmlsdGVyRnVuYywgdmFsdWVFbnRpdGllcywgQ29tcG9uZW50KSB7XG4gIGlmICghc2VhcmNoVmFsdWUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIG1hcEZpbHRlcmVkTm9kZVRvRGF0YShub2RlKSB7XG4gICAgaWYgKCFub2RlKSByZXR1cm4gbnVsbDtcbiAgICB2YXIgbWF0Y2ggPSBmYWxzZTtcblxuICAgIGlmIChmaWx0ZXJGdW5jKHNlYXJjaFZhbHVlLCBub2RlKSkge1xuICAgICAgbWF0Y2ggPSB0cnVlO1xuICAgIH1cblxuICAgIHZhciBjaGlsZHJlbiA9IHRvTm9kZUFycmF5KG5vZGUucHJvcHMuY2hpbGRyZW4pLm1hcChtYXBGaWx0ZXJlZE5vZGVUb0RhdGEpLmZpbHRlcihmdW5jdGlvbiAobikge1xuICAgICAgcmV0dXJuIG47XG4gICAgfSk7XG5cbiAgICBpZiAoY2hpbGRyZW4ubGVuZ3RoIHx8IG1hdGNoKSB7XG4gICAgICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIF9leHRlbmRzKHt9LCBub2RlLnByb3BzLCB7XG4gICAgICAgIGtleTogdmFsdWVFbnRpdGllc1tub2RlLnByb3BzLnZhbHVlXS5rZXlcbiAgICAgIH0pLCBjaGlsZHJlbik7XG4gICAgfVxuXG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICByZXR1cm4gdHJlZU5vZGVzLm1hcChtYXBGaWx0ZXJlZE5vZGVUb0RhdGEpLmZpbHRlcihmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBub2RlO1xuICB9KTtcbn0gLy8gPT09PT09PT09PT09PT09PT09PSBWYWx1ZSA9PT09PT09PT09PT09PT09PT09XG5cbi8qKlxuICogQ29udmVydCB2YWx1ZSB0byBhcnJheSBmb3JtYXQgdG8gbWFrZSBsb2dpYyBzaW1wbGlmeS5cbiAqL1xuXG5leHBvcnQgZnVuY3Rpb24gZm9ybWF0SW50ZXJuYWxWYWx1ZSh2YWx1ZSwgcHJvcHMpIHtcbiAgdmFyIHZhbHVlTGlzdCA9IHRvQXJyYXkodmFsdWUpOyAvLyBQYXJzZSBsYWJlbCBpbiB2YWx1ZVxuXG4gIGlmIChpc0xhYmVsSW5WYWx1ZShwcm9wcykpIHtcbiAgICByZXR1cm4gdmFsdWVMaXN0Lm1hcChmdW5jdGlvbiAodmFsKSB7XG4gICAgICBpZiAodHlwZW9mIHZhbCAhPT0gJ29iamVjdCcgfHwgIXZhbCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHZhbHVlOiAnJyxcbiAgICAgICAgICBsYWJlbDogJydcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHZhbDtcbiAgICB9KTtcbiAgfVxuXG4gIHJldHVybiB2YWx1ZUxpc3QubWFwKGZ1bmN0aW9uICh2YWwpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdmFsdWU6IHZhbFxuICAgIH07XG4gIH0pO1xufVxuZXhwb3J0IGZ1bmN0aW9uIGdldExhYmVsKHdyYXBwZWRWYWx1ZSwgZW50aXR5LCB0cmVlTm9kZUxhYmVsUHJvcCkge1xuICBpZiAod3JhcHBlZFZhbHVlLmxhYmVsKSB7XG4gICAgcmV0dXJuIHdyYXBwZWRWYWx1ZS5sYWJlbDtcbiAgfVxuXG4gIGlmIChlbnRpdHkgJiYgZW50aXR5Lm5vZGUucHJvcHMpIHtcbiAgICByZXR1cm4gZW50aXR5Lm5vZGUucHJvcHNbdHJlZU5vZGVMYWJlbFByb3BdO1xuICB9IC8vIFNpbmNlIHZhbHVlIHdpdGhvdXQgZW50aXR5IHdpbGwgYmUgaW4gbWlzc1ZhbHVlTGlzdC5cbiAgLy8gVGhpcyBjb2RlIHdpbGwgbmV2ZXIgcmVhY2hlZCwgYnV0IHdlIHN0aWxsIG5lZWQgdGhpcyBpbiBjYXNlLlxuXG5cbiAgcmV0dXJuIHdyYXBwZWRWYWx1ZS52YWx1ZTtcbn1cbi8qKlxuICogQ29udmVydCBpbnRlcm5hbCBzdGF0ZSBgdmFsdWVMaXN0YCB0byB1c2VyIG5lZWRlZCB2YWx1ZSBsaXN0LlxuICogVGhpcyB3aWxsIHJldHVybiBhbiBhcnJheSBsaXN0LiBZb3UgbmVlZCBjaGVjayBpZiBpcyBub3QgbXVsdGlwbGUgd2hlbiByZXR1cm4uXG4gKlxuICogYGFsbENoZWNrZWROb2Rlc2AgaXMgdXNlZCBmb3IgYHRyZWVDaGVja1N0cmljdGx5YFxuICovXG5cbmV4cG9ydCBmdW5jdGlvbiBmb3JtYXRTZWxlY3RvclZhbHVlKHZhbHVlTGlzdCwgcHJvcHMsIHZhbHVlRW50aXRpZXMpIHtcbiAgdmFyIHRyZWVOb2RlTGFiZWxQcm9wID0gcHJvcHMudHJlZU5vZGVMYWJlbFByb3AsXG4gICAgICB0cmVlQ2hlY2thYmxlID0gcHJvcHMudHJlZUNoZWNrYWJsZSxcbiAgICAgIHRyZWVDaGVja1N0cmljdGx5ID0gcHJvcHMudHJlZUNoZWNrU3RyaWN0bHksXG4gICAgICBzaG93Q2hlY2tlZFN0cmF0ZWd5ID0gcHJvcHMuc2hvd0NoZWNrZWRTdHJhdGVneTsgLy8gV2lsbCBoaWRlIHNvbWUgdmFsdWUgaWYgYHNob3dDaGVja2VkU3RyYXRlZ3lgIGlzIHNldFxuXG4gIGlmICh0cmVlQ2hlY2thYmxlICYmICF0cmVlQ2hlY2tTdHJpY3RseSkge1xuICAgIHZhciB2YWx1ZXMgPSB7fTtcbiAgICB2YWx1ZUxpc3QuZm9yRWFjaChmdW5jdGlvbiAod3JhcHBlZFZhbHVlKSB7XG4gICAgICB2YWx1ZXNbd3JhcHBlZFZhbHVlLnZhbHVlXSA9IHdyYXBwZWRWYWx1ZTtcbiAgICB9KTtcbiAgICB2YXIgaGllcmFyY2h5TGlzdCA9IGZsYXRUb0hpZXJhcmNoeSh2YWx1ZUxpc3QubWFwKGZ1bmN0aW9uIChfcmVmMykge1xuICAgICAgdmFyIHZhbHVlID0gX3JlZjMudmFsdWU7XG4gICAgICByZXR1cm4gdmFsdWVFbnRpdGllc1t2YWx1ZV07XG4gICAgfSkpO1xuXG4gICAgaWYgKHNob3dDaGVja2VkU3RyYXRlZ3kgPT09IFNIT1dfUEFSRU5UKSB7XG4gICAgICAvLyBPbmx5IGdldCB0aGUgcGFyZW50IGNoZWNrZWQgdmFsdWVcbiAgICAgIHJldHVybiBoaWVyYXJjaHlMaXN0Lm1hcChmdW5jdGlvbiAoX3JlZjQpIHtcbiAgICAgICAgdmFyIHZhbHVlID0gX3JlZjQubm9kZS5wcm9wcy52YWx1ZTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBsYWJlbDogZ2V0TGFiZWwodmFsdWVzW3ZhbHVlXSwgdmFsdWVFbnRpdGllc1t2YWx1ZV0sIHRyZWVOb2RlTGFiZWxQcm9wKSxcbiAgICAgICAgICB2YWx1ZTogdmFsdWVcbiAgICAgICAgfTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChzaG93Q2hlY2tlZFN0cmF0ZWd5ID09PSBTSE9XX0NISUxEKSB7XG4gICAgICAvLyBPbmx5IGdldCB0aGUgY2hpbGRyZW4gY2hlY2tlZCB2YWx1ZVxuICAgICAgdmFyIHRhcmdldFZhbHVlTGlzdCA9IFtdOyAvLyBGaW5kIHRoZSBsZWFmIGNoaWxkcmVuXG5cbiAgICAgIHZhciB0cmF2ZXJzZSA9IGZ1bmN0aW9uIHRyYXZlcnNlKF9yZWY1KSB7XG4gICAgICAgIHZhciB2YWx1ZSA9IF9yZWY1Lm5vZGUucHJvcHMudmFsdWUsXG4gICAgICAgICAgICBjaGlsZHJlbiA9IF9yZWY1LmNoaWxkcmVuO1xuXG4gICAgICAgIGlmICghY2hpbGRyZW4gfHwgY2hpbGRyZW4ubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgdGFyZ2V0VmFsdWVMaXN0LnB1c2goe1xuICAgICAgICAgICAgbGFiZWw6IGdldExhYmVsKHZhbHVlc1t2YWx1ZV0sIHZhbHVlRW50aXRpZXNbdmFsdWVdLCB0cmVlTm9kZUxhYmVsUHJvcCksXG4gICAgICAgICAgICB2YWx1ZTogdmFsdWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChlbnRpdHkpIHtcbiAgICAgICAgICB0cmF2ZXJzZShlbnRpdHkpO1xuICAgICAgICB9KTtcbiAgICAgIH07XG5cbiAgICAgIGhpZXJhcmNoeUxpc3QuZm9yRWFjaChmdW5jdGlvbiAoZW50aXR5KSB7XG4gICAgICAgIHRyYXZlcnNlKGVudGl0eSk7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0YXJnZXRWYWx1ZUxpc3Q7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHZhbHVlTGlzdC5tYXAoZnVuY3Rpb24gKHdyYXBwZWRWYWx1ZSkge1xuICAgIHJldHVybiB7XG4gICAgICBsYWJlbDogZ2V0TGFiZWwod3JhcHBlZFZhbHVlLCB2YWx1ZUVudGl0aWVzW3dyYXBwZWRWYWx1ZS52YWx1ZV0sIHRyZWVOb2RlTGFiZWxQcm9wKSxcbiAgICAgIHZhbHVlOiB3cmFwcGVkVmFsdWUudmFsdWVcbiAgICB9O1xuICB9KTtcbn1cbi8qKlxuICogVXNlIGByYy10cmVlYCBjb252ZXJ0RGF0YVRvVHJlZSB0byBjb252ZXJ0IHRyZWVEYXRhIHRvIFRyZWVOb2Rlcy5cbiAqIFRoaXMgd2lsbCBjaGFuZ2UgdGhlIGxhYmVsIHRvIHRpdGxlIHZhbHVlXG4gKi9cblxuZnVuY3Rpb24gcHJvY2Vzc1Byb3BzKHByb3BzKSB7XG4gIHZhciB0aXRsZSA9IHByb3BzLnRpdGxlLFxuICAgICAgbGFiZWwgPSBwcm9wcy5sYWJlbCxcbiAgICAgIGtleSA9IHByb3BzLmtleSxcbiAgICAgIHZhbHVlID0gcHJvcHMudmFsdWU7XG5cbiAgdmFyIGNsb25lUHJvcHMgPSBfb2JqZWN0U3ByZWFkKHt9LCBwcm9wcyk7IC8vIFdhcm5pbmcgdXNlciBub3QgdG8gdXNlIGRlcHJlY2F0ZWQgbGFiZWwgcHJvcC5cblxuXG4gIGlmIChsYWJlbCAmJiAhdGl0bGUpIHtcbiAgICBpZiAoIXdhcm5EZXByZWNhdGVkTGFiZWwpIHtcbiAgICAgIHdhcm5pbmcoZmFsc2UsIFwiJ2xhYmVsJyBpbiB0cmVlRGF0YSBpcyBkZXByZWNhdGVkLiBQbGVhc2UgdXNlICd0aXRsZScgaW5zdGVhZC5cIik7XG4gICAgICB3YXJuRGVwcmVjYXRlZExhYmVsID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBjbG9uZVByb3BzLnRpdGxlID0gbGFiZWw7XG4gIH1cblxuICBpZiAoIWtleSkge1xuICAgIGNsb25lUHJvcHMua2V5ID0gdmFsdWU7XG4gIH1cblxuICByZXR1cm4gY2xvbmVQcm9wcztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNvbnZlcnREYXRhVG9UcmVlKHRyZWVEYXRhKSB7XG4gIHJldHVybiByY0NvbnZlcnREYXRhVG9UcmVlKHRyZWVEYXRhLCB7XG4gICAgcHJvY2Vzc1Byb3BzOiBwcm9jZXNzUHJvcHNcbiAgfSk7XG59XG4vKipcbiAqIFVzZSBgcmMtdHJlZWAgY29udmVydFRyZWVUb0VudGl0aWVzIGZvciBlbnRpdGllcyBjYWxjdWxhdGlvbi5cbiAqIFdlIGhhdmUgYWRkaXRpb25hbCBlbnRpdGllcyBvZiBgdmFsdWVFbnRpdGllc2BcbiAqL1xuXG5mdW5jdGlvbiBpbml0V3JhcHBlcih3cmFwcGVyKSB7XG4gIHJldHVybiBfb2JqZWN0U3ByZWFkKHt9LCB3cmFwcGVyLCB7XG4gICAgdmFsdWVFbnRpdGllczoge31cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHByb2Nlc3NFbnRpdHkoZW50aXR5LCB3cmFwcGVyKSB7XG4gIHZhciB2YWx1ZSA9IGVudGl0eS5ub2RlLnByb3BzLnZhbHVlO1xuICBlbnRpdHkudmFsdWUgPSB2YWx1ZTsgLy8gVGhpcyBzaG91bGQgYmUgZW1wdHksIG9yIHdpbGwgZ2V0IGVycm9yIG1lc3NhZ2UuXG5cbiAgdmFyIGN1cnJlbnRFbnRpdHkgPSB3cmFwcGVyLnZhbHVlRW50aXRpZXNbdmFsdWVdO1xuXG4gIGlmIChjdXJyZW50RW50aXR5KSB7XG4gICAgd2FybmluZyhmYWxzZSwgXCJDb25mbGljdCEgdmFsdWUgb2Ygbm9kZSAnXCIuY29uY2F0KGVudGl0eS5rZXksIFwiJyAoXCIpLmNvbmNhdCh2YWx1ZSwgXCIpIGhhcyBhbHJlYWR5IHVzZWQgYnkgbm9kZSAnXCIpLmNvbmNhdChjdXJyZW50RW50aXR5LmtleSwgXCInLlwiKSk7XG4gIH1cblxuICB3cmFwcGVyLnZhbHVlRW50aXRpZXNbdmFsdWVdID0gZW50aXR5O1xufVxuXG5leHBvcnQgZnVuY3Rpb24gY29udmVydFRyZWVUb0VudGl0aWVzKHRyZWVOb2Rlcykge1xuICByZXR1cm4gcmNDb252ZXJ0VHJlZVRvRW50aXRpZXModHJlZU5vZGVzLCB7XG4gICAgaW5pdFdyYXBwZXI6IGluaXRXcmFwcGVyLFxuICAgIHByb2Nlc3NFbnRpdHk6IHByb2Nlc3NFbnRpdHlcbiAgfSk7XG59XG4vKipcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9hbnQtZGVzaWduL2FudC1kZXNpZ24vaXNzdWVzLzEzMzI4XG4gKiBXZSBuZWVkIGNhbGN1bGF0ZSB0aGUgaGFsZiBjaGVjayBrZXkgd2hlbiBzZWFyY2hWYWx1ZSBpcyBzZXQuXG4gKi9cbi8vIFRPRE86IFRoaXMgbG9naWMgbWF5IGJldHRlciBtb3ZlIHRvIHJjLXRyZWVcblxuZXhwb3J0IGZ1bmN0aW9uIGdldEhhbGZDaGVja2VkS2V5cyh2YWx1ZUxpc3QsIHZhbHVlRW50aXRpZXMpIHtcbiAgdmFyIHZhbHVlcyA9IHt9OyAvLyBGaWxsIGNoZWNrZWQga2V5c1xuXG4gIHZhbHVlTGlzdC5mb3JFYWNoKGZ1bmN0aW9uIChfcmVmNikge1xuICAgIHZhciB2YWx1ZSA9IF9yZWY2LnZhbHVlO1xuICAgIHZhbHVlc1t2YWx1ZV0gPSBmYWxzZTtcbiAgfSk7IC8vIEZpbGwgaGFsZiBjaGVja2VkIGtleXNcblxuICB2YWx1ZUxpc3QuZm9yRWFjaChmdW5jdGlvbiAoX3JlZjcpIHtcbiAgICB2YXIgdmFsdWUgPSBfcmVmNy52YWx1ZTtcbiAgICB2YXIgY3VycmVudCA9IHZhbHVlRW50aXRpZXNbdmFsdWVdO1xuXG4gICAgd2hpbGUgKGN1cnJlbnQgJiYgY3VycmVudC5wYXJlbnQpIHtcbiAgICAgIHZhciBwYXJlbnRWYWx1ZSA9IGN1cnJlbnQucGFyZW50LnZhbHVlO1xuICAgICAgaWYgKHBhcmVudFZhbHVlIGluIHZhbHVlcykgYnJlYWs7XG4gICAgICB2YWx1ZXNbcGFyZW50VmFsdWVdID0gdHJ1ZTtcbiAgICAgIGN1cnJlbnQgPSBjdXJyZW50LnBhcmVudDtcbiAgICB9XG4gIH0pOyAvLyBHZXQgaGFsZiBrZXlzXG5cbiAgcmV0dXJuIE9iamVjdC5rZXlzKHZhbHVlcykuZmlsdGVyKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgIHJldHVybiB2YWx1ZXNbdmFsdWVdO1xuICB9KS5tYXAoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgcmV0dXJuIHZhbHVlRW50aXRpZXNbdmFsdWVdLmtleTtcbiAgfSk7XG59XG5leHBvcnQgdmFyIGNvbmR1Y3RDaGVjayA9IHJjQ29uZHVjdENoZWNrOyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBOzs7OztBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQUE7QUFBQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQU9BO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTs7Ozs7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/rc-tree-select/es/util.js
