/*
QuillToolbar is deprecated. Consider switching to the official Quill
toolbar format, or providing your own toolbar instead. 
See https://quilljs.com/docs/modules/toolbar
*/


var React = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ReactDOMServer = __webpack_require__(/*! react-dom/server */ "./node_modules/react-dom/server.browser.js");

var createClass = __webpack_require__(/*! create-react-class */ "./node_modules/create-react-class/index.js");

var find = __webpack_require__(/*! lodash/find */ "./node_modules/lodash/find.js");

var isEqual = __webpack_require__(/*! lodash/isEqual */ "./node_modules/lodash/isEqual.js");

var T = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var DOM = __webpack_require__(/*! react-dom-factories */ "./node_modules/react-dom-factories/index.js");

var defaultColors = ['rgb(  0,   0,   0)', 'rgb(230,   0,   0)', 'rgb(255, 153,   0)', 'rgb(255, 255,   0)', 'rgb(  0, 138,   0)', 'rgb(  0, 102, 204)', 'rgb(153,  51, 255)', 'rgb(255, 255, 255)', 'rgb(250, 204, 204)', 'rgb(255, 235, 204)', 'rgb(255, 255, 204)', 'rgb(204, 232, 204)', 'rgb(204, 224, 245)', 'rgb(235, 214, 255)', 'rgb(187, 187, 187)', 'rgb(240, 102, 102)', 'rgb(255, 194, 102)', 'rgb(255, 255, 102)', 'rgb(102, 185, 102)', 'rgb(102, 163, 224)', 'rgb(194, 133, 255)', 'rgb(136, 136, 136)', 'rgb(161,   0,   0)', 'rgb(178, 107,   0)', 'rgb(178, 178,   0)', 'rgb(  0,  97,   0)', 'rgb(  0,  71, 178)', 'rgb(107,  36, 178)', 'rgb( 68,  68,  68)', 'rgb( 92,   0,   0)', 'rgb(102,  61,   0)', 'rgb(102, 102,   0)', 'rgb(  0,  55,   0)', 'rgb(  0,  41, 102)', 'rgb( 61,  20,  10)'].map(function (color) {
  return {
    value: color
  };
});
var defaultItems = [{
  label: 'Formats',
  type: 'group',
  items: [{
    label: 'Font',
    type: 'font',
    items: [{
      label: 'Sans Serif',
      value: 'sans-serif',
      selected: true
    }, {
      label: 'Serif',
      value: 'serif'
    }, {
      label: 'Monospace',
      value: 'monospace'
    }]
  }, {
    label: 'Size',
    type: 'size',
    items: [{
      label: 'Small',
      value: '10px'
    }, {
      label: 'Normal',
      value: '13px',
      selected: true
    }, {
      label: 'Large',
      value: '18px'
    }, {
      label: 'Huge',
      value: '32px'
    }]
  }, {
    label: 'Alignment',
    type: 'align',
    items: [{
      label: '',
      value: '',
      selected: true
    }, {
      label: '',
      value: 'center'
    }, {
      label: '',
      value: 'right'
    }, {
      label: '',
      value: 'justify'
    }]
  }]
}, {
  label: 'Text',
  type: 'group',
  items: [{
    type: 'bold',
    label: 'Bold'
  }, {
    type: 'italic',
    label: 'Italic'
  }, {
    type: 'strike',
    label: 'Strike'
  }, {
    type: 'underline',
    label: 'Underline'
  }, {
    type: 'color',
    label: 'Color',
    items: defaultColors
  }, {
    type: 'background',
    label: 'Background color',
    items: defaultColors
  }, {
    type: 'link',
    label: 'Link'
  }]
}, {
  label: 'Blocks',
  type: 'group',
  items: [{
    type: 'list',
    value: 'bullet'
  }, {
    type: 'list',
    value: 'ordered'
  }]
}, {
  label: 'Blocks',
  type: 'group',
  items: [{
    type: 'image',
    label: 'Image'
  }]
}];
var QuillToolbar = createClass({
  displayName: 'Quill Toolbar',
  propTypes: {
    id: T.string,
    className: T.string,
    style: T.object,
    items: T.array
  },
  getDefaultProps: function getDefaultProps() {
    return {
      items: defaultItems
    };
  },
  componentDidMount: function componentDidMount() {
    console.warn('QuillToolbar is deprecated. Consider switching to the official Quill ' + 'toolbar format, or providing your own toolbar instead. ' + 'See: https://github.com/zenoamaro/react-quill#upgrading-to-react-quill-v1-0-0');
  },
  shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(nextProps, this.props);
  },
  renderGroup: function renderGroup(item, key) {
    return DOM.span({
      key: item.label || key,
      className: 'ql-formats'
    }, item.items.map(this.renderItem));
  },
  renderChoiceItem: function renderChoiceItem(item, key) {
    return DOM.option({
      key: item.label || item.value || key,
      value: item.value
    }, item.label);
  },
  renderChoices: function renderChoices(item, key) {
    var choiceItems = item.items.map(this.renderChoiceItem);
    var selectedItem = find(item.items, function (item) {
      return item.selected;
    });
    var attrs = {
      key: item.label || key,
      title: item.label,
      className: 'ql-' + item.type,
      value: selectedItem.value
    };
    return DOM.select(attrs, choiceItems);
  },
  renderButton: function renderButton(item, key) {
    return DOM.button({
      type: 'button',
      key: item.label || item.value || key,
      value: item.value,
      className: 'ql-' + item.type,
      title: item.label
    }, item.children);
  },
  renderAction: function renderAction(item, key) {
    return DOM.button({
      key: item.label || item.value || key,
      className: 'ql-' + item.type,
      title: item.label
    }, item.children);
  },

  /* jshint maxcomplexity: false */
  renderItem: function renderItem(item, key) {
    switch (item.type) {
      case 'group':
        return this.renderGroup(item, key);

      case 'font':
      case 'header':
      case 'align':
      case 'size':
      case 'color':
      case 'background':
        return this.renderChoices(item, key);

      case 'bold':
      case 'italic':
      case 'underline':
      case 'strike':
      case 'link':
      case 'list':
      case 'bullet':
      case 'ordered':
      case 'indent':
      case 'image':
      case 'video':
        return this.renderButton(item, key);

      default:
        return this.renderAction(item, key);
    }
  },
  getClassName: function getClassName() {
    return 'quill-toolbar ' + (this.props.className || '');
  },
  render: function render() {
    var children = this.props.items.map(this.renderItem);
    var html = children.map(ReactDOMServer.renderToStaticMarkup).join('');
    return DOM.div({
      id: this.props.id,
      className: this.getClassName(),
      style: this.props.style,
      dangerouslySetInnerHTML: {
        __html: html
      }
    });
  }
});
module.exports = QuillToolbar;
QuillToolbar.defaultItems = defaultItems;
QuillToolbar.defaultColors = defaultColors;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmVhY3QtcXVpbGwvbGliL3Rvb2xiYXIuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9yZWFjdC1xdWlsbC9saWIvdG9vbGJhci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuUXVpbGxUb29sYmFyIGlzIGRlcHJlY2F0ZWQuIENvbnNpZGVyIHN3aXRjaGluZyB0byB0aGUgb2ZmaWNpYWwgUXVpbGxcbnRvb2xiYXIgZm9ybWF0LCBvciBwcm92aWRpbmcgeW91ciBvd24gdG9vbGJhciBpbnN0ZWFkLiBcblNlZSBodHRwczovL3F1aWxsanMuY29tL2RvY3MvbW9kdWxlcy90b29sYmFyXG4qL1xuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG52YXIgUmVhY3RET01TZXJ2ZXIgPSByZXF1aXJlKCdyZWFjdC1kb20vc2VydmVyJyk7XG52YXIgY3JlYXRlQ2xhc3MgPSByZXF1aXJlKCdjcmVhdGUtcmVhY3QtY2xhc3MnKTtcbnZhciBmaW5kID0gcmVxdWlyZSgnbG9kYXNoL2ZpbmQnKTtcbnZhciBpc0VxdWFsID0gcmVxdWlyZSgnbG9kYXNoL2lzRXF1YWwnKTtcbnZhciBUID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xudmFyIERPTSA9IHJlcXVpcmUoJ3JlYWN0LWRvbS1mYWN0b3JpZXMnKTtcblxudmFyIGRlZmF1bHRDb2xvcnMgPSBbXG5cdCdyZ2IoICAwLCAgIDAsICAgMCknLCAncmdiKDIzMCwgICAwLCAgIDApJywgJ3JnYigyNTUsIDE1MywgICAwKScsXG5cdCdyZ2IoMjU1LCAyNTUsICAgMCknLCAncmdiKCAgMCwgMTM4LCAgIDApJywgJ3JnYiggIDAsIDEwMiwgMjA0KScsXG5cdCdyZ2IoMTUzLCAgNTEsIDI1NSknLCAncmdiKDI1NSwgMjU1LCAyNTUpJywgJ3JnYigyNTAsIDIwNCwgMjA0KScsXG5cdCdyZ2IoMjU1LCAyMzUsIDIwNCknLCAncmdiKDI1NSwgMjU1LCAyMDQpJywgJ3JnYigyMDQsIDIzMiwgMjA0KScsXG5cdCdyZ2IoMjA0LCAyMjQsIDI0NSknLCAncmdiKDIzNSwgMjE0LCAyNTUpJywgJ3JnYigxODcsIDE4NywgMTg3KScsXG5cdCdyZ2IoMjQwLCAxMDIsIDEwMiknLCAncmdiKDI1NSwgMTk0LCAxMDIpJywgJ3JnYigyNTUsIDI1NSwgMTAyKScsXG5cdCdyZ2IoMTAyLCAxODUsIDEwMiknLCAncmdiKDEwMiwgMTYzLCAyMjQpJywgJ3JnYigxOTQsIDEzMywgMjU1KScsXG5cdCdyZ2IoMTM2LCAxMzYsIDEzNiknLCAncmdiKDE2MSwgICAwLCAgIDApJywgJ3JnYigxNzgsIDEwNywgICAwKScsXG5cdCdyZ2IoMTc4LCAxNzgsICAgMCknLCAncmdiKCAgMCwgIDk3LCAgIDApJywgJ3JnYiggIDAsICA3MSwgMTc4KScsXG5cdCdyZ2IoMTA3LCAgMzYsIDE3OCknLCAncmdiKCA2OCwgIDY4LCAgNjgpJywgJ3JnYiggOTIsICAgMCwgICAwKScsXG5cdCdyZ2IoMTAyLCAgNjEsICAgMCknLCAncmdiKDEwMiwgMTAyLCAgIDApJywgJ3JnYiggIDAsICA1NSwgICAwKScsXG5cdCdyZ2IoICAwLCAgNDEsIDEwMiknLCAncmdiKCA2MSwgIDIwLCAgMTApJyxcbl0ubWFwKGZ1bmN0aW9uKGNvbG9yKXsgcmV0dXJuIHsgdmFsdWU6IGNvbG9yIH0gfSk7XG5cbnZhciBkZWZhdWx0SXRlbXMgPSBbXG5cblx0eyBsYWJlbDonRm9ybWF0cycsIHR5cGU6J2dyb3VwJywgaXRlbXM6IFtcblx0XHR7IGxhYmVsOidGb250JywgdHlwZTonZm9udCcsIGl0ZW1zOiBbXG5cdFx0XHR7IGxhYmVsOidTYW5zIFNlcmlmJywgIHZhbHVlOidzYW5zLXNlcmlmJywgc2VsZWN0ZWQ6dHJ1ZSB9LFxuXHRcdFx0eyBsYWJlbDonU2VyaWYnLCAgICAgICB2YWx1ZTonc2VyaWYnIH0sXG5cdFx0XHR7IGxhYmVsOidNb25vc3BhY2UnLCAgIHZhbHVlOidtb25vc3BhY2UnIH1cblx0XHRdfSxcblx0XHR7IGxhYmVsOidTaXplJywgdHlwZTonc2l6ZScsIGl0ZW1zOiBbXG5cdFx0XHR7IGxhYmVsOidTbWFsbCcsICB2YWx1ZTonMTBweCcgfSxcblx0XHRcdHsgbGFiZWw6J05vcm1hbCcsIHZhbHVlOicxM3B4Jywgc2VsZWN0ZWQ6dHJ1ZSB9LFxuXHRcdFx0eyBsYWJlbDonTGFyZ2UnLCAgdmFsdWU6JzE4cHgnIH0sXG5cdFx0XHR7IGxhYmVsOidIdWdlJywgICB2YWx1ZTonMzJweCcgfVxuXHRcdF19LFxuXHRcdHsgbGFiZWw6J0FsaWdubWVudCcsIHR5cGU6J2FsaWduJywgaXRlbXM6IFtcblx0XHRcdHsgbGFiZWw6JycsIHZhbHVlOicnLCBzZWxlY3RlZDp0cnVlIH0sXG5cdFx0XHR7IGxhYmVsOicnLCB2YWx1ZTonY2VudGVyJyB9LFxuXHRcdFx0eyBsYWJlbDonJywgdmFsdWU6J3JpZ2h0JyB9LFxuXHRcdFx0eyBsYWJlbDonJywgdmFsdWU6J2p1c3RpZnknIH1cblx0XHRdfVxuXHRdfSxcblxuXHR7IGxhYmVsOidUZXh0JywgdHlwZTonZ3JvdXAnLCBpdGVtczogW1xuXHRcdHsgdHlwZTonYm9sZCcsIGxhYmVsOidCb2xkJyB9LFxuXHRcdHsgdHlwZTonaXRhbGljJywgbGFiZWw6J0l0YWxpYycgfSxcblx0XHR7IHR5cGU6J3N0cmlrZScsIGxhYmVsOidTdHJpa2UnIH0sXG5cdFx0eyB0eXBlOid1bmRlcmxpbmUnLCBsYWJlbDonVW5kZXJsaW5lJyB9LFxuXHRcdHsgdHlwZTonY29sb3InLCBsYWJlbDonQ29sb3InLCBpdGVtczpkZWZhdWx0Q29sb3JzIH0sXG5cdFx0eyB0eXBlOidiYWNrZ3JvdW5kJywgbGFiZWw6J0JhY2tncm91bmQgY29sb3InLCBpdGVtczpkZWZhdWx0Q29sb3JzIH0sXG5cdFx0eyB0eXBlOidsaW5rJywgbGFiZWw6J0xpbmsnIH1cblx0XX0sXG5cblx0eyBsYWJlbDonQmxvY2tzJywgdHlwZTonZ3JvdXAnLCBpdGVtczogW1xuXHRcdHsgdHlwZTonbGlzdCcsIHZhbHVlOididWxsZXQnIH0sXG5cdFx0eyB0eXBlOidsaXN0JywgdmFsdWU6J29yZGVyZWQnIH1cblx0XX0sXG5cblx0eyBsYWJlbDonQmxvY2tzJywgdHlwZTonZ3JvdXAnLCBpdGVtczogW1xuXHRcdHsgdHlwZTonaW1hZ2UnLCBsYWJlbDonSW1hZ2UnIH1cblx0XX1cblxuXTtcblxudmFyIFF1aWxsVG9vbGJhciA9IGNyZWF0ZUNsYXNzKHtcblxuXHRkaXNwbGF5TmFtZTogJ1F1aWxsIFRvb2xiYXInLFxuXG5cdHByb3BUeXBlczoge1xuXHRcdGlkOiAgICAgICAgVC5zdHJpbmcsXG5cdFx0Y2xhc3NOYW1lOiBULnN0cmluZyxcblx0XHRzdHlsZTogICAgIFQub2JqZWN0LFxuXHRcdGl0ZW1zOiAgICAgVC5hcnJheVxuXHR9LFxuXG5cdGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdGl0ZW1zOiBkZWZhdWx0SXRlbXNcblx0XHR9O1xuXHR9LFxuXG5cdGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcblx0XHRjb25zb2xlLndhcm4oXG5cdFx0XHQnUXVpbGxUb29sYmFyIGlzIGRlcHJlY2F0ZWQuIENvbnNpZGVyIHN3aXRjaGluZyB0byB0aGUgb2ZmaWNpYWwgUXVpbGwgJyArXG5cdFx0XHQndG9vbGJhciBmb3JtYXQsIG9yIHByb3ZpZGluZyB5b3VyIG93biB0b29sYmFyIGluc3RlYWQuICcgK1xuXHRcdFx0J1NlZTogaHR0cHM6Ly9naXRodWIuY29tL3plbm9hbWFyby9yZWFjdC1xdWlsbCN1cGdyYWRpbmctdG8tcmVhY3QtcXVpbGwtdjEtMC0wJ1xuXHRcdCk7XG5cdH0sXG5cblx0c2hvdWxkQ29tcG9uZW50VXBkYXRlOiBmdW5jdGlvbihuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuXHRcdHJldHVybiAhaXNFcXVhbChuZXh0UHJvcHMsIHRoaXMucHJvcHMpO1xuXHR9LFxuXG5cdHJlbmRlckdyb3VwOiBmdW5jdGlvbihpdGVtLCBrZXkpIHtcblx0XHRyZXR1cm4gRE9NLnNwYW4oe1xuXHRcdFx0a2V5OiBpdGVtLmxhYmVsIHx8IGtleSxcblx0XHRcdGNsYXNzTmFtZToncWwtZm9ybWF0cycgfSxcblx0XHRcdGl0ZW0uaXRlbXMubWFwKHRoaXMucmVuZGVySXRlbSlcblx0XHQpO1xuXHR9LFxuXG5cdHJlbmRlckNob2ljZUl0ZW06IGZ1bmN0aW9uKGl0ZW0sIGtleSkge1xuXHRcdHJldHVybiBET00ub3B0aW9uKHtcblx0XHRcdGtleTogaXRlbS5sYWJlbCB8fCBpdGVtLnZhbHVlIHx8IGtleSxcblx0XHRcdHZhbHVlOiBpdGVtLnZhbHVlIH0sXG5cdFx0XHRpdGVtLmxhYmVsXG5cdFx0KTtcblx0fSxcblxuXHRyZW5kZXJDaG9pY2VzOiBmdW5jdGlvbihpdGVtLCBrZXkpIHtcblx0XHR2YXIgY2hvaWNlSXRlbXMgPSBpdGVtLml0ZW1zLm1hcCh0aGlzLnJlbmRlckNob2ljZUl0ZW0pO1xuXHRcdHZhciBzZWxlY3RlZEl0ZW0gPSBmaW5kKGl0ZW0uaXRlbXMsIGZ1bmN0aW9uKGl0ZW0peyByZXR1cm4gaXRlbS5zZWxlY3RlZCB9KTtcblx0XHR2YXIgYXR0cnMgPSB7XG5cdFx0XHRrZXk6IGl0ZW0ubGFiZWwgfHwga2V5LFxuXHRcdFx0dGl0bGU6IGl0ZW0ubGFiZWwsXG5cdFx0XHRjbGFzc05hbWU6ICdxbC0nK2l0ZW0udHlwZSxcblx0XHRcdHZhbHVlOiBzZWxlY3RlZEl0ZW0udmFsdWUsXG5cdFx0fTtcblx0XHRyZXR1cm4gRE9NLnNlbGVjdChhdHRycywgY2hvaWNlSXRlbXMpO1xuXHR9LFxuXG5cdHJlbmRlckJ1dHRvbjogZnVuY3Rpb24oaXRlbSwga2V5KSB7XG5cdFx0cmV0dXJuIERPTS5idXR0b24oe1xuXHRcdFx0dHlwZTogJ2J1dHRvbicsXG5cdFx0XHRrZXk6IGl0ZW0ubGFiZWwgfHwgaXRlbS52YWx1ZSB8fCBrZXksXG5cdFx0XHR2YWx1ZTogaXRlbS52YWx1ZSxcblx0XHRcdGNsYXNzTmFtZTogJ3FsLScraXRlbS50eXBlLFxuXHRcdFx0dGl0bGU6IGl0ZW0ubGFiZWwgfSxcblx0XHRcdGl0ZW0uY2hpbGRyZW5cblx0XHQpO1xuXHR9LFxuXG5cdHJlbmRlckFjdGlvbjogZnVuY3Rpb24oaXRlbSwga2V5KSB7XG5cdFx0cmV0dXJuIERPTS5idXR0b24oe1xuXHRcdFx0a2V5OiBpdGVtLmxhYmVsIHx8IGl0ZW0udmFsdWUgfHwga2V5LFxuXHRcdFx0Y2xhc3NOYW1lOiAncWwtJytpdGVtLnR5cGUsXG5cdFx0XHR0aXRsZTogaXRlbS5sYWJlbCB9LFxuXHRcdFx0aXRlbS5jaGlsZHJlblxuXHRcdCk7XG5cdH0sXG5cblx0LyoganNoaW50IG1heGNvbXBsZXhpdHk6IGZhbHNlICovXG5cdHJlbmRlckl0ZW06IGZ1bmN0aW9uKGl0ZW0sIGtleSkge1xuXHRcdHN3aXRjaCAoaXRlbS50eXBlKSB7XG5cdFx0XHRjYXNlICdncm91cCc6XG5cdFx0XHRcdHJldHVybiB0aGlzLnJlbmRlckdyb3VwKGl0ZW0sIGtleSk7XG5cdFx0XHRjYXNlICdmb250Jzpcblx0XHRcdGNhc2UgJ2hlYWRlcic6XG5cdFx0XHRjYXNlICdhbGlnbic6XG5cdFx0XHRjYXNlICdzaXplJzpcblx0XHRcdGNhc2UgJ2NvbG9yJzpcblx0XHRcdGNhc2UgJ2JhY2tncm91bmQnOlxuXHRcdFx0XHRyZXR1cm4gdGhpcy5yZW5kZXJDaG9pY2VzKGl0ZW0sIGtleSk7XG5cdFx0XHRjYXNlICdib2xkJzpcblx0XHRcdGNhc2UgJ2l0YWxpYyc6XG5cdFx0XHRjYXNlICd1bmRlcmxpbmUnOlxuXHRcdFx0Y2FzZSAnc3RyaWtlJzpcblx0XHRcdGNhc2UgJ2xpbmsnOlxuXHRcdFx0Y2FzZSAnbGlzdCc6XG5cdFx0XHRjYXNlICdidWxsZXQnOlxuXHRcdFx0Y2FzZSAnb3JkZXJlZCc6XG5cdFx0XHRjYXNlICdpbmRlbnQnOlxuXHRcdFx0Y2FzZSAnaW1hZ2UnOlxuXHRcdFx0Y2FzZSAndmlkZW8nOlxuXHRcdFx0XHRyZXR1cm4gdGhpcy5yZW5kZXJCdXR0b24oaXRlbSwga2V5KTtcblx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdHJldHVybiB0aGlzLnJlbmRlckFjdGlvbihpdGVtLCBrZXkpO1xuXHRcdH1cblx0fSxcblxuXHRnZXRDbGFzc05hbWU6IGZ1bmN0aW9uKCkge1xuXHRcdHJldHVybiAncXVpbGwtdG9vbGJhciAnICsgKHRoaXMucHJvcHMuY2xhc3NOYW1lfHwnJyk7XG5cdH0sXG5cblx0cmVuZGVyOiBmdW5jdGlvbigpIHtcblx0XHR2YXIgY2hpbGRyZW4gPSB0aGlzLnByb3BzLml0ZW1zLm1hcCh0aGlzLnJlbmRlckl0ZW0pO1xuXHRcdHZhciBodG1sID0gY2hpbGRyZW4ubWFwKFJlYWN0RE9NU2VydmVyLnJlbmRlclRvU3RhdGljTWFya3VwKS5qb2luKCcnKTtcblx0XHRyZXR1cm4gRE9NLmRpdih7XG5cdFx0XHRpZDogdGhpcy5wcm9wcy5pZCxcblx0XHRcdGNsYXNzTmFtZTogdGhpcy5nZXRDbGFzc05hbWUoKSxcblx0XHRcdHN0eWxlOiB0aGlzLnByb3BzLnN0eWxlLFxuXHRcdFx0ZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw6IHsgX19odG1sOmh0bWwgfVxuXHRcdH0pO1xuXHR9LFxuXG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBRdWlsbFRvb2xiYXI7XG5RdWlsbFRvb2xiYXIuZGVmYXVsdEl0ZW1zID0gZGVmYXVsdEl0ZW1zO1xuUXVpbGxUb29sYmFyLmRlZmF1bHRDb2xvcnMgPSBkZWZhdWx0Q29sb3JzO1xuIl0sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBYUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBSEE7QUFLQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUpBO0FBTUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFKQTtBQVpBO0FBb0JBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFQQTtBQVVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUZBO0FBS0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFNQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBQ0E7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUF2QkE7QUF5QkE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUpBO0FBTUE7QUF2SEE7QUEySEE7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/react-quill/lib/toolbar.js
