var Path = __webpack_require__(/*! ../Path */ "./node_modules/zrender/lib/graphic/Path.js");

var polyHelper = __webpack_require__(/*! ../helper/poly */ "./node_modules/zrender/lib/graphic/helper/poly.js");
/**
 * 多边形
 * @module zrender/shape/Polygon
 */


var _default = Path.extend({
  type: 'polygon',
  shape: {
    points: null,
    smooth: false,
    smoothConstraint: null
  },
  buildPath: function buildPath(ctx, shape) {
    polyHelper.buildPath(ctx, shape, true);
  }
});

module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9Qb2x5Z29uLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvenJlbmRlci9saWIvZ3JhcGhpYy9zaGFwZS9Qb2x5Z29uLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBQYXRoID0gcmVxdWlyZShcIi4uL1BhdGhcIik7XG5cbnZhciBwb2x5SGVscGVyID0gcmVxdWlyZShcIi4uL2hlbHBlci9wb2x5XCIpO1xuXG4vKipcbiAqIOWkmui+ueW9olxuICogQG1vZHVsZSB6cmVuZGVyL3NoYXBlL1BvbHlnb25cbiAqL1xudmFyIF9kZWZhdWx0ID0gUGF0aC5leHRlbmQoe1xuICB0eXBlOiAncG9seWdvbicsXG4gIHNoYXBlOiB7XG4gICAgcG9pbnRzOiBudWxsLFxuICAgIHNtb290aDogZmFsc2UsXG4gICAgc21vb3RoQ29uc3RyYWludDogbnVsbFxuICB9LFxuICBidWlsZFBhdGg6IGZ1bmN0aW9uIChjdHgsIHNoYXBlKSB7XG4gICAgcG9seUhlbHBlci5idWlsZFBhdGgoY3R4LCBzaGFwZSwgdHJ1ZSk7XG4gIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9kZWZhdWx0OyJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBRUE7Ozs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBVEE7QUFDQTtBQVdBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/zrender/lib/graphic/shape/Polygon.js
