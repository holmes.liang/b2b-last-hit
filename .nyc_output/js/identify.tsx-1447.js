__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectSpread2.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits */ "./node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_3rd__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @common/3rd */ "./src/common/3rd.tsx");
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! antd */ "./node_modules/antd/es/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @styles */ "./src/styles/index.tsx");
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @common */ "./src/common/index.tsx");







var _jsxFileName = "/Users/bytesforce/Desktop/b2b/src/app/desk/ekyc/identify.tsx";

function _templateObject() {
  var data = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  \n   @keyframes shake-msg {\n    25% {\n      transform: translateX(6px);\n    }\n    50% {\n      transform: translateX(-4px);\n    }\n    75% {\n      transform: translateX(2px);\n    }\n    100% {\n      transform: translateX(0);\n    }\n  }\n  video {\n    width: 238px;\n    height: 238px;\n    border-radius: 50%;\n    object-fit: cover;\n    margin-left: 80px;\n    transform: rotateY(180deg);\n  }\n  .video-before {\n    width: 400px;\n    border-radius: 4px;\n    height: 348px;\n    border: 1px solid #d9d9d9;\n    margin: 20px 0;\n    p {\n      text-align: center;\n      margin: 20px 0;\n      line-height: 20px;\n      color: #000;\n    }\n    .video-btn {\n      width: 248px;\n      height: 248px;\n      border-radius: 50%;\n      border: 1px dashed #d9d9d9;\n      display: flex;\n      justify-content: center;\n      align-items: center;\n      margin: auto;\n      margin-top: 14px;\n      .icon-video {\n        padding-right: 10px;\n      }\n    }\n  }\n  .video-area {\n    border: 1px solid #d9d9d9;\n    padding: 5px;\n    width: 400px;\n    height: 348px;\n    margin: 20px 0;\n    position: relative;\n    border-radius: 4px;\n    .video-progress {\n      position: absolute;\n      top: 61px;\n      left: 80px;\n    }\n    .video-after {\n      position: absolute;\n        width: 400px;\n        height: 348px;\n        top: 0;\n        left: 0;\n      p {\n        text-align: center;\n        margin: 20px 0;\n        line-height: 20px;\n        color: #000;\n      }\n      .video-header {\n        width: 238px;\n        height: 238px;\n        margin: auto;\n        border-radius: 50%;\n        border: 1px dashed #2f2e2e;\n      }\n    }\n    \n  }\n  .video-area {\n     p {\n        text-align: center;\n        margin: 20px 0;\n        line-height: 20px;\n        color: #000;\n     }\n     .video-result {\n        display: flex;\n        justify-content: center;\n        video {\n          margin-left: 10px;\n          margin-top: 1px;\n        }\n     }\n     .ant-progress-inner {\n        font-size: 45px !important;\n     }\n  }\n  .fail {\n    display: flex;\n    color: #ff0000;\n    justify-content: center;\n    align-items: center;\n    .icon-ku {\n      font-size: 100px;\n    }\n    & > div {\n      text-align: center;\n    }\n    .ant-btn {\n      display: block;\n      margin: 0 0 0 5px;\n    }\n  }\n  p.err-msg {\n     &.shake {\n      animation: shake-msg .4s ease-in-out forwards;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}







var _Theme$getTheme = _styles__WEBPACK_IMPORTED_MODULE_10__["default"].getTheme(),
    COLOR_A = _Theme$getTheme.COLOR_A;

var Identify =
/*#__PURE__*/
function (_React$Component) {
  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Identify, _React$Component);

  function Identify() {
    var _getPrototypeOf2;

    var _this2;

    Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, Identify);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, (_getPrototypeOf2 = Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Identify)).call.apply(_getPrototypeOf2, [this].concat(args)));
    _this2.timeout = {};
    _this2.recorder = {};
    _this2.smoothlyAddInterval = {};
    _this2.mediaStreamTrack = {};
    _this2.state = {
      chunks: [],
      isShowVideo: false,
      isShowSuccess: false,
      isShowError: false,
      isShowProgress: false,
      progressNum: 1,
      errMsg: "",
      videoBase64: ""
    };
    return _this2;
  }

  Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(Identify, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(this.smoothlyAddInterval);
      clearTimeout(this.timeout);
    }
  }, {
    key: "successProgress",
    value: function successProgress() {
      var _this3 = this;

      this.smoothlyAddInterval = setInterval(function () {
        if (_this3.state.progressNum === 100) {
          clearInterval(_this3.smoothlyAddInterval);
          return;
        }

        if (_this3.state.progressNum <= 95) {
          _this3.setState({
            progressNum: _this3.state.progressNum + 1
          });
        }
      }, 250);
    }
  }, {
    key: "btnVideo",
    value: function btnVideo() {
      var _this4 = this;

      var _this = this; // 只获取视频


      var constraints = {
        audio: false,
        video: true
      };
      var video = document.getElementById("stream");
      navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
        if (video) {
          video.srcObject = stream;
        }

        _this.mediaStreamTrack = stream;
        _this.recorder = new window.MediaRecorder(stream);
        _this.recorder.ondataavailable = _this4.getRecordingData.bind(_this4);
        _this.recorder.onstop = _this4.saveRecordingData.bind(_this4);
      }).then(function () {
        _this.recorder.start();

        setTimeout(function () {
          _this4.setState({
            errMsg: "Please fit your head in the frame"
          }, function () {
            _this.setActions();
          });
        }, 1000);
      });
      _this.timeout = setTimeout(function () {
        _this.recorder.stop();
      }, 5000);
    }
  }, {
    key: "getRecordingData",
    value: function getRecordingData(e) {
      var chunks = [];
      chunks.push(e.data);
      this.setState({
        chunks: chunks
      });
    }
  }, {
    key: "setActions",
    value: function setActions() {
      var _this5 = this;

      setTimeout(function () {
        _this5.setState({
          errMsg: "Please blink eyes"
        }, function () {
          setTimeout(function () {
            _this5.setState({
              errMsg: "Please open mouth"
            }, function () {
              setTimeout(function () {
                _this5.setState({
                  errMsg: "Please waiting ..."
                }, function () {
                  _this5.setState({
                    isShowProgress: true
                  }, function () {
                    _this5.successProgress();
                  });
                });
              }, 2000);
            });
          }, 2000);
        });
      }, 1000);
    }
  }, {
    key: "saveRecordingData",
    value: function saveRecordingData() {
      var _this6 = this;

      var chunks = this.state.chunks;
      var currVideo;
      var blob = new Blob(chunks, {
        "type": "video/mp4"
      });
      currVideo = URL.createObjectURL(blob); // this.setState({
      //   isShowProgress: true,
      // }, () => {
      //   this.successProgress();
      // });

      this.blobToBase64(blob, function (res) {
        // 转化后的base64
        _this6.postLiveness(res);

        _this6.setState({
          chunks: []
        });
      });
    }
  }, {
    key: "blobToBase64",
    value: function blobToBase64(blob, callback) {
      var reader = new FileReader();

      reader.onload = function (e) {
        callback(e.target.result);
      };

      reader.readAsDataURL(blob);
    }
  }, {
    key: "postLiveness",
    value: function postLiveness(videoBase64) {
      var _this7 = this;

      var _this$props = this.props,
          uploadUrlBase64 = _this$props.uploadUrlBase64,
          model = _this$props.model;
      var authKey = _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_11__["Consts"].AUTH_KEY);
      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].post("/kyc/liveness", Object(_Users_bytesforce_Desktop_b2b_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_1__["default"])({}, {
        imageBase64: uploadUrlBase64,
        videoBase64: videoBase64,
        actionsSequence: "1,2"
      }, {}, model), {
        headers: {
          "recaptcha-token": authKey
        }
      }).then(function (res) {
        var respData = (res.body || {}).respData || {};

        var success = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(respData, "success");

        var stopCheck = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(respData, "stopCheck");

        var errMsg = lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(respData, "errMsg");

        if (success) {
          _this7.setState({
            isShowSuccess: true,
            isShowError: false,
            progressNum: 100,
            videoBase64: videoBase64
          }, function () {
            _this7.stopTakePhoto();

            _this7.props.successCallback();
          });
        } else {
          if (stopCheck) {
            _this7.setState({
              isShowSuccess: false,
              isShowError: true
            }, function () {
              _this7.stopTakePhoto();
            });
          } else {
            // this.setState({
            //   errMsg,
            // });
            if (_this7.recorder) {
              _this7.recorder.start();

              _this7.setActions();

              _this7.timeout = setTimeout(function () {
                _this7.recorder.stop();
              }, 5000);
            }
          }
        }
      });
    }
  }, {
    key: "stopTakePhoto",
    value: function stopTakePhoto() {
      if (this.mediaStreamTrack) {
        this.mediaStreamTrack.getTracks && this.mediaStreamTrack.getTracks()[0].stop();
      }
    }
  }, {
    key: "tryAgain",
    value: function tryAgain() {
      var _this8 = this;

      var authKey = _common__WEBPACK_IMPORTED_MODULE_11__["Storage"].Auth.session().get(_common__WEBPACK_IMPORTED_MODULE_11__["Consts"].AUTH_KEY);
      var model = this.props.model;
      _common__WEBPACK_IMPORTED_MODULE_11__["Ajax"].patch("/kyc/liveness/reactivation", {
        bizId: lodash__WEBPACK_IMPORTED_MODULE_7___default.a.get(model, "bizId"),
        bizType: "NB"
      }, {
        headers: {
          "recaptcha-token": authKey
        },
        loading: true
      }).then(function (res) {
        _this8.setState({
          isShowError: false,
          isShowSuccess: false,
          isShowProgress: false,
          progressNum: 0,
          errMsg: ""
        }, function () {
          _this8.btnVideo();
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this9 = this;

      var _this$state = this.state,
          isShowVideo = _this$state.isShowVideo,
          isShowSuccess = _this$state.isShowSuccess,
          errMsg = _this$state.errMsg,
          isShowError = _this$state.isShowError,
          isShowProgress = _this$state.isShowProgress;
      return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(VideoStyle, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 242
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 243
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 244
        },
        __self: this
      }, "Step 2"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 18,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 245
        },
        __self: this
      }, "Face recognition. Please follow the guidance")), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Row"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 249
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 6,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 250
        },
        __self: this
      }), !isShowVideo && !isShowSuccess && !isShowError && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 18,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 251
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-before",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 252
        },
        __self: this
      }, errMsg && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        className: "err-msg shake",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 253
        },
        __self: this
      }, errMsg), !errMsg && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 254
        },
        __self: this
      }, "Please fit your head in the frame"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-btn",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 255
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        onClick: function onClick() {
          if (_this9.props.uploadUrlBase64) {
            _this9.setState({
              isShowVideo: true
            }, function () {
              _this9.btnVideo();
            });
          } else {
            _this9.setState({
              errMsg: "Please take photo of your ID or passport"
            });
          }
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 256
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
        className: "iconfont icon-video",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }), " Launch Camera")))), isShowVideo && !isShowSuccess && !isShowError && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 18,
        className: "video-area",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 274
        },
        __self: this
      }, errMsg && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        className: "err-msg shake",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 275
        },
        __self: this
      }, errMsg), !errMsg && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 276
        },
        __self: this
      }, "Please fit your head in the frame"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("video", {
        autoPlay: true,
        id: "stream",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 277
        },
        __self: this
      }), isShowProgress && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-progress",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 278
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        status: "active",
        type: "circle",
        width: 248,
        strokeColor: COLOR_A,
        strokeWidth: 1,
        format: function format() {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 284
            },
            __self: this
          });
        },
        percent: this.state.progressNum,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 279
        },
        __self: this
      }))), isShowSuccess && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 18,
        className: "video-area",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 289
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 290
        },
        __self: this
      }, "Face validation passed. Thank you!"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-result",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 291
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("video", {
        src: this.state.videoBase64,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 292
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-progress",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 293
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Progress"], {
        status: "active",
        type: "circle",
        width: 248,
        strokeColor: COLOR_A,
        strokeWidth: 1,
        format: function format() {
          return _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
            style: {
              color: COLOR_A,
              fontSize: "60px"
            },
            className: "iconfont  icon-xiao",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 299
            },
            __self: this
          });
        },
        percent: this.state.progressNum,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 294
        },
        __self: this
      })))), isShowError && _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Col"], {
        span: 18,
        className: "video-area",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 309
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 310
        },
        __self: this
      }, "Face recognition failed. Please try again or skip this step"), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "video-result",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 311
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        className: "fail",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 312
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 313
        },
        __self: this
      }, _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement("i", {
        className: "iconfont icon-ku",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 314
        },
        __self: this
      }), _common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].createElement(antd__WEBPACK_IMPORTED_MODULE_9__["Button"], {
        onClick: function onClick() {
          _this9.tryAgain();
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 315
        },
        __self: this
      }, "Try Again")))))));
    }
  }]);

  return Identify;
}(_common_3rd__WEBPACK_IMPORTED_MODULE_8__["React"].Component);

var VideoStyle = _common_3rd__WEBPACK_IMPORTED_MODULE_8__["Styled"].div(_templateObject());
/* harmony default export */ __webpack_exports__["default"] = (Identify);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBwL2Rlc2svZWt5Yy9pZGVudGlmeS50c3guanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL3NyYy9hcHAvZGVzay9la3ljL2lkZW50aWZ5LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXyBmcm9tIFwibG9kYXNoXCI7XG5pbXBvcnQgeyBSZWFjdCwgU3R5bGVkIH0gZnJvbSBcIkBjb21tb24vM3JkXCI7XG5pbXBvcnQgeyBCdXR0b24sIENvbCwgUHJvZ3Jlc3MsIFJvdyB9IGZyb20gXCJhbnRkXCI7XG5pbXBvcnQgVGhlbWUgZnJvbSBcIkBzdHlsZXNcIjtcbmltcG9ydCB7IEFqYXgsIENvbnN0cywgU3RvcmFnZSB9IGZyb20gXCJAY29tbW9uXCI7XG5cblxuY29uc3QgeyBDT0xPUl9BIH0gPSBUaGVtZS5nZXRUaGVtZSgpO1xudHlwZSBJUHJvcHMgPSB7XG4gIGdldFVybEJhc2U2NDogYW55LFxuICB1cGxvYWRVcmxCYXNlNjQ6IGFueTtcbiAgbW9kZWw6IGFueTtcbiAgc3VjY2Vzc0NhbGxiYWNrOiBhbnk7XG59O1xuXG50eXBlIElTdGF0ZSA9IHtcbiAgY2h1bmtzOiBhbnlbXTtcbiAgaXNTaG93VmlkZW86IGJvb2xlYW47XG4gIGlzU2hvd1N1Y2Nlc3M6IGJvb2xlYW47XG4gIGlzU2hvd0Vycm9yOiBib29sZWFuO1xuICBwcm9ncmVzc051bTogbnVtYmVyO1xuICBlcnJNc2c6IHN0cmluZztcbiAgaXNTaG93UHJvZ3Jlc3M6IGJvb2xlYW47XG4gIHZpZGVvQmFzZTY0OiBhbnk7XG59O1xuXG5jbGFzcyBJZGVudGlmeSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudDxJUHJvcHMsIElTdGF0ZT4ge1xuICB0aW1lb3V0OiBhbnkgPSB7fTtcbiAgcmVjb3JkZXI6IGFueSA9IHt9O1xuICBzbW9vdGhseUFkZEludGVydmFsOiBhbnkgPSB7fTtcbiAgbWVkaWFTdHJlYW1UcmFjazogYW55ID0ge307XG4gIHN0YXRlID0ge1xuICAgIGNodW5rczogW10sXG4gICAgaXNTaG93VmlkZW86IGZhbHNlLFxuICAgIGlzU2hvd1N1Y2Nlc3M6IGZhbHNlLFxuICAgIGlzU2hvd0Vycm9yOiBmYWxzZSxcbiAgICBpc1Nob3dQcm9ncmVzczogZmFsc2UsXG4gICAgcHJvZ3Jlc3NOdW06IDEsXG4gICAgZXJyTXNnOiBcIlwiLFxuICAgIHZpZGVvQmFzZTY0OiBcIlwiLFxuICB9O1xuXG4gIGNvbXBvbmVudFdpbGxVbm1vdW50KCk6IHZvaWQge1xuICAgIGNsZWFySW50ZXJ2YWwodGhpcy5zbW9vdGhseUFkZEludGVydmFsKTtcbiAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KTtcbiAgfVxuXG4gIHN1Y2Nlc3NQcm9ncmVzcygpIHtcbiAgICB0aGlzLnNtb290aGx5QWRkSW50ZXJ2YWwgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICBpZiAodGhpcy5zdGF0ZS5wcm9ncmVzc051bSA9PT0gMTAwKSB7XG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5zbW9vdGhseUFkZEludGVydmFsKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMuc3RhdGUucHJvZ3Jlc3NOdW0gPD0gOTUpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgcHJvZ3Jlc3NOdW06IHRoaXMuc3RhdGUucHJvZ3Jlc3NOdW0gKyAxLFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9LCAyNTApO1xuICB9XG5cbiAgYnRuVmlkZW8oKSB7XG4gICAgY29uc3QgX3RoaXMgPSB0aGlzO1xuICAgIC8vIOWPquiOt+WPluinhumikVxuICAgIGxldCBjb25zdHJhaW50cyA9IHsgYXVkaW86IGZhbHNlLCB2aWRlbzogdHJ1ZSB9O1xuICAgIGxldCB2aWRlbzogYW55ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzdHJlYW1cIik7XG4gICAgbmF2aWdhdG9yLm1lZGlhRGV2aWNlcy5nZXRVc2VyTWVkaWEoY29uc3RyYWludHMpLnRoZW4oKHN0cmVhbSkgPT4ge1xuICAgICAgaWYgKHZpZGVvKSB7XG4gICAgICAgIHZpZGVvLnNyY09iamVjdCA9IHN0cmVhbTtcbiAgICAgIH1cbiAgICAgIF90aGlzLm1lZGlhU3RyZWFtVHJhY2sgPSBzdHJlYW07XG4gICAgICBfdGhpcy5yZWNvcmRlciA9IG5ldyAod2luZG93IGFzIGFueSkuTWVkaWFSZWNvcmRlcihzdHJlYW0pO1xuICAgICAgX3RoaXMucmVjb3JkZXIub25kYXRhYXZhaWxhYmxlID0gdGhpcy5nZXRSZWNvcmRpbmdEYXRhLmJpbmQodGhpcyk7XG4gICAgICBfdGhpcy5yZWNvcmRlci5vbnN0b3AgPSB0aGlzLnNhdmVSZWNvcmRpbmdEYXRhLmJpbmQodGhpcyk7XG4gICAgfSkudGhlbigoKSA9PiB7XG4gICAgICBfdGhpcy5yZWNvcmRlci5zdGFydCgpO1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIGVyck1zZzogYFBsZWFzZSBmaXQgeW91ciBoZWFkIGluIHRoZSBmcmFtZWAsXG4gICAgICAgIH0sICgpID0+IHtcbiAgICAgICAgICBfdGhpcy5zZXRBY3Rpb25zKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSwgMTAwMCk7XG4gICAgfSk7XG4gICAgX3RoaXMudGltZW91dCA9IHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgX3RoaXMucmVjb3JkZXIuc3RvcCgpO1xuICAgIH0sIDUwMDApO1xuICB9XG5cbiAgZ2V0UmVjb3JkaW5nRGF0YShlOiBhbnkpIHtcbiAgICBsZXQgY2h1bmtzOiBhbnlbXSA9IFtdO1xuICAgIGNodW5rcy5wdXNoKGUuZGF0YSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBjaHVua3MsXG4gICAgfSk7XG4gIH1cblxuICBzZXRBY3Rpb25zKCkge1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGVyck1zZzogYFBsZWFzZSBibGluayBleWVzYCxcbiAgICAgIH0sICgpID0+IHtcblxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIGVyck1zZzogYFBsZWFzZSBvcGVuIG1vdXRoYCxcbiAgICAgICAgICB9LCAoKSA9PiB7XG5cbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBlcnJNc2c6IGBQbGVhc2Ugd2FpdGluZyAuLi5gLFxuICAgICAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICBpc1Nob3dQcm9ncmVzczogdHJ1ZSxcbiAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICB0aGlzLnN1Y2Nlc3NQcm9ncmVzcygpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sIDIwMDApO1xuXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0sIDIwMDApO1xuXG4gICAgICB9KTtcbiAgICB9LCAxMDAwKTtcblxuICB9XG5cbiAgc2F2ZVJlY29yZGluZ0RhdGEoKSB7XG4gICAgY29uc3QgeyBjaHVua3MgfSA9IHRoaXMuc3RhdGU7XG4gICAgbGV0IGN1cnJWaWRlbzogYW55O1xuICAgIGxldCBibG9iID0gbmV3IEJsb2IoY2h1bmtzLCB7IFwidHlwZVwiOiBcInZpZGVvL21wNFwiIH0pO1xuICAgIGN1cnJWaWRlbyA9IFVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XG4gICAgLy8gdGhpcy5zZXRTdGF0ZSh7XG4gICAgLy8gICBpc1Nob3dQcm9ncmVzczogdHJ1ZSxcbiAgICAvLyB9LCAoKSA9PiB7XG4gICAgLy8gICB0aGlzLnN1Y2Nlc3NQcm9ncmVzcygpO1xuICAgIC8vIH0pO1xuICAgIHRoaXMuYmxvYlRvQmFzZTY0KGJsb2IsIChyZXM6IGFueSkgPT4ge1xuICAgICAgLy8g6L2s5YyW5ZCO55qEYmFzZTY0XG4gICAgICB0aGlzLnBvc3RMaXZlbmVzcyhyZXMpO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGNodW5rczogW10sXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGJsb2JUb0Jhc2U2NChibG9iOiBhbnksIGNhbGxiYWNrOiBhbnkpIHtcbiAgICBsZXQgcmVhZGVyOiBhbnkgPSBuZXcgRmlsZVJlYWRlcigpO1xuICAgIHJlYWRlci5vbmxvYWQgPSBmdW5jdGlvbihlOiBhbnkpIHtcbiAgICAgIGNhbGxiYWNrKGUudGFyZ2V0LnJlc3VsdCk7XG4gICAgfTtcbiAgICByZWFkZXIucmVhZEFzRGF0YVVSTChibG9iKTtcbiAgfVxuXG4gIHBvc3RMaXZlbmVzcyh2aWRlb0Jhc2U2NDogYW55KSB7XG4gICAgY29uc3QgeyB1cGxvYWRVcmxCYXNlNjQsIG1vZGVsIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGF1dGhLZXkgPSBTdG9yYWdlLkF1dGguc2Vzc2lvbigpLmdldChDb25zdHMuQVVUSF9LRVkpO1xuICAgIEFqYXgucG9zdChgL2t5Yy9saXZlbmVzc2AsIHtcbiAgICAgIC4uLntcbiAgICAgICAgaW1hZ2VCYXNlNjQ6IHVwbG9hZFVybEJhc2U2NCxcbiAgICAgICAgdmlkZW9CYXNlNjQ6IHZpZGVvQmFzZTY0LFxuICAgICAgICBhY3Rpb25zU2VxdWVuY2U6IFwiMSwyXCIsXG4gICAgICB9LFxuICAgICAgLi4ubW9kZWwsXG4gICAgfSwge1xuICAgICAgaGVhZGVyczoge1xuICAgICAgICBcInJlY2FwdGNoYS10b2tlblwiOiBhdXRoS2V5LFxuICAgICAgfSxcbiAgICB9KS50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgY29uc3QgcmVzcERhdGEgPSAocmVzLmJvZHkgfHwge30pLnJlc3BEYXRhIHx8IHt9O1xuICAgICAgY29uc3Qgc3VjY2VzcyA9IF8uZ2V0KHJlc3BEYXRhLCBcInN1Y2Nlc3NcIik7XG4gICAgICBjb25zdCBzdG9wQ2hlY2sgPSBfLmdldChyZXNwRGF0YSwgXCJzdG9wQ2hlY2tcIik7XG4gICAgICBjb25zdCBlcnJNc2cgPSBfLmdldChyZXNwRGF0YSwgXCJlcnJNc2dcIik7XG4gICAgICBpZiAoc3VjY2Vzcykge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBpc1Nob3dTdWNjZXNzOiB0cnVlLFxuICAgICAgICAgIGlzU2hvd0Vycm9yOiBmYWxzZSxcbiAgICAgICAgICBwcm9ncmVzc051bTogMTAwLFxuICAgICAgICAgIHZpZGVvQmFzZTY0OiB2aWRlb0Jhc2U2NCxcbiAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgIHRoaXMuc3RvcFRha2VQaG90bygpO1xuICAgICAgICAgIHRoaXMucHJvcHMuc3VjY2Vzc0NhbGxiYWNrKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKHN0b3BDaGVjaykge1xuICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgaXNTaG93U3VjY2VzczogZmFsc2UsXG4gICAgICAgICAgICBpc1Nob3dFcnJvcjogdHJ1ZSxcbiAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnN0b3BUYWtlUGhvdG8oKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAvLyAgIGVyck1zZyxcbiAgICAgICAgICAvLyB9KTtcbiAgICAgICAgICBpZiAodGhpcy5yZWNvcmRlcikge1xuICAgICAgICAgICAgdGhpcy5yZWNvcmRlci5zdGFydCgpO1xuICAgICAgICAgICAgdGhpcy5zZXRBY3Rpb25zKCk7XG4gICAgICAgICAgICB0aGlzLnRpbWVvdXQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5yZWNvcmRlci5zdG9wKCk7XG4gICAgICAgICAgICB9LCA1MDAwKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgc3RvcFRha2VQaG90bygpIHtcbiAgICBpZiAodGhpcy5tZWRpYVN0cmVhbVRyYWNrKSB7XG4gICAgICB0aGlzLm1lZGlhU3RyZWFtVHJhY2suZ2V0VHJhY2tzICYmIHRoaXMubWVkaWFTdHJlYW1UcmFjay5nZXRUcmFja3MoKVswXS5zdG9wKCk7XG4gICAgfVxuICB9XG5cbiAgdHJ5QWdhaW4oKSB7XG4gICAgY29uc3QgYXV0aEtleSA9IFN0b3JhZ2UuQXV0aC5zZXNzaW9uKCkuZ2V0KENvbnN0cy5BVVRIX0tFWSk7XG4gICAgY29uc3QgeyBtb2RlbCB9ID0gdGhpcy5wcm9wcztcbiAgICBBamF4LnBhdGNoKGAva3ljL2xpdmVuZXNzL3JlYWN0aXZhdGlvbmAsIHtcbiAgICAgIGJpeklkOiBfLmdldChtb2RlbCwgXCJiaXpJZFwiKSxcbiAgICAgIGJpelR5cGU6IFwiTkJcIixcbiAgICB9LCB7XG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIFwicmVjYXB0Y2hhLXRva2VuXCI6IGF1dGhLZXksXG4gICAgICB9LFxuICAgICAgbG9hZGluZzogdHJ1ZSxcbiAgICB9KS50aGVuKChyZXM6IGFueSkgPT4ge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIGlzU2hvd0Vycm9yOiBmYWxzZSxcbiAgICAgICAgaXNTaG93U3VjY2VzczogZmFsc2UsXG4gICAgICAgIGlzU2hvd1Byb2dyZXNzOiBmYWxzZSxcbiAgICAgICAgcHJvZ3Jlc3NOdW06IDAsXG4gICAgICAgIGVyck1zZzogXCJcIixcbiAgICAgIH0sICgpID0+IHtcbiAgICAgICAgdGhpcy5idG5WaWRlbygpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBpc1Nob3dWaWRlbywgaXNTaG93U3VjY2VzcywgZXJyTXNnLCBpc1Nob3dFcnJvciwgaXNTaG93UHJvZ3Jlc3MgfSA9IHRoaXMuc3RhdGU7XG4gICAgcmV0dXJuIDxWaWRlb1N0eWxlPlxuICAgICAgPFJvdz5cbiAgICAgICAgPENvbCBzcGFuPXs2fT5TdGVwIDI8L0NvbD5cbiAgICAgICAgPENvbCBzcGFuPXsxOH0+XG4gICAgICAgICAgRmFjZSByZWNvZ25pdGlvbi4gUGxlYXNlIGZvbGxvdyB0aGUgZ3VpZGFuY2VcbiAgICAgICAgPC9Db2w+XG4gICAgICA8L1Jvdz5cbiAgICAgIDxSb3c+XG4gICAgICAgIDxDb2wgc3Bhbj17Nn0vPlxuICAgICAgICB7IWlzU2hvd1ZpZGVvICYmICFpc1Nob3dTdWNjZXNzICYmICFpc1Nob3dFcnJvciAmJiA8Q29sIHNwYW49ezE4fT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJ2aWRlby1iZWZvcmVcIn0+XG4gICAgICAgICAgICB7ZXJyTXNnICYmIDxwIGNsYXNzTmFtZT17XCJlcnItbXNnIHNoYWtlXCJ9PntlcnJNc2d9PC9wPn1cbiAgICAgICAgICAgIHshZXJyTXNnICYmIDxwPntcIlBsZWFzZSBmaXQgeW91ciBoZWFkIGluIHRoZSBmcmFtZVwifTwvcD59XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17XCJ2aWRlby1idG5cIn0+XG4gICAgICAgICAgICAgIDxCdXR0b24gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnByb3BzLnVwbG9hZFVybEJhc2U2NCkge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGlzU2hvd1ZpZGVvOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmJ0blZpZGVvKCk7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIGVyck1zZzogXCJQbGVhc2UgdGFrZSBwaG90byBvZiB5b3VyIElEIG9yIHBhc3Nwb3J0XCIsXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17XCJpY29uZm9udCBpY29uLXZpZGVvXCJ9Lz4gTGF1bmNoIENhbWVyYVxuICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0NvbD59XG4gICAgICAgIHtpc1Nob3dWaWRlbyAmJiAhaXNTaG93U3VjY2VzcyAmJiAhaXNTaG93RXJyb3IgJiYgPENvbCBzcGFuPXsxOH0gY2xhc3NOYW1lPXtcInZpZGVvLWFyZWFcIn0+XG4gICAgICAgICAge2Vyck1zZyAmJiA8cCBjbGFzc05hbWU9e1wiZXJyLW1zZyBzaGFrZVwifT57ZXJyTXNnfTwvcD59XG4gICAgICAgICAgeyFlcnJNc2cgJiYgPHA+e1wiUGxlYXNlIGZpdCB5b3VyIGhlYWQgaW4gdGhlIGZyYW1lXCJ9PC9wPn1cbiAgICAgICAgICA8dmlkZW8gYXV0b1BsYXkgaWQ9XCJzdHJlYW1cIi8+XG4gICAgICAgICAge2lzU2hvd1Byb2dyZXNzICYmIDxkaXYgY2xhc3NOYW1lPXtcInZpZGVvLXByb2dyZXNzXCJ9PlxuICAgICAgICAgICAgPFByb2dyZXNzIHN0YXR1cz1cImFjdGl2ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImNpcmNsZVwiXG4gICAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezI0OH1cbiAgICAgICAgICAgICAgICAgICAgICBzdHJva2VDb2xvcj17Q09MT1JfQX1cbiAgICAgICAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17MX1cbiAgICAgICAgICAgICAgICAgICAgICBmb3JtYXQ9eygpID0+IDxkaXYvPn1cbiAgICAgICAgICAgICAgICAgICAgICBwZXJjZW50PXt0aGlzLnN0YXRlLnByb2dyZXNzTnVtfS8+XG4gICAgICAgICAgPC9kaXY+fVxuICAgICAgICA8L0NvbD59XG4gICAgICAgIHtpc1Nob3dTdWNjZXNzICYmXG4gICAgICAgIDxDb2wgc3Bhbj17MTh9IGNsYXNzTmFtZT17XCJ2aWRlby1hcmVhXCJ9PlxuICAgICAgICAgIDxwPkZhY2UgdmFsaWRhdGlvbiBwYXNzZWQuIFRoYW5rIHlvdSE8L3A+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1widmlkZW8tcmVzdWx0XCJ9PlxuICAgICAgICAgICAgPHZpZGVvIHNyYz17dGhpcy5zdGF0ZS52aWRlb0Jhc2U2NH0vPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e1widmlkZW8tcHJvZ3Jlc3NcIn0+XG4gICAgICAgICAgICAgIDxQcm9ncmVzcyBzdGF0dXM9XCJhY3RpdmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImNpcmNsZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD17MjQ4fVxuICAgICAgICAgICAgICAgICAgICAgICAgc3Ryb2tlQ29sb3I9e0NPTE9SX0F9XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17MX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdD17KCkgPT4gPGkgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IENPTE9SX0EsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjYwcHhcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIH19IGNsYXNzTmFtZT17XCJpY29uZm9udCAgaWNvbi14aWFvXCJ9Lz59XG4gICAgICAgICAgICAgICAgICAgICAgICBwZXJjZW50PXt0aGlzLnN0YXRlLnByb2dyZXNzTnVtfS8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9Db2w+XG4gICAgICAgIH1cbiAgICAgICAge2lzU2hvd0Vycm9yICYmXG4gICAgICAgIDxDb2wgc3Bhbj17MTh9IGNsYXNzTmFtZT17XCJ2aWRlby1hcmVhXCJ9PlxuICAgICAgICAgIDxwPkZhY2UgcmVjb2duaXRpb24gZmFpbGVkLiBQbGVhc2UgdHJ5IGFnYWluIG9yIHNraXAgdGhpcyBzdGVwPC9wPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcInZpZGVvLXJlc3VsdFwifT5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtcImZhaWxcIn0+XG4gICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPXtcImljb25mb250IGljb24ta3VcIn0vPlxuICAgICAgICAgICAgICAgIDxCdXR0b24gb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgdGhpcy50cnlBZ2FpbigpO1xuICAgICAgICAgICAgICAgIH19PlxuICAgICAgICAgICAgICAgICAgVHJ5IEFnYWluXG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvQ29sPlxuICAgICAgICB9XG4gICAgICA8L1Jvdz5cbiAgICA8L1ZpZGVvU3R5bGU+O1xuICB9XG5cblxufVxuXG5jb25zdCBWaWRlb1N0eWxlID0gU3R5bGVkLmRpdmBcbiAgXG4gICBAa2V5ZnJhbWVzIHNoYWtlLW1zZyB7XG4gICAgMjUlIHtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCg2cHgpO1xuICAgIH1cbiAgICA1MCUge1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC00cHgpO1xuICAgIH1cbiAgICA3NSUge1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDJweCk7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xuICAgIH1cbiAgfVxuICB2aWRlbyB7XG4gICAgd2lkdGg6IDIzOHB4O1xuICAgIGhlaWdodDogMjM4cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIG1hcmdpbi1sZWZ0OiA4MHB4O1xuICAgIHRyYW5zZm9ybTogcm90YXRlWSgxODBkZWcpO1xuICB9XG4gIC52aWRlby1iZWZvcmUge1xuICAgIHdpZHRoOiA0MDBweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgaGVpZ2h0OiAzNDhweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZDlkOWQ5O1xuICAgIG1hcmdpbjogMjBweCAwO1xuICAgIHAge1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgbWFyZ2luOiAyMHB4IDA7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIGNvbG9yOiAjMDAwO1xuICAgIH1cbiAgICAudmlkZW8tYnRuIHtcbiAgICAgIHdpZHRoOiAyNDhweDtcbiAgICAgIGhlaWdodDogMjQ4cHg7XG4gICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICBib3JkZXI6IDFweCBkYXNoZWQgI2Q5ZDlkOTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBtYXJnaW46IGF1dG87XG4gICAgICBtYXJnaW4tdG9wOiAxNHB4O1xuICAgICAgLmljb24tdmlkZW8ge1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAudmlkZW8tYXJlYSB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2Q5ZDlkOTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgd2lkdGg6IDQwMHB4O1xuICAgIGhlaWdodDogMzQ4cHg7XG4gICAgbWFyZ2luOiAyMHB4IDA7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAudmlkZW8tcHJvZ3Jlc3Mge1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiA2MXB4O1xuICAgICAgbGVmdDogODBweDtcbiAgICB9XG4gICAgLnZpZGVvLWFmdGVyIHtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgd2lkdGg6IDQwMHB4O1xuICAgICAgICBoZWlnaHQ6IDM0OHB4O1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICBwIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW46IDIwcHggMDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgfVxuICAgICAgLnZpZGVvLWhlYWRlciB7XG4gICAgICAgIHdpZHRoOiAyMzhweDtcbiAgICAgICAgaGVpZ2h0OiAyMzhweDtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGJvcmRlcjogMXB4IGRhc2hlZCAjMmYyZTJlO1xuICAgICAgfVxuICAgIH1cbiAgICBcbiAgfVxuICAudmlkZW8tYXJlYSB7XG4gICAgIHAge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbjogMjBweCAwO1xuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgIH1cbiAgICAgLnZpZGVvLXJlc3VsdCB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICB2aWRlbyB7XG4gICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMXB4O1xuICAgICAgICB9XG4gICAgIH1cbiAgICAgLmFudC1wcm9ncmVzcy1pbm5lciB7XG4gICAgICAgIGZvbnQtc2l6ZTogNDVweCAhaW1wb3J0YW50O1xuICAgICB9XG4gIH1cbiAgLmZhaWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgY29sb3I6ICNmZjAwMDA7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAuaWNvbi1rdSB7XG4gICAgICBmb250LXNpemU6IDEwMHB4O1xuICAgIH1cbiAgICAmID4gZGl2IHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgLmFudC1idG4ge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBtYXJnaW46IDAgMCAwIDVweDtcbiAgICB9XG4gIH1cbiAgcC5lcnItbXNnIHtcbiAgICAgJi5zaGFrZSB7XG4gICAgICBhbmltYXRpb246IHNoYWtlLW1zZyAuNHMgZWFzZS1pbi1vdXQgZm9yd2FyZHM7XG4gICAgfVxuICB9XG5gO1xuZXhwb3J0IGRlZmF1bHQgSWRlbnRpZnk7XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQWtCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBOzs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7OztBQUVBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUhBO0FBT0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBWkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFVQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJQTtBQVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFZQTs7OztBQTVTQTtBQUNBO0FBZ1RBO0FBK0hBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./src/app/desk/ekyc/identify.tsx
