/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var _util = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var each = _util.each;
var map = _util.map;
var isFunction = _util.isFunction;
var createHashMap = _util.createHashMap;
var noop = _util.noop;

var _task = __webpack_require__(/*! ./task */ "./node_modules/echarts/lib/stream/task.js");

var createTask = _task.createTask;

var _component = __webpack_require__(/*! ../util/component */ "./node_modules/echarts/lib/util/component.js");

var getUID = _component.getUID;

var GlobalModel = __webpack_require__(/*! ../model/Global */ "./node_modules/echarts/lib/model/Global.js");

var ExtensionAPI = __webpack_require__(/*! ../ExtensionAPI */ "./node_modules/echarts/lib/ExtensionAPI.js");

var _model = __webpack_require__(/*! ../util/model */ "./node_modules/echarts/lib/util/model.js");

var normalizeToArray = _model.normalizeToArray;
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @module echarts/stream/Scheduler
 */

/**
 * @constructor
 */

function Scheduler(ecInstance, api, dataProcessorHandlers, visualHandlers) {
  this.ecInstance = ecInstance;
  this.api = api;
  this.unfinished; // Fix current processors in case that in some rear cases that
  // processors might be registered after echarts instance created.
  // Register processors incrementally for a echarts instance is
  // not supported by this stream architecture.

  var dataProcessorHandlers = this._dataProcessorHandlers = dataProcessorHandlers.slice();
  var visualHandlers = this._visualHandlers = visualHandlers.slice();
  this._allHandlers = dataProcessorHandlers.concat(visualHandlers);
  /**
   * @private
   * @type {
   *     [handlerUID: string]: {
   *         seriesTaskMap?: {
   *             [seriesUID: string]: Task
   *         },
   *         overallTask?: Task
   *     }
   * }
   */

  this._stageTaskMap = createHashMap();
}

var proto = Scheduler.prototype;
/**
 * @param {module:echarts/model/Global} ecModel
 * @param {Object} payload
 */

proto.restoreData = function (ecModel, payload) {
  // TODO: Only restroe needed series and components, but not all components.
  // Currently `restoreData` of all of the series and component will be called.
  // But some independent components like `title`, `legend`, `graphic`, `toolbox`,
  // `tooltip`, `axisPointer`, etc, do not need series refresh when `setOption`,
  // and some components like coordinate system, axes, dataZoom, visualMap only
  // need their target series refresh.
  // (1) If we are implementing this feature some day, we should consider these cases:
  // if a data processor depends on a component (e.g., dataZoomProcessor depends
  // on the settings of `dataZoom`), it should be re-performed if the component
  // is modified by `setOption`.
  // (2) If a processor depends on sevral series, speicified by its `getTargetSeries`,
  // it should be re-performed when the result array of `getTargetSeries` changed.
  // We use `dependencies` to cover these issues.
  // (3) How to update target series when coordinate system related components modified.
  // TODO: simply the dirty mechanism? Check whether only the case here can set tasks dirty,
  // and this case all of the tasks will be set as dirty.
  ecModel.restoreData(payload); // Theoretically an overall task not only depends on each of its target series, but also
  // depends on all of the series.
  // The overall task is not in pipeline, and `ecModel.restoreData` only set pipeline tasks
  // dirty. If `getTargetSeries` of an overall task returns nothing, we should also ensure
  // that the overall task is set as dirty and to be performed, otherwise it probably cause
  // state chaos. So we have to set dirty of all of the overall tasks manually, otherwise it
  // probably cause state chaos (consider `dataZoomProcessor`).

  this._stageTaskMap.each(function (taskRecord) {
    var overallTask = taskRecord.overallTask;
    overallTask && overallTask.dirty();
  });
}; // If seriesModel provided, incremental threshold is check by series data.


proto.getPerformArgs = function (task, isBlock) {
  // For overall task
  if (!task.__pipeline) {
    return;
  }

  var pipeline = this._pipelineMap.get(task.__pipeline.id);

  var pCtx = pipeline.context;
  var incremental = !isBlock && pipeline.progressiveEnabled && (!pCtx || pCtx.progressiveRender) && task.__idxInPipeline > pipeline.blockIndex;
  var step = incremental ? pipeline.step : null;
  var modDataCount = pCtx && pCtx.modDataCount;
  var modBy = modDataCount != null ? Math.ceil(modDataCount / step) : null;
  return {
    step: step,
    modBy: modBy,
    modDataCount: modDataCount
  };
};

proto.getPipeline = function (pipelineId) {
  return this._pipelineMap.get(pipelineId);
};
/**
 * Current, progressive rendering starts from visual and layout.
 * Always detect render mode in the same stage, avoiding that incorrect
 * detection caused by data filtering.
 * Caution:
 * `updateStreamModes` use `seriesModel.getData()`.
 */


proto.updateStreamModes = function (seriesModel, view) {
  var pipeline = this._pipelineMap.get(seriesModel.uid);

  var data = seriesModel.getData();
  var dataLen = data.count(); // `progressiveRender` means that can render progressively in each
  // animation frame. Note that some types of series do not provide
  // `view.incrementalPrepareRender` but support `chart.appendData`. We
  // use the term `incremental` but not `progressive` to describe the
  // case that `chart.appendData`.

  var progressiveRender = pipeline.progressiveEnabled && view.incrementalPrepareRender && dataLen >= pipeline.threshold;
  var large = seriesModel.get('large') && dataLen >= seriesModel.get('largeThreshold'); // TODO: modDataCount should not updated if `appendData`, otherwise cause whole repaint.
  // see `test/candlestick-large3.html`

  var modDataCount = seriesModel.get('progressiveChunkMode') === 'mod' ? dataLen : null;
  seriesModel.pipelineContext = pipeline.context = {
    progressiveRender: progressiveRender,
    modDataCount: modDataCount,
    large: large
  };
};

proto.restorePipelines = function (ecModel) {
  var scheduler = this;
  var pipelineMap = scheduler._pipelineMap = createHashMap();
  ecModel.eachSeries(function (seriesModel) {
    var progressive = seriesModel.getProgressive();
    var pipelineId = seriesModel.uid;
    pipelineMap.set(pipelineId, {
      id: pipelineId,
      head: null,
      tail: null,
      threshold: seriesModel.getProgressiveThreshold(),
      progressiveEnabled: progressive && !(seriesModel.preventIncremental && seriesModel.preventIncremental()),
      blockIndex: -1,
      step: Math.round(progressive || 700),
      count: 0
    });
    pipe(scheduler, seriesModel, seriesModel.dataTask);
  });
};

proto.prepareStageTasks = function () {
  var stageTaskMap = this._stageTaskMap;
  var ecModel = this.ecInstance.getModel();
  var api = this.api;
  each(this._allHandlers, function (handler) {
    var record = stageTaskMap.get(handler.uid) || stageTaskMap.set(handler.uid, []);
    handler.reset && createSeriesStageTask(this, handler, record, ecModel, api);
    handler.overallReset && createOverallStageTask(this, handler, record, ecModel, api);
  }, this);
};

proto.prepareView = function (view, model, ecModel, api) {
  var renderTask = view.renderTask;
  var context = renderTask.context;
  context.model = model;
  context.ecModel = ecModel;
  context.api = api;
  renderTask.__block = !view.incrementalPrepareRender;
  pipe(this, model, renderTask);
};

proto.performDataProcessorTasks = function (ecModel, payload) {
  // If we do not use `block` here, it should be considered when to update modes.
  performStageTasks(this, this._dataProcessorHandlers, ecModel, payload, {
    block: true
  });
}; // opt
// opt.visualType: 'visual' or 'layout'
// opt.setDirty


proto.performVisualTasks = function (ecModel, payload, opt) {
  performStageTasks(this, this._visualHandlers, ecModel, payload, opt);
};

function performStageTasks(scheduler, stageHandlers, ecModel, payload, opt) {
  opt = opt || {};
  var unfinished;
  each(stageHandlers, function (stageHandler, idx) {
    if (opt.visualType && opt.visualType !== stageHandler.visualType) {
      return;
    }

    var stageHandlerRecord = scheduler._stageTaskMap.get(stageHandler.uid);

    var seriesTaskMap = stageHandlerRecord.seriesTaskMap;
    var overallTask = stageHandlerRecord.overallTask;

    if (overallTask) {
      var overallNeedDirty;
      var agentStubMap = overallTask.agentStubMap;
      agentStubMap.each(function (stub) {
        if (needSetDirty(opt, stub)) {
          stub.dirty();
          overallNeedDirty = true;
        }
      });
      overallNeedDirty && overallTask.dirty();
      updatePayload(overallTask, payload);
      var performArgs = scheduler.getPerformArgs(overallTask, opt.block); // Execute stubs firstly, which may set the overall task dirty,
      // then execute the overall task. And stub will call seriesModel.setData,
      // which ensures that in the overallTask seriesModel.getData() will not
      // return incorrect data.

      agentStubMap.each(function (stub) {
        stub.perform(performArgs);
      });
      unfinished |= overallTask.perform(performArgs);
    } else if (seriesTaskMap) {
      seriesTaskMap.each(function (task, pipelineId) {
        if (needSetDirty(opt, task)) {
          task.dirty();
        }

        var performArgs = scheduler.getPerformArgs(task, opt.block);
        performArgs.skip = !stageHandler.performRawSeries && ecModel.isSeriesFiltered(task.context.model);
        updatePayload(task, payload);
        unfinished |= task.perform(performArgs);
      });
    }
  });

  function needSetDirty(opt, task) {
    return opt.setDirty && (!opt.dirtyMap || opt.dirtyMap.get(task.__pipeline.id));
  }

  scheduler.unfinished |= unfinished;
}

proto.performSeriesTasks = function (ecModel) {
  var unfinished;
  ecModel.eachSeries(function (seriesModel) {
    // Progress to the end for dataInit and dataRestore.
    unfinished |= seriesModel.dataTask.perform();
  });
  this.unfinished |= unfinished;
};

proto.plan = function () {
  // Travel pipelines, check block.
  this._pipelineMap.each(function (pipeline) {
    var task = pipeline.tail;

    do {
      if (task.__block) {
        pipeline.blockIndex = task.__idxInPipeline;
        break;
      }

      task = task.getUpstream();
    } while (task);
  });
};

var updatePayload = proto.updatePayload = function (task, payload) {
  payload !== 'remain' && (task.context.payload = payload);
};

function createSeriesStageTask(scheduler, stageHandler, stageHandlerRecord, ecModel, api) {
  var seriesTaskMap = stageHandlerRecord.seriesTaskMap || (stageHandlerRecord.seriesTaskMap = createHashMap());
  var seriesType = stageHandler.seriesType;
  var getTargetSeries = stageHandler.getTargetSeries; // If a stageHandler should cover all series, `createOnAllSeries` should be declared mandatorily,
  // to avoid some typo or abuse. Otherwise if an extension do not specify a `seriesType`,
  // it works but it may cause other irrelevant charts blocked.

  if (stageHandler.createOnAllSeries) {
    ecModel.eachRawSeries(create);
  } else if (seriesType) {
    ecModel.eachRawSeriesByType(seriesType, create);
  } else if (getTargetSeries) {
    getTargetSeries(ecModel, api).each(create);
  }

  function create(seriesModel) {
    var pipelineId = seriesModel.uid; // Init tasks for each seriesModel only once.
    // Reuse original task instance.

    var task = seriesTaskMap.get(pipelineId) || seriesTaskMap.set(pipelineId, createTask({
      plan: seriesTaskPlan,
      reset: seriesTaskReset,
      count: seriesTaskCount
    }));
    task.context = {
      model: seriesModel,
      ecModel: ecModel,
      api: api,
      useClearVisual: stageHandler.isVisual && !stageHandler.isLayout,
      plan: stageHandler.plan,
      reset: stageHandler.reset,
      scheduler: scheduler
    };
    pipe(scheduler, seriesModel, task);
  } // Clear unused series tasks.


  var pipelineMap = scheduler._pipelineMap;
  seriesTaskMap.each(function (task, pipelineId) {
    if (!pipelineMap.get(pipelineId)) {
      task.dispose();
      seriesTaskMap.removeKey(pipelineId);
    }
  });
}

function createOverallStageTask(scheduler, stageHandler, stageHandlerRecord, ecModel, api) {
  var overallTask = stageHandlerRecord.overallTask = stageHandlerRecord.overallTask // For overall task, the function only be called on reset stage.
  || createTask({
    reset: overallTaskReset
  });
  overallTask.context = {
    ecModel: ecModel,
    api: api,
    overallReset: stageHandler.overallReset,
    scheduler: scheduler
  }; // Reuse orignal stubs.

  var agentStubMap = overallTask.agentStubMap = overallTask.agentStubMap || createHashMap();
  var seriesType = stageHandler.seriesType;
  var getTargetSeries = stageHandler.getTargetSeries;
  var overallProgress = true;
  var modifyOutputEnd = stageHandler.modifyOutputEnd; // An overall task with seriesType detected or has `getTargetSeries`, we add
  // stub in each pipelines, it will set the overall task dirty when the pipeline
  // progress. Moreover, to avoid call the overall task each frame (too frequent),
  // we set the pipeline block.

  if (seriesType) {
    ecModel.eachRawSeriesByType(seriesType, createStub);
  } else if (getTargetSeries) {
    getTargetSeries(ecModel, api).each(createStub);
  } // Otherwise, (usually it is legancy case), the overall task will only be
  // executed when upstream dirty. Otherwise the progressive rendering of all
  // pipelines will be disabled unexpectedly. But it still needs stubs to receive
  // dirty info from upsteam.
  else {
      overallProgress = false;
      each(ecModel.getSeries(), createStub);
    }

  function createStub(seriesModel) {
    var pipelineId = seriesModel.uid;
    var stub = agentStubMap.get(pipelineId);

    if (!stub) {
      stub = agentStubMap.set(pipelineId, createTask({
        reset: stubReset,
        onDirty: stubOnDirty
      })); // When the result of `getTargetSeries` changed, the overallTask
      // should be set as dirty and re-performed.

      overallTask.dirty();
    }

    stub.context = {
      model: seriesModel,
      overallProgress: overallProgress,
      modifyOutputEnd: modifyOutputEnd
    };
    stub.agent = overallTask;
    stub.__block = overallProgress;
    pipe(scheduler, seriesModel, stub);
  } // Clear unused stubs.


  var pipelineMap = scheduler._pipelineMap;
  agentStubMap.each(function (stub, pipelineId) {
    if (!pipelineMap.get(pipelineId)) {
      stub.dispose(); // When the result of `getTargetSeries` changed, the overallTask
      // should be set as dirty and re-performed.

      overallTask.dirty();
      agentStubMap.removeKey(pipelineId);
    }
  });
}

function overallTaskReset(context) {
  context.overallReset(context.ecModel, context.api, context.payload);
}

function stubReset(context, upstreamContext) {
  return context.overallProgress && stubProgress;
}

function stubProgress() {
  this.agent.dirty();
  this.getDownstream().dirty();
}

function stubOnDirty() {
  this.agent && this.agent.dirty();
}

function seriesTaskPlan(context) {
  return context.plan && context.plan(context.model, context.ecModel, context.api, context.payload);
}

function seriesTaskReset(context) {
  if (context.useClearVisual) {
    context.data.clearAllVisual();
  }

  var resetDefines = context.resetDefines = normalizeToArray(context.reset(context.model, context.ecModel, context.api, context.payload));
  return resetDefines.length > 1 ? map(resetDefines, function (v, idx) {
    return makeSeriesTaskProgress(idx);
  }) : singleSeriesTaskProgress;
}

var singleSeriesTaskProgress = makeSeriesTaskProgress(0);

function makeSeriesTaskProgress(resetDefineIdx) {
  return function (params, context) {
    var data = context.data;
    var resetDefine = context.resetDefines[resetDefineIdx];

    if (resetDefine && resetDefine.dataEach) {
      for (var i = params.start; i < params.end; i++) {
        resetDefine.dataEach(data, i);
      }
    } else if (resetDefine && resetDefine.progress) {
      resetDefine.progress(params, data);
    }
  };
}

function seriesTaskCount(context) {
  return context.data.count();
}

function pipe(scheduler, seriesModel, task) {
  var pipelineId = seriesModel.uid;

  var pipeline = scheduler._pipelineMap.get(pipelineId);

  !pipeline.head && (pipeline.head = task);
  pipeline.tail && pipeline.tail.pipe(task);
  pipeline.tail = task;
  task.__idxInPipeline = pipeline.count++;
  task.__pipeline = pipeline;
}

Scheduler.wrapStageHandler = function (stageHandler, visualType) {
  if (isFunction(stageHandler)) {
    stageHandler = {
      overallReset: stageHandler,
      seriesType: detectSeriseType(stageHandler)
    };
  }

  stageHandler.uid = getUID('stageHandler');
  visualType && (stageHandler.visualType = visualType);
  return stageHandler;
};
/**
 * Only some legacy stage handlers (usually in echarts extensions) are pure function.
 * To ensure that they can work normally, they should work in block mode, that is,
 * they should not be started util the previous tasks finished. So they cause the
 * progressive rendering disabled. We try to detect the series type, to narrow down
 * the block range to only the series type they concern, but not all series.
 */


function detectSeriseType(legacyFunc) {
  seriesType = null;

  try {
    // Assume there is no async when calling `eachSeriesByType`.
    legacyFunc(ecModelMock, apiMock);
  } catch (e) {}

  return seriesType;
}

var ecModelMock = {};
var apiMock = {};
var seriesType;
mockMethods(ecModelMock, GlobalModel);
mockMethods(apiMock, ExtensionAPI);

ecModelMock.eachSeriesByType = ecModelMock.eachRawSeriesByType = function (type) {
  seriesType = type;
};

ecModelMock.eachComponent = function (cond) {
  if (cond.mainType === 'series' && cond.subType) {
    seriesType = cond.subType;
  }
};

function mockMethods(target, Clz) {
  /* eslint-disable */
  for (var name in Clz.prototype) {
    // Do not use hasOwnProperty
    target[name] = noop;
  }
  /* eslint-enable */

}

var _default = Scheduler;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvc3RyZWFtL1NjaGVkdWxlci5qcy5qcyIsInNvdXJjZXMiOlsiL1VzZXJzL2J5dGVzZm9yY2UvRGVza3RvcC9iMmIvbm9kZV9tb2R1bGVzL2VjaGFydHMvbGliL3N0cmVhbS9TY2hlZHVsZXIuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbnZhciBfdXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBlYWNoID0gX3V0aWwuZWFjaDtcbnZhciBtYXAgPSBfdXRpbC5tYXA7XG52YXIgaXNGdW5jdGlvbiA9IF91dGlsLmlzRnVuY3Rpb247XG52YXIgY3JlYXRlSGFzaE1hcCA9IF91dGlsLmNyZWF0ZUhhc2hNYXA7XG52YXIgbm9vcCA9IF91dGlsLm5vb3A7XG5cbnZhciBfdGFzayA9IHJlcXVpcmUoXCIuL3Rhc2tcIik7XG5cbnZhciBjcmVhdGVUYXNrID0gX3Rhc2suY3JlYXRlVGFzaztcblxudmFyIF9jb21wb25lbnQgPSByZXF1aXJlKFwiLi4vdXRpbC9jb21wb25lbnRcIik7XG5cbnZhciBnZXRVSUQgPSBfY29tcG9uZW50LmdldFVJRDtcblxudmFyIEdsb2JhbE1vZGVsID0gcmVxdWlyZShcIi4uL21vZGVsL0dsb2JhbFwiKTtcblxudmFyIEV4dGVuc2lvbkFQSSA9IHJlcXVpcmUoXCIuLi9FeHRlbnNpb25BUElcIik7XG5cbnZhciBfbW9kZWwgPSByZXF1aXJlKFwiLi4vdXRpbC9tb2RlbFwiKTtcblxudmFyIG5vcm1hbGl6ZVRvQXJyYXkgPSBfbW9kZWwubm9ybWFsaXplVG9BcnJheTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKipcbiAqIEBtb2R1bGUgZWNoYXJ0cy9zdHJlYW0vU2NoZWR1bGVyXG4gKi9cblxuLyoqXG4gKiBAY29uc3RydWN0b3JcbiAqL1xuZnVuY3Rpb24gU2NoZWR1bGVyKGVjSW5zdGFuY2UsIGFwaSwgZGF0YVByb2Nlc3NvckhhbmRsZXJzLCB2aXN1YWxIYW5kbGVycykge1xuICB0aGlzLmVjSW5zdGFuY2UgPSBlY0luc3RhbmNlO1xuICB0aGlzLmFwaSA9IGFwaTtcbiAgdGhpcy51bmZpbmlzaGVkOyAvLyBGaXggY3VycmVudCBwcm9jZXNzb3JzIGluIGNhc2UgdGhhdCBpbiBzb21lIHJlYXIgY2FzZXMgdGhhdFxuICAvLyBwcm9jZXNzb3JzIG1pZ2h0IGJlIHJlZ2lzdGVyZWQgYWZ0ZXIgZWNoYXJ0cyBpbnN0YW5jZSBjcmVhdGVkLlxuICAvLyBSZWdpc3RlciBwcm9jZXNzb3JzIGluY3JlbWVudGFsbHkgZm9yIGEgZWNoYXJ0cyBpbnN0YW5jZSBpc1xuICAvLyBub3Qgc3VwcG9ydGVkIGJ5IHRoaXMgc3RyZWFtIGFyY2hpdGVjdHVyZS5cblxuICB2YXIgZGF0YVByb2Nlc3NvckhhbmRsZXJzID0gdGhpcy5fZGF0YVByb2Nlc3NvckhhbmRsZXJzID0gZGF0YVByb2Nlc3NvckhhbmRsZXJzLnNsaWNlKCk7XG4gIHZhciB2aXN1YWxIYW5kbGVycyA9IHRoaXMuX3Zpc3VhbEhhbmRsZXJzID0gdmlzdWFsSGFuZGxlcnMuc2xpY2UoKTtcbiAgdGhpcy5fYWxsSGFuZGxlcnMgPSBkYXRhUHJvY2Vzc29ySGFuZGxlcnMuY29uY2F0KHZpc3VhbEhhbmRsZXJzKTtcbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqIEB0eXBlIHtcbiAgICogICAgIFtoYW5kbGVyVUlEOiBzdHJpbmddOiB7XG4gICAqICAgICAgICAgc2VyaWVzVGFza01hcD86IHtcbiAgICogICAgICAgICAgICAgW3Nlcmllc1VJRDogc3RyaW5nXTogVGFza1xuICAgKiAgICAgICAgIH0sXG4gICAqICAgICAgICAgb3ZlcmFsbFRhc2s/OiBUYXNrXG4gICAqICAgICB9XG4gICAqIH1cbiAgICovXG5cbiAgdGhpcy5fc3RhZ2VUYXNrTWFwID0gY3JlYXRlSGFzaE1hcCgpO1xufVxuXG52YXIgcHJvdG8gPSBTY2hlZHVsZXIucHJvdG90eXBlO1xuLyoqXG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL21vZGVsL0dsb2JhbH0gZWNNb2RlbFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqL1xuXG5wcm90by5yZXN0b3JlRGF0YSA9IGZ1bmN0aW9uIChlY01vZGVsLCBwYXlsb2FkKSB7XG4gIC8vIFRPRE86IE9ubHkgcmVzdHJvZSBuZWVkZWQgc2VyaWVzIGFuZCBjb21wb25lbnRzLCBidXQgbm90IGFsbCBjb21wb25lbnRzLlxuICAvLyBDdXJyZW50bHkgYHJlc3RvcmVEYXRhYCBvZiBhbGwgb2YgdGhlIHNlcmllcyBhbmQgY29tcG9uZW50IHdpbGwgYmUgY2FsbGVkLlxuICAvLyBCdXQgc29tZSBpbmRlcGVuZGVudCBjb21wb25lbnRzIGxpa2UgYHRpdGxlYCwgYGxlZ2VuZGAsIGBncmFwaGljYCwgYHRvb2xib3hgLFxuICAvLyBgdG9vbHRpcGAsIGBheGlzUG9pbnRlcmAsIGV0YywgZG8gbm90IG5lZWQgc2VyaWVzIHJlZnJlc2ggd2hlbiBgc2V0T3B0aW9uYCxcbiAgLy8gYW5kIHNvbWUgY29tcG9uZW50cyBsaWtlIGNvb3JkaW5hdGUgc3lzdGVtLCBheGVzLCBkYXRhWm9vbSwgdmlzdWFsTWFwIG9ubHlcbiAgLy8gbmVlZCB0aGVpciB0YXJnZXQgc2VyaWVzIHJlZnJlc2guXG4gIC8vICgxKSBJZiB3ZSBhcmUgaW1wbGVtZW50aW5nIHRoaXMgZmVhdHVyZSBzb21lIGRheSwgd2Ugc2hvdWxkIGNvbnNpZGVyIHRoZXNlIGNhc2VzOlxuICAvLyBpZiBhIGRhdGEgcHJvY2Vzc29yIGRlcGVuZHMgb24gYSBjb21wb25lbnQgKGUuZy4sIGRhdGFab29tUHJvY2Vzc29yIGRlcGVuZHNcbiAgLy8gb24gdGhlIHNldHRpbmdzIG9mIGBkYXRhWm9vbWApLCBpdCBzaG91bGQgYmUgcmUtcGVyZm9ybWVkIGlmIHRoZSBjb21wb25lbnRcbiAgLy8gaXMgbW9kaWZpZWQgYnkgYHNldE9wdGlvbmAuXG4gIC8vICgyKSBJZiBhIHByb2Nlc3NvciBkZXBlbmRzIG9uIHNldnJhbCBzZXJpZXMsIHNwZWljaWZpZWQgYnkgaXRzIGBnZXRUYXJnZXRTZXJpZXNgLFxuICAvLyBpdCBzaG91bGQgYmUgcmUtcGVyZm9ybWVkIHdoZW4gdGhlIHJlc3VsdCBhcnJheSBvZiBgZ2V0VGFyZ2V0U2VyaWVzYCBjaGFuZ2VkLlxuICAvLyBXZSB1c2UgYGRlcGVuZGVuY2llc2AgdG8gY292ZXIgdGhlc2UgaXNzdWVzLlxuICAvLyAoMykgSG93IHRvIHVwZGF0ZSB0YXJnZXQgc2VyaWVzIHdoZW4gY29vcmRpbmF0ZSBzeXN0ZW0gcmVsYXRlZCBjb21wb25lbnRzIG1vZGlmaWVkLlxuICAvLyBUT0RPOiBzaW1wbHkgdGhlIGRpcnR5IG1lY2hhbmlzbT8gQ2hlY2sgd2hldGhlciBvbmx5IHRoZSBjYXNlIGhlcmUgY2FuIHNldCB0YXNrcyBkaXJ0eSxcbiAgLy8gYW5kIHRoaXMgY2FzZSBhbGwgb2YgdGhlIHRhc2tzIHdpbGwgYmUgc2V0IGFzIGRpcnR5LlxuICBlY01vZGVsLnJlc3RvcmVEYXRhKHBheWxvYWQpOyAvLyBUaGVvcmV0aWNhbGx5IGFuIG92ZXJhbGwgdGFzayBub3Qgb25seSBkZXBlbmRzIG9uIGVhY2ggb2YgaXRzIHRhcmdldCBzZXJpZXMsIGJ1dCBhbHNvXG4gIC8vIGRlcGVuZHMgb24gYWxsIG9mIHRoZSBzZXJpZXMuXG4gIC8vIFRoZSBvdmVyYWxsIHRhc2sgaXMgbm90IGluIHBpcGVsaW5lLCBhbmQgYGVjTW9kZWwucmVzdG9yZURhdGFgIG9ubHkgc2V0IHBpcGVsaW5lIHRhc2tzXG4gIC8vIGRpcnR5LiBJZiBgZ2V0VGFyZ2V0U2VyaWVzYCBvZiBhbiBvdmVyYWxsIHRhc2sgcmV0dXJucyBub3RoaW5nLCB3ZSBzaG91bGQgYWxzbyBlbnN1cmVcbiAgLy8gdGhhdCB0aGUgb3ZlcmFsbCB0YXNrIGlzIHNldCBhcyBkaXJ0eSBhbmQgdG8gYmUgcGVyZm9ybWVkLCBvdGhlcndpc2UgaXQgcHJvYmFibHkgY2F1c2VcbiAgLy8gc3RhdGUgY2hhb3MuIFNvIHdlIGhhdmUgdG8gc2V0IGRpcnR5IG9mIGFsbCBvZiB0aGUgb3ZlcmFsbCB0YXNrcyBtYW51YWxseSwgb3RoZXJ3aXNlIGl0XG4gIC8vIHByb2JhYmx5IGNhdXNlIHN0YXRlIGNoYW9zIChjb25zaWRlciBgZGF0YVpvb21Qcm9jZXNzb3JgKS5cblxuICB0aGlzLl9zdGFnZVRhc2tNYXAuZWFjaChmdW5jdGlvbiAodGFza1JlY29yZCkge1xuICAgIHZhciBvdmVyYWxsVGFzayA9IHRhc2tSZWNvcmQub3ZlcmFsbFRhc2s7XG4gICAgb3ZlcmFsbFRhc2sgJiYgb3ZlcmFsbFRhc2suZGlydHkoKTtcbiAgfSk7XG59OyAvLyBJZiBzZXJpZXNNb2RlbCBwcm92aWRlZCwgaW5jcmVtZW50YWwgdGhyZXNob2xkIGlzIGNoZWNrIGJ5IHNlcmllcyBkYXRhLlxuXG5cbnByb3RvLmdldFBlcmZvcm1BcmdzID0gZnVuY3Rpb24gKHRhc2ssIGlzQmxvY2spIHtcbiAgLy8gRm9yIG92ZXJhbGwgdGFza1xuICBpZiAoIXRhc2suX19waXBlbGluZSkge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBwaXBlbGluZSA9IHRoaXMuX3BpcGVsaW5lTWFwLmdldCh0YXNrLl9fcGlwZWxpbmUuaWQpO1xuXG4gIHZhciBwQ3R4ID0gcGlwZWxpbmUuY29udGV4dDtcbiAgdmFyIGluY3JlbWVudGFsID0gIWlzQmxvY2sgJiYgcGlwZWxpbmUucHJvZ3Jlc3NpdmVFbmFibGVkICYmICghcEN0eCB8fCBwQ3R4LnByb2dyZXNzaXZlUmVuZGVyKSAmJiB0YXNrLl9faWR4SW5QaXBlbGluZSA+IHBpcGVsaW5lLmJsb2NrSW5kZXg7XG4gIHZhciBzdGVwID0gaW5jcmVtZW50YWwgPyBwaXBlbGluZS5zdGVwIDogbnVsbDtcbiAgdmFyIG1vZERhdGFDb3VudCA9IHBDdHggJiYgcEN0eC5tb2REYXRhQ291bnQ7XG4gIHZhciBtb2RCeSA9IG1vZERhdGFDb3VudCAhPSBudWxsID8gTWF0aC5jZWlsKG1vZERhdGFDb3VudCAvIHN0ZXApIDogbnVsbDtcbiAgcmV0dXJuIHtcbiAgICBzdGVwOiBzdGVwLFxuICAgIG1vZEJ5OiBtb2RCeSxcbiAgICBtb2REYXRhQ291bnQ6IG1vZERhdGFDb3VudFxuICB9O1xufTtcblxucHJvdG8uZ2V0UGlwZWxpbmUgPSBmdW5jdGlvbiAocGlwZWxpbmVJZCkge1xuICByZXR1cm4gdGhpcy5fcGlwZWxpbmVNYXAuZ2V0KHBpcGVsaW5lSWQpO1xufTtcbi8qKlxuICogQ3VycmVudCwgcHJvZ3Jlc3NpdmUgcmVuZGVyaW5nIHN0YXJ0cyBmcm9tIHZpc3VhbCBhbmQgbGF5b3V0LlxuICogQWx3YXlzIGRldGVjdCByZW5kZXIgbW9kZSBpbiB0aGUgc2FtZSBzdGFnZSwgYXZvaWRpbmcgdGhhdCBpbmNvcnJlY3RcbiAqIGRldGVjdGlvbiBjYXVzZWQgYnkgZGF0YSBmaWx0ZXJpbmcuXG4gKiBDYXV0aW9uOlxuICogYHVwZGF0ZVN0cmVhbU1vZGVzYCB1c2UgYHNlcmllc01vZGVsLmdldERhdGEoKWAuXG4gKi9cblxuXG5wcm90by51cGRhdGVTdHJlYW1Nb2RlcyA9IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCwgdmlldykge1xuICB2YXIgcGlwZWxpbmUgPSB0aGlzLl9waXBlbGluZU1hcC5nZXQoc2VyaWVzTW9kZWwudWlkKTtcblxuICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgdmFyIGRhdGFMZW4gPSBkYXRhLmNvdW50KCk7IC8vIGBwcm9ncmVzc2l2ZVJlbmRlcmAgbWVhbnMgdGhhdCBjYW4gcmVuZGVyIHByb2dyZXNzaXZlbHkgaW4gZWFjaFxuICAvLyBhbmltYXRpb24gZnJhbWUuIE5vdGUgdGhhdCBzb21lIHR5cGVzIG9mIHNlcmllcyBkbyBub3QgcHJvdmlkZVxuICAvLyBgdmlldy5pbmNyZW1lbnRhbFByZXBhcmVSZW5kZXJgIGJ1dCBzdXBwb3J0IGBjaGFydC5hcHBlbmREYXRhYC4gV2VcbiAgLy8gdXNlIHRoZSB0ZXJtIGBpbmNyZW1lbnRhbGAgYnV0IG5vdCBgcHJvZ3Jlc3NpdmVgIHRvIGRlc2NyaWJlIHRoZVxuICAvLyBjYXNlIHRoYXQgYGNoYXJ0LmFwcGVuZERhdGFgLlxuXG4gIHZhciBwcm9ncmVzc2l2ZVJlbmRlciA9IHBpcGVsaW5lLnByb2dyZXNzaXZlRW5hYmxlZCAmJiB2aWV3LmluY3JlbWVudGFsUHJlcGFyZVJlbmRlciAmJiBkYXRhTGVuID49IHBpcGVsaW5lLnRocmVzaG9sZDtcbiAgdmFyIGxhcmdlID0gc2VyaWVzTW9kZWwuZ2V0KCdsYXJnZScpICYmIGRhdGFMZW4gPj0gc2VyaWVzTW9kZWwuZ2V0KCdsYXJnZVRocmVzaG9sZCcpOyAvLyBUT0RPOiBtb2REYXRhQ291bnQgc2hvdWxkIG5vdCB1cGRhdGVkIGlmIGBhcHBlbmREYXRhYCwgb3RoZXJ3aXNlIGNhdXNlIHdob2xlIHJlcGFpbnQuXG4gIC8vIHNlZSBgdGVzdC9jYW5kbGVzdGljay1sYXJnZTMuaHRtbGBcblxuICB2YXIgbW9kRGF0YUNvdW50ID0gc2VyaWVzTW9kZWwuZ2V0KCdwcm9ncmVzc2l2ZUNodW5rTW9kZScpID09PSAnbW9kJyA/IGRhdGFMZW4gOiBudWxsO1xuICBzZXJpZXNNb2RlbC5waXBlbGluZUNvbnRleHQgPSBwaXBlbGluZS5jb250ZXh0ID0ge1xuICAgIHByb2dyZXNzaXZlUmVuZGVyOiBwcm9ncmVzc2l2ZVJlbmRlcixcbiAgICBtb2REYXRhQ291bnQ6IG1vZERhdGFDb3VudCxcbiAgICBsYXJnZTogbGFyZ2VcbiAgfTtcbn07XG5cbnByb3RvLnJlc3RvcmVQaXBlbGluZXMgPSBmdW5jdGlvbiAoZWNNb2RlbCkge1xuICB2YXIgc2NoZWR1bGVyID0gdGhpcztcbiAgdmFyIHBpcGVsaW5lTWFwID0gc2NoZWR1bGVyLl9waXBlbGluZU1hcCA9IGNyZWF0ZUhhc2hNYXAoKTtcbiAgZWNNb2RlbC5lYWNoU2VyaWVzKGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIHZhciBwcm9ncmVzc2l2ZSA9IHNlcmllc01vZGVsLmdldFByb2dyZXNzaXZlKCk7XG4gICAgdmFyIHBpcGVsaW5lSWQgPSBzZXJpZXNNb2RlbC51aWQ7XG4gICAgcGlwZWxpbmVNYXAuc2V0KHBpcGVsaW5lSWQsIHtcbiAgICAgIGlkOiBwaXBlbGluZUlkLFxuICAgICAgaGVhZDogbnVsbCxcbiAgICAgIHRhaWw6IG51bGwsXG4gICAgICB0aHJlc2hvbGQ6IHNlcmllc01vZGVsLmdldFByb2dyZXNzaXZlVGhyZXNob2xkKCksXG4gICAgICBwcm9ncmVzc2l2ZUVuYWJsZWQ6IHByb2dyZXNzaXZlICYmICEoc2VyaWVzTW9kZWwucHJldmVudEluY3JlbWVudGFsICYmIHNlcmllc01vZGVsLnByZXZlbnRJbmNyZW1lbnRhbCgpKSxcbiAgICAgIGJsb2NrSW5kZXg6IC0xLFxuICAgICAgc3RlcDogTWF0aC5yb3VuZChwcm9ncmVzc2l2ZSB8fCA3MDApLFxuICAgICAgY291bnQ6IDBcbiAgICB9KTtcbiAgICBwaXBlKHNjaGVkdWxlciwgc2VyaWVzTW9kZWwsIHNlcmllc01vZGVsLmRhdGFUYXNrKTtcbiAgfSk7XG59O1xuXG5wcm90by5wcmVwYXJlU3RhZ2VUYXNrcyA9IGZ1bmN0aW9uICgpIHtcbiAgdmFyIHN0YWdlVGFza01hcCA9IHRoaXMuX3N0YWdlVGFza01hcDtcbiAgdmFyIGVjTW9kZWwgPSB0aGlzLmVjSW5zdGFuY2UuZ2V0TW9kZWwoKTtcbiAgdmFyIGFwaSA9IHRoaXMuYXBpO1xuICBlYWNoKHRoaXMuX2FsbEhhbmRsZXJzLCBmdW5jdGlvbiAoaGFuZGxlcikge1xuICAgIHZhciByZWNvcmQgPSBzdGFnZVRhc2tNYXAuZ2V0KGhhbmRsZXIudWlkKSB8fCBzdGFnZVRhc2tNYXAuc2V0KGhhbmRsZXIudWlkLCBbXSk7XG4gICAgaGFuZGxlci5yZXNldCAmJiBjcmVhdGVTZXJpZXNTdGFnZVRhc2sodGhpcywgaGFuZGxlciwgcmVjb3JkLCBlY01vZGVsLCBhcGkpO1xuICAgIGhhbmRsZXIub3ZlcmFsbFJlc2V0ICYmIGNyZWF0ZU92ZXJhbGxTdGFnZVRhc2sodGhpcywgaGFuZGxlciwgcmVjb3JkLCBlY01vZGVsLCBhcGkpO1xuICB9LCB0aGlzKTtcbn07XG5cbnByb3RvLnByZXBhcmVWaWV3ID0gZnVuY3Rpb24gKHZpZXcsIG1vZGVsLCBlY01vZGVsLCBhcGkpIHtcbiAgdmFyIHJlbmRlclRhc2sgPSB2aWV3LnJlbmRlclRhc2s7XG4gIHZhciBjb250ZXh0ID0gcmVuZGVyVGFzay5jb250ZXh0O1xuICBjb250ZXh0Lm1vZGVsID0gbW9kZWw7XG4gIGNvbnRleHQuZWNNb2RlbCA9IGVjTW9kZWw7XG4gIGNvbnRleHQuYXBpID0gYXBpO1xuICByZW5kZXJUYXNrLl9fYmxvY2sgPSAhdmlldy5pbmNyZW1lbnRhbFByZXBhcmVSZW5kZXI7XG4gIHBpcGUodGhpcywgbW9kZWwsIHJlbmRlclRhc2spO1xufTtcblxucHJvdG8ucGVyZm9ybURhdGFQcm9jZXNzb3JUYXNrcyA9IGZ1bmN0aW9uIChlY01vZGVsLCBwYXlsb2FkKSB7XG4gIC8vIElmIHdlIGRvIG5vdCB1c2UgYGJsb2NrYCBoZXJlLCBpdCBzaG91bGQgYmUgY29uc2lkZXJlZCB3aGVuIHRvIHVwZGF0ZSBtb2Rlcy5cbiAgcGVyZm9ybVN0YWdlVGFza3ModGhpcywgdGhpcy5fZGF0YVByb2Nlc3NvckhhbmRsZXJzLCBlY01vZGVsLCBwYXlsb2FkLCB7XG4gICAgYmxvY2s6IHRydWVcbiAgfSk7XG59OyAvLyBvcHRcbi8vIG9wdC52aXN1YWxUeXBlOiAndmlzdWFsJyBvciAnbGF5b3V0J1xuLy8gb3B0LnNldERpcnR5XG5cblxucHJvdG8ucGVyZm9ybVZpc3VhbFRhc2tzID0gZnVuY3Rpb24gKGVjTW9kZWwsIHBheWxvYWQsIG9wdCkge1xuICBwZXJmb3JtU3RhZ2VUYXNrcyh0aGlzLCB0aGlzLl92aXN1YWxIYW5kbGVycywgZWNNb2RlbCwgcGF5bG9hZCwgb3B0KTtcbn07XG5cbmZ1bmN0aW9uIHBlcmZvcm1TdGFnZVRhc2tzKHNjaGVkdWxlciwgc3RhZ2VIYW5kbGVycywgZWNNb2RlbCwgcGF5bG9hZCwgb3B0KSB7XG4gIG9wdCA9IG9wdCB8fCB7fTtcbiAgdmFyIHVuZmluaXNoZWQ7XG4gIGVhY2goc3RhZ2VIYW5kbGVycywgZnVuY3Rpb24gKHN0YWdlSGFuZGxlciwgaWR4KSB7XG4gICAgaWYgKG9wdC52aXN1YWxUeXBlICYmIG9wdC52aXN1YWxUeXBlICE9PSBzdGFnZUhhbmRsZXIudmlzdWFsVHlwZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBzdGFnZUhhbmRsZXJSZWNvcmQgPSBzY2hlZHVsZXIuX3N0YWdlVGFza01hcC5nZXQoc3RhZ2VIYW5kbGVyLnVpZCk7XG5cbiAgICB2YXIgc2VyaWVzVGFza01hcCA9IHN0YWdlSGFuZGxlclJlY29yZC5zZXJpZXNUYXNrTWFwO1xuICAgIHZhciBvdmVyYWxsVGFzayA9IHN0YWdlSGFuZGxlclJlY29yZC5vdmVyYWxsVGFzaztcblxuICAgIGlmIChvdmVyYWxsVGFzaykge1xuICAgICAgdmFyIG92ZXJhbGxOZWVkRGlydHk7XG4gICAgICB2YXIgYWdlbnRTdHViTWFwID0gb3ZlcmFsbFRhc2suYWdlbnRTdHViTWFwO1xuICAgICAgYWdlbnRTdHViTWFwLmVhY2goZnVuY3Rpb24gKHN0dWIpIHtcbiAgICAgICAgaWYgKG5lZWRTZXREaXJ0eShvcHQsIHN0dWIpKSB7XG4gICAgICAgICAgc3R1Yi5kaXJ0eSgpO1xuICAgICAgICAgIG92ZXJhbGxOZWVkRGlydHkgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIG92ZXJhbGxOZWVkRGlydHkgJiYgb3ZlcmFsbFRhc2suZGlydHkoKTtcbiAgICAgIHVwZGF0ZVBheWxvYWQob3ZlcmFsbFRhc2ssIHBheWxvYWQpO1xuICAgICAgdmFyIHBlcmZvcm1BcmdzID0gc2NoZWR1bGVyLmdldFBlcmZvcm1BcmdzKG92ZXJhbGxUYXNrLCBvcHQuYmxvY2spOyAvLyBFeGVjdXRlIHN0dWJzIGZpcnN0bHksIHdoaWNoIG1heSBzZXQgdGhlIG92ZXJhbGwgdGFzayBkaXJ0eSxcbiAgICAgIC8vIHRoZW4gZXhlY3V0ZSB0aGUgb3ZlcmFsbCB0YXNrLiBBbmQgc3R1YiB3aWxsIGNhbGwgc2VyaWVzTW9kZWwuc2V0RGF0YSxcbiAgICAgIC8vIHdoaWNoIGVuc3VyZXMgdGhhdCBpbiB0aGUgb3ZlcmFsbFRhc2sgc2VyaWVzTW9kZWwuZ2V0RGF0YSgpIHdpbGwgbm90XG4gICAgICAvLyByZXR1cm4gaW5jb3JyZWN0IGRhdGEuXG5cbiAgICAgIGFnZW50U3R1Yk1hcC5lYWNoKGZ1bmN0aW9uIChzdHViKSB7XG4gICAgICAgIHN0dWIucGVyZm9ybShwZXJmb3JtQXJncyk7XG4gICAgICB9KTtcbiAgICAgIHVuZmluaXNoZWQgfD0gb3ZlcmFsbFRhc2sucGVyZm9ybShwZXJmb3JtQXJncyk7XG4gICAgfSBlbHNlIGlmIChzZXJpZXNUYXNrTWFwKSB7XG4gICAgICBzZXJpZXNUYXNrTWFwLmVhY2goZnVuY3Rpb24gKHRhc2ssIHBpcGVsaW5lSWQpIHtcbiAgICAgICAgaWYgKG5lZWRTZXREaXJ0eShvcHQsIHRhc2spKSB7XG4gICAgICAgICAgdGFzay5kaXJ0eSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHBlcmZvcm1BcmdzID0gc2NoZWR1bGVyLmdldFBlcmZvcm1BcmdzKHRhc2ssIG9wdC5ibG9jayk7XG4gICAgICAgIHBlcmZvcm1BcmdzLnNraXAgPSAhc3RhZ2VIYW5kbGVyLnBlcmZvcm1SYXdTZXJpZXMgJiYgZWNNb2RlbC5pc1Nlcmllc0ZpbHRlcmVkKHRhc2suY29udGV4dC5tb2RlbCk7XG4gICAgICAgIHVwZGF0ZVBheWxvYWQodGFzaywgcGF5bG9hZCk7XG4gICAgICAgIHVuZmluaXNoZWQgfD0gdGFzay5wZXJmb3JtKHBlcmZvcm1BcmdzKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG5cbiAgZnVuY3Rpb24gbmVlZFNldERpcnR5KG9wdCwgdGFzaykge1xuICAgIHJldHVybiBvcHQuc2V0RGlydHkgJiYgKCFvcHQuZGlydHlNYXAgfHwgb3B0LmRpcnR5TWFwLmdldCh0YXNrLl9fcGlwZWxpbmUuaWQpKTtcbiAgfVxuXG4gIHNjaGVkdWxlci51bmZpbmlzaGVkIHw9IHVuZmluaXNoZWQ7XG59XG5cbnByb3RvLnBlcmZvcm1TZXJpZXNUYXNrcyA9IGZ1bmN0aW9uIChlY01vZGVsKSB7XG4gIHZhciB1bmZpbmlzaGVkO1xuICBlY01vZGVsLmVhY2hTZXJpZXMoZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgLy8gUHJvZ3Jlc3MgdG8gdGhlIGVuZCBmb3IgZGF0YUluaXQgYW5kIGRhdGFSZXN0b3JlLlxuICAgIHVuZmluaXNoZWQgfD0gc2VyaWVzTW9kZWwuZGF0YVRhc2sucGVyZm9ybSgpO1xuICB9KTtcbiAgdGhpcy51bmZpbmlzaGVkIHw9IHVuZmluaXNoZWQ7XG59O1xuXG5wcm90by5wbGFuID0gZnVuY3Rpb24gKCkge1xuICAvLyBUcmF2ZWwgcGlwZWxpbmVzLCBjaGVjayBibG9jay5cbiAgdGhpcy5fcGlwZWxpbmVNYXAuZWFjaChmdW5jdGlvbiAocGlwZWxpbmUpIHtcbiAgICB2YXIgdGFzayA9IHBpcGVsaW5lLnRhaWw7XG5cbiAgICBkbyB7XG4gICAgICBpZiAodGFzay5fX2Jsb2NrKSB7XG4gICAgICAgIHBpcGVsaW5lLmJsb2NrSW5kZXggPSB0YXNrLl9faWR4SW5QaXBlbGluZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIHRhc2sgPSB0YXNrLmdldFVwc3RyZWFtKCk7XG4gICAgfSB3aGlsZSAodGFzayk7XG4gIH0pO1xufTtcblxudmFyIHVwZGF0ZVBheWxvYWQgPSBwcm90by51cGRhdGVQYXlsb2FkID0gZnVuY3Rpb24gKHRhc2ssIHBheWxvYWQpIHtcbiAgcGF5bG9hZCAhPT0gJ3JlbWFpbicgJiYgKHRhc2suY29udGV4dC5wYXlsb2FkID0gcGF5bG9hZCk7XG59O1xuXG5mdW5jdGlvbiBjcmVhdGVTZXJpZXNTdGFnZVRhc2soc2NoZWR1bGVyLCBzdGFnZUhhbmRsZXIsIHN0YWdlSGFuZGxlclJlY29yZCwgZWNNb2RlbCwgYXBpKSB7XG4gIHZhciBzZXJpZXNUYXNrTWFwID0gc3RhZ2VIYW5kbGVyUmVjb3JkLnNlcmllc1Rhc2tNYXAgfHwgKHN0YWdlSGFuZGxlclJlY29yZC5zZXJpZXNUYXNrTWFwID0gY3JlYXRlSGFzaE1hcCgpKTtcbiAgdmFyIHNlcmllc1R5cGUgPSBzdGFnZUhhbmRsZXIuc2VyaWVzVHlwZTtcbiAgdmFyIGdldFRhcmdldFNlcmllcyA9IHN0YWdlSGFuZGxlci5nZXRUYXJnZXRTZXJpZXM7IC8vIElmIGEgc3RhZ2VIYW5kbGVyIHNob3VsZCBjb3ZlciBhbGwgc2VyaWVzLCBgY3JlYXRlT25BbGxTZXJpZXNgIHNob3VsZCBiZSBkZWNsYXJlZCBtYW5kYXRvcmlseSxcbiAgLy8gdG8gYXZvaWQgc29tZSB0eXBvIG9yIGFidXNlLiBPdGhlcndpc2UgaWYgYW4gZXh0ZW5zaW9uIGRvIG5vdCBzcGVjaWZ5IGEgYHNlcmllc1R5cGVgLFxuICAvLyBpdCB3b3JrcyBidXQgaXQgbWF5IGNhdXNlIG90aGVyIGlycmVsZXZhbnQgY2hhcnRzIGJsb2NrZWQuXG5cbiAgaWYgKHN0YWdlSGFuZGxlci5jcmVhdGVPbkFsbFNlcmllcykge1xuICAgIGVjTW9kZWwuZWFjaFJhd1NlcmllcyhjcmVhdGUpO1xuICB9IGVsc2UgaWYgKHNlcmllc1R5cGUpIHtcbiAgICBlY01vZGVsLmVhY2hSYXdTZXJpZXNCeVR5cGUoc2VyaWVzVHlwZSwgY3JlYXRlKTtcbiAgfSBlbHNlIGlmIChnZXRUYXJnZXRTZXJpZXMpIHtcbiAgICBnZXRUYXJnZXRTZXJpZXMoZWNNb2RlbCwgYXBpKS5lYWNoKGNyZWF0ZSk7XG4gIH1cblxuICBmdW5jdGlvbiBjcmVhdGUoc2VyaWVzTW9kZWwpIHtcbiAgICB2YXIgcGlwZWxpbmVJZCA9IHNlcmllc01vZGVsLnVpZDsgLy8gSW5pdCB0YXNrcyBmb3IgZWFjaCBzZXJpZXNNb2RlbCBvbmx5IG9uY2UuXG4gICAgLy8gUmV1c2Ugb3JpZ2luYWwgdGFzayBpbnN0YW5jZS5cblxuICAgIHZhciB0YXNrID0gc2VyaWVzVGFza01hcC5nZXQocGlwZWxpbmVJZCkgfHwgc2VyaWVzVGFza01hcC5zZXQocGlwZWxpbmVJZCwgY3JlYXRlVGFzayh7XG4gICAgICBwbGFuOiBzZXJpZXNUYXNrUGxhbixcbiAgICAgIHJlc2V0OiBzZXJpZXNUYXNrUmVzZXQsXG4gICAgICBjb3VudDogc2VyaWVzVGFza0NvdW50XG4gICAgfSkpO1xuICAgIHRhc2suY29udGV4dCA9IHtcbiAgICAgIG1vZGVsOiBzZXJpZXNNb2RlbCxcbiAgICAgIGVjTW9kZWw6IGVjTW9kZWwsXG4gICAgICBhcGk6IGFwaSxcbiAgICAgIHVzZUNsZWFyVmlzdWFsOiBzdGFnZUhhbmRsZXIuaXNWaXN1YWwgJiYgIXN0YWdlSGFuZGxlci5pc0xheW91dCxcbiAgICAgIHBsYW46IHN0YWdlSGFuZGxlci5wbGFuLFxuICAgICAgcmVzZXQ6IHN0YWdlSGFuZGxlci5yZXNldCxcbiAgICAgIHNjaGVkdWxlcjogc2NoZWR1bGVyXG4gICAgfTtcbiAgICBwaXBlKHNjaGVkdWxlciwgc2VyaWVzTW9kZWwsIHRhc2spO1xuICB9IC8vIENsZWFyIHVudXNlZCBzZXJpZXMgdGFza3MuXG5cblxuICB2YXIgcGlwZWxpbmVNYXAgPSBzY2hlZHVsZXIuX3BpcGVsaW5lTWFwO1xuICBzZXJpZXNUYXNrTWFwLmVhY2goZnVuY3Rpb24gKHRhc2ssIHBpcGVsaW5lSWQpIHtcbiAgICBpZiAoIXBpcGVsaW5lTWFwLmdldChwaXBlbGluZUlkKSkge1xuICAgICAgdGFzay5kaXNwb3NlKCk7XG4gICAgICBzZXJpZXNUYXNrTWFwLnJlbW92ZUtleShwaXBlbGluZUlkKTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBjcmVhdGVPdmVyYWxsU3RhZ2VUYXNrKHNjaGVkdWxlciwgc3RhZ2VIYW5kbGVyLCBzdGFnZUhhbmRsZXJSZWNvcmQsIGVjTW9kZWwsIGFwaSkge1xuICB2YXIgb3ZlcmFsbFRhc2sgPSBzdGFnZUhhbmRsZXJSZWNvcmQub3ZlcmFsbFRhc2sgPSBzdGFnZUhhbmRsZXJSZWNvcmQub3ZlcmFsbFRhc2sgLy8gRm9yIG92ZXJhbGwgdGFzaywgdGhlIGZ1bmN0aW9uIG9ubHkgYmUgY2FsbGVkIG9uIHJlc2V0IHN0YWdlLlxuICB8fCBjcmVhdGVUYXNrKHtcbiAgICByZXNldDogb3ZlcmFsbFRhc2tSZXNldFxuICB9KTtcbiAgb3ZlcmFsbFRhc2suY29udGV4dCA9IHtcbiAgICBlY01vZGVsOiBlY01vZGVsLFxuICAgIGFwaTogYXBpLFxuICAgIG92ZXJhbGxSZXNldDogc3RhZ2VIYW5kbGVyLm92ZXJhbGxSZXNldCxcbiAgICBzY2hlZHVsZXI6IHNjaGVkdWxlclxuICB9OyAvLyBSZXVzZSBvcmlnbmFsIHN0dWJzLlxuXG4gIHZhciBhZ2VudFN0dWJNYXAgPSBvdmVyYWxsVGFzay5hZ2VudFN0dWJNYXAgPSBvdmVyYWxsVGFzay5hZ2VudFN0dWJNYXAgfHwgY3JlYXRlSGFzaE1hcCgpO1xuICB2YXIgc2VyaWVzVHlwZSA9IHN0YWdlSGFuZGxlci5zZXJpZXNUeXBlO1xuICB2YXIgZ2V0VGFyZ2V0U2VyaWVzID0gc3RhZ2VIYW5kbGVyLmdldFRhcmdldFNlcmllcztcbiAgdmFyIG92ZXJhbGxQcm9ncmVzcyA9IHRydWU7XG4gIHZhciBtb2RpZnlPdXRwdXRFbmQgPSBzdGFnZUhhbmRsZXIubW9kaWZ5T3V0cHV0RW5kOyAvLyBBbiBvdmVyYWxsIHRhc2sgd2l0aCBzZXJpZXNUeXBlIGRldGVjdGVkIG9yIGhhcyBgZ2V0VGFyZ2V0U2VyaWVzYCwgd2UgYWRkXG4gIC8vIHN0dWIgaW4gZWFjaCBwaXBlbGluZXMsIGl0IHdpbGwgc2V0IHRoZSBvdmVyYWxsIHRhc2sgZGlydHkgd2hlbiB0aGUgcGlwZWxpbmVcbiAgLy8gcHJvZ3Jlc3MuIE1vcmVvdmVyLCB0byBhdm9pZCBjYWxsIHRoZSBvdmVyYWxsIHRhc2sgZWFjaCBmcmFtZSAodG9vIGZyZXF1ZW50KSxcbiAgLy8gd2Ugc2V0IHRoZSBwaXBlbGluZSBibG9jay5cblxuICBpZiAoc2VyaWVzVHlwZSkge1xuICAgIGVjTW9kZWwuZWFjaFJhd1Nlcmllc0J5VHlwZShzZXJpZXNUeXBlLCBjcmVhdGVTdHViKTtcbiAgfSBlbHNlIGlmIChnZXRUYXJnZXRTZXJpZXMpIHtcbiAgICBnZXRUYXJnZXRTZXJpZXMoZWNNb2RlbCwgYXBpKS5lYWNoKGNyZWF0ZVN0dWIpO1xuICB9IC8vIE90aGVyd2lzZSwgKHVzdWFsbHkgaXQgaXMgbGVnYW5jeSBjYXNlKSwgdGhlIG92ZXJhbGwgdGFzayB3aWxsIG9ubHkgYmVcbiAgLy8gZXhlY3V0ZWQgd2hlbiB1cHN0cmVhbSBkaXJ0eS4gT3RoZXJ3aXNlIHRoZSBwcm9ncmVzc2l2ZSByZW5kZXJpbmcgb2YgYWxsXG4gIC8vIHBpcGVsaW5lcyB3aWxsIGJlIGRpc2FibGVkIHVuZXhwZWN0ZWRseS4gQnV0IGl0IHN0aWxsIG5lZWRzIHN0dWJzIHRvIHJlY2VpdmVcbiAgLy8gZGlydHkgaW5mbyBmcm9tIHVwc3RlYW0uXG4gIGVsc2Uge1xuICAgICAgb3ZlcmFsbFByb2dyZXNzID0gZmFsc2U7XG4gICAgICBlYWNoKGVjTW9kZWwuZ2V0U2VyaWVzKCksIGNyZWF0ZVN0dWIpO1xuICAgIH1cblxuICBmdW5jdGlvbiBjcmVhdGVTdHViKHNlcmllc01vZGVsKSB7XG4gICAgdmFyIHBpcGVsaW5lSWQgPSBzZXJpZXNNb2RlbC51aWQ7XG4gICAgdmFyIHN0dWIgPSBhZ2VudFN0dWJNYXAuZ2V0KHBpcGVsaW5lSWQpO1xuXG4gICAgaWYgKCFzdHViKSB7XG4gICAgICBzdHViID0gYWdlbnRTdHViTWFwLnNldChwaXBlbGluZUlkLCBjcmVhdGVUYXNrKHtcbiAgICAgICAgcmVzZXQ6IHN0dWJSZXNldCxcbiAgICAgICAgb25EaXJ0eTogc3R1Yk9uRGlydHlcbiAgICAgIH0pKTsgLy8gV2hlbiB0aGUgcmVzdWx0IG9mIGBnZXRUYXJnZXRTZXJpZXNgIGNoYW5nZWQsIHRoZSBvdmVyYWxsVGFza1xuICAgICAgLy8gc2hvdWxkIGJlIHNldCBhcyBkaXJ0eSBhbmQgcmUtcGVyZm9ybWVkLlxuXG4gICAgICBvdmVyYWxsVGFzay5kaXJ0eSgpO1xuICAgIH1cblxuICAgIHN0dWIuY29udGV4dCA9IHtcbiAgICAgIG1vZGVsOiBzZXJpZXNNb2RlbCxcbiAgICAgIG92ZXJhbGxQcm9ncmVzczogb3ZlcmFsbFByb2dyZXNzLFxuICAgICAgbW9kaWZ5T3V0cHV0RW5kOiBtb2RpZnlPdXRwdXRFbmRcbiAgICB9O1xuICAgIHN0dWIuYWdlbnQgPSBvdmVyYWxsVGFzaztcbiAgICBzdHViLl9fYmxvY2sgPSBvdmVyYWxsUHJvZ3Jlc3M7XG4gICAgcGlwZShzY2hlZHVsZXIsIHNlcmllc01vZGVsLCBzdHViKTtcbiAgfSAvLyBDbGVhciB1bnVzZWQgc3R1YnMuXG5cblxuICB2YXIgcGlwZWxpbmVNYXAgPSBzY2hlZHVsZXIuX3BpcGVsaW5lTWFwO1xuICBhZ2VudFN0dWJNYXAuZWFjaChmdW5jdGlvbiAoc3R1YiwgcGlwZWxpbmVJZCkge1xuICAgIGlmICghcGlwZWxpbmVNYXAuZ2V0KHBpcGVsaW5lSWQpKSB7XG4gICAgICBzdHViLmRpc3Bvc2UoKTsgLy8gV2hlbiB0aGUgcmVzdWx0IG9mIGBnZXRUYXJnZXRTZXJpZXNgIGNoYW5nZWQsIHRoZSBvdmVyYWxsVGFza1xuICAgICAgLy8gc2hvdWxkIGJlIHNldCBhcyBkaXJ0eSBhbmQgcmUtcGVyZm9ybWVkLlxuXG4gICAgICBvdmVyYWxsVGFzay5kaXJ0eSgpO1xuICAgICAgYWdlbnRTdHViTWFwLnJlbW92ZUtleShwaXBlbGluZUlkKTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBvdmVyYWxsVGFza1Jlc2V0KGNvbnRleHQpIHtcbiAgY29udGV4dC5vdmVyYWxsUmVzZXQoY29udGV4dC5lY01vZGVsLCBjb250ZXh0LmFwaSwgY29udGV4dC5wYXlsb2FkKTtcbn1cblxuZnVuY3Rpb24gc3R1YlJlc2V0KGNvbnRleHQsIHVwc3RyZWFtQ29udGV4dCkge1xuICByZXR1cm4gY29udGV4dC5vdmVyYWxsUHJvZ3Jlc3MgJiYgc3R1YlByb2dyZXNzO1xufVxuXG5mdW5jdGlvbiBzdHViUHJvZ3Jlc3MoKSB7XG4gIHRoaXMuYWdlbnQuZGlydHkoKTtcbiAgdGhpcy5nZXREb3duc3RyZWFtKCkuZGlydHkoKTtcbn1cblxuZnVuY3Rpb24gc3R1Yk9uRGlydHkoKSB7XG4gIHRoaXMuYWdlbnQgJiYgdGhpcy5hZ2VudC5kaXJ0eSgpO1xufVxuXG5mdW5jdGlvbiBzZXJpZXNUYXNrUGxhbihjb250ZXh0KSB7XG4gIHJldHVybiBjb250ZXh0LnBsYW4gJiYgY29udGV4dC5wbGFuKGNvbnRleHQubW9kZWwsIGNvbnRleHQuZWNNb2RlbCwgY29udGV4dC5hcGksIGNvbnRleHQucGF5bG9hZCk7XG59XG5cbmZ1bmN0aW9uIHNlcmllc1Rhc2tSZXNldChjb250ZXh0KSB7XG4gIGlmIChjb250ZXh0LnVzZUNsZWFyVmlzdWFsKSB7XG4gICAgY29udGV4dC5kYXRhLmNsZWFyQWxsVmlzdWFsKCk7XG4gIH1cblxuICB2YXIgcmVzZXREZWZpbmVzID0gY29udGV4dC5yZXNldERlZmluZXMgPSBub3JtYWxpemVUb0FycmF5KGNvbnRleHQucmVzZXQoY29udGV4dC5tb2RlbCwgY29udGV4dC5lY01vZGVsLCBjb250ZXh0LmFwaSwgY29udGV4dC5wYXlsb2FkKSk7XG4gIHJldHVybiByZXNldERlZmluZXMubGVuZ3RoID4gMSA/IG1hcChyZXNldERlZmluZXMsIGZ1bmN0aW9uICh2LCBpZHgpIHtcbiAgICByZXR1cm4gbWFrZVNlcmllc1Rhc2tQcm9ncmVzcyhpZHgpO1xuICB9KSA6IHNpbmdsZVNlcmllc1Rhc2tQcm9ncmVzcztcbn1cblxudmFyIHNpbmdsZVNlcmllc1Rhc2tQcm9ncmVzcyA9IG1ha2VTZXJpZXNUYXNrUHJvZ3Jlc3MoMCk7XG5cbmZ1bmN0aW9uIG1ha2VTZXJpZXNUYXNrUHJvZ3Jlc3MocmVzZXREZWZpbmVJZHgpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwYXJhbXMsIGNvbnRleHQpIHtcbiAgICB2YXIgZGF0YSA9IGNvbnRleHQuZGF0YTtcbiAgICB2YXIgcmVzZXREZWZpbmUgPSBjb250ZXh0LnJlc2V0RGVmaW5lc1tyZXNldERlZmluZUlkeF07XG5cbiAgICBpZiAocmVzZXREZWZpbmUgJiYgcmVzZXREZWZpbmUuZGF0YUVhY2gpIHtcbiAgICAgIGZvciAodmFyIGkgPSBwYXJhbXMuc3RhcnQ7IGkgPCBwYXJhbXMuZW5kOyBpKyspIHtcbiAgICAgICAgcmVzZXREZWZpbmUuZGF0YUVhY2goZGF0YSwgaSk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChyZXNldERlZmluZSAmJiByZXNldERlZmluZS5wcm9ncmVzcykge1xuICAgICAgcmVzZXREZWZpbmUucHJvZ3Jlc3MocGFyYW1zLCBkYXRhKTtcbiAgICB9XG4gIH07XG59XG5cbmZ1bmN0aW9uIHNlcmllc1Rhc2tDb3VudChjb250ZXh0KSB7XG4gIHJldHVybiBjb250ZXh0LmRhdGEuY291bnQoKTtcbn1cblxuZnVuY3Rpb24gcGlwZShzY2hlZHVsZXIsIHNlcmllc01vZGVsLCB0YXNrKSB7XG4gIHZhciBwaXBlbGluZUlkID0gc2VyaWVzTW9kZWwudWlkO1xuXG4gIHZhciBwaXBlbGluZSA9IHNjaGVkdWxlci5fcGlwZWxpbmVNYXAuZ2V0KHBpcGVsaW5lSWQpO1xuXG4gICFwaXBlbGluZS5oZWFkICYmIChwaXBlbGluZS5oZWFkID0gdGFzayk7XG4gIHBpcGVsaW5lLnRhaWwgJiYgcGlwZWxpbmUudGFpbC5waXBlKHRhc2spO1xuICBwaXBlbGluZS50YWlsID0gdGFzaztcbiAgdGFzay5fX2lkeEluUGlwZWxpbmUgPSBwaXBlbGluZS5jb3VudCsrO1xuICB0YXNrLl9fcGlwZWxpbmUgPSBwaXBlbGluZTtcbn1cblxuU2NoZWR1bGVyLndyYXBTdGFnZUhhbmRsZXIgPSBmdW5jdGlvbiAoc3RhZ2VIYW5kbGVyLCB2aXN1YWxUeXBlKSB7XG4gIGlmIChpc0Z1bmN0aW9uKHN0YWdlSGFuZGxlcikpIHtcbiAgICBzdGFnZUhhbmRsZXIgPSB7XG4gICAgICBvdmVyYWxsUmVzZXQ6IHN0YWdlSGFuZGxlcixcbiAgICAgIHNlcmllc1R5cGU6IGRldGVjdFNlcmlzZVR5cGUoc3RhZ2VIYW5kbGVyKVxuICAgIH07XG4gIH1cblxuICBzdGFnZUhhbmRsZXIudWlkID0gZ2V0VUlEKCdzdGFnZUhhbmRsZXInKTtcbiAgdmlzdWFsVHlwZSAmJiAoc3RhZ2VIYW5kbGVyLnZpc3VhbFR5cGUgPSB2aXN1YWxUeXBlKTtcbiAgcmV0dXJuIHN0YWdlSGFuZGxlcjtcbn07XG4vKipcbiAqIE9ubHkgc29tZSBsZWdhY3kgc3RhZ2UgaGFuZGxlcnMgKHVzdWFsbHkgaW4gZWNoYXJ0cyBleHRlbnNpb25zKSBhcmUgcHVyZSBmdW5jdGlvbi5cbiAqIFRvIGVuc3VyZSB0aGF0IHRoZXkgY2FuIHdvcmsgbm9ybWFsbHksIHRoZXkgc2hvdWxkIHdvcmsgaW4gYmxvY2sgbW9kZSwgdGhhdCBpcyxcbiAqIHRoZXkgc2hvdWxkIG5vdCBiZSBzdGFydGVkIHV0aWwgdGhlIHByZXZpb3VzIHRhc2tzIGZpbmlzaGVkLiBTbyB0aGV5IGNhdXNlIHRoZVxuICogcHJvZ3Jlc3NpdmUgcmVuZGVyaW5nIGRpc2FibGVkLiBXZSB0cnkgdG8gZGV0ZWN0IHRoZSBzZXJpZXMgdHlwZSwgdG8gbmFycm93IGRvd25cbiAqIHRoZSBibG9jayByYW5nZSB0byBvbmx5IHRoZSBzZXJpZXMgdHlwZSB0aGV5IGNvbmNlcm4sIGJ1dCBub3QgYWxsIHNlcmllcy5cbiAqL1xuXG5cbmZ1bmN0aW9uIGRldGVjdFNlcmlzZVR5cGUobGVnYWN5RnVuYykge1xuICBzZXJpZXNUeXBlID0gbnVsbDtcblxuICB0cnkge1xuICAgIC8vIEFzc3VtZSB0aGVyZSBpcyBubyBhc3luYyB3aGVuIGNhbGxpbmcgYGVhY2hTZXJpZXNCeVR5cGVgLlxuICAgIGxlZ2FjeUZ1bmMoZWNNb2RlbE1vY2ssIGFwaU1vY2spO1xuICB9IGNhdGNoIChlKSB7fVxuXG4gIHJldHVybiBzZXJpZXNUeXBlO1xufVxuXG52YXIgZWNNb2RlbE1vY2sgPSB7fTtcbnZhciBhcGlNb2NrID0ge307XG52YXIgc2VyaWVzVHlwZTtcbm1vY2tNZXRob2RzKGVjTW9kZWxNb2NrLCBHbG9iYWxNb2RlbCk7XG5tb2NrTWV0aG9kcyhhcGlNb2NrLCBFeHRlbnNpb25BUEkpO1xuXG5lY01vZGVsTW9jay5lYWNoU2VyaWVzQnlUeXBlID0gZWNNb2RlbE1vY2suZWFjaFJhd1Nlcmllc0J5VHlwZSA9IGZ1bmN0aW9uICh0eXBlKSB7XG4gIHNlcmllc1R5cGUgPSB0eXBlO1xufTtcblxuZWNNb2RlbE1vY2suZWFjaENvbXBvbmVudCA9IGZ1bmN0aW9uIChjb25kKSB7XG4gIGlmIChjb25kLm1haW5UeXBlID09PSAnc2VyaWVzJyAmJiBjb25kLnN1YlR5cGUpIHtcbiAgICBzZXJpZXNUeXBlID0gY29uZC5zdWJUeXBlO1xuICB9XG59O1xuXG5mdW5jdGlvbiBtb2NrTWV0aG9kcyh0YXJnZXQsIENseikge1xuICAvKiBlc2xpbnQtZGlzYWJsZSAqL1xuICBmb3IgKHZhciBuYW1lIGluIENsei5wcm90b3R5cGUpIHtcbiAgICAvLyBEbyBub3QgdXNlIGhhc093blByb3BlcnR5XG4gICAgdGFyZ2V0W25hbWVdID0gbm9vcDtcbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlICovXG5cbn1cblxudmFyIF9kZWZhdWx0ID0gU2NoZWR1bGVyO1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7O0FBSUE7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7O0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7OztBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/stream/Scheduler.js
