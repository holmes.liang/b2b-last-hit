/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var _number = __webpack_require__(/*! ../util/number */ "./node_modules/echarts/lib/util/number.js");

var parsePercent = _number.parsePercent;

var _dataStackHelper = __webpack_require__(/*! ../data/helper/dataStackHelper */ "./node_modules/echarts/lib/data/helper/dataStackHelper.js");

var isDimensionStacked = _dataStackHelper.isDimensionStacked;

var createRenderPlanner = __webpack_require__(/*! ../chart/helper/createRenderPlanner */ "./node_modules/echarts/lib/chart/helper/createRenderPlanner.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/* global Float32Array */


var STACK_PREFIX = '__ec_stack_';
var LARGE_BAR_MIN_WIDTH = 0.5;
var LargeArr = typeof Float32Array !== 'undefined' ? Float32Array : Array;

function getSeriesStackId(seriesModel) {
  return seriesModel.get('stack') || STACK_PREFIX + seriesModel.seriesIndex;
}

function getAxisKey(axis) {
  return axis.dim + axis.index;
}
/**
 * @param {Object} opt
 * @param {module:echarts/coord/Axis} opt.axis Only support category axis currently.
 * @param {number} opt.count Positive interger.
 * @param {number} [opt.barWidth]
 * @param {number} [opt.barMaxWidth]
 * @param {number} [opt.barGap]
 * @param {number} [opt.barCategoryGap]
 * @return {Object} {width, offset, offsetCenter} If axis.type is not 'category', return undefined.
 */


function getLayoutOnAxis(opt) {
  var params = [];
  var baseAxis = opt.axis;
  var axisKey = 'axis0';

  if (baseAxis.type !== 'category') {
    return;
  }

  var bandWidth = baseAxis.getBandWidth();

  for (var i = 0; i < opt.count || 0; i++) {
    params.push(zrUtil.defaults({
      bandWidth: bandWidth,
      axisKey: axisKey,
      stackId: STACK_PREFIX + i
    }, opt));
  }

  var widthAndOffsets = doCalBarWidthAndOffset(params);
  var result = [];

  for (var i = 0; i < opt.count; i++) {
    var item = widthAndOffsets[axisKey][STACK_PREFIX + i];
    item.offsetCenter = item.offset + item.width / 2;
    result.push(item);
  }

  return result;
}

function prepareLayoutBarSeries(seriesType, ecModel) {
  var seriesModels = [];
  ecModel.eachSeriesByType(seriesType, function (seriesModel) {
    // Check series coordinate, do layout for cartesian2d only
    if (isOnCartesian(seriesModel) && !isInLargeMode(seriesModel)) {
      seriesModels.push(seriesModel);
    }
  });
  return seriesModels;
}

function makeColumnLayout(barSeries) {
  var seriesInfoList = [];
  zrUtil.each(barSeries, function (seriesModel) {
    var data = seriesModel.getData();
    var cartesian = seriesModel.coordinateSystem;
    var baseAxis = cartesian.getBaseAxis();
    var axisExtent = baseAxis.getExtent();
    var bandWidth = baseAxis.type === 'category' ? baseAxis.getBandWidth() : Math.abs(axisExtent[1] - axisExtent[0]) / data.count();
    var barWidth = parsePercent(seriesModel.get('barWidth'), bandWidth);
    var barMaxWidth = parsePercent(seriesModel.get('barMaxWidth'), bandWidth);
    var barGap = seriesModel.get('barGap');
    var barCategoryGap = seriesModel.get('barCategoryGap');
    seriesInfoList.push({
      bandWidth: bandWidth,
      barWidth: barWidth,
      barMaxWidth: barMaxWidth,
      barGap: barGap,
      barCategoryGap: barCategoryGap,
      axisKey: getAxisKey(baseAxis),
      stackId: getSeriesStackId(seriesModel)
    });
  });
  return doCalBarWidthAndOffset(seriesInfoList);
}

function doCalBarWidthAndOffset(seriesInfoList) {
  // Columns info on each category axis. Key is cartesian name
  var columnsMap = {};
  zrUtil.each(seriesInfoList, function (seriesInfo, idx) {
    var axisKey = seriesInfo.axisKey;
    var bandWidth = seriesInfo.bandWidth;
    var columnsOnAxis = columnsMap[axisKey] || {
      bandWidth: bandWidth,
      remainedWidth: bandWidth,
      autoWidthCount: 0,
      categoryGap: '20%',
      gap: '30%',
      stacks: {}
    };
    var stacks = columnsOnAxis.stacks;
    columnsMap[axisKey] = columnsOnAxis;
    var stackId = seriesInfo.stackId;

    if (!stacks[stackId]) {
      columnsOnAxis.autoWidthCount++;
    }

    stacks[stackId] = stacks[stackId] || {
      width: 0,
      maxWidth: 0
    }; // Caution: In a single coordinate system, these barGrid attributes
    // will be shared by series. Consider that they have default values,
    // only the attributes set on the last series will work.
    // Do not change this fact unless there will be a break change.
    // TODO

    var barWidth = seriesInfo.barWidth;

    if (barWidth && !stacks[stackId].width) {
      // See #6312, do not restrict width.
      stacks[stackId].width = barWidth;
      barWidth = Math.min(columnsOnAxis.remainedWidth, barWidth);
      columnsOnAxis.remainedWidth -= barWidth;
    }

    var barMaxWidth = seriesInfo.barMaxWidth;
    barMaxWidth && (stacks[stackId].maxWidth = barMaxWidth);
    var barGap = seriesInfo.barGap;
    barGap != null && (columnsOnAxis.gap = barGap);
    var barCategoryGap = seriesInfo.barCategoryGap;
    barCategoryGap != null && (columnsOnAxis.categoryGap = barCategoryGap);
  });
  var result = {};
  zrUtil.each(columnsMap, function (columnsOnAxis, coordSysName) {
    result[coordSysName] = {};
    var stacks = columnsOnAxis.stacks;
    var bandWidth = columnsOnAxis.bandWidth;
    var categoryGap = parsePercent(columnsOnAxis.categoryGap, bandWidth);
    var barGapPercent = parsePercent(columnsOnAxis.gap, 1);
    var remainedWidth = columnsOnAxis.remainedWidth;
    var autoWidthCount = columnsOnAxis.autoWidthCount;
    var autoWidth = (remainedWidth - categoryGap) / (autoWidthCount + (autoWidthCount - 1) * barGapPercent);
    autoWidth = Math.max(autoWidth, 0); // Find if any auto calculated bar exceeded maxBarWidth

    zrUtil.each(stacks, function (column, stack) {
      var maxWidth = column.maxWidth;

      if (maxWidth && maxWidth < autoWidth) {
        maxWidth = Math.min(maxWidth, remainedWidth);

        if (column.width) {
          maxWidth = Math.min(maxWidth, column.width);
        }

        remainedWidth -= maxWidth;
        column.width = maxWidth;
        autoWidthCount--;
      }
    }); // Recalculate width again

    autoWidth = (remainedWidth - categoryGap) / (autoWidthCount + (autoWidthCount - 1) * barGapPercent);
    autoWidth = Math.max(autoWidth, 0);
    var widthSum = 0;
    var lastColumn;
    zrUtil.each(stacks, function (column, idx) {
      if (!column.width) {
        column.width = autoWidth;
      }

      lastColumn = column;
      widthSum += column.width * (1 + barGapPercent);
    });

    if (lastColumn) {
      widthSum -= lastColumn.width * barGapPercent;
    }

    var offset = -widthSum / 2;
    zrUtil.each(stacks, function (column, stackId) {
      result[coordSysName][stackId] = result[coordSysName][stackId] || {
        offset: offset,
        width: column.width
      };
      offset += column.width * (1 + barGapPercent);
    });
  });
  return result;
}
/**
 * @param {Object} barWidthAndOffset The result of makeColumnLayout
 * @param {module:echarts/coord/Axis} axis
 * @param {module:echarts/model/Series} [seriesModel] If not provided, return all.
 * @return {Object} {stackId: {offset, width}} or {offset, width} if seriesModel provided.
 */


function retrieveColumnLayout(barWidthAndOffset, axis, seriesModel) {
  if (barWidthAndOffset && axis) {
    var result = barWidthAndOffset[getAxisKey(axis)];

    if (result != null && seriesModel != null) {
      result = result[getSeriesStackId(seriesModel)];
    }

    return result;
  }
}
/**
 * @param {string} seriesType
 * @param {module:echarts/model/Global} ecModel
 */


function layout(seriesType, ecModel) {
  var seriesModels = prepareLayoutBarSeries(seriesType, ecModel);
  var barWidthAndOffset = makeColumnLayout(seriesModels);
  var lastStackCoords = {};
  var lastStackCoordsOrigin = {};
  zrUtil.each(seriesModels, function (seriesModel) {
    var data = seriesModel.getData();
    var cartesian = seriesModel.coordinateSystem;
    var baseAxis = cartesian.getBaseAxis();
    var stackId = getSeriesStackId(seriesModel);
    var columnLayoutInfo = barWidthAndOffset[getAxisKey(baseAxis)][stackId];
    var columnOffset = columnLayoutInfo.offset;
    var columnWidth = columnLayoutInfo.width;
    var valueAxis = cartesian.getOtherAxis(baseAxis);
    var barMinHeight = seriesModel.get('barMinHeight') || 0;
    lastStackCoords[stackId] = lastStackCoords[stackId] || [];
    lastStackCoordsOrigin[stackId] = lastStackCoordsOrigin[stackId] || []; // Fix #4243

    data.setLayout({
      offset: columnOffset,
      size: columnWidth
    });
    var valueDim = data.mapDimension(valueAxis.dim);
    var baseDim = data.mapDimension(baseAxis.dim);
    var stacked = isDimensionStacked(data, valueDim
    /*, baseDim*/
    );
    var isValueAxisH = valueAxis.isHorizontal();
    var valueAxisStart = getValueAxisStart(baseAxis, valueAxis, stacked);

    for (var idx = 0, len = data.count(); idx < len; idx++) {
      var value = data.get(valueDim, idx);
      var baseValue = data.get(baseDim, idx);

      if (isNaN(value)) {
        continue;
      }

      var sign = value >= 0 ? 'p' : 'n';
      var baseCoord = valueAxisStart; // Because of the barMinHeight, we can not use the value in
      // stackResultDimension directly.

      if (stacked) {
        // Only ordinal axis can be stacked.
        if (!lastStackCoords[stackId][baseValue]) {
          lastStackCoords[stackId][baseValue] = {
            p: valueAxisStart,
            // Positive stack
            n: valueAxisStart // Negative stack

          };
        } // Should also consider #4243


        baseCoord = lastStackCoords[stackId][baseValue][sign];
      }

      var x;
      var y;
      var width;
      var height;

      if (isValueAxisH) {
        var coord = cartesian.dataToPoint([value, baseValue]);
        x = baseCoord;
        y = coord[1] + columnOffset;
        width = coord[0] - valueAxisStart;
        height = columnWidth;

        if (Math.abs(width) < barMinHeight) {
          width = (width < 0 ? -1 : 1) * barMinHeight;
        }

        stacked && (lastStackCoords[stackId][baseValue][sign] += width);
      } else {
        var coord = cartesian.dataToPoint([baseValue, value]);
        x = coord[0] + columnOffset;
        y = baseCoord;
        width = columnWidth;
        height = coord[1] - valueAxisStart;

        if (Math.abs(height) < barMinHeight) {
          // Include zero to has a positive bar
          height = (height <= 0 ? -1 : 1) * barMinHeight;
        }

        stacked && (lastStackCoords[stackId][baseValue][sign] += height);
      }

      data.setItemLayout(idx, {
        x: x,
        y: y,
        width: width,
        height: height
      });
    }
  }, this);
} // TODO: Do not support stack in large mode yet.


var largeLayout = {
  seriesType: 'bar',
  plan: createRenderPlanner(),
  reset: function reset(seriesModel) {
    if (!isOnCartesian(seriesModel) || !isInLargeMode(seriesModel)) {
      return;
    }

    var data = seriesModel.getData();
    var cartesian = seriesModel.coordinateSystem;
    var baseAxis = cartesian.getBaseAxis();
    var valueAxis = cartesian.getOtherAxis(baseAxis);
    var valueDim = data.mapDimension(valueAxis.dim);
    var baseDim = data.mapDimension(baseAxis.dim);
    var valueAxisHorizontal = valueAxis.isHorizontal();
    var valueDimIdx = valueAxisHorizontal ? 0 : 1;
    var barWidth = retrieveColumnLayout(makeColumnLayout([seriesModel]), baseAxis, seriesModel).width;

    if (!(barWidth > LARGE_BAR_MIN_WIDTH)) {
      // jshint ignore:line
      barWidth = LARGE_BAR_MIN_WIDTH;
    }

    return {
      progress: progress
    };

    function progress(params, data) {
      var largePoints = new LargeArr(params.count * 2);
      var dataIndex;
      var coord = [];
      var valuePair = [];
      var offset = 0;

      while ((dataIndex = params.next()) != null) {
        valuePair[valueDimIdx] = data.get(valueDim, dataIndex);
        valuePair[1 - valueDimIdx] = data.get(baseDim, dataIndex);
        coord = cartesian.dataToPoint(valuePair, null, coord);
        largePoints[offset++] = coord[0];
        largePoints[offset++] = coord[1];
      }

      data.setLayout({
        largePoints: largePoints,
        barWidth: barWidth,
        valueAxisStart: getValueAxisStart(baseAxis, valueAxis, false),
        valueAxisHorizontal: valueAxisHorizontal
      });
    }
  }
};

function isOnCartesian(seriesModel) {
  return seriesModel.coordinateSystem && seriesModel.coordinateSystem.type === 'cartesian2d';
}

function isInLargeMode(seriesModel) {
  return seriesModel.pipelineContext && seriesModel.pipelineContext.large;
} // See cases in `test/bar-start.html` and `#7412`, `#8747`.


function getValueAxisStart(baseAxis, valueAxis, stacked) {
  var extent = valueAxis.getGlobalExtent();
  var min;
  var max;

  if (extent[0] > extent[1]) {
    min = extent[1];
    max = extent[0];
  } else {
    min = extent[0];
    max = extent[1];
  }

  var valueStart = valueAxis.toGlobalCoord(valueAxis.dataToCoord(0));
  valueStart < min && (valueStart = min);
  valueStart > max && (valueStart = max);
  return valueStart;
}

exports.getLayoutOnAxis = getLayoutOnAxis;
exports.prepareLayoutBarSeries = prepareLayoutBarSeries;
exports.makeColumnLayout = makeColumnLayout;
exports.retrieveColumnLayout = retrieveColumnLayout;
exports.layout = layout;
exports.largeLayout = largeLayout;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvbGF5b3V0L2JhckdyaWQuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9sYXlvdXQvYmFyR3JpZC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBfbnVtYmVyID0gcmVxdWlyZShcIi4uL3V0aWwvbnVtYmVyXCIpO1xuXG52YXIgcGFyc2VQZXJjZW50ID0gX251bWJlci5wYXJzZVBlcmNlbnQ7XG5cbnZhciBfZGF0YVN0YWNrSGVscGVyID0gcmVxdWlyZShcIi4uL2RhdGEvaGVscGVyL2RhdGFTdGFja0hlbHBlclwiKTtcblxudmFyIGlzRGltZW5zaW9uU3RhY2tlZCA9IF9kYXRhU3RhY2tIZWxwZXIuaXNEaW1lbnNpb25TdGFja2VkO1xuXG52YXIgY3JlYXRlUmVuZGVyUGxhbm5lciA9IHJlcXVpcmUoXCIuLi9jaGFydC9oZWxwZXIvY3JlYXRlUmVuZGVyUGxhbm5lclwiKTtcblxuLypcbiogTGljZW5zZWQgdG8gdGhlIEFwYWNoZSBTb2Z0d2FyZSBGb3VuZGF0aW9uIChBU0YpIHVuZGVyIG9uZVxuKiBvciBtb3JlIGNvbnRyaWJ1dG9yIGxpY2Vuc2UgYWdyZWVtZW50cy4gIFNlZSB0aGUgTk9USUNFIGZpbGVcbiogZGlzdHJpYnV0ZWQgd2l0aCB0aGlzIHdvcmsgZm9yIGFkZGl0aW9uYWwgaW5mb3JtYXRpb25cbiogcmVnYXJkaW5nIGNvcHlyaWdodCBvd25lcnNoaXAuICBUaGUgQVNGIGxpY2Vuc2VzIHRoaXMgZmlsZVxuKiB0byB5b3UgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlXG4qIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZVxuKiB3aXRoIHRoZSBMaWNlbnNlLiAgWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4qXG4qICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4qXG4qIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZyxcbiogc29mdHdhcmUgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW5cbiogXCJBUyBJU1wiIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcbiogS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC4gIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlXG4qIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmQgbGltaXRhdGlvbnNcbiogdW5kZXIgdGhlIExpY2Vuc2UuXG4qL1xuXG4vKiBnbG9iYWwgRmxvYXQzMkFycmF5ICovXG52YXIgU1RBQ0tfUFJFRklYID0gJ19fZWNfc3RhY2tfJztcbnZhciBMQVJHRV9CQVJfTUlOX1dJRFRIID0gMC41O1xudmFyIExhcmdlQXJyID0gdHlwZW9mIEZsb2F0MzJBcnJheSAhPT0gJ3VuZGVmaW5lZCcgPyBGbG9hdDMyQXJyYXkgOiBBcnJheTtcblxuZnVuY3Rpb24gZ2V0U2VyaWVzU3RhY2tJZChzZXJpZXNNb2RlbCkge1xuICByZXR1cm4gc2VyaWVzTW9kZWwuZ2V0KCdzdGFjaycpIHx8IFNUQUNLX1BSRUZJWCArIHNlcmllc01vZGVsLnNlcmllc0luZGV4O1xufVxuXG5mdW5jdGlvbiBnZXRBeGlzS2V5KGF4aXMpIHtcbiAgcmV0dXJuIGF4aXMuZGltICsgYXhpcy5pbmRleDtcbn1cbi8qKlxuICogQHBhcmFtIHtPYmplY3R9IG9wdFxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9jb29yZC9BeGlzfSBvcHQuYXhpcyBPbmx5IHN1cHBvcnQgY2F0ZWdvcnkgYXhpcyBjdXJyZW50bHkuXG4gKiBAcGFyYW0ge251bWJlcn0gb3B0LmNvdW50IFBvc2l0aXZlIGludGVyZ2VyLlxuICogQHBhcmFtIHtudW1iZXJ9IFtvcHQuYmFyV2lkdGhdXG4gKiBAcGFyYW0ge251bWJlcn0gW29wdC5iYXJNYXhXaWR0aF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LmJhckdhcF1cbiAqIEBwYXJhbSB7bnVtYmVyfSBbb3B0LmJhckNhdGVnb3J5R2FwXVxuICogQHJldHVybiB7T2JqZWN0fSB7d2lkdGgsIG9mZnNldCwgb2Zmc2V0Q2VudGVyfSBJZiBheGlzLnR5cGUgaXMgbm90ICdjYXRlZ29yeScsIHJldHVybiB1bmRlZmluZWQuXG4gKi9cblxuXG5mdW5jdGlvbiBnZXRMYXlvdXRPbkF4aXMob3B0KSB7XG4gIHZhciBwYXJhbXMgPSBbXTtcbiAgdmFyIGJhc2VBeGlzID0gb3B0LmF4aXM7XG4gIHZhciBheGlzS2V5ID0gJ2F4aXMwJztcblxuICBpZiAoYmFzZUF4aXMudHlwZSAhPT0gJ2NhdGVnb3J5Jykge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIHZhciBiYW5kV2lkdGggPSBiYXNlQXhpcy5nZXRCYW5kV2lkdGgoKTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IG9wdC5jb3VudCB8fCAwOyBpKyspIHtcbiAgICBwYXJhbXMucHVzaCh6clV0aWwuZGVmYXVsdHMoe1xuICAgICAgYmFuZFdpZHRoOiBiYW5kV2lkdGgsXG4gICAgICBheGlzS2V5OiBheGlzS2V5LFxuICAgICAgc3RhY2tJZDogU1RBQ0tfUFJFRklYICsgaVxuICAgIH0sIG9wdCkpO1xuICB9XG5cbiAgdmFyIHdpZHRoQW5kT2Zmc2V0cyA9IGRvQ2FsQmFyV2lkdGhBbmRPZmZzZXQocGFyYW1zKTtcbiAgdmFyIHJlc3VsdCA9IFtdO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgb3B0LmNvdW50OyBpKyspIHtcbiAgICB2YXIgaXRlbSA9IHdpZHRoQW5kT2Zmc2V0c1theGlzS2V5XVtTVEFDS19QUkVGSVggKyBpXTtcbiAgICBpdGVtLm9mZnNldENlbnRlciA9IGl0ZW0ub2Zmc2V0ICsgaXRlbS53aWR0aCAvIDI7XG4gICAgcmVzdWx0LnB1c2goaXRlbSk7XG4gIH1cblxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBwcmVwYXJlTGF5b3V0QmFyU2VyaWVzKHNlcmllc1R5cGUsIGVjTW9kZWwpIHtcbiAgdmFyIHNlcmllc01vZGVscyA9IFtdO1xuICBlY01vZGVsLmVhY2hTZXJpZXNCeVR5cGUoc2VyaWVzVHlwZSwgZnVuY3Rpb24gKHNlcmllc01vZGVsKSB7XG4gICAgLy8gQ2hlY2sgc2VyaWVzIGNvb3JkaW5hdGUsIGRvIGxheW91dCBmb3IgY2FydGVzaWFuMmQgb25seVxuICAgIGlmIChpc09uQ2FydGVzaWFuKHNlcmllc01vZGVsKSAmJiAhaXNJbkxhcmdlTW9kZShzZXJpZXNNb2RlbCkpIHtcbiAgICAgIHNlcmllc01vZGVscy5wdXNoKHNlcmllc01vZGVsKTtcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gc2VyaWVzTW9kZWxzO1xufVxuXG5mdW5jdGlvbiBtYWtlQ29sdW1uTGF5b3V0KGJhclNlcmllcykge1xuICB2YXIgc2VyaWVzSW5mb0xpc3QgPSBbXTtcbiAgenJVdGlsLmVhY2goYmFyU2VyaWVzLCBmdW5jdGlvbiAoc2VyaWVzTW9kZWwpIHtcbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgY2FydGVzaWFuID0gc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbTtcbiAgICB2YXIgYmFzZUF4aXMgPSBjYXJ0ZXNpYW4uZ2V0QmFzZUF4aXMoKTtcbiAgICB2YXIgYXhpc0V4dGVudCA9IGJhc2VBeGlzLmdldEV4dGVudCgpO1xuICAgIHZhciBiYW5kV2lkdGggPSBiYXNlQXhpcy50eXBlID09PSAnY2F0ZWdvcnknID8gYmFzZUF4aXMuZ2V0QmFuZFdpZHRoKCkgOiBNYXRoLmFicyhheGlzRXh0ZW50WzFdIC0gYXhpc0V4dGVudFswXSkgLyBkYXRhLmNvdW50KCk7XG4gICAgdmFyIGJhcldpZHRoID0gcGFyc2VQZXJjZW50KHNlcmllc01vZGVsLmdldCgnYmFyV2lkdGgnKSwgYmFuZFdpZHRoKTtcbiAgICB2YXIgYmFyTWF4V2lkdGggPSBwYXJzZVBlcmNlbnQoc2VyaWVzTW9kZWwuZ2V0KCdiYXJNYXhXaWR0aCcpLCBiYW5kV2lkdGgpO1xuICAgIHZhciBiYXJHYXAgPSBzZXJpZXNNb2RlbC5nZXQoJ2JhckdhcCcpO1xuICAgIHZhciBiYXJDYXRlZ29yeUdhcCA9IHNlcmllc01vZGVsLmdldCgnYmFyQ2F0ZWdvcnlHYXAnKTtcbiAgICBzZXJpZXNJbmZvTGlzdC5wdXNoKHtcbiAgICAgIGJhbmRXaWR0aDogYmFuZFdpZHRoLFxuICAgICAgYmFyV2lkdGg6IGJhcldpZHRoLFxuICAgICAgYmFyTWF4V2lkdGg6IGJhck1heFdpZHRoLFxuICAgICAgYmFyR2FwOiBiYXJHYXAsXG4gICAgICBiYXJDYXRlZ29yeUdhcDogYmFyQ2F0ZWdvcnlHYXAsXG4gICAgICBheGlzS2V5OiBnZXRBeGlzS2V5KGJhc2VBeGlzKSxcbiAgICAgIHN0YWNrSWQ6IGdldFNlcmllc1N0YWNrSWQoc2VyaWVzTW9kZWwpXG4gICAgfSk7XG4gIH0pO1xuICByZXR1cm4gZG9DYWxCYXJXaWR0aEFuZE9mZnNldChzZXJpZXNJbmZvTGlzdCk7XG59XG5cbmZ1bmN0aW9uIGRvQ2FsQmFyV2lkdGhBbmRPZmZzZXQoc2VyaWVzSW5mb0xpc3QpIHtcbiAgLy8gQ29sdW1ucyBpbmZvIG9uIGVhY2ggY2F0ZWdvcnkgYXhpcy4gS2V5IGlzIGNhcnRlc2lhbiBuYW1lXG4gIHZhciBjb2x1bW5zTWFwID0ge307XG4gIHpyVXRpbC5lYWNoKHNlcmllc0luZm9MaXN0LCBmdW5jdGlvbiAoc2VyaWVzSW5mbywgaWR4KSB7XG4gICAgdmFyIGF4aXNLZXkgPSBzZXJpZXNJbmZvLmF4aXNLZXk7XG4gICAgdmFyIGJhbmRXaWR0aCA9IHNlcmllc0luZm8uYmFuZFdpZHRoO1xuICAgIHZhciBjb2x1bW5zT25BeGlzID0gY29sdW1uc01hcFtheGlzS2V5XSB8fCB7XG4gICAgICBiYW5kV2lkdGg6IGJhbmRXaWR0aCxcbiAgICAgIHJlbWFpbmVkV2lkdGg6IGJhbmRXaWR0aCxcbiAgICAgIGF1dG9XaWR0aENvdW50OiAwLFxuICAgICAgY2F0ZWdvcnlHYXA6ICcyMCUnLFxuICAgICAgZ2FwOiAnMzAlJyxcbiAgICAgIHN0YWNrczoge31cbiAgICB9O1xuICAgIHZhciBzdGFja3MgPSBjb2x1bW5zT25BeGlzLnN0YWNrcztcbiAgICBjb2x1bW5zTWFwW2F4aXNLZXldID0gY29sdW1uc09uQXhpcztcbiAgICB2YXIgc3RhY2tJZCA9IHNlcmllc0luZm8uc3RhY2tJZDtcblxuICAgIGlmICghc3RhY2tzW3N0YWNrSWRdKSB7XG4gICAgICBjb2x1bW5zT25BeGlzLmF1dG9XaWR0aENvdW50Kys7XG4gICAgfVxuXG4gICAgc3RhY2tzW3N0YWNrSWRdID0gc3RhY2tzW3N0YWNrSWRdIHx8IHtcbiAgICAgIHdpZHRoOiAwLFxuICAgICAgbWF4V2lkdGg6IDBcbiAgICB9OyAvLyBDYXV0aW9uOiBJbiBhIHNpbmdsZSBjb29yZGluYXRlIHN5c3RlbSwgdGhlc2UgYmFyR3JpZCBhdHRyaWJ1dGVzXG4gICAgLy8gd2lsbCBiZSBzaGFyZWQgYnkgc2VyaWVzLiBDb25zaWRlciB0aGF0IHRoZXkgaGF2ZSBkZWZhdWx0IHZhbHVlcyxcbiAgICAvLyBvbmx5IHRoZSBhdHRyaWJ1dGVzIHNldCBvbiB0aGUgbGFzdCBzZXJpZXMgd2lsbCB3b3JrLlxuICAgIC8vIERvIG5vdCBjaGFuZ2UgdGhpcyBmYWN0IHVubGVzcyB0aGVyZSB3aWxsIGJlIGEgYnJlYWsgY2hhbmdlLlxuICAgIC8vIFRPRE9cblxuICAgIHZhciBiYXJXaWR0aCA9IHNlcmllc0luZm8uYmFyV2lkdGg7XG5cbiAgICBpZiAoYmFyV2lkdGggJiYgIXN0YWNrc1tzdGFja0lkXS53aWR0aCkge1xuICAgICAgLy8gU2VlICM2MzEyLCBkbyBub3QgcmVzdHJpY3Qgd2lkdGguXG4gICAgICBzdGFja3Nbc3RhY2tJZF0ud2lkdGggPSBiYXJXaWR0aDtcbiAgICAgIGJhcldpZHRoID0gTWF0aC5taW4oY29sdW1uc09uQXhpcy5yZW1haW5lZFdpZHRoLCBiYXJXaWR0aCk7XG4gICAgICBjb2x1bW5zT25BeGlzLnJlbWFpbmVkV2lkdGggLT0gYmFyV2lkdGg7XG4gICAgfVxuXG4gICAgdmFyIGJhck1heFdpZHRoID0gc2VyaWVzSW5mby5iYXJNYXhXaWR0aDtcbiAgICBiYXJNYXhXaWR0aCAmJiAoc3RhY2tzW3N0YWNrSWRdLm1heFdpZHRoID0gYmFyTWF4V2lkdGgpO1xuICAgIHZhciBiYXJHYXAgPSBzZXJpZXNJbmZvLmJhckdhcDtcbiAgICBiYXJHYXAgIT0gbnVsbCAmJiAoY29sdW1uc09uQXhpcy5nYXAgPSBiYXJHYXApO1xuICAgIHZhciBiYXJDYXRlZ29yeUdhcCA9IHNlcmllc0luZm8uYmFyQ2F0ZWdvcnlHYXA7XG4gICAgYmFyQ2F0ZWdvcnlHYXAgIT0gbnVsbCAmJiAoY29sdW1uc09uQXhpcy5jYXRlZ29yeUdhcCA9IGJhckNhdGVnb3J5R2FwKTtcbiAgfSk7XG4gIHZhciByZXN1bHQgPSB7fTtcbiAgenJVdGlsLmVhY2goY29sdW1uc01hcCwgZnVuY3Rpb24gKGNvbHVtbnNPbkF4aXMsIGNvb3JkU3lzTmFtZSkge1xuICAgIHJlc3VsdFtjb29yZFN5c05hbWVdID0ge307XG4gICAgdmFyIHN0YWNrcyA9IGNvbHVtbnNPbkF4aXMuc3RhY2tzO1xuICAgIHZhciBiYW5kV2lkdGggPSBjb2x1bW5zT25BeGlzLmJhbmRXaWR0aDtcbiAgICB2YXIgY2F0ZWdvcnlHYXAgPSBwYXJzZVBlcmNlbnQoY29sdW1uc09uQXhpcy5jYXRlZ29yeUdhcCwgYmFuZFdpZHRoKTtcbiAgICB2YXIgYmFyR2FwUGVyY2VudCA9IHBhcnNlUGVyY2VudChjb2x1bW5zT25BeGlzLmdhcCwgMSk7XG4gICAgdmFyIHJlbWFpbmVkV2lkdGggPSBjb2x1bW5zT25BeGlzLnJlbWFpbmVkV2lkdGg7XG4gICAgdmFyIGF1dG9XaWR0aENvdW50ID0gY29sdW1uc09uQXhpcy5hdXRvV2lkdGhDb3VudDtcbiAgICB2YXIgYXV0b1dpZHRoID0gKHJlbWFpbmVkV2lkdGggLSBjYXRlZ29yeUdhcCkgLyAoYXV0b1dpZHRoQ291bnQgKyAoYXV0b1dpZHRoQ291bnQgLSAxKSAqIGJhckdhcFBlcmNlbnQpO1xuICAgIGF1dG9XaWR0aCA9IE1hdGgubWF4KGF1dG9XaWR0aCwgMCk7IC8vIEZpbmQgaWYgYW55IGF1dG8gY2FsY3VsYXRlZCBiYXIgZXhjZWVkZWQgbWF4QmFyV2lkdGhcblxuICAgIHpyVXRpbC5lYWNoKHN0YWNrcywgZnVuY3Rpb24gKGNvbHVtbiwgc3RhY2spIHtcbiAgICAgIHZhciBtYXhXaWR0aCA9IGNvbHVtbi5tYXhXaWR0aDtcblxuICAgICAgaWYgKG1heFdpZHRoICYmIG1heFdpZHRoIDwgYXV0b1dpZHRoKSB7XG4gICAgICAgIG1heFdpZHRoID0gTWF0aC5taW4obWF4V2lkdGgsIHJlbWFpbmVkV2lkdGgpO1xuXG4gICAgICAgIGlmIChjb2x1bW4ud2lkdGgpIHtcbiAgICAgICAgICBtYXhXaWR0aCA9IE1hdGgubWluKG1heFdpZHRoLCBjb2x1bW4ud2lkdGgpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVtYWluZWRXaWR0aCAtPSBtYXhXaWR0aDtcbiAgICAgICAgY29sdW1uLndpZHRoID0gbWF4V2lkdGg7XG4gICAgICAgIGF1dG9XaWR0aENvdW50LS07XG4gICAgICB9XG4gICAgfSk7IC8vIFJlY2FsY3VsYXRlIHdpZHRoIGFnYWluXG5cbiAgICBhdXRvV2lkdGggPSAocmVtYWluZWRXaWR0aCAtIGNhdGVnb3J5R2FwKSAvIChhdXRvV2lkdGhDb3VudCArIChhdXRvV2lkdGhDb3VudCAtIDEpICogYmFyR2FwUGVyY2VudCk7XG4gICAgYXV0b1dpZHRoID0gTWF0aC5tYXgoYXV0b1dpZHRoLCAwKTtcbiAgICB2YXIgd2lkdGhTdW0gPSAwO1xuICAgIHZhciBsYXN0Q29sdW1uO1xuICAgIHpyVXRpbC5lYWNoKHN0YWNrcywgZnVuY3Rpb24gKGNvbHVtbiwgaWR4KSB7XG4gICAgICBpZiAoIWNvbHVtbi53aWR0aCkge1xuICAgICAgICBjb2x1bW4ud2lkdGggPSBhdXRvV2lkdGg7XG4gICAgICB9XG5cbiAgICAgIGxhc3RDb2x1bW4gPSBjb2x1bW47XG4gICAgICB3aWR0aFN1bSArPSBjb2x1bW4ud2lkdGggKiAoMSArIGJhckdhcFBlcmNlbnQpO1xuICAgIH0pO1xuXG4gICAgaWYgKGxhc3RDb2x1bW4pIHtcbiAgICAgIHdpZHRoU3VtIC09IGxhc3RDb2x1bW4ud2lkdGggKiBiYXJHYXBQZXJjZW50O1xuICAgIH1cblxuICAgIHZhciBvZmZzZXQgPSAtd2lkdGhTdW0gLyAyO1xuICAgIHpyVXRpbC5lYWNoKHN0YWNrcywgZnVuY3Rpb24gKGNvbHVtbiwgc3RhY2tJZCkge1xuICAgICAgcmVzdWx0W2Nvb3JkU3lzTmFtZV1bc3RhY2tJZF0gPSByZXN1bHRbY29vcmRTeXNOYW1lXVtzdGFja0lkXSB8fCB7XG4gICAgICAgIG9mZnNldDogb2Zmc2V0LFxuICAgICAgICB3aWR0aDogY29sdW1uLndpZHRoXG4gICAgICB9O1xuICAgICAgb2Zmc2V0ICs9IGNvbHVtbi53aWR0aCAqICgxICsgYmFyR2FwUGVyY2VudCk7XG4gICAgfSk7XG4gIH0pO1xuICByZXR1cm4gcmVzdWx0O1xufVxuLyoqXG4gKiBAcGFyYW0ge09iamVjdH0gYmFyV2lkdGhBbmRPZmZzZXQgVGhlIHJlc3VsdCBvZiBtYWtlQ29sdW1uTGF5b3V0XG4gKiBAcGFyYW0ge21vZHVsZTplY2hhcnRzL2Nvb3JkL0F4aXN9IGF4aXNcbiAqIEBwYXJhbSB7bW9kdWxlOmVjaGFydHMvbW9kZWwvU2VyaWVzfSBbc2VyaWVzTW9kZWxdIElmIG5vdCBwcm92aWRlZCwgcmV0dXJuIGFsbC5cbiAqIEByZXR1cm4ge09iamVjdH0ge3N0YWNrSWQ6IHtvZmZzZXQsIHdpZHRofX0gb3Ige29mZnNldCwgd2lkdGh9IGlmIHNlcmllc01vZGVsIHByb3ZpZGVkLlxuICovXG5cblxuZnVuY3Rpb24gcmV0cmlldmVDb2x1bW5MYXlvdXQoYmFyV2lkdGhBbmRPZmZzZXQsIGF4aXMsIHNlcmllc01vZGVsKSB7XG4gIGlmIChiYXJXaWR0aEFuZE9mZnNldCAmJiBheGlzKSB7XG4gICAgdmFyIHJlc3VsdCA9IGJhcldpZHRoQW5kT2Zmc2V0W2dldEF4aXNLZXkoYXhpcyldO1xuXG4gICAgaWYgKHJlc3VsdCAhPSBudWxsICYmIHNlcmllc01vZGVsICE9IG51bGwpIHtcbiAgICAgIHJlc3VsdCA9IHJlc3VsdFtnZXRTZXJpZXNTdGFja0lkKHNlcmllc01vZGVsKV07XG4gICAgfVxuXG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufVxuLyoqXG4gKiBAcGFyYW0ge3N0cmluZ30gc2VyaWVzVHlwZVxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9HbG9iYWx9IGVjTW9kZWxcbiAqL1xuXG5cbmZ1bmN0aW9uIGxheW91dChzZXJpZXNUeXBlLCBlY01vZGVsKSB7XG4gIHZhciBzZXJpZXNNb2RlbHMgPSBwcmVwYXJlTGF5b3V0QmFyU2VyaWVzKHNlcmllc1R5cGUsIGVjTW9kZWwpO1xuICB2YXIgYmFyV2lkdGhBbmRPZmZzZXQgPSBtYWtlQ29sdW1uTGF5b3V0KHNlcmllc01vZGVscyk7XG4gIHZhciBsYXN0U3RhY2tDb29yZHMgPSB7fTtcbiAgdmFyIGxhc3RTdGFja0Nvb3Jkc09yaWdpbiA9IHt9O1xuICB6clV0aWwuZWFjaChzZXJpZXNNb2RlbHMsIGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIHZhciBkYXRhID0gc2VyaWVzTW9kZWwuZ2V0RGF0YSgpO1xuICAgIHZhciBjYXJ0ZXNpYW4gPSBzZXJpZXNNb2RlbC5jb29yZGluYXRlU3lzdGVtO1xuICAgIHZhciBiYXNlQXhpcyA9IGNhcnRlc2lhbi5nZXRCYXNlQXhpcygpO1xuICAgIHZhciBzdGFja0lkID0gZ2V0U2VyaWVzU3RhY2tJZChzZXJpZXNNb2RlbCk7XG4gICAgdmFyIGNvbHVtbkxheW91dEluZm8gPSBiYXJXaWR0aEFuZE9mZnNldFtnZXRBeGlzS2V5KGJhc2VBeGlzKV1bc3RhY2tJZF07XG4gICAgdmFyIGNvbHVtbk9mZnNldCA9IGNvbHVtbkxheW91dEluZm8ub2Zmc2V0O1xuICAgIHZhciBjb2x1bW5XaWR0aCA9IGNvbHVtbkxheW91dEluZm8ud2lkdGg7XG4gICAgdmFyIHZhbHVlQXhpcyA9IGNhcnRlc2lhbi5nZXRPdGhlckF4aXMoYmFzZUF4aXMpO1xuICAgIHZhciBiYXJNaW5IZWlnaHQgPSBzZXJpZXNNb2RlbC5nZXQoJ2Jhck1pbkhlaWdodCcpIHx8IDA7XG4gICAgbGFzdFN0YWNrQ29vcmRzW3N0YWNrSWRdID0gbGFzdFN0YWNrQ29vcmRzW3N0YWNrSWRdIHx8IFtdO1xuICAgIGxhc3RTdGFja0Nvb3Jkc09yaWdpbltzdGFja0lkXSA9IGxhc3RTdGFja0Nvb3Jkc09yaWdpbltzdGFja0lkXSB8fCBbXTsgLy8gRml4ICM0MjQzXG5cbiAgICBkYXRhLnNldExheW91dCh7XG4gICAgICBvZmZzZXQ6IGNvbHVtbk9mZnNldCxcbiAgICAgIHNpemU6IGNvbHVtbldpZHRoXG4gICAgfSk7XG4gICAgdmFyIHZhbHVlRGltID0gZGF0YS5tYXBEaW1lbnNpb24odmFsdWVBeGlzLmRpbSk7XG4gICAgdmFyIGJhc2VEaW0gPSBkYXRhLm1hcERpbWVuc2lvbihiYXNlQXhpcy5kaW0pO1xuICAgIHZhciBzdGFja2VkID0gaXNEaW1lbnNpb25TdGFja2VkKGRhdGEsIHZhbHVlRGltXG4gICAgLyosIGJhc2VEaW0qL1xuICAgICk7XG4gICAgdmFyIGlzVmFsdWVBeGlzSCA9IHZhbHVlQXhpcy5pc0hvcml6b250YWwoKTtcbiAgICB2YXIgdmFsdWVBeGlzU3RhcnQgPSBnZXRWYWx1ZUF4aXNTdGFydChiYXNlQXhpcywgdmFsdWVBeGlzLCBzdGFja2VkKTtcblxuICAgIGZvciAodmFyIGlkeCA9IDAsIGxlbiA9IGRhdGEuY291bnQoKTsgaWR4IDwgbGVuOyBpZHgrKykge1xuICAgICAgdmFyIHZhbHVlID0gZGF0YS5nZXQodmFsdWVEaW0sIGlkeCk7XG4gICAgICB2YXIgYmFzZVZhbHVlID0gZGF0YS5nZXQoYmFzZURpbSwgaWR4KTtcblxuICAgICAgaWYgKGlzTmFOKHZhbHVlKSkge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgdmFyIHNpZ24gPSB2YWx1ZSA+PSAwID8gJ3AnIDogJ24nO1xuICAgICAgdmFyIGJhc2VDb29yZCA9IHZhbHVlQXhpc1N0YXJ0OyAvLyBCZWNhdXNlIG9mIHRoZSBiYXJNaW5IZWlnaHQsIHdlIGNhbiBub3QgdXNlIHRoZSB2YWx1ZSBpblxuICAgICAgLy8gc3RhY2tSZXN1bHREaW1lbnNpb24gZGlyZWN0bHkuXG5cbiAgICAgIGlmIChzdGFja2VkKSB7XG4gICAgICAgIC8vIE9ubHkgb3JkaW5hbCBheGlzIGNhbiBiZSBzdGFja2VkLlxuICAgICAgICBpZiAoIWxhc3RTdGFja0Nvb3Jkc1tzdGFja0lkXVtiYXNlVmFsdWVdKSB7XG4gICAgICAgICAgbGFzdFN0YWNrQ29vcmRzW3N0YWNrSWRdW2Jhc2VWYWx1ZV0gPSB7XG4gICAgICAgICAgICBwOiB2YWx1ZUF4aXNTdGFydCxcbiAgICAgICAgICAgIC8vIFBvc2l0aXZlIHN0YWNrXG4gICAgICAgICAgICBuOiB2YWx1ZUF4aXNTdGFydCAvLyBOZWdhdGl2ZSBzdGFja1xuXG4gICAgICAgICAgfTtcbiAgICAgICAgfSAvLyBTaG91bGQgYWxzbyBjb25zaWRlciAjNDI0M1xuXG5cbiAgICAgICAgYmFzZUNvb3JkID0gbGFzdFN0YWNrQ29vcmRzW3N0YWNrSWRdW2Jhc2VWYWx1ZV1bc2lnbl07XG4gICAgICB9XG5cbiAgICAgIHZhciB4O1xuICAgICAgdmFyIHk7XG4gICAgICB2YXIgd2lkdGg7XG4gICAgICB2YXIgaGVpZ2h0O1xuXG4gICAgICBpZiAoaXNWYWx1ZUF4aXNIKSB7XG4gICAgICAgIHZhciBjb29yZCA9IGNhcnRlc2lhbi5kYXRhVG9Qb2ludChbdmFsdWUsIGJhc2VWYWx1ZV0pO1xuICAgICAgICB4ID0gYmFzZUNvb3JkO1xuICAgICAgICB5ID0gY29vcmRbMV0gKyBjb2x1bW5PZmZzZXQ7XG4gICAgICAgIHdpZHRoID0gY29vcmRbMF0gLSB2YWx1ZUF4aXNTdGFydDtcbiAgICAgICAgaGVpZ2h0ID0gY29sdW1uV2lkdGg7XG5cbiAgICAgICAgaWYgKE1hdGguYWJzKHdpZHRoKSA8IGJhck1pbkhlaWdodCkge1xuICAgICAgICAgIHdpZHRoID0gKHdpZHRoIDwgMCA/IC0xIDogMSkgKiBiYXJNaW5IZWlnaHQ7XG4gICAgICAgIH1cblxuICAgICAgICBzdGFja2VkICYmIChsYXN0U3RhY2tDb29yZHNbc3RhY2tJZF1bYmFzZVZhbHVlXVtzaWduXSArPSB3aWR0aCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgY29vcmQgPSBjYXJ0ZXNpYW4uZGF0YVRvUG9pbnQoW2Jhc2VWYWx1ZSwgdmFsdWVdKTtcbiAgICAgICAgeCA9IGNvb3JkWzBdICsgY29sdW1uT2Zmc2V0O1xuICAgICAgICB5ID0gYmFzZUNvb3JkO1xuICAgICAgICB3aWR0aCA9IGNvbHVtbldpZHRoO1xuICAgICAgICBoZWlnaHQgPSBjb29yZFsxXSAtIHZhbHVlQXhpc1N0YXJ0O1xuXG4gICAgICAgIGlmIChNYXRoLmFicyhoZWlnaHQpIDwgYmFyTWluSGVpZ2h0KSB7XG4gICAgICAgICAgLy8gSW5jbHVkZSB6ZXJvIHRvIGhhcyBhIHBvc2l0aXZlIGJhclxuICAgICAgICAgIGhlaWdodCA9IChoZWlnaHQgPD0gMCA/IC0xIDogMSkgKiBiYXJNaW5IZWlnaHQ7XG4gICAgICAgIH1cblxuICAgICAgICBzdGFja2VkICYmIChsYXN0U3RhY2tDb29yZHNbc3RhY2tJZF1bYmFzZVZhbHVlXVtzaWduXSArPSBoZWlnaHQpO1xuICAgICAgfVxuXG4gICAgICBkYXRhLnNldEl0ZW1MYXlvdXQoaWR4LCB7XG4gICAgICAgIHg6IHgsXG4gICAgICAgIHk6IHksXG4gICAgICAgIHdpZHRoOiB3aWR0aCxcbiAgICAgICAgaGVpZ2h0OiBoZWlnaHRcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwgdGhpcyk7XG59IC8vIFRPRE86IERvIG5vdCBzdXBwb3J0IHN0YWNrIGluIGxhcmdlIG1vZGUgeWV0LlxuXG5cbnZhciBsYXJnZUxheW91dCA9IHtcbiAgc2VyaWVzVHlwZTogJ2JhcicsXG4gIHBsYW46IGNyZWF0ZVJlbmRlclBsYW5uZXIoKSxcbiAgcmVzZXQ6IGZ1bmN0aW9uIChzZXJpZXNNb2RlbCkge1xuICAgIGlmICghaXNPbkNhcnRlc2lhbihzZXJpZXNNb2RlbCkgfHwgIWlzSW5MYXJnZU1vZGUoc2VyaWVzTW9kZWwpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gICAgdmFyIGNhcnRlc2lhbiA9IHNlcmllc01vZGVsLmNvb3JkaW5hdGVTeXN0ZW07XG4gICAgdmFyIGJhc2VBeGlzID0gY2FydGVzaWFuLmdldEJhc2VBeGlzKCk7XG4gICAgdmFyIHZhbHVlQXhpcyA9IGNhcnRlc2lhbi5nZXRPdGhlckF4aXMoYmFzZUF4aXMpO1xuICAgIHZhciB2YWx1ZURpbSA9IGRhdGEubWFwRGltZW5zaW9uKHZhbHVlQXhpcy5kaW0pO1xuICAgIHZhciBiYXNlRGltID0gZGF0YS5tYXBEaW1lbnNpb24oYmFzZUF4aXMuZGltKTtcbiAgICB2YXIgdmFsdWVBeGlzSG9yaXpvbnRhbCA9IHZhbHVlQXhpcy5pc0hvcml6b250YWwoKTtcbiAgICB2YXIgdmFsdWVEaW1JZHggPSB2YWx1ZUF4aXNIb3Jpem9udGFsID8gMCA6IDE7XG4gICAgdmFyIGJhcldpZHRoID0gcmV0cmlldmVDb2x1bW5MYXlvdXQobWFrZUNvbHVtbkxheW91dChbc2VyaWVzTW9kZWxdKSwgYmFzZUF4aXMsIHNlcmllc01vZGVsKS53aWR0aDtcblxuICAgIGlmICghKGJhcldpZHRoID4gTEFSR0VfQkFSX01JTl9XSURUSCkpIHtcbiAgICAgIC8vIGpzaGludCBpZ25vcmU6bGluZVxuICAgICAgYmFyV2lkdGggPSBMQVJHRV9CQVJfTUlOX1dJRFRIO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICBwcm9ncmVzczogcHJvZ3Jlc3NcbiAgICB9O1xuXG4gICAgZnVuY3Rpb24gcHJvZ3Jlc3MocGFyYW1zLCBkYXRhKSB7XG4gICAgICB2YXIgbGFyZ2VQb2ludHMgPSBuZXcgTGFyZ2VBcnIocGFyYW1zLmNvdW50ICogMik7XG4gICAgICB2YXIgZGF0YUluZGV4O1xuICAgICAgdmFyIGNvb3JkID0gW107XG4gICAgICB2YXIgdmFsdWVQYWlyID0gW107XG4gICAgICB2YXIgb2Zmc2V0ID0gMDtcblxuICAgICAgd2hpbGUgKChkYXRhSW5kZXggPSBwYXJhbXMubmV4dCgpKSAhPSBudWxsKSB7XG4gICAgICAgIHZhbHVlUGFpclt2YWx1ZURpbUlkeF0gPSBkYXRhLmdldCh2YWx1ZURpbSwgZGF0YUluZGV4KTtcbiAgICAgICAgdmFsdWVQYWlyWzEgLSB2YWx1ZURpbUlkeF0gPSBkYXRhLmdldChiYXNlRGltLCBkYXRhSW5kZXgpO1xuICAgICAgICBjb29yZCA9IGNhcnRlc2lhbi5kYXRhVG9Qb2ludCh2YWx1ZVBhaXIsIG51bGwsIGNvb3JkKTtcbiAgICAgICAgbGFyZ2VQb2ludHNbb2Zmc2V0KytdID0gY29vcmRbMF07XG4gICAgICAgIGxhcmdlUG9pbnRzW29mZnNldCsrXSA9IGNvb3JkWzFdO1xuICAgICAgfVxuXG4gICAgICBkYXRhLnNldExheW91dCh7XG4gICAgICAgIGxhcmdlUG9pbnRzOiBsYXJnZVBvaW50cyxcbiAgICAgICAgYmFyV2lkdGg6IGJhcldpZHRoLFxuICAgICAgICB2YWx1ZUF4aXNTdGFydDogZ2V0VmFsdWVBeGlzU3RhcnQoYmFzZUF4aXMsIHZhbHVlQXhpcywgZmFsc2UpLFxuICAgICAgICB2YWx1ZUF4aXNIb3Jpem9udGFsOiB2YWx1ZUF4aXNIb3Jpem9udGFsXG4gICAgICB9KTtcbiAgICB9XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGlzT25DYXJ0ZXNpYW4oc2VyaWVzTW9kZWwpIHtcbiAgcmV0dXJuIHNlcmllc01vZGVsLmNvb3JkaW5hdGVTeXN0ZW0gJiYgc2VyaWVzTW9kZWwuY29vcmRpbmF0ZVN5c3RlbS50eXBlID09PSAnY2FydGVzaWFuMmQnO1xufVxuXG5mdW5jdGlvbiBpc0luTGFyZ2VNb2RlKHNlcmllc01vZGVsKSB7XG4gIHJldHVybiBzZXJpZXNNb2RlbC5waXBlbGluZUNvbnRleHQgJiYgc2VyaWVzTW9kZWwucGlwZWxpbmVDb250ZXh0LmxhcmdlO1xufSAvLyBTZWUgY2FzZXMgaW4gYHRlc3QvYmFyLXN0YXJ0Lmh0bWxgIGFuZCBgIzc0MTJgLCBgIzg3NDdgLlxuXG5cbmZ1bmN0aW9uIGdldFZhbHVlQXhpc1N0YXJ0KGJhc2VBeGlzLCB2YWx1ZUF4aXMsIHN0YWNrZWQpIHtcbiAgdmFyIGV4dGVudCA9IHZhbHVlQXhpcy5nZXRHbG9iYWxFeHRlbnQoKTtcbiAgdmFyIG1pbjtcbiAgdmFyIG1heDtcblxuICBpZiAoZXh0ZW50WzBdID4gZXh0ZW50WzFdKSB7XG4gICAgbWluID0gZXh0ZW50WzFdO1xuICAgIG1heCA9IGV4dGVudFswXTtcbiAgfSBlbHNlIHtcbiAgICBtaW4gPSBleHRlbnRbMF07XG4gICAgbWF4ID0gZXh0ZW50WzFdO1xuICB9XG5cbiAgdmFyIHZhbHVlU3RhcnQgPSB2YWx1ZUF4aXMudG9HbG9iYWxDb29yZCh2YWx1ZUF4aXMuZGF0YVRvQ29vcmQoMCkpO1xuICB2YWx1ZVN0YXJ0IDwgbWluICYmICh2YWx1ZVN0YXJ0ID0gbWluKTtcbiAgdmFsdWVTdGFydCA+IG1heCAmJiAodmFsdWVTdGFydCA9IG1heCk7XG4gIHJldHVybiB2YWx1ZVN0YXJ0O1xufVxuXG5leHBvcnRzLmdldExheW91dE9uQXhpcyA9IGdldExheW91dE9uQXhpcztcbmV4cG9ydHMucHJlcGFyZUxheW91dEJhclNlcmllcyA9IHByZXBhcmVMYXlvdXRCYXJTZXJpZXM7XG5leHBvcnRzLm1ha2VDb2x1bW5MYXlvdXQgPSBtYWtlQ29sdW1uTGF5b3V0O1xuZXhwb3J0cy5yZXRyaWV2ZUNvbHVtbkxheW91dCA9IHJldHJpZXZlQ29sdW1uTGF5b3V0O1xuZXhwb3J0cy5sYXlvdXQgPSBsYXlvdXQ7XG5leHBvcnRzLmxhcmdlTGF5b3V0ID0gbGFyZ2VMYXlvdXQ7Il0sIm1hcHBpbmdzIjoiQUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQWpEQTtBQUNBO0FBbURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/layout/barGrid.js
