__webpack_require__.r(__webpack_exports__);
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! immutable */ "./node_modules/immutable/dist/immutable.js");
/* harmony import */ var immutable__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immutable__WEBPACK_IMPORTED_MODULE_0__);
/* eslint new-cap: [2, {capIsNewExceptions: ["Map"]}] */

var offset = Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])();
var mentionStore = {
  offset: Object(immutable__WEBPACK_IMPORTED_MODULE_0__["Map"])(),
  getOffset: function getOffset() {
    return offset;
  },
  getTrigger: function getTrigger(offsetKey) {
    var currentOffset = offset.get(offsetKey);
    return currentOffset && currentOffset.trigger;
  },
  activeSuggestion: function activeSuggestion(_ref) {
    var offsetKey = _ref.offsetKey;
    offset = offset.set(offsetKey, {
      offsetKey: offsetKey
    });
  },
  inActiveSuggestion: function inActiveSuggestion(_ref2) {
    var offsetKey = _ref2.offsetKey;
    offset = offset['delete'](offsetKey);
  },
  updateSuggestion: function updateSuggestion(_ref3) {
    var offsetKey = _ref3.offsetKey,
        position = _ref3.position,
        trigger = _ref3.trigger;
    offset = offset.set(offsetKey, {
      offsetKey: offsetKey,
      position: position,
      trigger: trigger
    });
  }
};
/* harmony default export */ __webpack_exports__["default"] = (mentionStore);//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvbW9kZWwvbWVudGlvblN0b3JlLmpzLmpzIiwic291cmNlcyI6WyIvVXNlcnMvYnl0ZXNmb3JjZS9EZXNrdG9wL2IyYi9ub2RlX21vZHVsZXMvcmMtZWRpdG9yLW1lbnRpb24vZXMvbW9kZWwvbWVudGlvblN0b3JlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludCBuZXctY2FwOiBbMiwge2NhcElzTmV3RXhjZXB0aW9uczogW1wiTWFwXCJdfV0gKi9cbmltcG9ydCB7IE1hcCB9IGZyb20gJ2ltbXV0YWJsZSc7XG5cbnZhciBvZmZzZXQgPSBNYXAoKTtcbnZhciBtZW50aW9uU3RvcmUgPSB7XG4gIG9mZnNldDogTWFwKCksXG4gIGdldE9mZnNldDogZnVuY3Rpb24gZ2V0T2Zmc2V0KCkge1xuICAgIHJldHVybiBvZmZzZXQ7XG4gIH0sXG4gIGdldFRyaWdnZXI6IGZ1bmN0aW9uIGdldFRyaWdnZXIob2Zmc2V0S2V5KSB7XG4gICAgdmFyIGN1cnJlbnRPZmZzZXQgPSBvZmZzZXQuZ2V0KG9mZnNldEtleSk7XG4gICAgcmV0dXJuIGN1cnJlbnRPZmZzZXQgJiYgY3VycmVudE9mZnNldC50cmlnZ2VyO1xuICB9LFxuICBhY3RpdmVTdWdnZXN0aW9uOiBmdW5jdGlvbiBhY3RpdmVTdWdnZXN0aW9uKF9yZWYpIHtcbiAgICB2YXIgb2Zmc2V0S2V5ID0gX3JlZi5vZmZzZXRLZXk7XG5cbiAgICBvZmZzZXQgPSBvZmZzZXQuc2V0KG9mZnNldEtleSwge1xuICAgICAgb2Zmc2V0S2V5OiBvZmZzZXRLZXlcbiAgICB9KTtcbiAgfSxcbiAgaW5BY3RpdmVTdWdnZXN0aW9uOiBmdW5jdGlvbiBpbkFjdGl2ZVN1Z2dlc3Rpb24oX3JlZjIpIHtcbiAgICB2YXIgb2Zmc2V0S2V5ID0gX3JlZjIub2Zmc2V0S2V5O1xuXG4gICAgb2Zmc2V0ID0gb2Zmc2V0WydkZWxldGUnXShvZmZzZXRLZXkpO1xuICB9LFxuICB1cGRhdGVTdWdnZXN0aW9uOiBmdW5jdGlvbiB1cGRhdGVTdWdnZXN0aW9uKF9yZWYzKSB7XG4gICAgdmFyIG9mZnNldEtleSA9IF9yZWYzLm9mZnNldEtleSxcbiAgICAgICAgcG9zaXRpb24gPSBfcmVmMy5wb3NpdGlvbixcbiAgICAgICAgdHJpZ2dlciA9IF9yZWYzLnRyaWdnZXI7XG5cbiAgICBvZmZzZXQgPSBvZmZzZXQuc2V0KG9mZnNldEtleSwge1xuICAgICAgb2Zmc2V0S2V5OiBvZmZzZXRLZXksXG4gICAgICBwb3NpdGlvbjogcG9zaXRpb24sXG4gICAgICB0cmlnZ2VyOiB0cmlnZ2VyXG4gICAgfSk7XG4gIH1cbn07XG5cbmV4cG9ydCBkZWZhdWx0IG1lbnRpb25TdG9yZTsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUEvQkE7QUFrQ0EiLCJzb3VyY2VSb290IjoiIn0=
//# sourceURL=webpack-internal:///./node_modules/rc-editor-mention/es/model/mentionStore.js
