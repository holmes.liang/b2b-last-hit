/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
var zrUtil = __webpack_require__(/*! zrender/lib/core/util */ "./node_modules/zrender/lib/core/util.js");

var graphic = __webpack_require__(/*! ../../util/graphic */ "./node_modules/echarts/lib/util/graphic.js");

var ChartView = __webpack_require__(/*! ../../view/Chart */ "./node_modules/echarts/lib/view/Chart.js");
/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

/**
 * @param {module:echarts/model/Series} seriesModel
 * @param {boolean} hasAnimation
 * @inner
 */


function updateDataSelected(uid, seriesModel, hasAnimation, api) {
  var data = seriesModel.getData();
  var dataIndex = this.dataIndex;
  var name = data.getName(dataIndex);
  var selectedOffset = seriesModel.get('selectedOffset');
  api.dispatchAction({
    type: 'pieToggleSelect',
    from: uid,
    name: name,
    seriesId: seriesModel.id
  });
  data.each(function (idx) {
    toggleItemSelected(data.getItemGraphicEl(idx), data.getItemLayout(idx), seriesModel.isSelected(data.getName(idx)), selectedOffset, hasAnimation);
  });
}
/**
 * @param {module:zrender/graphic/Sector} el
 * @param {Object} layout
 * @param {boolean} isSelected
 * @param {number} selectedOffset
 * @param {boolean} hasAnimation
 * @inner
 */


function toggleItemSelected(el, layout, isSelected, selectedOffset, hasAnimation) {
  var midAngle = (layout.startAngle + layout.endAngle) / 2;
  var dx = Math.cos(midAngle);
  var dy = Math.sin(midAngle);
  var offset = isSelected ? selectedOffset : 0;
  var position = [dx * offset, dy * offset];
  hasAnimation // animateTo will stop revious animation like update transition
  ? el.animate().when(200, {
    position: position
  }).start('bounceOut') : el.attr('position', position);
}
/**
 * Piece of pie including Sector, Label, LabelLine
 * @constructor
 * @extends {module:zrender/graphic/Group}
 */


function PiePiece(data, idx) {
  graphic.Group.call(this);
  var sector = new graphic.Sector({
    z2: 2
  });
  var polyline = new graphic.Polyline();
  var text = new graphic.Text();
  this.add(sector);
  this.add(polyline);
  this.add(text);
  this.updateData(data, idx, true); // Hover to change label and labelLine

  function onEmphasis() {
    polyline.ignore = polyline.hoverIgnore;
    text.ignore = text.hoverIgnore;
  }

  function onNormal() {
    polyline.ignore = polyline.normalIgnore;
    text.ignore = text.normalIgnore;
  }

  this.on('emphasis', onEmphasis).on('normal', onNormal).on('mouseover', onEmphasis).on('mouseout', onNormal);
}

var piePieceProto = PiePiece.prototype;

piePieceProto.updateData = function (data, idx, firstCreate) {
  var sector = this.childAt(0);
  var seriesModel = data.hostModel;
  var itemModel = data.getItemModel(idx);
  var layout = data.getItemLayout(idx);
  var sectorShape = zrUtil.extend({}, layout);
  sectorShape.label = null;

  if (firstCreate) {
    sector.setShape(sectorShape);
    var animationType = seriesModel.getShallow('animationType');

    if (animationType === 'scale') {
      sector.shape.r = layout.r0;
      graphic.initProps(sector, {
        shape: {
          r: layout.r
        }
      }, seriesModel, idx);
    } // Expansion
    else {
        sector.shape.endAngle = layout.startAngle;
        graphic.updateProps(sector, {
          shape: {
            endAngle: layout.endAngle
          }
        }, seriesModel, idx);
      }
  } else {
    graphic.updateProps(sector, {
      shape: sectorShape
    }, seriesModel, idx);
  } // Update common style


  var visualColor = data.getItemVisual(idx, 'color');
  sector.useStyle(zrUtil.defaults({
    lineJoin: 'bevel',
    fill: visualColor
  }, itemModel.getModel('itemStyle').getItemStyle()));
  sector.hoverStyle = itemModel.getModel('emphasis.itemStyle').getItemStyle();
  var cursorStyle = itemModel.getShallow('cursor');
  cursorStyle && sector.attr('cursor', cursorStyle); // Toggle selected

  toggleItemSelected(this, data.getItemLayout(idx), seriesModel.isSelected(null, idx), seriesModel.get('selectedOffset'), seriesModel.get('animation'));

  function onEmphasis() {
    // Sector may has animation of updating data. Force to move to the last frame
    // Or it may stopped on the wrong shape
    sector.stopAnimation(true);
    sector.animateTo({
      shape: {
        r: layout.r + seriesModel.get('hoverOffset')
      }
    }, 300, 'elasticOut');
  }

  function onNormal() {
    sector.stopAnimation(true);
    sector.animateTo({
      shape: {
        r: layout.r
      }
    }, 300, 'elasticOut');
  }

  sector.off('mouseover').off('mouseout').off('emphasis').off('normal');

  if (itemModel.get('hoverAnimation') && seriesModel.isAnimationEnabled()) {
    sector.on('mouseover', onEmphasis).on('mouseout', onNormal).on('emphasis', onEmphasis).on('normal', onNormal);
  }

  this._updateLabel(data, idx);

  graphic.setHoverStyle(this);
};

piePieceProto._updateLabel = function (data, idx) {
  var labelLine = this.childAt(1);
  var labelText = this.childAt(2);
  var seriesModel = data.hostModel;
  var itemModel = data.getItemModel(idx);
  var layout = data.getItemLayout(idx);
  var labelLayout = layout.label;
  var visualColor = data.getItemVisual(idx, 'color');
  graphic.updateProps(labelLine, {
    shape: {
      points: labelLayout.linePoints || [[labelLayout.x, labelLayout.y], [labelLayout.x, labelLayout.y], [labelLayout.x, labelLayout.y]]
    }
  }, seriesModel, idx);
  graphic.updateProps(labelText, {
    style: {
      x: labelLayout.x,
      y: labelLayout.y
    }
  }, seriesModel, idx);
  labelText.attr({
    rotation: labelLayout.rotation,
    origin: [labelLayout.x, labelLayout.y],
    z2: 10
  });
  var labelModel = itemModel.getModel('label');
  var labelHoverModel = itemModel.getModel('emphasis.label');
  var labelLineModel = itemModel.getModel('labelLine');
  var labelLineHoverModel = itemModel.getModel('emphasis.labelLine');
  var visualColor = data.getItemVisual(idx, 'color');
  graphic.setLabelStyle(labelText.style, labelText.hoverStyle = {}, labelModel, labelHoverModel, {
    labelFetcher: data.hostModel,
    labelDataIndex: idx,
    defaultText: data.getName(idx),
    autoColor: visualColor,
    useInsideStyle: !!labelLayout.inside
  }, {
    textAlign: labelLayout.textAlign,
    textVerticalAlign: labelLayout.verticalAlign,
    opacity: data.getItemVisual(idx, 'opacity')
  });
  labelText.ignore = labelText.normalIgnore = !labelModel.get('show');
  labelText.hoverIgnore = !labelHoverModel.get('show');
  labelLine.ignore = labelLine.normalIgnore = !labelLineModel.get('show');
  labelLine.hoverIgnore = !labelLineHoverModel.get('show'); // Default use item visual color

  labelLine.setStyle({
    stroke: visualColor,
    opacity: data.getItemVisual(idx, 'opacity')
  });
  labelLine.setStyle(labelLineModel.getModel('lineStyle').getLineStyle());
  labelLine.hoverStyle = labelLineHoverModel.getModel('lineStyle').getLineStyle();
  var smooth = labelLineModel.get('smooth');

  if (smooth && smooth === true) {
    smooth = 0.4;
  }

  labelLine.setShape({
    smooth: smooth
  });
};

zrUtil.inherits(PiePiece, graphic.Group); // Pie view

var PieView = ChartView.extend({
  type: 'pie',
  init: function init() {
    var sectorGroup = new graphic.Group();
    this._sectorGroup = sectorGroup;
  },
  render: function render(seriesModel, ecModel, api, payload) {
    if (payload && payload.from === this.uid) {
      return;
    }

    var data = seriesModel.getData();
    var oldData = this._data;
    var group = this.group;
    var hasAnimation = ecModel.get('animation');
    var isFirstRender = !oldData;
    var animationType = seriesModel.get('animationType');
    var onSectorClick = zrUtil.curry(updateDataSelected, this.uid, seriesModel, hasAnimation, api);
    var selectedMode = seriesModel.get('selectedMode');
    data.diff(oldData).add(function (idx) {
      var piePiece = new PiePiece(data, idx); // Default expansion animation

      if (isFirstRender && animationType !== 'scale') {
        piePiece.eachChild(function (child) {
          child.stopAnimation(true);
        });
      }

      selectedMode && piePiece.on('click', onSectorClick);
      data.setItemGraphicEl(idx, piePiece);
      group.add(piePiece);
    }).update(function (newIdx, oldIdx) {
      var piePiece = oldData.getItemGraphicEl(oldIdx);
      piePiece.updateData(data, newIdx);
      piePiece.off('click');
      selectedMode && piePiece.on('click', onSectorClick);
      group.add(piePiece);
      data.setItemGraphicEl(newIdx, piePiece);
    }).remove(function (idx) {
      var piePiece = oldData.getItemGraphicEl(idx);
      group.remove(piePiece);
    }).execute();

    if (hasAnimation && isFirstRender && data.count() > 0 // Default expansion animation
    && animationType !== 'scale') {
      var shape = data.getItemLayout(0);
      var r = Math.max(api.getWidth(), api.getHeight()) / 2;
      var removeClipPath = zrUtil.bind(group.removeClipPath, group);
      group.setClipPath(this._createClipPath(shape.cx, shape.cy, r, shape.startAngle, shape.clockwise, removeClipPath, seriesModel));
    } else {
      // clipPath is used in first-time animation, so remove it when otherwise. See: #8994
      group.removeClipPath();
    }

    this._data = data;
  },
  dispose: function dispose() {},
  _createClipPath: function _createClipPath(cx, cy, r, startAngle, clockwise, cb, seriesModel) {
    var clipPath = new graphic.Sector({
      shape: {
        cx: cx,
        cy: cy,
        r0: 0,
        r: r,
        startAngle: startAngle,
        endAngle: startAngle,
        clockwise: clockwise
      }
    });
    graphic.initProps(clipPath, {
      shape: {
        endAngle: startAngle + (clockwise ? 1 : -1) * Math.PI * 2
      }
    }, seriesModel, cb);
    return clipPath;
  },

  /**
   * @implement
   */
  containPoint: function containPoint(point, seriesModel) {
    var data = seriesModel.getData();
    var itemLayout = data.getItemLayout(0);

    if (itemLayout) {
      var dx = point[0] - itemLayout.cx;
      var dy = point[1] - itemLayout.cy;
      var radius = Math.sqrt(dx * dx + dy * dy);
      return radius <= itemLayout.r && radius >= itemLayout.r0;
    }
  }
});
var _default = PieView;
module.exports = _default;//# sourceURL=[module]
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvZWNoYXJ0cy9saWIvY2hhcnQvcGllL1BpZVZpZXcuanMuanMiLCJzb3VyY2VzIjpbIi9Vc2Vycy9ieXRlc2ZvcmNlL0Rlc2t0b3AvYjJiL25vZGVfbW9kdWxlcy9lY2hhcnRzL2xpYi9jaGFydC9waWUvUGllVmlldy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJcbi8qXG4qIExpY2Vuc2VkIHRvIHRoZSBBcGFjaGUgU29mdHdhcmUgRm91bmRhdGlvbiAoQVNGKSB1bmRlciBvbmVcbiogb3IgbW9yZSBjb250cmlidXRvciBsaWNlbnNlIGFncmVlbWVudHMuICBTZWUgdGhlIE5PVElDRSBmaWxlXG4qIGRpc3RyaWJ1dGVkIHdpdGggdGhpcyB3b3JrIGZvciBhZGRpdGlvbmFsIGluZm9ybWF0aW9uXG4qIHJlZ2FyZGluZyBjb3B5cmlnaHQgb3duZXJzaGlwLiAgVGhlIEFTRiBsaWNlbnNlcyB0aGlzIGZpbGVcbiogdG8geW91IHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZVxuKiBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Vcbiogd2l0aCB0aGUgTGljZW5zZS4gIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuKlxuKiAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuKlxuKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsXG4qIHNvZnR3YXJlIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuXG4qIFwiQVMgSVNcIiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXG4qIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuICBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZVxuKiBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kIGxpbWl0YXRpb25zXG4qIHVuZGVyIHRoZSBMaWNlbnNlLlxuKi9cblxudmFyIHpyVXRpbCA9IHJlcXVpcmUoXCJ6cmVuZGVyL2xpYi9jb3JlL3V0aWxcIik7XG5cbnZhciBncmFwaGljID0gcmVxdWlyZShcIi4uLy4uL3V0aWwvZ3JhcGhpY1wiKTtcblxudmFyIENoYXJ0VmlldyA9IHJlcXVpcmUoXCIuLi8uLi92aWV3L0NoYXJ0XCIpO1xuXG4vKlxuKiBMaWNlbnNlZCB0byB0aGUgQXBhY2hlIFNvZnR3YXJlIEZvdW5kYXRpb24gKEFTRikgdW5kZXIgb25lXG4qIG9yIG1vcmUgY29udHJpYnV0b3IgbGljZW5zZSBhZ3JlZW1lbnRzLiAgU2VlIHRoZSBOT1RJQ0UgZmlsZVxuKiBkaXN0cmlidXRlZCB3aXRoIHRoaXMgd29yayBmb3IgYWRkaXRpb25hbCBpbmZvcm1hdGlvblxuKiByZWdhcmRpbmcgY29weXJpZ2h0IG93bmVyc2hpcC4gIFRoZSBBU0YgbGljZW5zZXMgdGhpcyBmaWxlXG4qIHRvIHlvdSB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGVcbiogXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlXG4qIHdpdGggdGhlIExpY2Vuc2UuICBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbipcbiogICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbipcbiogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLFxuKiBzb2Z0d2FyZSBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhblxuKiBcIkFTIElTXCIgQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxuKiBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLiAgU2VlIHRoZSBMaWNlbnNlIGZvciB0aGVcbiogc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZCBsaW1pdGF0aW9uc1xuKiB1bmRlciB0aGUgTGljZW5zZS5cbiovXG5cbi8qKlxuICogQHBhcmFtIHttb2R1bGU6ZWNoYXJ0cy9tb2RlbC9TZXJpZXN9IHNlcmllc01vZGVsXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGhhc0FuaW1hdGlvblxuICogQGlubmVyXG4gKi9cbmZ1bmN0aW9uIHVwZGF0ZURhdGFTZWxlY3RlZCh1aWQsIHNlcmllc01vZGVsLCBoYXNBbmltYXRpb24sIGFwaSkge1xuICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgdmFyIGRhdGFJbmRleCA9IHRoaXMuZGF0YUluZGV4O1xuICB2YXIgbmFtZSA9IGRhdGEuZ2V0TmFtZShkYXRhSW5kZXgpO1xuICB2YXIgc2VsZWN0ZWRPZmZzZXQgPSBzZXJpZXNNb2RlbC5nZXQoJ3NlbGVjdGVkT2Zmc2V0Jyk7XG4gIGFwaS5kaXNwYXRjaEFjdGlvbih7XG4gICAgdHlwZTogJ3BpZVRvZ2dsZVNlbGVjdCcsXG4gICAgZnJvbTogdWlkLFxuICAgIG5hbWU6IG5hbWUsXG4gICAgc2VyaWVzSWQ6IHNlcmllc01vZGVsLmlkXG4gIH0pO1xuICBkYXRhLmVhY2goZnVuY3Rpb24gKGlkeCkge1xuICAgIHRvZ2dsZUl0ZW1TZWxlY3RlZChkYXRhLmdldEl0ZW1HcmFwaGljRWwoaWR4KSwgZGF0YS5nZXRJdGVtTGF5b3V0KGlkeCksIHNlcmllc01vZGVsLmlzU2VsZWN0ZWQoZGF0YS5nZXROYW1lKGlkeCkpLCBzZWxlY3RlZE9mZnNldCwgaGFzQW5pbWF0aW9uKTtcbiAgfSk7XG59XG4vKipcbiAqIEBwYXJhbSB7bW9kdWxlOnpyZW5kZXIvZ3JhcGhpYy9TZWN0b3J9IGVsXG4gKiBAcGFyYW0ge09iamVjdH0gbGF5b3V0XG4gKiBAcGFyYW0ge2Jvb2xlYW59IGlzU2VsZWN0ZWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBzZWxlY3RlZE9mZnNldFxuICogQHBhcmFtIHtib29sZWFufSBoYXNBbmltYXRpb25cbiAqIEBpbm5lclxuICovXG5cblxuZnVuY3Rpb24gdG9nZ2xlSXRlbVNlbGVjdGVkKGVsLCBsYXlvdXQsIGlzU2VsZWN0ZWQsIHNlbGVjdGVkT2Zmc2V0LCBoYXNBbmltYXRpb24pIHtcbiAgdmFyIG1pZEFuZ2xlID0gKGxheW91dC5zdGFydEFuZ2xlICsgbGF5b3V0LmVuZEFuZ2xlKSAvIDI7XG4gIHZhciBkeCA9IE1hdGguY29zKG1pZEFuZ2xlKTtcbiAgdmFyIGR5ID0gTWF0aC5zaW4obWlkQW5nbGUpO1xuICB2YXIgb2Zmc2V0ID0gaXNTZWxlY3RlZCA/IHNlbGVjdGVkT2Zmc2V0IDogMDtcbiAgdmFyIHBvc2l0aW9uID0gW2R4ICogb2Zmc2V0LCBkeSAqIG9mZnNldF07XG4gIGhhc0FuaW1hdGlvbiAvLyBhbmltYXRlVG8gd2lsbCBzdG9wIHJldmlvdXMgYW5pbWF0aW9uIGxpa2UgdXBkYXRlIHRyYW5zaXRpb25cbiAgPyBlbC5hbmltYXRlKCkud2hlbigyMDAsIHtcbiAgICBwb3NpdGlvbjogcG9zaXRpb25cbiAgfSkuc3RhcnQoJ2JvdW5jZU91dCcpIDogZWwuYXR0cigncG9zaXRpb24nLCBwb3NpdGlvbik7XG59XG4vKipcbiAqIFBpZWNlIG9mIHBpZSBpbmNsdWRpbmcgU2VjdG9yLCBMYWJlbCwgTGFiZWxMaW5lXG4gKiBAY29uc3RydWN0b3JcbiAqIEBleHRlbmRzIHttb2R1bGU6enJlbmRlci9ncmFwaGljL0dyb3VwfVxuICovXG5cblxuZnVuY3Rpb24gUGllUGllY2UoZGF0YSwgaWR4KSB7XG4gIGdyYXBoaWMuR3JvdXAuY2FsbCh0aGlzKTtcbiAgdmFyIHNlY3RvciA9IG5ldyBncmFwaGljLlNlY3Rvcih7XG4gICAgejI6IDJcbiAgfSk7XG4gIHZhciBwb2x5bGluZSA9IG5ldyBncmFwaGljLlBvbHlsaW5lKCk7XG4gIHZhciB0ZXh0ID0gbmV3IGdyYXBoaWMuVGV4dCgpO1xuICB0aGlzLmFkZChzZWN0b3IpO1xuICB0aGlzLmFkZChwb2x5bGluZSk7XG4gIHRoaXMuYWRkKHRleHQpO1xuICB0aGlzLnVwZGF0ZURhdGEoZGF0YSwgaWR4LCB0cnVlKTsgLy8gSG92ZXIgdG8gY2hhbmdlIGxhYmVsIGFuZCBsYWJlbExpbmVcblxuICBmdW5jdGlvbiBvbkVtcGhhc2lzKCkge1xuICAgIHBvbHlsaW5lLmlnbm9yZSA9IHBvbHlsaW5lLmhvdmVySWdub3JlO1xuICAgIHRleHQuaWdub3JlID0gdGV4dC5ob3Zlcklnbm9yZTtcbiAgfVxuXG4gIGZ1bmN0aW9uIG9uTm9ybWFsKCkge1xuICAgIHBvbHlsaW5lLmlnbm9yZSA9IHBvbHlsaW5lLm5vcm1hbElnbm9yZTtcbiAgICB0ZXh0Lmlnbm9yZSA9IHRleHQubm9ybWFsSWdub3JlO1xuICB9XG5cbiAgdGhpcy5vbignZW1waGFzaXMnLCBvbkVtcGhhc2lzKS5vbignbm9ybWFsJywgb25Ob3JtYWwpLm9uKCdtb3VzZW92ZXInLCBvbkVtcGhhc2lzKS5vbignbW91c2VvdXQnLCBvbk5vcm1hbCk7XG59XG5cbnZhciBwaWVQaWVjZVByb3RvID0gUGllUGllY2UucHJvdG90eXBlO1xuXG5waWVQaWVjZVByb3RvLnVwZGF0ZURhdGEgPSBmdW5jdGlvbiAoZGF0YSwgaWR4LCBmaXJzdENyZWF0ZSkge1xuICB2YXIgc2VjdG9yID0gdGhpcy5jaGlsZEF0KDApO1xuICB2YXIgc2VyaWVzTW9kZWwgPSBkYXRhLmhvc3RNb2RlbDtcbiAgdmFyIGl0ZW1Nb2RlbCA9IGRhdGEuZ2V0SXRlbU1vZGVsKGlkeCk7XG4gIHZhciBsYXlvdXQgPSBkYXRhLmdldEl0ZW1MYXlvdXQoaWR4KTtcbiAgdmFyIHNlY3RvclNoYXBlID0genJVdGlsLmV4dGVuZCh7fSwgbGF5b3V0KTtcbiAgc2VjdG9yU2hhcGUubGFiZWwgPSBudWxsO1xuXG4gIGlmIChmaXJzdENyZWF0ZSkge1xuICAgIHNlY3Rvci5zZXRTaGFwZShzZWN0b3JTaGFwZSk7XG4gICAgdmFyIGFuaW1hdGlvblR5cGUgPSBzZXJpZXNNb2RlbC5nZXRTaGFsbG93KCdhbmltYXRpb25UeXBlJyk7XG5cbiAgICBpZiAoYW5pbWF0aW9uVHlwZSA9PT0gJ3NjYWxlJykge1xuICAgICAgc2VjdG9yLnNoYXBlLnIgPSBsYXlvdXQucjA7XG4gICAgICBncmFwaGljLmluaXRQcm9wcyhzZWN0b3IsIHtcbiAgICAgICAgc2hhcGU6IHtcbiAgICAgICAgICByOiBsYXlvdXQuclxuICAgICAgICB9XG4gICAgICB9LCBzZXJpZXNNb2RlbCwgaWR4KTtcbiAgICB9IC8vIEV4cGFuc2lvblxuICAgIGVsc2Uge1xuICAgICAgICBzZWN0b3Iuc2hhcGUuZW5kQW5nbGUgPSBsYXlvdXQuc3RhcnRBbmdsZTtcbiAgICAgICAgZ3JhcGhpYy51cGRhdGVQcm9wcyhzZWN0b3IsIHtcbiAgICAgICAgICBzaGFwZToge1xuICAgICAgICAgICAgZW5kQW5nbGU6IGxheW91dC5lbmRBbmdsZVxuICAgICAgICAgIH1cbiAgICAgICAgfSwgc2VyaWVzTW9kZWwsIGlkeCk7XG4gICAgICB9XG4gIH0gZWxzZSB7XG4gICAgZ3JhcGhpYy51cGRhdGVQcm9wcyhzZWN0b3IsIHtcbiAgICAgIHNoYXBlOiBzZWN0b3JTaGFwZVxuICAgIH0sIHNlcmllc01vZGVsLCBpZHgpO1xuICB9IC8vIFVwZGF0ZSBjb21tb24gc3R5bGVcblxuXG4gIHZhciB2aXN1YWxDb2xvciA9IGRhdGEuZ2V0SXRlbVZpc3VhbChpZHgsICdjb2xvcicpO1xuICBzZWN0b3IudXNlU3R5bGUoenJVdGlsLmRlZmF1bHRzKHtcbiAgICBsaW5lSm9pbjogJ2JldmVsJyxcbiAgICBmaWxsOiB2aXN1YWxDb2xvclxuICB9LCBpdGVtTW9kZWwuZ2V0TW9kZWwoJ2l0ZW1TdHlsZScpLmdldEl0ZW1TdHlsZSgpKSk7XG4gIHNlY3Rvci5ob3ZlclN0eWxlID0gaXRlbU1vZGVsLmdldE1vZGVsKCdlbXBoYXNpcy5pdGVtU3R5bGUnKS5nZXRJdGVtU3R5bGUoKTtcbiAgdmFyIGN1cnNvclN0eWxlID0gaXRlbU1vZGVsLmdldFNoYWxsb3coJ2N1cnNvcicpO1xuICBjdXJzb3JTdHlsZSAmJiBzZWN0b3IuYXR0cignY3Vyc29yJywgY3Vyc29yU3R5bGUpOyAvLyBUb2dnbGUgc2VsZWN0ZWRcblxuICB0b2dnbGVJdGVtU2VsZWN0ZWQodGhpcywgZGF0YS5nZXRJdGVtTGF5b3V0KGlkeCksIHNlcmllc01vZGVsLmlzU2VsZWN0ZWQobnVsbCwgaWR4KSwgc2VyaWVzTW9kZWwuZ2V0KCdzZWxlY3RlZE9mZnNldCcpLCBzZXJpZXNNb2RlbC5nZXQoJ2FuaW1hdGlvbicpKTtcblxuICBmdW5jdGlvbiBvbkVtcGhhc2lzKCkge1xuICAgIC8vIFNlY3RvciBtYXkgaGFzIGFuaW1hdGlvbiBvZiB1cGRhdGluZyBkYXRhLiBGb3JjZSB0byBtb3ZlIHRvIHRoZSBsYXN0IGZyYW1lXG4gICAgLy8gT3IgaXQgbWF5IHN0b3BwZWQgb24gdGhlIHdyb25nIHNoYXBlXG4gICAgc2VjdG9yLnN0b3BBbmltYXRpb24odHJ1ZSk7XG4gICAgc2VjdG9yLmFuaW1hdGVUbyh7XG4gICAgICBzaGFwZToge1xuICAgICAgICByOiBsYXlvdXQuciArIHNlcmllc01vZGVsLmdldCgnaG92ZXJPZmZzZXQnKVxuICAgICAgfVxuICAgIH0sIDMwMCwgJ2VsYXN0aWNPdXQnKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIG9uTm9ybWFsKCkge1xuICAgIHNlY3Rvci5zdG9wQW5pbWF0aW9uKHRydWUpO1xuICAgIHNlY3Rvci5hbmltYXRlVG8oe1xuICAgICAgc2hhcGU6IHtcbiAgICAgICAgcjogbGF5b3V0LnJcbiAgICAgIH1cbiAgICB9LCAzMDAsICdlbGFzdGljT3V0Jyk7XG4gIH1cblxuICBzZWN0b3Iub2ZmKCdtb3VzZW92ZXInKS5vZmYoJ21vdXNlb3V0Jykub2ZmKCdlbXBoYXNpcycpLm9mZignbm9ybWFsJyk7XG5cbiAgaWYgKGl0ZW1Nb2RlbC5nZXQoJ2hvdmVyQW5pbWF0aW9uJykgJiYgc2VyaWVzTW9kZWwuaXNBbmltYXRpb25FbmFibGVkKCkpIHtcbiAgICBzZWN0b3Iub24oJ21vdXNlb3ZlcicsIG9uRW1waGFzaXMpLm9uKCdtb3VzZW91dCcsIG9uTm9ybWFsKS5vbignZW1waGFzaXMnLCBvbkVtcGhhc2lzKS5vbignbm9ybWFsJywgb25Ob3JtYWwpO1xuICB9XG5cbiAgdGhpcy5fdXBkYXRlTGFiZWwoZGF0YSwgaWR4KTtcblxuICBncmFwaGljLnNldEhvdmVyU3R5bGUodGhpcyk7XG59O1xuXG5waWVQaWVjZVByb3RvLl91cGRhdGVMYWJlbCA9IGZ1bmN0aW9uIChkYXRhLCBpZHgpIHtcbiAgdmFyIGxhYmVsTGluZSA9IHRoaXMuY2hpbGRBdCgxKTtcbiAgdmFyIGxhYmVsVGV4dCA9IHRoaXMuY2hpbGRBdCgyKTtcbiAgdmFyIHNlcmllc01vZGVsID0gZGF0YS5ob3N0TW9kZWw7XG4gIHZhciBpdGVtTW9kZWwgPSBkYXRhLmdldEl0ZW1Nb2RlbChpZHgpO1xuICB2YXIgbGF5b3V0ID0gZGF0YS5nZXRJdGVtTGF5b3V0KGlkeCk7XG4gIHZhciBsYWJlbExheW91dCA9IGxheW91dC5sYWJlbDtcbiAgdmFyIHZpc3VhbENvbG9yID0gZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ2NvbG9yJyk7XG4gIGdyYXBoaWMudXBkYXRlUHJvcHMobGFiZWxMaW5lLCB7XG4gICAgc2hhcGU6IHtcbiAgICAgIHBvaW50czogbGFiZWxMYXlvdXQubGluZVBvaW50cyB8fCBbW2xhYmVsTGF5b3V0LngsIGxhYmVsTGF5b3V0LnldLCBbbGFiZWxMYXlvdXQueCwgbGFiZWxMYXlvdXQueV0sIFtsYWJlbExheW91dC54LCBsYWJlbExheW91dC55XV1cbiAgICB9XG4gIH0sIHNlcmllc01vZGVsLCBpZHgpO1xuICBncmFwaGljLnVwZGF0ZVByb3BzKGxhYmVsVGV4dCwge1xuICAgIHN0eWxlOiB7XG4gICAgICB4OiBsYWJlbExheW91dC54LFxuICAgICAgeTogbGFiZWxMYXlvdXQueVxuICAgIH1cbiAgfSwgc2VyaWVzTW9kZWwsIGlkeCk7XG4gIGxhYmVsVGV4dC5hdHRyKHtcbiAgICByb3RhdGlvbjogbGFiZWxMYXlvdXQucm90YXRpb24sXG4gICAgb3JpZ2luOiBbbGFiZWxMYXlvdXQueCwgbGFiZWxMYXlvdXQueV0sXG4gICAgejI6IDEwXG4gIH0pO1xuICB2YXIgbGFiZWxNb2RlbCA9IGl0ZW1Nb2RlbC5nZXRNb2RlbCgnbGFiZWwnKTtcbiAgdmFyIGxhYmVsSG92ZXJNb2RlbCA9IGl0ZW1Nb2RlbC5nZXRNb2RlbCgnZW1waGFzaXMubGFiZWwnKTtcbiAgdmFyIGxhYmVsTGluZU1vZGVsID0gaXRlbU1vZGVsLmdldE1vZGVsKCdsYWJlbExpbmUnKTtcbiAgdmFyIGxhYmVsTGluZUhvdmVyTW9kZWwgPSBpdGVtTW9kZWwuZ2V0TW9kZWwoJ2VtcGhhc2lzLmxhYmVsTGluZScpO1xuICB2YXIgdmlzdWFsQ29sb3IgPSBkYXRhLmdldEl0ZW1WaXN1YWwoaWR4LCAnY29sb3InKTtcbiAgZ3JhcGhpYy5zZXRMYWJlbFN0eWxlKGxhYmVsVGV4dC5zdHlsZSwgbGFiZWxUZXh0LmhvdmVyU3R5bGUgPSB7fSwgbGFiZWxNb2RlbCwgbGFiZWxIb3Zlck1vZGVsLCB7XG4gICAgbGFiZWxGZXRjaGVyOiBkYXRhLmhvc3RNb2RlbCxcbiAgICBsYWJlbERhdGFJbmRleDogaWR4LFxuICAgIGRlZmF1bHRUZXh0OiBkYXRhLmdldE5hbWUoaWR4KSxcbiAgICBhdXRvQ29sb3I6IHZpc3VhbENvbG9yLFxuICAgIHVzZUluc2lkZVN0eWxlOiAhIWxhYmVsTGF5b3V0Lmluc2lkZVxuICB9LCB7XG4gICAgdGV4dEFsaWduOiBsYWJlbExheW91dC50ZXh0QWxpZ24sXG4gICAgdGV4dFZlcnRpY2FsQWxpZ246IGxhYmVsTGF5b3V0LnZlcnRpY2FsQWxpZ24sXG4gICAgb3BhY2l0eTogZGF0YS5nZXRJdGVtVmlzdWFsKGlkeCwgJ29wYWNpdHknKVxuICB9KTtcbiAgbGFiZWxUZXh0Lmlnbm9yZSA9IGxhYmVsVGV4dC5ub3JtYWxJZ25vcmUgPSAhbGFiZWxNb2RlbC5nZXQoJ3Nob3cnKTtcbiAgbGFiZWxUZXh0LmhvdmVySWdub3JlID0gIWxhYmVsSG92ZXJNb2RlbC5nZXQoJ3Nob3cnKTtcbiAgbGFiZWxMaW5lLmlnbm9yZSA9IGxhYmVsTGluZS5ub3JtYWxJZ25vcmUgPSAhbGFiZWxMaW5lTW9kZWwuZ2V0KCdzaG93Jyk7XG4gIGxhYmVsTGluZS5ob3Zlcklnbm9yZSA9ICFsYWJlbExpbmVIb3Zlck1vZGVsLmdldCgnc2hvdycpOyAvLyBEZWZhdWx0IHVzZSBpdGVtIHZpc3VhbCBjb2xvclxuXG4gIGxhYmVsTGluZS5zZXRTdHlsZSh7XG4gICAgc3Ryb2tlOiB2aXN1YWxDb2xvcixcbiAgICBvcGFjaXR5OiBkYXRhLmdldEl0ZW1WaXN1YWwoaWR4LCAnb3BhY2l0eScpXG4gIH0pO1xuICBsYWJlbExpbmUuc2V0U3R5bGUobGFiZWxMaW5lTW9kZWwuZ2V0TW9kZWwoJ2xpbmVTdHlsZScpLmdldExpbmVTdHlsZSgpKTtcbiAgbGFiZWxMaW5lLmhvdmVyU3R5bGUgPSBsYWJlbExpbmVIb3Zlck1vZGVsLmdldE1vZGVsKCdsaW5lU3R5bGUnKS5nZXRMaW5lU3R5bGUoKTtcbiAgdmFyIHNtb290aCA9IGxhYmVsTGluZU1vZGVsLmdldCgnc21vb3RoJyk7XG5cbiAgaWYgKHNtb290aCAmJiBzbW9vdGggPT09IHRydWUpIHtcbiAgICBzbW9vdGggPSAwLjQ7XG4gIH1cblxuICBsYWJlbExpbmUuc2V0U2hhcGUoe1xuICAgIHNtb290aDogc21vb3RoXG4gIH0pO1xufTtcblxuenJVdGlsLmluaGVyaXRzKFBpZVBpZWNlLCBncmFwaGljLkdyb3VwKTsgLy8gUGllIHZpZXdcblxudmFyIFBpZVZpZXcgPSBDaGFydFZpZXcuZXh0ZW5kKHtcbiAgdHlwZTogJ3BpZScsXG4gIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgc2VjdG9yR3JvdXAgPSBuZXcgZ3JhcGhpYy5Hcm91cCgpO1xuICAgIHRoaXMuX3NlY3Rvckdyb3VwID0gc2VjdG9yR3JvdXA7XG4gIH0sXG4gIHJlbmRlcjogZnVuY3Rpb24gKHNlcmllc01vZGVsLCBlY01vZGVsLCBhcGksIHBheWxvYWQpIHtcbiAgICBpZiAocGF5bG9hZCAmJiBwYXlsb2FkLmZyb20gPT09IHRoaXMudWlkKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGRhdGEgPSBzZXJpZXNNb2RlbC5nZXREYXRhKCk7XG4gICAgdmFyIG9sZERhdGEgPSB0aGlzLl9kYXRhO1xuICAgIHZhciBncm91cCA9IHRoaXMuZ3JvdXA7XG4gICAgdmFyIGhhc0FuaW1hdGlvbiA9IGVjTW9kZWwuZ2V0KCdhbmltYXRpb24nKTtcbiAgICB2YXIgaXNGaXJzdFJlbmRlciA9ICFvbGREYXRhO1xuICAgIHZhciBhbmltYXRpb25UeXBlID0gc2VyaWVzTW9kZWwuZ2V0KCdhbmltYXRpb25UeXBlJyk7XG4gICAgdmFyIG9uU2VjdG9yQ2xpY2sgPSB6clV0aWwuY3VycnkodXBkYXRlRGF0YVNlbGVjdGVkLCB0aGlzLnVpZCwgc2VyaWVzTW9kZWwsIGhhc0FuaW1hdGlvbiwgYXBpKTtcbiAgICB2YXIgc2VsZWN0ZWRNb2RlID0gc2VyaWVzTW9kZWwuZ2V0KCdzZWxlY3RlZE1vZGUnKTtcbiAgICBkYXRhLmRpZmYob2xkRGF0YSkuYWRkKGZ1bmN0aW9uIChpZHgpIHtcbiAgICAgIHZhciBwaWVQaWVjZSA9IG5ldyBQaWVQaWVjZShkYXRhLCBpZHgpOyAvLyBEZWZhdWx0IGV4cGFuc2lvbiBhbmltYXRpb25cblxuICAgICAgaWYgKGlzRmlyc3RSZW5kZXIgJiYgYW5pbWF0aW9uVHlwZSAhPT0gJ3NjYWxlJykge1xuICAgICAgICBwaWVQaWVjZS5lYWNoQ2hpbGQoZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgY2hpbGQuc3RvcEFuaW1hdGlvbih0cnVlKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIHNlbGVjdGVkTW9kZSAmJiBwaWVQaWVjZS5vbignY2xpY2snLCBvblNlY3RvckNsaWNrKTtcbiAgICAgIGRhdGEuc2V0SXRlbUdyYXBoaWNFbChpZHgsIHBpZVBpZWNlKTtcbiAgICAgIGdyb3VwLmFkZChwaWVQaWVjZSk7XG4gICAgfSkudXBkYXRlKGZ1bmN0aW9uIChuZXdJZHgsIG9sZElkeCkge1xuICAgICAgdmFyIHBpZVBpZWNlID0gb2xkRGF0YS5nZXRJdGVtR3JhcGhpY0VsKG9sZElkeCk7XG4gICAgICBwaWVQaWVjZS51cGRhdGVEYXRhKGRhdGEsIG5ld0lkeCk7XG4gICAgICBwaWVQaWVjZS5vZmYoJ2NsaWNrJyk7XG4gICAgICBzZWxlY3RlZE1vZGUgJiYgcGllUGllY2Uub24oJ2NsaWNrJywgb25TZWN0b3JDbGljayk7XG4gICAgICBncm91cC5hZGQocGllUGllY2UpO1xuICAgICAgZGF0YS5zZXRJdGVtR3JhcGhpY0VsKG5ld0lkeCwgcGllUGllY2UpO1xuICAgIH0pLnJlbW92ZShmdW5jdGlvbiAoaWR4KSB7XG4gICAgICB2YXIgcGllUGllY2UgPSBvbGREYXRhLmdldEl0ZW1HcmFwaGljRWwoaWR4KTtcbiAgICAgIGdyb3VwLnJlbW92ZShwaWVQaWVjZSk7XG4gICAgfSkuZXhlY3V0ZSgpO1xuXG4gICAgaWYgKGhhc0FuaW1hdGlvbiAmJiBpc0ZpcnN0UmVuZGVyICYmIGRhdGEuY291bnQoKSA+IDAgLy8gRGVmYXVsdCBleHBhbnNpb24gYW5pbWF0aW9uXG4gICAgJiYgYW5pbWF0aW9uVHlwZSAhPT0gJ3NjYWxlJykge1xuICAgICAgdmFyIHNoYXBlID0gZGF0YS5nZXRJdGVtTGF5b3V0KDApO1xuICAgICAgdmFyIHIgPSBNYXRoLm1heChhcGkuZ2V0V2lkdGgoKSwgYXBpLmdldEhlaWdodCgpKSAvIDI7XG4gICAgICB2YXIgcmVtb3ZlQ2xpcFBhdGggPSB6clV0aWwuYmluZChncm91cC5yZW1vdmVDbGlwUGF0aCwgZ3JvdXApO1xuICAgICAgZ3JvdXAuc2V0Q2xpcFBhdGgodGhpcy5fY3JlYXRlQ2xpcFBhdGgoc2hhcGUuY3gsIHNoYXBlLmN5LCByLCBzaGFwZS5zdGFydEFuZ2xlLCBzaGFwZS5jbG9ja3dpc2UsIHJlbW92ZUNsaXBQYXRoLCBzZXJpZXNNb2RlbCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBjbGlwUGF0aCBpcyB1c2VkIGluIGZpcnN0LXRpbWUgYW5pbWF0aW9uLCBzbyByZW1vdmUgaXQgd2hlbiBvdGhlcndpc2UuIFNlZTogIzg5OTRcbiAgICAgIGdyb3VwLnJlbW92ZUNsaXBQYXRoKCk7XG4gICAgfVxuXG4gICAgdGhpcy5fZGF0YSA9IGRhdGE7XG4gIH0sXG4gIGRpc3Bvc2U6IGZ1bmN0aW9uICgpIHt9LFxuICBfY3JlYXRlQ2xpcFBhdGg6IGZ1bmN0aW9uIChjeCwgY3ksIHIsIHN0YXJ0QW5nbGUsIGNsb2Nrd2lzZSwgY2IsIHNlcmllc01vZGVsKSB7XG4gICAgdmFyIGNsaXBQYXRoID0gbmV3IGdyYXBoaWMuU2VjdG9yKHtcbiAgICAgIHNoYXBlOiB7XG4gICAgICAgIGN4OiBjeCxcbiAgICAgICAgY3k6IGN5LFxuICAgICAgICByMDogMCxcbiAgICAgICAgcjogcixcbiAgICAgICAgc3RhcnRBbmdsZTogc3RhcnRBbmdsZSxcbiAgICAgICAgZW5kQW5nbGU6IHN0YXJ0QW5nbGUsXG4gICAgICAgIGNsb2Nrd2lzZTogY2xvY2t3aXNlXG4gICAgICB9XG4gICAgfSk7XG4gICAgZ3JhcGhpYy5pbml0UHJvcHMoY2xpcFBhdGgsIHtcbiAgICAgIHNoYXBlOiB7XG4gICAgICAgIGVuZEFuZ2xlOiBzdGFydEFuZ2xlICsgKGNsb2Nrd2lzZSA/IDEgOiAtMSkgKiBNYXRoLlBJICogMlxuICAgICAgfVxuICAgIH0sIHNlcmllc01vZGVsLCBjYik7XG4gICAgcmV0dXJuIGNsaXBQYXRoO1xuICB9LFxuXG4gIC8qKlxuICAgKiBAaW1wbGVtZW50XG4gICAqL1xuICBjb250YWluUG9pbnQ6IGZ1bmN0aW9uIChwb2ludCwgc2VyaWVzTW9kZWwpIHtcbiAgICB2YXIgZGF0YSA9IHNlcmllc01vZGVsLmdldERhdGEoKTtcbiAgICB2YXIgaXRlbUxheW91dCA9IGRhdGEuZ2V0SXRlbUxheW91dCgwKTtcblxuICAgIGlmIChpdGVtTGF5b3V0KSB7XG4gICAgICB2YXIgZHggPSBwb2ludFswXSAtIGl0ZW1MYXlvdXQuY3g7XG4gICAgICB2YXIgZHkgPSBwb2ludFsxXSAtIGl0ZW1MYXlvdXQuY3k7XG4gICAgICB2YXIgcmFkaXVzID0gTWF0aC5zcXJ0KGR4ICogZHggKyBkeSAqIGR5KTtcbiAgICAgIHJldHVybiByYWRpdXMgPD0gaXRlbUxheW91dC5yICYmIHJhZGl1cyA+PSBpdGVtTGF5b3V0LnIwO1xuICAgIH1cbiAgfVxufSk7XG52YXIgX2RlZmF1bHQgPSBQaWVWaWV3O1xubW9kdWxlLmV4cG9ydHMgPSBfZGVmYXVsdDsiXSwibWFwcGluZ3MiOiJBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBOzs7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFEQTtBQUdBO0FBQ0E7Ozs7Ozs7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBREE7QUFXQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUExRkE7QUE0RkE7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==
//# sourceURL=webpack-internal:///./node_modules/echarts/lib/chart/pie/PieView.js
